本分類收錄與**[電影](../Page/電影.md "wikilink")**相關的[小作品](../Page/Wikipedia:小作品.md "wikilink")。若要將條目加入本分類下，請加入****，或者****模板。

若條目有特定主題，請改用以下較為精確的小作品分類模板：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><strong>電影技術</strong>
<ul>
<li><a href="../Page/電影技術.md" title="wikilink">電影技術</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>劇情片</strong>
<ul>
<li><a href="../Category/劇情片.md" title="wikilink">劇情片</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>美國電影</strong>
<ul>
<li><a href="../Page/美國電影.md" title="wikilink">美國電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>中國電影</strong>
<ul>
<li><a href="../Page/中國電影.md" title="wikilink">中國電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>法國電影</strong>
<ul>
<li><a href="../Page/法國電影.md" title="wikilink">法國電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></td>
<td><ul>
<li><strong>香港電影</strong>
<ul>
<li><a href="../Page/香港電影.md" title="wikilink">香港電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>日本電影</strong>
<ul>
<li><a href="../Page/日本電影.md" title="wikilink">日本電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>韓國電影</strong>
<ul>
<li><a href="../Page/韓國電影.md" title="wikilink">韓國電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>新加坡電影</strong>
<ul>
<li><a href="../Page/新加坡電影.md" title="wikilink">新加坡電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
<li><strong>台灣電影</strong>
<ul>
<li><a href="../Page/台灣電影.md" title="wikilink">台灣電影</a>：<strong></strong> 或 <strong></strong></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

[\*](../Category/电影.md "wikilink") [M](../Category/娛樂小作品.md "wikilink")
[M](../Category/藝術小作品.md "wikilink")