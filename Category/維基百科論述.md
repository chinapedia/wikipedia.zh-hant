  - 本分類的主條目是**[Wikipedia:論述](../Page/Wikipedia:論述.md "wikilink")**。

This category contains **Wikipedia essays** — [project
pages](../Page/Wikipedia:Project_namespace.md "wikilink") on
Wikipedia-related topics, but with no official status. Pages tagged with
 categorize here if they are in the Wikipedia namespace and into
[:Category:User essays](../Category/User_essays.md "wikilink") if they
are in userspace. Please use more specific templates where appropriate:

  -
  -
  -
  -
  -
  -
  -   -
  -
  -   -
      -
      -
Essays in the Wikipedia namespace may be edited by anyone. Essays in the
*User* namespace are normally edited only by the user that hosts the
page and are categorized with [:Category:User
essays](../Category/User_essays.md "wikilink"), usually via . User
essays may be moved categorically into the *Wikipedia* namespace and
this category if they are frequently referenced, as evidenced by
becoming an evolving expression of multiple editors. Wikipedia essays
may be moved into userspace (or deleted) if they are found to be
unhelpful or to contradict a settled point of policy.

Additional essays may be found in the
[WMF](../Page/Wikimedia_Foundation.md "wikilink")-wide
[meta:Category:Essays](../Page/meta:Category:Essays.md "wikilink"). For
non-Wikipedia-related essays, as article topics, see
[:Category:Essays](../Category/Essays.md "wikilink").

[论述](../Category/维基百科管理.md "wikilink")
[论述](../Category/维基百科文化.md "wikilink")