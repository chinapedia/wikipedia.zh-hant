**艾伦·杰伊·黑格**（，），[美国](../Page/美国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")、[化学家](../Page/化学家.md "wikilink")，[诺贝尔化学奖获得者](../Page/诺贝尔化学奖.md "wikilink")。

黑格出生于[衣阿华州](../Page/衣阿华州.md "wikilink")[苏城](../Page/苏城.md "wikilink")。1957年在[内布拉斯加大学林肯分校获得物理及数学学士学位](../Page/内布拉斯加大学林肯分校.md "wikilink")。1961年在[伯克利加州大学获得物理学](../Page/伯克利加州大学.md "wikilink")[博士学位](../Page/博士.md "wikilink")。2000年，由于对[导电聚合物的研究和](../Page/导电聚合物.md "wikilink")[艾伦·麦克迪尔米德](../Page/艾伦·麦克迪尔米德.md "wikilink")、[白川英树一起获得了](../Page/白川英树.md "wikilink")[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")\[1\]。如今他是[圣塔芭芭拉加利福尼亚大学的教授](../Page/圣塔芭芭拉加利福尼亚大学.md "wikilink")。

## 参考资料

## 外部链接

  - [诺贝尔官方网站艾伦·杰伊·黑格自传](http://nobelprize.org/nobel_prizes/chemistry/laureates/2000/heeger-autobio.html)

[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:美国化学家](../Category/美国化学家.md "wikilink")
[Category:中国科学院外籍院士](../Category/中国科学院外籍院士.md "wikilink")
[Category:內布拉斯加大學林肯分校校友](../Category/內布拉斯加大學林肯分校校友.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:聖塔芭芭拉加州大學教師](../Category/聖塔芭芭拉加州大學教師.md "wikilink")

1.  <http://www.ch.ntu.edu.tw/nobel/2000.html>