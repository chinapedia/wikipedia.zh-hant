**穆斯林**（），即信仰[伊斯兰教者](../Page/伊斯兰教.md "wikilink")，也是伊斯兰信徒的自称。“穆斯林”一词音译自阿拉伯语“”，其原意为“顺从（信仰）者”、“实现和平者”。\[1\]。

[`缩略图`](https://zh.wikipedia.org/wiki/File:Halaq_at_Masjid_al-Haram,_6_April_2015,_Makkah,_Saudi_Arabia.jpg "fig:缩略图")`的穆斯林]] `[`Culmination_of_prayer_-_Flickr_-_Al_Jazeera_English.jpg`](https://zh.wikipedia.org/wiki/File:Culmination_of_prayer_-_Flickr_-_Al_Jazeera_English.jpg "fig:Culmination_of_prayer_-_Flickr_-_Al_Jazeera_English.jpg")

伊斯兰教是[世界三大宗教之一](../Page/世界三大宗教.md "wikilink")，也是当代发展最快的國際性的宗教之一，信仰[唯一真神](../Page/唯一真神.md "wikilink")，属[亚伯拉罕系](../Page/亞伯拉罕諸教.md "wikilink")[一神教](../Page/一神教.md "wikilink")。全世界大約有16億穆斯林，大約佔全地球人口的25%。在不同國家和地區，穆斯林佔當地人口比率各有不同，如在[北非](../Page/北非.md "wikilink")、[西亚](../Page/西亚.md "wikilink")、[中亚](../Page/中亚.md "wikilink")、[南亚](../Page/南亚.md "wikilink")、[东南亚的很多国家均是多數](../Page/东南亚.md "wikilink")，而在[東亞](../Page/東亞.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[美洲](../Page/美洲.md "wikilink")、[大洋洲則是少數](../Page/大洋洲.md "wikilink")。

[伊斯蘭教在中国有包括](../Page/中国伊斯兰教.md "wikilink")[回族](../Page/回族.md "wikilink")、[维吾尔族](../Page/维吾尔族.md "wikilink")、[塔塔尔族](../Page/塔塔尔族.md "wikilink")、[柯尔克孜族](../Page/柯尔克孜族.md "wikilink")、[哈萨克族](../Page/哈萨克族.md "wikilink")、[塔吉克族](../Page/塔吉克族.md "wikilink")、[乌孜别克族](../Page/乌孜别克族.md "wikilink")、[东乡族](../Page/东乡族.md "wikilink")、[撒拉族](../Page/撒拉族.md "wikilink")、[保安族等](../Page/保安族.md "wikilink")1700多万人口，该十个民族为「主要信仰伊斯兰教」的少数民族。

## 注释

## 参考文献

## 外部链接

## 参见

  - 非穆斯林：[齐米](../Page/齐米.md "wikilink")、[卡菲勒](../Page/卡菲勒.md "wikilink")

  - [穆民](../Page/穆民.md "wikilink")

  - [穆斯林民族](../Page/穆斯林民族.md "wikilink")

  - [伊斯兰黄金时代](../Page/伊斯兰黄金时代.md "wikilink")

  -
{{-}}

[穆斯林](../Category/穆斯林.md "wikilink")
[Muslim](../Category/伊斯蘭教.md "wikilink")
[Muslim](../Category/古蘭經詞彙.md "wikilink")

1.