**布里斯托縣**（）是[美國](../Page/美國.md "wikilink")[羅德島州東部的一個縣](../Page/羅德島州.md "wikilink")，東鄰[麻薩諸塞州](../Page/麻薩諸塞州.md "wikilink")。面積116平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口50,648。縣治[布里斯托](../Page/布里斯托_\(羅德島州\).md "wikilink")（該州各縣皆無縣政府建制）。

## 歷史

布里斯托縣於1746年分割自[麻薩諸塞州的](../Page/麻薩諸塞州.md "wikilink")[布里斯托縣](../Page/布里斯托縣_\(麻薩諸塞州\).md "wikilink")（成立於1685年6月2日）\[1\]，並被併入羅德島州。因此，羅德島州的布里斯托縣一直以來都是羅德島州與麻薩諸塞州之間的領土紛爭。\[2\]

起初，布里斯托縣屬於[普利茅斯殖民地](../Page/普利茅斯殖民地.md "wikilink")。布里斯托縣取名自其郡镇（今县城）[布里斯托尔](../Page/布里斯托尔_\(罗得岛州\).md "wikilink")。布里斯托縣成立於1746，並且其領土疆界並沒有再變化。布里斯托縣包括三個地區：[布里斯托尔](../Page/布里斯托尔_\(罗得岛州\).md "wikilink")、[巴林顿和](../Page/巴林顿_\(罗得岛州\).md "wikilink")[沃伦](../Page/沃伦_\(罗得岛州\).md "wikilink")。\[3\]

## 地理

根據[2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，布里斯托縣的面積為，是羅德島州中最小的縣。其中有，即44.80%的土地為水域。\[4\]若無視其水域領土，布里斯托縣是全美面積第三小的縣。若不把[弗吉尼亚州的城市看作縣的話](../Page/弗吉尼亚州城市列表.md "wikilink")，面積比布里斯托縣更小的縣有[卡拉沃縣和](../Page/卡拉沃縣_\(夏威夷州\).md "wikilink")[纽约县](../Page/曼哈頓.md "wikilink")。

### 毗邻县

  - [麻薩諸塞州的布里斯托縣](../Page/布里斯托縣_\(麻薩諸塞州\).md "wikilink")：東方
  - [普罗维登斯县](../Page/普罗维登斯县.md "wikilink")：北方
  - [肯特县](../Page/肯特县_\(罗得岛州\).md "wikilink")：西方
  - [紐波特縣](../Page/紐波特縣.md "wikilink")：南方

羅德島州的布里斯托尔县與麻薩諸塞州的布里斯托縣是一對稀有的相邻郡，全美只有五對郡名相同的相邻郡。而美國其他四對相邻同名郡有[德克薩斯州的沙賓縣與](../Page/沙賓縣_\(德克薩斯州\).md "wikilink")[路易斯安那州的沙賓縣](../Page/沙賓縣_\(路易斯安那州\).md "wikilink")、[路易斯安那州的猶尼昂縣與](../Page/猶尼昂縣_\(路易斯安那州\).md "wikilink")[阿肯色州的猶尼昂縣](../Page/猶尼昂縣_\(阿肯色州\).md "wikilink")、[马里兰州的肯特县與](../Page/肯特县_\(马里兰州\).md "wikilink")[特拉华州的肯特县](../Page/肯特县_\(特拉华州\).md "wikilink")、以及[阿拉巴馬州的艾斯康比亞縣與](../Page/艾斯康比亞縣_\(阿拉巴馬州\).md "wikilink")[佛羅里達州的艾斯康比亞縣](../Page/艾斯康比亞縣_\(佛羅里達州\).md "wikilink")。

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，布里斯托縣擁有126,697居民、47,224住戶和33,623家庭。\[5\]。其[人口密度為每平方英里](../Page/人口密度.md "wikilink")215居民（每平方公里83居民）。布里斯托縣擁有50,481間房屋单位，其密度為每平方英里86間（每平方公里33間）。布里斯托縣的人口是由73.49%[白人](../Page/歐裔美國人.md "wikilink")、20.66%[黑人](../Page/非裔美國人.md "wikilink")、0.64%[土著](../Page/美國土著.md "wikilink")、1.69%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.04%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、1.27%其他[種族和](../Page/種族.md "wikilink")2.22%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人僅佔人口](../Page/拉丁美洲人.md "wikilink")3.21%。在白人當中，有13.3%為[德國人](../Page/德國人.md "wikilink")、11.3%為[美國人](../Page/歐裔美國人.md "wikilink")、10.9%為[爱尔兰人](../Page/爱尔兰人.md "wikilink")、10.0%為[英國人](../Page/英國人.md "wikilink")、以及5.4%為[意大利人](../Page/意大利人.md "wikilink")。其中，92.5%人口母語為[英語](../Page/英語.md "wikilink")、3.3%為[西班牙語](../Page/西班牙語.md "wikilink")。\[6\]

在47,224住户中，有35.50%擁有一個或以上的兒童（18歲以下）、52.90%為夫妻、13.80%為單親家庭、而有28.80%為非家庭。其中23.00%户人為獨居，8.40%為同居長者。平均每戶有2.61人，而平均每個家庭則有3.06人。在50,648居民中，有27.30%為18歲以下、10.10%為18至24歲、29.80%為25至44歲、21.20%為45至64歲以及11.70%為65歲以上。人口的年齡中位數為34歲，而每100個女性就有93.10個男性。\[7\]

布里斯托縣的住戶收入中位數為$40,950，而家庭收入中位數則為$46,504。男性的收入中位數為$32,660，而女性的收入中位數則為$24,706。布里斯托縣的[人均收入為](../Page/人均收入.md "wikilink")$18,662。約8.10%家庭和10.70%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")。\[8\]

## 參考

[B](../Category/羅德島州行政區劃.md "wikilink")
[Category:大波士顿](../Category/大波士顿.md "wikilink")
[Category:罗德岛州布里斯托尔县](../Category/罗德岛州布里斯托尔县.md "wikilink")
[Category:普罗维登斯都会区](../Category/普罗维登斯都会区.md "wikilink")

1.  [Bristol County, Massachusetts,
    USA](http://www.rootsweb.ancestry.com/~mabristo/mabristo.htm)

2.

3.  *History of Bristol County, Massachusetts with Biographical Sketches
    of many of its Pioneers and Prominent Men, Part 1* edited by Duane
    Hamilton Hurd. J.W. Lewis and Co., 1883.
    [1](http://books.google.com/books?id=uauYBOCKCS0C&source=gbs_navlinks_s).
    p. 1.

4.

5.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

6.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

7.
8.