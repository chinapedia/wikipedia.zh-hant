[教宗](../Page/教宗.md "wikilink")**恩仁四世**（，），原名，於1431年3月3日—1447年2月23日岀任教宗。

## 譯名列表

  - 恩仁四世：[香港天主教教區檔案　歷任教宗](http://archives.catholic.org.hk/The%20Popes/PO-Index.htm)作恩仁四世。
  - 尤金四世：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=eugenius&mode=3)、[國立編譯舘](http://www.nict.gov.tw/tc/dic/search.php?p=2&ps=20&w1=eugene&w2=&w3=&c1=&c2=c2&l=&u=&pr=&r=&o=1&pri=undefined)作尤金。
  - 犹金四世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作犹金。

[E](../Category/教宗.md "wikilink")
[Category:威尼斯人](../Category/威尼斯人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")
[Category:15世纪意大利人](../Category/15世纪意大利人.md "wikilink")