**李鼎彝**（），[中国學者](../Page/中国.md "wikilink")、教師，[學名](../Page/學名.md "wikilink")**怀心**、**怀芯**，[字](../Page/表字.md "wikilink")**玑衡**、**季恒**，生於[大清帝国](../Page/大清帝国.md "wikilink")[吉林省](../Page/吉林省_\(清\).md "wikilink")[扶余县](../Page/扶余县.md "wikilink")，祖籍[山東](../Page/山東.md "wikilink")[濰縣](../Page/濰縣.md "wikilink")\[1\]\[2\]。曾任[東北大學](../Page/東北大學.md "wikilink")、吉林女子師範學校、[吉林大学講師](../Page/吉林大学.md "wikilink")，吉林省立第六中學校長，[中日戰爭時](../Page/中日戰爭.md "wikilink")，作為[國民黨](../Page/國民黨.md "wikilink")[軍統](../Page/軍統.md "wikilink")[特務潛伏於](../Page/特務.md "wikilink")[汪精衛政權](../Page/汪精衛政權.md "wikilink")。1949年隨[國民黨政府撤退來臺](../Page/國民黨政府.md "wikilink")，擔任[省立臺中第一中學國文科教師](../Page/國立臺中第一高級中學.md "wikilink")，兼國文科主任。

其子[李敖为知名](../Page/李敖.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")、[作家](../Page/作家.md "wikilink")。

## 早年

李鼎彝，祖先來自[雲貴烏撒](../Page/雲貴.md "wikilink")（今[雲南](../Page/雲南.md "wikilink")[鎮雄縣和](../Page/鎮雄縣.md "wikilink")[貴州](../Page/貴州.md "wikilink")[威寧縣](../Page/威寧縣.md "wikilink")），[明太祖](../Page/明太祖.md "wikilink")[洪武年間](../Page/洪武.md "wikilink")[遷徙至](../Page/遷徙.md "wikilink")[山東](../Page/山東.md "wikilink")\[3\]，李敖考據，其祖先可能為[苗族](../Page/苗族.md "wikilink")，是被[朝廷的](../Page/朝廷.md "wikilink")[征南將軍](../Page/征南將軍.md "wikilink")[傅友德強制遷徙來的](../Page/傅友德.md "wikilink")。父親名為[李鳳亭](../Page/李鳳亭.md "wikilink")，生於[山東](../Page/山東.md "wikilink")[濰縣](../Page/濰縣.md "wikilink")，幼年時遇[飢荒](../Page/飢荒.md "wikilink")，與母親一同行乞，卻遇到一群野狗攻擊，母親為了保護他而喪命，李鳳亭為了生存，[闖關東落腳](../Page/闖關東.md "wikilink")[關外](../Page/關外.md "wikilink")，因[不識字](../Page/不識字.md "wikilink")，曾經做過[農夫](../Page/農夫.md "wikilink")、[鐵路](../Page/鐵路.md "wikilink")[工人](../Page/工人.md "wikilink")、[馬賊](../Page/馬賊.md "wikilink")、[保安人員](../Page/保安.md "wikilink")，後成為[銀樓老闆](../Page/銀樓.md "wikilink")\[4\]。

李鼎彝是家中的次子，幼年入塾就讀，學名**懷心**，又作**懷芯**，資質穎異，能背誦《[論語](../Page/論語.md "wikilink")》、《[道德經](../Page/道德經.md "wikilink")》\[5\]。1920年，以吉林省公費生資格，進入[北京大學中文系](../Page/北京大學.md "wikilink")。因[李懷心就读](../Page/李懷心.md "wikilink")[北京大学时獲得](../Page/北京大学.md "wikilink")[吉林省的公费](../Page/吉林省.md "wikilink")，[冒籍吉林](../Page/冒籍.md "wikilink")[扶余县](../Page/扶余县.md "wikilink")，但其他親人，包括李敖，都以山東為籍貫，后来[國民黨政府將李家全家统一改为](../Page/國民黨政府.md "wikilink")「吉林扶餘」\[6\]。

1926年畢業後，回鄉擔任[東北大學講師](../Page/東北大學.md "wikilink")，吉林六中校長。寫作《中國文學史》。

## 个人年表

  - 1926年，[北大](../Page/北大.md "wikilink")[毕业后](../Page/毕业.md "wikilink")，任吉林六中[校长](../Page/校长.md "wikilink")。为了生计，又任教于吉林女子師範學校、[吉林大学等校](../Page/吉林大学.md "wikilink")。
  - 1931年[奉天事变後他与](../Page/奉天事变.md "wikilink")[吴焕章等人组织](../Page/吴焕章.md "wikilink")「黑龙江青年抗敌会」，任三民主義力行社「[东北四省抗敌协会](../Page/东北四省.md "wikilink")」（東北抗聯）理事，幫助[馬占山將軍及東北抗聯进行地下抗日活动](../Page/馬占山.md "wikilink")。曾於1932年11月幫助馬占山將軍撤到[蘇俄](../Page/蘇俄.md "wikilink")。
  - 1937年，東北抗聯被日軍圍剿失去組織關係，李鼎彝為避難举家逃至[北京](../Page/北京.md "wikilink")，数次搬迁后住内务部胡同甲四十号。不久接上[軍統關係](../Page/軍統.md "wikilink")，[七七事变爆發](../Page/七七事变.md "wikilink")，[国民党弃城](../Page/国民党.md "wikilink")，北京淪陷。經軍統同意李鼎彝入北京[汪精卫政府法部任科员](../Page/汪精卫政府.md "wikilink")，负责北京地区「地下抗日协会」情報工作。
  - 1940年，李鼎彝任[太原市禁烟局长](../Page/太原市.md "wikilink")。
  - 1941年，[日軍清除軍統太原分部](../Page/日軍.md "wikilink")，李鼎彝被查。但因無法查實與軍統關係，被指控“[贪污罪](../Page/贪污.md "wikilink")”入獄，六个月后释放。李回[北京一怒辭去軍統職務闲居](../Page/北京.md "wikilink")。
  - 1945年，李鼎彝在[东北营城子煤矿任总务处长](../Page/中国东北地区.md "wikilink")。
  - 1948年，李鼎彝决定南下避乱，转[天津赴](../Page/天津.md "wikilink")[上海](../Page/上海.md "wikilink")。途中满目疮痍，亲身体会到了[國共內戰给人民带来的苦难](../Page/國共內戰.md "wikilink")。
  - 1949年，[上海危急](../Page/上海.md "wikilink")。**李鼎彝**與其妻張桂貞，其子[李敖](../Page/李敖.md "wikilink")，登船离开[上海](../Page/上海.md "wikilink")。12日，抵[台湾](../Page/台湾.md "wikilink")，居[台中市](../Page/台中市.md "wikilink")，擔任[省立台中第一中學國文科教師](../Page/國立臺中第一高級中學.md "wikilink")，兼國文科主任。
  - 1955年，病逝於[台中](../Page/台中.md "wikilink")。骨灰置於[陽明山公墓](../Page/陽明山.md "wikilink")。

## 参考文献

## 相关条目

  - [扶余县](../Page/扶余县.md "wikilink")
  - [李敖](../Page/李敖.md "wikilink")

## 外部链接

  - [李敖二姐忆家事](http://shszx.eastday.com/node2/node4810/node4851/node4864/userobject1ai36224.html)

[D](../Category/李姓.md "wikilink")
[Category:扶余人](../Category/扶余人.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")

1.  [李敖傳奇（上）：他的身世和人生，如果拍成電影，絕對足夠精彩！](https://www.xuehua.us/2018/03/31/%E6%9D%8E%E6%95%96%E4%BC%A0%E5%A5%87%EF%BC%88%E4%B8%8A%EF%BC%89%EF%BC%9A%E4%BB%96%E7%9A%84%E8%BA%AB%E4%B8%96%E5%92%8C%E4%BA%BA%E7%94%9F%EF%BC%8C%E5%A6%82%E6%9E%9C%E6%8B%8D%E6%88%90%E7%94%B5%E5%BD%B1/zh-tw/)，雪花新聞，2018年3月31日

2.  [李敖五十年表](http://leeao.com.cn/forfashion/leeao/zizuan/fifty.htm)，李敖研究

3.
4.
5.
6.