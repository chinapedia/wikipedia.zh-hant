**5′端帽**()是在[真核生物中](../Page/真核生物.md "wikilink")[信使RNA](../Page/信使RNA.md "wikilink")（mRNA）的[5′端经修改後形成的的雙](../Page/5'端.md "wikilink")[核苷酸端点](../Page/核苷酸.md "wikilink")。5′加帽的過程對建立成熟的mRNA作[翻譯非常重要](../Page/翻譯_\(遺傳學\).md "wikilink")。加帽確保了mRNA在[蛋白質生物合成中進行轉譯的穩定性](../Page/蛋白質生物合成.md "wikilink")，並在[細胞核中是高度調控的過程](../Page/細胞核.md "wikilink")。

## 結構

[5'_cap_structure.png](https://zh.wikipedia.org/wiki/File:5'_cap_structure.png "fig:5'_cap_structure.png")
[Ribose_structure_2.png](https://zh.wikipedia.org/wiki/File:Ribose_structure_2.png "fig:Ribose_structure_2.png")

5′端帽是在[mRNA分子的](../Page/mRNA.md "wikilink")[5′端](../Page/5'端.md "wikilink")，並且包含一個以不正常5′至5′三磷酸鹽鏈結著mRNA的[鳥苷](../Page/鳥苷.md "wikilink")。

更多的修飾包括鳥苷的[甲基化](../Page/甲基化.md "wikilink")、及其他mRNA的5′端內頭三個[核酸糖的](../Page/核酸糖.md "wikilink")2′[羥基的甲基化](../Page/羥基.md "wikilink")。兩個2′羥基的甲基化於圖內可見。

5′端帽的功能與[RNA分子的](../Page/RNA.md "wikilink")[3′端相似](../Page/3'端.md "wikilink")，但端帽核酸糖的5′碳是鏈接的，但3′端則不是的。這會提供足夠對[核酸外切酶的抵抗](../Page/核酸外切酶.md "wikilink")。

## 加帽過程

加帽會是由[RNA沒有修改的](../Page/RNA.md "wikilink")[5′端開始](../Page/5'端.md "wikilink")。這會形成一個最後的[核苷酸](../Page/核苷酸.md "wikilink")，而隨後附帶於5′碳的三個[磷酸基](../Page/磷酸基.md "wikilink")。

1.  其中一個終結的磷酸基會由[磷酸酶移除](../Page/磷酸酶.md "wikilink")，留下兩個磷酸基。
2.  [三磷酸鳥苷](../Page/三磷酸鳥苷.md "wikilink")（GTP）會被[鳥苷轉移酶加入磷酸基](../Page/鳥苷轉移酶.md "wikilink")，並在過程中會失去本身的兩個磷酸基。這就會造成5′至5′三磷酸基鏈結。
3.  [鳥苷會被](../Page/鳥苷.md "wikilink")[甲基轉移酶所](../Page/甲基轉移酶.md "wikilink")[甲基化](../Page/甲基化.md "wikilink")。
4.  其他甲基轉移酶會選擇性地將接近5′的核苷酸甲基化。

## 加帽目標

在[轉錄前進行加帽所需的](../Page/轉錄.md "wikilink")[酶是與](../Page/酶.md "wikilink")[RNA聚合酶Ⅱ連結的](../Page/RNA聚合酶II.md "wikilink")。當新轉錄本的[5′端進入酶時就會開始進行加帽](../Page/5'端.md "wikilink")，這種方式的加帽是要確保進行[多聚腺苷酸化](../Page/多聚腺苷酸化.md "wikilink")。

用作加帽的酶必須是與RNA聚合酶Ⅱ連結的，為要確保其對某一轉錄本（差不多就是[mRNA](../Page/mRNA.md "wikilink")）的獨有性。

## 功能

5′端帽有四個主要功能：

  - 調控[細胞核的輸出](../Page/細胞核.md "wikilink")。細胞核輸出是由[帽結合複合物](../Page/帽結合複合物.md "wikilink")（CBC）來調控，它只會與加帽的[RNA結合](../Page/RNA.md "wikilink")。CBC接著會由[核孔複合物所辨認及排出](../Page/核孔複合物.md "wikilink")。
  - 阻止因[核酸外切酶造成的降解](../Page/核酸外切酶.md "wikilink")。[mRNA因核酸外切酶造成的降解會被功能上類似](../Page/mRNA.md "wikilink")[3′端的](../Page/3'端.md "wikilink")5′端帽所阻止。mRNA的[半衰期會因而上升](../Page/半衰期.md "wikilink")，方便[真核生物輸出所需的長時間](../Page/真核生物.md "wikilink")。
  - 促進[轉譯](../Page/轉譯.md "wikilink")。5′端帽亦是用作與[核糖體及](../Page/核糖體.md "wikilink")[翻譯起始因子結合](../Page/翻譯起始因子.md "wikilink")。CBC亦會涉及此過程，負責招聚起始因子。
  - 促進除去5′近端[內含子](../Page/內含子.md "wikilink")。這個過程的機制並不清楚，但5′端帽似乎會繞圈及與[剪接體在剪接過程中互相作用](../Page/剪接體.md "wikilink")，從而促進除去內含子。\[1\]

## 参见

  - [核糖體](../Page/核糖體.md "wikilink")
  - [RNA聚合酶Ⅱ](../Page/RNA聚合酶II.md "wikilink")

## 参考文献

## 外部連結

  -
  -
{{-}}

[Category:RNA](../Category/RNA.md "wikilink")

1.