Fair use in the article related to the named football team：

  - This image is a low-resolution image of the logo of an organization.
  - This image does not limit the copyright holder's ability to profit
    from the original source, nor will it dilute the importance or
    recognition of the logo in connection with its organization.
  - This image enhances the article in which it is displayed as it part
    of the visual identity established by the organization identified
    therein.
  - This image is used in compliance with the policy for the display of
    sports team logos as stated at Wikipedia:logos.
  - The image is not available from a free source.

[af:Lêer:SV Wehen
Logo.png](../Page/af:Lêer:SV_Wehen_Logo.png.md "wikilink")
[de:Datei:Logo SV Wehen
Wiesbaden.svg](../Page/de:Datei:Logo_SV_Wehen_Wiesbaden.svg.md "wikilink")
[en:<File:SV> Wehen
Wiesbaden.png](../Page/en:File:SV_Wehen_Wiesbaden.png.md "wikilink")
[fr:Fichier:Wehen-wiesbaden-neu.png](../Page/fr:Fichier:Wehen-wiesbaden-neu.png.md "wikilink")
[it:<File:SV> Wehen
Wiesbaden.png](../Page/it:File:SV_Wehen_Wiesbaden.png.md "wikilink")
[ro:Fișier:SV Wehen
Wiesbaden.png](../Page/ro:Fișier:SV_Wehen_Wiesbaden.png.md "wikilink")
[ru:Файл:ФК Веен
Висбаден.png](../Page/ru:Файл:ФК_Веен_Висбаден.png.md "wikilink")