[Collagentriplehelix.png](https://zh.wikipedia.org/wiki/File:Collagentriplehelix.png "fig:Collagentriplehelix.png")

**膠原蛋白**（collagen）佔哺乳類動物總蛋白質約20%，也是人體的一種非常重要的[蛋白質](../Page/蛋白質.md "wikilink")，主要存在於[结缔组织中](../Page/结缔组织.md "wikilink")。它有很强的伸张能力，是[韧带的主要成份](../Page/韧带.md "wikilink")，胶原蛋白也是[细胞外基质的主要组成成分](../Page/细胞外基质.md "wikilink")。它使[皮膚保持彈性](../Page/皮膚.md "wikilink")，而膠原蛋白的老化，則使皮膚出現[皺紋](../Page/皺紋.md "wikilink")。膠原蛋白亦是[眼睛](../Page/眼睛.md "wikilink")[角膜的主要成份](../Page/角膜.md "wikilink")，但以[結晶形式組成](../Page/結晶.md "wikilink")。同其他蛋白质相同，膠原蛋白無法被人体直接吸收，口服会被分解为[氨基酸](../Page/氨基酸.md "wikilink")。

## 胶原蛋白构造

构成胶原的[氨基酸序列非常有特色](../Page/氨基酸.md "wikilink")。首先，它富含[甘氨酸和](../Page/甘氨酸.md "wikilink")[脯氨酸](../Page/脯氨酸.md "wikilink")[残基](../Page/殘基.md "wikilink")，前者的含量达到总氨基酸残基的1/3后者则接近1/4；其次，序列中含有胺基酸的衍生物和[羟脯氨酸](../Page/羟脯氨酸.md "wikilink")，这两种氨基酸是在蛋白质[一级结构序列形成之后由特定的](../Page/一级结构.md "wikilink")[酶作用于序列中的](../Page/酶.md "wikilink")[赖氨酸和脯氨酸形成的](../Page/赖氨酸.md "wikilink")；最后，它的序列中只含有很少的[酪氨酸残基](../Page/酪氨酸.md "wikilink")，并且不含有[色氨酸和](../Page/色氨酸.md "wikilink")[半胱氨酸残基](../Page/半胱氨酸.md "wikilink")。膠原蛋白一级结构的另一个特点是它的氨基酸的排列。这些氨基酸一般以-甘氨酸-脯氨酸-羟脯氨酸-三联交替出现的顺序排列。只有很少的蛋白质有这样规则的氨基酸排列\[1\]。

在空间结构上，胶原蛋白显示出特殊的三股螺旋缠绕的结构，三条相互独立的胶原蛋白肽链依靠甘氨酸之间形成的氢键维系三股螺旋相互缠绕的结构。胶原蛋白肽链的三股螺旋结构不同于普通的[α螺旋结构](../Page/α螺旋.md "wikilink")，它的螺距更大，但每一圈螺旋所包含的氨基酸残基数却很小，仅为3.3个，因此胶原蛋白的三股螺旋显得细而长，螺旋中间的空间很小，仅能容纳一个[氢](../Page/氢.md "wikilink")[原子](../Page/原子.md "wikilink")，只有甘氨酸能够胜任这个位置。另外脯氨酸所特有的[肽平面夹角也是形成这种特殊螺旋结构的必须因素](../Page/肽平面.md "wikilink")。这也是胶原蛋白肽链中-甘氨酸-脯氨酸-羟脯氨酸-三联序列交替出现的原因。胶原蛋白这种特殊的三股螺旋结构保证了它的机械强度。这种三股螺旋被称为原胶原（procollagen）。

若干个原胶原横向堆积，序列中所含有的羟赖氨酸和羟脯氨酸侧链在酶作用下氧化生成[醛](../Page/醛.md "wikilink")，相互之间发生[羟醛缩合反应形成原胶原之间的共价链接](../Page/羟醛缩合.md "wikilink")，这种结构被称为[胶原微纤维](../Page/胶原微纤维.md "wikilink")。许多胶原微纤维横向堆积，以相同的方式通过[共价键链接](../Page/共价键.md "wikilink")，形成[胶原纤维](../Page/胶原纤维.md "wikilink")。胶原纤维是胶原蛋白行使生理作用的基本形态，在生物体内胶原纤维交织成富有机械强度和弹性的网状结构成为结缔组织最基本的组成成分。

## 醫療用途

  - 心臟應用
  - [整容手術](../Page/整容手術.md "wikilink")
  - 整形外科使用
  - 傷口護理管理使用

## 口服及外用爭議

对口服的胶原蛋白产品，在消化过程中胶原蛋白也和其他蛋白质一样。由于人体很难直接吸收蛋白质或多肽，口服蛋白质、多肽后，几乎都会在消化道消化成氨基酸后才被人体吸收。\[2\]

实际上，不管是口服还是外用，目前都没有证据显示额外补充胶原蛋白对延缓衰老和除皱有什么特殊功效。

## 參考文獻

## 外部連結

  - [12 types of
    collagen](http://themedicalbiochemistrypage.org/extracellularmatrix.html)
  - [Database of type I and type III collagen
    mutations](http://www.le.ac.uk/genetics/collagen/)
  - [Science.dirbix
    Collagen](http://science.dirbix.com/biology/collagen)
  - [Collagen Stability
    Calculator](http://compbio.cs.princeton.edu/csc/)
  - [Computer-generated animations of the assembly of Type I and Type IV
    Collagens](http://www.mc.vanderbilt.edu/cmb/collagen/)

[Category:膠原蛋白](../Category/膠原蛋白.md "wikilink")
[Category:结构蛋白](../Category/结构蛋白.md "wikilink")
[Category:食用增稠剂](../Category/食用增稠剂.md "wikilink")
[Category:整合素](../Category/整合素.md "wikilink")

1.  Linesnmayer TF, Gibney E, Gorden MK, Marchant JK, Hayashi M & Fitch
    JM. 1990. Extracellylar materices of developing chick retina and
    cornea. Localization of mRNAs for collagen types II and IX by in
    situ hybridization. Invest ophthalmol visual sci 31 (7):1271-1276
2.