《**One more time, One more
chance**》是1997年1月22日由「」發表的[山崎將義](../Page/山崎將義.md "wikilink")（）的第五張[單曲](../Page/單曲.md "wikilink")。

## 概要

  - 這是山崎第一次主演的電影《》（筱原哲雄監督）的主題歌，成為山崎的初期代表曲子之一。
  - 銷售額30萬張的作品，同年發行的專輯《[Home](../Page/Home.md "wikilink")》也收錄了此曲。
  - 曲子是山崎從山口縣進京後的1993年寫成的，山崎當時居住的[櫻木町在歌詞中登場](../Page/櫻木町.md "wikilink")。
  - 曲子是一個人與戀人分離後，難過的slow ballad。
  - 在第一次發表十年後的2007年，動畫電影《[秒速5公分](../Page/秒速5公分.md "wikilink")》採用本歌做為主題曲。這是由於動畫監督[新海誠認為這首歌很符合電影劇情](../Page/新海誠.md "wikilink")，這是一個樂曲作為不同電影的主題歌的特例。與2007年3月3日重新錄製的獨奏獨唱版本一起作為「One
    more time, One more chance『秒速5公分』Special
    Edition」，由[環球音樂再發行](../Page/環球音樂.md "wikilink")。
  - 香港歌手[鄭中基曾於](../Page/鄭中基.md "wikilink")1999年翻唱此曲並從新填上粵語歌詞，收錄於其專輯《ONE
    MORE TIME》中。

## 收錄曲

  - 原作

<!-- end list -->

1.  One more time, One more chance
      - 作詞、作曲：山崎將義；編曲：森俊之
2.  妖精といた夏
      - 作詞・作曲・編曲：山崎將義
3.  One more time, One more chance（卡拉OK版）
      - 作詞、作曲：山崎將義；編曲：森俊之

<!-- end list -->

  - [秒速5公分Special](../Page/秒速5公分.md "wikilink") Edition

<!-- end list -->

1.  One more time, One more chance
      - 作詞、作曲：山崎將義；編曲：森俊之
2.  雪の駅 ～One more time, One more chance～（from「秒速5公分」 Sound track）
      - 作詞、作曲；編曲：山崎將義
3.  One more time, One more chance（獨奏獨唱版本）
      - 作詞、作曲；編曲：山崎將義

## 外部連結

  -
  - [山崎まさよし 官方網站](http://www.universal-music.co.jp/yamazaki_masayoshi/)

[Category:1997年單曲](../Category/1997年單曲.md "wikilink")
[Category:日语歌曲](../Category/日语歌曲.md "wikilink")
[Category:電影主題曲](../Category/電影主題曲.md "wikilink")