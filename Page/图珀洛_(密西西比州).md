[Lee_County_Mississippi_Incorporated_and_Unincorporated_areas_Tupelo_Highlighted.svg](https://zh.wikipedia.org/wiki/File:Lee_County_Mississippi_Incorporated_and_Unincorporated_areas_Tupelo_Highlighted.svg "fig:Lee_County_Mississippi_Incorporated_and_Unincorporated_areas_Tupelo_Highlighted.svg")
**图珀洛**（[英语](../Page/英语.md "wikilink")：****）是[美国](../Page/美国.md "wikilink")[密西西比州的第](../Page/密西西比州.md "wikilink")8大城市，[李县的最大城市](../Page/李县_\(密西西比州\).md "wikilink")，也是该县的县治所在地。根据2000年的人口普查结果，图珀洛人口为34,211，2006年人口增加至35,930，横跨李、[庞托托克和](../Page/庞托托克县_\(密西西比州\).md "wikilink")[伊塔宛巴三县的](../Page/伊塔宛巴县.md "wikilink")[都会区人口则为](../Page/图珀洛都会区.md "wikilink")131,953。该城最为人所知的是作为歌手[埃尔维斯·普雷斯利的出生地](../Page/埃尔维斯·普雷斯利.md "wikilink")。\[1\]

图珀洛位于密西西比州东北部，[田纳西州](../Page/田纳西州.md "wikilink")[孟斐斯和](../Page/孟斐斯_\(田纳西州\).md "wikilink")[亚拉巴马州](../Page/亚拉巴马州.md "wikilink")[伯明翰之间](../Page/伯明翰_\(亚拉巴马州\).md "wikilink")，数年后将成为[22号州际公路的](../Page/22号州际公路.md "wikilink")[美国国道78号途经当地](../Page/美国国道78号.md "wikilink")。

## 當地名人

  - [艾維斯·普里斯萊](../Page/艾維斯·普里斯萊.md "wikilink")（1935年-1977年），一位音樂家和演員。[Elvis_Presley_house_in_Tupelo_3.jpg](https://zh.wikipedia.org/wiki/File:Elvis_Presley_house_in_Tupelo_3.jpg "fig:Elvis_Presley_house_in_Tupelo_3.jpg")\]\]

## 参考资料

## 外部链接

  - [图珀洛市官方网站](https://web.archive.org/web/20080216154013/http://www.ci.tupelo.ms.us/)

[Tupelo](../Category/密西西比州城市.md "wikilink")

1.