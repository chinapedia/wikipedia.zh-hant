**錫良**（），字**清弼**，**拜岳特氏**（**巴岳特氏**），[蒙古](../Page/蒙古.md "wikilink")[镶蓝旗人](../Page/镶蓝旗.md "wikilink")。晚清政治人物。

## 生平

[同治十三年](../Page/同治.md "wikilink")（1874年）甲戌科[进士](../Page/进士.md "wikilink")。

[光绪二十六年](../Page/光绪.md "wikilink")（1900年）时任[湖南](../Page/湖南.md "wikilink")[布政使](../Page/布政使.md "wikilink")，率部北上保卫首都[北京](../Page/北京.md "wikilink")，后得知[慈禧](../Page/慈禧.md "wikilink")、[光绪帝一行人逃往](../Page/光绪帝.md "wikilink")[西安](../Page/西安.md "wikilink")，便改道奔[山西](../Page/山西.md "wikilink")[太原护驾](../Page/太原.md "wikilink")，拜[山西巡抚](../Page/山西巡抚.md "wikilink")，防守后路。

光绪二十七年（1901年）12月，署理[河南巡抚兼管河工事务](../Page/河南巡抚.md "wikilink")。开办豫南公司，开采河南南部矿產。在开封创建河南大学堂。

光绪二十九年（1903年），调任[四川总督](../Page/四川总督.md "wikilink")，推行“新政”。

光绪三十三年（1907年）3月，调任[云贵总督](../Page/云贵总督.md "wikilink")。

光绪三十四年（1908年），[孙文革命党人在云南河口发动起事](../Page/孙文.md "wikilink")，锡良亲自督师，前往镇压。

[宣统元年](../Page/宣统.md "wikilink")（1909年）2月，授[钦差大臣](../Page/钦差大臣.md "wikilink")，[总督](../Page/总督.md "wikilink")[奉天](../Page/奉天.md "wikilink")、[吉林](../Page/吉林.md "wikilink")、[黑龙江等](../Page/黑龙江.md "wikilink")[东三省](../Page/东三省.md "wikilink")，兼任[热河都统](../Page/热河.md "wikilink")。

宣统二年（1910年）10月，与[瑞澂领衔](../Page/瑞澂.md "wikilink")，联名[內地十八省督抚致电](../Page/內地十八省.md "wikilink")[军机处](../Page/军机处.md "wikilink")，要求明年即开[国会](../Page/国会.md "wikilink")，组织[责任内阁](../Page/责任内阁.md "wikilink")，推动[君主立宪运动](../Page/君主立宪.md "wikilink")。

清亡後引退，1917年病逝。

## 參考文獻

  - 《[清史稿](../Page/清史稿.md "wikilink")》

{{-}}

[Category:蒙古鑲藍旗人](../Category/蒙古鑲藍旗人.md "wikilink")
[Category:清朝東三省總督](../Category/清朝東三省總督.md "wikilink")
[Category:清朝四川總督](../Category/清朝四川總督.md "wikilink")
[Category:清朝東河總督](../Category/清朝東河總督.md "wikilink")
[Category:清朝奉天巡撫](../Category/清朝奉天巡撫.md "wikilink")
[Category:参政院参政](../Category/参政院参政.md "wikilink")