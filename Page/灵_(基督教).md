在《[聖經](../Page/聖經.md "wikilink")》中，**靈**（[希伯來文](../Page/希伯來文.md "wikilink")：*ruach*，[希臘文](../Page/希臘文.md "wikilink")：*pneuma*）和“[魂](../Page/魂.md "wikilink")”（希伯來文：*nephesh*，希臘文：*psyche*）是不同的，是人裡面兩個不同的部分。

初期的[基督徒很多持這種觀點](../Page/基督徒.md "wikilink")，如[保羅在他的書信中提到](../Page/保罗.md "wikilink")，“又願你們的**靈**，與**魂**，與身子，得蒙保守。”\[1\]，以及“因为神的话是活的，是有功效的，比一切两刃的剑更锋利，能以刺入、甚至剖开**魂**与**灵**，骨节与骨髓，连心中的思念和主意都能辨明。
”\[2\]

大半[教父也都主張此](../Page/教父.md "wikilink")[三元論](../Page/三元論.md "wikilink")，如[愛任紐](../Page/愛任紐.md "wikilink")（Irenaeus）、[遊斯丁](../Page/遊斯丁.md "wikilink")（Justin
Martyr）、[革利免](../Page/革利免.md "wikilink")（Clement of
Alexander）、[俄利根](../Page/俄利根.md "wikilink")（Origen）和[岱迪瑪](../Page/岱迪瑪.md "wikilink")（Didymus
of
Alexandria）等，都是三元論者。遊斯丁在《论复活》中说道：“因为身体是魂的家；魂是灵的家。因着一同为神坚定的救赎而欢呼的三部分，将一同得救。”\[3\]。革力免也在《导师》中说道：“因为根据真理，灵与魂结合，从它（神圣的混合）得到灵感；而肉体，也因为道成肉身的缘故，归于道。”\[4\]。

但是，仍有不少基督徒肯定三元論。其中包括[德國的Delitzsch](../Page/德國.md "wikilink")；[南非的](../Page/南非.md "wikilink")[慕安德烈](../Page/慕安德烈.md "wikilink")、梅爾、[賓路易師母](../Page/賓路易師母.md "wikilink")、[達秘](../Page/達秘.md "wikilink")（J.
N.
Darby）和[中國的](../Page/中國.md "wikilink")[倪柝聲](../Page/倪柝聲.md "wikilink")。倪柝聲在他的《[屬靈人](../Page/屬靈人.md "wikilink")》中把《聖經》中論到靈、魂、體的經節分類，並加以分析，得出人的靈是人最深入的部分，又可分[良心](../Page/良心.md "wikilink")、[直覺和](../Page/直覺.md "wikilink")[交通的功用](../Page/交通.md "wikilink")，是人接触[神](../Page/神.md "wikilink")、敬拜神的惟一機關；魂是人的人格，它又可分[心思](../Page/心思.md "wikilink")、[意志和](../Page/意志.md "wikilink")[情感](../Page/情感.md "wikilink")；而體是外在與天然界來往的部分\[5\]。

## 参考文献

[Category:神學](../Category/神學.md "wikilink")
[Category:活力论](../Category/活力论.md "wikilink")

1.  [帖撒羅尼迦前書](../Page/帖撒羅尼迦前書.md "wikilink")5章23节
2.  [希伯来书](../Page/希伯来书.md "wikilink")4章12节
3.  遊斯丁：《论复活》第10章
4.  革力免：《导师》第2卷
5.  [倪柝聲](../Page/倪柝聲.md "wikilink")：《[屬靈人](../Page/屬靈人.md "wikilink")》