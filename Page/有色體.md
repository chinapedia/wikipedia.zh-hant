**有色體**（，又稱**雜色體**），是[植物](../Page/植物.md "wikilink")、[藻類等能行光合作用的真核](../Page/藻類.md "wikilink")[細胞中的一種](../Page/細胞.md "wikilink")[質粒體](../Page/質粒體.md "wikilink")\[1\]，通稱含有色素的質粒體，常和沒有色素的[白色體比較](../Page/白色體.md "wikilink")。有色體因含有[胡蘿蔔素](../Page/胡蘿蔔素.md "wikilink")（Carotene）、[類胡蘿蔔素](../Page/類胡蘿蔔素.md "wikilink")（Carotenoid）、[番茄紅素](../Page/番茄紅素.md "wikilink")（Lycopene）、[辣椒紅素](../Page/辣椒紅素.md "wikilink")（Capsanthin）等色素而呈紅色、橘色或黃色，其中類胡蘿蔔素占一半以上\[2\]。有色體是進行生產與儲存色素的場所\[3\]，為多數植物[花瓣及](../Page/花瓣.md "wikilink")[果實的顏色來源](../Page/果實.md "wikilink")。

有色體常被用作與白色體相對，[葉綠體因含有色素](../Page/葉綠體.md "wikilink")，也能被視為廣義的有色體之一。但有色體仍多專指含有葉黃素、類胡蘿蔔素等非葉綠素色素的質粒體。\[4\]\[5\]

## 形態

有色體大多在植物有色的器官細胞中被發現，如[水果和](../Page/水果.md "wikilink")[花瓣](../Page/花瓣.md "wikilink")，其中的[色素為這些部位的顏色來源](../Page/色素.md "wikilink")，主要的目的為吸引昆蟲等動物前來協助[授粉或傳播](../Page/授粉.md "wikilink")[種子](../Page/種子.md "wikilink")。
有色體的形態隨其中貯存的色素而異，如[胡蘿蔔中的有色體在成熟後呈帶狀](../Page/胡蘿蔔.md "wikilink")、針狀或板狀，[辣椒果皮中的有色體呈細絲束狀](../Page/辣椒.md "wikilink")，番茄果皮中呈柱狀，[向日葵](../Page/向日葵.md "wikilink")、[蒲公英等的花瓣中的有色體則呈球狀](../Page/蒲公英.md "wikilink")\[6\]。

## 轉換

有色體和[白色體](../Page/白色體.md "wikilink")、[葉綠體等](../Page/葉綠體.md "wikilink")[質粒體關係均很密切](../Page/質粒體.md "wikilink")，皆是來自[藍綠菌的](../Page/藍綠菌.md "wikilink")[內共生](../Page/內共生.md "wikilink")\[7\]，在某些情況下可互相轉換，如植物的[果實成熟時](../Page/果實.md "wikilink")，葉綠體和白色體可以轉換成有色體，造成果實的顏色。目前仍不清楚其作用機制，只知道葉綠體可能將其[類囊體破壞後](../Page/類囊體.md "wikilink")，在[基質中堆積葉黃素等色素的顆粒](../Page/基質.md "wikilink")。\[8\]

## 混淆

能在秋冬變紅的[樹葉](../Page/樹葉.md "wikilink")，其顏色來源是[葉綠素的分解](../Page/葉綠素.md "wikilink")，導致原本就存在於葉片中的[類胡蘿蔔素表現顏色](../Page/類胡蘿蔔素.md "wikilink")，使葉子的顏色從綠色變為橘紅色，因此其顏色來源和花果中的有色體無關。

## 相關條目

  - [白色體](../Page/白色體.md "wikilink")
  - [葉綠體](../Page/葉綠體.md "wikilink")

## 參考資料

[Category:質粒體](../Category/質粒體.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.