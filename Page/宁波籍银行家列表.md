自从近代概念的[银行和](../Page/银行.md "wikilink")[金融业在](../Page/金融业.md "wikilink")[中国出现后](../Page/中国.md "wikilink")，[宁波地区走出了很多](../Page/宁波.md "wikilink")[银行家](../Page/银行家.md "wikilink")。

## 历史背景

[清朝末年](../Page/清朝.md "wikilink")，以[严信厚为代表的宁波籍商人是](../Page/严信厚.md "wikilink")[南帮票号](../Page/南帮票号.md "wikilink")、[南帮汇票业的代表](../Page/南帮汇票业.md "wikilink")\[1\]\[2\]。清末民初，宁波籍商人垄断钱庄业，创立了过帐制度（又称“过帐码头”），并向近现代银行转型\[3\]\[4\]。民国中后期，中国银行业中有著名的“宁波系”，如[中国通商银行](../Page/中国通商银行.md "wikilink")、[四明银行等](../Page/四明银行.md "wikilink")\[5\]。民国时期，多位宁波籍金融人士在[中央银行](../Page/中央银行.md "wikilink")、国库局和各大银行中担任要职，如[宋汉章](../Page/宋汉章.md "wikilink")、[李思浩等](../Page/李思浩.md "wikilink")。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，不少宁波籍银行家又参与开拓了社会主义金融事业，如[谢寿天](../Page/谢寿天.md "wikilink")、[胡瑞荃](../Page/胡瑞荃.md "wikilink")、[林震峰等](../Page/林震峰.md "wikilink")\[6\]。不少宁波籍银行家为[台湾](../Page/台湾.md "wikilink")、[香港的经济发展作出了卓著贡献](../Page/香港.md "wikilink")，如[俞国华](../Page/俞国华.md "wikilink")、[孙义宣等均为](../Page/孙义宣.md "wikilink")[台湾经济腾飞的推手](../Page/台湾经济.md "wikilink")。并有数位宁波籍银行家闻名于国际金融界。

## 代表人物

### 国际

  - [黄益平](../Page/黄益平.md "wikilink")
  - [金立群](../Page/金立群.md "wikilink")
  - [王念祖 (联合国)](../Page/王念祖_\(联合国\).md "wikilink")

### 清末

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/宋汉章.md" title="wikilink">宋汉章</a></li>
<li><a href="../Page/陈笙郊.md" title="wikilink">陈笙郊</a></li>
<li><a href="../Page/谢纶辉.md" title="wikilink">谢纶辉</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/朱葆三.md" title="wikilink">朱葆三</a></li>
<li><a href="../Page/严信厚.md" title="wikilink">严信厚</a></li>
<li><a href="../Page/虞洽卿.md" title="wikilink">虞洽卿</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/傅筱庵.md" title="wikilink">傅筱庵</a></li>
<li><a href="../Page/叶星海.md" title="wikilink">叶星海</a></li>
<li><a href="../Page/王铭槐.md" title="wikilink">王铭槐</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/王槐山.md" title="wikilink">王槐山</a></li>
<li><a href="../Page/穆炳元.md" title="wikilink">穆炳元</a></li>
<li><a href="../Page/许春荣.md" title="wikilink">许春荣</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/李维庆.md" title="wikilink">李维庆</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 民国

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/赵家荪.md" title="wikilink">赵家荪</a></li>
<li><a href="../Page/宋汉章.md" title="wikilink">宋汉章</a></li>
<li><a href="../Page/盛竹书.md" title="wikilink">盛竹书</a></li>
<li><a href="../Page/胡孟嘉.md" title="wikilink">胡孟嘉</a></li>
<li><a href="../Page/叶琢堂.md" title="wikilink">叶琢堂</a></li>
<li><a href="../Page/孙衡甫.md" title="wikilink">孙衡甫</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/梁文臣.md" title="wikilink">梁文臣</a></li>
<li><a href="../Page/李思浩.md" title="wikilink">李思浩</a></li>
<li><a href="../Page/秦润卿.md" title="wikilink">秦润卿</a></li>
<li><a href="../Page/俞佐庭.md" title="wikilink">俞佐庭</a></li>
<li><a href="../Page/包玉刚.md" title="wikilink">包玉刚</a></li>
<li><a href="../Page/王伯元.md" title="wikilink">王伯元</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/虞洽卿.md" title="wikilink">虞洽卿</a></li>
<li><a href="../Page/童今吾.md" title="wikilink">童今吾</a></li>
<li><a href="../Page/黄振世.md" title="wikilink">黄振世</a></li>
<li><a href="../Page/周晋镳.md" title="wikilink">周晋镳</a></li>
<li><a href="../Page/林康侯.md" title="wikilink">林康侯</a></li>
<li><a href="../Page/袁禮敦.md" title="wikilink">袁禮敦</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/谢光甫.md" title="wikilink">谢光甫</a></li>
<li><a href="../Page/张迥伯.md" title="wikilink">张迥伯</a></li>
<li><a href="../Page/谢韬甫.md" title="wikilink">谢韬甫</a></li>
<li><a href="../Page/徐青甫.md" title="wikilink">徐青甫</a></li>
<li><a href="../Page/毛懋卿.md" title="wikilink">毛懋卿</a></li>
<li><a href="../Page/吳啟鼎.md" title="wikilink">吳啟鼎</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/徐圣禅.md" title="wikilink">徐圣禅</a></li>
<li><a href="../Page/孫鶴皋.md" title="wikilink">孫鶴皋</a></li>
<li><a href="../Page/徐继庄.md" title="wikilink">徐继庄</a></li>
<li><a href="../Page/徐世章.md" title="wikilink">徐世章</a></li>
<li><a href="../Page/叶叔眉.md" title="wikilink">叶叔眉</a></li>
<li><a href="../Page/胡咏骐.md" title="wikilink">胡咏骐</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/张章翔.md" title="wikilink">张章翔</a></li>
<li><a href="../Page/林鸿赍.md" title="wikilink">林鸿赍</a></li>
<li><a href="../Page/杨天受.md" title="wikilink">杨天受</a></li>
<li><a href="../Page/董汉槎.md" title="wikilink">董汉槎</a></li>
<li><a href="../Page/贺得霖.md" title="wikilink">贺得霖</a></li>
<li><a href="../Page/傅品圭.md" title="wikilink">傅品圭</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/朱志尧.md" title="wikilink">朱志尧</a></li>
<li><a href="../Page/徐懋棠.md" title="wikilink">徐懋棠</a></li>
<li><a href="../Page/周宗良.md" title="wikilink">周宗良</a></li>
<li><a href="../Page/竺芝珊.md" title="wikilink">竺芝珊</a></li>
<li><a href="../Page/李馥荪.md" title="wikilink">李馥荪</a></li>
<li><a href="../Page/徐庆云.md" title="wikilink">徐庆云</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/李祖恩.md" title="wikilink">李祖恩</a></li>
<li><a href="../Page/朱孔阳.md" title="wikilink">朱孔阳</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 大陆

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/童赠银.md" title="wikilink">童赠银</a></li>
<li><a href="../Page/庄晓天.md" title="wikilink">庄晓天</a></li>
<li><a href="../Page/谢旭人.md" title="wikilink">谢旭人</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/方诚国.md" title="wikilink">方诚国</a></li>
<li><a href="../Page/胡平西.md" title="wikilink">胡平西</a></li>
<li><a href="../Page/谢寿天.md" title="wikilink">谢寿天</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/王华庆.md" title="wikilink">王华庆</a></li>
<li><a href="../Page/陆华裕.md" title="wikilink">陆华裕</a></li>
<li><a href="../Page/楼福卿.md" title="wikilink">楼福卿</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/胡瑞荃.md" title="wikilink">胡瑞荃</a></li>
<li><a href="../Page/林震峰.md" title="wikilink">林震峰</a></li>
<li><a href="../Page/顾伟国.md" title="wikilink">顾伟国</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/沈日新.md" title="wikilink">沈日新</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 台湾

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/俞国华.md" title="wikilink">俞国华</a></li>
<li><a href="../Page/俞飞鹏.md" title="wikilink">俞飞鹏</a></li>
<li><a href="../Page/周宏藩.md" title="wikilink">周宏藩</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/周宏濤.md" title="wikilink">周宏濤</a></li>
<li><a href="../Page/陈师孟.md" title="wikilink">陈师孟</a></li>
<li><a href="../Page/孫義宣.md" title="wikilink">孫義宣</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/陈迟.md" title="wikilink">陈迟</a></li>
<li><a href="../Page/应昌期.md" title="wikilink">应昌期</a></li>
<li><a href="../Page/王惜寸.md" title="wikilink">王惜寸</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/蔡念祖.md" title="wikilink">蔡念祖</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 香港

  - [包玉刚](../Page/包玉刚.md "wikilink")
  - [王守业](../Page/王守业_\(银行家\).md "wikilink")
  - [徐大统](../Page/徐大统.md "wikilink")

## 参考资料

  - 《宁波钱庄与宁波帮》

## 相关条目

  - [宁波商帮](../Page/宁波商帮.md "wikilink")
  - [宁波籍工商人物列表](../Page/宁波籍工商人物列表.md "wikilink")

## 参考链接

[F宁波籍人物分类列表](../Category/宁波人.md "wikilink")
[银行家一览](../Category/宁波籍人物分类列表.md "wikilink")
[Category:中国银行家](../Category/中国银行家.md "wikilink")

1.
2.
3.
4.
5.
6.