[Ferrero_Rocher2.jpg](https://zh.wikipedia.org/wiki/File:Ferrero_Rocher2.jpg "fig:Ferrero_Rocher2.jpg")
[Ferrero_Rocher1.jpg](https://zh.wikipedia.org/wiki/File:Ferrero_Rocher1.jpg "fig:Ferrero_Rocher1.jpg")

**金莎巧克力**（）是[意大利](../Page/意大利.md "wikilink")[糖果廠](../Page/糖果.md "wikilink")[佛列羅的朱古力品牌](../Page/佛列羅.md "wikilink")。[巧克力內有一層](../Page/巧克力.md "wikilink")[薄脆](../Page/薄脆.md "wikilink")，外面蓋著[巧克力及](../Page/巧克力.md "wikilink")[榛果碎顆粒](../Page/榛果.md "wikilink")，而內餡裡則包著香濃的[榛果醬及一](../Page/榛果醬.md "wikilink")-{粒}-[榛果](../Page/榛果.md "wikilink")，每顆[巧克力均以金色](../Page/巧克力.md "wikilink")[錫箔紙包裝](../Page/錫箔紙.md "wikilink")、白色立體小貼紙貼在金色鋁箔紙上，再以咖啡色小紙杯裝載。

金莎在1980年代晚期在[香港售賣](../Page/香港.md "wikilink")，不但作為高級的零食，金莎更在聖誕節及農曆新年大量銷售禮盒裝，自此成為香港的送禮佳品，其後金莎在[台灣及](../Page/台灣.md "wikilink")[中國大陸也大受歡迎](../Page/中國大陸.md "wikilink")。早期的金莎都是意大利製造，但現在金莎已由費列羅授權加拿大、德國和波蘭製造。

## Ferrero種類

  - 金莎（Ferrero Rocher）
  - [朗莎](../Page/朗莎.md "wikilink")（朗多皮諾，Ferrero
    RondNoir）：被薄脆所包圍，以黑[朱古力忌廉包著一](../Page/朱古力.md "wikilink")-{粒}-黑朱古力珍珠（期後推出新配方為以[杏仁在中心](../Page/杏仁.md "wikilink")），外層灑上黑朱古力。
  - [雪莎](../Page/雪莎.md "wikilink")（Ferrero
    Raffaello）：被薄脆所包圍，以[牛奶忌廉包著一](../Page/牛奶.md "wikilink")-{粒}-杏仁，外層灑上椰絲。

## 侵權官司

1990年和1993年[费列罗公司分別在](../Page/费列罗.md "wikilink")[香港及](../Page/香港.md "wikilink")[台灣註冊](../Page/台灣.md "wikilink")「金莎」商標，但未在[中國大陸註冊](../Page/中國大陸.md "wikilink")。[張家港市一間乳品廠把](../Page/張家港市.md "wikilink")「金莎」註冊为商标，公司易名為[蒙特莎](../Page/蒙特莎.md "wikilink")，並推出價錢較便宜的「金莎[Tresordore](../Page/Tresordore.md "wikilink")」朱古力系列。

2003年，[费列罗在中國起訴蒙特莎侵權](../Page/费列罗.md "wikilink")，要求蒙特莎賠償70萬元[人民幣](../Page/人民幣.md "wikilink")，但被[天津市人民法院判敗訴](../Page/天津市人民法院.md "wikilink")，指[蒙特莎在中國的知名度較费列罗更大](../Page/蒙特莎.md "wikilink")；2006年1月，费列罗上訴得直，但[北京市高級人民法院介入](../Page/北京市高級人民法院.md "wikilink")，要求复審。

在數年的審訊中，雙方爭論誰最先在中國發售，[蒙特莎公司聲稱他們早於](../Page/蒙特莎.md "wikilink")1990年已引入[Tresordore朱古力](../Page/Tresordore.md "wikilink")，並委托本地公司設計及包裝，而费列罗公司則指他們的朱古力早在1984年已在中國發售。對於兩者近似的包裝，[蒙特莎的代表律師反駁說該包裝早在](../Page/蒙特莎.md "wikilink")1970年代已設計好，誰發明它仍很難說\[1\]。

該宗侵權官司引起[歐盟普遍關注](../Page/歐盟.md "wikilink")，2006年11月，英國駐[歐盟代表及](../Page/歐盟.md "wikilink")[歐盟貿易專員](../Page/歐盟貿易專員.md "wikilink")[彼得·曼德爾森](../Page/彼得·曼德爾森.md "wikilink")（Peter
Mandelson）曾向时任中國商務部長[薄熙來引用金莎官司](../Page/薄熙來.md "wikilink")，對中國的盜版及保護外資的法律表達關注\[2\]。

## 參考

<references />

## 外部連結

  - 金莎巧克力[生產商官方網頁](http://www.ferrero.com)

  -
[分類:巧克力糖](../Page/分類:巧克力糖.md "wikilink")

[Category:糖果](../Category/糖果.md "wikilink")
[Category:意大利食品](../Category/意大利食品.md "wikilink")
[Category:巧克力品牌](../Category/巧克力品牌.md "wikilink")
[Category:1982年面世的產品](../Category/1982年面世的產品.md "wikilink")

1.

2.