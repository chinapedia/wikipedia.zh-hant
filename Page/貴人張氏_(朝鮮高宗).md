**貴人張氏**，[朝鮮高宗的後宮](../Page/朝鮮高宗.md "wikilink")[嬪御](../Page/嬪御.md "wikilink")，[本贯德水](../Page/本贯.md "wikilink")。本是[明成皇后之](../Page/明成皇后.md "wikilink")[至密內人](../Page/至密內人.md "wikilink")，後調到高宗身邊為梳洗內人，並為高宗寵幸升為[承恩尚宮](../Page/承恩尚宮.md "wikilink")，後生下義和君[李堈](../Page/李堈.md "wikilink")（後封義親王），但她生前並未能成為正式嬪御。

## 生平

张氏生前的记录少之又少，这些记录都指出她因得罪[明成皇后而遭到迫害](../Page/明成皇后.md "wikilink")。[日本方面资料记载](../Page/日本.md "wikilink")，张氏本来是侍奉[翼宗之妻](../Page/朝鲜文祖.md "wikilink")[赵大妃的宫女](../Page/神贞王后.md "wikilink")，在[哲宗末年](../Page/朝鲜哲宗.md "wikilink")，王族[兴宣君时常出入赵大妃寝宫](../Page/兴宣大院君.md "wikilink")，其间曾与张氏及另一名宫女徐氏通奸。不久后兴宣君执掌朝政，成为[兴宣大院君](../Page/兴宣大院君.md "wikilink")，他只纳徐氏为妾，而拒纳张氏。张氏遂对此怀恨在心，后来她又联合[明成皇后打倒](../Page/明成皇后.md "wikilink")[大院君](../Page/兴宣大院君.md "wikilink")，一度成为了[明成皇后的红人](../Page/明成皇后.md "wikilink")。然而她在1877年得到高宗恩宠、生下王子[李堈以后](../Page/李堈.md "wikilink")，明成皇后大怒，並且把逐出宮用[匕首将她刺死](../Page/匕首.md "wikilink")。不过其兄[张甲福](../Page/张甲福.md "wikilink")（又名[张殷奎](../Page/张殷奎.md "wikilink")）后曾被[明成皇后派往日本刺杀](../Page/明成皇后.md "wikilink")[开化党领袖](../Page/开化党.md "wikilink")[金玉均](../Page/金玉均.md "wikilink")。\[1\]

[朝鲜史料](../Page/朝鲜.md "wikilink")《[梅泉野录](../Page/梅泉野录.md "wikilink")》有如下记载：

另外有说法是张氏在怀孕时就被逐出宫，[李堈十岁时](../Page/李堈.md "wikilink")，张氏在[南大门外的民宅中死去](../Page/崇礼门.md "wikilink")，并且传说是被明成皇后派人暗杀的。\[2\]在当时，有一本名为《张嫔录》的[谚文小说](../Page/谚文.md "wikilink")，在[朝鲜城乡广泛流传](../Page/朝鲜.md "wikilink")，即讲述张氏惨遭[明成皇后迫害](../Page/明成皇后.md "wikilink")，最后被逼死的故事。1895年2月5日，[日本驻朝公使](../Page/日本.md "wikilink")[井上馨向本国政府报告了这部书的情况](../Page/井上馨.md "wikilink")。\[3\]

她在1900年被追赠为[淑媛](../Page/淑媛.md "wikilink")，1906年追赠为[贵人](../Page/贵人.md "wikilink")。但她是高宗後宮中唯一沒有賜封[堂號的](../Page/堂號.md "wikilink")。（高宗的後宮中，連沒有生育子女的承恩尚宮都有冊封堂號，唯獨張氏沒有）

## 注释

<div class="references-small">

<references />

</div>

[Category:朝鮮王朝嬪御](../Category/朝鮮王朝嬪御.md "wikilink")
[\~](../Category/张姓.md "wikilink")

1.  《今世人物评传丛书》第一编，1896年，民友社，第131—132页、第136—137页。
2.  葛生能久：《东亚先觉志士记传》上，第520页。
3.  《驻韩日本公使馆记录》卷7，发第12号，〈〉。