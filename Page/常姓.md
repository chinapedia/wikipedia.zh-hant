**常姓**为[中文姓氏](../Page/中文姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》排名第80位。

## 起源

  - [周武王弟](../Page/周武王.md "wikilink")[卫康叔後裔](../Page/卫康叔.md "wikilink")：支庶一支[食邑于常](../Page/食邑.md "wikilink")（今[山东](../Page/山东.md "wikilink")[滕州市东南](../Page/滕州市.md "wikilink")），因以为氏。
  - 出自黄帝大臣：上古时代黄帝有大臣[常仪和大司空](../Page/常仪.md "wikilink")[常先](../Page/常先.md "wikilink")，常姓当此出，为河南常氏。
  - 出自吳國公族：春秋时吴王封其支庶于常（今[江苏省](../Page/江苏省.md "wikilink")[常州一带](../Page/常州.md "wikilink")），其后人以封邑名为氏，是为江苏常姓。
  - 出自楚國公族：[楚國](../Page/楚國.md "wikilink")[公族](../Page/公族.md "wikilink")[子常](../Page/囊瓦.md "wikilink")，子孫以常為姓。
  - 为避讳改姓而来：汉时本有恒姓者，因汉文帝名刘恒，以避名讳之故，恒姓改姓常。
  - 出自少数民族中有常姓：蒙、满、回等族均有常姓。

## 播遷

山东、河南、江苏为早期常姓发源地。汉时常在山东的一支已形成一大望族，有平原郡望；至三国时部分迁入四川；同时亦在河南形成望族。

唐时常姓迁至[福建](../Page/福建.md "wikilink")，宋时入粤。清代常姓有入[台](../Page/台灣.md "wikilink")，定居[新加坡等地者](../Page/新加坡.md "wikilink")。

今日常姓以河南、山东、黑龙江、吉林、河北等省居多，上述五省常姓约占全国汉族常姓人口的63%。常姓是当今中国姓氏排行第九十四位的大姓，人口较多，约占全国汉族人口的百分之零点一八。

## 歷史名人

  - [常林](../Page/常林.md "wikilink")，[曹魏名士](../Page/曹魏.md "wikilink")
  - [常璩](../Page/常璩.md "wikilink")，[东晋史学家](../Page/东晋.md "wikilink")，著[華陽國志](../Page/華陽國志.md "wikilink")。
  - [常建](../Page/常建.md "wikilink")，[唐代诗人](../Page/唐代.md "wikilink")。
  - [常衮](../Page/常衮.md "wikilink")，唐朝[宰相](../Page/宰相.md "wikilink")。
  - [常遇春](../Page/常遇春.md "wikilink")，[明朝開國大將](../Page/明朝.md "wikilink")。
  - [常蔭槐](../Page/常蔭槐.md "wikilink")，[北洋政府奉系軍閥官員](../Page/北洋政府.md "wikilink")。

## 現代名人

  - [常楓](../Page/常楓.md "wikilink")，[台灣資深](../Page/台灣.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [常昊](../Page/常昊.md "wikilink")，[中国著名的](../Page/中国.md "wikilink")[围棋高手](../Page/围棋.md "wikilink")。
  - [常玉](../Page/常玉.md "wikilink")，著名[畫家](../Page/畫家.md "wikilink")。
  - [常隆庆](../Page/常隆庆.md "wikilink")，著名[地质学家](../Page/地质学家.md "wikilink")。
  - [常萬全](../Page/常萬全.md "wikilink")，中國中央軍委委員、解放軍總裝備部部長。
  - [常振明](../Page/常振明.md "wikilink")，中信泰富主席兼董事總經理。
  - [常永祥](../Page/常永祥.md "wikilink")，中國舉重[運動員](../Page/運動員.md "wikilink")。
  - [常東昇](../Page/常東昇.md "wikilink")，中國[摔角大師](../Page/摔角.md "wikilink")。
  - [常寶華](../Page/常寶華.md "wikilink")，中國[相聲演員](../Page/相聲.md "wikilink")。
  - [常香玉](../Page/常香玉.md "wikilink")，中國[豫劇大師](../Page/豫劇.md "wikilink")。

## 虛構人物

  - 常威 (電影：九品芝麻官)

[C常](../Category/漢字姓氏.md "wikilink") [\*](../Category/常姓.md "wikilink")