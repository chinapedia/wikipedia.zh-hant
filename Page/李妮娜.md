**李妮娜**（），籍貫[遼寧](../Page/遼寧.md "wikilink")[本溪](../Page/本溪.md "wikilink")，[中國女子](../Page/中華人民共和國.md "wikilink")[自由式滑雪運動員](../Page/自由式滑雪.md "wikilink")。

## 經歷

李妮娜於8歲被成為[瀋陽體育學院競技巧體校練習的其中一員](../Page/瀋陽體育學院.md "wikilink")，在11歲時，李妮娜轉為練習[自由式滑雪的空中技巧賽](../Page/自由式滑雪.md "wikilink")。

她於2000年之時在全國錦標賽贏得[冠軍](../Page/冠軍.md "wikilink")、全國冠軍賽與世界杯中都獲得亞軍，而在世界錦標賽取得第16位。翌年，李妮娜在全國錦標賽得到殿軍，在世界杯中得季軍。於2002年，李妮娜再次贏得全國冠軍賽的冠軍，另外於[鹽湖城冬季奧運的女子自由式滑雪空中技巧小項名到第](../Page/2002年冬季奧林匹克運動會.md "wikilink")5，並於世界杯排名第5。

2003年，李妮娜再登上全國冠軍賽的冠軍寶座，又於[第10屆全國冬季運動會奪得](../Page/第10屆全國冬季運動會.md "wikilink")[金牌](../Page/金牌.md "wikilink")，並於多個分站的世界杯賽事獲得[亞軍](../Page/亞軍.md "wikilink")。次年，李妮娜在[加拿大以及](../Page/加拿大.md "wikilink")[美國分站世界杯取得亞軍](../Page/美國.md "wikilink")。而在2005年9月，李妮娜在[澳洲分站的世界杯賽事兩奪冠軍](../Page/澳洲.md "wikilink")。

2006年，李妮娜參與[都靈冬季奧運的女子自由式滑雪空中技巧小項](../Page/2006年冬季奧林匹克運動會.md "wikilink")，在初賽中以總分188.93，排名第3出線決賽，在決賽中，李妮娜表現保持水準，最終以197.39為中國摘下一面[銀牌](../Page/銀牌.md "wikilink")。

翌年又在[長春亞洲冬季運動會中奪得女子自由式小項的](../Page/2007年亞洲冬季運動會.md "wikilink")[金牌](../Page/金牌.md "wikilink")，隨後的兩個月，她於[意大利舉行的世界錦標賽以](../Page/意大利.md "wikilink")188·05分奪得冠軍。2009年的世界錦標賽中，李妮娜成功衛冕冠軍。翌年的[溫哥華冬季奧運](../Page/2010年冬季奧林匹克運動會.md "wikilink")，李妮娜不敵[澳洲對手](../Page/澳洲.md "wikilink")[拉斯拉](../Page/拉斯拉.md "wikilink")，再度於冬奧會中取得一面銀牌。

## 外部連結

  - [FIS-Ski.com](http://www.fis-ski.com/uk/604/613.html?sector=FS&listid=&competitorid=35144&type=result)
    Biography/Results

[Category:中国自由式滑雪运动员](../Category/中国自由式滑雪运动员.md "wikilink")
[Category:中国奥运自由式滑雪运动员](../Category/中国奥运自由式滑雪运动员.md "wikilink")
[Nina](../Category/李姓.md "wikilink")
[Category:中国奥林匹克运动会银牌得主](../Category/中国奥林匹克运动会银牌得主.md "wikilink")
[Category:本溪人](../Category/本溪人.md "wikilink")
[Category:2006年冬季奧林匹克運動會獎牌得主](../Category/2006年冬季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2010年冬季奧林匹克運動會獎牌得主](../Category/2010年冬季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2006年冬季奧林匹克運動會自由式滑雪運動員](../Category/2006年冬季奧林匹克運動會自由式滑雪運動員.md "wikilink")
[Category:2010年冬季奧林匹克運動會自由式滑雪運動員](../Category/2010年冬季奧林匹克運動會自由式滑雪運動員.md "wikilink")
[Category:2014年冬季奧林匹克運動會自由式滑雪運動員](../Category/2014年冬季奧林匹克運動會自由式滑雪運動員.md "wikilink")
[Category:奧林匹克運動會自由式滑雪獎牌得主](../Category/奧林匹克運動會自由式滑雪獎牌得主.md "wikilink")