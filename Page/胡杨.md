**胡杨**（[学名](../Page/学名.md "wikilink")：**），又称**胡桐**（《[汉书](../Page/汉书.md "wikilink")》）、**英雄树**、**异叶胡杨**、**异叶杨**、**水桐**、**三叶树**，是[杨柳科](../Page/杨柳科.md "wikilink")[杨属](../Page/杨属.md "wikilink")[胡杨亚属的一种植物](../Page/胡杨亚属.md "wikilink")，常生长在[沙漠中](../Page/沙漠.md "wikilink")，它耐寒、耐旱、耐盐碱、抗风沙，有很强的生命力。“胡杨生而千年不死，死而千年不倒，倒而千年不烂”。胡杨是生长在沙漠的唯一[乔木树种](../Page/乔木.md "wikilink")，且十分珍贵，可以和有“植物活化石”之称的[银杏树相媲美](../Page/银杏树.md "wikilink")。

## 形态

落叶[乔木](../Page/乔木.md "wikilink")，高达15米；叶子的形状多变异，披针形或线状披针形的叶子全缘或疏生锯齿，卵形、扁卵形、肾形的叶子具有缺刻或全缘，无毛，带有灰色或淡绿色。

## 分布

它曾经广泛分布于[中国西部的](../Page/中国.md "wikilink")[温带](../Page/温带.md "wikilink")[暖温带地区](../Page/暖温带.md "wikilink")，[新疆库车千佛洞](../Page/新疆.md "wikilink")、[甘肃](../Page/甘肃.md "wikilink")[敦煌铁匠沟](../Page/敦煌.md "wikilink")、[山西平隆等地](../Page/山西.md "wikilink")；如今，除了[柴达木盆地](../Page/柴达木盆地.md "wikilink")、[河西走廊](../Page/河西走廊.md "wikilink")、[内蒙古](../Page/内蒙古.md "wikilink")[阿拉善一些流入沙漠的河流两岸还可见到少量的胡杨外](../Page/阿拉善.md "wikilink")，全国胡杨林面积的90%以上都蜷缩于[新疆](../Page/新疆.md "wikilink")，而其中的90%又集中在新疆南部的[塔里木盆地](../Page/塔里木盆地.md "wikilink")。此外，在[阿富汗](../Page/阿富汗.md "wikilink")、[印度](../Page/印度.md "wikilink")、[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[塔吉克斯坦](../Page/塔吉克斯坦.md "wikilink")、[土库曼斯坦](../Page/土库曼斯坦.md "wikilink")、[乌兹别克斯坦等国也有分布](../Page/乌兹别克斯坦.md "wikilink")。世界上现仅存的三大原始胡杨林分别为：[中国新疆](../Page/中国.md "wikilink")[塔里木盆地胡杨林](../Page/塔里木盆地.md "wikilink")（面积约371万亩）；哈萨克斯坦胡杨林（面积约300万亩）；[内蒙古额济纳胡杨林](../Page/内蒙古.md "wikilink")（面积约45万亩）。

## 图集

## 参考文献

## 外部链接

  -
[Category:杨属](../Category/杨属.md "wikilink")