[Bundesarchiv_B_145_Bild-F031434-0006,_Aachen,_Technische_Hochschule,_Rechenzentrum.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_B_145_Bild-F031434-0006,_Aachen,_Technische_Hochschule,_Rechenzentrum.jpg "fig:Bundesarchiv_B_145_Bild-F031434-0006,_Aachen,_Technische_Hochschule,_Rechenzentrum.jpg")[亚琛的](../Page/亚琛.md "wikilink")[亞琛工業大學學生學習程序设计](../Page/亞琛工業大學.md "wikilink")\]\]
**程序员**（，或），它可以指在[程序设计某個專業領域中的專業人士](../Page/程序设计.md "wikilink")，或是从事[軟體撰寫](../Page/軟體.md "wikilink")，[程序开发](../Page/程序.md "wikilink")、维护的专业人员。但一般Coder特指進行編寫代碼的[編碼員](../Page/編碼員.md "wikilink")。

随着时代的发展，对程序员的要求也在变化，需要他们掌握更多的知识与技能，如：UML、单元测试、各种方法论等，以满足隨時代不断变化的商业、科技與應用等需求。

[英国著名诗人](../Page/英国.md "wikilink")[拜伦的女儿](../Page/拜伦.md "wikilink")[愛達·勒芙蕾絲曾设计了](../Page/愛達·勒芙蕾絲.md "wikilink")[巴贝奇分析机上計算](../Page/巴贝奇分析机.md "wikilink")[伯努利數的一个程序](../Page/伯努利數.md "wikilink")，她甚至还建立了[递归和](../Page/递归_\(计算机科学\).md "wikilink")[子程序的概念](../Page/子程序.md "wikilink")。由于愛達在[程序设计上的突破性創新](../Page/程序设计.md "wikilink")，她被称为世界上第一位程序员。

## 工作範圍

  - 系統分析與設計（Systems Analysis and Design）
  - 程式碼撰寫（Coding）
  - 測試與除錯（Testing and Debugging）
  - 撰寫技術文件（Write Specification）

## 相關職業

  - [系统管理员](../Page/系统管理员.md "wikilink")（SA）
  - [系统设计师](../Page/系统设计师.md "wikilink")（SD）
  - [数据库管理员](../Page/数据库管理员.md "wikilink")（DBA）
  - [应用分析师](../Page/应用分析师.md "wikilink")（AA）
  - [技术支持](../Page/技术支持.md "wikilink")（TS）

## 相關戲稱

  - 工程師
  - 程序猿（男性）/程序媛（女性）
  - 碼農（一般用來自嘲為像[農民高強度一樣的編碼工作者](../Page/農民.md "wikilink")）
  - 碼奴
  - 碼畜
  - [打字員](../Page/打字員.md "wikilink")

## 外部链接

  - [识别优秀程序员的关键要素](https://web.archive.org/web/20080115113751/http://www.inter-sections.net/2007/11/13/how-to-recognise-a-good-programmer/)

  - [程序员不為人知的事實](https://web.archive.org/web/20080115113751/http://www.inter-sections.net/2007/11/13/how-to-recognise-a-good-programmer/)

  - [香港程序员的平均薪金](https://www.payscale.com/research/HK/Job=Sr._Software_Engineer_%2F_Developer_%2F_Programmer/Salary)

  - [Banana Portal 接蕉喇 香港程序员
    freelance](https://www.bananaportal.com/%E5%88%86%E9%A1%9E/%E8%B3%87%E8%A8%8A%E7%A7%91%E6%8A%80%E6%88%96%E7%94%A2%E5%93%81%E9%96%8B%E7%99%BC/)

  - [Hellotoby 程序员
    freelance](https://www.hellotoby.com/en/explore/design-development)

[Category:计算机编程](../Category/计算机编程.md "wikilink")
[Category:職業](../Category/職業.md "wikilink")
[Category:程序员](../Category/程序员.md "wikilink")