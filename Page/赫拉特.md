[Section_of_Herat_in_2009.jpg](https://zh.wikipedia.org/wiki/File:Section_of_Herat_in_2009.jpg "fig:Section_of_Herat_in_2009.jpg")

**赫拉特**（），中亚古城，又称[哈烈](../Page/哈烈.md "wikilink")、黑鲁、海里、哈利，是[阿富汗西部](../Page/阿富汗.md "wikilink")[哈里河流域](../Page/哈里河.md "wikilink")[赫拉特省的一个城市](../Page/赫拉特省.md "wikilink")。位于赫里河中游右岸。人口约35万，居民以[波斯人](../Page/波斯人.md "wikilink")、[塔吉克人为主](../Page/塔吉克人.md "wikilink")。在11世纪—1220年之间，赫拉特发展成为中西亚的金属品制造业的中心，尤以镶金银的铜器闻名；是当时世界上最大的城市之一。现为赫拉特省首府。是中亚与西南亚、南亚交流的重要枢纽。

## 历史

赫拉特有二千多年的历史。其前身是建立于前500年的波斯古城“阿塔考纳”（Artacoana），因为古时赫拉特属于**阿利亚**省，因此常被称为“阿利亚”。

[Inside_Herat_International_Airport_in_February_2012.jpg](https://zh.wikipedia.org/wiki/File:Inside_Herat_International_Airport_in_February_2012.jpg "fig:Inside_Herat_International_Airport_in_February_2012.jpg")

前541年被[波斯帝国的](../Page/阿契美尼德王朝.md "wikilink")[居鲁士二世纳入其治下](../Page/居鲁士二世.md "wikilink")。

前329年在[马其顿帝国对](../Page/马其顿帝国.md "wikilink")[波斯帝国战争中](../Page/阿契美尼德王朝.md "wikilink")，[亚历山大大帝陷赫拉特城后建筑堡垒](../Page/亚历山大大帝.md "wikilink")。

前312年，该区成为[塞琉古帝国的一部分](../Page/塞琉古帝国.md "wikilink")。

前246年，该区成为[狄奥多特一世的](../Page/狄奥多特一世.md "wikilink")[巴克特里亚的一部分](../Page/大夏_\(中亚古国\).md "wikilink")。

前167年赫拉特为[帕提亚帝国的一部分](../Page/帕提亚帝国.md "wikilink")。

300年左右，赫拉特成为[萨珊王朝治下的](../Page/萨珊王朝.md "wikilink")[基督教的中心](../Page/基督教.md "wikilink")，设有一名[景教派主教](../Page/景教.md "wikilink")。

484年赫拉特为[嚈噠帝国的一部分](../Page/嚈噠.md "wikilink")。

644年之后成为[阿拉伯帝国的一部分](../Page/阿拉伯帝国.md "wikilink")。

786年－809年赫拉特是[阿拔斯王朝的一部分](../Page/阿拔斯王朝.md "wikilink")，后先后被[塔希爾王朝和](../Page/塔希爾王朝.md "wikilink")[萨法尔王朝统治](../Page/萨法尔王朝.md "wikilink")

867年－869年纳入[萨曼王朝统治](../Page/萨曼王朝.md "wikilink")。

1040年前从属于[伽色尼王国](../Page/伽色尼王国.md "wikilink")，1040年始被[塞尔柱帝国统治](../Page/塞尔柱帝国.md "wikilink")。

1175年被[阿富汗中部的](../Page/阿富汗.md "wikilink")[古尔王朝占领](../Page/古尔王朝.md "wikilink")，后归属[阿拉乌丁·摩诃末的](../Page/阿拉烏丁·摩訶末.md "wikilink")[花剌子模帝国](../Page/花剌子模.md "wikilink")。在这时期，赫拉特发展成为金属品制造业的中心，尤以镶金银的铜器闻名。

1221年，[成吉思汗四子](../Page/成吉思汗.md "wikilink")[拖雷攻破赫拉特](../Page/拖雷.md "wikilink")，拖雷下令大屠杀，杀一万余人。

1221年赫拉特叛变，蒙古将军宴只吉带在进行了6个月的围攻之后，于1222年6月14日再次攻占该城，赫拉特遭遇屠城浩劫，城中一百五十万人，在[蒙古人大屠杀之后](../Page/蒙古人.md "wikilink")，生还仅四十余人；这场屠杀整整占用了一周的时间。此后赫拉特的人口，再也不能恢复当年盛况，时至七百八十多年后的今天，人口仅仅35万。

1245年－1381年，此地成为[卡尔提德王朝的一部分](../Page/卡尔提德王朝.md "wikilink")。

1381年赫拉特又再度遭遇大屠杀浩劫。[帖木尔攻陷赫拉特后](../Page/帖木尔.md "wikilink")，采取了成吉思汗式的屠城方针。帖木儿令四子[沙哈鲁为赫拉特总督](../Page/沙哈鲁.md "wikilink")。

1405年帖木儿死，[沙哈鲁登上](../Page/沙哈鲁.md "wikilink")[帖木儿帝国国王宝座](../Page/帖木儿帝国.md "wikilink")，立赫拉特为国都。沙哈鲁王悉心建设赫拉特，终于将它建设成为一个媲美[撒马儿罕的美丽城市](../Page/撒马儿罕.md "wikilink")。

1413年沙哈鲁王遣使麼賚到中国朝贡\[1\]；同年[明太宗派遣中官李达](../Page/明太宗.md "wikilink")、吏部验封司员外郎[陈诚等](../Page/陈子鲁.md "wikilink")9人使团出使[帖木尔帝国](../Page/帖木尔帝国.md "wikilink")，访问[哈烈](../Page/哈烈.md "wikilink")\[2\]。

15世纪末叶，[高鶴紹德女王营建带双尖塔的穆塞拉回教寺院](../Page/高鶴紹德.md "wikilink")。高鶴紹德陵被认为是[帖木儿帝国的代表性建筑之一](../Page/帖木儿帝国.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

[Category:阿富汗城市](../Category/阿富汗城市.md "wikilink")
[Category:西域](../Category/西域.md "wikilink")
[Category:中外交通史地名](../Category/中外交通史地名.md "wikilink")
[Category:古都](../Category/古都.md "wikilink")
[Category:絲綢之路聚居地](../Category/絲綢之路聚居地.md "wikilink")

1.  明 羅月褧撰 《[咸賓錄](../Page/咸賓錄.md "wikilink")》西夷志卷之四﹕哈烈 中華書局 2000 ISBN
    7-101-02058-5
2.  [陈诚著](../Page/陈诚_\(明朝\).md "wikilink")
    《[西域番国志](../Page/西域番国志.md "wikilink")·[哈烈](../Page/哈烈.md "wikilink")》
    周连宽校注 2000 中华书局 ISBN 7-101-02058-5/K