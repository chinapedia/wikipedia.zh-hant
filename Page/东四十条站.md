**东四十条站**位于[北京市](../Page/北京市.md "wikilink")[东城区与](../Page/东城区.md "wikilink")[朝阳区的交界处](../Page/朝阳区_\(北京\).md "wikilink")，是[北京地铁](../Page/北京地铁.md "wikilink")[2号线的一个车站](../Page/北京地铁2号线.md "wikilink")，编号是213。

## 位置及利用状况

这个站位于[东四十条桥](../Page/东四十条桥.md "wikilink")，[東四十條](../Page/東四十條.md "wikilink")－[工人体育场北路](../Page/工人体育场北路.md "wikilink")（[平安大街](../Page/平安大街.md "wikilink")，东西向）与[朝阳门北大街](../Page/朝阳门北大街.md "wikilink")－[東直门南大街](../Page/東直门南大街.md "wikilink")（[东二环](../Page/北京二环路.md "wikilink")，南北向）交汇处，呈南北向。由于临近工人体育场，该站在[北京国安足球俱乐部主场比赛开始前和散场后常出现客流高峰](../Page/北京国安足球俱乐部.md "wikilink")。\[1\]\[2\]

## 车站结构

### 车站楼层

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面层</strong></p></td>
<td><p>出入口</p></td>
<td><p>A—D出入口</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>地下一层</strong></p></td>
<td><p>站厅</p></td>
<td><p>客务中心、自动售票机、进出站闸机</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地下二层</strong></p></td>
<td><p>北</p></td>
<td><p>列车往外环方向 <small>（）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a>，左边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>列车往内环方向 <small>（）</small></p></td>
<td><p>南 </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>地下三层</strong></p></td>
<td><p>西</p></td>
<td><p>废弃</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small>拟改为站厅</small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>废弃</p></td>
<td><p>东 </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地下四层<br />
（正在建设）</strong></p></td>
<td><p>西</p></td>
<td><p>预留列车往<a href="../Page/田村站_(北京市).md" title="wikilink">田村方向</a> <small>（）</small></p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a>，左边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>预留列车往方向 <small>（）</small></p></td>
<td><p>东 </p></td>
<td></td>
</tr>
</tbody>
</table>

### 站厅

2号线东四十条站在路口南北两侧各设一个站厅，建设年代较早，只通过楼梯与站台相连。

### 站台

东四十条站目前设有1个供2号线列车停靠的岛式站台，位于东二环路地底。站台中部设有预留下层站台的入口。

### 预留换乘

[Reserved_interchange_interface_at_L2_Dongsi_Shitiao_Station_(20170927181114).jpg](https://zh.wikipedia.org/wiki/File:Reserved_interchange_interface_at_L2_Dongsi_Shitiao_Station_\(20170927181114\).jpg "fig:Reserved_interchange_interface_at_L2_Dongsi_Shitiao_Station_(20170927181114).jpg")

东四十条站下层预留了远期规划[3号线的站台](../Page/北京地铁3号线.md "wikilink")，该站台在东四十条站建设时便已建成，但已长久没有使用。尽管不时有清洁工进入打扫，但站台上，尤其是站台两端，已经积了一层灰尘，墙壁上还有早期遗留的“消灭四害”等标语。在入口处仅有简单的可以移动的栏杆，且没有专人看守，因此有时也有好奇的旅客进入此站台。这座站台非常宽大，东四十条站也因此登上1980年代[北京十大建筑的榜单](../Page/北京十大建筑.md "wikilink")。\[3\]

该站台是按照6节车厢、B型车体（一说为C型车体）的标准设计的，但当2016年[3号线规划重启时](../Page/北京地铁3号线.md "wikilink")，规划地铁3号线以8节车厢、A型车体的标准建设，因而老站台不敷使用，设计者决定将原来的预留站台改为站厅，在地下另外修建站台。\[4\]

### 出入口

[C_exit_of_Dongsi_Shitiao_Station_(20150701181622).JPG](https://zh.wikipedia.org/wiki/File:C_exit_of_Dongsi_Shitiao_Station_\(20150701181622\).JPG "fig:C_exit_of_Dongsi_Shitiao_Station_(20150701181622).JPG")
东四十条站一共有4个出口：A西北、B東北、C東南、D西南，周边信息如下：

<table>
<thead>
<tr class="header">
<th><p>編號</p></th>
<th><p>建議前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>船检大厦、中汇广场</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>保利大厦、<a href="../Page/保利剧院.md" title="wikilink">保利剧院</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>港澳中心、首创大厦、亚洲大酒店、<a href="../Page/北京工人体育馆.md" title="wikilink">北京工人体育馆</a>、<a href="../Page/北京工人体育场.md" title="wikilink">北京工人体育场</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/新保利大厦.md" title="wikilink">新保利大厦</a>、北京军区总医院</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

该站是2号线首个安装升降平台的车站。\[5\]

### 车站艺术

由于临近[北京工人体育场和](../Page/北京工人体育场.md "wikilink")[北京工人体育馆](../Page/北京工人体育馆.md "wikilink")，本站的两幅壁画均以体育为主题。其中，外环方向站台的陶板壁画《华夏雄风》由严尚德等人创作，展现习武修行的三十余个人物形象；内环方向站台的陶瓷镶嵌壁画《走向世界》则由李化吉、权正环创作，描绘当代竞技运动。\[6\]

## 参考资料

[Category:北京市东城区地铁车站](../Category/北京市东城区地铁车站.md "wikilink")
[Category:1984年启用的铁路车站](../Category/1984年启用的铁路车站.md "wikilink")

1.
2.
3.
4.
5.
6.