**馮景禧**（）原籍[廣東](../Page/廣東.md "wikilink")，出生於[廣州市](../Page/廣州市.md "wikilink")。馮景禧是香港地產商、[證券商及](../Page/證券.md "wikilink")[銀行家](../Page/銀行.md "wikilink")。

## 生平

馮景禧1938年從[廣州南下到](../Page/廣州.md "wikilink")[香港](../Page/香港.md "wikilink")，以半工讀上學。
在1941年當過金银首饰店学徒工、商行的管事、财务等，其後經營小生意，成立「新禧公司」，包括買賣[大米](../Page/大米.md "wikilink")、經營[酒楼](../Page/酒楼.md "wikilink")、水路[運輸農產品](../Page/運輸.md "wikilink")。

1958年，馮景禧與[郭得勝和](../Page/郭得勝.md "wikilink")[李兆基等八人合資設立](../Page/李兆基.md "wikilink")「永業公司」，發展香港地產建築。1963年，三人再合作成立「新鴻基企業有限公司」，即是「[新鴻基地產公司](../Page/新鴻基地產.md "wikilink")」的前身。1965年，香港有华资银行挤提风潮，[文化大革命年代](../Page/文化大革命.md "wikilink")，香港出現移民潮。
但是，馮景禧對香港有信心，買入大量土地，和郭得勝、李兆基一起，在三年內建成逾20座大厦，其時經濟好轉，新鴻基獲巨利，馮景禧等三人成為「商界奇人」，又稱「三劍俠」。

1967年馮氏一家舉家[移民](../Page/移民.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[温哥華](../Page/温哥華.md "wikilink")，但馮很快就與其妻子-{回}-流[香港繼續其事業](../Page/香港.md "wikilink")，留下了孩子們在[温哥華完成其獨立的中學和大學的生涯](../Page/温哥華.md "wikilink")。馮氏一家皆擁有[加拿大國籍至今](../Page/加拿大.md "wikilink")。

1969年，馮景禧創辦[新鴻基證券及](../Page/新鴻基證券.md "wikilink")[新鴻基財務公司](../Page/新鴻基財務.md "wikilink")。

1978年成立新鴻基（中國）有限公司，並於1982年獲得[香港政府發出](../Page/香港政府.md "wikilink")[香港銀行牌照](../Page/香港銀行牌照.md "wikilink")，易名「新鴻基銀行」，擔任主席。1985年8月病逝，終年63歲。新鴻基銀行其後於1986年時被收購，現為[富邦銀行
(香港)](../Page/富邦銀行_\(香港\).md "wikilink")。

馮景禧的長子[馮永發是](../Page/馮永發.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[新時代集團（Fairchild
Group）創辦人](../Page/新時代集團_\(加拿大\).md "wikilink")、主席和行政總裁並擁有[新時代傳媒集團等業務](../Page/新時代傳媒集團.md "wikilink")，次子[馮永祥曾為](../Page/馮永祥.md "wikilink")[禹銘投資有限公司主席](../Page/禹銘投資有限公司.md "wikilink")，至2008年5月退任\[1\]。

## 參考資料

## 相關

  - [香港銀行公會](../Page/香港銀行公會.md "wikilink")
  - [香港銀行擠提事件列表](../Page/香港銀行擠提事件列表.md "wikilink")

## 外部連線

  - [馮景禧生平故事](http://www.ndcnc.gov.cn/datalib/2004/Character/DL/DL-20040311161836)

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港財經界人士](../Category/香港財經界人士.md "wikilink")
[Category:新鴻基地產](../Category/新鴻基地產.md "wikilink")
[Category:广州人](../Category/广州人.md "wikilink")
[J景](../Category/馮姓.md "wikilink")
[Category:加拿大房地产商](../Category/加拿大房地产商.md "wikilink")

1.  <http://hk.news.yahoo.com/article/080526/9/71z5.html>
    禹銘（00666）馮永祥退任主席及非執董