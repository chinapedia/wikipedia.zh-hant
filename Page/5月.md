**5月**是[公历年中的第五个月](../Page/公历.md "wikilink")，是[大月](../Page/大月.md "wikilink")，共有31天。

在[北半球](../Page/北半球.md "wikilink")，5月是[夏季的第一个月](../Page/夏季.md "wikilink")，这个月有两个[节气](../Page/节气.md "wikilink")：[立夏](../Page/立夏.md "wikilink")、[小满](../Page/小满.md "wikilink")。

[英文](../Page/英文.md "wikilink")5月名稱（May）可能源自[罗马神话豐收繁殖女神](../Page/罗马神话.md "wikilink")[迈亞](../Page/迈亚_\(罗马神话\).md "wikilink")，或来源于在5月庆祝节日的Bona
Dea女神，或来自拉丁语詞maiores(意为"较年长者")。

## 例行節日

  - **[5月1日](../Page/5月1日.md "wikilink")**：[劳动节](../Page/劳动节.md "wikilink")（又稱[五一国际劳动节](../Page/五一国际劳动节.md "wikilink")、[国际示威游行日](../Page/国际示威游行日.md "wikilink")），[英國將公眾假期設於](../Page/英國.md "wikilink")5月首個星期一
  - **[5月3日](../Page/5月3日.md "wikilink")**：[日本](../Page/日本.md "wikilink")[憲法紀念日公眾假期](../Page/憲法紀念日.md "wikilink")
  - **[5月4日](../Page/5月4日.md "wikilink")**：[中華人民共和國](../Page/中華人民共和國.md "wikilink")[五四青年節](../Page/五四青年節.md "wikilink")、[五四運動紀念日](../Page/五四運動紀念日.md "wikilink")、[科技傳播日](../Page/科技傳播日.md "wikilink")、[北京大學校慶日](../Page/北京大學.md "wikilink")。
  - **[5月5日](../Page/5月5日.md "wikilink")**：[日本](../Page/日本.md "wikilink")[兒童日](../Page/兒童節.md "wikilink")
  - **[5月7日](../Page/5月7日.md "wikilink")**：[美國國家自慰日](../Page/美國國家自慰日.md "wikilink")
  - **[5月8日](../Page/5月8日.md "wikilink")**：[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[歐戰勝利紀念日](../Page/歐戰勝利紀念日.md "wikilink")（V-E
    Day），[世界紅十字日](../Page/世界紅十字日.md "wikilink")
  - **[5月9日](../Page/5月9日.md "wikilink")**：大多數前[蘇聯和](../Page/蘇聯.md "wikilink")[東歐國家](../Page/東歐.md "wikilink")[衛國戰爭勝利日](../Page/衛國戰爭勝利日.md "wikilink")，[聯合國](../Page/聯合國.md "wikilink")[紀念與和解日](../Page/紀念與和解日.md "wikilink")
  - **[5月12日](../Page/5月12日.md "wikilink")**：[國際護士節](../Page/國際護士節.md "wikilink")
  - **[5月16日](../Page/5月16日.md "wikilink")**：[馬來西亞](../Page/馬來西亞.md "wikilink")[衛塞節](../Page/衛塞節.md "wikilink")、[國際牛奶節](../Page/國際牛奶節.md "wikilink")、[國際白飯節](../Page/國際白飯節.md "wikilink")
  - **[5月17日](../Page/5月17日.md "wikilink")**：[世界電信日](../Page/世界電信日.md "wikilink")、[国际不再恐同日](../Page/国际不再恐同日.md "wikilink")
  - **[5月18日](../Page/5月18日.md "wikilink")**：[國際博物館日](../Page/國際博物館日.md "wikilink")、[海峽兩岸經貿交易會](../Page/海峽兩岸經貿交易會.md "wikilink")
  - **[5月25日](../Page/5月25日.md "wikilink")**：[非洲解放日](../Page/非洲解放日.md "wikilink")
    、[阿根廷革命紀念日](../Page/阿根廷革命紀念日.md "wikilink")、[蘇丹](../Page/蘇丹.md "wikilink")[革命節](../Page/革命節.md "wikilink")、[世界預防中風日](../Page/世界預防中風日.md "wikilink")、
    [国际失踪儿童日](../Page/国际失踪儿童日.md "wikilink")
  - **[5月26日](../Page/5月26日.md "wikilink")**：[世界向人體條件挑戰日](../Page/世界向人體條件挑戰日.md "wikilink")
    、[國家道歉日](../Page/國家道歉日.md "wikilink")
  - **[5月29日](../Page/5月29日.md "wikilink")**：[聯合國維持和平人員國際日](../Page/聯合國維持和平人員國際日.md "wikilink")，於2002年12月11日57/129號決議通過
  - **[5月30日](../Page/5月30日.md "wikilink")**：[克羅地亞](../Page/克羅地亞.md "wikilink")[國慶日](../Page/克羅地亞國慶.md "wikilink")、[美國](../Page/美國.md "wikilink")[陣亡戰士的紀念日](../Page/陣亡戰士的紀念日.md "wikilink")、[法國](../Page/法國.md "wikilink")[聖女貞德紀念日](../Page/聖女貞德.md "wikilink")
  - **[5月31日](../Page/5月31日.md "wikilink")**：[世界無煙日](../Page/世界無煙日.md "wikilink")（又稱
    [世界不吸煙日](../Page/世界不吸煙日.md "wikilink")、[世界禁菸日](../Page/世界禁菸日.md "wikilink")）

## 其他例行節日

  - 5月的第二個星期日：[母親節](../Page/母親節.md "wikilink")（[美國等大多數](../Page/美國.md "wikilink")[國家](../Page/國家.md "wikilink")）
  - 5月的第三個星期日：[中華人民共和國](../Page/中華人民共和國.md "wikilink")[全國助殘日](../Page/全國助殘日.md "wikilink")，根據[1991年](../Page/1991年.md "wikilink")5月開始實施的《[殘疾人保障法](../Page/中華人民共和國殘疾人保障法.md "wikilink")》規定
  - 5月的最後的星期一：[英國春天](../Page/英國.md "wikilink")[銀行假期](../Page/銀行假期.md "wikilink")，是為公眾假期
  - 5月开始于一个星期中独自不同的一天。例如5月1日为周日，当年其他月份的1日都不为周日。[6月](../Page/6月.md "wikilink")，[8月有相同的属性](../Page/8月.md "wikilink")。

## 其他5月象徵

  - [星座](../Page/星座.md "wikilink")：[金牛宮](../Page/金牛宮.md "wikilink")、[雙子宮](../Page/雙子宮.md "wikilink")
  - [生日石](../Page/生日石.md "wikilink")：[祖母綠](../Page/祖母綠.md "wikilink")

## 參見

  - [5月35日](../Page/5月35日.md "wikilink")

[月05](../Category/月份.md "wikilink") [月05](../Category/日历.md "wikilink")
[\*](../Category/5月.md "wikilink")