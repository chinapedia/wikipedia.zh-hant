《**文化苦旅**》是一本散文集，作者[余秋雨敘述在](../Page/余秋雨.md "wikilink")[中國各個名勝古蹟旅遊時的記錄](../Page/中國.md "wikilink")\[1\]，話題探究中國文化，以及紓發自我的情感。

## 出版

本書部分文章曾於中國大陸的《收穫》雜誌全年專欄式連載，並陸續為海外報刊轉載，在兩岸三地華人間引發巨大迴響。余氏其他深度考察中國文化遺跡的作品，另包含《山居筆記》。

本書原文為簡體字，1992年由上海知識出版社出版，橫排，85049x11687毫米開本，32045678頁。

1992年11月20日，[爾雅出版社獲作者授權在臺灣出版此書](../Page/爾雅出版社.md "wikilink")，514頁。

本書於臺灣出版後，曾榮獲1992年聯合報「讀書人」最佳書獎；金石堂1992年年度最具影響力的書；誠品書店1993年1月「誠品選書」。並有部分篇章被選入臺灣高中國文教材。

2008年，余氏精選了《文化苦旅》與《山居筆記》中的著名篇章，加以改寫後，集結出版，名為《新文化苦旅：余秋雨文化散文全集》，由爾雅出版社出版發行。其中除了前述的精選篇章外，亦加入了數篇新的文章，余氏在此書總序中寫道：「從此，我的全部文化散文著作，均以這套書的文字和標題為準。」

## 作者與內容

余秋雨曾經是[上海戲劇學院院長](../Page/上海戲劇學院.md "wikilink")，也是[白先勇先生最推崇的中國當代學人](../Page/白先勇.md "wikilink")。

出版社對此書描述為：

此書中有提到的景點有很多，每一個景點都帶給余秋雨不同的感觸和震撼。觸碰到中國幾千年的文化，見證自己國家一路走來深刻的歷史痕跡。

## 書中包含的景點

  - 道士塔
  - [莫高窟](../Page/莫高窟.md "wikilink")
  - [陽關雪](../Page/陽關.md "wikilink")
  - [沙原隱泉](../Page/月牙泉.md "wikilink")
  - [柳侯祠](../Page/柳侯祠.md "wikilink")
  - [白蓮洞](../Page/白莲洞遗址.md "wikilink")
  - [都江堰](../Page/都江堰.md "wikilink")
  - [長江三峽](../Page/長江三峽.md "wikilink")
  - [洞庭一角](../Page/洞庭湖.md "wikilink")
  - [廬山](../Page/廬山.md "wikilink")
  - [貴池](../Page/貴池.md "wikilink")[儺戲](../Page/儺戲.md "wikilink")
  - 白髮[蘇州](../Page/蘇州.md "wikilink")
  - [江南小鎮](../Page/江南.md "wikilink")
  - 风雨[天一阁](../Page/天一阁.md "wikilink")
  - 五城記（[開封](../Page/开封市.md "wikilink")、[南京](../Page/南京市.md "wikilink")、[成都](../Page/成都市.md "wikilink")、[蘭州](../Page/兰州市.md "wikilink")、[廣州](../Page/广州市.md "wikilink")）

## 書籍資料

  -
## 參考資料

## 外部链接

  - [余秋雨現象](https://web.archive.org/web/20080412065847/http://staff.whsh.tc.edu.tw/~huanyin/mofa/y/mofa_yuchiouyu_criticized.htm)

[文](../Category/余秋雨.md "wikilink")
[Category:中国散文](../Category/中国散文.md "wikilink")
[Category:1992年书籍](../Category/1992年书籍.md "wikilink")
[Category:中华人民共和国文集](../Category/中华人民共和国文集.md "wikilink")

1.  [余秋雨新版《文化苦旅》](http://www.apdnews.com/news/76725.html)
    ，亚太日报，2014年3月18日