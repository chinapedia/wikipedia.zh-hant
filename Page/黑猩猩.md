**黑猩猩**（[学名](../Page/学名.md "wikilink")：**）是[黑猩猩属下的两个物种之一](../Page/黑猩猩属.md "wikilink")，另一种是[倭黑猩猩](../Page/倭黑猩猩.md "wikilink")（**）。根据[黑猩猩基因组计划的研究结果](../Page/黑猩猩基因组计划.md "wikilink")，黑猩猩和倭黑猩猩与[人类具有较高的](../Page/人类.md "wikilink")[基因相似度](../Page/基因.md "wikilink")，与人类有最近的[共同祖先](../Page/共同祖先.md "wikilink")，從[演化的角度看是現存生物中與人类最近的](../Page/演化.md "wikilink")[姊妹種](../Page/姊妹群.md "wikilink")，黑猩猩跟人類基因組的相似度高達98.8%\[1\]，大約在600萬年前「分家」。

黑猩猩的生活范围在[非洲西部及](../Page/非洲西部.md "wikilink")[中部](../Page/非洲中部.md "wikilink")，屬於[哺乳綱](../Page/哺乳綱.md "wikilink")[靈長目](../Page/靈長目.md "wikilink")，平日成群地生活，每個群體就像一個[部落](../Page/部落.md "wikilink")。

英文名Chimpanzee首次使用於1738年，載於《The London Magazine》，義為「mockman」\[2\]

## 特徵

黑猩猩體多毛，四肢修長且皆可握物，他們能以半直立的方式行走。黑猩猩經常會和[大猩猩搞混](../Page/大猩猩.md "wikilink")，但他們兩是不同的，黑猩猩的體型比大猩猩小；而且在遺傳上，黑猩猩與人類較為接近，與大猩猩較為不相似。

黑猩猩的[ABO血型以A为主](../Page/ABO血型.md "wikilink")，有少量O型，但没有B型。M血型和N血型也有发现。黑猩猩有48條[染色體](../Page/染色體.md "wikilink")（24對）。黑猩猩[细胞色素c上的](../Page/细胞色素c.md "wikilink")[氨基酸顺序与人类的相同](../Page/氨基酸.md "wikilink")。母猩猩[懷孕期達兩百三十天](../Page/懷孕.md "wikilink")，一胎生下一子。

## 成長

母黑猩猩懷孕期為八個月。幼嬰會在大約三歲時斷乳，但仍會與母親親密生活數年。[青春期為八到十歲](../Page/青春期.md "wikilink")，由人類圈養的黑猩猩，壽命可長達五十歲。

## 習性

黑猩猩的智商較高，已知他們能使用一些簡單的工具來進行一些工作，如捉[白蟻](../Page/白蟻.md "wikilink")。生物学家多次观察到雌黑猩猩可以制作工具来捕猎丛猴。。。。

[珍·古德在她的研究報告對於野外黑猩猩的生活習性多有描述](../Page/珍·古德.md "wikilink")。

## 食性

有一段長時間，人們都以為黑猩猩是[素食動物](../Page/素食.md "wikilink")，其實黑猩猩是[雜食性](../Page/雜食性.md "wikilink")，牠們會利用不同的方法來取不同的食物，黑猩猩會利用舔滿口水的細枝來粘[螞蟻](../Page/螞蟻.md "wikilink")，並利用兩塊石器放置、敲開果實。黑猩猩有時會捕食一些猴類（如紅疣猴、黑白疣猴）黑猩猩在捕食猴類時會策劃戰術，由於黑猩猩無法在樹上捕捉靈敏的[疣猴](../Page/疣猴.md "wikilink")，因此有一隻黑猩猩會先從陸地上超過樹上的疣猴群，而其它黑猩猩則會從樹上將牠們聚集並驅趕到埋伏地點，當陸上的黑猩猩到達埋伏地點時會在樹下等候，此時其它的黑猩猩會堵住疣猴群的路只留下一條有埋伏的通道，當疣猴進入這條路時，埋伏的黑猩猩會把牠趕到地上獵殺。

## 基因

[美國](../Page/美國.md "wikilink")[佐治亞州立大學教授霍普金斯](../Page/佐治亞州立大學.md "wikilink")（William
Hopkins）等人2014年7月在《當代生物學》期刊發表報告，稱他們對年齡介於9歲到54歲之間的99隻黑猩猩進行了測試。每隻黑猩猩都要參與13項認知測試，包括使用工具和空間記憶等。結果顯示，這些黑猩猩當中，有超過一半的認知能力受[遺傳基因顯著影響](../Page/遺傳基因.md "wikilink")。\[3\]

## 人類演化與黑猩猩的關係

[人類演化並不只是研究](../Page/人類演化.md "wikilink")[人屬生物](../Page/人屬.md "wikilink")，有時還研究早期的[人科生物](../Page/人科.md "wikilink")，甚至會研究[人類和黑猩猩的](../Page/人類.md "wikilink")[共同祖先是誰](../Page/共同祖先.md "wikilink")，並且會研究何時分道揚鑣。

### 何時分道揚鑣

科學家普遍認為人類和黑猩猩大約在600-700萬年前，分道揚鏣的時間為[中新世](../Page/中新世.md "wikilink")。而分子鐘計算出是在500萬年前分裂兩族，但普遍科學家並不認同，他們認同是上者。亦有些研究指是400萬年前才分家。

### 最近共同祖先

人類和黑猩猩的[最近共同祖先可能是](../Page/最近共同祖先.md "wikilink")[乍得沙赫人](../Page/乍得沙赫人.md "wikilink")。

## 其他

最近有些研究顯示黑猩猩能用[肢體語言](../Page/肢體語言.md "wikilink")，然而這點仍存有很大的爭議，目前科學界普遍的看法是他們雖然沒有[語言能力](../Page/語言能力.md "wikilink")，但能發出眾多不同的聲音、臉部表情與肢體動作，作為表達情緒或彼此溝通的信號。

## 參考文獻

## 外部連結

[kg:Kimpenzi](../Page/kg:Kimpenzi.md "wikilink")

[Category:黑猩猩屬](../Category/黑猩猩屬.md "wikilink")
[Category:瀕危物種](../Category/瀕危物種.md "wikilink")
[Category:非洲動物](../Category/非洲動物.md "wikilink")

1.  <http://www.amnh.org/exhibitions/permanent-exhibitions/human-origins-and-cultural-halls/anne-and-bernard-spitzer-hall-of-human-origins/understanding-our-past/dna-comparing-humans-and-chimps/>
2.  The London Magazine 465, September 1738. "A most surprising creature
    is brought over in the Speaker, just arrived from Carolina, that was
    taken in a wood at Guinea. She is the Female of the Creature which
    the Angolans call chimpanze, or the mockman." (cited after
    [OED](../Page/OED.md "wikilink"))
3.