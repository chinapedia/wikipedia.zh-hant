**Nvu**（發音N-view）是個[所見即所得的](../Page/所見即所得.md "wikilink")[萬維網網頁處理系統](../Page/萬維網.md "wikilink")，它是[自由軟體](../Page/自由軟體.md "wikilink")，建基於[Mozilla的Composer](../Page/Mozilla.md "wikilink")
Mode。它的目標是能與商業的網站開發工具如[Microsoft
FrontPage和](../Page/Microsoft_FrontPage.md "wikilink")[Macromedia
Dreamweaver爭一日之長短](../Page/Macromedia_Dreamweaver.md "wikilink")，及成為[Linux上最重要的所見即所得編輯器](../Page/Linux.md "wikilink")。Nvu的設計是極為適合非專業的電腦使用者。並不強調需要[HTML或](../Page/HTML.md "wikilink")[CSS的知識](../Page/CSS.md "wikilink")。

Nvu的作者為[Daniel
Glazman](../Page/Daniel_Glazman.md "wikilink")，這個計劃由[Linspire](../Page/Linspire.md "wikilink")（以前稱作Lindows）贊助。

Nvu有[Linux](../Page/Linux.md "wikilink")、[Mac OS
X和](../Page/Mac_OS_X.md "wikilink")[Microsoft
Windows的版本](../Page/Microsoft_Windows.md "wikilink")。原本的Nvu計畫是在開發完成之後，以此替代舊的Mozilla
Suite中的[Composer程式](../Page/Composer.md "wikilink")。而既然Mozilla
Suite已經停止，在發佈最後一個版本之後，在2006年9月16日作者宣佈停止了Nvu的官方開發工作，也就是說也許不再會有新版的官方Nvu版本可供更新。作者稱他正替另一個Mozilla基金會的專案效力，稱作「Composer」（或稱Mozilla
Composer 2.0），也就是說它將會是Mozilla
Suite的後繼者[SeaMonkey的HTML編輯器](../Page/SeaMonkey.md "wikilink")。由於Nvu在身後還仍然存有一些臭蟲，有人將這些[程序錯誤修正以後](../Page/程序錯誤.md "wikilink")，將軟體以「[KompoZer](../Page/KompoZer.md "wikilink")」的名字發佈。

根據原作者的說法，Nvu計畫將出現後續版本，並改名為[BlueGriffon](../Page/BlueGriffon.md "wikilink")\[1\]，據說Griffon可能起源於希臘神話中的[獅鷲獸](../Page/獅鷲.md "wikilink")（Griffin）\[2\]。

## 版本歷史

  - 1.0发布于2005年6月28日
  - 0.90發佈於2005年3月10日
  - 0.81發佈於2005年2月10日
  - 0.80發佈於2005年2月1日
  - 0.70發佈於2005年1月6日
  - 0.60發佈於2004年12月2日
  - 0.50發佈於2004年10月6日
  - 0.41發佈於2004年8月11日
  - 0.4發佈於2004年8月10日
  - 0.3發佈於2004年6月11日
  - 0.20發佈於2004年3月25日
  - 0.1發佈於2004年2月4日

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [Nvu官網](http://www.nvu.com)
  - [BlueGriffon官網](http://www.bluegriffon.org/)
  - [NvuDev](http://www.nvudev.org/)
  - [Nvu
    Portable](http://portableapps.com/apps/development/nvu_portable)
  - [Nvu 非官方正體中文版](http://www.moztw.org/nvu/)
  - [KompoZer
    非官方除蟲版](https://web.archive.org/web/20100410131549/http://kompozer.net/)
  - [NVU
    非官方简体中文语言包](https://web.archive.org/web/20060310135458/http://blogs.mozine.org/firefox/index.php?cat=7)

[Category:Mozilla](../Category/Mozilla.md "wikilink")
[Category:網站開發工具](../Category/網站開發工具.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")

1.  [1](http://www.glazman.org/weblog/dotclear/index.php?post/2008/09/29/BlueGriffon)
2.  [2](http://dasu88.twbbs.org/?p=199)