**董万瑞**（），[山西省](../Page/山西省.md "wikilink")[翼城县中卫乡董家坡人](../Page/翼城县.md "wikilink")，[中国人民解放军中将](../Page/中国人民解放军中将.md "wikilink")\[1\]。

## 生平

早年参加[中国人民解放军](../Page/中国人民解放军.md "wikilink")。曾任步兵某师师长。1993年任[陆军第三十一集团军军长](../Page/中国人民解放军陆军第三十一集团军.md "wikilink")。1996年任[南京军区副司令员](../Page/南京军区.md "wikilink")，并兼任南京军区国防动员委员会常务副主任、南京军区党委常委。1997年晋升中将军衔\[2\]\[3\]。他是[第十届全国人大代表](../Page/第十届全国人大代表.md "wikilink")，任内曾提出《[中华人民共和国国防动员法](../Page/中华人民共和国国防动员法.md "wikilink")》立法提案\[4\]。

1998年8月至9月，在[九八抗洪中](../Page/九八抗洪.md "wikilink")，董万瑞奉南京军区命令，担任南京军区九江抗洪前线总指挥，是取得九江抗洪胜利的主要功臣之一。董万瑞的儿子董三榕时任陆军第三十一集团军某部少尉排长，当时与父亲在同一个大堤上抗洪抢险，被传为佳话\[5\]\[6\]\[7\]。

董万瑞还是军事题材电视剧《[DA师](../Page/DA师.md "wikilink")》的总顾问。

2017年2月9日，董万瑞在[南京病逝](../Page/南京.md "wikilink")，享年75岁\[8\]\[9\]\[10\]。

## 参考文献

{{-}}

[Category:中国人民解放军中将](../Category/中国人民解放军中将.md "wikilink")
[Category:第十届全国人大代表](../Category/第十届全国人大代表.md "wikilink")
[Category:南京军区副司令员](../Category/南京军区副司令员.md "wikilink")
[Category:中国人民解放军陆军第三十一集团军军长](../Category/中国人民解放军陆军第三十一集团军军长.md "wikilink")
[Category:翼城人](../Category/翼城人.md "wikilink")
[W](../Category/董姓.md "wikilink")

1.
2.

3.

4.
5.
6.

7.
8.

9.

10.