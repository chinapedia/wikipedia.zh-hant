**芭芭拉·雷丁·摩根**（Barbara Radding
Morgan，），[美国小学](../Page/美国.md "wikilink")[教师](../Page/教师.md "wikilink")，第一个成功进入太空的[NASA](../Page/NASA.md "wikilink")“太空教学计划”的[宇航员](../Page/宇航员.md "wikilink")。她于2007年8月8日乘坐[奋进号航天飞机进入太空](../Page/奋进号航天飞机.md "wikilink")。摩根原是在1986年[挑战者号航天飞机灾难中丧生的教师](../Page/挑战者号航天飞机灾难.md "wikilink")[克丽斯塔·麦考利夫的替补](../Page/克丽斯塔·麦考利夫.md "wikilink")。

摩根出生于[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")，1974年在[蒙大拿州的一个](../Page/蒙大拿州.md "wikilink")[印第安人](../Page/印第安人.md "wikilink")[保留地中开始教学生涯](../Page/印第安人保留地.md "wikilink")。1986年挑战者号失事后，“太空教学计划”被迫停止，直到1998年，摩根才再次被NASA征召，成为全职宇航员。

## 外部链接

  - [Godspeed Barbara Morgan; Plans for Large Numbers of Teachers in
    Space](http://www.space-frontier.org/PressReleases/2007/20070808barbaramorgan.html)
  - [Barbara Morgan-Astronaut, Teacher in Space, NEA
    Member](https://web.archive.org/web/20070928032141/http://www.nea.org/people/0708morgan.html)
  - [CNN video clip featuring Barbara
    Morgan](http://www.cnn.com/2007/TECH/space/08/08/teacherspacereunion/index.html#cnnSTCVideo)
  - [After 22-year wait, teacher ready for space
    trip](http://www.cnn.com/2007/TECH/space/08/07/morgan.profile.ap/index.html)

[M](../Category/美国宇航员.md "wikilink") [M](../Category/美国教师.md "wikilink")
[M](../Category/加州人.md "wikilink")