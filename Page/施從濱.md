**施從濱**（），[字](../Page/表字.md "wikilink")**好善**，[號](../Page/號.md "wikilink")**漢亭**，是一位出生於[安徽桐城](../Page/安徽.md "wikilink")[孔城的](../Page/孔城鎮.md "wikilink")[將領](../Page/將軍_\(軍銜\).md "wikilink")。\[1\]
[施從濱.jpg](https://zh.wikipedia.org/wiki/File:施從濱.jpg "fig:施從濱.jpg")

## 生平

### 早年生活

施從濱有三個[弟弟](../Page/弟弟.md "wikilink")；他最小的弟弟[施從雲是](../Page/施從雲.md "wikilink")[灤州兵變的發起者之一](../Page/灤州兵變.md "wikilink")。他在[年幼時即進入](../Page/兒童.md "wikilink")[私塾就讀](../Page/私塾.md "wikilink")，並在1882年前後加入淮軍將領[吳長慶所率領的](../Page/吳長慶.md "wikilink")[部隊](../Page/軍隊.md "wikilink")；後來，他又進入[北洋新軍](../Page/新建陆军.md "wikilink")。

### 軍事生涯

[光緒三十三年](../Page/光緒.md "wikilink")（1907年），他進入[講武堂學習](../Page/讲武堂.md "wikilink")。[宣統元年](../Page/宣統.md "wikilink")（1909年），他被授與[陸軍](../Page/新軍.md "wikilink")[少將加](../Page/少將.md "wikilink")[中將銜](../Page/中將.md "wikilink")。

[民國三年](../Page/民國.md "wikilink")（1914年），他出任[鎮江](../Page/鎮江.md "wikilink")[鎮守使及第一混成旅](../Page/镇守使.md "wikilink")[旅長](../Page/旅長.md "wikilink")。民國五年（1916年），他成為[張宗昌部下並出任陸軍第一師](../Page/張宗昌.md "wikilink")[師長](../Page/師長.md "wikilink")，後歸[張作霖節制並駐紮於](../Page/张作霖.md "wikilink")[濟寧](../Page/济宁.md "wikilink")。民國十二年（1923年），他成為張宗昌屬下第二軍[軍長](../Page/軍長.md "wikilink")。

### 慘遭殺害

1925年10月，[奉浙戰爭爆發](../Page/奉直战争.md "wikilink")；[張作霖任命](../Page/张作霖.md "wikilink")[張宗昌為江蘇善後督辦](../Page/張宗昌.md "wikilink")、施從濱為安徽善後督辦，以南下迎擊[孫傳芳部隊](../Page/孙传芳.md "wikilink")。隨後，張宗昌任命施從濱為前敵總指揮，並自山東[兗州](../Page/兖州.md "wikilink")、[泰安開拔南進](../Page/泰安.md "wikilink")。

在[蚌埠地區作戰中](../Page/蚌埠.md "wikilink")，施從濱乘坐[鐵甲車督陣](../Page/裝甲車.md "wikilink")；但是，他的部隊隨即在[固鎮戰鬥遭擊敗](../Page/固鎮戰鬥.md "wikilink")，他也被孫傳芳部下[謝鴻勛](../Page/謝鴻勛.md "wikilink")[俘虜](../Page/战俘.md "wikilink")。

1926年初，孫傳芳命[李寶璋在](../Page/李寶璋.md "wikilink")[蚌埠站南側將施從濱](../Page/蚌埠站.md "wikilink")[斬首並暴屍三日](../Page/斬首.md "wikilink")。

## 家庭生活

他與[妻子董氏育有](../Page/妻子.md "wikilink")[兒女數人](../Page/子女.md "wikilink")；其中，[施谷蘭在](../Page/施劍翹.md "wikilink")1935年暗殺孫傳芳以為他報仇。\[2\]

## 参考文献

[Category:陆军第五镇将领](../Category/陆军第五镇将领.md "wikilink")
[Category:镇江镇守使](../Category/镇江镇守使.md "wikilink")
[Category:东北军将领](../Category/东北军将领.md "wikilink")
[Category:被處決的中華民國人](../Category/被處決的中華民國人.md "wikilink")
[Category:北洋将军府冠字将军](../Category/北洋将军府冠字将军.md "wikilink")
[Category:桐城人](../Category/桐城人.md "wikilink")
[C從](../Category/施姓.md "wikilink")

1.

2.