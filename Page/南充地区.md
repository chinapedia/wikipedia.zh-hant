**南充地区**，[中华人民共和国旧](../Page/中华人民共和国.md "wikilink")[地区名](../Page/地区_\(中国行政区划\).md "wikilink")，即今地级[南充市的前身](../Page/南充市.md "wikilink")，位于[四川省中北部](../Page/四川省.md "wikilink")，地区行政公署驻南充市。管辖区域大抵于[清朝](../Page/清朝.md "wikilink")[顺庆府与](../Page/顺庆府.md "wikilink")[中华民国](../Page/中华民国.md "wikilink")[四川省第十一行政督察区](../Page/四川省第十一行政督察区.md "wikilink")，中华人民共和国成立后，为[川北行署区和四川省之南充专区](../Page/川北行署区.md "wikilink")。

## 沿革

  - 1950撤消四川省，建立[川东](../Page/川东行署区.md "wikilink")、[川南](../Page/川南行署区.md "wikilink")、[川西](../Page/川西行署区.md "wikilink")、[川北](../Page/川北行署区.md "wikilink")4个省级行署区。川北行署区辖[南充市和](../Page/南充市_\(县级\).md "wikilink")[南充专区](../Page/南充专区.md "wikilink")、[遂宁专区](../Page/遂宁专区.md "wikilink")、[剑阁专区](../Page/剑阁专区.md "wikilink")、[达县专区](../Page/达县专区.md "wikilink")。南充专区辖南充市（行署和专区驻地）、[南充县](../Page/南充县.md "wikilink")、[西充县](../Page/西充县.md "wikilink")、[南部县](../Page/南部县.md "wikilink")、[仪陇县](../Page/仪陇县.md "wikilink")、[营山县](../Page/营山县.md "wikilink")、[蓬安县](../Page/蓬安县.md "wikilink")、[武胜县](../Page/武胜县.md "wikilink")、[岳池县](../Page/岳池县.md "wikilink")。
  - 1952年，撤消行署区，川北行署区也随之消失，但仍然保留专区建制。
  - 1953年3月10日，撤销[大竹专区](../Page/大竹专区.md "wikilink")，将所属的[广安县划归南充专区管辖](../Page/广安县_\(中华民国\).md "wikilink")。撤销[广元专区](../Page/广元专区.md "wikilink")，所属的[苍溪和](../Page/苍溪县.md "wikilink")[阆中两县划归南充专区管辖](../Page/阆中县.md "wikilink")。
  - 1968年，南充专区更名南充地区。
  - 1978年11月10日，取广安县和[邻水县部分地](../Page/邻水县.md "wikilink")，设立[华云工农示范区](../Page/华云工农示范区.md "wikilink")（县级，当时四川省共有4个[工农示范区](../Page/工农示范区.md "wikilink")，后来保留并建立县级单位的只有此区)。
  - 1985年2月4日，撤销[华云工农区](../Page/华云工农区.md "wikilink")，设立县级[华蓥市](../Page/华蓥市.md "wikilink")。至此，南充地区辖2市11县，为历史管辖最大范围。
  - 1986年1月，苍溪县划属[广元市](../Page/广元市.md "wikilink")。
  - 1991年1月，撤消阆中县，建立县级阆中市。
  - 1993年7月2日，撤销南充地区、县级南充市、南充县，设立地级南充市。原县级南充市和南充县境分设[顺庆区](../Page/顺庆区.md "wikilink")、[高坪区](../Page/高坪区.md "wikilink")、[嘉陵区](../Page/嘉陵区.md "wikilink")3区，市政府驻地顺庆区。析南充地区的华蓥市、广安县、岳池县、武胜县和达县地区的邻水县，设立[广安地区](../Page/广安地区.md "wikilink")。

## 参考文献

1.
2.
{{-}}

[Category:四川地区](../Category/四川地区.md "wikilink")
[Category:南充行政区划史](../Category/南充行政区划史.md "wikilink")
[Category:广安行政区划史](../Category/广安行政区划史.md "wikilink")
[Category:广元行政区划史](../Category/广元行政区划史.md "wikilink")
[Category:1968年建立的行政區劃](../Category/1968年建立的行政區劃.md "wikilink")
[Category:1993年废除的行政区划](../Category/1993年废除的行政区划.md "wikilink")