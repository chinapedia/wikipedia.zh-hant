[Langues_d'oïl.PNG](https://zh.wikipedia.org/wiki/File:Langues_d'oïl.PNG "fig:Langues_d'oïl.PNG")
**奥依语**（*langue
d'oïl*），是[罗曼语族的一支](../Page/罗曼语族.md "wikilink")，源自现在[法国](../Page/法国.md "wikilink")[卢瓦尔河以北](../Page/卢瓦尔河.md "wikilink")、一部分[比利时和](../Page/比利时.md "wikilink")[海峡群岛的地区](../Page/海峡群岛.md "wikilink")。

*Langue d'oïl* 通常是指整个奥依语支的语言，而最多人说的 *langue d'oïl*
是[法语](../Page/法语.md "wikilink")。但是有时，*langue d'oïl*
又用来指除法语外的所有 *oïl*
语言。自从20世纪中叶之后，在法语中若指整个语支的语言，改用了复数形式的
*langues d'oïl* 来表示。

在历史角度上， *langue d'oïl*
指旧法语，以便区分[高卢-罗曼语支](../Page/高卢-罗曼语支.md "wikilink")，如[奥克语](../Page/奥克语.md "wikilink")。

## 历史

*oïl* 是[古法语词](../Page/古法语.md "wikilink")，原意為「是」、「然也」的意思，即現代法語的 *oui*。

在中古时期意大利诗人[但丁以](../Page/但丁.md "wikilink")[拉丁语写成的](../Page/拉丁语.md "wikilink")《[论俗语](../Page/论俗语.md "wikilink")》（*De
vulgari eloquentia*）中：“*nam alii oc, alii si, alii vero dicunt
oil*”（“有些人说 oc、其他一些人说 si, 其他一些人说
oïl”），显示了罗曼语族区可分为三种：*oïl*（奥依语，在法国北部）、*oc*（[奥克语](../Page/奥克语.md "wikilink")，在法国南部）和*si*（[意大利和](../Page/意大利.md "wikilink")[伊比利亚半岛](../Page/伊比利亚半岛.md "wikilink")）。[通俗拉丁语发展出表达同意的几种说法](../Page/通俗拉丁语.md "wikilink")：*hoc
ille*（“that (is) it”）和*hoc* （“that”），它们相对地演变成奥依语和奥克语。后续的语言发展，把 *oïl*
演变成现在法语的 *oui*。（其他罗曼语族语言的“是”是由拉丁语 *sic*而来
，如[西班牙语的](../Page/西班牙语.md "wikilink")
*sí*、[意大利语的](../Page/意大利语.md "wikilink")
*sì*和[葡萄牙语的](../Page/葡萄牙语.md "wikilink") *sim*。）

现在语言学家普遍把[中古时期在法国使用的语言依区域划分为三个组别](../Page/中古时期.md "wikilink")：两个大的组别为奥依语和[奥克语](../Page/奥克语.md "wikilink")，第三个组别为[法兰克-普罗旺斯语](../Page/法兰克-普罗旺斯语.md "wikilink")（Franco-Provençal），被看成为上述两个组别的过渡语言。

奥依语的语言过渡，沿比利时直到法国的北部、中部和海峡群岛，形成[方言连续体](../Page/方言连续体.md "wikilink")（dialect
continuum）。

现在称为[法语的语言](../Page/法语.md "wikilink")，是奥依语的一种，但在法国多个世纪的历史中，曾有一大群并非使用法语的奥依语社群，另外还有不是使用奥依语的人口（参看*[法国语言](../Page/法国语言.md "wikilink")*）。

在中古时期，虽然有过各种奥依语在文学标准上的争夺，但在法兰西王国的中央统治下（以至在法兰西王国以外），使大多数奥依语在数个世纪中处于相对寂静的状态。

法语的兴起，主要有两种理论解释：

### 法兰西岛语理论（Francien theory）

有人认为法兰西岛语（Francien），即在[巴黎地区以至](../Page/巴黎.md "wikilink")“旧政体”（Ancien
Régime）所使用的语言，在法国王朝中因为国王的使用，使其变成了全国的[官方语言](../Page/官方语言.md "wikilink")。这些人声称法兰西岛语演变成今天的法语。

虽然在流通的教科书中仍提及法兰西岛语理论，但这理论现在的语言学者多不再重视。

### 法国通用语理论（lingua franca theory）

多数语言学者提出的理论，指出从维莱尔科特雷法案（）强制推行并取代拉丁语的“法语”，并不是单一种特定形式的奥依语，而是普遍通行的行政语言，各地区语言的变异被除去，成为一种大家都能明白的[通用语](../Page/通用语.md "wikilink")。

有议论说这种语言并非打算成为国家语言，它只是一种在法律和行政上使用的语言。但这种新语言的[文学发展](../Page/文学.md "wikilink")，促使了作家使用法语，而非本来的区域语言。这引致了[本土文学的衰落](../Page/本土文学.md "wikilink")。

直至[第一次世界大战前](../Page/第一次世界大战.md "wikilink")，法语还未是法国人的主要语言——区域性的语言仍是法国人在家中或生活范围内的语言，而奥依语的情况也大致如此。

## 文学

[Bram_Bilo_1890.jpg](https://zh.wikipedia.org/wiki/File:Bram_Bilo_1890.jpg "fig:Bram_Bilo_1890.jpg")写成的短篇故事\]\]

除了[法国文学的影响外](../Page/法国文学.md "wikilink")，其他奥依语的小规模文学作品仍能维持。在[庇卡底语](../Page/庇卡底语.md "wikilink")（Picard）和
Poitevin-Saintongeais
语中，戏剧编写最为突出。口述表演（说故事）是[高卢语的特色](../Page/高卢语.md "wikilink")，而[诺曼语](../Page/诺曼语.md "wikilink")（Norman）和[瓦龙语文学](../Page/瓦龙语.md "wikilink")，尤其是19世纪初期，侧重文字和诗句（例子可看
*[Wace](../Page/Wace.md "wikilink")*
和[泽西语文学](../Page/泽西语文学.md "wikilink")*（Jèrriais
literature）*）。

## 状况

[Fosses-la-Ville_JPG06W.jpg](https://zh.wikipedia.org/wiki/File:Fosses-la-Ville_JPG06W.jpg "fig:Fosses-la-Ville_JPG06W.jpg")

奥依语各语言，除法语外（法语是很多国家的官方语言），很少受到正式的认可。

现时，瓦龙语、[洛林语](../Page/洛林语.md "wikilink")（以 Gaumais
为当地名字）和[香槟语](../Page/香槟语.md "wikilink")，在[瓦隆地区](../Page/瓦隆.md "wikilink")（Wallonia）有区域性语言的地位。

在海峡群岛上的语言，获得行政区（Balliwick）政府给予一定的地位，在区域语言上也得[英格兰-爱尔兰议会认可](../Page/英格兰-爱尔兰议会.md "wikilink")。

法国政府认可奥依语为[法国语言](../Page/法国语言.md "wikilink")，但未在[欧洲区域或少數民族语言宪章](../Page/欧洲区域或少數民族语言宪章.md "wikilink")（European
Charter for Regional or Minority
Languages）[宪法上的正式批准](../Page/法国宪法.md "wikilink")。

## 影响

[Bilingual_signage-Gallo.jpg](https://zh.wikipedia.org/wiki/File:Bilingual_signage-Gallo.jpg "fig:Bilingual_signage-Gallo.jpg")（Rennes）地铁的[高卢语告示](../Page/高卢语.md "wikilink")\]\]

[英语在英国被](../Page/英语.md "wikilink")[诺曼人征服之后](../Page/诺曼人.md "wikilink")，受到很大的影响，很多词汇都采用了[诺曼语的典型特色](../Page/诺曼语.md "wikilink")。

[比利时法语可看到部分受到](../Page/比利时法语.md "wikilink")[瓦龙语影响](../Page/瓦龙语.md "wikilink")。

奥依语或多或少受到征服当地的[日尔曼人所影响](../Page/日尔曼人.md "wikilink")，尤其是[法兰克人](../Page/法兰克人.md "wikilink")。

在北美洲、法语的发展受原本来自法国西北部的移民所影响，他们之中有很多把奥依语的特征融入了他们所的法语之中。（参看*[美国使用的法语](../Page/美国使用的法语.md "wikilink")、[加拿大法语](../Page/加拿大法语.md "wikilink")*）

## 奥依语分支

以下列表是依照法兰西岛语理论。

  - [香槟语](../Page/香槟语.md "wikilink")（Champenois）
  - [弗朗什-孔泰语](../Page/弗朗什-孔泰语.md "wikilink")（Franc-Comtois）
  - [法蘭西島語](../Page/法蘭西島語.md "wikilink")（Francien）
      - [法语](../Page/法语.md "wikilink")
          - [加拿大法语](../Page/加拿大法语.md "wikilink")
              - [魁北克法语](../Page/魁北克法语.md "wikilink")
                  - [Joual](../Page/Joual.md "wikilink")
  - [洛林方言](../Page/洛林方言.md "wikilink")（Lorrain）
  - [诺曼语](../Page/诺曼语.md "wikilink")（Norman）
      - [盎格鲁-诺曼语](../Page/盎格鲁-诺曼语.md "wikilink")（Anglo-Norman，已消亡）
      - [Auregnais](../Page/Auregnais.md "wikilink")（已消亡，曾在[奥尔德尼](../Page/奥尔德尼.md "wikilink")(Alderney)使用)
      - [根西语](../Page/根西语.md "wikilink")（Dgèrnésiais，在[根西岛使用](../Page/根西岛.md "wikilink")）
      - [泽西语](../Page/泽西语.md "wikilink")（Jèrriais，在[泽西岛使用](../Page/泽西岛.md "wikilink")）
      - [Sercquiais](../Page/Sercquiais.md "wikilink")
        （在[萨克岛](../Page/萨克岛.md "wikilink")(Sark)使用）
  - [庇卡底语](../Page/庇卡底语.md "wikilink")（Picard）
  - [Poitevin-Saintongeais](../Page/Poitevin-Saintongeais.md "wikilink")
      - [阿卡迪亚法语](../Page/阿卡迪亚法语.md "wikilink")（français acadien，Acadian
        French）
          - [卡真法语](../Page/卡真法语.md "wikilink")（français cadien，Cajun
            French，在美国[路易斯安那州](../Page/路易斯安那州.md "wikilink")）
  - [瓦龙语](../Page/瓦龙语.md "wikilink")（Walon）

## 源自法语的克里奥尔语

源自奥依语的[克里奥尔语和](../Page/克里奥尔语.md "wikilink")[皮钦语](../Page/皮钦语.md "wikilink")（源自法语的语言也包括在内）

  - [Chiac语](../Page/Chiac_Language.md "wikilink")（亦有源自[英语](../Page/英语.md "wikilink")）
  - [海地克里奥尔语](../Page/海地克里奥尔语.md "wikilink")
  - [Michif](../Page/Michif.md "wikilink")

## 參考文獻

  - *Paroles d'Oïl*, Défense et promotion des Langues d'Oïl, Mougon
    1994, ISBN 2-905061-95-2
  - *Les langues régionales*, Jean Sibille, 2000, ISBN 2-08-035731-X

[Category:中古语言](../Category/中古语言.md "wikilink")
[Category:法國語言](../Category/法國語言.md "wikilink")
[Category:罗曼语族](../Category/罗曼语族.md "wikilink")