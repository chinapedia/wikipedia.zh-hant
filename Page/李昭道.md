**李昭道**（），字**希俊**，中国[唐代画家](../Page/唐代.md "wikilink")。

李昭道出自唐朝宗室郇王房，是郇王[李祎](../Page/李祎_\(隋朝\).md "wikilink")（太祖[李虎第六子](../Page/李虎.md "wikilink")）的玄孙，长平肃王[李叔良的曾孙](../Page/李叔良.md "wikilink")，华阳郡公、原州长史[李孝斌的孙子](../Page/李孝斌.md "wikilink")，隴西郡公[李思訓之子](../Page/李思訓.md "wikilink")，[李林甫從父兄](../Page/李林甫.md "wikilink")。李昭道曾官至太原府倉曹、直集賢院，官至太子中舍。擅繪[青綠山水](../Page/青綠山水.md "wikilink")，世稱小李將軍。兼善鳥獸、樓台、人物，並創海景。畫風巧贍精緻，雖“豆人寸馬”，也畫得鬚眉畢現。

李昭道的作品现今罕见，据说是他画的《[春山行旅图](../Page/春山行旅图.md "wikilink")》、《[明皇幸蜀图](../Page/明皇幸蜀图.md "wikilink")》等，很有可能是后人的[摹本](../Page/摹本.md "wikilink")。

他的绘画技法：「小李将军」设色用笔稍变其父法，「变父之势，妙又过之」
《明皇幸蜀图》:安史之乱时唐明皇避难入蜀途中落魄的情形，在崇山峻岭中跋涉｡「初见平陆，马皆若惊，而帝马见小桥，作徘徊不进状」。

## 圖片

## 參考文獻

  -

  - Three Thousand Years of Chinese Painting. [Yale University
    Press](../Page/耶魯大學出版社.md "wikilink"). 1997 pp 66-68

[L李](../Page/category:唐朝画家.md "wikilink")

[Z](../Category/唐朝宗室.md "wikilink")
[Category:山水畫畫家](../Category/山水畫畫家.md "wikilink")