**HAT-P-4b**是一個環繞[BD+36º2593的](../Page/BD+36º2593.md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")。它位于[牧夫座](../Page/牧夫座.md "wikilink")，距地球大约1000[光年](../Page/光年.md "wikilink")。它屬於[熱木星](../Page/熱木星.md "wikilink")，质量是[木星的](../Page/木星.md "wikilink")68%而半径是其的127%，密度是水的41%（木星是31%）。

## 参考资料

  -
## 外部链接

[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")