**阿克塞哈萨克族自治县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省](../Page/甘肃省.md "wikilink")[酒泉市下属的一个](../Page/酒泉市.md "wikilink")[自治县](../Page/自治县.md "wikilink")。位于[甘肃](../Page/甘肃.md "wikilink")、[青海](../Page/青海.md "wikilink")、[新疆的交界处](../Page/新疆.md "wikilink")。面积31374平方公里，2004年人口1万。[邮政编码](../Page/邮政编码.md "wikilink")
736400，县政府驻红柳湾镇。

## 行政区划

下辖1个[镇](../Page/镇.md "wikilink")、2个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 外在链接

  - [新华网-阿克塞哈萨克族自治县](http://www.gs.xinhuanet.com/dfpd/akesai/shouye.htm)

## 参考文献

[阿克塞县](../Category/阿克塞县.md "wikilink")
[县/自治县](../Category/酒泉区县市.md "wikilink")
[Category:甘肃省民族自治县](../Category/甘肃省民族自治县.md "wikilink")
[甘](../Category/中国哈萨克族自治县.md "wikilink")