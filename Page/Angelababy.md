**Angelababy**（），[本名](../Page/本名.md "wikilink")**楊穎**（），出生於[上海](../Page/上海.md "wikilink")，是[香港女](../Page/香港.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")、[女演員](../Page/女演員.md "wikilink")，14歲出道（在[日本當](../Page/日本.md "wikilink")[模特](../Page/模特.md "wikilink")），後回[香港發展](../Page/香港.md "wikilink")，目前為[华谊兄弟艺人](../Page/华谊兄弟.md "wikilink")、[風尚國際](../Page/風尚國際.md "wikilink")（Style
International
Management）之旗下模特兒。2014年因參加中国大陸綜藝節目《[奔跑吧兄弟](../Page/奔跑吧兄弟.md "wikilink")》而上演藝事業高峰。其个人演艺事业在[日本](../Page/日本.md "wikilink")、[韓國和](../Page/韓國.md "wikilink")[香港都有非常不错的成绩](../Page/香港.md "wikilink")\[1\]。

## 名字

Angelababy本名「楊穎」。“Angelababy”这个名字由她的英文名“Angela”和“Baby”混合而成。因为她的同学都觉得Angela这个名字难读，加上她有婴儿肥（Baby
Fat），所以都叫Baby。在主持[迪士尼节目时](../Page/迪士尼.md "wikilink")，用回Angela一名，后来不想大家混淆她的名字，才使用“Angelababy”这个混合的名字\[2\]。

不过这个名字曾受到[浙江](../Page/浙江.md "wikilink")《[都市快报](../Page/都市快报.md "wikilink")》读者的质疑，称建议中国媒体在报道Angelababy相关消息的时候，最好备注上她的中文名。事件旋即引发大量讨论。根据现在的使用情况，中国大陆官媒[人民日报](../Page/人民日报.md "wikilink")、[新华社使用中文名](../Page/新华社.md "wikilink")“杨颖”，其他媒体使用英文名。而国家语言文字工作委员会负责人表示，报刊、电视台、电台对外文词汇要使用中文译法，不能直接出现英文。但对娱乐新闻则没有硬性规定。另据原[新闻出版总署下发的通知](../Page/新闻出版总署.md "wikilink")，报刊、图书、音像制品和电子书、互联网等各类出版物，禁止随意出现夹带使用英文单词或字母缩写等外国语言文字\[3\]。

## 經歷

1989年2月28日出生于[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")，爷爷是德国籍印度人，爸爸是中印混血，在[上海从事服装生意](../Page/上海.md "wikilink")。母亲是[上海人](../Page/上海人.md "wikilink")，楊穎的幼年生涯都在上海度过，小学曾就读于位于[徐汇区](../Page/徐汇区.md "wikilink")[长乐路上的长乐学校](../Page/长乐路.md "wikilink")（现[上海市位育实验学校](../Page/上海市位育实验学校.md "wikilink")），但因为父母工作繁忙，在小学一年级时就要自己放学。幼年的她还在上海的演艺学校学演戏及跳舞，之后在上海读初中两年\[4\]。受到父亲的影响，她幼年开始对服装充满了兴趣\[5\]。楊穎还有一个弟弟，名叫杨帆，比她小7岁。

楊穎于13歲時移居[香港](../Page/香港.md "wikilink")，中三至[中七就讀](../Page/預科.md "wikilink")[獻主會聖母院書院](../Page/聖母院書院.md "wikilink")，并于14岁加入模特圈，与[熊黛林是同门师妹](../Page/熊黛林.md "wikilink")\[6\]。2005年12月擔任[周杰倫](../Page/周杰倫.md "wikilink")《07-08世界巡迴演唱會香港站》其中一位舞蹈員；2006年5月1日，香港[風尚國際與](../Page/風尚國際.md "wikilink")[日本](../Page/日本.md "wikilink")[愛貝克思聯合宣佈](../Page/愛貝克思.md "wikilink")，Angelababy將簽約至[Avex為其日本區域的全約經理人](../Page/Avex.md "wikilink")\[7\]。她也是[東京Girls
Collection的其中一位外籍模特兒](../Page/東京Girls_Collection.md "wikilink")。2006年后演藝事業转向中国大陆，主要向[電影方面發展](../Page/電影.md "wikilink")，現已經出演多部華語電影。2012年6月正式签约中國著名经纪公司[华谊兄弟](../Page/华谊兄弟.md "wikilink")，并成立个人工作室\[8\]。
而粉絲名為「楊家將」，口號為「楊家大將，如穎隨行」於2019年4月，發布非官方，自製應援曲「喜樂如意」

此外，Angelababy亦是餐廳Baby
Cafe（[屯門和](../Page/屯門.md "wikilink")[旺角分鋪](../Page/旺角.md "wikilink")）及美甲店股東及老闆之一\[9\]\[10\]\[11\]。2015年6月17日成立创投基金AB
Capital，并于当日宣布投资跨境电商企业和轻断食果蔬汁品牌HeyJuice\[12\]。

TC Candler在2016年中選出Angelababy為「全球100張最美面孔」第83名。

2018年2月1日，Angelababy在[北京的](../Page/北京.md "wikilink")[英国驻华大使馆官邸被授予VisitBritain英国旅游局友好大使](../Page/英国驻华大使馆.md "wikilink")、英国大使馆文化教育\#EnglishisGreat\#推广大使称号，并受到来华访问的的[英国首相](../Page/英国首相.md "wikilink")[特蕾莎·梅会见](../Page/特蕾莎·梅.md "wikilink")\[13\]。

## 戀情及婚姻

2005年，16歲的[Angelababy與](../Page/Angelababy.md "wikilink")20歲的[陳偉霆因合作](../Page/陳偉霆.md "wikilink")[香港迪士尼某檔兒童節目而相識](../Page/香港迪士尼.md "wikilink")，其後交往，2010年初，陳偉霆證實兩人已經分手\[14\]，戀情前後長達4年時間。

2014年2月，[黃曉明在Angelababy生日當天駕著](../Page/黃曉明.md "wikilink")[兰博基尼現身](../Page/兰博基尼.md "wikilink")，並送上所駕駛的跑車做為Angelababy的生日禮物，兩人牽手正式承認交往四年的戀情\[15\]。同年12月31日，两人在[上海举办的](../Page/上海.md "wikilink")[东方卫视跨年盛典上秀恩爱](../Page/东方卫视.md "wikilink")，当时黄晓明骑着马现身舞台，并为Angelababy戴上皇冠\[16\]。

2015年5月，香港媒体报道称，黄晓明及Angelababy已在黄曉明的家乡[青岛领证结婚](../Page/青岛.md "wikilink")，并将于10月在[上海展览馆举行婚宴](../Page/上海展览馆.md "wikilink")。但黄晓明的工作人员表示，两人并未领证结婚。虽然两人会在2015年举办婚礼，但具体时间还没定，地点也不可能在上海\[17\]。

2015年5月27日，黄晓明及Angelababy在微博上秀出結婚證，證實兩人登記結婚\[18\]。10月8日，黄晓明及Angelababy的婚礼在[上海展览中心举行](../Page/上海展览中心.md "wikilink")\[19\]，此次婚礼共有2000人出席，其中出席的明星多达100多人，创下了明星婚礼的新纪录。在此次婚礼举办之后，两人还会回黄晓明的家乡青岛再办一场\[20\]\[21\]。部分外媒在报道婚礼时称Angelababy为“中国版[金·卡戴珊](../Page/金·卡戴珊.md "wikilink")”及“亚洲顶级演员”\[22\]。

在2016年9月黄晓明主演的《[王牌逗王牌](../Page/王牌逗王牌.md "wikilink")》发布会当天，黄晓明首次确认Angelababy已经怀孕。2016年10月8日，Angelababy发布微博，宣布其正式怀孕\[23\]。

2017年1月17日清晨，Angelababy在黄曉明及家人的陪同下，於[香港港安醫院順利產下一子](../Page/香港港安醫院.md "wikilink")，起名黄辉皓，小名為小海綿，Angelababy為重新調養身體並照顧小海綿為由而缺席《[奔跑吧](../Page/奔跑吧.md "wikilink")》首7期的錄製\[24\]\[25\]。

亦曾於網上應粉絲要求，分享自己化妝使用化妝鏡時燈光選用的訣竅\[26\]。

## 爭議事件

### 整容

Angelababy出道後，不少網民以她出道前的照片作比較，大部分網民均指她樣貌前後相差太遠。之后Angelababy自己在微博晒出自己之前的两张旧照，她承认以前的确有矫正过牙齿，可能是因为牙箍戴久了使牙齿看起来有些奇怪，不久后就恢复正常。

2012年3月，中国[大连某整形医院在其官网中发布标题为](../Page/大连.md "wikilink")《Angelababy整形失败
网友称下巴极不自然》的文章，并盗用Angelababy本人的照片。随后在2015年5月，Angelababy将中国[大连某整形医院告上法庭](../Page/大连.md "wikilink")，称侵犯其肖像权和名誉权，要求该公司公开赔礼道歉，并赔偿其各项损失共计28.5万元人民币。不过整形医院辩称，侵权文章是公司从某健康网站转载的\[27\]。

2015年10月15日，媒体报道称Angelababy根据北京市朝阳区人民法院主审法官及代理律师的建议，赴医疗机构进行面部鉴定\[28\]\[29\]。根据中午公布的鉴定结果显示，Angelababy脸部的各部位没有整容。据整形專家祁佐良鑑定，Angelababy的雙眼皮、眼角和口腔部分正常，無切口痕跡。而其他部位的檢查結果，需要由院方根據科學分析最終得出\[30\]\[31\]。

### 提前上載照片侵犯商業機密

2012年3月，Angelababy在[松下電器Lumix系列](../Page/松下電器.md "wikilink") GF5
型號相機正式推出市面前將產品相片上傳至[Instagram](../Page/Instagram.md "wikilink")，使產品的外形曝光。事後[松下電器](../Page/松下電器.md "wikilink")[中國](../Page/中國.md "wikilink")[上海分公司認為其舉動侵犯了公司商業機密](../Page/上海.md "wikilink")，影響到銷售策略與計劃。於是[松下電器要求與廣告公司](../Page/松下電器.md "wikilink")[麥肯公司解除合約](../Page/麥肯廣告.md "wikilink")，並發還已付的900多萬元[人民幣和賠償](../Page/人民幣.md "wikilink")100萬損失。此外更要額外賠款275萬餘元\[32\]。

### 保育穿山甲广告相关争议

2018年12月，Angelababy在个人微博上发布[穿山甲保育廣告](../Page/穿山甲.md "wikilink")，承諾不用穿山甲製成的藥材來「通乳」，随即引发争议。不过有實驗指出，穿山甲的鱗片並不具有任何療效\[33\]。

## 影視作品

### 電影

| 上映年份  |                         电影名                          |                角色                |    备注    |
| :---: | :--------------------------------------------------: | :------------------------------: | :------: |
| 2007年 |           [破事儿](../Page/破事儿.md "wikilink")           |                德雅                |          |
| 2009年 |          [矮仔多情](../Page/矮仔多情.md "wikilink")          |              Angel               |          |
| 2010年 |      [花田囍事2010](../Page/花田囍事2010.md "wikilink")      |               遺珠公主               |          |
| 2010年 |       [全城熱戀熱辣辣](../Page/全城熱戀熱辣辣.md "wikilink")       |                小琪                |          |
| 2011年 |          [最強囍事](../Page/最強囍事.md "wikilink")          |              Better              |          |
| 2011年 |          [建黨偉業](../Page/建黨偉業.md "wikilink")          | [小鳳仙](../Page/小鳳仙.md "wikilink") |          |
| 2011年 |          [全球熱戀](../Page/全球熱戀.md "wikilink")          |              小妹黄牡丹               |          |
| 2011年 |         [夏日乐悠悠](../Page/夏日乐悠悠.md "wikilink")         |                夏米                |          |
| 2012年 |  [痞子英雄首部曲：全面開戰](../Page/痞子英雄首部曲：全面開戰.md "wikilink")  |                范寧                |          |
| 2012年 |            [桃姐](../Page/桃姐.md "wikilink")            |            Angelababy            |    客串    |
| 2012年 |    [第一次](../Page/第一次_\(2012年香港電影\).md "wikilink")    |               宋詩喬                |          |
| 2012年 |       [太極1從零開始](../Page/太極1從零開始.md "wikilink")       |               陳玉娘                |          |
| 2012年 |       [太極2英雄崛起](../Page/太極2英雄崛起.md "wikilink")       |               陳玉娘                |          |
| 2013年 |       [在一起](../Page/在一起_\(電影\).md "wikilink")        |               李勝男                |          |
| 2013年 |      [一場風花雪月的事](../Page/一場風花雪月的事.md "wikilink")      |               呂月月                |          |
| 2013年 |      [狄仁傑之神都龍王](../Page/狄仁傑之神都龍王.md "wikilink")      |               銀睿姬                |          |
| 2014年 |          [失戀急讓](../Page/失戀急讓.md "wikilink")          |             呂婉娉(阿黑)              |          |
| 2014年 |    [黃-{}-飛鴻之英雄有夢](../Page/黃飛鴻之英雄有夢.md "wikilink")    |             心蘭(陸小花)              |   特別出演   |
| 2014年 |       [微愛之漸入佳境](../Page/微愛之漸入佳境.md "wikilink")       |                陈西                |          |
| 2015年 |   [新娘大作戰](../Page/新娘大作戰_\(2015年電影\).md "wikilink")   |                何靜                |          |
| 2015年 |        [奔跑吧！兄弟](../Page/奔跑吧！兄弟.md "wikilink")        |            Angelababy            |          |
| 2015年 |     [何以笙簫默](../Page/何以笙簫默_\(電影\).md "wikilink")      |               何以玫                |          |
| 2015年 |     [刺客任務：殺手47](../Page/刺客任務：殺手47.md "wikilink")     |               黛安娜                |          |
| 2015年 |           [尋龍訣](../Page/尋龍訣.md "wikilink")           |               丁思甜                | 百花奖最佳女配角 |
| 2016年 |        [謀殺似水年華](../Page/謀殺似水年華.md "wikilink")        |                小麥                |          |
|       |                                                      |                                  |          |
| 2016年 |  [天煞地球反擊戰：復甦紀元](../Page/天煞地球反擊戰：復甦紀元.md "wikilink")  |             Rain Lao             |          |
| 2016年 |          [封神傳奇](../Page/封神傳奇.md "wikilink")          |                蓝蝶                |          |
| 2016年 |     [擺渡人](../Page/擺渡人_\(2016年電影\).md "wikilink")     |                小玉                |          |
| 2016年 | [微微一笑很傾城](../Page/微微一笑很傾城_\(2016年電影\).md "wikilink") |               貝微微                |          |

### 電視劇

| 首播年份  |                       剧名                        |                  角色                  |   备注    |
| :---: | :---------------------------------------------: | :----------------------------------: | :-----: |
| 2004年 |   [天下無敌](../Page/天下無敌_\(电视剧\).md "wikilink")    |                                      |         |
| 2006年 |       [獅子山下](../Page/獅子山下.md "wikilink")        | 学生小慧（[港漂](../Page/港漂.md "wikilink")） | 單元《菊帶霜》 |
| 2015年 |   [大漢情緣之雲中歌](../Page/大漢情緣之雲中歌.md "wikilink")    |                  雲歌                  |         |
| 2017年 |      [孤芳不自賞](../Page/孤芳不自賞.md "wikilink")       |                 白娉婷                  |         |
| 2018年 |       [創業時代](../Page/創業時代.md "wikilink")        |                  那蓝                  |         |
| 2019  |      [我的真朋友](../Page/我的真朋友.md "wikilink")       |                 程真真                  |         |
|  待播   |       [無名偵探](../Page/無名偵探.md "wikilink")        |                 陳宣美                  |         |
|  待播   | [渴望生活](../Page/渴望生活_\(2019年电视剧\).md "wikilink") |                  林俪                  |         |

### 綜藝節目

|      首播年份       |                播出頻道                |                                  節目名                                   |                                                                备注                                                                |
| :-------------: | :--------------------------------: | :--------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------: |
|      2004年      |                                    | 《香港迪士尼樂園[Viva\! Club Disney](../Page/Viva!_Club_Disney.md "wikilink")》 |                                                               節目主持                                                               |
|    2008年12月     |                                    |                《[我是．模特兒](../Page/我是．模特兒.md "wikilink")》                |                                            節目主持，夥拍名模[周汶錡](../Page/周汶錡.md "wikilink")                                             |
|     2009年4月     | [無綫電視](../Page/無綫電視.md "wikilink") |                   [美女廚房](../Page/美女廚房.md "wikilink")                   |                                                               參賽者                                                                |
|   2014年-2019年   | [浙江卫视](../Page/浙江卫视.md "wikilink") |                 《[奔跑吧兄弟](../Page/奔跑吧兄弟.md "wikilink")》                 |                                                 固定主持（第一季至第四季、第五季第8至12期，第六季和第七季）                                                  |
|    2015年5月2日    | [湖南卫视](../Page/湖南卫视.md "wikilink") |                 《[快乐大本营](../Page/快乐大本营.md "wikilink")》                 |                                       《[何以笙簫默 (電影)](../Page/何以笙簫默_\(電影\).md "wikilink")》宣傳                                       |
|    2015年6月6日    | [湖南卫视](../Page/湖南卫视.md "wikilink") |                 《[快乐大本营](../Page/快乐大本营.md "wikilink")》                 |                                                                                                                                  |
|   2015年12月5日    | [湖南卫视](../Page/湖南卫视.md "wikilink") |                 《[快乐大本营](../Page/快乐大本营.md "wikilink")》                 |                                                                                                                                  |
|   2016年1月29日    | [浙江卫视](../Page/浙江卫视.md "wikilink") |             《[王牌对王牌](../Page/王牌对王牌_\(真人秀\).md "wikilink")》             |                                                              與跑男成員                                                               |
|   2017年7月15日    | [湖南卫视](../Page/湖南卫视.md "wikilink") |                 《[快乐大本营](../Page/快乐大本营.md "wikilink")》                 | 與[張翰](../Page/張翰.md "wikilink")、[張亮](../Page/張亮.md "wikilink")、[王祖藍](../Page/王祖藍.md "wikilink")、[張藝興](../Page/張藝興.md "wikilink") |
|      2018       |                爱奇艺                 |                                 机器人争霸                                  |                                                                                                                                  |
| 2018年6月30日-7月6日 | [湖南卫视](../Page/湖南卫视.md "wikilink") |               《[嚮往的生活](../Page/嚮往的生活.md "wikilink")》第二季                |                                                                                                                                  |
|    2019年4月19    | [浙江衛視](../Page/浙江衛視.md "wikilink") |               《[王牌對王牌第四季](../Page/王牌對王牌.md "wikilink")》                |                                  [鄭愷](../Page/鄭愷.md "wikilink")、[李晨](../Page/李晨.md "wikilink")                                   |
|       待播        |                                    |                 《[不止AB面](../Page/不止AB面.md "wikilink")》                 |                                                                                                                                  |
|       待播        |                                    |               《[我們一起上春晚](../Page/我們一起上春晚.md "wikilink")》               |                                                                                                                                  |

### 配音

  - 2009年：《[-{zh-cn:豚鼠特攻队; zh-tw:鼠膽妙算;
    zh-hk:超鼠特攻;}-](../Page/超鼠特攻.md "wikilink")》
  - 2011年：《[魔髮奇緣](../Page/魔髮奇緣.md "wikilink")》－[樂佩公主](../Page/樂佩公主.md "wikilink")

### 廣告

  - 2016年：[Oral-B](../Page/Oral-B.md "wikilink") 3D White
  - 2016年：[Dove](../Page/Dove.md "wikilink")，夥拍[李易峰出演](../Page/李易峰.md "wikilink")
  - 2018年：[Lux](../Page/Lux.md "wikilink")

## 音樂

### MV演出

  - [陳偉霆](../Page/陳偉霆.md "wikilink")：《[千萬少年](../Page/千萬少年.md "wikilink")》
  - [レミオロメン](../Page/レミオロメン.md "wikilink")：《[恋の予感から](../Page/恋の予感から.md "wikilink")》
  - [レミオロメン](../Page/レミオロメン.md "wikilink")：《[花鳥風月](../Page/花鳥風月.md "wikilink")》
  - [林俊傑](../Page/林俊傑.md "wikilink")：《[可惜沒如果](../Page/可惜沒如果.md "wikilink")》，因林俊傑參加《奔跑吧，兄弟！》，Angelababy
    答應林俊傑參與此MV拍攝。

### 歌曲

  - 2010年：《[Beauty Survivor](../Page/Beauty_Survivor.md "wikilink")》
  - 2011年：《[Love Never Stops](../Page/Love_Never_Stops.md "wikilink")》
  - 2011年：《[Everyday's a Beautiful
    Story](../Page/Everyday's_a_Beautiful_Story.md "wikilink")》
  - 2011年：《[TRIPOD BABY](../Page/TRIPOD_BABY.md "wikilink")》〔收錄於[m-flo
    TRIBUTE](../Page/m-flo_TRIBUTE.md "wikilink")〕
  - 2011年：《[海底之心](../Page/海底之心.md "wikilink")》（與[林俊傑合唱](../Page/林俊傑.md "wikilink")）
  - 2011年：《[Say Goodbye](../Page/Say_Goodbye.md "wikilink")》
  - 2012年：《[都要微笑好嗎](../Page/都要微笑好嗎.md "wikilink")》
  - 2015年：《[綠羅裙](../Page/綠羅裙.md "wikilink")》
  - 2019年：《雪花赋》（与杨宗纬合唱）

## 寫真集

  - 2008年：[LOVE Angelababy](../Page/LOVE_Angelababy.md "wikilink")
    寫真集（攝影師：CK,Mikocokiki（[郭碧琪](../Page/郭碧琪.md "wikilink")））
  - 2009年：電子寫真《[LOVE Angelababy
    Album](../Page/LOVE_Angelababy_Album.md "wikilink")》
  - 2009年：《[Miss Angelababy](../Page/Miss_Angelababy.md "wikilink")》
  - 2010年：《[Paradise](../Page/Paradise_\(Angelababy寫真集\).md "wikilink")》
  - 2010年：《[Angelababy Fashion Mook
    時尚特刊](../Page/Angelababy_Fashion_Mook_時尚特刊.md "wikilink")》

## 设计產品

  - 2010年：[Angelababy
    Boxset](../Page/Angelababy_Boxset.md "wikilink")（聖誕卡）
  - 2015年：[美图手机M](../Page/美图手机.md "wikilink")4（主要设计人之一，兼产品顾问）\[34\]

## 獎項

### 大型頒獎禮獎項

| 年份                                       | 頒獎典禮              | 獎項                                               | 影片 |
| :--------------------------------------- | :---------------- | :----------------------------------------------- | :- |
| **2012年**                                | 中國時尚權力榜           | 年度引領社會風尚公眾人物大獎                                   |    |
| 第14屆Mnet Asian Music Awards              | 年度最佳紅毯造型獎         |                                                  |    |
| **2013年**                                | 新浪微博之夜            | 微博年度風尚人物獎                                        |    |
| 第9屆華鼎獎                                   | 中國最受媒體歡迎女演員獎      |                                                  |    |
| 娛樂樂翻天                                    | 十大娛樂風格人物獎         |                                                  |    |
| 第24屆香港專業電影攝影師學會                          | 專業電影攝影師眼中最具魅力女演員獎 |                                                  |    |
| 第13屆華語電影傳媒大獎                             | 觀眾票選最受矚目女演員獎      | 《[第一次](../Page/第一次_\(2012年香港電影\).md "wikilink")》 |    |
| **2014年**                                | 愛奇藝之夜年度盛典         | 亚洲全能偶像大獎                                         |    |
| 新浪微博之夜                                   | 微博年度吸引力獎          |                                                  |    |
| 第21屆北京大學生電影節                             | 最受歡迎女演員獎          | 《[狄仁杰之神都龙王](../Page/狄仁杰之神都龙王.md "wikilink")》     |    |
| **2015年**                                | 新浪微博之夜            | 微博年度女神獎、年度微博Queen                                |    |
| 愛奇藝之夜年度盛典                                | 年度飞跃大奖            |                                                  |    |
| **2016年**                                | 新浪微博之夜            | 年度卓越公益人物獎                                        |    |
| 第33屆大眾電影[百花獎](../Page/百花獎.md "wikilink") | 最佳女配角獎            | 《[尋龍訣](../Page/尋龍訣.md "wikilink")》               |    |
|                                          |                   |                                                  |    |

## 參考文献

## 外部連結

  -
  -
  -
  -
  -
  -
  -
  -
  -
  - [Angelababy Blog](http://www.fotopu.com/img/34823.jpg) Xanga Blog

  - [angelababy.asia](http://angelababy.asia)Official website

[Y穎](../Category/楊姓.md "wikilink")
[Category:香港上海人](../Category/香港上海人.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Category:德裔混血兒](../Category/德裔混血兒.md "wikilink")
[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:愛貝克思集團藝人](../Category/愛貝克思集團藝人.md "wikilink")
[Category:華誼兄弟藝人](../Category/華誼兄弟藝人.md "wikilink")
[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:上海演员](../Category/上海演员.md "wikilink")
[Category:聖母院書院校友](../Category/聖母院書院校友.md "wikilink")
[Category:華語電影傳媒大獎最受歡迎女演員得主](../Category/華語電影傳媒大獎最受歡迎女演員得主.md "wikilink")
[Category:中華人民共和國億萬富豪](../Category/中華人民共和國億萬富豪.md "wikilink")
[Category:入籍香港的大陆人](../Category/入籍香港的大陆人.md "wikilink")

1.

2.

3.

4.

5.

6.
7.
8.  [Angelababy正式签约华谊兄弟
    成立工作室](http://ent.sina.com.cn/s/h/2012-06-28/09133670304.shtml)2012年06月28日

9.  [Baby開咖啡店
    客串侍應送驚喜](http://ol.mingpao.com/cfm/star5.cfm?File=20111028/saa01/mch1.txt)，明報
    2011年10月28日

10. [Angelababy的美甲店Mini
    Nail開張喇](http://www.thenailworld.com.hk/nail-salon/salon-interview/301-nail-salon-angelababy-mininail)


11. [baby
    cafe(屯門)](http://hk.ulifestyle.com.hk/spot/detail.html?id=ABdGDVowBwNZEwNv)
    HK.Ulifestyle.com

12.

13.

14. [Angelababy陳偉霆分手原因真相曝光](http://jx.people.com.cn/BIG5/n2/2016/0125/c186330-27619325-2.html)

15. [親Angelababy照外流！](http://www.ettoday.net/news/20140303/330518.htm)[黃曉明](../Page/黃曉明.md "wikilink")：我會把女友當公主般寵愛

16.

17.

18.

19.

20.

21.

22.

23.

24.

25. [Baby曝产后现身照：运动服遮肚 脸上满是喜悦(图)
    凤凰财经](http://finance.ifeng.com/a/20170117/15150681_0.shtml)

26. [化淡妝出街卻變了個大濃妝｜ANGELABABY教路：原來關化妝鏡燈光事？](https://www.mings-fashion.com/angelababy-%E5%8C%96%E5%A6%9D-196124/)

27.

28.

29.

30.

31.

32. [Angelababy玩instagram玩出禍，廣告公司或賠款千萬](http://unwire.hk/2012/07/03/angelababy-instagram/news/)

33. [穿山甲神奇療效可通乳？　Angelababy公益保育廣告遭批反效果](https://pets.ettoday.net/news/1364010#ixzz5dXrKLIHz)

34.