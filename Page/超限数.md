**超限数**是大于所有[有限数](../Page/有限集合.md "wikilink")（但不必為[绝对无限](../Page/绝对无限.md "wikilink")）的[基数或](../Page/基数.md "wikilink")[序数](../Page/序数.md "wikilink")，分別叫做**超穷基数**（）和**超穷序数**（）。术语「超限」（transfinite）是[康托尔提出的](../Page/康托尔.md "wikilink")，他希望避免词语[无限](../Page/无限集合.md "wikilink")（infinite）和那些只不过不是有限（finite）的那些对象有关的某些暗含。當時其他的作者少有这些疑惑；现在被接受的用法是称超限基数或序数为无限的。但是术语「超限」仍在使用。

超穷序数可以確定超穷基数，並導出[阿列夫数序列](../Page/阿列夫数.md "wikilink")。

对于有限数，有两种方式考虑超限数，作为[基数和作为](../Page/基数.md "wikilink")[序数](../Page/序数.md "wikilink")。不像有限基数和序数，超限基数和超限序数定义了不同类别的数。

  - 最小超限[序数是](../Page/序数.md "wikilink")[ω](../Page/序数.md "wikilink")。
  - 第一个超限[基数是](../Page/基数.md "wikilink")[aleph-0](../Page/艾禮富數.md "wikilink")
    \(\aleph_0\)，[整数的](../Page/整数.md "wikilink")[无限集合的](../Page/无限集合.md "wikilink")[势](../Page/势.md "wikilink")。如果[选择公理成立](../Page/选择公理.md "wikilink")，下一个更高的[基数是aleph](../Page/基数.md "wikilink")-1
    \(\aleph_1\)。如果不成立，则有很多不可比较于aleph-1并大于aleph-0的其他基数。但是在任何情况下，没有基数大于aleph-0并小于aleph-1。

[连续统假设声称在aleph](../Page/连续统假设.md "wikilink")-0和连续统（[实数的集合](../Page/实数.md "wikilink")）的势之间没有中间基数：就是说，aleph-1是实数集合的势。已经在数学上证实了连续统假设不能被证明为真或假，由于[不完备性的影响](../Page/哥德爾不完備定理.md "wikilink")。

某些作者，比如Suppes、Rubin使用术语超限基数来称呼[戴德金无限集合的势](../Page/戴德金无限集合.md "wikilink")，在可以不等于无限基数的上下文中；就是说在不假定[可数选择公理成立的上下文中](../Page/可数选择公理.md "wikilink")。给定这个定义，下列是等价的：

  - \(\mathbf{m}\)是超限基数。就是说有一个戴德金无限集合*A*使得*A*的势是\(\mathbf{m}\)。
  - \(\mathbf{m}+1 = \mathbf{m}\)。
  - \(\aleph_0 \leq \mathbf{m}\)。
  - 有一个基数\(\mathbf{n}\)使得\(\aleph_0 + \mathbf{n} = \mathbf{m}\)。

## 引用

  - O'Connor, J. J. and E. F. Robertson (1998) ["Georg Ferdinand Ludwig
    Philipp
    Cantor"](http://www-groups.dcs.st-and.ac.uk/~history/Biographies/Cantor.html),
    *[MacTutor History of Mathematics
    archive](../Page/MacTutor_History_of_Mathematics_archive.md "wikilink")*.
  - [Patrick Suppes](../Page/Patrick_Suppes.md "wikilink"), "Axiomatic
    Set Theory", Dover, 1972, ISBN 0-486-61630-4
  - Jean E. Rubin, "Set Theory for the Mathematician", Holden-Day (San
    Francisico, 1967)

## 参见

  - [阿列夫数](../Page/阿列夫数.md "wikilink")

  - [ℶ 數](../Page/ℶ_數.md "wikilink")

  - [基数](../Page/基数.md "wikilink")

  -
  - [无穷小](../Page/无穷小.md "wikilink")

  - [大基数](../Page/大基数.md "wikilink")

  - [极限序数](../Page/极限序数.md "wikilink")

  -
  -
  - [序数算术](../Page/序数算术.md "wikilink")

  - [序数](../Page/序数.md "wikilink")

  - [超限归纳法](../Page/超限归纳法.md "wikilink")

[C](../Category/基数.md "wikilink") [C](../Category/序数.md "wikilink")
[C](../Category/无穷集合论基本概念.md "wikilink")