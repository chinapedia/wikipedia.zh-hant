[缩略图](https://zh.wikipedia.org/wiki/File:Baseball_swing.jpg "fig:缩略图")

**擊球員**（**打者**）是指[棒球比賽中](../Page/棒球.md "wikilink")，進攻方上場上場擔任打擊任務的球員。擊球員必須手握[球棒站在](../Page/球棒.md "wikilink")[本壘兩側的](../Page/本壘.md "wikilink")[打擊區內](../Page/打擊區.md "wikilink")，設法將防守方[投手所投的球擊出](../Page/投手.md "wikilink")，以便上壘。如能經過[一壘](../Page/一壘.md "wikilink")、[二壘和](../Page/二壘.md "wikilink")[三壘](../Page/三壘.md "wikilink")，最终回到[本壘](../Page/本壘.md "wikilink")，即可[得分](../Page/得分_\(棒球\).md "wikilink")。擊球員若無法得分，則有可能形成[出局或](../Page/出局_\(棒球\).md "wikilink")[殘壘](../Page/殘壘.md "wikilink")。

補充說明：擊球員或有人稱之為打擊手，但打擊手這個說法並非正式用語。

## 击球次序

比赛中球队教练会按击球员打击能力安排九名队员按固定次序上场打击，通常球队中打击能力较强的队员会被安排在第三四五棒，亦称为中心打线。
根据比赛规则不同，比赛中[投手可能不需要上场击球](../Page/投手.md "wikilink")，由一名指定[代打代为上场击球](../Page/代打.md "wikilink")。\[1\]

## 装备

  - [手套](../Page/手套.md "wikilink")：帮助击球员握棒击球。
  - [头盔](../Page/头盔.md "wikilink")、护肘、护腿板：保护击球员在被球砸到时减少伤害。
  - [钉鞋](../Page/钉鞋.md "wikilink")：帮助击球员加快跑垒速度。

## 特殊的打法

在[職業棒球選手間](../Page/職業棒球選手.md "wikilink")，特別被命名的打法（姿勢、揮棒法的總稱、打擊姿勢）舉出下列幾種。

  - 一本足打法（單腳打法、稻草人打法、金雞獨立打法、佛朗明哥打法）

<!-- end list -->

  -
    配合投手的投球姿勢抬起靠投手那一側的腳之打法。很難學會，據說適合打全壘打，實際上是為了讓球靠近手而抬腳。不過使用此姿勢的全壘打選手很多。如王貞治是相當的有名。[田淵幸一](../Page/田淵幸一.md "wikilink")、[門田博光](../Page/門田博光.md "wikilink")、[大豐泰昭等人也模仿了此姿勢](../Page/大豐泰昭.md "wikilink")。類似此打法的有[木保達彥的](../Page/木保達彥.md "wikilink")**伐木打法**。

<!-- end list -->

  - 高爾夫打法

<!-- end list -->

  -
    由下方揮上來的打法。極端的上划打法。[小鶴誠](../Page/小鶴誠.md "wikilink")、[田代富雄](../Page/田代富雄.md "wikilink")、[石井浩郎](../Page/石井浩郎.md "wikilink")、[小久保裕紀等人採行此法](../Page/小久保裕紀.md "wikilink")。

<!-- end list -->

  - 天平打法

<!-- end list -->

  -
    [近藤和彥用過的打法](../Page/近藤和彥.md "wikilink")。右手持球棒握柄，左手持球棒中間，使球棒在左肩上呈水平。

<!-- end list -->

  - [神主打法](../Page/神主打法.md "wikilink")

<!-- end list -->

  -
    如神主一般將球棒立在面前的打法。[岩本義行為始祖](../Page/岩本義行.md "wikilink")。[落合博滿的三冠王也主要是由此打法達成的](../Page/落合博滿.md "wikilink")。其他著名選手有[小笠原道大](../Page/小笠原道大.md "wikilink")、[中村紀洋](../Page/中村紀洋.md "wikilink")、[江藤智等人的姿勢雖然也被稱為神主打法但各選手仍有相當差異在](../Page/江藤智.md "wikilink")。

<!-- end list -->

  - 提燈打法

<!-- end list -->

  -
    [中登志雄用過](../Page/中登志雄.md "wikilink")。伸縮身體來調整好球帶，瞄準對自己有利的擊球。

<!-- end list -->

  - 蒟蒻打法

<!-- end list -->

  -
    [梨田昌孝用過](../Page/梨田昌孝.md "wikilink")。扭曲身體擺動的打法。據說是為了舒緩緊張而開始的。現在梨田的學生[大西宏明採行與此相近的打法](../Page/大西宏明.md "wikilink")。

<!-- end list -->

  - 單手打法

<!-- end list -->

  -
    韓國的-{[朴正泰](../Page/朴正泰.md "wikilink")}-用過的打法。左手置於左大腿附近，以右手單獨握棒。另外，阪神[金本知憲手腕受傷時曾用過類似此種的打法](../Page/金本知憲.md "wikilink")。

<!-- end list -->

  - [鐘擺打法](../Page/鐘擺打法.md "wikilink")

<!-- end list -->

  -
    讓投手側的腳好像晃到捕手側似的動作。[鈴木一朗](../Page/鈴木一朗.md "wikilink")、[坪井智哉等人](../Page/坪井智哉.md "wikilink")。

<!-- end list -->

  - 螃蟹腳打法

<!-- end list -->

  -
    [種田仁所用的打法](../Page/種田仁.md "wikilink")。被分類為開放性姿勢。右腳向三壘方向大幅跨出，呈螃蟹腳型態的特徵而得此名稱。

<!-- end list -->

  - [同步打法](../Page/同步打法.md "wikilink")

<!-- end list -->

  -
    [手塚一志所提倡的打法](../Page/手塚一志.md "wikilink")。大多數的一流職業選手使用。

必須注意的是，不管哪種打法都是先學會最基本打法後，個人依照自己的風格所產生出來的。一開始就模仿這些打法反而可能會妨礙選手習得正確的打法，因此少棒與青棒的教練多半會嚴厲要求這些地方。

## 參見

  - [球棒](../Page/球棒.md "wikilink")
  - [触身球](../Page/触身球.md "wikilink")
  - [三振](../Page/三振.md "wikilink")
  - [安打](../Page/安打.md "wikilink")
  - [完全打击](../Page/完全打击.md "wikilink")

## 外部連結

  -
[D打](../Category/棒球術語.md "wikilink")

1.  [1](http://baseball.sport.org.cn/jsgz/guize/2004-03-10/48410.html)，棒球规则：第6章
    击球员。