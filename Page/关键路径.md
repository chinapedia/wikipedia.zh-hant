[Pert_chart_colored.svg](https://zh.wikipedia.org/wiki/File:Pert_chart_colored.svg "fig:Pert_chart_colored.svg")项目中，5个[里程碑](../Page/里程碑.md "wikilink")（10到50个）和六项活动（A到F）。项目有两个关键路径：活动B和C，或者A,
D和F
–在快速跟踪情况下，项目最短时间为7个月。活动E是次重要的，有1个月的[浮动时间](../Page/浮动时间.md "wikilink")。\]\]

**关键路径法**（Critical Path
Method，CPM），又稱為**要徑法**，是计划项目活动中用到的一种[算术方法](../Page/算术.md "wikilink")。\[1\]對於有效的計劃管理而言，關鍵路徑是一個十分重要的工具。與[計畫評核術](../Page/計畫評核術.md "wikilink")（Program
Evaluation and Review
Technique，PERT）非常類似。要徑法所使用的估計作業時間是單一或確定的，而計畫評核術則是使用機率性的估計作業時間。這兩種技術經常混合使用，簡稱CPM/PERT。

## 历史

关键路径法（CPM）是一种项目模型技术，在1950年代由[杜邦的Morgan](../Page/杜邦.md "wikilink") R.
Walker和James E.
Kelley提出。\[2\]1989年，Kelley和Walker回忆了他们与开发CPM相关的记忆。\[3\]Kelley为[計畫評核術的开发人员提供了当时由](../Page/計畫評核術.md "wikilink")[博思艾伦汉密尔顿控股公司和](../Page/博思艾伦汉密尔顿控股公司.md "wikilink")[美国海军联合开发的](../Page/美国海军.md "wikilink")“关键路径”技术。\[4\]关键路径法最早由杜邦公司在1940到1943年间实行，直接导致了[曼哈顿计划的成功](../Page/曼哈顿计划.md "wikilink")。\[5\]

CPM可用于各类项目，包括建筑、空间和军事、软件开发、研究项目、产品研发、工程、工厂维护等等。任何具有独立活动的项目都可采用数学分析。尽管原始的CPM项目不再应用，但这个术语可用于任何分析项目网络逻辑图的方法。

## 基本技术

使用CPM的基本技术\[6\] \[7\]是建设一个包括以下方面的项目：

1.  列出完成项目的所有活动（一般通过[工作分解图](../Page/工作分解图.md "wikilink")）,
2.  每个活动完成需要的时间
3.  项目之间的[依赖性](../Page/依赖性.md "wikilink")

## 参见

  - [甘特图](../Page/甘特图.md "wikilink")
  - [项目管理软件列表](../Page/项目管理软件列表.md "wikilink")
  - [项目管理话题列表](../Page/项目管理话题列表.md "wikilink")
  - [计划评审技术](../Page/计划评审技术.md "wikilink")（PERT）
  - [项目](../Page/项目.md "wikilink")
  - [项目管理](../Page/项目管理.md "wikilink")
  - [项目计划](../Page/项目计划.md "wikilink")
  - [工作分解结构](../Page/工作分解结构.md "wikilink")
  - [关键链](../Page/关键链.md "wikilink")

## 参考

## 更多阅读

  -
  -
  -
  -
  -
  -
  -
  -
[Category:网络理论](../Category/网络理论.md "wikilink")
[Category:项目管理](../Category/项目管理.md "wikilink")
[Category:管理](../Category/管理.md "wikilink")
[Category:商业术语](../Category/商业术语.md "wikilink")
[Category:生产制造](../Category/生产制造.md "wikilink")
[Category:运营研究](../Category/运营研究.md "wikilink")
[Category:计划算术](../Category/计划算术.md "wikilink")

1.
2.
3.
4.
5.
6.  Samuel L. Baker, Ph.D. ["Critical Path Method
    (CPM)"](http://hspm.sph.sc.edu/COURSES/J716/CPM/CPM.html)
    *University of South Carolina*, Health Services Policy and
    Management Courses
7.