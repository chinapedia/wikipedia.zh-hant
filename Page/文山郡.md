**文山郡**（）為[台灣日治時期的行政區劃之一](../Page/台灣日治時期.md "wikilink")，該郡隸屬[臺北州](../Page/臺北州.md "wikilink")。

## 簡介

[Bunsan-gun_yakusho.jpg](https://zh.wikipedia.org/wiki/File:Bunsan-gun_yakusho.jpg "fig:Bunsan-gun_yakusho.jpg")

文山郡役所設於[新店街](../Page/新店街.md "wikilink")，管轄[新店街](../Page/新店街.md "wikilink")、[深坑-{庄}-](../Page/深坑庄.md "wikilink")、[石碇-{庄}-](../Page/石碇庄.md "wikilink")、[坪林-{庄}-及不設街](../Page/坪林庄.md "wikilink")-{庄}-的[蕃地](../Page/蕃地_\(文山郡\).md "wikilink")。轄域即今[新北市](../Page/新北市.md "wikilink")[新店區](../Page/新店區.md "wikilink")、[深坑區](../Page/深坑區.md "wikilink")、[石碇區](../Page/石碇區.md "wikilink")、[坪林區](../Page/坪林區.md "wikilink")、[烏來區](../Page/烏來區.md "wikilink")、[臺北市](../Page/臺北市.md "wikilink")[文山區等地](../Page/文山區.md "wikilink")。

## 機關

  - [臺北地方法院新店登記所](../Page/臺北地方法院.md "wikilink")（1906年設於新店街，1943年裁撤）
  - 郵便局
      - 深坑\[1\]
          - 1901年設於[深坑街](../Page/深坑老街.md "wikilink")，1939年遷至[深坑-{庄}-內湖](../Page/深坑庄.md "wikilink")（今[文山區順興里一帶](../Page/文山區.md "wikilink")）
      - 新店
          - 1915年設於大坪林-{庄}-
  - 警察
      - 1945年時設有[臺北州警務部](../Page/臺北州.md "wikilink")，本郡屬於文山郡警察課，而文山郡警察課又設立深坑分室及坪林分室。

## 交通

### 鐵路

  - [臺北鐵道株式會社](../Page/臺北鐵道株式會社.md "wikilink")[新店線](../Page/新店線_\(台鐵\).md "wikilink")
      - [十五份](../Page/萬隆站.md "wikilink") - 製罈會社前 -
        [景尾](../Page/景美站.md "wikilink") - 二十張 -
        [公學校前車站](../Page/公學校前車站.md "wikilink")－[大坪林](../Page/大坪林站.md "wikilink")－[七張](../Page/七張站.md "wikilink")－[新店](../Page/新店站.md "wikilink")－[郡役所前](../Page/郡役所前車站.md "wikilink")

## 資料統計

<table>
<caption>1942年本籍國籍別常住戶口[2]</caption>
<thead>
<tr class="header">
<th><p>街-{庄}-名</p></th>
<th><p>面積<br />
（km²）</p></th>
<th><p>人口（人）</p></th>
<th><p>人口密度<br />
（人/km²）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>本籍</p></td>
<td><p>國籍</p></td>
<td><p>計</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>臺灣</p></td>
<td><p>內地</p></td>
<td><p>朝鮮</p></td>
<td><p>中華民國</p></td>
</tr>
<tr class="odd">
<td><p>新店街</p></td>
<td><p>120.1180</p></td>
<td><p>23,496</p></td>
<td><p>599</p></td>
</tr>
<tr class="even">
<td><p>深坑-{庄}-</p></td>
<td><p>52.8655</p></td>
<td><p>21,689</p></td>
<td><p>286</p></td>
</tr>
<tr class="odd">
<td><p>石碇-{庄}-</p></td>
<td><p>144.3498</p></td>
<td><p>9,756</p></td>
<td><p>34</p></td>
</tr>
<tr class="even">
<td><p>坪林-{庄}-</p></td>
<td><p>170.8350</p></td>
<td><p>7,500</p></td>
<td><p>53</p></td>
</tr>
<tr class="odd">
<td><p>蕃地</p></td>
<td><p>321.1305</p></td>
<td><p>3,086</p></td>
<td><p>219</p></td>
</tr>
<tr class="even">
<td><p>文山郡</p></td>
<td><p><strong>809.2988</strong></p></td>
<td><p><strong>65,527</strong></p></td>
<td><p><strong>1,191</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

[Category:臺北州](../Category/臺北州.md "wikilink")
[文山郡](../Category/文山郡.md "wikilink")

1.  [深坑郵便局歷史](http://blog.sina.com.tw/stampinged/article.php?pbgid=1324&entryid=480552)
2.