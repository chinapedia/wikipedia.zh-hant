**道格拉斯DC-1**是[道格拉斯商業](../Page/道格拉斯.md "wikilink")[飛機的首個型號](../Page/飛機.md "wikilink")。

此型號的誕生可追溯於（Transcontinental & Western
Airlines，即[環球航空的前身](../Page/環球航空.md "wikilink")），一架[福克F10型飛機墜毀在](../Page/福克F.10.md "wikilink")[堪薩斯州附近](../Page/堪薩斯州.md "wikilink")，此意外共奪去了八人的性命。之後，[美國民用航空委员会規定所有乘客不能乘坐在機翼之上](../Page/美國民用航空委员会.md "wikilink")，於是[環大陸及西部航空便要求研發一台三引擎](../Page/環球航空.md "wikilink")，可容納十二名乘客，金屬機翼，可收起的降落架及當一台引擎失效時仍能飛抵目的地的飛機。

道格拉斯飛行器公司便研發一種雙引擎，金屬機翼，可容納十二名乘客及有空中服務員的飛機。此型號雖然是雙引擎，但其性能符合環大陸及西部航空航空的要求。此飛機於1933年首航，並命名為DC-1。

## 性能诸元

## 参见

### 相关研发

[道格拉斯DC-2](../Page/道格拉斯DC-2.md "wikilink")

[道格拉斯DC-3](../Page/道格拉斯DC-3.md "wikilink")

### 相似飞机

[波音247](../Page/波音247.md "wikilink")

## 参见

  - Gradidge, Jennifer M. (ed). "DC-1, DC-2, DC-3 The First Seventy
    Years", Air-Britain, Tonbridge UK, 2006, two volumes, ISBN
    0-85130-332-3.

## 外部链接

  - [DC-1，DC-2和DC-3](http://www.aviation-history.com/douglas/dc3.html)
  - [道格拉斯DC-1](http://www.flyinghigher.net/douglas/NC223Y.html)
  - [DC-1发展的故事](https://web.archive.org/web/20071022110618/http://dc3history.org/donalddouglas.htm)

[Category:道格拉斯](../Category/道格拉斯.md "wikilink")
[Category:美國航空器](../Category/美國航空器.md "wikilink")