**兴安盟**-{（）}-是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[内蒙古自治区下辖的](../Page/内蒙古自治区.md "wikilink")[盟](../Page/盟.md "wikilink")
，位于内蒙古自治区东北部。辖境北临[呼伦贝尔市](../Page/呼伦贝尔市.md "wikilink")，东接[黑龙江省](../Page/黑龙江省.md "wikilink")[齐齐哈尔市](../Page/齐齐哈尔市.md "wikilink")、[吉林省](../Page/吉林省.md "wikilink")[白城市](../Page/白城市.md "wikilink")，南邻[通辽市](../Page/通辽市.md "wikilink")，西界[锡林郭勒盟](../Page/锡林郭勒盟.md "wikilink")、[蒙古国](../Page/蒙古国.md "wikilink")。地处[大兴安岭中麓与](../Page/大兴安岭.md "wikilink")[松嫩平原过渡地带](../Page/松嫩平原.md "wikilink")，主要河流有[绰尔河](../Page/绰尔河.md "wikilink")、[洮儿河](../Page/洮儿河.md "wikilink")、[霍林河](../Page/霍林河.md "wikilink")，皆往东流入[嫩江](../Page/嫩江.md "wikilink")。全盟总面积5.51万平方公里，人口159.91万，[蒙古族人口比例约](../Page/蒙古族.md "wikilink")41%，盟行政公署驻[乌兰浩特市](../Page/乌兰浩特市.md "wikilink")。“兴安”为[满语音译](../Page/满语.md "wikilink")，意为“丘陵”\[1\]。

## 历史

[清代為](../Page/清朝.md "wikilink")[奉天省所屬](../Page/奉天省.md "wikilink")，[民國後為](../Page/中華民國.md "wikilink")[遼寧省所轄](../Page/遼寧省_\(中華民國\).md "wikilink")。

[九一八事變後日軍進攻](../Page/九一八事變.md "wikilink")[东北](../Page/中国东北地区.md "wikilink")，最後成為[滿洲國的領土](../Page/滿洲國.md "wikilink")。

[抗日戰爭勝利後自](../Page/中國抗日戰爭.md "wikilink")[遼寧省劃出改屬新設的](../Page/遼寧省_\(中華民國\).md "wikilink")[遼北省](../Page/遼北省.md "wikilink")。

1947年[共產黨在此地](../Page/中国共产党.md "wikilink")（[烏蘭浩特市](../Page/乌兰浩特市.md "wikilink")）宣佈成立[内蒙古自治政府](../Page/内蒙古自治政府.md "wikilink")，中共建制後撤[遼北省歸內蒙古自治區至今](../Page/遼北省.md "wikilink")。

1953年，[呼纳盟](../Page/呼纳盟.md "wikilink")、兴安盟和[哲里木盟合并设立](../Page/哲里木盟.md "wikilink")[内蒙古自治区东部区行政公署](../Page/内蒙古自治区东部区行政公署.md "wikilink")，驻乌兰浩特市。

1954年，行署撤销，原兴安盟与呼纳盟合并，改称[呼伦贝尔盟](../Page/呼伦贝尔市.md "wikilink")。

1969年，扎赉特旗随呼伦贝尔盟划归[黑龙江省](../Page/黑龙江省.md "wikilink")，科尔沁右翼前旗、突泉县划归[吉林省](../Page/吉林省.md "wikilink")[白城地区](../Page/白城地区.md "wikilink")，科尔沁右翼中旗随[哲里木盟划归吉林省](../Page/哲里木盟.md "wikilink")。

1979年，内蒙古自治区恢复原建制，扎赉特旗、科尔沁右翼前旗、突泉县划回呼伦贝尔盟，科尔沁右翼中旗仍属哲里木盟。

1980年，经国务院批准，恢复兴安盟建制。

## 地理

[大興安嶺橫貫全境](../Page/大興安嶺.md "wikilink")，西側是[蒙古高原的一部分](../Page/蒙古高原.md "wikilink")，東側是[松遼平原的一部分](../Page/松遼平原.md "wikilink")。是重要的林區與牧區。

兴安盟的西北部与[蒙古国](../Page/蒙古国.md "wikilink")，东与[吉林省](../Page/吉林省.md "wikilink")，东北与[黑龙江省接壤](../Page/黑龙江省.md "wikilink")，总面积6万7706平方公里，总人口160.9万人。

## 政治

### 现任领导

<table>
<caption>兴安盟四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党兴安盟委员会.md" title="wikilink">中国共产党<br />
兴安盟委员会</a><br />
<br />
书记</p></th>
<th><p><br />
<a href="../Page/内蒙古自治区人民代表大会.md" title="wikilink">内蒙古自治区人民代表大会</a><br />
常务委员会<br />
兴安盟工作委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/兴安盟行政公署.md" title="wikilink">兴安盟行政公署</a><br />
<br />
<br />
盟长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议兴安盟委员会.md" title="wikilink">中国人民政治协商会议<br />
兴安盟委员会</a><br />
<br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/张恩惠.md" title="wikilink">张恩惠</a>[2]</p></td>
<td><p><a href="../Page/李国栋.md" title="wikilink">李国栋</a>[3]</p></td>
<td><p><a href="../Page/奇巴图.md" title="wikilink">奇巴图</a>[4]</p></td>
<td><p><a href="../Page/尤国钧.md" title="wikilink">尤国钧</a>[5]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p><a href="../Page/蒙古族.md" title="wikilink">蒙古族</a></p></td>
<td><p>蒙古族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/内蒙古自治区.md" title="wikilink">内蒙古自治区</a><a href="../Page/托克托县.md" title="wikilink">托克托县</a></p></td>
<td><p><a href="../Page/辽宁省.md" title="wikilink">辽宁省</a><a href="../Page/北镇市.md" title="wikilink">北镇市</a></p></td>
<td><p>内蒙古自治区<a href="../Page/准格尔旗.md" title="wikilink">准格尔旗</a></p></td>
<td><p>辽宁省<a href="../Page/昌图县.md" title="wikilink">昌图县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年3月</p></td>
<td><p>2012年12月</p></td>
<td><p>2016年7月</p></td>
<td><p>2017年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖2个[县级市](../Page/县级市.md "wikilink")、1个[县](../Page/县级行政区.md "wikilink")、3个[旗](../Page/旗_\(行政区划\).md "wikilink")。

  - 县级市：[乌兰浩特市](../Page/乌兰浩特市.md "wikilink")、[阿尔山市](../Page/阿尔山市.md "wikilink")
  - 县：[突泉县](../Page/突泉县.md "wikilink")
  - 旗：[科尔沁右翼前旗](../Page/科尔沁右翼前旗.md "wikilink")、[科尔沁右翼中旗](../Page/科尔沁右翼中旗.md "wikilink")、[扎赉特旗](../Page/扎赉特旗.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>区划代码[6]</p></th>
<th><p>区划名称<br />
蒙古文</p></th>
<th><p>汉语拼音<br />
拉丁转写[7]</p></th>
<th><p>面积[8]<br />
（平方公里）</p></th>
<th><p>政府驻地</p></th>
<th><p>邮政编码</p></th>
<th><p>行政区划[9]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/街道办事处.md" title="wikilink">街道</a></p></td>
<td><p><a href="../Page/镇.md" title="wikilink">镇</a></p></td>
<td><p><a href="../Page/乡级行政区.md" title="wikilink">乡</a></p></td>
<td><p>其中：<br />
<a href="../Page/民族乡.md" title="wikilink">民族乡</a></p></td>
<td><p><a href="../Page/苏木_(行政区划).md" title="wikilink">苏木</a></p></td>
<td><p><a href="../Page/社区.md" title="wikilink">社区</a></p></td>
<td><p><a href="../Page/行政村.md" title="wikilink">行政村</a></p></td>
</tr>
<tr class="even">
<td><p>152200</p></td>
<td><p>兴安盟<br />
</p></td>
<td></td>
<td><p>55,130.73</p></td>
<td><p><a href="../Page/乌兰浩特市.md" title="wikilink">乌兰浩特市</a></p></td>
<td><p>137400</p></td>
<td><p>15</p></td>
</tr>
<tr class="odd">
<td><p>152201</p></td>
<td><p>乌兰浩特市<br />
</p></td>
<td></td>
<td><p>2,239.65</p></td>
<td><p><a href="../Page/和平街道_(乌兰浩特市).md" title="wikilink">和平街道</a></p></td>
<td><p>137400</p></td>
<td><p>11</p></td>
</tr>
<tr class="even">
<td><p>152202</p></td>
<td><p>阿尔山市<br />
</p></td>
<td></td>
<td><p>7,397.75</p></td>
<td><p><a href="../Page/温泉街道_(阿尔山市).md" title="wikilink">温泉街道</a></p></td>
<td><p>137800</p></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p>152221</p></td>
<td><p>科尔沁右翼前旗<br />
</p></td>
<td></td>
<td><p>16,791.17</p></td>
<td><p><a href="../Page/科尔沁镇.md" title="wikilink">科尔沁镇</a></p></td>
<td><p>137700</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>152222</p></td>
<td><p>科尔沁右翼中旗<br />
</p></td>
<td></td>
<td><p>12,789.67</p></td>
<td><p><a href="../Page/巴彦呼舒镇.md" title="wikilink">巴彦呼舒镇</a></p></td>
<td><p>029400</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>152223</p></td>
<td><p>扎赉特旗<br />
</p></td>
<td></td>
<td><p>11,115.53</p></td>
<td><p><a href="../Page/音德尔镇.md" title="wikilink">音德尔镇</a></p></td>
<td><p>137600</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>152224</p></td>
<td><p>突泉县<br />
</p></td>
<td></td>
<td><p>4,796.95</p></td>
<td><p><a href="../Page/突泉镇.md" title="wikilink">突泉镇</a></p></td>
<td><p>137500</p></td>
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>兴安盟各市（旗、县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[10]（2010年11月）</p></th>
<th><p>户籍人口[11]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>兴安盟</p></td>
<td><p>1613246</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>乌兰浩特市</p></td>
<td><p>327081</p></td>
<td><p>20.27</p></td>
</tr>
<tr class="even">
<td><p>阿尔山市</p></td>
<td><p>68311</p></td>
<td><p>4.23</p></td>
</tr>
<tr class="odd">
<td><p>科尔沁右翼前旗</p></td>
<td><p>299834</p></td>
<td><p>18.59</p></td>
</tr>
<tr class="even">
<td><p>科尔沁右翼中旗</p></td>
<td><p>251461</p></td>
<td><p>15.59</p></td>
</tr>
<tr class="odd">
<td><p>扎赉特旗</p></td>
<td><p>392346</p></td>
<td><p>24.32</p></td>
</tr>
<tr class="even">
<td><p>突泉县</p></td>
<td><p>274213</p></td>
<td><p>17.00</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/中华人民共和国第六次全国人口普查.md "wikilink")，全盟[常住人口为](../Page/常住人口.md "wikilink")1613250人\[12\]，同[第五次全国人口普查相比](../Page/中华人民共和国第五次全国人口普查.md "wikilink")，十年共减少5632人，减少0.35%。其中，男性人口为827993人，占51.32%；女性人口785257人，占48.68%。常住人口性别比（以女性人口为100）为105.44。0－14岁人口为232047人，占14.38%；15－64岁人口为1283477人，占79.56%；65岁及以上人口为97726人，占6.06%。

### 民族

常住人口中，[汉族人口为](../Page/汉族.md "wikilink")874393人，占54.2%；[蒙古族人口为](../Page/蒙古族.md "wikilink")665828人，占41.27%；其他[少数民族人口为](../Page/少数民族.md "wikilink")73029人，占4.53%。

{{-}}

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [蒙古族](../Page/蒙古族.md "wikilink") | [满族](../Page/满族.md "wikilink") | [朝鲜族](../Page/朝鲜族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [达斡尔族](../Page/达斡尔族.md "wikilink") | [锡伯族](../Page/锡伯族.md "wikilink") | [鄂温克族](../Page/鄂温克族.md "wikilink") | [鄂伦春族](../Page/鄂伦春族.md "wikilink") | [彝族](../Page/彝族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | -------------------------------- | ------------------------------ | -------------------------------- | ------------------------------ | ---------------------------------- | -------------------------------- | ---------------------------------- | ---------------------------------- | ------------------------------ | ---- |
| 人口数          | 874391                         | 665826                           | 62352                          | 5313                             | 2941                           | 1057                               | 419                              | 179                                | 173                                | 90                             | 505  |
| 占总人口比例（%）    | 54.20                          | 41.27                            | 3.87                           | 0.33                             | 0.18                           | 0.07                               | 0.03                             | 0.01                               | 0.01                               | 0.01                           | 0.03 |
| 占少数民族人口比例（%） | \---                           | 90.12                            | 8.44                           | 0.72                             | 0.40                           | 0.14                               | 0.06                             | 0.02                               | 0.02                               | 0.01                           | 0.07 |

**兴安盟民族构成（2010年11月）**\[13\]

## 参考文献

{{-}}

[兴安盟](../Category/兴安盟.md "wikilink")
[Category:内蒙古自治区的盟](../Category/内蒙古自治区的盟.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  （[第二次全国土地调查数据](../Page/第二次全国土地调查.md "wikilink")）
9.
10.
11.
12.
13.