**Free Pascal** （简称 **FPC**，原名为 **FPK Pascal**），是一个支持16位，32位和64位处理器的专业
[Pascal
语言编译器](../Page/Pascal_\(程式語言\).md "wikilink")，支持多种处理器架构，包括：[Intel
x86](../Page/Intel_x86.md "wikilink")（也支持8086）、[AMD64](../Page/AMD64.md "wikilink")/x86-64、[PowerPC及其](../Page/PowerPC.md "wikilink")64位架构、[SPARC](../Page/SPARC.md "wikilink")、[ARM](../Page/ARM架構.md "wikilink")、AArch64、[MIPS](../Page/MIPS架構.md "wikilink")
以及 [JVM](../Page/Java虚拟机.md "wikilink")。Free Pascal
支持多种操作系统，包括：[GNU/Linux](../Page/GNU/Linux.md "wikilink")、[FreeBSD](../Page/FreeBSD.md "wikilink")、[Haiku](../Page/Haiku.md "wikilink")、[Mac
OS
X](../Page/MacOS.md "wikilink")/[iOS](../Page/iOS.md "wikilink")/[Darwin](../Page/Darwin_\(操作系统\).md "wikilink")、[DOS](../Page/DOS.md "wikilink")、[Win32](../Page/Microsoft_Windows.md "wikilink")/64、[WinCE](../Page/WinCE.md "wikilink")、[OS/2](../Page/OS/2.md "wikilink")、MorphOS、[Nintendo
GBA](../Page/Nintendo_GameCube.md "wikilink")、[Nintendo
DS](../Page/Nintendo_DS.md "wikilink")、[Nintendo
Wii](../Page/Wii.md "wikilink")、[Android](../Page/Android.md "wikilink")、[AIX](../Page/IBM_AIX.md "wikilink")
and AROS，对 Motorola 68k 的支持也在开发中。

Free Pascal
是[自由软件](../Page/自由软件.md "wikilink")，软件包和[运行时库发布在](../Page/运行时库.md "wikilink")[GNU宽通用公共许可证下](../Page/GNU宽通用公共许可证.md "wikilink")，允许其他程序静态链接。编译器自身发布在[GNU通用公共许可证下](../Page/GNU通用公共许可证.md "wikilink")。编译器完全用
Pascal 语言写成。最新的版本为 3.0.4 。\[1\]

[Lazarus](../Page/Lazarus.md "wikilink") 项目建立在 Free Pascal
基础上，利用其作为编译器，提供了一个类似 Delphi 的快速应用开发（RAD）环境，与
Free Pascal一样支持多种平台，同样是自由软件。

現在，Free
Pascal已經被選定為中國大陸[全國青少年信息學奧林匹克聯賽（NOIP）以及中國大陸](../Page/全国青少年信息学奥林匹克联赛.md "wikilink")[全國青少年信息學奧林匹克競賽（NOI）以及](../Page/全国青少年信息学奥林匹克竞赛.md "wikilink")[国际信息学奥林匹克](../Page/国际信息学奥林匹克.md "wikilink")（IOI）的指定Pascal編譯環境。

## 未来开发计划

Free Pascal 计划在下一个大版本中添加一些特性。

## 參見

  - [Pascal](../Page/Pascal_\(編程語言\).md "wikilink")
  - [Object Pascal](../Page/Object_Pascal.md "wikilink")
  - [Lazarus](../Page/Lazarus.md "wikilink")
  - [Beyond Compare](../Page/Beyond_Compare.md "wikilink")

## 参考文献

<references />

## 外部链接

  - [Free Pascal官方网站](http://www.freepascal.org/)
  - [Free Pascal官方文档](http://www.freepascal.org/docs.var)
  - [Free Pascal官方wiki](http://www.freepascal.org/wiki)
  - [Introduction to Free
    Pascal 2.0](http://www.osnews.com/story.php?news_id=10607)（英文）：詳盡介紹FreePascal
    2.0的新功能及一些發展歷程。
  - [Lazarus](http://lazarus.freepascal.org/)
  - [Lazarus Code and Component
    Respository](http://lazarus-ccr.sourceforge.net/)：Lazarus文件、元件及程式庫的資料網站。

[Category:Pascal](../Category/Pascal.md "wikilink")
[Category:編譯器軟件](../Category/編譯器軟件.md "wikilink")
[Category:自由軟件](../Category/自由軟件.md "wikilink")

1.