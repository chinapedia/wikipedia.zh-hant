**噪鹛属**在[动物分类学上是](../Page/动物分类学.md "wikilink")[鸟纲](../Page/鸟纲.md "wikilink")[雀形目](../Page/雀形目.md "wikilink")[画眉科中的一个](../Page/画眉科.md "wikilink")[属](../Page/属_\(生物\).md "wikilink")。多数分布于[錫金与](../Page/錫金.md "wikilink")[不丹一带](../Page/不丹.md "wikilink")。

噪鹛属包括：

  - [灰头噪鹛](../Page/灰头噪鹛.md "wikilink")

  - [灰褐噪鹛](../Page/灰褐噪鹛.md "wikilink")

  - [红额噪鹛](../Page/红额噪鹛.md "wikilink")

  - [黑脸噪鹛](../Page/黑脸噪鹛.md "wikilink")

  - [白喉噪鹛](../Page/白喉噪鹛.md "wikilink")

  - [白冠噪鹛](../Page/白冠噪鹛.md "wikilink")

  - [小黑领噪鹛](../Page/小黑领噪鹛.md "wikilink")

  - [黑领噪鹛](../Page/黑领噪鹛.md "wikilink")

  - [黑噪鹛](../Page/黑噪鹛.md "wikilink")

  - [裸头噪鹛](../Page/裸头噪鹛.md "wikilink")

  - [条纹噪鹛](../Page/条纹噪鹛.md "wikilink")

  - [白颈噪鹛](../Page/白颈噪鹛.md "wikilink")

  - [黑冠噪鹛](../Page/黑冠噪鹛.md "wikilink")

  - [褐胸噪鹛](../Page/褐胸噪鹛.md "wikilink")

  - [栗颈噪鹛](../Page/栗颈噪鹛.md "wikilink")

  - [栗背噪鹛](../Page/栗背噪鹛.md "wikilink")

  - [黑喉噪鹛](../Page/黑喉噪鹛.md "wikilink")

  - [白脸噪鹛](../Page/白脸噪鹛.md "wikilink")

  - [黄喉噪鹛](../Page/黄喉噪鹛.md "wikilink")

  - [灰胸噪鹛](../Page/灰胸噪鹛.md "wikilink")

  - [栗臀噪鹛](../Page/栗臀噪鹛.md "wikilink")

  - [山噪鹛](../Page/山噪鹛.md "wikilink")

  - [黑额山噪鹛](../Page/黑额山噪鹛.md "wikilink")

  - [灰翅噪鹛](../Page/灰翅噪鹛.md "wikilink")

  - [棕颏噪鹛](../Page/棕颏噪鹛.md "wikilink")

  - [斑背噪鹛](../Page/斑背噪鹛.md "wikilink")

  - [白点噪鹛](../Page/白点噪鹛.md "wikilink")

  - [大噪鹛](../Page/大噪鹛.md "wikilink")

  - [眼纹噪鹛](../Page/眼纹噪鹛.md "wikilink")

  - [灰胁噪鹛](../Page/灰胁噪鹛.md "wikilink")

  - [台湾棕噪鹛](../Page/台湾棕噪鹛.md "wikilink")

  - [栗头噪鹛](../Page/栗头噪鹛.md "wikilink")

  - [斑胸噪鹛](../Page/斑胸噪鹛.md "wikilink")

  - [白颊噪鹛](../Page/白颊噪鹛.md "wikilink")

  - [灰颈噪鹛](../Page/灰颈噪鹛.md "wikilink")

  - [白胸噪鹛](../Page/白胸噪鹛.md "wikilink")

  - [细纹噪鹛](../Page/细纹噪鹛.md "wikilink")

  - [纹耳噪鹛](../Page/纹耳噪鹛.md "wikilink")

  - [褐顶噪鹛](../Page/褐顶噪鹛.md "wikilink")

  - [鳞斑噪鹛](../Page/鳞斑噪鹛.md "wikilink")

  - [纯色噪鹛](../Page/纯色噪鹛.md "wikilink")

  - [橙翅噪鹛](../Page/橙翅噪鹛.md "wikilink")

  - [杂色噪鹛](../Page/杂色噪鹛.md "wikilink")

  - [灰腹噪鹛](../Page/灰腹噪鹛.md "wikilink")

  - [黑顶噪鹛](../Page/黑顶噪鹛.md "wikilink")

  - [玉山噪鹛](../Page/玉山噪鹛.md "wikilink")

  - [红头噪鹛](../Page/红头噪鹛.md "wikilink")

  - [金翅噪鹛](../Page/金翅噪鹛.md "wikilink")

  - [纹枕噪鹛](../Page/纹枕噪鹛.md "wikilink")

  - [红翅噪鹛](../Page/红翅噪鹛.md "wikilink")

  - [红尾噪鹛](../Page/红尾噪鹛.md "wikilink")

  - [靛冠噪鹛](../Page/靛冠噪鹛.md "wikilink")

  - [台湾白喉噪鹛](../Page/台湾白喉噪鹛.md "wikilink")

  - [黑白噪鹛](../Page/黑白噪鹛.md "wikilink")

  - [柬埔寨噪鹛](../Page/柬埔寨噪鹛.md "wikilink")

  - [栗颊噪鹛](../Page/栗颊噪鹛.md "wikilink")

  - [棕噪鹛](../Page/棕噪鹛.md "wikilink")

  - [橙胸噪鹛](../Page/橙胸噪鹛.md "wikilink")

  - [不丹噪鹛](../Page/不丹噪鹛.md "wikilink")

  -
  - [银耳噪鹛](../Page/银耳噪鹛.md "wikilink")

  - [马来亚噪鹛](../Page/马来亚噪鹛.md "wikilink")

[\*](../Category/噪鹛属.md "wikilink")