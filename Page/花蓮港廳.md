[臺灣花蓮港廳廳舍正門.jpg](https://zh.wikipedia.org/wiki/File:臺灣花蓮港廳廳舍正門.jpg "fig:臺灣花蓮港廳廳舍正門.jpg")
[臺灣花蓮郵便局.jpg](https://zh.wikipedia.org/wiki/File:臺灣花蓮郵便局.jpg "fig:臺灣花蓮郵便局.jpg")
[臺灣花蓮港專賣支局.jpg](https://zh.wikipedia.org/wiki/File:臺灣花蓮港專賣支局.jpg "fig:臺灣花蓮港專賣支局.jpg")
[臺灣花蓮港臺灣銀行支店.jpg](https://zh.wikipedia.org/wiki/File:臺灣花蓮港臺灣銀行支店.jpg "fig:臺灣花蓮港臺灣銀行支店.jpg")

**花蓮港廳**是[台灣日治時期的行政區劃之一](../Page/台灣日治時期.md "wikilink")，設立於[明治](../Page/明治.md "wikilink")42年（1909年），台灣原有之二十廳，廢止合為十二廳之際，將東部之[台東廳分為台東](../Page/臺東廳.md "wikilink")、花蓮港兩廳，直到1945年改制為今[花蓮縣](../Page/花蓮縣.md "wikilink")。

## 十二廳時期

  - 1909年：廳治設於花蓮港，下轄[璞石閣支廳](../Page/璞石閣支廳.md "wikilink")。
  - 1914年：新設**新城支廳**、**內タロコ支廳（內太魯閣支廳）**。
  - 1916年：新設**鳳林支廳**。
  - 1918年：璞石閣支廳改稱玉里支廳。

## 1920年後行政區分

[1930年花蓮港廳行政區劃圖](https://zh.wikipedia.org/wiki/File:1930_Karenko_Cho.svg "fig:1930年花蓮港廳行政區劃圖")
1920年（大正9年）鳳林支廳併入花蓮支廳，新城支廳、內タロコ支廳合併為研海支廳。花蓮港廳下設有三支廳、二街-{庄}-、七區：

  - 花蓮支廳
      - 花蓮港街
      - 平野區
      - 吉野區
      - 壽區
      - 鳳林區
      - 新社區
      - 瑞穗區
  - 研海支廳
  - 玉里支廳
      - 玉里-{庄}-
      - 大-{庄}-區
  - 1922年研海支廳新增一研海區，研海區為普通行政區
  - 1928年增設鳳林支廳，下轄原花蓮支廳下的鳳林區、新社區、瑞穗區

## 1937年後行政區分

### 市、郡

花蓮港廳於[昭和](../Page/昭和.md "wikilink")12年（1937年）與台東廳開始實施街-{庄}-制，改支廳為郡、改區為街-{庄}-。至昭和20年（1945年）共管轄一市、三郡。

| 名稱                                 | [假名](../Page/假名.md "wikilink") | 備註                                      |
| ---------------------------------- | ------------------------------ | --------------------------------------- |
| [花蓮港市](../Page/花蓮港市.md "wikilink") | かれんこうし                         | 1940年花蓮港街升格為花蓮港市                        |
| [花蓮郡](../Page/花蓮郡.md "wikilink")   | かれんぐん                          | 郡役所設於[花蓮港市](../Page/花蓮港市.md "wikilink") |
| [鳳林郡](../Page/鳳林郡.md "wikilink")   | ほうりんぐん                         | 郡役所設於[鳳林街](../Page/鳳林街.md "wikilink")   |
| [玉里郡](../Page/玉里郡.md "wikilink")   | たまざとぐん                         | 郡役所設於[玉里街](../Page/玉里街.md "wikilink")   |

### 街、-{庄}-

<table>
<thead>
<tr class="header">
<th><p>郡</p></th>
<th><p>名稱</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/花蓮郡.md" title="wikilink">花<br />
蓮<br />
郡</a></p></td>
<td><p><a href="../Page/吉野庄.md" title="wikilink">吉野-{庄}-</a></p></td>
<td><p>今<a href="../Page/吉安鄉.md" title="wikilink">吉安鄉</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/壽庄.md" title="wikilink">壽-{庄}-</a></p></td>
<td><p>今<a href="../Page/壽豐鄉.md" title="wikilink">壽豐鄉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/研海庄.md" title="wikilink">研海-{庄}-</a></p></td>
<td><p>今<a href="../Page/新城鄉_(台灣).md" title="wikilink">新城鄉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蕃地_(花蓮郡).md" title="wikilink">蕃地</a></p></td>
<td><p>今<a href="../Page/秀林鄉.md" title="wikilink">秀林鄉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鳳林郡.md" title="wikilink">鳳<br />
林<br />
郡</a></p></td>
<td><p><a href="../Page/鳳林街.md" title="wikilink">鳳林街</a></p></td>
<td><p>今<a href="../Page/鳳林鎮_(台灣).md" title="wikilink">鳳林鎮</a>、<a href="../Page/光復鄉.md" title="wikilink">光復鄉一部分</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瑞穗庄.md" title="wikilink">瑞穗-{庄}-</a></p></td>
<td><p>今<a href="../Page/瑞穗鄉.md" title="wikilink">瑞穗鄉</a>、<a href="../Page/光復鄉.md" title="wikilink">光復鄉一部分</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新社庄_(花蓮港廳).md" title="wikilink">新社-{庄}-</a></p></td>
<td><p>今<a href="../Page/豐濱鄉.md" title="wikilink">豐濱鄉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蕃地_(鳳林郡).md" title="wikilink">蕃地</a></p></td>
<td><p>今<a href="../Page/萬榮鄉.md" title="wikilink">萬榮鄉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/玉里郡.md" title="wikilink">玉<br />
里<br />
郡</a></p></td>
<td><p><a href="../Page/玉里街.md" title="wikilink">玉里街</a></p></td>
<td><p>今<a href="../Page/玉里鎮.md" title="wikilink">玉里鎮</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/富里庄.md" title="wikilink">富里-{庄}-</a></p></td>
<td><p>今<a href="../Page/富里鄉.md" title="wikilink">富里鄉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蕃地_(玉里郡).md" title="wikilink">蕃地</a></p></td>
<td><p>今<a href="../Page/卓溪鄉.md" title="wikilink">卓溪鄉</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 人口

昭和16年（1941年）當時的統計

  - 總人口：153,785人
      - 詳細包含
          - 日本人：20,914人
          - 台灣人：130,720人
          - 朝鮮人：119人
          - 其他：2,032人

## 醫療

  - 台灣總督府鐵道局台北鐵道病院花蓮港分院
  - 台灣總督府花蓮港病院（今[署立花蓮醫院](../Page/署立花蓮醫院.md "wikilink")）
      - 台灣總督府花蓮港病院玉里分院

## 法院（裁判所）

昭和20年（1945年）當時設有

  - 台北地方法院花蓮港支部

## 警察

昭和20年（1945年）當時設有

  - 花蓮港廳警務課
      - 花蓮港警察署
      - 花蓮郡警察課
      - 鳳林郡警察課
      - 玉里郡警察課

## 教育

1927年設立花蓮港高等女學校，1937年改稱花蓮港廳立高等女學校（今[國立花蓮女子高級中學](../Page/國立花蓮女子高級中學.md "wikilink")）。

1936年6月花蓮地方熱心教育人士捐資興建設立花蓮港中學校，1937年10月改為花蓮港廳立花蓮港中學校（今[國立花蓮高級中學](../Page/國立花蓮高級中學.md "wikilink")）。

1940年設立花蓮港廳立工業學校（今[國立花蓮高級工業職業學校](../Page/國立花蓮高級工業職業學校.md "wikilink")），為著名的[八大省工之一](../Page/八大省工.md "wikilink")。

1921年設立花蓮港簡易農學校，1941年改名花蓮港廳立花蓮港農林學校（今[國立花蓮高級農業職業學校](../Page/國立花蓮高級農業職業學校.md "wikilink")）。

## 交通

[臺灣花蓮停車場.jpg](https://zh.wikipedia.org/wiki/File:臺灣花蓮停車場.jpg "fig:臺灣花蓮停車場.jpg")\]\]

### 總督府鐵道

昭和19年（1944年）當時路線包括

  - [台東線](../Page/臺東線.md "wikilink")

### 指定道路

昭和14年（1939年）當時包含以下「指定道路」

  - 花蓮港台東道 （今省道台九線，花東縱谷公路：花蓮→台東）
  - 上大和台東道（今省道台十一甲線：光復→豐濱。省道台十一線：豐濱→靜浦，台十一線花蓮至豐濱為戰後興築。）
  - 富里都蘭道（今省道台23線，富里→東河）
  - 瑞穗落合道（今縣道193線：瑞穗→玉里樂合）
  - 花蓮港初音道 （今省道台九丙線：花蓮→吉安→銅門）
  - 初音壽道 （今台九丙線：銅門→鯉魚潭→壽豐）
  - 蘇澳花蓮港道 （今台九線，蘇花公路）

## 港灣

昭和13年（1938年）當時

  - 花蓮港

## 神社

  - [花蓮港神社](../Page/花蓮港神社.md "wikilink")
  - [吉野神社](../Page/吉野神社.md "wikilink")
  - [豐田神社](../Page/豐田神社.md "wikilink")
  - [林田神社](../Page/林田神社.md "wikilink")
  - [佐久間神社](../Page/佐久間神社.md "wikilink")

## 国立公園

  - [次高太魯閣國立公園](../Page/次高太魯閣國立公園.md "wikilink")（次高タロコ-{国}-立公園，[雪霸國家公園](../Page/雪霸國家公園.md "wikilink")、[太魯閣國家公園前身](../Page/太魯閣國家公園.md "wikilink")）
  - [新高阿里山國立公園](../Page/新高阿里山國立公園.md "wikilink")（新高阿里山-{国}-立公園，[玉山國家公園](../Page/玉山國家公園.md "wikilink")、[阿里山國家風景區前身](../Page/阿里山國家風景區.md "wikilink")）

## 統計

<table>
<caption>1942年本籍國籍別常住戶口[1]</caption>
<thead>
<tr class="header">
<th><p>郡市名</p></th>
<th><p>面積<br />
（km²）</p></th>
<th><p>人口（人）</p></th>
<th><p>人口密度<br />
（人/km²）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>本籍</p></td>
<td><p>國籍</p></td>
<td><p>計</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>臺灣</p></td>
<td><p>內地</p></td>
<td><p>朝鮮</p></td>
<td><p>中華民國</p></td>
</tr>
<tr class="odd">
<td><p>花蓮港市</p></td>
<td><p>29.4095</p></td>
<td><p>27,160</p></td>
<td><p>11,644</p></td>
</tr>
<tr class="even">
<td><p>花蓮郡</p></td>
<td><p>1,953.8854</p></td>
<td><p>36,180</p></td>
<td><p>5,346</p></td>
</tr>
<tr class="odd">
<td><p>鳳林郡</p></td>
<td><p>1,196.0917</p></td>
<td><p>44,650</p></td>
<td><p>3,074</p></td>
</tr>
<tr class="even">
<td><p>玉里郡</p></td>
<td><p>1,449.1847</p></td>
<td><p>35,681</p></td>
<td><p>1,747</p></td>
</tr>
<tr class="odd">
<td><p>花蓮港廳</p></td>
<td><p><strong>4,628.5713</strong></p></td>
<td><p><strong>143,671</strong></p></td>
<td><p><strong>21,811</strong></p></td>
</tr>
</tbody>
</table>

## 參見

  - [台灣日治時期行政區劃](../Page/台灣日治時期行政區劃.md "wikilink")
  - [花蓮港廳長](../Page/花蓮港廳長.md "wikilink")

## 參考來源

[\*](../Category/花蓮港廳.md "wikilink")
[Category:台湾日治时期的厅](../Category/台湾日治时期的厅.md "wikilink")
[Category:花蓮縣歷史](../Category/花蓮縣歷史.md "wikilink") [Category:台灣日治時期行政區劃
(1909年-1920年)](../Category/台灣日治時期行政區劃_\(1909年-1920年\).md "wikilink")
[Category:1909年建立的行政区划](../Category/1909年建立的行政区划.md "wikilink")
[Category:1945年廢除的行政區劃](../Category/1945年廢除的行政區劃.md "wikilink")

1.