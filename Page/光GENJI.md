**光GENJI**，是1980年代後期[日本風靡一時的](../Page/日本.md "wikilink")[傑尼斯事務所的男性偶像組合](../Page/傑尼斯事務所.md "wikilink")，當時光GENJI算是[SMAP的前輩](../Page/SMAP.md "wikilink")。在1987年6月25日，事務所方面把「GENJI」和「光」的兩個組合合併，成為「光GENJI」。同年8月19日由[Pony
Canyon發行](../Page/Pony_Canyon.md "wikilink")《[STAR
LIGHT](../Page/STAR_LIGHT.md "wikilink")》開始出道。但組合在出道8年後，在1995年9月3日解散。名稱是由《[源氏物語](../Page/源氏物語.md "wikilink")》的主人公[光源氏而來](../Page/光源氏.md "wikilink")。值得一提，當初組成時他們亦稱為「**Light
In Shadow**」。

## 成員

1995年解散後，[內海光司及](../Page/內海光司.md "wikilink")[佐藤敦啟，现：佐藤アツヒロ（1995年解散改名）至今仍然待在傑尼斯事務所內](../Page/佐藤敦啟，现：佐藤アツヒロ（1995年解散改名）.md "wikilink")，繼續舞台活動。

**光**

  - [内海光司](../Page/内海光司.md "wikilink")（、1968年1月11日 - ）（隊長、副唱）
  - [大澤樹生](../Page/大澤樹生.md "wikilink")（、1969年4月20日 - ）（副唱）

**GENJI**

  - [諸星和己](../Page/諸星和己.md "wikilink")（、1970年8月12日 - ）（中心）
  - [佐藤寬之](../Page/佐藤寬之.md "wikilink")（、1970年11月2日 - ）（副唱）
  - [山本淳一](../Page/山本淳一.md "wikilink")（、1972年2月28日 - ）（主唱）
  - [赤坂晃](../Page/赤坂晃.md "wikilink")（、1973年5月8日 - ）（主唱）
  - [佐藤敦啟](../Page/佐藤敦啟.md "wikilink")：现佐藤アツヒロ（1995年改名。）（、1973年8月30日 -
    ）（副唱）

**光GENJI SUPER 5**

  - [内海光司](../Page/内海光司.md "wikilink")
  - [諸星和己](../Page/諸星和己.md "wikilink")
  - [山本淳一](../Page/山本淳一.md "wikilink")
  - [赤坂晃](../Page/赤坂晃.md "wikilink")
  - [佐藤敦啟](../Page/佐藤敦啟.md "wikilink")

## 團體內的組合

**SAY・S**

  - 佐藤寬之
  - 赤坂晃
  - 山本淳一
  - 佐藤敦啟

**AKIRA & KOHJI**

  - 赤坂晃
  - 内海光司

## 原GENJI成員

  - [田代秀高](../Page/田代秀高.md "wikilink")

## 簡歷

在1987年3月，以諸星、寬之、山本、敦啟、田代的5名組成「GENJI」。在同年4月5日，他們5人首次電視亮相，並在TBS的一個音樂節目公佈片尾曲「現在，想與你歌唱」。不久，田代退出，赤坂加入。

1987年6月25日，5人組的「GENJI」和2人組的「光」合成一體，當時他們初次亮相時的宣傳口號是「**來自超新星的訊息**」。他們因穿[滾軸溜冰表演歌曲而成為一時佳話](../Page/滾軸溜冰.md "wikilink")(雖然日本之前也有歌手在舞台上有零星類似的表演)，之後[滾軸溜冰開始全國的中小學生之間流行起來](../Page/滾軸溜冰.md "wikilink")。

當時成員的印象顏色，分別是內海光司=青色，大澤樹生=紫色，諸星和己=粉紅色，佐藤寬之=水色，山本淳一=紅色，赤坂晃=綠色，與及佐藤敦啟=黃色。這個成員印象顏色制度，在之後的後輩如[SMAP](../Page/SMAP.md "wikilink")、[V6](../Page/V6.md "wikilink")、[KinKi
Kids](../Page/KinKi_Kids.md "wikilink")、[嵐](../Page/嵐.md "wikilink")、[關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink")、[NEWS](../Page/NEWS.md "wikilink")、[Kis-My-Ft2等等](../Page/Kis-My-Ft2.md "wikilink")，甚至聲優組合[μ's](../Page/μ's.md "wikilink")、[Aqours及](../Page/Aqours.md "wikilink")[Aice5都有使用](../Page/Aice5.md "wikilink")。

光GENJI本身都有分組，分成「光」及「GENJI」，他們推出的CD間中都有各自的作品。之後，[V6及](../Page/V6.md "wikilink")[Hey\!
Say\!
JUMP都有這個分組制度](../Page/Hey!_Say!_JUMP.md "wikilink")。另外，在[V6出道之前](../Page/V6.md "wikilink")，他們是傑尼斯中繼[少年隊後](../Page/少年隊.md "wikilink"),全員可做後空翻的組合。

另外現在傑尼斯的組合[Kis-My-Ft2也仿傚光GENJI一樣穿](../Page/Kis-My-Ft2.md "wikilink")[滾軸溜冰表演](../Page/滾軸溜冰.md "wikilink")。

在1987至1988年所推出《[銀河天堂](../Page/銀河天堂.md "wikilink")》、《[玻璃的十代](../Page/玻璃的十代.md "wikilink")》及《[鑽石颱風](../Page/鑽石颱風.md "wikilink")》，在1988年都獨佔Oricon年間的1至3位。

在1988年至1995年期間，他們更在由[朝日電視台所播放的](../Page/朝日電視台.md "wikilink")《[Music
Station](../Page/Music_Station.md "wikilink")》總演出次數共234次，直到今天也沒有任何歌手及組合破下這個紀錄。但是，隨著偶像和音樂節目衰退時代的波浪吞沒，他們開始1990年初人氣開始下降。此後，他們所推出的作品，已經沒有在由初期由[CHAGE
and ASKA所提供的作品那麼出名了](../Page/CHAGE_and_ASKA.md "wikilink")。

在1994年8月31日，大澤樹生及佐藤寬之脫離後，光GENJI後就剩下五人，並改名為「**光GENJI SUPER
5**」。他們活動一年後，翌年1995年9月3日解散（當時事務所指他們不是「解散」而是「畢業」）。在1995年舉行解散音樂會的門票，更被黃牛黨一度炒到近100萬日元一張。

據聞光GENJI SUPER 5解散的原因，是因為當時核心成員諸星和己不同意傑尼斯事務所的經營方法，決定離開並且解散。

## 影音作品

### 單曲

1.  [STAR LIGHT](../Page/STAR_LIGHT.md "wikilink")

2.
3.
4.
5.
6.
7.
8.
9.  Little Birthday

10. CO CO RO

11.
12.
13.
14. WINNING RUN

15. GROWING UP

16. TAKE OFF

17.
18. Meet Me

19.
20.
21.
22. BOYS in August

23.
24.
25.
26. TRY TO REMEMBER

27. Melody Five

28. DON'T MIND 涙

29. Bye-Bye

### 專輯

1.  光GENJI (Hikaru GENJI)

2.  Hi\!

3.  Hey\! Say\!

4.  Hello･･･I Love You

5.
6.  Cool Summer

7.  White Dreaming with 光GENJI

8.  333 Thank You

9.
10. VICTORY

11. BEST FRIENDS

12. Pocket Album\~

13. DREAM PASSPORT

14. SPEEDY AGE

15. WELCOME

16.
17. HEART’N HEARTS

18. FOREVER YOURS

19. SUPER BEST TRY to REMEMBER

20. Someone Special

21. See You Again

## 外部链接

[\*](../Category/光GENJI.md "wikilink")
[Category:日本男子演唱團體](../Category/日本男子演唱團體.md "wikilink")
[H](../Category/前傑尼斯事務所所屬團體.md "wikilink")
[Category:日本男子偶像團體](../Category/日本男子偶像團體.md "wikilink")
[Category:日本唱片大獎獲獎者](../Category/日本唱片大獎獲獎者.md "wikilink")
[Category:FNS歌謠祭大獎獲獎者](../Category/FNS歌謠祭大獎獲獎者.md "wikilink")
[Category:Oricon單曲年榜冠軍獲得者](../Category/Oricon單曲年榜冠軍獲得者.md "wikilink")
[Category:Oricon專輯年榜冠軍獲得者](../Category/Oricon專輯年榜冠軍獲得者.md "wikilink")