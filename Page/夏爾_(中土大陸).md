**夏爾**（Shire）是作家[托爾金在](../Page/托爾金.md "wikilink")《[魔戒](../Page/魔戒.md "wikilink")》和其他著作所創造的[中土大陸](../Page/中土大陸.md "wikilink")（Middle
Earth）內的一個區域，主要居民為[哈比人](../Page/哈比人.md "wikilink")。夏爾位在中土大陸的北方，[伊利雅德和](../Page/伊利雅德.md "wikilink")[雅諾王國之內](../Page/雅诺.md "wikilink")。夏爾在[西方通用語裡為](../Page/西方通用語.md "wikilink")**Sûza**或是**Sûzat**，在辛達林語中為**i
Drann**。

## 地理

[缩略图|245x245像素| 根據托爾金小說所描繪的夏爾地圖。
](https://zh.wikipedia.org/wiki/File:TheShireMap.png "fig:缩略图|245x245像素| 根據托爾金小說所描繪的夏爾地圖。 ")
夏爾由東至西分成三個區域：[雄鹿地](../Page/雄鹿地.md "wikilink")、狹義夏爾和艾明貝瑞爾。

### 狹義夏爾

由圖克家族所統治，分為東西南北四區。

#### 東區

包含[史塔克](../Page/史塔克.md "wikilink")、巨木廳等區域，尊重雄鹿地領主的權威。

#### 西區

市政府（[米丘窟](../Page/米丘窟.md "wikilink")）所在地。

#### 南區

[菸葉產區](../Page/菸葉.md "wikilink")。

#### 北區

魔戒中哈比主角的故鄉，包含[哈比屯](../Page/哈比屯.md "wikilink")、綠丘鄉、[臨水區等區域](../Page/臨水區.md "wikilink")。

### 雄鹿地

由烈酒鹿家族所統治，使用特別的方言，與[布理關係密切](../Page/布理.md "wikilink")。

### 艾明貝瑞爾

[伊力薩王賜予哈比人的領土](../Page/伊力薩.md "wikilink")，位於塔丘和夏爾西區間。

## 語言

當地哈比人使用的語言是[中土世界](../Page/中土世界.md "wikilink")（Middle
Earth）的[通用語](../Page/通用語.md "wikilink")（Common Speech）。

## 歷史

[缩略图| 一個哈比人居住的洞穴。
](https://zh.wikipedia.org/wiki/File:Hobbit_hole.jpg "fig:缩略图| 一個哈比人居住的洞穴。 ")
在[第三紀元](../Page/第三紀元.md "wikilink")1601年，哈比人開始定居在夏爾。這就是夏爾開墾紀元（夏墾）的開始，日後所有曆法都以此年為元年。

夏爾很少發生過戰爭，夏墾1147年因一群[哥布林入侵夏爾](../Page/哥布林.md "wikilink")，才發生了「[綠原之戰](../Page/綠原之戰.md "wikilink")」，[班多拉斯·圖克率眾驅離了敵人](../Page/班多布拉斯·圖克.md "wikilink")。之後哈比人始終在夏爾過著與世無爭、自給自足的優閒生活，對於外界發生的事所知甚少。在第三紀元末期，中土大陸東方的黑暗魔君[索倫開始派出手下搜索](../Page/索倫_\(魔戒\).md "wikilink")[至尊魔戒的下落](../Page/至尊魔戒.md "wikilink")，為了防止邪惡勢力入侵夏爾，哈比人的守護者——灰袍[巫師](../Page/巫師_\(中土大陸\).md "wikilink")[甘道夫令一群](../Page/甘道夫.md "wikilink")[北方登丹遊俠在邊界看顧夏爾的安危](../Page/北方遊俠.md "wikilink")。可以說哈比人的安逸自在都是這些[游俠的奮鬥而換來的](../Page/遊俠_\(中土大陸\).md "wikilink")\[1\]\[2\]。

但在[魔戒聖戰時期](../Page/魔戒聖戰.md "wikilink")，遊俠們都去南方參戰，夏爾變得無人看守。使得從[艾辛格敗退的巫師](../Page/艾辛格.md "wikilink")[薩魯曼有機可乘](../Page/薩魯曼.md "wikilink")，帶著一批手下入主了夏爾，奴役著哈比人，並大肆破壞夏爾的自然環境。戰後[佛羅多·巴金斯等四個哈比人返鄉](../Page/佛羅多·巴金斯.md "wikilink")，發動了「[臨水之戰](../Page/臨水之戰.md "wikilink")」推翻薩魯曼的暴政，並開始復原夏爾的環境。

## 行政機構

夏爾地區的元首是領主，領主是夏爾議會的[議長](../Page/議長.md "wikilink")，也是夏爾[民兵和](../Page/民兵.md "wikilink")[義勇軍的將軍](../Page/義勇軍.md "wikilink")。但由於民兵和義勇軍都是在緊急時才會召集，所以領主的頭銜也就變成了單純名譽上的稱號。平時的首領是米丘窟的[市長](../Page/市长.md "wikilink")，或是稱作夏爾的市長，他同時兼任[郵政總局局長和](../Page/郵政總局.md "wikilink")[警察總長的工作](../Page/警察.md "wikilink")，而[警隊和](../Page/警隊.md "wikilink")[郵政也是夏爾唯一提供的](../Page/郵政.md "wikilink")[公共服務](../Page/公共服務.md "wikilink")。在每七年[夏至時舉辦的](../Page/夏至.md "wikilink")[嘉年華會上選出夏爾市長](../Page/嘉年華會.md "wikilink")。

警隊是夏爾維護秩序的[執法人員](../Page/執法人員.md "wikilink")，其主要工作為驅趕迷途的野獸，全夏爾共有十二名[警長](../Page/警長.md "wikilink")\[3\]。

## 夏爾名人

  - [比爾博·巴金斯](../Page/比爾博·巴金斯.md "wikilink")（Bilbo Baggins），佛羅多·巴金斯的叔叔
  - [佛羅多·巴金斯](../Page/佛羅多·巴金斯.md "wikilink")（Frodo Baggins）
  - [皮聘](../Page/皮瑞格林·圖克.md "wikilink")（Peregrin Took "Pippin"），佛羅多的表親
  - [梅里](../Page/梅里雅達克·烈酒鹿.md "wikilink")（Meriadoc Brandybuck
    "Merry"），佛羅多的親戚
  - [山姆](../Page/山姆衛斯·詹吉.md "wikilink")（Samwise Gamgee "Sam" ），佛羅多的

## 靈感

夏爾的靈感可能源自於作者托爾金童年時曾住過的\[4\]；他曾在受訪時表示「夏爾很像我童年記事時所處的那個世界」\[5\]。

## 電影改編

[左](https://zh.wikipedia.org/wiki/File:Hobbit_holes_reflected_in_water.jpg "fig:左")的哈比屯場景。\]\]
在[彼得·傑克森執導的](../Page/彼得·傑克森.md "wikilink")《[魔戒電影三部曲](../Page/魔戒電影三部曲.md "wikilink")》裡，夏爾出現於第一部《[魔戒首部曲：魔戒現身](../Page/魔戒首部曲：魔戒現身.md "wikilink")》和第三部《[魔戒三部曲：王者再臨](../Page/魔戒三部曲：王者再臨.md "wikilink")》。1999年，拍攝團隊於[紐西蘭的](../Page/紐西蘭.md "wikilink")開始搭建夏爾哈比屯的場景\[6\]，但隨著電影拍攝結束夏爾的場景也隨之拆除。不過由於魔戒電影系列的名氣，還是讓該地成為知名觀光景點。

2012年彼得·傑克森開始拍攝的魔戒前傳《[哈比人電影系列](../Page/霍比特人电影系列.md "wikilink")》裡，夏爾再度出現在《[哈比人：意外旅程](../Page/霍比特人：意外旅程.md "wikilink")》片頭和《[哈比人：五軍之戰](../Page/哈比人：五軍之戰.md "wikilink")》結尾。夏爾取景地依舊在馬塔馬塔的同一處，由木材和石頭的堅固材質建造。在電影結束拍攝後，該處即作為永久的旅遊景點「[哈比村](../Page/哈比村.md "wikilink")」（Hobbiton
Movie Set）開放給來自全球的遊客，為紐西蘭帶來觀光收入\[7\]。

## 參考資料

<references />

## 外部連結

  - [夏爾](http://tolkiengateway.net/wiki/The_Shire)在Tolkien Gateway上的條目
  - [哈比村官網](http://www.hobbitontours.com/en/)

[de:Regionen und Orte in Tolkiens
Welt\#Auenland](../Page/de:Regionen_und_Orte_in_Tolkiens_Welt#Auenland.md "wikilink")
[lb:Länner a Stied aus Middle-earth\#The
Shire](../Page/lb:Länner_a_Stied_aus_Middle-earth#The_Shire.md "wikilink")

[Category:中土大陸的城鎮](../Category/中土大陸的城鎮.md "wikilink")

1.
2.

3.

4.

5.

6.

7.