[GirolamoSavonarola.jpg](https://zh.wikipedia.org/wiki/File:GirolamoSavonarola.jpg "fig:GirolamoSavonarola.jpg")
**吉羅拉莫·薩佛納羅拉**（，）是一位[意大利](../Page/意大利.md "wikilink")[道明會修士](../Page/道明會.md "wikilink")，從1494年到1498年擔任[佛羅倫斯的精神和世俗領袖](../Page/佛羅倫斯.md "wikilink")。他以反對[文藝復興藝術和](../Page/文藝復興.md "wikilink")[哲學](../Page/哲學.md "wikilink")，焚燒藝術品和非宗教類書籍，毀滅被他認為不道德的奢侈品，以及嚴厲的[講道著稱](../Page/講道.md "wikilink")。他的講道往往充滿批評，並直接針對當時的教皇[亞歷山大六世以及](../Page/亞歷山大六世.md "wikilink")[美第奇家族](../Page/美第奇家族.md "wikilink")。薩佛納羅拉因施政嚴苛而被[佛羅倫斯的市民推翻](../Page/佛羅倫斯.md "wikilink")，以[火刑處死](../Page/火刑.md "wikilink")。

## 簡介

薩佛納羅拉生於義大利的[費拉拉](../Page/費拉拉.md "wikilink")，幼年就開始研讀《[聖經](../Page/聖經.md "wikilink")》和[聖托馬斯·阿奎那和](../Page/聖托馬斯·阿奎那.md "wikilink")[亞裏士多德的著作](../Page/亞裏士多德.md "wikilink")。他一開始就讀[法萊拉大學](../Page/法萊拉大學.md "wikilink")，似乎曾獲得高級的藝術或文學類學位。在這個時期，他反對宗教腐敗和世俗享樂的立場就已經在他幾部詩作中體現出來。

薩佛納羅拉在1475年成為道明會的修士，並加入了[波隆那的聖多米尼克修道院](../Page/波隆那.md "wikilink")。1482年，他被派往佛羅倫斯。他第一次佛羅倫斯之行幾無紀錄，五年之後他沒沒無聞的回到波隆那繼續研修[神學](../Page/神學.md "wikilink")。

1490年薩佛納羅拉被皮寇·德拉·[米蘭多拉伯爵邀請](../Page/米蘭多拉.md "wikilink")，再次回到佛羅倫斯。這一次他著重於末世的講道開始漸漸得到重視，除了宣揚聖經內容，他還經常聲稱自己得到上帝的[神諭](../Page/神諭.md "wikilink")，可以跟[上帝或者](../Page/上帝.md "wikilink")[聖人直接交談](../Page/聖人.md "wikilink")。這樣激烈的講道在當時並非罕見，但是接下來幾年發生的一系列歷史事件，有些可以用他的[預言來解釋](../Page/預言.md "wikilink")。比如[意大利戰爭](../Page/意大利戰爭.md "wikilink")、美第奇家族的衰落、[瘟疫爆發等等](../Page/瘟疫.md "wikilink")。文藝復興時期佛羅倫斯富商和貴族在古董和藝術上花費大筆金錢，造成嚴重的貧富不均。薩佛納羅拉這個時候宣揚摒棄人世間財富追求[耶穌基督的精神救贖也博得很多中下層平民的支持](../Page/耶穌基督.md "wikilink")。加之1500年馬上就要來臨，這樣整數的年度給[基督徒造成一種末世的錯覺](../Page/基督徒.md "wikilink")，所以人們就更容易受到[末世論的蠱惑](../Page/末世論.md "wikilink")。薩佛納羅拉在講道中激烈的反對當時的教皇亞歷山大六世，以及佛羅倫斯的主導家族美第奇家族，即便後者是他的資助人。

1494年[法國國王](../Page/法國.md "wikilink")[查理八世入侵佛羅倫斯](../Page/查理八世.md "wikilink")，掌權的美第奇家族被推翻，薩佛納羅拉成為佛羅倫斯新的精神和世俗領袖。他建立了佛羅倫斯宗教共和國，開始各種[清規戒律定為法律](../Page/清規.md "wikilink")，例如禁止[飲酒](../Page/飲酒.md "wikilink")、[賭博甚至是](../Page/賭博.md "wikilink")[下棋](../Page/下棋.md "wikilink")，把原來僅僅是罰款的[同性性行為變成可以用](../Page/同性性行為.md "wikilink")[死刑來懲罰的犯罪行為](../Page/死刑.md "wikilink")。他最有名的政績則是在1497年，他和一群跟隨者們在佛羅倫斯市政廳廣場點起一堆熊熊大火，薩佛納羅拉稱之為“[虛榮之火](../Page/虛榮之火.md "wikilink")”。他派遣兒童逐家逐戶搜集“世俗享樂物品”，包括：鏡子、化妝品、畫像、異教書籍、非[天主教主題雕塑](../Page/天主教.md "wikilink")、[賭博遊戲器具](../Page/賭博.md "wikilink")、[西洋棋](../Page/西洋棋.md "wikilink")、[魯特琴和其他](../Page/魯特琴.md "wikilink")[樂器](../Page/樂器.md "wikilink")、做工精細的衣著、女人的帽子和所有古典詩作，然後把搜集起來的這些東西一並扔進火裏燒掉。很多文藝復興時期偉大的藝術品都被這堆火永遠的燒掉了。曾經熱愛異教主題的著名文藝復興畫家[桑德罗·波提切利](../Page/桑德罗·波提切利.md "wikilink")，晚年也沈溺於薩佛納羅拉的[講道](../Page/講道.md "wikilink")，親自把很多晚期作品扔進火裏。

佛羅倫斯人很快厭倦了薩佛納羅拉的嚴厲，部分原因也在於薩佛納羅拉反對商業經營，以商業為主的佛羅倫斯一直陷於貧困之中。1500年越来越近也不見[世界末日來臨](../Page/世界末日.md "wikilink")，更沒有[救世主拯救佛羅倫斯人民](../Page/救世主.md "wikilink")，1497年5月4日，一群人在薩佛納羅拉講道時生事起鬨，隨後很快演變成[民變](../Page/民變.md "wikilink")。薩佛納羅拉的跟隨者漸漸離開，酒館再次開放，人們開始公開賭博。

1497年5月13日，教皇亞歷山大六世把薩佛納羅拉處以[破門律](../Page/破門律.md "wikilink")。1498年教皇公開要求佛羅倫斯逮捕並處死薩佛納羅拉。4月8日，一群人襲擊了[佛羅倫斯聖瑪爾谷大殿](../Page/佛羅倫薩聖瑪爾谷大殿.md "wikilink")，在血腥廝殺之後，薩佛納羅拉帶領兩位跟隨的修士多米尼克·達·帕奇亞和薩爾維斯特羅投降。薩佛納羅拉被指控信仰[異教](../Page/異教.md "wikilink")、偽造[預言](../Page/預言.md "wikilink")、妨礙治安等等罪名。

隨後的幾個星期，薩佛納羅拉和兩名跟隨者經受了嚴酷的折磨[刑求](../Page/刑求.md "wikilink")。行刑人故意留下他的右手完整，這樣他才能簽署認罪書。最終三個人都簽下[自白](../Page/自白.md "wikilink")。與此同時，薩佛納羅拉還完成了兩篇[默想](../Page/默想.md "wikilink")，大致是說他的肉體如何脆弱，無法經受折磨所以承認了沒有犯下的罪行云云。1498年5月23日，他和另外兩名修士被同時處以[火刑](../Page/火刑.md "wikilink")。就在薩佛納羅拉當初點燃“虛榮之火”的地方，他自己也被燒成灰燼。他死後不久，[美第奇家族再次掌權](../Page/美第奇家族.md "wikilink")，推翻共和國，自封為[托斯卡納大公](../Page/托斯卡納大公.md "wikilink")。

## 參考文獻

  - *The House of Medici: Its Rise and Fall*, Christopher Hibbert,
    Harper Perennial, 1999.
  - *Botticelli: Life and Work*, Ronald Lightbown, Abbeville Press,
    1989.
  - *Deeper Experiences of Famous Christians*, James Lawson, Warner
    Press, 1911, pp. 73–84.
  - *Bonfire Songs: Savonarola's Musical Legacy* (1998), Patrick Macey,
    Clarendon Press, Oxford
  - [New York Times](../Page/New_York_Times.md "wikilink"), *Savonarola,
    Second Lecture of the Course by Dr. Lord at Association Hall*,
    January 10, 1871, pp. 2–3.

[Category:義大利基督徒](../Category/義大利基督徒.md "wikilink")
[Category:15世纪意大利人](../Category/15世纪意大利人.md "wikilink")
[Category:被火刑處決者](../Category/被火刑處決者.md "wikilink")
[Category:意大利罗马天主教徒](../Category/意大利罗马天主教徒.md "wikilink")