**铜川市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[陕西省下辖的](../Page/陕西省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于陕西省中部，[渭河平原北部](../Page/渭河平原.md "wikilink")，是[关中经济带的重要组成部分](../Page/关中.md "wikilink")，是关中通往陕北的交通要道，素有“渭北之古邑，延南之旧戍”之称。全市面积3927平方千米，是陕西省面积最小的地级市，人口82万，其中工业人口占53％。

铜川原先主要产业是[煤矿](../Page/煤.md "wikilink")，由于面临资源枯竭和其他问题而转型和迁移行政中心。

## 历史沿革

[黄绿釉狮形琉璃烛插2，铜川.jpg](https://zh.wikipedia.org/wiki/File:黄绿釉狮形琉璃烛插2，铜川.jpg "fig:黄绿釉狮形琉璃烛插2，铜川.jpg")
[北魏](../Page/北魏.md "wikilink")[太平真君七年](../Page/太平真君.md "wikilink")（446年）设铜官县，属[北地郡](../Page/北地郡.md "wikilink")。[北周](../Page/北周.md "wikilink")[建德四年](../Page/建德_\(北周\).md "wikilink")（575年），改名同官县，属[宜州](../Page/宜州.md "wikilink")。[五代](../Page/五代.md "wikilink")[后唐](../Page/后唐.md "wikilink")[同光三年](../Page/同光.md "wikilink")（925年）起至[清代](../Page/清代.md "wikilink")，均隶属[耀州](../Page/耀州.md "wikilink")。

[民国](../Page/民国.md "wikilink")35年（1946年），因同官县与“潼关”同音，治所又设在铜水之川，改称铜川县。1949年，同官县归三原分区行政督察专员公署领导。1950年，改属咸阳行政公署。1953年改由省人民政府直接领导。1958年4月5日，撤销铜川县，成立铜川市。同年11月，撤销[富平县](../Page/富平县.md "wikilink")、耀县（今[耀州区](../Page/耀州区.md "wikilink")）建制，将两县的行政区域及宜君县部分并入铜川市。1961年恢复富平县、耀县建制，铜川市改属渭南行政公署。

1966年，铜川市再改为省辖市。1980年1月，将[蒲城县的广阳](../Page/蒲城县.md "wikilink")、高楼河、阿庄、肖家堡4个乡和耀县划归铜川市，同年5月，设置城区、郊区。1983年10月，又将宜君县划归铜川市。1992年7月，建立铜川新区，并于1993年升为省级经济技术开发区。2000年3月，铜川市城区更名为王益区，铜川市郊区更名为印台区。2002年，撤销耀县，设立[耀州区](../Page/耀州区.md "wikilink")。2003年，市政府正式迁驻铜川新区正阳路。2009年3月，成为国务院公布的资源枯竭型城市。

## 地理

### 位置与交通

铜川位于陕西省中部，地处关中经济带，介于东经108°34′至109°29′、北纬34°50′至35°34′之间。距[西安市区约](../Page/西安.md "wikilink")68公里，西安至[黄陵高速公路穿境而过](../Page/黄陵.md "wikilink")，境内有[咸铜铁路](../Page/咸铜铁路.md "wikilink")、[梅七铁路两条支线铁路](../Page/梅七铁路.md "wikilink")。

### 气候

属于[温带季风气候](../Page/温带季风气候.md "wikilink")。

### 自然资源

铜川拥有林地面积244.45万亩，牧草地152.07万亩。矿产资源丰富，其中煤炭储量30多亿吨，煤炭产量占全省总产量的三分之一。农业方面以[苹果产量最佳](../Page/苹果.md "wikilink")，苹果种植面积40万亩。铜川水系属[渭河水系](../Page/渭河.md "wikilink")，有石川河、洛河两条支流，[水资源总量](../Page/水资源.md "wikilink")22042万立方米，其中[地下水](../Page/地下水.md "wikilink")12607万立方米，已探明的地下水可开采量4494万立方米。

## 政治

### 现任领导

<table>
<caption>铜川市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党铜川市委员会.md" title="wikilink">中国共产党<br />
铜川市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/铜川市人民代表大会.md" title="wikilink">铜川市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/铜川市人民政府.md" title="wikilink">铜川市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议铜川市委员会.md" title="wikilink">中国人民政治协商会议<br />
铜川市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p>杨长亚[1]</p></td>
<td><p><a href="../Page/马秉寅.md" title="wikilink">马秉寅</a>[2]</p></td>
<td><p>李智远[3]</p></td>
<td><p><a href="../Page/张惠荣.md" title="wikilink">张惠荣</a>[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p>陕西商洛</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/曹县.md" title="wikilink">曹县</a></p></td>
<td><p>陕西扶风</p></td>
<td><p>陕西省<a href="../Page/神木市.md" title="wikilink">神木市</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年4月</p></td>
<td><p>2017年2月</p></td>
<td><p>2018年4月</p></td>
<td><p>2017年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市下辖3个[市辖区](../Page/市辖区.md "wikilink")、1个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[王益区](../Page/王益区.md "wikilink")、[印台区](../Page/印台区.md "wikilink")、[耀州区](../Page/耀州区.md "wikilink")
  - 县：[宜君县](../Page/宜君县.md "wikilink")

铜川新区在耀州的中心一带，该新区是由陕西省批准成立的经济开发区，而非民政部正式批准建立[县级行政区](../Page/县级行政区.md "wikilink")，因此也可以认为铜川市政府现在驻耀州区。

<table>
<thead>
<tr class="header">
<th><p><strong>铜川市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>610200</p></td>
</tr>
<tr class="odd">
<td><p>610202</p></td>
</tr>
<tr class="even">
<td><p>610203</p></td>
</tr>
<tr class="odd">
<td><p>610204</p></td>
</tr>
<tr class="even">
<td><p>610222</p></td>
</tr>
<tr class="odd">
<td><p>注：耀州区数字包含铜川新区（铜川经济开发区）所辖2街道。</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>铜川市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>铜川市</p></td>
<td><p>834437</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>王益区</p></td>
<td><p>200231</p></td>
<td><p>24.00</p></td>
</tr>
<tr class="even">
<td><p>印台区</p></td>
<td><p>217509</p></td>
<td><p>26.07</p></td>
</tr>
<tr class="odd">
<td><p>耀州区</p></td>
<td><p>325537</p></td>
<td><p>39.01</p></td>
</tr>
<tr class="even">
<td><p>宜君县</p></td>
<td><p>91160</p></td>
<td><p>10.92</p></td>
</tr>
<tr class="odd">
<td><p>注：耀州区的常住人口数据中包含铜川新区73678人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")834437人\[8\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加26381人，增长3.26%。年平均增长率为0.32%。其中，男性人口为432203人，占51.8%；女性人口为402234人，占48.2%。总人口性别比（以女性为100）为107.45。0－14岁人口为109099人，占13.07%；15－64岁人口为647197人，占77.56%；65岁及以上人口为78141人，占9.36%。

## 经济

全市经济以工业为主，传统产业为[煤炭开采](../Page/煤炭.md "wikilink")，全市煤炭年产量超过2000万吨。[水泥产业亦是重要支柱](../Page/水泥.md "wikilink")。近年正积极发展电力电源、装备制造、食品、医药等新兴产业，并引进了[华能国际电力等大企业](../Page/华能国际电力.md "wikilink")。

## 交通运输

2014年全社会客运量1823万人，货运量4598万吨。民用车辆拥有量122763辆，其中载客汽车50616辆、载货汽车11462辆、其他汽车7230辆；摩托车43692辆；拖拉机8351辆；挂车1412辆。

## 友好城市

### 国内

  - [广东省](../Page/广东省.md "wikilink")[湛江市](../Page/湛江市.md "wikilink")
  - [山东省](../Page/山东省.md "wikilink")[烟台市](../Page/烟台市.md "wikilink")
  - [江苏省](../Page/江苏省.md "wikilink")[徐州市](../Page/徐州市.md "wikilink")
  - [福建省](../Page/福建省.md "wikilink")[泉州市](../Page/泉州市.md "wikilink")
  - [内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[乌海市](../Page/乌海市.md "wikilink")

### 国外

  - [大韩民国](../Page/大韩民国.md "wikilink")[庆尚北道](../Page/庆尚北道.md "wikilink")[奉化郡](../Page/奉化郡.md "wikilink")

## 参见

  - [耀州窑](../Page/耀州窑.md "wikilink")

## 参考资料

## 外部链接

[Category:铜川](../Category/铜川.md "wikilink")
[Category:陕西地级市](../Category/陕西地级市.md "wikilink")
[Category:关中城市群](../Category/关中城市群.md "wikilink")
[Category:中华人民共和国第二批资源枯竭型城市](../Category/中华人民共和国第二批资源枯竭型城市.md "wikilink")
[Category:陕西地理之最](../Category/陕西地理之最.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.