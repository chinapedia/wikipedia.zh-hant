**环丁烯**是四个碳的[环烯烃](../Page/环烯烃.md "wikilink")，分子式为C<sub>4</sub>H<sub>6</sub>。

## 参见

  - [环丁烷](../Page/环丁烷.md "wikilink")
  - [环丁二烯](../Page/环丁二烯.md "wikilink")

## 外部链接

  - [环丁烯的合成](http://www.orgsyn.org/orgsyn/prep.asp?prep=cv7p0117)

[Category:环烯烃](../Category/环烯烃.md "wikilink")
[Category:四元环](../Category/四元环.md "wikilink")