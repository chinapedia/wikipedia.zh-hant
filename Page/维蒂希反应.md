**维蒂希反应**（**Wittig反应**）是[醛或](../Page/醛.md "wikilink")[酮与三苯基](../Page/酮.md "wikilink")[磷鎓內鹽](../Page/磷鎓內鹽.md "wikilink")（维蒂希试剂）作用生成[烯烃和](../Page/烯烃.md "wikilink")[三苯基氧膦的一类有机化学反应](../Page/三苯基氧膦.md "wikilink")，以发明人[德国化学家](../Page/德国.md "wikilink")[格奥尔格·维蒂希的](../Page/格奥尔格·维蒂希.md "wikilink")[姓氏命名](../Page/姓氏.md "wikilink")。\[1\]\[2\]

  -

      -
        [Wittig_Reaktion.svg](https://zh.wikipedia.org/wiki/File:Wittig_Reaktion.svg "fig:Wittig_Reaktion.svg")

[格奥尔格·维蒂希在](../Page/格奥尔格·维蒂希.md "wikilink")1954年发现该反应，并因此获得1979年[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。\[3\]\[4\]\[5\]
维蒂希反应在烯烃合成中有十分重要的地位。

维蒂希反应的反应物一般是醛/酮和单取代的磷[鎓內鹽](../Page/鎓內鹽.md "wikilink")。使用活泼叶立德时所得产物一般都是*Z*型的，或Z/E异构体比例相当；而使用比较稳定的叶立德时，或在[Schlosser改进的条件下](../Page/#Schlosser改进.md "wikilink")，产物则以*E*型为主。

## 反应机理

### 经典机理

维蒂希反应的经典机理为：
[Wittig_Reaction_Mechanism.png](https://zh.wikipedia.org/wiki/File:Wittig_Reaction_Mechanism.png "fig:Wittig_Reaction_Mechanism.png")
磷叶立德**1**中的电负性碳进攻与醛酮羰基**2**中的碳原子，发生[亲核加成](../Page/亲核加成.md "wikilink")。由于位阻原因，主要生成Ph<sub>3</sub>P<sup>+</sup>-和-O<sup>−</sup>处于反式的产物**3**。**3**C-C键旋转得到[偶极中间体](../Page/偶极中间体.md "wikilink")**4**。**4**在-78°C时比较稳定。然后生成含氧四元环过渡态**5**。**5**发生[消除得到顺式烯烃](../Page/消除反应.md "wikilink")**7**和[三苯基氧膦](../Page/三苯基氧膦.md "wikilink")**6**。

对于活泼的维蒂希试剂而言，与醛和酮反应时第一步的速率都较快，但第三步成环反应速率较慢，是[速控步](../Page/速控步.md "wikilink")。但对于[稳定的叶立德而言](../Page/#稳定的维蒂希试剂.md "wikilink")，R<sub>1</sub>基团可以稳定碳上的负电荷，第一步是速控步。因此总体的成烯反应速率减小，而且生成的烯烃中[E型比例较大](../Page/E型.md "wikilink")。这也是不活泼的维蒂希试剂与有[位阻的酮反应很慢的缘故](../Page/位阻.md "wikilink")。

### 近期研究

最近的研究表明，以上的机理并不能解释所有的实验数据。目前主要用[核磁共振谱来研究活泼维蒂希试剂的](../Page/核磁共振.md "wikilink")[反应中间体](../Page/反应中间体.md "wikilink")。但是对于偶极中间体（**3a**和**3b**）是否存在及它们之间的相互转换，现在仍有争议。\[6\]
有证据显示磷叶立德**1**可以与[羰基化合物](../Page/羰基化合物.md "wikilink")**2**发生π²s/π²a
\[2+2\][环加成反应](../Page/环加成反应.md "wikilink")，直接生成含氧的四元环**4a**和**4b**，并且产物**5**的[立体化学与叶立德](../Page/立体化学.md "wikilink")**1**和**2**羰基的加成以及**4a**和**4b**中间体之间的平衡有关。\[7\]\[8\]\[9\][布魯斯·瑪麗安諾夫](../Page/布魯斯·瑪麗安諾夫.md "wikilink")（Bruce
Maryanoff）和Reitz研究了它们之间的平衡，将其称为“立体化学移动”（Stereochemical drift）。

多年以来，维蒂希反应的立体选择性一直被认为与产物烯烃的Z/E标记有关，然而有一些反应物却不遵守这样简单的规律。而且[锂盐对反应的立体化学似乎也有复杂的影响](../Page/锂.md "wikilink")。\[10\]

[Wittig_Reaction_Mechanism_Update.png](https://zh.wikipedia.org/wiki/File:Wittig_Reaction_Mechanism_Update.png "fig:Wittig_Reaction_Mechanism_Update.png")

[脂肪](../Page/脂肪族化合物.md "wikilink")[醛和](../Page/醛.md "wikilink")[芳香醛](../Page/芳香化合物.md "wikilink")，以及脂肪族和芳香族的鏻盐在发生维蒂希反应时也有显著的不同。Vedejs等人已经表明直链醛在无锂盐存在时没有可逆反应，整个反应是[动力学控制的](../Page/动力学控制.md "wikilink")。\[11\]\[12\]
Vedejs也因此提出一套理论来解释活泼和稳定维蒂希反应之间的差别。\[13\]

## 维蒂希试剂

### 简单维蒂希试剂的制备

维蒂希试剂（Wittig）通常以四级[鏻盐在强碱作用下失去一分子](../Page/鏻盐.md "wikilink")[卤化氢制备](../Page/卤化氢.md "wikilink")，而鏻盐则可由[三苯基膦和](../Page/三苯基膦.md "wikilink")[卤代烃反应得到](../Page/卤代烃.md "wikilink")。前者制备反应通常在[乙醚或](../Page/乙醚.md "wikilink")[四氢呋喃中进行](../Page/四氢呋喃.md "wikilink")，强碱选用[苯基锂或](../Page/苯基锂.md "wikilink")[正丁基锂](../Page/正丁基锂.md "wikilink")。

  -
    Ph<sub>3</sub>P<sup>+</sup>−CH<sub>2</sub>−R X<sup>−</sup> +
    [C<sub>4</sub>H<sub>9</sub>Li](../Page/正丁基锂.md "wikilink") →
    Ph<sub>3</sub>P=CH−R + LiX +
    [C<sub>4</sub>H<sub>10</sub>](../Page/丁烷.md "wikilink")

最简单的维蒂希试剂是亚甲基三苯基膦（Ph<sub>3</sub>P<sup>+</sup>−C<sup>−</sup>H<sub>2</sub>），是一个橙黄色固体，对空气和水都不稳定，可通过三苯基膦和[溴甲烷生成的溴化三苯基甲基鏻](../Page/溴甲烷.md "wikilink")
Ph<sub>3</sub>P<sup>+</sup>-CH<sub>3</sub>·Br<sup>−</sup>
在干燥乙醚和[氮气流下用苯基锂处理失](../Page/氮气.md "wikilink")[溴化氢制得](../Page/溴化氢.md "wikilink")：\[14\]

  -
    Ph<sub>3</sub>P + CH<sub>3</sub>Br →
    Ph<sub>3</sub>P<sup>+</sup>-CH<sub>3</sub>·Br<sup>−</sup>
    -(干燥乙醚,PhLi)→
    Ph<sub>3</sub>P<sup>+</sup>-CH<sub>2</sub><sup>−</sup>

它也是另一种合成维蒂希试剂方法的原料。合成时一般不将它分离出来，而直接进行下一步的反应。

至于取代叶立德，可先用[卤代烃R](../Page/卤代烃.md "wikilink")−CH<sub>2</sub>−X[烷基化Ph](../Page/烷基化.md "wikilink")<sub>3</sub>P=CH<sub>2</sub>，得到一个取代的鏻盐：

  -
    Ph<sub>3</sub>P=CH<sub>2</sub> +
    [R-CH<sub>2</sub>-X](../Page/卤代烃.md "wikilink") →
    Ph<sub>3</sub>P<sup>+</sup>−CH<sub>2</sub>− CH<sub>2</sub>−R
    X<sup>−</sup>

再用C<sub>4</sub>H<sub>9</sub>Li[脱去质子](../Page/去质子化.md "wikilink")，生成Ph<sub>3</sub>P=CH−CH<sub>2</sub>−R。

### 稳定的维蒂希试剂

比较稳定的维蒂希试剂通常含有能够稳定类似[碳负离子的碳上的负电荷的基团](../Page/碳负离子.md "wikilink")，例如在[α碳上含有](../Page/α碳.md "wikilink")[羰基的Ph](../Page/羰基.md "wikilink")<sub>3</sub>P=CH−COOR、Ph<sub>3</sub>P=CH−[Ph](../Page/苯基.md "wikilink")。它们比简单的叶立德要稳定，且一般不与酮反应。对于不活泼叶立德和酮的反应可以参见[Horner-Wadsworth-Emmons反应](../Page/Horner-Wadsworth-Emmons反应.md "wikilink")。

稳定的叶立德可通过用较弱的碱来处理鏻盐制备，比如[醇盐](../Page/醇盐.md "wikilink")，有时也可用[氢氧化钠和](../Page/氢氧化钠.md "wikilink")[碳酸钠](../Page/碳酸钠.md "wikilink")。它们在维蒂希反应中通常生成E型为主的产物，即含羰基基团与β碳原子上较大的基团处于异侧。

### 结构

维蒂希试剂的结构可通过叶立德式，或更常见的含P=C双键的**Phosphorane**式来描述：

  -
    [Wittig_reagent_structure.gif](https://zh.wikipedia.org/wiki/File:Wittig_reagent_structure.gif "fig:Wittig_reagent_structure.gif")

但是该[共振式中](../Page/共振式.md "wikilink")[磷超过了](../Page/磷.md "wikilink")[八隅律的要求](../Page/八隅律.md "wikilink")。这个[超价性质尚不能用经典理论解释](../Page/超价.md "wikilink")。此外其共振倾向也不如[烯烃和](../Page/烯烃.md "wikilink")[亚胺中p](../Page/亚胺.md "wikilink")-p轨道交盖的[π键强烈](../Page/π键.md "wikilink")，意味着叶立德式对杂化体的贡献较大，碳原子具有[亲核性](../Page/亲核性.md "wikilink")。

## 应用和限制

由于应用性广泛，维蒂希反应已经成为[烯烃合成的重要方法](../Page/烯烃.md "wikilink")。它与[消除反应](../Page/消除反应.md "wikilink")（例如[卤代烃的](../Page/卤代烃.md "wikilink")[脱卤化氢反应](../Page/脱卤化氢反应.md "wikilink")）不同的是，消除反应得到由[查依采夫规则决定的](../Page/查依采夫规则.md "wikilink")[结构异构体的混合物](../Page/结构异构体.md "wikilink")，而维蒂希反应得到双键固定的烯烃。

很多[醛和](../Page/醛.md "wikilink")[酮都可发生该反应](../Page/酮.md "wikilink")，但[羧酸衍生物](../Page/羧酸衍生物.md "wikilink")（如[酯](../Page/酯.md "wikilink")）反应性不强。因此大多数情况下，单、二和三取代的烯烃都可以较高产率通过该反应制得。[羰基化合物可以带着](../Page/羰基化合物.md "wikilink")[-OH](../Page/羟基.md "wikilink")、[-OR](../Page/烷氧基.md "wikilink")、芳香[-NO<sub>2</sub>甚至](../Page/硝基.md "wikilink")[酯基官能团进行反应](../Page/酯.md "wikilink")。

有[位阻的酮类反应效果不理想](../Page/位阻.md "wikilink")，反应较慢且产率不高，尤其是在与稳定的叶立德反应时。可以用[Horner-Wadsworth-Emmons反应来弥补这个不足](../Page/Horner-Wadsworth-Emmons反应.md "wikilink")。而且该反应对不稳定的[醛类也不是很适合](../Page/醛.md "wikilink")，包括易氧化、聚合或分解的醛。在“Tandem氧化维蒂希反应”中，维蒂希反应中的醛是由相应的[醇在原地氧化获得的](../Page/醇.md "wikilink")。\[15\]

由于以二级卤代烷作原料生成鏻盐的产率很低，因此维蒂希试剂一般由一级卤代烷反应得到。这意味着四取代的烯烃最好通过其它方法来制取。但维蒂希试剂对很多基团都有很好的耐受性，包括烯烃、[芳香环](../Page/芳香环.md "wikilink")、[醚类甚至](../Page/醚.md "wikilink")[酯基和与叶立德](../Page/酯.md "wikilink")[共轭的C](../Page/共轭系统.md "wikilink")=O和[氰基](../Page/氰基.md "wikilink")。含两个P=C键的双叶立德也已成功制得并应用于反应中。

此外还有一个与产物[立体化学相关的限制](../Page/立体化学.md "wikilink")。对于简单的叶立德，产物主要是[Z型](../Page/Z型.md "wikilink")，用酮反应时E型比例高些。而当反应在[DMF中和](../Page/二甲基甲酰胺.md "wikilink")[LiI或](../Page/碘化锂.md "wikilink")[NaI存在下反应时](../Page/碘化钠.md "wikilink")，产物却几乎全都是Z型的。\[16\]
这种情况下可以通过[Schlosser改进来获得E型产物](../Page/#Schlosser改进.md "wikilink")。对于稳定的叶立德和[Horner-Wadsworth-Emmons反应](../Page/Horner-Wadsworth-Emmons反应.md "wikilink")，产物主要为E型。

## Schlosser改进

[Schlosser_Wittig.gif](https://zh.wikipedia.org/wiki/File:Schlosser_Wittig.gif "fig:Schlosser_Wittig.gif")
传统维蒂希反应的主要限制在于，反应主要经由[赤型的偶极中间体](../Page/赤型.md "wikilink")，从而导致Z型烯烃的生成。但Schlosser和Christmann\[17\]
发现，在低温及[苯基锂和](../Page/苯基锂.md "wikilink")[HCl的存在下](../Page/氯化氢.md "wikilink")，[苏型的中间体占主要地位](../Page/苏型.md "wikilink")，因此主要产物为[E型烯烃](../Page/E型.md "wikilink")。

[科里和山本进一步发现](../Page/艾里亚斯·詹姆斯·科里.md "wikilink")，通过用偶极中间体叶立德与一个二级醛反应，该改进可以被用于[烯丙醇的](../Page/烯丙醇.md "wikilink")[立体选择性合成](../Page/立体选择性.md "wikilink")。\[18\]
例如：

[Corey_Schlosser_Wittig.png](https://zh.wikipedia.org/wiki/File:Corey_Schlosser_Wittig.png "fig:Corey_Schlosser_Wittig.png")

## 应用举例

[Wittig_CH2_examples.png](https://zh.wikipedia.org/wiki/File:Wittig_CH2_examples.png "fig:Wittig_CH2_examples.png")

由于该反应具有很强的应用性，因此它已经成为有机合成化学家十分重要的工具。\[19\]

最常见的应用，即是用亚甲基三苯基膦（Ph<sub>3</sub>P=CH<sub>2</sub>）向分子中引入[亚甲基](../Page/亚甲基.md "wikilink")。在上面的例子中，即便是[樟脑这种有位阻的酮](../Page/樟脑.md "wikilink")，都可通过与甲基三苯基溴化鏻和[叔丁醇钾共热](../Page/叔丁醇钾.md "wikilink")（产生维蒂希试剂）而被转化为其亚甲基衍生物。\[20\]
在另外一个例子中，以[氨基钠作为碱产生叶立德](../Page/氨基钠.md "wikilink")，成功以62%的产率将反应物[醛转化为烯烃](../Page/醛.md "wikilink")**I**。\[21\]
这个反应是低温在[THF中进行的](../Page/THF.md "wikilink")，比较敏感的[硝基](../Page/硝基.md "wikilink")、[偶氮基和](../Page/偶氮.md "wikilink")[酚盐负离子都没有干扰反应](../Page/酚盐负离子.md "wikilink")。产物可用作[聚合物的光稳定剂](../Page/聚合物.md "wikilink")，防止聚合物被[紫外线破坏](../Page/紫外线.md "wikilink")。

[白三烯A甲酸酯的合成中也涉及到了维蒂希反应](../Page/白三烯.md "wikilink")。\[22\]\[23\]
第一步使用了一个稳定的叶立德，其中羰基与叶立德共轭以防止自身缩合，但还是意外地得到了顺式为主的产物。第二个维蒂希反应则使用的是一个活泼的维蒂希试剂，产物也主要是顺式。需要注意的是[环氧化合物和](../Page/环氧化合物.md "wikilink")[酯都没有对反应造成干扰](../Page/酯.md "wikilink")。

[LeukotrieneA_synthesis.png](https://zh.wikipedia.org/wiki/File:LeukotrieneA_synthesis.png "fig:LeukotrieneA_synthesis.png")

[甲氧基亚甲基三苯基膦是一个维蒂希试剂](../Page/甲氧基亚甲基三苯基膦.md "wikilink")，可用于醛的[同系化反应](../Page/同系化反应.md "wikilink")。

## 参见

  - [Corey-Chaykovsky试剂](../Page/Corey-Chaykovsky试剂.md "wikilink")
  - [Horner-Wadsworth-Emmons反应](../Page/Horner-Wadsworth-Emmons反应.md "wikilink")
  - [Julia-Lythgoe烯烃合成](../Page/Julia-Lythgoe烯烃合成.md "wikilink")
  - [Peterson烯烃合成](../Page/Peterson烯烃合成.md "wikilink")
  - [特伯试剂](../Page/特伯试剂.md "wikilink")
  - [有机磷化学](../Page/有机磷化学.md "wikilink")
  - [同系化反应](../Page/同系化反应.md "wikilink")

## 参考资料

## 外部链接

  - 维蒂希反应—[Organic Syntheses](../Page/Organic_Syntheses.md "wikilink"),
    Coll. Vol. 10, p. 703 (2004); Vol. 75, p. 153 (1998).
    ([文章](http://www.orgsynth.org/orgsyn/prep.asp?prep=v75p0153))
  - 维蒂希反应—[Organic Syntheses](../Page/Organic_Syntheses.md "wikilink"),
    Coll. Vol. 5, p. 361 (1973); Vol. 45, p. 33 (1965).
    ([文章](http://www.orgsynth.org/orgsyn/prep.asp?prep=cv5p0361))

[Category:人名反应](../Category/人名反应.md "wikilink")
[Category:碳－碳键形成反应](../Category/碳－碳键形成反应.md "wikilink")
[Category:德国发明](../Category/德国发明.md "wikilink")

1.
2.
3.  Maercker, A. *Org. React.* **1965**, *14*, 270-490. (Review)
4.  W. Carruthers, *Some Modern Methods of Organic Synthesis*, Cambridge
    University Press, Cambridge, UK, 1971, pp81-90. (ISBN 0-521-31117-9)
5.
6.
7.  B. E. Maryanoff, A. B. Reitz, M. S. Mutter, R. R. Inners, and H. R.
    Almond, Jr., "Detailed Rate Studies on the Wittig Reaction of
    Non-Stabilized Phosphorus Ylides via <sup>31</sup>P, <sup>1</sup>H,
    and <sup>13</sup>C NMR Spectroscopy. Insight into Kinetic vs.
    Thermodynamic Control of Stereochemistry", J. Am. Chem. Soc.,
    **107**, 1068-1070 (1985)
8.  B. E. Maryanoff, A. B. Reitz, D. W. Graden, and H. R. Almond, Jr.,
    "NMR Rate Study on the Wittig Reaction of 2,2-Dimethylpropanal and
    Tributylbutylidene-phosphorane", Tetrahedron Lett., **30**,
    1361-1364 (1989)
9.  B. E. Maryanoff, A. B. Reitz, M. S. Mutter, R. R. Inners, H. R.
    Almond, Jr., R. R. Whittle, and R. A. Olofson, "Stereochemistry and
    Mechanism of the Wittig Reaction. Diastereomeric Reaction
    Intermediates and Analysis of the Reaction Course", J. Am. Chem.
    Soc., **108**, 7664-7678 (1986)
10. A. B. Reitz, S. O. Nortey, A. D. Jordan, Jr., M. S. Mutter, and B.
    E. Maryanoff, "Dramatic Concentration Dependence of Stereochemistry
    in the Wittig Reaction. Examination of the Lithium-Salt Effect", J.
    Org. Chem., **51**, 3302-3308 (1986)
11.
12.
13. Vedejs, E.; Peterson, M. J. *Top. Stereochem.* **1994**, *21*, 1.
14. 邢其毅等。《基础有机化学》第三版上册。北京：高等教育出版社，2005年。ISBN 7-04-016637-2
15. Richard J. K. Taylor, Leonie Campbell, and Graeme D. McAllister
    (2008). "[(±) trans-3,3'-(1,2-Cyclopropanediyl)bis-2-(E)-propenoic
    Acid, Diethyl Ester: Tandem Oxidation Procedure (TOP) using MnO2
    Oxidation-Stabilized Phosphorane
    Trapping](http://www.orgsyn.org/orgsyn/pdfs/V85P0015.pdf) ". *[Org.
    Synth.](../Page/Org._Synth..md "wikilink")* **85**: 15-26.
16.
17.
18.
19.
20. Fitjer, L.; Quabeck, U. *Synthetic Communications* **1985**,
    *15(10)*, 855-864.
21.
22.
23.