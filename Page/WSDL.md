**WSDL**（Web服务描述语言，Web Services Description
Language）是为描述[Web服务发布的](../Page/Web服务.md "wikilink")[XML格式](../Page/XML.md "wikilink")。W3C组织（[World
Wide Web
Consortium](../Page/World_Wide_Web_Consortium.md "wikilink")）没有批准1.1版的WSDL，当前的WSDL版本是2.0，是W3C的推荐标准（recommendation）（一种官方标准），并将被W3C组织批准为正式标准。

在诸多技术文献中通常将Web服务描述语言简写为**WSDL**，读音通常发为："wiz-dəl"。

WSDL描述Web服务的公共接口。这是一个基于XML的关于如何与Web服务通讯和使用的服务描述；也就是描述与目录中列出的Web服务进行交互时需要绑定的协议和信息格式。通常采用抽象语言描述该服务支持的操作和信息，使用的时候再将实际的网络协议和信息格式绑定给该服务。

## 参见

  - [Web服务](../Page/Web服务.md "wikilink")
  - [SOAP](../Page/SOAP.md "wikilink")
  - [WSIF](../Page/WSIF.md "wikilink")
  - [UDDI](../Page/UDDI.md "wikilink")
  - [WS-I Basic Profile](../Page/WS-I_Basic_Profile.md "wikilink")

## 外部链接

  - [WSDL 1.1 Specification](http://www.w3.org/TR/wsdl)
  - [Web Services Description Working
    Group](http://www.w3.org/2002/ws/desc/)
  - [XML protocol activity](http://www.w3.org/2000/xp/)
  - [Online WSDL Validator](http://www.validwsdl.com/)
  - [W3School WSDL教程](http://www.w3school.com.cn/wsdl/index.asp)

[Category:Web服务规范](../Category/Web服务规范.md "wikilink")
[Category:基于XML的标准](../Category/基于XML的标准.md "wikilink")
[Category:W3C标准](../Category/W3C标准.md "wikilink")