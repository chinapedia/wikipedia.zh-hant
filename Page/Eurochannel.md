**Eurochannel**是一家有线卫星电视台。该台最初由Abril集团于1994在巴西创立，之后在2000年被MultiThematiques
Inc（[Canal+集团和](../Page/Canal+.md "wikilink")[Vivendi](../Page/Vivendi.md "wikilink")）收购。四年之后，Noos的高级主管Gustavo
Vainstein买下了Eurochannel。

## Eurochannel 欧洲频道

  - Eurochannel致力于向全世界宣扬欧洲丰富的文化和生活时尚。电视台一周7天每天24小时不间断连续播放百分之一百的欧洲原声节目。到目前为止，Eurochannel通过全球200多家有线和卫星电视提供商放送。
  - Eurochannel节目均配有西班牙和葡萄牙字母，频道在整个拉丁美洲，加拿大和加勒比海地区播放。
  - 如今20个国家，6百万户居民，2千2百万观众能够收看到Eurochannel。并且Eurochannel于2007末在香港和台湾地区播送。
  - Eurochannel带来高品质的电影和电视剧集，时尚艺术信息以及众多欧洲神秘景点的介绍，展现了一幅生动充满活力的当代欧洲画面。

## 节目

### 欧洲电影 Eurocinema

一周3部新电影，每月70多部电影，您有机会领略到诸多欧洲著名演员的表演，诸如[Penélope
Cruz](../Page/Penélope_Cruz.md "wikilink")，[Antonio
Banderas](../Page/Antonio_Banderas.md "wikilink")，[Gérard
Depardieu](../Page/Gérard_Depardieu.md "wikilink")，[Catherine
Deneuve](../Page/Catherine_Deneuve.md "wikilink")，[Marcello
Mastroianni](../Page/Marcello_Mastroianni.md "wikilink")，[Monica
Bellucci](../Page/Monica_Bellucci.md "wikilink")，[Olivier
Martinez](../Page/Olivier_Martinez.md "wikilink")，[Géraldine
Chaplin](../Page/Géraldine_Chaplin.md "wikilink")，[Anthony
Hopkins](../Page/Anthony_Hopkins.md "wikilink")，等等。

  - 影集专题：捆绑播放4部来自同一导演，同一演员或相同主题的电影
  - 主题夜晚：为纪念一名伟大的演员和导演，播送其相关电影和纪录片。
  - 怀旧夜晚：播放欧洲最著名的经典电影
  - 小型剧场：“Balzac, une vie de passion”，演员阵容：Gerard Depardieu和Fanny
    Ardant； “L’Enfant des Lumieres”，演员阵容Nathalie Baye；“Rastignac
    ”，演员阵容[Jocelyn
    Quivrin和](../Page/Jocelyn_Quivrin.md "wikilink")[Flannan
    Obé](../Page/Flannan_Obé.md "wikilink")；“Les Miserables”，演员阵容Gerard
    Depardieu，Jeanne Moreau 和John Malkovich；“Le Miroir de
    l’Eau”，演员阵容Line Renaud和Cristiana Reali。

### 欧洲剧集 Euroseires

  - [辦公室笑雲](../Page/辦公室笑雲.md "wikilink")（[*The
    Office*](../Page/办公室风云.md "wikilink")），这是一部震惊英国电视界的连续剧。这部真人秀将故事发生地搬到了那些白领们相当熟悉的办公室场景之中。它获得了2004年金球奖最佳戏剧类连续剧和最佳戏剧类表演奖。
  - 《[冤家成双对](../Page/冤家成双对.md "wikilink")》，这部精彩的电视剧通过简短幽默的对话，展现了一群喜欢在伦敦酒吧里放松消磨时光的年轻人的生活，以及他们之间的错综复杂的关系。
  - *[H](../Page/H.md "wikilink")*：一部充满风趣的法国喜剧，故事发生在一间由疯狂教授Strauss经营的外科整形诊所中。。。
  - *Manchild*：这是一部极具幽默感的电视剧，它描述了四个三十多岁的人绝望的尝试一切可能的方式，试图重回青春的故事。
  - *[Clara Sheller](../Page/Clara_Sheller.md "wikilink")*：如同“Sex and
    the City”，Clara Sheller
    是一部城市性感女性剧集。该片充满了幽默。这部连续剧荣获2005年Luchon电视节最佳电视剧殊荣。
  - *[Vénus et
    Apollon](../Page/Vénus_et_Apollon.md "wikilink")*：继电影Venus
    Beauté的成功之后，现在终于出现了电视剧版本。在一家安逸粉红色的美容院里，男人和女人相互观察，相互安慰，他们坦白各自的爱情与幻想，并且向对方倾诉。
  - *Le septième ciel*：（2005），由RTBF
    制作的比利时连续剧描述了一群报社编辑们的日常生活。剧中描绘了他们的工作与成功，同时也刻画了他们的失败与艰辛。
  - *People like us*（1999），英国连续剧。“People Like us”的中心角色Roy Mallard由Chris
    Langham饰演。Roy是一个很具天赋的研究员。他来到英格兰，拍摄以平常人为主角的电影。
  - *Burn it*（2003）英国连续剧。
  - *Fans United*（2005）英国连续剧。
  - *High Times*（2004）苏格兰连续剧。

### 欧洲音乐 Euromusic

音乐会，访谈，纪录片，欧洲流行乐坛的特别报道，艺人相关动态：[U2](../Page/U2.md "wikilink")，[Sting](../Page/Sting.md "wikilink")，[Paul
McCartney](../Page/Paul_McCartney.md "wikilink")，[Alejandro
Sanz](../Page/Alejandro_Sanz.md "wikilink")，[Oasis](../Page/Oasis.md "wikilink")，[Fatboy
Slim](../Page/Fatboy_Slim.md "wikilink")，[Coldplay](../Page/Coldplay.md "wikilink")，[Craig
David](../Page/Craig_David.md "wikilink")，[Elton
John](../Page/Elton_John.md "wikilink")，[Blur](../Page/Blur.md "wikilink")，[Stereophonics](../Page/Stereophonics.md "wikilink")，[Rolling
Stones](../Page/Rolling_Stones.md "wikilink")，[The Chemical
Brothers](../Page/The_Chemical_Brothers.md "wikilink")，[Robbie
Williams](../Page/Robbie_Williams.md "wikilink")，[Radiohead](../Page/Radiohead.md "wikilink")，[Texas](../Page/Texas.md "wikilink")，[Jamiroquai](../Page/Jamiroquai.md "wikilink")，[Björk](../Page/Björk.md "wikilink")。Euromusic每周日晚都放送一场盛大的音乐会或特别演出。

### 欧洲旅游 Eurotravel

Eurochannel带来欧洲最美丽，最与众不同景点的介绍。在展现最佳旅游胜地的同时，频道还将提供住房，购物，餐厅，文化活动等信息。。。使您尽享欧洲美景及生活。

### 欧洲时尚 Eurofashion

时尚届最新的流行咨询，最负盛名的设计师：[Giorgio
Armani](../Page/Giorgio_Armani.md "wikilink")，[Christian
Dior](../Page/Christian_Dior.md "wikilink")，Valentino，[Jean-Paul
Gaultier](../Page/Jean-Paul_Gaultier.md "wikilink")，[Gianni
Versace](../Page/Gianni_Versace.md "wikilink")，[Karl
Lagerfeld](../Page/Karl_Lagerfeld.md "wikilink")，Stella
McCartney，[Alexander
McQueen](../Page/Alexander_McQueen.md "wikilink")，Vivianne
Westwood，Dolce & Gabanna，Issey Miyake，Julien McDonald，John Galiano。。。

  - 著名设计师的详细介绍
  - 访谈
  - 最新时装发布秀
  - 关于著名时尚届人物的纪录片
  - 生活时尚及新流行趋势。。。

### 欧洲杂志 Euromagazine

Euromagazine带来诸多世界著名人物专题节目，这些名人有的来自时尚届，娱乐界，有的从事建筑，摄影，设计，艺术或文学。通过Euromagazine，您可以深入了解Peter
Lindberg，Philippe Starck，Cartier，Gaudi，Monet，Edith Piaf，Garcia
Lorca，您也可以看到Michael Caine，Hugh Jackman，Heather
Mills等的个人访谈，以及Harrod’s，Le Bon Marché等著名百货公司的纪录片。

### 原创制作 Original Productions

Eurochannel同时有自己的电影制作，名人访谈及纪录片。

  - Europa Paulistana

Eurochannel和Grifa Mixer
Production（巴西主要制作公司之一）在巴西[圣保罗做了一次特殊的旅行](../Page/圣保罗.md "wikilink")。目的是了解在巴西居住的欧洲移民，他们也被称作Europa
Paulistana。

Europa Paulistana讲述了在圣保罗这座迷人城市中，欧洲移民的日常生活习惯，以及他们的诸多喜悦与困难。

您将会在同一时间分别从一名西班牙记者，一个葡萄牙出租车司机，一位意大利生意人和一个德国学生身上看到４段不同的经历和观点。他们将讲述他们在Paulistana的生活和感受。这四段不同的经历和故事将使我们深入了解圣保罗生活的各个层面：美丽与怪诞，社会文化差异，家庭的重要性，对工作的热情，以及街头暴力。

整部电影在圣保罗拍摄，Michel Tikhomiroff和Edu Rajabally担任导演。Andre
Abujamra负责原声配乐。Europa
Paulistana描绘了一幅今日圣保罗人民生活的景象，真实的展现了他们的幸福，困难，感受和怀疑。

## 參見

  - [Canal+](../Page/Canal+.md "wikilink")

## 外部链接

Eurochannel的官方主页（http://www.eurochannel.com/）

[Category:电视台](../Category/电视台.md "wikilink")