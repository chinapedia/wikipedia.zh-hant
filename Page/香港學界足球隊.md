**香港學界足球隊**（[縮寫](../Page/縮寫.md "wikilink")：**H.K.S.S.F.**），是[香港學界體育聯會屬下的一支足球隊](../Page/香港學界體育聯會.md "wikilink")，代表[香港參加](../Page/香港.md "wikilink")[亞洲及國內學界足球比賽](../Page/亞洲.md "wikilink")，亦曾經派隊參與[香港足球總會主辦的](../Page/香港足球總會.md "wikilink")[香港丙組（地區）足球聯賽](../Page/香港丙組（地區）足球聯賽.md "wikilink")，於2010-11年度球季丙組榜末附加賽名列包尾，被淘汰出局。

## 球隊近況

於2005-06年度球季，香港學界足球隊以10勝3和2負積33分的成績，奪得[香港丙組（地區）足球聯賽冠軍](../Page/香港丙組（地區）足球聯賽.md "wikilink")\[1\]，惟在升班附加賽中只得第三名，未能夠取得升班資格。

2008-09年度球季，香港學界足球隊在[香港丙組（地區）足球聯賽以](../Page/香港丙組（地區）足球聯賽.md "wikilink")7勝2和5負得23分，以第5名完成賽季。

香港學界足球隊於2010-11年度球季丙組榜末附加賽名列包尾，按[香港足球總會賽例被淘汰出局](../Page/香港足球總會.md "wikilink")\[2\]。

## 球會榮譽

| <font color="white"> **榮譽**                           | <font color="white"> **次數** | <font color="white"> **年度** |
| ----------------------------------------------------- | --------------------------- | --------------------------- |
| **本 土 聯 賽**                                           |                             |                             |
| **[丙組（地區）聯賽](../Page/香港丙組（地區）足球聯賽.md "wikilink") 冠軍** | **1**                       | 2005－06年                    |

## 著名球員

  - [歐陽耀沖](../Page/歐陽耀沖.md "wikilink")、[葉頌朗](../Page/葉頌朗.md "wikilink")、[林學曦](../Page/林學曦.md "wikilink")、[陸建鳴](../Page/陸建鳴.md "wikilink")、[何浩文](../Page/何浩文.md "wikilink")

[240
px](https://zh.wikipedia.org/wiki/File:Auyeungyiuchung.JPG "fig:240 px")
歐陽耀沖

[`Ip_Chung_Long.JPG`](https://zh.wikipedia.org/wiki/File:Ip_Chung_Long.JPG "fig:Ip_Chung_Long.JPG")

葉頌朗

[Lamhokhei.JPG](https://zh.wikipedia.org/wiki/File:Lamhokhei.JPG "fig:Lamhokhei.JPG")
林學曦

何浩文

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [香港足球總會](http://www.hkfa.com)
  - [香港學界體育聯會官方網頁](http://www.hkssf.org.hk)

[Category:香港足球會](../Category/香港足球會.md "wikilink")

1.  [2005/06年度丙組地區聯賽總積分表](http://www.hkfa.com/zh-hk/match_score_table.php?leagueyear=2005-2006&leagueyear_id=5)
2.  [2010/11年度丙組榜末附加賽積分表](http://www.hkfa.com/zh-hk/match_score_table.php?leagueyear=2010-2011&leagueyear_id=440)