**豐田皇冠Comfort**（、），是1995年12月登場，豐田汽車生產的中型的[私家車](../Page/私家車.md "wikilink")。
Comfort是前2座、Crown
Comfort的轮罩基础长，并且是前2/3座位(前3座位已停產)。由於以開發作為[的士](../Page/的士.md "wikilink")（[計程車](../Page/計程車.md "wikilink")）用途為前提，比以前的CROWN，後座位的窗框變得接近垂直，和左側有的圍巾等差異。

Crown Comfort 在日本俗稱。

製造地點在[豐田集團旗下](../Page/豐田集團.md "wikilink")[關東自動車工業位於](../Page/關東自動車工業.md "wikilink")[靜岡縣](../Page/靜岡縣.md "wikilink")[裾野市的東富士工廠](../Page/裾野市.md "wikilink")。

豐田 皇冠Comfort
在日本於2017年3月底停產，並於日本在2017年5月全面停止售賣,於2017年9月推出新型混合動力石油氣及電動的JPN取代。

[File:Citycab-Crown.JPG|在新加坡的皇冠Comfort計程車](File:Citycab-Crown.JPG%7C在新加坡的皇冠Comfort計程車)
[File:Chuotaxi.jpg|日本的皇冠Comfort的士](File:Chuotaxi.jpg%7C日本的皇冠Comfort的士)

## 相關

  - [豐田汽車車款列表](../Page/豐田汽車車款列表.md "wikilink")
  - [豐田Comfort](../Page/豐田Comfort.md "wikilink")

## 外部連結

  - [豐田Crown
    Comfort官方网站](https://web.archive.org/web/20080124010103/http://toyota.jp/Showroom/All_toyota_lineup/crowncomfort/index.html)

[en:Toyota Crown Comfort](../Page/en:Toyota_Crown_Comfort.md "wikilink")
[ja:トヨタ・クラウンコンフォート](../Page/ja:トヨタ・クラウンコンフォート.md "wikilink") [th:โตโยต้า
คราวน์ คอมฟอร์ท](../Page/th:โตโยต้า_คราวน์_คอมฟอร์ท.md "wikilink")

[C](../Category/豐田車輛.md "wikilink")
[Category:中型車](../Category/中型車.md "wikilink")
[Category:計程車](../Category/計程車.md "wikilink")
[Category:前置引擎](../Category/前置引擎.md "wikilink")
[Category:後輪驅動](../Category/後輪驅動.md "wikilink")
[Category:好設計獎](../Category/好設計獎.md "wikilink")