**漢考克縣**（**Hancock County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州西北部的一個縣](../Page/肯塔基州.md "wikilink")，北隔[俄亥俄河與](../Page/俄亥俄河.md "wikilink")[印地安納州相望](../Page/印地安納州.md "wikilink")。面積515平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口8,392人。縣治[霍斯維爾](../Page/霍斯維爾.md "wikilink")
(Hawesville)。

成立於1829年3月1日，3月23日組成縣政府。以《美國獨立宣言》首名簽署者[約翰·漢考克命名](../Page/約翰·漢考克.md "wikilink")。

[H](../Category/肯塔基州行政区划.md "wikilink")