**季羡林**（），字**希逋**，又字**齐奘**\[1\]，生于[山东省](../Page/山东省.md "wikilink")[临清市](../Page/临清市.md "wikilink")，[中国](../Page/中国.md "wikilink")[语言学家](../Page/语言学家.md "wikilink")、[文学](../Page/文学.md "wikilink")[翻译家](../Page/翻译.md "wikilink")，[梵文](../Page/梵文.md "wikilink")、[巴利文专家](../Page/巴利文.md "wikilink")。[北京大學](../Page/北京大學.md "wikilink")[教授](../Page/教授.md "wikilink")、[輔仁大學教授](../Page/輔仁大學.md "wikilink")。季羨林通晓[梵语](../Page/梵语.md "wikilink")、[巴利语](../Page/巴利语.md "wikilink")、[吐火罗语等语言](../Page/吐火罗语.md "wikilink")，是世界上仅有的几位从事吐火罗语研究的学者之一\[2\]\[3\]。

1934年畢業於[清華大學](../Page/清華大學.md "wikilink")[西洋文学系](../Page/西洋文学.md "wikilink")，研习[莎士比亚](../Page/莎士比亚.md "wikilink")、[歌德](../Page/歌德.md "wikilink")、[塞万提斯等西洋文學名家](../Page/塞万提斯.md "wikilink")。翌年赴[德國](../Page/德國.md "wikilink")[哥廷根大學學習](../Page/哥廷根大學.md "wikilink")[梵文](../Page/梵文.md "wikilink")、[巴利文](../Page/巴利文.md "wikilink")、[吐火羅文](../Page/吐火羅文.md "wikilink")，結識留學生[章用](../Page/章用.md "wikilink")、[田德望等](../Page/田德望.md "wikilink")，遭逢[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")，獲[哲學博士學位](../Page/哲學博士.md "wikilink")。1946年回國，在[北京大學東方語言系任教授](../Page/北京大學.md "wikilink")。

1956年加入[中国共产党](../Page/中国共产党.md "wikilink")，參與反右運動，批判右派份子。[文化大革命初期](../Page/文化大革命.md "wikilink")，曾参加周培源主導的[造反派组织](../Page/造反派.md "wikilink")[井冈山兵团](../Page/井冈山兵团.md "wikilink")，反對[聶元梓派](../Page/聶元梓.md "wikilink")，在政治鬥爭中失利，被列為反革命，下放牛棚，遭受迫害。1973年開始翻譯[印度](../Page/印度.md "wikilink")[史詩](../Page/史詩.md "wikilink")《[羅摩衍那](../Page/羅摩衍那.md "wikilink")》，1977年完成全譯本。

1989年六四事件期間，根據季羨林門生張曼菱在《季羨林——追念與思考》一文中披露，季羨林曾兩度坐在三輪車上，高揚「一級教授季羨林」白幅，[前往天安門探望學生](http://big5.backchina.com/news/2014/05/09/296723.html)。

## 履历

  - 1923年，考入[济南](../Page/济南.md "wikilink")[正谊中学](../Page/正谊中学.md "wikilink")\[4\]。
  - 1930年，考入[清华大学西洋系](../Page/清华大学.md "wikilink")，师从[吴宓](../Page/吴宓.md "wikilink")、[叶公超](../Page/叶公超.md "wikilink")，学东西诗比较、英文、[梵文](../Page/梵文.md "wikilink")；选修[陈寅恪的](../Page/陈寅恪.md "wikilink")[佛经翻译文学](../Page/佛经.md "wikilink")、[朱光潜的](../Page/朱光潜.md "wikilink")[文艺心理学](../Page/文艺心理学.md "wikilink")、[俞平伯的唐宋诗词](../Page/俞平伯.md "wikilink")、[朱自清的](../Page/朱自清.md "wikilink")[陶渊明诗](../Page/陶渊明.md "wikilink")\[5\]。
  - 1934年获得清华大学文学[学士学位](../Page/学士学位.md "wikilink")，任[山东](../Page/山东.md "wikilink")[济南中学高中语文教师](../Page/济南中学.md "wikilink")。
  - 1935年，被[德国](../Page/德国.md "wikilink")[哥廷根大学录取](../Page/哥廷根大学.md "wikilink")。
  - 1936年，师从印度学、梵语学家[恩斯特·瓦尔德施米特学梵文](../Page/恩斯特·瓦尔德施米特.md "wikilink")、[巴利文和](../Page/巴利文.md "wikilink")[佛学](../Page/佛学.md "wikilink")\[6\]。
  - 1937年，开始兼任哥廷根大学汉学系讲师\[7\]。
  - 1941年，获[博士学位](../Page/博士.md "wikilink")。从[艾密尔·西克学](../Page/艾密尔·西克.md "wikilink")[吐火罗语](../Page/吐火罗语.md "wikilink")、《[十王子传](../Page/十王子传.md "wikilink")》、《大疏》、《[梨俱吠陀](../Page/梨俱吠陀.md "wikilink")》。
  - 1946年，回国，任教于[北京大学](../Page/北京大学.md "wikilink")，兼任东方语言文学系主任，時為北大歷史上最年輕的正教授\[8\]。
  - 1949年后积极参与各种社会活动，参加教授会的组织和领导工作，担任北京大学工会主席。\[9\]
  - 1956年，被任为[中国科学院](../Page/中国科学院.md "wikilink")[哲学社会科学部委员](../Page/哲学社会科学部委员.md "wikilink")\[10\]，成為中國共產黨黨員。
  - 1957年，[反右運動期間](../Page/反右運動.md "wikilink")，參與批鬥右派份子。
  - 1964年，北大[社會主義教育運動中](../Page/社會主義教育運動.md "wikilink")，同部分教职员工和学生一起反对北大校长[陆平](../Page/陆平.md "wikilink")。
  - 1965年秋，在京郊南口村任该村社教队副队长，分管整党工作。\[11\]
  - 1966年文革初期，積極參與文化大革命運動，属于[逍遥派](../Page/逍遥派.md "wikilink")。
  - 1967年夏秋之交，加入[周培源等为首的北大](../Page/周培源.md "wikilink")[造反派组织](../Page/造反派.md "wikilink")[井冈山兵团](../Page/井冈山兵团.md "wikilink")，反对[聂元梓](../Page/聂元梓.md "wikilink")[新北大公社](../Page/新北大公社.md "wikilink")，被推选为东语系勤务员。\[12\]
    同年11月30日深夜被抄家，找到「[反革命](../Page/反革命.md "wikilink")」证据，被打倒。受到造反派两派审讯，动念自杀，又被抓去批斗。
  - 1968年春，在北大劳动改造。5月4日，在煤厂大批斗。次日与100多个黑帮分子被拉往十三陵附近的北大分校劳动改造。不久关入牛棚。1969年春节前，半解放，回家。同年在延庆新华营接受贫下中农的再教育。1970年春节回校，担任门房工作。
  - 1973年—1977年翻译完成《[罗摩衍那](../Page/罗摩衍那.md "wikilink")》。
  - 1978年复出，续任北京大学东方语言文学系主任，并被任命为北京大学副校长、中国社会科学院·北京大学[南亚研究所所长](../Page/南亚.md "wikilink")\[13\]。
  - 1985年任[北京大学南亚东南亚研究所所长](../Page/北京大学.md "wikilink")\[14\]。
  - 1999年，應[聖嚴法師之邀](../Page/聖嚴法師.md "wikilink")，赴台訪問，並祭拜[胡適墓園](../Page/胡適.md "wikilink")，撰寫《站在胡適之先生墓前》一文\[15\]。
  - 2003年起，因病入住[301醫院](../Page/301醫院.md "wikilink")。
  - 2004年，由[中华民族文化促进会主办](../Page/中华民族文化促进会.md "wikilink")，2004（甲申）年9月在北京举行“2004文化高峰论坛”上，[许嘉璐](../Page/许嘉璐.md "wikilink")、季羡林、[任继愈](../Page/任继愈.md "wikilink")、[杨振宁](../Page/杨振宁.md "wikilink")、[王蒙
    (作家)五位发起人领衔](../Page/王蒙_\(作家\).md "wikilink")、七十二名文化人士共同签署的《[甲申文化宣言](../Page/甲申文化宣言.md "wikilink")》于会后发布。该宣言强调全球化日益深化的情况下文化多样性共存的必要性以及文化交流的平等权利。\[16\]
  - 2008年1月，季羡林获印度公民荣誉奖之一的[莲花装勋章](../Page/莲花装勋章.md "wikilink")\[17\]。
  - 2009年7月11日上午9时，病逝于301医院，享年97岁\[18\]。其子[季承称季羡林因心脏病突发昏迷](../Page/季承.md "wikilink")，而后抢救无效而去世\[19\]。

## 学术领域

  - [印度古代语言研究](../Page/印度.md "wikilink")
  - [佛教史研究](../Page/佛教史.md "wikilink")
  - [吐火罗语研究](../Page/吐火罗语.md "wikilink")
  - 中印文化交流史研究
  - 中外文化交流史研究
  - 翻译介绍[印度文学作品及](../Page/印度文学.md "wikilink")[印度文学研究](../Page/印度文学.md "wikilink")
  - [比较文学研究](../Page/比较文学.md "wikilink")
  - [东方文化研究](../Page/东方文化.md "wikilink")

## 作品

### 翻译

  - 《安娜·西格斯短篇小说集》
  - 《[沙恭达罗](../Page/沙恭达罗.md "wikilink")》
  - 《五卷书》
  - 《[优哩婆湿](../Page/优哩婆湿.md "wikilink")》
  - 《[罗摩衍那](../Page/罗摩衍那.md "wikilink")》

### 学术著作

  - 《[中印文化关系史论丛](../Page/中印文化关系史论丛.md "wikilink")》
  - 《[印度简史](../Page/印度简史.md "wikilink")》 梅特卡夫
  - 《[現代佛學大系](../Page/現代佛學大系.md "wikilink")》
  - 《[1857－1859年印度民族起义](../Page/1857－1859年印度民族起义.md "wikilink")》
  - 《[敦煌学大辞典](../Page/敦煌学大辞典.md "wikilink")》是与100多人共同编辑，不是著作。
  - 《[大唐西域记校注](../Page/大唐西域记.md "wikilink")》
    [中華書局](../Page/中華書局.md "wikilink") 1985年 ISBN
    7-101-00644-2
  - 《[吐火罗文弥勒会见记译释](../Page/吐火罗文弥勒会见记译释.md "wikilink")》
  - 《[吐火罗文A中的三十二相](../Page/吐火罗文A中的三十二相.md "wikilink")》
  - 《[敦煌吐魯番吐火羅語研究導論](../Page/敦煌吐魯番吐火羅語研究導論.md "wikilink")》
  - 《文化交流的軌跡：[中华蔗糖史](../Page/中华蔗糖史.md "wikilink")》
    [經濟日報出版社](../Page/經濟日報出版社.md "wikilink") 1997年
    ISBN 7801272846
  - 《[大國方略](../Page/大國方略.md "wikilink")：著名學者訪談錄》
  - 《[東方文學史](../Page/東方文學史.md "wikilink")》
  - 《[東方文化研究](../Page/東方文化研究.md "wikilink")》
  - 《[禪與東方文化](../Page/禪與東方文化.md "wikilink")》
  - 《[東西文化議論集](../Page/東西文化議論集.md "wikilink")》 季羡林等编
  - 《[世界文化史知識](../Page/世界文化史知識.md "wikilink")》季羡林 周一良 张芝联 主编

### 散文随笔

  - 《[清塘荷韵](../Page/清塘荷韵.md "wikilink")》
  - 《[赋得永久的悔](../Page/赋得永久的悔.md "wikilink")》
  - 《[留德十年](../Page/留德十年.md "wikilink")》
  - 《[万泉集](../Page/万泉集.md "wikilink")》
  - 《[清华园日记](../Page/清华园日记.md "wikilink")》
  - 《[牛棚杂忆](../Page/牛棚杂忆.md "wikilink")》
  - 《[朗润园随笔](../Page/朗润园随笔.md "wikilink")》
    [上海人民出版社](../Page/上海人民出版社.md "wikilink")
    2000年 ISBN 7-208-03280-7
  - 《[季羡林散文選集](../Page/季羡林散文選集.md "wikilink")》
  - 《[泰戈爾名作欣賞](../Page/泰戈爾名作欣賞.md "wikilink")》
  - 《[人生絮語](../Page/人生絮語.md "wikilink")》
  - 《[天竺心影](../Page/天竺心影.md "wikilink")》
  - 《[季羡林談讀書治學](../Page/季羡林談讀書治學.md "wikilink")》
  - 《[季羡林談師友](../Page/季羡林談師友.md "wikilink")》
  - 《[季羡林談人生](../Page/季羡林談人生.md "wikilink")》
  - 《[病塌雜記](../Page/病塌雜記.md "wikilink")》 新世界出版社 2007年1月初版 ISBN
    9-787-80228-217-9
  - 《[忆往述怀](../Page/忆往述怀.md "wikilink")》 2008年5月第一版
  - 《[新紀元文存](../Page/新紀元文存.md "wikilink")》 （香港）和平圖書有限公司出版 2003年8月第一版
    ISBN 962-238-327-0

## 學生

### 本科生

季羨林在清華大學就讀期間，主修德文，但曾選修陳寅恪《佛經翻譯文學》課程，開啟他對於梵文的興趣。

1946年，季羨林從[德國回中國](../Page/德國.md "wikilink")，至上海拜會陳寅恪。經[陳寅恪介紹](../Page/陳寅恪.md "wikilink")，至北京大學拜會校長傅斯年，之後出任[北京大學文學院東方語言文學系教授兼主任](../Page/北京大學.md "wikilink")。這個東方語言文學系是[第二次世界大戰后新成立的](../Page/第二次世界大戰.md "wikilink")（[抗日战争前的北大有一個東方文學系](../Page/抗日战争.md "wikilink")，只有[日本語專業](../Page/日本語.md "wikilink")，[周作人做主任](../Page/周作人.md "wikilink")，[蘆溝橋事變以后停辦](../Page/蘆溝橋事變.md "wikilink")）。

創系師資是季羨林（[梵語](../Page/梵語.md "wikilink")、[巴利語](../Page/巴利語.md "wikilink")，兼主任）、[金克木](../Page/金克木.md "wikilink")（[梵語](../Page/梵語.md "wikilink")、[巴利語](../Page/巴利語.md "wikilink")）、[馬堅](../Page/馬堅.md "wikilink")（[阿拉伯語](../Page/阿拉伯語.md "wikilink")）、[馬學良](../Page/馬學良.md "wikilink")（[彝語](../Page/彝語.md "wikilink")）、[于道泉](../Page/于道泉.md "wikilink")（[滿文](../Page/滿文.md "wikilink")、[蒙古語](../Page/蒙古語.md "wikilink")）、[王森](../Page/王森_\(藏学家\).md "wikilink")（[藏語](../Page/藏語.md "wikilink")）。

最早的上級領導是文學院院長[湯用彤](../Page/湯用彤.md "wikilink")、代理校長[傅斯年](../Page/傅斯年.md "wikilink")（1946年到1947年）和校長[胡適](../Page/胡適.md "wikilink")（1947年到1948年）。

1952年院系調整，設[蒙古語](../Page/蒙古語.md "wikilink")、[朝鮮語](../Page/朝鮮語.md "wikilink")、[日本語](../Page/日本語.md "wikilink")、[泰語](../Page/泰語.md "wikilink")、[印尼語](../Page/印尼語.md "wikilink")、[緬甸語](../Page/緬甸語.md "wikilink")、[印地語](../Page/印地語.md "wikilink")、[阿拉伯語](../Page/阿拉伯語.md "wikilink")9個專業。

1960年開設[梵語](../Page/梵語.md "wikilink")、[巴利語專業](../Page/巴利語.md "wikilink")，招收第1批17本科生趙國華、[蔣忠新](../Page/蔣忠新.md "wikilink")、[黃寶生](../Page/黃寶生.md "wikilink")、[郭良鋆](../Page/郭良鋆.md "wikilink")、[馬鵬雲](../Page/馬鵬雲.md "wikilink")、[韓霆杰](../Page/韓霆杰.md "wikilink")、[張保勝](../Page/張保勝.md "wikilink")、[張敏秋](../Page/張敏秋.md "wikilink")、[梁化仁](../Page/梁化仁.md "wikilink")、[許順慶](../Page/許順慶.md "wikilink")、[周玉華](../Page/周玉華.md "wikilink")、[趙炳双](../Page/趙炳双.md "wikilink")、[馬青川](../Page/馬青川.md "wikilink")、[陳貞輝](../Page/陳貞輝.md "wikilink")、[莫澤香](../Page/莫澤香.md "wikilink")、[黃恆斌](../Page/黃恆斌.md "wikilink")、[钟芳云](../Page/钟芳云.md "wikilink")，在季羨林和金克木指導下系統學習梵語、巴利語。

1984年梵語、巴利語專業第2次招生，有[錢文忠](../Page/錢文忠.md "wikilink")、[陸揚](../Page/陸揚.md "wikilink")、[周春
(梵語)](../Page/周春_\(梵語\).md "wikilink")、[劉孟](../Page/劉孟.md "wikilink")、[梁立軍](../Page/梁立軍.md "wikilink")、[張守川](../Page/張守川.md "wikilink")、[肖威](../Page/肖威.md "wikilink")、[王旬](../Page/王旬.md "wikilink")7位入讀，由[蔣忠新和](../Page/蔣忠新.md "wikilink")[郭良鋆這](../Page/郭良鋆.md "wikilink")2位季羨林和金克木的學生主持教學，1位被選送到[奧地利](../Page/奧地利.md "wikilink")[維也納大學學習](../Page/維也納大學.md "wikilink")，4位被選送到德國[漢堡大學學習](../Page/漢堡大學.md "wikilink")。

季羨林做東語系主任超過30年（1946年到1966年20年，[改革開放后又再任](../Page/改革開放.md "wikilink")），任期內東語系本科各語種專業的在讀生和畢業生在義理上都是他的學生。

### 研究生

季羡林培養了5位碩士和5位博士，共指導了9位研究生（王邦維碩士生博士生階段都是季羡林培養，在季羡林指導下讀了3年碩士生5年博士生）。

最早招收的碩士生是1978年的[任遠和](../Page/任遠.md "wikilink")[段晴](../Page/段晴.md "wikilink")，1979年收了[王邦维和](../Page/王邦维.md "wikilink")[葛維鈞](../Page/葛維鈞.md "wikilink")，4人在1982年畢業，是季羡林培養的第1批碩士。

王邦維碩士生畢業后在季羡林指導下讀博士生，1987年成為季羡林培養的第1位博士。

1991年畢業的[錢文忠是季羡林培養的第](../Page/錢文忠.md "wikilink")5位碩士（同時是季羡林指導的最后1位碩士生），同1年，季羡林培養出第2位博士[辛島靜志](../Page/辛島靜志.md "wikilink")（日本人，同時是季羡林僅有的1位不是中國籍的研究生）。

季羡林培養的第3位博士是1996年讀完的[李南](../Page/李南.md "wikilink")，1998年讀完的[高鴻是第](../Page/高鴻.md "wikilink")4位博士，2000年讀完的第5位博士[劉波是季羡林指導的最后](../Page/劉波_\(文化\).md "wikilink")1位研究生和最后1位博士生。

## 評價

  - 季羡林在大陸被许多人尊重，并被一些人奉為[中國大陸的](../Page/中國大陸.md "wikilink")“[国学大师](../Page/国学.md "wikilink")”、“学界泰斗”、“国宝”。對此，季羡林在他的《病榻杂记》中力辭這三頂“桂冠”：“我对哪一部古典，哪一个作家都没有下过死工夫，因为我从来没想成为一个国学家。除了尚能背诵几百首诗词和几十篇古文外；除了尚能在最大的宏观上谈一些与国学有关的自谓是大而有当的问题比如[天人合一外](../Page/天人合一.md "wikilink")，自己的国学知识并没有增加。环顾左右，朋友中国学基础胜于自己者，大有人在。在这样的情况下，我竟独占‘国学大师’的尊号，岂不折煞老身！我连‘国学小师’都不够，遑论‘大师’！”“我一直担任行政工作，想要做出什么成绩，岂不戛戛乎难矣哉！我这个‘泰斗’从哪里讲起呢？”“三顶桂冠一摘，还了我一个自由自在身。身上的泡沫洗掉了，露出了真面目，皆大欢喜。”\[20\]\[21\]
  - [余英時認為季羨林只不過是因晚年親共](../Page/余英時.md "wikilink")，且被[中共力捧的學術樣板](../Page/中共.md "wikilink")。不論在專業上或在操守上，都不配被稱為國學大師。\[22\]
  - 季羡林曾被授予2006年“[感动中国](../Page/感动中国.md "wikilink")”人物。頒獎詞中稱：“智者乐，仁者寿，长者随心所欲。一介布衣，言有物，行有格，贫贱不移，宠辱不惊。学问铸成大地的风景，他把心汇入传统，把心留在东方。……季羡林先生为人所敬仰，不仅因为他的学识，还因为他的品格。……他的书，不仅是个人一生的写照，也是近百年来中国知识分子历程的反映。”\[23\]
  - [溫家寶曾于](../Page/溫家寶.md "wikilink")2003年、2005年、2006年、2007年、2008年五次看望季羡林\[24\]。他稱：“您最大的特点就是一生笔耕不辍，桃李不言，下自成蹊。您写的作品，如行云流水，叙事真实，传承精神，非常耐读。”“您写的几本书，不仅是个人一生的写照，也是近百年来中国知识分子历程的反映。……您在最困难的时候，包括在‘牛棚’挨整的时候，也没有丢掉自己的信仰。”“您一生坎坷，敢说真话，直抒己见，这是值得人们学习的。”\[25\]
  - [李敖在接受](../Page/李敖.md "wikilink")[南都週刊專訪時](../Page/南都週刊.md "wikilink")，曾經評論季羨林並不足以稱為國學大師：「他不是國學大師！他是個很弱很弱的教授，他就是語文能力還不錯。別人全死光了，他還沒死，所以他就變成國學大師了！這些桂冠，他三個都不及格的，根本輪不到他！……季羨林只是個老資格的人，根本輪不到他做大師。」\[26\]
  - 针对李敖炮轰季羡林“三项桂冠”都不合格的说法，[钱文忠回应道](../Page/钱文忠.md "wikilink")：“季羡林的学问，李敖看都看不懂，他评论季羡林，就好像一个不懂英文的人评论莎士比亚，毫无意义。”\[27\]
  - 季羨林之子季承曾被趕出家門，長達13年父子不見面。在季承《我和父親季羨林》筆下的季羨林是一個孤獨、寂寞、吝嗇、無情的文人。

## 註釋

## 参考文献

  - 《季羡林自述》
  - 《牛棚杂忆》
  - 《[晚年季羨林病中新作首刊](https://web.archive.org/web/20061211173536/http://www.mingpaomonthly.com/ftp/Mpm/200612/cover.jpg)》專題，[明報月刊](../Page/明報月刊.md "wikilink")2006年12月號
  - 张光璘《季羡林先生》http://vip.book.sina.com.cn/book/catalog.php?book=38497

## 外部链接

  - [季羡林年谱](https://web.archive.org/web/20110707220447/http://www.bbtpress.com/homepagebook/615/a03.htm)
  - [季羡林自传](https://web.archive.org/web/20110707222034/http://www.bbtpress.com/homepagebook/615/a04.htm)
  - [学问人生——季羡林自述](http://book.sina.com.cn/nzt/his/jixianlinzishu/index.shtml)
  - 余英時：[〈談談季羡林任繼愈等「大師」〉](http://www.rfa.org/mandarin/pinglun/yuyingshi/yuyingshi-08042009104235.html)（2009）
  - 班固志：〈[季羡林与泰戈尔](http://www.nssd.org/articles/article_read.aspx?id=27852048)〉。
  - [季羡林先生——张光麟](http://book.sina.com.cn/nzt/1072763223_jixianlin/index.shtml)
  - [国学大师季羡林教授](http://pkuaa.org/forum/view.jsp?forum=26&thread=777)
  - [温家宝为季羡林祝寿](http://news.sina.com.cn/c/2006-08-06/21129673326s.shtml)
  - [傑出華人系列——季羨林](http://www.rthk.org.hk/classicschannel/video/90s_0096.asx)
  - [盼不到的「百歲述懷」](http://www.nownews.com/2009/07/19/142-2480272.htm)

{{-}}

[Xian羡林](../Category/季姓.md "wikilink")
[Category:临清人](../Category/临清人.md "wikilink")
[Category:哥廷根大學校友](../Category/哥廷根大學校友.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:國立清華大學校友](../Category/國立清華大學校友.md "wikilink")
[Category:北京大學教授](../Category/北京大學教授.md "wikilink")
[Category:輔仁大學教授](../Category/輔仁大學教授.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:中國語言學家](../Category/中國語言學家.md "wikilink")
[Category:中國翻譯家](../Category/中國翻譯家.md "wikilink")
[Category:中国印度学家](../Category/中国印度学家.md "wikilink")
[Category:中国东方学家](../Category/中国东方学家.md "wikilink")
[Category:佛教學者](../Category/佛教學者.md "wikilink")
[Category:鲁迅文学奖得主](../Category/鲁迅文学奖得主.md "wikilink")
[Category:感动中国年度人物](../Category/感动中国年度人物.md "wikilink")
[Category:蔡元培奖获得者](../Category/蔡元培奖获得者.md "wikilink")

1.
2.

3.  [季羡林简介](http://book.qq.com/a/20090711/000009.htm)
    原文“季羡林师从"梵文讲座"主持人、著名梵文学者瓦尔德施米特教授，成为他唯一的听课者。一个学期
    40多堂课，季羡林学习异常勤奋。佛典《大事》厚厚3大册，是用混合梵文写成的，他争分夺秒，致力于读和写，"开电灯以继晷，恒兀兀以穷年"。”“1940年12月至1941年2月，季羡林在论文答辩和印度学、斯拉夫语言、英文考试中得到4个"优"，获得博士学位。”，“研究翻译了梵文著作和德、英等国的多部经典”，“博士论文《〈大事〉渴陀中限定动词的变化》、《中世印度语言中语尾-am，向-o和-u的转化》、《使用不定过去式作为确定佛典的年代与来源的标准》等论文，在当时该研究领域内有开拓性贡献；”，“早期代表作《〈福力太子因缘经〉吐火罗语诸本诸平行译本》，为吐火罗语的语意研究开创了一个成功的方法，1948年起即对新疆博物馆藏吐火罗剧本《[弥勒会见记](../Page/弥勒会见记.md "wikilink")》进行译释，1980年又就7O年代新疆吐鲁番地区新发现的吐火罗语A《弥勒会见记》发表研究论文多篇，打破了"吐火罗文发现在中国，而研究在国外"的欺人之谈”

4.

5.
6.
7.
8.

9.  《牛棚杂忆·第10节劳改的初级阶段》

10.
11. 《牛棚杂忆·第2节从社教运动谈起》

12. 《牛棚杂忆·第6节自己跳出来》

13.
14.
15.

16.

17.

18.

19.

20.
21. [季羡林《病榻杂记》：还我自由自在身](http://www.booker.com.cn/GB/69398/5186610.html)

22. 余英時〈[谈谈季羡林任继愈等“大师”](http://www.rfa.org/mandarin/pinglun/yuyingshi/yuyingshi-08042009104235.html)〉2009-08-04
    [自由亞洲電台](../Page/自由亞洲電台.md "wikilink")

23. [季羡林：他把心汇入传统
    把心留在东方](http://news.sina.com.cn/c/2007-02-28/155012393318.shtml)

24.

25. [温家宝看望季羡林
    祝贺其95岁寿辰](http://www.china.com.cn/news/txt/2006-08/07/content_7042388_2.htm)

26. [李敖：大陆没有文化名流
    季羡林三桂冠都不及格](http://www.ce.cn/xwzx/xwrwzhk/peoplemore/200701/29/t20070129_10246142.shtml)
    来源：南都周刊

27. [百家名嘴駁"李敖炮轟季羨林"](http://book.hexun.com.tw/2007-07-16/103336540.html)