**酒田市**（）為[山形縣西北部的市](../Page/山形縣.md "wikilink")，人口數位居縣內第三位。為[庄內地方北部的中心都市](../Page/庄內地方.md "wikilink")，也是山形縣內重要的港灣城市。

## 历史

酒田市成立于1933年4月1日。2005年11月1日，饱海郡的[平田町](../Page/平田町_\(山形县\).md "wikilink")、[松山町和](../Page/松山町_\(山形县\).md "wikilink")[八幡町并入成为新酒田市](../Page/八幡町_\(山形县\).md "wikilink")。

电影[入殓师](../Page/入殓师.md "wikilink")（奥斯卡最佳外语片）曾在酒田市拍摄。

## 气候

酒田市为[副热带湿润气候](../Page/副热带湿润气候.md "wikilink")（[柯本气候分类法](../Page/柯本气候分类法.md "wikilink")），夏季温暖冬季寒冷。全年多降雨，其中2月至6月降雨较全年偏少。

<div style="width:70%;">

</div>

## 交通

  - [庄内机场](../Page/庄内机场.md "wikilink")（与东京和大阪通航）

## 地理

  - 山:
    [鳥海山](../Page/鳥海山.md "wikilink")、[飯森山](../Page/飯森山.md "wikilink")、[鷹尾山](../Page/鷹尾山.md "wikilink")、[胎蔵山](../Page/胎蔵山.md "wikilink")
  - 河川:
    [最上川](../Page/最上川.md "wikilink")、[日向川](../Page/日向川.md "wikilink")、[赤川](../Page/赤川.md "wikilink")、[京田川](../Page/京田川.md "wikilink")、[新井田川](../Page/新井田川.md "wikilink")（[荒瀨川](../Page/荒瀨川.md "wikilink")、[相澤川](../Page/相澤川.md "wikilink")、[田澤川](../Page/田澤川.md "wikilink")
  - 湖沼:[鶴間池](../Page/鶴間池.md "wikilink")、[與藏沼](../Page/與藏沼.md "wikilink")
  - 水庫:[田澤川水庫](../Page/田澤川水庫.md "wikilink")
  - 峡谷:[青澤峡](../Page/青澤峡.md "wikilink")、[前之川峽](../Page/前之川峽.md "wikilink")

## 寺廟

  - [丹能寺](../Page/丹能寺.md "wikilink")

### 相鄰的自治体

  - 山形縣
      - [鶴岡市](../Page/鶴岡市.md "wikilink")
      - [飽海郡](../Page/飽海郡.md "wikilink")：[遊佐町](../Page/遊佐町.md "wikilink")
      - [東田川郡](../Page/東田川郡.md "wikilink")：[庄內町](../Page/庄內町.md "wikilink")、[三川町](../Page/三川町.md "wikilink")
      - [最上郡](../Page/最上郡.md "wikilink")：[真室川町](../Page/真室川町.md "wikilink")、[鮭川村](../Page/鮭川村.md "wikilink")、[戶澤村](../Page/戶澤村.md "wikilink")
  - [秋田縣](../Page/秋田縣.md "wikilink")
      - [由利本莊市](../Page/由利本莊市.md "wikilink")
      - [仁賀保市](../Page/仁賀保市.md "wikilink")

## 知名人物

  - [中島春雄](../Page/中島春雄.md "wikilink")，演员
  - [大川周明](../Page/大川周明.md "wikilink")，极端民族主义者
  - [成田三樹夫](../Page/成田三樹夫.md "wikilink")，演员

## 外部連結

  - [山形縣酒田市](http://www.city.sakata.yamagata.jp/)