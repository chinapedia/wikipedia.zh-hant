**臺南市南區志開實驗小學** （英文名稱 Tainan Municipal South District Jhihkai
Experimental Elementary School），前身為「台南市南區志開國民小學」（Jhih Kai Elementary
School），簡稱志開實小，以紀念[中華民國空軍](../Page/中華民國空軍.md "wikilink")[王牌飛行員](../Page/王牌飛行員.md "wikilink")[周志開](../Page/周志開.md "wikilink")，位於[台灣](../Page/台灣.md "wikilink")[台南市](../Page/台南市.md "wikilink")[南區](../Page/南區_\(台南市\).md "wikilink")，與[台南市立體育場毗鄰](../Page/台南市立體育場.md "wikilink")，是一所都市中的森林小學。
[Jhih_Kai_Elementary_School.jpg](https://zh.wikipedia.org/wiki/File:Jhih_Kai_Elementary_School.jpg "fig:Jhih_Kai_Elementary_School.jpg")

## 國際交流

志開國小自1997年起，開始積極進行國際交流活動。

1997年合作專題：[The Ideal City](http://features.zkes.tn.edu.tw:81/vc/vc97/)
合作學校：

  - [Yale Elementary
    School](../Page/Yale_Elementary_School.md "wikilink")，
    [Aurora](../Page/Aurora.md "wikilink")，
    [Colorado](../Page/Colorado.md "wikilink")，
    [USA](../Page/USA.md "wikilink")
  - [East Gosford Public
    School](../Page/East_Gosford_Public_School.md "wikilink")，
    [Gosford](../Page/Gosford.md "wikilink")， [New South
    Wales](../Page/New_South_Wales.md "wikilink")，
    [Australia](../Page/Australia.md "wikilink")

1998年合作專題：[Virtual School](http://features.zkes.tn.edu.tw:81/vc/vc98/)
合作學校：

  - [Oxford Elementary
    School](https://web.archive.org/web/20090415112856/http://www.chuh.org/oxford.shtml)，
    [Cleveland Heights](../Page/Cleveland_Heights.md "wikilink")，
    \*[OH](../Page/OH.md "wikilink")， [USA](../Page/USA.md "wikilink")
  - [Albany Rise Elementary
    School](../Page/Albany_Rise_Elementary_School.md "wikilink")，
    [Australia](../Page/Australia.md "wikilink")

1999年合作專題：[VC19 Restaurant At Your Service: Japan, Taiwan, and the
USA](http://features.zkes.tn.edu.tw:81/vc/vc99/)
合作學校：

  - [Ougidai Elementary
    School](https://web.archive.org/web/20070928092238/http://www.incl.ne.jp/ougidai/index-e.html)，
    [Kanazawa](../Page/Kanazawa.md "wikilink")，
    [Japan](../Page/Japan.md "wikilink")
  - [Maxwell Hill Elementary
    School](https://web.archive.org/web/20070705233434/http://maxwellhill.rale.k12.wv.us/)，
    [WV](../Page/WV.md "wikilink")，[USA](../Page/USA.md "wikilink")

2000年合作專題：[Happy Life](http://features.zkes.tn.edu.tw:81/vc2000/)
合作學校：

  - [Ougidai Elementary
    School](https://web.archive.org/web/20070928092238/http://www.incl.ne.jp/ougidai/index-e.html)，
    [Kanazawa](../Page/Kanazawa.md "wikilink")，
    [Japan](../Page/Japan.md "wikilink")

2001年合作專題：[鐘聲傳海峽,攜手跨世紀](http://features.zkes.tn.edu.tw:81/vc2000/)
合作學校：

  - [新城花園小學](https://web.archive.org/web/20070515170110/http://www.xgps.net/)，
    [蘇州](../Page/蘇州.md "wikilink")， [江蘇省](../Page/江蘇省.md "wikilink")，
    [中國](../Page/中國.md "wikilink")

2009年合作專題：Artmile project

## 學校特色

  - 傳統鼓藝
  - 蝴蝶復育
  - 食農教學
  - 家政課程

## 社團

  - 相聲
  - 掌中戲
  - 鼓藝隊

## 外部連結

  - [台南市立志開實驗小學](https://web.archive.org/web/20170916033525/http://www.zkes.tn.edu.tw/)

[Category:臺南市立國民小學](../Category/臺南市立國民小學.md "wikilink")
[Category:1948年創建的教育機構](../Category/1948年創建的教育機構.md "wikilink")
[南](../Category/台灣冠以人名的教育機構.md "wikilink")
[校](../Category/中華民國空軍軍史.md "wikilink") [Category:南區
(臺南市)](../Category/南區_\(臺南市\).md "wikilink")