[LSQ_2.jpg](https://zh.wikipedia.org/wiki/File:LSQ_2.jpg "fig:LSQ_2.jpg")

**V字手勢**（[Emoji](../Page/Emoji.md "wikilink")：[✌](../Page/✌.md "wikilink")），手勢的做法除[食指及](../Page/食指.md "wikilink")[中指豎起外其他手指向手心彎曲](../Page/中指.md "wikilink")。通常，手心向內或外並無區別，但手心向內的手勢在某些西方國家有[侮辱含義](../Page/侮辱.md "wikilink")。

[英國首相](../Page/英國首相.md "wikilink")[邱吉爾於](../Page/邱吉爾.md "wikilink")[第二次世界大戰帶起V字手勢的風潮](../Page/第二次世界大戰.md "wikilink")。他以V字手勢代表[勝利](../Page/勝利.md "wikilink")（Victory）的第一個字母「V」，所以又名**勝利手勢**\[1\]\[2\]。

V字手勢在[美國](../Page/美國.md "wikilink")1960年代的反[越戰時期](../Page/越戰.md "wikilink")，“[要愛，不要戰爭](../Page/要愛，不要戰爭.md "wikilink")”[遊行中亦被用作表達](../Page/遊行.md "wikilink")[和平](../Page/和平.md "wikilink")，所以V字手勢亦可以有和平的意思。故又稱**和平手勢**。

## 用途

[Rihanna,_LOUD_Tour,_Minneapolis_6_crop.jpg](https://zh.wikipedia.org/wiki/File:Rihanna,_LOUD_Tour,_Minneapolis_6_crop.jpg "fig:Rihanna,_LOUD_Tour,_Minneapolis_6_crop.jpg")以V字手勢傳遞和平信息\]\]
V字手势的含意會因手的位置而改變。V字手势在不同位置下的含意：

  - 若手背朝向作手势者（即手掌朝外），那麼它可以指：
      - 數字二：以非語言的方式表達數量
      - 勝利：在戰爭或競賽的使用。是[溫斯頓·丘吉爾使這個意思流行起來的](../Page/溫斯頓·丘吉爾.md "wikilink")\[3\]。
      - 和平或朋友：是1960年代的[反戰](../Page/反戰.md "wikilink")[民權運動使這個意思流行起來的](../Page/民權運動.md "wikilink")。
      - 字母V：在[美國手語中使用](../Page/美國手語.md "wikilink")\[4\]。

<!-- end list -->

  - 若手掌朝向作手势者（即手背朝外），那麼這個V字手势就是一種侮辱。這種用法主要限於[澳大利亞](../Page/澳大利亞.md "wikilink")、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[南非和](../Page/南非.md "wikilink")[英國](../Page/英國.md "wikilink")\[5\]。

<!-- end list -->

  - V字手势在不同的移動方式下的含意：
      - 彎曲雙手的手指和手掌：表示[引号的意思](../Page/引号.md "wikilink")\[6\]。
      - 作手势者的食指和中指指向自己的眼睛，然後再轉身用食指指著別人：表示「我正在監視你」\[7\]。
      - 將V字手势由手掌朝外轉至手掌朝內：[美國手語中的序數詞](../Page/美國手語.md "wikilink")“第二”\[8\]

### 中國大陸、臺灣、香港、韓國等地對V字手勢的使用

在中國大陸、臺灣、香港、韓國等地，V字手勢是拍照時最受歡迎的手勢，常被用於非正式或正式的情境中\[9\]\[10\]。在臺灣、香港、大陸和韓國，V字手勢通常沒有和平或侮辱的意思；一些人似乎傾向認為此種手勢的意思為「勝利」或「吔」\[11\]（表示自己感到快樂）。手掌朝內或手掌朝外都很常見。

### 日本的V字手勢

[Japanese_Schoolgirls.jpg](https://zh.wikipedia.org/wiki/File:Japanese_Schoolgirls.jpg "fig:Japanese_Schoolgirls.jpg")
手掌朝外的V字手勢在日本經常用於非正式場合的拍照中，尤其年輕人間更是如此。一說認為這習俗是受[1972年冬季奥林匹克运动会時美國花式溜冰選手珍妮特](../Page/1972年冬季奥林匹克运动会.md "wikilink")‧琳恩（Janet
Lynn）的影響而來的，當時珍妮特‧琳恩在進行自由滑冰的階段（free-skate
period）的時候曾一度滑倒，但即便她坐在冰塊上，她依舊繼續保持著微笑。雖然最後珍妮特‧琳恩得到第三名，但她開朗的努力行徑，卻引起了許多日本人的共鳴，琳恩在一夜之間成了日本的外國偶像。做為和平主義運動者，琳恩在接受日本媒體採訪時經常比出V字手勢。儘管在二戰結束後盟軍佔領日本的期間，日本人就已知此V字手勢，但一些日本人認為琳恩是導致V字手勢自1970年代起在日本開始流行的原因\[12\]。

另近來在日本動漫的一些創作中，左右手同時比出V字手勢的動作有時會與「啊嘿顏」（）扯上關係\[13\]\[14\]。

## 表示勝利或自由的V字手势

[Norgeposthorn2ore1941v.jpg](https://zh.wikipedia.org/wiki/File:Norgeposthorn2ore1941v.jpg "fig:Norgeposthorn2ore1941v.jpg")
[Churchill_V_sign_HU_55521.jpg](https://zh.wikipedia.org/wiki/File:Churchill_V_sign_HU_55521.jpg "fig:Churchill_V_sign_HU_55521.jpg")[丘吉爾揮舞著V字手势](../Page/丘吉爾.md "wikilink")\]\]
於1941年1月14日，前比利時司法部長和[英國廣播公司比利時法語廣播頻道總監維克多於廣播中建議比利時人使用](../Page/英國廣播公司.md "wikilink")「V」字表達勝利（[法語](../Page/法語.md "wikilink")：*victoire*）和自由（[荷蘭語](../Page/荷蘭語.md "wikilink")：*'vrijheid*）來號召人們對抗[納粹德國](../Page/納粹德國.md "wikilink")。在廣播中，維克多說「佔領者在無限重複地看著這相同的標誌，\[將會\]意識到自己已被包圍，四周被一群巨大的人民環繞著，他們熱切地等待著敵人第一個軟弱的時刻，期待著敵人的第一次失敗。」。事實上，在維克多廣播數周後，比利時、荷蘭及北部法國均出現了很多「V」字記號\[15\]。

在這一成功的鼓舞下，英國廣播公司設定了“V代表勝利”計劃，並要求助理新聞編輯道格拉斯·里奇成為計劃主管。里奇建議使用聲音[莫爾斯電碼廣播](../Page/莫爾斯電碼.md "wikilink")「V」字，並利用擁有相同節奏的[貝多芬第5號交響曲頭段作為](../Page/第5號交響曲_\(貝多芬\).md "wikilink")[呼號](../Page/调谐信号.md "wikilink")（）。具有諷刺意味的是，貝多芬也是德國人，並且其標題「命運交響曲」暗示了納粹德國的命運已被注定——就是滅亡\[16\]\[17\]。英國廣播公司亦建議人們使用維克多建議的V字手势代替「V」字記號\[18\]。

到了1941年7月，使用「V」字的風氣已經傳播至整個德國佔領區中。於7月19日，[溫斯頓·丘吉爾在一次演講中批准了](../Page/溫斯頓·丘吉爾.md "wikilink")“V代表勝利”計劃\[19\]。自此，丘吉爾便開始使用V字手势。起初，丘吉爾的V字手势是手背朝外的\[20\]，後來才改為手背朝內\[21\]。但在戰後的政治生涯中，丘吉爾卻常以「反V」來侮辱政敵\[22\]。其他盟國領導人都跟隨丘吉爾使用V字手势。自1942年起，[夏爾·戴高樂都會在每次演講前擺出V字手势](../Page/夏爾·戴高樂.md "wikilink")，直至1969年為止\[23\]。

### 越南戰爭：勝利與和平

[Nixon-depart.png](https://zh.wikipedia.org/wiki/File:Nixon-depart.png "fig:Nixon-depart.png")[尼克松於](../Page/尼克松.md "wikilink")1974年8月9日辭職離開白宮時擺出V字手势\]\]
V字手势被抗議美國不停止[越南戰爭的遊行人仕使用](../Page/越南戰爭.md "wikilink")，並且用來象徵和平。因為當時的[嬉皮士不時使用此手势並呼叫](../Page/嬉皮士.md "wikilink")「和平」，因此V字手势成為了一個普遍的和平象徵標誌\[24\]。

## 作為侮辱的V字手势

[Robbie_paparazzi_V_sign.jpg](https://zh.wikipedia.org/wiki/File:Robbie_paparazzi_V_sign.jpg "fig:Robbie_paparazzi_V_sign.jpg")以手心向內的V字手勢作為侮辱。\]\]
[Steve_McQueen.jpg](https://zh.wikipedia.org/wiki/File:Steve_McQueen.jpg "fig:Steve_McQueen.jpg")在因酒後駕車被捕後，於拍攝[面部照片時舉起V字手勢](../Page/面部照片.md "wikilink")\]\]
手背朝外的V字手势通常會被視為和[豎中指程度相等的侮辱](../Page/豎中指.md "wikilink")。在[蘇格蘭西部和澳大利亞](../Page/蘇格蘭.md "wikilink")，由手腕或肘部向上甩手背朝外的V字手势被稱為「兩指敬禮」，是具侮辱性的\[25\]。而手背朝外的V字手势在英格蘭早已有了具侮辱性的含意\[26\]。因此，在英國、愛爾蘭、紐西蘭和澳大利亞中，手背朝外的V字手势均被限制使用<ref name=V-sign-as-an-insult>。作為侮辱的V字手势：

  - UK: Staff. [No ban for speeding V-sign
    biker](http://news.bbc.co.uk/2/hi/uk_news/england/beds/bucks/herts/7296889.stm)
    [BBC](../Page/BBC.md "wikilink") 2008-03-14
  - UK: Staff. [Two fingers
    Prescott](http://news.bbc.co.uk/vote2001/hi/english/features/newsid_1344000/1344190.stm)
    [BBC](../Page/BBC.md "wikilink"), 2001-05-22
  - IE: Staff. [Shambolic Irish give two fingers to the
    past](http://www.independent.ie/sport/soccer/shambolic-irish-give-two-fingers-to-the-past-135500.html)
    Irish Independent
  - AU: [Karl S.
    Kruszelnicki](http://www.abc.net.au/profiles/content/s2193276.htm?site=science/k2).
    [Arrow Up Yours &
    Plague 1](http://www.abc.net.au/science/articles/2002/10/01/655611.htm?site=science/greatmomentsinscience)
    [www.abc.net.au](http://www.abc.net.au/)
  - NZ: Glyn Harper [Just the
    Answer](http://www.massey.ac.nz/~wwpubafs/magazine/2002_Nov/stories/questions.html)
    Alumni Magazine \[Massey University\] November 2002.

</ref>手背朝外的V字手势經常被用來表示違抗、蔑視或嘲笑<ref>違抗、蔑視或嘲笑：

  - Staff, [V-sign](http://www.encyclopedia.com/doc/1O999-vsign.html),
    [encyclopedia.com](http://www.encyclopedia.com/about.aspx) cites The
    Oxford Pocket Dictionary of Current English 2008
  - Staff. [Hooligan grandson of
    legend](http://www.mirror.co.uk/sport/latest/2007/12/20/hooligan-grandson-of-legend-115875-20260703/),
    [Daily Mirror](../Page/Daily_Mirror.md "wikilink"), 2007-12-20
  - Staff. [V-sign led to assault on school bus
    teens](http://www.yorkpress.co.uk/news/2086046.vsign_led_to_assault_on_school_bus_teens/)
    The Press (York), 2008-03-01</ref>。

作為例子，於1990年11月1日，英國《[太陽報](../Page/太陽報_\(英國\).md "wikilink")》
頭版的一篇標題為「去你的，德洛爾」文章旁有一幅穿著印有[英國國旗的袖口的手](../Page/英國國旗.md "wikilink")，而那隻手正做著手背朝向讀者的V字手势。《太陽報》的目的是敦促讀者反對[歐洲聯盟委員會主席](../Page/歐洲聯盟委員會.md "wikilink")[雅克·德洛爾](../Page/雅克·德洛爾.md "wikilink")，因為他主張建立一個歐盟中央政府。這篇文章遭到很多讀者投訴，但是[英國報業評議會以傳媒在不侵犯英國利益下可以濫俗為由](../Page/英國報業評議會.md "wikilink")，拒絕受理這些投訴\[27\]\[28\]。

### 来源

一個普遍的V字手势起源傳說指出V字手势源自[英法百年戰爭中](../Page/英法百年戰爭.md "wikilink")[阿金庫爾戰役的弓箭手](../Page/阿金庫爾戰役.md "wikilink")。根據此故事指出，由於[法國人鄙視](../Page/法國人.md "wikilink")[英國](../Page/英國.md "wikilink")[弓箭手的低微](../Page/弓箭手.md "wikilink")，戰前宣稱說一旦抓住俘虜，會剁去他們的[中指和](../Page/中指.md "wikilink")[食指讓他們一輩子不能再張弓射箭](../Page/食指.md "wikilink")。但後來[英軍打敗了](../Page/英軍.md "wikilink")[法軍](../Page/法軍.md "wikilink")，勝利後，[英國人伸直](../Page/英國人.md "wikilink")[中指和](../Page/中指.md "wikilink")[食指](../Page/食指.md "wikilink")，掌心向內，向[法國](../Page/法國.md "wikilink")[俘虜示威](../Page/俘虜.md "wikilink")，意思是：我們的手指頭是完整的\[29\]\[30\]\[31\]。但是，至今仍沒有證據顯示當時的法國人有剁去英國弓箭手手指的習慣。當時的法國人會將戰俘集體處決，因為戰俘們沒有價值\[32\]。

最早擁有確鑿證據顯示人們使用V字手势作侮辱的年份為1901年。當時，一位羅瑟勒姆工人向鏡頭擺出V字手势，以表示自己不願意被攝錄\[33\]。

## 表情圖示

<table>
<thead>
<tr class="header">
<th><p>✌</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>U+270C</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Noto_Emoji_Oreo_270c.svg" title="fig:Noto_Emoji_Oreo_270c.svg">Noto_Emoji_Oreo_270c.svg</a><a href="https://zh.wikipedia.org/wiki/File:Twemoji2_270c.svg" title="fig:Twemoji2_270c.svg">Twemoji2_270c.svg</a></p>
<p>不同裝置上的<a href="../Page/繪文字.md" title="wikilink">繪文字</a></p></td>
</tr>
</tbody>
</table>

**✌**（[Unicode](../Page/Unicode.md "wikilink")：U+270C）是一個象徵[和平手勢的](../Page/和平手勢.md "wikilink")[繪文字](../Page/繪文字.md "wikilink")\[34\]，由食指和中指組成的[V字手勢](../Page/V字手勢.md "wikilink")，但習慣上常稱為勝利之手，另外還帶有多種含義。

此外，✌在[英國文化中被認為是一種冒犯的象徵](../Page/英國文化.md "wikilink")，類似於[豎中指](../Page/豎中指.md "wikilink")。

## 參見

  - [Emoji](../Page/Emoji.md "wikilink")

## 參考文獻

## 外部連結

  - V字手势的照片：
      - [戶外的丘吉爾](https://web.archive.org/web/20071202022237/http://www.number-10.gov.uk/files/images/wc%20outside%20D%20st.jpg)
      - [丘吉爾的勝利手勢](https://web.archive.org/web/20070613151055/http://www.number-10.gov.uk/files/images/WC%20V%20sign1.jpg)
      - [尼克松離開辦公室](http://library.educationworld.net/txt18/depart.jpg)
      - [帕沃·韋於呂寧](https://web.archive.org/web/20080817222111/http://kuvat2.iltasanomat.fi/iltasanomat/iDoc/1353189-vayrynen400b.jpg)
      - [Need4Peace](https://web.archive.org/web/20111206043828/http://www.need4peace.com/)

<!-- end list -->

  - 新聞中的V字手势：
      - [2002年6月衛報：圖片中的V字手势](http://www.guardian.co.uk/gall/0,8542,972149,00.html)
      - [2004年6月18日天空新聞：OAP因V字手势罰款100英鎊](https://web.archive.org/web/20040618185303/http://www.sky.com/skynews/article/0%2C%2C30100-13131728%2C00.html)
      - [2009年4月3日英國廣播公司：蘇格蘭足球運動員因V字手势結束職業生涯](http://news.bbc.co.uk/sport2/hi/football/teams/r/rangers/7981287.stm)
  - [都市傳奇參考頁](http://www.snopes.com/language/apocryph/pluckyew.asp)

[Category:表情符號](../Category/表情符號.md "wikilink")
[Category:網路迷因](../Category/網路迷因.md "wikilink")
[Category:日本發明](../Category/日本發明.md "wikilink")
[Category:網上聊天](../Category/網上聊天.md "wikilink")
[Category:信息圖形](../Category/信息圖形.md "wikilink")
[Category:肢體語言](../Category/肢體語言.md "wikilink")
[Category:手势](../Category/手势.md "wikilink")
[Category:和平符号](../Category/和平符号.md "wikilink")

1.
2.  Ronald A. Smith, *Churchill: images of greatness*, 1988, p. 31

3.  [V for Victory: The History of the "V" Sign for Victory and
    Peace](http://voices.yahoo.com/v-victory-history-v-sign-victory-851497.html)


4.  Staff. [American Manual Alphabet
    Chart](http://www.iidc.indiana.edu/cedir/kidsweb/asl.html) Center
    for Disability Information & Referral (CeDIR), Indiana Institute on
    Disability and Community at Indiana University

5.  Eric Patridge, Tom Dalzell, Terry Victor. (2008.) *[The Concise New
    Partridge Dictionary of Slang and Unconventional
    English](http://books.google.com/books?id=7UIjVGcSe8MC&pg=PA683#v=onepage&q&f=false)*,
    Routledge, p. 683. ISBN 0-203-96211-7

6.  [Air quotes](http://www.phrases.org.uk/meanings/air-quotes.html)
    entry on [www.phrases.org.uk](http://www.phrases.org.uk/) by [Gary
    Martin](http://www.phrases.org.uk/gary-martin.html).

7.  [See](http://www.lifeprint.com/asl101/pages-signs/s/see.htm), [ASL
    University](http://www.lifeprint.com/asl101/)

8.  [Cardinal and Ordinal
    numbers](http://www.lifeprint.com/asl101/pages-signs/n/numbersordianlandcardinal.htm)

9.  <http://qph.cf.quoracdn.net/main-qimg-6579398286cfe0ec1698231160b64512>

10. <http://3.bp.blogspot.com/-cHxqAN-RIUg/Tvk0RmJVaTI/AAAAAAAAD_k/N3VVQp77Sv4/s1600/jyj_photowall_black_suits_v_sign.jpg>
    站在紅毯上的南韓男子團體JYJ的成員

11. 音：Yeah

12.
13. <http://dic.pixiv.net/a/%E3%82%A2%E3%83%98%E9%A1%94%E3%83%80%E3%83%96%E3%83%AB%E3%83%94%E3%83%BC%E3%82%B9>

14. 另「啊嘿顏」（）是一種有時會出現在日本動漫或相關衍生作品，尤其十八禁作品中的一種扭曲的快樂表情，以眼神失焦、舌頭吐出為其特徵

15. [The
    V-campaign](https://web.archive.org/web/20050312223729/http://home.luna.nl/~arjan-muil/radio/history/ww-2/v-campaign.html)
    at the virtual radiomuseum

16.
17. C. Sterling, 2003, *Encyclopedia of Radio* London: Taylor and
    Francis, page 359. [at Google
    Books](http://books.google.co.uk/books?id=Z4XJQD4O_TkC&pg=PA359&dq=v+for+victory+campaign+bbc&hl=en&ei=u3XGTbjlL4zA8QPJ7tj2Bw&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCsQ6AEwAA#v=onepage&q=v%20for%20victory%20campaign%20bbc&f=false)

18.

19.

20.

21.

22. Staff. [The V Sign](http://postalheritage.org.uk/page/icons-vsign)
    The British Postal Museum & Archive (BPMA).

23. [Archive video of Charles de Gaulle's speech at the London Albert
    Hall](http://www.ina.fr/art-et-culture/arts-du-spectacle/video/I00011986/discours-de-charles-de-gaulle-a-l-albert-hall.fr.html),
    11 November 1942

24. Staff. [The Japanese Version (the Sign of
    Peace)](https://web.archive.org/web/20080621122852/http://www.icons.org.uk/theicons/collection/the-v-sign/a-harvey-smith-to-you/the-asian-v-sign-in-progress)
    [ICONS. A portrait of
    England](https://web.archive.org/web/20070623145709/http://www.icons.org.uk/about-us).
    Accessed 1 June 2008

25. Tony Keim ["Long tradition of flipping the
    bird"](http://www.couriermail.com.au/lifestyle/tradition-behind-the-bird/story-e6frer4f-1111118071016),
    *Courier Mail*, 2008-11-18

26. Staff [Henry
    V](https://web.archive.org/web/20080607024254/http://www.britishshakespearecompany.com/synopsis.html),
    British Shakespeare Company

27.

28.

29.
30. Glyn Harper [Just the
    Answer](http://www.massey.ac.nz/~wwpubafs/magazine/2002_Nov/stories/questions.html)
    Alumni Magazine \[Massey University\] November 2002.

31. Agincourt: The King, the Campaign, the Battle Pub: Little, Brown.
    ISBN 978-0-316-72648-1 (UK). ISBN 978-0-316-01503-5 (U.S.:
    Agincourt: Henry V and the Battle That Made England (2006)). p. 284

32. David Wilton, *Word Myths: Debunking Linguistic Urban Legends*,
    Oxford University Press, 2008, ISBN 978-0-19-537557-2.

33. Staff. [The V
    sign](https://web.archive.org/web/20081018230141/http://www.icons.org.uk/theicons/collection/the-v-sign/biography/v-for-get-stuffed),
    [www.icons.org.uk](https://web.archive.org/web/20070623145709/http://www.icons.org.uk/about-us).

34.