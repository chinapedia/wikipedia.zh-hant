**天津武备学堂**（），又名**北洋武备学堂**（），位于[天津大直沽](../Page/天津.md "wikilink")，是光绪十一年（1885年）[直隶总督兼](../Page/直隶总督.md "wikilink")[北洋大臣](../Page/北洋大臣.md "wikilink")[李鸿章在兴办](../Page/李鸿章.md "wikilink")[洋务运动中创设](../Page/洋务运动.md "wikilink")，意在通过新式教育培养新式陆军军官等新型军事人才，是中国近代第一所有影响力的综合性陆军军官学校\[1\]。天津武备学堂成立后，清廷更是向各省推广天津武备学堂成功的经验，各地纷纷兴起了新式陆军学校，且这些学校的办学方针、学校体制、教学内容甚至校名均参照其作法。1900年，[八国联军攻陷](../Page/八国联军.md "wikilink")[天津时](../Page/天津.md "wikilink")，天津武备学堂被焚毁。天津武备学堂存在的十五年间，毕业学生多数成为各省新军骨干，其中[段祺瑞](../Page/段祺瑞.md "wikilink")、[冯国璋](../Page/冯国璋.md "wikilink")、[王士珍](../Page/王士珍.md "wikilink")、[曹锟](../Page/曹锟.md "wikilink")、[吴佩孚等成为北洋军阀首领](../Page/吴佩孚.md "wikilink")\[2\]。

## 歷史

[Li_Hung_Chang_in_1896.jpg](https://zh.wikipedia.org/wiki/File:Li_Hung_Chang_in_1896.jpg "fig:Li_Hung_Chang_in_1896.jpg")\]\]
创设天津武备学堂的提议，最早出于[李鸿章属下](../Page/李鸿章.md "wikilink")[周盛波](../Page/周盛波.md "wikilink")、[周盛传兄弟](../Page/周盛传.md "wikilink")\[3\]。[中法战争期清廷重金聘请的](../Page/中法战争.md "wikilink")50名德国军官受到排斥，随着1885年战争结束面临去路问题，为建立天津武备学堂提供了一个直接的契机，既解决了德国军官的去处问题，又可以为新建的武备学堂提供师资\[4\]。

1885年（光绪十一年）正月，李鸿章在天津尝试举办天津武备学堂。李鸿章向直隶提督李长乐、广东水师提督曹克忠、广西提督唐仁廉、四川提督宋庆、总统铭军记名提督刘盛休、正定镇叶志超、通永镇吴育仁、大名镇徐道奎、皖南镇史宏祖等下令，要他们“各选弁兵，送堂肄业”，即选取合适士兵，送到天津武备学堂学习\[5\]。天津武备学堂的首任总办是道员[杨宗濂](../Page/杨宗濂.md "wikilink")。，首期百余学员系从[淮军中选拔的基层军官和优秀士兵](../Page/淮军.md "wikilink")。学堂初设步、马、炮、工程四科，1890年后增设铁路科。军事教习多聘用[德国退役军官](../Page/德国.md "wikilink")。该学堂的学规章程，奠定了晚清陆军学堂内部制度的基础，后为其它各省创办武备学堂所参用。

五月初五日，在武备学堂试办数月后，由[周馥起草](../Page/周馥_\(清朝\).md "wikilink")、由[李鸿章署名上奏了名为](../Page/李鸿章.md "wikilink")《直隶总督李鸿章为天津创设武备学堂拟由海防经费开支等事奏折》的建校的奏折\[6\]。

1889年，天津武备学堂曾派[段祺瑞等](../Page/段祺瑞.md "wikilink")5名学生赴德国留学。

1901年，[辛丑条约签订后](../Page/辛丑条约.md "wikilink")，天津附近20里地区内不得有中国军队驻扎，天津武备学堂复校无望。袁世凯任直隶总督兼北洋大臣后，遂将天津各项军事机构移于保定。

## 教学 

### 军事课程 

天津武备学堂军事课程又称堂内课设置了学习天文、格物、数学、化学等普通课程和兵器、筑城、测绘等军事课程，此外还安排学习外语课与经史课\[7\]。

### 派遣留学

1889年曾派[段祺瑞等](../Page/段祺瑞.md "wikilink")5名学生赴德国留学。另外4人是：[吴鼎元](../Page/吴鼎元.md "wikilink")（后任[新军第5镇](../Page/新军第5镇.md "wikilink")[统制](../Page/统制.md "wikilink")）、[商德全](../Page/商德全.md "wikilink")（后任[清河陆军中学校长](../Page/清河陆军中学.md "wikilink")、天津镇守使）、[滕毓藻](../Page/滕毓藻.md "wikilink")（武备校友）、[孔庆塘](../Page/孔庆塘.md "wikilink")（后任云南临元镇[总兵](../Page/总兵.md "wikilink")）。

## 毕业生

[DuanQirui.jpg](https://zh.wikipedia.org/wiki/File:DuanQirui.jpg "fig:DuanQirui.jpg")\]\]
从开办至1900年毁于八国联军兵火的十五年间，共培养了近一千名学生。[段祺瑞](../Page/段祺瑞.md "wikilink")、[冯国璋](../Page/冯国璋.md "wikilink")、[曹锟](../Page/曹锟.md "wikilink")、[靳云鹏](../Page/靳云鹏.md "wikilink")、[王士珍](../Page/王士珍.md "wikilink")、[段芝贵](../Page/段芝贵.md "wikilink")、[陆建章](../Page/陆建章.md "wikilink")、[李纯](../Page/李纯.md "wikilink")、[李长泰](../Page/李长泰.md "wikilink")、[鲍贵卿](../Page/鲍贵卿.md "wikilink")、[陈光远](../Page/陈光远.md "wikilink")、[王占元](../Page/王占元.md "wikilink")、[田中玉](../Page/田中玉.md "wikilink")、[何宗莲](../Page/何宗莲.md "wikilink")、[张怀芝](../Page/张怀芝.md "wikilink")、[景启](../Page/景启.md "wikilink")、[刘锡钧](../Page/刘锡钧.md "wikilink")、[唐国治](../Page/唐国治.md "wikilink")、李得胜、[杨汝钦](../Page/杨汝钦.md "wikilink")、[崔朝俊](../Page/崔朝俊.md "wikilink")、[韩辉增](../Page/韩辉增.md "wikilink")、[赵学治](../Page/赵学治.md "wikilink")、[何兰芬](../Page/何兰芬.md "wikilink")、[王风岗](../Page/王风岗.md "wikilink")、[孙鸿中](../Page/孙鸿中.md "wikilink")、[丁得胜](../Page/丁得胜.md "wikilink")、[徐邦杰](../Page/徐邦杰.md "wikilink")、[任永清](../Page/任永清.md "wikilink")、[梁华殿](../Page/梁华殿.md "wikilink")、[张锡藩](../Page/张锡藩.md "wikilink")，[刘承恩](../Page/刘承恩.md "wikilink")、[李天保](../Page/李天保.md "wikilink")、[吴凤岭](../Page/吴凤岭.md "wikilink")、[李壬霖](../Page/李壬霖.md "wikilink")、[申保亨](../Page/申保亨.md "wikilink")、[汪本崇](../Page/汪本崇.md "wikilink")、[劳本泉](../Page/劳本泉.md "wikilink")、[傅宪武](../Page/傅宪武.md "wikilink")、[张心全](../Page/张心全.md "wikilink")、[张绍曾](../Page/张绍曾.md "wikilink")、[吴佩孚](../Page/吴佩孚.md "wikilink")、[王者化](../Page/王者化.md "wikilink")、[张文元](../Page/张文元.md "wikilink")、[胡恩光](../Page/胡恩光.md "wikilink")、[吴鼎元](../Page/吴鼎元.md "wikilink")、[孔庆塘](../Page/孔庆塘.md "wikilink")、[滕毓藻等](../Page/滕毓藻.md "wikilink")。

## 參考

  - [保定陸軍軍官學校](../Page/保定陸軍軍官學校.md "wikilink")

## 参考文献

## 外部链接

  - [北洋军事学堂简忆](http://www.hoplite.cn/templates/mgjx0031.html)

[Category:天津军事史](../Category/天津军事史.md "wikilink")
[Category:清朝军事学校](../Category/清朝军事学校.md "wikilink")
[Category:天津教育史](../Category/天津教育史.md "wikilink")
[Category:洋务运动](../Category/洋务运动.md "wikilink")

1.

2.

3.

4.

5.
6.

7.