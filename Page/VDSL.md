[ZyXEL_P-872H_of_Chunghwa_Telecom_20061015.jpg](https://zh.wikipedia.org/wiki/File:ZyXEL_P-872H_of_Chunghwa_Telecom_20061015.jpg "fig:ZyXEL_P-872H_of_Chunghwa_Telecom_20061015.jpg")
**VDSL**（英文：**Very-high-bit-rate digital subscriber
line**），又稱**-{zh-hans:超高速数字用户线路;
zh-hant:超高速數位用戶迴路;}-**，是一种非对称[DSL](../Page/DSL.md "wikilink")，曾是速度最快的[xDSL技術](../Page/DSL.md "wikilink")，顧名思義較[HDSL](../Page/HDSL.md "wikilink")（-{zh-hans:高速数字用户线路;
zh-hant:高速數位用戶迴路;}-）為快，通过一对VDSL设备，用作光纤结点到附近用户的最后引线。VDSL允许用户端利用现有铜线获得高带宽服务而不必采用光纤。VDSL和[ADSL一样](../Page/ADSL.md "wikilink")，是以銅線傳輸的xDSL寬頻解決方案家族成員。可以经一对传统用户[雙絞線在一定服务范围内有效传送下行达](../Page/雙絞線.md "wikilink")12.9Mb/s至52.8Mb/s（實驗室理論值最高可達60Mb/s），上行达1.6Mb/s至2.3Mb/s的数据信息。但比起ADSL離固網機房約4公里的距離限制，VDSL有效傳輸距離只有600公尺，是「[光纖到戶](../Page/FTTH.md "wikilink")」時代前最後一公里的寬頻上網解決方案。

VDSL的缺點是傳輸速度與傳輸距離成反比，配線品質需相當好。而且VDSL目前還沒有一套統一標準，各家所製造的設備互不相容。

而实际上在日韓等國，此技术已经被广泛应用，且上下行速度均可达100Mb/s。

[VDSL2是VDSL第二代](../Page/VDSL2.md "wikilink")，是目前最快之DSL技術，短距離（350公尺以內）上下行均可達100Mb/s；普通環境應用通常可達下行30Mb/s，上行10Mb/s速度。VDSL2兼具VDSL高速與ADSL家族（ADSL/[ADSL2](../Page/ADSL2.md "wikilink")/[2+](../Page/ADSL2+.md "wikilink")）長距之優點\[1\]。

G.Fast是VDSL2的下一代，理論最高速度可以達到1Gb/s，實際商業化服務也可以達到500M/s。

## 版本

| 版本   | 標準名稱                    | Common name                                                              | 下載速率       | 上傳速率       | 應用於        |
| ---- | ----------------------- | ------------------------------------------------------------------------ | ---------- | ---------- | ---------- |
| VDSL | ITU G.993.1             | VDSL                                                                     | 55 Mbit/s  | 3Mbit/s    | 2004-06-13 |
| VDSL | ITU G.993.2             | [VDSL2](../Page/Very_high_speed_digital_subscriber_line_2.md "wikilink") | 100 Mbit/s | 100 Mbit/s | 2006-02-17 |
| VDSL | ITU G.993.2 Amendment 1 | VDSL2-Vplus                                                              | 300 Mbit/s | 100 Mbit/s | 2015-11-06 |

## 參見

  - [ISDN](../Page/ISDN.md "wikilink")（综合服务-{zh-hans:数字;zh-hk:數碼;zh-tw:數位;}-网）
  - [DSL](../Page/DSL.md "wikilink")（數-{zh-hans:字;zh-hk:碼;zh-tw:位;}-用户線路）
  - [ADSL](../Page/ADSL.md "wikilink")（非對稱數碼用戶線路）

## 注釋

<references />

[ja:デジタル加入者線\#VDSL](../Page/ja:デジタル加入者線#VDSL.md "wikilink") [sv:Digital
Subscriber
Line\#VDSL](../Page/sv:Digital_Subscriber_Line#VDSL.md "wikilink")

[Category:數字用戶線路](../Category/數字用戶線路.md "wikilink")

1.