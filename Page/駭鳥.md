**駭鳥**（[學名Phorusrhacidae](../Page/學名.md "wikilink")）是一類大型的[肉食性及不能飛行的](../Page/肉食性.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")，於6200萬-200萬年前[新生代的](../Page/新生代.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")，是非常普遍的獵食者。\[1\]牠們約有1-3米高。

本物種最初被歸入[鶴形目](../Page/鶴形目.md "wikilink")，而當中現今最接近牠們的相信是[叫鶴科](../Page/叫鶴科.md "wikilink")，後來駭鳥科、叫鶴科及其他相關連的化石物種從鶴形目獨立出來成為[叫鶴目](../Page/叫鶴目.md "wikilink")（Cariamiformes），是[地鳥亞綱](../Page/地鳥亞綱.md "wikilink")[澳大利亞鳥下綱的成員之一](../Page/澳大利亞鳥下綱.md "wikilink")。

[泰坦巨鳥](../Page/泰坦巨鳥.md "wikilink")（*Titanis
walleri*）是駭鳥中一種較大的[物種](../Page/物種.md "wikilink")，是唯一一種在南美洲以外的[北美洲發現](../Page/北美洲.md "wikilink")。在300萬年前形成[巴拿馬地峽時出現的](../Page/巴拿馬地峽.md "wikilink")[南北美洲生物大迁徙](../Page/南北美洲生物大迁徙.md "wikilink")，牠是當中唯一的大型獵食者。泰坦巨鳥的祖先不明，不過可以知道在北美洲會有更多發現。

駭鳥的體型巨大，是高階的獵食者，且與其他可怕的[肉食性動物一同棲息](../Page/肉食性.md "wikilink")。牠們的翼[演化成像肉鉤的結構](../Page/演化.md "wikilink")，可以像手臂般伸出，能協助狩捕獵物。大部份細小及一些大型的物種被认为善於奔跑。

## 分類

駭鳥其下有5個[亞科](../Page/亞科.md "wikilink")。根據 Alvarenga
(2003)，本科包含了13個[屬及](../Page/屬.md "wikilink")17個[種](../Page/種.md "wikilink")\[2\]；到2003年之後，又有多兩個新的屬加入本科。現時本科合共有15個屬，分別如下：

  - [雷鳴鳥亞科](../Page/雷鳴鳥亞科.md "wikilink")（Brontornithinae）
      - [雷鳴鳥屬](../Page/雷鳴鳥屬.md "wikilink")（*Brontornis*）
          - [梅氏雷鳴鳥](../Page/梅氏雷鳴鳥.md "wikilink")（*B. burmeisteri*）
      - *Physornis*
          - *P. fortis*
      - [副雷鳴鳥屬](../Page/副雷鳴鳥屬.md "wikilink")（*Paraphysornis*）
          - [巴西副雷嗚鳥](../Page/巴西副雷嗚鳥.md "wikilink")（*P. brasiliensis*）
  - [恐鶴亞科](../Page/恐鶴亞科.md "wikilink")（Phorusrhacinae）
      - [恐鶴屬](../Page/恐鶴屬.md "wikilink")（*Phorusrhacos*）
          - [長腿恐鶴](../Page/長腿恐鶴.md "wikilink")（*P. longissimus*）
      - *Devincenzia*
          - *D. pozzi*
      - [泰坦鳥屬](../Page/泰坦鳥屬.md "wikilink")（*Titanis*）
          - [泰坦巨鳥](../Page/泰坦巨鳥.md "wikilink")（*T. walleri*）
      - [竊鶴屬](../Page/竊鶴屬.md "wikilink")（*Kelenken*）
          - [吉氏竊鶴](../Page/吉氏竊鶴.md "wikilink")（*K. guillermoi*）
  - [巴塔哥尼亞鳥亞科](../Page/巴塔哥尼亞鳥亞科.md "wikilink")（Patagornithinae）
      - [巴塔哥尼亞鳥屬](../Page/巴塔哥尼亞鳥屬.md "wikilink")（*Patagornis*）
          - [馬氏巴塔哥尼亞鳥](../Page/馬氏巴塔哥尼亞鳥.md "wikilink")（*P. marshi*）
      - *Andrewsornis*
          - *A. abbotti*
      - [安達爾加拉鳥屬](../Page/安達爾加拉鳥屬.md "wikilink")（*Andalgalornis*）
          - [史氏安達爾加拉鳥](../Page/史氏安達爾加拉鳥.md "wikilink")（*A. steulleti*）
  - **Psilopterinae**
      - *Psilopterus*
          - *P. bachmanni*
          - *P. lemoinei*
          - *P. affinis*
          - *P. colzecus*
      - *Procariama*
          - *P. simplex*
      - *Paleopsilopterus*
          - *P. itaboraiensis*
  - **Mesembriornithinae**
      - *Mesembriornis*
          - *M. milneedwardsi*
          - *M. incertus*
      - [壯麗鳥屬](../Page/壯麗鳥屬.md "wikilink") *Llallawavis*\[3\]\[4\]

這個分類沒有包括於[歐洲發現的Sophiornithidae](../Page/歐洲.md "wikilink")，這類動物後來發現是[貓頭鷹的原始親屬](../Page/貓頭鷹.md "wikilink")。\[5\]

## 參考

## 外部連結

  - [Hooper
    Museum](http://hoopermuseum.earthsci.carleton.ca/flightless/phorus.htm)
  - ["Huge 'Terror Bird' Fossil Discovered in Patagonia" by Christopher
    Joyce of
    NPR](http://www.npr.org/templates/story/story.php?storyId=6381194)
  - [Palaeontology: Skull morphology of giant terror
    birds](http://www.nature.com/nature/journal/v443/n7114/edsumm/e061026-04.html)
  - [Terror Birds: Bigger and
    Faster](http://sciencenow.sciencemag.org/cgi/content/full/2006/1025/4?etoc)
  - [Darren Naish: Tetrapod Zoology: "terror
    birds"](http://darrennaish.blogspot.com/2006/10/terror-birds.html)

[\*](../Category/駭鳥科.md "wikilink") [P](../Category/已滅絕鳥類.md "wikilink")
[Category:南美洲史前历史](../Category/南美洲史前历史.md "wikilink")

1.
2.
3.
4.
5.