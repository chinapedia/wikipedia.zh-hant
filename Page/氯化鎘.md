**氯化鎘**是一種无色[单斜晶系晶体](../Page/单斜晶系.md "wikilink")。[分子式為CdCl](../Page/分子式.md "wikilink")<sub>2</sub>，极易溶于[水](../Page/水.md "wikilink")，微溶于[甲醇](../Page/甲醇.md "wikilink")、[乙醇](../Page/乙醇.md "wikilink")，难溶于[丙酮](../Page/丙酮.md "wikilink")、[乙醚](../Page/乙醚.md "wikilink")。

## 制备

氯化镉一般可以用[氯化氫和](../Page/氯化氫.md "wikilink")[鎘在](../Page/鎘.md "wikilink")450℃反應製成：

\[\ Cd+2HCl\to CdCl_2+H_2\]
也可以通过[乙酸镉的冰](../Page/乙酸镉.md "wikilink")[乙酸溶液和](../Page/乙酸.md "wikilink")[乙酰氯反应得到](../Page/乙酰氯.md "wikilink")，产生的白色沉淀即为产物：\[1\]

  -
    \(\ Cd(CH_3COO)_2+2CH_3COCl\to CdCl_2\downarrow+2(CH_3CO)_2O\)

## 用途

氯化鎘可用於製造[硫化鎘](../Page/硫化鎘.md "wikilink")：

\[\ CdCl_2+H_2S\to CdS+2HCl\]

## 参考资料

[Category:镉化合物](../Category/镉化合物.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:金属卤化物](../Category/金属卤化物.md "wikilink")

1.  朱文祥. 无机化合物制备手册. 化学工业出版社. pp 384. 【XII-26】氯化镉（cadmium chloride）