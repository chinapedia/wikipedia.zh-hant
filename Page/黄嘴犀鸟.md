**黃嘴犀鳥**（[學名](../Page/學名.md "wikilink")：***Tockus
leucomelas***），是一種棲息於[非洲的](../Page/非洲.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")，屬於[犀鳥科](../Page/犀鳥科.md "wikilink")[彎嘴犀鳥屬](../Page/彎嘴犀鳥屬.md "wikilink")。黃嘴犀鳥是一種中型鳥類，體長約48至60公分，特徵是黃色的長喙，以及只有雄性才有的盔突。眼睛及兩頰的皮膚帶著明顯的紅色。

黃嘴犀鳥的腹部是白色，頸部是灰色。牠的背部是黑色，但帶有大量的白色斑點和條紋。黃嘴犀鳥主要在地面覓食，牠們吃植物的[種子](../Page/種子.md "wikilink")、小[昆蟲](../Page/昆蟲.md "wikilink")，以及[蜘蛛和](../Page/蜘蛛.md "wikilink")[蠍子](../Page/蠍子.md "wikilink")。在乾季時，牠們則較喜歡吃[白蟻和](../Page/白蟻.md "wikilink")[螞蟻](../Page/螞蟻.md "wikilink")。

雌鳥一次會下3到4個白色的蛋在巢中，並且照顧那些蛋約25天直到孵化。雛鳥大約要過45天才會成熟。

黃嘴犀鳥主要分布在非洲南部。\[1\]

## 图集

<File:Tockus> leucomelas -Kruger National
Park-8.jpg|位于[克留格爾國家公園](../Page/克留格爾國家公園.md "wikilink")
<File:Hornbill> Zazu Chitwa South Africa Luca Galuzzi 2004.JPG|成年雌鸟
<File:Tockus> leucomelas -Kalahari Desert, Botswana
-8.jpg|位于[波札那](../Page/波札那.md "wikilink")[喀拉哈里沙漠的成年雄鸟](../Page/喀拉哈里沙漠.md "wikilink")
<File:Southern> yellow-billed hornbill flying.jpg|飞行中 <File:Southern>
Yellow Billed Hornbill Feeding on caterpillar.jpg|捕食 <File:Hornbill>
synthBG.jpg|[匹兰斯堡狩猎保护区的成年雄鸟](../Page/匹兰斯堡狩猎保护区.md "wikilink")
<File:Southern> Yellow-Billed Hornbill.jpg|克留格爾國家公園

## 參考資料

[Category:犀鳥科](../Category/犀鳥科.md "wikilink")
[Category:非洲鳥類](../Category/非洲鳥類.md "wikilink")

1.