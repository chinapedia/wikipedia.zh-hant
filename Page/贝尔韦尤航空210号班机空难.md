**[贝尔韦尤航空](../Page/贝尔韦尤航空公司.md "wikilink")210號班機**（[英語](../Page/英語.md "wikilink")：Bellview
Airlines Flight
210），是从[尼日利亚经济首都](../Page/尼日利亚.md "wikilink")[拉各斯](../Page/拉各斯.md "wikilink")[穆爾塔拉·穆罕默德國際機場飞往首都](../Page/穆爾塔拉·穆罕默德國際機場.md "wikilink")[阿布贾](../Page/阿布贾.md "wikilink")[納姆迪·阿齊基韋國際機場的定期客運航班](../Page/納姆迪·阿齊基韋國際機場.md "wikilink")，使用[波音737-200客机](../Page/波音737-200.md "wikilink")，飞行时间应50分钟。2005年10月22日当地时间22日晚8点45分，载有111名乘客和6名机组人员的班机从穆爾塔拉·穆罕默德国际机场起飞后不久坠毁，機上111名乘客和6名機組人員全數死亡。

## 事故經過

該架發機係於起飛後3分钟失踪。後來尼日利亚航空当局证实，该飞机已坠毁。尼日利亚总统办公室透露，出事飞机上载有多名高级官员，其中包括邮政局长，他们原定乘飞机前往该国政治首都阿布贾参加一场会议。尼日利亚总统府的一位人士称，失踪客机的飞行员曾在飞机起飞后数分钟发出过求救信号，这表明飞机曾遇到技术问题。尼日利亚新闻部长说，客机坠毁地点位于拉各斯以北约30公里[奥貢州埃佛附近的利萨村](../Page/奥貢州.md "wikilink")。赶赴坠机现场抢救的[红十字会人员称](../Page/红十字会.md "wikilink")，在失事现场没有发现生还者，到处都是飞机的残骸和遇难者的遗体。[西非经济共同体发言人证实](../Page/西非经济共同体.md "wikilink")，该组织负责政治、防卫和安全的副执行秘书奥马尔·迪亚拉在飞机上。

尼日利亚民航总局局长奥耶里里称，失事客机的机龄为24年，它于2005年2月通过了全面的技术检测，技术检测的有效期是18个月。飞机在起飞前十小时还进行过一次额外的检测。坠机现场的一名最高级别警官称，尼日利亚警方已找到了坠毁客机上的[飞行纪录仪](../Page/飞行纪录仪.md "wikilink")。当地村民说，他们看见这架飞机坠落前在空中发生[爆炸](../Page/爆炸.md "wikilink")。

当地警方说，空难发生后，坠机现场出现大量[洗劫者](../Page/洗劫者.md "wikilink")，他们从遇难者身上拿走现金与值钱的物品。

## 原因

貝爾韋尤航空於調查進行期間被勒令中止所有從拉各斯起飛的航班。出事的航班於起飛後不久就發出求救訊號，隨後在無線電通訊中聽見一聲強烈的[雷電聲](../Page/雷電.md "wikilink")，飛機便失去了聯絡。[波音派出工程師及調查人員前往尼日利亞協助當地政府調查](../Page/波音.md "wikilink")，他們初步指出，飛機的[維修](../Page/維修.md "wikilink")[保養不足](../Page/保養.md "wikilink")。而飛機起飛後，因被[閃電擊中右邊機翼](../Page/閃電.md "wikilink")，立即引致右邊[引擎著火](../Page/引擎.md "wikilink")，而後飛機向左傾側及失去控制，最終[失速墜毀](../Page/失速.md "wikilink")。

## 外部链接

  - [Report on the Accident involving Bellview Airlines Ltd B737 200
    Reg. 5N BFN at Lisa Village, Ogun State, Nigeria On 22
    October 2005](http://aib.gov.ng/reports/2-2009-BLV-2005-10-22-F.pdf)
    ([Archive](http://www.webcitation.org/6SDIFixoC)). .

  - "[尼日利亚客机坠毁](http://news.sina.com.cn/z/nigcrash05/index.shtml)"　[新浪网专题](../Page/新浪.md "wikilink")

[Category:2005年航空事故](../Category/2005年航空事故.md "wikilink")
[Category:奈及利亞航空事故](../Category/奈及利亞航空事故.md "wikilink")
[Category:天氣因素觸發的航空事故](../Category/天氣因素觸發的航空事故.md "wikilink")
[Category:維修失誤觸發的航空事故](../Category/維修失誤觸發的航空事故.md "wikilink")