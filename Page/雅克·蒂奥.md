[Jacques_Thibaud_01.jpg](https://zh.wikipedia.org/wiki/File:Jacques_Thibaud_01.jpg "fig:Jacques_Thibaud_01.jpg")

**雅克·蒂奥**（，）是一位[法国](../Page/法国.md "wikilink")[小提琴家](../Page/小提琴家.md "wikilink")。

## 生平

蒂奥出生在[波尔多](../Page/波尔多.md "wikilink")，在13岁进入[巴黎国立高等音乐学院](../Page/巴黎国立高等音乐学院.md "wikilink")(Conservatorie
de
Paris)之前跟随其父学习[小提琴](../Page/小提琴.md "wikilink")。1896年他同[皮埃尔·蒙特](../Page/皮埃尔·蒙特.md "wikilink")(Pierre
Monteux)（后来成为了著名的[指挥家](../Page/指挥家.md "wikilink")）联手获得了学院的小提琴奖。他在[第一次世界大战的战斗中不幸受伤](../Page/第一次世界大战.md "wikilink")，因此随后他不得不重操旧业。1943年他与[玛格丽特·朗](../Page/玛格丽特·朗.md "wikilink")(Marguerite
Long)联手创立了一项小提琴和钢琴的大赛，玛格丽特·朗-雅克·蒂奥国际比赛。
同时作为一名独奏者，蒂奥也是一位著名的[室内乐演奏家](../Page/室内乐.md "wikilink")，犹其是同钢琴家[阿尔弗雷德·科尔托](../Page/阿尔弗雷德·科尔托.md "wikilink")(Alfred
Cortot)和大提琴家[帕伯洛·卡薩爾斯](../Page/帕伯洛·卡薩爾斯.md "wikilink")(Pablo
Casals)组成的[钢琴三重奏](../Page/钢琴三重奏.md "wikilink")。
1953年，当他搭乘[法国航空公司](../Page/法国航空公司.md "wikilink")178航班时，飞机撞上了[法国阿尔卑斯山](../Page/法国阿尔卑斯山.md "wikilink")，机上人员全部遇难。他的那把1720年[斯特拉迪瓦里琴也随之化为灰烬](../Page/斯特拉迪瓦里琴.md "wikilink")\[1\]
。經過事後調查，[法國航空178號班機空難為](../Page/法國航空178號班機空難.md "wikilink")[可控飛行撞地](../Page/可控飛行撞地.md "wikilink")\[2\]。

## 參考資料

[Category:法國小提琴家](../Category/法國小提琴家.md "wikilink")
[Category:法國音樂家](../Category/法國音樂家.md "wikilink")
[Category:波爾多人](../Category/波爾多人.md "wikilink")
[Category:法國空難身亡者](../Category/法國空難身亡者.md "wikilink")

1.  \*
2.