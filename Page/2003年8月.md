[2003年](../Page/2003年.md "wikilink")[8月的新闻事件](../Page/8月.md "wikilink")：

**请参看：**

  - [2003年美加大停电](../Page/2003年美加大停电.md "wikilink")
  - [中东和平路线图](../Page/中东和平路线图.md "wikilink")
  - [欧盟扩大](../Page/欧盟.md "wikilink")
  - [北朝鲜核危机](../Page/北朝鲜核危机.md "wikilink")
  - [恐怖主义](../Page/恐怖主义.md "wikilink")
  - [美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")
  - [利比里亚](../Page/利比里亚.md "wikilink")
  - [钓鱼岛](../Page/钓鱼岛.md "wikilink")
  - [2003年侵华日军遗弃在华化学毒剂泄漏事件](../Page/2003年侵华日军遗弃在华化学毒剂泄漏事件.md "wikilink")
  - [2004年中华民国总统大选](../Page/2004年中华民国总统大选.md "wikilink")

## [8月28日](../Page/8月28日.md "wikilink")

  - [塔利班领导人](../Page/塔利班.md "wikilink")[奥马尔率领](../Page/奥马尔.md "wikilink")800名战士在阿南部露面。[1](http://www.chinanews.com.cn/n/2003-08-28/26/340331.html)
  - [印度](../Page/印度.md "wikilink")“[昆梅拉节](../Page/昆梅拉节.md "wikilink")”发生人群踩踏事件，45人丧生。[2](http://news.xinhuanet.com/world/2003-08/28/content_1051073.htm)
  - [2003年美加大停電](../Page/2003年美加大停電.md "wikilink")：美加停电调查初步结果显示，人为失误引发事故。[3](http://news.xinhuanet.com/world/2003-08/28/content_1050024.htm)

## [8月27日](../Page/8月27日.md "wikilink")

  - [火星大冲](../Page/火星大冲.md "wikilink")。

## [8月26日](../Page/8月26日.md "wikilink")

  - [北朝鲜核危机](../Page/北朝鲜核危机.md "wikilink")：朝核问题六方会谈代表团陆续抵达[北京](../Page/北京.md "wikilink")。[4](http://www.xinhuanet.com/newscenter/index.htm)
  - [B-HRX墜毀事故](../Page/B-HRX墜毀事故.md "wikilink")：[香港](../Page/香港.md "wikilink")[政府飛行服務隊一架](../Page/政府飛行服務隊.md "wikilink")[EC155
    B1海豚直升機](../Page/EC155_B1.md "wikilink")（飛機登記編號**B-HRX**）前往執行甲類疏散運送死傷者任務（Type
    A
    CASEVAC）時，於[大嶼山伯公坳附近發生](../Page/大嶼山.md "wikilink")[可控飛行撞地](../Page/可控飛行撞地.md "wikilink")，兩名機組人員殉職。

## [8月25日](../Page/8月25日.md "wikilink")

  - 中国卫生部常务副部长[高强向全国人大常委会报告当前重大传染性疾病防治工作情况时说](../Page/高强.md "wikilink")，目前[中国正值](../Page/中国.md "wikilink")[艾滋病发病和死亡高峰](../Page/艾滋病.md "wikilink")。
    [5](http://news.xinhuanet.com/newscenter/2003-08/25/content_1044257.htm)
  - [日本右翼团体](../Page/日本.md "wikilink")9名成员再次登上[钓鱼岛](../Page/钓鱼岛.md "wikilink")，[中国表示强烈抗议](../Page/中国.md "wikilink")。[6](http://news.xinhuanet.com/world/2003-08/25/content_1043429.htm)

## [8月20日](../Page/8月20日.md "wikilink")

  - [伊拉克首都的](../Page/伊拉克.md "wikilink")[联合国机构受到炸弹袭击](../Page/联合国.md "wikilink")，造成至少17人死亡，其中包括伊拉克问题特别顾问德梅格，另外有100多人受伤。[美国](../Page/美国.md "wikilink")、[中国](../Page/中国.md "wikilink")、[欧盟](../Page/欧盟.md "wikilink")、[拉美南方共同体国家等都强烈谴责这一](../Page/拉美南方共同体.md "wikilink")[恐怖袭击](../Page/恐怖主义.md "wikilink")。[7](http://www.msnbc.com/news/870749.asp?0cv=CA00)[8](http://news.xinhuanet.com/world/2003-08/20/content_1034443.htm)
    [9](http://news.bbc.co.uk/1/hi/world/middle_east/3163877.stm)
    [10](http://www.cbsnews.com/stories/2003/02/24/iraq/main541815.shtml)
    [11](http://www.usatoday.com/news/world/iraq/2003-08-19-iraq-unblast_x.htm)
  - [中东和平路线图](../Page/中东和平路线图.md "wikilink")：[耶路撒冷发生炸点袭击事件](../Page/耶路撒冷.md "wikilink")，造成至少20人死亡，100多人受伤。[12](http://news.xinhuanet.com/world/2003-08/20/content_1035425.htm)[13](http://www.reuters.com/newsArticle.jhtml?type=topNews&storyID=3302933)
    [14](https://web.archive.org/web/20030909171349/http://reuters.com/newsArticle.jhtml?type=topNews&storyID=3303310)
    [15](http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/1061318328538_25///?hub=TopStories)
  - [美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")：[伊拉克前副总统](../Page/伊拉克.md "wikilink")[塔哈·亚辛·拉马丹被美军逮捕](../Page/塔哈·亚辛·拉马丹.md "wikilink")。[16](http://abcnews.go.com/wire/World/ap20030819_261.html)
    [17](http://www.cnn.com/2003/WORLD/meast/08/19/sprj.irq.main/index.html)
    [18](http://news.bbc.co.uk/1/hi/world/middle_east/3163219.stm)

## [8月19日](../Page/8月19日.md "wikilink")

  - 一个与[阿尔盖达组织有关的组织发表一个声明为](../Page/阿尔盖达组织.md "wikilink")[大停电事件负责](../Page/2003美加大停电.md "wikilink")，声明表示他们是根据[本拉丁的指示发起的行动](../Page/本拉丁.md "wikilink")。（[19](http://gandalf.ics.uci.edu/blog/2003/08/al_qaeda_claims_responsibility_for_power_blackout.html)，[20](http://english.daralhayat.com/arab_news/08-2003/Article-20030818-14bdd659-c0a8-01ed-0079-6e1c903b7552/story.html)）
  - [俄塔社报道](../Page/俄塔社.md "wikilink")，[俄罗斯](../Page/俄罗斯.md "wikilink")[海军将于](../Page/海军.md "wikilink")18日在[远东及](../Page/远东.md "wikilink")[太平洋地区开始](../Page/太平洋.md "wikilink")[战略演习](../Page/战略演习.md "wikilink")。[21](http://news.xinhuanet.com/world/2003-08/19/content_1033171.htm)[22](http://news.xinhuanet.com/world/2003-08/19/content_1032306.htm)
  - [美国军方承认美国部队意外杀死一名在](../Page/美国.md "wikilink")[伊的](../Page/伊拉克.md "wikilink")[路透社记者](../Page/路透社.md "wikilink")。中央司令部发言人表示这是一个悲剧事件。[23](https://web.archive.org/web/20030922083130/http://www.msnbc.com/news/951994.asp?0cv=CB10&cp1=1)

## [8月18日](../Page/8月18日.md "wikilink")

  - [朝鲜](../Page/朝鲜.md "wikilink")[中央通讯社](../Page/中央通讯社.md "wikilink")18日发表社论批评日本部分政界人士15日到[靖国神社进行参拜](../Page/靖国神社.md "wikilink")。[24](http://news.xinhuanet.com/world/2003-08/19/content_1032298.htm)

## [8月17日](../Page/8月17日.md "wikilink")

  - [2003年美加大停電](../Page/2003年美加大停電.md "wikilink")：調查人相信這次大停電開始于[俄亥俄州](../Page/俄亥俄州.md "wikilink")。在美國供應超過140万人的[第一能源公司](../Page/第一能源公司.md "wikilink")（FirstEnergy
    Corporation），星期六發表一份報告說在東湖發電廠（Eastlake
    Plant）第五單位在大停電前曾經有三條綫路出現故障，這個能是造成大停電的原因。[25](http://www.cnn.com/2003/US/08/17/power.outage/index.html)

## [8月16日](../Page/8月16日.md "wikilink")

  - [2003年美加大停電](../Page/2003年美加大停電.md "wikilink")：電力供應在[紐約市](../Page/紐約市.md "wikilink")、[多倫多已經恢復](../Page/多倫多.md "wikilink")，[渥太華大部分已經恢復](../Page/渥太華.md "wikilink")。但是專家警告未來仍有停電的可能。[26](http://www.statesman.com/asection/content/auto/epaper/editions/saturday/news_f3d37d90212c526210d1.html)

## [8月15日](../Page/8月15日.md "wikilink")

  - [侵华日军遗弃在华化学毒剂泄漏事件](../Page/侵华日军遗弃在华化学毒剂泄漏事件.md "wikilink")：[芥子毒气受害者已达](../Page/芥子毒气.md "wikilink")49人[27](http://news.sina.com.cn/c/2003-08-15/09201547679.shtml)。毒气受害者及家属要求日方赔偿，并得到众多华人的支持。这起侵华日军遗弃在华的化学毒剂泄漏事件终将以“[8-4事件](../Page/8-4事件.md "wikilink")”的名称记入史册[28](http://news.sina.com.cn/z/yperite/index.shtml)[29](http://news.xinhuanet.com/newscenter/2003-08/10/content_1018910.htm)[30](https://web.archive.org/web/20030812010906/http://news.163.com/editor/030810/030810_770509.html)。

## [8月14日](../Page/8月14日.md "wikilink")

  - [2003年美加大停電](../Page/2003年美加大停電.md "wikilink")：[美國東部和](../Page/美國.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[安大略省當地時間](../Page/安大略省.md "wikilink")8月14日下午出現大範圍停電，涉及地區包括[紐約市](../Page/紐約市.md "wikilink")、[新澤西](../Page/新澤西.md "wikilink")、[渥太華](../Page/渥太華.md "wikilink")、[多倫多等](../Page/多倫多.md "wikilink")。城市交通曾一度陷於癱瘓。紐約市宣佈進入緊急狀態。[美國總統](../Page/美國總統.md "wikilink")[乔治·沃克·布什表示這不是恐怖襲擊](../Page/乔治·沃克·布什.md "wikilink")。加拿大方面表示是由於電廠受到電擊造成，但具體原因還沒有查明。[31](http://www.sunspot.net/news/nationworld/bal-newyork0815,0,2936608.story?coll=bal-home-headlines)[32](http://www.washingtonpost.com/wp-dyn/articles/A59083-2003Aug14.html)[33](http://www.cjad.com/content/cjad_news/article.asp?id=n081461A)、[ABC](http://abcnews.go.com/sections/us/DailyNews/power_outage030814.html)、[BBC](http://news.bbc.co.uk/2/hi/americas/3152451.stm)、[CNN](http://www.cnn.com/2003/US/08/14/power.outage/index.html)，據報道已經有9傢[核電厰安全關閉](../Page/核電厰.md "wikilink")。
  - [歐洲熱浪](../Page/熱浪.md "wikilink")：[法國衛生官員表示估計有多達](../Page/法國.md "wikilink")3,000人死于持續高溫天氣引起的各種疾病。法國健康系統的死亡率和致病率都在上升。[巴黎已經緊急啓動](../Page/巴黎.md "wikilink")*Plan
    blanc*計劃以應對情況。巴黎的氣溫已經從40 °C下降到30 °C。[34](http://www.cbc.ca/stories/2003/08/14/heat_france030814)

## [8月13日](../Page/8月13日.md "wikilink")

  - [北朝鲜核危机](../Page/北朝鲜核危机.md "wikilink")：[朝鲜首次向](../Page/朝鲜.md "wikilink")[美国提出解决核问题三条件](../Page/美国.md "wikilink")。[35](http://www.phoenixtv.com/home/news/world/200308/13/96573.html)

## [8月12日](../Page/8月12日.md "wikilink")

  - [辽宁省副省长](../Page/辽宁省.md "wikilink")[刘克田被](../Page/刘克田.md "wikilink")[双规](../Page/双规.md "wikilink")。[36](http://www.phoenixtv.com/home/news/Inland/200308/13/96682.html)
  - [美軍佔領伊拉克](../Page/美軍佔領伊拉克.md "wikilink")：[聯合社報道根據美軍指揮官的敍述](../Page/聯合社.md "wikilink")，在伊美軍將駐守伊拉克至少一年時閒[37](http://www.dunnconnect.com/articles/2003/08/12/ap/Headlines/apnews126737-01.txt)[38](http://www.bismarcktribune.com/articles/2003/08/12/ap/Headlines/apnews126737-01.txt)[39](http://abcnews.go.com/wire/World/ap20030812_1015.html)[40](http://www.guardian.co.uk/worldlatest/story/0,1280,-3019115,00.html)[41](http://www.ajc.com/news/content/news/ap/ap_story.html/Intl/AP.V9307.AP-Iraq-US-Troops.html)[42](http://www.fredericksburg.com/News/apmethods/apstory?urlfeed=D7SSID000.xml)。

## [8月11日](../Page/8月11日.md "wikilink")

  - [歐洲熱浪](../Page/熱浪.md "wikilink")：[巴黎衛生官員表示已經有](../Page/巴黎.md "wikilink")50人死于持續高溫引發的各種疾病，特別是老年人。[43](https://web.archive.org/web/20030812223105/http://www.globeandmail.com/servlet/story/RTGAM.20030810.wheat0811/BNStory/International/)
  - [利比里亞危機](../Page/利比里亞危機.md "wikilink")：利比里亞總統[查理斯·泰勒褫職](../Page/查理斯·泰勒.md "wikilink")。[44](https://web.archive.org/web/20031011163314/http://www.globeandmail.com/servlet/story/RTGAM.20030811.wliber0811_4/BNStory/International/)

## [8月7日](../Page/8月7日.md "wikilink")

  - [恐怖主义](../Page/恐怖主义.md "wikilink")：[约旦驻伊](../Page/约旦.md "wikilink")[大使馆发生爆炸](../Page/大使馆.md "wikilink")，至少造成11死40伤。[45](http://www.phoenixtv.com/home/news/world/200308/07/93894.html)
  - [2004年中華民國總統大選](../Page/2004年中華民國總統大選.md "wikilink")：[陈水扁仍称](../Page/陈水扁.md "wikilink")“一边一国”为次年大选定调。[46](http://www.phoenixtv.com/home/news/taiwan/200308/07/93902.html)
  - 十五国再提案支持[台湾加入](../Page/台湾.md "wikilink")[联合国](../Page/联合国.md "wikilink")。[47](http://www.phoenixtv.com/home/news/taiwan/200308/07/93524.html)

## [8月6日](../Page/8月6日.md "wikilink")

  - [上海合作组织举行首次多国联合](../Page/上海合作组织.md "wikilink")[军事演习](../Page/军事演习.md "wikilink")。[48](http://news.xinhuanet.com/mil/2003-08/06/content_1012723.htm)
  - 台当局称将与[日本就](../Page/日本.md "wikilink")[钓鱼岛海域开发问题再次讨论](../Page/钓鱼岛.md "wikilink")[49](http://www.chinanews.com.cn/n/2003-08-06/26/332504.html)

## [8月5日](../Page/8月5日.md "wikilink")

  - [中国今年人工繁育成活第一只](../Page/中国.md "wikilink")[大熊猫](../Page/大熊猫.md "wikilink")。[50](http://news.xinhuanet.com/newscenter/2003-08/05/content_1010775.htm)
  - 位于印尼首都[雅加达中区南部的五星级万豪大酒店遭炸弹袭击](../Page/雅加达.md "wikilink")。
  - [南京数名](../Page/南京.md "wikilink")[记者采访教育厅遭群殴](../Page/记者.md "wikilink")[51](http://www.people.com.cn/GB/shehui/8217/29024/index.html)

## [8月4日](../Page/8月4日.md "wikilink")

  - 韩国现代峨山公司董事长郑梦宪4日在公司总部跳楼自杀。[52](http://news.xinhuanet.com/world/2003-08/04/content_1008614.htm)
  - [黑龙江省](../Page/黑龙江.md "wikilink")[齐齐哈尔市发生](../Page/齐齐哈尔.md "wikilink")[侵华日军遗弃在华化学毒剂泄漏事件](../Page/侵华日军遗弃在华化学毒剂泄漏事件.md "wikilink")，目前已造成36人中毒住院[53](http://news.xinhuanet.com/newscenter/2003-08/10/content_1018910.htm)

## [8月3日](../Page/8月3日.md "wikilink")

  - [俄罗斯](../Page/俄罗斯.md "wikilink")[莫兹多克市军医院爆炸现场的救援工作已经结束](../Page/莫兹多克市.md "wikilink")，共发现50具尸体。[54](http://news.xinhuanet.com/world/2003-08/03/content_1007648.htm)
  - [美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")：美军在伊进行搜捕，共逮捕近50名[萨达姆政权支持者](../Page/萨达姆.md "wikilink")。[55](http://news.xinhuanet.com/world/2003-08/04/content_1008038.htm)

## [8月2日](../Page/8月2日.md "wikilink")

  - [联合国批准向](../Page/联合国.md "wikilink")[利比里亚派遣国际和平部队以维护当地秩序](../Page/利比里亚.md "wikilink")。而美国的坚持在利比里亚服役的美军可以获得免受战争法起诉的豁免权则受到批评。
  - 科学家表示，由于国际对碳氟化合物的禁令，[臭氧层开始表现出复原的迹象](../Page/臭氧层.md "wikilink")。[56](https://web.archive.org/web/20030821054606/http://news.independent.co.uk/world/environment/story.jsp?story=429802)
  - [皇家马德里足球队与中国龙之队进行的首场友好比赛中](../Page/皇家马德里.md "wikilink")，皇家马德里以4:0大胜龙之队。[57](http://news.xinhuanet.com/world/2003-08/04/content_1008604.htm)
  - [俄罗斯一家医院发生爆炸](../Page/俄罗斯.md "wikilink")，至少造成20人死亡。该医院中很多是在[车臣地区与叛乱分子战斗中受伤的士兵](../Page/车臣.md "wikilink")。[58](http://news.xinhuanet.com/world/2003-08/02/content_1006331.htm)

## [8月1日](../Page/8月1日.md "wikilink")

  - [北朝鲜核危机](../Page/北朝鲜核危机.md "wikilink")：[朝鲜方面表示赞同就](../Page/朝鲜.md "wikilink")[朝鲜半岛核问题进行六方会谈](../Page/朝鲜半岛.md "wikilink")。[59](http://news.xinhuanet.com/world/2003-07/31/content_1004278.htm)
  - 据台湾媒体报道，[乌克兰政府将送该国的两款飞机参加台北航空展](../Page/乌克兰.md "wikilink")，同时将在台北开办商务办事处。[60](http://www.phoenixtv.com/home/news/taiwan/200308/01/91819.html)
  - [美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")：美军士兵再次受到袭击，2名美军士兵被打死，5人受伤。[61](http://www.phoenixtv.com/home/news/world/200308/01/91594.html)
  - [利比里亚总统](../Page/利比里亚.md "wikilink")[查理斯·泰勒希望](../Page/查理斯·泰勒.md "wikilink")[联合国能在星期一向利比里亚派遣一支](../Page/联合国.md "wikilink")[维和部队以维护当地发生的反政府武装叛乱](../Page/维和部队.md "wikilink")。泰勒表示当维和部队到达以后愿意离开利比里亚。[62](http://www.phoenixtv.com/home/news/world/200308/01/91581.html)
  - [俄罗斯联邦军队星期四对](../Page/俄罗斯.md "wikilink")[车臣地区的武装分子进行轰炸](../Page/车臣.md "wikilink")，有15名武装分子死亡。[63](http://www.phoenixtv.com/home/news/world/200308/01/91775.html)
  - [纽约一些选民代表向法院提出诉讼](../Page/纽约.md "wikilink")，称在[2000年美国总统选举中他们的票数可能未被统计](../Page/2000年美国总统选举.md "wikilink")。[64](http://www.phoenixtv.com/home/news/world/200308/01/91791.html)
  - [梵蒂冈发表了题为](../Page/梵蒂冈.md "wikilink")《对给以同性恋者间法律认同的考虑》（*Considerations
    Regarding Proposals to Give Legal Recognition to Unions Between
    Homosexual
    Persons*）的声明，反对当前开始的合法化[同性婚姻进程](../Page/同性婚姻.md "wikilink")。
  - 在[加拿大](../Page/加拿大.md "wikilink")，总理[克雷蒂安和自由党领导候选人](../Page/克雷蒂安.md "wikilink")[保罗·马丁表示他们将继续推动](../Page/保罗·马丁.md "wikilink")[同性婚姻的进程](../Page/同性婚姻.md "wikilink")。