**W線百老匯慢車**（），又稱**紐約地鐵W線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的[地鐵系統](../Page/地鐵.md "wikilink")。由於該線在[曼哈頓使用](../Page/曼哈頓.md "wikilink")[BMT百老匯線](../Page/BMT百老匯線.md "wikilink")，因此其路線徽號為黃色\[1\]。

W線只在平日營運，深夜時不營運，來往[阿斯托利亞的](../Page/阿斯托里亞_\(皇后區\).md "wikilink")[阿斯托利亞-迪特馬斯林蔭路與](../Page/阿斯托利亞-迪特馬斯林蔭路車站_\(BMT阿斯托利亞線\).md "wikilink")[曼哈頓下城的](../Page/曼哈頓下城.md "wikilink")[白廳街-南碼頭](../Page/白廳街-南碼頭車站_\(BMT百老匯線\).md "wikilink")，停靠全線所有車站，繁忙時段有限度服務以的[86街](../Page/86街車站_\(BMT海灘線\).md "wikilink")，在布魯克林停靠所有車站\[2\]\[3\]\[4\]\[5\]。

2001年7月22日加入，W線任何時候都營運，在布魯克林的[BMT西城線和](../Page/BMT西城線.md "wikilink")[BMT第四大道線前往](../Page/BMT第四大道線.md "wikilink")[康尼島-斯提威爾大道](../Page/康尼島-斯提威爾大道車站.md "wikilink")。此線在2004年截成現時的營運模式，直至2010年6月25日直至[大都會運輸署財政危機而取消](../Page/大都會運輸署.md "wikilink")。2016年5月23日，MTA宣佈在2016年11月7日恢復此線，使用原初的標誌和走線，以彌補[第二大道線通車後的營運模式](../Page/第二大道線.md "wikilink")\[6\]<ref name=":2">

  - Press Release {{\!}} NYC Transit {{\!}} MTA Advances Work On Second
    Avenue Subway
    Service|last=|first=|date=|website=www.mta.info|publisher=Metropolitan
    Transportation Authority|access-date=February 19, 2016}

}\* </ref>\[7\]\[8\]\[9\]。

## 歷史

線於2001年6月22日開始營運，行駛線因[曼哈頓橋關閉重建而停駛的區域](../Page/曼哈頓橋.md "wikilink")。尖峰與中午時段往返[皇后區迪特馬士大道](../Page/皇后區.md "wikilink")-阿斯托利亞車站與科尼島-斯提威爾大道車站之間，於皇后區行駛尖峰快車、[曼哈頓](../Page/曼哈頓.md "wikilink")34街-赫拉德廣場車站與[布魯克林](../Page/布魯克林.md "wikilink")36街車站之間行快車，其他路線行慢車；平日夜晚曼哈頓36街車站以北行快車、以南行慢車；週末時自太平洋街車站發車，行車模式與平日夜晚同；凌晨行駛接駁服務往返36街與科尼島-斯提威爾大道車站之間。

2001年[911事件後](../Page/911事件.md "wikilink")，因線停駛，線列車於非凌晨時段往返迪特馬士大道-阿斯托利亞車站與科尼島-斯提威爾大道車站之間，以36街為劃分，以北為快車、以南為慢車；凌晨則分為兩區間：阿斯托利亞-迪特馬士大道車站至34街-赫拉德廣場車站，及36街車站至科尼島-斯提威爾大道車站。於10月28日恢復正常營運。

面對許多抱怨，紐約地鐵於2002年1月15日調整W線列車全天於[BMT阿斯托利亞線營運](../Page/BMT阿斯托利亞線.md "wikilink")。同年9月8日科尼島-斯提威爾大道車站關閉重建，但[BMT西側線月台仍有營運](../Page/BMT西側線.md "wikilink")，W線調整為全天營運。

2004年2月22日[曼哈頓橋北邊軌道重啟](../Page/曼哈頓橋.md "wikilink")，W線調整為週一至週五自迪特馬士大道-阿斯托利亞車站駛往白廳街車站，布魯克林部份改由線行駛。

2010年3月24日，[紐約大都會運輸署宣布](../Page/紐約大都會運輸署.md "wikilink")線由於資金不足而作廢，由線和線取代其原先服務。線於該年6月25日星期五進行最後一次運行，當天的最後一班開往阿斯托利亞-迪特馬士大道站列車於22時50分離開白廳街-南碼頭站。

2016年2月19日，[紐約大都會運輸署宣布](../Page/紐約大都會運輸署.md "wikilink")線將於2016年秋季重新投入服務，取代由[BMT阿斯托利亞線改道至](../Page/BMT阿斯托利亞線.md "wikilink")[2大道線的](../Page/紐約地鐵第二大道線.md "wikilink")線在皇后區的服務。

## 路線

### 服務形式

以下表格顯示W線所使用路線，特定時段在有陰影的格的路段內營運\[10\]

<table>
<thead>
<tr class="header">
<th><p>路線</p></th>
<th><p>起點</p></th>
<th><p>終點</p></th>
<th><p>軌道</p></th>
<th><p>時段</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>平日</p></td>
<td><p>繁忙時段</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BMT阿斯托利亞線.md" title="wikilink">BMT阿斯托利亞線</a>（全線）</p></td>
<td><p><a href="../Page/阿斯托利亞-迪特馬斯林蔭路車站_(BMT阿斯托利亞線).md" title="wikilink">阿斯托利亞-迪特馬斯林蔭路</a></p></td>
<td><p><a href="../Page/皇后區廣場車站_(BMT阿斯托利亞線).md" title="wikilink">皇后區廣場</a></p></td>
<td><p>慢車</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>全部</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>（全線）</p></td>
<td><p><a href="../Page/萊辛頓大道／59街車站_(BMT百老匯線).md" title="wikilink">萊辛頓大道／59街</a></p></td>
<td><p><a href="../Page/堅尼街車站_(BMT百老匯線).md" title="wikilink">堅尼街</a></p></td>
<td><p>慢車</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/市政府車站_(BMT百老匯線).md" title="wikilink">市政府</a></p></td>
<td><p><a href="../Page/白廳街-南碼頭車站_(BMT百老匯線).md" title="wikilink">白廳街-南碼頭</a></p></td>
<td><p>全部</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>全部</p></td>
<td><p> </p></td>
<td><p>有限度服務</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BMT第四大道線.md" title="wikilink">BMT第四大道線</a></p></td>
<td><p><a href="../Page/法庭街車站_(BMT第四大道線).md" title="wikilink">法庭街</a></p></td>
<td><p><a href="../Page/傑伊街-都會科技車站_(BMT第四大道線).md" title="wikilink">傑伊街-都會科技</a></p></td>
<td><p>全部</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德卡爾布大道車站_(BMT第四大道線).md" title="wikilink">德卡爾布大道</a></p></td>
<td><p><a href="../Page/59街車站_(BMT第四大道線).md" title="wikilink">59街</a></p></td>
<td><p>慢車</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BMT海灘線.md" title="wikilink">BMT海灘線</a></p></td>
<td><p><a href="../Page/第八大道車站_(BMT海灘線).md" title="wikilink">第八大道</a></p></td>
<td><p><a href="../Page/86街車站_(BMT海灘線).md" title="wikilink">86街</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 車站

更詳細的車站列表參見上方列出的路線。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-W.svg" title="fig:NYCS-bull-trans-W.svg">NYCS-bull-trans-W.svg</a></p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接及備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/皇后區.md" title="wikilink">皇后區</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/BMT阿斯托利亞線.md" title="wikilink">阿斯托利亞線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿斯托利亞-迪特馬斯林蔭路車站_(BMT阿斯托利亞線).md" title="wikilink">阿斯托利亞-迪特馬斯林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/阿斯托利亞林蔭路車站_(BMT阿斯托利亞線).md" title="wikilink">阿斯托利亞林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/30大道車站_(BMT阿斯托利亞線).md" title="wikilink">30大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>車站作為而關閉翻新直至2018年春天。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/百老匯車站_(BMT阿斯托利亞線).md" title="wikilink">百老匯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/36大道車站_(BMT阿斯托利亞線).md" title="wikilink">36大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>車站作為而關閉翻新直至2018年春天。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/39大道車站_(BMT阿斯托利亞線).md" title="wikilink">39大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/皇后區廣場車站_(BMT阿斯托利亞線).md" title="wikilink">皇后區廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/曼哈頓.md" title="wikilink">曼哈頓</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT百老匯線.md" title="wikilink">百老匯線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/萊辛頓大道／59街車站_(BMT百老匯線).md" title="wikilink">萊辛頓大道／59街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>）<br />
出站轉乘：（<a href="../Page/63街線.md" title="wikilink">63街線</a>，<a href="../Page/萊辛頓大道-63街車站.md" title="wikilink">萊辛頓大道-63街</a>）</p></td>
<td><p><a href="../Page/羅斯福島纜車.md" title="wikilink">羅斯福島纜車</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/第五大道／59街車站_(BMT百老匯線).md" title="wikilink">第五大道／59街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/57街-第七大道車站_(BMT百老匯線).md" title="wikilink">57街-第七大道</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/49街車站_(BMT百老匯線).md" title="wikilink">49街</a></p></td>
<td></td>
<td><p> ↑</p></td>
<td></td>
<td><p>車站僅北行可<a href="../Page/美國殘疾人法案.md" title="wikilink">無障礙通行</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/時報廣場-42街車站_(BMT百老匯線).md" title="wikilink">時報廣場-42街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）<br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）<br />
（<a href="../Page/42街接駁線.md" title="wikilink">42街接駁線</a>）<br />
（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>，<a href="../Page/42街-航港局客運總站車站_(IND第八大道線).md" title="wikilink">42街-航港局客運總站</a>）</p></td>
<td><p><a href="../Page/紐新航港局客運總站.md" title="wikilink">紐新航港局客運總站</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/34街-先驅廣場車站_(BMT百老匯線).md" title="wikilink">34街-先驅廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a>）</p></td>
<td><p><br />
<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/33街車站_(PATH).md" title="wikilink">33街</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/28街車站_(BMT百老匯線).md" title="wikilink">28街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/23街車站_(BMT百老匯線).md" title="wikilink">23街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/14街-聯合廣場車站_(BMT百老匯線).md" title="wikilink">14街-聯合廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>）<br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/第八街-紐約大學車站_(BMT百老匯線).md" title="wikilink">第八街-紐約大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/王子街車站_(BMT百老匯線).md" title="wikilink">王子街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/堅尼街車站_(BMT百老匯線).md" title="wikilink">堅尼街</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>）<br />
（<a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a>）</p></td>
<td><p>繁忙時段部分北行列車自此站起載</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/市政府車站_(BMT百老匯線).md" title="wikilink">市政府</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/科特蘭街車站_(BMT百老匯線).md" title="wikilink">科特蘭街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>，<a href="../Page/公園廣場車站_(IRT百老匯-第七大道線).md" title="wikilink">公園廣場</a>）<br />
（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>，<a href="../Page/錢伯斯街車站_(IND第八大道線).md" title="wikilink">錢伯斯街</a>）<br />
（IND第八大道線，<a href="../Page/世界貿易中心車站_(IND第八大道線).md" title="wikilink">世界貿易中心</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/世界貿易中心車站_(PATH).md" title="wikilink">世界貿易中心</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/雷克托街車站_(BMT百老匯線).md" title="wikilink">雷克托街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/白廳街-南碼頭車站_(BMT百老匯線).md" title="wikilink">白廳街-南碼頭</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）</p></td>
<td><p><br />
<a href="../Page/史泰登島渡輪.md" title="wikilink">史泰登島渡輪</a>，</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT第四大道線.md" title="wikilink">蒙塔格街支線</a></strong>（繁忙時段有限度服務來往<a href="../Page/86街車站_(BMT海灘線).md" title="wikilink">格雷夫森德-86街</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/法庭街車站_(BMT第四大道線).md" title="wikilink">法庭街</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>），<a href="../Page/區公所車站_(IRT百老匯-第七大道線).md" title="wikilink">區公所</a><br />
（<a href="../Page/IRT東公園道線.md" title="wikilink">IRT東公園道線</a>），<a href="../Page/區公所車站_(IRT東公園道線).md" title="wikilink">區公所</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/傑伊街-都會科技車站_(BMT第四大道線).md" title="wikilink">傑伊街-都會科技</a></p></td>
<td></td>
<td></td>
<td><p><br />
 （<a href="../Page/IND福爾頓街線.md" title="wikilink">IND福爾頓街線及</a><a href="../Page/IND卡爾弗線.md" title="wikilink">IND卡爾弗線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/德卡爾布大道車站_(BMT第四大道線).md" title="wikilink">德卡爾布大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT第四大道線.md" title="wikilink">第四大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/大西洋大道-巴克萊中心車站_(BMT第四大道線).md" title="wikilink">大西洋大道-巴克萊中心</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT布萊頓線.md" title="wikilink">BMT布萊頓線</a>）<br />
（<a href="../Page/IRT東公園道線.md" title="wikilink">IRT東公園道線</a>）</p></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/聯合街車站_(BMT第四大道線).md" title="wikilink">聯合街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第九街車站_(BMT第四大道線).md" title="wikilink">第九街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND卡爾弗線.md" title="wikilink">IND卡爾弗線</a>，<a href="../Page/第四大道車站_(IND卡爾弗線).md" title="wikilink">第四大道</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/展望大道車站_(BMT第四大道線).md" title="wikilink">展望大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/25街車站_(BMT第四大道線).md" title="wikilink">25街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/36街車站_(BMT第四大道線).md" title="wikilink">36街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/45街車站_(BMT第四大道線).md" title="wikilink">45街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/53街車站_(BMT第四大道線).md" title="wikilink">53街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/59街車站_(BMT第四大道線).md" title="wikilink">59街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT海灘線.md" title="wikilink">海灘線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第八大道車站_(BMT海灘線).md" title="wikilink">第八大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/漢密爾頓堡公園道車站_(BMT海灘線).md" title="wikilink">漢密爾頓堡公園道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>海灣公園道方向列車因翻新不停靠此站直至2018年秋天。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/新烏德勒支大道車站_(BMT海灘線).md" title="wikilink">新烏德勒支大道</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT西城線.md" title="wikilink">BMT西城線</a>，<a href="../Page/62街車站_(BMT西城線).md" title="wikilink">62街</a>）</p></td>
<td><p>海灣公園道方向列車因翻新不停靠此站直至2018年秋天。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/18大道車站_(BMT海灘線).md" title="wikilink">18大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>海灣公園道方向列車因翻新不停靠此站直至2018年秋天。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/20大道車站_(BMT海灘線).md" title="wikilink">20大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>海灣公園道方向列車因翻新不停靠此站直至2018年秋天。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/海灣公園道車站_(BMT海灘線).md" title="wikilink">海灣公園道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>海灣公園道方向列車因翻新不停靠此站直至2018年秋天。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/金斯公路車站_(BMT海灘線).md" title="wikilink">金斯公路</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>海灣公園道方向列車因翻新不停靠此站直至2018年秋天。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/U大道車站_(BMT海灘線).md" title="wikilink">U大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>海灣公園道方向列車因翻新不停靠此站直至2018年秋天。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/86街車站_(BMT海灘線).md" title="wikilink">86街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>海灣公園道方向列車因翻新不停靠此站直至2018年秋天。</p></td>
</tr>
</tbody>
</table>

## 備註

<references />

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20020728090822/http://www.mta.info/nyct/service/wline.htm)
  - [W線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit -
    W線時刻表（PDF）](https://web.archive.org/web/20031204032030/http://www.mta.info/nyct/service/pdf/twcur.pdf)

[W](../Category/紐約地鐵服務路線.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.

9.

10.