**IAST**是**國際梵語轉寫字母**的英語名稱（）的縮寫，是學術上對於[梵語](../Page/梵語.md "wikilink")[轉寫的標準](../Page/轉寫.md "wikilink")，亦變成了一般出版界，如書籍及雜誌的非業界標準。隨着[Unicode字型的普及](../Page/Unicode.md "wikilink")，它在電子文本的使用亦因此而相應地增加。這項標準是於1912年在[雅典舉行的](../Page/雅典.md "wikilink")[東方學會議](../Page/東方學會議.md "wikilink")（International
Congress of
Orientalists）上訂立的，而它又基於了1894年在[日內瓦舉行的東方學會議制定的標準](../Page/日內瓦.md "wikilink")。\[1\]\[2\]

IAST是羅馬化梵語和[巴利語的最流行的方案](../Page/巴利語.md "wikilink")，它允許無損的[天城文轉寫](../Page/天城文轉寫.md "wikilink")（和其他的[印度文字比如](../Page/婆羅米系文字.md "wikilink")[克什米爾語的傳統文字](../Page/克什米爾語.md "wikilink")[夏拉達文的轉寫](../Page/夏拉達文.md "wikilink")），而且不只是[梵語的](../Page/梵語.md "wikilink")[音素](../Page/音素.md "wikilink")，還允許本質上的語音標注（比如
[{{IAST](../Page/止韻.md "wikilink")  是詞尾的  和
的[同位異音](../Page/同位異音.md "wikilink")）。

## IAST 符號清單和約定

  -
    {| class="wikitable nounderlines" style="font-family:Arial;"

|- |<span style="font-size:150%;">अ</span> 
|<span style="font-size:150%;">आ</span> 
|<span style="font-size:150%;">इ</span> 
|<span style="font-size:150%;">ई</span> 
|<span style="font-size:150%;">उ</span> 
|<span style="font-size:150%;">ऊ</span> 
|<span style="font-size:150%;">ऋ</span> 
|<span style="font-size:150%;">ॠ</span> 
|<span style="font-size:150%;">ऌ</span> 
|<span style="font-size:150%;">ॡ</span> 
| style="font-size:80%;" | [元音](../Page/元音.md "wikilink") |}

  -
    {| class="wikitable nounderlines" style="font-family:Arial;"

|- |<span style="font-size:150%;">ए</span> 
|<span style="font-size:150%;">ऐ</span> 
|<span style="font-size:150%;">ओ</span> 
|<span style="font-size:150%;">औ</span> 
| style="font-size:80%;" | [雙元音](../Page/雙元音.md "wikilink") |}

  -
    {| class="wikitable nounderlines" style="font-family:Arial;"

|- |<span style="font-size:150%;">अं</span> 
| style="font-size:80%;" | [随韵](../Page/随韵.md "wikilink") |-
|<span style="font-size:150%;">अः</span> 
| style="font-size:80%;" | [止韵](../Page/止韵.md "wikilink") |}

  -
    {| class="wikitable nounderlines" style="font-family:Arial;"

|- | style="font-size:80%;" | [軟腭音](../Page/軟腭音.md "wikilink") |
style="font-size:80%;" | [硬腭音](../Page/硬腭音.md "wikilink") |
style="font-size:80%;" | [卷舌音](../Page/卷舌音.md "wikilink") |
style="font-size:80%;" | [齒音](../Page/齒音.md "wikilink") |
style="font-size:80%;" | [唇音](../Page/唇音.md "wikilink") |-
|<span style="font-size:150%;">क</span> 
|<span style="font-size:150%;">च</span> 
|<span style="font-size:150%;">ट</span> 
|<span style="font-size:150%;">त</span> 
|<span style="font-size:150%;">प</span> 
| style="font-size:80%;" |
[清](../Page/清音.md "wikilink")[塞音](../Page/塞音.md "wikilink")
|- |<span style="font-size:150%;">ख</span> 
|<span style="font-size:150%;">छ</span> 
|<span style="font-size:150%;">ठ</span> 
|<span style="font-size:150%;">थ</span> 
|<span style="font-size:150%;">फ</span> 
| style="font-size:80%;" |
[送氣](../Page/送氣.md "wikilink")[清](../Page/清音.md "wikilink")[塞音](../Page/塞音.md "wikilink")
|- |<span style="font-size:150%;">ग</span> 
|<span style="font-size:150%;">ज</span> 
|<span style="font-size:150%;">ड</span> 
|<span style="font-size:150%;">द</span> 
|<span style="font-size:150%;">ब</span> 
| style="font-size:80%;" |
[濁](../Page/濁音.md "wikilink")[塞音](../Page/塞音.md "wikilink")
|- |<span style="font-size:150%;">घ</span> 
|<span style="font-size:150%;">झ</span> 
|<span style="font-size:150%;">ढ</span> 
|<span style="font-size:150%;">ध</span> 
|<span style="font-size:150%;">भ</span> 
| style="font-size:80%;" |
[送氣](../Page/送氣.md "wikilink")[濁](../Page/濁音.md "wikilink")[塞音](../Page/塞音.md "wikilink")
|- |<span style="font-size:150%;">ङ</span> 
|<span style="font-size:150%;">ञ</span> 
|<span style="font-size:150%;">ण</span> 
|<span style="font-size:150%;">न</span> 
|<span style="font-size:150%;">म</span> 
| style="font-size:80%;" | [鼻音](../Page/鼻音_\(辅音\).md "wikilink") |- |  
|<span style="font-size:150%;">य</span> 
|<span style="font-size:150%;">र</span> 
|<span style="font-size:150%;">ल</span> 
|<span style="font-size:150%;">व</span> 
| style="font-size:80%;" | [半元音](../Page/半元音.md "wikilink") |- |  
|<span style="font-size:150%;">श </span> 
|<span style="font-size:150%;">ष</span> 
|<span style="font-size:150%;">स</span> 
|   | style="font-size:80%;" | [噝擦音](../Page/噝擦音.md "wikilink") |-
|<span style="font-size:150%;">ह</span> 
|   |   |   |   | style="font-size:80%;" |
[濁](../Page/濁音.md "wikilink")[擦音](../Page/擦音.md "wikilink") |}

[母音连接时](../Page/母音.md "wikilink")，a+a i+i u+u 分别使用 â î û 表示。

注意：與只用 [ASCII](../Page/ASCII.md "wikilink") 碼的羅馬化如
[ITRANS](../Page/ITRANS.md "wikilink") 或
[Harvard-Kyoto](../Page/Harvard-Kyoto.md "wikilink") 不同，使用變音符號的 IAST
允許專有名字的首字母大寫。永不出現在詞首的字母如
的大寫只在[波你尼的文法中有用](../Page/波你尼.md "wikilink")，這里有把
IT 聲音排版為大寫字母的約定（參見《[八篇書](../Page/八篇書.md "wikilink")》）。

## 與ISO15919的比較

在大多數情況下，IAST是[ISO
15919的一部分](../Page/ISO_15919.md "wikilink")。以下五個例外是由於ISO標準配合符號的擴展項目，以允許天城文及其他印度語字母的音譯用於梵文之外的其它語言。

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/天城文.md" title="wikilink">天城文</a></p></th>
<th><p>IAST</p></th>
<th><p>ISO 15919</p></th>
<th><p>備註 |- </p></th>
<th></th>
<th><p>e</p></th>
<th><p>ē</p></th>
<th><p>ISO <em>e</em> 表示 。</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>o</p></td>
<td><p>ō</p></td>
<td><p>ISO <em>o</em> 表示 。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>ISO <em></em> 表示 <a href="../Page/Gurmukhi.md" title="wikilink">Gurmukhi</a> <em><a href="../Page/Gurmukhi#Other_signs.md" title="wikilink">tippi</a></em> 。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p>ISO <em></em> 表示 ड़ 。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>與 <em></em> 一致。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>ḷ</p></td>
<td><p>l̥</p></td>
<td><p>ISO <em></em>表示 ळ .</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>ḹ</p></td>
<td><p>l̥̄</p></td>
<td><p>與<em></em>一致。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [天城文轉寫](../Page/天城文轉寫.md "wikilink")
  - [Harvard-Kyoto](../Page/Harvard-Kyoto.md "wikilink")
  - [ITRANS](../Page/ITRANS.md "wikilink")
  - [加爾各答國家圖書館羅馬化](../Page/加爾各答國家圖書館羅馬化.md "wikilink")
  - [ISO 15919](../Page/ISO_15919.md "wikilink")
  - [濕婆經](../Page/濕婆經.md "wikilink")
  - [梵文藏文轉寫體系](../Page/梵文藏文轉寫體系.md "wikilink")

## 引用

## 外部連結

  - [简洁多功能梵语、巴利语输入法](http://www.sanskrit-input.com/)
  - [IAST在线输入法](http://sanskrit.sinaapp.com/sanskritinput2/)
  - [太清梵语转写在线输入法](http://abkai.net/core/zh/online-keyboard/online-keyboard-iast/)
  - [IAST
    梵文输入法](http://blog.sina.cn/dpool/blog/s/blog_55784b070101frex.html?type=-1)
  - [Typing a
    macron](http://www.personal.psu.edu/ejp10/psu/gotunicode/macron.html)
    - page from Penn State University about typing with accents
  - [International Phonetic Alphabet chart with pronunciation
    guide](http://www.yorku.ca/earmstro/ipa/)
  - [A visual chart which shows clearly 1. Which part of the mouth for
    each sound 2. The 3 groups where the 12 diacritics
    appear.](https://drive.google.com/file/d/0B5ph4C2WV1JqZ1pjNTkxVll1ZGM)
  - [Sanskrit Pronunciation Tips for beginners & Simple Charts to help
    memorize where the diacritics fit
    in.](https://vimeo.com/groups/151501)
  - [A pronunciation guide with chart and pronunciation
    tips](https://drive.google.com/file/d/0B5ph4C2WV1JqeEdnZVhOVWx1MUE)
  - [IAST \<==\> Devanagari online converter (Transliteration
    tool)](https://sites.google.com/site/technicalhindi/files/IAST%20and%20other%20Roman%20encodings%20to%20Devanagari%20%20Converter_04.html?attredirects=0&d=1)

[Category:转写系统](../Category/转写系统.md "wikilink")
[Category:梵語](../Category/梵語.md "wikilink")

1.  [History of Skt. transcription and *1894, Rapport de la
    Trans.*](http://shashir.autodidactus.org/shashir_umich/sanskrit_transcription.html)
2.  X<sup>me</sup> Congrès International des Orientalistes, Session de
    Genève. 1894. [*Rapport de la Commission de
    Transcription*](http://shashir.autodidactus.org/shashir_umich/dl/rapportdetranscription.pdf).