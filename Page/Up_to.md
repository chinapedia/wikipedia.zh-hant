在[数学领域](../Page/数学.md "wikilink")，詞組“up to
xxx”表示为了某种目的同一[等价类中的元素视为一体](../Page/等价类.md "wikilink")。“xxxx”描述了某种性质或将中元素变为同一等价类中另一个的操作（即将元素和它变为的那个等价）。例如在[群论中](../Page/群论.md "wikilink")，我们有一个群*G*作用在集合*X*上，在此情形：如果*X*中两个元素在同一轨道中，我们可以说它们等价“up
to群作用”。

中文中没有类似对应的词组，翻譯成中文時，可以酌情譯為：在“xxx的意义下”或“差一个xxx”等。比如上面可以翻译为“差一个群作用的意义下等价”。但是，這個翻譯是既迂迴又笨拙，因為數學中「在xxx的意義下」通常是對有數個不等價定義的詞語指定其意義，對應英文“in
the sense
of”，例如「這個函數在[勒貝格的意義下可積](../Page/勒貝格積分.md "wikilink")，但是在[黎曼的意義下不可積](../Page/黎曼積分.md "wikilink")」，就對「[可積](../Page/可積函數.md "wikilink")」一詞先後指定兩個不等價的定義；然而，數學中英文短語“up
to”的重點不在確定某詞語的定義，而在省略掉一些非本質的次要差異。

## 举例

  - 在[八皇后问题中](../Page/八皇后问题.md "wikilink")，若把八个皇后看做不同的个体，则不同的解有3,709,440个。一般将八个皇后看作完全相同，此时有92（3709440/8\!）种不同的解，不考虑皇后间的不同[排列组合](../Page/排列组合.md "wikilink")（所对应的等价关系）。也就是说，我们将单个皇后的位置不同，但所有皇后所占[棋盘位置集相同的解等价起来](../Page/国际象棋.md "wikilink")，并只考虑不同的等价类。
  - 在上述解中，如果同时将可由棋盘[旋转](../Page/旋转.md "wikilink")、[翻转来互相转换的解等价起来](../Page/翻转.md "wikilink")，则只剩下12个不同的解（等价类）。
  - [群论中](../Page/群论.md "wikilink")，称[G集中同一](../Page/G集.md "wikilink")[轨道上的元素up](../Page/G集.md "wikilink")
    to[群作用等价](../Page/群作用.md "wikilink")。
  - [群论中](../Page/群论.md "wikilink")，**up
    to[群同构](../Page/群同构.md "wikilink")**，只有两种不同的四元素群。也就是说，所有四元素群通过群同构仅有两个不同的等价类。
  - [范畴论中](../Page/范畴论.md "wikilink")，由[泛性质确定的](../Page/泛性质.md "wikilink")[态射如存在多个](../Page/态射.md "wikilink")，则它们之间**up
    to[同构相等](../Page/同构态射.md "wikilink")**（又称**本质相等**），或者说，由泛性质所确定的态射**up
    to同构唯一**。
  - [并发理论中](../Page/并发理论.md "wikilink")，证明进程间的[互模拟往往需要构造很大的互模拟关系](../Page/互模拟.md "wikilink")。而借助“**up
    to……互模拟**”技术，可仅构造已知包含在互模拟关系中的某种“up to……互模拟”关系来简化证明。

## 说明

与“Up to”非常接近的一个词是[模](../Page/同余.md "wikilink")（modulo），如整数1和5模4相等。

## 参见

  - [商集](../Page/商集.md "wikilink")

[Category:数学术语](../Category/数学术语.md "wikilink")