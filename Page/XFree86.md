**XFree86**是[X
Window系统的其中一个实现](../Page/X_Window系统.md "wikilink")，自1992年，它一直循著自由發放的[開放源代碼模式被發展](../Page/開放源代碼.md "wikilink")。它主要的運作平台是[Unix类](../Page/Unix类.md "wikilink")[操作系統](../Page/操作系統.md "wikilink")，包括各版本的[UNIX](../Page/UNIX.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[BSD](../Page/BSD.md "wikilink")、[Solaris](../Page/Solaris.md "wikilink")、[Mac
OS
X](../Page/Mac_OS_X.md "wikilink")、[IRIX](../Page/IRIX.md "wikilink")、[OpenVMS及](../Page/OpenVMS.md "wikilink")[Cygwin/X等](../Page/Cygwin/X.md "wikilink")。

由2004年開始，它再不是以[GPL](../Page/GPL.md "wikilink")[軟件許可證的形式出現](../Page/軟件許可證.md "wikilink")，而是使用XFree86®Project公司所擁有的[XFree86
License](../Page/XFree86_License.md "wikilink") version 1.1
[軟件许可證模式發放](../Page/軟件许可證.md "wikilink")。現時XFree86仍由XFree86®Project公司負責开发，該公司的主要負責人是[David
Dawes](../Page/David_Dawes.md "wikilink")。

自1992年至2004年，XFree86是[X
Window系統最被廣泛使用的基建平台](../Page/X_Window系統.md "wikilink")。其後，XFree86的開發因[軟件許可證出現分岐](../Page/軟件許可證.md "wikilink")，而衍生出另一個發展自XFree86
4.4 RC2版本源代碼，名叫[X.Org伺服器的](../Page/X.Org伺服器.md "wikilink")[X
Window系統](../Page/X_Window系統.md "wikilink")。大部分使用XFree86的开源操作系统和开发者都转为使用X.Org。XFree86从2008年底发布4.8.0以后已经完全停止更新。

## 版本歷史

| 版本                                     | 發佈日期        | 主要改變                                                                                              |
| -------------------------------------- | ----------- | ------------------------------------------------------------------------------------------------- |
| [X386](../Page/X386.md "wikilink") 1.1 | 1991年2月11日  | 由Thomas Roell的第一個版本，基於[X11R4](../Page/X11R4.md "wikilink")                                        |
| X386 1.2                               | 1991年8月29日  | 包括X11R5                                                                                           |
| X386 1.2e 0.0                          | 1992年5月7日   | First pre-XFree86 code by eventual team members.                                                  |
| XFree86 1.0m                           | 1992年9月2日   | 第一個"XFree86"                                                                                      |
| XFree86 2.0                            | 1993年10月    |                                                                                                   |
| XFree86 2.1                            | 1994年3月11日  |                                                                                                   |
| XFree86 2.1.1                          | 1994年5月4日   | 最後一個基於X11R5                                                                                       |
| XFree86 3.0                            | 1994年8月26日  | 釋放X11R6                                                                                           |
| XFree86 3.1                            | 1994年9月29日  |                                                                                                   |
| XFree86 3.2                            | 1996年10月26日 |                                                                                                   |
| XFree86 3.2.1                          | 1996年       |                                                                                                   |
| XFree86 3.3                            | 1997年5月30日  | [XFree86 Acceleration Architecture](../Page/XFree86_Acceleration_Architecture.md "wikilink")（XAA） |
| XFree86 3.3.1                          | 1997年8月8日   |                                                                                                   |
| XFree86 3.3.2                          | 1998年5月24日  |                                                                                                   |
| XFree86 3.3.3                          | 1998年12月30日 |                                                                                                   |
| XFree86 3.3.3.1                        | 1998年12月30日 |                                                                                                   |
| XFree86 3.3.4                          | 1999年6月21日  |                                                                                                   |
| XFree86 3.3.5                          | 1999年8月17日  |                                                                                                   |
| XFree86 3.3.6                          | 1999年12月31日 | 最後一個3.x的版本                                                                                        |
| XFree86 4.0                            | 2000年3月8日   | 完整的新架構，包括X11R6.4                                                                                  |
| XFree86 4.0.1                          | 2000年6月30日  | [XRender](../Page/XRender.md "wikilink")                                                          |
| XFree86 4.0.2                          | 2000年12月18日 |                                                                                                   |
| XFree86 4.0.3                          | 2001年3月16日  |                                                                                                   |
| XFree86 4.0.4                          | 2001年       |                                                                                                   |
| XFree86 4.1.0                          | 2001年6月2日   |                                                                                                   |
| XFree86 4.2.0                          | J2002年1月18日 |                                                                                                   |
| XFree86 4.2.1                          | 2002年9月3日   |                                                                                                   |
| XFree86 4.3.0                          | 2003年2月26日  |                                                                                                   |
| XFree86 4.4.0                          | 2004年2月29日  | 第一個XFree86許可1.1的版本                                                                                |
| XFree86 4.5.0                          | 2005年3月16日  |                                                                                                   |
| XFree86 4.6.0                          | 2006年5月10日  |                                                                                                   |
| XFree86 4.7.0                          | 2007年8月12日  |                                                                                                   |
| XFree86 4.8.0                          | 2008年12月15日 |                                                                                                   |
|                                        |             |                                                                                                   |

## 参见

  - [X.Org服务器](../Page/X.Org服务器.md "wikilink")
  - [DirectFB](../Page/DirectFB.md "wikilink")

## 外部链接

  - [XFree86項目主页](http://www.xfree86.org)
  - [XFree86配置文件样本](http://tldp.org/HOWTO/XFree-Local-multi-user-HOWTO/examples_xf_confs.html)

[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:X服务器](../Category/X服务器.md "wikilink")