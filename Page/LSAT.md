**LSAT**（），即[法學院入學](../Page/法學院.md "wikilink")[考試](../Page/考試.md "wikilink")。它是由位於[美國](../Page/美國.md "wikilink")[賓西法尼亞州的](../Page/賓西法尼亞州.md "wikilink")[法學院入學委員會](../Page/法學院入學委員會.md "wikilink")（）負責主辦的法學院人學資格考試。\[1\]幾乎所有的美國和加拿大法學院都要求申請人參加LSAT考試。LSAT考試共有五個部分（包含一個不記分的供入學委員會評估用的評測部分），包括三個方面的內容，每部分時間為35分鐘，另加30分鐘的寫作。

## LSAT考試時間及評價標準

LSAT考試每年舉辦四次，分別在二月、六月、十月及十二月。[中國大陸地區六月和十二月在](../Page/中國.md "wikilink")[北京大學有固定考試](../Page/北京大學.md "wikilink")。部分中國大陸地區考生會選擇在二月、十月時到香港參加在那裡舉辦的LSAT考試。

LSAT考試滿分為180分，最低分為120分，其計算方法是根據選擇的正確的答案的數目來確定。選對25個左右，分數大概為130分；選對39個左右，分數大概為140分；選對55個，分數大致為150分；選對72個，分數為160；選對87個，分數為170分；選對98個以上，分數為滿分180分。一般來說，排名前14的法學院要求成績都在170分以上；排名前25的法學要求成績在160分以上。考試成績一般在考後五週左右由主辦機構寄出，LSAT成績在5年內有效。

LSAT考試的註冊費用從2009年六月起是$132，延遲註冊需加$64[美元](../Page/美元.md "wikilink")[1](https://os.lsac.org/release/lsat/LSAT_Register.aspx)。

## LSAT閱讀

閱讀理解部分包括4篇文章，其中每篇文章大約為400-600個單詞，有6-8個問題，4篇文章共有28個問題。從事題類型上來看，一般來講，閱讀理解問題的類型有以下幾種：

1.  有關中心思想的問題。這類問題主要涉及貫穿全文的主旨。
2.  有關作者態度的問題。這類問題主要涉及作者在文中的態度。
3.  有關推論型的問題。這類問題主要涉及從文章中提供的實施的出相應的結論。
4.  有關邏輯結構的問題。這類問題主要涉及整個文章的組織結構。
5.  有關文章中某一具體實施的問題。
6.  有關使用新的情況的問題

## LSAT邏輯推理

邏輯推理試題共有兩個部分，每部分有 24-26道試題。一般每道題都有一篇小的短文或對話，然後針對此短文或對話提出 問題。
短文或對話涉及的範圍很廣，包括[哲學](../Page/哲學.md "wikilink")、[文學](../Page/文學.md "wikilink")、[政治](../Page/政治.md "wikilink")、[科技](../Page/科技.md "wikilink")、[藝術](../Page/藝術.md "wikilink")、[歷史](../Page/歷史.md "wikilink")、[體育等等](../Page/體育.md "wikilink")。
邏輯推理試題主要測試考生的以下能力 ，

  - 確定中心思想
  - 找出推理中的假設
  - 從已知事實或前提得出合理結論
  - 確定推理的準則並將之應用於新的論證
  - 確定推理的方法或結構
  - 找出推理的錯誤及誤解
  - 確定新的事實或論證對現有論證或結論的加強或削弱
  - 對論證進行分析

**試題類型**

邏輯推理實體的類型一般包括以下幾種：

1\. 有關結論的問題

2\. 有關推論的問題

3\. 有關假設的問題

4.有關加強（Strengthen）論證的問題。

## LSAT分析推理

分析推理部分一般分四組，共有24個問題。每組裡面的每一個問題都基於一系列的條件，這些條件共同描述一種情況，例如，把人分成幾組，把物品按順序排列等等。

這部分試題主要測試考生理解有關關係結構並推出結論的能力。

**一、試題類型**

1、分配問題（Assignment）

2、有關排列次序的問題

3、有關編組的問題

4、有關空間分佈的問題

## LSAT寫作

LSAT考試中的作文是不記入總分的，法學院在錄取時，對於作文的分數只是作為參考。有的學校對於作文成績非常看重，但有的學校則完全忽略该成绩。

寫作部分時間為 35 分種，其內容是，就給定的題目中的兩種觀點選擇一個，並加以闡述。寫作部分不計入總分， 只作為參考。

2005年6月的LSAT考試對不計分的寫作環節進行了調整：寫作時間由30分鐘變為35分鐘，書寫的紙由一張變為兩張，同時還修改了答題指導。現在考生將被隨機分配兩種題目中的一種。在這兩種中，有一種是新題型。寫作部分將依然保持不計分。

## 註釋

## 外部連結

  - [Law School Admission Council](http://www.lsac.org/)

[Category:考試](../Category/考試.md "wikilink")
[Category:美國法學院](../Category/美國法學院.md "wikilink")

1.