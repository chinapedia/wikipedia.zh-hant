**3月12日**是[公历一年中的第](../Page/公历.md "wikilink")71天（[闰年第](../Page/闰年.md "wikilink")72天），离全年的结束还有294天。

## 大事记

### 17世紀

  - [1622年](../Page/1622年.md "wikilink")：[耶稣会的创始人](../Page/耶稣会.md "wikilink")[依纳爵·罗耀拉和](../Page/依纳爵·罗耀拉.md "wikilink")[圣方济·沙勿略被罗马教宗](../Page/圣方济·沙勿略.md "wikilink")[额我略十五世封为](../Page/额我略十五世.md "wikilink")[基督教圣人](../Page/基督教圣人.md "wikilink")。
  - [1664年](../Page/1664年.md "wikilink")：[新泽西成为](../Page/新泽西.md "wikilink")[英国](../Page/英国.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。

### 18世紀

  - [1782年](../Page/1782年.md "wikilink")：[四库全书编成](../Page/四库全书.md "wikilink")。

### 19世紀

  - [1894年](../Page/1894年.md "wikilink")：瓶装[可口可乐开始发售](../Page/可口可乐.md "wikilink")。

### 20世紀

  - [1913年](../Page/1913年.md "wikilink")：[澳大利亚正式命名其新都为](../Page/澳大利亚.md "wikilink")[堪培拉](../Page/堪培拉.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：[俄国宣布迁都](../Page/俄国.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")。
  - [1925年](../Page/1925年.md "wikilink")：[孫中山於](../Page/孫中山.md "wikilink")[北京逝世](../Page/北京.md "wikilink")，[國民黨由](../Page/國民黨.md "wikilink")[汪精衛接任主席](../Page/汪精衛.md "wikilink")。
  - [1926年](../Page/1926年.md "wikilink")：[大沽口事件发生](../Page/大沽口事件.md "wikilink")。
  - [1930年](../Page/1930年.md "wikilink")：为了反抗[英国殖民政府的](../Page/英国.md "wikilink")[食盐专营法](../Page/食盐.md "wikilink")，[印度](../Page/印度.md "wikilink")[民族主义运动领袖](../Page/民族主义.md "wikilink")[圣雄甘地发起](../Page/圣雄甘地.md "wikilink")[食盐进军的](../Page/食盐进军.md "wikilink")[不合作运动](../Page/不合作运动.md "wikilink")。
  - [1938年](../Page/1938年.md "wikilink")：[奥地利与](../Page/奥地利.md "wikilink")[纳粹德国](../Page/纳粹德国.md "wikilink")[統一](../Page/德奧合併.md "wikilink")，成为德国的一个州。
  - [1940年](../Page/1940年.md "wikilink")：[苏联和](../Page/苏联.md "wikilink")[芬兰签订](../Page/芬兰.md "wikilink")《[莫斯科和平协定](../Page/莫斯科和平协定.md "wikilink")》，苏联迫使芬兰割让[卡累利阿](../Page/卡累利阿.md "wikilink")、[薩拉在內的十分之一的](../Page/薩拉_\(芬蘭\).md "wikilink")[领土](../Page/领土.md "wikilink")，[冬季战争结束](../Page/冬季战争.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[冷战](../Page/冷战.md "wikilink")：[美国总统](../Page/美国总统.md "wikilink")[杜鲁门在](../Page/哈利·S·杜鲁门.md "wikilink")[国会发表国情咨文](../Page/美国国会.md "wikilink")，宣布援助受[共产主义威胁的国家](../Page/共产主义.md "wikilink")，[杜鲁门主义产生](../Page/杜鲁门主义.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")：[日本社会党书记长](../Page/日本社会党.md "wikilink")[浅沼稻次郎在北京发表演讲](../Page/浅沼稻次郎.md "wikilink")，指出“[美帝国主义是日中两国人民共同的敌人](../Page/美帝国主义.md "wikilink")”，引起日本舆论非议。
  - [1967年](../Page/1967年.md "wikilink")：[苏哈托政變奪權](../Page/苏哈托.md "wikilink")，迫[苏加诺下台](../Page/苏加诺.md "wikilink")，出任[印尼总统](../Page/印尼总统.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[香港足球代表隊在](../Page/香港足球代表隊.md "wikilink")[1978年世界盃亞洲區外圍賽上以](../Page/1978年世界盃.md "wikilink")1:0擊敗[新加坡奪得小組首名](../Page/新加坡國家足球隊.md "wikilink")。
  - [1985年](../Page/1985年.md "wikilink")：[日本](../Page/日本.md "wikilink")[青函隧道贯通](../Page/青函隧道.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[戈尔巴乔夫当选首任](../Page/戈尔巴乔夫.md "wikilink")[苏联总统](../Page/苏联总统.md "wikilink")。
  - 1990年：[台北市区里行政区域重新调整](../Page/台北市.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[印度](../Page/印度.md "wikilink")[孟買的](../Page/孟買.md "wikilink")13個地點發生[連環爆炸案](../Page/1993年孟买爆炸案.md "wikilink")，共造成253人死亡、713人受傷。
  - [1996年](../Page/1996年.md "wikilink")：世界氣象組織([WMO](../Page/WMO.md "wikilink"))指出缺水將是全世界城市面臨的首要問題。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：一艘貨櫃船與挖泥船在[汲水門大橋附近相撞](../Page/汲水門大橋.md "wikilink")，挖泥船沉沒，船上15個人墮海，只有7個人獲救，死者包括[俄羅斯和](../Page/俄羅斯.md "wikilink")[香港船員](../Page/香港.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")：[塞尔维亚总理](../Page/塞尔维亚总理.md "wikilink")[佐兰·金吉奇在塞尔维亚政府大楼前被枪手](../Page/佐兰·金吉奇.md "wikilink")[暗杀](../Page/暗杀.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")：[韩国国会投票通过以非法竞选和收贿等罪名弹劾](../Page/韩国国会.md "wikilink")[韩国总统](../Page/韩国总统.md "wikilink")[卢武铉](../Page/卢武铉.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[中國國務院接受](../Page/中國國務院.md "wikilink")[董建華辭去](../Page/董建華.md "wikilink")[香港特別行政區行政長官職務](../Page/香港特別行政區行政長官.md "wikilink")。按照[香港基本法的有關規定](../Page/香港基本法.md "wikilink")，由[政務司司長](../Page/政務司司長.md "wikilink")[曾蔭權署理行政長官](../Page/曾蔭權.md "wikilink")，直至選出新的行政長官為止。在同日閉幕的[全國政協十屆三次會議上](../Page/全國政協.md "wikilink")，董建華被增選為全國政協副主席。
  - [2011年](../Page/2011年.md "wikilink")：[日本](../Page/日本.md "wikilink")[九州新幹線全線通車](../Page/九州新幹線.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：根據[美國人口調查局的估計](../Page/美國人口調查局.md "wikilink")，[世界人口達到七十億人](../Page/世界人口.md "wikilink")。\[1\]

## 出生

  - [1672年](../Page/1672年.md "wikilink")：[愛新覺羅](../Page/愛新覺羅.md "wikilink")[允禔](../Page/允禔.md "wikilink")，[康熙帝第一子](../Page/康熙帝.md "wikilink")，[直郡王](../Page/直郡王.md "wikilink")（[1735年逝世](../Page/1735年.md "wikilink")）
  - [1823年](../Page/1823年.md "wikilink")：[勝海舟](../Page/勝海舟.md "wikilink")，日本政治家（[1899年去世](../Page/1899年.md "wikilink")）
  - [1838年](../Page/1838年.md "wikilink")：[威廉·亨利·珀金](../Page/威廉·亨利·珀金.md "wikilink")，英國化學家
    （[1907年逝世](../Page/1907年.md "wikilink")）
  - [1858年](../Page/1858年.md "wikilink")：[阿道夫·奧克斯](../Page/阿道夫·奧克斯.md "wikilink")，美國報紙出版商、《紐約時報》前擁有人（[1935年逝世](../Page/1935年.md "wikilink")）
  - [1881年](../Page/1881年.md "wikilink")：[穆斯塔法·凯末尔·阿塔土克](../Page/穆斯塔法·凯末尔.md "wikilink")，土耳其国父（[1938年去世](../Page/1938年.md "wikilink")）
  - [1894年](../Page/1894年.md "wikilink")：[葉山嘉樹](../Page/葉山嘉樹.md "wikilink")，日本小說家（[1945年去世](../Page/1945年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[田汉](../Page/田汉.md "wikilink")，中國文藝家、劇作家（[1968年去世](../Page/1968年.md "wikilink")）
  - [1910年](../Page/1910年.md "wikilink")：[大平正芳](../Page/大平正芳.md "wikilink")、日本第68任首相（[1980年去世](../Page/1980年.md "wikilink")）
  - [1915年](../Page/1915年.md "wikilink")：[赛福鼎·艾则孜](../Page/赛福鼎·艾则孜.md "wikilink")，中國共產黨第十、十一屆中央政治局候補委員，曾領導「三區革命」（[2003年逝世](../Page/2003年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[江崎玲於奈](../Page/江崎玲於奈.md "wikilink")，日本物理学家、1973年[諾貝爾物理獎得主](../Page/诺贝尔物理学奖.md "wikilink")
  - [1946年](../Page/1946年.md "wikilink")：[麗莎·明尼利](../Page/麗莎·明尼利.md "wikilink")，美國女演員與歌手
  - [1947年](../Page/1947年.md "wikilink")：[葉復台](../Page/葉復台.md "wikilink")，台灣男藝人
  - [1949年](../Page/1949年.md "wikilink")：[也斯](../Page/也斯.md "wikilink")，香港作家（[2013年去世](../Page/2013年.md "wikilink")）
  - [1956年](../Page/1956年.md "wikilink")：[詹宏志](../Page/詹宏志.md "wikilink")，台灣作家、出版人
  - [1958年](../Page/1958年.md "wikilink")：[朱天心](../Page/朱天心.md "wikilink")，台灣作家
  - [1963年](../Page/1963年.md "wikilink")：[戴玉强](../Page/戴玉强.md "wikilink")，中国男高音歌唱家
  - [1968年](../Page/1968年.md "wikilink")：[本田雄](../Page/本田雄.md "wikilink")，日本動畫人物設計師
  - [1969年](../Page/1969年.md "wikilink")：[岡村明美](../Page/岡村明美.md "wikilink")，日本女性聲優
  - [1971年](../Page/1971年.md "wikilink")：，日本男性演員
  - [1971年](../Page/1971年.md "wikilink")：，香港賽車手
  - [1974年](../Page/1974年.md "wikilink")：[椎名碧流](../Page/椎名碧流.md "wikilink")，日本女性聲優
  - [1975年](../Page/1975年.md "wikilink")：[沈可欣](../Page/沈可欣.md "wikilink")，香港女演員
  - [1976年](../Page/1976年.md "wikilink")：[趙薇](../Page/趙薇.md "wikilink")，中國演員、導演
  - [1978年](../Page/1978年.md "wikilink")：[種村有菜](../Page/種村有菜.md "wikilink")，日本漫畫家
  - [1978年](../Page/1978年.md "wikilink")：[南宮玟](../Page/南宮玟.md "wikilink")，韓國男演員
  - [1979年](../Page/1979年.md "wikilink")：[劉璇](../Page/刘璇_\(体操运动员\).md "wikilink")，中國體操運動員、演员、记者
  - [1981年](../Page/1981年.md "wikilink")：[齋藤千和](../Page/齋藤千和.md "wikilink")，日本女性聲優
  - [1984年](../Page/1984年.md "wikilink")：[舒蕾亞·戈沙爾](../Page/舒蕾亞·戈沙爾.md "wikilink")，印度女歌手
  - [1987年](../Page/1987年.md "wikilink")：[謝茜嘉·哈迪](../Page/謝茜嘉·哈迪.md "wikilink")，美国女子游泳運動員
  - [1988年](../Page/1988年.md "wikilink")：[王啸坤](../Page/王啸坤.md "wikilink")，中国內地男歌手
  - [1988年](../Page/1988年.md "wikilink")：[劉雨欣](../Page/劉雨欣.md "wikilink")，中國內地女演員
  - [1988年](../Page/1988年.md "wikilink")：[大原桃子](../Page/大原桃子.md "wikilink")，日本女性聲優
  - [1995年](../Page/1995年.md "wikilink")：[福田花音](../Page/福田花音.md "wikilink")，日本組合[S/mileage第一期成員](../Page/S/mileage.md "wikilink")
  - [1999年](../Page/1999年.md "wikilink")：[小田櫻](../Page/小田櫻.md "wikilink")，日本組合[早安少女組第十一期成員](../Page/早安少女組。.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[克里斯蒂娜·圭密](../Page/克里斯蒂娜·圭密.md "wikilink")，[美國女歌手](../Page/美國.md "wikilink")

## 逝世

  - [969年](../Page/969年.md "wikilink")：[辽穆宗耶律璟](../Page/辽穆宗.md "wikilink")，辽朝皇帝（[931年出生](../Page/931年.md "wikilink")）
  - [1872年](../Page/1872年.md "wikilink")：[曾國藩](../Page/曾國藩.md "wikilink")，晚清军事家、理学家、政治家，湘军创始人，晚清散文“湘乡派”创立人，洋务运动代表人物（[1811年出生](../Page/1811年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[約翰·巴耳末](../Page/約翰·巴耳末.md "wikilink")，瑞士數學家、物理學家（[1825年出生](../Page/1825年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[孫中山](../Page/孫中山.md "wikilink")，[中華民國國父](../Page/中華民國.md "wikilink")，中國民主革命先驅（[1866年出生](../Page/1866年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：，可口可樂的發明者（[1851年出生](../Page/1851年.md "wikilink")）
  - [1955年](../Page/1955年.md "wikilink")：[查利·帕克](../Page/查利·帕克.md "wikilink")，Bebop爵士樂演奏家（[1920年出生](../Page/1920年.md "wikilink")）
  - [1978年](../Page/1978年.md "wikilink")：[約翰·卡佐爾](../Page/約翰·卡佐爾.md "wikilink")，美國男演員（[1935年出生](../Page/1935年.md "wikilink")）
  - [1993年](../Page/1993年.md "wikilink")：[王震](../Page/王震.md "wikilink")，中国共产党领导人、中國人民解放軍将领、中華人民共和國副主席（[1908年出生](../Page/1908年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[劉紹棠](../Page/劉紹棠.md "wikilink")，中國鄉土文學作家
  - [2000年](../Page/2000年.md "wikilink")：[龚品梅](../Page/龚品梅.md "wikilink")，中国天主教枢机（[1901年出生](../Page/1901年.md "wikilink")）
  - [2001年](../Page/2001年.md "wikilink")：[西瓜刨](../Page/西瓜刨.md "wikilink")，原名林根，香港演員，在粵語片時代飾演牙擦蘇而聞名（[1918年出生](../Page/1918年.md "wikilink")）\[2\]
  - [2003年](../Page/2003年.md "wikilink")：[佐蘭·金吉奇](../Page/佐蘭·金吉奇.md "wikilink")，塞爾維亞總理（[1952年出生](../Page/1952年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：，國際田徑聯合會秘書長（[1943年出生](../Page/1943年.md "wikilink"))\[3\]
  - [2014年](../Page/2014年.md "wikilink")：[維拉·希蒂洛娃](../Page/維拉·希蒂洛娃.md "wikilink")，捷克電影導演（[1929年出生](../Page/1929年.md "wikilink"))
  - [2016年](../Page/2016年.md "wikilink")：[劉天健](../Page/劉天健.md "wikilink")，台灣音樂製作人（[1963年出生](../Page/1963年.md "wikilink"))
  - [2016年](../Page/2016年.md "wikilink")：[劳埃德·沙普利](../Page/劳埃德·沙普利.md "wikilink")，2012年[诺贝尔经济学奖](../Page/诺贝尔经济学奖.md "wikilink")[获得者](../Page/诺贝尔经济学奖得主列表.md "wikilink")（[1923年出生](../Page/1923年.md "wikilink")）

## 节日、风俗习惯

  - [世界反對網絡審查日](../Page/世界反對網絡審查日.md "wikilink")

  - 、：[植树节](../Page/植树节.md "wikilink")

  - ：[國父逝世紀念日](../Page/國父逝世紀念日.md "wikilink")

  - ：[國慶](../Page/國慶.md "wikilink")

## 参考文献

<references />

1.  [US Census - World Population
    Clock](http://www.census.gov/main/www/popclock.html)
2.  [文匯首頁 \> \>
    娛樂-西瓜刨逝世](http://paper.wenweipo.com/2001/03/21/EN0103210008.htm)

3.  [新华网：国际田联秘书长匈牙利人久洛伊逝世
    享年63岁](http://news.xinhuanet.com/sports/2006-03/13/content_4296827.htm)