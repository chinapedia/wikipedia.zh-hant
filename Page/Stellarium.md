**Stellarium**是一款[天文类](../Page/天文學.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")，以[GNU通用公共许可证发布](../Page/GNU通用公共许可证.md "wikilink")，可以运行在[Linux](../Page/Linux.md "wikilink")、[Windows及](../Page/Windows.md "wikilink")[Mac
OS X操作系统中](../Page/Mac_OS_X.md "wikilink")。Stellarium
Mobile是该项目衍生的付费移动应用版本，支持[安卓](../Page/Android.md "wikilink")、[iOS和](../Page/iOS.md "wikilink")[塞班操作系统](../Page/Symbian.md "wikilink")。Stellarium使用[OpenGL对星空做实时](../Page/OpenGL.md "wikilink")[渲染](../Page/渲染.md "wikilink")。

## 功能与特性

Stellarium是一個模擬[天文台](../Page/天文台.md "wikilink")，常用于辅助天文观测。選定時間與地點，即可以看見如同使用眼睛、雙筒望遠鏡或是小型天文望遠鏡所見的[星空](../Page/天球.md "wikilink")，（默认）包括60多萬顆[恆星](../Page/恆星.md "wikilink")、[太陽系的主要天體和](../Page/太陽系.md "wikilink")8万個[深空天體](../Page/深空天體.md "wikilink")。

Stellarium也能够逼真地模擬[日出](../Page/日出.md "wikilink")、[日落](../Page/日落.md "wikilink")、[日食](../Page/日食.md "wikilink")、[月食](../Page/月食.md "wikilink")、[凌日](../Page/凌日.md "wikilink")、[流星雨等各種](../Page/流星雨.md "wikilink")[天文現象](../Page/天文現象.md "wikilink")，具有接近于照片效果的大气层模拟：可以准确地展示大气的散射、折射和温度不均对观测的影响，亦可以模拟城市光污染。Stellarium还可以在[天球上叠加显示实用信息](../Page/天球.md "wikilink")，如[星座圖像](../Page/天宮圖.md "wikilink")、[赤道座標](../Page/赤道座標系統.md "wikilink")、[地平座標和二十餘種世界各地的](../Page/地平座標系統.md "wikilink")[星空文化等](../Page/星座.md "wikilink")。

Stellarium具有高度的可扩展性，可随时在线更新天体数据，也具有连接控制望远镜的能力。\[1\]

## 历史

Stellarium项目源于2001年，由法国程序员Fabien Chéreau发起。

2006年，Stellarium成为了[SourceForge上的特色项目](../Page/SourceForge.md "wikilink")。

现在该项目由Alexander Wolf等人维护。

自2011年，Stellarium成为了[Ubuntu软件中心的特色应用](../Page/Ubuntu_軟體中心.md "wikilink")。

2017年，Stellarium将项目从[SourceForge迁移到了](../Page/SourceForge.md "wikilink")[GitHub](../Page/GitHub.md "wikilink")。\[2\]

### 中文化

較早Stellarium版本對中文的支持較弱，使用者必須自行安裝中文字型才能正確顯示中文。在Stellarium
0.9.0之后的版本中，已经基本解决[中文化问题](../Page/中文化.md "wikilink")。

2007年，[刘春滨依照伊世同的](../Page/刘春滨.md "wikilink")《中西對照恆星圖表1950.0》，製作了「中文星名擴展包」。

2011年，刘春滨製作了「中文星名擴展包【終極版】」
\[3\]，中文星名數量擴充到到3247個，將[中國古星名全部纳入其中](../Page/中西星名对照表.md "wikilink")。

2017年，完整的中国星空文化已经包含在Stellarium发行版本中，并有英、德、俄等多国的本地化。

Stellarium已经将本地化从[Launchpad迁移到了](../Page/Launchpad.md "wikilink")[Transifex](../Page/Transifex.md "wikilink")，和其他国家与地区的本地化工作一样，Stellarium的中文本地化由社区志愿者完成。

## 截圖

<File:Stellarium> 001.jpg|使用0.6.2版本观察的星空及星座图绘 <File:Stellarium> for
Java.jpg|Stellarium的Java版本 0.8.2 <File:Stellarium>
002.jpg|使用0.8.0版本观察的[德国](../Page/德国.md "wikilink")[汉堡的星空](../Page/汉堡.md "wikilink")
[File:Stellarium090.jpg|使用0.9.0版本观察的](File:Stellarium090.jpg%7C使用0.9.0版本观察的)[希腊](../Page/希腊.md "wikilink")[马科普隆的星空](../Page/马科普隆.md "wikilink")
[File:Stellarium-0.12.4-ScreenShot.png|0.12.4繁體中文版](File:Stellarium-0.12.4-ScreenShot.png%7C0.12.4繁體中文版)

## 參見

  - [天文软件](../Page/天文软件.md "wikilink")
  - [Celestia](../Page/Celestia.md "wikilink")
  - [SpaceEngine](../Page/SpaceEngine.md "wikilink")
  - [KStars](../Page/KStars.md "wikilink")：開源的天文學軟體
  - [Cartes du Ciel](../Page/Cartes_du_Ciel.md "wikilink")
  - [Starry Night](../Page/Starry_Night.md "wikilink")
  - [星光飞扬](../Page/星光飞扬.md "wikilink")

## 參考資料

## 外部連結

  - [Stellarium 官方網站](http://www.stellarium.org/zh/)

  - [Stellarium
    Wiki](https://web.archive.org/web/20170601230102/http://www.stellarium.org/wiki/index.php/%E9%A6%96%E9%A0%81)

  - [Stellarium
    正體中文增強包](http://timc.idv.tw/stellarium/)：在Windows上自動完成Stellarium中文化設定的程式。

  - \[<https://web.archive.org/web/20110916154711/http://www.stellarium.org/wiki/index.php/%E4%B8%AD%E6%96%87%E6%98%9F%E5%90%8D%E6%89%A9%E5%85%85%E3%80%90%E7%BB%88%E6%9E%81%E7%89%88%E3%80%91(%E9%80%82%E7%94%A8%E4%BA%8EStellarium_0.10.6.1>)
    中文星名扩充【终极版】\]：將中国古星名全部纳入Stellarium

[Category:天文软件](../Category/天文软件.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")

1.
2.
3.