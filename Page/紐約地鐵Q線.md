**Q線百老匯快車**（），又稱**紐約地鐵Q線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的[地鐵路線](../Page/地鐵.md "wikilink")。由於該線在[曼哈頓使用](../Page/曼哈頓.md "wikilink")[BMT百老匯線](../Page/BMT百老匯線.md "wikilink")，因此其路線徽號為黃色。

Q線任何時候都營運，來往曼哈頓[上東城的](../Page/上東城.md "wikilink")[96街／第二大道與](../Page/96街車站_\(第二大道線\).md "wikilink")[布魯克林](../Page/布魯克林.md "wikilink")[康尼島的](../Page/康尼島.md "wikilink")[康尼島-斯提威爾大道](../Page/康尼島-斯提威爾大道車站_\(BMT布萊頓線\).md "wikilink")。日間列車在曼哈頓以快車營運，在布魯克林以慢車營運，深夜時段停靠全線所有車站。

過去，「QB」與「QT」分別經[曼哈頓大橋和](../Page/曼哈頓大橋.md "wikilink")營運在[BMT布萊頓線](../Page/BMT布萊頓線.md "wikilink")，但隧道服務後來停運。1988年至2001年間，Q線只在平日營運，途經[IND第六大道線前往](../Page/IND第六大道線.md "wikilink")[21街-皇后橋](../Page/21街-皇后橋車站_\(IND_63街線\).md "wikilink")，因此塗為黃色。2001年至2004年間有快車由布萊頓線前往[布萊頓海灘](../Page/布萊頓海灘車站_\(BMT布萊頓線\).md "wikilink")。2010年，Q線延長至皇后區的[阿斯托利亞-迪特馬斯林蔭路](../Page/阿斯托利亞-迪特馬斯林蔭路車站_\(BMT阿斯托利亞線\).md "wikilink")，但在2017年改道到[第二大道線](../Page/第二大道線.md "wikilink")。

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p><a href="../Page/R1_(紐約地鐵車輛).md" title="wikilink">R1</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a></p>
</div></td>
</tr>
</tbody>
</table>

</div>

## 路線

### 服務頻率

以下表格顯示Q線所使用路線，特定時段在有陰影的格的路段內營運\[1\]：

| 路線                                           | 起點                                                       | 終點                                                           | 軌道 | colspan =2 | 時段 |
| -------------------------------------------- | -------------------------------------------------------- | ------------------------------------------------------------ | -- | --------------- |
| 任何時段（深夜除外）                                   | 深夜                                                       |                                                              |    |                 |
| [IND第二大道線](../Page/IND第二大道線.md "wikilink")   | [96街](../Page/96街車站_\(第二大道線\).md "wikilink")             | [72街](../Page/72街車站_\(第二大道線\).md "wikilink")                 | 全部 |                 |
| [BMT 63街線](../Page/63街線.md "wikilink")（全線）   | [萊辛頓大道-63街](../Page/萊辛頓大道-63街車站_\(63街線\).md "wikilink")  | 全部                                                           |    |                 |
| [BMT百老匯線](../Page/BMT百老匯線.md "wikilink")     | [57街-第七大道](../Page/57街-第七大道車站_\(BMT百老匯線\).md "wikilink") | [堅尼街](../Page/堅尼街車站_\(BMT曼哈頓橋線\).md "wikilink")              | 快車 |                 |
| 慢車                                           |                                                          |                                                              |    |                 |
| [曼哈頓大橋](../Page/曼哈頓大橋.md "wikilink")         | 南                                                        |                                                              |    |                 |
| [BMT布萊頓線](../Page/BMT布萊頓線.md "wikilink")（全線） | [德卡爾布大道](../Page/德卡爾布大道車站_\(BMT布萊頓線\).md "wikilink")     | [康尼島-斯提威爾大道](../Page/康尼島-斯提威爾大道車站_\(BMT布萊頓線\).md "wikilink") | 慢車 |                 |

## 歷史

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p><a href="../Page/R27_(紐約地鐵車輛).md" title="wikilink">R27</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</div>

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p>1967-1979年使用（環狀）</p>
</div></td>
</tr>
</tbody>
</table>

</div>

1920年8月1日，列車開始在[BMT布來頓線上營運](../Page/BMT布來頓線.md "wikilink")，自前程公園至得卡大道。週一至週六快車經[曼哈頓橋](../Page/曼哈頓橋.md "wikilink")（下簡稱曼橋）駛往時代廣場-42街車站，慢車經[蒙塔格街隧道](../Page/蒙塔格街隧道.md "wikilink")（下簡稱蒙街隧道）駛往皇后區廣場車站；凌晨時慢車改駛曼橋；晚上以及週日行快車經曼橋至57街/第六大道車站。

1930年代起，加開每日經由曼橋駛往[欽伯斯街晨間尖峰列車](../Page/欽伯斯街.md "wikilink")。\[2\]1950年6月29日，晚上亦加開前述列車，但在一年後加開列車的措施取消。

1952年6月26日，行駛於百老匯的列車全部延駛至57街/第六大道車站。

1955年12月1日，週一至週五的慢車取道剛通車的[BMT60街隧道延駛至七十一大道](../Page/BMT60街隧道.md "wikilink")-洲陸大道-森林高地車站，快車則駛往迪特馬士大道車站。

1957年5月4日，週六列車改行慢車駛往迪特馬士大道車站，以代替經蒙街隧道的慢車。10月24日，經由曼橋的慢車改於王子街車站換軌，夜間列車亦改駛曼橋。

1959年5月28日起，布來頓慢車只於中午行駛。加開的納蘇街特殊列車經蒙街隧道行駛尖峰方向，反向則經曼橋。

隔年11月15日，[R27車廂啟用](../Page/R27_\(紐約地鐵車輛\).md "wikilink")，Q線改為快車，經由蒙街隧道稱QT、經由曼橋稱QB。

1961年1月1日起有部份更動：Q線自57街/第六道道駛往布來頓海灘車站；QT於迪特馬士大道車站發車，訖於斯提威爾大道車站。

隔年4月2日，週六Q線取消，QT於晚上、凌晨、週末訖於迪特馬士大道車站。

1967年11月26日[IND克莉絲蒂街連接道通車](../Page/IND克莉絲蒂街連接道.md "wikilink")，Q、QB、QT等三線取消，改由線以及QJ線代替行駛，但因招致民怨，又開行NX線列車確保如布來頓海灘、海洋園道、西8街等地乘客的權益。

1986年，取消雙字表快車的措施。Q線調整自57街/第六大道發車，於曼哈頓行快車、布魯克林行慢車，訖於斯提威爾大道車站。在曼橋維修期間，Q線與線實施跳暫停措施。

1988年，北邊軌道再度開放、南邊則閉線維修，Q線調整為憑日快車，經[IRT第六大道線駛往](../Page/IRT第六大道線.md "wikilink")57街/的六大道車站（隔年延往21街-皇后橋）。平日夜晚與深夜時，開行區間線列車自57街/第六大道至第二大道。

1990年9月30日，夜晚及凌晨分別被線、線取代。

1995年5月，曼橋北邊軌道於中午、週末閉線維修，Q線調整於布魯克臨行慢車並經蒙街隧道駛往運河街車站。

2001年[911事件後](../Page/911事件.md "wikilink")線停駛，Q線慢車調整於非凌晨時段駛往七十一大道-洲陸大道-森林高地，於10月28日恢復正常。

2003年4月27日起曼橋南邊軌道於週末關閉，Q線於該時段改行駛蒙街隧道。

2004年2月22日曼橋北邊軌道再度開放，加開的Q線列車改由（布魯克林）、（曼哈頓）兩線取代。

## 車站

更詳細的車站列表參見上方列出的路線。

<table style="width:154%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 30%" />
<col style="width: 36%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-Q.svg" title="fig:NYCS-bull-trans-Q.svg">NYCS-bull-trans-Q.svg</a></p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接及備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/曼哈頓.md" title="wikilink">曼哈頓</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/第二大道線.md" title="wikilink">第二大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/96街車站_(第二大道線).md" title="wikilink">96街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/86街車站_(第二大道線).md" title="wikilink">86街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/72街車站_(第二大道線).md" title="wikilink">72街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/63街線.md" title="wikilink">63街線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/萊辛頓大道-63街車站_(63街線).md" title="wikilink">萊辛頓大道-63街</a></p></td>
<td></td>
<td></td>
<td><p><br />
出站轉乘：<br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>，<a href="../Page/59街車站_(IRT萊辛頓大道線).md" title="wikilink">59街</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>，<a href="../Page/萊辛頓大道／59街車站_(BMT百老匯線).md" title="wikilink">萊辛頓大道／59街</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/BMT百老匯線.md" title="wikilink">百老匯線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/57街-第七大道車站_(BMT百老匯線).md" title="wikilink">57街-第七大道</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/49街車站_(BMT百老匯線).md" title="wikilink">49街</a></p></td>
<td></td>
<td><p> ↑</p></td>
<td></td>
<td><p>車站僅北行可<a href="../Page/美國殘疾人法案.md" title="wikilink">無障礙通行</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/時報廣場-42街車站_(BMT百老匯線).md" title="wikilink">時報廣場-42街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）<br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）<br />
（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>，<a href="../Page/42街-航港局客運總站車站_(IND第八大道線).md" title="wikilink">42街-航港局客運總站</a>）<br />
（<a href="../Page/42街接駁線.md" title="wikilink">42街接駁線</a>）</p></td>
<td><p><a href="../Page/紐新航港局客運總站.md" title="wikilink">紐新航港局客運總站</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/34街-先驅廣場車站_(BMT百老匯線).md" title="wikilink">34街-先驅廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a>）</p></td>
<td><p><br />
<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/33街車站_(PATH).md" title="wikilink">33街</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/28街車站_(BMT百老匯線).md" title="wikilink">28街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/23街車站_(BMT百老匯線).md" title="wikilink">23街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/14街-聯合廣場車站_(BMT百老匯線).md" title="wikilink">14街-聯合廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>）<br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第八街-紐約大學車站_(BMT百老匯線).md" title="wikilink">第八街-紐約大學</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/王子街車站_(BMT百老匯線).md" title="wikilink">王子街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/BMT百老匯線.md" title="wikilink">曼哈頓大橋支線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/堅尼街車站_(BMT百老匯線經橋).md" title="wikilink">堅尼街</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>）<br />
（<a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a>）</p></td>
<td><p>停靠下層。</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT布萊頓線.md" title="wikilink">布萊頓線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/德卡爾布大道車站_(BMT第四大道線).md" title="wikilink">德卡爾布大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大西洋大道-巴克萊中心車站_(BMT第四大道線).md" title="wikilink">大西洋大道-巴克萊中心</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT第四大道線.md" title="wikilink">BMT第四大道線</a>）<br />
（<a href="../Page/IRT東公園道線.md" title="wikilink">IRT東公園道線</a>）</p></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第七大道車站_(BMT布萊頓線).md" title="wikilink">第七大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/展望公園車站_(BMT布萊頓線).md" title="wikilink">展望公園</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT富蘭克林大道線.md" title="wikilink">BMT富蘭克林大道線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/帕克賽德大道車站_(BMT布萊頓線).md" title="wikilink">帕克賽德大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/教堂大道車站_(BMT布萊頓線).md" title="wikilink">教堂大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/貝弗里路車站_(BMT布萊頓線).md" title="wikilink">貝弗里路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/科特柳路車站_(BMT布萊頓線).md" title="wikilink">科特柳路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/紐科克廣場車站_(BMT布萊頓線).md" title="wikilink">紐科克廣場</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/H大道車站_(BMT布萊頓線).md" title="wikilink">H大道</a></p></td>
<td></td>
<td><p> ↓</p></td>
<td></td>
<td><p>車站只限南行方向可<a href="../Page/美國殘疾人法案.md" title="wikilink">無障礙通行</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/J大道車站_(BMT布萊頓線).md" title="wikilink">J大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/M大道車站_(BMT布萊頓線).md" title="wikilink">M大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/金斯公路車站_(BMT布萊頓線).md" title="wikilink">金斯公路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/U大道車站_(BMT布萊頓線).md" title="wikilink">U大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/尼克路車站_(BMT布萊頓線).md" title="wikilink">尼克路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/羊頭灣車站_(BMT布萊頓線).md" title="wikilink">羊頭灣</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/布萊頓海灘車站_(BMT布萊頓線).md" title="wikilink">布萊頓海灘</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/海洋公園道車站_(BMT布萊頓線).md" title="wikilink">海洋公園道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/西八街-紐約水族館車站_(BMT布萊頓線).md" title="wikilink">西八街-紐約水族館</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IND卡爾弗線.md" title="wikilink">IND卡爾弗線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/康尼島-斯提威爾大道車站_(BMT布萊頓線).md" title="wikilink">康尼島-斯提威爾大道</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/BMT西城線.md" title="wikilink">BMT西城線</a>）<br />
（<a href="../Page/IND卡爾弗線.md" title="wikilink">IND卡爾弗線</a>）<br />
（<a href="../Page/BMT海灘線.md" title="wikilink">BMT海灘線</a>）</p></td>
<td></td>
</tr>
</tbody>
</table>

## 備註

<references />

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20130208065422/http://www.mta.info/nyct/service/qline.htm)
  - [M線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit -
    M線時刻表（PDF）](https://web.archive.org/web/20031203132516/http://www.mta.info/nyct/service/pdf/tqcur.pdf)

[Q](../Category/紐約地鐵服務路線.md "wikilink")

1.
2.  此為週日至週五開行的特殊列車。