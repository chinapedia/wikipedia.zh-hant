**罗姆人**（Roma）（也稱為**吉普賽人**或**吉卜賽人**\[1\]\[2\]）为起源于[印度北部](../Page/印度.md "wikilink")，散居全世界的流浪[民族](../Page/民族.md "wikilink")。不過，“吉普赛”一词源于[欧洲人对罗姆人起源的误解](../Page/欧洲.md "wikilink")，当时欧洲人认为罗姆人来自[埃及](../Page/埃及.md "wikilink")，于是称之为“埃及人”，而“吉普赛”是“埃及”的音变。而大多数罗姆人也认为“吉普赛人”這個名称有歧视意义，所以並不使用。

其他地区对包括了：[波希米亚人](../Page/波希米亚人.md "wikilink")（法国）、黑塔诺（黑塔娜）（西班牙）、茨冈人（俄罗斯）、埃弗吉特人（阿尔巴尼亚）、罗里人（伊朗）、阿金加诺人（Atsinganoi，希腊）、[阿什卡利人與](../Page/阿什卡利人和巴尔干埃及人.md "wikilink")。羅姆人称自己为罗姆（Rom），敘利亞和波斯吉卜賽人的「[多姆人](../Page/多姆人.md "wikilink")」（Dorn），在梵文中對應是「Doma」，現代印地語是「Dom」，意思是「靠歌舞為生的下等人」。在歐洲羅姆语中，“罗姆”的原意是“人”。\[3\]

罗姆人以其神秘的形象著称，历史上多从事[占卜](../Page/占卜.md "wikilink")、歌舞等职业。但罗姆人也因為流浪與貧窮的生活所演化出的特殊生活方式與求生方法而長期遭受[歧视和迫害](../Page/歧视.md "wikilink")。羅姆人犯罪率的確較高，至今有许多人对罗姆人仍保有极其反面的印象，认为罗姆人是[乞丐](../Page/乞丐.md "wikilink")、[扒手](../Page/扒手.md "wikilink")、[小偷或](../Page/小偷.md "wikilink")[人贩子](../Page/人贩子.md "wikilink")。

## 历史

[Pearson,_Transylvanian_Gipsy_group.png](https://zh.wikipedia.org/wiki/File:Pearson,_Transylvanian_Gipsy_group.png "fig:Pearson,_Transylvanian_Gipsy_group.png")（现属[罗马尼亚](../Page/罗马尼亚.md "wikilink")）的罗姆人\]\]
罗姆人的名稱约于公元1025年出現，據[菲爾多西的](../Page/菲爾多西.md "wikilink")《[列王紀](../Page/列王紀_\(伊朗\).md "wikilink")》記載，中世纪的高度中央集权的君主制国—[依兰沙赫尔國王](../Page/薩珊王朝.md "wikilink")[巴赫拉姆五世曾向](../Page/巴赫拉姆五世.md "wikilink")[印度國王](../Page/笈多王朝.md "wikilink")[商古勒要求提供一萬名](../Page/商古勒.md "wikilink")[囉哩](../Page/囉哩.md "wikilink")[豎琴手供國內平民娛樂](../Page/豎琴.md "wikilink")，国王提供穀种与耕牛，但他們甚麼事也不幹，一年後國王送走他們。他们第一次遷徙是在5世紀時離開印度，第二次是10世紀時，[阿富汗人與](../Page/阿富汗.md "wikilink")[突厥人入侵](../Page/突厥.md "wikilink")，由印度前往波斯，再前往歐洲。

以希腊语称谓“Atsinganoi”从语源学解释羅姆人，学术界出现了两个推论：一说认为西方文化出于对羅姆人的歧视；另一解释是，该群体最早系属于印度西北部的低種姓中下阶层。目前虽然对这两说仍有争论，但较通行的看法是，羅姆人最早是生活在印度西北部，其语言则属印欧语系中的北印度語。

羅姆人最初离开家园的原因和确切时间无从考证，目前可以知道的是，5-7世纪之间，他们出现在伊朗；10-11世纪，他们经过西亞到达巴尔干半岛；15-16世纪，羅姆人已散布于全欧洲。羅姆人从未建立过自己的国家，而且他们恪守其民族传统，刻意与主流社会保持距离，因此在多国都被看作是“二等公民”\[4\]。

德国文献记载，1423年，[神聖羅馬帝國皇帝](../Page/神聖羅馬帝國.md "wikilink")[西吉斯蒙德特准外来的羅姆人居住在他管辖的领域里](../Page/西吉斯蒙德_\(神圣罗马帝国\).md "wikilink")，所有入境的羅姆人都获得一份相当于居留卡的“保护证”。凭着这份“保护证”，羅姆人得以在欧洲[哈布斯堡王朝领区内自由通行](../Page/哈布斯堡王朝.md "wikilink")。可惜这个对羅姆人有若黄金的时代却不到百年，15世纪末，神聖羅馬帝國取消羅姆人的“保护证”，下令驱逐所有境内的羅姆人，违者格杀勿论。不仅神聖羅馬帝國驱逐羅姆人，其他欧洲国家也纷纷采取同样的驱逐政策，当时反羅姆人的理由大致是：传染[黑死病](../Page/黑死病.md "wikilink")、[鼠疫](../Page/鼠疫.md "wikilink")、[霍乱](../Page/霍乱.md "wikilink")、偷窃、行巫术，他们甚至被认为替土耳其人当间谍，还被当作[犹太人的亲戚](../Page/犹太人.md "wikilink")，以上都是敌视他们的罪名。从此，羅姆人就不断地从一地迁往另一地，栖息的地方总是在隐蔽的荒乡僻野\[5\]。

罗姆人长途迁徙的缘故至今尚为史界之谜。罗姆人的祖先是低[种姓的](../Page/种姓.md "wikilink")[印度教徒](../Page/印度教.md "wikilink")，应征入[雇佣兵军队后](../Page/雇佣兵.md "wikilink")，升为[剎帝利](../Page/剎帝利.md "wikilink")（即战士）种姓，同时被派往西面以抵制[伊斯兰教国家的扩张](../Page/伊斯兰教.md "wikilink")。另有学者称，[穆斯林征服印度北部后](../Page/穆斯林.md "wikilink")，其俘虏沦为[奴隶并发展出自身独特的文化](../Page/奴隶.md "wikilink")，成为罗姆人的前身。但罗姆人到达[中东后](../Page/中东.md "wikilink")，为何不折返印度，反而继续前进，进入欧洲，则是不解之谜。罗姆人后来也大量迁入[美洲](../Page/美洲.md "wikilink")。

羅姆人也曾出現在[中國](../Page/中國.md "wikilink")，名為[囉哩回回](../Page/囉哩回回.md "wikilink")，經常[盗竊](../Page/偷竊.md "wikilink")，地方官指他們有[近親相交之嫌](../Page/近親相交.md "wikilink")，理由是他們人數不多，不與外人通婚。

由於文化及宗教上的差異，進入歐洲的罗姆人經常被歐洲人迫害。[纳粹德国也曾迫害他們](../Page/纳粹德国.md "wikilink")\[6\]。

## 名稱

[Romanis-historical-distribution.png](https://zh.wikipedia.org/wiki/File:Romanis-historical-distribution.png "fig:Romanis-historical-distribution.png")

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - [印地語等](../Page/印地語.md "wikilink")：Lambani, Rabari, Banjara

## 人口

[Movimiento_gitano.jpg](https://zh.wikipedia.org/wiki/File:Movimiento_gitano.jpg "fig:Movimiento_gitano.jpg")
全世界约有五百万至一千万罗姆人，其中大多数居住在[欧洲](../Page/欧洲.md "wikilink")。罗姆人主要聚居地有：[巴尔干半岛](../Page/巴尔干半岛.md "wikilink")、[中欧](../Page/中欧.md "wikilink")、[美国及前](../Page/美国.md "wikilink")[苏联加盟共和国](../Page/苏联.md "wikilink")。另外[西欧](../Page/西欧.md "wikilink")、[中东及](../Page/中东.md "wikilink")[北非也有罗姆人居住](../Page/北非.md "wikilink")。

罗姆人在[罗马尼亚](../Page/罗马尼亚.md "wikilink")、[保加利亚](../Page/保加利亚.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[西班牙和](../Page/西班牙.md "wikilink")[美国的人口超过五十万](../Page/美国.md "wikilink")。[保加利亚的罗姆人占总人口的约](../Page/保加利亚.md "wikilink")5%-6%，其比例在世界各国当中排名第一。捷克公开提出願意離開的罗姆人可給津贴。

## 现状

[Eurogipsy.PNG](https://zh.wikipedia.org/wiki/File:Eurogipsy.PNG "fig:Eurogipsy.PNG")
在今日，有一部分罗姆人正试图保持其传统生活方式，居无定所。在[东欧许多地区](../Page/东欧.md "wikilink")，罗姆人定居在生活条件极差的[棚户区内](../Page/棚户区.md "wikilink")，和其他民族居民仍然时有冲突。但在其他地区，罗姆人则靠其传统文化（如罗马尼亚罗姆人的传统铜匠工艺）成功-{致}-富。

在[东欧](../Page/东欧.md "wikilink")[共产主义时期](../Page/共产主义.md "wikilink")，东欧各国由于实行[计划经济](../Page/计划经济.md "wikilink")，罗姆人在就业方面障碍较少，但后来东欧转为[市场经济以后](../Page/市场经济.md "wikilink")，罗姆人失业问题愈显严重。现在多国有大量罗姆人靠[社会福利为生](../Page/社会福利.md "wikilink")，这导致针对罗姆人的[歧视深化](../Page/歧视.md "wikilink")，同时也成为了社会隐患，如斯洛伐克在进行社会福利改革时曾有罗姆人聚居点发生骚乱。

2004年6月，[匈牙利选出了](../Page/匈牙利.md "wikilink")[欧洲议会的第一位罗姆人议员](../Page/欧洲议会.md "wikilink")：女士。同时东欧七国于2005年发起“[容纳罗姆人年代](../Page/容纳罗姆人年代.md "wikilink")”，旨在改善罗姆人的生活。

## 文化

罗姆人结婚较早，不少女性在未满20岁时就已成婚。

[罗姆音乐着重使用高](../Page/罗姆音乐.md "wikilink")[音符以及音符之间大跨度的滑动](../Page/音符.md "wikilink")，歌唱以其情感丰富而著称，罗姆人也是西班牙盛行的[弗拉明戈舞的主要起源](../Page/弗拉明戈舞.md "wikilink")。

## 文學及藝術

直接描写罗姆人的文学、藝術作品有：

  - 小说《[巴黎圣母院](../Page/巴黎圣母院_\(小说\).md "wikilink")》艾斯梅拉达（[维克多·雨果](../Page/维克多·雨果.md "wikilink")）
  - 歌剧《[卡门](../Page/卡门.md "wikilink")》（[乔治·比才](../Page/乔治·比才.md "wikilink")）
  - [墨西哥电影](../Page/墨西哥.md "wikilink")《[叶塞尼亚](../Page/叶塞尼亚.md "wikilink")》
  - 美國女歌星[史蒂薇尼克斯演唱抒情歌曲](../Page/史蒂薇尼克斯.md "wikilink")《[吉普賽](../Page/吉普賽.md "wikilink")》（Gypsy）
  - 拉丁樂壇天后[夏奇拉歌曲](../Page/夏奇拉.md "wikilink")《[吉普賽](../Page/吉普賽.md "wikilink")》（Gypsy）
  - 美國樂壇天后[女神卡卡歌曲](../Page/女神卡卡.md "wikilink")《[吉普賽](../Page/吉普賽.md "wikilink")》（Gypsy）
  - 紀錄片《一路平安》（Tony Gatlif）
  - 美劇 《迷眼》 (Shut Eye)

有关罗姆人（茨冈人）的文艺作品更是不胜枚举，尤其在西洋音乐方面，[李斯特](../Page/李斯特.md "wikilink")（如[匈牙利狂想曲等](../Page/匈牙利狂想曲.md "wikilink")）、[萨拉萨蒂](../Page/萨拉萨蒂.md "wikilink")（[流浪者之歌](../Page/流浪者之歌_\(小提琴作品\).md "wikilink")）、[勃拉姆斯](../Page/勃拉姆斯.md "wikilink")（如[匈牙利舞曲等](../Page/匈牙利舞曲.md "wikilink")）的音乐就多援引羅姆元素。（注意，在以前[茨冈人常与](../Page/茨冈人.md "wikilink")[匈牙利人相混](../Page/匈牙利人.md "wikilink")，所以前述作品中对羅姆常有“匈牙利”的误称。）

## 參見

  - [羅姆語](../Page/羅姆語.md "wikilink")
  - [芬兰罗姆人](../Page/芬兰罗姆人.md "wikilink")

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
{{-}}

[罗姆人](../Category/罗姆人.md "wikilink")

1.  《[牛津英語詞典](../Page/牛津英語詞典.md "wikilink")》：member of a wandering race
    (by themselves called Romany), of Indian origin, which first
    appeared in England about the beginning of the 16th c.

2.  根據英國法律《Caravan Sites and Control of Development Act 1960》定義了吉普賽人

3.

4.
5.
6.