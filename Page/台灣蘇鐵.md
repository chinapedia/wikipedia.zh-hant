**台灣蘇鐵**（[學名](../Page/學名.md "wikilink")：），分布於中國福建、廣東一帶，原始標本將採及於台灣台東、現改名為台東蘇鐵的物種相混淆，而將其[種小名冠以](../Page/種小名.md "wikilink")""(意即「台灣的」)。但根據進一步的型態及生化研究，已經將台灣蘇鐵與[台東蘇鐵在分類學上區隔開了](../Page/台東蘇鐵.md "wikilink")。

[台東蘇鐵](../Page/台東蘇鐵.md "wikilink")（），過去被誤為台灣蘇鐵，實則為不同之兩種植物，英文名稱為Taiwan
Cycas。形成臺灣蘇鐵不產於臺灣的現象。

## 特徵

本種圓柱形樹榦，高達3.5米，幹徑達40釐米。葉集莖頂，葉柄長25-150釐米，具刺。葉身長1.5-3米，羽狀全裂，羽片76-144對，條形，薄革質，平展。雄球花序，圓柱形，長49-70釐米，大孢子葉密生黃褐色絨毛，頂片菱形，寬卵形，長6-16釐米，寬6-16釐米，邊緣篦齒狀分裂。

## 產地

僅產於[中國](../Page/中國.md "wikilink")[福建](../Page/福建.md "wikilink")、[廣東](../Page/廣東.md "wikilink")、沿海區域以及[湖南南部](../Page/湖南.md "wikilink")、[雲南東南部](../Page/雲南.md "wikilink")，族群數量及分布範圍都令其面臨消失的危險。目前列為保護之稀有珍貴樹種。

## 參考資料

## 外部連結

  - [中文百科在線(台灣蘇鐵)](http://www.zwbk.org/MyLemmaShow.aspx?zh=zh-tw&lid=256562)
  - [痞客邦旅遊(台灣蘇鐵)](http://acchl.pixnet.net/blog/post/40076611-%E5%8F%B0%E7%81%A3%E8%98%87%E9%90%B5)
  - [蘇鐵](http://hast.sinica.edu.tw/chinese/plant_c/Sago%20palm.htm)

[Category:台灣植物](../Category/台灣植物.md "wikilink")
[Category:台灣特有種](../Category/台灣特有種.md "wikilink")
[Category:蘇鐵屬](../Category/蘇鐵屬.md "wikilink")
[Category:雌雄異株植物](../Category/雌雄異株植物.md "wikilink")