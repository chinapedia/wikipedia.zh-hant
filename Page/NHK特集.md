《**NHK特集**》（，）是[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）的[紀錄片節目](../Page/紀錄片.md "wikilink")，開播於1976年4月，至1989年4月將節目名稱的[漢字](../Page/日語漢字.md "wikilink")「特集」（）一詞改為[英語拼寫的](../Page/英語.md "wikilink")「Special」。原則上在[日本時間](../Page/日本標準時間.md "wikilink")（UTC+9）每[週日](../Page/週日.md "wikilink")21:00-21:50於[NHK綜合頻道首播](../Page/NHK綜合頻道.md "wikilink")、[週三](../Page/週三.md "wikilink")0:00～0:50重播，不過當逢重大主題時會延長時間播出、並增加播出密度。

## 知名的系列節目

### 「NHK特集」時代

  - 戰\-{zh-hans:后;zh-hant:後}-的日本（1977年－1978年）

  - （第1部：1980年－1981年、第2部：1983年－1984年）音樂：[喜多郎](../Page/喜多郎.md "wikilink")

  - [人类以何为食](../Page/人类以何为食.md "wikilink")
    [食物和文明的世界群像](../Page/食物和文明的世界群像.md "wikilink")（1985年）

  - [大黃河](../Page/大黃河.md "wikilink")（1986年）音樂：[宗次郎](../Page/宗次郎.md "wikilink")

  - 地球大紀行（1987年）音樂：[吉川洋一郎](../Page/吉川洋一郎.md "wikilink")

  - 21世紀的警告

  - （1988年－1989年）音楽：[S.E.N.S.](../Page/S.E.N.S..md "wikilink")

### 「NHK Special」（）時代

  - 驚異的小宇宙 人体（1989年）音樂：[久石讓](../Page/久石讓.md "wikilink")

  - [人类以何为食](../Page/人类以何为食.md "wikilink")
    [亚洲丰富的饮食世界](../Page/亚洲丰富的饮食世界.md "wikilink")（1989年）

  - 電子立國 日本的自傳（1991年）

  - [人类以何为食](../Page/人类以何为食.md "wikilink")
    [河海的猎人们](../Page/河海的猎人们.md "wikilink")（1992年）

  - [太平洋戰爭](../Page/太平洋戰爭_\(紀錄片\).md "wikilink")（1993年）

  - 驚異的小宇宙 人体Ⅱ 大腦與心臟（1993年）音樂：[久石讓](../Page/久石讓.md "wikilink")

  - 生命40億年 遙遠的旅行（1994年）音樂：[大島滿](../Page/大島滿.md "wikilink")

  - [世紀影像](../Page/世紀影像.md "wikilink")（1995年－1996年）音樂：

  - 新·電子立国（1995年－1996年）

  - 戰\-{zh-hans:后;zh-hant:後}-五十年 那時的日本（1995年－1996年）

  - （1996年）音樂：[S.E.N.S.](../Page/神思者.md "wikilink")

  - 家族的肖像（1997年－1998年）

  - 金錢革命（1998年）

  - 海・未知的世界（1998年）

  - 驚異的小宇宙 人体III 遺傳子．DNA（1999年）音樂：[久石讓](../Page/久石讓.md "wikilink")

  - 超越世紀（1999年－2000年）

  - 四大文明（2000年）音樂：[喜多郎](../Page/喜多郎.md "wikilink")

  - 21世紀・日本的課題（2001年－2004年、-{zh-hans:后;zh-hant:後}-改為「未來的日本」）

  - 宇宙 未知への大紀行（2001年）音樂：東儀秀樹、野見祐二

  - [日本人的遙遠旅途](../Page/日本人的遙遠旅途.md "wikilink")（2001年）

  - 非洲 21世紀（2001年）

  - 亞洲古都物語（2001年）

  - [全球市场：财富攻防战](../Page/全球市场：财富攻防战.md "wikilink")（2002年－2003年）

  - [地球大进化](../Page/地球大进化.md "wikilink")（2004年）音樂：土井宏紀

  - 21世紀的潮流（2004年－）

  - 日本的群像・再起的20年（2005年）

  - （2005年）

  - 走近北朝鮮（2006年）

  - [新絲綢之路·動盪的大地紀行](../Page/新絲綢之路·動盪的大地紀行.md "wikilink")（2007年）

  - [激流中国](../Page/激流中国.md "wikilink")（2007年）

  - [金钱资本主义](../Page/金钱资本主义.md "wikilink")（2009年）

  - [生命動能](../Page/生命動能.md "wikilink")（2011年）

  - [生命動能](../Page/生命動能.md "wikilink")（2011年）

  - （2015年）

## 外部連結

  - [NHK特集](http://www.nhk.or.jp/special/)
  - [（播放過的專題一覽）](https://web.archive.org/web/20070528192553/http://www.nhk.or.jp/archives/nhk-special/)
  - [（播放過的專題一覽）](https://web.archive.org/web/20110927011938/http://www.nhk.or.jp/archives/nhk-tokushu/)

[NHK特集](../Category/NHK特集.md "wikilink")
[Category:橋田賞獲獎作品](../Category/橋田賞獲獎作品.md "wikilink")