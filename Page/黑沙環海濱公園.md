**黑沙環海濱公園**（）是位於[澳門半島](../Page/澳門半島.md "wikilink")[黑沙環區海傍的一個市政](../Page/黑沙環.md "wikilink")[公園](../Page/公園.md "wikilink")，佔地約1.3萬平方公尺。

## 沿革

公園原是沒有刻意規劃的地段，只是因開闢[關閘通往](../Page/關閘.md "wikilink")[機場之公路而興建的海傍堤岸](../Page/澳門國際機場.md "wikilink")。後來[澳葡政府才將海傍堤岸規劃為綠化休憩區](../Page/澳葡政府.md "wikilink")，[葡萄牙設計師](../Page/葡萄牙.md "wikilink")[弗郎西斯柯·卡代拉·卡勃蘭](../Page/弗郎西斯柯·卡代拉·卡勃蘭.md "wikilink")(Francisco
Caldeira
Cabral)負責設計。黑沙環海濱公園於1998年6月已峻工，直到1999年3月14日正式揭幕。現時是區內居民[運動](../Page/運動.md "wikilink")、[休閒和](../Page/休閒.md "wikilink")[垂釣的主要場所](../Page/垂釣.md "wikilink")。

## 設施

  - 露天廣場
  - 中葡友誼紀念物：[東方明珠](../Page/東方明珠_\(澳門\).md "wikilink")

<File:A> square at Parque Marginal da Areia Preta.jpg|露天廣場 <File:Parque>
Marginal da Areia Preta 3.jpg|沿海濱而建的步道 <File:Parque> Marginal da Areia
Preta 1.jpg|海濱公園鄰近的道路

## 參考資料

[Category:澳門公園](../Category/澳門公園.md "wikilink")
[Category:澳門康樂設施](../Category/澳門康樂設施.md "wikilink")