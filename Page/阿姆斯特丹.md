**阿姆斯特丹**（），有時也稱其為[荷京](../Page/阿姆斯特丹.md "wikilink")，是[荷兰](../Page/荷兰.md "wikilink")[首都及](../Page/首都.md "wikilink")[最大城市](../Page/荷兰城市列表.md "wikilink")，位于该国西部[省份](../Page/荷兰省份.md "wikilink")[北荷兰省](../Page/北荷兰省.md "wikilink")。根据2008年1月的统计数据，这座城市人口达747,290人；而该城市所处的[兰斯台德都市圈](../Page/兰斯台德.md "wikilink")，大约有670万人口，是[欧洲](../Page/欧洲.md "wikilink")[第6大](../Page/依人口划分的欧洲都市圈列表.md "wikilink")[都市圈](../Page/都市圈.md "wikilink")。

其名称源于*Amstel dam*，\[1\]
这表明了该城市的起源：一个位于[阿姆斯特尔河上的](../Page/阿姆斯特尔河.md "wikilink")[水坝](../Page/水坝.md "wikilink")，即今[水坝广场址](../Page/水坝广场.md "wikilink")。12世纪晚期一个小渔村建于此，而后由于[贸易的迅猛发展](../Page/贸易.md "wikilink")，阿姆斯特丹在[荷兰黄金时代一跃而成为世界上最重要的](../Page/荷兰黄金时代.md "wikilink")[港口](../Page/港口.md "wikilink")。在那个时代，该城是金融和[钻石的中心](../Page/钻石.md "wikilink")。\[2\]
19和20世纪，该城[扩展](../Page/阿姆斯特丹的扩展.md "wikilink")，许多新的街坊与近郊住宅区形成。

阿姆斯特丹是荷兰的[金融和](../Page/金融.md "wikilink")[文化首都](../Page/文化.md "wikilink")\[3\]。许多荷兰大型机构的总部都设于此，其中包括[飞利浦和](../Page/飞利浦.md "wikilink")[ING等](../Page/ING.md "wikilink")7家世界500强企业的总部\[4\]。作为[泛欧交易所的一部分](../Page/泛欧交易所.md "wikilink")，[阿姆斯特丹证券交易所坐落于城市中心](../Page/阿姆斯特丹证券交易所.md "wikilink")。阿姆斯特丹有很多[旅游景点](../Page/阿姆斯特丹旅游景点列表.md "wikilink")，包括历史悠久的[运河网](../Page/阿姆斯特丹运河.md "wikilink")、[荷兰国家博物馆](../Page/荷兰国家博物馆.md "wikilink")、[凡·高博物馆](../Page/凡·高博物馆.md "wikilink")、[安妮之家](../Page/安妮之家.md "wikilink")、[红灯区以及许多](../Page/红灯区.md "wikilink")[大麻咖啡馆](../Page/大麻咖啡馆.md "wikilink")。每年有大约420万游客来此观光\[5\]。

作为当前荷兰第一大城市，阿姆斯特丹历经了从渔村到国际化大都市的发展过程，经历了辉煌与破坏，以及世界大战的洗礼，从一定程度上讲，她的历史也是荷兰历史的一个缩影。

## 历史

阿姆斯特丹的历史最早可以追溯到13世纪时的渔村。人们曾在附近[阿姆斯特尔河上建筑](../Page/阿姆斯特尔河.md "wikilink")[水坝](../Page/水坝.md "wikilink")，阿姆斯特丹就得名于此。原来的名字“Amstelredam”，意指“阿姆斯特尔水坝”。17世纪是阿姆斯特丹历史上的“[黄金时代](../Page/荷蘭黃金時代.md "wikilink")”。阿姆斯特丹是当时世界上最重要的[港口和](../Page/港口.md "wikilink")[银行业中心](../Page/银行.md "wikilink")。

### 城市建立和初期发展

在上个千年之初，一些冒险者乘着由挖空的原木做成的船从阿姆斯特尔河顺流而下，并在河周围的[沼泽湿地之外修建了](../Page/沼泽.md "wikilink")[堤坝](../Page/堤坝.md "wikilink")。“阿姆斯特丹”这个词最早于1275年10月27日被记录在册。当年，荷兰[伯爵](../Page/伯爵.md "wikilink")免除了通过这座大坝的费用\[6\]。史料将最早居住在大坝周边的居民叫做“homines
manentes apud
Amestelledamme”。到了1327年，这个名称演化为“Aemsterdam”。与[奈梅亨](../Page/奈梅亨.md "wikilink")、[鹿特丹与](../Page/鹿特丹.md "wikilink")[乌特勒支等更古老的荷兰城市相比](../Page/乌特勒支.md "wikilink")，阿姆斯特丹的历史相对比較短暂。[2008年10月](../Page/2008年10月.md "wikilink")，历史地理学家克里斯·德·邦特（Chris
de
Bont）聲稱，早在10世纪，阿姆斯特丹周边的土地就已经存在开垦的痕迹。当然，这并不意味着当时该地区已经被开垦。肥沃的土地可能尚未有人耕种，这可能是生产[泥炭所留下的痕迹](../Page/泥炭.md "wikilink")\[7\]。

阿姆斯特丹於1300年（一说1306年）被正式授予[城市资格](../Page/城市.md "wikilink")\[8\]。从14世纪起，阿姆斯特丹开始蓬勃发展，这主要归功于与[汉萨同盟的贸易](../Page/汉萨同盟.md "wikilink")。1345年，卡尔弗街成了市民[朝圣的地方](../Page/朝圣.md "wikilink")，直到[新教成为了荷兰的国教](../Page/新教.md "wikilink")。这些朝圣礼仪现今已经不存在，只剩下当时那些华贵的服装供后人瞻仰\[9\]。

[Amsterdam_in_1538_-_bMA.jpg](https://zh.wikipedia.org/wiki/File:Amsterdam_in_1538_-_bMA.jpg "fig:Amsterdam_in_1538_-_bMA.jpg")
16世纪，由于当时统治荷兰的[西班牙王室开始推行新的税收政策](../Page/西班牙.md "wikilink")，以及建立迫害新教徒的[西班牙宗教裁判所](../Page/西班牙宗教裁判所.md "wikilink")，荷兰人开始反抗西班牙国王[腓力二世和他的继任者](../Page/腓力二世_\(西班牙\).md "wikilink")。新兴资本孕育出了一大批新教徒，再加上[加尔文教徒们开始反抗](../Page/加爾文教派.md "wikilink")，这场[起义不久升级为](../Page/起义.md "wikilink")“[80年战争](../Page/80年战争.md "wikilink")”，并最终促成了荷兰的[独立](../Page/独立.md "wikilink")\[10\]。起义的领袖，“[值得赞美的奥兰治拿骚的威廉](../Page/威廉一世_\(奥兰治\).md "wikilink")”宣布荷兰北方8个省（包括阿姆斯特丹）独立为[荷兰共和国](../Page/荷兰共和国.md "wikilink")，他也成为了第一位荷兰皇室成员。他推行了一系列较为宽松的宗教政策，使得[伊比利亚半岛的](../Page/伊比利亚半岛.md "wikilink")[犹太人](../Page/犹太人.md "wikilink")、[法国的](../Page/法国.md "wikilink")[胡格诺派](../Page/胡格诺派.md "wikilink")、[佛兰德斯的富商和印刷工](../Page/佛兰德斯.md "wikilink")，以及来自西班牙控制的[低地国家的经济与宗教難民在阿姆斯特丹找到了安全的栖身之所](../Page/低地国家.md "wikilink")。佛兰德印刷工的涌入以及对各种思想的包容使得阿姆斯特丹成为了欧洲的自由出版中心\[11\]。

### 黄金年代

[AmsterdamDamsquar.jpg](https://zh.wikipedia.org/wiki/File:AmsterdamDamsquar.jpg "fig:AmsterdamDamsquar.jpg")

[Anthonie_Heinsius.JPG](https://zh.wikipedia.org/wiki/File:Anthonie_Heinsius.JPG "fig:Anthonie_Heinsius.JPG")末期的大議長。18世紀頭二十年的荷蘭領袖，是[西班牙王位繼承戰爭的靈魂人物](../Page/西班牙王位繼承戰爭.md "wikilink")，任期內見證聯省共和國最後的輝煌與衰落\]\]
17世纪被认为是阿姆斯特丹的黄金年代。荷兰商船从阿姆斯特丹开往[波罗的海](../Page/波罗的海.md "wikilink")、[北美洲和](../Page/北美洲.md "wikilink")[非洲](../Page/非洲.md "wikilink")，以及今天的[印尼](../Page/印尼.md "wikilink")、[印度](../Page/印度.md "wikilink")、[斯里兰卡和](../Page/斯里兰卡.md "wikilink")[巴西](../Page/巴西.md "wikilink")，由此构建了世界贸易网络的基础。[荷兰东印度公司与](../Page/荷兰东印度公司.md "wikilink")[荷兰西印度公司发行的大量](../Page/荷兰西印度公司.md "wikilink")[股票为阿姆斯特丹商人所拥有](../Page/股票.md "wikilink")。这两个公司所夺得的海外属地后来演变为了荷兰的[殖民地](../Page/殖民地.md "wikilink")。阿姆斯特丹也在此时成为了欧洲航运和世界融资的中心\[12\]。1602年，荷兰东印度公司的阿姆斯特丹办公室开始出售自己的[股票](../Page/股票.md "wikilink")，并成为了世界上第一家[证券交易所](../Page/证券交易所.md "wikilink")\[13\]。
然而，从18世纪开始，阿姆斯特丹的繁荣开始褪色。荷兰与[英国和](../Page/英国.md "wikilink")[法国之间的战争](../Page/法国.md "wikilink")，打击了处于巅峰的阿姆斯特丹。后来，荷兰被[拿破仑率领的法国军队所占领](../Page/拿破仑.md "wikilink")。直到1815年，摆脱法国统治的荷兰与现在的[比利时和](../Page/比利时.md "wikilink")[卢森堡组成荷兰王国](../Page/卢森堡.md "wikilink")，这座城市才迎来了发展的第二次春天。

19世纪末期也被称作阿姆斯特丹的第二个黄金年代\[14\]。该市新建成了一些[博物馆](../Page/博物馆.md "wikilink")、[中央车站以及](../Page/阿姆斯特丹中央车站.md "wikilink")[阿姆斯特丹音乐厅](../Page/音乐厅_\(阿姆斯特丹\).md "wikilink")。与此同时，她也迎来了[工业革命](../Page/工业革命.md "wikilink")。[阿姆斯特丹-莱茵运河](../Page/阿姆斯特丹-莱茵运河.md "wikilink")（）的成功开掘也是这座城市直接连接到了[莱茵河](../Page/莱茵河.md "wikilink")；同时[北海运河也缩短了城市与北海的距离](../Page/北海运河.md "wikilink")。两项工程极大地促进了与欧洲及世界其他地方的商业交流。1906年，作家[约瑟夫·康拉德用](../Page/约瑟夫·康拉德.md "wikilink")“[海之镜](http://en.wikiquote.org/wiki/Joseph_Conrad#The_Mirror_of_the_Sea_.281906.29)”精辟的从海边眺望阿姆斯特丹的景象。[第一次世界大战前](../Page/第一次世界大战.md "wikilink")，该城市的规模开始拓展，建立了一些新的[郊区](../Page/郊区.md "wikilink")。

[AmstelAmsterdamNederland.jpg](https://zh.wikipedia.org/wiki/File:AmstelAmsterdamNederland.jpg "fig:AmstelAmsterdamNederland.jpg")

### 世界大战期间

尽管在一战中荷兰保持了[中立](../Page/中立.md "wikilink")，但阿姆斯特丹还是遭受了[食品和](../Page/食品.md "wikilink")[燃料的短缺](../Page/燃料.md "wikilink")，甚至由此还发生小规模的[暴乱](../Page/暴乱.md "wikilink")，并造成了人员的伤亡。这次暴乱被称为“[马铃薯暴乱](../Page/马铃薯暴乱.md "wikilink")”（）。人们开始抢劫[商店和](../Page/商店.md "wikilink")[仓库](../Page/仓库.md "wikilink")，以获得生活必需品，主要是食品\[15\]。

1940年5月10日，[纳粹德国](../Page/纳粹德国.md "wikilink")[入侵荷兰](../Page/荷蘭戰役.md "wikilink")。德国人在阿姆斯特丹建立了纳粹政权并开始迫害犹太人。一些市民冒着极大的风险庇护犹太人，但最终超过十万荷兰籍犹太人被关进[集中营](../Page/集中营.md "wikilink")。其中最著名的受害者，就是死在[伯根-贝尔森集中营的](../Page/伯根-贝尔森集中营.md "wikilink")[安妮·弗兰克](../Page/安妮·弗兰克.md "wikilink")\[16\]。只有5000名左右的荷兰籍犹太人幸免于难。二战末期，阿姆斯特丹与其他地区的通讯完全中断，食品和燃料也极度短缺。许多市民涌入农村避难。狗、猫、生甜菜以及郁金香球根都被人们当作食物充饥\[17\]。阿姆斯特丹的许多树木被市民砍伐当作柴火；犹太人的房子也被拆掉，其中的木料也被一抢而空。

### 战后的发展

[NieuwmarktAmsterdam.jpg](https://zh.wikipedia.org/wiki/File:NieuwmarktAmsterdam.jpg "fig:NieuwmarktAmsterdam.jpg")
[二战後](../Page/二战.md "wikilink")，阿姆斯特丹[市郊得到了进一步发展](../Page/市郊.md "wikilink")。许多公园和广场建于市郊，新的居民住宅也在那里建立，一般拥有更为宽敞明亮的空间、[花园以及](../Page/花园.md "wikilink")[阳台](../Page/阳台.md "wikilink")。由于两次世界大战的消耗，当政者和其他有影响力的人物试图重新规划阿姆斯特丹。[-{zh-cn:写字楼;zh-hk:寫字樓;zh-tw:辦公大樓;}-的需求与日俱增](../Page/写字楼.md "wikilink")；而由于[汽车逐渐进入寻常百姓家](../Page/汽车.md "wikilink")，对新建道路的需求也大大增加。1977年，阿姆斯特丹兴建了第一条从市中心通往的[地铁](../Page/地铁.md "wikilink")。更长远的规划则是建设一条连接中央车站、市中心与城市其他地区的高速路。

战前的犹太人聚居区后来逐渐被拆除。一些窄小的街道，如犹太大街，因为需要拓宽的缘故，原来的老房子大多遭到拆除。大批拆除老建筑也激怒了一部分市民，并最终引发了“[新广场骚乱](../Page/新广场骚乱.md "wikilink")”（）\[18\]。后来，市民的示威收到了效果，街道的拆除扩建被终止，只会在地下继续修建地铁，这也很大程度上保护了老建筑和城市风貌。新的[市政厅就建立在几乎完全拆毁的](../Page/市政厅.md "wikilink")[滑铁卢广场上](../Page/滑铁卢广场.md "wikilink")。与此同时，大量旨在恢复城市中心风貌的私人机构建立起来。尽管他们的努力在今天看来已经有了不错的成效，城市的风貌得到了恢复，但他们的工作仍在延续。现在市中心已经基本恢复了她黄金时代的原貌，并且成为了城市历史保护区。这里许多建筑已经被划为[文物](../Page/文物.md "wikilink")，其中如荷兰运河等正在申报[世界遗产](../Page/世界遗产.md "wikilink")\[19\]。

## 地理

[Amsterdam_4.89943E_52.37109N.jpg](https://zh.wikipedia.org/wiki/File:Amsterdam_4.89943E_52.37109N.jpg "fig:Amsterdam_4.89943E_52.37109N.jpg")
作为[北荷兰省的一部分](../Page/北荷兰省.md "wikilink")，阿姆斯特丹坐落在荷兰的西北部，与[乌特勒支省和](../Page/乌特勒支省.md "wikilink")[弗莱福兰省相邻](../Page/弗莱福兰省.md "wikilink")。阿姆斯特尔河在市中心分流进许多小运河，最终流入了[IJ湾](../Page/IJ湾.md "wikilink")。阿姆斯特丹平均[海拔为](../Page/海拔.md "wikilink")2-{zh:米;zh-hans:米;zh-hk:米;zh-tw:公尺;}-\[20\]</ref>。城市主要地形是[平原](../Page/平原.md "wikilink")，西南部是一片人造森林。北海运河将阿姆斯特丹与[北海连接起来](../Page/北海_\(大西洋\).md "wikilink")。

阿姆斯特丹及周边地区已经高度[城市化](../Page/城市化.md "wikilink")。该市[面积为](../Page/面积.md "wikilink")219.4平方-{zh:千米;zh-hans:千米;zh-hk:千米;zh-tw:公里;}-，人口密度为每平方-{zh:千米;zh-hans:千米;zh-hk:千米;zh-tw:公里;}-4457人，房屋密度为每平方-{zh:千米;zh-hans:千米;zh-hk:千米;zh-tw:公里;}-2275间\[21\]。该城市林木覆盖率达到12%\[22\]。
阿姆斯特丹气候宜人，天气情况主要受到来自[北海的气流影响](../Page/北海.md "wikilink")，属于终年温和的[温带海洋性气候](../Page/温带海洋性气候.md "wikilink")。[冬季气温温和](../Page/冬季.md "wikilink")，很少低过0 °C。按照美国农业部最新的评级，处于欧洲大陆北端的阿姆斯特丹及北荷兰省大部分地区属于9级抗寒区。遭受来自欧洲大陆、[斯堪的那维亚](../Page/斯堪的那维亚.md "wikilink")、[俄罗斯以及](../Page/俄罗斯.md "wikilink")[西伯利亚的](../Page/西伯利亚.md "wikilink")[寒流侵袭的少数地区可能出现霜冻](../Page/寒流.md "wikilink")。由于阿姆斯特丹三面环水，并且具有很强的[热岛效应](../Page/热岛效应.md "wikilink")，夜间气温很少低过-5 °C，但是25-{zh:千米;zh-hans:千米;zh-hk:千米;zh-tw:公里;}-外的东南市郊[希爾弗瑟姆](../Page/希爾弗瑟姆.md "wikilink")，最低气温可达-12 °C。夏季温暖但不炎热。八月平均最高气温仅有22 °C，超过30 °C的高温一般来讲只有3天左右。该城市平均每年有约175天降水，但年平均降水量只有不到760-{zh:毫米;zh-hans:毫米;zh-hk:毫米;zh-tw:公釐;}-。雨季一般从10月到次年3月，降水方式以小雨为主。极少数情况下，该城市会遭受[暴风雨](../Page/暴风雨.md "wikilink")。

## 行政

阿姆斯特丹分为7个行政区域。毗邻[迪门和](../Page/迪门.md "wikilink")[阿姆斯特尔芬](../Page/阿姆斯特尔芬.md "wikilink")。

### 概况

[AmsterdamStadsdelen.png](https://zh.wikipedia.org/wiki/File:AmsterdamStadsdelen.png "fig:AmsterdamStadsdelen.png")
一般人们所说的阿姆斯特丹仅指市区部分，有些偏远的乡村并没有算在阿姆斯特丹的范围之内。阿姆斯特丹一般来讲分为三个范畴：阿姆斯特丹都市圈、大阿姆斯特丹（）和阿姆斯特丹市区。阿姆斯特丹统计局的统计范围一般指的是城市市区和两个城市郊区。

按照2006年的统计数据，该城城区人口大约为742,981人；整个都市圈人口大约为1,021,870人；包含7个区域的大阿姆斯特丹人口则为1,211,503人；再加上人口稠密的[赞斯塔德区](../Page/赞斯塔德.md "wikilink")，这些区域总共约有1,468,122人；而包括阿姆斯特丹在内的[兰斯台德地区则有人口](../Page/兰斯台德.md "wikilink")6,659,300人。

### 市政府

就像所有的荷兰城市一样，阿姆斯特丹的行政当局是由市长领导的市[政府和市](../Page/政府.md "wikilink")[议会组成的](../Page/议会.md "wikilink")。然而，与其它城市不同的是，虽然城市分为7个区，但是由于实行统一的行政制度，所以执政效率较高。市政府的活动是由各区负责具体执行的，7个区域有自己的民选议会。

2010年5月1日，阿姆斯特丹正式执行在2009年由市议会通过的“精简城市行政区划”的决议。城市行政分区由原先的15个缩编为7个\[23\]。

### 国家政府

现行的[荷兰王国宪法第](../Page/荷蘭憲法.md "wikilink")2章第32条规定：「国王的加冕仪式在首都阿姆斯特丹 (“de
hoofdstad
Amsterdam”)举行。」而以前的宪法并没有明确将阿姆斯特丹称为首都。事实上，除去1808年到1810年，荷兰政府、[国会和](../Page/国会.md "wikilink")[最高法院一直设在](../Page/最高法院.md "wikilink")[海牙](../Page/海牙.md "wikilink")，各国的使领馆也均位于海牙。除此之外，阿姆斯特丹也不是所在的[北荷兰省的首府](../Page/北荷兰省.md "wikilink")。

## 经济

阿姆斯特丹是荷兰金融商贸之都\[24\]。她也是欧洲乃至世界上最好的国际贸易都市之一。她在一项排名中位列第五，排在[伦敦](../Page/伦敦.md "wikilink")、[巴黎](../Page/巴黎.md "wikilink")、[法兰克福和](../Page/法兰克福.md "wikilink")[巴塞罗那之后](../Page/巴塞罗那.md "wikilink")\[25\]。许多荷兰的大型企业和银行都把它们的总部设于阿姆斯特丹，其中包括[荷兰银行](../Page/荷兰银行.md "wikilink")、[飞利浦](../Page/飞利浦.md "wikilink")、[阿克蘇諾貝爾](../Page/阿克蘇諾貝爾.md "wikilink")、[ING集团](../Page/ING集团.md "wikilink")、[皇家阿霍德](../Page/皇家阿霍德.md "wikilink")、[海尼根和](../Page/海尼根.md "wikilink")[绿色和平组织](../Page/绿色和平.md "wikilink")；另外，包括[荷兰皇家航空和](../Page/荷兰皇家航空.md "wikilink")[毕马威等知名跨国企业也将它的全球总部设立与该市南边的](../Page/毕马威.md "wikilink")[阿姆斯特尔芬](../Page/阿姆斯特尔芬.md "wikilink")。

虽然很多小公司仍在坐落于老运河附近，但是更多的公司选择在市中心之外建立。成为了新的金融和法律枢纽\[26\]。该区域不仅座拥荷兰前五大律师事务所，而且还拥有包括[波士頓顧問集團以及](../Page/波士頓顧問集團.md "wikilink")[埃森哲公司在内的多家咨询机构](../Page/埃森哲公司.md "wikilink")。[阿姆斯特丹世界贸易中心也坐落于此](../Page/世界贸易中心_\(阿姆斯特丹\).md "wikilink")。

这座城市还有三个小型金融区。其中一个位于[阿姆斯特丹斯洛特迪克火车站附近](../Page/阿姆斯特丹斯洛特迪克火车站.md "wikilink")。包括[电讯报](../Page/电讯报.md "wikilink")（）等几家报业公司、大众运输公司与荷兰电报公司位于此地。第二处位于[阿姆斯特丹球场周边](../Page/阿姆斯特丹球场.md "wikilink")。而第三处则在[阿姆斯特尔河车站附近](../Page/阿姆斯特尔河车站.md "wikilink")。阿姆斯特丹最高的建筑，就是飞利浦的总部，[伦勃朗大厦](../Page/伦勃朗大厦.md "wikilink")\[27\]\[28\]。

[阿姆斯特丹证券交易所](../Page/阿姆斯特丹证券交易所.md "wikilink")（AEX）现今已加入[泛欧交易所](../Page/泛欧交易所.md "wikilink")，它是世界上最古老的证券交易所，而且是欧洲最大的交易所之一。它坐落在市中心[水坝广场的旁边](../Page/水坝广场.md "wikilink")。

## 城市景观与建筑

[AnneFrankHuisAmsterdam.jpg](https://zh.wikipedia.org/wiki/File:AnneFrankHuisAmsterdam.jpg "fig:AnneFrankHuisAmsterdam.jpg")\]\]
[阿姆斯特丹中央车站是市民南下的必经之路](../Page/阿姆斯特丹中央车站.md "wikilink")。而[达姆拉克大街是通往](../Page/达姆拉克大街.md "wikilink")[罗金街的主要道路](../Page/罗金街.md "wikilink")。而城市裡最古老的地区是[德瓦伦](../Page/德瓦伦.md "wikilink")。它位于达姆拉克大街东侧，著名的[红灯区也位于此处](../Page/红灯区.md "wikilink")。而它的南部就是犹太人聚居区——滑铁卢广场。环绕于市中心的，就是始建于17世纪的[荷兰运河](../Page/荷兰运河.md "wikilink")（）。越过荷兰运河就是原先基层的聚居区。

### 运河

精心的城市规划造就了现在阿姆斯特丹的运河系统\[29\]。17世纪早期，人们就开始营造以IJ湾为核心的半圆形运河系统。著名的荷兰运河就是为了发展居民区而兴建的，它主要分为[绅士运河](../Page/绅士运河.md "wikilink")、[皇帝运河与](../Page/皇帝运河.md "wikilink")[王子运河三部分](../Page/王子运河.md "wikilink")。而第四运河[辛厄尔运河](../Page/辛厄尔运河.md "wikilink")（）（不要与古老的[辛厄尔](../Page/辛厄尔.md "wikilink")（[辛格運河](../Page/辛格運河.md "wikilink")）相混淆）主要是为[水管理和城市防御而修建](../Page/水管理.md "wikilink")。防御工事主要修建于城门附近，是由护城河以及防御堤坝构成。

工程开始于1613年，由东向西延伸，跨越整个城市。历史学家[黑特·马柯](../Page/黑特·马柯.md "wikilink")（）形容其像一个巨大的雨刷。南部工程于1656年竣工。随后，居民区的建设也缓慢开展，但是东部城区的辐射阿姆斯特尔河与IJ湾运河计划却从未得到实施。在接下来的几个世纪裡，这些地区被用来兴建[公园](../Page/公园.md "wikilink")、[敬老院](../Page/老人院.md "wikilink")、[剧院和其他公共设施](../Page/剧院.md "wikilink")，却没有兴建水利\[30\]。

如今，一些运河已经被改建成了街道\[31\]。

### 扩建

在运河建成之后的两个多世纪裡，阿姆斯特丹并没有扩建。而扩建的第一个实施者就是[萨穆埃尔·萨法蒂](../Page/萨穆埃尔·萨法蒂.md "wikilink")（）。当时他希望把阿姆斯特丹建设成下一个[巴黎或](../Page/巴黎.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")。他计划在荷兰运河之外，建设新的房屋和公共设施。更主要的目的，是通过改善居民的居住环境来改善市民的健康状况。虽然该计划并没有很大程度上拓展城市规模，但这的确带给阿姆斯特丹一些大型公共建筑\[32\]\[33\]\[34\]。

在萨法蒂之后，范·尼夫特里克（Van
Niftrik）和卡尔夫（Kalff）规划了市中心周围的环状地带。而这些环状地带后来大多用于建造工薪阶层的住所\[35\]。

20世纪，随着[人口的增加](../Page/人口.md "wikilink")，城市急需得到拓展。人们提出了两套方案，以建设一个前所未有的阿姆斯特丹，其中第一套方案的设计者名叫[亨德里克·伯爾拉赫](../Page/亨德里克·伯爾拉赫.md "wikilink")（），他的方案指出，应该设计一系列的[社区](../Page/社区.md "wikilink")，以供各个阶层的市民居者有其屋\[36\]\[37\]。

二战後，城市的西部、南部和北部建设了大量新社区。新社区的建立不仅解决了城市住房短缺的问题，并且为市民提供了现代化生活的便利\[38\]\[39\]。

### 建筑

[Thèrése_Schwartzeplein_Amsterdam.jpg](https://zh.wikipedia.org/wiki/File:Thèrése_Schwartzeplein_Amsterdam.jpg "fig:Thèrése_Schwartzeplein_Amsterdam.jpg")
阿姆斯特丹拥有悠久的建筑历史。其中最古老的建筑当属[贝居安会院](../Page/贝居安会院_\(阿姆斯特丹\).md "wikilink")\[40\]。它竣工于1425年，是阿姆斯特丹仅存的两座纯木质建筑之一。它也是阿姆斯特丹少见的[哥特式建筑](../Page/哥特式建筑.md "wikilink")。从16世纪开始，砖石建筑逐渐取代了木质建筑的地位。这个时期的很多建筑都带有明显的文艺复兴时期的风格。后来，在[亨德里克·德·凯泽](../Page/亨德里克·德·凯泽.md "wikilink")（）的指导下，阿姆斯特丹迅速树立了独特的文艺复兴建筑风格\[41\]。其中最著名的建筑是由凯泽设计的[西教堂](../Page/西教堂.md "wikilink")。17世纪，[巴洛克风格建筑在欧洲风行](../Page/巴洛克建筑.md "wikilink")。这也正是阿姆斯特丹的黄金年代。[雅各布·范·坎彭](../Page/雅各布·范·坎彭.md "wikilink")（）等建筑师领导了这一建筑潮流\[42\]。其中[菲利普·文布恩斯](../Page/菲利普·文布恩斯.md "wikilink")（）为这座城市的商人设计了房屋。阿姆斯特丹最著名巴洛克式建筑是建于[水坝广场的王宫](../Page/水坝广场.md "wikilink")。而18世纪的建筑很大程度上被法国风格所影响。

1815年前后，建筑师开始打破巴洛克式建筑的桎梏，开始转向设计多种全新风格的建筑\[43\]。大多数新哥特式建筑建造于那个时代。而在19世纪末，随着[新艺术风格的流行](../Page/新艺术运动.md "wikilink")，更多这类风格的新建筑落成。这个时期阿姆斯特丹的规模迅速扩张，但新城区的建筑风格和老城区保持了一致。在[博物馆广场附近的房屋就是新艺术风格的最好代表](../Page/博物馆广场_\(阿姆斯特丹\).md "wikilink")。在那个时期最后一种流行的建筑风格是装饰艺术。而阿姆斯特丹的装饰艺术风格也自成一体，它的杰出代表就是\[44\]，其中城市一些区域整体建筑风格都是如此。阿姆斯特丹学派建筑最显著的特征，就是建筑外墙都具有绚丽的装饰，门窗的形状都很奇特。

200Đ世纪建造的新艺术风格建筑一般分布在市区周围，但现在市中心也出现了这种风格的建筑。而更富历史气息的建筑大多在市中心，比如运河旁的商铺。

## 交通

[Amsterdam_bike.jpg](https://zh.wikipedia.org/wiki/File:Amsterdam_bike.jpg "fig:Amsterdam_bike.jpg")
[Neatherlands_Amsterdam_CanalSide.jpg](https://zh.wikipedia.org/wiki/File:Neatherlands_Amsterdam_CanalSide.jpg "fig:Neatherlands_Amsterdam_CanalSide.jpg")
阿姆斯特丹鼓励市民利用[自行车出行](../Page/自行车.md "wikilink")，很多道路都配有自行车道和存车处。据2006年的统计数据，该市约有465,000辆自行车\[45\]。然而自行车被盗的现象也比较普遍，在2005年，大约有5,400辆的自行车被盗\[46\]。市民的日常生活也离不开自行车，哪怕像接孩子上下学和购买食品这样的小事。各种各样的自行车都可以在阿姆斯特丹找到，比如公路自行车、山地自行车，甚至比赛用自行车。阿姆斯特丹的交通状况还是比较安全的，2007年，有18人不幸在[交通事故中](../Page/交通事故.md "wikilink")[遇难](../Page/遇难.md "wikilink")，相比之下，该市有26人死于[谋杀](../Page/谋杀.md "wikilink")\[47\]\[48\]。

当局并不鼓励驾车出入市中心。在市中心，-{zh:存车费;zh-hans:存车费;zh-hant:停車費}-是十分高昂的，而且市区一些道路禁止机动车驶入，而另一些则是[单行道](../Page/单行道.md "wikilink")\[49\]
此外，市政府透过 *Autodelen* 与 *Meerijden.nu*
两个网站赞助市民自发的[汽车共享及](../Page/汽车共享.md "wikilink")[汽车共乘](../Page/汽车共乘.md "wikilink")\[50\]。

阿姆斯特丹的公交系统主要包括[公共汽车和](../Page/公共汽车.md "wikilink")[有轨电车](../Page/有轨电车.md "wikilink")。目前有15条[阿姆斯特丹有軌電車线路正在运营](../Page/阿姆斯特丹有軌電車.md "wikilink")\[51\]，一些新的线路也在规划中；[阿姆斯特丹地鐵有](../Page/阿姆斯特丹地鐵.md "wikilink")4条地铁线路正在营运，而第5条[地铁线路也正在建设中](../Page/地铁.md "wikilink")；从IJ湾到北阿姆斯特丹有三条搭载行人和自行车的免费[渡轮](../Page/渡轮.md "wikilink")，从城区往东、西部的海岸有两条收费渡轮通行。同时，运河上还有水上[的士](../Page/的士.md "wikilink")、水上巴士以及游轮搭载市民去他们想去的地方。

1932年，阿姆斯特丹成为了荷兰公路网的枢纽\[52\]。由于第二次世界大战的爆发，只有A1、A2和A4按照原先的计划完工。1970年，为了保护“绿色之心”（Groene
Hart），A3高速路被取消。通往北赞丹的A8公路和A10在1968年到1974年相继开通\[53\]。A10环路将城市周边与荷兰国家高速公路网连接起来。人们可以经由连接A10的18条高速路去荷兰的其他地方。自行车可以在其中的大部分道路上通行。除去A1、A2、A4、A8之外，A6和A7也是通往阿姆斯特丹的主要快速路。

阿姆斯特丹拥有9座火车站\[54\]。5座用于长途客运，4座用作本地客运。“欧洲线”（Eurolines）长途汽车也从阿姆斯特丹发往欧洲各地。

[阿姆斯特丹中央車站也是一座国际中转站](../Page/阿姆斯特丹中央車站.md "wikilink")。这里有通往[奥地利](../Page/奥地利.md "wikilink")、[白俄罗斯](../Page/白俄罗斯.md "wikilink")、[比利时](../Page/比利时.md "wikilink")、[捷克](../Page/捷克.md "wikilink")、[丹麦](../Page/丹麦.md "wikilink")、[法国](../Page/法国.md "wikilink")、[德国](../Page/德国.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[波兰](../Page/波兰.md "wikilink")、[俄罗斯和](../Page/俄罗斯.md "wikilink")[瑞士的常规线路](../Page/瑞士.md "wikilink")。[大力士高速列车直接连接阿姆斯特丹](../Page/大力士高速列车.md "wikilink")－史基普国际机场－[海牙](../Page/海牙.md "wikilink")－[鹿特丹](../Page/鹿特丹.md "wikilink")－[安特卫普](../Page/安特卫普.md "wikilink")－[布鲁塞尔](../Page/布鲁塞尔.md "wikilink")－[巴黎](../Page/巴黎.md "wikilink")。方便快捷的高速火车适合于想去比利时或巴黎旅行的乘客。阿姆斯特丹－布鲁塞尔的车行时间为2小时44分钟，阿姆斯特丹－巴黎的车行时间为4小时13分，每日从阿姆斯特丹发车多达8次。

从中央车站到[史基浦机场只需要](../Page/史基浦机场.md "wikilink")20分钟时间。按乘客流量计算，它是荷兰第一大、欧洲第六大和世界第十二大机场。每年大概有4,600万游客经过这里。2006年，史基浦机场是世界上第三繁忙的国际空港\[55\]\[56\]。

## 文化

荷兰的文化中心：[管弦乐团](../Page/管弦乐团.md "wikilink")、[芭蕾](../Page/芭蕾.md "wikilink")、[剧场](../Page/剧场.md "wikilink")、博物馆、[美术馆和两所](../Page/美术馆.md "wikilink")[大学](../Page/大学.md "wikilink")。其中[荷兰国家博物馆和](../Page/荷兰国家博物馆.md "wikilink")[凡·高博物馆是这里最著名的博物馆](../Page/凡·高博物馆.md "wikilink")。[阿姆斯特丹音乐厅及其管弦乐团都享有盛名](../Page/音樂廳_\(阿姆斯特丹\).md "wikilink")。
[RijksmuseumAmsterdamMuseumplein.jpg](https://zh.wikipedia.org/wiki/File:RijksmuseumAmsterdamMuseumplein.jpg "fig:RijksmuseumAmsterdamMuseumplein.jpg")\]\]
16世纪晚期，举行了协会间的[诗歌与](../Page/诗歌.md "wikilink")[戏剧比赛](../Page/戏剧.md "wikilink")。1638年，阿姆斯特丹建立了第一座[剧院](../Page/剧院.md "wikilink")。而1642年，[芭蕾舞第一次走进了这里](../Page/芭蕾舞.md "wikilink")。18世纪，法国剧院又在这里流行。[意大利和](../Page/意大利.md "wikilink")[法国](../Page/法国.md "wikilink")[歌剧在](../Page/歌剧.md "wikilink")1677年传入这里，而后来[德国歌剧又在](../Page/德国.md "wikilink")18世纪登陆该市。19世纪，通俗文化，尤其是[杂技表演和](../Page/杂技.md "wikilink")[音乐又流行开来](../Page/音乐.md "wikilink")。1812年，[德里希·尼古拉斯·温克尔](../Page/德里希·尼古拉斯·温克尔.md "wikilink")（）在这里发明了欧洲古典乐十分重要的[乐器](../Page/乐器.md "wikilink")—[节拍器](../Page/节拍器.md "wikilink")。19世纪末，荷兰国立博物馆建成。1888年，荷兰[王家音乐厅乐团组建](../Page/王家音乐厅乐团.md "wikilink")。随着20世纪的到来，[电影](../Page/电影.md "wikilink")、[广播和](../Page/广播.md "wikilink")[电视也逐渐走入该城](../Page/电视.md "wikilink")。[SBS
6电视台总部就设在这里](../Page/SBS_6.md "wikilink")\[57\]。

### 博物馆

[博物馆广场汇集了大多数主要的](../Page/博物馆广场_\(阿姆斯特丹\).md "wikilink")[博物馆](../Page/博物馆.md "wikilink")。它坐落於城市南方，是该城市最大的广场。它是在19世纪末期为举办[世界博览会而兴建的](../Page/世界博览会.md "wikilink")。广场北部是巨大的荷兰国家博物馆。博物馆前是一个長的長方形人造池塘。冬天的时候，这里可以作为一个溜冰场\[58\]。广场西部是[梵谷博物馆及](../Page/梵谷博物馆.md "wikilink")[市立博物馆](../Page/阿姆斯特丹市立博物館.md "wikilink")。广场南部是[范·巴尔勒大街](../Page/范·巴尔勒大街.md "wikilink")（），这也是通向广场的主要道路。阿姆斯特丹音乐厅就坐落于此。广场几乎全部被草坪覆盖，只有北部的小路是石子铺成的。1999年，广场完成了改造，成了如今的样子。夏天，这个广场会承办一些露天音乐会。由于有很多市民对广场现在的外观不甚满意，所以该广场计划于2008年再度改造\[59\]。

[The_Nightwatch_by_Rembrandt.jpg](https://zh.wikipedia.org/wiki/File:The_Nightwatch_by_Rembrandt.jpg "fig:The_Nightwatch_by_Rembrandt.jpg")'''
[-{zh-hans:伦勃朗;zh-hant:林布蘭;zh-tw:林布蘭;zh-hk:林布蘭;zh-mo:林布蘭;}-](../Page/伦勃朗.md "wikilink")\]\]
荷兰国家博物馆陈列了大量[荷兰艺术作品](../Page/荷兰艺术.md "wikilink")\[60\]。它于1885年开馆，现在馆藏大约100万件展品。\[61\]
画家[-{zh-hans:伦勃朗;zh-hant:林布蘭;zh-tw:林布蘭;zh-hk:林布蘭;zh-mo:林布蘭;}-是和这座城市联系最紧密的艺术家之一](../Page/伦勃朗.md "wikilink")，他和他的学生的作品大多被收藏在国家博物馆。他的名作《[夜巡](../Page/夜巡.md "wikilink")》是该馆的镇馆之宝之一。该馆还收藏了诸如[扬·弗美尔](../Page/扬·弗美尔.md "wikilink")、[弗兰斯·哈尔斯](../Page/弗兰斯·哈尔斯.md "wikilink")、[费迪南德·博尔](../Page/费迪南德·博尔.md "wikilink")（）以及[保卢斯·波特](../Page/保卢斯·波特.md "wikilink")（）的作品。该馆还收藏了许多装饰艺术作品。目前，博物馆正在扩建，只有一侧展馆对外开放，新馆将於2012年至2013年竣工开放\[62\]。

[梵高曾经短暂居住在阿姆斯特丹](../Page/梵高.md "wikilink")。这座博物馆是该片区域中少数几个现代建筑之一，是由[赫里特·里特费尔德设计的](../Page/赫里特·里特费尔德.md "wikilink")。所有的展品都由博物馆永久收藏。1999年，博物馆建造了一座名叫“表现之翼”（Wing
of
Performance）新楼，这座大楼是由[日本设计师](../Page/日本.md "wikilink")[黑川纪章所设计的](../Page/黑川纪章.md "wikilink")，主要用来展示那些临时展品。像《[吃马铃薯的人](../Page/吃马铃薯的人.md "wikilink")》等一些著名的画作现在都藏于该馆。而梵高博物馆也是全市参观者最多的博物馆\[63\]\[64\]\[65\]。

梵高博物馆旁边是市立博物馆。这座博物馆与广场是同一时期的产物，目前藏有[皮特·蒙德里安和](../Page/皮特·蒙德里安.md "wikilink")[卡雷尔·阿佩尔等艺术家的作品](../Page/卡雷尔·阿佩尔.md "wikilink")。与国家博物馆一样，市立现代艺术博物馆也正经历大规模的扩建。博物馆大厅将从广场一直延伸到保卢斯·波特街（Paulus
Potterstraat）。2009年，博物馆将重新开放，目前，该馆临时寄居在中央火车站附近的一所旧[邮局裡](../Page/邮局.md "wikilink")\[66\]。

博物馆广场以外还有其他一些博物馆，有些规模很小，比如[安妮·弗兰克之家](../Page/安妮·弗兰克之家.md "wikilink")，有些则规模很大，比如[阿姆斯特丹博物馆](../Page/阿姆斯特丹博物馆.md "wikilink")。这些博物馆几乎都位于市中心附近。

### 教育

阿姆斯特丹有两所大学：[阿姆斯特丹大学与](../Page/阿姆斯特丹大学.md "wikilink")[阿姆斯特丹自由大学](../Page/阿姆斯特丹自由大学.md "wikilink")。其他的高等学府还包括：以及其他一些高等专业学校。阿姆斯特丹[社会历史国际学院是世界上最大的关于](../Page/社会历史国际学院.md "wikilink")[社会史的记录和研究机构](../Page/社会史.md "wikilink")，尤其是历史上的[劳工运动](../Page/劳工运动.md "wikilink")。始建於1638年的[阿姆斯特丹植物园則是世界上最古老的](../Page/阿姆斯特丹植物园.md "wikilink")[植物园之一](../Page/植物园.md "wikilink")，其中陈列了许多稀有的植物，包括整个[拉丁美洲的](../Page/拉丁美洲.md "wikilink")[咖啡标本](../Page/咖啡.md "wikilink")\[67\]。

阿姆斯特丹拥有完善的初等教育系统。其中一些学校的教育方式比较独特，比如[蒙台梭利学校](../Page/蒙台梭利.md "wikilink")。也有一些是[教会学校](../Page/教会学校.md "wikilink")，原先主要是[天主教和](../Page/天主教.md "wikilink")[新教建立的](../Page/新教.md "wikilink")，后来随着[穆斯林的迁入](../Page/穆斯林.md "wikilink")，[伊斯兰教学校也开始出现](../Page/伊斯兰教学校.md "wikilink")。在城市南郊也有一些[犹太教学校](../Page/犹太教学校.md "wikilink")。

中等教育也比较不错。阿姆斯特丹有三类不同的文法学校（荷兰语：gymnasia），其中一部分会教授[古典希腊语和](../Page/古典希腊语.md "wikilink")[拉丁语](../Page/拉丁语.md "wikilink")。虽然直到现在都有一语法学校過時且流於[精英主义](../Page/精英主义.md "wikilink")，但最近仍有一批语法学校獲得复苏。

### 演出

#### 音乐

[喜力音乐厅位于](../Page/喜力音乐厅.md "wikilink")[阿姆斯特丹球场附近](../Page/阿姆斯特丹球场.md "wikilink")，是一个大型[流行音乐演出场所](../Page/流行音乐.md "wikilink")。许多国际知名[艺人曾在此演出](../Page/艺人.md "wikilink")。另外两个经常举行演出的地方名叫[帕拉迪索和](../Page/帕拉迪索.md "wikilink")[银河](../Page/銀河_\(阿姆斯特丹\).md "wikilink")（），它们都位于[莱顿广场附近](../Page/莱顿广场.md "wikilink")，主要上演[独立摇滚](../Page/独立摇滚.md "wikilink")、[嘻哈和](../Page/嘻哈.md "wikilink")[节奏布鲁斯等风格的音乐](../Page/节奏布鲁斯.md "wikilink")。

[Concert18.jpg](https://zh.wikipedia.org/wiki/File:Concert18.jpg "fig:Concert18.jpg")
阿姆斯特丹的[王家音乐厅乐团在世界上享有盛誉](../Page/王家音乐厅乐团.md "wikilink")。他们的主演奏厅就是阿姆斯特丹音乐厅。评论家认为这座音乐厅是世界上声音效果最出色的音乐厅之一。它由三座音乐厅组成。每年在这里大概上演800场[音乐会](../Page/音乐会.md "wikilink")，大约有850,000名观众在此聆听音乐。

[市政歌剧院是市政厅与歌剧院两座建筑的合称](../Page/市政歌剧院.md "wikilink")，位于早期[犹太人聚居的](../Page/犹太人.md "wikilink")[滑铁卢广场和阿姆斯特尔河附近](../Page/滑铁卢广场_\(阿姆斯特丹\).md "wikilink")。这座歌剧院于1986年正式对外开放。

#### 戏剧

阿姆斯特丹最主要的剧院是位于莱兹广场的。它建成于1984年，大多数剧目在大厅（Great
Hall）上演，其中包含很多种类的戏剧。这座剧院目前也在修缮之中。

荷兰在余兴节目方面有悠久的历史，最初的历史可追溯到1930年代。

1993年，随着[喜剧火车](../Page/喜剧火车.md "wikilink")（）和两个团体的建立，阿姆斯特丹的新派喜剧也随之诞生。喜剧火车是一个系列喜剧，该剧最初是在喜剧咖啡馆（Comedy
Cafe）上演，后来搬到到一个叫做Toomler的地方继续演出。许多现在在荷兰大名鼎鼎的喜剧剧目都和他们有一定渊源。

爆发芝加哥是与科特·莱兹大街（Korte Leidsedwarsstraat）上的一个剧院一同建立的，而四年后那里已经成为了糖厂（Sugar
Factory）俱乐部。1998年，他们来到莱兹广场剧院（Leidseplein Theater）继续演出。

喜剧咖啡馆和喜剧剧院也是不错的观看喜剧的地方。

#### 夜生活

阿姆斯特丹也因为她充满活力以及与众不同的[夜生活而著名](../Page/夜生活.md "wikilink")。最主要的两个夜生活广场是[莱顿广场和](../Page/莱顿广场.md "wikilink")[伦勃朗广场](../Page/伦勃朗广场.md "wikilink")。

阿姆斯特丹有很多[咖啡馆](../Page/咖啡馆.md "wikilink")。他们的规模有大有小。若隐若现的灯光和烛光使得传统的棕色咖啡馆（brown
cafe）给人一种古色古香的感觉。夏天的时候，很多咖啡店会摆出[大排档](../Page/大排档.md "wikilink")。这时候最常见的景象是一广场的大排档以及享用[啤酒或](../Page/啤酒.md "wikilink")[葡萄酒的人们](../Page/葡萄酒.md "wikilink")。

阿姆斯特丹多元的文化使这里拥有各种各样的[餐馆](../Page/餐馆.md "wikilink")。从小饭馆到豪华餐厅都可以找到。荷兰许多最高档的餐厅也汇集于此。

阿姆斯特丹也有许多[迪斯科舞厅](../Page/迪斯科.md "wikilink")。大部分舞厅都坐落在伦勃朗广场和莱兹广场附近。帕拉迪索、银河以及糖厂这样的大型夜店也会有迪斯科舞场。但舞厅的典型代表是“逃离”俱乐部（Escape）和“家”（Club
Home），除此之外“巴拿马”（Panama）和“力量地带”（The Powerzone）也较有名。

[雷古里尔大街](../Page/雷古里尔大街.md "wikilink")（）是该城市主要的[同性恋大街](../Page/同性恋.md "wikilink")。

[Koninginnedag2007.jpg](https://zh.wikipedia.org/wiki/File:Koninginnedag2007.jpg "fig:Koninginnedag2007.jpg")盛况\]\]

### 节日

阿姆斯特丹是座“节日之都”。2007年在该城市总共举行过约140次节日庆祝活动\[68\]。

大多数庆祝活动在[女王日](../Page/女王日.md "wikilink")（现在为[国王日](../Page/国王日.md "wikilink")）、[同性恋自豪日和](../Page/同性恋自豪日.md "wikilink")期间举行。2013年以前，每年的4月30日，女王日那天，大约有十万人左右走上街头庆祝。大多数城区都会开办自由市场，并举行各种演唱会，到处都是人头攒动的景象。2013年荷兰新国王[威廉-亚历山大·克劳斯·乔治·费迪南德继位](../Page/威廉-亚历山大·克劳斯·乔治·费迪南德.md "wikilink")，改成每年4月27日的国王日庆祝。

8月的第一个[周末](../Page/周末.md "wikilink")，[同性恋自豪日大游行期间](../Page/同性恋自豪日.md "wikilink")，在城市运河上都会有长长的船队载着狂欢的人群到处游乐，狂欢会一直持续整个[周末](../Page/周末.md "wikilink")\[69\]。文化季一般在8月底举行，为期三天，会有各种诗人和音乐家的表演\[70\]。

### 體育

| 俱樂部                                                          | 所屬聯賽                                       | 體育項目                               | 主場                                       | 成立年份  | 榮譽                                                                    |
| ------------------------------------------------------------ | ------------------------------------------ | ---------------------------------- | ---------------------------------------- | ----- | --------------------------------------------------------------------- |
| [阿贾克斯](../Page/阿贾克斯.md "wikilink")                           | [荷蘭甲組足球聯賽](../Page/荷蘭甲組足球聯賽.md "wikilink") | [足球](../Page/足球.md "wikilink")     | [阿姆斯特丹球場](../Page/阿姆斯特丹球場.md "wikilink") | 1900年 | 4屆[欧洲冠军联赛冠軍](../Page/欧洲冠军联赛.md "wikilink")                            |
| [阿姆斯特丹上將](../Page/阿姆斯特丹上將.md "wikilink")（Amsterdam Admirals） | [歐洲美式足球聯盟](../Page/歐洲美式足球聯盟.md "wikilink") | [美式足球](../Page/美式足球.md "wikilink") | [阿姆斯特丹球場](../Page/阿姆斯特丹球場.md "wikilink") | 1995年 | 1屆[-{zh-hans:世界碗;zh-hk:世界碗;zh-tw:世界盃;}-冠軍](../Page/世界碗.md "wikilink") |

阿姆斯特丹曾經於1928年舉辦過[第九屆夏季奧林匹克運動會](../Page/1928年夏季奧林匹克運動會.md "wikilink")，是目前唯一一個荷蘭城市舉行過夏季奧林匹克運動會，該屆奧運會亦是首次在奧運會期間點燃奧運聖火\[71\]。

阿姆斯特丹是[荷兰足球甲级联赛球隊](../Page/荷兰足球甲级联赛.md "wikilink")[阿贾克斯的所在地](../Page/阿積士.md "wikilink")，主場位於[阿姆斯特丹球場](../Page/阿姆斯特丹球場.md "wikilink")。阿積士被譽為荷蘭足球歷史的代表，不單曾經奪得過歐洲三大盃賽（[歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")、[歐洲足協盃和](../Page/歐洲足協盃.md "wikilink")[歐洲盃賽冠軍盃](../Page/歐洲盃賽冠軍盃.md "wikilink")），並且曾經兩度成為[洲際杯](../Page/洲際杯足球賽.md "wikilink")[冠军](../Page/冠军.md "wikilink")。阿積士出產的多名優秀荷蘭球員，往往成為[荷蘭國家足球隊的核心球員](../Page/荷蘭國家足球隊.md "wikilink")，包括[約翰·克魯伊夫](../Page/約翰·克魯伊夫.md "wikilink")、[范·巴斯滕](../Page/范·巴斯滕.md "wikilink")、[-{zh-hk:柏金;zh-hans:丹尼斯·博格坎普;zh-tw:博格坎普;}-](../Page/丹尼斯·博格坎普.md "wikilink")、[-{zh-hk:古華特;zh-hans:帕特里克·克鲁伊维特;zh-tw:派屈克·克魯伊維特;}-等](../Page/帕特里克·克鲁伊维特.md "wikilink")\[72\]。

的主场是[夏普·艾登](../Page/夏普·艾登.md "wikilink")（）[冰球场](../Page/冰球场.md "wikilink")，他们代表这座城市参加荷兰冰球联赛\[73\]。

阿姆斯特丹海盗队代表阿姆斯特丹参加[荷兰棒球联赛](../Page/荷兰棒球联赛.md "wikilink")。而这座城市拥有三支[曲棍球队](../Page/曲棍球.md "wikilink")，以及一只参加[荷兰篮球超级联赛](../Page/荷兰篮球超级联赛.md "wikilink")（Dutch
premier division）的[阿姆斯特丹队](../Page/阿姆斯特丹队.md "wikilink")（）\[74\]。

## 杂项

### 红灯区

[RedLightDistrictAmsterdamTheNetherlands.jpg](https://zh.wikipedia.org/wiki/File:RedLightDistrictAmsterdamTheNetherlands.jpg "fig:RedLightDistrictAmsterdamTheNetherlands.jpg")\]\]
**德瓦伦**（De Wallen，亦称**Walletjes**或**Rosse
Buurt**），是一个[合法卖淫的指定区域](../Page/管制卖淫.md "wikilink")，也是阿姆斯特丹最大和最著名的[红灯区](../Page/红灯区.md "wikilink")。它由道路与小巷的网络组成，在这些街巷内是数百座小的单间公寓，由女性[性工作者租赁](../Page/性工作者.md "wikilink")，她们在特以红灯照亮的窗户或玻璃门后面提供服务。该区域也有许多[性用品商店](../Page/性用品商店.md "wikilink")，[性剧院](../Page/性产业.md "wikilink")，[西洋景](../Page/西洋景.md "wikilink")（Peep
show），一家[性博物馆](../Page/性博物馆.md "wikilink")，一家[大麻博物馆](../Page/大麻.md "wikilink")，以及许多提供各种大麻制品的[大麻咖啡馆](../Page/大麻咖啡馆.md "wikilink")。虽然游客中有26%会来此区“看看”，但妓院数量仍然日趋下降，以至于[商会](../Page/商会.md "wikilink")（Chamber
of commerce）发出了警告。\[75\]

## 友好城市

  - [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg")
    [中華人民共和國](../Page/中華人民共和國.md "wikilink")
    **[北京](../Page/北京.md "wikilink")**
  - [Flag_of_Canada.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Canada.svg "fig:Flag_of_Canada.svg")
    [加拿大](../Page/加拿大.md "wikilink")
    **[多倫多](../Page/多倫多.md "wikilink")**
  - [Flag_of_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_United_Kingdom.svg "fig:Flag_of_United_Kingdom.svg")
    [英國](../Page/英國.md "wikilink")
    **[曼徹斯特](../Page/曼徹斯特.md "wikilink")**

## 資料來源

## 外部链接

  - [地理坐标](../Page/地理坐标.md "wikilink")：

<!-- end list -->

  - 政府

<!-- end list -->

  - [Amsterdam.nl](http://www.amsterdam.nl/) - 政府官方網站
  - [I amsterdam](http://www.iamsterdam.com/) - 國際旅客的入口

{{-}}

[Category:北荷兰省城市](../Category/北荷兰省城市.md "wikilink")
[阿姆斯特丹](../Category/阿姆斯特丹.md "wikilink")
[Category:欧洲首都](../Category/欧洲首都.md "wikilink")
[Category:夏季奥林匹克运动会主办城市](../Category/夏季奥林匹克运动会主办城市.md "wikilink")

1.  [Encyclopædia Britannica Eleventh
    Edition](../Page/Encyclopædia_Britannica_Eleventh_Edition.md "wikilink"),
    Vol 1, p896-898.

2.  [1](http://www.cambridge.org/catalogue/catalogue.asp?isbn=9780521845359&ss=exc)
    Capitals of Capital -A History of International Financial Centres -
    1780–2005, Youssef Cassis, ISBN 978-0-521-84535-9

3.  在1985年的[雅典和](../Page/雅典.md "wikilink")1986年的[佛罗伦萨之后](../Page/佛罗伦萨.md "wikilink")，阿姆斯特丹于1986年当选[欧洲文化之都](../Page/欧洲文化之都.md "wikilink")，并巩固了她在荷兰乃至欧洲文化界的地位。点击
    [这里](http://ec.europa.eu/culture/our-programmes-and-actions/doc443_en.htm)
     可以查看近年欧洲文化首都的列表。

4.

5.

6.  Berns, Jan; Daan, Jo (1993年). Hij zeit wat: de Amsterdamse
    volkstaal. The Hague: BZZTôH. p. 91. ISBN 90-6291-756-9.

7.

8.

9.

10.

11. 典型的例子：1633年，经过尝试和考察，伽利略选择阿姆斯特丹的一家出版社来出版他最好的作品之一《[关于两门新科学的对话](../Page/关于两门新科学的对话.md "wikilink")》。参见[韦德·罗兰](../Page/韦德·罗兰.md "wikilink")（2003），《伽利略的错误：关于伽利略与教会斗争的新观点》，纽约：商务出版社，ISBN
    1-55970-684-8，p. 260

12.

13.

14.

15.

16.

17.

18.

19.

20.
21.

22.

23.

24.

25.

26.

27.

28.

29.

30. 黑特·马柯. (1995). Een kleine geschiedenis van Amsterdam. 阿姆斯特丹/安特卫普:
    Uitgeverij Atlas. ISBN 90-450-1232-4.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57.

58.

59.

60.

61.

62.

63.

64.

65.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.