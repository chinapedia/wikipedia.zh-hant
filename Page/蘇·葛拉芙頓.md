[SueGrafton.jpg](https://zh.wikipedia.org/wiki/File:SueGrafton.jpg "fig:SueGrafton.jpg")的葛拉芙頓\]\]
**蘇·葛拉芙頓**
（，），生於[美國](../Page/美國.md "wikilink")[肯塔基州](../Page/肯塔基州.md "wikilink")[路易維爾](../Page/路易維爾.md "wikilink")，是美國當代著名的女性[偵探小說](../Page/偵探小說.md "wikilink")[作家](../Page/作家.md "wikilink")。

葛拉芙頓最著名的作品是女私家偵探[金絲·梅芳](../Page/金絲·梅芳.md "wikilink")（）的探案系列，作品標題以[英文字母做為開頭](../Page/英文字母.md "wikilink")。雖作品相當暢銷、有26種語言譯本\[1\]，但她始終拒絕自己的作品被改編為電影或是電視劇\[2\]。她女兒也表示葛拉芙頓堅決反對他人代筆。

2017年12月28日因癌症於加州聖巴巴拉離世。

## 作品

### 金絲·梅芳系列

1.  《[不在場證明](../Page/不在場證明.md "wikilink")》（）1982年

2.  《[瞞天過海](../Page/瞞天過海.md "wikilink")》（）1985年

3.  《[死亡陷阱](../Page/死亡陷阱.md "wikilink")》（）1986年

4.  《[兇手的意外](../Page/兇手的意外.md "wikilink")》（）1987年

5.  《[鐵證如山](../Page/鐵證如山.md "wikilink")》（）1988年

6.  《[亡命之徒](../Page/亡命之徒.md "wikilink")》（）1989年

7.  《[魅影殺手](../Page/魅影殺手.md "wikilink")》（）1990年

8.  《[異色狙殺](../Page/異色狙殺.md "wikilink")》（）1991年

9.  《[孤注一擲 (1992年小說)](../Page/孤注一擲_\(1992年小說\).md "wikilink")》（）1992年

10. 《[變調的審判](../Page/變調的審判.md "wikilink")》（）1993年

11. 《[情色殺機](../Page/情色殺機.md "wikilink")》（）1994年

12. 《[法外正義](../Page/法外正義.md "wikilink")》（）1995年

13. 《[金色預謀](../Page/金色預謀.md "wikilink")》（）1996年

14. 《[殺意的圈套](../Page/殺意的圈套.md "wikilink")》（）1998年

15. 《[逍遙法外](../Page/逍遙法外.md "wikilink")》（）1999年

16. 《[危机四伏](../Page/危机四伏.md "wikilink")》（）2001年

17. 《[猎物追击](../Page/猎物追击.md "wikilink")》（）2003年

18. 《[让子弹飞](../Page/让子弹飞.md "wikilink")》（）2004年

19. 2005年

20. 《[非法入侵](../Page/非法入侵.md "wikilink")》（）2007年

21. 2009年

22. 2011年

23. 2013年

24. 2015年

25. 2017年

## 參考資料

## 外部連結

  - [Sue Grafton](http://www.suegrafton.com/) 官方網站

  -
[G](../Category/推理小說作家.md "wikilink")
[Category:美國女性作家](../Category/美國女性作家.md "wikilink")
[Category:路易斯维尔大学校友](../Category/路易斯维尔大学校友.md "wikilink")
[Category:安东尼奖获得者](../Category/安东尼奖获得者.md "wikilink")

1.
2.  [美國犯罪小說家蘇葛拉芙頓癌逝
    享壽77](http://www.cna.com.tw/news/aopl/201712300030-1.aspx)