[ams-ix.k.root-servers.net.jpg](https://zh.wikipedia.org/wiki/File:ams-ix.k.root-servers.net.jpg "fig:ams-ix.k.root-servers.net.jpg")
**根域名服务器**（）是[互联网](../Page/互联网.md "wikilink")[域名解析系统](../Page/域名解析系统.md "wikilink")（DNS）中最高级别的[域名服务器](../Page/域名服务器.md "wikilink")，负责返回顶级域的权威域名服务器地址。它们是互联网基础设施中的重要部分，因为所有域名解析操作均离不开它们。由于DNS和某些协议（未分片的[用户数据报协议](../Page/用户数据报协议.md "wikilink")（UDP）数据包在[IPv4内的最大有效大小为](../Page/IPv4.md "wikilink")512[字节](../Page/字节.md "wikilink")）的共同限制，根域名服务器地址的数量被限制为13个。幸运的是，采用[任播技术架设镜像服务器可解决该问题](../Page/任播.md "wikilink")，并使得实际运行的根域名服务器数量大大增加。截至2017年11月，全球共有800台根域名服务器在运行。

{{-}}

## 管理机构及设置地点

全球13组根域名伺服器以英文字母A到M依序命名，網域名稱格式為「`字母.root-servers.net`」。全部已以[任播技術在全球多個地點設立鏡像站](../Page/任播.md "wikilink")。\[1\]

<table>
<thead>
<tr class="header">
<th><p>字母</p></th>
<th><p><a href="../Page/IPv4.md" title="wikilink">IPv4位址</a></p></th>
<th><p><a href="../Page/IPv6.md" title="wikilink">IPv6位址</a></p></th>
<th><p><a href="../Page/自治系統_(網際網路).md" title="wikilink">自治系統編號</a>[2]</p></th>
<th><p>曾用名</p></th>
<th><p>運營單位</p></th>
<th><p>設置地點<br />
#數量（全球/地區）[3]</p></th>
<th><p>軟體</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="http://a.root-servers.org/">A</a></p></td>
<td><p>198.41.0.4</p></td>
<td><p>2001:503:ba3e::2:30</p></td>
<td><p>AS19836,[4][5] AS36619, AS36620, AS36622, AS36625, AS36631, AS64820[6][7]</p></td>
<td><p>ns.internic.net</p></td>
<td><p><a href="../Page/威瑞信.md" title="wikilink">威瑞信</a></p></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
5/0</p></td>
<td><p>、<a href="../Page/威瑞信.md" title="wikilink">威瑞信ATLAS</a></p></td>
</tr>
<tr class="even">
<td><p><a href="http://b.root-servers.org/">B</a></p></td>
<td><p>199.9.14.201[8][9] [10]</p></td>
<td><p>2001:500:200::b[11]</p></td>
<td><p>AS394353[12]</p></td>
<td><p>ns1.isi.edu</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/南加州大學.md" title="wikilink">南加州大學</a><a href="../Page/資訊科學研究所.md" title="wikilink">資訊科學研究所</a></p></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
2/0</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="http://c.root-servers.org/">C</a></p></td>
<td><p>192.33.4.12</p></td>
<td><p>2001:500:2::c</p></td>
<td><p>AS2149[13][14]</p></td>
<td><p>c.psi.net</p></td>
<td><p><a href="../Page/Cogent通信.md" title="wikilink">Cogent通信</a></p></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
8/0</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a></p></td>
</tr>
<tr class="even">
<td><p><a href="http://d.root-servers.org/">D</a></p></td>
<td><p>199.7.91.13[15][16]</p></td>
<td><p>2001:500:2d::d</p></td>
<td><p>AS27[17][18]</p></td>
<td><p>terp.umd.edu</p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/馬里蘭大學學院市分校.md" title="wikilink">馬里蘭大學學院市分校</a></p></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
50/67</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://e.root-servers.org/">E</a></p></td>
<td><p>192.203.230.10</p></td>
<td><p>2001:500:a8::e</p></td>
<td><p>AS21556[19][20]</p></td>
<td><p>ns.nasa.gov</p></td>
<td></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
95/96</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a>、</p></td>
</tr>
<tr class="even">
<td><p><a href="http://f.root-servers.org/">F</a></p></td>
<td><p>192.5.5.241</p></td>
<td><p>2001:500:2f::f</p></td>
<td><p>AS3557,[21][22] AS1280, AS30132[23]</p></td>
<td><p>ns.isc.org</p></td>
<td></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
57/0</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a> [24]</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://disa.mil/G-Root">G</a>[25]</p></td>
<td><p>192.112.36.4[26]</p></td>
<td><p>2001:500:12::d0d[27]</p></td>
<td><p>AS5927[28][29]</p></td>
<td><p>ns.nic.ddn.mil</p></td>
<td></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
6/0</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a></p></td>
</tr>
<tr class="even">
<td><p><a href="http://h.root-servers.org/">H</a></p></td>
<td><p>198.97.190.53[30][31]</p></td>
<td><p>2001:500:1::53[32][33]</p></td>
<td><p>AS1508[34][35][36]</p></td>
<td><p>aos.arl.army.mil</p></td>
<td></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/馬里蘭州.md" title="wikilink">馬里蘭州</a><a href="../Page/阿伯丁試驗場.md" title="wikilink">阿伯丁試驗場</a><br />
以及<a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a><a href="../Page/聖地牙哥_(加利福尼亞州).md" title="wikilink">聖地牙哥</a><br />
2/0</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://web.archive.org/web/20101123140731/http://i.root-servers.org/">I</a></p></td>
<td><p>192.36.148.17</p></td>
<td><p>2001:7fe::53</p></td>
<td><p>AS29216[37][38]</p></td>
<td><p>nic.nordu.net</p></td>
<td></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
58/0</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a></p></td>
</tr>
<tr class="even">
<td><p><a href="http://j.root-servers.org/">J</a></p></td>
<td><p>192.58.128.30[39]</p></td>
<td><p>2001:503:c27::2:30</p></td>
<td><p>AS26415,[40][41] AS36626, AS36628, AS36632[42]</p></td>
<td></td>
<td><p><a href="../Page/威瑞信.md" title="wikilink">威瑞信</a></p></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
61/13</p></td>
<td><p>、<a href="../Page/威瑞信.md" title="wikilink">威瑞信ATLAS</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="http://k.root-servers.org/">K</a></p></td>
<td><p>193.0.14.129</p></td>
<td><p>2001:7fd::1</p></td>
<td><p>AS25152[43][44][45]</p></td>
<td></td>
<td></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
5/23</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a>、、[46]</p></td>
</tr>
<tr class="even">
<td><p><a href="http://l.root-servers.org/">L</a></p></td>
<td><p>199.7.83.42[47][48]</p></td>
<td><p>2001:500:9f::42[49][50]</p></td>
<td><p>AS20144[51][52][53]</p></td>
<td></td>
<td><p><a href="../Page/ICANN.md" title="wikilink">ICANN</a></p></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
161/0</p></td>
<td><p>、[54]</p></td>
</tr>
<tr class="odd">
<td><p><a href="http://m.root-servers.org/">M</a></p></td>
<td><p>202.12.27.33</p></td>
<td><p>2001:dc3::35</p></td>
<td><p>AS7500[55][56][57]</p></td>
<td></td>
<td></td>
<td><p>以<a href="../Page/任播.md" title="wikilink">任播技術设置於多處</a><br />
6/1</p></td>
<td><p><a href="../Page/BIND.md" title="wikilink">BIND</a></p></td>
</tr>
</tbody>
</table>

[thumb](../Page/file:Root_Nameserver.svg.md "wikilink")

## 根域文件

所有根域名服务器都是以同一份根域文件（Root Zone
file，文件名为root.zone）返回顶级域名权威服务器（包括[通用顶级域和](../Page/通用顶级域.md "wikilink")[国家顶级域](../Page/国家顶级域.md "wikilink")），文件只有2[MB](../Page/百万字节.md "wikilink")\[58\]大小。截至2017年10月9日，一共记录了1542个顶级域。对于没被收录的顶级域，是没法通过根域名服务器查出相应的权威服务器。而其他递归DNS服务器则只需要配置Root
Hits文件，只包含根域名服务器的地址。

## 事件

  - 2010年3月16日前，中国大陆有F、I、J这3个根域DNS镜像\[59\]，但因为多次發生[DNS污染而影響外国网络](../Page/DNS污染.md "wikilink")，中国大陆境内的I根域镜像曾被撤销路由通告\[60\]\[61\]\[62\]。现今，中国大陆境内共有F、I、J、L这4个根域的6台DNS镜像（L有三台镜像）在提供服务\[63\]。

<!-- end list -->

  - 2014年1月21日下午15时左右，[中国大陆](../Page/中国大陆.md "wikilink")[DNS被污染](../Page/DNS.md "wikilink")，导致众多[顶级域名被错误的解析到](../Page/顶级域名.md "wikilink")「65.49.2.178」。[百度](../Page/百度.md "wikilink")、[新浪](../Page/新浪.md "wikilink")、[淘宝等众多中國大陸网站无法访问](../Page/淘宝.md "wikilink")。\[64\]而根据部分人员分析，认为可能是[防火长城设置错误导致](../Page/防火长城.md "wikilink")\[65\]。

## 参见

  - [任播](../Page/任播.md "wikilink")
  - [DNSSEC](../Page/DNSSEC.md "wikilink")
  - [DNS Backbone DDoS
    Attacks](../Page/DNS_Backbone_DDoS_Attacks.md "wikilink")
  - [EDNS0](../Page/EDNS0.md "wikilink") (Extended DNS, version 0)
  - [DNS](../Page/DNS.md "wikilink")
  - [Internet backbone](../Page/Internet_backbone.md "wikilink")
  - [Open Root Server
    Network](../Page/Open_Root_Server_Network.md "wikilink")
  - [黑洞服务器](../Page/黑洞服务器.md "wikilink")

## 脚注

## 参考

  - [Root Server Technical Operations
    Association](http://www.root-servers.org/)
  - [Google地图](http://maps.google.com/maps/ms?ie=UTF8&hl=en&msa=0&msid=103993808347197645891.00043a30b2657ea61ea33&ll=11.424429,26.178063&spn=142.883537,288.632813&z=2&om=1)根域名服务器地理位置
  - [DNS Root Server System Advisory
    Committee](https://web.archive.org/web/20030927191521/http://www.rssac.org/)
  - [DNS Root Name Servers Explained For
    Non-Experts](http://www.isoc.org/briefings/019/)
  - [DNS Root Name Servers Frequently Asked
    Questions](http://www.isoc.org/briefings/020/)
  - [Location of Root servers in
    Asia-Pacific](https://web.archive.org/web/20051229123012/http://www.apnic.net/services/rootserver/)
  - [Bogus Queries received at the Root
    Servers](http://www.bind9.net/dnshealth/)
  - [ORSN, Open Root Server Network with IPv6 support in
    europe](http://www.orsn.org/)
  - RFC 2826 - IAB Technical Comment on the Unique DNS Root
  - RFC 2870 - Root Name Server Operational Requirements
  - RFC 4697 - Observed DNS Resolution Misbehavior (from observations on
    the Root Servers)

## 外部链接

  - [Root Server Technical Operations
    Association](http://www.root-servers.org/)（根域名服务器官网）

  - <ftp://ftp.internic.net/domain/>

  - [根域名服务器响应时间](https://web.archive.org/web/20081218184426/http://private.dnsstuff.com/info/roottimes.htm)

[Category:域名](../Category/域名.md "wikilink")

1.

2.  AS-numbers and IP-addresses from [Root-servers.org
    homepage](http://root-servers.org/) checked 9 January 2014

3.  Location and sites from [Root-servers.org
    homepage](http://root-servers.org/) checked 10 October 2014

4.
5.  [AS19836](https://stat.ripe.net/widget/bgplay#w.resource=AS19836&w.ignoreReannouncements=true&w.rrcs=0,1,6,7,11,14,3,4,5,10,12,13,15&w.starttime=1356998400&w.endtime=1389250800&w.instant=null&w.type=bgp)
    is not listed by the RIPEstat tool

6.  [AS64820](http://www.ris.ripe.net/cgi-bin/riswhois.cgi?address=198.41.0.4&matchtype=L&submit=Query+RISwhois)
    is listed as "private use" in RIPE's RISwhois tool

7.

8.  舊IP位址為128.9.0.107，已于2004年1月至2017年10月期間更換至192.228.79.201

9.

10.

11.

12.

13.
14.

15. 啟用于2013年1月3日，舊IP位址為128.8.10.90

16.

17.
18. [RISwhois](http://www.ris.ripe.net/cgi-bin/riswhois.cgi?address=199.7.91.13&matchtype=L&submit=Query+RISwhois),
    excluding less-specific AS3303 route announcement

19.
20.

21.
22.
23.

24. [F-root | Internet Systems
    Consortium](https://www.isc.org/community/f-root/)

25. (formerly [<http://www.nic.mil/> (Internet Archive
    link)](https://web.archive.org/web/20110621074912/http://www.nic.mil/);
    unlike all other DNS root servers, G-Root does not implement a
    homepage under root-servers.org, i.e. <http://g.root-servers.org/>)

26. 不像其他根網域名稱伺服器，G-Root不會回應[Ping](../Page/Ping.md "wikilink")

27. 不像其他根網域名稱伺服器，G-Root不會回應[Ping](../Page/Ping.md "wikilink")

28.
29.

30. 啟用於2015年12月1日，舊IP位址為128.63.2.53

31. <https://www.ietf.org/mail-archive/web/dnsop/current/msg15330.html>

32. 啟用於2015年12月1日，舊IP位址為2001:500:1::803f:235

33.

34.
35. 啟用於2015年12月1日，舊為AS13

36.

37.
38.

39. 啟用於2002年11月，舊IP位址為198.41.0.10

40.
41.

42.
43.
44.

45.

46. [K-root Homepage](http://k.root-servers.org/)

47. 啟用於2007年11月1日，舊IP位址為198.32.64.12

48.

49. 啟用於2016年3月23日，舊IP位址為2001:500:3::42

50.

51.
52. [1](http://www.ris.ripe.net/cgi-bin/riswhois.cgi?address=199.7.83.42&matchtype=L&submit=Query+RISwhois),
    excluding less-specific AS3303 route announcement

53.

54. [l.root-servers.net](http://l.root-servers.org/)

55.
56.

57.

58.

59. [Root Server Technical Operations
    Assn](https://web.archive.org/web/20100310015847/http://root-servers.org/)，[根域名服务器技术协会于](../Page/根域名服务器技术协会.md "wikilink")2010年3月10日的存档

60.

61.

62.

63.

64.

65.