**威爾遜**（），或译**威耳逊**、**威尔森**，是英文[姓氏](../Page/姓氏.md "wikilink")，在英语国家较为常见，该姓来源于[中世纪流行人名威尔](../Page/中世纪.md "wikilink")（Will）的[父名形式](../Page/父名.md "wikilink")。威尔一名来源于一些含有[日耳曼词根wil](../Page/日耳曼语族.md "wikilink")（意思是渴望）的名字\[1\]。这些名字中最常见的可能是[威廉](../Page/威廉.md "wikilink")（William），由词根wil和helm组成，意思是“渴望”和“头盔”、“保护\[2\]。1324年，威尔逊以Willeson的形式首次作为姓氏被记载于英语文献中\[3\]
。

## 人物列表

  - [伍德罗·威尔逊](../Page/伍德罗·威尔逊.md "wikilink")，美国政治家，曾任[美國總統](../Page/美國總統.md "wikilink")
  - [哈羅德·威爾遜](../Page/哈羅德·威爾遜.md "wikilink")，英国政治家，曾任[英国首相](../Page/英国首相.md "wikilink")
  - [罗伯特·威尔逊](../Page/罗伯特·威尔逊.md "wikilink")，美国射电天文学家
  - [艾德华·威尔森](../Page/艾德华·威尔森.md "wikilink")，美国昆虫学家
  - [查尔斯·威尔逊](../Page/查尔斯·威尔逊.md "wikilink")，英国物理学家，[诺贝尔物理奖得主](../Page/诺贝尔物理奖.md "wikilink")
  - [厄内斯特·亨利·威尔逊](../Page/厄内斯特·亨利·威尔逊.md "wikilink")，20世纪初英国植物学家，探险家。
  - [欧文·威尔逊](../Page/欧文·威尔逊.md "wikilink")，美国演员和剧作家。
  - [丹尼·威爾遜](../Page/丹尼·威爾遜.md "wikilink")，蘇格蘭足球運動員。
  - [詹姆斯·威尔逊](../Page/詹姆斯·威尔逊.md "wikilink")，英格蘭足球運動員。
  - [白琪·威爾遜](../Page/白琪·威爾遜.md "wikilink")，加拿大兒童文學作家。
  - [威尔伯·威尔逊](../Page/威尔伯·威尔逊.md "wikilink")，美以美会传教士
  - [Joe
    Wilson](../Page/Joe_Wilson.md "wikilink")，美国政治家，曾任[參議院議員](../Page/參議院.md "wikilink")
  - 英格兰数学家[John
    Wilson的](../Page/John_Wilson_\(mathematician\).md "wikilink")[威尔逊定理](../Page/威尔逊定理.md "wikilink")

## 注释

[Category:英語姓氏](../Category/英語姓氏.md "wikilink")

1.  。该网站引用.
2.  .
3.  .