[Kenting_Youth_Activity_Center_information_20060403.jpg](https://zh.wikipedia.org/wiki/File:Kenting_Youth_Activity_Center_information_20060403.jpg "fig:Kenting_Youth_Activity_Center_information_20060403.jpg")
[CYC-KTYAC_room_towel_2_20130519.jpg](https://zh.wikipedia.org/wiki/File:CYC-KTYAC_room_towel_2_20130519.jpg "fig:CYC-KTYAC_room_towel_2_20130519.jpg")
[CYC-KTYAC_room_towel_1_20130519.jpg](https://zh.wikipedia.org/wiki/File:CYC-KTYAC_room_towel_1_20130519.jpg "fig:CYC-KTYAC_room_towel_1_20130519.jpg")
**救國團墾丁青年活動中心**，另簡稱**墾丁青年活動中心**，位於[台灣](../Page/台灣.md "wikilink")[墾丁國家公園](../Page/墾丁國家公園.md "wikilink")，屬於[救國團經營之住宿與會議設施](../Page/中國青年救國團.md "wikilink")，有經典[閩南式建築住宅博物館之稱](../Page/閩南.md "wikilink")，面積24公頃，是墾丁著名的景點，北鄰墾丁路，其餘三面臨海，視野十分遼闊。

## 建築

墾丁青年活動中心係請建築師[漢寶德設計](../Page/漢寶德.md "wikilink")，由[榮工處承建](../Page/榮工處.md "wikilink")，1981年施工，1983年落成。全區房舍都屬於閩南式建築，總住宿容量520人，共有17棟各具代表性的[三合院](../Page/三合院.md "wikilink")、[三落院](../Page/三落院.md "wikilink")、[四合院等閩南式傳統建築](../Page/四合院.md "wikilink")，每棟房舍分別以台灣最常見的[堂號命名](../Page/堂號.md "wikilink")，有「潁川堂」、「西河堂」等，每個堂號都代表台灣的一個大姓：如潁川堂，就是陳姓的堂號；潁川堂門口還有陳姓由來的簡史解說立牌，簡介陳氏的歷史。另有仿古[書院](../Page/書院.md "wikilink")「菁莪堂」與「勵志書院」，可供450人開會研習活動使用。各建築物外觀十分古樸，不過內部則是現代化的西式裝潢與設備。

## 使用

墾丁青年活動中心是臺灣著名學生住宿地之一，不少臺灣學生都有[畢業旅行至此下榻團體房的經驗](../Page/畢業旅遊.md "wikilink")，而一般散客較少進駐。這是由於活動中心提供合菜、多人房、仿四合院的住宿區的服務較適合機關團體。

## 青蛙石公園

青蛙石位於墾丁青年活動中心區內，沿岸設有環青蛙石海洋遊憩步道。步道依海岸邊修建，可觀賞海岸景觀如珊瑚礁岩、海蝕崖，與墾丁特有熱帶海岸植物如棋盤腳、水芫花、草海桐與林投等。步道並可通往後方的貝殼沙灘。

## 外部連結

  - [墾丁青年活動中心簡介](http://kenting.cyh.org.tw/)

[K](../Category/屏東縣旅館.md "wikilink")
[K](../Category/救國團青年活動中心.md "wikilink")
[Category:墾丁國家公園](../Category/墾丁國家公園.md "wikilink")
[Category:1983年完工建築物](../Category/1983年完工建築物.md "wikilink")