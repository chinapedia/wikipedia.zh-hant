**AXN**是[索尼影業擁有的電視網](../Page/索尼影業.md "wikilink")，於1997年9月21日在[日本](../Page/日本.md "wikilink")[Sky
PerfecTV\!平台啟播](../Page/Sky_PerfecTV!.md "wikilink")，AXN至今已於全世界不同的地區播放。AXN
24小時播放電視劇、電影、動畫、真實歷險和體育節目。

AXN亞洲區經營者為索尼影業旗下的SPE
Networks亞洲有限公司（簡稱SPENA），[台灣區經營者為](../Page/台灣.md "wikilink")「美商超躍有限公司台灣分公司」。
AXN台灣頻道的前身，為[超級電視台於](../Page/超級電視台.md "wikilink")1996年12月開播的「超視二台」。
1997年，隨著索尼影業與超級電視台以互相投資的方式合作，超視二台先挪出夜間固定時段作為熱身時段《極度動感AXN》（後改名《AXN動感特區》），後轉型為AXN台灣頻道。1999年1月，[緯來企業取得AXN台灣獨家代理權](../Page/緯來企業.md "wikilink")。
2004年1月1日，[ANIMAX台灣頻道開播](../Page/Animax_Asia.md "wikilink")，AXN台灣頻道尚未播畢的動畫節目移往Animax台灣頻道繼續播出。
2017年12月20日，台灣地區啟用高畫質版本「AXN 台灣頻道 HD」，2018年1月4日起，陸續在各有線電視系統商播出。

2019農曆春節，AXN播出《蜘蛛人：返校日》等有線頻道難得一見的漫威與好萊塢首播強片，在批踢踢實業坊（ptt.cc）引發熱烈討論。有鄉民感嘆，很難得看到有線頻道播的好萊塢強片，肯定AXN的春節誠意。

台灣行銷由徐樹翔統籌，[《馬蓋先》VR體驗活動2016年在網路掀起熱烈討論](https://www.facebook.com/AXNTW/posts/1516216148407322)。《亞洲達人秀》第三季於2019年2月7日（週四）晚間8:30全球首播，台灣嘻哈舞團MANIAC
Family的亮眼表現，也被《[蘋果日報](../Page/蘋果日報_\(台灣\).md "wikilink")》、《[Yahoo奇摩新聞](../Page/Yahoo奇摩.md "wikilink")》、《[中時電子報](../Page/中時電子報.md "wikilink")》與《[NOWnews今日新聞](../Page/NOWnews今日新聞.md "wikilink")》大篇幅報導。

## 概述

AXN 頻道是亞洲首屈一指的動作及實境冒險頻道，以 18 至 39
歲的青年觀眾為主要對象，24小時全天候播放來自世界各地高收視電視影集、電影、實境冒險、動畫及
AXN亞洲原創節目。

於1997年開播，AXN頻道目前已經是國際知名頻道，並於全世界60個國家播放，其中包括拉丁美洲和歐洲。在亞洲，橫跨21
個亞洲國家的一億六百萬收視戶，共有一億四千八百萬觀眾群，其中約有五千萬的收視戶位於大中華地區。

2009年3月份，AXN也推出了HD高畫質頻道 （HD）。

AXN Asia隸屬Sony Pictures Entertainment並在Sony Pictures Television
Networks, Asia (SPENA) 轄下，該公司同時運作GEM、Animax、Sony Channel與ONE等頻道。

## 播出節目

### 歐美影集

#### 播映中

##### 台灣

  -
  -
  - [諜海黑名單](../Page/黑名單_\(2013年電視劇\).md "wikilink")

  - [警網急先鋒](../Page/警察世家.md "wikilink")

  - [芝加哥醫情](../Page/芝加哥醫情.md "wikilink")

  - [全院警戒](../Page/黑色代碼.md "wikilink")

  -
  - [檀島警騎2.0](../Page/天堂執法者.md "wikilink")

  - [謀殺入門課](../Page/逍遙法外_\(電視劇\).md "wikilink")

  - [貞愛好孕到](../Page/處女情緣.md "wikilink")

  - [星際獵殺](../Page/太空獵手掃興者.md "wikilink")

  -
  - [馬蓋先](../Page/馬蓋先_\(2016年電視劇\).md "wikilink")

  - [國務卿女士](../Page/國務卿女士.md "wikilink")

  - [下一站，天后](../Page/音樂之鄉.md "wikilink")

  - [重返犯罪現場：LA／重返犯罪現場：洛杉磯](../Page/海軍罪案調查處：洛杉磯.md "wikilink")

  - [蝶影行動](../Page/諜網.md "wikilink")

  - [大偵探福爾摩斯／新世紀福爾摩斯](../Page/神探夏洛克.md "wikilink")

  - [超自然檔案](../Page/邪惡力量.md "wikilink")

  -
  - [第二春](../Page/年輕一代.md "wikilink")

  - [暗殺教室](../Page/暗殺教室.md "wikilink")

##### 香港

  -
  - [叛諜黑名單](../Page/黑名單_\(2013年電視劇\).md "wikilink")

  - [執法尊家](../Page/警察世家.md "wikilink")

  - [H50型警](../Page/天堂執法者.md "wikilink")

  - [掃興者](../Page/太空獵手掃興者.md "wikilink")

  - [玉面飛龍](../Page/馬蓋先_\(2016年電視劇\).md "wikilink")

  - [海狼特警：洛杉磯篇](../Page/海軍罪案調查處：洛杉磯.md "wikilink")

  -
  - [福爾摩斯新世紀](../Page/神探夏洛克.md "wikilink")

  - [臥底潛行](../Page/諜網.md "wikilink")

  - [超自然檔案](../Page/邪惡力量.md "wikilink")

  -
  -
#### 播過動畫

  - 《[JoJo的奇妙冒險](../Page/JoJo的奇妙冒險_\(電視動畫\).md "wikilink")》
  - 《**[工作細胞](../Page/工作細胞.md "wikilink")**》
  - 《[七大罪](../Page/七大罪.md "wikilink")》
  - 《[暗殺教室](../Page/暗殺教室.md "wikilink") 》
  - 《[頭文字D](../Page/頭文字D.md "wikilink") 》
  - 《[魔導少年](../Page/魔導少年.md "wikilink") 》
  - 《[惡魔奶爸](../Page/惡魔奶爸.md "wikilink") 》
  - 《[獵人(新版)](../Page/獵人_\(新版\).md "wikilink") 》
  - 《[烈火之炎](../Page/烈火之炎.md "wikilink") 》
  - 《[數碼寶貝](../Page/數碼寶貝.md "wikilink") 》
  - 《[亂馬1/2](../Page/亂馬1/2.md "wikilink") 》
  - 《[神劍闖江湖](../Page/神劍闖江湖.md "wikilink") 》
  - 《[夢幻遊戲](../Page/夢幻遊戲.md "wikilink") 》
  - 《[CLAMP學園偵探團](../Page/CLAMP學園偵探團.md "wikilink") 》
  - 《[天使怪盜](../Page/天使怪盜.md "wikilink") 》
  - 《[神風怪盜](../Page/神風怪盜.md "wikilink") 》
  - 《[忍空](../Page/忍空.md "wikilink") 》
  - 《[幽遊白書](../Page/幽遊白書.md "wikilink") 》
  - 《[偵探學園Q](../Page/偵探學園Q.md "wikilink") 》
  - 《[GS美神](../Page/GS美神.md "wikilink") 》
  - 《[VR快打5](../Page/VR快打5.md "wikilink") 》
  - 《[三隻眼](../Page/三隻眼.md "wikilink") 》
  - 《[艾儷兒](../Page/艾儷兒.md "wikilink") 》
  - 《[非常偶像Key](../Page/非常偶像Key.md "wikilink") 》
  - 《[秋葉原電腦組](../Page/秋葉原電腦組.md "wikilink") 》
  - 《[天才小魚郎](../Page/天才小魚郎.md "wikilink") 》
  - 《[快傑蒸汽偵探團](../Page/快傑蒸汽偵探團.md "wikilink") 》
  - 《[影技](../Page/影技.md "wikilink") 》
  - 《[戰艦女高手](../Page/戰艦女高手.md "wikilink") 》
  - 《[雙鎗神嬌](../Page/雙鎗神嬌.md "wikilink") 》
  - 《[警花逮捕令](../Page/警花逮捕令.md "wikilink") 》
  - 《[魔法寶石](../Page/魔法寶石.md "wikilink") 》
  - 《[變身席格那](../Page/變身席格那.md "wikilink") 》
  - 《[神雕俠侶](../Page/神雕俠侶.md "wikilink") 》
  - 《[櫻花大戰](../Page/櫻花大戰.md "wikilink") 》
  - 《[灌籃高手](../Page/灌籃高手.md "wikilink") 》
  - 《[新戀愛白書](../Page/新戀愛白書.md "wikilink") 》

##### 台灣

  - [24反恐任務](../Page/24_\(電視劇\).md "wikilink")

  -
  - [雙面女間諜](../Page/特務A.md "wikilink")

  - [我欲為人](../Page/我欲為人.md "wikilink")

  -
  - [腦殘政客](../Page/吃腦外星人.md "wikilink")

  -
  - [芝加哥律政](../Page/芝加哥正義.md "wikilink")

  - [宅男特務](../Page/超市特工.md "wikilink")

  -
  - [妙探雙雄](../Page/共同法則.md "wikilink")

  - [跨國大追緝](../Page/跨國大追緝.md "wikilink")

  -
  - [犯罪心理：疑犯動機](../Page/犯罪心理：嫌犯動機.md "wikilink")

  - [CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")

  - [CSI：網路犯罪／CSI犯罪現場：網路犯罪](../Page/CSI犯罪現場：網路犯罪.md "wikilink")

  - [CSI犯罪現場：邁阿密](../Page/CSI犯罪現場：邁阿密.md "wikilink")

  - [CSI犯罪現場：紐約](../Page/CSI犯罪現場：紐約.md "wikilink")

  - [金權遊戲](../Page/法網裂痕.md "wikilink")

  - [末世黑天使](../Page/末世黑天使.md "wikilink")

  -
  - [美女上錯身](../Page/美女上錯身.md "wikilink")

  - [肯恩醫生煩不煩](../Page/肯醫師煩事多.md "wikilink")

  - [新聞超感應](../Page/新聞超感應.md "wikilink")

  - [大家都愛雷蒙](../Page/人人都愛雷蒙德.md "wikilink")

  - [末日決戰](../Page/隕落星辰.md "wikilink")

  - [黑色豪門企業](../Page/糖衣陷阱_\(電視劇\).md "wikilink")

  -
  - [雙面人魔](../Page/漢尼拔_\(電視劇\).md "wikilink")

  -
  - [超異能英雄](../Page/英雄_\(電視劇\).md "wikilink")

  -
  -
  - [核艦風雲](../Page/破釜沉舟_\(美國電視劇\).md "wikilink")

  - [偷天任務](../Page/偷天任務.md "wikilink")

  - [Lost檔案](../Page/迷失.md "wikilink")

  -
  - [超級英雄](../Page/超級英雄_\(電視劇\).md "wikilink")

  - [重返犯罪現場](../Page/重返犯罪現場.md "wikilink")

  - [辣媽當關](../Page/辣女隊醫.md "wikilink")

  - [夜班急診室](../Page/夜班醫生.md "wikilink")

  -
  - [雙生情謎](../Page/替身姐妹.md "wikilink")

  - [性福沒滿](../Page/性滿意足.md "wikilink")

  - [國務風雲](../Page/國務危情.md "wikilink")

  - [少年狼](../Page/少狼_\(電視劇\).md "wikilink")

  - [魔鬼終結者之莎拉·康納戰紀](../Page/終結者外傳.md "wikilink")

  - [噬血真愛](../Page/真愛如血.md "wikilink")

  - [醜女大翻身](../Page/醜女貝蒂.md "wikilink")

  -
##### 香港

  - [特務A](../Page/特務A.md "wikilink")

  - [吃腦外星人](../Page/吃腦外星人.md "wikilink")

  -
  - [妙探雙雄](../Page/共同法則.md "wikilink")

  -
  - [心理追兇：疑犯動機](../Page/犯罪心理：嫌犯動機.md "wikilink")

  - [CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")

  - [CSI犯罪現場：罪案網戰](../Page/CSI犯罪現場：網路犯罪.md "wikilink")

  - [CSI犯罪現場：邁阿密](../Page/CSI犯罪現場：邁阿密.md "wikilink")

  - [CSI犯罪現場：紐約](../Page/CSI犯罪現場：紐約.md "wikilink")

  -
  - [天外突攻](../Page/隕落星辰.md "wikilink")

  - [沉默的羔羊前傳](../Page/漢尼拔_\(電視劇\).md "wikilink")

  - [英雄](../Page/英雄_\(電視劇\).md "wikilink")

### 實境

  - [金氏世界瘋狂全紀錄](../Page/金氏世界瘋狂全紀錄.md "wikilink")(第2季)

  - [百战铁人王](../Page/百战铁人王.md "wikilink")(第6季)

  - [美國好聲音](../Page/美國好聲音.md "wikilink")(第8季)

  - [驚險全記錄](../Page/驚險全記錄.md "wikilink")(第7季)

  - [极限忍者王](../Page/极限忍者王.md "wikilink")

  - [亚洲达人秀](../Page/亚洲达人秀.md "wikilink")

  -
## 歷年LOGO

Image:AXN logo.svg|使用期間：開台至2015年9月23日 Image:Axn
global.png|使用期間：2015年9月24日至今 Image:|使用期間： Image:|使用期間：

## 註釋

## 參考資料

## 外部連結

  - [AXN](http://www.axn.com/)
  - [AXN亞洲](http://www.axn-asia.com/)
  - [AXN印度](http://www.axn-india.com/)
  - [AXN台灣](http://www.axn-taiwan.com/)
      -
      -
  - [AXN日本](http://axn.co.jp/)
  - [AXN韓國](http://www.axntv.co.kr/)

[Category:電視頻道](../Category/電視頻道.md "wikilink")
[Category:日本電視台](../Category/日本電視台.md "wikilink")
[Category:韓國電視台](../Category/韓國電視台.md "wikilink")
[Category:印度電視台](../Category/印度電視台.md "wikilink")
[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:索尼影視電視公司](../Category/索尼影視電視公司.md "wikilink")