{{elementbox |name=Uue |enname=Ununennium |number=119 |symbol=Uue
|left=\[\[鿫|-{zh-hans:

**Uue**（英語：**Ununennium**，化學符號為**Uue**）是一種尚未被發現的[化學元素](../Page/化學元素.md "wikilink")，原子序數是119，在元素週期表中排列在[第8周期](../Page/第8週期元素.md "wikilink")、[1族](../Page/1族元素.md "wikilink")。其相對原子質量約為297u。

## 預測性質

据推测，Uue是一种不稳定的[放射性元素](../Page/放射性元素.md "wikilink")，[相对原子质量约为](../Page/相对原子质量.md "wikilink")297u，其[单质可能为银白色或](../Page/单质.md "wikilink")[灰色的](../Page/灰色.md "wikilink")[金属](../Page/金属.md "wikilink")。另外，由于Uue是一种[碱金属元素](../Page/碱金属.md "wikilink")，根据碱金属元素的递变性，Uue单质可能是一种[液态金属](../Page/液态金属.md "wikilink")，[电负性](../Page/电负性.md "wikilink")小於0.7，活性很大，如果製作出穩定[同位素](../Page/同位素.md "wikilink")，需要保存於[礦物油中](../Page/礦物油.md "wikilink")，要不然會與氧化合生成氧化Uue（Uue<sub>2</sub>O）。

由于Uue是一种[碱金属元素](../Page/碱金属.md "wikilink")，所以容易形成[陽離子](../Page/陽離子.md "wikilink")（即Uue<sup>+</sup>），其[氢氧化物](../Page/氢氧化物.md "wikilink")（氫氧化Uue即UueOH）可能为[强碱](../Page/强碱.md "wikilink")。

## 預測衰變特性

[原子](../Page/原子.md "wikilink")[質量數為](../Page/質量數.md "wikilink")1700以內，且質子數為100≤Z≤130已經可以用**量子計算模型**與**隧道α-衰變
Q值**來進行α-衰變的半衰期的估計\[1\]\[2\]\[3\]α-衰變[半衰期預測](../Page/半衰期.md "wikilink")<sup>291</sup>Uue-<sup>307</sup>Uue都是以[微秒計時](../Page/微秒.md "wikilink")。半衰期最長的α衰變半衰期的預測模型中的量子隧道與估計從宏觀-微觀模型:<sup>294</sup>Uue半衰期約為485[微秒](../Page/微秒.md "wikilink")。對於<sup>302</sup>Uue是163微秒，在**預測**中<sup>294</sup>Uue擁有最長的α衰變半衰期485[微秒](../Page/微秒.md "wikilink")，約0.5毫秒。

在[穩定島理論中](../Page/穩定島理論.md "wikilink")[幻數有](../Page/魔數.md "wikilink")[2](../Page/2.md "wikilink")、[8](../Page/8.md "wikilink")、[20](../Page/20.md "wikilink")、[28](../Page/28.md "wikilink")、[50](../Page/50.md "wikilink")、[82](../Page/82.md "wikilink")、[126](../Page/126.md "wikilink")、[184](../Page/184.md "wikilink")，<sup>303</sup>Uue有184個中子，為[幻數也許較穩定](../Page/魔數.md "wikilink")。

## 未成功的合成实验

1985年[加利福尼亚州的](../Page/加利福尼亚州.md "wikilink")[superHILAC加速器进行了一次合成Uue的实验](../Page/superHILAC.md "wikilink")，以<sup>48</sup>[Ca离子轰击](../Page/钙.md "wikilink")<sup>254</sup>[Es](../Page/锿.md "wikilink")，实验中没有观测到产生的新原子。\[4\]
反应方程式如下：

\[\,^{254}_{99}\mathrm{Es} + \,^{48}_{20}\mathrm{Ca} \to \,^{302}_{119}\mathrm{Uue} ^{*} \to\]
無原子

## 同位素与核特性

\=== 能产生Z=119复核的目标、发射体组合 === 下表列出各种可用以产生原子序为119的目标、发射体组合。

<table>
<thead>
<tr class="header">
<th><p>目标</p></th>
<th><p>发射体</p></th>
<th><p>CN</p></th>
<th><p>结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><sup>254</sup>Es</p></td>
<td><p><sup>48</sup>Ca</p></td>
<td><p><sup>302</sup>Uue</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>249</sup>Bk</p></td>
<td><p><sup>50</sup>Ti</p></td>
<td><p><sup>299</sup>Uue</p></td>
<td></td>
</tr>
</tbody>
</table>

### 蒸发截面理论计算

下表列出各种目标-发射体组合，并给出最高的预计产量。

MD = 多面；DNS = 双核系统； σ = 截面

| 目标               | 发射体             | CN                | 通道（产物）                 | σ <sub>max</sub> | 模型  | 参考资料  |
| ---------------- | --------------- | ----------------- | ---------------------- | ---------------- | --- | ----- |
| <sup>254</sup>Es | <sup>48</sup>Ca | <sup>302</sup>Uue | 3n (<sup>299</sup>Uue) | 0.5 pb           | DNS | \[5\] |

## 参考资料

[Category:碱金属](../Category/碱金属.md "wikilink")
[8A](../Category/第8周期元素.md "wikilink")
[8A](../Category/化学元素.md "wikilink")

1.
2.
3.
4.
5.