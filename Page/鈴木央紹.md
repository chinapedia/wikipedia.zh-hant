**鈴木央紹**（），[大阪府出生的音樂演奏家](../Page/大阪府.md "wikilink")。以關西做為中心活躍的薩克斯風演奏家。曾參與2004年[ZARD](../Page/ZARD.md "wikilink")『[What
a beautiful moment](../Page/What_a_beautiful_moment.md "wikilink")』演出。

## 經歷

四歲起受父親影響開始學習音樂，小學四年級起跟著母親學[鋼琴](../Page/鋼琴.md "wikilink")，小學六年級停止鋼琴學習，開始學習薩克斯風。高中時期開始嶄露頭角。畢業於[大阪音樂大學音樂學部主修](../Page/大阪音樂大學.md "wikilink")[薩克斯風](../Page/薩克斯風.md "wikilink")。之後在[韓國組成big](../Page/韓國.md "wikilink")
band，在[首爾的大學演奏](../Page/首爾.md "wikilink")。現在，把自己的四重奏做為基礎，與各式各樣有名的[爵士樂團共同演出](../Page/爵士.md "wikilink")。

  - 曾擔任
      - 1992～1999年河合樂器Sax科講師。
      - 1994～2002年山葉樂器Sax科講師。

## 獲獎紀錄

「AXIA MUSIC AUDITION」中，獲獎AXIA獎Instrumental部門Grand Prix。

## 使用樂器

  - T.Sax 「A.Selmer Mark Ⅵ」
  - S.Sax 「H.Selmer Mark Ⅵ」
  - A.Sax 「H.Selmer Mark Ⅵ」

## 參與作品

  - 「OMORI BAND」
  - 「OMORI BAND　ＣＤ-Ｒ盤」
  - 湯井一葉「La Derniere Valse」
  - 箕作元総「Songs For Unknown Great People」
  - Keiko Hamada & SoulCats「Mist Rain」
  - 3 Sax Lab.「Live at the Royal Horse」
  - 多田恵美子「EMIKO TADA AND FRIENDS」
  - Woong San「introducing WOONG SAN」
  - 藤村麻紀「With love」
  - [ZARD](../Page/ZARD.md "wikilink")「與你的距離」
  - [竹井詩織里](../Page/竹井詩織里.md "wikilink")「second tune 世界 暫停吧」
  - Woong San「The Blues」
  - Woong San「Call Me」
  - 大野雄二「LUPIN THE THIRD JAZZ the 10th～New Flight～」
  - 秋田慎治「moments in life」
  - 大野雄二\&Lupintic Five 「SEVEN DAYS RHAPSODY」

## 相關網站

  - [鈴木央紹官方網站](http://www.hisax.net/index.html)
  - [鈴木央紹部落格](http://hisax.blog48.fc2.com/)

[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")