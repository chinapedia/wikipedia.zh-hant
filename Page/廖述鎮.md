**廖述鎮**（），[台中縣](../Page/台中縣.md "wikilink")[大雅鄉](../Page/大雅區.md "wikilink")（今[臺中市大雅區](../Page/臺中市.md "wikilink")）出生。[台灣政治人物](../Page/台灣.md "wikilink")
，為[民主進步黨黨員](../Page/民主進步黨.md "wikilink")，曾任[第一屆臺中市(直轄市)議員](../Page/第一屆臺中市\(直轄市\)議員列表.md "wikilink")\[1\]。

## 學歷

  - 三和國小畢業
  - 大雅初中
  - [新民商工](../Page/新民商工.md "wikilink")
  - [勤益技術學院](../Page/勤益技術學院.md "wikilink")
    (已於2007年改制為[國立勤益科技大學](../Page/國立勤益科技大學.md "wikilink"))\[2\]\[3\]
  - 日本玉山台灣研究班
  - 美國聖地牙哥大學人力資源研究班\[4\]

## 經歷

  - [民進黨全國黨員代表](../Page/民進黨.md "wikilink")
  - [民進黨台中縣黨部創黨黨員](../Page/民進黨.md "wikilink")
  - 台中縣黨部二任評議委員
  - 台中縣黨部副執行長
  - 大雅鄉農會第十二、十三、十四、十五、十六屆代表
  - 台中縣議會第十四、十五屆、十六屆縣議員（民進黨黨團幹事長、黨團招集人）
  - [第一屆臺中市議會(直轄市)議員](../Page/第一屆臺中市\(直轄市\)議員列表.md "wikilink")

## 資料來源

<references/>

## 參見

  - [臺中市政府](../Page/臺中市政府.md "wikilink")
  - [臺中市議會](../Page/臺中市議會.md "wikilink")
  - [第一屆臺中市(直轄市)議員列表](../Page/第一屆臺中市\(直轄市\)議員列表.md "wikilink")

## 外部連結

  - [台中市議會議員介紹](http://www.tccn.gov.tw/wb_introduction02.asp?cno=149#top)
  - [廖述鎮 Facebook](https://www.facebook.com/LiaoShuZhen)

[Category:第16屆臺中縣議員](../Category/第16屆臺中縣議員.md "wikilink")
[Category:第15屆臺中縣議員](../Category/第15屆臺中縣議員.md "wikilink")
[Category:第14屆臺中縣議員](../Category/第14屆臺中縣議員.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:民主進步黨全國黨代表](../Category/民主進步黨全國黨代表.md "wikilink")
[Category:國立勤益科技大學校友](../Category/國立勤益科技大學校友.md "wikilink")
[Category:臺中市私立新民高級中學校友](../Category/臺中市私立新民高級中學校友.md "wikilink")
[Category:臺中市立大雅國民中學校友](../Category/臺中市立大雅國民中學校友.md "wikilink")
[Category:臺中市三和國小校友](../Category/臺中市三和國小校友.md "wikilink")
[Category:大雅人](../Category/大雅人.md "wikilink")
[Shu述](../Category/張廖姓.md "wikilink")

1.  [清廉 勤政 愛鄉土－廖述鎮](http://www.wretch.cc/blog/dpptcs/9986714)
2.  [勤益檔案展示網-改制及改名專區](http://archives.ncut.edu.tw/files/11-1027-449.php)
3.  [勤益科技大學網站-本校簡史](http://web2.ncut.edu.tw/files/11-1000-16-1.php)
4.  [台中市議會議員介紹](http://www.tccn.gov.tw/wb_introduction02.asp?cno=149#top)