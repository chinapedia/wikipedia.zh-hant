**詹姆斯·乔治·温纳瑞斯**（James George
Veneris，），[美国军人](../Page/美国.md "wikilink")（军号RA13009671），1950年11月28日在[朝鲜战争中被](../Page/朝鲜战争.md "wikilink")[中国人民志愿军俘虏](../Page/中国人民志愿军.md "wikilink")，后拒绝返回美国，定居中国大陸并加入[中华人民共和国国籍](../Page/中华人民共和国国籍.md "wikilink")，人称“老温”。

温纳瑞斯在战俘营中曾担任战俘营营际奥运会委员\[1\]。1953年朝鲜战争停战后，他同另外20名美国士兵和1名[英国士兵选择前往中国定居](../Page/英国.md "wikilink")。到中国以后，他被分配到[山东](../Page/山东.md "wikilink")[济南造纸四厂工作](../Page/济南.md "wikilink")。

1963年，他进入[中国人民大学学习](../Page/中国人民大学.md "wikilink")，1966年毕业后回到了济南。在[文革中他一度遭到冲击](../Page/文革.md "wikilink")，但在[周恩来的保护下得以幸免](../Page/周恩来.md "wikilink")。他在中国共结过3次婚，生有四男二女，其中最小的女儿和最小的儿子回到了美国。他本人曾经于1976年和1990年左右两次赴美国探亲。老温退休后，在[山东大学教授](../Page/山东大学.md "wikilink")[英语](../Page/英语.md "wikilink")。

晚年老温积极要求加入[中国共产党](../Page/中国共产党.md "wikilink")，但是未能如愿，他于2004年去世。

## 参考资料

  - [齐鲁晚报·一个老兵的最后追求](http://news.sina.com.cn/s/2001-08-30/343595.html)
  - 边震遐著《朝鲜战争中的“联合国军”战俘》
  - [青年参考·21名美军战俘命运备忘录](http://news.sina.com.cn/o/2006-03-01/00028327142s.shtml)

## 注释

[V](../Category/归化中华人民共和国公民的美国人.md "wikilink")
[V](../Category/朝鲜战争战俘.md "wikilink")
[V](../Category/中國人民大學校友.md "wikilink")
[V](../Category/美國陸軍人物.md "wikilink")
[V](../Category/山东大学教授.md "wikilink")
[Category:美國韓戰軍事人物](../Category/美國韓戰軍事人物.md "wikilink")

1.