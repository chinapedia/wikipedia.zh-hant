**眼压**（），又称为**眼内压**，[眼球内容物对眼球内壁的](../Page/眼球.md "wikilink")[压力](../Page/压力.md "wikilink")。「正常眼压」通常是在10-21mmHg之间，但实际上「正常眼压」这个提法是不准确的，这实际应该是大部分人（人群的95%）的眼压范围。正常人的眼压一般双眼的差异不大於5mmHg，每天的波动范围在8mmHg之内。眼压高，往往会对[视神经造成损害](../Page/视神经.md "wikilink")，导致[青光眼](../Page/青光眼.md "wikilink")。\[1\]

人类的眼压，是通过[房水的生成和排出的动态平衡来维持的](../Page/房水.md "wikilink")。房水由[睫状突的无色素细胞分泌](../Page/睫状突.md "wikilink")，进入[后房](../Page/后房.md "wikilink")，经过瞳孔到达[前房](../Page/前房.md "wikilink")，有两大排出途径：

1.  经过[前房角的](../Page/前房角.md "wikilink")[小梁网进入](../Page/小梁网.md "wikilink")，到达集合管，再到达睫状前静脉
2.  经过前房角的睫状体带进入[睫状肌间隙](../Page/睫状肌.md "wikilink")，进入[睫状体和](../Page/睫状体.md "wikilink")[脉络膜上腔](../Page/脉络膜.md "wikilink")，然后通过血管的间隙进入静脉引流。

这两个通道到目前为止人们发现都是压力依赖性的，并没有一个主动运输的过程。

[Eye_angle_humer.JPG](https://zh.wikipedia.org/wiki/File:Eye_angle_humer.JPG "fig:Eye_angle_humer.JPG")
[Patient_and_tonometer.jpg](https://zh.wikipedia.org/wiki/File:Patient_and_tonometer.jpg "fig:Patient_and_tonometer.jpg")\]\]

## 測量

眼壓於[眼睛檢查中透過一](../Page/眼睛檢查.md "wikilink")[眼壓計](../Page/眼壓計.md "wikilink")。目前使用的眼壓測量方法主要分為

  - 平壓式
  - 壓凹式

在平壓式的眼壓計，我們測量的是壓平角膜一小塊固定面積所需要的力。而在壓凹式的眼壓計，我們觀察的是在角膜施與一個固定的力後，眼球被壓凹或變形的程度。

測得的眼壓會受[角膜厚度與硬度影響](../Page/角膜.md "wikilink")。\[2\]

## 診斷

正常眼压的范围为1.47～2.79kPa（11～21mmHg)\[3\]

[眼壓過高定義為在無](../Page/眼壓過高.md "wikilink")[視神經傷害或](../Page/視神經.md "wikilink")[視野損失下](../Page/視野.md "wikilink")，眼壓高於正常值。\[4\]\[5\]

低眼壓症，被定義為眼壓等於或低於5mmHg。\[6\]\[7\] 如此如此低的眼壓可判斷眼球之液體滲漏或塌陷。
研究顯示，眼壓的分布不完全是一個[高斯分布](../Page/高斯分布.md "wikilink")（Gaussian
distribution），而是稍微偏高眼壓。因此不能藉由簡化平均值，再加上兩至三個的[標準差作為眼壓的上限值](../Page/標準差.md "wikilink")。反之，正常範圍的眼壓值，也不能保證不發生青光眼視神經病變。有一個簡單的通則可以運用，就是兩個眼睛之間的眼壓差別。在正常的族群中只有不到4%的人，兩眼間的眼壓差會超過4mmHg。但在青光眼的病人中，這種程度的差別就比較常見。\[8\]

## 影響因素

### 全天變動

眼壓成天變動。正常人一整天下來眼壓平均的變化為3-6mmHg，青光眼病人的變化相對大了許多。
眼壓的晝夜的變化通常是因為房水分泌的變化所造成，而這個最主要的影響因素可能是循環中[兒茶酚胺](../Page/兒茶酚胺.md "wikilink")
(catecholamine) 的濃度。也有學者報告在冬季會有較高的眼壓，而將此歸因於日照時間及氣壓變化的影響。

### 運動或姿勢的改變

劇烈的運動會使眼壓短暫下降，可能成因爲血漿中[滲透壓的改變](../Page/滲透壓.md "wikilink")。正常人由坐姿變成仰臥時，眼壓可增加達6mmHg
。 將一個人倒立，眼壓更可能升高30mmHg之多。 同樣的，在青光眼的病人這個現象比較明顯。

### 荷爾蒙、食物、及藥物因素

眼壓的晝夜變化和循環中類皮質醣 (glucocorticoid)
的濃度有關。局部或全身性使用的[類固醇也會使眼壓升高](../Page/類固醇.md "wikilink")。眼壓也會隨著[月經週期而變化](../Page/月經.md "wikilink")，在[懷孕末期眼壓也會降低](../Page/懷孕.md "wikilink")。除了ketamine及trichlorethylene之外，眼壓會隨著全身[麻醉的深度增加而降低](../Page/麻醉.md "wikilink")。含[酒精的飲料經由抑制抗利尿](../Page/酒精.md "wikilink")[荷爾蒙及減少](../Page/荷爾蒙.md "wikilink")[房水的分泌](../Page/房水.md "wikilink")，達到降眼壓的目的。局部使用的[睫狀肌麻痺劑](../Page/睫狀肌麻痺劑.md "wikilink")（常用來治療近視）有可能會升高眼壓，尤其在青光眼的病人。短時間飲用大量的水，也有可能引起眼壓升高。

### 眼球局部的因素

1.  屈調的異常：眼壓與眼軸長短或近視與否有關。
2.  [眼球運動及眼瞼閉合](../Page/眼球.md "wikilink")：眼睛往極端的方向或有阻力的方向運動，有可能會讓眼壓急遽升高。用力緊閉與睜大眼睛會使眼壓增加。
3.  眼內[發炎](../Page/發炎.md "wikilink")：房水分泌減少，通常眼壓會下降。但如房水通路遭到發炎物質阻塞，則眼壓有可能升高。

### 其他因素

眼壓也會因諸如[心臟搏動頻率](../Page/心臟.md "wikilink")，[血壓](../Page/血壓.md "wikilink")，[年齡](../Page/年齡.md "wikilink")，性別、種族，[遺傳皆有關係](../Page/遺傳.md "wikilink")。\[9\]
[呼吸](../Page/呼吸.md "wikilink")，攝取水量，[外用藥物也會影響眼壓](../Page/外用藥物.md "wikilink")。
服用[酒精與](../Page/酒精.md "wikilink")[大麻會使眼壓下降](../Page/大麻.md "wikilink")，[咖啡因則可能導致眼壓上升](../Page/咖啡因.md "wikilink")。\[10\]
[甘油](../Page/甘油.md "wikilink")（經常與果汁混合以稀釋其甜味）可導致一急速且短暫的眼壓下降。欲對急升眼壓做緊急處理時此常常用到。\[11\]

## 參考

[Category:解剖学](../Category/解剖学.md "wikilink")
[Category:眼睛](../Category/眼睛.md "wikilink")

1.
2.  [眼壓－影響的因素及測量的方法](https://www.ntuh.gov.tw/OPH/DocLib10/眼壓－影響的因素及測量的方法.aspx)
3.
4.
5.  [American Optometric Association - Ocular
    Hypertension](http://www.aoa.org/ocular-hypertension.xml)
6.  [eMedicine - Ocular Hypotony - Author: Sheila P Sanders,
    MD](http://emedicine.medscape.com/article/1207657-overview)
7.
8.  \[ <http://www.ntuh.gov.tw/OPH/DocLib10/眼壓－影響的因素及測量的方法.aspx>
9.  [](http://www.ntuh.gov.tw/OPH/DocLib10/眼壓－影響的因素及測量的方法.aspx)
10. Intraocular pressure measure on normal eyes by Pardianto G et al.,
    in Mimbar Ilmiah Oftalmologi Indonesia.2005;2:78-9.
11. Effect of Oral Glycerol on Intraocular Pressure in Normal and
    Glaucomatous Eyes, S. M. Drance, MD, FRCS (ENG) *Arch Ophthalmol.*
    1964;72(4):491-493.