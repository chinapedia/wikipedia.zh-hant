**罗清泉**（），[湖北](../Page/湖北.md "wikilink")[江陵人](../Page/江陵.md "wikilink")，大學學歷。曾任湖北省省長、[中共湖北省委書記](../Page/中共湖北省委.md "wikilink")、省人大常委會主任。

## 生平

羅在1968年参加工作，1975年加入[中國共產黨](../Page/中國共產黨.md "wikilink")。歷任中共湖北省[宜昌市委副書記](../Page/宜昌市.md "wikilink")、代市長、市長、市委書記，中共湖北省委副書記、省紀委書記、[武漢市委書記](../Page/武漢市.md "wikilink")。2002年10月任湖北省副省長、代省長。2003年1月當選湖北省省長。2007年10月接替[俞正声任中共湖北省委书记](../Page/俞正声.md "wikilink")。2008年，担任中共十七届中央委员，全国人大环境与资源保护委员会副主任委员\[1\]。2010年12月因年龄原因不再担任省委书记职务。2010年12月25日，任[全国人大环境与资源保护委员会副主任委员](../Page/全国人大.md "wikilink")。

中共第十五届中纪委委员，第十六、十七届中央委员。

## 參考文獻

[L](../Category/江陵人.md "wikilink") [Q](../Category/羅姓.md "wikilink")
[Category:第七届全国人大代表](../Category/第七届全国人大代表.md "wikilink")
[Category:第八届全国人大代表](../Category/第八届全国人大代表.md "wikilink")
[Category:第十届全国人大代表](../Category/第十届全国人大代表.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:第十二届全国人大常委会委员](../Category/第十二届全国人大常委会委员.md "wikilink")
[Category:中华人民共和国湖北省省长](../Category/中华人民共和国湖北省省长.md "wikilink")
[Category:中共湖北省委书记](../Category/中共湖北省委书记.md "wikilink")
[Category:中华人民共和国宜昌市市长](../Category/中华人民共和国宜昌市市长.md "wikilink")
[Category:中共宜昌市委书记](../Category/中共宜昌市委书记.md "wikilink")
[Category:中共武汉市委书记](../Category/中共武汉市委书记.md "wikilink")
[Category:中国共产党第十五届中央纪律检查委员会委员](../Category/中国共产党第十五届中央纪律检查委员会委员.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中共十五大代表](../Category/中共十五大代表.md "wikilink")
[Category:中共十六大代表](../Category/中共十六大代表.md "wikilink")
[Category:中共十七大代表](../Category/中共十七大代表.md "wikilink")
[Category:中共十九大代表](../Category/中共十九大代表.md "wikilink")
[Category:湖北省人大常委会主任](../Category/湖北省人大常委会主任.md "wikilink")

1.