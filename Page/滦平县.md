**滦平县**在[中国](../Page/中国.md "wikilink")[河北省北部](../Page/河北省.md "wikilink")，是[承德市下辖的一个县](../Page/承德.md "wikilink")。

[北京通往](../Page/北京.md "wikilink")[承德的公路和通往](../Page/承德.md "wikilink")[通辽的铁路都穿过滦平镇](../Page/通辽.md "wikilink")（旧称喀喇城），交通发达。全县基本处于山区，[滦河和](../Page/滦河.md "wikilink")[潮河分别穿过该县东西两面](../Page/潮河.md "wikilink")，县城是两条河流流域的分水岭。[长城是该县和](../Page/长城.md "wikilink")[北京市](../Page/北京市.md "wikilink")（[密云区](../Page/密云区.md "wikilink")）的分界线。

## 气候

年降水量500毫米，年均温7.6℃。

## 人口

有汉族、满族、和其他回族、蒙古族、朝鲜族等少数民族。

## 行政区划

下辖1个[街道办事处](../Page/街道办事处.md "wikilink")、8个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、9个[民族乡](../Page/民族乡.md "wikilink")：

。

## 风景名胜

  - [碧霞山](../Page/碧霞山.md "wikilink")，属[丹霞地貌](../Page/丹霞地貌.md "wikilink")
  - [白云峡](../Page/白云峡.md "wikilink")，属[火山岩地貌](../Page/火山岩地貌.md "wikilink")

## 参考文献

## 外部链接

  - [滦平县人民政府门户网站](http://www.lpx.gov.cn/)

{{-}}

[承德](../Category/河北省县份.md "wikilink")
[滦平县](../Category/滦平县.md "wikilink")
[县](../Category/承德区县.md "wikilink")
[冀](../Category/国家级贫困县.md "wikilink")