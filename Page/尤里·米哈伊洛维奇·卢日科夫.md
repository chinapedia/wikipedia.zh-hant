**尤里·米哈伊洛维奇·卢日科夫**（，），[俄罗斯](../Page/俄罗斯.md "wikilink")[政治家](../Page/政治家.md "wikilink")，曾在1992年至2010年担任[莫斯科](../Page/莫斯科.md "wikilink")[市长一职](../Page/市长.md "wikilink")。

## 經歷

尤里·卢日科夫毕业于莫斯科古布金石油和天然气学院，1958-1964年在合成树脂材料研究所从事研究工作，1964年加入化学工业部工作，并于1980年成为该部门的负责人；1987-1990年任莫斯科市苏维埃执行委员会第一副主席，同时是莫斯科市农工总合体局局长。1990-1991年任莫斯科市苏维埃执行委员会主席，1991年7月担任莫斯科市副市长一职，并于1992年6月在[加夫里尔·哈里托诺维奇·波波夫退休后担任了莫斯科市长一职务](../Page/加夫里尔·哈里托诺维奇·波波夫.md "wikilink")，且在3次竞选中（1996,
1999, 2003）胜出获得连任，担任市长一职务至2010年。

2007年11月8日，[俄羅斯聯邦委員會簽署](../Page/俄羅斯聯邦委員會.md "wikilink")[政令](../Page/政令_\(俄羅斯\).md "wikilink")，自2008年1月1日起，首都市長產生方式恢復中央任命。並決定由盧日科夫繼續擔任該職務。

2010年9月28日，[俄羅斯聯邦委員會簽署](../Page/俄羅斯聯邦委員會.md "wikilink")[政令](../Page/政令_\(俄羅斯\).md "wikilink")，免去盧日科夫的市長職務。\[1\]

## 参考资料

## 外部連結

  - [Юрий Лужков в Лентапедии](http://lenta.ru/lib/14159332/full.htm)
  - [Литературные произведения Юрия
    Лужкова](https://web.archive.org/web/20100330160944/http://www.luzhkov.mos.ru/)
  - [Мэр, спортсмен и пчеловод. Биография Юрия
    Лужкова](http://www.rian.ru/spravka/20070410/63391547.html)
  - [Борис Немцов: «Лужков. Итоги»](http://www.nemtsov.ru/?id=705916)
  - [Борис Немцов: «Лужков.
    Итоги — 2»](https://web.archive.org/web/20101204073752/http://www.luzhkov-itogi.ru/doklad/)
  - [Справка на сайте Совета
    Федерации](http://www.council.gov.ru/staff/members/power38929.html)
  - \[<http://mos.ru/wps/portal/!ut/p/c1/04_SB8K8xLLM9MSSzPy8xBz9CP0os3hXN1e3QHMPIwMDU1djAyM_SxOXUEs_IwN_Q6B8pFm8n79RqJuJp6GhhZmroYGRmYeJk0-Yp4G7izExug1wAEcDArrDQa7FbztIHo_5fh75uan6BbmhEQZZJooA0IaW2A>\!\!/dl2/d1/L3dJVkkvd0xNQUJrQUVrQSEhL1lCcHhKRjFOQUEhIS82X0VGRUZRN0gyMDA1RTMwMk45NERVOU4yME8xLzdfRUZFRlE3SDIwMERVOTAyTjlUUDRKNjEwQjI\!?nID=6_EFEFQ7H2005E302N94DU9N20O1\&cID=6_EFEFQ7H2005E302N94DU9N20O1\&rubricId=3139
    Биография на официальном сервере Правительства Москвы\]
  - [Страница на сайте Международного
    университета](https://web.archive.org/web/20100815080940/http://www.interun.ru/f_ukg.html)

<!-- end list -->

  - 文章

<!-- end list -->

  - [Первое с момента отставки большое интервью, журнал «The New Times»,
    № 32, 04.10.2010.](http://newtimes.ru/articles/detail/28421)
  - [Первый мэр Москвы **Гавриил Попов** — о втором мэре Юрии Лужкове.
    Радио Свобода *(текст, видео,
    аудио)*](http://www.svobodanews.ru/content/feature/2157411.html)

[Category:俄國政治人物](../Category/俄國政治人物.md "wikilink")
[Category:莫斯科市长](../Category/莫斯科市长.md "wikilink")
[Category:蘇聯政治人物](../Category/蘇聯政治人物.md "wikilink")
[Category:蘇聯科學家](../Category/蘇聯科學家.md "wikilink")
[Category:俄羅斯正教徒](../Category/俄羅斯正教徒.md "wikilink")
[Category:莫斯科人](../Category/莫斯科人.md "wikilink")
[Category:奥林匹克勋章获得者](../Category/奥林匹克勋章获得者.md "wikilink")
[Category:一级祖国功勋勋章获得者](../Category/一级祖国功勋勋章获得者.md "wikilink")
[Category:二级祖国功勋勋章获得者](../Category/二级祖国功勋勋章获得者.md "wikilink")
[Category:三级祖国功勋勋章获得者](../Category/三级祖国功勋勋章获得者.md "wikilink")
[Category:四级祖国功勋勋章获得者](../Category/四级祖国功勋勋章获得者.md "wikilink")

1.