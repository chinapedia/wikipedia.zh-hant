**張雁書**是[臺灣](../Page/臺灣.md "wikilink")[臺南市的](../Page/臺南市.md "wikilink")[乒乓球运动员](../Page/乒乓球.md "wikilink")，與[蔣澎龍搭配的雙打曾經排名世界第一](../Page/蔣澎龍.md "wikilink")。1998年曼谷亞運中奪得男子雙打銅牌，張雁書與蔣澎龍則在雪梨奧運男子雙打名列第5。

## 成績

  - 2001年世界桌球錦標賽男子雙打銅牌
  - 2001年日本公開賽賽男子單打前四名
  - 2006年杜哈亞運男子團體銅牌
  - 2007年世界桌球錦標賽男子雙打銅牌

## 參考資料

## 外部連結

[Category:台灣退役乒乓球運動員](../Category/台灣退役乒乓球運動員.md "wikilink")
[Category:張姓](../Category/張姓.md "wikilink")
[Category:2000年夏季奥林匹克运动会乒乓球运动员](../Category/2000年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:2002年亞洲運動會銅牌得主](../Category/2002年亞洲運動會銅牌得主.md "wikilink")
[Category:2006年亚洲运动会铜牌得主](../Category/2006年亚洲运动会铜牌得主.md "wikilink")
[Category:2008年夏季奥林匹克运动会乒乓球运动员](../Category/2008年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:亞洲運動會桌球獎牌得主](../Category/亞洲運動會桌球獎牌得主.md "wikilink")