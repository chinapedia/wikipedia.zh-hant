**提请刪除页面的请求，請根據以下三個步驟：**

<table>
<thead>
<tr class="header">
<th><p>壹</p></th>
<th><div>
<p><big><strong>在該頁面放置刪除標籤</strong></big></p>
</div>
<p>請根據以下三種不同情況，在被提報的頁面頂部加入以下的模板：</p>
<ol>
<li>重定向頁： <strong>{{vfd|理由|r|date=--}}</strong></li>
<li>移動到其他維基媒體計劃：
<ul>
<li>移動到維基詞典　： <strong>{{vfd|理由|wikt|date=--}}</strong></li>
<li>移動到維基語錄　： <strong>{{vfd|理由|q|date=--}}</strong></li>
<li>移動到維基教科書： <strong>{{vfd|理由|b|date=--}}</strong></li>
<li>移動到維基文庫　： <strong>{{vfd|理由|s|date=--}}</strong></li>
</ul></li>
<li>其他： <strong>{{vfd|理由|date=--}}</strong></li>
</ol>
<dl>
<dt></dt>
<dd><small>註：如需在其他特殊情況下使用模板，可到<a href="Template:Vfd/usage.md" title="wikilink">此處了解</a>。</small>
</dd>
</dl>
<p>加入模板時請在<a href="Help:編輯摘要.md" title="wikilink">編輯摘要中加入</a>「Vfd」、「Afd」或相關字眼，也不要標示為「小修改」。您可以考慮監視該頁，以了解最新情況。</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>貳</p></td>
<td><div>
<p><big><strong>在本頁提交討論</strong></big></p>
</div>
<p>請按<strong></strong>，並遵照裏面的指示提交。</p></td>
</tr>
<tr class="even">
<td><p>-{zh-cn:叁;zh-tw:參;zh-hk:叁;zh-sg:叁}-</p></td>
<td><div>
<p><big><strong>通知相關聯者</strong></big></p>
</div>
<p>通知該頁面的作者，如有可能最好再通知近期經常編輯該頁的編者，請使用在他（們）的對話頁底留下相關訊息，提醒他（們）該頁面被列入存廢討論。</p></td>
</tr>
</tbody>
</table>

<noinclude></noinclude>

[Category:刪除模板](../Category/刪除模板.md "wikilink")