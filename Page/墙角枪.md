**墙角枪**（CornerShot）是一种应用于[巷战的特种](../Page/城鎮戰.md "wikilink")[武器](../Page/武器.md "wikilink")，由[以色列牆角射擊公司](../Page/以色列.md "wikilink")（Corner
Shot Holdings, LLC）設計。

## 歷史

其實早於[一戰](../Page/第一次世界大战.md "wikilink")、[二戰時期已經有墙角枪的意念](../Page/第二次世界大战.md "wikilink")。一戰時的[澳洲軍](../Page/澳大利亚.md "wikilink")、二戰時[納粹德國有製作了類似的組件](../Page/納粹德國.md "wikilink")，當中納粹德國製作的組件為[StG44突擊步槍專用](../Page/StG44突击步枪.md "wikilink")。

## 設計

墙角枪可以在墙角能够在不暴露自己身体的任何部分的情况下攻击另一边，但嚴格來說，墙角枪只是一個槍支的支架。

墙角枪分前后两部分，中间用活页相连，前面的部分包括[手枪和安装在原刺刀位置的彩色](../Page/手枪.md "wikilink")[摄像头](../Page/摄像头.md "wikilink")，可以左右转向63度，后部主要是枪托、扳机、2.5英寸[液晶屏监视器](../Page/液晶屏.md "wikilink")、[戰術導軌和电池](../Page/戰術導軌.md "wikilink")（可以提供枪支150分钟的工作时间）；前部的手枪可以旋转63度攻击墙角另一边的敌人，前部的摄像头将信号传递到后半部的液晶屏幕，枪手可以进行观察和瞄准，另外摄像头下瑞亦有[電筒以應付黑暗環境](../Page/手電筒.md "wikilink")。

墙角枪的标准型配置的是一般而不同口徑的[半自动手枪](../Page/半自动手枪.md "wikilink")（如貝瑞塔系列、格洛克系列或92式、CZ系列、Five-seveN系列、SIG
Sauer系列等），也可以裝配[轉輪手枪](../Page/轉輪手枪.md "wikilink")（目前只限中國的216兵工廠型號）。改装型可以用来发射40毫米[榴弹發射器及短](../Page/榴弹發射器.md "wikilink")[突击步枪或](../Page/突击步枪.md "wikilink")[卡賓槍](../Page/卡宾枪.md "wikilink")（如[AK系列](../Page/AK-47突击步枪.md "wikilink")、[M16系列等](../Page/M16突擊步槍.md "wikilink")），甚至[Panzerfaust火箭筒](../Page/反坦克榴弹发射器.md "wikilink")。

墙角枪手枪型的有效射程有100米，若使用5.7×28毫米口徑手槍的話則可以達到200米。

## 型號

  - **手枪型**（CSM）—[半自动手枪](../Page/半自動手槍.md "wikilink")
  - **步槍型**（APR）－短[突击步枪或](../Page/突击步枪.md "wikilink")[卡賓槍](../Page/卡宾枪.md "wikilink")
  - **榴弹發射器型**－所有40毫米[榴弹發射器](../Page/榴弹发射器.md "wikilink")
  - **反坦克火箭型**（CSP）

## 類似的武器

[中國](../Page/中國.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[印度及](../Page/印度.md "wikilink")[南韓有製造類似墙角枪的武器](../Page/大韩民国.md "wikilink")，牆角射擊公司也有和[法國的Omega公司合作研發OMEGA](../Page/法国.md "wikilink")-Cornershot，中國的216兵工廠更製造了裝配[轉輪手枪的墙角枪](../Page/左轮手枪.md "wikilink")。

  - [中國](../Page/中國.md "wikilink")：HD-66、CF-06、CS/LW9型拐弯枪
  - [法國](../Page/法国.md "wikilink")： OMEGA-Cornershot
  - [巴基斯坦](../Page/巴基斯坦.md "wikilink")：

## 採購

原廠製造的墙角枪由於因為過高的價格，目前僅有少部分國家使用，主要用於反[恐及營救](../Page/恐怖主义.md "wikilink")[人质](../Page/人质.md "wikilink")；而大多數的國家所採用的墙角枪，皆為類似但自主研發的拐角射擊裝置。

## 使用國

  -   - [特種部隊單位](../Page/特種部隊.md "wikilink")\[1\]

  -   - [中國人民武裝警察部隊雪豹突擊隊](../Page/中國人民武裝警察部隊雪豹突擊隊.md "wikilink")
      - [中國人民武裝警察部隊特種警察學院](../Page/中國人民武裝警察部隊特種警察學院.md "wikilink")
          - [北京市](../Page/北京市.md "wikilink")[公安特警](../Page/公安特警.md "wikilink")\[2\]（採用本土仿製品）
          - [東莞市](../Page/東莞市.md "wikilink")[公安特警](../Page/公安特警.md "wikilink")（採用本土仿製品）
          - [成都市](../Page/成都市.md "wikilink")[公安特警](../Page/公安特警.md "wikilink")（採用本土仿製品）
          - [鹽城市](../Page/鹽城市.md "wikilink")[公安特警](../Page/公安特警.md "wikilink")（採用本土仿製品）

  -   - [印度陸軍](../Page/印度陸軍.md "wikilink")

      - [印度海軍](../Page/印度海軍.md "wikilink")[海軍陸戰隊](../Page/海軍陸戰隊.md "wikilink")

      -
      - [德里](../Page/德里.md "wikilink")[特種警察](../Page/特種警察.md "wikilink")

      -
      - 某些[警察單位](../Page/警察.md "wikilink")（採用本土仿製品）

  -   - [特種部隊單位](../Page/特種部隊.md "wikilink")

  - ：採用本土仿製品

  -   - [以色列國境警察特勤隊](../Page/以色列國境警察特勤隊.md "wikilink")

  -   - [卡賓槍騎兵](../Page/卡賓槍騎兵.md "wikilink")[特別干預組](../Page/特別干預組_\(意大利\).md "wikilink")

  -   - [特種警察單位](../Page/特種警察.md "wikilink")\[3\]

  -   - （採用）

  -   - [特種部隊單位](../Page/特種部隊.md "wikilink")

  -   - [俄羅斯聯邦內務部](../Page/俄羅斯聯邦內務部.md "wikilink")
      - [聯邦安全局](../Page/聯邦安全局.md "wikilink")[特種部隊單位](../Page/特種部隊.md "wikilink")

  -   - [大韓民國第707特殊任務營](../Page/大韓民國第707特殊任務營.md "wikilink")
      - [特種警察單位](../Page/特種警察.md "wikilink")

  -   - 部份地方警察局的[特種武器和戰術部隊](../Page/特種武器和戰術部隊.md "wikilink")

  -   - [警察單位](../Page/警察.md "wikilink")

## 流行文化

### 電影

  - 2008年－《[刺客聯盟](../Page/刺客聯盟.md "wikilink")》（Wanted）：被火狐在[超市與克洛斯交火時所使用](../Page/超級市場.md "wikilink")。（裝上的槍械為[Safari
    Arms Matchmaster](../Page/M1911.md "wikilink")）
  - 2014年—《[-{zh-hk:轟天猛將3; zh-tw:浴血任務3;
    zh-cn:敢死队3;}-](../Page/敢死队3.md "wikilink")》（The
    Expendables 3）：被「火星」所使用。（裝上的槍械為[M4A1](../Page/M4卡賓槍.md "wikilink")）
  - 2016年—《[湄公河行動](../Page/湄公河行动.md "wikilink")》（Operation
    Mekong）：為中國生產的版本，被郭冰所使用。（裝上的槍械為[白朗寧大威力手槍](../Page/白朗寧大威力手槍.md "wikilink")）

## 参见

  - [墙角射击公司](../Page/墙角射击公司.md "wikilink")（Corner Shot Holdings, LLC）
  - [反恐战争](../Page/反恐战争.md "wikilink")
  - [以色列](../Page/以色列.md "wikilink")
  - [反坦克榴弹发射器](../Page/反坦克榴弹发射器.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - —[Corner Shot官方網頁](http://www.cornershot.com/)

  - —[D Boy Gun World—Corner
    Shot拐角射击系统](http://firearmsworld.net/isreal/cornershot/corner.htm)

[Category:手枪](../Category/手枪.md "wikilink")
[Category:步枪](../Category/步枪.md "wikilink")
[Category:榴弹发射器](../Category/榴弹发射器.md "wikilink")
[Category:反坦克火箭筒](../Category/反坦克火箭筒.md "wikilink")
[Category:以色列槍械](../Category/以色列槍械.md "wikilink")

1.
2.
3.