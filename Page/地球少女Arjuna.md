《**地球少女Arjuna**》是一套[日本的環保](../Page/日本.md "wikilink")[動畫](../Page/動畫.md "wikilink")，由[Bandai
Visual制作](../Page/Bandai_Visual.md "wikilink")。講述地球失衡之後世界的變化。共有六個部份13集。有[DVD發行](../Page/DVD.md "wikilink")。已在日本、香港和台灣上映。

## 登場人物

  - 有吉樹奈：[東山麻美](../Page/東山麻美.md "wikilink")
  - 大島時夫： [關智一](../Page/關智一.md "wikilink")
  - 白河小百合： [久川綾](../Page/久川綾.md "wikilink")
  - クリス・ホーケン：[上田祐司](../Page/うえだゆうじ.md "wikilink")
  - シンディ・クライン：[新谷真弓](../Page/新谷真弓.md "wikilink")
  - テレサ・ウォン：[沢海陽子](../Page/沢海陽子.md "wikilink")
  - 鬼塚指令：[玄田哲章](../Page/玄田哲章.md "wikilink")

## 製作人員

  - 原作、系列構成、監督：[河森正治](../Page/河森正治.md "wikilink")
  - 副監督：佐藤英一
  - 角色設定：[岸田隆宏](../Page/岸田隆宏.md "wikilink")
  - 音樂：[菅野洋子](../Page/菅野洋子.md "wikilink")
  - 動畫製作：[SATELIGHT](../Page/SATELIGHT.md "wikilink")
  - 主題曲：「マメシバ」
      - 作曲、編曲：[菅野洋子](../Page/菅野洋子.md "wikilink")
      - 作詞、歌：[坂本真绫](../Page/坂本真绫.md "wikilink")
  - 插入曲：「バイク」
      - 作曲、編曲：[菅野洋子](../Page/菅野洋子.md "wikilink")
      - 作詞、歌：[坂本真绫](../Page/坂本真绫.md "wikilink")

## 外部連結

  - [官方網頁](http://www.bandaivisual.co.jp/arjuna/)
  - [台版代理普威爾](http://www.prowaremedia.com.tw/shop/default.php?cPath=500_132)

[Category:2001年日本電視動畫](../Category/2001年日本電視動畫.md "wikilink")
[Category:東京電視網動畫](../Category/東京電視網動畫.md "wikilink")
[Category:环境保护题材作品](../Category/环境保护题材作品.md "wikilink")
[Category:兵庫縣背景作品](../Category/兵庫縣背景作品.md "wikilink")
[Category:日本原創電視動畫](../Category/日本原創電視動畫.md "wikilink")
[Category:SATELIGHT](../Category/SATELIGHT.md "wikilink")