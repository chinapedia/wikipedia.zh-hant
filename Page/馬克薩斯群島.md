**馬克薩斯群島**（），又称**馬貴斯群島**，是[法屬玻里尼西亞一部分](../Page/法屬玻里尼西亞.md "wikilink")，人口有8000餘人。[法國在](../Page/法國.md "wikilink")19世紀中將此地併入領土。[塔希提岛在其东北方约](../Page/塔希提岛.md "wikilink")1500km的海域。最高点为[瓦普岛上的](../Page/瓦普岛.md "wikilink")，海拔1230m。中心为[希瓦瓦岛及](../Page/希瓦瓦岛.md "wikilink")[努库希瓦岛](../Page/努库希瓦岛.md "wikilink")。

经济主要依靠出口[椰子核](../Page/椰子核.md "wikilink")、[香草及](../Page/香草.md "wikilink")[烟草](../Page/烟草.md "wikilink")。

1595年西班牙人首次发现该岛，并以当时的[秘鲁总督](../Page/秘鲁总督.md "wikilink")[卡涅特侯爵的称号](../Page/卡涅特.md "wikilink")（）命名。1842年成为法属领地。

画家[高更病逝于群岛中的](../Page/高更.md "wikilink")[希瓦瓦岛上](../Page/希瓦瓦岛.md "wikilink")。[赫尔曼·梅尔维尔的小说](../Page/赫尔曼·梅尔维尔.md "wikilink")《[泰皮](../Page/泰皮.md "wikilink")》（Typee）以[努库希瓦岛为背景](../Page/努库希瓦岛.md "wikilink")。

## 地理

[Flag_of_Marquesas_Islands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Marquesas_Islands.svg "fig:Flag_of_Marquesas_Islands.svg")
[Marquesas-administrative.png](https://zh.wikipedia.org/wiki/File:Marquesas-administrative.png "fig:Marquesas-administrative.png")
馬克薩斯群島距[夏威夷](../Page/夏威夷.md "wikilink")3500公里，由七個主島和少數島礁構成，皆由一座火山的多個原始火山口噴發出來。這些島嶼大致可分為西北和東南兩大群，在地理特徵及生物物種分佈皆有其獨特之處。這兩組分別是：

  - groupe Nord-Ouest: Nuku Hiva. Ua Huka et Ua Pou et les ilots Hati,
    Eiao et Hatutu;
  - groupe Sud-Est: Fatu Hiva, Hiva Oa, Tahuata, Motane et les ilots
    Thomasset et Fatu Huku.

### 行政区划

该群岛包含6个市镇：

  - 東南組：[希瓦瓦](../Page/希瓦瓦.md "wikilink")（Hiva
    Oa）、[法图伊瓦](../Page/法图伊瓦.md "wikilink")（Fatu
    Hiva）、[塔瓦塔](../Page/塔瓦塔.md "wikilink")（Tahuata）
  - 西北組：[努库希瓦](../Page/努库希瓦.md "wikilink")（Nuku
    Hiva）、[瓦胡卡](../Page/瓦胡卡.md "wikilink")（Ua
    Huka）、[瓦普](../Page/瓦普_\(马克萨斯群岛\).md "wikilink")（Ua Pou）

[Nuku_Hiva.jpg](https://zh.wikipedia.org/wiki/File:Nuku_Hiva.jpg "fig:Nuku_Hiva.jpg")

### 物種

。

## 参考文献

## 參見

  -
## 外部連結

  - [Official site (Tahiti Tourism board)](http://www.TahitiTourism.com)
  - [The Marquesas Online](http://www.marquises.pf/2index.htm)

[Category:法属波利尼西亚群岛](../Category/法属波利尼西亚群岛.md "wikilink")
[\*](../Category/马克萨斯群岛.md "wikilink")