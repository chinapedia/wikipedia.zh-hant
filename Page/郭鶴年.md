**郭鶴年**，籍貫[福建省](../Page/福建省.md "wikilink")[福州市](../Page/福州市.md "wikilink")[倉山區](../Page/倉山區.md "wikilink")[蓋山鎮](../Page/蓋山鎮.md "wikilink")[郭宅村](../Page/郭宅村.md "wikilink")，[馬來西亞企業家](../Page/馬來西亞.md "wikilink")，出生於[馬來西亞](../Page/馬來西亞.md "wikilink")[柔佛州](../Page/柔佛州.md "wikilink")[新山](../Page/新山.md "wikilink")，以经营白糖业起家，有“亚洲糖王”之称。被[富比士列為馬來西亞首富](../Page/富比士.md "wikilink")，華人富豪榜第四位，旗下資產最大部份都在[香港](../Page/香港.md "wikilink")，包括大量的香港豪宅、商場、酒店、辦公室、[香格里拉酒店集團等等](../Page/香格里拉酒店集團.md "wikilink")。郭鶴年在世界百富排名中名列40，他長居香港，而且積極開拓在中國大陸的企業版圖。\[1\]

## 生平

郭鶴年出生於[柔佛](../Page/柔佛.md "wikilink")[新山](../Page/新山.md "wikilink")，籍貫中國[福建省](../Page/福建省.md "wikilink")[福州市](../Page/福州市.md "wikilink")[蓋山镇](../Page/蓋山镇.md "wikilink")。其父[郭欽鑑從中國福建前往馬來亞南端謀生](../Page/郭欽鑑.md "wikilink")，曾當過店員，開過[咖啡店](../Page/咖啡.md "wikilink")，其後創辦[東昇公司](../Page/東昇公司.md "wikilink")，轉營米糧及糖的生意。

2011年的資產為170億美元，在馬來西亞、[香港及華人地區都擁有大量資產](../Page/香港.md "wikilink")，如不動產、商場、飯店、商務大樓與[南華早報集團](../Page/南華早報集團.md "wikilink")。2011年馬來西亞首富，華人富豪榜第四位。

郭鶴年在新山完成中學課程，1941年進入[新加坡](../Page/新加坡.md "wikilink")[萊佛士書院繼續學業](../Page/萊佛士書院.md "wikilink")。1942年至1945年二戰時期，任職於[三菱公司新山分行米糧部](../Page/三菱公司.md "wikilink")。不久就以二十歲的年紀升為米糧部經理，戰後到東昇公司協助其父管理業務。1947年，成立力克務公司，專營雜貨和船務等，至1965年易名為郭兄弟（新）有限公司。其父於1948年逝世，郭家分家，其家族籌組一間名為郭兄弟（馬）有限公司，繼續經營白糖及米糧業務，並由郭鶴年主理。

1950年代，郭氏遠赴[英國](../Page/英國.md "wikilink")，其後返回馬來西亞成立民天有限公司，並接受兩名日本人合資在1959年設立馬來亞糖廠，同時設立馬來亞三夾板有限公司。1961年，郭氏全力投資[煉糖工業](../Page/煉糖.md "wikilink")，搶在世界糖價上漲之前，從[印度輸入廉價](../Page/印度.md "wikilink")[白糖傾銷大馬](../Page/白糖.md "wikilink")，並投巨資於白糖期貨貿易。隨著糖價的上漲，一舉購進60萬馬元，震驚了國際糖交易市場。郭鶴年的郭氏兄弟公司不但掌握了大馬糖業市場的80%，壟斷馬來西亞市場，且通過多邊貿易，每年控制的食糖總量達150萬噸左右，約佔當時國際糖業市場的10%，贏得「亞洲糖王」之名。

1963年進軍[香港](../Page/香港.md "wikilink")，成立萬通有限公司，與[中國通商](../Page/中華人民共和國.md "wikilink")。1971年，與友人在新加坡創立[香格里拉酒店](../Page/香格里拉酒店.md "wikilink")。1977年，開始在香港投資[地產及](../Page/地產.md "wikilink")[酒店](../Page/酒店.md "wikilink")，在[尖東購入地皮興建香格里拉酒店](../Page/尖東.md "wikilink")。1993年，郭鶴年以[嘉里公司之名義向](../Page/嘉里公司.md "wikilink")[新聞集團購入](../Page/新聞集團.md "wikilink")34.9%的[南華早報股權](../Page/南華早報.md "wikilink")，三年後又透過南華早報收購無綫電視附屬之[TVE公司](../Page/電視廣播企業.md "wikilink")，在香港傳媒行業占據一角。除了南華早報外，旗下的[嘉里物流現時亦有在港上市](../Page/嘉里物流.md "wikilink")。2016年郭氏將南華早報售予阿里巴巴集團。

2018年5月，[马来西亚首次政党轮替之后](../Page/2018年马来西亚大选.md "wikilink")，获首相[马哈迪邀请担任](../Page/马哈迪.md "wikilink")[内阁顾问](../Page/第七次马哈迪内阁.md "wikilink")，成为资政理事会的5名成员之一。

## 家庭

母親鄭格如，郭鶴年有兩名兄長，大哥[郭鶴舉](../Page/郭鶴舉.md "wikilink")，二哥[郭鶴齡](../Page/郭鶴齡.md "wikilink")，前亡妻謝碧蓉與現妻[何寶蓮](../Page/何寶蓮.md "wikilink")。

大哥郭鶴舉（）是馬來西亞的外交家，曾獲國家派駐[德國](../Page/德國.md "wikilink")、[南斯拉夫](../Page/南斯拉夫.md "wikilink")、[荷蘭](../Page/荷蘭.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[盧森堡及](../Page/盧森堡.md "wikilink")[丹麥的大使](../Page/丹麥.md "wikilink")。郭鶴舉已婚，與太太育有二子二女；二哥郭鶴齡是[馬來亞共產黨成員](../Page/馬來亞共產黨.md "wikilink")，1952年[馬來亞緊急狀態時被殺](../Page/馬來亞緊急狀態.md "wikilink")。\[2\]\[3\]

郭鶴年兩段婚姻為他帶來八名子女\[4\]。

## 参考文献

## 外部链接

  - [亞洲糖王郭鶴年](http://big5.huaxia.com/sw/szjy/2008/00810140.html)
  - [大马20大富豪](http://www.malaysiaeconomy.net/business/my_people/2010-02-17/4098.html)

[Category:嘉里集團](../Category/嘉里集團.md "wikilink")
[Category:馬來西亞企業家](../Category/馬來西亞企業家.md "wikilink")
[Category:馬來西亞億萬富豪](../Category/馬來西亞億萬富豪.md "wikilink")
[Category:福州裔馬來西亞人](../Category/福州裔馬來西亞人.md "wikilink")
[Category:新山人](../Category/新山人.md "wikilink")
[H鶴](../Category/郭姓.md "wikilink")
[Category:郭鶴年家族](../Category/郭鶴年家族.md "wikilink")
[Category:福建企业家](../Category/福建企业家.md "wikilink")

1.  [世界百富排名 郭鹤年540亿名列40](http://www.nanyang.com/node/502177?tid=460)
    ．[南洋商报](../Page/南洋商报.md "wikilink")，2013，1(4)

2.

3.

4.