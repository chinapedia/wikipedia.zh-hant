**何超儀**（，），藝名**何超**，[香港女](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")，[電影](../Page/電影.md "wikilink")[演員](../Page/演員.md "wikilink")，著名澳門賭王[何鴻燊和二太太](../Page/何鴻燊.md "wikilink")[藍瓊瓔所生的幼女](../Page/藍瓊瓔.md "wikilink")，其胞姐是[何超瓊](../Page/何超瓊.md "wikilink")、[何超鳳](../Page/何超鳳.md "wikilink")、[何超蕸](../Page/何超蕸.md "wikilink")，其胞弟是[何猷龙](../Page/何猷龙.md "wikilink")。

## 生平

18歲簽約滾石唱片公司，先以歌手身份出道，但她一直無法進入主流，始終未留下令人印象深刻的歌唱作品。\[1\]

1995年在陳勳奇執導爱情《没有老公的日子》中飾演黑仔出道。\[2\]

1999年憑借《紫雨風暴》入圍[第36届台北金馬獎展最佳女配角](../Page/第36届台北金馬獎.md "wikilink")。\[3\]

2003年在電影《[豪情](../Page/豪情.md "wikilink")》中飾演風情萬種的口技女王「趙啷啷」，奪得第23屆[香港電影金像獎最佳](../Page/香港電影金像獎.md "wikilink")[女配角獎](../Page/女配角.md "wikilink")。

2004年，她主演同性戀題材電影《[蝴蝶](../Page/蝴蝶_\(2004年電影\).md "wikilink")》，在[威尼斯電影節首映](../Page/威尼斯電影節.md "wikilink")，並獲提名角逐「國際影評人協會獎」及「人道精神獎」。她亦在獨立短片《太太》中，演繹一個生活沉悶的富豪妻子。

2006年10月發行個人專輯《地獄廚房》。\[4\]

2008年4月發行個人國語專輯《Elastic Rock》。\[5\]

2010年正式成立Josie & The Uni
Boys([何超與海膽仔](../Page/何超與海膽仔.md "wikilink"))樂隊，她的強烈個人風格，就如同她一向給人的螢幕形象一樣，展示對搖滾音樂堅持有自我前衛新潮的態度。

2010年何超儀憑藉電影《維多利亞壹號》在第43屆西班牙錫切斯電影節獲得的「最佳女主角獎」。\[6\]

2014年1月，何超儀獲[無綫電視邀請拍攝新劇](../Page/無綫電視.md "wikilink")《[再戰明天](../Page/再戰明天.md "wikilink")》\[7\]，並憑此劇獲頒[萬千星輝頒獎典禮](../Page/萬千星輝頒獎典禮.md "wikilink")「最佳女配角」殊榮。

2015年2月，[何超與海膽仔樂隊在香港九龍灣國際貿易中心Music](../Page/何超與海膽仔.md "wikilink")
Zone舉辦《摇滾妹子》演唱会 。\[8\] 3月，参加2015腾讯音乐盛典。\[9\]

2017年，何超儀憑藉電影《無限春光27》在的馬來西亞國際電影人年展暨頒獎典禮上獲頒「最佳女配角獎」。\[10\]

2018年10月，何超儀出席西班牙錫切斯奇幻電影節(SITGES)並擔任評審。\[11\]。大會向超儀頒發Time
Machine大獎，表揚她多年來對奇幻電影的貢獻。\[12\]

## 私人生活

2003年11月，何超儀與[陳子聰在](../Page/陳子聰.md "wikilink")[澳洲註冊結婚](../Page/澳洲.md "wikilink")。2016年8月，[陳子聰由於肝腸病入院](../Page/陳子聰.md "wikilink")，當時，夫妻的好友[徐濠縈](../Page/徐濠縈.md "wikilink")，在社交網上載與兩人的合照，令到[陳子聰暴瘦憔悴的病容曝光](../Page/陳子聰.md "wikilink")，惹到**何超儀**公開指責徐濠縈有「分享症」，兩姊妹感情亦因此而破裂。\[13\]

## 作品

### 唱片

  - 《造反》
  - 《[何家淑女](../Page/何家淑女.md "wikilink")》
  - 《[說謊](../Page/說謊_\(何超儀專輯\).md "wikilink")》
  - 《[Sickid](../Page/Sickid.md "wikilink")》
  - 《[地獄廚房](../Page/地獄廚房_\(專輯\).md "wikilink")》
  - 《[Elastic Rock](../Page/Elastic_Rock.md "wikilink")》
  - 《天眼》（以Josie & the Uni Boys名義推出）
  - 《[搖滾妹子](../Page/搖滾妹子.md "wikilink")》（以Josie & the Uni Boys名義推出）
  - 《何飄移》（7吋黑膠唱片）（以Josie & the Uni Boys名義推出）

### 电视剧

  - 1996年 《[O记实录II](../Page/O记实录II.md "wikilink")》飾 方小敏（Toby）
  - 1997年 《[迷离档案](../Page/迷离档案.md "wikilink")》飾 鄭雅兒
  - 1997年 《[香港人在广州](../Page/香港人在广州.md "wikilink")》飾 余穎金
  - 1997年 《[东方母亲](../Page/东方母亲.md "wikilink")》飾 施小芳
  - 2000年 《[創世紀II天地有情](../Page/创世纪_\(电视剧\).md "wikilink")》飾 寶　儀
  - 2014年 《**[再戰明天](../Page/再戰明天.md "wikilink")**》飾 **丁好好**
  - 2019年《**天仙局**》飾 **郭美儀**

### 電影

| 年份                                                       | 中文片名                                                                                               | 角色         |
| -------------------------------------------------------- | -------------------------------------------------------------------------------------------------- | ---------- |
| 1994                                                     | [青春火花](../Page/青春火花.md "wikilink")                                                                 |            |
| 1995                                                     | [没有老公的日子](../Page/没有老公的日子.md "wikilink")                                                           | 黑仔         |
| [现代蛊惑仔](../Page/现代蛊惑仔.md "wikilink")                     |                                                                                                    |            |
| 1996                                                     | [天涯海角](../Page/天涯海角.md "wikilink")                                                                 |            |
| [四个32A和一个香蕉少年](../Page/四个32A和一个香蕉少年.md "wikilink")       | Minnie大姐                                                                                           |            |
| 1997                                                     | [97家有囍事](../Page/97家有囍事.md "wikilink")                                                             | 三兒子女友      |
| [中国匣](../Page/中国匣.md "wikilink")                         |                                                                                                    |            |
| 1998                                                     | [戀戀瓊瑤](../Page/戀戀瓊瑤.md "wikilink")                                                                 |            |
| [想見你](../Page/想見你.md "wikilink")                         |                                                                                                    |            |
| [安娜瑪德蓮娜](../Page/安娜馬德蓮娜.md "wikilink")                   | Josie Ho                                                                                           |            |
| 1999                                                     | [紫雨風暴](../Page/紫雨風暴.md "wikilink")                                                                 | 觀艾         |
| [無問旅程](../Page/無問旅程.md "wikilink")                       |                                                                                                    |            |
| 2000                                                     | [古惑女2](../Page/古惑女2.md "wikilink")                                                                 |            |
| [友情岁月山鸡故事](../Page/友情岁月山鸡故事.md "wikilink")               |                                                                                                    |            |
| [Bad Boy特工](../Page/Bad_Boy特工.md "wikilink")             |                                                                                                    |            |
| 2001                                                     | [敵對](../Page/敵對.md "wikilink")                                                                     |            |
| [地久天長](../Page/地久天長.md "wikilink")                       |                                                                                                    |            |
| [恐怖热线之大头怪婴](../Page/恐怖热线之大头怪婴.md "wikilink")             |                                                                                                    |            |
| [欲望之城](../Page/欲望之城.md "wikilink")                       |                                                                                                    |            |
| [飞哥传奇](../Page/飞哥传奇.md "wikilink")                       |                                                                                                    |            |
| 2002                                                     | [悭钱家族](../Page/悭钱家族.md "wikilink")                                                                 | Mrs. Lai   |
| [当男人变成女人](../Page/当男人变成女人.md "wikilink")                 |                                                                                                    |            |
| 2003                                                     | [1：99电影行动](../Page/1：99电影行动.md "wikilink")                                                         |            |
| [行运超人](../Page/行运超人.md "wikilink")                       |                                                                                                    |            |
| [豪情](../Page/豪情.md "wikilink")                           | 趙啷啷                                                                                                |            |
| [千機變](../Page/千機變.md "wikilink")                         |                                                                                                    |            |
| 2004                                                     | [六壮士](../Page/六壮士.md "wikilink")                                                                   |            |
| [蝴蝶](../Page/蝴蝶_\(2004年電影\).md "wikilink")               |                                                                                                    |            |
| [太陽無知](../Page/太陽無知.md "wikilink")                       |                                                                                                    |            |
| [太太](../Page/太太.md "wikilink")                           |                                                                                                    |            |
| 2005                                                     | [精武家庭](../Page/精武家庭.md "wikilink")                                                                 |            |
| 2006                                                     | [放·逐](../Page/放·逐.md "wikilink")                                                                   |            |
| [伊莎貝拉](../Page/伊莎贝拉_\(电影\).md "wikilink")                |                                                                                                    |            |
| [大丈夫2](../Page/大丈夫2.md "wikilink")                       |                                                                                                    |            |
| [四大天王](../Page/四大天王.md "wikilink")                       |                                                                                                    |            |
| [春田花花同學會](../Page/春田花花同學會.md "wikilink")                 |                                                                                                    |            |
| 2007                                                     | [戰‧鼓](../Page/戰‧鼓.md "wikilink")                                                                   |            |
| [戲王之王](../Page/戲王之王.md "wikilink")                       |                                                                                                    |            |
| 2008                                                     | [十分鍾情](../Page/十分鍾情.md "wikilink")                                                                 |            |
| 2009                                                     | [Street Fighter: The Legend of Chun-L](../Page/Street_Fighter:_The_Legend_of_Chun-L.md "wikilink") |            |
| [殺人犯](../Page/殺人犯_\(电影\).md "wikilink")                  | 凌敏（Minnie）                                                                                         |            |
| [撲克王](../Page/撲克王.md "wikilink")                         | 況小姐                                                                                                |            |
| 2010                                                     | [維多利亞壹號](../Page/維多利亞壹號.md "wikilink")                                                             | 鄭麗嫦        |
| 2012                                                     | [寒戰](../Page/寒戰.md "wikilink")                                                                     | （客串）       |
| [車手](../Page/車手_\(電影\).md "wikilink")                    | Madam蕙                                                                                             |            |
| 2013                                                     | [開棺/天坑墳地](../Page/開棺/天坑墳地.md "wikilink")                                                           | Brown Eyes |
| 2014                                                     | [诡镇](../Page/诡镇.md "wikilink")                                                                     |            |
| [豪情3D](../Page/豪情3D.md "wikilink")                       | 嶋山初代子                                                                                              |            |
| [第七謊言](../Page/第七謊言.md "wikilink")                       |                                                                                                    |            |
| 2015                                                     | [全力扣殺](../Page/全力扣殺.md "wikilink")                                                                 | 吳久秀        |
| 2016                                                     | [惡人谷](../Page/惡人谷_\(電影\).md "wikilink")                                                            | Gigi       |
| [無限春光27](../Page/無限春光27.md "wikilink")（英文：*In the Room*） |                                                                                                    |            |

### 舞台剧

  - <恋恋琼瑶>
  - <恋恋之师>
  - <男式之情人再见>

## 派台歌曲成績

| **派台歌曲成績**                                         |
| -------------------------------------------------- |
| 唱片                                                 |
| **1996年**                                          |
| [何家淑女](../Page/何家淑女.md "wikilink")                 |
| 何家淑女                                               |
| 何家淑女                                               |
| [我們不哭了](../Page/我們不哭了.md "wikilink")               |
| **1997年**                                          |
| [說謊](../Page/說謊_\(何超儀專輯\).md "wikilink")           |
| 說謊                                                 |
| 說謊                                                 |
| 說謊                                                 |
| **2004年**                                          |
| [Sickid](../Page/Sickid.md "wikilink")             |
| **2006年**                                          |
| [地獄廚房](../Page/地獄廚房_\(專輯\).md "wikilink")          |
| **2008年**                                          |
| [Elastic Rock](../Page/Elastic_Rock.md "wikilink") |
|                                                    |
| **2015年**                                          |
|                                                    |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

  - 仍上榜（\*）

## 音樂殊榮

  - 1996年[1996年度十大勁歌金曲](../Page/1996年度十大勁歌金曲.md "wikilink") - 最受欢迎新人奖
  - 1996年[1996年度新城勁爆頒獎禮](../Page/1996年度新城勁爆頒獎禮.md "wikilink") - 新登場女歌手

## 電影殊榮

  - 2002年[第七屆香港電影金紫荊獎](../Page/第七屆香港電影金紫荊獎.md "wikilink") - 最佳女配角《地久天長》
  - 2004年[第23屆香港電影金像獎](../Page/第23屆香港電影金像獎.md "wikilink") - 最佳女配角《豪情》
  - 2005年[第十屆香港電影金紫荊獎](../Page/第十屆香港電影金紫荊獎.md "wikilink") - 最佳女配角提名《蝴蝶》
  - 2010年[第四十三屆西班牙錫切斯國際電影節](../Page/第四十三屆西班牙錫切斯國際電影節.md "wikilink") -
    最佳女主角《維多利亞壹號》
  - 2011年[第三十屆香港電影金像獎](../Page/第三十屆香港電影金像獎.md "wikilink") -
    最佳女主角提名《維多利亞壹號》
  - 2014年[萬千星輝頒獎典禮2014](../Page/萬千星輝頒獎典禮2014.md "wikilink") -
    最佳女配角（[再戰明天](../Page/再戰明天.md "wikilink") - 丁好好）
  - 2017年[第一屆馬來西亞國際電影人年展](../Page/第一屆馬來西亞國際電影人年展.md "wikilink") -
    最佳女配角《無限春光27》

## 註釋

## 外部連結

  - [官方網頁](http://www.josieho.com/)

  - [何超儀 蝴蝶訪問, 輸入"3"字, sound &
    video](http://www.pinkwork.com/theatre/entrance.htm)

  - [何超儀訪問(sound & video)](http://www.pinkwork.com/chiuyee/ho2.htm)

  - [林燕妮訪問何超儀 (南方报业网) -
    "何超仪：香港女演员很惨，所以我成立了自己的电影公司"](https://web.archive.org/web/20100504171653/http://nf.nfdaily.cn/nfdsb/content/2010-04/29/content_11509353.htm)

  - [何超的博客-我的藝術家空間](http://www.alivenotdead.com/josieho)

  - [所屬經理人公司](http://www.cmd.com.hk)

  -
  - [YouTube:香港經典廣告-1993年 - 護舒寶普通裝
    (何超儀)](https://www.youtube.com/watch?v=IXiyY-gJJ-w)

## 參看

  - [何東家族](../Page/何東家族.md "wikilink")

<center>

</center>

[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:何東家族](../Category/何東家族.md "wikilink")
[Chiu](../Category/何姓.md "wikilink")
[Category:寶安人](../Category/寶安人.md "wikilink")
[Category:何鴻燊家族](../Category/何鴻燊家族.md "wikilink")
[Category:香港猶太人](../Category/香港猶太人.md "wikilink")
[Category:香港電影金紫荊獎最佳女配角得主](../Category/香港電影金紫荊獎最佳女配角得主.md "wikilink")

1.  [何超仪 嚣张有因](https://www.iconmalaysia.com.my/cover-story/何超仪-嚣张有因/)
2.  [沒有老公的日子](http://hkmdb.com/db/movies/view.mhtml?id=8033&display_set=big5)
3.  [第36届台北金馬獎入圍名單](http://www.goldenhorse.org.tw/awards/nw/?serach_type=award&sc=10&search_regist_year=1999&ins=26)
4.  [何超儀《地獄廚房》](http://ent.163.com/06/1101/11/2URCNDUD00031NIQ.html)
5.  [何超儀《Elastic
    Roc》](http://ent.sina.com.cn/y/m/2008-05-14/17512026391.shtml)
6.  [跨界百變女王何超儀獲西班牙Sitges電影節成就獎](https://tw.news.yahoo.com/%E8%B7%A8%E7%95%8C%E7%99%BE%E8%AE%8A%E5%A5%B3%E7%8E%8B%E4%BD%95%E8%B6%85%E5%84%80%E7%8D%B2%E8%A5%BF%E7%8F%AD%E7%89%99stiges%E9%9B%BB%E5%BD%B1%E7%AF%80%E6%88%90%E5%B0%B1%E7%8D%8E-142732361.html)
7.  [《再战明天》饰演囚犯，与王浩信有感情线](http://www.ontvb.com/html/2014/new_0116/4297.html)
8.  [何超仪香港演唱会一票难求
    范晓萱助阵有惊喜](http://ent.163.com/15/0206/11/AHP429V000031H0O.html)
9.  [“文艺”PK“摇滚” QQ音乐盛典多元碰撞](http://ent.qq.com/a/20150320/066420.htm)
10. [跨界百變女王何超儀獲西班牙Sitges電影節成就獎](https://tw.news.yahoo.com/%E8%B7%A8%E7%95%8C%E7%99%BE%E8%AE%8A%E5%A5%B3%E7%8E%8B%E4%BD%95%E8%B6%85%E5%84%80%E7%8D%B2%E8%A5%BF%E7%8F%AD%E7%89%99stiges%E9%9B%BB%E5%BD%B1%E7%AF%80%E6%88%90%E5%B0%B1%E7%8D%8E-142732361.html)
11. [遠在西班牙電影節的香港紅地氈高手︰何超儀的偏鋒時尚](https://www.mings-fashion.com/%E7%B4%85%E5%9C%B0%E6%AF%AF-%E4%BD%95%E8%B6%85%E5%84%80-214189/)
12. [再度揚威西班牙電影節
    ﻿何超儀﻿﻿送「成就獎」冧老公﻿](http://hd.stheadline.com/life/ent/daily/708739/)
13. [超市狂掃貨 直擊何超儀谷肥陳子聰](https://eastweek.my-magazine.me/main/81970)