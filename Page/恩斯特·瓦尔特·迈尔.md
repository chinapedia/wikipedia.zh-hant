[Mayr.jpg](https://zh.wikipedia.org/wiki/File:Mayr.jpg "fig:Mayr.jpg")
**恩斯特·瓦尔特·迈尔**（，），20世紀最主要的演化生物學家之一，也是一位分類學家、熱帶探險家、[鳥類學家](../Page/鳥類學家.md "wikilink")、博物學家與科學史家。他原本在德國的[柏林博物館研究鳥類](../Page/柏林博物館.md "wikilink")，後來前往美國紐約的自然史博物館。1954年當選[美國國家科學院院士](../Page/美國國家科學院.md "wikilink")，1970年獲得國家科學獎章，他一生中共得33個獎項。

## 生平

1904年出生於[德國](../Page/德國.md "wikilink")[巴伐利亞的](../Page/巴伐利亞.md "wikilink")[肯普滕](../Page/肯普滕.md "wikilink")。

在美國的期間，他研究鳥類的分類，並描述了南太平洋的26個新種與455個新亞種。這些研究形成了生物學物種觀念（biological species
concept），他將物種定義為一個能夠互相交配且生下具生育力後代的群體。此外他也提出新的物種的形成理論，稱為[邊域性物種形成](../Page/邊域性物種形成.md "wikilink")（peripatric
speciation）。

1942年出版的書《動物學家的系統分類學與物種起源觀點》（Systematics and the Origin of Species from
the Viewpoint of a
Zoologist），是[演化論的](../Page/演化論.md "wikilink")[現代綜合理論建立根基之一](../Page/現代綜合理論.md "wikilink")。這種理論在1930年代左右成形，是一種將19世紀孟德爾遺傳學與達爾文演化論整合的理論。

1953年他到到哈佛大學的比較動物學博物館擔任館長，研究科學史與哲學。1975年退休之後，出版了許多書籍。直到99歲仍然出版了《什麼是演化？》（What
Evolution Is） (ISBN 0465044263)。

他結縭五十五年的妻子瑪格麗特·迈尔（Margarete Mayr），已在一九九○年去世。他們共留下兩個女兒、五個孫子與十個曾孫。他則享壽一百歲。

2005年，迈尔逝世於美國[麻塞諸塞的](../Page/麻塞諸塞.md "wikilink")[貝德福德](../Page/貝德福德.md "wikilink")（Bedford）。

## 參見

  - [演化](../Page/演化.md "wikilink")
  - [演化思想史](../Page/演化思想史.md "wikilink")
  - [現代綜合理論](../Page/現代綜合理論.md "wikilink")

## 外部連結

  - [Harvard Gazette: Ernst Mayr, giant among evolutionary biologists,
    dies
    at 100](https://web.archive.org/web/20080119034049/http://www.news.harvard.edu/gazette/daily/2005/02/04-mayr.html)
  - Sciscape新聞報導 - [生物：哈佛演化學巨擘Ernst
    Mayr逝世，享年一百](https://web.archive.org/web/20070927173023/http://www.sciscape.org/news_detail.php?news_id=1726)

[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:美國國家科學獎獲獎者](../Category/美國國家科學獎獲獎者.md "wikilink")
[Category:德国生物学家](../Category/德国生物学家.md "wikilink")
[Category:美国生物学家](../Category/美国生物学家.md "wikilink")
[Category:演化生物學家](../Category/演化生物學家.md "wikilink")
[Category:德國鳥類學家](../Category/德國鳥類學家.md "wikilink")
[Category:美國鳥類學家](../Category/美國鳥類學家.md "wikilink")
[Category:哈佛大学教师](../Category/哈佛大学教师.md "wikilink")
[Category:美國科學作家](../Category/美國科學作家.md "wikilink")
[Category:美國無神論者](../Category/美國無神論者.md "wikilink")
[Category:德國無神論者](../Category/德國無神論者.md "wikilink")
[Category:格賴夫斯瓦爾德大學校友](../Category/格賴夫斯瓦爾德大學校友.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:德國人瑞](../Category/德國人瑞.md "wikilink")
[Category:巴伐利亞人](../Category/巴伐利亞人.md "wikilink")
[Category:克拉福德奖获得者](../Category/克拉福德奖获得者.md "wikilink")