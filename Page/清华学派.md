**清华学派**是指20世纪20年代到50年代（[全国院系调整前](../Page/全国院系调整.md "wikilink")）以清华大学师生为主体形成的一个文史哲学派。由[王瑶于](../Page/王瑶.md "wikilink")1988年提出。该学派的主要学术特点是所谓的“会通”：会通古今，会通中西，会通文理。而维系命脉则是陈寅恪祭王国维文中所写的“独立之思想，自由之精神”。

清华学派始于1925年[清华学校研究院的创立](../Page/清华学校研究院.md "wikilink")。当时由[吴宓负责筹备](../Page/吴宓.md "wikilink")，并先后聘请[王国维](../Page/王国维.md "wikilink")、[梁启超](../Page/梁启超.md "wikilink")、[赵元任和](../Page/赵元任.md "wikilink")[陈寅恪四人为导师](../Page/陈寅恪.md "wikilink")，[李济为讲师](../Page/李济.md "wikilink")，[赵万里和](../Page/赵万里.md "wikilink")[浦江清为助理教授](../Page/浦江清.md "wikilink")。同年，[朱自清也从](../Page/朱自清.md "wikilink")[浙江两江师范学院来到清华中文系任教](../Page/浙江两江师范学院.md "wikilink")。

1928到1935年，陆续从外校延揽了大批著名教授，包括文学方面的[杨振声](../Page/杨振声_\(教育家\).md "wikilink")、[刘文典](../Page/刘文典.md "wikilink")、[俞平伯](../Page/俞平伯.md "wikilink")、[黄节](../Page/黄节.md "wikilink")、[闻一多等](../Page/闻一多.md "wikilink")；历史学的[蒋廷黻](../Page/蒋廷黻.md "wikilink")、[张荫麟](../Page/张荫麟.md "wikilink")、[雷海宗等](../Page/雷海宗.md "wikilink")；[哲学方面的](../Page/哲学.md "wikilink")[金岳霖](../Page/金岳霖.md "wikilink")、[冯友兰等](../Page/冯友兰.md "wikilink")；[社会学方面的](../Page/社会学.md "wikilink")[潘光旦](../Page/潘光旦.md "wikilink")、[陈达](../Page/陈达.md "wikilink")、[吴景超等](../Page/吴景超.md "wikilink")。他们构成了清华学派的二代中坚。

第三代以清华自身尤其是研究院培养的[王力](../Page/王力_\(语言学家\).md "wikilink")（赵元任弟子）、[吴其昌](../Page/吴其昌.md "wikilink")、[蒋天枢](../Page/蒋天枢.md "wikilink")、[刘节](../Page/刘节.md "wikilink")（以上为陈寅恪弟子）、[贺麟](../Page/贺麟.md "wikilink")、[陈铨](../Page/陈铨.md "wikilink")（以上为吴宓弟子）、[高亨](../Page/高亨.md "wikilink")、[罗根泽](../Page/罗根泽.md "wikilink")、[姜亮夫等为代表](../Page/姜亮夫.md "wikilink")。

第四代以30年代清华学生为主力，如文学院“四才子”[钱锺书](../Page/钱锺书.md "wikilink")、[夏鼐](../Page/夏鼐.md "wikilink")、[吴晗](../Page/吴晗.md "wikilink")、[朱湘](../Page/朱湘.md "wikilink")，“清华四剑客”[李长之](../Page/李长之.md "wikilink")、[林庚](../Page/林庚.md "wikilink")、[季羡林](../Page/季羡林.md "wikilink")、[吴组缃](../Page/吴组缃.md "wikilink")，以及[曹禺](../Page/曹禺.md "wikilink")、[杨绛](../Page/杨绛.md "wikilink")、[杨联陞](../Page/杨联陞.md "wikilink")、[费孝通](../Page/费孝通.md "wikilink")、王瑶、[冯契](../Page/冯契.md "wikilink")、[何炳棣等](../Page/何炳棣.md "wikilink")。

第五代多为联大时期的学生，包括[殷海光](../Page/殷海光.md "wikilink")、[王浩](../Page/王浩.md "wikilink")、[季镇淮](../Page/季镇淮.md "wikilink")、[任继愈](../Page/任继愈.md "wikilink")、[朱德熙](../Page/朱德熙.md "wikilink")、[李赋宁](../Page/李赋宁.md "wikilink")、[许国璋](../Page/许国璋.md "wikilink")、[杨周翰](../Page/杨周翰.md "wikilink")、[王佐良](../Page/王佐良.md "wikilink")、[卞之琳](../Page/卞之琳.md "wikilink")、[汪曾祺](../Page/汪曾祺.md "wikilink")、[穆旦](../Page/穆旦.md "wikilink")、[郑敏等](../Page/郑敏.md "wikilink")。

王瑶所提出的“清华学派”三特征为： 第一，既有国学根底又深切了解西方文化； 第二，既不是卫道士，又不是西方文化的搬运夫；
第三，有一大批中西会通的著作。

## 外部链接

  - [刘超“清华学派”及其终结](http://www.cuhk.edu.hk/ics/21c/supplem/essay/0503075g.htm)
  - [何兆武
    也谈“清华学派”](http://www.booker.com.cn/gb/paper18/36/class001800001/hwz191545.htm)

[category:清华大学](../Page/category:清华大学.md "wikilink")

[\*](../Category/清华大学学者.md "wikilink")