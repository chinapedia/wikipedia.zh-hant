[Taiwan_NASA_Terra_MODIS_23791.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_NASA_Terra_MODIS_23791.jpg "fig:Taiwan_NASA_Terra_MODIS_23791.jpg")\]\]
[台灣是位於](../Page/台灣.md "wikilink")[亞洲東部](../Page/亞洲.md "wikilink")、[太平洋西北側的一個](../Page/太平洋.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")。目前已知棲息於此並建立族群的[兩棲動物計有](../Page/两栖动物.md "wikilink")2目7科41種，其中17種為台灣特有種，還有[亞洲錦蛙](../Page/亞洲錦蛙.md "wikilink")、[斑腿樹蛙](../Page/斑腿樹蛙.md "wikilink")、[海蛙](../Page/海蛙.md "wikilink")，以及[美洲牛蛙等](../Page/美洲牛蛙.md "wikilink")4種確認在台灣建立族群的外來種\[1\]。台灣境內的[有尾目僅](../Page/有尾目.md "wikilink")1科5種，其餘全屬[無尾目物種](../Page/無尾目.md "wikilink")。目前最新發表的種類為2017年的[太田樹蛙](../Page/太田樹蛙.md "wikilink")\[2\]。

1861到1866年間，英國首任駐台領事[郇和](../Page/郇和.md "wikilink")（Robert
Swinhoe）利用職務之便，最早對於台灣的兩棲類動物進行研究，並發表了4種兩棲類。

## 物種列表

### [有尾目](../Page/有尾目.md "wikilink") Urodela

台灣的有尾目僅有山椒魚科一科，計有五種\[3\]，全部都為台灣特有種，且皆列名於行政院農業委員會《保育類野生動物名錄》的瀕臨絕種野生動物之列。其中[北大武山的阿里山山椒魚為全世界山椒魚科分布之最南界](../Page/北大武山.md "wikilink")\[4\]。

#### [山椒魚科](../Page/山椒魚科.md "wikilink")（Hynobiidae）

<table>
<caption>align="center" style="background:#9BC4E2; color:white" |<big><strong>山椒魚科（Hynobiidae）</strong></big> |- -</caption>
<thead>
<tr class="header">
<th><p>scope="col" style="background-color:Linen; color:Black" width=100 px|中文俗名</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width=150 px|學名</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width=550 px|分布</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width=90 px|保育狀態</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width=100 px|圖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>!scope="row" style="background:#ffffff" | <a href="../Page/阿里山山椒魚.md" title="wikilink">阿里山山椒魚</a></p></td>
<td><p><em>Hynobius arisanensis</em> <small>Maki, 1922</small></p></td>
<td><p>台灣特有種，主要分布於<a href="../Page/阿里山山脈.md" title="wikilink">阿里山及玉山山系</a>，往南達北大武山。主要分布於海拔1800-3650公尺間[5]。</p></td>
<td><p>[6]第 I 級[7]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>!scope="row" style="background:#ffffff" | <a href="../Page/台灣山椒魚.md" title="wikilink">台灣山椒魚</a></p></td>
<td><p><em>Hynobius formosanus</em> <small>Maki, 1922</small></p></td>
<td><p>台灣特有種，目前記錄於中央山脈中、北段（北起雲稜山莊，南至<a href="../Page/能高山.md" title="wikilink">能高山</a>）及<a href="../Page/雪山山脈.md" title="wikilink">雪山山脈南段</a>（最北至<a href="../Page/伊澤山.md" title="wikilink">伊澤山</a>）。分布於台灣2100至3000公尺之間的高山上[8]。</p></td>
<td><p>[9]第 I 級[10]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>!scope="row" style="background:#ffffff" | <a href="../Page/觀霧山椒魚.md" title="wikilink">觀霧山椒魚</a></p></td>
<td><p><em>Hynobius fuca</em> <small>Lai &amp; Lue, 2008</small></p></td>
<td><p>台灣特有種，分布於台灣<a href="../Page/雪山山脈.md" title="wikilink">雪山山脈西北部的山區</a>，海拔約1,500公尺左右[11]。</p></td>
<td><p>第 I 級[12]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>!scope="row" style="background:#ffffff" | <a href="../Page/南湖山椒魚.md" title="wikilink">南湖山椒魚</a></p></td>
<td><p><em>Hynobius glacialis</em> <small>Lai &amp; Lue, 2008</small></p></td>
<td><p>台灣特有種，目前發現地區集中在<a href="../Page/南湖大山.md" title="wikilink">南湖大山周圍</a>，海拔約 3,000 至 3,500 公尺處[13]。</p></td>
<td><p>第 I 級[14]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>!scope="row" style="background:#ffffff" | <a href="../Page/楚南氏山椒魚.md" title="wikilink">楚南氏山椒魚</a></p></td>
<td><p><em>Hynobius sonani</em> <small>(Maki, 1922)</small></p></td>
<td><p>台灣特有種，主要分布於中央山脈中北部（合歡山及<a href="../Page/奇萊山.md" title="wikilink">奇萊山山系一帶</a>），海拔範圍約介於 2,600 至 3,100 公尺之間。</p></td>
<td><p>[15]第 I 級[16]</p></td>
<td></td>
</tr>
</tbody>
</table>

### [无尾目](../Page/无尾目.md "wikilink")（Anura）

台灣的無尾目有[蟾蜍科](../Page/蟾蜍科.md "wikilink")、[叉舌蛙科](../Page/叉舌蛙科.md "wikilink")、[樹蟾科](../Page/樹蟾科.md "wikilink")、[狹口蛙科](../Page/狹口蛙科.md "wikilink")、[赤蛙科](../Page/赤蛙科.md "wikilink")，以及[樹蛙科等](../Page/樹蛙科.md "wikilink")6科。

#### 蟾蜍科（Bufonidae）

台灣的蟾蜍科僅有兩種，其中盤古蟾蜍為台灣特有種\[17\]。

<table>
<caption>align="center" style="background:#9BC4E2; color:white" |<big><strong>蟾蜍科（Bufonidae）</strong></big> |- -</caption>
<thead>
<tr class="header">
<th><p>scope="col" style="background-color:Linen; color:Black" width="100" px |中文俗名</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="150" px |學名</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="550" px |分布</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="90" px |保育狀態</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="100" px |圖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/盤古蟾蜍.md" title="wikilink">盤古蟾蜍</a></p></td>
<td><p><em>Bufo bankorensis</em> <small>Barbour, 1908</small></p></td>
<td><p>台灣特有種，廣泛分布於台灣全島各地及龜山島，海拔從平地到3,000公尺的高山[18]。</p></td>
<td><p>[19]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bufo_bankorensis_Taroko_Canyon_Taiwan.jpg" title="fig:Bufo_bankorensis_Taroko_Canyon_Taiwan.jpg">Bufo_bankorensis_Taroko_Canyon_Taiwan.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑眶蟾蜍.md" title="wikilink">黑眶蟾蜍</a></p></td>
<td><p><em>Duttaphrynus melanosticus</em> <small>Schneider, 1799</small></p></td>
<td><p>分佈於海拔600公尺以下的平地及山區，繁殖力強[20]。</p></td>
<td><p>[21]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bufo_melanosticus_01.JPG" title="fig:Bufo_melanosticus_01.JPG">Bufo_melanosticus_01.JPG</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### [叉舌蛙科](../Page/叉舌蛙科.md "wikilink") Dicroglossidae

台灣的叉舌蛙科計有3屬4種，其中海蛙為外來種。近年研究認為台灣東部的澤蛙屬於另一不同種群，但仍待進一步研究\[22\]。

<table>
<caption>align="center" style="background:#9BC4E2; color:white" |<big><strong>叉舌蛙科 Dicroglossidae</strong></big> |- -</caption>
<thead>
<tr class="header">
<th><p>scope="col" style="background-color:Linen; color:Black" width="100" px |中文俗名</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="150" px |學名</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="550" px |分布</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="90" px |保育狀態</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="100" px |圖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/海蛙.md" title="wikilink">海蛙</a></p></td>
<td><p><em>Fejervarya cancrivora</em> <small>(Gravenhorst, 1829)</small></p></td>
<td><p>外來種，主要棲息於海潮能到的海岸地區，並以紅樹林地區較為常見，耐鹽性較其他的蛙類高。台灣目前僅在<a href="../Page/東港鎮_(台灣).md" title="wikilink">東港及</a><a href="../Page/佳冬鄉.md" title="wikilink">佳冬發現其蹤跡</a>[23]。</p></td>
<td><p>[24]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/澤蛙.md" title="wikilink">澤蛙</a></p></td>
<td><p><em>Fejervarya limnocharis</em> <small>(Boie, 1835)</small></p></td>
<td><p>為台灣平地最常見的蛙類之一，主要棲息在稻田、池塘、湖沼及水溝附近[25]。</p></td>
<td><p>[26]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Rana_limnocharis_sal.jpg" title="fig:Rana_limnocharis_sal.jpg">Rana_limnocharis_sal.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/虎皮蛙.md" title="wikilink">虎皮蛙</a></p></td>
<td><p><em>Hoplobatrachus rugulosus</em> <small>(Wiegmann, 1803)</small></p></td>
<td><p>分布於低海拔稻田、水池、草澤、溝渠等地[27]。</p></td>
<td><p>[28]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Hoplobatrachus_rugulosus.jpg" title="fig:Hoplobatrachus_rugulosus.jpg">Hoplobatrachus_rugulosus.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/福建大頭蛙.md" title="wikilink">福建大頭蛙</a></p></td>
<td><p><em>Limnonectes fujianensis</em> <small>(Ye and Fei, 1994)</small></p></td>
<td><p>常見於台灣北部及西部1,000以下的山區路旁水溝或小溪，主要分布於遮蔽良好、溝底有落葉淤泥的淺水溝或溪澗中[29]。</p></td>
<td><p>[30]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:L_kuhlii_060616_7531_jbti.jpg" title="fig:L_kuhlii_060616_7531_jbti.jpg">L_kuhlii_060616_7531_jbti.jpg</a></p></td>
</tr>
</tbody>
</table>

#### [樹蟾科](../Page/樹蟾科.md "wikilink") Hylidae

台灣的樹蟾科僅有中國樹蟾一種。

<table>
<caption>align="center" style="background:#9BC4E2; color:white" |<big><strong>樹蟾科 Hylidae</strong></big> |- -</caption>
<thead>
<tr class="header">
<th><p>scope="col" style="background-color:Linen; color:Black" width="100" px |中文俗名</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="150" px |學名</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="550" px |分布</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="90" px |保育狀態</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="100" px |圖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/中國樹蟾.md" title="wikilink">中國樹蟾</a></p></td>
<td><p><em>Hyla chinensis</em> <small>Gunther, 1859</small></p></td>
<td><p>生活於灌叢、水塘蘆葦以及美人蕉及麥杆等高杆作物上。</p></td>
<td><p>[31]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Hyla_chinensis.jpg" title="fig:Hyla_chinensis.jpg">Hyla_chinensis.jpg</a></p></td>
</tr>
</tbody>
</table>

#### [狹口蛙科](../Page/狹口蛙科.md "wikilink") Microhylidae

台灣的狹口蛙科已之在台灣建立族群的有3屬5種，其中史丹吉氏小雨蛙為特有種，[亞洲錦蛙為外來種](../Page/亞洲錦蛙.md "wikilink")。[蕃茄蛙](../Page/蕃茄蛙.md "wikilink")（*Dyscophus
guinetiI* ）及[爪哇狹口蛙](../Page/爪哇狹口蛙.md "wikilink")（*Kaloula
baleata*）僅知有輸入台灣，但並未在野外建立族群，故未納入本列表。

<table>
<caption>align="center" style="background:#9BC4E2; color:white" |<big><strong>狹口蛙科（Microhylidae）</strong></big> |- -</caption>
<thead>
<tr class="header">
<th><p>scope="col" style="background-color:Linen; color:Black" width="100" px |中文俗名</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="150" px |學名</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="550" px |分布</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="90" px |保育狀態</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="100" px |圖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/亞洲錦蛙.md" title="wikilink">亞洲錦蛙</a></p></td>
<td><p><em>Kaloula pulchra</em> <small>(Gray, 1831)</small></p></td>
<td><p>分佈在台灣南部的台南與高屏地區，棲地廣泛，草地、森林中之潮濕處都能為其所利用；牠們常躲藏在自己於地面上所挖掘的洞中，或者藏匿在樹洞、石頭等掩蔽物下。※外來種</p></td>
<td><p>[32]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:ChubbyFrog_02.jpg" title="fig:ChubbyFrog_02.jpg">ChubbyFrog_02.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴氏小雨蛙.md" title="wikilink">巴氏小雨蛙</a></p></td>
<td><p><em>Microhyla butleri</em> <small>Boulenger, 1901</small></p></td>
<td><p>分佈在中南部低海拔山區，平常棲息在底層落葉間。</p></td>
<td><p>[33]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:_Microhyla_butleri.jpg" title="fig:_Microhyla_butleri.jpg">_Microhyla_butleri.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黑蒙西氏小雨蛙.md" title="wikilink">黑蒙西氏小雨蛙</a></p></td>
<td><p><em>Microhyla heymonsi</em> <small>Vogt, 1911</small></p></td>
<td><p>分布於山區水域附近的草叢中。</p></td>
<td><p>[34]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Microhyla_heymonsi.jpg" title="fig:Microhyla_heymonsi.jpg">Microhyla_heymonsi.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小雨蛙.md" title="wikilink">小雨蛙</a></p></td>
<td><p><em>Microhyla fissipes</em> <small>Dumeril &amp; Bibron, 1841</small></p></td>
<td><p>分布於水田或水塘中及水域附近草叢。</p></td>
<td><p>[35]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:MicrohylaOrnata1.jpg" title="fig:MicrohylaOrnata1.jpg">MicrohylaOrnata1.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史丹吉氏小雨蛙.md" title="wikilink">史丹吉氏小雨蛙</a></p></td>
<td><p><em>Micryletta steinegeri</em> <small>(Boulenger, 1909)</small></p></td>
<td><p>台灣特有種，零散分佈在嘉義及台中近郊、曾文水庫、台南仙公廟、墾丁、台東、花蓮等地[36]。</p></td>
<td><p>[37]</p></td>
<td></td>
</tr>
</tbody>
</table>

#### [赤蛙科](../Page/赤蛙科.md "wikilink") Ranidae

台灣的赤蛙科共有10種，其中斯文豪氏赤蛙及梭德氏赤蛙為特有種，美洲牛蛙為外來種。

<table>
<caption>align="center" style="background:#9BC4E2; color:white" |<big><strong>赤蛙科（Ranidae）</strong></big> |- -</caption>
<thead>
<tr class="header">
<th><p>scope="col" style="background-color:Linen; color:Black" width="100" px |中文俗名</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="150" px |學名</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="550" px |分布</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="90" px |保育狀態</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="100" px |圖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/腹斑蛙.md" title="wikilink">腹斑蛙</a></p></td>
<td><p><em>Babina adenopleura</em> <small>(Boulenger, 1909)</small></p></td>
<td><p>其生存的海拔範圍為480至1800米。多生活於靜水池塘以及沼澤內隱蔽物下或池岸斜坡上。</p></td>
<td><p>[38]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Rana_adenopleura.jpg" title="fig:Rana_adenopleura.jpg">Rana_adenopleura.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/豎琴蛙.md" title="wikilink">豎琴蛙</a></p></td>
<td><p><em>Babina okinavana</em> <small>(Boettger, 1895)</small></p></td>
<td><p>目前台灣僅在<a href="../Page/南投縣.md" title="wikilink">南投縣魚池鄉蓮華池一帶及宜蘭發現</a>，棲息於森林環境之靜止水域[39]。</p></td>
<td><p>[40]第 II 級[41]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/貢德氏赤蛙.md" title="wikilink">貢德氏赤蛙</a></p></td>
<td><p><em>Hylarana guentheri</em> <small>(Boulenger, 1882)</small></p></td>
<td><p>常棲息於靜水池或稻田以及溪流。其生存的海拔範圍為452至1200米。</p></td>
<td><p>[42]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:貢德氏赤蛙.jpg" title="fig:貢德氏赤蛙.jpg">貢德氏赤蛙.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉都希氏赤蛙.md" title="wikilink">拉都希氏赤蛙</a></p></td>
<td><p><em>Hylarana latouchii</em> <small>(Boulenger, 1899)</small></p></td>
<td><p>其生存的海拔範圍為30至1500米。一般棲息于山區丘陵地帶的水田及靜水池塘中。</p></td>
<td><p>[43]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Rana_latouchii.jpg" title="fig:Rana_latouchii.jpg">Rana_latouchii.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台北赤蛙.md" title="wikilink">台北赤蛙</a></p></td>
<td><p><em>Hylarana taipehensis</em> <small>(Van Denburgh, 1909)</small></p></td>
<td><p>零散分佈在台灣北部及南部低海拔茶園、山坡地以及平地草澤。原為台灣平地常見的蛙類，但由於農藥的濫用以及棲地的破壞，目前僅零散出現在台北石門及三芝、桃園楊梅、台南官田、屏東屏科大附近等少數地方，被列為保育類動物。</p></td>
<td><p>[44]第 II 級</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Hylarana_taipehensis.jpg" title="fig:Hylarana_taipehensis.jpg">Hylarana_taipehensis.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美洲牛蛙.md" title="wikilink">美洲牛蛙</a></p></td>
<td><p><em>Lithobates catesbeianus</em> <small>(Shaw, 1802)</small></p></td>
<td><p>外來種，常棲息於平地的靜水域。</p></td>
<td><p>[45]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:North-American-bullfrog1.jpg" title="fig:North-American-bullfrog1.jpg">North-American-bullfrog1.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斯文豪氏赤蛙.md" title="wikilink">斯文豪氏赤蛙</a></p></td>
<td><p><em>Odorrana swinhoana</em> <small>(Boulenger, 1903)</small></p></td>
<td><p>※台灣特有種</p></td>
<td><p>[46]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:斯文豪氏赤蛙.JPG" title="fig:斯文豪氏赤蛙.JPG">斯文豪氏赤蛙.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金線蛙.md" title="wikilink">金線蛙</a></p></td>
<td><p><em>Pelophylax fukienensis</em> <small>(Pope, 1929)</small></p></td>
<td><p>出現於台灣的濕地和農田附近。※其他應予保育野生動物</p></td>
<td><p>[47]第 III 級[48]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:金線蛙.JPG" title="fig:金線蛙.JPG">金線蛙.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長腳赤蛙.md" title="wikilink">長腳赤蛙</a></p></td>
<td><p><em>Rana longicrus</em> <small>Stejneger, 1898</small></p></td>
<td><p>常棲息於落葉林、池塘、沼澤以及水田。其生存的海拔上限為500米。</p></td>
<td><p>[49]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梭德氏赤蛙.md" title="wikilink">梭德氏赤蛙</a></p></td>
<td><p><em>Rana sauteri</em> <small>Boulenger, 1909</small></p></td>
<td><p>棲息於台灣低海拔溪流的蛙屬兩棲動物。※台灣特有種</p></td>
<td><p>[50]</p></td>
<td></td>
</tr>
</tbody>
</table>

#### [樹蛙科](../Page/樹蛙科.md "wikilink") Rhacophoridae

台灣的樹蛙科計有4屬14種\[51\]。

<table>
<caption>align="center" style="background:#9BC4E2; color:white" |<big><strong>樹蛙科（Rhacophoridae）</strong></big> |- -</caption>
<thead>
<tr class="header">
<th><p>scope="col" style="background-color:Linen; color:Black" width="100" px |中文俗名</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="150" px |學名</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="550" px |分布</p></th>
<th><p>scope="col" style="background-color:Linen; color:Black" width="90" px |保育狀態</p></th>
<th><p>scope="col" style="background:Linen; color:Black" width="100" px |圖像</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/日本樹蛙.md" title="wikilink">日本樹蛙</a></p></td>
<td><p><em>Buergeria japonica</em> (<small>Hallowell, 1861)</small></p></td>
<td></td>
<td><p>[52]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:リュウキュウカジカガエル1.JPG" title="fig:リュウキュウカジカガエル1.JPG">リュウキュウカジカガエル1.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/太田樹蛙.md" title="wikilink">太田樹蛙</a></p></td>
<td><p><em>Buergeria otai</em> <small>Wang, Hsiao, Lee, Tseng, Lin, Komaki &amp; Lin, 2017</small></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/褐樹蛙.md" title="wikilink">褐樹蛙</a></p></td>
<td><p><em>Buergeria robustus</em> <small>(Boulenger, 1909)</small></p></td>
<td><p>※台灣特有物種</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:母褐樹蛙眼部.JPG" title="fig:母褐樹蛙眼部.JPG">母褐樹蛙眼部.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/碧眼樹蛙.md" title="wikilink">碧眼樹蛙</a></p></td>
<td><p><em>Kurixalus berylliniris</em> <small>Wu, Huang, Tsai, Li, Jhang, and Wu, 2016</small></p></td>
<td><p>其主要棲息於台灣海拔225～1,250公尺（738～4,101英尺）山區潮濕的灌木叢或草原。 台灣特有物種</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/艾氏樹蛙.md" title="wikilink">艾氏樹蛙</a></p></td>
<td><p><em>Kurixalus eiffingeri</em> <small>(Boettger, 1895)</small></p></td>
<td><p>低海拔、氣候濕潤的森林或熱帶濕潤山地森林</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/面天樹蛙.md" title="wikilink">面天樹蛙</a></p></td>
<td><p><em>Kurixalus idiootocus</em> <small>(Kuramoto &amp; Wang, 1987)</small></p></td>
<td><p>主要棲息於台灣中低海拔山區潮濕的灌木叢或草原※台灣特有物種</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王氏樹蛙.md" title="wikilink">王氏樹蛙</a></p></td>
<td><p><em>Kurixalus wangi</em> <small>Wu, Huang, Tsai, Li, Jhang, and Wu, 2016</small></p></td>
<td><p>其主要棲息於台灣海拔500公尺（1,600英尺）山區潮濕的灌木叢或草原。※台灣特有物種</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布氏樹蛙.md" title="wikilink">布氏樹蛙</a></p></td>
<td><p><em>Polypedates braueri</em> <small>(Vogt, 1911)</small></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/斑腿樹蛙.md" title="wikilink">斑腿樹蛙</a></p></td>
<td><p><em>Polypedates megacephalus</em> <small>Hallowell, 1861</small></p></td>
<td><p>多棲息於丘陵地帶的稻田或泥窩內、田梗石縫草叢、灌木枝上以及路邊。※外來種</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Polypedates_megacephalus.jpg" title="fig:Polypedates_megacephalus.jpg">Polypedates_megacephalus.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/諸羅樹蛙.md" title="wikilink">諸羅樹蛙</a></p></td>
<td><p><em>Rhacophorus arvalis</em> <small>Lue, Lai &amp; Chen, 1995</small></p></td>
<td><p>分布在台灣嘉義雲林一帶的竹林；果園、低窪積水處。※台灣特有物種 / 珍貴稀有野生動物</p></td>
<td><p>第 II 級[53]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/橙腹樹蛙.md" title="wikilink">橙腹樹蛙</a></p></td>
<td><p><em>Rhacophorus aurantiventris</em> <small>Lue, Lai &amp; Chen, 1994</small></p></td>
<td><p>※台灣特有物種 / 珍貴稀有野生動物</p></td>
<td><p>第 II 級[54]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫氏樹蛙.md" title="wikilink">莫氏樹蛙</a></p></td>
<td><p><em>Rhacophorus moltrechti</em> <small>Boulenger, 1908</small></p></td>
<td><p>※台灣特有物種</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/翡翠樹蛙.md" title="wikilink">翡翠樹蛙</a></p></td>
<td><p><em>Rhacophorus smaragdinus</em> <small>Mou, Risch &amp; Lue, 1983</small></p></td>
<td><p>分布在台灣北部南、北勢溪流域及宜蘭低海拔山區。因為在翡翠水庫附近發現而得名。※台灣特有物種 / 其他應予保育野生動物</p></td>
<td><p>第 III 級[55]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/台北樹蛙.md" title="wikilink">台北樹蛙</a></p></td>
<td><p><em>Rhacophorus taipeianus</em> <small>Liang &amp; Wang, 1978</small></p></td>
<td><p>可在台灣中部南投以北的海拔1,500米以下山區丘陵地見到，但主要分布於台北盆地地區。北部是台北樹蛙數量第一多的棲息地，基隆則是台灣台北樹蛙密度第一名的縣市。※台灣特有物種 / 其他應予保育野生動物</p></td>
<td><p>[56]第 III 級[57]</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Taipei_tree_frog_-_Rhacophorus_taipeianus.jpg" title="fig:Taipei_tree_frog_-_Rhacophorus_taipeianus.jpg">Taipei_tree_frog_-_Rhacophorus_taipeianus.jpg</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 註釋

本列表的「[保护状况](../Page/保护状况.md "wikilink")」參考自[國際自然保護聯盟瀕危物種紅色名錄](../Page/國際自然保護聯盟瀕危物種紅色名錄.md "wikilink")，以及[中華民國行政院農委會的](../Page/行政院農業委員會.md "wikilink")《保育類野生動物名錄》\[58\]，因此反映的為該物種在全球的保護狀況：

## 參考文獻

## 外部連結

  - [台灣兩棲類動物名錄](http://tesri.tesri.gov.tw/view.php?catid=1827)，農委會特有生物研究保育中心

[列表](../Category/台灣動物.md "wikilink")
[Category:各地兩棲動物列表](../Category/各地兩棲動物列表.md "wikilink")
[Category:各國生物列表](../Category/各國生物列表.md "wikilink")

1.

2.   新頭殼 Newtalk|accessdate=2017-10-22|author=|date=|work=新頭殼
    Newtalk|publisher=|language=zh-TW}}

3.
    台灣生物多樣性資訊入口網|accessdate=2017-10-21|author=|date=|work=taibif.tw|publisher=|language=zh-hant}}

4.

5.
6.

7.

8.

9.

10.
11.

12.
13.

14.
15.

16.
17.

18.
19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.
42.

43.
44.

45.

46.

47.

48.
49.

50.

51.

52.

53.
54.
55.
56.

57.
58.