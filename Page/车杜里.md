**车杜里**，出生於[西德](../Page/西德.md "wikilink")[法兰克福](../Page/法兰克福.md "wikilink")，前[韩国足球运动员](../Page/韩国.md "wikilink")，韩国著名球星[车范根之子](../Page/车范根.md "wikilink")，2010年夏季免費投效[蘇超](../Page/蘇超.md "wikilink")[老字號](../Page/老字號_\(足球\).md "wikilink")[些路迪](../Page/凯尔特人足球俱乐部.md "wikilink")<small>\[1\]</small>，曾效力[FC首爾](../Page/FC首爾.md "wikilink")。車斗里已於2015年退休，現為韓國國家足球隊助理教練。

车杜里是當其父親车范根效力[德甲球隊](../Page/德甲.md "wikilink")[法蘭克福時在](../Page/法兰克福足球俱乐部.md "wikilink")[德國出生](../Page/德國.md "wikilink")，其父稍後轉投[勒沃库森](../Page/勒沃库森足球俱乐部.md "wikilink")，故车杜里幼年在德國渡過。特別的是他出生後並未取漢字名，因此「車-{斗}-利」、「車-{斗}--{里}-」及「車-{杜}--{里}-」等成為中文媒體經常使用的譯名，但事實上「車-{斗}--{里}-」較符合Du-Ri（韓文原意為「二人」）的韓語發音。

## 生平

车杜里的俱乐部生涯很幸运的在德国开始，在被勒沃库森引进后，车杜里就被租借给了[比勒菲尔德](../Page/比勒費爾德足球俱樂部.md "wikilink")，及后再转投[法兰克福](../Page/法兰克福足球俱乐部.md "wikilink")。

2002年车杜里还是一名大学球员，但在4月20日同哥斯达黎加的世界杯热身赛中，他获得了在国家队的处子进球，也获得了主帅[希丁克的青睐](../Page/希丁克.md "wikilink")，完成了以一名业余球员身份参加世界杯的壮举。

车杜里从2004/05赛季开始，曾短暂坐稳了球队的主力位置，不过第二年他的进球效率就较前一年有了一个较大的滑坡，被法兰克福队放弃。

接下来的几个赛季，车杜里转战德国各级球队，先后效力于[美因茨和](../Page/美因茨05.md "wikilink")[科布伦茨](../Page/TuS科布倫斯.md "wikilink")，2009年轉投[德甲升班馬](../Page/德甲.md "wikilink")[弗赖堡](../Page/弗赖堡体育俱乐部.md "wikilink")<small>\[2\]</small>，在这个过程中，原本踢前锋的车杜里被逐渐转型为一名边后卫。

2015年11月7日，在與[水原藍翼的比賽結束後](../Page/水原藍翼.md "wikilink")，於[FC首爾結束選手生涯](../Page/FC首爾.md "wikilink")，正式宣布退役。

## 榮譽

  - 些路迪

<!-- end list -->

  - [蘇格蘭足總盃冠軍](../Page/蘇格蘭足總盃.md "wikilink")：2011年；

## 参考资料

## 外部連結

  -
<!-- end list -->

  - [National Team Player
    Record](http://www.kfa.or.kr/koreateam/fb_amatch_contents.asp?p_unique=20071701031)


  - [FIFA Player
    Statistics](http://www.fifa.com/worldfootball/statisticsandrecords/players/player=188387/index.html)

  -
  - [Stats](http://www.fussballdaten.de/spieler/chadoori/)
    fussballdaten.de

[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2011年亞洲盃足球賽球員](../Category/2011年亞洲盃足球賽球員.md "wikilink")
[Category:2015年亞洲盃足球賽球員](../Category/2015年亞洲盃足球賽球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:利華古遜球員](../Category/利華古遜球員.md "wikilink")
[Category:比勒費爾德球員](../Category/比勒費爾德球員.md "wikilink")
[Category:法蘭克福球員](../Category/法蘭克福球員.md "wikilink")
[Category:緬恩斯球員](../Category/緬恩斯球員.md "wikilink")
[Category:科布倫斯球員](../Category/科布倫斯球員.md "wikilink")
[Category:弗賴堡球員](../Category/弗賴堡球員.md "wikilink")
[Category:些路迪球員](../Category/些路迪球員.md "wikilink")
[Category:德素多夫球員](../Category/德素多夫球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:蘇超球員](../Category/蘇超球員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:蘇格蘭外籍足球運動員](../Category/蘇格蘭外籍足球運動員.md "wikilink")
[Category:足球邊後衛](../Category/足球邊後衛.md "wikilink")
[Category:高麗大學校友](../Category/高麗大學校友.md "wikilink")
[Category:韓國足球運動員](../Category/韓國足球運動員.md "wikilink")
[Category:在英國的外國人](../Category/在英國的外國人.md "wikilink")
[Category:韓裔德國人](../Category/韓裔德國人.md "wikilink")
[Category:法蘭克福人](../Category/法蘭克福人.md "wikilink")
[Du](../Category/車姓_\(韓國\).md "wikilink")

1.
2.