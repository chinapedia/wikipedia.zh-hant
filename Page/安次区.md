**安次区**是[河北省](../Page/河北省.md "wikilink")[廊坊市下辖的一个区](../Page/廊坊.md "wikilink")。

## 历史

[西汉时置](../Page/西汉.md "wikilink")**安次县**，后废。[元朝时为](../Page/元朝.md "wikilink")**东安州**。[明朝](../Page/明朝.md "wikilink")[洪武初年](../Page/洪武.md "wikilink")，降为[东安县](../Page/东安县_\(洪武\).md "wikilink")。民国初年，复置**安次县**。

## 气候

属暖温带大陆性季风气候，年平均气温11.4℃，7月份平均气温为26℃，年平均降水量598毫米，全年无霜冻期181—201天。

## 行政区划

下辖3个[街道办事处](../Page/街道办事处.md "wikilink")、4个[镇](../Page/行政建制镇.md "wikilink")、4个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [安次区人民政府](http://anci.lf.gov.cn/)

## 参考文献

[安次区](../Page/category:安次区.md "wikilink")
[区安次区](../Page/category:廊坊区县市.md "wikilink")

[廊坊](../Category/河北市辖区.md "wikilink")