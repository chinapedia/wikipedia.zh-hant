**蔡確**（），字持正，[福建](../Page/福建.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[晉江縣人](../Page/晉江縣.md "wikilink")。[王安石变法的中坚人物](../Page/王安石变法.md "wikilink")。

父[蔡黃裳在](../Page/蔡黃裳.md "wikilink")[陳州被](../Page/陳州.md "wikilink")[陳執中貶逐](../Page/陳執中.md "wikilink")。蔡確早年在[泗州中山寺读书](../Page/泗州.md "wikilink")\[1\]，仁宗[嘉祐四年](../Page/嘉祐.md "wikilink")（1059年）進士，调（京兆府）邠州（治所今陕西彬县）司理参军。外號“倒懸[蛤蜊](../Page/蛤蜊.md "wikilink")（殼菜）”。[王安石当政时](../Page/王安石.md "wikilink")，荐为三班主簿，[王安石變法中的](../Page/王安石變法.md "wikilink")“常平、免役皆成其手”。元丰五年（1082年），拜尚书右仆射兼中书侍郎，此時[富弼在](../Page/富弼.md "wikilink")[西京](../Page/西京.md "wikilink")，上言「蔡确小人，不宜大用」。史载：“确既相，屡兴罗织之狱，缙绅士大夫重足而立矣。”

元豐八年（1085年）三月，神宗病逝，哲宗即位，[宣仁聖烈皇后攝政](../Page/宣仁皇后.md "wikilink")，转左[仆射兼](../Page/仆射.md "wikilink")[门下侍郎](../Page/门下侍郎.md "wikilink")，[司馬光執政](../Page/司馬光.md "wikilink")，幾盡罷新法，改革派人士幾乎全招貶職。元佑二年（1087年）出知[陈州](../Page/陈州.md "wikilink")、[安州](../Page/安州.md "wikilink")（今湖北[安陆县](../Page/安陆县.md "wikilink")）、[邓州](../Page/邓州.md "wikilink")，元祐年間，蔡確在[安州遊車蓋亭時](../Page/安州.md "wikilink")，寫下《夏日遊車蓋亭》十首絕句。其政敵[吳處厚向朝廷說](../Page/吳處厚.md "wikilink")“內五篇皆涉譏訕，而二篇譏訕尤甚，上及君親”，又指出“矯矯名臣郝甑山，忠言直節上元間”之句，是將攝政的高太皇太后比做[武則天](../Page/武則天.md "wikilink")。

高太皇太后怒不可遏，將蔡確貶為[光禄寺卿](../Page/光禄寺.md "wikilink")、分司南京，[范祖禹认为](../Page/范祖禹.md "wikilink")：“蔡之罪，天下不容，尚为列卿，恐难平民愤。”最後貶至[英州别驾](../Page/英州.md "wikilink")（今[广东](../Page/广东.md "wikilink")[英德市](../Page/英德市.md "wikilink")）、[嶺南](../Page/嶺南.md "wikilink")[新州](../Page/新州.md "wikilink")（今[广东](../Page/广东.md "wikilink")[新兴县](../Page/新兴县.md "wikilink")）安置，此舉開啟北宋貶官至嶺南的先例。[呂大防和](../Page/呂大防.md "wikilink")[劉摯曾以蔡確之母年老](../Page/劉摯.md "wikilink")，嶺南路遠，請改遷他處，高太皇太后說：“山可移，此州不可移。”不久死於新州。《[宋史](../Page/宋史.md "wikilink")》将其列入奸臣列传。

蔡确府宅在[泉州城蔡巷](../Page/泉州城.md "wikilink")(今称菜巷)\[2\]。

## 评价

蔡确为[吕惠卿](../Page/吕惠卿.md "wikilink")、[章惇罢贬之后](../Page/章惇.md "wikilink")[王安石变法的中坚人物](../Page/王安石变法.md "wikilink")。蔡确坚持"变法图治、富国强兵"具有积极的意义。
应予以肯定。

元丰五年（1082年），北宋與[西夏之战争](../Page/西夏.md "wikilink")，惨败[永乐城](../Page/永乐城.md "wikilink")，亦是变法的失败，蔡确有很大的责任。

蔡确有才能，但重用[酷吏撕裂社会](../Page/酷吏.md "wikilink")。主持「[陈世儒案](../Page/陈世儒.md "wikilink")」、「[乌台诗案](../Page/乌台诗案.md "wikilink")」，曾經拘捕[吕公着](../Page/吕公着.md "wikilink")、[苏颂](../Page/苏颂.md "wikilink")、[苏轼下狱](../Page/苏轼.md "wikilink")，以[權術打击对手](../Page/權術.md "wikilink")，失势后遭「車蓋亭案」报复。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

[Category:宋朝宰相](../Category/宋朝宰相.md "wikilink")
[Category:嘉祐四年己亥科進士](../Category/嘉祐四年己亥科進士.md "wikilink")
[Category:晋江人](../Category/晋江人.md "wikilink")
[Q](../Category/蔡姓.md "wikilink")

1.  《过庭录·蔡持正山寺书壁》
2.