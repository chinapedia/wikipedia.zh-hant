**轉乘站**（**Transfer
Station**）是一個[城市軌道交通系統的專用詞](../Page/城市軌道交通系統.md "wikilink")。意思是指一個或多個[鐵路車站](../Page/鐵路車站.md "wikilink")，供乘客在不同路線之間進行跨線乘坐[列車的行為](../Page/鐵路列車.md "wikilink")，期間需要**離開車站付費區**或**另行購買車票**。具體地說，就是乘客在某個車站下車，需另行購票，才可以由原本乘坐的路線，轉換至另一條路線繼續行程，而車費則按每次乘坐里程獨立計算。

**無需離開車站付費區**及**無需另行購買車票**，就可乘坐其他路線列車的車站稱為[換乘站](../Page/換乘站.md "wikilink")。在一般口頭語下，-{zh-hans:换乘站;
zh-hant:換乘站;}-與-{zh-hans:转乘站;
zh-hant:轉乘站;}-不作區分。其中，[台灣統稱為](../Page/台灣.md "wikilink")**-{zh-hans:转乘站;
zh-hant:轉乘站;}-**，[香港統稱為](../Page/香港.md "wikilink")**-{zh-hans:转车站;
zh-hant:轉車站;}-**，中國大陸城市（[上海除外](../Page/上海.md "wikilink")）統稱為**-{zh-hans:换乘站;
zh-hant:換乘站;}-**，而東南亞地區的[馬來西亞和](../Page/馬來西亞.md "wikilink")[新加坡則統稱為](../Page/新加坡.md "wikilink")**-{zh-hans:转换站;
zh-hant:轉換站;}-**。

自2007年10月7日[北京地鐵五號線啟用](../Page/北京地鐵5號線.md "wikilink")，全網（不含機場軌道交通線）實行單一票制，北京所有轉乘站都已取消。

## 轉乘方式

[Longyang_Road_Station_Exit_4th.JPG](https://zh.wikipedia.org/wiki/File:Longyang_Road_Station_Exit_4th.JPG "fig:Longyang_Road_Station_Exit_4th.JPG")的乘客只需穿過出口，利用電動扶梯即可到達[磁浮線大堂](../Page/上海磁浮示範運營線.md "wikilink")。\]\]
轉乘大致可分為站內轉乘及站外轉乘兩種。站內轉乘又可細分為大堂轉乘（[共構](../Page/共構.md "wikilink")）及通道轉乘（[共站](../Page/共站.md "wikilink")）。

### 大堂轉乘（共構）

由不同經營者營運的兩條路線，經過良好的磋商及規劃，可將兩條路線的月台設置在同一車站內，方便乘客無需離開車站即可轉車。乘客下車後一般需要利用[樓梯或](../Page/樓梯.md "wikilink")[電動扶梯](../Page/電動扶梯.md "wikilink")，到達位於另一樓層的車站大堂，離開付費區（出閘），然後進入另一路線的付費區（入閘），再前往另一條路線的月臺，例如[香港地鐵和](../Page/香港地鐵.md "wikilink")[九廣西鐵合併前的](../Page/西鐵綫.md "wikilink")[南昌站](../Page/南昌站_\(香港\).md "wikilink")。

### 通道轉乘（共站）

由於地理因素，或建設前沒有良好的規劃，使兩條路線的車站相距較遠，未能設置站內[換乘設施或轉乘大堂](../Page/換乘站.md "wikilink")，例如香港的[尖沙咀站與](../Page/尖沙咀站.md "wikilink")[尖東站和日本](../Page/尖東站.md "wikilink")[東京車站的](../Page/東京車站.md "wikilink")[京葉線](../Page/京葉線.md "wikilink")（與[山手線](../Page/山手線.md "wikilink")、[東海道本線等路線的月台相距較遠](../Page/東海道本線.md "wikilink")，轉乘需步行15至20分鐘）。也有一些情況是因為兩條路線由不同經營者營運，商業上不可能存在站內換乘設施，香港[兩鐵合併前的](../Page/兩鐵合併.md "wikilink")[地鐵與](../Page/香港地鐵.md "wikilink")[九廣鐵路以及台灣高鐵](../Page/九廣鐵路.md "wikilink")[苗栗車站與](../Page/苗栗車站_\(高鐵\).md "wikilink")2016年站房北移後的台鐵[豐富車站等為典型的例子](../Page/豐富車站.md "wikilink")。

為解決乘客轉車困難，通道轉乘便是常見的替代方式。兩個車站間一般會設有專用通道使之連接起來，乘客離開車站付費區後需經過專用通道到達另一路線的車站，有時候亦需要在中途轉換樓層。

### 站外轉乘

兩條路線的車站設於相距較遠的位置，由於商業決定及技術上的限制，未能設置轉乘通道，乘客便需在轉乘途中離開車站範圍。例如[北京地鐵二号线和十三号线转乘通道建好前的](../Page/北京地鐵.md "wikilink")[西直門站](../Page/西直門站.md "wikilink")，乘客轉乘途中需走出地面步行。在[上海軌道交通](../Page/上海軌道交通.md "wikilink")[龍陽路站](../Page/龍陽路站.md "wikilink")，乘客離開[二號線車站後只需步行十米](../Page/上海軌道交通二號線.md "wikilink")，即可利用電動扶梯到達[磁浮線大堂](../Page/上海磁浮示範運營線.md "wikilink")。[台北捷運與](../Page/台北捷運.md "wikilink")[機場捷運的](../Page/桃園機場捷運.md "wikilink")[台北車站及](../Page/台北車站.md "wikilink")[北門站](../Page/北門站.md "wikilink")、[三重站](../Page/三重站.md "wikilink")、[新北產業園區站亦為此轉乘方式之實例](../Page/新北產業園區站.md "wikilink")。

## 可供轉乘的車站

以下只列出已投入服務的車站。

###

### 上海軌道交通

#### 虚拟换乘（出站换乘）站

使用[上海公共交通卡时](../Page/上海公共交通卡.md "wikilink")，若在下列车站出闸后30分钟内进入邻线闸机，此站相当于[换乘站](../Page/换乘站.md "wikilink")，乘坐里程连续计算。

  - [1号线与](../Page/上海轨道交通1号线.md "wikilink")[3号线](../Page/上海轨道交通3号线.md "wikilink")/1号线与[4号线](../Page/上海轨道交通4号线.md "wikilink")：[上海火车站](../Page/上海火车站_\(地铁\).md "wikilink")
  - [2号线与](../Page/上海轨道交通2号线.md "wikilink")[10号线](../Page/上海轨道交通10号线.md "wikilink")：[虹桥2号航站楼站](../Page/虹桥2号航站楼站.md "wikilink")
  - 2号线、[12号线与](../Page/上海轨道交通12号线.md "wikilink")[13号线](../Page/上海轨道交通13号线.md "wikilink")：[南京西路站](../Page/南京西路站.md "wikilink")
  - 7号线与13号线：[长清路站](../Page/长清路站.md "wikilink")

#### 转乘站

  - [2號線](../Page/上海軌道交通2號線.md "wikilink")、[7号线](../Page/上海轨道交通7号线.md "wikilink")、[16号线與](../Page/上海轨道交通16号线.md "wikilink")[磁浮線](../Page/上海磁浮示範運營線.md "wikilink")：[龍陽路站](../Page/龍陽路站.md "wikilink")
  - [2号线与](../Page/上海轨道交通2号线.md "wikilink")[磁浮线](../Page/上海磁浮示范运营线.md "wikilink")：[浦东国际机场站](../Page/浦东国际机场站.md "wikilink")

###

### 港鐵

#### 有条件出站换乘站

使用[八達通时](../Page/八達通.md "wikilink")，若在下列车站出闸后30分钟内进入邻线闸机，此站相当于[轉車站](../Page/轉車站.md "wikilink")，乘坐里程连续计算。使用單程票或“本日第二程车程优惠”則按兩個獨立車程分別計算票價。

  - [荃湾綫及](../Page/荃湾綫.md "wikilink")[西鐵綫](../Page/西鐵綫.md "wikilink")：[尖沙咀站及](../Page/尖沙咀站.md "wikilink")[尖东站](../Page/尖东站.md "wikilink")

#### 转乘站

使用[八達通可免費乘坐](../Page/八達通.md "wikilink")[機場快綫外的一程車程](../Page/機場快綫.md "wikilink")。使用單程票則按兩個獨立車程分別計算票價。

  - [东涌綫及機場快綫](../Page/东涌綫.md "wikilink")：[香港站](../Page/香港站.md "wikilink")、[九龙站](../Page/九龙站.md "wikilink")、[青衣站](../Page/青衣站.md "wikilink")
  - [西鐵綫及機場快綫](../Page/西鐵綫.md "wikilink")：九龙站 -
    [柯士甸站](../Page/柯士甸站.md "wikilink")

###

#### 捷運、輕軌

  - [台北捷運](../Page/台北捷運.md "wikilink")[淡水信義線](../Page/淡水信義線.md "wikilink")、[新北捷運](../Page/新北捷運.md "wikilink")[淡海輕軌](../Page/淡海輕軌.md "wikilink")[綠山線](../Page/綠山線.md "wikilink")：[紅樹林站](../Page/紅樹林站.md "wikilink")
  - [高雄捷運](../Page/高雄捷運.md "wikilink")[紅線](../Page/高雄捷運紅線.md "wikilink")[凱旋站](../Page/凱旋站_\(臺灣\).md "wikilink")、[高雄捷運](../Page/高雄捷運.md "wikilink")[環狀輕軌](../Page/高雄環狀輕軌.md "wikilink")：[前鎮之星站](../Page/前鎮之星站.md "wikilink")
  - [高雄捷運](../Page/高雄捷運.md "wikilink")[橘線](../Page/高雄捷運橘線.md "wikilink")[西子灣站](../Page/西子灣站.md "wikilink")、[高雄捷運](../Page/高雄捷運.md "wikilink")[環狀輕軌](../Page/高雄環狀輕軌.md "wikilink")：[哈瑪星站](../Page/哈瑪星站.md "wikilink")

#### 臺灣鐵路

  - [臺鐵](../Page/臺鐵.md "wikilink")[縱貫線](../Page/縱貫線_\(北段\).md "wikilink")、[臺灣高鐵](../Page/臺灣高鐵.md "wikilink")、[台北捷運](../Page/台北捷運.md "wikilink")[板南線](../Page/板南線.md "wikilink")、[台北捷運](../Page/台北捷運.md "wikilink")[淡水信義線](../Page/淡水信義線.md "wikilink")：[臺北車站](../Page/臺北車站.md "wikilink")、[桃園捷運](../Page/桃園捷運.md "wikilink")[機場線](../Page/桃園捷運機場線.md "wikilink")：[臺北車站](../Page/臺北車站.md "wikilink")
  - [臺鐵](../Page/臺鐵.md "wikilink")[縱貫線](../Page/縱貫線_\(北段\).md "wikilink")、[臺灣高鐵](../Page/臺灣高鐵.md "wikilink")、[台北捷運](../Page/台北捷運.md "wikilink")[板南線](../Page/板南線.md "wikilink")：[南港車站](../Page/南港車站.md "wikilink")、[板橋車站](../Page/板橋車站_\(臺灣\).md "wikilink")
  - [臺鐵](../Page/臺鐵.md "wikilink")[縱貫線](../Page/縱貫線_\(北段\).md "wikilink")、[台北捷運](../Page/台北捷運.md "wikilink")[松山新店線](../Page/松山新店線.md "wikilink")：[松山車站](../Page/松山車站.md "wikilink")
  - [臺鐵](../Page/臺鐵.md "wikilink")[六家線與臺灣高鐵](../Page/六家線.md "wikilink")：[高鐵新竹站與](../Page/高鐵新竹站.md "wikilink")[六家車站](../Page/六家車站.md "wikilink")
  - [臺鐵](../Page/臺鐵.md "wikilink")[臺中線與臺灣高鐵](../Page/臺中線.md "wikilink")：[高鐵臺中站與](../Page/高鐵臺中站.md "wikilink")[新烏日車站](../Page/新烏日車站.md "wikilink")、[高鐵苗栗站與](../Page/高鐵苗栗站.md "wikilink")[豐富車站](../Page/豐富車站.md "wikilink")
  - [臺鐵](../Page/臺鐵.md "wikilink")[沙崙線與臺灣高鐵](../Page/沙崙線.md "wikilink")：[高鐵臺南站與](../Page/高鐵臺南站.md "wikilink")[沙崙車站](../Page/沙崙車站.md "wikilink")
  - [臺鐵](../Page/臺鐵.md "wikilink")[縱貫線](../Page/縱貫線_\(南段\).md "wikilink")、臺灣高鐵與[高雄捷運](../Page/高雄捷運.md "wikilink")[紅線](../Page/高雄捷運紅線.md "wikilink")：[高鐵左營站與](../Page/高鐵左營站.md "wikilink")[新左營車站](../Page/新左營車站.md "wikilink")
  - [臺鐵](../Page/臺鐵.md "wikilink")[縱貫線](../Page/縱貫線_\(南段\).md "wikilink")、[高雄捷運](../Page/高雄捷運.md "wikilink")[紅線](../Page/高雄捷運紅線.md "wikilink")：[橋頭車站](../Page/橋頭車站.md "wikilink")、[高雄車站](../Page/高雄車站.md "wikilink")

## 註釋

<references group="注"/>

## 參考資料

<references />

1.  [上海交通网－轨道交通线路介绍](https://web.archive.org/web/20070527233525/http://www.jt.sh.cn/node110/node112/200609/con120219.htm)
2.  [上海交通网－轨道交通线换乘](https://web.archive.org/web/20070523032826/http://www.jt.sh.cn/node110/node114/200609/con120169.htm)
3.  [上海地鐵運營有限公司－換乘指引](https://web.archive.org/web/20070509180058/http://www.shmetro.com/operation/zhiyin/index.htm)

## 參見

  - [換乘站](../Page/換乘站.md "wikilink")
  - [共構與共站](../Page/共構與共站.md "wikilink")
  - [總站](../Page/總站.md "wikilink")
  - [城市軌道交通](../Page/城市軌道交通.md "wikilink")

[Category:鐵路](../Category/鐵路.md "wikilink")
[Category:鐵路車站](../Category/鐵路車站.md "wikilink")
[Category:鐵路運作](../Category/鐵路運作.md "wikilink")
[Category:城市轨道交通](../Category/城市轨道交通.md "wikilink")