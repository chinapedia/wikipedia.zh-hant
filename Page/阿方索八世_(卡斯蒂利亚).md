[Alfonso_VIII_de_Castilla_01.jpg](https://zh.wikipedia.org/wiki/File:Alfonso_VIII_de_Castilla_01.jpg "fig:Alfonso_VIII_de_Castilla_01.jpg")
**阿方索八世**（'''Alfonso VIII
'''，）[卡斯蒂利亚国王](../Page/卡斯蒂利亚.md "wikilink")（1158年\~1214年在位）。

阿方索八世为[桑乔三世之子](../Page/桑乔三世_\(卡斯蒂利亚\).md "wikilink")，1158年父王去世后继承王位。在他成人前，卡斯蒂利亚多次受到[纳瓦拉王国的干涉和侵扰](../Page/纳瓦拉王国.md "wikilink")。阿方索八世[亲政后](../Page/亲政.md "wikilink")，即与[阿拉贡结成同盟](../Page/阿拉贡.md "wikilink")，这个卡斯蒂利亚-阿拉贡联盟成为未来统一的[西班牙的雏形](../Page/西班牙.md "wikilink")。

1176年，阿方索八世与[英格兰国王](../Page/英格兰.md "wikilink")[亨利二世之女](../Page/亨利二世_\(英格兰\).md "wikilink")[埃利诺结婚](../Page/英格兰的埃莉诺.md "wikilink")。

从12世纪70年代起，阿方索八世致力于应付武力强大的[摩洛哥](../Page/摩洛哥.md "wikilink")[阿尔摩哈王朝对西班牙各](../Page/阿尔摩哈王朝.md "wikilink")[基督教王国的侵犯](../Page/基督教.md "wikilink")。阿尔摩哈王朝崛起于[阿特拉斯山脉](../Page/阿特拉斯山脉.md "wikilink")，到12世纪后期已兼并了原摩洛哥[阿尔摩拉维王朝在](../Page/阿尔摩拉维王朝.md "wikilink")[北非和西班牙的全部领土](../Page/北非.md "wikilink")。1195年，阿方索八世被阿尔摩哈王朝最强大的统治者在中击败。

1212年，阿方索八世在著名的中打败摩洛哥统帅。这是一次决定性的胜利，直接导致了阿尔摩哈王朝的衰亡。阿方索八世因此被称为西班牙历史上最伟大的基督教国王之一。

[Category:卡斯蒂利亚君主](../Category/卡斯蒂利亚君主.md "wikilink")