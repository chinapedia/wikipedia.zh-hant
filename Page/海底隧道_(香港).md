**海底隧道**（），簡稱**紅隧**、**海隧**、**舊隧**，因隧道在九龍的出入口位于[紅磡灣](../Page/紅磡灣.md "wikilink")，故多稱為**紅磡海底隧道**，是[香港首條連貫](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[銅鑼灣和](../Page/銅鑼灣.md "wikilink")[九龍](../Page/九龍.md "wikilink")[紅磡灣的過海行車隧道](../Page/紅磡灣.md "wikilink")，是香港以至世界最繁忙的行車隧道之一。[隧道人員獲授權執行香港法例](../Page/隧道交通督導員.md "wikilink")368章《行車隧道（政府）條例》。

## 沿革

[左](https://zh.wikipedia.org/wiki/File:Victoriaharbourbridge.jpg "fig:左")
香港最早草擬興建跨越[維多利亞港的基建設施](../Page/維多利亞港.md "wikilink")，可追溯到1948年《[亞拔高比報告](../Page/亞拔高比報告.md "wikilink")》。報告首次提到以[海底隧道形式](../Page/海底隧道.md "wikilink")，從[尖沙咀來往](../Page/尖沙咀.md "wikilink")[中環](../Page/中環.md "wikilink")，以紓緩交通及完善維港兩岸的運輸系統。

到了1961年，[維多利亞發展有限公司提出興建維港](../Page/維多利亞發展有限公司.md "wikilink")[跨海大橋的方案](../Page/跨海大橋.md "wikilink")，大橋兩岸的出入口跟現在的紅磡海底隧道相約。此方案與當時海底隧道方案競爭，不過政府最後選擇海底隧道，基於以下主要原因：

1.  擬議大橋最高點達400米，會對在[啟德機場升降的飛機造成危險](../Page/啟德機場.md "wikilink")
2.  大橋較容易受到極端天氣影響
3.  如果大橋發生重大事故，將有機會截斷來往[鯉魚門至](../Page/鯉魚門.md "wikilink")[尖沙咀的航道](../Page/尖沙咀.md "wikilink")

[港英政府銳意開拓銅鑼灣](../Page/港英政府.md "wikilink")、紅磡、尖沙嘴之經濟，決定了如今之出入口位置。1965年，[鍾士元預見四線行車隧道會迅速飽和](../Page/鍾士元.md "wikilink")，曾提出六線方案，惜因成本過高未獲採納\[1\]。

## 概要

海底隧道於1969年9月1日動工，1972年8月2日通車，耗資港幣3.2億港元。隧道全長1.86公里，以[沉管式隧道建造](../Page/沉管式隧道.md "wikilink")，車速限制每小時70公里。由[香港隧道有限公司](../Page/香港隧道有限公司.md "wikilink")（[港通控股有限公司前身](../Page/港通控股有限公司.md "wikilink")）以[「建造、營運、移轉」形式興建及經營](../Page/民間興建營運後轉移模式.md "wikilink")，專營權在1999年9月1日屆滿。隧道跨越[維多利亞港](../Page/維多利亞港.md "wikilink")，將[九龍半島和](../Page/九龍半島.md "wikilink")[香港島兩岸獨立的](../Page/香港島.md "wikilink")[道路網絡連接起來](../Page/道路.md "wikilink")。海底隧道南端出入口位於[奇力島](../Page/奇力島.md "wikilink")，位於港島[灣仔區](../Page/灣仔區.md "wikilink")。因工程關係，該島已與香港島連接。北端出入口位於[紅磡灣填海區](../Page/紅磡灣.md "wikilink")。收費廣場位於[紅磡灣出口](../Page/紅磡灣.md "wikilink")，共有16個收費亭，其中10個為人手收費亭，2個為[專利巴士專用的](../Page/香港巴士.md "wikilink")[快易通自動繳費亭](../Page/快易通.md "wikilink")，而餘下4個為[快易通專用的自動繳費亭](../Page/快易通.md "wikilink")。2017年12月3日起，駕車人士可以在海底隧道的人手收費亭，以[八達通或本地發行的非接觸式信用卡](../Page/八達通.md "wikilink")（包括[Visa
payWave](../Page/Visa_payWave.md "wikilink")、[感應式Mastercard及](../Page/MasterCard_Contactless.md "wikilink")[銀聯閃付](../Page/闪付.md "wikilink")\[2\]）拍卡繳付隧道費。

### 行車流量

海底隧道是香港最繁忙的4線行車隧道之一，也是香港最繁忙、使用率最高的道路，其次為[東區海底隧道](../Page/東區海底隧道.md "wikilink")。其雙線雙程行車的舊式設計，使隧道的行車流量早於1982年便告飽和，幾乎全日都會出現交通擠塞。因此，除隧道收費外，由1984年6月1日起，當局向所有車輛徵收隧道稅，冀減低流量，但成效甚微，後來政府決定在1986至1989下半年期間建設東區海底隧道以分流部分車輛以減輕壓力，但東隧往返港島東及九龍東，且行車時間長且距離較遠，不及紅隧方便。東區海底隧道的專營權已在2016年8月7日屆滿，交還特區政府管理。

### 接駁交通

紅隧九龍入口位於[港鐵](../Page/港鐵.md "wikilink")[紅磡站旁](../Page/紅磡站.md "wikilink")，由於[東鐵綫暫未能直接過海](../Page/東鐵綫.md "wikilink")，不少新界東部的居民會先乘搭東鐵綫到紅磡站，再前往紅隧收費廣場的[巴士站再轉乘](../Page/巴士站.md "wikilink")[過海巴士](../Page/香港過海隧道巴士路線.md "wikilink")，特別在早上上班時間，過海隧道巴士站經常擠滿乘客，排隊上車的人龍有時更長至連接港鐵紅磡站的天橋。為此，巴士公司會於繁忙時間為部分路線安排空車抵達過海隧道巴士站以疏導轉車乘客。

## 通車儀式

1972年8月2日，第25任[香港總督](../Page/香港總督.md "wikilink")[麥理浩爵士](../Page/麥理浩.md "wikilink")（Baron
MacLehose of
Beoch）主持啟用剪綵儀式，宣佈紅磡海底隧道正式通車啟用，當紅的電視藝人[沈殿霞乘坐的一輛老爺車](../Page/沈殿霞.md "wikilink")，隨即駛過紅磡海底隧道，成為第1位正式使用紅磡海底隧道的市民。\[3\]\[4\]

## 現況

### 擠塞情況

雖然先後有[東區海底隧道及](../Page/東區海底隧道.md "wikilink")[西區海底隧道通車分流](../Page/西區海底隧道.md "wikilink")，但由於兩隧收費較紅隧高昂（收費由$13-$155不等），且地理位置又不及紅隧方便，故紅隧擠塞的情況不但未有改善，而且日益惡化，尤其在繁忙時間、周末及假日前夕，紅隧交通必定癱瘓，連接紅隧的道路，包括[公主道](../Page/公主道.md "wikilink")、[加士居道天橋](../Page/加士居道天橋.md "wikilink")、[暢運道](../Page/暢運道.md "wikilink")、[漆咸道南](../Page/漆咸道南.md "wikilink")、[漆咸道北](../Page/漆咸道北.md "wikilink")、[東九龍走廊](../Page/東九龍走廊.md "wikilink")、[堅拿道天橋](../Page/堅拿道天橋.md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[維園道及](../Page/維園道.md "wikilink")[東區走廊](../Page/東區走廊.md "wikilink")，亦會嚴重擠塞，沙中線工程期間，部份路段擠塞情況加重，甚至到午夜12時仍可見到車龍。而[香港仔隧道亦受紅隧倒塞的車龍影響](../Page/香港仔隧道.md "wikilink")，繁忙時間如有需要，會進行間封，情況在[南港島線於](../Page/南港島線.md "wikilink")2016年12月28日通車前最為明顯。2015年，平均每日行車量達115,722架次，相比其設計用量8萬架次，超出近半。\[5\]因此，政府一直被要求加快研究興建香港第四條過海隧道的可行性。

另外，[東區海底隧道及](../Page/東區海底隧道.md "wikilink")[大老山隧道的](../Page/大老山隧道.md "wikilink")30年[專營權分別已於](../Page/專營權.md "wikilink")2016年8月7日及2018年7月11日屆滿，已由香港政府收回管理，但由於[香港2號幹線行車隧道](../Page/香港2號幹線.md "wikilink")（東隧及大隧）位置相對[香港1號幹線行車隧道](../Page/香港1號幹線.md "wikilink")（紅隧及獅隧）而言偏遠，早已定位往返[新界東](../Page/新界東.md "wikilink")、[九龍東和](../Page/九龍東.md "wikilink")[港島東](../Page/港島東.md "wikilink")，加上[香港3號幹線的](../Page/香港3號幹線.md "wikilink")[西區海底隧道多次加價](../Page/西區海底隧道.md "wikilink")，估計最快要到[中環灣仔繞道於](../Page/中環灣仔繞道.md "wikilink")2019年2月24日全面通車後\[6\]，西隧出口配套交通改善後，擠塞情況得才有望開始紓緩。但始終要到[沙中綫分階段投入服務](../Page/沙中綫.md "wikilink")，以逐步減少過海巴士的需求，以及到2023年夏季，即[西區海底隧道的](../Page/西區海底隧道.md "wikilink")30年[專營權屆滿後](../Page/專營權.md "wikilink")，紅隧擠塞情況才會有更明顯改善。

### 管道維修

平日凌晨時份，紅隧僅有的2條管道其中一條會停止使用，以進行維修。剩下的一條管道則會改為雙程行車，並會提高車速限制。2001年，紅隧曾無法在凌晨完成重鋪路面的工程，使紅隧在上班繁忙時間仍需要維持單管道行車，幾乎完全癱瘓港島和九龍的交通\[7\]，車龍成「十字型」分成東西向和南北向兩段，分別由港島東區的[太古城伸至西區的](../Page/太古城.md "wikilink")[西營盤](../Page/西營盤.md "wikilink")，以及由九龍[公主道](../Page/公主道.md "wikilink")、[窩打老道伸延至](../Page/窩打老道.md "wikilink")[香港仔隧道及](../Page/香港仔隧道.md "wikilink")[香港島南區](../Page/南區_\(香港\).md "wikilink")。

## 行車時間監察系統

近年，[運輸署在海底隧道及其餘兩條海底隧道安裝了行車時間監察系統](../Page/運輸署.md "wikilink")，並在香港及九龍通往隧道的主要道路設置行車時間顯示板，顯示經3條隧道過海的預計行車時間，以方便駕駛人士選擇使用最方便的海底隧道及行車路線。

### 隧道職工會

紅磡海底隧道設有職[工會](../Page/工會.md "wikilink")，分別為[香港隧道及公路幹線從業員總會紅磡海底隧道分會](../Page/香港隧道及公路幹線從業員總會.md "wikilink")（[工聯會屬會](../Page/香港工會聯合會.md "wikilink")，登記編號：853），理事長為[辛榕城先生](../Page/辛榕城.md "wikilink")；另一工會為[香港收費道路從業員協會](../Page/香港收費道路從業員協會.md "wikilink")（[職工盟屬會](../Page/香港職工會聯盟.md "wikilink")，登記編號：926）。

## 過往多年的收費

以下費用皆以港幣計算。

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>分類</strong></p></td>
<td style="text-align: center;"><p><strong>車輛類別</strong></p></td>
<td style="text-align: center;"><p><strong>1972年8月2日<br />
至<br />
1984年5月31日</strong></p></td>
<td style="text-align: center;"><p><strong>1984年6月1日<br />
至<br />
1999年8月31日</strong></p></td>
<td style="text-align: center;"><p><strong>1999年9月1日<br />
至<br />
2019年2月16日</strong></p></td>
<td style="text-align: center;"><p><strong>2019年2月17日<br />
至今</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1</p></td>
<td style="text-align: center;"><p><a href="../Page/電單車.md" title="wikilink">電單車</a>／<a href="../Page/機動三輪車.md" title="wikilink">機動三輪車</a></p></td>
<td style="text-align: center;"><p>$3</p></td>
<td style="text-align: center;"><p>$4</p></td>
<td style="text-align: center;"><p>$8</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2</p></td>
<td style="text-align: center;"><p><a href="../Page/私家車.md" title="wikilink">私家車</a></p></td>
<td style="text-align: center;"><p>$5</p></td>
<td style="text-align: center;"><p>$10</p></td>
<td style="text-align: center;"><p>$20</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/出租車.md" title="wikilink">的士</a></p></td>
<td style="text-align: center;"><p>$10</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>3</p></td>
<td style="text-align: center;"><p>公共及私家小型巴士</p></td>
<td style="text-align: center;"><p>$8</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>4</p></td>
<td style="text-align: center;"><p>總重5.5公噸或以下的輕型貨車</p></td>
<td style="text-align: center;"><p>$15</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>5</p></td>
<td style="text-align: center;"><p>總重5.5至24公噸的中型貨車</p></td>
<td style="text-align: center;"><p>$10</p></td>
<td style="text-align: center;"><p>$20</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>6</p></td>
<td style="text-align: center;"><p>總重超逾24公噸的重型貨車</p></td>
<td style="text-align: center;"><p>$15</p></td>
<td style="text-align: center;"><p>$30</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>7</p></td>
<td style="text-align: center;"><p>公共及私家單層巴士</p></td>
<td style="text-align: center;"><p>非專營</p></td>
<td style="text-align: center;"><p>$10</p></td>
<td style="text-align: center;"><p>$10</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>專營</p></td>
<td style="text-align: center;"><p>免費</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>8</p></td>
<td style="text-align: center;"><p>公共及私家雙層巴士</p></td>
<td style="text-align: center;"><p>非專營</p></td>
<td style="text-align: center;"><p>$15</p></td>
<td style="text-align: center;"><p>$15</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>專營</p></td>
<td style="text-align: center;"><p>免費</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>9</p></td>
<td style="text-align: center;"><p>超過兩條車軸的車輛，每條車軸額外收費</p></td>
<td style="text-align: center;"><p>專營巴士</p></td>
<td style="text-align: center;"><p>$5</p></td>
<td style="text-align: center;"><p>$10</p></td>
<td style="text-align: center;"><p>免費</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>其他車輛</p></td>
<td style="text-align: center;"><p>$10</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 鄰近地標

  - [香港理工大學](../Page/香港理工大學.md "wikilink")
  - [置富都會](../Page/置富都會.md "wikilink")
  - [香港體育館](../Page/香港體育館.md "wikilink")
  - [香港歷史博物館](../Page/香港歷史博物館.md "wikilink")
  - [香港科學館](../Page/香港科學館.md "wikilink")

## 鄰近交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - 九龍入口：<font color="{{東鐵綫色彩}}">█</font>[東鐵綫](../Page/東鐵綫.md "wikilink")、<font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[紅磡站A](../Page/紅磡站.md "wikilink")1、D1出口
  - 港島入口：<font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[銅鑼灣站D](../Page/銅鑼灣站.md "wikilink")1出口

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")（不途經本隧道）

## 途經之公共交通服務

<div class="NavFrame collapsed" style="clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

  - 特別日子服務

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 觀塘/牛頭角至銅鑼灣線\[8\]
  - 中環/灣仔至觀塘/牛頭角線\[9\]
  - 觀塘至跑馬地馬場線\[10\]
  - 西貢至銅鑼灣線\[11\]
  - 北角/西灣河至紅磡/土瓜灣線\[12\]
  - 慈雲山至灣仔線\[13\]
  - 土瓜灣至筲箕灣線\[14\]
  - 旺角至筲箕灣線\[15\]\[16\]
  - 旺角至灣仔線\[17\]
  - 旺角至西環線\[18\]
  - 中環至旺角線\[19\]
  - 石澳至旺角線\[20\]
  - 銅鑼灣至青山道線\[21\]
  - 荃灣至灣仔線\[22\]
  - 上水至銅鑼灣線\[23\]

</div>

</div>

## 圖片

[File:XHT-1970s.jpg|1970年代位於奇力島入口](File:XHT-1970s.jpg%7C1970年代位於奇力島入口)
<File:HK> CrossHarbourTunnelNight 20070930.jpg|海底隧道紅磡收費廣場
[File:Hk-cross-harbour-tunnel-005.png|海底隧道內觀](File:Hk-cross-harbour-tunnel-005.png%7C海底隧道內觀)
<File:HK> Bus 101 Tour view CWB Hung Hing Road Cross-Harbur Tunnel
entrance Shell V-Power outdoor ads sign Apr-2013.JPG|名稱 <File:CHT>
Southbound Busstop2.jpg|海底隧道往香港方向巴士站，早上繁忙時間須安排大量巴士以接載乘客（攝於1999年5月）
<File:TrafficTimeNotice> CrossHurbourTunnel Hong
Kong.jpg|在香港仔隧道出口的顯示板，顯示使用西區、東區及紅磡海底隧道的時間
<File:HK> Hung Hum Cross-Harbour Tunnel 101 Bus Stop.JPG|位於紅隧的巴士站

## 相关

  - [拆弹专家](../Page/拆弹专家_\(电影\).md "wikilink")：2017年上映的[香港電影](../Page/香港.md "wikilink")，由[邱禮濤執導](../Page/邱禮濤.md "wikilink")，[劉德華投資監製兼領銜主演](../Page/劉德華.md "wikilink")。電影後半部份講述由在逃的頭號通緝犯「火爆」洪繼鵬（[姜武飾](../Page/姜武.md "wikilink")）率領一群悍匪封鎖紅隧，並挾持及殺害人質，甚至不惜炸毀紅隧，務求向[香港警方](../Page/香港警方.md "wikilink")[爆炸品處理課](../Page/爆炸品處理課.md "wikilink")[警司及拆彈專家章在山](../Page/警司.md "wikilink")（劉德華飾）報回在本片開頭不久自己的犯罪團隊被章在山出賣及被警方逮捕瓦解之仇（因本片開首講述章在山是「火爆」洪繼鵬犯罪團伙中的一名鑽研爆破罪犯的臥底學徒；在一年半後，被列為在逃頭號通緝犯的洪繼鵬為了報被出賣之仇引章在山出馬，就在香港紅磡海底隧道裏製造爆炸事件。）電影製作團隊以1:1比例搭出了三分之二長度的紅磡海底隧道，然後在片尾炸掉。

## 參考資料

## 外部連結

  - [運輸署行車隧道收費表](https://web.archive.org/web/20111107024338/http://www.td.gov.hk/tc/transport_in_hong_kong/tunnels_and_bridges/tunnels_and_bridges/index.html)
  - [香港到九龍過海行車速度及時間](http://tis.td.gov.hk/rtisPDA/ttis/jtis.do?locale=zh)

[Category:香港公路隧道](../Category/香港公路隧道.md "wikilink")
[Category:銅鑼灣](../Category/銅鑼灣.md "wikilink")
[Category:紅磡灣](../Category/紅磡灣.md "wikilink")
[Category:海底隧道](../Category/海底隧道.md "wikilink")
[Category:維多利亞港](../Category/維多利亞港.md "wikilink")
[Category:香港的世界之最](../Category/香港的世界之最.md "wikilink")
[Category:紅色公共小巴可行駛的幹線街道](../Category/紅色公共小巴可行駛的幹線街道.md "wikilink")

1.
2.  [拍卡過隧道！城隧月尾先行
    一文睇晒各隧道實施時間表](https://hk.news.appledaily.com/local/realtime/article/20170713/56950329)
3.  [沈殿霞港式娛樂的隕落](http://ent.sina.com.cn/r/m/2008-03-03/17021934196.shtml)www.sina.com.cn
    2008年03月03日 三聯生活周刊
4.  [2011年8月2日
    《成報》](http://www.singpao.com/NewsArticle.aspx?NewsID=186344&Lang=tc)
5.  [2016運輸資料年報 - 交通流量 -
    海底隧道](http://www.td.gov.hk/mini_site/atd/2016/tc/section4_10.html)
6.  [1](https://www.hk01.com/%E7%A4%BE%E6%9C%83%E6%96%B0%E8%81%9E/296318/%E4%B8%AD%E7%92%B0%E7%81%A3%E4%BB%94%E7%B9%9E%E9%81%93-%E5%85%A8%E9%83%A8%E5%B7%A5%E7%A8%8B%E5%A4%A7%E8%87%B4%E5%AE%8C%E6%88%90-2%E6%9C%8824%E6%97%A5%E4%B8%8A%E5%8D%887%E6%99%82%E5%85%A8%E9%9D%A2%E9%80%9A%E8%BB%8A)
7.
8.  [銅鑼灣鵝頸橋—牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh01.html)
9.  [中環／灣仔＞牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh41.html)
10. [觀塘協和街＞跑馬地馬場](http://www.16seats.net/chi/rmb/r_kh11.html)
11. [西貢市中心—銅鑼灣登龍街](http://www.16seats.net/chi/rmb/r_hn98.html)
12. [慈雲山＞西灣河及北角＞紅磡及土瓜灣](http://www.16seats.net/chi/rmb/r_kh33.html)
13. [慈雲山＞銅鑼灣及灣仔](http://www.16seats.net/chi/rmb/r_kh32.html)
14. [筲箕灣站—土瓜灣欣榮花園](http://www.16seats.net/chi/rmb/r_kh06.html)
15. [筲箕灣站—旺角砵蘭街](http://www.16seats.net/chi/rmb/r_kh02.html)
16. [旺角銀行中心—筲箕灣](http://www.16seats.net/chi/rmb/r_kh22.html)
17. [旺角銀行中心—灣仔天樂里](http://www.16seats.net/chi/rmb/r_kh20.html)
18. [旺角銀行中心—西環](http://www.16seats.net/chi/rmb/r_kh21.html)
19. [中環蘭桂坊＞旺角](http://www.16seats.net/chi/rmb/r_kh03.html)
20. [石澳＞旺角](http://www.16seats.net/chi/rmb/r_kh09.html)
21. [銅鑼灣鵝頸橋＞旺角／青山道](http://www.16seats.net/chi/rmb/r_kh12.html)
22. [荃灣福來邨—灣仔站](http://www.16seats.net/chi/rmb/r_hn40.html)
23. [銅鑼灣鵝頸橋—大埔及上水](http://www.16seats.net/chi/rmb/r_hn73.html)