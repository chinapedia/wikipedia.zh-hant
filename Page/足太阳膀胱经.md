[缩略图](https://zh.wikipedia.org/wiki/File:Hua_t08.jpg "fig:缩略图")

**足太陽膀胱經**，（），[十二正经之一](../Page/十二正经.md "wikilink")，与[足少陰腎經相表里](../Page/足少陰腎經.md "wikilink")。本經起於[睛明](../Page/睛明穴.md "wikilink")，止於[至陰](../Page/至陰穴.md "wikilink")，左右各67個[腧穴](../Page/腧穴.md "wikilink")，有49個穴位分佈在頭面部、項背部和腰背部，18個穴位分佈在下肢後。

## 经脉循行

足太陽膀胱經首穴從內眼角[睛明穴](../Page/睛明穴.md "wikilink")，上行額部（[攢竹](../Page/攢竹穴.md "wikilink")、眉衝、曲差；會神庭、頭臨泣），交會於頭頂（五處、承光、通天、[百會](../Page/百會穴.md "wikilink")），分支至耳上角，在枕部分出兩支向下，分別循行分佈於背腰、臀部，末穴是小趾端[至陰穴](../Page/至陰穴.md "wikilink")。\[1\]\[2\]

## 主治概要

本经主要治疗心、項、目、背、腰、下肢部的病变。

## 穴位

1.  [睛明](../Page/睛明穴.md "wikilink")
2.  [攢竹](../Page/攢竹穴.md "wikilink")
3.  [眉衝](../Page/眉衝穴.md "wikilink")
4.  [曲差](../Page/曲差穴.md "wikilink")
5.  [五處](../Page/五處穴.md "wikilink")
6.  [承光](../Page/承光穴.md "wikilink")
7.  [通天](../Page/通天穴.md "wikilink")
8.  [絡卻](../Page/絡卻穴.md "wikilink")
9.  [玉枕](../Page/玉枕穴.md "wikilink")
10. [天柱](../Page/天柱穴.md "wikilink")
11. [大杼](../Page/大杼穴.md "wikilink")
12. [風門](../Page/風門穴.md "wikilink")
13. [肺俞](../Page/肺俞穴.md "wikilink")
14. [厥陰俞](../Page/厥陰俞穴.md "wikilink")
15. [心俞](../Page/心俞穴.md "wikilink")
16. [督俞](../Page/督俞穴.md "wikilink")
17. [膈俞](../Page/膈俞穴.md "wikilink")
18. [肝俞](../Page/肝俞穴.md "wikilink")
19. [膽俞](../Page/膽俞穴.md "wikilink")
20. [脾俞](../Page/脾俞穴.md "wikilink")
21. [胃俞](../Page/胃俞穴.md "wikilink")
22. [三焦俞](../Page/三焦俞穴.md "wikilink")
23. [腎俞](../Page/腎俞穴.md "wikilink")
24. [氣海俞](../Page/氣海俞穴.md "wikilink")
25. [大腸俞](../Page/大腸俞穴.md "wikilink")
26. [關元俞](../Page/關元俞穴.md "wikilink")
27. [小腸俞](../Page/小腸俞穴.md "wikilink")
28. [膀胱俞](../Page/膀胱俞穴.md "wikilink")
29. [中膂俞](../Page/中膂俞穴.md "wikilink")
30. [白環俞](../Page/白環俞穴.md "wikilink")
31. [上髎](../Page/上髎穴.md "wikilink")
32. [次髎](../Page/次髎穴.md "wikilink")
33. [中髎](../Page/中髎穴.md "wikilink")
34. [下髎](../Page/下髎穴.md "wikilink")
35. [會陽](../Page/會陽穴.md "wikilink")
36. [承扶](../Page/承扶穴.md "wikilink")
37. [殷門](../Page/殷門穴.md "wikilink")
38. [浮郄](../Page/浮郄穴.md "wikilink")
39. [委陽](../Page/委陽穴.md "wikilink")
40. [委中](../Page/委中穴.md "wikilink")，[合穴](../Page/合穴.md "wikilink")
41. [附分](../Page/附分穴.md "wikilink")
42. [魄戶](../Page/魄戶穴.md "wikilink")
43. [膏肓](../Page/膏肓穴.md "wikilink")
44. [神堂](../Page/神堂穴.md "wikilink")
45. [噫譆](../Page/噫譆穴.md "wikilink")
46. [膈關](../Page/膈關穴.md "wikilink")
47. [魂門](../Page/魂門穴.md "wikilink")
48. [陽綱](../Page/陽綱穴.md "wikilink")
49. [意舍](../Page/意舍穴.md "wikilink")
50. [胃倉](../Page/胃倉穴.md "wikilink")
51. [肓門](../Page/肓門穴.md "wikilink")
52. [志室](../Page/志室穴.md "wikilink")
53. [胞肓](../Page/胞肓穴.md "wikilink")
54. [秩邊](../Page/秩邊穴.md "wikilink")
55. [合陽](../Page/合陽穴.md "wikilink")
56. [承筋](../Page/承筋穴.md "wikilink")
57. [承山](../Page/承山穴.md "wikilink")
58. [飛揚](../Page/飛揚穴.md "wikilink")，[絡穴](../Page/絡穴.md "wikilink")
59. [跗陽](../Page/跗陽穴.md "wikilink")，陽蹻郄穴
60. [崑崙](../Page/崑崙穴.md "wikilink")，[經穴](../Page/經穴.md "wikilink")
61. [僕參](../Page/僕參穴.md "wikilink")
62. [申脈](../Page/申脈穴.md "wikilink")，[八脈交會穴](../Page/八脈交會穴.md "wikilink")，通於[陽蹻脈](../Page/陽蹻脈.md "wikilink")
63. [金門](../Page/金門穴.md "wikilink")，[郄穴](../Page/郄穴.md "wikilink")
64. [京骨](../Page/京骨穴.md "wikilink")，[原穴](../Page/原穴.md "wikilink")
65. [束骨](../Page/束骨穴.md "wikilink")，[輸穴](../Page/輸穴.md "wikilink")
66. [足通谷](../Page/足通谷穴.md "wikilink")，[滎穴](../Page/滎穴.md "wikilink")
67. [至陰](../Page/至陰穴.md "wikilink")，[井穴](../Page/井穴.md "wikilink")

## 参考文献

[Category:十二正經](../Category/十二正經.md "wikilink")

1.
2.  《[靈樞](../Page/靈樞.md "wikilink")·經脈》：膀胱足太陽之脈，起於目內眦，上額，交巔。 其支者：從巔至耳上角。
    其直者：從巔入絡腦，還出別下項，循肩膊內，夾脊抵腰中，入循膂，絡腎，屬膀胱。 其支者：從腰中，下夾脊，貫腎，入膕中。
    其支者：從膊內左右別下貫胛，夾脊內，過髀樞，循髀外後廉下合膕中，以下貫踹內，出外踝之後，循京骨至小指外側。