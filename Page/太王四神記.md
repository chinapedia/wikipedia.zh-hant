《**太王四神記**》（）是[韓國](../Page/韓國.md "wikilink")[MBC於](../Page/文化廣播_\(韓國\).md "wikilink")2007年9月10日起播放的[水木連續劇](../Page/MBC水木迷你連續劇.md "wikilink")，由TSG製作公司（TSG
Production Company
LLC）製作，製作費高達430多億韓元，為韓劇史上投資製作費第一。與一般傳統歷史古裝劇不同，充滿神秘幻想的獨特體裁，以[高句麗開國](../Page/高句麗.md "wikilink")[歷史的](../Page/歷史.md "wikilink")[檀君朝鮮前的](../Page/檀君朝鮮.md "wikilink")[傳說和高句麗第十九世君主](../Page/傳說.md "wikilink")[談德的故事為主題](../Page/廣開土王.md "wikilink")，由[金鍾學執導](../Page/金鍾學.md "wikilink")，[宋智娜編劇](../Page/宋智娜.md "wikilink")，[日本著名](../Page/日本.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")[久石讓擔任](../Page/久石讓.md "wikilink")[音樂監製](../Page/音樂.md "wikilink")，[裴勇俊主演](../Page/裴勇俊.md "wikilink")，另有[文素利](../Page/文素利.md "wikilink")、[李智雅](../Page/李智雅.md "wikilink")、[尹泰榮](../Page/尹泰榮.md "wikilink")、[崔民秀等參演](../Page/崔民秀.md "wikilink")。

## 劇情簡介

天帝[桓雄下凡人間並建立名叫](../Page/桓雄.md "wikilink")[朝鮮](../Page/古朝鮮.md "wikilink")（，[台灣版本音譯為](../Page/台灣.md "wikilink")[肅慎](../Page/肅慎.md "wikilink")）的國家，這片土地上有著崇仰老虎的虎族與崇仰熊的熊族。桓雄與熊族女戰士賽澳相戀；虎族神女嘉真心生嫉妒，將他們的兒子拋下懸崖。擁有朱雀之心的賽澳因為過度悲傷與憤怒，突然喪失理智，燃燒了整個大地。桓雄召喚出白虎（風伯）、青龍（雲師）、玄武（雨師）試圖撲滅火勢，無奈控制不住；為了拯救世界，桓雄只好選擇殺掉賽澳，嘉真最後也走向死路。桓雄將朱雀、白虎、青龍、玄武封印在信物中，便回到天上去了，並預言：朝鮮王星升起之時為新王誕生之日，四神會再度覺醒並輔佐朝鮮王。幾千年後，當朝鮮王星升起時，[談德與](../Page/高談德.md "wikilink")[高句麗第一貴族淵氏家族之子虎凱同時誕生了](../Page/高句麗.md "wikilink")……。

## 演出陣容

  - [裴勇俊](../Page/裴勇俊.md "wikilink")：[朝鮮](../Page/古朝鮮.md "wikilink")[桓雄](../Page/桓雄.md "wikilink")、[高句麗](../Page/高句麗.md "wikilink")[谈德](../Page/高谈德.md "wikilink")（好太王）<small>（少年：[俞承豪](../Page/俞承豪.md "wikilink")）</small>（粵語配音：[陳欣](../Page/陳欣.md "wikilink")）
  - [文素利](../Page/文素利.md "wikilink")：嘉真、徐琦荷<small>（少年：[朴恩玭](../Page/朴恩玭.md "wikilink")、幼年：[金恩瑞](../Page/金恩瑞.md "wikilink")）</small>
  - [李智雅](../Page/李智雅.md "wikilink")：赛澳、秀芝妮<small>（少年：[沈恩敬](../Page/沈恩敬.md "wikilink")）</small>（粵語配音：[陳凱婷](../Page/陳凱婷.md "wikilink")）
  - [吳光祿](../Page/吳光祿.md "wikilink")：玄高（玄武）<small>（少年：[吳承允](../Page/吳承允.md "wikilink")）</small>
  - [朴誠雄](../Page/朴誠雄.md "wikilink")：主武峙（白虎）（粵語配音：[劉昭文](../Page/劉昭文.md "wikilink")）
  - [李必立](../Page/李必立.md "wikilink")：處虜（青龍）<small>（少年：[李玹雨](../Page/李玹雨.md "wikilink")）</small>
  - [朴相元](../Page/朴相元.md "wikilink")：淵加黎
  - [尹泰榮](../Page/尹泰榮.md "wikilink")：淵虎凱<small>（少年：[金鎬英](../Page/金鎬英.md "wikilink")）</small>（粵語配音：[陳廷軒](../Page/陳廷軒.md "wikilink")）
  - [崔民秀](../Page/崔民秀.md "wikilink")：火天會大長老
  - [朴成民](../Page/朴成民_\(演員\).md "wikilink")：史亮
  - [獨孤永宰](../Page/獨孤永宰.md "wikilink")：[高句麗](../Page/高句麗.md "wikilink")[伊連](../Page/高伊連.md "wikilink")（故國壤王）
  - [李多熙](../Page/李多熙.md "wikilink")：閣丹
  - [朴正赫](../Page/朴正赫.md "wikilink")：高羽忠
  - [張航善](../Page/張航善.md "wikilink")：黑蓋
  - [閔智悟](../Page/閔智悟.md "wikilink")：鐵兜屢
  - [金美京](../Page/金美京.md "wikilink")：巴巽
  - [朴在鎮](../Page/朴在鎮.md "wikilink")：火石
  - [申恩廷](../Page/申恩廷.md "wikilink")：達妃
  - [洪景燕](../Page/洪景燕.md "wikilink")：大神官
  - [金光永](../Page/金光永.md "wikilink")：甘潼
  - [宋起賢](../Page/宋起賢.md "wikilink")：趙周道
  - [禹賢](../Page/禹賢.md "wikilink")：麻糖大叔
  - [金宣敬](../Page/金宣敬.md "wikilink")：淵氏夫人
  - [金赫](../Page/金赫.md "wikilink")：達九
  - [陽基沅](../Page/陽基沅.md "wikilink")：[百济](../Page/百济.md "wikilink")[阿莘王](../Page/阿莘王.md "wikilink")
  - [郑在坤](../Page/郑在坤.md "wikilink")：[后燕](../Page/后燕.md "wikilink")[慕容云](../Page/慕容云.md "wikilink")
  - [鄭允錫](../Page/鄭允錫.md "wikilink")：阿植（談德和琦荷之子）

## 收視率

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>播放日期</p></th>
<th><p>TNmS 收視率[1]</p></th>
<th><p>AGB 收視率[2]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/首爾.md" title="wikilink">首爾地區</a></p></td>
<td><p><a href="../Page/大韓民國.md" title="wikilink">全國地區</a></p></td>
<td><p>首爾地區</p></td>
<td><p>全國地區</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>2007/09/11</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>2007/09/12</p></td>
<td><p>27.7%</p></td>
<td><p>26.9%</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2007/09/13</p></td>
<td><p>28.3%</p></td>
<td><p>26.9%</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>2007/09/19</p></td>
<td><p>33.3%</p></td>
<td><p>31.7%</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>2007/09/20</p></td>
<td><p>33.1%</p></td>
<td><p>31.5%</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2007/09/26</p></td>
<td><p>25.3%</p></td>
<td><p>23.4%</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2007/09/27</p></td>
<td><p>33.0%</p></td>
<td><p>30.9%</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2007/10/10</p></td>
<td><p>27.9%</p></td>
<td><p>25.9%</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2007/10/11</p></td>
<td><p>29.3%</p></td>
<td><p>28.2%</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2007/10/17</p></td>
<td><p>28.6%</p></td>
<td><p>27.5%</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>2007/10/18</p></td>
<td><p>29.7%</p></td>
<td><p>28.3%</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>2007/10/24</p></td>
<td><p>30.2%</p></td>
<td><p>29.6%</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>2007/10/25</p></td>
<td><p>30.4%</p></td>
<td><p>29.1%</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>2007/10/31</p></td>
<td><p>29.0%</p></td>
<td><p>27.9%</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>2007/11/01</p></td>
<td><p>33.2%</p></td>
<td><p>31.9%</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>2007/11/07</p></td>
<td><p>31.0%</p></td>
<td><p>30.0%</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>2007/11/08</p></td>
<td><p>31.9%</p></td>
<td><p>30.4%</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>2007/11/14</p></td>
<td><p>31.5%</p></td>
<td><p>30.4%</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>2007/11/15</p></td>
<td><p>32.8%</p></td>
<td><p>31.6%</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>2007/11/21</p></td>
<td><p>32.1%</p></td>
<td><p>30.4%</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>2007/11/22</p></td>
<td><p>33.4%</p></td>
<td><p>32.3%</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>2007/11/28</p></td>
<td><p>32.9%</p></td>
<td><p>31.6%</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>2007/11/29</p></td>
<td><p>34.6%</p></td>
<td><p>33.0%</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>2007/12/05</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>平均收視率</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>特別篇1</p></td>
<td><p>2007/09/10</p></td>
<td><p>14.5%</p></td>
<td><p>14.1%</p></td>
</tr>
<tr class="odd">
<td><p>特別篇2</p></td>
<td><p>2007/12/06</p></td>
<td><p>16.9%</p></td>
<td><p>16.0%</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<small>

  - 收視最低的集數以表示，收視最高的集數以表示，而空格則表示該集的收視沒有相關數據。</small>

## 播放權

MBC原定2007年5月首播《太王四神記》，但因為製作上的問題，首播日期由一度由5月改至6月尾。其後，MBC始終未能將此劇如期在6月尾播映，金鍾學因此曾公開道歉\[3\]。2007年9月10日，MBC首播《太王四神記》。

2007年9月，[香港](../Page/香港.md "wikilink")[關-{于}-娛樂文化科技有限公司](../Page/關于娛樂文化科技有限公司.md "wikilink")（[英文全名](../Page/英文.md "wikilink")“All
About Entertainment Culture Tech.
Ltd.”，英文簡稱“aae”）購得《太王四神記》[亞洲區](../Page/亞洲.md "wikilink")（南韓與日本除外）[版權](../Page/版權.md "wikilink")，[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）購得《太王四神記》日本地區公開播映權。

2007年11月，[中國電視公司與](../Page/中國電視公司.md "wikilink")[中天電視聯手以將近](../Page/中天電視.md "wikilink")[新台幣](../Page/新台幣.md "wikilink")3000萬元購得《太王四神記》[台灣公開播映權](../Page/台灣.md "wikilink")，每集授權金額高達新台幣97萬元，可在3年內播映9次。\[4\]
2007年12月14日起，[中視無線台每周五](../Page/中視無線台.md "wikilink")20:00～21:30以[雙語](../Page/雙語.md "wikilink")(主聲道為[華語](../Page/華語.md "wikilink")[配音](../Page/配音.md "wikilink")，副聲道為[韓語原音](../Page/韓語.md "wikilink"))首播《太王四神記》，每週四21:30～23:00以雙語(主聲道為韓語原音，副聲道為華語配音)重播《太王四神記》，兩者均有加註[正體中文](../Page/正體中文.md "wikilink")[字幕](../Page/字幕.md "wikilink")。\[5\]
韓語[主題曲](../Page/主題曲.md "wikilink")《千年戀歌》是久石讓譜曲，[東方神起主唱](../Page/東方神起.md "wikilink")；華語[片尾曲](../Page/片尾曲.md "wikilink")《傳說》是[施人誠填詞](../Page/施人誠.md "wikilink")，[鄭楠譜曲](../Page/鄭楠.md "wikilink")，[林宥嘉](../Page/林宥嘉.md "wikilink")、[劉力揚合唱](../Page/劉力揚.md "wikilink")。

2007年12月，[中華電信購得](../Page/中華電信.md "wikilink")《太王四神記》[台灣地區](../Page/台灣地區.md "wikilink")[VOD播映權](../Page/VOD.md "wikilink")。2008年1月11日，[中華電信MOD正式推](../Page/中華電信MOD.md "wikilink")-{出}-《太王四神記》，每週上架六集，韓語發音，正體中文字幕。2008年2月14日，《太王四神記》下架。\[6\]\[7\]

2007年12月3日起，[NHK數位衛星高清頻道播映](../Page/NHK數位衛星高清頻道.md "wikilink")《太王四神記》，[韓語發音](../Page/韓語.md "wikilink")，加註[日文字幕](../Page/日文.md "wikilink")。2008年4月5日至2008年9月27日，[NHK綜合頻道播映](../Page/NHK綜合頻道.md "wikilink")《太王四神記》[日語配音修剪版](../Page/日語.md "wikilink")。2008年10月5日至2009年3月，[NHK衛星第2頻道播映](../Page/NHK衛星第2頻道.md "wikilink")《太王四神記》日語配音完整版。

香港[TVB有購買該劇播放權](../Page/TVB.md "wikilink")，於2009年6月24日起在[無線劇集台播出](../Page/無線劇集台.md "wikilink")，其後在[高清翡翠台](../Page/高清翡翠台.md "wikilink")、[翡翠台播出](../Page/翡翠台.md "wikilink")。

## 舞台版

2009年日本[寶塚歌劇團將劇集改編成舞台劇](../Page/寶塚歌劇團.md "wikilink")，於2009年1-3月由寶塚歌劇團[花組在](../Page/花組.md "wikilink")[寶塚大劇場初演](../Page/寶塚大劇場.md "wikilink")。主角高談德由花組[真飛聖飾演](../Page/真飛聖.md "wikilink")，導演/腳本為小池修一郎。

其後小池修一郎對花組的劇本作出少許更改，於2009年6-9月由寶塚歌劇團[星組在寶塚大劇場再演](../Page/星組.md "wikilink")，主角高談德轉由星組[柚希禮音飾演](../Page/柚希禮音.md "wikilink")，導演/腳本依然是小池修一郎，劇名改為《太王四神記Ver.Ⅱ》。

此外，寶塚歌劇團更製作了《太王四神記Ver.Ⅱ》電影版，2010年2月13日在日本上映。

## 爭議

  - 抄襲疑雲
    本劇被指內容抄襲題材近似的韓國漫畫《[風之國](../Page/風之國.md "wikilink")》。
  - 竄改歷史
    本劇之劇本多牽涉歷史，而對[中原地區和整個](../Page/中原.md "wikilink")[東北亞地區](../Page/東北亞.md "wikilink")（今[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")[遠東地區](../Page/遠東地區.md "wikilink")、[日本及](../Page/日本.md "wikilink")[中國東北地區](../Page/中國東北.md "wikilink")）的描述與史實多有出入，可能誤導觀眾。

## 腳註

## 外部連結

  - [《太王四神記》南韓官方網站](http://www.taesagi.com/)
  - [《太王四神記》南韓MBC官方網站](http://www.imbc.com/broad/tv/drama/legend/)
  - [《太王四神記》日本官方網頁](https://web.archive.org/web/20070706224605/http://www.nifty.com/taiousijinki/)
  - [《太王四神記》日本NHK官方網頁](https://web.archive.org/web/20071022042049/http://www3.nhk.or.jp/kaigai/taioshijinki/)
  - [《太王四神記》台灣中視官方網頁](https://web.archive.org/web/20080703171451/http://www.ctv.com.tw/event/2007/tsg/)
  - [《太王四神記》香港無綫官方網頁](http://programme.tvb.com/drama/thelegend/)
  - 《太王四神記》輪迴中的甦醒[1](http://www.epochtimes.com/b5/14/11/15/n4296865.htm),[2](http://www.epochtimes.com/b5/14/11/17/n4297526.htm),[3](http://www.epochtimes.com/b5/14/11/18/n4298253.htm)

[Category:2007年韓國電視劇集](../Category/2007年韓國電視劇集.md "wikilink")
[Category:MBC水木迷你連續劇](../Category/MBC水木迷你連續劇.md "wikilink")
[T太](../Category/中視外購電視劇.md "wikilink")
[Category:好太王題材作品](../Category/好太王題材作品.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:四象题材作品](../Category/四象题材作品.md "wikilink")
[Category:王子主角題材電視劇](../Category/王子主角題材電視劇.md "wikilink")
[Category:中天電視外購電視劇](../Category/中天電視外購電視劇.md "wikilink")
[Category:被中國大陸禁播的電視劇](../Category/被中國大陸禁播的電視劇.md "wikilink")
[Category:MBC時代劇](../Category/MBC時代劇.md "wikilink")

1.  TNmS收視資料：[TNmS Media Korea](http://www.tnms.tv/)
2.  AGB收視資料：[AGB Nielsen Media](http://www.agbnmr.co.kr/)
3.  新聞資料：[韓國《朝鮮日報》：〈金鐘鶴就《太王四神記》推遲開播公開道歉〉](http://chinese.chosun.com/big5/site/data/html_dir/2007/06/08/20070608000033.html)，2007年6月8日。
4.  許怡筠
    報導，[〈中視、中天斥資3千萬　共同取得「太王四神記」版權〉](http://mol.mcu.edu.tw/show.php?nid=99141)，[銘傳大學傳播學院](../Page/銘傳大學.md "wikilink")《[銘報即時新聞](../Page/銘報.md "wikilink")》，2007年12月8日。
5.  詳見《太王四神記》中視官方網站。
6.  [中華電信MOD首頁](http://mod.cht.com.tw/)
7.  中華電信-MOD討論區，[〈感恩回饋區/戲劇\~"太王四神記"1/11上片〉](http://mod.cht.com.tw/phpbb/viewtopic.php?t=3839)，2008年1月8日。