[Andrew_mellon_stamp.JPG](https://zh.wikipedia.org/wiki/File:Andrew_mellon_stamp.JPG "fig:Andrew_mellon_stamp.JPG")
**安德鲁·威廉·梅隆**（**Andrew William
Mellon**，），[美国银行家](../Page/美国.md "wikilink")、工业家、慈善家、艺术品收藏家、政治家，[美国共和党成员](../Page/美国共和党.md "wikilink")，曾任[美国财政部长](../Page/美国财政部长.md "wikilink")（1921年-1932年）和[美国驻英国大使](../Page/美国驻英国大使.md "wikilink")（1932年-1933年）。

梅隆是唯一一位历经三任总统（[沃伦·G·哈定](../Page/沃伦·G·哈定.md "wikilink")、[卡尔文·柯立芝和](../Page/卡尔文·柯立芝.md "wikilink")[赫伯特·胡佛](../Page/赫伯特·胡佛.md "wikilink")）的财政部长。

[宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink")[匹兹堡出生](../Page/匹兹堡_\(宾夕法尼亚州\).md "wikilink")。1873年畢業於西部賓州大學，也就是現今的[匹茲堡大學](../Page/匹茲堡大學.md "wikilink")。

[纽约州](../Page/纽约州.md "wikilink")[南安普敦逝世](../Page/南安普敦_\(纽约州\).md "wikilink")。

## 外部链接

  - [The Andrew W. Mellon - Mellon Foundation Biography by David
    Cannadine](https://web.archive.org/web/20061127035203/http://www.mellon.org/about_foundation/history/andrew-w-mellon)
  - [The Andrew W. Mellon Foundation](http://www.mellon.org/)
  - [taxhistory.org](http://www.taxhistory.org/thp/readings.nsf/0/1d6628f544d4a43c85256ee0004d414d?OpenDocument)
    - an essay on Mellon's tax policies
  - [Overview of Mellon Tax Cut
    Plans](https://web.archive.org/web/20120324062205/http://www.cato.org/pubs/tbb/tbb-0302-13.pdf)
  - [Pittsburgh Post-Gazette article on history of the Mellons and
    Mellon
    Financial](http://www.post-gazette.com/pg/07182/798280-28.stm)
  - [Pittsburgh Post-Gazette series on Mellon's involvement in the oil
    industry and mid-east
    oil](http://www.post-gazette.com/businessnews/20030316mideast2.asp)

[Category:美国财政部长](../Category/美国财政部长.md "wikilink")
[Category:美国驻英国大使](../Category/美国驻英国大使.md "wikilink")
[Category:美國共和黨黨員](../Category/美國共和黨黨員.md "wikilink")
[Category:美國銀行家](../Category/美國銀行家.md "wikilink")
[Category:美國慈善家](../Category/美國慈善家.md "wikilink")
[Category:大蕭條](../Category/大蕭條.md "wikilink")
[Category:匹茲堡大學校友](../Category/匹茲堡大學校友.md "wikilink")
[Category:愛爾蘭裔美國人](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:賓夕法尼亞州人](../Category/賓夕法尼亞州人.md "wikilink")
[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")
[Category:美國聖公宗教徒](../Category/美國聖公宗教徒.md "wikilink")