**四眼魚科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鯉齒目的其中一](../Page/鯉齒目.md "wikilink")[科](../Page/科.md "wikilink")。

## 分類

**四眼魚科**下分3個屬，如下：

  - 四眼魚屬(*Anableps*)
      - [四眼魚](../Page/四眼魚.md "wikilink")(*Anableps anableps*)
      - [道氏四眼魚](../Page/道氏四眼魚.md "wikilink")(*Anableps dowei*)
      - [小鱗四眼魚](../Page/小鱗四眼魚.md "wikilink")(*Anableps microlepis*)
  - 任氏鱂屬(*Jenynsia*)
      - [互連斑任氏鱂](../Page/互連斑任氏鱂.md "wikilink")(*Jenynsia
        alternimaculata*)
      - (*Jenynsia darwni*)
      - [迪菲任氏鱂](../Page/迪菲任氏鱂.md "wikilink")(*Jenynsia diphyes*)
      - [艾格曼任氏鱂](../Page/艾格曼任氏鱂.md "wikilink")(*Jenynsia eigenmanni*)
      - [連斑任氏鱂](../Page/連斑任氏鱂.md "wikilink")(*Jenynsia eirmostigma*)
      - [任氏鱂](../Page/任氏鱂.md "wikilink")(*Jenynsia lineata*)
      - (*Jenynsia luxata*)
      - [斑任氏鱂](../Page/斑任氏鱂.md "wikilink")(*Jenynsia maculata*)
      - [多齒任氏鱂](../Page/多齒任氏鱂.md "wikilink")(*Jenynsia multidentata*)
      - [臀紋任氏鱂](../Page/臀紋任氏鱂.md "wikilink")(*Jenynsia obscura*)
      - [豹任氏鱂](../Page/豹任氏鱂.md "wikilink")(*Jenynsia onca*)
      - [桑克任氏鱂](../Page/桑克任氏鱂.md "wikilink")(*Jenynsia
        sanctaecatarinae*)
      - [圖庫曼任氏鱂](../Page/圖庫曼任氏鱂.md "wikilink")(*Jenynsia tucumana*)
      - [單紋任氏鱂](../Page/單紋任氏鱂.md "wikilink")(*Jenynsia unitaenia*)
      - [韋氏任氏鱂](../Page/韋氏任氏鱂.md "wikilink")(*Jenynsia weitzmani*)
  - 尖歡鱂屬(*Oxyzygonectes*)
      - [尖歡鱂](../Page/尖歡鱂.md "wikilink")(*Oxyzygonectes dovii*)

[\*](../Category/四眼魚科.md "wikilink")