**三韩**是公元前2世纪末至公元后4世纪左右[朝鲜半岛南部三个部落联盟](../Page/朝鲜半岛.md "wikilink")，包括[马韩](../Page/马韩.md "wikilink")、[辰韩和](../Page/辰韩.md "wikilink")[弁韩](../Page/弁韩.md "wikilink")。\[1\]《三国志·魏志·东夷传》中提到：“韩在带方之南，东西以海为限，南与倭接，方可四千里。
”按照秦汉时期一里为415.8米的标准来计算，那么三韩部落所占据的总面积应该不下十七万平方公里，已将大半个朝鲜半岛都囊括在了其中。
其中[马韩是三韩中最强大的](../Page/马韩.md "wikilink")，被三韩拥立为“辰王”，定都目支国，统领三韩之地。\[2\]4世纪[马韩已被](../Page/马韩.md "wikilink")[百济兼并](../Page/百济.md "wikilink")。[辰韩被](../Page/辰韩.md "wikilink")[新罗兼并](../Page/新罗.md "wikilink")，[弁韩则演化成](../Page/弁韩.md "wikilink")[伽倻后又被](../Page/伽倻.md "wikilink")[新罗兼并](../Page/新罗.md "wikilink")。

“三韩”一词最早在中国的典籍《后汉书·东夷列传》中出现。三韩所在的具体位置存在争议。不同时期，三韩的边界也不断的变化。但大體上认为在朝鮮半島的南部，大致为今天的大韩民国。《[后汉书](../Page/后汉书.md "wikilink")》曰三韩之地“皆古之[辰国也](../Page/辰国.md "wikilink")”\[3\]

「韓」（）在現代[韩语中被賦予](../Page/韩语.md "wikilink")“伟大”或“领袖”的意思。[大韩民国的国名中就含有](../Page/大韩民国.md "wikilink")“韩”字。「韓」後來演變為[朝鮮族的別名](../Page/朝鮮族.md "wikilink")。三韩是现在[朝鲜族的直系祖先](../Page/朝鲜族.md "wikilink")，也是如今韩国国名得来的原因。

## 概况

《三国志》中[辰韩與秦人有關](../Page/辰韩.md "wikilink")，也有说法沿用了[辰国的](../Page/辰国.md "wikilink")“辰”字。
[弁韩的另一个称呼](../Page/弁韩.md "wikilink")“弁辰”也使用“辰”这个字。另外[马韩的统治者在一段时间称自己为辰王表示对整个三韩的统治](../Page/马韩.md "wikilink")。

[马韩是三韩中最大的一个](../Page/马韩.md "wikilink")，由54个部落构成。大致位于[全罗道](../Page/全罗道.md "wikilink")，[忠清道和](../Page/忠清道.md "wikilink")[京畿道](../Page/京畿道.md "wikilink")。

[辰韩主要位于](../Page/辰韩.md "wikilink")[洛东江以东](../Page/洛东江.md "wikilink")，由12个城邦组成。其中的一个城邦后来吸收了其它城邦发展成[新罗](../Page/新罗.md "wikilink")。

[弁韩由](../Page/弁韩.md "wikilink")12个城邦组成，发展成[伽倻后被](../Page/伽倻.md "wikilink")[新罗吸收](../Page/新罗.md "wikilink")。弁韩位于洛东江以南和以西。三韓领袖叫天君，有祭天的义务。

## 技术

铁制工具使农业耕作更加容易，从而也促进了朝鲜半岛的农业发展。[全罗道在当时已发展成](../Page/全罗道.md "wikilink")[水稻生产中心](../Page/水稻.md "wikilink")。

## 典籍中的三韩

《後漢書·東夷列傳》第七十五：「韓有三種：一曰馬韓、二曰辰韓、三曰弁辰。馬韓在西，有五十四國，其北與樂浪，南與倭接，辰韓在東，十有二國，其北與[濊貊接](../Page/濊貊.md "wikilink")。弁辰在辰韓之南，亦十有二國，其南亦與[倭接](../Page/倭.md "wikilink")。凡七十八國，伯濟是其一國焉。大者萬余戶，小者數千家，各在山海間，地合方四千余里，東西以海為限，皆古之[辰國也](../Page/辰國.md "wikilink")。馬韓最大，共立其種為辰王，都目支國，盡王三韓之地。其諸國王先皆是馬韓種人焉。」

雖然[朝鮮半島古代國名多變](../Page/朝鮮半島.md "wikilink")，但是「韓人」、「韓民」之稱一直在民間延續，在中韓兩國的法律文書中也時有出現，比如：19世紀80年代的《中朝商民水陸貿易章程》中就有「華民」、「韓人」之稱，19世紀[甲午戰爭前夕](../Page/甲午戰爭.md "wikilink")，清光緒帝的《[對日宣戰詔書](../Page/:s:光緒二十年對日宣戰詔書.md "wikilink")》中，也有「著李鴻章嚴飭派出各軍，迅速進剿，厚集雄師，陸續進發，以拯**韓民**于塗炭」之稱。

## 與漢四郡的關係

[Ancient_Korea_Taihougun.png](https://zh.wikipedia.org/wiki/File:Ancient_Korea_Taihougun.png "fig:Ancient_Korea_Taihougun.png")
[Map_of_The_east_barbarian_0.png](https://zh.wikipedia.org/wiki/File:Map_of_The_east_barbarian_0.png "fig:Map_of_The_east_barbarian_0.png")**三韩**諸国与汉四郡位置。\]\]
[汉四郡中的](../Page/汉四郡.md "wikilink")[乐浪郡似乎与三韩的每个城邦都有联系](../Page/乐浪郡.md "wikilink")。

[真番郡治](../Page/真番郡.md "wikilink")[霅縣與其餘無考的七縣為開](../Page/霅縣.md "wikilink")[辰國北部所置](../Page/辰國.md "wikilink")。[三國志魏志馬韓傳云](../Page/三國志.md "wikilink")：“從事吳林以樂浪本統三韓，分割辰韓八國以與樂浪。”韓國學者[李丙燾認為](../Page/李丙燾.md "wikilink")，此處“辰韓八國”可能就是真番郡南部沒入辰國的八縣，包括霅縣\[4\]。

昭帝[始元五年](../Page/始元.md "wikilink")（前82年），罷真番郡，以北部七縣入樂浪郡，隸屬于南部都尉。霅縣及其南部無考的七縣可能于此時沒于辰國，後入馬韓。[獻帝](../Page/漢獻帝.md "wikilink")[建安中](../Page/建安_\(漢獻帝\).md "wikilink")，割據[遼東的公孫康分樂浪郡屯有縣以南地置](../Page/遼東.md "wikilink")[帶方郡](../Page/帶方郡.md "wikilink")\[5\]，辖带方、列口、長岑、昭明、含資、提奚、海冥七縣\[6\]，即西漢時真番郡北部故地。

[魏](../Page/曹魏.md "wikilink")[齊王](../Page/曹芳.md "wikilink")[正始年閒](../Page/正始.md "wikilink")，魏國遣[毌丘儉等伐](../Page/毌丘儉.md "wikilink")[辰韓](../Page/辰韓.md "wikilink")，恢復了對臨屯郡故地的統治，東濊、辰韓諸部落多由帶方郡管轄。[西晉](../Page/西晉.md "wikilink")[太康中改昭明縣為南新縣](../Page/太康_\(晉\).md "wikilink")\[7\]。[晉愍帝](../Page/晉愍帝.md "wikilink")[建興二年](../Page/建兴_\(晋愍帝\).md "wikilink")（314年），帶方郡沒入[高句麗](../Page/高句麗.md "wikilink")。此後中原王朝在朝鮮半島的直接統治暫時中斷，直至[唐代初年滅高句麗](../Page/唐代.md "wikilink")。

三韩与汉四郡有着广泛的贸易往来。朝鲜半岛各地都有[汉朝的钱币出土](../Page/汉朝.md "wikilink")。

## 與古朝鮮的關係

中国史书《[尚书大传](../Page/尚书大传.md "wikilink")》是最早记载[商周时代的移民](../Page/商周.md "wikilink")[箕子朝鲜方国的文献](../Page/箕子朝鲜.md "wikilink")，同时也是最早提及朝鲜的文献，据推断大约在公元前900年左右。朝鮮有史料記載的年代遠遠長過三韓，卫满朝鲜時曾經阻止三韓等部落朝貢[漢朝](../Page/漢朝.md "wikilink")。

## 與今韓國的關係

1897年[甲午戰爭清朝戰敗后](../Page/甲午戰爭.md "wikilink")，[日本与中国](../Page/日本.md "wikilink")[清朝签署](../Page/清朝.md "wikilink")[馬关条約之後](../Page/馬关条約.md "wikilink")，中國承認朝鮮是自主独立國家，不再主張朝鮮之[宗主國](../Page/宗主國.md "wikilink")，1897年，朝鮮王宣布建制稱帝，[李氏朝鮮将本国国号改为](../Page/李氏朝鮮.md "wikilink")「[大韩帝国](../Page/大韩帝国.md "wikilink")」，「韓」第一次從民間進入國號。

1910年大日本帝国将[韓國併合後](../Page/韓國併合.md "wikilink")、该地域的称呼被改回为「朝鮮」。之后的1919年，[李承晩](../Page/李承晩.md "wikilink")（後来韩国的首任总统）、[金九等独立運動家流亡中國後成立了](../Page/金九.md "wikilink")[大韩民国临时政府](../Page/大韩民国临时政府.md "wikilink")，该[流亡政府的名称采用了](../Page/流亡政府.md "wikilink")「大韓」的名称。

## 轶话

《[三国史记](../Page/三国史记.md "wikilink")》，《[三国遗事](../Page/三国遗事.md "wikilink")》和《[帝王韵记](../Page/帝王韵记.md "wikilink")》将马韩说成是[高句丽](../Page/高句丽.md "wikilink")。此观点最早由[新罗儒学和历史学者](../Page/新罗.md "wikilink")[崔致遠提出的](../Page/崔致遠.md "wikilink")。

[李氏朝鲜晚期的](../Page/李氏朝鲜.md "wikilink")“实学”学者则强调在地理上讲马韩应用[百济更为相关](../Page/百济.md "wikilink")。

“三韩”一般为古代[朝鲜半岛南部的](../Page/朝鲜半岛.md "wikilink")[马韩](../Page/马韩.md "wikilink")、[辰韩](../Page/辰韩.md "wikilink")、[弁韩之总称](../Page/弁韩.md "wikilink")，后泛指[朝鲜](../Page/朝鲜.md "wikilink")。但是在一些汉人史书或者小说笔记中也常指代[辽东其他民族](../Page/辽东.md "wikilink")。

在[唐朝时一些投靠唐朝的](../Page/唐朝.md "wikilink")[高句丽族或者](../Page/高句丽族.md "wikilink")[靺鞨将领也在历史上被称为](../Page/靺鞨.md "wikilink")“三韩贵种”，如[李多祚](../Page/李多祚.md "wikilink")。[辽](../Page/辽.md "wikilink")[开泰](../Page/开泰.md "wikilink")（公元1012—1020年）中，[辽圣宗伐](../Page/辽圣宗.md "wikilink")[高丽](../Page/高丽.md "wikilink")，以俘户置[高州](../Page/高州.md "wikilink")，又以其中三韩遗员置[三韩县](../Page/三韩县.md "wikilink")，属[中京道](../Page/中京道.md "wikilink")。[金属](../Page/金.md "wikilink")[北京路](../Page/北京路.md "wikilink")[大定府](../Page/大定府.md "wikilink")，址在今之[内蒙古](../Page/内蒙古.md "wikilink")[赤峰市东](../Page/赤峰市.md "wikilink")。[顾炎武](../Page/顾炎武.md "wikilink")《日知录·外国·三韩》条谓：“今人谓辽东为三韩者，……原其故，本于天启初失辽阳以后，奏章之文遂有谓辽人为三韩者，外之也。今江人乃以之自称，夫亦自外也矣。”

“古营州”人，“三韩人”在辽金以后的中国东北文人笔下乃指辽东。清以来，常以“三韩”作辽东之代称。如[曹寅祖籍](../Page/曹寅.md "wikilink")[辽阳](../Page/辽阳.md "wikilink")，[韩炎](../Page/韩炎.md "wikilink")《有怀堂文稿》卷六《织造曹使君寿序》谓“以余所见，三韩曹使君子清，乃诚善读书者”，可为例证。在清人所著[满洲源流考中](../Page/满洲源流考.md "wikilink")，三韩被认为是三汗的讹传，认为三韩为满族人的来源之一，地理位置在“[奉天东北](../Page/奉天.md "wikilink")，吉林一带，壤接朝鲜”，辽代也在此设立国[三韩县](../Page/三韩县.md "wikilink")。

《[日本書紀](../Page/日本書紀.md "wikilink")》中也将百濟、新羅、高句麗三国称呼为[三韓](../Page/三韓.md "wikilink")。

## 註釋及徵引文獻

<div class="references-small">

<references />

</div>

## 參考書目

  - 王先謙，《後漢書集解》，中華書局影印虛受堂本
  - 譚其驤等，1974年，《中國歷史地圖集》，北京：中国地图出版社
  - 周振鶴，1987年，《西漢政區地理》，北京：人民出版社
  - 李曉傑，1999年，《東漢政區地理》，濟南：山東教育出版社

<!-- end list -->

  - Kim, J.-B. (1974). Characteristics of Mahan in ancient Korean
    society. *Korea Journal* 14(6), 4-10.
    [1](https://web.archive.org/web/20050530105415/http://www.ekoreajournal.net/archive/detail.jsp?VOLUMENO=14&BOOKNUM=6&PAPERNUM=1)

<!-- end list -->

  - Lee, K.-b. (1984). *A new history of Korea.* Tr. by E.W. Wagner &
    E.J. Schulz, based on 1979 rev. ed. Seoul: Ilchogak. ISBN
    89-337-0204-0

<!-- end list -->

  - Yi, H.-h. (2001). International trade system in East Asia from the
    first to the fourth century. *Korea Journal* 41(4), 239-268.
    [2](https://web.archive.org/web/20050530102744/http://www.ekoreajournal.net/archive/detail.jsp?VOLUMENO=41&BOOKNUM=4&PAPERNUM=11)

[Category:東北历史](../Category/東北历史.md "wikilink")
[Category:朝鲜半岛历史](../Category/朝鲜半岛历史.md "wikilink")
[Category:前三國時期](../Category/前三國時期.md "wikilink")
[\*](../Category/三韓時期.md "wikilink")

1.  《后汉书·东夷列传》第七十五：“韩有三种”。
2.  《后汉书/卷85》：马韩最大，共立其种为辰王，都目支国，尽王三韩之地。
3.  《后汉书/卷85》：（三韩）
    凡七十八国，伯济是其一国焉。大者万余户，小者数千家，各在山海闲，地合方四千余里，东西以海为限，皆古之辰国也。
4.  李丙燾，《真番郡考》，周一良譯，《禹貢》期刊2-{卷}-10期
5.  三國志魏志東夷傳
6.  [續漢書郡國志](../Page/續漢書.md "wikilink")
7.  [晉書地理志](../Page/晉書.md "wikilink")