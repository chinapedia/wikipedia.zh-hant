[Infineon_HYB18T256161AF-28_(GDDR2_SDRAM).jpg](https://zh.wikipedia.org/wiki/File:Infineon_HYB18T256161AF-28_\(GDDR2_SDRAM\).jpg "fig:Infineon_HYB18T256161AF-28_(GDDR2_SDRAM).jpg")\]\]

**英飞凌**科技股份有限公司（**Infineon
Technologies**，）总部位于德国慕尼黑，主力提供半导体和系统解决方案，解决在高能效、移动性\[1\]和安全性\[2\]方面帶來的挑战（而主要業務亦包括為關連公司[西門子交通集團生產鐵路機車車輛牽引系統內](../Page/西門子交通集團.md "wikilink")[IGBT](../Page/IGBT.md "wikilink")-[VVVF之半導體組件](../Page/VVVF.md "wikilink")）。

## 历史

2012财年（截止于9月30日），公司报告的销售额达 39 亿欧元，在全球拥有约 26700
名员工。英飞凌在法兰克福证券交易所（股票代码：IFX）和美国柜台交易市场
OTCQX International
Premier（股票代码：IFNNY）挂牌上市。其前身是[西门子集团的半导体部门](../Page/西門子公司.md "wikilink")（Siemens
Semiconductor），于1999年独立，2000年[上市](../Page/上市.md "wikilink")。中文名曾被称为**亿恒科技**，2002年起更至現名。其無線解決方案部門在2010年8月售給[英特爾](../Page/英特爾.md "wikilink")\[3\]。

## 参考文献

## 外部链接

  - [英飞凌](http://www.infineon.com)
  - [英飞凌中国](http://www.infineon.com.cn)

## 参见

  - [全球二十大半导体厂商](../Page/全球二十大半导体厂商.md "wikilink")

{{-}}

[Category:西门子公司](../Category/西门子公司.md "wikilink")
[Category:德國電子公司](../Category/德國電子公司.md "wikilink")
[Category:半導體公司](../Category/半導體公司.md "wikilink")
[Category:慕尼黑公司](../Category/慕尼黑公司.md "wikilink")

1.
2.
3.