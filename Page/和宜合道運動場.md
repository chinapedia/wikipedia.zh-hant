**和宜合道運動場**（）是[香港一個](../Page/香港.md "wikilink")[田徑](../Page/田徑.md "wikilink")[運動場](../Page/運動場.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[葵涌](../Page/葵涌.md "wikilink")[和宜合道](../Page/和宜合道.md "wikilink")298號，為「北葵涌體育中心」（North
Kwai Chung Sports
Centre）三項毗連的體育設施之一，其他還有[北葵涌賽馬會游泳池及](../Page/北葵涌賽馬會游泳池.md "wikilink")[北葵涌鄧肇堅體育館](../Page/北葵涌鄧肇堅體育館.md "wikilink")，是[康樂及文化事務署管理下草坪面積最小的真草田徑運動場](../Page/康樂及文化事務署.md "wikilink")，為[葵青區三個室外運動場之一](../Page/葵青區.md "wikilink")，主要服務對像為[北葵涌地區的居民](../Page/北葵涌.md "wikilink")。

## 歷史

1982至83年，運動場是[無線電視劇集](../Page/無線電視.md "wikilink")[奔向太陽的主要拍攝場地之一](../Page/奔向太陽.md "wikilink")。本劇由國際巨星[劉德華領銜主演](../Page/劉德華.md "wikilink")。劇集主題曲:[歡呼聲](../Page/歡呼聲.md "wikilink")，主唱者:[蔡楓華](../Page/蔡楓華.md "wikilink")。

## 設施

和宜合道運動場設有一個真草足球場和一條6線，由300米至340米長的跑道，配套設施有更衣室及720個座位的有蓋看台。

### 高爾夫球練習場

草地足球場亦會作[高爾夫球練習場使用](../Page/高爾夫球.md "wikilink")，康文署轄下4項高爾夫球設施之一，於1995年9月14日啟用，設有15條球道，發球距離最長可達60碼。

[File:HK_WoYiHopRoadSportsGround_GrassPatch.JPG|球場草坪](File:HK_WoYiHopRoadSportsGround_GrassPatch.JPG%7C球場草坪)
[File:HK_WoYiHopRoadSportsGround_Golf.JPG|高爾夫球練習圍網](File:HK_WoYiHopRoadSportsGround_Golf.JPG%7C高爾夫球練習圍網)
[File:HK_WoYiHopRoadSportsGround_Stand.JPG|細小的看台](File:HK_WoYiHopRoadSportsGround_Stand.JPG%7C細小的看台)
[File:HK_WoYiHopRoadSportsGround_RunningTracks.JPG|不合規格的跑道](File:HK_WoYiHopRoadSportsGround_RunningTracks.JPG%7C不合規格的跑道)

## 變遷

跟其他早期設立的運動場一樣，和宜合道運動場早年的跑道是由瀝青鋪成。約在1987年後才改為鋪設全天候跑道。

由於運動場規模較小，不少使用該運動場作陸運會的學校，唯有將400米及4X100米接力賽的部份賽道重疊，因此參與該類賽跑的選手，必須在看台前的100米直路跑道多跑一次。

## 大型活動

  - 仁濟慈善行：[仁濟醫院一年一度的大型籌款項目](../Page/仁濟醫院.md "wikilink")，參加人數達萬人，於「和宜合道運動場」舉行起步禮及起步點\[1\]。

## 交通

## 保養日及緩步跑時間

逢星期四為運動場保養日，而每天06:15-22:30為緩步跑時間。

## 参考資料

## 外部連結

  - [康文署葵青區運動場](https://www.lcsd.gov.hk/tc/facilities/facilitieslist/facilities.php?fid=851)
      - [和宜合道高爾夫球練習場](https://web.archive.org/web/20070827152756/http://www.lcsd.gov.hk/b5/ls_golf_woyihop.php)

[Category:葵涌](../Category/葵涌.md "wikilink")
[Category:香港運動場](../Category/香港運動場.md "wikilink")

1.  [「仁濟慈善行2006」](http://www.yanchai.org.hk/html/walk2006.htm)