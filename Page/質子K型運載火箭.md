**质子-K运载火箭**（[俄语](../Page/俄语.md "wikilink")：Прото́н-К）[苏联](../Page/苏联.md "wikilink")[质子号运载火箭家族中的三级型号](../Page/质子号运载火箭.md "wikilink")。[美国国防部对这种火箭的代号是](../Page/美国国防部.md "wikilink")“SL-13”，[美国国会的](../Page/美国国会.md "wikilink")[谢尔顿命名法](../Page/谢尔顿命名法.md "wikilink")（用于识别火箭的衍生型号）则称其为“D-1”。此火箭主要用来发射[空间站](../Page/空间站.md "wikilink")。

在[切洛勉的](../Page/切洛勉.md "wikilink")[UR500](../Page/UR500.md "wikilink")（即两级型质子号）基础上发展三级火箭的计划在1964年就已经决定了。当时为这种新火箭安排的任务是发射“[金刚石](../Page/金刚石军用空间站.md "wikilink")”军用空间站（其中实际发射成功的都被编进了礼炮号的编号序列）和[LK-1载人环月飞船](../Page/LK-1.md "wikilink")（此飞船后来被取消）。但火箭的发展出了一些问题，主要是可靠性一直达不到要求。第一枚带有第三级的质子号于1967年3月10日发射，但这是一枚带有“[D组级](../Page/D组级.md "wikilink")”上面级的四级火箭；不带上面级的质子-K于1968年11月16日首次发射，将[质子4号科学卫星](../Page/质子4号.md "wikilink")（重达16吨）送入太空。

以下是由质子-K发射成功的空间站。

礼炮号系列：[礼炮1号](../Page/礼炮1号.md "wikilink")，[礼炮2号](../Page/礼炮2号.md "wikilink")，[礼炮3号](../Page/礼炮3号.md "wikilink")，[礼炮4号](../Page/礼炮4号.md "wikilink")，[礼炮5号](../Page/礼炮5号.md "wikilink")，[礼炮6号](../Page/礼炮6号.md "wikilink")，[礼炮7号](../Page/礼炮7号.md "wikilink")；

TKS空间舱系列：[宇宙-929](../Page/宇宙-929.md "wikilink")，[宇宙-1267](../Page/宇宙-1267.md "wikilink")，[宇宙-1443](../Page/宇宙-1443.md "wikilink")，[宇宙-1686](../Page/宇宙-1686.md "wikilink")（注意这些舱段通常是对接到礼炮号空间站上作为后者的组件）；

[和平号空间站的](../Page/和平号空间站.md "wikilink")6个组件：[核心舱](../Page/和平号空间站核心舱.md "wikilink")，[量子1号天文物理舱](../Page/量子1号.md "wikilink")，[量子2号气闸舱](../Page/量子2号.md "wikilink")，“[晶体](../Page/晶体号实验舱.md "wikilink")”号实验舱，“[光谱](../Page/光谱号遥感舱.md "wikilink")”号遥感舱，“[自然](../Page/自然号地球观测舱.md "wikilink")”号地球观测舱；

[国际空间站的两个组件](../Page/国际空间站.md "wikilink")：[曙光号功能货舱和](../Page/曙光号功能货舱.md "wikilink")[星辰号服务舱](../Page/星辰号服务舱.md "wikilink")。

截止2000年，质子-K共发射31次，其中失败4次。

## 资料来源

  - [质子-K](http://www.astronautix.com/lvs/pro8k82k.htm)
  - [赫鲁尼切夫航天中心：质子-K](http://www.khrunichev.ru/khrunichev/live/full_raket.asp?id=13184)
  - 《中国航天》，2004年10月号，26页

[category:质子号运载火箭](../Page/category:质子号运载火箭.md "wikilink")