**荃灣選舉聯盟**是[2003年香港區議會選舉中](../Page/2003年香港區議會選舉.md "wikilink")，一個非正式的參選[政治聯盟](../Page/政治聯盟.md "wikilink")。廣義而言不算政治組織，但是政治理念相同的候選人，取用同一個政治政綱共同參選，其中領頭的是[立法會議員](../Page/立法會.md "wikilink")[陳偉業](../Page/陳偉業.md "wikilink")。

## 參選區域

顧名思義，荃灣選舉聯盟就是以行政區[荃灣為主的選舉聯盟組織](../Page/荃灣.md "wikilink")，但除了在荃灣的行政區參選外，同時亦推出候選人在[葵青區參選](../Page/葵青區.md "wikilink")；而[陳偉業同樣在新界西北結合當地親](../Page/陳偉業.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")（主要為前[一二三民主聯盟成員](../Page/一二三民主聯盟.md "wikilink")、[港九工團聯合總會成員](../Page/港九工團聯合總會.md "wikilink")）的地方政治人物，如[元朗區議員](../Page/元朗.md "wikilink")[麥業成](../Page/麥業成.md "wikilink")、前[屯門區議員](../Page/屯門.md "wikilink")[宋景輝等組成](../Page/宋景輝.md "wikilink")「[元朗天水圍民主陣線](../Page/元朗天水圍民主陣線.md "wikilink")」。

## 政治立場

統一以「[獨立民主派](../Page/獨立民主派.md "wikilink")」身份登記參選，但向外宣傳則採取統一式樣的文宣；而且均以要求爭取2007年、2008年雙普選、取消區議會委任制為政治政綱。

## 參選人物

荃灣選舉聯盟的候選人多數為[荃灣及](../Page/荃灣.md "wikilink")[葵青區內非民主黨的](../Page/葵青.md "wikilink")[泛民主派無黨籍候選人](../Page/泛民主派.md "wikilink")，而且主要在陳偉業立法會議員辦事處充任公職的荃灣地方政治人物，如[王化民](../Page/王化民.md "wikilink")、[陳惠蘭等](../Page/陳惠蘭.md "wikilink")；現任議員則包括[陳偉業本人](../Page/陳偉業.md "wikilink")、[李洪波及曾加入](../Page/李洪波.md "wikilink")[民主黨](../Page/民主黨.md "wikilink")，現已轉投[公民黨的](../Page/公民黨.md "wikilink")[陳琬琛](../Page/陳琬琛.md "wikilink")。另外，[葵青區的](../Page/葵青區.md "wikilink")[劉偉傑及](../Page/劉偉傑.md "wikilink")[潘小屏均屬此聯盟的成員](../Page/潘小屏.md "wikilink")。

## 選舉結果(2007年區議會選舉)

註：所有候選人均以2003年當選並競逐連任者為準， <政黨>是在2003年區議會選舉時以荃灣選舉聯盟名義參選，競逐連任時以所屬的政黨名義參選，
粗體是2003年區議會選舉時以荃灣選舉聯盟名義參選，競逐連任時以獨立名義參選。

  - 荃灣中心選區

<table style="width:28%;">
<colgroup>
<col style="width: 27%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

|}

  - 麗興選區

<table style="width:28%;">
<colgroup>
<col style="width: 27%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

|}

  - 梨木樹東選區

<table style="width:28%;">
<colgroup>
<col style="width: 27%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

|}

  - 梨木樹西選區

<table style="width:28%;">
<colgroup>
<col style="width: 27%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

|}

  - 葵青區青發選區

<table style="width:28%;">
<colgroup>
<col style="width: 27%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

|}

## 資料來源

[香港特別行政區選舉管理委員會](https://web.archive.org/web/20050903031746/http://www.elections.gov.hk/elections/dc2003/tc_chi/result/result_tw.html)

[Category:香港選舉](../Category/香港選舉.md "wikilink")
[Category:香港泛民主派組織](../Category/香港泛民主派組織.md "wikilink")
[Category:荃灣區](../Category/荃灣區.md "wikilink")