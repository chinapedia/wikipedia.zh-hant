[吳大澂1.jpg](https://zh.wikipedia.org/wiki/File:吳大澂1.jpg "fig:吳大澂1.jpg")
**吳大**（），[字](../Page/表字.md "wikilink")**止敬**，又字**清卿**，[号](../Page/号.md "wikilink")**恒轩**，又号**愙齋**，[吳縣](../Page/吳縣.md "wikilink")（今[江蘇](../Page/江蘇.md "wikilink")[蘇州](../Page/蘇州.md "wikilink")）人。[清代學者](../Page/清代.md "wikilink")、[金石學家](../Page/金石學家.md "wikilink")、[書畫家](../Page/書畫家.md "wikilink")、官员。

## 生平

[同治七年](../Page/同治.md "wikilink")（1868年）考上[進士](../Page/進士.md "wikilink")，初授編修，後歷任陝西學政、[河北道](../Page/河北.md "wikilink")、[太仆寺卿](../Page/太仆寺.md "wikilink")、左副都[御史](../Page/御史.md "wikilink")。[光绪十一年](../Page/光绪.md "wikilink")（1885年），诏赴吉林，先会见宁古塔副统[容山](../Page/容山.md "wikilink")，后会同珲春副都统[依克唐阿与俄国使臣重勘](../Page/依克唐阿.md "wikilink")[珲春黑顶子地边界](../Page/珲春.md "wikilink")，援[咸丰十一年旧界图](../Page/咸丰.md "wikilink")，立碑五座，建铜柱，自篆铭曰：“疆域有表国有维，此柱可立不可移。”收回侵界，“而船之出入[图们江者亦卒以通航无阻](../Page/图们江.md "wikilink")。”

[光緒十二年](../Page/光緒.md "wikilink")（1886年）擢[廣東](../Page/廣東.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")。光緒十四年（1888年）署[河南](../Page/河南.md "wikilink")[山東](../Page/山東.md "wikilink")[河道總督](../Page/河道總督.md "wikilink")，修[黃河水患](../Page/黃河.md "wikilink")，用[水泥砌築磚石壩](../Page/水泥.md "wikilink")，加固“[鄭州十堡](../Page/鄭州十堡.md "wikilink")”工程。光緒十五年（1889年）請用新法測繪黃河圖，[光緒帝命名](../Page/光緒帝.md "wikilink")《御覽三省黃河全圖》。光緒十八年（1892年）授[湖南巡撫](../Page/湖南.md "wikilink")。光緒二十年（1894年）[中日甲午戰爭中](../Page/中日甲午戰爭.md "wikilink")，自请率领湘军出关应战，戰敗于[海城](../Page/海城.md "wikilink")，罷歸，返回湖南，不久受命开缺。

光緒二十四年（1898年），朝廷再降旨，將其革職，永不叙用。光緒二十八年（1902年）卒，年六十八。《清史稿》有傳。\[1\]

## 著作

吳大澂善畫山水、花卉，尤精於[篆書](../Page/篆書.md "wikilink")，少從[陳碩甫學篆](../Page/陳碩甫.md "wikilink")，書法酷似[李陽冰](../Page/李陽冰.md "wikilink")，後受[楊沂孫啟發](../Page/楊沂孫.md "wikilink")，結合[小篆與](../Page/小篆.md "wikilink")[金文](../Page/金文.md "wikilink")，自成一家。作品有《愙齋集古錄》、《古字說》、《權衡度量考》、《恆軒所見所藏吉金錄》等。

## 家族

  - 弟弟[吳大衡](../Page/吳大衡.md "wikilink")。
  - 吳大澂為[汪鸣銮的姨表兄](../Page/汪鸣銮.md "wikilink")，[吴湖帆的祖父](../Page/吴湖帆.md "wikilink")，[姻親中較出名是](../Page/姻親.md "wikilink")[張之洞](../Page/張之洞.md "wikilink")，[袁世凯](../Page/袁世凯.md "wikilink")。
  - 女儿吴本娴嫁给[袁世凯的长子](../Page/袁世凯.md "wikilink")[袁克定](../Page/袁克定.md "wikilink")。
  - 女儿吳本靜嫁給[費樹蔚](../Page/費樹蔚.md "wikilink")。
  - 侄兒吳本善之女相繼嫁給[張是彝](../Page/張是彝.md "wikilink")。吳本善之子吳湖帆過繼給吳大澂爲嗣孫。

## 註釋

<div class="references-small">

<references />

</div>

## 參考文獻

  - 趙爾巽等.《清史稿》. 中華書局點校本.
  - 白谦慎：〈[吴大澂与全形拓](http://www.nssd.org/articles/article_read.aspx?id=664211014)〉。

{{-}}

<table>
<thead>
<tr class="header">
<th><p><strong>官衔</strong>    </p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝陝西學政](../Category/清朝陝西學政.md "wikilink")
[Category:清朝太僕寺卿](../Category/清朝太僕寺卿.md "wikilink")
[Category:清朝湖南巡抚](../Category/清朝湖南巡抚.md "wikilink")
[Category:清朝東河總督](../Category/清朝東河總督.md "wikilink")
[Category:清朝廣東巡撫](../Category/清朝廣東巡撫.md "wikilink")
[Category:清朝書法家](../Category/清朝書法家.md "wikilink") [Category:中俄关系
(清朝)](../Category/中俄关系_\(清朝\).md "wikilink")
[Category:吳縣人](../Category/吳縣人.md "wikilink")
[D](../Category/吳姓.md "wikilink")

1.  [《清史稿·列传二百三十七》](http://www.guoxue.com/shibu/24shi/qingshigao/qsgx_450.htm)