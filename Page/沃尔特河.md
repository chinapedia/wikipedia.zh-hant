**沃尔特河**（；，又译做**伏塔河**）是[西非的](../Page/西非.md "wikilink")[河流](../Page/河流.md "wikilink")，[加纳的主要饮用水资源](../Page/加纳.md "wikilink")。沃尔特河汇合三条支流，分别为[红沃尔特河](../Page/红沃尔特河.md "wikilink")、[黑沃尔特河](../Page/黑沃尔特河.md "wikilink")、和[白沃尔特河](../Page/白沃尔特河.md "wikilink")。1960年代因为在沃尔特河上兴建的[阿可桑布大坝而成的](../Page/阿可桑布大坝.md "wikilink")[沃尔特水库是世界最大的](../Page/沃尔特水库.md "wikilink")[人工水库之一](../Page/水库.md "wikilink")。

## 地理

[thumb](../Page/图像:Woertehe-hong_bai_hei-zh.png.md "wikilink")
沃尔特河大致呈南北走向，干流位于加纳，由北向南经沃尔特水库在加纳的阿达市汇入大西洋。

### 水文

#### 径流

### 生物

#### 鱼类

#### 植物

## 水利工程

1961年至1965年，由世界银行等投资，美国公司设计，意大利公司施工，加纳政府在沃尔特河的阿科松博峡谷，建起一座弧形拦河大坝，使千百年来类似脱缰野马恣意奔腾的沃尔特河变得温顺，形成狭长广阔的水域，在大坝上方克瓦胡高原与阿克瓦皮姆山区之间出现阿科松博湖这一奇特景观。坝高141米，长660米，水面以上74米，体积794万立方米。

## 參考文獻

## 外部链接

  - [曲折之河：沃尔特河](http://www.waterchina.com/main/Web/Article/2006/12/31/1054055781C79576.aspx)（中国水星）

[Category:幾內亞灣](../Category/幾內亞灣.md "wikilink")
[Category:非洲跨國河流](../Category/非洲跨國河流.md "wikilink")
[Category:布吉納法索河流](../Category/布吉納法索河流.md "wikilink")
[Category:迦納河流](../Category/迦納河流.md "wikilink")
[Category:象牙海岸河流](../Category/象牙海岸河流.md "wikilink")