[Zhang_zuolin_in_1928.jpg](https://zh.wikipedia.org/wiki/File:Zhang_zuolin_in_1928.jpg "fig:Zhang_zuolin_in_1928.jpg")
**皇姑屯事件**是[奉系军阀首领](../Page/奉系军阀.md "wikilink")[张作霖在铁路上被](../Page/张作霖.md "wikilink")[暗殺的事件](../Page/暗殺.md "wikilink")。
1928年6月4日5点30分，张作霖乘坐专列经过[京奉](../Page/京奉鐵路.md "wikilink")、[南满铁路交叉处的皇姑屯车站三孔桥时](../Page/南满铁路.md "wikilink")，火车被预埋的[炸藥炸毁](../Page/炸藥.md "wikilink")，张作霖被炸成重伤，送回[沈阳后](../Page/沈阳.md "wikilink")，于当日死去。案發[皇姑屯站以东](../Page/皇姑屯站.md "wikilink")，史称**皇姑屯事件**。當時在日本國內，由於沒有公佈兇手，日本政府一直以「**滿洲某重大事件**」代稱。

## 事件经过

[Huanggutun_Incident.jpg](https://zh.wikipedia.org/wiki/File:Huanggutun_Incident.jpg "fig:Huanggutun_Incident.jpg")
[Huanggutun_Incident01.PNG](https://zh.wikipedia.org/wiki/File:Huanggutun_Incident01.PNG "fig:Huanggutun_Incident01.PNG")
张作霖是[奉系的最高](../Page/奉系.md "wikilink")[领袖](../Page/领袖.md "wikilink")，是统治[中国东北的](../Page/中国东北.md "wikilink")[軍閥](../Page/軍閥.md "wikilink")，人稱“东北王”、“老帅”，勢力不斷在[山海關](../Page/山海關.md "wikilink")[关内扩张](../Page/中國本土.md "wikilink")，于1927年6月18日在[北京建立了](../Page/北京.md "wikilink")[安国军政府](../Page/安国军政府.md "wikilink")，自称[中华民国陆海军大元帅](../Page/中华民国.md "wikilink")，成为[北洋政府末代统治者](../Page/北洋政府.md "wikilink")，也是当时尚未被国际承认的中华民国元首。1928年，张作霖抵挡不住“[國民革命軍北伐](../Page/國民革命軍北伐.md "wikilink")”攻打奉军，只得向[南京國民政府通电求和](../Page/南京國民政府.md "wikilink")，后于6月3日夜乘[火车返回沈阳](../Page/火车.md "wikilink")。

1928年6月4日凌晨5点30分左右，当张作霖乘坐的火車行驶至沈阳西北皇姑屯车站以东，[南满铁路](../Page/南满铁路.md "wikilink")（该路由日本控制）与[京奉铁路交叉处三洞桥](../Page/京奉铁路.md "wikilink")（桥上是南满铁路，下方是京奉铁路）时，预先埋设的炸药在此引爆了。张作霖所坐为第八节车，后来公开发表的日军中尉神田泰之助保留下来的照片显示，在第八，九，十节车进入桥下时引爆炸药，使上方桥梁的钢板下塌，将张作霖专列车厢压碎，黑龙江督军[吴俊升当场死亡](../Page/吳俊升_\(民國軍政人物\).md "wikilink")，张作霖被甩出车外受重伤，急救回帅府，9时30分伤重去世，但秘不发丧。其子[张学良从前线动身](../Page/张学良.md "wikilink")，于6月18日赶回沈阳\[1\]，稳定了东北局势，直到張學良21日繼承父親職務後，才正式公開發喪。

根据直接参與布置暗杀的[河本大作自述](../Page/河本大作.md "wikilink")，日军独立守备队第二大队的神田泰之助中尉、富田大尉负责勘定地点；工兵中尉队长桐原貞寿等人装置电流炸药；日军独立守备队第四中队长[东宫铁男大尉负责控制引爆](../Page/东宫铁男.md "wikilink")。日本关东军派自[朝鲜军调遣来的工兵](../Page/朝鲜军.md "wikilink")，在铁路交叉点上，工作6个小时，将120公斤黄色炸药，分装在30个麻袋内，装置在铁路交叉点桥墩上面的两处地方，设置了两道爆炸装置。在桥墩500米外的望台上设有电线按钮，以控制触发爆炸。\[2\]

## 幕后主謀

根據現存檔案以及關東軍密信
\[3\]，由於日本在支持张作霖（如镇压[郭松龄及入关夺取政权](../Page/郭松龄.md "wikilink")）後希望在中国东北享有各种特权，但张作霖並未盡力履行甚至背弃先前对日本作出的承诺，因而引起日本不滿。在时任日本首相的[田中义一拒绝批准关东军对](../Page/田中义一.md "wikilink")[东北军进行缴械後](../Page/东北军.md "wikilink")，关东军遂决定私自策划暗杀张作霖，以打乱奉系的指挥系统，使东北陷入混乱，以便出兵占领\[4\]。暗殺由关东军高级参谋[河本大作大佐具体制定详细计划](../Page/河本大作.md "wikilink")，奉天特务机关长[秦真次](../Page/秦真次.md "wikilink")、[土肥原贤二参与策划](../Page/土肥原贤二.md "wikilink")，並由时任关东军司令官的[村冈长太郎中将下達命令](../Page/村冈长太郎.md "wikilink")；河本大作和村冈长太郎后来亦因皇姑屯事件被编入预备役。据《小矶回忆录》记载，1928年6月16日河本大作从中国东北回到日本东京，对[小矶国昭私下吐露自己是谋杀张作霖的真凶](../Page/小矶国昭.md "wikilink")，並在自己死後出版的回憶錄《我杀張作霖》中承認殺死了張作霖。\[5\]

张作霖死后，日本未能立刻占领[满洲](../Page/中国东北地区.md "wikilink")，日本政府为了掩盖真相，谎称皇姑屯事件系“南方國民政府便衣队员”所为。案发之后，日本[陆军省反对公布本案的真相](../Page/陆军省.md "wikilink")，以免影响日本的国家形象並進一步刺激當時中國強烈的[反日情緒](../Page/反日.md "wikilink")，使中日雙方爆發軍事衝突。

1946年，[远东国际军事法庭审理日本](../Page/远东国际军事法庭.md "wikilink")[战犯时](../Page/战犯.md "wikilink")，就是将皇姑屯事件视为日本侵华的起点。因为张作霖是当时仍為國際公認的中国[国家元首](../Page/中華民國國家元首列表.md "wikilink")，谋杀一国元首理应是违反[国际法](../Page/国际法.md "wikilink")，屬[战争罪行为](../Page/战争罪.md "wikilink")。根据[东京审判记录田中内阁时期的海相](../Page/东京审判.md "wikilink")[冈田启介的证词表明](../Page/冈田启介.md "wikilink")，张作霖一面接受日军的援助，一面卻在北京向英、美示好，因此被日本陆军铲除。原日本原陆军省兵务局局长、陆军中将[田中隆吉作证](../Page/田中隆吉.md "wikilink")：“1928年6月3日，在南满铁路和京奉线交叉处，河本大佐带领他的手下，爆破了北京开来的列车，张作霖就坐在那辆列车上。第二天，张作霖就死了。”\[6\]

1945年，河本大作在[日本投降后投靠](../Page/日本投降.md "wikilink")[阎锡山](../Page/阎锡山.md "wikilink")。1949年，[解放军攻取太原之后](../Page/中国人民解放军.md "wikilink")，河本大作作为日本战犯被捕。1952年，经审讯之后，他详细交代了策划炸死张作霖的全过程。1955年，河本大作死于[太原日籍战犯管理所](../Page/太原.md "wikilink")。

## 其他說法

2010年后有部分[日本右翼指出](../Page/日本右翼.md "wikilink")，[俄罗斯作家](../Page/俄罗斯.md "wikilink")称张作霖系[苏联情报人员暗杀](../Page/苏联.md "wikilink")，然后嫁祸给日本关东军\[7\]。該說法並未被日本主流歷史學界採信，国际史学界对此无反应。普罗霍罗夫声明他的观点不是以未公开过的秘密档案为根据的，只是通过“综合分析”，“几乎可以断定”“张作霖被炸是苏联情报机构所为”。\[8\]普罗霍罗夫全名为德米特里·彼特罗维奇·普罗霍罗夫，是俄罗斯的一个历史小说作者\[9\]。很多俄罗斯专家都表示没有听说过普罗霍罗夫。

日本歷史學家[宮脇淳子根據](../Page/宮脇淳子.md "wikilink")[加藤康男所著](../Page/加藤康男.md "wikilink")《解謎「炸死張作霖事件」》，認為「河本大作暗殺張作霖」絕對是謊言，河本大作在爆炸之後特地拍攝照片，橋上滿鐵路軌橋樑完全掉落，橋下方是京奉鐵路，地面卻毫無傷，只有火車正上方被炸飛；張作霖替身搭乘之其他火車仍在張作霖所搭乘之火車之前後方行駛，因此炸死張作霖事件顯然是由張陣營內部人士策劃，炸藥是裝在張作霖所搭乘車輛之天花板或其他地方，這樣策劃者能在車輛抵達下方時按下開關，從而製造出爆炸\[10\]。因此張學良在父親張作霖過世後隔年正式成為中國國民黨黨員，卻私下殺害同時加入中國國民黨之[楊宇霆和](../Page/楊宇霆.md "wikilink")[常蔭槐](../Page/常蔭槐.md "wikilink")；張學良受過極佳近代教育，認為「中國應該統一」，宣稱這兩人是間諜，實際上兩人可能是炸死張作霖之執行犯\[11\]。宮脇淳子確信張學良是[共產國際內部人員](../Page/共產國際.md "wikilink")，他認為證據是後來共產國際頒授勳章給張學良\[12\]。另有關張學良與共產黨的關係，可參見楊奎松《張學良的「通共」與蔣介石的置若罔聞》、陳益南《「西安事變」中若干鮮為人知的細節》等文，張學良在1936年提出加入共產黨，不是加入中共，此事需中共向共產國際匯報，最後以「特殊黨員」之身份加入，即可得到共產國際支持\[13\]。

## 现状

[Huanggutun_Incident_Museum.jpg](https://zh.wikipedia.org/wiki/File:Huanggutun_Incident_Museum.jpg "fig:Huanggutun_Incident_Museum.jpg")
當年的事發現場，現爲沈阳“三洞橋”，鐵路交叉處曾立有“张作霖被炸处”的標志牌，後被卧式黑色大理石碑取代，碑上鐫刻“皇姑屯事件发生地”
\[14\]。2004年，张作霖被炸处及石碑列入沈阳市第一批不可移动文物名录。2015年，当地政府在[皇姑屯站以西](../Page/皇姑屯站.md "wikilink")，原[满铁奉天妇婴医院旧址内设立了皇姑屯事件博物馆](../Page/满铁.md "wikilink")\[15\]。

## 参见

  - [东北易帜](../Page/东北易帜.md "wikilink")
  - [九一八事变](../Page/九一八事变.md "wikilink")
  - [皇姑屯站](../Page/皇姑屯站.md "wikilink")

### 日方關係人

  - [关东军](../Page/关东军.md "wikilink")
  - [田中义一](../Page/田中义一.md "wikilink")
  - [河本大作](../Page/河本大作.md "wikilink")
  - [芳澤謙吉](../Page/芳澤謙吉.md "wikilink")

## 注释

## 外部链接

  - [紀念抗日戰爭勝利六十週年：皇姑屯事件](http://mil.news.sina.com.cn/nz/huanggutun/index.shtml)
    新浪網

  - [日軍拍攝皇姑屯事變爆炸瞬間：張作霖坐車被炸飛](http://news.ifeng.com/history/gaoqing/detail_2013_11/15/31278479_0.shtml)
    鳳凰網歷史頻道

  -
[Category:20世纪暗杀事件](../Category/20世纪暗杀事件.md "wikilink")
[Category:1928年中国政治事件](../Category/1928年中国政治事件.md "wikilink")
[Category:1928年日本政治事件](../Category/1928年日本政治事件.md "wikilink")
[Category:中华民国大陆时期命案](../Category/中华民国大陆时期命案.md "wikilink")
[Category:在中华民国犯下的战争罪行](../Category/在中华民国犯下的战争罪行.md "wikilink")
[Category:中国爆炸案](../Category/中国爆炸案.md "wikilink")
[Category:關東軍](../Category/關東軍.md "wikilink")
[Category:沈阳政治事件](../Category/沈阳政治事件.md "wikilink")
[Category:沈阳命案](../Category/沈阳命案.md "wikilink")
[Category:1928年6月](../Category/1928年6月.md "wikilink")
[Category:1928年爆炸案](../Category/1928年爆炸案.md "wikilink")
[Category:中华民国大陆时期事件与事变](../Category/中华民国大陆时期事件与事变.md "wikilink")
[Category:鐵路車站襲擊事件](../Category/鐵路車站襲擊事件.md "wikilink")

1.  ;《重庆日报》：[张作霖被炸死
    张学良为何13天后才迟迟返回奉天？](http://history.people.com.cn/GB/198305/198865/13373065.html)

2.  [徐彻/辽海出版社：究竟是谁炸死了张作霖](http://news.xinhuanet.com/theory/2007-07/02/content_6316182_1.htm)

3.  中国第二历史档案馆，[河本大作与日军山西" 残留":
    日本帝国主义侵华档案资料选编](https://books.google.com/books?id=v9l9AAAAIAAJ)，中华书局，1995年
    ISBN 9787101012620

4.  张劲松，从河本大作的密信，剖析“皇姑屯事件” 之阴谋，辽宁大学学报(哲学社会科学版)，1998年

5.  河本大作等著，陳鵬仁譯《我杀死了张作霖》，吉林文史出版社，1986年

6.  高群书著，《东京审判》，中央广播电视出版社

7.  普罗霍罗夫：《张作霖元帅之死档案》

8.  [《[环球时报](../Page/环球时报.md "wikilink")》，2006年4月4日，第16版](http://culture.people.com.cn/GB/40479/40480/4276336.html)

9.  [环球时报：《日右翼收买卑劣文人称关东军受苏指使炸死张作霖》](http://news.163.com/06/0406/16/2E1R143F00011248.html)

10.

11.
12.
13.
14. [皇姑屯：战争导火索在这里点燃](http://news.sina.com.cn/o/2005-07-16/08016448682s.shtml)
    [大連日報](../Page/大連日報.md "wikilink")

15.