[1923_Ford_Model_T_Pickup_MGK176.jpg](https://zh.wikipedia.org/wiki/File:1923_Ford_Model_T_Pickup_MGK176.jpg "fig:1923_Ford_Model_T_Pickup_MGK176.jpg")皮卡，為皮卡始祖\]\]
['16_Ford_F-150_Crew_Cab_(Carrrfour_Angrignon).jpg](https://zh.wikipedia.org/wiki/File:'16_Ford_F-150_Crew_Cab_\(Carrrfour_Angrignon\).jpg "fig:'16_Ford_F-150_Crew_Cab_(Carrrfour_Angrignon).jpg")是[美系皮卡代表](../Page/美國.md "wikilink")。\]\]
[2002_Suzuki_Carry.jpg](https://zh.wikipedia.org/wiki/File:2002_Suzuki_Carry.jpg "fig:2002_Suzuki_Carry.jpg")是[日系皮卡代表](../Page/日本.md "wikilink")。\]\]
[2011_Holden_Ute_(VE_II_MY11)_SV6_utility_(2011-04-22)_01.jpg](https://zh.wikipedia.org/wiki/File:2011_Holden_Ute_\(VE_II_MY11\)_SV6_utility_\(2011-04-22\)_01.jpg "fig:2011_Holden_Ute_(VE_II_MY11)_SV6_utility_(2011-04-22)_01.jpg")Ute是[澳系皮卡代表](../Page/澳洲.md "wikilink")。\]\]
[2010-2011_Nissan_Navara_(D22_MY2010)_DX_2-door_cab_chassis_(2011-04-22).jpg](https://zh.wikipedia.org/wiki/File:2010-2011_Nissan_Navara_\(D22_MY2010\)_DX_2-door_cab_chassis_\(2011-04-22\).jpg "fig:2010-2011_Nissan_Navara_(D22_MY2010)_DX_2-door_cab_chassis_(2011-04-22).jpg")Navara是銷量最高的皮卡\]\]
**皮卡**（），业界称为**轻便客货两用车**或是**貨卡**，通常指带开放式载货区的轻型[卡车](../Page/卡车.md "wikilink")，一般车身外形可以明显的分成发动机舱，驾驶室和货舱三段，也有部份車款引擎是在駕駛室下方而沒有引擎室。通常的皮卡载重量在一[噸左右](../Page/噸.md "wikilink")，大型皮卡，如[福特公司的](../Page/福特公司.md "wikilink")[F-350可达到接近](../Page/F-350.md "wikilink")3吨。
皮卡车体轻便，既可以载货也可以载人，部份車款設有後座，總共可載2至6人。

在[美国](../Page/美国.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[澳大利亚等地廣人稀的](../Page/澳大利亚.md "wikilink")[國家](../Page/國家.md "wikilink")，自家用大件物品運載需求大，皮卡是汽车市场的主力车型，在[泰国](../Page/泰国.md "wikilink")、[中东和](../Page/中东.md "wikilink")[非洲等地路段鋪面以泥土路為主](../Page/非洲.md "wikilink")，這時負責載貨的車輛，便是以越野性能優越著稱的皮卡，也得到了非常广泛的应用，甚至在戰場上，車斗還改裝上了機關槍。也是美國風情的組成之一，例如，[福特汽車的](../Page/福特汽車.md "wikilink")[F-150就保持了多年美国汽车市场单款车](../Page/福特F系列.md "wikilink")（包括轿车和卡车）的销量冠军地位。按照出产地，皮卡可以分为美系和日系两大类。美系皮卡普遍动力强劲、高大威猛，但油耗较大；日系皮卡车身较小，油耗较低，比较经济适用。中国大陆市场上销售的目前主要是日系或仿日系皮卡。不管美系还是日系，[底盘都高于一般](../Page/底盘.md "wikilink")[轿车](../Page/轿车.md "wikilink")，和[越野车差不多](../Page/越野车.md "wikilink")，都有两驱和[四驱可选](../Page/四驱.md "wikilink")。车身结构比越野车简单可靠。少数越野发烧友开始购买皮卡，改装之后用于越野游玩。从燃油选择上可以分为柴油版和汽油版。柴油版优点是扭力較大、载重爬坡能力强，购买便宜，油錢和油耗也低；缺点是噪音大，速度慢。汽油版的优点是速度快，噪音低；缺点是车价较贵，油耗大，扭力較小。

## 知名車款

  - [豐田Hilux](../Page/豐田Hilux.md "wikilink")

  -
  - [福特F系列](../Page/福特F系列.md "wikilink")

  - [五十铃D-Max](../Page/五十铃D-Max.md "wikilink")

## 參見

  - [武裝改裝車（Technical）](../Page/武裝改裝車.md "wikilink")

[P](../Category/車體風格.md "wikilink") [P](../Category/汽車種類.md "wikilink")