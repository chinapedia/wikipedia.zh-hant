**栖霞区**位于[中國](../Page/中國.md "wikilink")[江蘇省](../Page/江蘇省.md "wikilink")[南京东北部](../Page/南京.md "wikilink")。栖霞区北临[长江](../Page/长江.md "wikilink")，东界[镇江](../Page/镇江.md "wikilink")[句容市](../Page/句容市.md "wikilink")，西连[玄武区与](../Page/玄武区.md "wikilink")[鼓楼区](../Page/鼓楼区.md "wikilink")，南接[江宁区](../Page/江宁区.md "wikilink")。栖霞区源于[中华民国时期的第九区](../Page/中华民国.md "wikilink")（燕子矶区）和第十区（孝陵区）。

栖霞区是中国重要的科教中心和航运中心，[华东地区现代工业](../Page/华东.md "wikilink")、科技、人才集中区，华东地区重要的先进制造业基地、世界级光电显示产业基地、国家级长江航运物流枢纽，是以医药电子、机械制造、港口运输、建材工业、风景名胜、生态农业为主要职能的现代化江滨区。

## 行政劃分

栖霞区辖9个街道办事处：

  - [尧化街道](../Page/尧化街道.md "wikilink")
  - [迈皋桥街道](../Page/迈皋桥街道.md "wikilink")
  - [燕子矶街道](../Page/燕子矶街道.md "wikilink")
  - [马群街道](../Page/马群街道.md "wikilink")
  - [栖霞街道](../Page/栖霞街道.md "wikilink")
  - [龙潭街道](../Page/龙潭街道_\(南京市\).md "wikilink")
  - [八卦洲街道](../Page/八卦洲街道.md "wikilink")
  - [仙林街道](../Page/仙林街道.md "wikilink")
  - [西岗街道](../Page/西岗街道.md "wikilink")

## 地理環境

栖霞区地质构造属宁镇褶皱带。地势起伏大，地貌类型多。地形大势为南高北低。境内无海拔300米以上低山。
[缩略图](https://zh.wikipedia.org/wiki/File:Nanjing_-_new_building_near_Jinmalu_Metro_station_-_P1070007.JPG "fig:缩略图"))\]\]
栖霞区属北亚热带湿润气候带和季风环流的海洋性气候区，季风显著，冬冷夏热，四季分明，日照充足，水资源充沛。

## 教育

栖霞区是南京市重要的科技和人才集中区。

[仙林大学城位於栖霞区西部](../Page/仙林大学城.md "wikilink")，截至2017年11月，已有[南京大学](../Page/南京大学.md "wikilink")、[南京師範大學](../Page/南京師範大學.md "wikilink")、[南京财经大学](../Page/南京财经大学.md "wikilink")、[南京邮电大学](../Page/南京邮电大学.md "wikilink")、[南京中医药大学](../Page/南京中医药大学.md "wikilink")、[南京森林警察学院](../Page/南京森林警察学院.md "wikilink")、[南京理工大学紫金学院](../Page/南京理工大学紫金学院.md "wikilink")、[南京审计学院金审学院](../Page/南京审计大学金审学院.md "wikilink")、[南京工业职业技术学院](../Page/南京工业职业技术学院.md "wikilink")、[南京信息职业技术学院](../Page/南京信息职业技术学院.md "wikilink")、[应天职业技术学院](../Page/应天职业技术学院.md "wikilink")、[南京技师学院等院校及其校区入驻](../Page/南京技师学院.md "wikilink")。

## 景点

  - [栖霞山](../Page/栖霞山.md "wikilink")
  - [栖霞寺](../Page/栖霞寺.md "wikilink")
  - [舍利塔](../Page/舍利塔.md "wikilink")
  - [燕子矶](../Page/燕子矶.md "wikilink")
  - [六朝石刻](../Page/六朝石刻.md "wikilink")

## 参考文献

## 外部链接

  - [南京市栖霞区政府网站](http://www.njqxq.gov.cn)

{{-}}

[栖霞区](../Category/栖霞区.md "wikilink") [区](../Category/南京区县.md "wikilink")
[南京](../Category/江苏市辖区.md "wikilink")