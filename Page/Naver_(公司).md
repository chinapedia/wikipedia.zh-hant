**Naver株式會社**（；）是[韓國目前最大的](../Page/韓國.md "wikilink")[互聯網服務公司](../Page/互聯網.md "wikilink")。專營搜尋引擎業務，旗下擁有經營通訊軟體的[LINE公司及新創企業孵化器的Camp](../Page/LINE_\(公司\).md "wikilink")
Mobile公司。

## 沿革

以**NHN株式會社**開始營運，其中的「NHN」有「**N**ext **H**uman
**N**etwork」的意思。在NHN時代，即為韓國國內最大型的[搜索引擎](../Page/搜索引擎.md "wikilink")[NAVER與世界最大型的](../Page/NAVER.md "wikilink")[網絡遊戲網站](../Page/網絡遊戲.md "wikilink")[Hangame](../Page/Hangame.md "wikilink")，這兩項為NHN公司的兩大支柱業務。

在韓國新興的[股票市場的](../Page/股票市場.md "wikilink")[KOSDAQ中](../Page/KOSDAQ.md "wikilink")，NHN的股票市值總額曾名列第一位（在2005年1月時約為1兆5千億韓圜；股票編號：35420）。2006年1月的[商業週刊說](../Page/商業週刊.md "wikilink")，NHN是導致Google在韓國市場的競爭中屢屢受挫的最主要的企業。

2008年6月，美國商業週刊公布2007年全球資訊科技業一百強排名，NHN名列全球第27名。\[1\]

根據[comScore的報告](../Page/comScore.md "wikilink")，2008年7月，NHN在全球的搜尋引擎排行第5。名次由高到低為，[Google](../Page/Google.md "wikilink")、[雅虎](../Page/雅虎.md "wikilink")、[百度](../Page/百度.md "wikilink")、[微軟及NHN](../Page/微軟.md "wikilink")
Naver。\[2\]

2013年8月1日，NHN宣布將遊戲事業從公司中剝離，專營NAVER搜尋引擎，並更名為「Naver株式會社」；而遊戲部門則獨立為「[NHN娛樂株式會社](../Page/NHN娛樂株式會社.md "wikilink")」。\[3\]Naver公司與NHN公司於2017年後，已無相互投資關係，僅Naver李海珍持有NHN股票，公司法人本身透過出售持股的方式結束投資關係。

2017年，Naver投資1000億韓元（約為美金8852萬元）入股[YG娛樂](../Page/YG娛樂.md "wikilink")，成為第二大股東。\[4\]

## 海外事業

  - [日本](../Page/日本.md "wikilink")
      - 2000年，NHN透過NHN Japan開始Naver Japan與Hangame
        Japan等服務，隨後推出[LINE通訊軟體](../Page/LINE.md "wikilink")，並將Hangame
        Japan更名為[LINE株式會社](../Page/LINE_\(公司\).md "wikilink")。
  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")
      - 2004年6月，其中国公司NHN
        China不仅经营[联众世界在线游戏平台](../Page/联众世界.md "wikilink")，之後还通过其子公司“北京东方慧灵公司”运营[n词酷](../Page/n词酷.md "wikilink")[在线词典服务](../Page/在线词典.md "wikilink")。
  - [美國](../Page/美國.md "wikilink")
      - 2005年5月，於美國成立[ijji遊戲入口網站](../Page/ijji.md "wikilink")；同年7月，NHN
        USA正式成立。
  - [中華民國](../Page/中華民國.md "wikilink")
      - 2008年4月16日，設立台灣分公司「世聯互動網-{}-路有限公司」（NHN
        Taiwan），同年11月11日成立線上字典n詞酷。
      - 2010年9月2日，世聯互動網-{}-路有限公司清算完結。
      - 2013年12月，收購[Whoscall開發公司](../Page/Whoscall.md "wikilink")。

## 相關服務及品牌

  - [NAVER](../Page/NAVER.md "wikilink")
  - [NHN PlayArt](../Page/NHN_PlayArt.md "wikilink")
  - [LINE](../Page/LINE.md "wikilink")
  - [LINE (公司)](../Page/LINE_\(公司\).md "wikilink")
  - [Hangame](../Page/Hangame.md "wikilink")
  - [联众世界](../Page/联众世界.md "wikilink")
  - [n词酷](../Page/n词酷.md "wikilink")
  - [ENTOI](../Page/ENTOI.md "wikilink")
  - [LINE webtoon](../Page/LINE_webtoon.md "wikilink")
  - [Whoscall](../Page/Whoscall.md "wikilink")
  - [WEBZEN](../Page/WEBZEN.md "wikilink")
  - [YG娛樂](../Page/YG娛樂.md "wikilink")

## 收購WEBZEN事件

[WEBZEN在韓國本部長達四年的經營虧損及事業部門](../Page/WEBZEN.md "wikilink")、開發部門的經營、開發策略錯誤，各項新產品開發陷入關鍵技術無法順利進展，使得新產品開發進入長期的開發延遲、並消耗大量預算狀態，推出的新作品[Soul
of the Ultimate
Nation也未獲得韓國](../Page/Soul_of_the_Ultimate_Nation.md "wikilink")、台灣等地區消費者好評，使其新作品無法獲利。各地子公司長期性的營運虧損，無法貢獻獲利又要求總部支援的情形下造成WEBZEN及其他分公司只能依靠其舊作品[MU做為其營收來源並勉強營運](../Page/奇蹟_\(遊戲\).md "wikilink")。其公司經營狀態又在爆發一連串惡意併購、長期虧損、重要開發成員離職、核心領導脫離團隊另謀發展、過度裁員造成經營效率下降、海外子公司持續的營運惡化等影響下，於2008年6月11日確定由韓國第一大網路營運公司NHN轄下的子公司NHN
Games收購[NEOWAV](../Page/NEOWAV.md "wikilink")、[Liveplex所持有的股份共](../Page/Liveplex.md "wikilink")10.52%的股份，成為其持有股份最大的股東，並取得WEBZEN的經營權，並於同年9月4日取得前代表理事[金南州持有股份共計](../Page/金南州.md "wikilink")23.74%，宣告正式取得WEBZEN公司營運權，但是尚不考慮兩公司合併的可能性。NHN擬定將NHN
Games及WEBZEN成為相互合作拓展線上遊戲市場的兩大子公司。\[5\]

## 外部連結

  -
## 參考文獻

[Category:韓國互聯網公司](../Category/韓國互聯網公司.md "wikilink")
[Category:KOSDAQ上市公司](../Category/KOSDAQ上市公司.md "wikilink")
[Category:Naver](../Category/Naver.md "wikilink")
[Category:韓國品牌](../Category/韓國品牌.md "wikilink")

1.
2.
3.
4.
5.