**谷正伦**（），字**纪常**，[贵州](../Page/贵州.md "wikilink")[安顺人](../Page/安顺.md "wikilink")。[中華民国国军軍事将领](../Page/中華民国国军.md "wikilink")，官拜[憲兵](../Page/中華民國憲兵.md "wikilink")[中將](../Page/中將.md "wikilink")。被譽為「**現代中國憲兵之父**」，並揭示：「**不說謊，不作假，守本分，盡職責。**」為中華民國憲兵官兵座右銘。

兩位胞弟[谷正綱](../Page/谷正綱.md "wikilink")、[谷正鼎](../Page/谷正鼎.md "wikilink")，皆[中華民國與](../Page/中華民國.md "wikilink")[國民黨的黨員](../Page/國民黨.md "wikilink")。

目前安葬於五指山國軍公墓，與夫人同葬。

## 1889年——1925年

1906年，毕业于贵州[陆军小学](../Page/陆军小学.md "wikilink")，保送[湖北](../Page/湖北.md "wikilink")[陆军第三中学堂学习](../Page/陆军第三中学堂.md "wikilink")。1909年再保送[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[振武学校深造](../Page/振武学校.md "wikilink")，并于其间参加[中国同盟会](../Page/中国同盟会.md "wikilink")。1911年，回国随[黄兴参加](../Page/黄兴.md "wikilink")[武昌起义](../Page/武昌起义.md "wikilink")，12月任[汉阳总指挥部](../Page/汉阳.md "wikilink")[少校副官](../Page/少校.md "wikilink")。

1913年（民國二年），任[南京临时政府总统府陆军部少校科员](../Page/南京临时政府.md "wikilink")，不久到[日本士官学校学习](../Page/日本士官学校.md "wikilink")[炮兵](../Page/炮兵.md "wikilink")。1916年，学成归国后任黔军炮兵团上校团长，不久转任步兵团长。随后的[护法战争中率部入川](../Page/护法战争.md "wikilink")，攻克[成都](../Page/成都.md "wikilink")、[遂宁](../Page/遂宁.md "wikilink")。1920年，升任黔军第二混成旅旅长。

1921年率部撤反贵州，奉孙中山令兼任贵州南路卫戍[中将司令](../Page/中将.md "wikilink")。在平定桂军[陆荣廷叛乱中](../Page/陆荣廷.md "wikilink")，6月兼任中央直辖黔军第四路军中将司令，率部攻克[柳州](../Page/柳州.md "wikilink")，进占[桂林](../Page/桂林.md "wikilink")，12月升任中央直辖黔军[上将总司令](../Page/上将.md "wikilink")。后因部属叛变回[贵阳](../Page/贵阳.md "wikilink")，化装经[重庆抵](../Page/重庆.md "wikilink")[上海闭门读书](../Page/上海.md "wikilink")。1923年7月，任湘军第一师顾问，10月兼军官讲习所[少将教育长](../Page/少将.md "wikilink")。

## 1926年——1936年

1926年（民國十五年），任[国民革命军独立第二师副师长兼第一旅旅长](../Page/国民革命军.md "wikilink")，率部[北伐克](../Page/北伐.md "wikilink")[九江](../Page/九江.md "wikilink")。1927年4月，升任[国民革命军第四十军第一师师长](../Page/国民革命军第四十军.md "wikilink")。1928年，兼任[南京上将戒严司令](../Page/南京.md "wikilink")、首都中将卫戍副司令。1932年，任首都上将卫戍司令、[宪兵中将司令](../Page/宪兵.md "wikilink")、代理南京市市长。

1933年5月，於[南京](../Page/南京.md "wikilink")[江寧成立憲兵訓練所](../Page/江寧.md "wikilink")。1934年6月，平息南京领事馆副领事藏本失踪事件。1935年，在[中国国民党第五次全国代表大会上当选为中央委员](../Page/中国国民党.md "wikilink")、中央执行委员。1936年3月，兼任[憲兵學校中將教育長](../Page/憲兵學校.md "wikilink")。1937年，兼任军事委员会军法执行总部副监。

## 1937年——1945年

  - 1937年（民国二十六年）抗战爆发后，率部参加[南京保卫战](../Page/南京保卫战.md "wikilink")。南京失陷后，率宪兵司令部至湖南[长沙](../Page/长沙.md "wikilink")。
  - 1939年2月，任鄂湘川黔[绥靖主任兼第六战区副司令长官](../Page/绥靖.md "wikilink")。任中清查户口，建全保甲，巩固抗战后方。
  - 1940年，调任[甘肃省主席兼保安司令](../Page/甘肃.md "wikilink")。
  - 1941年，兼西北训练团副团长，9月任[行政院水利委员](../Page/行政院.md "wikilink")。
  - 1942年，兼甘新公路督办。任职期间重视建设、[民生](../Page/民生.md "wikilink")，发展农林牧，修筑天兰铁路；整顿行政，改善粮政，除弊便民。

## 1947年——去世

1947年（民國三十六年）4月23日，[国民政府准免糧食部部長谷正倫本職](../Page/国民政府.md "wikilink")；特任谷正倫為[行政院政务委员](../Page/行政院.md "wikilink")，兼[粮食部部长](../Page/中華民國糧食部.md "wikilink")。\[1\]5月23日，糧食部長谷正倫向[國民參政會報告](../Page/國民參政會.md "wikilink")，謂管理糧食辦法，為拋售糧食，抑平漲風，平價配售等。\[2\]7月18日，國民政府舉行國務會議，通過糧食部部長谷正倫辭職，任命[俞飛鵬為糧食部長](../Page/俞飛鵬.md "wikilink")。\[3\]

1948年5月，回任[贵州省政府主席兼保安司令](../Page/贵州省政府.md "wikilink")，后有任贵州[绥靖公署](../Page/绥靖公署.md "wikilink")[二级上将主任](../Page/二级上将.md "wikilink")。任职期间召训财经干部，扩充[保安团](../Page/保安团.md "wikilink")，训练反共自卫干部。
1949年11月，[中国人民解放军进逼](../Page/中国人民解放军.md "wikilink")[贵阳](../Page/贵阳.md "wikilink")，只身经[昆明飞](../Page/昆明.md "wikilink")[香港](../Page/香港.md "wikilink")，又转飞[台灣](../Page/台灣.md "wikilink")。

1950年，任中華民國总统府国策顾问。1953年11月，因病在[台北逝世](../Page/台北.md "wikilink")。

## 參考文獻

## 相關連結

  - [中華民國憲兵](../Page/中華民國憲兵.md "wikilink")
  - [憲兵學校](../Page/憲兵學校.md "wikilink")
  - [谷正纲](../Page/谷正纲.md "wikilink")
  - [谷正鼎](../Page/谷正鼎.md "wikilink")
  - [皮以書](../Page/皮以書.md "wikilink")

[分類:安葬於國軍示範公墓者](../Page/分類:安葬於國軍示範公墓者.md "wikilink")

[G谷](../Category/安顺人.md "wikilink")
[G谷](../Category/中華民國陸軍二級上將.md "wikilink")
[G谷](../Category/中華民國大陸時期軍事人物.md "wikilink")
[G谷](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:中華民國憲兵司令](../Category/中華民國憲兵司令.md "wikilink")
[Category:台灣戰後貴州移民](../Category/台灣戰後貴州移民.md "wikilink")
[G谷](../Category/1889年出生.md "wikilink")
[G谷](../Category/1953年逝世.md "wikilink")
[Category:中華民國反共主義者](../Category/中華民國反共主義者.md "wikilink")

1.

2.
3.