[Setouchi_Ushimado_Port07n3200.jpg](https://zh.wikipedia.org/wiki/File:Setouchi_Ushimado_Port07n3200.jpg "fig:Setouchi_Ushimado_Port07n3200.jpg")
**瀨戶内市**（）是位於[岡山縣東南部](../Page/岡山縣.md "wikilink")[岡山平原的](../Page/岡山平原.md "wikilink")[城市](../Page/城市.md "wikilink")，鄰近[岡山市](../Page/岡山市.md "wikilink")。轄區內多屬海拔100公尺到300公尺之間的山林地，東南部面向[瀨戶內海](../Page/瀨戶內海.md "wikilink")。

產業以[農業和](../Page/農業.md "wikilink")[漁業為主](../Page/漁業.md "wikilink")，農產包括野菜、水果，漁業則以[牡蠣養殖為主](../Page/牡蠣.md "wikilink")。轄內的長船地區、福岡地區自古為生產刀劍的著名地點。

## 歷史

### 年表

  - 1889年6月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：邑久郡：牛窗村、鹿忍村、長濱村、邑久村、福田村、今城村、本庄村、笠加村、玉津村、裳掛村、豐原村、美和村、國府村、行幸村、大宮村。
  - 1896年2月26日：牛窗村改制為[牛窗町](../Page/牛窗町.md "wikilink")。
  - 1924年4月1日：鹿忍村改制為[鹿忍町](../Page/鹿忍町.md "wikilink")。
  - 1952年4月1日：邑久村、福田村、今城村、豐原村、本-{}-庄村、笠加村[合併為](../Page/市町村合併.md "wikilink")[邑久町](../Page/邑久町.md "wikilink")。
  - 1954年1月1日：玉津村被併入邑久町。
  - 1954年10月1日：牛窗町、鹿忍町、長濱村合併為新設置的牛窗町。
  - 1955年3月31日：
      - 大宮村的部分地區被併入牛窗町。
      - 美和村、國府村、行幸村合併為[長船町](../Page/長船町.md "wikilink")。
  - 1958年4月1日：裳掛村被併入邑久町。
  - 2004年11月1日：邑久町、牛窗町、長船町合併為**瀨戶內市**。\[1\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年6月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>邑久村</p></td>
<td><p>1952年4月1日<br />
邑久町</p></td>
<td><p>邑久町</p></td>
<td><p>邑久町</p></td>
<td><p>2004年11月1日<br />
瀨戶內市</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>福田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>今城村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>本-{}-庄村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>笠加村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>豐原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>玉津村</p></td>
<td><p>1954年1月1日<br />
併入邑久町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>裳掛村</p></td>
<td><p>1958年4月1日<br />
併入邑久町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>美和村</p></td>
<td><p>1955年3月31日<br />
長船町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>國府村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>行幸村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>牛窗村</p></td>
<td><p>1896年2月26日<br />
牛窗町</p></td>
<td><p>1954年10月1日<br />
牛窗町</p></td>
<td><p>牛窗町</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鹿忍村</p></td>
<td><p>1924年4月1日<br />
鹿忍町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>長濱村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大宮村</p></td>
<td><p>大宮村</p></td>
<td><p>1955年3月31日<br />
牛窗町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1956年2月20日<br />
併入西大寺市</p></td>
<td><p>1969年2月18日<br />
併入岡山市</p></td>
<td><p>2009年4月1日<br />
岡山市東區</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1954年10月1日<br />
併入西大寺市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

  - 歴代市長

<!-- end list -->

1.  [立岡脩二](../Page/立岡脩二.md "wikilink")：2004年12月5日 - 2008年12月4日
2.  島村俊一：2008年12月5日 - 2009年6月9日
3.  武久顯也：2009年7月19日 - 現任

## 交通

[JRW-OkuStation.jpg](https://zh.wikipedia.org/wiki/File:JRW-OkuStation.jpg "fig:JRW-OkuStation.jpg")

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - **[山陽新幹線](../Page/山陽新幹線.md "wikilink")**：（←[備前市](../Page/備前市.md "wikilink")）
        - （通過轄內但未設有車站） - （[岡山市](../Page/岡山市.md "wikilink")→）

      - **[赤穗線](../Page/赤穗線.md "wikilink")**：（←備前市） -
        [長船車站](../Page/長船車站.md "wikilink") -
        [邑久車站](../Page/邑久車站.md "wikilink") -
        [大富車站](../Page/大富車站_\(日本\).md "wikilink") -
        （岡山市）

### 海上航線

[Maejima_and_Ferry.jpg](https://zh.wikipedia.org/wiki/File:Maejima_and_Ferry.jpg "fig:Maejima_and_Ferry.jpg")

  - 前島渡輪（[牛窗港](../Page/牛窗港.md "wikilink")～前島）

## 觀光資源

[Yumeji_Art_Museum01s3200.jpg](https://zh.wikipedia.org/wiki/File:Yumeji_Art_Museum01s3200.jpg "fig:Yumeji_Art_Museum01s3200.jpg")

### 景點

  - [夢二鄉土美術館分館](../Page/夢二鄉土美術館.md "wikilink")
  - [備前長船刀劍博物館](../Page/備前長船刀劍博物館.md "wikilink")

### 祭典、活動

  - 喜之助慶典（毎年8月下旬）
  - 牛窗秋祭（毎年10月第4日曜日-[岡山三大だんじり祭りの一つ](../Page/岡山三大だんじり祭り.md "wikilink")）
  - 備前長船名刀祭

## 教育

  - 高等學校

<!-- end list -->

  - [岡山縣立邑久高等學校](../Page/岡山縣立邑久高等學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [幌加內町](../Page/幌加內町.md "wikilink")（[北海道](../Page/北海道.md "wikilink")[雨龍郡](../Page/雨龍郡.md "wikilink")）：於1989年7月締結為姊妹町
  - [對馬市](../Page/對馬市.md "wikilink")（[長崎縣](../Page/長崎縣.md "wikilink")）：於1996年11月締結為姐妹町

### 海外

  - [米蒂利尼](../Page/米蒂利尼.md "wikilink")（[希臘](../Page/希臘.md "wikilink")[北愛琴](../Page/北愛琴.md "wikilink")[萊斯沃斯州](../Page/萊斯沃斯州.md "wikilink")）：於1982年7月6日締結為國際姊妹都市

## 本地出身之名人

  - [宇喜多直家](../Page/宇喜多直家.md "wikilink")：[戰國時代](../Page/戰國時代_\(日本\).md "wikilink")[大名](../Page/大名.md "wikilink")
  - [柴田義董](../Page/柴田義董.md "wikilink")：[江戶時代](../Page/江戶時代.md "wikilink")[繪師](../Page/繪師.md "wikilink")
  - [竹久夢二](../Page/竹久夢二.md "wikilink")：[畫家](../Page/畫家.md "wikilink")、[詩人](../Page/詩人.md "wikilink")
  - [竹田喜之助](../Page/竹田喜之助.md "wikilink")：[傀儡戲操作人](../Page/傀儡戲.md "wikilink")
  - [綠川洋一](../Page/綠川洋一.md "wikilink")：[攝影師](../Page/攝影師.md "wikilink")
  - [立岡脩二](../Page/立岡脩二.md "wikilink")：前市長、電影[製作人](../Page/製作人.md "wikilink")
  - [バクシーシ山下](../Page/バクシーシ山下.md "wikilink")：[AV監督](../Page/AV監督.md "wikilink")
  - [橋本五郎](../Page/橋本五郎_\(小説家\).md "wikilink")：[偵探小說](../Page/偵探小說.md "wikilink")[作家](../Page/作家.md "wikilink")
  - [DJ KAWASAKI](../Page/DJ_KAWASAKI.md "wikilink")：DJ
  - [太田行紀](../Page/太田行紀.md "wikilink")：[田徑選手](../Page/田徑.md "wikilink")
  - [安木祥二](../Page/安木祥二.md "wikilink")：前[職業棒球選手](../Page/職業棒球.md "wikilink")

## 參考資料

## 外部連結

  - [瀨戶內市觀光協會](http://www.i-setouchi.org/)

  - [瀨戶內市商工會](http://www.setouchi.org/)

<!-- end list -->

1.