**差猜·春哈旺**（；[RTGS](../Page/RTGS.md "wikilink")：Chatchai Chunhawan；
），[泰国第](../Page/泰国.md "wikilink")17任[泰國總理](../Page/泰國總理.md "wikilink")（1988年–1991年），是泰國史上第17位首相。差猜乃泰国华裔，本姓[林](../Page/林姓.md "wikilink")，祖籍为中國[澄海](../Page/澄海.md "wikilink")。\[1\]\[2\]\[3\]

1940年，差猜毕业于泰國皇家军事学院；1940年开始在军队服役。1949年后，历任多国大使以及外交部长、工业部长、副首相等职。1974年参与创建[泰国民族党](../Page/泰国民族党.md "wikilink")，后任该党领袖。1976年3月，参与创立泰中友好协会，并任主席。1988年8月差猜·春哈旺出任首相兼国防部长；1990年3月辞去国防部长一职。1991年2月23日，三军最高统帅[顺通·空颂蓬上将和](../Page/顺通·空颂蓬.md "wikilink")[苏钦达上将发动军事政变](../Page/苏钦达.md "wikilink")，差猜政府被推翻。

## 参考文件

<references/>

## 外部链接

  - [颂提出任临时首相
    承诺尽快还政于民（图）](http://www.sxrb.com/mag4/20060921/ca498799.htm)

{{-}}

[Category:泰国总理](../Category/泰国总理.md "wikilink")
[Category:被政變推翻的領導人](../Category/被政變推翻的領導人.md "wikilink")
[Category:軍人出身的政府首腦](../Category/軍人出身的政府首腦.md "wikilink")
[Category:泰國軍事人物](../Category/泰國軍事人物.md "wikilink")
[Category:泰國潮汕人](../Category/泰國潮汕人.md "wikilink")
[Category:澄海人](../Category/澄海人.md "wikilink")
[Category:林姓](../Category/林姓.md "wikilink")

1.  [创温州土地拍卖史新高　绿城33亿拍得温州地王](http://216.109.125.130/search/cache?p=%E5%AF%9F%E7%8C%9C+%E6%BE%84%E6%B5%B7&ei=UTF-8&fr=yfp-t-501&fp_ip=SG&x=wrt&meta=0&u=www.cnwz.com/read.php%3Ftid%3D601545&w=%22%E5%AF%9F+%E7%8C%9C%22+%E6%BE%84%E6%B5%B7&d=fP5IZPmdOoIA&icp=1&.intl=us)
2.  [曾经叱咤风云的泰国政坛澄海人](http://www.ydtz.com/news/shownews.asp?id=22114)
3.  [潮汕人](http://www.czpp.com/cgi-bin/topic.cgi?forum=7&topic=365&show=25)