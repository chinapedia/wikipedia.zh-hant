**北京动物园**位于[北京市](../Page/北京.md "wikilink")[西城区西直门外大街](../Page/西城区.md "wikilink")，原来名为农事实验场、天然博物院、万牲园、西郊公园，是中国开放最早、动物种类最多的[动物园](../Page/动物园.md "wikilink")，从[清](../Page/清朝.md "wikilink")[光绪三十二年](../Page/光绪.md "wikilink")（1906年）正式建园至今已有逾百年的历史。北京动物园曾经繁殖和饲养了众多珍贵的[野生动物](../Page/野生动物.md "wikilink")，例如[大熊猫](../Page/大熊猫.md "wikilink")、[朱鹮](../Page/朱鹮.md "wikilink")、[金丝猴等](../Page/金丝猴.md "wikilink")。现在共饲养各类野生动物490余种，近5000头只。该园现隶属于[北京市公园管理中心](../Page/北京市公园管理中心.md "wikilink")。

## 历史

[Beijing_Zoo_-_Oct_2009_-_IMG_1240.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_1240.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_1240.jpg")

### 前身

该园前身为“两园两寺”即[乐善园和](../Page/乐善园.md "wikilink")[继园以及](../Page/继园.md "wikilink")[广善寺和](../Page/广善寺.md "wikilink")[惠安寺等处](../Page/惠安寺.md "wikilink")。

  - [乐善园](../Page/乐善园.md "wikilink")：[康熙帝将位于今北京动物园东部以及](../Page/康熙帝.md "wikilink")[北京展览馆西部的土地赐给平定](../Page/北京展览馆.md "wikilink")[耿精忠叛乱有功的](../Page/耿精忠.md "wikilink")[康亲王](../Page/康亲王.md "wikilink")[杰书建康亲王别墅](../Page/杰书.md "wikilink")，名“乐善园”。[乾隆朝](../Page/乾隆帝.md "wikilink")，该园成为皇帝中途小憩之所。[乾隆十二年起](../Page/乾隆.md "wikilink")，乐善园成为皇家行宫，由[内务府](../Page/内务府.md "wikilink")[奉宸苑管辖](../Page/奉宸苑.md "wikilink")。[嘉庆帝继位后因财力渐绌](../Page/嘉庆帝.md "wikilink")，乐善园行宫日圮。[清朝后期](../Page/清朝.md "wikilink")，该处成为租地，征招种植[莲藕](../Page/莲藕.md "wikilink")、[芦苇](../Page/芦苇.md "wikilink")、[蒲草等](../Page/蒲草.md "wikilink")，其收成上缴。

<!-- end list -->

  - [继园](../Page/继园.md "wikilink")：该园故址位于今北京动物园西北部。[康熙朝](../Page/康熙帝.md "wikilink")，有园名“邻善园”，系[康熙帝赐其皇三子](../Page/康熙帝.md "wikilink")[允祉的园林](../Page/允祉.md "wikilink")。[明义得到邻善园后进行了大规模修葺](../Page/明义.md "wikilink")，并将其更名为“环溪别墅”。[道光年间](../Page/道光.md "wikilink")，[宝文庄公有园名](../Page/觉罗宝兴.md "wikilink")“可园”。[光绪初年](../Page/光绪.md "wikilink")，可园更名“继园”，后此园归属[内务府](../Page/内务府.md "wikilink")[奉宸苑](../Page/奉宸苑.md "wikilink")。对于该园的沿革仍在研究之中。

<!-- end list -->

  - [广善寺和](../Page/广善寺_\(北京西城\).md "wikilink")[惠安寺](../Page/惠安寺.md "wikilink")：位于今北京动物园东南角，今[西直门外大街北侧](../Page/西直门外大街.md "wikilink")。[广善寺仍存遗址](../Page/广善寺_\(北京西城\).md "wikilink")，其东侧的[惠安寺已无存](../Page/惠安寺.md "wikilink")。

[Beijing_Zoo_-_Oct_2009_-_IMG_1216.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_1216.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_1216.jpg")

### 清末建园

清朝[光绪三十二年](../Page/光绪.md "wikilink")（1906年）在原[乐善园](../Page/乐善园.md "wikilink")、[继园](../Page/继园.md "wikilink")（又称“三贝子花园”，[福康安郡王府邸](../Page/福康安.md "wikilink")，福康安征戰各地，喜豢養珍禽異獸於邸內著稱）和[广善寺](../Page/广善寺_\(北京西城\).md "wikilink")、[惠安寺旧址上](../Page/惠安寺.md "wikilink")，由清[农工商部领衔建立农事试验场](../Page/农工商部.md "wikilink")。农事试验场占地面积约71[公顷](../Page/公顷.md "wikilink")，对各类农作物分为五大宗进行实验，即“谷麦试验”“蚕桑试验”“蔬菜试验”“果木试验”和“花卉试验”。农事实验场附设的动物园是中国历史上最早的近代公共动物园，1908年首次售票开放。

北京动物园最初的展品是[南洋商务大臣兼](../Page/南洋商务大臣.md "wikilink")[两江总督](../Page/两江总督.md "wikilink")[端方自](../Page/端方.md "wikilink")[德国购回的部分动物及全国各地抚督送献清朝政府的动物](../Page/德国.md "wikilink")，约数十种百余只。农事试验场位于西直门外，交通便捷，又是中国历史上第一个集动物、植物科学普及为一体的，带有公园性质的农事试验场，因此，从开业以来，人气极旺。

1984年乐善园建筑遗存成为[北京市文物保护单位之一](../Page/北京市文物保护单位.md "wikilink")。

### 民国时期的发展

[Beijing_Zoo_-_Oct_2009_-_IMG_1229.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_1229.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_1229.jpg")
辛亥革命后清政府倒台，农事试验场几易其名，从民国初年的“中央农事试验场”，到1929年更名“国立北平天然博物院”，1934年又复名“中央农事试验场”，再到1941年更名“实业总署园艺试验场”直至‘北平市园艺试验场”，日伪时期曾更名“北京特别市农事试验场”并挂“乐善公园”对外开放。

由于连年战乱，民生凋敝，该场大部分动物都因为种种原因而夭折，[抗日战争时期](../Page/抗日战争.md "wikilink")，仅有的一头[亚洲象饿死](../Page/亚洲象.md "wikilink")，园中猛兽如[狮](../Page/狮.md "wikilink")、[虎](../Page/虎.md "wikilink")、[豹等在](../Page/豹.md "wikilink")[抗日战争末期的](../Page/抗日战争.md "wikilink")1943年以防空的理由被日军下令毒杀。1945年抗日战争结束时，园艺试验场被日军占据作为守备仓库，[国民政府接收后又被防空部队作为临时营房长期占用](../Page/国民政府.md "wikilink")，1946年重新开放时仅有[猴十只](../Page/猴.md "wikilink")、[鹅](../Page/鹅.md "wikilink")、[兔子](../Page/兔子.md "wikilink")、[鸽子两只](../Page/鸽子.md "wikilink")、[火鸡](../Page/火鸡.md "wikilink")、[鸵鸟](../Page/鸵鸟.md "wikilink")、[孔雀](../Page/孔雀.md "wikilink")、[鹰](../Page/鹰.md "wikilink")、[葵花鹦鹉](../Page/葵花鹦鹉.md "wikilink")、[白鹦鹉](../Page/白鹦鹉.md "wikilink")、[桃红鹦鹉各一只](../Page/桃红鹦鹉.md "wikilink")。1949年2月，该农林实验所被[北平市人民政府接收时](../Page/北平市人民政府.md "wikilink")，园内只剩下13只[猕猴和一只](../Page/猕猴.md "wikilink")[鸸鹋](../Page/鸸鹋.md "wikilink")。

农事试验场曾经采取种种措施吸引游客，其中尤以“巨人售票”最为引人瞩目，当时的农事试验场管理部门专门招募了两名身高在220厘米以上的巨人担任检票工作，其中一名為[張英武](../Page/張英武.md "wikilink")。

### 中华人民共和国初期的西郊公园

[Beijing_Zoo_-_satellite_image_(1967-09-20).jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_satellite_image_\(1967-09-20\).jpg "fig:Beijing_Zoo_-_satellite_image_(1967-09-20).jpg")
1949年2月，北京市人民政府接管了当时名为“北平市农林实验所”的北京动物园后，将其更名为“北平市农林实验场”。考虑到农时试验场的条件已经不适应进行农桑实验，经过整修、改造和绿化，于同年9月1日定名为“西郊公园”。

1950年3月1日，西郊公园正式开放。为准备开放西郊公园，西郊公园管理处将园内的围墙、牡丹亭、豳风堂、动物园兽舍等加以修缮。并整修了鸟笼、鹿棚、猴山和甬路，增添一些小型动物，购入了[黄雀](../Page/黄雀.md "wikilink")、[交嘴雀](../Page/交嘴雀.md "wikilink")、[金翅](../Page/金翅.md "wikilink")、[太平鸟](../Page/太平鸟.md "wikilink")、[燕雀](../Page/燕雀.md "wikilink")、[火鸡](../Page/火鸡.md "wikilink")、[鹰](../Page/鹰.md "wikilink")、[灰鹤](../Page/灰鹤.md "wikilink")、[大雁等](../Page/雁属.md "wikilink")[鸟类及](../Page/鸟类.md "wikilink")[鹿](../Page/鹿.md "wikilink")、[狼](../Page/狼.md "wikilink")、[鼠](../Page/鼠.md "wikilink")、[豹等](../Page/豹.md "wikilink")[兽类](../Page/兽类.md "wikilink")。中华人民共和国国家领导人[毛泽东](../Page/毛泽东.md "wikilink")、[周恩来](../Page/周恩来.md "wikilink")、[朱德等还将赠送的](../Page/朱德.md "wikilink")[亚洲象](../Page/亚洲象.md "wikilink")、[猞猁](../Page/猞猁.md "wikilink")、[长臂猿](../Page/长臂猿.md "wikilink")、[黑熊](../Page/黑熊.md "wikilink")、[大耳羊](../Page/大耳羊.md "wikilink")、[麋鹿](../Page/麋鹿.md "wikilink")、印度[犀牛等珍贵动物转送给西郊公园饲养展出](../Page/犀牛.md "wikilink")。此外，西郊公园管理处还从国外采购和交换动物，这些工作扩充了西郊公园的动物种类及数量。

### 北京动物园的发展

[Beijing_Zoo_-_Oct_2009_-_IMG_1214.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_1214.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_1214.jpg")
[Beijing_Zoo_Waterfalls.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_Waterfalls.jpg "fig:Beijing_Zoo_Waterfalls.jpg")
1955年至1975年的20年间，北京动物园获得了一定的发展，先后兴建了象房、狮虎山、猕猴馆、猩猩馆、海兽馆、两栖爬行动物馆等场馆，其中狮虎山、猩猩馆、两栖爬行馆等场馆使用至今，并成为北京动物园的标志建筑。

北京动物园曾先后饲养展出过[毛冠鹿](../Page/毛冠鹿.md "wikilink")、[秘鲁企鹅](../Page/漢波德企鵝.md "wikilink")、[西里伯斯水牛](../Page/西里伯斯水牛.md "wikilink")、[华南虎](../Page/华南虎.md "wikilink")、北美[麝牛](../Page/麝牛.md "wikilink")、[亚洲象](../Page/亚洲象.md "wikilink")“米杜拉”、[日本鬣羚](../Page/日本鬣羚.md "wikilink")、[智利火烈鸟](../Page/智利火烈鸟.md "wikilink")、[美洲河狸](../Page/美洲河狸.md "wikilink")、[白犀](../Page/白犀.md "wikilink")、[鼷鹿](../Page/鼷鹿.md "wikilink")、[暹罗鳄](../Page/暹罗鳄.md "wikilink")、[中美貘](../Page/中美貘.md "wikilink")、加勒比[海牛](../Page/海牛.md "wikilink")、[白唇鹿](../Page/白唇鹿.md "wikilink")、[阿拉伯狒狒](../Page/阿拉伯狒狒.md "wikilink")、[山魈](../Page/山魈.md "wikilink")、[黑犀](../Page/黑犀.md "wikilink")、[象龟](../Page/象龟.md "wikilink")、[绿狒](../Page/東非狒狒.md "wikilink")、[大羚羊](../Page/大羚羊.md "wikilink")、[土豚等各国领导人赠送中国的礼品动物](../Page/土豚.md "wikilink")。，以及北京动物园从国外征集采购和交换来的珍稀动物。

除了接受和饲养外交礼品动物，北京动物园还向国外提供了很多中国特有的珍贵动物，作为国际交流的礼品赠送出国，为中华人民共和国在1970年代打开[外交局面作出了独特的贡献](../Page/外交.md "wikilink")。
1980年代中国[改革开放之后](../Page/改革开放.md "wikilink")，北京动物园迅速发展，兴建了非洲象馆、雉鸡苑、中型猛兽馆、鹿苑、豺狐兽舍、袋鼠兽舍、箭猪豚鼠兽舍、鹤类繁殖岛、火烈鸟馆、朱鹮馆、夜行动物馆、热带鱼馆、热带小型猴类馆、大猩猩馆、大熊猫馆、金丝猴馆、黑颈鹤繁殖兽舍、狼山兽舍、长颈鹿馆扩建兽舍、大熊猫繁殖兽舍、小熊猫繁殖兽舍、鹤类繁殖兽舍、大熊猫馆等新兽舍场馆，其中为1990年[亚运会配套工程的新大熊猫馆还获评当年度的](../Page/亚运会.md "wikilink")[北京新十大建筑](../Page/北京新十大建筑.md "wikilink")，成为北京动物园的标志性建筑之一。
在这个阶段，北京动物园的动物繁育饲养研究也取得了长足的进步，其中大熊猫、朱鹮等珍稀动物的人工繁育技术处于国际领先地位。

1990年代，北京动物园向北扩建，同时将西部及南部的土地作为商业用地高价转让。这一时期修建了新象馆、犀牛河马馆、北京海洋馆、科普馆、鸟馆、非洲动物放养区等新馆舍，同时采取较科学的动物喂养方法，拟造其原生环境，并将部分动物散养或混养。

## 园内古迹

### 正门

[Beijing_Zoo_-_Oct_2009_-_IMG_1110.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_1110.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_1110.jpg")
[Beijing_Zoo_-_Oct_2009_-_IMG_1109.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_1109.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_1109.jpg")
动物园正门即原“农事试验场”正门，西洋式门楼，上方有雕花龙纹。门上原有农事试验场督办、大学士[刘春霖所题](../Page/刘春霖.md "wikilink")“农事试验场”。1955年4月1日，西郊公园正式更名为“北京动物园”后，时任[中国科学院院长](../Page/中国科学院.md "wikilink")[郭沫若题写园名](../Page/郭沫若.md "wikilink")“北京动物园”，制成竖牌挂在门口。[文化大革命期间](../Page/文化大革命.md "wikilink")，门楼上方的雕花龙纹被砸毁，郭沫若题写的园名牌匾被摘除，从[毛泽东手书的诗词](../Page/毛泽东.md "wikilink")《[沁园春·雪](../Page/沁园春·雪.md "wikilink")》、《七律·人民解放军占领南京》、《七律·送瘟神》等作品中选出“北京动物园”五个字拼凑成新的园名牌匾，使用至今。[文化大革命结束后](../Page/文化大革命.md "wikilink")，1987年，门楼上的雕花龙纹获得恢复。\[1\]

[Changguanlou.jpg](https://zh.wikipedia.org/wiki/File:Changguanlou.jpg "fig:Changguanlou.jpg")
[DSCN1071.JPG](https://zh.wikipedia.org/wiki/File:DSCN1071.JPG "fig:DSCN1071.JPG")

### 畅观楼

畅观楼建成于清光绪三十四年，是作为[慈禧乘船延](../Page/慈禧.md "wikilink")[长河前往](../Page/长河.md "wikilink")[颐和园途中的一个](../Page/颐和园.md "wikilink")[行宫修建的](../Page/行宫.md "wikilink")，为欧洲[巴洛克风格建筑](../Page/巴洛克.md "wikilink")，砖木混合结构二层楼房，周围有深外廊，山墙面向前开窗，屋顶内为阁楼。红砖墙体，局部抹灰及灰塑线脚并加以砖雕花饰。[爱奥尼式柱](../Page/爱奥尼式柱.md "wikilink")，曲线形山墙上缀以球形装饰。正面两端转角处为八字形阁楼，上覆拱形铁皮顶。畅观楼建成后不久慈禧、[光绪先后去世](../Page/光绪.md "wikilink")，实际上并未使用过。

[辛亥革命后](../Page/辛亥革命.md "wikilink")[孙中山来](../Page/孙中山.md "wikilink")[北京时](../Page/北京.md "wikilink")，曾先后于1912年8月29日、8月31日、9月1日三次来此，8月29日参加广东公会、全国铁路协会、邮政协会的欢迎会，8月31日参加[北京临时参议院欢迎会](../Page/北京临时参议院.md "wikilink")，9月1日参加军警界为其举行的欢迎会。\[2\]

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，北京市的第一个规划方案就诞生在畅观楼，称为“[畅观楼方案](../Page/畅观楼方案.md "wikilink")”。
畅观楼在1990年代被改为“北京皇家俱乐部”，不对游人开放。

### 南熏桥

畅观楼周围环水，楼南数十米处有白石小桥一座。该桥自修成后几经改建。
[Wangtianhou.jpg](https://zh.wikipedia.org/wiki/File:Wangtianhou.jpg "fig:Wangtianhou.jpg")

### 铜狮、铜吼

南熏桥南东为一铜狮，西为一铜吼，口均能喷水。《本国新游记》中称，“此狮于开筑园场时得之土中者。”1909年《[顺天时报](../Page/顺天时报.md "wikilink")》刊登的一篇游记中写道：“东边的铜狮，由狮口内向下喷水，水线万道；西边的麒麟脑袋向上，作回顾状，却由口内向上喷水，激射力大，如同巨泉，水声悦耳。两者的下边，各有一长方形池，池内有金鱼数尾，池水被喷水冲滴如同活水一般”。

### 陆谟克堂

陆谟克堂位于[西直门外大街](../Page/西直门外大街.md "wikilink")141号，是1934年为纪念[法国生物学家](../Page/法国.md "wikilink")[陆谟克](../Page/拉马克.md "wikilink")（今译“[拉马克](../Page/拉马克.md "wikilink")”）而建的一所中西合壁三层小楼，由[中法文化教育基金会](../Page/中法文化教育基金会.md "wikilink")、[国立北平研究院及](../Page/国立北平研究院.md "wikilink")[国立北平天然博物院等共同主持用退还的](../Page/国立北平天然博物院.md "wikilink")[庚子赔款合建](../Page/庚子赔款.md "wikilink")，是中国最早的[植物学科研楼](../Page/植物学.md "wikilink")，曾是中国现代植物科学研究的基地。整个建筑平面呈长方形，为法式3层楼，外墙砖石为灰红两色，楼南面正中刻有“陆谟克堂”四字（现已不存），一层和二层布局类似，为实验室、研究室和图书馆，三层为标本室，由[国立北平研究院植物研究所使用](../Page/国立北平研究院.md "wikilink")。1949年，中国科学院将[中华科学社](../Page/中华科学社.md "wikilink")、[静生生物调查所](../Page/静生生物调查所.md "wikilink")、[云南农林植物调查研究所等单位合并](../Page/云南农林植物调查研究所.md "wikilink")，在这里建立了[中国科学院植物分类研究所](../Page/中国科学院.md "wikilink")，1953年改为[中国科学院植物研究所](../Page/中国科学院植物研究所.md "wikilink")。\[3\]现为[中国植物学会办公地和](../Page/中国植物学会.md "wikilink")[中国科学院植物标本陈列馆筹备处](../Page/中国科学院植物标本陈列馆.md "wikilink")。2007年4月全面停用。

### 宋教仁纪念塔

[辛亥革命先驱](../Page/辛亥革命.md "wikilink")，[国民党元老](../Page/國民黨_\(1912年－1913年\).md "wikilink")[宋教仁在](../Page/宋教仁.md "wikilink")[袁世凯的](../Page/袁世凯.md "wikilink")[中华民国北京政府担任农林总长期间](../Page/中华民国北京政府.md "wikilink")，曾在[农林部下属的农事实验场内的鬯春堂居住](../Page/中华民国农林部.md "wikilink")。1913年3月宋教仁获选中华民国国务总理后遇刺身亡。1916年6月在宋教仁住过的鬯春堂北面建立了一座2米高的“宋教仁纪念塔”，塔型采用[古希腊](../Page/古希腊.md "wikilink")[方尖碑的形式](../Page/方尖碑.md "wikilink")，环塔四周种植柏树百余株。1966年[文化大革命爆发](../Page/文化大革命.md "wikilink")，因为宋教仁的身份为国民党元老，其纪念塔被毁，仅余二层混凝土基座。2009年11月，在二层混凝土基座上新立了一块“宋教仁纪念塔遗址”碑。“宋教仁纪念塔塔座”为[西城区普查登记文物](../Page/西城区普查登记文物.md "wikilink")。

### 四烈士墓

[Tomb_of_Four_Martyrs_in_Beijing_Zoo_20041205.jpg](https://zh.wikipedia.org/wiki/File:Tomb_of_Four_Martyrs_in_Beijing_Zoo_20041205.jpg "fig:Tomb_of_Four_Martyrs_in_Beijing_Zoo_20041205.jpg")
1912年1月26日国民革命先驱[彭家珍身怀炸弹](../Page/彭家珍.md "wikilink")，在[西四红罗厂刺杀](../Page/西四.md "wikilink")[宗社党党魁](../Page/宗社党.md "wikilink")[良弼](../Page/良弼.md "wikilink")，功成身死。1912年1月16日由革命党人[黄之萌](../Page/黄之萌.md "wikilink")、[张先培](../Page/张先培.md "wikilink")、[钱铁如](../Page/钱铁如.md "wikilink")、[吴若龙](../Page/吴若龙.md "wikilink")、[杨禹昌](../Page/杨禹昌.md "wikilink")、[罗明典](../Page/罗明典.md "wikilink")、[郑毓秀等](../Page/郑毓秀.md "wikilink")18人组成的暗杀团体在北京东华门伺机刺杀[袁世凯](../Page/袁世凯.md "wikilink")，制造了[东华门事件](../Page/东华门事件.md "wikilink")，当场炸死袁世凯卫队长等10余人。革命党人黄之萌、张先培、杨禹昌等10人被捕并被杀。1912年2月，南京临时政府褒扬彭黄张杨四人的革命业绩，追赠彭家珍为大将军，并为彭家珍、杨禹昌、黄芝萌、张先培等四位烈士在农事试验场营建墓地。整个墓地呈正八角形，距地面约1米，正中立约8米的纪念碑，碑上刻“彭、杨、黄、张四烈士墓”。底座的东南、东北、西南、西北各有七级台阶通向纪念碑。四烈士就安葬于正南、北、东、西四面的石冢下，每座墓前均有碑文，记录烈士事迹，并有[中国国民党党徽装饰](../Page/中国国民党.md "wikilink")。1966年文革爆发，红卫兵砸毁了饰有[青天白日标记的四烈士墓](../Page/青天白日.md "wikilink")，现仅存一块四烈士墓遗址碑存留。“四烈士墓”为[西城区普查登记文物](../Page/西城区普查登记文物.md "wikilink")。

[Lsy.jpg](https://zh.wikipedia.org/wiki/File:Lsy.jpg "fig:Lsy.jpg")

### 鬯春堂

鬯春堂位于动物园四南部畅观楼南侧，建于1908年，为中国传统建筑格局，因其屋顶结构系由三个房脊相连而成，故而也被称作三卷，前廊后厦，穿堂门，房屋四壁全部是高大的玻璃窗，堂内地板由[金砖铺成](../Page/金砖.md "wikilink")，建成后鬯春堂内摆设有[紫檀木和](../Page/紫檀木.md "wikilink")[花梨木家具以及](../Page/花梨木.md "wikilink")[慈禧太后御笔书画作品十二幅](../Page/慈禧太后.md "wikilink")。鬯春堂四周由假山环抱，假山之外一度环绕种植了[芭蕉](../Page/芭蕉.md "wikilink")、[梨树](../Page/梨.md "wikilink")、[杏树](../Page/杏.md "wikilink")、[桃树等作物](../Page/桃.md "wikilink")，环境非常幽静。

日军占领北京期间，北京政府实业总署署长[王荫泰经常在鬯春堂宴请日本顾问](../Page/王荫泰.md "wikilink")，1945年初日军占据园艺实验场，将鬯春堂作为仓库使用，1945年8月日本投降后[国民革命军接管](../Page/国民革命军.md "wikilink")，继续将鬯春堂作为仓库，直到1990年初不慎失火烧毁鬯春堂全部木构建筑，不久重建，重建后的鬯春堂一直空置至今。

### 豳风堂

[Binfeng_tang.JPG](https://zh.wikipedia.org/wiki/File:Binfeng_tang.JPG "fig:Binfeng_tang.JPG")
[Beijing_Zoo_-_Oct_2009_-_IMG_1213.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_1213.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_1213.jpg")
豳風堂位于动物园东部偏北，建于1907年，最初作为农事试验场的陈列室，展示各种农林物资和工具，1929年农事试验场改组为[国立北平研究院下属的博物馆后](../Page/国立北平研究院.md "wikilink")，豳风堂招商包租，经营茶点食品。豳风堂的主体建筑是五间房屋和一个庭院，建筑均为中国传统风格，镶大块玻璃窗，堂北假山堆砌，并有人造瀑布，堂内曾陈设清军[健锐营营中物品作为装饰](../Page/健锐营.md "wikilink")，包括[乾隆帝御笔匾额一面](../Page/乾隆帝.md "wikilink")。1949年后豳风堂原有建筑拆除，在原址改建豳风堂餐厅，现在是动物园内的一家饭馆和动物园职工食堂。

### 牡丹厅

牡丹厅位于豳风堂东侧，鹰山南侧，建成于1908年，是一座环形的长廊，南北两部分的长廊合成一个圆形，长廊南北各有一个方厅，圆形中心曾经有一个玻璃厅，后拆除。

### 东洋房

东洋房位于豳风堂东侧岛上，建于1906年，是一幢日本风格的建筑，房屋建筑在高台上，四面为日式推拉门窗，进入房间后是日式他他米，房屋周围种有樱花。东洋房最初作为日本茶馆使用，1929年农事试验场改组为天然博物院后改为仓库存储杂物，此后拆除。现在东洋房所在的小岛是水禽湖饲养班的办公地和部分鸟类的繁殖地，饲养有[黑颈鹤](../Page/黑颈鹤.md "wikilink")、[沙丘鹤](../Page/沙丘鹤.md "wikilink")、[冕鹤](../Page/冕鹤.md "wikilink")、[中白鹭](../Page/中白鹭.md "wikilink")、[黑颈天鹅等鸟类](../Page/黑颈天鹅.md "wikilink")。

### 来远楼

来远楼位于动物园西北部长河南岸，是一幢欧式风格的三层小楼，楼东西两翼有长廊，楼内曾设西餐馆，农事试验场内的照相馆也设立在此。1929年后来远楼改为博物学研究所，一度是标本剥制室所在地，1949年后拆除。

### 万字楼

万字楼位于农事试验场正中，是一座二层小楼，因其建筑平面为[卐字形状故而得名](../Page/卐.md "wikilink")，曾设有茶座，1935年失火烧毁。

### 观稼轩

观稼轩又名自在庄，位于万字楼南侧，是一片茅草房屋，其建筑形式犹如农舍，与周围的试验田非常协调，内设茶座，后因扩建动物馆舍拆除。

### 依绿亭

位于狮虎山附近。该亭原为二层海棠式亭，乃是清朝农事试验场的遗存建筑。现上层已损坏，仅余下层。2006年，为纪念北京动物园向公众开放一百周年，公园管理处在亭内立“百年纪念”碑一通，画家[黄永玉题碑名](../Page/黄永玉.md "wikilink")“百年纪念”四字，[北京大学哲学系博士](../Page/北京大学哲学系.md "wikilink")[胡仲平撰写了](../Page/胡仲平.md "wikilink")“北京动物园百年碑记”。

### 松风萝月轩

位于狮虎山附近。

### 荟芳轩

位于大熊猫馆后面，1908年建成。中式建筑，一排九间，外有栏杆，门窗采用券拱形式。1928年起为农产品标本室，陈列有数千种农产品标本。轩前为“药圃”。如今荟芳轩已经改建并已改作他用。

### 磊桥

原名青石桥，因其桥墩及桥面均为青石板而得名。1923年更名为磊桥，时任北洋政府农商总长[李根源为其题名](../Page/李根源.md "wikilink")“磊桥”，立刻有“磊桥”的石碑于桥头，碑上除“磊桥”外，只有落款“李根源”，五个字均为红色。[文化大革命初期该碑被毁](../Page/文化大革命.md "wikilink")。1997年[中国人民政治协商会议北京市第八届委员会第五次会议政协委员提案恢复该碑](../Page/中国人民政治协商会议北京市委员会.md "wikilink")，北京动物园邀[李根源之子为](../Page/李根源.md "wikilink")“磊桥”题字，恢复了该碑。\[4\]

### 广善寺和惠安寺

位于今北京动物园东南角，今西直门外大街北侧，是北京动物园的前身“两园两寺”中的两寺。广善寺仍存遗址，其东侧的惠安寺已无存。

## 主要馆舍

### 东区

[Monkey_hill.jpg](https://zh.wikipedia.org/wiki/File:Monkey_hill.jpg "fig:Monkey_hill.jpg")

#### 猴山

猴山位于北京动物园东南角，建于1946年，是动物园现存历史最悠久的馆舍，也是现存唯一一个兴建于1949年以前的馆舍。猴山占地面积1000平方米，为下沉式结构，场馆中央用山石堆积成两座假山，之间悬挂软梯、轮胎等游乐设施。

猴山的投喂现象很严重，并有鼠患，而且引起环境特殊，鼠患难以得到有效控制。

2007年因修建立交桥，旧猴山已经拆除，将另建新猴山。

#### 犬科动物区

位于北京动物园东南角，展示[犬科动物](../Page/犬科.md "wikilink")。

#### 熊山

熊山位于动物园东北角，原址为稻田，1952年动工兴建熊山，占地面积4275平方米，由黑熊和白熊两个下沉式露天馆舍组成，白熊山位于东侧，内有假山和水池。黑熊山位于西侧，分南北两个部分，南侧场地较大，有假山和水池，内为棕熊。北侧场地较小，仅有水池一座，内为黑熊。

熊山的投喂现象特别严重。

#### 猫科动物馆

位于熊山北侧，展览[朝鲜豹](../Page/遠東豹.md "wikilink")、[雪豹](../Page/雪豹.md "wikilink")、[云豹](../Page/云豹.md "wikilink")、[黑豹](../Page/黑豹.md "wikilink")、[猞猁等](../Page/猞猁.md "wikilink")[猫科动物](../Page/猫科.md "wikilink")。

[Shi_hu_shan.JPG](https://zh.wikipedia.org/wiki/File:Shi_hu_shan.JPG "fig:Shi_hu_shan.JPG")
[Beijing_Zoo_-_Oct_2009_-_IMG_1218.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_1218.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_1218.jpg")

#### 狮虎山

狮虎山建于1956年，是北京动物园的标志性建筑之一。在照相机在中国属于高档消费品的时代，许多人在动物园的纪念照都是以狮虎山为背景的。狮虎山的建筑被装饰成独特的山型结构，进入馆舍参观有如进入神秘的山洞，连接室外活动场和室内动物馆舍的通道也设计成山洞的形式，这种设计不仅有很好的视觉效果，而且可以避免冬季寒风直接吹入展厅内部。狮虎山内饲养了包括[非洲狮](../Page/非洲狮.md "wikilink")、[孟加拉虎](../Page/孟加拉虎.md "wikilink")、[东北虎](../Page/东北虎.md "wikilink")、[美洲虎](../Page/美洲虎.md "wikilink")、[黑豹在内的多种大型猫科动物](../Page/黑豹.md "wikilink")。过去，每逢假日，饲养员会向笼中投放活[鸡](../Page/鸡.md "wikilink")，以此来训练动物的野性。

#### 雉鸡苑

雉鸡苑建于1983年，位于动物园正门东侧，犬科动物区西侧，育幼室以南，展示各种[雉形目鸟类](../Page/雉形目.md "wikilink")，以及[南美鹦鹉](../Page/南美鹦鹉.md "wikilink")、[大鸨和](../Page/大鸨.md "wikilink")[神鹰](../Page/神鹰.md "wikilink")。
[Beijing_Zoo_baby_chimpanzee.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_baby_chimpanzee.jpg "fig:Beijing_Zoo_baby_chimpanzee.jpg")\]\]

#### 育幼室

雉鸡苑东侧数间房屋在1990年代被改为动物育幼室，向游客展示人工哺育[灵长目动物的过程](../Page/灵长目.md "wikilink")。

#### 水禽湖

动物园中央湖区，放养水禽。

#### 夜行动物馆

位于育幼室以北，大熊猫馆东北。该馆展示夜行动物，白天内部光线较暗，以营造夜间环境，方便游人观看。

#### 大熊猫馆

[Panda_house1.JPG](https://zh.wikipedia.org/wiki/File:Panda_house1.JPG "fig:Panda_house1.JPG")
北京动物园现存的大熊猫馆是1990年作为[第十一届亚运会](../Page/第十一届亚运会.md "wikilink")“献礼工程”兴建的，造型独特，曾经入选当年度“北京十大建筑”和“北京市优质建筑”，但实际上大熊猫馆建筑质量非常一般，使用十年之后就已经开始出现漏雨等问题。

大熊猫馆总占地面积1万平米，建筑面积1452平米，主体建筑呈盘绕的竹节形状，有11道半圆形的拱圈沿竹节延伸的方向分布，象征[第十一届亚运会](../Page/第十一届亚运会.md "wikilink")，东南侧为馆舍参观入口，西北侧为通向室外活动场的出口，室内共有三个公开展室，参观大厅顶部用悬吊12个玻璃钢大球，以调节室内音响效果，功能性区块包括隔离间、治疗间、饲料间、鲜竹储存间、产房、饲料制作间、电视监控室等均设在半地下，[大熊猫的室外活动场地自然起伏](../Page/大熊猫.md "wikilink")，设有木质栖架和游乐设施。大熊猫馆周围绿化以竹为主，通向大熊猫馆步道装饰有黑白两色鹅卵石。

另行收费，或购买门票时选择套票。

#### 貘馆

位于水禽湖以北，展示[美洲貘等](../Page/美洲貘.md "wikilink")。

#### 美洲动物区

位于貘馆以北，[长河南岸](../Page/长河.md "wikilink")，展示[美洲动物](../Page/美洲.md "wikilink")。

#### 澳洲动物区

位于豳风堂以北，[长河南岸](../Page/长河.md "wikilink")，展示[大洋洲动物](../Page/大洋洲.md "wikilink")。

### 西区

#### 鸟苑

原位于水禽湖北部小岛上，1990年代在其西北方向（现科普馆南，游乐场原址）兴建新馆群。新馆群陈设仿照自然环境，中央馆允许游客与鸟类零距离接触（原先火烈鸟馆也有此设计，开放后很快便改为普通的游客与动物隔离的观赏）。2000年代旧馆已停用。

[Nest_of_Phoenicopterus.JPG](https://zh.wikipedia.org/wiki/File:Nest_of_Phoenicopterus.JPG "fig:Nest_of_Phoenicopterus.JPG")
[Beijing_Zoo_flamingo.JPG](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_flamingo.JPG "fig:Beijing_Zoo_flamingo.JPG")

#### 火烈鸟馆

位于鸟苑以南，展示[火烈鸟](../Page/火烈鸟.md "wikilink")。

#### 朱鹮馆

位于火烈鸟馆以西，展示[朱鹮](../Page/朱鹮.md "wikilink")。

#### 两栖爬行动物馆

[Lxpx.jpg](https://zh.wikipedia.org/wiki/File:Lxpx.jpg "fig:Lxpx.jpg")

两栖爬行动物馆位于动物园西南部，紧邻鬯春堂，建于1979年，建筑面积4345平方米，曾经是北京动物园设施最好的馆舍之一。该馆分上下两层，设计有展厅、参观廊、展室、操作廊间、饲料饲养间等功能区块，馆内设置大小不一的展室90余个，展室面积根据展示的动物不同而有很大差异，用于饲养和展示蟒的中央展厅高达两层，扬子鳄的展厅面积最大，179平方米，展示蛇的展厅面积则不足1平方米。门厅左侧展示[鳄](../Page/鳄.md "wikilink")、[龟](../Page/龟.md "wikilink")、[鳖等爬行动物](../Page/鳖.md "wikilink")，最大的一间[鳄鱼展室就位于此](../Page/鳄鱼.md "wikilink")，门厅右侧楼下展示[两栖动物](../Page/两栖动物.md "wikilink")，楼上展示[蛇类](../Page/蛇.md "wikilink")，右侧展厅中央是饲养和展示网蟒的蟒蛇展厅，展厅贯通上下两层，长12[米](../Page/米.md "wikilink")，宽6.4米，高11米展，厅中央用[混凝土铸造了一棵假树供蟒蛇攀援](../Page/混凝土.md "wikilink")，厅内有空调系统和湿度调节系统，这在1970年代的中国是非常先进的。两爬馆馆外西侧有三个室外活动场，与馆内鳄鱼展室相连，活动场用鳄鱼、[青蛙等动物雕塑装饰](../Page/青蛙.md "wikilink")，突出了两栖爬行馆的主题，展馆东侧室外临湖，依水建榭与主体建筑用回廊链接，并有跃层悬梯相连。两爬馆建筑形式活泼，功能完善，是1970年代和1980年代北京动物园最重要最成功的建筑之一。

2004年至2007年，随着《提高两栖爬行动物生存力》课题的研究和成果应用，开始逐渐两栖爬行馆进行了室内展区的大规模改造，经过历时三年共三期的工程，使原有动物生活展示条件大幅度提高，甚至很多种动物开始实现人工饲养条件下的繁殖。目前两栖爬行馆仍然是北京动物园最受欢迎的动物场馆之一。

#### 猩猩馆

[Gorilla_house_in_Beijing_Zoo.JPG](https://zh.wikipedia.org/wiki/File:Gorilla_house_in_Beijing_Zoo.JPG "fig:Gorilla_house_in_Beijing_Zoo.JPG")
位于动物园西部，馆内展出大猩猩、黑猩猩等。馆外设有放养区。

#### 金丝猴馆

位于猩猩馆东，1990年代初建成。

#### 热带小猴馆

位于金丝猴馆西，展示来自[热带的各种小型猴类](../Page/热带.md "wikilink")。

#### 叶猴馆

位于猩猩馆西，展示各种[叶猴](../Page/叶猴.md "wikilink")。

#### 山魈馆

位于热带小猴馆西，展示[山魈](../Page/山魈.md "wikilink")。

#### 非洲动物区

位于科普馆北，原址为绿化树林。于2000年代开放，放养[斑马](../Page/斑马.md "wikilink")、[羚羊等非洲动物](../Page/羚羊.md "wikilink")。

#### 科普馆

[Scientific_museum.jpg](https://zh.wikipedia.org/wiki/File:Scientific_museum.jpg "fig:Scientific_museum.jpg")
1990年代，随着房地产的热潮，北京动物园在[北京市园林局的领导下](../Page/北京市园林局.md "wikilink")，对动物园南侧一面的临街地段进行了房地产开发。为了平衡广大市民的意见，逐决定在动物园内修建科普馆，“还惠于民”，2003年，世界上动物园中最大的科普馆落成，位于原有的羚羊馆的位置。科普馆展区分为四层：地上三层和地下一层，展馆开放初期为收费参观，目前免费。几年来由于在场馆建设尤其是内部装修方面存在的隐患，使馆内多数展品失修，危机四伏。2008年对原有负一层进行了改造，增加了小型脊椎动物展区，并对原有的无脊椎动物展区进行了扩建，最重要的是对原有机动展厅内的温室进行了彻底改造，建设成为“周末课堂”。2008年至2009年，科普馆开展了多项保护教育项目，受到游客的欢迎。

#### 长颈鹿馆

[Two_giraffes.JPG](https://zh.wikipedia.org/wiki/File:Two_giraffes.JPG "fig:Two_giraffes.JPG")
位于科普馆西，展示[长颈鹿](../Page/长颈鹿.md "wikilink")。

#### 儿童动物园

位于长颈鹿馆西北，儿童可以给动物喂食及触摸动物。这里深受儿童欢迎。

#### 企鹅馆

[Beijing_Zoo_penguin.JPG](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_penguin.JPG "fig:Beijing_Zoo_penguin.JPG")
位于动物园西南端，另行收费。地上部分展示[洪氏环企鹅](../Page/洪氏环企鹅.md "wikilink")，地下室展示[果蝠](../Page/果蝠.md "wikilink")，但两种动物均没有室外活动空间，生活状况堪忧。尤其是果蝠，几乎全军覆没，目前已经停止展览。展馆内的企鹅为南美企鹅，但馆内环境布置成冰天雪地的南极风格，与栖息地景观不符，动物也缺少必要的丰容环境。

#### 水獭展区

水獭展区是北京动物园在英国和澳大利亚驻华使馆资助下兴建的笼舍丰容示范项目，在新水獭馆里为动物修建了很多量身定做的游乐设施。室外有一玻璃水池，可免费观看[水獭](../Page/水獭.md "wikilink")。

#### 鹿苑

位于儿童动物园以西，展示各种[鹿](../Page/鹿.md "wikilink")。

#### 西鹤岛

位于畅观楼以东的湖中，展示[鹤](../Page/鹤.md "wikilink")。

### 北区

[Elephant_house.JPG](https://zh.wikipedia.org/wiki/File:Elephant_house.JPG "fig:Elephant_house.JPG")

#### 象馆

象馆位于长河北岸，建于1996年至1998年间，东部展览亚洲象，西部展览非洲象。馆外有活动区。旧象房位于猴山北侧，东邻[北京展览馆和](../Page/北京展览馆.md "wikilink")[莫斯科餐厅](../Page/莫斯科餐厅.md "wikilink")，1960年代曾因展览[斯里兰卡总统](../Page/斯里兰卡.md "wikilink")[班达拉奈克夫人赠送的亚洲象而闻名](../Page/班达拉奈克夫人.md "wikilink")。
[Rhinoceros_and_hippo_house.JPG](https://zh.wikipedia.org/wiki/File:Rhinoceros_and_hippo_house.JPG "fig:Rhinoceros_and_hippo_house.JPG")

#### 犀牛河马馆

犀牛河马馆位于长河北岸，建于1992年至1994年，内有[印度犀牛](../Page/印度犀牛.md "wikilink")、[白犀牛及](../Page/白犀牛.md "wikilink")[河马](../Page/河马.md "wikilink")，并设有餐厅。

旧河马馆位于水禽湖西部。

[北京海洋馆.JPG](https://zh.wikipedia.org/wiki/File:北京海洋馆.JPG "fig:北京海洋馆.JPG")

#### 北京海洋馆

1990年代兴建，位于动物园东北端。另行收费。有[海豚等海洋动物的表演](../Page/海豚.md "wikilink")。

#### 鹰山

[Beijing_Zoo_-_Oct_2009_-_IMG_0940.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Zoo_-_Oct_2009_-_IMG_0940.jpg "fig:Beijing_Zoo_-_Oct_2009_-_IMG_0940.jpg")
原位于河马馆旧址（现马来貘馆）东面的小山上，于2000年代迁往犀牛河马馆新馆西（动物园西北端）。

## 珍稀动物

  - [大熊猫](../Page/大熊猫.md "wikilink")
  - [朱鹮](../Page/朱鹮.md "wikilink")

## 重大事件

### 大蟒事件

文革末期传出谣言——

  -
    *东北某地发现一条大蟒，吃了很多人和动物。之后派出军队甚至使用了重武器才将大蟒制服，并用一整列火车将大蟒运至北京动物园展出。*

巧合的是，那段时间北京动物园因故停业，于是人们认为这验证了传言，是在给大蟒造房子。尽管官方辟谣，但人们依然不信。北京动物园重新开门迎客之后，人们蜂拥而至，据说离北京动物园五站地的路上已经挤满了人，并有传言说有人被踩死。\[5\]\[6\]

### 刘海洋伤熊案

2002年1月29日至2月23日，[清华大学](../Page/清华大学.md "wikilink")[电机系学生刘海洋先后数次用浓](../Page/电机系.md "wikilink")[硫酸和](../Page/硫酸.md "wikilink")[氢氧化钠烧伤北京动物园熊山的三只](../Page/氢氧化钠.md "wikilink")[黑熊](../Page/黑熊.md "wikilink")，一只[棕熊和一只](../Page/棕熊.md "wikilink")[马熊](../Page/马熊.md "wikilink")，动物园工作人员透露，动物园“最好的种公熊”受重伤失去生育能力，给动物园带来了很大损失。后被公安机关抓获，经庭审被定罪免刑。\[7\]此事引起了社会的关注，讨论的焦点集中于当代[大学生的](../Page/大学生.md "wikilink")[教育](../Page/教育.md "wikilink")、素质培养、[单亲家庭子女的](../Page/单亲家庭.md "wikilink")[心理健康](../Page/心理健康.md "wikilink")、[动物权利立法等不同方面](../Page/动物权利.md "wikilink")。

### 北京动物园搬迁案

2004年春季，北京市[发改委和北京市](../Page/发展与改革委员会.md "wikilink")[大兴区政府筹划将北京动物园迁出目前的位置](../Page/大兴区.md "wikilink")，与经济发生困难的大兴区北京野生动物园合并，并将动物园用地作为商业用地出售。消息透露后遭到了普通市民、环保人士、动物专家和新闻媒体的强力反对，在社会各界的强大压力下，北京动物园搬迁的提案搁浅。

### 北京动物园志愿者

[Volunteers.JPG](https://zh.wikipedia.org/wiki/File:Volunteers.JPG "fig:Volunteers.JPG")
2004年春季，北京动物园、[中国动物园协会](../Page/中国动物园协会.md "wikilink")、[中国青年报绿岛](../Page/中国青年报.md "wikilink")、[绿家园志愿者等单位联合发起向社会公开招募动物园](../Page/绿家园志愿者.md "wikilink")[志愿者](../Page/志愿者.md "wikilink")，为游客提供讲解服务。北京动物园的[志愿者是目前](../Page/志愿者.md "wikilink")[中国](../Page/中国.md "wikilink")[大陆最早的也是唯一的动物园志愿者组织](../Page/大陆.md "wikilink")。

### 原北京动物园副园长肖绍祥涉贪千万

2014年7月，据北京市检二分院指控，利用担任北京动物园副园长、陶然亭公园管理处园长的便利，肖绍祥在主管动物园基建、110千伏输变电站拆迁、草库拆迁、北京动物园兽舍改造、陶然亭公园玉虹桥改建及休息游廊工程项目中，采取侵吞工程款及拆迁款、虚开发票等手段，贪污1400余万元。而在案发前，其还有800余万元钱款无法说明具体合法来源。肖绍祥涉嫌贪污罪、受贿罪、巨额财产来源不明罪，被北京市检二分院提起公诉。\[8\]

## 参考文献

## 外部链接

  - [北京动物园官方网站](http://www.bjzoo.com)

{{-}}

[Category:北京市公园](../Category/北京市公园.md "wikilink")
[Category:中国动物园](../Category/中国动物园.md "wikilink")
[Category:北京市纪念碑](../Category/北京市纪念碑.md "wikilink")
[Category:北京市西城区普查登记文物](../Category/北京市西城区普查登记文物.md "wikilink")

1.  [从农事实验场到北京动物园，北青网，2006-12-12](http://www.ynet.com/view.jsp?oid=17898064)

2.  [董宝光，北京动物园中的辛亥革命记录，中国共产党新闻网，2011年07月28日](http://dangshi.people.com.cn/GB/15275265.html)
3.  [陆谟克堂，西城文化，于2013-02-24查阅](http://wenhua.bjxch.gov.cn/pub/xch_zhuzhan/B/xch_wenhua/wh_wwbh/qjwwbh/200803/t20080310_1109457.html)
4.  杨小燕，北京动物园的『磊桥』，北京档案2008(6)
5.  [北京动物园惊人的巨蟒](http://blog.sina.com.cn/s/blog_4b3c2d0001008mlx.html)
6.  [北京动物园大蟒](http://ma3r.blogbus.com/logs/218239898.html)
7.
8.  [北京动物园副园长涉贪千万](http://www.ce.cn/xwzx/fazhi/201407/31/t20140731_3260891.shtml)，中国经济网，2014年7月31日