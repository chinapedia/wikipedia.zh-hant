**臘肉**是中國[醃肉的一种](../Page/醃.md "wikilink")，主要流行于[四川](../Page/四川.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[江西和](../Page/江西.md "wikilink")[广东一带](../Page/广东.md "wikilink")，但在南方其他地区也有制作，由于通常是在[农历的](../Page/农历.md "wikilink")[腊月进行腌制](../Page/腊月.md "wikilink")，所以称作“腊肉”。

## 歷史

据《[易经](../Page/易经.md "wikilink")-噬嗑篇释文》：“晞于阳而炀于火，曰腊肉。”证明臘肉已经有两千多年的历史。由于南方气候[潮湿](../Page/潮湿.md "wikilink")，把生肉（[猪肉](../Page/猪肉.md "wikilink")、[牛肉](../Page/牛肉.md "wikilink")、[鱼肉](../Page/鱼肉.md "wikilink")、[禽肉如](../Page/禽肉.md "wikilink")[雞肉](../Page/雞肉.md "wikilink")，[鴨肉等](../Page/鴨肉.md "wikilink")）制成臘肉可以延长保质时期，而且臘肉味美，深得当地人的喜爱。

## 製作

煙燻臘肉：先将[晶体状的](../Page/晶体.md "wikilink")[粗盐](../Page/粗盐.md "wikilink")[炒](../Page/炒.md "wikilink")[热](../Page/热.md "wikilink")，在一定程度时把新鲜的猪肉放进锅裏，与盐混合均匀。冷却以后腌渍一周时间。一周后把肉拿出洗白，再盖上纱网晾晒到乾。之后用浓[烟熏烤](../Page/烟熏.md "wikilink")，火候要控制好，太大了肉就熟，太小了就不入香味而且有很重的烟熏味。肉外表呈灰黄色时就算制作完毕。也有很多时候长时间熏烤，最后成为深黑色。

金門高梁臘肉：採取方式為依照等比例之五花肉搭配中藥藥材和金門高梁酒去浸製，中藥需研磨味道與效果會更佳，浸泡過程中也需要添加醬油或鹽，約浸泡6天後拿出風乾即可。此種方式較健康無煙燻的致癌顧慮。

### 广式腊肉

与其他地区以熏烤为制作手段的腊肉不同，[广式腊肉多采取](../Page/广式腊肉.md "wikilink")[风干的方法制作](../Page/风干.md "wikilink")。将[五花肉切成约一寸宽的条状](../Page/五花肉.md "wikilink")，然后以[酱油](../Page/酱油.md "wikilink")、[酒](../Page/酒.md "wikilink")、[白糖等混合而成的](../Page/白糖.md "wikilink")[调味料](../Page/调味料.md "wikilink")[腌渍](../Page/腌渍.md "wikilink")，待其颜色转为褐色时，在肉条的一端钻孔，穿上线绳，挂于户外风干即成广式腊肉。

## 菜式

关于臘肉的[湘菜有](../Page/湘菜.md "wikilink")：

  - [冬笋炒臘肉](../Page/冬笋.md "wikilink")
  - [苦瓜炒臘肉](../Page/苦瓜.md "wikilink")
  - 臘味合蒸（臘肉、[腊鱼](../Page/腊鱼.md "wikilink")、[腊鸡](../Page/腊鸡.md "wikilink")）
  - 白椒炒臘肉
  - 蒜苗臘肉

有关腊肉的江浙菜/杭帮菜有:

  - [腌笃鲜](../Page/腌笃鲜.md "wikilink")
  - 咸肉冬瓜汤
  - 咸肉焖笋

有关腊肉的赣菜有:

  - [藜蒿炒腊肉](../Page/藜蒿炒腊肉.md "wikilink")
  - [油菜柳](../Page/油菜.md "wikilink")（[菜苔](../Page/菜苔.md "wikilink")）炒腊肉'''

关于臘肉的[鄂菜有](../Page/鄂菜.md "wikilink")：

  - [洪山菜薹炒腊肉](../Page/洪山菜薹.md "wikilink")

[Category:四川食品](../Category/四川食品.md "wikilink")
[Category:湖南食品](../Category/湖南食品.md "wikilink")
[Category:广东食品](../Category/广东食品.md "wikilink")
[Category:香港食品](../Category/香港食品.md "wikilink")
[Category:臘味](../Category/臘味.md "wikilink")