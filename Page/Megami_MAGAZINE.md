《**Megami
MAGAZINE**》（，「女神雜誌」）是[學習研究社發行的](../Page/學習研究社.md "wikilink")[動畫中心](../Page/動畫.md "wikilink")[美少女](../Page/美少女.md "wikilink")[角色雜誌](../Page/角色.md "wikilink")。1999年7月發行，自2003年3月號起獨立創刊。簡稱是「Megamaga」（****）。

## 概要

最初是在1999年7月，因為該社發行的《[Looker](../Page/Looker.md "wikilink")》前身雜誌《[AnimeV](../Page/AnimeV.md "wikilink")》休刊，而利用此空缺作為《[Animedia](../Page/Animedia.md "wikilink")》增刊號發行，自34號（2003年3月號）起獨立創刊。本來內容主要是以所謂的[電子遊戲為中心](../Page/電子遊戲.md "wikilink")，不過隨著2000年12月發售的增刊《Megami
MAGAZINE
Special》發行，成為月刊後轉換方向變成以[動畫為中心](../Page/動畫.md "wikilink")。剛創刊時挖掘了許多小眾作品，尤其是最早以特集介紹很少被動畫雜誌報導的《[彗星公主](../Page/彗星公主_\(動畫\).md "wikilink")》這件事相當著名。

其特徵是每號附錄20面以上的封面介紹與[海報](../Page/海報.md "wikilink")。介紹過的作品，不論大小，已超過1000個。除了美少女角色的極端暴露設計的[泳裝與](../Page/泳裝.md "wikilink")[內衣描寫相當顯著以外](../Page/內衣.md "wikilink")，也會刊載[半裸圖像](../Page/半裸.md "wikilink")；雖是以男性為讀者對象，也有少數的女性讀者。尤其是投稿插圖到讀者專欄「MegaTen\!」的讀者，據說有不少是女性（同樣的傾向在[MediaWorks的](../Page/MediaWorks.md "wikilink")《[電擊G's
magazine](../Page/電擊G's_magazine.md "wikilink")》也能見到）。

該雜誌的成功，影響其他出版社也發行了《[A☆Princess](../Page/A☆Princess.md "wikilink")》（[德間書店](../Page/德間書店.md "wikilink")・[Animage增刊](../Page/Animage.md "wikilink")）、《[otomex](../Page/otomex.md "wikilink")》（[Hobby
Japan](../Page/Hobby_Japan.md "wikilink")）、《[電擊Animaga](../Page/電擊Animaga.md "wikilink")》（[MediaWorks](../Page/MediaWorks.md "wikilink")）、《[Comp
H's](../Page/Comp_H's.md "wikilink")》（[角川書店](../Page/角川書店.md "wikilink")，[Comptiq增刊](../Page/Comptiq.md "wikilink")）等多數以[萌系動畫為中心的增刊或](../Page/萌系動畫.md "wikilink")[情報誌](../Page/情報誌.md "wikilink")，都尚未能夠與該雜誌成為競爭對手的關係。

## 封面一覽

  - 1號：（鳴瀨真奈美、冰川菜織）
  - 2號：[秋之回憶](../Page/秋之回憶.md "wikilink")（今坂唯笑、檜月彩花）畫：[佐佐木睦美](../Page/佐佐木睦美.md "wikilink")
  - 3號：[機動戰艦](../Page/機動戰艦.md "wikilink")（星野琉璃）
  - 4號：[守護月天](../Page/守護月天.md "wikilink")（小璘、汝昂）
  - 5號：[歡迎來到Pia Carrot2 DX](../Page/歡迎來到Pia_Carrot!!系列.md "wikilink")（）
  - 6號：[Di Gi Charat](../Page/Di_Gi_Charat.md "wikilink")（數-{}-碼子、雛子）
  - 7號：[純情房東俏房客](../Page/純情房東俏房客.md "wikilink")（成瀨川奈留）
  - 8號：[幸運女神](../Page/幸運女神.md "wikilink")（蓓爾丹蒂）
  - 9號：Di Gi Charat（數-{}-碼子、布子）
  - 10號：[逮捕令](../Page/逮捕令.md "wikilink")（過本夏實、小早川美幸）
  - 11號：純情房東俏房客（成瀨川奈留、前原忍、卡歐拉·蘇）
  - 12號：Di Gi Charat（數-{}-碼子、雛子）
  - 13號：逮捕令（過本夏實、小早川美幸）畫：[中嶋敦子](../Page/中嶋敦子.md "wikilink")
  - 14號：[漫畫派對](../Page/漫畫派對.md "wikilink")（高瀨瑞希、長谷部彩）
  - 15號：[銀河天使](../Page/銀河天使.md "wikilink")（[梅爾優·櫻葉、薄荷·布拉曼修、香草·亞修](../Page/銀河天使登場人物列表#天使隊.md "wikilink")）
  - 16號：[魔力女管家](../Page/魔力女管家.md "wikilink")（安藤麻幌）
  - 17號：[天使的尾巴](../Page/天使的尾巴.md "wikilink")（金魚的蘭）
  - 18號：[守護月天](../Page/守護月天.md "wikilink")（小璘、紀柳）
  - 19號：Di Gi Charat（數-{}-碼子、布子、免田光、雛子）
  - 20號：天使的尾巴（金魚的蘭、兔子的美香、倉鼠的久留美、貓的珠美、青蛙的、狸貓的綠）2面
  - 21號：[櫻花大戰](../Page/櫻花大戰.md "wikilink")（真宮寺櫻）
  - 22號：[星空的邂逅](../Page/星空的邂逅.md "wikilink")（風見瑞穗、綠川小石）畫：
  - 23號：銀河天使（梅爾優·櫻葉）
  - 24號：[Chobits](../Page/Chobits.md "wikilink")（小唧）
  - 25號：星空的邂逅（）畫：
  - 26號：[青出於藍](../Page/青出於藍_\(動畫\).md "wikilink")（櫻庭葵）
  - 27號：[迷糊天使](../Page/迷糊天使.md "wikilink")（美紗、紫亞、植松小星）
  - 28號：[陸上防衛隊](../Page/陸上防衛隊.md "wikilink")（鬼瓦真緒、築島美空、丸山席維亞）畫：[赤松健](../Page/赤松健.md "wikilink")
  - 29號：銀河天使（梅爾優·櫻葉、薄荷·布拉曼修）
  - 30號：[魔力女管家～更美麗的事物～](../Page/魔力女管家.md "wikilink")（安藤麻幌、安藤美奈和）2面
  - 31號：[妹妹公主 ～Re Pure～](../Page/妹妹公主.md "wikilink")（可憐、咲耶、千影）
  - 32號：青出於藍（櫻庭葵）
  - 33號：銀河天使（梅爾優·櫻葉）
  - 34號：妹妹公主RePure（咲耶）
  - 35號：[天使特警](../Page/天使特警.md "wikilink")（安妮、爱丽丝）畫：[後藤圭二](../Page/後藤圭二.md "wikilink")
  - 36號：[天使的尾巴Chu\!](../Page/天使的尾巴.md "wikilink")（金魚的蘭、蛇的、兔子的美香、烏龜的、鸚鵡的翼、倉鼠的久留美、狐狸的茜、狸貓的綠、貓的珠美、猴子的桃、狗的奈奈、青蛙的）
  - 37號：[Di Gi Charat
    Nyo](../Page/Di_Gi_Charat_Nyo.md "wikilink")（數-{}-碼子、布子）
  - 38號：[青檸色之戰奇譚](../Page/青檸色之戰奇譚.md "wikilink")（真田木綿）
  - 39號：[拜託了雙子星](../Page/拜託了雙子星.md "wikilink")（宮藤深衣奈、小野寺樺恋）畫：
  - 40號：[Popotan](../Page/Popotan.md "wikilink")（）
  - 41號：[真月譚月姬](../Page/真月譚月姬.md "wikilink")（爱尔奎特·布伦史塔德）畫：[TYPE-MOON](../Page/TYPE-MOON.md "wikilink")
  - 42號：[你所期望的永遠](../Page/你所期望的永遠.md "wikilink")（涼宮遥、速瀨水月）畫：[杉原鎧](../Page/杉原鎧.md "wikilink")
  - 43號：[青出於藍 ～緣～](../Page/青出於藍#第二部.md "wikilink")（櫻庭葵）
  - 44號：[D.C. ～戀愛學園～](../Page/初音島.md "wikilink")（朝倉音夢、白河小鸟、芳乃樱）
  - 45號：[愛的魔法](../Page/愛的魔法.md "wikilink")（宮間夕菜）
  - 46號：你所期望的永遠（涼宮遥、速瀨水月、涼宮茜）
  - 47號：[夢與現實](../Page/夢與現實.md "wikilink")（）
  - 48號：愛的魔法（宮間夕菜）
  - 49號：[青檸色之戰奇譚 ～南國夢浪漫～](../Page/青檸色之戰奇譚#OVA版.md "wikilink")（真田木綿）
  - 50號：[這醜陋又美麗的世界](../Page/這醜陋又美麗的世界.md "wikilink")（光、明）畫：[高村和宏](../Page/高村和宏.md "wikilink")
  - 51號：[月東日西 ～Operation
    Sanctuary～](../Page/月東日西_～Operation_Sanctuary～.md "wikilink")（天之崎美琴、藤枝保奈美）畫：
  - 52號：[親愛一族](../Page/親愛一族.md "wikilink")（）畫：[PEACH-PIT](../Page/PEACH-PIT.md "wikilink")
  - 53號：銀河天使（梅爾優·櫻葉、烏丸千歲）畫：
  - 54號：[下級生2
    ～瞳の中の少女たち～](../Page/下級生2.md "wikilink")（）畫：[門井亞矢](../Page/門井亞矢.md "wikilink")
  - 55號：[你所期望的永遠 外传](../Page/你所期望的永遠.md "wikilink")（涼宮茜、榊千鶴）畫：杉原鎧
  - 56號：[To Heart ～Remenber my
    memories～](../Page/To_Heart.md "wikilink")（）
  - 57號：幸運女神（蓓爾丹蒂）画：[大河原晴男](../Page/大河原晴男.md "wikilink")
  - 58號：[AIR（劇場版）](../Page/AIR.md "wikilink")（神尾觀鈴）画：[小林明美](../Page/小林明美.md "wikilink")
  - 59號：[魔法老師（2005年版）](../Page/魔法老師_\(動畫\).md "wikilink")（[佐佐木莳绘](../Page/佐佐木莳绘.md "wikilink")、[近衛木乃香](../Page/近衛木乃香.md "wikilink")、[神乐坂明日菜](../Page/神乐坂明日菜.md "wikilink")）
  - 60號：[AIR（TV版）](../Page/AIR.md "wikilink")（神尾觀鈴）
  - 61號：[D.C.S.S.
    ～戀愛學園～](../Page/初音島.md "wikilink")（白河曆、爱西亚）画：[高品有桂](../Page/高品有桂.md "wikilink")
  - 62號：[我的主人愛作怪](../Page/我的主人愛作怪.md "wikilink")（澤渡泉、澤渡美月、倉内安奈）畫：[高村和宏](../Page/高村和宏.md "wikilink")
  - 63號：魔法老師（2005年版）（神乐坂明日菜、近衛木乃香）畫：
  - 64號：[銀河天使II](../Page/銀河天使II.md "wikilink")（櫻葉、櫻葉、）畫：
  - 65號：[AIR（夏・特別編）](../Page/AIR.md "wikilink")（神尾觀鈴、神奈備命）畫：[樋上至](../Page/樋上至.md "wikilink")
  - 66號：（朝倉音夢、白河小鸟）画：[島澤範子](../Page/島澤範子.md "wikilink")
  - 67號：[To Heart
    2](../Page/To_Heart_2.md "wikilink")（向坂環、小牧愛佳）畫：[西尾公博](../Page/西尾公博.md "wikilink")
  - 68號：[魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")（高町奈葉、菲特・泰斯塔羅沙）畫：[奧田泰弘](../Page/奧田泰弘.md "wikilink")
  - 69號：[Fate/stay
    night](../Page/Fate/stay_night.md "wikilink")（Saber）畫：[石原惠](../Page/石原惠.md "wikilink")
  - 70號：D.C.S.S. ～戀愛學園～（朝倉音夢、白河曆、水越萌、水越眞子、天枷美春、芳乃櫻、爱西亚）畫：高品有桂
  - 71號：[舞-乙HiME](../Page/舞-乙HiME.md "wikilink")（爱莉卡·梦宫、鴇羽舞衣、妮娜·王）畫：[久行宏和](../Page/久行宏和.md "wikilink")

<!-- end list -->

  -
    [女生愛女生](../Page/女生愛女生.md "wikilink")（大佛彈、、来栖止）畫：[桂遊生丸](../Page/桂遊生丸.md "wikilink")
    2面

<!-- end list -->

  - 72號：[灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（夏娜）畫：[伊東雜音](../Page/伊東雜音_\(1999年出道\).md "wikilink")
  - 73號：Fate/stay night（Saber）畫：石原惠
  - 74號：[校園迷糊大王
    二学期](../Page/校園迷糊大王.md "wikilink")（[塚本天滿、塚本八雲、澤近愛理、周防美琴](../Page/校園迷糊大王登場人物.md "wikilink")）
  - 75號：[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")（涼宮春日、朝比奈實玖留、長門有希）画：[池田晶子](../Page/池田晶子.md "wikilink")
  - 76號：[犬神\!](../Page/犬神!.md "wikilink")（）
  - 77號：[魔法少女奈葉StrikerS](../Page/魔法少女奈葉StrikerS.md "wikilink")（高町奈葉、菲特·T·哈洛溫、八神疾風、天使之翼II）畫：奧田泰弘
  - 78號：[魔法老師\!?](../Page/魔法老師_\(動畫\).md "wikilink")（神乐坂明日菜）畫：大田和寬
  - 79號：（櫻葉、櫻葉）畫：
  - 80號：[夜明前的瑠璃色](../Page/夜明前的瑠璃色.md "wikilink")（鷹見澤菜月、）畫：
  - 81號：[kanon](../Page/kanon.md "wikilink")（月宮亞由、水瀨名雪）畫：池田和美
  - 82號：魔法少女奈葉StrikerS（高町奈葉、昴・中島）畫：奧田泰弘
  - 83號：魔法老师\!?（神乐坂明日菜）畫：大田和寛
  - 84號：魔法少女奈葉StrikerS（高町奈葉、菲特·T·哈洛溫、八神疾風、昴·中島、蒂亞娜·蘭斯塔）畫：奧田泰弘
  - 85號：[旋風管家](../Page/旋風管家.md "wikilink")（三千院凪、瑪利亞、桂雛菊）畫：[堀內修](../Page/堀內修.md "wikilink")
  - 86號：[sola](../Page/sola.md "wikilink")（四方茉莉、森宮蒼乃）畫：[七尾奈留](../Page/七尾奈留.md "wikilink")
  - 87號：[零之使魔～雙月的騎士～](../Page/零之使魔.md "wikilink")（露易絲·法蘭西斯·露·布朗·杜·拉·瓦里埃爾）畫：[藤井昌宏](../Page/藤井昌宏.md "wikilink")
  - 88號：魔法少女奈葉StrikerS（高町奈葉、菲特·T·哈洛溫）畫：奧田泰弘
  - 89號：[七色星露](../Page/七色星露.md "wikilink")（）畫：伊東雜音
  - 90號：魔法少女奈葉StrikerS（高町奈葉、菲特·T·哈洛溫、昴·中島、蒂亞娜·蘭斯塔、凱珞·露·露茜、艾力歐·蒙迪爾、薇薇鷗）畫：奧田泰弘
  - 91號：魔法少女奈葉StrikerS（高町奈葉、菲特·T·哈洛溫）畫：奧田泰弘
  - 92號：灼眼的夏娜（夏娜）畫：伊東雜音
  - 93號：[CLANNAD](../Page/CLANNAD.md "wikilink")（古河渚）畫：池田和美
  - 94號：[BAMBOO
    BLADE](../Page/BAMBOO_BLADE.md "wikilink")（川添珠姬）畫：[双柳雪智](../Page/双柳雪智.md "wikilink")
  - 95號：[灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（夏娜）畫：[大塚舞](../Page/大塚舞.md "wikilink")
  - 96號：CLANNAD（坂上智代、古河渚、藤林杏）畫：池田和美
  - 97號：[初音岛II](../Page/初音岛II.md "wikilink")（朝倉音姬、朝倉由夢、月島小戀、雪村杏、花咲茜、白河奈奈佳）画：
  - 98號：[空之境界
    劇場版](../Page/空之境界.md "wikilink")（兩儀式、黑桐鲜花）画：[武内崇](../Page/武内崇.md "wikilink")
  - 99號：[零之使魔～三美姬的輪舞～](../Page/零之使魔.md "wikilink")（露易絲·法蘭西斯·露·布朗·杜·拉·瓦里埃爾）画：藤井昌宏
  - 100號：魔法少女奈葉StrikerS（[高町奈葉](../Page/高町奈葉.md "wikilink")、[菲特·T·哈洛溫](../Page/菲特·T·哈洛溫.md "wikilink")）畫：奧田泰弘
  - 101号：[天元突破
    紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")（优子）画：[锦织敦史](../Page/锦织敦史.md "wikilink")
  - 102号：CLANNAD（古河渚、藤林杏、一之濑琴美、坂上智代）畫：池田和美
  - 103号：[Strike
    Witches](../Page/Strike_Witches.md "wikilink")（宫藤芳佳、莉涅特·毕晓普、佛兰切斯卡·鲁基尼）画：[山川宏治](../Page/山川宏治.md "wikilink")
  - 104号：[武装机甲](../Page/武装机甲.md "wikilink")（城崎絵美）画：[坂崎忠](../Page/坂崎忠.md "wikilink")
  - 105号：[穿越宇宙的少女](../Page/穿越宇宙的少女.md "wikilink")（狮子堂秋叶）画：[椛岛洋介](../Page/椛岛洋介.md "wikilink")
  - 106号：[神薙](../Page/神薙.md "wikilink")（薙、忏悔）画：[門脇聰](../Page/門脇聰.md "wikilink")
  - 107號：[TIGER×DRAGON！](../Page/TIGER×DRAGON！.md "wikilink")（櫛枝實乃梨、逢坂大河、川嶋亞美）畫：[田中將賀](../Page/田中將賀.md "wikilink")
  - 108號：[CLANNAD ～AFTER
    STORY～](../Page/CLANNAD_～AFTER_STORY～.md "wikilink")（古河早苗、岡崎汐、古河渚）畫：池田和美
  - 109號：[K-ON\!](../Page/K-ON!.md "wikilink")（田井中律、秋山澪、平澤唯、琴吹紬）画：[堀口悠纪子](../Page/堀口悠纪子.md "wikilink")
  - 110號：[天才麻將少女](../Page/天才麻將少女.md "wikilink")（宮永咲、原村和）畫：[佐佐木政勝](../Page/佐佐木政勝.md "wikilink")
  - 111号：[福音战士新剧场版：破](../Page/福音战士新剧场版：破.md "wikilink")（式波·明日香·兰格雷、綾波零、真希波·真理·伊拉絲多莉亞斯）画：[饭田史雄](../Page/饭田史雄.md "wikilink")
  - 112號：魔法少女奈葉 1st Movie（高町奈葉、菲特·T·哈洛溫）畫：奧田泰弘
  - 113號：Fate/stay night UBW 剧场版（Saber、远坂凛）畫：石原惠
  - 114号：[科学超电磁炮](../Page/科学超电磁炮.md "wikilink")（[御坂美琴](../Page/御坂美琴.md "wikilink")、[白井黑子](../Page/白井黑子.md "wikilink")）画：[石仓敬一](../Page/石仓敬一.md "wikilink")
  - 115号：涼宮春日的憂鬱（涼宮春日、朝比奈實玖留、長門有希）画：[西屋太志](../Page/西屋太志.md "wikilink")
  - 116号：[碧阳学园学生会议事录](../Page/碧阳学园学生会议事录.md "wikilink")（樱野栗梦、红叶知弦、椎名深夏、椎名真冬）画：[堀井久美](../Page/堀井久美.md "wikilink")
  - 117號：魔法少女奈葉 1st Movie（高町奈葉）畫：奧田泰弘
  - 118號：魔法少女奈葉 1st Movie（高町奈葉、八神疾风、天使之翼）畫：奧田泰弘
  - 119号：科学超电磁炮（御坂美琴）画：[柳伸亮](../Page/柳伸亮.md "wikilink")
  - 120號：[K-ON\!\!](../Page/K-ON!!.md "wikilink")（秋山澪、平澤唯）画：堀口悠纪子
  - 121號：[迷糊餐厅](../Page/迷糊餐厅.md "wikilink")（伊波真昼）画：[足立慎吾](../Page/足立慎吾.md "wikilink")
  - 122號：K-ON\!\!（平澤唯）画：堀口悠纪子
  - 123號：[Angel
    Beats](../Page/Angel_Beats.md "wikilink")（ゆり、立华奏）画：[关口可奈味](../Page/关口可奈味.md "wikilink")
  - 124号：[Strike
    Witches](../Page/Strike_Witches.md "wikilink")（宫藤芳佳、莉涅特·毕晓普）画：山川宏治
  - 125號：K-ON\!\!（秋山澪、平澤唯）画：堀口悠纪子
  - 126号：[魔法禁书目录II](../Page/魔法禁书目录.md "wikilink")（御坂美琴、Index）画：[田中雄一](../Page/田中雄一.md "wikilink")
  - 127号：[我的妹妹哪有这么可爱](../Page/我的妹妹哪有这么可爱.md "wikilink")（高坂桐乃）画：[石田可奈](../Page/石田可奈.md "wikilink")
  - 128號：魔法少女奈葉 1st Movie（高町奈葉、菲特·T·哈洛溫）畫：奧田泰弘
  - 129号：[魔法少女小圆](../Page/魔法少女小圆.md "wikilink")（鹿目圆）画：[高桥美香](../Page/高桥美香.md "wikilink")
  - 130号：[偶像大师](../Page/偶像大师.md "wikilink")（天海春香）
    画：[高田晃](../Page/高田晃.md "wikilink")
  - 131号：魔法少女小圆（晓美焰）画：[谷口淳一郎](../Page/谷口淳一郎.md "wikilink")
  - 132号：[DOG
    DAYS](../Page/DOG_DAYS.md "wikilink")（米露茜欧莱·F·比斯科帝）画：[坂田理](../Page/坂田理.md "wikilink")
  - 133号：[绯弹的亚莉亚](../Page/绯弹的亚莉亚.md "wikilink")（神崎·H·亚莉亚）画：[岩仓和宪](../Page/岩仓和宪.md "wikilink")
  - 134号：[电波女与青春男](../Page/电波女与青春男.md "wikilink")（藤和艾莉欧、星宫社）画：[西田亚沙子](../Page/西田亚沙子.md "wikilink")
  - 135号：偶像大师（星井美希、如月千早、菊地真、四条贵音） 画：[饭塚晴子](../Page/饭塚晴子.md "wikilink")
  - 136号：[Steins;Gate](../Page/Steins;Gate.md "wikilink")（牧濑红莉栖、椎名真由里）画：[坂井久太](../Page/坂井久太.md "wikilink")
  - 137號：[摇曳百合](../Page/摇曳百合.md "wikilink")（赤座灯里、岁纳京子、船见结衣、吉川千夏）畫：[皆川一德](../Page/皆川一德.md "wikilink")
  - 138號：偶像大师（天海春香、水瀬伊織）畫：饭塚晴子
  - 139號：[我的朋友很少](../Page/我的朋友很少.md "wikilink")（柏崎星奈、三日月夜空）画：[渡边义弘](../Page/渡边义弘.md "wikilink")
  - 140號：偶像大师（星井美希、如月千早、高槻弥生） 画：饭塚晴子
  - 141號：魔法少女奈葉 2nd Movie A's（高町奈葉、菲特·T·哈洛溫）畫：奧田泰弘
  - 142号：偶像大师（天海春香、如月千早）畫：饭塚晴子
  - 143號：[伪物语](../Page/伪物语.md "wikilink")（忍野忍）畫：[渡边明夫](../Page/渡边明夫.md "wikilink")
  - 144号：[天才麻将少女 阿知贺篇](../Page/天才麻将少女.md "wikilink")（高鴨穏乃、原村和）画：佐佐木政胜
  - 145号：魔法少女奈葉 2nd Movie A's（高町奈葉、菲特·T·哈洛溫）畫：奧田泰弘
  - 146号：[加速世界](../Page/加速世界.md "wikilink")（黑雪公主）画：[神户洋行](../Page/神户洋行.md "wikilink")
  - 147號：魔法少女奈葉 2nd Movie A's（高町奈葉、菲特·T·哈洛溫）畫：奧田泰弘
  - 148號：DOG
    DAYS'（米露茜欧莱·F·比斯科帝、高槻七海、瑞贝卡・安德森）画：[桥立佳奈](../Page/桥立佳奈.md "wikilink")
  - 149号：摇曳百合（赤座灯里、岁纳京子、船见结衣、吉川千夏）畫：[中岛千明](../Page/中岛千明.md "wikilink")
  - 150號：魔法少女奈葉 2nd Movie A's & Dog Days'（高町奈葉、米露茜欧莱·F·比斯科帝）畫：奧田泰弘、坂田理
  - 151號：[剧场版 魔法少女小圆](../Page/剧场版_魔法少女小圆.md "wikilink")（鹿目圆、晓美焰）画：谷口淳一郎
  - 152號：[中二病也要谈恋爱](../Page/中二病也要谈恋爱.md "wikilink")（小鸟游六花）画：池田和美
  - 153號：[出包王女DARKNESS](../Page/出包王女.md "wikilink")（金色之暗、提亚悠・鲁纳提克）画：[冈勇一](../Page/冈勇一.md "wikilink")
  - 154号：[少女&坦克](../Page/少女&坦克.md "wikilink")（西住美穗）画：[杉本功](../Page/杉本功.md "wikilink")
  - 155號：[Vividred
    Operation](../Page/Vividred_Operation.md "wikilink")（一色茜）画：[高瀬智章](../Page/高瀬智章.md "wikilink")
  - 156号：[少女&坦克](../Page/少女&坦克.md "wikilink")（西住美穗&武部沙织&五十铃华&秋山优花里&冷泉麻子）画：[杉本功](../Page/杉本功.md "wikilink")
  - 157号：[变态王子与不笑猫](../Page/变态王子与不笑猫.md "wikilink")（筒隐月子）画：[监督](../Page/监督_\(插画家\).md "wikilink")
  - 158号：[少女&坦克](../Page/少女&坦克.md "wikilink")（西住美穗&安丘比）画：[伊藤岳史](../Page/伊藤岳史.md "wikilink")
  - 159号：[某科学的超电磁炮S](../Page/某科学的超电磁炮.md "wikilink")（[御坂美琴](../Page/御坂美琴.md "wikilink")&[御坂妹](../Page/妹妹们.md "wikilink")）画：田中雄一
  - 160号：[恶魔高校D×D
    NEW](../Page/恶魔高校D×D.md "wikilink")（莉雅丝·吉蒙里）画：[ごとうじゅんじ](../Page/后藤润二.md "wikilink")
  - 161号：[物语系列](../Page/物語系列_第二季.md "wikilink")（忍野忍&八九寺真宵）画：渡边明夫
  - 162号：[恶魔高校D×D NEW](../Page/恶魔高校D×D.md "wikilink")（莉雅丝·吉蒙里）画：ごとうじゅんじ
  - 163号：[剧场版 魔法少女小圆
    ［新篇］叛逆的物语](../Page/剧场版_魔法少女小圆.md "wikilink")（百江渚&巴麻美）画：[谷口淳一郎](../Page/谷口淳一郎.md "wikilink")
  - 164号：[IS〈Infinite
    Stratos〉2](../Page/IS〈Infinite_Stratos〉.md "wikilink")（夏洛特·迪努亚&）画：[近藤奈都子](../Page/近藤奈都子.md "wikilink")
  - 165号：IS〈Infinite Stratos〉2（夏洛特·迪努亚&）画：近藤奈都子
  - 166号：偶像大师剧场版（星井美希&天海春香）画：[高濑智章](../Page/高濑智章.md "wikilink")
  - 167号：[Wake Up,
    Girls\!](../Page/Wake_Up,_Girls!.md "wikilink")（岛田真梦）画：[小岛大和](../Page/小岛大和.md "wikilink")
  - 168号：[噬血狂袭](../Page/噬血狂袭.md "wikilink")（姬柊雪菜）画：[佐野恵一](../Page/佐野恵一.md "wikilink")
  - 169号：[偶像大师
    灰姑娘女孩](../Page/偶像大师_灰姑娘女孩.md "wikilink")（本田未央、島村卯月、渋谷凜）画：[田中裕介](../Page/田中裕介.md "wikilink")
  - 170号：[请问您今天要来点兔子吗？](../Page/请问您今天要来点兔子吗？.md "wikilink")（保登心爱、香风智乃、天天座理世、提比）画：[奥田阳介](../Page/奥田阳介.md "wikilink")
  - 171号：少女&坦克（安丘比）画：[古市裕一](../Page/古市裕一.md "wikilink")
  - 172号：[LoveLive\!](../Page/LoveLive!.md "wikilink")（矢泽妮可）画：村山公辅、小野旭
  - 173号：[DOG
    DAYS''](../Page/DOG_DAYS.md "wikilink")&[魔法少女奈叶ViVid](../Page/魔法少女奈叶ViVid.md "wikilink")（米露茜欧莱·F·比斯科帝&高町薇薇欧）画：[藤真拓哉](../Page/藤真拓哉.md "wikilink")
  - 174号：[临时女友](../Page/临时女友.md "wikilink")（克洛艾·鲁梅尔&椎名心実）画：[堤谷典子](../Page/堤谷典子.md "wikilink")
  - 175号：[甘城光辉游乐园](../Page/甘城光辉游乐园.md "wikilink")（千斗五十铃）画：[丸子达就](../Page/丸子达就.md "wikilink")
  - 176号：[日常生活中的异能战斗](../Page/日常生活中的异能战斗.md "wikilink")（姬木千冬、神崎灯代）画：[山口智](../Page/山口智.md "wikilink")
  - 177号：刀剑神域II（结城明日奈）画：[铃木勘太](../Page/铃木勘太.md "wikilink")
  - 178号：[请问您今天要来点兔子吗？](../Page/请问您今天要来点兔子吗？.md "wikilink")（香风智乃&桐间纱路）画：奥田阳介
  - 179号：[偶像大师 灰姑娘女孩](../Page/偶像大师_灰姑娘女孩.md "wikilink")（島村卯月&渋谷
    凛）画：[赤井俊文](../Page/赤井俊文.md "wikilink")
  - 180号：[恶魔高校D×D BoRN](../Page/恶魔高校D×D.md "wikilink")（莉雅丝·吉蒙里）画：ごとうじゅんじ
  - 181号：[魔法少女奈叶ViVid](../Page/魔法少女奈叶ViVid.md "wikilink")（高町薇薇欧、高町奈叶）画：[清水祐実](../Page/清水祐実.md "wikilink")
  - 182号：[可塑性記憶](../Page/可塑性記憶.md "wikilink")（艾拉）画：[山崎淳](../Page/山崎淳.md "wikilink")
  - 183号：[恶魔高校D×D BorN](../Page/恶魔高校D×D.md "wikilink")（莉雅丝·吉蒙里）画：ごとうじゅんじ
  - 184号：悠哉日常大王（一条萤、越谷小鞠、越谷夏海、宮內蓮華）画：[古川英树](../Page/古川英树.md "wikilink")
  - 185号：[Charlotte](../Page/Charlotte_\(动画\).md "wikilink")（友利奈绪）画：[夏住爱子](../Page/夏住爱子.md "wikilink")
  - 186号：[请问您今天要来点兔子吗？](../Page/请问您今天要来点兔子吗？.md "wikilink")（香风智乃&桐间纱路）画：[佐佐木贵宏](../Page/佐佐木贵宏.md "wikilink")
  - 187号：[女武神驱动](../Page/女武神驱动.md "wikilink")（敷岛魅零&处女守）画：[金子拓](../Page/金子拓.md "wikilink")
  - 188号：少女&坦克（島田愛里寿、西住美穗）画：[小仓典子](../Page/小仓典子.md "wikilink")
  - 189号：[请问您今天要来点兔子吗？](../Page/请问您今天要来点兔子吗？.md "wikilink")（香风智乃）画：[只野和子](../Page/只野和子.md "wikilink")
  - 190号：[无彩限的幻影世界](../Page/无彩限的幻影世界.md "wikilink")（川神
    舞）画：[池田和美](../Page/池田和美.md "wikilink")
  - 191号：[少女们向荒野进发](../Page/少女们向荒野进发.md "wikilink")（黑田砂雪）画：[野口孝行](../Page/野口孝行.md "wikilink")
  - 192号：[为美好的世界献上祝福！](../Page/为美好的世界献上祝福！.md "wikilink")（阿克娅、达斯堤尼斯·福特·拉拉蒂娜、惠惠）画：[北村友幸](../Page/北村友幸.md "wikilink")
  - 193号：[高校舰队](../Page/高校舰队.md "wikilink")（ヴィルヘルミーナ・ブラウンシュヴァイク・
    インゲノール・フリーデブルク）画：[米泽优](../Page/米泽优.md "wikilink")
  - 194号：请问您今天要来点兔子吗？（香風 智乃&保登 心爱）画：[阪野日香莉](../Page/阪野日香莉.md "wikilink")
  - 195号：高校舰队（岬 明乃&宗谷 ましろ）画：[奥田阳介](../Page/奥田阳介.md "wikilink")
  - 196号：[NEW GAME\!](../Page/NEW_GAME!.md "wikilink")（涼風
    青葉）画：[菊池爱](../Page/菊池爱.md "wikilink")
  - 197号：[Re:從零開始的異世界生活](../Page/Re:從零開始的異世界生活.md "wikilink")（雷姆、艾米莉亚）画：[大塚真一郎](../Page/大塚真一郎.md "wikilink")
  - 198号：NEW GAME\!（涼風 青葉&滝本 ひふみ）画：[菊池爱](../Page/菊池爱.md "wikilink")
  - 199号：请问您今天要来点兔子吗？（香風 智乃&保登 心爱 天天座
    理世）画：[迪谷阳介](../Page/迪谷阳介.md "wikilink")
  - 200号：[ViVid Strike\!](../Page/ViVid_Strike!.md "wikilink")（）画：
  - 201号：[少女编号](../Page/少女编号.md "wikilink")（烏丸
    千歳）画：[木野下澄江](../Page/木野下澄江.md "wikilink")
  - 202号：[BanG
    Dream\!](../Page/BanG_Dream!.md "wikilink")（戸山香澄）画：[仁多マツコ](../Page/仁多マツコ.md "wikilink")
  - 203号：剧场版 刀剑神域（结城明日奈）画：[足立慎吾](../Page/足立慎吾.md "wikilink")
  - 204号：[路人女主的养成方法](../Page/路人女主的养成方法.md "wikilink")♭（加藤惠）画：[高瀬智章](../Page/高瀬智章.md "wikilink")
  - 205号：路人女主的养成方法♭（加藤惠、泽村·斯潘塞·英梨梨、霞之丘诗羽）画：[奥田佳子](../Page/奥田佳子.md "wikilink")
  - 206号：[Just
    Because\!](../Page/Just_Because!.md "wikilink")（夏目美绪）画：[比村奇石](../Page/比村奇石.md "wikilink")
  - 207号：[情色漫画老师](../Page/情色漫画老师.md "wikilink")（和泉纱雾）画：[织田広之](../Page/织田広之.md "wikilink")
  - 208号：[魔法少女奈叶Reflection](../Page/魔法少女奈叶Reflection.md "wikilink")（高町奈叶、菲特·T·哈洛温、阿米蒂埃·弗洛利安、琦莉耶·弗洛利安）画：[桥立佳奈](../Page/桥立佳奈.md "wikilink")
  - 209号：战姬绝唱SYMPHOGEAR
    AXZ（立花响、风鸣翼、雪音克莉丝）画：[原田大基](../Page/原田大基.md "wikilink")
  - 210号：[天使的3P！](../Page/天使的3P！.md "wikilink")（五岛润、红叶谷希美、金城空）画：[野口孝行](../Page/野口孝行.md "wikilink")
  - 211号：请问您今天要来点兔子吗？？ ～Dear My
    Sister～（保登心爱、保登萌香、香风智乃、提比）画：[小川茜](../Page/小川茜.md "wikilink")
  - 212号：Just Because\!（小宫惠那）画：比村奇石
  - 213号：少女&坦克 最终章（西住美穗）画：杉本功
  - 214号：[龙王的工作！](../Page/龙王的工作！.md "wikilink")（夜叉神
    天衣、雏鹤爱）画：[矢野茜](../Page/矢野茜.md "wikilink")
  - 215号：[原书·原书使](../Page/原书·原书使.md "wikilink")（键村叶月）画：[森川侑纪](../Page/森川侑纪.md "wikilink")
  - 216号：[摇曳露营△](../Page/摇曳露营△.md "wikilink")（志摩凛、各务原抚子）画：佐佐木睦美
  - 217号：请问您今天要来点兔子吗？？ ～Dear My Sister～（保登心爱、香风智乃）画：小川茜
  - 218号：[Comic
    Girls](../Page/Comic_Girls.md "wikilink")（萌田薰子、恋冢小梦、色川琉姬、胜木翼）画：[野田康行](../Page/野田康行.md "wikilink")
  - 219号：[恶魔高校D×D
    HERO](../Page/恶魔高校D×D.md "wikilink")（莉雅丝·吉蒙里）画：[鲤川慎平](../Page/鲤川慎平.md "wikilink")
  - 220号：[摇曳庄的幽奈小姐](../Page/摇曳庄的幽奈小姐.md "wikilink")（汤之花幽奈、仲居千岁）画：[竹谷今日子](../Page/竹谷今日子.md "wikilink")
  - 221号：[少女☆歌剧Revue
    Starlight](../Page/少女☆歌剧Revue_Starlight.md "wikilink")（爱城华恋）画：[野口孝之](../Page/野口孝之.md "wikilink")
  - 222号：[关于我转生变成史莱姆这档事](../Page/关于我转生变成史莱姆这档事.md "wikilink")（莉姆露・坦派斯特）画：矢野茜
  - 223号：[魔法少女奈葉Detonation](../Page/魔法少女奈葉Detonation.md "wikilink")（高町奈叶、菲特·T·哈洛温）画：桥立佳奈
  - 224号：[SSSS.GRIDMAN](../Page/SSSS.GRIDMAN.md "wikilink")（宝多六花、新条茜）画：[中村真由美](../Page/中村真由美.md "wikilink")
  - 225号：噬血狂袭Ⅲ（姬柊雪菜）画：古川英树
  - 226号：[佐贺偶像是传奇](../Page/佐贺偶像是传奇.md "wikilink")（）画：
  - 227号：刀剑神域 Alicization（结城明日奈）画：[横田拓己](../Page/横田拓己.md "wikilink")
  - 228号：[五等分的花嫁](../Page/五等分的花嫁.md "wikilink")（）画：[](../Page/.md "wikilink")

## 增刊號

### Megami MAGAZINE Special

2000年11月，在Vol.8和Vol.9的中間發售。此後由隔月刊改為月刊。另外，隔月刊時刊載了許多[美少女遊戲的情報](../Page/美少女遊戲.md "wikilink")，Special刊行後則固定以[動畫中心的介紹](../Page/動畫.md "wikilink")。

  - Special: Di Gi Charat（數-{}-碼子、布子）

### Megami MAGAZINE DELUXE

將刊載於本誌的海報與姊妹誌《Animedia》的版權畫濃縮收錄的總集篇。

  - DELUXE Vol.1：妹妹公主RePure（可憐）2002年12月發售。
  - DELUXE Vol.2：拜託雙胞胎（宮藤深衣奈、小野寺樺恋）2003年12月發售。
  - DELUXE Vol.3: D.C. ～戀愛學園～（朝倉音夢）2004年8月發售。
  - DELUXE Vol.4: AIR（TV版）（神尾觀鈴、霧島佳乃、遠野美凪）2005年3月發售。
  - DELUXE Vol.5:
    [舞-HiME](../Page/舞-HiME.md "wikilink")（鴇羽舞衣、玖我夏樹）2005年8月發售。
  - DELUXE Vol.6：魔法少女奈葉A's（高町奈葉、菲特·泰斯塔羅沙、八神疾風）2006年3月25日發售。
  - DELUXE Vol.7：涼宮春日的憂鬱（涼宮春日、朝比奈實玖留、長門有希）2006年8月發售。
  - DELUXE Vol.8：[SHUFFLE\!
    MEMORIES](../Page/SHUFFLE!.md "wikilink")（時雨亞沙）2007年3月發售。
  - DELUXE Vol.9：魔法少女奈葉StrikerS（高町奈葉、菲特·泰斯塔羅沙、八神疾風）2007年8月發售。
  - DELUXE Vol.10：魔法少女奈葉StrikerS（高町奈葉、菲特·泰斯塔羅沙、八神疾風）2008年3月發售。
  - DELUXE Vol.11:
    [CLANNAD](../Page/CLANNAD.md "wikilink")（古河渚、伊吹风子、坂上智代）2008年8月發售。
  - DELUXE Vol.12: CLANNAD ～AFTER STORY～（古河渚）2009年3月25日發售。
  - DELUXE Vol.13:
    [K-ON！轻音部](../Page/K-ON！轻音部.md "wikilink")（平沢唯、秋山澪、中野梓）2009年8月發售。
  - DELUXE Vol.14: [魔法少女奈叶 The MOVIE
    1st](../Page/魔法少女奈叶_The_MOVIE_1st.md "wikilink")（高町奈叶&菲特·泰斯塔罗莎）2010年3月發售。
  - DELUXE Vol.15: K-ON！轻音部（平沢唯、中野梓）2010年8月30日發售。
  - DELUXE Vol.16: 高町奈叶&菲特·泰斯塔罗莎（[魔法少女奈叶 The MOVIE 2nd
    A's](../Page/魔法少女奈叶_The_MOVIE_2nd_A's.md "wikilink")）2011年3月發售。
  - DELUXE Vol.17: 剧场版 K-ON！轻音部（平沢唯、秋山澪、中野梓、田井中律、琴吹䌷）2011年9月30日發售。
  - DELUXE Vol.18: K-ON！轻音部（平沢唯、中野梓）2012年3月22日發售。
  - DELUXE Vol.19: 高町奈叶&菲特·泰斯塔罗莎&八神疾风（魔法少女奈叶 The MOVIE 2nd
    A's）2012年9月27日發售。
  - DELUXE Vol.20:
    西住美穗&西住真穗（[少女&坦克](../Page/少女&坦克.md "wikilink")）2013年3月28日發售。
  - DELUXE Vol.21: 西住美穗&秋山优花里（少女&坦克）2013年9月27日發售。
  - DELUXE Vol.22: 塞西莉亚·奥尔科特&夏洛特·迪诺亚&拉芙拉·博迪维希&更识簪（[IS〈Infinite
    Stratos〉](../Page/IS〈Infinite_Stratos〉.md "wikilink")）2014年3月18日發售。
  - DELUXE Vol.23: 白&初濑伊纲（[NO GAME NO
    LIFE](../Page/NO_GAME_NO_LIFE.md "wikilink")）2014年9月23日發售。
  - DELUXE Vol.24: 莉雅丝·吉蒙里&姬岛朱乃（恶魔高校D×D BORN）2015年3月19日發售。
  - DELUXE Vol.25:
    黃前久美子&高坂麗奈（[吹響吧！上低音號～歡迎加入北宇治高中管樂團～](../Page/吹響吧！上低音號～歡迎加入北宇治高中管樂團～.md "wikilink")）2015年9月29日發售。
  - DELUXE Vol.26:
    香风智乃&天天座理世&保登心爱（[请问您今天要来点兔子吗？](../Page/请问您今天要来点兔子吗？.md "wikilink")）2016年3月31日發售。
  - DELUXE Vol.27: 凉风青叶&樱宁宁（[NEW
    GAME\!](../Page/NEW_GAME!.md "wikilink")）2016年9月30日發售。
  - DELUXE Vol.28:
    姬柊雪菜（[噬血狂袭](../Page/噬血狂袭.md "wikilink")Ⅱ）2017年3月30日發售。
  - DELUXE Vol.29: 和泉纱雾、山田妖精、千寿村征（埃罗芒阿老师）2017年9月29日发售。
  - DELUXE Vol.30: 保登心爱、香风智乃（请问您今天要来点兔子吗？？ ～Dear My
    Sister～）2018年3月30日发售。
  - DELUXE Vol.31: 姬柊雪菜（噬血狂袭Ⅲ）2018年9月29日发售。
  - DELUXE Vol.32:
    樱岛麻衣（[青春猪头少年不会梦到兔女郎学姐](../Page/青春猪头少年不会梦到兔女郎学姐.md "wikilink")）2019年3月30日发售。
  - Megami MAGAZINE SUPER DELUXE:
    魔法少女奈葉StrikerS（高町奈葉、菲特·泰斯塔羅沙）2009年4月發售。

### Megami MAGAZINE Creators

以[插畫畫家的原創作品或收錄本誌](../Page/插畫畫家.md "wikilink")「Girl's Avenue」的訪談所構成。

  - Creators Vol.1: [駒都英二](../Page/駒都英二.md "wikilink") Original
    2004年3月發售。
  - Creators Vol.2: [べっかんこう](../Page/べっかんこう.md "wikilink") Original
    2004年10月發售。
  - Creators Vol.3: [SHAA](../Page/SHAA.md "wikilink") Original
    2005年3月15日發售。
  - Creators Vol.4: [☆畫野朗](../Page/☆畫野朗.md "wikilink") Original
    2005年9月29日發售。
  - Creators Vol.5: [後藤潤二](../Page/後藤潤二.md "wikilink") Original
    2006年3月2日發售。
  - Creators Vol.6：駒都英二 Original 2006年8月9日發售。
  - Creators Vol.7：べっかんこう Original 2007年2月27日發售。
  - Creators Vol.8：駒都英二 Original 2007年6月29日發售。
  - Creators Vol.9：[伊東雜音](../Page/伊東雜音.md "wikilink") ALICE Parade
    2007年9月29日發售。
  - Creators Vol.10：七尾奈留 Original 2007年12月18日發售
  - Creators Vol.11：駒都英二 Original 2008年3月22日發售
  - Creators Vol.12：泳装美少女特集 Original 2008年6月24日發售
  - Creators Vol.13：駒都英二 Original 2008年9月26日發售
  - Creators Vol.14：[西又葵大特集](../Page/西又葵.md "wikilink") Original
    2008年12月17日發售
  - Creators Vol.15：駒都英二 Original 2009年3月25日發售
  - Creators Vol.16：駒都英二 Original 2009年6月26日发售
  - Creators Vol.17：[森沢晴行](../Page/森沢晴行.md "wikilink")、駒都英二 Original
    2009年9月17日發售
  - Creators Vol.18：温泉少女大特集 Original 2009年12月19日發售
  - Creators Vol.19：駒都英二 Original 2010年3月27日发售
  - Creators Vol.20：水着少女特集 Original 2010年6月29日发售
  - Creators Vol.23：偶像大师新作纪念特大号 Original 2011年6月23日发售

## 關連項目

  - [動畫雜誌](../Page/動畫雜誌.md "wikilink")
  - [Animedia](../Page/Animedia.md "wikilink")
  - [聲優Animedia](../Page/聲優Animedia.md "wikilink")（同社的聲優雜誌，版面編排以Megami
    MAGAZINE為底）
  - [Megami文庫](../Page/Megami文庫.md "wikilink")
  - [萌](../Page/萌.md "wikilink")
  - [少女愛](../Page/少女愛.md "wikilink")

<!-- end list -->

  - 從本誌連載轉為展開複合媒體的作品

<!-- end list -->

  - [天使的尾巴](../Page/天使的尾巴.md "wikilink")（）
  - [カンブリアンQTS](../Page/カンブリアンQTS.md "wikilink")
  - [みっくすJUICE](../Page/みっくすJUICE.md "wikilink")（→[妄想科学シリーズ
    ワンダバスタイル](../Page/妄想科学シリーズ_ワンダバスタイル.md "wikilink")）
  - [流星戰隊Musumet](../Page/流星戰隊Musumet.md "wikilink")

<!-- end list -->

  - 其他連載

<!-- end list -->

  - [水瓶戰記](../Page/水瓶戰記.md "wikilink")（）
  - [備長炭](../Page/備長炭_\(動畫\).md "wikilink")（当初は本誌で漫畫連載されていたが、[COMIC
    BLADEへ移籍](../Page/COMIC_BLADE.md "wikilink")。読者コーナーは2006年9月號まで続いた）
  - [發明工坊](../Page/發明工坊.md "wikilink")（2004年2月號より「エンサイクロペディア・オブ・トリスティア」を長期連載中）
  - [GEMINI×KNIVES](../Page/GEMINI×KNIVES.md "wikilink")

## 参考資料

## 外部連結

  - [Megami MAGAZINE
    官方網站](https://web.archive.org/web/20130304220217/http://www.e-animedia.net/archives/cat_212458.html)

  - [DMP Books](http://www.dmpbooks.com/)（北米版經銷商）

[Category:學習研究社](../Category/學習研究社.md "wikilink")
[Category:動畫雜誌](../Category/動畫雜誌.md "wikilink")
[Animedia](../Category/Animedia.md "wikilink")
[Megami_MAGAZINE](../Category/Megami_MAGAZINE.md "wikilink")
[Category:1999年創辦的雜誌](../Category/1999年創辦的雜誌.md "wikilink")
[Category:1999年日本建立](../Category/1999年日本建立.md "wikilink")