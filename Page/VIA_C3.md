**VIA
C3**是一款[x86](../Page/x86.md "wikilink")[处理器](../Page/中央处理器.md "wikilink")，早期为继承从Cyrix收购来的智慧产权，而被命名为「**VIA
Cyrix III**」，但它实际上是由从[Integrated Device
Technology](../Page/Integrated_Device_Technology.md "wikilink")（IDT）中收购来的Centaur
Technology部門的開發人員以[WinChip處理器為基礎而開發](../Page/WinChip.md "wikilink")，由[台湾的](../Page/台湾.md "wikilink")[威盛電子](../Page/威盛電子.md "wikilink")（VIA）制造，与基于**Joshua核心**的**Cyrix
III**无关。后被更名为VIA C3，是VIA“中国芯”C系列的经典版本。

[VIA_C3_C5XL_CPGA.jpg](https://zh.wikipedia.org/wiki/File:VIA_C3_C5XL_CPGA.jpg "fig:VIA_C3_C5XL_CPGA.jpg")

## 功能

### Samuel核心\[1\]

  - L1
    [Cache指令](../Page/Cache.md "wikilink")、-{zh-tw:資料;zh-cn:数据}-各有64KB
  - 16级[指令管線](../Page/指令管線.md "wikilink")
  - 采用100或133 MHz [FSB](../Page/FSB.md "wikilink")
  - [0.18微米製程](../Page/0.18微米製程.md "wikilink")
  - \-{zh-tw:支援;zh-cn:支持}-[MMX和](../Page/MMX.md "wikilink")[3DNow\!指令集](../Page/3DNow!.md "wikilink")
  - 半倍频的[浮点运算单元](../Page/FPU.md "wikilink")
  - 可与部分Intel Pentium II/III主板兼容

### Samuel II核心\[2\]

  - L1
    [Cache指令](../Page/Cache.md "wikilink")、-{zh-tw:資料;zh-cn:数据}-各有64KB
  - 淘汰型(victim)L2 [Cache](../Page/Cache.md "wikilink") 64KB
  - 12级[指令管線](../Page/指令管線.md "wikilink")
  - 采用100或133 MHz [FSB](../Page/FSB.md "wikilink")
  - 0.15[微米制程](../Page/微米.md "wikilink")
  - \-{zh-tw:支援;zh-cn:支持}-[MMX和](../Page/MMX.md "wikilink")[3DNow\!指令集](../Page/3DNow!.md "wikilink")
  - 半倍频的[浮点运算单元](../Page/FPU.md "wikilink")
  - 可与部分Intel Pentium II/III主板兼容

### Ezra核心\[3\]

  - L1
    [Cache指令](../Page/Cache.md "wikilink")、-{zh-tw:資料;zh-cn:数据}-各有64KB
  - 淘汰型(victim)L2 [Cache](../Page/Cache.md "wikilink") 64KB
  - 12级[指令管線](../Page/指令管線.md "wikilink")
  - 采用100或133 MHz [FSB](../Page/FSB.md "wikilink")
  - [0.13微米製程](../Page/0.13微米製程.md "wikilink")
  - \-{zh-tw:支援;zh-cn:支持}-[MMX和](../Page/MMX.md "wikilink")[3DNow\!指令集](../Page/3DNow!.md "wikilink")
  - 半倍频的[浮点运算单元](../Page/FPU.md "wikilink")

### Nehemiah核心\[4\]

  - L1
    [Cache指令](../Page/Cache.md "wikilink")、-{zh-tw:資料;zh-cn:数据}-各有64KB
  - 淘汰型(victim)L2 [Cache](../Page/Cache.md "wikilink") 64KB
  - 12级[指令管線](../Page/指令管線.md "wikilink")
  - 采用133或200 MHz [FSB](../Page/FSB.md "wikilink")
  - 0.13[微米制程](../Page/微米.md "wikilink")
  - \-{zh-tw:支援;zh-cn:支持}-[MMX和](../Page/MMX.md "wikilink")[SSE指令集](../Page/SSE.md "wikilink")
  - 和前代[Ezra-T架构相比](../Page/Ezra-T.md "wikilink")，[浮点数处理单元](../Page/浮点数.md "wikilink")（FPU）可以在核心频率下全速工作。虽然在性能方面比當時[Intel及](../Page/Intel.md "wikilink")[AMD的处理器低](../Page/AMD.md "wikilink")，但该款处理器功耗低，放出的热力也比较少，较适合在[嵌入式系统](../Page/嵌入式系统.md "wikilink")、[工業電腦中使用](../Page/工業電腦.md "wikilink")。

## 另見

  - [VIA Eden處理器列表](../Page/VIA_Eden處理器列表.md "wikilink")
  - [VIA Nano處理器列表](../Page/VIA_Nano處理器列表.md "wikilink")

## 參考

  - [威盛電子](../Page/威盛電子.md "wikilink")
  - [S3 Graphics](../Page/S3_Graphics.md "wikilink")
  - [VIA envy](../Page/VIA_envy.md "wikilink")
  - [VIA C3-M](../Page/VIA_C3-M.md "wikilink")
  - [VIA CoreFusion](../Page/VIA_CoreFusion.md "wikilink")
  - [VIA Eden](../Page/VIA_Eden.md "wikilink")
  - [VIA C7-D](../Page/VIA_C7-D.md "wikilink")
  - [VIA C7-M](../Page/VIA_C7-M.md "wikilink")
  - [VIA PV530](../Page/VIA_PV530.md "wikilink")
  - [VIA Nano](../Page/VIA_Nano.md "wikilink")

## 外部連結

  - [威盛電子官方網站](http://www.viatech.com.tw/)
  - [雙核心CPU + 32 Cores IGP
    VIA推出Nano雙核心處理器平台](https://web.archive.org/web/20101204012240/http://global.hkepc.com/5342)
  - [VIA展示Dual Core Nano處理器 最高達2GHz時脈 功耗約20W
    TDP](http://www.hkepc.com/5026)
  - [預期2010年第一季度投入量產 VIA Nano 3000系列處理器](http://www.hkepc.com/4215)
  - [針對Windows 7嵌入式裝置市場 VIA全系列x86處理器全面支援](http://www.hkepc.com/4088)
  - [VIA Nano處理器進軍伺服器領域 Dell推出XS11-VX8超輕薄伺服器](http://www.hkepc.com/2975)
  - [2009年VIA處理器最新佈局 Nano 3000、Dual
    Core計劃曝光](http://www.hkepc.com/tag/via/page/3)
  - [全新VIA Isaiah微架構 VIA Nano L2100處理器上市](http://www.hkepc.com/1797)
  - [VIA全新Nano處理器正式發佈 效能比上代高4倍 陳文琦︰VIA未來希望](http://www.hkepc.com/1227)
  - [VIA's C3 Hits 1
    GHz](http://www.tomshardware.com/reviews/VIA-s-C3-Hits-1-GHz,472.html)
  - [Atom, Athlon, or Nano? Energy-Savers
    Compared](http://www.tomshardware.com/reviews/Athlon-Atom-Nano-power,2036.html)

[Category:X86架構](../Category/X86架構.md "wikilink")
[Category:威盛处理器](../Category/威盛处理器.md "wikilink")

1.  <http://www.cpu-world.com/CPUs/C3/TYPE-C3%20Samuel.html>
2.  <http://www.cpu-world.com/CPUs/C3/TYPE-C3%20Samuel2.html>
3.  <http://www.cpu-world.com/CPUs/C3/TYPE-C3%20Ezra.html>
4.  <http://www.cpu-world.com/CPUs/C3/TYPE-C3%20Nehemiah.html>