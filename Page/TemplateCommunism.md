[马列毛主义](马列毛主义.md "wikilink"){{·}}[霍查主义](霍查主义.md "wikilink")）
[卢森堡主义](卢森堡主义.md "wikilink"){{·}}[托洛茨基主义](托洛茨基主义.md "wikilink")
[铁托主义](铁托主义.md "wikilink"){{·}}[赫魯曉夫主義](赫魯曉夫主義.md "wikilink")
[卡斯特罗主义](卡斯特罗主义.md "wikilink"){{·}}[格瓦拉主義](格瓦拉主義.md "wikilink")
[卡达尔主义](古拉什共产主义.md "wikilink"){{·}}[胡萨克主义](胡萨克主义.md "wikilink")
[胡志明思想](胡志明思想.md "wikilink"){{·}}[中国特色社会主义](中国特色社会主义.md "wikilink")
[左翼共产主义](左翼共产主义.md "wikilink"){{·}}[無政府共產主義](無政府共產主義.md "wikilink")
[宗教共產主義](宗教共產主義.md "wikilink")（[基督教共產主義](基督教共產主義.md "wikilink")）
[欧洲共产主义](欧洲共产主义.md "wikilink"){{·}}[世界共產主義](世界共產主義.md "wikilink"){{·}}[民族共产主义](民族共产主义.md "wikilink"){{·}}[無国籍共產主義](無国籍共產主義.md "wikilink")

|list4name = 國際組織 |list4title = 國際組織 |list4 =
[共產主義者同盟](共產主義者同盟.md "wikilink")
[第一国际](第一国际.md "wikilink")
[第二国际](第二国际.md "wikilink")
[第三国际](第三国际.md "wikilink")
[第四国际](第四国际.md "wikilink")
[共产党和工人党国际会议](共产党和工人党国际会议.md "wikilink")
***[左翼国际政党组织列表](左翼国际政党组织列表.md "wikilink")***

|list5name = 人物 |list5title = 人物 |list5 = [马克思](卡尔·马克思.md "wikilink")
[恩格斯](弗里德里希·恩格斯.md "wikilink")
[羅莎·盧森堡](羅莎·盧森堡.md "wikilink")
[卡尔·李卜克内西](卡尔·李卜克内西.md "wikilink")
[列寧](弗拉基米尔·伊里奇·列宁.md "wikilink")
[托洛茨基](列夫·达维多维奇·托洛茨基.md "wikilink")
[斯大林](约瑟夫·维萨里奥诺维奇·斯大林.md "wikilink")
[葛兰西](安东尼奥·葛兰西.md "wikilink")
[毛泽东](毛泽东.md "wikilink")
[胡志明](胡志明.md "wikilink")
[金日成](金日成.md "wikilink")
[铁托](约瑟普·布罗兹·铁托.md "wikilink")
[霍查](恩維爾·霍查.md "wikilink")
[菲德尔·卡斯特罗](菲德尔·卡斯特罗.md "wikilink")
[-{zh-hans:切·格瓦拉;zh-hk:哲·古華拉;zh-tw:切·格瓦拉;}-](切·格瓦拉.md "wikilink")

|list7name = 相關条目 |list7title = 相關条目 |list7 =
[反資本主義](反資本主義.md "wikilink")
[反法西斯主義](反法西斯主義.md "wikilink")
[左翼政治](左派.md "wikilink")
[新阶级](新阶级.md "wikilink"){{·}} [新左翼](新左翼.md "wikilink")
[原始共產主義](原始共產主義.md "wikilink")
[科学社会主义](科学社会主义.md "wikilink")
[社会主义](社会主义.md "wikilink")
[社会主义政治经济学](社会主义政治经济学.md "wikilink")
[後共產主義](後共產主義.md "wikilink")
[反共主義](反共主義.md "wikilink")
[去共产主义化](去共化.md "wikilink")
[反共大屠杀](反共大屠杀.md "wikilink")
[對共產主義的批評](對共產主義的批評.md "wikilink")
[对共产主义国家的批评](对共产主义国家的批评.md "wikilink")

|below = ![Symbol-hammer-and-sickle.svg](Symbol-hammer-and-sickle.svg
"Symbol-hammer-and-sickle.svg") [共产主义主题](Portal:共产主义.md "wikilink")
}}<noinclude>

## 參見

  - [Template:社会主义](Template:Socialism_sidebar.md "wikilink") -
    [社会主义模板](社会主义.md "wikilink")
  - [Template:马克思主义](Template:马克思主义.md "wikilink") -
    [马克思主义模板](马克思主义.md "wikilink")
  - [Template:马克思列宁主义](Template:马克思列宁主义.md "wikilink") -
    [马克思列宁主义模板](马克思列宁主义.md "wikilink")
  - [Template:毛泽东思想](Template:毛泽东思想.md "wikilink") -
    [毛泽东思想模板](毛泽东思想.md "wikilink")
  - [Template:共产党](Template:Communist_parties.md "wikilink") -
    [共产党模板](共产党.md "wikilink")

</noinclude>

[Category:共产主义模板](../Category/共产主义模板.md "wikilink")