**马尔康·斯科特·卡彭特**（，\[1\]）曾是一位[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")，执行过[水星-宇宙神7号任务](../Page/水星-宇宙神7号.md "wikilink")。

## 生平

卡彭特生于[科罗拉多州的博尔德](../Page/科罗拉多州.md "wikilink")，母亲是Marion Scott
Carpenter，父亲 Florence \[née Noxon\] Carpenter
在[哥伦比亚大学做](../Page/哥伦比亚大学.md "wikilink")[博士后研究](../Page/博士后.md "wikilink")。高中毕业后进入海军学院训练中心成为航空学员。二战后于1945年11月进入[科羅拉多大學博爾德分校学习](../Page/科羅拉多大學博爾德分校.md "wikilink")[航空航天工程](../Page/航空航天工程.md "wikilink")。[朝鲜战争期间](../Page/朝鲜战争.md "wikilink")，他进入[美国海军服役](../Page/美国海军.md "wikilink")，1958年成为[大黃蜂號航空母艦_(CV-12)一名空中情报指挥官](../Page/大黃蜂號航空母艦_\(CV-12\).md "wikilink")。1959年入选为NASA[水星计划的成员](../Page/水星计划.md "wikilink")。

他结过四次婚，离过三次婚。1948年与Rene Louise Price结婚，生育四个子女——Marc Scott, Kristen
Elaine, Candace Noxon, 和 Robyn Jay；1972与电影制片人[Hal
Roach的女儿Maria](../Page/Hal_Roach.md "wikilink")
Roach结婚，有两名子女——Matthew Scott 与电影制作人[Nicholas
Andre](../Page/Nicholas_Carpenter.md "wikilink")；1988年又与Barbara
Curtin结婚，有一个子女Zachary Scott\[2\]\[3\]，七年后再次离婚。1999年与Patty
Barrett结婚\[4\]。

生前曾因[中风进入医院接受治疗](../Page/中风.md "wikilink")，并在医院病逝于2013年10月10日，享年88岁\[5\]\[6\]\[7\]。

## 所著书籍

  - 'We Seven: By the Astronauts Themselves,'' ISBN 978-1439181034, by
    M. Scott Carpenter (Author) , Gordon L. Cooper (Author) , John H.
    Glenn (Author) , Virgil I. Grissom (Author) , Walter M. Schirra
    (Author) , Alan B. Shepard (Author) , Donald K. Slayton (Author),
  - *For Spacious Skies: The Uncommon Journey of a Mercury Astronaut,*
    ISBN 0-15-100467-6 or the revised paperback edition ISBN
    0-451-21105-7, Carpenter's biography, co-written with his daughter
    Kris Stoever; describes his childhood, his experiences as a naval
    aviator, a Mercury astronaut, including an account of what went
    wrong, and right, on the flight of *Aurora 7.*
  - *[Into That Silent Sea](../Page/Into_That_Silent_Sea.md "wikilink"):
    Trailblazers of the Space Era, 1961–1965*, by [Francis
    French](../Page/Francis_French.md "wikilink") and [Colin
    Burgess](../Page/Colin_Burgess_\(author\).md "wikilink"), 2007. A
    Carpenter-approved account of his life and space flight.
  - *The Steel Albatross,* ISBN 978-0831776084. Science Fiction. A
    'technothriller' set around the life of a fighter pilot in the US
    Navy's Top Gun school.
  - *Deep Flight,* ISBN 978-0671759032. Science Fiction. This follow-on
    to The Steel Albatross is an underwater outburst of war and action.

## 參考資料

## 外部链接

  - [美国国家航空航天局网站的卡彭特介绍](http://www.jsc.nasa.gov/Bios/htmlbios/carpenter-ms.html)

  - [Spacefacts biography of Scott
    Carpenter](http://www.spacefacts.de/bios/astronauts/english/carpenter_scott.htm)

  - [Personal Web Site](http://www.scottcarpenter.com)

  - [NYTimes
    obituary](http://www.nytimes.com/2013/10/11/us/scott-carpenter-mercury-astronaut-who-orbited-earth-dies-at-88.html?hp)

[Category:美国宇航员](../Category/美国宇航员.md "wikilink")
[Category:水星计划7人](../Category/水星计划7人.md "wikilink")
[Category:水星计划](../Category/水星计划.md "wikilink")
[Category:美國海軍中校](../Category/美國海軍中校.md "wikilink")
[Category:美國海軍飛行員](../Category/美國海軍飛行員.md "wikilink")
[Category:科羅拉多大學博爾德分校校友](../Category/科羅拉多大學博爾德分校校友.md "wikilink")
[Category:科羅拉多州人](../Category/科羅拉多州人.md "wikilink")
[Category:美國韓戰軍事人物](../Category/美國韓戰軍事人物.md "wikilink")
[Category:美国自传作家](../Category/美国自传作家.md "wikilink")

1.
2.  [Tara Gray. Scott Carpenter biography; "40th Anniversary of the
    Mercury 7"](http://history.nasa.gov/40thmerc7/carpenter.htm)
    NASA.gov. Retrieved January 9, 2010.
3.  [Scott Carpenter biography at Encyclopedia
    Astronautica](http://www.astronautix.com/astros/carenter.htm)
4.
5.  <http://www.dailycamera.com/news/boulder/ci_24282964/scott-carpenter-dies-astronaut-boulder-aurora-7>
6.  <http://www.ctvnews.ca/sci-tech/american-astronaut-scott-carpenter-dies-at-88-1.1492605>
7.