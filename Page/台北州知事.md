**台北州知事**為[台灣日治時期](../Page/台灣日治時期.md "wikilink")1920年－1945年[台北州之首長](../Page/台北州.md "wikilink")。該行政區劃包括今[台北市](../Page/台北市.md "wikilink")、[新北市](../Page/新北市.md "wikilink")、[基隆市](../Page/基隆市.md "wikilink")、[宜蘭縣](../Page/宜蘭縣.md "wikilink")。

## 歷任

  - [相賀照鄉](../Page/相賀照鄉.md "wikilink")：1920年9月1日─1921年9月17日
  - [高田富藏](../Page/高田富藏.md "wikilink")：1921年9月17日─1924年12月23日
  - [吉岡荒造](../Page/吉岡荒造.md "wikilink")：1924年12月23日─1927年7月27日
  - [三浦碌郎](../Page/三浦碌郎.md "wikilink")：1927年7月27日─1928年7月21日
  - [高橋親吉](../Page/高橋親吉.md "wikilink")：1928年7月21日─1929年4月20日
  - [片山三郎](../Page/片山三郎.md "wikilink")：1929年4月20日─1931年5月8日
  - [宇賀四郎](../Page/宇賀四郎.md "wikilink")：1931年5月8日─1931年9月12日
  - [平山泰](../Page/平山泰.md "wikilink")：1931年9月12日─1932年3月15日
  - [中瀨拙夫](../Page/中瀨拙夫.md "wikilink")：1932年3月15日─1933年8月4日
  - [野口敏治](../Page/野口敏治.md "wikilink")：1933年8月4日─1936年2月26日
  - [今川淵](../Page/今川淵.md "wikilink")：1936年2月26日─1936年10月16日
  - [藤田傊治郎](../Page/藤田傊治郎.md "wikilink")：1936年10月16日─1939年1月28日
  - [戶水昇](../Page/戶水昇.md "wikilink")：1939年1月28日─1939年12月28日
  - [川村直岡](../Page/川村直岡.md "wikilink")：1939年12月28日─1941年5月14日
  - [三輪幸助](../Page/三輪幸助.md "wikilink")：1941年5月14日─1942年7月3日
  - [梁井淳二](../Page/梁井淳二.md "wikilink")：1942年7月3日─1943年11月13日
  - [坂口主稅](../Page/坂口主稅.md "wikilink")：1943年11月13日─1944年3月20日
  - [西村高兄](../Page/西村高兄.md "wikilink")：1944年3月20日─1945年5月23日
  - [高橋衛](../Page/高橋衛.md "wikilink")：1945年5月23日─1945年10月25日

## 參考文獻

  - [劉寧顏](../Page/劉寧顏.md "wikilink")
    編：《重修台灣省通志》，台北市：[台灣省文獻委員會](../Page/台灣省文獻委員會.md "wikilink")，1994年。
  - [台湾総督府](https://web.archive.org/web/20070311072823/http://homepage1.nifty.com/kitabatake/taiwan2.html)

{{-}}

[台北州知事](../Category/台北州知事.md "wikilink")
[Category:台灣日治時期官職](../Category/台灣日治時期官職.md "wikilink")