**塞萨尔·奥古斯托·加维里亚·特鲁希略**（[西班牙语](../Page/西班牙语.md "wikilink")：****，），
[哥伦比亚总统](../Page/哥伦比亚总统列表.md "wikilink")、[美洲国家组织秘书长](../Page/美洲国家组织秘书长.md "wikilink")。[哥伦比亚自由党唯一领袖](../Page/哥伦比亚自由党.md "wikilink")。

## 生平

加维里亚60年代就读于洛斯安德斯大学(Universidad de los
Andes)，1974年任佩雷拉市市长，并进入议会，曾任[比尔希略·巴尔科政府的财政部长和内政部长](../Page/比尔希略·巴尔科·巴尔加斯.md "wikilink")。1990年当选总统，1994年总统卸任后出任美洲国家组织秘书长。

[category:哥伦比亚总统](../Page/category:哥伦比亚总统.md "wikilink")

[Category:美洲国家组织秘书长](../Category/美洲国家组织秘书长.md "wikilink")