[Meleagris_gallopavo_MHNT.ZOO.2010.11.9.30.jpg](https://zh.wikipedia.org/wiki/File:Meleagris_gallopavo_MHNT.ZOO.2010.11.9.30.jpg "fig:Meleagris_gallopavo_MHNT.ZOO.2010.11.9.30.jpg")
**火雞**（），又名**七面鳥**或**吐绶鸡**，是一種原產於[北美洲的](../Page/北美洲.md "wikilink")[家禽](../Page/家禽.md "wikilink")。火雞體型比一般[雞大](../Page/雞.md "wikilink")，可達10公斤以上。根據傳統，[美國人會在](../Page/美國人.md "wikilink")[感恩節及](../Page/感恩節.md "wikilink")[聖誕節烹調火雞](../Page/聖誕節.md "wikilink")。

和其他雞形目鳥類相似，雌鳥較雄鳥小，顏色較不鮮艷。火雞翼展可達 1.5 - 1.8 公尺，是當地開放林地最大的鳥類，很難與其他種類搞混。

火雞有兩種，分別是：分佈於[北美的](../Page/北美.md "wikilink")[野生火雞](../Page/野生火雞.md "wikilink")，和分佈於[中美洲的](../Page/中美洲.md "wikilink")[眼斑火雞](../Page/眼斑火雞.md "wikilink")。現代的是由[墨西哥的](../Page/墨西哥.md "wikilink")[原住民馴化當地的野生火雞而得來](../Page/美洲原住民.md "wikilink")。

## 孤雌生殖

由於鳥類、[兩棲類及](../Page/兩棲類.md "wikilink")[魚類的性別由卵子所攜帶的性染色體決定](../Page/魚類.md "wikilink")，所以在某些特定情況下，有研究声称，可以進行[孤雌生殖](../Page/孤雌生殖.md "wikilink")，而火雞在這方面的能力比較著名。在缺乏雄性的情況下，雌鳥生產的未受精卵亦可孵化，而其所孵出的後代通常虛弱，並幾乎都是雄性\[1\]。但由于鸟类是高等的脊椎动物，生殖细胞特化，缺乏孤雌生殖的基础，上述文献中的情况有待确认和深入研究。

## 在大眾文化

在[感恩節裡](../Page/感恩節.md "wikilink")，[烤](../Page/烤.md "wikilink")[火雞肉是必備的節慶食物](../Page/火雞肉.md "wikilink")。而在[美國的](../Page/美國.md "wikilink")[白宮則有](../Page/白宮.md "wikilink")「[赦免火雞](../Page/赦免火雞.md "wikilink")」的儀式。

## 圖集

<center>

[File:Turkeys.jpg|馴化的火雞](File:Turkeys.jpg%7C馴化的火雞) <File:Turkeys> on
path.jpg|都會區中偶尔可以發現野生火雞 <File:Wild> turkey.jpg|野火雞
[File:Meissner-truthahn-m-fak-ffm001.jpg|火雞雕像](File:Meissner-truthahn-m-fak-ffm001.jpg%7C火雞雕像)

</center>

## 參見

  - [嘉義火雞肉飯](../Page/嘉義火雞肉飯.md "wikilink")
  - [感恩節大餐](../Page/感恩節大餐.md "wikilink")
  - [火雞肉](../Page/火雞肉.md "wikilink")
  - [沙威瑪](../Page/沙威瑪.md "wikilink")
  - [潛艇堡](../Page/潛艇堡.md "wikilink")

## 參考

## 外部連結

  - [台灣火雞料理的考究](http://www.chineseypage.com/rest/topic/turkey/turkey-cook.htm)

[Category:雉科](../Category/雉科.md "wikilink")
[Category:家禽](../Category/家禽.md "wikilink")
[Category:火鸡属](../Category/火鸡属.md "wikilink")
[Category:感恩節](../Category/感恩節.md "wikilink")

1.  <http://www.blackwell-synergy.com/doi/abs/10.1111/j.1474-919X.2007.00755.x>