**克里縣**（**Creek County,
Oklahoma**）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州東部的一個縣](../Page/奧克拉荷馬州.md "wikilink")。面積2,512平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口67,367人。縣治[薩帕爾帕](../Page/薩帕爾帕.md "wikilink")
(Sapulpa)。

成立於1907年11月16日。縣名來自[克里人](../Page/克里人.md "wikilink")。

[C](../Category/俄克拉何马州行政区划.md "wikilink")