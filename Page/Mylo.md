**Mylo**是[索尼公司于](../Page/索尼.md "wikilink")2006年8月发布的一款数码通讯终端，其形状酷似[PSP](../Page/PSP.md "wikilink")。**Mylo**是**My**
**L**ife
**O**nline的缩写。它拥有标准[Wi-Fi](../Page/Wi-Fi.md "wikilink")，可以用内置的[Skype与人通话](../Page/Skype.md "wikilink")，也可以发送短信。Mylo内置了[Opera浏览器](../Page/Opera_Devices.md "wikilink")，可以用藏在屏幕下方的键盘浏览网页，也可用[MSN
Messenger](../Page/MSN_Messenger.md "wikilink")、[Google
Talk等主流通讯软件与人随时沟通](../Page/Google_Talk.md "wikilink")。在每次开机之后，Mylo将会自动搜寻离你最近的热点。估计其将会在2006年9月份上市，价格大概是350美元。

## 外部連結

  - [Sony
    Mylo可能引發「數位暴動」？](https://web.archive.org/web/20070927004241/http://tech.digitimes.com.tw/ShowNews.aspx?zCatId=416&zNotesDocId=BE38BB626AC1ADA6482571E200674273)


[Category:索尼](../Category/索尼.md "wikilink")
[Category:資訊設備](../Category/資訊設備.md "wikilink")