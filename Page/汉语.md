**漢語**，又稱**中文**、**華文**、**唐話**\[1\]，或被視為[漢語族](../Page/漢語族.md "wikilink")，或被視為[漢藏語系](../Page/漢藏語系.md "wikilink")[漢語族下之唯一語言](../Page/漢語族.md "wikilink")。

作為語言而言，為[世界使用人数最多的语言](../Page/以人口排列的语言列表.md "wikilink")，目前世界有五分之一人口做為[母語](../Page/母語.md "wikilink")。漢語有多種[分支](../Page/汉语變體.md "wikilink")，當中[官話最為流行](../Page/官話.md "wikilink")，為[中华人民共和国的](../Page/中华人民共和国.md "wikilink")[国家通用語言](../Page/中华人民共和国国家通用语言文字法.md "wikilink")（又稱為[普通話](../Page/普通話.md "wikilink")）、以及[中華民國的](../Page/中華民國.md "wikilink")[國語](../Page/中華民國國語.md "wikilink")。此外，中文還是[聯合國](../Page/聯合國.md "wikilink")[正式語文](../Page/聯合國正式語文.md "wikilink")\[2\]，並被[上海合作组织等國際組織採用為官方語言](../Page/上海合作组织.md "wikilink")。在[中國大陸](../Page/中國大陸.md "wikilink")，漢語通稱為「漢語」。在[聯合國](../Page/聯合國.md "wikilink")\[3\]、
[臺灣](../Page/臺灣.md "wikilink")\[4\]、
[香港](../Page/香港.md "wikilink")\[5\]及
[澳門](../Page/澳門.md "wikilink")\[6\]
，通稱為「中文」。在[新加坡及](../Page/新加坡.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")，通稱為「華語」\[7\]。其他稱呼僅限特定人群使用，請另見相關條目。</ref>。

**[汉字](../Page/汉字.md "wikilink")**是汉语的[文字書寫系统](../Page/文字.md "wikilink")，又称**汉文**、**華文**、**中文**、**唐文**，在中华民国又称为**国文**，是一种[意音文字](../Page/意音文字.md "wikilink")，表意之同時也具一定表音功能。漢語属[分析语](../Page/分析语.md "wikilink")，且[声调辨義](../Page/声调.md "wikilink")。漢語包含[書面語及](../Page/書面語.md "wikilink")[口語兩部分](../Page/口語.md "wikilink")，古代書面语称为[文言文](../Page/文言文.md "wikilink")，現代書面语一般指[官話白話文](../Page/官話白話文.md "wikilink")，即使用標準官話語法、詞彙的中文通行文体。[粵](../Page/廣東.md "wikilink")[港](../Page/香港.md "wikilink")[澳等地又流行夾雜](../Page/澳門.md "wikilink")[文言文](../Page/文言文.md "wikilink")、[官話白話文及](../Page/官話白話文.md "wikilink")[粵語白話文的](../Page/粵語白話文.md "wikilink")[三及第文體](../Page/三及第.md "wikilink")，吳語區亦偶有人書寫[吳語白話文](../Page/吳語白話文.md "wikilink")。

对于汉语，学界主要有两种观点，一种观点将汉语定义为语言，并将[官话](../Page/官话.md "wikilink")、[贛語](../Page/贛語.md "wikilink")、[闽语](../Page/闽语.md "wikilink")、[粤语](../Page/粤语.md "wikilink")、[客家语](../Page/客家语.md "wikilink")、[吴语](../Page/吴语.md "wikilink")、[湘语七大分支定义为](../Page/湘语.md "wikilink")[一级方言](../Page/汉语方言.md "wikilink")；另一种观点则将汉语视为语族，七大分支因無法互相溝通而視為语支，而语支下面的各个分支被视为独立的语言\[8\]，如[國際標準化組織就將漢語族分為](../Page/國際標準化組織.md "wikilink")13種語言：[闽东语](../Page/闽东语.md "wikilink")、[晋语](../Page/晋语.md "wikilink")、[官话](../Page/官话.md "wikilink")、[莆仙语](../Page/莆仙语.md "wikilink")、[徽语](../Page/徽语.md "wikilink")、[闽中语](../Page/闽中语.md "wikilink")、[赣语](../Page/赣语.md "wikilink")、[客家语](../Page/客家语.md "wikilink")、[湘语](../Page/湘语.md "wikilink")、[闽北语](../Page/闽北语.md "wikilink")、[闽南语](../Page/闽南语.md "wikilink")、[吴语](../Page/吴语.md "wikilink")、[粤语](../Page/粤语.md "wikilink")。

## 槪論

### 通用語

漢語不同分支不一定能[互通](../Page/相互理解性.md "wikilink")，不過不同漢語分支的人一般通曉[標準官話](../Page/標準官話.md "wikilink")，各地官話標準包括[中华人民共和国](../Page/中华人民共和国.md "wikilink")[普通話](../Page/普通話.md "wikilink")、[新馬華語和](../Page/新馬華語.md "wikilink")[中華民國國語大致相同](../Page/中華民國國語.md "wikilink")，僅有個別字詞的讀音有些微分別。書面通用語通常指現代[官話白話文](../Page/官話白話文.md "wikilink")。

[大中华地区的中小學多有](../Page/大中华地区.md "wikilink")[中國語文科](../Page/中國語文科.md "wikilink")，教授中文字、漢語語法、文學，又稱語文課、中文堂、國文課、華語課。在[中國大陸](../Page/中國大陸.md "wikilink")，漢語標準為[普通話](../Page/普通話.md "wikilink")——以[北京話为标准语音](../Page/北京話.md "wikilink")、以[官話白話文著作作为](../Page/官話白話文.md "wikilink")[語法規範](../Page/語法規範.md "wikilink")，並以此教授語文課。在[香港和](../Page/香港.md "wikilink")[澳門](../Page/澳門.md "wikilink")，以[粵語為](../Page/粵語.md "wikilink")[通用口語及](../Page/通用語.md "wikilink")[教學語言](../Page/教學語言.md "wikilink")，學校會以粵語授課，但公文書面語以[官話白話文為準](../Page/官話白話文.md "wikilink")，亦偶有夾雜文言。在[臺灣及](../Page/臺灣.md "wikilink")[新](../Page/新加坡.md "wikilink")[馬地區](../Page/馬來西亞.md "wikilink")，則分別採用[國語和](../Page/中華民國國語.md "wikilink")[華語為標準](../Page/華語.md "wikilink")，並以此授中文課。此外，中華民國、香港和澳門以[正體中文為主要文字](../Page/正體中文.md "wikilink")，中華人民共和國、[新加坡及](../Page/新加坡.md "wikilink")[馬來西亞以](../Page/馬來西亞.md "wikilink")[簡體中文為主要文字](../Page/簡體中文.md "wikilink")。

若視漢語為單一語言，漢語則為當今世界上作为[母语使用人数最多的語言](../Page/母语.md "wikilink")。世界上大約有五分之一人口以漢語為母語，主要集中在[中國](../Page/中國.md "wikilink")。[海外華人亦使用漢語](../Page/海外華人.md "wikilink")。在[中華人民共和國](../Page/中華人民共和國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[中华民国及](../Page/中华民国.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")，漢語被列為[官方語言](../Page/官方語言.md "wikilink")，更是[联合国的](../Page/联合国.md "wikilink")[正式語文和工作语言](../Page/聯合國正式語文.md "wikilink")。隨著[大中華地區在世界的影響力增加](../Page/大中華地區.md "wikilink")，在一些國家逐漸興起學習漢語的風潮。而在部份國家為吸引華裔觀光客，會在主要車站、機場等公共場所及觀光地區增加中文的標示及說明，部份服務業亦會安排通曉漢語的服務人員。

### 各種變體

[中國大陸大部分语言学家认为汉语是一种单一的语言](../Page/中國大陸.md "wikilink")，但大部分國際语言学家和中国国内的小部分语言学家及一些[地方主义者认为漢語作為](../Page/地方主义.md "wikilink")[語族是](../Page/语言系属分类.md "wikilink")[官话](../Page/官话.md "wikilink")、[粵語](../Page/粵語.md "wikilink")、[吳語](../Page/吳語.md "wikilink")、[閩語](../Page/閩語.md "wikilink")、[客家话等語言的統稱](../Page/客家话.md "wikilink")，即汉语是由一簇亲属语言组成的语族，為一种语言类别。

在[語言學原則上](../Page/語言學.md "wikilink")，互相之間不能通話的應該被定性為[語言而非](../Page/語言.md "wikilink")[方言](../Page/方言.md "wikilink")。由於[官話](../Page/官話.md "wikilink")、[吳語](../Page/吳語.md "wikilink")、[閩語](../Page/閩語.md "wikilink")、[粵語等語之使用者在口語上不能互相通話](../Page/粵語.md "wikilink")，各分支被[國際標準化組織](../Page/國際標準化組織.md "wikilink")\[9\]定性為語言，在2007年的国际认证ISO
639-3国际[语种代號的编制中](../Page/语言系属分类.md "wikilink")，国际标准化组织將汉语拆分为13种不能互相溝通的“语言”：cdo
– [闽东语](../Page/闽东语.md "wikilink")，cjy –
[晋语](../Page/晋语.md "wikilink")，cmn –
[官话](../Page/官话.md "wikilink")，cpx –
[莆仙语](../Page/莆仙语.md "wikilink")，czh –
[徽语](../Page/徽语.md "wikilink")，czo –
[闽中语](../Page/闽中语.md "wikilink")，gan –
[赣语](../Page/赣语.md "wikilink")，hak –
[客家语](../Page/客家语.md "wikilink")，hsn –
[湘语](../Page/湘语.md "wikilink")，mnp –
[闽北语](../Page/闽北语.md "wikilink")，nan –
[闽南语](../Page/闽南语.md "wikilink")，wuu –
[吴语](../Page/吴语.md "wikilink")，yue –
[粤语](../Page/粤语.md "wikilink")。它们之间是否为独立的语言关系，至今学界[仍存在争论](../Page/漢語變體#變體地位.md "wikilink")。在[中國大陸](../Page/中國大陸.md "wikilink")，这13种“语言”往往被学者当作汉语的方言。

儘管不同漢語分支的[白話文差異甚大](../Page/白話文.md "wikilink")，但由於[官話白話文最為流行](../Page/官話白話文.md "wikilink")，[中文書面語較為統一](../Page/中文書面語.md "wikilink")，但各地之間的[口語與詞彙有一定差異](../Page/口語.md "wikilink")。又因為有統一標準的[書面語和文字](../Page/漢語書面語.md "wikilink")，被认为與西方語言學體制有一定差別。有人認為漢語在口語上更像是建立在[書寫共同文字上的](../Page/書同文.md "wikilink")[語族而非單一語言](../Page/語族.md "wikilink")。

目前，[中華人民共和國以](../Page/中華人民共和國.md "wikilink")[普通话為通用語言](../Page/普通话.md "wikilink")，[中華民國以](../Page/中華民國.md "wikilink")[国語為通用語言](../Page/中華民國國語.md "wikilink")，兩者都是基于[官话之](../Page/官话.md "wikilink")[北京音](../Page/北京话.md "wikilink")，偶有不同之處，但並不妨礙交流。同时在[兩廣地區](../Page/兩廣.md "wikilink")、[香港與](../Page/香港.md "wikilink")[澳門等地以及部份海外](../Page/澳門.md "wikilink")[華人以](../Page/華人.md "wikilink")[粵語作為通用語](../Page/粵語.md "wikilink")，另外使用[潮州话](../Page/潮州话.md "wikilink")、[闽南语](../Page/闽南语.md "wikilink")、[客家语](../Page/客家语.md "wikilink")、[吳語等或其他漢語分支的人会使用自己的](../Page/吳語.md "wikilink")[母语作為交際通用語言](../Page/母语.md "wikilink")。

### 文體

漢语主要以使用[漢字書寫](../Page/漢字.md "wikilink")，為[語素文字](../Page/語素文字.md "wikilink")。現[漢字擁有兩套文字系統](../Page/漢字.md "wikilink")，分別為[正體字與](../Page/正體字.md "wikilink")[簡體字](../Page/簡體字.md "wikilink")。[文言文和](../Page/文言文.md "wikilink")[白話文均係中文書面語](../Page/白話文.md "wikilink")，書面語語法雖然較統一和規範，但[漢字表音富於變化](../Page/漢字.md "wikilink")。「蓋時有古今，地有南北；字有更革，音有轉移，亦勢所必至」\[10\]，在漫長的歷史時期、廣袤的地域內，漢語音韻、用詞及語法因時隨地變遷，並產生不同[分支](../Page/漢語變體.md "wikilink")。不過古代通行以[正體字為文字系統及以](../Page/正體字.md "wikilink")[上古漢語所使用的](../Page/上古漢語.md "wikilink")「[雅言](../Page/雅言.md "wikilink")」為基礎的書面語——[文言文](../Page/文言文.md "wikilink")，消除因地域差異造成的交流障礙。

文言文在[古代的一些](../Page/古代.md "wikilink")[東亞](../Page/東亞.md "wikilink")、[東南亞國家為官方行文的標準](../Page/東南亞.md "wikilink")，稱為[漢字文化圈](../Page/漢字文化圈.md "wikilink")。現時[東亞國家使用文言文亦可交流](../Page/東亞.md "wikilink")，但此傳統文體的使用者越來越少，現今大多使用現代文體，及學習漢語以外的語言來交流。

[白話文運動所推動的書面漢語通常指官話白話文](../Page/白話文運動.md "wikilink")，即以[北方官話為基礎的書面語](../Page/北方官話.md "wikilink")。[白话文运动兴起之前](../Page/白话文运动.md "wikilink")，偶有人用白话文写作，比如[宋朝](../Page/宋朝.md "wikilink")、[元朝的一些](../Page/元朝.md "wikilink")[筆記小說以及](../Page/筆記小說.md "wikilink")[白話小說](../Page/白話小說.md "wikilink")，但正式場合使用文言文體。明清[官話白話文的作品有](../Page/官話白話文.md "wikilink")《[红楼梦](../Page/红楼梦.md "wikilink")》、《[老残游记](../Page/老残游记.md "wikilink")》、《[官场现形记](../Page/官场现形记.md "wikilink")》等，其中《[儒林外史](../Page/儒林外史.md "wikilink")》、《[二十年目睹之怪現狀](../Page/二十年目睹之怪現狀.md "wikilink")》用詞接近現代官話，[吳語白話文的作品有](../Page/吳語白話文.md "wikilink")《[海上花列傳](../Page/海上花列傳.md "wikilink")》等。在现代[中文书面语中](../Page/中文书面语.md "wikilink")，雖然純文言已经很少使用，但是在[海峽兩岸的語文教學中](../Page/海峽兩岸.md "wikilink")，[文言文仍然佔有重要地位](../Page/文言文.md "wikilink")，一些官方文書和文藝作品仍然採用文白之間之文體。白话文运动所推动的“[白話文](../Page/白話文.md "wikilink")”，分別有[官話白話文](../Page/官話白話文.md "wikilink")、[粵語白話文](../Page/粵語白話文.md "wikilink")、[吳語白話文](../Page/吳語白話文.md "wikilink")、[中州韻白話文](../Page/中州韻白話文.md "wikilink")、[臺語白話文等](../Page/臺語白話文.md "wikilink")。

中國漢語各變體母語人口比例：  {{\#invoke:Chart|pie chart | radius = 150 | slices =
(84.78:官話:\#ac8761) (7.72:吳語:\#c8c8a8) (7.18:闽語:\#b1bab6)
(6.22:粵語:\#bf6e7b) (4.5:晉語:\#96642c) (3.60:湘語:\#a5ae87)
(3.01:客語:\#eeb36a) (2.06:贛語:\#fbd98d) (0.46:徽語:\#9b987f) | units
suffix = 千萬 | percent = true }}

## 歷史

漢語擁有極長且連續之歷史記錄。古代的漢語有三個明顯的發展階段：以[先秦時代為核心的](../Page/先秦.md "wikilink")[上古漢語](../Page/上古漢語.md "wikilink")，其語音體系一般稱上古音，以詩經和形聲字的[古韻推測](../Page/古韻.md "wikilink")，文字為[甲骨文](../Page/甲骨文.md "wikilink")、[金文和](../Page/金文.md "wikilink")[篆書](../Page/篆書.md "wikilink")；以[唐代和](../Page/唐代.md "wikilink")[宋代為核心的](../Page/宋代.md "wikilink")[中古漢語](../Page/中古漢語.md "wikilink")
，其語音體系一般稱[中古音](../Page/中古音.md "wikilink")，為[等韻和](../Page/等韻.md "wikilink")[韻圖所記錄](../Page/韻圖.md "wikilink")，文字為楷書；以明末[清初為核心的](../Page/清朝.md "wikilink")[近代漢語](../Page/近代漢語.md "wikilink")。亦有人分成先秦、兩漢[六朝](../Page/六朝.md "wikilink")、唐宋、元明清四個階段。古代漢語在不同階段最明顯的差別是語音，而[上古漢語還有文字也與後世不同](../Page/上古漢語.md "wikilink")。狹義的古代漢語的面貌大體上和[中古漢語一致](../Page/中古漢語.md "wikilink")，因為這是古代漢語發展最繁榮、影響力最大、產生最多代表性作品的時期。

### 先秦

先秦時期的漢語一般稱為[上古漢語](../Page/上古漢語.md "wikilink")，音系以詩經和形聲字的[古韻推測](../Page/古韻.md "wikilink")，文字為[甲骨文](../Page/甲骨文.md "wikilink")、[金文和](../Page/金文.md "wikilink")[篆書](../Page/篆書.md "wikilink")，文體為[古文](../Page/古文_\(文學\).md "wikilink")。

相传[黄帝时中原有](../Page/黄帝.md "wikilink")“万国”，[夏朝时还有三千国](../Page/夏朝.md "wikilink")，[周初分封八百诸侯](../Page/周朝.md "wikilink")，而“五方之民，言语不通”（《[礼记](../Page/礼记.md "wikilink")·王制》）。上古汉语存在于周朝前期和中期（前11世纪－7世纪），文字记录有[青铜器上的刻铭](../Page/青铜器.md "wikilink")、《[诗经](../Page/诗经.md "wikilink")》、历史书《[书经](../Page/书经.md "wikilink")》以及部分《[易经](../Page/易经.md "wikilink")》。[春秋初期](../Page/春秋時期.md "wikilink")，见于记载的诸侯国还有170多个。至[战国时期](../Page/戰國_\(中國\).md "wikilink")，形成“七雄”，“诸侯力政，不统于王，……言语异声，文字异形”（《[说文解字](../Page/说文解字.md "wikilink")·叙》）。先秦[诸子百家在著作中使用被称为](../Page/诸子百家.md "wikilink")“[雅言](../Page/雅言.md "wikilink")”的共同语。“子所雅言，《诗》、《书》、执《礼》，皆雅言也。”（《[论语](../Page/论语.md "wikilink")·述而》）秦统一天下之后，实行“车同轨，书同文，行同伦”，规范了文字，以[小篆作为正式官方文字](../Page/小篆.md "wikilink")。

重构上古汉语发音的工作开始于[清朝的语言学家](../Page/清朝.md "wikilink")。西方的古汉语先锋是瑞典的语言学家[高本汉](../Page/高本汉.md "wikilink")，他主要研究汉字的形式和诗经的韵律。

### 兩漢六朝

### 隋至宋

[唐](../Page/唐朝.md "wikilink")[宋時期的漢語一般稱為](../Page/宋朝.md "wikilink")[中古漢語](../Page/中古漢語.md "wikilink")，其語音體系一般稱[中古音](../Page/中古音.md "wikilink")，為[等韻和](../Page/等韻.md "wikilink")[韻圖所記錄](../Page/韻圖.md "wikilink")，文字為楷書。大致可以分為早期的《[切韻](../Page/切韻.md "wikilink")》（601年）以及反映晚期的《[廣韻](../Page/廣韻.md "wikilink")》（10世紀）。高本漢將這個階段稱為「古代漢語」。部份現代語言學家透過[比照法而提出的現代](../Page/比照法.md "wikilink")[閩語支各語言的](../Page/閩語支.md "wikilink")[祖語](../Page/祖語.md "wikilink")。閩語支各語言擁有不少在[中古漢語乃至現代其他](../Page/中古漢語.md "wikilink")[漢語族語言中都沒有的特徵](../Page/漢語族.md "wikilink")，相信[原始閩語於此時與中古漢語分道掦鑣](../Page/原始閩語.md "wikilink")。

正如[印歐語系諸古語言可以由現代印歐語言重構一樣](../Page/印歐語系.md "wikilink")，中古漢語也可以透過現代漢語的變體重建。语言学家已能较自信地重构中古汉语的语音系统。这种证据来自几个方面：多样的现代方言、[韵书以及对外语的翻译](../Page/韵书.md "wikilink")。中國古代的文學家花費了很大精力來總結漢語的語音體系，例如令[詩](../Page/詩.md "wikilink")[詞](../Page/詞.md "wikilink")[押韻](../Page/押韻.md "wikilink")，此等資料是現代語言學家工作之基礎。最後，古漢語語音可以從古時對外國語言之翻譯中瞭解到。

### 元明清

中國北方經歷[遼](../Page/遼.md "wikilink")[金](../Page/金.md "wikilink")[元等遊牧民族改朝換代](../Page/元朝.md "wikilink")，飽經戰亂，北方漢語大變。到[明](../Page/明朝.md "wikilink")[清時](../Page/清朝.md "wikilink")，[北方漢語已經和很多南方漢語變體平行發展](../Page/官話.md "wikilink")。學者[魯國堯認為客家話](../Page/魯國堯.md "wikilink")、贛語是中古漢語的[南朝漢語後裔](../Page/客、贛、通泰方言源於南朝通語說.md "wikilink")\[11\]；
而吳語、湘語、粵語也獲認為是南朝漢語的後裔。
《[水浒传](../Page/水浒传.md "wikilink")》《[西游记](../Page/西游记.md "wikilink")》等书所用语言即为近代官話。
《[二十年目睹之怪现状](../Page/二十年目睹之怪现状.md "wikilink")》等清代小说中的文字更與今日之[官話白話文没有多少区别](../Page/官話白話文.md "wikilink")，足以反映清朝官话与今日的[官話没有实质差异](../Page/官話.md "wikilink")。

### 現代

1913年，讀音統一會用逐字投票方式確定了「國音」標準，嘗試匯通南北，現稱為[老國音](../Page/老國音.md "wikilink")。

1923年，當時的國語統一籌備會成立了「國音字典增修委員會」，決定改用北京語音為標準。現代標準漢語以[北方官話為基礎](../Page/北方官話.md "wikilink")，並以典範的官話白話文著作為語法規範的漢語。

## 地域變體

### 地方语言

由於古代中國有[韻書但無強制教育發音標準](../Page/韻書.md "wikilink")，故漢語口語在中國各地發音有所變化，有部分變化很大。在使用汉语的非语言学人士中，多数人都用“地方語言”来指称口語發音相互有差别的汉语。漢語也有被視為一個包含一組[親屬語言的](../Page/親屬語言.md "wikilink")[語族](../Page/語族.md "wikilink")「[漢語族](../Page/漢語族.md "wikilink")」。這種觀點也得到中國國内部分學者的支持\[12\]。

現在語言學上有兩種不同的觀點：

  - 漢語語族只有漢語一種語言，只是口語發音有所不同。此觀點將閩語、粵語、客語、吳語、官話、晉語、贛語、湘語等列為漢語的方言。
  - 漢語族包含閩語、粵語、客語、吳語、贛語、官話、湘語等七大語言（或者是閩語、粵語、客語、吳語、贛語、官話、湘語、晉語、徽語、平話），再加上閩語內部不能互通，所以閩語實際上是一類語言，而在語言學上的歸屬應該是閩語語群，其下的閩南語、閩東語、閩北語、閩中語和莆仙語則為單一語言。此觀點認為漢語為[語族](../Page/汉语族.md "wikilink")，由一簇互相關聯的親屬語言。

瑞典著名汉学家[高本汉在其著作](../Page/高本汉.md "wikilink")《中国音韵学研究》中将[朝鲜语](../Page/朝鲜语.md "wikilink")、[日本语](../Page/日本语.md "wikilink")、[越南语等其他](../Page/越南语.md "wikilink")[漢字文化圈之](../Page/漢字文化圈.md "wikilink")
语言称作汉语的“域外方言”。这是作者在汉语研究的特殊条件下为贯彻历史[比较语言学的方法而采用的比拟性质的简便说法](../Page/比较语言学.md "wikilink")。

另有說法主張[白語](../Page/白語.md "wikilink")（[白族的語言](../Page/白族.md "wikilink")）也屬於漢語族，如[美國漢學家](../Page/美國.md "wikilink")[白保羅](../Page/白保羅.md "wikilink")。由於學術界一般肯定白語与漢語的分裂是在公元前2世紀左右，更因為[白族不是](../Page/白族.md "wikilink")[漢族](../Page/漢族.md "wikilink")，不適用於如此“具有鮮明漢民族特色”的語謂體系，因此這種將白語納入漢語方言的說法无法獲普遍认同。

### 汉语分支

\-{zh-hans:[Map_of_sinitic_languages_full-zh.svg](https://zh.wikipedia.org/wiki/File:Map_of_sinitic_languages_full-zh.svg "fig:Map_of_sinitic_languages_full-zh.svg");
zh-hant:[Map_of_sinitic_languages_full-zh-hant.svg](https://zh.wikipedia.org/wiki/File:Map_of_sinitic_languages_full-zh-hant.svg "fig:Map_of_sinitic_languages_full-zh-hant.svg")}-
中國國内語言學家根據漢語分支的不同特點，把漢語劃分為傳統的七大方言。\[13\]在這七大方言內部，仍存在不同的次方言區。有時這些次方言區內的使用者也不能相互理解。在不同的方言區的人的語言意識也有一定的區別。例如，使用[厦门话的厦门人可能會感到與操](../Page/厦门话.md "wikilink")[海南话的海口人有很多共同點](../Page/海南话.md "wikilink")，雖然他們可能在相互理解上存在些許的困难。

在[華北官話](../Page/華北官話.md "wikilink")、[西北官話或者](../Page/西北官話.md "wikilink")[西南官話地區](../Page/西南官話.md "wikilink")，各地区内相隔幾百公里的人一般也可以相互口頭交流；然而在中国南方的许多地区，尤其是山区，較小地理範圍内可能存在相互口語交流困難的方言。例如，如福建[闽东地区或](../Page/闽东.md "wikilink")[温州](../Page/温州.md "wikilink")[瑞安](../Page/瑞安.md "wikilink")、[平阳](../Page/平阳县_\(湖南\).md "wikilink")、[苍南等地](../Page/苍南.md "wikilink")，[南吴方言](../Page/温州话.md "wikilink")、[北吴方言](../Page/苏州话.md "wikilink")、[蛮话](../Page/蛮讲.md "wikilink")、[闽语区交错](../Page/闽语.md "wikilink")，相隔只有十公里的當地居民也許已經不能自如地口頭交流了。

#### 官話

官话，或称官话方言、北方話等：指华北、东北及西北地區、湖北大部、四川、重庆、雲南、貴州、湖南西北部、江西沿江地區、安徽大部、江蘇大部所使用的母語方言。官話大致分為[華北官話](../Page/華北官話.md "wikilink")、[西北官話](../Page/西北官話.md "wikilink")、[西南官話](../Page/西南官話.md "wikilink")、[江淮官話](../Page/江淮官話.md "wikilink")，华北官话分布在北方东部，以[北京話為代表](../Page/北京話.md "wikilink")，西北官話分佈在北方西部，以[西安話為代表](../Page/西安話.md "wikilink")，西南官話分佈在南方西部，以[成都話為代表](../Page/成都話.md "wikilink")，江淮官話分佈在南方東部，以[扬州話為江淮話的代表](../Page/扬州話.md "wikilink")。類似上古时期的中原[雅音在](../Page/雅音.md "wikilink")[五胡亂華](../Page/五胡亂華.md "wikilink")、[衣冠南渡后](../Page/衣冠南渡.md "wikilink")，分化成為中古汉语等语音。而现代“官话方言”，主要形成于明清时期。[清朝](../Page/清朝.md "wikilink")[官话在形成之后](../Page/官话.md "wikilink")，在南北方分别发展，由分化成了[南方官話和](../Page/南方官話.md "wikilink")[北方官話](../Page/北方官話.md "wikilink")，[北京話至今為](../Page/北京話.md "wikilink")[現代標準漢語的基礎](../Page/現代標準漢語.md "wikilink")（中國大陸稱為[普通話](../Page/普通話.md "wikilink")，臺灣目前仍被定義稱為[國語](../Page/國語.md "wikilink")）。使用這一方言的人占中國人口的70%。

需要指出的是，“官话方言”，过去曾经称为“北方方言”，現今并不局限于中国北方。相反，中国西南地区和江淮地区的南方方言也属于官话方言，但相对其他地区的北方方言，西南官话与江淮官话在官话区的可通行度相对较低，很多北方地区的官话使用者较难理解南方官话地区的使用者的语言，而反之则较容易。

官话的明顯特點包括：失落了全部中古入聲（除[江淮官话及](../Page/江淮官话.md "wikilink")[西南官话中的少部分小片以外](../Page/西南官话.md "wikilink")，如灌赤片），中古漢語中的“−p，−t，−k，−m，−n，−ng”韻尾現在只剩下“−n，−ng”，并出現了大量[兒化韻](../Page/兒化韻.md "wikilink")“−r”韻尾。原本連接“i，ü”韻母的“g，k，h”聲母已被[顎音化成](../Page/顎音化.md "wikilink")“j，q，x”聲母。官話在失去清濁對立的過程中，沒有經過劇烈的聲調分化，但出現了中古[平上去入以外的輕聲](../Page/四聲.md "wikilink")。因此，官话方言包含了大量的同音字以及相應産生的複合詞。

#### 吳語

吴语，或称吴方言：主要通行於中國江蘇南部、安徽南部、上海、浙江大部分地區、江西東北部和福建西北角，以及香港、日本九州島、美國三藩市等地說吳語的部分移民中間。典型的吳語以[苏州话為代表](../Page/苏州话.md "wikilink")。其中安徽东南部受[贛語](../Page/贛語.md "wikilink")、[江淮官话影响](../Page/江淮官话.md "wikilink")，浙江南部吳語使用人數大約為總人口的8.4%，與北部吳語差異較大。吳語最重要的特徵是中古全濁聲母仍保留濁音音位，比如「凍」、「痛」、「洞」的聲母分別\[t\]、\[tʰ\]、\[d\]（普通話「洞」的聲母清化為\[t\]），北部吳語儘管全濁聲母在起首或單唸時通常清化，即清音濁流，只在詞或語句中維持濁音，在南部吳語中濁音的表現形式一般為濁音濁流。吳語中的濁音聲母基本保留了中古漢語的特點，個數為8到11個，但受到北方官話的影響，吳語的聲母個數是漢語方言中最多的，一般為30個左右，而聲母最少的閩南話僅為16
個，粵語17個；吳語是以單元音為主體的方言。普通話中，ai，ei，ao，ou等都是雙元音韻母，發音的時候聲音拖得很長，而且口部很鬆，而吳語恰好相反，一般來說，對應普通話ai，ei，ao，ou的音，在吳語中分別是ɛ/ø,e,ɔ,o，都是單元音，並且發音的時候口形是比較緊的。絕大多數地區保留入聲韻（除[甌江片](../Page/甌江片.md "wikilink")、[金衢片的部分地區外](../Page/金衢片.md "wikilink")，均收[喉塞音](../Page/喉塞音.md "wikilink")\[ʔ\]）。

#### 赣语

赣语，或称赣方言：以南昌話為代表，主要通行于江西、湖南东部、湖北东南部、安徽西南部和福建的西部等地区，是该些地区事实上的公用语。使用赣语的人口在6000万，约占中国人口的6%左右，世界排第三十位。其中湖北通城方言、湖南平江方言有独特性。

#### 閩語

閩語以閩東語、閩北語和閩南語為代表。[閩東语](../Page/閩東语.md "wikilink")（或称閩東方言，北片以福安音為代表，南片以福州音為代表）在福建東部的寧德、福州、馬祖、東南亞多國以及美國唐人街中使用，分佈範圍甚廣。[闽南语](../Page/闽南语.md "wikilink")（或称闽南方言，以泉州府音為正宗）在台湾和福建南部的泉州、廈門、漳州、海南、廣東東部以及東南亞華人中使用，分布泛圍較廣。广义的闽南语是河洛语，存在于江苏宜兴、温州苍南、闽南、潮汕、雷州、海南、广西等地。

闽语是所有地方语言中唯一不完全与中古汉语韵书存在直接对应的语系。

閩南語保留“−m，−n，−ng，−p，−t，−k，[−ʔ](../Page/聲門塞音.md "wikilink")”七種輔音韻尾，閩東語則出現“入声弱化”，\[−p̚/−t̚/−k̚\]
全部變成
\[ʔ\]。[閩語是](../Page/閩語.md "wikilink")[漢語中聲調较複雜的方言](../Page/漢語.md "wikilink")，[閩東语有七個聲調](../Page/閩東语.md "wikilink")（不含輕聲調和語流音變聲調）。閩東語極為發達的語流音變現象，是其顯著特徵和學習難點。而閩東語的分支[福州話](../Page/福州話.md "wikilink")，當地人將之稱為平話。[闽南语泉州音有八個聲調](../Page/闽南语.md "wikilink")（不含轻声），漳州音、厦门音、臺湾音通常有七個声调（不含轻声調、高聲調）。泉州音和漳州音是其它支系的祖语，閩（南）臺片的閩南語内部较为一致。广义的闽南方言还包括[海南話](../Page/海南話.md "wikilink")、[雷州話](../Page/雷州話.md "wikilink")、[潮州話](../Page/潮州話.md "wikilink")、[浙南閩语等](../Page/浙南閩语.md "wikilink")。

#### 粵語

粵语以[廣州話為標準](../Page/廣州話.md "wikilink")，在[廣東](../Page/廣東.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[廣西東部和海外華人中使用](../Page/廣西.md "wikilink")，中國的一些[少數民族如](../Page/少數民族.md "wikilink")[京族](../Page/京族.md "wikilink")、部分[壯族也使用粵語](../Page/壯族.md "wikilink")。[粵語是漢語眾多分支中聲調最複雜的一種](../Page/粵語.md "wikilink")。[標準粵語有九個声调](../Page/標準粵語.md "wikilink")，某些方言如勾漏方言、[桂南](../Page/桂南.md "wikilink")[平話方言具有十个声调](../Page/廣西平話.md "wikilink")。粵語完整保留了中古漢語的
−p、−t、−k、−m、−n、−ng 六種輔音韻尾。
粤语没有混合入声，普遍认为粤语中保留的古汉语成分最為接近隋唐時期的官話。粵語有一套自己的書面的[白話文表示方式](../Page/白話文.md "wikilink")，參見[粵語白話文](../Page/粵語白話文.md "wikilink")。标准粤语对现代汉语白话文中的常用汉字作了比较完整的审音配字工作。相比之下，吴语、闽南语和客家語皆未完成汉字审音配字的标准化工作。[粵语內部具有多種方言](../Page/粵语.md "wikilink")，但是内部高度统一，大多广西粤语方言可以与广东粤语方言直接无障碍沟通。詳細請參看[粵語方言](../Page/粵語方言.md "wikilink")。使用粵语的漢族人口大約為漢族人口總數的9%。海外華人特別是美洲、澳洲華人社區已經成為使用人數最多的漢語。

#### 湘语

湘语，或称湘方言，通常以[長沙話為標準](../Page/長沙話.md "wikilink")。使用者主要分布在湖南大部分地区，即湘江及其支系流域。湘语内部又可以按是否保留中浊声母分类，可分为[老湘和](../Page/老派湘语.md "wikilink")[新湘兩類](../Page/新派湘语.md "wikilink")，其中新湘語的全浊声母已基本清化。新湘语形成的时间不太长，其中受到过赣语以及北方移民的影响，在语音体系上体现出明显靠近官话的特征\[14\]。新湘语只有少数一些地方保留了全浊声母，而老湘语全浊声母保留相对完整，比如邵阳(蔡桥)方言共有33个声母，包括完整的浊擦音，浊塞音，浊塞擦音\[15\]。娄底方言:波\[p\],坡\[pʰ\],婆\[b\]
不同音。湘语分别以長沙話（新）及雙峰話（老）為代表，使用者約占總人口的5%。根据湘语主要城市人口统计湘语使用人口3596万，湖南总人口6440万，约占该省人口的56%\[16\]。

#### 客家語

  - 客家話，或称客家方言、客家語、客語，在中国南方和[东南亚的客家人和多数的](../Page/东南亚.md "wikilink")[畲族中广泛使用](../Page/畲族.md "wikilink")，包括广东东部、北部、福建西部、江西南部、台湾西北部和广西东南部，以广东[梅县话为代表](../Page/梅县话.md "wikilink")。虽然是一种南方方言，但客家话是在北方移民南下影响中形成的——“客家”意思是“客人的家”。虽然客家话与普通话有很多共同之处，但它们的语音并不能相互理解，使用客家话的人口大约为4%，[客家话的特點是上聲](../Page/客家话.md "wikilink")，去聲不分陰陽，但平聲，入聲分陰陽，保留了一些中古中原話的特點。

#### 东干语

东干语为近代汉语[中原官话和](../Page/中原官话.md "wikilink")[兰银官话在中国境外的特殊变体](../Page/兰银官话.md "wikilink")。融合了[俄语](../Page/俄语.md "wikilink")、[阿拉伯语](../Page/阿拉伯语.md "wikilink")、[波斯语和](../Page/波斯语.md "wikilink")[突厥语等语言的部分词汇](../Page/突厥语.md "wikilink")。

#### 其他變體

下面的幾種變體是否能與上述變體並肩，現在尚有爭議：

  - [晋语](../Page/晋语.md "wikilink")：在山西绝大部分以及陕西北部、河北西部、河南西北部、内蒙古[河套地区等地使用](../Page/河套地区.md "wikilink")，以[太原话为代表](../Page/太原话.md "wikilink")，有[入声韵](../Page/入声韵.md "wikilink")——
    \[−ʔ\]（在入聲 \[−p̚/−t̚/−k̚\] 消失之前，先發生‘入声弱化’，\[−p̚/−t̚/−k̚\] 全部變成
    \[−ʔ\]）。其[白读系统与](../Page/白读系统.md "wikilink")[官话截然不同](../Page/官话.md "wikilink")。以前（及现在的不少语言学学者）将其归于[官话](../Page/官话.md "wikilink")。
  - [平话](../Page/廣西平話.md "wikilink")：在广西中部和南部的部分地区使用。傳統上將[桂南平话归于](../Page/桂南平话.md "wikilink")[粤语](../Page/粤语.md "wikilink")，但與粵語仍有較大差異。近年來有人主張將[桂北平话当成孤立的土语存在](../Page/桂北平话.md "wikilink")。
  - [徽語](../Page/徽語.md "wikilink")：在安徽南部及赣浙部分毗邻地区使用。以前（及现在的部分语言学学者）将其归于[吳語](../Page/吳語.md "wikilink")。
  - [瓦鄉話](../Page/瓦鄉話.md "wikilink")：分佈在湖南西部以沅陵縣城為中心、沿沅水和酉水呈放射狀分佈的武陵山區，也稱為[鄉話](../Page/鄉話.md "wikilink")。[瓦鄉話處在周邊的西南官話](../Page/瓦鄉話.md "wikilink")、苗語湘西方言以及湘語的包圍之中，但與周邊各個方言都差別巨大，同時又保存了大量的古漢語音韻以及詞彙。聲母保留了全濁音，有知组读端，轻唇读重唇，来母部分读擦音和塞擦音，以母读擦音和塞擦音，定母部分读边音或塞擦音。韵母保留支旨之三分，支微入鱼，元音高化链，四等读洪音
  - [粵北土話](../Page/粤北土话.md "wikilink")：分佈在[廣東北部的樂昌](../Page/廣東.md "wikilink")、仁化、乳源、曲江、南雄、湞江、武江、連州、連南等地；也稱“韶州土話”，當地人稱為虱乸話。由於近年來廣東北部的粵語和普通話的強勢，現在[粵北土話在很多地方瀕臨失傳](../Page/粵北土話.md "wikilink")。
  - [湘南土話](../Page/湘南土話.md "wikilink")：分佈在[湖南南部的郴州和永州的大部分地區](../Page/湖南.md "wikilink")，主要在鄉鎮和農村使用，古老而獨特，且內部差異較大。[湘南土話和](../Page/湘南土話.md "wikilink")[粵北土話語音特點大致相同](../Page/粵北土話.md "wikilink")，與[粵北土話應歸為同一種語言](../Page/粵北土話.md "wikilink")。

## 語言系統

### 汉语音韻

漢語所有變體基本均为[声调语言](../Page/声调语言.md "wikilink")（吳語通常被看為由聲調語言向[音高重音語言演化中](../Page/音高重音.md "wikilink")），其声调在漫长的历史时期中不断地分化组合。在学术界，通常以“平上去入”[四声作为基本声调分类](../Page/四声.md "wikilink")。在平上去入四类的基础上，加上阴，阳，上，下等形容词作为[清浊的标记](../Page/清濁音.md "wikilink")。例如“阴入”，“阳入”，意为“清入声”和“浊入声”。其他以此类推。

汉语声调的变化，是推断古汉语语音的重要语音学证据。现存各變體中的声调调类和调值，也是推断此變體保留继承了那一历史时期的古汉语语音的最重要的语音学证据。

上古汉语的声调，现在学术界比较倾向于[王力的](../Page/王力_\(语言学家\).md "wikilink")“促舒四调”一说，舒声调有平声，上声，浊声调为长入，短入。

随后在两汉时期，去声大量从“浊上”这一声调中转化出来，被称为“浊上变去”。

在[魏](../Page/曹魏.md "wikilink")[晋](../Page/晋朝.md "wikilink")[南北朝期间](../Page/南北朝.md "wikilink")，汉语[四声稳定为](../Page/四声.md "wikilink")“平上去入”四声，但具体清浊调值则尚未研究清楚。此后，“平上去入”四声作为汉语的标准四声规范，一直沿用到[宋](../Page/宋朝.md "wikilink")[元时期](../Page/元朝.md "wikilink")。

入声通常以−p，−t，−k为辅音结尾。但在宋代，三种辅音结尾开始界限模糊，出现了混合入声。随后在元代，近代官话逐渐形成，入声在华北等地的官话中消亡，原本唸入聲的字，分到了別的音裡面，例如：雪、白等，有時造成詩歌裡平仄分辨錯誤的狀況。但是[南方漢語和](../Page/南方漢語.md "wikilink")[江淮官話](../Page/江淮官話.md "wikilink")，仍然保留了入声。

到了明清，入声消亡的情形在北方地区进一步加剧，并且“平声”逐渐出现了清浊之分，是为“阴平”和“阳平”。到了现代，以北方語为基础的大陆地区“普通话”，臺湾“国语”，马来西亚和新加坡的“标准华语”，均没有入声。但是，这三种官方语言的声调为“阴平，阳平，上声，去声”，仍然为四声，只是此“四声”并非中古汉语“平上去入四声”。

汉语各分支可从其声调的类别和入声的存在和消亡程度粗略的推断出其保留了何时期的古汉语音韵结构。

其中，[闽南语不仅有](../Page/闽南语.md "wikilink")
−p，−t，−k，也有模糊入声，证明闽南语历史上受到不同时期汉语音韵的多次重叠覆盖，可认为是较多的保留了上古及中古汉语音韵。
[粤语中有](../Page/粤语.md "wikilink")−p，−t，−k，部分粤语方言甚至保留了混合入声及全浊音，例如[勾漏方言](../Page/勾漏方言.md "wikilink")，还有非常完整的保留有极少见的“长入”和“短入”之分，并保留了中古汉语音韵。

[客家語與](../Page/客家語.md "wikilink")[赣语有](../Page/赣语.md "wikilink")−p，−t，−k，有入声韵尾，并保留中古汉语音韵。[吴语和](../Page/吴语.md "wikilink")[湘语都只有混合入声](../Page/湘语.md "wikilink")。

“官话方言”绝大多数次级方言都没有入声，学术界基本认定“官话方言”形成于宋元之后的明代初年\[17\]。

### 汉字

漢字是漢語書寫的最基本單元，其使用最晚始於商代，歷經甲骨文、大篆、小篆、隸書、楷書（草書、行書）諸般書體變化。[秦始皇統一中國](../Page/秦始皇.md "wikilink")，[李斯整理小篆](../Page/李斯.md "wikilink")，“書同文”的歷史從此開始。儘管漢語分支發音差異很大，但是書寫系統的統一減少語言差異造成的交流障礙。汉字的书写也不尽相同，所以出现许多[异体字](../Page/异体字.md "wikilink")，还有历朝历代规定一些[避讳的汉字书写](../Page/避讳.md "wikilink")（改字，缺笔等），但一般不影响阅读。

[東漢](../Page/東漢.md "wikilink")[許慎在](../Page/許慎.md "wikilink")《[說文解字](../Page/說文解字.md "wikilink")》中將漢字構造規律概括為“六書”：象形、指事、會意、形聲、轉注、假借。其中，象形、指事、會意、形聲四項為造字原理，是“造字法”；而轉注、假借則為用字規律，是“用字法”。

中國大陸將漢字筆劃參考異體字行書草書加以省簡，於1956年1月28日審訂通過《[簡化字總表](../Page/簡化字總表.md "wikilink")》，在中國大陸使用至今，后被马来西亚、新加坡等华人聚集地采用
\[18\]，臺灣、香港和澳門則一直使用傳統漢字（臺湾亦稱正-{}-體中文）。

### 汉语语法

汉语是一种[分析语](../Page/分析语.md "wikilink")，汉语存在用于表达时间的副词（“昨日”、“以后”）以及一些表示不同动作状态的助词。助词也用来表达问句；现代标准汉语中问句的语序与陈述句的语序相同（主—谓—宾结构），只使用末尾的语气助词，例如在普通话中的“吗”，来表达疑问语气。名词的复数形式只在代词及多音节（指人）名词中出现。

因为没有曲折变化，汉语与欧洲语言，如[罗曼语族语言相比](../Page/罗曼语族.md "wikilink")，语法看似简单。然而，汉语语法中由词序、助词等所构成的句法复杂程度却又大大地超过了以拉丁语为例的曲折性语言。加上由於日常口語經常錯誤使用，連母語者也難以判斷每一句漢語文法是否正確。例如，汉语中存在“体”用于表达不同的时间发生的动作及其状态（目前这种看法存在分歧）。如“了”或“过”常用于表示已经发生的动作，但二者存在差别：第一个是指「完成式」，表示完成某件事，例如“我做完了這項工作”，另一个却意味着「過去式」表示曾經做過某件事，并不与目前相关，例如“我做過這項工作”。汉语还有一套复杂的系统用于区分方向、可能以及动作是否成功，例如“走”及“走上来”、“打”及“打碎”、“看”及“看不懂”、“找”及“找到”。最后，现代标准汉语的名词与数词在连接时通常要求有[量词](../Page/量词.md "wikilink")。因此必须说“兩條麵包”而不是“兩麵包”。其中的“條”是一个量词。在汉语中有大量的量词，而且在现代标准汉语中每个量词都对应一定的名词使用。

此外，漢語文言文中的助詞運用非常频繁且複雜。例如:「有朋自遠方來，不亦樂乎」（[孔子](../Page/孔子.md "wikilink")）其中的「乎」便是無意義的語尾助詞，「大去之期不遠矣」的「矣」亦是。

目前，将本地[白话文](../Page/白话文.md "wikilink")，而非官話白話文作为普遍书写习惯的地区有[香港和](../Page/香港.md "wikilink")[澳门](../Page/澳门.md "wikilink")。書寫[粤语白话文在香港十分普遍](../Page/粤语白话文.md "wikilink")，[澳門社會亦受](../Page/澳門.md "wikilink")[香港粵語影響](../Page/香港粵語.md "wikilink")，部份詞彙亦為澳門通用\[19\]。香港普遍的正式学校教育使用粤语授課，而粤语又能和[官話對字的一個](../Page/官話.md "wikilink")（即每個官話單字都可對應一個粵語單字），學校都教授学生写作标准的官話白話文文章，並符合現代漢語語法，能應對普通話，這樣不论身處哪裡，習慣說那種文言的[華人都能读能懂](../Page/華人.md "wikilink")，政府、商界、民間、官方文件和正式公文往來必以官話白話文行文。香港[报章正文](../Page/报章.md "wikilink")（如头版、本地、国际、财经等）大多以[官話行文](../Page/官話.md "wikilink")，其他副刊内文，如娱乐、体育等则適度以粤语入文，可见现代汉语与「粵語入文」仍有主次之分。很多香港和澳门在非正式书写时，会使用[粤语白话文](../Page/粤语白话文.md "wikilink")，特別在香港的居民相当多媒体也会使用[粤语白话文](../Page/粤语白话文.md "wikilink")。

### 漢語詞類表

  - 实词，词汇中含有实际意义的词语
      - **名詞**：表示人或事物（包括具體事物、抽象事物、時間、處所、方位等）的名稱。
      - **動詞**：表示動作行為、發展變化、心理活動、可能意願等意義。
      - **形容詞**：表示事物的形狀、性質、狀態等。
      - **數詞**：表示數目（包括確數、概數和序數）
      - **量詞**：表示事物或動作、行為的單位。
      - **代詞**：代替人和事物，或起區別指示作用，或用來提問。

<!-- end list -->

  - 虚词，词汇中没有实际意义的词
      - **副詞**：用來修飾、限制動詞或形容詞，表示時間、頻率、範圍、語氣、程度等。
      - **介詞**：用在名詞、代詞或名詞性短語前，同這些詞或短語一起表示時間、處所、方向、對象等。
      - **連詞**：用來連接詞、短語或句子，表示前後有並列、遞進、轉折、因果、假設等關係。
      - **助詞**：用來表示詞語之間的某種結構關係，或動作行為的狀態，或表示某種語氣。
      - **嘆詞**：表示感歎、呼喚、應答等聲音。
      - **擬聲詞**：模擬人或事物發出的聲音。

### 短語結構類型表

  - 並列短語：由兩個或兩個以上的名詞，動詞或形容片語合而成，詞與詞之間是並列關係，中間常用頓號或“和、及、又、與、並”等連詞。

<!-- end list -->

  - [偏正短語](../Page/偏正短語.md "wikilink")：由名詞、動詞或形容詞與它們前頭起修飾作用的片語组合而成，其中名詞、動詞、形容詞是中心語，名詞前頭的修飾成分是定語，動詞、形容詞前頭的修飾成分是狀語。

<!-- end list -->

  - [動賓短語](../Page/動賓短語.md "wikilink")：由動詞與後面受動詞支配的成分組合而成，受動詞支配的成分是賓語。

<!-- end list -->

  - [動補短語](../Page/動補短語.md "wikilink")：由動詞或形容詞與後面起補充作用的成分組合而成，常用“得”字表示，起補充作用的成分是補語。

<!-- end list -->

  - [主謂短語](../Page/主謂短語.md "wikilink")：由表示陳述和被陳述關係的兩個成分組合而成，表示被陳述物件的是主語，用來陳述的是謂語。

### 句子成分見表\[20\]

  - [主語](../Page/主語.md "wikilink")：句子中的陳述物件，說明是誰或什麼。
  - 符號：双下划线

<!-- end list -->

  - [謂語](../Page/謂語.md "wikilink")：對句子的主語作陳述的成分，說明主語是說明或怎麼樣。
  - 符號：____

<!-- end list -->

  - [賓語](../Page/賓語.md "wikilink")：[謂語動詞的支配成分](../Page/謂語動詞.md "wikilink")，表示動作行為的物件，結果、處所、工具等。
  - 符號：波浪線

<!-- end list -->

  - [補語](../Page/補語.md "wikilink")：[謂語動詞的補充成分](../Page/謂語動詞.md "wikilink")，補充說明動作行為的情況、結果、處所、數量、時間等。
  - 符號：\< \>

<!-- end list -->

  - [定語](../Page/定語.md "wikilink")：句子中[名詞](../Page/名詞.md "wikilink")[中心語前頭的修飾成分](../Page/中心語.md "wikilink")，說明事物的性質、狀態、或限定事物的領屬、質料、數量等。
  - 符號：( )

<!-- end list -->

  - [狀語](../Page/狀語.md "wikilink")：句子中[動詞或](../Page/動詞.md "wikilink")[形容詞](../Page/形容詞.md "wikilink")[中心語前頭的修飾成分](../Page/中心語.md "wikilink")，表示動作行為的方式、狀態、時間、處所或性狀的程度等。
  - 符號：\[ \]

### 漢語語言的各級單位

語素—詞—短語（片語）—句子（單句、複句）—句群—段—篇

1.  語素是構成語言的最小單位，能獨立表達意義。分成單音節語素（例如：山、水、來、去等），和雙音節（如：蟋蟀、蚱蜢、蝴蝶等不能分開的表達意義的詞），及多音節語素（如拖拉機等外來詞語）。
2.  詞分成實詞（名詞、動詞、形容詞、代詞、數詞、量詞）和虛詞（副詞、介詞、助詞、擬聲詞、嘆詞、連詞）。
3.  短語分成主謂短語、偏正短語、動宾短語、動補短語、並列短語。
4.  句子分成單句（主謂句和非主謂句）和複句（單重複句和多重複句）。
    1.  主謂句是由主謂短語構成的。根據謂語的不同構成情況，主謂句分為：
        1.  動詞謂語句（由動詞和動詞性短語做謂語，如：“你改悔罢！”（鲁迅《藤野先生》）“我就往仙台的醫學專修學校去。”（鲁迅《藤野先生》））
        2.  形容詞謂語句（由形容詞或形容詞性短語做謂語，如：“我母親的氣量大。”（胡适《我的母亲》）“我母親心裡又氣又急。”（胡适《我的母亲》））
        3.  名詞謂語句（由名詞或名詞性短語做謂語，如：“每人一盞燈籠。”（胡适《我的母亲》））
        4.  主謂謂語句（由主謂短語做謂語，如：“我的講義，你能抄下來麼？”（鲁迅《藤野先生》））
    2.  非主謂句是由單個詞或主謂短語以外的短語構成的。非主謂句分為：
        1.  動詞非主謂句（由動詞或動詞性短語構成的，如：“站住！”）
        2.  形容詞非主謂句（由名詞或名詞性短語構成的，如：“好！”“實在標緻極了。”（鲁迅《藤野先生》））
        3.  名詞非主謂短語（由名詞或名詞性短語構成的，如：“飛機！”“多美的景色呀！”）
    3.  複句是由兩個或兩個以上的單句構成的，根據分句之間的關係可分為：並列關係複句、遞進關係複句、選擇關係複句、轉折關係複句、假設關係複句和條件關係複句。

### 音节速率、信息密度、阅读效率

因[聲調辨義](../Page/聲調.md "wikilink")，汉语的音节效率比较高，单个音节的信息密度比较大。因此音节速率不用太高。\[21\]从世界范围来看，汉语和[英语是低音节速率](../Page/英语.md "wikilink")、高音节信息密度的語言，[日语和](../Page/日语.md "wikilink")[西班牙语则是高音节速率](../Page/西班牙语.md "wikilink")、低音节信息密度的語言。\[22\]如果将汉语延深入汉文，则汉文的信息密度更大。文言的汉文其信息密度在全世界无出其右者。因此汉文的阅读效率相当惊人。\[23\]

## 語言影響力

[Hanzi_Wenhuaquan.png](https://zh.wikipedia.org/wiki/File:Hanzi_Wenhuaquan.png "fig:Hanzi_Wenhuaquan.png")的位置
**綠色**：完全使用漢字的地區
**深綠色**：位於圈內，現在部分使用漢字的地區
**淺綠色**：位於圈內但仍然主要或同時使用其他文字的地區
**黃色**：過去漢字文化圈曾涉及到的地區\]\]

### 对其它语言的影响

漢語也曾对其周边的国家的語言文字产生过重要影响，如[日語](../Page/日語.md "wikilink")、[朝鲜語](../Page/朝鲜語.md "wikilink")、[越南語中都保留有大量的漢語借词以及漢語书写体系](../Page/越南語.md "wikilink")，在[泰语](../Page/泰语.md "wikilink")、[高棉语](../Page/高棉语.md "wikilink")、[马来语中也有一些汉语](../Page/马来语.md "wikilink")（南方語言）借词。在新词汇的产生过程中，亦对少数民族语言产生影响。如[手机](../Page/手机.md "wikilink")、[信号等词被](../Page/信号_\(电信\).md "wikilink")[维吾尔语](../Page/维吾尔语.md "wikilink")、[苗语等少数民族语言借用](../Page/苗语.md "wikilink")。

### 受其他語言的影響

在古代，随着佛教的传入，[梵文对汉语的词汇产生过较小影响](../Page/梵文.md "wikilink")；近代特别是[五四运动以后](../Page/五四运动.md "wikilink")，[和製漢語](../Page/和製漢語.md "wikilink")、[俄語](../Page/俄語.md "wikilink")、[英語詞彙大量傳入](../Page/英語.md "wikilink")，語法也日漸受到英語等歐洲語言影響，形成了所谓[歐化中文现象](../Page/歐化中文.md "wikilink")，这既部分适应了当代语言使用的需要，同时历来也招致民间和学术界不少尖锐的批评。目前汉语仍不断受到全球各种语言的复杂影响。汉语一脉相承，汉字汉化了所有的外来族群，超越語言差異的汉字统一了中国（除了有[粵語字的](../Page/粵語字.md "wikilink")[粵港澳](../Page/粵港澳.md "wikilink")），拼音字则不然。\[24\]\[25\]\[26\]\[27\]

## 閱讀更多

  - [汉语族](../Page/汉语族.md "wikilink")
  - [汉语作为外语教学](../Page/汉语作为外语教学.md "wikilink")

## 註解

<references group="註"/>

## 參考資料

### 參考字典、词典

  - 《[汉语大词典](../Page/汉语大词典.md "wikilink")》
  - [新華字典](../Page/新華字典.md "wikilink")
  - [現代漢語詞典](../Page/現代漢語詞典.md "wikilink")
  - [國語大字典](../Page/國語大字典.md "wikilink")
  - [BitEx中文（在线汉语词典）](http://bitex-cn.com)
  - [汉典（在线汉语字典）](http://www.zdic.net)
  - [象形字典（在线汉语字典）](http://www.vividict.com/)

### 英文資料

  - Hannas, William. C. 1997. Asia's Orthographic Dilemma. University of
    Hawaii Press. ISBN 978-0-8248-1892-0 (paperback); ISBN
    978-0-8248-1842-5 (hardcover)
  - [DeFrancis, John](../Page/德范克.md "wikilink"). 1990. The Chinese
    Language: Fact and Fantasy. Honolulu: University of Hawaii Press.
    ISBN 978-0-8248-1068-9

## 外部链接

  - [Online College of Chinese Language
    汉语远程学院教学网](https://web.archive.org/web/20051014131717/http://www.hanyu.com.cn/)（中国国家汉办、华东师大合办）
  - [Cantonese Help Sheets](http://www.cantonese.sheik.co.uk/) 粤语教程
  - 顾明栋：〈[走出语音中心主义——对汉民族文字性质的哲学思考](http://www.nssd.org/articles/article_read.aspx?id=664884356)〉。
  - 顾明栋：〈[西方语言哲学理论是普适性的吗？——中西关于汉语汉字悬而未决的争论](http://www.nssd.org/articles/Article_Read.aspx?id=47975849)〉。

{{-}}

[汉语](../Category/汉语.md "wikilink")
[Category:漢藏語系](../Category/漢藏語系.md "wikilink")
[Category:中国语言](../Category/中国语言.md "wikilink")
[Category:语言之最](../Category/语言之最.md "wikilink")
[Category:聲調語言](../Category/聲調語言.md "wikilink")
[Category:孤立语](../Category/孤立语.md "wikilink")

1.  參見[唐話番字初學](../Page/唐話番字初學.md "wikilink")
2.
3.
4.  /
5.
6.  《中華人民共和國澳門特別行政區基本法》
7.  ，本文一律以「漢語」來表示，國際間常稱**中文**
8.
9.
10. 《毛詩古音攷自序》
11. 《魯國堯語言學論文集·客、贛、通泰方言源於南朝通語說》，123\~135頁，魯國堯，江蘇教育出版社，2003年，ISBN
    7534354994
12.
13.
14.
15.
16. 《湖南省的汉语方言》
17.
18.
19. 由於澳門長期受到香港文化影響，而且香港與澳門關係及交流密切，而且兩地文化相近，所以香港的用語絕大部份都通用於澳門
20.
21.
22.
23.
24. 语言定理：1786年威廉·琼斯爵士于提出，某些语言有着词汇、语法、构词法和语音使用上的相似性，因此它们必定是来自單一祖语。现在流行的学术观念认为，…中国人也由非洲经中东印度越南经海上二三万年前到达，一二万年前已经遍布大陆，并越过白令海峡进入美洲。所以印度语言数量比中国的丰富，中国南方语言数量比北方的丰富。…这可以解释汉藏语系同源，汉语變體同源。[语言进化论用树状图来理解](../Page/语言进化论.md "wikilink")、定位各语言的关系。
25. 隔离下：语言进化论又告诉我们，随着时间的流逝，那些成为分隔的词形就会越来越彼此趋异，从而在词汇中出现新词。如果从未发生过语言替换，则趋异就是语言变化的主要原因，而语言地图就会呈现出语言小单元的镶嵌图的形式。这种镶嵌形式在澳大利亚北部的士著语言中很显著（Colin
    Renfrew《世界的语言多样性》）。…这可以解释中国南方山区方言的格局，也可解释华夏文明初期，中华大地方国部落有如群星灿烂。
26. 交流下：会产生所谓[语言联盟](../Page/语言联盟.md "wikilink")，即同一地区或毗邻地区通行的数种没有亲属关系的语言，由于长期接触，互相影响而获得语法、语音等方面的许多相似或共同特征。甚至音位和形态系统的相互渗透并无界限可言。如不同语系的数种巴尔干国家的语言，由于拜占庭文化在这个地区显示出的较强凝聚力和各语言间的相互影响、渗透，在长达几个世纪的接触中，不仅表现在词汇平面上，也表现在音位和形态、句法平面上，获得了被世人称为
    "巴尔干语言特点"
    的许多共同特征。…这可以解释汉字汉文化同质化了中国语言，同类化了日韩越语言。即使假定一些語言原本不是汉语也会汉化，如一种观念认为，客家话是选择了汉字才成为汉语的。
27. 语言替换：例如，奥塔戈大学的
    C．F．W．Higham提出，（1）农业扩展带来语言替换的原因，…汉藏语系诸语言的扩展看来最初也是同黄河流域的粟和其它谷类植物的驯化有关，只是在更晚的时候才同稻的驯化有关。（2）在高度等级化的社会中，入侵的少数民族控制了权力杠杆并且以贵族自居，它赋予了它的语言以显赫的威望，以致诱导本地人宁愿采用征服者的语言而不愿使用其母语。在华南，…汉语仅仅在历史时期由于秦帝国的军事扩展才开始使用。阿尔泰语言，特别是突厥诸语言，是在更晚的时候由骑在马背上的游牧部族的精英统治带到遥远的地方的（Colin
    Renfrew《世界的语言多样性》）。…