**HMV**（）是一間[連鎖](../Page/連鎖店.md "wikilink")[唱片店](../Page/唱片.md "wikilink")，源自[英國](../Page/英國.md "wikilink")，在世界多個國家設有分店。HMV原名（牠的主人的聲音），HMV是簡稱，來自它的「小狗聽留聲機」商標。HMV原本是生產留聲機，及發行音樂唱片，後來不再生產留聲機，改為唱片零售。

## 歷史

首間HMV唱片店於1921年在[倫敦開業](../Page/倫敦.md "wikilink")。現時，HMV分店遍佈多個國家，包括[英國](../Page/英國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[日本](../Page/日本.md "wikilink")、[香港](../Page/香港.md "wikilink")、[新加坡等地](../Page/新加坡.md "wikilink")。

因為不敵網絡影音娛樂的普及，業務無法扭轉劣勢，再加上未能尋求資金持續營運，於2013年1月15日，HMV母公司委任[德勤會計師事務所為](../Page/德勤會計師事務所.md "wikilink")[破產管理人](../Page/破產.md "wikilink")\[1\]，接管英國及[愛爾蘭業務](../Page/愛爾蘭.md "wikilink")，並尋求清盤或賣盤。

HMV於2013年4月被希爾科公司（Hilco）收購。HMV香港、新加坡的全數股權及中國、台灣及澳門的hmv經營權由私募基金[匯友資本](../Page/匯友資本.md "wikilink")（AID
Partners）收購。

## 香港HMV

[Hmv_causeway_bay_flagship_store_201511.jpg](https://zh.wikipedia.org/wiki/File:Hmv_causeway_bay_flagship_store_201511.jpg "fig:Hmv_causeway_bay_flagship_store_201511.jpg")

HMV在1994年登陸香港，首間分店設於[銅鑼灣](../Page/銅鑼灣.md "wikilink")[皇室堡地庫](../Page/皇室堡.md "wikilink")（現時蘇寧電器的位置，百佳超級市場對面），其後於2006年遷往對面的柏寧酒店。而位於[北京道與](../Page/北京道.md "wikilink")[漢口道交界全座共](../Page/漢口道.md "wikilink")4層、面積約3萬呎的尖沙咀分店於1995年開業，曾是全港最大唱片店。\[2\]自2006年初搬至漢口道後，面積縮減至兩層但仍設有[爵士和](../Page/爵士音樂.md "wikilink")[經典音樂專區](../Page/古典音樂.md "wikilink")。由於舖租不斷上升，於2011年3月17日尖沙咀分店搬至[iSQUARE](../Page/iSQUARE.md "wikilink")，面積縮減至一層。HMV曾於1996年於[沙田](../Page/沙田區.md "wikilink")[新城市廣場](../Page/新城市廣場.md "wikilink")4樓開設分店，該分店在2000年縮細。到2005年4月因商場翻新而關閉。另外HMV亦曾於[金鐘](../Page/金鐘.md "wikilink")[金鐘廊開設分店](../Page/金鐘廊.md "wikilink")，該分店其後受商場店舖易手進行重組而結業。

HMV曾經有中文名「音樂無限」，於2000年代在香港全線分店播放[香港商業電台的英語頻道](../Page/香港商業電台.md "wikilink")[豁達864](../Page/豁達864.md "wikilink")，並曾將該頻道冠名為「HMV864」。

目前HMV在香港設有5間分店，包括[中環](../Page/中環.md "wikilink")[萬年大廈](../Page/萬年大廈.md "wikilink")、[銅鑼灣](../Page/銅鑼灣.md "wikilink")[翡翠明珠廣場](../Page/翡翠明珠廣場.md "wikilink")、[旺角](../Page/旺角.md "wikilink")[The
Forest](../Page/SKYPARK.md "wikilink")、[尖沙咀](../Page/尖沙咀.md "wikilink")[iSquare及](../Page/iSquare.md "wikilink")[九龍灣](../Page/九龍灣.md "wikilink")[德福廣場](../Page/德福廣場.md "wikilink")；並於[紅磡](../Page/紅磡.md "wikilink")[黃埔新天地開設hmv](../Page/黃埔新天地.md "wikilink")
kids兒童學習中心。

匯友資本2013年2月28日宣布，以5,680萬元全資收購HMV在香港、新加坡的全部業務以及中國內地、台灣及澳門的永久經營權。其後將唱片影碟店轉型成為多元化生活概念店，並加入咖啡店、小型書店、時裝、黑膠碟銷售及兒童閱讀區等新元素。中環[娛樂行總店](../Page/娛樂行.md "wikilink")，在2014年8月完成翻新後以「HMVideal」品牌重新登場，到2016年結業。\[3\]

原[銅鑼灣柏寧酒店](../Page/銅鑼灣.md "wikilink")[潮流觸覺分店已於](../Page/潮流觸覺.md "wikilink")2015年8月6日起關閉，到同年11月在明珠廣場2樓至4樓設HMV香港旗艦店，以「音樂。時尚。餐飲」三大主題，佔地接近四萬平方尺。而原[黃埔新天地分店亦已於](../Page/黃埔新天地.md "wikilink")2015年11月22日起關閉。

2016年3月21日，[中國3D數碼娛樂有限公司收購HMV香港](../Page/HMV數碼中國集團.md "wikilink")，並位於香港[銅鑼灣旗艦店舉行](../Page/銅鑼灣.md "wikilink")「本土文化，國際視野」併購記者會\[4\]。

不過到2018年11月起，HMV香港[德福廣場分店遭港鐵入禀法庭追討欠款](../Page/德福廣場.md "wikilink")。到12月，中環萬年大廈和銅鑼灣珠城大廈的2家分店分別拖欠業主143萬和342萬，並要求HMV交還舖位。\[5\]環球唱片有限公司亦入稟區域法院，追討50.3萬元貨款餘款及違反協議的賠償。\[6\]

2018年12月18日，HMV數碼中國公布旗下HMV零售因缺乏償債能力，決定自願清盤和即日遣散逾70名員工。而店鋪未有營業，翌日，HMV香港[德福廣場分店舖位懸空](../Page/德福廣場.md "wikilink")。\[7\]

## 圖片

[File:hmv.jpg|標誌](File:hmv.jpg%7C標誌) <File:HMV> - Oxford Street
1.jpg|位於[英國](../Page/英國.md "wikilink")[倫敦的HMV分店](../Page/倫敦.md "wikilink")
<File:HMV> Oxford Street by Alex Liivet.jpg|HMV 英國倫敦牛津街分店於2013年11月以新形象重開
<File:HMV> Central Store Upper floor view
201501.jpg|往時位於[中環](../Page/中環.md "wikilink")[中建大廈的HMV分店](../Page/中建大廈.md "wikilink")，於2014年已遷往至[娛樂行](../Page/娛樂行.md "wikilink")，並以新品牌
HMVideal開設，佔地約12,500平方呎。 <File:HKElementsHMV>
20071026.jpg|位於[西九龍](../Page/西九龍.md "wikilink")[圓方的HMV分店](../Page/圓方.md "wikilink")，現已遷往場內另一個舖位
<File:HMV> in iSquare Hong Kong on
2018-12-17.jpg|位於[尖沙咀](../Page/尖沙咀.md "wikilink")[iSquare的HMV分店](../Page/iSquare.md "wikilink")，攝於其最後一個營業日（2018年12月17日）

## 參考

## 外部連結

  - [HMV英國](http://www.hmv.co.uk/)

  - [HMV香港](http://www.hmv.com.hk/)

  - [HMV日本](https://www.hmv.co.jp)

  -
  -
[Category:1921年成立的公司](../Category/1921年成立的公司.md "wikilink")
[Category:日本音樂零售商](../Category/日本音樂零售商.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:英国唱片公司](../Category/英国唱片公司.md "wikilink")
[Category:音樂零售商](../Category/音樂零售商.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")

1.
2.
3.  [HMV變身多元生活概念店 《資本企業家》
    第118期 2014年9月11日](http://www.capitalentrepreneur.com/common_emag/view_content.php?issue_no=118&pub_id=23&top_id=153&art_id=195939)
4.  [中國3D數碼與HMV合併　張智霖喜見公司發展強大](http://www.china3d8078.com/news/detail.html?news_id=25)
5.
6.
7.