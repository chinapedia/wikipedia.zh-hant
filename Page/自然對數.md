[Mplwp_ln.svg](https://zh.wikipedia.org/wiki/File:Mplwp_ln.svg "fig:Mplwp_ln.svg")。\]\]
[Log-def.svg](https://zh.wikipedia.org/wiki/File:Log-def.svg "fig:Log-def.svg")

**自然对数**（）為以数学常数 *[e](../Page/e_\(数学常数\).md "wikilink")*
為[底數的](../Page/底數_\(對數\).md "wikilink")[对数函数](../Page/对数函数.md "wikilink")，標記作\(\ln x\)或\(\log_e x\)，其[反函数為](../Page/反函数.md "wikilink")[指數函數](../Page/指數函數.md "wikilink")\(e^x\)。

自然对数定義為：對任何正[實數](../Page/實數.md "wikilink")\(x\)，由 \(1\) 到 \(x\) 所圍成，
\(xy=1\) [曲線下的面積](../Page/積分.md "wikilink") 。如果\(x\)小於1，則計算面積為負數。

\[\ln x = \int_1^x \frac{dt}{t}\,\]

\(e\) 則定義為唯一的實數 \(x\) 使得 \(\ln x = 1\) 。

## 数学表示方法

自然对数的一般表示方法為 \(\ln x\!\) ，數學中亦有以 \(\log x\!\) 表示自然對數。 \[1\]
若要避免與底為10的[常用對數](../Page/常用對數.md "wikilink")
\(\log x\!\) 混淆，可用「全寫」 \(log_{\boldsymbol{e}} x\) 。

## 歷史

### 十七世纪

[Hyperbolic_sector.svg](https://zh.wikipedia.org/wiki/File:Hyperbolic_sector.svg "fig:Hyperbolic_sector.svg")是[笛卡爾平面](../Page/笛卡爾平面.md "wikilink")\(\{(x,y)\}\)上的一個區域，由從原點到\((a,\frac{1}{a})\)和\((b,\frac{1}{b})\)的射線，以及[雙曲線](../Page/雙曲線.md "wikilink")\(xy=1\)圍成。在標準位置的雙曲線扇形有\(a=1\)且\(b>1\)，它的面積為\(\ln(b)\)\[2\]，此時雙曲線扇形對應正[雙曲角](../Page/雙曲角.md "wikilink")。\]\]
[Gregoire_de_St_Vincent_Quadrature.svg](https://zh.wikipedia.org/wiki/File:Gregoire_de_St_Vincent_Quadrature.svg "fig:Gregoire_de_St_Vincent_Quadrature.svg")，\(\frac{x_2}{x_1}=\frac{x_1}{x_0}=k\)，\(y\)的值也呈等比數列，\(\frac{x_2}{x_1}=\frac{x_1}{x_0}=\frac{1}{k}\)。\]\]
[約翰·納皮爾在](../Page/約翰·納皮爾.md "wikilink")1614年\[3\]以及[约斯特·比尔吉在](../Page/约斯特·比尔吉.md "wikilink")6年後\[4\]，分別發表了獨立編制的[對數表](../Page/對數表.md "wikilink")，當時通過對接近1的底數的大量乘[冪運算](../Page/冪.md "wikilink")，來找到指定範圍和精度的[對數和所對應的真數](../Page/對數.md "wikilink")，當時還沒出現有理數冪的概念。按後世的觀點，约斯特·比尔吉的底數1.0001<sup>10000</sup>相當接近自然對數的底數\(e\)，而[約翰·納皮爾的底數](../Page/約翰·納皮爾.md "wikilink")0.9999999<sup>10000000</sup>相當接近\(\frac{1}{e}\)\[5\]。實際上不需要做開高次方這種艱難運算，[約翰·納皮爾用了](../Page/約翰·納皮爾.md "wikilink")20年時間進行相當於數百萬次乘法的計算，建議納皮爾改用10為底數未果，他用自己的方法\[6\]於1624年部份完成了[常用對數表的編制](../Page/常用對數.md "wikilink")。

形如\(f(x)=x^p\)的曲線都有一個代數[反導數](../Page/反導數.md "wikilink")，除了特殊情況\(p=-1\)對應於雙曲線的，即[雙曲線扇形](../Page/雙曲線扇形.md "wikilink")；其他情況都由1635年發表的給出<ref>[博納文圖拉·卡瓦列里在](../Page/博納文圖拉·卡瓦列里.md "wikilink")1635年的《Geometria
indivisibilibus continuorum nova quadam ratione
promota》中給出[定積分](../Page/定積分.md "wikilink")：

\[\int_0^a x^n\,dx = \tfrac{1}{n+1}\, a^{n+1} \qquad n \geq 0,\]
其[不定積分形式為](../Page/不定積分.md "wikilink")：

\[\int x^n\,dx = \tfrac{1}{n+1}\, x^{n+1} + C \qquad n \neq -1.\]
獨立發現者還有：[皮埃爾·德·費馬](../Page/皮埃爾·德·費馬.md "wikilink")、和[埃萬傑利斯塔·托里拆利](../Page/埃萬傑利斯塔·托里拆利.md "wikilink")。</ref>，其中拋物線的弓形面積由公元前3世紀的[阿基米德完成](../Page/阿基米德.md "wikilink")（），雙曲線的弓形面積需要發明一個新函數。1647年將對數聯繫於雙曲線\(xy=1\)的弓形面積，他發現x軸上\([a,b]\)兩點對應的雙曲線線段與原點圍成的[雙曲線扇形同](../Page/雙曲線扇形.md "wikilink")\([c,d]\)對應的扇形，在\(\frac{a}{b}=\frac{c}{d}\)時面積相同，這指出了雙曲線從\(x=1\)到\(x=t\)的積分\(f(t)\)滿足\[7\]：

\[f(tu) = f(t) + f(u).\,\]
1649年，將雙曲線下的面積解釋為對數。大約1665年，[伊薩克·牛頓推廣了](../Page/伊薩克·牛頓.md "wikilink")[二項式定理](../Page/二項式定理.md "wikilink")，他將\(\frac{1}{1+x}\)展開並逐項積分，得到了自然對數的無窮級數。“自然對數”最早描述見於[尼古拉斯·麥卡托在](../Page/尼古拉斯·麥卡托.md "wikilink")1668年出版的著作《Logarithmotechnia》中\[8\]，他也獨立發現了同樣的級數，即自然對數的[麥卡托級數](../Page/麥卡托級數.md "wikilink")。

### 十八世纪

大約1730年，[歐拉定義互為逆函數的](../Page/歐拉.md "wikilink")[指數函數和自然對數為](../Page/指數函數.md "wikilink")<ref>
卡瓦列里弓形面積公式，對於負數值的*n*(*x*的負數冪)，由於在*x* = 0處有個[奇點](../Page/奇點_\(數學\).md "wikilink")，因此定積分的下限為1，而不是0，即為：

\[\int_1^a x^n\,dx = \tfrac{1}{n+1} (a^{n+1} - 1) \qquad n \neq -1.\]
[歐拉的自然對數定義](../Page/歐拉.md "wikilink")：

\[\begin{align}
\ln(x)  &=\lim_{n \rightarrow \infty} n(x^{1/n} - 1) \\
&=\lim_{n \rightarrow -1} \tfrac{1}{n+1} (x^{n+1} - 1). \\
\end{align}\] </ref>\[9\]：

\[e^x = \lim_{n \rightarrow \infty} \left(1+\frac{x}{n}\right)^n,\]

\[\ln(x) = \lim_{n \rightarrow \infty} n\left(x^{\frac{1}{n}} - 1\right)\]
1742年[威廉·琼斯發表了現在的](../Page/威廉·琼斯_\(数学家\).md "wikilink")[冪](../Page/冪.md "wikilink")[指數概念](../Page/指數.md "wikilink")\[10\]。

## 形式定義

[歐拉定義自然對數為](../Page/歐拉.md "wikilink")[序列的極限](../Page/序列的極限.md "wikilink")：

\[\ln(x) = \lim_{n \rightarrow \infty} n\left(x^{\frac{1}{n}} - 1\right).\]

\(\ln (a)\)正式定義為[積分](../Page/積分.md "wikilink")，

\[\ln(a)=\int_1^a \frac{1}{x}\,dx.\]

這個函數為對數是因滿足[對數的基本性質](../Page/對數.md "wikilink"):

\[\ln(ab)=\ln(a)+\ln(b). \,\!\]

這可以通過將定義了\(\ln(ab)\)的積分拆分為兩部份，並在第二部份中進行[換元](../Page/換元積分法.md "wikilink")\(x=ta\)來證實:

\[\ln (ab)
= \int_1^{ab} \frac{1}{x} \; dx
= \int_1^a \frac{1}{x} \; dx \; + \int_a^{ab} \frac{1}{x} \; dx
=\int_1^{a} \frac{1}{x} \; dx \; + \int_1^{b} \frac{1}{at} \; d(at)\]

\[=\int_1^{a} \frac{1}{x} \; dx \; + \int_1^{b} \frac{1}{t} \; dt
= \ln (a) + \ln (b).\]

冪公式\(\ln (t')=r\ln (t)\)可如下推出:

\[\ln(t^r) = \int_1^{t^r} \frac{1}{x}dx = \int_1^t \frac{1}{u^r} d\left(u^{r}\right) = \int_1^t \frac{1}{u^r} \left(ru^{r - 1} \, du\right) = r \int_1^t \frac{1}{u} \, du = r \ln(t).\]
第二個等式使用了[換元](../Page/換元積分法.md "wikilink")\(u=x^{\frac{1}{r}}\)。

自然對數還有在某些情況下更有用的另一個積分表示：

\[\ln(x) = -\lim_{\epsilon \to 0} \int_\epsilon^\infty \frac{dt}{t}\left( e^{-xt} - e^{-t} \right).\]

## 性質

  - \(\ln(1) =\int_1^1 \frac{1}{t}\,dt=0 \,\)
  - \(\ln(-1) = i \pi \,\)

<!-- end list -->

  -

      -
        (參見[複數對數](../Page/複數對數.md "wikilink"))

<!-- end list -->

  - \(\ln(x) < \ln(y) \quad{\rm for}\quad 0 < x < y \,\)
  - \(\lim_{x \to 0} \frac{\ln(1+x)}{x} = 1 \,\)
  - \(\ln(x^y) = y \, \ln(x) \,\)
  - \(\frac{x-1}{x} \leq \ln(x) \leq x-1 \quad{\rm for}\quad x > 0 \,\)
  - \(\ln{( 1+x^\alpha )} \leq \alpha x \quad{\rm for}\quad x \ge 0, \alpha \ge 1 \,\)

<!-- end list -->

  -
    {| class="toccolours collapsible collapsed" width="90%"
    style="text-align:left"

\!證明 |- |
\(\lim_{h \to 0} \frac{\ln(1+h)}{h} = \lim_{h \to 0} \frac{\ln(1+h)-\ln 1}{h} = \frac{d}{dx} \ln x \Bigg|_{x=1} = 1\)
|}

## 導數

[Logarithm_derivative.svg](https://zh.wikipedia.org/wiki/File:Logarithm_derivative.svg "fig:Logarithm_derivative.svg")
[LogTay.svg](https://zh.wikipedia.org/wiki/File:LogTay.svg "fig:LogTay.svg")

自然對數的[導數為](../Page/導數.md "wikilink")

\[\frac{d}{dx} \ln(x) = \frac{1}{x}.\,\]

證明一
（微積分第一基本定理）\[\frac{d}{dx} \ln(x)=\frac{d}{dx} \int_1^x \frac{1}{t}\,dt = \frac{1}{x}\]

證明二:
[按此影片](https://www.youtube.com/watch?v=yUpDRpkUhf4&list=PL19E79A0638C8D449&index=28)

\[\frac{d}{dx} \ln (x) =\lim_{h \to 0} \frac{\ln(x+h)-\ln(x)}{h}\]

\[=\lim_{h \to 0} \frac{\ln(\frac{x+h}{x})}{h}\]

\[=\lim_{h \to 0} \left[ \frac{1}{h} \ln\left( 1 + \frac{h}{x} \right)\right]\quad\]

\[= \lim_{h \to 0} \ln\left( 1 + \frac{h}{x} \right)^\frac{1}{h}\]

设 \(u=\frac{h}{x} \Rightarrow ux=h\)

\[\frac{1}{h}=\frac{1}{ux}\]

\[\frac{d}{dx} \ln (x)=\lim_{u \to 0} \ln (1+u)^\frac {1}{ux}\]

\[=\lim_{u \to 0}\ln\left[(1+u)^\frac{1}{u} \right]^\frac{1}{x}\]

\[=\frac{1}{x} \lim_{u \to 0} \ln(1+u)^\frac {1}{u}\]

设 \(n=\frac{1}{u} \Rightarrow u=\frac{1}{n}\)

\[\frac{d}{dx} \ln(x) = \frac{1}{x} \lim_{n \to \infty} \ln \left( 1 + \frac{1}{n} \right)^n\]

\[=\frac{1}{x} \ln \left [ \lim_{n \to \infty}\left( 1 + \frac{1}{n} \right)^n \right ]\]

\[=\frac{1}{x} \ln e\]

\[=\frac {1}{x}\]

用自然對數定義的更一般的對數函數，\(\log_b(x)=\frac{\ln(x)}{\ln(b)}\)，根據其[逆函數即一般](../Page/逆函數.md "wikilink")[指數函數的性質](../Page/指數函數.md "wikilink")，它的導數為\[11\]\[12\]：

  -
    \(\frac{d}{dx} \log_b(x) = \frac{1}{x\ln(b)}.\)

根據[鏈式法則](../Page/鏈式法則.md "wikilink")，以\(f(x)\)為參數的自然對數的導數為

\[\frac{d}{dx} \ln[f(x)] = \frac{f'(x)}{f(x)}.\]
右手端的商叫做\(f\)的，通過\(\ln(f(x))\)的導數的方法計算\(f'(x)\)叫做[對數微分](../Page/對數微分.md "wikilink")\[13\]。

## 冪級數

自然對數的導數性質導致了\(\ln(1+x)\)在0處的[泰勒級數](../Page/泰勒級數.md "wikilink")，也叫做[麥卡托級數](../Page/麥卡托級數.md "wikilink")：

\[\ln(1+x)=\sum_{n=1}^\infty \frac{(-1)^{n+1}}{n} x^n = x - \frac{x^2}{2} + \frac{x^3}{3} - \cdots\]

  -

      -
        對於所有\(\left|x\right| \leq 1,\) 但不包括\(x = -1.\)

把\(x-1\)代入\(x\)中，可得到\(\ln(x)\)自身的級數。通過在麥卡托級數上使用[歐拉變換](../Page/二項式變換.md "wikilink")，可以得到對絕對值大於1的任何\(x\)有效的如下級數:

\[\ln{x \over {x-1}} = \sum_{n=1}^\infty {1 \over {n x^n}} = {1 \over x}+ {1 \over {2x^2}} + {1 \over {3x^3}} + \cdots \,.\]
這個級數類似於[贝利-波尔温-普劳夫公式](../Page/贝利-波尔温-普劳夫公式.md "wikilink")。

還要注意到\(x \over {x-1}\)是自身的逆函數，所以要生成特定數\(y\)的自然對數，簡單把\(x \over {x-1}\)代入*\(x\)*中。

\[\ln{x} = \sum_{n=1}^\infty {1 \over {n}} \left( {x - 1 \over x} \right)^n = \left( {x - 1 \over x} \right) + {1 \over 2} \left( {x - 1 \over x} \right)^2 + {1 \over 3} \left( {x - 1 \over x} \right)^3 + \cdots \,\]

  -

      -
        對於\(\operatorname{Re} (x) \geq \frac12 \,.\)

自然數的倒數的總和

\[1 + \frac 1 2 + \frac 1 3 + \cdots + \frac 1 n = \sum_{k=1}^n \frac{1}{k},\]
叫做[調和級數](../Page/調和級數.md "wikilink")。它與自然對數有密切聯繫：當\(n\)趨於無窮的時候，差

\[\sum_{k=1}^n \frac{1}{k} - \ln(n),\]
[收斂於](../Page/序列的極限.md "wikilink")[欧拉-马歇罗尼常数](../Page/欧拉-马歇罗尼常数.md "wikilink")。這個關係有助於分析算法比如[快速排序的性能](../Page/快速排序.md "wikilink")。\[14\]

## 積分

自然對數通過[分部積分法積分](../Page/分部積分法.md "wikilink"):

\[\int \ln (x) \,dx = x \ln (x)  - x + C.\]

假設:

\[u = \ln(x) \Rightarrow du = \frac{dx}{x}\]

\[dv = dx  \Rightarrow v = x\,\]

所以:

  -
    <math>

\\begin{align} \\int \\ln (x) \\,dx & = x \\ln (x) - \\int \\frac{x}{x}
\\,dx \\\\ & = x \\ln (x) - \\int 1 \\,dx \\\\ & = x \\ln (x) - x + C
\\end{align} </math>

自然對數可以簡化形如\(g(x)=\frac{f'(x)}{f(x)}\)的函數的積分：\(g(x)\)的一個[原函數給出為](../Page/原函數.md "wikilink")\(\ln(\left \vert f(x) \right \vert)\)。這是基於[鏈式法則和如下事實](../Page/鏈式法則.md "wikilink"):

\[\ {d \over dx}\ln \left| x \right| = {1 \over x}.\]

換句話說，

\[\int { 1 \over x} dx = \ln|x| + C\] 且

\[\int { \frac{f'(x)}{f(x)}\, dx} = \ln |f(x)| + C.\]

### 例子

下面是\(g(x)=\tan x\)的例子：

\[\begin{align}
\int \tan x \,dx &= \int {\sin x \over \cos x} \,dx \\
&= \int {-{d \over dx} \cos x \over {\cos x}} \,dx. \\
\end{align}\]

設\(f(x)=\cos x\)且\(f'(x)=-\sin x\)：

\[\begin{align}\int \tan x \,dx &= -\ln{\left| \cos x \right|} + C \\
&= \ln{\left| \sec x \right|} + C \\
\end{align}\]

## 與雙曲函數的關係

[Cartesian_hyperbolic_triangle.svg](https://zh.wikipedia.org/wiki/File:Cartesian_hyperbolic_triangle.svg "fig:Cartesian_hyperbolic_triangle.svg")(方程\(y=\frac{1}{x}\))下，雙曲線三角形(黃色)，和對應於[雙曲角](../Page/雙曲角.md "wikilink")*\(u\)*的[雙曲線扇形](../Page/雙曲線扇形.md "wikilink")(紅色)。這個三角形的邊分別是[雙曲函數中](../Page/雙曲函數.md "wikilink")\(\cosh\)和\(\sinh\)的\(\sqrt{2}\)倍。\]\]
[Hyperbolic_functions-2.svg](https://zh.wikipedia.org/wiki/File:Hyperbolic_functions-2.svg "fig:Hyperbolic_functions-2.svg")\(\scriptstyle x^2\ -\ y^2\ =\ 1\)於點\(\scriptstyle (\cosh\,a,\,\sinh\,a)\)，這裡的\(\scriptstyle a\)是射線、雙曲線和\(\scriptstyle x\)軸圍成的面積的二倍。對於雙曲線上位於x軸下方的點，這個面積被認為是負值。\]\]
在18世紀，[約翰·海因里希·蘭伯特介入](../Page/約翰·海因里希·蘭伯特.md "wikilink")[雙曲函數](../Page/雙曲函數.md "wikilink")\[15\]，並計算[雙曲幾何中](../Page/雙曲幾何.md "wikilink")[雙曲三角形的面積](../Page/雙曲三角形.md "wikilink")\[16\]。對數函數是在[直角雙曲線](../Page/直角雙曲線.md "wikilink")\(xy=1\)下定義的，可構造雙曲線直角三角形，底邊在線\(y=x\)上，一個頂點是原點，另一個頂點在雙曲線。這裡以自然對數即雙曲角作為參數的函數，是自然對數的逆函數[指數函數](../Page/指數函數.md "wikilink")，即要形成指定[雙曲角](../Page/雙曲角.md "wikilink")\(u\)，在漸近線即x或y軸上需要有的\(x\)或\(y\)的值。顯見這裡的底邊是\(\left(e^u + e^{ -u}\right) \frac{\sqrt{2}}{2}\)，垂線是\(\left(e^u - e^{-u}\right) \frac{\sqrt{2}}{2}\)。

通過旋轉和縮小[線性變換](../Page/線性變換.md "wikilink")，得到[單位雙曲線下的情況](../Page/單位雙曲線.md "wikilink")，有：

  - \(\cosh x = \frac{e^x + e^{-x}}{2}\)
  - \(\sinh x = \frac{e^x - e^{-x}}{2}\)

[單位雙曲線中雙曲線扇形的面積是對應](../Page/單位雙曲線.md "wikilink")[直角雙曲線](../Page/直角雙曲線.md "wikilink")\(xy=1\)下雙曲角的\(\frac{1}{2}\)。

## 連分數

儘管自然對數沒有簡單的[連分數](../Page/連分數.md "wikilink")，但有一些[廣義連分數如](../Page/廣義連分數.md "wikilink"):

\[\begin{align}
\ln (1+x) &= \frac{x^1}{1}-\frac{x^2}{2}+\frac{x^3}{3}-\frac{x^4}{4}+\frac{x^5}{5}-\cdots \\
 &= \cfrac{x}{1-0x+\cfrac{1^2x}{2-1x+\cfrac{2^2x}{3-2x+\cfrac{3^2x}{4-3x+\cfrac{4^2x}{5-4x+\ddots}}}}} \\
\end{align}\]

\[\begin{align}
\ln \left( 1+\frac{x}{y} \right) &= \cfrac{x} {y+\cfrac{1x} {2+\cfrac{1x} {3y+\cfrac{2x} {2+\cfrac{2x} {5y+\cfrac{3x} {2+\ddots}}}}}} \\
 &= \cfrac{2x} {2y+x-\cfrac{(1x)^2} {3(2y+x)-\cfrac{(2x)^2} {5(2y+x)-\cfrac{(3x)^2} {7(2y+x)-\ddots}}}} \\
\end{align}\]

這些連分數特別是最後一個對接近1的值快速收斂。但是，更大的數的自然對數，可以輕易的用這些更小的數的自然對數的加法來計算，帶有類似的快速收斂。

例如，因為\(2=1.25^3\times 1.024\)，[2的自然對數可以計算為](../Page/2的自然對數.md "wikilink"):

\[\begin{align}
\ln 2 &= 3 \ln \left( 1+\frac{1}{4} \right) + \ln \left( 1+\frac{3}{125} \right) \\
&= \cfrac{6} {9-\cfrac{1^2} {27-\cfrac{2^2} {45-\cfrac{3^2} {63-\ddots}}}}
+ \cfrac{6} {253-\cfrac{3^2} {759-\cfrac{6^2} {1265-\cfrac{9^2} {1771-\ddots}}}}. \\
\end{align}\]

進而，因為\(10=1.25^{10}\times 1.024^3\)，10的自然對數可以計算為:

\[\begin{align}
\ln 10 &= 10 \ln \left( 1+\frac{1}{4} \right) + 3\ln \left( 1+\frac{3}{125} \right) \\
 &= \cfrac{20} {9-\cfrac{1^2} {27-\cfrac{2^2} {45-\cfrac{3^2} {63-\ddots}}}}
+ \cfrac{18} {253-\cfrac{3^2} {759-\cfrac{6^2} {1265-\cfrac{9^2} {1771-\ddots}}}}. \\
\end{align}\]

## 複數對數

[指數函數可以擴展為對任何複數](../Page/指數函數.md "wikilink")\(x\)得出複數值為\(e^x\)的函數，只需要簡單使用*\(x\)*為複數的無窮級數；這個指數函數的逆函數形成複數對數，並帶有正常的對數的多數性質。但是它涉及到了兩個困難:
不存在*\(x\)*使得\(e^x=0\)；並且有著\(e^{2\pi i}=1=e^0\)。因為乘法性質仍適用於複數指數函數，\(e^z=e^{z+2n\pi i}\)，對於所有複數\(z\)和整數\(n\)。

所以對數不能定義在整個[複平面上](../Page/複平面.md "wikilink")，並且它是[多值函數](../Page/多值函數.md "wikilink")，就是說任何複數對數都可以增加\(2\pi i\)的任何整數倍而成為等價的對數。複數對數只能在[切割平面上是單值函數](../Page/複平面#切割複平面.md "wikilink")。例如，\(\ln i=\frac{1}{2}\pi i\)或\(\frac{5}{2}\pi i\)或\(-\frac{3}{2}\pi i\)等等；儘管\(i^4=1\)，\(4\log=i\)不能定義為\(2\pi i\)或\(10\pi i\)或\(-6\pi i\)，以此類推。

Image:Natural Logarithm Re.svg| *z* = Re(ln(x+iy)) Image:Natural
Logarithm Im Abs.svg| *z* = |Im(ln(x+iy))| Image:Natural Logarithm
Abs.svg| *z* = |ln(x+iy)| Image:Natural Logarithm All.svg| 前三圖的疊加

### 主值定義

對於每個非0複數\(z=x+yi\)，主值\(\log z\)是虛部位於區間\((-\pi, \pi]\)內的對數。表達式\(\log 0\)不做定義，因為沒有複數\(w\)滿足\(e^w=0\)。

要對\(\log z\)給出一個公式，可以先將\(z\)表達為[極坐標形式](../Page/極坐標.md "wikilink")，\(z=re^{i\theta}\)。給定\(z\)，極坐標形式不是確切唯一的，因為有可能向\(\theta\)增加\(2\pi\)的整數倍，所以為了保證唯一性而要求*\(\theta\)*位於區間\((-\pi, \pi]\)內；這個*\(\theta\)*叫做幅角的主值，有時寫為\(\operatorname{arg} z\)或\(\operatorname{atan}2(y,x)\)。則對數的主值可以定義為\[17\]
：

\[\operatorname{Log} z := \text{ln } r + i \theta = \ln |z| + i \operatorname{Arg} z = \operatorname{ln}\sqrt{x^2+y^2} + i \operatorname{atan2}(y,x).\]

例如，\(\log(-3i)=\ln 3-\frac{\pi i}{2}\)。

## 常見科学用法

自然指数有应用於表达放射衰变（[放射性](../Page/放射性.md "wikilink")）之类关于衰減的过程，如放射性原子数目\(N\)随时间变化率\(\frac{dN}{dt}=-pN\)，常数\(p\)为原子衰变概率，积分得\(N(t)=N(0)\exp (-pt)\)。

## 註釋與引用

## 參考

  - [John B. Conway](../Page/John_B._Conway.md "wikilink"), *Functions
    of one complex variable*, 2nd edition, Springer, 1978.
  - [Serge Lang](../Page/Serge_Lang.md "wikilink"), *Complex analysis*,
    3rd edition, Springer-Verlag, 1993.
  - Gino Moretti, *Functions of a Complex Variable*, Prentice-Hall,
    Inc., 1964.
  - Donald Sarason, *[Complex function
    theory](http://books.google.com/books?id=FUWPyHM-XK0C&pg=PA40&dq=logarithm+intitle:Complex+intitle:function+intitle:theory+inauthor:sarason&lr=&as_brr=0&ei=df4UScGONJ_EtAPZ5-XjCw)*,
    2nd edition, American Mathematical Society, 2007.
  - [E. T. Whittaker](../Page/E._T._Whittaker.md "wikilink") and [G. N.
    Watson](../Page/G._N._Watson.md "wikilink"), *A Course in Modern
    Analysis*, fourth edition, Cambridge University Press, 1927.

[de:Logarithmus\#Natürlicher
Logarithmus](../Page/de:Logarithmus#Natürlicher_Logarithmus.md "wikilink")

[Category:基本特殊函数](../Category/基本特殊函数.md "wikilink")
[Category:对数](../Category/对数.md "wikilink")

1.  例如[哈代和](../Page/戈弗雷·哈羅德·哈代.md "wikilink")[賴特所著的](../Page/愛德華·梅特蘭·賴特.md "wikilink")《數論入門》"Introduction
    to the theory of numbers" (1.7, Sixth edition, Oxford 2008)的注解 "log
    x is, of course the 'Napierian' logarithm of x, to base e. 'Common'
    logarithms have no mathematical interest."（log x
    當然是以e為基，x的「[納皮爾](../Page/約翰·納皮爾.md "wikilink")」對數。「常用」對數在數學上毫無重要。）
2.  證明：從1到*b*積分1/*x*，增加三角形{(0, 0), (1, 0), (1, 1)}，並減去三角形{(0, 0), (*b*,
    0), (*b*, 1/*b*)}。
3.
4.
5.  選取接近e的底數b，對數表涉及的b<sup>x</sup>為單調增函數，定義域為0到1而值域為1到b；選取接近1/e的底數b，對數表涉及的b<sup>x</sup>為單調減函數，定義域為0到∞而值域為1到0。
6.  以\(10^{\frac{1}{2^{54}}}\)這個接近1的數為基礎。
7.  設a=1，x軸上\[a,b\]兩點對應的雙曲線線段與原點圍成的[雙曲線扇形面積為f](../Page/雙曲線扇形.md "wikilink")(b)，\[c,d\]對應的扇形面積為f(d)-f(c)，d=bc，即為f(bc)-f(c)，當且僅當f(bc)=f(b)+f(c)時，兩雙曲線扇形面積相等。
8.
9.  ,sections 1, 1.
    , section 9-3
    , p. 484, 489
10. \(\left(1 + \frac{1}{n}\right)^x=\left(\left(1 + \frac{1}{n}\right)^n\right)^{\frac {x}{n}}\)
    在最初的概念下，底數是接近1的數，而對數是整數；經過簡單變換後，底數變大了，成為接近數學常量e的數，而對數變小了，成為 x/n。
11.
12.
13. , p. 386
14. , sections 11.5 and 13.8
15.
16.
17. Sarason, Section IV.9.