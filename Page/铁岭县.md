**铁岭县**位于中国[辽宁省东北部](../Page/辽宁省.md "wikilink")，是[铁岭市下辖的一个县](../Page/铁岭市.md "wikilink")。

## 历史

[辽置银州](../Page/辽.md "wikilink")，隶辽阳府。金为新兴县，隶咸平府。[明移卫至此始称铁岭](../Page/明.md "wikilink")。[清废卫建铁岭县](../Page/清.md "wikilink")，中华民国十八年（1929年）隶辽宁省。1956年隶铁岭专区，1970年隶铁岭地区，1984年隶铁岭市。

## 行政区划

下辖7个镇、7个乡、2个民族乡：

  - 镇：[阿吉镇](../Page/阿吉镇.md "wikilink")、[镇西堡镇](../Page/镇西堡镇.md "wikilink")、[新台子镇](../Page/新台子镇.md "wikilink")、[腰堡镇](../Page/腰堡镇.md "wikilink")、[凡河镇](../Page/凡河镇.md "wikilink")、[平顶堡镇](../Page/平顶堡镇.md "wikilink")、[大甸子镇](../Page/大甸子镇.md "wikilink")。
  - 乡：[蔡牛乡](../Page/蔡牛乡.md "wikilink")、[双井子乡](../Page/双井子乡.md "wikilink")、[官屯乡](../Page/官屯乡.md "wikilink")、[李千户乡](../Page/李千户乡.md "wikilink")、[鸡冠山乡](../Page/鸡冠山乡.md "wikilink")。
  - 民族乡：[横道河子满族乡](../Page/横道河子满族乡.md "wikilink")、[白旗寨满族乡](../Page/白旗寨满族乡.md "wikilink")。

[铁岭县](../Category/铁岭县.md "wikilink")
[县](../Category/铁岭区县市.md "wikilink")
[铁岭](../Category/辽宁省县份.md "wikilink")