《**火星任务**》（），是一部2000年的[美国](../Page/美国.md "wikilink")[科幻電影](../Page/科幻片.md "wikilink")。由[布萊恩·狄帕瑪執導](../Page/布萊恩·狄帕瑪.md "wikilink")，、及編劇。演員有[加利·仙尼斯](../Page/加利·仙尼斯.md "wikilink")、[添·羅賓斯](../Page/添·羅賓斯.md "wikilink")、[當·卓度](../Page/當·卓度.md "wikilink")、[康妮·尼爾森](../Page/康妮·尼爾森.md "wikilink")、[傑瑞·奧康內爾和](../Page/傑瑞·奧康內爾.md "wikilink")。

影片采用了写实的手法来拍摄。影片拍摄的全过程都有[美国太空總署](../Page/美国太空總署.md "wikilink")（NASA）作为技术顾问，从[太空梭的外观与内部陈设](../Page/太空梭.md "wikilink")，全触摸式的交互界面和自动化程度很高的飞船操作系统，直到种种[火星探测仪器](../Page/火星.md "wikilink")，都以目前[空间科学的研究](../Page/空间科学.md "wikilink")、模拟做参照，创作出的画面令人十分震撼。为了求得真实感，影片花费了大量资金用于拍摄[失重场面](../Page/失重.md "wikilink")，在这些场面中甚至包括一段失重状态下的太空舞蹈。

除了令人震撼的真实感外，影片着力刻画[太空人的生活](../Page/太空人.md "wikilink")，他们的风趣幽默、职业的高度风险、严格的纪律要求，以及他们拥有的科学态度和奉献精神。

## 劇情簡介

故事讲述人类首次的载人火星探测计划突然遭遇神秘的事故，一支救援队出发前往救援、调查事故；正当要抵达时，飞船却遭遇了太空尘埃撞击，船长伍德罗为了拯救大家，而前往挂接在外太空的补给舱，虽然成功挂接，但自己却飘落到太空中，为了不让他的妻子特芮拯救他，他勇敢的摘下了自己的头盔，结束了自己的生命。当剩下三人历经艰险到达火星后，卢克告知他们他发现造成事故的原因是远古火星人留下的「[神秘人脸](../Page/塞东尼亚区.md "wikilink")」；遥控漫游车解开了火星人遗迹的秘密之后，四名太空人中的除菲尔以外的三名太空人前往遗迹。当成功进入遗迹内部后，发现火星人在很久前因为[撞擊事件逃去了](../Page/撞擊事件#科幻小說.md "wikilink")[河外星系](../Page/河外星系.md "wikilink")，并且最后一艘火星人的太空船是[地球](../Page/地球.md "wikilink")[生命的始祖](../Page/泛種論#引導性泛種論.md "wikilink")，又发现那是火星人的邀请，邀请他们乘坐火星太空船前往他们的星系；太空人吉姆决定乘坐火星人的太空船前往了火星人的星系，吉姆在火星人的太空船中的液体可以呼吸，另外三名太空人则返回了地球。

## 主要角色

  - [蓋瑞·辛尼茲](../Page/蓋瑞·辛尼茲.md "wikilink") 饰 吉姆·麦克尼尔（Jim McConnell）

  - [蒂姆·罗宾斯](../Page/蒂姆·罗宾斯.md "wikilink") 饰 伍德罗·伍迪·布莱克（Woodrow 'Woody'
    Blake）

  - [唐·钱德尔](../Page/唐·钱德尔.md "wikilink") 饰 卢克·格雷厄姆（Luke Graham）

  - [康妮·尼尔森](../Page/康妮·尼尔森.md "wikilink") 饰 特芮·菲舍尔（Terri Fisher）

  - [傑瑞·奧康內爾](../Page/傑瑞·奧康內爾.md "wikilink") 饰 菲尔·奥枚尔（Phil Ohlmyer）

  - [彼得·奧特布里奇](../Page/彼得·奧特布里奇.md "wikilink") 饰 塞尔基·基洛夫（Sergei Kirov）

  - [卡萬·史密斯](../Page/卡萬·史密斯.md "wikilink") 饰 尼古拉斯·威利斯（Nicholas Willis）

  - 饰 瑞尼·科太（Reneé Coté）

  - 饰 黛布拉·格雷厄姆（Debra Graham）

  - 饰 玛吉·麦克尼尔（Maggie McConnell）

  - 小罗伯特·贝利（Robert Bailey Jr.）饰 博比·格雷厄姆（Bobby Graham）

  - [艾文·穆拿-史圖](../Page/艾文·穆拿-史圖.md "wikilink") 饰 Ramier Beck

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  -
  -
  -
  -
  -
[Category:2000年電影](../Category/2000年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:科幻片](../Category/科幻片.md "wikilink")
[Category:冒險片](../Category/冒險片.md "wikilink")
[Category:劇情片](../Category/劇情片.md "wikilink")
[Category:驚悚片](../Category/驚悚片.md "wikilink")
[Category:試金石電影](../Category/試金石電影.md "wikilink")
[Category:火星背景電影](../Category/火星背景電影.md "wikilink")
[M](../Category/布赖恩·德帕尔马电影.md "wikilink")