[Kinetochore.jpg](https://zh.wikipedia.org/wiki/File:Kinetochore.jpg "fig:Kinetochore.jpg")中期的显微照片：蓝色为缩聚的[染色体](../Page/染色体.md "wikilink")，粉色为[着丝点](../Page/着丝点.md "wikilink")，绿色为微管\]\]
**纺锤体**是[真核细胞](../Page/真核细胞.md "wikilink")[有丝分裂或](../Page/有丝分裂.md "wikilink")[减数分裂过程中形成的中间宽两端窄的](../Page/减数分裂.md "wikilink")状[细胞结构](../Page/细胞结构.md "wikilink")，主要由大量纵向排列的[微管构成](../Page/微管.md "wikilink")。纺锤体一般产生于[早前期](../Page/早前期.md "wikilink")（PreProphase），并在[分裂末期](../Page/分裂末期.md "wikilink")（Telophase）消失。纺锤体主要元件包括[极间丝](../Page/极间丝.md "wikilink")、[着丝点丝](../Page/着丝点丝.md "wikilink")、[星体丝及](../Page/星体丝.md "wikilink")[区间丝四种微管和附着在微管上的动力分子](../Page/区间丝.md "wikilink")[分子马达以及一系列复杂的](../Page/分子马达.md "wikilink")[超分子结构组成](../Page/超分子.md "wikilink")。

[动物细胞内的纺锤体两端具有由](../Page/动物细胞.md "wikilink")[中心粒构成的](../Page/中心粒.md "wikilink")“[星体](../Page/星体_\(细胞结构\).md "wikilink")”，所以动物细胞的纺锤体也称为“[星纺锤体](../Page/星纺锤体.md "wikilink")”。[高等植物的细胞的纺锤体则不含中心粒](../Page/高等植物.md "wikilink")，称为“[无星纺锤体](../Page/无星纺锤体.md "wikilink")”。而[真菌细胞的纺锤体含](../Page/真菌.md "wikilink")[纺锤极体](../Page/纺锤极体.md "wikilink")（Spindle
Pole Body），一般被视为中心体的[同源细胞结构](../Page/同源细胞结构.md "wikilink")。

## 形成

在含[中心体的细胞中](../Page/中心体.md "wikilink")，纺锤体的形成开始于细胞分裂前初期，[核膜破裂](../Page/核膜破裂.md "wikilink")（Nuclear
Envelope
Breakdown，NEB）之前。初期的结构为两个相互独立的以中心体为核的[星状体](../Page/星状体.md "wikilink")（asters）。当[核膜消失后](../Page/核膜.md "wikilink")，星状体和原本位于[细胞核内的](../Page/细胞核.md "wikilink")[染色体发生一系列复杂的](../Page/染色体.md "wikilink")[相互作用](../Page/相互作用.md "wikilink")。最终结果为所有的染色体在纺锤体的中央（[赤道板](../Page/有絲分裂#赤道板.md "wikilink")）排列整齐，每一个染色体上的两个[着丝点各被一束极性相同的微管](../Page/着丝点.md "wikilink")（通常称为[纺锤丝](../Page/纺锤丝.md "wikilink")）附着。此时细胞处于分裂中期，纺锤体生成完毕。实验证明，中心体在这个过程中的作用不是必需的。动物细胞在中心体被[激光捣毁后仍旧能够筑构纺锤体](../Page/激光.md "wikilink")，但其位置通常不在细胞的大致几何中心，其后的[胞质分裂也会受严重影响](../Page/胞质分裂.md "wikilink")。

在不含中心体的细胞中，纺锤体的生成是由染色体本身主导的。此过程由一小[分子量的](../Page/分子量.md "wikilink")[GTP连接蛋白](../Page/GTP连接蛋白.md "wikilink")（Ran
GTP
ase）控制。核膜破裂后，纺锤丝由染色体周围生成。其后这些纺锤丝会在动力分子与微管的协同影响下自动排列为极性相反、数目大致相同的两组，每组的极性相对于一组着丝点。同时在微管远端的[动力蛋白会将这些微管束集中到一点](../Page/动力蛋白.md "wikilink")，形成[纺锤极区](../Page/纺锤极区.md "wikilink")（Spindle
Polar Zone）。与此同时，染色体会自动在[赤道板排列整齐](../Page/赤道板.md "wikilink")。纺锤体完成形成过程。

## 功能与分解

在细胞分裂中，纺锤体的主要功能可分为两个部分。

1.  排列与分裂染色体。纺锤体的完整性决定了染色体分裂的正确性。纺锤体的正常生成是染色体排列的必要条件。纺锤体生成完毕后一般会有5-20[min的延迟](../Page/分钟.md "wikilink")，以供细胞调整着丝点上微管束的极性，以及决定是否所有的着丝点都附着正确。此后细胞进入[分裂后期](../Page/分裂后期.md "wikilink")，染色体分裂为两组数目相等的[姐妹染色单体](../Page/姐妹染色单体.md "wikilink")。同样，纺锤体的完整性决定这个分裂过程在时间和空间上的准确性。
2.  纺锤体另一功能为决定[细胞质分裂的](../Page/细胞质分裂.md "wikilink")[分裂面](../Page/分裂面.md "wikilink")。染色体分裂的同时，纺锤体中的一部分微管不随染色体分裂到两极，而停止在纺锤体中央，
    形成[中心纺锤体](../Page/中心纺锤体.md "wikilink")（central
    spindle）。在纺锤中体的中央为两组极性相反的微管交叠的区域，称为“[纺锤中间区](../Page/纺锤中间区.md "wikilink")”（spindle
    midzone）。这个区域就是接下来的胞质分裂面。胞质分裂开始于[分裂后期的较晚时期](../Page/分裂后期.md "wikilink")。胞质分裂一般结束于分裂末期之后的1-2h，此期间两个子细胞由[中间体](../Page/中间体.md "wikilink")（midbody）连接。
    一般认为纺锤体的分解发生在细胞分裂末期。

## 参考文献

## 参见

  - [纺锤丝](../Page/纺锤丝.md "wikilink")

{{-}}

[Category:细胞解剖学](../Category/细胞解剖学.md "wikilink")
[Category:有絲分裂](../Category/有絲分裂.md "wikilink")