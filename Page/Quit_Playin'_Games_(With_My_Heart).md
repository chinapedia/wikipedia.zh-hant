是[新好男孩組合的第五首單曲](../Page/新好男孩.md "wikilink")，被收入了[新好男孩專輯和其後的](../Page/新好男孩_\(國際版\).md "wikilink")[美國版專輯](../Page/新好男孩_\(美國版\).md "wikilink")。單曲發行於1997年，其後很快在[英國單曲榜和](../Page/英國單曲榜.md "wikilink")[Billboard
單曲百名榜上位列第二名](../Page/Billboard_單曲百名榜.md "wikilink")。在[瑞士與](../Page/瑞士.md "wikilink")[澳大利亞的榜單上這首單曲位列第一](../Page/澳大利亞.md "wikilink")，在[荷蘭的榜單上位列第七](../Page/荷蘭.md "wikilink")。

這首單曲是[新好男孩早年最成功的作品](../Page/新好男孩.md "wikilink")，直到團隊鼎盛之作
才將其超過。這首單曲在1997年的[亞洲音樂電視榜單上亦位列前茅](../Page/亞洲音樂電視.md "wikilink")。

這首單曲有一個[義大利文版](../Page/義大利文.md "wikilink")，名為 ，組合在義大利演出時即演唱此版。

## 曲目

1.
2.
3.
4.
5.
## 錄影帶

[BackstreetBoys_QuitPlayingGames_video.jpg](https://zh.wikipedia.org/wiki/File:BackstreetBoys_QuitPlayingGames_video.jpg "fig:BackstreetBoys_QuitPlayingGames_video.jpg")

單曲的[音樂錄影帶由](../Page/音樂錄影帶.md "wikilink")
導演，背景是夜幕降臨后的一個廢棄[籃球場](../Page/籃球場.md "wikilink")。錄影帶放至一半時，時間進入午夜，球場也下起雨來。后半段中[A·J·麥克林等三人上衣扣解開](../Page/A·J·麥克林.md "wikilink")，露出他們的肌肉。這是自“[接招](../Page/接招.md "wikilink")”樂團以來，男孩組合的一個傳統，但這也是新好男孩唯一一次在錄影帶中沒有裝點整齊。

[Category:新好男孩歌曲](../Category/新好男孩歌曲.md "wikilink")