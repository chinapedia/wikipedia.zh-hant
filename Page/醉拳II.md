也作《醉拳2》，是1994年首映的一部[香港](../Page/香港.md "wikilink")[功夫片](../Page/功夫片.md "wikilink")，由[劉家良執導](../Page/劉家良.md "wikilink")，[成龍主演](../Page/成龍.md "wikilink")，[黃日華](../Page/黃日華.md "wikilink")、[狄龍](../Page/狄龍.md "wikilink")、[梅艷芳](../Page/梅艷芳.md "wikilink")、劉家良等人共同出演。梅艷芳演出鬼馬後母，為電影增添了許多喜劇元素，演出備受讚賞，是電影的一大亮點。片尾的決鬥場面超過十分鐘，成龍與[成家班成員之一的](../Page/成家班.md "wikilink")[盧惠光有一段](../Page/盧惠光.md "wikilink")[對手戲](../Page/對手戲.md "wikilink")。該片於2015年被[美國](../Page/美國.md "wikilink")《[時代](../Page/時代_\(雜誌\).md "wikilink")》周刊評為「百大不朽電影」之一\[1\]。

在大陆地区该电影称为《大醉拳》。

## 劇情

黃飛鴻為了阻止外國人偷運中國國寶出境、結果錯喝了工業酒精導致腦子燒壞變成阿達阿達了

## 演員表

|                                  |         |                                  |                                                                      |
| -------------------------------- | ------- | -------------------------------- | -------------------------------------------------------------------- |
| **演　員**                          | **角　色** | **粵語配音**                         | **備　註**                                                              |
| [成龍](../Page/成龍.md "wikilink")   | 黃飛鴻     | [陳欣](../Page/陳欣.md "wikilink")   |                                                                      |
| [狄龍](../Page/狄龍.md "wikilink")   | 黃麒英     | [周永光](../Page/周永光.md "wikilink") |                                                                      |
| [梅艷芳](../Page/梅艷芳.md "wikilink") | 莫桂蘭     | [陸惠玲](../Page/陸惠玲.md "wikilink") | 黃麒英妻(黃飛鴻繼母)                                                          |
| [黃日華](../Page/黃日華.md "wikilink") | 阿燦      | [高翰文](../Page/高翰文.md "wikilink") | 蔡李佛傳人                                                                |
| [劉家良](../Page/劉家良.md "wikilink") | 福民祺     | [源家祥](../Page/源家祥.md "wikilink") | [滿洲最後一位](../Page/滿洲地區.md "wikilink")[武舉人](../Page/武舉人.md "wikilink") |
| [何永芳](../Page/何永芳.md "wikilink") | 阿芳      | [何錦華](../Page/何錦華.md "wikilink") | 市場賣蛇女子                                                               |
| [錢嘉樂](../Page/錢嘉樂.md "wikilink") | 火生      |                                  | 鋼鐵工廠工人                                                               |
| [盧惠光](../Page/盧惠光.md "wikilink") | 約翰      |                                  |                                                                      |
| [蔣志光](../Page/蔣志光.md "wikilink") | 雞骨草     | [羅君左](../Page/羅君左.md "wikilink") | 黃家管家                                                                 |
| [劉德華](../Page/劉德華.md "wikilink") | 張學良（客串） |                                  | 東北少帥                                                                 |
| [翁虹](../Page/翁虹.md "wikilink")   | 小媽牌友    |                                  |                                                                      |
| [鮑方](../Page/鮑方.md "wikilink")   |         |                                  | 大排檔廚師                                                                |
| [曹穎](../Page/曹穎.md "wikilink")   |         |                                  | 黃家丫鬟                                                                 |
| [張嘉年](../Page/張嘉年.md "wikilink") |         |                                  | 路人                                                                   |
|                                  |         |                                  |                                                                      |

## 電影配樂

  - [成龍](../Page/成龍.md "wikilink")《醉拳》

## 相關

  - [醉拳](../Page/醉拳.md "wikilink")
  - [醉拳III](../Page/醉拳III.md "wikilink")

## 参考资料

## 外部連結

  - {{@movies|fYatm0864097|醉拳2}}

  -
  -
  -
  -
  -
  -
[Category:香港动作片](../Category/香港动作片.md "wikilink")
[Category:1990年代动作片](../Category/1990年代动作片.md "wikilink")
[Category:功夫片](../Category/功夫片.md "wikilink")
[Category:黃飛鴻電影](../Category/黃飛鴻電影.md "wikilink")
[Category:香港續集電影](../Category/香港續集電影.md "wikilink")
[4](../Category/1990年代香港電影作品.md "wikilink")
[Category:劉家良電影](../Category/劉家良電影.md "wikilink")
[Category:香港電影金像獎最佳動作設計獲獎電影](../Category/香港電影金像獎最佳動作設計獲獎電影.md "wikilink")
[Category:金马奖最佳动作设计获奖电影](../Category/金马奖最佳动作设计获奖电影.md "wikilink")
[Category:胡伟立配乐电影](../Category/胡伟立配乐电影.md "wikilink")
[Category:嘉禾电影](../Category/嘉禾电影.md "wikilink")

1.