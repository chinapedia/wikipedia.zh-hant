《**戀愛-{巴士}-**》（）是[日本](../Page/日本.md "wikilink")[富士電視台的](../Page/富士電視台.md "wikilink")[綜藝性的](../Page/綜藝.md "wikilink")[戀愛](../Page/戀愛.md "wikilink")[紀錄片](../Page/紀錄片.md "wikilink")[節目](../Page/電視節目.md "wikilink")。1999年10月11日開播，於[日本時間毎星期一](../Page/日本時間.md "wikilink")23:00～23:30播出，日本時間2009年3月23日結束播映。2010年12月5日，於富士電視台旗下的[通信衛星電視頻道](../Page/通信衛星.md "wikilink")「富士電視台TWO」復播，節目名稱更改為《戀愛巴士２》，首播時間為隔週的星期六。

## 概要

節目製作單位招募未婚男女，坐上製作單位在當地租用、漆成[粉紅色的小型巴士](../Page/粉紅色.md "wikilink")，展開旅程。

## 主持人

  - [久本雅美](../Page/久本雅美.md "wikilink") (1999年10月11日－停播)
  - [今田耕司](../Page/今田耕司.md "wikilink") (1999年10月11日－停播)
  - [Wentz瑛士](../Page/Wentz瑛士.md "wikilink") (2006年4月10日－停播)
  - [加藤晴彥](../Page/加藤晴彥.md "wikilink") (1999年10月11日－2006年3月20日)

## 歷代主題歌

  - ()

  - ()

  - [fragile](../Page/脆弱_\(小事樂團\).md "wikilink") ([Every Little
    Thing](../Page/小事樂團.md "wikilink"))

  - [Way of Difference](../Page/Way_of_Difference.md "wikilink")
    ([GLAY](../Page/GLAY.md "wikilink"))

  -
  - ([I WiSH](../Page/I_WiSH.md "wikilink"))

  - [OH MY LITTLE GIRL](../Page/OH_MY_LITTLE_GIRL.md "wikilink") ()

  - ()

  - ()

  - ()

  - [My LOVE](../Page/My_LOVE_\(川嶋愛\).md "wikilink") ()

  - fate ()

  - ()

## 經由地點

[Ainori_Countries.png](https://zh.wikipedia.org/wiki/File:Ainori_Countries.png "fig:Ainori_Countries.png")

  - 1999年 : ₀ ₁ ₂
    2000年 : ₃ ₄ ₅ ₆ ₇ ₈ ₉ ₁₀ ₁₁ ₁₂ ₁₃
    2001年 : ₁₄ (誤入的國家)₁₅ ₁₆ ₁₇ ₁₈ ₁₉ ₂₀ ₂₁ ₂₂ ₂₃ ₂₄ ₂₅
    2002年 : ₂₆ ₂₇ ₂₈ ₂₉ ₃₀ ₃₁ ₃₂ ₃₃ ([美國換乘](../Page/美國.md "wikilink"))
    ₃₄
    2003年 : ₃₅ ₃₆ ₃₇ ₃₈ ₃₉ ₄₀ ₄₁([馬來西亞換乘](../Page/馬來西亞.md "wikilink"))
    ₄₂ ₄₃ ₄₄
    2004年 : ₄₅ ₄₆ ₄₇ ₄₈ ₄₉ ₅₀ ₅₁ ₅₂ ₅₃
    2005年 : ₅₄ ₅₅ ₅₆ ₅₇ ₅₈ ₅₉ ₆₀ ₆₁ ₆₂ ₆₃
    2006年 : ₆₄ ₆₅ ₆₆ ₆₇ ₆₈ ₆₉ ₇₀ ₇₁ ₇₂
    2007年 : ₇₃ ₇₄ ₇₅ ₇₆ ₇₇ ₇₈ ([斐濟换乘](../Page/斐濟.md "wikilink")) ₇₉ ₈₀
    ₈₁ ₈₂
    2008年 : ₈₃ ₈₄ ₈₅ ₈₆ ₈₇ ₈₈ ₈₉ ₉₀
    2009年 : ₉₁ ₉₂

<!-- end list -->

  - 戀愛巴士2
    2010年 :
    2011年 :

※於日本播出年

  - 因安全問題而跳過的國家:[北韓](../Page/北韓.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[哥倫比亞](../Page/哥倫比亞.md "wikilink")
    原先因因安全問題而跳過的國家，後因證實為錯誤訊息再度排入旅程的國家:[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")

## 冷知識

  - 行經中國[南京時](../Page/南京.md "wikilink")，成員曾因對[南京大屠殺的歷史見解不同而引發爭執](../Page/南京大屠殺.md "wikilink")。
  - [巴拉圭並不算戀愛巴士的旅行國家之一](../Page/巴拉圭.md "wikilink")，而是因為前一個國家[巴西的司機不小心誤打誤撞的把戀愛巴士開進巴拉圭](../Page/巴西.md "wikilink")，原本的路程是直接進入[阿根廷](../Page/阿根廷.md "wikilink")。
  - 進入[哥斯大黎加時原本有位女性新成員要在此國加入隊伍行列](../Page/哥斯大黎加.md "wikilink")，但最終原因不明的被哥斯大黎加拒絕入境，該名女成員只得在下一國[牙買加才得以亮相加入](../Page/牙買加.md "wikilink")。
  - 行經哥斯大黎加時，有三名男性成員露出臀部與當地的小學生嘻鬧而遭到當地警方關切，最後在隨行的工作人員的協調下免於被驅逐出境的命運，但仍遭到警方的嚴厲訓斥。
  - 中歐小國[列支敦斯登則是戀愛巴士停留最短的國家](../Page/列支敦斯登.md "wikilink")，僅20分鐘。
  - 而在列支敦斯登之前停留最短的則是太平洋島國[東加](../Page/東加.md "wikilink")，僅僅只有一天左右的時間。
  - 戀愛巴士在結束第一輪環遊世界之旅後，為了慶祝第二輪環遊世界之旅開始，而在首站的[菲律賓當地](../Page/菲律賓.md "wikilink")，再度招募當地一名女性成員加入行列。
  - 行經[印度尼西亞時](../Page/印度尼西亞.md "wikilink")，一名女性成員染上[登革熱](../Page/登革熱.md "wikilink")，被強制隔離退出旅程。
  - 在[斯里蘭卡時](../Page/斯里蘭卡.md "wikilink")，因該節目的粉紅色戀愛巴士在旅途中發生故障，最後只好換成普通巴士結束該國旅程。
  - 2009年3月23日播放結束為止，在戀愛巴士所有經過的國家中，[南非是誕生最多情侶的國家](../Page/南非.md "wikilink")，共有3對。
  - 戀愛巴士抵達[烏克蘭時](../Page/烏克蘭.md "wikilink")，曾有意安排成員前往[車諾比核電廠事故現場探訪](../Page/車諾比核電廠事故.md "wikilink")，但最終因安全問題取消，改由攝影工作人員進入。
  - 戀愛巴士進入[台灣時](../Page/台灣.md "wikilink")，曾遇到一名當地懷念[日治時代的老太太](../Page/台灣日治時期.md "wikilink")，成員跟她進行對談，但由於內容有爭議，引起部分留學日本的台灣學生不滿，向電視台提出抗議，[台灣在播出這集時](../Page/台灣.md "wikilink")，剪掉具有爭議性的這一段對談。
  - 早期該節目只把焦點放在成員的戀愛和人際關係上，直到中後期才開始介紹成員所在[國家的風土民情](../Page/國家.md "wikilink")、歷史情結，甚至是[社會問題](../Page/社會問題.md "wikilink")。

### 正式名稱的變遷

1.  **** (初期)
2.  ****
3.  **** (現在)

### 日語『』是自創新詞

本来『』是『』（「共乘」之意）的省略語。但是[日語](../Page/日語.md "wikilink")『相』與『』的發音相同。用日語[平假名表示](../Page/平假名.md "wikilink")，有雙關（「共乘」與「愛上」）的意味。

## 相關條目

  - [富士電視台](../Page/富士電視台.md "wikilink")
  - [國興衛視](../Page/國興衛視.md "wikilink")

## 節目的變遷

[Category:富士電視台綜藝節目](../Category/富士電視台綜藝節目.md "wikilink")
[Category:1999年日本電視節目](../Category/1999年日本電視節目.md "wikilink")
[Category:電視紀錄片](../Category/電視紀錄片.md "wikilink")
[Category:愛情題材作品](../Category/愛情題材作品.md "wikilink")
[Category:巴士題材作品](../Category/巴士題材作品.md "wikilink")
[Category:國興衛視電視節目](../Category/國興衛視電視節目.md "wikilink")
[Category:旅遊節目](../Category/旅遊節目.md "wikilink")