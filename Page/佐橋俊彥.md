**佐橋俊彥**，出身於[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")，是一位日本[作曲家](../Page/作曲家.md "wikilink")。1986年畢業於[東京藝術大學音樂學部作曲科](../Page/東京藝術大學.md "wikilink")。佐橋曾為[動畫](../Page/動畫.md "wikilink")（包括[OVA](../Page/OVA.md "wikilink")、電影、以及連載）、電視遊戲、電影、戲劇、以及音樂劇寫曲。

他的作品包括[閃電霹靂車](../Page/閃電霹靂車.md "wikilink")(SAGA、SIN)、[機動戰士GUNDAM
SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")、[機動戰士GUNDAM SEED
DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")、[神槍少女](../Page/神槍少女.md "wikilink")、[Black
Blood
Brothers](../Page/Black_Blood_Brothers.md "wikilink")、[驚爆危機](../Page/驚爆危機.md "wikilink")、以及[SIMOUN](../Page/SIMOUN.md "wikilink")。與他共事的[倫敦交響樂團已經出版了兩張專輯](../Page/倫敦交響樂團.md "wikilink")，分別是改編自機動戰士GUNDAM
SEED以及機動戰士GUNDAM SEED DESTINY。

他的作品富有交響性與古典感，証明他在西洋古典音樂的訓練很扎實。他也常使用鍵盤樂器製造良好的音效在音樂中。

## 主要作品

### 電視節目

  - [大家的体操](../Page/大家的体操.md "wikilink")（[NHK](../Page/日本放送協会.md "wikilink")）
  - [BSジュニアのど自慢](../Page/BSジュニアのど自慢.md "wikilink")（NHK・BS2
    1999年4月～2002年9月まで同番組のテーマ曲を担当）
  - [紅白歌合戰](../Page/紅白歌合戰.md "wikilink")（NHK　1991年から1994年まで　テーマ曲を作曲）

### 電影

  - [THE MOTION PICTURE 餓狼伝説](../Page/餓狼傳說.md "wikilink")（1994年）
  - [レジェンドオブクリスタニア
    はじまりの冒険者たち](../Page/レジェンドオブクリスタニア_はじまりの冒険者たち.md "wikilink")（1995年）大島ミチルとの連名
  - [(ハル)](../Page/\(ハル\).md "wikilink")（1996年）
  - [史上最大作弊戰爭](../Page/史上最大作弊戰爭.md "wikilink")（1996年）
  - [心跳回憶](../Page/心跳回憶_\(電影\).md "wikilink")（1997年）
  - [甜心戰士F](../Page/甜心戰士.md "wikilink")（1996年）
  - [マグニチュード](../Page/マグニチュード.md "wikilink")（1997年）
  - [刑法第39条](../Page/刑法第39条.md "wikilink")（1998年）
  - [お受験](../Page/お受験.md "wikilink")（1999年）
  - [ウルトラマンティガ・ウルトラマンダイナ&ウルトラマンガイア
    超時空の大決戦](../Page/ウルトラマンティガ・ウルトラマンダイナ&ウルトラマンガイア_超時空の大決戦.md "wikilink")（1999年）
  - [ドリームメーカー](../Page/ドリームメーカー.md "wikilink")（1999年）
  - [烏龍派出所 THE MOVIE](../Page/烏龍派出所.md "wikilink")（1999年）
  - [世界奇妙物語](../Page/世界奇妙物語.md "wikilink") 映画の特別編（2000年）
  - [柏拉圖式性愛](../Page/柏拉圖式性愛.md "wikilink")（2001年）
  - 仮面ライダーアギト PROJECT G4（2001年）
  - ぷりてぃ・ウーマン（2002年）
  - [金肉人II世](../Page/金肉人II世.md "wikilink")（2003年）（編曲のみ。作曲は渡部チェル）
  - [こちら葛飾区亀有公園前派出所 THE MOVIE2 UFO襲来\!
    トルネード大作戦\!\!](../Page/烏龍派出所.md "wikilink")（2003年）
  - [假面騎士響鬼與七人的戰鬼](../Page/假面騎士響鬼與七人的戰鬼.md "wikilink")（2005年）
  - ウルトラマンメビウス&ウルトラ兄弟（2006年）
  - [假面騎士電王劇場版之《老子，誕生\!》（
    俺、誕生\!）](../Page/假面騎士電王_老子，誕生！.md "wikilink")（2007年）
  - [假面騎士電王劇場版之CLIMAX刑事](../Page/假面騎士電王&KIVA_CLIMAX刑事.md "wikilink")（2008年）
  - [豐臣公主](../Page/豐臣公主.md "wikilink")（2011年）

### 電視劇

  - [男の選びかた](../Page/男の選びかた.md "wikilink")（1997年、[富士電視台](../Page/富士電視台.md "wikilink")）
  - [素敵に女ざかり](../Page/素敵に女ざかり.md "wikilink")（1998年、[NHK](../Page/NHK.md "wikilink")）
  - [板橋マダムス](../Page/板橋マダムス.md "wikilink")（1998年、富士電視台）
  - [小市民ケーン](../Page/小市民ケーン.md "wikilink")（1998年、富士電視台）
  - [催眠](../Page/催眠_\(テレビドラマ\).md "wikilink")（2000年、[TBS](../Page/東京放送.md "wikilink")）
  - [救命病棟24時](../Page/救命病棟24時.md "wikilink")（2001年、2005年、富士電視台）
  - [明日があるさ](../Page/明日があるさ_\(テレビドラマ\).md "wikilink")（2001年、[日本電視台](../Page/日本電視台.md "wikilink")）
  - [東京庭付き一戸建て](../Page/東京庭付き一戸建て.md "wikilink")（2002年、日本電視台）
  - [ぼくらはみんな生きている](../Page/ぼくらはみんな生きている.md "wikilink")（2003年、朝日電視台）
  - [犬神家の一族](../Page/犬神家の一族.md "wikilink")（2003年、富士電視台）
  - [太閤記～サルと呼ばれた男～](../Page/太閤記～サルと呼ばれた男～.md "wikilink")（2003年、富士電視台）
  - [ドラマ愛の詩](../Page/ドラマ愛の詩.md "wikilink")「[ミニモニ。でブレーメンの音楽隊](../Page/ミニモニ。でブレーメンの音楽隊.md "wikilink")」（2004年、[NHK教育電視台](../Page/NHK教育電視台.md "wikilink")）
  - [八墓村](../Page/八墓村.md "wikilink")（2004年、富士電視台）
  - [徳川綱吉 イヌと呼ばれた男](../Page/徳川綱吉_イヌと呼ばれた男.md "wikilink")（2004年、富士電視台）
  - [ワンダフルライフ](../Page/ワンダフルライフ_\(TVドラマ\).md "wikilink")（2004年、富士電視台）
  - [恋におちたら～僕の成功の秘密～](../Page/恋におちたら～僕の成功の秘密～.md "wikilink")（2005年、富士電視台）
  - [刑事部屋～六本木おかしな捜査班～](../Page/刑事部屋～六本木おかしな捜査班～.md "wikilink")（2005年、朝日電視台）
  - [女刑事みずき～京都洛西署物語～](../Page/女刑事みずき～京都洛西署物語～.md "wikilink")（2005年、2007年、朝日電視台）
  - [女王蜂](../Page/女王蜂_\(横溝正史\).md "wikilink")（2006年1月6日、富士電視台）
  - [ザ・ヒットパレード](../Page/ザ・ヒットパレード.md "wikilink")（2006年5月26・27日、富士電視台）
  - [悪魔が来りて笛を吹く](../Page/悪魔が来りて笛を吹く.md "wikilink")（2007年1月5日、富士電視台）
  - [花嫁とパパ](../Page/花嫁とパパ.md "wikilink")（2007年、富士電視台）
  - [ちりとてちん](../Page/ちりとてちん.md "wikilink")（2007年、NHK、10月放送予定）

### 特攝作品

  - [超人力霸王帕瓦德](../Page/帕瓦特·奥特曼.md "wikilink")（1993年、ビデオ作品）
  - [激走戰隊CARRANGER](../Page/激走戰隊車連者.md "wikilink")（1996年、[朝日電視台](../Page/朝日電視台.md "wikilink")）
  - [星獸戰隊GINGAMAN](../Page/星獸戰隊銀河人.md "wikilink")（1998年、朝日電視台）
  - [超人力霸王蓋亞](../Page/超人力霸王蓋亞.md "wikilink")（1998年、[毎日放送](../Page/毎日放送.md "wikilink")）
  - [假面騎士KUUGA](../Page/假面騎士KUUGA.md "wikilink")（2000年、朝日電視台）
  - [假面騎士AGITO](../Page/假面騎士AGITO.md "wikilink")（2001年、朝日電視台）
  - [假面騎士響鬼](../Page/假面騎士響鬼.md "wikilink")（2005年、朝日電視台）
  - [超人力霸王梅比斯](../Page/超人力霸王梅比斯.md "wikilink")（2006年、[中部日本放送](../Page/中部日本放送.md "wikilink")）
  - [假面騎士電王](../Page/假面騎士電王.md "wikilink")（2007年、朝日電視台）
  - [超人力霸王梅比斯外傳 幽靈重生](../Page/超人力霸王梅比斯.md "wikilink")（2009年，OV - 原聲帶協力）
  - \-{[大魔神華音](../Page/大魔神華音.md "wikilink")}-（2010年、[東京電視台](../Page/東京電視台.md "wikilink")）
  - [獸電戰隊強龍者](../Page/獸電戰隊強龍者.md "wikilink")（2013年、朝日電視台）
  - [假面騎士ZI-O](../Page/假面騎士ZI-O.md "wikilink")（2018-2019年、朝日電視台）

### 動畫

  - （1990年）

  - （1991年）

  - [妙廚老爹](../Page/妙廚老爹.md "wikilink")（1992年）

  - [GS美神](../Page/GS美神極樂大作戰.md "wikilink")（1993年）

  - [バトルファイターズ餓狼伝説](../Page/餓狼傳說.md "wikilink")（1993年）

  - [小紅帽恰恰](../Page/小紅帽恰恰.md "wikilink")（1994年）[手塚理との連名](../Page/手塚理.md "wikilink")

  - [霸王大系龍騎士
    亞迪傳說](../Page/霸王大系龍騎士.md "wikilink")（1994年）[奥慶一との連名](../Page/奥慶一.md "wikilink")

  - [魔法騎士雷阿斯](../Page/魔法騎士雷阿斯.md "wikilink")（1994年）

  - [閃電霹靂車SAGA](../Page/閃電霹靂車.md "wikilink")（1996年）

  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")（1996年）[米光亮との連名](../Page/米光亮.md "wikilink")

  - [甜心戰士F](../Page/甜心戰士.md "wikilink")（1997年）

  - [超魔神英雄傳](../Page/魔神英雄傳.md "wikilink")（1997年）[朝川朋之との連名](../Page/朝川朋之.md "wikilink")

  - [閃電霹靂車SIN](../Page/閃電霹靂車.md "wikilink")（1998年）

  - [星方天使](../Page/星方天使.md "wikilink")（1999年）

  - [鋼鐵天使](../Page/鋼鐵天使.md "wikilink")（1999年）

  - [魅影巨神](../Page/魅影巨神.md "wikilink")（1999年）

  - [Hunter × Hunter](../Page/Hunter_×_Hunter.md "wikilink")（1999年）

  - [鋼鐵天使二式](../Page/鋼鐵天使.md "wikilink")（2000年）

  - [隨風飄的月影蘭](../Page/隨風飄的月影蘭.md "wikilink")（2000年）

  - [GEAR戰士電童](../Page/GEAR戰士電童.md "wikilink")（2000年）

  - [バンパイヤン・キッズ](../Page/バンパイヤン・キッズ.md "wikilink")（2001年）

  - [驚爆危機](../Page/驚爆危機.md "wikilink")（2002年）

  - [哨聲響起](../Page/哨聲響起.md "wikilink")（2002年）

  - [炸彈小新娘](../Page/炸彈小新娘.md "wikilink")（2002年）

  - [機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")（2002年）

  - [THEビッグオー For Second Season](../Page/魅影巨神.md "wikilink")（2002年）

  - [驚爆危機校園篇](../Page/驚爆危機.md "wikilink")（2003年）

  - [神槍少女](../Page/神槍少女.md "wikilink")（2003年）

  - [超變身巫女祈鬥士](../Page/超變身巫女祈鬥士.md "wikilink")（2004年）

  - [超變身角色扮演前傳](../Page/超變身角色扮演前傳.md "wikilink")（2004年）

  - [LOVE♥LOVE?](../Page/LOVE♥LOVE?.md "wikilink")（2004年）

  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")（2004年）

  - [次元艦隊](../Page/次元艦隊.md "wikilink")（2004年）

  - [驚爆危機 The Second Raid](../Page/驚爆危機.md "wikilink")（2005年）

  - [極速方程式](../Page/極速方程式.md "wikilink")（2005年）

  - [SIMOUN](../Page/SIMOUN.md "wikilink")（2006年）

  - [BLACK BLOOD
    BROTHERS](../Page/BLACK_BLOOD_BROTHERS.md "wikilink")（2006年）

  - [家庭教師HITMAN
    REBORN\!](../Page/家庭教師HITMAN_REBORN!.md "wikilink")（2006年）

  - [元素獵人](../Page/元素獵人.md "wikilink")（2009年）

  - [いばらの王](../Page/いばらの王.md "wikilink")/古城荊棘王King of Thorn(2010年)

  - [Sacred Seven](../Page/Sacred_Seven.md "wikilink")（2011年）

  - [圣斗士星矢Ω](../Page/圣斗士星矢Ω.md "wikilink")（2012年）

### 音樂劇、舞台劇

動畫關連

  - [-{zh-cn:缎带魔法少女;zh-hk:百變小姬子;zh-tw:緞帶魔法姬;}-](../Page/百變小姬子.md "wikilink")（1993年）
  - [小紅帽恰恰](../Page/小紅帽恰恰.md "wikilink")（1994年）
  - [りりかSOS](../Page/りりかSOS.md "wikilink")（1995年）
  - [水色時代](../Page/水色時代.md "wikilink")（1996年）
  - [ケロケロちゃいむ](../Page/ケロケロちゃいむ.md "wikilink")（1997年）
  - [発明BOYカニパン](../Page/発明BOYカニパン.md "wikilink")（1998年）
  - [リカちゃん](../Page/麗佳公主.md "wikilink")（1999年）
  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")（1999年・2003年・2006年 - ）
  - [Hunter × Hunter](../Page/Hunter_×_Hunter.md "wikilink")（2001年 - ）
  - [網球王子舞台劇](../Page/網球王子舞台劇.md "wikilink")（2003年 - 2010年）
      - [網球王子舞台劇第二季](../Page/網球王子舞台劇第二季.md "wikilink")（2011年 - 2014）
  - [銀河天使](../Page/銀河天使.md "wikilink")（2005年 - ）
  - [忍者イリュージョンNARUTO](../Page/忍者イリュージョンNARUTO.md "wikilink")（2006年）
  - [飛輪少年](../Page/飛輪少年.md "wikilink")（2007年 -）

### 獎項

  - [新城勁爆兒歌頒獎禮2008](../Page/2008年度新城勁爆兒歌頒獎禮得獎名單.md "wikilink")－－「新城勁爆兒歌」-
    《[響](../Page/響.md "wikilink")》（改編布施明原唱之《始まりの君へ》）

## 外部連結

  - [官方網頁](http://www.face-music.co.jp/2_artist/sahashi.htm)
  - [動畫網路新聞 -
    佐橋俊彥](http://www.animenewsnetwork.com/encyclopedia/people.php?id=114)

[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:動畫相關人物](../Category/動畫相關人物.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")