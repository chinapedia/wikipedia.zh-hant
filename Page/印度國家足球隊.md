**印度國家足球隊**是[印度官方的男子國家足球代表隊](../Page/印度.md "wikilink")，由[印度足球總會負責管轄](../Page/印度足球總會.md "wikilink")，現時屬於[國際足協及](../Page/國際足協.md "wikilink")[亞洲足協成員國之一](../Page/亞洲足協.md "wikilink")。印度在國際賽事並不突出，至今從未參與世界盃足球賽決賽週，亦只曾經於1964年、1984年、2011年、2019年晉身過4屆[亞洲盃足球賽的決賽周](../Page/亞洲盃足球賽.md "wikilink")。2011年的亞洲杯，印度在分組賽三戰全敗小組末席出局。

1950年世界盃足球賽外圍賽對手相繼退出，印度曾經有機會晉身[1950年世界盃足球賽決賽週](../Page/1950年世界盃足球賽.md "wikilink")。可是，由於國際足協要求所有球員必須穿著足球鞋，而眾多印度足球員拒絕遵守該項守則，印度惟有退出世界盃。印度亦曾經在[1956年夏季奧林匹克運動會奪得第四](../Page/1956年夏季奧林匹克運動會.md "wikilink")，又在[1962年亞洲運動會奪得金牌](../Page/1962年亞洲運動會.md "wikilink")，更在其後兩屆亞運會打進四強，當時可謂印度足球的黃金時代。

印度近期的成績不好，只有在英藉教練[Stephen
Constantine領導下](../Page/Stephen_Constantine.md "wikilink")，在2002年LG盃氣走[越南](../Page/越南國家足球隊.md "wikilink")，及於其後的[2005年南亞足球錦標賽奪標而回](../Page/2005年南亞足球錦標賽.md "wikilink")。從2006年6月至今印度由英藉教練[鲍比·霍顿任教](../Page/鲍比·霍顿.md "wikilink")。他於過往30年曾執教非洲，亞洲，中東，北美及歐洲的球隊，並有不俗的成績。

印度隊的球衣贊助商是[耐克](../Page/Nike.md "wikilink")（Nike）。

## 世界盃成績

  - [1930年至](../Page/1930年世界盃足球賽.md "wikilink")[1938年](../Page/1938年世界盃足球賽.md "wikilink")-沒有參賽
  - [1950年](../Page/1950年世界盃足球賽.md "wikilink")-晉級後退出
  - [1954年](../Page/1954年世界盃足球賽.md "wikilink")-被國際足協禁止參賽
  - [1958年至](../Page/1958年世界盃足球賽.md "wikilink")[1970年](../Page/1970年世界盃足球賽.md "wikilink")-沒有參賽
  - [1974年至](../Page/1974年世界盃足球賽.md "wikilink")[2018年](../Page/2018年世界盃足球賽.md "wikilink")-外圍賽出局

## 奥运会足球赛成績

### 1960年奥运会

-----

-----

### 1956年奥运会

-----

-----

### 1952年奥运会

### 1948年奥运会

## 亞洲盃成績

  - [1956年](../Page/1956年亞洲盃足球賽.md "wikilink")-沒有參賽
  - [1960年](../Page/1960年亞洲盃足球賽.md "wikilink")-外圍賽
  - [1964年](../Page/1964年亞洲盃足球賽.md "wikilink")-**亞軍**
  - [1968年](../Page/1968年亞洲盃足球賽.md "wikilink")-外圍賽
  - [1972年至](../Page/1972年亞洲盃足球賽.md "wikilink")[1980年](../Page/1980年亞洲盃足球賽.md "wikilink")-沒有參賽
  - [1984年](../Page/1984年亞洲盃足球賽.md "wikilink")-第一圈
  - [1988年至](../Page/1988年亞洲盃足球賽.md "wikilink")[2007年](../Page/2007年亞洲盃足球賽.md "wikilink")-外圍賽
  - [2011年](../Page/2011年亞洲盃足球賽.md "wikilink")-第一圈
  - [2015年](../Page/2015年亞洲盃足球賽.md "wikilink")-外圍賽
  - [2019年](../Page/2019年亞洲盃足球賽.md "wikilink")-第一圈

## 南亞錦標賽成績

  - 1993年-**冠軍**
  - 1995年-亞軍
  - 1997年-**冠軍**
  - 1999年-**冠軍**
  - 2003年-季軍
  - 2005年-**冠軍**

## 亞洲挑戰盃成績

  - [2006年](../Page/2006年亞洲挑戰盃.md "wikilink")-八強
  - [2008年](../Page/2008年亞洲挑戰盃.md "wikilink")-**冠軍**
  - [2010年](../Page/2010年亞洲挑戰盃.md "wikilink")-小組賽
  - [2012年](../Page/2012年亞洲挑戰盃.md "wikilink")-小組賽
  - [2014年](../Page/2014年亞洲挑戰盃.md "wikilink")-會外賽出局

## 近期賽事

## 現有陣容

|----- bgcolor="\#EEEEEE" | colspan="7" align="left" |
**[守門員](../Page/守門員.md "wikilink")**    |-----
bgcolor="\#EEEEEE" | colspan="7" align="left" |
**[後衛](../Page/後衛_\(足球\).md "wikilink")**          |-----
bgcolor="\#EEEEEE" | colspan="7" align="left" |
**[中場](../Page/中場.md "wikilink")**            |-----
bgcolor="\#EEEEEE" | colspan="7" align="left" |
**[前鋒](../Page/前鋒_\(足球\).md "wikilink")**

## 外部連結

  - [印度足球總會官方網站](http://www.the-aiff.com)
  - [印度足球明星般迪亞簡介網頁](https://web.archive.org/web/20070504051509/http://www.freewebtown.com/kachun/)
  - [般迪亞拍Nike廣告短片](http://www.youtube.com/watch?v=l2aPBU7X2dg)

[Category:印度足球](../Category/印度足球.md "wikilink")
[Category:亞洲足球代表隊](../Category/亞洲足球代表隊.md "wikilink")