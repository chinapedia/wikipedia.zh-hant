**诺基亚
6230i**是[诺基亚公司在](../Page/诺基亚.md "wikilink")2005年上半年推出的一款商务音乐[手机](../Page/手机.md "wikilink")。是对于诺基亚
6230的升级版。采用了S40的[操作系统](../Page/操作系统.md "wikilink")。

6230i照相手機是 Nokia 6230 的進階版，新增 130 萬畫素相機、即按即說（PoC）等功能。內建的 208 x 208
畫素新螢幕，更可滿足重視影音功能的商務型玩家。

Nokia 6230i 的高解析度螢幕（208 x 208 畫素）非常適合播放多媒體內容，更能展現出內建 130 萬畫素、3
倍數位變焦相機所拍攝的精彩畫面。使用 Nokia 6230i 隨機附贈的 MMC 記憶卡，即可錄製長達
1 小時的影片。

Nokia 6230i 相關功能列表：

  - 內建 130 萬相機鏡頭、3 倍數位變焦
  - 內建 32 MB 記憶體，可儲存通訊錄、訊息、多媒體訊息、鈴聲、行事曆
  - 支援 MMC 卡擴充(最高可到 2GB)
  - 支援最高一小時影音錄影
  - 支援 3GPP、H.263 video、MPEG-4、AMR 等影片格式
  - 支援 64 和絃
  - 支援 MIDI、原音鈴聲、AAC、MP3、M4a 等鈴聲
  - 支援 SMTP、POP3、IMAP4 等 e-mail 格式
  - 支援[即按即說](../Page/即按即說.md "wikilink") PoC
  - 支援[藍芽](../Page/藍芽.md "wikilink")、USB
    埠、[紅外線](../Page/紅外線.md "wikilink")
  - 支援簡訊最多 150 則、通訊錄 1000 筆

[en:Nokia
6230\#Nokia_6230i](../Page/en:Nokia_6230#Nokia_6230i.md "wikilink")

[6230i](../Category/诺基亚手机.md "wikilink")
[Category:2005年面世的手機](../Category/2005年面世的手機.md "wikilink")