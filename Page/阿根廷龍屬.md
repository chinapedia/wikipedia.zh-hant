**阿根廷龍屬**（[屬名](../Page/學名.md "wikilink")：，意為「阿根廷的蜥蜴」）是[蜥腳下目](../Page/蜥腳下目.md "wikilink")[泰坦巨龍類的一個屬](../Page/泰坦巨龍類.md "wikilink")，是種[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，可能是[地球上曾经生活过的体型最巨大的陆地动物](../Page/地球.md "wikilink")。牠們在[勞亞古陸](../Page/勞亞古陸.md "wikilink")[侏羅紀的近親](../Page/侏羅紀.md "wikilink")（如[迷惑龍](../Page/迷惑龍.md "wikilink")）滅絕後，才於[白堊紀中期的](../Page/白堊紀.md "wikilink")[南美洲出現](../Page/南美洲.md "wikilink")，即距今約1億年前。

## 描述

[Argentinosaurus_BW.jpg](https://zh.wikipedia.org/wiki/File:Argentinosaurus_BW.jpg "fig:Argentinosaurus_BW.jpg")
[Longest_dinosaurs1.png](https://zh.wikipedia.org/wiki/File:Longest_dinosaurs1.png "fig:Longest_dinosaurs1.png")(紅)、[馬門溪龍](../Page/馬門溪龍.md "wikilink")(綠)、[超龍](../Page/超龍.md "wikilink")(橘)、[梁龍](../Page/梁龍.md "wikilink")(紫)、[富塔隆柯龍](../Page/富塔隆柯龍.md "wikilink")(藍)\]\]
沒有太多阿根廷龍的[化石被發現](../Page/化石.md "wikilink")。[正模標本](../Page/正模標本.md "wikilink")（編號PVPH-1）包含：三節前段背椎、三節後段背椎、有第1到5節薦椎（都只有椎體的腹側）、數個破碎的背肋、一個右側薦肋、右[脛骨](../Page/脛骨.md "wikilink")。第一背椎的高度（神經棘頂端到椎體腹側）是1.59公尺，而脛骨約為1.55米長。\[1\]另外，一根不完整的[股骨](../Page/股骨.md "wikilink")（編號MLP-DP
46-VIII-21-3）也被歸類於阿根廷龍，是一節不完整的股骨幹，長1.18公尺。\[2\]

在1990年代，葛瑞格利·保羅（Gregory S. Paul）、T.
Appenzeller曾多次提出阿根廷龍的身長、體重估計值，身長估計值約35公尺、體重估計值多介於90到100公噸之間。\[3\]\[4\]
但當時缺乏較完整的[泰坦巨龍類化石可供比較](../Page/泰坦巨龍類.md "wikilink")。近年來，有較為完整的泰坦巨龍類化石發現，例如[薩爾塔龍](../Page/薩爾塔龍.md "wikilink")、[後凹尾龍](../Page/後凹尾龍.md "wikilink")、[掠食龍](../Page/掠食龍.md "wikilink")，使科學家可以估計出泰坦巨龍類的較準確體型。在2006年，肯尼思·卡彭特（Kenneth
Carpenter）的一份[雙腔龍與其他巨型蜥腳類恐龍的研究中](../Page/雙腔龍.md "wikilink")，估計阿根廷龍的身長是30公尺。\[5\]在湯瑪斯·霍爾茲的2008年版恐龍列表裡，則將阿根廷龍的身長記為36.6公尺。\[6\]

在2004年，佩爾·克里斯坦森等人的[南美洲白堊紀恐龍研究裡](../Page/南美洲.md "wikilink")，提出正模標本裡的脛骨其實是個[腓骨](../Page/腓骨.md "wikilink")，並將新發現的股骨列入計算，參照其他13種蜥腳類恐龍的骨頭密度，計算出阿根廷龍的骨頭密度介於60到88公噸，最有可能是73公噸。\[7\]

## 分類及歷史

[Argentinosaurus_9.svg](https://zh.wikipedia.org/wiki/File:Argentinosaurus_9.svg "fig:Argentinosaurus_9.svg")
[Museo_de_La_Plata_-_Argentinosaurus_(fémur).jpg](https://zh.wikipedia.org/wiki/File:Museo_de_La_Plata_-_Argentinosaurus_\(fémur\).jpg "fig:Museo_de_La_Plata_-_Argentinosaurus_(fémur).jpg")，位於阿根廷\]\]
阿根廷龍的[模式種是](../Page/模式種.md "wikilink")**烏因庫爾阿根廷龍**（*Argentinosaurus
huinculensis*），是由[阿根廷](../Page/阿根廷.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[約瑟·波拿巴](../Page/約瑟·波拿巴.md "wikilink")（José
F. Bonaparte）及[羅多爾夫·科里亞](../Page/羅多爾夫·科里亞.md "wikilink")（Rodolfo
Coria）於1993年發表及命名的。牠是屬於[白堊紀](../Page/白堊紀.md "wikilink")[阿爾布階至](../Page/阿爾布階.md "wikilink")[森諾曼階](../Page/森諾曼階.md "wikilink")，距今約112.2-93.5百萬年前。發現[化石的地點是位於阿根廷](../Page/化石.md "wikilink")[內烏肯省的](../Page/內烏肯省.md "wikilink")[利邁河地層](../Page/利邁河地層.md "wikilink")。

## 大眾文化

阿根廷龍出現於《[與恐龍共舞](../Page/與恐龍共舞.md "wikilink")》（*Walking with
Dinosaurs*）的特別節目《巨龍國度》（*Land of
Giants*），在節目中一群阿根廷龍走到河岸以產下牠們的蛋，但在途中遭到一群[南方巨獸龍所獵食](../Page/南方巨獸龍.md "wikilink")。阿根廷龍也出現在在[IMAX電影](../Page/IMAX電影.md "wikilink")《巴塔哥尼亞恐龍》（*Dinosaurs:
Giants of
Patagonia*），在電影中[羅多爾夫·科里亞](../Page/羅多爾夫·科里亞.md "wikilink")（Rodolfo
Coria）介紹在[阿根廷的主要挖掘地點](../Page/阿根廷.md "wikilink")。

## 參考資料

  - Haines, Tim & Chambers, Paul. (2006) The Complete Guide to
    Prehistoric Life. Canada: Firefly Books Ltd.

## 外部連結

  - [阿根廷龍脊骨的實際大小（附有相片）](https://web.archive.org/web/20040501182902/http://www.hcc.hawaii.edu/~pine/Phil100/argentinosaurus.html)

  - [阿根廷龍的標本詳述 -
    Dinodata](https://web.archive.org/web/20090127051344/http://www.dinodata.org/index.php?option=com_content&task=view&id=5932&Itemid=67)

  - [阿根廷龍 - 中國恐龍網](http://site.sinodino.com/Museum/Argentinosaurus.htm)

  - [阿根廷龍的簡介](http://www.interpatagonia.com/paseos/carmen_funes/index_i.html)
    - The Carmen Funes Municipal Museum

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:泰坦巨龍類](../Category/泰坦巨龍類.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")

1.

2.

3.

4.

5.

6.  Holtz, Thomas R. Jr. (2008) *Dinosaurs: The Most Complete,
    Up-to-Date Encyclopedia for Dinosaur Lovers of All Ages*
    [Supplementary
    Information](http://www.geol.umd.edu/~tholtz/dinoappendix/DinoappendixSummer2008.pdf)

7.