[DNA_sequence.png](https://zh.wikipedia.org/wiki/File:DNA_sequence.png "fig:DNA_sequence.png")

**核酸序列**（，亦称为**核酸的一级结构**）使用一串字母表示的真实的或者假设的携带[基因信息的DNA分子的](../Page/基因.md "wikilink")[一级结构](../Page/一级结构.md "wikilink")。每个字母代表一种[核鹼基](../Page/核鹼基.md "wikilink")，两个碱基形成一个碱基对，碱基对的配对规律是固定的，A=T,C≡G。三个相邻的碱基对形成一个密码子。一种密码子对应一种氨基酸，不同的氨基酸合成不同的蛋白质。在DNA的复制及蛋白质的合成过程中，碱基配对规律是十分关键的。

可能的字母只有*A*, *C*,
*G*和*T*，分别代表组成DNA的四种[核苷酸](../Page/核苷酸.md "wikilink")－[腺嘌呤](../Page/腺嘌呤.md "wikilink")，[胞嘧啶](../Page/胞嘧啶.md "wikilink")，[鸟嘌呤](../Page/鸟嘌呤.md "wikilink")，[胸腺嘧啶](../Page/胸腺嘧啶.md "wikilink")。典型的他们无间隔的排列在一起，例如序列AAAGTCTGAC。任意长度大于4的一串核苷酸被称作一个序列。
关于它的生物功能，则依赖于上下文的序列，一个序列可能被正读，反读；包含[编码或者](../Page/遗传密码.md "wikilink")[无编码](../Page/非编码DNA.md "wikilink")。DNA序列也可能包含[非編碼DNA](../Page/非編碼DNA.md "wikilink")。

核酸也具有[二级结构和](../Page/核酸二级结构.md "wikilink")[三级结构](../Page/核酸三级结构.md "wikilink")
。 一级结构有时被错误地称为一级序列。 相反，没有并行的二级或三级序列概念。

## 核苷酸

[RNA_chemical_structure.GIF](https://zh.wikipedia.org/wiki/File:RNA_chemical_structure.GIF "fig:RNA_chemical_structure.GIF")
[RNA-codons.png](https://zh.wikipedia.org/wiki/File:RNA-codons.png "fig:RNA-codons.png")分子中的一系列密码子。
每个密码子由三个[核苷酸组成](../Page/核苷酸.md "wikilink")，通常代表单个[氨基酸](../Page/氨基酸.md "wikilink")。\]\]

[核酸由称为](../Page/核酸.md "wikilink")[核苷酸的连接单位的长链组成](../Page/核苷酸.md "wikilink")。
每个[核苷酸由三个亚基组成](../Page/核苷酸.md "wikilink")：磷酸基团和糖（在[RNA的情况下是](../Page/RNA.md "wikilink")[核糖](../Page/核糖.md "wikilink")，而在[DNA中的](../Page/DNA.md "wikilink")[脱氧核糖](../Page/脱氧核糖.md "wikilink")）构成核酸链的骨架，并且与糖连接是一组[核碱基之一](../Page/核碱基.md "wikilink")。
[核碱基在链的](../Page/核碱基.md "wikilink")[碱基对中是重要的](../Page/碱基对.md "wikilink")，以形成更高级的[二级结构和](../Page/核酸二级结构.md "wikilink")[三级结构](../Page/核酸三级结构.md "wikilink")，例如着名的[双螺旋](../Page/雙股螺旋.md "wikilink")。

可能的字母是A，C，G和T，代表DNA链的四个[核苷酸](../Page/核苷酸.md "wikilink")[碱基](../Page/碱基.md "wikilink")
-
[腺嘌呤](../Page/腺嘌呤.md "wikilink")，[胞嘧啶](../Page/胞嘧啶.md "wikilink")，[鸟嘌呤](../Page/鸟嘌呤.md "wikilink")，[胸腺嘧啶](../Page/胸腺嘧啶.md "wikilink")
-
与[磷酸二酯骨架](../Page/磷酸二酯鍵.md "wikilink")[共价连接](../Page/共价键.md "wikilink")。
在典型情况下，序列无间隙地相互邻接被印刷，如AAAGTCTGAC序列中从[5'到3'方向从左到右读](../Page/方向性_\(分子生物学\).md "wikilink")。
关于[转录](../Page/转录.md "wikilink")，如果序列与转录的RNA具有相同的顺序，则序列位于编码链上。

一个序列可以与另一个序列，这意味着它们在互补的每个位置上具有碱基（即A至T，C至G）并且以相反的顺序。 例如，TTAC的互补序列是GTAA。
如果双链DNA的一条链被认为是有[义链](../Page/義_\(分子生物學\).md "wikilink")（sense
strand），那么被认为是反义链的另一条链将具有与有义链的互补序列。

## 符号

比较和确定两个核苷酸序列之间的％差异。

  - AA**T**CC**GC**TAG
  - AA**A**CC**CT**TAG
  - 给定两个10个核苷酸的序列，将它们排成一行并比较它们之间的差异。 通过将不同DNA碱基的数量除以核苷酸的总数来计算相似性百分比。
    在上述情况下，10个核苷酸序列存在三个差异。 因此，将7/10除以得到70％的相似度，并从100％减去得到30％的差异。

虽然A，T，C和G代表某个位置的特定核苷酸，但也有代表模糊性的字母，当在该位置可能出现一种以上的核苷酸时使用这些字母。
国际纯粹与应用化学联合会（[IUPAC](../Page/國際純化學和應用化學聯合會.md "wikilink")）的规则如下\[1\]：

| 符号\[2\] | 描述                                 | 被表现碱基 | 互补碱基 |
| ------- | ---------------------------------- | ----- | ---- |
| **A**   | [腺嘌呤](../Page/腺嘌呤.md "wikilink")   | A     |      |
| **C**   | [胞嘧啶](../Page/胞嘧啶.md "wikilink")   |       | C    |
| **G**   | [鸟嘌呤](../Page/鸟嘌呤.md "wikilink")   |       |      |
| **T**   | [胸腺嘧啶](../Page/胸腺嘧啶.md "wikilink") |       |      |
| **U**   | [尿嘧啶](../Page/尿嘧啶.md "wikilink")   |       |      |
| **W**   | **W**eak                           | A     |      |
| **S**   | **S**trong                         |       | C    |
| **M**   | [胺](../Page/胺.md "wikilink")       | A     | C    |
| **K**   | [酮](../Page/酮.md "wikilink")       |       |      |
| **R**   | [嘌呤](../Page/嘌呤.md "wikilink")     | A     |      |
| **Y**   | [嘧啶](../Page/嘧啶.md "wikilink")     |       | C    |
| **B**   | 除A外 (**B** comes after A)          |       | C    |
| **D**   | 除C外 (**D** comes after C)          | A     |      |
| **H**   | 除G外 (**H** comes after G)          | A     | C    |
| **V**   | 除T外 (**V** comes after T and U)    | A     | C    |
| **N**   | 任何 **N**ucleotide (not a gap)      | A     | C    |
| **Z**   | [0](../Page/0.md "wikilink")       |       |      |

这些符号对RNA也有效，除了用U（尿嘧啶）代替T（胸腺嘧啶）\[3\]。

除了腺嘌呤（A），胞嘧啶（C），鸟嘌呤（G），胸腺嘧啶（T）和尿嘧啶（U）之外，DNA和RNA还含有在核酸链形成后已被修饰的碱基。
在DNA中，最常见的修饰碱是（m5C）。
在RNA中，有许多修饰的碱基，包括假尿苷（Ψ），二氢尿苷（D），肌苷（I），核糖胸苷（rT）和[7-甲基鸟苷](../Page/7-甲基鸟苷.md "wikilink")（m7G）\[4\]\[5\]。
[次黄嘌呤和](../Page/次黄嘌呤.md "wikilink")[黄嘌呤是通过诱变剂存在产生的许多碱中的两种](../Page/黄嘌呤.md "wikilink")，它们都通过脱氨作用（用羰基取代胺基）。
[次黄嘌呤是由](../Page/次黄嘌呤.md "wikilink")[腺嘌呤产生的](../Page/腺嘌呤.md "wikilink")，而[黄嘌呤是由](../Page/黄嘌呤.md "wikilink")[鸟嘌呤产生的](../Page/鸟嘌呤.md "wikilink")\[6\]
。
类似地，[胞嘧啶的脱氨基作用导致](../Page/胞嘧啶.md "wikilink")[尿嘧啶](../Page/尿嘧啶.md "wikilink")。

## 生物学意义

[Kooditabel.png](https://zh.wikipedia.org/wiki/File:Kooditabel.png "fig:Kooditabel.png")的一个描述，通过该[遗传密码将](../Page/遗传密码.md "wikilink")[核酸中包含的信息](../Page/核酸.md "wikilink")[翻译成](../Page/翻译_\(生物学\).md "wikilink")[蛋白质中的](../Page/蛋白质.md "wikilink")[氨基酸序列](../Page/氨基酸.md "wikilink")。\]\]

在生物系统中，[核酸含有活细胞用于构建特定](../Page/核酸.md "wikilink")[蛋白质的信息](../Page/蛋白质.md "wikilink")。
核酸链上的[核碱基序列通过细胞机器翻译成构成蛋白质链的](../Page/核碱基.md "wikilink")[氨基酸序列](../Page/氨基酸.md "wikilink")。
被称为一个[密码子(codon)的每组三个](../Page/遗传密码.md "wikilink")[碱基对应于单个氨基酸](../Page/碱基对.md "wikilink")，并且存在特定的遗传密码，通过该遗传密码，三个碱基的每种可能组合对应于特定[氨基酸](../Page/氨基酸.md "wikilink")。

[分子生物学的中心法则概述了使用核酸中包含的信息构建蛋白质的机制](../Page/分子生物学的中心法则.md "wikilink")。
[DNA被](../Page/DNA.md "wikilink")[转录成](../Page/转录.md "wikilink")[mRNA分子](../Page/mRNA.md "wikilink")，其进入[核糖体](../Page/核糖体.md "wikilink")，其中mRNA用作构建蛋白质链的模板。
由于核酸可以与具有互补序列的分子结合，因此在编码蛋白质的“有[义](../Page/義_\(分子生物學\).md "wikilink")”序列和本身无功能但可以与有义链结合的互补“反义”序列之间存在区别。

## 参考文献

## 参见

  - [單核苷酸多態性](../Page/單核苷酸多態性.md "wikilink")

  - [四進位](../Page/四進位.md "wikilink")

  - [脱氧核糖核酸](../Page/脱氧核糖核酸.md "wikilink")

  -
{{-}}

[Category:DNA](../Category/DNA.md "wikilink")

1.  [Nomenclature for Incompletely Specified Bases in Nucleic Acid
    Sequences](http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html),
    NC-IUB, 1984.

2.

3.
4.

5.

6.