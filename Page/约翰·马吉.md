**约翰·马吉**（，），籍貫[賓州](../Page/賓州.md "wikilink")[匹茲堡](../Page/匹茲堡.md "wikilink")，美国[传教士](../Page/传教士.md "wikilink")，以在[南京大屠杀期间救助中国平民而著名](../Page/南京大屠杀.md "wikilink")。

1912年至1940年5月，馬吉在[南京](../Page/南京.md "wikilink")[下关](../Page/下关区.md "wikilink")[挹江门外的](../Page/挹江门.md "wikilink")[道胜堂](../Page/道胜堂.md "wikilink")[教堂](../Page/教堂.md "wikilink")[传教](../Page/传教.md "wikilink")。南京大屠杀期间，马吉牧师担任国际[红十字会南京委员会主席以及](../Page/红十字会.md "wikilink")[南京安全区国际委员会委员](../Page/南京安全区.md "wikilink")。马吉牧师曾参与救助中国难民，并拍摄了日本军人屠杀中国人的纪录片。1946年，马吉牧师曾经在[日本](../Page/日本.md "wikilink")[东京设立的](../Page/东京.md "wikilink")[远东国际军事法庭上为日军南京大屠杀作证](../Page/远东国际军事法庭.md "wikilink")，称“日军到处杀害中国人”，但是当被问及“是你亲眼看到的吗？”时，马吉牧师回答称：“我亲眼看见的只有一起”。马吉牧师称该起日军射杀中国人事件发生在1937年12月17日，当时他在自家的阳台上看见一名中国人被日本兵杀害，马吉牧师描述的整个事件经过是一名中国人向一名日本兵做了什么之后忽然逃走，日本兵追上去后将其射杀\[1\]。马吉牧师所称的该起事件在其日记中虽然也有提及，但是日记中马吉牧师明确表示“我并没有真正看到杀害现场”\[2\]。

2000年8月2日，[南京市](../Page/南京市.md "wikilink")[下关区政府将马吉牧师曾经传教的](../Page/下关区.md "wikilink")[道胜堂教堂](../Page/道胜堂.md "wikilink")（今[南京市第十二中学图书馆](../Page/南京市第十二中学.md "wikilink")）命名为[约翰·马吉图书馆](../Page/约翰·马吉图书馆.md "wikilink")\[3\]。

## 参见

  - [南京大屠杀](../Page/南京大屠杀.md "wikilink")
  - [南京安全區](../Page/南京安全區.md "wikilink")
  - [魏特琳](../Page/魏特琳.md "wikilink")（[華群](../Page/魏特琳.md "wikilink")）
  - [约翰·拉贝](../Page/约翰·拉贝.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[M](../Category/在华基督教传教士.md "wikilink")
[M](../Category/美國新教徒.md "wikilink")
[M](../Category/南京大屠杀见证人.md "wikilink")
[M](../Category/1884年出生.md "wikilink")
[M](../Category/1953年逝世.md "wikilink")
[Category:南京安全区国际委员会成员](../Category/南京安全区国际委员会成员.md "wikilink")
[Category:国际红十字会南京委员会成员](../Category/国际红十字会南京委员会成员.md "wikilink")

1.  《南京大残虐事件資料集1》青木書店1985,p103
2.  [滝谷二郎](../Page/滝谷二郎.md "wikilink")《目撃者の南京事件　発見されたマギー牧師の日記》（三交社、1997)
3.