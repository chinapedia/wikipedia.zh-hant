《**2002**》是一部[科幻電影](../Page/科幻電影.md "wikilink")，由[謝霆鋒](../Page/謝霆鋒.md "wikilink")、[馮德倫](../Page/馮德倫.md "wikilink")、[李璨琛主演](../Page/李璨琛.md "wikilink")。

## 演員

### 主要角色

  - [謝霆鋒](../Page/謝霆鋒.md "wikilink") 飾 游邦潮（潮）
  - [馮德倫](../Page/馮德倫.md "wikilink") 飾 鄭庭風（風）
  - [李璨琛](../Page/李璨琛.md "wikilink") 飾 Sam
  - [李彩樺](../Page/李彩樺.md "wikilink") 飾 Rain
  - [Danielle Graham](../Page/Danielle_Graham.md "wikilink") 飾
    護士Danielle
  - [方力申](../Page/方力申.md "wikilink") 飾 水鬼
  - [安雅](../Page/安雅_\(演員\).md "wikilink") 飾 火鬼
  - [羅家英](../Page/羅家英.md "wikilink") 飾 紙紮陳

### 客串角色

  - [谷德昭](../Page/谷德昭.md "wikilink") 飾 醫生
  - [李力持](../Page/李力持.md "wikilink") 飾 警察
  - [唐詩詠](../Page/唐詩詠.md "wikilink") 飾 女學生
  - [譚偉豪](../Page/譚偉豪.md "wikilink") 飾 豪仔
  - [侯煥玲](../Page/侯煥玲.md "wikilink") 飾 四婆

## 外部連結

  - [2002 -
    香港電影資料館](http://ipac.hkfa.lcsd.gov.hk:81/ipac20/ipac.jsp?session=12131E6505I75.909&profile=hkfa&uri=full=3100018@!28117@!1&ri=4&aspect=basic_search&menu=search&source=192.168.110.61@!horizon&ipp=20&staffonly=&term=2002&index=FILMBRP&uindex=&aspect=basic_search&menu=search&ri=4)

[Category:2001年電影](../Category/2001年電影.md "wikilink")
[Category:香港科幻片](../Category/香港科幻片.md "wikilink")
[Category:粵語電影](../Category/粵語電影.md "wikilink")
[Category:2000年代科幻片](../Category/2000年代科幻片.md "wikilink")
[Category:葉偉信電影](../Category/葉偉信電影.md "wikilink")
[Category:嘉禾電影](../Category/嘉禾電影.md "wikilink")