**5X5
(Five-by-Fives)**是[篮球的术语](../Page/篮球.md "wikilink")，指一場比賽中球員的個人表現在以下五項中達到至少5個的數目：[得分](../Page/得分.md "wikilink")、[籃板](../Page/籃板.md "wikilink")、[助攻](../Page/助攻.md "wikilink")、[抢断及](../Page/抢断.md "wikilink")[盖帽](../Page/盖帽.md "wikilink")\[1\]
。

搶斷與蓋帽在NBA1973-74賽季之前並不被記錄，所以5X5祇有可能出現在之後的賽季中。得分、籃板、助攻三者皆是非常容易達到的。而抢断及盖帽要達至5個已是一個防守高超的球員才可達到，而身材过高者灵活性受限不利于抢断，身材过矮者控制范围受限不利于盖帽，使抢断及盖帽同時有5個难上加难。而且，能進攻又可防守的球員更加萬中無一，必須集身高、跳躍力、反應靈敏，身手敏捷能能力於一身才可達到，因此達至5X5的球員皆為全能球員。曾多次達到5X5的球員只有[阿基姆·奥拉朱旺和](../Page/阿基姆·奥拉朱旺.md "wikilink")[安德烈·基里连科](../Page/安德烈·根納季耶維奇·基里連科.md "wikilink")（基於1985-86賽季以來的記錄）\[2\]。而現今可做到5X5的[NBA球員的例子有安德烈](../Page/NBA.md "wikilink")·基里连科、[马库斯·坎比](../Page/马库斯·坎比.md "wikilink")，基里连科是NBA歷史中唯一一名曾經於7日中2次達至5X5的球員。

2005年12月3日及12月10日基里连科曾做出NBA史上唯一一次於一星期內兩次做到5X5；而2006年1月3日基里连科更曾做出NBA史上唯一一次於48分鐘內做到的**5X6**（**Five-by-Sixes**）\[3\]。

## 曾做到5X5的球員

  - [阿基姆·奥拉朱旺](../Page/阿基姆·奥拉朱旺.md "wikilink")（已退役）6次

  - [大衛·羅賓遜](../Page/大衛·羅賓遜.md "wikilink")（已退役）1次

  - [德里克·科尔曼](../Page/德里克·科尔曼.md "wikilink")（已退役）1次

  - [弗拉德·迪瓦茨](../Page/弗拉德·迪瓦茨.md "wikilink") （已退役）1次

  - 1次

  - [马库斯·坎比](../Page/马库斯·坎比.md "wikilink") 1次

  - [安德烈·基里连科](../Page/安德烈·根納季耶維奇·基里連科.md "wikilink")（已退役）3次

  - [尼古拉·巴通姆](../Page/尼古拉·巴通姆.md "wikilink") 1次

  - [德雷蒙德·格林](../Page/德雷蒙德·格林.md "wikilink") 1次

  - [安東尼·戴維斯](../Page/安東尼·戴維斯_\(籃球運動員\).md "wikilink") 1次

  - [優素福·諾基奇](../Page/優素福·諾基奇.md "wikilink") 1次

## 接近5X5的紀錄(僅列舉部分例子)

  - [德雷蒙德·格林於](../Page/德雷蒙德·格林.md "wikilink")2017年2月10日，在[金州勇士對](../Page/金州勇士.md "wikilink")[曼菲斯灰熊的比賽中拿下](../Page/曼菲斯灰熊.md "wikilink")4分，12籃板，10助攻，10抄截，5阻攻，僅差1分即可完成5X5的成就。
  - [德馬庫斯·考辛斯於](../Page/德馬庫斯·考辛斯.md "wikilink")2017年2月23日，在[紐奧良鵜鶘對](../Page/紐奧良鵜鶘.md "wikilink")[休士頓火箭的比賽中拿下](../Page/休士頓火箭.md "wikilink")27分，14籃板，5助攻，5抄截，4阻攻，僅差1阻攻即可完成5x5的成就。此場賽事亦為其季中(全明星週)從[沙加緬度國王被交易至](../Page/沙加緬度國王.md "wikilink")[紐奧良鵜鶘後之首戰](../Page/紐奧良鵜鶘.md "wikilink")。
  - [德雷蒙德·格林於](../Page/德雷蒙德·格林.md "wikilink")2017年3月14日，在[金州勇士對](../Page/金州勇士.md "wikilink")[費城76人的比賽中拿下](../Page/費城76人.md "wikilink")20分，8籃板，8助攻，4抄截，6阻攻，僅差1抄截即可完成5X5的成就。
  - [詹姆斯·哈登於](../Page/詹姆斯·哈登.md "wikilink")2019年2月3日，在[休士頓火箭對](../Page/休士頓火箭.md "wikilink")[猶他爵士的比賽中拿下](../Page/猶他爵士.md "wikilink")43分，12籃板，5助攻，6抄截，4阻攻，僅差1阻攻即可完成5X5的成就。球隊也以125-98大勝！本場比賽也是哈登連續26場得分30+，僅次於[威爾特·張伯倫位居歷史第三](../Page/威爾特·張伯倫.md "wikilink")！

## 曾做到5X6的球員

  - [阿基姆·奥拉朱旺](../Page/阿基姆·奥拉朱旺.md "wikilink")（38分、17籃板、12助攻、7抢断、6盖帽）（於1987年的2次加時）
  - [安德烈·基里连科](../Page/安德烈·根納季耶維奇·基里連科.md "wikilink")（14分、8籃板、9助攻、6抢断、7盖帽）（於2006年1月3日NBA史上唯一一次於48分鐘內5X6）

## 參見

  - [兩双](../Page/兩双.md "wikilink")
  - [三双](../Page/三双.md "wikilink")
  - [四双](../Page/四双.md "wikilink")

## 資料來源

<references/>

[en:Double
(basketball)\#Five-by-five](../Page/en:Double_\(basketball\)#Five-by-five.md "wikilink")

[Category:籃球術語](../Category/籃球術語.md "wikilink")

1.

2.
3.