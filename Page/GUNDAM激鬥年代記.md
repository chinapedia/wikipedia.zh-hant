《**GUNDAM激鬥年代記**》（、-{GUNDAM BATTLE CHRONICLE}-）是由[Namco
Bandai發行的](../Page/万代南梦宫游戏.md "wikilink")[PlayStation
Portable主機專用的遊戲](../Page/PlayStation_Portable.md "wikilink")。本作品是[-{GUNDAM}-
BATTLE ROYALE的續集](../Page/GUNDAM_BATTLE_ROYALE.md "wikilink")。

## 概要

玩者扮演[吉翁公國或](../Page/吉翁公國.md "wikilink")[地球聯邦軍的士兵](../Page/地球聯邦軍.md "wikilink")。隨其中一個時代完結，玩者可以再度選擇新的勢力。本作新增了宇宙無重力、水中的戰爭，任務總數達160多個。此外追加了[機動戰士Z
GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")、[機動戰士GUNDAM 0083:Stardust
Memory](../Page/機動戰士GUNDAM_0083:Stardust_Memory.md "wikilink")、[機動戰士GUNDAM
ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")、[GUNDAM
SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")（僅機體）及[機動戰士GUNDAM
逆襲的夏亞的人物及機體](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")。

## 參戰作品

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")
  - [MOBILE SUIT VARIATION:
    MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM0080 口袋中的戰爭](../Page/機動戰士GUNDAM0080_口袋中的戰爭.md "wikilink")
  - [機動戰士GUNDAM外傳 THE BLUE
    DESTINY](../Page/機動戰士GUNDAM外傳_THE_BLUE_DESTINY.md "wikilink")
  - [機動戰士GUNDAM外傳 殖民地墮落之地](../Page/機動戰士GUNDAM外傳_殖民地墮落之地.md "wikilink")
  - [吉翁最前線 機動戰士GUNDAM0079](../Page/吉翁最前線_機動戰士GUNDAM0079.md "wikilink")
  - [機動戰士GUNDAM戰記 Lost War
    Chronicles](../Page/機動戰士GUNDAM戰記_Lost_War_Chronicles.md "wikilink")
  - [機動戰士GUNDAM CLIMAX
    U.C.](../Page/MOBILE_SUIT_GUNDAM_CLIMAX_U.C..md "wikilink")
  - [機動戰士GUNDAM MS戰線0079](../Page/機動戰士GUNDAM_MS戰線0079.md "wikilink")
  - [機動戰士GUNDAM外傳
    宇宙、閃光的盡頭…](../Page/機動戰士GUNDAM外傳_宇宙、閃光的盡頭….md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")（客串參戰）
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")（客串參戰）
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")（客串參戰）
  - [湯尼嶽崎的GUNDAM漫畫](../Page/湯尼嶽崎的GUNDAM漫畫.md "wikilink")
  - [機動戰士 鋼彈桑](../Page/機動戰士_鋼彈桑.md "wikilink")

## 參考資料

  - [-{【PSP】鋼彈激鬥年代記 -
    巴哈姆特}-](http://acg.gamer.com.tw/acgDetail.php?s=11563)
  - [](http://www.famitsu.com/game/coming/2007/08/01/104,1185975363,76200,0,0.html)

## 外部連結

  - [官方網站](http://www.bandaigames.channel.or.jp/list/psp_gundam_chronicle/)

[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[Category:GUNDAM系列電子遊戲](../Category/GUNDAM系列電子遊戲.md "wikilink")