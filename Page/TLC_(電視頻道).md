**TLC頻道**（****）是[探索傳播](../Page/探索傳播.md "wikilink")（）旗下其中的一個頻道，美國版本解作「The
Learning Channel」（學習頻道）；亞太版本則解作「Travel & Living Channel」（旅遊生活頻道）。

## 亞洲TLC頻道

主要播放遊歷險、廚藝烹飪和家居裝飾節目。Discovery旅遊生活頻道的前身為**探索旅遊歷險頻道**（****）及**探索旅遊生活頻道**（****）。前者以播放旅遊歷險節目為主，後者則加入家居生活節目。2010年9月1日起正式配合全球策略，改名為「TLC」。

## 探索旅遊生活頻道

探索旅遊生活頻道在2005年10月1日啟播，配合整個探索電視網重組，取代探索旅遊歷險頻道，定位為旅遊體驗與家居生活頻道。當時除[北美使用獨特的市場定位外](../Page/北美.md "wikilink")，[探索通信全球均使用Discovery旅遊生活頻道作為組合的一份子](../Page/探索通信.md "wikilink")；然而各個地區所播映的節目會隨版權和當地的文化而作出調節。現在TLC旅遊生活頻道擁有以下數個版本：亞洲、台灣(2003年開播)、印度、澳洲、紐西蘭、歐洲、英國、義大利、Viajar
y
Vivir（美國西班牙語版）和巴西／拉丁美洲，並與[中國大陸境內多家地方電視台合作](../Page/中國大陸.md "wikilink")、設立專屬節目播放區塊。

## 節目列表

### 亞太區重點節目

  - [勇闖天涯](../Page/勇闖天涯.md "wikilink")
  - [波登不設限](../Page/波登不設限.md "wikilink")
  - [L.A.刺青客](../Page/L.A.刺青客.md "wikilink")
  - [邁阿密刺青客](../Page/邁阿密刺青客.md "wikilink")
  - [暢遊拉丁美洲](../Page/暢遊拉丁美洲.md "wikilink")（）
  - [9點度假趣](../Page/9點度假趣.md "wikilink")（）
  - [決戰時裝伸展台](../Page/決戰時裝伸展台.md "wikilink")（）(第1季\~第14季)
  - [1000個必遊勝地](../Page/1000個必遊勝地.md "wikilink")（）
  - [決戰三千髮絲](../Page/決戰三千髮絲.md "wikilink")（）
  - [古怪食物](../Page/古怪食物.md "wikilink")
  - [帥哥廚師到我家](../Page/帥哥廚師到我家.md "wikilink")
  - [決戰明日彩妝](../Page/決戰明日彩妝.md "wikilink")
  - [酷男的異想世界](../Page/粉雄救兵.md "wikilink")（）
  - [蛋糕天王](../Page/蛋糕天王.md "wikilink")（Cake Boss）(第1季\~第6季)
  - [吃遍湄公河](../Page/吃遍湄公河.md "wikilink")
  - [大廚異想世界](../Page/大廚異想世界.md "wikilink")
  - [超冏刺青](../Page/超冏刺青.md "wikilink")
  - [美食大口吃](../Page/美食大口吃.md "wikilink")
  - [省錢出絕招](../Page/省錢出絕招.md "wikilink")
  - [省錢折價王](../Page/省錢折價王.md "wikilink")
  - [蛋糕天王來掌廚](../Page/蛋糕天王來掌廚.md "wikilink")
  - [奧利佛30分鐘上菜](../Page/奧利佛30分鐘上菜.md "wikilink")
  - [奧利佛15分鐘上菜](../Page/奧利佛15分鐘上菜.md "wikilink")
  - [奧利佛大英食典](../Page/奧利佛大英食典.md "wikilink")
  - [英國美食新勢力](../Page/英國美食新勢力.md "wikilink")
  - [穿金戴銀遊印度](../Page/穿金戴銀遊印度.md "wikilink")
  - [食在苦差事](../Page/食在苦差事.md "wikilink")
  - [全球怪餐廳](../Page/全球怪餐廳.md "wikilink")
  - [名模烘焙坊](../Page/名模烘焙坊.md "wikilink")
  - [寶玲的藝術廚房](../Page/寶玲的藝術廚房.md "wikilink")
  - [廚房女神奈潔拉](../Page/廚房女神奈潔拉.md "wikilink")
  - [艾默洛的美國原味](../Page/艾默洛的美國原味.md "wikilink")
  - [決戰蛋糕天王](../Page/決戰蛋糕天王.md "wikilink")
  - [沙雕大師](../Page/沙雕大師.md "wikilink")
  - [義式美食輕鬆做](../Page/義式美食輕鬆做.md "wikilink")
  - [度假天堂](../Page/度假天堂.md "wikilink")
  - [我的夢幻婚紗](../Page/我的夢幻婚紗.md "wikilink")
  - [阿卡出國去](../Page/阿卡出國去.md "wikilink")
  - [越南家鄉味](../Page/越南家鄉味.md "wikilink")
  - [魔幻大廚赫斯頓](../Page/魔幻大廚赫斯頓.md "wikilink")
  - [名模家常菜](../Page/名模家常菜.md "wikilink")
  - [料理鬥陣俱樂部](../Page/料理鬥陣俱樂部.md "wikilink")
  - [非吃不可](../Page/非吃不可.md "wikilink")
  - [全球怪餐廳第一季](../Page/全球怪餐廳第一季.md "wikilink")
  - [全球怪餐廳第二季](../Page/全球怪餐廳第二季.md "wikilink")
  - [決戰好味道](../Page/決戰好味道.md "wikilink")
  - [波登闖異地](../Page/波登闖異地.md "wikilink")
  - [明星時尚之路](../Page/明星時尚之路.md "wikilink")
  - [海灘假期](../Page/海灘假期.md "wikilink")
  - [消夜大車拚](../Page/消夜大車拚.md "wikilink")
  - [烘焙兄弟第一季](../Page/烘焙兄弟第一季.md "wikilink")
  - [烘焙兄弟第二季](../Page/烘焙兄弟第二季.md "wikilink")(2014年)
  - [完全改造:超級減重篇第1季](../Page/完全改造:超級減重篇第1季.md "wikilink")
  - [完全改造:超級減重篇第2季](../Page/完全改造:超級減重篇第2季.md "wikilink")
  - [完全改造:超級減重篇第3季](../Page/完全改造:超級減重篇第3季.md "wikilink")(2014年)
  - [幫我做造型](../Page/幫我做造型.md "wikilink")
  - [名模輕鬆上菜](../Page/名模輕鬆上菜.md "wikilink")
  - [紐約刺青客第一季](../Page/紐約刺青客第一季.md "wikilink")
  - [紐約刺青客第二季](../Page/紐約刺青客第二季.md "wikilink")
  - [紐約刺青客第三季](../Page/紐約刺青客第三季.md "wikilink")
  - [時尚大買家](../Page/時尚大買家.md "wikilink")
  - [古怪美食](../Page/古怪美食.md "wikilink")
  - [波登過境](../Page/波登過境.md "wikilink")
  - 瑪莎與史奴比的晚餐趴[Martha & Snoop's Potluck Dinner
    Party](../Page/Martha_&_Snoop's_Potluck_Dinner_Party.md "wikilink")

### 台灣版特色節目

  - [瘋台灣](../Page/瘋台灣.md "wikilink")
  - [瘋台灣大挑戰](../Page/瘋台灣大挑戰.md "wikilink")
  - [玩美女人窩](../Page/玩美女人窩.md "wikilink")
  - [摩登新中國](../Page/摩登新中國.md "wikilink")
  - [龐銚敲敲門](../Page/龐銚敲敲門.md "wikilink")
  - [Maggie魔法料理](../Page/Maggie魔法料理.md "wikilink")
  - [帥哥好煮意](../Page/帥哥好煮意.md "wikilink")
  - [雙廚出任務](../Page/雙廚出任務.md "wikilink")
  - [高雄愛玩樂](../Page/高雄愛玩樂.md "wikilink")(2013年10月)
  - [瘋台灣全明星](../Page/瘋台灣全明星.md "wikilink")(2014年3月9日，晚上9點播出)
  - [瘋台灣首遊](../Page/瘋台灣首遊.md "wikilink")

## 其他節目

  - [全球首選綠住家](../Page/全球首選綠住家.md "wikilink")
  - [精選主題旅遊](../Page/精選主題旅遊.md "wikilink")
  - [台灣綠生活](../Page/台灣綠生活.md "wikilink")

## 參考資料

## 外部連結

  - [台灣TLC旅遊生活頻道](http://www.tlc-tw.com)
  - [西班牙語Discovery旅遊生活頻道](http://www.tudiscovery.com/programacion-de-tv/?type=day&channel_code=TLCL-SP)

[\*](../Category/探索頻道節目.md "wikilink")
[\*](../Category/探索傳播.md "wikilink")
[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")