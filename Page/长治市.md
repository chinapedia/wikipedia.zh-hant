**长治市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山西省下辖的](../Page/山西省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，地处山西省东南部，[太行山](../Page/太行山.md "wikilink")、[太岳山之间](../Page/太岳山.md "wikilink")。南界[晋城市](../Page/晋城市.md "wikilink")，西邻[临汾市](../Page/临汾市.md "wikilink")，北接[晋中市](../Page/晋中市.md "wikilink")，东与[河北省](../Page/河北省.md "wikilink")[邯郸市及](../Page/邯郸市.md "wikilink")[河南省](../Page/河南省.md "wikilink")[安阳市毗邻](../Page/安阳市.md "wikilink")。全市总面积13,955平方公里，人口342.04万。

## 历史沿革

长治市古为“上党”、“潞州”地域，其历史上常为兵家必争之地\[1\]。

[殷商时为黎国](../Page/殷商.md "wikilink")，属[冀州](../Page/冀州_\(古代\).md "wikilink")。[春秋时期曾为潞子婴儿国](../Page/春秋时期.md "wikilink")，后并于[晋国](../Page/晋国.md "wikilink")。[战国时](../Page/战国.md "wikilink")，此地域归属[韩国管辖](../Page/韩国_\(战国\).md "wikilink")。[周显王二十一年](../Page/周显王.md "wikilink")（前348年），韩国在此地设[上党郡](../Page/上党郡.md "wikilink")，后又归于[赵国](../Page/赵国.md "wikilink")。东汉[刘熙](../Page/刘熙.md "wikilink")《[释名](../Page/释名.md "wikilink")》曰：“党，所也。在山上，其所最高，故曰上也。”[秦王政二十六年](../Page/秦王政.md "wikilink")（前221年），[秦始皇统一中国](../Page/秦始皇.md "wikilink")，实行[郡县制](../Page/郡县制.md "wikilink")，上党郡为开国[36郡之一](../Page/秦朝行政区划.md "wikilink")。[西汉](../Page/西汉.md "wikilink")、[东汉沿用秦制](../Page/东汉.md "wikilink")，上党郡属[并州](../Page/并州.md "wikilink")。[汉](../Page/汉朝.md "wikilink")[建安十八年](../Page/建安_\(东汉\).md "wikilink")（213年），上党郡入[冀州](../Page/冀州_\(古代\).md "wikilink")。[三国时期](../Page/三国.md "wikilink")，[魏](../Page/魏_\(三国\).md "wikilink")[黄初元年](../Page/黄初.md "wikilink")（220年），上党郡复归并州。

[西晋](../Page/西晋.md "wikilink")[永兴元年](../Page/永兴_\(晋\).md "wikilink")（汉元熙三年，304年），[匈奴人](../Page/匈奴人.md "wikilink")[刘渊起兵反晋](../Page/刘渊.md "wikilink")，建国号为“[汉](../Page/汉赵.md "wikilink")”，上党郡归其管辖。[东晋](../Page/东晋.md "wikilink")[大兴二年](../Page/大兴_\(晋\).md "wikilink")（319年），[前赵](../Page/前赵.md "wikilink")[羯族将领](../Page/羯族.md "wikilink")[石勒与](../Page/石勒.md "wikilink")[刘曜决裂](../Page/刘曜.md "wikilink")，自立政权，后灭掉“前赵”，建立“[后赵](../Page/后赵.md "wikilink")”，上党郡归之。东晋[永和六年](../Page/永和_\(晋\).md "wikilink")（350年），[石虎](../Page/石虎.md "wikilink")（石勒之侄）养孙[冉闵乘后赵内乱尽杀石氏子孙及羯胡](../Page/冉闵.md "wikilink")，自立为帝，改国号魏，史称[冉魏](../Page/冉魏.md "wikilink")，上党郡归于[冉魏](../Page/冉魏.md "wikilink")。永和八年（352年），[前燕](../Page/前燕.md "wikilink")[慕容俊杀死](../Page/慕容俊.md "wikilink")[冉闵并称帝](../Page/冉闵.md "wikilink")，前燕版图扩大，收上党郡。东晋[太和五年](../Page/太和_\(晋\).md "wikilink")（370年），[前秦的](../Page/前秦.md "wikilink")[苻坚](../Page/苻坚.md "wikilink")、[王猛率部灭](../Page/王猛.md "wikilink")[前燕](../Page/前燕.md "wikilink")，而后几年内逐步统一中国北方，上党郡入[前秦](../Page/前秦.md "wikilink")。但鼎盛期后，其领地渐渐缩小。最终于东晋[太元十九年](../Page/太元_\(东晋\).md "wikilink")（394年）被[后秦和](../Page/后秦.md "wikilink")[西秦所灭](../Page/西秦.md "wikilink")。太元十一年（386年），[西燕皇族河东王](../Page/西燕.md "wikilink")[慕容永率众](../Page/慕容永.md "wikilink")[鲜卑贵族从](../Page/鲜卑.md "wikilink")[关中东归](../Page/关中平原.md "wikilink")，占据[长子](../Page/长子县.md "wikilink")（今长子县以西）称帝，上党郡归[西燕](../Page/西燕.md "wikilink")；同年，[前燕贵族](../Page/前燕.md "wikilink")[慕容垂定都中山](../Page/慕容垂.md "wikilink")（今河北[定州](../Page/定州.md "wikilink")），称帝，史称“[后燕](../Page/后燕.md "wikilink")”，并与同为宗室的[西燕争夺燕国的领导权](../Page/西燕.md "wikilink")。太元十九年（394年），[后燕](../Page/后燕.md "wikilink")[慕容垂杀死](../Page/慕容垂.md "wikilink")[慕容永及其公卿大將三十多人](../Page/慕容永.md "wikilink")，灭[西燕](../Page/西燕.md "wikilink")，上党郡归后燕。东晋[义熙三年](../Page/义熙_\(晋安帝\).md "wikilink")（407年），[北燕汉人](../Page/北燕.md "wikilink")[冯跋杀死](../Page/冯跋.md "wikilink")[慕容垂之子](../Page/慕容垂.md "wikilink")[慕容熙](../Page/慕容熙.md "wikilink")，推立[慕容云为帝](../Page/慕容云.md "wikilink")，后杀之自立为帝，延用国号“燕”，史称“[北燕](../Page/北燕.md "wikilink")”，上党郡归之。

[缩略图](https://zh.wikipedia.org/wiki/File:西魏･東魏･梁.PNG "fig:缩略图")

[北魏](../Page/北魏.md "wikilink")[太延二年](../Page/太延.md "wikilink")（436年）<sup>注：此处未用东晋年号，因东晋已于420年被[刘裕所灭](../Page/刘裕.md "wikilink")</sup>，北魏（386年[拓跋珪建国称帝](../Page/拓跋珪.md "wikilink")，建都今山西[大同](../Page/大同市.md "wikilink")）灭北燕并逐步统一中国北方，上党郡归之。534年至535年，北魏衰落，权臣鲜卑人[宇文泰拥立](../Page/宇文泰.md "wikilink")[北魏孝文帝的孙子](../Page/北魏孝文帝.md "wikilink")[元宝炬为帝](../Page/元宝炬.md "wikilink")，史称“[西魏](../Page/西魏.md "wikilink")”。同时，权臣[高欢拥立北魏孝文帝](../Page/高欢.md "wikilink")11岁的曾孙[元善见为帝](../Page/魏孝静帝.md "wikilink")，史称“[东魏](../Page/东魏.md "wikilink")”。上党郡属两魏交接地带，归属未明。但西魏治国屡胜东魏。西魏于557年被宇文泰所灭，建立[北周](../Page/北周.md "wikilink")。东魏于550年被高欢所灭，建立[北齐](../Page/北齐.md "wikilink")。北齐于577年被北周所灭，上党郡归之。[北周](../Page/北周.md "wikilink")[建德七年](../Page/建德_\(北周\).md "wikilink")（578年），[北周武帝](../Page/北周武帝.md "wikilink")[宇文邕分上党郡置](../Page/宇文邕.md "wikilink")[潞州](../Page/潞州.md "wikilink")，是为潞州建置之始，上党郡属潞州。潞州得名于潞子之国。

[隋](../Page/隋朝.md "wikilink")[开皇元年](../Page/开皇.md "wikilink")（北周[大定元年](../Page/大定_\(北周\).md "wikilink")，581年），[杨坚取代北周](../Page/杨坚.md "wikilink")，隋朝开国。开皇三年（583年），废上党郡，移潞州于[壶关](../Page/壶关.md "wikilink")。[大业元年](../Page/大业.md "wikilink")（605年），改潞州为上党郡，隶[冀州](../Page/冀州_\(古代\).md "wikilink")。619年，隋朝被[王世充所灭](../Page/王世充.md "wikilink")。随后，[唐朝建立](../Page/唐朝.md "wikilink")。此间未改行政区划。[唐](../Page/唐朝.md "wikilink")[武德年间](../Page/武德.md "wikilink")，改上党郡为潞州，并置[都督府](../Page/都督府.md "wikilink")。[唐玄宗](../Page/唐玄宗.md "wikilink")[开元十七年](../Page/开元.md "wikilink")（729年），以玄宗[李隆基曾任潞州](../Page/李隆基.md "wikilink")[别驾的原故](../Page/别驾.md "wikilink")，设大都督府，并置上党郡。开元二十一年（733年），潞州、上党郡属[河东道](../Page/河东道.md "wikilink")。[大历元年](../Page/大历.md "wikilink")（766年），置[昭义军](../Page/昭义军.md "wikilink")（至德初称为“上党节度使”，宝应初改称“泽潞节度使”）。[五代后](../Page/五代十国.md "wikilink")[唐庄宗初](../Page/唐庄宗.md "wikilink")，置潞州。[梁末帝时](../Page/梁末帝.md "wikilink")（913年－914年），改为[匡义军](../Page/匡义军.md "wikilink")。岁余，[后唐灭](../Page/后唐.md "wikilink")[后梁](../Page/后梁.md "wikilink")，改为[安义军](../Page/安义军.md "wikilink")。[后晋](../Page/后晋.md "wikilink")（936年－947年），复为[昭义军](../Page/昭义军.md "wikilink")。[后汉和](../Page/后汉.md "wikilink")[后周](../Page/后周.md "wikilink")（947－960）循旧制。

[宋朝时](../Page/宋朝.md "wikilink")，[太平兴国初](../Page/太平兴国.md "wikilink")（976为元年），改昭义军为[昭德军](../Page/昭德军.md "wikilink")，后为潞州。[北宋](../Page/北宋.md "wikilink")[元丰年间](../Page/元丰_\(北宋\).md "wikilink")，为[隆德府](../Page/隆德府.md "wikilink")、大都督府、上党郡、昭义军，隶[河东路](../Page/河东路.md "wikilink")。[建中靖国元年](../Page/建中靖国.md "wikilink")（1101年），昭义军改为[威胜军](../Page/威胜军.md "wikilink")。[崇宁三年](../Page/崇宁.md "wikilink")（1104年），复为隆德府，后为昭德军。[金代潞州隶](../Page/金代.md "wikilink")[河东南路](../Page/河东南路.md "wikilink")。[金](../Page/金朝.md "wikilink")[天会六年](../Page/天会_\(金朝\).md "wikilink")（1128年），置潞南辽沁观察处。[元朝潞州属](../Page/元朝.md "wikilink")[晋宁路](../Page/晋宁路.md "wikilink")。元初为[隆德府](../Page/隆德府.md "wikilink")（行都元帅府事）。[元太宗三年](../Page/元太宗.md "wikilink")（1231年），复为潞州，隶[平阳路](../Page/平阳路.md "wikilink")。[明](../Page/明朝.md "wikilink")[洪武元年](../Page/洪武.md "wikilink")（1368年），沿用元制。洪武初潞州隶行[中书省](../Page/中书省.md "wikilink")[布政使司](../Page/布政使司.md "wikilink")；洪武二年（1369年），隶山西中书省。九年（1376年），隶[布政司](../Page/布政司.md "wikilink")。[嘉靖八年](../Page/嘉靖.md "wikilink")（1529年）二月，升潞州为[潞安府](../Page/潞安府.md "wikilink")，设潞安兵备，分巡[冀南道](../Page/冀南道.md "wikilink")，治潞安。[清朝沿用明制](../Page/清朝.md "wikilink")，潞安府治今长治城。

[中华民国元年](../Page/中华民国.md "wikilink")（1912年），实行省、道、县三级制，废潞安府，原潞安府所领各县均属[冀宁道](../Page/冀宁道.md "wikilink")；同年4月，于今长治市置潞泽辽沁镇守使署。民国5年（1916年）改为潞泽辽沁营务处。民国13年（1924年），撤销营务处。民国19年（1930），撤销冀宁道，各县直隶山西省。民国26年（1937年），山西省政府（[阎锡山政权](../Page/阎锡山.md "wikilink")）置第三、第五专区。[中国抗日战争时期](../Page/中国抗日战争.md "wikilink")，[中国共产党以](../Page/中国共产党.md "wikilink")[太行](../Page/太行山.md "wikilink")、[太岳山为依托](../Page/太岳山.md "wikilink")，建立了根据地。[第二次国共内战时期](../Page/第二次国共内战.md "wikilink")，历经[上党战役](../Page/上党战役.md "wikilink")，长治于1945年10月8日由中共占领。当月中旬，长治市（县级）建立，成为中国共产党控制的地区中最早设立的城市之一，隶太行四专区。1946年6月，长治市直隶太行区。

1950年3月，长治市改为长治工矿区（县级）。1952年3月，工矿区复为长治市，并恢复为[省辖市](../Page/省辖市.md "wikilink")，由[长治专区代管](../Page/长治专区.md "wikilink")。1953年7月1日，长治市改由山西省直辖。1958年，长治专区改为[晋东南专区](../Page/晋东南专区.md "wikilink")，代管长治市。1971年，晋东南专区改为[晋东南地区](../Page/晋东南地区.md "wikilink")。1975年，长治市复由[山西省直辖](../Page/山西省.md "wikilink")，设城、郊两区。1983年9月，长治市辖城、郊两区和长治、潞城2县。1985年5月，晋东南地区撤消，晋东南地区所辖平顺、壶关、黎城、屯留、长子、武乡、沁源、襄垣、沁县划归长治市领属，长治市辖2区11县（市）。

2018年9月30日，《山西省人民代表大会常务委员会关于长治市部分行政区划调整若干问题的决定》公布，长治市城区和郊区撤销，合并设立长治市潞州区，以原城区和郊区的行政区域为新设潞州区的行政区域。长治县、屯留县和潞城市撤销，分别设立长治市上党区、屯留区和潞城区，以原长治县、屯留县和潞城市的行政区域分别为新设上党区、屯留区和潞城区的行政区域。同年11月23日，新设立的潞州区、上党区、屯留区、潞城区正式挂牌，长治市辖变为4区8县。

## 地理

## 政治

### 现任领导

<table>
<caption>长治市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党长治市委员会.md" title="wikilink">中国共产党<br />
长治市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/长治市人民代表大会.md" title="wikilink">长治市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/长治市人民政府.md" title="wikilink">长治市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议长治市委员会.md" title="wikilink">中国人民政治协商会议<br />
长治市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/孙大军.md" title="wikilink">孙大军</a>[2]</p></td>
<td><p><a href="../Page/郭康锋.md" title="wikilink">郭康锋</a>[3]</p></td>
<td><p><a href="../Page/杨勤荣.md" title="wikilink">杨勤荣</a>[4]</p></td>
<td><p><a href="../Page/许霞.md" title="wikilink">许霞</a>（女）[5]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/辽宁省.md" title="wikilink">辽宁省</a><a href="../Page/沈阳市.md" title="wikilink">沈阳市</a><a href="../Page/辽中区.md" title="wikilink">辽中区</a></p></td>
<td><p><a href="../Page/山西省.md" title="wikilink">山西省</a><a href="../Page/运城市.md" title="wikilink">运城市</a><a href="../Page/盐湖区.md" title="wikilink">盐湖区</a></p></td>
<td><p>山西省<a href="../Page/新绛县.md" title="wikilink">新绛县</a></p></td>
<td><p>山西省<a href="../Page/襄垣县.md" title="wikilink">襄垣县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年2月</p></td>
<td><p>2017年3月</p></td>
<td><p>2018年1月</p></td>
<td><p>2016年7月</p></td>
</tr>
</tbody>
</table>

### 行政区划

长治市现辖4个[市辖区](../Page/市辖区.md "wikilink")、8个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[潞州区](../Page/潞州区.md "wikilink")、[上党区](../Page/上党区.md "wikilink")、[屯留区](../Page/屯留区.md "wikilink")、[潞城区](../Page/潞城区.md "wikilink")
  - 县：[襄垣县](../Page/襄垣县.md "wikilink")、[平顺县](../Page/平顺县.md "wikilink")、[黎城县](../Page/黎城县.md "wikilink")、[壶关县](../Page/壶关县.md "wikilink")、[长子县](../Page/长子县.md "wikilink")、[武乡县](../Page/武乡县.md "wikilink")、[沁县](../Page/沁县.md "wikilink")、[沁源县](../Page/沁源县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>长治市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[6]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>140400</p></td>
</tr>
<tr class="odd">
<td><p>140403</p></td>
</tr>
<tr class="even">
<td><p>140404</p></td>
</tr>
<tr class="odd">
<td><p>140405</p></td>
</tr>
<tr class="even">
<td><p>140406</p></td>
</tr>
<tr class="odd">
<td><p>140423</p></td>
</tr>
<tr class="even">
<td><p>140425</p></td>
</tr>
<tr class="odd">
<td><p>140426</p></td>
</tr>
<tr class="even">
<td><p>140427</p></td>
</tr>
<tr class="odd">
<td><p>140428</p></td>
</tr>
<tr class="even">
<td><p>140429</p></td>
</tr>
<tr class="odd">
<td><p>140430</p></td>
</tr>
<tr class="even">
<td><p>140431</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>长治市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[7]（2010年11月）</p></th>
<th><p>户籍人口[8]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>长治市</p></td>
<td><p>3334565</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>城　区</p></td>
<td><p>483628</p></td>
<td><p>14.50</p></td>
</tr>
<tr class="even">
<td><p>郊　区</p></td>
<td><p>281213</p></td>
<td><p>8.43</p></td>
</tr>
<tr class="odd">
<td><p>长治县</p></td>
<td><p>340963</p></td>
<td><p>10.23</p></td>
</tr>
<tr class="even">
<td><p>襄垣县</p></td>
<td><p>270216</p></td>
<td><p>8.10</p></td>
</tr>
<tr class="odd">
<td><p>屯留县</p></td>
<td><p>263844</p></td>
<td><p>7.91</p></td>
</tr>
<tr class="even">
<td><p>平顺县</p></td>
<td><p>150955</p></td>
<td><p>4.53</p></td>
</tr>
<tr class="odd">
<td><p>黎城县</p></td>
<td><p>158541</p></td>
<td><p>4.75</p></td>
</tr>
<tr class="even">
<td><p>壶关县</p></td>
<td><p>291609</p></td>
<td><p>8.75</p></td>
</tr>
<tr class="odd">
<td><p>长子县</p></td>
<td><p>353266</p></td>
<td><p>10.59</p></td>
</tr>
<tr class="even">
<td><p>武乡县</p></td>
<td><p>182549</p></td>
<td><p>5.47</p></td>
</tr>
<tr class="odd">
<td><p>沁　县</p></td>
<td><p>172205</p></td>
<td><p>5.16</p></td>
</tr>
<tr class="even">
<td><p>沁源县</p></td>
<td><p>158702</p></td>
<td><p>4.76</p></td>
</tr>
<tr class="odd">
<td><p>潞城市</p></td>
<td><p>226874</p></td>
<td><p>6.80</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")3334564人\[9\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加195573人，增长6.23%。年平均增长率为0.61%。其中，男性人口为1710680人，占51.30%；女性人口为1623884人，占48.70%。总人口性别比（以女性为100）为105.34。0－14岁人口为584606人，占17.53%；15－64岁人口为2520823人，占75.60%
；65岁及以上人口为229135人，占6.87%。

## 资源

### 植物

特产有沁州黄小米、[党参](../Page/党参.md "wikilink")、紫团参、潞城中药大风丸、荫城铁货、潞麻、长子县乐器、草帽辫。

### 矿产

矿产以煤炭为主，兼有硫铁、硫磺、锰铁、钛、铜、铝矾土、油岩、石灰岩、大理石、白云石、石英、云母、石膏、陶土等。

## 经济

长治市是新兴的工业城市和晋东南工商业中心。农业以粮食种植业为主，是重要的粮食生产基地。乡镇工业成为农村经济的支柱。GDP长期位列山西省第二名。

## 交通

### 铁路

[太焦铁路](../Page/太焦铁路.md "wikilink")、[邯长铁路](../Page/邯长铁路.md "wikilink")、[瓦日铁路](../Page/瓦日铁路.md "wikilink")

### 公路

太长高速公路（[太原](../Page/太原.md "wikilink")-长治）、长晋高速公路（长治-[晋城](../Page/晋城.md "wikilink")）、长邯高速公路（长治-[邯郸](../Page/邯郸.md "wikilink")）、[长临高速公路](../Page/长临高速公路.md "wikilink")（长治-[临汾](../Page/临汾.md "wikilink")）、长安高速公路
（长治-安阳）、[207国道](../Page/207国道.md "wikilink")、[208国道](../Page/208国道.md "wikilink")、[309国道](../Page/309国道.md "wikilink")

### 航空

[长治王村机场](../Page/长治王村机场.md "wikilink")

### 公共交通

[长治公交](../Page/长治公交.md "wikilink")

## 教育

  - 高校有：[长治医学院](../Page/长治医学院.md "wikilink")、[长治学院](../Page/长治学院.md "wikilink")、山西机电职业技术学院等；
  - 高中有：[长治一中](../Page/长治一中.md "wikilink")、[长治二中](../Page/长治二中.md "wikilink")、长治三中、长治四中、长治五中、长治六中、长治七中、长治八中、长治九中、长治十中、长治十二中、长治十三中、长治十四中、长治十五中、长治十六中、长治十七中、长治十八中、长治十九中等市直高中、长治学院附属太行中学等。
  - 中职有：[长治卫生学校](../Page/长治卫生学校.md "wikilink")、[长治第一职业中学](../Page/长治第一职业中学.md "wikilink")、[长治第二职业中学](../Page/长治第二职业中学.md "wikilink")、长治市文化艺术学校等。

## 医疗

  - 三级甲等：[长治医学院附属和平医院](../Page/长治医学院附属和平医院.md "wikilink")、[长治医学院附属和济医院](../Page/长治医学院附属和济医院.md "wikilink")、[长治市第一人民医院](../Page/长治市第一人民医院.md "wikilink")
  - 二级甲等：[长治市妇幼保健院](../Page/长治市妇幼保健院.md "wikilink")、[长治市第二人民医院](../Page/长治市第二人民医院.md "wikilink")、[长治市中医医院](../Page/长治市中医医院.md "wikilink")、[长治市中医研究所附属医院](../Page/长治市中医研究所附属医院.md "wikilink")
  - 各县区、矿区、厂区职工医院

## 文化

  - [上党梆子是山西省的](../Page/上党梆子.md "wikilink")[四大梆子之一](../Page/四大梆子.md "wikilink")。
  - 尚有上党落子、长治鼓书、八音会等多样的文化艺术形式在这片土地上广泛流行。

### 方言

大部分县市方言属于[晋语](../Page/晋语.md "wikilink")[上党片](../Page/上党片.md "wikilink")。

## 名胜古迹

  - [上党门](../Page/上党门.md "wikilink")
  - [正觉寺](../Page/正觉寺_\(长治\).md "wikilink")
  - [法兴寺](../Page/法兴寺_\(长子\).md "wikilink")（[长子县](../Page/长子县.md "wikilink")）
  - [老顶山](../Page/老顶山.md "wikilink")－炎帝像、百草堂（城区）
  - [天下都城隍](../Page/天下都城隍.md "wikilink")（全国唯一一个不在城市内的城隍庙）
  - [长治县都城隍庙](../Page/长治县都城隍庙.md "wikilink")
  - [崇庆寺](../Page/崇庆寺.md "wikilink")（长子县）

## 参考资料

[Category:长治](../Category/长治.md "wikilink")
[Category:山西地级市](../Category/山西地级市.md "wikilink")
[晋](../Category/中国大城市.md "wikilink")
[3](../Category/全国文明城市.md "wikilink")
[鲁](../Category/国家卫生城市.md "wikilink")
[Category:中原经济区城市](../Category/中原经济区城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.