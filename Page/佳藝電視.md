**佳藝電視**（），簡稱**佳視**、**CTV**，是[香港第三間免費商營](../Page/香港.md "wikilink")[電視台](../Page/電視台.md "wikilink")，1975年9月7日下午6時正（[UTC+8](../Page/UTC+8.md "wikilink")）開台，\[1\]1978年8月21日[倒閉](../Page/倒閉.md "wikilink")。

## 歷史

### 開台及發展

1973年，根據[廣播發展工作小組的建議](../Page/廣播發展工作小組.md "wikilink")，[港英政府決定增加兩間無線電視台](../Page/港英政府.md "wikilink")\[2\]，並在同年3月邀請有興趣者參加。同年8月10日，港英政府發出兩個無線電視經營權，佳藝電視獲得的是「中文電視台」經營權，[麗的電視亦獲得新的](../Page/麗的電視.md "wikilink")「中英文電視台」經營權：這意味著佳藝電視跟其他兩間電視台（[無綫電視及麗的電視](../Page/電視廣播有限公司.md "wikilink")）不同，只可以營辦一個中文台。

1975年春季，佳藝電視曾將一批工作人員送往[台灣](../Page/台灣.md "wikilink")，接受[光啟文教視聽節目服務社訓練三個月](../Page/光啟文教視聽節目服務社.md "wikilink")\[3\]；這一年，[中國電視公司節目部經理](../Page/中國電視公司.md "wikilink")[翁炳榮協助佳藝電視的創建](../Page/翁炳榮.md "wikilink")。

佳藝電視的台徽是以六個成120度角之線條所組成的環狀[六角形](../Page/六角形.md "wikilink")，代表[古代中國](../Page/古代中國.md "wikilink")[儒家的](../Page/儒家.md "wikilink")[六藝](../Page/六藝.md "wikilink")「禮、樂、射、御、書、數」（而「御書數」被嘲成[香港粵語](../Page/香港粵語.md "wikilink")[俚語](../Page/俚語.md "wikilink")「預輸數」，意為「預料會失敗」\[4\]）；無獨有偶，六角形亦正好代表佳視的六個主要[股東](../Page/股東.md "wikilink")：[商業電台](../Page/香港商業電台.md "wikilink")、[怡和洋行](../Page/怡和洋行.md "wikilink")、《[星島日報](../Page/星島日報.md "wikilink")》、《[華僑日報](../Page/華僑日報.md "wikilink")》及《[工商日報](../Page/工商日報.md "wikilink")》。在佳視創台的六個股東當中，商業電台擁有最大的主導權，故由商台董事長[何世義](../Page/何佐芝.md "wikilink")（即何佐芝）兼任佳視總經理（《[工商日報](../Page/工商日報.md "wikilink")》東主[何世禮為其異母兄](../Page/何世禮.md "wikilink")）；直到1976年[林炳炎家族入股](../Page/林炳炎.md "wikilink")，則改由[林秀峰出任佳視總經理](../Page/林秀峰.md "wikilink")。商業電台更一度創辦「佳視[足球隊](../Page/足球隊.md "wikilink")」為佳視做勢，其後卻因佳視倒閉而作罷；值得一提，當時任職商業電台的[伍晃榮曾為該隊的領隊](../Page/伍晃榮.md "wikilink")。\[5\]

佳視總部「佳藝電視大廈」位於[九龍塘](../Page/九龍塘.md "wikilink")[廣播道](../Page/廣播道.md "wikilink")1號A，今「[香港電台電視大廈](../Page/香港電台.md "wikilink")」。佳視啓播後，廣播道所在的小山丘集結了香港五間電子傳媒的總部，因此被稱為「五台山」。\[6\]

更值得一提的是，當年佳藝電視一推出《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》，就轟動全港；而俏麗可人的[米雪因為演活了](../Page/米雪.md "wikilink")[黄蓉](../Page/黄蓉.md "wikilink")，成為當時香港最紅的女星，成為各大影視周刊最搶手的封面女郎，名氣日大，紅遍整個[東南亞](../Page/東南亞.md "wikilink")。佳視結束後，米雪旋即成為炙手可熱的女星，片約不絕，而當時[亞洲電視與無綫電視都爭相邀請米雪拍劇](../Page/亞洲電視.md "wikilink")；最後由於無綫電視出價極高，邀得米雪拍《[絕代雙驕](../Page/絕代雙驕.md "wikilink")》。

### 七月攻勢

[ctvjuly.jpg](https://zh.wikipedia.org/wiki/File:ctvjuly.jpg "fig:ctvjuly.jpg")
1978年初，佳視以高薪羅致無綫電視總經理[周梁淑怡及其多名主要下屬](../Page/周梁淑怡.md "wikilink")，包括[葉潔馨](../Page/葉潔馨.md "wikilink")、[劉天賜](../Page/劉天賜.md "wikilink")、[盧國沾](../Page/盧國沾.md "wikilink")、[林旭華](../Page/林旭華.md "wikilink")、[石少鳴](../Page/石少鳴.md "wikilink")，他們被統稱
「[佳視六君子](../Page/佳視六君子.md "wikilink")」。佳視又以高成本招請原任職於無綫電視的幕前及幕後人員效力，使全台職員人數由1978年初的約600人增加至800多人\[7\]。

周梁淑怡於1978年7月1日為佳視推出「七月攻勢」，並於香港進行大規模宣傳活動，包括連續多日於多份報章刊登全版廣告，又於[尖沙咀碼頭派發](../Page/尖沙咀碼頭.md "wikilink")[卡式錄音帶以宣傳新節目](../Page/卡式錄音帶.md "wikilink")。七月攻勢包括多部重頭劇集：[鄭裕玲](../Page/鄭裕玲.md "wikilink")、米雪主演的《[名流情史](../Page/名流情史.md "wikilink")》，[徐克導演](../Page/徐克.md "wikilink")、[余安安主演的](../Page/余安安.md "wikilink")《[金刀情俠](../Page/金刀情俠.md "wikilink")》，加上《推理劇場》、《急先鋒》，及[香港電視史首創的成人](../Page/香港電視史.md "wikilink")[清談節目](../Page/清談節目.md "wikilink")《[哈囉夜歸人](../Page/哈囉夜歸人.md "wikilink")》等。

可惜，七月攻勢推出後，收效及收視率增長皆未如預期；同時又因花費大量資金，令佳視的營運資金迅速耗盡。周梁淑怡與佳視六君子迅即引咎辭職\[8\]，周梁的呈辭於8月4日獲佳視董事局接納而觸發[羅生門事件](../Page/羅生門.md "wikilink")\[9\]，並由李雪廬接替總經理一職\[10\]。

### 倒閉

[RTHK_TelevisionHouse.jpg](https://zh.wikipedia.org/wiki/File:RTHK_TelevisionHouse.jpg "fig:RTHK_TelevisionHouse.jpg")
由於佳視不惜工本製作電視片集造成虧損嚴重，1978年8月21日早上，佳視突然發表聲明，宣佈因財政陷入困境而停止運作，並在總部大門外貼出告示，表示佳視當日起停播。同時有佳視職員向[法庭申請將佳視](../Page/法庭.md "wikilink")[清盤](../Page/清盤.md "wikilink")。

1978年9月7日，[香港最高法院委任破產管理官霍斯為臨時清盤官](../Page/香港最高法院.md "wikilink")，接管佳視資產。同日下午，300多名佳視職員、藝員到[港督府門外](../Page/港督府.md "wikilink")[請願](../Page/請願.md "wikilink")，以佳視新聞部編輯兼首席主播[何鉅華為首](../Page/何鉅華.md "wikilink")，向[港督副官遞交請願信](../Page/港督副官.md "wikilink")，並臂纏黑紗於港督府門外靜坐4個半小時，要求港英政府「追究一切有關佳視之責任承擔，盡速恢復電視台之播映，及保障電視專業人員之就業權益」\[11\]。佳視職員、藝員先後3次到港督府請願，但警方於10月3日清晨拘捕何鉅華、[徐聖祺](../Page/徐聖祺.md "wikilink")、[朱克及](../Page/朱克.md "wikilink")[林旭華](../Page/林旭華.md "wikilink")，並對4人及全體職藝員提出「非法集會」的[檢控](../Page/檢控.md "wikilink")\[12\]。結果佳視在同年10月19日被法庭頒令清盤。\[13\]

其實早在佳視倒閉前，公司組織上出現變動。尤其佳視消耗大量資金，於是引入林氏家族的資金注資至佳視；隨著林氏注資日增，令港英政府認為可能這與牌照條款有牴觸，因此[總督會同行政局在](../Page/總督會同行政局.md "wikilink")1977年12月下令[電視諮詢委員會調查](../Page/電視諮詢委員會.md "wikilink")。調查後，當局致函佳視，著令整頓若-{干}-與公司組織有關的事務；但不久佳視便宣佈倒閉。\[14\]
然而在2000年至2001年亞洲電視[本港台播出的劇集](../Page/本港台.md "wikilink")《[電視風雲](../Page/電視風雲.md "wikilink")》裡，以影射方式把佳視倒閉原因歸咎於港英政府不容許[中資入股香港電子傳媒](../Page/中資.md "wikilink")。

佳視倒閉後，佳視的大批台前幕後精英分別加入無綫電視及麗的電視，為香港電視界日後的黃金時代寫下序幕。

### 復台「傳聞」

[ACTVHK.jpg](https://zh.wikipedia.org/wiki/File:ACTVHK.jpg "fig:ACTVHK.jpg")
在[YouTube網站內的](../Page/YouTube.md "wikilink")[香港主權移交10週年紀念歌曲](../Page/香港主權移交10週年.md "wikilink")《[始終有你](../Page/始終有你.md "wikilink")》中及若-{干}-新聞短片，有着「佳藝電視寬頻一-{台}-」的標誌，更有人因此傳出佳視重開的傳聞。但實際上，該標誌純粹是[中學生把現成電視節目剪輯後自行加插](../Page/中學生.md "wikilink")，並且放在他們的網頁上，對外宣稱是自製節目及佳視為其組織之附屬機構。[1](http://www.youtube.com/watch?v=9oz4DiYKn30)

### 後續

[香港政府直到](../Page/香港政府.md "wikilink")2013年再發出第三及第四個免費電視牌照之前，香港免費電視一直維持兩台（無綫電視及麗的電視，麗的電視於1982年股權易手、改組為[亞洲電視](../Page/亞洲電視.md "wikilink")）爭雄的局面，其後至今造成無綫電視「一台獨大」30年的局面。不過，隨著香港政府推出香港電台數碼地面電視及正式[發出新電視牌照的決定](../Page/香港免費電視牌照爭議.md "wikilink")，再加上亞視在2016年4月1日因為免費電視牌照不獲續期而停播，香港的電視界可能會有新發展。2016年4月2日凌晨，亞視旗下模擬及數碼電視頻道因為政府不為其免費電視牌照續期而停播；當晚，香港電台正式接管亞視的模擬電視頻道，而新數碼電視頻道[ViuTV則在亞視停播後數天正式開台](../Page/ViuTV.md "wikilink")、接管部分亞視頻譜。

佳視倒閉後，佳藝電視大廈曾一度成為無綫電視的製作分廠（Production
Annex）；1988年，無綫電視總部從[廣播道遷到](../Page/廣播道.md "wikilink")[清水灣電視城後](../Page/清水灣電視城.md "wikilink")，佳藝電視大廈成為現在的香港電台電視大廈。

前[廣播處長](../Page/廣播處長.md "wikilink")[張敏儀表示](../Page/張敏儀.md "wikilink")，1975年商業電台董事長[何佐芝創辦佳藝電視](../Page/何佐芝.md "wikilink")，一年後已將股份全售予他人；其後佳視突然結業，和何佐芝無關，但他站出來、慷慨地用自己的錢支付員工遣散費，「佢嘅仁厚精神，所有老闆都應該學-{吓}-」（他的仁厚精神，所有僱主都應該學習）。

## 發牌條件

佳藝電視在競投牌照時自動提出多項條件，包括：保證每一股東不會擁有超過15%之股權\[15\]；星期一至五晚[黃金時間播放兩小時](../Page/黃金時間.md "wikilink")[教育節目](../Page/教育.md "wikilink")（這個亦是佳視的死穴之一），下午5時至6時則播放香港電台製作之節目\[16\]。

前佳藝電視總經理[李雪廬於其回憶錄](../Page/李雪廬.md "wikilink")《李雪廬回憶錄》中指出，佳藝電視放棄開設英文台、並於中文台播放教育節目的安排，概念由馬評人[簡而清提出](../Page/簡而清.md "wikilink")；但這安排等於只是半個電視台，難以攤分製作成本\[17\]。

## 節目表

佳藝電視開播當天（1975年9月7日）的節目表：

  -
    {| class="wikitable"

\! 播出時間 \! 節目名稱 |- | 18:00 | 啟播錄 |- | 18:05 | 佳視節目巡禮 |- | 18:45 |
賀佳視開播特輯 |- | 19:15 | 佳視新聞及天氣報導 |- | 20:00 | 名作劇場：武當弟子 |- |
21:00 | 特備綜藝節目：佳視良辰 |- | 22:10 | 比利球王大賽 |- | 23:40 | 佳視新聞及天氣報導 |- |
23:45 | 彩色人生 |}

佳藝電視開播初期的一週節目表：

| 時間    | 星期一<small>                              | 星期二<small>                     | 星期三<small>                                      | 星期四<small>  | 星期五<small> | 星期六<small> | 星期日<small> |
| ----- | --------------------------------------- | ------------------------------ | ----------------------------------------------- | ----------- | ---------- | ---------- | ---------- |
| 18:00 | 節目預告                                    |                                |                                                 |             |            |            |            |
| 18:05 | 觀點與角度                                   | [警訊](../Page/警訊.md "wikilink") | [少年警訊](../Page/少年警訊.md "wikilink")              | 「這一代」青年音樂節目 |            |            |            |
| 18:10 | 「[三國演義](../Page/三國演義.md "wikilink")」木偶劇 |                                |                                                 |             |            |            |            |
| 18:15 | [獅子山下](../Page/獅子山下.md "wikilink")      |                                |                                                 |             |            |            |            |
| 18:45 | 「一雀四鳳」歌舞趣劇                              |                                |                                                 |             |            |            |            |
| 18:50 | 「錦標」遊戲節目 (上)                            |                                |                                                 |             |            |            |            |
| 19:15 | 佳視新聞及天氣報導                               | 佳視新聞及天氣報導                      |                                                 |             |            |            |            |
| 19:45 | 「錦標」遊戲節目 (下)                            | 「錦標大賽」遊戲節目                     |                                                 |             |            |            |            |
| 20:00 | 名作劇場「武當弟子」                              | 名作劇場「武當弟子」                     |                                                 |             |            |            |            |
| 20:30 | 「遊戲人間」諷刺劇                               |                                |                                                 |             |            |            |            |
| 21:00 | 佳視劇場-全新手法電視劇                            | 「佳視群英會」綜藝節目                    | 「美齡與你」以[陳美齡為中心的歌唱節目](../Page/陳美齡.md "wikilink") |             |            |            |            |
| 21:30 | 教學電視                                    | 「歷刼鴛鴦」日本片集                     |                                                 |             |            |            |            |
| 22:00 | 週末劇場「歡天喜地」                              |                                |                                                 |             |            |            |            |
| 22:30 | 太平洋戰史                                   |                                |                                                 |             |            |            |            |
| 23:30 | 佳視新聞及天氣報導                               | 佳視新聞及天氣報導                      |                                                 |             |            |            |            |
| 23:35 | 「成年人時間」妙論人生百態                           | 「迷離集」懸疑片集                      |                                                 |             |            |            |            |
| 23:40 | 「迷離集」懸疑片集                               |                                |                                                 |             |            |            |            |

## 發射頻道

| 發射站                                | [特高頻](../Page/特高頻.md "wikilink")(UHF)頻道 | 頻率 (MHz) | 覆蓋範圍                                                                                                                                                                                              |
| ---------------------------------- | --------------------------------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [慈雲山](../Page/慈雲山.md "wikilink")   | 29                                      | 535.25   | [港島北](../Page/港島北.md "wikilink")、[九龍](../Page/九龍.md "wikilink")                                                                                                                                   |
| [青山](../Page/青山.md "wikilink")     | 46                                      | 671.25   | [大嶼山北部](../Page/大嶼山.md "wikilink")、[屯門至](../Page/屯門.md "wikilink")[大欖涌](../Page/大欖涌.md "wikilink")、[落馬洲](../Page/落馬洲.md "wikilink")、[元朗](../Page/元朗.md "wikilink")、[石崗](../Page/石崗.md "wikilink") |
| [飛鵝山](../Page/飛鵝山.md "wikilink")   | 46                                      | 671.25   | [觀塘](../Page/觀塘.md "wikilink")、[西貢](../Page/西貢.md "wikilink")                                                                                                                                     |
| [赤柱](../Page/赤柱.md "wikilink")     | 46                                      | 671.25   | [赤柱](../Page/赤柱.md "wikilink")                                                                                                                                                                    |
| [金山](../Page/金山.md "wikilink")     | 47                                      | 679.25   | [荃灣](../Page/荃灣.md "wikilink")、[青龍頭](../Page/青龍頭.md "wikilink")、[青衣](../Page/青衣.md "wikilink")、[葵涌](../Page/葵涌.md "wikilink")                                                                     |
| [南朗山](../Page/南朗山.md "wikilink")   | 47                                      | 679.25   | [深水灣](../Page/深水灣.md "wikilink")、[黃竹坑](../Page/黃竹坑.md "wikilink")、[石澳](../Page/石澳.md "wikilink")                                                                                                  |
| [柴灣](../Page/柴灣.md "wikilink")     | 56                                      | 751.25   | [柴灣](../Page/柴灣.md "wikilink")                                                                                                                                                                    |
| [聶高信山](../Page/聶高信山.md "wikilink") | 56                                      | 751.25   | [跑馬地](../Page/跑馬地.md "wikilink")、[黃泥涌](../Page/黃泥涌.md "wikilink")                                                                                                                                 |
| [九龍坑山](../Page/九龍坑山.md "wikilink") | 57                                      | 759.25   | [北區](../Page/北區_\(香港\).md "wikilink")、[大埔區](../Page/大埔區.md "wikilink")、[沙田](../Page/沙田區.md "wikilink")                                                                                            |
| [南丫島](../Page/南丫島.md "wikilink")   | 57                                      | 759.25   | [薄扶林](../Page/薄扶林.md "wikilink")、[淺水灣](../Page/淺水灣.md "wikilink")、[南灣](../Page/南灣.md "wikilink")                                                                                                  |
| [琵琶山](../Page/琵琶山.md "wikilink")   | 57                                      | 759.25   | [蘇屋](../Page/蘇屋.md "wikilink")、[長沙灣](../Page/長沙灣.md "wikilink")、[深水埗](../Page/深水埗.md "wikilink")                                                                                                  |

## 節目

### 教育節目

當初佳視的發牌條件之一是，佳視需要在星期一至五每晚9:30至11:30的黃金時間播放兩小時的[教育節目](../Page/教育.md "wikilink")\[18\]，教育節目時段內不得加插廣告；而其條件已漸漸應用於[公開大學節目](../Page/公開大學.md "wikilink")。佳視的教育節目主要以[成人教育為主](../Page/成人教育.md "wikilink")，並與[香港中文大學](../Page/香港中文大學.md "wikilink")[校外進修部和基督教職業訓練學校合辦](../Page/香港中文大學專業進修學院.md "wikilink")，\[19\]
課程包括汽車工程、[簿記](../Page/簿記.md "wikilink")、[室內設計](../Page/室內設計.md "wikilink")、外語、繪畫和插花等等。以上的教育節目更設有課外導修及進行考試，合格的話可獲取文憑，儼如一間[成人](../Page/成人.md "wikilink")[遙距教學學校](../Page/遙距教學.md "wikilink")。\[20\]

但教育節目的規定也是佳視未能取得收支平衡而導致佳視倒閉的原因之一，情況跟[日本兩家商營電視台類似](../Page/日本.md "wikilink")（[朝日電視台和](../Page/朝日電視台.md "wikilink")[東京電視台的前身都是商營的教育電視台](../Page/東京電視台.md "wikilink")。然而這類牟利的教育電視台因為嚴重經營不善，兼且[廣告收入比綜合電視台低](../Page/廣告.md "wikilink")，最終令它們在1973年11月放棄教育功能、轉作綜合電視台）。但佳視的情況就沒有它們幸運，猶如「宣告」商營教育電視頻道的「試驗」失敗。

### 電視劇

佳視曾經製作過很多被[電視迷視為經典的](../Page/電視迷.md "wikilink")[電視節目](../Page/電視節目.md "wikilink")，包括《[射鵰英雄傳](../Page/射鵰英雄傳_\(1976年電視劇\).md "wikilink")》、《[神鵰俠侶](../Page/神鵰俠侶_\(1976年電視劇\).md "wikilink")》等。佳視更是全世界第一間改編[華文](../Page/華文.md "wikilink")[武俠小說成為電視劇的電視台](../Page/武俠小說.md "wikilink")；佳視更帶領潮流，率先改編[金庸武俠小說作電視劇](../Page/金庸.md "wikilink")。一些著名電視幕後人員及演員如[徐克](../Page/徐克.md "wikilink")、[周梁淑怡](../Page/周梁淑怡.md "wikilink")、[鄭裕玲都曾任職於佳視](../Page/鄭裕玲.md "wikilink")。

  - [隋唐風雲](../Page/隋唐風雲.md "wikilink")（演員：[湘漪](../Page/湘漪.md "wikilink")，[鮑漢琳](../Page/鮑漢琳.md "wikilink")，[劉丹](../Page/劉丹.md "wikilink")，[喬宏](../Page/喬宏.md "wikilink")，[馮淬帆](../Page/馮淬帆.md "wikilink")，[焦姣](../Page/焦姣.md "wikilink")，[秦沛](../Page/秦沛.md "wikilink")）
  - [武則天](../Page/武則天_\(1976年電視劇\).md "wikilink")（演員：[湘漪](../Page/湘漪.md "wikilink")、[李通明](../Page/李通明.md "wikilink")、袁報華、[陳琪琪](../Page/陳琪琪.md "wikilink")、[馮淬帆](../Page/馮淬帆.md "wikilink")、[伊達](../Page/伊達.md "wikilink")、[秦沛](../Page/秦沛.md "wikilink")、[魏秋樺](../Page/魏秋樺.md "wikilink")、[劉丹](../Page/劉丹.md "wikilink")、[黄百鳴](../Page/黄百鳴.md "wikilink")、[景黛音](../Page/景黛音.md "wikilink")、[楊澤霖](../Page/楊澤霖.md "wikilink")、[鄭裕玲](../Page/鄭裕玲.md "wikilink")、古耀文、[張鴻昌](../Page/張鴻昌.md "wikilink")）
  - [頂爺](../Page/頂爺.md "wikilink")（演員：[劉丹](../Page/劉丹.md "wikilink")）
  - [追虎擒龍](../Page/追虎擒龍.md "wikilink")（演員：[劉丹](../Page/劉丹.md "wikilink")，[湘漪](../Page/湘漪.md "wikilink")，[石天](../Page/石天.md "wikilink")，[楊澤霖](../Page/楊澤霖.md "wikilink")，[陶敏明](../Page/陶敏明.md "wikilink")，[魏秋樺](../Page/魏秋樺.md "wikilink")）
  - [明星](../Page/明星_\(電視劇\).md "wikilink")（演員：[張瑪莉](../Page/張瑪莉.md "wikilink")，[曾江](../Page/曾江.md "wikilink")）
  - [賭國仇城](../Page/賭國仇城.md "wikilink")（演員：[歐嘉慧](../Page/歐嘉慧.md "wikilink")、[劉丹](../Page/劉丹.md "wikilink")、[劉鶴年](../Page/劉鶴年.md "wikilink")、[丁茵](../Page/丁茵.md "wikilink")）
  - [廣東好漢](../Page/廣東好漢.md "wikilink")（演員：[白彪](../Page/白彪.md "wikilink")，[米雪](../Page/米雪.md "wikilink")，[梁小龍](../Page/梁小龍.md "wikilink")）
  - [射鵰英雄傳](../Page/射鵰英雄傳_\(1976年電視劇\).md "wikilink")（演員：[白彪](../Page/白彪.md "wikilink")，[米雪](../Page/米雪.md "wikilink")，[梁小龍](../Page/梁小龍.md "wikilink")、[孟秋](../Page/孟秋.md "wikilink")）
  - [神鵰俠侶](../Page/神鵰俠侶_\(1976年電視劇\).md "wikilink")（演員：[羅樂林](../Page/羅樂林.md "wikilink")、[李通明](../Page/李通明.md "wikilink")、[白彪](../Page/白彪.md "wikilink")、[米雪](../Page/米雪.md "wikilink")、[梁小龍](../Page/梁小龍.md "wikilink")、[曹達華](../Page/曹達華.md "wikilink")、[楊澤霖](../Page/楊澤霖.md "wikilink")）
  - [碧血劍](../Page/碧血劍_\(1977年電視劇\).md "wikilink")（演員：[陳強](../Page/陳強.md "wikilink")、[文雪兒](../Page/文雪兒.md "wikilink")、[李通明](../Page/李通明.md "wikilink")、[石天](../Page/石天.md "wikilink")、[魏秋樺](../Page/魏秋樺.md "wikilink")、林秀([元秋](../Page/元秋.md "wikilink"))、[楊澤霖](../Page/楊澤霖.md "wikilink")）
  - [獨行殺手](../Page/獨行殺手.md "wikilink")
    (演員：[陳惠敏](../Page/陳惠敏.md "wikilink"))
  - [雪山飛狐](../Page/雪山飛狐_\(1978年電視劇\).md "wikilink")（演員：[衛子雲](../Page/衛子雲.md "wikilink")、[米雪](../Page/米雪.md "wikilink")、[文雪兒](../Page/文雪兒.md "wikilink")、[李通明](../Page/李通明.md "wikilink")、[白彪](../Page/白彪.md "wikilink")、[羅樂林](../Page/羅樂林.md "wikilink")、[鄭雷](../Page/鄭雷.md "wikilink")、[陳寶儀](../Page/陳寶儀.md "wikilink")、甘婉霞、陳琬筠、[曹達華](../Page/曹達華.md "wikilink")）
  - [鹿鼎記](../Page/鹿鼎記_\(1977年電視劇\).md "wikilink")（演員：[文雪兒](../Page/文雪兒.md "wikilink")、[程思俊](../Page/程思俊.md "wikilink")、[李通明](../Page/李通明.md "wikilink")、[陳琪琪](../Page/陳琪琪.md "wikilink")、[馬海倫](../Page/馬海倫.md "wikilink")、[魏秋樺](../Page/魏秋樺.md "wikilink")）
  - [萍蹤俠影錄](../Page/萍蹤俠影錄_\(1977年電視劇\).md "wikilink")（演員：[陳強](../Page/陳強.md "wikilink")、[楊盼盼](../Page/楊盼盼.md "wikilink")、[李通明](../Page/李通明.md "wikilink")、[梁小龍](../Page/梁小龍.md "wikilink")、[魏秋樺](../Page/魏秋樺.md "wikilink")、[劉江](../Page/劉江.md "wikilink")、[鄭雷](../Page/鄭雷.md "wikilink")）
  - [白髮魔女傳](../Page/白髮魔女傳_\(1977年電視劇\).md "wikilink")（演員：[李麗麗](../Page/李麗麗.md "wikilink")、[白彪](../Page/白彪.md "wikilink")、[文雪兒](../Page/文雪兒.md "wikilink")、[羅樂林](../Page/羅樂林.md "wikilink")、覃恩美、[鄭雷](../Page/鄭雷.md "wikilink")、[劉江](../Page/劉江.md "wikilink")、[曹達華](../Page/曹達華.md "wikilink")、[鄭裕玲](../Page/鄭裕玲.md "wikilink")、[魏秋樺](../Page/魏秋樺.md "wikilink")）
  - [武林外史](../Page/武林外史_\(1977年電視劇\).md "wikilink")（演員：[米雪](../Page/米雪.md "wikilink")，[李通明](../Page/李通明.md "wikilink")，[文雪兒](../Page/文雪兒.md "wikilink")、[劉丹](../Page/劉慶基.md "wikilink")，[劉江](../Page/劉江.md "wikilink")，[魏秋樺](../Page/魏秋樺.md "wikilink")）
  - [金刀情俠](../Page/金刀情俠_\(電視劇\).md "wikilink")（演員：[游天龍](../Page/游天龍.md "wikilink")，[余安安](../Page/余安安.md "wikilink")，[鄭裕玲](../Page/鄭裕玲.md "wikilink")，[魏秋樺](../Page/魏秋樺.md "wikilink")，[朱承彩](../Page/朱承彩.md "wikilink")，[白彪](../Page/白彪.md "wikilink")，[楊澤霖](../Page/楊澤霖.md "wikilink")）
  - [名流情史](../Page/名流情史.md "wikilink")
    （演員：[鄧碧雲](../Page/鄧碧雲.md "wikilink")，[羅艷卿](../Page/羅艷卿.md "wikilink")，[白彪](../Page/白彪.md "wikilink")，[米雪](../Page/米雪.md "wikilink")，
    [韋基舜](../Page/韋基舜.md "wikilink")，[鄭裕玲](../Page/鄭裕玲.md "wikilink")、[文雪兒](../Page/文雪兒.md "wikilink")）
  - [流星蝴蝶劍](../Page/流星蝴蝶劍_\(1978年電視劇\).md "wikilink")（演員：[羅樂林](../Page/羅樂林.md "wikilink")，[魏秋樺](../Page/魏秋樺.md "wikilink")，[劉江](../Page/劉江_\(香港\).md "wikilink")，[楊澤霖](../Page/楊澤霖.md "wikilink")，[馬海倫](../Page/馬海倫.md "wikilink")、[文雪兒](../Page/文雪兒.md "wikilink")）
  - [仙鶴神針](../Page/仙鶴神針_\(1977年電視劇\).md "wikilink")（演員：[羅樂林](../Page/羅樂林.md "wikilink")，[馬海倫](../Page/馬海倫.md "wikilink")，[覃恩美](../Page/覃恩美.md "wikilink")，[楊盼盼](../Page/楊盼盼.md "wikilink")，[秦沛](../Page/秦沛.md "wikilink")，[李通明](../Page/李通明.md "wikilink")）
  - [紅樓夢](../Page/紅樓夢_\(1977年電視劇\).md "wikilink")（演員：[伍衛國](../Page/伍衛國.md "wikilink")、[毛舜筠](../Page/毛舜筠.md "wikilink")、[米雪](../Page/米雪.md "wikilink")、[黃蕙芬](../Page/黃蕙芬.md "wikilink")、[吳浣儀](../Page/吳浣儀.md "wikilink")、[朱瑞棠](../Page/朱瑞棠.md "wikilink")、[楊澤霖](../Page/楊澤霖.md "wikilink")、[丁櫻](../Page/丁櫻.md "wikilink")、[鄭雷](../Page/鄭雷.md "wikilink")、[丁茵](../Page/丁茵.md "wikilink")、[朱承彩](../Page/朱承彩.md "wikilink")、[劉丹](../Page/劉丹.md "wikilink")、[馬海倫](../Page/馬海倫.md "wikilink")、[張寶之](../Page/張寶之.md "wikilink")、[陶敏明](../Page/陶敏明.md "wikilink")、[文雪兒](../Page/文雪兒.md "wikilink")、[李通明](../Page/李通明.md "wikilink")、[劉江](../Page/劉江.md "wikilink")、[林燕妮](../Page/林燕妮.md "wikilink")、[鄭裕玲](../Page/鄭裕玲.md "wikilink")、[魏秋樺](../Page/魏秋樺.md "wikilink")、[羅樂林](../Page/羅樂林.md "wikilink")、[陳琪琪](../Page/陳琪琪.md "wikilink")、[白彪](../Page/白彪.md "wikilink")、[陳寶儀](../Page/陳寶儀.md "wikilink")、甘婉霞、陳琬筠、[張鴻昌](../Page/張鴻昌.md "wikilink")）
  - [風雷第一刀](../Page/風雷第一刀.md "wikilink")（未完成拍攝）（演員：[劉江飾演葉開](../Page/劉江.md "wikilink")，[伍衛國飾演傅紅雪](../Page/伍衛國.md "wikilink")，[文雪儿飾演丁靈琳](../Page/文雪儿.md "wikilink")，[米雪飾演馬芳鈴](../Page/米雪.md "wikilink")，[魏秋桦飾演翠濃](../Page/魏秋桦.md "wikilink")，[吳桐飾演馬空群](../Page/吳桐.md "wikilink")，[陶敏明飾演沈三娘](../Page/陶敏明.md "wikilink")）
  - [細魚吃大魚](../Page/細魚吃大魚.md "wikilink")（演員：[林偉圖](../Page/林偉圖.md "wikilink")、[米雪](../Page/米雪.md "wikilink")、[楊盼盼](../Page/楊盼盼.md "wikilink")）
  - [精武門](../Page/精武門.md "wikilink")（演員：[梁小龍](../Page/梁小龍.md "wikilink")、[麥子雲](../Page/麥子雲.md "wikilink")、[秦沛](../Page/秦沛.md "wikilink")、[張鴻昌](../Page/張鴻昌.md "wikilink")、[楊澤霖](../Page/楊澤霖.md "wikilink")、[魏秋樺](../Page/魏秋樺.md "wikilink")）
  - 千王之王（已經由無綫電視繼續拍攝）

### 外購日劇

  - （原名：雑居時代）

  - （原名：）

  - （原名：美人はいかが?）

  - （原名：狼・無頼控）

  - （原名：パパ愛してる\!\!）

### 外購動畫

  - [百變龍](../Page/百變龍.md "wikilink")（1976年XX月XX日 -
    1977年7月10日）（原名：スーパーロボット マッハバロン）

  - [七星俠](../Page/超人七號.md "wikilink")（1976年XX月XX日 -
    1977年7月10日）（原名：ウルトラセブン）

  - （1976年XX月XX日 - 1977年11月12日）（原名：チビラくん）

  - （1977年8月7日 - 1978年8月20日）（原名：まんが世界昔ばなし）

  - [大眼妹與波比](../Page/大眼妹與波比.md "wikilink")

  - [急救組卡通](../Page/急救組卡通.md "wikilink")

#### 日本特攝片

「301部隊」時段，是對應無綫電視「430穿梭機」時段，指1978年7月1日 -
1978年8月19日期間每逢星期六下午3點零一分播映的日本[特攝片](../Page/特攝.md "wikilink")。

  - [五勇士](../Page/秘密戰隊五連者.md "wikilink")（原名：秘密戦隊ゴレンジャー）

  - （原名：大鉄人17）（註：大鐵人其後被轉往無綫電視翡翠台播出，並易名作《無敵大鐵人》）

  - （原名：氷河戦士ガイスラッガー）

  - [金剛飛天鑽](../Page/鋼鐵吉克.md "wikilink")（原名：鋼鉄ジーグ）（註：金剛飛天鑽於82至84年間轉往無綫電視翡翠台播出，並易名作《磁力鐵甲人》）

### 新聞節目

佳藝電視每日提供兩節電視新聞，啓播初期分別於晚上7:15和11:30播出新聞及天氣報告，\[21\]
後來改為每日傍晚六時二十五分播放「佳視新聞」及晚上八時五十五分播出「新聞精華」。最初佳視由[商業電台調派播音員](../Page/商業電台.md "wikilink")[楊廣培](../Page/楊廣培.md "wikilink")、[尹芳玲](../Page/尹芳玲.md "wikilink")、[金剛等為主播](../Page/金剛_\(播音員\).md "wikilink")，後來引入專業新聞人員，著名主播[何鉅華](../Page/何鉅華.md "wikilink")（演員[盧宛茵前夫](../Page/盧宛茵.md "wikilink")，現移民美國邁阿密）曾長期為傍晚佳視新聞的主播，而[蘇凌峰](../Page/蘇凌峰.md "wikilink")（前[加拿大](../Page/加拿大.md "wikilink")[多元文化電視台新聞總編輯及主播](../Page/多元文化電視台.md "wikilink")）、[梁家榮](../Page/梁家榮.md "wikilink")（現[香港電台廣播處長](../Page/香港電台.md "wikilink")）、[趙應春](../Page/趙應春.md "wikilink")（現[有線電視新聞有限公司執行董事](../Page/有線電視.md "wikilink")）則為新聞精華的主播。因為佳視在每晚九時起須播放教育電視節目，故此作為全日新聞總結的新聞精華節目，較其他兩台的夜間新聞時間早（當時無綫的夜間新聞約為十時半，而麗的則在十點），直至倒閉前才在晚上十一時半重新加上最後新聞時段。

同時，佳視是香港首家會為本地重大時事製播特備節目的電視台，如在1977年的[金禧中學學潮](../Page/金禧中學.md "wikilink")，佳視曾為此製作「金禧學潮論壇」節目，當時為金禧教師的現[社民連骨幹成員](../Page/社民連.md "wikilink")[劉子濂曾在該節目上表示](../Page/劉子濂.md "wikilink")，曾在報章撰文指學運遭「革命馬克思主義同盟」（革馬盟，[四五行動前身](../Page/四五行動.md "wikilink")）滲透是受校長指使的，而使學潮矛頭更傾向同情學生。

## 主要演員

  - 源自無綫電視，佳藝電視倒閉後重返無綫電視=
      - [林偉圖](../Page/林偉圖.md "wikilink")（移民美國三藩市）
      - [楊盼盼](../Page/楊盼盼.md "wikilink")（轉職電影界）
      - [伍衛國](../Page/伍衛國.md "wikilink")（曾往麗的，後重返無綫）

<!-- end list -->

  - 源自麗的電視（今亞洲電視），佳藝電視倒閉後重返麗的電視（今亞洲電視）
      - [江圖](../Page/江圖.md "wikilink")（曾多次往返無綫與麗的，轉往內地發展）

<!-- end list -->

  - 源自無綫電視，佳藝電視倒閉後轉投麗的電視（今亞洲電視）
      - [余安安](../Page/余安安.md "wikilink")（先麗的，其後重返無綫，再一次轉投亞洲電視，現轉職電影界）
      - [鮑漢琳](../Page/鮑漢琳.md "wikilink") （先麗的，其後轉職電影界，已去世）

<!-- end list -->

  - 源自麗的電視（今亞洲電視），佳藝電視倒閉後轉投無綫電視
      - [劉丹](../Page/劉丹.md "wikilink")（轉投無綫，曾再加盟亞視，其後再一次重返無綫）
      - [黃韻詩](../Page/黃韻詩.md "wikilink")（曾多次往返無綫與麗的，其後移民加拿大）
      - [毛舜筠](../Page/毛舜筠.md "wikilink")（現轉職電影界）

<!-- end list -->

  - 佳藝電視出身，倒閉後轉無綫電視
      - [黃錦燊](../Page/黃錦燊.md "wikilink")（轉職法律界，現任遼寧省政協委員）
      - [秦煌](../Page/秦煌.md "wikilink")
        （佳視時用本名梁日成，現轉往無綫電視，佳視及無綫拍攝射鵰英雄傳都是扮演老頑童一角。）
      - [鄭裕玲](../Page/鄭裕玲.md "wikilink")
      - [陳榮峻](../Page/陳榮峻.md "wikilink")
      - [麥子雲](../Page/麥子雲.md "wikilink")

<!-- end list -->

  - 佳藝電視出身，倒閉後轉投麗的/亞洲電視
      - [梁小龍](../Page/梁小龍.md "wikilink") （現轉職電影界）

<!-- end list -->

  - 佳藝電視出身，倒閉後轉投無綫及麗的/亞洲電視
      - [白彪](../Page/白彪.md "wikilink")
        （先麗的，其後轉投無綫／現轉投[香港電視網絡](../Page/香港電視網絡.md "wikilink")）
      - [米雪](../Page/米雪.md "wikilink")（先無綫後麗的，再重返無綫）
      - [魏秋樺](../Page/魏秋樺.md "wikilink")
        （先麗的，其後轉投無綫，[亞洲會會長](../Page/亞洲會.md "wikilink")，現已息影）
      - [張鴻昌](../Page/張鴻昌.md "wikilink")（先麗的，其後轉投無綫，移民澳大利亞坎培拉）
      - [劉江](../Page/劉江_\(香港\).md "wikilink")（先麗的，其後轉投無綫）
      - [李麗麗](../Page/李麗麗.md "wikilink") （先麗的，其後轉投無綫）
      - [馬海倫](../Page/馬海倫.md "wikilink") （先麗的，其後轉投無綫）
      - [文雪兒](../Page/文雪兒.md "wikilink") （先麗的，其後轉投無綫）
      - [楊澤霖](../Page/楊澤霖.md "wikilink")（先麗的，其後轉投無綫，已離開娛樂圏）
      - [曹達華](../Page/曹達華.md "wikilink")（先麗的，其後轉投無綫，再重返麗的，已去世）
      - [鄭雷](../Page/鄭雷.md "wikilink")（先麗的，其後轉投無綫，移民加拿大多倫多）
      - [秦沛](../Page/秦沛.md "wikilink")（先麗的，其後轉投無綫，再加碼亞洲電視，又重返無綫，現主要內地發展）
      - [羅樂林](../Page/羅樂林.md "wikilink")（先麗的，其後轉投無綫）
      - [李通明](../Page/李通明.md "wikilink")（息影，移民美國邁阿密）
      - [麥天恩](../Page/麥天恩.md "wikilink")（已去世）
      - [湘漪](../Page/湘漪.md "wikilink")（先無綫後亞視，已淡出影視圈）

## 參見

  - [佳視六君子](../Page/佳視六君子.md "wikilink")

## 參考資料及註釋

<div style="font-size:small" class="references-2column">

<references />

</div>

## 外部連結

  - [1978年「七月佳視」宣傳聲帶(第一段)(粵語)](http://www.youtube.com/watch?v=1ixqgsHyW40)
  - [1978年「七月佳視」宣傳聲帶(第二段)(粵語)](http://www.youtube.com/watch?v=4JVeAJz4G0E)
  - [三台鼎立消失逾30年](http://mylexiconboard2.blogspot.tw/2009/07/30.html)，《明報》，2009-07-29

[Category:香港電視台](../Category/香港電視台.md "wikilink")
[Category:佳藝電視](../Category/佳藝電視.md "wikilink")
[Category:香港已停播的電視頻道](../Category/香港已停播的電視頻道.md "wikilink")
[Category:香港已結業公司](../Category/香港已結業公司.md "wikilink")
[Category:1975年成立的公司](../Category/1975年成立的公司.md "wikilink")
[Category:1978年結業的公司](../Category/1978年結業的公司.md "wikilink")

1.  《工商日報》1975年9月7日佳藝電視廣告

2.  《Hong Kong 1974, Report for the year 1973》Hong Kong Government,
    1974: page 149

3.  [中華民國電視學會電視年鑑暨電視叢書編纂委員會](../Page/中華民國電視學會.md "wikilink")
    編纂，《[中華民國電視年鑑第一輯](../Page/中華民國電視年鑑.md "wikilink")：民國五十年至六十四年》，中華民國電視學會1976年5月30日出版，第206頁。

4.  [無綫電視](../Page/電視廣播有限公司.md "wikilink")《[由1967開始](../Page/由1967開始.md "wikilink")：破舊立新的1975》，2009年11月30日

5.  《波係圓嘅：阿盲44年傳媒生涯回憶錄》，伍晃榮著，[經濟日報出版社](../Page/經濟日報出版社.md "wikilink")2005年10月初版，ISBN
    962-678-297-8

6.
7.
8.
9.  工商日報，1978年8月5日，第8頁

10.
11. 華僑日報，1978年9月8日

12. 大公報，1978年10月5日

13. 《1977年[香港年報](../Page/香港年報.md "wikilink")》。香港政府，1978年:第143至144頁

14.
15. 工商日報，1978年9月22日，第1頁

16. 大公報，1978年10月20日，第1張第4版

17. 《李雪廬回憶錄》，李雪廬，[三聯書店（香港）](../Page/三聯書店（香港）.md "wikilink")，2010年，ISBN
    9789620429231

18.
19.
20. 《1976年[香港年報](../Page/香港年報.md "wikilink")》。香港政府，1977年:第150頁

21.