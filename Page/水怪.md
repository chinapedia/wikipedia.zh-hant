[Soe_Orm_1555.jpg](https://zh.wikipedia.org/wiki/File:Soe_Orm_1555.jpg "fig:Soe_Orm_1555.jpg")攻击海上船只\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:20000_squid_holding_sailor.jpg "fig:缩略图")》插圖中的水怪\]\]
**水怪**（Sea
monster，或称“**海怪**”以特指[海洋中的水怪](../Page/海.md "wikilink")）是指生活在水里的[神话传说和未知名的生物](../Page/神话传说.md "wikilink")。普遍认为，在海中大约1000[米深的地方](../Page/米.md "wikilink")，有许多大型未知生物，体长约在18-20米之间。

水怪形式多样，如[龙](../Page/龙.md "wikilink")、[海蛇及长有多支手的](../Page/海蛇.md "wikilink")[怪物等](../Page/怪物.md "wikilink")。

## 有关水怪的记载

在许多与海或其他较大水域有接触的国家中都能找到有关水怪的纪载。[加拿大](../Page/加拿大.md "wikilink")[歐肯納根湖中有著名的水怪](../Page/歐肯納根湖.md "wikilink")[奧古布古](../Page/奧古布古.md "wikilink")。[俄罗斯的](../Page/俄罗斯.md "wikilink")[貝加爾湖](../Page/貝加爾湖.md "wikilink")、[美国的](../Page/美国.md "wikilink")[尚普兰湖也存在有水怪](../Page/尚普兰湖.md "wikilink")。仅就[中國而言](../Page/中國.md "wikilink")，[新疆的](../Page/新疆.md "wikilink")[喀納斯湖](../Page/喀納斯湖.md "wikilink")、[吉林省的](../Page/吉林省.md "wikilink")[長白山天池](../Page/長白山天池.md "wikilink")、[青海省的](../Page/青海省.md "wikilink")[青海湖](../Page/青海湖.md "wikilink")、[河南省驻马店市的](../Page/河南省.md "wikilink")[銅山湖也都有水怪的傳聞](../Page/銅山湖.md "wikilink")。譬如，1985年《[新疆日報](../Page/新疆日報.md "wikilink")》在头版头条位置刊出一則消息：“喀纳斯湖发现巨型[大红鱼](../Page/大红鱼.md "wikilink")”，长约10-15米，[新疆大学生物系师生在湖边親眼所見](../Page/新疆大学.md "wikilink")，后懷疑是一隻[哲羅鮭](../Page/哲羅鮭.md "wikilink")。

最早關於水怪的記載可追溯到565年。當時，[愛爾蘭](../Page/愛爾蘭.md "wikilink")[傳教士聖哥倫伯和友人在](../Page/傳教士.md "wikilink")[英国的](../Page/英国.md "wikilink")[尼斯湖游泳](../Page/尼斯湖.md "wikilink")，水怪突然他的朋友襲來，幸好教士情急之下厲聲喝罵：「不要前進，也不要傷人。從速離去。」湖怪掉头潜入水底，友人幸運地游回岸上。

[Hans_Egede_sea_serpent_1734.jpg](https://zh.wikipedia.org/wiki/File:Hans_Egede_sea_serpent_1734.jpg "fig:Hans_Egede_sea_serpent_1734.jpg")1734年7月6日，[丹麦传教士](../Page/丹麦.md "wikilink")在航向[格陵兰西海岸的](../Page/格陵兰.md "wikilink")[努克市的途中突遇一只形似海蛇的怪物](../Page/努克.md "wikilink")，他对此记载道：

“那边出现了一个非常可怕的海洋生物。它高高伸出海面，头比船的大桅楼还高，鼻子长而尖，可以像鲸一样喷射，鳍宽而大，身上长着褶皱不平的坚硬表皮。而且，它身体的下部分和蛇很像，当它再次潜入水中时，会向后仰，同时尾部露出水面，身躯和整条船一样长。当晚，天气十分恶劣。”

汉斯·艾吉提还收集[十八世紀以來有關目擊水怪的報告](../Page/十八世紀.md "wikilink")，集結成專書。

1861年11月30日，[法國](../Page/法國.md "wikilink")[軍艦阿力頓號的船員在](../Page/軍艦.md "wikilink")[西班牙開往](../Page/西班牙.md "wikilink")[騰納立夫島途中](../Page/騰納立夫島.md "wikilink")，遇上一隻巨大的海怪，長得很像[巨型章魚](../Page/巨型章魚.md "wikilink")。有船員用[魚叉把怪物叉中](../Page/魚叉.md "wikilink")，但怪物掙扎後逃去，魚叉上留有碎肉\[1\]。

根據1909年3月[日本](../Page/日本.md "wikilink")《[朝日新聞](../Page/朝日新聞.md "wikilink")》報導，當時一艘英國[汽船](../Page/汽船.md "wikilink")「蘇丹號」在[台灣近海目擊到一隻背上有一排棘刺的巨大海獸](../Page/台灣.md "wikilink")。後來，該海怪被命名為「[海棘獸](../Page/海棘獸.md "wikilink")」，名列[台灣妖怪之一](../Page/台灣妖怪列表.md "wikilink")\[2\]。

1934年4月，[倫敦醫生威爾遜途經尼斯湖](../Page/倫敦.md "wikilink")，發現水怪，連忙用相機拍下了水怪的照片。有科學家認為是七千多萬年前滅絕的[蛇頸龍](../Page/蛇頸龍.md "wikilink")\[3\]。

1977年4月，一艘日本籍遠洋漁船[瑞洋丸號](../Page/瑞洋丸.md "wikilink")（ずいようまる），在[紐西蘭外海捕獲一具身份不名的生物骸骨](../Page/紐西蘭.md "wikilink")，長度約有10公尺。由於該屍體發出惡臭，船長田中昭不願意將牠帶回日本，只在照了幾張照片後，便丟回海中。經過分析之後，科學家認為此動物只是一隻[姥鯊的屍體](../Page/姥鯊.md "wikilink")，只是屍體腐爛的程度相當嚴重，因此無法認出這隻姥鯊生前的樣貌\[4\]\[5\]\[6\]\[7\]。

这些“怪物”究竟是什么还存有争议。它们可能是[皱鳃鲨](../Page/皱鳃鲨.md "wikilink")、姥鲨、[桨鱼](../Page/皇带鱼.md "wikilink")、[大王乌贼](../Page/大王乌贼.md "wikilink")、[乌贼或](../Page/乌贼.md "wikilink")[鲸](../Page/鲸.md "wikilink")。还有假说认为人们所见到的怪物可能是巨大[海生爬行动物的存活个体](../Page/海生爬行动物.md "wikilink")，如[侏罗纪和](../Page/侏罗纪.md "wikilink")[白垩纪时期的](../Page/白垩纪.md "wikilink")[鱼龙或蛇颈龙](../Page/鱼龙.md "wikilink")，或已灭绝的鲸类的存活个体，如[龙王鲸](../Page/龙王鲸.md "wikilink")。

还有可能的情况是，许多的报告把鲨鱼或鲸的尸体，大型海藻，木头或包括遗弃的木筏、[独木舟和](../Page/独木舟.md "wikilink")[渔网等在内的其他漂浮物误当成了海怪](../Page/渔网.md "wikilink")。

## 水怪種類

### 龍

世界上有許多有關於[龍的傳說](../Page/龍.md "wikilink")，大部份人認為龍生活在水中，偶爾會到陸地（當然也可以飛）。《[山海經](../Page/山海經.md "wikilink")》上也記載了許多龍的事情，其中提到了龍生活於水中。而疑似發現龍的案例——[湄公河的巨龍](../Page/湄公河.md "wikilink")[那伽](../Page/那伽.md "wikilink")，就是十分典型的例子。

### 爬虫类

#### 巨龜

巨龜是比較少見的例子，不過確實有這個案例，比如美国印第安纳州的[巴斯科怪兽](../Page/巴斯科怪兽.md "wikilink")。

#### 巨蛇

如[北歐神話裡的](../Page/北歐神話.md "wikilink")[耶夢加得和](../Page/耶夢加得.md "wikilink")[希臘神話裡的](../Page/希臘神話.md "wikilink")[九頭蛇](../Page/九頭蛇.md "wikilink")。

### 巨型海怪

巨型海怪事實上就是一種未被發現的巨大海洋生物，在未來被發現且命名的可能性極高，越深海怪的體型越大，甚至有體型由30到100米以上的海怪，1000米以下的深度是十分有可能會發現巨型海怪的地方，巨型海怪可能是巨大海生[爬行動物的存活個體](../Page/爬行動物.md "wikilink")，食量非常大，或其他已滅絕的巨大海洋生物的存活個體，還有可能的情況是，巨型海怪是個全新品種的巨大海洋生物。

其实海怪还有第二小名～在北马海域里，海怪被称之为“祖豪”英文称“Africa ostrich ”......

### 人魚

是否真正存在[人魚目前也尚無法確定](../Page/人魚.md "wikilink")。有记载的人鱼叫作[塞壬](../Page/塞壬.md "wikilink")，据说是海神的女儿。还有一种是下半身是鱼，上半身是一种酷似人的样子。在中国《山海经》中也有相似的記載。

### 无脊椎动物

  -
  - [大王乌贼](../Page/大王乌贼.md "wikilink")

  - [巨型章魚](../Page/巨型章魚.md "wikilink")

  - [挪威海怪](../Page/挪威海怪.md "wikilink")

#### 蟹

在中國[唐代](../Page/唐代.md "wikilink")[戴孚](../Page/戴孚.md "wikilink")《[廣異記](../Page/廣異記.md "wikilink")》中記載了一則海中大蟹與一隻名為「[山神](../Page/山神.md "wikilink")」的巨蛇搏鬥的事件，該蟹被形容的異常巨大，牠的兩隻螯高幾百丈，從遠處看來就像是兩座高山一樣\[8\]。此外在[清末的台灣近海處](../Page/清末.md "wikilink")，亦有「闊如桌面，兩螯如巨剪」的大蟹出沒的記錄\[9\]。

## 古地圖上的水怪

在[中世紀和](../Page/中世紀.md "wikilink")[文藝復興時期的一些歐洲古](../Page/文藝復興.md "wikilink")[地圖上出現了各式各樣的水怪](../Page/地圖.md "wikilink")。據地圖史學家切特·凡·杜澤分析：地圖上的水怪有著兩大功能，一是為船員指引可能的危險，二是成為地圖的裝飾性元素。手繪地圖上的水怪通常是製圖師根據客戶要求而畫上的，而[印刷地圖上的水怪則主要是作為增加銷量的元素](../Page/印刷.md "wikilink")\[10\]。

## 参见

  - [中国水怪列表](../Page/中国水怪列表.md "wikilink")
  - [世界湖怪列表](../Page/世界湖怪列表.md "wikilink")
  - [刻托](../Page/刻托.md "wikilink")
  - [大海蛇](../Page/大海蛇.md "wikilink")

## 參考文獻

## 外部連結

  - [尼斯湖水怪](http://dinoweb.myweb.hinet.net/watermonster.htm)

[Category:神話傳說中的水棲生物](../Category/神話傳說中的水棲生物.md "wikilink")
[Category:水棲神秘生物](../Category/水棲神秘生物.md "wikilink")
[Category:怪物](../Category/怪物.md "wikilink")
[Category:海事民间传说](../Category/海事民间传说.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.