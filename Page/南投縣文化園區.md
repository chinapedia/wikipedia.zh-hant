[Nan-tou_Country_Cluture_Park_20060815-1.jpg](https://zh.wikipedia.org/wiki/File:Nan-tou_Country_Cluture_Park_20060815-1.jpg "fig:Nan-tou_Country_Cluture_Park_20060815-1.jpg")
[Nan-tou_Country_Cluture_Park_20060826-1.jpg](https://zh.wikipedia.org/wiki/File:Nan-tou_Country_Cluture_Park_20060826-1.jpg "fig:Nan-tou_Country_Cluture_Park_20060826-1.jpg")
**南投縣文化園區**（**Nantou Country Culture
Park，NCCP**），是一座以「傳統建築物再利用」的方式進行規畫的歷史園區，園區內的建築主體是以興建於[台灣日治時期的](../Page/台灣日治時期.md "wikilink")**[武德殿](../Page/武德殿.md "wikilink")**修復而成，是[台灣第一個縣級的歷史博物館](../Page/台灣.md "wikilink")。園區包含[南投縣縣史館](../Page/南投縣.md "wikilink")、南投陶展示館、南投縣縣史圖書文獻室、南投縣藝術家資料館等不同展館。

## 歷史

位於[南投市市中心的武德殿興建於昭和](../Page/南投市.md "wikilink")12年（1937年），日治時期在[日本政府曾在台灣各城市普設宏揚](../Page/日本政府.md "wikilink")[武德的](../Page/武德.md "wikilink")[體育建築](../Page/體育.md "wikilink")，提供[柔道](../Page/柔道.md "wikilink")、[劍道或](../Page/劍道.md "wikilink")[弓箭等](../Page/弓箭.md "wikilink")[武術練習場所](../Page/武術.md "wikilink")，為一座仿唐式建築，並同時融合了[中國北方與](../Page/中國北方.md "wikilink")[日本建築風格](../Page/日本.md "wikilink")。

比鄰武德殿的南投縣藝術家資料館興建於1952年，建築風格亦同是仿唐式風格的獨棟兩層樓建築。

隨著時代的變遷，園區內的建築也曾被做為不同的用途使用，[戰後曾先後做為南投縣](../Page/太平洋戰爭.md "wikilink")[警察局](../Page/警察局.md "wikilink")、台灣省山地農牧局、南投縣機關學校員工合作社聯合社供銷部及南投縣[環保局等不單位的使用](../Page/環保局.md "wikilink")，1997年12月14日，正式更名為**南投縣縣史館**。

1999年的[九二一大地震](../Page/九二一大地震.md "wikilink")，使得園區內建築嚴重損毀，歷經一年的施工修復，南投縣縣史館於2001年10月重新開幕對民眾提供服務，南投縣藝術家資料館則於2003年6月15日正式營運。

## 園區展示館

### 南投縣縣史館

南投縣政府文化局於1996年12月成立「縣史館籌備處」，開始規劃縣史館成立事宜，1997年12月14日正式揭幕使用，館內設有該縣古道、古蹟、[台灣原住民文化](../Page/台灣原住民.md "wikilink")、古文書、文獻、碑碣拓本、及老[照片展示等](../Page/照片.md "wikilink")，共分以下展覽項目：

  - 專題特展：定期更換的展示，每次展出不同的主題，如：[泰雅族原住民的](../Page/泰雅族.md "wikilink")[黥面專題特展](../Page/黥面.md "wikilink")。
  - [古道資料展](../Page/古道.md "wikilink")：介紹南投開墾史上的知名古道。
  - 族群概說與服飾展：介紹南投縣開墾史上各個族群的分佈與遷徙的歷史、文化及服飾。
  - 歷史影像資料展及其他：提供影像播放系統播出歷年收藏出版的老照片專輯。

除了以上的展項之外，館方還製作了三座縣內著名的古蹟書院模型、十三鄉鎮的文史資料及地圖集，多媒體導覽系統等設備供民眾使用。

[Nan-tou_Pottery_Exhibition_Hall_show_20060826.jpg](https://zh.wikipedia.org/wiki/File:Nan-tou_Pottery_Exhibition_Hall_show_20060826.jpg "fig:Nan-tou_Pottery_Exhibition_Hall_show_20060826.jpg")

### 南投陶展示館

南投縣已有二百年的製陶歷史，在日治時期製陶業達到鼎盛，「**南投燒**」（**南投陶**）已成為南投縣特殊產物之一，南投陶展示館展出了南投陶業發展歷程及[陶器的製作流程](../Page/陶器.md "wikilink")。展示館共分以下展覽項目：

  - 成型技法與裝飾技法
  - 陶土的種類與特質
  - 窯的介紹
  - 現代陶藝

[Nan-tou_Country_History_Reference_Hall_20060526.jpg](https://zh.wikipedia.org/wiki/File:Nan-tou_Country_History_Reference_Hall_20060526.jpg "fig:Nan-tou_Country_History_Reference_Hall_20060526.jpg")

### 南投縣縣史圖書文獻室

南投縣縣史圖書文獻室典藏縣內各種重要的文獻資料，如：[政府公報](../Page/政府公報.md "wikilink")、碩博士[論文](../Page/論文.md "wikilink")、[期刊](../Page/期刊.md "wikilink")、[地圖](../Page/地圖.md "wikilink")、[族譜](../Page/族譜.md "wikilink")、[地方誌等五千餘冊](../Page/地方誌.md "wikilink")。提供民眾查閱、[影印等服務](../Page/影印.md "wikilink")。

### 南投縣藝術家資料館

南投縣藝術家資料館成立的目的在於建立及保存縣內[藝術家史料檔案](../Page/藝術家.md "wikilink")，促進縣民接觸藝術史料並與藝術工作者交流機會。

館內一樓設有簡報室，備有視聽簡報設備，可提供作品發表，作品賞析、專題講座及簡報等用途，此外，交誼室，則提供另一個較鬆的空間讓藝術家、民眾可以聯誼、交流的場所。二樓為展覽空間，提供平面固定展示、展示櫃及可動展示壁面等展覽設施。

## 相關連結

  - [南投縣文化資產](../Page/南投縣文化資產.md "wikilink")
  - [南投縣](../Page/南投縣.md "wikilink")

## 外部連結

  - [南投縣政府文化局](http://www.nthcc.gov.tw/)

## 參考資料

1.  王灝，《南投山水歌》，文化總會，2004年7月：40—43頁 ISBN 986-7430-08-5

[category:台湾历史博物馆](../Page/category:台湾历史博物馆.md "wikilink")

[Category:南投縣文化資產](../Category/南投縣文化資產.md "wikilink")
[Category:南投縣博物館](../Category/南投縣博物館.md "wikilink")
[Category:文物博物館](../Category/文物博物館.md "wikilink")
[Category:南投市](../Category/南投市.md "wikilink")
[Category:台灣的武德殿](../Category/台灣的武德殿.md "wikilink")
[Category:1937年台灣建立](../Category/1937年台灣建立.md "wikilink")