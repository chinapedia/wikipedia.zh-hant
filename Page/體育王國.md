**體育王國**（たいいくおうこく）是[日本](../Page/日本.md "wikilink")[TBS電視台中從](../Page/TBS電視台.md "wikilink")2002年10月12日開始播出的運動競技節目。

## 摘要

由於「挑戰冠軍王」中的意外事故，加強了安全對策而播出。第11、12回時[極限體能王SASUKE屬於此節目的特別節目](../Page/極限體能王SASUKE.md "wikilink")。日本以外的國家並未播出，全部的競技都以挑戰冠軍王為主軸，推出了類似的單元。

## 播出時間

日本每個星期六19時播出。

## 出演者

  - ，現在是[報道Station](../Page/報道Station.md "wikilink")[主持人](../Page/主持人.md "wikilink")

  -
  -
  -
  -
  - （2002年10月12日～2003年1月25日）

  - （2003年4月12日～2003年9月13日）

## 代表的競技

### 普通節目

  - <span style="color: Red;">[SASUKE](../Page/極限體能王SASUKE.md "wikilink")
    Junior</span>

<!-- end list -->

  -
    以12歲以下的小孩為主，設計一些小關卡，讓小朋友們體驗極限體能王的難度。其中分為Regular Stage以及Final Stage。

<!-- end list -->

  - <span style="color: Orange;">SASUKE Senior</span>

<!-- end list -->

  -
    以15歲以上的青年為主，主要是為了讓他們回味以前體格強魄的時期，獲得成就感。在極限體能王裡也有參賽的[原島雅美](../Page/原島雅美.md "wikilink")、[倉持捻都有參賽](../Page/倉持捻.md "wikilink")。

<!-- end list -->

  - <span style="color: Green;">Struck Out（ストラックアウト）</span>

<!-- end list -->

  -
    與『挑戰冠軍王』中的棒球九宮格類似，你可以選擇200萬元的球道或300萬元的，每次有12顆球，只要在這12顆球內把九塊壓克力版打下就算獲勝。不過如果失敗四次的話，遊戲即告結束。[松阪大輔也有參賽](../Page/松阪大輔.md "wikilink")，在300萬元的球道中打下了8顆球，全破者是児玉みゆき，是本節目開辦以來唯一一位過關者。

<!-- end list -->

  - <span style="color: Purple;">Kick Target（キックターゲット）</span>

<!-- end list -->

  -
    與『挑戰冠軍王』中的棒球九宮格類似，全破獎金200萬元。

<!-- end list -->

  - <span style="color: Blue;">Monkey Bars（モンキーバース）</span>

<!-- end list -->

  -
    必須闖過100公尺的雲梯，沒有限制時間。不過在起點時你必須在30秒以內做舉重上下10回（[啞鈴約](../Page/啞鈴.md "wikilink")30公斤）；20公尺時必須在40秒內做「ローラー」五回；40公尺時則是要在40秒內做20次[伏地挺身](../Page/伏地挺身.md "wikilink")。如果全部過關就可以獲得[極限體能王SASUKE的挑戰權及](../Page/極限體能王SASUKE.md "wikilink")30萬元日幣獎金。極限體能王全明星中的[竹田敏浩也有參加](../Page/竹田敏浩.md "wikilink")，但是在95.6公尺的地方落敗。此外在極限體能王也有參加的[山田康司](../Page/山田康司.md "wikilink")、[小林信治等都有參賽](../Page/小林信治.md "wikilink")，且都創下驚人紀錄。

### 特別節目

  - <span style="color: Gold;">極限體能王</span>：詳情請參考[極限體能王SASUKE](../Page/極限體能王SASUKE.md "wikilink")

## 參見

  - [極限體能王SASUKE](../Page/極限體能王SASUKE.md "wikilink")

## 外部連結

  - [体育王国公式網站](https://web.archive.org/web/20080130075434/http://www.tbs.co.jp/taiiku/)

[Category:TBS電視台綜藝節目](../Category/TBS電視台綜藝節目.md "wikilink")
[Category:2002年日本電視節目](../Category/2002年日本電視節目.md "wikilink")