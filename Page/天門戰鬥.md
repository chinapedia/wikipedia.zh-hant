**天門戰鬥**發生於1932年5月2日－30日，地點則是在[中國](../Page/中國.md "wikilink")[湖北省](../Page/湖北省_\(中華民國\).md "wikilink")[天门县张家场](../Page/天门县.md "wikilink")（在今天门市[九真镇](../Page/九真镇.md "wikilink")），中共方面又称为张家场战斗或九真张家场战斗\[1\]，交戰一方為[中華民國](../Page/中華民國.md "wikilink")[國民革命軍](../Page/國民革命軍.md "wikilink")，另一方則為[中國共產黨所領導之](../Page/中國共產黨.md "wikilink")[中國工農紅軍](../Page/中國工農紅軍.md "wikilink")[红三军](../Page/红三军.md "wikilink")。此次战斗，国军取得完全胜利，亦被认为是在[夏曦主导下](../Page/夏曦.md "wikilink")，红三军“得不偿失的消耗战”\[2\]。

这是自5月初，国军对红军发起的[刁汊湖戰鬥的另一戰場](../Page/刁汊湖戰鬥.md "wikilink")，戰鬥起初，「鄂豫皖剿匪軍左路」第44師師長蕭之楚急行佔領[應城](../Page/應城.md "wikilink")[皂市](../Page/皂市.md "wikilink")，並以此為據點對天門猛攻。\[3\]5月23日，[贺龙](../Page/贺龙.md "wikilink")[段德昌和代政委](../Page/段德昌.md "wikilink")[宋盘铭率红三军主力](../Page/宋盘铭.md "wikilink")，对皂市附近九真镇张家场\[4\]一带的国军第44师132旅及补充团(分由44師長蕭之楚、[徐源泉](../Page/徐源泉.md "wikilink")([湖北全省清郷督辦](../Page/湖北全省清郷督辦.md "wikilink"))率)发起袭击。29日，国军第44师131旅和补充一团从皂市来援，红军退至文家墩。30日，国家第10军特务团来援。红三军撤出战斗。国军131旅旅长于兆龙以下八百人伤亡\[5\]。31日，国军出击至柳河，红三军主力已撤退\[6\]。

张家场战斗遗址现已列入天门市[爱国主义教育基地](../Page/爱国主义教育基地.md "wikilink")\[7\]。

## 注释

## 參見

  - [國共內戰戰鬥列表](../Page/國共內戰戰鬥列表.md "wikilink")

## 參考文獻

  - [中華民國](../Page/中華民國.md "wikilink")[國防大學編](../Page/國防大學_\(臺灣\).md "wikilink")，《中國現代軍事史主要戰役表》

[Category:1932年国共内战战役](../Category/1932年国共内战战役.md "wikilink")
[Category:湖北民国时期战役](../Category/湖北民国时期战役.md "wikilink")
[Category:天门历史](../Category/天门历史.md "wikilink")
[Category:1932年5月](../Category/1932年5月.md "wikilink")

1.

2.

3.  [中華民國史事日誌](http://lib.jmu.edu.cn/departments2/magazine/philosophyol/1932.htm)


4.
5.

6.
7.