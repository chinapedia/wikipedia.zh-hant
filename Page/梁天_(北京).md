**梁天**，[中国](../Page/中国.md "wikilink")[北京人](../Page/北京.md "wikilink")，著名[喜剧演员](../Page/喜剧演员.md "wikilink")。

## 生平

  - 1977-1979在[北京军区装甲兵坦克六师第二十一团一营三连任炮长](../Page/北京军区.md "wikilink")
  - 1979-1980在北京军区装甲兵坦克六师文艺宣传队
  - 1980-1980在北京军区守备三师文艺宣传队
  - 1981-1981在[中国作家协会外联部及中国新闻社电影部协助工作](../Page/中国作家协会.md "wikilink")
  - 1981-1989在北京市服装八厂工作
  - 1989-1992在[北京电影学院青年电影制片厂工作](../Page/北京电影学院.md "wikilink")
  - 1994年3月组建北京好来西影视策划公司

## 家庭成员

  - 父亲：[范荣康](../Page/范荣康.md "wikilink")　　本名梁达，原[人民日报社副](../Page/人民日报.md "wikilink")[主编](../Page/主编.md "wikilink")
  - 母亲：[谌容](../Page/谌容.md "wikilink")
    　　[中国作家协会](../Page/中国作家协会.md "wikilink")[北京市分会专业](../Page/北京市.md "wikilink")[作家](../Page/作家.md "wikilink")
  - 哥哥：[梁左](../Page/梁左.md "wikilink") 　　中国艺术研究院研究员
  - 妹妹：[梁欢](../Page/梁欢.md "wikilink")
    　　[北京英氏艺术责任有限公司](../Page/北京英氏艺术责任有限公司.md "wikilink")
  - 妹夫：[英达](../Page/英达.md "wikilink")
    　　著名[导演](../Page/导演.md "wikilink")，[演员](../Page/演员.md "wikilink")

## 主要作品

### 电影

#### 出演

  - 《二子开店》
  - 《喜剧明星》
  - 《本命年》
  - 《[顽主](../Page/顽主.md "wikilink")》
  - 《[斗鸡](../Page/斗鸡.md "wikilink")》
  - 《龙年警官》
  - 《西行囚车》
  - 《老店》
  - 《过年》
  - 《烈火金刚》
  - 《[天生胆小](../Page/天生胆小.md "wikilink")》
  - 《男妇女主任》
  - 2008年 《[木乃伊3：龙帝之墓](../Page/木乃伊3：龙帝之墓.md "wikilink")》
  - 2010年 《[72家租客](../Page/72家租客.md "wikilink")》
  - 2013年 《[私人订制](../Page/私人订制.md "wikilink")》
  - 2015年 《[老炮儿](../Page/老炮儿.md "wikilink")》
  - 2017年 《[绑架者](../Page/绑架者.md "wikilink")》

#### 执导

  - 《防守反击》

### 电视剧

#### 出演

  - 《海马歌舞厅》
  - 《[我爱我家](../Page/我爱我家_\(家庭情景喜剧\).md "wikilink")》
  - 《经过上海》
  - 《[临时家庭](../Page/临时家庭.md "wikilink")》
  - 《大宅门1912》
  - 《盜墓筆記少年篇文章沙海》

#### 执导

  - 《低头不见抬头见》二十集
  - 《美好生活》二十集　
  - 《不谈爱情》六集
  - 《懒得结婚》十集　
  - 《太阳出世》八集
  - 《心想事成》五集

### 电视电影

  - 《称心如意》

## 获奖情况

  - 《天生胆小》荣获大众电影[百花奖最佳男女配角奖](../Page/百花奖.md "wikilink")、第五届上海大学生电影节最佳故事片、最佳男主角奖

## 参考资料

  - [梁天介绍](http://zhuanti.chinacity.net/9/14/21/276.htm)

## 外部鏈接

  -
[L](../Category/中国电影男演员.md "wikilink")
[L](../Category/中国电视男演员.md "wikilink")
[L](../Category/北京男演员.md "wikilink")
[T天](../Category/梁姓.md "wikilink")