**原懶猴**（*Pronycticebus*）是生存於[始新世中期至末期的](../Page/始新世.md "wikilink")[兔猴型下目](../Page/兔猴型下目.md "wikilink")。牠有一組差不多完整的標本在[德國發現的](../Page/德國.md "wikilink")。

## 形態

原懶猴在每隻腳的第二趾都有爪，很像現今的[原猴](../Page/原猴.md "wikilink")。\[1\]牠的[齒式是](../Page/齒式.md "wikilink")2:1:4:3。原懶猴有[顱骨外凸及](../Page/顱骨外凸.md "wikilink")[眶後棒](../Page/眶後棒.md "wikilink")。牠們的眼眶很大，可能是夜間或拂曉活動的。牠有較大的[陰莖骨](../Page/陰莖骨.md "wikilink")，體重約為825克。

## 分佈

原懶猴生活在[歐洲現今的](../Page/歐洲.md "wikilink")[德國](../Page/德國.md "wikilink")。

## 運動

基於原懶猴的肢骨形態，相信牠們是四足行走的，且會跳躍及攀爬。牠們較[假熊猴亞科少跳躍](../Page/假熊猴亞科.md "wikilink")，且較[兔猴科少用四足行走](../Page/兔猴科.md "wikilink")。\[2\]

## 參考

  - Conroy, G.C. 1990. Primate Evolution. W.W. Norton and Co.: New York.
  - Martin, R.D. 1990. Primate Origins and Evolution: A Phylogenetic
    Reconstruction. Princeton University Press: Princeton, New Jersey.

## 外部連結

  - <http://members.tripod.com/cacajao/pronycticebus_neglectus.html>
  - <http://www.aim.unizh.ch/StaffofInstitute/AffResearchers/uthal/Publications.html>
  - [Mikko's Phylogeny
    Archive](https://web.archive.org/web/20051126150447/http://www.fmnh.helsinki.fi/users/haaramo/metazoa/Deuterostoma/Chordata/Synapsida/Eutheria/primates/Strepsirrhini/Cercamoniinae.htm)

[Category:假熊猴科](../Category/假熊猴科.md "wikilink")

1.

2.