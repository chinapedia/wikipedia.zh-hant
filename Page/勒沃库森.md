**勒沃库森**\[1\]（），[德国](../Page/德国.md "wikilink")[北莱茵-威斯特法伦州南部的直辖市](../Page/北莱茵-威斯特法伦州.md "wikilink")。勒沃库森以[拜耳股份公司和](../Page/拜耳股份公司.md "wikilink")“[拜耳04勒沃库森足球俱乐部](../Page/勒沃库森足球俱乐部.md "wikilink")”而出名。

## 歷史

該市在古時純是鄉村一個，直到十九世紀晚期，才開始有所發展，化學工業興起。1861年第一家化學工廠於該市設立，生產[群青](../Page/群青.md "wikilink")。後來此工廠被[拜耳公司收購](../Page/拜耳.md "wikilink")，時為1891年；1912年拜耳把總部搬到該市。

[二次大戰期間](../Page/二次大戰.md "wikilink")，[英國皇家空軍曾於](../Page/英國皇家空軍.md "wikilink")1943年8月22日轟炸該廠；而[美國第八航空軍亦於](../Page/美國第八航空軍.md "wikilink")1943年12月1日發動同樣攻擊。

## 出身人物

  - [迪特马尔·默根堡](../Page/迪特马尔·默根堡.md "wikilink")：跳高運動員
  - [德特夫·史伦夫](../Page/德特夫·史伦夫.md "wikilink")：前[NBA球員](../Page/NBA.md "wikilink")

## 友好城市

  - [奧盧](../Page/奧盧.md "wikilink")

  - [布拉克內爾](../Page/布拉克內爾.md "wikilink")

  - [阿斯克新城](../Page/阿斯克新城.md "wikilink")

  - [施韋特](../Page/施韋特_\(勃蘭登堡州\).md "wikilink")

  - [卢布尔雅那](../Page/卢布尔雅那.md "wikilink")

  - [無錫](../Page/無錫.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

[L](../Category/北莱茵-威斯特法伦州市镇.md "wikilink")

1.  [Britannica Online Traditional Chinese
    Edition](http://wordpedia.eb.com/tbol/article?i=044002&db=big5&q=Leverkusen)