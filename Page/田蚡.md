**田蚡**\[1\]（），[西漢](../Page/西漢.md "wikilink")[內史長陵](../Page/內史_\(秦漢行政區\).md "wikilink")（今[陝西](../Page/陝西.md "wikilink")[咸陽市東北](../Page/咸陽.md "wikilink")）人，[汉武帝的](../Page/汉武帝.md "wikilink")[舅舅](../Page/舅舅.md "wikilink")、大臣，封[武安侯](../Page/武安侯.md "wikilink")。後來與魏其侯[窦婴不和](../Page/窦婴.md "wikilink")，設計殺害[灌夫及窦婴](../Page/灌夫.md "wikilink")，窦婴死後，田蚡亦發瘋而亡。《[史記](../Page/史記.md "wikilink")》中說，[占卜師分析田蚡是因為窦婴和灌夫兩鬼的靈魂缠身而死亡](../Page/占卜師.md "wikilink")。

## 生平

田蚡相貌醜陋，巧於文辭。魏其侯[窦婴掌权时](../Page/窦婴.md "wikilink")，田蚡还是个郎官，往来于窦婴家，陪窦婴饮酒，时跪时起，对窦婴的恭敬就好像是窦婴家的晚辈一样。

其同母異父的姐姐[王氏后成为](../Page/孝景王皇后.md "wikilink")[景帝的第二任](../Page/汉景帝.md "wikilink")[皇后](../Page/皇后.md "wikilink")，外甥[劉徹又被立为](../Page/漢武帝.md "wikilink")[太子](../Page/太子.md "wikilink")。[建元元年](../Page/建元_\(汉武帝\).md "wikilink")（前140年），劉徹[登基](../Page/登基.md "wikilink")，為漢武帝。同年田蚡被封為武安侯，拜[太尉](../Page/太尉.md "wikilink")，後遷[丞相](../Page/丞相.md "wikilink")，受到[趙綰](../Page/趙綰.md "wikilink")、[王臧的牽連被免職](../Page/王臧.md "wikilink")。建元六年（前135年），[太皇太后](../Page/太皇太后.md "wikilink")[窦氏去世](../Page/窦皇后_\(汉文帝\).md "wikilink")，田蚡被封为[丞相](../Page/丞相.md "wikilink")，好[儒術](../Page/儒術.md "wikilink")，立[五經博士](../Page/五經博士.md "wikilink")。独断专横，“由此滋骄，治宅甲诸第”。[元光三年](../Page/元光_\(西汉\).md "wikilink")（前132年）[黄河改道南流](../Page/黄河.md "wikilink")，十六郡遭严重水灾，他因封邑鄃（今山东平原）在旧河道以北，没有受到水灾，力阻治理，使治河工作停止二十年之久。

田蚡[拜相之後](../Page/拜相.md "wikilink")，入朝奏事，一坐就是很長時間，他所推薦的人，有人從[平民直奔](../Page/平民.md "wikilink")[二千石級](../Page/二千石.md "wikilink")，任命大批官員，不讓皇帝說話，皇帝忍耐不住了，於是說：「你封的官吏已經完了沒有？我也想封個幾個官呢！」田蚡也曾經要求把考工官署的土地收歸己有，讓自己擴建住宅，皇帝諷刺他說：「怎麼不把兵器倉庫也拿去！」從這以後才收斂一些。

田蚡身材矮小，其貌不揚，可是地位已經很高了。有一次，他請客人宴飲，讓他的哥哥蓋侯王信南向坐，自己卻東向坐在尊貴的位置，田蚡自認自己是當朝宰相之尊，就算是私下跟兄長飲酒，也不能委曲自己，從此更加驕縱。他修建住宅，其規模、豪華超過了所有的貴族的府第。田地莊園都極其肥沃，他派到各郡縣去購買器物的人，在大道上絡繹不絕。前堂擺投著鐘鼓，豎立著曲柄長幡，在後房的美女數以百計。諸侯奉送給他的珍寶金玉、狗馬和玩好器物，數也數不清。

田蚡得志後，與窦婴有許多糾紛，包括相約遲到，與索求土地不遂等。元光四年（前131年）田蚡娶燕王[刘定国之女为夫人](../Page/刘定国.md "wikilink")，辦婚宴，[王太后命令大臣都得去喜酒場上祝賀](../Page/孝景王皇后.md "wikilink")，灌夫不願去，魏其侯竇嬰卻強迫[灌夫一起參與](../Page/灌夫.md "wikilink")，田蚡在酒席中受到眾人推崇，而竇嬰卻遭到冷落，灌夫很生氣。恰好灌夫的[結誼親戚臨汝侯灌賢](../Page/結誼.md "wikilink")（[灌嬰的後代](../Page/灌嬰.md "wikilink")）與[衛尉](../Page/衛尉.md "wikilink")[程不識竊竊私語](../Page/程不識.md "wikilink")，沒有迴避竇嬰的敬酒。灌夫為人剛直，大罵灌賢：「你平時說程不識不值一錢，今天長輩敬你酒，你卻像是女孩子一樣跟程不識說悄悄話咧！」田蚡知道灌夫尊敬[李廣](../Page/李廣.md "wikilink")，就出來勸解：「程將軍和李將軍都是[衛尉](../Page/衛尉.md "wikilink")，你當眾侮辱程將軍，不是也罵了李將軍？」灌夫說：「今天把我殺了，我都不在乎，管甚麼程將軍、李將軍！」\[2\]，田蚡向汉武帝弹劾灌夫“罵座不敬”，大毀灌夫平素橫行不法，將灌夫處死。竇嬰怒而揭露田蚡與淮南王[刘安來往](../Page/刘安.md "wikilink")，田蚡心生怨恨。

元光五年（前130年），魏其侯竇嬰曾经受[汉景帝遗诏](../Page/汉景帝.md "wikilink")“事有不便，以便宜论上”，灌夫被灭族前，诸位大臣没有再敢进言相救的。竇嬰就上书言之，书奏上，而案尚书却找不到遗诏的记录，就弹劾窦婴矫先帝诏，罪当弃市。十二月，窦婴在渭城被以“偽造聖旨罪”[滅族](../Page/滅族.md "wikilink")。竇嬰死後，春三月乙卯，丞相田蚡也得病去世。田蚡病倒时，病中喃喃口呼謝罪，家人請來能視陰陽鬼事之人，得知是魏其侯窦婴和灌夫兩鬼守住田蚡，鞭笞索命，群醫束手，只能眼睜睜看著田蚡不治。田蚡死後，其子[田恬继承武安侯爵](../Page/田恬.md "wikilink")，公元前127年因穿[直裾入宫不合礼仪被废除了爵位](../Page/直裾.md "wikilink")\[3\]。

## 后事

元狩元年（前122年），[淮南王](../Page/淮南王.md "wikilink")[刘安谋反的事被发觉了](../Page/刘安.md "wikilink")，汉武帝追查此事。刘安前次来朝，田蚡担任[太尉](../Page/太尉.md "wikilink")，当时到霸上来迎接淮南王说：“皇上没有太子，大王最贤明，又是高祖的孙子，一旦皇上去世，不是大王继承皇位，还应该是谁呢！”刘安十分欢喜，送给田蚡许多金银财物。汉武帝自从[窦婴的事件发生时就不认为武安侯是对的](../Page/窦婴.md "wikilink")，只是碍着[王太后的缘故罢了](../Page/王太后.md "wikilink")。等听到刘安向田蚡送金银财物时，汉武帝说：“假使武安侯还活着的话，该灭族了。”

## 分析

《[漢書](../Page/漢書.md "wikilink")》「竇嬰田蚡灌夫列傳」說：「魏其銳身為救。曰終不令灌仲孺獨死。上無意殺魏其。乃有蜚語聞上。以十二月晦，棄市。及春，武安病，專呼謝罪。使視鬼者視之。見魏其灌夫共守，欲殺之，竟死。」說明竇嬰矯詔判死原因為武安侯田蚡[誣告魏其侯矯先帝遺詔](../Page/誣告.md "wikilink")，及毀謗朝廷罪狀，武帝大怒命有司案治，一族獲罪抄斬。另外在魏其侯死後，田蚡亦發瘋而亡。

有人認為是武帝因田蚡在他登基之初的那段時間，倚仗自己是王太后的弟弟，肆無忌憚，令武帝早就暗中對他不滿。而漢武帝故意找巫师来医治他，並且以鬼神之說恫嚇，因此令田蚡受到惊吓不治而亡。

## 注释

<references />

## 參考資料

  -
  - 《[汉书](../Page/汉书.md "wikilink")·[卷五十二](../Page/s:漢書/卷052.md "wikilink")》

[Category:西汉丞相](../Category/西汉丞相.md "wikilink")
[Category:西汉列侯](../Category/西汉列侯.md "wikilink")
[Category:汉朝外戚](../Category/汉朝外戚.md "wikilink")
[Category:咸阳人](../Category/咸阳人.md "wikilink")
[F蚡](../Category/田姓.md "wikilink")

1.
2.  這是[成語](../Page/成語.md "wikilink")“灌夫罵座”、“一錢不值”的由來
3.  《史记》卷107：元朔三年，武安侯坐衣襜褕入宫，不敬。