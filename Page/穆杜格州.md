**穆杜格州**（[索馬里語](../Page/索馬里語.md "wikilink")：）是[索馬里中部的一個州](../Page/索馬里.md "wikilink")，西鄰[埃塞俄比亞](../Page/埃塞俄比亞.md "wikilink")，東臨[印度洋](../Page/印度洋.md "wikilink")。面積46,126平方公里。首府[加勒卡約](../Page/加勒卡約.md "wikilink")（Gaalkacyo）。目前由[邦特蘭所控制](../Page/邦特蘭.md "wikilink")。

## 行政區域

穆杜格州包括下列行政區域：

  - [加勒卡約](../Page/加勒卡約.md "wikilink")（Gaalkacyo）（區域首都）
  - [加勒多葛博](../Page/加勒多葛博.md "wikilink")（Galdogob）
  - [賀伯尤](../Page/賀伯尤.md "wikilink")（Hobyo）
  - [哈拉爾得荷雷](../Page/哈拉爾得荷雷.md "wikilink")（Harardheere）
  - [吉利班](../Page/吉利班.md "wikilink")（Jiriiban）

## 重要城鎮

  - 阿巴雷(Abaarey)
  - [阿福古馬蘇雷](../Page/阿福古馬蘇雷.md "wikilink")(Afgumasoorey)
  - [巴杰拉](../Page/巴杰拉.md "wikilink")(Bajeela)
  - [巴蘭巴爾雷](../Page/巴蘭巴爾雷.md "wikilink")(Balanballe)
  - [班迪拉得雷](../Page/班迪拉得雷.md "wikilink")(Bandiiradley)
  - [巴爾達葛爾](../Page/巴爾達葛爾.md "wikilink")(Bardagool)
  - [比塔雷](../Page/比塔雷.md "wikilink")(Bitaale)
  - [巴卡得溫](../Page/巴卡得溫.md "wikilink")(Bacaadweyn)
  - **[貝伊拉](../Page/貝伊拉.md "wikilink")**(Beyra)
  - [布得布的](../Page/布得布的.md "wikilink")(Budbud)
  - **[布爾薩拉斯](../Page/布爾薩拉斯.md "wikilink")(Bur Saalax)**
  - [賽爾胡爾](../Page/賽爾胡爾.md "wikilink")(Ceel Dibir)
  - [賽爾胡爾](../Page/賽爾胡爾.md "wikilink")(Ceel Huur)
  - [可爾古拉](../Page/可爾古拉.md "wikilink")(Colguula)
  - [達巴羅](../Page/達巴羅.md "wikilink")(Dabarroow)
  - [達嘎力](../Page/達嘎力.md "wikilink")(Dagaari)
  - [達吉馬雷](../Page/達吉馬雷.md "wikilink")(Dajimaale)
  - [都口爾](../Page/都口爾.md "wikilink")(Docol)
  - [都卡卡](../Page/都卡卡.md "wikilink")(Duqaaqa)
  - [嘎坎費爾](../Page/嘎坎費爾.md "wikilink")(Gacanfare)
  - [嘎里蘇爾](../Page/嘎里蘇爾.md "wikilink")(Galinsoor)
  - [嘎萬得黑雷](../Page/嘎萬得黑雷.md "wikilink")(Gawaan Dheere)
  - [葛達得](../Page/葛達得.md "wikilink")(Godad)
  - [葛羅爾](../Page/葛羅爾.md "wikilink")(Golol)
  - [葛拉羅](../Page/葛拉羅.md "wikilink")(Gowlalo)
  - **[哈拉布克哈得](../Page/哈拉布克哈得.md "wikilink")(Halabookhad)**
  - [哈拉胡爾](../Page/哈拉胡爾.md "wikilink")(Haraahur))
  - [希拉拉也](../Page/希拉拉也.md "wikilink")(Hiilalaaye)
  - [希爾莫](../Page/希爾莫.md "wikilink")(Hilmo)
  - [艾達萬](../Page/艾達萬.md "wikilink")(Iidaan)
  - [拉萬雷](../Page/拉萬雷.md "wikilink")(Laanwaaleey)
  - [拉薩卡達雷](../Page/拉薩卡達雷.md "wikilink")(Laasa Cadale)
  - [拉庫拉克賽](../Page/拉庫拉克賽.md "wikilink")(Laguraqsay)
  - [米爾薩雷](../Page/米爾薩雷.md "wikilink")(Mirsaale)
  - [可蘭樓](../Page/可蘭樓.md "wikilink")(Qaranrow)
  - [卡而可拉達希爾](../Page/卡而可拉達希爾.md "wikilink")(Qarqoora Dheer)
  - [可蘇爾迪雷](../Page/可蘇爾迪雷.md "wikilink")(Qosoltire)
  - [卡也達拉](../Page/卡也達拉.md "wikilink")(Qaydara)
  - **[坎薩雷](../Page/坎薩雷.md "wikilink")(Qansaxle)**
  - **[魯荷](../Page/魯荷.md "wikilink")(Roox)**
  - **[理古馬內](../Page/理古馬內.md "wikilink")(Rigoomane)**
  - [薩巴卡得](../Page/薩巴卡得.md "wikilink")(Sabacad)
  - [薩其羅](../Page/薩其羅.md "wikilink")(Saqiiro)
  - [薩哈古侖](../Page/薩哈古侖.md "wikilink")(Saxaqurun)
  - [西得斯希格雷](../Page/西得斯希格雷.md "wikilink")(Sedex Xigle)
  - [瓦納基爾](../Page/瓦納基爾.md "wikilink")(Wanakir)
  - [瓦嘎羅](../Page/瓦嘎羅.md "wikilink")(Wargalo)
  - [威西爾](../Page/威西爾.md "wikilink")(Wisil)
  - [荷羅打嘎雷](../Page/荷羅打嘎雷.md "wikilink")(Xero Dhagaxley)
  - [辛達瓦可](../Page/辛達瓦可.md "wikilink")(Xiindawaco)
  - [辛古得](../Page/辛古得.md "wikilink")(Xingood)

## 參考資料

## 外部鏈結

  - [穆杜格州行政區域圖](http://reliefweb.int/sites/reliefweb.int/files/resources/121024_Administrative_Map_Mudug_A4.pdf)

[M](../Category/索馬里行政區劃.md "wikilink")
[\*](../Category/穆杜格州.md "wikilink")
[Category:加勒穆杜格](../Category/加勒穆杜格.md "wikilink")