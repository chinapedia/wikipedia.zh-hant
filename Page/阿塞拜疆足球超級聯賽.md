**阿塞拜疆足球超級聯賽**是[阿塞拜疆足球協會舉辦的職業](../Page/阿塞拜疆足球協會.md "wikilink")[足球聯賽](../Page/足球.md "wikilink")，阿塞拜疆足球超級聯賽一共有
8 支角逐。

## 參賽球隊

2018–19年阿塞拜疆足球超級聯賽參賽隊伍共有 8 支。

| 中文名稱                                    | 英文名稱             | 所在城市                               | 上季成績     |
| --------------------------------------- | ---------------- | ---------------------------------- | -------- |
| [卡拉巴克](../Page/卡拉巴克足球會.md "wikilink")   | Qarabağ FK       | [阿格達姆](../Page/阿格達姆.md "wikilink") | 第 1 位    |
| [卡巴拉](../Page/卡巴拉足球會.md "wikilink")     | Gabala FK        | [蓋貝萊](../Page/蓋貝萊.md "wikilink")   | 第 2 位    |
| [尼菲治巴庫](../Page/尼菲治巴庫足球會.md "wikilink") | Neftchi Baku PFK | [巴庫](../Page/巴庫.md "wikilink")     | 第 3 位    |
| [施拉](../Page/施拉足球會.md "wikilink")       | Zira FK          | [巴庫](../Page/巴庫.md "wikilink")     | 第 4 位    |
| [薩姆加耶特](../Page/薩姆加耶特足球會.md "wikilink") | Sumgayit FK      | [蘇姆蓋特](../Page/蘇姆蓋特.md "wikilink") | 第 5 位    |
| [克斯拉](../Page/克斯拉足球會.md "wikilink")     | Keşla FK         | [克斯拉](../Page/克斯拉.md "wikilink")   | 第 6 位    |
| [沙巴爾](../Page/沙巴爾足球會.md "wikilink")     | Səbail FK        | [巴庫](../Page/巴庫.md "wikilink")     | 第 7 位    |
| [沙巴巴庫](../Page/沙巴巴庫足球會.md "wikilink")   | Sabah FK         | [巴庫](../Page/巴庫.md "wikilink")     | 甲組，第 5 位 |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[1\]

## 奪冠次數

### 按球會

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>冠軍次數</p></th>
<th><p>冠軍年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/尼菲治巴庫職業足球會.md" title="wikilink">尼菲治巴庫</a></p></td>
<td><center>
<p>8</p></td>
<td><p>1992, 1995–96, 1996–97, 2003–04, 2004–05, 2010–11, 2011–12, 2012–13</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡拉巴克足球會.md" title="wikilink">卡拉巴克</a></p></td>
<td><center>
<p>6</p></td>
<td><p>1993, 2013–14, 2014–15, 2015–16, 2016–17, 2017–18</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/森基亞足球會.md" title="wikilink">森基亞</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1999–00, 2000–01, 2001–02</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卡柏斯足球會.md" title="wikilink">卡柏斯</a></p></td>
<td><center>
<p>3</p></td>
<td><p>1994–95, 1997–98, 1998–99</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/國際巴古體育會.md" title="wikilink">國際巴古</a></p></td>
<td><center>
<p>2</p></td>
<td><p>2007–08, 2009–10</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴庫足球會.md" title="wikilink">巴庫</a></p></td>
<td><center>
<p>2</p></td>
<td><p>2005–06, 2008–09</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈扎爾蘭卡倫足球會.md" title="wikilink">哈扎爾</a></p></td>
<td><center>
<p>1</p></td>
<td><p>2006–07</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杜蘭托武茲足球會.md" title="wikilink">托武茲</a></p></td>
<td><center>
<p>1</p></td>
<td><p>1993–94</p></td>
</tr>
</tbody>
</table>

### 按城市

<table>
<thead>
<tr class="header">
<th><p>城市</p></th>
<th><p>冠軍次數</p></th>
<th><p>球會（冠軍次數）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/巴庫.md" title="wikilink">巴庫</a></p></td>
<td><center>
<p>18</p></td>
<td><p><a href="../Page/尼菲治巴庫職業足球會.md" title="wikilink">尼菲治巴庫</a>（8），<a href="../Page/卡拉巴克足球會.md" title="wikilink">卡拉巴克</a>（6），<a href="../Page/國際巴古體育會.md" title="wikilink">國際巴古</a>（2），<a href="../Page/巴庫足球會.md" title="wikilink">巴庫</a>（2）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沙姆基爾.md" title="wikilink">沙姆基爾</a></p></td>
<td><center>
<p>3</p></td>
<td><p><a href="../Page/森基亞足球會.md" title="wikilink">森基亞</a>（3）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/占賈.md" title="wikilink">占賈</a></p></td>
<td><center>
<p>3</p></td>
<td><p><a href="../Page/卡柏斯足球會.md" title="wikilink">卡柏斯</a>（3）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/連科蘭.md" title="wikilink">連科蘭</a></p></td>
<td><center>
<p>1</p></td>
<td><p><a href="../Page/哈扎爾蘭卡倫足球會.md" title="wikilink">哈扎爾</a>（1）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塔烏茲.md" title="wikilink">塔烏茲</a></p></td>
<td><center>
<p>1</p></td>
<td><p><a href="../Page/杜蘭托武茲足球會.md" title="wikilink">托武茲</a>（1）</p></td>
</tr>
</tbody>
</table>

## 考參

[Category:阿塞拜疆足球](../Category/阿塞拜疆足球.md "wikilink")
[Category:國家頂級足球聯賽](../Category/國家頂級足球聯賽.md "wikilink")

1.