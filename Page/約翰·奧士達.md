**约翰·奥斯特**（**John
Oster**，）是[威爾斯足球運動員](../Page/威爾斯.md "wikilink")，出生於[英格蘭](../Page/英格蘭.md "wikilink")[林肯郡](../Page/林肯郡.md "wikilink")[波士頓](../Page/波士頓_\(林肯郡\).md "wikilink")。目前效力[英格蘭足球冠軍聯賽球隊](../Page/英格蘭足球冠軍聯賽.md "wikilink")[唐卡士打](../Page/唐卡斯特流浪足球俱乐部.md "wikilink")。

## 生平

職業生涯[甘士比首度亮相](../Page/格林斯比镇足球俱乐部.md "wikilink")，僅上陣25次年僅18歲即以150萬[英鎊加盟](../Page/英鎊.md "wikilink")[愛華頓](../Page/愛華頓.md "wikilink")<small>\[1\]</small>。未能在愛華頓更進一步，其後轉投新特蘭及般尼。在效力新特蘭期間曾用[氣槍意外擊中預備隊隊友马克](../Page/氣槍.md "wikilink")·马利（Mark
Maley）的[眼睛](../Page/眼睛.md "wikilink")<small>\[2\]</small>；在外借到[-{zh-hans:利兹联;zh-hk:列斯聯}-時亦因於](../Page/利兹联足球俱乐部.md "wikilink")[聖誕派對鬧事而被遣回](../Page/聖誕節.md "wikilink")<small>\[3\]</small>。

2005年8月2日加盟[雷丁](../Page/雷丁足球俱乐部.md "wikilink")<small>\[4\]</small>。於2008年夏天遭棄用<small>\[5\]</small>，於8月11日投效[水晶宮](../Page/水晶宫足球俱乐部.md "wikilink")，初步簽約6個月<small>\[6\]</small>。2009年夏季離開水晶宮後，以短約形式加盟另一支[英冠球隊](../Page/英冠.md "wikilink")[唐卡士打](../Page/唐卡斯特流浪足球俱乐部.md "wikilink")<small>\[7\]</small>。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -
[Category:威爾斯足球運動員](../Category/威爾斯足球運動員.md "wikilink")
[Category:甘士比球員](../Category/甘士比球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:新特蘭球員](../Category/新特蘭球員.md "wikilink")
[Category:列斯聯球員](../Category/列斯聯球員.md "wikilink")
[Category:巴恩斯利球员](../Category/巴恩斯利球员.md "wikilink")
[Category:雷丁球員](../Category/雷丁球員.md "wikilink")
[Category:水晶宫球員](../Category/水晶宫球員.md "wikilink")
[Category:唐卡士打球員](../Category/唐卡士打球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")

1.
2.
3.
4.
5.
6.
7.