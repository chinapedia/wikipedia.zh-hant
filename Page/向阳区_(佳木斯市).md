**向阳区**是[黑龙江省](../Page/黑龙江省.md "wikilink")[佳木斯市下辖的一个区](../Page/佳木斯市.md "wikilink")，是佳木斯市的中心城区，也是佳木斯市经济、社会、文化的中心。

## 行政区划

向阳区行政区划西起红旗路、学院街，东至中山街，南起[同三高速公路](../Page/同三高速公路.md "wikilink")，北至[松花江](../Page/松花江.md "wikilink")（[柳树岛](../Page/柳树岛.md "wikilink")）。
向阳区下无乡镇级行政区，区政府直辖各社区和村。

2016年撤销原下辖的6个街道\[1\]\[2\]：[西林街道](../Page/西林街道_\(佳木斯市\).md "wikilink")，[保卫街道](../Page/保卫街道.md "wikilink")，[桥南街道](../Page/桥南街道_\(佳木斯市\).md "wikilink")，[西南岗街道](../Page/西南岗街道.md "wikilink")，[建设街道](../Page/建设街道_\(佳木斯市\).md "wikilink")，[长安街道](../Page/长安街道_\(佳木斯市\).md "wikilink")。

## 参考资料

## 外部链接

  - [佳木斯向阳区政府网](http://www.xyq.gov.cn/)

[\*](../Category/向阳区_\(佳木斯市\).md "wikilink")
[佳木斯](../Category/黑龙江市辖区.md "wikilink")

1.
2.  [2016年中华人民共和国县以下行政区划变更情况](http://www.mca.gov.cn/article/sj/tjbz/a/2016/201702/201702231129.html)