\-{T|zh-cn:分数单位;zh-tw:單位分數}-

**-{A|zh-cn:分数单位;zh-tw:單位分數}-**，或称**-{zh-cn:单位分数;zh-tw:分數單位}-**，是分子是1，分母是正[整數并寫成](../Page/整數.md "wikilink")[分數的](../Page/分數.md "wikilink")[有理數](../Page/有理數.md "wikilink")，。因此單位分數都是某一個正整數的[倒數](../Page/倒數.md "wikilink")，1/n。例如1/2、1/3、1/4、1/5等都是-{A|zh-cn:分数单位;zh-tw:單位分數}-。

## 基本代數

\-{A|zh-cn:分数单位;zh-tw:單位分數}-的[積必為](../Page/乘法.md "wikilink")-{A|zh-cn:分数单位;zh-tw:單位分數}-。

\[\frac{1}{m} \cdot \frac{1}{n} = \frac{1}{nm}\]

但[加法](../Page/加法.md "wikilink")、[減法及](../Page/減法.md "wikilink")[除法的結果不一定為](../Page/除法.md "wikilink")-{A|zh-cn:分数单位;zh-tw:單位分數}-

\[\frac{1}{m} + \frac{1}{n} = \frac{n+m}{nm}\]

\[\frac{1}{m} - \frac{1}{n} = \frac{n-m}{nm}\]

\[\frac1x \div \frac1y = \frac{y}{x}.\]

## 其他

它們的和

\(\sum_{k=1}^n \frac{1}{k} = 1 + \frac{1}{2} + \frac{1}{3} + \frac{1}{4} + \ldots + \frac{1}{n-1} + \frac{1}{n}\)

就是[調和級數](../Page/調和級數.md "wikilink")，隨着*n*增大，它會逐漸接近[ln](../Page/自然對數.md "wikilink")(*n*)+[γ](../Page/歐拉-馬歇羅尼常數.md "wikilink")。

所有單位分數之[和趨向](../Page/加法.md "wikilink")[無限](../Page/無限.md "wikilink")。

\(\sum_{k=1}^\infty \frac{1}{k} \to \infty\)

所有有理數都可以寫成單位分數之和(見[古埃及分數](../Page/古埃及分數.md "wikilink"))。

[Category:實數](../Category/實數.md "wikilink")
[Category:分数](../Category/分数.md "wikilink")
[Category:算术](../Category/算术.md "wikilink")
[Category:一](../Category/一.md "wikilink")