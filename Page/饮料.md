[Serving_ice_cold_cola.jpg](https://zh.wikipedia.org/wiki/File:Serving_ice_cold_cola.jpg "fig:Serving_ice_cold_cola.jpg")

[Beer_bottles.jpg](https://zh.wikipedia.org/wiki/File:Beer_bottles.jpg "fig:Beer_bottles.jpg")

**饮料**或**飲品**是指經過加工的液体，供予饮用，分为[软饮料和](../Page/软饮料.md "wikilink")[酒精饮料](../Page/酒精饮料.md "wikilink")。

## 常見分類方式

1.  依包裝方式（[罐裝飲料](../Page/易開罐.md "wikilink")、[瓶裝飲料](../Page/寶特瓶.md "wikilink")、[鋁箔包飲料](../Page/利樂包裝.md "wikilink")）
2.  依是否含有[酒精](../Page/酒精.md "wikilink")（[酒類](../Page/酒類.md "wikilink")、無酒精飲料）
3.  依溫度（冷飲、熱飲）[糖](../Page/糖.md "wikilink")（無糖飲料）
4.  依制作材料（[茶饮料](../Page/茶.md "wikilink")(茶叶)、酒饮料(酒精)、[咖啡饮料](../Page/咖啡.md "wikilink")([咖啡豆](../Page/咖啡豆.md "wikilink"))、[蔬果汁饮料](../Page/果汁.md "wikilink")(各类[蔬果](../Page/蔬果.md "wikilink"))、牛奶饮料([牛奶](../Page/牛奶.md "wikilink"))）

## 種類

### 酒類

  - [水果酒](../Page/水果酒.md "wikilink")
      - [葡萄酒](../Page/葡萄酒.md "wikilink")
          - [红葡萄酒](../Page/红葡萄酒.md "wikilink")
          - [白葡萄酒](../Page/白葡萄酒.md "wikilink")
          - [粉红葡萄酒](../Page/桃红葡萄酒.md "wikilink")
          - [氣泡红葡萄酒](../Page/氣泡红葡萄酒.md "wikilink")
          - [氣泡白葡萄酒](../Page/气泡酒.md "wikilink")
          - [氣泡粉红葡萄酒](../Page/氣泡粉红葡萄酒.md "wikilink")
          - [加强葡萄酒](../Page/加强葡萄酒.md "wikilink")
          - [白兰地](../Page/白兰地.md "wikilink") (蒸馏酒)
          - 無酒精葡萄酒
      - [苹果酒](../Page/苹果酒.md "wikilink")
          - [氣泡苹果酒](../Page/苹果气泡酒.md "wikilink")
      - [梨子酒](../Page/梨子酒.md "wikilink")
      - [李子酒](../Page/李子酒.md "wikilink")
      - [梅子酒](../Page/梅酒.md "wikilink")
  - [穀物酒](../Page/糧食酒.md "wikilink")
      - [啤酒](../Page/啤酒.md "wikilink")
          - 無酒精啤酒
      - [濁酒](../Page/濁酒.md "wikilink")
      - [白酒](../Page/白酒.md "wikilink") (日本酒)
      - [燒酒](../Page/燒酒.md "wikilink") (蒸馏酒)
      - [威士忌](../Page/威士忌.md "wikilink") (蒸馏酒)
      - [伏特加](../Page/伏特加.md "wikilink") (蒸馏酒)
      - [琴酒](../Page/琴酒.md "wikilink") (蒸馏酒)
  - 其他原料酒
      - [朗姆酒](../Page/朗姆酒.md "wikilink") (蒸馏酒)
      - 龙舌兰酒 (蒸馏酒)
  - [利口酒](../Page/利口酒.md "wikilink")
  - 混合酒（[鸡尾酒](../Page/鸡尾酒.md "wikilink")）
      - 無酒精鸡尾酒 (mocktail)

### 茶類

  - [麥茶](../Page/麥茶.md "wikilink")
  - [白茶](../Page/白茶.md "wikilink")
  - [绿茶](../Page/绿茶.md "wikilink")
  - [黄茶](../Page/黄茶.md "wikilink")
  - [青茶](../Page/青茶.md "wikilink")（[乌龙茶](../Page/乌龙茶.md "wikilink")）
  - [红茶](../Page/红茶.md "wikilink")
  - [黑茶](../Page/黑茶.md "wikilink")
      - [普洱茶](../Page/普洱茶.md "wikilink")
  - 加工茶
      - [花茶](../Page/花茶.md "wikilink")
      - [紧压茶](../Page/紧压茶.md "wikilink")
          - [砖茶](../Page/砖茶.md "wikilink")
          - [沱茶](../Page/沱茶.md "wikilink")
  - 再加工茶
      - [袋茶](../Page/袋茶.md "wikilink")
      - [速溶茶](../Page/速溶茶.md "wikilink")
      - [液体茶](../Page/液体茶.md "wikilink")
  - [草本茶](../Page/草本茶.md "wikilink")
      - [菊花茶](../Page/菊花茶.md "wikilink")
  - [奶茶](../Page/奶茶.md "wikilink")

### 果汁類

  - [橙汁](../Page/橙汁.md "wikilink")
  - [苹果汁](../Page/苹果汁.md "wikilink")
  - [柠檬汁](../Page/柠檬汁.md "wikilink")
  - [葡萄汁](../Page/葡萄汁.md "wikilink")
  - [芒果汁](../Page/芒果汁.md "wikilink")
  - [菠萝汁](../Page/菠萝汁.md "wikilink")

### 碳酸飲料

  - [汽水](../Page/汽水.md "wikilink")
  - [可乐](../Page/可乐.md "wikilink")
  - [沙士](../Page/沙士_\(飲品\).md "wikilink")

### 咖啡

  - [拿鐵咖啡](../Page/拿鐵咖啡.md "wikilink")
  - [美式咖啡](../Page/美式咖啡.md "wikilink")
  - [摩卡咖啡](../Page/摩卡咖啡.md "wikilink")
  - [小果咖啡](../Page/小果咖啡.md "wikilink")
  - [中果咖啡](../Page/中果咖啡.md "wikilink")
  - [大果咖啡](../Page/大果咖啡.md "wikilink")
  - [高产咖啡](../Page/高产咖啡.md "wikilink")

### 其他

  - [牛奶](../Page/牛奶.md "wikilink")
  - [豆漿](../Page/豆漿.md "wikilink")
  - [蜂蜜水](../Page/蜂蜜水.md "wikilink")
  - 運動飲料
  - 提神飲料
  - 冬瓜茶
  - [可可](../Page/可可.md "wikilink")（巧克力）
      - 白可可
      - 香草可可
  - 乳酸飲料

## 參見

  - [軟性飲料](../Page/軟性飲料.md "wikilink")
  - [易開罐](../Page/易開罐.md "wikilink")
  - [含糖飲料稅](../Page/含糖飲料稅.md "wikilink")

[饮料](../Category/饮料.md "wikilink")