《**世說新語**》是[魏晉南北朝時期](../Page/魏晉南北朝.md "wikilink")[筆記小說的代表作](../Page/筆記小說.md "wikilink")，內容大多記載[東漢至東晉間的高士名流的言行風貌和軼文趣事](../Page/東漢.md "wikilink")，由[南朝宋](../Page/南朝宋.md "wikilink")[劉義慶召集門下](../Page/劉義慶.md "wikilink")[食客共同編撰](../Page/食客.md "wikilink")。全書分上、中、下三卷，依內容分有：德行、言語、政事、文學、方正、雅量、識鑑等等，共三十六類（門），每類收有若干則，全書共一千多則，每則文字長短不一，有的數行，有的三言兩語，由此可見志人小說隨手而記的特性。

## 內容

《世說新語》主要记述[士人的生活和思想](../Page/士人.md "wikilink")，及统治阶级当时的情況，反映了魏晉時期文人的思想言行和[門閥社會的生活面貌](../Page/門閥.md "wikilink")，这样的描寫有助讀者了解當時的人所處的時代狀況及政治社會環境，更清楚地見識所謂魏晉[清談的風貌](../Page/清談.md "wikilink")。

[唐朝修](../Page/唐朝.md "wikilink")《[晋书](../Page/晋书.md "wikilink")》多採《世说新语》，引用的312条约占《世说新语》条目的三成左右\[1\]。對此後世的史家常有批評\[2\]。

此外，《世說新語》善用對照、[比喻](../Page/比喻.md "wikilink")、誇張與描繪的文學技巧，不僅使它保留下許多膾炙人口的佳言名句，更為全書增添了無限光彩。如今，《世說新語》除了文學欣賞的價值外，人物事跡、文學典故等也多為後世作者所取材、引用，對後來的小說發展影響尤其大。《[唐语林](../Page/唐语林.md "wikilink")》、《[续世说](../Page/续世说.md "wikilink")》、《[何氏语林](../Page/何氏语林.md "wikilink")》、《[今世说](../Page/今世说.md "wikilink")》、《[明語林](../Page/明語林.md "wikilink")》等都是仿《世說新語》之作，稱之“世說體”。一說[晏殊刪並](../Page/晏殊.md "wikilink")《世說新語》\[3\]\[4\]。《世说新语》成书以后，[敬胤](../Page/敬胤.md "wikilink")、[刘孝标等人皆为之作注](../Page/刘孝标.md "wikilink")，今僅存孝标注\[5\]。

〈[王藍田食雞子](../Page/王藍田.md "wikilink")〉、〈[曹植](../Page/曹植.md "wikilink")[七步成詩](../Page/七步成詩.md "wikilink")〉、〈[晉明帝論日近遠](../Page/晉明帝.md "wikilink")〉、〈[管寧割席絕交](../Page/管寧.md "wikilink")〉、〈[華歆](../Page/華歆.md "wikilink")、[王朗乘船避難](../Page/王朗.md "wikilink")〉、〈[陳元方答客問](../Page/陳元方.md "wikilink")〉、〈[王導罵新亭對泣](../Page/王導.md "wikilink")〉、〈[石崇殺妓](../Page/石崇.md "wikilink")〉、〈[裴令公弔](../Page/裴楷.md "wikilink")[阮步兵](../Page/阮步兵.md "wikilink")〉、〈[王子猷訪](../Page/王子猷.md "wikilink")[戴安道](../Page/戴安道.md "wikilink")〉、〈[曹操](../Page/曹操.md "wikilink")、[楊脩過](../Page/楊脩.md "wikilink")[曹娥碑](../Page/曹娥碑.md "wikilink")〉、〈[曹操捉刀](../Page/曹操.md "wikilink")〉、〈[曹操望梅止渴](../Page/曹操.md "wikilink")〉、〈[王敦出婢妾](../Page/王敦.md "wikilink")〉、〈[支道林養鶴](../Page/支道林.md "wikilink")〉、〈[顧愷之倒食甘蔗](../Page/顧愷之.md "wikilink")〉、〈[嵇康臨刑索琴](../Page/嵇康.md "wikilink")〉、
〈[謝奕作剡令](../Page/謝奕.md "wikilink")〉、〈[謝安聽](../Page/謝安.md "wikilink")[阮裕](../Page/阮裕.md "wikilink")[白馬論](../Page/白馬論.md "wikilink")〉、〈[謝安](../Page/謝安.md "wikilink")、[王坦之赴宴](../Page/王坦之.md "wikilink")〉、〈[謝道韞詠絮](../Page/謝道韞.md "wikilink")〉、〈[謝玄北征不為名](../Page/謝玄.md "wikilink")〉、〈[謝萬石雅量](../Page/謝萬石.md "wikilink")〉、〈[劉伶禱神戒酒](../Page/劉伶.md "wikilink")〉、〈[王羲之坦腹東床](../Page/王羲之.md "wikilink")〉、〈[王子敬人琴俱亡](../Page/王子敬.md "wikilink")〉等篇曾收錄於[台灣中學階段各版本課本](../Page/台灣.md "wikilink")，或補充教材\[6\]\[7\]。今日有許多成語典故、漢晉文人軼事幾出自此，與《[詩經](../Page/詩經.md "wikilink")》、《[春秋左氏傳](../Page/春秋左氏傳.md "wikilink")》、《[戰國策](../Page/戰國策.md "wikilink")》等同樣被視為出現大量成語的文言文典籍。今日來看，亦可將《世說新語》視為古代短篇[小小說合體](../Page/小小說.md "wikilink")。

## 類目

  - 德行第一
  - 言語第二
  - 政事第三
  - 文學第四
  - 方正第五
  - 雅量第六
  - 識鑒第七
  - 賞譽第八
  - 品藻第九

<!-- end list -->

  - 規箴第十
  - 捷悟第十一
  - 夙慧第十二
  - 豪爽第十三
  - 容止第十四
  - 自新第十五
  - 企羡第十六
  - 傷逝第十七
  - 棲逸第十八

<!-- end list -->

  - 賢媛第十九
  - 術解第二十
  - 巧藝第二十一
  - 寵禮第二十二
  - 任誕第二十三
  - 簡傲第二十四
  - 排調第二十五
  - 輕詆第二十六
  - 假譎第二十七

<!-- end list -->

  - 黜免第二十八
  - 儉嗇第二十九
  - 汰侈第三十
  - 忿狷第三十一
  - 讒險第三十二
  - 尤悔第三十三
  - 紕漏第三十四
  - 惑溺第三十五
  - 仇隙第三十六

## 版本

  - 唐写本残卷
  - 南宋绍兴八年董弅刊本，原本存于日本
  - 南宋淳熙十五年陆游刻本
  - 淳熙十六年湘中刻本
  - [余嘉錫](../Page/余嘉錫.md "wikilink")：[世說新語箋疏](../Page/世說新語箋疏.md "wikilink")·

## 参考文献

## 外部連結

  - [《世说新语》全文在线阅读
    (简繁体白话翻译)](http://www.8bei8.com/book/shishuoxinyu.html)
  - [《世說新語》全文](http://ctext.org/shi-shuo-xin-yu/zh)
    ([繁體](http://ctext.org/shi-shuo-xin-yu/zh)/[簡體](http://ctext.org/shi-shuo-xin-yu/zhs))
    - [中国哲学书电子化计划](../Page/中国哲学书电子化计划.md "wikilink")
  - [世说新语全文](https://web.archive.org/web/20141009033743/http://www.njmuseum.com/zh/book/zzbj/shisuoxinyu/shishu.html)
  - 周一良：〈[关于《世说新语》的作者问题](http://www.nssd.org/articles/article_read.aspx?id=21181650)〉。

[Category:筆記小說](../Category/筆記小說.md "wikilink")
[Category:中國古典小說](../Category/中國古典小說.md "wikilink")
[Category:魏晉南北朝典籍](../Category/魏晉南北朝典籍.md "wikilink")
[Category:文言小說](../Category/文言小說.md "wikilink")
[Category:中國思想](../Category/中國思想.md "wikilink")
[Category:南京书籍](../Category/南京书籍.md "wikilink")

1.  [高淑清](../Page/高淑清.md "wikilink")《晋书取材世说新语之管见》
2.  [刘知几在](../Page/刘知几.md "wikilink")《[史通](../Page/史通.md "wikilink")·采撰篇》中指出：“晋世杂书，谅非一族，若《语林》、《世说》、《幽明录》、《搜神记》之徒，其所载或诙谐小辩，或神鬼怪物。其事非圣，扬雄所不观；其言乱神，宣尼所不语。皇朝新撰晋史，多采以为书。夫以干、邓之所粪除，王、虞之所糠秕，持为逸史，用补前传，此何异魏朝之撰《皇览》，梁世之修《遍略》，务多为美，聚博为功，虽取说于小人，终见嗤于君子矣。”《[旧唐书](../Page/旧唐书.md "wikilink")·房玄龄传》亦載：唐修《晋书》，“史官多是文咏之士，好采诡谬碎事，以广异闻；又所评论，竞为绮艳，不求笃实，由是颇为学者所讥。”[王应麟](../Page/王应麟.md "wikilink")《困学纪闻》卷十三亦云：“刘知几《史通》：‘《晋史》所采多小书，若《语林》、《世说》、《搜神记》、《幽明录》是也。曹（嘉之）、干（宝）两《纪》，孙（盛）、檀（道鸾）二《阳秋》，皆不之取。其中所载美事，遗略甚多。'又云：‘唐修《晋书》，作者皆词人，远弃史、班，近亲徐、庾。'[晁子止亦谓](../Page/晁子止.md "wikilink")：《晋史》丛冗最甚。”
3.  明[袁褧本](../Page/袁褧.md "wikilink")《世說新語》載南宋[董弅跋](../Page/董弅.md "wikilink")：“余家舊藏蓋得之王原叔家，後得晏元獻公手自校本，盡去重復，其註亦小加剪截，最為善本。”
4.  鲁迅《史略》：“今存者三卷曰《世说新语》，为宋人晏殊所删并，于注亦小有剪裁，然不知何人又加新语二字，唐时则曰新书，殆以《汉志》儒家类录刘向所序六十七篇中，已有《世说》，因增字以别之也。”
5.  敬胤注已佚，宋绍兴八年董弅刻本《世说新语》中附[汪藻](../Page/汪藻.md "wikilink")《世说叙录》中《考异》一卷有殘語。
6.  [翰林書局高中國文第一冊與補充教材](../Page/翰林書局.md "wikilink")，2014修訂
7.  南一書局出版國民中學國文第二冊，2009年初版，2011修訂