| **武汉经济协作区**                        |
| ---------------------------------- |
|                                    |
| **[长沙](../Page/长沙.md "wikilink")** |
| [常德](../Page/常德.md "wikilink")     |
| [鄂州](../Page/鄂州.md "wikilink")     |
| [黄冈](../Page/黄冈.md "wikilink")     |
| [黄石](../Page/黄石.md "wikilink")     |
| [荆门](../Page/荆门.md "wikilink")     |
| [荆州](../Page/荆州.md "wikilink")     |
| [景德镇](../Page/景德镇.md "wikilink")   |
| [九江](../Page/九江.md "wikilink")     |
| [南昌](../Page/南昌.md "wikilink")     |
| [南阳](../Page/南阳.md "wikilink")     |
| [萍乡](../Page/萍乡.md "wikilink")     |
| [潜江](../Page/潜江.md "wikilink")     |
| [上饶](../Page/上饶.md "wikilink")     |
| [十堰](../Page/十堰.md "wikilink")     |
| [石首](../Page/石首.md "wikilink")     |
| [天门](../Page/天门.md "wikilink")     |
| **[武汉](../Page/武汉.md "wikilink")** |
| [仙桃](../Page/仙桃.md "wikilink")     |
| [咸宁](../Page/咸宁.md "wikilink")     |
| [湘潭](../Page/湘潭.md "wikilink")     |
| [襄阳](../Page/襄阳.md "wikilink")     |
| [孝感](../Page/孝感.md "wikilink")     |
| [信阳](../Page/信阳.md "wikilink")     |
| [宜昌](../Page/宜昌.md "wikilink")     |
| [宜春](../Page/宜春.md "wikilink")     |
| [益阳](../Page/益阳.md "wikilink")     |
| [岳阳](../Page/岳阳.md "wikilink")     |
|                                    |
| [株洲](../Page/株洲.md "wikilink")     |
|                                    |

**武汉经济协作区**（長江中遊經濟圈）于1987年5月23日在[湖南](../Page/湖南.md "wikilink")[岳阳成立](../Page/岳阳.md "wikilink")，為中國大陸中部地區最大的[经济合作](../Page/经济.md "wikilink")[组织](../Page/组织.md "wikilink")，以武汉为中心，跨越[长江中游地区的](../Page/长江.md "wikilink")[湖南](../Page/湖南.md "wikilink")（湘）、[湖北](../Page/湖北.md "wikilink")（鄂）、[江西](../Page/江西.md "wikilink")（赣）三[省](../Page/省.md "wikilink")[武汉城市群](../Page/武汉城市群.md "wikilink")、[湘东北城市群和](../Page/湘东北城市群.md "wikilink")[赣北城市群构成的](../Page/赣北城市群.md "wikilink")[经济圈](../Page/经济圈.md "wikilink")。截止2005年11月市长联席会议，成员共计31个[城市](../Page/城市.md "wikilink")。其最高协调和决策机构为“市长联席会议”，每二年举行一次。\[1\]\[2\]

## 目标

突破以[行政区划](../Page/行政区划.md "wikilink")、单一地区或单一[城市发展规划](../Page/城市.md "wikilink")，建立区域内[产业分工体系](../Page/产业.md "wikilink")，促进城市间各个要素的融合、和城市间市场衔接等。以[省会城市为中心](../Page/省会.md "wikilink")，组建[城市群](../Page/城市群.md "wikilink")（城市圈）；协作区以城市群为基础，相向发展，形成大的经济圈。
未来[长江中游地区成为](../Page/长江.md "wikilink")[长江流域重要经济区](../Page/长江流域.md "wikilink")，成为[中国大陆继](../Page/中国大陆.md "wikilink")[长江三角洲](../Page/长江三角洲.md "wikilink")[经济圈](../Page/长江三角洲经济圈.md "wikilink")、[珠江三角洲](../Page/珠江三角洲.md "wikilink")[经济圈](../Page/珠江三角洲经济圈.md "wikilink")、环[渤海](../Page/渤海.md "wikilink")[经济圈之后第四个经济快速增长的地区](../Page/环渤海经济圈.md "wikilink")。\[3\]\[4\]

<table>
<caption><strong>武汉经济协作区各个<a href="../Page/城市群.md" title="wikilink">城市群</a></strong></caption>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Wuhan_Metropolitan_areas.png" title="fig:Wuhan_Metropolitan_areas.png">Wuhan_Metropolitan_areas.png</a><br />
</p>
<center>
<p>周边白色部分属于行政区划管辖范围<br />
中间白色部分表示未加入该组织</p>
</center></td>
</tr>
</tbody>
</table>

## 区域基本情况

武汉经济协作区面积近40万平方公里，人口约1亿5千万，31个城市政区2004年[GDP总量约](../Page/国内生产总值.md "wikilink")14000亿元。區內[铁路干线有南北的](../Page/铁路.md "wikilink")[京九线](../Page/京九铁路.md "wikilink")、[京广线](../Page/京广铁路.md "wikilink")、[焦柳线和东西走向的](../Page/焦柳铁路.md "wikilink")[宁西线](../Page/宁西铁路.md "wikilink")、[汉丹线](../Page/汉丹铁路.md "wikilink")、[襄渝线](../Page/襄渝铁路.md "wikilink")、[长荆线](../Page/长荆铁路.md "wikilink")、[武九线](../Page/武九铁路.md "wikilink")、[石长线](../Page/石长铁路.md "wikilink")、[浙赣线](../Page/浙赣铁路.md "wikilink")，[公路有南北走向的](../Page/公路.md "wikilink")[京珠高速](../Page/京珠高速.md "wikilink")、[105国道](../Page/105国道.md "wikilink")、[106国道](../Page/106国道.md "wikilink")、[107国道](../Page/107国道.md "wikilink")、[207国道和东西走向的](../Page/207国道.md "wikilink")[312国道](../Page/312国道.md "wikilink")、[316国道](../Page/316国道.md "wikilink")、[318国道](../Page/318国道.md "wikilink")、[320国道](../Page/320国道.md "wikilink")。

## 历史

1980年代中期，随着[中国大陆的](../Page/中国大陆.md "wikilink")[改革开放和](../Page/改革开放.md "wikilink")[经济发展走上正常轨道](../Page/中国经济.md "wikilink")。为顺应经济的发展，以重庆、武汉、南京三市市委书记共同发起，于1985年12月组建了长江沿岸中心城市经济协调会。自此长江流域地区自1985年至1987年，以[上海](../Page/上海.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、武汉、[南京四个城市为主体](../Page/南京.md "wikilink")，开始分别组建区域性经济协作会。\[5\]\[6\]

1985年，武汉、岳阳、黄石等城市发起，“武汉经济协作区”筹组工作启动，1987年5月23日经济协作区在岳阳成立。在此期间，[南京区域经济协调会](../Page/南京区域经济协调会.md "wikilink")、[重庆经济协作区和](../Page/重庆经济协作区.md "wikilink")[长江三角洲经济区分别成立](../Page/长江三角洲经济区.md "wikilink")。\[7\]\[8\]

## 历次市长联系会议

  - 第一次市长联席会议暨成立大会，1987年5月23日于湖南岳阳召开。
  - 第二次市长联席会议于1988年8月31日在[江西](../Page/江西.md "wikilink")[南昌召开](../Page/南昌.md "wikilink")；通过了《关于加强武汉经济协作区工作网络活动的意见》。
  - 第三次市长联席会议于1990年5月在[湖北](../Page/湖北.md "wikilink")[武汉召开](../Page/武汉.md "wikilink")；修定《协作区互惠原则》，通过《资源联合开发》、接纳《共同组建企业集团》等提案。
  - 第四次市长职席会议于1991年10月18日在湖北[荆州召开](../Page/荆州.md "wikilink")；原则通过《关于在区域内进一步开展科技成果推广的意见》。
  - 第五次市长联席会议于1994年9月16日在河南南阳召开；通过了《共建油料市场》、《组建企业家联谊会》、《金星计划》、《联建宜昌经协大厦》等提案。
  - 第七次市长联席会议于1997年6月10日在湖北孝感召开；提出建立利用好区域市场，开展跨地区资产重组，推进基础设施建设的提案。
  - 第八次市长联席会议于1999年5月18日在河南信阳召开；会议通过在信阳建立茶叶电子商务市场、建立华中旅游圈提案，修订武汉经济协作区互惠办法。
  - 第九次市长联席会议于2001年6月16日河南驻马店召开；通过《新世纪实现联合发展新跨越的指导意见》、《联办武汉经贸洽谈会》的提案。
  - 第十次市长联席会暨第四届中国竹文化节于2003年10月11日至12日在湖北咸宁召开；通过了新的武汉经济协作区章程；成员28个。
  - 第十一次市长联席会议于2005年11月18日至19日在湖南长沙召开，成员31个。

## 参考

## 参见

  - [武汉城市圈](../Page/武汉城市圈.md "wikilink")

## 外部链接

  - [武汉经协网](https://web.archive.org/web/20060304004003/http://www.whjx.gov.cn/)

[Category:湖北经济](../Category/湖北经济.md "wikilink")
[Category:江西经济](../Category/江西经济.md "wikilink")
[Category:湖南经济](../Category/湖南经济.md "wikilink")
[Category:中国中部经济区](../Category/中国中部经济区.md "wikilink")
[Category:中国经济合作区](../Category/中国经济合作区.md "wikilink")
[Category:长江流域](../Category/长江流域.md "wikilink")
[Category:武汉](../Category/武汉.md "wikilink")

1.

2.

3.
4.
5.
6.
7.
8.