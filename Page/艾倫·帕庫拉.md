**艾伦·杰伊·帕库拉**（，），[美国电影导演](../Page/美国.md "wikilink")、剧本作家、制作人，以对[阴谋片的贡献闻名](../Page/阴谋片.md "wikilink")。

## 作品

  - 1997年 《[致命突擊隊](../Page/致命突擊隊.md "wikilink")》（The Devil's Own）
  - 1993年 《[絕對機密](../Page/絕對機密.md "wikilink")》（The Pelican Brief）
  - 1992年 《[夜驚情](../Page/夜驚情.md "wikilink")》(Consenting Adults)
  - 1990年 《[無罪的證人](../Page/無罪的證人.md "wikilink")》（Presumed Innocent）
  - 1989年 《[異姓吸引力](../Page/異姓吸引力.md "wikilink")》(See You in the Morning)
  - 1987年 《[鎖匙少年](../Page/鎖匙少年.md "wikilink")》（Orphans）
  - 1986年 *[Dream Lover](../Page/Dream_Lover.md "wikilink")*
  - 1982年 《[蘇菲的選擇](../Page/蘇菲的選擇.md "wikilink")》（Sophie's Choice）
      - 提名[奧斯卡最佳改編劇本獎](../Page/奧斯卡最佳改編劇本獎.md "wikilink")
  - 1981年 《[金融大恐慌](../Page/金融大恐慌.md "wikilink")》（Rollover）
  - 1979年 《[不結婚的男人](../Page/不結婚的男人.md "wikilink")》（Starting Over）
  - 1978年 《[牧場風雲](../Page/牧場風雲.md "wikilink")》(Comes a Horseman)
  - 1976年 《[驚天大陰謀](../Page/驚天大陰謀.md "wikilink")》（All the President's
    Men）
      - 提名[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")
      - 提名[英國電影學院獎最佳導演](../Page/英國電影學院獎最佳導演.md "wikilink")
      - 提名[金球獎最佳導演](../Page/金球獎最佳導演.md "wikilink")
  - 1974年 《[視差景觀](../Page/視差景觀.md "wikilink")》（The Parallax View）
  - 1973年 *[Love and Pain and the Whole Damn
    Thing](../Page/Love_and_Pain_and_the_Whole_Damn_Thing.md "wikilink")*
  - 1971年 《[柳巷芳草](../Page/柳巷芳草.md "wikilink")》（Klute）
  - 1969年 《[何日卿再來](../Page/何日卿再來.md "wikilink")》（The Sterile Cuckoo）
  - 1962年 《[怪屋疑雲](../Page/怪屋疑雲.md "wikilink")》（To Kill a
    Mockingbird）(擔任製作人)
      - 提名[奧斯卡最佳影片獎](../Page/奧斯卡最佳影片獎.md "wikilink")

## 外部链接

  -
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:美国电影监制](../Category/美国电影监制.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:波兰裔美国人](../Category/波兰裔美国人.md "wikilink")
[Category:耶鲁大学校友](../Category/耶鲁大学校友.md "wikilink")
[Category:美國车祸身亡者](../Category/美國车祸身亡者.md "wikilink")
[Category:布朗克斯人](../Category/布朗克斯人.md "wikilink")