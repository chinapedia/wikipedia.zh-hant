**教学设计**是作为教者，基于对[学生和](../Page/学生.md "wikilink")[教学任务的分析](../Page/教学任务.md "wikilink")，而对教学目标、教学方法、教学材料、教学进度、课程评估等做出系统设计的一门学科。
教学设计者经常使用[教学技术以改进教学](../Page/教学技术.md "wikilink")。

## 历史

教学设计的诞生归功于[第二次世界大战](../Page/第二次世界大战.md "wikilink")，当时[美国需要快速培训大批人员完成复杂的技术任务](../Page/美国.md "wikilink")，征召了大批教育心理学家(其中包括加涅，Leslie
Briggs，Robert
Merril)去培训军人。战后，这些教育心理学家完成了包括一系列创新的分析、设计和评估程序在内的比较正式的教学系统。在商业和工业训练中首先采纳了战争期间成功的训练模式，后来又被引入中小学课堂。

## 有影响的研究者和理论家

  - [斯金纳](../Page/斯金纳.md "wikilink") - 行为主义 - 1940s-程序教学
  - [布卢姆](../Page/布卢姆.md "wikilink") - 认知、情感和心因动作技能分类学 - 1955
  - [马杰](../Page/马杰.md "wikilink") - 教学目标的ABCD模式 - 1962
  - [皮亚杰](../Page/皮亚杰.md "wikilink") - 认知发展 - 1960s
  - [西摩尔·派普特](../Page/西摩尔·派普特.md "wikilink") - Logo程序语言 - 1970s
  - [加涅](../Page/加涅.md "wikilink") - 教学过程九阶段 - 1970s
  - [布鲁纳](../Page/布鲁纳.md "wikilink") - [结构主义
    (教育学)](../Page/结构主义_\(教育学\).md "wikilink")
  - [迪克—凯瑞教学系统设计模型](../Page/迪克—凯瑞教学系统设计模型.md "wikilink") - 1978
  - [M. David Merrill和](../Page/M._David_Merrill.md "wikilink")[Charles
    Reigeluth](../Page/Charles_Reigeluth.md "wikilink") - Elaboration
    理论/Component Display Theory / PEAnets - 1980s
  - [Robert Heinich, Michael Molenda, James
    Russell](../Page/Robert_Heinich,_Michael_Molenda,_James_Russell.md "wikilink")
    - 教学媒介和新的教学技术*3rd ed.* - 教育技术 - 1989
  - [Roger Schank](../Page/Roger_Schank.md "wikilink") - 结构主义
    simulations - 1990s
  - [David Jonassen](../Page/David_Jonassen.md "wikilink") - Cognitivist
    问题解决策略 - 1990s

## 教学设计模型

最流行的教学模式可能是[ADDIE 模型](../Page/ADDIE_模型.md "wikilink")，该模型包括:

  - **分析**（Analysis） - 分析学习者特征和学习任务；
  - **设计**（Design） - 确定学习目标，选择教学途径
  - **开发**（Development） - 准备教学或[训练材料](../Page/训练材料.md "wikilink")
  - **执行**（Implementation） - 讲授或分配教学材料
  - **评估**（Evaluation）- 确信达到预定目标

教学理论在教学材料设计中扮演重要角色。[行为主义](../Page/行为主义.md "wikilink")、[结构主义](../Page/结构主义.md "wikilink")、[社会学习理论和](../Page/社会学习理论.md "wikilink")[认知主义
(心理学)等理论帮助形成和解释教学结果](../Page/认知主义_\(心理学\).md "wikilink")。

## 参见

教学设计与许多领域相关。

  - [assessment](../Page/assessment.md "wikilink")
  - [教育热诚](../Page/教育热诚.md "wikilink")
  - [教育心理学](../Page/教育心理学.md "wikilink")
  - [教育理论](../Page/教育理论.md "wikilink")
  - [e-学习](../Page/e-学习.md "wikilink")
  - [评估](../Page/评估.md "wikilink")
  - [教学技术](../Page/教学技术.md "wikilink")
  - [教学理论](../Page/教学理论.md "wikilink")
  - [学习科学](../Page/学习科学.md "wikilink")
  - [m-学习](../Page/m-学习.md "wikilink")
  - [在线教育](../Page/在线教育.md "wikilink")
  - [教学设计storyboarding](../Page/教学设计storyboarding.md "wikilink")
  - [training](../Page/training.md "wikilink")
  - [interdisciplinary
    teaching](../Page/interdisciplinary_teaching.md "wikilink")

## 外部链接

  - [ANGEL Learning LMS](http://www.angellearning.com) - an enabling LMS
    for instructional design
  - [WideOpenDoors.net](http://www.wideopendoors.net) - 教学设计资源
  - [IMS Global学习设计](http://www.imsglobal.org/learningdesign/index.html)
    - XML Specifications
  - [EServer TC
    图书馆:教学设计](https://web.archive.org/web/20070613192051/http://tc.eserver.org/dir/Instructional-Design)
  - [KitWiki](http://wiki.kitzzy.com/index.php/KitWiki:About) (class
    notes of a student taking an ID course)
  - [Creating an Instructor
    Kit](https://web.archive.org/web/20060208215839/http://www.williamrice.com/content/view/23/)
    (how-to article)

[Category:教育技术学](../Category/教育技术学.md "wikilink")
[Category:教育心理学](../Category/教育心理学.md "wikilink")
[Category:教育學](../Category/教育學.md "wikilink")
[Category:设计](../Category/设计.md "wikilink")