本列表统计的是**国家篮球协会（[NBA](../Page/NBA.md "wikilink")）球员个人职业生涯[常规赛籃板总和](../Page/NBA常规赛.md "wikilink")**，NBA球员在其它联赛（如[ABA](../Page/ABA.md "wikilink")）中的数据并不包括在内。所有数据都是截至到2017年12月16日（**[2017-18](../Page/2017-18_NBA賽季.md "wikilink")**）。
[Wilt_Chamberlain3.jpg](https://zh.wikipedia.org/wiki/File:Wilt_Chamberlain3.jpg "fig:Wilt_Chamberlain3.jpg")\]\]
[Bill_Russell_in_the_Green_Room.jpg](https://zh.wikipedia.org/wiki/File:Bill_Russell_in_the_Green_Room.jpg "fig:Bill_Russell_in_the_Green_Room.jpg")\]\]
[Kareem-Abdul-Jabbar_Lipofsky.jpg](https://zh.wikipedia.org/wiki/File:Kareem-Abdul-Jabbar_Lipofsky.jpg "fig:Kareem-Abdul-Jabbar_Lipofsky.jpg")\]\]
[Elvin_Hayes_1975_Bullets.jpg](https://zh.wikipedia.org/wiki/File:Elvin_Hayes_1975_Bullets.jpg "fig:Elvin_Hayes_1975_Bullets.jpg")\]\]
[Moses_Malone.jpg](https://zh.wikipedia.org/wiki/File:Moses_Malone.jpg "fig:Moses_Malone.jpg")\]\]

|    |              |
| -- | ------------ |
| ^  | 现役球员         |
| \* | 入选了奈史密斯篮球名人堂 |

<table>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>球员</p></th>
<th><p>场上位置</p></th>
<th><p>效力球队及赛季</p></th>
<th><p>进攻篮板</p></th>
<th><p>防守篮板</p></th>
<th><p>篮板总数[1]</p></th>
<th><p>平均籃板</p></th>
<th><p>入选[2]名人堂</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><strong><a href="../Page/威尔特·张伯伦.md" title="wikilink">威尔特·张伯伦</a></strong></p></td>
<td><p><a href="../Page/中锋.md" title="wikilink">中锋</a></p></td>
<td><p><a href="../Page/费城勇士.md" title="wikilink">费城勇士</a><br />
(<a href="../Page/1959-60_NBA赛季.md" title="wikilink">1959-60</a>--<a href="../Page/1964-65_NBA赛季.md" title="wikilink">1964-65</a>[3])<br />
<a href="../Page/费城76人.md" title="wikilink">费城76人</a><br />
（<a href="../Page/1964-65_NBA赛季.md" title="wikilink">1964-65</a>--<a href="../Page/1967-68_NBA赛季.md" title="wikilink">1967-68</a>）<br />
<a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a><br />
（<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>--<a href="../Page/1972-73_NBA赛季.md" title="wikilink">1972-73</a>）</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p><font color="#007FFF"><strong>23,924</strong></font></p></td>
<td><p><font color="#007FFF"><strong>22.89</strong></font></p></td>
<td><p>1979年</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><strong><a href="../Page/比尔·拉塞尔.md" title="wikilink">比尔·拉塞尔</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/波士顿塞爾提克.md" title="wikilink">波士顿塞爾提克</a><br />
（<a href="../Page/1956-57_NBA赛季.md" title="wikilink">1956-57</a>--<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>）</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p><font color="#007FFF"><strong>21,620</strong></font></p></td>
<td><p><font color="#007FFF"><strong>22.45</strong></font></p></td>
<td><p>1975年</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><strong><a href="../Page/卡里姆·阿卜杜勒·贾巴尔.md" title="wikilink">卡里姆·阿卜杜勒·贾巴尔</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/密尔沃基雄鹿.md" title="wikilink">密尔沃基雄鹿</a><br />
（<a href="../Page/1969-70_NBA赛季.md" title="wikilink">1969-70</a>--<a href="../Page/1974-75_NBA赛季.md" title="wikilink">1974-75</a>）<br />
<a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a><br />
（<a href="../Page/1975-76_NBA赛季.md" title="wikilink">1975-76</a>--<a href="../Page/1988-89_NBA赛季.md" title="wikilink">1988-89</a>）</p></td>
<td><p>2,975</p></td>
<td><p>9,394</p></td>
<td><p><font color="#007FFF"><strong>17,440</strong></font></p></td>
<td><p><font color="#007FFF"><strong>11.18</strong></font></p></td>
<td><p>1995年</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><strong><a href="../Page/埃尔文·海耶斯.md" title="wikilink">埃尔文·海耶斯</a></strong></p></td>
<td><p><a href="../Page/大前锋.md" title="wikilink">大前锋</a></p></td>
<td><p><a href="../Page/休斯顿火箭.md" title="wikilink">休斯顿火箭</a><br />
（<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>--<a href="../Page/1971-72_NBA赛季.md" title="wikilink">1971-72</a>[4]<br />
<a href="../Page/1981-82_NBA赛季.md" title="wikilink">1981-82</a>--<a href="../Page/1983-84_NBA赛季.md" title="wikilink">1983-84</a>）<br />
<a href="../Page/华盛顿子弹.md" title="wikilink">华盛顿子弹</a><br />
(<a href="../Page/1972-73_NBA赛季.md" title="wikilink">1972-73</a>--<a href="../Page/1980-81_NBA赛季.md" title="wikilink">1980-81</a>[5])</p></td>
<td><p>2,778</p></td>
<td><p>6,973</p></td>
<td><p><font color="#007FFF"><strong>16,279</strong></font></p></td>
<td><p><font color="#007FFF"><strong>12.49</strong></font></p></td>
<td><p>1990年</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><strong><a href="../Page/摩西·马龙.md" title="wikilink">摩西·马龙</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/洛杉矶快船.md" title="wikilink">布法罗勇士</a><br />
(<a href="../Page/1976-77_NBA赛季.md" title="wikilink">1976-77</a>[6])<br />
<a href="../Page/休斯顿火箭.md" title="wikilink">休斯顿火箭</a><br />
（<a href="../Page/1976-77_NBA赛季.md" title="wikilink">1976-77</a>--<a href="../Page/1981-82_NBA赛季.md" title="wikilink">1981-82</a>）<br />
<a href="../Page/费城76人.md" title="wikilink">费城76人</a><br />
（<a href="../Page/1982-83_NBA赛季.md" title="wikilink">1982-83</a>--<a href="../Page/1985-86_NBA赛季.md" title="wikilink">1985-86</a>，<br />
<a href="../Page/1993-94_NBA赛季.md" title="wikilink">1993-94</a>）<br />
<a href="../Page/华盛顿子弹.md" title="wikilink">华盛顿子弹</a><br />
（<a href="../Page/1986-87_NBA赛季.md" title="wikilink">1986-87</a>--<a href="../Page/1987-88_NBA赛季.md" title="wikilink">1987-88</a>）<br />
<a href="../Page/亚特兰大鹰.md" title="wikilink">亚特兰大鹰</a><br />
（<a href="../Page/1989-90_NBA赛季.md" title="wikilink">1989-90</a>--<a href="../Page/1990-91_NBA赛季.md" title="wikilink">1990-91</a>）<br />
<a href="../Page/密尔沃基雄鹿.md" title="wikilink">密尔沃基雄鹿</a><br />
（<a href="../Page/1991-92_NBA赛季.md" title="wikilink">1991-92</a>--<a href="../Page/1992-93_NBA赛季.md" title="wikilink">1992-93</a>）<br />
<a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a><br />
（<a href="../Page/1994-95_NBA赛季.md" title="wikilink">1994-95</a>）</p></td>
<td><p>6,731</p></td>
<td><p>9,481</p></td>
<td><p><font color="#007FFF"><strong>16,212</strong></font></p></td>
<td><p><font color="#007FFF"><strong>12.20</strong></font></p></td>
<td><p>2001年</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><strong><a href="../Page/蒂姆·邓肯.md" title="wikilink">蒂姆·邓肯</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a><br />
(<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>--<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>)</p></td>
<td><p>3,859</p></td>
<td><p>11,232</p></td>
<td><p><font color="#007FFF"><strong>15,091</strong></font></p></td>
<td><p><font color="#007FFF"><strong>10.9</strong></font></p></td>
<td><p>|-</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><strong><a href="../Page/卡尔·马龙.md" title="wikilink">卡尔·马龙</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/犹他爵士.md" title="wikilink">犹他爵士</a><br />
（<a href="../Page/1985-86_NBA赛季.md" title="wikilink">1985-86</a>--<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a><br />
（<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a>）</p></td>
<td><p>3,562</p></td>
<td><p>11,406</p></td>
<td><p><font color="#007FFF"><strong>14,968</strong></font></p></td>
<td><p><font color="#007FFF"><strong>10.14</strong></font></p></td>
<td><p>2010年</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><strong><a href="../Page/罗伯特·帕里什.md" title="wikilink">罗伯特·帕里什</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a><br />
（<a href="../Page/1976-77_NBA赛季.md" title="wikilink">1976-77</a>--<a href="../Page/1979-80_NBA赛季.md" title="wikilink">1979-80</a>）<br />
<a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a><br />
（<a href="../Page/1980-81_NBA赛季.md" title="wikilink">1980-81</a>--<a href="../Page/1992-93_NBA赛季.md" title="wikilink">1992-93</a>）<br />
<a href="../Page/夏洛特黄蜂.md" title="wikilink">夏洛特黄蜂</a><br />
（<a href="../Page/1994-95_NBA赛季.md" title="wikilink">1994-95</a>--<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
（<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>）</p></td>
<td><p>4,598</p></td>
<td><p>10,117</p></td>
<td><p><font color="#007FFF"><strong>14,715</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.13</strong></font></p></td>
<td><p>2003年</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/凯文·加内特.md" title="wikilink">凯文·加内特</a></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/明尼苏达森林狼.md" title="wikilink">明尼苏达森林狼</a><br />
(<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>--<a href="../Page/2006-07_NBA赛季.md" title="wikilink">2006-07</a>)<br />
<a href="../Page/波士頓凱爾特人.md" title="wikilink">波士頓凱爾特人</a><br />
(<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>--<a href="../Page/2011-12_NBA赛季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/布魯克林籃網.md" title="wikilink">布魯克林籃網</a><br />
（<a href="../Page/2013-14_NBA賽季.md" title="wikilink">2013-14</a>--<a href="../Page/2014-15_NBA賽季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a><br />
（<a href="../Page/2014-15_NBA賽季.md" title="wikilink">2014-15</a>--<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>)</p></td>
<td><p>3,209</p></td>
<td><p>11,453</p></td>
<td><p><font color="#007FFF"><strong>14,662</strong></font></p></td>
<td><p><font color="#007FFF"><strong>10.03</strong></font></p></td>
<td><p>|-</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><strong><a href="../Page/内特·瑟蒙德.md" title="wikilink">内特·瑟蒙德</a></strong></p></td>
<td><p>中锋<br />
<br />
大前锋</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a><br />
(<a href="../Page/1963-64_NBA赛季.md" title="wikilink">1963-64</a>--<a href="../Page/1973-74_NBA赛季.md" title="wikilink">1973-74</a>[7])<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
(<a href="../Page/1974-75_NBA赛季.md" title="wikilink">1974-75</a>--<a href="../Page/1975-76_NBA赛季.md" title="wikilink">1973-74</a>[8])<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a><br />
（<a href="../Page/1975-76_NBA赛季.md" title="wikilink">1975-76</a>--<a href="../Page/1976-77_NBA赛季.md" title="wikilink">1976-77</a>）</p></td>
<td><p>744</p></td>
<td><p>1,827</p></td>
<td><p><font color="#007FFF"><strong>14,464</strong></font></p></td>
<td><p><font color="#007FFF"><strong>15.00</strong></font></p></td>
<td><p>1985年</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><strong><a href="../Page/沃尔特·贝拉米.md" title="wikilink">沃尔特·贝拉米</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/巴尔的摩子弹.md" title="wikilink">巴尔的摩子弹</a><br />
(<a href="../Page/1961-62_NBA赛季.md" title="wikilink">1961-62</a>--<a href="../Page/1965-66_NBA赛季.md" title="wikilink">1965-66</a>[9])<br />
<a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a><br />
(<a href="../Page/1965-66_NBA赛季.md" title="wikilink">1965-66</a>--<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>[10])<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a><br />
(<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>--<a href="../Page/1969-70_NBA赛季.md" title="wikilink">1969-70</a>[11])<br />
<a href="../Page/亚特兰大鹰.md" title="wikilink">亚特兰大鹰</a><br />
（<a href="../Page/1969-70_NBA赛季.md" title="wikilink">1969-70</a>--<a href="../Page/1973-74_NBA赛季.md" title="wikilink">1973-74</a>）<br />
<a href="../Page/犹他爵士.md" title="wikilink">新奥尔良爵士</a><br />
（<a href="../Page/1974-75_NBA赛季.md" title="wikilink">1974-75</a>）</p></td>
<td><p>264</p></td>
<td><p>481</p></td>
<td><p><font color="#007FFF"><strong>14,241</strong></font></p></td>
<td><p><font color="#007FFF"><strong>13.65</strong></font></p></td>
<td><p>1993年</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><strong><a href="../Page/韦斯·昂塞尔德.md" title="wikilink">韦斯·昂塞尔德</a></strong></p></td>
<td><p>中锋<br />
<br />
大前锋</p></td>
<td><p><a href="../Page/华盛顿子弹.md" title="wikilink">华盛顿子弹</a><br />
(<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>--<a href="../Page/1972-73_NBA赛季.md" title="wikilink">1972-73</a>[12])</p></td>
<td><p>2,085</p></td>
<td><p>4,974</p></td>
<td><p><font color="#007FFF"><strong>13,769</strong></font></p></td>
<td><p><font color="#007FFF"><strong>13.99</strong></font></p></td>
<td><p>1988年</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><strong><a href="../Page/哈基姆·奥拉朱旺.md" title="wikilink">哈基姆·奥拉朱旺</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/休斯顿火箭.md" title="wikilink">休斯顿火箭</a><br />
（<a href="../Page/1984-85_NBA赛季.md" title="wikilink">1984-85</a>--<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/多伦多猛龙.md" title="wikilink">多伦多猛龙</a><br />
（<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>）</p></td>
<td><p>4,034</p></td>
<td><p>9,714</p></td>
<td><p><font color="#007FFF"><strong>13,748</strong></font></p></td>
<td><p><font color="#007FFF"><strong>11.10</strong></font></p></td>
<td><p>2008年</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><strong><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a><br />
(<a href="../Page/1992-93_NBA赛季.md" title="wikilink">1992-93</a>--<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>)<br />
<a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a><br />
(<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a>)<br />
<a href="../Page/迈阿密热火.md" title="wikilink">迈阿密热火</a><br />
(<a href="../Page/2004-05_NBA赛季.md" title="wikilink">2004-05</a>--<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>[13])<br />
<a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a><br />
(<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>--<a href="../Page/2008-09_NBA赛季.md" title="wikilink">2008-09</a>)<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a><br />
(<a href="../Page/2009-10_NBA赛季.md" title="wikilink">2009-10</a>)<br />
<a href="../Page/波士頓凱爾特人.md" title="wikilink">波士頓凱爾特人</a><br />
(<a href="../Page/2010-11_NBA赛季.md" title="wikilink">2010-11</a>)</p></td>
<td><p>4,209</p></td>
<td><p>8,890</p></td>
<td><p><font color="#007FFF"><strong>13,099</strong></font></p></td>
<td><p><font color="#007FFF"><strong>10.85</strong></font></p></td>
<td><p>2016年</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><strong><a href="../Page/巴克·威廉姆斯.md" title="wikilink">巴克·威廉姆斯</a></strong></p></td>
<td><p>大前锋<br />
<br />
中锋</p></td>
<td><p><a href="../Page/新澤西籃網.md" title="wikilink">新澤西籃網</a><br />
（<a href="../Page/1981-82_NBA赛季.md" title="wikilink">1981-82</a>--<a href="../Page/1988-89_NBA赛季.md" title="wikilink">1988-89</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a><br />
（<a href="../Page/1989-90_NBA赛季.md" title="wikilink">1989-90</a>--<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a><br />
（<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>）</p></td>
<td><p>4,526</p></td>
<td><p>8,491</p></td>
<td><p><font color="#007FFF"><strong>13,017</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.96</strong></font></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/杰里·卢卡斯.md" title="wikilink">杰里·卢卡斯</a></p></td>
<td><p>大前锋<br />
<br />
中锋</p></td>
<td><p><a href="../Page/薩克拉門托國王.md" title="wikilink">辛辛那提皇家</a><br />
(<a href="../Page/1963-64_NBA赛季.md" title="wikilink">1963-64</a>--<a href="../Page/1969-70_NBA赛季.md" title="wikilink">1969-70</a>[14])<br />
<a href="../Page/金州勇士.md" title="wikilink">三藩市勇士</a><br />
（<a href="../Page/1969-70_NBA赛季.md" title="wikilink">1969-70</a>--<a href="../Page/1970-71_NBA赛季.md" title="wikilink">1970-71</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a><br />
（<a href="../Page/1971-72_NBA赛季.md" title="wikilink">1971-72</a>--<a href="../Page/1973-74_NBA赛季.md" title="wikilink">1973-74</a>）</p></td>
<td><p>62</p></td>
<td><p>312</p></td>
<td><p><font color="#007FFF"><strong>12,942</strong></font></p></td>
<td><p><font color="#007FFF"><strong>15.61</strong></font></p></td>
<td><p>1980年</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><strong><a href="../Page/鲍勃·佩蒂特.md" title="wikilink">鲍勃·佩蒂特</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/圣路易斯老鹰.md" title="wikilink">圣路易斯老鹰</a><br />
(<a href="../Page/1954-55_NBA赛季.md" title="wikilink">1954-55</a>--<a href="../Page/1964-65_NBA赛季.md" title="wikilink">1964-65</a>[15])</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p><font color="#007FFF"><strong>12,849</strong></font></p></td>
<td><p><font color="#007FFF"><strong>16.22</strong></font></p></td>
<td><p>1971年</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><strong><a href="../Page/查尔斯·巴克利.md" title="wikilink">查尔斯·巴克利</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/费城76人.md" title="wikilink">费城76人</a><br />
（<a href="../Page/1984-85_NBA赛季.md" title="wikilink">1984-85</a>--<a href="../Page/1991-92_NBA赛季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a><br />
（<a href="../Page/1992-93_NBA赛季.md" title="wikilink">1992-93</a>--<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/休斯顿火箭.md" title="wikilink">休斯顿火箭</a><br />
（<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/1999-00_NBA赛季.md" title="wikilink">1999-00</a>）</p></td>
<td><p>4,260</p></td>
<td><p>8,286</p></td>
<td><p><font color="#007FFF"><strong>12,546</strong></font></p></td>
<td><p><font color="#007FFF"><strong>11.69</strong></font></p></td>
<td><p>2006年</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p><strong><a href="../Page/德怀特·霍华德.md" title="wikilink">德怀特·霍华德</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a><br />
(<a href="../Page/2004-05_NBA赛季.md" title="wikilink">2004-05</a>--<a href="../Page/2011-12_NBA赛季.md" title="wikilink">2011-12</a>)<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a><br />
(<a href="../Page/2012-13_NBA赛季.md" title="wikilink">2012-13</a>)<br />
<a href="../Page/休斯頓火箭.md" title="wikilink">休斯頓火箭</a><br />
（<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>—2015-16）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a><br />
(<a href="../Page/2016-17_NBA赛季.md" title="wikilink">2016-17</a>--)</p></td>
<td><p>3,492</p></td>
<td><p>8,949</p></td>
<td><p><font color="#007FFF"><strong>12,441</strong></font></p></td>
<td><p><font color="#007FFF"><strong>12.7</strong></font></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p><strong><a href="../Page/迪肯贝·穆托姆博.md" title="wikilink">迪肯贝·穆托姆博</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a><br />
（<a href="../Page/1991-92_NBA赛季.md" title="wikilink">1991-92</a>--<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/亞特蘭大老鹰.md" title="wikilink">亞特蘭大老鹰</a><br />
(<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>[16])<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a><br />
（<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>--<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a><br />
（<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a><br />
(<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a><br />
<a href="../Page/休斯頓火箭.md" title="wikilink">休斯頓火箭</a>）<br />
（<a href="../Page/2004-05_NBA赛季.md" title="wikilink">2004-05</a>--<a href="../Page/2008-09_NBA赛季.md" title="wikilink">2008-09</a>）</p></td>
<td><p>3,808</p></td>
<td><p>8,551</p></td>
<td><p><font color="#007FFF"><strong>12,359</strong></font></p></td>
<td><p><font color="#007FFF"><strong>10.33</strong></font></p></td>
<td><p>2015年</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><strong><a href="../Page/保罗·西拉斯.md" title="wikilink">保罗·西拉斯</a></strong></p></td>
<td><p>大前锋<br />
<br />
中锋</p></td>
<td><p><a href="../Page/亞特蘭大老鹰.md" title="wikilink">亞特蘭大老鹰</a><br />
(<a href="../Page/1964-65_NBA赛季.md" title="wikilink">1964-65</a>--<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>[17])<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a><br />
（<a href="../Page/1969-70_NBA赛季.md" title="wikilink">1969-70</a>--<a href="../Page/1971-72_NBA赛季.md" title="wikilink">1971-72</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a><br />
（<a href="../Page/1972-73_NBA赛季.md" title="wikilink">1972-73</a>--<a href="../Page/1975-76_NBA赛季.md" title="wikilink">1975-76</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a><br />
（<a href="../Page/1976-77_NBA赛季.md" title="wikilink">1976-77</a>）<br />
<a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a><br />
（<a href="../Page/1977-78_NBA赛季.md" title="wikilink">1977-78</a>--<a href="../Page/1979-80_NBA赛季.md" title="wikilink">1979-80</a>）</p></td>
<td><p>2,035</p></td>
<td><p>3,213</p></td>
<td><p><font color="#007FFF"><strong>12,357</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.85</strong></font></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><strong><a href="../Page/查尔斯·奥克利.md" title="wikilink">查尔斯·奥克利</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
（<a href="../Page/1985-86_NBA赛季.md" title="wikilink">1985-86</a>--<a href="../Page/1987-88_NBA赛季.md" title="wikilink">1987-88</a>，<br />
<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a><br />
（<a href="../Page/1988-89_NBA赛季.md" title="wikilink">1988-89</a>--<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a><br />
（<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>--<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/華盛頓奇才.md" title="wikilink">华盛顿奇才</a><br />
（<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a><br />
（<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a>）</p></td>
<td><p>3,924</p></td>
<td><p>8,281</p></td>
<td><p><font color="#007FFF"><strong>12,205</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.52</strong></font></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p><strong><a href="../Page/丹尼斯·罗德曼.md" title="wikilink">丹尼斯·罗德曼</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a><br />
（<a href="../Page/1986-87_NBA赛季.md" title="wikilink">1986-87</a>--<a href="../Page/1992-93_NBA赛季.md" title="wikilink">1992-93</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a><br />
（<a href="../Page/1993-94_NBA赛季.md" title="wikilink">1993-94</a>--<a href="../Page/1994-95_NBA赛季.md" title="wikilink">1994-95</a>）<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
（<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>--<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a><br />
（<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a><br />
（<a href="../Page/1999-2000_NBA赛季.md" title="wikilink">1999-00</a>）</p></td>
<td><p>4,329</p></td>
<td><p>7,625</p></td>
<td><p><font color="#007FFF"><strong>11,954</strong></font></p></td>
<td><p><font color="#007FFF"><strong>13.12</strong></font></p></td>
<td><p>2011年</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p><strong><a href="../Page/凯文·威利斯.md" title="wikilink">凯文·威利斯</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a><br />
（<a href="../Page/1984-85_NBA赛季.md" title="wikilink">1984-85</a>--<a href="../Page/1994-95_NBA赛季.md" title="wikilink">1994-95</a>[18],<br />
<a href="../Page/2004-05_NBA赛季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a><br />
(<a href="../Page/1994-95_NBA赛季.md" title="wikilink">1994-95</a>--<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>[19])<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a><br />
（<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a><br />
（<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>，<br />
<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a><br />
(<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>--<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>[20])<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a><br />
（<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a><br />
（<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>--<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a><br />
（<a href="../Page/2006-07_NBA赛季.md" title="wikilink">2006-07</a>）</p></td>
<td><p>4,132</p></td>
<td><p>7,769</p></td>
<td><p><font color="#007FFF"><strong>11,901</strong></font></p></td>
<td><p><font color="#007FFF"><strong>8.36</strong></font></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p><strong><a href="../Page/帕特里克·尤因.md" title="wikilink">帕特里克·尤因</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a><br />
（<a href="../Page/1985-86_NBA赛季.md" title="wikilink">1985-86</a>--<a href="../Page/1999-00_NBA赛季.md" title="wikilink">1999-00</a>）<br />
<a href="../Page/西雅图超音速.md" title="wikilink">西雅图超音速</a><br />
（<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a><br />
（<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>）</p></td>
<td><p>2,752</p></td>
<td><p>8,855</p></td>
<td><p><font color="#007FFF"><strong>11,607</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.81</strong></font></p></td>
<td><p>2008年</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p><strong><a href="../Page/埃尔金·贝勒.md" title="wikilink">埃尔金·贝勒</a></strong></p></td>
<td><p>大前锋<br />
小前锋</p></td>
<td><p><a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a><br />
(<a href="../Page/1958-59_NBA赛季.md" title="wikilink">1958-59</a>--<a href="../Page/1971-72_NBA赛季.md" title="wikilink">1971-72</a>[21])</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p><font color="#007FFF"><strong>11,463</strong></font></p></td>
<td><p><font color="#007FFF"><strong>13.55</strong></font></p></td>
<td><p>1977年</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p><strong><a href="../Page/多尔夫·谢伊斯.md" title="wikilink">多尔夫·谢伊斯</a></strong></p></td>
<td><p>前锋</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a><br />
(<a href="../Page/1949-50_NBA赛季.md" title="wikilink">1949-50</a>--<a href="../Page/1963-64_NBA赛季.md" title="wikilink">1963-64</a>[22])</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p><font color="#007FFF"><strong>11,256</strong></font></p></td>
<td><p><font color="#007FFF"><strong>12.08</strong></font></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p><strong><a href="../Page/德克·诺维茨基.md" title="wikilink">德克·诺维茨基</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a><br />
（<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>--）</p></td>
<td><p>1,453</p></td>
<td><p>9,605</p></td>
<td><p><font color="#007FFF"><strong>11,058</strong></p></td>
<td><p>7.87</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p><strong><a href="../Page/比尔·布里吉斯.md" title="wikilink">比尔·布里吉斯</a></strong></p></td>
<td><p>前锋</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a><br />
(<a href="../Page/1962-63_NBA赛季.md" title="wikilink">1962-63</a>--<a href="../Page/1971-72_NBA赛季.md" title="wikilink">1971-72</a>[23])<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a><br />
(<a href="../Page/1971-72_NBA赛季.md" title="wikilink">1971-72</a>--<a href="../Page/1972-73_NBA赛季.md" title="wikilink">1972-73</a>[24])<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a><br />
(<a href="../Page/1972-73_NBA赛季.md" title="wikilink">1972-73</a>--<a href="../Page/1974-75_NBA赛季.md" title="wikilink">1974-75</a>[25])<br />
<a href="../Page/金州勇士.md" title="wikilink">-{金州勇士}-</a><br />
（<a href="../Page/1974-75_NBA赛季.md" title="wikilink">1974-75</a>）</p></td>
<td><p>257</p></td>
<td><p>376</p></td>
<td><p><font color="#007FFF"><strong>11,054</strong></font></p></td>
<td><p><font color="#007FFF"><strong>11.94</strong></font></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p><strong><a href="../Page/杰克·西克马.md" title="wikilink">杰克·西克马</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a><br />
（<a href="../Page/1977-78_NBA赛季.md" title="wikilink">1977-78</a>--<a href="../Page/1985-86_NBA赛季.md" title="wikilink">1985-86</a>）<br />
<a href="../Page/密爾沃基雄鹿.md" title="wikilink">密爾沃基雄鹿</a><br />
（<a href="../Page/1986-87_NBA赛季.md" title="wikilink">1986-87</a>--<a href="../Page/1990-91_NBA赛季.md" title="wikilink">1990-91</a>）</p></td>
<td><p>2,542</p></td>
<td><p>8,274</p></td>
<td><p><font color="#007FFF"><strong>10,816</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.77</strong></font></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td><p><a href="../Page/保罗·加索尔.md" title="wikilink">保罗·加索尔</a></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/曼菲斯灰熊.md" title="wikilink">曼菲斯灰熊</a><br />
（<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>--<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>)<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a><br />
(<a href="../Page/2008-09_NBA赛季.md" title="wikilink">2008-09</a>--<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>)<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
(<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>--<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>)<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a><br />
(<a href="../Page/2016-17_NBA赛季.md" title="wikilink">2016-17</a>--)</p></td>
<td><p>2,973</p></td>
<td><p>7,792</p></td>
<td><p><font color="#007FFF"><strong>10,765</strong></p></td>
<td><p>9.52</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p><strong><a href="../Page/大卫·罗宾逊.md" title="wikilink">大卫·罗宾逊</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a><br />
（<a href="../Page/1989-90_NBA赛季.md" title="wikilink">1989-90</a>--<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>3,083</p></td>
<td><p>7,414</p></td>
<td><p><font color="#007FFF"><strong>10,497</strong></font></p></td>
<td><p><font color="#007FFF"><strong>10.64</strong></font></p></td>
<td><p>2009年</p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td><p><strong><a href="../Page/本·华莱士.md" title="wikilink">本·华莱士</a></strong></p></td>
<td><p>大前锋<br />
<br />
中锋</p></td>
<td><p><a href="../Page/華盛頓奇才.md" title="wikilink">華盛頓奇才</a><br />
(<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>)<br />
<a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a><br />
(<a href="../Page/1999-2000_NBA赛季.md" title="wikilink">1999-2000</a>)<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a><br />
(<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>--<a href="../Page/2005-06_NBA赛季.md" title="wikilink">2005-06</a>)<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
(<a href="../Page/2006-07_NBA赛季.md" title="wikilink">2006-07</a>--<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>)<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克-{里}-夫蘭騎士</a><br />
(<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>--<a href="../Page/2008-09_NBA赛季.md" title="wikilink">2008-09</a>)<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a><br />
(<a href="../Page/2009-10_NBA赛季.md" title="wikilink">2009-10</a>--<a href="../Page/2011-12_NBA赛季.md" title="wikilink">2011-12</a>)</p></td>
<td><p>3,444</p></td>
<td><p>7,038</p></td>
<td><p><font color="#007FFF"><strong>10,482</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.63</strong></font></p></td>
<td><p>|-</p></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td><p><strong><a href="../Page/大卫·考温斯.md" title="wikilink">大卫·考温斯</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/波士顿塞爾提克.md" title="wikilink">波士顿塞爾提克</a><br />
（<a href="../Page/1970-71_NBA赛季.md" title="wikilink">1970-71</a>--<a href="../Page/1979-80_NBA赛季.md" title="wikilink">1979-80</a>）<br />
<a href="../Page/密爾沃基公鹿.md" title="wikilink">密爾沃基公鹿</a><br />
（<a href="../Page/1982-83_NBA赛季.md" title="wikilink">1982-83</a>）</p></td>
<td><p>1,574</p></td>
<td><p>5,122</p></td>
<td><p><font color="#007FFF"><strong>10,444</strong></font></p></td>
<td><p><font color="#007FFF"><strong>13.63</strong></font></p></td>
<td><p>1990年</p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td><p><strong><a href="../Page/比尔·兰比尔.md" title="wikilink">比尔·兰比尔</a></strong></p></td>
<td><p>中锋<br />
</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克-{里}-夫蘭騎士</a><br />
(<a href="../Page/1980-81_NBA赛季.md" title="wikilink">1980-81</a>--<a href="../Page/1981-82_NBA赛季.md" title="wikilink">1981-82</a>[26])<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a><br />
（<a href="../Page/1981-82_NBA赛季.md" title="wikilink">1981-82</a>--<a href="../Page/1993-94_NBA赛季.md" title="wikilink">1993-94</a>）</p></td>
<td><p>2,819</p></td>
<td><p>7,581</p></td>
<td><p><font color="#007FFF"><strong>10,400</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.74</strong></font></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td><p><strong><a href="../Page/奥蒂斯·索普.md" title="wikilink">奥蒂斯·索普</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/薩克拉門托國王.md" title="wikilink">薩克拉門托國王</a><br />
（<a href="../Page/1984-85_NBA赛季.md" title="wikilink">1984-85</a>--<a href="../Page/1987-88_NBA赛季.md" title="wikilink">1987-88</a>[27],<br />
<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a><br />
(<a href="../Page/1988-89_NBA赛季.md" title="wikilink">1988-89</a>--<a href="../Page/1994-95_NBA赛季.md" title="wikilink">1994-95</a>[28])<br />
<a href="../Page/波特蘭开拓者.md" title="wikilink">波特蘭开拓者</a><br />
（<a href="../Page/1994-95_NBA赛季.md" title="wikilink">1994-95</a>）<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a><br />
（<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>--<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>）<br />
<a href="../Page/溫哥華灰熊.md" title="wikilink">溫哥華灰熊</a><br />
(<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>[29])<br />
<a href="../Page/華盛頓奇才.md" title="wikilink">華盛頓奇才</a><br />
（<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a><br />
（<a href="../Page/1999-2000_NBA赛季.md" title="wikilink">1999-2000</a>）<br />
<a href="../Page/夏洛特黃蜂.md" title="wikilink">夏洛特黃蜂</a><br />
（<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>）</p></td>
<td><p>3,446</p></td>
<td><p>6,924</p></td>
<td><p><font color="#007FFF"><strong>10,370</strong></font></p></td>
<td><p><font color="#007FFF"><strong>8.25</strong></font></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td><p><a href="../Page/肖恩·马里昂.md" title="wikilink">肖恩·马里昂</a></p></td>
<td><p>小前鋒、大前鋒</p></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a><br />
（<a href="../Page/1999-00_NBA赛季.md" title="wikilink">1999-00</a>--<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>)<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a><br />
(<a href="../Page/2008-09_NBA赛季.md" title="wikilink">2008-09</a>)<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a><br />
(<a href="../Page/2009_NBA赛季.md" title="wikilink">2009</a>)<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a><br />
(<a href="../Page/2009-10_NBA赛季.md" title="wikilink">2009-10</a>--<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>)<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a><br />
(<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>)</p></td>
<td><p>2,732</p></td>
<td><p>7,369</p></td>
<td><p><font color="#007FFF"><strong>10,101</strong></p></td>
<td><p>8.69</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td><p><strong><a href="../Page/约翰尼·科尔.md" title="wikilink">约翰尼·科尔</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a><br />
(<a href="../Page/1954-55_NBA赛季.md" title="wikilink">1954-55</a>--<a href="../Page/1964-65_NBA赛季.md" title="wikilink">1964-65</a>[30])<br />
<a href="../Page/巴爾的摩子彈.md" title="wikilink">巴爾的摩子彈</a><br />
（<a href="../Page/1965-66_NBA赛季.md" title="wikilink">1965-66</a>）</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p><font color="#007FFF"><strong>10,092</strong></font></p></td>
<td><p><font color="#007FFF"><strong>11.15</strong></font></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p><a href="../Page/扎克·兰多夫.md" title="wikilink">扎克·兰多夫</a></p></td>
<td><p>大前鋒</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a><br />
（<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>--<a href="../Page/2006-07_NBA赛季.md" title="wikilink">2006-07</a>)<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a><br />
（<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a><br />
（<a href="../Page/2008-09_NBA赛季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/曼菲斯灰熊.md" title="wikilink">曼菲斯灰熊</a><br />
（<a href="../Page/2009-10_NBA赛季.md" title="wikilink">2009-10</a>--）</p></td>
<td><p>3,228</p></td>
<td><p>6,766</p></td>
<td><p><font color="#007FFF"><strong>9,994</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p><a href="../Page/泰森·钱德勒.md" title="wikilink">泰森·钱德勒</a></p></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
（<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>--<a href="../Page/2005-06_NBA赛季.md" title="wikilink">2005-06</a>)<br />
<a href="../Page/紐奧良黃蜂.md" title="wikilink">紐奧良黃蜂</a><br />
（<a href="../Page/2006-07_NBA赛季.md" title="wikilink">2006-07</a>--<a href="../Page/2008-09_NBA赛季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/夏洛特山貓.md" title="wikilink">夏洛特山貓</a><br />
（<a href="../Page/2009-10_NBA赛季.md" title="wikilink">2009-10</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a><br />
（<a href="../Page/2010-11_NBA赛季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a><br />
（<a href="../Page/2011-12_NBA赛季.md" title="wikilink">2011-12</a>--<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a><br />
（<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>--<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a><br />
（<a href="../Page/2016-17_NBA赛季.md" title="wikilink">2016-17</a>--）</p></td>
<td><p>3,340</p></td>
<td><p>6,539</p></td>
<td><p><font color="#007FFF"><strong>9,879</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td><p><strong><a href="../Page/鲍伯·兰尼尔.md" title="wikilink">鲍伯·兰尼尔</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/波士顿塞爾提克.md" title="wikilink">波士顿塞爾提克</a><br />
(<a href="../Page/1970-71_NBA赛季.md" title="wikilink">1970-71</a>--<a href="../Page/1979-80_NBA赛季.md" title="wikilink">1979-80</a>[31])<br />
<a href="../Page/密尔沃基雄鹿.md" title="wikilink">密尔沃基雄鹿</a><br />
（<a href="../Page/1979-80_NBA赛季.md" title="wikilink">1979-80</a>--<a href="../Page/1983-84_NBA赛季.md" title="wikilink">1983-84</a>）</p></td>
<td><p>1,843</p></td>
<td><p>4,853</p></td>
<td><p><font color="#007FFF"><strong>9,698</strong></font></p></td>
<td><p><font color="#007FFF"><strong>10.11</strong></font></p></td>
<td><p>1992年</p></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td><p><strong><a href="../Page/塞缪尔·拉西.md" title="wikilink">塞缪尔·拉西</a></strong></p></td>
<td><p>中锋</p></td>
<td><p><a href="../Page/堪薩斯城國王.md" title="wikilink">堪薩斯城國王</a><br />
(<a href="../Page/1970-71_NBA赛季.md" title="wikilink">1970-71</a>--<a href="../Page/1981-82_NBA赛季.md" title="wikilink">1981-82</a>[32])<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a><br />
（<a href="../Page/1981-82_NBA赛季.md" title="wikilink">1981-82</a>）<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克-{里}-夫蘭騎士</a><br />
（<a href="../Page/1982-83_NBA赛季.md" title="wikilink">1982-83</a>）</p></td>
<td><p>1,647</p></td>
<td><p>5,226</p></td>
<td><p><font color="#007FFF"><strong>9,687</strong></font></p></td>
<td><p><font color="#007FFF"><strong>9.67</strong></font></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td><p><strong><a href="../Page/大卫·德布斯切尔.md" title="wikilink">大卫·德布斯切尔</a></strong></p></td>
<td><p>前锋</p></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a><br />
(<a href="../Page/1962-63_NBA赛季.md" title="wikilink">1962-63</a>--<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>[33])<br />
<a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a><br />
（<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>--<a href="../Page/1973-74_NBA赛季.md" title="wikilink">1973-74</a>）</p></td>
<td><p>134</p></td>
<td><p>623</p></td>
<td><p><font color="#007FFF"><strong>9,618</strong></font></p></td>
<td><p><font color="#007FFF"><strong>10.99</strong></font></p></td>
<td><p>1983年</p></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td><p><a href="../Page/马库斯·坎比.md" title="wikilink">马库斯·坎比</a></p></td>
<td><p>大前鋒</p></td>
<td><p><a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a><br />
（<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>)<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a><br />
（<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>--<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a><br />
（<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>--<a href="../Page/2007-08_NBA赛季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a><br />
（<a href="../Page/2008-09_NBA赛季.md" title="wikilink">2008-09</a>--<a href="../Page/2009-10_NBA赛季.md" title="wikilink">2009-10</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a><br />
（<a href="../Page/2010-11_NBA赛季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/休斯頓火箭.md" title="wikilink">休斯頓火箭</a><br />
（<a href="../Page/2012-13_NBA赛季.md" title="wikilink">2012-13</a>）</p></td>
<td><p>2,611</p></td>
<td><p>8,902</p></td>
<td><p><font color="#007FFF"><strong>9,513</strong></p></td>
<td><p>9.78</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td><p><strong><a href="../Page/A·C·格林.md" title="wikilink">A·C·格林</a></strong></p></td>
<td><p>大前锋</p></td>
<td><p><a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a><br />
（<a href="../Page/1985-86_NBA赛季.md" title="wikilink">1985-86</a>--<a href="../Page/1992-93_NBA赛季.md" title="wikilink">1992-93</a>，<br />
<a href="../Page/1999-2000_NBA赛季.md" title="wikilink">1999-2000</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a><br />
(<a href="../Page/1993-94_NBA赛季.md" title="wikilink">1993-94</a>--<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>[34])<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a><br />
（<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a><br />
（<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>）</p></td>
<td><p>3,354</p></td>
<td><p>6,119</p></td>
<td><p><font color="#007FFF"><strong>9,473</strong></font></p></td>
<td><p><font color="#007FFF"><strong>7.41</strong></font></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td><p><strong><a href="../Page/霍勒斯·格兰特.md" title="wikilink">霍勒斯·格兰特</a></strong></p></td>
<td><p>大前锋<br />
<br />
中锋</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
（<a href="../Page/1987-88_NBA赛季.md" title="wikilink">1987-88</a>--<a href="../Page/1993-94_NBA赛季.md" title="wikilink">1993-94</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a><br />
（<a href="../Page/1994-95_NBA赛季.md" title="wikilink">1994-95</a>--<a href="../Page/1998-99_NBA赛季.md" title="wikilink">1998-99</a>，<br />
<a href="../Page/2001-02_NBA赛季.md" title="wikilink">2001-02</a>--<a href="../Page/2002-03_NBA赛季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a><br />
（<a href="../Page/1999-2000_NBA赛季.md" title="wikilink">1999-2000</a>）<br />
<a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a><br />
（<a href="../Page/2000-01_NBA赛季.md" title="wikilink">2000-01</a>，<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a>）</p></td>
<td><p>3,467</p></td>
<td><p>5,976</p></td>
<td><p><font color="#007FFF"><strong>9,443</strong></font></p></td>
<td><p><font color="#007FFF"><strong>8.11</strong></font></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td><p><a href="../Page/巴里·霍维尔.md" title="wikilink">巴里·霍维尔</a></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a><br />
（<a href="../Page/1959-60_NBA赛季.md" title="wikilink">1959-60</a>--<a href="../Page/1963-64_NBA赛季.md" title="wikilink">1963-64</a>)<br />
<a href="../Page/巴爾的摩子彈.md" title="wikilink">巴爾的摩子彈</a><br />
（<a href="../Page/1964-65_NBA赛季.md" title="wikilink">1964-65</a>--<a href="../Page/1965-66_NBA赛季.md" title="wikilink">1965-66</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a><br />
（<a href="../Page/1966-67_NBA赛季.md" title="wikilink">1966-67</a>--<a href="../Page/1969-70_NBA赛季.md" title="wikilink">1969-70</a>）<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a><br />
（<a href="../Page/1970-71_NBA赛季.md" title="wikilink">1970-71</a>）</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p><font color="#007FFF"><strong>9,383</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td><p><a href="../Page/弗拉德·迪瓦茨.md" title="wikilink">弗拉德·迪瓦茨</a></p></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a><br />
（<a href="../Page/1989-90_NBA赛季.md" title="wikilink">1989-90</a>--<a href="../Page/1995-96_NBA赛季.md" title="wikilink">1995-96</a>)<br />
<a href="../Page/夏洛特黃蜂.md" title="wikilink">夏洛特黃蜂</a><br />
（<a href="../Page/1996-97_NBA赛季.md" title="wikilink">1996-97</a>--<a href="../Page/1997-98_NBA赛季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a><br />
（<a href="../Page/1999-00_NBA赛季.md" title="wikilink">1999-00</a>--<a href="../Page/2003-04_NBA赛季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a><br />
（<a href="../Page/2004-05_NBA赛季.md" title="wikilink">2004-05</a>）</p></td>
<td><p>2,881</p></td>
<td><p>6,445</p></td>
<td><p><font color="#007FFF"><strong>9,326</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td><p><a href="../Page/阿蒂斯·吉尔莫尔.md" title="wikilink">阿蒂斯·吉尔莫尔</a></p></td>
<td><p>中鋒</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
（<a href="../Page/1976-77_NBA赛季.md" title="wikilink">1976-77</a>--<a href="../Page/1981-82_NBA赛季.md" title="wikilink">1981-82</a>)<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a><br />
（<a href="../Page/1982-83_NBA赛季.md" title="wikilink">1982-83</a>--<a href="../Page/1986-87_NBA赛季.md" title="wikilink">1986-87</a>）<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><br />
（<a href="../Page/1987_NBA赛季.md" title="wikilink">1987</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a><br />
（<a href="../Page/1988_NBA赛季.md" title="wikilink">1988</a>）</p></td>
<td><p>2,639</p></td>
<td><p>6,522</p></td>
<td><p><font color="#007FFF"><strong>9,161</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>50</p></td>
<td><p><a href="../Page/约翰尼·格林.md" title="wikilink">约翰尼·格林</a></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a><br />
（<a href="../Page/1959-60_NBA赛季.md" title="wikilink">1959-60</a>--<a href="../Page/1964-65_NBA赛季.md" title="wikilink">1964-65</a>)<br />
<a href="../Page/巴爾的摩子彈.md" title="wikilink">巴爾的摩子彈</a><br />
（<a href="../Page/1965-66_NBA赛季.md" title="wikilink">1965-66</a>--<a href="../Page/1966-67_NBA赛季.md" title="wikilink">1966-67</a>）<br />
<a href="../Page/聖地牙哥火箭.md" title="wikilink">聖地牙哥火箭</a><br />
（<a href="../Page/1967-68_NBA赛季.md" title="wikilink">1967-68</a>）<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a><br />
（<a href="../Page/1968-69_NBA赛季.md" title="wikilink">1968-69</a>）<br />
<a href="../Page/辛辛那提皇家.md" title="wikilink">辛辛那提皇家</a><br />
（<a href="../Page/1969-70_NBA赛季.md" title="wikilink">1969-70</a>--<a href="../Page/1972-73_NBA赛季.md" title="wikilink">1972-73</a>）</p></td>
<td><p>N/A</p></td>
<td><p>N/A</p></td>
<td><p><font color="#007FFF"><strong>9,083</strong></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参看

  - [NBA](../Page/NBA.md "wikilink")
  - [NBA历届籃板王](../Page/NBA历届籃板王.md "wikilink")
  - [NBA个人单赛季平均籃板记录](../Page/NBA个人单赛季平均籃板记录.md "wikilink")

## 数据来源

  - [Basketball-Reference.com Career Leaders for Total
    Rebounds](http://www.basketball-reference.com/leaders/TRB_career.html)
  - [Basketball-Reference.com Career Leaders for Rebounds Per
    Game](http://www.basketball-reference.com/leaders/RPG_career.html)

[Category:NBA数据统计](../Category/NBA数据统计.md "wikilink")

1.  由於聯盟於1973－74年賽季才開始分別計算進攻籃板和防守籃板，因此本列表中部分總籃板數字並不等於進攻籃板和防守籃板的總和.

2.  球员必需正式退役满五年才有资格入选[美国篮球名人堂](../Page/美国篮球名人堂.md "wikilink").

3.  [费城勇士于](../Page/费城勇士.md "wikilink")[1962-63赛季搬到](../Page/1962-63_NBA赛季.md "wikilink")[旧金山并改名为旧金山勇士](../Page/旧金山.md "wikilink").[1964-65
    NBA赛季中途](../Page/1964-65_NBA赛季.md "wikilink")[威尔特·张伯伦被交换到](../Page/威尔特·张伯伦.md "wikilink")[费城76人](../Page/费城76人.md "wikilink").

4.  [休斯顿火箭在](../Page/休斯顿火箭.md "wikilink")[1970-71赛季前称圣地牙哥火箭](../Page/1970-71_NBA赛季.md "wikilink").

5.  [华盛顿子弹在](../Page/华盛顿子弹.md "wikilink")[1972-73赛季前称巴尔的摩子弹](../Page/1972-73_NBA赛季.md "wikilink"),[1973-74赛季称首都子弹](../Page/1973-74_NBA赛季.md "wikilink").

6.  [摩西·马龙仅仅为](../Page/摩西·马龙.md "wikilink")[布法罗勇士打了两场比赛就被交易到了](../Page/洛杉矶快船.md "wikilink")[休斯顿火箭](../Page/休斯顿火箭.md "wikilink").

7.  三藩市勇士於1971-72賽季改名為金州勇士

8.  [内特·瑟蒙德於](../Page/内特·瑟蒙德.md "wikilink")1975-76賽季中途被交換至[克里夫蘭騎士](../Page/克里夫蘭騎士.md "wikilink").

9.  [巴尔的摩子弹在](../Page/巴尔的摩子弹.md "wikilink")[1961-62赛季称芝加哥包装工人](../Page/1961-62_NBA赛季.md "wikilink"),[1962-63赛季称芝加哥西风](../Page/1962-63_NBA赛季.md "wikilink").[1965-66赛季中途](../Page/1965-66_NBA赛季.md "wikilink")[沃尔特·贝拉米被交换到](../Page/沃尔特·贝拉米.md "wikilink")[纽约尼克斯](../Page/纽约尼克斯.md "wikilink").

10. [1968-69赛季中途贝拉米被交换到](../Page/1968-69_NBA赛季.md "wikilink")[底特律活塞](../Page/底特律活塞.md "wikilink").

11. [1969-70赛季中途贝拉米被交换到](../Page/1969-70_NBA赛季.md "wikilink")[亚特兰大鹰](../Page/亚特兰大鹰.md "wikilink").

12.
13. 在[2007-08賽季中途](../Page/2007-08_NBA赛季.md "wikilink")[沙奎尔·奥尼尔被交換至菲尼克斯太阳](../Page/沙奎尔·奥尼尔.md "wikilink").

14. [1969-70赛季中途](../Page/1969-70_NBA赛季.md "wikilink")[傑里·盧卡斯被交換至](../Page/傑里·盧卡斯.md "wikilink")[三藩市勇士](../Page/三藩市勇士.md "wikilink").

15. [圣路易斯老鹰在](../Page/圣路易斯老鹰.md "wikilink")[1954-55赛季称密尔沃基老鹰](../Page/1954-55_NBA赛季.md "wikilink").

16. 在[2000-01赛季中途](../Page/2000-01_NBA赛季.md "wikilink")[迪肯貝·穆托姆博被交換至費城](../Page/迪肯貝·穆托姆博.md "wikilink")76人.

17. [亞特蘭大老鹰在](../Page/亞特蘭大老鹰.md "wikilink")[1968-69赛季前称圣路易斯老鹰](../Page/1968-69_NBA赛季.md "wikilink").

18. 在[1994-95賽季中途](../Page/1994-95_NBA赛季.md "wikilink")[凯文·威利斯被交換至邁阿密熱火](../Page/凯文·威利斯.md "wikilink").

19. 在[1995-96賽季中途](../Page/1995-96_NBA赛季.md "wikilink")[Kevin
    Willis被交換至金州勇士](../Page/Kevin_Willis.md "wikilink").

20. 在[2000-01賽季中途](../Page/2000-01_NBA赛季.md "wikilink")[Kevin
    Willis被交換至丹佛金塊](../Page/Kevin_Willis.md "wikilink").

21. [洛杉矶湖人在](../Page/洛杉矶湖人.md "wikilink")[1959-60赛季前称明尼阿波利斯湖人](../Page/1959-60_NBA赛季.md "wikilink").

22. [費城76人在](../Page/費城76人.md "wikilink")[1963-64赛季前称雪城民族](../Page/1963-64_NBA赛季.md "wikilink").

23. [亞特蘭大老鷹在](../Page/亞特蘭大老鷹.md "wikilink")[1968-69赛季前称聖路易斯老鷹](../Page/1968-69_NBA赛季.md "wikilink").
    [1971-72赛季中途](../Page/1971-72_NBA赛季.md "wikilink")[William C.
    Bridges被交换到費城](../Page/William_C._Bridges.md "wikilink")76人.

24. [1972-73赛季中途](../Page/1972-73_NBA赛季.md "wikilink")[William C.
    Bridges被交换到洛杉磯湖人](../Page/William_C._Bridges.md "wikilink").

25. [1974-75赛季中途](../Page/1974-75_NBA赛季.md "wikilink")[William C.
    Bridges被交换到](../Page/William_C._Bridges.md "wikilink")-{金州勇士}-.

26. [1981-82赛季中途](../Page/1981-82_NBA赛季.md "wikilink")[比尔·兰比尔被交换到](../Page/比尔·兰比尔.md "wikilink")[底特律活塞](../Page/底特律活塞.md "wikilink").

27. [薩克拉門托國王在](../Page/薩克拉門托國王.md "wikilink")[1985-86赛季前称堪薩斯城國王](../Page/1985-86_NBA赛季.md "wikilink").

28. [1994-95赛季中途](../Page/1994-95_NBA赛季.md "wikilink")[奧狄斯·科普被交换到波特蘭开拓者](../Page/奧狄斯·科普.md "wikilink").

29. [1997-98赛季中途](../Page/1997-98_NBA赛季.md "wikilink")[奧狄斯·科普被交换到薩克拉門托國王](../Page/奧狄斯·科普.md "wikilink").

30. [費城76人在](../Page/費城76人.md "wikilink")[1963-64赛季前称雪城國家](../Page/1963-64_NBA赛季.md "wikilink").

31. [1979-80赛季中途](../Page/1979-80_NBA赛季.md "wikilink")[鲍伯·兰尼尔被交换到](../Page/鲍伯·兰尼尔.md "wikilink")[密尔沃基雄鹿](../Page/密尔沃基雄鹿.md "wikilink").

32. [堪薩斯城國王在](../Page/堪薩斯城國王.md "wikilink")[1972-73赛季前称辛辛那提皇家](../Page/1972-73_NBA赛季.md "wikilink"),
    [1972-73赛季至](../Page/1972-73_NBA赛季.md "wikilink")[1974-75赛季称堪薩斯城](../Page/1974-75_NBA赛季.md "wikilink")-奧瑪哈國王.
    在[1981-82賽季中途](../Page/1981-82_NBA赛季.md "wikilink")[Samuel
    Lacey被交換至紐澤西籃網](../Page/Samuel_Lacey.md "wikilink").

33. 在[1968-69賽季中途](../Page/1968-69_NBA赛季.md "wikilink")[David
    DeBusschere被交換至纽约尼克斯](../Page/David_DeBusschere.md "wikilink").

34. 在[1996-97賽季中途](../Page/1996-97_NBA赛季.md "wikilink")[A·C·格林被交換至達拉斯小牛](../Page/A·C·格林.md "wikilink").