[Newtontelescope.png](https://zh.wikipedia.org/wiki/File:Newtontelescope.png "fig:Newtontelescope.png")\]
**牛頓望遠鏡**是英國天文學家[伊萨克·牛顿](../Page/伊萨克·牛顿.md "wikilink")（1643-1727）發明的[反射望遠鏡](../Page/反射望遠鏡.md "wikilink")，主鏡使用[拋物面鏡](../Page/拋物面反射.md "wikilink")，第二反射鏡是平面的對角反射鏡。

## 牛頓式設計的優點

  - 與其他形式望遠鏡比較，無論口徑大小，在品質相當的情況下，牛頓式總是比較便宜。
  - 由於光線無須穿透[物鏡](../Page/物鏡.md "wikilink")（他只從鏡子的表面反射），所以不需要特別的玻璃，材料只需要能掌握住正確的形狀。
  - 因為只需要處理一個表面（折射鏡通常需要處理四個表面），因此非常適合非專業人士[自製屬於個人的樣式](../Page/業餘望遠鏡製作.md "wikilink")。業餘天文學家自製的[杜布森望遠鏡多屬此型望遠鏡](../Page/杜布森望遠鏡.md "wikilink")。
  - 短的[焦比可以更容易的獲得較大的](../Page/焦比.md "wikilink")[視野](../Page/視野.md "wikilink")。
  - 長[焦距的牛頓式望遠鏡可以獲得卓越的行星外觀](../Page/焦距.md "wikilink")。
  - 沒有[折光器造成的](../Page/折光器.md "wikilink")[色差](../Page/色差.md "wikilink")。
  - 目鏡的位置在望遠鏡統前端，與短焦比結合可以使用短而緊湊的架台系統，減少費用和增加便利性。

[NewtonsTelescopeReplica.jpg](https://zh.wikipedia.org/wiki/File:NewtonsTelescopeReplica.jpg "fig:NewtonsTelescopeReplica.jpg")

## 牛頓式設計的缺點

  - 容易產生[彗形像差](../Page/彗形像差.md "wikilink")，造成影樣偏離軸心擴散的變形現象。這種擴散在光軸上為零，隨著鏡子的[視域呈線性的增加](../Page/視域.md "wikilink")，也與[焦距除以](../Page/焦距.md "wikilink")[口徑的商](../Page/口徑.md "wikilink")（[焦比](../Page/焦比.md "wikilink")）的平方反比來擴散。彗形像差的型式通常是'''
    3θ/16F ²
    '''，此處的θ是軸到圖像的[角度](../Page/弧度.md "wikilink")，F是焦比。通常在焦比大於[f/6的系統](../Page/焦比.md "wikilink")，[彗形像差已經可以忽略掉](../Page/彗形像差.md "wikilink")，不會影響目視或攝影的結果。焦比小於[f/4的系統](../Page/焦比.md "wikilink")，雖然不能忽視彗形像差，但可以藉由廣視野和低倍率成像來避免。商業用的透鏡也可以用在修正牛頓主鏡的彗形像差上，讓影像恢復原有的明銳。

<!-- end list -->

  - 第二反射鏡在光路的中間，會遮蔽掉部分的光線，支撐結構還會造成[繞射形成所謂的](../Page/繞射.md "wikilink")[蜘蛛網](../Page/蜘蛛網.md "wikilink")，並且降低對比。使用二或三支腳的支撐可以減少視覺上的蜘蛛網。減少繞射的[肩峰值強度更可以以四的因次有效的增強對比](../Page/肩峰值.md "wikilink")，但圓形的蜘蛛網通常是因支撐不穩，而由風造成擺動形成的懲罰。雖然四隻腳的支撐能比三隻腳更有效的消除蜘蛛網，但三支腳造成的蜘蛛網會給人一種審美上的良好觀感。

<!-- end list -->

  - 可攜式牛頓式的校準是個問題。主鏡和次鏡的準直性會因為運輸和操作時的震動而偏離，這意味著望遠鏡可能在每次使用前都需要校準。其他型式的設計，像折光鏡和折反射鏡（尤其是[馬克蘇托夫蓋塞格林式](../Page/馬克蘇托夫望遠鏡.md "wikilink")），準直性都已經固定住了。

## 相關條目

  - [施密特攝星儀](../Page/施密特攝星儀.md "wikilink")
  - [施密特-牛頓望遠鏡](../Page/施密特-牛頓望遠鏡.md "wikilink")
  - [馬克蘇托夫望遠鏡](../Page/馬克蘇托夫望遠鏡.md "wikilink")
  - [里奇-克萊琴望遠鏡](../Page/里奇-克萊琴望遠鏡.md "wikilink")

## 參考資料

  - Smith, Warren J., *Modern Optical Engineering*, McGraw-Hill Inc.,
    1966, p. 400.

[N](../Category/望遠鏡類型.md "wikilink")
[Category:艾萨克·牛顿](../Category/艾萨克·牛顿.md "wikilink")