[Kennedy_center_honors_2006.jpg](https://zh.wikipedia.org/wiki/File:Kennedy_center_honors_2006.jpg "fig:Kennedy_center_honors_2006.jpg")、第一夫人[劳拉·布什](../Page/劳拉·布什.md "wikilink")、副总统[迪克·切尼](../Page/迪克·切尼.md "wikilink")、夫人琳·切尼出席了2006年肯尼迪中心荣誉奖颁奖典礼。左起为2006年获奖者[安德鲁·劳埃德·韦伯](../Page/安德鲁·劳埃德·韦伯.md "wikilink")、[斯蒂芬·斯皮尔伯格](../Page/斯蒂芬·斯皮尔伯格.md "wikilink")、[多莉·帕顿](../Page/多莉·帕顿.md "wikilink")、[祖宾·梅塔和](../Page/祖宾·梅塔.md "wikilink")[史摩基·罗宾逊](../Page/史摩基·罗宾逊.md "wikilink")。\]\]
**肯尼迪中心荣誉奖**（**Kennedy Center
Honors**）用以表彰终身为[美国文化作出贡献的杰出人士](../Page/美国.md "wikilink")，也称为**肯尼迪中心终身成就奖**。该奖项于1978年开始每年在美国[华盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")[约翰·肯尼迪表演艺术中心颁发](../Page/约翰·肯尼迪表演艺术中心.md "wikilink")。

## 奖项介绍

每年年初，肯尼迪艺术中心理事会提交一份候获选人名单，其中包括全球知名的艺术家而以前落选的候选人。这些艺术家终身致力于美国和全球文化，领域包括[音乐](../Page/音乐.md "wikilink")、[舞蹈](../Page/舞蹈.md "wikilink")、[戏剧](../Page/戏剧.md "wikilink")、[歌剧](../Page/歌剧.md "wikilink")、[电影和](../Page/电影.md "wikilink")[电视](../Page/电视.md "wikilink")。之后从每一个艺术分支选出一名代表者，由委员会在年中的时候宣布。\[1\]

每年的12月的第一个周末举行颁奖典礼，这是一场没有界线，没有失望，没有竞争的艺术界盛会。\[2\][电视台也会全程摄制](../Page/电视台.md "wikilink")，之后在圣诞节期间播出。2006年肯尼迪中心荣誉奖颁奖典礼于2006年12月3日举行，[哥伦比亚广播公司于](../Page/哥伦比亚广播公司.md "wikilink")12月26日播出盛况。\[3\]

## 历届获奖者

下面列出了历届的获奖者。\[4\]

### 1970年代

  - 1978年：[玛丽安·安德森](../Page/玛丽安·安德森.md "wikilink")、[弗雷德·阿斯泰尔](../Page/弗雷德·阿斯泰尔.md "wikilink")、[乔治·巴兰钦](../Page/乔治·巴兰钦.md "wikilink")、[理查德·罗杰斯](../Page/理查德·罗杰斯.md "wikilink")、[阿图尔·鲁宾斯坦](../Page/阿图尔·鲁宾斯坦.md "wikilink")
  - 1979年：[阿隆·科普兰](../Page/阿隆·科普兰.md "wikilink")、[艾拉·费兹洁拉](../Page/艾拉·费兹洁拉.md "wikilink")、[亨利·方达](../Page/亨利·方达.md "wikilink")、[玛莎·葛兰姆](../Page/玛莎·葛兰姆.md "wikilink")、[田纳西·威廉斯](../Page/田纳西·威廉斯.md "wikilink")

### 1980年代

  - 1980年：[伦纳德·伯恩斯坦](../Page/伦纳德·伯恩斯坦.md "wikilink")、[詹姆斯·卡格尼](../Page/詹姆斯·卡格尼.md "wikilink")、[艾格尼斯·德米尔](../Page/艾格尼斯·德米尔.md "wikilink")、[林恩·方丹](../Page/林恩·方丹.md "wikilink")、[利昂蒂尼·普莱斯](../Page/利昂蒂尼·普莱斯.md "wikilink")
  - 1981年：[考特·贝茜](../Page/考特·贝茜.md "wikilink")、[加里·格兰特](../Page/加里·格兰特.md "wikilink")、[海伦·海斯](../Page/海伦·海斯.md "wikilink")、[杰洛姆·罗宾斯](../Page/杰洛姆·罗宾斯.md "wikilink")、[鲁道夫·塞尔金](../Page/鲁道夫·塞尔金.md "wikilink")
  - 1982年：[乔治·阿博特](../Page/乔治·阿博特.md "wikilink")、[莉莲·吉许](../Page/莉莲·吉许.md "wikilink")、[本尼·古德曼](../Page/本尼·古德曼.md "wikilink")、[金·凯利](../Page/金·凯利.md "wikilink")、[尤金·奥曼迪](../Page/尤金·奥曼迪.md "wikilink")
  - 1983年：[凯瑟琳·邓纳姆](../Page/凯瑟琳·邓纳姆.md "wikilink")、[伊利亚·卡赞](../Page/伊利亚·卡赞.md "wikilink")、[法兰克·辛纳屈](../Page/法兰克·辛纳屈.md "wikilink")、[詹姆斯·史都華](../Page/詹姆斯·史都華.md "wikilink")、[维吉尔·汤姆逊](../Page/维吉尔·汤姆逊.md "wikilink")
  - 1984年：[莲娜·霍恩](../Page/莲娜·霍恩.md "wikilink")、[丹尼·凯](../Page/丹尼·凯.md "wikilink")、[吉安·卡洛·梅诺蒂](../Page/吉安·卡洛·梅诺蒂.md "wikilink")、[阿瑟·米勒](../Page/阿瑟·米勒.md "wikilink")、[以撒·斯坦](../Page/以撒·斯坦.md "wikilink")
  - 1985年：[梅西·康宁汉](../Page/梅西·康宁汉.md "wikilink")、[艾琳·邓恩](../Page/艾琳·邓恩.md "wikilink")、[鲍勃·霍普](../Page/鲍勃·霍普.md "wikilink")、[艾伦·杰伊·勒纳](../Page/艾伦·杰伊·勒纳.md "wikilink")、[弗雷德里克·罗威](../Page/弗雷德里克·罗威.md "wikilink")、[贝克佛蕾·西尔斯](../Page/贝克佛蕾·西尔斯.md "wikilink")
  - 1986年：[露西尔·鲍尔](../Page/露西尔·鲍尔.md "wikilink")、[休姆·克罗宁](../Page/休姆·克罗宁.md "wikilink")、[杰西卡·坦迪](../Page/杰西卡·坦迪.md "wikilink")、[耶胡迪·梅纽因](../Page/耶胡迪·梅纽因.md "wikilink")、[安东尼·图多尔](../Page/安东尼·图多尔.md "wikilink")、[雷·查尔斯](../Page/雷·查尔斯.md "wikilink")
  - 1987年：[佩里·科莫](../Page/佩里·科莫.md "wikilink")、[贝蒂·戴维斯](../Page/贝蒂·戴维斯.md "wikilink")、[莎米·戴维斯](../Page/莎米·戴维斯.md "wikilink")、[内什·米尔斯坦](../Page/内什·米尔斯坦.md "wikilink")、[艾尔文·尼古莱斯](../Page/艾尔文·尼古莱斯.md "wikilink")
  - 1988年：[阿尔文·艾利](../Page/阿尔文·艾利.md "wikilink")、[乔治·伯恩斯](../Page/乔治·伯恩斯.md "wikilink")、[茂娜·罗埃](../Page/茂娜·罗埃.md "wikilink")、[亚历山大·施乃德](../Page/亚历山大·施乃德.md "wikilink")、[罗杰·L·史蒂文斯](../Page/罗杰·L·史蒂文斯.md "wikilink")
  - 1989年：[哈利·貝拉方提](../Page/哈利·貝拉方提.md "wikilink")、[克劳黛·考尔白](../Page/克劳黛·考尔白.md "wikilink")、[亚历山德拉·丹尼洛娃](../Page/亚历山德拉·丹尼洛娃.md "wikilink")、[玛丽·马丁](../Page/玛丽·马丁.md "wikilink")、[威廉·舒曼](../Page/威廉·舒曼.md "wikilink")

### 1990年代

  - 1990年：[迪齐·吉莱斯皮](../Page/迪齐·吉莱斯皮.md "wikilink")、[凯瑟琳·赫本](../Page/凯瑟琳·赫本.md "wikilink")、[里西·史蒂文斯](../Page/里西·史蒂文斯.md "wikilink")、[朱尔·史泰恩](../Page/朱尔·史泰恩.md "wikilink")、[比利·威尔德](../Page/比利·威尔德.md "wikilink")
  - 1991年：[罗伊·阿卡夫](../Page/罗伊·阿卡夫.md "wikilink")、[贝蒂·康姆顿](../Page/贝蒂·康姆顿.md "wikilink")、[阿道夫·格林](../Page/阿道夫·格林.md "wikilink")、[费雅德·尼可拉斯](../Page/费雅德·尼可拉斯.md "wikilink")、[哈罗德·尼可拉斯](../Page/哈罗德·尼可拉斯.md "wikilink")、[格里高利·派克](../Page/格里高利·派克.md "wikilink")、[罗伯特·肖](../Page/罗伯特·肖.md "wikilink")
  - 1992年：[莱昂内尔·汉普顿](../Page/莱昂内尔·汉普顿.md "wikilink")、[保罗·纽曼](../Page/保罗·纽曼.md "wikilink")、[乔安妮·伍德沃德](../Page/乔安妮·伍德沃德.md "wikilink")、[珍姬·罗杰丝](../Page/珍姬·罗杰丝.md "wikilink")、[姆斯蒂斯拉夫·罗斯特罗波维奇](../Page/姆斯蒂斯拉夫·罗斯特罗波维奇.md "wikilink")、[保罗·泰勒](../Page/保罗·泰勒.md "wikilink")
  - 1993年：[约翰尼·卡森](../Page/约翰尼·卡森.md "wikilink")、[亚瑟·米特切尔](../Page/亚瑟·米特切尔.md "wikilink")、[乔治·索尔蒂](../Page/乔治·索尔蒂.md "wikilink")、[斯蒂芬·桑德海姆](../Page/斯蒂芬·桑德海姆.md "wikilink")、[马里昂·威廉姆斯](../Page/马里昂·威廉姆斯.md "wikilink")
  - 1994年：[柯克·道格拉斯](../Page/柯克·道格拉斯.md "wikilink")、[艾瑞莎·弗兰克林](../Page/艾瑞莎·弗兰克林.md "wikilink")、[莫顿·古德](../Page/莫顿·古德.md "wikilink")、[哈罗德·普林斯](../Page/哈罗德·普林斯.md "wikilink")、[皮特·西格](../Page/皮特·西格.md "wikilink")
  - 1995年：[杰克斯·唐波士](../Page/杰克斯·唐波士.md "wikilink")、[玛丽娅·霍恩](../Page/玛丽娅·霍恩.md "wikilink")、[B.B.金](../Page/B.B.金.md "wikilink")、[西德尼·波蒂埃](../Page/西德尼·波蒂埃.md "wikilink")、[尼尔·西蒙](../Page/尼尔·西蒙.md "wikilink")
  - 1996年：[爱德华·阿尔比](../Page/爱德华·阿尔比.md "wikilink")、[本尼·卡特](../Page/本尼·卡特.md "wikilink")、[约翰尼·卡什](../Page/约翰尼·卡什.md "wikilink")、[杰克·莱蒙](../Page/杰克·莱蒙.md "wikilink")、[玛莉亚·托契夫](../Page/玛莉亚·托契夫.md "wikilink")
  - 1997年：[劳伦·巴考尔](../Page/劳伦·巴考尔.md "wikilink")、[鲍勃·迪伦](../Page/鲍勃·迪伦.md "wikilink")、[查尔顿·赫斯顿](../Page/查尔顿·赫斯顿.md "wikilink")、[杰西·诺曼](../Page/杰西·诺曼.md "wikilink")、[爱德华·维雷拉](../Page/爱德华·维雷拉.md "wikilink")
  - 1998年：[比尔·考斯比](../Page/比尔·考斯比.md "wikilink")、[约翰·坎德尔](../Page/约翰·坎德尔.md "wikilink")&[弗雷德·埃伯](../Page/弗雷德·埃伯.md "wikilink")、[威利·纳尔逊](../Page/威利·纳尔逊.md "wikilink")、[安德列·普列文](../Page/安德列·普列文.md "wikilink")、[秀兰·邓波儿](../Page/秀兰·邓波儿.md "wikilink")
  - 1999年：[維托·埔柱](../Page/維托·埔柱.md "wikilink")、[肖恩·康纳利](../Page/肖恩·康纳利.md "wikilink")、[朱迪斯·詹米森](../Page/朱迪斯·詹米森.md "wikilink")、[贾森·罗巴兹](../Page/贾森·罗巴兹.md "wikilink")、[史提夫·汪达](../Page/史提夫·汪达.md "wikilink")

### 2000年代

  - 2000年：[米克哈伊·巴什尼可夫](../Page/米克哈伊·巴什尼可夫.md "wikilink")、[查克·贝里](../Page/查克·贝里.md "wikilink")、[普拉西多·多明戈](../Page/普拉西多·多明戈.md "wikilink")、[克林特·伊斯特伍德](../Page/克林特·伊斯特伍德.md "wikilink")、[安杰拉·兰斯伯里](../Page/安杰拉·兰斯伯里.md "wikilink")
  - 2001年：[朱丽·安德鲁丝](../Page/朱丽·安德鲁丝.md "wikilink")、[范·克莱本](../Page/范·克莱本.md "wikilink")、[昆西·琼斯](../Page/昆西·琼斯.md "wikilink")、[杰克·尼科尔森](../Page/杰克·尼科尔森.md "wikilink")、[卢奇亚诺·帕瓦罗蒂](../Page/卢奇亚诺·帕瓦罗蒂.md "wikilink")
  - 2002年：[詹姆斯·厄尔·琼斯](../Page/詹姆斯·厄尔·琼斯.md "wikilink")、[詹姆士·列文](../Page/詹姆士·列文.md "wikilink")、[奇塔·里维拉](../Page/奇塔·里维拉.md "wikilink")、[保罗·西蒙](../Page/保罗·西蒙.md "wikilink")、[伊丽莎白·泰勒](../Page/伊丽莎白·泰勒.md "wikilink")
  - 2003年：[詹姆斯·布朗](../Page/詹姆斯·布朗.md "wikilink")、[卡罗尔·伯内特](../Page/卡罗尔·伯内特.md "wikilink")、[洛蕾塔·林恩](../Page/洛蕾塔·林恩.md "wikilink")、[迈克·尼科尔斯](../Page/迈克·尼科尔斯.md "wikilink")、[伊扎克·帕尔曼](../Page/伊扎克·帕尔曼.md "wikilink")
  - 2004年：[沃伦·比蒂](../Page/沃伦·比蒂.md "wikilink")、[奥西·戴维斯](../Page/奥西·戴维斯.md "wikilink")、[鲁比·迪伊](../Page/鲁比·迪伊.md "wikilink")、[埃尔顿·约翰](../Page/埃尔顿·约翰.md "wikilink")、[琼·萨瑟兰](../Page/琼·萨瑟兰.md "wikilink")、[约翰·威廉姆斯](../Page/约翰·威廉姆斯.md "wikilink")
  - 2005年：[托尼·班尼特](../Page/托尼·班尼特.md "wikilink")、[苏珊娜·法雷尔](../Page/苏珊娜·法雷尔.md "wikilink")、[朱莉·哈里斯](../Page/朱莉·哈里斯.md "wikilink")、[罗伯特·雷德福](../Page/罗伯特·雷德福.md "wikilink")、[蒂娜·特纳](../Page/蒂娜·特纳.md "wikilink")
  - 2006年：[祖宾·梅塔](../Page/祖宾·梅塔.md "wikilink")、[多莉·帕顿](../Page/多莉·帕顿.md "wikilink")、[史摩基·羅賓森](../Page/史摩基·羅賓森.md "wikilink")、[斯蒂芬·斯皮尔伯格](../Page/斯蒂芬·斯皮尔伯格.md "wikilink")、[安德鲁·劳埃德·韦伯](../Page/安德鲁·劳埃德·韦伯.md "wikilink")
  - 2007年：[里昂·弗莱谢尔](../Page/里昂·弗莱谢尔.md "wikilink")、[史提夫·马丁](../Page/史提夫·马丁.md "wikilink")、[戴安娜·羅斯](../Page/戴安娜·羅斯.md "wikilink")、[马丁·斯科塞斯](../Page/马丁·斯科塞斯.md "wikilink")、[布莱恩·威尔森](../Page/布莱恩·威尔森.md "wikilink")
  - 2008年：[摩根·弗里曼](../Page/摩根·弗里曼.md "wikilink")、[乔治·琼斯](../Page/乔治·琼斯.md "wikilink")、[芭芭拉·史翠珊](../Page/芭芭拉·史翠珊.md "wikilink")、[Twyla
    Tharp](../Page/Twyla_Tharp.md "wikilink")、[谁人乐队](../Page/谁人乐队.md "wikilink")（[皮特·汤申德和](../Page/皮特·汤申德.md "wikilink")[罗杰·多特里](../Page/罗杰·多特里.md "wikilink")）
  - 2009年：[梅尔·布鲁克斯](../Page/梅尔·布鲁克斯.md "wikilink")、[戴夫·布鲁贝克](../Page/戴夫·布鲁贝克.md "wikilink")、[Grace
    Bumbry](../Page/Grace_Bumbry.md "wikilink")、[罗伯特·德尼罗](../Page/罗伯特·德尼罗.md "wikilink")、[布鲁斯·斯普林斯廷](../Page/布鲁斯·斯普林斯廷.md "wikilink")

### 2010年代

  - 2010年：[梅尔·哈格德](../Page/梅尔·哈格德.md "wikilink")、[Jerry
    Herman](../Page/Jerry_Herman.md "wikilink")、[Bill T.
    Jones](../Page/Bill_T._Jones.md "wikilink")、[保罗·麦卡特尼](../Page/保罗·麦卡特尼.md "wikilink")、[奥普拉·温弗里](../Page/奥普拉·温弗里.md "wikilink")
  - 2011年：[Barbara
    Cook](../Page/Barbara_Cook.md "wikilink")、[尼爾·戴門](../Page/尼爾·戴門.md "wikilink")、[马友友](../Page/马友友.md "wikilink")、[桑尼·罗林斯](../Page/桑尼·罗林斯.md "wikilink")、[梅丽尔·斯特里普](../Page/梅丽尔·斯特里普.md "wikilink")
  - 2012年：[巴迪·盖伊](../Page/巴迪·盖伊.md "wikilink")、[达斯汀·霍夫曼](../Page/达斯汀·霍夫曼.md "wikilink")、[齊柏林飛船](../Page/齊柏林飛船_\(樂團\).md "wikilink")（[约翰·保罗·琼斯](../Page/约翰·保罗·琼斯_\(音乐家\).md "wikilink")、[吉米·佩奇](../Page/吉米·佩奇.md "wikilink")、[罗伯特·普兰特](../Page/罗伯特·普兰特.md "wikilink")）、[大卫·莱特曼](../Page/大卫·莱特曼.md "wikilink")、[Natalia
    Makarova](../Page/Natalia_Makarova.md "wikilink")
  - 2013年：[Martina
    Arroyo](../Page/Martina_Arroyo.md "wikilink")、[贺比·汉考克](../Page/贺比·汉考克.md "wikilink")、[比利·乔尔](../Page/比利·乔尔.md "wikilink")、[雪莉·麦克雷恩](../Page/雪莉·麦克雷恩.md "wikilink")、[卡洛斯·山塔那](../Page/卡洛斯·山塔那.md "wikilink")
  - 2014年：[阿尔·格林](../Page/阿尔·格林.md "wikilink")、[汤姆·汉克斯](../Page/汤姆·汉克斯.md "wikilink")、[Patricia
    McBride](../Page/Patricia_McBride.md "wikilink")、[史汀](../Page/史汀.md "wikilink")、[莉莉·汤普琳](../Page/莉莉·汤普琳.md "wikilink")
  - 2015年：[卡洛尔·金](../Page/卡洛尔·金.md "wikilink")、[乔治·卢卡斯](../Page/乔治·卢卡斯.md "wikilink")、[丽塔·莫瑞诺](../Page/丽塔·莫瑞诺.md "wikilink")、[小泽征尔](../Page/小泽征尔.md "wikilink")、[Cicely
    Tyson](../Page/Cicely_Tyson.md "wikilink")
  - 2016年：[玛尔塔·阿赫里奇](../Page/玛尔塔·阿赫里奇.md "wikilink")、[老鹰乐队](../Page/老鹰乐队.md "wikilink")（[唐·亨利](../Page/唐·亨利.md "wikilink")、[提摩西·許密特](../Page/提摩西·許密特.md "wikilink")、[乔·沃尔什](../Page/乔·沃尔什.md "wikilink")、[格林·佛莱](../Page/格林·佛莱.md "wikilink")）、[艾尔·帕西诺](../Page/艾尔·帕西诺.md "wikilink")、[Mavis
    Staples](../Page/Mavis_Staples.md "wikilink")、[詹姆斯·泰勒](../Page/詹姆斯·泰勒.md "wikilink")
  - 2017年：[Carmen de
    Lavallade](../Page/Carmen_de_Lavallade.md "wikilink")、[格洛丽亚·埃斯特凡](../Page/格洛丽亚·埃斯特凡.md "wikilink")、[LL
    Cool
    J](../Page/LL_Cool_J.md "wikilink")、[諾曼·李爾](../Page/諾曼·李爾.md "wikilink")、[莱昂纳尔·里奇](../Page/莱昂纳尔·里奇.md "wikilink")

## 参考

## 外部链接

  - [肯尼迪中心荣誉奖官方网站](http://www.kennedy-center.org/programs/specialevents/honors/)

[Category:肯尼迪中心荣誉奖得主](../Category/肯尼迪中心荣誉奖得主.md "wikilink")
[Category:美国艺术奖项](../Category/美国艺术奖项.md "wikilink")
[Category:1978年建立的奖项](../Category/1978年建立的奖项.md "wikilink")
[Category:奖项获得者列表](../Category/奖项获得者列表.md "wikilink")
[Category:华盛顿哥伦比亚特区文化](../Category/华盛顿哥伦比亚特区文化.md "wikilink")
[Category:圣诞节电视特别节目](../Category/圣诞节电视特别节目.md "wikilink")

1.  [肯尼迪中心荣誉奖介绍](http://www.kennedy-center.org/programs/specialevents/honors/about/home.html)


2.
3.  [2006年第29届肯尼迪中心荣誉奖](http://www.kennedy-center.org/programs/specialevents/honors/)

4.  [肯尼迪中心荣誉奖历届获奖者](http://www.kennedy-center.org/programs/specialevents/honors/history/home.html)