**CeBIT**是**办公及信息技术中心**（）的[缩写](../Page/缩写.md "wikilink")，又称“**CeBIT信息及通信技术博览会**”，是一个国际性的以[信息技术](../Page/信息技术.md "wikilink")（IT业）和[信息工程](../Page/信息工程.md "wikilink")（IE业）为主的大型[展览会](../Page/展览会.md "wikilink")，1986年起的每年春季在[德国](../Page/德国.md "wikilink")[汉诺威举行](../Page/汉诺威.md "wikilink")。展览会的组织者是[德意志展览股份公司](../Page/德意志展览股份公司.md "wikilink")（Deutsche
Messe AG，简称DMAG）。

CeBIT是全球最大的信息和[通信工程类展览会](../Page/通信工程.md "wikilink")\[1\]。

2018年11月28日，主辦單位宣布自2019年起，停辦CeBIT，展覽內容併入[漢諾威工業博覽會](../Page/漢諾威工業博覽會.md "wikilink")\[2\]。

## 发展史

[CeBIT_2000_exhibition_hall.jpg](https://zh.wikipedia.org/wiki/File:CeBIT_2000_exhibition_hall.jpg "fig:CeBIT_2000_exhibition_hall.jpg")

CeBIT传统上是[汉诺威工业博览会的一部分](../Page/汉诺威工业博览会.md "wikilink")。到了1970年[展览正式更名为](../Page/展览.md "wikilink")。快速发展的[信息和通讯技术使这个论坛充满了活力](../Page/IT.md "wikilink")，而这个公平独特的[论坛也给](../Page/论坛.md "wikilink")[信息和通讯技术提供了更多的发展空间和机遇](../Page/IT.md "wikilink")。1986年汉诺威展览公司彻底将CeBIT从汉诺威工业展览中独立出来，这使CeBIT的参展厂家、参观人数、场地规模也逐年增加。1986年3月12日第一次以独立的身份出现的CeBIT展馆[面积就达到了](../Page/面积.md "wikilink")200.000平方米，吸引了2.000多家厂商参展，其中也包括第一参展的国家[电信部门](../Page/电信.md "wikilink")。尽管有些人批评CeBIT从汉诺威工业博览会中分离出来，但其观人数还是达到了334.400位，大大超过了汉诺威工业博览会。现在它已经超过了[Systems成长成为了一个在](../Page/Systems.md "wikilink")[IT领域中极其重要的展览](../Page/IT.md "wikilink")，在[IT业中也有着相当重要的地位](../Page/IT.md "wikilink")。

1996年和1998年CeBIT以**CeBIT
Home**为主题将重点放在了家庭方面，一些[家用游戏机](../Page/家用游戏机.md "wikilink")、娱乐硬件设备、娱乐电子成为主要展览对象。也象是当时其他这方面的重要展览如[柏林国际无线电博览会](../Page/柏林国际无线电博览会.md "wikilink")（IFA）和2000年的[世界博览会也将重点放在了家用电子设备方面](../Page/世界博览会.md "wikilink")，但是后来因为一些原因，CeBIT并没有把**CeBIT
Home**这个主题继续延续下去。

1999年[德意志展览股份公司将CeBIT展览扩展到了在海外](../Page/德意志展览股份公司.md "wikilink")，例如**CeBIT
America**在[纽约举办和](../Page/纽约.md "wikilink")**CeBIT
Asia**在[上海举办](../Page/上海.md "wikilink")。

## 历届CeBIT

[Cebitsony.jpg](https://zh.wikipedia.org/wiki/File:Cebitsony.jpg "fig:Cebitsony.jpg")展区\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:CeBIT_2006_Chi_Mei_Optoelectronics_56LCD_QuadHDTV_digital_radiography_Digitalroentgen_by_HDTVTotalDOTcom.jpg "fig:缩略图")
按时间顺序排列。

### CeBIT2003

CeBIT2003有来自60个[国家的](../Page/国家.md "wikilink")6.500家厂商参展，展馆[面积达到了](../Page/面积.md "wikilink")360.000[平方米分布在](../Page/平方米.md "wikilink")27个展厅中。展期在3月12日到19日共吸引了560.000名参观者比去年少17%，尽管这样但是交易额仍比去年高出了10%。

这届展览中有着许多新鲜的技术，象是[UMTS](../Page/UMTS.md "wikilink")-移动通讯技术、移动[宽带](../Page/宽带.md "wikilink")[通讯技术的前景](../Page/通讯.md "wikilink")。还有IT-安全技术在这一年也同样重要。还有[开放源代码的](../Page/开放源代码.md "wikilink")[Linux的一些新产品如](../Page/Linux.md "wikilink")[Knoppix](../Page/Knoppix.md "wikilink")
3.2 和 [SuSE Linux](../Page/SuSE_Linux.md "wikilink")
8.2这些[自由软件都是商用和私人的办公解决方案](../Page/自由软件.md "wikilink")。

下面是由著名电脑杂志[CHIP评选的](../Page/CHIP.md "wikilink")2个奖项“CeBIT Oscar”和“CeBIT
Bremse”。

**CeBIT Oscar**

  - 使[笔记本电脑有更长使用时间的](../Page/笔记本电脑.md "wikilink")[Intel](../Page/Intel.md "wikilink")[迅驰](../Page/迅驰.md "wikilink")（Centrino）技术。
  - “Apple iLife”多媒体功能。
  - Intellon HomePlug 1.0简易[网络连接技术](../Page/网络.md "wikilink")。
  - [Microsoft在线游戏平台](../Page/Microsoft.md "wikilink")[Xbox](../Page/Xbox_\(遊戲機\).md "wikilink")
    live。

**CeBIT Bremse**

  - [TCPA](../Page/TCPA.md "wikilink")（Trusted Computing Platform
    Alliance）

### CeBIT2004

2004年来自64个[国家的](../Page/国家.md "wikilink")6.100家厂商参展，参观510.000位比期望的多了10.000名。本届重点主题是[无线上网](../Page/无线上网.md "wikilink")、家庭网络、移动电话和[UMTS](../Page/UMTS.md "wikilink")、[数码相机](../Page/数码相机.md "wikilink")。

### CeBIT2005

CeBIT 2005展期从3月10日到16日，参观人数大约480.000人其中29%来自国外。本届重点主题是[Voice Over
IP](../Page/Voice_Over_IP.md "wikilink")（[网络电话](../Page/网络电话.md "wikilink")）。

### CeBIT2006

本届CeBIT在2006年3月9日到15日在[汉诺威举办](../Page/汉诺威.md "wikilink")，主题为工作及生活数字解决方案（Digital
Solutions for Work and
Life）。在本届CeBIT展会上，主办方首次在27号馆推出“数字生活”展示类别，为历届消费电子展区填充更多内容，增添更多激情。

### CeBIT2007

2007年的CeBIT于3月15日至3月21日举行，吸引了大约48万参观者。这届展会一共有来自77个国家的6059家参展商，其中2748家来自德国。这届展会的主题之一为：因特网的移动技术。此外，通过[GPS等媒介的导航系统也引起的很大关注](../Page/GPS.md "wikilink")。

### CeBIT2008

[CeBIT_2008_Haupteingang_der_Halle_13.jpg](https://zh.wikipedia.org/wiki/File:CeBIT_2008_Haupteingang_der_Halle_13.jpg "fig:CeBIT_2008_Haupteingang_der_Halle_13.jpg")

[2014-03-14_CeBIT_Global_Conferences,_Jimmy_Wales,_Founder_Wikipedia,_(26)_On_stage_showing_the_world_for_Wikipedia_Zero_(500_millions),_while_Brent_Goff_is_still_listening.jpg](https://zh.wikipedia.org/wiki/File:2014-03-14_CeBIT_Global_Conferences,_Jimmy_Wales,_Founder_Wikipedia,_\(26\)_On_stage_showing_the_world_for_Wikipedia_Zero_\(500_millions\),_while_Brent_Goff_is_still_listening.jpg "fig:2014-03-14_CeBIT_Global_Conferences,_Jimmy_Wales,_Founder_Wikipedia,_(26)_On_stage_showing_the_world_for_Wikipedia_Zero_(500_millions),_while_Brent_Goff_is_still_listening.jpg")
2014 on *CeBIT Global Conferences*, [Wikipedia
Zero](../Page/Wikipedia_Zero.md "wikilink")\]\]

2008年CeBIT于3月4日到9日举行。共5845个来自77个国家的展览商在此展示了自己的商品。这次CeBIT共有49万5千名游客到访，其中五分之一是非德国人\[3\]。

## 参考文献

## 外部链接

  - [cebit.de Cebit官方网站](http://www.cebit.de)
  - [www.messe.de 德意志展览股份公司官方主页](http://www.messe.de)
  - [heise.de/cebit 详细信息](http://www.heise.de/cebit/)

[Category:電腦資訊展覽](../Category/電腦資訊展覽.md "wikilink")
[Category:德国贸易展览会](../Category/德国贸易展览会.md "wikilink")
[Category:電腦相關獎項](../Category/電腦相關獎項.md "wikilink")
[Category:漢諾威公司](../Category/漢諾威公司.md "wikilink")
[Category:1970年建立的週期性事件](../Category/1970年建立的週期性事件.md "wikilink")
[Category:2019年廢除的週期性事件](../Category/2019年廢除的週期性事件.md "wikilink")

1.
2.
3.