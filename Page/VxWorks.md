**VxWorks**是[美国](../Page/美国.md "wikilink")[溫瑞爾系統](../Page/溫瑞爾系統.md "wikilink")（）公司于1983年设计开发的一种[嵌入式](../Page/嵌入式.md "wikilink")[实时操作系统](../Page/实时操作系统.md "wikilink")（[RTOS](../Page/RTOS.md "wikilink")），是嵌入式开发环境的关键组成部分。良好的持续发展能力、高性能的[内核以及友好的用户开发环境](../Page/内核.md "wikilink")，在嵌入式实时操作系统领域占据一席之地。VxWorks支持几乎所有现代市场上的嵌入式CPU，包括[x86系列](../Page/x86.md "wikilink")、[MIPS](../Page/MIPS.md "wikilink")、
[PowerPC](../Page/PowerPC.md "wikilink")、[Freescale
ColdFire](../Page/Freescale_ColdFire.md "wikilink")、[Intel
i960](../Page/Intel_i960.md "wikilink")、[SPARC](../Page/SPARC.md "wikilink")、[SH-4](../Page/SuperH.md "wikilink")、[ARM](../Page/ARM_architecture.md "wikilink"),
[StrongARM以及](../Page/StrongARM.md "wikilink")[xScale](../Page/xScale.md "wikilink")
CPU。

它以其良好的可靠性和卓越的实时性被广泛地应用在通信、军事、航空、航天等高精尖技术及实时性要求极高的领域中，如卫星通讯、军事演习、弹道制导、飞机导航等。在美国的[F-16](../Page/F-16戰隼戰鬥機.md "wikilink")、[F/A-18战斗机](../Page/F-18.md "wikilink")、[B-2隐形轰炸机和](../Page/B-2幽灵隐形战略轰炸机.md "wikilink")[爱国者导弹上](../Page/爱国者导弹.md "wikilink")，甚至连一些[火星探測器](../Page/火星探測器.md "wikilink")，如1997年7月登陆的火星探测器，2008年5月登陆的[凤凰号](../Page/凤凰号.md "wikilink")\[1\]，和2012年8月登陸的[好奇號也都使用到了VxWorks](../Page/火星科学实验室.md "wikilink")。

## 參考文獻

## 外部链接

  -
  -
{{-}}

[Category:实时操作系统](../Category/实时操作系统.md "wikilink")
[Category:机器人操作系统](../Category/机器人操作系统.md "wikilink")

1.  <http://blogs.windriver.com/deliman/2008/05/did-you-watch-i.html>