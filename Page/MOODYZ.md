**MOODYZ**（）是[日本一家](../Page/日本.md "wikilink")[成人视频制造商](../Page/成人视频制造商.md "wikilink")，隸屬於[北都集團](../Page/北都集團.md "wikilink")，於2000年成立，總部位日本[石川縣](../Page/石川縣.md "wikilink")[加賀市美岬町](../Page/加賀市.md "wikilink")1-1
AVC運動中心。

## 歷史

MOODYZ的前身為於1996年成立的Mr.President，2000年，Mr.President切割成為一家公司，以製作小組MOODYS為名，又將當中的S改成Z。

MOODYZ首部作品為由[金澤文子與](../Page/金澤文子.md "wikilink")[鈴木麻奈美等共同出演的](../Page/鈴木麻奈美.md "wikilink")《Chaos-色情四姉妹-》，於2000年9月推出。

2001年9月，由已經引退的[小澤圓跨刀演出](../Page/小澤圓.md "wikilink")，於2002年則有[紋舞蘭](../Page/紋舞蘭.md "wikilink")、[nana和](../Page/nana_\(日本AV女優\).md "wikilink")[南波杏等知名女優活耀](../Page/南波杏.md "wikilink")。

## 主要商標（2008年6月至今）

  - DIVA
  - acid
  - Gati
  - REAL
  - BEST（総集編）
  - VALUE
  - Q
  - まんきつ

## 専属女優

**2016年**

  - **[西宮このみ](../Page/西宮このみ.md "wikilink")**（2016年11月 - ）
  - **[高橋しょう子](../Page/高橋聖子.md "wikilink")**（2016年9月 - ）
  - **[千早希](../Page/千早希.md "wikilink")**（2016年7月 - ）

**2015年**

  - **[伊東ちなみ](../Page/伊東ちなみ.md "wikilink")**（いとう ちなみ、2015年10月 - ）

**2014年**

  - [神咲詩織](../Page/神咲詩織.md "wikilink")（2014年 4月 - ）
  - [初川みなみ](../Page/初川南.md "wikilink")（2014年 3月 - ）
  - [森田まゆ](../Page/森田まゆ.md "wikilink")（2014年 3月 - ）
  - [秋山祥子](../Page/秋山祥子.md "wikilink")（2014年 2月 - ）

**2013年**

  - [つぼみ](../Page/蕾_\(AV女優\).md "wikilink") （2013年 9月 - ）
  - [西川ゆい](../Page/西川ゆい.md "wikilink") （2013年 5月 - ）
  - [尾上若葉](../Page/尾上若葉.md "wikilink") （2013年 1月 - ）

**2012年**

  - [JULIA](../Page/JULIA_\(AV女優\).md "wikilink") （2012年 2月 - ）

**2011年**

  - [ももかさくら](../Page/ももかさくら.md "wikilink") （2011年 5月 - ）
  - [菅野さゆき](../Page/菅野さゆき.md "wikilink") （2011年 3月 - ）

**2010年**

  - [仁科百華](../Page/仁科百華.md "wikilink") （2010年12月 - ）
  - [周防ゆきこ](../Page/周防雪子.md "wikilink") （2010年 8月 - ）
  - [芦名未帆](../Page/芦名未帆.md "wikilink") （2010年 7月 - ）
  - [篠めぐみ](../Page/篠惠美.md "wikilink") （2010年 7月 - ）
  - [大沢美加](../Page/大澤美加.md "wikilink") （2010年 6月 - ）
  - [桐原エリカ](../Page/桐原绘里香.md "wikilink") （2010年 5月 - ）
  - [佐山愛](../Page/佐山愛.md "wikilink") （2010年 4月 - ）

**2009年**

  - [きよみ玲](../Page/きよみ玲.md "wikilink") （2009年12月 - ）
  - [桃野なごみ](../Page/桃野なごみ.md "wikilink") （2009年 9月 - ）
  - [美月](../Page/美月_\(AV女優\).md "wikilink") （2009年 6月 - ）
  - [亜梨](../Page/亜梨.md "wikilink") （2009年 2月 - ）

**2008年**

  - [松嶋れいな](../Page/松嶋れいな.md "wikilink") （2008年 8月 - ）
  - [西野翔](../Page/西野翔.md "wikilink") （2008年 3月 - ）
  - [大橋未久](../Page/大橋未久.md "wikilink") （2008年 1月 - ）

**2007年**

  - [浜崎りお](../Page/濱崎里緒.md "wikilink") （2007年11月 - ）

**2006年**

  - [峰なゆか](../Page/峰なゆか.md "wikilink") （2006年 1月 - ）

**2005年**

  - [天衣みつ](../Page/天衣みつ.md "wikilink") （2005年 5月 - ）
  - [持田茜](../Page/持田茜.md "wikilink") （2005年 5月 - ）
  - [乃亜](../Page/乃亞.md "wikilink") （2005年 2月 - ）

**2004年**

  - [紅音ほたる](../Page/紅音螢.md "wikilink") （2004年11月 - ）
  - [青木玲](../Page/青木玲.md "wikilink") （2004年 1月 - ）

**2003年**

  - [吉永ゆりあ](../Page/吉永ゆりあ.md "wikilink") （2003年 9月 - ）
  - [彩名杏子](../Page/彩名杏子.md "wikilink") （2003年 6月 - ）

**2002年**

  - [金沢文子](../Page/金澤文子.md "wikilink") （2002年 8月 - ）

## 封面女郎

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>1月</p></th>
<th><p>2月</p></th>
<th><p>3月</p></th>
<th><p>4月</p></th>
<th><p>5月</p></th>
<th><p>6月</p></th>
<th><p>7月</p></th>
<th><p>8月</p></th>
<th><p>9月</p></th>
<th><p>10月</p></th>
<th><p>11月</p></th>
<th><p>12月</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2000</strong></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>吉川みなみ</p></td>
<td><p><a href="../Page/藤崎彩花.md" title="wikilink">藤崎彩花</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2001</strong></p></td>
<td><p><a href="../Page/夢野まりあ.md" title="wikilink">夢野まりあ</a></p></td>
<td><p><a href="../Page/清水かおり.md" title="wikilink">清水かおり</a></p></td>
<td><p><a href="../Page/沢木まゆみ.md" title="wikilink">沢木まゆみ</a></p></td>
<td><p><a href="../Page/大澤恵.md" title="wikilink">美月ゆいな</a></p></td>
<td><p><a href="../Page/林絵理.md" title="wikilink">林絵理</a></p></td>
<td><p><a href="../Page/篠原まこと.md" title="wikilink">篠原まこと</a></p></td>
<td><p><a href="../Page/森野いずみ.md" title="wikilink">森野いずみ</a></p></td>
<td><p><a href="../Page/小泉麻由.md" title="wikilink">小泉麻由</a></p></td>
<td><p><a href="../Page/小沢まどか.md" title="wikilink">小沢まどか</a></p></td>
<td><p><a href="../Page/なごみもえ.md" title="wikilink">なごみもえ</a></p></td>
<td><p><a href="../Page/宝来みゆき.md" title="wikilink">宝来みゆき</a></p></td>
<td><p><a href="../Page/百華.md" title="wikilink">百華</a>（中田しこ）</p></td>
</tr>
<tr class="odd">
<td><p><strong>2002</strong></p></td>
<td><p><a href="../Page/岡田りな.md" title="wikilink">岡田りな</a></p></td>
<td><p><a href="../Page/深田涼子.md" title="wikilink">深田涼子</a></p></td>
<td><p><a href="../Page/新山愛里.md" title="wikilink">新山愛里</a></p></td>
<td><p>百華<br />
<a href="../Page/京野真里奈.md" title="wikilink">京野真里奈</a><br />
<a href="../Page/紋舞らん.md" title="wikilink">紋舞らん</a><br />
Saiko</p></td>
<td><p>瑞穂このみ</p></td>
<td><p><a href="../Page/安来めぐ.md" title="wikilink">安来めぐ</a></p></td>
<td><p>紋舞らん<br />
京野真里奈<br />
蒼吹雪<br />
藤木那奈<br />
百華</p></td>
<td><p>西村萌<br />
<a href="../Page/深芳野.md" title="wikilink">深芳野</a></p></td>
<td><p><a href="../Page/南波杏.md" title="wikilink">南波杏</a></p></td>
<td><p>南波杏</p></td>
<td><p>南波杏</p></td>
<td><p><a href="../Page/苺みるく.md" title="wikilink">苺みるく</a><br />
<a href="../Page/藤森エレナ.md" title="wikilink">藤森エレナ</a><br />
<a href="../Page/佐々木空.md" title="wikilink">佐々木空</a><br />
<a href="../Page/デヴィ_(AV女優).md" title="wikilink">デヴィ</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2003</strong></p></td>
<td><p><a href="../Page/黒崎扇菜.md" title="wikilink">黒崎扇菜</a></p></td>
<td><p><a href="../Page/京野真里奈.md" title="wikilink">京野真里奈</a><br />
百華<br />
<a href="../Page/紋舞らん.md" title="wikilink">紋舞らん</a><br />
MECUMI<br />
<a href="../Page/矢田あき.md" title="wikilink">矢田あき</a></p></td>
<td><p><a href="../Page/長谷川瞳_(AV女優).md" title="wikilink">長谷川瞳</a><br />
<a href="../Page/野原りん.md" title="wikilink">野原りん</a></p></td>
<td><p>金沢文子<br />
<a href="../Page/鈴木麻奈美.md" title="wikilink">鈴木麻奈美</a></p></td>
<td><p><a href="../Page/君嶋もえ.md" title="wikilink">君嶋もえ</a><br />
ゆりあ</p></td>
<td><p><a href="../Page/春菜まい.md" title="wikilink">春菜まい</a><br />
<a href="../Page/彩名杏子.md" title="wikilink">彩名杏子</a></p></td>
<td><p><a href="../Page/大空あすか.md" title="wikilink">大空あすか</a></p></td>
<td><p>南波杏</p></td>
<td><p>吉永ゆりあ</p></td>
<td><p>君嶋もえ<br />
ゆりあ</p></td>
<td><p>南波杏</p></td>
<td><p>君嶋もえ<br />
吉永ゆりあ<br />
ゆりあ<br />
南波杏</p></td>
</tr>
<tr class="odd">
<td><p><strong>2004</strong></p></td>
<td><p><a href="../Page/遥優衣.md" title="wikilink">遥優衣</a><br />
<a href="../Page/上原美紀.md" title="wikilink">上原美紀</a></p></td>
<td><p>吉永ゆりあ</p></td>
<td><p><a href="../Page/COCOLO_(AV女優).md" title="wikilink">COCOLO</a></p></td>
<td><p>COCOLO</p></td>
<td><p><a href="../Page/来生ひかり.md" title="wikilink">来生ひかり</a></p></td>
<td><p>南波杏<br />
<a href="../Page/小泉キラリ.md" title="wikilink">小泉キラリ</a></p></td>
<td><p>南波杏<br />
遥優衣</p></td>
<td><p><a href="../Page/野々宮りん.md" title="wikilink">野々宮りん</a></p></td>
<td><p><a href="../Page/nana_(AV女優).md" title="wikilink">nana</a></p></td>
<td><p>藤崎楓</p></td>
<td><p><a href="../Page/黒沢愛.md" title="wikilink">黒沢愛</a></p></td>
<td><p>南波杏</p></td>
</tr>
<tr class="even">
<td><p><strong>2005</strong></p></td>
<td><p><a href="../Page/吉岡なつみ.md" title="wikilink">吉岡なつみ</a></p></td>
<td><p>nana</p></td>
<td><p>吉岡なつみ</p></td>
<td><p>南波杏<br />
<a href="../Page/森下くるみ.md" title="wikilink">森下くるみ</a></p></td>
<td><p><a href="../Page/日野鈴.md" title="wikilink">日野鈴</a></p></td>
<td><p><a href="../Page/早咲まみ.md" title="wikilink">早咲まみ</a></p></td>
<td><p><a href="../Page/沙雪_(AV女優).md" title="wikilink">沙雪</a></p></td>
<td><p><a href="../Page/山城美姫.md" title="wikilink">山城美姫</a></p></td>
<td></td>
<td><p><a href="../Page/柚木あや.md" title="wikilink">柚木あや</a></p></td>
<td><p><a href="../Page/神谷りの.md" title="wikilink">神谷りの</a></p></td>
<td><p><a href="../Page/鈴木杏里.md" title="wikilink">鈴木杏里</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/南つかさ.md" title="wikilink">南つかさ</a></p></td>
<td><p><a href="../Page/天乃みお.md" title="wikilink">天乃みお</a></p></td>
<td><p><a href="../Page/峰なゆか.md" title="wikilink">峰なゆか</a></p></td>
<td><p>鈴木杏里</p></td>
<td><p>峰なゆか</p></td>
<td><p><a href="../Page/能田曜子.md" title="wikilink">能田曜子</a></p></td>
<td><p><a href="../Page/佐藤ショコラ.md" title="wikilink">佐藤ショコラ</a></p></td>
<td><p><a href="../Page/ICHIKA.md" title="wikilink">ICHIKA</a></p></td>
<td><p>ICHIKA</p></td>
<td><p><a href="../Page/花宮あみ.md" title="wikilink">花宮あみ</a></p></td>
<td><p><a href="../Page/結川るり.md" title="wikilink">結川るり</a></p></td>
<td><p><a href="../Page/倉持りおな.md" title="wikilink">倉持りおな</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/聖乃マリア.md" title="wikilink">聖乃マリア</a></p></td>
<td><p>峰なゆか<br />
<a href="../Page/水元ゆうな.md" title="wikilink">水元ゆうな</a><br />
<a href="../Page/妃乃ひかり.md" title="wikilink">妃乃ひかり</a><br />
<a href="../Page/@YOU.md" title="wikilink">@YOU</a></p></td>
<td><p><a href="../Page/あいだゆあ.md" title="wikilink">あいだゆあ</a></p></td>
<td><p><a href="../Page/青木りん.md" title="wikilink">青木りん</a></p></td>
<td><p>リナ・ディゾン</p></td>
<td><p><a href="../Page/秋原亜由.md" title="wikilink">秋原亜由</a></p></td>
<td><p>EREN</p></td>
<td><p><a href="../Page/竹内あい.md" title="wikilink">竹内あい</a></p></td>
<td><p><a href="../Page/青山菜々.md" title="wikilink">青山菜々</a></p></td>
<td><p>峰なゆか<br />
竹内あい</p></td>
<td><p>竹内あい</p></td>
<td><p>竹内あい<br />
峰なゆか<br />
青山菜々<br />
@YOU<br />
妃乃ひかり<br />
<a href="../Page/風間ゆみ.md" title="wikilink">風間ゆみ</a><br />
<a href="../Page/はるか悠.md" title="wikilink">はるか悠</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2008</strong></p></td>
<td><p>浜崎りお</p></td>
<td><p>南波杏</p></td>
<td><p>南波杏</p></td>
<td><p>青山菜々</p></td>
<td><p><a href="../Page/心有花.md" title="wikilink">心有花</a></p></td>
<td><p><a href="../Page/水森れん.md" title="wikilink">水森れん</a></p></td>
<td><p><a href="../Page/七瀬ジュリア.md" title="wikilink">七瀬ジュリア</a></p></td>
<td><p><a href="../Page/水無月レイラ.md" title="wikilink">水無月レイラ</a></p></td>
<td><p>青山菜々</p></td>
<td><p><a href="../Page/小川あさ美.md" title="wikilink">小川あさ美</a></p></td>
<td><p><a href="../Page/鮎川なお.md" title="wikilink">鮎川なお</a><br />
竹内あい<br />
<a href="../Page/百瀬涼.md" title="wikilink">百瀬涼</a><br />
<a href="../Page/水城奈緒.md" title="wikilink">水城奈緒</a></p></td>
<td><p><a href="../Page/さくら結衣.md" title="wikilink">さくら結衣</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2009</strong></p></td>
<td><p><a href="../Page/彩月あかり.md" title="wikilink">彩月あかり</a></p></td>
<td><p>西野翔</p></td>
<td><p><a href="../Page/小峰ひなた.md" title="wikilink">小峰ひなた</a></p></td>
<td><p>小川あさ美</p></td>
<td><p><a href="../Page/亜梨.md" title="wikilink">亜梨</a></p></td>
<td><p><a href="../Page/花井メイサ.md" title="wikilink">花井メイサ</a></p></td>
<td><p><a href="../Page/杉崎りか.md" title="wikilink">杉崎りか</a></p></td>
<td><p><a href="../Page/松すみれ.md" title="wikilink">松すみれ</a></p></td>
<td><p><a href="../Page/佐々木香里奈.md" title="wikilink">佐々木香里奈</a></p></td>
<td><p>一色百音</p></td>
<td><p><a href="../Page/中山エリス.md" title="wikilink">中山エリス</a></p></td>
<td><p><a href="../Page/椎名くるみ.md" title="wikilink">椎名くるみ</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2010</strong></p></td>
<td><p><a href="../Page/福音未来.md" title="wikilink">福音未来</a></p></td>
<td><p><a href="../Page/Hitomi_(AV女優).md" title="wikilink">Hitomi</a></p></td>
<td><p><a href="../Page/松乃涼.md" title="wikilink">松乃涼</a></p></td>
<td><p>Hitomi</p></td>
<td><p><a href="../Page/七色あん.md" title="wikilink">七色あん</a></p></td>
<td><p>七色あん</p></td>
<td><p>周防ゆきこ</p></td>
<td><p><a href="../Page/優希まこと.md" title="wikilink">優希まこと</a></p></td>
<td><p><a href="../Page/小嶋ゆい菜.md" title="wikilink">小嶋ゆい菜</a></p></td>
<td><p><a href="../Page/花村沙知.md" title="wikilink">花村沙知</a></p></td>
<td><p><a href="../Page/西真奈美.md" title="wikilink">西真奈美</a></p></td>
<td><p><a href="../Page/佐山愛.md" title="wikilink">佐山愛</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2011</strong></p></td>
<td><p><a href="../Page/かすみりさ.md" title="wikilink">かすみりさ</a></p></td>
<td><p>菅野さゆき</p></td>
<td><p><a href="../Page/前田優希.md" title="wikilink">前田優希</a></p></td>
<td><p>峰なゆか<br />
西野翔</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 主要系列

  - MOODYZファン感謝祭 バコバコバスツアー
  - 女教師レイプ輪姦
  - はじめての真性中出し
  - ドリームウーマン
  - ハイパーデジタルモザイク
  - 初セル
  - 新星
  - 最高のオナニーのために
  - SEX革命
  - 6本番
  - Sex On The Beach
  - 激ピストン
  - 究極3P×4本番
  - ドリームアイドル
  - 極乱交
  - 拷問くらぶ
  - 使い捨てＭ奴隷
  - パイパンハイパーデジタルモザイク
  - 黒人とセックス
  - ぶっかけ中出しアナルFuck
  - 強制中出しエロ玩具
  - ゴックンランド
  - 男汁バイキング
  - 実話輪姦
  - 石橋渉の素人生ドル
  - チンポを見たがる女たち
  - バコバコバスツアー
  - ドリーム学園
  - 石橋渉のHUNTING

## 外部連結

  - [MOODYZ官網](http://www.moodyz.com/)

  -
[Category:2000年成立的公司](../Category/2000年成立的公司.md "wikilink")
[Category:日本色情片公司](../Category/日本色情片公司.md "wikilink")