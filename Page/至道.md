**至道**（995年—997年）是[宋太宗的最后一个](../Page/宋太宗.md "wikilink")[年号](../Page/年号.md "wikilink")，[北宋使用这个年号共](../Page/北宋.md "wikilink")3年，有指**最好的学说、道德或政治制度**的意思。

至道三年三月[宋真宗即位沿用](../Page/宋真宗.md "wikilink")。\[1\]

## 纪年

| 至道                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 995年                           | 996年                           | 997年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙未](../Page/乙未.md "wikilink") | [丙申](../Page/丙申.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [統和](../Page/統和.md "wikilink")（983年六月至1012年閏十月）：[契丹](../Page/契丹.md "wikilink")—[遼聖宗耶律隆绪之年號](../Page/遼聖宗.md "wikilink")
      - [廣明](../Page/广明_\(段素英\).md "wikilink")：[大理國](../Page/大理國.md "wikilink")—[段素英](../Page/段素英.md "wikilink")（985年至1009年在位）之年號
      - [廣德](../Page/廣德.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明治](../Page/明治.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明應](../Page/明應.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明統](../Page/明統.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明德](../Page/明德.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明聖](../Page/明聖.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明法](../Page/明法.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [明運](../Page/明運.md "wikilink")：大理國—段素英（985年至1009年在位）之年號
      - [天兴](../Page/天兴_\(尉迟僧伽罗摩\).md "wikilink")（986年-999年）：[于闐](../Page/于闐.md "wikilink")—[尉迟僧伽罗摩之年號](../Page/尉迟僧伽罗摩.md "wikilink")
      - [正曆](../Page/正曆_\(一條天皇\).md "wikilink")（990年十一月七日至995年二月二十二日）：日本[一條天皇年号](../Page/一條天皇.md "wikilink")
      - [長德](../Page/長德.md "wikilink")（995年二月二十二日至999年一月十三日）：日本一條天皇年号
      - [應天](../Page/應天_\(黎桓\).md "wikilink")（994年至1007年）：前黎朝—黎桓、黎龍鉞、黎龍鋌之年號

## 参考文献

<div class="references-small">

<references />

</div>

[Category:北宋年号](../Category/北宋年号.md "wikilink")
[Category:10世纪中国年号](../Category/10世纪中国年号.md "wikilink")
[Category:990年代中国政治](../Category/990年代中国政治.md "wikilink")

1.