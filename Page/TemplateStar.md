<table>
<caption><strong></strong></caption>
<thead>
<tr class="header">
<th><p>观测数据<br />
<small><a href="曆元.md" title="wikilink">曆元</a></small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="变星.md" title="wikilink">变星</a></p></td>
</tr>
<tr class="even">
<td><p><a href="光谱类型.md" title="wikilink">光谱类型</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="赤经.md" title="wikilink">赤经</a></p></td>
</tr>
<tr class="even">
<td><p><a href="赤纬.md" title="wikilink">赤纬</a></p></td>
</tr>
<tr class="odd">
<td><p>距离<a href="地球.md" title="wikilink">地球</a></p></td>
</tr>
<tr class="even">
<td><p><a href="视星等.md" title="wikilink">视星等</a> <small>(V)</small></p></td>
</tr>
<tr class="odd">
<td><p>物理属性</p></td>
</tr>
<tr class="even">
<td><p>质量</p></td>
</tr>
<tr class="odd">
<td><p>半径</p></td>
</tr>
<tr class="even">
<td><p>色彩<small>(B-V)</small></p></td>
</tr>
<tr class="odd">
<td><p>色彩<small>(V-I)</small></p></td>
</tr>
<tr class="even">
<td><p><a href="绝对星等.md" title="wikilink">绝对星等</a> <small>(V)</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="光度.md" title="wikilink">光度</a> <small>(V)</small></p></td>
</tr>
<tr class="even">
<td><p><a href="表面温度.md" title="wikilink">表面温度</a></p></td>
</tr>
<tr class="odd">
<td><p>年龄</p></td>
</tr>
<tr class="even">
<td><p>自转周期</p></td>
</tr>
<tr class="odd">
<td><p>振荡周期</p></td>
</tr>
<tr class="even">
<td><p>显著特征</p></td>
</tr>
<tr class="odd">
<td><p>其他名称</p></td>
</tr>
<tr class="even">
<td><p>系统</p></td>
</tr>
<tr class="odd">
<td><p>恒星子星</p></td>
</tr>
<tr class="even">
<td><p>恒星子星</p></td>
</tr>
<tr class="odd">
<td><p>行星和小天体</p></td>
</tr>
</tbody>
</table>