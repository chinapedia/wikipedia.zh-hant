嘉定通志

``| chuquocngu=Gia Định Thành Thông Chí{{`}}Gia Định Thông Chí``
`}}`

《**嘉定城通志**》（[越南語](../Page/越南語.md "wikilink")：***Gia Định Thành Thông
Chí***），又稱為《*'嘉定通志**》（[越南語](../Page/越南語.md "wikilink")：*** Gia Định
Thông Chí
'''''），古代[越南南方的地方志](../Page/越南.md "wikilink")，採用[漢語](../Page/漢語.md "wikilink")[文言文](../Page/文言文.md "wikilink")，由[阮朝官員兼學者](../Page/阮朝.md "wikilink")[鄭懷德](../Page/鄭懷德.md "wikilink")（[越南語](../Page/越南語.md "wikilink")：Trịnh
Hoài
Đức）於1820年編成，內容是[嘉定城及](../Page/嘉定城.md "wikilink")[越南南部一帶](../Page/越南.md "wikilink")（[嘉定城](../Page/嘉定城.md "wikilink")、[藩安鎮](../Page/藩安鎮.md "wikilink")、[邊和鎮](../Page/邊和鎮.md "wikilink")、[定祥鎮](../Page/定祥鎮.md "wikilink")、[永清鎮及](../Page/永清鎮.md "wikilink")[河仙鎮](../Page/河仙鎮.md "wikilink")）的歷史沿革、建置、疆域、物產、商業、交通、水利、城鎮等情況，以及提到了當地[華僑的事跡](../Page/華僑.md "wikilink")，是研究[阮朝初年歷史及](../Page/阮朝.md "wikilink")[越南南部](../Page/越南.md "wikilink")[華僑史的重要史料](../Page/華僑.md "wikilink")。\[1\]

## 成書過程

### 成書背景

《嘉定城通志》的作者[鄭懷德](../Page/鄭懷德.md "wikilink")，曾於[嘉隆十五年](../Page/嘉隆.md "wikilink")（1816年）至[明命元年](../Page/明命.md "wikilink")（1820年）期間，在[嘉定城擔任官職](../Page/嘉定城.md "wikilink")。\[2\]至於該書的編撰，是為了嚮應[阮朝政府](../Page/阮朝.md "wikilink")「詔求故典」而進行的。據《[大南實錄](../Page/大南實錄.md "wikilink")·正編·第二紀》卷三[庚辰明命元年](../Page/庚辰.md "wikilink")（1820年）五月條裡所述，[越南經歷了](../Page/越南.md "wikilink")[黎](../Page/後黎朝.md "wikilink")[阮鼎革的大動亂後](../Page/阮朝.md "wikilink")，政府的文書資料多已喪失，所以朝廷「特准中外臣庶，凡有家藏編錄記先朝故典，不拘詳略，以原本進納或送官抄錄，各有獎賞。由是中外（指[阮朝中央政府及](../Page/阮朝.md "wikilink")[越南地方](../Page/越南.md "wikilink")）各以編錄來獻。尚書[鄭懷德獻](../Page/鄭懷德.md "wikilink")《嘉定通志》三卷及《[明渤遺漁文書草](../Page/明渤遺漁文書草.md "wikilink")》。」\[3\]按照這條史料，[鄭懷德是在](../Page/鄭懷德.md "wikilink")1820年寫成此書並向[阮朝朝廷進呈的](../Page/阮朝.md "wikilink")。

### 編撰過程

學者[戴可來認為](../Page/戴可來.md "wikilink")[鄭懷德並非在](../Page/鄭懷德.md "wikilink")[明命元年](../Page/明命.md "wikilink")（1820年）「詔求故典」時，才匆匆撰書的，而是「在1820年上進之前已經成書。根據文中有[嘉隆十八年](../Page/嘉隆.md "wikilink")（1819）之記事，可以推定其草成於此時。」因此，在1820年進呈的《嘉定城通志》，已是一早編成的定稿。\[4\]

## 內容

### 書中述及的地域

本書的名稱雖冠以「[嘉定城](../Page/嘉定城.md "wikilink")」，但郤包括當時的另外五個鎮，即[藩安鎮](../Page/藩安鎮.md "wikilink")（現今屬於[胡志明市](../Page/胡志明市.md "wikilink")、[西寧省](../Page/西寧省.md "wikilink")、[同奈省](../Page/同奈省.md "wikilink")、[前江省](../Page/前江省.md "wikilink")、[隆安省等地](../Page/隆安省.md "wikilink")）、[邊和鎮](../Page/邊和鎮.md "wikilink")、[定祥鎮](../Page/定祥鎮.md "wikilink")、[永清鎮](../Page/永清鎮.md "wikilink")（現今屬於[後江省](../Page/後江省.md "wikilink")、[安江省](../Page/安江省.md "wikilink")、[檳椥省等地](../Page/檳椥省.md "wikilink")）及[河仙鎮](../Page/河仙鎮.md "wikilink")。理由是[嘉隆七年](../Page/嘉隆.md "wikilink")（1808年）時，[阮朝以](../Page/阮朝.md "wikilink")[嘉定城統攝該五鎮](../Page/嘉定城.md "wikilink")，這個設置一直到[明命十三年](../Page/明命.md "wikilink")（1832年）才罷除。因此，《嘉定城通志》並非載及嘉定一座城池，而是把[嘉定城作為一個行政區劃單位](../Page/嘉定城.md "wikilink")，述及其屬下的五個鎮，亦即整個[越南南方](../Page/越南.md "wikilink")。\[5\]

### 篇幅

《嘉定城通志》原書共三卷，分別是：第一卷《星野志》（內容是傳統[中國](../Page/中國.md "wikilink")[占星術的分野概念及氣候](../Page/占星術.md "wikilink")）、《山川志》、《風俗志》；第二卷《疆域志》；第三卷《物產志》、《城池志》。不過後來郤出現了五卷或六卷本，這是由於後人根據原書各志的長短，重加編排，內容則沒有明顯的增補修改的跡象。\[6\]

### 有關[華僑歷史的內容](../Page/華僑.md "wikilink")

  - [明](../Page/明.md "wikilink")[清之際](../Page/清.md "wikilink")[華人投奔](../Page/華人.md "wikilink")[越南的情況](../Page/越南.md "wikilink")：17世紀正值是[中國](../Page/中國.md "wikilink")[明](../Page/明.md "wikilink")[清政權易手](../Page/清.md "wikilink")，所以大批[漢人移居外地](../Page/漢人.md "wikilink")，當中一部份來到[越南南方](../Page/越南.md "wikilink")，《嘉定城通志》亦有所提及，如《疆域志》記載1679年，[明朝](../Page/明朝.md "wikilink")[廣東省鎮守龍門水陸等處地方總兵官](../Page/廣東省.md "wikilink")[楊彥迪](../Page/楊彥迪.md "wikilink")、副將[黃進等](../Page/黃進.md "wikilink")，帶領士卒及家眷三千餘人、戰船五十餘艘，南投[越南](../Page/越南.md "wikilink")[阮主所轄的](../Page/阮主.md "wikilink")[思容](../Page/思容.md "wikilink")、[峴港等地](../Page/峴港.md "wikilink")，原因是「不肯臣事[大清](../Page/大清.md "wikilink")，南來投誠，願為臣僕。」\[7\]另外又記載[廣東](../Page/廣東.md "wikilink")[雷州人](../Page/雷州.md "wikilink")[鄚玖因](../Page/鄚玖.md "wikilink")「[明亡](../Page/明.md "wikilink")，不服[大清初政](../Page/大清.md "wikilink")，留髮南投於[高蠻國](../Page/柬埔寨.md "wikilink")[南榮府](../Page/南榮府.md "wikilink")」，並成為了日後[河仙的實際統治者](../Page/河仙.md "wikilink")。\[8\]這些記載，反映了17世紀[中國人因逃避](../Page/中國人.md "wikilink")[滿清而掀起移民潮](../Page/滿清.md "wikilink")，大批[華僑去國南下的情景](../Page/華僑.md "wikilink")。
  - [華僑聚居點出現的情況](../Page/華僑.md "wikilink")：大批[中國人的南移](../Page/中國人.md "wikilink")[越南或到這裡經商](../Page/越南.md "wikilink")，形成了一些[華僑聚居點](../Page/華僑.md "wikilink")，這在《嘉定城通志》裡亦有所介紹，如[邊和鎮便在](../Page/邊和鎮.md "wikilink")[華僑領袖](../Page/華僑.md "wikilink")[陳上川的策劃下](../Page/陳上川.md "wikilink")「招致[唐商](../Page/中國.md "wikilink")，營建鋪街。瓦屋粉牆，岑樓層觀，炫江耀目，聯絡五里。」\[9\]又例如《風俗志》裡記載[藩安鎮的](../Page/藩安鎮.md "wikilink")「文物、服舍、器用多與[中國同](../Page/中國.md "wikilink")」\[10\]，描寫了[華僑在](../Page/華僑.md "wikilink")[越南南部的聚居情況及其所帶來的文化](../Page/越南.md "wikilink")、風俗上的影響。

## 史料價值

據學者[戴可來所說](../Page/戴可來.md "wikilink")，《嘉定城通志》詳細記載了[嘉定城](../Page/嘉定城.md "wikilink")、[藩安鎮](../Page/藩安鎮.md "wikilink")、[邊和鎮](../Page/邊和鎮.md "wikilink")、[定祥鎮](../Page/定祥鎮.md "wikilink")、[永清鎮及](../Page/永清鎮.md "wikilink")[河仙鎮的建置沿革](../Page/河仙鎮.md "wikilink")、疆城變遷、山川河流、城池貿易、風俗物產和[華僑事跡等方面的情況](../Page/華僑.md "wikilink")，成為17世紀至19世紀初有關[南圻史實的最完備撰述](../Page/南圻.md "wikilink")，是後人研究這個時期該地區歷史、地理、經濟、文化及[華僑問題的最重要史料](../Page/華僑.md "wikilink")。

除了學術上的價值外，該書還有實際用途。在明命朝以後，凡赴[南圻任要職的](../Page/南圻.md "wikilink")[越南官員](../Page/越南.md "wikilink")，都要先在蒞任前先讀此書，因而成為治理[越南南方的重要參考書](../Page/越南.md "wikilink")。此外，[阮朝官修的](../Page/阮朝.md "wikilink")《[大南實錄](../Page/大南實錄.md "wikilink")》、《[大南一統志](../Page/大南一統志.md "wikilink")》等文獻，都以此書為重要根據。甚至到[法國人在](../Page/法國人.md "wikilink")1862年佔領[南圻的](../Page/南圻.md "wikilink")[嘉定城](../Page/嘉定城.md "wikilink")、[邊和鎮及](../Page/邊和鎮.md "wikilink")[定祥鎮後](../Page/定祥鎮.md "wikilink")，這部書仍被十分重視，並被介紹到[法國去](../Page/法國.md "wikilink")。\[11\]

## 流傳情況

《嘉定城通志》面世後，到1863年，[法國人](../Page/法國人.md "wikilink")[鄂伯黎](../Page/鄂伯黎.md "wikilink")（G·Aubaret）譯成[法文](../Page/法文.md "wikilink")，帶到[巴黎出版](../Page/巴黎.md "wikilink")，受到西方學術界的重視。在[中國大陸](../Page/中國大陸.md "wikilink")，[鄭州的](../Page/鄭州.md "wikilink")[中州古籍出版社於](../Page/中州古籍出版社.md "wikilink")1991年，將《嘉定城通志》、《[嶺南摭怪](../Page/嶺南摭怪.md "wikilink")》及《[河仙鎮葉鎮鄚氏家譜](../Page/河仙鎮葉鎮鄚氏家譜.md "wikilink")》三種[越南史料組成一部](../Page/越南.md "wikilink")，由學者[戴可來](../Page/戴可來.md "wikilink")、[楊保筠校注](../Page/楊保筠.md "wikilink")，編成左起橫排的簡體中文版。\[12\]

## 注釋

## 參考文獻

<div class="references-small">

  -
  -

</div>

[Category:地方志](../Category/地方志.md "wikilink")
[Category:阮朝](../Category/阮朝.md "wikilink")
[Category:越南漢文典籍](../Category/越南漢文典籍.md "wikilink")
[Category:越南古代史書](../Category/越南古代史書.md "wikilink")
[Category:阮朝典籍](../Category/阮朝典籍.md "wikilink")
[Category:越南史書](../Category/越南史書.md "wikilink")

1.  《東南亞歷史詞典·「嘉定通志」條》，442頁。
2.  戴可來《<嘉定通志>、<鄚氏家譜>中所見17\~19世紀初葉的南圻華僑史跡》，附錄於《嶺南摭怪等史料三種》，284頁。
3.  原文載於《大南實錄·正編·第二紀》卷三庚辰明命元年（1820年）五月條，轉引自戴可來《<嘉定通志>、<鄚氏家譜>中所見17\~19世紀初葉的南圻華僑史跡》，附錄於《嶺南摭怪等史料三種》，287頁。
4.  戴可來《<嘉定通志>、<鄚氏家譜>中所見17\~19世紀初葉的南圻華僑史跡》，附錄於《嶺南摭怪等史料三種》，287頁。
5.  戴可來《<嘉定通志>、<鄚氏家譜>中所見17\~19世紀初葉的南圻華僑史跡》，附錄於《嶺南摭怪等史料三種》，287-288頁。
6.  戴可來《<嘉定通志>、<鄚氏家譜>中所見17\~19世紀初葉的南圻華僑史跡》，附錄於《嶺南摭怪等史料三種》， 288頁。
7.  鄭懷德《嘉定城通志·疆域志》，收錄於《嶺南摭怪等史料三種》，121頁。
8.  鄭懷德《嘉定城通志·疆域志·河仙鎮》，收錄於《嶺南摭怪等史料三種》，151頁。
9.  鄭懷德《嘉定城通志·城池志·邊和鎮》，收錄於《嶺南摭怪等史料三種》，219頁。
10. 鄭懷德《嘉定城通志·風俗志·五鎮風俗》，收錄於《嶺南摭怪等史料三種》，180頁。
11. 戴可來《<嘉定通志>、<鄚氏家譜>中所見17\~19世紀初葉的南圻華僑史跡》，附錄於《嶺南摭怪等史料三種》，286頁。
12. 戴可來《嶺南摭怪等史料三種》「前言」，1-4頁。