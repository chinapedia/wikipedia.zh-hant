[Shibuya_109_Building_Tokyo_January_2006.jpg](https://zh.wikipedia.org/wiki/File:Shibuya_109_Building_Tokyo_January_2006.jpg "fig:Shibuya_109_Building_Tokyo_January_2006.jpg")作為舞台，特別是澀谷交叉口附近一帶\]\]
[Onomichi_Channel03s3200.jpg](https://zh.wikipedia.org/wiki/File:Onomichi_Channel03s3200.jpg "fig:Onomichi_Channel03s3200.jpg")\]\]
《**CHAOS;HEAD**》（標誌上寫作**Chäos;HEAd**）是[5pb.原案](../Page/5pb..md "wikilink")、[Nitro+於](../Page/Nitro+.md "wikilink")2008年4月25日发售的[Windows用电脑游戏](../Page/Windows.md "wikilink")。為[妄想科學ADV系列的首部作品](../Page/妄想科學ADV系列.md "wikilink")。游戏的类型是“[妄想科学小说](../Page/冒險遊戲.md "wikilink")”，推荐对象是15岁以上。企劃初期時的公布的遊戲名稱為「」，更早期沒有正式公佈的是「哀SWORD」和「C.O.D.E.」\[1\]。其後發展出[漫畫](../Page/漫畫.md "wikilink")、[動畫等](../Page/動畫.md "wikilink")，並移植遊戲至[Xbox
360](../Page/Xbox_360.md "wikilink")，標題為「」，2009年2月26日發售，因為折原梢线过于血腥的情节以及画面，被分級為[CERO](../Page/CERO.md "wikilink")
Z（推薦18歲以上）。PSP版移植时对折原梢线的画面表现加以抑制，分级降为D。iOS版为PSP版的移植。

2010年3月25日在[Xbox 360平台上推出续作CHAOS](../Page/Xbox_360.md "wikilink");HEAD
らぶChu☆Chu\!，2011年1月27日CHAOS;HEAD
らぶChu☆Chu\!经移植后在[PSP平台发售](../Page/PSP.md "wikilink")。

## 剧情简介

高中私立翠明學園二年级学生的西条拓巳，自称对三次元没有兴趣，和家里的大量美少女人偶一起生活。在他所住的涩谷在2008年9月7日以來发生了连续猎奇杀人事件「」，成为了网络和电视上的热门话题。

2008年9月28日深夜，拓巳在聊天室遇到了一个自称“将军”的人。“将军”发言中的URL链接到像是预言下一个猎奇事件的恐怖图像。

第二天，拓巳遇到了和图像中的手法一样的杀人现场。在尸体前站着浑身是血的少女，咲畑梨深。

## 登场人物

※遊戲與動畫聲優一樣

  -
    生日：6月21日，身高：170cm，體重：54kg，血型：AB型。
    本作主人公，对三次元的女性没有兴趣，重度[御宅族](../Page/御宅族.md "wikilink")，使用網名是「」（在後作[ROBOTICS;NOTES有再度出現過](../Page/ROBOTICS;NOTES.md "wikilink")）。一星期只維持上學最低要求的2.5天，大部分時間都在家中看[動畫和玩](../Page/動畫.md "wikilink")[網絡遊戲](../Page/網絡遊戲.md "wikilink")。不擅和網絡外的人溝通，特別是女性。頭腦聰明，學校測驗經常獲得高分數。不是和家人同居，住在澀谷區[神泉町](../Page/神泉町.md "wikilink")，父親管理的大厦KURENAI的頂樓中的一個鐵皮箱中。從小就覺得有種「神的視線」在望著他，拓巳在這時多會說「」，但從來沒有向其他人說過，只在年幼時的作文提及過有關字句，而該作文的背面莫名的畫了奇怪機器（實為Noah2的造型）的塗鴉與不明公式「fun^10xint^40=Ir2」。
    實際上這個拓巳是被將軍（拓巳本體）妄想出來的人物，在一年半前被創造出來，代替真的西條拓巳在外生活，其過去的記憶皆是繼承將軍。由於本身是妄想的存在，所以可以讓物理攻擊無效化，且擁有將軍大半的力量，甚至可窺測他人思維。將軍創造這個妄想拓巳的目的主要是讓他可以替自己破壞即將運作的**Noah2**，同時也是替已病入膏肓的自己製造活下去的未來。最後妄想拓巳將**Noah2**成功破壞後被將軍託付所有的記憶與力量，成了真的西條拓巳。

<!-- end list -->

  - ;

      -
        拓巳在聊天室中遇到的謎之人物。在聊天室傳送了十多個恐怖圖像的連結，並說出「fun^10xint^40=Ir2」的不明公式和拓巳從未向人說過的「」。
        事實上是拓巳的真身，是所有giga-lo-maniac中能力最強大者，拓巳年幼的記憶和親朋好友關係全源自將軍。十歲時真的拓巳身體突然停止成長，並出現老化症的現象，由於越使用giga-lo-maniac的能力症狀便越嚴重，因此推測是此能力便是病因，估計壽命剩不到幾年。一年半前創造出妄想拓巳代替自己生活，並暗中賦予摧毀**Noah2**的任務，但因這是使用giga-lo-maniac的力量之故，所以會加速病症減短自己的壽命。最後把所有的記憶與力量轉移至妄想拓巳後去世。

<!-- end list -->

  -
    生日：7月3日，身高：158cm，體重：46kg，三圍：84-58-86，血型：B型。
    本作主要女主角，突然出现在拓巳面前的謎之少女，被拓巳稱作「惡魔女」。口頭禪是「」。

<!-- end list -->

  -
    生日：2月17日，身高：152cm，體重：42kg，三圍：77-54-80，血型：B型。
    本作女主角之一，拓巳的妹妹。小拓巳一岁，讀同一所學校。經常逞強，但內心很擔心哥哥，稱主角為「」。

<!-- end list -->

  -
    生日：1月29日，身高：168cm，體重：49kg，三圍：81-56-82，血型：O型。
    本作女主角之一，主角的同级生。人气乐团的歌手，藝名是「FES」，寫的歌詞經常和近來的殺人事件有關，因此曾被警察追捕。持有一把叫「Di-Sword」（）的劍的擁有者之一。

<!-- end list -->

  -
    生日：3月8日，身高：162cm，體重：46kg，三圍：75-54-79，血型：AB型。
    本作女主角之一，拓巳的学姐，但是不常上学。經常在澀谷徘徊，喜歡吃冰棒「」。持有一把叫「Di-Sword」（）的長劍的擁有者之一。

<!-- end list -->

  -
    生日：10月18日，身高：159cm，體重：53kg，三圍：87-60-90，血型：A型。
    本作女主角之一，对众人都很温柔的学姐，即使对后辈也使用敬语。喜歡動畫和漫畫。

<!-- end list -->

  -
    生日：11月13日，身高：149cm，體重：40kg，三圍：73-51-75，血型：O型。
    本作女主角之一，主角的同级生，剛轉校。性格冒失，易哭，少說話。自稱「」。故鄉是[廣島縣](../Page/廣島縣.md "wikilink")[尾道市](../Page/尾道市.md "wikilink")，持有一把叫「Di-Sword」（）的劍的擁有者之一。

<!-- end list -->

  -
    生日：5月22日，身高：156cm，體重：42kg，三圍：88-56-87，血型：B型。
    拓巳喜欢的动画《》中的女主角。流星學園2年級，以「☆之力」可變身為[魔法少女](../Page/魔法少女.md "wikilink")，在第12話進入覺醒型態。口頭禪是「」。

<!-- end list -->

  -
    主角的同学。是个美男子，很受女性欢迎，但經常和自己形象完全不同的御宅族的拓巳談話。

<!-- end list -->

  -
    警視庁刑事部捜査一課所属。警部補。說話表面看來不認真。調查**新世代的瘋狂**事件的過程中逐漸察覺到**希望Technology
    Group公司**的陰謀與**giga-lo-maniac**的存在，但還沒發現Noah2就先被諏訪開槍暗殺。

<!-- end list -->

  -
    警視庁刑事部捜査一課所属。巡査部長。非常喜歡[電影](../Page/電影.md "wikilink")，特別是《》。
    實際上是**希望Technology
    Group公司**派來的內奸，觀察警方是否察覺到他們的計謀。是**新世代的瘋狂**事件的真凶之一。最終在執行捕獲西條拓巳的過程中被拓巳以反粒子干涉妄想而打敗。

<!-- end list -->

  -
    調查專門的公司社長，和判是舊交。

<!-- end list -->

  -
    精神科醫生，以前是主角的主診醫生。最終被葉月志乃以湯匙挖出腦部而亡，成了**新世代的瘋狂**第五起事件的受害者。

<!-- end list -->

  -
    高科屬下的護士，使用網名叫「Grim」，常與拓巳談**新世代的瘋狂**的事情。
    天成神光會信徒。是**新世代的瘋狂**事件的真凶之一，最後為引發最終實驗（Third Melt）而自刎。

<!-- end list -->

  -
    露宿者，經常拿著寫有不明意思語句的紙板躺在街上。
    曾是**希望Technology
    Group公司**開發**Noah2**的研究員，也是蒼井瀨名的生父。因**Noah2**的開發過程使自己的妻女都被野呂瀨社長強迫成試驗品又無能阻止而大感慚愧。最終在諏訪追殺瀨名的過程中替她擋子彈而亡。

<!-- end list -->

  -
    **希望Technology
    Group公司**（NOZOMI）的社長。**Noah2**開發計劃的主謀，可以為目標而不擇手段犧牲必要的人。察覺到人類的欲望將最終自取滅亡的危機，才會想藉由**Noah2**的力量改寫所有人的人格，但另一個角度來想也是讓自己成為神的存在。

<!-- end list -->

  -
    天成神光會教祖。為了教會興盛而與野呂瀨社長合作開發**Noah2**，讓自己大量的信眾成為測試人員。最終遭野呂瀨背叛與豬鼻在妄想中對殺而死。

<!-- end list -->

  -
    眾議院議員，屬明和黨。為了政黨支持度而與野呂瀨社長合作開發**Noah2**，調度大量的開發資金。最終遭野呂瀨背叛與倉持在妄想中對殺而死。

## 用語及設定

  -
    簡稱「」，又或再簡稱「乳毛」。指2008年9月7日以來在澀谷發生的連串極奇血腥的殺人事件。9月7日是五人高中生集體跳樓，網上有人認為是被謀殺。9月19日是一男性被剖開腹部並被放入懷孕32週的嬰兒。9月29日是一大學教授被施[十字架刑](../Page/十字架.md "wikilink")。其後仍然不斷發生新事件。
  - 妄想
    在腦中浮現的想法。
  -
    可以干涉[狄拉克之海](../Page/狄拉克之海.md "wikilink")，把妄想變成現實的人，在極端的情感波動下有可能這能力會因此覺醒。giga-lo-maniac是比[誇大狂患者](../Page/誇大狂.md "wikilink")（Megalomaniac）程度更強的意思。
    本作一共登場八位giga-lo-maniac，分別是西條拓巳、咲畑梨深、西條七海、岸本綾瀨、蒼井瀨名、楠優愛、折原梢和野呂瀨玄一。
  -
    透過[盲點讓周圍的人認知妄想](../Page/盲點_\(眼\).md "wikilink")，把這妄想變成現實。
  -
    來自狄拉克之海、把妄想變成現實的捷徑，只有giga-lo-maniac看到的時候是發出藍光的劍，Real boot後會再發出紅光。
  -
    將giga-lo-maniac利用公式「fun^10xint^40=Ir2」具現化的機器，由希望Technology
    Group公司與天成神光會和明和黨聯手測試研發。由一台主機進行運算，再由終端機「Porter」釋放[電磁波改寫讓周圍的人認知](../Page/電磁波.md "wikilink")，可說是人造的giga-lo-maniac。擁有自我保護能力，只有野呂瀨玄一能接近並破壞主機。其設計似乎是[永動機](../Page/永動機.md "wikilink")，因此無法破壞以外的方式停止運作。

## 音樂

  - 開頭歌曲
    ;「Find the blue」

      -
        歌：[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
        作詞、作曲：[志倉千代丸](../Page/志倉千代丸.md "wikilink")，編曲：[磯江俊道](../Page/磯江俊道.md "wikilink")

  - 結尾歌曲
    ;「Desire Blue sky」

      -
        歌：[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
        作詞：[江幡育子](../Page/江幡育子.md "wikilink")，作曲、編曲：磯江俊道

  - ;「」

      -
        歌：[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")
        作詞：江幡育子，作曲、編曲：磯江俊道

  - ;「」

      -
        歌：[榊原由依](../Page/榊原由依.md "wikilink")
        作詞：[志倉千代丸](../Page/志倉千代丸.md "wikilink")，作曲、編曲：[林達志](../Page/林達志.md "wikilink")

  - 插入曲
    ;「」

      -
        歌：榊原由依
        作詞：[志倉千代丸](../Page/志倉千代丸.md "wikilink")，作曲、編曲：林達志

## 電視動畫

最初於2008年7月26日發售的《月刊Comic電擊大王》9月號公布，預定同年10月播放。

每話的預告最後，都會有人說「」。

### 工作人員

  - 原作：5pb.、Nitroplus、RED FLAGSHIP
  - 監督：
  - 系列構成、劇本：[井上敏樹](../Page/井上敏樹.md "wikilink")
  - 人物原案：
  - 人物設定：[島村秀一](../Page/島村秀一.md "wikilink")
  - 音響監督：中嶋聰彥
  - 音響制作：[Techno Sound](../Page/Techno_Sound.md "wikilink")
  - 音樂：[tOkyO](../Page/tOkyO.md "wikilink")
  - 動畫製作：[Madhouse](../Page/Madhouse.md "wikilink")
  - 製作：CHAOS;HEAD製作委員會

### 主題曲

  - 片頭曲
    ;「F.D.D.」

      -
        作詞、作曲：志倉千代丸
        編曲：江幡育子
        歌：[伊藤香奈子](../Page/伊藤香奈子.md "wikilink")

  - 片尾曲
    ;「Super Special」

      -
        作詞：
        作曲：駒場雅信、Koichi MAKAI
        編曲：Takashi I kezawa
        歌：

  - 插入曲
    ;「」(第三話)

      -
        作詞：志倉千代丸
        作詞：林達志
        歌：榊原由依

  - ;「」

      -
        作詞：志倉千代丸
        作詞：林達志
        歌：榊原由依

### 各集資料

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>標題</p></th>
<th><p>劇本</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>起動<br />
boot up</p></td>
<td><p><a href="../Page/井上敏樹.md" title="wikilink">井上敏樹</a></p></td>
<td><p>宍戸淳</p></td>
<td><p>大谷肇</p></td>
<td><p>大塚美登理<br />
渡邊和夫<br />
小堺能夫</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>自我<br />
ego</p></td>
<td><p><a href="../Page/石山貴明.md" title="wikilink">石山貴明</a></p></td>
<td><p>臼井文明</p></td>
<td><p>PARK KI DUG<br />
LEE SI MIN</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>接觸<br />
contact</p></td>
<td><p>川畑喬</p></td>
<td><p>高木晴美<br />
小林多加志</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>初動<br />
commencing</p></td>
<td><p>堀之内元</p></td>
<td><p>川久保圭史</p></td>
<td><p>玉井公子<br />
LEE SI MIN<br />
KWON YONG SANG</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>先導<br />
guidance</p></td>
<td><p>南雲福蔵</p></td>
<td><p>高木秀文</p></td>
<td><p>PARK KI DUG</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>抱擁<br />
embracement</p></td>
<td><p>石山貴明</p></td>
<td><p>草野新二郎</p></td>
<td><p>LEE SI MIN<br />
KWON YONG SANG</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>自覺<br />
realization</p></td>
<td><p>森田浩光</p></td>
<td><p>臼井文明</p></td>
<td><p>玉井公子<br />
大塚美登理<br />
渡边和夫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>連動<br />
linkage</p></td>
<td><p>吉川博明</p></td>
<td><p>小野田雄亮</p></td>
<td><p>PARK KI DUG</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>拒絶<br />
rejection</p></td>
<td><p>石山贵明</p></td>
<td><p><a href="../Page/石踊宏.md" title="wikilink">石踊宏</a></p></td>
<td><p>川島尚</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>洗禮<br />
purification</p></td>
<td><p>森田浩光</p></td>
<td><p>川久保圭史</p></td>
<td><p>玉井公子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>自立<br />
independence</p></td>
<td><p>森田浩光</p></td>
<td><p>高木秀文<br />
草野新二郎</p></td>
<td><p>玉井公子<br />
大塚美登理</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>使命<br />
mission</p></td>
<td><p>石山貴明</p></td>
<td><p>石山貴明<br />
川久保圭史<br />
草野新二郎</p></td>
<td><p>大塚美登理<br />
玉井公子<br />
渡邊和夫<br />
LEE SI MIN<br />
KWON YONG SANG</p></td>
<td></td>
</tr>
</tbody>
</table>

### 播放电视台

| 播放地域                               | 电视台                                                | 播放日期                      | 播放时间                | 備注                                           |
| ---------------------------------- | -------------------------------------------------- | ------------------------- | ------------------- | -------------------------------------------- |
| [千葉县](../Page/千葉县.md "wikilink")   | [千葉電視台](../Page/千葉電視台.md "wikilink")               | 2008年10月9日 - 12月25日       | 星期四 23時00分 - 23時30分 | [独立UHF系](../Page/全国独立UHF放送協議会.md "wikilink") |
| [埼玉县](../Page/埼玉县.md "wikilink")   | [埼玉電視台](../Page/埼玉電視台.md "wikilink")               | 星期四 25時30分 - 26時00分       |                     |                                              |
| [神奈川县](../Page/神奈川县.md "wikilink") | [神奈川電視台](../Page/神奈川電視台.md "wikilink")             | 2008年10月11日-12月27日        | 星期六 25時30分 - 26時00分 |                                              |
| 全日本                                | [Kids Station](../Page/Kids_Station.md "wikilink") | |2008年10月14日 - 2009年1月6日  | 星期二 24時00分 - 24時30分 | CS放送                                         |
| [東京都](../Page/東京都.md "wikilink")   | [東京都會電視台](../Page/東京都會電視台.md "wikilink")           | 2008年12月12日 - 2009年2月20日  | 星期二 26時00分 - 26時30分 | 独立UHF系                                       |
| 日本全国                               | [ShowTime](../Page/ShowTime.md "wikilink")         | |2008年12月12日 - 2009年2月20日 | 星期五 18時00分更新        | [網路電視](../Page/網路電視.md "wikilink")           |
| [Gyao](../Page/Gyao.md "wikilink") | |2009年1月18日 - 4月6日                                 | 星期日 12時00分更新              |                     |                                              |

## 漫畫

  - Chaos;HEAd
    於2008年5月21日開始於《[月刊Comic電擊大王](../Page/月刊Comic電擊大王.md "wikilink")》2008年7月號連載，作畫。
  - CHAOS;HEAD～BLUE COMPLEX～
    於2008年9月26日開始於《[月刊Comic
    Alive](../Page/月刊Comic_Alive.md "wikilink")》2008年11月號連載，[沙垣長子作畫](../Page/沙垣長子.md "wikilink")。
  -
    於2008年9月27日開始於《[月刊Comic
    Rush](../Page/月刊Comic_Rush.md "wikilink")》2008年11月號連載，作畫。故事並非基於原作，而是愛情喜劇。

## 廣播

「Chaos;HEAd廣播
妄想電波局」從2008年3月28日至5月30日由[音泉發放](../Page/音泉.md "wikilink")，逢星期五更新，共10回，由[吉野裕行](../Page/吉野裕行.md "wikilink")、[喜多村英梨主持](../Page/喜多村英梨.md "wikilink")。

## 小說

  - Blood Tune THE NOVELIZATION
    2008年11月5日發售的衍生小說《Blood Tune THE
    NOVELIZATION》，[絲井健一著](../Page/絲井健一.md "wikilink")，插畫，出版。
  -
    2008年12月發售的小說《》，絲井健一著，[小形聖史插畫](../Page/小形聖史.md "wikilink")，出版，ISBN
    978-4-434-12310-8。

## 关联条目

  - [Steins;Gate](../Page/Steins;Gate.md "wikilink")（以本作之后的世界为舞台的作品）
  - [ROBOTICS;NOTES](../Page/ROBOTICS;NOTES.md "wikilink")（以Steins;Gate之后的世界为舞台的作品）

## 参考資料

<div class="references-small">

<references />

  -
  -

</div>

## 外部链接

  - [遊戲官方网站](https://web.archive.org/web/20080415224315/http://www.nitroplus.co.jp/pc/lineup/into_15/html/)
  - [動畫官方網站](https://web.archive.org/web/20080727001528/http://www.chaoshead.jp/)
  - [Xbox 360版官方網站](http://5pb.jp/games/chaoshead/)（年龄认证 需18岁以上）
  - [Nitro+](https://web.archive.org/web/20090110114236/http://www.nitroplus.co.jp/pc/)
  - [CHAOS;HEAD廣播
    妄想電波局](https://web.archive.org/web/20080517050927/http://www.onsen.ag/popup/mousou/index.html)
  - [音泉](http://www.onsen.ag/)

[Category:Nitro+](../Category/Nitro+.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink") [Category:Xbox
360遊戲](../Category/Xbox_360遊戲.md "wikilink")
[Category:恐怖遊戲](../Category/恐怖遊戲.md "wikilink")
[Category:恐怖動畫](../Category/恐怖動畫.md "wikilink")
[Category:冒險遊戲](../Category/冒險遊戲.md "wikilink")
[Category:2008年电子游戏](../Category/2008年电子游戏.md "wikilink")
[Category:2008年UHF動畫](../Category/2008年UHF動畫.md "wikilink")
[Category:動畫廣播](../Category/動畫廣播.md "wikilink")
[Category:陰謀論題材作品](../Category/陰謀論題材作品.md "wikilink")
[Category:網路題材作品](../Category/網路題材作品.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:月刊Comic Alive](../Category/月刊Comic_Alive.md "wikilink")
[Category:月刊Comic電擊大王連載作品](../Category/月刊Comic電擊大王連載作品.md "wikilink")
[Category:澀谷背景作品](../Category/澀谷背景作品.md "wikilink")
[Category:科學ADV系列](../Category/科學ADV系列.md "wikilink")
[Category:隱蔽青年題材作品](../Category/隱蔽青年題材作品.md "wikilink")
[Category:IPad遊戲](../Category/IPad遊戲.md "wikilink")
[Category:PlayStation 3游戏](../Category/PlayStation_3游戏.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")

1.