**偉鱷獸屬**（學名：**）意為“巨型鱷魚”，屬於[獸孔目](../Page/獸孔目.md "wikilink")[恐頭獸亞目](../Page/恐頭獸亞目.md "wikilink")，但並非[鱷魚](../Page/鱷魚.md "wikilink")，生存於[二疊紀中期的](../Page/二疊紀.md "wikilink")[南非](../Page/南非.md "wikilink")。偉鱷獸的身長估計可達2.5公尺\[1\]。

偉鱷獸與[姜氏獸](../Page/姜氏獸.md "wikilink")、[麝足獸是近親](../Page/麝足獸.md "wikilink")，牠們都生存於晚[二疊紀的](../Page/二疊紀.md "wikilink")[南非](../Page/南非.md "wikilink")，約2億6500萬年前。偉鱷獸是[肉食性動物](../Page/肉食性.md "wikilink")，牠們可能以姜氏獸、麝足獸、以及其他[脊椎動物為食](../Page/脊椎動物.md "wikilink")。偉鱷獸有銳利的[門齒以及](../Page/門齒.md "wikilink")[犬齒](../Page/犬齒.md "wikilink")，相當適合咬獵物\[2\]。

[巨型獸的體型相當於偉鱷獸](../Page/巨型獸.md "wikilink")，也是[肉食性](../Page/肉食性.md "wikilink")[恐頭獸類](../Page/恐頭獸類.md "wikilink")，但生存於[俄羅斯](../Page/俄羅斯.md "wikilink")。偉鱷獸與[始巨鱷雖然都是](../Page/始巨鱷.md "wikilink")[獸孔目](../Page/獸孔目.md "wikilink")，但其實關係很遠。

## 參考資料

[Category:恐頭獸亞目](../Category/恐頭獸亞目.md "wikilink")
[Category:二疊紀合弓類](../Category/二疊紀合弓類.md "wikilink")

1.  <http://palaeos.com/vertebrates/therapsida/tapinocephalia.html>
2.