**成文法**（），又稱**制定法**，是指由各国的[立法机关根据](../Page/立法机关.md "wikilink")[宪法的授权](../Page/宪法.md "wikilink")，按照一定的[立法程序制訂而具有普遍效力的法律条文](../Page/立法程序.md "wikilink")。成文法例較[不成文法條文清楚](../Page/不成文法.md "wikilink")、考量立法環境、時移世易且經過討論而制定，也可改變[普通法及其](../Page/普通法.md "wikilink")[衡平法所定立的準則](../Page/衡平法.md "wikilink")，以便更適合環境。

## Act或Law

經國家立法機關通過[法案生效的成文法](../Page/法案.md "wikilink")，英美國家稱「Act」，在一般非法律英漢辭典混同「法案」釋義，香港則沿襲[港英時代習慣譯成](../Page/港英時代.md "wikilink")「[法令](../Page/法令.md "wikilink")」\[1\]。而中華人民共和國官方英譯「Law」\[2\]\[3\]，[中華民國自由地區舉凡法](../Page/中華民國自由地區.md "wikilink")、律、條例、通則等經[立法院通過](../Page/立法院.md "wikilink")、[總統公布之法律皆譯](../Page/中華民國總統.md "wikilink")「Act」\[4\]。

## 參考資料

## 相關條目

  - [不成文法](../Page/不成文法.md "wikilink")
  - [法規](../Page/法規.md "wikilink")
  - [習慣法](../Page/習慣法.md "wikilink")
  - [判例法](../Page/判例法.md "wikilink")
  - [欧陆法系](../Page/欧陆法系.md "wikilink")
  - [法律體系](../Page/法律體系.md "wikilink")
  - [十二铜表法](../Page/十二铜表法.md "wikilink")
  - [英国政治](../Page/英国政治.md "wikilink")
  - [民法典](../Page/民法典.md "wikilink")
  - [大清律例](../Page/大清律例.md "wikilink")
  - [行省 (法国)](../Page/行省_\(法国\).md "wikilink")
  - [香港法例](../Page/香港法例.md "wikilink")
  - [逆權侵佔](../Page/逆權侵佔.md "wikilink")
  - [僱傭條例 (香港法例第57章)](../Page/僱傭條例_\(香港法例第57章\).md "wikilink")

## 外部參考

  - [宋飞:
    制定法、成文法概念的比较研究](http://www.law-lib.com.cn/lw/lw_view.asp?no=6427)
  - [Parliamentary Fact
    Sheets](http://www.parliament.uk/about/how/guides/factsheets/legislation/)
    United Kingdom

[Category:法系](../Category/法系.md "wikilink")
[成文法](../Category/成文法.md "wikilink")

1.  [香港法例第88章《英國法律應用條例》第2條
    釋義](http://www.blis.gov.hk/blis_ind.nsf/AllVerAllChinDoc/836DFB10ED2B017F482565A600084DA3)
2.
3.
4.