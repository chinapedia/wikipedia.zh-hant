**提姆·韦克菲尔德**（**Timothy Stephen "Tim" Wakefield**、)
是[美國](../Page/美國.md "wikilink")[職棒大聯盟球員](../Page/美國職棒大聯盟.md "wikilink")。韦克菲尔德是位[先發投手](../Page/先發投手.md "wikilink")，曾效力[波士頓紅襪隊](../Page/波士頓紅襪.md "wikilink")。

在參與[美國](../Page/美國.md "wikilink")[職棒大聯盟](../Page/美國職棒大聯盟.md "wikilink")19個球季後，2012年2月17日，提姆·韦克菲尔德宣布退休。

## 職業生涯

### 匹茲堡海盜（1992-1993）

  - 韋克菲爾德在1992年升上大聯盟，成績為8勝1敗，防禦率2.15的佳績，帶領球隊獲得分區冠軍。
  - 1993年韋克菲爾德24場先發中拿下6勝11敗，防禦率5.61的不理想成績。1994年整年都待在小聯盟。1995在季前被球隊釋出，一個禮拜後加入波士頓紅襪。

### 波士頓紅襪（1995-2011）

  - 來到紅襪隊以後，韋克菲爾德和蝴蝶球名投手[Phil
    Niekro和](../Page/Phil_Niekro.md "wikilink")[Joe
    Niekro學習蝴蝶球](../Page/Joe_Niekro.md "wikilink")，而這一年獲得16勝8敗，防御率2.95的好成績，並且在[賽揚獎投票獲得第](../Page/賽揚獎.md "wikilink")3名。也因這一年的成功，使韋克菲爾德長期待在紅襪隊。2009年Wakefield首度入選明星賽。

## 專屬捕手

由於[傑森·瓦瑞泰克無法接](../Page/傑森·瓦瑞泰克.md "wikilink")[蝴蝶球](../Page/蝴蝶球.md "wikilink")，所以韋克菲爾德有自己的專屬捕手，2002年開始韋克菲爾德專屬捕手為[Doug
Mirabelli](../Page/Doug_Mirabelli.md "wikilink")\[1\]。Doug
Mirabelli在2005年季賽結束離開紅襪以後，由[喬什·巴德擔任韋克菲爾德的專屬捕手](../Page/喬什·巴德.md "wikilink")，但巴德在5場和韋克菲爾德的組合卻出現10次的[捕逸](../Page/捕逸.md "wikilink")，紅襪隊只好和[聖地牙哥教士隊交易回Mirabelli](../Page/聖地牙哥教士.md "wikilink")。2008年韋克菲爾德專屬捕手為[凱文·凱什](../Page/凱文·凱什.md "wikilink")。

## 特別成就

  - 2010年9月9日，以44歲又37天高齡，成為[波士頓紅襪隊史上最高齡拿勝投的投手](../Page/波士頓紅襪.md "wikilink")。

## 生涯成績

<table>
<thead>
<tr class="header">
<th></th>
<th><p>球季成績</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>球季</p></td>
<td><p>球隊</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/匹茲堡海盜.md" title="wikilink">匹茲堡海盜</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/匹茲堡海盜.md" title="wikilink">匹茲堡海盜</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td><p>17 年</p></td>
<td><p>合計</p></td>
</tr>
</tbody>
</table>

## 球種

  - 韋克菲爾德最有名的投球種類為蝴蝶球，偶爾搭配快速球，球速雖不快（約60-75
    mph），但卻常讓打者無法擊中球心而變飛球或滾地球出局\[2\]。

## 參考資料

## 外部連結

  -
[Category:美國棒球選手](../Category/美國棒球選手.md "wikilink")
[Category:匹茲堡海盜隊球員](../Category/匹茲堡海盜隊球員.md "wikilink")
[Category:波士頓紅襪隊球員](../Category/波士頓紅襪隊球員.md "wikilink")

1.  Ian Browne / MLB.com, "[Red Sox release catcher Mirabelli / Surprise
    move opens door for Cash to assume backup
    role](http://mlb.mlb.com/news/article.jsp?ymd=20080313&content_id=2423992&vkey=spt2008news&fext=.jsp&c_id=mlb),"
    *The Official Site of Major League Baseball*, March 13,
    2008.　2008年3月22日閲覧。
2.  FOXSports.com, "[Preview 2008: Boston Red
    Sox](http://msn.foxsports.com/mlb/story/7920362/Preview-2008:-Boston-Red-Sox)
    ," *FOX Sports on MSN*, March 18, 2008.　2008年3月22日閲覧。