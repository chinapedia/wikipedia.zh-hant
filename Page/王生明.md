**王生明**（）\[1\]\[2\]\[3\]\[4\]，字至誠，祖籍[湖南省](../Page/湖南省.md "wikilink")[祁陽縣](../Page/祁陽縣.md "wikilink")，[胡宗南舊部](../Page/胡宗南.md "wikilink")，[中華民國陸軍軍官](../Page/中華民國陸軍.md "wikilink")，於[一江山島戰役率領七百多名守軍奮勇抵抗](../Page/一江山島戰役.md "wikilink")[中国人民解放军三天](../Page/中国人民解放军.md "wikilink")，最後說了一句“共匪距離我50米，只剩下一顆[手榴弹给自己](../Page/手榴弹.md "wikilink")”\[5\]\[6\]，[追贈](../Page/追贈.md "wikilink")[少將](../Page/少將.md "wikilink")，獎金800元。

## 生平

### 早年

年幼時入讀[私塾兩年](../Page/私塾.md "wikilink")，15歲從軍，歷經大小戰役屢立戰功，自預備兵晉升至地區司令。

1927年，[國民革命軍](../Page/國民革命軍.md "wikilink")[北伐中](../Page/北伐.md "wikilink")[南京之战](../Page/南京.md "wikilink")，[孙传芳以](../Page/孙传芳.md "wikilink")[白俄傭兵和北伐军反复争夺](../Page/白俄.md "wikilink")[雨花台](../Page/雨花台.md "wikilink")，十七岁的王生明在学兵队中擔任班长生俘[白俄](../Page/白俄.md "wikilink")[傭兵两名](../Page/傭兵.md "wikilink")。後升任四十軍[少尉排長](../Page/少尉.md "wikilink")，旋即在[中原大戰中立功升任](../Page/中原大戰.md "wikilink")[中尉](../Page/中尉.md "wikilink")。

### 勦共

此後參加對[蘇區的圍剿](../Page/蘇區.md "wikilink")，在[紅軍損失最大的](../Page/中国工农红军.md "wikilink")[廣昌戰鬥中負傷](../Page/廣昌戰鬥.md "wikilink")。1935年，王生明率部包围了红军总政治部副主任[贺昌](../Page/贺昌.md "wikilink")，贺昌自戕。

### 抗日

1937年[對日抗戰](../Page/對日抗戰.md "wikilink")，王生明率部参加[淞滬戰役](../Page/淞滬戰役.md "wikilink")，死守[蕰藻浜](../Page/蕰藻浜.md "wikilink")，所部只有9人生還，此後王生明即受到[胡宗南的屢次提拔](../Page/胡宗南.md "wikilink")，先後参加[中條山戰役](../Page/中條山戰役.md "wikilink")，[朱仙镇戰役](../Page/朱仙镇.md "wikilink")，抗戰结束时已經升到[上校](../Page/上校.md "wikilink")。\[7\]

### 一江山

1949年[胡宗南所部敗退](../Page/胡宗南.md "wikilink")[西康](../Page/西康.md "wikilink")，王生明放棄駐防[臺灣第一九八師副師長的職務](../Page/臺灣.md "wikilink")，追隨胡宗南到[西康軍中擔任第一三五師副師長](../Page/西康.md "wikilink")，後和[羅列在西康打遊擊](../Page/羅列_\(軍人\).md "wikilink")，戰至彈盡援絕後化妝1950年2月輾轉返台。王生明入國防部政幹班受訓後，再度追隨化名秦東昌的胡宗南調任大陳防衛部一江山地區副司令，不久繼任南麂地區副司令。胡宗南將卅多股各自為政的游擊武力，整編為[反共救國軍六個大隊](../Page/反共救國軍.md "wikilink")，將船隻整編為海上突擊總隊。1953年8月，蔣中正總統決定增調正規軍到大陳島，派出陸軍第46師，並由留美的[劉廉一中將擔任大陳防衛司令](../Page/劉廉一.md "wikilink")，取代胡宗南。陸軍第46師防守大陳本島，救國軍駐守周邊的一江山、漁山、南麂島。劉廉一於1954年將王生明升任司令，同年10月，晉任一江山地區司令。

1955年元旦獲頒第五屆戰鬥英雄第一名，並由時任[總統](../Page/中華民國總統.md "wikilink")[蔣中正親自授獎](../Page/蔣中正.md "wikilink")。王生明知道守一江山島必死，命令部隊中夫妻、兄弟、父子必須撤退一人。

1月18日，[中國人民解放軍猛攻](../Page/中國人民解放軍.md "wikilink")[一江山島](../Page/一江山島.md "wikilink")，國軍七百二十人誓死堅守，奮戰三晝夜，殲敵二千餘，終至全部壯烈成仁\[8\]。蔣以陸軍上校王生明，於一江山之役，壯烈殉國，特予[追晉陸軍](../Page/追贈.md "wikilink")[少將](../Page/少將.md "wikilink")\[9\]。王生明身後由總統蔣中正親臨致祭，並入祀[忠烈祠](../Page/忠烈祠.md "wikilink")。

## 評價

王應文於紀念一江山戰役周年座談會說：[蔣緯國將軍曾向我提過](../Page/蔣緯國.md "wikilink")，在當年的軍事會議中，先總統蔣公對王生明將軍所率領官兵誓死捍衛一江山表達肯定，並對他們堅守陣地深表信心，因為「堅守一天，就可使台灣的人心振奮起來；守兩天，就能使大陸人心及中共對我們的看法不一樣；守三天，就把[白宮翻過來](../Page/白宮.md "wikilink")。」事後果然一如蔣公所預判，美國不但對我國大為改觀，而且國會快速通過中美共同防禦條約，這完全是一江山戰役國軍官兵壯烈犧牲所換來，他們對國家安全的貢獻非常深遠\[10\]。

## 紀念

  - [高雄市](../Page/高雄市.md "wikilink")[鳳山區](../Page/鳳山區.md "wikilink")[陸軍軍官學校附近有生明社區及王生明路](../Page/陸軍軍官學校.md "wikilink")。
  - 士林芝山岩的「至誠路」，是以王生明的字「至誠」命名。

## 注释

## 外部連結

  - [榮民文化網〈戰鬥英雄王生明〉](http://lov.vac.gov.tw/Protection/Content.aspx?Para=98&Control=5)

  - \[ 〈一江山精忠氣節
    半世紀輝耀古今〉\]，《[青年日報](../Page/青年日報.md "wikilink")》，2005年1月20日。

  - [〈紀念一江山戰役五十週年〉](http://epaper.vac.gov.tw/admin/web/content.php?mc_sn=8&ts_sn=483)，《榮光電子報》2079期。

  - [大陳浪
    淘盡半世紀恩仇](http://udn.com/NEWS/NATIONAL/NAT2/6140644.shtml)，《[聯合報](../Page/聯合報.md "wikilink")》，2011年2月9日。

  - [懷念父親王生明將軍](http://yikiangshan.blogspot.com/p/blog-page.html)

  - [1955年國共一江山戰役的新發現(下)](http://armourforceman.pixnet.net/blog/post/13687111)

  - [年輕人不知「一江山戰役」　烈士王生明之子王應文籲納入課本](http://www.ettoday.net/news/20150120/456089.htm)，《[東森新聞](../Page/東森新聞.md "wikilink")》，2015年1月20日。

[Category:中華民國陸軍少將](../Category/中華民國陸軍少將.md "wikilink")
[Category:中华民国战争身亡者](../Category/中华民国战争身亡者.md "wikilink")
[Category:祁阳人](../Category/祁阳人.md "wikilink")
[Category:王姓](../Category/王姓.md "wikilink")

1.  。值得注意的是，民前二年二月二十九于[中央研究院](https://sinocal.sinica.edu.tw)的查询结果却是1910年4月8日，而2009年3月25日就是农历二月二十九日。

2.

3.

4.

5.

6.  《108年1月第3週[莒光園地](../Page/莒光園地.md "wikilink")》

7.  [抗日名將錄](http://aode.mnd.gov.tw/Unit/Content/110?unitId=178)，全民國防教育網，[國防部政治作戰局](../Page/國防部政治作戰局.md "wikilink")

8.

9.
10.