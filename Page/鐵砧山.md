**鐵砧山**（**鐵砧山風景特定區**），位於[臺中市](../Page/臺中市.md "wikilink")[大甲區](../Page/大甲區.md "wikilink")、[外埔區一帶](../Page/外埔區.md "wikilink")，北臨[大安溪](../Page/大安溪.md "wikilink")，山頂地勢平坦，遠望形似一座鐵匠打鐵用的[鐵砧](../Page/砧板.md "wikilink")，因而得名。最高點位於[外埔區境內](../Page/外埔區.md "wikilink")，海拔236公尺，有一等三角點及47號三等三角點基石各一顆，為[台灣小百岳之一](../Page/台灣小百岳.md "wikilink")\[1\]。

鐵砧山昔為[軍事重地](../Page/軍事.md "wikilink")，經建設，已成[臺中海線地區觀光旅遊地點之一](../Page/大甲郡.md "wikilink")，山上之望海亭，可遠眺[外埔區](../Page/外埔區.md "wikilink")、大甲平原與[大安港](../Page/大安港.md "wikilink")，[黃昏時可望見海面上漁船點點](../Page/黃昏.md "wikilink")。

## 區內景點

  - [鎮瀾宮](../Page/鎮瀾宮.md "wikilink")
  - 台中軍人忠靈祠（國軍紀念公園）
  - 中正公園
  - 永信運動公園
  - 成功公園
  - 鄭成功紀念館
  - 大甲國姓廟
  - [鄭成功石像](../Page/鄭成功.md "wikilink")
  - [劍井](../Page/劍井.md "wikilink")
  - 桂花泉步道
  - 櫻花步道
  - 藝術大道

[File:劍井.jpg|鐵砧山劍井](File:劍井.jpg%7C鐵砧山劍井)
[File:大甲國姓廟.jpg|大甲](File:大甲國姓廟.jpg%7C大甲)[國姓廟](../Page/國姓廟.md "wikilink")
[File:大甲鄭成功像.jpg|大甲](File:大甲鄭成功像.jpg%7C大甲)[鄭成功像](../Page/鄭成功.md "wikilink")
[File:大甲鄭成功紀念館.jpg|大甲鄭成功紀念館](File:大甲鄭成功紀念館.jpg%7C大甲鄭成功紀念館)
[File:大甲中正公園.jpg|大甲中正公園](File:大甲中正公園.jpg%7C大甲中正公園)[蔣中正銅像](../Page/蔣中正.md "wikilink")
[File:台中軍人忠靈祠.jpg|台中軍人忠靈祠](File:台中軍人忠靈祠.jpg%7C台中軍人忠靈祠)

## 相關條目

  - [台灣小百岳](../Page/台灣小百岳.md "wikilink")
  - [參山國家風景區](../Page/參山國家風景區.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[Category:台中市山峰](../Category/台中市山峰.md "wikilink")
[Category:台灣小百岳](../Category/台灣小百岳.md "wikilink")
[Category:台中市旅遊自然景點](../Category/台中市旅遊自然景點.md "wikilink")
[Category:大甲區](../Category/大甲區.md "wikilink")
[Category:外埔區](../Category/外埔區.md "wikilink")

1.  [謝季燕，《全民登山運動之推展--以小百岳規劃為例》](http://old1.taroko.gov.tw/tit1/little%20mountain%20by%20shea.htm)