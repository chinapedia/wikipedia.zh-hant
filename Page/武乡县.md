**武乡县**在[中国](../Page/中国.md "wikilink")[山西省东南部](../Page/山西省.md "wikilink")、[太行山西侧](../Page/太行山.md "wikilink")、[浊漳河上游](../Page/浊漳河.md "wikilink")，是[长治市所辖的一个](../Page/长治市.md "wikilink")[县](../Page/县.md "wikilink")。

总面积为1610平方公里，2002年人口为21万人。

## 历史

西汉置**涅县**，属[并州](../Page/并州.md "wikilink")[上党郡](../Page/上党郡.md "wikilink")，治今故城镇。西晋[泰始年间](../Page/泰始.md "wikilink")（265年-
274年），涅县分为武乡县、镣阳县和涅县。[后赵置武乡郡](../Page/后赵.md "wikilink")。北魏[延和二年](../Page/延和.md "wikilink")（433年），武乡郡改为乡郡，武乡县改为**乡县**。北魏[太和十五年](../Page/太和.md "wikilink")（491年），县治迁至南亭川（今武乡故县村）。北齐将乡郡改为南垣州、丰州、戎州，北周恢复乡郡。隋开皇三年（583年），废乡郡，乡县属[上党郡](../Page/上党郡.md "wikilink")；西部涅县先后更名为阳城县、贾水县，隋[大业元年](../Page/大业.md "wikilink")（605年）拆分并入铜鞮县和乡县；隋义宁元年（617年）乡县析置榆社县。唐初属河东道[韩州](../Page/韩州.md "wikilink")；[贞观十七年](../Page/贞观.md "wikilink")（643年），韩州废，乡县归属潞州；唐[景云元年](../Page/景云.md "wikilink")（710年），恢复武乡县名。1940年7月武乡县分为武乡县、武西县。1945年9月武乡、武西合并。1958年11月，榆社县并入武乡县。1959年7月，榆社县分出。\[1\]

## 地理

武乡县绵亘于[太行](../Page/太行.md "wikilink")、[太岳两山之间](../Page/太岳.md "wikilink")，地势呈东西高，中间低，状若[如意](../Page/如意.md "wikilink")。县境东部地区海拔大部分在1400米以上，最高峰[花儿垴达](../Page/花儿垴达.md "wikilink")2008米。西部地区海拔在1300米左右，最高峰[紫金山海拔](../Page/紫金山.md "wikilink")1809米。北部和南部的大部分山岭多在1000-1300米之间。中部地势比较平缓，最低处监漳滩至西川一带海拔800米。全县可分为石质山区，黄土丘陵区和较平川区三个不同的地形区域。

## 行政区划

下辖5个[镇](../Page/行政建制镇.md "wikilink")、9个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 交通

  - [太焦铁路](../Page/太焦铁路.md "wikilink")、[武墨铁路](../Page/武墨铁路.md "wikilink")
  - [太长高速公路](../Page/太长高速公路.md "wikilink")（[二广高速公路](../Page/二广高速公路.md "wikilink")）

## 人口

2010年第六次全国人口普查武乡县常住人口182548人。\[2\]

## 经济

2012年GDP66.00亿元。

## 风景名胜

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[八路军总司令部旧址](../Page/八路军总司令部旧址.md "wikilink")、[洪济院](../Page/洪济院.md "wikilink")、[武乡县大云寺](../Page/武乡县大云寺.md "wikilink")、[会仙观](../Page/会仙观.md "wikilink")、[武乡真如寺](../Page/武乡真如寺.md "wikilink")、
  - [山西省文物保护单位](../Page/山西省文物保护单位.md "wikilink")：[北良侯村造像](../Page/北良侯村造像.md "wikilink")
  - [武乡县文物保护单位](../Page/武乡县文物保护单位.md "wikilink")

## 名人

## 参考文献

## 外部链接

  - [长治市武乡县政府网站](http://www.zgwx.gov.cn/)

[Category:长治区县市](../Category/长治区县市.md "wikilink")
[长治](../Category/山西省县份.md "wikilink")
[晋](../Category/国家级贫困县.md "wikilink")

1.  [历史沿革](http://www.zgwx.gov.cn/info/1013/2565.htm)
2.  [长治市2010年第六次全国人口普查主要数据公报](http://www.changzhi.gov.cn/info/news/2011/nry/203579.htm)