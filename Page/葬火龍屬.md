**葬火龍屬**（[屬名](../Page/屬.md "wikilink")：）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[偷蛋龍科的一](../Page/偷蛋龍科.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生活於[上白堊紀的](../Page/上白堊紀.md "wikilink")[蒙古](../Page/蒙古.md "wikilink")，化石發現於[戈壁沙漠](../Page/戈壁沙漠.md "wikilink")[烏哈托喀](../Page/烏哈托喀.md "wikilink")（Ukhaa
Tolgod）地區的[德加多克塔組](../Page/德加多克塔組.md "wikilink")（Djadokhta
Formation）。牠是相當著名的偷蛋龍科恐龍之一，因為目前已發現幾副保存完好的骨骼，包括幾個在巢中孵蛋的標本。這些標本鞏固了恐龍與[鳥類之間的關聯](../Page/鳥類.md "wikilink")。葬火龍與[偷蛋龍的外表類似](../Page/偷蛋龍.md "wikilink")，兩者常被混淆。

## 描述

[Citipati_profiles1.jpg](https://zh.wikipedia.org/wiki/File:Citipati_profiles1.jpg "fig:Citipati_profiles1.jpg")
[Citipatibcn3.JPG](https://zh.wikipedia.org/wiki/File:Citipatibcn3.JPG "fig:Citipatibcn3.JPG")的想像圖是根據其而重建\]\]
[Citipati_(1).jpg](https://zh.wikipedia.org/wiki/File:Citipati_\(1\).jpg "fig:Citipati_(1).jpg")\]\]
最大的葬火龍有[鴯鶓般的大小](../Page/鴯鶓.md "wikilink")，約3米長；在2007年[巨盜龍出土前](../Page/巨盜龍.md "wikilink")，葬火龍是最大的[偷蛋龍科恐龍](../Page/偷蛋龍科.md "wikilink")。就像其他偷蛋龍科，葬火龍有著較其他[獸腳亞目長的頸部及短尾巴](../Page/獸腳亞目.md "wikilink")。牠的[頭顱骨很短](../Page/頭顱骨.md "wikilink")，有著很多洞孔，喙嘴堅固，沒有[牙齒](../Page/牙齒.md "wikilink")。葬火龍最特別的特徵是牠的高頭冠，外表與現今的[鶴鴕很相似](../Page/鶴鴕.md "wikilink")。[模式種是](../Page/模式種.md "wikilink")**奧氏葬火龍**（*C.
osmolskae*），頭頂的冠狀物較低矮，而在另一個未被命名的標本（暫稱*C.
sp.*）則較高、較尖。葬火龍的前肢長，具有三指，可抓握，上有彎曲指爪。[脛骨與足部長](../Page/脛骨.md "wikilink")，顯示牠們可以高速奔跑\[1\]。

### 偷蛋龍？

由於[偷蛋龍](../Page/偷蛋龍.md "wikilink")[模式標本的](../Page/模式標本.md "wikilink")[頭顱骨保存得較差且已碎裂](../Page/頭顱骨.md "wikilink")，而另一個[偷蛋龍科標本](../Page/偷蛋龍科.md "wikilink")（編號IGN或GIN
100/42）就成為了牠的典型參考，並且甚至在[科學文獻中被命名為](../Page/科學.md "wikilink")[嗜角偷蛋龍](../Page/嗜角偷蛋龍.md "wikilink")（*Oviraptor
philoceratops*）\[2\]。但是，這個有著高冠狀物的標本有著與葬火龍更多的相同特徵，有可能是葬火龍屬的另一個[物種](../Page/物種.md "wikilink")，或是全新的[屬](../Page/屬.md "wikilink")，但須更多的研究證明\[3\]。再者，那個孵蛋的偷蛋龍科標本在被命名為葬火龍前，就引起了大量的關注。當經常以偷蛋龍科來表示時，很容易就與偷蛋龍混淆。況且首個偷蛋龍標本也是在巢中被發現，使得更為混淆。大部份偷蛋龍的著名重組其實都是取自葬火龍，而偷蛋龍的現有[化石資料太破碎](../Page/化石.md "wikilink")，很難製成可靠的重組。

### 名稱

葬火龍的學名是來自[梵語](../Page/梵語.md "wikilink")，意即「火葬柴堆的主」。在[藏傳佛教神話中](../Page/藏傳佛教.md "wikilink")，葬火龍的學名即[屍林主](../Page/屍林主.md "wikilink")，是兩個正在默想中被強盜斬首的[僧侶](../Page/僧侶.md "wikilink")\[4\]。屍林主一般都會以兩個被火焰包圍並正在跳舞的骨骼來代表，故以此來命名骨骼保存完好的葬火龍。[模式種是](../Page/模式種.md "wikilink")**奧氏葬火龍**（*C.
osmolskae*），是在2001年由[詹姆斯·克拉克](../Page/詹姆斯·克拉克.md "wikilink")（James M.
Clark）、[馬克·諾瑞爾](../Page/馬克·諾瑞爾.md "wikilink")（Mark
Norell）、[瑞欽·巴思缽](../Page/瑞欽·巴思缽.md "wikilink")（Rinchen
Barsbold）等人所命名，為紀念著名[波蘭](../Page/波蘭.md "wikilink")[古生物學家Halska](../Page/古生物學家.md "wikilink")
Osmólska，她曾對[蒙古的](../Page/蒙古.md "wikilink")[獸腳亞目及](../Page/獸腳亞目.md "wikilink")[偷蛋龍科作出極大的研究貢獻](../Page/偷蛋龍科.md "wikilink")\[5\]。除模式種之外，可能還有第二個種。

## 古生物學

[Nesting_Citipati.jpg](https://zh.wikipedia.org/wiki/File:Nesting_Citipati.jpg "fig:Nesting_Citipati.jpg")
最少有四個葬火龍標本都是在孵蛋的姿勢，當中最著名的是暱稱為「大媽媽」的大型標本，是於1995年被發表\[6\]，於1999年被描述\[7\]，及於2001年被命名為葬火龍屬\[8\]。所有孵蛋標本都是位於蛋群之上，牠們的兩肢對稱乎放在巢的兩側，前肢覆蓋整個巢的周界。這個孵蛋的姿勢只有現今的[鳥類可以見到](../Page/鳥類.md "wikilink")，支持了鳥類與[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍之間的行為關聯](../Page/恐龍.md "wikilink")\[9\]。葬火龍的孵蛋姿勢亦支持了牠們與其他[偷蛋龍科的前肢有著](../Page/偷蛋龍科.md "wikilink")[羽毛的假說](../Page/羽毛.md "wikilink")。若以前肢來覆蓋巢的周圍，除非牠們有著羽毛，否則大部份的蛋都不會被牠們的身體所覆蓋\[10\]。

[Citipati_IGM_100_979.jpg](https://zh.wikipedia.org/wiki/File:Citipati_IGM_100_979.jpg "fig:Citipati_IGM_100_979.jpg")
[Citipati_egg.jpg](https://zh.wikipedia.org/wiki/File:Citipati_egg.jpg "fig:Citipati_egg.jpg")。\]\]
雖然[化石化的恐龍蛋很稀少](../Page/化石.md "wikilink")，但葬火龍及偷蛋龍科的蛋都很知名。在[戈壁沙漠發現四個孵蛋的葬火龍標本之同時](../Page/戈壁沙漠.md "wikilink")，亦發掘出一些偷蛋龍科的蛋。葬火龍的蛋形狀像拉長了的[橢圓形](../Page/橢圓形.md "wikilink")，組織及結構就像[鴕鳥目的蛋](../Page/鴕鳥目.md "wikilink")。在巢中，葬火龍的蛋排列成三層的同心圓，一次可以孵多達22個蛋\[11\]。葬火龍的蛋是已知偷蛋龍科的蛋中最大的，長18厘米。相反，[偷蛋龍的蛋就只有](../Page/偷蛋龍.md "wikilink")14厘米長\[12\]。

諷刺的是，偷蛋龍科的名字是因牠們與蛋的關聯。首個偷蛋龍科的蛋（屬於偷蛋龍），是在[角龍下目的](../Page/角龍下目.md "wikilink")[原角龍的化石附近被發現](../Page/原角龍.md "wikilink")，並被認為是偷蛋龍在捕獵原角龍的蛋\[13\]。直至1993年，當在一個被認為是原角龍的蛋中發現葬火龍的[胚胎時](../Page/胚胎.md "wikilink")，這個錯誤才被糾正\[14\]。於是在2001年，諾瑞爾等人根據胚胎[前上頜骨的垂直稜脊](../Page/前上頜骨.md "wikilink")（一種葬火龍才有的特徵），將這個蛋被編回偷蛋龍科中。這個有著胚胎的蛋較其他葬火龍的蛋為小，約只有12厘米，因部份被侵蝕而裂開為三個部份，使得準確的估量較為困難\[15\]。這個蛋與其他的偷蛋龍科的蛋在結構上完全一樣，且是在一個獨立的巢中被發現，排列成圓環狀。另外兩個獸腳亞目恐龍的[頭顱骨也在這個巢附近被發現](../Page/頭顱骨.md "wikilink")\[16\]。

目前已經發現兩個[傷齒龍科](../Page/傷齒龍科.md "wikilink")[拜倫龍的幼年或胚胎頭骨](../Page/拜倫龍.md "wikilink")，位於一個葬火龍的蛋巢內。由於其中一顆蛋內有葬火龍的胚胎\[17\]。科學家無法解釋為何幼年拜倫龍會出現在葬火龍的蛋巢。目前的相關解釋有：幼年拜倫龍成為葬火龍的獵物、拜倫龍以剛出生的的葬火龍為食、拜倫龍有與其他動物共生的習性\[18\]。

## 參考資料

## 外部連結

  - [葬火龍頭顱骨的電腦斷層掃描](http://www.digimorph.org/specimens/Citipati_osmolskae/)
  - [葬火龍的圖畫](http://dino.lm.com/taxa/display.php?name=Citipati)

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:偷蛋龍科](../Category/偷蛋龍科.md "wikilink")
[Category:有羽毛恐龍](../Category/有羽毛恐龍.md "wikilink")

1.

2.  Barsbold, R., Maryanska, T., and Osmolska, H. (1990).
    "Oviraptorosauria," in Weishampel, D.B., Dodson, P., and Osmolska,
    H. (eds.). *The Dinosauria*. Berkeley: University of California
    Press, pp. 249-258.

3.  Clark, J.M., Norell, M.A., & Barsbold, R. (2001). "Two new
    oviraptorids (Theropoda:Oviraptorosauria), upper Cretaceous
    Djadokhta Formation, Ukhaa Tolgod, Mongolia." *Journal of Vertebrate
    Paleontology* **21(2)**:209-213., June 2001.

4.

5.
6.  Norell, M.A., Clark, J.M., Chiappe, L.M., and Dashzeveg, D. (1995).
    "A nesting dinosaur." *Nature* **378**:774-776.

7.  Clark, J.M., Norell, M.A., & Chiappe, L.M. (1999). "An oviraptorid
    skeleton from the Late Cretaceous of Ukhaa Tolgod, Mongolia,
    preserved in an avianlike brooding position over an oviraptorid
    nest." *American Museum Novitates,* **3265**: 36 pp., 15 figs.;
    (American Museum of Natural History) New York. (5.4.1999).

8.
9.
10. Paul, G.S. (2002). *Dinosaurs of the Air: The Evolution and Loss of
    Flight in Dinosaurs and Birds.* Baltimore: Johns Hopkins University
    Press.

11. Varricchio, D.J. (2000). "Reproduction and Parenting," in Paul, G.S.
    (ed.). *The Scientific American Book of Dinosaurs.* New York: St.
    Martin's Press, pp. 279-293.

12.
13. Osborn, H.F. (1924). "Three new Theropoda, *Protoceratops* zone,
    central Mongolia." *American Museum Novitates,* **144**: 12 pp., 8
    figs.; (American Museum of Natural History) New York. (11.7.1924).

14. Norell, M. A., J. M. Clark, D. Dashzeveg, T. Barsbold, L. M.
    Chiappe, A. R. Davidson, M. C. McKenna, and M. J. Novacek (1994). "A
    theropod dinosaur embryo, and the affinities of the Flaming Cliffs
    Dinosaur eggs." *Science* **266**: 779–782.

15.
16.
17. Bever, G.S. and Norell, M.A. (2009). "The perinate skull of
    *Byronosaurus* (Troodontidae) with observations on the cranial
    ontogeny of paravian theropods." *American Museum Novitates*,
    **3657**: 51 pp.

18.