**釘螺**泛指所有**釘螺屬**（學名：）的物種，是一种在[田间](../Page/田.md "wikilink")[生活的](../Page/生活.md "wikilink")[腹足綱](../Page/腹足綱.md "wikilink")[軟體動物](../Page/軟體動物.md "wikilink")。釘螺屬曾經屬於[觿螺科](../Page/觿螺科.md "wikilink")（Hydrobiidae）及[麂眼螺總科](../Page/麂眼螺總科.md "wikilink")\[1\]，今屬[截螺總科](../Page/截螺總科.md "wikilink")[盖螺科](../Page/盖螺科.md "wikilink")\[2\]\[3\]。

## 形態特徵

钉螺外壳小，呈圆锥形，有[螺层](../Page/螺层.md "wikilink")7个左右，像一个小[螺丝钉](../Page/螺丝钉.md "wikilink")，因而得名。壳面光滑或有各种粗细不同的直棱；壳口呈卵圆形，周围完整，略向外翻，有角质[厣片](../Page/厣片.md "wikilink")。（厣：yǎn）

钉螺由[螺壳和](../Page/螺壳.md "wikilink")[软体两部分组成](../Page/软体.md "wikilink")。软体部分的前部为[头](../Page/头.md "wikilink")、[颈](../Page/颈.md "wikilink")、[足和](../Page/足.md "wikilink")[外套膜](../Page/外套膜.md "wikilink")，后部是[内脏包](../Page/内脏包.md "wikilink")。

## 物種

[Oncomelania_minima.png](https://zh.wikipedia.org/wiki/File:Oncomelania_minima.png "fig:Oncomelania_minima.png")''\]\]
釘螺屬物種基本上可以分為下列兩種\[4\]：

  - *[Oncomelania hupensis](../Page/Oncomelania_hupensis.md "wikilink")*
    Gredler, 1881\[5\]：[模式種](../Page/模式種.md "wikilink")\[6\] this
    [polytypic](../Page/Polytypic_taxon.md "wikilink") species has a
    number of subspecies
  - *[Oncomelania minima](../Page/Oncomelania_minima.md "wikilink")* P.
    Bartsch, 1936\[7\]

Woodruff et al. (1999)也確認以下物種：

  - *[Oncomelania
    lindoensis](../Page/Oncomelania_lindoensis.md "wikilink")* Davis &
    Carney, 1973 - from [Lake Lindu](../Page/Lake_Lindu.md "wikilink"),
    [Sulawesi](../Page/Sulawesi.md "wikilink")\[8\]

Japanese Red List Data Book (2006) recognizes also the following
additional species:

  - *[Oncomelania shini](../Page/Oncomelania_shini.md "wikilink")* - as
    [Vulnerable species](../Page/Vulnerable_species.md "wikilink") in
    Japan\[9\]
  - *[Oncomelania
    sakuyamai](../Page/Oncomelania_sakuyamai.md "wikilink")* - with no
    conservation status evaluated in Japan\[10\]

表面有纵肋者称[肋壳钉螺](../Page/肋壳钉螺.md "wikilink")，壳长约10毫米，宽约4毫米，生存於[湖](../Page/湖.md "wikilink")[沼或水网](../Page/沼.md "wikilink")[地区](../Page/地区.md "wikilink")；壳面光滑者为[光壳钉螺](../Page/光壳钉螺.md "wikilink")，比肋壳钉螺稍小，长、宽分别为6毫米和3毫米，多见於[山丘地区](../Page/山丘.md "wikilink")。

## 習性

钉螺属[软体动物](../Page/软体动物.md "wikilink")，有[雌](../Page/雌.md "wikilink")、[雄之分](../Page/雄.md "wikilink")，水陆两栖，活躍於15\~20℃度的[气温](../Page/气温.md "wikilink")，主要靠[吃](../Page/吃.md "wikilink")[藻类而生存](../Page/藻类.md "wikilink")，多孳生于水分充足、[有机物丰富](../Page/有机物.md "wikilink")、[杂草丛生](../Page/杂草.md "wikilink")、[潮湿荫蔽的灌溉沟或河边浅滩](../Page/潮湿.md "wikilink")；通常生活在水线上下，[冬季随气温下降深入地面下数厘米蛰伏越冬](../Page/冬季.md "wikilink")。钉螺也可在地面生活，但活动范围有限，速度缓慢。

钉螺的活动范围并不大，但可随水流漂到很远的地方，也可附着在杂草或其他漂浮物上而[扩散到远处](../Page/扩散.md "wikilink")。人们穿的草鞋、牛[蹄间隙](../Page/蹄.md "wikilink")、打水草或移种[水生植物](../Page/水生植物.md "wikilink")（如[芦苇](../Page/芦苇.md "wikilink")、[茭白等](../Page/茭白.md "wikilink")）、运送鱼苗等也能使钉螺[扩散](../Page/扩散.md "wikilink")。钉螺扩散后，遇到适合的[环境便](../Page/环境.md "wikilink")“安家落户”、孳生[繁殖](../Page/繁殖.md "wikilink")，形成新的钉螺孳生地。

钉螺的[寿命一般为](../Page/寿命.md "wikilink")1年，有的钉螺可存活2-3年，甚至超过5年。感染了血吸虫毛蚴的钉螺叫「感染螺」，感染螺的寿命一般不到1年，最长也可存活2年多。

## 寄生蟲

钉螺是[日本血吸虫](../Page/日本血吸虫.md "wikilink")[幼虫的主要](../Page/幼虫.md "wikilink")[宿主](../Page/宿主.md "wikilink")，所以[水患期间](../Page/水災.md "wikilink")，消灭钉螺是防治血吸虫的一个重要环节。

## 參考文獻

## 外部連結

  - [关于钉螺](http://www.hwcc.com.cn/newsdisplay/newsdisplay.asp?Id=114354)

[釘螺屬](../Category/釘螺屬.md "wikilink")

1.
2.
3.
4.

5.
6.
7.
8.  Woodruff D. S., Carpenter M. P., Upatham E. S. & Viyanant V. (1999).
    "Molecular Phylogeography of *Oncomelania lindoensis* (Gastropoda:
    Pomatiopsidae), the Intermediate Host of *Schistosoma Japonicum* in
    Sulawesi". ''[Journal of Molluscan
    Studies](../Page/Journal_of_Molluscan_Studies.md "wikilink")
    **65**(1): 21-31. .

9.
    ["ヨナクニカタヤマガイ"](http://www.jpnrdb.com/search.php?mode=map&q=110504020100292)
    . 日本のレッドデータ検索システム \[Japanese Red List Data Book\], accessed 17 July
    2011.

10.
    ["サクヤマカワツボ"](http://www.jpnrdb.com/search.php?mode=map&q=110504020100291)
    . 日本のレッドデータ検索システム \[Japanese Red List Data Book\], accessed 17 July
    2011.