**山崎富榮**（），是[日本的一名美容師](../Page/日本.md "wikilink")，[作家](../Page/作家.md "wikilink")[太宰治的](../Page/太宰治.md "wikilink")[愛人](../Page/愛人.md "wikilink")。1948年，在[玉川上水](../Page/玉川上水.md "wikilink")，與太宰治一同投河自殺。
[TomieYamazaki.jpg](https://zh.wikipedia.org/wiki/File:TomieYamazaki.jpg "fig:TomieYamazaki.jpg")

## 生平

山崎富榮出生於[東京市](../Page/東京市.md "wikilink")（今[東京都](../Page/東京都.md "wikilink")[文京区](../Page/文京区.md "wikilink")[本鄉](../Page/本鄉.md "wikilink")）。父[山崎晴弘是日本最早的美容学校](../Page/山崎晴弘.md "wikilink")「[東京婦人美髪美容学校](../Page/東京婦人美髪美容学校.md "wikilink")」（）的創辦人。山崎富榮是家中次女。

她在父親的美容學校接受菁英教育。從[錦秋高等女学校轉到](../Page/錦秋高等女学校.md "wikilink")[京華高等女学校之後畢業](../Page/京華女子中学校・高等学校.md "wikilink")。畢業後於[YWCA學習](../Page/基督教女青年會.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[英語與](../Page/英語.md "wikilink")[戲劇](../Page/戲劇.md "wikilink")。她也在[慶應義塾大學旁聽](../Page/慶應義塾大學.md "wikilink")，並和[義姊](../Page/義姊.md "wikilink")[山崎つた一起](../Page/山崎つた.md "wikilink")，在[銀座](../Page/銀座.md "wikilink")2丁目經營オリンピア美容院。

1944年12月9日，她與[三井物產的員工奥名修一結婚](../Page/三井物產.md "wikilink")。婚後十天奧名修一就被派到三井物產的[馬尼拉分店任職](../Page/馬尼拉.md "wikilink")。後來遇到美軍進攻馬尼拉，他參與戰爭之後下落不明。

1945年3月的[東京大轟炸](../Page/東京大轟炸.md "wikilink")，オリンピア美容院與お茶の水美容学校都被燒毀。山崎富榮與雙親被強制疏散到[滋賀縣](../Page/滋賀縣.md "wikilink")[神崎郡](../Page/神崎郡_\(滋賀縣\).md "wikilink")[八日市町](../Page/八日市町.md "wikilink")（今[八日市市](../Page/八日市市.md "wikilink")）。

1946年春天，與義姊於[鎌倉市](../Page/鎌倉市.md "wikilink")[長谷開了美容院](../Page/長谷.md "wikilink")。1946年11月14日，搬至[東京](../Page/東京.md "wikilink")[三鷹](../Page/三鷹.md "wikilink")。在過去茶の水美容学校畢業生經營的ミタカ美容院工作，晚上則於[駐日盟軍專用的](../Page/駐日盟軍總司令.md "wikilink")[卡巴萊中的美容部任職](../Page/卡巴萊.md "wikilink")。

1947年3月27日晚上，在烏龍麵攤認識了正在喝酒的[太宰治](../Page/太宰治.md "wikilink")。她二哥[山崎年一](../Page/山崎年一.md "wikilink")（やまざき
としかず）是[旧制弘前高等学校大太宰治兩年的學長](../Page/旧制弘前高等学校.md "wikilink")。她最後與太宰治一起跳水自殺[殉情](../Page/殉情.md "wikilink")。

## 參考文獻

  - ，大光社 （1967年）
    [ASIN](../Page/Amazon_Standard_Identification_Number.md "wikilink")：B000JA8SQY

  - 　長篠康一郎 編著

  - 長篠康一郎 編著

  -  著

  - [山岸外史](../Page/山岸外史.md "wikilink")

  - [野田宇太郎](../Page/野田宇太郎.md "wikilink") 著

  - [梶原梯子](../Page/梶原梯子.md "wikilink") 著

  - [瀨戶內晴美](../Page/瀨戶內晴美.md "wikilink")（）

  - [井伏鱒二](../Page/井伏鱒二.md "wikilink")

  -

  -

  - [坂口安吾](../Page/坂口安吾.md "wikilink")

[Category:日本自殺者](../Category/日本自殺者.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")