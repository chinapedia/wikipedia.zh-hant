**下加利福尼亞州**（），簡稱**下加州**和**北下加州**，是[墨西哥最北部的](../Page/墨西哥.md "wikilink")[州](../Page/墨西哥行政區劃.md "wikilink")。為了與位於[下加利福尼亞半島南部的](../Page/下加利福尼亞半島.md "wikilink")[南下加利福尼亞州區分](../Page/南下加利福尼亞州.md "wikilink")，又稱**北下加利福尼亞州**（****）。北面與[美國](../Page/美國.md "wikilink")[加州接壤](../Page/加州.md "wikilink")。首府[墨西卡利](../Page/墨西卡利.md "wikilink")，大城市包括[蒂華納](../Page/蒂華納.md "wikilink")、[羅薩里多](../Page/羅薩里多.md "wikilink")、[恩森那達及](../Page/恩森那達.md "wikilink")[特卡特](../Page/特卡特.md "wikilink")。

## 歷史

人類最初在此出現是14,000年前。1690年代至1700年間[西班牙人向](../Page/西班牙.md "wikilink")[加利福尼亞殖民](../Page/加利福尼亞.md "wikilink")。1773年以相若於今日美墨邊界分為[上加利福尼亞省和](../Page/上加利福尼亞省.md "wikilink")**下加利福尼亞省**，界北歸[方濟各會](../Page/方濟各會.md "wikilink")，南歸[多明我會](../Page/多明我會.md "wikilink")。1824年成為墨西哥的一部分。1848年[美墨戰爭后上加利福尼亞省成為美國的一部分](../Page/美墨戰爭.md "wikilink")。1930年下加利福尼亞分為南北兩個聯邦區。1953年北部建州。

## 注释

[\*](../Category/下加利福尼亚州.md "wikilink")