**劉京**（），祖籍[河南](../Page/河南.md "wikilink")[新安县](../Page/新安县.md "wikilink")；官方简历中其籍贯为[山西](../Page/山西.md "wikilink")[盂县](../Page/盂县.md "wikilink")，[北京工业大学](../Page/北京工业大学.md "wikilink")[无线电技术专业毕业](../Page/无线电.md "wikilink")。1965年8月加入[中国共产党](../Page/中国共产党.md "wikilink")，是[中共第十六](../Page/中国共产党第十六届中央委员会委员.md "wikilink")、[十七屆中央委員](../Page/中国共产党第十七届中央委员会委员.md "wikilink")。官至[中华人民共和国公安部副部长](../Page/中华人民共和国公安部.md "wikilink")、党委副书记，[610办公室主任](../Page/610办公室.md "wikilink")、党组书记，副总警监警衔。

## 家庭背景

  - 生父[韩钧曾任中共](../Page/韩钧.md "wikilink")[北平市委委员](../Page/北平.md "wikilink")、市委秘书长兼市军管会秘书长。
  - [养父刘岱峰](../Page/养父.md "wikilink")（山西盂县人）曾任[国家计委副主任](../Page/中华人民共和国国家计划委员会.md "wikilink")。

## 简历

虽说是无线电技术专业毕业，但刘京日后所从事的工作，其领域之广，可以说有些让人不可思议。1968年大学毕业，刘京入伍参军。在三年兵营锻炼后，他进入北京东方红医疗器械厂当工人；不久，转为干部；历任[北京医疗器械研究所技术员](../Page/北京医疗器械研究所.md "wikilink")、助理工程师。

1983年7月，刘京进入政府部门工作；是[外经贸部国际联络局的一名副处长](../Page/外经贸部.md "wikilink")。自1985年2月，刘京开始在[中国残疾人福利基金会任职](../Page/中国残疾人福利基金会.md "wikilink")；历任国内部副主任、主任，基金会副秘书长、[中国肢体伤残康复研究中心代主任等职](../Page/中国肢体伤残康复研究中心.md "wikilink")；并在1988年4月，当选[中国残疾人联合会执行理事会副理事长](../Page/中国残疾人联合会.md "wikilink")。1992年3月，刘京又被“空降”到[云南](../Page/云南.md "wikilink")，任省外经贸厅厅长，后又出任云南省外经贸委员会主任；一年后，就升任云南省副省长。1998年4月，刘京回到北京，出任[中华人民共和国海关总署副署长](../Page/中华人民共和国海关总署.md "wikilink")、党组副书记。

2000年6月，刘京被任命为[610办公室副主任](../Page/610办公室.md "wikilink")、党组副书记等职。2001年1月又开始兼[中华人民共和国公安部副部长](../Page/中华人民共和国公安部.md "wikilink")、党委副书记。自2001年9月，他又晋升为[610办公室主任](../Page/610办公室.md "wikilink")；2001年1月被授予[副总警监警衔](../Page/副总警监.md "wikilink")。

## 参看

  - [杨家将事件](../Page/杨家将事件.md "wikilink")

## 参考文献

  - [刘京同志简历](https://web.archive.org/web/20080719220353/http://www.mps.gov.cn/n16/n1282/n3463/n3598/n1175146/n1175887/1181771.html)
    [中华人民共和国公安部网站](../Page/中华人民共和国公安部.md "wikilink")

[Category:中华人民共和国云南省副省长](../Category/中华人民共和国云南省副省长.md "wikilink")
[Category:中华人民共和国公安部正部长级副部长](../Category/中华人民共和国公安部正部长级副部长.md "wikilink")
[Category:副总警监](../Category/副总警监.md "wikilink")
[Category:中华人民共和国海关总署副署长](../Category/中华人民共和国海关总署副署长.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:北京工业大学校友](../Category/北京工业大学校友.md "wikilink")
[Category:中国共产党党员
(1965年入党)](../Category/中国共产党党员_\(1965年入党\).md "wikilink")
[L刘](../Category/盂县人.md "wikilink") [J京](../Category/刘姓.md "wikilink")