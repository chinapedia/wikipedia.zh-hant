**张江树**（），又名雪帆，[江苏](../Page/江苏.md "wikilink")[常熟人](../Page/常熟.md "wikilink")。物理化学家，教育家，政治人物。中国[物理化学的先驱](../Page/物理化学.md "wikilink")。[华东理工大学和](../Page/华东理工大学.md "wikilink")[中国化学会的创始人](../Page/中國化學會_\(中華民國\).md "wikilink")。曾担任[上海市人民委员会第一至五届委员](../Page/上海市人民委员会.md "wikilink")\[1\]。

1910年，张江树就读于[上海](../Page/上海.md "wikilink")[龙门师范学校](../Page/上海中学.md "wikilink")，1914年毕业后考入[南京高等师范学校](../Page/南京高等师范学校.md "wikilink")（後改為[國立東南大學](../Page/國立東南大學.md "wikilink")、[中央大學](../Page/國立中央大學.md "wikilink")、[南京大學](../Page/南京大學.md "wikilink")）数学理化部，1918年毕业留校任化学助教。1923年赴美国留学，先在美国[加州大学学化学](../Page/加州大学.md "wikilink")，1924年又入[哈佛大学](../Page/哈佛大学.md "wikilink")。1926年回国，先后在[光华大学](../Page/光华大学.md "wikilink")、[中山大学](../Page/中山大学.md "wikilink")、中央大学等校任教授。1932年起担任[武汉大学化学系教授](../Page/武汉大学.md "wikilink")，曾任中央大学化学系主任、理学院院长，中央大学改名南京大学后，任南京大学理学院院长、教务长。1952年，以南京大学工学院为基础创建[南京工学院](../Page/南京工学院.md "wikilink")，任筹备委员会主任委员，10月开学后调任中国第一所高等化工院校[华东化工学院院长](../Page/华东化工学院.md "wikilink")，该校于1960年被列为全国重点院校。1981年退休，担任华东化工学院（1993年更名为[华东理工大学](../Page/华东理工大学.md "wikilink")）名誉院长。曾长期担任全国高等工科院校化学教材编审委员会主任和《[辞海](../Page/辞海.md "wikilink")》化学分科主编。1989年10月在上海逝世。\[2\]

## 参考文献

## 外部链接

  - [华东理工大学网站对张江树的介绍](https://www.ecust.edu.cn/2011/0525/c63a3133/page.htm)

[分類:國立中央大學教席學者](../Page/分類:國立中央大學教席學者.md "wikilink")
[分類:南京大學教席學者](../Page/分類:南京大學教席學者.md "wikilink")

[Z张](../Category/中国教育家.md "wikilink")
[Z张](../Category/中国化学家.md "wikilink")
[Z张](../Category/苏州人.md "wikilink")
[理Z张](../Category/中央大学校友.md "wikilink")
[Z张](../Category/南京大学校友.md "wikilink")
[0Z张](../Category/东南大学校友.md "wikilink")
[Category:东南大学校长](../Category/东南大学校长.md "wikilink")
[Category:武汉大学教授](../Category/武汉大学教授.md "wikilink")
[Category:中华人民共和国政治人物](../Category/中华人民共和国政治人物.md "wikilink")
[Z](../Category/國立中山大學教授.md "wikilink")

1.
2.