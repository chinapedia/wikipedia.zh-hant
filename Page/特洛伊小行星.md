[Asteroid_Belt.jpg](https://zh.wikipedia.org/wiki/File:Asteroid_Belt.jpg "fig:Asteroid_Belt.jpg")與[木星軌道之間的主要](../Page/木星.md "wikilink")[小行星帶](../Page/小行星帶.md "wikilink")\]\]

**特洛伊群小行星**是與木星共用軌道，一起繞著[太陽運行的一大群小行星](../Page/太陽.md "wikilink")。從固定在[木星上的座標系統來看](../Page/木星.md "wikilink")，他們是在所謂的[拉格朗日點中穩定的兩個點](../Page/拉格朗日點.md "wikilink")，分別位於木星軌道前方（**L<sub>4</sub>**）和後方（**L<sub>5</sub>**）60度的位置上。\[1\]

依照原本的規範，特洛伊[小行星的軌道](../Page/小行星.md "wikilink")[半長軸是介於](../Page/半長軸.md "wikilink")5.05至5.40[天文單位](../Page/天文單位.md "wikilink")，並且在是在兩個[拉格朗日點的一段弧形區域內](../Page/拉格朗日點.md "wikilink")。這個規範現在也適用在其他天體的相似情況下，而在這些情形下會標示出主要的天體。例如：**[海王星的特洛伊小行星](../Page/海王星特洛伊.md "wikilink")**。

在2006年，夏威夷[凱克天文台的一個小組宣佈](../Page/凱克天文台.md "wikilink")，他們曾經測量到一個小行星[（617）普特洛克勒斯](../Page/小行星617.md "wikilink")（Patroclus）的密度比結冰的水還要低，因而建議這是一對小行星，而且許多特洛伊小行星都可能是雙星。[彗星或](../Page/彗星.md "wikilink")[柯伊伯带天體在大小和組成上](../Page/柯伊伯带.md "wikilink")（冰與包覆在外圍的塵埃），也是可能的對象。而在未來，他們可能才是主要的小行星帶天體。（reference:
2.Feb issue of Nature）

## 歷史

[巴納德被認為是第一個觀察到特洛伊小行星的人](../Page/愛德華·愛默生·巴納德.md "wikilink")。在1904年，當時沒有人注意到他的觀測，而認為他觀測到的是[土星的](../Page/土星.md "wikilink")[衛星](../Page/衛星.md "wikilink")[土衛九](../Page/土衛九.md "wikilink")（Phoebe），因為當時兩者在天空中的距離只有兩弧分角，或者只是一顆[恆星](../Page/恆星.md "wikilink")。直到特洛伊小行星[（12126）1999
RM11在](../Page/（12126）1999_RM11.md "wikilink")1999年再度被發現與確認軌道之後，巴納德的觀測才受到重視。但真的要確認巴納德看到的究竟是哪個天體，大概只有歷史學家才有興趣了。

在1906年2月，德國天文學家[馬克斯·沃夫](../Page/馬克斯·沃夫.md "wikilink")（Max
Wolf）發現一顆位於[太陽](../Page/太陽.md "wikilink")-[木星的](../Page/木星.md "wikilink")[拉格朗日點L](../Page/拉格朗日點.md "wikilink")<sub>4</sub>上的[小行星](../Page/小行星.md "wikilink")，後來以[荷馬在神話故事](../Page/荷馬.md "wikilink")[伊利亞特的的英雄](../Page/伊利亞特.md "wikilink")[阿基里斯命名為](../Page/阿基里斯.md "wikilink")[（588）阿基里斯](../Page/小行星588.md "wikilink")，在幾個月內呈現異常的軌道運動，並且在不久之後，許多其他的小行星也在這個點的附近被發現（包括[太陽](../Page/太陽.md "wikilink")-[木星系統的另一個](../Page/木星.md "wikilink")[拉格朗日點L](../Page/拉格朗日點.md "wikilink")<sub>5</sub>）。

迄2007年9月，已經確認的特洛伊小行星有2,239顆，其中1,192顆在L<sub>4</sub>點，1,047顆在L<sub>5</sub>點，而在1999年10月，總數只有177顆被賦予數字的編號，在2004年7月也才只有877顆，2005年8月，達到1,826顆，到了2006年6月已經達到2,049顆。另外，還有6顆在海王星的軌道上，4顆在火星軌道上。最大的特洛伊小行星是[（624）赫克特](../Page/小行星624.md "wikilink")（Hektor），測量得到長370公里，寬195公里。無庸置疑的，有許多更小的天體以現在的儀器還無法看見。

## 命名法

在[馬克斯·沃夫的主導下](../Page/馬克斯·沃夫.md "wikilink")，特洛伊小行星都以[伊利亞特劇中的希臘英雄人物命名](../Page/伊利亞特.md "wikilink")，事實上只限於**L<sub>4</sub>**點，所以也稱為[希臘群或以](../Page/希臘群.md "wikilink")[阿基里斯為代表稱為](../Page/阿基里斯.md "wikilink")[阿基里斯群](../Page/阿基里斯群.md "wikilink")；在L<sub>5</sub>點的則以[特洛伊的英雄來命名](../Page/特洛伊.md "wikilink")，代表人物則是[普特洛克罗斯](../Page/普特洛克罗斯.md "wikilink")，所以也稱為[特洛伊群或](../Page/特洛伊群.md "wikilink")[普特洛克勒斯群](../Page/普特洛克勒斯群.md "wikilink")。但令人困惑的是，[普特洛克罗斯是屬於希臘的英雄人物](../Page/普特洛克罗斯.md "wikilink")，但[（617）普特洛克勒斯卻是在L](../Page/小行星617.md "wikilink")<sub>5</sub>點發現的第一顆小行星。其實是因為當時還沒有建立希臘和特洛伊分開命名的規則，所以在[希臘群中也有](../Page/希臘群.md "wikilink")[特洛伊的英雄](../Page/特洛伊.md "wikilink")[（624）赫克特](../Page/小行星624.md "wikilink")。

由於[伊利亞特劇中](../Page/伊利亞特.md "wikilink")[特洛伊戰爭的人物被用於](../Page/特洛伊戰爭.md "wikilink")[特洛依群小行星的命名](../Page/特洛依群.md "wikilink")，而最初[特洛伊又僅用於稱呼和木星分享軌道的小行星](../Page/特洛伊.md "wikilink")，所以當在[火星與](../Page/火星.md "wikilink")[海王星的](../Page/海王星.md "wikilink")[拉格朗日點都有小行星被發現後](../Page/拉格朗日點.md "wikilink")，這些特洛伊小行星就必須稱為[火星特洛伊或](../Page/火星特洛伊.md "wikilink")[海王星特洛伊](../Page/海王星特洛伊.md "wikilink")。另外，在[土星的衛星中也發現了兩組](../Page/土星.md "wikilink")[特洛依衛星](../Page/特洛依衛星.md "wikilink")，[土衛十三](../Page/土衛十三.md "wikilink")（Telesto）-[土衛三](../Page/土衛三.md "wikilink")（Tethys）-[土衛十四](../Page/土衛十四.md "wikilink")（Calypso）和[土衛十二](../Page/土衛十二.md "wikilink")（Helene）-[土衛四](../Page/土衛四.md "wikilink")（Dione）-[土衛三十四](../Page/土衛三十四.md "wikilink")（Polydeuces）
。

廣泛分布的特洛伊小行星，或許也扮演了[衛星形成的重要角色](../Page/衛星.md "wikilink")。在居於領導地位的[大碰撞理論中](../Page/大碰撞說.md "wikilink")，在非常早期的太陽系，就有一顆火星大小的行星和地球發生碰撞。由於撞擊不僅必須擊中地球的側面，還不能太為猛烈（不然這兩個天體都將被徹底的毀滅）。因此這顆假設中的行星[忒亚](../Page/忒亚.md "wikilink")（Theia），在撞擊地球之前必須是顆位在[地球](../Page/地球.md "wikilink")-[太陽系統穩定的](../Page/太陽.md "wikilink")[拉格朗日點上](../Page/拉格朗日點.md "wikilink")，然後才有一些因素慢慢的改變了他的軌道，最後才進入與年輕的地球碰撞的軌道上。

## 地球的特洛伊小行星

[2010 TK<sub>7</sub>是首颗被发现的地球的特洛伊小行星](../Page/2010_TK7.md "wikilink")。

## 相關條目

  - [特洛伊天體](../Page/特洛伊天體.md "wikilink")

## 外部連結

  - [Minor Planet Center's List of Trojan Minor
    Planets](http://cfa-www.harvard.edu/iau/lists/Trojans.html)
  - [New Trojan asteroid hints at huge Neptunian
    cloud](http://www.newscientistspace.com/article.ns?id=dn9340&feedId=online-news_rss20)
    - New Scientist

[Category:小行星](../Category/小行星.md "wikilink")
[Category:特洛伊小行星](../Category/特洛伊小行星.md "wikilink")

1.