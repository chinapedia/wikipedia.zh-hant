**遠東航空116號班機**是由[高雄國際機場飛往](../Page/高雄國際機場.md "wikilink")[臺北松山機場的班機](../Page/臺北松山機場.md "wikilink")。1993年10月25日，一架[遠東航空的](../Page/遠東航空.md "wikilink")[MD-82型飛機執行此航班任務時](../Page/MD-82.md "wikilink")，由於左發動故障而需折返，迫降時衝出跑道跌入水溝、擦撞機場圍牆，雖無人喪生，但機體已無法修復。

## 飛機基本資料

  - 飛機機型：MD-82
  - 製造序號（msn）：53065/1925
  - 機身編號：B-28003
  - 出廠年：1991年

## 概要

B-28003飛機，執行FE116號航班的飛行任務，從[高雄國際機場飛往](../Page/高雄國際機場.md "wikilink")[台北松山機場](../Page/台北松山機場.md "wikilink")，於上午11時8分起飛。起飛3分鐘後，機長聽到「砰砰」2聲巨響，發動機外殼掉落於[小港區](../Page/小港區.md "wikilink")[高松路上](../Page/高松路.md "wikilink")，機艙內亦冒出大量濃煙，故決定立刻返回[高雄國際機場](../Page/高雄國際機場.md "wikilink")。但由於[迫降時飛機速度過快](../Page/迫降.md "wikilink")，飛機衝出跑道，起落架斷裂，飛機機鼻著地與地面摩擦發出巨響並冒出大量黑煙，機長將飛機偏轉90度將[動量全部轉移給機場東側之圍牆以停止前進](../Page/動量.md "wikilink")，並減緩衝擊力道，才免於繼續直行危及[小港區](../Page/小港區.md "wikilink")[高松里民宅](../Page/高松里.md "wikilink")。否則以當時飛機滿載油料的狀況，後果不堪設想。

## 事故原因

左發動機葉片突然斷裂，導致事故。

## 事故後續

一、由於[B-2603及B](../Page/遠東航空103號班機.md "wikilink")-28003相繼失事，從此遠東航空決定未來所有機身編號將永遠跳過字尾03號。由於遠東航空原無偶數字尾，而字尾9號又已因2次失事而被跳過，從此遠東航空機身編號可用的字尾尚有1、3(13以上)、5、7。

二、高雄國際機場於飛機拖離事故現場後，將毀損之圍牆修繕成新圍牆。

三、事故發生後，小港區居民表示希望高雄國際機場遷建，以維居住安全。

## 事故附近之政府機關、學校

　一、本事故附近之政府機關為[高雄市政府警察局](../Page/高雄市政府警察局.md "wikilink")[小港分局](../Page/小港分局.md "wikilink")、高雄市消防局小港分隊、內政部航警局高雄分局\*[1](http://www.apb.gov.tw/index.php/ch/intro-kaohsiung-tw)、[高雄市政府警察局高松派出所](../Page/高雄市政府警察局.md "wikilink")、[中華郵政高松郵局](../Page/中華郵政.md "wikilink")\*[2](http://www.post.gov.tw/post/internet/I_location/index_post.jsp?prsb_no=004175-1)。特別的是，緊鄰飛機事故附近100公尺內，為中國石油（後更名為[台灣中油](../Page/台灣中油.md "wikilink")）經營之加油站（位於[小港區高松里高鳳路](../Page/小港區.md "wikilink")）。

　二、附近之學校為高雄市立太平國小\*[3](http://www.tpps.kh.edu.tw/)、[坪頂國小](../Page/高雄市小港區坪頂國民小學.md "wikilink")、[青山國小](../Page/青山國小.md "wikilink")、市立[明義國中](../Page/明義國中.md "wikilink")、[中山國中](../Page/高雄市立中山國民中學.md "wikilink")、市立[小港高中](../Page/小港高中.md "wikilink")、[私立高鳳工業家事職業學校](../Page/私立高鳳工業家事職業學校.md "wikilink")，與正值籌劃期間之[國立高雄餐旅大學](../Page/國立高雄餐旅大學.md "wikilink")。

　三、本事故因上列之地緣關係、飛機擦撞之圍牆地點隸屬[小港區高松里](../Page/小港區.md "wikilink")，故也稱為**高松空難**。

## 参见

  - [中国东方航空5398号班机空难](../Page/中国东方航空5398号班机空难.md "wikilink")：一天后即发生在[福州义序机场](../Page/福州义序机场.md "wikilink")，同样涉及MD-82
  - [美国航空1420号班机空难](../Page/美国航空1420号班机空难.md "wikilink")

## 外部連結

  - [Aviation Safety
    Net](http://aviation-safety.net/database/record.php?id=19931025-2)
    事故資料

[Category:1993年台灣](../Category/1993年台灣.md "wikilink")
[Category:機械故障觸發的航空事故](../Category/機械故障觸發的航空事故.md "wikilink")
[Category:1993年10月](../Category/1993年10月.md "wikilink")
[Category:高雄市歷史](../Category/高雄市歷史.md "wikilink")
[Category:小港區](../Category/小港區.md "wikilink")
[Category:MD-82航空事故](../Category/MD-82航空事故.md "wikilink")