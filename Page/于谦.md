**-{于}-謙**（），[字](../Page/表字.md "wikilink")**廷益**，[号](../Page/号.md "wikilink")**节庵**，[谥](../Page/谥号.md "wikilink")**忠肃**，[明朝重臣](../Page/明朝.md "wikilink")，[浙江](../Page/浙江等处承宣布政使司.md "wikilink")[錢塘縣](../Page/钱塘县.md "wikilink")（今[浙江省](../Page/浙江省.md "wikilink")[杭州市](../Page/杭州市.md "wikilink")）人。官至[兵部尚书](../Page/明朝兵部尚书.md "wikilink")。

\-{于}-謙由[進士出身](../Page/進士.md "wikilink")，因參與平定漢王[朱高煦謀反有功](../Page/朱高煦.md "wikilink")，得到[明宣宗器重](../Page/明宣宗.md "wikilink")，擔任明朝[山西河南巡撫](../Page/山西河南巡撫.md "wikilink")。[明英宗時期](../Page/明英宗.md "wikilink")，因得罪[王振下獄](../Page/王振.md "wikilink")，后釋放，起為[兵部侍郎](../Page/兵部侍郎.md "wikilink")。[土木堡之變后](../Page/土木堡之變.md "wikilink")，-{于}-謙繼任兵部尚書，指挥明军取得[京師保衛戰的勝利](../Page/京師保衛戰.md "wikilink")。代宗朝，-{于}-謙官至[少保](../Page/明朝三孤.md "wikilink")、[太子太傅](../Page/东宫三师.md "wikilink")，世稱**-{于}-少保**。英宗發動[奪門之變并成功复辟后](../Page/夺门之变.md "wikilink")，-{于}-謙被诬陷下狱而冤死。[成化年間获得平反](../Page/成化.md "wikilink")。現[北京市](../Page/北京市.md "wikilink")、[杭州](../Page/杭州市.md "wikilink")[西湖旁均有紀念](../Page/西湖.md "wikilink")-{于}-謙的祠堂、故居。

## 生平

### 早年政途

《[明史](../Page/明史.md "wikilink")》记载，-{于}-謙七歲時，有位僧人以其不凡，稱之為“他日救时宰相”。[永樂十九年](../Page/永乐_\(明朝\).md "wikilink")（1421年），-{于}-謙登辛丑科[进士](../Page/进士.md "wikilink")。[宣德初年](../Page/宣德.md "wikilink")，授職[監察御史](../Page/監察御史.md "wikilink")。在與[明宣宗上奏對答時](../Page/明宣宗.md "wikilink")，言談博雅流暢，宣宗為之傾聽。[顧佐擔任](../Page/顧佐_\(都御史\).md "wikilink")[都御史時](../Page/都御史.md "wikilink")，對屬僚往往非常嚴厲，卻惟獨尊讓-{于}-謙，認為他的才華勝過自己。[朱高煦謀反時](../Page/朱高煦.md "wikilink")，-{于}-謙跟隨宣宗[朱瞻基親征](../Page/朱瞻基.md "wikilink")[樂安](../Page/樂安.md "wikilink")，朱高煦出城投降，宣宗命于謙口頭列數朱高煦罪狀。-{于}-謙嚴詞正氣嚴切，厲聲威嚴激烈。朱高煦趴在地上發抖，稱罪該萬死。宣宗對此十分滿意。大军班师后，-{于}-謙得賞與各位大臣相同\[1\]\[2\]。

### 治理地方事務

\-{于}-謙隨後被重用巡按江西等地，獲得昭雪鳴冤的囚犯達數百人。-{于}-謙并上書奏報在陝西各處欺壓百姓的各級低級文武官吏，宣宗并命派御史逮捕。因此皇帝知道-{于}-謙能夠託付大任，正值當時增設各部右侍郎任各地巡撫，於是宣宗親自寫上-{于}-謙名字交予吏部，-{于}-謙因此越級升遷任[兵部右侍郎](../Page/兵部侍郎.md "wikilink")，[巡撫河南](../Page/河南巡撫.md "wikilink")、[山西等地](../Page/山西巡撫.md "wikilink")\[3\]。-{于}-謙抵達官所后，就輕裝騎馬全面視察其管轄的各地，尋訪父老鄉親，巡查各需需要重建與革除之處，并據實上疏奏請。稍有水旱災害，他就上報請救，一年中多達數次\[4\]。

\-{于}-謙莅任后，体察民情，勤于建言。[正統六年](../Page/正统_\(年号\).md "wikilink")（1440年），他上疏稱：

[英宗詔令施行其建議](../Page/明英宗.md "wikilink")。當時，河南靠近黃河的地方，不時有決口出現。-{于}-謙命令加厚修築堤壩，按里程設立亭，每亭設立亭長，负责督促修築維護，并命令種樹鑿井，以方便旅人。當時[大同孤零零地隔離于塞外](../Page/大同市.md "wikilink")，負責按察山西的官員無法按時抵達，-{于}-謙遂上疏另外設立御史管理，始為[大同巡撫建制](../Page/大同巡撫.md "wikilink")。此外，他还建议全部强取邊疆官員私自開墾的田地為官屯，用其資助邊防用度。他的威望惠德因此到處傳播，[太行一带的盜賊都纷纷避開藏匿](../Page/太行山.md "wikilink")。于謙任職九年后，[正统十三年](../Page/正统_\(年号\).md "wikilink")（1448年），升遷為兵部左侍郎，享受二品俸祿。\[5\]\[6\]

英宗初期，[三杨](../Page/三杨_\(明朝\).md "wikilink")（[杨士奇](../Page/杨士奇.md "wikilink")、[杨荣](../Page/杨荣.md "wikilink")、[杨溥](../Page/杨溥_\(明朝\).md "wikilink")）主持朝政，很器重-{于}-謙。于谦所奏请的内容，上午呈報下午就獲批復許可，此均為三楊所主持。而-{于}-謙每次到京師議事，均是空著口袋，各權貴不能沒有怨恨。三楊陸續去世后，太監[王振開始干政](../Page/王振.md "wikilink")，恰逢有姓名與-{于}-謙相似的御史曾得罪過王振。-{于}-謙入朝時，舉薦參政[王來](../Page/王來.md "wikilink")、[孫原貞代替自己](../Page/孫原貞.md "wikilink")。[通政使](../Page/通政使.md "wikilink")[李錫迎合王振意圖](../Page/李錫_\(通政使\).md "wikilink")，於是彈劾-{于}-謙因長期不升遷而怨恨，擅自舉薦他人代替自己。奏摺下交到三司審查，-{于}-謙被判處死罪，關在獄中三月。不久，王振自知失誤，-{于}-謙因此得以釋放，貶官為[大理寺少卿](../Page/大理寺.md "wikilink")\[7\]\[8\]。

\-{于}-謙被貶期間，山東、河南的官員百姓數千人均上疏請求留用他，而身在两省的明朝宗室周王[朱有爝](../Page/朱有爝.md "wikilink")、晋王[朱鍾鉉也附同此議](../Page/朱鍾鉉.md "wikilink")，英宗於是又命-{于}-謙再任巡撫。當時，山東、陝西因受災，流亡至河南的飢民就有二十多萬，-{于}-謙請求調發[河南府](../Page/洛阳市.md "wikilink")、[懷慶府所積蓄的粟米賑災](../Page/懷慶府.md "wikilink")，并上奏請求命時任[布政使的](../Page/承宣布政使司布政使.md "wikilink")[年富採取安民之策](../Page/年富.md "wikilink")，給予土地供給耕牛、穀種，并派里長監督檢查。-{于}-謙在地方任職前後十九年，每逢父母喪事，都在回家治喪后隨即被起用再任\[9\]。

### 臨危受命

<table>
<tbody>
<tr class="odd">
<td><div style="text-align: center;">
<p>《石灰吟》<br />
<br />
千錘萬擊出深山，烈火焚燒若等閒。<br />
粉骨碎身全不怕，要留清白在人間。</p>
</div></td>
</tr>
</tbody>
</table>

正統十三年（1448年），明朝邊防吃緊，-{于}-謙被召入京師，擔任[兵部左侍郎](../Page/兵部侍郎.md "wikilink")。第二年秋，蒙古[也先率軍大舉進犯明朝](../Page/也先.md "wikilink")，[王振唆使英宗親征](../Page/王振.md "wikilink")。[明朝兵部尚書](../Page/明朝兵部尚書.md "wikilink")[鄺埜與](../Page/鄺埜.md "wikilink")-{于}-謙均極力勸阻，英宗不聽。鄺埜只能跟隨英宗親征處理軍務，而留下-{于}-謙管理[兵部事務](../Page/兵部.md "wikilink")。同年發生[土木堡之變](../Page/土木之变.md "wikilink")，英宗被俘，百官身亡，明軍大敗并主力喪盡\[10\]。在得知英宗被俘后，京師大為震驚，百官不知該如何。負責監國的郕王[朱祁钰召集群臣商議攻防之策](../Page/明代宗.md "wikilink")。[翰林院](../Page/翰林院.md "wikilink")[侍講](../Page/侍講學士.md "wikilink")[徐珵称其观星象有變](../Page/徐有貞.md "wikilink")，应当向南迁都避敵。-{于}-謙听后，高声厲喝道：“主張南遷的人。應該[斬首](../Page/斬首.md "wikilink")。京師是天下的根本，一旦[遷都將大勢而去](../Page/遷都.md "wikilink")。難道沒看見[宋朝南遷的歷史么](../Page/宋朝.md "wikilink")？”郕王听后称是，于是留守策略始定\[11\]。

然而，明朝京師的精锐部队均在[土木堡陣亡](../Page/土木堡.md "wikilink")，所餘的疲憊不堪的殘軍不足十万，人心恐慌，官員百姓都沒有堅守的信心。-{于}-謙請求郕王發佈檄文集合[順天府](../Page/順天府.md "wikilink")、[應天府](../Page/应天府.md "wikilink")、河南的備操軍，山東和[南直隸沿海的備倭軍](../Page/南直隶.md "wikilink")，江北和應天府各地的運糧軍，立即奔赴順天府，依次經營籌畫部署，人心遂稍稍安定。隨後，-{于}-謙當即升任[兵部尚書](../Page/明朝兵部尚書.md "wikilink")，全权负责筹划京师防御\[12\]。

在一邊籌畫備戰方略同時，明朝廷臣呼籲嚴懲土木之變禍首王振及其餘黨的行動也開展。一日，郕王朱祁钰攝朝朝议时，[右都御史](../Page/都御史.md "wikilink")[陳鎰上奏请求將王振](../Page/陳鎰.md "wikilink")[滅族](../Page/滅族.md "wikilink")，廷臣纷纷响应。朱祁钰无法做决定，于是下令擇時改議，廷臣则抗议不依。此时，王振党羽、[锦衣卫都指挥使](../Page/锦衣卫.md "wikilink")[馬順站出叱斥百官](../Page/馬順.md "wikilink")。[户科](../Page/六科.md "wikilink")[给事中](../Page/给事中.md "wikilink")[王竑突然帶頭在朝廷上猛击馬順](../Page/王竑.md "wikilink")，众臣纷纷跟随，馬順當即斃命，一時血濺朝堂，而士卒亦聲洶欲誅。郕王朱祁钰看後大惧，欲起身离去，-{于}-謙挤到郕王身前，扶臂勸導道：“马顺等人罪当死，请不要追罪于各位大臣。”众人听后方止，而此时于谦的袍袖已经裂开\[13\]。在他退出左掖門时，[吏部尚书](../Page/吏部尚書.md "wikilink")[王直握着于谦的手叹道](../Page/王直_\(明朝官员\).md "wikilink")：“国家正是倚仗您的時候。今天这样的情况，即使是一百个王直也处理不了啊！”\[14\]
在那時，朝廷上下都倚重-{于}-謙，而-{于}-謙亦毅然以社稷安危為己任\[15\]。

### 北京保衛戰

因为英宗被俘、明朝无主、太子又年幼、瓦剌大軍又逼近，明朝大臣纷纷请[皇太后立郕王](../Page/孝恭章皇后.md "wikilink")[朱祁鈺为国君](../Page/明代宗.md "wikilink")。郕王听后再三推辭，-{于}-謙大聲說：“臣等實在是憂慮國家，並非為了私人打算。”郕王於是接受此議\[16\]。同年九月，朱祁钰即位，為明景帝。-{于}-謙入朝對答陳述京師防衛策略，他激動地哭泣着說：

{{Cquote|
敵寇得意，要挟持扣留太上皇，这样形势下他們必然輕視我大明，長驅直入到南方。所以請求朝廷命令各邊防駐守大臣協力防守阻擊。京城軍隊的武器幾乎用盡，應當馬上各分道募集部隊，并命工部修理武器盔甲。此外，亦派遣都督[孫鏜](../Page/孫鏜.md "wikilink")、[衛穎](../Page/衛穎.md "wikilink")、\[\[張軏|張


[Category:明朝監察御史](../Category/明朝監察御史.md "wikilink")
[Category:明朝大理寺少卿](../Category/明朝大理寺少卿.md "wikilink")
[Category:明朝山西河南巡撫](../Category/明朝山西河南巡撫.md "wikilink")
[Category:明朝兵部尚書](../Category/明朝兵部尚書.md "wikilink")
[Category:明朝太子三少](../Category/明朝太子三少.md "wikilink")
[Category:明朝三孤](../Category/明朝三孤.md "wikilink")
[Category:明朝军事家](../Category/明朝军事家.md "wikilink")
[Category:明朝被處決者](../Category/明朝被處決者.md "wikilink")
[Category:道教神祇](../Category/道教神祇.md "wikilink")
[Category:人物神](../Category/人物神.md "wikilink")
[Category:杭州人](../Category/杭州人.md "wikilink")
[Q謙](../Category/于姓.md "wikilink")
[Category:諡忠肅](../Category/諡忠肅.md "wikilink")
[Category:葬于杭州](../Category/葬于杭州.md "wikilink")

1.  [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“于謙，字廷益，錢塘人。生七歲，有僧奇之曰：「他日救時宰相也。」舉永樂十九年進士。宣德初，授御史。奏對，音吐鴻暢，帝為傾聽。顧佐為都御史，待寮屬甚嚴，獨下謙，以為才勝己也。扈蹕樂安，高煦出降，帝命謙口數其罪。謙正詞嶄嶄，聲色震厲。高煦伏地戰慄，稱萬死。帝大悅。師還，賞賚與諸大臣等。”
2.  [明](../Page/明朝.md "wikilink")·[焦竑編](../Page/焦竑.md "wikilink")，《[國朝獻徵錄](../Page/國朝獻徵錄.md "wikilink")·兵部尚書于公謙傳》（卷38，[王世貞書](../Page/王世贞.md "wikilink")）：“扈蹕下樂安州漢王高煦既面縛降上卒命謙數其罪謙肆口而成其言皆正大剴切高煦俯首戰慄稱萬七而已上大悅還師第賞鈔幣與大臣同”
3.  [明](../Page/明朝.md "wikilink")·[焦竑編](../Page/焦竑.md "wikilink")，《[國朝獻徵錄](../Page/國朝獻徵錄.md "wikilink")·兵部尚書于公謙傳》（卷38，[王世貞書](../Page/王世贞.md "wikilink")）：“入之議增補各部右侍郎治諸鎮糧稅兼練卒撫民上手書謙姓名授吏部遂超拜兵部右侍郎兼河河南山西奉璽書以往時年僅三十三時人榮之”
4.  [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“出按江西，雪冤囚數百。疏奏陝西諸處官校為民害，詔遣御史捕之。帝知謙可大任，會增設各部右侍郎為直省巡撫，乃手書謙名授吏部，超遷兵部右侍郎，巡撫河南、山西。謙至官，輕騎徧歷所部，延訪父老，察時事所宜興革，即具疏言之。一歲凡數上，小有水旱，輒上聞。”
5.  [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“詔行之。河南近河處，時有衝決。謙令厚築隄障，計里置亭，亭有長，責以督率修繕。並令種樹鑿井，榆柳夾路，道無渴者。大同孤懸塞外，按山西者不及至，奏別設御史治之。盡奪鎮將私墾田為官屯，以資邊用。威惠流行，太行伏盜皆避匿。在官九年，遷左侍郎，食二品俸。”
6.  [明](../Page/明朝.md "wikilink")·[焦竑編](../Page/焦竑.md "wikilink")，《[國朝獻徵錄](../Page/國朝獻徵錄.md "wikilink")·兵部尚書于公謙傳》（卷38，[王世貞書](../Page/王世贞.md "wikilink")）：“謙感上知遇夙夜拊循郡邑延訪父老以便益病苦歲饒則多出官鏹糴民粟歸庾儉則吐庾粟減直以糶公私得相贍而於下尤利齊秦民飢徙入河南者謙令邑各給田初與之牛種而以次責其稅毋令與土著淆河勢將潰謙厚築是障之多植榆柳其上五里有亭亭有長暨卒責以修補乃至所過經由官道俱責種樹間鑿井以蔭行者而飲渴者其在山西則以大同在塞外巡按御史不能至其地往往玩狎請別設御史併上谷治之而又盡奪大同鎮將之役卒墾私田者?官屯邊用充溢謙于吏術民事?所不精剴而尤以足兵食明舉措振綱紀”
7.  [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“初，三楊在政府，雅重謙。謙所奏，朝上夕報可，皆三楊主持。而謙每議事京師，空橐以入，諸權貴人不能無望。及是，三楊已前卒，太監王振方用事，適有御史姓名類謙者，嘗忤振。謙入朝，薦參政王來、孫原貞自代。通政使李錫阿振指，劾謙以久不遷怨望，擅舉人自代。下法司論死，繫獄三月。已而振知其誤，得釋，左遷大理寺少卿。”
8.  [明](../Page/明朝.md "wikilink")·[焦竑編](../Page/焦竑.md "wikilink")，《[國朝獻徵錄](../Page/國朝獻徵錄.md "wikilink")·兵部尚書于公謙傳》（卷38，[王世貞書](../Page/王世贞.md "wikilink")）：“當是時居政本者三楊皆重謙所奏請朝上毋不夕報可以是得行其志它措署多遂著?甲令滿九歲遷左侍郎食二品俸得封其父母謙既在官久威惠流聞嘗輕騎自河內歷太行而盜有窺者謙厲聲叱之皆大驚散走曰不知?我公死罪幸赦我謙數當入朝議事人謂即不槖金往寧無一二土物如合薌乾菌褁頭之類足以充內交際耶謙笑而兩舉其袖曰吾惟有清風而已且交際物之幾何而閭閻短長可畏也因賦詩見志入朝舉參政孫原貞王來自代是時三楊前後卒而中貴人王振持秉勢張甚以謙無私謁嗛之勒給事中廷劾謙以久不遷怨望擅舉人自代亡人臣禮下法司論斬繫”
9.  [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“山西、河南吏民伏闕上書，請留謙者以千數，周、晉諸王亦言之，乃復命謙巡撫。時山東、陝西流民就食河南者二十餘萬，謙請發河南、懷慶二府積粟以振。又奏令布政使年富安集其眾，授田給牛種，使里老司察之。前後在任十九年，丁內外艱，皆令歸治喪，旋起復。”
10. [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“十三年以兵部左侍郎召。明年秋，也先大入寇，王振挾帝親征。謙與尚書鄺埜極諫，不聽。埜從治兵，留謙理部事。”
11. [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“及駕陷土木，京師大震，眾莫知所為。郕王監國，命羣臣議戰守。侍講徐珵言星象有變，當南遷。謙厲聲曰：「言南遷者，可斬也。京師天下根本，一動則大事去矣，獨不見宋南渡事乎！」王是其言，守議乃定。”
12. [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“時京師勁甲精騎皆陷沒，所餘疲卒不及十萬，人心震恐，上下無固志。謙請王檄取兩京、河南備操軍，山東及南京沿海備倭軍，江北及北京諸府運糧軍，亟赴京師。以次經畫部署，人心稍安。即遷本部尚書。”
13. [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“郕王方攝朝，廷臣請族誅王振。而振党馬順者，輒叱言官。於是給事中王竑廷擊順，眾隨之。朝班大亂，衛卒聲洶洶。王懼欲起，謙排眾直前掖王止，且啟王宣諭曰：「順等罪當死，勿論。」眾乃定。謙袍袖為之盡裂。”
14. [明](../Page/明朝.md "wikilink")·[焦竑編](../Page/焦竑.md "wikilink")，《[國朝獻徵錄](../Page/國朝獻徵錄.md "wikilink")·兵部尚書于公謙傳》（卷38，[王世貞書](../Page/王世贞.md "wikilink")）：“正統己巳虜酋也先既破略獨石馬營諸鎮至秋益猖獗振遂挾上下詔親征謙與鄺埜上言也先醜豎子耳諸邊將士足制之陛下?宗廟社稷主奈何不自重奉以與犬羊角乎請毋煩六師上不聽埜乃從治兵而留謙治部事車駕至土木蒙塵報至京師大震皇太子幼不能監國大后乃命郕王攝政以輔之王御左順門時中貴人振雖巳歿虜中外恨而欲食其肉於是九卿臺諫廷劾振罪請用反法於其家奏未竟而錦衣指揮馬順者振黨也妄傳王旨叱眾退給事中王竑起直前擒順曰此正所謂翼虎者今日猶敢爾眾爭捶之立死又捶二閹之嘗私振者死時衞卒聲洶洶王懼欲退諸大臣皆披靡有趨匿者公獨直前掖王且啟王下令曰捶順與二閹死者義激無罪少選俟得請皇太后即族振且籍順等家眾姑退於是王乃起謙徐徐步出掖門吏部尚書王直者最？篤老臣執謙手而歎曰朝廷正藉公耳今日雖百王直何能？”
15. [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“退出左掖門，吏部尚書王直執謙手歎曰「國家正賴公耳。今日雖百王直何能為！」當是時，上下皆倚重-{于}-謙，謙亦毅然以社稷安危為己任。”
16. [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷170）：“初，大臣憂國無主，太子方幼，寇且至，請皇太后立郕王。王驚謝至再。謙颺言曰：「臣等誠憂國家，非為私計。」王乃受命。”