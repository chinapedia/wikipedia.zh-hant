**駒都英二**\[1\]（，こまつえーじ）是一位[日本](../Page/日本.md "wikilink")[插畫家](../Page/插畫家.md "wikilink")、原畫家，亦以[筆名](../Page/筆名.md "wikilink")****（CO2A）進行[同人活動](../Page/同人.md "wikilink")。[神奈川県出身](../Page/神奈川県.md "wikilink")\[2\]

## 畫風

駒都擅長繪畫[MS少女等](../Page/MS少女.md "wikilink")[兵器娘](../Page/兵器娘.md "wikilink")，而每個角色均有對少女和兵器的詳細設定解說。

此外，駒都畫中的女生常常都是穿著很短的裙子，擺出容易[走光的姿勢](../Page/走光_\(行為\).md "wikilink")，而且暴露大腿甚至是臀部，卻沒有走光，或沒有繪畫[內褲](../Page/內褲.md "wikilink")。結果引起網路上對其繪畫的角色是否有穿內褲的激烈討論。部分華語地區的網民則把其畫風戲稱為「薛丁格的內褲」（源自[薛丁格貓](../Page/薛丁格貓.md "wikilink")）\[3\]。

## 作品列表

**駒都英二**名義

  - [愛的魔法](../Page/愛的魔法.md "wikilink")（[插畫](../Page/插畫.md "wikilink")）
  - [伊里野的天空、UFO的夏天](../Page/伊里野的天空、UFO的夏天.md "wikilink")（插畫）
  - [南方之島·夢境之中](../Page/南方之島·夢境之中.md "wikilink")（插畫）
  - [發明工坊](../Page/發明工坊.md "wikilink")（[角色設定](../Page/角色設定.md "wikilink")）
  - [星之夢](../Page/星之夢.md "wikilink")（[人物設定](../Page/人物設定.md "wikilink")、[原畫](../Page/原畫.md "wikilink")）
  - [發明工坊2](../Page/發明工坊.md "wikilink")（角色設定、CG監修）
  - [部落格妖精Cocolo](../Page/部落格妖精Cocolo.md "wikilink")（角色設定、原畫）
  - [超級機器人學園](../Page/超級機器人學園.md "wikilink")（角色設定）
  - [絕境行星](../Page/絕境行星.md "wikilink")（插畫）
  - [青春紀行](../Page/青春紀行.md "wikilink")（插畫）
  - [發明工坊3](../Page/發明工坊.md "wikilink")（角色設定）

**CO2A**名義

  - 《POWDER SNOW》（角色設定、原畫）
  - 《H ～終會到達的地方～》（）（角色設定、原畫）
  - 《小魔女 a la mode》（）（角色設定、原畫）
  - 《21 -TwoOne-》（角色設定、原畫）
  - 《[旋風管家](../Page/旋風管家.md "wikilink")
    [交換卡片遊戲](../Page/交換卡片遊戲.md "wikilink")》（插畫）

**畫集**

  - 《駒都英二畫集@工畫堂工作室》（）
      -
        收錄[發明工坊](../Page/發明工坊.md "wikilink")Ⅰ、Ⅱ範圍，包括OVA、機械設定等插圖。

## 註釋

<div class="references-small">

<references />

</div>

## 参考文献

### 書籍

著者不明、「駒都英二」、久保田賢二編《ILLUSTRATION 2013》 Sotec 2013年，90 - 91頁。ISBN
978-4-88166-888-7。

## 外部連結

  - [Passing Rim](http://www.maroon.dti.ne.jp/co2a/)

[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:電子遊戲繪圖及原畫家](../Category/電子遊戲繪圖及原畫家.md "wikilink")

1.  《[發明工坊](../Page/發明工坊.md "wikilink")》OVA台灣動畫代理商[勝利國際多媒體的譯名](../Page/勝利國際多媒體.md "wikilink")。資料來源：[電玩
    e
    世代](http://egame.ezla.com.tw/?REQUEST_ID=cGFnZT1hbmltYXRlZGRldGFpbCZNVklEPTEwNA==)

2.  著者不明、「駒都英二」、久保田賢二編《ILLUSTRATION 2013》 Sotec 2013年，90 - 91頁。ISBN
    978-4-88166-888-7。
3.  [KomicaWiki－薛丁格的內褲](http://wiki.komica.org/wiki/?%E8%96%9B%E4%B8%81%E6%A0%BC%E7%9A%84%E5%85%A7%E8%A4%B2)