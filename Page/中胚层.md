**中胚层**（mesoderm）是[胚胎中位于内外胚层之间的](../Page/胚胎.md "wikilink")[胚层](../Page/胚层.md "wikilink")。在绘图中，传统上用红色表示。

[人类的中胚层在胚胎](../Page/人类.md "wikilink")[发育第三周出现](../Page/发育.md "wikilink")，[原条的形成代表着中胚层的出现](../Page/原条.md "wikilink")。

并非所有[动物胚胎都有中胚层](../Page/动物.md "wikilink")。只有[两侧对称动物才有](../Page/两侧对称动物.md "wikilink")[三胚层](../Page/三胚层.md "wikilink")。

以下[器官由中胚层形成](../Page/器官.md "wikilink")：

  - [真皮](../Page/真皮.md "wikilink")
  - [骨](../Page/骨.md "wikilink")
  - [骨骼肌](../Page/骨骼肌.md "wikilink")
  - [结缔组织](../Page/结缔组织.md "wikilink")
  - [内臟的](../Page/内臟.md "wikilink")[平滑肌](../Page/平滑肌.md "wikilink")
  - [心臟](../Page/心臟.md "wikilink")
  - [血管](../Page/血管.md "wikilink")
  - [血细胞](../Page/血细胞.md "wikilink")
  - [脾](../Page/脾.md "wikilink")
  - [淋巴结](../Page/淋巴结.md "wikilink")
  - [淋巴管](../Page/淋巴管.md "wikilink")
  - [肾](../Page/肾.md "wikilink")
  - [生殖系统](../Page/生殖系统.md "wikilink")
  - [腹膜](../Page/腹膜.md "wikilink")

## 划分

  - 胚内中胚层
      - 轴中胚层（[脊索](../Page/脊索.md "wikilink")）
      - [脊索旁中胚层](../Page/脊索.md "wikilink")（[肌节](../Page/肌节.md "wikilink")；[生骨节](../Page/生骨节.md "wikilink")，[生肌节](../Page/生肌节.md "wikilink")，[生皮节](../Page/生皮节.md "wikilink")）
      - 间介中胚层（[泌尿系统](../Page/泌尿系统.md "wikilink")、[生殖系统](../Page/生殖系统.md "wikilink")）
      - 侧中胚层（[壁层](../Page/壁层.md "wikilink")，[脏层](../Page/脏层.md "wikilink")）
  - 胚外中胚层
      - 包绕 [胚外体腔](../Page/胚外体腔.md "wikilink")

中胚层和[间充质并非同一概念](../Page/间充质.md "wikilink")。中胚层是[胚胎学概念](../Page/胚胎学.md "wikilink")，而间充质却是[组织学概念](../Page/组织学.md "wikilink")。

## 延伸閱讀

  -
  -
  -
  -
  -
  -
## 外部連結

[Category:發育生物學](../Category/發育生物學.md "wikilink")
[Category:胚胎学](../Category/胚胎学.md "wikilink")
[Category:原腸化](../Category/原腸化.md "wikilink")