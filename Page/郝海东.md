**郝海東**（），[中国](../Page/中国.md "wikilink")[山东](../Page/山东.md "wikilink")[青岛人](../Page/青岛.md "wikilink")，中國[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，曾經効力於[八一及](../Page/八一足球俱乐部.md "wikilink")[大连实德兩家](../Page/大连实德.md "wikilink")[足球會](../Page/足球會.md "wikilink")，亦曾經多次入選[中國國家足球隊](../Page/中國國家足球隊.md "wikilink")，是1990年代中国著名球员。

## 生平

1980年10月，郝海东入选八一少年足球队，1986年，进入八一队効力。1992年，郝海东开始入选[中国国家足球队](../Page/中国国家足球队.md "wikilink")，并且参加同年的[亚洲杯](../Page/1992年亞洲盃足球賽.md "wikilink")。

1995年，郝海东曾經有机会留洋，但是当时郝海东还是八一队的球员，带着[军籍](../Page/军籍.md "wikilink")，不能夠到外国踢球。1997年1月，以220万元从[八一转会到](../Page/八一足球俱乐部.md "wikilink")[大连万达](../Page/大连万达.md "wikilink")。同年[甲A联赛第](../Page/甲A联赛.md "wikilink")17轮，郝海东在代表大连万达与[广东宏远的对决中打进](../Page/广东宏远.md "wikilink")4球，帮助球队5-2取胜，这也是中国职业足球联赛史上的第一顶[帽子戏法](../Page/帽子戏法.md "wikilink")。

1999年2月，[亚洲足联因郝海东在](../Page/亚洲足联.md "wikilink")1998年12月[亚运会上对裁判有不礼貌行为](../Page/亚运会.md "wikilink")，对其作出禁赛一年并罚款2000美元的处罚。

2002年，郝海东参加[日韩世界杯](../Page/2002年世界杯足球赛.md "wikilink")。2004年，郝海东以34岁的高龄继续把持中国队头号前锋交椅参加在中国本土举行的[亚洲杯在半决赛与人相撞](../Page/2004年亚洲杯足球赛.md "wikilink")，郝海东头破血流，不得不中途下场，但是郝海东还是裹绷带参加了3天后与日本队的决赛，同年11月中国在2006年世界杯预选赛出局后郝海东退出国家队。郝海东共代表国家队出赛115场共打进41球，其中103场为国际级赛打进37个国际赛进球。

2005年1月從[大连实德以](../Page/大连实德.md "wikilink")1英鎊的轉會費，轉投当时[英格蘭超級聯賽球隊](../Page/英格蘭超級聯賽.md "wikilink")[谢菲尔德联](../Page/谢菲尔德联足球俱乐部.md "wikilink")，球衣號碼為39號，但他从未替[谢菲尔德联在聯賽上過陣](../Page/谢菲尔德联足球俱乐部.md "wikilink")。

郝海東曾為[湖南湘軍的董事長](../Page/湖南湘軍.md "wikilink")。2007年底至2012年底担任足球俱乐部[天津松江的俱乐部总经理](../Page/天津松江.md "wikilink")。

## 荣誉

### 俱乐部

  -
    [大连万达](../Page/大连万达.md "wikilink")/[大连实德](../Page/大连实德.md "wikilink")

<!-- end list -->

  - [中国足球甲A联赛冠军](../Page/中国足球甲A联赛.md "wikilink") (5):
    [1997](../Page/1997年中国足球甲A联赛.md "wikilink"),
    [1998](../Page/1998年中国足球甲A联赛.md "wikilink"),
    [2000](../Page/2000年中国足球甲A联赛.md "wikilink"),
    [2001](../Page/2001年中国足球甲A联赛.md "wikilink"),
    [2002](../Page/2002年中国足球甲A联赛.md "wikilink")
  - [中国足协杯](../Page/中国足协杯.md "wikilink") (1):
    [2001](../Page/2001年中国足协杯.md "wikilink")
  - [中国足球超霸杯冠军](../Page/中国足球协会超级杯.md "wikilink") (3): 1996, 2000, 2002

### 国家队

  -  中国国家队

<!-- end list -->

  - [亚洲杯足球赛](../Page/亚足联亚洲杯.md "wikilink")

<!-- end list -->

  -

      -
        亚军 (1): [2004](../Page/2004年亞洲盃足球賽.md "wikilink")
        季军 (1): [1992](../Page/1992年亞洲盃足球賽.md "wikilink")

<!-- end list -->

  - [亚运会](../Page/亚洲运动会足球比赛.md "wikilink")

<!-- end list -->

  -

      -
        铜牌 (1): [1998](../Page/1998年亞洲運動會足球比賽.md "wikilink")

### 个人

  - [中国足球先生](../Page/中国足球先生.md "wikilink") (1): 1998
  - [甲A联赛金靴奖](../Page/甲A联赛金靴奖.md "wikilink") (3): 1997, 1998, 2001
  - [中国十佳运动员](../Page/全国十名最佳运动员.md "wikilink") (1): 2001
  - [亚冠最佳射手](../Page/亚冠.md "wikilink") (1):
    [2002-03](../Page/2002年至2003年亚足联冠军联赛.md "wikilink")

### 国家队入球

<table>
<thead>
<tr class="header">
<th></th>
<th><p>时间</p></th>
<th><p>地点</p></th>
<th><p>对手</p></th>
<th><p>即时比分</p></th>
<th><p>比分</p></th>
<th><p>赛事</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>8 November 1992</p></td>
<td><p><a href="../Page/广岛广域公园陆上竞技场.md" title="wikilink">广岛广域公园陆上竞技场</a>, <a href="../Page/广岛.md" title="wikilink">广岛</a>, <a href="../Page/日本.md" title="wikilink">日本</a></p></td>
<td></td>
<td><p><strong>1</strong>–1</p></td>
<td><p>1–1 (4–3 <a href="../Page/Penalty_shoot-out_(association_football).md" title="wikilink">PSO</a>)</p></td>
<td><p><a href="../Page/1992_AFC_Asian_Cup.md" title="wikilink">1992 AFC Asian Cup</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>22 May 1993</p></td>
<td><p><a href="../Page/Al_Hassan_Stadium.md" title="wikilink">Al Hassan Stadium</a>, <a href="../Page/伊尔比德.md" title="wikilink">伊尔比德</a>, <a href="../Page/约旦.md" title="wikilink">约旦</a></p></td>
<td></td>
<td><p><strong>4</strong>–0</p></td>
<td><p>5–0</p></td>
<td><p><a href="../Page/1994_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">1994 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>12 June 1993</p></td>
<td><p><a href="../Page/成都.md" title="wikilink">成都</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>2</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/1994_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">1994 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>26 October 1995</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>2–1</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>30 January 1996</p></td>
<td><p><a href="../Page/旺角大球場.md" title="wikilink">旺角大球場</a>, <a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td></td>
<td><p><strong>6</strong>–1</p></td>
<td><p>7–1</p></td>
<td><p><a href="../Page/1996_AFC_Asian_Cup_qualification.md" title="wikilink">1996 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>1 February 1996</p></td>
<td><p><a href="../Page/旺角大球場.md" title="wikilink">旺角大球場</a>, <a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>7–0</p></td>
<td><p><a href="../Page/1996_AFC_Asian_Cup_qualification.md" title="wikilink">1996 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>1 February 1996</p></td>
<td><p><a href="../Page/旺角大球場.md" title="wikilink">旺角大球場</a>, <a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td></td>
<td><p><strong>2</strong>–0</p></td>
<td><p>7–0</p></td>
<td><p><a href="../Page/1996_AFC_Asian_Cup_qualification.md" title="wikilink">1996 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>1 February 1996</p></td>
<td><p><a href="../Page/旺角大球場.md" title="wikilink">旺角大球場</a>, <a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td></td>
<td><p><strong>4</strong>–0</p></td>
<td><p>7–0</p></td>
<td><p><a href="../Page/1996_AFC_Asian_Cup_qualification.md" title="wikilink">1996 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>28 June 1996</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>2–0</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>17 July 1996</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–1</p></td>
<td><p>1–1</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>25 September 1996</p></td>
<td><p><a href="../Page/首尔.md" title="wikilink">首尔</a>, <a href="../Page/韩国.md" title="wikilink">韩国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>1–3</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>26 November 1996</p></td>
<td><p><a href="../Page/广州.md" title="wikilink">广州</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–1</p></td>
<td><p>2–3</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>16 December 1996</p></td>
<td><p><a href="../Page/阿布扎比.md" title="wikilink">阿布扎比</a>, <a href="../Page/阿联酋.md" title="wikilink">阿联酋</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>3–4</p></td>
<td><p><a href="../Page/1996_AFC_Asian_Cup.md" title="wikilink">1996 AFC Asian Cup</a></p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>29 January 1997</p></td>
<td><p><a href="../Page/昆明.md" title="wikilink">昆明</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>2–1</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>23 February 1997</p></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a>, <a href="../Page/马来西亚.md" title="wikilink">马来西亚</a></p></td>
<td></td>
<td><p><strong>2</strong>–1</p></td>
<td><p>2–1</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>2 March 1997</p></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a>, <a href="../Page/马来西亚.md" title="wikilink">马来西亚</a></p></td>
<td></td>
<td><p><strong>3</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>20 April 1997</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>2</strong>–0</p></td>
<td><p>5–0</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>11 May 1997</p></td>
<td><p><a href="../Page/杜尚别.md" title="wikilink">杜尚别</a>, <a href="../Page/塔吉克斯坦.md" title="wikilink">塔吉克斯坦</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>1–0</p></td>
<td><p><a href="../Page/1998_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">1998 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>25 May 1997</p></td>
<td><p><a href="../Page/胡志明市.md" title="wikilink">胡志明市</a>, <a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td></td>
<td><p><strong>3</strong>–1</p></td>
<td><p>3–1</p></td>
<td><p><a href="../Page/1998_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">1998 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>22 June 1997</p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>2</strong>–0</p></td>
<td><p>4–0</p></td>
<td><p><a href="../Page/1998_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">1998 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>26 September 1997</p></td>
<td><p><a href="../Page/多哈.md" title="wikilink">多哈</a>, <a href="../Page/卡塔尔.md" title="wikilink">卡塔尔</a></p></td>
<td></td>
<td><p><strong>1</strong>–1</p></td>
<td><p>1–1</p></td>
<td><p><a href="../Page/1998_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">1998 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>10 October 1997</p></td>
<td><p><a href="../Page/科威特城.md" title="wikilink">科威特城</a>, <a href="../Page/科威特.md" title="wikilink">科威特</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>2–1</p></td>
<td><p><a href="../Page/1998_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">1998 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>6 November 1997</p></td>
<td><p><a href="../Page/利雅得.md" title="wikilink">利雅得</a>, <a href="../Page/沙特阿拉伯.md" title="wikilink">沙特阿拉伯</a></p></td>
<td></td>
<td><p><strong>1</strong>–1</p></td>
<td><p>1–1</p></td>
<td><p><a href="../Page/1998_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">1998 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>27 June 1998</p></td>
<td><p><a href="../Page/曼谷.md" title="wikilink">曼谷</a>, <a href="../Page/泰国.md" title="wikilink">泰国</a></p></td>
<td></td>
<td><p><strong>3</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>10 December 1998</p></td>
<td><p><a href="../Page/曼谷.md" title="wikilink">曼谷</a>, <a href="../Page/泰国.md" title="wikilink">泰国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>6–1</p></td>
<td><p><a href="../Page/Football_at_the_1998_Asian_Games.md" title="wikilink">1998 Asian Games</a></p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p>14 December 1998</p></td>
<td><p><a href="../Page/曼谷.md" title="wikilink">曼谷</a>, <a href="../Page/泰国.md" title="wikilink">泰国</a></p></td>
<td></td>
<td><p><strong>3</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/1998_Asian_Games.md" title="wikilink">1998 Asian Games</a></p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>16 January 2000</p></td>
<td><p><a href="../Page/广州.md" title="wikilink">广州</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>1–0</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p>23 January 2000</p></td>
<td><p>, <a href="../Page/胡志明市.md" title="wikilink">胡志明市</a>, <a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td></td>
<td><p><strong>4</strong>–0</p></td>
<td><p>8–0</p></td>
<td><p><a href="../Page/2000_AFC_Asian_Cup_qualification.md" title="wikilink">2000 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p>26 January 2000</p></td>
<td><p>, <a href="../Page/胡志明市.md" title="wikilink">胡志明市</a>, <a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>19–0</p></td>
<td><p><a href="../Page/2000_AFC_Asian_Cup_qualification.md" title="wikilink">2000 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p>26 January 2000</p></td>
<td><p>, <a href="../Page/胡志明市.md" title="wikilink">胡志明市</a>, <a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td></td>
<td><p><strong>9</strong>–0</p></td>
<td><p>19–0</p></td>
<td><p><a href="../Page/2000_AFC_Asian_Cup_qualification.md" title="wikilink">2000 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td><p>26 January 2000</p></td>
<td><p>, <a href="../Page/胡志明市.md" title="wikilink">胡志明市</a>, <a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td></td>
<td><p><strong>10</strong>–0</p></td>
<td><p>19–0</p></td>
<td><p><a href="../Page/2000_AFC_Asian_Cup_qualification.md" title="wikilink">2000 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p>26 January 2000</p></td>
<td><p>, <a href="../Page/胡志明市.md" title="wikilink">胡志明市</a>, <a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td></td>
<td><p><strong>12</strong>–0</p></td>
<td><p>19–0</p></td>
<td><p><a href="../Page/2000_AFC_Asian_Cup_qualification.md" title="wikilink">2000 AFC Asian Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td><p>5 August 2001</p></td>
<td><p><a href="../Page/上海.md" title="wikilink">上海</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">Friendly international</a></p></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td><p>25 August 2001</p></td>
<td><p><a href="../Page/沈阳奥林匹克体育中心.md" title="wikilink">沈阳奥林匹克体育中心</a>, <a href="../Page/沈阳.md" title="wikilink">沈阳</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>3</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/2002_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">2002 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td><p>13 October 2001</p></td>
<td><p><a href="../Page/沈阳奥林匹克体育中心.md" title="wikilink">沈阳奥林匹克体育中心</a>, <a href="../Page/沈阳.md" title="wikilink">沈阳</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>3</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/2002_FIFA_World_Cup_qualification_(AFC).md" title="wikilink">2002 FIFA World Cup qualifier</a></p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td><p>2004年2月3日</p></td>
<td><p><a href="../Page/天河体育场.md" title="wikilink">天河体育场</a>, <a href="../Page/广州.md" title="wikilink">广州</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>2</strong>–1</p></td>
<td><p>2–1</p></td>
<td><p><a href="../Page/Exhibition_game#Association_football.md" title="wikilink">友谊赛</a></p></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td><p>2004年2月18日</p></td>
<td><p><a href="../Page/天河体育场.md" title="wikilink">天河体育场</a>, <a href="../Page/广州.md" title="wikilink">广州</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>1–0</p></td>
<td><p><a href="../Page/2006年世界盃足球賽外圍賽-亞洲區.md" title="wikilink">2006世界盃预选赛</a></p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td><p>2004年3月31日</p></td>
<td><p><a href="../Page/小西湾运动场.md" title="wikilink">小西湾运动场</a>, <a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>1–0</p></td>
<td><p><a href="../Page/2006年世界盃足球賽外圍賽-亞洲區.md" title="wikilink">2006世界盃预选赛</a></p></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p>2004年6月9日</p></td>
<td><p><a href="../Page/泰达足球场.md" title="wikilink">泰达足球场</a>, <a href="../Page/天津.md" title="wikilink">天津</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>4–0</p></td>
<td><p><a href="../Page/2006年世界盃足球賽外圍賽-亞洲區.md" title="wikilink">2006世界盃预选赛</a></p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p>2004年7月21日</p></td>
<td><p><a href="../Page/工人体育场.md" title="wikilink">工人体育场</a>, <a href="../Page/北京.md" title="wikilink">北京</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>2</strong>–0</p></td>
<td><p>5–0</p></td>
<td><p><a href="../Page/2004年亚洲杯足球赛.md" title="wikilink">2004年亚洲杯足球赛</a></p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td><p>2004年7月30日</p></td>
<td><p><a href="../Page/工人体育场.md" title="wikilink">工人体育场</a>, <a href="../Page/北京.md" title="wikilink">北京</a>, <a href="../Page/中华人民共和国.md" title="wikilink">中国</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/2004年亚洲杯足球赛.md" title="wikilink">2004年亚洲杯足球赛</a></p></td>
</tr>
</tbody>
</table>

## 外部連結

[大羽追平海东96球纪录
即将独霸联赛第一射手王](https://web.archive.org/web/20070419012826/http://cn.sports.yahoo.com/07-04-/324/29g06.html)(2007-04-15)

[Category:青岛籍足球运动员](../Category/青岛籍足球运动员.md "wikilink")
[Category:中国海外足球运动员](../Category/中国海外足球运动员.md "wikilink")
[Category:中国国家足球队运动员](../Category/中国国家足球队运动员.md "wikilink")
[Category:八一足球俱樂部球員](../Category/八一足球俱樂部球員.md "wikilink")
[Category:大连实德球员](../Category/大连实德球员.md "wikilink")
[Category:錫菲聯球員](../Category/錫菲聯球員.md "wikilink")
[Haidong](../Category/郝姓.md "wikilink")
[Category:1992年亞洲盃足球賽球員](../Category/1992年亞洲盃足球賽球員.md "wikilink")
[Category:1996年亞洲盃足球賽球員](../Category/1996年亞洲盃足球賽球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年亞洲盃足球賽球員](../Category/2004年亞洲盃足球賽球員.md "wikilink")
[Category:FIFA世纪俱乐部](../Category/FIFA世纪俱乐部.md "wikilink")