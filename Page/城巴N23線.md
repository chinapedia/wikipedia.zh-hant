**城巴N23線**是[香港的一條通宵巴士路線](../Page/香港.md "wikilink")，來往[慈雲山](../Page/慈雲山.md "wikilink")（北）及[東涌站](../Page/東涌站_\(東涌綫\).md "wikilink")。

## 歷史

  - 1999年4月25日：投入服務，初時來往[藍田（北）及機場](../Page/藍田（北）巴士總站.md "wikilink")（地面運輸中心）
  - 1999年12月20日：配合機場通宵路線重組，改為[慈雲山（中）至東涌站](../Page/慈雲山（中）巴士總站.md "wikilink")，往東涌站方向改經[黃埔花園](../Page/黃埔花園.md "wikilink")、[油麻地](../Page/油麻地.md "wikilink")、[旺角](../Page/旺角.md "wikilink")、[大角咀及機場](../Page/大角咀.md "wikilink")，往慈雲山（中）方向改經機場及黃埔花園
  - 2001年1月7日：總站由慈雲山（中）延長至慈雲山（北）

## 服務時間（詳細班次）

  - 東涌站開：00:15、01:10
  - 慈雲山（北）開：03:35、04:35、05:05

## 收費

全程：$23（機場員工優惠票價：$20）

  - 青嶼幹線收費廣場往[慈雲山](../Page/慈雲山.md "wikilink")（北）：$20
  - [佐敦道往慈雲山](../Page/佐敦道.md "wikilink")（北）：$12
  - [九龍城迴旋處](../Page/世運公園.md "wikilink")（[太子道東](../Page/太子道東.md "wikilink")）往慈雲山（北）：$7
  - [青嶼幹線收費廣場往](../Page/青嶼幹線.md "wikilink")[東涌站](../Page/東涌站_\(東涌綫\).md "wikilink")：$13
  - [暢運路](../Page/暢運路.md "wikilink")（二號閘）往東涌站：$5

## 行車路線

**東涌站開**經：[達東路](../Page/達東路.md "wikilink")、[順東路](../Page/順東路.md "wikilink")、[赤鱲角南路](../Page/赤鱲角南路.md "wikilink")、[駿坪路](../Page/駿坪路.md "wikilink")、[駿運路](../Page/駿運路.md "wikilink")、[駿運路交匯處](../Page/駿運路交匯處.md "wikilink")、[航膳東路](../Page/航膳東路.md "wikilink")、駿運路交匯處、[觀景路](../Page/觀景路.md "wikilink")、[東岸路](../Page/東岸路.md "wikilink")、[機場路](../Page/機場路_\(香港\).md "wikilink")、[暢連路](../Page/暢連路.md "wikilink")、[機場（地面運輸中心）巴士總站](../Page/機場（地面運輸中心）巴士總站.md "wikilink")、暢連路、[暢達路](../Page/暢達路.md "wikilink")、[機場北交匯處](../Page/機場北交匯處.md "wikilink")、機場路、[機場南交匯處](../Page/機場南交匯處.md "wikilink")、機場路、[北大嶼山公路](../Page/北大嶼山公路.md "wikilink")、[青嶼幹線](../Page/青嶼幹線.md "wikilink")、[青衣西北交匯處](../Page/青衣西北交匯處.md "wikilink")、[長青公路](../Page/長青公路.md "wikilink")、[長青隧道](../Page/長青隧道.md "wikilink")、[青葵公路](../Page/青葵公路.md "wikilink")、[西九龍公路](../Page/西九龍公路.md "wikilink")、[連翔道](../Page/連翔道.md "wikilink")、[佐敦道](../Page/佐敦道.md "wikilink")、[加士居道](../Page/加士居道.md "wikilink")、[漆咸道南、漆咸道北](../Page/漆咸道.md "wikilink")、隧道、[機利士南路](../Page/機利士南路.md "wikilink")、[蕪湖街](../Page/蕪湖街.md "wikilink")、[德民街](../Page/德民街.md "wikilink")、[德安街](../Page/德安街.md "wikilink")、[黃埔花園巴士總站](../Page/黃埔花園巴士總站.md "wikilink")、[德康街](../Page/德康街.md "wikilink")、[紅磡道](../Page/紅磡道.md "wikilink")、[庇利街](../Page/庇利街.md "wikilink")、[新柳街](../Page/新柳街.md "wikilink")、漆咸道北、[馬頭圍道](../Page/馬頭圍道.md "wikilink")、[馬頭涌道](../Page/馬頭涌道.md "wikilink")、[太子道西、太子道東](../Page/太子道.md "wikilink")、[彩虹道](../Page/彩虹道.md "wikilink")、[蒲崗村道](../Page/蒲崗村道.md "wikilink")、[鳳德道](../Page/鳳德道.md "wikilink")、[斧山道](../Page/斧山道.md "wikilink")、蒲崗村道、慈雲山道、[惠華街](../Page/惠華街.md "wikilink")、[雲華街及](../Page/雲華街.md "wikilink")[慈雲山道](../Page/慈雲山道.md "wikilink")。

**慈雲山（北）開**經：慈雲山道、惠華街、雲華街、慈雲山道、蒲崗村道、斧山道、鳳德道、蒲崗村道、彩虹道、天橋、太子道東、太子道西、馬頭涌道、馬頭圍道、漆咸道北、蕪湖街、德民街、德安街、黃埔花園巴士總站、德康街、紅磡道、[紅磡繞道](../Page/紅磡繞道.md "wikilink")、[公主道連接路](../Page/公主道.md "wikilink")、[漆咸道南](../Page/漆咸道南.md "wikilink")、加士居道、[彌敦道](../Page/彌敦道.md "wikilink")、[亞皆老街](../Page/亞皆老街.md "wikilink")、[櫻桃街](../Page/櫻桃街.md "wikilink")、隧道、[海輝道](../Page/海輝道.md "wikilink")、連翔道、[荔灣交匯處](../Page/荔灣交匯處.md "wikilink")、西九龍公路、青葵公路、長青隧道、長青公路、青衣西北交匯處、青嶼幹線、北大嶼山公路、機場路、暢連路、暢達路、機場北交匯處、機場路、機場南交匯處、機場路、東岸路、觀景路、[駿明路](../Page/駿明路.md "wikilink")、觀景路、駿運路交匯處、航膳東路、駿運路交匯處、駿運路、駿坪路、赤鱲角南路、順東路及達東路。

### 沿線車站

[N23RtMap.png](https://zh.wikipedia.org/wiki/File:N23RtMap.png "fig:N23RtMap.png")

| [東涌站開](../Page/東涌站_\(東涌綫\).md "wikilink") | [慈雲山](../Page/慈雲山.md "wikilink")（北）開          |
| ----------------------------------------- | --------------------------------------------- |
| **序號**                                    | **車站名稱**                                      |
| 1                                         | [東涌站](../Page/東涌站_\(東涌綫\).md "wikilink")      |
| 2                                         | [赤鱲角南路](../Page/赤鱲角南路.md "wikilink")          |
| 3                                         | [亞洲空運中心](../Page/亞洲空運中心.md "wikilink")        |
| 4                                         | [機場空運中心](../Page/機場空運中心.md "wikilink")        |
| 5                                         | [香港空運貨站](../Page/超級一號貨站.md "wikilink")        |
| 6                                         | [赤鱲角消防局](../Page/香港消防局列表#新界西南區.md "wikilink") |
| 7                                         | [國泰空廚](../Page/國泰空廚.md "wikilink")            |
| 8                                         | [國泰城](../Page/國泰城.md "wikilink")              |
| 9                                         | 二號檢查閘                                         |
| 10                                        | [機場](../Page/香港國際機場.md "wikilink")（地面運輸中心）    |
| 11                                        | [暢達路](../Page/暢達路.md "wikilink")              |
| 12                                        | [富豪機場酒店](../Page/富豪機場酒店.md "wikilink")        |
| 13                                        | [青嶼幹線繳費廣場](../Page/青嶼幹線繳費廣場.md "wikilink")    |
| 14                                        | [柯士甸站](../Page/柯士甸站.md "wikilink")            |
| 15                                        | [上海街](../Page/上海街.md "wikilink")              |
| 16                                        | [志和街](../Page/志和街.md "wikilink")              |
| 17                                        | [衛理道](../Page/衛理道.md "wikilink")              |
| 18                                        | [觀音街](../Page/觀音街.md "wikilink")              |
| 19                                        | [德民街](../Page/德民街.md "wikilink")              |
| 20                                        | [黃埔花園](../Page/黃埔花園.md "wikilink")            |
| 21                                        | [民裕街](../Page/民裕街.md "wikilink")              |
| 22                                        | [鶴園東街](../Page/鶴園東街.md "wikilink")            |
| 23                                        | [崇潔街](../Page/崇潔街.md "wikilink")              |
| 24                                        | [山西街](../Page/山西街.md "wikilink")              |
| 25                                        | [江西街](../Page/江西街.md "wikilink")              |
| 26                                        | [天光道](../Page/天光道.md "wikilink")              |
| 27                                        | [馬頭圍邨洋葵樓](../Page/馬頭圍邨.md "wikilink")         |
| 28                                        | 亞皆老街遊樂場                                       |
| 29                                        | [富豪東方酒店](../Page/富豪東方酒店.md "wikilink")        |
| 30                                        | [天主教伍華中學](../Page/天主教伍華中學.md "wikilink")      |
| 31                                        | [東頭邨泰東樓](../Page/東頭邨.md "wikilink")           |
| 32                                        | 黃大仙警署                                         |
| 33                                        | [鳳德商場](../Page/鳳德邨#鳳德商場.md "wikilink")        |
| 34                                        | 志蓮淨苑                                          |
| 35                                        | [真鐸學校](../Page/真鐸學校.md "wikilink")            |
| 36                                        | [宏景花園](../Page/宏景花園.md "wikilink")            |
| 37                                        | 德愛中學                                          |
| 38                                        | [慈民邨](../Page/慈民邨.md "wikilink")              |
| 39                                        | 慈安苑                                           |
| 40                                        | [慈愛苑愛富閣](../Page/慈愛苑.md "wikilink")           |
| 41                                        | 慈雲山（北）                                        |
|                                           | 42                                            |
| 43                                        | [東涌纜車站](../Page/東涌站_\(昂坪360\).md "wikilink")  |
| 44                                        | 東涌站                                           |

## 參考資料

  - 《二十世紀巴士路線發展史－－渡海、離島及機場篇》，ISBN 9789628414680，BSI (香港)，156-157頁

## 外部連結

  - [Allnight Citybus Route
    城巴通宵路線－N23](http://www.681busterminal.com/n23.html)

[N023](../Category/城巴及新世界第一巴士路線.md "wikilink")
[N023](../Category/離島區巴士路線.md "wikilink")
[N023](../Category/黃大仙區巴士路線.md "wikilink")
[N023](../Category/機場巴士路線.md "wikilink")