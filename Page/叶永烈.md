**叶永烈**（），[汉族人](../Page/汉族.md "wikilink")，出生于[浙江](../Page/浙江.md "wikilink")[温州](../Page/温州.md "wikilink")，[中国](../Page/中国.md "wikilink")[科普及](../Page/科学普及.md "wikilink")[传记](../Page/传记.md "wikilink")[作家](../Page/作家.md "wikilink")。1963年毕业于[北京大学](../Page/北京大学.md "wikilink")[化学系](../Page/化学.md "wikilink")。现为[上海作家协会专业一级作家](../Page/中国作家协会.md "wikilink")，[中国科普作家协会常务理事](../Page/中国科普作家协会.md "wikilink")，[世界科幻小说协会理事](../Page/世界科幻小说协会.md "wikilink")，[香港海外文联名誉主席](../Page/香港.md "wikilink")。

## 生平

叶永烈在1978年8月发表了中国在[文革之后的第一部](../Page/文化大革命.md "wikilink")[科幻小说](../Page/科幻小说.md "wikilink")《[小灵通漫游未来](../Page/小灵通漫游未来.md "wikilink")》。这部作品写于1961年，书中预言了大量新技术及新发明，出版之后大获成功，初版即行销160余万册，在当时中国的青少年中引起了强烈的反响\[1\]。叶本人也因为这部小说及其续集声名鹊起，成为当时中国最著名的科幻与科普作家之一。但1983年开始的[清除精神污染运动严重影响了当时中国的科幻小说事业](../Page/清除精神污染.md "wikilink")。保守派声称，有不少科幻小说宣传[资产阶级自由化](../Page/资产阶级自由化.md "wikilink")，在青少年中造成了严重的精神污染\[2\]。叶本人的作品《黑影》也在1983年11月3日的《[中国青年报](../Page/中国青年报.md "wikilink")》上被点名批评。之后迫于种种压力，叶永烈被迫放弃了科幻小说的创作，转而写作[传记文学](../Page/传记文学.md "wikilink")，特别是[中共领导人如](../Page/中共.md "wikilink")[毛泽东等人的传记](../Page/毛泽东.md "wikilink")\[3\]。

1994年成为香港文学艺术家协会荣誉会员\[4\]。

## 作品

### 科普

  - 《[十万个为什么](../Page/十万个为什么.md "wikilink")》（叶永烈为作者之一）
  - 《[小灵通漫游未来](../Page/小灵通漫游未来.md "wikilink")》
  - 《[毛澤東復活](../Page/毛澤東復活.md "wikilink")》
  - 《叶永烈自选集》

### 传记

  - 《红色的起点》
  - 《历史选择了毛泽东》
  - 《[毛泽东与](../Page/毛泽东.md "wikilink")[蒋介石](../Page/蒋介石.md "wikilink")》
  - 《[邓小平改变中国](../Page/邓小平.md "wikilink")》
  - 《“[四人帮](../Page/四人帮.md "wikilink")”传》(包括四人的分卷)
      - 《[張春橋浮沉錄](../Page/張春橋浮沉錄.md "wikilink")》
      - 《[江青實錄](../Page/江青實錄.md "wikilink")》
  - 《[陈伯达传](../Page/陈伯达传.md "wikilink")》
  - 《[他影响了中国：陈云全传](../Page/他影响了中国：陈云全传.md "wikilink")》
  - 《[胡乔木传](../Page/胡乔木传.md "wikilink")》
  - 《毛泽东的衣食住行》
  - 《[马思聪传](../Page/马思聪传.md "wikilink")》
  - 《[反右派始末](../Page/反右派始末.md "wikilink")》
  - 《“四人帮”全传》
      - 《江青传》
      - 《张春桥传》
      - 《王洪文传》
      - 《姚文元传》
  - 《[“四人帮”兴亡](../Page/“四人帮”兴亡.md "wikilink")》

### 游记

  - 《[真实的朝鲜](../Page/真实的朝鲜.md "wikilink")》
  - 《[美国自由行](../Page/美国自由行.md "wikilink")》
  - 《俄罗斯自由行》
  - 《欧洲自由行》
  - 《今天的越南》
  - 《我的台湾之旅》
  - 《星条旗下的生活》
  - 《澳大利亚自由行》
  - 《叶永烈海外游记》
  - 《中国自由行：中西部卷》
  - 《中国自由行：东部卷》
  - 《中国自由行：台港澳卷》

## 参考文献

## 外部链接

  -
  -
[Y叶](../Category/中华人民共和国报告文学作家.md "wikilink")
[Y叶](../Category/中国科幻作家.md "wikilink")
[Y叶](../Category/傳記作家.md "wikilink")
[Y叶](../Category/上海作家.md "wikilink")
[Y叶](../Category/温州人.md "wikilink")
[Y永](../Category/叶姓.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")

1.
2.
3.
4.