**容国团**（），生于[英屬香港](../Page/英屬香港.md "wikilink")，原籍中國[廣東省](../Page/廣東省.md "wikilink")[中山縣南屏乡](../Page/中山縣.md "wikilink")（今属广东省[珠海市](../Page/珠海市.md "wikilink")[南屏镇](../Page/南屏镇_\(珠海市\).md "wikilink")），男子[乒乓球运动员](../Page/乒乓球.md "wikilink")，為[中華人民共和國建國以來首位運動項目世界冠軍](../Page/中華人民共和國.md "wikilink")，他所研究出來的快速抽擊，打破了當時主導歐洲和日本的花巧式打球方法。[文化大革命期间遭到中共及](../Page/文化大革命.md "wikilink")[红卫兵批判](../Page/红卫兵.md "wikilink")，自杀身亡。

## 生平

1937年8月10日生于出生于香港贫下阶层家庭，住在[筲箕灣山上的木屋區](../Page/筲箕灣.md "wikilink")（位置為今[耀東邨](../Page/耀東邨.md "wikilink")），與國際著名經濟學家[張五常為兒時好友](../Page/張五常.md "wikilink")，他凭借自创的“直拍四法门”，成为“街头球王”。

1952年（15歲）代表亲中共的[香港工會聯合會乒乓球隊參加比賽](../Page/香港工會聯合會.md "wikilink")。1954年，17岁的容国团在香港乒乓球埠标赛获得冠军，更在1956年战胜23届[世乒赛](../Page/世乒赛.md "wikilink")[日本新科状元](../Page/日本.md "wikilink")[荻村伊智朗](../Page/荻村伊智朗.md "wikilink")，一战成名。

1957年11月容国团前往内地（20歲），进[广州体育学院学习](../Page/广州体育学院.md "wikilink")。由于政治环境，当时内地的体育项目得不到西方支持，运动员的体育成绩不被认可。面对此种国际环境，容国团在广州体委一次大会上，立下“三年夺取世界冠军”的誓言，引起轰动。1958年被选入广东省乒乓球队，同年参加[全国乒乓球锦标赛](../Page/全国乒乓球锦标赛.md "wikilink")，获男子单打冠军。随后被选为[国家集训队队员](../Page/国家集训队.md "wikilink")。

1959年4月在[联邦德国](../Page/联邦德国.md "wikilink")[多特蒙德第二十五届](../Page/多特蒙德.md "wikilink")[世界乒乓球锦标赛上](../Page/世界乒乓球锦标赛.md "wikilink")，容国团的“小球路”以3：1战胜[匈牙利名将悉多](../Page/匈牙利.md "wikilink")，为中华人民共和国夺得了第一个乒乓球男子单打世界冠军，也是[中华人民共和国第一个世界冠军获得者](../Page/中华人民共和国.md "wikilink")。比赛结束后，中华人民共和国副总理[贺龙亲自到机场接机](../Page/贺龙.md "wikilink")、献花。他亦受到[毛泽东](../Page/毛泽东.md "wikilink")、[周恩来多次接见](../Page/周恩来.md "wikilink")。有外国政要来访，容国团更是被邀为座上宾。外形俊朗的容国团迅速成为中国国内年轻人的偶像，信件堆满了乒乓球队的传达室，不乏女青年的求爱信，球队也专门为此设立看信班\[1\]。

1961年在[北京舉行第二十六屆世界乒乓球錦標賽上](../Page/北京.md "wikilink")，他為中國隊第一次奪得男子團體冠軍做出了重要貢獻，並奠定了幾十年來中國在乒乓球壇地位。但由於容國團來自香港，遭受政治迫害，迅速被其他人取代，取而代之的是[莊則棟](../Page/莊則棟.md "wikilink")。

1964年后他担任中国乒乓球女队教练，此后，中国女队在第二十八届世界乒乓球锦标赛上，获得了女子团体冠军。

在[文化大革命期間](../Page/文化大革命.md "wikilink")，[賀龍遭受批鬥](../Page/賀龍.md "wikilink")，被指為賀龍派系的人，也同遭迫害。容國團因写申请参加比赛遭[紅衛兵批鬥为](../Page/紅衛兵.md "wikilink")[修正主义](../Page/修正主义.md "wikilink")，曾被「體委紅旗」及「清中紅衛兵」等等造反派揪鬥，侮辱和毒打。1968年6月20日清晨，容国团上吊自杀，身后被指因反革命畏罪自杀，但最终未被定性。容国团没有葬礼，火化费也是由其家人和他一部分的工资承担的。同样来自香港的[傅其芳和](../Page/傅其芳.md "wikilink")[姜永宁也以同样的方式自杀](../Page/姜永宁.md "wikilink")。1978年，国家体委为容国团恢复名誉，并补开了追悼会\[2\]\[3\]。

## 荣誉

<table>
<tbody>
<tr class="odd">
<td><p><a href="../Page/國際乒聯.md" title="wikilink">國際乒聯</a><br />
<a href="../Page/世界乒乓球錦標賽.md" title="wikilink">世界乒乓球錦標賽</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>年份</p></td>
<td><p>主辦城市</p></td>
</tr>
<tr class="odd">
<td><p>1959年</p></td>
<td><p><a href="../Page/西德.md" title="wikilink">西德</a><br />
<a href="../Page/多特蒙德.md" title="wikilink">-{zh-hans:多特蒙德;zh-hk:多蒙特;zh-tw:多特蒙德;}-</a></p></td>
</tr>
</tbody>
</table>

  - 1958年获“国家运动健将”称号。
  - 1959年、1961年两次获国家体委颁发的体育运动荣誉奖章。
  - 1984年被评为中华人民共和国成立三十五年来杰出运动员之一。
  - 2009年被評選為100位1949年以來感動中國人物。

## 資料

[Category:中国退役乒乓球运动员](../Category/中国退役乒乓球运动员.md "wikilink")
[Category:中国国家女子乒乓球队主教练](../Category/中国国家女子乒乓球队主教练.md "wikilink")
[Category:文革自殺者](../Category/文革自殺者.md "wikilink")
[Category:自杀运动员](../Category/自杀运动员.md "wikilink")
[Category:中国共产党党员
(建国后入党)](../Category/中国共产党党员_\(建国后入党\).md "wikilink")
[Category:葬于北京市万安公墓](../Category/葬于北京市万安公墓.md "wikilink")
[Category:珠海人](../Category/珠海人.md "wikilink")
[Category:香港人](../Category/香港人.md "wikilink")
[G](../Category/容姓.md "wikilink")

1.  [我的奥林匹克—容国团](http://v.titan24.com/html/2009/04/22/14/16951.html)
2.
3.  《容国团：冠军陨落(下)》.上海纪实频道《往事》栏目. 2010年4月15日.
    [视频链接](http://www.youtube.com/watch?v=PpoFEq8RXmI)