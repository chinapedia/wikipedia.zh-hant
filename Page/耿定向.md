**耿定向**（），[字](../Page/表字.md "wikilink")**在伦**，[号](../Page/号.md "wikilink")**楚侗**，人称**天台先生**，[湖广](../Page/湖广.md "wikilink")[黄安](../Page/黄安.md "wikilink")（今[湖北](../Page/湖北.md "wikilink")[红安](../Page/红安.md "wikilink")）人，[明朝政治人物](../Page/明朝.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。

## 生平

[嘉靖三十五年](../Page/嘉靖.md "wikilink")（1556年）登丙辰科[进士](../Page/进士.md "wikilink")，官历[行人](../Page/行人.md "wikilink")、[御史](../Page/御史.md "wikilink")。吏部尚书[吴鹏攀附](../Page/吴鹏.md "wikilink")[严嵩](../Page/严嵩.md "wikilink")，耿定向上书奏揭发吴鹏的六条罪状。歷任[学政](../Page/学政.md "wikilink")、[大理寺](../Page/大理寺.md "wikilink")[右丞](../Page/右丞.md "wikilink")、[右副都御史至](../Page/右副都御史.md "wikilink")[户部尚书](../Page/户部尚书.md "wikilink")，[总督仓场](../Page/总督仓场.md "wikilink")。嘉靖四十一年（1552年）主张建置[黄安县](../Page/黄安县.md "wikilink")。调[南京](../Page/南京.md "wikilink")，督理[学政](../Page/学政.md "wikilink")。[隆庆初年](../Page/隆庆.md "wikilink")（1567年）为[大理寺](../Page/大理寺.md "wikilink")[右丞](../Page/右丞.md "wikilink")。[高拱擅权](../Page/高拱.md "wikilink")，被贬为[横州](../Page/横州.md "wikilink")[判官](../Page/判官.md "wikilink")。後迁[衡州府](../Page/衡州府.md "wikilink")[推官](../Page/推官.md "wikilink")。[萬曆年間任](../Page/萬曆.md "wikilink")[福建巡撫](../Page/福建巡撫.md "wikilink")。官至[户部尚书](../Page/户部尚书.md "wikilink")。他还是[明代著名的](../Page/明代.md "wikilink")[理学家](../Page/理学家.md "wikilink")。耿定向是活跃在[阳明后学时期讲学舞台上的重要人物](../Page/阳明.md "wikilink")。耿定向因[李贄鼓倡狂禅](../Page/李贄.md "wikilink")，学者靡然从风，遂污蔑李为「異端」思想，污蔑李贄使「後學承風步影，毒流萬世之下」。

晚年辞官回乡，与弟耿定理、耿定力一起居[天台山创设书院](../Page/天台山.md "wikilink")，讲学授徒。去世后，[朝廷追赠](../Page/朝廷.md "wikilink")[太子少保](../Page/太子少保.md "wikilink")，[谥](../Page/谥.md "wikilink")**恭簡**。

## 著作

著有《冰玉堂语录》、《硕辅宝鉴要览》、《耿子庸言》、《先进遗风》、《耿天台文集》20卷等。

## 注釋

## 參考文獻

  - 《明史》卷221《耿定向》
  - 《福建省志》，福建省地方誌編纂委員會編，1992年。

[Category:明朝行人司行人](../Category/明朝行人司行人.md "wikilink")
[Category:明朝大理寺丞](../Category/明朝大理寺丞.md "wikilink")
[Category:明朝衡州府推官](../Category/明朝衡州府推官.md "wikilink")
[Category:明朝福建巡抚](../Category/明朝福建巡抚.md "wikilink")
[Category:明朝戶部尚書](../Category/明朝戶部尚書.md "wikilink")
[Category:諡恭簡](../Category/諡恭簡.md "wikilink")
[Category:红安人](../Category/红安人.md "wikilink")
[D](../Category/耿姓.md "wikilink")