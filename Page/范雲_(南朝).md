**范雲**（），[字彥龍](../Page/表字.md "wikilink")，[南朝梁文學家](../Page/南朝梁.md "wikilink")，[南鄉](../Page/南乡郡.md "wikilink")[舞陰](../Page/舞阴县.md "wikilink")（今[河南省](../Page/河南省.md "wikilink")[淅川县](../Page/淅川县.md "wikilink")[李官桥镇一带](../Page/李官桥镇.md "wikilink")）人，歷仕齊、梁二朝，為[竟陵八友之一](../Page/竟陵八友.md "wikilink")。

## 生平

[東晉時期曾任安北](../Page/東晉.md "wikilink")[將軍](../Page/將軍.md "wikilink")[范汪之六世孫](../Page/范汪.md "wikilink")。其祖父[范璩之在](../Page/范璩之.md "wikilink")[南朝宋其間曾任](../Page/南朝宋.md "wikilink")[中書侍郎](../Page/中書侍郎.md "wikilink")。其父[范抗為](../Page/范抗.md "wikilink")[郢府](../Page/郢.md "wikilink")[參軍](../Page/參軍.md "wikilink")。

[蕭衍執政](../Page/蕭衍.md "wikilink")，與[沈約等輔助](../Page/沈約.md "wikilink")。[齊武帝永明十年](../Page/齊武帝.md "wikilink")（492年），和[蕭琛出使北魏](../Page/蕭琛.md "wikilink")，受到[魏孝文帝的稱賞](../Page/魏孝文帝.md "wikilink")。加入梁後，官至[尚書右僕射](../Page/尚書右僕射.md "wikilink")、霄城侯。

天监二年五月丁巳（503年6月15日）卒\[1\]，年五十二。追赠侍中、卫将军\[2\]，谥曰文。

## 著作

范云有文集三十卷，他的詩詞風格清麗，敘情婉轉。\[3\]

## 子孙

  - 子：范孝才，官至太子中舍人。
  - 子：范祖解，梁[驸马都尉](../Page/驸马都尉.md "wikilink")，隋仪同大将军、苞信县开国子，食邑四百户。
      - 孙：范世贵，唐魏郡顿丘县令。
          - 曾孙：范志玄\[4\]

## 参看

  - [顺阳范氏](../Page/顺阳范氏.md "wikilink")

## 參考文獻

[Category:南朝诗人](../Category/南朝诗人.md "wikilink")
[Category:南齐官员](../Category/南齐官员.md "wikilink")
[Category:南梁官员](../Category/南梁官员.md "wikilink")
[Category:淅川人](../Category/淅川人.md "wikilink")
[W](../Category/顺阳范氏.md "wikilink")
[W](../Category/范姓.md "wikilink")

1.  《梁書·武帝紀中》：五月丁巳，尚書右僕射范雲卒。
2.  《梁书》：「诏曰：“追远兴悼，常情所笃；况问望斯在，事深朝寄者乎！故散骑常侍、尚书右仆射、霄城侯云，器范贞正，思怀经远，爰初立志，素履有闻。脱巾来仕，清绩仍著。燮务登朝，具瞻惟允。绸缪翊赞，义简朕心，虽勤非负靮，而旧同论讲。方骋远涂，永毘庶政；奄致丧殒，伤悼于怀。宜加命秩，式备徽典。可追赠侍中、卫将军，仆射、侯如故。并给鼓吹一部。”」
3.  《南史‧列傳第四十七‧范雲傳》：「范雲字彥龍，南鄉舞陰人，晉平北將軍汪六世孫也。祖璩之，宋中書侍郎。雲六歲就其姑夫袁叔明讀[毛詩](../Page/毛詩.md "wikilink")，日誦九紙。陳郡殷琰名知人，候叔明見之，曰『公輔才也』。……子孝才嗣。」
4.  《范志玄墓志》：「皇考彥龍，梁右仆射、封霄城侯。王考祖解，梁驸马都尉，隋仪同大将军、苞信县开国子，食邑四百户。严考世贵，皇魏郡顿丘县令。」