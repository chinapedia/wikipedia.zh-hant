**贝尔福地区**（）是[法國](../Page/法國.md "wikilink")[弗朗什-孔泰](../Page/弗朗什-孔泰.md "wikilink")[大區所轄的](../Page/大區.md "wikilink")[省份](../Page/省_\(法国\).md "wikilink")，東鄰[上莱茵省](../Page/上莱茵省.md "wikilink")、北接[孚日省](../Page/孚日省.md "wikilink")、西临[上索恩省](../Page/上索恩省.md "wikilink")、南与[杜省](../Page/杜省.md "wikilink")、[瑞士相邻](../Page/瑞士.md "wikilink")。省編號90。

## 历史

考古学显示，早在[新石器时代](../Page/新石器时代.md "wikilink")，贝尔福地区就有人类活动。公元前58年该地区被[罗马帝国征服](../Page/罗马帝国.md "wikilink")。罗马帝国衰落后，[法兰克人](../Page/法兰克人.md "wikilink")、[汪达尔人](../Page/汪达尔人.md "wikilink")、[阿拉曼人](../Page/阿拉曼人.md "wikilink")、[勃艮第人相继进入贝尔福地区](../Page/勃艮第人.md "wikilink")。[勃艮第王国被](../Page/勃艮第王国.md "wikilink")[法兰克王国打败后](../Page/法兰克王国.md "wikilink")，该地成为[阿尔萨斯](../Page/阿尔萨斯.md "wikilink")[公爵的领地](../Page/公爵.md "wikilink")，后又转属[奥地利大公](../Page/奥地利.md "wikilink")。在[三十年战争期间](../Page/三十年战争.md "wikilink")，贝尔福被法国占领。1648年正式并入法国。

## 地理

贝尔福地区面积仅609平方千米，是[法国除了](../Page/法国.md "wikilink")[巴黎及其近郊的三个省之外最小的省份](../Page/巴黎.md "wikilink")。
贝尔福地区的地形可分为三部分：北部是[孚日山脉](../Page/孚日山脉.md "wikilink")，南部是[汝拉山](../Page/汝拉山.md "wikilink")，中部为河谷地带。

## 交通

[Carte_du_Territoire_de_Belfort.svg](https://zh.wikipedia.org/wiki/File:Carte_du_Territoire_de_Belfort.svg "fig:Carte_du_Territoire_de_Belfort.svg")

因为贝尔福地区南北两面都是山区，所以*贝尔福山口*一直是[罗讷河流域与](../Page/罗讷河.md "wikilink")[莱茵河流域之间的交通要道](../Page/莱茵河.md "wikilink")，链接两大河流的[罗讷河-莱茵河运河就从贝尔福地区穿过](../Page/罗讷河-莱茵河运河.md "wikilink")。

## 参考资料

<https://web.archive.org/web/20091014023712/http://www.quid.fr/departements.html?mode=detail&dep=90&style=fiche>
法语 Territoire de Belfort

[Category:法国省份](../Category/法国省份.md "wikilink")
[T](../Category/勃艮第-弗朗什-孔泰大區.md "wikilink")