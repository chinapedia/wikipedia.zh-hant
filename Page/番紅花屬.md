**番紅花屬**（又称**藏紅花**）是[鳶尾科的一个属](../Page/鳶尾科.md "wikilink")，是一类[多年生](../Page/多年生植物.md "wikilink")[球根](../Page/球根.md "wikilink")[植物](../Page/植物.md "wikilink")。原產於[欧洲](../Page/欧洲.md "wikilink")[爱琴海岸及](../Page/爱琴海.md "wikilink")[小亞細亞至](../Page/小亞細亞.md "wikilink")[中亞](../Page/中亞.md "wikilink")。[花叶细长而状似](../Page/叶.md "wikilink")[松树叶子](../Page/松树.md "wikilink")。全世界共有约80种番红花。

[藏紅花精的来源为番红花](../Page/藏紅花精.md "wikilink")（**）的干燥[柱头](../Page/柱头.md "wikilink")，可入药。

## 種

[Bee_crocus_macro_4.jpg](https://zh.wikipedia.org/wiki/File:Bee_crocus_macro_4.jpg "fig:Bee_crocus_macro_4.jpg")于番紅花朵上\]\]
[Unidentified_Crocus_01.jpg](https://zh.wikipedia.org/wiki/File:Unidentified_Crocus_01.jpg "fig:Unidentified_Crocus_01.jpg")
*Crocus longiflorus*\]\]

  - （**）
  - （**）
  - （**）
  - （**）
  - （**）
  - （**）
  - （**）
  - [黃金堆番紅花](../Page/黃金堆番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - [羅馬尼亞番紅花](../Page/羅馬尼亞番紅花.md "wikilink")（**）
  - [藍綠番紅花](../Page/藍綠番紅花.md "wikilink")（**）
  - [雙花番紅花](../Page/雙花番紅花.md "wikilink")（**）
  - [希臘番紅花](../Page/希臘番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - （**）
  - （**）
  - [卡萊番紅花](../Page/卡萊番紅花.md "wikilink")（**）
  - （**）
  - [黃金番紅花](../Page/黃金番紅花.md "wikilink")（**）
  - [科西加番紅花](../Page/科西加番紅花.md "wikilink")（**）
  - [南斯拉夫番紅花](../Page/南斯拉夫番紅花.md "wikilink")（**）
  - （**）
  - [達馬希亞番紅花](../Page/達馬希亞番紅花.md "wikilink")（**）
  - （**）
  - [義大利番紅花](../Page/義大利番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - [橙黃番紅花](../Page/橙黃番紅花.md "wikilink")（**）
  - （**）
  - [多芽番紅花](../Page/多芽番紅花.md "wikilink")（**）
  - （**）
  - [網皮番紅花](../Page/網皮番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - [傑格番紅花](../Page/傑格番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - （**）
  - [黎巴嫩番紅花](../Page/黎巴嫩番紅花.md "wikilink")（**）
  - [光皮番紅花](../Page/光皮番紅花.md "wikilink")（**）
  - （**）
  - [香番紅花](../Page/香番紅花.md "wikilink")（**）
  - [雪白番紅花](../Page/雪白番紅花.md "wikilink")（**）
  - （**）
  - [法國番紅花](../Page/法國番紅花.md "wikilink")（**）
  - （**）
  - [小番紅花](../Page/小番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - [走莖番紅花](../Page/走莖番紅花.md "wikilink")（**）
  - [乳黃番紅花](../Page/乳黃番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - （**）
  - （**）
  - （**）
  - [可愛番紅花](../Page/可愛番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - （**）
  - [番红花](../Page/番红花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - [西柏番紅花](../Page/西柏番紅花.md "wikilink")（**）
  - （**）
  - [美麗番紅花](../Page/美麗番紅花.md "wikilink")（**）
  - （**）
  - [托氏番紅花](../Page/托氏番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）
  - [荷蘭番紅花](../Page/荷蘭番紅花.md "wikilink")（**）
  - （**）
  - （**）
  - （**）

[Category:花卉](../Category/花卉.md "wikilink")
[Category:番红花属](../Category/番红花属.md "wikilink")