**Xpress
200**是[ATI的一個晶片組系列](../Page/ATI.md "wikilink")，可以支援[AMD的](../Page/AMD.md "wikilink")64-bit，亦同時支援[INTEL的](../Page/INTEL.md "wikilink")[Pentium
4](../Page/Pentium_4.md "wikilink")，[Pentium
D和](../Page/Pentium_D.md "wikilink")[Celeron處理器](../Page/Celeron.md "wikilink")。這個系列有獨立型和整合型的晶片組。對於INTEL的版本，它的記憶體控制器支援[DDR和](../Page/DDR.md "wikilink")[DDR-II記憶體](../Page/DDR-II.md "wikilink")。相對於上一代產品，新增了[PCI-Express接口](../Page/PCI-Express.md "wikilink")，所以晶片組的名稱亦改變為Xpress系列。

之後，ATI將Xpress 200 Crossfire Edition更名為CrossFire Xpress
1600。在AMD收購ATI後，又再次更名為AMD [480X
CrossFire晶片組](../Page/480X_CrossFire.md "wikilink")。

## XPRESS 200晶元組列表

**AMD Edition**
以下的晶元組的結構是相近的，而且都支援[AMD的](../Page/AMD.md "wikilink")[Athlon
64和](../Page/Athlon_64.md "wikilink")[Sempron全系列处理器](../Page/Sempron.md "wikilink")。

  - **RX480** - **Xpress 200P** - 不带显示核心
  - **RS480** - **Xpress 200** - 集成显示核心（**X300**），支援**SidePort**技術
  - **RS482** - 是RS480的增強版本，但取消對SidePort的支援。
  - **Xpress 200 Crossfire Edition** - **Xpress 1600** - **480X
    CrossFire** - 支援兩條PCI-Express。

**Intel Edition**

  - **RS400**- **Xpress 200** - 支持雙通道DDR2/DDR400內存，集成**X300**顯示芯片。
  - **RC400**- **Xpress 200** - 僅支持單通道DDR2/DDR400內存，集成**X300**顯示芯片。
  - **RD400**- **Xpress 200** - 支持雙通道DDR2/DDR400內存，不集成顯示芯片。
  - **RC410**- **Xpress 200** - RC400的0.11um工藝版本，支持PentiumD處理器。
  - **RC415**- **Xpress 200** - RC400的0.11um工藝版本，支持Core2Duo處理器。
  - **RXC410**- **Xpress 200** - RC410去掉圖形核心的版本，支持PentiumD處理器。

## Xpress 200 (RS480)

Xpress 200的整合式顯示核心可以支援**DirectX 9.0b**，整合了[Radeon
X300顯示核心](../Page/Radeon_X_Series.md "wikilink")，有兩條像素流水線。顯示記憶體方面，利用**[HyperMemory](../Page/HyperMemory.md "wikilink")**技術，可以分享系統記憶體作為顯示記憶體，再利用**SidePort**技術，增加一顆專用32位元頻寬的DDR內存，供集成显示核心作為显示記億体。

接口方面，可以支援新的[PCI-Express](../Page/PCI-Express.md "wikilink")。它上一代的產品是[9100
IGP](../Page/9100_IGP.md "wikilink")。縱使9100 IGP的圖形效能不錯，但是只支援DirectX
8，在電腦科技急速發展中，已顯得不足。所以ATI就推出了支援DirectX
9的整合式晶片組。與此同時，對手[NVIDIA依然取不到INTEL的授權](../Page/NVIDIA.md "wikilink")，去開發用於INTEL平台的整合式晶片組，而當年AMD的市場是亦是玩家居多，所以只有ATI去開發高級的整合式晶片組。這個情況，一路到NVIDIA推出[NVidia
C51芯片组才有改變](../Page/NVidia_C51芯片组.md "wikilink")。

[南橋方面](../Page/南橋.md "wikilink")，ATI推出了新的IXP400(SB400)與之配合。它支援[SATA和](../Page/SATA.md "wikilink")[RAID功能](../Page/RAID.md "wikilink")。另外，主板廠商可搭配[ULi的](../Page/ULi.md "wikilink")**M1573**南橋，提供更靈活的組建方案。一路而來，ATI的南橋总是不甚完美，例如著名的[USB问題](../Page/USB.md "wikilink")，所以主機版製造商有時会使用[ULi南橋替代](../Page/ULi.md "wikilink")。直到NVIDIA收購ULi，NVIDIA停止向廠商供應南橋，迫使ATI自行研發更好的南橋晶片。

## 南橋

### IXP400南僑的規格：

南橋方面，ATi推出了**IXP400**(**SB400**)來與**Radeon Xpress 200**晶片組配搭

  - 支援2个PATA通道
  - 支援4个SATA150接口
  - 支援RAID 0和RAID 1
  - 支援8個USB2.0
  - 具備5.1或7.1聲道的[AC97音效輸出能力](../Page/AC97.md "wikilink")

### SB450南僑的規格：

南橋方面，ATi推出了**SB450**南橋來與RS482配搭：

  - 支援2个PATA通道
  - 支援4个SATA150接口
  - 支援RAID 0和RAID 1
  - 支援8个USB2.0接口
  - 支援HD-Audio

註：SB450没有集成网絡卡

另外，主板廠商可搭配[ULi的](../Page/ULi.md "wikilink")**M1575**和M1573南橋

[Category:主板](../Category/主板.md "wikilink")
[Category:冶天科技](../Category/冶天科技.md "wikilink")
[Category:AMD晶片組](../Category/AMD晶片組.md "wikilink")