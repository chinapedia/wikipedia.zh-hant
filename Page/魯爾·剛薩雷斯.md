**魯爾·冈萨雷斯**（1973年12月27日－）為[波多黎各的](../Page/波多黎各.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經來台效力於[中華職棒](../Page/中華職棒.md "wikilink")[La
New熊隊](../Page/La_New熊.md "wikilink")，在[中華職棒登錄為](../Page/中華職棒.md "wikilink")**剛力士R.G**。

## 經歷

  - [美國職棒](../Page/美國職棒.md "wikilink")[芝加哥小熊隊](../Page/芝加哥小熊.md "wikilink")（2000年）
  - [美國職棒](../Page/美國職棒.md "wikilink")[辛辛那提紅人隊](../Page/辛辛那提紅人.md "wikilink")（2001年－2002年）
  - [美國職棒](../Page/美國職棒.md "wikilink")[紐約大都會隊](../Page/紐約大都會.md "wikilink")（2002年－2003年）
  - [美國職棒](../Page/美國職棒.md "wikilink")[辛辛那提紅人隊](../Page/辛辛那提紅人.md "wikilink")（2004年）
  - [美國職棒](../Page/美國職棒.md "wikilink")[紐約大都會隊](../Page/紐約大都會.md "wikilink")（2004年）
  - [美國職棒](../Page/美國職棒.md "wikilink")[克里夫蘭印地安人隊](../Page/克里夫蘭印地安人.md "wikilink")（2004年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[La
    New熊隊](../Page/La_New熊.md "wikilink")（2007年）

## 職棒生涯成績

| 年度    | 球隊                                       | 出賽 | 打數  | 安打 | 全壘打 | 打點 | 盜壘 | 四死球 | 三振 | 壘打數 | 雙殺打 | 打擊率   |
| ----- | ---------------------------------------- | -- | --- | -- | --- | -- | -- | --- | -- | --- | --- | ----- |
| 2007年 | [La New熊](../Page/La_New熊.md "wikilink") | 29 | 113 | 38 | 2   | 18 | 0  | 14  | 17 | 53  | 4   | 0.336 |
| 合計    | 一年                                       | 29 | 113 | 38 | 2   | 18 | 0  | 14  | 17 | 53  | 4   | 0.336 |

## 特殊事蹟

## 外部連結

[R](../Category/1973年出生.md "wikilink")
[R](../Category/在世人物.md "wikilink")
[R](../Category/波多黎各棒球選手.md "wikilink")
[R](../Category/中華職棒外籍球員.md "wikilink")
[R](../Category/芝加哥小熊隊球員.md "wikilink")
[R](../Category/辛辛那提紅人隊球員.md "wikilink")
[R](../Category/紐約大都會隊球員.md "wikilink")
[R](../Category/克里夫蘭印地安人隊球員.md "wikilink")
[R](../Category/Lamigo桃猿隊球員.md "wikilink")