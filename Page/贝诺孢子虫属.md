**贝诺孢子虫属**（[學名](../Page/學名.md "wikilink")：）\[1\]，又名**必斯內蟲屬**，是[原生生物的一種](../Page/原生生物.md "wikilink")，屬於[頂複動物門的一個](../Page/頂複動物門.md "wikilink")[屬](../Page/屬.md "wikilink")，是一種[寄生性動物](../Page/寄生性動物.md "wikilink")，會造成許多[畜牲生病](../Page/畜牲.md "wikilink")。

## 分類

  - *[Besnoitia bennetti](../Page/Besnoitia_bennetti.md "wikilink")*
  - [贝氏贝诺孢子虫](../Page/贝氏贝诺孢子虫.md "wikilink") *Besnoitia besnoiti*
  - *[Besnoitia darlingi](../Page/Besnoitia_darlingi.md "wikilink")*
  - *[Besnoitia jellisoni](../Page/Besnoitia_jellisoni.md "wikilink")*
  - *[Besnoitia sauriana](../Page/Besnoitia_sauriana.md "wikilink")*
  - *[Besnoitia tarandi](../Page/Besnoitia_tarandi.md "wikilink")*
  - *[Besnoitia wallacei](../Page/Besnoitia_wallacei.md "wikilink")*

## 生命週期

本屬物種的生命週期大多數都是未知，特別是當中所涉及的中間宿主和載體較多樣化（例如有各種[虻科物種](../Page/虻科.md "wikilink")），使研究變得不容易。

## 病理學及對宿主的影響

感染了本屬物種可引起[有蒂病变](../Page/有蒂病变.md "wikilink")，病發地點可以在家畜的皮膚、鼻腔或喉部。病變有[外生性結節](../Page/外生性結節.md "wikilink")，從[黏膜的表面突出](../Page/黏膜.md "wikilink")。

## 參看

  - [贝诺孢子虫病](../Page/贝诺孢子虫病.md "wikilink")（）\[2\]

## 參考資料

[Category:真球蟲目](../Category/真球蟲目.md "wikilink")

1.
2.