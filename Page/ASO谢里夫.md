**谢里夫奥林匹克体育协会**（简称：**ASO Chlef**
[法语](../Page/法语.md "wikilink")：**Association Sportive
Olympique de Chlef**,[阿拉伯语](../Page/阿拉伯语.md "wikilink")：جمعية أولمبي
الشلف）是位于[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")[谢里夫的足球俱乐部](../Page/谢里夫.md "wikilink")，创建于1947年。俱乐部的队色为红色和白色。主体育场为克容纳30,000多名球迷的默哈莫德·博美兹拉格体育场（Boumezrag
Mohamed Stadium）。

## 球队荣誉

  - **[阿尔及利亚杯](../Page/阿尔及利亚杯.md "wikilink")**: 1次

<!-- end list -->

  -

      -
        2005年

[Category:阿尔及利亚足球俱乐部](../Category/阿尔及利亚足球俱乐部.md "wikilink")