**彼得·約翰·卡勒西莫**（，經常被稱為P. J.
Carlesimo，），生于[宾夕法尼亚州斯克兰顿](../Page/宾夕法尼亚州.md "wikilink")，大学和职业篮球教练，曾任[布魯克林籃網队總教練](../Page/布魯克林籃網队.md "wikilink")。

他是斯克兰顿大学和[福特汉姆大学篮球队前主教练和运动总监彼得](../Page/福特汉姆大学.md "wikilink")·A·卡勒西莫的儿子。

## 早年执教生涯

卡勒西莫1971年毕业于[福特汉姆大学](../Page/福特汉姆大学.md "wikilink")，成为了学校篮球队的助理教练。他也非常成功的执教过[纽约斯坦顿岛瓦格拉学院](../Page/纽约.md "wikilink")
(Wagner College)篮球队。

## 大学篮球教练

卡勒西莫在1982-1994期间执教过西顿霍尔大学 (Seton Hall
University)取得212胜166负的成绩，他也成为了学校的“世纪教练”带领西顿霍尔大学海盗篮球队打入1989年NCAA锦标赛总决赛，在加时赛中惜败于[密歇根大学](../Page/密歇根大学.md "wikilink")。

1990年[世界男子篮球锦标赛中他作为](../Page/世界男子篮球锦标赛.md "wikilink")[美国国家男子篮球队主教练](../Page/美国国家男子篮球队.md "wikilink")[迈克·沙舍夫斯基的助理教练](../Page/迈克·沙舍夫斯基.md "wikilink")，球队最终夺得赛事铜牌。\[1\]

## NBA执教生涯

卡勒西莫在1994－97年期间执教[波特兰开拓者队](../Page/波特兰开拓者队.md "wikilink")，1997－99年期间执教[金州勇士队](../Page/金州勇士队.md "wikilink")，两次都是接替[里克·阿德尔曼](../Page/里克·阿德尔曼.md "wikilink")。他期间还作为1992年奥运会[梦之队的助理教练](../Page/梦之队.md "wikilink")。2002年他成为了[格雷格·波波维奇](../Page/格雷格·波波维奇.md "wikilink")[圣安东尼奥马刺队的助理教练](../Page/圣安东尼奥马刺队.md "wikilink")。2007年7月5日他同意执教[西雅图超音速](../Page/西雅图超音速.md "wikilink")，双方的合约细节未透露。\[2\].

### 拉崔爾·史普利威爾鎖喉事件

在1997年12月1日，发生了NBA联盟历史上著名的傷人事件，受害者就是卡勒西莫。当时是[金州勇士队主教练的卡勒西莫](../Page/金州勇士队.md "wikilink")，對著當時狀況不佳的[史普利威爾喋喋不休](../Page/拉崔爾·史普利威爾.md "wikilink")，隨後兩人发生冲突。斯普雷维尔上前勒住他使其窒息长达15秒，如果队友不即时阻止住斯普雷维尔可能会导致严重的伤害，甚至死亡。这一事件发生后，斯普雷维尔赛季中剩餘的68場比賽全數遭聯盟停赛，勇士队在下一個賽季也立即將斯普雷维尔交易至[紐約尼克隊](../Page/紐約尼克.md "wikilink")。\[3\]\[4\]

## 接任布魯克林籃網教練

2012年，布魯克林籃網的開季戰績不如預期，籃網的管理階層決定開除前任總教頭[艾弗里·约翰逊](../Page/艾弗里·约翰逊.md "wikilink")。卡勒西莫擔任代理總教練。他上任後，執教風格跟強森不同，減少了球員個人單打，並且發揮球員的特質，如讓布魯克‧洛佩茲在籃下發揮，而不是在外圍接球投籃；讓德隆‧威廉斯有更多的擋拆機會。

2013年，季後賽3-4輸給[芝加哥公牛後](../Page/芝加哥公牛.md "wikilink")，[布魯克林籃網宣布不與他續約](../Page/布魯克林籃網.md "wikilink")，由[贾森·基德接任總教練](../Page/贾森·基德.md "wikilink")。

## 主教练战绩

| 隊伍                                    | 年份                                             | 常規賽 | 季後賽 |
| ------------------------------------- | ---------------------------------------------- | --- | --- |
| 比賽                                    | 勝                                              | 負   | 勝率  |
| [POR](../Page/波特蘭拓荒者.md "wikilink")   | [1994-95](../Page/1994-95_NBA賽季.md "wikilink") | 82  | 44  |
| [POR](../Page/波特蘭拓荒者.md "wikilink")   | [1995-96](../Page/1995-96_NBA賽季.md "wikilink") | 82  | 44  |
| [POR](../Page/波特蘭拓荒者.md "wikilink")   | [1996-97](../Page/1996-97_NBA賽季.md "wikilink") | 82  | 49  |
| [GSW](../Page/金州勇士.md "wikilink")     | [1997-98](../Page/1997-98_NBA賽季.md "wikilink") | 82  | 19  |
| [GSW](../Page/金州勇士.md "wikilink")     | [1998-99](../Page/1998-99_NBA賽季.md "wikilink") | 50  | 21  |
| [GSW](../Page/金州勇士.md "wikilink")     | [1999-00](../Page/1999-00_NBA賽季.md "wikilink") | 27  | 6   |
| [SEA](../Page/西雅圖超音速.md "wikilink")   | [2007-08](../Page/2007-08_NBA賽季.md "wikilink") | 82  | 20  |
| [OKC](../Page/俄克拉何马城雷霆.md "wikilink") | [2008-09](../Page/2008-09_NBA賽季.md "wikilink") | 13  | 1   |
| [BKN](../Page/布魯克林籃網.md "wikilink")   | [2012-13](../Page/2012-13_NBA賽季.md "wikilink") | 54  | 35  |
| 總計                                    | 554                                            | 239 | 315 |

## 参考资料

[Carlesimo, PJ](../Category/1949年出生.md "wikilink") [Carlesimo,
PJ](../Category/在世人物.md "wikilink") [Carlesimo,
PJ](../Category/美国篮球教练.md "wikilink") [Carlesimo,
PJ](../Category/金州勇士队教练.md "wikilink") [Carlesimo,
PJ](../Category/波特兰开拓者队教练.md "wikilink") [Carlesimo,
PJ](../Category/西雅图超音速队教练.md "wikilink") [Carlesimo,
PJ](../Category/布魯克林籃網教練.md "wikilink")
[C](../Category/福坦莫大學校友.md "wikilink")

1.  [1990 USA
    Basketball](http://www.usabasketball.com/history/mwc_1990.html)
2.  [Report: Spurs assistant Carlesimo to be named Sonics
    coach](http://sports.espn.go.com/nba/news/story?id=2925357)
3.
4.