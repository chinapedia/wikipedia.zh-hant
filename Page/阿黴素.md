**阿黴素**（**Doxorubicin**，又稱**hydroxyldaunorubicin**、**多柔比星**，商品名稱是**Adriamycin**），是一種作用於[DNA的](../Page/DNA.md "wikilink")[藥物](../Page/藥物.md "wikilink")，廣泛使用於化學治療。屬於[蒽環類](../Page/蒽环类药物.md "wikilink")（Anthracycline）[抗生素](../Page/抗生素.md "wikilink")，結構與[道諾黴素](../Page/道諾黴素.md "wikilink")
(Daunorubicin)相似，且與之同樣能夠對DNA發生[嵌入作用](../Page/嵌入_\(化學\).md "wikilink")。可用來治療多種[癌症](../Page/癌症.md "wikilink")。

此藥物的給藥方式為[注射](../Page/注射.md "wikilink")，市場上販賣所使用的商品名稱包括**Adriamycin
PFS**、**Adriamycin RDF**或**Rubex**\[1\]。

另有一種[脂質體包覆的藥劑](../Page/脂質體.md "wikilink")，稱為**Doxil**，生產者為[强生公司](../Page/强生公司.md "wikilink")，可減少[心臟毒性](../Page/心臟毒性.md "wikilink")（Cardiotoxicity）。

## 歷史

阿黴素的歷史可以追溯至1950年代，一間義大利的研究公司－[Farmitalia實驗室](../Page/Farmitalia實驗室.md "wikilink")，致力於從土壤中的微生物尋找[抗癌物質](../Page/抗癌物質.md "wikilink")。他們在[蒙特堡附近一座](../Page/蒙特堡.md "wikilink")13世紀的城堡周圍分離出了一種土壤樣本，進一步的發現一種能產生紅色染劑的新的黴菌菌株－*[Streptomyces
peucetius](../Page/:en:Streptomyces_peucetius.md "wikilink")*，並發現這種菌株能製造一種有效對抗小鼠腫瘤的抗生素，同一時間，另一組在法國的研究人員也發現了相同的化合物。這兩的團隊為這種新發現的抗生素命名為「[道諾黴素](../Page/道諾黴素.md "wikilink")（daunorubicin），結合分離出菌株的那塊土地的古老部落名－[道尼人](../Page/道尼人.md "wikilink")（Dauni），以及用來形容其顏色的法語單詞－rubis
(紅寶石)\[2\]。這種抗生素的臨床實驗開始于1960年代，並成功的治療[急性白血病和](../Page/急性白血病.md "wikilink")[淋巴瘤](../Page/淋巴瘤.md "wikilink")。然而，在1967年，道諾黴素被確認會產生致命的心臟毒性。\[3\]

Farmitalia實驗室的研究人員很快的發現，透過細微的改變化合物結構，就能使其生物活性產生變化，他們利用N-甲基-N-亞硝基氨基甲酸乙酯去突變[鏈黴菌](../Page/鏈黴菌.md "wikilink")
([Streptomyces](../Page/:en:Streptomyces.md "wikilink"))，而這種新的菌株會製造出一種新的紅色抗生素，並命名為－[Adriamycin](../Page/:en:Adriamycin.md "wikilink")，後來為了符合命名規則而改名為－doxorubicin。\[4\]
阿黴素在對抗小鼠腫瘤上有著比道諾黴素還要好的活性，特別是在實質固態腫瘤上面，但是阿黴素依然擁有心臟毒性。\[5\]

阿黴素和道諾黴素可以被認為是原型的蒽環類抗生素，世界各地許多的研究者後續研究發現許多其他蒽環類抗生素或類似物，據統計，目前有超過2,000種已知的阿黴素類似物。\[6\]

## 臨床應用

阿黴素通常用於治療某些[白血病及](../Page/白血病.md "wikilink")[霍奇金淋巴瘤](../Page/霍奇金淋巴瘤.md "wikilink")，以及[膀胱癌](../Page/膀胱癌.md "wikilink")、[乳腺癌](../Page/乳腺癌.md "wikilink")、[胃癌](../Page/胃癌.md "wikilink")、[肺癌](../Page/肺癌.md "wikilink")、[卵巢癌](../Page/卵巢癌.md "wikilink")、[甲狀腺癌](../Page/甲狀腺癌.md "wikilink")、[軟組織肉瘤](../Page/軟組織肉瘤.md "wikilink")、[多發性骨髓瘤](../Page/多發性骨髓瘤.md "wikilink")……等等。\[7\]常見含阿黴素的療法是AC
(阿黴素、[環磷酰胺](../Page/環磷酰胺.md "wikilink"))、TAC
([剋癌易](../Page/剋癌易.md "wikilink")、CA)、ABVD
(阿黴素、[博萊黴素](../Page/博萊黴素.md "wikilink")、[長春新鹼](../Page/長春新鹼.md "wikilink")、[達卡巴嗪](../Page/達卡巴嗪.md "wikilink"))、BEACOPP、CHOP
(環磷酰胺、阿黴素、長春新鹼、[強的松](../Page/強的松.md "wikilink")) 和FAC (5-氟脲嘧啶、阿黴素、環磷酰胺)。
[Doxil主要用於治療以鉑類為基礎的化療後復發的卵巢癌](../Page/:en:Doxil.md "wikilink")，或與[艾滋病有關的](../Page/艾滋病.md "wikilink")[卡波西氏肉瘤的治療](../Page/卡波西氏肉瘤.md "wikilink")。\[8\]

### 實驗性治療

[雷帕霉素](../Page/雷帕霉素.md "wikilink")（Sirolimus）和阿黴素的聯合治療實驗顯現出治療小鼠AKT陽性淋巴瘤的可能性。\[9\]

最近的動物研究結合小鼠的[單株抗體與阿黴素](../Page/單株抗體.md "wikilink")，開發出一個能夠消除HIV-1感染的[免疫偶聯物](../Page/免疫偶聯物.md "wikilink")。目前的治療與[抗反轉錄病毒療法](../Page/抗反轉錄病毒藥物.md "wikilink")
(ART)仍然無法針對在寄主內的愛滋病毒，而免疫偶聯物則可提供一個彌補ART不足的療法，以消除抗原表現T細胞。\[10\]

### 脂質體劑型

**[Doxil](../Page/:en:Doxil.md "wikilink")**是阿黴素的[聚乙二醇](../Page/聚乙二醇.md "wikilink")（聚乙二醇塗層）[脂質體包裹形式](../Page/脂質體.md "wikilink")，它是開發來治療一種愛滋病相關的癌症－卡波西氏肉瘤，這種癌症會導致口腔、鼻腔、喉嚨的內襯、皮下或其他器官產生病變成長。聚二乙醇的塗層導致Doxil優先聚集在皮膚，然而，這也附加了[肢端紅腫症](../Page/肢端紅腫症.md "wikilink")
([palmar plantar
erythrodysesthesia](../Page/:en:palmar_plantar_erythrodysesthesia.md "wikilink"),
PPE)或稱為[手足症候群](../Page/手足症候群.md "wikilink") ([Hand-Foot
Syndrome](../Page/:en:Hand-Foot_Syndrome.md "wikilink"))的副作用。以下為Doxil的使用方法：少量的藥物可以從毛細血管滲漏進雙手手掌和腳掌，而導致紅腫、易痛以及皮膚剝離造成的不舒服，甚至疼痛。在臨床試驗中，若每4週給予50
mg/m2的劑量，會有50.6％的患者產生手足症候群。這種副作用的發生限制了Doxil與阿黴素的聯合療法，從而限制了Doxil的替代可能性。Doxil的替代是本來值得期待的，因為脂質體包裹阿黴素的心臟毒性比未包裹的阿黴素還來的低。
Doxil也被FDA批准用於治療[卵巢癌和](../Page/卵巢癌.md "wikilink")[多發性骨髓瘤](../Page/多發性骨髓瘤.md "wikilink")。\[11\]\[12\]

**[Myocet](../Page/:en:Myocet.md "wikilink")**\[13\]是非聚乙二醇脂質體阿黴素，
Myocet在歐洲和加拿大被批准與環磷酰胺聯合治療[轉移性乳腺癌](../Page/轉移性乳腺癌.md "wikilink")，但尚未被FDA批准在美國使用。Sopherion
Therapeutics對它的研發目前正到了一個關鍵的全球性第三階段，同時使用「[曲妥珠單抗](../Page/曲妥珠單抗.md "wikilink")」
([Trastuzumab](../Page/:en:Trastuzumab.md "wikilink"))和「[紫杉醇](../Page/紫杉醇.md "wikilink")」
([Paclitaxel](../Page/:en:Paclitaxel.md "wikilink"))治療HER2陽性的轉移性乳腺癌。不同於Doxil，Myocet脂質體並沒有聚乙二醇塗層，因此不會導致手足症候群發生。減少這種副作用使其可以利用在同一個療法中提供一個替代阿黴素的替代物，從而在不降低療效的前提下增加安全性。和Doxil一樣，阿黴素的脂質體包裹限制了心臟毒性。在理論上，利用脂質體包裹阿黴素連限制其心臟心臟毒性，就可以安全地與其他心臟毒性的化療藥物同時性治療，例如曲妥珠單抗。但有一個FDA發布的[黑框警語](../Page/黑框警語.md "wikilink")
([black box
warning](../Page/:en:black_box_warning.md "wikilink"))表示曲妥珠單抗不能與阿黴素使用於同時性治療，只能使用於連續性治療。雖然曲妥珠單抗與阿黴素的同時性治療在臨床研究中對抗腫瘤的反應優越，但卻會產生無法承受的心臟毒性，包括心臟衰竭導致[充血性心臟衰竭](../Page/充血性心臟衰竭.md "wikilink")（CHF）的風險。據公佈的第二階段研究結果顯示－Myocet、曲妥珠單抗、紫杉醇可以安全地使用於同時性治療，而不會有產生心臟副作用的風險，能利用[左心室射出分率](../Page/左心室射出分率.md "wikilink")
(LVEF)功能的下降來偵測，同時又能在腫瘤對抗上有著卓越表現，此發現是使FDA批准第三階段試驗的基礎。

## 負面影響

阿黴素的急性副作用包括噁心、嘔吐和心律不整。它也可引起[嗜中性白血球缺乏症](../Page/嗜中性白血球缺乏症.md "wikilink")
([neutropenia](../Page/:en:neutropenia.md "wikilink"))，以及完全的脫髮。另一個比較溫和的副作用是尿液變色－給藥後48小時會將尿液變成鮮豔的紅色。當阿黴素的累積劑量達到500-550mg/m²，副作用－包括充血性心衰竭
(CHF)、[擴張型心肌病甚至死亡的發生風險都顯著提高](../Page/擴張型心肌病.md "wikilink")。阿黴素心臟毒性的特點在於粒線體氧化磷酸化導致的劑量依存性下降，阿黴素和鐵的相互作用產生的活性氧，可以破壞心肌，造成肌原纖維的損失和細胞質的液泡化。此外，部分患者可能發展為「肢端紅腫症」
(PPE)，特點為手掌或腳掌上的皮膚出疹、腫脹、疼痛和紅斑。\[14\]
由於這些副作用和紅色的症狀，阿黴素擁有「紅色魔鬼」\[15\]或「紅死病」\[16\]的稱號。

化療會引發[B型肝炎](../Page/B型肝炎.md "wikilink")，而含阿黴素的療法也不例外。\[17\]\[18\]

Doxorubin和一些化療藥物會引起色素沉著異常，包括環磷酰胺、抗瘧疾藥物、胺碘酮，重金屬（鐵除外）、四環素類、抗精神病藥物。\[19\]

## 生物合成

阿黴素（DXR）是[道諾黴素](../Page/道諾黴素.md "wikilink")
([daunorubicin](../Page/:en:daunorubicin.md "wikilink"))的14-羥基化，而道諾黴素是阿黴素在生物合成途徑中的直接先驅物，在自然界中含量相當豐富，因為它能由一些不同的野生型鏈黴菌生產。相比之下，只有一個已知的非野生型黴菌株－*[Streptomyces
peucetius](../Page/:en:Streptomyces_peucetius.md "wikilink")*
ATCC27952，被發現能夠產生廣泛使用的阿黴素。\[20\]該菌株是由Arcamone等人在1969年製造的，藉由突變1株本來只會產生道諾黴素而不會製造阿黴素的菌株。\[21\]隨後，Hutchinson的研究小組發現，特殊環境條件下，或通過基因修改的引入，能使鏈黴菌產生阿黴素\[22\]，他的研究小組還複製了很多生產阿黴素所需的基因，雖然不是每一段都擁有充分的特點。1996年，Strohl的研究小組發現並分離出[dox
A](../Page/:en:dox_A.md "wikilink")，此基因能轉譯出一種能將道諾黴素轉化成阿黴素的酵素\[23\]。到了1999，他們製造出重組過的
dox A，能轉譯出細胞色素P450的氧化酶 (cytochrome P450
oxidase)，並發現，它催化了許多阿黴素在生物合成中的步驟，包括形成道諾黴素的步驟\[24\]。這意義相當重大，因為它確認了所有的道諾黴素產生菌要產生阿黴素所需要的必須基因。Hutchinson的研究小組開發提高阿黴素產量的方法，從發酵過程中使其商業化生產，而不僅僅是引用dox
A的[質體](../Page/質體.md "wikilink")
([plasmids](../Page/:en:plasmids.md "wikilink"))，還通過利用突變來削去酵素的活性，使阿黴素的前驅物轉向產生用處較少的產物，例如baumycin式的[糖苷](../Page/糖苷.md "wikilink")\[25\]。有些三重突變體，會過表達dox
A使阿黴素的產量為原來的兩倍。這是超過學術價值的，因為現在阿黴素的成本每公斤約137萬美元，於1999年的生產量為一年225公斤\[26\]。更高效率的生產技術也使無脂質體包裹的阿黴素價格下跌到每公斤110萬美元。雖然阿黴素可以利用道諾黴素合成，過程中涉及親電子的[溴化反應和多個步驟](../Page/滷化.md "wikilink")，而且產量差\[27\]，自從道諾黴素可以利用發酵法生產，如果細菌可以更有效地完全合成阿黴素，那這個方法將是具有潛力的。

## 作用機制

[Doxorubicin–DNA_complex_1D12.png](https://zh.wikipedia.org/wiki/File:Doxorubicin–DNA_complex_1D12.png "fig:Doxorubicin–DNA_complex_1D12.png")
阿黴素與DNA利用插入和抑制大分子的生物合成來相互作用\[28\]\[29\]。這個作用抑制了解開DNA超螺旋的[拓撲異構酶II](../Page/拓撲異構酶.md "wikilink")。在拓撲異構酶II為了複製而解開DNA鏈後，阿黴素會穩定拓撲異構酶II，防止DNA雙股螺旋再結合在一起，從而停止複製過程。

平面芳香族分子的發色團部分插入在兩個DNA鹼基對之間，而六碳氨糖坐落在次要凹槽和側邊與插入位緊鄰的鹼基對互相作用，作為多個晶體結構的證明。\[30\]\[31\]

## 抗瘧疾作用

證據顯示：阿黴素和類似的化合物擁有抗瘧疾活性。在2009年，類似阿黴素結構的化合物被發現可以抑制 [plasmepsin
II](../Page/:en:plasmepsin_II.md "wikilink")－一種瘧疾寄生蟲 *[Plasmodium
falciparum](../Page/:en:Plasmodium_falciparum.md "wikilink")*特有的酵素\[32\]。製藥公司GlaxoSmithKline
(GSK)後來查明，阿黴素類似的化合物能抑制寄生蟲的生長\[33\]。

## 參考文獻

## 外部連結

  - [Overview](http://www.bccancer.bc.ca/HPI/DrugDatabase/DrugIndexPro/Doxorubicin.htm)
    at [BC Cancer Agency](../Page/BC_Cancer_Agency.md "wikilink")
  - [Doxil
    Site](https://web.archive.org/web/20070629043538/http://www.orthobiotech.com/doxil.html)

[Category:蒽环类](../Category/蒽环类.md "wikilink")
[Category:多酚苷](../Category/多酚苷.md "wikilink")
[Category:IARC第2A类致癌物质](../Category/IARC第2A类致癌物质.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")

1.  "[Doxorubicin
    (Systemic)](http://www.mayoclinic.com/health/drug-information/DR202209)."
    *[Mayo Clinic](../Page/Mayo_Clinic.md "wikilink").* Last updated on:
    1999-06-15. Retrieved on 2007-04-19.

2.

3.

4.
5.

6.
7.
8.  "[DOXIL Product
    Information](http://www.orthobiotech.com/common/prescribing_information/DOXIL/PDF/DOXIL_PI_Booklet.pdf)
    ." *[Ortho Biotech Products, L.P.](http://www.orthobiotech.com/)*
    Retrieved on April 19, 2007.

9.

10.

11.

12.

13.
14.
15.

16.

17.

18.

19. <http://www.nejm.org/image-challenge?ci=09012011&query=TOC>

20.

21.

22.

23.

24.

25.
26.

27.

28.

29.

30.  Crystal structure is available for download as a
    [PDB](http://www.rcsb.org/pdb/explore.do?structureId=1D12) file.

31.

32.

33.