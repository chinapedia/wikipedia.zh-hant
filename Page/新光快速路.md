**新光快速路**是[中国](../Page/中国.md "wikilink")[广州市一条](../Page/广州市.md "wikilink")[南](../Page/南.md "wikilink")[北向的](../Page/北.md "wikilink")[快速路](../Page/快速路.md "wikilink")，连接[广州](../Page/广州.md "wikilink")[东部城区至](../Page/东部.md "wikilink")[番禺区](../Page/番禺区.md "wikilink")[光明北路](../Page/光明北路.md "wikilink")，以[猎德大桥](../Page/猎德大桥.md "wikilink")、[新光大桥跨越](../Page/新光大桥.md "wikilink")[珠江](../Page/珠江.md "wikilink")，全长19.9[公里](../Page/公里.md "wikilink")。主线按双向八车道设计，[大桥](../Page/大桥.md "wikilink")、[高架路](../Page/高架路.md "wikilink")、[隧道按双向六车道设计](../Page/隧道.md "wikilink")。

工程分三期建设，一期（[海珠区](../Page/海珠区.md "wikilink")[新滘中路](../Page/新滘中路.md "wikilink")-番禺光明北路）於2006年9月建成；由于[收费](../Page/收费.md "wikilink")[问题未能解决](../Page/问题.md "wikilink")，2007年1月20日试[免费通车](../Page/免费.md "wikilink")，2月10日正式实行[收费通车](../Page/收费.md "wikilink")\[1\]。二期（[天河区](../Page/天河区.md "wikilink")[珠江新城](../Page/珠江新城.md "wikilink")[花城大道](../Page/花城大道.md "wikilink")-[海珠区](../Page/海珠区.md "wikilink")[新滘中路](../Page/新滘中路.md "wikilink")），三期向北延伸连接[广州环城高速公路](../Page/广州环城高速公路.md "wikilink")。2009年7月30日[猎德大桥](../Page/猎德大桥.md "wikilink")[系统主线](../Page/系统.md "wikilink")[工程及](../Page/工程.md "wikilink")4个匝道开通。2010年11月1日全线[免費通行](../Page/免費.md "wikilink")。

[新光大桥以](../Page/新光大桥.md "wikilink")[北](../Page/北.md "wikilink")[设计](../Page/设计.md "wikilink")[时速为](../Page/时速.md "wikilink")60公里、以[南则为](../Page/南.md "wikilink")80[公里](../Page/公里.md "wikilink")。建成后可缓解[广州大道及](../Page/广州大道.md "wikilink")[广州市区至](../Page/广州市区.md "wikilink")[番禺之间的](../Page/番禺.md "wikilink")[交通](../Page/交通.md "wikilink")[压力](../Page/压力.md "wikilink")。

## 沿途交汇公路

## 注释

[Category:广州市高速公路](../Category/广州市高速公路.md "wikilink")

1.