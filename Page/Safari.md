**Safari**是[蘋果公司所開發](../Page/蘋果公司.md "wikilink")，並內建於[macOS](../Page/macOS.md "wikilink")（前稱OS
X、Mac OS
X）的[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")。Safari在2003年1月7日首度發行測試版\[1\]，並從[Mac
OS X Panther開始成為Mac](../Page/Mac_OS_X_Panther.md "wikilink") OS
X的預設瀏覽器，也是[iOS內建的預設瀏覽器](../Page/iOS.md "wikilink")。[Windows版本的首個測試版在](../Page/Windows.md "wikilink")2007年6月11日推出，支援[Windows
XP](../Page/Windows_XP.md "wikilink")、[Windows
Vista和](../Page/Windows_Vista.md "wikilink")[Windows
7](../Page/Windows_7.md "wikilink")\[2\]，並在2008年3月18日推出正式版，但蘋果已於2012年7月25日停止開發Windows版的Safari\[3\]。

Safari發行後的[市佔率不斷攀升](../Page/網頁瀏覽器的使用分佈.md "wikilink")。2008年2月，TheCounter.com報告指Safari的市佔率為3.34%\[4\]，而Net
Applications則指其市佔率為2.63%。其後市佔率再從2009年1月的3.62%爬升至2011年4月的7.1%。\[5\]在行動裝置平台，Net
Applications表示Safari占有率为62.17%。\[6\]

## 開發歷程

在1997年以前，[麥金塔電腦預裝的瀏覽器是](../Page/麥金塔電腦.md "wikilink")[Netscape
Navigator](../Page/Netscape_Navigator.md "wikilink")。之後蘋果和微軟達成協議，以在Mac上使用[Internet
Explorer for
Mac作預設瀏覽器換取微軟開發Mac版的](../Page/Internet_Explorer_for_Mac.md "wikilink")[Microsoft
Office](../Page/Microsoft_Office.md "wikilink")。直至2003年6月，蘋果才推出自家的Safari瀏覽器，同時微軟也終止開發蘋果版的IE瀏覽器。[Mac
OS X
v10.3仍保留IE](../Page/Mac_OS_X_v10.3.md "wikilink")，但至10.4版蘋果就僅預裝Safari瀏覽器。

2005年4月，Safari的開發人員之一[戴夫·海厄特](../Page/戴夫·海厄特.md "wikilink")，就他為Safari進行除錯的進展方面提交文件，使之能通過[Acid2測試](../Page/Acid2.md "wikilink")。同年4月27日，海厄特宣佈其內部試驗版本的Safari通過了Acid2測試，成為第一個通過該測試的瀏覽器\[7\]。

2005年4月29日，Safari
2.0版推出，內置[RSS及](../Page/RSS.md "wikilink")[Atom閱讀器](../Page/Atom.md "wikilink")，其他新功能包含隱密瀏覽、收藏及電郵網頁、搜尋網址書籤等，其速度是1.2.4版本的1.8倍\[8\]。至同年10月31日，Safari
2.0.2版隨Mac OS 10.4.3更新套件正式推出。

2005年6月，KHTML的開發人員曾批評蘋果不去整理產品改動的記錄，蘋果方面遂將[WebCore及](../Page/WebKit#WebCore.md "wikilink")[JavaScriptCore的開發及錯誤回報交予](../Page/WebKit#JavaScriptCore.md "wikilink")[OpenDarwin](../Page/Darwin_\(作業系統\).md "wikilink").org負責。WebKit本身也是以開放原始碼方式發行，但Safari瀏覽器自身的外觀，如[使用介面等](../Page/圖形使用者介面.md "wikilink")，則維持專有。

2007年6月11日，蘋果公司推出了同時支持Mac和PC的Safari 3 Public
Beta版，在推出的前三天，Beta版的下載量就突破了百萬大關。同時[iPhone的瀏覽器也是基於正式版的Safari](../Page/iPhone.md "wikilink")
3\[9\]。

Safari
4于2008年6月2日推出，提高了[JavaScript性能和速度](../Page/JavaScript.md "wikilink")\[10\]。

Safari 5于2010年6月7日推出，加入阅读器功能。5.1.7是最後一個支援Windows的版本。

Safari 6于2012年6月11日随[OS X Mountain
Lion推出](../Page/OS_X_Mountain_Lion.md "wikilink")，增加了分享列表等特性。

Safari
7在2013年6月10日的[苹果公司全球软体开发者年会上公佈](../Page/苹果公司全球软体开发者年会.md "wikilink")。新版本重新設計了閱讀列表和Top
Sites版面，又在側邊欄加入了「共享的链接」，顯示用戶在社交網絡上已關注的人所發佈的网址。Safari 7亦採用了Nitro Tiered
JIT和Fast Start技術，提高網站瀏覽速度。新版本的節能技術會將在背景運行的網站所使用的系統資源減低，提高電池續航力。Safari
7將會預載於[OS X Mavericks上](../Page/OS_X_Mavericks.md "wikilink")。

2014年10月16日，隨著OS X Yosemite的發布，蘋果推出了Safari 8。

2015年9月30日，隨著OS X El Capitan的發布，蘋果推出了Safari 9（同樣適用於Mavericks和Yosemite）。

2016年9月21日，隨著macOS Sierra的發佈，蘋果推出了Safari 10。

2017年9月25日，隨著macOS High Sierra的發佈，蘋果推出了Safari 11。

2018年9月24日，隨著macOS Mojave的發佈，蘋果推出了Safari 12。

## 服務

阅读列表可以自动按照阅读阅读情况来为收藏文章自动归类，并支持[iCloud云同步服务](../Page/iCloud.md "wikilink")，在所有支持阅读列表和iCloud的设备中都可以使用。Safari
5.1时\[11\]，阅读列表添加了离线阅读支持，可以下载收藏的网页并在没有网络的时候一样可以阅读。

阅读列表实际上是一组可以按照阅读顺序排列的特殊的书签，并且支持云同步\[12\]。

### 对比

因为网络所提供的资讯繁杂，所以就产生了离线阅读软件来净化干扰并优化阅读体验\[13\]。阅读列表可以下载网页并离线阅读，但阅读列表并不是真正意义上的离线阅读软件类似于[Pocket或者](../Page/Pocket.md "wikilink")[Instapaper等](../Page/Instapaper.md "wikilink")。因为阅读列表本身不能优化网页的表现结构，而其他的稍后读软件会对网页本身的阅读体验优化（不过也可以和内置于Safari阅读器相搭配来完成阅读优化工作）\[14\]，同时也没有归类功能，只能按照阅读和未阅读区分。

和书签相比，阅读列表具有分类功能：当收藏了一个新的链接时，阅读列表会自动把链接归入没有阅读部分；在阅读完后，阅读列表又会自动归入已阅读，可以帮助整理。如果书签需要实现同样的功能，就会在人工整理和归类里花费过多时间\[15\]\[16\]。

### 外观

在Safari 6之前的阅读列表的底纹是亚麻材质的底纹\[17\]，而Safari
6所有窗口，包括阅读列表的亚麻质感的底纹则修改成暗灰色的背景，\[18\]\[19\]这和[macOS和](../Page/macOS.md "wikilink")[iOS整体的极简设计趋势相关](../Page/iOS.md "wikilink")。

### 使用

在所有支持阅读列表的Safari中，都可以使用以下方法来在阅读列表添加链接\[20\]：

  - 在网页上按下Shift-Command-D；
  - 按下分享按钮，选择添加到阅读列表（iOS中的Safari也可以使用这种方法\[21\]）；
  - 右击链接，选择添加到阅读列表；
  - 在书签下拉菜单中选择添加到阅读列表；
  - 通过点击地址栏左侧的+按钮来添加阅读列表，或按住并在弹出菜单中选择添加到阅读列表。

## 參見

  - [iPhone](../Page/iPhone.md "wikilink")、[iPod
    touch与](../Page/iPod_touch.md "wikilink")[iPad](../Page/iPad.md "wikilink")
    - 蘋果搭載修改過的Safari的產品。
  - [WebKit](../Page/WebKit.md "wikilink") - Safari的核心。
  - [網頁瀏覽器列表](../Page/網頁瀏覽器列表.md "wikilink")
  - [網頁瀏覽器比較](../Page/網頁瀏覽器比較.md "wikilink")
  - [Safari版本列表](../Page/Safari版本列表.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [Safari - Apple](https://www.apple.com/safari/)
  - [Safari - Apple (中国)](https://www.apple.com/cn/safari/)
  - [Safari - Apple (香港)](https://www.apple.com/hk/safari/)
  - [Safari - Apple (台灣)](https://www.apple.com/tw/safari/)

[Category:2003年軟體](../Category/2003年軟體.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")
[Category:IOS软件](../Category/IOS软件.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:Webkit衍生軟體](../Category/Webkit衍生軟體.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.