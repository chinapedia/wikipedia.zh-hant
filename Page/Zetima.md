**Zetima** 是[日本藝能公司](../Page/日本.md "wikilink")[Up-Front
Works旗下擁有的一間](../Page/Up-Front_Works.md "wikilink")[唱片公司](../Page/唱片公司.md "wikilink")。這個品牌的唱片是由[史詩唱片日本負責發行](../Page/史詩唱片日本.md "wikilink")。在日本以外的地區，這間公司最為人熟悉的是發行[Hello\!
Project旗下成員的唱片](../Page/Hello!_Project.md "wikilink")，最受歡迎的包括[早安少女組](../Page/早安少女組.md "wikilink")。另外，此公司亦曾經發行唱片的歌手包括[平家充代](../Page/平家充代.md "wikilink")、[迷你早安](../Page/迷你早安.md "wikilink")（Mini
Moni）和[田村直美](../Page/田村直美.md "wikilink")。

## 歌手和組合

直至年：

<div style="-moz-column-width: 16em; -moz-column-gap: 2em;">

  - [麻生詩織](../Page/麻生詩織.md "wikilink")
  - [椰果少女組。](../Page/椰果少女組.md "wikilink")（）
  - [鄉村少女組。](../Page/鄉村少女組.md "wikilink")（）
  - [DEF.DIVA](../Page/DEF.DIVA.md "wikilink")
  - [Earthshaker](../Page/Earthshaker.md "wikilink")
  - [Flex Life](../Page/Flex_Life.md "wikilink")
  - [GaGaalinG](../Page/GaGaalinG.md "wikilink")
  - [H.P. 全員](../Page/H!P_夏季洗牌組合#H.P._全員.md "wikilink")（）
  - [HANGRY\&ANGRY](../Page/HANGRY&ANGRY.md "wikilink")
  - [Hello\! Project 洗牌組合](../Page/Hello!_Project_洗牌組合.md "wikilink")
  - [犬神馬戲團](../Page/犬神馬戲團.md "wikilink")（）
  - [加藤紀子](../Page/加藤紀子.md "wikilink")
  - [上沼恵美子](../Page/上沼恵美子.md "wikilink")
  - [KAN](../Page/KAN.md "wikilink")
  - [松浦亞彌](../Page/松浦亞彌.md "wikilink")
  - [哈密瓜紀念日](../Page/哈密瓜紀念日.md "wikilink")（）
  - [迷你早安](../Page/迷你早安.md "wikilink")（）
  - [森高千里](../Page/森高千里.md "wikilink")
  - [早安少女組。](../Page/早安少女組。.md "wikilink")（）
  - [Red Velvet](../Page/Red_Velvet.md "wikilink")
  - [草娥](../Page/草娥.md "wikilink")
  - [高橋愛](../Page/高橋愛.md "wikilink")
  - [新垣里沙](../Page/新垣里沙.md "wikilink")
  - [龜井繪里](../Page/龜井繪里.md "wikilink")
  - [道重沙由美](../Page/道重沙由美.md "wikilink")
  - [田中麗奈](../Page/田中麗奈_\(早安少女組\).md "wikilink")
  - [早安少女組。誕生10年紀念隊](../Page/早安少女組誕生10年紀念隊.md "wikilink")（）
  - [中島卓偉](../Page/中島卓偉.md "wikilink")
  - [月島星璃](../Page/偶像宣言#登場人物.md "wikilink") starring
    [久住小春](../Page/久住小春.md "wikilink")（早安少女組。）（）
  - [吉澤瞳](../Page/吉澤瞳.md "wikilink")
  - [紺野朝美](../Page/紺野朝美.md "wikilink")
  - [矢口真里](../Page/矢口真里.md "wikilink")
  - [中澤裕子](../Page/中澤裕子.md "wikilink")
  - [後浦夏美](../Page/後浦夏美.md "wikilink")（）
  - [Nyle](../Page/Nyle.md "wikilink")
  - [小早安](../Page/小早安.md "wikilink")（）
  - [射亂Q](../Page/射亂Q.md "wikilink")（）
      - [淳君](../Page/淳君.md "wikilink")（）
          - （）
  - （）
  - [杉田二郎](../Page/杉田二郎.md "wikilink")
  - [Romans](../Page/Romans.md "wikilink")
  - [℃-ute](../Page/℃-ute.md "wikilink")
  - [Buono\!](../Page/Buono!.md "wikilink")
  - [T\&Cボンバー](../Page/T&Cボンバー.md "wikilink")
  - [蒲公英(歌手組合)](../Page/蒲公英\(歌手組合\).md "wikilink")（）
  - [W](../Page/W_\(Double_U\).md "wikilink")（）
  - [湯原昌幸](../Page/湯原昌幸.md "wikilink")

</div>

## 外部連結

  - [Up-Front Works 官方網頁](http://www.up-front-works.jp)

[ja:アップフロントワークス](../Page/ja:アップフロントワークス.md "wikilink")

[Category:日本唱片公司](../Category/日本唱片公司.md "wikilink")