<includeonly>{{\#if:| {{\!}}- valign="top" {{\!}} **簡稱** {{\!}}
<span class="additional-name"></span> }} |- valign="top" {{\#if: |
{{\!}} colspan="2" style="text-align: center;" {{\!}} ![}}}]({{{Photo
"}}}")
<small></small>}} |- valign="top" {{\#if:| {{\!}} **國家** {{\!}}
<span class="nickname"></span> }} |-valign="top" |**行政區類別**
||<span class="category"></span> |- valign="top" |
||<span class="adr"><span class="locality"></span></span> |-valign="top"
{{\#if: | {{\!}} **其他大型縣市** {{\!}}  }} {{\#if:| {{\!}} **城市格言** {{\!}}
}} |-valign="top" | || |-valign="top" |**政府網站**||\[ \] |-valign="top"
{{\#if:| {{\!}} **設立始年** {{\!}}  }} |-valign="top"
{{\#if:||style="display:none;"}} |**區劃**|| |- valign="top"
{{\#if:||style="display:none;"}} |**總面積** ||

<div style="font-size: 95%; color: #666">

</div>

|-valign="top" {{\#if:||style="display:none;"}} |**總人口** ||

<div style="font-size: 95%; color: #666">

</div>

|-valign="top" {{\#if:||style="display:none;"}} |**都會區人口** ||
|-valign="top" {{\#if:||style="display:none;"}} |**人口密度** ||

<div style="font-size: 95%; color: #666">

</div>

|- style="vertical-align: top;" {{\#if:| {{\!}} **** {{\!}}  }} |-
style="vertical-align: top;" {{\#if:| {{\!}} **** {{\!}}  }} |-
style="vertical-align: top;" {{\#if:| {{\!}} **** {{\!}}  }}
|-style="vertical-align: text-top;" {{\#if:| {{\!}}**方言**{{\!\!}} }}
|-style="vertical-align: text-top;" {{\#if:| {{\!}}**毗鄰省市**{{\!\!}} }}
|- | colspan="2" style="text-align: center;"|![]({{{MapLocation}}})

<div style="clear: both; margin-top: 0.2em;">

</div>

</includeonly><noinclude>

## 使用方式

  - 以下為可供複製使用的原始碼。除特別標示的欄位之外均為「必填」。

<table>
<tbody>
<tr class="odd">
<td><p><small></p>
<pre><code>{{一覽表 政區 省分|
ShortName=|
Photo=|
Photoname=|
PolDivType=|
Country =|
CapitalTitle=|
Capital=|
LargestOther=|
Motto=|
HeadTitle=|
Head=|
Website=|
WebsiteTitle=|
SetYear =|
LowerDivisions =|
AreaTotal=|
AreaTotalRank=|
PopTotal=|
PopTotalRank=|
PopMetro=|
PopMetroRank=|
PopDensity=|
PopDensityRank=|
FlowerTitle=|
Flower=|
TreeTitle=|
Tree=|
AnimalTitle=|
Animal=|
Dialect=|
Neighbors=|
MapLocation=|
MapLocationSize=|
MapLocationTitle=|
}}</code></pre>
<p></small></p></td>
<td><p><small></p>
<pre><code>
簡稱（選填）
代表圖像
代表圖像名稱
所屬行政區種類
國家
首府頭銜
首府所在地
其他大型縣市（選填）
城市格言
省份首長職銜
省份首長名稱
省份網站網址
省份網站名稱
設立始年
區劃
總面積（平方公里）
總面積排名
總人口
總人口排名
都會區人口
都會區人口排名
人口密度（選填）
人口密度排名（選填）
代表花頭銜（市花）
代表花
代表樹頭銜（市樹）
代表樹
代表鳥頭銜（市鳥）
代表鳥
方言
毗鄰省市
地圖圖片名稱
地圖大小
地圖說明</code></pre>
<p></small></p></td>
</tr>
</tbody>
</table>

## 參見

  - [Template:一覽表 標頭](Template:一覽表_標頭.md "wikilink")
  - [Template:一覽表 旗徽](Template:一覽表_旗徽.md "wikilink")
  - [Template:一覽表 政區 省分](Template:一覽表_政區_省分.md "wikilink")
  - [Template:一覽表 政區 縣市](Template:一覽表_政區_縣市.md "wikilink")
  - [Template:一覽表 地理 省分](Template:一覽表_地理_省分.md "wikilink")
  - [Template:一覽表 地理 縣市](Template:一覽表_地理_縣市.md "wikilink")
  - [Template:一覽表 氣候](Template:一覽表_氣候.md "wikilink")

</noinclude>

[](../Category/国家模板.md "wikilink")
[亚](../Category/行政区划信息框模板.md "wikilink")