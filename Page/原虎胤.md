**原虎胤**（1497年－1564年3月11日）是[日本戰國時代的武將](../Page/日本戰國時代.md "wikilink")。父親是[原友胤](../Page/原友胤.md "wikilink")。最初是[千葉氏家臣](../Page/千葉氏.md "wikilink")。後來成為[甲斐國](../Page/甲斐國.md "wikilink")[武田氏的家臣](../Page/武田氏.md "wikilink")，擔任[足輕大將](../Page/足輕大將.md "wikilink")。別名**信知**。後世譽為[武田五名臣之一](../Page/武田五名臣.md "wikilink")。

## 生平

### 武田信虎時期

在[明應](../Page/明應.md "wikilink")6年（1497年）出生。原本是[下總國千葉氏的重臣兼一門](../Page/下總國.md "wikilink")[原氏](../Page/原氏.md "wikilink")。[永正](../Page/永正.md "wikilink")14年（1517年），居城[小弓城被](../Page/小弓城.md "wikilink")[足利義明所奪](../Page/足利義明.md "wikilink")，於是與父親友胤一同逃到[甲斐投靠](../Page/甲斐.md "wikilink")[武田信虎](../Page/武田信虎.md "wikilink")，成為[武田氏家臣](../Page/武田氏.md "wikilink")。

友胤投靠信虎之後屢建功績，此時自身亦被[武田信虎賜予](../Page/武田信虎.md "wikilink")[偏諱](../Page/偏諱.md "wikilink")（「虎」字），以[足輕大將的身份行動](../Page/足輕大將.md "wikilink")。[大永元年](../Page/大永.md "wikilink")（1521年），在甲斐[飯田河原的戰鬥中擊殺](../Page/飯田河原.md "wikilink")[今川軍的](../Page/今川氏.md "wikilink")[福島正成](../Page/福島正成.md "wikilink")（[北条綱成的實父](../Page/北条綱成.md "wikilink")，但有異說）而獲得功績。

### 武田信玄時期

在信虎遭到其子[信玄流放後](../Page/武田信玄.md "wikilink")，繼續為信玄效力。一般被列為[武田二十四將和](../Page/武田二十四將.md "wikilink")[甲陽五名臣之一](../Page/甲陽五名臣.md "wikilink")（此外，他的兒子[康景是甲陽五名臣之一](../Page/橫田康景.md "wikilink")[横田高松的養子](../Page/横田高松.md "wikilink")）。

由於與[小笠原氏交戰時表現活躍](../Page/小笠原氏.md "wikilink")，被任命為[平瀨城城代等](../Page/平瀨城.md "wikilink")，受到信玄的重用。[天文](../Page/天文_\(後奈良天皇\).md "wikilink")22年（1553年），因為[日蓮宗信徒](../Page/日蓮宗.md "wikilink")（屬於[法華宗派系](../Page/法華宗.md "wikilink")）與[淨土宗信徒的紛爭](../Page/淨土宗.md "wikilink")（虎胤是法華宗信徒），當時信玄希望虎胤能改為淨土宗，但虎胤不肯，因此有一段時間被流放，之後前往[相模国依附](../Page/相模国.md "wikilink")[北條氏](../Page/後北条氏.md "wikilink")，在[善德寺會盟之際歸返](../Page/甲相駿三國同盟.md "wikilink")，在回歸之際，[北条氏康對虎胤的回歸感到相當惋惜](../Page/北条氏康.md "wikilink")。[永祿](../Page/永祿.md "wikilink")2年（1559年），信玄[剃髮](../Page/剃髮.md "wikilink")，於是一同剃髮並自號**清岩**。

永祿4年（1561年），在攻略[信濃](../Page/信濃.md "wikilink")[割嶽城時受傷](../Page/割嶽城.md "wikilink")，沒有參與同年的第4次[川中島之戰](../Page/川中島之戰.md "wikilink")。在永祿7年（1564年）1月28日病死。享年68歲。

## 人物

  - 雖然只是在[信虎時代新加入的家臣](../Page/武田信虎.md "wikilink")，不過與[板垣信方等](../Page/板垣信方.md "wikilink")[國人眾不同](../Page/國人眾.md "wikilink")，與信虎關係非常良好。在信虎被流放後，於[信濃得知消息並急忙返回](../Page/信濃.md "wikilink")[甲斐](../Page/甲斐.md "wikilink")，向板垣信方和[甘利虎泰猛烈抗議](../Page/甘利虎泰.md "wikilink")。

<!-- end list -->

  - 死後，[美濃守的官位被](../Page/美濃守.md "wikilink")[馬場信春繼承而廣為人知](../Page/馬場信春.md "wikilink")。有著「變成武名很高的鬼美濃」的意思。

<!-- end list -->

  - 武名非常高的猛將，有**鬼美濃**、**夜叉美濃**等名號而被畏懼著、而且非常有情義，曾在戰場上扶著負傷的敵將返回敵陣。還擅長攻城戰，攻下的城池經過最低限度的修補就可以再次使用。

## 外部連結

  - [割ヶ岳城～城と古戦場～](http://utsu02.fc2web.com/shiro442.html)
  - [武家家伝＿下総原氏](http://www2.harimaya.com/sengoku/html/c_hara_k.html)

## 登場作品

  - 電視劇

<!-- end list -->

  - 『[武田信玄](../Page/武田信玄_\(大河劇\).md "wikilink")』（[NHK大河劇](../Page/NHK大河劇.md "wikilink")，1988年，演員：[宍戶錠](../Page/宍戶錠.md "wikilink")）
  - 『[風林火山](../Page/風林火山_\(大河劇\).md "wikilink")』（NHK大河劇，2007年，演員：[宍戶開](../Page/宍戶開.md "wikilink")）

## 相關條目

  - [原氏](../Page/原氏.md "wikilink")

[Category:原氏](../Category/原氏.md "wikilink")
[Category:武田二十四將](../Category/武田二十四將.md "wikilink")
[Category:1497年出生](../Category/1497年出生.md "wikilink")
[Category:1564年逝世](../Category/1564年逝世.md "wikilink")
[Category:下總國出身人物](../Category/下總國出身人物.md "wikilink")