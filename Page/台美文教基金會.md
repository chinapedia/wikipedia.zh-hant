**財團法人王桂榮台美文教基金會**（[英語](../Page/英語.md "wikilink")：Taiwanese-American
Foundation），簡稱**台美基金會**、**TAF**，是[台灣與](../Page/台灣.md "wikilink")[美國的一個](../Page/美國.md "wikilink")[基金會](../Page/基金會.md "wikilink")，由[台灣人公共事務會](../Page/台灣人公共事務會.md "wikilink")（FAPA）前會長[王桂榮及其妻](../Page/王桂榮.md "wikilink")[王賽美於](../Page/王賽美.md "wikilink")1982年捐獻一百萬[美元創設](../Page/美元.md "wikilink")，台灣總部設於[台北市](../Page/台北市.md "wikilink")，美國總部設於[加州](../Page/加州.md "wikilink")[長灘](../Page/長灘_\(加利福尼亞州\).md "wikilink")。自1983年起，台美基金會於每兩年11月頒發「人才成就獎」，簡稱「台美獎」（TAF
Award），以獎勵台灣各界傑出人士。

台美獎依創設人建議，先設立三項人才成就獎：「科技工程成就獎」、「人文科學成就獎」、「社會服務及社會科學成就獎」；後又設立「科技研究生獎」和「國際青年社區服務領導獎」。

## 歷屆得獎名單

### 第一屆：1983年

  - 科技工程成就獎——[廖述宗](../Page/廖述宗.md "wikilink")
  - 人文科學成就獎——[陳五福](../Page/陳五福.md "wikilink")
  - 社會服務及社會科學成就獎——[楊逵](../Page/楊逵.md "wikilink")、[江文也](../Page/江文也.md "wikilink")

### 第二屆：1984年

  - 科技工程成就獎——[陳坤木](../Page/陳坤木.md "wikilink")
  - 人文科學成就獎——[王詩琅](../Page/王詩琅.md "wikilink")
  - 社會服務及社會科學成就獎——[陳永興](../Page/陳永興.md "wikilink")

### 第三屆：1985年

  - 科技工程成就獎——[簡逸文](../Page/簡逸文.md "wikilink")、[林俊義](../Page/林俊義.md "wikilink")
  - 人文科學成就獎——[張良澤](../Page/張良澤.md "wikilink")

### 第四屆：1986年

  - 科技工程成就獎——[吳政彥](../Page/吳政彥.md "wikilink")
  - 人文科學成就獎——[鍾肇政](../Page/鍾肇政.md "wikilink")
  - 社會服務及社會科學成就獎——[高俊明](../Page/高俊明.md "wikilink")

### 第五屆：1987年

  - 特別榮譽獎——[李遠哲](../Page/李遠哲.md "wikilink")
  - 科技工程成就獎——[李鎮源](../Page/李鎮源.md "wikilink")、[林宗義](../Page/林宗義.md "wikilink")
  - 人文科學成就獎——[謝里法](../Page/謝里法.md "wikilink")
  - 科技研究生獎——[喻鵬飛](../Page/喻鵬飛.md "wikilink")

### 第六屆：1989年

  - 科技工程成就獎——[林明璋](../Page/林明璋.md "wikilink")、[廖一久](../Page/廖一久.md "wikilink")
  - 人文科學成就獎——[蕭泰然](../Page/蕭泰然.md "wikilink")
  - 社會服務及社會科學成就獎——[釋證嚴](../Page/釋證嚴.md "wikilink")
  - 科技研究生獎——[劉國瑞](../Page/劉國瑞.md "wikilink")

### 第七屆：1991年

  - 科技工程成就獎——[蔡振水](../Page/蔡振水.md "wikilink")、[蘇仲卿](../Page/蘇仲卿.md "wikilink")
  - 人文科學成就獎——[葉石濤](../Page/葉石濤.md "wikilink")
  - 社會服務及社會科學成就獎——[蘭大弼](../Page/蘭大弼.md "wikilink")、[薄柔纜](../Page/薄柔纜.md "wikilink")
  - 科技研究生獎——[林宏哲](../Page/林宏哲.md "wikilink")

### 第八屆：1993年

  - 特別榮譽獎——[彭明敏](../Page/彭明敏.md "wikilink")
  - 科技工程成就獎——[吳成文](../Page/吳成文.md "wikilink")、[王光燦](../Page/王光燦.md "wikilink")、[周烒明](../Page/周烒明.md "wikilink")
  - 人文科學成就獎——[李淑德](../Page/李淑德.md "wikilink")、[林文德](../Page/東方白.md "wikilink")
  - 社會服務及社會科學成就獎——[張漢裕](../Page/張漢裕.md "wikilink")
  - 美術貢獻獎——[李石樵](../Page/李石樵.md "wikilink")
  - 新聞自由貢獻獎——[吳豐山](../Page/吳豐山.md "wikilink")

### 第九屆：1995年

  - 奉獻台灣特別獎——[魏克思](../Page/魏克思.md "wikilink")
  - 科技工程成就獎——[林聖賢](../Page/林聖賢.md "wikilink")、[莊明哲](../Page/莊明哲.md "wikilink")
  - 人文科學成就獎——[李能棋](../Page/李能棋.md "wikilink")（[李喬](../Page/李喬.md "wikilink")）
  - 科技工程研究生獎——[陳威光](../Page/陳威光.md "wikilink")

### 第十屆：1997年

  - 特別榮譽獎——[何大一](../Page/何大一.md "wikilink")
  - 科技工程成就獎——[翁啟惠](../Page/翁啟惠.md "wikilink")、[鄭天佐](../Page/鄭天佐.md "wikilink")、[許重義](../Page/許重義.md "wikilink")
  - 人文科學成就獎——林哲雄
  - 社會服務及社會科學成就獎——[吳繼釗](../Page/吳繼釗.md "wikilink")

### 第十一屆：1999年

  - 科技工程成就獎——[張俊彥](../Page/張俊彥.md "wikilink")
  - 人文科學成就獎——[柏楊](../Page/柏楊.md "wikilink")、[楊青矗](../Page/楊青矗.md "wikilink")
  - 社會服務及社會科學成就獎——[柯蔡玉瓊](../Page/柯蔡玉瓊.md "wikilink")
  - 科技研究生獎——[林希龍](../Page/林希龍.md "wikilink")
  - 國際青年社區服務領導獎——[杜文苓](../Page/杜文苓.md "wikilink")、[Kumi
    Naidoo](../Page/Kumi_Naidoo.md "wikilink")

### 第十二屆：2001年

  - 特別貢獻獎——[李登輝](../Page/李登輝.md "wikilink")
  - 科技工程成就獎——[范良政](../Page/范良政.md "wikilink")、[何汝諧](../Page/何汝諧.md "wikilink")、[洪伯文](../Page/洪伯文.md "wikilink")
  - 人文科學成就獎——[黃娟](../Page/黃娟.md "wikilink")、[蔡瑞月](../Page/蔡瑞月.md "wikilink")、[陳慕融](../Page/陳慕融.md "wikilink")
  - 社會服務及社會科學成就獎——[劉俠](../Page/劉俠.md "wikilink")

### 第十三屆：2003年

  - 特別貢獻獎——[呂秀蓮](../Page/呂秀蓮.md "wikilink")
  - 科技工程成就獎——[賴明詔](../Page/賴明詔.md "wikilink")、[李國雄](../Page/李國雄.md "wikilink")
  - 人文科學成就獎——[林榮德](../Page/林榮德.md "wikilink")
  - 社會服務及社會科學成就獎——[陳文彥](../Page/陳文彥.md "wikilink")、[金恒煒](../Page/金恒煒.md "wikilink")

### 第十四屆：2005年

  - 科技工程成就獎——[李文雄](../Page/李文雄.md "wikilink")
  - 人文科學成就獎——[陳錦芳](../Page/陳錦芳.md "wikilink")、[李壬癸](../Page/李壬癸.md "wikilink")

### 第十五屆：2007年

  - 科技工程成就獎——[王惠鈞](../Page/王惠鈞.md "wikilink")、[陳垣崇](../Page/陳垣崇.md "wikilink")
  - 人文科學成就獎——[呂泉生](../Page/呂泉生.md "wikilink")
  - 社會服務及社會科學成就獎——[柴-{松}-林](../Page/柴松林.md "wikilink")

## 外部連結

  - [王桂榮台美文教基金會](https://web.archive.org/web/20130523033943/http://tafawards.org.tw/)

[Category:台灣基金會](../Category/台灣基金會.md "wikilink")
[Category:美國民間組織](../Category/美國民間組織.md "wikilink")
[Category:1982年建立的組織](../Category/1982年建立的組織.md "wikilink")