**黄纬禄**（
），[安徽省](../Page/安徽省.md "wikilink")[芜湖人](../Page/芜湖.md "wikilink")，自动控制和导弹技术专家，[中国导弹与航天技术的主要开拓者之一](../Page/中国.md "wikilink")，[两弹一星功勋奖章获得者](../Page/两弹一星功勋奖章.md "wikilink")，曾参与开拓[中国核动力潜水艇（核潜艇）的研制](../Page/09I型核潜艇.md "wikilink")。\[1\]\[2\]

1940年毕业于[国立中央大学](../Page/国立中央大学_\(南京\).md "wikilink")[工学院电机工程系](../Page/东南大学.md "wikilink")，1947年在英国[伦敦大学](../Page/伦敦大学.md "wikilink")[帝国学院获硕士学位](../Page/倫敦帝國學院.md "wikilink")。1957年回国，1950年代末出任中国液体战略导弹控制系统的总设计师。曾参与研制多级的“东风”系列中远程导弹和由它改进而成的[长征一号运载火箭](../Page/长征一号.md "wikilink")。1970年，调任潜艇发射的固体战略导弹“[巨浪一号](../Page/巨浪-1.md "wikilink")”研发工作的总负责人，1979年被任命为中国核潜艇副总设计师之一\[3\]，1982年10月中国第一代战略弹道潜射导弹巨浪一号由潜艇水下试射成功\[4\]。

2006年10月，与[钱学森](../Page/钱学森.md "wikilink")、[屠守锷](../Page/屠守锷.md "wikilink")、[任新民](../Page/任新民.md "wikilink")、[梁守槃等共](../Page/梁守槃.md "wikilink")5位专家获“中国航天事业五十年最高荣誉奖”。

## 参考文献

## 参见

  - [东方红人造卫星系列](../Page/东方红人造卫星系列.md "wikilink")
  - [中国航天史](../Page/中国航天史.md "wikilink")

{{-}}

[Category:中華人民共和國工程師](../Category/中華人民共和國工程師.md "wikilink")
[Category:倫敦帝國學院校友](../Category/倫敦帝國學院校友.md "wikilink")
[工](../Category/中央大学校友.md "wikilink")
[Category:南京大學校友](../Category/南京大學校友.md "wikilink")
[⒗H](../Category/东南大学校友.md "wikilink")
[Category:芜湖人](../Category/芜湖人.md "wikilink")
[Wei緯祿](../Category/黃姓.md "wikilink")
[Category:安徽科学家](../Category/安徽科学家.md "wikilink")
[Category:扬州中学校友](../Category/扬州中学校友.md "wikilink")
[Category:中国科学院信息技术科学部院士](../Category/中国科学院信息技术科学部院士.md "wikilink")

1.  [黄纬禄 —
    让“蛟龙”跃出海面的人](https://web.archive.org/web/20141204145325/http://news.xinhuanet.com/video/2006-11/16/content_5339277.htm)
2.  [“两弹一星”元勋黄纬禄同志逝世](http://cpc.people.com.cn/GB/64093/87393/16436271.html)
3.
4.