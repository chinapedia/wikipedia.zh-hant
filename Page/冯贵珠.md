**冯贵珠**（**Evangeline Frances "Eva"
French**，1869年-1960年7月8日）是一位[英国](../Page/英国.md "wikilink")[内地会派往中国的女传教士](../Page/内地会.md "wikilink")。

1869年，冯贵珠出生在[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")，是家中的长女
，在瑞士[日内瓦接受中学教育](../Page/日内瓦.md "wikilink")。1893年，她受内地会差遣，前往中国，在山西[霍州传教](../Page/霍州.md "wikilink")。1900年夏天，义和团事变发生时，冯贵珠被迫逃离中国。1902年，她重返中国。1908年，她母亲去世之后，妹妹冯贵石也前来加入传教行列。

1923年，冯贵石、冯贵珠和[盖群英](../Page/盖群英.md "wikilink")（Alice Mildred
Cable）3位女传教士离开山西霍州，以甘肃肃州（酒泉）为基地，开始对新疆的巡回布道，直到1950年代被迫离开。

1960年7月8日，冯贵珠在英国[多塞特郡的Shaftesbury去世](../Page/多塞特郡.md "wikilink")。

## 延伸阅读

  - [内地会在华传教士列表](../Page/内地会在华传教士列表.md "wikilink")

## 外部链接

  - [Muslims, Missionaries and Warlords in Northwestern
    China](https://web.archive.org/web/20060909093553/http://www2.oakland.edu/oujournal/files/Benson.pdf)
    (article by Dr. Benson)

[F](../Category/英國基督徒.md "wikilink")
[F](../Category/内地会传教士.md "wikilink")
[F](../Category/1869年出生.md "wikilink")
[F](../Category/1960年逝世.md "wikilink")