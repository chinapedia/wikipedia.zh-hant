《**唱遊大世界王菲香港演唱會 98-99**》（***Faye HK Scenic Tour
98-99***）是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王菲的第二張](../Page/王菲.md "wikilink")[演唱會專輯](../Page/演唱會.md "wikilink")，於1999年12月在[香港发行](../Page/香港.md "wikilink")，2CD裝。CD1是17首歌曲，CD2為12首歌曲。專輯是1998年12月31日晚迎接新年演唱會上的實錄。

## 曲目

## 幕後人員

出自專輯內頁

  - 導演 - Ida Wong
  - 音樂總監 - Terry Chan
  - Drum - 竇唯
  - Synthesizer - Frankie Li
  - Guitar - 張亞東 / 蘇德華 / Danny Leung
  - Bass - 岳浩崑
  - Percussion - 劉效松
  - Chorus - Jackie Cho / Chow Siu Kwan / 竇穎 / 江竹 / 王英姿

<!-- end list -->

  - Producer - Alvin Leong
  - Live Recording Engineers - David Sum / Bryan Choy / Raymond Yu
  - Mixing Studio - Avon Recording Studios
  - Mixing Engineer - David Sum
  - Studio re-recording and Editing Engineer - Simon Li
  - Mastering Studio - Avon Recording Studios
  - Mastered by - Anthony Yeung
  - Executive Producer - A Production House

## 唱片版本

  - [香港版](../Page/香港.md "wikilink")
  - [台灣版](../Page/台灣.md "wikilink")
  - [日本版](../Page/日本.md "wikilink")：附加「Eyes one Me (Live)」，2000年發行。
  - [大陸版](../Page/大陸.md "wikilink")

影像出版：

  - [卡拉OK版](../Page/卡拉OK.md "wikilink")
    [DVD](../Page/DVD.md "wikilink")/[VCD](../Page/VCD.md "wikilink")：2000年發行，去掉了「Bohemian
    Rhapsody」和「Auld Lang Syne」。
  - 視聽版 [DVD](../Page/DVD.md "wikilink")：2000年發行，去掉了「Bohemian Rhapsody」。

## 注釋

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[Category:王菲音樂專輯](../Category/王菲音樂專輯.md "wikilink")
[Category:1999年音樂專輯](../Category/1999年音樂專輯.md "wikilink")