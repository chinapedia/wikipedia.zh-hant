**云岩区**，[贵阳市的城区之一](../Page/贵阳市.md "wikilink")。2002年末，总人口54.68万人，其中[少数民族人口](../Page/少数民族.md "wikilink")7.97万人，人数较多的[少数民族是](../Page/少数民族.md "wikilink")[苗族](../Page/苗族.md "wikilink")；非农业人口51.44万人。

云岩区是[贵阳市的经济强区之一](../Page/贵阳市.md "wikilink")。2003年，该区[国内生产总值突破](../Page/国内生产总值.md "wikilink")100亿元[人民币](../Page/人民币.md "wikilink")，为[贵州省最高](../Page/贵州省.md "wikilink")。该区经济中，第二、三产业的比重大，增长快；而传统的[农业则在近几年来有所下降](../Page/农业.md "wikilink")。

该区[商业发达](../Page/商业.md "wikilink")，区内有星力百货、[国贸](../Page/国贸.md "wikilink")、[北京华联等大型的](../Page/北京华联.md "wikilink")[购物商场和](../Page/购物商场.md "wikilink")[超市](../Page/超市.md "wikilink")。同时该区也是[贵阳市重要的](../Page/贵阳市.md "wikilink")[文化](../Page/文化.md "wikilink")、[行政区域](../Page/行政.md "wikilink")，区内有[贵州省](../Page/贵州省.md "wikilink")[图书馆](../Page/图书馆.md "wikilink")、[贵州省](../Page/贵州省.md "wikilink")[博物馆](../Page/博物馆.md "wikilink")、[贵州省](../Page/贵州省.md "wikilink")[人民政府等单位](../Page/人民政府.md "wikilink")。

云岩区的著名[旅游景点有](../Page/旅游景点.md "wikilink")：[黔灵公园](../Page/黔灵公园.md "wikilink")、东山等。
云岩区在[贵州省](../Page/贵州省.md "wikilink")2004年全省“20强县”中排名第一。

云岩区的教育机构有：[贵州教育厅](../Page/贵州教育厅.md "wikilink")、[贵州师范大学](../Page/贵州师范大学.md "wikilink")、[贵州商业高等专业学校](../Page/贵州商业高等专业学校.md "wikilink")、[贵阳财校](../Page/贵阳财校.md "wikilink")、[贵州交通职业技术学院](../Page/贵州交通职业技术学院.md "wikilink")、[贵州化工职业技术学院等高校](../Page/贵州化工职业技术学院.md "wikilink")。

云岩区的书店有：[新华书店](../Page/新华书店.md "wikilink")、[西西弗书店](../Page/西西弗.md "wikilink")、[西南风书店等多家书店](../Page/西南风书店.md "wikilink")

## 行政区划

下辖1个[镇](../Page/镇.md "wikilink")、27个社区服务中心\[1\]\[2\]：

。

## 参考文献

[云岩区](../Category/云岩区.md "wikilink")
[区](../Category/贵阳区县市.md "wikilink")
[贵阳](../Category/贵州市辖区.md "wikilink")

1.
2.  [贵阳市人民政府关于撤销四十九个街道办事处设立九十个社区服务中心的通知（筑府发〔2012〕58号）](http://www.gygov.gov.cn/art/2012/8/14/art_18583_411039.html)