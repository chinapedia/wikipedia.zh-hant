**开运**（944年七月—946年）是[后晋出帝](../Page/后晋.md "wikilink")[石重贵的](../Page/石重贵.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。[吳越成宗](../Page/吳越.md "wikilink")[钱弘佐](../Page/钱弘佐.md "wikilink")、[楚文昭王](../Page/楚.md "wikilink")[馬希範](../Page/馬希範.md "wikilink")、[荆南文獻王](../Page/荆南.md "wikilink")[高從誨也用此年号](../Page/高從誨.md "wikilink")\[1\]。

## 纪年

| 开运                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 944年                           | 945年                           | 946年                           |
| [干支](../Page/干支纪年.md "wikilink") | [甲辰](../Page/甲辰.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他政权使用的[开运年号](../Page/开运.md "wikilink")
  - 同期存在的其他政权年号
      - [保大](../Page/保大_\(李璟\).md "wikilink")（943年三月至957年）：[南唐](../Page/南唐.md "wikilink")—[李璟之年號](../Page/李璟.md "wikilink")
      - [天德](../Page/天德_\(王延政\).md "wikilink")（943年二月至945年八月）：[殷](../Page/闽_\(十国\).md "wikilink")—[王延政之年號](../Page/王延政.md "wikilink")
      - [乾和](../Page/乾和.md "wikilink")（943年十一月至958年七月）：南漢—劉晟之年號
      - [廣政](../Page/廣政_\(孟昶\).md "wikilink")（938年正月至965年正月）：[後蜀](../Page/後蜀.md "wikilink")—[孟昶之年號](../Page/孟昶.md "wikilink")
      - [文經](../Page/文經.md "wikilink")（945年）：大理國—[段思英之年號](../Page/段思英.md "wikilink")
      - [至治](../Page/至治_\(段思良\).md "wikilink")（946年至951年）：大理國—[段思良之年號](../Page/段思良.md "wikilink")
      - [會同](../Page/会同_\(辽朝\).md "wikilink")（938年十一月至947年正月）：契丹—耶律德光之年號
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天慶](../Page/天慶_\(朱雀天皇\).md "wikilink")（938年五月二十二日至947年四月二十二日）：日本[朱雀天皇](../Page/朱雀天皇.md "wikilink")、[村上天皇年号](../Page/村上天皇.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:后晋年号](../Category/后晋年号.md "wikilink")
[Category:吴越年号](../Category/吴越年号.md "wikilink")
[Category:马楚年号](../Category/马楚年号.md "wikilink")
[Category:荆南年号](../Category/荆南年号.md "wikilink")
[Category:940年代中国政治](../Category/940年代中国政治.md "wikilink")

1.  李崇智，中国历代年号考，中华书局，2004年12月,147-149，156 ISBN 7101025129