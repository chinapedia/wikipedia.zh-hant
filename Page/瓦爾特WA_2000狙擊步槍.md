**瓦尔特WA
2000**（）是由[卡爾·瓦爾特運動槍有限公司設計及生產的](../Page/卡爾·瓦爾特運動槍有限公司.md "wikilink")[犢牛式](../Page/犢牛式.md "wikilink")[狙擊步槍](../Page/狙擊步槍.md "wikilink")，WA
2000
是完全以軍警狙擊手需要為唯一目標的全新設計，不同於其他狙擊步槍多以現有槍型改造加強來滿足市場需要。該槍設計優異，準確度極高。不過由於完全以品質為首不計製造成本，造成售價昂貴，乏人問津，1988年11月停止量產，但现在仍然接受订制。

## 設計

瓦尔特WA 2000采用[-{zh-hans:无托结构;
zh-hant:犢牛式;}-把發射組件後移至](../Page/犢牛式.md "wikilink")[槍托以內](../Page/槍托.md "wikilink")，縮短其整體長度，而保持槍管長度。

瓦尔特WA 2000在夜間可使用PV4[夜視鏡來協助狙擊](../Page/夜視鏡.md "wikilink")。

以[.300
溫徹斯特麥格農作為主要彈藥的原因是其遠距離準確度較高](../Page/.300_Winchester_Magnum.md "wikilink")，6發可分離式彈匣令裝填更快。

## 歷史

瓦爾特WA 2000
於1988年11月停止生產時只製造了176支，其中有兩代、三種口徑，[-{zh-hans:.300温彻斯特马格南;zh-hk:.300溫徹斯特馬格南;zh-tw:.300溫徹斯特麥格農;}-](../Page/.300_Winchester_Magnum.md "wikilink")、[7.62×51毫米北約彈和最後推出的瑞士](../Page/7.62×51mm_NATO.md "wikilink")7.5×55毫米彈，各代沒有獨立名稱。第一代裝有罐型，第二代則使用傳統式消焰器。

瓦爾特WA 2000有限地被德國警察所使用，但基於在戰場上可靠性較低的原故而沒有被任何軍隊採用。

在1970年代後期推出的瓦爾特WA
2000售價昂貴，至1988年，每枝售價大約$9,000至$12,500美元（不包括瞄準鏡）。而176支瓦爾特WA
2000中僅有15支出口到美國，其中11支由Earl J. Sheehan, Jr.（瓦爾特
美國分公司總裁）私人擁有。由於非常罕有，現在美國市場新槍售價高達$75,000至$80,000美元。

## 使用國

  - /
    －有限地被德國的[警察部隊及特警隊](../Page/警察.md "wikilink")[特别行动突击队](../Page/特别行动突击队.md "wikilink")（SEK）所採用。

## 流行文化

### 電影

  - 1987年—《[-{zh-hans:黎明生机; zh-hk:鐵金剛大戰特務飛龍;
    zh-hant:007：黎明生機;}-](../Page/黎明生机.md "wikilink")》：[詹姆斯·邦德](../Page/詹姆斯·邦德.md "wikilink")（[提摩西·達頓飾演](../Page/提摩西·達頓.md "wikilink") ）曾使用过一枝带有[夜视仪和](../Page/夜视仪.md "wikilink")[镭射瞄准器的WA](../Page/镭射瞄准器.md "wikilink")2000。
  - 2002年—《[-{zh-hans:撕裂的末日; zh-hk:未來殺人網絡;
    zh-hant:重裝任務;}-](../Page/重裝任務.md "wikilink")》（Equilibrium）：被清道夫們射殺狗隻時所使用，奇怪地沒有裝上瞄準鏡及[兩腳架並被在聲效上類似於](../Page/兩腳架.md "wikilink")[霰彈槍](../Page/霰彈槍.md "wikilink")。
  - 2007年—《[-{zh-hans:杀手：代号47;
    zh-hant:殺手47;}-](../Page/殺手47.md "wikilink")》（Hitman）：代號47（[蒂莫西·奧利芬特飾演](../Page/蒂莫西·奧利芬特.md "wikilink")）與CIA幹員電話交涉時，用WA2000瞄準對方。

### 電子遊戲

  - 1998年—《[-{zh-hans:彩虹六号;zh-hant:虹彩六號;}-](../Page/彩虹六号.md "wikilink")》（Rainbow
    Six）：早期版本的-{zh-hans:彩虹六号;zh-hant:虹彩六號;}-游戏当中WA2000是可以选择的一把武器。
  - 2000年—《[-{zh-hans:杀手：代号47;
    zh-hant:刺客任務：殺手47;}-](../Page/殺手：代號47.md "wikilink")》（Hitman）
  - 2004年—《[特種部隊Online](../Page/特種部隊Online.md "wikilink")》（Special Force
    Online）：為目前遊戲三把半自動[狙擊步槍中之一](../Page/狙擊步槍.md "wikilink")，彈匣容量為5發，價格75000SP。
  - 2006年—《[-{zh-hans:杀手：血钱;zh-hant:刺客任務：黑錢交易;}-](../Page/刺客任務：黑錢交易.md "wikilink")》（Hitman:Blood
    Money）：為遊戲中主角一開始就能使用的狙擊槍，並能進行各項升級（滅音、改裝等等）。
  - 2008年—《[AVA
    Online](../Page/戰地之王.md "wikilink")》：遊戲內為7.62mm口徑版本，彈匣容量為6發。
  - 2007年—《[-{zh-hans:反恐精英Online;zh-hant:絕對武力Online;}-](../Page/絕對武力Online.md "wikilink")》（Counter
    Strike
    Online）：本武器在遊戲中稱作「**疾風獵鷹**」（大陆称狂怒骑士），奇怪的可裝塡12發7.62mm子彈。也有特殊的黃金版衍生型，可裝彈15發。
  - 2009年—《[-{zh-hans:使命召唤：现代战争2;zh-hant:決勝時刻：現代戰爭2;}-](../Page/決勝時刻：現代戰爭2.md "wikilink")》（Call
    of Duty: Modern Warfare
    2）：在單人模式與多人模式皆有出現。在單人模式可裝彈10發，聯機模式則為6發，最高攜彈量為18發（聯機模式）。單人模式及特種作戰模式時被[俄羅斯](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝力量及影子部队所使用](../Page/極端民族主義.md "wikilink")。在聯機模式於等級36解鎖，並可以使用[紅點鏡](../Page/紅點鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、[心跳探測器](../Page/心率.md "wikilink")、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[熱能探測式瞄具](../Page/熱輻射.md "wikilink")、[前握把](../Page/輔助握把.md "wikilink")、、、[延長彈匣](../Page/彈匣.md "wikilink")。
  - 2010年—《[-{zh-hans:使命召唤：黑色行动;zh-hant:決勝時刻：黑色行動;}-](../Page/決勝時刻：黑色行動.md "wikilink")》（Call
    of Duty: Black
    Ops）：於單機模式及聯機模式中出現，載彈量為6發，最高攜彈量為18發（聯機模式）。奇怪地會在1960年代出現。聯機模式時於等級10解鎖，可使用的改裝包括：延長彈匣、[ACOG光學瞄準鏡](../Page/先進戰鬥光學瞄準鏡.md "wikilink")、[紅外線探測式瞄具](../Page/紅外線.md "wikilink")、[抑制器和增大範圍瞄準鏡](../Page/抑制器.md "wikilink")。
  - 2010年—《[-{zh-hans:合金装备
    和平行者;zh-hant:潜龍諜影：和平先驅;}-](../Page/潜龍諜影：和平先驅.md "wikilink")》
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：命名为“Walther WA
    2000”，为狙击手专用武器，使用6发[弹匣](../Page/弹匣.md "wikilink")，奇怪的卸掉了机匣上部的双脚架且在下护木上加装战术导轨。中国大陆服务器为专家解锁、欧美与俄罗斯服务器为高级解锁，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、狙击枪消音器、[狙击枪制退器](../Page/炮口制动器.md "wikilink")）、战术导轨配件（[狙击枪双脚架](../Page/两脚架.md "wikilink")、特殊狙击枪双脚架）以及瞄准镜（[狙击枪5.5x瞄准镜](../Page/瞄准镜.md "wikilink")、狙击枪4.5x中程瞄准镜、[狙击枪4x短程瞄准镜](../Page/Trijicon_TR20瞄准镜.md "wikilink")、狙击槍5x快速瞄准镜）。
  - 2013年—《[-{zh-hans:收获日;zh-hant:劫薪日;}-2](../Page/劫薪日2.md "wikilink")》（Payday
    2）：命名為「Lebensauger .308」，奇怪的可裝塡10發子彈。
  - 2016年—《[刺客任務](../Page/杀手_\(2016年游戏\).md "wikilink")》：命名為W2000，可透過遊戲中獎勵獲得。
  - 2017年—《[少女前線](../Page/少女前線.md "wikilink")》（Girl's
    Frontline）：以[拟人化美少女的形象出现](../Page/萌拟人化.md "wikilink")，性格[傲嬌](../Page/傲嬌.md "wikilink")，五星，由日本聲優[戶松遙擔任配音](../Page/戶松遙.md "wikilink")。
  - 2018年—《[刺客任務2
    (2018年遊戲)](../Page/杀手2_\(2018年游戏\).md "wikilink")》：可使用傳承包獲得2016年版的造型，遊戲本身也有專屬的造型和改良型的登場。

### 小說

  - 1998年—《[-{zh-hans:全金属狂潮;zh-hant:驚爆危機;}-](../Page/驚爆危機.md "wikilink")》（Full
    Metal Panic\!）：角色[克鲁兹·威巴的随身武器](../Page/克鲁兹·威巴.md "wikilink")。
  - 2006年—《[Fate/Zero](../Page/Fate/Zero.md "wikilink")》：角色衛宮切嗣使用的武器之一。
  - 2008年—《[-{zh-hans:绯弹的亚里亚;zh-hant:緋彈的亞莉亞;}-](../Page/緋彈的亞莉亞.md "wikilink")》（Aria
    the Scarlet Ammo）：角色佩特拉的随身武器。

### 動畫

  - 2003年—《[神槍少女](../Page/神槍少女.md "wikilink")》（Gunslinger
    Girl）：被荷莉葉特所使用。

## 參考

  - [牛犢式結構](../Page/犢牛式_\(槍械\).md "wikilink")
  - [狙擊步槍](../Page/狙擊步槍.md "wikilink")
  - [HK PSG1](../Page/HK_PSG1狙擊步槍.md "wikilink")
  - [卡爾·瓦爾特運動槍有限公司](../Page/卡爾·瓦爾特運動槍有限公司.md "wikilink")

## 外部連結

  - —[WA 2000圖片資料](http://www.snipercentral.com/wa2000.htm)

  - —[Modern Firearms—Walther
    WA 2000](http://world.guns.ru/sniper/sniper-rifles/de/walther-wa-2000-e.html)

  - —[槍炮世界—WA 2000狙击步枪](http://firearmsworld.net/german/walther/wa2000/wa2000.htm)

[Category:半自動步槍](../Category/半自動步槍.md "wikilink")
[Category:狙击步枪](../Category/狙击步枪.md "wikilink") [Category:.300
Winchester
Magnum口徑槍械](../Category/.300_Winchester_Magnum口徑槍械.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")