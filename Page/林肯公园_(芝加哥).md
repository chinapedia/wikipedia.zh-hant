**林肯公园**（）以[亚伯拉罕·林肯命名](../Page/亚伯拉罕·林肯.md "wikilink")，佔地1,208英畝（489公頃），位於[伊利諾伊州北部的](../Page/伊利諾伊州.md "wikilink")[芝加哥湖畔](../Page/芝加哥.md "wikilink")，面向[密歇根湖](../Page/密歇根湖.md "wikilink")。它是芝加哥面積最大的公園。林肯公園每年有2000萬名遊客參觀，是美國第二多遊客的公園。\[1\]

## 参考资料

[Category:芝加哥公园](../Category/芝加哥公园.md "wikilink")
[Category:密歇根湖](../Category/密歇根湖.md "wikilink")

1.   p. 25.