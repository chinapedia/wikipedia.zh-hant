**川菜**是[中國菜中的](../Page/中國菜.md "wikilink")[四大菜系之一](../Page/四大菜系.md "wikilink")，因起源于四川地区（含今[重庆直辖市](../Page/重庆.md "wikilink")）而得名，以麻、辣、鲜、香為特色，以一菜一格、百菜百味而闻名。其中，[麻辣是川菜最具特色的口味](../Page/麻辣.md "wikilink")，其每日所用的大部分餐點都含有高於一般人能接受程度的麻辣，居住於此地區的四川人和重庆人也以能吃辣而聞名於世。

## 历史

古典川菜的出现可追溯至[秦汉](../Page/秦汉.md "wikilink")，在[宋代已经形成流派](../Page/宋代.md "wikilink")，当时的影响范围主要是[中原](../Page/中原.md "wikilink")。\[1\][花椒原生於中國秦蜀等地](../Page/花椒.md "wikilink")，《[神農本草經](../Page/神農本草經.md "wikilink")》載有秦椒及蜀椒。

[明末](../Page/明.md "wikilink")[清初](../Page/清.md "wikilink")，[辣椒由](../Page/辣椒.md "wikilink")[美洲經](../Page/美洲.md "wikilink")[歐洲引入](../Page/歐洲.md "wikilink")[中國](../Page/中國.md "wikilink")，到清朝中期，[中国菜确定主要](../Page/中国菜.md "wikilink")[调味剂](../Page/调味剂.md "wikilink")——辣椒和[蔗糖的运用](../Page/蔗糖.md "wikilink")。川菜也開始用上[辣椒调味](../Page/辣椒.md "wikilink")，遂形成以麻辣味为主的料理方式，现代意义上的川菜開始成形。1646年，[肅親王](../Page/肅親王.md "wikilink")[豪格殺](../Page/豪格.md "wikilink")[張獻忠後](../Page/張獻忠.md "wikilink")，大批[漢人西徙四川](../Page/漢人.md "wikilink")，創製出四川獨有的迩調技藝。\[2\]清[乾隆年间](../Page/乾隆.md "wikilink")，四川[罗江文人](../Page/罗江.md "wikilink")[李调元在其](../Page/李调元.md "wikilink")《[函海](../Page/函海.md "wikilink")·醒园录》中系统地搜集了川菜的[38种烹调方法](../Page/川菜#烹调方法.md "wikilink")。

晚清以来，川菜逐步形成地方风味极其浓郁的菜系，由筵席菜、大众便餐菜、家常菜、三蒸九扣菜、风味[小吃等](../Page/小吃.md "wikilink")5类菜肴组成完整的风味体系。其风味则是清、[鲜](../Page/鲜.md "wikilink")、[醇](../Page/醇.md "wikilink")、浓并重，并以[麻辣著称](../Page/麻辣.md "wikilink")。对[长江上游和](../Page/长江.md "wikilink")[滇](../Page/滇.md "wikilink")、[黔等地均有相当的影响](../Page/黔.md "wikilink")。

## 特点

川菜以[成都和](../Page/成都.md "wikilink")[重庆两地的菜肴为代表](../Page/重庆.md "wikilink")。所用的调味品有[花椒](../Page/花椒.md "wikilink")、[胡椒](../Page/胡椒.md "wikilink")、[辣椒](../Page/辣椒.md "wikilink")，合稱“三椒”；[葱](../Page/葱.md "wikilink")、[薑](../Page/薑.md "wikilink")、[蒜](../Page/蒜.md "wikilink")，合稱“三香”；以及[郫县豆瓣酱](../Page/郫县豆瓣酱.md "wikilink")、[永川豆豉等亦使用频繁](../Page/永川豆豉.md "wikilink")，以調味為重點的有“鱼香”、“怪味”等菜。川菜的风格朴实而又清新，官家川菜精细别致，农家川菜具浓厚的乡土气息

川菜有“七滋八味”之说，“七滋”指[甜](../Page/甜.md "wikilink")、[酸](../Page/酸.md "wikilink")、[麻](../Page/麻.md "wikilink")、[辣](../Page/辣.md "wikilink")、[苦](../Page/苦.md "wikilink")、[香](../Page/香.md "wikilink")、[咸](../Page/咸.md "wikilink")；“八味”即是[鱼香](../Page/鱼香.md "wikilink")、[酸辣](../Page/酸辣.md "wikilink")、椒麻、[怪味](../Page/怪味.md "wikilink")、[麻辣](../Page/麻辣.md "wikilink")、[红油](../Page/红油.md "wikilink")、[姜汁](../Page/姜汁.md "wikilink")、[家常](../Page/家常.md "wikilink")。

川菜突出的是[麻](../Page/麻.md "wikilink")、[辣](../Page/辣.md "wikilink")、[香](../Page/香.md "wikilink")、[鲜及](../Page/鲜.md "wikilink")[油大](../Page/油.md "wikilink")、味厚的特点，重用“三椒”和鲜[薑](../Page/薑.md "wikilink")。在7种基本味型的基础上，又可调配变化为多种复合味型。

川菜的复合味型有20多种，如咸鲜味型、家常味型、[麻辣味型](../Page/麻辣.md "wikilink")、糊辣味型、鱼香味型、姜汁味型、怪味味型、椒麻味型、酸辣味型、[红油味型](../Page/红油.md "wikilink")、[蒜泥味型](../Page/蒜泥.md "wikilink")、[麻酱味型](../Page/麻酱.md "wikilink")、酱香味型、烟香味型、[荔枝味型](../Page/荔枝.md "wikilink")、[五香味型](../Page/五香.md "wikilink")、香糟味型、糖醋味型、甜香味型、[陈皮味型](../Page/陈皮.md "wikilink")、[芥末味型](../Page/芥末.md "wikilink")、咸甜味型、[椒盐味型](../Page/椒盐.md "wikilink")、糊辣荔枝味型、[茄汁味型等等](../Page/茄汁.md "wikilink")，形成了川菜的特殊风味。这其中以鱼香、[红油](../Page/红油.md "wikilink")、怪味、[麻辣较为常见](../Page/麻辣.md "wikilink")。

在川菜烹饪过程中，如能运用味的主次、浓淡、多寡，调配变化，加之选料、切配和烹调得当，即可获得色香味形俱佳的具有特殊风味的各种美味佳肴。

### 烹调

川菜烹调讲究品种丰富、味多，並讲究[烹饪技术](../Page/烹饪.md "wikilink")、制作工艺精细、操作要求严格。

川菜烹调有四个特点：一是选料认真、二是[刀工精细](../Page/刀工.md "wikilink")、三是合理搭配、四是精心烹调。

#### 烹调方法

在烹调方法上常用[炒](../Page/炒.md "wikilink")、[滑](../Page/滑.md "wikilink")、[熘](../Page/熘.md "wikilink")、爆、[煸](../Page/煸.md "wikilink")、[炸](../Page/炸.md "wikilink")、[煮](../Page/煮.md "wikilink")、[煨等](../Page/煨.md "wikilink")，尤为小[煎](../Page/煎.md "wikilink")、[小炒](../Page/小炒.md "wikilink")、干煸和干烧有極具特色。

  - 热菜类：

[炒](../Page/炒.md "wikilink")、[滑](../Page/滑.md "wikilink")、爆、[煸](../Page/煸.md "wikilink")、[溜](../Page/溜.md "wikilink")、[炝](../Page/炝.md "wikilink")、[炸](../Page/炸.md "wikilink")、[煮](../Page/煮.md "wikilink")、烫、糁、[煎](../Page/煎.md "wikilink")、[蒙](../Page/蒙.md "wikilink")、[贴](../Page/贴.md "wikilink")、[酿](../Page/酿.md "wikilink")、[卷](../Page/卷.md "wikilink")、[蒸](../Page/蒸.md "wikilink")、[烧](../Page/烧.md "wikilink")、[焖](../Page/焖.md "wikilink")、[炖](../Page/炖.md "wikilink")、[摊](../Page/摊.md "wikilink")、[煨](../Page/煨.md "wikilink")、[烩](../Page/烩.md "wikilink")、淖、[烤](../Page/烤.md "wikilink")、烘、粘、[汆](../Page/汆.md "wikilink")、糟、[醉](../Page/醉.md "wikilink")、[冲](../Page/冲.md "wikilink")

  - 冷菜类：

[拌](../Page/拌.md "wikilink")、[滷](../Page/滷.md "wikilink")、[燻](../Page/燻.md "wikilink")、[醃](../Page/醃.md "wikilink")、[腊](../Page/腊.md "wikilink")、[冻](../Page/冻.md "wikilink")、[醬](../Page/醬.md "wikilink")

## 派別

### 上河帮

又稱**成都菜**，也稱**蓉菜**或**蓉派川菜**，以[成都菜式為主](../Page/成都.md "wikilink")，此派菜式精致细腻，多为流传久远的传统川菜，旧时历来作为四川总督的官家菜，一般酒店中高级宴会菜式中的川菜均以成都川菜为标准菜谱制作。其中被誉为川菜之王，名厨[黄敬临在清宫御膳房时创制的高级清汤菜](../Page/黄敬临.md "wikilink")，常常用于比喻厨师厨艺最高等级的“开水白菜”便是成都川菜登封造极的菜式。

成都地处天府之国川西平原，因此蓉派川菜讲求用料上等，配比精细准确，严格以传统经典菜谱为准，其味温和，绵香悠长。通常颇具典故。近几年风靡全国的清油火锅是成都人改良的。

代表名菜：

  - 家常菜
      - [麻婆豆腐](../Page/麻婆豆腐.md "wikilink")
      - [回锅肉](../Page/回锅肉.md "wikilink")
      - [宫保鸡丁](../Page/宫保鸡丁.md "wikilink")
      - 东坡肘子
      - 咸烧白
      - [粉蒸肉](../Page/粉蒸肉.md "wikilink")
      - [夫妻肺片](../Page/夫妻肺片.md "wikilink")
      - [蚂蚁上树](../Page/蚂蚁上树.md "wikilink")
      - 灯影牛肉
      - 蒜泥白肉
      - [口水鸡](../Page/口水鸡.md "wikilink")
      - 雞公煲
      - [辣子鸡](../Page/辣子鸡.md "wikilink")
      - 花椒鸡
      - [樟茶鸭](../Page/樟茶鸭.md "wikilink")
      - 白油豆腐
      - [鱼香肉丝](../Page/鱼香肉丝.md "wikilink")
      - 泉水豆花
      - 干煸四季豆
      - 盐煎肉
      - [干煸鳝片](../Page/干煸鳝片.md "wikilink")
      - [水煮鱼](../Page/水煮鱼.md "wikilink")
      - 水煮牛肉
      - 冷吃兔
      - 冷吃牛肉
      - [烧白](../Page/烧白.md "wikilink")
      - [酸辣汤](../Page/酸辣汤.md "wikilink")
  - 宴会菜
      - 东坡墨鱼
      - 清蒸江团
      - 圆笼糯香骨
      - [开水白菜](../Page/开水白菜.md "wikilink")

### 下河帮

又稱**重慶菜**，也稱**渝菜**或**渝派川菜**，以[重慶菜式為主](../Page/重慶.md "wikilink")，此派菜式大方粗犷，以花样翻新迅速、用料大胆、不拘泥于材料著称称，俗称江湖菜。较早的菜式起源于长江边拉纤的码头纤夫、平民家庭厨房或路边小店，并逐渐在市民中流传。其次，重庆川菜受到了民国时期和三线建设时期大量江浙移民的影响，对于[海鲜](../Page/海鲜.md "wikilink")、贝类、[梅菜](../Page/梅菜.md "wikilink")、[年糕等东部地区的食材使用较多](../Page/年糕.md "wikilink")，部分重庆菜口味相对四川川菜，带有[淮扬菜和](../Page/淮扬菜.md "wikilink")[上海菜浓油赤酱的特点](../Page/上海菜.md "wikilink")。

代表名菜

  - [麻辣火锅](../Page/麻辣火锅.md "wikilink")
  - 干菜炖烧系列（多以干豇豆为主）
  - 水煮系列
      - [水煮肉片](../Page/水煮肉片.md "wikilink")
      - [水煮鱼](../Page/水煮鱼.md "wikilink")
  - 辣子系列
      - [辣子鸡](../Page/辣子鸡.md "wikilink")
      - 辣子田螺
      - 香辣虾
      - 豆瓣虾
      - 香辣贝
      - 辣子肥肠
  - 干烧系列
      - 泉水鸡
      - 烧鸡公
      - 芋儿鸡
      - 啤酒鸭
  - 泡椒系列
      - 泡椒鸡杂
      - 泡椒鱿鱼
      - 泡椒兔
  - 干锅系列
      - 干锅排骨
      - 香辣虾
  - [酸菜鱼](../Page/酸菜鱼.md "wikilink")
  - [毛血旺](../Page/毛血旺.md "wikilink")
  - [口水鸡](../Page/口水鸡.md "wikilink")

### 小河帮

又稱**[盐帮菜](../Page/盐帮菜.md "wikilink")**或**自貢菜**，是以[自貢为中心的川南菜色的统称](../Page/自貢.md "wikilink")，还涵盖了宜宾、内江和泸州。自贡盐帮菜又分为盐商菜、盐工菜、会馆菜三大支系，以麻辣味、辛辣味、甜酸味为三大类别。自贡自古是重要的盐产地，中国古代盐业对应这巨大的商贸利益，盐业贸易导致了古代自贡经济的高度发达。故盐帮菜以精致、奢华、怪异、麻辣、鲜香、鲜嫩味浓为特色。除了自贡盐帮菜，其它川南城市也各具特色，例如内江有著名的球溪鲶鱼系列，宜宾则是冷锅鱼、兔火锅的发源地，小吃也非常有名，宜宾特色：[宜宾燃面](../Page/宜宾燃面.md "wikilink")、竹海名菜、李庄白肉、[猪儿粑](../Page/猪儿粑.md "wikilink")、泥溪芝麻糕、柏溪潮糕、兔火锅。泸州特色小吃：[白糕](../Page/白糕.md "wikilink")、[伦教糕](../Page/伦教糕.md "wikilink")、[黄粑](../Page/黄粑.md "wikilink")、[猪儿粑](../Page/猪儿粑.md "wikilink")、窖沙珍珠丸、两河桃片、合江烤鱼、姜氏卤菜一绝、老牌鸭子、朱氏杂酱。

特色菜：

  - [烧鹅掌](../Page/烧鹅掌.md "wikilink")
      -
        让数十只活鹅跑过预先铺上的烧红木炭,将鹅掌烫出燎泡后再砍下去骨烹调
  - [炒空心菜](../Page/炒空心菜.md "wikilink")
      -
        将空心菜预先灌入肉馅，然后炒制
  - [火鞭子牛肉](../Page/火鞭子牛肉.md "wikilink")
  - [冷吃兔](../Page/冷吃兔.md "wikilink")
  - [冷吃牛肉](../Page/冷吃牛肉.md "wikilink")
  - [凉拌鸡丝](../Page/凉拌鸡丝.md "wikilink")
  - [富顺豆花](../Page/富顺豆花.md "wikilink")
  - [跳水鱼](../Page/跳水鱼.md "wikilink")
  - [鲜锅兔](../Page/鲜锅兔.md "wikilink")
  - [牛佛烘肘](../Page/牛佛烘肘.md "wikilink")
  - [宜宾燃面](../Page/宜宾燃面.md "wikilink")
  - [猪儿粑](../Page/猪儿粑.md "wikilink")
  - [李庄白肉](../Page/李庄白肉.md "wikilink")

## 川菜菜式

### 代表菜

川菜的代表菜有：[宫保鸡丁](../Page/宫保鸡丁.md "wikilink")、干烧鱼、[回锅肉](../Page/回锅肉.md "wikilink")、[麻婆豆腐](../Page/麻婆豆腐.md "wikilink")、[家常豆腐](../Page/家常豆腐.md "wikilink")、[黄焖鸭](../Page/黄焖鸭.md "wikilink")、[夫妻肺片](../Page/夫妻肺片.md "wikilink")、[盐水鸭](../Page/盐水鸭.md "wikilink")、[锅巴肉片](../Page/锅巴肉片.md "wikilink")、[合川肉片](../Page/合川肉片.md "wikilink")、[江津肉片](../Page/江津肉片.md "wikilink")、[樟茶鸭子](../Page/樟茶鸭.md "wikilink")、干煸牛肉丝、怪味鸡块、灯影牛肉、[鱼香肉丝](../Page/鱼香肉丝.md "wikilink")、水煮牛肉，水煮肉片等。

### 不辣的川菜

川菜的不用辣椒烹饪手法有香糟、荔枝、糖醋、椒盐、曝腌、水晶、软烧、酱汁、烤、酱烧、红烧、白汁、清炖、清汤、奶汤、锅贴、芙蓉、香酥、豆渣、三鲜、清蒸、京酱、冰糖、蜜汁等等。不辣的名菜有[东坡肘子](../Page/东坡肘子.md "wikilink")、[黄焖鸭](../Page/黄焖鸭.md "wikilink")、[锅巴肉片](../Page/锅巴肉片.md "wikilink")、[合川肉片](../Page/合川肉片.md "wikilink")、[江津肉片](../Page/江津肉片.md "wikilink")、[黄烧鱼翅](../Page/黄烧鱼翅.md "wikilink")、[白汁鱼唇](../Page/白汁鱼唇.md "wikilink")、[清汤鱿鱼方](../Page/清汤鱿鱼方.md "wikilink")、[清蒸元鱼](../Page/清蒸元鱼.md "wikilink")、[烤乳猪](../Page/烤乳猪.md "wikilink")、[樟茶鸭子](../Page/樟茶鸭子.md "wikilink")、[红烧牛头](../Page/红烧牛头.md "wikilink")、[豆渣猪头](../Page/豆渣猪头.md "wikilink")、[坛子肉](../Page/坛子肉.md "wikilink")、[肝糕汤](../Page/肝糕汤.md "wikilink")、[口袋豆腐](../Page/口袋豆腐.md "wikilink")、[龙眼烧白](../Page/龙眼烧白.md "wikilink")、[平锅豆腐](../Page/平锅豆腐.md "wikilink")、[桃园香芋丝和水果汤圆等等](../Page/桃园香芋丝和水果汤圆.md "wikilink")，其中尤以[开水白菜最为淡雅](../Page/开水白菜.md "wikilink")。

### 小吃

四川各地小吃通常也被看作是川菜的组成部分。由于重庆地区小吃相对较少，除重庆麻辣小面外，川菜小吃主要以成都小吃为主。
主要有[担担面](../Page/担担面.md "wikilink")、[川北凉粉](../Page/川北凉粉.md "wikilink")、[麻辣小面](../Page/麻辣小面.md "wikilink")、[酸辣麵](../Page/酸辣麵.md "wikilink")、
[酸辣粉](../Page/酸辣粉.md "wikilink")、[叶儿粑](../Page/叶儿粑.md "wikilink")、[酸辣豆花](../Page/酸辣豆花.md "wikilink")、[红油抄手等以及用创始人姓氏命名的](../Page/红油抄手.md "wikilink")[赖汤圆](../Page/赖汤圆.md "wikilink")、[龙抄手](../Page/龙抄手.md "wikilink")、[钟水饺](../Page/钟水饺.md "wikilink")、[吴抄手等](../Page/吴抄手.md "wikilink")。

## 健康問題

在川菜中，高鈉問題屢見不鮮。川菜的普遍菜式因濃郁，厚重口味的需要，加入類似[郫縣豆瓣等鈉含量極高的調味料](../Page/郫县豆瓣.md "wikilink")。在一些質量不佳的四川泡菜中，更會含有[亞硝酸鹽](../Page/亚硝酸盐.md "wikilink")\[3\]，過多食用對身體健康會產生嚴重影響。

同時，川菜因口味需要，大部分菜式加入大量辣椒，對並無吃辣習慣之人士可能帶來腸胃問題。

## 图片集

<File:Fuqi> Fei Pian.jpg|夫妻肺片
[File:Mayishangshu.jpg|螞蟻上樹](File:Mayishangshu.jpg%7C螞蟻上樹)
<File:Mapo> tofu.JPG|thumb|麻婆豆腐
[File:Kung-pao-shanghai.jpg|宮保雞丁](File:Kung-pao-shanghai.jpg%7C宮保雞丁)
<File:Dan-dan> noodles, Shanghai.jpg|担担面 <File:Red> Oil Wontons
(红油抄手).jpg|红油抄手 <File:Hot> & Sour Noodle Soup (酸辣肉絲湯麵).jpg|酸辣肉絲麵

## 參考

<references />

[\*](../Category/川菜.md "wikilink")

1.  [宋](../Page/宋.md "wikilink")[孟元老](../Page/孟元老.md "wikilink")《[東京夢華錄](../Page/東京夢華錄.md "wikilink")》卷4《食店》記載[北宋](../Page/北宋.md "wikilink")[汴梁](../Page/汴梁.md "wikilink")「有川飯店，則有插肉面、大燠面、大小抹肉、淘煎燠肉、雜煎事件、生熟燒飯」
2.
3.