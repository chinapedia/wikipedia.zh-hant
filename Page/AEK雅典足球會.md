**AEK雅典足球會**（[希臘文](../Page/希臘文.md "wikilink")：****），是一間職業的[足球會](../Page/足球會.md "wikilink")，位於[希臘](../Page/希臘.md "wikilink")[雅典](../Page/雅典.md "wikilink")，現時是在[希臘足球超級聯賽作賽](../Page/希臘足球超級聯賽.md "wikilink")。

## 歷史

球會於1924年成立於雅典，由一群來自[土耳其](../Page/土耳其.md "wikilink")[伊斯坦布爾的移民所創立](../Page/伊斯坦布爾.md "wikilink")。成立後十多年，球會已贏得了11個頂級聯賽冠軍，以及13個希臘盃。並曾出席過[歐洲聯賽冠軍盃和](../Page/歐洲聯賽冠軍盃.md "wikilink")[歐洲足協盃等大型歐洲賽事](../Page/歐洲足協盃.md "wikilink")。

## 榮譽

  - [希臘頂級聯賽冠軍](../Page/希臘足球超級聯賽.md "wikilink")：
      - 1939, 1940, 1963, 1968, 1971, 1978, 1979, 1989, 1992, 1993,
        1994, 2018

<!-- end list -->

  - [希臘盃冠軍](../Page/希臘盃.md "wikilink")：
      - 1932, 1939, 1949, 1950, 1956, 1964, 1966, 1978, 1983, 1996,
        1997, 2000, 2002, 2011, 2016

## 外部連結

  - [官方网站](http://www.aekfc.gr/)

[A](../Category/希臘足球俱樂部.md "wikilink")
[Category:1924年建立的足球俱樂部](../Category/1924年建立的足球俱樂部.md "wikilink")