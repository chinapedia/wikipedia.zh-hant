**黄蜂**，又称为**胡蜂**、**馬蜂**，是分布广泛、种类繁多、飞翔迅速的[昆虫](../Page/昆虫.md "wikilink")，属[膜翅目](../Page/膜翅目.md "wikilink")，其中又分为許多科。雌蜂身上有一根长[螫针](../Page/螫针.md "wikilink")，在遇到攻击或不友善干扰时，會群起攻击，可以致人出現[过敏反应和毒性反应](../Page/全身型過敏性反應.md "wikilink")，嚴重者可導致[死亡](../Page/死亡.md "wikilink")。

## 身體構造

[Vespa_crabro_head_01.jpg](https://zh.wikipedia.org/wiki/File:Vespa_crabro_head_01.jpg "fig:Vespa_crabro_head_01.jpg")头部\]\]

[Waspstinger1658-2.jpg](https://zh.wikipedia.org/wiki/File:Waspstinger1658-2.jpg "fig:Waspstinger1658-2.jpg")

黄蜂的[卵常呈椭圆形](../Page/卵.md "wikilink")，白色，光滑，在每个巢室中有1枚，其基部有一丝质柄固着，直至孵出[幼虫](../Page/幼虫.md "wikilink")。
幼虫梭形，白色，无足，体分13节。成虫后头、胸、腹分明，主要器官均明显可见。在幼虫消化道的中肠端部，由围食膜形成一个封闭囊，不与排泄孔相通。排泄物贮在此囊中，于体内呈游离状。
[蛹为离蛹](../Page/蛹.md "wikilink")，黄白色，颜色随龄期而加深，体内已不再有排泄物与囊，等待[羽化](../Page/羽化.md "wikilink")。
羽化成熟后就破茧而出，变成完整的黄蜂了。
黃蜂成蟲時期的身體外觀亦具有[昆蟲的標準特徵](../Page/昆蟲.md "wikilink")，包括頭部、胸部、腹部、三對腳和一對觸角；同时，牠的單眼、複眼與翅膀，也是多數昆蟲共有的特徵；此外，腹部尾端內隱藏了一支退化的輸卵管，即有毒蜂針。體長是14-18mm。

## 发育变态

黃蜂属于[完全變態的动物](../Page/完全變態.md "wikilink")，一生經歷四個階段：卵、幼蟲、蛹、成蟲，每個階段的身體外觀都不同。由卵孵化後的幼蟲尾部仍附著於巢穴底，即使窩巢是倒吊的也不至於摔落，並且由工蜂負責餵食，待發育成熟時身軀由晶瑩剔透逐漸轉為明黃色，接著在穴口封上一層薄繭並化成蛹，等到羽化為成蟲後就破繭而出，從卵到羽化只需要二到三星期的時間。幼蟲階段是以其他小蟲為食，尤其是毛毛蟲。

## 蜂巢

[螞蜂亞科和](../Page/螞蜂亞科.md "wikilink")[胡蜂亞科的胡蜂所建立的](../Page/胡蜂亞科.md "wikilink")[蜂巢是用](../Page/蜂巢.md "wikilink")[木漿所造成的](../Page/木漿.md "wikilink")，而非[蜂蠟](../Page/蜂蠟.md "wikilink")。這一點與[蜜蜂及其他胡蜂有所不同](../Page/蜜蜂.md "wikilink")。

<File:Wespennest> 005.JPG|马蜂巢
[File:Wwabe.jpg|马蜂巢内部结构](File:Wwabe.jpg%7C马蜂巢内部结构)

## 分类

  - [榕小蜂科](../Page/榕小蜂科.md "wikilink")（Agaonidae）
  - [小蜂科](../Page/小蜂科.md "wikilink")（Chalcididae）
  - [青蜂科](../Page/青蜂科.md "wikilink")（Chrysididae）
  - [方头泥蜂科](../Page/方头泥蜂科.md "wikilink")（Crabronidae）
  - [瘿蜂科](../Page/瘿蜂科.md "wikilink")（Cynipidae）
  - [跳小蜂科](../Page/跳小蜂科.md "wikilink")（Encyrtidae）
  - [寡节小蜂科](../Page/寡节小蜂科.md "wikilink")（Eulophidae）
  - [旋小蜂科](../Page/旋小蜂科.md "wikilink")（Eupelmidae）
  - [姬蜂科](../Page/姬蜂科.md "wikilink")（Ichneumonidae）
  - [蚁蜂科](../Page/蚁蜂科.md "wikilink")（Mutillidae）
  - [柄翅卵蜂科](../Page/柄翅卵蜂科.md "wikilink")（Mymaridae）
  - [蛛蜂科](../Page/蛛蜂科.md "wikilink")（Pompilidae）
  - [金小蜂科](../Page/金小蜂科.md "wikilink")（Pteromalidae）
  - [缘腹卵蜂科](../Page/缘腹卵蜂科.md "wikilink")（Scelionidae）
  - [土蜂科](../Page/土蜂科.md "wikilink")（Scoliidae）
  - [泥蜂科](../Page/泥蜂科.md "wikilink")（Sphecidae）
  - [臀钩土蜂科](../Page/臀钩土蜂科.md "wikilink")（Tiphiidae）
  - [长尾小蜂科](../Page/长尾小蜂科.md "wikilink")（Torymidae）
  - [纹翅卵蜂科](../Page/纹翅卵蜂科.md "wikilink")（Trichogrammatidae）
  - [胡蜂科](../Page/胡蜂科.md "wikilink")（Vespidae） -
    [虎頭蜂](../Page/虎頭蜂.md "wikilink")

[de:Echte Wespen](../Page/de:Echte_Wespen.md "wikilink") [frr:Echt
wespen](../Page/frr:Echt_wespen.md "wikilink")
[hr:Ose](../Page/hr:Ose.md "wikilink")
[nv:Tsísʼnáłtsooí](../Page/nv:Tsísʼnáłtsooí.md "wikilink")
[pl:Osa](../Page/pl:Osa.md "wikilink") [sr:Оса
(инсект)](../Page/sr:Оса_\(инсект\).md "wikilink")

[Category:膜翅目](../Category/膜翅目.md "wikilink")