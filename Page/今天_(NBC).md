《**今天**》（），也被称为《**今日秀**》（），是一档[美国的](../Page/美国.md "wikilink")[晨间新闻和](../Page/晨间新闻.md "wikilink")[脱口秀节目](../Page/脱口秀.md "wikilink")。每个工作日的晨间在[美国全国广播公司](../Page/美国全国广播公司.md "wikilink")（[NBC](../Page/NBC.md "wikilink")）播出，于1952年1月14日首播。《今天》是首档早间资讯娱乐电视节目，日后在美国及全世界又出现了许多同类节目。该节目也是美国播出时间第三长的电视节目，仅次于《[与媒体见面](../Page/与媒体见面.md "wikilink")（**）》（1947年11月6日开播）和《[CBS晚间新闻](../Page/CBS晚间新闻.md "wikilink")（**）》（1948年8月14日开播）。

起初《今天》是一档逢周一至周五播出的两个小时的节目，1987年开始在周日播出（现时周日版节目时长为1小时），1992年起又开设周六版。2000年起其平日版播出延长至三小时，2007年又开播第四小时的节目。

《今天》节目的霸主地位长期以来几乎令其他电视网无法打破，直到20世纪80年代末被[美国广播公司](../Page/美国广播公司.md "wikilink")（[ABC](../Page/ABC.md "wikilink")）的《[早安美国](../Page/早安美国.md "wikilink")（**）》所超越。经过一段时期的重振，自1995年12月11日至今，根据尼尔森收视率调查，《今天》一直是美国每周收视率最高的早间新闻和脱口秀节目。

## 现时主持人

《今天》节目每逢周一至周五前两个小时的节目由[马特·劳尔](../Page/马特·劳尔.md "wikilink")（）、[梅莉蒂丝·维埃拉](../Page/梅莉蒂丝·维埃拉.md "wikilink")（），以及气象主播[阿尔·罗克](../Page/阿尔·罗克.md "wikilink")（）、新闻主播[安·克莉](../Page/安·克莉.md "wikilink")（）和记者[娜特莉·莫拉莱斯](../Page/娜特莉·莫拉莱斯.md "wikilink")（）共同主持。[阿尔·罗克](../Page/阿尔·罗克.md "wikilink")、[安·克莉和](../Page/安·克莉.md "wikilink")[娜特莉·莫拉莱斯还将联合主持第三小时的节目](../Page/娜特莉·莫拉莱斯.md "wikilink")。第四小时的节目则由[欧塔·卡比](../Page/欧塔·卡比.md "wikilink")（）和[凯瑟·李·基弗德](../Page/凯瑟·李·基弗德.md "wikilink")（）联合主持。《今天（周末版）》由男主持[莱斯特·赫特](../Page/莱斯特·赫特.md "wikilink")（）、女主持[艾米·卢贝克](../Page/艾米·卢贝克.md "wikilink")（）（周六）和女主持[简娜·沃尔菲](../Page/简娜·沃尔菲.md "wikilink")（）（周日）共同主持。

[安·克莉](../Page/安·克莉.md "wikilink")（）、[娜特莉·莫拉莱斯](../Page/娜特莉·莫拉莱斯.md "wikilink")（）、[欧塔·卡比](../Page/欧塔·卡比.md "wikilink")（）或者[艾米·卢贝克](../Page/艾米·卢贝克.md "wikilink")（）通常会充当[梅莉蒂丝·维埃拉](../Page/梅莉蒂丝·维埃拉.md "wikilink")（）缺席时的代班主播（）。[马特·劳尔](../Page/马特·劳尔.md "wikilink")（）通常的代班主播是[安·克莉](../Page/安·克莉.md "wikilink")（）、[莱斯特·赫特](../Page/莱斯特·赫特.md "wikilink")（）或《[与媒体见面](../Page/与媒体见面.md "wikilink")（*[Meet
the
Press](../Page/Meet_the_Press.md "wikilink")*）》的主持人（）[大卫·格雷戈里](../Page/大卫·格雷戈里.md "wikilink")（）。

其他的国内记者包括（）、[简娜·沃尔菲](../Page/link-en.md "wikilink")（）、驻[国会山](../Page/国会山.md "wikilink")（）记者[柯琳·欧丹妮尔](../Page/link-en.md "wikilink")（）、[鲍勃·多特森](../Page/link-en.md "wikilink")（）、以及[蒂基·巴伯](../Page/link-en.md "wikilink")（）。是节目的娱乐评论员（），[彼得·格林伯格](../Page/link-en.md "wikilink")（）担任旅游编辑（）。《[金钱杂志](../Page/金钱杂志.md "wikilink")（）》的编辑为节目提供每周金融市场信息。

女主持（）出任游戏节目《[谁想成为百万富翁](../Page/谁想成为百万富翁.md "wikilink")（**）》主持人的合约禁止她出现在《今天》每天前两个小时节目以外的时间段。而男主持（）每天也介绍第三小时的节目内容。

## 节目历史

《今天》节目于1952年1月14日美国东部时间早上7点首次播出。它是日后成为[美国全国广播公司](../Page/美国全国广播公司.md "wikilink")（[NBC](../Page/NBC.md "wikilink")）副总裁的[帕特·维弗](../Page/帕特·维弗.md "wikilink")（）的创意。[维弗在](../Page/维弗.md "wikilink")1953至1955年期间出任公司总裁（其间《今天》的深夜姊妹节目《[今夜秀](../Page/今夜秀.md "wikilink")**》开播），几年后他又担任了董事会主席。[帕特·维弗是演员](../Page/帕特·维弗.md "wikilink")的父亲。

当《今天》节目开播之时，它是第一档晨间新闻及脱口秀节目。最初的主持人是[戴弗·加洛维](../Page/戴弗·加洛维.md "wikilink")（）。节目中包含了国内新闻头条、与新闻当事人的深度访谈、生活时尚专题、其他各类软新闻、小环节和噱头（如早年在节目中出现了[大猩猩](../Page/大猩猩.md "wikilink")[J.
Fred
Muggs充当吉祥物](../Page/J._Fred_Muggs.md "wikilink")），以及地方台的最新消息。而《今天》节目的成功也引发了其他一些类似的节目，诸如[美国广播公司](../Page/美国广播公司.md "wikilink")（[ABC](../Page/ABC.md "wikilink")）的《[早安美国](../Page/早安美国.md "wikilink")（**）》和美国[哥伦比亚广播公司](../Page/哥伦比亚广播公司.md "wikilink")（[CBS](../Page/CBS.md "wikilink")）的《[早间秀](../Page/早间秀.md "wikilink")（**）》。

[[Dave Garroway](../Page/Dave_Garroway.md "wikilink")
第一集《今天》节目的开场|200px|thumb|left](https://zh.wikipedia.org/wiki/File:First_Episode_of_Today.PNG "fig:Dave Garroway 第一集《今天》节目的开场|200px|thumb|left")其他国家也模仿这一节目形态，最明显的诸如[英国广播公司](../Page/英国广播公司.md "wikilink")（[BBC](../Page/BBC.md "wikilink")）的《[BBC早餐](../Page/BBC早餐.md "wikilink")（**）》、[英国独立电视台](../Page/英国独立电视台.md "wikilink")（[ITV](../Page/ITV.md "wikilink")）的《[早安英国](../Page/早安英国.md "wikilink")（**）》、[加拿大](../Page/加拿大.md "wikilink")[CTV电视网的](../Page/CTV电视网.md "wikilink")《[加拿大早晨](../Page/加拿大早晨.md "wikilink")（**）》以及[菲律宾](../Page/菲律宾.md "wikilink")[ABS-CBN的](../Page/ABS-CBN.md "wikilink")**(Mornings
Are Beautiful)。

最初《今天》节目只在[美国东部时区和](../Page/美国东部时区.md "wikilink")[中部时区直播](../Page/中部时区.md "wikilink")，每天早上播出三个小时，但是每个时区只能看到两个小时的节目。之后，《今天》在每周一早上直播五个小时，但是在美国四个时区内只能连续看到两个小时节目。1958年起，《今天》通过[延时录播的方式在不同的时区里播出](../Page/延时录播.md "wikilink")。在此之后的很多年里，除[阿拉斯加](../Page/阿拉斯加.md "wikilink")、[夏威夷和](../Page/夏威夷.md "wikilink")[美属维京群岛外](../Page/美属维京群岛.md "wikilink")，《今天》节目在每个时区的早上7点至9点播出两个小时，直到2000年10月2日[NBC方面将节目延长至](../Page/NBC.md "wikilink")3个小时。2007年9月10日又增加了第四个小时的节目。在一些地区（例如位于[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[波士顿的](../Page/波士顿.md "wikilink")[WHDH-TV](../Page/WHDH-TV.md "wikilink")）《今天》第三和第四小时的节目将继续延时播出。

在每天节目的头三小时里，节目每逢:25和:55分都提供一个五分钟的窗口给地方附属台，用以插播地方新闻。

### 演播室

[GE_Building_by_David_Shankbone.JPG](https://zh.wikipedia.org/wiki/File:GE_Building_by_David_Shankbone.JPG "fig:GE_Building_by_David_Shankbone.JPG")
[Today_torino.jpg](https://zh.wikipedia.org/wiki/File:Today_torino.jpg "fig:Today_torino.jpg")
[Today_beijing.JPG](https://zh.wikipedia.org/wiki/File:Today_beijing.JPG "fig:Today_beijing.JPG")

《今天》节目最早在位于[美国](../Page/美国.md "wikilink")[纽约](../Page/纽约.md "wikilink")49街的[美国无线电公司](../Page/美国无线电公司.md "wikilink")（[RCA](../Page/RCA.md "wikilink")）展览厅里播出，距现在的节目演播室仅一个街区之隔，现在那里属于[克里斯蒂拍卖行](../Page/克里斯蒂拍卖行.md "wikilink")。节目的第一个演播室中设置了一个新闻工作间，也就是当时的主持人[加洛维所称的](../Page/加洛维.md "wikilink")“世界的神经中枢（the
nerve center of the
world）”，前台与后台也几乎不存在障碍，[加洛维和播出团队经常直接穿过演播室](../Page/加洛维.md "wikilink")。摄影团队和技术人员的身影以及他们与[加洛维交谈的画外音也时常直接出现在节目中](../Page/加洛维.md "wikilink")。渐渐地，机器和人员被安置在幕后整合新闻和天气报告，而新闻工作间也于1955年被撤走。1958年，节目移至与原演播室一街之隔的[美国无线电公司大厦里的](../Page/美国无线电公司.md "wikilink")3K演播室录制，直到20世纪60年代初。

1962年7月9日，节目搬到一个街边的演播室里，那里后来成为了[the Florida
Showcase](../Page/the_Florida_Showcase.md "wikilink")。1965年9月13日，《今天》节目回到[美国无线电公司大厦内录制](../Page/美国无线电公司.md "wikilink")。那时美国各大电视网的新闻节目都转为全彩色播出，[NBC不能平均分配其彩色摄影机给位于](../Page/NBC.md "wikilink")[the
Florida Showcase的演播室](../Page/the_Florida_Showcase.md "wikilink")。

在接下来的20年里，节目使用了位于[NBC总部三层](../Page/NBC.md "wikilink")、六层、八层的一系列演播室。其中具有代表性的包括20世纪70年代使用的3K演播室、70年代末80年代初使用的8G演播室（毗邻《[周六夜现场](../Page/周六夜现场.md "wikilink")*[Saturday
Night
Live](../Page/Saturday_Night_Live.md "wikilink")*》的8H演播室）以及在1983年到1994年所使用的3B演播室。1994年6月，《今天》节目迁至现时使用的街边演播室（鱼缸式演播室），与节目20世纪50年代的草创时期形成了某种联系。

自20世纪90年代新演播室启用之后，各大广播网和有线新闻网的早间节目都移到了街边，包括《今天》所在的[洛克菲勒中心的两个邻居](../Page/洛克菲勒中心.md "wikilink")——[福克斯新闻频道](../Page/福克斯新闻频道.md "wikilink")（[Fox
News
Channel](../Page/Fox_News_Channel.md "wikilink")）的《[福克斯和朋友们](../Page/福克斯和朋友们.md "wikilink")（*[Fox
&
Friends](../Page/Fox_&_Friends.md "wikilink")*）》（位于[美洲大道](../Page/美洲大道.md "wikilink")，即[曼哈顿](../Page/曼哈顿.md "wikilink")[第六大道](../Page/第六大道.md "wikilink")）以及美国[有线电视新闻网](../Page/有线电视新闻网.md "wikilink")（[CNN](../Page/CNN.md "wikilink")）的《[美国早晨](../Page/美国早晨.md "wikilink")（*[American
Morning](../Page/American_Morning.md "wikilink")*）》（2005年夏天，[CNN逆势放弃了街边演播室转往位于](../Page/CNN.md "wikilink")[Columbus
Circle的](../Page/Columbus_Circle.md "wikilink")[时代华纳中心楼上](../Page/时代华纳中心.md "wikilink")）。另外[美国广播公司](../Page/美国广播公司.md "wikilink")（[ABC](../Page/ABC.md "wikilink")）的《[早安美国](../Page/早安美国.md "wikilink")（*[Good
Morning
America](../Page/Good_Morning_America.md "wikilink")*）》也从其[时报广场演播室](../Page/时报广场演播室.md "wikilink")（[Times
Square Studios](../Page/Times_Square_Studios.md "wikilink")）播出。

2006年，1A演播室进行了重大改造，为[1080i](../Page/1080i.md "wikilink")[高清广播作准备](../Page/高清广播.md "wikilink")。2006年夏季，在[凯蒂·库瑞克](../Page/凯蒂·库瑞克.md "wikilink")（[Katie
Couric](../Page/Katie_Couric.md "wikilink")）离开以及新演播室正在装修之际，节目移至位于[洛克菲勒广场的一个临时户外演播室播出](../Page/洛克菲勒广场.md "wikilink")，同样的户外演播室NBC也在奥运会期间在[希腊](../Page/希腊.md "wikilink")[雅典](../Page/雅典.md "wikilink")、[意大利](../Page/意大利.md "wikilink")[都灵](../Page/都灵.md "wikilink")，以及2008年的[中国](../Page/中国.md "wikilink")[北京使用过](../Page/北京.md "wikilink")。\[1\]
在2006年8月28日这一周内，由于[MTV电视网为举办](../Page/MTV.md "wikilink")2006年“MTV音乐录影带大奖”将它们的户外演播室移往它们的红地毯展台，所以《今天》节目又移往1A演播室外的一个临时地点播出。此外，每逢恶劣天气时，《今天》节目也会在《日界线（Dateline
NBC）》节目演播室内设立虚拟演播室。同时，它们也使用在洛克菲勒中心30号（30 Rock）的临时户外演播室。

2006年9月13日，《今天》节目启用全新演播室。这个新演播室在一层分为五个不同的区域，包括专访区（the interview
area）、聊天区（the couch area）、新闻播报台（the news
desk）、用于表演/访问/其他活动的区域（the
performance/interview/extra space area），以及节目开始时主播使用的主场景（the home
base）。一个巨大的[松下](../Page/松下.md "wikilink")103英寸[等离子显示器通常被用于动态影像背景](../Page/等离子.md "wikilink")。节目使用的厨房设在主演播室的楼上。主场景的蓝色背景板是可以上下伸缩的，在节目开始时它会升起，而在7：30之后将会落下，主场景外的观众可通过玻璃看到演播室内的情况。

2013年9月16日，《今天》啟用了全新片頭和全新1A演播室（最初設定亮相9月9日，但被推遲一個星期完成最終的設計細節）。在“home
base” 有可旋轉360度的平台，因此，使攝影機根據半小時來改變拍攝方向。“沙發區” 添加了新的沙發和背景（主播坐下來討論的話題）。
“Orange
Room”，也被加入到演播室1A，其中包含六個屏幕連接到一個6'×16'的屏幕為時尚/專題區，顯示Twitter的評論或熱門話題;
卡森達利 （Carson Daly）是Orange Room的主播。
新的片頭（在11月，進一步整合早晨新聞節目的品牌與《今天》修訂後的版本首次亮相Early
Today，）孔雀動畫從左上角移動右下角。圓形拱門的數目由五個縮減到三個。顏色切換由一般深淺不一的紅色、橙色和黃色描繪日出，改為全橙色。

2015年5月12日，更換片頭，但演播室背景及主題音樂不變。

## 节目人员

### 主持人

《今天》节目的主播起初被叫做“传播者（Communicators）”。在创始人[帕特·维弗](../Page/帕特·维弗.md "wikilink")（[Pat
Weaver](../Page/Pat_Weaver.md "wikilink")）的设想中，这个人的职责将超越传统意义上静静坐着的新闻主播。传播者需要采访、报道、提问对话并将整个节目组合成一个整体。首任主持[戴弗·加洛维](../Page/戴弗·加洛维.md "wikilink")（[Dave
Garroway](../Page/Dave_Garroway.md "wikilink")）以及他的继任者们都遵循这种模式，几乎没有变化。如今，该节目主持人的作用与以往大致相同，即每天与记者、新闻当事人及生活时尚专家交谈，开始和结束每半个小时的节目，参与一些特别版块（诸如厨艺与时尚环节），以及从不同地点主持特别节目。虽然“传播者”这一头衔早已不再使用，但是节目主持人的工作基本上还是相同的。《今天》节目历任的主持人（或称主播）包括：

  - [戴弗·加洛维](../Page/戴弗·加洛维.md "wikilink")（[Dave
    Garroway](../Page/Dave_Garroway.md "wikilink")） (1952–1961)
  - [约翰·钱塞勒](../Page/约翰·钱塞勒.md "wikilink")（[John
    Chancellor](../Page/John_Chancellor.md "wikilink")） (1961–1962)
  - [休·唐斯](../Page/休·唐斯.md "wikilink")（[Hugh
    Downs](../Page/Hugh_Downs.md "wikilink")） (1962–1966)
  - [休·唐斯](../Page/休·唐斯.md "wikilink")（Hugh Downs） 和
    [芭芭拉·沃尔特斯](../Page/芭芭拉·沃尔特斯.md "wikilink")（[Barbara
    Walters](../Page/Barbara_Walters.md "wikilink")） (1966–1971)\[2\]
  - [芭芭拉·沃尔特斯](../Page/芭芭拉·沃尔特斯.md "wikilink")（Barbara Walters） 和
    [弗兰克·麦克吉](../Page/弗兰克·麦克吉.md "wikilink")（[Frank
    McGee](../Page/Frank_McGee_\(journalism\).md "wikilink")）
    (1971–1974)
  - [芭芭拉·沃尔特斯](../Page/芭芭拉·沃尔特斯.md "wikilink")（Barbara Walters） 和
    [吉姆·哈兹](../Page/吉姆·哈兹.md "wikilink")（[Jim
    Hartz](../Page/Jim_Hartz.md "wikilink")） (1974–1976)
  - [汤姆·布罗考](../Page/汤姆·布罗考.md "wikilink")（[Tom
    Brokaw](../Page/Tom_Brokaw.md "wikilink")） 和
    [简·波利](../Page/简·波利.md "wikilink")（[Jane
    Pauley](../Page/Jane_Pauley.md "wikilink")） (1976–1982)
  - [简·波利](../Page/简·波利.md "wikilink")（Jane Pauley） 和
    [布赖恩特·冈贝尔](../Page/布赖恩特·冈贝尔.md "wikilink")（[Bryant
    Gumbel](../Page/Bryant_Gumbel.md "wikilink")） (1982–1989)
  - [布赖恩特·冈贝尔](../Page/布赖恩特·冈贝尔.md "wikilink")（Bryant Gumbel） 和
    [黛博拉·诺维尔](../Page/黛博拉·诺维尔.md "wikilink")（[Deborah
    Norville](../Page/Deborah_Norville.md "wikilink")） (1990–1991)
  - [布赖恩特·冈贝尔](../Page/布赖恩特·冈贝尔.md "wikilink")（Bryant Gumbel） 和
    [凯蒂·库瑞克](../Page/凯蒂·库瑞克.md "wikilink")（[Katie
    Couric](../Page/Katie_Couric.md "wikilink")） (1991–1997)
  - [凯蒂·库瑞克](../Page/凯蒂·库瑞克.md "wikilink")（Katie Couric） 和
    [马特·劳尔](../Page/马特·劳尔.md "wikilink")（[Matt
    Lauer](../Page/Matt_Lauer.md "wikilink")） (1997–2006)
  - [马特·劳尔](../Page/马特·劳尔.md "wikilink")（Matt Lauer） 和
    [梅莉蒂丝·维埃拉](../Page/梅莉蒂丝·维埃拉.md "wikilink")（[Meredith
    Vieira](../Page/Meredith_Vieira.md "wikilink")） (2006 至今)

芭芭拉·沃尔特斯（Barbara Walters）在1966年成为节目的联合主持人，但是直到1974年才成为节目正式的联合主持人。 \[3\]

### 新闻主播

从节目的草创之处，新闻主播就被设计成具有在节目中提供最新消息的功能。在这方面，至少有一个人一直坐在那里，他的工作就是编写和提供新闻播报。在1952年，这个人开始被称为《今天》的“新闻编辑”（"news
editor"）或“新闻官”（"news
chief"）（非正式称谓）。在现代用语中，“新闻播报员”（"newsreader"）或“新闻主播”（"news
anchor"）这两个称谓成为首选。原先在两个小时的节目中，总共有四次新闻播报，每半小时就会有一次。现在在三个小时节目中总共只有三次新闻播报，在每个小时的一开始播出。一些主播（包括Jim
Fleming、Lew Wood、Floyd Kalber和John
Palmer）在加入节目之前已经是很成熟的新闻记者。其他的一些主播（例如[Ann
Curry](../Page/Ann_Curry.md "wikilink")）是为了增加他们的新闻敏感度而被安排在这一位置上。有时当大事发生时，他们也会离开播报台直接前往现场报道。《今天》节目的新闻主播包括：

  - Jim Fleming (1952–1953)
  - [Frank Blair](../Page/Frank_Blair_\(journalist\).md "wikilink")
    (1953–1975)
  - [Lew Wood](../Page/Lew_Wood.md "wikilink") (1975–1976)
  - [Floyd Kalber](../Page/Floyd_Kalber.md "wikilink") (1976–1979)
  - [Tony Guida](../Page/Tony_Guida.md "wikilink") (1979)
  - 空缺 (1979–1981； 这一时期由主持人汤姆·布罗考和简·波利播报头条新闻)
  - [Chris Wallace](../Page/Chris_Wallace_\(journalist\).md "wikilink")
    (1982)
  - [John Palmer](../Page/John_Palmer_\(TV_journalist\).md "wikilink")
    (1982–1989)
  - [Deborah Norville](../Page/Deborah_Norville.md "wikilink") (1989)
  - [Faith Daniels](../Page/Faith_Daniels.md "wikilink") (1990–1992)
  - [Margaret Larson](../Page/Margaret_Larson.md "wikilink") (1992–1994)
  - [Matt Lauer](../Page/Matt_Lauer.md "wikilink") (1994–1997)
  - [Ann Curry](../Page/Ann_Curry.md "wikilink") (1997年至今)

### 气象播报员

在节目最初的25年里，天气报告一直是由节目主持人或新闻播报员负责播报的。最初，主持人[戴弗·加洛维会基于位于](../Page/戴弗·加洛维.md "wikilink")[华盛顿哥伦比亚特区的](../Page/华盛顿哥伦比亚特区.md "wikilink")[美国国家气象局清晨提供的信息](../Page/美国国家气象局.md "wikilink")，将当天云系的锋面以及各地区的降水量画在一块绘有[美国地图的黑板上](../Page/美国.md "wikilink")。后任主持人[约翰·钱塞勒以及](../Page/约翰·钱塞勒.md "wikilink")[休·唐斯终止了黑板气象云图的概念](../Page/休·唐斯.md "wikilink")，取而代之的是在一块静态的气象云图上播报天气摘要。当节目在1965年转为全彩色播出之后，气象云图开始出现新闻主播[弗莱克·布莱尔](../Page/弗莱克·布莱尔.md "wikilink")（[Frank
Blair](../Page/Frank_Blair.md "wikilink")）身后的屏幕上，布莱尔会在新闻播报之后提供天气预测。如今，在《今天》三个小时的节目里，每半个小时都将播报一次天气情况。

加洛维、布莱尔以及其他的气象播报员在气象学方面都没有实际工作经验或学历。《今天》节目的气象播报员包括：

  - [Bob Ryan](../Page/Bob_Ryan_\(meteorologist\).md "wikilink")
    (1978–1980)
  - [Willard Scott](../Page/Willard_Scott.md "wikilink") (1980–1996)
  - [Al Roker](../Page/Al_Roker.md "wikilink") (1996年至今)

[NBC在全国天气预报之后将提供其地方附属台一个](../Page/NBC.md "wikilink")30秒的窗口用以在节目中插播地方天气预报。[阿尔·罗克](../Page/阿尔·罗克.md "wikilink")（[Al
Roker](../Page/Al_Roker.md "wikilink")）在地方天气报告前会说这样的串联语：*"That's what's
going on around the country, here's what's happening in your neck of the
woods."*（如果附属台没有地方天气预报插入，那么总台将会播放全国简要的温度报告。）

因为滑稽的服装、道具和表现闻名、现处于半退休状态的[Willard
Scott](../Page/Willard_Scott.md "wikilink")\[4\]
仍然会以[阿尔·罗克的代班主持的身份在节目中偶尔出现](../Page/阿尔·罗克.md "wikilink")，同时Scott还会延续他向百岁老人祝贺生日的传统。Scott通常的串联词是*"Here's
what's happening in your world, even as we speak."*

### 经常性参与者

“参与者（panelist）”的工作并没有界定。参与者的工作可以从对谈到从演播室或外景报道一些话题。《今天》节目的经常性参与者包括：

  - [Jack Lescoulie](../Page/Jack_Lescoulie.md "wikilink") (1952–1965)
  - [Edwin Newman](../Page/Edwin_Newman.md "wikilink") (1952–1984)
  - [Barbara Walters](../Page/Barbara_Walters.md "wikilink") (1962–1974
    其工作实际上是联合主持) (1974年正式成为节目的联合主持)
  - [Judith Crist](../Page/Judith_Crist.md "wikilink") (1964–1973)
  - [Joe Garagiola](../Page/Joe_Garagiola,_Sr..md "wikilink")
    (1967–1973, 1990–1992)
  - [Gene Shalit](../Page/Gene_Shalit.md "wikilink") (1973年至今)

### 《今天》女郎

从1952年到1964年，一些著名的女性节目参与者被称为“《今天》女郎”，她们通常是艺人。一般“《今天》女郎”会讨论时尚和生活话题，她们会报道一些软性新闻话题或者与主持人[戴弗·加洛维展开唇枪舌剑](../Page/戴弗·加洛维.md "wikilink")。Estelle
Parsons被认为是第一个担任这一工作的人，虽然当时她的头衔是节目的“女性话题编辑（Women's
Editor）”。在她1955年离开之后，“《今天》女郎”的名称被采用。最后一位“《今天》女郎”是[芭芭拉·沃尔特斯](../Page/芭芭拉·沃尔特斯.md "wikilink")，1966年开始她实际上已经与[休·唐斯合作成为节目的联合主持](../Page/休·唐斯.md "wikilink")，而那时也没有人能够取代她的位置。

  - [Estelle Parsons](../Page/Estelle_Parsons.md "wikilink") (官方头衔:
    "Women's Editor") (1952–1955)
  - [Lee Meriwether](../Page/Lee_Meriwether.md "wikilink") (1955–1956)
  - [Helen O'Connell](../Page/Helen_O'Connell.md "wikilink") (1956–1958)
  - [Betsy Palmer](../Page/Betsy_Palmer.md "wikilink") (1958)
  - [Florence Henderson](../Page/Florence_Henderson.md "wikilink")
    (1959–1960)
  - [Beryl Pfizer](../Page/Beryl_Pfizer.md "wikilink") (1960–1961)
  - [Robbin Bain](../Page/Robbin_Bain.md "wikilink") (1961)
  - [Louise King](../Page/Louise_King.md "wikilink") (1962)
  - [Pat Fontaine](../Page/Pat_Fontaine.md "wikilink") (1963–1964)
  - [Maureen O'Sullivan](../Page/Maureen_O'Sullivan.md "wikilink")
    (1964)
  - [Barbara Walters](../Page/Barbara_Walters.md "wikilink") (1964)

## 争议与变迁

### “备忘门”事件

1989年，主持人[布赖恩特·冈贝尔向](../Page/布赖恩特·冈贝尔.md "wikilink")《今天》的执行制片人Marty
Ryan写了一份备忘录，其中批评《今天》节目的其他主持人，然而这份备忘录却被泄露给新闻界。在这份备忘录中，[冈贝尔评论气象环节主持人](../Page/布赖恩特·冈贝尔.md "wikilink")[Willard
Scott](../Page/Willard_Scott.md "wikilink")"holds the show hostage to
his assortment of whims, wishes, birthdays and bad taste...This guy is
killing us and no one's even trying to rein him in"。他评论主持人[Gene
Shalit的影评](../Page/Gene_Shalit.md "wikilink")"are often late and
his interviews aren't very good."\[5\]

[冈贝尔对Scott的评论招致强烈反弹](../Page/布赖恩特·冈贝尔.md "wikilink")。\[6\]

### 从简·波利到诺维尔

1989年，黛博拉·诺维尔取代John Palmer成为《今天》节目的新闻主播，而John
Palmer则承担了诺弗莉之前在《NBC日出新闻》的工作。诺维尔同时还成为[汤姆·布罗考在](../Page/汤姆·布罗考.md "wikilink")《[NBC晚间新闻](../Page/NBC晚间新闻.md "wikilink")》的代班主播。诺维尔成为《今天》新闻主播后不久，她又成为了《今天》节目非正式的第三位主持人。鉴于之前Palmer从一个在主持台旁边的播报台上播报新闻，诺弗莉在每天节目开始和结束时，坐在节目主持人的身边播报。此前，娱乐专栏和媒体观察家们预测NBC将会调走节目女主持简·波利，诺维尔将取而代之以改善该节目收视率下降的现状。

1989年末，NBC宣布担任《今天》节目主持长达13年的简·波利将在年底离开该节目。正如预期的，NBC宣布黛布罗·诺维尔将取代波利成为新的联合主持。这一消息在节目中宣布后，诺维尔在节目中与波利拥抱，而NBC方面也希望因诺维尔加盟而产生的负面新闻能就此消除。但事与愿违。在波利离开的消息宣布之前，许多评论都针对诺维尔的年轻貌美，并指责她是“第三者”和“（《今天》）家庭破坏者”，认为是她导致了[冈贝尔与](../Page/布赖恩特·冈贝尔.md "wikilink")[波利的电视组合最终破裂](../Page/简·波利.md "wikilink")。

在波利宣布辞职后不仅负面新闻加剧，而且NBC官员要求诺弗莉三缄其口，使她无法挽回有关她精心策划自己在《今天》异军突起的不实报道对自己声誉的影响。1990年1月，没有了简·波利，[布赖恩特·冈贝尔与](../Page/布赖恩特·冈贝尔.md "wikilink")[黛博拉·诺维尔的全新主持组合带来了灾难性的后果](../Page/黛博拉·诺维尔.md "wikilink")。节目收视率开始暴跌。批评者认为[冈贝尔与诺维尔之间缺乏互动](../Page/布赖恩特·冈贝尔.md "wikilink")，许多忠实的观众开始转向节目的竞争对手——[美国广播公司](../Page/美国广播公司.md "wikilink")（[ABC](../Page/ABC.md "wikilink")）的《[早安美国](../Page/早安美国.md "wikilink")（*[Good
Morning America](../Page/Good_Morning_America.md "wikilink")*）》。

1990年末，《今天》这个长期以来占主导地位的节目，成为了落后于《[早安美国](../Page/早安美国.md "wikilink")》的收视第二的节目，而大部分指责都针对诺维尔。1991年，海湾战争爆发，诺维尔发现她作为联合主持的作用被不断地减少到最低限度。《今天》播出了《战争中的美国（***America
at
War***）》特别节目，[冈贝尔单独主持了大部分的特别节目](../Page/布赖恩特·冈贝尔.md "wikilink")。这对诺维尔来说是不寻常的，她甚至只出现在两个小时节目的第二个半小时里。1991年2月，诺维尔因产假而离开节目。节目方面宣布[凯蒂·库瑞克将在诺维尔离开期间代其主持](../Page/凯蒂·库瑞克.md "wikilink")。在诺维尔离开、[库瑞克开始工作之后](../Page/凯蒂·库瑞克.md "wikilink")，节目收视率立即反弹。

在产假期间，诺维尔接受了《[人物](../Page/人物_\(杂志\).md "wikilink")》周刊的访问。在访问中，她避谈在《今天》节目中遇到的种种问题，而更多谈论的是她刚出生的儿子。她还公布她用母乳喂儿子的照片，这一看上去不伤大雅的事情，却令NBC高层极为不满，认为照片内容低俗。1991年4月，鉴于《今天》节目收视率的提高以及NBC方面对《[人物](../Page/人物_\(杂志\).md "wikilink")》周刊所刊登照片的不满，NBC方面宣布诺维尔将不再回到《今天》节目，而[凯蒂·库瑞克将成为节目新的联合主持](../Page/凯蒂·库瑞克.md "wikilink")。据透露，虽然诺维尔将不会出现在任何一档NBC新闻节目中，但NBC还将按照合同继续支付诺维尔的薪水。

### 有关库瑞克和劳尔争执的传闻

从2003年开始，《今天》节目主持人[凯蒂·库瑞克和](../Page/凯蒂·库瑞克.md "wikilink")[马特·劳尔产生心结的传言甚嚣尘上](../Page/马特·劳尔.md "wikilink")。传闻指这是由于库瑞克在节目中的表现，她被普遍认为操作新闻节目，同时也被认为是能保证这一早间新闻节目高收视率的唯一一个人。\[7\]

### 库瑞克离开，维埃拉加盟

2006年4月5日，《今天》节目主持人[凯蒂·库瑞克在她成为节目联合主持](../Page/凯蒂·库瑞克.md "wikilink")15周年的这一天宣布，她将在5月底离开《今天》节目以及[NBC新闻部](../Page/NBC.md "wikilink")，並将跳槽到[哥伦比亚广播公司](../Page/哥伦比亚广播公司.md "wikilink")（CBS），成为《[CBS晚间新闻](../Page/CBS晚间新闻.md "wikilink")》的新任主播暨主编，同时出任CBS周日晚间播出的《[60分钟时事杂志](../Page/60分钟时事杂志.md "wikilink")》的记者。2006年5月31日，库瑞克在《今天》节目最后一次主持。那天的特别节目回顾了她15年來主持节目的历程，同时也庆祝她坐上CBS的主播台。在节目中库瑞克说："It's
been a pleasure hosting this program, and thank you for fifteen great
years."

在库瑞克宣布离职的翌日，[美国广播公司](../Page/美国广播公司.md "wikilink")（[ABC](../Page/ABC.md "wikilink")）《[观点](../Page/观点.md "wikilink")（*[The
View](../Page/The_View.md "wikilink")*）》节目主持人[梅莉蒂丝·维埃拉](../Page/梅莉蒂丝·维埃拉.md "wikilink")（Meredith
Vieira）在节目中宣布她将从九月起接替库瑞克，成为《今天》节目的联合主持。NBC新闻部亦於同一天开始播出欢迎维埃拉加盟的新宣传片，並宣布2006年夏季《今天》节目将移往一个临时的户外演播室播出，1A演播室将为节目的高清播出而重新装修。2006年夏季，包括[安·克莉](../Page/安·克莉.md "wikilink")（[Ann
Curry](../Page/Ann_Curry.md "wikilink")）、[娜特莉·莫拉莱斯](../Page/娜特莉·莫拉莱斯.md "wikilink")（[Natalie
Morales](../Page/Natalie_Morales.md "wikilink")）、[坎普尔·布朗](../Page/坎普尔·布朗.md "wikilink")（[Campbell
Brown](../Page/Campbell_Brown.md "wikilink")）等多位女主播（她们都曾是有機接替库瑞克的人選）都短暫擔任過《今天》节目的臨時联合主持，而维埃拉則於2006年9月13日正式加盟NBC，开始与[马特·劳尔共同主持](../Page/马特·劳尔.md "wikilink")《今天》节目。

男主持[马特·劳尔的合同保证他未来几年还将继续出任节目主持](../Page/马特·劳尔.md "wikilink")。他的合约签至2011年，同时他也获得了相当可观的加薪。

### 简·方达使用粗俗的语言

2008年2月14日早晨大约8：20，在有关舞台剧《阴道独白（*[The Vagina
Monologues](../Page/The_Vagina_Monologues.md "wikilink")*）》的现场访问环节中，[简·方达](../Page/简·方达.md "wikilink")（Jane
Fonda）使用了"[cunt](../Page/屄.md "wikilink")"这一单词。在大约8：40的广告时段之前，主持人[梅莉蒂丝·维埃拉为此事件致歉](../Page/梅莉蒂丝·维埃拉.md "wikilink")。NBC方面也重新编辑了稍后将在其他时区播出的节目。\[8\]\[9\]
[美国电视家长协会](../Page/美国电视家长协会.md "wikilink")（[Parents Television
Council](../Page/Parents_Television_Council.md "wikilink")）主席Tim
Winter稍后发表了一份控告书。\[10\]

## 注释

<references/>

## 外部連結

  - [Today](http://today.msnbc.com)
  - [Weekend
    Today](https://web.archive.org/web/20080724215424/http://weekendtoday.msnbc.com/)

[Today](../Category/美國電視新聞節目.md "wikilink")
[Category:早间电视节目](../Category/早间电视节目.md "wikilink")
[Today](../Category/NBC電視節目.md "wikilink")
[Category:NBC新闻](../Category/NBC新闻.md "wikilink")

1.  [A New Dawn for 'Today’ - 8/21/2006 - Broadcasting &
    Cable](http://www.broadcastingcable.com/article/CA6364164.html)

2.

3.
4.  [1](http://query.nytimes.com/gst/fullpage.html?res=9B0DEFDD103CF936A35757C0A961948260)

5.  [Monica Collins, "Memo to NBC: We Love Scott", *[USA
    Today](../Page/USA_Today.md "wikilink")*, [March
    1](../Page/March_1.md "wikilink"),
    1989.](http://pqasb.pqarchiver.com/USAToday/access/4066132.html?dids=4066132&FMT=ABS&FMTS=ABS&date=Mar+1%2C+1989&author=Collins%2C+Monica&pub=USA+TODAY&edition=&startpage=D1&desc=Memo+to+NBC%3A+We+Love+Scott)

6.  [Brian Donlon, "On *Today*, it's kiss and make up", ''[USA
    Today](../Page/USA_Today.md "wikilink"), [March
    14](../Page/March_14.md "wikilink"),
    1989.](http://pqasb.pqarchiver.com/USAToday/access/55908252.html?dids=55908252:55908252&FMT=ABS&FMTS=ABS:FT&date=Mar+14%2C+1989&author=Brian+Donlon&pub=USA+TODAY+\(pre-1997+Fulltext\)&edition=&startpage=01.D&desc=On+%60Today%2C%27+it%27s+kiss+and+make+up)

7.

8.  [[Redlasso](../Page/Redlasso.md "wikilink") - Jane Fonda
    Video](http://www.redlasso.com/ClipPlayer.aspx?id=45607794-9819-40e8-908e-a72101d8c22a)


9.  [Newsvine - Jane Fonda Uses Vulgar Slang on
    \`Today'](http://www.newsvine.com/_news/2008/02/14/1301473-jane-fonda-uses-vulgar-slang-on-today)

10. [NBC Assaults Families with Offensive Language on Today
    Show](http://www.parentstv.org/PTC/publications/release/2008/0214.asp)