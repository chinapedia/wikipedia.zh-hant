**國際疾病傷害及死因分類標準第十版**（，）是[世界卫生组织依据疾病的某些特征](../Page/世界卫生组织.md "wikilink")，按照规则将疾病分门别类，并用编码的方法来表示的系统。现有版本包括15.5万种代码，并记录多种新型诊断及预测，与[ICD-9版本相比较](../Page/ICD-9编码列表.md "wikilink")，该版本增加了1.7万个代码。\[1\]

ICD-10的研究起始于1983年，并于1992年完成。\[2\]

## ICD-10列表

下表为ICD-10列表，最新2016年英文版本详见：http://apps.who.int/classifications/icd10/browse/2016/en/

|       |                                                                   |                                                                                                               |
| ----- | ----------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| 章     | 块                                                                 | 标题                                                                                                            |
| I     | [A00-B99](../Page/ICD-10_第一章：某些传染病和寄生虫病.md "wikilink")            | 某些[传染病和](../Page/传染病.md "wikilink")[寄生虫病](../Page/寄生虫病.md "wikilink")                                         |
| II    | [C00-D48](../Page/ICD-10_第二章：肿瘤.md "wikilink")                    | [肿瘤](../Page/肿瘤.md "wikilink")                                                                                |
| III   | [D50-D89](../Page/ICD-10_第三章：血液及造血器官疾病和某些涉及免疫机制的疾患.md "wikilink") | [血液及](../Page/血液.md "wikilink")[造血器官疾病和某些涉及](../Page/造血器官.md "wikilink")[免疫系統的疾患](../Page/免疫系統.md "wikilink") |
| IV    | [E00-E90](../Page/ICD-10_第四章：内分泌，营养和代谢疾病.md "wikilink")           | [内分泌](../Page/内分泌.md "wikilink")，[营养和](../Page/营养.md "wikilink")[代谢疾病](../Page/代谢.md "wikilink")              |
| V     | [F00-F99](../Page/ICD-10_第五章：精神和行为障碍.md "wikilink")               | 精神和行为疾患                                                                                                       |
| VI    | [G00-G99](../Page/ICD-10_第六章：神经系统疾病.md "wikilink")                | [神经系统疾病](../Page/神经系统.md "wikilink")                                                                          |
| VII   | [H00-H59](../Page/ICD-10_第七章：眼和附器疾病.md "wikilink")                | [眼和附器疾病](../Page/眼.md "wikilink")                                                                             |
| VIII  | [H60-H95](../Page/ICD-10_第八章：耳和乳突疾病.md "wikilink")                | [耳和](../Page/耳.md "wikilink")[乳突疾病](../Page/乳突.md "wikilink")                                                 |
| IX    | [I00-I99](../Page/ICD-10_第九章：循环系统疾病.md "wikilink")                | [循环系统疾病](../Page/循环系统.md "wikilink")                                                                          |
| X     | [J00-J99](../Page/ICD-10_第十章：呼吸系统疾病.md "wikilink")                | [呼吸系统疾病](../Page/呼吸系统.md "wikilink")                                                                          |
| XI    | [K00-K93](../Page/ICD-10_第十一章：消化系统疾病.md "wikilink")               | [消化系统疾病](../Page/消化系统.md "wikilink")                                                                          |
| XII   | [L00-L99](../Page/ICD-10_第十二章：皮肤和皮下组织疾病.md "wikilink")            | [皮肤和](../Page/皮肤.md "wikilink")[皮下组织疾病](../Page/皮下组织.md "wikilink")                                           |
| XIII  | [M00-M99](../Page/ICD-10_第十三章：肌肉骨骼系统和结缔组织疾病.md "wikilink")        | [肌肉骨骼系统和](../Page/肌肉骨骼系统.md "wikilink")[结缔组织疾病](../Page/结缔组织.md "wikilink")                                   |
| XIV   | [N00-N99](../Page/ICD-10_第十四章：泌尿生殖系统疾病.md "wikilink")             | [泌尿生殖系统疾病](../Page/泌尿生殖系统.md "wikilink")                                                                      |
| XV    | [O00-O99](../Page/ICD-10_第十五章：妊娠、分娩和产褥期.md "wikilink")            | [妊娠](../Page/妊娠.md "wikilink")、[分娩和](../Page/分娩.md "wikilink")[产褥期](../Page/产褥期.md "wikilink")                |
| XVI   | [P00-P96](../Page/ICD-10_第十六章：起源于围产期的某些情况.md "wikilink")          | 起源于[围产期的某些情况](../Page/围产期.md "wikilink")                                                                      |
| XVII  | [Q00-Q99](../Page/ICD-10_第十七章：先天畸形、变形和染色体异常.md "wikilink")        | [先天畸形](../Page/先天畸形.md "wikilink")、变形和[染色体异常](../Page/染色体异常.md "wikilink")                                    |
| XVIII | [R00-R99](../Page/ICD-10_第十八章：症状、体征和临床与实验室异常所见.md "wikilink")     | 症状、体征和临床与实验室异常所见，不可归类在他处者                                                                                     |
| XIX   | [S00-T98](../Page/ICD-10_第十九章：损伤、中毒和外因的某些其他后果.md "wikilink")      | [损伤](../Page/损伤.md "wikilink")、[中毒和外因的某些其他后果](../Page/中毒.md "wikilink")                                       |
| XX    | [V01-Y98](../Page/ICD-10_第二十章：疾病和死亡的外因.md "wikilink")             | 疾病和[死亡的外因](../Page/死亡.md "wikilink")                                                                          |
| XXI   | [Z00-Z99](../Page/ICD-10_第二十一章：影响健康状态和与保健机构接触的因素.md "wikilink")   | 影响健康状态和与保健机构接触的因素                                                                                             |
| XXII  | [U00-U99](../Page/ICD-10_第二十二章：特殊目的代码.md "wikilink")              | 特殊目的代码                                                                                                        |

## 国际影响及趨勢

### 澳大利亚

[澳大利亚于](../Page/澳大利亚.md "wikilink")1998年引进ICD-10版本["ICD-10-AM"](http://nis-web.fhs.usyd.edu.au/ncch_new/2.aspx#)。

### 加拿大

[加拿大于](../Page/加拿大.md "wikilink")2000年引进该版本["ICD-10-CA"](https://web.archive.org/web/20090217054804/http://secure.cihi.ca/cihiweb/dispPage.jsp?cw_page=codingclass_icd10_e)。

### 德国

[德国在ICD](../Page/德国.md "wikilink")-10的基础上进行改版，即ICD-10-GM (German
Modification)。

### 美国

美国将于2013年10月1日正式官方採用该版本。\[3\]2012年4月9日HHS(美國衛生與公共服務部)公告將延至2014年10月起實施。
2014年4月1日美国国会通过法案，将实施日期从2014年10月1日推迟至2015年10月1日。

### 台灣

台灣目前研議中，[衛生署](../Page/中華民國衛生福利部.md "wikilink")[中央健康保險局預計在](../Page/中央健康保險局.md "wikilink")2015年後採用該版本(2012.10.22)\[4\]已延至2016年。

## 参考文献

## 相關條目

  - [ICD](../Page/國際疾病與相關健康問題統計分類.md "wikilink")
  - [ICD-9](../Page/ICD-9编码列表.md "wikilink")
  - [ICD-11](../Page/ICD-11.md "wikilink")

## 外部链接

  - [World Health Organization](../Page/世界卫生组织.md "wikilink")：ICD-10 -
    *International Statistical Classification of Diseases and Related
    Health
    Problems*[1](http://www.who.int/classifications/apps/icd/icd10online/)
    (10th Revision, Version for 2007)

[Category:受控词表](../Category/受控词表.md "wikilink")
[Category:受控医学词表](../Category/受控医学词表.md "wikilink")
[Category:國際疾病分類](../Category/國際疾病分類.md "wikilink")

1.
2.  [WHO | International Classification of Diseases
    (ICD)](http://www.who.int/classifications/icd/en/)
3.  [National Center for Health
    Statistics](http://www.cdc.gov/nchs/about/otheract/icd9/abticd10.htm)
4.  會務報導,"中央健保局函知ICD-10-CM/PCS往後展延至民國104年實施",中華民國藥師工會聯合會,rettieved
    Feb.13,2014.