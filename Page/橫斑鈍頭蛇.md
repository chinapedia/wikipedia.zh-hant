**橫斑鈍頭蛇**（[學名](../Page/學名.md "wikilink")：*Pareas
macularius*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[新蛇科](../Page/新蛇科.md "wikilink")[鈍頭蛇屬下的一個蛇種](../Page/鈍頭蛇屬.md "wikilink")，主要分布於[印度](../Page/印度.md "wikilink")、[孟加拉國](../Page/孟加拉國.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[馬來西亞西北部與及](../Page/馬來西亞.md "wikilink")[中國南部](../Page/中國.md "wikilink")（包括[雲南省](../Page/雲南省.md "wikilink")、[廣西省及](../Page/廣西省.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")）。

主要生活于山区。其生存的海拔范围为920至1620米。该物种的模式产地在缅甸。\[1\]

## 備註

  - Theobald, WILLIAM 1868 Catalogue of the reptiles of British Birna,
    embracing the provinces of Pegu, Martaban, and Tenasserim; with
    descriptions of new or little-known species. Jour. Linnean Soc.,
    London, Zool., 10: 4-67.

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")

1.