**西瓦鹿**（''Sivatherium
*），又名**濕婆鹿**，是已[滅絕的](../Page/滅絕.md "wikilink")[長頸鹿](../Page/長頸鹿.md "wikilink")，分佈在[非洲至](../Page/非洲.md "wikilink")[南亞](../Page/南亞.md "wikilink")。其下於非洲發現的*S.
maurusium*曾一度被分類在*Libytherium''[屬中](../Page/屬.md "wikilink")。

*S.
giganteum*的化石發現於[喜馬拉雅山脈山麓地帶地質年代約西元一百萬年前的地層](../Page/喜馬拉雅山脈.md "wikilink")。*S.
maurusium*可能直到8,000年前才滅絕，因為在[撒哈拉沙漠](../Page/撒哈拉沙漠.md "wikilink")\[1\]與[中央邦](../Page/中央邦.md "wikilink")\[2\]地區的中有出現類似的描繪。

## 描述

[MEPAN_Sivatherium.jpg](https://zh.wikipedia.org/wiki/File:MEPAN_Sivatherium.jpg "fig:MEPAN_Sivatherium.jpg")
[SivatheriumLyd.jpg](https://zh.wikipedia.org/wiki/File:SivatheriumLyd.jpg "fig:SivatheriumLyd.jpg")
[SivatheriumSkull.jpg](https://zh.wikipedia.org/wiki/File:SivatheriumSkull.jpg "fig:SivatheriumSkull.jpg")
西瓦鹿外觀像現今的[霍加狓](../Page/霍加狓.md "wikilink")，但體型較大及粗壯，[肩高達](../Page/肩.md "wikilink")，總高度可達，體重則可達。一個更新的估計顯示西瓦鹿的體重可能重達\[3\]，這讓西瓦鹿成為目前已知最大型的[反芻動物](../Page/反芻動物.md "wikilink")，此估計體重不含雄性鹿角的重量，因此實際體重可能更重。牠有一對很闊的[鹿角](../Page/鹿角.md "wikilink")，[眼睛上則有第二對鹿角](../Page/眼睛.md "wikilink")。牠的肩膀非常強壯，可以支撐[頸部](../Page/頸部.md "wikilink")[肌肉及重的](../Page/肌肉.md "wikilink")[頭顱骨](../Page/頭顱骨.md "wikilink")\[4\]。

## 參看

  - *[原利比鹿](../Page/原利比鹿.md "wikilink")*

## 參考文獻

## 延伸閱讀

  - Barry Cox, Colin Harrison, R.J.G. Savage, and Brian Gardiner.
    (1999): The Simon & Schuster Encyclopedia of Dinosaurs and
    Prehistoric Creatures: A Visual Who's Who of Prehistoric Life. Simon
    & Schuster.
  - David Norman. (2001): The Big Book Of Dinosaurs. pg. 228, Walcome
    books.

## 外部連結

[Category:西瓦鹿屬](../Category/西瓦鹿屬.md "wikilink")
[Category:古動物](../Category/古動物.md "wikilink")
[Category:非洲史前哺乳動物](../Category/非洲史前哺乳動物.md "wikilink")
[Category:亞洲史前哺乳動物](../Category/亞洲史前哺乳動物.md "wikilink")

1.
2.

3.

4.