1952年度[環球小姐比賽是首届舉辦](../Page/環球小姐.md "wikilink")。地點為[美國](../Page/美國.md "wikilink")[加利福尼亞州的](../Page/加利福尼亞州.md "wikilink")[長灘](../Page/長灘_\(加利福尼亞州\).md "wikilink")。這次比賽吸引了三十個國家地區參加。而冠軍就是[芬蘭的](../Page/芬蘭.md "wikilink")得到。

## 有關資料

  - 届數：第一届
  - 參賽選手總數：三十位
  - 地點：[美國](../Page/美國.md "wikilink")[加州](../Page/加利福尼亞州.md "wikilink")[長灘](../Page/長灘_\(加利福尼亞州\).md "wikilink")
  - 總決賽日期：六月二十八日

1952年－1967年的環球小姐比賽是跟美國小姐一起舉辦。

## 最後成績

  - **冠軍**：[芬蘭小姐](../Page/芬蘭.md "wikilink")
  - **亞軍**：[夏威夷小姐Elza](../Page/夏威夷.md "wikilink") Kananionapua Edsman
  - **季軍**：[希臘小姐Daisy](../Page/希臘.md "wikilink") Mavraki
  - **第四名**：[香港小姐Judy](../Page/香港.md "wikilink")
    Dan（[但茱迪](../Page/但茱迪.md "wikilink")）
  - **第五名**：[德國小姐Renate](../Page/德國.md "wikilink") Hoy
  - **第六至第十名**（排名不分前後）：
      - [墨西哥小姐Olga](../Page/墨西哥.md "wikilink") Llorens Perez Castillo
      - [南非小姐Catherine](../Page/南非.md "wikilink") Edwina Higgins
      - [瑞典小姐Anne](../Page/瑞典.md "wikilink") Marie Tistler
      - [烏拉圭小姐Gladys](../Page/烏拉圭.md "wikilink") Rubio
      - [美國小姐Jackie](../Page/美國.md "wikilink") Loughery
  - **特別獎**：
      - **親善小姐**：[比利時小姐Myriam](../Page/比利時.md "wikilink") Lynn
      - **最受歡迎小姐**：[智利小姐Esther](../Page/智利.md "wikilink") Saavedra
        Yoacham
      - **友誼小姐**：[蒙塔拿洲小姐Valerie](../Page/蒙塔拿洲.md "wikilink") Johnson

注：由於當年環球小姐比賽和美國小姐一起舉辦，所以友誼小姐由美國小姐比賽的蒙大拿洲得到。但由於非環球小姐選手得到，所以可不承認為當年環球小姐比賽的友誼小姐。

## 選手名單

**1952年度環球小姐比賽選手名單**

**注**：

  - [阿拉斯加小姐和](../Page/阿拉斯加.md "wikilink")[夏威夷小姐是美國的洲份](../Page/夏威夷.md "wikilink")，而1959年開始此兩洲的選手會直接參加[美國小姐](../Page/美國小姐.md "wikilink")。
  - [香港和](../Page/香港.md "wikilink")[加拿大當時是英國屬地](../Page/加拿大.md "wikilink")。
  - [南非是屬地](../Page/南非.md "wikilink")。

<!-- end list -->

  - **[Flag_of_Alaska.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Alaska.svg "fig:Flag_of_Alaska.svg")
    [阿拉斯加](../Page/阿拉斯加.md "wikilink")** - Shirley Burnett
  - **** - Leah MacCartney
  - **** - Myriam Lynn
  - **** - Ruth Carrier
  - **** - Esther Saavedra Yoacham
  - **** - Gladys López
  - **** - Hanne Sorensen
  - **** - Jackie Loughery
  - **** - Teresita Torralba Sanchez
  - **** - Armi Kuusela
  - **** - Claude Goddart
  - **** - Renate Hoy
  - **** - Aileen Chase
  - **** - Daisy Mavraki

<!-- end list -->

  - **[Flag_of_Hawaii.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hawaii.svg "fig:Flag_of_Hawaii.svg")
    [夏威夷](../Page/夏威夷.md "wikilink")** - Elza Kananionapua Edsman
  - **[Flag_of_Hong_Kong_1959.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong_1959.svg "fig:Flag_of_Hong_Kong_1959.svg")[香港](../Page/香港.md "wikilink")**
    - 但茱迪 (Judy Dan)
  - **** - Indrani Rahman
  - **** - Ora Vered
  - **** - Giovanna Mazzotti
  - **** - Himeko Kojima
  - **** - Olga Llorens Pérez Castillo
  - **** - Eva Roine
  - **** - Elzibir Gisela Malek
  - **** - Ada Gabriela Bueno
  - **** - Marilia Levy
  - **** - Catherine Edwina Higgins
  - **** - Anne Marie Tistler
  - **** - Gelengul Tayforoglu
  - **** - Gladys Rubio
  - **** - Sofia Silva Inserri

## 重要記錄

  - [瑞典小姐是](../Page/瑞典.md "wikilink")1952年[歐洲小姐的參賽選手](../Page/歐洲小姐.md "wikilink")。
  - [英國小姐參加了](../Page/英國.md "wikilink")1951年的[世界小姐比賽](../Page/世界小姐.md "wikilink")。
  - [烏拉圭小姐原是Rosa](../Page/烏拉圭.md "wikilink") Adela Prunell，但後來改成Gladys
    Rubio。

## 評判員

  - **Vincent Trotta**
  - **Milo Anderson**
  - **Gilbert Roland**
  - **Samuel Heavenrich**
  - **Robert Goldstein**
  - **Tom Kelly**
  - **Robert Palmer**
  - **Arlene Dahl**
  - **Yucca Salamunich**
  - **Constance Moore**

[Category:環球小姐](../Category/環球小姐.md "wikilink")
[Category:1952年美国](../Category/1952年美国.md "wikilink")