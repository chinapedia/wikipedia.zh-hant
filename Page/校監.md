**校監**（）有时也译为**名誉校长**，是[學校校務會議主席](../Page/學校.md "wikilink")，相當於企業[董事長](../Page/董事長.md "wikilink")（Chairman）的角色。負責主持[校董會](../Page/校董會.md "wikilink")（校務會議）和畢業大典，以及決定學校長遠發展願景。

## 英式傳統

許多中文翻譯會誤將Chancellor翻譯為校長，或將Vice-Chancellor錯誤翻譯為副校長。正確翻譯如下：

  - 校監（Chancellor）
      - 副校監（Pro-Chancellor）
  - [校長](../Page/校長.md "wikilink")（Vice-Chancellor & President）
      - 首席副校長（Deputy Vice-Chancellor）
          - 副校長（Pro Vice-Chancellor）

### 英國

知名大學的校監多半是由德高望重的政治人物或王室成員出任，例如[牛津大學的現任校監為末代](../Page/牛津大學.md "wikilink")[香港總督](../Page/香港總督.md "wikilink")[彭定康](../Page/彭定康.md "wikilink")；[劍橋大學的校監則為](../Page/劍橋大學.md "wikilink")[盛伯理男爵](../Page/盛伯理男爵.md "wikilink")。在英國，王室成員有可兼任不只一所大學校監的慣例，例如：[安妮公主就兼任](../Page/安妮公主.md "wikilink")[倫敦大學和](../Page/倫敦大學.md "wikilink")[愛丁堡大學校監](../Page/愛丁堡大學.md "wikilink")。

### 澳大利亞與新西蘭

知名大學的校監和副校監多半是由德高望重的政治人物或學者出任，例如[澳洲國立大學](../Page/澳洲國立大學.md "wikilink")（ANU）的現任校監曾任澳洲聯邦[參議院議長](../Page/參議院.md "wikilink")、[外交部部長和](../Page/外交部.md "wikilink")[澳洲工黨](../Page/澳洲工黨.md "wikilink")[黨魁等等](../Page/黨魁.md "wikilink")。過去也曾由[諾貝爾獎得主和卸任](../Page/諾貝爾獎.md "wikilink")[澳洲總理擔任ANU校監](../Page/澳洲總理.md "wikilink")。另如[奧克蘭大學現任校監Ian](../Page/奧克蘭大學.md "wikilink")
M. Parton博士曾任新西蘭工程師協會主席等職務，現任副校監Scott St. John為新西蘭著名金融機構First NZ
Capital的掌門人。而2013年上任的現任的[悉尼大學校監](../Page/悉尼大學.md "wikilink")，是金融和商界人士，她是歷史上首個當選悉大校監的商人，是悉大原經濟系的畢業生。

### 香港

在[香港特別行政區](../Page/香港特別行政區.md "wikilink")，所有受獨立條例規管的[法定大專院校](../Page/香港專上教育.md "wikilink")，都需按有關法例規定，由[香港行政長官兼任校監](../Page/香港行政長官.md "wikilink")，在[主權移交前為](../Page/香港主權移交.md "wikilink")[香港總督](../Page/香港總督.md "wikilink")。因此，香港的[公立大專院校的校監](../Page/公立大學.md "wikilink")，都是香港行政長官，當中包括接受[大學教育資助委員會資助的大學](../Page/大學教育資助委員會.md "wikilink")，財政自給的[香港公開大學和受公帑資助的](../Page/香港公開大學.md "wikilink")[香港演藝學院](../Page/香港演藝學院.md "wikilink")\[1\]
。香港的私立大專院校，例如[香港樹仁大學和](../Page/香港樹仁大學.md "wikilink")[珠海學院的校監](../Page/珠海學院.md "wikilink")，可由有關辦學團體決定。而隸屬[職業訓練局的](../Page/職業訓練局.md "wikilink")[香港專業教育學院](../Page/香港專業教育學院.md "wikilink")，由政府委任的職業訓練局主席，則具有類似校監的職能。

香港的學校如要獲得政府資助，都需要按規定設立校董會或[法團校董會](../Page/法團校董會.md "wikilink")，所以香港大部分學校都設有校董會，而校監也是校董會的成員\[2\]，但不能兼任[教師或](../Page/教師.md "wikilink")[校長](../Page/校長.md "wikilink")。香港接受政府資助的學校，校監可由校董會推選或由辦學團體委任，因此很多有[宗教背景的學校](../Page/宗教.md "wikilink")，其校監都是該辦學團體委派的牧師、神父等宗教或神職人員。就接受[教育局規管的](../Page/教育局.md "wikilink")[中學和](../Page/中學.md "wikilink")[小學而言](../Page/小學.md "wikilink")，校監的職責包括主持法團校董會的會議，向教育局通知校董、校長或教員的任免，監察學校的帳目，並確保學校合法運作。

## 美國

大多數的（單一主校區）美國大學，並無[校長與校監職權分立之設計](../Page/校長.md "wikilink")，因此校長（President）便相當於校監，既是學校的對外代表人，亦是最高行政首長。另設置副校長（Provost）協助校務。

但少數擁有多個校園的美國大學，則會增設校監一職，位階高於個別校區的[校長](../Page/校長.md "wikilink")，以方便校務整合與推動。

然而，亦有些[州立大學系統反其道而行](../Page/州立大學系統.md "wikilink")，例如：[伊利諾大學](../Page/伊利諾大學.md "wikilink")、[麻薩諸塞大學和](../Page/麻薩諸塞大學.md "wikilink")[加利福尼亞大學](../Page/加利福尼亞大學.md "wikilink")，改以校長擔任大學系統之最高行政首長，各[州立大學系統分校另設置分校校監進行管理](../Page/州立大學系統.md "wikilink")。

## 日韓

在[日本和](../Page/日本.md "wikilink")[韓國的](../Page/韓國.md "wikilink")[私立學校中](../Page/私立學校.md "wikilink")，設有相當於校董會的[理事會](../Page/理事會.md "wikilink")，其負責人稱為[理事長](../Page/理事長.md "wikilink")，相當於校監的角色。在韓國副校長叫做校監。

## 參考資料

<references />

[Category:教育人物](../Category/教育人物.md "wikilink")
[Category:職業](../Category/職業.md "wikilink")
[Category:學術榮譽](../Category/學術榮譽.md "wikilink")

1.
2.