**維荻婭·茂瓦德**（***Vidya
Malvade***）是一個[寶萊塢的電影演員](../Page/寶萊塢.md "wikilink")。

## 個人生活

維荻婭曾經學習過法律和作過空中小姐，她的第一任丈夫曾是Alliance Air航空公司的飛行員巴嘎機長（Capt
Bagga），但是他不幸死于一次墜機事故
[1](https://web.archive.org/web/20070311192336/http://www.apunkachoice.com/scoop/bollywood/20040109-3.html)
[2](http://bollywood.allindiansite.com/g/2005101906.html).
(詳情請看《[飛行資訊](https://web.archive.org/web/20071122181009/http://civilaviation.nic.in/coi/)》)。

## 電影事業

初出于2003年，現今演過最知名的電影是《[加油印度！](../Page/加油印度！.md "wikilink")》，
在這部影片中[沙·茹克·罕扮演印度女子曲棍球隊的教練](../Page/沙·茹克·罕.md "wikilink")，她則扮演守門員兼隊長。

## 出演電影

| 年份    | 電影名                                                        | 角色名            | 備註             |
| ----- | ---------------------------------------------------------- | -------------- | -------------- |
| 2003年 | *[Inteha](../Page/Inteha_\(2003_film\).md "wikilink")*     | Nandini Saxena |                |
| 2005年 | *[Mashooka](../Page/Mashooka.md "wikilink")*               | Monica         |                |
| 2007年 | *[Chak De India](../Page/Chak_De_India.md "wikilink")*     | Vidya Sharma   |                |
| 2007年 | *[Benaam](../Page/Benaam.md "wikilink")*                   |                |                |
| 2007年 | *[Kidnap](../Page/Kidnap_\(film\).md "wikilink")*          |                | filming        |
| 2007年 | *[Bollywood Blues](../Page/Bollywood_Blues.md "wikilink")* | Kajol Dixit    | pre-production |
| 2008年 | *[Striker](../Page/Striker_\(film\).md "wikilink")* \[1\]  | Kajol Dixit    | pre-production |

## 外部連結

  -
  - [Vidya's new movie Chak De
    India](https://web.archive.org/web/20070824055258/http://chakde-india.com/)

  - [Vidya's Fan Blog](http://vidya-malvade.blogspot.com/)

## 參考文獻

[M](../Category/印度电影女演员.md "wikilink")
[Category:21世纪印度女演员](../Category/21世纪印度女演员.md "wikilink")
[Category:孟买女演员](../Category/孟买女演员.md "wikilink")

1.  <http://sify.com/movies/bollywood/fullstory.php?id=14550722>