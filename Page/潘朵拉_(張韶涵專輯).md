《**潘朵拉**》（[英語](../Page/英語.md "wikilink")：**Pandora**）為[台灣藝人](../Page/台灣.md "wikilink")[張韶涵發行的第三張錄音室專輯](../Page/張韶涵.md "wikilink")。專輯中《[隱形的翅膀](../Page/隱形的翅膀.md "wikilink")》的歌詞曾被作為[北京市](../Page/北京.md "wikilink")2009年高考作文題\[1\]。歌曲《口袋的天空》入選[日本教材](../Page/日本.md "wikilink")，被收錄於日l本日中通信社出版的書籍《通過唱歌記住中國話》中。張韶涵憑藉著《潘朵拉》專輯入圍2007年[第18屆金曲獎最佳國語女歌手獎獎項](../Page/第18屆金曲獎.md "wikilink")，也同時入圍[馬來西亞當地華語樂壇權威性大獎](../Page/馬來西亞.md "wikilink")2006「娛協獎」「國際組」20強。

## 發行版本

  - 《潘朵拉》
  - 《潘朵拉寫真慶功版(潘朵拉繽紛愛殺版)》(CD+DVD)（2006年2月24日）

## 曲目

## MV

| \#   | 歌名                                                    | 導演                                                                | 附註                                                    |
| ---- | ----------------------------------------------------- | ----------------------------------------------------------------- | ----------------------------------------------------- |
| 1\.  | [隱形的翅膀](https://www.youtube.com/watch?v=be2wvNFTLMc)  | [黃中平](../Page/黃中平.md "wikilink")                                  |                                                       |
| 2\.  | [潘朵拉](https://www.youtube.com/watch?v=vAtz0NWJbWo)    | [黃中平](../Page/黃中平.md "wikilink")                                  | 與[任佑明演出](../Page/任佑明.md "wikilink")                   |
| 3\.  | [香水百合](https://www.youtube.com/watch?v=8y0OGAA_NKo)   | [楊大慶](../Page/楊大慶.md "wikilink")+[比爾賈](../Page/比爾賈.md "wikilink") |                                                       |
| 4\.  | [真的](https://www.youtube.com/watch?v=5HkOhMLK9Qg)     | [楊富鳴](../Page/楊富鳴.md "wikilink")                                  | 與[陸廷威演出](../Page/陸廷威.md "wikilink")                   |
| 7\.  | [保護色](https://www.youtube.com/watch?v=1m9gp9PsUMk)    | [賴偉康](../Page/賴偉康.md "wikilink")                                  |                                                       |
| 8\.  | [口袋的天空](https://www.youtube.com/watch?v=IDhZy6tfJGY)  | [楊富鳴](../Page/楊富鳴.md "wikilink")                                  | MV部分取景於[新北市立十三行博物館](../Page/新北市立十三行博物館.md "wikilink") |
| 9\.  | [愛情旅程](https://www.youtube.com/watch?v=LeMp08kK03I)   | [張清峰](../Page/張清峰.md "wikilink")                                  |                                                       |
| 10\. | [喜歡你沒道理](https://www.youtube.com/watch?v=rhuNm2X7nbA) | [黃中平](../Page/黃中平.md "wikilink")                                  |                                                       |

## 銷售成績

  - 5大唱片 5大金榜 華語榜 冠軍（2006年第2週）<ref>

<https://www.5music.com.tw/CDTop.asp></ref>

## 獎項

  - [第18屆金曲獎](../Page/第18屆金曲獎.md "wikilink")——最佳國語女歌手獎入圍
  - 2006 CCTV-MTV音樂盛典-台灣最受歡迎女歌手
  - 2006星光大典-台港最受歡迎女歌手獎
  - 2006年第十三屆華語榜中榜頒獎-最受歡迎女歌手獎（台港部分） ：張韶涵
  - 2006年第十三屆華語榜中榜頒獎-年度最佳歌曲獎（台港部分） ：張韶涵《隱形的翅膀》
  - 2006MusicRadio中國TOP排行榜最受歡迎女歌手（台港）：張韶涵《潘朵拉》
  - 2006MusicRadio中國TOP排行榜點播冠軍曲（台港）：張韶涵《隱形的翅膀》
  - 2006年度雪碧中國原創音樂流行榜季選-台港金曲獎：張韶涵《真的》
  - 2006年度雪碧中國原創音樂流行榜季選-最具人氣歌手獎：張韶涵
  - 2006年度雪碧中國原創音樂流行榜季選-優秀表現獎：張韶涵
  - 2006年度雪碧音樂榜-台港金曲獎：張韶涵《真的》
  - 2006年度雪碧音樂榜-最優秀視像音樂獎：張韶涵《潘朵拉》
  - 2006年度雪碧音樂榜-最受歡迎女歌手：台灣地區張韶涵
  - 2007KKBox頒獎典禮-年度十大專輯《潘朵拉》
  - 2007KKBox頒獎典禮-年度二十大單曲：《隱形的翅膀》張韶涵《潘朵拉》

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部链接

  - [福茂唱片](https://web.archive.org/web/20061015003821/http://www.linfairrecords.com/music/album.php?id=2650)

[Category:張韶涵音樂專輯](../Category/張韶涵音樂專輯.md "wikilink")
[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")

1.  [。歌曲《隐形的翅膀》作者對北京作文題目"很意外"](http://news.163.com/09/0607/16/5B7GTEGS00013DM4.html)