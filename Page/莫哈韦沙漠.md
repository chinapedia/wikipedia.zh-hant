**莫哈韦沙漠**（**Mojave
Desert**）是個位於[美國西南部](../Page/美國.md "wikilink")，南[加利福尼亞州東南部的](../Page/加利福尼亞州.md "wikilink")[沙漠](../Page/沙漠.md "wikilink")。莫哈韦沙漠橫跨[猶他州](../Page/猶他州.md "wikilink")、[內華達州南部及](../Page/內華達州.md "wikilink")[亞利桑那州西北部地區](../Page/亞利桑那州.md "wikilink")。佔地超過2萬2000平方英里（5萬7000平方公里）是典型的[盆地地形](../Page/盆地.md "wikilink")，有特殊的西部沙漠景致。

[美國太空總署](../Page/美國太空總署.md "wikilink")（NASA）也在此設置一座偵測[衛星基地](../Page/衛星.md "wikilink")，並和其他地方的基地聯合，成立[遠太空聯絡網](../Page/深空網路.md "wikilink")。另外有一座[莫哈維機場設於此](../Page/莫哈維機場.md "wikilink")，封存許多停用的民用飛機。

此沙漠亦是蘋果公司於2018年6月發表的[macOS
Mojave作業系統之命名由來以及](../Page/macOS_Mojave.md "wikilink")[桌布拍攝地](../Page/電腦壁紙.md "wikilink")。

## 參考資料

## 外部链接

  - [美國沙漠網](http://www.desertusa.com/du_mojave.html)

[Category:美國沙漠](../Category/美國沙漠.md "wikilink")
[Category:南加利福尼亚州地理](../Category/南加利福尼亚州地理.md "wikilink")
[Category:內華達州地理](../Category/內華達州地理.md "wikilink")
[Category:亞利桑那州地理](../Category/亞利桑那州地理.md "wikilink")
[Category:猶他州地理](../Category/猶他州地理.md "wikilink")
[Category:加利福尼亚州克恩县地理](../Category/加利福尼亚州克恩县地理.md "wikilink")