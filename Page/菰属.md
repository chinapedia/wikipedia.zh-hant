**菰属**（[学名](../Page/学名.md "wikilink")：**）是[禾本科](../Page/禾本科.md "wikilink")[水生](../Page/水生植物.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")，共有4种。其中3种分布于[北美洲](../Page/北美洲.md "wikilink")，[亚洲只有一种](../Page/亚洲.md "wikilink")，即[菰](../Page/菰.md "wikilink")（*Z.
latifolia*）。其子呈黑色，可食用。

英语称其子为 "wild rice"，即“野米”。不过它与大米只是远亲关系。比起在中国，在西方国家其子被更经常食用。

## 分类

本属共有4个[物种](../Page/物种.md "wikilink")\[1\]：

  - [水生菰](../Page/水生菰.md "wikilink")（*Zizania
    aquatica*）：[一年生](../Page/一年生.md "wikilink")，分布于北美洲[五大湖及](../Page/五大湖.md "wikilink")[北方针叶林区的湖泽地带](../Page/北方针叶林.md "wikilink")。
  - [菰](../Page/菰.md "wikilink")（*Zizania
    latifolia*）：又名**茭白筍**，[多年生](../Page/多年生.md "wikilink")[宿根](../Page/宿根.md "wikilink")，主要产于[东亚和](../Page/东亚.md "wikilink")[东南亚](../Page/东南亚.md "wikilink")。
  - [沼生菰](../Page/沼生菰.md "wikilink")（*Zizania
    palustris*）：一年生，分布于北美洲[圣劳伦斯河及](../Page/圣劳伦斯河.md "wikilink")[美国](../Page/美国.md "wikilink")[大西洋及](../Page/大西洋.md "wikilink")[墨西哥湾沿岸](../Page/墨西哥湾.md "wikilink")。
  - [得州菰](../Page/得州菰.md "wikilink")（*Zizania
    texana*）：多年生，仅分布于美国[得州的](../Page/得州.md "wikilink")一带狭小区域，因栖息地丧失及环境污染问题而处于[濒危状态](../Page/濒危.md "wikilink")。

## 粮食

菰在古代中國曾被食用，茭米为九谷或六谷之一，在菰茎中寄生的[菰黑粉菌](../Page/菰黑粉菌.md "wikilink")（*Ustilago
esculenta*）会刺激[薄壁组织的生长](../Page/薄壁组织.md "wikilink")，使幼嫩茎部膨大，成为[茭白](../Page/茭白.md "wikilink")（又名茭瓜、[茭白筍](../Page/茭白筍.md "wikilink")），是[中国南方常见的一种](../Page/中国.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")。

自古以來[奧吉比瓦族](../Page/奧吉比瓦族.md "wikilink")[印第安人也主要以](../Page/印第安人.md "wikilink")[沼生菰](../Page/沼生菰.md "wikilink")（*Z.
palustris*）為食。

## 参考文献

## 外部链接

  - [菰的误译和野生稻](http://agri-history.net/scholars/yxl/gu%20and%20wild%20rice.htm)：游修龄

[Category:禾本科](../Category/禾本科.md "wikilink")
[Category:粮食](../Category/粮食.md "wikilink")
[Category:谷物](../Category/谷物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  USDA, ARS, National Genetic Resources Program. *Germplasm Resources
    Information Network - (GRIN)* \[Online Database\]. National
    Germplasm Resources Laboratory, Beltsville, Maryland. URL:  (05
    March 2014)