[Slavyanka_1900.jpg](https://zh.wikipedia.org/wiki/File:Slavyanka_1900.jpg "fig:Slavyanka_1900.jpg")”（）屯墾區,
1900年\]\]

**哈桑斯基县**（）处于[俄罗斯](../Page/俄罗斯.md "wikilink")[滨海边疆区的西南部](../Page/滨海边疆区.md "wikilink")，与[中国和](../Page/中国.md "wikilink")[朝鲜接壤](../Page/朝鲜.md "wikilink")。人口44,000人，現任區長為克利辛·維亞切斯拉夫·阿列克謝耶維奇（）。

## 歷史

**哈桑斯基县**前身為波谢耶特县，1926年4月1日成立，区中心位於[斯拉维扬卡](../Page/斯拉维扬卡.md "wikilink")。1938年[二战前夕](../Page/二战.md "wikilink")[大日本帝國和蘇聯於双方边界发生了](../Page/大日本帝國.md "wikilink")[张鼓峰事件](../Page/张鼓峰事件.md "wikilink")(哈桑湖事件)，以日軍战败為终结，苏联为纪念此事件中的阵亡军人，于1939年6月5日将波谢耶特区更名为哈桑区。

## 氣候

哈桑区的面积占整个滨海边疆区面积的2.5%，地处沿海，地形多样，河流众多。该地属海洋性温带气候，夏季平均温度在20°到25°之间,夏季降水量占年降水量的70%。

## 行政区划

**哈桑斯基县**下设6个（）和2个[乡](../Page/农村居民点.md "wikilink")（)，6个[社区](../Page/市級鎮.md "wikilink")（市級鎮）和31[村](../Page/村.md "wikilink")（）共37个村级定居点。

<table>
<thead>
<tr class="header">
<th><p>序号</p></th>
<th><p>中文名字</p></th>
<th><p>本地名字</p></th>
<th><p>行政中心</p></th>
<th><p>下辖村级单位<br />
数量</p></th>
<th><p>人口<br />
人</p></th>
<th><p>面积<br />
km<sup>2</sup></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/扎鲁比诺.md" title="wikilink">扎鲁比诺社区</a></p></td>
<td><p>6</p></td>
<td><p>3,795</p></td>
<td><p>244.00</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/克拉斯基诺.md" title="wikilink">克拉斯基诺社区</a></p></td>
<td><p>6</p></td>
<td><p>3,708</p></td>
<td><p>848.05</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波西耶特.md" title="wikilink">波谢特社区</a></p></td>
<td><p>2</p></td>
<td><p>2,410</p></td>
<td><p>119.70</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td></td>
<td></td>
<td><p>1</p></td>
<td><p>2,024</p></td>
<td><p>2.97</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/斯拉维扬卡.md" title="wikilink">斯拉维扬卡社区</a></p></td>
<td><p>7</p></td>
<td><p>13,033</p></td>
<td><p>466.90</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td></td>
<td><p>社区</p></td>
<td><p>2</p></td>
<td><p>695</p></td>
<td><p>46.00</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td></td>
<td><p>村</p></td>
<td><p>7</p></td>
<td><p>6,334</p></td>
<td><p>488.00</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td></td>
<td><p>村</p></td>
<td><p>6</p></td>
<td><p>1,368</p></td>
<td><p>66.0</p></td>
</tr>
</tbody>
</table>

## 關連項目

  - [扎魯比諾](../Page/扎魯比諾.md "wikilink")

  - （）

  - [哈桑湖](../Page/哈桑湖.md "wikilink")（）

  - （）

  - （）

  - [西定民燈塔](../Page/西定民燈塔.md "wikilink")（，位於“克拉利島”（）上）

  - [納利馬夫燈塔](../Page/納利馬夫燈塔.md "wikilink")（，位於“巴謝達灣”前之“納利馬夫岬”（）上）

  - [巴謝達灣](../Page/波西耶特灣.md "wikilink")（）

  - [克拉利島](../Page/克拉利島.md "wikilink")（）

  - [納利馬夫岬](../Page/納利馬夫岬.md "wikilink")（）

  - [張鼓峰事件](../Page/張鼓峰事件.md "wikilink")

## 外部連結

  - [非官方哈桑縣網頁（）](http://khasan-district.narod.ru/)

  - [哈桑燈塔（）](http://khasan-district.narod.ru/history/mayak.htm)

  - [哈桑縣克拉利島（）](http://khasan-district.narod.ru/directory/geograf/krolichiy_o.htm)

  - [哈桑縣納利馬夫岬（）](http://khasan-district.narod.ru/directory/geograf/nazimova_m.htm)

  - [哈桑縣巴謝達灣（）](http://khasan-district.narod.ru/directory/geograf/posieta_z.htm)

  - [哈桑概况（）](http://www.asiadata.ru/?lang=ru&path=0:700:616:751:618:619:1527&id=1547)

[Х](../Category/濱海邊疆區.md "wikilink")