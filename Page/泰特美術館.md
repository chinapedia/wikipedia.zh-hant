**泰特美術館**（**Tate**） ，收藏[英國與](../Page/英國.md "wikilink")[現代藝術](../Page/現代藝術.md "wikilink")，並不屬於英國政府組織機構，只是其主要的贊助來自於英國文化部門，在[英格蘭有四個據點](../Page/英格蘭.md "wikilink")：[泰特不列顛](../Page/泰特不列顛.md "wikilink")（1897年成立、2000年重新命名）、泰特利物浦美術館（1988年成立）、泰特聖艾富思美術館（1993年成立）以及[泰特現代藝術館](../Page/泰特現代藝術館.md "wikilink")（2000年成立），另外還有一個互相搭配的網站：泰特線上（1998年成立）。

## 外部連結

  - [Tate Online](http://www.tate.org.uk/) — 65,000 works from the Tate
    Collection online, information on Tate's exhibitions and events
    programmes, and online learning resources.
  - [TATE ETC. Magazine](http://www.tate.org.uk/tateetc/)
  - [Turner
    Worldwide](http://www.tate.org.uk/servlet/BrowseGroup?cgroupid=999999953)
    - an ongoing online cataloguing of JMW Turner's work around the
    world.
  - [Turner Collection
    Online](http://www.tate.org.uk/servlet/BrowseGroup?cgroupid=999999998)
    The online catalogue of Tate's collection of nearly 300 oil
    paintings and 30,000 works on paper by JMW Turner.
  - [Tate Gallery
    Records](http://www.tate.org.uk/research/researchservices/archive/archiverecords.htm)
    Tate's own historical records.
  - [Tate Podcasts](http://www.tate.org.uk/podcasts/) Audio and video
    podcasts from Tate.
  - [Tate in Space](http://www.tate.org.uk/space/)
  - [Turner
    Museum](https://web.archive.org/web/20100216031921/http://www.turnermuseum.org/)

[Category:英國美術館](../Category/英國美術館.md "wikilink")
[Category:英国当代艺术](../Category/英国当代艺术.md "wikilink")
[Category:當代藝術館](../Category/當代藝術館.md "wikilink")