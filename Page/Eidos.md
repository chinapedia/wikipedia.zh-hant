**Eidos
Interactive**，上海子公司译名**艺夺**\[1\]。是一家总部位于[英国的](../Page/英国.md "wikilink")[电子游戏发行公司](../Page/电子游戏.md "wikilink")，成立于1990年。它初期經營影片壓縮科技。但隨著年代的發展，Eidos於1995年進軍遊戲娛樂市場并獲得了遊戲公司Domark的Championship
Manager版權和在[倫敦証券交易所上掛牌](../Page/倫敦証券交易所.md "wikilink")。在1996年4月，Eidos取得包括Core
Design 和 US Gold 的Centregold
Group并鞏固了其在遊戲工業的地位。其後更先後於1998年和1999年取得[晶体动力和](../Page/晶体动力.md "wikilink")
Pyro
Studios的股權，成為舉足輕重的遊戲商。現時Eidos在全世界已擁有超過500個員工（包括超過250個開發者）以及在附屬公司的一個更進一步的650個開發者的使用。

2009年2月12日，Eidos被[日本](../Page/日本.md "wikilink")[史克威爾艾尼克斯在英国的子公司以](../Page/史克威爾艾尼克斯.md "wikilink")8400万英镑（合1.2亿美元）收购。\[2\]

## 參考文獻

[Eidos](../Category/Eidos.md "wikilink")
[Category:1990年開業電子遊戲公司](../Category/1990年開業電子遊戲公司.md "wikilink")
[Category:2009年結業公司](../Category/2009年結業公司.md "wikilink")
[Category:已結業電子遊戲公司](../Category/已結業電子遊戲公司.md "wikilink")
[Category:英國電子遊戲公司](../Category/英國電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:電子遊戲發行商](../Category/電子遊戲發行商.md "wikilink")
[Category:史克威爾艾尼克斯](../Category/史克威爾艾尼克斯.md "wikilink")

1.  <http://www.eidos-shanghai.cn/>
2.