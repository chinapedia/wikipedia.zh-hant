[黃金博物館.png](https://zh.wikipedia.org/wiki/File:黃金博物館.png "fig:黃金博物館.png")
[黃金博物館-水湳洞選煉廠.jpg](https://zh.wikipedia.org/wiki/File:黃金博物館-水湳洞選煉廠.jpg "fig:黃金博物館-水湳洞選煉廠.jpg")選煉廠\]\]
[Taiwan_2009_JinGuaShi_Historic_Gold_Mine_FRD_8738.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_2009_JinGuaShi_Historic_Gold_Mine_FRD_8738.jpg "fig:Taiwan_2009_JinGuaShi_Historic_Gold_Mine_FRD_8738.jpg")
**新北市立黃金博物館**（又名**黃金博物園區**、**黃金博物館**、**黃金館**）是由過去的[金礦礦區轉型而成的](../Page/金礦.md "wikilink")[博物館區](../Page/博物館.md "wikilink")。位處[台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[瑞芳區](../Page/瑞芳區.md "wikilink")[金瓜石金光路本山五坑旁一帶](../Page/金瓜石.md "wikilink")，原址是昔日[亞洲最大金礦產區](../Page/亞洲.md "wikilink")。黃金博物館位於金瓜石的山城裡，於2004年11月成立，是重新整建舊時的[臺灣金屬鑛業股份有限公司辦公室而成](../Page/臺灣金屬鑛業股份有限公司.md "wikilink")。

## 歷史

最初在此地發現的金礦[露頭](../Page/露頭.md "wikilink")（outcrop）有二處，分別是[大金瓜與](../Page/大金瓜.md "wikilink")[小金瓜](../Page/小金瓜.md "wikilink")。隨後在1890年（清光緒16年），時任[台灣巡撫的](../Page/台灣巡撫.md "wikilink")[劉銘傳帶領工人修建](../Page/劉銘傳.md "wikilink")[鐵道時](../Page/鐵道.md "wikilink")，在河床意外發現[金砂](../Page/金砂.md "wikilink")，溯河而上至金瓜石遂發現金礦脈，自此[清政府設礦業局開採後](../Page/清朝.md "wikilink")，開啟了金瓜石的採金工業。

之後此地歷經[日治時期](../Page/台灣日治時期.md "wikilink")，[日本政府引進機器設備](../Page/大日本帝國.md "wikilink")，一度讓金瓜石的產金量躍升亞洲第一。

到了[中華民國時期](../Page/臺灣戰後時期.md "wikilink")，由[臺灣金屬鑛業股份有限公司](../Page/臺灣金屬鑛業股份有限公司.md "wikilink")（簡稱**臺金公司**）經營的時代，金瓜石仍是台灣[黃金採礦業的重要產地](../Page/黃金.md "wikilink")；而在產金之外，銅礦也是重要的產品之一。然而隨著金、銅等[貴金屬的竭盡](../Page/貴金屬.md "wikilink")，1978年後金礦品質開始下降，於是先是轉為大規模機器露天開採來取代傳統坑道開採，1987年後則廢止採礦，先後交由[台灣電力公司與](../Page/台灣電力公司.md "wikilink")[台灣糖業公司管理](../Page/台灣糖業公司.md "wikilink")，山城也漸漸荒蕪。後來[台北縣政府](../Page/台北縣政府.md "wikilink")（今[新北市政府](../Page/新北市政府.md "wikilink")）為了重振台灣珍貴的產金歷史，自2002年起將原臺灣金屬鑛業股份有限公司辦公室重新整建，整建廢礦坑、臺金公司辦公室等建築，設立**黃金博物園區**，並於2004年11月4日正式開園，成為台灣首座以生態博物館為理念的博物園區，期望藉此紀錄下金瓜石的昔日風光。

## 樓層

  - 一樓：以[九份](../Page/九份.md "wikilink")、金瓜石地區的[採礦](../Page/採礦.md "wikilink")[歷史和相關礦業文物為展覽主題](../Page/歷史.md "wikilink")。
  - 二樓：由歷史、[文化](../Page/文化.md "wikilink")、[藝術](../Page/藝術.md "wikilink")、[科學等多方面地來探討](../Page/科學.md "wikilink")[黃金的](../Page/黃金.md "wikilink")[價值](../Page/價值.md "wikilink")。
  - 三樓：露天式的[淘金體驗區](../Page/淘金.md "wikilink")。

## 山系

位於「金九」（[金瓜石與](../Page/金瓜石.md "wikilink")[九份](../Page/九份.md "wikilink")）交界地的山岳為[基隆山](../Page/基隆山.md "wikilink")，又稱「大肚美人山」，因形似仰躺的大肚子美人而得名。

而黃金博物園區一帶的山系則與[基隆山相望](../Page/基隆山.md "wikilink")。主要[礦坑區所在山岳稱為](../Page/礦坑.md "wikilink")**本山**。而往東北有座山岳，頂上砂岩、頁岩結構從園區角度看去貌似無[提耳的](../Page/提耳.md "wikilink")[茶壺](../Page/茶壺.md "wikilink")，故名[無耳茶壺山](../Page/無耳茶壺山.md "wikilink")；而從靠近北濱的[水湳洞一帶看去則似俯臥的獅子](../Page/水湳洞.md "wikilink")，故又稱「獅子岩山」。[無耳茶壺山的東面則有](../Page/無耳茶壺山.md "wikilink")[半平山](../Page/半平山.md "wikilink")（半屏山）。眾多山系上多為草原，由芒草或蕨類植物交雜而成，有些背對[東北季風的](../Page/東北季風.md "wikilink")[背風面則有樹林的生長](../Page/背風面.md "wikilink")。

## 園區建築與古蹟

  - 環境館
  - [太子賓館](../Page/太子賓館.md "wikilink")：1923年啟用，1945年10月25日廢止，2004年11月修復。
  - 黃金博物館
  - 本山五坑坑道體驗
  - 煉金樓
  - 生活美學體驗坊
  - [金瓜石社](../Page/金瓜石社.md "wikilink")：1897年10月啟用，1933年遷建，1945年10月25日廢止，1950年毀損。
  - 金瓜石派出所：1933年設置，1983年改建，2004年11月修復。
  - 金瓜石郵局
  - 金礦寓所（修復中）
  - 金瓜石車站
  - 時雨中學

<File:新北市立黃金博物館FUJI6176.JPG>| 新北市立黃金博物館 Image:黃金博物館-水湳洞選煉廠.jpg|水湳洞選煉廠
Image:黃金博物館.jpg|太子賓館 Image:Taiwan 2009 JinGuaShi Historic Gold Mine
Abandoned Facilities FRD 7545.jpg|金礦遺址 Image:Taiwan 2009 JinGuaShi
Historic Gold Mine Fingerpost at Main Square FRD 7476.jpg Image:Taiwan
2009 JinGuaShi Historic Gold Mine Outdoor Cafe FRD 7494.jpg Image:Taiwan
2009 JinGuaShi Historic Gold Mine Abadoned Railway Tracks FRD 8753.jpg

## 生態

### 植被

  - [芒草原](../Page/芒草.md "wikilink")
  - 芒其原與[栗厥原](../Page/栗厥.md "wikilink")
  - [鐘萼木](../Page/鐘萼木.md "wikilink")：[第三紀](../Page/第三紀.md "wikilink")[冰河](../Page/冰河.md "wikilink")[孑遺植物](../Page/孑遺植物.md "wikilink")，[清明節開粉色花](../Page/清明節.md "wikilink")。
  - [山櫻花](../Page/山櫻.md "wikilink")
  - [金花石蒜](../Page/金花石蒜.md "wikilink")
  - [台灣百合](../Page/台灣百合.md "wikilink")

## 相關條目

  - [九份](../Page/九份.md "wikilink")
  - [金瓜石](../Page/金瓜石.md "wikilink")
  - [金瓜石社](../Page/金瓜石社.md "wikilink")
  - [金瓜石戰俘營](../Page/金瓜石戰俘營.md "wikilink")
  - [金瓜石勸濟堂](../Page/金瓜石勸濟堂.md "wikilink")
  - [陰陽海](../Page/陰陽海.md "wikilink")
  - [基隆山](../Page/基隆山.md "wikilink")
  - [水湳洞](../Page/水湳洞.md "wikilink")

## 參考文獻

  - 書目

<!-- end list -->

  - 余炳盛 :《九份的前世今生》, 北縣文化 81 民93.06 頁33-37
  - [金瓜石的再生與懷舊](http://teacher.yuntech.edu.tw/yangyf/topre/204leo1.html)

<!-- end list -->

  - 引用

## 外部連結

  - [新北市立黃金博物館](http://www.gep.ntpc.gov.tw/)

  -
  - [流金歲月 -
    協和工商資料處理科理仁甲班](http://210.71.63.1/dp986a/work/oneweb/MOON/stone1.html)

[H](../Page/category:新北市博物館.md "wikilink")

[Category:珠宝博物馆](../Category/珠宝博物馆.md "wikilink")
[H](../Category/礦業博物館.md "wikilink")
[H](../Category/基隆北海岸景點.md "wikilink")
[Category:2004年台灣建立](../Category/2004年台灣建立.md "wikilink")
[Category:文建會歷史建築百景](../Category/文建會歷史建築百景.md "wikilink")
[Category:瑞芳區](../Category/瑞芳區.md "wikilink")
[Category:新北市博物館](../Category/新北市博物館.md "wikilink")
[Category:臺灣礦業](../Category/臺灣礦業.md "wikilink")
[Category:金](../Category/金.md "wikilink")