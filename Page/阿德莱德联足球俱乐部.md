**阿德莱德联足球俱乐部**（**Adelaide United Football
Club**）是[澳洲](../Page/澳洲.md "wikilink")[阿得雷德的一个俱乐部](../Page/阿得雷德.md "wikilink")，是[南澳大利亞州唯一一支参加](../Page/南澳大利亞州.md "wikilink")[澳聯的球队](../Page/澳洲職業足球聯賽.md "wikilink")。在2003年阿德莱德市队倒闭了，自此阿德莱德再沒有全国足球联赛的球隊，因此[南澳足球联会创建了这队](../Page/南澳足球联会.md "wikilink")。主场是[海馬許體育場](../Page/海馬許體育場.md "wikilink")。

俱乐部同时拥有阿德莱德联队女子足球队，参加[澳洲女子足球聯賽](../Page/澳洲女子足球聯賽.md "wikilink")。

## 历史

### 全国足球联赛

2003年8月，[阿德莱德城足球俱乐部退出了](../Page/阿德莱德城足球俱乐部.md "wikilink")[全国足球联赛](../Page/全国足球联赛.md "wikilink")，这是自该联赛1977年举办以来[阿德莱德球队第一次缺席比赛](../Page/阿德莱德.md "wikilink")。因此阿德莱德联在历经几周的艰辛努力后于2003年9月成立。当年10月，阿德莱德联在超过
16,000 观众的球场上迎来了在NSL的第一场比赛，并以 1–0 打败对手[Brisbane
Strikers](../Page/Brisbane_Strikers.md "wikilink")。在接下来包括2003年11–12月间连续
7 场不败的赛季中，阿德莱德联打入了preliminary
final，最后被[珀斯光荣击败](../Page/珀斯光荣.md "wikilink")。而全国足球联赛也在赛季结束后戛然而止。

### 澳超

阿德莱德联成为新创建的[澳大利亚足球职业联赛第一个赛季的八个俱乐部之一](../Page/澳大利亚足球职业联赛.md "wikilink")，此外还有在NSL取消后幸存下来的珀斯光荣和[纽卡斯尔联喷气机](../Page/纽卡斯尔联喷气机.md "wikilink")。阿德莱德联早于大部分俱乐部准备，并在2005年2月前宣布了20人大名单中的三分之二球员。俱乐部聚集了一些阿德莱德联和[南澳大利亚的本土球员如](../Page/南澳大利亚.md "wikilink")[安杰洛·科斯坦索](../Page/安杰洛·科斯坦索.md "wikilink")，[特拉维斯·多德和](../Page/特拉维斯·多德.md "wikilink")[卢卡斯·潘泰利斯等曾代表阿德莱德城参加NSL的球员](../Page/卢卡斯·潘泰利斯.md "wikilink")，并在2005年3月从[上海申花租借外援](../Page/上海申花.md "wikilink")[曲圣卿](../Page/曲圣卿.md "wikilink")。

#### 2008–09年赛季

在 2008–09
球季，阿德莱德联签下了不少重要球员以增强球队实力。这其中最著名的是与来自[蒂爾堡威廉二世的前锋](../Page/蒂爾堡威廉二世.md "wikilink")[Cristiano签下了两年合约](../Page/Cristiano_dos_Santos_Rodrigues.md "wikilink")。而引入原效力于[CA
Juventus的](../Page/CA_Juventus.md "wikilink")[艾佛遜·艾蘭蒂斯·迪·奧利華拉以填补](../Page/艾佛遜·艾蘭蒂斯·迪·奧利華拉.md "wikilink")[Richie
Alagich](../Page/Richie_Alagich.md "wikilink")
退役后左路的真空也十分重要。来自[布里斯班獅吼足球俱樂部的](../Page/布里斯班獅吼足球俱樂部.md "wikilink")[Saša
Ognenovski增强了后防](../Page/Saša_Ognenovski.md "wikilink")，来自[布莱顿足球俱乐部的中场球员](../Page/布莱顿足球俱乐部.md "wikilink")[Paul
Reid以及一些出众的年轻球员](../Page/Paul_Reid_\(Australian_footballer\).md "wikilink")（
[Scott Jamieson](../Page/Scott_Jamieson.md "wikilink")，Daniel
Mullen，[马克·比里吉蒂和](../Page/马克·比里吉蒂.md "wikilink")[Robert
Younis](../Page/Robert_Younis.md "wikilink")）也与球队签约。而
[卜比·比達](../Page/卜比·比達.md "wikilink"), Shaun Ontong,
[米兰·苏萨克](../Page/米兰·苏萨克.md "wikilink"), [Dez
Giraldi](../Page/Dez_Giraldi.md "wikilink") and [Robert
Bajic](../Page/Robert_Bajic.md "wikilink") were all released； [Bruce
Djite以创下澳超记录的](../Page/Bruce_Djite.md "wikilink")850,000[澳元转会至](../Page/澳元.md "wikilink")[Genclerbirligi](../Page/Genclerbirligi.md "wikilink")。同时[Nathan
Burns以](../Page/Nathan_Burns.md "wikilink")500,000澳元转入
[AEK雅典](../Page/AEK雅典足球會.md "wikilink")，实现了他的欧洲梦。\[1\]\[2\]另外Kristian
Sarkies，Lucas
Pantelis和[法比安·巴尔别罗更新了与俱乐部的合约](../Page/法比安·巴尔别罗.md "wikilink")。

2008–09赛季是阿德莱德联队成绩最好的一年，不仅在[澳超和](../Page/澳超.md "wikilink")[亚冠都打进总决赛](../Page/亚冠.md "wikilink")，还参加了在[日本举办的](../Page/日本.md "wikilink")[国际足联俱乐部世界杯并最终获得第五名及公平竞赛奖](../Page/国际足联俱乐部世界杯.md "wikilink")。\[3\]

## 现役球员

\[4\]

## 曾经效力的球员

  -  **澳大利亚**

<!-- end list -->

  - [保罗·阿戈斯蒂诺](../Page/保罗·阿戈斯蒂诺.md "wikilink")
  - [罗斯·阿卢瓦西](../Page/罗斯·阿卢瓦西.md "wikilink")
  - [Louis Brain](../Page/Louis_Brain.md "wikilink")
  - [Chad Bugeja](../Page/Chad_Bugeja.md "wikilink")
  - [纳丹·伯恩斯](../Page/纳丹·伯恩斯.md "wikilink")
  - [安杰洛·科斯坦索](../Page/安杰洛·科斯坦索.md "wikilink")
  - [尼克·克罗斯利](../Page/尼克·克罗斯利.md "wikilink")
  - [Bruce Djite](../Page/Bruce_Djite.md "wikilink")
  - [Dez Giraldi](../Page/Dez_Giraldi.md "wikilink")
  - [阿龙·古尔丁](../Page/阿龙·古尔丁.md "wikilink")
  - [斯科特·贾米森](../Page/斯科特·贾米森.md "wikilink")
  - [马修·肯普](../Page/马修·肯普.md "wikilink")
  - [Goran Lozanovski](../Page/Goran_Lozanovski.md "wikilink")
  - [迈克尔·马罗内](../Page/迈克尔·马罗内_\(足球运动员\).md "wikilink")
  - [迈克尔·马特里恰尼](../Page/迈克尔·马特里恰尼.md "wikilink")
  - [Tomislav Milardovic](../Page/Tomislav_Milardovic.md "wikilink")

<!-- end list -->

  - [莎莎·奥格内诺夫斯基](../Page/莎莎·奥格内诺夫斯基.md "wikilink")
  - [Shaun Ontong](../Page/Shaun_Ontong.md "wikilink")
  - [格雷格·欧文斯](../Page/格雷格·欧文斯.md "wikilink")
  - [阿德里亚诺·佩莱格里诺](../Page/阿德里亚诺·佩莱格里诺.md "wikilink")
  - [克里斯蒂安·里斯](../Page/克里斯蒂安·里斯.md "wikilink")
  - [马克·鲁丹](../Page/马克·鲁丹.md "wikilink")
  - [乔纳斯·萨丽](../Page/乔纳斯·萨丽.md "wikilink")
  - [Mimi Saric](../Page/Mimi_Saric.md "wikilink")
  - [Kristian Sarkies](../Page/Kristian_Sarkies.md "wikilink")
  - [贾森·斯帕格纳洛](../Page/贾森·斯帕格纳洛.md "wikilink")
  - [米兰·舒沙克](../Page/米兰·舒沙克.md "wikilink")
  - [Michael Valkanis](../Page/Michael_Valkanis.md "wikilink")
  - [Adam van Dommele](../Page/Adam_van_Dommele.md "wikilink")
  - [Carl Veart](../Page/Carl_Veart.md "wikilink")
  - [奥雷利奥·维德马尔](../Page/奥雷利奥·维德马尔.md "wikilink")
  - [亚当·格里菲斯](../Page/亚当·格里菲斯.md "wikilink")
  - [马修·莱基](../Page/马修·莱基.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [Alemão](../Page/埃弗森·阿兰特斯·德·奥利维拉.md "wikilink")
  - [克里斯蒂亚诺](../Page/克里斯蒂亚诺·罗德里格多斯·桑托斯.md "wikilink")
  - [费兰度·列特](../Page/费兰度·列特.md "wikilink")
  - [罗马里奥](../Page/罗马里奥.md "wikilink")
  - [迭戈·沃尔什](../Page/迭戈·沃尔什.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [曲圣卿](../Page/曲圣卿.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [沙恩·施梅尔茨](../Page/沙恩·施梅尔茨.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [鲍比·佩塔](../Page/鲍比·佩塔.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [申恩顺](../Page/申恩顺.md "wikilink")

## 教练组

*截止2010年6月5日*

<table>
<thead>
<tr class="header">
<th><p>职务</p></th>
<th><p>姓名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>经理 (总教练)</p></td>
<td><p><a href="../Page/Rini_Coolen.md" title="wikilink">Rini Coolen</a></p></td>
</tr>
<tr class="even">
<td><p>助理教练</p></td>
<td><p><a href="../Page/菲尔·斯塔宾斯.md" title="wikilink">菲尔·斯塔宾斯</a></p></td>
</tr>
<tr class="odd">
<td><p>专家教练</p></td>
<td><p><a href="../Page/卡尔·维尔特.md" title="wikilink">卡尔·维尔特</a></p></td>
</tr>
<tr class="even">
<td><p>守门员教练</p></td>
<td><p><a href="../Page/彼得·布拉任契奇.md" title="wikilink">彼得·布拉任契奇</a></p></td>
</tr>
<tr class="odd">
<td><p>青年队教练</p></td>
<td><p><a href="../Page/乔·马伦_(足球运动员).md" title="wikilink">乔·马伦</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 经理

*截止2010年6月5日*

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>任职期间</p></th>
<th><p>执教场次</p></th>
<th><p>胜</p></th>
<th><p>平</p></th>
<th><p>负</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/约翰·科斯米纳.md" title="wikilink">约翰·科斯米纳</a></p></td>
<td><p>2003年9月 - 2007年2月</p></td>
<td><p>86</p></td>
<td><p>38</p></td>
<td><p>25</p></td>
<td><p>23</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥雷利奥·维德马尔.md" title="wikilink">奥雷利奥·维德马尔</a></p></td>
<td><p>2007年2月 - 2010年6月</p></td>
<td><p>107</p></td>
<td><p>42</p></td>
<td><p>23</p></td>
<td><p>42</p></td>
</tr>
<tr class="odd">
<td><p>→  菲尔·斯塔宾斯 (临时)</p></td>
<td><p>2010年6月</p></td>
<td><p>0</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Rini_Coolen.md" title="wikilink">Rini Coolen</a></p></td>
<td><p>2010年6月 - <em>至今</em></p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
</tr>
</tbody>
</table>

## 荣誉

**国内**

  - '''[澳超常规赛](../Page/澳大利亚职业足球联赛历届冠军#常规赛.md "wikilink"):
      - **冠军** (1): 2005-06
      - **亚军** (2): 2006-07, 2008–09
  - '''[澳超决赛](../Page/澳大利亚职业足球联赛历届冠军#决赛.md "wikilink"):
      - **亚军** (2): 2006-07, 2008–09
  - '''[澳超季前挑战杯](../Page/澳大利亚职业足球联赛历届冠军#季前挑战杯.md "wikilink"):
      - **冠军** (2): 2006, 2007
  - '''[澳大利亚职业足球国家青年联赛](../Page/澳大利亚职业足球国家青年联赛.md "wikilink"):
      - **亚军** (1): 2008-09


**国际**

  - **[潍坊杯（U18）](../Page/潍坊杯U18国际足球锦标赛.md "wikilink"):**
      - **第六名** (1): 2011年
  - **[亚冠联赛](../Page/亚冠联赛.md "wikilink"):**
      - **小组赛** (1): 2007年
      - **亚军** (1): 2008年
  - **[世俱杯](../Page/世俱杯.md "wikilink"):**
      - **第五名** (1): 2008年

## 比赛记表

|     | 场次 | 胜  | 平 | 负 | 入球 | 失球 | 积分  | 位 | 班  | 平均观众(主场) |
| --- | -- | -- | - | - | -- | -- | --- | - | -- | -------- |
| 分組賽 | 24 | 11 | 7 | 6 | 28 | 25 | 40  | 3 | 13 | 12614    |
| 淘汰赛 | 4  | 2  | 0 | 2 | 6  | 10 | N/A | 3 | 6  | 16408    |

**2003-04年全国足球联赛 比赛记表**

|     | 场次 | 胜  | 平 | 负 | 入球 | 失球 | 积分  | 位 | 班 | 平均观众(主场) |
| --- | -- | -- | - | - | -- | -- | --- | - | - | -------- |
| 分組賽 | 21 | 13 | 4 | 4 | 33 | 25 | 43  | 1 | 8 | 10947    |
| 淘汰赛 | 3  | 0  | 1 | 2 | 3  | 5  | N/A | 3 | 4 | 15104    |

**2005-06年甲级联赛 比赛记表**

## 参考资料

## 外部連結

  - [adelaideunited.com.au](http://www.adelaideunited.com.au/) - 球队网页
  - [Hyundai A-League](http://www.a-league.com.au/) - 联赛网页

[Category:澳洲足球俱樂部](../Category/澳洲足球俱樂部.md "wikilink")
[Category:阿德萊德聯](../Category/阿德萊德聯.md "wikilink")
[Category:2003年建立的足球俱樂部](../Category/2003年建立的足球俱樂部.md "wikilink")
[Category:2003年澳大利亞建立](../Category/2003年澳大利亞建立.md "wikilink")

1.  [Burns is Athens
    bound](http://au.fourfourtwo.com/news/77984,burns-is-athens-bound--official.aspx?r=rss)*Retrieved
    on 11 June 2008*
2.  "Rivaldo Inspires Burns", Adelaide Advertiser, Friday 13 June 2008,
    p118
3.
4.  <http://www.adelaideunited.com.au/team/a_league>