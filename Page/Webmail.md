**Webmail**（**基于[万维网的电子邮件服务](../Page/万维网.md "wikilink")**）是[因特网上一种主要使用](../Page/因特网.md "wikilink")[网页浏览器来阅读或发送](../Page/网页浏览器.md "wikilink")[电子邮件的服务](../Page/电子邮件.md "wikilink")，与使用[Microsoft](../Page/Microsoft.md "wikilink")
Outlook.com、[Mozilla
Thunderbird等电子邮件客户端软件的电子邮件服务相对](../Page/Mozilla_Thunderbird.md "wikilink")。

因特网上的许多公司，诸如[Google](../Page/Google.md "wikilink")、[雅虎](../Page/雅虎.md "wikilink")、中国的[新浪和](../Page/新浪.md "wikilink")[网易等](../Page/网易.md "wikilink")，都提供有webmail服务。此外，个人的[因特网服务提供商也可能提供webmail](../Page/因特网服务提供商.md "wikilink")，这样他们的用户可以不必通过他们一般使用的上网终端而直接使用他们的邮件服务。

## 历史

世界上第一个webmail服务是[杰克·史密斯和](../Page/杰克·史密斯.md "wikilink")[印度的](../Page/印度.md "wikilink")[沙比尔·巴蒂亚](../Page/沙比尔·巴蒂亚.md "wikilink")（Sabeer
Bhatia）一起创办的[Hotmail](../Page/Hotmail.md "wikilink")。后来[微软将Hotmail买下并将其与](../Page/微软.md "wikilink")[Outlook.com服務相结合](../Page/Outlook.com.md "wikilink")。而有提供電子郵件信箱的-{zh-hans:[门户网站](../Page/门户网站.md "wikilink");
zh-hant:[入口網站](../Page/入口網站.md "wikilink");}-及[ISP也大多提供了webmail供用戶使用](../Page/ISP.md "wikilink")。

一些[开放源代码的程序允许用户建立自己的webmail](../Page/开放源代码.md "wikilink")。

## 特色

多数webmail具有下列特色：

  - 文件夾
  - 不同的邮件分入不同的文件夹
  - 垃圾筒
  - 地址簿

不少webmail服务还同時具有下列特征：

  - [垃圾邮件检查](../Page/垃圾邮件.md "wikilink")
  - [POP3收取其他電子信箱](../Page/POP3.md "wikilink")
  - 检查附件中的[电脑病毒](../Page/电脑病毒.md "wikilink")
  - 字典（词典）功能
  - 拼写错误检查

## 优点和缺点

### 优点

  - 用户可以在任何连接至互联网且拥有网页浏览器的地方读取和发送电子邮件，而不必使用特定的客户端软件
  - 邮件不必被下载
  - 许多公司提供匿名服务

### 缺点

  - 用户若没有连接至互联网，就无法使用webmail，即使是读取从前的邮件也无法进行
  - 一般webmail的存储量有限
  - 用户可能必须接受界面中的网页[广告](../Page/广告.md "wikilink")
  - 若网络速度较慢，webmail很难使用
  - 一般电子邮件都比较短，但webmail由于要在原来的邮件基础上新增[HTML的指令和结构](../Page/HTML.md "wikilink")，所以会让邮件体积变大

## 其他特征

  - **匿名性**

Webmail拥有一定程度的匿名性。一般来说国家安全机构可以比较容易地确定谁在使用webmail服务，但私人一般比较难确定使用webmail的到底是谁。

## Webmail伺服軟體舉隅

  - [OpenWebmail](../Page/OpenWebmail.md "wikilink")
  - [Mail2000](../Page/Mail2000.md "wikilink")

[Category:电子邮件网站](../Category/电子邮件网站.md "wikilink")