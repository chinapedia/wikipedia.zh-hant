**鄭曼青**（），[浙江](../Page/浙江.md "wikilink")[温州](../Page/温州.md "wikilink")[永嘉人](../Page/永嘉.md "wikilink")，現代著名中醫師、畫家，[楊澄甫弟子](../Page/楊澄甫.md "wikilink")，精於太極拳，為鄭子太極拳的創始人。

## 生平

鄭曼青於[清](../Page/清朝.md "wikilink")[光緒](../Page/光緒.md "wikilink")28年壬寅[農曆](../Page/農曆.md "wikilink")6月25日出生（1902年7月29日\[1\]\[2\]），逝世於1975年3月14日\[3\]。文化水準很高，詩書傳家，年少曾投名儒[錢名山門下](../Page/錢名山.md "wikilink")，並非傳統的武人，有「五絕老人」的稱譽，五絕所指的是[詩](../Page/詩.md "wikilink")、[書](../Page/書法.md "wikilink")、[畫](../Page/畫.md "wikilink")、[中醫和](../Page/中醫.md "wikilink")[太極拳等](../Page/太極拳.md "wikilink")5種絕藝。曾出版英文太極拳著作，而他以[哲學](../Page/哲學.md "wikilink")、[科學](../Page/科學.md "wikilink")、[醫學的角度來闡釋太極拳](../Page/醫學.md "wikilink")，開風氣之先，令人耳目一新。

鄭氏27歲在[上海向](../Page/上海.md "wikilink")[太極拳大師](../Page/太極拳.md "wikilink")[楊澄甫學習](../Page/楊澄甫.md "wikilink")[楊家老架太極拳](../Page/楊氏太極拳.md "wikilink")，任職[湖南省政府咨議兼國術館館長期間](../Page/湖南省.md "wikilink")，推動國術為該省全民運動，並規定每兩個月調派全省各縣國術館長及教官40人，傳授太極拳課程。但因學習時間較短，為便於傳授學習，乃刪減老架的重覆招式，精簡為37式，名為：「[鄭子簡易太極拳](../Page/鄭子太極拳.md "wikilink")」。

1941年與丁慕韓之女丁惟莊女士結婚，育子女 6 人。

48歲攜眷來[台灣](../Page/台灣.md "wikilink")，應當時[台北市長](../Page/台北市長.md "wikilink")[游彌堅先生的邀請](../Page/游彌堅.md "wikilink")，創設時中學社於[台北市](../Page/台北市.md "wikilink")[中山堂頂樓](../Page/中山堂.md "wikilink")，後遷至台北市仁愛國小，於2002年末遷至台北市五常國中，並曾任國立藝術學校（今[國立臺灣藝術大學](../Page/國立臺灣藝術大學.md "wikilink")）的教授。

## 用藥風格

鄭曼青擅長內外科，處方以**「劑大量多」**為其特色。為兼顧後天胃氣與先天之原氣，喜歡重用於白朮、骨碎補，每用至1－2兩，又喜用木蝴蝶以清降龍雷上升之火。

## 左家心法

根據鄭曼青弟子吳國忠的說法，鄭曼青曾與山西道士**左萊蓬**學習道家內功，稱為**左家內功心法**。這套內功，後來傳授給羅邦楨與吳國忠。但是這個說法並沒有被其他弟子接受。

## 著作

  - 《鄭子太極拳自修新法》
  - 《太極十三篇》
  - 《鄭子太極拳劍專輯》
  - 《曼冉三論》
  - 《易全》
  - 《老子易知解》
  - 《中華醫藥學史》

## 相關連結

  - [楊氏太極拳](../Page/楊氏太極拳.md "wikilink")
  - [鄭子太極拳](../Page/鄭子太極拳.md "wikilink")

## 参考來源

  - 文献

<!-- end list -->

  -
<!-- end list -->

  - 引用

<div class="references-small">

<references />

</div>

## 外部連結

  - [鄭子太極拳研究會](http://www.37TaiChi.org.tw)（時中學社）

  - [財團法人中華太極館](http://www.suppletaichi.org)（鬆柔太極）

  - [Cheng Man-ch'ing Biography
    Project](http://www.chengbiography.blogspot.com)（「鄭曼青傳」計劃）

  - [台北市薪傳鄭子太極拳協會](http://www.taichi37.org/index.htm)

  - [楊氏鄭子太極拳有緣拳社](http://blog.udn.com/Chengtaichi)

  - [鱼太极新浪博客](http://blog.sina.com.cn/chengtaichi)

  - [如魚太極學院](http://medianchen.myweb.hinet.net/p2.htm)

[Category:太極拳](../Category/太極拳.md "wikilink")
[Category:中華民國武術家](../Category/中華民國武術家.md "wikilink")
[Category:近現代武術家](../Category/近現代武術家.md "wikilink")
[Category:台灣武術家](../Category/台灣武術家.md "wikilink")
[Category:台灣中醫師](../Category/台灣中醫師.md "wikilink")
[Category:中華民國大陸時期中醫學家](../Category/中華民國大陸時期中醫學家.md "wikilink")
[Category:中華民國書法家](../Category/中華民國書法家.md "wikilink")
[Category:制憲國民大會代表](../Category/制憲國民大會代表.md "wikilink")
[Category:台灣戰後浙江移民](../Category/台灣戰後浙江移民.md "wikilink")
[Z](../Category/温州人.md "wikilink") [M](../Category/鄭姓.md "wikilink")
[Category:中華民國畫家](../Category/中華民國畫家.md "wikilink")

1.  [清德宗光緒28年6月](http://140.112.30.230/datemap/content.php?ChQueryText=%E6%B8%85%E5%BE%B7%E5%AE%97%E5%85%89%E7%B7%9228%E5%B9%B46%E6%9C%884%E6%97%A5&coreg=on)，[國立臺灣大學數位典藏與自動推論實驗室](../Page/國立臺灣大學.md "wikilink")
    數位典藏研究發展中心
2.  [清德宗光緒28年6月](http://authority.ddbc.edu.tw/time/search.php?chk=on&julianSwitch=on&strict=1&filterType=b:0;&dpk=%E6%B8%85&epk=%E5%BE%B7%E5%AE%97&yct=28&mpk=%E5%85%AD&ypk=%E5%85%89%E7%B7%92&startFrom=1)，[時間規範資料庫](http://authority.ddbc.edu.tw/time/)，佛學規範資料庫，[法鼓文理學院](../Page/法鼓文理學院.md "wikilink")
3.  [時中學社：《宗師生平》](http://www.37taichi.org.tw/totalFrameset-44.html)