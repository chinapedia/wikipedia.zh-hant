[Quakecon_logo.png](https://zh.wikipedia.org/wiki/File:Quakecon_logo.png "fig:Quakecon_logo.png")
**Quakecon**是一个[BYOC](../Page/BYOC.md "wikilink")（自己帶自己的電腦）性质的[电脑游戏赛事](../Page/电脑游戏.md "wikilink")，每年举办一次。Quakecon的名字是用[id
Software的游戏](../Page/id_Software.md "wikilink")[雷神之锤命名的](../Page/雷神之锤.md "wikilink")，每年举办的时候，各地的[玩家都会汇聚到赛场庆祝id的成功](../Page/玩家.md "wikilink")。它是[北美最大的局域网赛事](../Page/北美.md "wikilink")，也是世界上最大的免费局域网赛事，一般会持续四天。

现在Quakecon已经不仅仅是一个游戏赛事，更是一个各大公司展示最新游戏和硬件的机遇，一般这些公司都和[id
Software及其发行商](../Page/id_Software.md "wikilink")[Activision有关系](../Page/Activision.md "wikilink")，比如销售商、游戏外围设备供应商和软件供应商。

Quakecon和其他赛事最核心的不同点就是所有的工作人员都是义务参与工作的。每年赛事的策划、建立和组织都由一个小组领导，通常有上千人在为赛事工作，他们在一周之内的工作量相当于两年的人力。所以Quakecon经常被称为是“游戏界的[伍德斯托克节](../Page/伍德斯托克节.md "wikilink")”，或者“和平、友爱和战斗”的一周！

## 由来

Quakecon最初是由一群活跃在[EFnet的](../Page/EFnet.md "wikilink")[IRC](../Page/IRC.md "wikilink")-[Quake频道上面的玩家所组织的](../Page/Quake.md "wikilink")。很多到过这个频道的人表达了希望能聚集在一起进行游戏的愿望，所以一个来自[美国](../Page/美国.md "wikilink")[德克萨斯州](../Page/德克萨斯.md "wikilink")[达拉斯的玩家吉米](../Page/达拉斯.md "wikilink")·艾尔森（Jim
Elson，游戏id为“H2H”）组织了一个本地的达拉斯区域的游戏社群。一名来自[加拿大](../Page/加拿大.md "wikilink")[安大略湖](../Page/安大略湖.md "wikilink")[滑铁卢的电脑商约萨里安](../Page/滑铁卢.md "wikilink")·霍姆博格（“Yossarian
Holmberg，游戏id“Yossman”），最终在一个旅馆里面组织起了这次活动。最初的赛事名字就是“\#quakecon”，那个频道的名字，后来慢慢演化成了“Quakecon”。前两年的的赛事，艾尔森完成了组织方面的大多数工作，直到一批玩家志愿的参加到组织工作中来，成批的志愿者分成不同的小组来协助设置赛事和排除问题，以及调试赛事所用的设备和网络。

## 过去的赛事

**第一次赛事**，是在1996年8月，在位于[德克萨斯](../Page/德克萨斯.md "wikilink")[加兰市举办的](../Page/加兰市.md "wikilink")。举办地点离[id
Software的办公室只有](../Page/id_Software.md "wikilink")1到2英里。当时只有30个人参加了这次聚会，到结束的时候增加到大概100人。聚会的消息不久就被放到了网上。

来参加聚会的玩家都带上了自己的电脑，他们一起在旅馆的房间内搭建起了一个小型网络，一起进行[雷神之锤I和](../Page/雷神之锤I.md "wikilink")[毁灭战士的游戏](../Page/毁灭战士.md "wikilink")，比试自己的[死亡竞赛的能力](../Page/死亡竞赛.md "wikilink")。甚至还出现了一个小型的赛事，最后胜者可以拿到一件[T恤作为奖品](../Page/T恤.md "wikilink")。

最后一天快结束时候才是整个赛事的亮点，当时[id
Software的全体员工出现在了聚会中](../Page/id_Software.md "wikilink")，这让玩家们惊喜不已，id被热烈的欢迎进赛场，大家不停和他们交流。[卡马克](../Page/卡马克.md "wikilink")（约翰·卡马克），id的头牌程序员，编写了[毁灭战士和](../Page/毁灭战士.md "wikilink")[雷神之锤的天才](../Page/雷神之锤.md "wikilink")，参加了一个半个小时到45分钟左右的和玩家交流的座谈会。大家给了他一些意见，最后这些意见变成了[雷神之锤未来的更新](../Page/雷神之锤.md "wikilink")。这次的“卡马克座谈”已经变成了每年Quakecon上面的保留节目。

### Qcon 1997

**Quakecon
1997**最终的参加人数大约有650人，聚会地点选在了[达拉斯](../Page/达拉斯.md "wikilink")[普莱诺的](../Page/普莱诺.md "wikilink")[假日旅店](../Page/假日旅店.md "wikilink")。网络和媒体都对这次赛事进行了大量的报道，id和Activision也对比赛进行了一些赞助。

### Qcon 1998

**Quakecon
1998**是id的玩家们和[CPL一同协力举办起来的](../Page/CPL.md "wikilink")。因为计划不周全和缺少那些帮助前两届赛事成功的玩家的参与，Qcon
98被认为是一次不太成功的比赛。但是这也促成了玩家们组成了一个小组全力策划Quakecon的比赛。

  - 参与人数：800
  - BYOC规模：300
  - 地点：美国，[德克萨斯](../Page/德克萨斯.md "wikilink")，[达拉斯](../Page/达拉斯.md "wikilink")，[Infomart](../Page/Infomart.md "wikilink")

### Qcon 1999

**Quakecon 1999**第一次由id公司参与组织。因为意识到需要大型赞助商的介入，两位组织了Qcon 96的元老大卫·米勒（David
Miller，游戏id“Wino”）和保罗·霍洛基（Paul Horoky，游戏id“devilseye”）联系了[id
Software](../Page/id_Software.md "wikilink")，希望他们能够成为赞助商，并请他们帮助自己找到更多的赞助。他们和安娜·康（Anna
Kang，id的雇员，现在是[卡马克的妻子](../Page/卡马克.md "wikilink")）以及一些志愿者，米勒和霍洛基成功的举办了Qcon
99，这次赛事也成为以后几年内各种游戏赛事的范例。

  - 参与人数：1100
  - BYOC规模：500
  - 地点：美国，德克萨斯，麦思奎特，麦思奎特会议中心
  - 亮点：地点的转换让整个赛事焕然一新，并且得到了前所未有的发展，并且id公司当时就在麦思奎特大街上。id的雇员们，包括卡马克，都参与了由玩家们组织的比赛里面。大量的公司对这次的比赛进行了赞助，比如[Activision](../Page/Activision.md "wikilink")、[AMD](../Page/AMD.md "wikilink")、[苹果电脑](../Page/苹果电脑.md "wikilink")、[ATI](../Page/ATI.md "wikilink")、[罗技](../Page/罗技.md "wikilink")、[Linksys和](../Page/Linksys.md "wikilink")[Lucent](../Page/Lucent.md "wikilink")。

### Qcon 2000

  - 参与人数：3000
  - BYOC规模：1250
  - 地点：麦思奎特会议中心
  - 亮点：这年的比赛扩展了规模，可以让众多的卖主体验到在[E3大展上面的感觉](../Page/E3.md "wikilink")。研究发展会首次初夏你，让玩家和开发者们可以面对面的讨论游戏业和社群的发展，并且是在一种轻松的没有任何压力的情况下。

### Qcon 2001

  - 参与人数：3000以上
  - BYOC规模：1250
  - 地点：麦思奎特会议中心

### Qcon 2002

  - 参与人数：3250以上
  - BYOC规模：1300
  - 地点：麦思奎特会议中心
  - 亮点：增加了[重返德军总部](../Page/重返德军总部.md "wikilink")（[Return to Castle
    Wolfenstein](../Page/Return_to_Castle_Wolfenstein.md "wikilink")）的组队比赛。首次举办了非官方性质的女子比赛，也第一次出现了比[布兰妮的影片](../Page/布兰妮.md "wikilink")[十字路口还要华丽的表演](../Page/十字路口.md "wikilink")。

### Qcon 2003

  - 参与人数：4000以上
  - BYOC规模：2000
  - 地点：美国，德克萨斯，达拉斯，亚当·马克旅馆
  - 亮点：自从展会地点搬到麦思奎特后首次搬家，女子比赛成为官方承认的比赛，并且有厂商赞助。id也揭开了[毁灭战士III的面纱](../Page/毁灭战士III.md "wikilink")。

### Qcon 2004

  - 参与人数：5000以上
  - BYOC规模：3000
  - 地点：美国，德克萨斯，Grapevine，Gaylord·Texan 会展中心
  - 亮点：Qcon再次搬家，并且成为了北美最大的局域网赛事，3000多名玩家带来了自己的电脑。比赛的总奖金数超过了15万美元，而且举办了世界上第一次的[毁灭战士III比赛](../Page/毁灭战士III.md "wikilink")，最终Johnnathan
    “[Fatal1ty](../Page/Fatal1ty.md "wikilink")”
    Wedel获得冠军。这次的比赛延续了歌星助阵，来自德克萨斯的MST3K乐团进行了表演，还有[九寸钉乐团以前的鼓手](../Page/九寸钉.md "wikilink")、id的音效师克里斯·弗莱纳（Chris
    Vrenna）。

### Qcon 2005

  - 参与人数：6000以上
  - BYOC规模：3200
  - 地点：美国，德克萨斯，Grapevine，Gaylord·Texan 会展中心
  - 亮点：这次的展示中玩家们会看到[雷神之锤IV的真面目](../Page/雷神之锤IV.md "wikilink")。

### Qcon 2006

  - 参与人数：4000以上
  - BYOC规模：1800
  - 地点：美国，德克萨斯，Hilton Anatole 饭店

### Qcon 2007

  - 参与人数：7000以上（估计）
  - BYOC规模：2700
  - 地点：美国，德克萨斯，Hilton Anatole 饭店

### Qcon 2008

  - 地点：美国，德克萨斯，Hilton Anatole 饭店

## 统计数字

关于Qcon 04，我们可以得到以下数字：

  - 超过200000平方英尺（19000平方米）的BYOC展会
  - BYOC部分利用了超过1200条电源线
  - 网线总数超过80000英尺（24公里）
  - 超过144个交换机
  - 核心系统：使用了748个端口连接了6509个[Cisco系统交换机](../Page/Cisco.md "wikilink")，总监视模块720个，电源输出4000瓦特
  - 这次赛事的工作（电源、网络和其他）是超过500名没有报酬的志愿者在两天时间内完成的。

## 所用游戏

  - [战地风云](../Page/战地风云.md "wikilink")（[Battlefield
    2](../Page/Battlefield_2.md "wikilink")）
  - [使命召唤](../Page/使命召唤.md "wikilink")（[Call of
    Duty](../Page/Call_of_Duty.md "wikilink")）
  - [毁灭战士](../Page/毁灭战士.md "wikilink")（[Doom](../Page/Doom.md "wikilink")）
  - [毁灭战士III](../Page/毁灭战士III.md "wikilink")（[Doom3](../Page/Doom3.md "wikilink")）
  - <s>[雷神之锤III](../Page/雷神之锤III.md "wikilink")（[Quake3](../Page/Quake3.md "wikilink")）</s>
    *已经被取消*
  - [Quake Live](../Page/Quake_Live.md "wikilink")（[Quake
    Live](../Page/Quake_Live.md "wikilink")）
  - [重返德军总部](../Page/重返德军总部.md "wikilink")（[Return to Castle
    Wolfenstein](../Page/Return_to_Castle_Wolfenstein.md "wikilink")）
  - [Wolfenstein: Enemy
    Territory](../Page/Wolfenstein:_Enemy_Territory.md "wikilink")

## 社群

Qcon的社群是组织赛事的主要领导者，他们通过IRC交流。他们使用的主要频道为[\#quakecon](irc://irc.enterthegame.com/quakecon)。在这里你不仅可以和他们聊比赛，更可以和他们谈天说地。

Qcon的网站也是由[Qcon论坛](http://www.quakecon.org/forums)创建的，你可以在论坛中向他们介绍自己，提出关于赛事的问题或者和他们谈谈怎样组织和发展比赛。

## 參看

  - [BYOC](../Page/BYOC.md "wikilink")
  - [LAN party](../Page/LAN_party.md "wikilink")

## 外部链接

  - [QuakeCon官方网站](http://www.quakecon.org/)
  - [第一次比赛时候的照片](https://web.archive.org/web/20050310182845/http://www.planetquake.com/photos/quakecon96/)
  - [过去几年Qcon的影像资料](http://www.quakeconpics.com/)
  - [id Software 主页](http://www.idsoftware.com/)

[Category:電子競技賽事](../Category/電子競技賽事.md "wikilink")
[Category:電子遊戲展覽](../Category/電子遊戲展覽.md "wikilink")