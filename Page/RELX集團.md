**RELX集團**（RELX Group），原稱**里德·愛思唯爾集團**或**里德·埃爾塞維爾**（Reed
Elsevier），成立於1993年，由[英國的](../Page/英國.md "wikilink")[里德國際公司](../Page/里德國際公司.md "wikilink")（Reed
International
PLC）和[荷蘭的](../Page/荷蘭.md "wikilink")[愛思唯爾公司](../Page/愛思唯爾.md "wikilink")（Elsevier
NV）合併組成，並投資設立了「里德·愛思唯爾出版集團」（Reed Elsevier Group
PLC）和「愛思唯爾·里德金融集團」（Elsevier Reed Finance
BV）兩家公司。

里德·愛思唯爾出版集團在英國註冊，負責集團內所有[圖書](../Page/圖書.md "wikilink")[出版和](../Page/出版.md "wikilink")[線上](../Page/線上.md "wikilink")[資料庫等業務](../Page/資料庫.md "wikilink")；愛思唯爾·里德金融集團在荷蘭註冊，專為里德·愛思唯爾集團提供[財政](../Page/財政.md "wikilink")、[金融和](../Page/金融.md "wikilink")[保險服務](../Page/保險.md "wikilink")。

2002年，里德國際公司和愛思唯爾公司各分別易名為「**Reed Elsevier PLC**」和「**Reed Elsevier
NV**」。前者在英國[倫敦證交所](../Page/倫敦證交所.md "wikilink")（LSE）掛牌，代碼REL，持有出版集團的50%股權和金融集團的39%股權；後者在[泛歐證交所](../Page/泛歐股票交易所.md "wikilink")（Euronext）掛牌，代碼REN，持有出版集團的50%股權和金融集團的61%股權。

## 歷史沿革

### 里德國際公司

里德國際公司成立於1894年英國，創始人是Albert E. Reed，從新聞用紙製造商起家。公司原以創始人Albert
Reed為名，1903年Albert Reed & Co. 公開發行，1970後年陸續易名為Reed International
Ltd（1970），Reed International PLC（1982），到現在的 Reed Elsevier PLC（2002）。

### 愛思唯爾公司

[Elsevier.gif](https://zh.wikipedia.org/wiki/File:Elsevier.gif "fig:Elsevier.gif")
 {{-}}

## 旗下出版物

  - [《細胞》](../Page/細胞_\(期刊\).md "wikilink")（Cell）
  - [《柳葉刀》](../Page/柳葉刀_\(雜誌\).md "wikilink")（The Lancet） 或譯《刺胳針》
  - 荷蘭《醫學文摘》（Excerpta Medica Database）
  - 《Tetrahedron Letters》
  - [《新科學人》](../Page/新科學人.md "wikilink")（New Scientist）

## 旗下子公司

  - Academic Press
  - Butterworth Heinemann
  - Cahners Business Information
  - Cahners Travel Group
  - Churchill Livingstone
  - Elsevier Business Information
  - Elsevier Opleidingen
  - Elsevier Science
  - Les Editions du Juris-Classeur
  - LexisNexis
  - Reed Business Information
  - Reed Educational & Professional Publishing
  - Reed Exhibitions
  - Reed Technology and Information Services
  - Springhouse Corporation
  - Staempfli Verlag
  - Martindale-Hubbell
  - Mosby
  - Pan European Publishing Company
  - W. B. Saunders

## 参考文献

[Category:跨國公司](../Category/跨國公司.md "wikilink")
[Category:出版社](../Category/出版社.md "wikilink")
[Category:1993年成立的公司](../Category/1993年成立的公司.md "wikilink")
[Category:总部在荷兰的跨国公司](../Category/总部在荷兰的跨国公司.md "wikilink")
[Category:总部在英国的跨国公司](../Category/总部在英国的跨国公司.md "wikilink")