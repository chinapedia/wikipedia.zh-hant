**藿香薊**（學名：**）是[菊科](../Page/菊科.md "wikilink")[藿香薊屬一種原產於美洲熱帶地區](../Page/藿香薊屬.md "wikilink")，特別是[巴西的](../Page/巴西.md "wikilink")[一年生](../Page/一年生植物.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")，其頑強的生命力在其他地區常被視為[入侵](../Page/入侵物種.md "wikilink")[雜草](../Page/雜草.md "wikilink")。[越南语稱呼本物種為](../Page/越南语.md "wikilink")「豬屎草」（），因為這種植物會在骯髒的地方生長。這種植物株高約0.5–1米，長2–6厘米、寬1-3厘米的卵形或三角形[對生](../Page/對生.md "wikilink")[葉片](../Page/葉片.md "wikilink")，[花為](../Page/花.md "wikilink")[白色到淺](../Page/白色.md "wikilink")[紫紅色](../Page/紫紅色.md "wikilink")\[1\]或[淺藍色](../Page/淺藍色.md "wikilink")，由多朵細小的管狀花聚生而成。披白色柔毛，揉碎後有異臭。果實為黑色線狀[瘦果](../Page/瘦果.md "wikilink")。它們喜曬[太陽](../Page/太陽.md "wikilink")，長於荒地、平地和田邊比較乾燥貧脊的[土壤上](../Page/土壤.md "wikilink")。

## 別名

**霍香薊**、**勝紅薊**、**一枝香**、**鹹蝦花**、**毛麝香**、**蝶子草**、**南風草**、**柳仔黃**、**白花草**、**白花臭草**、**yumun**
(泰雅語）和**saapan**（排灣語）。

## 應用

作為一種[药用植物](../Page/药用植物.md "wikilink")，本物種在多種傳統醫療文化用作治療[痢疾及](../Page/痢疾.md "wikilink")[腹瀉](../Page/腹瀉.md "wikilink")\[2\]。在傳統中醫藥，藿香薊可用作治療「感冒發熱、咽喉腫痛，咯血，衄血，崩漏，脘腹疼痛，風濕痹痛」或治療[中耳炎](../Page/中耳炎.md "wikilink")、外傷出血等症。
本物種亦可用作植物[農藥](../Page/農藥.md "wikilink")，可毒殺[昆蟲及](../Page/殺蟲劑.md "wikilink")\[3\]\[4\]。

## 毒性

本物種具[肝毒性](../Page/肝毒性.md "wikilink")，不慎誤服可引致[肝臟](../Page/肝臟.md "wikilink")及導致[贅生物形成](../Page/贅生物.md "wikilink")\[5\]\[6\]。在[埃塞俄比亞曾發生過因為穀物中混雜有本物種而造成的集體中毒事件](../Page/埃塞俄比亞.md "wikilink")\[7\]。本植物含有及等[吡咯里西啶](../Page/吡咯里西啶.md "wikilink")。

## 野草風險

在野外，本物種常會發展成[野草](../Page/野草.md "wikilink")。在非洲、澳大利亞、東南亞及美國，本物種被視為入侵性野草\[8\]\[9\]。在亞洲其他稻米種植地區，本物種的泛濫被視為只有中度\[10\]，其乾草在一般農民會視為普通燃料來生火。

## 圖庫

<File:Ageratum> conizoides in Narshapur forest, AP W IMG 1100.jpg|in .
<File:Ageratum> conizoides in Narshapur forest, AP W IMG 0808.jpg|in .
<File:Ageratum> conizoides in Narshapur forest, AP W IMG 0805.jpg|in .
<File:Ageratum> conizoides in Narshapur forest, AP W2 IMG 1100.jpg|in .
<File:Blue> flower Used for Pookkalam DSC 0945.jpg| From Kerala.

## 參考文獻

## 外部連結

  - [Plants For Future: *Ageratum
    conyzoides*](http://www.pfaf.org/database/plants.php?Ageratum+conyzoides)

  - [Tropical Plant Database: *Ageratum
    conyzoides*](http://www.rain-tree.com/ageratum.htm)

  - [Global Invasive Species Database: *Ageratum
    conyzoides*](http://issg.org/database/species/ecology.asp?si=1493&fr=1&sts=sss&lang=EN)

  - [*Ageratum
    conyzoides*](http://www.plantasdaninhasonline.com.br/mentrasto/pagina.htm)
    photos

  -
[Category:中药](../Category/中药.md "wikilink")
[conyzoides](../Category/藿香蓟属.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")
[Category:南美洲植物](../Category/南美洲植物.md "wikilink")
[Category:入侵植物种](../Category/入侵植物种.md "wikilink")
[Category:Medicinal plants of South
America](../Category/Medicinal_plants_of_South_America.md "wikilink")
[Category:有毒植物](../Category/有毒植物.md "wikilink")
[Category:植物毒素殺蟲劑](../Category/植物毒素殺蟲劑.md "wikilink")
[Category:1753年描述的植物](../Category/1753年描述的植物.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.

9.

10.