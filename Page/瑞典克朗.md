**瑞典克朗**（，，[代號](../Page/ISO_4217.md "wikilink")：SEK，[符號](../Page/貨幣符號.md "wikilink")：kr）是自1873年通行於[瑞典的貨幣](../Page/瑞典.md "wikilink")。

主要货币单位为克朗和欧尔。1克朗（）下分為100歐爾（），但所有歐爾硬幣已經於2010年10月起停止流通。貨品仍能使用歐爾定價，但以現金交易時會把金額捨入至整數克朗。

在2013年4月，瑞典克朗是世界上交易量第十一大的货币\[1\]。

## 歷史

瑞典於1873年加入[斯堪的納維亞貨幣聯盟後](../Page/斯堪的納維亞貨幣聯盟.md "wikilink")，發行了瑞典克朗，取代原來使用的國家圓（）。貨幣聯盟成員國[丹麥和](../Page/丹麥.md "wikilink")[挪威也同樣發行名為](../Page/挪威.md "wikilink")「[克朗](../Page/克朗.md "wikilink")」的貨幣，三國克朗都與[金價掛勾](../Page/金本位.md "wikilink")，並互相流通。該貨幣聯盟於第一次世界大戰時解散，三國克朗不再互相自由流通，但仍為各自的法定貨幣。

雖然瑞典於1995年加入了[歐洲聯盟](../Page/歐洲聯盟.md "wikilink")，但2003年的[公投否決以](../Page/瑞典公民投票.md "wikilink")[歐元取代克朗](../Page/歐元.md "wikilink")\[2\]。

## 硬幣

現時流通硬幣有1克朗、5克朗、10克朗三種。

| 瑞典克朗硬幣 |
| ------ |
| 圖樣     |
|        |
|        |
|        |

### 紀念幣

21世紀發行的紀念幣均為[無限法償](../Page/無限法償.md "wikilink")\[3\]。

| 年份    | 金幣幣值      | 銀幣幣值                                              | 紀念事項                                                                                       |
| ----- | --------- | ------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| 2000年 | 1克朗（白銅）   | 新千禧年來臨                                            |                                                                                            |
| 2001年 | 2000克朗    | 200克朗                                             | [卡爾十六世·古斯塔夫國王和](../Page/卡爾十六世·古斯塔夫.md "wikilink")[希爾維亞王后銀婚](../Page/希爾維亞王后.md "wikilink")  |
| 2001年 | 2000克朗    | 200克朗                                             | [諾貝爾獎設立](../Page/諾貝爾獎.md "wikilink")100週年                                                  |
| 2002年 | 2000克朗    | 200克朗                                             | [斯德哥爾摩](../Page/斯德哥爾摩.md "wikilink")[750週年](../Page/斯德哥爾摩歷史.md "wikilink")                 |
| 2002年 | 50克朗（北歐金） | [阿斯特麗德·林格倫](../Page/阿斯特麗德·林格倫.md "wikilink")95歲壽辰 |                                                                                            |
| 2003年 | 2000克朗    | 200克朗                                             | [聖畢哲塔](../Page/聖畢哲塔.md "wikilink")700歲壽辰                                                   |
| 2003年 | 2000克朗    | 200克朗                                             | [卡爾十六世·古斯塔夫國王在位](../Page/卡爾十六世·古斯塔夫.md "wikilink")30年                                      |
| 2004年 | 2000克朗    | 200克朗                                             | [斯德哥爾摩王宮落成](../Page/斯德哥爾摩王宮.md "wikilink")250週年                                            |
| 2005年 | 2000克朗    | 200克朗                                             | [瑞典-挪威聯合解體](../Page/瑞典-挪威聯合解體.md "wikilink")100週年                                          |
| 2005年 | 2000克朗    | 200克朗                                             | [達格·哈馬舍爾德](../Page/達格·哈馬舍爾德.md "wikilink")100歲壽辰                                           |
| 2005年 | 50克朗（北歐金） | 瑞典首枚[郵票問世](../Page/郵票.md "wikilink")150週年         |                                                                                            |
| 2006年 | 2000克朗    | 200克朗                                             | [瑞典鐵路運輸啟用](../Page/瑞典鐵路運輸.md "wikilink")150週年                                              |
| 2007年 | 2000克朗    | 200克朗                                             | [卡爾·林奈](../Page/卡爾·林奈.md "wikilink")300歲壽辰                                                 |
| 2008年 | 2000克朗    | 200克朗                                             | [塞爾瑪·拉格洛夫](../Page/塞爾瑪·拉格洛夫.md "wikilink")150歲壽辰                                           |
| 2010年 | 4000克朗    | 300克朗                                             | [王儲維多利亞公主和](../Page/維多利亞_\(瑞典\).md "wikilink")[丹尼爾·韋斯特林婚禮](../Page/丹尼爾·韋斯特林.md "wikilink") |

此外，瑞典國家銀行為紀念瑞典和芬蘭兩國友誼，於2009年[芬蘭脫離瑞典統治](../Page/芬蘭.md "wikilink")200週年時，發行1克朗特別幣\[4\]。

## 紙幣

2010年，國家銀行建議重新起用2克朗硬幣，發行200克朗紙幣，並發行20克朗硬幣取代紙幣，同時將會重新設計所有流通硬幣和紙幣。[瑞典議會通過發行](../Page/瑞典議會.md "wikilink")2克朗硬幣和200克朗紙幣的建議，但保留20克朗硬幣。2011年4月6日，國家銀行宣布新紙幣將會出現的人物和背景。

新發行的流通紙幣有20克朗、50克朗、100克朗、200克朗、500克朗、1000克朗六種面額。

| 瑞典克朗紙幣       |
| ------------ |
| 幣值           |
| 正面           |
| 20克朗\[5\]    |
| 50克朗\[6\]    |
| 100克朗\[7\]   |
| 200克朗\[8\]   |
| 500克朗\[9\]   |
| 1000克朗\[10\] |

### 紀念幣

國家銀行於2004年6月宣佈發行一款紀念幣\[11\]。

| 年份    | 幣值    | 正面圖案                                     | 背面圖案     | 紀念事項         |
| ----- | ----- | ---------------------------------------- | -------- | ------------ |
| 2005年 | 100克朗 | 國家象徵[斯韋阿母親](../Page/斯韋阿母親.md "wikilink") | 通巴印鈔廠、造紙 | 通巴印鈔廠成立250週年 |

## 新發行貨幣

2012年9月11日，瑞典中央銀行宣佈於2016年將會發行一系列取代1克朗及5克朗的新硬幣，主題為「太陽、風和水」（）。而2克朗亦將會被重新起用，10克朗將維持不變。新硬幣會將國王的畫像重新設計，而發行新硬幣的其中一個目的是不再於硬幣中使用鎳。

| 新瑞典克朗硬幣                                |
| -------------------------------------- |
| 幣值                                     |
| <small>北歐金使用89 %銅 5 %鋁 5%鋅 1%錫</small> |

## 匯率

## 參見

  - [瑞典經濟](../Page/瑞典經濟.md "wikilink")

## 參考資料

{{-}}

[Category:瑞典经济](../Category/瑞典经济.md "wikilink")

1.
2.  [歐盟委員會](http://ec.europa.eu/economy_finance/the_euro/your_country_euro9164_en.htm)
3.  [Sveriges Riksbank/Riksbanken - 21st
    century](http://www.riksbank.com/templates/Page.aspx?id=10886)
4.  [Sveriges Riksbank/Riksbanken - Jubilee and commemorative
    coins](http://www.riksbank.com/templates/Page.aspx?id=10885)
5.  [Sveriges Riksbank/Riksbanken
    - 20-krona](http://www.riksbank.com/templates/Page.aspx?id=10569)
6.  [Sveriges Riksbank/Riksbanken
    - 50-krona](http://www.riksbank.com/templates/Page.aspx?id=10570)
7.  [Sveriges Riksbank/Riksbanken
    - 100-krona](http://www.riksbank.com/templates/Page.aspx?id=10571)
8.  [Sveriges Riksbank/Riksbanken
    - 100-krona](http://www.riksbank.com/templates/Page.aspx?id=10571)
9.  [Sveriges Riksbank/Riksbanken
    - 500-krona](http://www.riksbank.com/templates/Page.aspx?id=10572)
10. [Sveriges Riksbank/Riksbanken
    - 1000-krona](http://www.riksbank.com/templates/Page.aspx?id=10572)
11. [Sveriges Riksbank/Riksbanken - Commemorative
    banknote](http://www.riksbank.com/templates/Page.aspx?id=16013)