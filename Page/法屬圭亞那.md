**法屬圭-{}-亞那**（），正式名称是**圭-{}-亚那**（），是[法国的一个海外大区和](../Page/法国.md "wikilink")，下辖一个[海外省](../Page/海外省.md "wikilink")，即圭-{}-亚那省。法属圭亚那也是[欧盟的一部分](../Page/欧盟.md "wikilink")；位于[南美洲北部](../Page/南美洲.md "wikilink")[大西洋边](../Page/大西洋.md "wikilink")，与[巴西和](../Page/巴西.md "wikilink")[苏里南交界](../Page/苏里南.md "wikilink")。地理位置为[北纬](../Page/纬度.md "wikilink")4°0-{’}-和[西经](../Page/经度.md "wikilink")53°0-{’}-。

## 译名

在南美洲的北部有兩個名稱相近的地區与國家，分別是獨立的國家「」以及法國海外屬地「」。[中華民國將獨立的國家譯為](../Page/中華民國.md "wikilink")「蓋-{}-亚那」，將法屬的領地譯為「法属圭-{}-亚那」。\[1\]或「**法屬蓋-{}-亞那**」。

## 地理

法属圭-{}-亞那面积83,534平方公里，与[奥地利相當](../Page/奥地利.md "wikilink")。北濱大西洋，海岸线长378公里。东、南面与巴西为邻，边界长673公里，西面是苏里南，边界长510公里。

法属圭-{}-亞那南部位于横跨南美洲北部的[圭-{}-亞那高地前](../Page/圭亞那高原.md "wikilink")，最高点高850-{zh-hans:米;
zh-hant:公尺;}-。重要河流有[马罗尼河](../Page/马罗尼河.md "wikilink")（它也是与苏里南的边界）、[西纳马利河和](../Page/西纳马利河.md "wikilink")[奥亚波克河](../Page/奥亚波克河.md "wikilink")（与巴西的边界）等。

法属圭-{}-亞那境內98%是热带雨林，森林覆盖率90%，是法国和欧洲联盟的大片森林區。大多数人口集中在北部海岸，大城市也集中於此。

法属圭-{}-亞那的气候属[热带雨林气候](../Page/热带雨林气候.md "wikilink")，年降水量3500毫米。全年温度在28℃左右。从8月到12月是旱期，其它时间是雨期。平均湿度比较高，终年在80%到90%左右。法属圭-{}-亞那不受[加勒比海](../Page/加勒比海.md "wikilink")[飓风的袭击](../Page/飓风.md "wikilink")。

## 历史

法属圭-{}-亞那的原来的居民是加勒比人和[阿拉瓦克人](../Page/阿拉瓦克人.md "wikilink")。

1498年，[克里斯托弗·哥伦布来到圭](../Page/克里斯托弗·哥伦布.md "wikilink")-{}-亞那的海岸。一百多年后[荷兰人开始在这里殖民](../Page/荷兰.md "wikilink")。

1604年，[法国开始侵入](../Page/法国.md "wikilink")，建立居民点。后[英国](../Page/英国.md "wikilink")、荷兰、法国和[葡萄牙相互争夺此地](../Page/葡萄牙.md "wikilink")，直到1816年最后归属法国。

1946年，法国宣布法属圭-{}-亞那为法国的“[海外省](../Page/海外省.md "wikilink")”。从此以后，许多土著号召自治，但支持从法国独立之支持度只有5%，部分因为[法国政府提供更多的津贴](../Page/法国政府.md "wikilink")。

从1947年开始，法属圭-{}-亞那拥有有限的自治权。从此该地区在[法国国民议会和](../Page/法国国民议会.md "wikilink")[法国参议院各有两个议席](../Page/法国参议院.md "wikilink")。

1968年，欧盟在[库鲁建立了一个](../Page/库鲁.md "wikilink")[圭亞那太空中心](../Page/圭亞那太空中心.md "wikilink")，[阿丽亚娜火箭在这个发射场发射](../Page/阿丽亚娜火箭.md "wikilink")。在库鲁，「欧洲太空计划」使法属圭-{}-亞那的这个角落变成了现代的世界，并吸引了一些移居到此工作的人力。

1977年，成为法国的一个大区。

## 政治

如同法国其它省份一样，法屬圭亞那也参加法国的[国民议会和省级立法机构](../Page/法國國民議會.md "wikilink")。在法国国民议会和参议院中法屬圭亞那各拥有两个议席。

所有法国法律在法屬圭亞那均被视为有效，但法国宪法第73条规定地方亦有权按地方特点对当地法律进行一定的修改。

从1982年开始法屬圭亞那是法国的一个大区和省。它分为两个区，19个乡和22个县。

法屬圭亞那設省政府及大區政府，政府首腦為政府委員會主席。各省選舉產生省議會和省政府。省主席為法屬圭亞那省份的領導人。法屬圭亞那省政府主席兼任大區政府主席。

每隔六年法属圭-{}-亞那进行省议会（19席）和大区议会（31席）的改选，从2010年3月26日起大区主席是[鲁道夫·亚历山大](../Page/鲁道夫·亚历山大.md "wikilink")（[Rodolphe
Alexandre](../Page/w:Rodolphe_Alexandre.md "wikilink")）。

法属圭-{}-亞那是[欧洲联盟的一部分](../Page/欧洲联盟.md "wikilink")，因此它与[巴西及](../Page/巴西.md "wikilink")[苏里南的边界是](../Page/苏里南.md "wikilink")[欧洲联盟極西的边界](../Page/欧洲联盟.md "wikilink")，但不屬於[申根區的地區](../Page/申根區.md "wikilink")（所有[法國海外部分均不歸入申根區](../Page/法國海外部分.md "wikilink")）。

2016年1月1日，第一届[法属圭亚那议会正式成立](../Page/法属圭亚那议会.md "wikilink")，取代此前的[圭亚那大区委员会和圭亚那总委员会](../Page/圭亚那大区委员会.md "wikilink")，作为法属圭亚那新的立法机关。

## 行政区划

分为两个区，19个縣和22个市鎮:
[Guyane_administrative.PNG](https://zh.wikipedia.org/wiki/File:Guyane_administrative.PNG "fig:Guyane_administrative.PNG")

<table>

<tr>

<th width="50%">

[聖洛朗杜馬羅尼区](../Page/聖洛朗杜馬羅尼区.md "wikilink")

</th>

<th width="50%">

[卡宴区](../Page/卡宴区.md "wikilink")

</th>

</tr>

<td valign="top">

1.  [Awala-Yalimapo](../Page/Awala-Yalimapo.md "wikilink")
2.  [法力](../Page/法力.md "wikilink")
3.  [聖洛朗杜馬羅尼](../Page/聖洛朗杜馬羅尼.md "wikilink")
4.  [阿帕图](../Page/阿帕图.md "wikilink")
5.  [格朗桑蒂](../Page/格朗桑蒂.md "wikilink")
6.  [Papaïchton](../Page/Papaïchton.md "wikilink")
7.  [扫罗](../Page/扫罗.md "wikilink")
8.  [马里帕苏拉](../Page/马里帕苏拉.md "wikilink")

</td>

<td valign="top">

9.  [Camopi](../Page/Camopi.md "wikilink")
10. [Saint-Georges](../Page/Saint-Georges,_French_Guiana.md "wikilink")
11. [Ouanary](../Page/Ouanary.md "wikilink")
12. [Régina](../Page/Régina.md "wikilink")
13. [Roura](../Page/Roura.md "wikilink")
14. [Saint-Élie](../Page/Saint-Élie.md "wikilink")
15. [Iracoubo](../Page/Iracoubo.md "wikilink")
16. [Sinnamary](../Page/Sinnamary.md "wikilink")
17. [庫魯](../Page/庫魯.md "wikilink")
18. [馬古利亞](../Page/馬古利亞.md "wikilink")
19. [Montsinéry-Tonnegrande](../Page/Montsinéry-Tonnegrande.md "wikilink")
20. [Matoury](../Page/Matoury.md "wikilink")
21. [卡宴](../Page/卡宴.md "wikilink")

<li>

[Remire-Montjoly](../Page/Remire-Montjoly.md "wikilink")

</td>

</tr>

</table>

### 主要居住点

  - [卡宴](../Page/卡宴.md "wikilink") 58,369人
  - [聖洛朗杜馬羅尼](../Page/聖洛朗杜馬羅尼.md "wikilink") 34,336人
  - [库鲁](../Page/库鲁.md "wikilink") 25,918人
  - [Matoury](../Page/Matoury.md "wikilink") 25,191人
  - [Remire-Montjoly](../Page/Remire-Montjoly.md "wikilink") 18,947人
  - [馬納](../Page/馬納.md "wikilink") 8,322人
  - [馬古利亞](../Page/馬古利亞.md "wikilink") 8,386人
  - [阿帕图](../Page/阿帕图.md "wikilink") 6,360人
  - [Maripasoula](../Page/Maripasoula.md "wikilink") 5,584人
  - [Saint-Georges](../Page/Saint-Georges,_French_Guiana.md "wikilink")
    3,692人
  - [Grand-Santi](../Page/Grand-Santi.md "wikilink") 3,430人
  - [Sinnamary](../Page/Sinnamary.md "wikilink") 3,137人

## 经济

[缩略图](https://zh.wikipedia.org/wiki/File:French_Guiana_Cacao_house_03.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Ile_Royale_musée_du_bagne.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Cayenne_Association_Fa_Kiao_Kon_So.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Cayenne_avenue_du_Général_Charles-de-Gaulle_2013.jpg "fig:缩略图")
林业是法屬圭亞那的一个重要经济行业，农业只集中在海岸地区，此外捕虾业和金矿是值得一提的产业。

最重要的贸易对象是法国本土、[特立尼达和多巴哥和](../Page/特立尼达和多巴哥.md "wikilink")[意大利](../Page/意大利.md "wikilink")。主要出口品有[鱼](../Page/鱼.md "wikilink")、[米和](../Page/米.md "wikilink")[金](../Page/金.md "wikilink")，主要进口品有[机器和](../Page/机器.md "wikilink")[汽车](../Page/汽车.md "wikilink")。

库鲁发射场目前成为内地的一个经济和旅游动力。

近年来，日益猖獗的[巴西非法淘金人](../Page/巴西.md "wikilink")，导致严重的生态环境破坏：他们使用[汞等有害物质来清理和加工黄金](../Page/汞.md "wikilink")。

## 语言

圭-{}-亞那原住民的语言有六种，分成三种语系：

1.  [瓦扬皮语](../Page/瓦扬皮语.md "wikilink")（wayampi）、[埃梅里用永语](../Page/埃梅里用永语.md "wikilink")（émérillon）属于[图皮-瓜拉尼语系](../Page/图皮-瓜拉尼语系.md "wikilink")（Tupi-Guarani）；
2.  [阿拉瓦克语系](../Page/阿拉瓦克语系.md "wikilink")（arawak）、[巴里库尔语](../Page/巴里库尔语.md "wikilink")（palikour）属于[阿拉瓦语系](../Page/阿拉瓦语系.md "wikilink")；
3.  [卡里纳语](../Page/卡里纳语.md "wikilink")（kalihna）、[瓦亚纳语](../Page/瓦亚纳语.md "wikilink")（wayana）属于[加勒比语系](../Page/加勒比语系.md "wikilink")。

除此之外，还有一些基于[法文和](../Page/法文.md "wikilink")[英文的](../Page/英文.md "wikilink")[克里奥尔语言](../Page/克里奥尔语.md "wikilink")。

## 其它

糖和雨林木材成为了殖民地的经济支柱。奴隶们从非洲带了种植糖园的工作，尽管他们的成功被本地的印地安人的敌视和热带的疾病所限制。且在1848年的奴隶制度废除以后，种植园的产量，从来无法配合法国的加勒比海殖民地，本地的工业几乎崩溃了。在大约相同的时间，法国决定减少监狱的费用，将受刑人移民至圭-{}-亞那，贡献殖民地的发展。约有70,000罪犯，包括阿尔弗雷德·德雷福斯和亨利·帕皮隆·查利尔尼（Herry
Papillon
Charriere），在1852年和1939年之间到达。刚开始，许多流放的罪犯，熬过了恶劣的环境，大部分都死于疟疾和发烧。在第二次世界大战以后，圭-{}-亞那仍然是罪犯的殖民地。

### 发射场

[欧洲空间局从](../Page/欧洲空间局.md "wikilink")[库鲁发射场使用](../Page/库鲁发射场.md "wikilink")[阿丽亚娜火箭发射](../Page/阿丽亚娜火箭.md "wikilink")[人造卫星](../Page/人造卫星.md "wikilink")。

### 军用

法国在罗尚波有一个军事基地，[外籍兵团亦設有森林基地](../Page/法国外籍兵团.md "wikilink")，并在这里驻扎和培训上百士兵，同時擔任太空中心的守備任務。2004年3月法国的外籍兵团就是从这里出发进驻[海地](../Page/海地.md "wikilink")。

## 徽章与旗帜

## 註釋

## 延伸閱讀

  - Robert Aldrich and John Connell. *France's Overseas Frontier :
    Départements et territoires d'outre-mer* Cambridge University
    Press, 2006. ISBN 0-521-03036-6.

  - René Belbenoit. *Dry guillotine: Fifteen years among the living
    dead* 1938, Reprint: Berkley (1975). ISBN 0-425-02950-6.

  - René Belbenoit. *Hell on Trial* 1940, translated from the original
    French manuscript by Preston Rambo. E. P Dutton & Co. Reprint by
    Blue Ribbon Books, New York, 194 p. Reprint: Bantam Books, 1971.

  - [Henri Charrière](../Page/Henri_Charrière.md "wikilink"). *Papillon*
    Reprints: Hart-Davis, MacGibbon Ltd. 1970. ISBN 0-246-63987-3 (hbk);
    Perennial, 2001. ISBN 0-06-093479-4 (sbk).

  - John Gimlette, *Wild Coast: Travels on South America's Untamed Edge*
    2011

  -
  - Peter Redfield. *Space in the Tropics: From Convicts to Rockets in
    French Guiana* ISBN 0-520-21985-6.

  - Miranda Frances Spieler. *Empire and Underworld: Captivity in French
    Guiana* (Harvard University Press; 2012) studies slaves, criminals,
    indentured workers, and other marginalized people from 1789 to 1870.

## 外部链接

  -

  -

  -
  -
  -
  - [Consular Information
    Sheet](https://web.archive.org/web/20070405215304/http://travel.state.gov/travel/cis_pa_tw/cis/cis_1117.html)
    from the [美国国务院](../Page/美国国务院.md "wikilink")

  - [Ethnologue French Guiana
    page](https://web.archive.org/web/20120111093033/http://www.ethnologue.com/show_country.asp?name=French+Guiana)

  - [Silvolab Guyanae – scientific interest group in French
    Guiana](https://web.archive.org/web/20070426065007/http://kourou.cirad.fr/)

  - [Article on separatism in French
    Guiana](http://www.luxner.com/cgi-bin/view_article.cgi?articleID=661)

  - [About.com French Guiana travel
    site](http://gosouthamerica.about.com/cs/frenchguiana/)

  - [Status of Forests in French
    Guiana](http://rainforests.mongabay.com/20frenchg.htm)

  - [Officials reports, thesis, scientific papers about French Guiana
    (en|fr)](https://archive.is/20121208162312/http://www.infos-guyane.com/documents/english/)

  - [Training legionnaires to fight in French
    Guiana](http://www.iht.com/articles/2008/12/01/europe/01legion.php)

  - [The IRD's database AUBLET2 stores information about botanical
    specimens collected in the Guianas, mainly in French
    Guiana](https://web.archive.org/web/20070206024746/http://www.cayenne.ird.fr/aublet2/aublet2_uk.php3)

  - James Rogers and Luis Simón. [The Status and Location of the
    Military Installations of the Member States of the European Union
    and Their Potential Role for the European Security and Defence
    Policy
    (ESDP).](http://www.europarl.europa.eu/meetdocs/2004_2009/documents/dv/sede300309studype407004_/SEDE300309StudyPE407004_en.pdf)
    Brussels: European Parliament, 2009. 25 pp.

[\*](../Category/法屬圭亞那.md "wikilink")
[G](../Category/法国大区.md "wikilink")
[G](../Category/法国省份.md "wikilink")
[Category:南美洲属地](../Category/南美洲属地.md "wikilink")

1.