**SPFDisk（SPecialFDisk）**
是一套[磁碟分割工具與](../Page/磁碟分割工具.md "wikilink")[啟動管理程式](../Page/啟動程式.md "wikilink")（Boot
Manager），使用圖形化的操作介面，並且支援[繁體中文](../Page/繁體中文.md "wikilink")、[簡體中文和](../Page/簡體中文.md "wikilink")[英文](../Page/英文.md "wikilink")，作者為[馮緒平](../Page/馮緒平.md "wikilink")。

SPFDisk
使用[C語言和](../Page/C語言.md "wikilink")[組合語言寫成](../Page/組合語言.md "wikilink")。

## 功能

  - [硬碟分割](../Page/硬碟分割.md "wikilink")
  - 啟動管理程式安裝與設定
  - [硬碟分割表救援](../Page/硬碟.md "wikilink")
  - 快速[硬碟格式化](../Page/硬碟格式化.md "wikilink")

## 參見

  - [fdisk](../Page/fdisk.md "wikilink")
  - [Norton PartitionMagic](../Page/Norton_PartitionMagic.md "wikilink")
  - [Diskgen](../Page/Diskgen.md "wikilink")

## 外部連結

  - [鳥哥的 Linux 私房菜 -- 一個簡單的 spfdisk
    分割硬碟實例](http://linux.vbird.org/linux_basic/0140spfdisk.php)
  - [iThome online
    :：創造SPFDisk，打敗FDisk的達人](https://web.archive.org/web/20070419033229/http://www.ithome.com.tw/itadm/article.php?c=36343)

[Category:磁盘级数据恢复软件](../Category/磁盘级数据恢复软件.md "wikilink")