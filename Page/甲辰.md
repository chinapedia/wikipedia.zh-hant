**甲辰**为[干支之一](../Page/干支.md "wikilink")，顺序为第41个。前一位是[癸卯](../Page/癸卯.md "wikilink")，后一位是[乙巳](../Page/乙巳.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之甲屬陽之木](../Page/天干.md "wikilink")，[地支之辰屬陽之土](../Page/地支.md "wikilink")，是木尅土相尅。

## 甲辰年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")41年称“**甲辰年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘44，或年份數減3，除以10的餘數是1，除以12的餘數是5，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“甲辰年”：

<table>
<caption><strong>甲辰年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/44年.md" title="wikilink">44年</a></li>
<li><a href="../Page/104年.md" title="wikilink">104年</a></li>
<li><a href="../Page/164年.md" title="wikilink">164年</a></li>
<li><a href="../Page/224年.md" title="wikilink">224年</a></li>
<li><a href="../Page/284年.md" title="wikilink">284年</a></li>
<li><a href="../Page/344年.md" title="wikilink">344年</a></li>
<li><a href="../Page/404年.md" title="wikilink">404年</a></li>
<li><a href="../Page/464年.md" title="wikilink">464年</a></li>
<li><a href="../Page/524年.md" title="wikilink">524年</a></li>
<li><a href="../Page/584年.md" title="wikilink">584年</a></li>
<li><a href="../Page/644年.md" title="wikilink">644年</a></li>
<li><a href="../Page/704年.md" title="wikilink">704年</a></li>
<li><a href="../Page/764年.md" title="wikilink">764年</a></li>
<li><a href="../Page/824年.md" title="wikilink">824年</a></li>
<li><a href="../Page/884年.md" title="wikilink">884年</a></li>
<li><a href="../Page/944年.md" title="wikilink">944年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1004年.md" title="wikilink">1004年</a></li>
<li><a href="../Page/1064年.md" title="wikilink">1064年</a></li>
<li><a href="../Page/1124年.md" title="wikilink">1124年</a></li>
<li><a href="../Page/1184年.md" title="wikilink">1184年</a></li>
<li><a href="../Page/1244年.md" title="wikilink">1244年</a></li>
<li><a href="../Page/1304年.md" title="wikilink">1304年</a></li>
<li><a href="../Page/1364年.md" title="wikilink">1364年</a></li>
<li><a href="../Page/1424年.md" title="wikilink">1424年</a></li>
<li><a href="../Page/1484年.md" title="wikilink">1484年</a></li>
<li><a href="../Page/1544年.md" title="wikilink">1544年</a></li>
<li><a href="../Page/1604年.md" title="wikilink">1604年</a></li>
<li><a href="../Page/1664年.md" title="wikilink">1664年</a></li>
<li><a href="../Page/1724年.md" title="wikilink">1724年</a></li>
<li><a href="../Page/1784年.md" title="wikilink">1784年</a></li>
<li><a href="../Page/1844年.md" title="wikilink">1844年</a></li>
<li><a href="../Page/1904年.md" title="wikilink">1904年</a></li>
<li><a href="../Page/1964年.md" title="wikilink">1964年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2024年.md" title="wikilink">2024年</a></li>
<li><a href="../Page/2084年.md" title="wikilink">2084年</a></li>
<li><a href="../Page/2144年.md" title="wikilink">2144年</a></li>
<li><a href="../Page/2204年.md" title="wikilink">2204年</a></li>
<li><a href="../Page/2264年.md" title="wikilink">2264年</a></li>
<li><a href="../Page/2324年.md" title="wikilink">2324年</a></li>
<li><a href="../Page/2384年.md" title="wikilink">2384年</a></li>
<li><a href="../Page/2444年.md" title="wikilink">2444年</a></li>
<li><a href="../Page/2504年.md" title="wikilink">2504年</a></li>
<li><a href="../Page/2564年.md" title="wikilink">2564年</a></li>
<li><a href="../Page/2624年.md" title="wikilink">2624年</a></li>
<li><a href="../Page/2684年.md" title="wikilink">2684年</a></li>
<li><a href="../Page/2744年.md" title="wikilink">2744年</a></li>
<li><a href="../Page/2804年.md" title="wikilink">2804年</a></li>
<li><a href="../Page/2864年.md" title="wikilink">2864年</a></li>
<li><a href="../Page/2924年.md" title="wikilink">2924年</a></li>
<li><a href="../Page/2984年.md" title="wikilink">2984年</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 甲辰月

天干丁年和壬年，[清明到](../Page/清明.md "wikilink")[立夏的時間段](../Page/立夏.md "wikilink")，就是**甲辰月**：

  - ……
  - [1977年](../Page/1977年.md "wikilink")4月清明到5月立夏
  - [1982年](../Page/1982年.md "wikilink")4月清明到5月立夏
  - [1987年](../Page/1987年.md "wikilink")4月清明到5月立夏
  - [1992年](../Page/1992年.md "wikilink")4月清明到5月立夏
  - [1997年](../Page/1997年.md "wikilink")4月清明到5月立夏
  - [2002年](../Page/2002年.md "wikilink")4月清明到5月立夏
  - [2007年](../Page/2007年.md "wikilink")4月清明到5月立夏
  - ……

## 甲辰日

## 甲辰時

天干丁日和壬日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）7時到9時，就是**甲辰時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/甲辰年.md "wikilink")