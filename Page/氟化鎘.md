**氟化鎘**是一種[分子式為CdF](../Page/分子式.md "wikilink")<sub>2</sub>的[無機化合物](../Page/無機化合物.md "wikilink")，[熔點](../Page/熔點.md "wikilink")1110
°C，沸點1748 °C。

## 相關化合物

  - [氯化鎘](../Page/氯化鎘.md "wikilink")
  - [溴化鎘](../Page/溴化鎘.md "wikilink")
  - [碘化鎘](../Page/碘化鎘.md "wikilink")
  - [氯化鋅](../Page/氯化鋅.md "wikilink")

## 外部連結

  - [National Pollutant Inventory - Cadmium and compounds fact
    sheet](https://web.archive.org/web/20061210213049/http://www.npi.gov.au/database/substance-info/profiles/17.html)
  - [National Pollutant Inventory - Fluoride and compounds fact
    sheet](https://web.archive.org/web/20060116134617/http://www.npi.gov.au/database/substance-info/profiles/44.html)

[Category:鎘化合物](../Category/鎘化合物.md "wikilink")
[Category:氟化物](../Category/氟化物.md "wikilink")
[Category:金属卤化物](../Category/金属卤化物.md "wikilink")