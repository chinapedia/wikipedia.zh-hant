**社会企业**（[英文](../Page/英文.md "wikilink")：**social
enterprise**），簡稱**社企**，是从[英国兴起的](../Page/英国.md "wikilink")[企业型態](../Page/企业.md "wikilink")，目前并无统一的定义。概括而言，社会企业从事的是公益性事业，它通过[市场机制来调动社会力量](../Page/市场机制.md "wikilink")，将商业策略最大程度运用于改善人类和环境生存条件，而非为外在的利益相关者谋取最大利益。其[投资主要用于企业本身或](../Page/投资.md "wikilink")[社会](../Page/社会.md "wikilink")。与一般其他[私有企业不同的是](../Page/私有企业.md "wikilink")，它不只是为了[股东或者企业的拥有者谋取最大的](../Page/股东.md "wikilink")[利润而運作](../Page/利润.md "wikilink")。此概念于中国通常称“福利企业”。

社会企业的[法人身分可以是營利性质的](../Page/法人.md "wikilink")、也可以是非营利性质的，并且它的表现形式也可能是共同合作模式、成熟的组织构架、非独立实体、\[1\]社会商业或慈善组织。\[2\]

许多商业企业试图认为他们自己具有社會為目标，而对于这些目标的投入是以最终将其使企业获取更多财政价值为目的的。相反，社会企业的目的不是对投资者提供任何利益回报，除非是那些他们认为最终可以使他们实现社会和环境目标的部分。

由于社会企业概念的慈善性根源来自美国，同时伴随着在英国、欧洲和亚洲的共同发展，因此它具有混合的且有争议的社會關懷報告传承。\[3\]
在美国，这个概念伴随着‘通过交易做慈善’，而非‘做慈善的同时做交易’。在其他国家，相比于慈善事业，更明显地侧重于社区组织、重要的且相互关系的民主控制。\[4\]近几年，关于社会目的型商业的概念越来越多，它们以推行社会责任为目的，或是为慈善项目寻求基金。\[5\]

## 各地区發展

### 台灣

  - 在台灣，2014年由[行政院頒布了](../Page/行政院.md "wikilink")[社會企業行動方案](../Page/社會企業行動方案.md "wikilink")\[6\]，並訂立該年為「社企元年」，以「調法規」、「建平台」、「籌資金」及「倡育成」四大面向，交付[勞動部以及](../Page/勞動部.md "wikilink")[經濟部執行](../Page/經濟部.md "wikilink")，預計於2016年年底完成於全台育成100家社會企業、協助20家社會企業參與國際論壇等目標。

### 香港

  - [香港社會創投基金](http://www.sv-hk.org)
    是香港小數的民間社企基金，旨在運用專業界別的知識為社會企業提供慈善創投方面的支援，協助創新的社會企業在香港發展，解決各類緊迫的社會問題，推動社會進步。
  - [歲閲悠遊是舉辦香港本土地道導賞團的社會企業](../Page/歲閲悠遊.md "wikilink")。公司透過[伸手助人協會聘請](../Page/香港社會服務聯會.md "wikilink")65歲以上的長者，與大專生一同被培訓成為導賞員，帶領參加者從多角度感受社區文化，從而關懷長者及消除社會隔閡。\[7\]
  - 大宝化妆品公司，是北京市的明星福利企业，其员工中35%为以聋哑人为主的残疾员工。该公司2008年已为美国强生公司收购。
  - [全心庫予社會企業Social
    Enterprise計劃](https://www.facebook.com/HeartsofGive/timeline)
    是首個以推動特色社會企業的社企計劃，旨在鼓勵社會各界人士了解一些特色的社會企業，很多都是跟創造、文化藝術、關愛教育和義工服務推動有關的表表者。
  - [影視明星](../Page/電影演員.md "wikilink")[曾志偉和](../Page/曾志偉.md "wikilink")[戚美珍合作投資香港社會企業素食餐廳](../Page/戚美珍.md "wikilink")「[樂農](../Page/樂農.md "wikilink")」\[8\]\[9\]，並透過香港慈善機構「[龍耳](../Page/龍耳.md "wikilink")」聘請聾人\[10\]，主要目的是希望能幫助到社會上的一些弱勢社群，讓他們有機會就業謀生。

### 德國

  - [無聲對話工作坊源自德國社會企業的國際品牌](../Page/無聲對話工作坊.md "wikilink")\[11\]，參加者要在沒有任何聲音的情況下，盡力以非語言的方式如[手勢](../Page/手勢.md "wikilink")、身體語言與別人[溝通](../Page/溝通.md "wikilink")，提高健聽人士對聽障人士的了解，達到傷健共融\[12\]。

### 日本

  - 龍貓故鄉基金會（Totoro National Trust）

## 参考文献

## 外部鏈接

  - [社会企业资源中心](http://sebc.org.hk)
  - [社會企業](http://www.sv-hk.org)
  - [香港政府社會企業網站](http://www.social-enterprises.gov.hk/index.html)
  - [社企流-台灣第一個華文社會企業資訊匯流平台](http://www.seinsights.asia/)
  - [全心庫予社會企業Social
    Enterprise計劃](https://www.facebook.com/HeartsofGive/timeline/)

## 参见

  - [綠色經濟](../Page/綠色經濟.md "wikilink")
  - [企業全成本](../Page/企業全成本.md "wikilink"):
    考慮包括外在社會[成本](../Page/成本.md "wikilink")，例如工業生產可能造成的[環境](../Page/環境.md "wikilink")[污染成本等](../Page/污染.md "wikilink")。
  - [環保](../Page/環保.md "wikilink")、[社會成本](../Page/社會成本.md "wikilink")、[經濟](../Page/經濟.md "wikilink")[三線發展](../Page/三線發展.md "wikilink")。
  - [庇护工场](../Page/庇护工场.md "wikilink")

[de:Social Business](../Page/de:Social_Business.md "wikilink") [he:עסק
חברתי](../Page/he:עסק_חברתי.md "wikilink")

[Category:社會工作與福利](../Category/社會工作與福利.md "wikilink")
[Category:社會企業](../Category/社會企業.md "wikilink")

1.  "What is a Disregarded Entity - Disregarded Entity Definition".
    Biztaxlaw.about.com. 2013-07-13. Retrieved 2013-10-21.
2.  Ridley-Duff, R. J. and Bull, M. (2011) Understanding Social
    Enterprise: Theory and Practice, London: Sage Publications.
3.  Ridley-Duff, R. J. and Bull, M. (2011) Understanding Social
    Enterprise: Theory and Practice, London: Sage Publications, see
    Chapter 3.
4.  Kerlin, J. (2009) Social Enterprise: A Global Comparison, University
    Press of New England.
5.  Ridley-Duff, R. J. and Southcombe, C. (2011) "The Social Enterprise
    Mark: a critical review of its conceptual dimensions", paper to 34th
    International Small Business and Entrepreneurship Conference,
    Sheffield. Winner of 'Best Research and Knowledge Transfer Paper in
    Conference'
6.  [社會企業行動方案](http://www.ey.gov.tw/Upload/RelFile/26/716149/8d8b6be7-0e21-4a37-9c72-871e28b325d2.pdf)
    [行政院經濟部](../Page/行政院經濟部.md "wikilink"). 2014-9，.
7.
8.
9.
10.
11.
12.