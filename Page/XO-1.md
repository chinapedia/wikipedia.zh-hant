**XO-1**，在之前常被稱作「百元電腦」或「兒童的機器」，是一部追求價廉的[筆記型電腦](../Page/筆記型電腦.md "wikilink")。這部電腦由「一個小孩一部手提電腦」（[OLPC](../Page/OLPC.md "wikilink")）的[貿易協會所開發](../Page/貿易協會.md "wikilink")。其目的是要使電腦應用推廣到全世界的[兒童](../Page/兒童.md "wikilink")，特別是[發展中國家](../Page/發展中國家.md "wikilink")，好讓他們能夠獲取知識及現代化的教育。OLPC是一個由[美國麻省理工學院媒體實驗室的所有成員組成的](../Page/麻省理工学院.md "wikilink")[非營利組織](../Page/非營利組織.md "wikilink")，他們會負責整個設計、生產以至推廣這部電腦的工作。

XO-1追求的目標是低階、低耗能，因此，不少功能及設備都朝着轻量化的方向。例如電腦將用[固态硬盘取代](../Page/固态硬盘.md "wikilink")[硬碟](../Page/硬碟.md "wikilink")，而且會使用[Fedora
Core作為它的](../Page/Fedora_Core.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")\[1\]
。[行動隨意網路將會使用在其中](../Page/Ad_hoc網路.md "wikilink")，以便提供多部機器只須經由一個連結點就能進入網路。

基於一個小孩一部手提電腦的宗旨，這部電腦將會賣給政府，再經由學校發放給兒童。價格目前定位於135美元到175美元間，而目標是在2008年時達到美金一百元的目標。在2006年的夏天，大約500個設計面板（Alpha-1）被發送；大約在2007年中期收到875原件（Beta
1）\[2\]
。[廣達電腦是這項計畫的合約製造商](../Page/廣達電腦.md "wikilink")，表示在2007年2月已經收到一百萬部的訂單，並且指出他們可以在這年達到出貨五百萬至一千萬部。當中有七個國家已經預訂了XO-1給當地的兒童，包括[阿根廷](../Page/阿根廷.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[利比亞](../Page/利比亞.md "wikilink")、[奈及利亞](../Page/奈及利亞.md "wikilink")、[盧安達](../Page/盧安達.md "wikilink")、[泰國和](../Page/泰國.md "wikilink")[烏拉圭等](../Page/烏拉圭.md "wikilink")\[3\]。

OLPC沒有計畫在消費市場上發售\[4\]，然而，[廣達將會提供相類似的手提電腦](../Page/廣達電腦.md "wikilink")\[5\]。

## 技術

[LaptopOLPC_b.jpg](https://zh.wikipedia.org/wiki/File:LaptopOLPC_b.jpg "fig:LaptopOLPC_b.jpg")

XO-1 將是廉價、小型、耐用且實用。將搭載輕量化的 Fedora Linux
和著重孩童合作的[Sugar](../Page/Sugar.md "wikilink")[圖形界面](../Page/GUI.md "wikilink")。XO-1
同時具備視訊攝影機、麥克風、廣範圍 [Wi-Fi](../Page/Wi-Fi.md "wikilink")
以及二合一手寫／觸控面板。計劃使用人力作為電力來源，使完全不需商業電源的操作成為可能。

[瑪麗傑布森所述的設計目標如下](../Page/Mary_Lou_Jepsen.md "wikilink")：

  - 最低的電力消耗，目標是 2–3  [瓦的總耗電量](../Page/瓦.md "wikilink")；
  - 最低的生產成本，目標是每部 100 美金（以百萬部為單位的大量生產）；
  - 要酷，暗示外觀的創新風格；
  - 使用[電子書功能時](../Page/電子書.md "wikilink")**極低**的電力消耗；
  - 提供的軟體均為[開放源始碼和](../Page/開放源始碼.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")。

在[Design
Continuum及](../Page/Design_Continuum.md "wikilink")[Fuseproject兩家公司的幫助下](../Page/Fuseproject.md "wikilink")，OLPC考察了不同的使用模式，包括：手提電腦、[電子書](../Page/電子書.md "wikilink")、劇場、模擬功能、攜帶型以及平板電腦等的內部結構。而目前，由Fuseproject設計的，使用了一個變壓器樞鈕將手提電腦、電子書及路由器模式合而為一。

### 硬體規格

[OLPC-Frame.png](https://zh.wikipedia.org/wiki/File:OLPC-Frame.png "fig:OLPC-Frame.png")下不同工作。藉由點選圖示，[Wi-Fi啟動連結](../Page/Wi-Fi.md "wikilink")\]\]

2007年3月推出的[硬體規格如下](../Page/電腦硬體.md "wikilink")：

  - CPU: 433 [MHz](../Page/MHz.md "wikilink") [AMD
    Geode](../Page/AMD_Geode.md "wikilink") LX-700處理器，耗能0.8
    [瓦特](../Page/瓦特.md "wikilink")，搭載整合顯示卡。
  - 1200×900
    7.5吋[LCD](../Page/液晶顯示器.md "wikilink")（200 [dpi](../Page/解析度.md "wikilink")），在不同模式下耗能0.1到1.0瓦特。兩種模式分別如下：
      - 反射（背光燈關閉）單色模式，用在太陽光下的低耗能模式。這種模式提供非常清晰影像給高質文本。
      - 背光彩色模式， 非對稱式減少使用複雜的方法提供一個有效的解析，詳見以下細項。
  - 256 [MB](../Page/mebibyte.md "wikilink")
    [DDR](../Page/DDR.md "wikilink")266雙通道記憶體133 MHz
    [DRAM](../Page/DRAM.md "wikilink")（在2006年規格中宣稱只有128MB的記憶體\[6\]）
  - 1024 [KB](../Page/kibibyte.md "wikilink") (1 MB)
    快閃唯讀記憶體，使用開放源始碼的[LinuxBIOS和](../Page/LinuxBIOS.md "wikilink")[開放韌體](../Page/開放韌體.md "wikilink")
  - 1024 MB的SLC NAND [快閃記憶體](../Page/快閃記憶體.md "wikilink")
    （在2006年規格中宣稱只有512MB的快閃記憶體\[7\]）
  - 內建[SD記憶卡插槽](../Page/SD記憶卡.md "wikilink")\[8\]
  - 無線網路使「廣範圍」的 802.11b/g無線晶片組運作在一個低位元率(2 Mbit/s)以減少耗能。
  - [Marvell公司](../Page/Marvell_Technology_Group.md "wikilink")8388無線晶片，選擇的原因是基於它能在中央處理器被關掉的情況下仍能自主地傳送數據包。一枚[ARM包含在內](../Page/ARM_architecture.md "wikilink")。
  - 兩個可調整的[天線](../Page/天線.md "wikilink")。
  - 兼備[滑鼠及手寫功能的](../Page/滑鼠.md "wikilink")[觸控式屏幕](../Page/觸控式屏幕.md "wikilink")
  - 內建彩色攝錄機到屏幕右方，VGA解像度(640×480)
  - 內建[立體聲](../Page/立體聲.md "wikilink")[揚聲器](../Page/揚聲器.md "wikilink")
  - 內建[麥克風](../Page/麥克風.md "wikilink")
  - 3個外置的[通用序列匯流排](../Page/通用序列匯流排.md "wikilink")2.0
  - 電源

#### 故意删去的硬體

为了达到省电和长时间工作的效果, 在设计这个笔记本电脑的时候故意省略掉一些耗电高的硬件;
这个笔记本电脑没有[硬盘](../Page/硬盘.md "wikilink"),
无光驱(CD/DVD),无-{zh-hans:[软盘驱动器](../Page/软盘驱动器.md "wikilink");
zh-hant:[軟碟機](../Page/軟碟機.md "wikilink");}-和风扇.由于没有硬盘，ATA接口也显得没有必要了.这个电脑也没有PC卡槽,
但是提供了 [SD卡插槽](../Page/SD卡.md "wikilink") .

[打印机](../Page/打印机.md "wikilink"), [硬盘](../Page/硬盘.md "wikilink"),
[CD](../Page/CD.md "wikilink"),
[DVD光碟機](../Page/DVD.md "wikilink"),和其他周邊设备可以透過[通用串列匯流排口链接](../Page/通用串列匯流排.md "wikilink").更大的扩展可以通过SD卡插槽实现.
在原始设计中集成了手摇式发电机, 但是 Negroponte
在2006年[LinuxWorld大会上宣布手摇式发电机将不会集成在笔记本电脑中](../Page/LinuxWorld.md "wikilink"),但是将手摇或脚踩发电机会集成在另外的电源中.
\[9\]

#### 耗电量

在正常工作的情况下这个笔记本电脑在的功耗是2
[瓦特的电力](../Page/瓦特.md "wikilink")。功耗比普通的笔记本电脑的45 W
功耗要小的。\[10\]

在电子书的模式下，所有的硬件和配件及系统都将减少用电量，显示屏降低为单色显示（包括各种显示屏的背光）。当用户转换到不同的页数时，整个系统将会从睡眠唤醒，显示出新的页面然后重返睡眠状态。在电子书的模式下，笔记本电脑的功率为
0.3 至 0.8 W之间。

[OLPC-XO_in_Color.jpg](https://zh.wikipedia.org/wiki/File:OLPC-XO_in_Color.jpg "fig:OLPC-XO_in_Color.jpg")

#### Display

OLPC 笔记本使用的是特殊的
[LCD](../Page/液晶显示器.md "wikilink")，不只沒有反光的問題，將背光關閉後螢幕會被切成黑白顯示。由於該
LCD 的顏色排列不像一般的液晶螢幕，在黑白狀態下可達 1200×900，彩色的可見解析度為 984×738，而且另外有顯示控制器 (DCON)
可以在電腦睡眠時將畫面輸出定格。\[11\]

### 硬體規格

（根據2006/11/30的BTest-1機型，[參考資料：OLPC
Wiki](http://wiki.laptop.org/go/Hardware_specification)）

\==== 外觀 ====

  - 尺寸：193mm × 229mm × 64mm
  - 質量：小於1.5KG
  - 配置：可旋轉，可逆向的顯示。防灰塵、防溼氣的system enclosure。

#### 核心

  - CPU：[AMD Geode](../Page/AMD_Geode.md "wikilink") LX-700 at
    0.8W，內建圖形控制器。
  - CPU頻率：433MHz。
  - 相容性：IBM X86/X87 系列相容。
  - 晶片組：AMD CS5536 [南橋晶片](../Page/南橋晶片.md "wikilink")。
  - 顯示晶片：整合於 AMD Geode CPU 中。使用 [UMA](../Page/共享內存.md "wikilink") 技術。
  - BIOS：1024KB SPI-interface 快闪ROM; LinuxBIOS open-source BIOS; Open
    Firmware bootloader。
  - 嵌入式控制器 (for production), ENE KB3700
  - DRAM：256 MB。Dual DDR266-133 Mhz。
  - 儲存介面：無硬碟。以內建 SLC NAND 1024 MB 快閃記憶體取代。
  - Drives：無旋轉式媒體。

#### 顯示

  - 螢幕：7.5 吋雙重模式 TFT LCD。
  - 可視區域：152.4 mm × 114.3 mm。
  - 解析度：1200x900 (200dpi)。
  - 單色顯示：高解析度、反射式單色模式。
  - 彩色顯示：標準解析度、梅花式樣、彩色模式。

#### 內建週邊

  - 輸入裝置：70多鍵，1.2mm薄膜鍵盤。按键由橡胶密封。
  - 游標控制鍵：五鍵游標控制手把；四方向鍵 + Enter。
  - 觸控板：双电阻／电容触控板，支持手写输入。
  - 音效：Analog Devices AD1888, AC97-compatible audio codec; stereo, with
    dual internal speakers; monophonic, with internal microphone and
    using the Analog Devices SSM2211 for audio amplification
  - 無線通訊：[IEEE 802.11b](../Page/IEEE_802.11.md "wikilink")/g 無線晶片。
  - 狀態指示：電力、電池、WiFi；visible lid open or closed
  - 攝影機：640x480 解析度，30 FPS。

#### 外部連結

  - 电源接口: 2-pin 直流输入, 10到25 V, -23到-10 V
  - 线性输出: 标准 3.5mm 3-pin 立体声接口
  - 麦克风接口: 标准 3.5mm 2-pin 单声道; selectable sensor-input mode
  - 擴充性：3個 [USB](../Page/USB.md "wikilink") 2.0 插槽，SD卡插槽。
  - 最大电流: 500 mA

#### 電力

  - 電力：10–25V DC插座供電或7.2V鎳氫電池。
  - 电池形式: 5芯6V密封可拆卸式
  - 電量：22.8 瓦·時
  - 電池種類：NiMH
  - 包装保护：集成包类型标识
  - 內置熱感應器
  - 內置多保險絲電流限制器
  - 循环寿命：最低2000充电/放电循环。
  - 电源管理将是关键

#### BIOS／啟動程式

  - LinuxBIOS is our BIOS for production units
  - Open Firmware :作啟動程式。

### 軟體

在此種手提電腦上，所預裝的全部軟體都將是[開放原始碼的](../Page/開放原始碼.md "wikilink")。（2006年11月）計畫中預裝的軟體有：

  - 作業系統：[Fedora Core](../Page/Fedora_Core.md "wikilink")
    [Linux](../Page/Linux.md "wikilink")。
  - [GUI](../Page/GUI.md "wikilink")：Sugar，使用[Python語言撰寫](../Page/Python.md "wikilink")。
  - 瀏覽器：[Mozilla](../Page/Mozilla.md "wikilink")
    [Firefox](../Page/Firefox.md "wikilink")。
  - 文字處理器：一個基於[AbiWord的文字處理器](../Page/AbiWord.md "wikilink")。
  - 電子郵件：[Gmail網路郵件](../Page/Gmail.md "wikilink")。
  - 即時通訊：網路語音和聊天程式。
  - 多媒體：Jean Piché's TamTam，以及Mplayer或Helix。
  - [程式開發環境](../Page/程式開發環境.md "wikilink")：[Python](../Page/Python.md "wikilink")，[JavaScript](../Page/JavaScript.md "wikilink")，CSound，Squeak
    / Etoys，Logo，an open source JVM and/or a Flash VM

[蘋果電腦](../Page/蘋果電腦.md "wikilink")[執行長](../Page/執行長.md "wikilink")[史蒂夫·乔布斯曾經免費提供](../Page/史蒂夫·乔布斯.md "wikilink")[Mac
OS
X給OLPC的電腦使用](../Page/Mac_OS_X.md "wikilink")，但根據[MIT一名退休教授Seymour](../Page/MIT.md "wikilink")
Papert的說法，設計師需要的是一個能修補的[作業系統](../Page/作業系統.md "wikilink")。「我們拒絕了，因為它不是[开源软件](../Page/開放原始碼.md "wikilink")。」因此選了Linux作為[作業系統](../Page/作業系統.md "wikilink")。
2006年8月4日，[維基媒體基金會宣佈OLPC將內建精選過的離線版](../Page/維基媒體基金會.md "wikilink")[維基百科](../Page/維基百科.md "wikilink")。維基媒體基金會理事會主席[吉米·威爾士說](../Page/吉米·威爾士.md "wikilink")：「[OLPC的任務與我們的目標](../Page/OLPC.md "wikilink")——將百科辭典似的知識，免費地散佈到世界上的每一個人——不謀而合。」

## 註釋

<div class="references-small">

<references />

</div>

## 參考資料

### 中文

  - [2007新版OLPC中文主页](http://www.laptop.org/zh-CN/)
  - 《今週刊》No.499：林百里、郭台銘都看好的人海商機，做40億人生意。低價電腦產生，只要100美元，2006.07.17
  - 龐文真，[{{〈}}一百美元買到一部電腦不是夢{{〉}}](http://www.bnext.com.tw/article/view/id/3314)，出自《數位時代雙週》，2006.01.01。

### 英文

  - [One Laptop per Child 官方網站](http://laptop.org/)
  - [OLPC Wiki](http://wiki.laptop.org/)
  - [One Laptop Per Child News](http://www.olpcnews.com/)（更新快速）

### 相關條目

  - [OLPC](../Page/OLPC.md "wikilink")
  - [E化教室](../Page/E化教室.md "wikilink")

[Category:筆記型電腦](../Category/筆記型電腦.md "wikilink")
[Category:Netbook](../Category/Netbook.md "wikilink")
[Category:美國組織](../Category/美國組織.md "wikilink")
[Category:非营利组织](../Category/非营利组织.md "wikilink")

1.  {{ cite web | url =
    <http://wiki.laptop.org/go/Our_software#What_software_will_be_used_with_the_.24100_laptop.3F>
    | title = OLPC's Software | publisher = One Laptop Per Child |
    accessdate = 2006-01-27 }}

2.
3.  [One million OLPC laptop orders
    confirmed](http://www.itworld.com/Tech/2987/070215olpc/)

4.  {{ cite
    web|url=<http://digital50.com/news/items/BW/2001/07/14/20070112005706/one-laptop-per-child-has-no-plans-to-commercialize-xo-computer.html>
    |title=One Laptop per Child Has No Plans to Commercialize XO
    Computer |publisher=Business Wire |accessdate=2007-01-16
    |deadurl=yes
    |archiveurl=<https://web.archive.org/web/20070120091352/http://digital50.com/news/items/BW/2001/07/14/20070112005706/one-laptop-per-child-has-no-plans-to-commercialize-xo-computer.html>
    |archivedate=2007-01-20 }}

5.  {{ cite web | url =
    <http://arstechnica.com/news.ars/post/20070329-olpc-xo-manufacturer-to-sell-budget-portables-in-developed-countries.html>
    | title = OLPC manufacturer to sell $200 laptop | publisher =
    Arstechnica | accessdate = 2007-03-29 }}

6.  {{ cite web | url = <http://lwn.net/Articles/188060/> | title =
    Interview: Jim Gettys (Part I)|publisher = LWN.net|date = June 28,
    2006 | accessdate = }}

7.
8.  Microsoft looking to run Windows on OLPC, VNUnet,
    <https://web.archive.org/web/20070927195019/http://www.vnunet.com/vnunet/news/2170209/microsoft-looking-windows-olpc>

9.

10. [For $150, Third-World Laptop Stirs Big
    Debate](http://www.nytimes.com/2006/11/30/technology/30laptop.html).
    *The New York Times*, 30 November 2006.

11. [Display — OLPC](http://wiki.laptop.org/go/Display)