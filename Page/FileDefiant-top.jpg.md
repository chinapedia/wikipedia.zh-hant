## 摘要

| 描述摘要 | [联邦星舰勇抗号 (NX-74205)](../Page/联邦星舰勇抗号_\(NX-74205\).md "wikilink")                                                                                                                         |
| ---- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 來源   | 《[星际旅行：深空九号](../Page/星际旅行：深空九号.md "wikilink")》，该图来自[Ex Astris Scientia](../Page/:en:Ex_Astris_Scientia.md "wikilink")：[1](http://www.ex-astris-scientia.org/scans/other/defiant-top.jpg) |
| 日期   | [2007年](../Page/2007年.md "wikilink")[7月31日上传](../Page/7月31日.md "wikilink")                                                                                                               |
| 作者   | [*Paramount Pictures*](../Page/派拉蒙影业.md "wikilink")                                                                                                                                      |
| 許可   | 图片版权属于[*Paramount Pictures*](../Page/派拉蒙影业.md "wikilink")，许可见下 {{\#switch: 档案其他版本（可留空）                                                                                                   |

## 许可协议

[Category:星艦奇航記](../Category/星艦奇航記.md "wikilink")