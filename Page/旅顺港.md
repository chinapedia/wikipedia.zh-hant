[Lushunkou_01.JPG](https://zh.wikipedia.org/wiki/File:Lushunkou_01.JPG "fig:Lushunkou_01.JPG")
**旅顺港**（，或称），亦称**旅顺军港**\[1\]，是[辽宁省](../Page/辽宁省.md "wikilink")[大连市](../Page/大连市.md "wikilink")[旅顺口区的一座](../Page/旅顺口区.md "wikilink")[不冻港](../Page/不冻港.md "wikilink")，位于[白玉山南](../Page/白玉山.md "wikilink")。是[晚清时](../Page/晚清.md "wikilink")，为[北洋水师所兴建的港口](../Page/北洋水师.md "wikilink")。亦是近代以来，[中国北方最重要的军港之一](../Page/中国北方.md "wikilink")。现由[中国人民解放军海军北海舰队使用](../Page/中国人民解放军海军北海舰队.md "wikilink")，属[海军旅顺基地](../Page/中国人民解放军海军旅顺基地.md "wikilink")。

## 晚清

1880年代，[清政府在](../Page/清政府.md "wikilink")[旅顺口为](../Page/旅顺口.md "wikilink")[北洋水师修建此处军港](../Page/北洋水师.md "wikilink")。负责修建工程的[袁保龄认为](../Page/袁保龄.md "wikilink")，此处港口为“[北洋第一险碍](../Page/北洋.md "wikilink")”。与其他北洋港口相比，旅顺港是建设军港的首选。1880年，清政府批准修建旅顺港坞，1890年9月，工程竣工\[2\]。当时修建的[船坞留存至今](../Page/船坞.md "wikilink")，2013年时以“[旅顺船坞旧址](../Page/旅顺船坞旧址.md "wikilink")”之名列为[第七批全国重点文物保护单位](../Page/第七批全国重点文物保护单位中的近现代重要史迹及代表性建筑.md "wikilink")。

## 日俄（苏）占时期

## 现代

## 图片

## 注释

[Category:中国军港](../Category/中国军港.md "wikilink")
[Category:中国人民解放军海军旅顺基地](../Category/中国人民解放军海军旅顺基地.md "wikilink")
[Category:旅顺口区](../Category/旅顺口区.md "wikilink")

1.

2.