**禹**，[远古时期](../Page/远古时期.md "wikilink")[中国神话人物](../Page/中国神话人物.md "wikilink")，**[姒姓](../Page/姒姓.md "wikilink")**，**[夏-{后}-氏](../Page/夏后氏.md "wikilink")**，传说[名](../Page/名.md "wikilink")**文命**，後世尊稱为**大禹**，是[黃帝軒轅氏玄孫](../Page/黃帝軒轅氏.md "wikilink")，因在[大禹治水中成功治理洪水之患的故事而广为人知](../Page/大禹治水.md "wikilink")。生於公元前2123年，卒於前2055年。传说中，禹是中国历史上第一个国家[夏朝的开创者](../Page/夏朝.md "wikilink")，定都于[安邑](../Page/安邑.md "wikilink")（今[山西](../Page/山西.md "wikilink")[夏县](../Page/夏县.md "wikilink")）。禹在尧时被封为夏伯，因又史称**伯禹**、**夏禹**。

## 生平

### 早期生活

以[黃帝軒轅氏到禹](../Page/黃帝.md "wikilink")，共八世：禹的父親[鯀](../Page/鯀.md "wikilink")，鯀的五世祖叫[顓頊](../Page/顓頊.md "wikilink")\[1\]，顓頊的父親叫[昌意](../Page/昌意.md "wikilink")，昌意是[嫘祖为黄帝所生的次子](../Page/嫘祖.md "wikilink")。禹的父親鯀被封在[汶山石紐地區](../Page/汶山.md "wikilink")（今四川省[北川縣](../Page/北川縣.md "wikilink")）\[2\]，母親是[有莘氏之女](../Page/有莘氏.md "wikilink")，名叫女志，也叫女嬉\[3\]。禹幼年隨父親鯀東遷，來到中原。其父鯀被帝[堯封於崇](../Page/堯.md "wikilink")（即中岳[嵩山](../Page/嵩山.md "wikilink")）。帝[堯時](../Page/堯.md "wikilink")，[中原洪水氾濫造成水患災禍](../Page/中原.md "wikilink")，百姓愁苦不堪。帝堯命令鯀治水，鯀受命治理洪水，盗窃了天帝的息壤来填埋洪水，却九年也未能平息水災，结果被祝融所杀\[4\]，死后化为黄龙\[5\]。接著帝舜命鯀的兒子禹繼任治水之职。

### 處理洪水

[大禹治水圖.png](https://zh.wikipedia.org/wiki/File:大禹治水圖.png "fig:大禹治水圖.png")治水圖\]\]
禹立即與益和[后稷一起](../Page/后稷.md "wikilink")，召集百姓前來協助，他視察[河道](../Page/河道.md "wikilink")，並檢討鯀治水失敗的原因。禹總結了其父親治水失敗的教訓，改革治水方法以疏導河川治水為主導，用水利向低處流的自然趨勢，疏通了九河。禹不仅率領百姓餐風露宿，疏通河道，更有[应龙为他画地成河](../Page/应龙.md "wikilink")，并用神力为禹开辟了龙门\[6\]\[7\]。禹堅韌不拔，勇於開拓的精神，經過了十三年治理，終於成功消除了[中原氾濫的洪水](../Page/中原.md "wikilink")，后世道教神话中称禹治水时曾得到黄帝留下的神书十二卷\[8\]。大禹整治[黃河水患有功](../Page/黃河.md "wikilink")，受[舜](../Page/舜.md "wikilink")[禪讓繼帝位](../Page/禪讓.md "wikilink")。夏禹王登天子之位，並以自己的封國[夏為天下之號](../Page/夏部落.md "wikilink")，宣告夏王朝正式建立。禹王子[啟王是](../Page/啟.md "wikilink")[夏朝的繼位](../Page/夏朝.md "wikilink")[天子是為王太子](../Page/天子.md "wikilink")。建碑《大禹陵》。

治水期間，禹翻山越嶺，淌河過川，拿著量測儀工具，從西向東，一路測度地形的高低，樹立標桿，規划水道。他帶領治水的民工，走遍各地，根據標桿，逢山開山，遇窪築堤，以疏通水道，引洪水入[海](../Page/海.md "wikilink")。禹為了治水，費盡腦筋，不怕勞苦，從來不敢休息。他與[塗山氏](../Page/塗山.md "wikilink")[女嬌新婚不久](../Page/女嬌.md "wikilink")\[9\]，就離開妻子，踏上治水的道路。後來，他路過家門口，聽到妻子生產，兒子呱呱墜地的聲音，扶正青蔥色的頭衣後，不敢進家門。第三次經過家鄉的時候，其子[啟正被](../Page/啟.md "wikilink")[母親抱在懷裏](../Page/母親.md "wikilink")，他已經懂得叫爸爸，揮動小手，和禹打招呼，禹只是向妻兒揮揮手，表示自己看到他們了，還是沒有停下來\[10\]。因治洪水有功，人們為表達對禹的感激之情，尊稱他為“大禹”，即「偉大的禹」。
在治水的過程中，禹走遍天下，對各地的地形、習俗、物產等皆瞭如指掌。\[11\]禹重新將天下規劃為[九個州](../Page/九州_\(中国\).md "wikilink")，並制定了各州的貢物品種。帝夏禹王還規定：天子帝畿以外五百里的地區叫[甸服](../Page/甸服.md "wikilink")，再外五百里叫[侯服](../Page/侯服.md "wikilink")，再外五百里叫[綏服](../Page/綏服.md "wikilink")，再外五百里叫[要服](../Page/要服.md "wikilink")，最外五百里叫[荒服](../Page/荒服.md "wikilink")。甸、侯、綏三服，進納不同的物品或負擔不同的勞務。[要服](../Page/要服.md "wikilink")，不納物服役，只要求接受管教、遵守法制政令。荒服，則根據其習俗進行管理，不強制推行中朝政教。
（神話則是說：禹具有神力，治水時，會化身成一頭巨熊，用來劈山開路、造溝清沼。其新婚妻女嬌替禹送餐到工程地方，她不知禹能化身，見巨熊碎石，大驚，遂昏死。待禹完成工作之後，見到女嬌的屍體，大哭，但女嬌隨後誕下啟，讓禹無後顧之憂。）

### 即位

帝舜在位三十三年時，正式把天子位禪讓給禹。十七年以後，舜在南巡中逝世。三年治喪結束，禹避居夏地的一個小邑[陽城](../Page/陽城.md "wikilink")，將帝位讓給舜的兒子[商均](../Page/商均.md "wikilink")。但天下的[諸侯都離開商均去朝見禹王](../Page/諸侯.md "wikilink")。諸侯的擁戴下，五十三歲的禹王正式即王位，以[安邑](../Page/安邑.md "wikilink")（今[山西](../Page/山西.md "wikilink")[夏縣](../Page/夏縣.md "wikilink")）為都城，國號[夏](../Page/夏朝.md "wikilink")。分封[丹朱於唐](../Page/丹朱.md "wikilink")，分封商均於虞。改定曆日稱為夏曆，以建寅之月為[正月](../Page/正月.md "wikilink")。又收取天下的[銅](../Page/銅.md "wikilink")，鑄成了[九鼎](../Page/九鼎.md "wikilink")，作为天下共主的象征。《[說苑](../Page/說苑.md "wikilink")》記載大禹“卑小宮室，損薄飲食，土階三等，衣裳細布。”\[12\]禹在位的第十五年，其子[啟王繼位](../Page/啟.md "wikilink")[夏朝的](../Page/夏朝.md "wikilink")[天子](../Page/天子.md "wikilink")，享年六十八歲。

## 影響

夏禹王是為中華民族的歷史發展做出了巨大貢獻的歷史人物。他的功績不僅在於治理洪水，發展國家生產，使人民安居樂業，更重要的是結束中國原始社會部落聯盟的社會組織形態，創造了“國家”這一新型的社會政治形態。夏禹王完成了國家的建立，用[階級代替](../Page/社会階級.md "wikilink")[原始社會](../Page/原始社會.md "wikilink")，以[文明時代社會代替](../Page/文明時代社會.md "wikilink")[野蠻時代社會](../Page/野蠻時代社會.md "wikilink")，推動[中國帝王室歷史沿革發展](../Page/中國帝王室歷史沿革.md "wikilink")。

## 考古证据

  - 与禹王同时代的出土文物及商朝甲骨文中尚未发现关于禹王的记载。目前所能找到的最早提到禹王的文物是约一千年以后西周的[遂公盨](../Page/遂公盨.md "wikilink")[1](https://web.archive.org/web/20111210140537/http://www.yingbishufa.com/ldbt/0108.htm)，此外还有[齐侯钟](../Page/齐侯钟.md "wikilink")、[秦公簋](../Page/秦公簋.md "wikilink")。
  - [顾颉刚经过考证](../Page/顾颉刚.md "wikilink")，认为“禹”这个字的字源与龍有一定的关系，因此關於大禹的崇拜可能最早來自對更早的動物紋飾的誤讀，並和關於龍王的傳説有關，至周代初年原先的“禹”传说已化为“最早的人”，而后世传说中比禹更早的传说人物（如尧、舜等）则为周人所创造。然而这种观点被别人夸张成“禹是一條龍”，再被夸张成為“禹是一條蟲”。這種观点在当时受到史學家[柳翼謀等人的反对](../Page/柳翼謀.md "wikilink")，但由于没有同时代甚至离开禹王时代一千年以内的文物或文字记载可以佐证，因此无法确凿的辨别禹传说最早的形态。
  - 2016年7月[《科學》期刊刊登論文](../Page/科学_\(期刊\).md "wikilink")，[南京師範大學地理系教授吳慶龍和普度大學教授葛蘭傑聯合研究](../Page/南京師範大學.md "wikilink")，表明公元前1920年左右青海省地區有過大地震引發的山崩，之後堰塞湖阻斷[黃河](../Page/黃河.md "wikilink")，幾個月後積水滿溢時潰堤，導致大洪水，為禍下游2000公里，洪水高出現代河水位達38公尺。所以大禹年代大洪水是可能存在的，只是年代後推並非是一般認知的五六千年以上而是四千年前左右為大禹時代，那時若有一人帶領眾人疏導河水求取生路，最後取得政治領導權是有可能的。\[13\]

## 另类观点

  - 曾上过[百家讲坛的](../Page/百家讲坛.md "wikilink")[紀連海於一期電視节目中宣稱](../Page/紀連海.md "wikilink")「三過家門不入」的大禹因與女子[瑤姬在治水過程中相戀](../Page/瑤姬.md "wikilink")。紀連海認為，瑤姬將傳說中寫有治水妙法的「[紅寶書](../Page/丹玉之書.md "wikilink")（[丹玉之書](../Page/丹玉之書.md "wikilink")）」作為定情物獻給大禹，以確認大禹是在當時的「[走婚](../Page/走婚.md "wikilink")」（即丈夫不需與妻子共居）習俗下，已成為她的丈夫。而大禹則編造出「巫山神女瑤姬」協助自己治水的故事以矇騙故鄉的髮妻，因此羞於面對妻子，故曾三過家門不入。此观点荒诞不经，[四川省社會科學院](../Page/四川省社會科學院.md "wikilink")[禹羌文化研究所所長](../Page/禹羌文化研究所.md "wikilink")[謝興鵬痛斥此為無稽之談](../Page/謝興鵬.md "wikilink")，其表示瑤姬為傳說中[王母娘娘的女兒](../Page/王母娘娘.md "wikilink")，屬於「[神](../Page/神.md "wikilink")」，故無法與身為「[人類](../Page/人類.md "wikilink")」的大禹相戀。謝興鵬並要求立即停播該集節目。\[14\]

## 大禹祭祀

### 國祭

[夏桀被](../Page/夏桀.md "wikilink")[流放後](../Page/流放.md "wikilink")，[商湯封夏王室](../Page/商湯.md "wikilink")[姒姓一支](../Page/姒姓.md "wikilink")[貴族於](../Page/貴族.md "wikilink")[杞國](../Page/杞國.md "wikilink")，以奉祀[宗廟祖先](../Page/宗廟.md "wikilink")。“杞在商時，或封或絕”。[周朝](../Page/周朝.md "wikilink")[武王克殷後](../Page/武王克殷.md "wikilink")，封禹的後裔[東樓公於杞地](../Page/東樓公.md "wikilink")，延續杞國國祚，主管對禹的祭祀。

大禹王祭祀原为国家祭祀。目前，公祭禹王陵典礼已经恢复为国祭\[15\]。

### 道教與民間信仰

[道教中](../Page/道教.md "wikilink")，禹被尊為[三官大帝中的](../Page/三官大帝.md "wikilink")**水官大帝**，[誕日為十月十五日](../Page/神誕.md "wikilink")[下元節](../Page/下元節.md "wikilink")。另有人尊其為[水仙大王](../Page/水仙大王.md "wikilink")。

## 禹陵

“禹東巡狩至於會稽而崩”。[大禹陵位於](../Page/大禹陵.md "wikilink")[浙江](../Page/浙江.md "wikilink")[紹興東南郊的](../Page/紹興.md "wikilink")[會稽山山麓](../Page/會稽山.md "wikilink")，由禹陵、禹祠、禹廟三大建築群組成\[16\]。

在[绍兴](../Page/绍兴.md "wikilink")[禹陵村](../Page/禹陵村.md "wikilink")，夏禹[姒姓后代世代为禹守陵至今](../Page/姒姓.md "wikilink")\[17\]，但[仓修良認為此说不可信](../Page/仓修良.md "wikilink")。\[18\]\[19\]

## 參見

  -
  - [盤古王表](../Page/盤古王表.md "wikilink")（相傳為大禹所作）

  - [喇家遺址](../Page/喇家遺址.md "wikilink")

  - [禹山](../Page/禹山.md "wikilink")

  - [三官大帝](../Page/三官大帝.md "wikilink")

  - [水仙王](../Page/水仙王.md "wikilink")

  - [北川縣](../Page/北川縣.md "wikilink")

  - [羌族](../Page/羌族.md "wikilink")

  - [后羿](../Page/后羿.md "wikilink")

  - [顧頡剛](../Page/顧頡剛.md "wikilink")

  - [夏朝](../Page/夏朝.md "wikilink")

  - [禹王治水](../Page/鲧禹治水.md "wikilink")

  - [禹州市](../Page/禹州市.md "wikilink")

  - [九州 (中國)](../Page/九州_\(中國\).md "wikilink")

  - [九鼎](../Page/九鼎.md "wikilink")

  - 《[大禹謨](../Page/w:大禹謨.md "wikilink")》

  - [離](../Page/離.md "wikilink")

## 參考資料

  - 中文维基文库 - 《[尚书](../Page/尚书.md "wikilink")·大禹谟》

<div class="references-small">

<references />

</div>

## 外部連結

  - \[<http://www.epochtimes.com/b5/nf4966.htm>　【千古英雄人物】禹\]
  - 李學勤：[〈遂公盨與大禹治水傳說〉](http://www.xinfajia.net/1245.html)（2003）

[Category:中国上古人物](../Category/中国上古人物.md "wikilink")
[Category:道教尊神](../Category/道教尊神.md "wikilink")
[Category:夏朝君主](../Category/夏朝君主.md "wikilink")
[Y](../Category/中國民間信仰.md "wikilink")
[Y](../Category/水神.md "wikilink")
[Y](../Category/水仙尊王.md "wikilink")
[Category:下元節](../Category/下元節.md "wikilink")
[Category:夏朝人](../Category/夏朝人.md "wikilink")
[夏](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")

1.  皇甫谧和《世本》都认为鲧是颛顼之子，但《汉书·律历志》记““颛顼五代而生鲧”，司马贞《史记索隐》认为汉书的说法较为可信。《史记索隐》“按：鲧既仕尧，与舜代系殊悬，舜即颛顼六代孙，则鲧非是颛顼之子。盖班氏之言近得其实。”
2.  《蜀王本记》载：“禹本汶山郡广柔县人，生于石钮”；《水经·沫水注》称：“广柔（今北川）县，有石钮乡禹所生也，今夷人共管之”。
3.  《吴越春秋》记：“母吞珠孕禹，剖胁而产，家在西羌，地曰石钮，在蜀西川也”。
4.  《山海经·海内经》：“洪水滔天，鲧窃帝之息壤以堙洪水，不待帝命。帝令祝融杀鲧于羽郊。”
5.  《归藏》：“滔滔洪水，无所止极，伯鲧乃以息石息壤以填洪水，死，三岁不腐，副之以吴刀，化为黄龙。”
6.  《楚辞》：“应龙何画？河海何历？”[王逸注此句云](../Page/王逸.md "wikilink")：“或曰禹治洪水时，有神龙以尾画地，导水径所当决者，因而治之”
7.  《山东通志》：“应龙画而伊阙凿”
8.  《龙瑞观禹穴阳明洞天图经》：“风后日：黄帝藏於会稽之山，其坎深千尺，镇以盘石。又《遁甲开山图》日：禹治水至会稽，宿於衡岭，宛委之神奏玉匮之书十二卷以授禹，禹未及持之，四卷飞入泉，四卷飞上天，禹得四卷，开而视之，乃《遁甲开山图》”
9.  《史记·夏本纪》“予娶涂山”[司马贞索隐](../Page/司马贞.md "wikilink")：“《系本》曰‘涂山氏女名女媧’，是禹娶涂山氏号女媧也。”亦称“
    女娇”、“ 女趫”。
10. 《虞书·益稷》篇云：“予创若时，娶于涂山，辛壬癸甲，启呱呱而泣。予弗子，惟荒度土功。”《孟子·膝文公上》：“禹八年于外，三过其门而不入。”《吕氏春秋》曰：“禹娶涂山氏女，不以私害公，自辛至甲四日，复往治水。”《史記·夏本紀》：“禹傷先人父鯀功之不成受誅，乃勞身焦思，居外十三年，過家門不敢入。”[常璩](../Page/常璩.md "wikilink")《华阳国志·巴志》：“
    禹娶於涂，辛壬癸甲而去，生子启呱呱啼不及视，三过其门而不入室，务在救时，今江州涂山是也，帝禹之庙铭存焉。”
11. 《史记·五帝本纪》言：“禹践天子位，尧子丹朱、舜子商均皆有疆土，以奉先祀，服其服，礼乐如之。”
12. 劉向《說苑·反質》
13. [中時-大禹治水可能性大增](http://www.chinatimes.com/realtimenews/20160805002404-260401)
14.
15. [中国将公祭大禹陵典礼升格为国祭](http://news.xinhuanet.com/society/2007-03/16/content_5858086.htm)
16. [大禹陵](http://www.sx.gov.cn/portal/html/20060321000084/20060324000053.html)
17. [姒姓人为大禹守了144代陵](http://www.zjol.com.cn/gb/node2/node802/node1155/node200575/node200592/userobject15ai2234340.html)
18. 仓修良《关于谱学研究的几点意见》 历史研究 1997年 第5期
19. 仓修良《史家·史籍·史学》 山东教育出版社 2000年 ISBN 7-5328-2942-1 989页