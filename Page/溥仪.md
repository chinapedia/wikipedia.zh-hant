**溥儀**（<ref>《滿漢大辭典》，安雙成

`編，遼寧民族出版社，1993年出版，第1172頁`</ref>`，``），`[`愛新覺羅氏`](../Page/愛新覺羅.md "wikilink")`
， `[`乳名`](../Page/乳名.md "wikilink")**`午格`**`，`[`字`](../Page/表字.md "wikilink")**`耀之`**`，`[`號`](../Page/號.md "wikilink")**`浩然`**\[1\]`，`[`英語名`](../Page/英語人名.md "wikilink")`「亨利」（``），`[`西藏方面尊為`](../Page/清朝治藏歷史.md "wikilink")`「`**[`文殊皇帝`](../Page/文殊皇帝.md "wikilink")**`」``，`[`年號`](../Page/年號.md "wikilink")`「`[`宣統`](../Page/宣統.md "wikilink")`」，後世通稱「`**`宣統皇帝`**`」。`

溥儀正式[登基時年僅](../Page/登基.md "wikilink")3歲，其實權由父親攝政王[載灃掌握](../Page/載灃.md "wikilink")。[辛亥革命以後](../Page/辛亥革命.md "wikilink")，溥儀被[袁世凱強逼退位](../Page/袁世凱.md "wikilink")，故亦有「**清遜帝**」或「**清廢帝**」之稱。
1917年，溥儀在[張勳等人的支持和幫助下](../Page/張勳.md "wikilink")，曾[短暫復辟但最終失敗](../Page/張勳復辟.md "wikilink")。[1934年](../Page/1934年.md "wikilink")，溥儀在日本支持和幫助下登基成為[滿洲國皇帝](../Page/满洲国.md "wikilink")，年號「[康德](../Page/康德_\(满洲国\).md "wikilink")」，故又得名「**康德皇帝**」。

溥儀是大清帝國第十二位君主、[清兵入關以來的第十位皇帝](../Page/明清战争.md "wikilink")，是清朝最後一任皇帝即[清末帝和公認的](../Page/清末帝.md "wikilink")「[中國最後一位皇帝](../Page/中国皇帝.md "wikilink")」即「末代皇帝」，亦是全世界唯一登基三次和退位三次的皇帝。

## 家世

[PuYi_1909.jpg](https://zh.wikipedia.org/wiki/File:PuYi_1909.jpg "fig:PuYi_1909.jpg")和弟弟[溥杰身边](../Page/溥杰.md "wikilink")。\]\]

溥儀的祖父為[道光帝七子](../Page/道光帝.md "wikilink")、[咸豐帝之弟](../Page/咸豐帝.md "wikilink")[奕譞](../Page/奕譞.md "wikilink")，外祖父為[榮祿](../Page/榮祿.md "wikilink")，生父[載灃为](../Page/載灃.md "wikilink")[光绪帝之弟](../Page/光绪帝.md "wikilink")，后继承[醇親王爵位](../Page/醇親王.md "wikilink")，母嫡福晋[幼兰](../Page/幼兰.md "wikilink")。光绪三十四年冬（公元1908年），光绪帝载湉病重，[慈禧太后下令将溥仪养育在宫中](../Page/慈禧太后.md "wikilink")。不久光绪帝去世，慈禧太后命溥仪继承皇统，[过继于](../Page/过继.md "wikilink")[同治帝载淳](../Page/同治帝.md "wikilink")，同时兼承光绪帝之[祧](../Page/祧.md "wikilink")，一人祧两房\[2\]。

## 生平经历

### 早年生涯

[Xuantong.jpg](https://zh.wikipedia.org/wiki/File:Xuantong.jpg "fig:Xuantong.jpg")

[光绪三十二年春正月十四日](../Page/光绪.md "wikilink")（1906年2月7日），溥儀出生在北京[醇親王府](../Page/醇親王府.md "wikilink")（中國[北京市西城区后海北沿](../Page/北京市.md "wikilink")44号）。祖父是[道光皇帝旻寧七子](../Page/道光帝.md "wikilink")[咸豐皇帝奕詝之弟](../Page/咸豐帝.md "wikilink")[奕譞](../Page/奕譞.md "wikilink")，外祖父是[榮祿](../Page/榮祿.md "wikilink")，父親是[載灃為](../Page/載灃.md "wikilink")[光緒皇帝載湉之弟](../Page/光緒帝.md "wikilink")，後繼承[醇親王爵位](../Page/醇親王.md "wikilink")，母親是[蘇完瓜爾佳·幼蘭](../Page/蘇完瓜爾佳·幼蘭.md "wikilink")。

[載灃生了](../Page/載灃.md "wikilink") 4
個兒子，依次為溥儀、二子[溥傑](../Page/溥傑.md "wikilink")（1907年－1994年）、三子[溥倛](../Page/溥倛.md "wikilink")（1915年－1918年）、四子[溥任](../Page/溥任.md "wikilink")（1918年－2015年）與7個女兒，依次為長女[韞媖](../Page/韞媖.md "wikilink")（1909年－1925年）、次女[韞龢](../Page/韞龢.md "wikilink")（1911年－2001年）、三女[韞穎](../Page/韞穎.md "wikilink")（1913年－1992年）、四女[韞嫻](../Page/韞嫻.md "wikilink")（1914年－2003年）、五女[韞馨](../Page/韞馨.md "wikilink")（1917年－1998年）、六女[韞娛](../Page/韞娛.md "wikilink")（1919年－1982年）、七女[韞歡](../Page/韞歡.md "wikilink")（1921年－2004年）。

光緒三十四年冬季（1908年），光緒皇帝載湉患重病，11月13日三歲的溥儀被抱入皇城，[慈禧太后命令將溥儀養育在宮中](../Page/慈禧太后.md "wikilink")，11月14日光緒皇帝載湉病逝，慈禧太后命令溥儀繼承登基成為皇帝，[過繼於](../Page/過繼.md "wikilink")[同治帝](../Page/同治帝.md "wikilink")，同時[兼承光緒皇帝載湉之宗祧](../Page/兼祧.md "wikilink")，11月15日慈禧太后駕崩，享壽七十三歲《[清史稿](../Page/清史稿.md "wikilink")·本紀二十五·宣統皇帝本紀》：三十四年冬十月壬申，德宗疾大漸，太皇太后命教養宮內。癸酉，德宗崩，奉太皇太后懿旨，入承大統，為嗣皇帝，嗣穆宗，兼承大行皇帝之祧，時年三歲，溥仪小时候曾被太监欺负，所以导致他出现畸形性格。

### 第一次皇帝登基和退位

光绪皇帝去世後，1908年12月2日，奉[慈禧太后遺言交代](../Page/慈禧太后.md "wikilink")，溥儀在[紫禁城](../Page/紫禁城.md "wikilink")[太和殿內登基成為](../Page/太和殿.md "wikilink")[大清王朝第十二任皇帝](../Page/大清王朝.md "wikilink")，年號[宣統](../Page/宣統.md "wikilink")。年僅三歲，其父親[載灃擔任](../Page/載灃.md "wikilink")[監國](../Page/監國.md "wikilink")[攝政王](../Page/攝政王.md "wikilink")。\[3\]

1911年[武昌起義成功](../Page/武昌起義.md "wikilink")，大清帝國各[行省各自宣布](../Page/行省.md "wikilink")[獨立](../Page/獨立.md "wikilink")，脫離大清帝國政府管轄和控制，但大清帝國政府依然管轄和控制北京市附近省份，並派遣[袁世凱使用](../Page/袁世凱.md "wikilink")[北洋陸軍攻打革命黨人](../Page/北洋陸軍.md "wikilink")。南方革命軍與袁世凱商定若能成功逼使溥儀退位和結束大清政權，便讓他成為[中華民國大總統](../Page/中華民國大總統.md "wikilink")，是為[南北議和](../Page/南北議和.md "wikilink")。袁世凱便一面好言相勸，一面威逼利誘溥儀退位。

1912年2月12日（宣統三年十二月戊午），[隆裕皇太后以大清帝國皇太后名義宣布](../Page/隆裕太后.md "wikilink")《[退位詔書](../Page/宣統帝退位詔書.md "wikilink")》，溥儀正式退位，結束大清帝國自[順治皇帝福臨入主](../Page/順治帝.md "wikilink")[中原以来](../Page/中原.md "wikilink")268年的統治。

### 第二次皇帝登基和退位：張勳發動政變，復辟清朝

溥儀退位之後，因為[中華民國](../Page/中華民國.md "wikilink")[北洋政府的](../Page/北洋政府.md "wikilink")《[清室優待條件](../Page/清室優待條件.md "wikilink")》而能繼續居住在[紫禁城內](../Page/紫禁城.md "wikilink")，並保留「大清帝國」國號（只限在[紫禁城內](../Page/紫禁城.md "wikilink")），溥儀依然保留[宦官](../Page/宦官.md "wikilink")，[宮女在](../Page/宮女.md "wikilink")[紫禁城內供其使喚](../Page/紫禁城.md "wikilink")。

1917年7月1日，[北洋政府](../Page/北洋政府.md "wikilink")[陸軍](../Page/陸軍.md "wikilink")[定武上將軍](../Page/定武軍.md "wikilink")，[安徽省](../Page/安徽省.md "wikilink")[督軍](../Page/都督.md "wikilink")[張勳協同](../Page/張勳.md "wikilink")[陳寶琛](../Page/陳寶琛.md "wikilink")、[王士珍](../Page/王士珍.md "wikilink")、[陳光遠](../Page/陳光遠.md "wikilink")、[康有為](../Page/康有為.md "wikilink")、[劉廷琛](../Page/劉廷琛.md "wikilink")、[沈曾植和](../Page/沈曾植.md "wikilink")[勞乃宣等人發動](../Page/勞乃宣.md "wikilink")[政變](../Page/政變.md "wikilink")，宣佈[大清帝國](../Page/清朝.md "wikilink")[復辟](../Page/復辟.md "wikilink")。[張勳復辟](../Page/張勳.md "wikilink")[大清帝國的行動遭到共和派系勢力的反對與攻擊](../Page/清朝.md "wikilink")，在[北洋政府各界壓力和不滿之下](../Page/北洋政府.md "wikilink")，復辟行動僅12天便宣告失敗，溥儀也第二次宣布退位，結束[大清帝國政權](../Page/清朝.md "wikilink")。

#### 溥儀與莊士敦時期

[Puyi's_schoolbook_-_Forbidden_City.JPG](https://zh.wikipedia.org/wiki/File:Puyi's_schoolbook_-_Forbidden_City.JPG "fig:Puyi's_schoolbook_-_Forbidden_City.JPG")

1919年，英國[蘇格蘭人](../Page/蘇格蘭.md "wikilink")[莊士敦前往北京紫禁城](../Page/莊士敦.md "wikilink")，擔任溥儀[帝師](../Page/帝師.md "wikilink")，教授并指導溥儀學習[英語](../Page/英語.md "wikilink")、[數學](../Page/數學.md "wikilink")、[世界歷史和](../Page/世界歷史.md "wikilink")[地理](../Page/地理.md "wikilink")。溥儀和莊士敦二人關係良好感情深厚，傳為佳話，為人津津樂道。溥儀因此對現代世界眼界大開，开闊了國際視野，增加了西方基本知識。溥儀剪掉自己的[髮辮并穿著](../Page/剃髮令.md "wikilink")[西服](../Page/西服.md "wikilink")，此舉遭到[陳寶琛](../Page/陳寶琛.md "wikilink")，[鄭孝胥等傳統保守派人士的不滿和批評](../Page/鄭孝胥.md "wikilink")。

#### 被驅逐離開紫禁城時期

[Aisin-Gioro_Puyi_01.jpg](https://zh.wikipedia.org/wiki/File:Aisin-Gioro_Puyi_01.jpg "fig:Aisin-Gioro_Puyi_01.jpg")

1924年10月23日，[馮玉祥](../Page/馮玉祥.md "wikilink")、[胡景翼和](../Page/胡景翼.md "wikilink")[孫岳發動](../Page/孫岳.md "wikilink")[北京政變](../Page/北京政變.md "wikilink")。11月5日，馮玉祥突然帶領軍隊包圍[紫禁城](../Page/紫禁城.md "wikilink")。[鹿鍾麟奉](../Page/鹿鍾麟.md "wikilink")[大總統](../Page/中華民國大總統.md "wikilink")[黃郛之命令](../Page/黃郛.md "wikilink")，帶著《[修正清室優待條件](../Page/修正清室優待條件.md "wikilink")》宣言文件，與[李石曾和](../Page/李石曾.md "wikilink")[張璧帶領軍隊佔領紫禁城](../Page/張璧.md "wikilink")，使用武力要求溥儀簽署宣統皇帝退位聲明，取消宣統皇帝尊稱和命令溥儀離開紫禁城，如果溥儀拒絕答應其要求，馮玉祥威脅會使用多門火炮射擊[紫禁城](../Page/紫禁城.md "wikilink")。溥儀為保護紫禁城免遭破壞，別無選擇只能無奈地答應其要求，馮玉祥限溥儀二天時間內收拾個人物品離開紫禁城。

溥儀離開紫禁城之後，先前往父親載灃的宅邸醇王府居住，並由[國民軍進行保護](../Page/西北軍.md "wikilink")（實際上是[監視](../Page/監視.md "wikilink")）。1925年2月24日，溥儀在鄭孝胥，陳寶琛和日本人的協助下，裝扮成商人經[東交民巷日本](../Page/東交民巷.md "wikilink")[大使館至使館前方的火車站乘車逃往](../Page/大使館.md "wikilink")[天津市](../Page/天津市.md "wikilink")，溥儀先後居住於[天津市日租界的](../Page/天津日租界.md "wikilink")[張園和](../Page/天津張園.md "wikilink")[靜園](../Page/靜園.md "wikilink")。\[4\]

1928年6月，[孙殿英以](../Page/孙殿英.md "wikilink")“军事演习”的旗号，对清东陵当中的裕陵和菩陀峪定东陵进行大规模盗掘，并将其中部分盗取的宝物贿赂[宋美龄](../Page/宋美龄.md "wikilink")、[孔祥熙等人](../Page/孔祥熙.md "wikilink")，案件查办最终不了了之。该事件史称“[东陵事件](../Page/东陵事件.md "wikilink")”，国民政府不追究孙殿英的责任，导致溥仪和国民政府完全决裂，这是溥仪和日本人合作的重要原因之一\[5\]。

### 第三次皇帝登基和退位時期

#### 滿洲國大同年代時期

[Puyi-Manchukuo.jpg](https://zh.wikipedia.org/wiki/File:Puyi-Manchukuo.jpg "fig:Puyi-Manchukuo.jpg")
[溥仪和婉容.jpg](https://zh.wikipedia.org/wiki/File:溥仪和婉容.jpg "fig:溥仪和婉容.jpg")合照\]\]
1931年9月18日[滿洲事變發生後](../Page/九一八事變.md "wikilink")，前大清帝國[穆爾哈齊的後裔](../Page/穆爾哈齊.md "wikilink")[熙洽](../Page/熙洽.md "wikilink")，趁東北邊防軍駐吉林副司令官、吉林省政府主席[張作相參加母親葬禮不在](../Page/張作相.md "wikilink")[吉林市城內之機](../Page/吉林市.md "wikilink")，命令士兵開啟城門，向日本關東軍投降。熙洽給溥儀發信邀請其前往祖宗發祥地，復辟大清帝國，「救民於水火」，在日本的支持下，先擁有滿洲，再圖關內。以任職吉林省政府主席的熙洽為首的前大清帝國貴族向日本人提出迎接溥儀前来滿洲，建立君主制國家。日本[關東軍也早已認定溥儀是適合的新國家](../Page/關東軍.md "wikilink")（滿洲國）君主人選。

1931年11月8日，[土肥原賢二製造了](../Page/土肥原賢二.md "wikilink")「[天津事件](../Page/天津事件.md "wikilink")」，將溥儀從其日租界的住所秘密帶出，溥儀經[大沽街](../Page/大沽口.md "wikilink")，[營口市](../Page/營口市.md "wikilink")，[旅順口區](../Page/旅順口區.md "wikilink")，最後再前往[撫順市](../Page/撫順市.md "wikilink")。
1932年2月16日，日本關東軍召集[張景惠](../Page/張景惠.md "wikilink")、[熙洽](../Page/熙洽.md "wikilink")、[馬占山](../Page/馬占山.md "wikilink")、[臧式毅](../Page/臧式毅.md "wikilink")、[謝介石](../Page/謝介石.md "wikilink")、[-{于}-沖漢](../Page/于冲汉.md "wikilink")、[趙欣伯和](../Page/趙欣伯.md "wikilink")[袁金鎧在](../Page/袁金鎧.md "wikilink")[瀋陽市大和旅館召開](../Page/瀋陽市.md "wikilink")「東北政務會議」，會議由日本關東軍司令官[本庄繁主持](../Page/本庄繁.md "wikilink")。東北政務會議決定迎接溥儀成為滿洲國執政，並分配了與會者在滿洲國政權中的職位，其中[板垣徵四郎任職奉天特務機關長](../Page/板垣徵四郎.md "wikilink")，為滿洲國軍政部最高顧問。
18日，發布《滿洲國獨立宣言》：「從即日起宣佈滿蒙地區同中國中央政府脫離關係，根據滿蒙居民的自由選擇與呼籲，滿蒙地區從此實行完全獨立，成立完全獨立自主之政府。
」23日，板垣徵四郎在撫順與溥儀會面，告知溥儀出任滿洲國執政。原本以為能夠重新成為皇帝的溥儀儘管對於日本人所安排的「執政」職位甚為失望，但最後還是欣然接受。

1932年3月1日，日本在[滿洲地區正式成立滿洲國](../Page/滿洲地區.md "wikilink")。3月9日，溥儀在長春市[吉長道尹公署道台衙門大堂舉行就職典禮儀式](../Page/長春市城市建設展覽館.md "wikilink")，宣布就任滿洲國執政。

#### 滿洲國康德年代時期

[伪满皇帝溥仪颁发的即位诏书。伪满皇宫博物院.jpg](https://zh.wikipedia.org/wiki/File:伪满皇帝溥仪颁发的即位诏书。伪满皇宫博物院.jpg "fig:伪满皇帝溥仪颁发的即位诏书。伪满皇宫博物院.jpg")展示的溥儀登基滿洲國康德皇帝诏书\]\]
1934年3月1日，溥儀正式[登基成為皇帝](../Page/登基.md "wikilink")，年號[康德](../Page/康德_\(满洲国\).md "wikilink")，又被稱為**康德皇帝**。日本昭和天皇為表慎重其事，在溥儀登基典禮的時候，贈送一輛[凱迪拉克豪華都鐸](../Page/凱迪拉克.md "wikilink")8C型[轎車](../Page/轎車.md "wikilink")（Cadillac
Deluxe Tudor Limousine
8C）。車首前方，車體後方和車輪中央都鑲有滿洲國國徽，以表示對溥儀登基成為滿洲國康德皇帝的祝賀。溥儀雖然名義上貴為滿洲國康德皇帝，但實際上所有重大權力和決定都要得到日本關東軍的批准才可以實行。而滿洲國康德皇帝只是个象徵性的頭銜，实为[傀儡](../Page/傀儡.md "wikilink")。

[Stamp_Manchukuo_1935_15f.jpg](https://zh.wikipedia.org/wiki/File:Stamp_Manchukuo_1935_15f.jpg "fig:Stamp_Manchukuo_1935_15f.jpg")
1935年4月6日，溥儀首次訪問日本[東京都](../Page/東京都.md "wikilink")。1940年6月26日，溥儀第二次訪問日本東京都，日本昭和天皇裕仁親自迎接。

據美國《歷史》雜誌報導，1940年，溥儀秘密聯繫[薩爾瓦多外交代表團人員](../Page/薩爾瓦多.md "wikilink")，希望能逃亡薩爾瓦多，擺脫日本人控制。薩爾瓦多外交代表團人員返國後，將溥儀的意願報告給薩爾瓦多總統[馬丁內斯](../Page/馬克西米利亞諾·埃爾南德斯·馬丁內斯.md "wikilink")。正好馬丁內斯是一個[神秘主義者](../Page/神秘主義.md "wikilink")，他認為溥儀前往薩爾瓦多是上天的安排，便不顧與日本關係惡化的危險，亳不猶豫地答應了溥儀的請求。馬丁內斯認為和溥儀都是[螞蟻轉世](../Page/螞蟻.md "wikilink")，他曾對薩爾瓦多外交代表團人員說：“殺死一隻螞蟻，比殺死一個人罪行嚴重得多！”

1941年10月，又有薩爾瓦多外交代表團人員到達[新京特別市](../Page/新京.md "wikilink")（今[吉林省](../Page/吉林省.md "wikilink")[長春市](../Page/長春市.md "wikilink")），溥儀把逃亡薩爾瓦多的計劃告訴了一名滿洲國[禁衛隊軍官](../Page/禁衛隊.md "wikilink")，打算讓滿洲國禁衛隊護送自己前往薩爾瓦多大使館，然後再裝扮成大使館職員逃離滿洲國。沒想到的是，滿洲國禁衛隊早被日本關東軍收買，那名禁衛隊軍官向日本關東軍告密，溥儀逃亡計劃完全失敗。[日本陸軍參謀本部立即派出](../Page/參謀本部_\(大日本帝國\).md "wikilink")[憲兵隊](../Page/日本憲兵.md "wikilink")，將薩爾瓦多外交代表團人員驅逐，關閉薩爾瓦多駐滿洲國大使館和數間薩爾瓦多駐滿洲國[貿易公司以作懲罰](../Page/貿易.md "wikilink")，從此薩爾瓦多中斷與日本的外交結盟關係。日本關東軍人員前往[滿洲國宮內府向溥儀提出威胁性交涉和斥责](../Page/滿洲國宮內府.md "wikilink")。

1945年8月9日，蘇聯開始[八月風暴行動](../Page/八月風暴行動.md "wikilink")。蘇聯軍隊迅速打敗了駐守在[中國東北的日本關東軍](../Page/中國東北.md "wikilink")。
11日晚上，溥儀，[溥傑](../Page/溥傑.md "wikilink")，[嵯峨浩和其他親屬在日本關東軍士兵挟持下在](../Page/嵯峨浩.md "wikilink")[新京東站登上火車展開逃亡行動](../Page/長春東站.md "wikilink")。
13日到達[臨江市](../Page/臨江市.md "wikilink")[大栗子街](../Page/大栗子街道.md "wikilink")，停留數日觀察最新戰爭局勢来決定是否要前往[鴨綠江大橋進入](../Page/中朝友誼橋.md "wikilink")[朝鮮半島境內](../Page/朝鮮半島.md "wikilink")。
15日，日本裕仁天皇宣布[日本投降](../Page/日本投降.md "wikilink")。
17日晚上，溥儀在大栗子溝宣讀[滿洲國皇帝退位詔書和取消滿洲國康德皇帝尊稱](../Page/滿洲國皇帝退位詔書.md "wikilink")，宣告滿洲國正式滅亡。\[6\]之後，溥儀，溥傑，嵯峨浩和其他親屬乘坐火車前往通化市，然后在[瀋陽東塔機場乘坐日本關東軍飛機欲逃亡日本](../Page/瀋陽東塔機場.md "wikilink")。

### 戰犯囚禁時期

[Soviet_Union_Military_Officer_and_Puyi.JPG](https://zh.wikipedia.org/wiki/File:Soviet_Union_Military_Officer_and_Puyi.JPG "fig:Soviet_Union_Military_Officer_and_Puyi.JPG")

1945年8月19日，溥儀、溥傑、嵯峨浩和其他親屬在瀋陽東塔機場乘坐日本關東軍飛機準備逃亡日本的時侯，被[蘇聯紅軍](../Page/蘇聯紅軍.md "wikilink")[空降傘兵逮捕](../Page/傘兵.md "wikilink")，溥儀等人被蘇聯士兵扣留在[通遼市至](../Page/通遼市.md "wikilink")8月20日（有一說法是8月21日），然後被蘇聯空軍飛機運送到俄羅斯[赤塔一號軍用機場](../Page/赤塔.md "wikilink")，囚禁於莫洛可夫卡30號特別監獄直至11月初。之後被囚禁在[伯力](../Page/伯力.md "wikilink")45號特別監獄直至1946年春季。溥儀在伯力45號特別監獄內受到優厚的待遇，令其多次向[蘇聯政府表示願意申請在蘇聯永久居留](../Page/蘇聯政府.md "wikilink")，並申請加入[蘇聯共產黨](../Page/蘇聯共產黨.md "wikilink")，但有推測認為這可能是溥儀害怕日後被追究責任，故而申請在蘇聯永久居留。溥儀在蘇聯囚禁期間，曾經作為證人出席[遠東國際軍事法庭](../Page/遠東國際軍事法庭.md "wikilink")
。溥儀在遠東國際軍事法庭出任證人的時候，聲稱自己在就任滿洲國康德皇帝期間，完全為日本關東軍所控制，自己也是身不由己的，也沒有滿洲國康德皇帝作為最高元首的最大實際決策權力和指揮權力。但是，溥儀被蘇聯轉交給[中華人民共和國後](../Page/中華人民共和國.md "wikilink")，他承認由於懼怕自己日後被中國政府追究責任，在遠東國際軍事法庭出任證人的時候，將[戰爭罪行的責任完全推卸給日本](../Page/戰爭罪.md "wikilink")，在部分涉及溥儀和日本所犯下戰爭罪行的責任方面皆有所保留。\[7\]

1950年8月1日，溥儀與滿洲國其他263名戰犯在[綏芬河由蘇聯移交給中華人民共和國](../Page/綏芬河.md "wikilink")，後被送往[撫順戰犯管理所接受為期](../Page/撫順戰犯管理所.md "wikilink")10年的[勞動改造和](../Page/勞動改造.md "wikilink")[思想教育](../Page/思想政治課.md "wikilink")。溥儀的囚犯編號是981。

### 中華人民共和國公民和任職全國政協時期

1959年12月4日，[中華人民共和國最高人民法院根據](../Page/中華人民共和國最高人民法院.md "wikilink")[國家主席](../Page/中華人民共和國主席.md "wikilink")[劉少奇的](../Page/劉少奇.md "wikilink")[特赦令對溥儀予以釋放](../Page/中华人民共和国特赦.md "wikilink")。特赦令說：「溥儀關押已經滿十年。在關押期間，經過勞動改造和思想教育，已經有確實改惡從善的表現，符合[特赦令第一條的規定](../Page/中华人民共和国特赦.md "wikilink")，予以釋放。」從此，溥儀成為[中華人民共和國公民](../Page/中華人民共和國公民.md "wikilink")。

1960年3月，溥儀被分配到[中国科学院北京植物园任職植物護理員和售票員](../Page/中国科学院北京植物园.md "wikilink")。1964年1月1日，溥儀加入[政協全國委員會](../Page/中國人民政治協商會議全國委員會.md "wikilink")，任職文化歷史資料研究委員會專員。

### 文革時期

1966年[文化大革命爆发](../Page/文化大革命.md "wikilink")，因[周恩來將溥儀列為重要](../Page/周恩來.md "wikilink")[保護對象](../Page/一份應予保護的幹部名單.md "wikilink")，溥儀免受[紅衛兵的](../Page/紅衛兵.md "wikilink")[批鬥攻擊](../Page/批鬥.md "wikilink")。

### 去世

1967年10月17日，溥儀因患[腎癌](../Page/腎癌.md "wikilink")，在[北京大學人民醫院病逝](../Page/北京大學人民醫院.md "wikilink")，終年61歲。

溥儀的遺體依據中華人民共和國的有關法規[火化](../Page/火化.md "wikilink")，溥儀的[骨灰放置在](../Page/骨灰.md "wikilink")[八寶山革命公墓](../Page/八寶山革命公墓.md "wikilink")。1995年，溥儀的遺孀[李淑賢將溥儀的骨灰葬於北京市西南](../Page/李淑賢.md "wikilink")120公里的[河北省](../Page/河北省.md "wikilink")[易縣](../Page/易縣.md "wikilink")[華龍皇家陵園](../Page/華龍皇家陵園.md "wikilink")，[溥儀墓在](../Page/溥儀墓.md "wikilink")[清西陵附近](../Page/清西陵.md "wikilink")。

溥儀的親弟弟[溥傑後來娶了](../Page/溥傑.md "wikilink")[昭和天皇的表妹](../Page/昭和天皇.md "wikilink")[嵯峨浩](../Page/嵯峨浩.md "wikilink")，滿洲國的帝位繼承法規定以溥傑繼承沒有子嗣的溥儀。

溥儀的異母弟弟[溥任](../Page/溥任.md "wikilink")（1918年）取漢名[金友之](../Page/金友之.md "wikilink")，曾居住於中國大陸直到2015年去世。金友之曾于2006年就溥儀的肖像權和隱私權提起訴訟。金友之聲稱，“中國最後的帝王世家展”嚴重侵犯了溥儀的肖像權，同時對死者親屬造成了巨大的精神侵害，侵犯了原告對溥儀肖像的使用權。金友之的上訴最終被駁回。

## 家庭成员

### 妻妾

[Puyi_and_wanrong.jpg](https://zh.wikipedia.org/wiki/File:Puyi_and_wanrong.jpg "fig:Puyi_and_wanrong.jpg")

  - [婉容](../Page/婉容.md "wikilink")（1904年－1946年）：[達斡爾族旗人](../Page/達斡爾族.md "wikilink")。1922年，她17歲跟溥儀[結婚](../Page/結婚.md "wikilink")，為皇后。父親榮源為[內務府大臣](../Page/內務府.md "wikilink")。起初夫妻关系尚好，溥仪在婉容与文繡中明显偏向婉容，生性多疑的溥仪曾表现对婉容的信任，后天津时期文繡出走后，溥仪迁怒婉容，婉容开始遭到溥儀冷落，染上[鴉片煙癮](../Page/鴉片.md "wikilink")，满洲国时期婉容并不愿去东北，却被日本[關東軍强行带去](../Page/關東軍.md "wikilink")，常年不堪忍受日本人暴行而發瘋。日本投降後，婉容被[中共游擊隊俘虜](../Page/中國共產黨.md "wikilink")，最後釋放。[鴉片煙癮發作](../Page/鴉片.md "wikilink")，卒於中國[吉林省](../Page/吉林省.md "wikilink")[延吉](../Page/延吉.md "wikilink")，葬地不明。經其弟[潤麒同意](../Page/潤麒.md "wikilink")，於2006年10月23日[招魂與溥儀合葬於河北](../Page/招魂.md "wikilink")[清西陵外的華龍陵園](../Page/清西陵.md "wikilink")。

<!-- end list -->

  - [文繡](../Page/文繡.md "wikilink")（1909年－1953年）：滿洲[鄂爾德特氏](../Page/鄂爾德特氏.md "wikilink")[旗人](../Page/八旗制度.md "wikilink")。1922年，她跟16歲的溥儀[結婚](../Page/結婚.md "wikilink")。溥儀首選的第一位妃子是文繡，但是父親逝世後[端康太妃為首的](../Page/瑾妃.md "wikilink")[四大太妃們](../Page/四大太妃.md "wikilink")，皆認為文繡家境貧寒、長相不好，讓王公勸溥儀重選。文繡被冊封為[淑妃](../Page/淑妃.md "wikilink")。1931年文繡與溥儀離婚。

<!-- end list -->

  - [譚玉齡](../Page/譚玉齡.md "wikilink")（1920年－1942年）：[北京](../Page/北京.md "wikilink")[滿洲人](../Page/滿族.md "wikilink")，原姓[他他拉氏](../Page/他他拉氏.md "wikilink")。1937年，經[貝勒](../Page/貝勒.md "wikilink")[毓朗之女介紹下與溥儀結婚](../Page/毓朗.md "wikilink")，封為「祥妃」。六年後病故，由溥儀追諡「明賢貴妃」。

<!-- end list -->

  - [李玉琴](../Page/李玉琴.md "wikilink")（1928年－2001年）：[吉林](../Page/吉林.md "wikilink")[長春漢人](../Page/長春.md "wikilink")。1943年，被日本官員挑選入宮，封為福嬪。1957年5月，正式與溥儀[離婚](../Page/離婚.md "wikilink")，后再嫁。[文化大革命期間](../Page/文化大革命.md "wikilink")，她因為曾作過溥儀的[貴人而受到迫害](../Page/貴人.md "wikilink")。2001年，因[肝硬化病故](../Page/肝硬化.md "wikilink")。

<!-- end list -->

  - [李淑賢](../Page/李淑賢.md "wikilink")（1925年－1997年）：[漢族護士](../Page/漢族.md "wikilink")。1962年在[周恩來的安排下与溥仪结婚](../Page/周恩來.md "wikilink")。未育有任何子女。

### 嗣子

  - [毓嵒](../Page/毓嵒.md "wikilink")，惇勤亲王[奕誴的曾孙](../Page/奕誴.md "wikilink")，[载濂之孙](../Page/载濂.md "wikilink")，[溥偁第二子](../Page/溥偁.md "wikilink")，溥仪的堂侄，1950年夏季的某一天，溥仪在[伯力市第四十五收容所内](../Page/伯力市.md "wikilink")，带着毓嵒向列祖列宗行三跪九叩礼，再让毓嵒对自己行三跪九叩礼，立毓嵒为皇子\[8\]。

#### 弟弟

:\*
[溥傑](../Page/溥傑.md "wikilink")（1907年－1994年），乳名「譽格」，字「俊之」，號「秉藩」，英文名「William」。與[唐石霞結婚](../Page/唐石霞.md "wikilink")，字[怡莹](../Page/唐怡莹.md "wikilink")，二人從未誕下任何子女，后离婚。再與[嵯峨浩結婚](../Page/嵯峨浩.md "wikilink")，誕下兩名女兒。

:\* 溥倛（1915年－1918年），不滿三歲夭折。

:\*
[溥任](../Page/溥任.md "wikilink")（1918年－2015年），乳名「聯格」，字「友之」，改名「金友之」；1950年代在北京市任職小学教师。與[金瑜庭結婚](../Page/金瑜庭.md "wikilink")，誕下三子兩女。

#### 妹妹

:\*
[韞瑛](../Page/韞瑛.md "wikilink")（1909年－1925年），乳名「毓格」，字「蕊欣」，號「秉瑛」，英語名稱「Lucy」。與[潤良結婚](../Page/潤良.md "wikilink")，二人從未誕下任何子女。

:\*
[韞龢](../Page/韞龢.md "wikilink")（1911年－2001年），乳名「碩格」，字「蕊菡」，號「秉熹」，英語名稱「Mary」，現名「金欣如」。1950年代在北京市主持一家街道托儿所。與[鄭孝胥孫子](../Page/鄭孝胥.md "wikilink")[鄭廣元結婚](../Page/鄭廣元.md "wikilink")，1950年代在北京市任職邮电部门工程师，誕下一子三女。

:\*
[韞穎](../Page/韞穎.md "wikilink")（1913年－1992年），乳名「佩格」，字「蕊秀」，號「秉顥」，英語名稱「Lily」，改名「金蕊秀」；與[潤麒結婚](../Page/潤麒.md "wikilink")。1954年，[中央文史研究馆馆长](../Page/中央文史研究馆.md "wikilink")[章士钊经](../Page/章士钊.md "wikilink")[载涛介绍见到了韫颖](../Page/载涛.md "wikilink")，并要韫颖写一份自述，由章士钊呈送[毛泽东](../Page/毛泽东.md "wikilink")。毛泽东在韫颖的自述后写下：“走进了人民群众变成了一个有志气的人。”毛泽东批示送周恩来阅，考虑是否酌情处理。不久，韫颖被安排任職北京市[东城区政协委员](../Page/东城区.md "wikilink")。1957年潤麒从[抚顺战犯管理所被免予起诉](../Page/抚顺战犯管理所.md "wikilink")，释放回到北京市后安排到东城区政协学习，誕下兩子一女。

:\*
[韞嫻](../Page/韞嫻.md "wikilink")（1914年－2003年），乳名「來格」，字「蕊珠」，英語名稱「Ellen」，改名「金韞嫻」，1950年代在北京[故宫博物院的档案部门工作](../Page/故宫博物院.md "wikilink")。與[趙琪璠結婚](../Page/趙琪璠.md "wikilink")，誕下一子一女。

:\*
[韞馨](../Page/韞馨.md "wikilink")（1917年－1998年），乳名「悅格」，字「蕊潔」，英語名稱「Rose」，改名「金蕊潔」；與[萬嘉熙結婚](../Page/萬嘉熙.md "wikilink")，誕下三子一女。

:\*
[韞娛](../Page/韞娛.md "wikilink")（1919年－1982年），乳名「星格」，字「蕊樂」，改名「溥韞娛」；嫁[完顏·愛蘭](../Page/完顏·愛蘭.md "wikilink")。夫妇两人任職[画家](../Page/画家.md "wikilink")，誕下一子四女。

:\*
[韞歡](../Page/金志堅_\(皇妹\).md "wikilink")（1921年－2004年），小名「姞格」，字「蕊莟」，號「璧月」，改名「金志堅」；1950年2月12日嫁[喬宏志](../Page/喬宏志.md "wikilink")。夫妇两人在北京市任職小学教育工作者。1955年任職北京市[崇文区政协常委](../Page/崇文区.md "wikilink")。1957年任職精忠街小学教导主任。1959年金志堅出席在[人民大会堂召开的群英会](../Page/人民大会堂.md "wikilink")，誕下兩子一女。

### 家系

## 影響

### 個人自傳書籍

[缩略图](https://zh.wikipedia.org/wiki/File:Imperial_train_\(191840228\).jpg "fig:缩略图")

  - 《[我的前半生](../Page/我的前半生.md "wikilink")》：第一版（1960年）、第二版（1964年）、完整版（2007年）。
  - 《溥儀跟他的五個女人》：1993年7月15日，徐虹，希代出版社，ISBN
    978-957-544-461-7（曾翻拍成電視連續劇，於台灣東森綜合台播出。）
  - 《末代皇帝皇妃秘聞》：1994年1月1日，潘際坰，天地圖書出版社，ISBN 978-962-2571-14-3。
  - 《隨侍溥儀紀實》：1999年1月1日，王慶祥，東方出版社，ISBN 978-7-5060-1179-2。
  - 《夢斷紫禁城：溥儀的後半生》：2002年7月10日，王慶祥，慧明文化出版社，ISBN 978-986-7940-36-0。
  - 《日落紫禁城：我的前半生，溥儀自傳》：2002年9月19日，愛新覺羅·溥傑，慧明文化出版社，ISBN
    978-986-7940-47-6。
  - 《毛澤東、周恩來與溥儀》：2012年5月1日，王慶祥，人民出版社，ISBN 978-701-0105-22-2。
  - 《名家說清史：宣統皇帝》：2016年10月1日，王慶祥，紫禁城出版社，ISBN 978-751-3409-15-5。

### 影视形象

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

## 注释

## 外部連結

  - [先後被三任妻子背叛的末代皇帝](http://www.39.net/eden/ynr/jt/79394.html)
  - [溥儀《我的前半生》全本出版](http://www.china.com.cn/book/txt/2006-12/11/content_7487836.htm)
  - [潘際坰【末代皇帝皇妃秘聞】（全文閱讀）](../Page/iarchive:science229_yahoo_20180701_1026.md "wikilink")

{{-}}         |-     |-   |-   |-    |-    |-    |-

[溥仪](../Category/溥仪.md "wikilink")
[Category:第四屆全國政協委員](../Category/第四屆全國政協委員.md "wikilink")
[Category:清朝皇帝](../Category/清朝皇帝.md "wikilink")
[Category:滿洲國皇室](../Category/滿洲國皇室.md "wikilink")
[Category:第二次世界大戰領袖](../Category/第二次世界大戰領袖.md "wikilink")
[Category:中国末代皇帝](../Category/中国末代皇帝.md "wikilink")
[Category:曾入獄的領袖](../Category/曾入獄的領袖.md "wikilink")
[Category:中华民国大陆时期政治人物](../Category/中华民国大陆时期政治人物.md "wikilink")
[Category:辛亥革命人物](../Category/辛亥革命人物.md "wikilink")
[Category:戰俘](../Category/戰俘.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[Category:满族人](../Category/满族人.md "wikilink")
[Category:愛新覺羅氏](../Category/愛新覺羅氏.md "wikilink")

1.
2.  《[清史稿](../Page/清史稿.md "wikilink")·本纪二十五·宣统皇帝本纪》：三十四年冬十月壬申，德宗疾大渐，太皇太后命教养宫内。癸酉，德宗崩，奉太皇太后懿旨，入承大统，为嗣皇帝，嗣穆宗，兼承大行皇帝之祧，时年三岁。
3.
4.  <http://www.ettoday.com/2003/02/24/742-1416754.htm>
    遊中國／前清官邸靜園　「末代皇帝」溥儀落難安樂\]，《東森新聞網》
5.  [愛新覺羅·溥儀\>我的前半生\>四　東陵事件](http://www.bwsk.net/js/p/puyi/wdqb/028.htm)
6.
7.  [趙艾沙編譯整理](../Page/趙艾沙.md "wikilink")\<龍俘——愛新覺羅·溥儀在蘇聯\>，《[明報月刊](../Page/明報月刊.md "wikilink")》，[香港](../Page/香港.md "wikilink")，1993年7月號，頁12-13。
8.