**吉野郡**（）為[奈良縣轄下的](../Page/奈良縣.md "wikilink")[郡](../Page/郡.md "wikilink")。人口59,020人、面積2257.79
km²。（2003年）

吉野郡郡包含以下三個町及八村。

  - [大-{淀}-町](../Page/大淀町.md "wikilink")（おおよどちょう）
  - [下市町](../Page/下市町.md "wikilink")（しもいちちょう）
  - [吉野町](../Page/吉野町.md "wikilink")（よしのちょう）
  - [上北山村](../Page/上北山村.md "wikilink")（かみきたやまむら）
  - [川上村](../Page/川上村_\(奈良縣\).md "wikilink")（かわかみむら）
  - [黑瀧村](../Page/黑瀧村.md "wikilink")（くろたきむら）
  - [下北山村](../Page/下北山村.md "wikilink")（しもきたやまむら）
  - [天川村](../Page/天川村.md "wikilink")（てんかわむら）
  - [十津川村](../Page/十津川村.md "wikilink")（とつかわむら）
  - [野迫川村](../Page/野迫川村.md "wikilink")（のせがわむら）
  - [東吉野村](../Page/東吉野村.md "wikilink")（ひがしよしのむら）

## 沿革

[西吉野村及](../Page/西吉野村.md "wikilink")[大塔村於](../Page/大塔村_\(奈良縣\).md "wikilink")2005年9月25日被併入[五條市](../Page/五條市.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1912年</p></th>
<th><p>1912年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>上市町</p></td>
<td><p>上市町</p></td>
<td><p>上市町</p></td>
<td><p>上市町</p></td>
<td><p>1956年5月3日<br />
吉野町</p></td>
<td><p>吉野町</p></td>
<td><p>吉野町</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>吉野村</p></td>
<td><p>吉野村</p></td>
<td><p>1928年4月24日<br />
町制</p></td>
<td><p>吉野町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>國樔村</p></td>
<td><p>國樔村</p></td>
<td><p>國樔村</p></td>
<td><p>國樔村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1952年6月8日<br />
中莊村</p></td>
<td><p>中莊村</p></td>
<td><p>中莊村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>龍門村</p></td>
<td><p>龍門村</p></td>
<td><p>龍門村</p></td>
<td><p>龍門村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1890年9月23日<br />
中龍門村</p></td>
<td><p>中龍門村</p></td>
<td><p>中龍門村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1890年9月23日<br />
上龍門村</p></td>
<td><p>1942年2月11日<br />
宇陀郡<br />
大宇陀町一部份</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/宇陀市.md" title="wikilink">宇陀市</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大-{淀}-村</p></td>
<td><p>大-{淀}-村</p></td>
<td><p>1922年2月11日<br />
町制</p></td>
<td><p>大-{淀}-町</p></td>
<td><p>大-{淀}-町</p></td>
<td><p>大-{淀}-町</p></td>
<td><p>大-{淀}-町</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>下市村</p></td>
<td><p>1890年4月1日<br />
町制</p></td>
<td><p>下市町</p></td>
<td><p>下市町</p></td>
<td><p>下市町</p></td>
<td><p>下市町</p></td>
<td><p>下市町</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>秋野村</p></td>
<td><p>秋野村</p></td>
<td><p>秋野村</p></td>
<td><p>秋野村</p></td>
<td><p>1956年1月1日<br />
被併入下市町</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>南芳野村</p></td>
<td><p>1912年7月1日<br />
丹生村</p></td>
<td><p>丹生村</p></td>
<td><p>丹生村</p></td>
<td><p>1956年9月30日<br />
被併入下市町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1912年7月1日<br />
黑瀧村</p></td>
<td><p>黑瀧村</p></td>
<td><p>黑瀧村</p></td>
<td><p>黑瀧村</p></td>
<td><p>黑瀧村</p></td>
<td><p>黑瀧村</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>天川村</p></td>
<td><p>天川村</p></td>
<td><p>天川村</p></td>
<td><p>天川村</p></td>
<td><p>天川村</p></td>
<td><p>天川村</p></td>
<td><p>天川村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>野迫川村</p></td>
<td><p>野迫川村</p></td>
<td><p>野迫川村</p></td>
<td><p>野迫川村</p></td>
<td><p>野迫川村</p></td>
<td><p>野迫川村</p></td>
<td><p>野迫川村</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>下北山村</p></td>
<td><p>下北山村</p></td>
<td><p>下北山村</p></td>
<td><p>下北山村</p></td>
<td><p>下北山村</p></td>
<td><p>下北山村</p></td>
<td><p>下北山村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>上北山村</p></td>
<td><p>上北山村</p></td>
<td><p>上北山村</p></td>
<td><p>上北山村</p></td>
<td><p>上北山村</p></td>
<td><p>上北山村</p></td>
<td><p>上北山村</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>川上村</p></td>
<td><p>川上村</p></td>
<td><p>川上村</p></td>
<td><p>川上村</p></td>
<td><p>川上村</p></td>
<td><p>川上村</p></td>
<td><p>川上村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>北十津川村</p></td>
<td><p>1890年6月19日<br />
十津川村</p></td>
<td><p>十津川村</p></td>
<td><p>十津川村</p></td>
<td><p>十津川村</p></td>
<td><p>十津川村</p></td>
<td><p>十津川村</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>十津川花園村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>中十津川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>西十津川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>南十津川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>東十津川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>四鄉村</p></td>
<td><p>四鄉村</p></td>
<td><p>四鄉村</p></td>
<td><p>四鄉村</p></td>
<td><p>1958年3月1日<br />
東吉野村</p></td>
<td><p>東吉野村</p></td>
<td><p>東吉野村</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>高見村</p></td>
<td><p>高見村</p></td>
<td><p>高見村</p></td>
<td><p>高見村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>小川村</p></td>
<td><p>小川村</p></td>
<td><p>小川村</p></td>
<td><p>小川村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>大塔村</p></td>
<td><p>大塔村</p></td>
<td><p>大塔村</p></td>
<td><p>大塔村</p></td>
<td><p>大塔村</p></td>
<td><p>2005年9月25日<br />
被併入五條市</p></td>
<td><p><a href="../Page/五條市.md" title="wikilink">五條市</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>白銀村</p></td>
<td><p>白銀村</p></td>
<td><p>白銀村</p></td>
<td><p>白銀村</p></td>
<td><p>1959年4月1日<br />
西吉野村</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>賀名生村</p></td>
<td><p>賀名生村</p></td>
<td><p>賀名生村</p></td>
<td><p>賀名生村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>宗檜村</p></td>
<td><p>宗檜村</p></td>
<td><p>宗檜村</p></td>
<td><p>宗檜村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>