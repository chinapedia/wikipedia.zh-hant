**M98**（也稱為**NGC
4192**）是一個位於[后髮座的](../Page/后髮座.md "wikilink")[螺旋星系](../Page/螺旋星系.md "wikilink")，也是[室女座星系團](../Page/室女座星系團.md "wikilink")（距離[銀河系最近的一個星系團](../Page/銀河系.md "wikilink")）的成員之一，距離銀河系6000萬[光年](../Page/光年.md "wikilink")，由法國天文學家[皮埃爾·梅香](../Page/皮埃爾·梅香.md "wikilink")（Pierre
Méchain）於1781年所發現。

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [Spiral Galaxy M98 @ SEDS Messier
    pages](https://web.archive.org/web/20071211221734/http://www.seds.org/messier/m/m098.html)
  - [**WIKISKY.ORG**: SDSS image,
    M98](http://www.wikisky.org/?object=M98&img_source=SDSS&zoom=9)

[Category:螺旋星系](../Category/螺旋星系.md "wikilink")
[Category:后髮座](../Category/后髮座.md "wikilink")
[Category:后髮座NGC天體](../Category/后髮座NGC天體.md "wikilink")
[Category:室女座星系團](../Category/室女座星系團.md "wikilink")
[098](../Category/梅西耶天體.md "wikilink")