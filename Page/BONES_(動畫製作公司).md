**BONES**（****）是日本的一家動畫工作室，由于旗下一些较高水準的动画作品，而受到业内外瞩目。被許多動畫迷視為業界良心。其代表作有《[鋼之鍊金術師](../Page/鋼之鍊金術師_\(電視動畫\).md "wikilink")》、《[交响诗篇](../Page/交响诗篇.md "wikilink")》、《[狼雨](../Page/狼雨.md "wikilink")》、《[櫻蘭高校男公關部](../Page/櫻蘭高校男公關部.md "wikilink")》、《[東京震級8.0](../Page/東京震級8.0.md "wikilink")》、《[血界戰線](../Page/血界戰線.md "wikilink")》、《[文豪Stray
Dogs](../Page/文豪Stray_Dogs.md "wikilink")》等等。

## 历史

BONES工作室是在1998年10月由前[日昇動畫](../Page/日昇動畫.md "wikilink")（）公司的成員南雅彥及逢坂浩司、川元利浩一起創立的。而工作室的早期作品之一，就是和日昇動畫公司合作的《星際牛仔：天國之扉》。

“BONES”這個詞，在英語中是“骨頭”（複數）的意思，是根據公司社長南雅彥“想要做充滿深刻內涵的動畫片”（）的想法和理念而作為工作室命名。

## 作品一览

### [電視動畫](../Page/電視動畫.md "wikilink")

  - [機巧奇傳希窩烏戰記](../Page/機巧奇傳.md "wikilink")（2000年10月24日－2001年5月1日）
  - [天使領域](../Page/天使領域.md "wikilink")（2001年4月1日－9月30日）
  - [翼神世音](../Page/翼神世音.md "wikilink")（2002年1月21日－9月10日）
  - [狼雨](../Page/狼雨.md "wikilink")（2003年1月6日－7月29日）
  - [廢棄公主](../Page/廢棄公主.md "wikilink")（2003年4月8日－10月7日）
  - [鋼之鍊金術師](../Page/鋼之鍊金術師_\(電視動畫\).md "wikilink")（2003年10月4日－2004年10月2日）
  - [絢爛舞踏祭](../Page/絢爛舞踏祭.md "wikilink")（2004年4月1日－9月23日）
  - [KURAU Phantom
    Memory](../Page/KURAU_Phantom_Memory.md "wikilink")（2004年6月24日－12月15日）
  - [交響詩篇](../Page/交響詩篇.md "wikilink")（2005年4月17日－2006年4月2日）
  - [櫻蘭高校男公關部](../Page/櫻蘭高校男公關部.md "wikilink")（2006年4月4日－9月27日）
  - [獸王星](../Page/獸王星.md "wikilink")（2006年4月13日－6月22日）
  - [天保異聞 妖奇士](../Page/天保異聞_妖奇士.md "wikilink")（2006年10月7日－2007年3月31日）
  - [DARKER THAN BLACK
    -黑之契約者-](../Page/DARKER_THAN_BLACK.md "wikilink")（2007年4月5日－9月28日）
  - [鬼面騎士](../Page/鬼面騎士.md "wikilink")（2007年4月28日－7月21日）
  - [SOUL EATER](../Page/SOUL_EATER.md "wikilink")（2008年4月7日－2009年3月30日）
  - [二十面相少女](../Page/二十面相少女.md "wikilink")（2008年4月12日－9月17日）
  - [鋼之煉金術師 FULLMETAL
    ALCHEMIST](../Page/鋼之鍊金術師_BROTHERHOOD.md "wikilink")（2009年4月5日－2010年7月4日）
  - [東京震級8.0](../Page/東京震級8.0.md "wikilink")（2009年7月9日－2009年9月17日）
  - [DARKER THAN BLACK
    -流星之雙子-](../Page/DARKER_THAN_BLACK.md "wikilink")（2009年10月9日－2010年1月1日）
  - [HEROMAN](../Page/HEROMAN.md "wikilink")（2010年4月1日－2010年9月23日）
  - [STAR DRIVER
    閃亮的塔科特](../Page/STAR_DRIVER_閃亮的塔科特.md "wikilink")（2010年10月3日－2011年4月3日）
  - [GOSICK](../Page/GOSICK.md "wikilink")（2011年1月7日－7月1日）
  - [NO.6](../Page/NO.6.md "wikilink")（2011年7月7日－9月15日）
  - [UN-GO](../Page/UN-GO.md "wikilink")（2011年10月13日－12月22日）
  - [交響詩篇AO](../Page/交響詩篇AO.md "wikilink")（2012年4月12日－2012年11月19日）
  - [絕園的暴風雨](../Page/絕園的暴風雨.md "wikilink")（2012年10月4日－2013年3月29日）
  - [宇宙浪子](../Page/宇宙浪子.md "wikilink")（又名：太空丹迪）（2014年1月5日－3月30日）
  - \-{zh-hans:[野良神](../Page/野良神.md "wikilink");zh-hant:[流浪神差](../Page/流浪神差.md "wikilink");zh-cn:[野良神](../Page/野良神.md "wikilink");zh-tw:[流浪神差](../Page/流浪神差.md "wikilink");zh-hk:[流浪神差](../Page/流浪神差.md "wikilink");}-（2014年1月5日－3月23日）
  - [地球隊長](../Page/地球隊長.md "wikilink")（2014年4月5日－9月20日）
  - [Soul Eater
    Not\!](../Page/Soul_Eater_Not!.md "wikilink")（2014年4月8日－7月1日）
  - [棺姬嘉依卡](../Page/棺姬嘉依卡.md "wikilink")（2014年4月9日－6月25日）
  - [棺姬嘉依卡 AVENGING
    BATTLE](../Page/棺姬嘉依卡.md "wikilink")（2014年10月8日－12月10日）
  - [SHOW BY
    ROCK\!\!](../Page/SHOW_BY_ROCK!!.md "wikilink")（2015年4月5日－2015年6月21日）
  - [血界戰線](../Page/血界戰線.md "wikilink")（2015年4月5日－2015年10月4日）
  - [赤髮白雪姬](../Page/赤髮白雪姬.md "wikilink")（2015年7月6日－2015年9月11日）
  - \-{zh-hans:[野良神](../Page/野良神.md "wikilink");zh-hant:[流浪神差](../Page/流浪神差.md "wikilink");zh-cn:[野良神](../Page/野良神.md "wikilink");zh-tw:[流浪神差](../Page/流浪神差.md "wikilink");zh-hk:[流浪神差](../Page/流浪神差.md "wikilink");}-
    ARAGOTO（2015年10月2日－2015年12月25日）
  - [超人幻想](../Page/Concrete_Revolutio～超人幻想～.md "wikilink")（2015年10月4日－2015年12月27日）
  - [赤髮白雪姬](../Page/赤髮白雪姬.md "wikilink") 第二期（2016年1月11日－2016年3月28日）
  - [超人幻想](../Page/Concrete_Revolutio～超人幻想～.md "wikilink")
    第二期（2016年4月10日－2016年6月19日）
  - [我的英雄學院](../Page/我的英雄學院.md "wikilink") 第一季 （2016年4月3日－2016年6月26日）
  - [文豪Stray Dogs](../Page/文豪Stray_Dogs.md "wikilink")
    第一季（2016年4月6日－2016年6月22日）
  - [SHOW BY ROCK\!\!
    Short\!\!](../Page/SHOW_BY_ROCK!!.md "wikilink")（2016年7月4日－2016年9月19日）
  - [路人超能100](../Page/路人超能100.md "wikilink")（2016年7月11日－9月26日）
  - [SHOW BY ROCK\!\!
    第二季](../Page/SHOW_BY_ROCK!!.md "wikilink")（2016年2016年10月2日－2016年12月18日）
  - [文豪Stray Dogs](../Page/文豪Stray_Dogs.md "wikilink")
    第二季（2016年10月6日－2016年12月16日）
  - [我的英雄學院](../Page/我的英雄學院.md "wikilink") 第二季 （2017年4月1日 - 2017年9月30日）
  - [血界戰線](../Page/血界戰線.md "wikilink") 第二季（2017年10月8日－2017年12月24日）
  - [我的英雄學院](../Page/我的英雄學院.md "wikilink") 第三季 （2018年4月7日－9月29日）
  - [飛龍女孩](../Page/飛龍女孩.md "wikilink")（2018年4月12日－2018年6月28日）
  - [路人超能100](../Page/路人超能100.md "wikilink") 第二季（2019年1月7日－）
  - [卡萝与星期二](../Page/Carole_&_Tuesday.md "wikilink")（2019年4月10日－）
  - [文豪Stray Dogs](../Page/文豪Stray_Dogs.md "wikilink") 第三季（2019年4月12日－）

<table>
<thead>
<tr class="header">
<th><p>中文名稱</p></th>
<th><p>日文名稱</p></th>
<th><p>播出時間</p></th>
<th><p>導演</p></th>
<th><p>原作類別</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2019年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Carole_&amp;_Tuesday.md" title="wikilink">卡蘿與星期二</a></p></td>
<td></td>
<td><p>4月－預定</p></td>
<td><p>渡边信一郎、堀元宣</p></td>
<td><p>原創</p></td>
<td><p>20周年纪念动画</p></td>
</tr>
<tr class="odd">
<td><p>我的英雄學院</p></td>
<td></td>
<td><p>10月－預定</p></td>
<td><p>長崎健司</p></td>
<td><p>漫畫</p></td>
<td><p>第4期</p></td>
</tr>
</tbody>
</table>

### [网络动画](../Page/网络动画.md "wikilink")

  - [亡念之扎姆德](../Page/亡念之扎姆德.md "wikilink")（通过[PlayStation
    3的网络发布](../Page/PlayStation_3.md "wikilink")，2008年9月24日－2009年2月）
  - [A.I.C.O.
    -Incarnation-](../Page/A.I.C.O._-Incarnation-.md "wikilink")（[Netflix平台整季發布](../Page/Netflix.md "wikilink")，2018年3月9日）

### [劇場版](../Page/劇場版.md "wikilink")

  - [星際牛仔：天國之門](../Page/星際牛仔：天國之門.md "wikilink")（）（2001年9月1日公映）
  - [翼神世音 多元變奏曲](../Page/翼神世音.md "wikilink") （2003年4月19日公映）
  - [鋼之鍊金術師 - 香巴拉的征服者](../Page/鋼之鍊金術師#電影.md "wikilink")（2005年7月23日公映）
  - [異邦人 無皇刃譚](../Page/異邦人_無皇刃譚.md "wikilink")（2007年9月29日公映）
  - [交響詩篇艾蕾卡7 口袋裡的彩虹](../Page/交響詩篇.md "wikilink")（）（2009年4月25日公映）
  - [永遠之久遠](../Page/永遠之久遠.md "wikilink")（）（第1話於2011年6月18日
    第2話於2011年7月16日公映）（第3話於2011年8月13日
    第4話於2011年9月10日公映）（第5話於2011年11月5日
    第6話於2011年11月26日公映）（全六話）
  - [鋼之鍊金術師
    嘆息之丘的神聖之星](../Page/鋼之鍊金術師_嘆息之丘的聖星.md "wikilink")（2011年7月2日公映）
  - [STAR DRIVER 閃亮的塔科特
    劇場版](../Page/STAR_DRIVER_閃亮的塔科特.md "wikilink")（）（2013年2月9日公映）
  - [我的英雄學院THE MOVIE ～2人的英雄～](../Page/我的英雄學院.md "wikilink")（僕のヒーローアカデミア
    THE MOVIE 〜2人の英雄〜）（2018年8月3日公映）
  - [文豪ストレイドッグス DEAD APPLE](../Page/文豪ストレイドッグス.md "wikilink")

### 游戏

  - [翼神世音
    蒼穹幻想曲](../Page/翼神世音.md "wikilink")（[BANDAI](../Page/BANDAI.md "wikilink")，2003年8月7日）
  - [交响诗篇 TR1:NEW WAVE](../Page/交响诗篇.md "wikilink")（BANDAI，2005年10月27日）
  - 交响诗篇 NEW VISION（BANDAI，2006年5月11日）
  - [钢之炼金术师](../Page/钢之炼金术师.md "wikilink")
    （PS2，[史克威尔艾尼克斯](../Page/史克威尔艾尼克斯.md "wikilink")）
  - [钢之炼金术师](../Page/钢之炼金术师.md "wikilink")2 （PS2，史克威尔艾尼克斯）
  - [钢之炼金术师](../Page/钢之炼金术师.md "wikilink")3 （PS2，史克威尔艾尼克斯）
  - [我的英雄學院 Battle For All](../Page/我的英雄學院.md "wikilink") 僕のヒーローアカデミア
    バトル・フォー・オール（BANDAI，2016年5月19日）
  - [我的英雄學院 激突！英雄戰鬥](../Page/我的英雄學院.md "wikilink") 僕のヒーローアカデミア
    激突！ヒーローズバトル（街機，2016年4月28日）
  - [我的英雄學院 SMASH TAP](../Page/我的英雄學院.md "wikilink") 僕のヒーローアカデミア SMASH
    TAP（BANDAI，手機遊戲，2017年5月29日）
  - [我的英雄學院 唯我正義](../Page/我的英雄學院.md "wikilink") 僕のヒーローアカデミア One's
    Justice（BANDAI，[PS4](../Page/PS4.md "wikilink")/[Nintendo
    Switch](../Page/Nintendo_Switch.md "wikilink")，2018年8月23日）

## 参见

  - [日本動畫工作室列表](../Page/日本動畫工作室列表.md "wikilink")

## 外部链接

  - [官方网站](https://www.bones.co.jp/)

[Category:1998年成立的公司](../Category/1998年成立的公司.md "wikilink")
[\*](../Category/BONES.md "wikilink")
[Category:日本動畫工作室](../Category/日本動畫工作室.md "wikilink")
[Category:杉並區公司](../Category/杉並區公司.md "wikilink")