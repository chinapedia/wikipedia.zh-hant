**柯克伍德**（）是[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[布魯姆縣的一個鎮](../Page/布魯姆縣.md "wikilink")，地处布鲁姆县中南部、[宾汉姆顿东南](../Page/宾汉姆顿.md "wikilink")。[2010年美国人口普查时](../Page/2010年美国人口普查.md "wikilink")，该镇有5857人。\[1\]其名称来源于负责修建当地铁路的工程师。

## 参考文献

[Category:纽约州城镇](../Category/纽约州城镇.md "wikilink")

1.