**虎艷芬**（），籍貫[中国](../Page/中华人民共和国.md "wikilink")[四川省](../Page/四川省.md "wikilink")，[廣東省](../Page/廣東省.md "wikilink")[廣州市出生](../Page/廣州市.md "wikilink")，是中国著名[演员](../Page/演员.md "wikilink")，也是[广东](../Page/广东.md "wikilink")[珠三角地区熟悉的群众艺术家](../Page/珠三角.md "wikilink")，参与多部[电影](../Page/电影.md "wikilink")、[电视剧表演](../Page/电视剧.md "wikilink")，也曾在[话剧](../Page/话剧.md "wikilink")[舞台剧上演活无数经典角色](../Page/舞台剧.md "wikilink")，堪称广东本土喜剧的大姐大。

人称“小虎”、“虎姐”的[虎艳芬](../Page/虎艳芬.md "wikilink")，在广东戏剧界德高望重，[八十年代加入](../Page/八十年代.md "wikilink")[广州话剧团](../Page/广州话剧团.md "wikilink")，其后转入[广东话剧院](../Page/广东话剧院.md "wikilink")，曾参与300多部[话剧](../Page/话剧.md "wikilink")、[舞台剧的演出](../Page/舞台剧.md "wikilink")，有丰富的舞台表演经验，从演以来，无论小人物还是枭雄、[粤语市井还是流利](../Page/粤语.md "wikilink")[京腔](../Page/京腔.md "wikilink")，[虎艳芬都能演绎得收放自如](../Page/虎艳芬.md "wikilink")。而最为人熟知的是从2000年开始，参与拍摄[广东电视台本土电视剧](../Page/广东电视台.md "wikilink")《[外来媳妇本地郎](../Page/外来媳妇本地郎.md "wikilink")》，饰演“康家二媳妇”，一名本土市井草根的广州主妇，虎艳芬洒脱自然的演绎，与已故演员[郭昶默契十足](../Page/郭昶.md "wikilink")，生动搞怪的演出，风靡整个华南地区，成为[粤语电视剧中经典的喜剧小人物角色](../Page/粤语电视剧.md "wikilink")，该剧受欢迎程度更使其不断添食，如今已经拍摄愈十年，拍摄播放超过2000集，成为[中國大陸最長夀的](../Page/中國大陸.md "wikilink")[电视剧集](../Page/电视剧集.md "wikilink")。

除电视剧演出外，虎艳芬也参与多部[内地和](../Page/内地.md "wikilink")[香港的电影演出](../Page/香港.md "wikilink")，如[香港电影](../Page/香港电影.md "wikilink")《[七十二家租客](../Page/七十二家租客.md "wikilink")》、《[非典人生](../Page/非典人生.md "wikilink")》、电影版本的《[外来媳妇本地郎](../Page/外来媳妇本地郎.md "wikilink")》等。此外，凭借机灵聪敏的口才，反应敏捷的脑筋，[虎艳芬也担任过多个大型](../Page/虎艳芬.md "wikilink")[综艺节目的](../Page/综艺节目.md "wikilink")[主持人和](../Page/主持人.md "wikilink")[嘉宾](../Page/嘉宾.md "wikilink")，亦庄亦谐的主持风格，成为廣東地區最有影响力、能够压住台风的[金牌司儀](../Page/金牌司儀.md "wikilink")。

虎艳芬乐善好施，阔达开朗，喜欢提携后辈，因此朋友遍天下，是[广东文艺圈中人缘极好的](../Page/广东文艺圈.md "wikilink")[艺术家](../Page/艺术家.md "wikilink")。连[香港著名艺人](../Page/香港.md "wikilink")[曾志偉和](../Page/曾志偉.md "wikilink")[肥肥姐](../Page/肥肥.md "wikilink")[沈殿霞在与虎艳芬合作后](../Page/沈殿霞.md "wikilink")，都赞不绝口。

## 生平

虎艷芬出身於演劇世家，曾經擔任[广东话剧院喜剧团团长](../Page/广东话剧院喜剧团.md "wikilink")，屬於[二级演员](../Page/二级演员.md "wikilink")。[广东戏剧家协会会员](../Page/广东戏剧家协会.md "wikilink")，[中国电视家协会会员](../Page/中国电视家协会.md "wikilink")。

配偶是中國演員[蘇志丹](../Page/蘇志丹.md "wikilink")（于《外来媳妇本地郎》饰演康家长子康祈光一角）。二人育有一女。

## 演出作品

  - 《[外来媳妇本地郎](../Page/外来媳妇本地郎.md "wikilink")》 飾 蘇妙嬋
  - 《[我的大嚿父母](../Page/我的大嚿父母.md "wikilink")》 飾 蘇妙嬋
  - 《[冼夫人](../Page/冼夫人.md "wikilink")》 飾 海椰女
  - 《[婚恋奇情](../Page/婚恋奇情.md "wikilink")》
  - 《[都市梦寻](../Page/都市梦寻.md "wikilink")》
  - 《[生仔梦](../Page/生仔梦.md "wikilink")》
  - 《[香港地恩仇记](../Page/香港地恩仇记.md "wikilink")》
  - 《[万花筒](../Page/万花筒.md "wikilink")》
  - 《[首富人家](../Page/首富人家.md "wikilink")》
  - 《[72家租客](../Page/72家租客.md "wikilink")》飾 自由行旅客

## 榮譽

  - 第四届广东艺术节表演奖
  - 建国四十周年优秀节目奖
  - 全国人口文化奖
  - 全国小品大奖赛三等奖
  - 90年度全国[飞天奖最佳女配角提名](../Page/飞天奖.md "wikilink")

## 外部連結

  -
  - [虎艳芬的新浪博客](http://blog.sina.com.cn/u/1186955844)

  - [虎艳芬的百度博客](http://hi.baidu.com/eva0238/blog)

  - [戏中的假完美哪及现实的真亲切](http://www.nanfangdaily.com.cn/southnews/sh/hyyjt/200402040084.asp)

  - [“大哥”“二嫂”的罗曼史](http://special.dayoo.com/2006/node_3100/node_3108/2006/03/08/1141801024100800.shtml)

[H虎](../Category/中国电影女演员.md "wikilink")
[H虎](../Category/中国电视女演员.md "wikilink")
[H虎](../Category/二级演员.md "wikilink")
[H虎](../Category/四川人.md "wikilink")
[H虎](../Category/广州人.md "wikilink")
[H虎](../Category/四川女演员.md "wikilink")
[H虎](../Category/广东女演员.md "wikilink")
[Y艷](../Category/虎姓.md "wikilink")