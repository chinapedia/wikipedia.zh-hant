**友谊路站**位于[上海](../Page/上海.md "wikilink")[寶山区](../Page/寶山区.md "wikilink")[同濟路](../Page/同濟路.md "wikilink")[友谊路](../Page/友谊路_\(上海\).md "wikilink")，为[上海轨道交通3号线的高架](../Page/上海轨道交通3号线.md "wikilink")[侧式车站](../Page/侧式站台.md "wikilink")。

## 公交换乘

159、160、952B、淞罗专线、淞嘉线、淞嘉专线

## 车站出口

  - 1号口：同济路西侧，友谊路北

## 参考资料

  - [友谊路站建设工程竣工规划验收合格证](https://web.archive.org/web/20070927193009/http://www.shghj.gov.cn/Ct_4.aspx?ct_id=00070608N08837)

[Category:上海市宝山区地铁车站](../Category/上海市宝山区地铁车站.md "wikilink")
[Category:2006年启用的铁路车站](../Category/2006年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")