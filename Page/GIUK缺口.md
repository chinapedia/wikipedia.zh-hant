[GIUK_gap.png](https://zh.wikipedia.org/wiki/File:GIUK_gap.png "fig:GIUK_gap.png")
**GIUK缺口**是描述位於[北大西洋海域上一處與](../Page/北大西洋.md "wikilink")[海軍作戰有重大關係的海域](../Page/海軍.md "wikilink")。GIUK的縮寫來自於[格陵蘭](../Page/格陵蘭.md "wikilink")、[冰島和](../Page/冰島.md "wikilink")[英國這三個地方](../Page/英國.md "wikilink")，GIUK缺口就是指位於這三地之間的海域，通常是引用在軍事相關的論述上。

## 簡史

GIUK缺口對[英國皇家海軍尤其重要](../Page/英國皇家海軍.md "wikilink")，因為歐洲北方的國家如果要進入[大西洋海域](../Page/大西洋.md "wikilink")，不是得要通過防守容易的[英倫海峽](../Page/英倫海峽.md "wikilink")，就是要從冰島兩側的其中一條航道溜過去。只有[法國](../Page/法國.md "wikilink")、[西班牙與](../Page/西班牙.md "wikilink")[葡萄牙具有直接且難以被皇家海軍封鎖的航線進入大西洋](../Page/葡萄牙.md "wikilink")。

[二次世界大戰期間](../Page/二次世界大戰.md "wikilink")，[德國海軍企圖利用自本土和](../Page/德國海軍.md "wikilink")[挪威港口出發的](../Page/挪威.md "wikilink")[艦艇](../Page/艦艇.md "wikilink")，圖闢缺口後攻擊對[蘇聯的運補船團](../Page/蘇聯.md "wikilink")，但是往往被英國艦隊在北海或者是缺口附近海域攔阻。[法國淪陷對德國的幫助很大](../Page/法國淪陷.md "wikilink")，因為他們可以將[潛艇部署在法國沿岸](../Page/潛艇.md "wikilink")。從1940年到1942年，在冰島和格陵蘭之間的海域是少數[英國皇家空軍擔任巡邏的](../Page/英國皇家空軍.md "wikilink")[轟炸機無法涵蓋的區域](../Page/轟炸機.md "wikilink")，大量的海上攻擊活動在集中在這些空窗區域中。

**缺口**這個說法最早出現可以追溯到這個時期，當時有一個被稱為**格陵蘭空中缺口**的區域欠缺來自空中的掩護。到了1943年長程飛機，像是[蕭特公司](../Page/蕭特公司.md "wikilink")[桑德蘭與](../Page/桑德蘭.md "wikilink")[B-24轟炸機的出現填補了這個缺口](../Page/B-24轟炸機.md "wikilink")，使得潛艇要在大西洋活動近乎不可能。

到了1950年代，GIUK缺口再度成為海上作戰計畫的關鍵區域，因為這是[蘇聯潛艇從](../Page/蘇聯.md "wikilink")[可拉半島上的基地](../Page/可拉半島.md "wikilink")[莫曼斯克進入北大西洋的唯一路徑](../Page/莫曼斯克.md "wikilink")（以[聖彼得堡為據點的](../Page/聖彼得堡.md "wikilink")[波羅的海艦隊](../Page/波羅的海艦隊.md "wikilink")、以[塞瓦斯托波和](../Page/塞瓦斯托波.md "wikilink")[尼可拉耶夫為據點的](../Page/尼可拉耶夫.md "wikilink")[黑海艦隊一出港就會遭到北約海軍截擊](../Page/黑海艦隊.md "wikilink")）。[美國與](../Page/美國.md "wikilink")[英國將戰後海軍戰略重心放在圍堵這個缺口上](../Page/英國.md "wikilink")，並且在缺口的海底安裝成串被稱為[SOSUS的海底聽音站](../Page/SOSUS.md "wikilink")。這些部署的主要著眼點在於一旦[冷戰轉熱之際](../Page/冷戰.md "wikilink")，若是任由蘇聯潛艇在北大西洋活動，美國增援歐洲的海上船團將會蒙受無法忍受的損失。

英國皇家海軍冷戰時期的主要任務，除了扮演核子嚇阻的角色以外，就是[反潛作戰](../Page/反潛作戰.md "wikilink")（ASW）。[無敵級航空母艦的建造也是基於這個作戰準則](../Page/無敵級航空母艦.md "wikilink")，使用[海王直昇機進行反潛作戰是她的主要任務](../Page/海王直昇機.md "wikilink")。[23級巡航艦原先設計是單純的反潛載具](../Page/23級巡航艦.md "wikilink")，不過在[福克蘭群島戰爭之後該艦的功能基於戰爭經驗而擴充](../Page/福克蘭群島戰爭.md "wikilink")。

## 流行文化

GIUK缺口在許多書中被描述為作戰計畫的重要關鍵點，例如[湯姆·克蘭西的](../Page/湯姆·克蘭西.md "wikilink")《[赤色風暴](../Page/赤色風暴.md "wikilink")》和《[獵殺紅色十月號](../Page/獵殺紅色十月號.md "wikilink")》。

## 相關條目

  - [富爾達缺口](../Page/富爾達缺口.md "wikilink")
  - [不可思議行動](../Page/不可思議行動.md "wikilink")

[Category:军事设施](../Category/军事设施.md "wikilink")
[Category:冷战](../Category/冷战.md "wikilink")