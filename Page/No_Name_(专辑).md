《**No Name**》是[余憲忠](../Page/余憲忠.md "wikilink")（艺名[No
Name](../Page/No_Name.md "wikilink")）於2004年9月3日由[維京唱片發行的專輯](../Page/維京唱片.md "wikilink")，專輯名稱就叫做《**No
Name**》。[余憲忠於該專輯發行前](../Page/余憲忠.md "wikilink")，曾於2004年4月發行過單曲《[起飛與降落](../Page/起飛與降落.md "wikilink")》，而《No
Name》是他第一張音樂專輯。

專輯《No
Name》由[李偲菘與](../Page/李偲菘.md "wikilink")[李偉菘擔任製作人](../Page/李偉菘.md "wikilink")，作詞作曲者包括[姚謙](../Page/姚謙.md "wikilink")、[李格弟](../Page/李格弟.md "wikilink")、[李偲菘](../Page/李偲菘.md "wikilink")、[李偉菘](../Page/李偉菘.md "wikilink")、[姚若龍與](../Page/姚若龍.md "wikilink")[厲曼婷等人](../Page/厲曼婷.md "wikilink")。前一張單曲中的歌曲〈起飛與降落〉亦有收錄在此專輯中。另外，此專輯收錄的歌曲〈愛情帶來的改變〉與〈守護天使的告白〉因為分別是[韓劇](../Page/韓劇.md "wikilink")《[火鳥](../Page/火鳥.md "wikilink")》的片頭與片尾曲，該劇受到歡迎也使得此專輯十分暢銷\[1\]\[2\]。

## 曲目

## 參考文獻

<references />

## 外部連結

  - [No
    Name同名專輯官方網站](https://web.archive.org/web/20080222144326/http://www.emimusic.com.tw/pop/Noname/)

[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:2004年音樂專輯](../Category/2004年音樂專輯.md "wikilink")

1.
2.