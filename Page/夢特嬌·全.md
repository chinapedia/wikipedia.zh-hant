**夢特嬌·全**，原名**全湛玉**，是[香港插畫家](../Page/香港.md "wikilink")、作家、動畫家及癌症康復者，曾為女演員，自行改為Montagut.c。取這個筆名的原因，是因為她「經常做夢，行為獨特，恃寵生嬌」。

小時候，她與同學比試繪畫以贏取[萬變咭](../Page/萬變咭.md "wikilink")，自此愛上[繪畫](../Page/繪畫.md "wikilink")。她喜愛《[三毛流浪記](../Page/三毛流浪記.md "wikilink")》和[宮崎駿的作品](../Page/宮崎駿.md "wikilink")。她的作品曾於[尖沙咀的](../Page/尖沙咀.md "wikilink")[酒吧展覽](../Page/酒吧.md "wikilink")，她曾當過[電影](../Page/電影.md "wikilink")[幕後工作者](../Page/幕後工作者.md "wikilink")，以及在[閣樓](../Page/閣樓.md "wikilink")[咖啡店任職](../Page/咖啡店.md "wikilink")「[侍應生](../Page/侍應生.md "wikilink")」。

[中三時](../Page/中三.md "wikilink")，她初嘗第一口[煙](../Page/煙草.md "wikilink")，成為虔誠[煙民](../Page/煙民.md "wikilink")，2005年1月6日(22歲)時證實患上三期[鼻咽癌](../Page/鼻咽癌.md "wikilink")。在治療期間，她見盡人生苦樂，所以她在康復後以自己的畫集，說出對人間的情感。她希望用[圖畫來安慰所有曾經患病的人](../Page/圖畫.md "wikilink")，尤其是一班也曾受[香煙所害的人](../Page/香煙.md "wikilink")。透過[圖畫](../Page/圖畫.md "wikilink")，令讀者可以切切實實地感受到一隻斷了[翅膀的](../Page/翅膀.md "wikilink")[蝴蝶](../Page/蝴蝶.md "wikilink")，如何再次飛起，繼續她的[人生旅程](../Page/人生旅程.md "wikilink")，而且比以前飛得更高、更遠。她之所以出版[心靈](../Page/心靈.md "wikilink")[增值](../Page/增值.md "wikilink")[書籍](../Page/書籍.md "wikilink")《愈痛愈美麗──死神共處的276天》，是希望和讀者分享她的過去和她的「人生觀」。她
的部分作品連載於《[都市日報](../Page/都市日報.md "wikilink")》及《[am730](../Page/am730.md "wikilink")》，而她的著作《你是若白仔！》曾在《[都市日報](../Page/都市日報.md "wikilink")》上連載。

2006年，她與[叱吒903DJ](../Page/叱吒903.md "wikilink")[阿喱合力於](../Page/阿喱.md "wikilink")[My903](../Page/My903.md "wikilink").Com製作[黑色](../Page/黑色.md "wikilink")[童話](../Page/童話.md "wikilink")《[Wat
da
Hell](../Page/有耳喱民#Wat_da_Hell.md "wikilink")》。2007年7月，參與[商業電台第二台](../Page/商業電台.md "wikilink")[叱吒903的](../Page/叱吒903.md "wikilink")[廣播劇](../Page/廣播劇.md "wikilink")《[Wat
da
Hell](../Page/有耳喱民#Wat_da_Hell.md "wikilink")》第二輯。9月21日至9月28日，參與主持電台節目《[有耳喱民](../Page/有耳喱民.md "wikilink")》([有個喱全](../Page/有耳喱民#有個喱全.md "wikilink"))。2007年11月，參與[叱吒903](../Page/叱吒903.md "wikilink")[聖誕節特備](../Page/聖誕節.md "wikilink")[廣播劇](../Page/廣播劇.md "wikilink")《[Wat
da
Hell](../Page/有耳喱民#Wat_da_Hell.md "wikilink")》第三輯〈[大野叔叔拆你屋](../Page/大野叔叔拆你屋.md "wikilink")〉。

由2008年開始致力個人動畫短片創作，包括《大坑的貓世界》，《麵粉八十》,《Little York》,《William Outcast
Chan》部份作品於香港電台電視節目《8花齊放》內播映。2011年更為此節目擔當主持人。當中作品《Little
York》得到日本「第十五屆TBS DigCon6大賞」香港區選拔賽努力賞。

2015年，為香港防癌會親手繪圖，炮製了片長15分鐘的動畫短片《Before
Dying》。該短片以「死前」為大前提，透視出「在生」的意義，同時呼應了華人永遠墳場管理委員會推廣「社區建設、生死教育」的命題。

《Before
Dying》故事講述主角在醫院彌留期間的所見所聞，以超現實角度描繪病友遭遇，預視「生命盡頭就是新的開始」。她希望以短片主角親睹病友遭遇，再配合平行時空的概念，讓觀眾感受生死邊緣的感覺。2015年《Before
Dying》於香港唯一仍保留傳統戲院運作、本港殯儀業重鎮的紅磡寶石戲院舉行發布會。2015年《Before
Dying》入圍第二十屆IFVA獨立短片及影像媒體比賽 ,奪得動畫組銀獎。

2015年1月，她應邀出席第42屆法國安古蘭漫畫節作藝術及文化交流。

## 書籍

  -
  -
## 主持電台節目

  - [有耳喱民](../Page/有耳喱民.md "wikilink")
  - [大野禪寺](../Page/大野禪寺.md "wikilink")

## 廣播劇

  - 全仔 @ 《[Cafe de Nath](../Page/有耳喱民#Cafe_de_Nath.md "wikilink")》
  - 滴滴仔 @《[大野先生與滴滴仔](../Page/有耳喱民#大野先生與滴滴仔.md "wikilink")》
  - 滴滴仔 @《[Wat da Hell第二輯](../Page/有耳喱民#Wat_da_Hell.md "wikilink")》
  - 滴滴仔 @《[Wat da
    Hell第三輯](../Page/有耳喱民#Wat_da_Hell.md "wikilink")－[大野叔叔拆你屋](../Page/大野叔叔拆你屋.md "wikilink")》

## 外部連結

  - [夢特嬌‧全的Blog](http://montagutchuen.tumblr.com/)
  - [癌症中的感冒圖
    插我唔死](https://web.archive.org/web/20080411155528/http://www1.appledaily.atnext.com/template/apple_sub/art_main.cfm?iss_id=20060926&sec_id=38163&subsec_id=6038907&art_id=6347070)
  - [癌症中的感冒圖
    插我唔死](http://space.uwants.com/html/78/941678_itemid_56838.html)
  - [夢特嬌‧全於傳媒春秋的訪問](http://www.rthk.org.hk/rthk/tv/cmedia_watch/20070407.html)
  - [活着多好
    全湛玉](http://health.atnext.com/index.php?fuseaction=Article.ListArticle&sec_id=6349009&iss_id=20061026&art_id=6507266)

[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港廣播劇演員](../Category/香港廣播劇演員.md "wikilink")