**罗伊·杰·格劳伯**（，\[1\]），美国物理学家，[哈佛大学](../Page/哈佛大学.md "wikilink")[物理学教授和](../Page/物理学.md "wikilink")[亞利桑那大學光學科學兼職教授](../Page/亞利桑那大學.md "wikilink")，出生於[紐約市](../Page/紐約市.md "wikilink")。他因“对[光学](../Page/光学.md "wikilink")[相干性的](../Page/相干性.md "wikilink")[量子理论的贡献](../Page/量子理论.md "wikilink")”而获得一半的2005年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")，另一半由[美国](../Page/美国.md "wikilink")[科罗拉多大学的](../Page/科罗拉多大学.md "wikilink")[约翰·霍尔和](../Page/约翰·霍尔.md "wikilink")[德国](../Page/德国.md "wikilink")[慕尼黑路德维希](../Page/慕尼黑.md "wikilink")-马克西米利安大学[特奥多尔·亨施分享](../Page/特奥多尔·亨施.md "wikilink")。他亦是[搞笑諾貝爾獎頒獎典禮的掃帚保管員](../Page/搞笑諾貝爾獎.md "wikilink")，總是負責清掃臺上的紙飛機。

## 生平

羅伊·格勞伯1941年畢業於布朗克斯科學高中，之後便繼續在哈佛大學读本科，在大學二年級時他被招募到[洛斯阿拉莫斯實驗室之中加入](../Page/洛斯阿拉莫斯實驗室.md "wikilink")[曼哈頓計畫](../Page/曼哈頓計畫.md "wikilink")，當時他18歲，是當時參與的科學家中最年輕的一位，而他的工作是涉及計算臨界質量的[原子彈](../Page/原子彈.md "wikilink")。兩年後他在洛斯阿拉莫斯收到他的學士學位，於是他回到哈佛，在1946年和1949年獲得博士學位。

## 成就

他製作了光電探測的模型，並解釋不同類型光的基本特徵，例如：激光燈（見[相干態](../Page/相干態.md "wikilink")）和光從燈泡（見[黑體](../Page/黑體.md "wikilink")），並在1963年發表這項發現。而他的理論被廣泛的應用於量子光學。罗伊·格劳伯对于[物理学最突出的贡献是提出了](../Page/物理学.md "wikilink")[相干态的概念和其后的数学基础](../Page/相干态.md "wikilink")。

## 榮譽

羅伊·格勞伯的研究獲得許多榮譽，包括1985年機管局在費城富蘭克林學會的[邁克爾遜獎章和美國光學學會的](../Page/邁克爾遜獎章.md "wikilink")[馬克斯玻恩獎](../Page/馬克斯玻恩獎.md "wikilink")，1996年[美國物理學會的](../Page/美國物理學會.md "wikilink")[丹尼·海涅曼數學物理獎以及](../Page/丹尼·海涅曼數學物理獎.md "wikilink")2005年的諾貝爾物理獎。2008年4月22日，格勞伯教授在西班牙的馬德里被授予西班牙國家研究委員會的金牌。

## 私生活

他生前住在美國馬薩諸塞州的阿靈頓。2010年4月15日阿靈頓警方抓到一名男子，他涉嫌闖入羅伊·格勞伯的家，後來被判非法入侵，但最後只是把羅伊·格勞伯諾貝爾獎杯的複製品被偷走。

羅伊·格勞伯有一個兒子、一個女兒和五個孫子。

## 参考资料

## 外部链接

  - [罗伊·格劳伯在哈佛大学的网页](http://www.physics.harvard.edu/people/facpages/glauber.html)
  - [2005年诺贝尔物理学奖
    英文](http://nobelprize.org/physics/laureates/2005/index.html)
  - [Dannie Heineman
    Prize 1996](https://web.archive.org/web/20080517124134/http://www.aps.org/praw/heineman/96winner.cfm)
  - [NYC High
    Schools](http://www.city-journal.org/html/9_2_how_gothams_elite.html)
  - [罗伊·格劳伯在光學領域中的貢獻](http://psroc.phys.ntu.edu.tw/bimonth/download.php?d=1&cpid=148&did=3)

{{-}}

[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:猶太諾貝爾獎獲得者](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:香港浸會大學榮譽博士](../Category/香港浸會大學榮譽博士.md "wikilink")
[Category:丹尼·海涅曼数学物理奖获得者](../Category/丹尼·海涅曼数学物理奖获得者.md "wikilink")

1.  [In Memoriam: Roy J.
    Glauber, 1925-2018](https://www.osa.org/en-us/about_osa/newsroom/obituaries/roy_j_glauber/)