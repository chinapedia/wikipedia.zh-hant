**易安信公司**（；）為[美国一家跨国](../Page/美国.md "wikilink")[資訊科技企业](../Page/資訊科技.md "wikilink")，主要提供[数据存储](../Page/数据存储.md "wikilink")、[資訊安全](../Page/資訊安全.md "wikilink")、[虚拟化](../Page/虚拟化.md "wikilink")、[云计算等用于存储](../Page/云计算.md "wikilink")、管理、保护和分析大量数据的产品和服务。EMC公司创建于1979年，总部设在[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[霍普金顿市](../Page/霍普金顿市.md "wikilink")。

2015年10月12日，易安信和[戴尔双方证实](../Page/戴尔.md "wikilink")，[迈克尔·戴尔](../Page/迈克尔·戴尔.md "wikilink")、MSD合伙公司（MSD
Partners，迈克尔的家族公司）、银湖（Silver
Lake）会斥资670億美金合并EMC\[1\]，这将是科技产业界最大规模的并购\[2\]。

## 历史

[EMC_Corporation_logo.svg](https://zh.wikipedia.org/wiki/File:EMC_Corporation_logo.svg "fig:EMC_Corporation_logo.svg")
[英特尔前高管](../Page/英特尔.md "wikilink")[理查德·伊根](../Page/理查德·伊根.md "wikilink")（Richard
Egan）与大学室友[罗杰·马里诺](../Page/罗杰·马里诺.md "wikilink")（Roger
Marino）在1979年创建公司。公司名称**EMC**代表创始人的首写字母，第三和第四创始人分别为Connelly和Curly，因此有EMC<sup>2</sup>。[首席执行官Joseph](../Page/首席执行官.md "wikilink")
Tucci在EMC World 2012大会上提到，公司的全称是EMC
Corporation。该公司的标志参考[爱因斯坦](../Page/爱因斯坦.md "wikilink")[质能方程](../Page/质能方程.md "wikilink")，亦采用指数2。

易安信在1981年为Prime计算机公司推出第一款64千字節內存板，并持續开发其他類型的計算機內存板。1980年代中期，該公司的业务拓展到內存板之外的計算機數據存儲類型和網絡存儲平台。1990年，易安信開始提供其旗艦產品Symmetrix。而Symmetrix由Moshe
Yanai領導的团队开发。該產品系列也是易安信公司在1990年代快速增长的主要原因。

易安信目前仍是世界上最大的数据存储平台供應商，直接与[IBM](../Page/IBM.md "wikilink")、[NetApp](../Page/NetApp.md "wikilink")、[惠普以及](../Page/惠普.md "wikilink")[日立數據系統等公司競爭](../Page/日立.md "wikilink")。除此之外，諮詢和IT服務也已成為其越來越重要的收入來源。

EMC公司[股票于](../Page/股票.md "wikilink")1986年4月6日以每股16.5[美金公开发行](../Page/美金.md "wikilink")，股票代码为EMC，在[纽约股票交易所交易](../Page/纽约股票交易所.md "wikilink")，是[S\&P
500成份股之一](../Page/標準普爾500指數.md "wikilink")。

2003年，易安信以6.25亿美金收购[VMware](../Page/VMware.md "wikilink")。

## 产品与服务

  - 存储系统
      - Symmetrix VMAX：旗舰存储平台
      - Symmetrix VMAXe：入门级旗舰存储平台
      - XtremIO：高端全闪存储产品
      - Unity：中端存储产品
      - VNX：中端存储产品
      - VNXe：入门级中端存储产品
      - Isilon：横向扩展[網路附加儲存](../Page/網路附加儲存.md "wikilink")（NAS）解决方案
      - Data Domain：备份专用存储产品
  - 虚拟化
      - VMware：虚拟化和云计算基础架构产品
  - [大数据](../Page/大数据.md "wikilink")
      - Pivotal One：大数据分析套件
      - ECS：对象云存储
  - 软件定义存储
      - ScaleIO：横向扩展软件定义存储软件

## 参考文献

## 外部链接

  - [易安信官方网站](http://www.emc.com/)
  - [EMC
    Forum 2015](https://web.archive.org/web/20161011202102/https://livehouse.in/channel/EMC/record/-K3rWArVO40BNde8bFon)直播，在[LIVEhouse.in播出](../Page/LIVEhouse.in.md "wikilink")

[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")

1.
2.