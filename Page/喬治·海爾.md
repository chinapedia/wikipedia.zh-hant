[George_Ellery_Hale_1905.jpg](https://zh.wikipedia.org/wiki/File:George_Ellery_Hale_1905.jpg "fig:George_Ellery_Hale_1905.jpg")
**喬治·埃勒里·海爾**（，），美国[天文學家](../Page/天文學家.md "wikilink")。

## 生平

1868年出生於[美國](../Page/美國.md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")，父親是製造[電梯的實業家](../Page/電梯.md "wikilink")。

他曾於[麻省理工學院](../Page/麻省理工學院.md "wikilink")、[哈佛大學天文台](../Page/哈佛大學.md "wikilink")（1889－90）及[柏林](../Page/柏林.md "wikilink")（1893－94）接受教育。當他仍是麻省理工學院大學生時，便已發明了[太陽單色光照相儀](../Page/太陽單色光照相儀.md "wikilink")（spectroheliograph），並且透過該儀器發現了太陽表面的渦流（solar
vortices）及[太陽黑子的](../Page/太陽黑子.md "wikilink")[磁場](../Page/磁場.md "wikilink")。

1890年，他出任的台長，1893年至1905年出任[芝加哥大學的教授](../Page/芝加哥大學.md "wikilink")，他也是《[天文與天體物理學報](../Page/天文與天體物理學報.md "wikilink")》（Astronomy
and
Astrophysics）編輯（1892－1895）及《[天文物理學報](../Page/天文物理期刊.md "wikilink")》（Astrophysical
Journal，1895年起）的創刊編輯。“天文物理”（Astrophysics）這個詞語，便是由他所創的。

他擅長向富商籌集經費，並曾促成三個大型天文台的成立，並安裝當時最先進的儀器，包括擁有全世界最大口徑[折射望遠鏡的](../Page/折射望遠鏡.md "wikilink")[葉凱士天文台](../Page/葉凱士天文台.md "wikilink")（他於1895年至1905年出任該台的台長）及[威爾遜山天文台](../Page/威爾遜山天文台.md "wikilink")（於1904年至1923年出任該台台長）。在威爾遜山天文台工作期間，他聘用了[沙普利和](../Page/哈罗·沙普利.md "wikilink")[哈勃兩位後來有名的天文學家](../Page/愛德文·哈勃.md "wikilink")。1928年，海爾又獲得[洛克菲勒基金會](../Page/洛克菲勒基金會.md "wikilink")600萬美元捐款，興建[帕洛馬山天文臺](../Page/帕洛馬山天文臺.md "wikilink")，並安裝一台口徑200英吋的[反射望遠鏡](../Page/反射望遠鏡.md "wikilink")，為當時全球最大的望遠鏡，此望遠鏡由[加州理工學院管理](../Page/加州理工學院.md "wikilink")，海爾對該校的發展上亦擔當過重要角色，令它成為頂尖的研究大學。1969年，帕洛马山天文台和威尔逊山天文台合并，命名为[海爾天文台](../Page/海爾天文台.md "wikilink")。

## 榮譽

**獎項**

  - [亨利·德雷伯獎章](../Page/亨利·德雷伯獎章.md "wikilink")，1904年
  - [英國皇家天文學會金質獎章](../Page/英國皇家天文學會金質獎章.md "wikilink")，1904年
  - [布鲁斯奖](../Page/布鲁斯奖.md "wikilink")，1916年
  - [让森奖章](../Page/让森奖章.md "wikilink")，1917年
  - [伽利略奖](../Page/伽利略奖.md "wikilink")，[佛羅倫斯](../Page/佛羅倫斯.md "wikilink")，1920年
  - [Actonian Prize](../Page/Actonian_Prize.md "wikilink")，1921年
  - [科普利獎章](../Page/科普利獎章.md "wikilink")，1932年

（註：海爾曾多次被提名角逐[諾貝爾奬](../Page/諾貝爾奬.md "wikilink")，但從未得獎。）

**以他的名字來命名的物件**

  - [帕洛馬山天文臺的](../Page/帕洛馬山天文臺.md "wikilink")[海爾望遠鏡](../Page/海爾望遠鏡.md "wikilink")
  - 22年的太陽周期[海爾周期](../Page/太陽週期.md "wikilink")（雙黑子週期）
  - [小行星](../Page/小行星.md "wikilink")[(1024)
    海爾](../Page/小行星1024.md "wikilink")
  - [月球及](../Page/月球.md "wikilink")[火星上的環形山](../Page/火星.md "wikilink")

## 外部連結

  - [簡歷](https://web.archive.org/web/20051221230543/http://astro.uchicago.edu/yerkes/virtualmuseum/Hale.html)
  - [Bruce Medal
    page](http://www.phys-astro.sonoma.edu/BruceMedalists/Hale/index.html)
  - [Awarding of the Bruce Medal: PASP **28** (1916)
    12](http://adsabs.harvard.edu//full/seri/PASP./0028//0000012.000.html)
  - [Awarding of the RAS gold medal: MNRAS **64** (1904)
    388](http://adsabs.harvard.edu//full/seri/MNRAS/0064//0000388.000.html)

### 訃聞

  - \[<http://adsabs.harvard.edu//full/seri/ApJ>../0087//0000369.000.html
    ApJ **87** (1938) 369\]
  - [JRASC **32** (1938)
    192](http://adsabs.harvard.edu//full/seri/JRASC/0032//0000192.000.html)
  - [MNRAS **99** (1939)
    322](http://adsabs.harvard.edu//full/seri/MNRAS/0099//0000322.000.html)
  - [PASP **50** (1938)
    156](http://adsabs.harvard.edu//full/seri/PASP./0050//0000156.000.html)

[H](../Category/美国企业家.md "wikilink")
[H](../Category/美国天文学家.md "wikilink")
[H](../Category/英國皇家天文學會金質獎章獲得者.md "wikilink")
[Category:亨利·德雷伯奖章获得者](../Category/亨利·德雷伯奖章获得者.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")
[Category:让森奖章获得者](../Category/让森奖章获得者.md "wikilink")
[Category:拉姆福德奖获得者](../Category/拉姆福德奖获得者.md "wikilink")