**阿尔弗雷多·詹姆斯·帕西諾**（，），暱稱**艾爾·帕西諾**（）是一名美國[電影和舞台劇演員](../Page/電影.md "wikilink")、電影製作人和編劇家，帕西諾擁有超過50年的職業生涯，在此期間他獲得了許多讚揚和榮譽，其中包括一項[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")、兩項[東尼獎](../Page/東尼獎.md "wikilink")、兩項[黃金時段艾美獎](../Page/黃金時段艾美獎.md "wikilink")、一項[英國電影學院獎](../Page/英國電影學院獎.md "wikilink")、四項[金球獎](../Page/金球獎.md "wikilink")、[美國電影學會的](../Page/美國電影學會.md "wikilink")[AFI終身成就獎](../Page/AFI終身成就獎.md "wikilink")、[金球獎西席·地密爾獎](../Page/西席·地密爾獎.md "wikilink")、和[甘迺迪中心榮譽獎](../Page/甘迺迪中心榮譽獎.md "wikilink")。他也是少數贏得被稱為「演藝三重冠」有競爭性的奧斯卡獎、艾美獎和東尼獎的表演者之一。

他是一名[方法演技演員和在紐約市的](../Page/方法演技.md "wikilink")和的前學生，在那裡他受Charlie
Laughton和教導。帕西諾在《》（1969年）中首次演出，飾演一個小角色，並在《》（1971年）中飾演海洛因上癮的主角而獲得稱讚關注。他曾因出演[法蘭斯·哥普拉的](../Page/法蘭斯·哥普拉.md "wikilink")《[教父](../Page/教父_\(電影\).md "wikilink")》（1972年）飾演突破性的角色[麥可·柯里昂而獲得了國際上的讚譽和認可](../Page/麥可·柯里昂.md "wikilink")。他得到他的第一個[奧斯卡獎提名](../Page/奧斯卡獎.md "wikilink")，並在同樣成功的續集《[教父續集](../Page/教父續集.md "wikilink")》（1974年）和《[教父第三集](../Page/教父第三集.md "wikilink")》（1990年）中重演該角色。帕西諾在飾演柯里昂的表演被視為電影史上最偉大的電影演出之一。之後更是佳作不斷，如《[盜火線](../Page/盜火線.md "wikilink")》，《》，《[魔鬼代言人](../Page/魔鬼代言人.md "wikilink")》，以及《》等。

1992年，因在经典電影《[闻香识女人](../Page/闻香识女人.md "wikilink")》中的精湛演技，终于獲得當年[奧斯卡最佳男演員獎](../Page/奧斯卡.md "wikilink")（第八次提名）。劇中，他扮演性格暴躁，偏激的退休軍官。在一次[手榴彈事故中雙目失明](../Page/手榴彈.md "wikilink")。但是，他可以靠靈敏的鼻子辨別周圍女人身上的香味。他在受僱照顧他的高中生查理的陪同下，準備去[紐約享受最後一次奢華的旅程](../Page/紐約.md "wikilink")。

2003年他出演了[HBO改編的](../Page/HBO.md "wikilink")[電視連續短劇](../Page/電視.md "wikilink")《[天使在美國](../Page/天使在美國_\(連續短劇\).md "wikilink")》，扮演[美國歷史上臭名昭著的](../Page/美國歷史.md "wikilink")[麥卡錫主義者](../Page/麥卡錫主義.md "wikilink")[羅依·科恩](../Page/羅依·科恩.md "wikilink")（Roy
Cohn），出色的演技為他帶來了第61屆[金球獎和第](../Page/金球獎.md "wikilink")56屆[艾美獎電視連續短劇最佳男主角的殊榮](../Page/艾美獎.md "wikilink")。2007年，艾爾更奪得[美國電影學會頒發的](../Page/美國電影學會.md "wikilink")[終身成就獎](../Page/AFI終身成就獎.md "wikilink")。

## 生平

帕西諾生於[紐約市](../Page/紐約市.md "wikilink")[曼哈頓一個](../Page/曼哈頓.md "wikilink")[義大利裔家庭](../Page/義大利.md "wikilink")，兩歲時父母離異，母親帶他到[布朗克斯區的外公外婆家居住](../Page/布朗克斯.md "wikilink")。他父親移居到[加州做保險及餐廳的生意](../Page/加州.md "wikilink")。童年時期，重覆模仿同樣的電影情節及演員的對白成了他的嗜好之一。帕西諾對學校不感興趣，但在學校接觸[戲劇之後](../Page/戲劇.md "wikilink")，他的興趣與天份得到充分的發揮。開始了舞台生涯之後，有很長的一段時間，帕西諾在失意與窮困中度過，甚至必須向人借錢坐巴士去試鏡。

## 事業

[Al_Pacino_and_Robert_Duvall_in_the_Godfather.jpg](https://zh.wikipedia.org/wiki/File:Al_Pacino_and_Robert_Duvall_in_the_Godfather.jpg "fig:Al_Pacino_and_Robert_Duvall_in_the_Godfather.jpg")
1966年帕西諾進入位於紐約市頗負盛名的「演員工作室（），在傳奇的表演藝術教師[李·史特拉斯伯格](../Page/李·史特拉斯伯格.md "wikilink")（）席下學習。終於在「*The
Indian Wants the
Bronx*」劇中成功演出，並獲得跨年度（1966－1967）的Obie獎。建立起一些名聲之後，帕西諾又以「*Does
the Tiger Wear the Necktie?*」一劇贏得[東尼獎](../Page/東尼獎.md "wikilink")。

1969年，首次參與電影「*Me, Natalie*」的演出。1971年，在「*The Panic in Needle
Park*」中飾演毒癮者受到注目。

1972年，參與電影《[教父1](../Page/教父_\(電影\).md "wikilink")》（The
Godfather）的演出更是改變他的一生。

## 个人生活

尽管他没有结婚，但是他的感情生活非常豐富，有三个孩子：大女儿Julie Marie生于1989年，是他与表演教练Jan
Tarrant所生；他还有一对龙凤[双胞胎](../Page/双胞胎.md "wikilink")：儿子Anton
James和女儿Olivia Rose生于2001年，为女演员Beverly
D'Angelo所生，两人的关系从1996年维持到2003年\[1\]\[2\]。

他还与[黛安·基顿在拍摄](../Page/黛安·基顿.md "wikilink")《[教父1](../Page/教父1.md "wikilink")》與《[教父2](../Page/教父2.md "wikilink")》时有过一段感情\[3\]\[4\]。

其他曾經發展過的對象，包括Tuesday Weld、Marthe Keller、Kathleen Quinlan和Lyndall
Hobbs\[5\]。

## 影視作品列表

### 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>譯名標題</p></th>
<th><p>原名標題</p></th>
<th><p>角色</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1969</p></td>
<td></td>
<td><p><em>Me, Natalie</em></p></td>
<td><p>Tony</p></td>
<td><p>首次出演的电影</p></td>
</tr>
<tr class="even">
<td><p>1971</p></td>
<td></td>
<td><p><em>The Panic in Needle Park</em></p></td>
<td><p>Bobby</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1972</p></td>
<td><p><a href="../Page/教父_(电影).md" title="wikilink">教父</a></p></td>
<td><p><em>The Godfather</em></p></td>
<td><p><a href="../Page/麥可·柯里昂.md" title="wikilink">麥可·柯里昂</a></p></td>
<td><p>提名：<a href="../Page/奥斯卡最佳男配角.md" title="wikilink">奥斯卡最佳男配角</a><br />
提名：<a href="../Page/英国电影学院奖.md" title="wikilink">英国电影学院奖最佳新人</a>[6]<br />
提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[7]</p></td>
</tr>
<tr class="even">
<td><p>1973</p></td>
<td></td>
<td><p><em>Scarecrow</em></p></td>
<td><p>Francis Lionel 'Lion' Delbuchi</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><em>Serpico</em></p></td>
<td></td>
<td><p><a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[8]<br />
提名：<a href="../Page/奥斯卡最佳男主角.md" title="wikilink">奥斯卡最佳男主角</a><br />
提名：<a href="../Page/英国电影学院奖.md" title="wikilink">英国电影学院奖最佳男主角</a>[9]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1974</p></td>
<td><p><a href="../Page/教父II.md" title="wikilink">教父II</a></p></td>
<td><p><em>The Godfather Part II</em></p></td>
<td><p>麥可·柯里昂</p></td>
<td><p><a href="../Page/英国电影学院奖.md" title="wikilink">英国电影学院奖最佳男主角</a>[10]<br />
提名：<a href="../Page/奥斯卡最佳男主角.md" title="wikilink">奥斯卡最佳男主角</a><br />
提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[11]</p></td>
</tr>
<tr class="odd">
<td><p>1975</p></td>
<td><p><a href="../Page/熱天午後.md" title="wikilink">熱天午後</a></p></td>
<td><p><em>Dog Day Afternoon</em></p></td>
<td></td>
<td><p><a href="../Page/英国电影学院奖.md" title="wikilink">英国电影学院奖最佳男主角</a>[12]<br />
提名：<a href="../Page/奥斯卡最佳男主角.md" title="wikilink">奥斯卡最佳男主角</a><br />
提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[13]</p></td>
</tr>
<tr class="even">
<td><p>1977</p></td>
<td></td>
<td><p><em>Bobby Deerfield</em></p></td>
<td><p>Bobby Deerfield</p></td>
<td><p>提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[14]</p></td>
</tr>
<tr class="odd">
<td><p>1979</p></td>
<td></td>
<td><p><em>...And Justice for All</em></p></td>
<td><p>Arthur Kirkland</p></td>
<td><p>提名：<a href="../Page/奥斯卡最佳男主角.md" title="wikilink">奥斯卡最佳男主角</a><br />
提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[15]</p></td>
</tr>
<tr class="even">
<td><p>1980</p></td>
<td></td>
<td><p><em>Cruising</em></p></td>
<td><p>Steve Burns</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1982</p></td>
<td></td>
<td><p><em>Author! Author!</em></p></td>
<td><p>Ivan Travalian</p></td>
<td><p>提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–音乐或喜剧类最佳男主角[16]</p></td>
</tr>
<tr class="even">
<td><p>1983</p></td>
<td><p><a href="../Page/疤面煞星.md" title="wikilink">疤面煞星</a></p></td>
<td><p><em>Scarface</em></p></td>
<td></td>
<td><p>提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[17]</p></td>
</tr>
<tr class="odd">
<td><p>1985</p></td>
<td></td>
<td><p><em>Revolution</em></p></td>
<td><p>Tom Dobb</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1989</p></td>
<td></td>
<td><p><em>Sea of Love</em></p></td>
<td><p>Frank Keller</p></td>
<td><p>提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[18]</p></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td></td>
<td><p><em>The Local Stigmatic</em></p></td>
<td><p>Graham</p></td>
<td><p>也是監製</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/至尊神探.md" title="wikilink">至尊神探</a></p></td>
<td><p><em>Dick Tracy</em></p></td>
<td></td>
<td><p>提名：<a href="../Page/奥斯卡最佳男配角.md" title="wikilink">奥斯卡最佳男配角</a><br />
提名：<a href="../Page/英国电影学院奖.md" title="wikilink">英国电影学院奖最佳男配角</a>[19]<br />
提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–最佳男配角[20]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/教父III.md" title="wikilink">教父III</a></p></td>
<td><p><em>The Godfather Part III</em></p></td>
<td><p>麥可·柯里昂</p></td>
<td><p>提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[21]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td></td>
<td><p><em>Frankie and Johnny</em></p></td>
<td><p>Johnny</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td></td>
<td><p><em>Glengarry Glen Ross</em></p></td>
<td></td>
<td><p>提名：<a href="../Page/奥斯卡最佳男配角.md" title="wikilink">奥斯卡最佳男配角</a>[22]<br />
提名：<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–最佳男配角[23]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/闻香识女人.md" title="wikilink">闻香识女人</a></p></td>
<td><p><em>Scent of a Woman</em></p></td>
<td><p>Frank Slade</p></td>
<td><p><a href="../Page/奥斯卡最佳男主角.md" title="wikilink">奥斯卡最佳男主角</a>[24]<br />
<a href="../Page/金球奖.md" title="wikilink">金球奖</a>–剧情类最佳男主角[25]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/情枭的黎明.md" title="wikilink">情枭的黎明</a></p></td>
<td><p><em>Carlito's Way</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
<td></td>
<td><p><em>Two Bits</em></p></td>
<td><p>Grandpa</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/盜火線.md" title="wikilink">盜火線</a></p></td>
<td><p><em>Heat</em></p></td>
<td><p>Lieutenant Vincent Hanna</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td></td>
<td><p><em>City Hall</em></p></td>
<td><p>John Pappas</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><a href="../Page/忠奸人_(電影).md" title="wikilink">忠奸人</a></p></td>
<td><p><em>Donnie Brasco</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔鬼代言人.md" title="wikilink">魔鬼代言人</a></p></td>
<td><p><em>The Devil's Advocate</em></p></td>
<td><p>John Milton</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td></td>
<td><p><em>The Insider</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/挑戰星期天.md" title="wikilink">挑戰星期天</a></p></td>
<td><p><em>Any Given Sunday</em></p></td>
<td><p>Tony D'Amato</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td></td>
<td><p><em>Chinese Coffee</em></p></td>
<td><p>Harry Levine</p></td>
<td><p>导演; 1997年拍摄</p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/白夜追兇.md" title="wikilink">白夜追兇</a></p></td>
<td><p><em>Insomnia</em></p></td>
<td><p>Will Dormer</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/虛擬偶像.md" title="wikilink">虛擬偶像</a></p></td>
<td><p><em>Simone</em></p></td>
<td><p>Viktor Taransky</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>People I Know</em></p></td>
<td><p>Eli Wurman</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td></td>
<td><p><em>The Recruit</em></p></td>
<td><p>Walter Burke</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>Gigli</em></p></td>
<td><p>Starkman</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td></td>
<td><p><em>The Merchant of Venice</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td></td>
<td><p><em>Two for the Money</em></p></td>
<td><p>Walter Abrams</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/瞞天過海：十三王牌.md" title="wikilink">瞞天過海：十三王牌</a></p></td>
<td><p><em>Ocean's Thirteen</em></p></td>
<td><p>Willie Bank</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/88分鐘.md" title="wikilink">88分鐘</a></p></td>
<td><p><em>88 Minutes</em></p></td>
<td><p>Dr. Jack Gramm</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><a href="../Page/世紀交鋒.md" title="wikilink">世紀交鋒</a></p></td>
<td><p><em>Righteous Kill</em></p></td>
<td><p>Detective David "Rooster" Fisk</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td></td>
<td><p><em>The Son of No One</em></p></td>
<td><p>Detective Stanford</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><em>Jack &amp; Jill</em></p></td>
<td><p>自己</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td></td>
<td><p><em>Stand Up Guys</em></p></td>
<td><p>Val</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td></td>
<td><p><em>Manglehorn</em></p></td>
<td><p>A.J. Manglehorn</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>The Humbling</em></p></td>
<td><p>Simon Axler</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/巨星的回信.md" title="wikilink">巨星的回信</a></p></td>
<td><p><em>Danny Collins</em></p></td>
<td><p>Danny Collins</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td></td>
<td><p><em>Misconduct</em></p></td>
<td><p>Abrams</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td></td>
<td><p><em>Dabka</em></p></td>
<td><p>Seymour Tolbin</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>Hangman</em></p></td>
<td><p>Detective Archer</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2019</p></td>
<td><p><a href="../Page/愛爾蘭人_(2019年電影).md" title="wikilink">愛爾蘭人</a></p></td>
<td><p><em>The Irishman</em></p></td>
<td><p><a href="../Page/吉米·霍法.md" title="wikilink">吉米·霍法</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/好萊塢殺人事件.md" title="wikilink">好萊塢殺人事件</a></p></td>
<td><p><em>Once Upon a Time in Hollywood</em></p></td>
<td><p>Marvin Shwarz</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 紀錄片

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>譯名標題</p></th>
<th><p>原名標題</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1990</p></td>
<td></td>
<td><p><em>Madonna: Truth or Dare</em></p></td>
<td><p>他自己</p></td>
<td><p>未掛名</p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td></td>
<td><p><em>Looking for Richard</em></p></td>
<td><p>解说员/<a href="../Page/理查三世_(英格蘭).md" title="wikilink">理查三世</a></p></td>
<td><p>也是导演和監製；导演工会奖—杰出纪录片导演[26]</p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td><p>他自己</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td></td>
<td><p><em>America: A Tribute to Heroes</em></p></td>
<td><p>他自己</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td><p>他自己</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td></td>
<td><p><em>Wilde Salomé</em></p></td>
<td><p><a href="../Page/希律·安提帕斯.md" title="wikilink">希律王</a></p></td>
<td><p>也是導演</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td></td>
<td><p><em>Casting By</em></p></td>
<td><p>他自己</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>譯名標題</p></th>
<th><p>原名標題</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1968</p></td>
<td><p>-</p></td>
<td><p><em></em></p></td>
<td><p>John James</p></td>
<td><p>集數："Deadly Circle of Violence"</p></td>
</tr>
<tr class="even">
<td><p>1977</p></td>
<td></td>
<td><p><em>The Godfather: A Novel for Television</em></p></td>
<td><p><a href="../Page/麥可·柯里昂.md" title="wikilink">麥可·柯里昂</a></p></td>
<td><p>4集</p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/天使在美國_(連續短劇).md" title="wikilink">天使在美國</a></p></td>
<td><p><em>Angels in America</em></p></td>
<td><p><a href="../Page/羅依·科恩.md" title="wikilink">羅依·科恩</a></p></td>
<td><p>6集<br />
<a href="../Page/Primetime_Emmy_Award_for_Outstanding_Lead_Actor_–_Miniseries_or_a_Movie.md" title="wikilink">Emmy Award for Best Lead Actor – Miniseries or a Movie</a>[27]<br />
<a href="../Page/金球奖.md" title="wikilink">金球奖迷你剧或电视电影系列最佳男主角</a>[28]<br />
<a href="../Page/Screen_Actors_Guild_Award_for_Outstanding_Performance_by_a_Male_Actor_in_a_Miniseries_or_Television_Movie.md" title="wikilink">Screen Actors Guild Award–Best Actor in A Mini-Series or Television Movie</a>[29]</p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td></td>
<td><p><em>You Don't Know Jack</em></p></td>
<td><p><a href="../Page/傑克·凱沃基安.md" title="wikilink">傑克·凱沃基安醫生</a></p></td>
<td><p>电视电影<br />
<a href="../Page/Primetime_Emmy_Award_for_Outstanding_Lead_Actor_–_Miniseries_or_a_Movie.md" title="wikilink">Emmy Award for Outstanding Lead Actor – Miniseries or a Movie</a><br />
<a href="../Page/Golden_Globe_Award_for_Best_Actor_–_Miniseries_or_Television_Film.md" title="wikilink">Golden Globe Award for Best Actor in a Mini-Series or Motion Picture</a>[30]<br />
<a href="../Page/Screen_Actors_Guild_Award_for_Outstanding_Performance_by_a_Male_Actor_in_a_Miniseries_or_Television_Movie.md" title="wikilink">Screen Actors Guild Award–Best Actor in A Mini-Series or Television Movie</a>[31]</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td></td>
<td><p><em>Phil Spector</em></p></td>
<td><p><a href="../Page/菲爾·斯佩克特.md" title="wikilink">菲爾·斯佩克特</a></p></td>
<td><p>電視電影</p></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td></td>
<td><p><em>Paterno</em></p></td>
<td></td>
<td><p>電視電影</p></td>
</tr>
</tbody>
</table>

### 電子遊戲

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>譯名標題</p></th>
<th><p>原名標題</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2006</p></td>
<td></td>
<td><p><em>Scarface: The World Is Yours</em></p></td>
<td></td>
<td><p>只用肖像</p></td>
</tr>
</tbody>
</table>

## 奖项

### 奥斯卡

三次男配角和五次男主角提名，一次男主角获奖：

  - 1973年：《[教父](../Page/教父.md "wikilink")》-最佳男配角提名
  - 1974年：《[冲突](../Page/冲突.md "wikilink")》-最佳男主角提名
  - 1975年：《[教父II](../Page/教父II.md "wikilink")》-最佳男主角提名
  - 1976年：《[熱天午後](../Page/熱天午後.md "wikilink")》-最佳男主角提名
  - 1980年：《[義勇急先鋒](../Page/義勇急先鋒.md "wikilink")》-最佳男主角提名
  - 1991年：《[至尊神探](../Page/至尊神探.md "wikilink")》-最佳男配角提名
  - 1993年：《[拜金一族](../Page/拜金一族.md "wikilink")》-最佳男配角提名
  - 1993年：**《[闻香识女人](../Page/闻香识女人.md "wikilink")》-最佳男主角奖**

### 金球奖

十五次(剧情类最佳男主角10次，音乐或喜剧类佳男主角1次，剧情类最佳男配角2次，迷你剧或电视电影系列最佳男主角2次)提名，四次获奖：

  - 1973年：《[教父](../Page/教父.md "wikilink")》-剧情类最佳男主角提名
  - 1974年：**《[冲突](../Page/冲突.md "wikilink")》-剧情类最佳男主角奖**
  - 1975年：《[教父II](../Page/教父II.md "wikilink")》-剧情类最佳男主角提名
  - 1976年：《[熱天午後](../Page/熱天午後.md "wikilink")》-剧情类最佳男主角提名
  - 1978年：《[夕陽之戀](../Page/夕陽之戀.md "wikilink")》-剧情类最佳男主角提名
  - 1980年：《[義勇急先鋒](../Page/義勇急先鋒.md "wikilink")》-剧情类最佳男主角提名
  - 1982年：《[歡喜冤家](../Page/歡喜冤家.md "wikilink")》-音乐或喜剧类最佳男主角提名
  - 1984年：《[疤面人](../Page/疤面煞星.md "wikilink")》-剧情类最佳男主角提名
  - 1990年：《[激情劊子手](../Page/激情劊子手.md "wikilink")》-剧情类最佳男主角提名
  - 1991年：《[教父III](../Page/教父III.md "wikilink")》-剧情类最佳男主角提名
  - 1991年：《[至尊神探](../Page/至尊神探.md "wikilink")》-剧情类最佳男配角提名
  - 1993年：《[拜金一族](../Page/拜金一族.md "wikilink")》-剧情类最佳男配角提名
  - 1993年：**《[闻香识女人](../Page/闻香识女人.md "wikilink")》-剧情类最佳男主角奖**
  - 2004年：**《[天使在美國](../Page/天使在美國.md "wikilink")》-迷你剧或电视电影系列最佳男主角奖**
  - 2010年：**《[你不了解杰克](../Page/你不了解杰克.md "wikilink")》-迷你剧或电视电影系列最佳男主角奖**

### BAFTA奖

五次提名两次获奖：

  - 1973年：《[教父](../Page/教父.md "wikilink")》-最佳新人提名
  - 1975年：《[冲突](../Page/冲突.md "wikilink")》-最佳男主角提名
  - 1975年：**《[教父II](../Page/教父II.md "wikilink")》-最佳男主角奖**
  - 1976年：**《[熱天午後](../Page/熱天午後.md "wikilink")》-最佳男主角奖**
  - 1991年：《[至尊神探](../Page/至尊神探.md "wikilink")》-最佳男配角提名

## 荣誉

2001年，获得[金球奖終身成就獎](../Page/金球奖.md "wikilink")—[塞西尔·B·德米尔奖](../Page/塞西尔·B·德米尔奖.md "wikilink")。

2007年，获得[美國電影學會頒發的](../Page/美國電影學會.md "wikilink")[AFI終身成就獎](../Page/AFI終身成就獎.md "wikilink")。

2011年9月4日，获得[第68届威尼斯电影节授予的](../Page/第68届威尼斯电影节.md "wikilink")“电影制作人荣誉奖”\[32\]。

## 註釋

## 参考资料

## 外部链接

  -
  -
  -
  -
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:奧斯卡最佳男主角獎獲獎演員](../Category/奧斯卡最佳男主角獎獲獎演員.md "wikilink")
[Category:英国电影学院奖最佳男主角获得者](../Category/英国电影学院奖最佳男主角获得者.md "wikilink")
[Category:金球獎最佳電影男主角獲得者](../Category/金球獎最佳電影男主角獲得者.md "wikilink")
[Category:金球獎終身成就獎獲得者](../Category/金球獎終身成就獎獲得者.md "wikilink")
[Category:金酸莓獎獲獎演員](../Category/金酸莓獎獲獎演員.md "wikilink")
[Category:艾美獎獲獎者](../Category/艾美獎獲獎者.md "wikilink")
[Category:東尼獎得主](../Category/東尼獎得主.md "wikilink")
[Category:威尼斯影展獲獎者](../Category/威尼斯影展獲獎者.md "wikilink")
[Category:布朗克斯人](../Category/布朗克斯人.md "wikilink")
[Category:西西里裔美國人](../Category/西西里裔美國人.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:英国电影学院奖得主](../Category/英国电影学院奖得主.md "wikilink")
[Category:美國國家藝術獎章獲得者](../Category/美國國家藝術獎章獲得者.md "wikilink")
[Category:英語電影導演](../Category/英語電影導演.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:美國舞台男演員](../Category/美國舞台男演員.md "wikilink")

1.

2.

3.  The Barbara Walters Special, February 29, 2004

4.

5.  Grobel; p. xxxviii

6.

7.

8.
9.

10.

11.
12.  }

13.
14.
15.
16.
17.
18.
19.

20.
21.
22. Grobel; p. xxviii

23.
24.
25.
26.

27.

28.
29.

30.
31.

32.