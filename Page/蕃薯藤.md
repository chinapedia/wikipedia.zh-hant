**蕃薯藤**（**yam**）為[台灣一個](../Page/台灣.md "wikilink")[網際網路](../Page/網際網路.md "wikilink")[入口網站](../Page/入口網站.md "wikilink")。

## 歷史

蕃薯藤之創立者為[陳正然](../Page/陳正然.md "wikilink")。蕃薯藤前身為「台灣社會文化網路」，於1993年10月創立，以為[弱勢團體架設網站為宗旨](../Page/弱勢團體.md "wikilink")。1994年底，陳正然、[蕭景燈與](../Page/蕭景燈.md "wikilink")[吳俊興開始推動蕃薯藤網站](../Page/吳俊興.md "wikilink")。1995年8月，蕃薯藤正式推出搜尋引擎，從此打響名聲。

1994年，在多位不願曝光的[企業](../Page/企業.md "wikilink")[財團](../Page/財閥.md "wikilink")[資金支持下](../Page/資金.md "wikilink")，陳正然分別成立蕃薯藤與開拓文教基金會；前者以聲援弱勢團體為宗旨，後者以收集[台灣史](../Page/台灣史.md "wikilink")[史料為宗旨](../Page/史料.md "wikilink")。1997年，蕃薯藤獲得[民主進步黨](../Page/民主進步黨.md "wikilink")[新潮流系免費提供辦公室](../Page/新潮流系.md "wikilink")，引起外界質疑：[中華民國視訊網路協會理事長](../Page/中華民國視訊網路協會.md "wikilink")[李世勛說](../Page/李世勛.md "wikilink")，蕃薯藤主要成員均是當年[野百合學運分子](../Page/野百合學運.md "wikilink")，與民進黨淵源甚深，再加上目前辦公室由民進黨免費提供，「易遭外界認為富有濃厚的民進黨[政治色彩](../Page/政治色彩.md "wikilink")」；陳正然回應，民進黨「不能、也不會」利用蕃薯藤資源，不做商業化活動是蕃薯藤不變的堅持，「除非商業化能夠顧及原始理念」\[1\]。

2000年9月，[昱泉國際與蕃薯藤數位科技合資成立](../Page/昱泉國際.md "wikilink")「昱藤數位人力資源服務股份有限公司」。

2006年9月1日，蕃薯藤數位科技所經營的yam.com入口網站由「網絡數碼股份有限公司」（Webs-TV
Inc.）所收購，並於2007年3月將公司更名為「[天空傳媒股份有限公司](../Page/天空傳媒.md "wikilink")」（webs-tv
inc.），並以「[yam天空](../Page/yam天空.md "wikilink")」做為全新的品牌識別系統，網址統一使用原蕃薯藤網址「yam.com」。2010年10月，天空傳媒入口網站再度改回蕃薯藤品牌，也以「yam蕃薯藤」做為全新的企業識別系統。

## 公司基本資料

  - 成立時間：1998年11月17日（改制為公司）
  - 創辦人：[陳正然](../Page/陳正然.md "wikilink")、[蕭景燈](../Page/蕭景燈.md "wikilink")、吳俊興（現任教於[國立高雄大學資訊工程學系](../Page/國立高雄大學.md "wikilink")）
  - [批踢踢創辦人](../Page/批踢踢.md "wikilink")[杜奕瑾於](../Page/杜奕瑾.md "wikilink")1998年加入，為其網路社群創辦人。
  - [賣蕃天創辦人](../Page/賣蕃天.md "wikilink")[劉永信於](../Page/劉永信.md "wikilink")1998年加入，為其電子商務創辦人。
  - 資本額：新台幣268,408,000元整。
  - 營業項目：入口網站“yam.com”（2006年9月由網絡數碼收購）、[網路廣告與整合行銷](../Page/網路廣告.md "wikilink")、[電子商務](../Page/電子商務.md "wikilink")、網路應用整合服務（4C-ASP）、[Wireless
    LAN](../Page/Wireless_LAN.md "wikilink")。

## 相關條目

  - [天空傳媒](../Page/天空傳媒.md "wikilink")
  - [小番薯](../Page/小番薯.md "wikilink")

## 參考文獻

## 外部連結

  - [小番薯](http://kids.yam.com/comic/film_list.php?area_id=woolon)

[Category:1998年成立的公司](../Category/1998年成立的公司.md "wikilink")
[Category:台灣網站](../Category/台灣網站.md "wikilink")

1.