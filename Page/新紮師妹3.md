是2006年的香港喜劇電影，為《新紮師妹》系列的最新續集。由[馬偉豪執導](../Page/馬偉豪.md "wikilink")，但主角改由[薛凱琪飾演的范樹娃及](../Page/薛凱琪.md "wikilink")[鈴木仁飾演的伊豆](../Page/鈴木仁.md "wikilink")。

《新紮師妹3》於2006年6月29日於[香港上映](../Page/香港.md "wikilink")。

## 劇情

當方麗娟和區海文已前往巴西看世界盃之際，少女范樹娃投考警隊，與鍾Sir、劍雄及Over等一見如故，如家人一樣相處，被受寵愛，無憂地當警察。國際刑警日本分部派伊豆到香港整頓鍾Sir等人，但大事未成，終日與范樹娃一起，而產生愛慕之情。范樹娃的好友Vito對現實不滿，聯同一班Model成立反社會組織，終闖禍，更威脅到鍾Sir等人的安危。

## 演員

  - [薛凱琪](../Page/薛凱琪.md "wikilink") 飾 范樹娃
  - [鈴木仁](../Page/鈴木仁.md "wikilink") 飾 鈴木伊豆
  - [森　美](../Page/森美.md "wikilink") 飾 Over
  - [許紹雄](../Page/許紹雄.md "wikilink") 飾 鍾Sir/鍾樂海
  - [黃浩然](../Page/黃浩然.md "wikilink") 飾 周劍雄
  - [苑瓊丹](../Page/苑瓊丹.md "wikilink") 飾 呂麗娜
  - [黎耀祥](../Page/黎耀祥.md "wikilink") 飾 古惑哥
  - [唐劍康](../Page/唐劍康.md "wikilink") 飾 廖俊熙 Vito
  - [周　驄](../Page/周驄.md "wikilink") 飾 區耀山
  - [鄒凱光](../Page/鄒凱光.md "wikilink") 飾 海灘叔
  - [火　火](../Page/火火.md "wikilink") 飾 查查
  - [周家聲](../Page/周家聲.md "wikilink") 飾
  - [曹震豪](../Page/曹震豪.md "wikilink") 飾 周公子 Noblebo
  - [冼色麗](../Page/冼色麗.md "wikilink") 飾 阿玲（Model）
  - [周秀娜](../Page/周秀娜.md "wikilink") 飾 阿明（Model）
  - [陳文剛](../Page/陳文剛.md "wikilink") 飾 學堂教官

## 軼事

  - 於上輯《[新紮師妹](../Page/新紮師妹.md "wikilink")》、《[新紮師妹2美麗任務](../Page/新紮師妹2美麗任務.md "wikilink")》中主演的[楊千嬅和](../Page/楊千嬅.md "wikilink")[吳彥祖](../Page/吳彥祖.md "wikilink")，並無在此電影演出。

## 外部連結

  - {{@movies|f1hk59610021}}

  -
  -
  -
  -
  -
[Category:香港浪漫喜劇片](../Category/香港浪漫喜劇片.md "wikilink")
[6](../Category/2000年代香港電影作品.md "wikilink")
[Category:馬偉豪電影](../Category/馬偉豪電影.md "wikilink")