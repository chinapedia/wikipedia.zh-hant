[Rod\&Cone.jpg](https://zh.wikipedia.org/wiki/File:Rod&Cone.jpg "fig:Rod&Cone.jpg"))
and cones ([cones](../Page/:en:cone_cell.md "wikilink"))\[1\]\]\]

**感光细胞**，是在[眼球的](../Page/眼球.md "wikilink")[视网膜中发现的](../Page/视网膜.md "wikilink")，具有[光转化能力的一类特殊](../Page/光转化.md "wikilink")[神经细胞](../Page/神经细胞.md "wikilink")。更具体点说就是，感光细胞从视野范围内吸收[光子](../Page/光子.md "wikilink")，然后经一系列特殊复杂的[生物化学通路](../Page/生物化学.md "wikilink")，将这些信息以[膜电位改变的形式进行信号传导](../Page/膜电位.md "wikilink")。最后，[视觉系统对这些信号信息进行处理](../Page/视觉系统.md "wikilink")，以呈现一个完整的视觉世界。当然，以上所述的是[脊椎动物的感光细胞](../Page/脊椎动物.md "wikilink")。而对于像[昆虫类和](../Page/昆虫.md "wikilink")[软体动物类这样的](../Page/软体动物.md "wikilink")[无脊椎动物而言](../Page/无脊椎动物.md "wikilink")，它们的感光细胞在形态和生化通路上都是不同的。

## 参考资料

## 參考文獻

  -
  -
## 外部連結

  - [NIF Search – Photoreceptor
    Cell](https://www.neuinfo.org/mynif/search.php?q=Photoreceptor%20Cell&t=data&s=cover&b=0&r=20)
    via the [Neuroscience Information
    Framework](../Page/Neuroscience_Information_Framework.md "wikilink")

[Category:眼睛](../Category/眼睛.md "wikilink")
[Category:組織學](../Category/組織學.md "wikilink")

1.  Human Physiology and Mechanisms of Disease by Arthur C. Guyton
    (1992) p.373