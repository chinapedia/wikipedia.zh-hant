這是[英特爾](../Page/英特爾.md "wikilink")[公司推出的](../Page/公司.md "wikilink")[主機板](../Page/主機板.md "wikilink")[晶片組](../Page/晶片組.md "wikilink")[列表](../Page/列表.md "wikilink")，它包含四大類，分別是

  - 早期晶片組
  - 桌面與移動晶片組
  - [伺服器與](../Page/伺服器.md "wikilink")[工作站晶片組](../Page/工作站.md "wikilink")
  - [嵌入式晶片組](../Page/嵌入式.md "wikilink")

## 早期晶片組

[英特爾授權](../Page/英特爾.md "wikilink")[ZyMOS推出的POACH系列晶片組來搭配](../Page/ZyMOS.md "wikilink")[80286與](../Page/80286.md "wikilink")[Intel
80386SX處理器](../Page/Intel_80386.md "wikilink")。

[英特爾早期晶片組資料](../Page/英特爾.md "wikilink"):\[1\]

  - 82350 EISA
  - 82350DT EISA
  - 82310 MCA
  - 82340SX PC AT
  - 82340DX PC AT
  - 82320 MCA
  - 82360SL
    用來搭配移動版[80386SL與](../Page/80386SL.md "wikilink")[80486SL處理器](../Page/80486SL.md "wikilink")。

## 桌面與移動晶片組

### 4xx系列晶片組

#### [80486晶片組](../Page/80486.md "wikilink")

| [晶片組](../Page/晶片組.md "wikilink") | 研發代號      | 南橋      | 發表日期     | 處理器電壓及插座     | [前端匯流排](../Page/前端匯流排.md "wikilink") | [對稱多處理機](../Page/對稱多處理機.md "wikilink") | 記憶體類型                            | 最大記憶體                              | 奇偶校驗/錯誤檢查   | 二級[快取類型](../Page/快取.md "wikilink") | [PCI支援](../Page/PCI.md "wikilink") |
| -------------------------------- | --------- | ------- | -------- | ------------ | ------------------------------------ | -------------------------------------- | -------------------------------- | ---------------------------------- | ----------- | ---------------------------------- | ---------------------------------- |
| 420TX                            | Saturn    | SIO     | 1992年11月 | 5V（486）      | 直至33 MHz                             | rowspan="3"                            | [FPM](../Page/FPM.md "wikilink") | 128 [MB](../Page/MB.md "wikilink") | rowspan="3" | 異步                                 | 2.0                                |
| 420EX                            | Aries     | 82426EX | 1994年3月  | 5V/3.3V（486） | 直至50 MHz                             |                                        |                                  |                                    |             |                                    |                                    |
| 420ZX                            | Saturn II | SIO     | 直至33 MHz | 160 MB       | 2.1                                  |                                        |                                  |                                    |             |                                    |                                    |

#### [Pentium晶片組](../Page/Pentium.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器</p></th>
<th><p>前端匯流排</p></th>
<th><p>對稱多處理機</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>最大記憶體快取範圍</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>二級快取類型</p></th>
<th><p>PCI支援</p></th>
<th><p><a href="../Page/AGP.md" title="wikilink">AGP支援</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>430LX</p></td>
<td><p>Mercury[2]</p></td>
<td><p>S82434LX</p></td>
<td><p>SIO</p></td>
<td><p>1993年3月</p></td>
<td><p>P60/66</p></td>
<td><p>66 MHz</p></td>
<td></td>
<td><p>FPM</p></td>
<td><p>192 MB</p></td>
<td><p>192 MB</p></td>
<td><p>rowspan="2" </p></td>
<td><p>異步</p></td>
<td><p>2.0</p></td>
<td><p>rowspan="7" </p></td>
</tr>
<tr class="even">
<td><p>430NX</p></td>
<td><p>Neptune[3]</p></td>
<td><p>S82433NX S82434NX</p></td>
<td><p>1994年3月</p></td>
<td><p>P75+</p></td>
<td></td>
<td><p>512 MB</p></td>
<td><p>512 MB</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>430FX</p></td>
<td><p>Triton[4][5]</p></td>
<td><p>SB82437FX-66 SB82437JX</p></td>
<td><p>PIIX</p></td>
<td><p>1995年1月</p></td>
<td><p>rowspan="2" </p></td>
<td><p>FPM/EDO</p></td>
<td><p>128 MB</p></td>
<td><p>64 MB</p></td>
<td><p>rowspan="2" </p></td>
<td><p>異步/管線爆發</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>430MX</p></td>
<td><p>Mobile Triton</p></td>
<td><p>82437MX</p></td>
<td><p>MPIIX</p></td>
<td><p>1995年10月</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>430HX</p></td>
<td><p>Triton II[6][7]</p></td>
<td><p>FW82439HX FW82439JHX</p></td>
<td><p>PIIX3</p></td>
<td><p>1996年2月</p></td>
<td></td>
<td><p>512 MB</p></td>
<td><p>512 MB/64 MB</p></td>
<td></td>
<td><p>2.1</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>430VX</p></td>
<td><p>SB82437VX SB82438VX</p></td>
<td><p>rowspan="2" </p></td>
<td><p>FPM/EDO/<a href="../Page/SDR_SDRAM.md" title="wikilink">SDRAM</a></p></td>
<td><p>128 MB</p></td>
<td><p>64 MB</p></td>
<td><p>rowspan="2" </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>430TX[8]</p></td>
<td></td>
<td><p>FW82439TX</p></td>
<td><p>PIIX4</p></td>
<td><p>1997年2月</p></td>
<td><p>256 MB</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### [Pentium Pro](../Page/Pentium_Pro.md "wikilink")/[Pentium II](../Page/Pentium_II.md "wikilink")/[Pentium III晶片組](../Page/Pentium_III.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器</p></th>
<th><p>前端匯流排</p></th>
<th><p>對稱多處理機</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>記憶庫</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>PCI支援</p></th>
<th><p>AGP支援</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>450KX</p></td>
<td><p>Mars</p></td>
<td><p>82451KX, 82452KX, 82453KX, 82454KX</p></td>
<td><p>SIO, SIO.A(ISA) PCEB/ESC(EISA)</p></td>
<td><p>1995年11月</p></td>
<td><p>Pentium Pro</p></td>
<td><p>66 MHz</p></td>
<td></td>
<td><p>FPM</p></td>
<td><p>8 <a href="../Page/gigabyte.md" title="wikilink">GB</a></p></td>
<td></td>
<td><p>rowspan="2" </p></td>
<td><p>2.0</p></td>
<td><p>rowspan="2" </p></td>
</tr>
<tr class="even">
<td><p>450GX</p></td>
<td><p>Orion</p></td>
<td><p>82451GX, 82452GX, 82453GX, 82454GX</p></td>
<td><p>SIO.A(ISA) PCEB/ESC(EISA)</p></td>
<td><p>(最多四个)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/440FX.md" title="wikilink">440FX</a></p></td>
<td><p>Natoma</p></td>
<td><p>82441FX, 82442FX</p></td>
<td><p>PIIX3</p></td>
<td><p>May 1996</p></td>
<td><p>Pentium Pro/Pentium II</p></td>
<td><p>66 MHz</p></td>
<td></td>
<td><p>FPM/EDO/BEDO</p></td>
<td><p>1 GB</p></td>
<td><p>4</p></td>
<td></td>
<td><p>2.1</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>440LX</p></td>
<td><p>Balboa</p></td>
<td><p>82443LX</p></td>
<td><p>PIIX4</p></td>
<td><p>August 1997</p></td>
<td><p>Pentium II/Celeron</p></td>
<td><p>66 MHz</p></td>
<td></td>
<td><p>FPM/EDO/SDRAM</p></td>
<td><p>1 GB EDO/512 MB SDRAM</p></td>
<td><p>4</p></td>
<td></td>
<td><p>2.1</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>440EX</p></td>
<td></td>
<td><p>82443EX</p></td>
<td><p>PIIX4E</p></td>
<td><p>April 1998</p></td>
<td><p>Celeron/Pentium II</p></td>
<td><p>66 MHz</p></td>
<td></td>
<td><p>EDO/SDRAM</p></td>
<td><p>256 MB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>2.1</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Intel_440BX.md" title="wikilink">440BX</a></p></td>
<td><p>Seattle</p></td>
<td><p>82443BX</p></td>
<td><p>PIIX4E</p></td>
<td><p>April 1998</p></td>
<td><p>Pentium II/III, Celeron</p></td>
<td><p>66/100 MHz</p></td>
<td></td>
<td><p>EDO/SDRAM</p></td>
<td><p>1 GB</p></td>
<td><p>4</p></td>
<td></td>
<td><p>2.1 （可選64位）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>440GX</p></td>
<td></td>
<td><p>82443GX</p></td>
<td><p>PIIX4E</p></td>
<td><p>June 1998</p></td>
<td><p>Pentium II/III, <a href="../Page/Xeon.md" title="wikilink">Xeon</a></p></td>
<td><p>100 MHz</p></td>
<td></td>
<td><p>SDRAM</p></td>
<td><p>2 GB</p></td>
<td><p>4</p></td>
<td></td>
<td><p>2.1</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>450NX</p></td>
<td></td>
<td><p>82451NX, 82452NX, 82453NX, 82454NX</p></td>
<td><p>PIIX4E</p></td>
<td><p>June 1998</p></td>
<td><p>Pentium II/III, Xeon</p></td>
<td><p>100 MHz</p></td>
<td><p>(最多四个)</p></td>
<td><p>FPM/EDO</p></td>
<td><p>8 GB</p></td>
<td><p>4</p></td>
<td></td>
<td><p>2.1 （可選64位）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>440ZX/440ZX-66</p></td>
<td></td>
<td><p>82443ZX</p></td>
<td><p>PIIX4E</p></td>
<td><p>November 1998</p></td>
<td><p>Celeron, Pentium II/III</p></td>
<td><p>100/66 MHz</p></td>
<td></td>
<td><p>SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>2.1</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>440MX</p></td>
<td><p>Banister</p></td>
<td><p>82443MX</p></td>
<td><p>Same chip</p></td>
<td></td>
<td><p>Pentium II/mobile Celeron</p></td>
<td><p>66/100 MHz</p></td>
<td></td>
<td><p>SDRAM</p></td>
<td><p>256 MB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>2.2</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 4xx系列晶片組[南橋](../Page/南橋.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>型號</p></th>
<th><p><a href="../Page/ATA.md" title="wikilink">ATA支援</a></p></th>
<th><p><a href="../Page/USB.md" title="wikilink">USB支援</a></p></th>
<th><p><a href="../Page/CMOS.md" title="wikilink">CMOS</a>/時鐘</p></th>
<th><p>ISA支援</p></th>
<th><p><a href="../Page/LPC匯流排.md" title="wikilink">LPC支援</a></p></th>
<th><p>電源管理</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>SIO</p></td>
<td><p>82378IB/ZB</p></td>
<td><p>無</p></td>
<td><p>無</p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/系統管理模式.md" title="wikilink">SMM</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PIIX.md" title="wikilink">PIIX</a></p></td>
<td><p>82371FB</p></td>
<td><p><a href="../Page/PIO.md" title="wikilink">PIO</a>/<a href="../Page/WDMA.md" title="wikilink">WDMA</a></p></td>
<td><p>無</p></td>
<td></td>
<td></td>
<td></td>
<td><p>SMM</p></td>
</tr>
<tr class="odd">
<td><p>MPIIX</p></td>
<td><p>82371MX</p></td>
<td><p>PIO</p></td>
<td><p>無</p></td>
<td></td>
<td></td>
<td></td>
<td><p>SMM</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PIIX.md" title="wikilink">PIIX</a>3</p></td>
<td><p>82371SB</p></td>
<td><p>PIO/WDMA</p></td>
<td><p>1個控制器, 2個接口</p></td>
<td></td>
<td></td>
<td></td>
<td><p>SMM</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/PIIX.md" title="wikilink">PIIX</a>4</p></td>
<td><p>82371AB</p></td>
<td><p>PIO/<a href="../Page/UDMA.md" title="wikilink">UDMA</a> 33</p></td>
<td><p>1個控制器, 2個接口</p></td>
<td></td>
<td></td>
<td></td>
<td><p>SMM</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PIIX.md" title="wikilink">PIIX</a>4E</p></td>
<td><p>82371EB</p></td>
<td><p>PIO/UDMA 33</p></td>
<td><p>1個控制器, 2個接口</p></td>
<td></td>
<td></td>
<td></td>
<td><p>SMM</p></td>
</tr>
</tbody>
</table>

### 8xx系列晶片組

#### Pentium II/Pentium III晶片組

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器</p></th>
<th><p>前端匯流排</p></th>
<th><p>對稱多處理機</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>記憶庫</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>PCI</p></th>
<th><p>AGP速度</p></th>
<th><p>集成顯核</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Intel_810.md" title="wikilink">810</a></p></td>
<td><p>Whitney</p></td>
<td><p>82810</p></td>
<td><p><a href="../Page/I/O路径控制器.md" title="wikilink">ICH</a>/ICH0</p></td>
<td><p>April 1999</p></td>
<td><p>Celeron, Pentium II/III</p></td>
<td><p>66/100 MHz</p></td>
<td></td>
<td><p>EDO/PC100 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Intel_810.md" title="wikilink">810E</a></p></td>
<td><p>Whitney</p></td>
<td><p>82810E</p></td>
<td><p>ICH</p></td>
<td><p>September 1999</p></td>
<td><p>Celeron, Pentium II/III</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC100 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Intel_810.md" title="wikilink">810E2</a></p></td>
<td><p>Whitney</p></td>
<td><p>8210E</p></td>
<td><p>ICH2</p></td>
<td></td>
<td><p>Celeron, Pentium II/III</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC100 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>820</p></td>
<td><p>Camino</p></td>
<td><p>82820</p></td>
<td><p>ICH</p></td>
<td><p>November 1999</p></td>
<td><p>Pentium II/III, Celeron</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC800 <a href="../Page/RDRAM.md" title="wikilink">RDRAM</a>/PC133 SDRAM</p></td>
<td><p>1 GB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>840</p></td>
<td><p>Carmel</p></td>
<td><p>82840</p></td>
<td><p>ICH</p></td>
<td><p>October 1999</p></td>
<td><p>Pentium III, Xeon</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p><a href="../Page/Dual-channel_architecture.md" title="wikilink">Dual-Channel</a> PC800 <a href="../Page/RDRAM.md" title="wikilink">RDRAM</a></p></td>
<td><p>4 GB</p></td>
<td><p>2×4</p></td>
<td></td>
<td><p>v2.2/66 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>820E</p></td>
<td><p>Camino</p></td>
<td><p>82820</p></td>
<td><p>ICH2</p></td>
<td><p>June 2000</p></td>
<td><p>Pentium II/III, Celeron</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC800 <a href="../Page/RDRAM.md" title="wikilink">RDRAM</a>/PC133 SDRAM</p></td>
<td><p>1 GB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>815</p></td>
<td><p>Solano</p></td>
<td><p>82815</p></td>
<td><p>ICH</p></td>
<td><p>June 2000</p></td>
<td><p>Celeron, Pentium II/III</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>815E</p></td>
<td><p>Solano</p></td>
<td><p>82815</p></td>
<td><p>ICH2</p></td>
<td><p>June 2000</p></td>
<td><p>Celeron, Pentium II/III</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>815EP</p></td>
<td><p>Solano</p></td>
<td><p>82815EP</p></td>
<td><p>ICH2</p></td>
<td><p>November 2000</p></td>
<td><p>Celeron, Pentium II/III</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>3</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>815P</p></td>
<td><p>Solano</p></td>
<td><p>82815EP</p></td>
<td><p>ICH/ICH0</p></td>
<td><p>March 2001</p></td>
<td><p>Celeron, Pentium III</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>3</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>815G</p></td>
<td><p>Solano</p></td>
<td><p>82815G</p></td>
<td><p>ICH/ICH0</p></td>
<td><p>September 2001</p></td>
<td><p>Celeron, Pentium III</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>3</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>815EG</p></td>
<td><p>Solano</p></td>
<td><p>82815G</p></td>
<td><p>ICH2</p></td>
<td><p>September 2001</p></td>
<td><p>Celeron, Pentium III</p></td>
<td><p>66/100/133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>512 MB</p></td>
<td><p>3</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### Pentium III-M移動晶片組

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器</p></th>
<th><p>前端匯流排</p></th>
<th><p>對稱多處理機</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>記憶庫</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>PCI</p></th>
<th><p>AGP速度</p></th>
<th><p>集成顯核</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>830M</p></td>
<td><p>Almador</p></td>
<td><p>82830M</p></td>
<td><p>ICH3M</p></td>
<td><p>July 2001</p></td>
<td><p>Celeron, Pentium III-M</p></td>
<td><p>133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>1 GB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>830MG</p></td>
<td><p>Almador</p></td>
<td><p>82830MG</p></td>
<td><p>ICH3M</p></td>
<td></td>
<td><p>Celeron, Pentium III-M</p></td>
<td><p>133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>1 GB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>830MP</p></td>
<td><p>Almador</p></td>
<td><p>82830MP</p></td>
<td><p>ICH3M</p></td>
<td></td>
<td><p>Celeron, Pentium III-M</p></td>
<td><p>133 MHz</p></td>
<td></td>
<td><p>PC133 SDRAM</p></td>
<td><p>1 GB</p></td>
<td><p>2</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### [Pentium 4晶片組](../Page/Pentium_4.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器</p></th>
<th><p>前端匯流排</p></th>
<th><p>對稱多處理機</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>PCI類型</p></th>
<th><p>顯示核心</p></th>
<th><p><a href="../Page/TDP.md" title="wikilink">TDP</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Intel_850.md" title="wikilink">850</a></p></td>
<td><p>Tehama</p></td>
<td><p>82850 (MCH)</p></td>
<td><p>ICH2</p></td>
<td><p>November 2000</p></td>
<td><p>Pentium 4</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>PC800/600 RDRAM</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4×</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>860</p></td>
<td><p>Colusa</p></td>
<td><p>82860 (MCH)</p></td>
<td><p>ICH2</p></td>
<td><p>May 2001</p></td>
<td><p>Xeon</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>PC800/600 RDRAM</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4×</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>845</p></td>
<td><p>Brookdale</p></td>
<td><p>82845 (MCH)</p></td>
<td><p>ICH2</p></td>
<td><p>January 2002</p></td>
<td><p>Celeron, Pentium 4</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 200/266 or PC133</p></td>
<td><p>2 GB DDR, 3 GB SDR</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4×</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>850E</p></td>
<td><p>Tehama-E</p></td>
<td><p>82850E (MCH)</p></td>
<td><p>ICH2</p></td>
<td><p>May 2002</p></td>
<td><p>Pentium 4</p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>PC1066/800 RDRAM</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4×</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>845E</p></td>
<td><p>Brookdale-E</p></td>
<td><p>82845E (MCH)</p></td>
<td><p>ICH4</p></td>
<td><p>May 2002</p></td>
<td><p>Celeron, Celeron D, Pentium 4</p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>DDR 200/266</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4×</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>845GL</p></td>
<td><p>Brookdale-GL</p></td>
<td><p>82845GL (GMCH)</p></td>
<td><p>ICH4</p></td>
<td><p>May 2002</p></td>
<td><p>Celeron, Pentium 4</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 266, PC133</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>整合</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>845G</p></td>
<td><p>Brookdale-G</p></td>
<td><p>82845G (GMCH)</p></td>
<td><p>ICH4</p></td>
<td><p>May 2002</p></td>
<td><p>Celeron, Celeron D, Pentium 4</p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>DDR 266, PC133</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4× &amp; 整合</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>845GE</p></td>
<td><p>Brookdale-GE</p></td>
<td><p>82845GE (GMCH)</p></td>
<td><p>ICH4</p></td>
<td><p>October 2002</p></td>
<td><p>Pentium 4</p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a></p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4× &amp; 整合</p></td>
<td><p>6.3 W</p></td>
</tr>
<tr class="odd">
<td><p>845PE</p></td>
<td><p>Brookdale-PE</p></td>
<td><p>82845PE (MCH)</p></td>
<td><p>ICH4</p></td>
<td><p>October 2002</p></td>
<td><p>Celeron, Pentium 4</p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a></p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4×</p></td>
<td><p>5.6 W</p></td>
</tr>
<tr class="even">
<td><p>845GV</p></td>
<td><p>Brookdale-GV</p></td>
<td><p>82845GV (GMCH)</p></td>
<td><p>ICH4</p></td>
<td><p>October 2002</p></td>
<td><p>Celeron, 測試不支援 90 nm Celeron D, Pentium 4</p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a>, PC133</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>整合</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>875P</p></td>
<td><p>Canterwood</p></td>
<td><p>82875P (MCH)</p></td>
<td><p>ICH5/ICH5R</p></td>
<td><p>April 2003</p></td>
<td><p>Pentium 4, Xeon</p></td>
<td><p>400/533/800 MHz</p></td>
<td></td>
<td><p>雙通道 DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a>/400</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>v2.3/33 MHz</p></td>
<td><p>AGP 8×</p></td>
<td><p>12.1 W</p></td>
</tr>
<tr class="even">
<td><p>865G</p></td>
<td><p>Springdale</p></td>
<td><p>82865G (GMCH)</p></td>
<td><p>ICH5/ICH5R</p></td>
<td><p>May 2003</p></td>
<td><p>Pentium 4, Celeron, Celeron D</p></td>
<td><p>400/533/800 MHz</p></td>
<td></td>
<td><p>雙通道 DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a>/400</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>v2.3/33 MHz</p></td>
<td><p>AGP 8× &amp; 整合</p></td>
<td><p>12.9 W</p></td>
</tr>
<tr class="odd">
<td><p>865P</p></td>
<td><p>Springdale-P</p></td>
<td><p>82865P</p></td>
<td><p>ICH5</p></td>
<td><p>May 2003</p></td>
<td><p>Pentium 4, Celeron D</p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>雙通道 DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a></p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>v2.3/33 MHz</p></td>
<td><p>AGP 8×</p></td>
<td><p>10.3 W</p></td>
</tr>
<tr class="even">
<td><p>865PE</p></td>
<td><p>Springdale-PE</p></td>
<td><p>82865PE</p></td>
<td><p>ICH5/ICH5R</p></td>
<td><p>May 2003</p></td>
<td><p>Pentium 4, Celeron D, Core 2</p></td>
<td><p>400/533/800 MHz</p></td>
<td></td>
<td><p>雙通道 DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a>/400</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>v2.3/33 MHz</p></td>
<td><p>AGP 8×</p></td>
<td><p>11.3 W</p></td>
</tr>
<tr class="odd">
<td><p>848P</p></td>
<td><p>Breeds Hill</p></td>
<td><p>82848P (MCH)</p></td>
<td><p>ICH5/ICH5R</p></td>
<td><p>August 2003</p></td>
<td><p>Celeron, Celeron D, Pentium 4</p></td>
<td><p>400/533/800 MHz</p></td>
<td></td>
<td><p>DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a>/400</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.3/33 MHz</p></td>
<td><p>AGP 8×</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>865GV</p></td>
<td><p>Springdale-GV</p></td>
<td><p>82865GV (GMCH)</p></td>
<td><p>ICH5/ICH5R</p></td>
<td><p>September 2003</p></td>
<td><p>Pentium 4, Celeron D</p></td>
<td><p>400/533/800 MHz</p></td>
<td></td>
<td><p>雙通道 DDR 266/<a href="../Page/PC2700.md" title="wikilink">333</a>/400</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>v2.3/33 MHz</p></td>
<td><p>整合</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

摘要：

  - 875P (Canterwood)
      - 規格與E7205基本相同,
        但它支援800 MHz前端匯流排，支援[雙通道DDR](../Page/雙通道.md "wikilink")
        400 ECC或Non-ECC記憶體以及CSA，SATA RAID與PAT技術
      - 支援[Socket 640接口的Xeon處理器](../Page/Socket_640.md "wikilink")
  - 865PE (Springdale)
      - 與875P相比，取消了PAT技術，只支援雙通道DDR 400 Non-ECC記憶體，後期支援Core 2
        800MHz外頻的處理器，算是生命週期很長的晶片組。
      - 簡化版：
          - 865P - 規格與865PE基本相同，但只支援533 MHz前端匯流排與雙通道DDR 333記憶體
          - 848P - 規格與865PE基本相同，但不支援雙通道記憶體
  - 865G (Springdale-G)
      - 與865PE相比內部整合了[Extreme Graphics
        2顯示核心](../Page/Intel_Extreme_Graphics.md "wikilink")
      - 簡化版：
          - 865GL - 規格與865G基本相同，但不提供AGP插槽

#### Pentium 4-M/[Pentium M](../Page/Pentium_M.md "wikilink")/[Celeron M移動晶片組](../Page/Celeron_M.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器</p></th>
<th><p>前端匯流排</p></th>
<th><p>對稱多處理機</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>PCI類型</p></th>
<th><p>顯示核心</p></th>
<th><p><a href="../Page/TDP.md" title="wikilink">TDP</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>845MP</p></td>
<td><p>Brookdale-M</p></td>
<td><p>82845 (MCH)</p></td>
<td><p>ICH3-M</p></td>
<td><p>March 2002</p></td>
<td><p>Mobile Celeron, Pentium 4-M</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 200/266</p></td>
<td><p>1 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4×</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>845MZ</p></td>
<td><p>Brookdale-MZ</p></td>
<td><p>82845 (MCH)</p></td>
<td><p>ICH3-M</p></td>
<td><p>March 2002</p></td>
<td><p>Mobile Celeron, Pentium 4-M</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 200</p></td>
<td><p>1 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 4×</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>852GM</p></td>
<td><p>Montara-GM</p></td>
<td><p>82852GM (GMCH)</p></td>
<td><p>ICH4-M</p></td>
<td><p>Q2, '04</p></td>
<td><p><a href="../Page/Pentium_4#Mobile_Pentium_4_M.md" title="wikilink">Pentium 4-M</a>, <a href="../Page/Celeron#Celeron_.28NetBurst.29.md" title="wikilink">Celeron</a>, <a href="../Page/Celeron#Mobile_Celeron_and_Celeron_M.md" title="wikilink">Celeron M</a></p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 200/266</p></td>
<td><p>1 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>整合32位 3D 顯示核心 @ 133 MHz</p></td>
<td><p>3.2 W</p></td>
</tr>
<tr class="even">
<td><p>852GMV</p></td>
<td><p>Montara-GM</p></td>
<td><p>82852GMV (GMCH)</p></td>
<td><p>ICH4-M</p></td>
<td></td>
<td><p>Pentium 4-M, Celeron, Celeron M</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 200/266</p></td>
<td><p>1 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>整合32位 3D Core @ 133 MHz</p></td>
<td><p>3.2 W</p></td>
</tr>
<tr class="odd">
<td><p>852PM</p></td>
<td><p>Montara-GM</p></td>
<td><p>82852PM (MCH)</p></td>
<td><p>ICH4-M</p></td>
<td></td>
<td><p>Pentium 4-M, Celeron, <a href="../Page/Celeron#Celeron_D.md" title="wikilink">Celeron D</a></p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>DDR 200/266/333</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 1x/2×/4×</p></td>
<td><p>5.7 W</p></td>
</tr>
<tr class="even">
<td><p>852GME</p></td>
<td><p>Montara-GM</p></td>
<td><p>82852GME (GMCH)</p></td>
<td><p>ICH4-M</p></td>
<td><p>Q4, '03</p></td>
<td><p>Pentium 4-M, Celeron, Celeron D</p></td>
<td><p>400/533 MHz</p></td>
<td></td>
<td><p>DDR 200/266/333</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>整合 Extreme Graphics 2 顯示核心</p></td>
<td><p>5.7 W</p></td>
</tr>
<tr class="odd">
<td><p>855GM</p></td>
<td><p>Montara-GM</p></td>
<td><p>82855GM (GMCH)</p></td>
<td><p>ICH4-M</p></td>
<td><p>March 2003</p></td>
<td><p><a href="../Page/Pentium_M.md" title="wikilink">Pentium M</a>, Celeron M</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 200/266/333</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>整合 Extreme Graphics 2 顯示核心</p></td>
<td><p>3.2 W</p></td>
</tr>
<tr class="even">
<td><p>855GME</p></td>
<td><p>Montara-GM</p></td>
<td><p>82855GME (MCH)</p></td>
<td><p>ICH4-M</p></td>
<td><p>March 2003</p></td>
<td><p>Pentium M, Celeron M</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 200/266/333</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>整合 Extreme Graphics 2 顯示核心</p></td>
<td><p>4.3 W</p></td>
</tr>
<tr class="odd">
<td><p>855PM</p></td>
<td><p>Odem</p></td>
<td><p>82855PM (MCH)</p></td>
<td><p>ICH4-M</p></td>
<td><p>March 2003</p></td>
<td><p>Pentium M, Celeron M</p></td>
<td><p>400 MHz</p></td>
<td></td>
<td><p>DDR 200/266</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>v2.2/33 MHz</p></td>
<td><p>AGP 2×/4×</p></td>
<td><p>5.7 W</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 8xx系列晶片組南橋

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>型號</p></th>
<th><p><a href="../Page/ATA.md" title="wikilink">ATA</a></p></th>
<th><p><a href="../Page/SATA.md" title="wikilink">SATA</a></p></th>
<th><p><a href="../Page/RAID.md" title="wikilink">RAID等級</a></p></th>
<th><p><a href="../Page/USB.md" title="wikilink">USB</a></p></th>
<th><p><a href="../Page/PCI.md" title="wikilink">PCI</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ICH</p></td>
<td><p>82801AA</p></td>
<td><p>UDMA 66/33</p></td>
<td></td>
<td></td>
<td><p>1.1版, 2個接口</p></td>
<td><p>Rev 2.2, 6個PCI接口</p></td>
</tr>
<tr class="even">
<td><p>ICH0</p></td>
<td><p>82801AB</p></td>
<td><p>UDMA 33</p></td>
<td></td>
<td></td>
<td><p>1.1版, 2個接口</p></td>
<td><p>Rev 2.2, 4個PCI接口</p></td>
</tr>
<tr class="odd">
<td><p>ICH2</p></td>
<td><p>82801BA</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>1.1版, 4個接口</p></td>
<td><p>Rev 2.2, 6個PCI接口</p></td>
</tr>
<tr class="even">
<td><p>ICH2-M</p></td>
<td><p>82801BAM</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>1.1版, 2個接口</p></td>
<td><p>Rev 2.2, 2個PCI接口</p></td>
</tr>
<tr class="odd">
<td><p>ICH3-S</p></td>
<td><p>82801CA</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>1.1版, 6個接口</p></td>
<td><p>Rev 2.2, 6個PCI接口</p></td>
</tr>
<tr class="even">
<td><p>ICH3-M</p></td>
<td><p>82801CAM</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>1.1版, 2個接口</p></td>
<td><p>Rev 2.2, 2個PCI接口</p></td>
</tr>
<tr class="odd">
<td><p>ICH4</p></td>
<td><p>82801DB</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>2.0版, 6個接口</p></td>
<td><p>Rev 2.2, 6個PCI接口</p></td>
</tr>
<tr class="even">
<td><p>ICH4-M</p></td>
<td><p>82801DBM</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>2.0版, 4個接口</p></td>
<td><p>Rev 2.2, 3個PCI接口</p></td>
</tr>
<tr class="odd">
<td><p>ICH5</p></td>
<td><p>82801EB</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>2.0版, 6個接口</p></td>
<td><p>Rev 2.3, 6個PCI接口</p></td>
</tr>
<tr class="even">
<td><p>ICH5R</p></td>
<td><p>82801ER</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>2.0版, 6個接口</p></td>
<td><p>Rev 2.3, 6個PCI接口</p></td>
</tr>
<tr class="odd">
<td><p>ICH5-M</p></td>
<td><p>82801EBM</p></td>
<td><p>UDMA 100/66/33</p></td>
<td></td>
<td></td>
<td><p>2.0版, 4個接口</p></td>
<td><p>Rev 2.3, 4個PCI接口</p></td>
</tr>
</tbody>
</table>

### 9xx與x3x/x4x系列晶片組

#### Pentium 4/[Pentium D](../Page/Pentium_D.md "wikilink")/[Pentium EE晶片組](../Page/Pentium_Extreme_Edition.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器支援</p></th>
<th><p>前端匯流排 (MHz)</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>顯示核心</p></th>
<th><p><a href="../Page/TDP.md" title="wikilink">TDP</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>910GL</p></td>
<td><p>Grantsdale-GL</p></td>
<td><p>82910GL (GMCH)</p></td>
<td><p>ICH6</p></td>
<td><p>September 2004</p></td>
<td><p>Pentium 4, Celeron, Celeron D</p></td>
<td><p>533</p></td>
<td><p><a href="../Page/DDR_SDRAM.md" title="wikilink">DDR</a> 333/400</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>整合 <a href="../Page/Intel_GMA#GMA_900.md" title="wikilink">GMA 900</a></p></td>
<td><p>16.3 W</p></td>
</tr>
<tr class="even">
<td><p>915P</p></td>
<td><p>Grantsdale</p></td>
<td><p>82915P (MCH)</p></td>
<td><p>ICH6</p></td>
<td><p>June 2004</p></td>
<td><p>Pentium 4, Celeron D</p></td>
<td><p>533/800</p></td>
<td><ul>
<li>DDR 333/400</li>
<li>DDR2 400/533</li>
</ul></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td><p>16.3 W</p></td>
</tr>
<tr class="odd">
<td><p>915PL</p></td>
<td><p>Grantsdale-PL</p></td>
<td><p>82915PL (MCH)</p></td>
<td><p>ICH6</p></td>
<td><p>March 2005</p></td>
<td><p>Pentium 4, Celeron D</p></td>
<td><p>533/800</p></td>
<td><p>DDR 333/400</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td><p>16.3 W</p></td>
</tr>
<tr class="even">
<td><p>915G</p></td>
<td><p>Grantsdale-G</p></td>
<td><p>82915G (GMCH)</p></td>
<td><p>ICH6</p></td>
<td><p>June 2004</p></td>
<td><p>Pentium 4, Celeron D</p></td>
<td><p>533/800</p></td>
<td><ul>
<li>DDR 333/400</li>
<li>DDR2 400/533</li>
</ul></td>
<td><p>4 GB</p></td>
<td></td>
<td><ul>
<li>PCI-Express 16×</li>
<li>整合 GMA 900</li>
</ul></td>
<td><p>16.3 W</p></td>
</tr>
<tr class="odd">
<td><p>915GL</p></td>
<td><p>Grantsdale-GL</p></td>
<td><p>82915GL (GMCH)</p></td>
<td><p>ICH6</p></td>
<td><p>March 2005</p></td>
<td><p>Pentium 4, Celeron D</p></td>
<td><p>533/800</p></td>
<td><p>DDR 333/400</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>整合 GMA 900</p></td>
<td><p>16.3 W</p></td>
</tr>
<tr class="even">
<td><p>915GV</p></td>
<td><p>Grantsdale-GV</p></td>
<td><p>82915GV (GMCH)</p></td>
<td><p>ICH6</p></td>
<td><p>June 2004</p></td>
<td><p>Pentium 4, Celeron D</p></td>
<td><p>533/800</p></td>
<td><ul>
<li>DDR 333/400</li>
<li>DDR2 400/533</li>
</ul></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>整合 GMA 900</p></td>
<td><p>16.3 W</p></td>
</tr>
<tr class="odd">
<td><p>925X</p></td>
<td><p>Alderwood</p></td>
<td><p>82925X (MCH)</p></td>
<td><p>ICH6</p></td>
<td><p>June 2004</p></td>
<td><p>Pentium 4, Pentium 4 EE</p></td>
<td><p>800</p></td>
<td><p><a href="../Page/DDR2_SDRAM.md" title="wikilink">DDR2</a> 400/533</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td><p>12.3 W</p></td>
</tr>
<tr class="even">
<td><p>925XE</p></td>
<td><p>Alderwood-XE</p></td>
<td><p>82925XE (MCH)</p></td>
<td><p>ICH6</p></td>
<td><p>November 2004</p></td>
<td><p>Pentium 4, Pentium 4 EE</p></td>
<td><p>800/1066</p></td>
<td><p>DDR2 400/533</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td><p>13.3 W</p></td>
</tr>
<tr class="odd">
<td><p>955X</p></td>
<td><p>Glenwood</p></td>
<td><p>82955X (MCH)</p></td>
<td><p>ICH7</p></td>
<td><p>April 2005</p></td>
<td><p>Pentium 4, Pentium 4 EE, Pentium D, Pentium XE</p></td>
<td><p>800/1066</p></td>
<td><p>DDR2 533/667</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td><p>13.5 W</p></td>
</tr>
<tr class="even">
<td><p>945P</p></td>
<td><p>Lakeport</p></td>
<td><p>82945P (MCH)</p></td>
<td><p>ICH7</p></td>
<td><p>May 2005</p></td>
<td><p>Pentium 4, Pentium D, Celeron D</p></td>
<td><p>533/800/1066</p></td>
<td><p>DDR2 400/533/667</p></td>
<td><p>4 GB<sup>1</sup></p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td><p>15.2 W</p></td>
</tr>
<tr class="odd">
<td><p>945PL</p></td>
<td><p>Lakeport-PL</p></td>
<td><p>82945PL (MCH)</p></td>
<td><p>ICH7</p></td>
<td><p>March 2006</p></td>
<td><p>Pentium 4, Pentium D, Celeron D</p></td>
<td><p>533/800</p></td>
<td><p>DDR2 400/533</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td><p>15.2 W</p></td>
</tr>
<tr class="even">
<td><p>945G</p></td>
<td><p>Lakeport-G</p></td>
<td><p>82945G (GMCH)</p></td>
<td><p>ICH7</p></td>
<td><p>May 2005</p></td>
<td><p>Pentium 4, Pentium D, Celeron D</p></td>
<td><p>533/800/1066</p></td>
<td><p>DDR2 400/533/667</p></td>
<td><p>4 GB<sup>1</sup></p></td>
<td></td>
<td><ul>
<li>PCI-Express 16×</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_950.md" title="wikilink">GMA 950</a></li>
</ul></td>
<td><p>22.2 W</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

摘要：

  - 915P (Grantsdale)
      - 支援800 MHz的前端匯流排。支援最大4GB的雙通道DDR 400或DDR2 533記憶體。最多可提供一個PCI-E 1.0
        X16插槽與四個PCI-E 1.0 X1插槽。可與其搭配ICH6R南橋來支援混合RAID
      - 簡化版：
          - 915PL - 與915P規格基本相同，但只支援最大2GB的雙通道DDR 400記憶體
  - 915G (Grantsdale-G)
      - 與915P相比內部整合了[GMA900顯示核心](../Page/Intel_GMA.md "wikilink")
      - 簡化版：
          - 915GL -
            與915PL規格基本相同，內部整合了[GMA900顯示核心](../Page/Intel_GMA.md "wikilink")，只支援最大4GB的雙通道DDR
            400記憶體，不提供PCI-E X16插槽
          - 915GV - 與915GL規格基本相同，只支援最大2GB的雙通道DDR 400記憶體，不提供PCI-E X16插槽
          - 910GL - 與915GV規格基本相同，但只支援533 MHz的前端匯流排
  - 925XE (Alderwood)
      - 上一代875P的後續品，與915P規格基本相同，支援1066 MHz前端匯流排，強制支援雙通道DDR2 400/533
        ECC或Non-ECC記憶體而不能使用DDR 400記憶體
      - 簡化版：
          - 925X - 與925XE規格基本相同，只支援800 MHz前端匯流排
  - 945P (Lakeport)
      - 上一代915P的替代品，原生支援雙核心處理器，支援1066 MHz前端匯流排，最大4GB的雙通道DDR2
        533/667記憶體，可與其搭配ICH7R系列南橋支援SATA 3.0
        Gbit/s與RAID5等級。晚期支援部份Core 2系列的CPU。
      - 簡化版：
          - 945PL -與945P規格基本相同，只支援800 MHz前端匯流排，最大2GB記憶體
  - 945G (Lakeport-G)
      - 與945P相比內部整合了[GMA950顯示核心](../Page/Intel_GMA.md "wikilink")
      - 簡化版：
          - 945GZ -
            與945PL規格基本相同，內部整合了[GMA950顯示核心](../Page/Intel_GMA.md "wikilink")，不提供PCI-E
            X16插槽
  - 955X (Glenwood)
      - 上一代925XE的替代品，與945P規格基本相同，支援最大8GB的DDR2 533/667 ECC或Non-ECC記憶體

#### [Pentium M](../Page/Pentium_M.md "wikilink")/[Celeron M移動晶片組](../Page/Celeron_M.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器支援</p></th>
<th><p>前端匯流排 (MHz)</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>顯示核心</p></th>
<th><p><a href="../Page/TDP.md" title="wikilink">TDP</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>910GML</p></td>
<td><p>Alviso-GM</p></td>
<td><p>82910GML (GMCH)</p></td>
<td><p>ICH6-M</p></td>
<td><p>January 2005</p></td>
<td><p>Celeron M</p></td>
<td><p>400 MHz</p></td>
<td><p>DDR 333, DDR2 400</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>整合 <a href="../Page/Intel_GMA#GMA_900.md" title="wikilink">GMA 900</a></p></td>
<td><p>10.5 W</p></td>
</tr>
<tr class="even">
<td><p>915GMS</p></td>
<td><p>Alviso-GM</p></td>
<td><p>82915GMS (GMCH)</p></td>
<td><p>ICH6-M</p></td>
<td><p>January 2005</p></td>
<td><p>Pentium M, Celeron M</p></td>
<td><p>400 MHz</p></td>
<td><p>DDR2 400</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>整合 GMA 900</p></td>
<td><p>13.9 W</p></td>
</tr>
<tr class="odd">
<td><p>915GM</p></td>
<td><p>Alviso-GM</p></td>
<td><p>82915GM (GMCH)</p></td>
<td><p>ICH6-M</p></td>
<td><p>January 2005</p></td>
<td><p>Pentium M, Celeron M</p></td>
<td><p>400/533 MHz</p></td>
<td><p>DDR 333, DDR2 400/533</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>整合 GMA 900</p></td>
<td><p>13.9 W</p></td>
</tr>
<tr class="even">
<td><p>915PM</p></td>
<td><p>Alviso</p></td>
<td><p>82915PM (MCH)</p></td>
<td><p>ICH6-M</p></td>
<td><p>January 2005</p></td>
<td><p>Pentium M, Celeron M</p></td>
<td><p>400/533 MHz</p></td>
<td><p>DDR 333, DDR2 400/533</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td><p>10.5 W</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### [Core](../Page/Intel_Core.md "wikilink")/[Core 2移動晶片組](../Page/Intel_Core_2.md "wikilink")

| 晶片組     | 研發代號      | 型號               | 南橋     | 發表日期         | 處理器支援                                                                                                                                                                       | 前端匯流排       | 記憶體類型            | 最大記憶體 | 顯示核心                                                                       | [TDP](../Page/TDP.md "wikilink") |
| ------- | --------- | ---------------- | ------ | ------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- | ---------------- | ----- | -------------------------------------------------------------------------- | -------------------------------- |
| 940GML  | Calistoga | 82940GML (GMCH)  | ICH7-M | January 2006 | Celeron M, Core Solo, Pentium Dual-Core                                                                                                                                     | 533 MHz     | DDR2 400/533     | 2 GB  | 整合 [GMA 950](../Page/Intel_GMA#GMA_950.md "wikilink") 顯示核心（最高166 MHz 3D渲染） | 7 W                              |
| 943GML  | Calistoga | 82943GML (GMCH)  | ICH7-M | January 2006 | Celeron M, Core Solo, Pentium Dual-Core                                                                                                                                     | 533 MHz     | DDR2 400/533     | 2 GB  | 整合 GMA 950 顯示核心 （最高200 MHz 3D渲染）                                           |                                  |
| 945GMS  | Calistoga | 82945GMS (GMCH)  | ICH7-M | January 2006 | [Core 2 Duo](../Page/Intel_Core_2.md "wikilink"), [Core Duo](../Page/Intel_Core.md "wikilink"), Pentium Dual-Core, [Core Solo](../Page/Intel_Core.md "wikilink"), Celeron M | 533/667 MHz | DDR2 400/533     | 2 GB  | 整合 GMA 950 顯示核心（最高166 MHz 3D渲染）                                            | 7 W                              |
| 945GSE  | Calistoga | 82945GSE (GMCH)  | ICH7-M | Q2, '08      | [Intel Atom](../Page/Intel_Atom.md "wikilink")                                                                                                                              | 533/667 MHz | DDR2 400/533     | 2 GB  | 整合 GMA 950 顯示核心（最高166 MHz 3D渲染）                                            | 6 W                              |
| 945GM/E | Calistoga | 82945GM/E (GMCH) | ICH7-M | January 2006 | Core 2 Duo, Core Duo, Pentium Dual-Core, Core Solo, Celeron M                                                                                                               | 533/667 MHz | DDR2 400/533/667 | 4 GB  | 整合 GMA 950 顯示核心 （最高250 MHz 3D渲染）                                           | 7 W                              |
| 945PM   | Calistoga | 82945PM (MCH)    | ICH7-M | January 2006 | Core 2 Duo, Core Duo, Pentium Dual-Core, Core Solo, Celeron M                                                                                                               | 533/667 MHz | DDR2 400/533/667 | 4 GB  | PCI-Express 16×                                                            | 7 W                              |
|         |           |                  |        |              |                                                                                                                                                                             |             |                  |       |                                                                            |                                  |

#### [Core 2晶片組](../Page/Intel_Core_2.md "wikilink")

以下所列晶片組全部支援Pentium
Dual-Core以及[Core微架構的Celeron處理器](../Page/Intel_Core微處理器架構.md "wikilink")，支援部分[LGA
775接口的](../Page/LGA_775.md "wikilink")[NetBurst架構處理器](../Page/NetBurst.md "wikilink")\[9\]\[10\]。

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器支援</p></th>
<th><p>處理器製程</p></th>
<th><p>硬件虛擬化支援</p></th>
<th><p>前端匯流排</p></th>
<th><p>記憶體類型</p></th>
<th><p>最大記憶體</p></th>
<th><p>奇偶校驗/錯誤檢查</p></th>
<th><p>顯示核心</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>945GC</p></td>
<td><p>Lakeport-GC</p></td>
<td><p>82945GC (MCH)</p></td>
<td><p>ICH7/ICH7R/ICH7-DH</p></td>
<td><p>2005年1月</p></td>
<td><p>Pentium 4/Pentium D/Celeron D/Core 2 Duo/Pentium Dual-Core/Atom</p></td>
<td><p>45 nm</p></td>
<td><p>rowspan="14" </p></td>
<td><p>533/800 MHz</p></td>
<td><p><a href="../Page/DDR2_SDRAM.md" title="wikilink">DDR2</a> 533/667</p></td>
<td><p>2 GB(部分主板支援4GB但只有3.27GB可用)</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16×</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_950.md" title="wikilink">GMA 950</a> graphics</li>
</ul></td>
</tr>
<tr class="even">
<td><p>945GZ</p></td>
<td><p>Lakeport-GZ</p></td>
<td><p>82945GZ (GMCH)</p></td>
<td><p>ICH7</p></td>
<td><p>2005年6月</p></td>
<td><p>Pentium 4/Pentium D/Celeron D/Core 2 Duo/Pentium Dual-Core</p></td>
<td><p>65 nm</p></td>
<td><p>533/800 MHz</p></td>
<td><p>DDR2 400/533</p></td>
<td><p>2 GB</p></td>
<td></td>
<td><p>整合 GMA 950</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>946PL</p></td>
<td><p>Broadwater-PL</p></td>
<td><p>82946PL (MCH)</p></td>
<td><p>2006年7月</p></td>
<td><p>533/800 MHz</p></td>
<td><p>DDR2 533/667</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><p>PCI-Express 16×</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>946GZ</p></td>
<td><p>Broadwater-GZ</p></td>
<td><p>82946GZ (GMCH)</p></td>
<td><p>533/800 MHz</p></td>
<td><p>DDR2 533/667</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><ul>
<li>PCI-Express 16×</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_3000.md" title="wikilink">GMA 3000</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>975X</p></td>
<td><p>Glenwood</p></td>
<td><p>82975X (MCH)[11]</p></td>
<td><p>ICH7/ICH7R/ICH7-DH</p></td>
<td><p>2005年11月</p></td>
<td><p>Pentium D, Core 2 Quad, Core 2 Duo, Pentium Dual-Core</p></td>
<td><p>533/667/800/1066 MHz</p></td>
<td><p>533/667/800</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16×</li>
<li>2 PCI-Express 8×</li>
</ul></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>P965</p></td>
<td><p>Broadwater(P)</p></td>
<td><p>82P965 (MCH)</p></td>
<td><p>ICH8/ICH8R/ICH8-DH</p></td>
<td><p>2006年6月</p></td>
<td><p>Pentium Dual-Core/Core 2 Quad/Core 2 Duo</p></td>
<td><p>45 nm</p></td>
<td><p>533/800/1066 MHz</p></td>
<td><p>DDR2 533/667/800</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><ul>
<li>PCI-Express 16×</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p>G965</p></td>
<td><p>Broadwater(GC)</p></td>
<td><p>82G965 (GMCH)</p></td>
<td><p>Pentium Dual-Core/Core 2 Duo</p></td>
<td><p>65 nm</p></td>
<td><p>533/800/1066 MHz</p></td>
<td><p>DDR2 533/667/800</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><ul>
<li>PCI-Express 16×</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_X3000.md" title="wikilink">GMA X3000</a> graphics</li>
</ul></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Q965</p></td>
<td><p>Broadwater(G)</p></td>
<td><p>82Q965 (GMCH)</p></td>
<td><p>533/800/1066 MHz</p></td>
<td><p>DDR2 533/667/800</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><ul>
<li>PCI-Express 16×</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_3000.md" title="wikilink">GMA 3000</a> graphics</li>
<li>Supports ADD2 Card</li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>P31</p></td>
<td><p>Bearlake (P)</p></td>
<td><p>82P31 (MCH)</p></td>
<td><p>ICH7</p></td>
<td><p>2007年8月</p></td>
<td><p>Pentium Dual-Core/Core 2 Quad/Core 2 Duo</p></td>
<td><p>45 nm</p></td>
<td><p>800/1066 MHz</p></td>
<td><p>DDR2 667/800</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16×</li>
<li>1 PCI-Express 4×</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Intel_P35.md" title="wikilink">P35</a></p></td>
<td><p>Bearlake (P+)</p></td>
<td><p>82P35 (MCH)</p></td>
<td><p>ICH9/ICH9R/ICH9-DH</p></td>
<td><p>2007年6月</p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p><a href="../Page/DDR3_SDRAM.md" title="wikilink">DDR3</a> 800/1066/1333<br />
DDR2 667/800/1066</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16×</li>
<li>1 PCI-Express 4×</li>
</ul></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>G31</p></td>
<td><p>Bearlake (G)</p></td>
<td><p>82G31 (GMCH)</p></td>
<td><p>ICH7</p></td>
<td><p>2007年8月</p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR2 667/800</p></td>
<td><p>4 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16×</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_3100.md" title="wikilink">GMA 3100</a> graphics</li>
</ul></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>G33</p></td>
<td><p>Bearlake (G+)</p></td>
<td><p>82G33 (GMCH)</p></td>
<td><p>ICH9/ICH9R/ICH9-DH</p></td>
<td><p>2007年6月</p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR2 667/800</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><p>整合 GMA 3100 包含：</p>
<ul>
<li><a href="../Page/Intel_Clear_Video_Technology.md" title="wikilink">Intel Clear Video Technology</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>G35</p></td>
<td><p>Broadwater(GC)-Refresh</p></td>
<td><p>82G35 (GMCH)</p></td>
<td><p>ICH8/ICH8R/ICH8-DH</p></td>
<td><p>2007年8月</p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR2 667/800[12][13]</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><p>整合 <a href="../Page/Intel_GMA#GMA_X3500.md" title="wikilink">GMA X3500</a> 包含：</p>
<ul>
<li><a href="../Page/Direct3D#Direct3D_10.md" title="wikilink">DX10</a></li>
<li><a href="../Page/Intel_Clear_Video_Technology.md" title="wikilink">Intel Clear Video Technology</a></li>
</ul></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Q33</p></td>
<td><p>Bearlake (QF)</p></td>
<td><p>82Q33 (MCH)</p></td>
<td><p>ICH9/ICH9R</p></td>
<td><p>2007年6月</p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR2 667/800</p></td>
<td><p>8 GB</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Q35</p></td>
<td><p>Bearlake (Q)</p></td>
<td><p>82Q35 (MCH)</p></td>
<td><p>ICH9/ICH9R/ICH9-DO</p></td>
<td></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR2 667/800</p></td>
<td><p>8 GB</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>X38</p></td>
<td><p>Bearlake (X)</p></td>
<td><p>82X38 (MCH)</p></td>
<td><p>ICH9/ICH9R/ICH9-DH</p></td>
<td><p>2007年8月[14]</p></td>
<td><p>Core 2 Quad/Core 2 Duo/Core 2 Extreme</p></td>
<td><p>rowspan="2" </p></td>
<td><p>800/1066/1333 MHz[15]</p></td>
<td><p>DDR3 800/1066/1333<br />
DDR2 667/800/1066</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><ul>
<li>2 PCI-Express 16× 2.0</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p>X48</p></td>
<td><p>Eaglelake (X)</p></td>
<td><p>82X48 (MCH)</p></td>
<td><p>2008年3月</p></td>
<td><p>1066/1333/1600 MHz</p></td>
<td><p>DDR3 1066/1333/1600<br />
DDR2 533/667/800/1066</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><ul>
<li>2 PCI-Express 16× 2.0</li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>P43</p></td>
<td><p>Eaglelake (P)</p></td>
<td><p>82P43 (MCH)</p></td>
<td><p>ICH10/ICH10R</p></td>
<td><p>2008年6月</p></td>
<td><p>Core 2 Quad/Core 2 Duo</p></td>
<td></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR3 800/1066<br />
DDR2 667/800</p></td>
<td><p>16 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16× 2.0</li>
</ul></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Intel_P45.md" title="wikilink">P45</a></p></td>
<td><p>Eaglelake (P+)</p></td>
<td><p>82P45 (MCH)</p></td>
<td></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR3 800/1066/1333<br />
DDR2 667/800/1066</p></td>
<td><p>16 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16× 2.0</li>
<li>2 PCI-Express 8× 2.0</li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>G41</p></td>
<td><p>Eaglelake (G)</p></td>
<td><p>82G41 (GMCH)</p></td>
<td><p>ICH7</p></td>
<td><p>2008年9月</p></td>
<td><p>rowspan="3" </p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR3 800/1066<br />
DDR2 667/800</p></td>
<td><p>8 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16×</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_X4500.md" title="wikilink">GMA X4500</a> graphics</li>
</ul></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>G43</p></td>
<td><p>82G43 (GMCH)</p></td>
<td><p>ICH10/ICH10R</p></td>
<td><p>2008年6月</p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR3 800/1066<br />
DDR2 667/800</p></td>
<td><p>16 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16× 2.0</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_X4500.md" title="wikilink">GMA X4500</a> graphics</li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>G45</p></td>
<td><p>Eaglelake (G+)</p></td>
<td><p>82G45 (GMCH)</p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR3 800/1066<br />
DDR2 667/800[16]</p></td>
<td><p>16 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16× 2.0</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_X4500.md" title="wikilink">GMA X4500HD</a> graphics</li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>B43</p></td>
<td><p>Eaglelake (B)</p></td>
<td><p>82B43 (GMCH)</p></td>
<td><p>ICH10D</p></td>
<td><p>2008年12月</p></td>
<td><p>rowspan="2" </p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR3 800/1066/1333<br />
DDR2 667/800/1066[17]</p></td>
<td><p>16 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16× 2.0</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_X4500.md" title="wikilink">GMA X4500</a> graphics</li>
</ul></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Q43</p></td>
<td><p>Eaglelake (Q)</p></td>
<td><p>82Q43 (GMCH)</p></td>
<td><p>ICH10/ICH10R/ICH10D</p></td>
<td><p>2008年9月</p></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR3 800/1066<br />
DDR2 667/800[18]</p></td>
<td><p>16 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16× 2.0</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_X4500.md" title="wikilink">GMA X4500</a> graphics</li>
</ul></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Q45</p></td>
<td><p>82Q45 (GMCH)</p></td>
<td><p>ICH10/ICH10R/ICH10-DO</p></td>
<td></td>
<td><p>800/1066/1333 MHz</p></td>
<td><p>DDR3 800/1066<br />
DDR2 667/800[19]</p></td>
<td><p>16 GB</p></td>
<td></td>
<td><ul>
<li>1 PCI-Express 16× 2.0</li>
<li>整合 <a href="../Page/Intel_GMA#GMA_X4500.md" title="wikilink">GMA X4500</a>[20] graphics</li>
</ul></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

摘要：

  - 975X (Glenwood)
      - 上一代955X的替代品，支援65奈米處理器，後期通過更改供電電路來支援[Core
        2處理器](../Page/Core_2.md "wikilink")。提供兩個PCI-E
        X16插槽，採用X8+X8的方式來支援[ATi的](../Page/ATi.md "wikilink")[CrossFire雙顯卡互聯技術](../Page/CrossFire.md "wikilink")，支援最大8GB的雙通道DDR2
        667 ECC或Non-ECC記憶體
  - P965 (Broadwater)
      - 上一代945P的後續品，規格與975X基本相同，原生支援65奈米的[Core
        2處理器](../Page/Core_2.md "wikilink")，後期通過更改供電電路來支援45奈米[Core
        2處理器](../Page/Core_2.md "wikilink")，支援最大8GB的雙通道DDR2 667/800
        Non-ECC。記憶體搭配ICH8系列南橋
  - G965 (BroadwaterG)
      - 與P965相比內部整合了[GMA X3000顯示核心](../Page/Intel_GMA.md "wikilink")
  - Q965 (Broadwater)
      - 定位於商用辦公平臺，規格與G965基本相同，支援[vPro技術](../Page/博銳.md "wikilink"),
        支援[ADD2轉接顯示卡](../Page/ADD2.md "wikilink")。
      - 簡化版：
          - Q963 -
            規格與Q965基本相同，但不支援[ADD2轉接顯示卡](../Page/ADD2.md "wikilink")。
  - P35 (Bearlake)
      - 上一代P965的後續品，原生支援65nm、1333 MHz前端匯流排[Core
        2處理器](../Page/Core_2.md "wikilink")，後期也支援45nm[Core
        2處理器](../Page/Core_2.md "wikilink")。支援最大8GB的雙通道DDR2(實際支援到16GB決定於主機板)
        533/667/800或DDR3(8G) 800/1066記憶體。搭配ICH9系列南橋
      - 簡化版：
          - P31 - 規格與P35基本相同，但記憶體只支援到最大4GB的雙通道DDR2
            533/667/800，只搭配ICH7南橋。
  - G35 Broadwater(GC) Refresh
      - 上一代G965的後續品，整合了支援[DirectX](../Page/DirectX.md "wikilink")10的[GMA
        X3500顯示核心](../Page/Intel_GMA.md "wikilink")，支援最大8GB的雙通道DDR2
        533/667/800記憶體
      - 簡化版：
          - G33 Bearlake (G+) - 規格與G35基本相同，整合了[GMA
            X3500的簡化版](../Page/Intel_GMA.md "wikilink")[GMA
            X3100顯示核心](../Page/Intel_GMA.md "wikilink")
          - G31 Bearlake (G) - 規格與G33基本相同，整合了[GMA
            X3500的簡化版](../Page/Intel_GMA.md "wikilink")[GMA
            X3100顯示核心](../Page/Intel_GMA.md "wikilink")，只支援最大4GB的雙通道DDR2
            533/667/800記憶體，南橋只能搭配ICH7系列
  - X38 Bearlake (X)
      - 上一代975X的後續品，規格與975X基本相同，支援65nm、1333 MHz前端匯流排[Core
        2處理器](../Page/Core_2.md "wikilink")。支援最大8GB的雙通道DDR2
        533/667/800 ECC或Non-ECC記憶體，或DDR3 800/1066 Non-ECC記憶體
  - X48 Eaglelake (X)
      - 上一代X38的後續品，規格與X38基本相同，原生支援1600
        MHz前端匯流排並支援PCIE2.0。支援最大16GB的雙通道DDR2
        533/667/800/1066 ECC或Non-ECC記憶體，或DDR3 800/1066/1333
        Non-ECC記憶體。搭配ICH10系列南橋。
  - P45 Eaglelake (P+)
      - 上一代P35的後續品，規格與P35基本相同，原生支援45nm、1600 MHz前端匯流排[Core
        2並支援PCIE](../Page/Core_2.md "wikilink")2.0。支援雙通道DDR2(16GB)
        533/667/800/1066或DDR3(8GB) 800/1066/1333記憶體。搭配ICH10系列南橋。
      - 簡化版：
          - P43 Eaglelake (P) -
            規格與P45基本相同，但不支援組建[ATi的](../Page/ATi.md "wikilink")[CrossFire](../Page/CrossFire.md "wikilink")，只支援最大8GB的雙通道DDR2
            533/667/800或DDR3 800/1066記憶體
  - G45 Eaglelake (G+)
      - 上一代G35的後續品，規格與P45基本相同，但整合了[GMA
        X4500HD顯示核心](../Page/Intel_GMA.md "wikilink")
      - 簡化版：
          - G43 Bearlake (G+) - 規格與G45基本相同，整合了[GMA
            X4500顯示核心](../Page/Intel_GMA.md "wikilink")
          - G41 Bearlake (G) - 規格與G43基本相同，只支援雙通道DDR2(8G)
            533/667/800或DDR3(4G) 800/1066記憶體，南橋只能搭配ICH7系列

#### [Core 2移動晶片組](../Page/Intel_Core_2.md "wikilink")

| 晶片組   | 研發代號           | 型號                                    | 南橋              | 發表日期                            | 處理器支援                                                                                        | 前端匯流排                                | 記憶體類型                      | 最大記憶體                                                                             | 顯示核心                                                                            | [TDP](../Page/TDP.md "wikilink") |
| ----- | -------------- | ------------------------------------- | --------------- | ------------------------------- | -------------------------------------------------------------------------------------------- | ------------------------------------ | -------------------------- | --------------------------------------------------------------------------------- | ------------------------------------------------------------------------------- | -------------------------------- |
| GL960 | Crestline      | 82960GL (GMCH)                        | ICH8-M          | 2007年5月                         | Celeron M, Pentium Dual-Core                                                                 | 533 MHz                              | DDR2 533                   | 2 GB                                                                              | 整合 [GMA X3100](../Page/Intel_GMA#GMA_X3100.md "wikilink") 顯示核心 （最高400 MHz 3D渲染） | 13.5 W                           |
| GM965 | 82965GM (GMCH) | Core 2 Duo                            | 533/800 MHz     | DDR2 533/667                    | 4 GB                                                                                         | 整合 GMA X3100 顯示核心 （最高500 MHz 3D渲染）   |                            |                                                                                   |                                                                                 |                                  |
| PM965 | 82965PM (MCH)  | PCI-Express 16×                       | 8 W             |                                 |                                                                                              |                                      |                            |                                                                                   |                                                                                 |                                  |
| GL40  | Cantiga        | 82GL40 (GMCH)                         | ICH9-M          | 2008年9月                         | Core 2 Duo, Celeron, Celeron M, [Pentium Dual-Core](../Page/Pentium_Dual-Core.md "wikilink") | 667/800 MHz                          | DDR2 667/800, DDR3 667/800 | 整合 [GMA X4500HD](../Page/Intel_GMA#GMA_X4500.md "wikilink") 顯示核心 （最高380 MHz 3D渲染） | 12 W                                                                            |                                  |
| GS45  | 82GS45 (GMCH)  | Core 2 Duo, Core 2 Extreme, Celeron M | 800/1066 MHz    | DDR2 667/800, DDR3 667/800/1066 | 8 GB                                                                                         | 整合 GMA X4500HD 顯示核心 （最高533 MHz 3D渲染） |                            |                                                                                   |                                                                                 |                                  |
| GM45  | 82GM45 (GMCH)  | 667/800/1066 MHz                      |                 |                                 |                                                                                              |                                      |                            |                                                                                   |                                                                                 |                                  |
| PM45  | 82PM45 (MCH)   | Core 2 Duo, Core 2 Extreme            | PCI-Express 16× | 7 W                             |                                                                                              |                                      |                            |                                                                                   |                                                                                 |                                  |

#### 9xx與x3x/x4x系列晶片組南橋

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>型號</p></th>
<th><p><a href="../Page/ATA.md" title="wikilink">ATA</a></p></th>
<th><p><a href="../Page/SATA.md" title="wikilink">SATA</a></p></th>
<th><p><a href="../Page/RAID.md" title="wikilink">RAID等級</a></p></th>
<th><p><a href="../Page/USB.md" title="wikilink">USB</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>ICH6</p></td>
<td><p>82801FB</p></td>
<td><p>rowspan="9" </p></td>
<td><p>4個SATA 1.5 Gbit/s接口</p></td>
<td></td>
<td><p>6個2.0版接口</p></td>
</tr>
<tr class="even">
<td><p>ICH6R</p></td>
<td><p>82801FR</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH6-M</p></td>
<td><p>82801FBM</p></td>
<td><p>2個SATA 1.5 Gbit/s接口</p></td>
<td><p>rowspan="2" </p></td>
<td><p>4個2.0版接口</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH7</p></td>
<td><p>82801GB</p></td>
<td><p>4個SATA 3.0 Gbit/s接口</p></td>
<td><p>8個2.0版接口</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH7R</p></td>
<td><p>82801GR</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH7DH</p></td>
<td><p>82801GDH</p></td>
<td><p>rowspan="2" </p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH7DO</p></td>
<td><p>82801GDO</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH7-M</p></td>
<td><p>82801GBM</p></td>
<td><p>2個SATA 1.5 Gbit/s接口</p></td>
<td></td>
<td><p>4個2.0版接口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH7-M DH</p></td>
<td><p>82801GHM</p></td>
<td><p>4個SATA 1.5 Gbit/s接口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH8</p></td>
<td><p>82801HB</p></td>
<td><p>rowspan="4" </p></td>
<td><p>4個SATA 3.0 Gbit/s接口</p></td>
<td></td>
<td><p>10個2.0版接口</p></td>
</tr>
<tr class="odd">
<td><p>ICH8R</p></td>
<td><p>82801HR</p></td>
<td><p>6個SATA 3.0 Gbit/s接口</p></td>
<td><p>rowspan="3" </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH8DH</p></td>
<td><p>82801HDH</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH8DO</p></td>
<td><p>82801HDO</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH8M</p></td>
<td><p>82801HBM</p></td>
<td><p>rowspan="2" </p></td>
<td><p>4個SATA 3.0 Gbit/s接口</p></td>
<td></td>
<td><p>6個2.0版接口</p></td>
</tr>
<tr class="odd">
<td><p>ICH8M-E</p></td>
<td><p>82801HEM</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH9</p></td>
<td><p>82801IB</p></td>
<td><p>rowspan="12" </p></td>
<td></td>
<td><p>12個2.0版接口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH9R</p></td>
<td><p>82801IR</p></td>
<td><p>6個SATA 3.0 Gbit/s接口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH9DH</p></td>
<td><p>82801IH</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH9DO</p></td>
<td><p>82801IO</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH9M</p></td>
<td><p>82801IBM</p></td>
<td><p>4個SATA 3.0 Gbit/s接口</p></td>
<td></td>
<td><p>8個2.0版接口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH9M-E</p></td>
<td><p>82801IEM</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH10</p></td>
<td><p>82801JB</p></td>
<td><p>6個SATA 3.0 Gbit/s接口</p></td>
<td></td>
<td><p>12個2.0版接口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH10R</p></td>
<td><p>82801JR</p></td>
<td><p>rowspan="3" </p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH10DH</p></td>
<td><p>82801JH</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH10DO</p></td>
<td><p>82801JO</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ICH10M</p></td>
<td><p>82801JBM</p></td>
<td><p>4個SATA 3.0 Gbit/s接口</p></td>
<td></td>
<td><p>8個2.0版接口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>ICH10M-E</p></td>
<td><p>8280JEM</p></td>
<td><p>6個SATA 3.0 Gbit/s接口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

ICH6-ICH8基础版本均只支援IDE模式，不支援AHCI模式，不能使用NCQ，热插拔等。 ICH9可由BIOS軟體控制AHCI、NCQ。
ICH10原生支援AHCI、NCQ。 ICH8之後不再支援原生的IDE ATA 接口。

### 5/6/7/8/9/100/200系列晶片組

#### [Core i系列晶片組](../Page/Intel_Nehalem.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號（步進）</p></th>
<th><p>南橋（類型）</p></th>
<th><p>發表日期</p></th>
<th><p>支援處理器插座</p></th>
<th><p>介面類型</p></th>
<th><p>介面速度</p></th>
<th><p>PCI-E通道及版本</p></th>
<th><p>PCI支持</p></th>
<th><p><a href="../Page/SATA.md" title="wikilink">SATA</a></p></th>
<th><p><a href="../Page/USB.md" title="wikilink">USB</a></p></th>
<th><p>整合式繪圖顯示通道</p></th>
<th><p>功耗</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>X58</p></td>
<td><p>Tylersburg</p></td>
<td><p>SLGBT (B2)<br />
SLGMX (B3)<br />
SLH3M (C2)</p></td>
<td><p>AC82X58 (IOH)</p></td>
<td><p>2008年11月</p></td>
<td><p>LGA 1366</p></td>
<td><p>QPI</p></td>
<td><p>6.4 GT/s</p></td>
<td><p>36 PCI-E 2.0</p></td>
<td><p>rowspan="5" </p></td>
<td><p>6個3 Gbit/s接口</p></td>
<td><p>12個2.0版接口</p></td>
<td><p>rowspan="2" </p></td>
<td><p>28.6 W</p></td>
</tr>
<tr class="even">
<td><p>P55</p></td>
<td><p>Ibex Peak</p></td>
<td><p>SLGWV (B2)<br />
SLH24 (B3)</p></td>
<td><p>BD82P55 (PCH)</p></td>
<td><p>2009年9月</p></td>
<td><p>LGA 1156</p></td>
<td><p>DMI</p></td>
<td><p>2.5 GT/s</p></td>
<td><p>8 PCI-E 2.0</p></td>
<td><p>14個2.0版接口</p></td>
<td><p>4.7 W</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>H55</p></td>
<td><p>SLGZX (B3)</p></td>
<td><p>BD82H55 (PCH)</p></td>
<td><p>2009年12月</p></td>
<td><p>6 PCI-E 2.0</p></td>
<td><p>12個2.0版接口</p></td>
<td><p>rowspan="3" </p></td>
<td><p>5.2 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>H57</p></td>
<td><p>SLGZL (B3)</p></td>
<td><p>BD82H57 (PCH)</p></td>
<td><p>8 PCI-E 2.0</p></td>
<td><p>14個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Q57</p></td>
<td><p>SLGZW (B3)</p></td>
<td><p>BD82Q57 (PCH)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>P67</p></td>
<td><p>Cougar Point</p></td>
<td><p>SLH84 (B2)<br />
SLJ4C (B3)</p></td>
<td><p>BD82P67 (PCH)</p></td>
<td><p>2011年1月9日</p></td>
<td><p>LGA 1155</p></td>
<td><p>DMI 2.0</p></td>
<td><p>5 GT/s</p></td>
<td><p>rowspan="3" </p></td>
<td><p>2個6 Gbit/s接口，4個3 Gbit/s接口</p></td>
<td></td>
<td><p>6.1 W</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>H67</p></td>
<td><p>SLH82 (B2)<br />
SLJ49 (B3)</p></td>
<td><p>BD82H67 (PCH)</p></td>
<td><p>rowspan="6" </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>H61</p></td>
<td><p>SLH83 (B2)<br />
SLJ4B (B3)</p></td>
<td><p>BD82H61 (PCH)</p></td>
<td><p>6 PCI-E 2.0</p></td>
<td><p>4個3 Gbit/s接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Q67</p></td>
<td><p>SLH85 (B2)<br />
SLJ4D (B3)</p></td>
<td><p>BD82Q67 (PCH)</p></td>
<td><p>2011年2月20日</p></td>
<td><p>8 PCI-E 2.0</p></td>
<td><p>rowspan="3" </p></td>
<td><p>2個6 Gbit/s接口，4個3 Gbit/s接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>B65</p></td>
<td><p>SLH86 (B2)<br />
SLJ4A (B3)</p></td>
<td><p>BD82B65 (PCH)</p></td>
<td><p>1個6 Gbit/s接口，5個3 Gbit/s接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Q65</p></td>
<td><p>SLH99 (B2)<br />
SLJ4E (B3)</p></td>
<td><p>BD82Q65 (PCH)</p></td>
<td><p>2011年第2季</p></td>
<td><p>12個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Z68</p></td>
<td><p>SLHAT (B2)<br />
SLJ4F (B3)</p></td>
<td><p>BD82Z68 (PCH)</p></td>
<td><p>2011年5月11日</p></td>
<td><p>rowspan="5" </p></td>
<td><p>2個6 Gbit/s接口，4個3 Gbit/s接口</p></td>
<td><p>14個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Intel_X79.md" title="wikilink">X79</a></p></td>
<td><p>Patsburg</p></td>
<td><p>SLJHW(C0)<br />
SKJN7 (C1)</p></td>
<td><p>BD82X79 (PCH)</p></td>
<td><p>2011年11月14日</p></td>
<td><p>LGA 2011</p></td>
<td><p>2個6 Gbit/s接口，4個3 Gbit/s接口</p></td>
<td></td>
<td><p>7.8 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>H77</p></td>
<td><p>Panther Point</p></td>
<td><p>SLJ88(C1)</p></td>
<td><p>BD82H77 (PCH)</p></td>
<td><p>2012年4月6日</p></td>
<td><p>LGA 1155</p></td>
<td><p>4個3.0版接口，10個2.0版接口</p></td>
<td><p>rowspan="14" </p></td>
<td><p>6.7 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Z77</p></td>
<td><p>SLJC7(C1)</p></td>
<td><p>BD82Z77 (PCH)</p></td>
<td><p>2012年4月8日</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Z75</p></td>
<td><p>SLJ87(C1)</p></td>
<td><p>BD82Z75 (PCH)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Q77</p></td>
<td><p>SLJ83(C1)</p></td>
<td><p>BD82Q77 (PCH)</p></td>
<td><p>2012年5月13日</p></td>
<td><p>rowspan="3" </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Q75</p></td>
<td><p>SLJ84(C1)</p></td>
<td><p>BD82Q75 (PCH)</p></td>
<td><p>1個6 Gbit/s接口，5個3 Gbit/s接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B75</p></td>
<td><p>SLJ85(C1)</p></td>
<td><p>BD82B75 (PCH)</p></td>
<td><p>4個3.0版接口，8個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Z87</p></td>
<td><p>Lynx Point</p></td>
<td><p>SR13A(C1)<br />
SR176(C2)</p></td>
<td><p>DH82Z87 (PCH)</p></td>
<td><p>2013年6月</p></td>
<td><p>LGA 1150</p></td>
<td><p>rowspan="18" </p></td>
<td><p>6個6 Gbit/s接口</p></td>
<td><p>6個3.0版接口，8個2.0版接口</p></td>
<td><p>4.1 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>H87</p></td>
<td><p>SR139(C1)<br />
SR175(C2)</p></td>
<td><p>DH82H87 (PCH)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>H81</p></td>
<td><p>SR13B(C1)<br />
SR177(C2)</p></td>
<td><p>DH82H81 (PCH)</p></td>
<td><p>6 PCI-E 2.0</p></td>
<td><p>2個6 Gbit/s接口，4個3 Gbit/s接口</p></td>
<td><p>2個3.0版接口，8個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Q87</p></td>
<td><p>SR137(C1)<br />
SR173(C2)</p></td>
<td><p>DH82Q87 (PCH)</p></td>
<td><p>8 PCI-E 2.0</p></td>
<td><p>6個6 Gbit/s接口</p></td>
<td><p>6個3.0版接口，8個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Q85</p></td>
<td><p>SR138(C1)<br />
SR174(C2)</p></td>
<td><p>DH82Q85 (PCH)</p></td>
<td><p>4個6 Gbit/s接口，2個3 Gbit/s接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B85</p></td>
<td><p>SR13C(C1)<br />
SR178(C2)</p></td>
<td><p>DH82B85 (PCH)</p></td>
<td><p>4個3.0版接口，8個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Z97</p></td>
<td><p>Wildcat Point</p></td>
<td><p>SR1JJ</p></td>
<td><p>DH82Z97 (PCH)</p></td>
<td><p>2014年5月</p></td>
<td><p>6個6 Gbit/s接口</p></td>
<td><p>6個3.0版接口，8個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>H97</p></td>
<td><p>SR1JK</p></td>
<td><p>DH82H97 (PCH)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>X99</p></td>
<td><p>Wellsburg</p></td>
<td><p>SLKDE (B1)</p></td>
<td><p>DH82031 (PCH)</p></td>
<td><p>2014年8月29日</p></td>
<td><p>LGA 2011-v3</p></td>
<td><p>10個6 Gbit/s接口</p></td>
<td></td>
<td><p>6.5 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Z170</p></td>
<td><p>Sunrise Point</p></td>
<td><p>SR2C9</p></td>
<td><p>GL82Z170 (PCH)</p></td>
<td><p>2015年8月</p></td>
<td><p>LGA 1151</p></td>
<td><p>DMI 3.0</p></td>
<td><p>8 GT/s</p></td>
<td><p>20 PCI-E 3.0</p></td>
<td><p>6個6 Gbit/s接口</p></td>
<td><p>10個3.0版接口，14個2.0版接口</p></td>
<td><p>rowspan="5" </p></td>
<td><p>6 W</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Q170</p></td>
<td><p>SR2C5</p></td>
<td><p>GL82Q170 (PCH)</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>H170</p></td>
<td><p>SR2C8</p></td>
<td><p>GL82H170 (PCH)</p></td>
<td><p>16 PCI-E 3.0</p></td>
<td><p>4個6 Gbit/s接口</p></td>
<td><p>8個3.0版接口，14個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>B150</p></td>
<td><p>SR2C7</p></td>
<td><p>GL82B150 (PCH)</p></td>
<td><p>8 PCI-E 3.0</p></td>
<td><p>6個6 Gbit/s接口</p></td>
<td><p>6個3.0版接口，12個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>H110</p></td>
<td><p>SR2CA</p></td>
<td><p>GL82H110 (PCH)</p></td>
<td><p>DMI 2.0</p></td>
<td><p>5 GT/s</p></td>
<td><p>6 PCI-E 2.0</p></td>
<td><p>4個6 Gbit/s接口</p></td>
<td><p>4個3.0版接口，10個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Z270</p></td>
<td><p>Union Point</p></td>
<td><p>SR2WB</p></td>
<td><p>GL82Z270</p></td>
<td><p>2017年1月</p></td>
<td><p>LGA 1151</p></td>
<td><p>DMI 3.0</p></td>
<td><p>8 GT/s</p></td>
<td><p>24 PCI-E 3.0</p></td>
<td><p>6個6 Gbit/s接口</p></td>
<td><p>10個3.0版接口，14個2.0版接口</p></td>
<td><p>rowspan="5" </p></td>
<td><p>6 W</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Q270</p></td>
<td><p>SR2WE</p></td>
<td><p>GL82Q270</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>H270</p></td>
<td><p>SR2WA</p></td>
<td><p>GL82H270</p></td>
<td><p>20 PCI-E 3.0</p></td>
<td><p>6個6 Gbit/s接口</p></td>
<td><p>8個3.0版接口，14個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B250</p></td>
<td><p>SR2WC</p></td>
<td><p>GL82B250</p></td>
<td><p>12 PCI-E 3.0</p></td>
<td><p>6個6 Gbit/s接口</p></td>
<td><p>6個3.0版接口，12個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

摘要：

  - X58 (Tylersburg)
      - 上一代X48的替代品，支援[LGA
        1366接口的](../Page/LGA_1366.md "wikilink")[Nehalem與](../Page/Nehalem.md "wikilink")[Westmere處理器](../Page/Westmere.md "wikilink")，由於處理器已整合傳統北橋中的記憶體控制器，所以原來的北橋MCH更名為IOH，不同於P5x系列，由於PCI-E控制器未被整合，所以通過全新設計的一條高速[QPI介面來連接處理器中的記憶體控制器](../Page/QPI.md "wikilink")，支援的最高速度為6.4
        GT/s。與其搭配的仍為[ICH10南橋](../Page/I/O路径控制器.md "wikilink")，IOH仍通過[DMI介面來連接南橋](../Page/DMI.md "wikilink")。同時支援[ATi的](../Page/ATi.md "wikilink")[CrossFire和](../Page/CrossFire.md "wikilink")[nVIDIA的](../Page/nVIDIA.md "wikilink")[SLI](../Page/SLI.md "wikilink")，支援最大24GB的[三通道DDR](../Page/三通道.md "wikilink")3
        800/1066記憶體
  - P55 (Ibex Peak)
      - 上一代P45的替代品，支援[LGA
        1156接口的](../Page/LGA_1156.md "wikilink")[Nehalem處理器](../Page/Nehalem.md "wikilink")，由於處理器已整合過去[北橋中的所有功能](../Page/北橋.md "wikilink")，如記憶體控制器與PCI-E控制器（Lynnfield處理器）甚至顯示核心（Clarkdale處理器），傳統意義上的北橋已不存在，所以改為單晶片設計，被稱為[PCH](../Page/Platform_Controller_Hub.md "wikilink")，功能相當於過去的[南橋](../Page/南橋.md "wikilink")。類似的PCH不存在需要高連接的設備，所以仍通過[DMI介面來連接到處理器中的原北橋部分](../Page/DMI.md "wikilink")，速度為2.5
        GT/s。同時支援[ATi的](../Page/ATi.md "wikilink")[CrossFire和](../Page/CrossFire.md "wikilink")[nVIDIA的](../Page/nVIDIA.md "wikilink")[SLI](../Page/SLI.md "wikilink")，支援最大16GB的雙通道DDR3
        800/1066/1333記憶體。提供8條PCI-E 2.0通道（不包括處理器內PCI-E控制器所提供的16條PCI-E
        2.0通道），6個SATA接口（支援Frame Information Structure\[21\]），14個USB接口。
      - 加強版：
  - H55 (Ibex Peak)
      - 規格與P55基本相同，支援Identity Protection Technology與Flexible Display
        Interface技術，可以使用Clarkdale處理器中的集成顯示核心。但只提供6條PCI-E
        2.0通道，6個SATA接口（支援Frame Information
        Structure），12個USB接口。
      - 加強版：
          - H57 (Ibex Peak) - 規格與P57基本相同，支援Identity Protection
            Technology，Flexible Display InterfaceBraidwood技術。
  - Q57 (Ibex Peak)
      - 上一代Q45的替代品，規格與H57基本相同，支援Flexible Display
        Interface與Braidwood技術，並支援商用的Intel主动管理技术（AMT）6.0。

#### [Core i系列移動晶片組](../Page/Intel_Nehalem.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>晶片組</p></th>
<th><p>研發代號</p></th>
<th><p>型號</p></th>
<th><p>南橋</p></th>
<th><p>發表日期</p></th>
<th><p>處理器支援</p></th>
<th><p>介面類型</p></th>
<th><p>介面速度</p></th>
<th><p>PCI-E通道及版本</p></th>
<th><p><a href="../Page/SATA.md" title="wikilink">SATA</a></p></th>
<th><p><a href="../Page/USB.md" title="wikilink">USB</a></p></th>
<th><p>整合式繪圖顯示通道</p></th>
<th><p><a href="../Page/TDP.md" title="wikilink">TDP</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>PM55</p></td>
<td><p>Ibex Peak-M</p></td>
<td><p>SLGWN (B2)<br />
SLH23 (B3)</p></td>
<td><p>BD82PM55 (PCH)[22]</p></td>
<td><p>2009年9月</p></td>
<td><p>45 nm，32 nm</p></td>
<td><p>DMI</p></td>
<td><p>2 GB/s</p></td>
<td><p>8 PCI-E 2.0</p></td>
<td><p>6個3 Gbit/s介面</p></td>
<td><p>14個2.0版接口</p></td>
<td></td>
<td><p>3.5 W</p></td>
</tr>
<tr class="even">
<td><p>HM55</p></td>
<td><p>SLGZS (B3)</p></td>
<td><p>BD82HM55 (PCH)[23]</p></td>
<td><p>2010年1月</p></td>
<td><p>6 PCI-E 2.0</p></td>
<td><p>4個3 Gbit/s介面</p></td>
<td><p>12個2.0版接口</p></td>
<td><p>rowspan="21" </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>HM57</p></td>
<td><p>SLGZR (B3)</p></td>
<td><p>BD82HM57 (PCH)[24]</p></td>
<td><p>8 PCI-E 2.0</p></td>
<td><p>6個3 Gbit/s介面</p></td>
<td><p>14個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>QM57</p></td>
<td><p>SLGZQ (B3)</p></td>
<td><p>BD82QM57 (PCH)[25]</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>QS57</p></td>
<td><p>SLGZV (B3)</p></td>
<td><p>BD82QS57 (PCH)[26]</p></td>
<td><p>3.4 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>HM65</p></td>
<td><p>Cougar Point-M</p></td>
<td><p>SLH9D (B2)<br />
SLJ4P (B3)</p></td>
<td><p>BD82HM65 (PCH)[27]</p></td>
<td><p>2011年1月9日</p></td>
<td><p>32 nm</p></td>
<td><p>DMI 2.0</p></td>
<td><p>4 GB/s</p></td>
<td><p>2個6 Gbit/s介面，4個3 Gbit/s介面</p></td>
<td><p>12個2.0版接口</p></td>
<td><p>3.9 W</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>HM67</p></td>
<td><p>SLH9C (B2)<br />
SLJ4N (B3)</p></td>
<td><p>BD82HM67 (PCH)[28]</p></td>
<td><p>14個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>UM67</p></td>
<td><p>SLH9U(B2)<br />
SLJ4L(B3)</p></td>
<td><p>BD82UM67 (PCH)[29]</p></td>
<td><p>2011年2月20日</p></td>
<td><p>3.4 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>QM67</p></td>
<td><p>SLH9B (B2)<br />
SLJ4M (B3)</p></td>
<td><p>BD82QM67 (PCH)[30]</p></td>
<td><p>3.9 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>QS67</p></td>
<td><p>SLHAG (B2)<br />
SLJ4K (B3)</p></td>
<td><p>BD82QS67 (PCH)[31]</p></td>
<td><p>2011月20日</p></td>
<td><p>3.4 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>NM70</p></td>
<td><p>Panther Point-M</p></td>
<td><p>SLJTA(C1)</p></td>
<td><p>BD82NM70 (PCH)</p></td>
<td><p>2012年8月</p></td>
<td><p>22 nm</p></td>
<td><p>4 PCI-E 2.0</p></td>
<td><p>1個6 Gbit/s介面，4個3 Gbit/s介面</p></td>
<td><p>8個2.0版接口</p></td>
<td><p>4.1 W</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>HM70</p></td>
<td><p>SJTNV(C1)</p></td>
<td><p>BD82HM70 (PCH)[32]</p></td>
<td><p>2012年4月8日</p></td>
<td><p>8 PCI-E 2.0</p></td>
<td><p>4個3.0版接口，6個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>HM75</p></td>
<td><p>SLJ8F(C1)</p></td>
<td><p>BD82HM75 (PCH)[33]</p></td>
<td><p>2個6 Gbit/s介面，4個3 Gbit/s介面</p></td>
<td><p>12個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>HM76</p></td>
<td><p>SLJ8E(C1)</p></td>
<td><p>BD82HM76 (PCH)[34]</p></td>
<td><p>4個3.0版接口，8個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>HM77</p></td>
<td><p>SLJ8C(C1)</p></td>
<td><p>BD82HM77 (PCH)[35]</p></td>
<td><p>4個3.0版接口，10個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>UM77</p></td>
<td><p>SLJ8D(C1)</p></td>
<td><p>BD82UM77 (PCH)[36]</p></td>
<td><p>4 PCI-E 2.0</p></td>
<td><p>1個6 Gbit/s介面，3個3 Gbit/s介面</p></td>
<td><p>4個3.0版接口，6個2.0版接口</p></td>
<td><p>3.0 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>QM77</p></td>
<td><p>SLJ8A(C1)</p></td>
<td><p>BD82QM77 (PCH)[37]</p></td>
<td><p>8 PCI-E 2.0</p></td>
<td><p>2個6 Gbit/s介面，4個3 Gbit/s介面</p></td>
<td><p>4個3.0版接口，10個2.0版接口</p></td>
<td><p>4.1 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>QS77</p></td>
<td><p>SLJ8B(C1)</p></td>
<td><p>BD82QS77 (PCH)[38]</p></td>
<td><p>3.0~3.6 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>QM87</p></td>
<td><p>Lynx Point-M</p></td>
<td><p>SR13G(C1)<br />
SR17C(C2)</p></td>
<td><p>DH82QM87 (PCH)</p></td>
<td><p>2013年6月</p></td>
<td><p>4個6 Gbit/s介面，2個3 Gbit/s介面</p></td>
<td><p>6個3.0版接口，8個2.0版接口</p></td>
<td><p>2.7 W</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>HM87</p></td>
<td><p>SR13H(C1)<br />
SR17D(C2)</p></td>
<td><p>DH82HM87 (PCH)</p></td>
<td><p>6個3.0版接口，10個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>HM86</p></td>
<td><p>SR13J(C1)<br />
SR17E(C2)</p></td>
<td><p>DH82HM86 (PCH)</p></td>
<td><p>5個3.0版接口，10個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>HM97</p></td>
<td><p>Wildcat Point-M</p></td>
<td><p>SR1JN</p></td>
<td><p>DH82HM97 (PCH)</p></td>
<td><p>2014年5月</p></td>
<td><p>6個3.0版接口，10個2.0版接口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 伺服器與工作站晶片組

<http://www.intel.com/products/server/chipsets/>

## 嵌入式晶片組

<http://www.intel.com/products/embedded/chipsets.htm>

## 參考鏈接

## 相關列表

  - [ALi晶片組列表](../Page/ALi晶片組列表.md "wikilink")
  - [AMD晶片組列表](../Page/AMD晶片組列表.md "wikilink")
  - [ATi晶片組列表](../Page/ATi晶片組列表.md "wikilink")
  - [NVIDIA晶片組列表](../Page/NVIDIA晶片組列表.md "wikilink")
  - [SiS晶片組列表](../Page/SiS晶片組列表.md "wikilink")
  - [ULi晶片組列表](../Page/ULi晶片組列表.md "wikilink")
  - [VIA晶片組列表](../Page/VIA晶片組列表.md "wikilink")

## 外部連結

  - [Intel晶片組產品](http://developer.intel.com/products/chipsets/)
  - [Intel ARK](http://ark.intel.com/)

[Category:计算机硬件列表](../Category/计算机硬件列表.md "wikilink")

1.

2.  [Intel 430LX
    ("Mercury")](http://www.pcguide.com/ref/mbsys/chip/pop/g5iI430LX-c.html),
    PC Guide, accessed August 20, 2007.

3.  [Intel 430NX
    ("Neptune")](http://www.pcguide.com/ref/mbsys/chip/pop/g5iI430NX-c.html),
    PC Guide, accessed August 20, 2007.

4.  [Intel 430FX
    ("Triton")](http://www.pcguide.com/ref/mbsys/chip/pop/g5iI430FX-c.html),
    PC Guide, accessed August 20, 2007.

5.  [Summary of P5
    chipsets](http://groups.google.com/group/comp.sys.intel/msg/0a0cd1fe2b61a6ff),
    comp.sys.intel, September 1996.

6.  [Intel 430HX ("Triton
    II")](http://www.pcguide.com/ref/mbsys/chip/pop/g5iI430HX-c.html),
    PC Guide, accessed August 20, 2007.

7.
8.  [Intel 430TX](http://www.pcguide.com/ref/mbsys/chip/pop/g5iI430TX-c.html),
    PC Guide, accessed August 20, 2007.

9.  [1](http://www.tomshardware.com/reviews/intel-intros-3-series-chipsets-FSB1333-ddr3,1607-3.html)

10.

11. <http://www.intel.com/products/chipsets/975x/index.htm> Intel 975X
    Express Chipset Overview

12. [Intel G35 Express Chipset
    Overview](http://www.intel.com/products/chipsets/G35/index.htm)

13. [Intel said to have cut G35 chipset features - The Tech
    Report](http://techreport.com/discussions.x/11720)

14. Pancescu, Alexandru. [Intel's X38 Express Chipset Is
    Ready](http://news.softpedia.com/news/Intel-039-s-X38-Express-Chipset-Is-Ready-62908.shtml),
    Softpedia News, August 16, 2007.

15. <http://www.intel.com/products/chipsets/x38/index.htm> Intel X38
    Express Chipset Overview

16. <http://www.intel.com/products/desktop/chipsets/g45/g45-overview.htm>
    Intel G45 Express Chipset

17. <http://www.intel.com/Products/Desktop/Motherboards/DB43LD/DB43LD-overview.htm>
    Intel Desktop Board DB43LD

18. <http://www.intel.com/Products/Desktop/Chipsets/Q43/Q43-overview.htm>
    Intel Q43 Express Chipset

19. <http://www.intel.com/products/desktop/chipsets/q45/q45-overview.htm>
    Intel Q45 Express Chipset

20. <http://www.intel.com/products/desktop/chipsets/q45/q45-overview.htm>
    Intel Q45 Express Chipset

21.  SATA-IO: Port Multipliers

22. [Intel PM55 Express
    Chipset](http://ark.intel.com/chipset.aspx?familyID=42692)

23. [Intel HM55 Express
    Chipset](http://ark.intel.com/chipset.aspx?familyID=43181)

24. [Intel HM57 Express
    Chipset](http://ark.intel.com/chipset.aspx?familyID=43177)

25. [Intel QM57 Express
    Chipset](http://ark.intel.com/chipset.aspx?familyID=43185)

26. [Intel QM57 Express
    Chipset](http://ark.intel.com/chipset.aspx?familyID=47581)

27. [Intel HM65 Express
    Chipset](http://ark.intel.com/Product.aspx?id=52808)

28. [Intel HM67 Express
    Chipset](http://ark.intel.com/Product.aspx?id=52809)

29. [Intel UM67 Express
    Chipset](http://ark.intel.com/products/chipsets/52778)

30. [Intel QM67 Express
    Chipset](http://ark.intel.com/Product.aspx?id=52813)

31. [Intel QS67 Express
    Chipset](http://ark.intel.com/Product.aspx?id=52814)

32. [Intel HM70 Express Chipset](http://ark.intel.com/products/67419)

33. [Intel HM75 Express
    Chipset](http://ark.intel.com/products/chipsets/64346)

34. [Intel HM76 Express
    Chipset](http://ark.intel.com/products/chipsets/64343)

35. [Intel HM77 Express
    Chipset](http://ark.intel.com/products/chipsets/64337)

36. [Intel UM77 Express
    Chipset](http://ark.intel.com/products/chipsets/64340)

37. [Intel QM77 Express Chipset](http://ark.intel.com/products/64333)

38. [Intel QS77 Express Chipset](http://ark.intel.com/products/64336)