**潘佩珠**（，），原名**潘文珊**（），因避[維新帝諱](../Page/維新帝.md "wikilink")，改名潘佩珠，別名**是漢**（），號**巢南**（），是20世紀初期[越南的](../Page/越南.md "wikilink")[民族主義革命家](../Page/民族主義.md "wikilink")，也是越南的愛國志士。

## 生平

潘佩珠出生於[乂安省](../Page/乂安省.md "wikilink")[英山府](../Page/英山府.md "wikilink")[南塘縣南和社丹染村](../Page/南塘縣.md "wikilink")（今屬乂安省[南壇縣](../Page/南壇縣.md "wikilink")）一個貧苦學者的家庭，從小受其父親的影響而成長為一名愛國知識份子。當[法國](../Page/法國.md "wikilink")[殖民主義侵略越南時](../Page/殖民主義.md "wikilink")，年輕的他號召一些愛國青年組成學生軍反抗法國的入侵，但是越南終究敗於法國精良的武裝。[成泰十二年](../Page/成泰.md "wikilink")（1900年），他參加[鄉試](../Page/鄉試.md "wikilink")，考中乂安場[解元](../Page/解元.md "wikilink")\[1\]。後來他在1904年5月成立了[維新會](../Page/維新會.md "wikilink")，次年為了尋求資金和武器的緩助而來到[日本](../Page/日本.md "wikilink")。

在1905年至1908年居留日本期間，他認識了[中國維新運動領袖](../Page/中國.md "wikilink")[梁啟超](../Page/梁啟超.md "wikilink")，也認識了中國民主革命領袖[孫中山和](../Page/孫中山.md "wikilink")[章太炎](../Page/章太炎.md "wikilink")，而梁啟超和孫中山的思想也啟發其思路。之後他撰寫許多文章祕密散佈回越南，鼓吹祖國人民從法國殖民統治中解放出來。結果他被迫離開日本先到中國[廣州](../Page/廣州.md "wikilink")，再轉往到暹羅（[泰國](../Page/泰國.md "wikilink")），1912年1月又來到廣州。後來他在當地見到[辛亥革命成功](../Page/辛亥革命.md "wikilink")，於是改組越南維新會而成為[越南光復會](../Page/越南光復會.md "wikilink")，這是模仿孫中山的革命組織所重新創建的團體。

1925年（[啟定十年](../Page/啟定.md "wikilink")）6月30日，潘佩珠被叛徒出賣，在[上海遭到法國特務祕密拘捕](../Page/上海.md "wikilink")，被控叛逆罪，並在[順化終身軟禁直到逝世](../Page/順化.md "wikilink")。

## 著作

  - 《[越南亡國史](../Page/越南亡國史.md "wikilink")》
  - 《[後陳逸史](../Page/後陳逸史.md "wikilink")》
  - 《[琉球血淚新書](../Page/琉球血淚新書.md "wikilink")》
  - 《[咀菜禪師](../Page/咀菜禪師.md "wikilink")》

## 註釋

## 參考文獻

  - .

  - .

  - .

  - .

  - .

  - .

  - .

  - .

  - .

## 外部連結

  - [Phan Boi Chau Youth Group](http://www.pbctoronto.org/)
  - [Giáo sư sử học Vĩnh Sính viết về Phan Chu Trinh và Phan Bội
    Châu](http://www.erct.com/2-ThoVan/VinhSinh/13-Thu_nhin_lai-1.htm)
  - [Giáo sư sử học Vĩnh Sính viết về Phan Chu Trinh và Phan Bội Châu
    nhân 100 năm Phong Trào Duy
    Tân](http://213.251.176.152:8080/diendan/phe-binh-nghien-cuu/100-phong-trao-duy-tan/)
  - [Phan Bội
    Châu](http://www.nhandan.com.vn/tinbai/?top=43&sub=126&article=21685),
    từ [báo Nhân Dân](../Page/人民报_\(越南\).md "wikilink")
  - [Học lại Phan Bội Châu và Phong trào Đông
    Du](http://www.bbc.co.uk/vietnamese/programmes/story/2005/07/interview31yr2005phaboichau.shtml),
    từ BBC Việt ngữ

[潘佩珠](../Category/潘佩珠.md "wikilink") [P](../Category/越南作家.md "wikilink")
[P](../Category/越南革命家.md "wikilink")
[P](../Category/越南民族主義者.md "wikilink")
[P](../Category/乂安省人.md "wikilink")
[B](../Category/潘姓.md "wikilink")
[Category:成泰十二年庚子科乂安場舉人](../Category/成泰十二年庚子科乂安場舉人.md "wikilink")
[Category:阮朝乂安場解元](../Category/阮朝乂安場解元.md "wikilink")

1.  [成泰十二年乂安場《鄉試文選》](http://lib.nomfoundation.org/collection/1/volume/1141/page/2)