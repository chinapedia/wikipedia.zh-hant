[thumb](../Page/image:Man_and_woman_undergoing_public_exposure_for_adultery_in_Japan-J._M._W._Silver.jpg.md "wikilink")，因[通姦被示眾的男女](../Page/通姦.md "wikilink")（引自，1867年出版）\]\]

**外遇**，也稱作**婚外情**、**出軌**、**第三者插足**（此第三者俗稱“小三”），是指有[婚姻關係的其中一人](../Page/婚姻.md "wikilink")，與[配偶以外的人發生超出友誼的](../Page/配偶.md "wikilink")[愛情關係](../Page/愛情.md "wikilink")（不論是否有[性行為](../Page/人类性行为.md "wikilink")）。外遇的定義依不同研究領域有些許不同，在社會學辭典中，外遇即有發生性交（[通姦](../Page/通姦.md "wikilink")）行為；若僅有「思想或行為上的不貞」而無實際的性接觸，可稱之為**精神外遇**。法律上，有了[性關係被視為是外遇的必要因素](../Page/性關係.md "wikilink")，此性關係是指已婚者與非配偶發生自願性的性行為。

## 人類學

在[人類學的觀點中](../Page/人類學.md "wikilink")，外遇是延續該種族的策略之一。[美國人類學者海倫](../Page/美國.md "wikilink")·費雪對42本民族誌進行查閱，發現其中所提的民族皆有婚外情的情形。1940年代，人類學者莫達克（Murdock）對古今148個社會調查的結果中指出，有120個社會視外遇為禁忌，19個社會設有外遇條件，5個社會完全容許婚外情。

## 外遇的類型與因素

[Jules_Arsène_Garnier_-_Le_supplice_des_adultères.jpg](https://zh.wikipedia.org/wiki/File:Jules_Arsène_Garnier_-_Le_supplice_des_adultères.jpg "fig:Jules_Arsène_Garnier_-_Le_supplice_des_adultères.jpg")
外遇可分為下列幾種情形：傳統型外遇、拈花惹草型、保護型外遇、情境型外遇、舊情復燃型外遇、感性型外遇。

常見的外遇因素包含了夫妻間溝通不良、夫妻間角色協調不當、處理問題技巧不足、性格不合、夫妻觀念與認知衝突、性生活不協調、個人人格因素。

人類外遇的原因主要包括與舊情人復合、[唯美主義的外遇](../Page/唯美主義.md "wikilink")、沉淪式的外遇、缺乏關愛的外遇、婚外性行為、[炮友的吸引力](../Page/炮友.md "wikilink")、[報復性外遇](../Page/報復.md "wikilink")（因配偶的外遇行為）、[性交易](../Page/性交易.md "wikilink")、夫妻性生活不協調、對另一半不滿、追求刺激。

## 兩性外遇

傳統[父權社會對於兩性外遇有不同的看法](../Page/父權.md "wikilink")，對於[男性發生外遇行為的形容詞包括風流倜儻](../Page/男性.md "wikilink")、齊人之福、拈花惹草，女性應稍加「容忍」；形容[女性外遇行為的形容詞有紅杏出牆](../Page/女性.md "wikilink")、水性楊花、不守婦道、討契兄（讨情夫，閩南語發音），導致男性[戴綠帽](../Page/戴綠帽.md "wikilink")。

現代提倡男女平等和自由戀愛的思想下，兩性外遇同樣受到譴責，認為是破壞家庭幸福、對配偶不忠的行為，但當出現外遇時，現代社會鼓勵男女雙方在平等的態度盡快去處理，選擇緩解，或結束雙方的感情關係。

童年经历过父母婚外情及[離婚的人往往会对结婚生子有着相当深的恐惧和厌恶](../Page/離婚.md "wikilink")。

### 女性

  - **配偶外遇**：面對配偶外遇的反應，通常是伴隨激烈的情緒波動。
  - **女性外遇**：兩性不平等的社會機制下，在婚約中反映了兩性不公平，[離婚一般認為女性可獲更多保障](../Page/離婚.md "wikilink")，導致因此女性外遇的意義，不只在情感和性慾的滿足，有些是對丈夫外遇的報復，在外尋找無感情負擔的[炮友](../Page/炮友.md "wikilink")。中國大陆、臺灣等地近年亦有女性[包養年輕男性為](../Page/包養.md "wikilink")[情夫的現象](../Page/情夫.md "wikilink")，即與外遇保持固定[性關係](../Page/性關係.md "wikilink")，並提供經濟支援的行為，稱為「養小狼狗」、「包二爺」、「貼小白臉」。

### 男性

  - **配偶外遇**：中国传统上将妻子有婚外情的男性形容为“戴绿帽”，男性會認為配偶對其不忠，顯然是對其的羞辱。
  - **男性外遇**：以東亞傳統婚姻制度為例，為[一夫一妻制](../Page/一夫一妻制.md "wikilink")，雖然在不少朝代律例中規定，男子娶第二位[正室是不合法](../Page/正室.md "wikilink")，但存在一夫一妻多妾制，亦有些時代容許男性有[平妻](../Page/平妻.md "wikilink")。在[一夫一妻制社會中](../Page/一夫一妻制.md "wikilink")，男性也會有在外尋找無感情負擔的[炮友](../Page/炮友.md "wikilink")、或包養[情婦的行為](../Page/情婦.md "wikilink")。

## 相關法律

[中華民國法律中](../Page/中華民國法律.md "wikilink")，[刑法](../Page/刑法.md "wikilink")239條規定[通姦罪是](../Page/通姦罪.md "wikilink")：『有配偶而與人通姦者，處一年以下有期徒刑。其相姦者，亦同』，通姦罪屬告訴乃論罪，通姦的連續犯會加重其刑二分之一。通姦罪告訴期間是六個月，若超過期限提出告訴，其罪名無法成立。

## 相關作品

  - 《[查泰萊夫人的情人](../Page/查泰萊夫人的情人.md "wikilink")》：小說，[D·H·勞倫斯著](../Page/D·H·勞倫斯.md "wikilink")。
  - 《[失樂園](../Page/失樂園_\(小說\).md "wikilink")》：小說，[渡邊淳一著](../Page/渡邊淳一.md "wikilink")，ISBN：9867413407
  - 《[麥迪遜之橋](../Page/麥迪遜之橋.md "wikilink")》（又名《[廊桥遗梦](../Page/廊桥遗梦.md "wikilink")》）：小說。電影（[-{zh-hans:克林特·伊斯特伍德;zh-hk:奇連·伊士活;zh-tw:克林·伊斯威特;}-](../Page/克林·伊斯威特.md "wikilink")，[-{zh-hans:梅丽·史翠普;zh-hk:梅麗·史翠普;zh-tw:梅莉·史翠普;}-主演](../Page/梅莉·史翠普.md "wikilink")）
  - 《愛情決勝點（Match point）》：2005年電影，[伍迪·艾伦導演](../Page/伍迪·艾倫.md "wikilink")。
  - 《愛人》：[日文歌](../Page/日本流行音樂.md "wikilink")，[鄧麗君演唱](../Page/鄧麗君.md "wikilink")
  - 《神化》：[粵語歌](../Page/粵語流行音樂.md "wikilink")，[鄭秀文演唱](../Page/鄭秀文.md "wikilink")
  - 《烟霞》：粵語歌，[容祖儿演唱](../Page/容祖儿.md "wikilink")
  - 《吴哥窟》：粵語歌，[吴雨霏演唱](../Page/吴雨霏.md "wikilink")
  - 《告白》：粵語歌，吴雨霏演唱
  - 《无人之境》：粵語歌，[陈奕迅演唱](../Page/陈奕迅.md "wikilink")
  - 《漩涡》，粵語歌，[彭羚](../Page/彭羚.md "wikilink")、[黄耀明演唱](../Page/黄耀明.md "wikilink")
  - 《罪人》，粵語歌，[李克勤演唱](../Page/李克勤.md "wikilink")
  - 《逢星期四》，粵語歌，[应昌佑演唱](../Page/应昌佑.md "wikilink")
  - 《花落无声》，粵語歌，[杨斌演唱](../Page/杨斌.md "wikilink")
  - 《一拍两散》，粵語歌，容祖儿演唱
  - 《两个只能爱一个》，粵語歌，[叶文辉演唱](../Page/叶文辉.md "wikilink")
  - 《我估不到》，粵語歌，[刘美君演唱](../Page/刘美君.md "wikilink")
  - 《短罪》，粵語歌，[乐瞳演唱](../Page/乐瞳.md "wikilink")
  - 《三位一体》，粵語歌，吴雨霏、[方力申](../Page/方力申.md "wikilink")、[廖梦乐演唱](../Page/廖梦乐.md "wikilink")
  - 《我只能跳舞》：粵語歌，[杨千嬅演唱](../Page/杨千嬅.md "wikilink")
  - 《三角關係完全手冊》：電子書，陳靜怡心理師＆失戀花園。

## 參見

  - [緋聞](../Page/緋聞.md "wikilink")
  - [炮友](../Page/炮友.md "wikilink")
  - [劈腿族](../Page/劈腿族.md "wikilink")
  - [通姦](../Page/通姦.md "wikilink")
  - [绿帽子](../Page/绿帽子.md "wikilink")
  - [第三者 (愛情)](../Page/第三者_\(愛情\).md "wikilink")
  - [情婦](../Page/情婦.md "wikilink") / [情夫](../Page/情夫.md "wikilink")
  - [包養](../Page/包養.md "wikilink")

<!-- end list -->

  - [一夫一妻制](../Page/一夫一妻制.md "wikilink")
  - [一夫多妻制](../Page/一夫多妻制.md "wikilink")
  - [一妻多夫制](../Page/一妻多夫制.md "wikilink")

<!-- end list -->

  - [離婚](../Page/離婚.md "wikilink")
  - [浸豬籠](../Page/浸豬籠.md "wikilink")

## 註腳

## 參考資料

  -
  -
  - [當代台灣女性外遇發展之社會學研究](https://web.archive.org/web/20070927044712/http://192.192.148.77/ETD-db/ETD-search/getfile?URN=etd-1208104-155655-246&filename=etd-1208104-155655-246.pdf)

{{-}}  [category:性學](../Page/category:性學.md "wikilink")
[category:婚姻](../Page/category:婚姻.md "wikilink")

[Category:外遇](../Category/外遇.md "wikilink")