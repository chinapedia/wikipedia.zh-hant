[The_Graffitied_SharpCorporation_MZ-80_BASIC_Manuals_Document_copy.jpg](https://zh.wikipedia.org/wiki/File:The_Graffitied_SharpCorporation_MZ-80_BASIC_Manuals_Document_copy.jpg "fig:The_Graffitied_SharpCorporation_MZ-80_BASIC_Manuals_Document_copy.jpg")
[1987](../Page/1987.md "wikilink")
**MZ-80 BASIC Manual**第110页。
*这起源MML（开放源码）*。\]\] **Music Macro
Language**是[電腦上所使用表示](../Page/電腦.md "wikilink")[樂譜的語言之一](../Page/樂譜.md "wikilink")，它以[循序的方式來表示樂譜](../Page/循序.md "wikilink")。它有可能會和以[XML技術發展的](../Page/XML.md "wikilink")[Music
Markup
Language搞混](../Page/Music_Markup_Language.md "wikilink")，因兩者縮寫皆為[MML](../Page/MML.md "wikilink")，且[HTML與XML都有](../Page/HTML.md "wikilink")**ML**做結尾。在[線上遊戲](../Page/線上遊戲.md "wikilink")[瑪奇中也支援了簡易的MML格式樂譜](../Page/瑪奇.md "wikilink")。

## 表示法

以下將敘述MML的表示方法。

|           |                                                                                                                                                                                                                           |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 代碼        | 說明                                                                                                                                                                                                                        |
| `CDEFGAB` | [音符](../Page/音符.md "wikilink")。後面跟隨著「`#`」或是「`+`」作為[升記號](../Page/升記號.md "wikilink")，`-`作為[降記號](../Page/降記號.md "wikilink")。後面跟隨著數字或點表示音符時值，如「`A+2.`」表示升A[附點](../Page/附點音符.md "wikilink")[二分音符](../Page/二分音符.md "wikilink")。 |
| `R`       | [休止符](../Page/休止符.md "wikilink")。與音符表示法相同，後面跟隨的數字或點表示音符長度。                                                                                                                                                                |
| `O`       | 指定[八度](../Page/八度.md "wikilink")。後面跟隨著數字來指定樂器演奏哪個[八度](../Page/八度.md "wikilink")。                                                                                                                                          |
| `>`、`<`   | 控制樂譜高八度或低八度。使用「`>`」表示樂譜之後為高八度，用「`<`」表示樂譜之後為低八度。                                                                                                                                                                           |
| `L`       | 指定[音符時值](../Page/音符時值.md "wikilink")。使用此方式指定如果「`A`」～「`G`」或是「`R`」之後沒有接數字的話代表的音符時值為何。常見的預設值為[四分音符](../Page/四分音符.md "wikilink")。如「`L4CCCC`」表示四個C的四分音符。                                                                       |
| `V`       | 指定音量大小。後面跟隨的數字可指定之後演奏樂器的音量大小。                                                                                                                                                                                             |
| `T`       | 指定樂器的[速度](../Page/速度_\(音樂\).md "wikilink")。例如「`T120`」表示以120BPM來演奏。                                                                                                                                                        |
|           |                                                                                                                                                                                                                           |

## 相關條目

  - [瑪奇](../Page/瑪奇.md "wikilink")
  - [MIDI](../Page/MIDI.md "wikilink")

## 外部連結

  - [MML奇幻音樂廳](http://mabinogi.fws.tw/ac_comproser.php)新瑪奇網路遊戲適用範本音樂

\*[3ML Editor](http://3ml.jp/)[2](http://aloe.or.tl/tools/3mle)

  - [まきまびしーく](http://www.geocities.jp/makimabi)
  - [SPICE](http://gorry.haun.org/spice/index.html)

[Category:樂譜檔案格式](../Category/樂譜檔案格式.md "wikilink")
[Category:電子音樂軟體](../Category/電子音樂軟體.md "wikilink")
[Category:遊戲音樂](../Category/遊戲音樂.md "wikilink")
[Category:自由音訊軟體](../Category/自由音訊軟體.md "wikilink")