[2014_Gini_Index_World_Map,_income_inequality_distribution_by_country_per_World_Bank.svg](https://zh.wikipedia.org/wiki/File:2014_Gini_Index_World_Map,_income_inequality_distribution_by_country_per_World_Bank.svg "fig:2014_Gini_Index_World_Map,_income_inequality_distribution_by_country_per_World_Bank.svg")
|language=en |year=2014
|access-date=}}</ref>基尼系数越小收入分配越平均，基尼系数越大收入分配越不平均。\]\]

**基尼系數**（），是20世纪初[意大利学者](../Page/意大利.md "wikilink")[科拉多·基尼](../Page/科拉多·基尼.md "wikilink")(另一說[赫希曼](../Page/赫希曼.md "wikilink"))根据[劳伦茨曲线所定義的判断年](../Page/劳伦茨曲线.md "wikilink")[收入分配公平程度的指标](../Page/收入分配.md "wikilink")\[1\]，是[比例數值](../Page/比例.md "wikilink")，在0和1之間。**基尼指數**（Gini
index）是基尼系數乘100倍作[百分比表示](../Page/百分比.md "wikilink")。在民众收入中，[基尼系數最大为](../Page/百分比.md "wikilink")“1”，最小為“0”。前者表示居民之间的年收入分配绝对不平均（即该年所有收入都集中在一個人手裏，其餘的國民沒有收入），而后者则表示居民之间的该年收入分配绝对平均，即人与人之间收入绝对平等，基尼系數的实际数值只能介于这两种极端情况，即0～1之间。基尼系數越小，年收入分配越平均；基尼系数越大，年收入分配越不平均。要注意基尼系数只计算某一时段，如一年的收入，不计算已有财产，因此它不能反映国民的总积累财富分配情况。

## 定義

[thumb|400px
|图中橫軸為人口累計百分比，縱軸為該部分人的收入佔人口總收入的百分比，三條色線各表示不同情況下後者和前者的比例。表示人口收入分配處於绝对平均状态，表示绝对不平均（即所有收入由一人獨佔），則表示实际情况。紅線和綠線之間的面積越小，則收入分配越平等。](../Page/File:lorenz-curve1.png.md "wikilink")

设右圖中的[**实际收入分配曲线**](../Page/勞倫茨曲線.md "wikilink")（）和**收入分配绝对平等线**（）之间的面积为A，**实际收入分配曲线**（）和**收入分配绝对不平等线**（）之间的面积为B，則表示收入與人口之間的比例的**基尼系數**為\(\tfrac{A}{A + B}\)\[2\]。

如果A为零，即基尼系数为0，表示收入分配**完全平等**（紅線和綠線重疊）；如果B为零，则係数为1，收入分配**绝对不平等**（紅線和藍線重疊）。该系数可在0和1之间取任何值。收入分配越趋向平等，[劳伦茨曲线的弧度越小](../Page/劳伦茨曲线.md "wikilink")（斜度越傾向45度），基尼系数也越小；反之，收入分配越趋向不平等，劳伦茨曲线的弧度越大，那么基尼系数也越大。

基尼系数的调节需要国家通过[财政政策进行](../Page/财政政策.md "wikilink")[国民收入的二次分配](../Page/国民收入.md "wikilink")，例如对民众的财政[公共服务支出和](../Page/公共服务.md "wikilink")[税收等](../Page/税收.md "wikilink")，从而讓收入均等化，令基尼系数縮小。

## 基尼系數的区段划分

## 各国基尼系數

歐洲主要发达国家的基尼指数在0.24到0.36之间，美国较高，2007年为0.45\[3\]，2013年为0.49。Janet Gornick
教授2013年的比較圖顯示，美国與主要发达国家的基尼指数用稅前計算差距不大，以稅後計算則偏高。\[4\]

在收入差距方面，據[美國人口調查局提供的數據](../Page/美國人口調查局.md "wikilink")，1973年，收入最高20%的家庭收入佔美國總收入的44%；2002年佔50%；而到2012年，這一比例已經增至51%。對收入最低20%的家庭而言，他們的收入佔美國總收入的比例從1973年的4.2%，2002年的3.5%，降至2012年的3.2%。\[5\]\[6\]。

而在財富占有的差距方面，據美國[加州大學伯克利分校的調查研究](../Page/加州大學伯克利分校.md "wikilink")，美國前10%的富人大約占有80%的社會總財富，而前1%的富人占有40%的財富，前0.1%的富人占有20%的財富，前0.01%仍然占有10%的財富。\[7\]

目前全球基尼系數最高的地方是[非洲的](../Page/非洲.md "wikilink")[納米比亞](../Page/納米比亞.md "wikilink")。2001年以後[香港達到](../Page/香港.md "wikilink")0.525，2006年高達0.533，2012年更高達0.537\[8\]，香港成為已發达經濟体中貧富懸殊最嚴重的地區；即使把發展中經濟體包括在內，[香港的貧富懸殊也十分嚴重](../Page/香港.md "wikilink")，僅次於[薩爾瓦多](../Page/薩爾瓦多.md "wikilink")、[哥倫比亞](../Page/哥倫比亞.md "wikilink")、[智利](../Page/智利.md "wikilink")、[危地馬拉](../Page/危地馬拉.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[南非以及一系列](../Page/南非.md "wikilink")[非洲經濟體而排名倒數](../Page/非洲.md "wikilink")18位\[9\]，[中華民國](../Page/中華民國.md "wikilink")2010年官方的基尼系數為0.342\[10\]2013年學者計算為0.36\[11\]；[日本](../Page/日本.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、西歐、東歐等經濟體的收入基尼係數也低於0.4。

根據北京大學社會科學院的報告，2012年中國家庭凈財產的基尼系數達到0.73，頂端1%的家庭占有全國1/3以上的財產。\[12\][丹麥](../Page/丹麥.md "wikilink")，[瑞士等國則紛紛超過](../Page/瑞士.md "wikilink")0.8，在美國這一數字更是高達0.84。\[13\]

2013年1月18日，[中华人民共和国](../Page/中华人民共和国.md "wikilink")[国家统计局一次性公布了自](../Page/国家统计局.md "wikilink")2003年以来十年的全国基尼系数。中國统计局局长马建堂称，按照國際新的统计口径，中國居民收入的基尼系数，2003年是0.479，2004年是0.473，2005年是0.485，2006年是0.487，2007年是0.484，2008年是0.491，2009年是0.490，2010年是0.481，2011年是0.477，2012年是0.474。数据显示自2008年起，中国基尼系数在逐年下降。\[14\]。有媒体文章根据民间统计结果质疑国家统计局的结果\[15\]。2014年[密西根大學](../Page/密西根大學.md "wikilink")[谢宇教授根據中國的六份調查](../Page/谢宇.md "wikilink")，估算中國2005年後基尼系数為0.53–0.55，並指出差距主要來自沿海與內陸差距以及城鄉差距。\[16\]\[17\]

## 基尼系數的不足

[2008_Top1percentUSA.png](https://zh.wikipedia.org/wiki/File:2008_Top1percentUSA.png "fig:2008_Top1percentUSA.png")

1.  没有显示出来在**哪里**存在[分配不公](../Page/分配不公.md "wikilink")。例如同樣的基尼系數下，如果青年平均收入比中老人低太多，社會則會出現大問題——就算是年輕人的父母能夠金援他們也是一樣，年輕人接受父母金援時會認為，他們沒有辦法在未來金援自己的子女，因此容易拒絕生育。
2.  國際間並無制定基尼系數的準則，一些問題（如應否除稅項，應否剔除公共援助受益者，應否剔除非本地居民，或應否加入政府的福利）並沒有一致性，以至缺乏比較的準則。
3.  基尼系数一般是按年收入来算的，这样对年收入波动很大的地区（如商业投资为主导）的估计会显著高于年收入波动小的地区（如公务员为主导的地区）。如果年收入波动很大，则基尼系数会很高，但多年份积累积累下来的收入差距并没有基尼系数显示得那么大。這也反映了基尼系数高的另一个原因可能是收入波动高，社会阶层流动快。

## 参见

  - [各国收入均衡列表](../Page/各国收入均衡列表.md "wikilink")
  - [戴尔指数](../Page/戴尔指数.md "wikilink")
  - [恩格尔系数](../Page/恩格尔系数.md "wikilink")
  - [人類發展指數列表](../Page/人類發展指數列表.md "wikilink")
  - [M型社會](../Page/M型社會.md "wikilink")
  - [在職貧窮](../Page/在職貧窮.md "wikilink")
  - [盧德運動](../Page/盧德運動.md "wikilink")
  - [中国贫富差距](../Page/中国贫富差距.md "wikilink")

## 参考文献

## 外部連結

  - [主要国家基尼系数](https://www.cia.gov/library/publications/the-world-factbook/rankorder/2172rank.html)
    - [美国中情局](../Page/中央情报局.md "wikilink")

  - \- [香港特区政府經濟顧問](../Page/香港特別行政區政府.md "wikilink") 撰文

  - [線上計算器](http://www.poorcity.richcity.org/cgi-bin/inequality.cgi)

  - [電子試算表](../Page/電子試算表.md "wikilink")：

  -
{{-}}

[Category:发展经济学](../Category/发展经济学.md "wikilink")
[Category:经济指数](../Category/经济指数.md "wikilink")

1.

2.

3.
4.

5.  [美國貧富差距日趨增大 20%人口占有50%財富](http://news.xinhuanet.com/fortune/2004-08/23/content_1862980.htm).[新华网](../Page/新华网.md "wikilink")

6.

7.  [1](http://gabriel-zucman.eu/files/SaezZucman2014Slides.pdf).\[Wealth
    Inequality in the United\]

8.  [香港坚尼系数0.537
    贫富差距令人震惊](http://www.chinese.rfi.fr/%E7%A4%BE%E4%BC%9A/20120619-%E9%A6%99%E6%B8%AF%E5%9D%9A%E5%B0%BC%E7%B3%BB%E6%95%B00537-%E8%B4%AB%E5%AF%8C%E5%B7%AE%E8%B7%9D%E4%BB%A4%E4%BA%BA%E9%9C%87%E6%83%8A).[法广](../Page/法广.md "wikilink")

9.  [CIA World
    Factbook](https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html)

10. [行政院主計處國情統計通報](http://www.stat.gov.tw/public/Data/0101218104471.pdf)

11.
12.

13. [THE LEVEL AND DISTRIBUTION OF GLOBAL HOUSEHOLD
    WEALTH](http://piketty.pse.ens.fr/fichiers/Daviesetal2009.pdf),
    [美國全國經濟研究所](../Page/美國全國經濟研究所.md "wikilink")

14.

15.

16.

17.