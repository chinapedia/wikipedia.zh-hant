**印度洋**（），位于[亚洲](../Page/亚洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[大洋洲和](../Page/大洋洲.md "wikilink")[南极洲之间](../Page/南极洲.md "wikilink")\[1\]，[印度位在印度洋北部的中央位置](../Page/印度.md "wikilink")，這也是印度洋名稱的由來\[2\]\[3\]，印度洋大部分在[南半球](../Page/南半球.md "wikilink")。總面积7491万[平方公里](../Page/平方公里.md "wikilink")，约占[世界](../Page/世界.md "wikilink")[海洋总](../Page/海洋.md "wikilink")[面积的](../Page/面积.md "wikilink")21.1%\[4\]，是世界第三大洋\[5\]。印度洋的範圍北至[印度次大陸及](../Page/印度次大陸.md "wikilink")[阿拉伯半島](../Page/阿拉伯半島.md "wikilink")（[南亞及](../Page/南亞.md "wikilink")[西亞](../Page/西亞.md "wikilink")）；西達[東非](../Page/東非.md "wikilink")；東側則以[印度尼西亞](../Page/印度尼西亞.md "wikilink")、[巽他群島及](../Page/巽他群島.md "wikilink")[澳大利亞為界](../Page/澳大利亞.md "wikilink")；南迄[南冰洋](../Page/南冰洋.md "wikilink")（也有定義至[南極洲](../Page/南極洲.md "wikilink")）。印度洋西面于开始于[厄加勒斯角的东经](../Page/厄加勒斯角.md "wikilink")20°处与大西洋相接，东面于东经146°55'处与太平洋相接。\[6\]印度洋的最北边大概为[波斯湾内北纬](../Page/波斯湾.md "wikilink")30°处。印度洋的宽度有将近（从非洲南部尖端到澳大利亚之间），它的总面积大约为\[7\]，包括[红海和](../Page/红海.md "wikilink")[波斯湾](../Page/波斯湾.md "wikilink")。印度洋的水量大约为\[8\]。

## 地理

[Indian_Ocean_bathymetry_srtm.png](https://zh.wikipedia.org/wiki/File:Indian_Ocean_bathymetry_srtm.png "fig:Indian_Ocean_bathymetry_srtm.png")\]\]
非洲，印度和南极的地壳[板块在印度洋的](../Page/板块构造论.md "wikilink")处汇合。

印度洋的[大陸棚較為狹窄](../Page/大陸棚.md "wikilink")，扣除澳大利亞西海岸較寬廣的大陸棚區（平均寬度1000公里），其他地區的大陸棚平均寬度為200公里。平均深度为3,890[公尺](../Page/公尺.md "wikilink")，最大深度为[蒂阿曼蒂那海溝](../Page/蒂阿曼蒂那海溝.md "wikilink")（Diamantina
Trench），达8,047公尺；而[爪哇海溝](../Page/爪哇海溝.md "wikilink")（深度7,258–7,725公尺）曾長期被認為是印度洋最深的[海溝](../Page/海溝.md "wikilink")\[9\]。

印度洋上的一些主要咽喉要道有[曼德海峡](../Page/曼德海峡.md "wikilink")、[霍爾木茲海峽](../Page/霍爾木茲海峽.md "wikilink")、[龙目海峡](../Page/龙目海峡.md "wikilink")、[马六甲海峡和](../Page/马六甲海峡.md "wikilink")[保克海峡](../Page/保克海峡.md "wikilink")。

### 位置与范围

印度洋西南以通过[南非](../Page/南非.md "wikilink")[厄加勒斯角的经线同](../Page/厄加勒斯角.md "wikilink")[大西洋分界](../Page/大西洋.md "wikilink")，东南以通过[塔斯马尼亚岛东南角至南极大陆的经线与](../Page/塔斯马尼亚岛.md "wikilink")[太平洋联结](../Page/太平洋.md "wikilink")。印度洋的轮廓为北部为陆地封闭，南面則以南緯60度為界，與[南冰洋相連](../Page/南冰洋.md "wikilink")。

印度洋的主要属海和海湾是[红海](../Page/红海.md "wikilink")、[阿拉伯海](../Page/阿拉伯海.md "wikilink")、[亚丁湾](../Page/亚丁湾.md "wikilink")、[波斯湾](../Page/波斯湾.md "wikilink")、[阿曼湾](../Page/阿曼湾.md "wikilink")、[孟加拉湾](../Page/孟加拉湾.md "wikilink")、[安达曼海](../Page/安达曼海.md "wikilink")、[阿拉弗拉海](../Page/阿拉弗拉海.md "wikilink")、[帝汶海](../Page/帝汶海.md "wikilink")、[卡奔塔利亚湾](../Page/卡奔塔利亚湾.md "wikilink")、[大澳大利亚湾](../Page/大澳大利亚湾.md "wikilink")、[莫桑比克海峡等等](../Page/莫桑比克海峡.md "wikilink")。

印度洋有很多岛屿，其中大部分是[大陆岛](../Page/大陆岛.md "wikilink")，如[马达加斯加岛](../Page/马达加斯加岛.md "wikilink")、[斯里兰卡岛](../Page/斯里兰卡岛.md "wikilink")、[安达曼群岛](../Page/安达曼群岛.md "wikilink")、[尼科巴群岛](../Page/尼科巴群岛.md "wikilink")、[明打威群岛等](../Page/明打威群岛.md "wikilink")。[留尼汪岛](../Page/留尼汪岛.md "wikilink")、[科摩罗群岛](../Page/科摩罗群岛.md "wikilink")、[阿姆斯特丹岛](../Page/阿姆斯特丹岛.md "wikilink")、[克罗泽群岛](../Page/克罗泽群岛.md "wikilink")、[凯尔盖朗群岛为](../Page/凯尔盖朗群岛.md "wikilink")[火山岛](../Page/火山岛.md "wikilink")。[拉克沙群岛](../Page/拉克沙群岛.md "wikilink")、[马尔代夫群岛](../Page/马尔代夫群岛.md "wikilink")、[查戈斯群岛](../Page/查戈斯群岛.md "wikilink")，以及爪哇西南的[圣诞岛](../Page/圣诞岛.md "wikilink")、[科科斯群岛是](../Page/科科斯（基林）群岛.md "wikilink")[珊瑚岛](../Page/珊瑚岛.md "wikilink")。

### 水文学

流入印度洋的大河有[赞比西河](../Page/赞比西河.md "wikilink")，[阿拉伯河](../Page/阿拉伯河.md "wikilink")，[印度河](../Page/印度河.md "wikilink")，[讷尔默达河](../Page/讷尔默达河.md "wikilink")，[恒河](../Page/恒河.md "wikilink")，[布拉马普特拉河](../Page/布拉马普特拉河.md "wikilink")，[朱巴河和](../Page/朱巴河.md "wikilink")[伊洛瓦底江等等](../Page/伊洛瓦底江.md "wikilink")。印度洋的[洋流主要被](../Page/洋流.md "wikilink")[季风控制](../Page/季风.md "wikilink")。它有两个较大的环流构成了主要的流型，一个是在北半球的顺时针环流；另一个是赤道南部的逆时针环流。但是在冬季的季风期间，北方的环流会逆转方向。

## 气候

印度洋大部分位于[热带](../Page/热带.md "wikilink")，经常有[熱帶氣旋产生](../Page/熱帶氣旋.md "wikilink")。於[十月至隔年](../Page/十月.md "wikilink")[四月有強烈東北風吹拂](../Page/四月.md "wikilink")，[五月至](../Page/五月.md "wikilink")[十月則有盛行南風及西風](../Page/十月.md "wikilink")。[阿拉伯海上強烈的](../Page/阿拉伯海.md "wikilink")[季風為印度次大陸帶來了降雨](../Page/季風.md "wikilink")，[南半球的風大多溫和](../Page/南半球.md "wikilink")，但[模里西斯附近的暴風雨卻十分劇烈](../Page/模里西斯.md "wikilink")。\[10\]

## 經濟

### 资源

印度洋[石油极为丰富](../Page/石油.md "wikilink")，[波斯湾](../Page/波斯湾.md "wikilink")、[红海](../Page/红海.md "wikilink")、[阿拉伯海](../Page/阿拉伯海.md "wikilink")、[孟加拉湾](../Page/孟加拉湾.md "wikilink")、[苏门答腊岛与](../Page/苏门答腊岛.md "wikilink")[澳大利亚西部沿海都蕴藏有海底石油](../Page/澳大利亚.md "wikilink")，於[沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[印度及](../Page/印度.md "wikilink")[澳大利亞西部的近海地區開採](../Page/澳大利亞.md "wikilink")[碳氫化合物](../Page/碳氫化合物.md "wikilink")，據估計，世界上40%的海上開採石油來自印度洋\[11\]。[波斯湾是世界海底](../Page/波斯湾.md "wikilink")[石油最大的产区](../Page/石油.md "wikilink")。

海水上层浮游生物很丰富，盛产[飞鱼](../Page/飞鱼.md "wikilink")、[金鲭](../Page/金鲭.md "wikilink")、[金枪鱼](../Page/金枪鱼.md "wikilink")、[马鲛鱼等](../Page/马鲛鱼.md "wikilink")，[鲸](../Page/鲸.md "wikilink")、[海豹](../Page/海豹.md "wikilink")、[企鹅也很多](../Page/企鹅.md "wikilink")。[棘皮动物中多](../Page/棘皮动物.md "wikilink")[海胆](../Page/海胆.md "wikilink")、[海参](../Page/海参.md "wikilink")、[蛇尾](../Page/蛇尾.md "wikilink")、[海百合等](../Page/海百合.md "wikilink")。海生[哺乳动物中](../Page/哺乳动物.md "wikilink")[儒艮是印度洋的特产](../Page/儒艮.md "wikilink")。[波斯湾和](../Page/波斯湾.md "wikilink")[斯里兰卡岛盛产珍珠](../Page/斯里兰卡岛.md "wikilink")。此外，[植物有各种](../Page/植物.md "wikilink")[藻类及各种](../Page/藻类.md "wikilink")[红树林](../Page/红树林.md "wikilink")。

### 交通运输

印度洋的地理位置非常重要，是沟通[亚洲](../Page/亚洲.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[欧洲和](../Page/欧洲.md "wikilink")[大洋洲的交通要道](../Page/大洋洲.md "wikilink")\[12\]。向东通过[马六甲海峡可以进入](../Page/马六甲海峡.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")，向西绕过[好望角可达](../Page/好望角.md "wikilink")[大西洋](../Page/大西洋.md "wikilink")，向西北通过[红海](../Page/红海.md "wikilink")、[苏伊士运河](../Page/苏伊士运河.md "wikilink")，可入[地中海](../Page/地中海.md "wikilink")。航线主要由亚、欧航线和[南亚](../Page/南亚.md "wikilink")、[东南亚](../Page/东南亚.md "wikilink")、[南非](../Page/南非.md "wikilink")、[大洋洲之间的航线](../Page/大洋洲.md "wikilink")。印度洋的海底电缆多分布在北部，重要的线路有[亞丁](../Page/亞丁_\(也門\).md "wikilink")—[孟买](../Page/孟买.md "wikilink")—[马德拉斯](../Page/马德拉斯.md "wikilink")—[新加坡线](../Page/新加坡.md "wikilink")；[亞丁](../Page/亞丁_\(也門\).md "wikilink")—[科伦坡线](../Page/科伦坡.md "wikilink")；东非沿岸线。[塞舌尔群岛的马埃岛](../Page/塞舌尔群岛.md "wikilink")、[毛里求斯岛和](../Page/毛里求斯岛.md "wikilink")[科科斯群岛是主要的海底电缆枢纽站](../Page/科科斯（基林）群岛.md "wikilink")。沿岸港口终年不冻，四季通航。

#### 主要港口

  - 印度：[加爾各答](../Page/加爾各答.md "wikilink")、[清奈](../Page/清奈.md "wikilink")、[孟買](../Page/孟買.md "wikilink")
  - 斯里蘭卡：[科倫坡](../Page/科倫坡.md "wikilink")
  - 南非：[德爾班和](../Page/德爾班.md "wikilink")[理查兹湾](../Page/理查兹湾.md "wikilink")
  - 印尼：[雅加達](../Page/雅加達.md "wikilink")
  - 澳大利亞：[費利曼圖市](../Page/費利曼圖市_\(西澳\).md "wikilink")

## 周邊國家及地區

下表列出印度洋周邊的國家及地區（領地以*斜體*標示）：

### 非洲

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li><em></em></li>
<li></li>
<li><em></em></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

### 亞洲

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li><em></em></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

### 大洋洲

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li>''  ''</li>
<li><em></em></li>
</ul></td>
<td><ul>
<li><em></em></li>
<li><em></em></li>
<li>'' ''</li>
</ul></td>
</tr>
</tbody>
</table>

## 主要海域，海峽及海灣

下表列出印度洋主要海域，海峽及海灣：

  - [阿拉伯海](../Page/阿拉伯海.md "wikilink")
  - [波斯湾](../Page/波斯湾.md "wikilink")
  - [红海](../Page/红海.md "wikilink")
  - [阿曼灣](../Page/阿曼灣.md "wikilink")
  - [亚丁湾](../Page/亚丁湾.md "wikilink")
  - [曼德海峽](../Page/曼德海峽.md "wikilink")
  - [嘠奇灣](../Page/嘠奇灣.md "wikilink")
  - [坎贝湾](../Page/坎贝湾.md "wikilink")
  - [保克海峡](../Page/保克海峡.md "wikilink")
  - [孟加拉灣](../Page/孟加拉灣.md "wikilink")
  - [安达曼海](../Page/安达曼海.md "wikilink")
  - [马六甲海峡](../Page/马六甲海峡.md "wikilink")
  - [莫桑比克海峡](../Page/莫桑比克海峡.md "wikilink")
  - [大澳洲灣](../Page/大澳洲灣.md "wikilink")
  - [曼納灣](../Page/曼納灣.md "wikilink")

## 參考文獻

## 外部連結

  - [NOAA In-situ Ocean Data
    Viewer](https://web.archive.org/web/20111224071631/http://dapper.pmel.noaa.gov/dchart/index.html?map=-11.25,73.125&z=2)
    Plot and download ocean observations
  - [The Indian Ocean in World History: Educational
    Website](http://www.indianoceanhistory.org) Interactive resource
    from the [Sultan Qaboos Cultural
    Center](../Page/Sultan_Qaboos_Cultural_Center.md "wikilink")
  - [The Regional Tuna Tagging Project-Indian Ocean with details of the
    importance of Tuna in the Indian
    Ocean..](http://www.rttp-io.org/en/)
  - [Detailed maps of the Indian
    Ocean](https://web.archive.org/web/20111112134042/http://www.alphavilla.co.uk/mauritius_map_indian_ocean.html)
  - [The Indian Ocean Trade: A Classroom
    Simulation](https://web.archive.org/web/20110925023358/http://www.bu.edu/africa/outreach/materials/handouts/indian.html)

[Category:远洋](../Category/远洋.md "wikilink")
[Category:海洋](../Category/海洋.md "wikilink")
[Category:印度洋](../Category/印度洋.md "wikilink")
[Category:印度洋地形](../Category/印度洋地形.md "wikilink")

1.  毛漢英《印度洋》，《中國大百科全書》条目

2.

3.  [Indo-American relations : foreign policy orientations and
    perspectives of P.V. Narasimha Rao and Bill
    Clinton](http://books.google.co.in/books?id=pTR2AAAAMAAJ) By Anand
    Mathur; Page 138 *“India occupies the central position in the Indian
    Ocean region that is why the Ocean was named after India”*

4.

5.
6.  [*Limits of Oceans and
    Seas*](http://www.iho.shom.fr/publicat/free/files/S23_1953.pdf) .
    International Hydrographic Organization Special Publication No. 23,
    1953.

7.  [Earth's Oceans](http://www.enchantedlearning.com/subjects/ocean/).
    EnchantedLearning.com. Retrieved on 2013-07-16.

8.
9.
10.

11.
12.