[鐵拳無敵孫中山.jpg](https://zh.wikipedia.org/wiki/File:鐵拳無敵孫中山.jpg "fig:鐵拳無敵孫中山.jpg")
**鐵拳無敵孫中山**為一網路集體創作的[惡搞人物](../Page/惡搞文化.md "wikilink")。將[中華民國之](../Page/中華民國.md "wikilink")[國父](../Page/國父.md "wikilink")[孫中山化身為武俠人物](../Page/孫中山.md "wikilink")，以[香港漫畫的](../Page/香港漫畫.md "wikilink")[旁白敘事形式寫成的文章](../Page/旁白.md "wikilink")，重新演繹教科書中僵化的[民國史以及典故](../Page/中華民國歷史.md "wikilink")。

初見於1992年在[香港上映的](../Page/香港.md "wikilink")《[家有囍事](../Page/家有囍事.md "wikilink")》電影中，當時常歡（[周星馳飾](../Page/周星馳.md "wikilink")）欺騙何里玉（[張曼玉飾](../Page/張曼玉.md "wikilink")）說他自己只穿內褲的造型是模仿《南拳北腿孫中山》的續集《醉拳甘迺迪》。1993年，香港知名樂團[溫拿五虎為了紀念成軍](../Page/溫拿樂隊.md "wikilink")，以團員拍攝一部民國歷史片《[廣東五虎之鐵拳無敵孫中山](../Page/廣東五虎之鐵拳無敵孫中山.md "wikilink")》，劇情中在拼湊密函過程裡，主角們將密函拼錯成「小心鐵拳無敵孫中山」，對孫中山小心戒備，在片尾將孫中山揍了一頓。但票房反應不佳，片名中的「鐵拳無敵孫中山」反而成為話題。

## 武功

  - 鐵沙神拳
  - 天魔神功
  - 蒼天之拳
  - 穿心彈腿
  - 紫禁皇掌

其它

## 小說版

鐵拳無敵孫中山一詞在香港出現時，也是[台灣出現香港漫畫熱潮之時](../Page/台灣.md "wikilink")。2000年，[黃玉郎旗下的](../Page/黃玉郎.md "wikilink")[天子傳奇連載至第四部](../Page/天子傳奇.md "wikilink")「[大唐威龍](../Page/大唐威龍.md "wikilink")」，由於作品賣座，各式討論以及[同人文章紛紛出籠](../Page/同人.md "wikilink")。而[天子傳奇是以每個朝代的開國](../Page/天子傳奇.md "wikilink")[君主](../Page/君主.md "wikilink")，都擁有絕世武功來平定[天下作為發想](../Page/天下.md "wikilink")，又因為[天子傳奇劇情以歷史](../Page/天子傳奇.md "wikilink")[演義為藍本](../Page/演義.md "wikilink")，最後是否會演出到民國初年，甚至近代？就成了一個話題。因此學生族群也以[教科書中照本宣科的](../Page/教科書.md "wikilink")[清末革命作為題材](../Page/清末革命.md "wikilink")，寫出一篇篇近現代[武俠故事](../Page/武俠.md "wikilink")，甚至惡搞當代的政治人物，作為對[聯考壓力的抒發](../Page/聯考.md "wikilink")。

由於[三民主義是臺灣昔日](../Page/三民主義.md "wikilink")[大學聯考的必修科目](../Page/大學聯考.md "wikilink")，有學生就把課程的內容改寫成一套套[武功](../Page/武功.md "wikilink")，而孫中山亦由一個文弱書生，變成一位身負驚世絕學的真命天子，並在[BBS連線版上流傳](../Page/BBS.md "wikilink")，被各大站台轉載。

### 版本

**本作品集體創作，版本與設定都不統一，並列如下**

  - 三回合版（潛入兩廣總督府）
  - 序篇版（[戊戌變法](../Page/戊戌變法.md "wikilink")）
  - 中篇章（片段式敘史，部分參考[天子傳奇劇情](../Page/天子傳奇.md "wikilink")）
  - 天子傳奇民國風雲（主角名為[孫逸仙](../Page/孫逸仙.md "wikilink")，師承洪秀全）

### 故事人物

  - 清末民初四大高手
      - 鐵拳無敵[孫中山](../Page/孫中山.md "wikilink")，武功天下第一
      - 洪憲帝王[袁世凱](../Page/袁世凱.md "wikilink")
      - 穿林北腿[蔣中正](../Page/蔣中正.md "wikilink")，孫中山首徒
      - 混世魔王[毛澤東](../Page/毛澤東.md "wikilink")
  - 其他人物
      - 垂簾屠龍老佛爺[慈禧太后](../Page/慈禧太后.md "wikilink")，屠龍師太逆徒
      - 大內高手[李蓮英](../Page/李蓮英.md "wikilink")，死於孫中山拳下
      - 飲冰主人[梁啟超](../Page/梁啟超.md "wikilink")（徒弟[徐志摩](../Page/徐志摩.md "wikilink")）
      - 絕世琴魔[黃興](../Page/黃興.md "wikilink")，被大內五大高手所殺
      - 太平天王[洪秀全](../Page/洪秀全.md "wikilink")，孫中山授業恩師
      - 一步蓮華[蔣經國](../Page/蔣經國.md "wikilink")（自古留傳蔣經國沒死就好了）
      - [密宗伏魔尊者](../Page/密宗.md "wikilink")（在[倫敦蒙難記事件中救出孫逸仙](../Page/倫敦蒙難記.md "wikilink")，真實身分[福爾摩斯](../Page/福爾摩斯.md "wikilink")）
      - 辜湯生（即[辜鴻銘](../Page/辜鴻銘.md "wikilink")）
  - 劇情中人物（未出場）
      - [虯髯客虎嘯皇拳傳人](../Page/虯髯客.md "wikilink")：三拳分力[孟德斯鳩](../Page/孟德斯鳩.md "wikilink")
      - 逆練如來神掌[希特勒](../Page/希特勒.md "wikilink")（由納粹的逆卐字發想）
  - 東西方天子系譜
      - 東方:[姬發](../Page/姬發.md "wikilink")－[嬴政](../Page/嬴政.md "wikilink")－[劉邦](../Page/劉邦.md "wikilink")－[李世民](../Page/李世民.md "wikilink")－[趙匡胤](../Page/趙匡胤.md "wikilink")－[朱元璋](../Page/朱元璋.md "wikilink")－[洪秀全](../Page/洪秀全.md "wikilink")（部分參考[天子傳奇系列](../Page/天子傳奇.md "wikilink")）
      - 西方:[摩西](../Page/摩西.md "wikilink")－[參孫](../Page/參孫.md "wikilink")－[耶穌](../Page/耶穌.md "wikilink")－[亞瑟王](../Page/亞瑟王.md "wikilink")－[理查一世](../Page/理查一世.md "wikilink")－[阿克巴](../Page/阿克巴.md "wikilink")－[洪秀全](../Page/洪秀全.md "wikilink")（網友自創）

### 武功心法

  - 網友自創武功部分
      - 五拳憲法:（參看[五權憲法](../Page/五權憲法.md "wikilink")）：行正拳、靂法拳、絲髮拳、烤世拳、奸鍘拳
      - 五拳現法 ─ 形正‧力發‧思法‧靠勢‧兼察
      - 三明主義（光技）：（參看[三民主義](../Page/三民主義.md "wikilink")）：明拳（攻擊）、明足（輕功）、明身（內功）
      - 三冥主義（影技）：冥遊‧冥至‧冥想（心法）
      - 明拳出步-輕功（參看[民權初步](../Page/民權初步.md "wikilink")）
      - 劍國大綱-劍法（參看[建國大綱](../Page/建國大綱.md "wikilink")）
      - 建國大罡-護身[罡氣](../Page/罡氣.md "wikilink")，比媲少林金鐘罩十二關威力
      - 天國神訣（參考[十誡](../Page/十誡.md "wikilink")）：
          - 十方靈動 \[光\]
          - 十面楚歌 \[影\]
          - 一暴十寒 \[水\]
          - 十風凌雲 \[風\]
          - 十字分金 \[電\]
          - 十萬火急 \[火\]
          - 十嶺峰高 \[土\]（參看星期）
          - 十指浩繁 \[亂\]
          - 十來運轉 \[卸\]
          - 十破天驚 \[聚\]
          - 魔鬼神經(的黎波里)

<!-- end list -->

  - 參考武功部分
      - [如來神掌](../Page/如來神掌.md "wikilink")（參考同名香港漫畫）
      - [紫雷神功](../Page/紫雷神功.md "wikilink")（紫雷刀法，參考天子傳奇3[項羽武功](../Page/項羽.md "wikilink")）
      - 鈾光球－轟毀[廣島](../Page/廣島.md "wikilink")、[長崎](../Page/長崎.md "wikilink")（參考[龍虎五世雷烈武功](../Page/龍虎五世.md "wikilink")）
      - [天魔功](../Page/天魔功.md "wikilink")（參考[天子傳奇](../Page/天子傳奇.md "wikilink")）
      - [渾天寶鑑](../Page/渾天寶鑑.md "wikilink")（參考[天子傳奇](../Page/天子傳奇.md "wikilink")）
      - [化骨綿掌](../Page/化骨綿掌.md "wikilink")（參考[鹿鼎記](../Page/鹿鼎記.md "wikilink")）
      - 六神訣（參考[天子傳奇](../Page/天子傳奇.md "wikilink")、[龍虎五世](../Page/龍虎五世.md "wikilink")）

## 漫畫版

在經歷數年的[港漫文潮後](../Page/港漫文.md "wikilink")，「鐵拳無敵孫中山」暫時沉寂，07年適逢[黃玉郎旗下書系中的](../Page/黃玉郎.md "wikilink")[神兵玄奇推出新刊](../Page/神兵玄奇.md "wikilink")[神兵玄奇F](../Page/神兵玄奇F.md "wikilink")，編劇[鍾英偉善用](../Page/鍾英偉.md "wikilink")[網路上的](../Page/網路.md "wikilink")[同人創作加以發揮](../Page/同人創作.md "wikilink")，第一回即有身穿白色[中山裝的](../Page/中山裝.md "wikilink")「鐵拳無敵孫中山」於倫敦清朝駐英公使館登場，並施展出「虎嘯皇拳」將清兵擊退，使得孫中山得以順利逃離公使館。然而「鐵拳無敵孫中山」是由神兵玄奇主角南宮先生假扮，孫中山仍是一介書生。

## 类似作品

  - 《[吸血鬼猎人林肯](../Page/吸血鬼猎人林肯_\(电影\).md "wikilink")》

## 外部連結

  - [鐵拳無敵孫中山風靡網路](http://www.richyli.com/report/2001_11_29_01.htm)

[category:网络文化](../Page/category:网络文化.md "wikilink")

[Category:孫中山題材作品](../Category/孫中山題材作品.md "wikilink")
[Category:臺灣惡搞文化](../Category/臺灣惡搞文化.md "wikilink")