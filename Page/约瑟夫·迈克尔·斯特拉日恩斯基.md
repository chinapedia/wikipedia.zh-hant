**约瑟夫·迈克尔·斯特拉日恩斯基**（Joseph Michael
Straczynski，），是一位获奖颇丰的美国[编剧](../Page/编剧.md "wikilink")、[制片人](../Page/制片人.md "wikilink")、[作家](../Page/作家.md "wikilink")。他曾经写过[电视剧本](../Page/电视剧本.md "wikilink")、[小说](../Page/小说.md "wikilink")、[短篇小说](../Page/短篇小说.md "wikilink")、[连环画册以及](../Page/连环画册.md "wikilink")[广播剧](../Page/广播剧.md "wikilink")。他还是[科幻电视剧集](../Page/科幻.md "wikilink")《[巴比伦五号](../Page/巴比伦五号.md "wikilink")》（*Babylon
5*）及其衍生剧集《[巴比伦五号：东征](../Page/巴比伦五号：东征.md "wikilink")》（*Crusade*）的创作者、[执行制片与剧本主编](../Page/执行制片.md "wikilink")。《巴比伦五号》共110集中，斯特拉日恩斯基写了91集，特别是第三、四季完整的59集，以及第五季只有一集不是他写的。他还撰写了4部《巴比伦五号》的电影剧本。

## 參考文獻

  -
## 外部連結

  -
  -
  -
  - [J. Michael
    Straczynski](https://web.archive.org/web/20060115025142/http://www.hot.ee/b5races/jms_bibliography.htm)
    at B5races

  - [JMSNews](http://www.jmsnews.com/)

[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")