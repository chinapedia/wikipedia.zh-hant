[New_MSN_Logo.png](https://zh.wikipedia.org/wiki/File:New_MSN_Logo.png "fig:New_MSN_Logo.png")
（全名：）是由[微軟公司架設的](../Page/微軟.md "wikilink")[入口網站及資訊服務](../Page/入口網站.md "wikilink")，於1995年8月24日首度以[網際網路服務供應商隨著](../Page/網際網路服務供應商.md "wikilink")[Windows
95一起問世](../Page/Windows_95.md "wikilink")。\[1\]

MSN原是一个類似[CompuServe及](../Page/CompuServe.md "wikilink")[AOL的收费服務](../Page/AOL.md "wikilink")，提供[撥號上網及附屬訊息](../Page/撥號上網.md "wikilink")、[網路聊天室](../Page/網路聊天室.md "wikilink")（網絡即時通訊）等服務，但同时亦允許其它現有[網際網路使用者透過因特網来使用](../Page/網際網路.md "wikilink")。MSN曾是[世界上第二大的網際網路服務供應商](../Page/世界.md "wikilink")，擁有9百萬的使用者，僅次於AOL。後来隨著[網際網路普及化](../Page/網際網路.md "wikilink")，使微軟將大部份原来要付費的項目轉變為免費的MSN[搜尋引擎網站](../Page/搜尋引擎.md "wikilink")，这是MSN的第二春。微軟在收購[Hotmail之后](../Page/Hotmail.md "wikilink")，不斷將旗下的服務重新整合；特别是在微軟的「[Microsoft
Account](../Page/Microsoft_Account.md "wikilink")」認證技術成熟之後，MSN擴充[Outlook.com](../Page/Outlook.com.md "wikilink")
([Microsoft Office家族](../Page/Microsoft_Office.md "wikilink"))、[MSN
Messenger](../Page/MSN_Messenger.md "wikilink")（終止服務）和MSN Spaces服務（MSN
Spaces後被整合為[OneDrive成為](../Page/OneDrive.md "wikilink")[Microsoft
Office家族的一部分](../Page/Microsoft_Office.md "wikilink")。

## 服務

### MSN入口網站

提供多國語言介面，並為[Windows 10搭載的](../Page/Windows_10.md "wikilink")[Microsoft
Edge瀏覽器預設首頁](../Page/Microsoft_Edge.md "wikilink")。

### 微軟新聞

微軟新聞 (Microsoft News) 由MSN 新聞改版而來，為新聞資訊匯集平台。

### MSN 天氣

MSN 天氣 (MSN Weather)，過去為Bing 天氣，後規劃為MSN家族產品，提供世界各地天氣服務。

### MSN 財經

MSN 財經 (MSN Money)，過去曾為Bing
財經，後劃歸MSN家族，提供使用者查詢[股價](../Page/股價.md "wikilink")、[匯率及](../Page/匯率.md "wikilink")[財經新聞等服務](../Page/財經.md "wikilink")。

### MSN 體育

MSN 體育 (MSN Sports)，過去曾為 Bing Sports，現為MSN家族，提供全球各類體育賽事資訊及比分。

## 過去的服務

### Outlook.com

Outlook.com，前稱Hotmail，是一個免費的[webmail服務](../Page/webmail.md "wikilink")，1996年首次運行時是獨立的。1997年才成為MSN的一部份。2006年後成為微軟[Windows
Live在線服務系統的一部份](../Page/Windows_Live.md "wikilink")，現為[Microsoft
Office家族的一部分](../Page/Microsoft_Office.md "wikilink")。\[2\]
2013年4月3日，Hotmail被Outlook.com取代。

### MSN世界服務

在美國，MSN與當地電信公司和廣播站聯合推出其服務，在[加拿大](../Page/加拿大.md "wikilink")，與[互聯網服務提供商](../Page/互聯網服務提供商.md "wikilink")[加拿大貝爾旗下的Bell](../Page/加拿大貝爾.md "wikilink")
Sympatico合作，創出“Sympatico / MSN”。\[3\]

在[澳大利亞](../Page/澳大利亞.md "wikilink")，微軟最初與[澳洲電信合作推出](../Page/澳洲電信.md "wikilink")“OnAustralia”，不過次年便從合資企業中撤出。因此澳洲電信便有了100%的擁有權並將之重命名為“[BigPond](../Page/BigPond.md "wikilink")”。微軟隨後轉向與[第九頻道電視臺創出](../Page/第九頻道電視臺.md "wikilink")“[ninemsn](../Page/ninemsn.md "wikilink")”。在[墨西哥與](../Page/墨西哥.md "wikilink")[墨西哥電信合作](../Page/墨西哥電信.md "wikilink")，創出“Prodigy
/
MSN”。\[4\]在[新西蘭與](../Page/新西蘭.md "wikilink")[Xtra及](../Page/Xtra_\(ISP\).md "wikilink")[新西蘭電信的合作創立的](../Page/新西蘭電信.md "wikilink")[XtraMSN於](../Page/XtraMSN.md "wikilink")2006年終止服務。\[5\]

MSN的服務遍及全球。2007年，微軟在[中國](../Page/中國.md "wikilink")[上海](../Page/上海.md "wikilink")[紫竹科學園區建立了一個MSN研發中心](../Page/紫竹科學園區.md "wikilink")，該部門為MSN服務提供技術支持。\[6\]

2012年7月15日，微軟與[NBC發表聯合聲明](../Page/NBC.md "wikilink")，結束16年的合作關係，由[NBC收購微軟持有的](../Page/NBC.md "wikilink")50%股票。\[7\]因此[MSNBC脫離與MSN入口網站的關係](../Page/MSNBC.md "wikilink")。MSNBC後啟用自己的新聞網站，即MSNBC.com。

### 中國大陆

MSN入口於2005年5月在[中国大陆正式上線](../Page/中国大陆.md "wikilink")，由上海美思恩網-{}-絡通訊技術有限公司經營，並與《[北京青年報](../Page/北京青年報.md "wikilink")》等多個內容提供商聯合推出內容服務。
MSN中文网于2016年4月29日发布通知，其于2016年6月7日停止提供[简体中文服务](../Page/简体中文.md "wikilink")。\[8\]

## 参考文献

## 外部連結

  -
[Category:門戶網站](../Category/門戶網站.md "wikilink")
[Category:1995年建立的网站](../Category/1995年建立的网站.md "wikilink")
[MSN](../Category/MSN.md "wikilink")
[Category:網上聊天](../Category/網上聊天.md "wikilink")
[Category:網際網路會議](../Category/網際網路會議.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.