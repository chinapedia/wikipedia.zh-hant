**啟思中學**（，簡稱CSS、**啟思**）位於[香港](../Page/香港.md "wikilink")[將軍澳](../Page/將軍澳新市鎮.md "wikilink")[蓬萊路](../Page/蓬萊路.md "wikilink")3號，毗鄰[清水灣半島](../Page/清水灣半島_\(住宅\).md "wikilink")，於2006年創校，於2009年2月被錄取為[國際文憑組織](../Page/國際文憑組織.md "wikilink")[學校](../Page/學校.md "wikilink")\[1\]，是[香港一家](../Page/香港.md "wikilink")[全日制男女](../Page/全日.md "wikilink")[中學](../Page/中學.md "wikilink")。全體師生人數約1000多人。啟思中學是一所[直資的](../Page/直資.md "wikilink")[國際文憑組織](../Page/國際文憑組織.md "wikilink")[學校](../Page/學校.md "wikilink")（國際文憑組織學校編號：003069），提供[香港](../Page/香港.md "wikilink")[新高中課程及](../Page/新高中.md "wikilink")[國際文憑組織IB](../Page/國際文憑組織.md "wikilink")
Middle Years Programme & IB
Diploma課程。2012年學校成立六週年，第一屆中六生畢業。在2014年度，成為了全港最貴的直資英文中學。其直屬小學是[啟思小學](../Page/啟思小學.md "wikilink")。

## 學社

啟思中學推行[學社制](../Page/社會團體.md "wikilink")，其六個社分別是[紫荊社](../Page/洋紫荊.md "wikilink")（Bauhinia）、[紅棉社](../Page/木棉.md "wikilink")（Bombax）、[黃槐社](../Page/決明.md "wikilink")（Cassia）、[鳳凰社](../Page/鳳凰木.md "wikilink")（Delonix）、[藍楹社](../Page/藍花楹.md "wikilink")（Jacaranda）及[龍柏社](../Page/龍柏社.md "wikilink")（Juniper）。\[2\]

## 校舍

啟思中學的[校舍總體分為三個部份](../Page/校舍.md "wikilink")，分別是A座、B座及C座。

A座最接近校門，方便接待[賓客](../Page/賓客.md "wikilink")，主要是[禮堂及](../Page/禮堂.md "wikilink")[校務處](../Page/校務處.md "wikilink")。

B座主要是[科學](../Page/科學.md "wikilink")[實驗室及](../Page/實驗室.md "wikilink")[食物](../Page/食物.md "wikilink")[科技中心](../Page/科技.md "wikilink")，以及在一樓的[教員休息室](../Page/教員休息室.md "wikilink")。

C座主要是[課室](../Page/課室.md "wikilink")、[音樂室](../Page/音樂.md "wikilink")、[電腦室](../Page/電腦.md "wikilink")、[設計與](../Page/設計.md "wikilink")[科技中心及](../Page/科技.md "wikilink")[圖書館](../Page/學校圖書館.md "wikilink")。在2014年暑假擴建了2/F的空地爲三間課室，有一間利用活動式門板分割。

## 鄰近

  - [日出康城](../Page/日出康城.md "wikilink")
  - [邵氏影城](../Page/邵氏影城.md "wikilink")
  - [清水灣半島](../Page/清水灣半島_\(住宅\).md "wikilink")

## 交通

<div class="NavFrame collapsed" style="clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{將軍澳綫色彩}}">█</font>[將軍澳綫](../Page/將軍澳綫.md "wikilink")：[將軍澳站](../Page/將軍澳站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<small>註：[新界的士不可進入蓬萊路](../Page/新界的士.md "wikilink")</small>

</div>

</div>

## 參考資料及注釋

<references/>

## 外部連結

  - [啟思中學首頁](http://www.css.edu.hk)
  - [啟思中學教師列表](http://www.css.edu.hk/index.php?option=com_content&task=view&id=26&Itemid=68)
  - [啟思中學地圖](http://203.198.184.24/documents/location.pdf)
  - [啟思中學外貌](http://commondatastorage.googleapis.com/static.panoramio.com/photos/original/42712320.jpg)

[Category:香港直資學校](../Category/香港直資學校.md "wikilink")
[Category:百勝角](../Category/百勝角.md "wikilink")
[C](../Category/西貢區中學.md "wikilink")
[Category:國際文憑組織學校](../Category/國際文憑組織學校.md "wikilink")
[Category:2006年創建的教育機構](../Category/2006年創建的教育機構.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")
[Category:國際文憑組織授權學校](../Category/國際文憑組織授權學校.md "wikilink")

1.  <http://www.ibo.org/school/003069/>
2.  <http://www.css.edu.hk/index.php?option=com_content&view=article&id=362&Itemid=98>