**三鳳宮**，[臺灣](../Page/臺灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[三民區廟宇](../Page/三民區.md "wikilink")，初建於[清聖祖](../Page/清聖祖.md "wikilink")[康熙十二年](../Page/康熙.md "wikilink")（1673年），舊名稱「三鳳亭」，所奉主神為[中壇元帥](../Page/中壇元帥.md "wikilink")（[李哪吒](../Page/李哪吒.md "wikilink")），是[南臺灣著名的](../Page/南臺灣.md "wikilink")[道教聖地之一](../Page/道教.md "wikilink")。

[三鳳宮鎮殿中壇元帥.JPG](https://zh.wikipedia.org/wiki/File:三鳳宮鎮殿中壇元帥.JPG "fig:三鳳宮鎮殿中壇元帥.JPG")[三民區三鳳宮鎮殿](../Page/三民區.md "wikilink")[中壇元帥寶像](../Page/中壇元帥.md "wikilink")。\]\]
[高雄三鳳宮文化大樓.jpg](https://zh.wikipedia.org/wiki/File:高雄三鳳宮文化大樓.jpg "fig:高雄三鳳宮文化大樓.jpg")
[三鳳宮廟匾.jpg](https://zh.wikipedia.org/wiki/File:三鳳宮廟匾.jpg "fig:三鳳宮廟匾.jpg")

## 命名

「三鳳」二字以所在地「[三塊厝](../Page/三塊厝_\(高雄市\).md "wikilink")」（今[高雄市](../Page/高雄市.md "wikilink")[三民區](../Page/三民區.md "wikilink")）與「[鳳鼻頭山](../Page/鳳鼻頭山.md "wikilink")」之「鳳」首字命名。

另說，本廟外環三塊厝港（現在的三民區幸福川，原二號運河）、[凹子底港和](../Page/凹子底.md "wikilink")[龍水港三水域](../Page/龍水港.md "wikilink")，故取「三水」的「三」和鳳鼻頭山的「鳳」字。

## 歷史

三鳳宮，[清](../Page/清.md "wikilink")[康熙時主神](../Page/康熙.md "wikilink")[中壇元帥](../Page/中壇元帥.md "wikilink")[分靈於](../Page/分靈.md "wikilink")[鼓山區龍水港](../Page/鼓山區.md "wikilink")[龍水化龍宮](../Page/龍水化龍宮.md "wikilink")，一直以來為[高雄火車站一帶的著名廟宇](../Page/高雄車站.md "wikilink")，原在[建國三路上](../Page/建國三路.md "wikilink")，1964年建國三路拓寬，而遂遷廟至今河北二路的現址，1971年後重建完成，正式更名為「三鳳宮」，也成為全臺灣規模最大的哪吒三太子廟。

## 建築

三鳳宮現今建築為[謝自南](../Page/謝自南.md "wikilink")(澎湖馬公市後窟潭人)所設計，據傳三鳳宮首開臺灣二、三樓層廟宇的建築模式，使後來不少廟宇建築相繼跟進，引領臺灣廟宇建築的風潮\[1\]。

一樓大殿正龕奉祀[中壇元帥哪吒太子](../Page/中壇元帥.md "wikilink")，配祀[天上聖母](../Page/天上聖母.md "wikilink")、[李府千歲](../Page/李府千歲.md "wikilink")、龍邊神龕奉祀[註生娘娘](../Page/註生娘娘.md "wikilink")、虎邊神龕奉祀[福德正神](../Page/福德正神.md "wikilink")，開基中壇元帥神像為[康熙年間分靈之](../Page/康熙.md "wikilink")「老祖」，供信眾膜拜。

二樓大雄寶殿則供奉[釋迦三尊](../Page/釋迦三尊.md "wikilink")（[釋迦牟尼佛](../Page/釋迦牟尼佛.md "wikilink")、[文殊菩薩](../Page/文殊菩薩.md "wikilink")、[普賢菩薩](../Page/普賢菩薩.md "wikilink")）、[觀世音菩薩](../Page/觀世音菩薩.md "wikilink")、[彌勒菩薩](../Page/彌勒菩薩.md "wikilink")、[韋馱](../Page/韋馱.md "wikilink")、[伽藍護法](../Page/伽藍護法.md "wikilink")、[十八羅漢](../Page/十八羅漢.md "wikilink")。

三樓凌霄寶殿上祀道教尊神[玉皇上帝](../Page/玉皇上帝.md "wikilink")、[南斗星君](../Page/南斗星君.md "wikilink")、[北斗星君](../Page/北斗星君.md "wikilink")，供善信祈求延壽消災。

三樓龍邊[光明燈殿奉祀](../Page/光明燈.md "wikilink")[中壇元帥](../Page/中壇元帥.md "wikilink")、虎邊[太歲殿奉祀六十甲子太歲星君](../Page/太歲.md "wikilink")，提供信眾點光明燈、[安太歲的服務](../Page/安太歲.md "wikilink")。

三鳳宮的[門神為](../Page/門神.md "wikilink")「平塗彩畫」，為廟宇繪畫大師[潘麗水之作](../Page/潘麗水.md "wikilink")，潘之彩繪以莊嚴細膩聞名，於[臺灣極富盛名](../Page/臺灣.md "wikilink")，值得一覽，高雄三鳳宮被譽為「潘麗水門神彩繪最高顛峰的作品」\[2\]。

[三鳳宮抱鼓石.jpg](https://zh.wikipedia.org/wiki/File:三鳳宮抱鼓石.jpg "fig:三鳳宮抱鼓石.jpg")

[三鳳宮執事牌_(1)_.jpg](https://zh.wikipedia.org/wiki/File:三鳳宮執事牌_\(1\)_.jpg "fig:三鳳宮執事牌_(1)_.jpg")

[三鳳宮執事牌_(2).jpg](https://zh.wikipedia.org/wiki/File:三鳳宮執事牌_\(2\).jpg "fig:三鳳宮執事牌_(2).jpg")

## 祭典

[舊曆九月初九](../Page/舊曆.md "wikilink")[重陽節](../Page/重陽節.md "wikilink")，哪吒太子元帥誕辰日。
農曆八月至九月誕辰日為三鳳宮中壇元帥香期，分靈全國各地之廟宇齊聚廟庭參香謁祖。

## 參見

  - [哪吒](../Page/哪吒.md "wikilink")
  - [龍水化龍宮](../Page/龍水化龍宮.md "wikilink")
  - [廣興宮](../Page/八張犁廣興宮.md "wikilink")
  - [覆鼎金保安宮](../Page/覆鼎金保安宮.md "wikilink")
  - [新營太子宮](../Page/新營太子宮.md "wikilink")

## 註解

<div class="references-2column">

<references/>

</div>

## 外部連結

  - [高雄市三塊厝興德團（三鳳宮）](http://www.sunfong.org.tw/)

[Category:高雄市旅遊景點](../Category/高雄市旅遊景點.md "wikilink")
[S](../Category/高雄市廟宇.md "wikilink")
[S](../Category/台灣哪吒廟.md "wikilink")
[S](../Category/三民區.md "wikilink")

1.  。[新台灣新聞週刊 2008.01.31 李丹妮《高雄三鳳宮
    太子爺廟高人氣》：一九六二年三鳳宮董事及建築師，決定以「北方式」的宮殿構造三鳳宮，據傳三鳳宮首開台灣二、三樓廟宇的建築模式，也使得後來為數不少的廟宇建築相繼跟進，為臺灣廟宇建築轉變的先驅](http://www.newtaiwan.com.tw/bulletinview.jsp?bulletinid=76864)

2.  [廟修很大
    黑面神變粉面](http://www.appledaily.com.tw/appledaily/article/headline/20140221/35655315/)