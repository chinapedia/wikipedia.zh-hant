**唐閔帝李從厚**（），小字菩薩奴，[五代時期](../Page/五代十国.md "wikilink")[後唐皇帝](../Page/後唐.md "wikilink")，為後唐明宗[李嗣源之子](../Page/唐明宗.md "wikilink")，母[昭懿皇后夏氏](../Page/昭懿皇后_\(後唐\).md "wikilink")，有一胞兄[李从荣](../Page/李从荣.md "wikilink")。李嗣源因李從厚與自己相像，特別喜歡他。

## 生平

李從厚於李嗣源在位時，原被封為宋王。後唐[長興四年](../Page/长兴_\(年号\).md "wikilink")（933年），李嗣源病重，本為繼承人的秦王李從榮誤以為李嗣源已死，為確保能夠繼位，遂帶兵入宮，事敗被殺。李嗣源不得已，召時任[天雄節度使的李從厚回京](../Page/天雄军节度使.md "wikilink")。不久，李嗣源去世，李從厚繼位。次年（934年），改年號[應順](../Page/应顺.md "wikilink")。

李從厚即帝位後，信任[朱弘昭](../Page/朱弘昭.md "wikilink")、[馮贇等人](../Page/冯赟.md "wikilink")，二人於應順元年（934年），調動各重要[節度使](../Page/節度使.md "wikilink")，準備[削藩](../Page/削藩.md "wikilink")，鳳翔節度使潞王[李從珂恐懼](../Page/李從珂.md "wikilink")，遂反，攻入京師[洛陽](../Page/洛陽.md "wikilink")，李從厚出逃[魏州](../Page/魏州.md "wikilink")，途经[卫州](../Page/卫州.md "wikilink")，遇到河东节度使[石敬瑭](../Page/石敬瑭.md "wikilink")。石敬瑭無意救之。李从厚的亲随不满石敬瑭，抽刀要杀石敬瑭，结果反被石敬瑭的侍卫杀死。石敬瑭的部將[劉知遠尽杀闵帝亲随](../Page/劉知遠.md "wikilink")，把闵帝安置在卫州後，不久離去。皇太后下令降閔帝為鄂王。不久闵帝為潞王李從珂派人所殺。後晉高祖[石敬瑭稱帝後](../Page/石敬瑭.md "wikilink")，將他[諡為閔皇帝](../Page/谥号.md "wikilink")。

李從厚個性仁慈，對兄弟很和睦，雖遭李從榮忌恨，卻能坦誠相待，所以當時才能逃過一劫。本來與李從珂也沒有過節，只因輕易地聽信周遭人的讒言，才會招來大禍。

## 家庭

### 妻子

  - [孔皇后](../Page/孔皇后.md "wikilink")

### 子女

#### 子

1.  [李重哲等四子](../Page/李重哲.md "wikilink")，孔-{后}-所生，被李从珂所杀。

## 参考文献

{{-}}

[Category:後唐皇帝](../Category/後唐皇帝.md "wikilink")
[3](../Category/唐明宗皇子.md "wikilink")
[Category:后唐被杀害人物](../Category/后唐被杀害人物.md "wikilink")
[Category:五代十国废帝](../Category/五代十国废帝.md "wikilink")
[Category:五代十国被杀害皇帝](../Category/五代十国被杀害皇帝.md "wikilink")
[C从厚](../Category/李姓.md "wikilink")
[Category:諡閔](../Category/諡閔.md "wikilink")