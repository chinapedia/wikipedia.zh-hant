[Laprascopy-Roentgen.jpg](https://zh.wikipedia.org/wiki/File:Laprascopy-Roentgen.jpg "fig:Laprascopy-Roentgen.jpg")時的X射線影像图\]\]

**-{zh:X射线;zh-hans:X射线;zh-hant:X射線;zh-cn:X射线;zh-tw:X射線;zh-hk:X光;zh-mo:X射线}-**（），又被称为**爱克斯射线**、**艾克斯射线**、**伦琴射线**或**-{zh:X光;zh-hans:X光;zh-hant:X光;zh-cn:X光;zh-tw:X光;zh-hk:X射線;zh-mo:X光}-**，是一种[波长范围在](../Page/波长.md "wikilink")0.01[纳米到](../Page/纳米.md "wikilink")10纳米之间（对应频率范围30
[PHz到](../Page/赫兹.md "wikilink")30[EHz](../Page/赫兹.md "wikilink")）的[电磁辐射形式](../Page/电磁辐射.md "wikilink")。X射线最初用于[医学成像诊断和](../Page/医学成像.md "wikilink")[X射线结晶学](../Page/X光散射技術.md "wikilink")。X射线也是[游離輻射等这一类对人体有危害的射线](../Page/游離輻射.md "wikilink")。
[替代=人体肺部的X射线](https://zh.wikipedia.org/wiki/File:Lung_X-Ray.jpg "fig:替代=人体肺部的X射线")
X射線[波長範圍在較短處與](../Page/波長.md "wikilink")[伽馬射線較長處重疊](../Page/伽馬射線.md "wikilink")。

## 历史

早期X射线重要的研究者有Ivan
Pului教授、[威廉·克鲁克斯](../Page/威廉·克鲁克斯.md "wikilink")[爵士](../Page/爵士.md "wikilink")、[约翰·威廉·希托夫](../Page/约翰·威廉·希托夫.md "wikilink")、[欧根·戈尔德斯坦](../Page/欧根·戈尔德斯坦.md "wikilink")、[海因里希·魯道夫·赫茲](../Page/海因里希·魯道夫·赫茲.md "wikilink")、[菲利普·莱纳德](../Page/菲利普·莱纳德.md "wikilink")、[亥姆霍兹](../Page/亥姆霍兹.md "wikilink")、[尼古拉·特斯拉](../Page/尼古拉·特斯拉.md "wikilink")、[爱迪生](../Page/爱迪生.md "wikilink")、[查爾斯·巴克拉](../Page/查爾斯·巴克拉.md "wikilink")、[馬克思·馮·勞厄和](../Page/馬克思·馮·勞厄.md "wikilink")[威廉·伦琴](../Page/威廉·伦琴.md "wikilink")。

[Roentgen-Roehre.svg](https://zh.wikipedia.org/wiki/File:Roentgen-Roehre.svg "fig:Roentgen-Roehre.svg")

1869年物理学家约翰·威廉·希托夫观察到[真空管中的阴极发出的射线](../Page/真空管.md "wikilink")。当这些射线遇到玻璃管壁会产生荧光。1876年这种射线被[欧根·戈尔德斯坦命名为](../Page/欧根·戈尔德斯坦.md "wikilink")「[阴极射线](../Page/阴极射线.md "wikilink")」。随后，英国物理学家克鲁克斯研究[稀有气体裡的能量释放](../Page/稀有气体.md "wikilink")，并且制造了克鲁克斯管。这是一种玻璃真空管，内有可以产生高电压的电极。他还发现，当将未曝光的相片底片靠近这种管时，一些部分被感光了，但是他没有继续研究这一现象。1887年4月，尼古拉·特斯拉开始使用自己设计的高电压真空管与克鲁克斯管研究X光。他发明了单电极X光管，在其中电子穿过物质，发生了现在叫做[轫致辐射的效应](../Page/轫致辐射.md "wikilink")，生成高能X光射线。1892年特斯拉完成了这些实验，但是他并没有使用X光这个名字，而只是笼统地称为放射能。他继续进行实验，并提醒科学界注意阴极射线对生物体的危害性，但他没有公开自己的实验成果。1892年赫兹进行实验，提出阴极射线可以穿透非常薄的金属箔。赫兹的学生倫納德进一步研究这一效应，对很多金属进行了实验。[亥姆霍兹则对光的电磁本性进行了数学推导](../Page/亥姆霍兹.md "wikilink")。

[X-ray_by_Wilhelm_Röntgen_of_Albert_von_Kölliker's_hand_-_18960123-02.jpg](https://zh.wikipedia.org/wiki/File:X-ray_by_Wilhelm_Röntgen_of_Albert_von_Kölliker's_hand_-_18960123-02.jpg "fig:X-ray_by_Wilhelm_Röntgen_of_Albert_von_Kölliker's_hand_-_18960123-02.jpg")拍摄的一張X射线照片，伦琴夫人的手骨与戒指\]\]
1895年11月8日德国科学家伦琴开始进行阴极射线的研究。1895年12月28日他完成了初步的实验报告“一种新的射线”。他把这项成果发布在[維爾茨堡的Physical](../Page/維爾茨堡.md "wikilink")-Medical
Society杂志上。为了表明这是一种新的射线，伦琴采用表示未知数的X来命名。很多科学家主张命名为伦琴射线，伦琴自己坚决反对，但是这一名称直至今日仍然被广泛使用，尤其在德语国家。1901年伦琴获得[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

1895年[爱迪生研究了材料在X光照射下发出荧光的能力](../Page/爱迪生.md "wikilink")，发现[钨酸钙最为明显](../Page/钨酸钙.md "wikilink")。1896年3月爱迪生-{zh-hans:发;
zh-hant:發;}-明了荧光观察管，后来被用于医用X光的检验。然而1903年爱迪生终止了自己对X光的研究，因为他公司的一名玻璃工人喜欢将X光管放在手上检验，最後得了癌症，尽管进行了截肢手术仍然没能挽回生命。巴克拉发现X射线能够被气体散射，并且每一种元素有其特征X谱线。他因此获得了1917年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

在20世纪80年代，X射线激光器被设置为[罗纳德·里根总统的战略主动防御计划的一部分](../Page/罗纳德·里根.md "wikilink")。然而对该装置（一种类似激光炮，或者死亡射线的装置，由[热核反应提供能量](../Page/热核反应.md "wikilink")）最初的、同时也是仅有的试验并没有给出结论性的结果。同时，由于政治和技术的原因，整体的计划（包括X射线激光器）被搁置了（然而该计划后来又被重新启动——使用了不同的技术，并作为[布什总统](../Page/乔治·赫伯特·沃克·布什.md "wikilink")[国家导弹防御计划的一部分](../Page/国家导弹防御计划.md "wikilink")）。

在20世纪90年代，[哈佛大学建立了](../Page/哈佛大学.md "wikilink")[钱德拉X射线天文台](../Page/钱德拉X射线天文台.md "wikilink")，用来观测[宇宙中强烈的天文现象中产生的X射线](../Page/宇宙.md "wikilink")。与从[可见光观测到的相对稳定的](../Page/可见光.md "wikilink")[宇宙不同](../Page/宇宙.md "wikilink")，从X射线观测到的[宇宙是不稳定的](../Page/宇宙.md "wikilink")。它向人们展示了[恒星如何被](../Page/恒星.md "wikilink")[黑洞绞碎](../Page/黑洞.md "wikilink")，[星系间的碰撞](../Page/星系.md "wikilink")，[超新星和](../Page/超新星.md "wikilink")[中子星](../Page/中子星.md "wikilink")。

## X射线的产生

X射线波长略大于0.5 [nm的被称作](../Page/nm.md "wikilink")**软X射线**。波长短于0.1
[nm的叫做](../Page/nm.md "wikilink")**硬X射线**。硬X射线与波长长的（能量小）[伽马射线范围重叠](../Page/伽马射线.md "wikilink")，二者的区别在于辐射源，而不是波长：X射线[光子产生于高能](../Page/光子.md "wikilink")[电子加速](../Page/电子.md "wikilink")，伽马射线则来源于[原子核衰变](../Page/原子核.md "wikilink")。

产生X射线的最簡單方法是用加速后的電子撞击[金属靶](../Page/金属.md "wikilink")。撞击过程中，电子突然减速，其损失的动能会以光子形式放出，形成X光光谱的连续部分，稱之為[制動輻射](../Page/制動輻射.md "wikilink")。通过加大加速电压，电子携带的能量增大，则有可能将金属原子的内层电子撞出。于是内层形成空穴，外层电子跃迁回内层填补空穴，同时放出波长在0.1纳米左右的光子。由于外层电子跃迁放出的能量是量子化的，所以放出的光子的波长也集中在某些部分，形成了X光谱中的特征线，此稱為[特性輻射](../Page/特性輻射.md "wikilink")。

此外，高強度的X射線亦可由[同步加速器或](../Page/同步加速器.md "wikilink")[自由電子雷射產生](../Page/自由電子雷射.md "wikilink")。[同步輻射光源](../Page/同步輻射.md "wikilink")，具有高強度、連續波長、光束準直、極小的光束截面積並具有時間脈波性與偏振性，因而成為科學研究最佳之X光光源。

## 探测器

X射线的探测可基于多种方法。最普通的一种方法叫做[照相底板法](../Page/照相底板.md "wikilink")，这种方法在[医院里经常使用](../Page/医院.md "wikilink")。將一片照相[底片放置於人體後方](../Page/底片.md "wikilink")，X射線穿過人體內軟組織（[皮膚及器官](../Page/皮膚.md "wikilink")）後會照射到[底片](../Page/底片.md "wikilink")，令這些部位於[底片經顯影後保留黑色](../Page/底片.md "wikilink")；X射線無法穿過人體內的硬組織，如[骨或其他被注射含](../Page/骨.md "wikilink")[鋇或](../Page/鋇.md "wikilink")[碘的物質](../Page/碘.md "wikilink")，[底片於顯影後會顯示成白色](../Page/底片.md "wikilink")。[光激影像板](../Page/光激影像板.md "wikilink")（image
plate）因容易數位化，在少部分醫院已取代傳統底片。另一方法是利用X光照射在特定材質上以產生[螢光](../Page/螢光.md "wikilink")，例如碘化鈉（NaI）。科學研究上，除了使用X光[CCD](../Page/CCD.md "wikilink")，也利用X光[游離](../Page/電離.md "wikilink")[氣體的特性](../Page/氣體.md "wikilink")，使用氣體游離腔做為X光強度之偵測。這些方法只能顯示出X射線的[光子密度](../Page/光子.md "wikilink")，但無法顯示出X射線的[光子能量](../Page/光子.md "wikilink")。X光光子的能量通常以[晶體使X光](../Page/結晶.md "wikilink")[繞射再依](../Page/繞射.md "wikilink")[布拉格定律计算出](../Page/布拉格定律.md "wikilink")。

## X射线衍射

在晶体学研究上，[劳厄发现了X射线通过晶体之后产生的衍射现象](../Page/劳厄.md "wikilink")，即X光衍射。[布拉格则使用布拉格定律对衍射关系进行了定量的描述](../Page/威廉·劳伦斯·布拉格.md "wikilink")。

## 医学用途

[Polydactyly_01_Lhand_AP.jpg](https://zh.wikipedia.org/wiki/File:Polydactyly_01_Lhand_AP.jpg "fig:Polydactyly_01_Lhand_AP.jpg")手掌\]\]

[伦琴发现X射线后仅仅几个月时间内](../Page/伦琴.md "wikilink")，它就被应用于[医学影像](../Page/医学影像.md "wikilink")。1896年2月，[苏格兰医生](../Page/苏格兰.md "wikilink")在[格拉斯哥](../Page/格拉斯哥.md "wikilink")设立了世界上第一个放射科。

[放射医学是](../Page/放射医学.md "wikilink")[医学的一个专门领域](../Page/医学.md "wikilink")，它使用[放射线照相术和其他技术产生](../Page/放射线照相术.md "wikilink")[诊断图像](../Page/诊断图像.md "wikilink")。的确，这可能是X射线技术应用最广泛的地方。X射线的用途主要是探测[骨骼的病变](../Page/骨骼.md "wikilink")，但对于探测[软组织的病变也相当有用](../Page/软组织.md "wikilink")。常见的例子有胸腔X射线，用来诊断肺部疾病，如[肺炎](../Page/肺炎.md "wikilink")、[肺癌或](../Page/肺癌.md "wikilink")[肺气肿](../Page/肺气肿.md "wikilink")；而腹腔X射线则用来检测[肠道梗塞](../Page/肠道梗塞.md "wikilink")，自由气体（free
air，由于內臟穿孔）及自由液体（free
fluid）。某些情況下，使用X射线诊断还存在争议，例如[结石](../Page/结石.md "wikilink")（对X射线几乎没有阻挡效应）或[肾结石](../Page/肾结石.md "wikilink")（一般可见，但并不总是可见）。

借助计算机，人们可以把不同角度的X射线影像合成成三维图像，在医学上常用的[电脑断层扫描](../Page/电脑断层扫描.md "wikilink")（CT扫描）就是基于这一原理。

X射线穿透能力与其频率有关，利用其容易被高原子序数材料吸收的特点，防护上一般可用2-3mm左右的铅板加以屏蔽。

美國曾利用X射線製造出美容除毛機並建立崔可公司\[1\]，但因為輻射使他罹患癌症，最後為避免癌症擴散，他切除了右手，而X射線的美容除毛機也導致數百萬名婦女出現皱纹、色斑、感染、溃疡，甚至皮肤癌等症狀\[2\]。

## 参考文献

### 引用

### 来源

  - [电离辐射](https://archive.is/20121130093308/http://www.envir.gov.cn/sisr/newpage/rad.htm)
  - [x射线](http://www.chinabaike.com/article/316/414/2007/20070501108197.html)

## 参见

  - [X射线晶体学](../Page/X射线晶体学.md "wikilink")
  - [X射线天文学](../Page/X射线天文学.md "wikilink")
  - [X射线光学](../Page/X射线光学.md "wikilink")
  - [X光吸收光譜](../Page/X光吸收光譜.md "wikilink")
  - [X射线机](../Page/X射线机.md "wikilink")
  - [X射线显微术](../Page/X射线显微术.md "wikilink")
  - [盖革计数器](../Page/盖革计数器.md "wikilink")
  - [N射线](../Page/N射线.md "wikilink")
  - [同步輻射光源](../Page/同步輻射光源.md "wikilink")

{{-}}

[X射线](../Category/X射线.md "wikilink")
[Category:電磁波譜](../Category/電磁波譜.md "wikilink")
[Category:IARC第1类致癌物质](../Category/IARC第1类致癌物质.md "wikilink")
[Category:医学物理](../Category/医学物理.md "wikilink")
[Category:放射成像](../Category/放射成像.md "wikilink")
[Category:乌克兰发明](../Category/乌克兰发明.md "wikilink")

1.  Herzig, Rebecca "The Matter of Race in Histories of American
    Technology" in *Technology and the African-American Experience*
2.  [80年前的X射线致癌事件](http://discovery.163.com/09/0220/11/52JELAE0000125LI.html)