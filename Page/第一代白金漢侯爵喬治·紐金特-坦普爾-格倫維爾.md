[1stMarquessOfBuckingham.jpg](https://zh.wikipedia.org/wiki/File:1stMarquessOfBuckingham.jpg "fig:1stMarquessOfBuckingham.jpg")
**乔治·纽金特-坦普尔-格倫维尔，第一代白金汉侯爵**，[KG](../Page/嘉德勳章.md "wikilink")，[PC](../Page/英國樞密院.md "wikilink")（**George
Nugent-Temple-Grenville, 1st Marquess of
Buckingham**，1753年6月17日－1813年2月11日），[英国政治家](../Page/英国.md "wikilink")，曾任外交大臣。

他的父親[喬治·格倫維爾和弟弟](../Page/喬治·格倫維爾.md "wikilink")[格倫維爾勳爵都曾任首相](../Page/威廉·溫德姆·格倫維爾，第一代格倫維爾男爵.md "wikilink")。

毕业于[伊頓公學和](../Page/伊頓公學.md "wikilink")[牛津大學](../Page/牛津大學.md "wikilink")。

## 稱呼

  - 佐治·格倫維爾 （1753年－1774年）
  - 佐治·格倫維爾 （1774年－1779年）
  - 坦普爾伯爵 （1779年－1782年）
  - 坦普爾伯爵 （1782年－1784年）
  - 白金漢侯爵 （1784年－1786年）
  - 白金漢侯爵 （1786年－1813年）

|-

[B](../Category/1753年出生.md "wikilink")
[B](../Category/1813年逝世.md "wikilink")
[B](../Category/英國政治人物.md "wikilink")
[B](../Category/英國外相.md "wikilink")
[B](../Category/大不列顛侯爵.md "wikilink")
[B](../Category/愛爾蘭伯爵.md "wikilink")
[B](../Category/嘉德騎士.md "wikilink")