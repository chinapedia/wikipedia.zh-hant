[Citistore_Tsuen_Wan_interior_2018.jpg](https://zh.wikipedia.org/wiki/File:Citistore_Tsuen_Wan_interior_2018.jpg "fig:Citistore_Tsuen_Wan_interior_2018.jpg")
[Metro_Harbour_Plaza.jpg](https://zh.wikipedia.org/wiki/File:Metro_Harbour_Plaza.jpg "fig:Metro_Harbour_Plaza.jpg")的千色Citistore\]\]
[Citistore,_Tseung_Kwan_O_Store_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Citistore,_Tseung_Kwan_O_Store_\(Hong_Kong\).jpg "fig:Citistore,_Tseung_Kwan_O_Store_(Hong_Kong).jpg")的千色Citistore\]\]
[KOLOUR_Yuen_Long_Citistore_interior1_201508.jpg](https://zh.wikipedia.org/wiki/File:KOLOUR_Yuen_Long_Citistore_interior1_201508.jpg "fig:KOLOUR_Yuen_Long_Citistore_interior1_201508.jpg")[元朗千色匯的千色Citistore](../Page/元朗千色匯.md "wikilink")\]\]

**千色Citistore**（**Citistore**），是[香港一家](../Page/香港.md "wikilink")[恒基兆業發展全資擁有的](../Page/恒基兆業發展.md "wikilink")[百貨公司](../Page/百貨公司.md "wikilink")，前名為**千色店**及**千色百貨**。

## 歷史

*千色Citistore*所有分店均設於母公司[恆基兆業旗下物業内](../Page/恆基兆業.md "wikilink")。1989年12月，[荃灣店正式開業](../Page/荃灣.md "wikilink")，為首間分店，及後主要於[新界各地區](../Page/新界.md "wikilink")（[元朗](../Page/元朗.md "wikilink")、[屯門](../Page/屯門.md "wikilink")、[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")、[將軍澳](../Page/將軍澳.md "wikilink")）開設分店，及後才於[尖沙咀](../Page/尖沙咀.md "wikilink")[美麗華商場設首間九龍分店](../Page/美麗華商場.md "wikilink")，其分店於2005年12月轉型為服裝專門店，稱為id:c，而[屯門分店亦於](../Page/屯門.md "wikilink")2006年轉型。

至2006年9月，再進駐[大角咀](../Page/大角咀.md "wikilink")[港灣豪庭廣場](../Page/港灣豪庭廣場.md "wikilink")，現為九龍唯一的分店。同時全線千色百貨換上全新的企業商標，易名為「**千色Citistore**」。

2013年3月，位於[屯門的旗下服裝專門店id](../Page/屯門.md "wikilink"):c結業，至4月重新裝修開設**千色Citistore**。

## 分店

### 千色Citistore

  - [荃灣](../Page/荃灣.md "wikilink")[荃灣千色匯II期](../Page/荃灣千色匯.md "wikilink")
    （1989年12月開業）
  - [元朗](../Page/元朗.md "wikilink")[元朗千色匯](../Page/元朗千色匯.md "wikilink")
    （1995年8月開業）
  - [馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")[新港城中心](../Page/新港城中心.md "wikilink")
    （1997年12月開業）
  - [將軍澳](../Page/將軍澳.md "wikilink")[新都城中心商場二期](../Page/新都城中心.md "wikilink")
    （2000年12月開業）
  - [大角咀](../Page/大角咀.md "wikilink")[港灣豪庭廣場](../Page/港灣豪庭廣場.md "wikilink")
    （2006年9月開業）
  - [屯門](../Page/屯門.md "wikilink")[屯門時代廣場](../Page/屯門時代廣場.md "wikilink")
    （2013年4月開業）

## 參見

  - [荃灣千色匯](../Page/荃灣千色匯.md "wikilink")
  - [元朗千色匯](../Page/元朗千色匯.md "wikilink")

## 外部連結

  - [千色Citistore](http://www.citistore.com.hk/)

[C](../Category/香港百貨公司.md "wikilink")
[C](../Category/華資百貨公司.md "wikilink")
[Category:恒基兆業](../Category/恒基兆業.md "wikilink")