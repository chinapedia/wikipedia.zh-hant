**普林斯·菲爾德**（**Prince Semien
Fielder**、）是[美國職棒大聯盟球員](../Page/美國職棒大聯盟.md "wikilink")，已退休，守備位置是一壘手。其父親Cecil
Fielder，曾效力[底特律老虎](../Page/底特律老虎.md "wikilink")、[德州遊騎兵](../Page/德克薩斯遊騎兵_\(棒球\).md "wikilink")。

## 生涯

### 大聯盟之前

當他的父親效力底特律老虎隊時，傳聞當時12歲的普林斯曾經在[老虎球場練習時打出一支全壘打](../Page/老虎球場.md "wikilink")。\[1\]2002年年選秀會中，被[密爾瓦基釀酒人隊以第一輪第](../Page/密爾瓦基釀酒人.md "wikilink")7順位選中。

### 大聯盟

2005年6月13日對[坦帕灣魔鬼魚隊的跨聯盟比賽中](../Page/坦帕灣魔鬼魚.md "wikilink")，菲爾德以[指定打擊的身份首次在大聯盟亮相](../Page/指定打擊.md "wikilink")。8月27日再次在大聯盟亮相，當時他以[代打身份上陣](../Page/代打.md "wikilink")。同年與Chanel結婚。

2006年球季，自從一壘手[萊爾·歐佛貝被交易到](../Page/萊爾·歐佛貝.md "wikilink")[多倫多藍鳥隊後](../Page/多倫多藍鳥.md "wikilink")，普林斯成為了釀酒人的先發一壘手，最初他的表現不太好，不過季中亦逐漸回復狀態，最後以28支全壘打在該季新人中排行第一。

2007年球季，普林斯首次以國聯先發一壘手入選[明星賽](../Page/美國職棒大聯盟全明星賽.md "wikilink")。9月25日擊出該球季的第50支全壘打\[2\]，成為最年輕達成[50全壘打俱樂部的球員](../Page/50全壘打俱樂部.md "wikilink")，當時只有23歲139天。
2011年球季，7月13日明星賽國聯0:1落後的情況下菲爾德敲出逆轉3分全壘打，不只幫國聯以5:1打敗美聯，且還幫國聯拿到世界大賽主場優勢。

2013年5月28日，普林斯提出与Chanel离婚\[3\]
同年底，普林斯被交易到德州遊騎兵队，对此媒体猜测家庭问题是普林斯提出要离开底特律的一个重要因素。\[4\]

### 底特律老虎

在2011年世界大賽結束以後，菲爾德成為自由球員。2012年1月26日，菲爾德和[底特律老虎簽下](../Page/底特律老虎.md "wikilink")9年2億1千4百萬美金的合約，預計將擔任一壘手\[5\]。

2012年明星赛全垒打大赛夺得冠军，其中首轮轰出7支全垒打，第二轮11支，第三轮12支，总数28支。

### 德州遊騎兵

遊騎兵以[伊恩·金斯勒交換老虎隊的普林斯](../Page/伊恩·金斯勒.md "wikilink")·菲爾德

### 退休

2016年8月11日，因為頸部傷勢宣布退休。

## 獎項

  - 明星賽最有價值球員：2011年
  - 明星賽全壘打大賽冠軍：2012年
  - 美聯東山再起獎：2015年

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  -
[Category:美國棒球選手](../Category/美國棒球選手.md "wikilink")
[Category:密爾瓦基釀酒人隊球員](../Category/密爾瓦基釀酒人隊球員.md "wikilink")
[Category:底特律老虎隊球員](../Category/底特律老虎隊球員.md "wikilink")
[Category:國家聯盟全壘打王](../Category/國家聯盟全壘打王.md "wikilink")
[Category:國家聯盟打點王](../Category/國家聯盟打點王.md "wikilink")
[Category:漢克阿倫獎得主](../Category/漢克阿倫獎得主.md "wikilink")

1.  [1](http://www.usatoday.com/sports/baseball/nl/brewers/2006-03-12-preview_x.htm)
2.  [Prince Fielder Reaches 50
    HRs](http://sports.aol.com/story/_a/prince-fielder-reaches-50-hrs/n20070926050209990002)AOL
    Sports
3.  [2](http://www.usatoday.com/story/sports/mlb/tigers/2013/08/15/prince-fielder-files-divorce/2660063/)
4.  [3](http://deadspin.com/denny-mclain-says-domestic-issues-drove-prince-fielde-1469185897)
5.  ["Prince Fielder signs with the
    Tigers".](http://eye-on-baseball.blogs.cbssports.com/mcc/blogs/entry/22297882/34555236?ttag=gen10_on_all_fb_na_txt_0001)
    Cbssports.com. January 23, 2012.