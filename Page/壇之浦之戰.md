[壇ノ浦の戦い.png](https://zh.wikipedia.org/wiki/File:壇ノ浦の戦い.png "fig:壇ノ浦の戦い.png")

**壇之浦之戰**（）為[日本](../Page/日本.md "wikilink")[平安時代末期](../Page/平安時代.md "wikilink")[1185年](../Page/1185年.md "wikilink")[4月25日](../Page/4月25日.md "wikilink")（[元曆](../Page/元曆.md "wikilink")2年/[壽永](../Page/壽永.md "wikilink")4年[3月24日](../Page/三月廿四.md "wikilink")）於日本[長門國](../Page/長門國.md "wikilink")[壇之浦](../Page/壇之浦.md "wikilink")（位於今[山口縣](../Page/山口縣.md "wikilink")[下關市](../Page/下關市.md "wikilink")）發生的一場戰役，為[源平合戰的最後一戰](../Page/源平合戰.md "wikilink")。

## 背景

1183年（壽永2年）6月[篠原之戰](../Page/篠原之戰.md "wikilink")，[平家被](../Page/平家.md "wikilink")[源義仲打敗](../Page/源義仲.md "wikilink")，後於同年7月帶著[安德天皇與](../Page/安德天皇.md "wikilink")[三神器逃離](../Page/三神器.md "wikilink")[京都](../Page/京都.md "wikilink")；隨後，平家趁著源義仲與其堂兄[源賴朝之間的對立](../Page/源賴朝.md "wikilink")，奪回[攝津國](../Page/攝津國.md "wikilink")[福原京](../Page/福原京.md "wikilink")（位於今[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")）。然而，1184年（壽永3年）2月[一之谷之戰](../Page/一之谷之戰.md "wikilink")，平家又被打敗而逃到[讚岐國](../Page/讚岐國.md "wikilink")[屋島](../Page/屋島.md "wikilink")（位於今[香川縣](../Page/香川縣.md "wikilink")[高松市](../Page/高松市.md "wikilink")）與[長門國](../Page/長門國.md "wikilink")[彥島](../Page/彥島.md "wikilink")（位於今[山口縣](../Page/山口縣.md "wikilink")[下關市](../Page/下關市.md "wikilink")）。

源賴朝命其弟[源範賴率](../Page/源範賴.md "wikilink")3萬騎兵進軍[山陽道](../Page/山陽道.md "wikilink")，以渡過[九州](../Page/九州.md "wikilink")，阻斷平家軍後退的道路。不過，因為源範賴軍的兵糧不足，且平家軍擁有優勢的[水軍抵抗](../Page/水軍.md "wikilink")，使得進軍無法順利進行。[源義經見到此一情況](../Page/源義經.md "wikilink")，向[後白河法皇取得了追討平家的認可](../Page/後白河法皇.md "wikilink")，並壓制了京都內[公家的反對意見](../Page/公家.md "wikilink")，向屋島進軍。1185年（元曆2年/壽永4年）2月[屋島之戰](../Page/屋島之戰.md "wikilink")，義經奇襲並攻下了屋島，平家總大將[平宗盛帶著安德天皇在](../Page/平宗盛.md "wikilink")[瀨戶內海輾轉逃亡](../Page/瀨戶內海.md "wikilink")，最後逃到了彥島。

同時，源範賴成功地調到了兵糧與軍船，並渡過九州。於[葦屋浦之戰](../Page/葦屋浦之戰.md "wikilink")，源範賴軍大破當地的平家軍，成功阻斷平家軍後退的道路。至此，平家軍已被孤立於彥島之中。

## 過程

[屋島之戰後](../Page/屋島之戰.md "wikilink")，平家撤退到[長門的](../Page/長門.md "wikilink")[彥島據守](../Page/彥島.md "wikilink")，而[源範賴和](../Page/源範賴.md "wikilink")[源義經亦在對岸佈陣對峙](../Page/源義經.md "wikilink")。雙方已有海戰的覺悟，開始糾集戰船，平家500艘，源氏840艘（《[吾妻鏡](../Page/吾妻鏡.md "wikilink")》版本）。

1185年3月24日清晨6時許，在[關門海峽的](../Page/關門海峽.md "wikilink")[壇之浦](../Page/壇之浦.md "wikilink")（）開戰，由平家主動展開攻擊。由於平家擅於海戰，而且[潮流對平家有利](../Page/潮流.md "wikilink")，艦艇機動靈活，所以一開始平家即佔了上風。相反地，逆流進軍的源氏艦艇如陷泥沼，成為平家箭陣的活靶。此時[源義經下令集中狙殺平家的水手及舵手](../Page/源義經.md "wikilink")，失去機動能力的平家艦隊反而比源氏更加動彈不得，正午過後，潮流改變，源氏順勢接近登船，展開白刃血戰，戰情也隨之逆轉。激戰過後，眼見大勢已去，[平資盛](../Page/平資盛.md "wikilink")、[平有盛](../Page/平有盛.md "wikilink")、[平經盛](../Page/平經盛.md "wikilink")、[平教盛](../Page/平教盛.md "wikilink")、[平行盛等大將陸續投海身亡](../Page/平行盛.md "wikilink")。平家領袖[平宗盛及子](../Page/平宗盛.md "wikilink")[平清宗](../Page/平清宗.md "wikilink")、妹[平德子雖然企圖跳海自盡](../Page/平德子.md "wikilink")，但為源氏士兵所救。而年僅8歲的平家血脈[安德天皇](../Page/安德天皇.md "wikilink")（平德子所生）則由外祖母[二位尼挾抱跳海身亡](../Page/二位尼.md "wikilink")。据说[三神器中的](../Page/三神器.md "wikilink")[天叢雲劍亦在此次战役中沉入大海](../Page/天叢雲劍.md "wikilink")。

值得一提的是，傳說曾在屋島之戰中射死源義經愛將[佐藤繼信的平家猛將](../Page/佐藤繼信.md "wikilink")[能登守平教經](../Page/平教經.md "wikilink")，知戰不能勝但也要和源義經同歸於盡。便以義經為第一目標，找到機會欲出手時，義經身輕如燕從船上跳到另一船。欲追之，義經又飛越至另一船，如此連續飛跳八船（即義經著名的「****」-
）。平教經啞然，最後挾著兩個源軍勇士跳海，壯絕而亡。另外，平家猛將[平知盛為免不能絕命](../Page/平知盛.md "wikilink")，遂著重甲，負[錨](../Page/錨.md "wikilink")[碇](../Page/碇.md "wikilink")，投水而死。日暮時分，壇之浦之戰結束，平家滅亡。

## 相關藝術作品

日本薩摩琵琶演奏家、說唱家鶴田錦史 (1911-1995)
曾根據壇之浦之戰寫成琵琶說唱作品「」，由水木洋子作詞，刻劃平源兩家最後戰役的慘烈景況。

[Category:源平合戰](../Category/源平合戰.md "wikilink")
[Category:長門國](../Category/長門國.md "wikilink")
[Category:下關市](../Category/下關市.md "wikilink")
[Category:日本戰國水軍](../Category/日本戰國水軍.md "wikilink")
[Category:日本海戰](../Category/日本海戰.md "wikilink")
[Category:瀨戶內海](../Category/瀨戶內海.md "wikilink")
[Category:1185年](../Category/1185年.md "wikilink")