《**东宫西宫**》（
）是中國1996年的一套[同志電影](../Page/同志電影.md "wikilink")，由[張元導演](../Page/張元_\(導演\).md "wikilink")。電影以1991年在北京一次假借健康調查為名而清算市內同性戀人士的事件作為藍本，並由當時嶄露頭角的新星[胡軍](../Page/胡軍.md "wikilink")、[趙薇等演出](../Page/趙薇.md "wikilink")。本片獲當年阿根廷Mardel
Plata电影节的「最佳导演」、「最佳编剧」奖項。

## 故事大綱

故事以[北京故宮的某公园為背景](../Page/北京故宮.md "wikilink")，那裡是當地同性恋者的幽会场所。為打擊這些人在公园幽會，派出所會派警察到公園巡查，並遞捕可疑人士調查。故事從這裡開始……

## 角色列表

|                                  |        |        |
| -------------------------------- | ------ | ------ |
| **演员**                           | **角色** | **备注** |
| [司汗](../Page/司汗.md "wikilink")   | 阿蘭     |        |
| [胡軍](../Page/胡軍.md "wikilink")   | 小史     |        |
| [劉欲曉](../Page/劉欲曉.md "wikilink") | 女贼     |        |
| [馬雯](../Page/馬雯.md "wikilink")   | 衙役     |        |
| [趙薇](../Page/趙薇.md "wikilink")   | "公共汽車" |        |
| [葉靜](../Page/葉靜.md "wikilink")   | 阿蘭（少年） |        |
| [王全](../Page/王全.md "wikilink")   | 阿蘭（童年） |        |
| [呂蓉](../Page/呂蓉.md "wikilink")   | 阿蘭的母親  |        |

## 外部链接

  -
  - [东宫西宫](http://www.dianying.com/jt/title.php?titleid=dgx1996)在中文电影资料库

[Category:1996年電影](../Category/1996年電影.md "wikilink")
[Category:1990年代劇情片](../Category/1990年代劇情片.md "wikilink")
[D东](../Category/中國劇情片.md "wikilink")
[Category:中國LGBT相關電影](../Category/中國LGBT相關電影.md "wikilink")
[Category:北京背景电影](../Category/北京背景电影.md "wikilink")
[Category:中國大陸禁片](../Category/中國大陸禁片.md "wikilink")