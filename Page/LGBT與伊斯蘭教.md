[LGBT_Ensign_of_Pakistan.svg](https://zh.wikipedia.org/wiki/File:LGBT_Ensign_of_Pakistan.svg "fig:LGBT_Ensign_of_Pakistan.svg")
[伊斯蘭教的](../Page/伊斯蘭教.md "wikilink")[LGBT概念受到](../Page/LGBT.md "wikilink")[穆斯林的文化法律相連的歷史及國家背景影響](../Page/穆斯林.md "wikilink")，特別是[古蘭經和集錄](../Page/古蘭經.md "wikilink")[先知](../Page/先知.md "wikilink")[穆罕默德言行的](../Page/穆罕默德.md "wikilink")[聖訓](../Page/聖訓.md "wikilink")。

古蘭經中記載著[魯特的故事](../Page/伊斯蘭教中的羅得.md "wikilink")（亦被稱為[所多瑪與蛾摩拉](../Page/所多瑪與蛾摩拉.md "wikilink")），多數教派將該故事視為古蘭經禁止同性性行為的依據。

伊斯蘭教的著名學者[謝赫及沙非](../Page/謝赫伊斯蘭.md "wikilink")（）規定禁止同性之間的性行為，違反的人會被處以[極刑](../Page/極刑.md "wikilink")。\[1\]大部分以穆斯林為主要人口的國家都用法律懲罰同性之間的性行為。在伊斯蘭政權國家如[伊朗](../Page/伊朗.md "wikilink")、[毛里塔尼亞](../Page/毛里塔尼亞.md "wikilink")、[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")、[蘇丹和](../Page/蘇丹.md "wikilink")[也門](../Page/也門.md "wikilink")，會被處以死刑。而[尼日利亞及](../Page/尼日利亞.md "wikilink")[索馬里的部分地區亦執行此法](../Page/索馬里.md "wikilink")。\[2\]而[雞姦的懲罰於不同的法律下有不同處理手法](../Page/雞姦.md "wikilink")，部分人會被處以死刑，而部分人則會被[監禁](../Page/監禁.md "wikilink")。而部分較[世俗的國家雖然以穆斯林為主](../Page/世俗.md "wikilink")，但不依上述手法處理。如[印度尼西亞](../Page/印度尼西亞.md "wikilink")，\[3\][約旦](../Page/約旦.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[哈薩克](../Page/哈薩克.md "wikilink")、[塔吉克](../Page/塔吉克.md "wikilink")、[吉爾吉斯](../Page/吉爾吉斯.md "wikilink")、[亞塞拜然](../Page/亞塞拜然.md "wikilink")、[阿爾巴尼亞](../Page/阿爾巴尼亞.md "wikilink")、[科索沃](../Page/科索沃.md "wikilink")、[馬利](../Page/馬利.md "wikilink")、[尼日及](../Page/尼日.md "wikilink")[幾內亞比索等](../Page/幾內亞比索.md "wikilink")。

與嚴格的法律相反，[中世紀以降的穆斯林](../Page/中世紀.md "wikilink")[詩歌及](../Page/詩歌.md "wikilink")[文學卻反映出同性相愛的傾向](../Page/文學.md "wikilink")，並有描寫男性之間的愛。\[4\]

## 伊斯蘭法律

### 古蘭經

根據傳統，[伊斯蘭教法或其他傳統伊斯蘭法律都是根據古蘭經及聖訓去撰寫的](../Page/伊斯蘭教法.md "wikilink")。古蘭經是真主阿拉的啟示，而聖訓則是穆罕默德的訓話。\[5\]

古蘭經是伊斯蘭最重要的經典，作為神的啟示而被穆斯林人景仰。\[6\]根據古蘭經記載關於[所多瑪與蛾摩拉](../Page/所多瑪與蛾摩拉.md "wikilink")，以及先知[魯特的篇章](../Page/魯特.md "wikilink")（參考古蘭經
7:80-84, 11:77-83, 21:74, 22:43, 26:165-175, 27:56-59, 及
29:27-33）\[7\]\[8\]

阿拉伯詞語liwat（意指*同性性行為*）及luti（意指*進行同性性行為*）都是從魯特（lut）而來。\[9\]
。多數教派將該故事視為古蘭經禁止同性性行為的依據，但在古蘭經中，沒有處罰同性性行為的罰則，因此各教派自行推導嚴厲程度不一的[伊斯蘭教法](../Page/伊斯蘭教法.md "wikilink")，來決定處罰方式。

當代學者Scott
Kugle主張[魯特傳說並非著重在性行為](../Page/魯特.md "wikilink")，而是部落的背信和對羅得預言職分的拒絕。相關經文指涉的亦是非合意的強暴性行為，因此古蘭經並不禁止同性之間的性行為\[10\]。在經文的細節上，Scott
Kugle認為(7:80–84)和(26:165-166)的經文係指，這些意欲強行侵犯陌生人的男性，已經有了伴侶（azwaj），真主為他們所造的妻子。因此，這裡的道德議題是他們無視其已有配偶的事實，而想向陌生人強行求歡\[11\]。

在所多瑪的毀滅中，整個城市都因為犯罪而被摧毀了，這包括魯特的妻子、其它的女性和小孩，Scott
Kugle因此主張，將城市所犯的罪歸咎為「男同性性行為」並不合理。他也引用兩段聖訓，當中提到所多瑪的毀滅是因為，他們沒有在如廁或性事後清潔自身、他們不分享食物、他們貪婪又吝嗇\[12\]。

### 聖訓

### 法理

## 現代教法

## 伊斯蘭教的同性戀史

### 中世紀時代

### 文學

### 現代

## 参考文献

## 外部連結

### 文章

  - 游謙：[伊斯蘭對同性戀的態度 ---
    以安華事件為例](http://www.islambook.net/xueshu/list.asp?id=2150)
  - [同志阿訇有話說：穆斯林與同性戀傾向並不衝突](https://web.archive.org/web/20121113140912/http://www.danlan.org/disparticle_33593.htm)
  - [逃過生父威脅及IS處死 「我是伊拉克的同性戀者」](http://www.storm.mg/article/59548)
  - [伊斯蘭世界天生 「反同」？歷史並非如此](http://cnpolitics.org/2016/06/muslim-world/)
  - [The secret mosques opening their doors to gay
    Muslims](http://www.abc.net.au/news/2016-04-29/secret-mosques-doors-lgbti-muslims/7341142)
  - Mustafa Akyol：[What Does Islam Say About Being
    Gay?](http://www.nytimes.com/2015/07/29/opinion/mustafa-akyol-what-does-islam-say-about-being-gay.html?_r=0)
  - Muhsin Hendricks：[Islamic Texts: A Source for Acceptance of Queer
    Individuals into Mainstream Muslim
    Society](http://www.equalrightstrust.org/ertdocumentbank/muhsin.pdf)
  - Scott Siraj al-Haqq Kugle：[Sexual diversity in Islam: Is there room
    in Islam for lesbian, gay, bisexual and transgender
    Muslims?](http://www.mpvusa.org/sexuality-diversity)
  - [DAAYIEE'S PLACE OF INNER
    PEACE](http://daayieesplaceofinnerpeace.com/)

### 接納LGBT的穆斯林團體

  - [Inner Circle](http://theinnercircle.org.za/)（南非）
  - [Muslim Alliance for Sexual and Gender
    Diversity](http://www.muslimalliance.org/)（美國）
  - [Muslims for Progressive Values](http://www.mpvusa.org/)（美國）

[\*](../Category/LGBT与伊斯兰教.md "wikilink")

1.  [Homosexuality and Lesbianism: Sexual
    Perversions](http://www.islamonline.net/servlet/Satellite?pagename=IslamOnline-English-Ask_Scholar/FatwaE/FatwaE&cid=1119503545556)
     Fatwa on Homosexuality from IslamOnline.net
2.  [ILGA: Lesbian and Gay Rights in the World
    (2009)](http://www.ilga.org/Statehomophobia/ILGA_map_2009_A4.pdf) .
3.
4.
5.
6.
7.  Duran (1993) p. 179
8.  Kligerman (2007) pp. 53-54
9.  Wayne Dynes, Encyclopaedia of Homosexuality, New York, 1990
10.
11.
12.