**魏斯·安德森**（，），[美國](../Page/美國.md "wikilink")[電影導演](../Page/電影導演.md "wikilink")、编剧和监制。
在2015年，他为意大利奢侈品公司[普拉達设计了](../Page/普拉達.md "wikilink")[普拉达咖啡酒吧](../Page/普拉达咖啡酒吧.md "wikilink")
（, Largo Isarco, 2, Milano)，就像在五十年代的电影里，回想起那个时代米兰设计宫殿的标志性设计。

## 生平

安德森全名Wesley Wales
Anderson，1969年5月1日生於美國[德克薩斯州](../Page/德克薩斯州.md "wikilink")[休士頓](../Page/休士頓.md "wikilink")。父親Melver
Leonard在廣告界工作，在[休士頓擁有一間公關公司](../Page/休士頓.md "wikilink")；母親Texas
Ann（閨姓Burroughs）曾是[考古學家](../Page/考古學家.md "wikilink")，現時當[地產經紀](../Page/地產經紀.md "wikilink")，《[天才一族](../Page/天才一族.md "wikilink")》中母親的角色即以她為藍本；兩人育有三子，安德森在三兄弟中排第二。

安德森大學畢業於[德克薩斯州大學奧斯汀分校](../Page/德克薩斯州大學奧斯汀分校.md "wikilink")，主修[哲學](../Page/哲學.md "wikilink")，期間認識了[歐文·威爾森](../Page/歐文·威爾森.md "wikilink")，而威爾森亦屢次在安德森的電影中演出。

安德森本來只是拍一些[獨立小品電影](../Page/獨立電影.md "wikilink")，卻因《[天才一族](../Page/天才一族.md "wikilink")》入圍[奧斯卡](../Page/奧斯卡金像獎.md "wikilink")[最佳原創劇本獎而一鳴驚人](../Page/奧斯卡最佳原創劇本獎.md "wikilink")。雖然如此，安德森並沒讓作品商業化，仍然搞冷調幽默。外間評他為新一代的[吉姆·賈木許](../Page/吉姆·賈木許.md "wikilink")（Jim
Jarmusch），有不少大牌明星願意為他降價演出。

他的《海海人生》是向法國海洋學者[雅克-伊夫·库斯托](../Page/雅克-伊夫·库斯托.md "wikilink")（Jacques-Yves
Cousteau）\[1\]致敬的影片：電影中的主角Steve
Zissou（[比爾·莫瑞飾](../Page/比爾·莫瑞.md "wikilink")）亦是海洋學家，更如庫斯托一樣，和他的組員頭上一直戴著紅色扁帽。電影劇本由魏斯·安德森和《[親情難捨](../Page/親情難捨.md "wikilink")》（The
Squid and the Whale）導演[諾亞·波拜克](../Page/諾亞·波拜克.md "wikilink")（Noah
Baumbach）聯合撰寫。

2014年作品《[布达佩斯大饭店](../Page/布达佩斯大饭店.md "wikilink")》成为[第64届柏林电影节开幕片](../Page/第64届柏林电影节.md "wikilink")\[2\]并赢得评审团大奖。获得[第87届奥斯卡金像奖最佳影片](../Page/第87届奥斯卡金像奖.md "wikilink")、最佳导演、最佳原创剧本等九项提名。

2018年，安德遜執導的第九部長片作品《犬之島》，而他亦多番公開提到，此片很大部分受到黑澤明的啟發，片中有很多地方都有對日本電影致敬。而片中的狗仔性格設定，令人想起日本武士精神。而片中的劇情及對白不乏對現今社會的嘲諷\[3\]。

2018年8月，傳出 Wes Anderson
將會開拍下一套電影，亦是他第十部長篇故事電影。電影將會選址法國西南部夏朗德地區的首府昂古萊姆，計劃於下年二月開始進行為期四個月的拍攝，場景的設定就是二次大戰後的時期\[4\]。

## 導演作品

  - 1996年: 《[脫線沖天炮](../Page/脫線沖天炮.md "wikilink")》（*Bottle Rocket*）
  - 1998年: 《[都是愛情惹的禍](../Page/都是愛情惹的禍.md "wikilink")》（*Rushmore*）
  - 2001年: 《[天才一族](../Page/天才一族.md "wikilink")》（*The Royal Tenenbaums*）
      - 提名-[奧斯卡最佳原創劇本獎](../Page/奧斯卡最佳原創劇本獎.md "wikilink")
  - 2004年: 《海海人生》（*The Life Aquatic with Steve Zissou*）
  - 2007年: 《[穿越大吉岭](../Page/穿越大吉岭.md "wikilink")》（*The Darjeeling
    Limited*）
  - 2009年: 《[超級狐狸先生](../Page/超級狐狸先生.md "wikilink")》（*Fantastic Mr. Fox*）
      - 提名-[奧斯卡最佳動畫片獎](../Page/奧斯卡最佳動畫片獎.md "wikilink")
  - 2012年: 《[月升王国](../Page/月升王国.md "wikilink")》（*Moonrise Kingdom*）
      - 提名-[奧斯卡最佳原創劇本獎](../Page/奧斯卡最佳原創劇本獎.md "wikilink")
  - 2014年: 《[歡迎來到布達佩斯大飯店](../Page/歡迎來到布達佩斯大飯店.md "wikilink")》（*The Grand
    Budapest Hotel*）
      - 提名-[奧斯卡最佳影片獎](../Page/奧斯卡最佳影片獎.md "wikilink")
      - 提名-[奧斯卡最佳導演獎](../Page/奧斯卡最佳導演獎.md "wikilink")
      - 提名-[奧斯卡最佳原創劇本獎](../Page/奧斯卡最佳原創劇本獎.md "wikilink")
      - 提名-[奧斯卡最佳攝影獎](../Page/奧斯卡最佳攝影獎.md "wikilink")
      - 提名-[奧斯卡最佳剪輯獎](../Page/奧斯卡最佳剪輯獎.md "wikilink")
      - [奧斯卡最佳配樂獎](../Page/奧斯卡最佳配樂獎.md "wikilink")
      - [奧斯卡最佳美術指導獎](../Page/奧斯卡最佳美術指導獎.md "wikilink")
      - [奧斯卡最佳服裝設計獎](../Page/奧斯卡最佳服裝設計獎.md "wikilink")
      - [奧斯卡最佳化妝與髮型獎](../Page/奧斯卡最佳化妝與髮型獎.md "wikilink")
  - 2018年: 《[犬之島](../Page/犬之島.md "wikilink")》（*Isle of Dogs*）
      - 提名-[奧斯卡最佳動畫片獎](../Page/奧斯卡最佳動畫片獎.md "wikilink")

## 參考

## 外部連結

  -
  - [Wes
    Anderson](http://www.unsungfilms.com/914/wesley-wales-anderson/) at
    [Unsung Films](http://www.unsungfilms.com/)

  -
  - [Tête-à-Tête with Nic
    Harcourt](http://www.latimesmagazine.com/2010/02/ttette-wes-anderson.html)
    at *Los Angeles Times Magazine*

  - ["Into The
    Deep"](http://www.guardian.co.uk/weekend/story/0,,1409900,00.html),
    in-depth Anderson profile at *[The
    Guardian](../Page/The_Guardian.md "wikilink")* 12 February 2005

  - ["Wes Anderson"](http://www.esquire.com/features/wes-anderson-0300),
    brief profile by Martin Scorsese. *Esquire*.

  - [Interview with Wes Anderson for The Darjeeling Limited at
    Ioncinema.com](http://www.ioncinema.com/news.php?nid=2212/)

  - [Wes Anderson
    Interview](http://www.cbc.ca/thehour/video.php?id=1772) on *[The
    Hour](../Page/The_Hour_\(Canadian_TV_series\).md "wikilink")* with
    [George
    Stroumboulopoulos](../Page/George_Stroumboulopoulos.md "wikilink")
    (Video)

  - ["Notes on
    Quirky"](http://www2.warwick.ac.uk/fac/arts/film/movie/contents/notes_on_quirky.pdf)
    Movie: A Journal of Film Criticism, No.1, 2010

  - [韦斯·安德森完全解码手册](http://news.mtime.com/2014/06/30/1528844.html#p1)

[Category:美国电影导演](../Category/美国电影导演.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:美国电影监制](../Category/美国电影监制.md "wikilink")
[Category:休斯敦人](../Category/休斯敦人.md "wikilink")
[Category:在法國的美國人](../Category/在法國的美國人.md "wikilink")
[Category:德州大学奥斯汀分校校友](../Category/德州大学奥斯汀分校校友.md "wikilink")
[Category:独立精神奖获得者](../Category/独立精神奖获得者.md "wikilink")
[Category:英国电影学院奖最佳原创剧本获得者](../Category/英国电影学院奖最佳原创剧本获得者.md "wikilink")
[Category:柏林影展獲獎者](../Category/柏林影展獲獎者.md "wikilink")

1.  庫斯托和[路易·馬盧](../Page/路易·馬盧.md "wikilink")（Louis
    Malle）合導的影片《[沈靜的世界](../Page/沈靜的世界.md "wikilink")》（Le
    Monde du
    silence）獲1956年[坎城影展](../Page/坎城影展.md "wikilink")[金棕櫚獎](../Page/金棕櫚獎.md "wikilink")。
2.
3.  [Wes
    Anderson向黑澤明致敬　《犬之島》定格動畫耗心力製500隻狗](https://bka.mpweekly.com/film-music/film/%E5%A4%A7%E8%A9%B1%E8%A5%BF%E6%B4%8B/20180419-109833)
4.  [WES ANDERSON 選址法國 ANGOULÊME
    開拍最新一部電影｜一睹過往經典作品那風光如畫的實景](https://www.mings-fashion.com/wes-anderson-angouleme-%E9%9B%BB%E5%BD%B1-197093/)