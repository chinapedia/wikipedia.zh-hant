**Synaptics**（）是一個為大部份主要電腦及筆記型電腦公司提供[觸控板的](../Page/觸控板.md "wikilink")[OEM供應商](../Page/OEM.md "wikilink")，其中包括了[華碩](../Page/華碩.md "wikilink")、[宏碁](../Page/宏碁.md "wikilink")、[戴爾](../Page/戴爾.md "wikilink")、[惠普](../Page/惠普.md "wikilink")、[索尼](../Page/索尼.md "wikilink")、[東芝](../Page/東芝.md "wikilink")、、[國際商業機器](../Page/國際商業機器.md "wikilink")、[聯想集團與](../Page/聯想集團.md "wikilink")[三星電子等](../Page/三星電子.md "wikilink")。Synaptics於1986年由Carver
Mead與Federico
Faggin創立，總部位於[加州](../Page/加州.md "wikilink")[聖塔克拉拉](../Page/聖塔克拉拉_\(美國加州\).md "wikilink")。

Synaptics公司從事於為多種行動計算、通訊與其他電子設備的互動開發與供應用戶界面方案，並供應包括[專用集成電路](../Page/專用集成電路.md "wikilink")、[韌體](../Page/韌體.md "wikilink")、[軟體](../Page/軟體.md "wikilink")、[模式識別與觸感技術等多項技術](../Page/模式識別.md "wikilink")。

該公司成功為多個MP3播放器建立界面方案，包括了某些版本的[iPod與](../Page/iPod.md "wikilink")[iPod
mini和用於後期](../Page/iPod_mini.md "wikilink")[Creative
Zen裝置的觸控板](../Page/Creative_Zen.md "wikilink")；以及與[蘋果公司的](../Page/蘋果公司.md "wikilink")[iPhone不同的觸控式螢幕概念手機Onyx](../Page/iPhone.md "wikilink")\[1\]。

2018年初，與[中國大陸](../Page/中國大陸.md "wikilink")[手機廠商](../Page/手機.md "wikilink")[Vivo一起推出了全球第一款](../Page/Vivo.md "wikilink")「屏幕下指紋識別」手機Vivo
X20 Plus\[2\]。

## 參考來源

## 外部連結

  - [官方網站](http://www.synaptics.com)

[Category:美國硬體公司](../Category/美國硬體公司.md "wikilink")
[Category:電腦外設生產商](../Category/電腦外設生產商.md "wikilink")
[S](../Category/聖塔克拉拉公司.md "wikilink")

1.  [The Onyx phone is packed with
    features](https://archive.is/20130425114220/http://news.com.com/1606-12994_3-6150132.html?tag=ne.video.6150150)
2.