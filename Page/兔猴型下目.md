**兔猴型下目**（Adapiformes），又名**兔猴下目**，是已[滅絕的原始](../Page/滅絕.md "wikilink")[靈長目](../Page/靈長目.md "wikilink")。牠們的近親有[狐猴](../Page/狐猴.md "wikilink")、[蜂猴亞科及](../Page/蜂猴亞科.md "wikilink")[嬰猴科](../Page/嬰猴科.md "wikilink")。兔猴型下目主要出沒在北方大陸，最南至[北非及](../Page/北非.md "wikilink")[亞洲的](../Page/亞洲.md "wikilink")[熱帶地區](../Page/熱帶.md "wikilink")。牠們生存於[始新世至](../Page/始新世.md "wikilink")[中新世](../Page/中新世.md "wikilink")。大部份兔猴型下目都像現今的狐猴。

## 分類

  - [假熊猴科](../Page/假熊猴科.md "wikilink")（Notharctidae）
      - Cercamoniinae
          - *Anchomomys*
          - *Buxella*
          - *Donrussellia*
          - *Europolemur*
          - *Mahgarita*
          - *Periconodon*
          - [原懶猴屬](../Page/原懶猴屬.md "wikilink")（*Pronycticebus*）
      - [假熊猴亞科](../Page/假熊猴亞科.md "wikilink")（Notharctinae）
          - *Cantius*
          - *Copelemur*
          - *Hesperolemur*
          - [假熊猴屬](../Page/假熊猴屬.md "wikilink")（*Notharctus*）
          - *Pelycodus*
          - *Smilodectes*
  - [西瓦兔猴科](../Page/西瓦兔猴科.md "wikilink")（Sivaladapidae）
      - *Siamoadapis*
      - [西瓦兔猴屬](../Page/西瓦兔猴屬.md "wikilink")（*Sivaladapis*）
  - [兔猴科](../Page/兔猴科.md "wikilink")（Adapidae）
      - [兔猴屬](../Page/兔猴屬.md "wikilink")（*Adapis*）
      - [擬兔猴屬](../Page/擬兔猴屬.md "wikilink")（*Adapoides*）
      - *Leptadapis*
      - [高帝納猴屬](../Page/高帝納猴屬.md "wikilink")（*Godinotia*）
  - 位置不明
      - *Omanodon*
      - *Muangthanhinius*

## 外部連結

  - [Mikko's Phylogeny
    Archive](https://web.archive.org/web/20080122062511/http://www.fmnh.helsinki.fi/users/haaramo/Metazoa/Deuterostoma/Chordata/Synapsida/Eutheria/Primates/strepsirrhini.htm)

[Category:原猴亚目](../Category/原猴亚目.md "wikilink")
[Category:兔猴型下目](../Category/兔猴型下目.md "wikilink")