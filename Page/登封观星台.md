**登封觀星台**（），也稱為**告成天文台**（），最早是[周公建造的觀星台](../Page/周朝.md "wikilink")，位於中國[河南省](../Page/河南省.md "wikilink")[登封](../Page/登封.md "wikilink")，鄰近[告成縣](../Page/告成縣.md "wikilink")，是[世界遺產之一](../Page/世界遺產.md "wikilink")。這個觀星台有悠久的歷史，從[西周開始到](../Page/西周.md "wikilink")[元朝的初期](../Page/元朝.md "wikilink")。元朝的[天文學家](../Page/天文學家.md "wikilink")[郭守敬在AD](../Page/郭守敬.md "wikilink")1276年將它重建成一個巨大的天文台，是[中国现存最早的古代](../Page/中国.md "wikilink")[天文台](../Page/天文台.md "wikilink")\[1\]。附近還有建於[唐朝](../Page/唐朝.md "wikilink")[開元十一年](../Page/開元十一年.md "wikilink")（AD723年）的[周公測景台](../Page/周公測景台.md "wikilink")。

## 西周

據說[周公在這個地方樹立了](../Page/周公.md "wikilink")[圭表](../Page/圭表.md "wikilink")（使用陰影或[晷針測量太陽位置](../Page/晷針.md "wikilink")），他在數學、天文與星占的成就，記載在[周髀算經](../Page/周髀算經.md "wikilink")。

## 唐朝

唐朝的天文學家[一行在國內各地建立了](../Page/一行.md "wikilink")20餘個標準化的晷針來測量不同地點的[均時差](../Page/均時差.md "wikilink")。後續的劉焯從AD604年起，在在現今的[本初子午線以東](../Page/本初子午線.md "wikilink")114°
上，沿著子午線從中亞到越南建立的10個測站，在理想的球面上測量地球這個球體。其中一個測站就建在現今的告成鎮附近，而觀測的成果被用來創建[大衍曆](../Page/大衍曆.md "wikilink")。

在觀星台的南方可以找到由一行建造的石龜圖，根據《[周禮](../Page/周禮.md "wikilink")》的記載，這個地方是地球的中心。

## 元朝

巨大的觀星台始建於1276年，是[元朝初期的君王](../Page/元朝.md "wikilink")[忽必烈命令](../Page/忽必烈.md "wikilink")[郭守敬和](../Page/郭守敬.md "wikilink")[王恂](../Page/王恂_\(元朝\).md "wikilink")（1235年-1291年）建造的，用來觀測太陽和星星，以紀錄時間。

觀星台由青石和磚塊建造，分成兩個部分：主體和青石塊的[圭](../Page/圭表.md "wikilink")，也稱為量天尺。主體是高9.46米的平台，加上建於其上兩間小閣樓的高度是12.62米，在兩間閣樓之間的空隙水平的安裝了一根晷針。兩間閣樓的一间放[漏壶](../Page/漏壶.md "wikilink")，另一间放[浑仪](../Page/浑仪.md "wikilink")。作為量天尺的圭長31.19米，寬0.53，由36塊方形的青石組成，並有兩條水道以校準它的水平。量天尺的位置常向正北，就如同子午線的方向。在測量時，晷針的投影落在量天尺上，使用安置在水道上移動的[景符](../Page/圭表#郭守敬的改良.md "wikilink")（利用針孔成像使晷針影子清晰的儀器）測量投影的位置，可精確至2mm。在冬至，正午時晷針的影子落在靠近量天尺尾端的位置。通过测量一年当中影子长度的变化，可以确定一[年的长度](../Page/年.md "wikilink")。\[2\]

這種非常準確的觀測維新的[授時曆提供服務](../Page/授時曆.md "wikilink")，從1281年起共使用了364年。它的[回歸年年長度是](../Page/回歸年.md "wikilink")365日5小時49分20秒，與現行[格里曆的數值相同](../Page/格里曆.md "wikilink")，但是早300年就得到這個數值。

在1787年，[拉普拉斯利用這些測量資料來檢查黃赤交角和地球軌道離心率的變化](../Page/拉普拉斯.md "wikilink")。

它也是元朝初期建立的27座觀星台中的第一座。

## 現況

观星台是由台身和量天尺组成的一座青砖石结构建筑，台身状如覆斗，其作用是
“昼参日影，夜观极星，以正朝夕”。观星台不仅保存了中国古代[圭表测影的实物](../Page/圭表.md "wikilink")，也是自周公土圭测影以来测影技术发展的高峰，对中国天文史和建筑史都具重要研究价值。\[3\]

## 相關條目

  - [北京古观象台](../Page/北京古观象台.md "wikilink")
  - [天文台列表](../Page/天文台列表.md "wikilink")
  - [陶寺遺址](../Page/陶寺遺址.md "wikilink")：位於山西襄汾，包含古觀象臺的晚期龍山新石器時代遺址。

## 图库

<center>

Image:Dengfeng Observatory.jpg|登封观星台 Image:The Dengfeng
Observatory2.jpg|登封观星台 Image:The Dengfeng Observatory3.jpg|登封观星台
Image:The Dengfeng Observatory4.jpg|登封观星台

</center>

## 外部連結

  - [The Antiquarian
    Society](http://www.theantiquariansociety.com/tasbase/home/siteview.php?id=29)

  - [Early Chinese
    Astronomy](http://www.planetquest.org/learn/chinese.html)

  - [Ancient
    Observatories](http://sunearthday.nasa.gov/2005/locations/gaocheng.htm)

  - [Gaocheng at ChinaCulture.org showing both
    observatories](https://web.archive.org/web/20070203195054/http://www.chinaculture.org/gb/en_travel/2003-09/24/content_34489.htm)

  - [Surveying of the
    meridian](http://www.orientaldiscovery.com/2006/9-21/2006921163256.html)

  -
## 参考资料

[G](../Category/登封建筑.md "wikilink")
[G](../Category/中國傳統建築.md "wikilink")
[G](../Category/中华人民共和国天文台.md "wikilink")
[Category:天地之中](../Category/天地之中.md "wikilink")

1.
2.
3.