**玉山北峰**是[台灣](../Page/台灣.md "wikilink")[玉山山脈中的一座](../Page/玉山山脈.md "wikilink")[山峰](../Page/山峰.md "wikilink")，為[台灣百岳名峰之一](../Page/台灣百岳.md "wikilink")，位於[玉山主峰北側](../Page/玉山主峰.md "wikilink")，標高3,858[公尺](../Page/公尺.md "wikilink")。[日治時期名為](../Page/台灣日治時期.md "wikilink")**北山**、**新高北山**、**斗六新高**，與玉山北北峰相隔一鞍，狀似[駱駝的駝峰](../Page/駱駝.md "wikilink")，台灣[登山家](../Page/登山家.md "wikilink")[邢天正稱其為](../Page/邢天正.md "wikilink")**天駝峰**\[1\]。[日治時期起](../Page/台灣日治時期.md "wikilink")，峰頂上設有[玉山氣象站](../Page/玉山氣象站.md "wikilink")（標高3,844.8公尺），至今仍為台灣[海拔最高的](../Page/海拔.md "wikilink")[建築物](../Page/建築物.md "wikilink")。[玉山國家公園成立後](../Page/玉山國家公園.md "wikilink")，玉山北峰劃入被保護範圍。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

[category:台灣百岳](../Page/category:台灣百岳.md "wikilink")
[category:南投縣山峰](../Page/category:南投縣山峰.md "wikilink")

[Category:信義鄉 (臺灣)](../Category/信義鄉_\(臺灣\).md "wikilink")
[Category:玉山山脈](../Category/玉山山脈.md "wikilink")
[Category:玉山國家公園](../Category/玉山國家公園.md "wikilink")

1.  台灣百岳全集，連鋒宗編著，上河文化發行，2007年6月初版