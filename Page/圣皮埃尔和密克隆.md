**圣皮埃尔和密克隆**（），位于北[大西洋上](../Page/大西洋.md "wikilink")，其中最主要的岛屿是[聖皮耶島和](../Page/聖皮耶島_\(聖皮耶與密克隆群島\).md "wikilink")（Miquelon-Langlade）等。該群島的幾個主要島嶼中，圣皮埃尔岛面积约26平方公里，密克隆岛约115平方公里，朗格拉德岛面积约91平方公里。群岛的位置在[加拿大](../Page/加拿大.md "wikilink")[纽芬兰海岸外](../Page/纽芬兰岛.md "wikilink")25公里（13海里）处。

圣皮埃尔和密克隆是[法国的一个海外](../Page/法国.md "wikilink")[屬地](../Page/屬地.md "wikilink")（,
COM）之一，因此也是[欧盟的一部分](../Page/欧盟.md "wikilink")，但是由于特殊的移民程序，非法国公民的[欧盟国民不允许在](../Page/欧盟.md "wikilink")[群岛上自由迁徙或者从事商业活动](../Page/群岛.md "wikilink")\[1\]。整個行政區由8个小岛组成，面积242平方千米，各岛多裸露岩丘。气候冷湿多雾。人口6000，多法国移民后裔，信奉[天主教](../Page/天主教.md "wikilink")，讲[法语](../Page/法语.md "wikilink")。居民多从事渔业和鱼产品加工，另有养[狐养](../Page/狐.md "wikilink")[貂业](../Page/貂.md "wikilink")。首府[-{zh-hans:圣皮埃尔;
zh-hant:聖皮耶;}-](../Page/圣皮埃尔_\(圣皮埃尔和密克隆\).md "wikilink")，人口5,400多。

圣皮埃尔和密克隆是前法国殖民地[新法蘭西中仅存](../Page/新法蘭西.md "wikilink")、依然由法国统治的地區。

## 時區

圣皮埃尔和密克隆是採用**[UTC-3](../Page/UTC-3.md "wikilink")**的[時區](../Page/時區.md "wikilink")。[日光節約時間是採用](../Page/夏时制.md "wikilink")[北美時刻表](../Page/北美.md "wikilink")，以替代[法國本土所採用的歐洲時刻表](../Page/法國本土.md "wikilink")。

下列表格是當標準時間（非日光節約時間）實施時，聖皮耶與密克隆群島與其他不同地方的時刻比較：\[2\]

| 地點                                              | 時刻      | 常用時區名稱                   | 協調世界時                                      |
| ----------------------------------------------- | ------- | ------------------------ | ------------------------------------------ |
| [巴黎](../Page/巴黎.md "wikilink")                  | 4pm     | 歐洲中部時間 (CET)             | [UTC+1](../Page/UTC+1.md "wikilink")       |
| [伦敦](../Page/伦敦.md "wikilink")                  | 3pm     | 格林尼治標準時間 (GMT)           | [UTC](../Page/UTC.md "wikilink")           |
| [努克](../Page/努克.md "wikilink")                  | 中午      | 格陵蘭西部時間 (WGT)            | [UTC-3](../Page/UTC-3.md "wikilink")       |
| **聖皮耶與密克隆群島**                                   | **中午**  | **聖皮耶與密克隆群島標準時間 (PMST)** | **[UTC-3](../Page/UTC-3.md "wikilink")**   |
| [聖約翰斯](../Page/聖約翰斯_\(紐芬蘭-拉布拉多\).md "wikilink") | 11:30am | 紐芬蘭島標準時間 (NST)           | [UTC-3:30](../Page/UTC-3:30.md "wikilink") |
| [哈利法克斯](../Page/哈利法克斯.md "wikilink")            | 11am    | 大西洋標準時間 (AST)            | [UTC-4](../Page/UTC-4.md "wikilink")       |
| [纽约](../Page/纽约.md "wikilink")                  | 10am    | 北美東部標準時間 (EST)           | [UTC-5](../Page/UTC-5.md "wikilink")       |

[File:Winter_in_saint-pierre,_SPM,_white_house.JPG|冬天的聖皮埃尔](File:Winter_in_saint-pierre,_SPM,_white_house.JPG%7C冬天的聖皮埃尔)
<File:Ile_aux_marines,_SPM,_ship_wreck.JPG>|「跨越太平洋」號貨輪殘骸
[File:Ile_aux_marines,_landscape.JPG|水手島](File:Ile_aux_marines,_landscape.JPG%7C水手島)
[File:Port_of_saint-pierre,_SPM.jpg|聖皮埃尔港](File:Port_of_saint-pierre,_SPM.jpg%7C聖皮埃尔港)
[File:Langlade_Petit_Miquilon.jpg|朗格拉德島空拍](File:Langlade_Petit_Miquilon.jpg%7C朗格拉德島空拍)
[File:Saint-Pierre_harbor.jpg|漁業區](File:Saint-Pierre_harbor.jpg%7C漁業區)
<File:Saint> Pierre and Miquelon, winter scene.jpg|冬季冰封的漁業區

## 参考文献

## 外部链接

## 参见

  - [法国海外省和海外领地](../Page/法国海外省和海外领地.md "wikilink")

{{-}}

[Category:法國海外領地](../Category/法國海外領地.md "wikilink")
[圣皮埃尔和密克隆](../Category/圣皮埃尔和密克隆.md "wikilink")
[Category:法國島嶼](../Category/法國島嶼.md "wikilink")
[Category:大西洋島嶼](../Category/大西洋島嶼.md "wikilink")
[Category:北美洲島嶼](../Category/北美洲島嶼.md "wikilink")

1.  [Frequently Asked
    Questions](http://www.st-pierre-et-miquelon.com/english/questions.php)

2.