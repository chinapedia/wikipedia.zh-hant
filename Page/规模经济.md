[Economies_of_scale.PNG](https://zh.wikipedia.org/wiki/File:Economies_of_scale.PNG "fig:Economies_of_scale.PNG")
**规模经济**（**Economies of
scale**）是指扩大生产规模引起[经济效益增加的现象](../Page/经济效益.md "wikilink")，是长期平均总成本随产量增加而减少的特性。规模经济反映的是[生产要素的集中程度同经济效益之间的关系](../Page/生产要素.md "wikilink")。

规模经济的优越性在于：

1.  能够实现产品规格的统一和标准化；
2.  通过大量购入[原材料](../Page/原材料.md "wikilink")，而使单位购入成本下降；
3.  有利于管理人员和工程技术人员的专业化和精简；
4.  有利于新产品开发；
5.  具有较强的竞争力；
6.  充分利用规模经济，对于提高企业乃至整个国民经济的效益，具有重大意义。

隨著產量的增加，長期平均總成本下降的特性。但这并不仅仅意味着生产规模越大越好，因为规模经济追求的是能获取最佳经济效益的生产规模。一旦企业生产规模扩大到超过一定的规模，[边际效益却会逐渐下降](../Page/边际效益.md "wikilink")，甚至跌破趋向零，乃至变成负值，引发[规模不经济现象](../Page/规模不经济.md "wikilink")。

## 规模经济的原因

1.  专业化，从[亚当·斯密的著作](../Page/亚当·斯密.md "wikilink")《[國富論](../Page/國富論.md "wikilink")》开始，人们认识到[分工可以提高效率](../Page/分工.md "wikilink")。规模越大的企业，其分工也必然是更详细的。
2.  学习效应，随着产量的增加，工人可以使熟练程度增加，提高效率。
3.  可以有效地承担[研发费用等](../Page/研发.md "wikilink")。
4.  运输、採购原材料等方面存在的经济性。
5.  价格[谈判上的强势地位](../Page/谈判.md "wikilink")。

## 形式定义

\(F(K,L) \,\)存在以下定义:

  - **规模收益不变**：如果\(F(aK,aL)=aF(K,L) \,\)（[齊次函數](../Page/齐次函数.md "wikilink")1次）
  - **规模收益递增**：如果\(F(aK,aL)>aF(K,L), \,\)（齊次函數\>1次）
  - **规模收益递减**：如果\(F(aK,aL)<aF(K,L) \,\)（齊次函數\<1次）

资本和劳动是生产因素，[资本和](../Page/资本.md "wikilink")[劳动以及](../Page/劳动.md "wikilink")\(a\,\)是规模因素。

## 參考

  - [需求方规模经济](../Page/需求方规模经济.md "wikilink")
  - [規模不經濟](../Page/規模不經濟.md "wikilink")
  - [微觀經濟學](../Page/微觀經濟學.md "wikilink")
  - [範圍經濟](../Page/:en:Economies_of_scope.md "wikilink")

[Category:经济学](../Category/经济学.md "wikilink")