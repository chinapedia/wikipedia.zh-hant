**朱利叶斯·亨利·格魯喬·馬克思**（，）是美國的[喜劇演員與](../Page/喜劇演員.md "wikilink")[電影明星](../Page/電影明星.md "wikilink")。他以機智問答及比喻聞名，與家族成員（[馬克思兄弟](../Page/馬克思兄弟.md "wikilink")）合作拍攝了15部電影，並且單飛成績也十分耀眼，以擔任廣播及電視節目*You
Bet Your Life*主持人著名。馬克斯的特色十分顯著：顯眼的鬍鬚、眉毛以及眼鏡。

## 参考资料

## 扩展阅读

  -
  -
  -
  -
  -
  -
  -
  -
  -
  - [*Julius H. (Groucho) Marx v. Commissioner of Internal
    Revenue*](http://www.starkman.com/hippo/articles/marx.html), 29 T.C.
    88 (1957)

## 外部链接

  -
  -
  -
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國主持人](../Category/美國主持人.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:加利福尼亚州民主党人](../Category/加利福尼亚州民主党人.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:奥斯卡荣誉奖获得者](../Category/奥斯卡荣誉奖获得者.md "wikilink")
[Category:美国喜剧男演员](../Category/美国喜剧男演员.md "wikilink")
[Category:死于肺炎的人](../Category/死于肺炎的人.md "wikilink")
[Category:歌舞雜耍表演者](../Category/歌舞雜耍表演者.md "wikilink")
[Category:猶太喜劇演員](../Category/猶太喜劇演員.md "wikilink")
[Category:美國舞台男演員](../Category/美國舞台男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")