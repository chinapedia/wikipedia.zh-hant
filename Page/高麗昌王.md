**高丽昌王**（；），諱**王昌**（），[高丽王朝第](../Page/高丽王朝.md "wikilink")33位国王（1388年—1389在位），前任国王[王禑之子](../Page/王禑.md "wikilink")。明太祖洪武二十一年（1388年）五月，[守门下侍中](../Page/守门下侍中.md "wikilink")[李成桂在](../Page/李成桂.md "wikilink")[威化岛回军](../Page/威化岛回军.md "wikilink")，王禑被逼迫退位，王昌即位。次年十一月，王禑意图复辟，事觉，李成桂废了王昌，改立高丽宗室王瑶，是为[高丽恭让王](../Page/高丽恭让王.md "wikilink")。迁王禑于江陵，流王昌于江华。十二月，杀之，年仅八岁。

[朝鮮王朝編著的史料](../Page/朝鮮王朝.md "wikilink")《[高麗史](../Page/高麗史.md "wikilink")》不承認昌王王位的正統性，將其稱為「**辛昌**」（）。

## 家庭

  - 父：高丽国王[王禑](../Page/王禑.md "wikilink")
  - 母：谨妃李氏（[李琳之女](../Page/李琳_\(高麗\).md "wikilink")）

## 参考文献

  - 《[高丽史](../Page/高丽史.md "wikilink")》
  - 《[朝鲜王朝实录](../Page/朝鲜王朝实录.md "wikilink")》

{{-}}  |-style="text-align: center; background: \#FFE4E1;"
|align="center" colspan="3"|**高麗昌王** |-

[Category:高麗君主](../Category/高麗君主.md "wikilink")
[Category:朝鮮廢君](../Category/朝鮮廢君.md "wikilink")