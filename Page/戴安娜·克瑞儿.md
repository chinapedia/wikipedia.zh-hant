[Diana_krall.jpg](https://zh.wikipedia.org/wiki/File:Diana_krall.jpg "fig:Diana_krall.jpg")
**戴安娜·克瑞儿**，[OC](../Page/加拿大勳章.md "wikilink")，[OBC](../Page/不列顛哥倫比亞勳章.md "wikilink")（，)，[加拿大](../Page/加拿大.md "wikilink")[爵士樂](../Page/爵士樂.md "wikilink")[鋼琴手](../Page/鋼琴.md "wikilink")、[歌手](../Page/歌手.md "wikilink")，以[女低音](../Page/女低音.md "wikilink")\[1\]聞名。

## 傳記

1964年11月16日，戴安娜·克瑞儿生於[加拿大](../Page/加拿大.md "wikilink")[不列颠哥伦比亚](../Page/不列颠哥伦比亚.md "wikilink")（卑詩）省[納奈莫](../Page/納奈莫.md "wikilink")。畢業於[伯克利音乐学院](../Page/伯克利音乐学院.md "wikilink")，主修爵士[钢琴](../Page/钢琴.md "wikilink")，後成為[爵士樂音樂家](../Page/爵士樂.md "wikilink")。其在2004年所出最新專輯，《在另外的房间的女孩》（The
Girl in the Other
Room）[公告牌二百强专辑榜暢銷排名第四名](../Page/公告牌二百强专辑榜.md "wikilink")。2005年獲頒[加拿大勳章](../Page/加拿大勳章.md "wikilink")。

## 歷年專輯

  - Stepping Out (1993)
  - Only Trust Your Heart (1995)
  - All for You: A Dedication to the Nat King Cole Trio (1996)
  - Love Scenes (1997)
  - When I Look in Your Eyes (1999)
  - The Look of Love (2001)
  - Live in Paris (2002)
  - The Girl in the Other Room (2004)
  - Christmas Songs (2005)
  - From This Moment On (2006)
  - Quiet Nights (2009)
  - Glad Rag Doll (2012)
  - Wallflower (2015)
  - Turn Up the Quiet (2017)
  - Love Is Here to Stay with Tony Bennett (2018)

## 參考文獻

## 外部連結

  - [個人官方網站](http://www.dianakrall.com)

[Category:加拿大鋼琴家](../Category/加拿大鋼琴家.md "wikilink")
[Category:加拿大歌手](../Category/加拿大歌手.md "wikilink")
[Category:不列顛哥倫比亞省人](../Category/不列顛哥倫比亞省人.md "wikilink")
[Category:伯克利音樂學院校友](../Category/伯克利音樂學院校友.md "wikilink")

1.  [Barnes and Noble review: Diana
    Krall](http://music.barnesandnoble.com/The-Look-of-Love/Diana-Krall/e/731454984621)