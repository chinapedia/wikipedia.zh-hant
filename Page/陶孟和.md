[Tao_Menghe.jpg](https://zh.wikipedia.org/wiki/File:Tao_Menghe.jpg "fig:Tao_Menghe.jpg")
**陶孟和**（）\[1\]\[2\]，原名**履恭**，字**孟和**，[以字行](../Page/以字行.md "wikilink")，[天津人](../Page/天津.md "wikilink")，祖籍[浙江](../Page/浙江.md "wikilink")[紹興](../Page/紹興.md "wikilink")，[社會學家](../Page/社會學家.md "wikilink")。

## 生平

陶孟和幼時就讀於教育家[嚴修創辦的中西學並重的](../Page/嚴修.md "wikilink")「嚴氏家塾」（1904年改稱[私立敬業中學堂](../Page/天津市南开中学.md "wikilink")，1906年遷址后，改稱私立第一中學堂）
\[3\]。1906年畢業於该學堂第一屆師範班，以官費生被派赴[日本](../Page/日本.md "wikilink")[東京高等師範學校研習](../Page/東京高等師範學校.md "wikilink")[教育學](../Page/教育學.md "wikilink")，後因故回國\[4\]。1910年改赴[英國](../Page/英國.md "wikilink")[倫敦大學](../Page/倫敦大學.md "wikilink")[倫敦政治經濟學院研習](../Page/倫敦政治經濟學院.md "wikilink")[社會學和](../Page/社會學.md "wikilink")[經濟學](../Page/經濟學.md "wikilink")。1913年獲得經濟學博士學位。同年歸國後擔任[北京高等師範學校教授](../Page/北京高等師範學校.md "wikilink")。1914年至1927年擔任[北京大學教授](../Page/北京大學.md "wikilink")、系主任、文學院院長、教務長等職。1926年至1934年擔任[中華教育文化基金董事會](../Page/中華教育文化基金董事會.md "wikilink")(美國退還庚子賠款管理機構)
社會調查部（1929年獨立為社會調查所）負責人，致力於社會調查事業。1932年，陶孟和任[南开大学校董会董事](../Page/南开大学.md "wikilink")。1934年社會調查所併入[中央研究院社會科學研究所](../Page/中央研究院.md "wikilink")（1945年改稱社會研究所），陶孟和擔任所長。

[抗日战争期間](../Page/抗日战争.md "wikilink")，陶孟和負責將研究所遷至[四川南溪李莊](../Page/四川.md "wikilink")，勝利後始遷回[南京](../Page/南京.md "wikilink")。陶孟和还历任[國民參政會第一屆](../Page/國民參政會.md "wikilink")、第二屆、第三屆、第四屆參政員，[立法院](../Page/立法院.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。[中央研究院社會研究所所長](../Page/中央研究院社會研究所.md "wikilink")。

1949年中國政權更迭之際，在其主導下將社會研究所全部人員及圖書、研究資料留在南京。1949年10月起，陶孟和擔任[中國科學院副院長](../Page/中國科學院.md "wikilink")，負責社會、歷史、考古和語言４個研究所，並兼任社會研究所（1953年改名經濟研究所）所長。1952年將社會研究所遷往[北京](../Page/北京.md "wikilink")，隨後辭去所長職務。1960年4月，赴[上海參加中國科學院第三次學部會議前](../Page/上海.md "wikilink")，因病於4月17日去世，享年73歲\[5\]。

## 学术贡献

他開創了中國早期最重要的社會學研究機構之一，並積極將所作為社會調查成果推介到國際上。負責社會調查所期間，主導創立《社會科學雜誌》、《中國近代經濟史研究集刊》等重要學術刊物\[6\]，對中國社會學奠基與發展貢獻卓著。1948年獲選為第一屆[中央研究院院士](../Page/中央研究院院士.md "wikilink")。

陶孟和也是[中國科學院圖書館的創始人與領導人](../Page/中國科學院.md "wikilink")，先後成立了「中國科學院圖書委員會」、「中國科學院圖書館選書委員會」、「中國科學院科學圖書分類委員會」\[7\]，對科學院圖書館事業發展貢獻良多。

陶孟和早年專治[社會學](../Page/社會學.md "wikilink")，但在[中華人民共和國成立後](../Page/中華人民共和國.md "wikilink")，由於社會學一度被當作[偽科學而遭到否定](../Page/偽科學.md "wikilink")，因此嚴重影響其學術地位的公正評價\[8\]。

## 著作

  - 《中外地理大全》(與楊文洵合編)
  - 《中國鄉村與城鎮生活》(與梁宇皋合著，英文)
  - 《北京人力車夫之生活情形》
  - 《北平生活費之分析》
  - 《中國社會之研究》
  - 《歐洲和議後之經濟》
  - 《中國勞工生活程度》
  - 《社會與教育》
  - 《公民教育》
  - 《社會問題》
  - 《中國之縣地方財政》
  - 《孟和文存》

## 論文

  - 《中國之工業與勞工》(1931年[荷蘭](../Page/荷蘭.md "wikilink")[海牙世界社會經濟會議](../Page/海牙.md "wikilink")，與林頌河合著，英文)
  - 《中國勞工生活程度》(1931年[上海太平洋國際學會第四次會議](../Page/上海.md "wikilink")，英文)

## 參考文献

[Category:中央研究院人文及社會科學組院士](../Category/中央研究院人文及社會科學組院士.md "wikilink")
[Category:中国科学院哲学社会科学部院士](../Category/中国科学院哲学社会科学部院士.md "wikilink")
[Category:中華人民共和國社會學家](../Category/中華人民共和國社會學家.md "wikilink")
[Category:中華民國社會學家](../Category/中華民國社會學家.md "wikilink")
[Category:第一屆全國人大代表](../Category/第一屆全國人大代表.md "wikilink")
[Category:天津市全國人民代表大會代表](../Category/天津市全國人民代表大會代表.md "wikilink")
[Category:第一届国民参政会参政员](../Category/第一届国民参政会参政员.md "wikilink")
[Category:第二届国民参政会参政员](../Category/第二届国民参政会参政员.md "wikilink")
[Category:第三届国民参政会参政员](../Category/第三届国民参政会参政员.md "wikilink")
[Category:第四届国民参政会参政员](../Category/第四届国民参政会参政员.md "wikilink")
[Category:北京大學教授](../Category/北京大學教授.md "wikilink")
[Category:北京師範大學教授](../Category/北京師範大學教授.md "wikilink")
[Category:伦敦政治经济学院校友](../Category/伦敦政治经济学院校友.md "wikilink")
[Category:筑波大學校友](../Category/筑波大學校友.md "wikilink")
[Category:南開中學校友](../Category/南開中學校友.md "wikilink")
[Category:天津人](../Category/天津人.md "wikilink")
[Category:绍兴人](../Category/绍兴人.md "wikilink")
[Category:浙江裔天津人](../Category/浙江裔天津人.md "wikilink")
[Category:绍兴裔天津人](../Category/绍兴裔天津人.md "wikilink")
[Meng孟](../Category/陶姓.md "wikilink")

1.  出生年月據《中國大百科全書》總編委會編.中國大百科全書 22.中國大百科全書出版社,2009.03.第49頁

2.  [中央研究院
    陶孟和院士基本資料](https://db1n.sinica.edu.tw/textdb/ioconas/02.php?func=22&_op=?ID:HD028)

3.  [中國科學院 陶孟和 (白國應撰)](http://www.cas.cn/html/cas50/ldr/taomenghe.html)

4.  [社會學人類學中國網
    智效民：中国社会学的奠基者——陶孟和](http://www.sachina.edu.cn/Htmldata/article/2005/09/270.html)

5.
6.
7.
8.