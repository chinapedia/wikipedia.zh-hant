**鄭麗媛**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")。前女子組合[Chakra四位成員之一](../Page/Chakra_\(組合\).md "wikilink")，2004年退出組合後轉型成為[演員](../Page/演員.md "wikilink")。因演出《[我叫金三順](../Page/我叫金三順.md "wikilink")》而人氣急升，之後的《[你來自哪顆星](../Page/你來自哪顆星.md "wikilink")》也獲得相當不俗的收視成績。

## 演出作品

### 電視劇

|                                          |                                            |                                                            |      |    |
| ---------------------------------------- | ------------------------------------------ | ---------------------------------------------------------- | ---- | -- |
| 年份                                       | 电视台                                        | 劇目                                                         | 角色   | 備注 |
| 2002年                                    | [KBS](../Page/KBS.md "wikilink")           | [Saxophone](../Page/Saxophone.md "wikilink")               |      |    |
| [SBS](../Page/SBS.md "wikilink")         | [認真生活](../Page/認真生活.md "wikilink")         |                                                            |      |    |
| 2003年                                    | [MBC](../Page/MBC.md "wikilink")           | [ARGON](../Page/ARGON.md "wikilink")                       |      |    |
| 2005年                                    | [再見，弗蘭西斯卡](../Page/再見，弗蘭西斯卡.md "wikilink") |                                                            |      |    |
| [我叫金三順](../Page/我叫金三順.md "wikilink")     | 俞熙真                                        | 第二女主角                                                      |      |    |
| [秋日驟雨](../Page/秋日驟雨.md "wikilink")       | 朴妍舒                                        | 第一女主角                                                      |      |    |
| 2006年                                    | [你來自哪顆星](../Page/你來自哪顆星.md "wikilink")     | 李惠秀／李惠琳／金福實                                                |      |    |
| 2009年                                    | SBS                                        | 《[自鳴鼓](../Page/自鳴鼓.md "wikilink")》                         | 自鳴公主 |    |
| 2012年                                    | [工薪族楚漢誌](../Page/工薪族楚漢誌.md "wikilink")     | 白呂雉                                                        |      |    |
| [電視劇帝王](../Page/電視劇帝王.md "wikilink")     | 李高恩                                        |                                                            |      |    |
| 2013年                                    | MBC                                        | [Medical Top Team](../Page/Medical_Top_Team.md "wikilink") | 徐珠英  |    |
| 2015年                                    | [tvN](../Page/TVN.md "wikilink")           | [沒禮貌的英愛小姐14](../Page/沒禮貌的英愛小姐.md "wikilink")               | 金杏兒  | 客串 |
| [泡泡糖](../Page/泡泡糖_\(電視劇\).md "wikilink") | 金杏兒                                        | 第一女主角                                                      |      |    |
| 2017年                                    | KBS                                        | [魔女的法庭](../Page/魔女的法庭.md "wikilink")                       | 馬翌   |    |
| 2018年                                    | SBS                                        | [油膩的Melo](../Page/油膩的Melo.md "wikilink")                   | 段賽雅  |    |

### 電影

  - 2002年：《[緊急措施19號](../Page/緊急措施19號.md "wikilink")》
  - 2004年：《[我的B型男友](../Page/我的B型男友.md "wikilink")》
  - 2007年：《[雙面女友](../Page/雙面女友.md "wikilink")》
  - 2009年：《[荒島·愛](../Page/荒島·愛.md "wikilink")》
  - 2011年：《[與敵共寢](../Page/與敵共寢.md "wikilink")》
  - 2011年：《[痛症](../Page/痛症.md "wikilink")》
  - 2012年：《[永不結束的故事](../Page/永不結束的故事_\(電影\).md "wikilink")》
  - 2017年：《[Gate](../Page/Gate.md "wikilink")》飾演 素恩

### MV

  - 2008年：[Epik High](../Page/Epik_High.md "wikilink")《One》
  - 2010年：[GUMMY](../Page/GUMMY.md "wikilink")《因為是男人》（與[金賢重](../Page/金賢重.md "wikilink")）
  - 2011年：[Alex](../Page/秋憲坤.md "wikilink")《就算想要瘋狂》
  - 2015年：[Nell](../Page/Nell_\(樂團\).md "wikilink")《第三人稱的必要性》

## 主持

### 節目主持

  - 2014年：Art Star Korea

### 典禮主持

|     日期      |                   電視台                   |                   節目名稱                   |               合作藝人               |
| :---------: | :-------------------------------------: | :--------------------------------------: | :------------------------------: |
| 2012年12月31日 | [SBS](../Page/SBS_\(韓國\).md "wikilink") | [SBS演技大賞](../Page/SBS演技大赏.md "wikilink") | [李棟旭](../Page/李棟旭.md "wikilink") |
|             |                                         |                                          |                                  |

## 獲獎

  - 2005年：[MBC演技大賞](../Page/MBC演技大賞.md "wikilink")- 女子優秀演技獎
  - 2007年：第28屆[青龍電影獎](../Page/青龍電影獎.md "wikilink") 新人女優獎
  - 2008年：SIA STYLE ICON AWARDS 時尚達人獎
  - 2008年：PREMIERE Asia Rising Star Awards 新人獎
  - 2012年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－十大明星獎、迷你系列劇部門
    女子最優秀演技獎（[工薪族楚漢誌](../Page/工薪族楚漢誌.md "wikilink")、[電視劇帝王](../Page/電視劇帝王.md "wikilink")）
  - 2017年：[KBS演技大賞](../Page/KBS演技大賞.md "wikilink")－女子最優秀演技獎、最佳情侶獎
    (與[尹賢旻](../Page/尹賢旻.md "wikilink"))（[魔女的法庭](../Page/魔女的法庭.md "wikilink")）

## 外部連結

  - [aboutyoana官方網站](https://web.archive.org/web/20131007095915/http://aboutyoana.com/abouthtml/index.asp)


  - [EPG](https://web.archive.org/web/20090201174109/http://epg.epg.co.kr/star/profile/index.asp?actor_id=711)


  - [Daum
    영화](http://movie.daum.net/movieperson/Summary.do?personId=93924)


  -
  -
[J](../Category/韓國電視演員.md "wikilink")
[J](../Category/韓國電影演員.md "wikilink")
[J](../Category/高麗大學校友.md "wikilink")
[Category:Chakra](../Category/Chakra.md "wikilink")
[Category:鄭姓](../Category/鄭姓.md "wikilink")