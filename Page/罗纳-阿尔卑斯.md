**罗讷-阿尔卑斯大区**是已被合并的[法国大区](../Page/法国大区.md "wikilink")，位于法国的东南部。下轄[安省](../Page/安省.md "wikilink")（01）、[阿爾代什省](../Page/阿爾代什省.md "wikilink")（07）、[德龍省](../Page/德龍省.md "wikilink")（26）、[伊澤爾省](../Page/伊澤爾省.md "wikilink")（38）、[盧瓦爾省](../Page/盧瓦爾省.md "wikilink")（42）、[隆河省](../Page/隆河省.md "wikilink")（69）、[薩瓦省](../Page/薩瓦省.md "wikilink")（73）、[上薩瓦省](../Page/上薩瓦省.md "wikilink")（74）。

与[普罗旺斯-阿尔卑斯-蓝色海岸大区](../Page/普罗旺斯-阿尔卑斯-蓝色海岸大区.md "wikilink")、[朗格多克-鲁西永大区](../Page/朗格多克-鲁西永大区.md "wikilink")、[奥弗涅大区](../Page/奥弗涅大区.md "wikilink")、[勃艮第大区](../Page/勃艮第大区.md "wikilink")、[弗朗什-孔泰大区相邻](../Page/弗朗什-孔泰大区.md "wikilink")。与之接壤的国外地区有：意大利的[奧斯塔山谷大區](../Page/奧斯塔山谷大區.md "wikilink")（Val
d'Aoste）和[皮埃蒙特大区](../Page/皮埃蒙特大区.md "wikilink")（Piémont），瑞士的[沃州](../Page/沃州.md "wikilink")（Vaud）、[瓦莱州](../Page/瓦莱州.md "wikilink")（Valais）和[日内瓦州](../Page/日内瓦州.md "wikilink")（Genève）。罗讷-阿尔卑斯地区西起中央高原（Massif
Central），东至阿尔卑斯山（Alpes）。除了自然区域之外，历史、文化还有美食也是这里的城市所拥有的几个主要财富。历史悠久的保护城区（[里昂](../Page/里昂.md "wikilink")、[尚贝里](../Page/尚贝里.md "wikilink")、[安錫等](../Page/安錫.md "wikilink")），欧洲最密集的博物馆网络之一，城市中心的花园等，都體現了罗讷-阿尔卑斯地区辉煌的历史。

2016年1月1日，罗讷-阿尔卑斯大区与[奧弗涅大区合併为](../Page/奧弗涅大区.md "wikilink")[奧弗涅-羅納-阿爾卑斯大區](../Page/奧弗涅-羅納-阿爾卑斯大區.md "wikilink")。

## 省

[Château_des_Adhémar_08_2006_058.jpg](https://zh.wikipedia.org/wiki/File:Château_des_Adhémar_08_2006_058.jpg "fig:Château_des_Adhémar_08_2006_058.jpg")\]\]

  - **[安省](../Page/安省.md "wikilink")（01）**（Ain）

<!-- end list -->

  - **[阿尔代什省](../Page/阿尔代什省.md "wikilink")（07）**（Ardèche）

<!-- end list -->

  -
    [Crussol](../Page/Château_de_Crussol.md "wikilink")

<!-- end list -->

  - **[德龙省](../Page/德龙省.md "wikilink")（26）**（Drôme）

<!-- end list -->

  -
    [Adhémar](../Page/Château_des_Adhémar.md "wikilink"){{·}} **

<!-- end list -->

  - **[伊泽尔省](../Page/伊泽尔省.md "wikilink")（38）**（Isère）

<!-- end list -->

  -
    [Arthaudière](../Page/Château_de_l'Arthaudière.md "wikilink"){{·}}
    [Bayard (Pontcharra)](../Page/Château_Bayard.md "wikilink"){{·}}
    [Fallavier](../Page/Château_de_Fallavier.md "wikilink")

<!-- end list -->

  - **[卢瓦尔省](../Page/卢瓦尔省.md "wikilink")（42）**（Loire）

<!-- end list -->

  -
    [Chalmazel](../Page/Château_de_Chalmazel.md "wikilink"){{·}} **{{·}}
    **{{·}} [la Roche
    (Saint-Priest-la-Roche)](../Page/Château_de_la_Roche.md "wikilink")
    [1](https://web.archive.org/web/20070915145736/http://www.jedecouvrelafrance.com/f-1491.loire-chateau-de-saint-priest-la-roche.html)

<!-- end list -->

  - **[罗讷省](../Page/罗讷省.md "wikilink")（69）**（Rhône〔department〕）

<!-- end list -->

  -
    **

<!-- end list -->

  - **[萨瓦省](../Page/萨瓦省.md "wikilink")（73）**（Savoie）

<!-- end list -->

  -
    **

<!-- end list -->

  - **[上萨瓦省](../Page/上萨瓦省.md "wikilink")（74）**（Haute-Savoie）

<!-- end list -->

  -
    [Annecy](../Page/Château_d'Annecy.md "wikilink"){{·}}
    [Menthon-Saint-Bernard](../Page/Château_de_Menthon-Saint-Bernard.md "wikilink"){{·}}
    **{{·}} [Sales](../Page/Château_de_Sales.md "wikilink"){{·}}
    [Thorens](../Page/Château_de_Thorens.md "wikilink")

{{-}}

## 参考文献

## 參見

  - [法国大区](../Page/法国大区.md "wikilink")
      - [奧弗涅-羅納-阿爾卑斯大區](../Page/奧弗涅-羅納-阿爾卑斯大區.md "wikilink")

{{-}}

[Category:法國已撤銷的大區](../Category/法國已撤銷的大區.md "wikilink")
[Category:奥弗涅-罗讷-阿尔卑斯大区历史](../Category/奥弗涅-罗讷-阿尔卑斯大区历史.md "wikilink")
[Category:2016年废除的行政区划](../Category/2016年废除的行政区划.md "wikilink")