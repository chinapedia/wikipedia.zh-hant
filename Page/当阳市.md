**当阳市**位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[湖北省西部](../Page/湖北省.md "wikilink")，是[宜昌市代管的一个](../Page/宜昌市.md "wikilink")[县级市](../Page/县级市.md "wikilink")。

## 历史

[秦昭襄王二十九年](../Page/秦昭襄王.md "wikilink")（前278年），白起伐楚，在郢都置南郡，始设当阳县。[秦朝取消当阳县](../Page/秦朝.md "wikilink")，其地并入郢县，属南郡。[西汉曾改称江陵县](../Page/西汉.md "wikilink")，景帝中元二年（前148年），析江陵复置当阳县。1948年7月，成立荆当县。1949年5月，分荆当设当阳县，隶属宜昌地区。1988年10月22日，当阳撤县设市。

当阳市境内的[玉泉寺在中国佛教历史上有重要的影响](../Page/玉泉寺.md "wikilink")，其始建于汉末，兴于武周，是禅宗高僧神秀大师（禅宗‘北渐南顿’中北宗的六祖）的道场，也是天台宗智者大师（天台宗的实际创始人）的道场。[长坂坡位于当阳城区东](../Page/长坂坡.md "wikilink")，为三国时期战役[长坂坡之战的战场](../Page/长坂坡之战.md "wikilink")。

境内的绿林山为西汉末年绿林起义的根据地。

[麦城在当阳市两河镇境内](../Page/麦城.md "wikilink")，距市玉阳镇20余公里，[东汉](../Page/东汉.md "wikilink")[建安二十四年](../Page/建安.md "wikilink")（220年），[蜀汉大将](../Page/蜀汉.md "wikilink")[关羽失守](../Page/关羽.md "wikilink")[荆州](../Page/荆州.md "wikilink")，退守麦城。

## 地理

地处[荆山山脉向](../Page/荆山.md "wikilink")[江汉平原的过渡地带](../Page/江汉平原.md "wikilink")。地势西北高，东南低，境内一般[海拔高度在](../Page/海拔.md "wikilink")300[米以下](../Page/米.md "wikilink")。最高峰是与[远安县交界的南包](../Page/远安县.md "wikilink")，海拔高度1087[米](../Page/米.md "wikilink")。最低处是两河镇黄家场一带，海拔高度44[米](../Page/米.md "wikilink")。境内地貌类型包括[山地](../Page/山地.md "wikilink")、[丘陵](../Page/丘陵.md "wikilink")、岗地、[平原](../Page/平原.md "wikilink")。其中山地占总面积14.83%，丘陵、岗地占56.43%，平原占28.74%。主要山脉包括玉泉山、云梦山、锦屏山、玉阳山、方山、绿林山、紫盖山等。

亚热带季风性湿润气候，四季分明，雨季集中于夏季。

## 经济

2009年，全市实现[生产总值](../Page/生产总值.md "wikilink")1330532万元，按可比价计算，比上年增长16.3％；其中，[第一产业实现增加值](../Page/第一产业.md "wikilink")299972万元，增长2％；[第二产业实现增加值](../Page/第二产业.md "wikilink")605960万元，增长29.6％；[第三产业实现增加值](../Page/第三产业.md "wikilink")424599万元，增长9.8％。[三次产业结构由上年的](../Page/三次产业结构.md "wikilink")25.5：40．1：33.4调整为22.5：45.6：31.9。

## 交通

  - 高速公路：[<span style="color:green;">荆宜高速公路</span>](../Page/沪蓉高速公路.md "wikilink")（沪蓉高速[Expressway_G42.jpg](https://zh.wikipedia.org/wiki/File:Expressway_G42.jpg "fig:Expressway_G42.jpg")）有出入口。
  - 铁路：[焦柳铁路纵贯](../Page/焦柳铁路.md "wikilink")，火车站位于市郊。
  - 飞机：有军民两用机场。

## 区划人口

### 行政区划

  - [街道](../Page/街道_\(行政区划\).md "wikilink"):玉陽街道、壩陵街道、玉泉街道
  - [鎮](../Page/鎮.md "wikilink"):兩河鎮、河溶鎮、淯溪鎮、廟前鎮、王店鎮、半月鎮、草埠湖鎮

### 人口

2017年，全市年末户籍总人口为46.80万人。全年出生人口3556人，出生率为7.14‰，符合政策生育率为99.35%；全年死亡人口2855人，死亡率为5.73%；人口自然增长率为1.41%。出生人口性别比为100.98。

汉族为居住的主要民族，另有土家族、回族、苗族、满族、蒙古族、侗族等散居少数民族18个，人口共2070人，占总人口的0.45%。

## 外部链接

  - [湖北当阳网](https://web.archive.org/web/20061115101831/http://www.hbdangyang.com/)
  - [2009年当阳市国民经济和社会发展统计公报](http://www.hbdangyang.com/jrdy/dygk/200903/2767.html)

[当阳市](../Category/当阳市.md "wikilink")
[市](../Category/宜昌区县市.md "wikilink")
[宜昌市](../Category/湖北县级市.md "wikilink")