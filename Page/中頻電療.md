**中頻電療**（Middle frequency
electrotherapy）是屬於[物理治療中電刺激治療方法的一種](../Page/物理治療.md "wikilink")。由於這種治療的方法使用的是[頻率介於](../Page/頻率.md "wikilink")1kHz至10kHz的干擾電流刺激，故命名為中頻電療。由於治療的方法是使用兩個不同中頻的電刺激所產生的干擾波進行治療（原理如同是兩個不同頻率的訊號所產生的[拍頻](../Page/拍頻.md "wikilink")
beat），故又名為**干擾波治療**（interference wave therapy）或**干擾電流療法**（interference
current therapy, IFC
therapy）。中頻電療所產生的干擾電流可刺激[神經](../Page/神經.md "wikilink")、促使[肌肉交替進行收縮與放鬆](../Page/肌肉.md "wikilink")，以進行治療。

## 中頻電療的用途

1.  干擾神經的痛覺傳遞功能，並刺激身體分泌[內啡肽](../Page/內啡肽.md "wikilink")，而產生止痛效果
2.  促進血液循環與消除[水腫](../Page/水腫.md "wikilink")
3.  促進組織癒合

## 中頻電療在臨床上的適應症

  - [運動傷害](../Page/運動傷害.md "wikilink")，例如肌肉拉傷或挫傷
  - [退化性關節炎](../Page/退化性關節炎.md "wikilink")
  - [下背痛](../Page/下背痛.md "wikilink")

## 中頻電療在臨床上的禁忌症

  - [骨折](../Page/骨折.md "wikilink")
  - 體內裝有[心臟節律器或其他金屬物品](../Page/心臟節律器.md "wikilink")
  - [惡性腫瘤](../Page/惡性腫瘤.md "wikilink")
  - [懷孕](../Page/懷孕.md "wikilink")
  - 有感染發炎的情形
  - 具有開放性傷口的部位

## 參考資料

1.  廖文炫 張梅蘭 蔡美文 王淑芬：物理因子治療學 ─ 電磁療學。合計圖書出版社，2006
2.  [G. C. Goats, PhD, MCSP：Physiotherapy treatment modalities ─
    Interferential current therapy, Br. J. Sp. Med；Vol 24,
    No. 2](http://www.pubmedcentral.nih.gov/articlerender.fcgi?tool=pubmed&pubmedid=1702337)

## 外部連結

  - [愛鄰復健科診所
    物理治療簡介](https://web.archive.org/web/20090228071915/http://www.rehcare.com.tw/css/PT/a5.htm)
  - [佳德復健醫療保健專業網](http://www.chader.com.tw/modules/newbb/viewtopic.php?topic_id=29)

[Category:治療](../Category/治療.md "wikilink")