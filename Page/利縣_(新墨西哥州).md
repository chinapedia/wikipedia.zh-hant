**利縣**（）是[美國](../Page/美國.md "wikilink")[新墨西哥州東南角的一個縣](../Page/新墨西哥州.md "wikilink")，東、南鄰[德克薩斯州](../Page/德克薩斯州.md "wikilink")，面積11,380平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口64,727人。本縣縣治為[拉溫頓](../Page/拉溫頓_\(新墨西哥州\).md "wikilink")（Lovington）。

本縣成立於1917年3月7日，縣名紀念[新墨西哥軍事學院創建者約瑟夫](../Page/新墨西哥軍事學院.md "wikilink")·卡洛維·利（Joseph
Calloway Lea）。

## 地理

[Lea_County_New_Mexico_Court_House.jpg](https://zh.wikipedia.org/wiki/File:Lea_County_New_Mexico_Court_House.jpg "fig:Lea_County_New_Mexico_Court_House.jpg")
[Lovington_New_Mexico_Public_Library.jpg](https://zh.wikipedia.org/wiki/File:Lovington_New_Mexico_Public_Library.jpg "fig:Lovington_New_Mexico_Public_Library.jpg")
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，利縣的總面積為，其中有，即99.98%為陸地；，即0.02%為水域。\[1\]本縣既是新墨西哥州面積第11大的縣份，亦是[美國面積第89大的縣份](../Page/美國面積最大縣份列表.md "wikilink")。

### 毗鄰縣

  - [新墨西哥州](../Page/新墨西哥州.md "wikilink")
    [羅斯福縣](../Page/羅斯福縣_\(新墨西哥州\).md "wikilink")：北方
  - 新墨西哥州 [沙維什縣](../Page/沙維什縣.md "wikilink")：西方
  - 新墨西哥州 [埃迪縣](../Page/埃迪縣_\(新墨西哥州\).md "wikilink")：西方
  - [德克薩斯州](../Page/德克薩斯州.md "wikilink")
    [洛文縣](../Page/洛文縣.md "wikilink")：南方
  - 德克薩斯州 [溫克勒縣](../Page/溫克勒縣_\(德克薩斯州\).md "wikilink")：東南方
  - 德克薩斯州 [安德魯斯縣](../Page/安德魯斯縣_\(德克薩斯州\).md "wikilink")：東方
  - 德克薩斯州 [蓋恩斯縣](../Page/蓋恩斯縣_\(德克薩斯州\).md "wikilink")：東方
  - 德克薩斯州 [約克姆縣](../Page/約克姆縣_\(德克薩斯州\).md "wikilink")：東方
  - 德克薩斯州 [科克倫縣](../Page/科克倫縣_\(德克薩斯州\).md "wikilink")：東北方

## 人口

### 2010年

根據[2010年人口普查](../Page/2010年美國人口普查.md "wikilink")，利縣擁有64,727居民。而人口是由75%[白人](../Page/歐裔美國人.md "wikilink")、4.1%[黑人](../Page/非裔美國人.md "wikilink")、1.2%[土著](../Page/美國土著.md "wikilink")、0.5%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.1%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、16.5%其他[種族和](../Page/種族.md "wikilink")2.6%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")51.1%。\[2\]

### 2000年

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，利縣擁有55,511居民、19,699住戶和14,715家庭。\[3\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")13居民（每平方公里5居民）。\[4\]本縣擁有23,405間房屋单位，其密度為每平方英里5間（每平方公里2間）。\[5\]而人口是由67.13%[白人](../Page/歐裔美國人.md "wikilink")、4.37%[黑人](../Page/非裔美國人.md "wikilink")、0.99%[土著](../Page/美國土著.md "wikilink")、0.39%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.04%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、23.81%其他[種族和](../Page/種族.md "wikilink")3.27%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")39.65%。\[6\]

在19,699
住户中，有39.30%擁有一個或以上的兒童（18歲以下）、57.8%為夫妻、12.2%為單親家庭、25.3%為非家庭、22.5%為獨居、9.9%住戶有同居長者。平均每戶有2.73人，而平均每個家庭則有3.2人。在55,511居民中，有30.1%為18歲以下、10.1%為18至24歲、27.3%為25至44歲、20.3%為45至64歲以及12.2%為65歲以上。人口的年齡中位數為33歲，女子對男子的性別比為100：100.3。成年人的性別比則為100：99。\[7\]

本縣的住戶收入中位數為$29,799，而家庭收入中位數則為$34,665。男性的收入中位數為$32,005，而女性的收入中位數則為$20,922，[人均收入為](../Page/人均收入.md "wikilink")$14,184。約17.3%家庭和21.1%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括28%兒童（18歲以下）及14.9%長者（65歲以上）。\[8\]

## 參考文獻

[L](../Category/新墨西哥州行政区划.md "wikilink")

1.

2.  ["2010 Census P.L. 94-171 Summary File
    Data"](http://www2.census.gov/census_2010/01-Redistricting_File--PL_94-171/California/).
    United States Census Bureau.

3.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

4.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

5.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

6.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

7.  [American FactFinder](http://factfinder.census.gov/)

8.