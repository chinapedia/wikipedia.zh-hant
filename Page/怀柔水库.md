**怀柔水库**是位于[北京市](../Page/北京市.md "wikilink")[怀柔区城区西侧的](../Page/怀柔区.md "wikilink")[水库](../Page/水库.md "wikilink")，拦截[怀九河](../Page/怀九河.md "wikilink")、[怀沙河](../Page/怀沙河.md "wikilink")，出口为[怀河](../Page/怀河.md "wikilink")。

于1958年建成。库容9670万立方米。水库集水面积525平方公里。

[Category:北京市水库](../Category/北京市水库.md "wikilink")