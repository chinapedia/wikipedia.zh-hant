**对位红**（1-((对硝基苯基)偶氮)-2-萘酚，）是一种[偶氮染料](../Page/偶氮染料.md "wikilink")，通常被用于纺织物的染色，也被用于生物学试验中的染色。

对位红的结构与[苏丹一号相似](../Page/苏丹一号.md "wikilink")，只是在[苯基偶氮对位增加了一个](../Page/苯基.md "wikilink")[硝基](../Page/硝基.md "wikilink")，主要染色原理和毒性也与苏丹一号相似。对位红是1870年代发现的一系列最早的单偶氮酸性染料之一，它可以将纺织物的纤维素染色为明亮的红色，但是不够牢固。

在[英国](../Page/英国.md "wikilink")，自1995年起它被禁止加入到食品中。2005年4月15日，[英国食品标准署](../Page/英国食品标准署.md "wikilink")（FSA）发出警告，称[美国](../Page/美国.md "wikilink")[通用磨坊食品公司](../Page/通用磨坊.md "wikilink")（General
Mills）生产的Old El Paso牌辣味墨西哥菜及墨西哥玉米煎饼中含有对位红染料，提醒消费者食用该类商品可能有潜在致癌风险。

## 参见

  - [苏丹一号](../Page/苏丹一号.md "wikilink")

## 外部链接

  - [英国食品标准署关于部分Old El
    Paso食品含有非法添加剂的警告](http://www.food.gov.uk/news/newsarchive/2005/apr/oldelpaso)

[Category:偶氮染料](../Category/偶氮染料.md "wikilink")
[Category:硝基苯](../Category/硝基苯.md "wikilink")
[Category:2-萘酚](../Category/2-萘酚.md "wikilink")