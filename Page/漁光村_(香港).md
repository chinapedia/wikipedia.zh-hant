[Yue_Kwong_Chuen_Playground_(brighter).jpg](https://zh.wikipedia.org/wiki/File:Yue_Kwong_Chuen_Playground_\(brighter\).jpg "fig:Yue_Kwong_Chuen_Playground_(brighter).jpg")
**漁光村**（）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，位於[南區](../Page/南區_\(香港\).md "wikilink")[香港仔東北部](../Page/香港仔.md "wikilink")[石排灣的](../Page/石排灣_\(香港仔\).md "wikilink")[香港仔水塘道](../Page/香港仔水塘道.md "wikilink")，[石排灣邨以北](../Page/石排灣邨.md "wikilink")，[香港仔郊野公園以南](../Page/香港仔郊野公園.md "wikilink")，是香港仔市區和郊區之分界，也是不少登山人士出發的地點。漁光邨是[香港房屋協會唯一於南區的出租屋邨](../Page/香港房屋協會.md "wikilink")，由建築師阮達祖設計。

## 歷史

1960年代初期，[香港政府計劃發展香港仔](../Page/香港殖民地時期#香港政府.md "wikilink")，漁光村在1962年建成，為[南區和](../Page/南區_\(香港\).md "wikilink")[香港仔第一座成立的屋邨](../Page/香港仔.md "wikilink")，也是房屋協會建立的第六座屋邨。

2010年至2012年年期，房協每年都為漁光村翻新，並且安裝了[升降機](../Page/升降機.md "wikilink")。

2012年12月14日，房協主席[鄔滿海表示](../Page/鄔滿海.md "wikilink")，正計劃重新興建漁光邨以增加房屋供應，居民對此意見不一\[1\]\[2\]\[3\]。

2018年中，為善用現有出租房屋資源，房協把漁光村因應重建計劃而騰空的約200個單位翻新，這批單位全屬小型單位，並推出首個「暫租住屋」過渡性房屋計劃，在8月3日截止申請\[4\]\[5\]，在同年底至2019年5月陸續入伙\[6\]。

房協預計在2023年為漁光村分期進行重建\[7\]。

## 住宅及設施

漁光村設有5座住宅，分兩期落成，第一期為[白沙樓](../Page/白沙樓.md "wikilink")、[順風樓及](../Page/順風樓.md "wikilink")[海港樓](../Page/海港樓.md "wikilink")；第二期為[靜海樓及](../Page/靜海樓.md "wikilink")[海鷗樓](../Page/海鷗樓.md "wikilink")，房協只在第二期加設了[電梯及保安大閘](../Page/升降機.md "wikilink")，而第一期樓宇則兩者皆沒有。漁光村是房協也是香港仔現存最舊的屋邨。邨內設施貧乏，只有一個小型露天[公眾停車場](../Page/公眾停車場.md "wikilink")（54個車位），8個電單車位和一個小型休憩公園。

房協本來在2000年打算重建漁光邨，後來擱置。在2010至2012年在第二期樓宇內，大幅度地改裝原有設施，加鋪地磚，加設門牌及指示牌，加裝電梯及大廈閘門。

<File:Yue> Kwong Chuen.jpg|第一期的「白沙樓」、「順風樓」及「海港樓」
[File:YKC2.jpg|第二期的「靜海樓」及「海鷗樓](File:YKC2.jpg%7C第二期的「靜海樓」及「海鷗樓)」
<File:HK> Yue Kwong Chuen near Shek Pai Wan Estate 2.JPG <File:Yue>
Kwong Chuen.JPG

2018年房協花費2,000萬翻新漁光村而成的「暫住租屋」過渡性房屋出租計劃提供1至3人單位3個類別，面積從1人單位的14平方米至最大3人單位的28平方米，暫租費用介乎560至1400元。\[8\]\[9\]

## 屋邨資料

### 落成日期及層數

|      |          |     |      |
| ---- | -------- | --- | ---- |
| 樓宇名稱 | 落成日期     | 層數  | 單位數目 |
| 白沙樓  | 1962年12月 | 10層 | 155  |
| 順風樓  | 1963年1月  | 7層  | 195  |
| 海港樓  | 1963年4月  | 11層 | 282  |
| 靜海樓  | 1965年2月  | 9層  | 269  |
| 海鷗樓  | 1965年2月  | 9層  | 274  |

### 出租單位面積及編配標凖

|      |       |               |
| ---- | ----- | ------------- |
| 單位款式 | 編配標凖  | 室內樓面面積        |
| 小型單位 | 1-3人  | 約14-27.8平方米   |
| 中型單位 | 4-6人  | 約28.8-40.8平方米 |
| 大型單位 | 6人或以上 | 約50平方米        |

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/石排灣邨公共運輸交匯處.md" title="wikilink">石排灣邨公共運輸交匯處</a></dt>

</dl>
<dl>
<dt><a href="../Page/漁光道.md" title="wikilink">漁光道</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/香港仔.md" title="wikilink">香港仔至</a><a href="../Page/石排灣_(香港仔).md" title="wikilink">石排灣線</a>[10]</li>
</ul>
<dl>
<dt><a href="../Page/香港仔水塘道.md" title="wikilink">香港仔水塘道</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li><a href="../Page/香港仔.md" title="wikilink">香港仔至</a><a href="../Page/石排灣_(香港仔).md" title="wikilink">石排灣線</a></li>
</ul>
<dl>
<dt><a href="../Page/海暉道.md" title="wikilink">海暉道</a><br />
<a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>香港仔至西環線[11]</li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部連結

  - [房協漁光邨資料](https://www.hkhs.com/tc/housing_archive/id/30)

[en:Public housing estates in Pok Fu Lam, Aberdeen and Ap Lei Chau\#Yue
Kwong
Chuen](../Page/en:Public_housing_estates_in_Pok_Fu_Lam,_Aberdeen_and_Ap_Lei_Chau#Yue_Kwong_Chuen.md "wikilink")

[Category:石排灣 (香港仔)](../Category/石排灣_\(香港仔\).md "wikilink")

1.  [房協計劃重建漁光邨增供應](http://hk.news.yahoo.com/房協計劃重建漁光邨增供應-124914062.html)

2.  [50年樓齡 漁光邨擬重建 老化難解決
    重建增單位](https://archive.is/20130105110207/hk.news.yahoo.com/50%E5%B9%B4%E6%A8%93%E9%BD%A1-%E6%BC%81%E5%85%89%E9%82%A8%E6%93%AC%E9%87%8D%E5%BB%BA-%E8%80%81%E5%8C%96%E9%9B%A3%E8%A7%A3%E6%B1%BA-%E9%87%8D%E5%BB%BA%E5%A2%9E%E5%96%AE%E4%BD%8D-211247623.html)
    《明報》 2012年12月15日

3.  [重建漁光村
    田灣擬建接收屋](http://orientaldaily.on.cc/cnt/news/20150408/00176_042.html)
    《東方日報》 2015年4月8日

4.
5.

6.
7.
8.

9.

10. [公共小巴香港仔至石排灣線](http://hkbus.wikia.com/wiki/%E5%85%AC%E5%85%B1%E5%B0%8F%E5%B7%B4%E9%A6%99%E6%B8%AF%E4%BB%94%E8%87%B3%E7%9F%B3%E6%8E%92%E7%81%A3%E7%B7%9A)

11. [香港仔漁暉道　—　石塘咀創業商場](http://www.16seats.net/chi/rmb/r_h43.html)