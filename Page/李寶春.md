**李寶春**（），幼名**李寶寶**，曾用藝名**李泳**、**李永孩**，[台灣](../Page/台灣.md "wikilink")[京劇演員](../Page/京劇.md "wikilink")，工[老生](../Page/老生.md "wikilink")，1950年在[中國](../Page/中華人民共和國.md "wikilink")[北京出生](../Page/北京.md "wikilink")，祖籍[河北](../Page/河北.md "wikilink")[霸縣](../Page/霸縣.md "wikilink")，曾在[台北](../Page/台北市.md "wikilink")[中國文化大學戲劇學系中國戲劇組專任副教授](../Page/中國文化大學.md "wikilink")、[國立台灣大學戲劇學系兼任副教授](../Page/國立台灣大學.md "wikilink")。

## 生平

祖父[李桂春](../Page/李桂春.md "wikilink")（藝名**小達子**）是[河北梆子和](../Page/河北梆子.md "wikilink")[京劇演員](../Page/京劇.md "wikilink")，父親[李少春拜師](../Page/李少春.md "wikilink")[余叔岩](../Page/余叔岩.md "wikilink")、[周信芳](../Page/周信芳.md "wikilink")、[蓋叫天](../Page/蓋叫天.md "wikilink")，兩代都是文武雙全（重唱工的文戲和重身段動作的武戲都很能做）的老生演員，母親[侯玉蘭是京劇旦行演員](../Page/侯玉蘭.md "wikilink")。

他[青少年時考進](../Page/青少年.md "wikilink")[馬連良擔任校長的](../Page/馬連良.md "wikilink")[北京市戲曲學校](../Page/北京市.md "wikilink")，師從馬連良、[王少樓](../Page/王少楼_\(京剧老生\).md "wikilink")、[馬長禮等名師學唱老生](../Page/馬長禮.md "wikilink")，1969年畢業，到革命現代京劇（[京劇現代戲](../Page/京劇現代戲.md "wikilink")）《[杜鵑山](../Page/杜鵑山.md "wikilink")》劇組工作，飾演李石堅（副生，正面人物）1角，並參與拍攝[謝鐵驪執導的電影版](../Page/謝鐵驪.md "wikilink")。

1980年代中期后移民[美國](../Page/美國.md "wikilink")，1990年代初到台灣工作，直到現在。

1994年10月12日晚上，他在台灣[國家音樂廳舉行](../Page/國家音樂廳.md "wikilink")「李寶春與兩岸國劇巨星：交響樂下唱皮黃」音樂會，[胡炳旭指揮台灣](../Page/胡炳旭.md "wikilink")[國家交響樂團](../Page/國家交響樂團.md "wikilink")（當時叫國家音樂廳交響樂團），京劇樂隊（胡琴、鼓等）來自北京[中國京劇院](../Page/中國京劇院.md "wikilink")（現在的[中國國家京劇院](../Page/中國國家京劇院.md "wikilink")）。

這場音樂會讓他成為史上第1個，配上大型西方管弦樂伴奏演唱京劇的台灣演員，台灣國家交響樂團則成為台灣史上第1個給京劇伴奏的愛樂管弦樂團。

1996年，他錄製《[一夜京戲](../Page/一夜京戲.md "wikilink")》個人京劇唱腔音樂專輯，[林鑫濤編曲](../Page/林鑫濤.md "wikilink")，[謝光榮打鼓](../Page/謝光榮.md "wikilink")，京劇胡琴（[京胡](../Page/京胡.md "wikilink")）劉鐵山，京劇二胡（[京二胡](../Page/京二胡.md "wikilink")）王英奎。

1998年，創辦[台北新劇團](../Page/台北新劇團.md "wikilink")。

[Category:京剧演员](../Category/京剧演员.md "wikilink")
[Category:生行演员](../Category/生行演员.md "wikilink")
[Category:台灣男演員](../Category/台灣男演員.md "wikilink")
[L](../Category/霸州人.md "wikilink") [B](../Category/李姓.md "wikilink")