**俾斯麥**（）是[美國](../Page/美國.md "wikilink")[北達科塔州中部](../Page/北達科塔州.md "wikilink")[伯利縣的縣治](../Page/伯利縣_\(北达科他州\).md "wikilink")，也是北達科塔州的首府，俾斯麥是北達科塔州中部商業、醫療中心。人口55,532（2000年統計）。城市建立在1872年，名字是紀念德國首相[奧托·馮·俾斯麥](../Page/奧托·馮·俾斯麥.md "wikilink")。俾斯麥位於[密蘇里河河畔](../Page/密蘇里河.md "wikilink")。面積71.0
km²，水域面積佔了1.3 km²。属于[温带草原气候](../Page/温带草原气候.md "wikilink")。

## 参考资料

[Category:北達科他州城市](../Category/北達科他州城市.md "wikilink")
[B](../Category/美国各州首府.md "wikilink")