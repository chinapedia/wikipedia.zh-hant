**轉角牛羚**（[學名](../Page/學名.md "wikilink")：**），又稱**南非大羚羊**或**黑面狷羚**，是一種生活在[蘇丹](../Page/蘇丹.md "wikilink")、[乍得](../Page/乍得.md "wikilink")、[坦桑尼亞及](../Page/坦桑尼亞.md "wikilink")[南非](../Page/南非.md "wikilink")[大草原及](../Page/大草原.md "wikilink")[氾濫平原的](../Page/氾濫平原.md "wikilink")[狷羚](../Page/狷羚.md "wikilink")。

[Damaliscus_topi.jpg](https://zh.wikipedia.org/wiki/File:Damaliscus_topi.jpg "fig:Damaliscus_topi.jpg")[馬賽馬拉站立的轉角牛羚](../Page/馬賽馬拉.md "wikilink")\]\]

轉角牛羚站立時的肩高為1米，體重80-160公斤。牠們的毛皮一般都是鏽紅色，黑色的[腳部及](../Page/腳.md "wikilink")[胸部](../Page/胸部.md "wikilink")，並在前額至鼻尖有一黑帶。角呈豎琴形，並且明顯是環狀角，雄性及雌性的角都長達70厘米。

轉角牛羚生活在大草原及氾濫平原上，以其上的[草為主要食糧](../Page/草.md "wikilink")。雄性的領域由幾十平方米至幾公里不等，並以[尿液及](../Page/尿液.md "wikilink")[糞便為邊界](../Page/糞便.md "wikilink")。20頭雌性轉角牛羚組成了一群族，並由一頭雄性帶領，而千多頭轉角牛羚亦會同時遷徙。

雄性轉角牛羚會為自己的領域而發生爭鬥，多會以牠們的角刺對方的[膝部](../Page/膝.md "wikilink")。轉角牛羚在受驚時可以奔走達每小時70公里，及會跳越其他牛羚以逃避危險。牠們被認為是最快速的狷羚。

## 外部連結

  - ARKive -
    [轉角牛羚的圖片及映片](http://www.arkive.org/species/GES/mammals/Damaliscus_lunatus/)

[Category:转角牛羚属](../Category/转角牛羚属.md "wikilink")