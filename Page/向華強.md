**向华强**（，），[香港電影出品人](../Page/香港.md "wikilink")、[中國星集團董事會主席和電影監製](../Page/中國星集團.md "wikilink")。

## 簡介

向华强出生于[广东](../Page/广东.md "wikilink")。其父[向前是](../Page/向前.md "wikilink")[中華民國國軍](../Page/中華民國國軍.md "wikilink")[少將](../Page/少將.md "wikilink")。他在向氏13個兄弟中排行第10，妻子是来自台湾的陈嵐（又名[陳明英](../Page/陳明英.md "wikilink")），育有2子：长子[向佐](../Page/向佐.md "wikilink")，二子[向佑](../Page/向佑.md "wikilink")。

向華強多年來醉心於香港電影業，在中華民國軍教片《[黃埔軍魂](../Page/黃埔軍魂.md "wikilink")》電影中出飾一名由[軍校學員升任](../Page/軍校.md "wikilink")[陸軍軍官學校校長的角色](../Page/陸軍軍官學校.md "wikilink")，並因此獲[中華民國國防部頒發](../Page/中華民國國防部.md "wikilink")「陸海空軍褒揚令」表揚。1983年与弟弟[向华胜创办](../Page/向华胜.md "wikilink")[永盛电影公司](../Page/永盛电影公司.md "wikilink")，
其後於1993年分道揚鑣。夫妻二人於1994年成立永盛娛樂（[中國星前身](../Page/中國星.md "wikilink")），并主演过多部[电影](../Page/电影.md "wikilink")，包括，《[藍江傳之反飛組風雲](../Page/藍江傳之反飛組風雲.md "wikilink")》中饰演[蓝江](../Page/藍剛.md "wikilink")、《[赌神](../Page/赌神.md "wikilink")》、《[赌俠](../Page/赌俠.md "wikilink")》、《[赌神2](../Page/赌神2.md "wikilink")》中饰演龙五等，1996年经营[中国星集團有限公司](../Page/中国星集團有限公司.md "wikilink")，出品过電影《[黑金](../Page/黑金.md "wikilink")》、《[龙在江湖](../Page/龙在江湖.md "wikilink")》、《[暗战](../Page/暗战.md "wikilink")》、《[孤男寡女](../Page/孤男寡女.md "wikilink")》、《[瘦身男女](../Page/瘦身男女.md "wikilink")》、《[大隻佬](../Page/大隻佬.md "wikilink")》、《[神探](../Page/神探.md "wikilink")》等影片，其中1992年的《蓝江传》更获提名[第十二屆香港電影金像獎](../Page/第十二屆香港電影金像獎.md "wikilink")[最佳男主角](../Page/香港電影金像獎最佳男主角.md "wikilink")。

李連杰壹基金於2005年首先在香港成立。李連杰及家人與向華強夫婦及家人於2004年在[馬爾代夫渡假親歷](../Page/馬爾代夫.md "wikilink")[南亞海嘯世紀災難後](../Page/南亞海嘯.md "wikilink")，[李連杰決心成立壹基金救災扶貧](../Page/李連杰.md "wikilink")。同樣，向華強及其妻經歷海嘯後，也決定義助李連杰成立[壹基金](../Page/壹基金.md "wikilink")。向華強夫婦不但捐款支持，更出力策劃及向身邊好友募捐。李連杰壹基金除了積極救災扶貧，更開設[北京師範大學壹基金公益研究院](../Page/北京師範大學.md "wikilink")，旨在培育公益人才、傳播公益理念及推動公益實踐。

2007年壹基金將其地域伸延至[中國大陸](../Page/中國大陸.md "wikilink")，籌備初期，向華強夫婦不但捐款支持壹基金在中國大陆的啟動，還向其好友[陳泰銘](../Page/陳泰銘.md "wikilink")、[蔡志明](../Page/蔡志明.md "wikilink")、朱李月華及[李志強夫婦募捐](../Page/李志強.md "wikilink")。另於2010年，向華強妻子陳明英推介李連杰壹基金(香港)捐款100萬給「亮晴工程慈善基金」，為65歲以上低收入[白內障患者免費提供白內障手術](../Page/白內障.md "wikilink")，林順潮教授更親身幫助200位貧苦老人做手術，給予老人家重見光明的機會。

## 电影

### 演员

  - 2016年 《[赌城风云III](../Page/赌城风云III.md "wikilink")》飾演龍五
  - 2004年 《[这个阿爸真爆炸](../Page/这个阿爸真爆炸.md "wikilink")》飾演家长
  - 1994年 《[赌神2](../Page/赌神2.md "wikilink")》飾演 龍五
  - 1992年 《[庙街十二少](../Page/庙街十二少.md "wikilink")》飾演 蓝江
  - 1992年 《[蓝江传之反飞组风云](../Page/蓝江传之反飞组风云.md "wikilink")》飾演 蓝江
  - 1991年 《[赌侠Ⅱ之上海滩赌圣](../Page/赌侠Ⅱ之上海滩赌圣.md "wikilink")》飾演 龍五
  - 1991年 《[五亿探长雷洛传II之父子情仇](../Page/五亿探长雷洛传II之父子情仇.md "wikilink")》飾演 蓝江
  - 1990年 《[赌侠](../Page/赌侠.md "wikilink")》飾演 龍五
  - 1989年 《[赌神](../Page/赌神.md "wikilink")》飾演 龍五
  - 1989年 《[至尊无上](../Page/至尊无上.md "wikilink")》饰演 龙哥
  - 1978年 《[黄埔军魂](../Page/黄埔军魂.md "wikilink")》飾演
    [黃埔軍校校長](../Page/黃埔軍校.md "wikilink")

<!-- end list -->

  - 老虎燕星 (1973)
  - 人魚戀 (1973)
  - 分秒必爭 (1973)
  - 龍虎地頭蛇 (1973)
  - 狂風暴雨 (1973)
  - 猛漢 (1973)
  - 莫名其妙發橫財 (1973)
  - 猛虎鬥狂龍 (1974)
  - 鐵証 (1974) ... 姜華陽
  - 重建精武門 (1975)
  - 咆哮山林 (1975)
  - 英雄榜 (1975) ... 鄭世宏
  - 銀海浪子 (1975)
  - 盲女奇緣 (1975)
  - 鱷潭群英會 (1976)
  - 秋詩篇篇 (1977)
  - 紮馬 (1978)
  - 交貨 (1978)
  - 梁山怪招 (1978)

### 出品

  - 2018年 《[吃貨拳王](../Page/吃貨拳王.md "wikilink")》.... 出品人
  - 2018年 《[真.三國無雙](../Page/真.三國無雙.md "wikilink")》.... 聯合出品人
  - 2016年 《[封神傳奇](../Page/封神傳奇.md "wikilink")》.... 出品人
  - 2016年 《[賭城風雲III](../Page/賭城風雲III.md "wikilink")》.... 聯合出品人
  - 2009年 《[撲克王](../Page/撲克王.md "wikilink")》.... 出品人
  - 2009年 《[再生號](../Page/再生號.md "wikilink")》.... 出品人
  - 2008年 《[内衣少女](../Page/内衣少女.md "wikilink")》 .... 出品人
  - 2008年 《[夺帅](../Page/夺帅.md "wikilink")》 .... 出品人
  - 2008年 《[黑勢力](../Page/黑勢力.md "wikilink")》....出品人
  - 2007年 《[神探](../Page/神探.md "wikilink")》 .... 出品人
  - 2007年 《[降頭](../Page/降頭.md "wikilink")》 .... 出品人
  - 2007年 《[魔鬼天使](../Page/魔鬼天使.md "wikilink")》 .... 出品人
  - 2007年 《[黑拳](../Page/黑拳.md "wikilink")》 .... 出品人
  - 2006年 《[黑社会以和为贵](../Page/黑社会以和为贵.md "wikilink")》 .... 出品人
  - 2006年 《[戀愛初歌](../Page/戀愛初歌.md "wikilink")》 .... 出品人
  - 2006年 《[爱上尸新娘](../Page/爱上尸新娘.md "wikilink")》 .... 出品人
  - 2006年 《[血戰到底](../Page/血戰到底.md "wikilink")》 .... 出品人
  - 2006年 《[最愛女人購物狂](../Page/最愛女人購物狂.md "wikilink")》 .... 出品人
  - 2005年 《[妃子笑](../Page/妃子笑.md "wikilink")》 .... 出品人
  - 2005年 《[黑社會](../Page/黑社會.md "wikilink")》 .... 出品人
  - 2005年 《[雀圣](../Page/雀圣.md "wikilink")》 .... 出品人
  - 2005年 《[凶男寡女](../Page/凶男寡女.md "wikilink")》 .... 出品人
  - 2005年 《[非常青春期](../Page/非常青春期.md "wikilink")》 .... 出品人
  - 2005年 《[做頭](../Page/做頭.md "wikilink")》 .... 出品人
  - 2005年 《[借兵](../Page/借兵.md "wikilink")》 .... 出品人
  - 2005年 《[喜馬拉亞星](../Page/喜馬拉亞星.md "wikilink")》 .... 出品人
  - 2004年 《[小白龍情海翻波](../Page/小白龍情海翻波.md "wikilink")》 .... 出品人
  - 2004年 《[身驕肉貴](../Page/身驕肉貴.md "wikilink")》 .... 出品人
  - 2004年 《[煎釀叁寶](../Page/煎釀叁寶.md "wikilink")》 .... 出品人
  - 2004年 《[柔道龙虎榜](../Page/柔道龙虎榜.md "wikilink")》 .... 出品人
  - 2004年 《[絕世好賓](../Page/絕世好賓.md "wikilink")》 .... 出品人
  - 2004年 《[戀情告急](../Page/戀情告急.md "wikilink")》 .... 出品人
  - 2004年 《[这个阿爸真爆炸](../Page/这个阿爸真爆炸.md "wikilink")》 .... 出品人
  - 2004年 《[七年很癢](../Page/七年很癢.md "wikilink")》 .... 出品人
  - 2004年 《[鬼馬狂想曲](../Page/鬼馬狂想曲.md "wikilink")》 .... 出品人
  - 2003年 《[忘不了](../Page/忘不了.md "wikilink")》 .... 出品人
  - 2003年 《[愛，斷了線](../Page/愛，斷了線.md "wikilink")》 .... 出品人
  - 2003年 《[豪情](../Page/豪情.md "wikilink")》 .... 出品人
  - 2003年 《[絕種鐵金剛](../Page/絕種鐵金剛.md "wikilink")》 .... 出品人
  - 2003年 《[大只佬](../Page/大只佬.md "wikilink")》 .... 出品人
  - 2003年 《[少年往事](../Page/少年往事.md "wikilink")》 .... 出品人
  - 2003年 《[我的麻煩老友](../Page/我的麻煩老友.md "wikilink")》 .... 出品人
  - 2003年 《[戀上你的床](../Page/戀上你的床.md "wikilink")》 .... 出品人
  - 2003年 《[黑白森林](../Page/黑白森林.md "wikilink")》 .... 出品人
  - 2003年 《[我的婆婆黃飛鴻](../Page/我的婆婆黃飛鴻.md "wikilink")》 .... 出品人
  - 2003年 《[低一點的天空](../Page/低一點的天空.md "wikilink")》 .... 出品人
  - 2003年 《[絕種好男人](../Page/絕種好男人.md "wikilink")》 .... 出品人
  - 2003年 《[失憶𠝹女王](../Page/失憶𠝹女王.md "wikilink")》 .... 出品人

<!-- end list -->

  - 2003年 《[奇逢敵手](../Page/奇逢敵手.md "wikilink")》 .... 出品人
  - 2003年 《[百年好合](../Page/百年好合.md "wikilink")》 .... 出品人
  - 2003年 《[黑俠II](../Page/黑俠II.md "wikilink")》 .... 出品人
  - 2002年 《[每天嚇你8小時](../Page/每天嚇你8小時.md "wikilink")》 .... 出品人
  - 2002年 《[我家有一隻河東獅](../Page/我家有一隻河東獅.md "wikilink")》 .... 出品人
  - 2002年 《[絕世好B](../Page/絕世好B.md "wikilink")》 .... 出品人
  - 2002年 《[我左眼見到鬼](../Page/我左眼見到鬼.md "wikilink")》 .... 出品人
  - 2002年 《[豬扒大聯盟](../Page/豬扒大聯盟.md "wikilink")》 .... 出品人
  - 2002年 《[衛斯理藍血人](../Page/衛斯理藍血人.md "wikilink")》 .... 出品人
  - 2002年 《[嚦咕嚦咕新年財](../Page/嚦咕嚦咕新年財.md "wikilink")》 .... 出品人
  - 2002年 《[無限復活](../Page/無限復活.md "wikilink")》 .... 出品人
  - 2002年 《[暗戰2](../Page/暗戰2.md "wikilink")》 .... 出品人
  - 2001年 《[男歌女唱](../Page/男歌女唱.md "wikilink")》 .... 出品人
  - 2001年 《[廢柴同盟](../Page/廢柴同盟.md "wikilink")》 .... 出品人
  - 2001年 《[絕世好Bra](../Page/絕世好Bra.md "wikilink")》 .... 出品人
  - 2001年 《[蜀山傳](../Page/蜀山傳.md "wikilink")》 .... 出品人
  - 2001年 《[瘦身男女](../Page/瘦身男女.md "wikilink")》 .... 出品人
  - 2001年 《[野獸之瞳](../Page/野獸之瞳.md "wikilink")》 .... 出品人
  - 2001年 《[愛上我吧](../Page/愛上我吧.md "wikilink")》 .... 出品人
  - 2001年 《[等候董建華發落](../Page/等候董建華發落.md "wikilink")》 .... 出品人
  - 2001年 《[老夫子2001](../Page/老夫子2001.md "wikilink")》 .... 出品人
  - 2001年 《[钟无艳](../Page/钟无艳.md "wikilink")》 .... 出品人
  - 2001年 《[頭號人物](../Page/頭號人物.md "wikilink")》 .... 出品人
  - 2000年 《[天有眼](../Page/天有眼.md "wikilink")》 .... 出品人
  - 2000年 《[江湖告急](../Page/江湖告急.md "wikilink")》 .... 出品人
  - 2000年 《[辣手回春](../Page/辣手回春.md "wikilink")》 .... 出品人
  - 2000年 《[恋战冲绳](../Page/恋战冲绳.md "wikilink")》 .... 出品人
  - 2000年 《[孤男寡女](../Page/孤男寡女.md "wikilink")》 .... 出品人
  - 2000年 《[極速傳說](../Page/極速傳說.md "wikilink")》 .... 出品人
  - 2000年 《[决战紫禁之巅](../Page/决战紫禁之巅.md "wikilink")》 .... 出品人
  - 2000年 《[生死拳速](../Page/生死拳速.md "wikilink")》 .... 出品人
  - 2000年 《[笨小孩](../Page/笨小孩.md "wikilink")》 .... 出品人
  - 1999年 《[我愛777](../Page/我愛777.md "wikilink")》 .... 出品人
  - 1999年 《[暗战](../Page/暗战.md "wikilink")》 .... 出品人
  - 1999年 《[赌侠大战拉斯维加斯](../Page/赌侠大战拉斯维加斯.md "wikilink")》 .... 出品人
  - 1999年 《[原始武器](../Page/原始武器.md "wikilink")》 .... 出品人
  - 1999年 《[黑馬王子](../Page/黑馬王子.md "wikilink")》 .... 出品人
  - 1998年 《[賭俠1999](../Page/賭俠1999.md "wikilink")》 .... 出品人
  - 1998年 《[龍在江湖](../Page/龍在江湖.md "wikilink")》 .... 出品人
  - 1998年 《[杀手之王](../Page/杀手之王.md "wikilink")》 .... 出品人
  - 1998年 《[碧血藍天](../Page/碧血藍天.md "wikilink")》 .... 出品人
  - 1998年 《[陰陽路之升棺發財](../Page/陰陽路之升棺發財.md "wikilink")》 .... 出品人
  - 1997年 《[黑金](../Page/黑金.md "wikilink")》 .... 出品人
  - 1997年 《[恐怖雞](../Page/恐怖雞.md "wikilink")》 .... 出品人
  - 1997年 《[龍城正月](../Page/龍城正月.md "wikilink")》 .... 出品人

<!-- end list -->

  - 1997年 《[誤人子弟](../Page/誤人子弟.md "wikilink")》 .... 出品人
  - 1997年 《[阴阳路之我在你左右](../Page/阴阳路之我在你左右.md "wikilink")》 .... 出品人
  - 1997年 《[對不起，多謝你](../Page/對不起，多謝你.md "wikilink")》 .... 出品人
  - 1997年 《[求戀期](../Page/求戀期.md "wikilink")》 .... 出品人
  - 1997年 《[天地雄心](../Page/天地雄心.md "wikilink")》 .... 出品人
  - 1997年 《[黄飞鸿之西域雄狮](../Page/黄飞鸿之西域雄狮.md "wikilink")》 .... 出品人
  - 1996年 《[黑侠](../Page/黑侠.md "wikilink")》 .... 出品人
  - 1996年 《[新上海灘](../Page/新上海灘.md "wikilink")》 .... 出品人
  - 1997年 《[天若有情Ⅲ之烽火佳人](../Page/天若有情.md "wikilink")》 .... 出品人
  - 1996年 《[冒险王](../Page/冒险王.md "wikilink")》 .... 出品人
  - 1996年 《[大內密探零零發](../Page/大內密探零零發.md "wikilink")》 .... 出品人
  - 1996年 《[飛虎隊](../Page/飛虎隊.md "wikilink")》 .... 出品人
  - 1995年 《[烈火战车](../Page/烈火战车.md "wikilink")》 .... 出品人
  - 1995年 《[山水有相逢](../Page/山水有相逢.md "wikilink")》 .... 出品人
  - 1995年 《[呆老拜壽](../Page/呆老拜壽.md "wikilink")》 .... 出品人
  - 1995年 《[百變星君](../Page/百變星君.md "wikilink")》 .... 出品人
  - 1995年 《[大冒險家](../Page/大冒險家.md "wikilink")》 .... 出品人
  - 1995年 《[小飞侠](../Page/小飞侠.md "wikilink")》 .... 出品人
  - 1995年 《[慈禧秘密生活](../Page/慈禧秘密生活.md "wikilink")》 .... 出品人
  - 1995年 《[給爸爸的信](../Page/給爸爸的信.md "wikilink")》 .... 出品人
  - 1994年 《[錦繡前程](../Page/錦繡前程.md "wikilink")》 .... 出品人
  - 1994年 《[九品芝麻官](../Page/九品芝麻官.md "wikilink")》 .... 出品人
  - 1994年 《[新天龍八部之天山童姥](../Page/新天龍八部之天山童姥.md "wikilink")》 .... 出品人
  - 1994年 《[天與地](../Page/天與地_\(1994年電影\).md "wikilink")》 .... 出品人
  - 1993年 《[唐伯虎点秋香](../Page/唐伯虎点秋香.md "wikilink")》 .... 出品人
  - 1993年 《[黄飞鸿之铁鸡斗蜈蚣](../Page/黄飞鸿之铁鸡斗蜈蚣.md "wikilink")》 .... 出品人
  - 1993年 《[倚天屠龍記之魔教教主](../Page/倚天屠龍記之魔教教主.md "wikilink")》 .... 出品人
  - 1993年 《[武状元苏乞儿](../Page/武状元苏乞儿.md "wikilink")》 .... 出品人
  - 1993年 《[逃学威龙3之龙过鸡年](../Page/逃学威龙3之龙过鸡年.md "wikilink")》 .... 出品人
  - 1993年 《[护花情狂](../Page/护花情狂.md "wikilink")》 .... 出品人
  - 1992年 《[黑豹传说](../Page/黑豹传说.md "wikilink")》 .... 出品人
  - 1992年 《[战龙在野](../Page/战龙在野.md "wikilink")》 .... 监制
  - 1992年 《[庙街十二少](../Page/庙街十二少.md "wikilink")》 .... 出品人
  - 1991年 《[非洲和尚](../Page/非洲和尚.md "wikilink")》 .... 出品人
  - 1990年 《[皇家赌船](../Page/皇家赌船.md "wikilink")》 .... 出品人
  - 1990年 《[捉鬼合家欢](../Page/捉鬼合家欢.md "wikilink")》 .... 监制
  - 1989年 《[追女重案组](../Page/追女重案组.md "wikilink")》 .... 监制
  - 1989年 《[傲气雄鹰](../Page/傲气雄鹰.md "wikilink")》 .... 监制
  - 1989年 《[赌神](../Page/赌神.md "wikilink")》 .... 出品人
  - 1988年 《[火舞风云](../Page/火舞风云.md "wikilink")》 .... 出品人
  - 1987年 《[義本無言](../Page/義本無言.md "wikilink")》 .... 出品人

## 电视剧

  - 1999年 《[中華大丈夫](../Page/中華大丈夫.md "wikilink")》
  - 1998年 《[難得有情人](../Page/難得有情人.md "wikilink")》
  - 1998年 《[情牽日月星](../Page/情牽日月星.md "wikilink")》
  - 1998年 《[江山美人](../Page/江山美人.md "wikilink")》
  - 1998年 《[球愛情緣](../Page/球愛情緣.md "wikilink")》
  - 1998年 《[食神](../Page/食神.md "wikilink")》
  - 1998年 《[小兵阿福](../Page/小兵阿福.md "wikilink")》
  - 1998年 《[新少年五祖](../Page/新少年五祖.md "wikilink")》
  - 1998年 《[祝君早安](../Page/祝君早安.md "wikilink")》
  - 1998年 《[上海之戀](../Page/上海之戀.md "wikilink")》
  - 1997年 《[原野](../Page/原野.md "wikilink")》
  - 1997年 《[鴉片戰爭演義](../Page/鴉片戰爭演義.md "wikilink")》
  - 1997年 《[绝地苍狼](../Page/绝地苍狼.md "wikilink")》
  - 1997年 《[花和尚外傳](../Page/花和尚外傳.md "wikilink")》
  - 1997年 《[儂本多情](../Page/儂本多情.md "wikilink")》
  - 1997年 《[縱橫四海](../Page/縱橫四海.md "wikilink")》

## 參考資料

  - [李连杰和女儿海啸生还
    创立“壹基金”做慈善](http://ent.sina.com.cn/s/h/2006-12-26/14211386871.html)

## 外部連結

  -
  -
  -
  -
  -
  -
[Category:向前家族](../Category/向前家族.md "wikilink")
[Category:陆丰人](../Category/陆丰人.md "wikilink")
[Category:香港潮汕人](../Category/香港潮汕人.md "wikilink")
[Category:香港電影人](../Category/香港電影人.md "wikilink")
[Category:香港電影監製](../Category/香港電影監製.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港三合會成員](../Category/香港三合會成員.md "wikilink")
[H](../Category/民生書院校友.md "wikilink")
[Huaqiang](../Category/向姓.md "wikilink")
[Category:中國星集團](../Category/中國星集團.md "wikilink")