**亞歐外長會議**是[亞歐基金旗下的](../Page/亞歐基金.md "wikilink")[亞歐會議分支出來的組織](../Page/亞歐會議.md "wikilink")，由[亞洲及](../Page/亞洲.md "wikilink")[歐洲各國外長一起舉行的會議](../Page/歐洲.md "wikilink")。現時會議的成員包括：[中國](../Page/中華人民共和國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[韩国](../Page/大韓民國.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[东盟](../Page/东盟.md "wikilink")10国、[印度](../Page/印度.md "wikilink")、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[欧盟](../Page/欧盟.md "wikilink")27国的外长或代表，合共45个成员國\[1\]。亞歐外長會議的首次會議於公元1997年於[新加坡舉辦](../Page/新加坡.md "wikilink")\[2\]，之後由各國輪流主持。

## 歷屆會議

  - 第一屆（1997年）：2月14日-15日在[新加坡舉行](../Page/新加坡.md "wikilink")\[3\]
  - 第二屆（1999年）：3月29日在[德國](../Page/德國.md "wikilink")[柏林舉行](../Page/柏林.md "wikilink")\[4\]
  - 第三屆（2001年）：5月24日-25日在[中國](../Page/中國.md "wikilink")[北京舉行](../Page/北京.md "wikilink")\[5\]
  - 第四屆（2002年）：6月6日-7日在[西班牙](../Page/西班牙.md "wikilink")[馬德里舉行](../Page/馬德里.md "wikilink")\[6\]
  - 第五屆（2003年）：7月24日在[印尼](../Page/印尼.md "wikilink")[峇里](../Page/峇里.md "wikilink")\[7\]
  - 第六屆（2004年）：[4月17](../Page/4月17.md "wikilink")-18日在[愛爾蘭](../Page/愛爾蘭.md "wikilink")[基爾代爾舉行](../Page/基爾代爾.md "wikilink")\[8\]
  - 第七屆（2005年）：5月6日-7日在[日本](../Page/日本.md "wikilink")[京都舉行](../Page/京都.md "wikilink")\[9\]
  - 第八屆（2007年）：5月28日-29日在[德国](../Page/德国.md "wikilink")[汉堡举行](../Page/汉堡.md "wikilink")\[10\]。
      - 本屆新成員：[印度](../Page/印度.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[東盟秘書處](../Page/東盟.md "wikilink")、[羅馬尼亞和](../Page/羅馬尼亞.md "wikilink")[保加利亞](../Page/保加利亞.md "wikilink")\[11\]
      - 議題：氣候變化、能源安全、反恐、[防止核武器擴散](../Page/不擴散核武器條約.md "wikilink")、世界貿易等全球性問題，也包括[阿富汗](../Page/阿富汗.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[朝鮮半島無核化和](../Page/朝鮮半島無核化.md "wikilink")[中東局勢等國際熱點問題](../Page/中東.md "wikilink")\[12\]。

<!-- end list -->

  - 第九屆 (2009年) :
    5月25日至26日在[越南](../Page/越南.md "wikilink")[河内](../Page/河内.md "wikilink")

<!-- end list -->

  - 第十屆 (2011年) :
    6月7日在[匈牙利](../Page/匈牙利.md "wikilink")[格德勒](../Page/格德勒.md "wikilink")

<!-- end list -->

  - 第十一屆 (2013年) :
    11月11日在[印度](../Page/印度.md "wikilink")[新德里](../Page/新德里.md "wikilink")

<!-- end list -->

  - 第十二屆 (2015年) : 11月5日至6日在[盧森堡](../Page/盧森堡.md "wikilink")

<!-- end list -->

  - 第十三屆 (2017年) : 在[緬甸](../Page/緬甸.md "wikilink")

## 參考

<references />

## 參看

  - [亚欧会议](../Page/亚欧会议.md "wikilink")

## 外部連結

  -
[亚欧会议](../Category/亚欧会议.md "wikilink")

1.  [新華網：第八届亚欧外长会议在德国汉堡开幕
    杨洁篪出席](http://news.xinhuanet.com/world/2007-05/29/content_6165823.htm)

2.  [人民網—亞歐會議大事記](http://www.people.com.cn/BIG5/shizheng/8198/39213/39217/2905541.html)

3.
4.
5.
6.
7.  [李肇星外长在第五届亚欧外长会议上的发言](http://www.fmprc.gov.cn/ce/ceee/chn/dtxw/t107125.htm)

8.  [第六届亚欧外长会议主席声明](http://www.fmprc.gov.cn/chn/wjb/zzjg/gjs/gjzzyhy/1132/1134/t127974.htm)


9.  [新華網：第七届亚欧外长会议在京都开幕](http://news.xinhuanet.com/world/2005-05/06/content_2924105.htm)

10. [新华网：杨洁篪出席第八届亚欧外长会议](http://news.xinhuanet.com/world/2007-05/29/content_6170790.htm)

11.
12. [大公網：亞歐外長會議發表主席聲明](http://www.takungpao.com/news/07/05/30/YM-743925.htm)