[Mizoramdistrictsmap.png](https://zh.wikipedia.org/wiki/File:Mizoramdistrictsmap.png "fig:Mizoramdistrictsmap.png")

**米佐拉姆邦**（[印地語](../Page/印地語.md "wikilink")：****，[拉丁字母轉寫](../Page/拉丁字母.md "wikilink")：）是位於[印度東北部的一個邦](../Page/印度東北部.md "wikilink")。於2001年人口普查中邦人口約為890,000。面积21081km²。米佐拉姆邦的[識字率高達](../Page/識字率.md "wikilink")89%，於印度各邦中僅次於[喀拉拉邦](../Page/喀拉拉邦.md "wikilink")。

## 民族

米佐拉姆邦的主要民族是[米佐族](../Page/米佐人.md "wikilink")（[Mizo
people](../Page/:en:Mizo_people.md "wikilink")），使用[米佐语](../Page/米佐语.md "wikilink")。米佐族分為很多的亞族，其中最大的是[Lushai](../Page/:en:Lushai.md "wikilink")（卢赛部落），幾乎佔了全邦三分之二的人口。其他亞族包括有[Ralte](../Page/:en:Ralte.md "wikilink")、[Hmar](../Page/:en:Hmar.md "wikilink")、[Paihte](../Page/:en:Paite.md "wikilink")、[Poi](../Page/:en:Poi.md "wikilink")、[Mara及](../Page/:en:Mara.md "wikilink")[Pawi](../Page/:en:Pawi.md "wikilink")，此外還由不屬米佐族，源自[Arakanese的](../Page/:en:Arakanese.md "wikilink")[Chakma族](../Page/:en:Chakma.md "wikilink")（[查克瑪人](../Page/查克瑪人.md "wikilink")）。

## 语言

该邦的官方语言为[米佐语和](../Page/米佐语.md "wikilink")[英语](../Page/英语.md "wikilink")。

## 宗教

全邦約87%的人口为[基督徒](../Page/基督徒.md "wikilink")，是印度三个以基督教為主的邦之一。少数人信仰佛教或印度教。

几乎所有的米佐族人都信奉[基督教](../Page/基督教.md "wikilink")，大多屬于[長老宗或](../Page/長老宗.md "wikilink")[浸信宗](../Page/浸信宗.md "wikilink")。另外少数民族查克瑪族信奉混合了[印度教和萬物有靈成份的](../Page/印度教.md "wikilink")[南传佛教](../Page/南传佛教.md "wikilink")。

1980年代，一位本地研究者宣佈米佐人是「[以色列人](../Page/以色列人.md "wikilink")[失落的支派](../Page/失踪的十支派.md "wikilink")」，其後大約有5,000名米佐人和[庫奇人](../Page/庫奇人.md "wikilink")（與米佐人相關的一個民族）在近年歸信了[猶太教](../Page/猶太教.md "wikilink")。然而邦內最具影響力、差不多包括了75万米佐族人口的基督教會认为這種說法是無稽之談。

然而於2005年4月1日，[以色列的](../Page/以色列.md "wikilink")[瑟法底犹太人](../Page/瑟法底犹太人.md "wikilink")（Sephardic
Jews）首席[拉比Shlomo](../Page/拉比.md "wikilink")
Amar宣佈確認此猶太教社群為可信的以色列失落支派，並同時派遣一小隊拉比到印度為該社群舉行[犹太教正统派](../Page/犹太教正统派.md "wikilink")（Orthodox
Judaism）的正式歸信儀式。根據以色列的[回歸法](../Page/回歸法.md "wikilink")，此批自稱屬[瑪拿西支派的米佐猶太人將可移居以色列](../Page/瑪拿西.md "wikilink")。雖然該社群的男性[基因](../Page/基因.md "wikilink")（見[Y染色體](../Page/Y染色體.md "wikilink")）找不到與[猶太人的任何聯繫](../Page/猶太人.md "wikilink")，但在女性的基因中卻找到明顯的中東人特徵，這正符合認定猶太人的常規：母親是猶太人則子女就是猶太人。

## 經濟

### 主要經濟走向

下表是表示米佐拉姆邦邦內生產總值
[估計](https://web.archive.org/web/20040626084721/http://mospi.nic.in/mospi_nad_main.htm)由印度統計和計畫部門所公佈的資料，單位為百萬[印度盧比](../Page/印度盧比.md "wikilink")

| 年份    | 米佐拉姆邦邦內生產總值 |
| ----- | ----------- |
| 1980年 | 680         |
| 1985年 | 1,810       |
| 1990年 | 3,410       |
| 1995年 | 9,370       |
| 2000年 | 17,690      |

[Category:米佐拉姆邦](../Category/米佐拉姆邦.md "wikilink")
[邦](../Category/印度的邦和中央直辖区.md "wikilink")