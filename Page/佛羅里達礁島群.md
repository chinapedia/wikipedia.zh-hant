[Conch_Republic_map.png](https://zh.wikipedia.org/wiki/File:Conch_Republic_map.png "fig:Conch_Republic_map.png")
[Floridakeys-nasa.jpg](https://zh.wikipedia.org/wiki/File:Floridakeys-nasa.jpg "fig:Floridakeys-nasa.jpg")衛星照片下的佛羅里達礁島群。\]\]
**佛羅里達礁島群**（）是一個位於[美國](../Page/美國.md "wikilink")[佛羅里達州南部外海的](../Page/佛羅里達州.md "wikilink")[群島](../Page/群島.md "wikilink")，地處美國最東南端的該群島，是由總數約1700座的大小[島嶼所組成](../Page/島嶼.md "wikilink")。群島從[佛羅里達半島東南角靠近](../Page/佛羅里達半島.md "wikilink")[邁阿密處一路往南分佈並漸漸彎曲向西](../Page/邁阿密.md "wikilink")，直到最底端的[西礁島](../Page/西礁島.md "wikilink")（又常音譯為「基威斯特島」（），意指其位於礁島群的最西端），往外則有無人居住的[海龟岛](../Page/海龟国家公园.md "wikilink")。這些島嶼位在[佛羅里達海峽中](../Page/佛羅里達海峽.md "wikilink")，將[大西洋與](../Page/大西洋.md "wikilink")[墨西哥灣分為東西兩處](../Page/墨西哥灣.md "wikilink")，並大致劃出[佛羅里達灣的範圍](../Page/佛羅里達灣.md "wikilink")。西礁島最南角是距離鄰國[古巴最近的美國領土](../Page/古巴.md "wikilink")，之間相距僅90[哩](../Page/哩.md "wikilink")（145[公里](../Page/公里.md "wikilink")）。

佛羅里達礁島群有95%的範圍都位在[門羅縣](../Page/门罗县_\(佛罗里达州\).md "wikilink")（Monroe
County）境內，只有[托頓島](../Page/托頓島.md "wikilink")（Totten
Key）等小部分的範圍，劃屬隔鄰的[邁阿密-戴德郡](../Page/邁阿密-戴德郡.md "wikilink")（Miami-Dade
County）。[基韋斯特](../Page/基韋斯特_\(佛羅里達州\).md "wikilink")（City of Key
West）是門羅郡的[縣治](../Page/縣治.md "wikilink")，該郡治理的範圍包含一部份位於佛羅里達本土的土地，以及從[大礁島](../Page/大礁島.md "wikilink")（Key
Largo）開始往西南方向的所有島嶼。其中，位於本土上的門羅郡領土幾乎都位在[大沼澤地國家公園](../Page/大沼澤地國家公園.md "wikilink")（Everglades）內。

## 主要島嶼

[美國1號公路](../Page/美國1號公路.md "wikilink")，此處稱「[跨海公路](../Page/跨海公路_\(美國\).md "wikilink")」行經佛羅里達群島的居住島嶼。這些島嶼列如後文，按照的次序是從東北往西南方向。

### 上群島

[Islamorada_Florida.jpg](https://zh.wikipedia.org/wiki/File:Islamorada_Florida.jpg "fig:Islamorada_Florida.jpg")

  - [大礁島](../Page/大礁島.md "wikilink")（Key Largo）
  - [農園礁島](../Page/農園礁島.md "wikilink")（Plantation Key）
  - [Windley Key](../Page/Windley_Matecumbe_Key,_Florida.md "wikilink")
  - [Upper Matecumbe
    Key](../Page/Upper_Matecumbe_Key,_Florida.md "wikilink")
  - [下馬泰坎伯礁島](../Page/下馬泰坎伯礁島.md "wikilink")（Lower Matecumbe Key）

農園礁島到下馬泰坎伯礁島合稱[伊斯拉摩拉達](../Page/伊斯拉摩拉達.md "wikilink")（Islamorada），意指「島村」（佛羅里達觀光導覽解為[西班牙文的](../Page/西班牙文.md "wikilink")「紫色之島」）。大礁島的「城中心」與[塔芬尼爾則沒算入](../Page/塔芬尼爾_\(佛羅里達群島\).md "wikilink")。

### 中群島

  - [Craig Key](../Page/Craig_Key,_Florida.md "wikilink")
  - [喜慶日礁島](../Page/喜慶日礁島.md "wikilink")（Fiesta Key）
  - [Rattlesnake Key](../Page/Rattlesnake_Key,_Florida.md "wikilink")
  - [長礁島](../Page/長礁島.md "wikilink")（Long Key）
  - [海螺礁島](../Page/海螺礁島.md "wikilink")（Conch Key）
  - [鴨礁島](../Page/鴨礁島.md "wikilink")（Duck Key）
  - [綠茵礁島](../Page/綠茵礁島.md "wikilink")（Grassy Key）
  - [鹿礁島](../Page/鹿礁島.md "wikilink")（Deer Key）
  - [Key Vaca](../Page/Key_Vaca,_Florida.md "wikilink")
  - [馬拉松島](../Page/馬拉松島.md "wikilink")（Marathon）
  - [靴礁島](../Page/靴礁島.md "wikilink")（Boot Key）

### 下群島

  - [巴伊亞翁達](../Page/巴伊亞翁達.md "wikilink")（Bahia Honda）
  - [West Summerland
    Key](../Page/West_Summerland_Key,_Florida.md "wikilink")
  - [無名礁島](../Page/無名礁島.md "wikilink")（No Name Key）
  - [大松礁島](../Page/大松礁島.md "wikilink")（Big Pine Key）
  - [火炬礁島](../Page/火炬礁島.md "wikilink")（Torch Key）
  - [Ramrod Key](../Page/Ramrod_Key,_Florida.md "wikilink")
  - [夏地礁島](../Page/夏地礁島.md "wikilink")（Summerland Key）
  - [Cudjoe Key](../Page/Cudjoe_Key,_Florida.md "wikilink")
  - [Sugarloaf Key](../Page/Sugarloaf_Key,_Florida.md "wikilink")
  - [Saddlebunch Keys](../Page/Saddlebunch_Keys,_Florida.md "wikilink")
  - [Big Coppitt Key](../Page/Big_Coppitt_Key,_Florida.md "wikilink")
  - [Boca Chica](../Page/Boca_Chica,_Florida.md "wikilink")
  - [Key Haven](../Page/Key_Haven,_Florida.md "wikilink")
  - [西礁島](../Page/西礁島.md "wikilink")（Key West）

### 外圍島嶼

可以透過船隻到達。

  - [比斯坎國家公園](../Page/比斯坎國家公園.md "wikilink")（Biscayne National Park）的群島

<!-- end list -->

  -
    礁島（Transitional keys）
      - [Soldier礁島](../Page/Soldier礁島.md "wikilink")（Soldier Key）
      - [Ragged礁島](../Page/Ragged礁島.md "wikilink")（Ragged Key）
      - [Boca Chita礁島](../Page/Boca_Chita礁島.md "wikilink")（Boca Chita
        Key）
      - [Sands礁島](../Page/Sands礁島.md "wikilink")（Sands Key）
    True Florida keys, exposed ancient coral reefs
      - [Elliott Key,
        Florida](../Page/Elliott_Key,_Florida.md "wikilink")
      - [亞當斯礁島](../Page/亞當斯礁島.md "wikilink")（Adams Key）
      - [Reid礁島](../Page/Reid礁島.md "wikilink")（Reid Key）
      - [Rubicon礁島](../Page/Rubicon礁島.md "wikilink")（Rubicon Key）
      - [Totten礁島](../Page/Totten礁島.md "wikilink")（Totten Key）
      - [Old Rhodes礁島](../Page/Old_Rhodes礁島.md "wikilink")（Old Rhodes
        Key）
    among others

<!-- end list -->

  - the [Marquesas Keys](../Page/Marquesas_Keys.md "wikilink")
  - [乾龜國家公園](../Page/乾龜國家公園.md "wikilink")：

:\*[乾龜群島](../Page/乾龜群島.md "wikilink")（Dry Tortugas，沒有在地圖上標示）

## 交通

多數島嶼通過[跨海公路連接](../Page/跨海公路_\(美國\).md "wikilink")。過去曾有過鐵路，但在1935年後就停駛。*參見[歷史部份](../Page/佛羅里達群島#歷史.md "wikilink")。*

## 歷史

### 跨海鐵路

過去群島曾長時間只能透過水路抵達。而這狀況在1910年代早期，[亨利·富萊格勒](../Page/亨利·富萊格勒.md "wikilink")（Henry
Flagler）完成了他的[跨海鐵路之後就做了轉變](../Page/跨海鐵路_\(美國\).md "wikilink")。富萊格勒，一位佛羅里達大西洋岸的主要開發者，運用為數驚人的跨海鐵道[棧橋](../Page/棧橋.md "wikilink")（trestle），將他的[佛羅里達東岸鐵路延伸到奇威斯特](../Page/佛羅里達東岸鐵路.md "wikilink")。

### 1935年勞動節颶風

  -
    ''主條目：[1935年勞動節颶風](../Page/1935年勞動節颶風.md "wikilink")

超過400人罹難，雖然有些估計聲稱超過600人死於這場恐怖的風暴。

大約從西元1850年左右，美國海岸的天氣開始被有計畫的紀錄，史上只有三個[薩菲爾-辛普森颶風等級五級颶風登陸北美大陸](../Page/薩菲爾-辛普森颶風等級.md "wikilink").
1935年勞動節颶風就是其中之一，另外兩個分別是1969年的[Camille和](../Page/Hurricane_Camille.md "wikilink")1992年的[Andrew](../Page/Hurricane_Andrew.md "wikilink").

於1935年，為了連接群島全體的公路而造的新橋正在興建。數以百計的[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")[退役軍人在其上築路](../Page/退役軍人.md "wikilink")。

這場風暴也終止了23年的跨海鐵路經營；受毀損的軌道就未再重建，而跨海公路（Overseas
Highway，即在群島上這段的美國1號公路，U.S. Highway
1）取代了鐵路，成為從邁阿密到奇威斯特的主要交通路線。

### 七哩長橋

興建當時為[極長橋之一](../Page/極長橋.md "wikilink")，[七哩長橋連結了](../Page/七哩長橋.md "wikilink")[Vaca
Key](../Page/Vaca_Key.md "wikilink")（中群島的[馬拉松之島上城鎮](../Page/馬拉松_\(佛羅里達群島\).md "wikilink")）直到下群島的[巴伊亞翁達](../Page/巴伊亞翁達_\(佛羅里達群島\).md "wikilink")（根據[西班牙文拼作](../Page/西班牙文.md "wikilink")*ba-EE-uh
OWN-dah*）。一如其名，它有七哩或約十一公里長，並越過鴿島（Pigeon Key），而舊橋的一段則允許下到這個小島上。

在颶風摧毀了鐵路之後，美國聯邦政府興建了這座橋與其他橋墩，作為[汽車通行用的](../Page/汽車.md "wikilink")[公路](../Page/公路.md "wikilink")。[美國1號公路](../Page/美國1號公路.md "wikilink")（US
1）行經了整段群島長，並且在此處稱為[跨海公路](../Page/跨海公路_\(美國\).md "wikilink")（US
1也行經整個[美國東岸直到](../Page/美國東岸.md "wikilink")[緬因州](../Page/緬因州.md "wikilink")。）

### 海螺共和國

在1982年，[美利堅合眾國邊境巡邏局在美國](../Page/美利堅合眾國邊境巡邏局.md "wikilink")1號公路設下路障及路檢點，將所有往北開回大陸上相鄰城市——[佛羅里達市的車流阻擋下來](../Page/佛羅里達市.md "wikilink")，以在各個車輛上搜索違禁藥品及非法移民。奇威斯特市議會對這些發出反覆的抱怨，稱這些路障對於從奇威斯特離開的人們造成重大的不便，並且傷害了對群島來說很重要的觀光事業。

在多次無效的抱怨，以及在[邁阿密的聯邦法庭中](../Page/邁阿密.md "wikilink")，取得撤消路障強制令的嘗試之舉失敗以後，於1982年4月23日，奇威斯特市長[丹尼斯·沃德婁](../Page/丹尼斯·沃德婁.md "wikilink")（Dennis
Wardlow）及市議會宣布了群島獨立，國名稱作[海螺共和國](../Page/海螺共和國.md "wikilink")。在一分鐘的脫離獨立後，他（作為「總統」）向奇威斯特海軍航空站（Key
West Naval Air Station,
NAS）的一名軍官投降，並請求十億美元（$1,000,000,000）的「[外援](../Page/外援.md "wikilink")」。

這樣的噱頭成功地向大眾傳達群島的困境，而檢查站路障最終撤除。

## 外部連結

  - [A Gazetteer of the Florida Keys](http://keys.fiu.edu/gazetteer)

  - [KEYS HISTOREUM](http://www.keyshistory.org/)

  - [City of Key
    West](https://web.archive.org/web/20110917043054/http://www.keywestcity.com/)

  - [History of the Conch
    Republic](https://web.archive.org/web/20121102113712/http://www.conchrepublic.com/history.htm)

  - [More about the
    Marquesas](http://www.bonefishingkeywest.com/marquesas.htm)

  - [National Park Service: Dry Tortugas](http://www.nps.gov/drto)

  - [NOAA Marine
    Sanctuary](https://web.archive.org/web/20031003221709/http://www.fknms.nos.noaa.gov/)

  - [Key West](http://www.keywesttravelguide.com) Vacation and visitor
    information to the Southernmost City.

  - [Keysplants.com](https://web.archive.org/web/20130612200510/http://keysplants.com/)
    Florida Keys native plants and ecology

[category:佛羅里達礁島群](../Page/category:佛羅里達礁島群.md "wikilink")

[Category:南佛罗里达](../Category/南佛罗里达.md "wikilink")
[Category:佛罗里达州各地区](../Category/佛罗里达州各地区.md "wikilink")