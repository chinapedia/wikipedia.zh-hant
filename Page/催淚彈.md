[Gren_Hand_Burst_Irrt-_A.Riot-_N225.jpg](https://zh.wikipedia.org/wiki/File:Gren_Hand_Burst_Irrt-_A.Riot-_N225.jpg "fig:Gren_Hand_Burst_Irrt-_A.Riot-_N225.jpg")氣體。\]\]
[Tear_gas_bomb.JPG](https://zh.wikipedia.org/wiki/File:Tear_gas_bomb.JPG "fig:Tear_gas_bomb.JPG")

**催淚彈**（，亦可視乎內含催淚劑而命名為或），亦稱**催泪瓦斯**、**催淚氣體**（）或**催淚煙**（），是一種以[催淚性毒剂為](../Page/催淚性毒剂.md "wikilink")[有效載荷的](../Page/有效載荷.md "wikilink")[彈藥](../Page/彈藥.md "wikilink")，施放後可快速及大範圍擴散，屬於[非致命性武器](../Page/非致命性武器.md "wikilink")。現為各國[军队及](../Page/军队.md "wikilink")[警察廣泛應用於](../Page/警察.md "wikilink")[防暴用途](../Page/防暴.md "wikilink")，一般將其分為爆炸式及燃燒式兩種。前者常利用[榴彈發射器](../Page/榴彈發射器.md "wikilink")、較大口徑之[霰彈槍](../Page/霰彈槍.md "wikilink")（如[雷明登870泵動式霰彈槍](../Page/雷明登870泵動式霰彈槍.md "wikilink")）或配備相應附件的槍械發射，通過炸藥爆炸將毒劑迅速釋放到目標區域，易對人體造成嚴重傷害；後者則能夠產生足夠氣化毒劑的高溫，起效速度不如前者亦較為安全，常透過手擲使用，容易讓投擲對象躲過，甚至反擲。現時亦有類似[胡椒噴霧](../Page/胡椒噴霧.md "wikilink")，按押式噴灑的催淚噴霧或噴劑，以及液化的[催淚水劑](../Page/催淚水劑.md "wikilink")，兩者的威力均不及催淚彈。當中後者由於揮發性較強，因此在戶外使用的威力有限，較適用於室內防暴工作，不過這反而是一般催淚彈沒有的優勢，因為催淚彈若在室內使用，催淚氣體將無法散去，導致示威者（甚至其他市民）窒息死亡。

目前常用的催淚劑主要有[CS催淚性毒氣](../Page/CS催淚性毒氣.md "wikilink")、[CR催淚性毒氣及](../Page/CR催淚性毒氣.md "wikilink")[辣椒素等](../Page/辣椒素.md "wikilink")，而[DM](../Page/亚当氏毒气.md "wikilink")、[CN等傳統毒劑因副作用過大等缺陷已逐步被淘汰](../Page/2-氯苯乙酮.md "wikilink")。刺激性毒剂一般只称作[刺激剂](../Page/刺激剂.md "wikilink")，其主要作用是刺激眼、[鼻](../Page/鼻.md "wikilink")（类似切洋葱）、[喉及](../Page/喉.md "wikilink")[皮肤感觉](../Page/皮肤.md "wikilink")[神经末梢](../Page/神经末梢.md "wikilink")，能夠使人迅速出現流泪、流涕、眼痛、喷嚏、咳嗽、恶心、呕吐、胸痛、头痛以及皮肤灼痛等症状，失去正常行为能力從而妨礙其行動\[1\]。

## 歷史

催淚瓦斯於[第一次世界大戰前後開始被廣泛應用於實戰](../Page/第一次世界大戰.md "wikilink")。1912年，法國警方已經將[溴乙酸乙酯](../Page/溴乙酸乙酯.md "wikilink")（）作為催淚劑用在勤務中\[2\]。一戰爆發後，在西部戰線[伊珀爾](../Page/伊珀爾.md "wikilink")（）防守的法軍將裝有溴乙酸乙酯的手榴彈用來對抗德軍。德軍隨後對催淚劑進行了積極研究並將其作為制式武器在戰場上發揚光大\[3\]。簽署於1925年的[日內瓦協議](../Page/日內瓦協議.md "wikilink")（）已明令禁止將催淚瓦斯用於戰爭的行為。

目前催淚瓦斯主要被各國军警用於[反恐及控制](../Page/反恐.md "wikilink")[騷亂](../Page/騷亂.md "wikilink")。

## 風險

與所有非致命性或低致命性武器相同，使用催淚彈有導致永久性傷害或死亡的風險。這包括被催淚彈彈體擊中，其中包括嚴重的瘀傷、[失明](../Page/失明.md "wikilink")、顱骨骨折等。來自伊朗的報導顯示，使用催淚瓦斯彈的案例也有嚴重的血管損傷，伴有神經損傷（44%）和較高截肢率（17%），以及年輕人頭部受傷的實例。

而毒劑本身通常限於輕微的皮膚炎症，例如裸露的皮膚接觸到CS可能會引起化學灼傷或誘發過敏性接觸性皮炎。延遲並發症也是可能的：如果患有呼吸系統疾病，如哮喘，尤其危險\[4\]，有可能需要就醫和可有時需要住院治療，甚至通氣支持。當無防護人員在近距離被催淚彈命中、直接暴露於高濃度毒劑或長時間暴露於低濃度毒劑中時，可能將因角膜受損而永久失去視覺能力\[5\]。

## 傷亡

催淚彈屬於非致命性武器，然而亦發生過使用催淚彈而致命的罕有個案，[印度據傳曾發生催淚彈彈殼擊中示威者頭部的致死事件](../Page/印度.md "wikilink")\[6\]。[韓國民主運動史上甚至有兩起警用催淚彈致死案](../Page/韓國民主運動.md "wikilink")（1960年及1987年[李韓烈案](../Page/李韓烈.md "wikilink")）引發大規模示威，演變為民主化轉捩點\[7\]。

## 相關條目

  - [防暴](../Page/防暴.md "wikilink")
  - [非致命性武器](../Page/非致命性武器.md "wikilink")

## 參考文獻

## 外部連結

  - [專家：催淚煙不會造成嚴重傷亡](http://www.msn.com/zh-hk/video/watch/%E5%B0%88%E5%AE%B6%E5%82%AC%E6%B7%9A%E7%85%99%E4%B8%8D%E6%9C%83%E9%80%A0%E6%88%90%E5%9A%B4%E9%87%8D%E5%82%B7%E4%BA%A1%E5%8F%AA%E6%9C%83%E4%BB%A4%E4%BA%BA%E4%B8%8D%E9%81%A9/vi-BBayD2t)
  - [專家：催淚彈傷皮膚
    誤用致命](http://www.mingpaocanada.com/Tor/htm/News/20141002/HK-gam1_r.htm)
  - [致命催淚彈 戰場禁用
    驅散平民「合法」](http://hk.apple.nextmedia.com/realtime/international/20140817/52804071)
  - [嚴重恐窒息喪命　香港藥劑師工會要求停用](http://www.ettoday.net/news/20140930/407622.htm)

[Category:化学武器](../Category/化学武器.md "wikilink")
[Category:非致命武器](../Category/非致命武器.md "wikilink")

1.  [催淚彈：非致命武器簡介 Tear Gas: A brief description as a non-lethal
    weapon](http://linepost.hk/view.php?iid=3&id=632)
2.  [Public health response to biological and chemical
    weapons](http://www.who.int/entity/csr/delibepidemics/chapter3.pdf),
    Chapter 3, Biological and Chemical agents, WHO Guidance\]
3.
4.  [不滿被聲稱僅短暫刺激
    名醫指催淚彈損肺功能](http://news.singtao.ca/toronto/2014-10-06/hongkong1412575834d5266421.html)，星島日報，2014-10-16
5.  [專家﹕催淚彈傷皮膚
    誤用致命](http://www.e123.hk/ElderlyPro/details/352663/75)，長青網，2014年10月2日
6.  [印控克什米尔抗议男孩被杀事件](http://m.voachinese.com/a/kashmir-20100206-83709712/461054.html)《美國之音》2010年2月6日
7.