[StaatsbibliothekBerlin.jpg](https://zh.wikipedia.org/wiki/File:StaatsbibliothekBerlin.jpg "fig:StaatsbibliothekBerlin.jpg")
[Stamps_of_Germany_(Berlin)_1978,_MiNr_577.jpg](https://zh.wikipedia.org/wiki/File:Stamps_of_Germany_\(Berlin\)_1978,_MiNr_577.jpg "fig:Stamps_of_Germany_(Berlin)_1978,_MiNr_577.jpg")
**柏林国立图书馆**（），位於[德國](../Page/德國.md "wikilink")[柏林的](../Page/柏林.md "wikilink")[圖書館](../Page/圖書館.md "wikilink")，隶属于。该图书馆依照联邦州法律的规定座落在首都柏林，以大约一千万的藏书量成为[德国最大的综合性图书馆](../Page/德国.md "wikilink")。

## 图书馆历史

1661年柏林国立图书馆的前身「[施普雷河畔的科隆選帝侯圖書館](../Page/施普雷河.md "wikilink")」（*Churfürstliche
Bibliothek zu Cölln an der Spree*）在柏林成立。1701年更名為「王家圖書館」（*Königliche
Bibliothek*）。[第一次世界大战之后图书馆由普魯士文化遺產基金會接管](../Page/第一次世界大战.md "wikilink")。1954年开始[东德确立此图书馆为东德的国家图书馆](../Page/东德.md "wikilink")。直到两德统一后的1992年1月1日，东西两德的国家图书馆才正式合并为现在规模的德国国立图书馆。

## 图书馆任务

  - 作为国家图书馆的柏林国立图书馆不仅担负着收藏本国所有出版物的重任，部分德语国家的出版物在这里也能得到很好的宝藏。
  - 1871年到1912年德国的印刷收藏品。
  - 国际标准书号管理局座落也在柏林国立图书馆，担负着世界性范围内[国际标准书号](../Page/国际标准书号.md "wikilink")（ISBN）的发放与管理。
  - 特种书收藏区：收集了包括东南亚、斯洛文尼亚、法律、政治的善本孤本。

## 图书馆结构

如今的柏林国立图书馆由两部分构成：

  - 坐落于菩提树下大街的1大楼主要收集了到1955年为止的历史书籍
  - 于1978年建成的波茨坦大街的2号大楼主要担负着借阅与大型信息图书馆的功用
  - 位于Westhafen的图书馆主要收藏期刊类读物

## 外部链接

  - [柏林国立图书馆](http://staatsbibliothek-berlin.de/)

[Category:德國圖書館](../Category/德國圖書館.md "wikilink")
[Category:国家图书馆](../Category/国家图书馆.md "wikilink")
[Category:柏林建築物](../Category/柏林建築物.md "wikilink")
[Category:柏林组织](../Category/柏林组织.md "wikilink")