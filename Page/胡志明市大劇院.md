[Municipal_Theatre_Saigon_1915.jpg](https://zh.wikipedia.org/wiki/File:Municipal_Theatre_Saigon_1915.jpg "fig:Municipal_Theatre_Saigon_1915.jpg")

**胡志明市大劇院**（）是一座位於[胡志明市市中心](../Page/胡志明市.md "wikilink")[第一郡的](../Page/胡志明市第一郡.md "wikilink")[劇院](../Page/劇院.md "wikilink")。這座劇院由[法國人修建](../Page/法國.md "wikilink")，於1898年動工，1900年落成。胡志明市大劇院的建築受[法蘭西第三共和國華麗的建築風格影響](../Page/法蘭西第三共和國.md "wikilink")。[越南戰爭時期](../Page/越南戰爭.md "wikilink")，本劇場是[越南共和國的下議院會場](../Page/越南共和國.md "wikilink")。1975年，[北越共產軍隊](../Page/北越.md "wikilink")[越南人民軍佔領](../Page/越南人民軍.md "wikilink")[西貢](../Page/胡志明市.md "wikilink")（胡志明市舊名），越南南北統一，這座建築恢復了文藝設施的功能。

[Category:1898年建筑](../Category/1898年建筑.md "wikilink")
[Category:胡志明市建築物](../Category/胡志明市建築物.md "wikilink")
[Category:越南剧院](../Category/越南剧院.md "wikilink")