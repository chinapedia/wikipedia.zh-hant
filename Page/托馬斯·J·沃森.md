**托馬斯·J·沃森**（，）全名**大托馬斯·約翰·沃森**（），是[美國](../Page/美國.md "wikilink")[商人](../Page/商人.md "wikilink")，在1914－1956年間出任[國際商用機器公司](../Page/國際商用機器公司.md "wikilink")（IBM）第1任[行政總裁](../Page/行政總裁.md "wikilink")，帶領[IBM在](../Page/IBM.md "wikilink")1920－1950年代發展成[國際知名的](../Page/國際.md "wikilink")[商業機構](../Page/商業.md "wikilink")。他實行有效的[管理風格](../Page/管理.md "wikilink")，使IBM賺大[錢](../Page/錢.md "wikilink")。

## 生平

托馬斯·J·沃森的[父親叫喬治](../Page/父親.md "wikilink")·馬歇爾·沃森<small>（George Marshall
Watson）</small>，[母親叫瑪麗](../Page/母親.md "wikilink")·凱勒·沃森<small>（Mary
Keller Watson）</small>。托馬斯·J·沃森在1913年4月17日與珍妮特·M·基特里奇<small>Jeanette M.
Kittredge</small>[結婚](../Page/結婚.md "wikilink")。

## 参考文献

## 外部链接

## 参见

  - [IBM](../Page/IBM.md "wikilink")

{{-}}

[category:美國企業家](../Page/category:美國企業家.md "wikilink")

[Category:美國童軍人物](../Category/美國童軍人物.md "wikilink")
[Category:美国循道宗教徒](../Category/美国循道宗教徒.md "wikilink")
[Category:哥伦比亚大学校友](../Category/哥伦比亚大学校友.md "wikilink")
[Category:IBM员工](../Category/IBM员工.md "wikilink")
[Category:纽约州民主党人](../Category/纽约州民主党人.md "wikilink")
[Category:水牛城人](../Category/水牛城人.md "wikilink")
[Category:曼哈顿人](../Category/曼哈顿人.md "wikilink")