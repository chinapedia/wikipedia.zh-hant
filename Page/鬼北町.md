**鬼北町**（）是[愛媛縣](../Page/愛媛縣.md "wikilink")[南予地方的](../Page/南予地方.md "wikilink")[町](../Page/町.md "wikilink")，轄區位於[四萬十川支流](../Page/四萬十川.md "wikilink")[廣見川的上游](../Page/廣見川.md "wikilink")，以[鬼北盆地及周邊山區為主](../Page/鬼北盆地.md "wikilink")。

「鬼北」之地名是源自由於位於[宇和島市的鬼之城山的北方](../Page/宇和島市.md "wikilink")。

## 歷史

### 年表

  - 1889年12月15日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬[北宇和郡](../Page/北宇和郡.md "wikilink")：旭村、好藤村、愛治村、泉村、三島村、[日吉村](../Page/日吉村_\(愛媛縣北宇和郡\).md "wikilink")。
  - 1941年11月10日：旭村改名並改制為[近永町](../Page/近永町.md "wikilink")。
  - 1955年3月31日：好藤村、愛治村、近永町、泉村、三島村[合併為](../Page/市町村合併.md "wikilink")[廣見町](../Page/廣見町_\(愛媛縣\).md "wikilink")。
  - 2005年1月1日：廣見町和日吉村合併為**鬼北町**。\[1\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>旭村</p></td>
<td><p>1941年11月10日<br />
改名並改制為近永町</p></td>
<td><p>1955年3月31日<br />
合併為廣見町</p></td>
<td><p>2005年1月1日<br />
合併為鬼北町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>好藤村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>愛治村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>泉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>三島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>日吉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [四國旅客鐵道](../Page/四國旅客鐵道.md "wikilink")
      - [予土線](../Page/予土線.md "wikilink")：[深田車站](../Page/深田車站.md "wikilink")
        - [近永車站](../Page/近永車站.md "wikilink") -
        [出目車站](../Page/出目車站.md "wikilink")

## 觀光資源

  - 成川溪谷
  - 節安故鄉之森
  - 轟之甌穴群

## 教育

  - 高等學校

<!-- end list -->

  - [愛媛縣立北宇和高等學校](../Page/愛媛縣立北宇和高等學校.md "wikilink")
  - [愛媛縣立北宇和高等學校日吉分校](../Page/愛媛縣立北宇和高等學校日吉分校.md "wikilink")

## 本地出身之名人

  - [奧島孝康](../Page/奧島孝康.md "wikilink")：前[早稻田大學校長](../Page/早稻田大學.md "wikilink")
  - [芝正](../Page/芝正.md "wikilink")：前[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [高田康太郎](../Page/高田康太郎.md "wikilink")
    ：[漫畫家](../Page/漫畫家.md "wikilink")
  - [兵頭精](../Page/兵頭精.md "wikilink")：日本第一位女性[飛行員](../Page/飛行員.md "wikilink")

## 參考資料

## 外部連結

  - [鬼北町、松野町合併協議會](https://web.archive.org/web/20110812011218/http://www.ai-kihoku.jp/gappei/)

  - [鬼北町商工會](http://www.kihoku.or.jp/)

<!-- end list -->

1.