**文素利**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/演員.md "wikilink")。2006年12月與導演張俊煥結婚。

## 演出作品

### 電視劇

  - 2007年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[太王四神記](../Page/太王四神記.md "wikilink")》飾演
    嘉真/徐琦荷
  - 2008年：MBC《[我人生的黃金期](../Page/我人生的黃金期.md "wikilink")》飾演 李黃
  - 2013年：MBC《[Drama
    Festival](../Page/Drama_Festival.md "wikilink")－[天空嶺殺人事件](../Page/Drama_Festival#第一季.md "wikilink")》飾演
    靜芬
  - 2016年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[藍色海洋的傳說](../Page/藍色海洋的傳說.md "wikilink")》飾演
    安珍珠/四月
  - 2018年：[JTBC](../Page/JTBC.md "wikilink")《[Life](../Page/Life_\(電視劇\).md "wikilink")》飾演
    吳世華

### 電影

  - 2000年：《[薄荷糖](../Page/薄荷糖_\(電影\).md "wikilink")》
  - 2002年：《[綠洲](../Page/綠洲_\(電影\).md "wikilink")》飾演 韓恭洙
  - 2003年：《[家有豔妻](../Page/家有豔妻.md "wikilink")》飾演 恩鎬晶
  - 2004年：《[孝子洞理髮師](../Page/孝子洞理髮師.md "wikilink")》飾演 金旻子
  - 2006年：《[女教授的神秘魅力](../Page/女教授的神秘魅力.md "wikilink")》飾演 趙恩淑
  - 2006年：《[家族的誕生](../Page/家族的誕生_\(電影\).md "wikilink")》飾演 美拉
  - 2007年：《[我們人生中最棒的瞬間](../Page/我們人生中最棒的瞬間.md "wikilink")》
  - 2008年：《[蘋果](../Page/蘋果_\(南韓電影\).md "wikilink")》飾演 美淑
  - 2009年：《[Like You Know It
    All](../Page/Like_You_Know_It_All.md "wikilink")》
  - 2009年：《[Fly，Penguin](../Page/Fly，Penguin.md "wikilink")》
  - 2010年：《[笑笑笑](../Page/笑笑笑.md "wikilink")》飾演 王聖玉
  - 2010年：《[下女](../Page/下女_\(2010年電影\).md "wikilink")》飾演 醫生
  - 2012年：《[在別的國度](../Page/在別的國度.md "wikilink")》飾演 琴熙
  - 2013年：《[憤怒的倫理學](../Page/憤怒的倫理學.md "wikilink")》
  - 2013年：《[間諜](../Page/間諜_\(2013年電影\).md "wikilink")》飾演 安英熙
  - 2014年：《[官能的法則](../Page/官能的法則.md "wikilink")》飾演 趙美妍
  - 2014年：《[萬神](../Page/萬神.md "wikilink")》 飾演 金錦花
  - 2014年：《[自由之丘](../Page/自由之丘_\(電影\).md "wikilink")》飾演 英善
  - 2015年：《[胶片时代爱情](../Page/胶片时代爱情.md "wikilink")》
  - 2016年：《[小姐](../Page/小姐_\(電影\).md "wikilink")》飾演 姨母
  - 2017年：《[特別市民](../Page/特別市民.md "wikilink")》飾演 鄭彩雅
  - 2018年：《[小森林](../Page/小森林_\(電影\).md "wikilink")》飾演 惠媛母親
  - 2018年：《[好日子](../Page/好日子_\(電影\).md "wikilink")》
  - 2018年：《》

### 綜藝節目

  - 2014年2月2日：SBS《Running Man》Ep183
  - 2014年7月8日－2014年11月18日：SBS《Magic Eye》

## 外部連結

  - [NAVER](http://people.search.naver.com/search.naver?where=nexearch&query=%EB%AC%B8%EC%86%8C%EB%A6%AC&sm=tab_etc&ie=utf8&key=PeopleService&os=94309)

[Category:韓國電影演員](../Category/韓國電影演員.md "wikilink")
[Category:韓國舞台演員](../Category/韓國舞台演員.md "wikilink")
[Category:大鐘獎獲獎者](../Category/大鐘獎獲獎者.md "wikilink")
[Category:成均館大學校友](../Category/成均館大學校友.md "wikilink")
[Category:釜山廣域市出身人物](../Category/釜山廣域市出身人物.md "wikilink")
[Category:文姓](../Category/文姓.md "wikilink")
[Category:亞洲電影大獎最佳女配角得主](../Category/亞洲電影大獎最佳女配角得主.md "wikilink")