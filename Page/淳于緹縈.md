**淳-{于}-缇萦**，[西汉](../Page/西汉.md "wikilink")[臨淄人](../Page/臨淄.md "wikilink")。前167年她上书[汉文帝](../Page/汉文帝.md "wikilink")，为其即將受刑的父親[倉公求情](../Page/倉公.md "wikilink")，文章情辞恳切，打动了汉文帝，使其废除了残忍的肉刑。缇萦因此事而闻名，其[孝義事蹟](../Page/孝.md "wikilink")“缇萦救父”也成为一个家喻戶曉的故事。

其上书全文如下：“妾父为吏，齐中皆称其廉平，今坐法当刑。妾伤夫死者不可复生，刑者不可复属，虽复欲改过自新，其道无由也。妾愿没入为官婢，赎父刑罪，使得自新。”此案，也成為漢文帝日後廢除肉刑的開始。

[班固有](../Page/班固.md "wikilink")[五言古诗](../Page/五言古诗.md "wikilink")《詠史》赞曰：“百男何愦愦，不如一缇萦。”

[Category:西汉女性人物](../Category/西汉女性人物.md "wikilink")
[Category:淳于姓](../Category/淳于姓.md "wikilink")
[Category:临淄人](../Category/临淄人.md "wikilink")