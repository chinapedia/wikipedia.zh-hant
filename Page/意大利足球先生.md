**意大利足球先生**是[意大利球員協會每年一度舉行的選舉](../Page/意大利球員協會.md "wikilink")，以表揚每年在[意大利甲組聯賽表現最佳的意大利職業足球員](../Page/意甲.md "wikilink")，被譽為意大利球壇的[奧斯卡獎項](../Page/奧斯卡.md "wikilink")。

## 歷屆得主

| 年度    | 球員名稱                                                                        | 效力球隊                                                                      |
| ----- | --------------------------------------------------------------------------- | ------------------------------------------------------------------------- |
| 1997年 | [罗伯托·曼奇尼](../Page/罗伯托·曼奇尼.md "wikilink")                                    | [森多利亞](../Page/森多利亞.md "wikilink") / [拉素](../Page/拉素足球會.md "wikilink")    |
| 1998年 | [-{zh-hans:皮耶罗;zh-hk:迪比亞路;zh-tw:皮耶羅;}-](../Page/亚历桑德罗·德尔·皮耶罗.md "wikilink") | [祖雲達斯](../Page/祖雲達斯.md "wikilink")                                        |
| 1999年 | [韋利](../Page/克里斯蒂安·维耶里.md "wikilink")                                       | [拉素](../Page/拉素足球會.md "wikilink")                                         |
| 2000年 | [托迪](../Page/弗朗切斯科·托蒂.md "wikilink")                                        | [羅馬](../Page/羅馬足球會.md "wikilink")                                         |
| 2001年 | [托迪](../Page/弗朗切斯科·托蒂.md "wikilink")                                        | [羅馬](../Page/羅馬足球會.md "wikilink")                                         |
| 2002年 | [韋利](../Page/克里斯蒂安·维耶里.md "wikilink")                                       | [國際米蘭](../Page/國際米蘭.md "wikilink")                                        |
| 2003年 | [托迪](../Page/弗朗切斯科·托蒂.md "wikilink")                                        | [羅馬](../Page/羅馬足球會.md "wikilink")                                         |
| 2004年 | [托迪](../Page/弗朗切斯科·托蒂.md "wikilink")                                        | [羅馬](../Page/羅馬足球會.md "wikilink")                                         |
| 2005年 | [-{zh-hans:吉拉迪诺;zh-hk:基拿甸奴;zh-tw:吉拉迪諾;}-](../Page/阿尔贝托·吉拉迪诺.md "wikilink")  | [帕爾瑪](../Page/帕爾馬足球會.md "wikilink") / [AC米蘭](../Page/AC米蘭.md "wikilink")  |
| 2006年 | [-{zh-hans:卡纳瓦罗;zh-hk:簡拿華路;zh-tw:卡納瓦羅;}-](../Page/法比奥·卡纳瓦罗.md "wikilink")   | [尤文图斯](../Page/尤文图斯.md "wikilink") / [皇家馬德里](../Page/皇家馬德里.md "wikilink") |
| 2007年 | [托迪](../Page/弗朗切斯科·托蒂.md "wikilink")                                        | [羅馬](../Page/羅馬足球會.md "wikilink")                                         |
| 2008年 | [-{zh-hans:皮耶罗;zh-hk:迪比亞路;zh-tw:皮耶羅;}-](../Page/亚历桑德罗·德尔·皮耶罗.md "wikilink") | [祖雲達斯](../Page/祖雲達斯.md "wikilink")                                        |
| 2009年 | [-{zh-hans:德罗西;zh-hk:迪羅斯;zh-tw:德羅西;}-](../Page/达尼埃莱·德罗西.md "wikilink")      | [羅馬](../Page/羅馬足球會.md "wikilink")                                         |
| 2010年 | [-{zh-hans:迪纳塔莱;zh-hk:迪拿達利;zh-tw:迪納塔萊;}-](../Page/安東尼奧·迪拿達利.md "wikilink")  | [乌迪内斯](../Page/乌迪内斯足球俱乐部.md "wikilink")                                   |
| 2012年 | [-{zh-hans:皮尔洛;zh-hk:派路;zh-tw:皮爾洛;}-](../Page/安德列·皮尔洛.md "wikilink")        | [尤文图斯](../Page/尤文图斯足球俱乐部.md "wikilink")                                   |

[es:Futbolista del año en
Italia](../Page/es:Futbolista_del_año_en_Italia.md "wikilink")

[Category:意大利足球奖项](../Category/意大利足球奖项.md "wikilink")
[Category:各国足球先生](../Category/各国足球先生.md "wikilink")