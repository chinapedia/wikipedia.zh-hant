[缩略图](https://zh.wikipedia.org/wiki/File:85th_Cadet_Class_Commissioning_\(4293790372\).jpg "fig:缩略图")
[MuseeMarine-sabreOfficer-p1000451.jpg](https://zh.wikipedia.org/wiki/File:MuseeMarine-sabreOfficer-p1000451.jpg "fig:MuseeMarine-sabreOfficer-p1000451.jpg")[法國海軍軍官用的](../Page/法國.md "wikilink")[軍刀](../Page/軍刀.md "wikilink")。\]\]
[Sabre_mg_0644.jpg](https://zh.wikipedia.org/wiki/File:Sabre_mg_0644.jpg "fig:Sabre_mg_0644.jpg")時的[軍刀的柄](../Page/軍刀.md "wikilink")。\]\]
**佩劍**（或
）又稱**軍刀**與**馬刀**，大部分有著彎曲的刀身與單面刀鋒（因真實軍隊使用的軍刀未必都是單刃的刀，所以後來也譯佩劍）。原是[匈牙利人所使用的武器](../Page/匈牙利.md "wikilink")，使用的歷史最早可追溯到西元[九世紀前後](../Page/九世紀.md "wikilink")。多為騎兵所使用，為了方便在馬上單手使用，造得輕巧，刀身也盡量造得較長。刀身共有直刀身、微彎刀身、大彎刀身三種。根據ＦＩＥ劍種說明，現代的運動項目用的佩劍是由[十九世紀末](../Page/十九世紀.md "wikilink")[海軍用劍發展而成的](../Page/海軍.md "wikilink")。

## 来源

17世纪，被称为Sabre的佩剑传入西欧。它们来源于东欧Szabla型佩剑，最早是中世纪时的单刃刀。西欧开始配备这种刀始于匈牙利[骠骑兵](../Page/驃騎兵.md "wikilink")。\[1\]这些骑兵主要作为轻骑兵起到骚扰敌方散兵、蹂躏炮兵阵地和追逐逃兵的作用。17世纪末和18世纪，许多匈牙利骠骑兵逃亡到中、西欧国家并成为当地骠骑兵的中坚。\[2\]匈牙利语的“佩剑”----“Szablya”可以追溯至钦察突厥语的Selebe以及匈牙利语的Szab，意为“切”。

## 運動項目

## 參見

  - [史懷哲軍刀](../Page/史懷哲軍刀.md "wikilink")

## 参考来源

[Category:刀](../Category/刀.md "wikilink")
[Category:擊劍](../Category/擊劍.md "wikilink")
[Category:刃物](../Category/刃物.md "wikilink")
[Category:劍](../Category/劍.md "wikilink")

1.
2.  [Bavaria](../Page/Bavaria.md "wikilink") raised its first hussar
    regiment in 1688 and a second one in about 1700.
    [Prussia](../Page/Prussia.md "wikilink") followed suit in 1721 when
    [Frederick the Great](../Page/Frederick_the_Great.md "wikilink")
    used hussar units extensively during the [War of the Austrian
    Succession](../Page/War_of_the_Austrian_Succession.md "wikilink").
    France established a number of hussar regiments from 1692 onward,
    recruiting originally from Hungary and Germany, then subsequently
    from German-speaking frontier regions within France itself. The
    first hussar regiment in France was founded by a Hungarian
    lieutenant named [Ladislas Ignace de
    Bercheny](../Page/Ladislas_Ignace_de_Bercheny.md "wikilink").
    [Hungarian-history.hu](http://www.hungarian-history.hu/lib/thou/thou12.htm)