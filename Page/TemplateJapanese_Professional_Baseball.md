<table>
<thead>
<tr class="header">
<th><p><a href="日本棒球.md" title="wikilink">日本棒球</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="日本野球機構.md" title="wikilink">日本野球機構</a>（日本職業棒球組織）</p></td>
</tr>
<tr class="even">
<td><p><a href="中央聯盟.md" title="wikilink">中央聯盟</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="讀賣巨人.md" title="wikilink">讀賣巨人</a> | <a href="東京養樂多燕子.md" title="wikilink">東京養樂多燕子</a> | <a href="橫濱DeNA海灣之星.md" title="wikilink">橫濱DeNA海灣之星</a> | <a href="中日龍.md" title="wikilink">中日龍</a> | <a href="阪神虎.md" title="wikilink">阪神虎</a> | <a href="廣島東洋鯉魚.md" title="wikilink">廣島東洋鯉魚</a></p></td>
</tr>
<tr class="even">
<td><p>東部聯盟</p></td>
</tr>
<tr class="odd">
<td><p><a href="北海道日本火腿鬥士.md" title="wikilink">北海道日本火腿鬥士</a> | <a href="東北樂天金鷹.md" title="wikilink">東北樂天金鷲</a> | <a href="埼玉西武獅.md" title="wikilink">埼玉西武獅</a> | <a href="千葉羅德海洋.md" title="wikilink">千葉羅德海洋</a> | <a href="讀賣巨人.md" title="wikilink">讀賣巨人</a> | <a href="東京養樂多燕子.md" title="wikilink">東京養樂多燕子</a> | <a href="橫濱灣星.md" title="wikilink">橫濱灣星</a></p></td>
</tr>
<tr class="even">
<td><p><a href="日本大賽.md" title="wikilink">日本大賽</a> | <a href="日本職棒全明星賽.md" title="wikilink">日本職棒全明星賽</a> | <a href=":ja:クライマックスシリーズ.md" title="wikilink">日本職棒季後賽</a> | <a href=":ja:セ・パ交流戦.md" title="wikilink">交流戰</a><br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href=":ja:独立リーグ.md" title="wikilink">獨立聯盟</a> / 女子職業聯盟 / OB聯盟</p></td>
</tr>
<tr class="even">
<td><p><a href=":ja:四国・九州アイランドリーグ.md" title="wikilink">四國・九州島聯盟</a> | <a href="棒球挑戰聯盟.md" title="wikilink">棒球挑戰聯盟</a> | <a href="關西獨立聯盟.md" title="wikilink">關西獨立聯盟</a> | <a href=":ja:ジャパン・フューチャーベースボールリーグ.md" title="wikilink">日本未來棒球聯盟</a> | <a href=":ja:日本女子プロ野球機構.md" title="wikilink">日本女子職業棒球機構</a> | <a href=":ja:プロ野球マスターズリーグ.md" title="wikilink">職業棒球大師級聯盟</a></p></td>
</tr>
<tr class="odd">
<td><p>已經停止運作組織</p></td>
</tr>
<tr class="even">
<td><p><a href=":ja:日本野球連盟_(プロ野球).md" title="wikilink">日本野球連盟</a>（1936-1949） | <a href=":ja:国民野球連盟.md" title="wikilink">國民棒球聯盟</a>（1947-1948） | <a href=":ja:日本女子野球連盟.md" title="wikilink">日本女子棒球聯盟</a>（1950-1951） | <a href=":JA:グローバルリーグ.md" title="wikilink">全球聯盟</a>（1969）</p></td>
</tr>
<tr class="odd">
<td><p>其他相關連組織</p></td>
</tr>
<tr class="even">
<td><p><a href=":ja:日本プロ野球選手会.md" title="wikilink">日本職業棒球選手工會</a> | <a href=":ja:日本プロ野球名球会.md" title="wikilink">日本職業棒球名球會</a> | <a href=":ja:日本プロ野球OBクラブ.md" title="wikilink">日本職業棒球OB俱樂部</a> | <a href=":ja:独立リーグ連絡協議会.md" title="wikilink">獨立聯盟連絡協會</a></p></td>
</tr>
</tbody>
</table>

<noinclude> </noinclude>

[](../Category/日本棒球模板.md "wikilink")