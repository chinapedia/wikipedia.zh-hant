**归中反应**（**Comproportionation**）是一类[氧化还原反应](../Page/氧化还原反应.md "wikilink")，其中由两个或多个含有某元素而不同[氧化数的反应物](../Page/氧化数.md "wikilink")，得到氧化数相同的单一产物，其逆反应為[歧化反应](../Page/歧化反应.md "wikilink")。

例如一反應的反應物中，A元素的氧化數分別為0和+2，而生成物中的A元素的氧化數為+1，此反應即為归中反应。

## 弗洛斯特图

二反應物是否會產生归中反应，可以藉由[弗洛斯特图來確認](../Page/弗洛斯特图.md "wikilink")。若在弗洛斯特图中，二反應物的ΔG/F連線，中間有其他物質的ΔG/F低於上述的連線，則這二個反應物會產生归中反应。

## 舉例

  - 在[鉛酸蓄電池中的放電化學反應](../Page/鉛酸蓄電池.md "wikilink")：（其中鉛的氧化數為0、[二氧化鉛的則為](../Page/二氧化鉛.md "wikilink")+4，反應生成後[硫酸鉛中鉛的氧化價為](../Page/硫酸鉛.md "wikilink")+2）

<!-- end list -->

  -

      -
        Pb(s) + PbO<sub>2</sub>(s) + 2 H<sub>2</sub>SO<sub>4</sub>(aq) →
        2 PbSO<sub>4</sub>(s) + 2 H<sub>2</sub>O(l)

<!-- end list -->

  - [高锰酸钾中](../Page/高锰酸钾.md "wikilink")[锰的](../Page/锰.md "wikilink")[氧化数为](../Page/氧化数.md "wikilink")+7。它在和氧化数为+2的锰化合物反应时，产物是[二氧化锰](../Page/二氧化锰.md "wikilink")（MnO<sub>2</sub>，锰的氧化数为+4）、[氢氧化钾和水](../Page/氢氧化钾.md "wikilink")。
  - 15Se + SeCl<sub>4</sub> + 4AlCl<sub>3</sub> → Na\[AlCl\]<sub>4</sub>
    + 3Se<sub>8</sub>\[AlCl<sub>4</sub>\]<sub>2</sub>
  - 火山爆发时会发生反应：2H<sub>2</sub>S(g) + SO<sub>2</sub>(g) → 3S(s) +
    2H<sub>2</sub>O(g)
  - IO<sub>3</sub><sup>-</sup> + 5 I<sup>-</sup> + 6 H <sup>+</sup> → 3
    I<sub>2</sub> + 3 H<sub>2</sub>O
  - 制取氮气：NH<sub>4</sub>Cl + NaNO<sub>2</sub> =△= NaCl + 2H<sub>2</sub>O
    + N<sub>2</sub>↑

## 参见

  - [歧化反应](../Page/歧化反应.md "wikilink")：归中反应的逆反应。

[G](../Category/化学反应.md "wikilink")