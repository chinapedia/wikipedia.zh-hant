[AFR_PC_8405_01.jpg](https://zh.wikipedia.org/wiki/File:AFR_PC_8405_01.jpg "fig:AFR_PC_8405_01.jpg")
[Jhushan_Line_PC_Interior.jpg](https://zh.wikipedia.org/wiki/File:Jhushan_Line_PC_Interior.jpg "fig:Jhushan_Line_PC_Interior.jpg")
**祝山線型客車**是[阿里山森林鐵路的](../Page/阿里山森林鐵路.md "wikilink")[鐵路客車](../Page/鐵路客車.md "wikilink")，現為祝山線、眠月線、神木區間線專用車。在[林務局的紀錄中](../Page/林務局.md "wikilink")，祝山線型客車最多共有二十六輛。

## 歷史

  - 1994年-1996年：祝山線型客車投入服務。

## 塗裝

祝山線型客車另一大特點在於其塗裝，它的塗裝一改當時過去林鐵只有紅白色系的傳統，因此除紅底單白線外，於車窗窗帶改為墨綠色，車門不再牽就車體而改為銀色，整體塗裝源自[瑞士](../Page/瑞士.md "wikilink")[冰河列車RhB](../Page/冰河列車.md "wikilink")（Rhtische
Bahn）之Bernina
Express登山客車，除車頂外，可謂RhB客車翻版，而內裝仿自台鐵[EMU500型電車](../Page/EMU500型.md "wikilink")。

## 特殊構造

車門一改過去鋼體客車側面單門的傳統，比照木造客車改為雙門設計，加快上下車速率，如此設計可能是為了因應[祝山觀](../Page/祝山.md "wikilink")[日出與](../Page/日出.md "wikilink")[眠月觀](../Page/眠月.md "wikilink")[一葉蘭與石猴的大量人潮](../Page/一葉蘭.md "wikilink")，但內裝仍沿襲今日SP客車長條椅設計。車窗改良過去巴士窗型式，以七個大窗搭配上方可滑動鋁窗呈現，亦是台灣首見。

## 車廂資料與規格

| 製造年月           | 輛數 | 車號          | 製造廠        | 車體尺寸（長×寬×高） | 備註                                                 |
| -------------- | -- | ----------- | ---------- | ----------- | -------------------------------------------------- |
| 1994年（民國83年）6月 | 10 | 祝8301-祝8310 | 台灣省農工嘉義機械廠 |             | 祝8306、祝8408[事故報廢](../Page/阿里山林鐵事故一覽.md "wikilink") |
| 1995年（民國84年）6月 | 9  | 祝8401-祝8409 |            |             |                                                    |
| 1996年（民國85年）1月 | 7  | 祝8501-祝8507 |            |             |                                                    |

[Category:台灣鐵路車輛](../Category/台灣鐵路車輛.md "wikilink")
[Category:阿里山森林鐵路](../Category/阿里山森林鐵路.md "wikilink")
[Category:鐵路客車](../Category/鐵路客車.md "wikilink")