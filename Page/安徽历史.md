[安徽地域是](../Page/安徽.md "wikilink")[汉族诞生的摇篮之一](../Page/汉族.md "wikilink")。
安徽于[清朝](../Page/清朝.md "wikilink")[康熙六年](../Page/康熙.md "wikilink")（公元1667年）始建省，省名取[安庆府与](../Page/安庆.md "wikilink")[徽州府名第一字为安徽](../Page/徽州.md "wikilink")。这就是安徽名称的由来。安徽因历史上有古[皖国和境内的](../Page/皖国.md "wikilink")[皖山](../Page/皖山.md "wikilink")、[皖河而简称](../Page/皖河.md "wikilink")“皖”，分屬漢地[徐](../Page/徐州_\(九州\).md "wikilink")、揚二州地域

## 商朝

[商朝](../Page/商朝.md "wikilink")（前16世纪－前11世纪）的开国君主[汤曾经短期定都在安徽北部的](../Page/汤.md "wikilink")[亳州](../Page/亳州.md "wikilink")，当时安徽生活着很多邦國或部落，是後世[汉族分支之一](../Page/汉族.md "wikilink")。

## 春秋战国

春秋（前770年～476年）时，安徽各地分属于[吴国](../Page/吴国.md "wikilink")([天长](../Page/天长.md "wikilink")，[马鞍山等地](../Page/马鞍山.md "wikilink"))，[楚国](../Page/楚国.md "wikilink")([六安等地](../Page/六安.md "wikilink"))，[越国](../Page/越国.md "wikilink")（[宣城等地](../Page/宣城.md "wikilink")）、[萧国](../Page/萧国.md "wikilink")（[萧县等地](../Page/萧县.md "wikilink")）、[桐国](../Page/桐国.md "wikilink")（[安庆等地](../Page/安庆.md "wikilink")），[宋国](../Page/宋国.md "wikilink")(阜阳等地)、[蔡国](../Page/蔡国.md "wikilink")（[凤台](../Page/凤台.md "wikilink")，[淮南等地](../Page/淮南.md "wikilink")）、[徐国](../Page/徐国.md "wikilink")（[泗县](../Page/泗县.md "wikilink")，[宿州等地](../Page/宿州.md "wikilink")）等。

[战国末期](../Page/戰國_\(中國\).md "wikilink")，前278年，[楚国的郢都](../Page/楚国.md "wikilink")（今湖北[荆州](../Page/荆州市.md "wikilink")）被[秦国攻占](../Page/秦国.md "wikilink")，于是迁都[寿春](../Page/寿春.md "wikilink")（今安徽[寿县](../Page/寿县.md "wikilink")）。前224年，楚国大将[项燕被秦国的](../Page/项燕.md "wikilink")[王翦](../Page/王翦.md "wikilink")60万大军击败，前223年，寿春陷落，[楚国灭亡](../Page/楚国.md "wikilink")。

## [秦汉](../Page/秦汉.md "wikilink")

前221年，秦国完成征服六国、统一天下的计划。安徽大部分地区属于[九江郡](../Page/九江郡.md "wikilink")（今安徽[寿县](../Page/寿县.md "wikilink")），北端属于[泗水郡和](../Page/泗水郡.md "wikilink")[砀郡](../Page/砀郡.md "wikilink")。

秦汉之际，[项羽曾以此](../Page/项羽.md "wikilink")[西楚为国号](../Page/西楚.md "wikilink")，号称[西楚霸王为当时中国国家元首但未正式称帝](../Page/西楚霸王.md "wikilink")。
西楚以今天江苏[淮安楚州为中心](../Page/楚州区.md "wikilink")，范围大概是今[苏北与](../Page/苏北.md "wikilink")[皖北](../Page/皖北.md "wikilink");而最大城市[彭城](../Page/彭城.md "wikilink")（今江苏[徐州市](../Page/徐州市.md "wikilink")）以东到海被称为东楚；楚国历史上的中心，[江陵到](../Page/江陵.md "wikilink")[寿春被称为南楚](../Page/寿春.md "wikilink")。[项羽在今安徽](../Page/项羽.md "wikilink")[固镇县的](../Page/固镇.md "wikilink")[垓下之战中败给](../Page/垓下之战.md "wikilink")[刘邦](../Page/刘邦.md "wikilink")，自刎于[乌江](../Page/乌江.md "wikilink")（今安徽[和县](../Page/和县.md "wikilink")）。[刘邦建立](../Page/刘邦.md "wikilink")[汉朝](../Page/汉朝.md "wikilink")。

[汉武帝时](../Page/汉武帝.md "wikilink")[东瓯国举国迁到](../Page/东瓯国.md "wikilink")[卢江郡](../Page/卢江郡.md "wikilink")（安徽西部的[舒城地区](../Page/舒城.md "wikilink")）。

|                                  |        |                                                                                                                     |
| :------------------------------: | :----: | :-----------------------------------------------------------------------------------------------------------------: |
|                郡国                | 郡治（国都） |                                                        县、侯国                                                         |
| [九江郡](../Page/九江郡.md "wikilink") |  寿春县   |                                      寿春、成德、曲阳、当涂、阴陵、钟离、东城、合淝、浚遒、橐皋、阜陵、历阳、建阳、博乡                                      |
| [六安国](../Page/六安国.md "wikilink") |   六县   |                                            六、安风、雩娄、阳泉、蓼、安丰（后三县位于今河南省境内）                                             |
| [庐江郡](../Page/庐江郡.md "wikilink") |   舒县   |                                    舒、临湖、襄安、枞阳、居巢、皖、湖陵、松兹、龙舒、灊、金兰、寻阳（后两县位于今湖北省境内）                                    |
| [丹阳郡](../Page/丹楊郡.md "wikilink") |  宛陵县   |                      宛陵、宣城、芜湖、石城、丹阳、春谷、泾、歙、黟、陵阳、秣陵、溧阳、胡孰、江乘、句容（此五县位于今江苏省境内）、故鄣、于潜（此两县位于今浙江省境内）                      |
|  [沛郡](../Page/沛郡.md "wikilink")  |   相县   |         相、萧、谯、城父、下蔡、平阿、龙亢、义成、向、谷阳、蕲、夏丘、符离、竹、郸、铚、山桑、谷阳、𧈬、建成、敬丘、建平、栗、祁乡、芒、酂（此七县位于今河南省境内）、丰、沛、广戚（此三县位于今江苏省境内）          |
|                梁国                |  睢阳县   |                                          睢阳、虞、蒙、已氏、砀、下邑、杼秋（仅下邑、杼秋位于今安徽省境内）                                          |
|               汝南郡                |  平舆县   | 平舆、宜春、新蔡、期思、弋阳、新息、安阳、成阳、慎阳、安成、阳安、安昌、朗陵、灈阳、上蔡、吴房、西平、召陵、定陵、阳城、女阳、博阳、南顿、西华、长平、㶏强、项、宜禄、女阴、新阳、慎、富波、鲖阳、新郪、𥧲（仅后七县位于今安徽省境内） |

魏晋南北朝[汉朝末年](../Page/汉朝.md "wikilink")，寿春成为[军阀](../Page/军阀.md "wikilink")[袁术的基地](../Page/袁术.md "wikilink")。袁术后来自称皇帝，但不久病死，
寿春归属军阀[曹操](../Page/曹操.md "wikilink")（今安徽[亳州人](../Page/亳州.md "wikilink")，[三国中](../Page/三国.md "wikilink")[魏国的实际开创者](../Page/曹魏.md "wikilink")）。

[五胡十六国期间](../Page/五胡十六国.md "wikilink")，从4世纪开始的几百年间，几支北方游牧民族在中国北方相继建立政权，南方则一直保持着汉人的王朝。安徽北部成为南北方对峙的前线，战火不断。383年在北方的[前秦和南方的](../Page/前秦.md "wikilink")[东晋之间展开的](../Page/东晋.md "wikilink")[淝水之戰就是其中比较重要的一次战役](../Page/淝水之戰.md "wikilink")。

## 隋唐

[隋朝](../Page/隋朝.md "wikilink")
(581年-618年)统一了漢地九州。经过短暂的混战，[唐朝](../Page/唐朝.md "wikilink")
(618年-907年)建立政权，并保持了一个多世纪的和平和统一。[安史之乱中](../Page/安史之乱.md "wikilink")，安徽北部淮河地区遭受战火蹂躏。唐末875年[王仙芝](../Page/王仙芝.md "wikilink")、[黄巢](../Page/黄巢.md "wikilink")[起义](../Page/起义.md "wikilink")，
从[河南经今安徽](../Page/河南.md "wikilink")[和县渡长江入](../Page/和县.md "wikilink")[皖南](../Page/皖南.md "wikilink")，到[福建](../Page/福建.md "wikilink")、[广东](../Page/广东.md "wikilink")，后经安徽攻[长安](../Page/长安.md "wikilink")，天下大乱，藩镇割据，不久唐朝灭亡。

|                                |                                         |                                                                                                                                                                                                                                      |
| :----------------------------: | :-------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|               州                |                   附郭县                   |                                                                                                                  县                                                                                                                   |
| [歙州](../Page/歙州.md "wikilink") |     [歙县](../Page/歙县.md "wikilink")      |                                                  [休宁县](../Page/休宁县.md "wikilink") [婺源县](../Page/婺源县.md "wikilink") [黟县](../Page/黟县.md "wikilink") [北野县](../Page/北野县.md "wikilink")                                                   |
| [宣州](../Page/宣州.md "wikilink") | [宣城县](../Page/宣城县_\(隋朝\).md "wikilink") | [泾县](../Page/泾县.md "wikilink") [南陵县](../Page/南陵县.md "wikilink") [秋浦县](../Page/秋浦县.md "wikilink") [当涂县](../Page/当涂县.md "wikilink") [绥安县](../Page/绥安县.md "wikilink") [溧水县](../Page/溧水县.md "wikilink") [溧阳县](../Page/溧阳县.md "wikilink") |
| [舒州](../Page/舒州.md "wikilink") |    [怀宁县](../Page/怀宁县.md "wikilink")     |                                                 [同安县](../Page/桐城县.md "wikilink") [太湖县](../Page/太湖县.md "wikilink") [宿松县](../Page/宿松县.md "wikilink") [望江县](../Page/望江县.md "wikilink")                                                  |
| [庐州](../Page/庐州.md "wikilink") |    [合肥县](../Page/合肥县.md "wikilink")     |                                                   [舒城县](../Page/舒城县.md "wikilink") [慎县](../Page/慎县.md "wikilink") [庐江县](../Page/庐江县.md "wikilink") [巢县](../Page/巢县.md "wikilink")                                                    |
| [滁州](../Page/滁州.md "wikilink") |    [清流县](../Page/清流县.md "wikilink")     |                                                                                  [全椒县](../Page/全椒县.md "wikilink") [永阳县](../Page/来安县.md "wikilink")                                                                                   |
| [和州](../Page/和州.md "wikilink") |    [历阳县](../Page/历阳县.md "wikilink")     |                                                                                  [含山县](../Page/含山县.md "wikilink") [乌江县](../Page/乌江县.md "wikilink")                                                                                   |
| [寿州](../Page/寿州.md "wikilink") |    [寿春县](../Page/寿春县.md "wikilink")     |                                                                  [安丰县](../Page/安丰县.md "wikilink") [霍邱县](../Page/霍邱县.md "wikilink") [盛唐县](../Page/六安县.md "wikilink")                                                                  |
| [濠州](../Page/濠州.md "wikilink") |    [钟离县](../Page/钟离县.md "wikilink")     |                                                                                  [定远县](../Page/定远县.md "wikilink") [招义县](../Page/招义县.md "wikilink")                                                                                   |
| [颍州](../Page/颍州.md "wikilink") |    [汝阴县](../Page/汝阴县.md "wikilink")     |                                                                  [颍上县](../Page/颍上县.md "wikilink") [沈丘县](../Page/沈丘县.md "wikilink") [下蔡县](../Page/下蔡县.md "wikilink")                                                                  |
| [亳州](../Page/亳州.md "wikilink") |     [谯县](../Page/谯县.md "wikilink")      | [山桑县](../Page/山桑县.md "wikilink") [城父县](../Page/城父县.md "wikilink") [永城县](../Page/永城县.md "wikilink") [鹿邑县](../Page/鹿邑县.md "wikilink") [真源县](../Page/真源县.md "wikilink") [酂县](../Page/酂县.md "wikilink") [临涣县](../Page/临涣县.md "wikilink") |

## 五代、杨吴、南唐

[五代](../Page/五代.md "wikilink")、[杨吴](../Page/杨吴.md "wikilink")、[南唐都是晚唐](../Page/南唐.md "wikilink")[藩镇割据的产物](../Page/藩镇割据.md "wikilink")。其时今安徽省淮河以北大部分地区属五代，今[皖南地区属](../Page/皖南.md "wikilink")[杨吴和](../Page/杨吴.md "wikilink")[南唐](../Page/南唐.md "wikilink")。[景福元年](../Page/景福_\(唐昭宗\).md "wikilink")（892年），[淮西](../Page/淮西.md "wikilink")（今属安徽）人[杨行密在](../Page/杨行密.md "wikilink")[扬州建立](../Page/扬州.md "wikilink")[吴国](../Page/吴国.md "wikilink")，937年徐州人[李昇代杨吴自立](../Page/李昇.md "wikilink")，建立南唐，定都[江宁](../Page/江宁府.md "wikilink")。

[太平興國元年](../Page/太平興國.md "wikilink")（976年），[宋朝大军攻入江宁](../Page/宋朝.md "wikilink")，[淮河以南最后一个阻碍宋朝统一的政权](../Page/淮河.md "wikilink")——南唐灭亡。安徽省全境进入了宋朝的疆界。

## 宋朝

[北宋在今安徽省长江以南部分置](../Page/北宋.md "wikilink")[江南东路](../Page/江南东路.md "wikilink")，首府[江宁府](../Page/江宁府.md "wikilink")(今[南京地区](../Page/南京.md "wikilink"))；长江以北大部分属[淮南西路和](../Page/淮南西路.md "wikilink")[淮南东路](../Page/淮南东路.md "wikilink")。今[萧县属于](../Page/萧县.md "wikilink")[京东西路](../Page/京东西路.md "wikilink")[徐州](../Page/徐州.md "wikilink")。

[北宋后期](../Page/北宋.md "wikilink")，北方的[金朝崛起](../Page/金朝.md "wikilink")。[建炎元年](../Page/建炎.md "wikilink")（1127年）以后，安徽北部的淮河再次成为南北对峙的前沿：北方的金和南方的[南宋](../Page/南宋.md "wikilink")。淮南东路、西路淮河以南各州仍由南宋管辖，建制仍然保留，淮河以北各州属金朝[南京路
(金朝)](../Page/南京路_\(金朝\).md "wikilink")。

[紹興十一年](../Page/紹興.md "wikilink")（1161年），金朝海陵王[完颜亮出兵伐宋](../Page/完颜亮.md "wikilink")，在今安徽[马鞍山](../Page/马鞍山市.md "wikilink")[采石大战失败](../Page/采石大战.md "wikilink")。[端平元年](../Page/端平.md "wikilink")（1234年），金亡于[蒙古帝国](../Page/蒙古帝国.md "wikilink")。[德祐二年](../Page/德祐.md "wikilink")（1276年），[蒙古人攻占南宋京城杭州](../Page/蒙古族.md "wikilink")。安徽全境归[元朝](../Page/元朝.md "wikilink")。

|                                  |                                  |                                                                                                                                                                                                     |
| :------------------------------: | :------------------------------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|               州、军                |               附郭县                |                                                                                                  县                                                                                                  |
|  [歙州](../Page/歙州.md "wikilink")  |  [歙县](../Page/歙县.md "wikilink")  |                 [休宁县](../Page/休宁县.md "wikilink") [婺源县](../Page/婺源县.md "wikilink") [祁门县](../Page/祁门县.md "wikilink") [黟县](../Page/黟县.md "wikilink") [绩溪县](../Page/绩溪县.md "wikilink")                  |
|  [宣州](../Page/宣州.md "wikilink")  | [宣城县](../Page/宣城县.md "wikilink") |                 [宁国县](../Page/宁国县.md "wikilink") [泾县](../Page/泾县.md "wikilink") [太平县](../Page/太平县.md "wikilink") [旌德县](../Page/旌德县.md "wikilink") [南陵县](../Page/南陵县.md "wikilink")                  |
|  [池州](../Page/池州.md "wikilink")  | [贵池县](../Page/贵池县.md "wikilink") |                [青阳县](../Page/青阳县.md "wikilink") [铜陵县](../Page/铜陵县.md "wikilink") [石埭县](../Page/石埭县.md "wikilink") [建德县](../Page/建德县.md "wikilink") [东流县](../Page/东流县.md "wikilink")                 |
| [太平州](../Page/太平州.md "wikilink") | [当涂县](../Page/当涂县.md "wikilink") |                                                                  [芜湖县](../Page/芜湖县.md "wikilink") [繁昌县](../Page/繁昌县.md "wikilink")                                                                  |
|  [舒州](../Page/舒州.md "wikilink")  | [怀宁县](../Page/怀宁县.md "wikilink") |                                 [桐城县](../Page/桐城县.md "wikilink") [太湖县](../Page/太湖县.md "wikilink") [宿松县](../Page/宿松县.md "wikilink") [望江县](../Page/望江县.md "wikilink")                                 |
|  [庐州](../Page/庐州.md "wikilink")  | [合肥县](../Page/合肥县.md "wikilink") |                                                                   [舒城县](../Page/舒城县.md "wikilink") [慎县](../Page/慎县.md "wikilink")                                                                   |
|  [滁州](../Page/滁州.md "wikilink")  | [清流县](../Page/清流县.md "wikilink") |                                                                  [全椒县](../Page/全椒县.md "wikilink") [来安县](../Page/来安县.md "wikilink")                                                                  |
|  [和州](../Page/和州.md "wikilink")  | [历阳县](../Page/历阳县.md "wikilink") |                                                                  [含山县](../Page/含山县.md "wikilink") [乌江县](../Page/乌江县.md "wikilink")                                                                  |
|  [寿州](../Page/寿州.md "wikilink")  | [下蔡县](../Page/下蔡县.md "wikilink") |                                 [寿春县](../Page/寿春县.md "wikilink") [安丰县](../Page/安丰县.md "wikilink") [霍邱县](../Page/霍邱县.md "wikilink") [六安县](../Page/六安县.md "wikilink")                                 |
|  [濠州](../Page/濠州.md "wikilink")  | [钟离县](../Page/钟离县.md "wikilink") |                                                                                  [定远县](../Page/定远县.md "wikilink")                                                                                   |
|  [颍州](../Page/颍州.md "wikilink")  | [汝阴县](../Page/汝阴县.md "wikilink") |                                                 [颍上县](../Page/颍上县.md "wikilink") [沈丘县](../Page/沈丘县.md "wikilink") [万寿县](../Page/万寿县.md "wikilink")                                                  |
|  [亳州](../Page/亳州.md "wikilink")  |  [谯县](../Page/谯县.md "wikilink")  | [蒙城县](../Page/蒙城县.md "wikilink") [城父县](../Page/城父县.md "wikilink") [永城县](../Page/永城县.md "wikilink") [鹿邑县](../Page/鹿邑县.md "wikilink") [卫真县](../Page/卫真县.md "wikilink") [酂县](../Page/酂县.md "wikilink") |
|  [宿州](../Page/宿州.md "wikilink")  | [符离县](../Page/符离县.md "wikilink") |                                   [蕲县](../Page/蕲县.md "wikilink") [零璧县](../Page/零璧县.md "wikilink") [临涣县](../Page/临涣县.md "wikilink") [虹县](../Page/虹县.md "wikilink")                                   |
| [广德军](../Page/广德军.md "wikilink") | [广德县](../Page/广德县.md "wikilink") |                                                                                  [建平县](../Page/建平县.md "wikilink")                                                                                   |
| [无为军](../Page/无为军.md "wikilink") | [无为县](../Page/无为县.md "wikilink") |                                                                   [庐江县](../Page/庐江县.md "wikilink") [巢县](../Page/巢县.md "wikilink")                                                                   |

## 元朝

13世纪，蒙古人用了68年時間征服漢地九州。[元朝初期](../Page/元朝.md "wikilink")，安徽属江淮行省；后划江而治理，长江以南与[苏南](../Page/苏南.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[福建同属](../Page/福建.md "wikilink")[江浙行省](../Page/江浙行省.md "wikilink")，长江以北与[苏北](../Page/苏北.md "wikilink")、[河南](../Page/河南.md "wikilink")、[湖北北部同属](../Page/湖北.md "wikilink")[河南江北行省](../Page/河南江北行省.md "wikilink")。

|     |     |                            |                                                                           |
| :-: | :-: | :------------------------: | :-----------------------------------------------------------------------: |
|  路  | 附郭县 |             州              |                                     县                                     |
| 庐州路 | 合肥县 |         和州、无为州、六安州         |           合肥县、梁县、舒城县、庐江县、历阳县、乌江县、含山县、无为县、巢县、六安县、英山县（英山位于今湖北省境内）           |
| 安庆路 | 怀宁县 |             /              |                            怀宁县、桐城县、望江县、太湖县、宿松县                            |
| 安丰路 | 寿春县 |             濠州             |                      寿春县、安丰县、霍丘县、怀远县、下蔡县、蒙城县、定远县、钟离县                      |
| 太平路 | 当涂县 |             /              |                                当涂县、芜湖县、繁昌县                                |
| 宁国路 | 宣城县 |             /              |                          宣城县、泾县、南陵县、太平县、旌德县、宁国县                           |
| 广德路 | 广德县 |             /              |                                  广德县、建平县                                  |
| 池州路 | 贵池县 |             /              |                          贵池县、铜陵县、青阳县、石埭县、东流县、建德县                          |
| 徽州路 | 歙县  |       婺源州（位于今江西省境内）        |                             歙县、绩溪县、休宁县、黟县、祁门县                             |
| 汝宁府 | 汝阳县 | 信阳州、光州、息州、颍州（仅颍州位于今安徽省境内）  | 汝阳县、新蔡县、真阳县、确山县、遂平县、西平县、上蔡县、信阳县、罗山县、定城县、光山县、固始县、沈丘县、太和县、颍上县（仅后三县位于今安徽省境内） |
| 归德府 | 睢阳县 | 徐州、邳州、亳州、宿州（亳州、宿州位于今安徽省境内） |         睢阳县、宁陵县、下邑县、鹿邑县、永城县、下邳县、睢宁县、宿迁县、灵璧县、谯县、城父县（仅后三县位于今安徽省境内）          |
| 淮安路 | 山阳县 |         泗州、海宁州、安东州         |       山阳县、清河县、桃园县、盐城县、沭阳县、朐山县、赣榆县、临淮县、盱眙县、天长县、虹县、五河县（仅后三县位于今安徽省境内）        |
| 扬州路 | 江都县 |  泰州、通州、真州、滁州（仅滁州位于今安徽省境内）  |           江都县、扬子县、海陵县、泰兴县、静海县、如皋县、六合县、清流县、来安县、全椒县（仅后三县位于今安徽省境内）           |

[YUAN(South_China).jpg](https://zh.wikipedia.org/wiki/File:YUAN\(South_China\).jpg "fig:YUAN(South_China).jpg")

## 明朝

[洪武元年](../Page/洪武.md "wikilink")（1368年），明太祖[朱元璋在](../Page/朱元璋.md "wikilink")[南京建立](../Page/南京.md "wikilink")[明朝](../Page/明朝.md "wikilink")，收復漢地九州。由于定都南京，現今的江苏省和安徽省直屬中央[六部管理](../Page/六部.md "wikilink")，称为“[直隶](../Page/直隶.md "wikilink")”。该区域跨长江和淮河南北，首次将经济和文化差异很大的广阔地区，包括今安徽北部朱元璋洪水频繁的家乡[凤阳](../Page/凤阳.md "wikilink")，今安徽南部多山的[徽州](../Page/徽州.md "wikilink")，和今江苏南部以富裕著称的[太湖流域](../Page/太湖流域.md "wikilink")，都划归同一个行政区管辖。

明太祖定凤阳为**中都**，一度考虑迁都于此，后来放弃这个计划，只建成陵墓和一部分城墙。明代在安徽境内共设有7个府，其中4个位于长江以南：[徽州府](../Page/徽州府.md "wikilink")、[宁国府](../Page/宁国府.md "wikilink")、[太平府](../Page/太平府.md "wikilink")、[池州府](../Page/池州府.md "wikilink")；2个位于长江北岸：[安庆府](../Page/安庆府.md "wikilink")、[庐州府](../Page/庐州府.md "wikilink")；而整个淮河流域只有[凤阳府一府](../Page/凤阳府.md "wikilink")。长江南岸的[芜湖縣在明朝发展成新兴的經濟中心](../Page/芜湖縣.md "wikilink")。[崇禎八年](../Page/崇禎.md "wikilink")（1635年）明末[流寇](../Page/流寇.md "wikilink")[李自成部攻陷凤阳](../Page/李自成.md "wikilink")，掘[明朝皇室的祖坟](../Page/明朝皇室.md "wikilink")，焚毁朱元璋曾经出家的“[皇觉寺](../Page/皇觉寺.md "wikilink")”。

|                                          |                                  |                                                                                                                                                            |                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| :--------------------------------------: | :------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                    府                     |               附郭县                |                                                                            散州、厅                                                                            |                                                                                                                                                                                                                         县                                                                                                                                                                                                                          |
|     [安庆府](../Page/安庆府.md "wikilink")     | [怀宁县](../Page/怀宁县.md "wikilink") |                                                                             /                                                                              |                                                                                                                                        [桐城县](../Page/桐城县.md "wikilink") [潜山县](../Page/潜山县.md "wikilink") [太湖县](../Page/太湖县.md "wikilink") [宿松县](../Page/宿松县.md "wikilink") [望江县](../Page/望江县.md "wikilink")                                                                                                                                        |
|     [徽州府](../Page/徽州府.md "wikilink")     |  [歙县](../Page/歙县.md "wikilink")  |                                                                             /                                                                              |                                                                                                                                         [休宁县](../Page/休宁县.md "wikilink") [婺源县](../Page/婺源县.md "wikilink") [祁门县](../Page/祁门县.md "wikilink") [黟县](../Page/黟县.md "wikilink") [绩溪县](../Page/绩溪县.md "wikilink")                                                                                                                                         |
| [宁国府](../Page/宁国府_\(安徽省\).md "wikilink") | [宣城县](../Page/宣城县.md "wikilink") |                                                                             /                                                                              |                                                                                                                                         [宁国县](../Page/宁国县.md "wikilink") [泾县](../Page/泾县.md "wikilink") [太平县](../Page/太平县.md "wikilink") [旌德县](../Page/旌德县.md "wikilink") [南陵县](../Page/南陵县.md "wikilink")                                                                                                                                         |
|     [池州府](../Page/池州府.md "wikilink")     | [贵池县](../Page/贵池县.md "wikilink") |                                                                             /                                                                              |                                                                                                                                        [青阳县](../Page/青阳县.md "wikilink") [铜陵县](../Page/铜陵县.md "wikilink") [石埭县](../Page/石埭县.md "wikilink") [建德县](../Page/建德县.md "wikilink") [东流县](../Page/东流县.md "wikilink")                                                                                                                                        |
| [太平府](../Page/太平府_\(安徽省\).md "wikilink") | [当涂县](../Page/当涂县.md "wikilink") |                                                                             /                                                                              |                                                                                                                                                                                         [芜湖县](../Page/芜湖县.md "wikilink") [繁昌县](../Page/繁昌县.md "wikilink")                                                                                                                                                                                          |
|     [庐州府](../Page/庐州府.md "wikilink")     | [合肥县](../Page/合肥县.md "wikilink") |                                             [六安州](../Page/六安州.md "wikilink") [无为州](../Page/无为州.md "wikilink")                                              |                                                                                                                                         [舒城县](../Page/舒城县.md "wikilink") [庐江县](../Page/庐江县.md "wikilink") [巢县](../Page/巢县.md "wikilink") [英山县](../Page/英山县.md "wikilink") [霍山县](../Page/霍山县.md "wikilink")                                                                                                                                         |
|     [凤阳府](../Page/凤阳府.md "wikilink")     | [凤阳县](../Page/凤阳县.md "wikilink") | [颍州](../Page/颍州.md "wikilink") [泗州](../Page/泗州.md "wikilink") [亳州](../Page/亳州.md "wikilink") [寿州](../Page/寿州.md "wikilink") [宿州](../Page/宿州.md "wikilink") | [临淮县](../Page/临淮县.md "wikilink") [怀远县](../Page/怀远县.md "wikilink") [定远县](../Page/定远县.md "wikilink") [凤台县](../Page/凤台县.md "wikilink") [灵璧县](../Page/灵璧县.md "wikilink") [盱眙县](../Page/盱眙县.md "wikilink") [天长县](../Page/天长县.md "wikilink")（从扬州府划入） [五河县](../Page/五河县.md "wikilink") [虹县](../Page/虹县.md "wikilink") [颍上县](../Page/颍上县.md "wikilink") [霍邱县](../Page/霍邱县.md "wikilink") [太和县](../Page/太和县.md "wikilink") [蒙城县](../Page/蒙城县.md "wikilink") |
|   [广德直隶州](../Page/广德直隶州.md "wikilink")   |                /                 |                                                                             /                                                                              |                                                                                                                                                                                                          [建平县](../Page/建平县.md "wikilink")                                                                                                                                                                                                          |
|   [滁州直隶州](../Page/滁州直隶州.md "wikilink")   |                /                 |                                                                             /                                                                              |                                                                                                                                                                                         [全椒县](../Page/全椒县.md "wikilink") [来安县](../Page/来安县.md "wikilink")                                                                                                                                                                                          |
|   [和州直隶州](../Page/和州直隶州.md "wikilink")   |                /                 |                                                                             /                                                                              |                                                                                                                                                                                                          [含山县](../Page/含山县.md "wikilink")                                                                                                                                                                                                          |

## 清朝

[順治二年](../Page/順治.md "wikilink")（1645年），[清軍攻占南京](../Page/清.md "wikilink")，将南直隶改为[江南省](../Page/江南省.md "wikilink")，最初设一位[江南布政使司](../Page/江南布政使司.md "wikilink")（驻[江宁府](../Page/江宁府.md "wikilink")），以及三[巡抚](../Page/巡抚.md "wikilink")([江南巡抚](../Page/江南巡抚.md "wikilink")、[凤庐巡抚和](../Page/凤庐巡抚.md "wikilink")[操江巡抚](../Page/操江巡抚.md "wikilink"))。根据[康熙年间的](../Page/康熙.md "wikilink")《大清会典》，布政使为一省之长。因此江南布政使的到任，可以认为江南省成立。

[顺治十八年](../Page/顺治.md "wikilink")，江南布政使分为[左](../Page/江南左布政使司.md "wikilink")、[右两位](../Page/江南右布政使司.md "wikilink")，分驻[江宁府](../Page/江宁府.md "wikilink")、[苏州府](../Page/苏州府.md "wikilink")。右布政使辖宁、镇、苏、松、常，左布政使辖余下地方。然这两位布政使仍冠以“江南”头衔，所以不可能认为江南分省。

[康熙五年](../Page/康熙.md "wikilink")，[江南右布政使增领](../Page/江南右布政使.md "wikilink")[扬州府](../Page/扬州府.md "wikilink")、[淮安府和](../Page/淮安府.md "wikilink")[徐州](../Page/徐州.md "wikilink")，左布政使则相应减少。因此左、右两布政使辖境已经和现在[安徽](../Page/安徽.md "wikilink")、[江苏两省十分接近](../Page/江苏.md "wikilink")。康熙六年，左、右布政使分别改为[安徽布政使](../Page/安徽布政使.md "wikilink")、[江苏布政使](../Page/江苏布政使.md "wikilink")（这是“安徽”、“江苏”这两个省名出现之始），依然分驻[江宁府](../Page/江宁府.md "wikilink")、[苏州府](../Page/苏州府.md "wikilink")，但江宁府本身属于江苏布政使辖境。此后，清代典籍里面“安徽”、“江苏”出现次数逐渐增加，但“江南省”字眼仍十分常见。康熙朝也并未有记载“分省”。所以这里的两个布政使辖境还只是两省的雏形罢了。另外，康熙二十九年，分定江南、[浙江](../Page/浙江.md "wikilink")[洋汛](../Page/洋汛.md "wikilink")，[嵊泗地域划归](../Page/嵊泗.md "wikilink")[江南省](../Page/江南省.md "wikilink")[江苏布政使司的](../Page/江苏布政使司.md "wikilink")[崇明县](../Page/崇明县.md "wikilink")。

[乾隆二十五年](../Page/乾隆.md "wikilink")，[安徽布政使从](../Page/安徽布政使.md "wikilink")[江宁府前往](../Page/江宁府.md "wikilink")[安庆府](../Page/安庆府.md "wikilink")。同时，[江苏布政使一分为二](../Page/江苏布政使.md "wikilink")——[江宁布政使](../Page/江宁布政使.md "wikilink")（驻[江宁](../Page/江宁府.md "wikilink")）和[江苏布政使](../Page/江苏布政使.md "wikilink")（驻[苏州](../Page/苏州.md "wikilink")）。江苏省境内形成了一巡抚两布政的格局。不过，从清朝中期开始，巡抚逐步取代了布政使成为一省之长，所以江苏内部分两个布政的情况并不被认为是“分省”，毕竟名义上还有“江苏巡抚”统辖坐镇。至此江苏、安徽两省官僚体系完全定型，在清代文献中出现频率逐渐高过“江南”的频率，因而坐实了分省的事实。

晚清时期，[太平天国定都南京](../Page/太平天国.md "wikilink")，安徽南部是其主要粮食和军事基地，在[曾国藩的](../Page/曾国藩.md "wikilink")[湘军和](../Page/湘军.md "wikilink")[陈玉成的太平军反复拉锯战中](../Page/陈玉成.md "wikilink")，安徽南部流失大部分人口，经济遭到嚴重打击，战后从河南、湖北居民移居皖南，文化习俗有所改变。同时期，[捻军也发源于贫困而土匪横行的安徽北部](../Page/捻军.md "wikilink")。[李鸿章在合肥附近招募](../Page/李鸿章.md "wikilink")[淮军赴江浙作战](../Page/淮军.md "wikilink")。太平天国之后淮军成了清朝的一支重要力量。

|                                          |                                  |                                                               |                                                                                                                                                                      |
| :--------------------------------------: | :------------------------------: | :-----------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|                    府                     |               附郭县                |                             散州、厅                              |                                                                                  县                                                                                   |
|     [安庆府](../Page/安庆府.md "wikilink")     | [怀宁县](../Page/怀宁县.md "wikilink") |                               /                               | [桐城县](../Page/桐城县.md "wikilink") [潜山县](../Page/潜山县.md "wikilink") [太湖县](../Page/太湖县.md "wikilink") [宿松县](../Page/宿松县.md "wikilink") [望江县](../Page/望江县.md "wikilink") |
|     [徽州府](../Page/徽州府.md "wikilink")     |  [歙县](../Page/歙县.md "wikilink")  |                               /                               |  [休宁县](../Page/休宁县.md "wikilink") [婺源县](../Page/婺源县.md "wikilink") [祁门县](../Page/祁门县.md "wikilink") [黟县](../Page/黟县.md "wikilink") [绩溪县](../Page/绩溪县.md "wikilink")  |
| [宁国府](../Page/宁国府_\(安徽省\).md "wikilink") | [宣城县](../Page/宣城县.md "wikilink") |                               /                               |  [宁国县](../Page/宁国县.md "wikilink") [泾县](../Page/泾县.md "wikilink") [太平县](../Page/太平县.md "wikilink") [旌德县](../Page/旌德县.md "wikilink") [南陵县](../Page/南陵县.md "wikilink")  |
|     [池州府](../Page/池州府.md "wikilink")     | [贵池县](../Page/贵池县.md "wikilink") |                               /                               | [青阳县](../Page/青阳县.md "wikilink") [铜陵县](../Page/铜陵县.md "wikilink") [石埭县](../Page/石埭县.md "wikilink") [建德县](../Page/建德县.md "wikilink") [东流县](../Page/东流县.md "wikilink") |
| [太平府](../Page/太平府_\(安徽省\).md "wikilink") | [当涂县](../Page/当涂县.md "wikilink") |                               /                               |                                                  [芜湖县](../Page/芜湖县.md "wikilink") [繁昌县](../Page/繁昌县.md "wikilink")                                                   |
|     [庐州府](../Page/庐州府.md "wikilink")     | [合肥县](../Page/合肥县.md "wikilink") |               [无为州](../Page/无为州.md "wikilink")                |                                   [舒城县](../Page/舒城县.md "wikilink") [庐江县](../Page/庐江县.md "wikilink") [巢县](../Page/巢县.md "wikilink")                                   |
|     [凤阳府](../Page/凤阳府.md "wikilink")     | [凤阳县](../Page/凤阳县.md "wikilink") | [寿州](../Page/寿州.md "wikilink") [宿州](../Page/宿州.md "wikilink") |                 [怀远县](../Page/怀远县.md "wikilink") [定远县](../Page/定远县.md "wikilink") [凤台县](../Page/凤台县.md "wikilink") [灵璧县](../Page/灵璧县.md "wikilink")                  |
|     [颍州府](../Page/颍州府.md "wikilink")     | [阜阳县](../Page/阜阳县.md "wikilink") |                [亳州](../Page/亳州.md "wikilink")                 | [颍上县](../Page/颍上县.md "wikilink") [霍邱县](../Page/霍邱县.md "wikilink") [涡阳县](../Page/涡阳县.md "wikilink") [太和县](../Page/太和县.md "wikilink") [蒙城县](../Page/蒙城县.md "wikilink") |
|   [广德直隶州](../Page/广德直隶州.md "wikilink")   |                /                 |                               /                               |                                                                   [建平县](../Page/建平县.md "wikilink")                                                                   |
|   [滁州直隶州](../Page/滁州直隶州.md "wikilink")   |                /                 |                               /                               |                                                  [全椒县](../Page/全椒县.md "wikilink") [来安县](../Page/来安县.md "wikilink")                                                   |
|   [和州直隶州](../Page/和州直隶州.md "wikilink")   |                /                 |                               /                               |                                                                   [含山县](../Page/含山县.md "wikilink")                                                                   |
|   [六安直隶州](../Page/六安直隶州.md "wikilink")   |                /                 |                               /                               |                                                  [英山县](../Page/英山县.md "wikilink") [霍山县](../Page/霍山县.md "wikilink")                                                   |
|   [泗州直隶州](../Page/泗州直隶州.md "wikilink")   |                /                 |                               /                               |                                  [盱眙县](../Page/盱眙县.md "wikilink") [天长县](../Page/天长县.md "wikilink") [五河县](../Page/五河县.md "wikilink")                                  |

## 民國

民國元年（1912年）1月1日[中华民国成立](../Page/中华民国.md "wikilink")，安徽首任都督兼民政长是同盟会员[柏文蔚](../Page/柏文蔚.md "wikilink")。民國二年（1913年）7月[二次革命](../Page/二次革命.md "wikilink")，安徽宣佈独立，反对袁世凯。失败后柏文蔚流亡日本，袁世凯命[倪嗣冲为安徽都督兼民政长](../Page/倪嗣冲.md "wikilink")。倪嗣冲都皖8年。民國九年（1920年）[直皖战争皖系失败](../Page/直皖战争.md "wikilink")，9月倪被解职。[张文生任安徽督军](../Page/张文生.md "wikilink")，[许世英任省长](../Page/许世英.md "wikilink")。民國十一年（1922年）10月，[马联甲任安徽督理军务](../Page/马联甲.md "wikilink")。以后历任安徽地方最高长官的还有[王普](../Page/王普.md "wikilink")（倪嗣冲女婿）、[王揖唐](../Page/王揖唐.md "wikilink")、[吕调元](../Page/吕调元.md "wikilink")、[姜登选](../Page/姜登选.md "wikilink")、[陈调元](../Page/陈调元.md "wikilink")。民國十四年（1926年），陈调元归顺[北伐](../Page/國民革命軍北伐.md "wikilink")[国民革命军](../Page/国民革命军.md "wikilink")。民國十六年（1927年）国共分裂之后，安徽西部的大别山区曾经是红军武装割据的地区之一。

民國二十六年（1937年），[全面抗战爆发](../Page/抗日战争_\(中国\).md "wikilink")，安徽成了[日本人](../Page/日本.md "wikilink")、[汪精卫政权](../Page/汪精卫政权.md "wikilink")、[国军和](../Page/國民革命軍.md "wikilink")[中国共产党的](../Page/中国共产党.md "wikilink")[新四军势力交错的地方](../Page/新四军.md "wikilink")。1940年，在安徽南部的泾县，发生国共冲突的[皖南事变](../Page/皖南事变.md "wikilink")。

民國三十四年（1945年）日本投降後，國民政府把安徽省會從[安慶遷往](../Page/安慶.md "wikilink")[合肥](../Page/合肥.md "wikilink")。民國三十七年至三十八年（1948年至1949年），安徽北部地方是[國共內戰期間](../Page/國共內戰.md "wikilink")[徐蚌會戰的主戰場](../Page/徐蚌會戰.md "wikilink")。此役國軍被殲55萬人，中共控制了安徽省長江以北地區。民國三十八年（1949年）4月15日，中共成立[皖北行署區](../Page/皖北行署區.md "wikilink")，管轄其統治區。4月27日，國軍獨立團團長[胡廣益聯合屯溪警保大隊長](../Page/胡廣益.md "wikilink")[方師岳投靠中共](../Page/方師岳.md "wikilink")。4月29日，共軍進駐屯溪。5月7日，中共成立[皖南行署區](../Page/皖南行署區.md "wikilink")，轄安徽江南地區。

1912年，[津浦铁路全线通车](../Page/津浦铁路.md "wikilink")，与[淮河航线交汇处的](../Page/淮河.md "wikilink")[蚌埠成为一个新兴城市](../Page/蚌埠.md "wikilink")。

## 中华人民共和国

### 1950年代

1949年[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，安徽一度以长江为界分为皖北和皖南2个行政公署（省级）。1952年又重新合并。省界有小幅的调整：[徽州的](../Page/徽州.md "wikilink")[婺源县划归江西](../Page/婺源.md "wikilink")，[盱眙县和](../Page/盱眙县.md "wikilink")[泗洪县划归江苏](../Page/泗洪县.md "wikilink")，作为交换，江苏徐州的[萧县和](../Page/萧县.md "wikilink")[砀山县划给了安徽](../Page/砀山县.md "wikilink")。但这又使得[天长县成为伸入江苏境内的一个锲子](../Page/天长县.md "wikilink")。

### 1960年代

在1959年\~1961年的大饥荒中，安徽省是死亡人数最多的省份之一。省委书记[曾希圣用](../Page/曾希圣.md "wikilink")"[包产到户](../Page/包产到户.md "wikilink")"来补救，在[七千人大会上遭到](../Page/七千人大会.md "wikilink")[刘少奇的批判](../Page/刘少奇.md "wikilink")。

### 1970年代

1978年，[凤阳县](../Page/凤阳县.md "wikilink")[小岗村是](../Page/小岗村.md "wikilink")“[包产到户](../Page/包产到户.md "wikilink")”的实验区。

### 1980年代

1980年代，安徽中南部就以[乡镇企业著称](../Page/乡镇企业.md "wikilink")。但[邓小平发展沿海改革开放政策](../Page/邓小平.md "wikilink")，安徽失去了[毛泽东](../Page/毛泽东.md "wikilink")[论十大关系中的沿海省份的定位](../Page/论十大关系.md "wikilink")，被[华东其他沿海省份远远抛在后面](../Page/华东.md "wikilink")，成为[中部经济区](../Page/中部经济区.md "wikilink")。

### 1990年代

1990年代开始，以上海为中心的[长江三角洲地区得到重视](../Page/长江三角洲.md "wikilink")，但是安徽东部地区一直没有能进入长三角经济区，只有[黄山市以观察员的身份参与一些旅游方面的分工合作](../Page/黄山市.md "wikilink")。\[1\]

### 21世纪

中国实施[中部崛起政策后](../Page/中部崛起.md "wikilink")，[安徽经济有一定的提高](../Page/安徽经济.md "wikilink")。[合肥市](../Page/合肥市.md "wikilink")、[马鞍山市](../Page/马鞍山市.md "wikilink")、[黄山市等安徽城市也陆续加盟](../Page/黄山市.md "wikilink")[长三角经济区](../Page/长江三角洲.md "wikilink")。2010年1月12日，[国务院批复](../Page/中华人民共和国国务院.md "wikilink")《[皖江城市带承接产业转移示范区规划](../Page/皖江城市带.md "wikilink")》。

## 参考文献

{{-}}

[AH](../Category/中国各省历史.md "wikilink")
[安徽歷史](../Category/安徽歷史.md "wikilink")

1.  [黄山市与长三角](http://www.tt91.com/article/09-06-08/13562289_1.shtml)