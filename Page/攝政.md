**攝政**是指在[君主制下](../Page/君主制.md "wikilink")，一個[國家的](../Page/國家.md "wikilink")[即位](../Page/即位.md "wikilink")[君主不能管理國家時](../Page/君主.md "wikilink")，由他人代替君主處理國政。攝政最常见的情况即是君主仍然幼小而不能親自定奪裁批朝政。在君主患病或不在國內等情況下也会偶尔出现。攝政者多由在位君主的直系亲属担任职务，如[儲君](../Page/儲君.md "wikilink")（多稱為[監國](../Page/監國.md "wikilink")）或[母親](../Page/母親.md "wikilink")（多稱為[垂簾聽政](../Page/垂帘听政.md "wikilink")），或是通常由称为**摄政王**的皇族所担任。除此之外，摄政者也有由君主的[外祖父等](../Page/外祖父.md "wikilink")[外戚](../Page/外戚.md "wikilink")，或是德高望重、深具資歷經驗的大臣（多称为[辅政大臣](../Page/辅政大臣.md "wikilink")）所擔任的。

有些攝政者攝政時，國家並沒有君主在位，這種情況下的攝政者是國家的實際統治者，只是沒有君主身份。

在摄政期间，摄政者給予建議、並讓皇帝從各項建議與執行中進行學習，直至年歲更長，而能親自執政為止。然而，於很多情況，攝政者都會獨攬大權不放，甚至把皇帝當作[傀儡](../Page/傀儡政權.md "wikilink")。故皇帝長大後，亦往往與這些攝政者產生權力矛盾，而引發權力鬥爭。單以清朝為例，就有清初[康熙帝除去](../Page/康熙帝.md "wikilink")[鰲拜](../Page/鰲拜.md "wikilink")，清末[光緒帝嘗試與](../Page/光緒帝.md "wikilink")[慈禧太后爭權](../Page/慈禧太后.md "wikilink")（但失敗）等例子。

## 歷史上著名的攝政者

### 中國

  - ［商］[伊尹](../Page/伊尹.md "wikilink")（对[太甲](../Page/太甲.md "wikilink")）
  - ［西周］[周公旦](../Page/周公旦.md "wikilink")（對[周成王](../Page/周成王.md "wikilink")）
  - ［西周］[共和执政](../Page/共和_\(西周\).md "wikilink")（对[周厉王](../Page/周厉王.md "wikilink")）
  - ［西汉］[霍光](../Page/霍光.md "wikilink")（對[汉昭帝](../Page/汉昭帝.md "wikilink")、[汉宣帝](../Page/汉宣帝.md "wikilink")）
  - ［西汉］[王莽](../Page/王莽.md "wikilink")（對[漢平帝](../Page/漢平帝.md "wikilink")、[孺子婴](../Page/孺子婴.md "wikilink")）
  - ［东汉］[曹操](../Page/曹操.md "wikilink")，[曹丕](../Page/曹丕.md "wikilink")（對[汉献帝](../Page/汉献帝.md "wikilink")）
  - ［蜀漢］[諸葛亮](../Page/諸葛亮.md "wikilink")（對[蜀漢後主](../Page/刘禅.md "wikilink")）
  - ［曹魏］[司马懿](../Page/司马懿.md "wikilink")，[司马师](../Page/司马师.md "wikilink")，[曹爽](../Page/曹爽.md "wikilink")（对[魏齐王](../Page/魏齐王.md "wikilink")）
  - ［曹魏］[司马昭](../Page/司马昭.md "wikilink")（对[魏高贵乡公](../Page/曹髦.md "wikilink")）
  - ［曹魏］[司马炎](../Page/司马炎.md "wikilink")（对[魏元帝](../Page/魏元帝.md "wikilink")）
  - ［明］[张居正](../Page/张居正.md "wikilink")（對[万历帝](../Page/万历帝.md "wikilink")）
  - ［清］[多爾袞](../Page/多爾袞.md "wikilink")，[孝莊文皇后](../Page/孝莊文皇后.md "wikilink")（對[順治帝](../Page/順治帝.md "wikilink")）
  - ［清］[索尼](../Page/索尼_\(人物\).md "wikilink")、[遏必隆](../Page/遏必隆.md "wikilink")、[蘇克薩哈](../Page/蘇克薩哈.md "wikilink")、[鰲拜](../Page/鰲拜.md "wikilink")、[孝莊文皇后](../Page/孝莊文皇后.md "wikilink")（对[康熙帝](../Page/康熙帝.md "wikilink")）
  - ［清］[顾命八大臣](../Page/顾命八大臣.md "wikilink")，[慈禧太后](../Page/慈禧太后.md "wikilink")，[慈安太后](../Page/慈安太后.md "wikilink")（對[同治帝](../Page/同治帝.md "wikilink")）
  - ［清］[慈禧太后](../Page/慈禧太后.md "wikilink")、[慈安太后](../Page/慈安太后.md "wikilink")（對[光緒帝](../Page/光緒帝.md "wikilink")）
  - ［清］[监国摄政王](../Page/监国.md "wikilink")[载沣](../Page/载沣.md "wikilink")，[隆裕太后](../Page/隆裕太后.md "wikilink")（對[宣統帝](../Page/溥仪.md "wikilink")）

### 日本

在古代[日本](../Page/日本.md "wikilink")，攝政是替未成年[天皇執政之臣](../Page/天皇.md "wikilink")，其在天皇成年後稱[關白](../Page/關白.md "wikilink")。自從858年[清和天皇即位](../Page/清和天皇.md "wikilink")、天皇外祖父[藤原良房攝政以來](../Page/藤原良房.md "wikilink")，攝政一職便由[藤原北家所獨佔](../Page/藤原北家.md "wikilink")（[丰臣秀吉和](../Page/丰臣秀吉.md "wikilink")[丰臣秀次父子是唯一的例外](../Page/丰臣秀次.md "wikilink")），直到明治時代。

  - [神功皇后](../Page/神功皇后.md "wikilink")
  - [聖德太子](../Page/聖德太子.md "wikilink")
  - [藤原氏](../Page/藤原氏.md "wikilink")
  - [藤原北家](../Page/藤原北家.md "wikilink")
  - [藤原良房](../Page/藤原良房.md "wikilink")
  - [藤原基經](../Page/藤原基經.md "wikilink")
  - [藤原道長](../Page/藤原道長.md "wikilink")
  - [二條齊敬](../Page/二條齊敬.md "wikilink")
  - [裕仁親王](../Page/昭和天皇.md "wikilink")

### 琉球國

[琉球國自](../Page/琉球國.md "wikilink")1372年向中國[朝貢以來](../Page/朝貢.md "wikilink")，每逢王位更替之時，中國必遣使[冊封](../Page/冊封.md "wikilink")。在先王薨去、嗣君尚未接受冊封時以[世子身份監國](../Page/世子.md "wikilink")，相當於攝政王。此外，攝政也是古琉球的一種官職。

### 越南

越南阮朝設置輔政大臣之職，由前任皇帝任命，以輔佐嗣君。不過到了法國殖民政府時期，輔政大臣為殖民政府任命，以架空皇帝。

  - [范登興](../Page/范登興.md "wikilink")、[黎文悅](../Page/黎文悅.md "wikilink")（輔佐[明命帝](../Page/明命帝.md "wikilink")）
  - [張登桂](../Page/張登桂.md "wikilink")（輔佐[紹治帝](../Page/紹治帝.md "wikilink")）
  - 張登桂、[武文解](../Page/武文解.md "wikilink")、[阮知方](../Page/阮知方.md "wikilink")、[林維浹](../Page/林維浹.md "wikilink")（輔佐[嗣德帝](../Page/嗣德帝.md "wikilink")）
  - [阮文祥](../Page/阮文祥.md "wikilink")、[尊室說](../Page/尊室說.md "wikilink")、[陳踐誠](../Page/陳踐誠.md "wikilink")（輔佐[育德帝](../Page/育德帝.md "wikilink")、[協和帝](../Page/協和帝.md "wikilink")）
  - 阮文祥、尊室說、嘉興王[阮福洪休](../Page/阮福洪休.md "wikilink")（輔佐[建福帝](../Page/建福帝.md "wikilink")）
  - 阮文祥、尊室說、嘉興王阮福洪休、懷德公[阮福綿㝝](../Page/阮福綿㝝.md "wikilink")（輔佐[咸宜帝](../Page/咸宜帝.md "wikilink")）
  - 壽春王[阮福綿定](../Page/阮福綿定.md "wikilink")、阮文祥、[阮有度](../Page/阮有度.md "wikilink")、[潘廷評](../Page/潘廷評.md "wikilink")（輔佐[同慶帝](../Page/同慶帝.md "wikilink")）
  - 綏理王[阮福綿寊](../Page/阮福綿寊.md "wikilink")、懷德郡王阮福綿㝝、[阮仲合](../Page/阮仲合.md "wikilink")、[張光憻](../Page/張光憻.md "wikilink")、[裴殷年](../Page/裴殷年.md "wikilink")、[阮紳](../Page/阮紳.md "wikilink")、[黃高啟](../Page/黃高啟.md "wikilink")（輔佐[成泰帝](../Page/成泰帝.md "wikilink")）
  - 安城王[阮福綿𡫯](../Page/阮福綿𡫯.md "wikilink")、[張如岡](../Page/張如岡.md "wikilink")、[黎貞](../Page/黎貞.md "wikilink")、[黃琨](../Page/黃琨_\(阮朝\).md "wikilink")、[尊室訢](../Page/尊室訢.md "wikilink")、[阮有排](../Page/阮有排.md "wikilink")、[王維楨](../Page/王維楨_\(阮朝\).md "wikilink")、[高春育](../Page/高春育.md "wikilink")、[陳廷樸](../Page/陳廷樸.md "wikilink")、[胡得忠](../Page/胡得忠.md "wikilink")、[段廷𧀲](../Page/段廷𧀲.md "wikilink")（輔佐[維新帝](../Page/維新帝.md "wikilink")）
  - [尊室訢](../Page/尊室訢.md "wikilink")（輔佐[保大帝](../Page/保大帝.md "wikilink")）

### 英國

#### 摄政王

[乔治四世是英国历史上唯一获得](../Page/乔治四世.md "wikilink")“摄政王”（Prince
Regent）称号的摄政，在1810年－1820年其父由于精神病无法视事时作为王储监国，国会因此设立“摄政王”职位。这段历史时期（以及当时的时尚、文化、建筑风格等）因此称为“[摄政时代](../Page/英國攝政時期.md "wikilink")”。

#### 大不列颠王国摄政

[安妮女王在](../Page/安妮女王.md "wikilink")1714年去世后，由首席法官为首的辅政大臣负责监国，同时派人前往[汉诺威迎来王位继承人](../Page/汉诺威.md "wikilink")[乔治一世赴英国](../Page/喬治一世_\(大不列顛\).md "wikilink")。

#### 英格兰王国摄政和苏格兰王国摄政

在合并为大不列颠王国之前，英格兰王国和苏格兰王国历史上都屡次出现过摄政和辅政大臣。其中当政时间较长的英格兰王国摄政包括：

  - [威廉·弗里茲朗](../Page/威廉·弗里茲朗.md "wikilink")（William
    Longchamp，1189年－1199年）
  - William
    Marshal，第一代[彭布羅克伯爵](../Page/彭布羅克伯爵.md "wikilink")（1216年－1219年）及貝爾·德伯格（Hubert
    de Burgh，第一代[肯特伯爵](../Page/肯特伯爵.md "wikilink")，1219年－1227年）
  - 亨利，第三代[蘭開斯特伯爵](../Page/蘭開斯特伯爵.md "wikilink")（1327年－1330年）為首的攝政委員會（Regent
    Committee）
  - 約翰，[貝德福德公爵](../Page/贝德福德公爵.md "wikilink")（1422年－1435年）與[漢弗萊](../Page/汉弗莱_\(格洛斯特公爵\).md "wikilink")，[格洛斯特公爵](../Page/格洛斯特公爵.md "wikilink")（1422年－1437年）
  - 第三代約克公爵[理查·金雀花](../Page/第三代約克公爵理查·金雀花.md "wikilink")（1454年－1455年；1455年－1456年）
  - 理查德，格洛斯特公爵（1483年）
  - [愛德華·西摩](../Page/愛德華·西摩.md "wikilink")，第一代[薩默塞特公爵](../Page/薩默塞特公爵.md "wikilink")（1547年－1550年）
  - [約翰·達德利](../Page/約翰·達德利.md "wikilink")，第一代[諾森伯蘭公爵](../Page/諾森伯蘭公爵.md "wikilink")（1550年－1553年）

### 泰国

### 匈牙利

  - [霍尔蒂·米克洛什](../Page/霍尔蒂·米克洛什.md "wikilink")（1920年－1944年），自稱攝政，然而摄政期間沒有實際在位的君主。

### 西班牙

  - [弗朗西斯科·佛朗哥](../Page/弗朗西斯科·佛朗哥.md "wikilink")（1947年－1975年），自稱攝政，然而摄政期間沒有實際在位的君主。

[攝政](../Category/攝政.md "wikilink")