是一款由[Factor
5开发并由](../Page/Factor_5.md "wikilink")[索尼电脑娱乐发行在](../Page/索尼电脑娱乐.md "wikilink")[PlayStation
3平台上的](../Page/PlayStation_3.md "wikilink")[動作遊戲](../Page/動作遊戲.md "wikilink")。本遊戲使用[荷里活的經驗技術](../Page/荷里活.md "wikilink")，更邀請負責「[-{zh-hans:罪恶之城;zh-hk:罪惡城;zh-tw:萬惡城市;}-](../Page/罪恶之城.md "wikilink")」、「[-{zh-hans:四眼天鸡;zh-hk:四眼雞丁;zh-tw:四眼天雞;}-](../Page/四眼天鸡.md "wikilink")」及「[-{zh-hans:受难曲;zh-hk:受難曲;zh-tw:耶穌受難記;}-](../Page/耶穌受難記.md "wikilink")」的好莱坞的音樂及美術總監John
Debney。

《龙穴》最早在2005年的E3游戏展上公布，并计划作为PlayStation
3的首发游戏于2006年11月发售，但游戏最后几经延期于2007年9月在北美发售、10月在日本和香港发售、11月在欧洲发售。游戏在发售后获得了业界的差评。而本作也是开发商Factor
5制作的最后一款游戏，公司于2009年关闭。\[1\]

## 概要

  - 舞台為人類與龍共存的太古的世界。被稱為「大分裂」時代來臨，以自然資源發展的「阿希裏亞帝國」與蒸汽技術發展的「魔蓋族」等二個文明互相持續···玩者扮演名為「隆（Rohn）」的戰士，操控龍在天空及大地縱橫驅馳。

<!-- end list -->

  - 歐美的遊戲標題為「Lair」，而日本的遊戲標題為「RISE FROM LAIR」。

## 関連情報

  - 於2007年7月20日在PLAYSTATION Store上公開高清畫質（720p）的短片。
  - 於2007年8月28日在PLAYSTATION Store上公開高清畫質（1080p）的宣傳片。

## 製作人員

  - 製作人：ブライアン クルーガー
  - 美術總監：ウェイン ロー
  - 音效：John Debney

## 相關條目

  - [龍](../Page/龍.md "wikilink")
  - [美國電影](../Page/美國電影.md "wikilink")

## 参考资料

## 外部連結

  - [Lair 龍潭虎穴 官方網站](http://www.jp.playstation.com/scej/title/lair/)

  - [SCEJ Software 官方網站](http://www.jp.playstation.com/scej/)

  - [Factor 5 官方網站](http://www.factor5.com/)

  - [PlayStation.com (Asia) 香港 Lair 龍潭虎穴
    中英文合版新聞發佈](https://web.archive.org/web/20070829041222/http://asia.playstation.com/tch_hk/index.php?q=node%2F957)

[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[Category:動作冒险遊戲](../Category/動作冒险遊戲.md "wikilink")
[Category:索尼互動娛樂遊戲](../Category/索尼互動娛樂遊戲.md "wikilink")
[Category:PlayStation 3游戏](../Category/PlayStation_3游戏.md "wikilink")
[Category:PlayStation
3独占游戏](../Category/PlayStation_3独占游戏.md "wikilink")
[Category:龍題材作品](../Category/龍題材作品.md "wikilink")
[Category:美國開發電子遊戲](../Category/美國開發電子遊戲.md "wikilink")
[Category:官方繁体中文化游戏](../Category/官方繁体中文化游戏.md "wikilink")

1.  [Lair: What went
    wrong](https://www.polygon.com/features/2018/1/17/16886514/lair-what-went-wrong).Polygon.2018-01-17.\[2018-01-21\].