**詹姆斯·丹佛斯·“丹”·奎尔**（James Danforth "Dan"
Quayle，）是[美国第](../Page/美国.md "wikilink")44任[副总统](../Page/副总统.md "wikilink")，曾分别担任代表印第安纳州的[参议员和](../Page/美國參議院.md "wikilink")[众议员](../Page/美國眾議院.md "wikilink")。現任[博龙资产管理旗下主管](../Page/博龙资产管理.md "wikilink")。

## 簡歷

奎尔在[印第安纳州的](../Page/印第安纳州.md "wikilink")[首府](../Page/首府.md "wikilink")[印第安纳波利斯出生](../Page/印第安纳波利斯.md "wikilink")。1969年在[德葩大学毕业](../Page/德葩大学.md "wikilink")，并取得[政治科学](../Page/政治科学.md "wikilink")[学士的资格](../Page/学士.md "wikilink")，畢業後加入不會被外派到越南戰場的美國國家衛隊陸軍擔任士官，並於服役期間取得Indiana
University School of Law –
Indianapolis法學士(J.D.)。其后在1976年以29岁的年轻形象赢取了[众议员的资格](../Page/美國眾議院.md "wikilink")。33岁时，再被选为[参议员](../Page/美國參議院.md "wikilink")。

1988年，年仅41岁的奎尔獲老布希提名副總統候選人。由于年龄的因素，奎尔在任期间始终受到“经验不足”的困扰，[民主党人士曾在此问题上对他大加攻击](../Page/民主黨_\(美國\).md "wikilink")。在1988年的副总统[电视辩论上](../Page/电视辩论.md "wikilink")，奎尔不适当地自比[約翰·肯尼迪以说明自己的年轻有为](../Page/約翰·肯尼迪.md "wikilink")，遭到民主党候选人[勞埃德·本特森奚落](../Page/勞埃德·本特森.md "wikilink")：“参议员，你根本不像肯尼迪”，这成为他政治生涯中最著名的镜头之一。

奎尔一直也是[美国传媒取笑的对象](../Page/美国传媒.md "wikilink")。其中的经典在他一次在传媒面前跟一班小学生练习英语拼字时出丑。當時他指一位小學生把“potato”（[马铃薯](../Page/马铃薯.md "wikilink")）拼错了，因為他認為正確的拼法應該是“potatoe”。由於他的這次訪問是直播，这件事情立即成為了全國的笑柄，並使奎尔背上了“不学无术”的名声，严重地影响了他的公众形相。另一方面，他亦因為這件事而“榮獲”1991年度第一屆[搞笑諾貝爾獎的教育獎](../Page/搞笑諾貝爾獎.md "wikilink")，“因為他示範了[科學教育的重要性](../Page/科學教育.md "wikilink")”。此外，[RFC文件的](../Page/RFC.md "wikilink")[惡搞版本有RFC](../Page/惡搞RFC.md "wikilink")
1437，當中部份內容以他為取笑對象。

上任後，他曾经以副总统的身份出访47个国家。1992年他與老布希競選落敗，他在副總統競選中敗給當時與他同樣年輕的參议员[阿尔·戈尔](../Page/阿尔·戈尔.md "wikilink")。

卸任後，他曾于1996年參選印第安纳州長和[2000年美國總統選舉](../Page/2000年美國總統選舉.md "wikilink")，但最終在黨內提名中落敗。他的兒子曾擔任美國眾議員。

[Category:美国副总统](../Category/美国副总统.md "wikilink")
[Category:美国共和党联邦参议员](../Category/美国共和党联邦参议员.md "wikilink")
[Category:印第安納州人](../Category/印第安納州人.md "wikilink")
[Category:共和党美国副总统](../Category/共和党美国副总统.md "wikilink")