**宁海路**是位于中国江苏省[南京市鼓楼区的一条南北向街道](../Page/南京.md "wikilink")，1930年代开辟。北起江苏路广场，南到[随家仓](../Page/随家仓.md "wikilink")（广州路）。与之交汇的道路有：[江苏路](../Page/江苏路_\(南京\).md "wikilink")、[山西路](../Page/山西路_\(南京\).md "wikilink")、[珞珈路](../Page/珞珈路_\(南京\).md "wikilink")、[颐和路](../Page/颐和路_\(南京\).md "wikilink")、[莫干路](../Page/莫干路_\(南京\).md "wikilink")、[牯岭路](../Page/牯岭路.md "wikilink")、北阴阳营、北京西路、天目路、武夷路、苏州路、南阴阳营、汉口西路、北东瓜市、南东瓜市等。
[Ninghai_Road.jpg](https://zh.wikipedia.org/wiki/File:Ninghai_Road.jpg "fig:Ninghai_Road.jpg")

## 建筑

  - 2号，[马鸿逵公馆旧址](../Page/马鸿逵.md "wikilink")，今部队住宅，列为南京市文物保护单位\[1\]
  - 5号，[马歇尔公馆旧址](../Page/马歇尔公馆旧址.md "wikilink")，今[南京军区司令部住宅](../Page/南京军区.md "wikilink")
    ，列为[江苏省文物保护单位](../Page/江苏省文物保护单位.md "wikilink")\[2\]
  - 11号，[蒋锄欧旧居](../Page/蒋锄欧.md "wikilink")，列为重要近现代保护建筑
  - 14号，原巴西大使馆旧址，列为南京市文物保护单位\[3\]
  - 15号，[黄仁霖旧居](../Page/黄仁霖.md "wikilink")，列为重要近现代保护建筑
  - 17号，[刘嘉树旧居](../Page/刘嘉树.md "wikilink")，列为重要近现代保护建筑
  - 122号（南段西侧），[南京师范大学](../Page/南京师范大学.md "wikilink")（原[金陵女子大学](../Page/金陵女子大学.md "wikilink")，兴建于1923年），列为全国重点文物保护单位

<File:Ninghai> Road 5.jpg|宁海路5号

## 参考文献

<div class="references-small">

<references />

</div>

[Category:南京街道](../Category/南京街道.md "wikilink")

1.  [鼓楼区各级文物保护单位一览表](http://www.njgl.gov.cn/art/2007/1/22/art_1945_14856.html)
2.  [鼓楼区各级文物保护单位一览表](http://www.njgl.gov.cn/art/2007/1/22/art_1945_14856.html)
3.  [鼓楼区各级文物保护单位一览表](http://www.njgl.gov.cn/art/2007/1/22/art_1945_14856.html)