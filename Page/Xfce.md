**Xfce**是[桌面環境](../Page/桌面環境.md "wikilink")，用於[Unix與](../Page/Unix.md "wikilink")[Unix-like作業系統](../Page/Unix-like.md "wikilink")，如[Linux與](../Page/Linux.md "wikilink")[FreeBSD](../Page/FreeBSD.md "wikilink")。Xfce由許多彼此獨立的組件所構成，預設的[視窗管理器為Xfwm](../Page/視窗管理器.md "wikilink")，亦可搭配[Openbox等其他視窗管理器協同運作](../Page/Openbox.md "wikilink")。Xfce的設計目的是「設計為可作為實際應用，快速載入及執行程式，並減少耗用系統資源」。（Olivier
Fourdan，原創者）

Xfce同時為程式設計者提供開發框架。除了Xfce本身，還有第三方的程式使用Xfce的[程序庫](../Page/程序庫.md "wikilink")，如文字編輯器
- Mousepad、多媒體播放程式 - Parole與終端機模擬器 - Terminal emulator。

Xfce建基在[GTK+之上](../Page/GTK+.md "wikilink")。它使用[Xfwm作為視窗管理器](../Page/Xfwm.md "wikilink")。"Xfce"的名字最初是代表"
Common
Environment"，但自從那時起，Xfce已被重寫了兩次而且不再使用XForms工具包。名字雖然仍被保留為Xfce，但現在已不會將其大寫作"XFce"。而開發者現在的任務是要使Xfce不再代表任何東西。

現在最新版本為4.12，是在2015年2月28日發行的\[1\]。

以Xfce作為預設桌面的發行版：

  - [Xubuntu](../Page/Xubuntu.md "wikilink")
  - [Xfld](../Page/Xfld.md "wikilink")（Xfce live demo）
  - [Archie](../Page/Archie_\(Linux\).md "wikilink")
  - [Manjaro Linux](../Page/Manjaro_Linux.md "wikilink")
  - [Morphix](../Page/Morphix.md "wikilink")（*LightGUI* version）
  - [Slax](../Page/Slax.md "wikilink")（*Popcorn Edition*）
  - [Whoppix](../Page/Whoppix.md "wikilink")
  - [Zenwalk](../Page/Zenwalk.md "wikilink")
  - [CDlinux](../Page/CDlinux.md "wikilink")

## 脚注

## 外部連結

  - [Xfce - 桌面环境](http://www.xfce.org/)
  - [Xfce主题和艺术设计](http://www.xfce-look.org/)
  - [Xfld "Xfce live demo" CD](http://www.xfld.org/Xfld/en/)

[Category:自由桌面环境](../Category/自由桌面环境.md "wikilink") [Category:X
Window系统](../Category/X_Window系统.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:使用GTK+的软件](../Category/使用GTK+的软件.md "wikilink")

1.