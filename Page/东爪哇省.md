| image_flag = {{\#property:p41}} }}

**東爪哇省**（[印尼语](../Page/印尼语.md "wikilink")：）是[印度尼西亚省份之一](../Page/印度尼西亚.md "wikilink")。它的省府是[泗水](../Page/泗水_\(印尼\).md "wikilink")。該省面積共47,922平方公里，西部與[中爪哇省相連](../Page/中爪哇省.md "wikilink")，管轄島嶼有[馬都拉島](../Page/馬都拉島.md "wikilink")、[卡里摩爪哇群島](../Page/卡里摩爪哇群島.md "wikilink")、[巴韦安島](../Page/巴韦安島.md "wikilink")、[康厄安群島](../Page/康厄安群島.md "wikilink")；[卡里摩爪哇群島面积约](../Page/卡里摩爪哇群島.md "wikilink")110.117平方公里。

## 地理

[缩略图](https://zh.wikipedia.org/wiki/File:Kwan_Sing_Bio_Temple,_Tuban,_East_Java,_Indonesia.jpg "fig:缩略图")
東爪哇省位處[爪哇島的東部](../Page/爪哇島.md "wikilink")，北部和南部分別面瀶[爪哇海和](../Page/爪哇海.md "wikilink")[印度洋](../Page/印度洋.md "wikilink")，東部隔海峽面對景色秀麗的旅遊區峇里島。首府泗水是印尼第二大城市，是印尼重要商港和軍港。

## 人口

東爪哇省約有人口34,766,000人（2000年統計數字）。當中有[印尼人](../Page/印尼人.md "wikilink")、[華人](../Page/華人.md "wikilink")、[印度人和](../Page/印度人.md "wikilink")[阿拉伯人](../Page/阿拉伯人.md "wikilink")。

## 語言

東爪哇省的語言分別是[爪哇语](../Page/爪哇语.md "wikilink")、[馬都拉语和](../Page/馬都拉语.md "wikilink")[印尼语](../Page/印尼语.md "wikilink")。

## 參考文獻

[\*](../Category/东爪哇省.md "wikilink")
[Category:爪哇島](../Category/爪哇島.md "wikilink")