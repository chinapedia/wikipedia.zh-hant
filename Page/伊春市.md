**伊春市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[黑龙江省下辖的](../Page/黑龙江省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于黑龙江省北部。总面积3.28万平方公里，人口121.19万。[汉族为主](../Page/汉族.md "wikilink")，还有[满](../Page/满族.md "wikilink")、[朝鲜](../Page/朝鲜族.md "wikilink")、[回](../Page/回族.md "wikilink")、[鄂伦春等少数民族](../Page/鄂伦春族.md "wikilink")。2005年聯合國授予伊春市「城市森林生態保護和可持續發展範例—綠色伊春」榮譽\[1\]\[2\]\[3\]。

## 历史

[1952-07_1951年新建的伊春工人俱乐部.png](https://zh.wikipedia.org/wiki/File:1952-07_1951年新建的伊春工人俱乐部.png "fig:1952-07_1951年新建的伊春工人俱乐部.png")
伊春市，以[汤旺河支流](../Page/汤旺河.md "wikilink")[伊春河得名](../Page/伊春河.md "wikilink")。伊春，[蒙古语](../Page/蒙古语.md "wikilink")“依逊”，意为“九数”。清代，先后隶属[齐齐哈尔副都统和](../Page/齐齐哈尔副都统.md "wikilink")[呼兰副都统管辖](../Page/呼兰副都.md "wikilink")。清末民初，划归汤原县管辖，为第五区（南岔区），为原始森林地区。1950年设伊春林业局。1952年9月29日设伊春县，隶属于松江省[松花江专署](../Page/松花江专署.md "wikilink")。1954年2月
松江省委根据东北局指示决定：撤销中共伊春县委，成立中共伊春林区工作委员会。中共伊春工委对伊春县、伊春森林工业管理局实行统一领导。[刘思聪任书记](../Page/刘思聪.md "wikilink")。1957年9月13日经国务院批准伊春县改伊春市，隶属黑龙江省松花江专署。1964年1月15日伊春市改为黑龙江省直接领导。1964年6月23日中发（1964）386号通知，中共中央、国务院批准，撤销伊春市，成立伊春特区，实行实行以林
业部党组织领导为主、省委领导为辅的政企合一管理体制。特区下设15个区，在原10个区的基础上，
增设乌敏河、友好、红星、东风、上甘岭5个区。各区和林业局实行政企合一。1965年4月16日，经省委批准，撤销伊新区，设置伊春镇。伊东区和伊东林业局实行政企合一。5月27日，经林业部批准，伊东、乌敏河林业局合并，名为乌敏河林业局。伊东区建制撤销，原伊东区的行政区域归乌敏河管辖。1966年11月，带岭林业实验局由国家林业部下放伊春林业管理局领导。

1967年5月5日经黑龙江省革命委员会批准，建立伊春市革命委员会，这标志着伊春特区实际改名为伊春市（[省辖市](../Page/省辖市.md "wikilink")）。下辖16个区革命委员会。同年，乌伊岭林业局建成投产。11月17日，克林区革命委员会成立。至此，伊春市革命委员会下辖17个区革命委员会（伊春、带岭、南岔、浩良河、大丰、美溪、翠峦、乌敏河、双子河、友好、上甘岭、五营、红星、新青、东风、乌伊岭、克林）。

1969年11月13日南岔区（南岔林业局）与浩良河区（浩良河林业局）合并，称南岔区（南岔林业局）；友好区（友好林业局）与双子河区（双子河林业局）合并，称友好区（友好林业局）；成立西林区。

1970年4月1日国务院批准：撤销伊春市，成立伊春地区，将铁力、嘉荫两县划归伊春地区。黑龙江省批准，将双丰、铁力、桃山、朗乡四个林业局（“南四局”）和铁力木材干馏厂划归伊春林业管理局。伊春地区革命委员会下辖2个县（嘉荫、铁力）、3个区（伊春、西林、克林）、13个区局合一的区（带岭、南岔、大丰、美溪、翠峦、乌敏河、友好、上甘岭、五营、红星、新青、东风、乌伊岭）、4个林业局（铁力、双丰、朗乡、桃山）。1972年5月23日，撤销克林区。1974年11月20日，黑龙江省革命委员会批复，带岭林业实验局仍由省林业总局直辖。1979年12月14日，国务院288号文件决定，恢复伊春市，由省直接领导，政企合一体制不变。原伊春地区领导的嘉荫、铁力2县仍归伊春市领导。1980年10月25日，伊春市革命委员会改为伊春市人民政府。1983年12月24日，黑龙江省人民政府批复，乌敏河区改为乌马河区、东风区改为汤旺河区、大丰区改为金山屯区。

2008年國務院於首批[資源枯竭城市名單中將伊春列為](../Page/資源枯竭城市.md "wikilink")5個資源型城市經濟轉型試點城市之一。

## 地理

境内为低山丘陵区，[小兴安岭主脉由此向东南延伸](../Page/小兴安岭.md "wikilink")。属中温带大陆性季风气候，年降水量约630毫米，年均温1.0℃，1月均温－22.5℃，7月均温21.0℃。

### 气候

属于[温带季风气候](../Page/温带季风气候.md "wikilink")。

## 政治

### 现任领导

<table>
<caption>伊春市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党伊春市委员会.md" title="wikilink">中国共产党<br />
伊春市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/伊春市人民代表大会.md" title="wikilink">伊春市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/伊春市人民政府.md" title="wikilink">伊春市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议伊春市委员会.md" title="wikilink">中国人民政治协商会议<br />
伊春市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/赵万山.md" title="wikilink">赵万山</a>[4]</p></td>
<td><p><a href="../Page/于大海.md" title="wikilink">于大海</a>[5]</p></td>
<td><p><a href="../Page/韩库.md" title="wikilink">韩库</a>[6]</p></td>
<td><p><a href="../Page/王瑞.md" title="wikilink">王瑞</a>[7]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td></td>
<td><p><a href="../Page/黑龙江省.md" title="wikilink">黑龙江省</a><a href="../Page/哈尔滨市.md" title="wikilink">哈尔滨市</a></p></td>
<td><p><a href="../Page/吉林省.md" title="wikilink">吉林省</a><a href="../Page/榆树市.md" title="wikilink">榆树市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年12月</p></td>
<td><p>2019年1月</p></td>
<td><p>2016年11月</p></td>
<td><p>2018年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖15个[市辖区](../Page/市辖区.md "wikilink")、1个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[伊春区](../Page/伊春区.md "wikilink")、[南岔区](../Page/南岔区.md "wikilink")、[友好区](../Page/友好区.md "wikilink")、[西林区](../Page/西林区.md "wikilink")、[翠峦区](../Page/翠峦区.md "wikilink")、[新青区](../Page/新青区.md "wikilink")、[美溪区](../Page/美溪区.md "wikilink")、[金山屯区](../Page/金山屯区.md "wikilink")、[五营区](../Page/五营区.md "wikilink")、[乌马河区](../Page/乌马河区.md "wikilink")、[汤旺河区](../Page/汤旺河区.md "wikilink")、[带岭区](../Page/带岭区.md "wikilink")、[乌伊岭区](../Page/乌伊岭区.md "wikilink")、[红星区](../Page/红星区.md "wikilink")、[上甘岭区](../Page/上甘岭区.md "wikilink")
  - 县级市：[铁力市](../Page/铁力市.md "wikilink")
  - 县：[嘉荫县](../Page/嘉荫县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>区划代码[8]</p></th>
<th><p>区划名称</p></th>
<th><p>汉语拼音</p></th>
<th><p>面积[9]<br />
（平方公里）</p></th>
<th><p>政府驻地</p></th>
<th><p>邮政编码</p></th>
<th><p>行政区划[10]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>街道</p></td>
<td><p>镇</p></td>
<td><p>乡</p></td>
<td><p>其中：<br />
民族乡</p></td>
<td><p>社区</p></td>
<td><p>行政村</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>230700</p></td>
<td><p>伊春市</p></td>
<td></td>
<td><p>32759.66</p></td>
<td><p><a href="../Page/伊春区.md" title="wikilink">伊春区</a></p></td>
<td><p>153000</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>230702</p></td>
<td><p>伊春区</p></td>
<td></td>
<td><p>88.82</p></td>
<td><p><a href="../Page/旭日街道_(伊春市).md" title="wikilink">旭日街道</a></p></td>
<td><p>153000</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p>230703</p></td>
<td><p>南岔区</p></td>
<td></td>
<td><p>3080.22</p></td>
<td><p><a href="../Page/联合街道_(伊春市).md" title="wikilink">联合街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>230704</p></td>
<td><p>友好区</p></td>
<td></td>
<td><p>2364.21</p></td>
<td><p><a href="../Page/友好街道_(伊春市).md" title="wikilink">友好街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>230705</p></td>
<td><p>西林区</p></td>
<td></td>
<td><p>381.34</p></td>
<td><p><a href="../Page/西林街道_(伊春市).md" title="wikilink">西林街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>230706</p></td>
<td><p>翠峦区</p></td>
<td></td>
<td><p>1539.88</p></td>
<td><p><a href="../Page/曙光街道_(伊春市).md" title="wikilink">曙光街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>230707</p></td>
<td><p>新青区</p></td>
<td></td>
<td><p>1049.06</p></td>
<td><p><a href="../Page/新青街道.md" title="wikilink">新青街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>230708</p></td>
<td><p>美溪区</p></td>
<td></td>
<td><p>2256.68</p></td>
<td><p><a href="../Page/美溪街道.md" title="wikilink">美溪街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>230709</p></td>
<td><p>金山屯区</p></td>
<td></td>
<td><p>1852.68</p></td>
<td><p><a href="../Page/奋斗街道_(伊春市).md" title="wikilink">奋斗街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>230710</p></td>
<td><p>五营区</p></td>
<td></td>
<td><p>1039.96</p></td>
<td><p><a href="../Page/五营街道.md" title="wikilink">五营街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>230711</p></td>
<td><p>乌马河区</p></td>
<td></td>
<td><p>1211.12</p></td>
<td><p><a href="../Page/乌马河街道.md" title="wikilink">乌马河街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>230712</p></td>
<td><p>汤旺河区</p></td>
<td></td>
<td><p>1243.71</p></td>
<td><p><a href="../Page/河南街道_(伊春市).md" title="wikilink">河南街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>230713</p></td>
<td><p>带岭区</p></td>
<td></td>
<td><p>1041.62</p></td>
<td><p><a href="../Page/带岭街道.md" title="wikilink">带岭街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>230714</p></td>
<td><p>乌伊岭区</p></td>
<td></td>
<td><p>898.73</p></td>
<td><p><a href="../Page/乌伊岭街道.md" title="wikilink">乌伊岭街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>230715</p></td>
<td><p>红星区</p></td>
<td></td>
<td><p>885.05</p></td>
<td><p><a href="../Page/红星街道_(伊春市).md" title="wikilink">红星街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>230716</p></td>
<td><p>上甘岭区</p></td>
<td></td>
<td><p>633.67</p></td>
<td><p><a href="../Page/红山街道_(伊春市).md" title="wikilink">红山街道</a></p></td>
<td><p>153000</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>230722</p></td>
<td><p>嘉荫县</p></td>
<td></td>
<td><p>6748.44</p></td>
<td><p><a href="../Page/朝阳镇_(嘉荫县).md" title="wikilink">朝阳镇</a></p></td>
<td><p>153200</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>230781</p></td>
<td><p>铁力市</p></td>
<td></td>
<td><p>6444.49</p></td>
<td><p><a href="../Page/铁力镇.md" title="wikilink">铁力镇</a></p></td>
<td><p>152500</p></td>
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>伊春市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[11]（2010年11月）</p></th>
<th><p>户籍人口[12]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>伊春市</p></td>
<td><p>1148126</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>伊春区</p></td>
<td><p>145954</p></td>
<td><p>12.71</p></td>
</tr>
<tr class="even">
<td><p>南岔区</p></td>
<td><p>118593</p></td>
<td><p>10.33</p></td>
</tr>
<tr class="odd">
<td><p>友好区</p></td>
<td><p>53493</p></td>
<td><p>4.66</p></td>
</tr>
<tr class="even">
<td><p>西林区</p></td>
<td><p>52741</p></td>
<td><p>4.59</p></td>
</tr>
<tr class="odd">
<td><p>翠峦区</p></td>
<td><p>44960</p></td>
<td><p>3.92</p></td>
</tr>
<tr class="even">
<td><p>新青区</p></td>
<td><p>43061</p></td>
<td><p>3.75</p></td>
</tr>
<tr class="odd">
<td><p>美溪区</p></td>
<td><p>41028</p></td>
<td><p>3.57</p></td>
</tr>
<tr class="even">
<td><p>金山屯区</p></td>
<td><p>39218</p></td>
<td><p>3.42</p></td>
</tr>
<tr class="odd">
<td><p>五营区</p></td>
<td><p>33988</p></td>
<td><p>2.96</p></td>
</tr>
<tr class="even">
<td><p>乌马河区</p></td>
<td><p>31416</p></td>
<td><p>2.74</p></td>
</tr>
<tr class="odd">
<td><p>汤旺河区</p></td>
<td><p>30979</p></td>
<td><p>2.70</p></td>
</tr>
<tr class="even">
<td><p>带岭区</p></td>
<td><p>32259</p></td>
<td><p>2.81</p></td>
</tr>
<tr class="odd">
<td><p>乌伊岭区</p></td>
<td><p>21147</p></td>
<td><p>1.84</p></td>
</tr>
<tr class="even">
<td><p>红星区</p></td>
<td><p>21845</p></td>
<td><p>1.90</p></td>
</tr>
<tr class="odd">
<td><p>上甘岭区</p></td>
<td><p>19496</p></td>
<td><p>1.70</p></td>
</tr>
<tr class="even">
<td><p>嘉荫县</p></td>
<td><p>68579</p></td>
<td><p>5.97</p></td>
</tr>
<tr class="odd">
<td><p>铁力市</p></td>
<td><p>349369</p></td>
<td><p>30.43</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")1148126人\[13\]。其中，男性为577388人，占50.29%；女性为570738人，占49.71%。性别比（以女性为100）为101.17。0－14岁的人口为107418人，占9.36%；15－64岁的人口为907731人，占79.06%；65岁及以上的人口为132977人，占11.58%。

2017年末全市总人口1159286人。其中：城镇人口941288人；乡村人口217998人。户籍人口城镇化率81.2%，比上年提高0.4个百分点。男性人口575072人，占总人口的49.6%；女性人口584214人，占总人口的50.4%。全市出生人口5192人，出生率为4.4%；死亡人口15567人，死亡率为13.3%，人口自然增长率为-8.9%。出生人口性别比为107.9。

## 资源

有金、银、铜、铁、锡、铅、煤等矿藏。

盛产药材和野生动物。土特产有[人参](../Page/人参.md "wikilink")、[刺五加](../Page/刺五加.md "wikilink")、[松子](../Page/松子.md "wikilink")、[木耳](../Page/木耳.md "wikilink")、[蘑菇](../Page/蘑菇.md "wikilink")、[猴头菇](../Page/猴头菇.md "wikilink")、[飞龙鸟](../Page/飞龙鸟.md "wikilink")、[紫貂](../Page/紫貂.md "wikilink")、[鹿茸](../Page/鹿茸.md "wikilink")、[笃斯越桔](../Page/笃斯越桔.md "wikilink")。

## 经济

是中国重点林区之一，素有祖国林都和红松的故乡之称。森林总蓄积量达2.45亿立方米。树种主要有红松、落叶松、云冷杉、杨、桦、胡桃楸、水曲柳。年产木材占全国的1／10。

## 交通

南乌铁路。

哈尔滨市至伊春干线公路的终点。

[伊春林都機場](../Page/伊春林都機場.md "wikilink")（2009年8月27日通航）

## 教育

高职（专科）院校：[伊春职业技术学院](../Page/伊春职业技术学院.md "wikilink")

## 名胜

  - 2008年中国第一个[国家公园](../Page/国家公园.md "wikilink")：[汤旺河国家公园](../Page/汤旺河国家公园.md "wikilink")(范围包含2007年国家AAAA级旅游区：汤旺河林海奇石风景区)
  - 2002年第三批国家AAAA级旅游区、国家级森林公园：伊春[五营国家森林公园](../Page/五营国家森林公园.md "wikilink")
  - 2007年国家AAAA级旅游区、第二批国家地质公园：黑龙江[嘉荫恐龙国家地质公园](../Page/嘉荫恐龙国家地质公园.md "wikilink")
  - 第三批国家地质公园：黑龙江[伊春花岗岩石林国家地质公园](../Page/伊春花岗岩石林国家地质公园.md "wikilink")

### 国家级自然保护区

  - 黑龙江[丰林自然保护区](../Page/丰林自然保护区.md "wikilink")（丰林生物圈保护区，[联合国人与生物圈计划](../Page/联合国人与生物圈计划.md "wikilink")，1997年）
  - 黑龙江[凉水自然保护区](../Page/凉水自然保护区.md "wikilink")
  - 伊春[乌伊岭湿地](../Page/乌伊岭湿地.md "wikilink")（2007）
  - [红星湿地](../Page/红星湿地.md "wikilink")（2007）

### 国家级森林公园

  - [溪水国家森林公园](../Page/溪水国家森林公园.md "wikilink")
  - [梅花山国家森林公园](../Page/梅花山国家森林公园.md "wikilink")
  - [回龙湾国家森林公园](../Page/回龙湾国家森林公园.md "wikilink")
  - [桃山国家森林公园](../Page/桃山国家森林公园.md "wikilink")
  - [日月峡国家森林公园](../Page/日月峡国家森林公园.md "wikilink")
  - [茅兰沟国家森林公园](../Page/茅兰沟国家森林公园.md "wikilink")
  - [兴安国家森林公园](../Page/兴安国家森林公园.md "wikilink")
  - [金山国家森林公园](../Page/金山国家森林公园.md "wikilink")
  - [小兴安岭石林国家森林公园](../Page/小兴安岭石林国家森林公园.md "wikilink")

## 友好城市

  - [巴特维尔东根市](../Page/巴特維爾東根.md "wikilink")\[14\]（1988年8月25日）

  - [比罗比詹市](../Page/比羅比詹.md "wikilink")\[15\]（2011年5月18日）

  - [阿祖加市](../Page/阿祖加.md "wikilink")\[16\]（2013年6月16日）

  - [卡姆罗斯市](../Page/卡姆罗斯.md "wikilink")\[17\]（2015年10月9日）

  - [凯米耶尔维市](../Page/凱米耶爾維.md "wikilink")\[18\]（2017年9月22日）

## 参见

  - [河南航空8387号班机空难](../Page/河南航空8387号班机空难.md "wikilink")

## 外部链接

  - [伊春市人民政府](http://www.yc.gov.cn/)

## 資料來源

[伊春](../Category/伊春.md "wikilink")
[Category:黑龙江地级市](../Category/黑龙江地级市.md "wikilink")
[黑](../Category/中国大城市.md "wikilink")
[黑](../Category/国家卫生城市.md "wikilink")
[Category:中华人民共和国首批资源枯竭型城市](../Category/中华人民共和国首批资源枯竭型城市.md "wikilink")
[Category:中国地理之最](../Category/中国地理之最.md "wikilink")
[5](../Category/全国文明城市.md "wikilink")

1.  [黑龍江伊春市被聯合國授予「綠色伊春」稱號](http://www.stdaily.com/big5/development/2007-11/09/content_738968.htm)，振興東北網，原載於[新華網](../Page/新華網.md "wikilink")，2005年10月12日
2.  [發展循環經濟營建綠色伊春](http://www.stdaily.com/big5/development/2007-11/09/content_738968.htm)，科技日報科報網
3.  [興生態伊春
    建綠色地球](http://www.wenweipo.com/news_print.phtml?news_id=zt0709030006)
    《[文匯報](../Page/文匯報.md "wikilink")》2007年9月3日
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.