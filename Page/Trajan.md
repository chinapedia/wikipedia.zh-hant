**Trajan**
是一种[衬线](../Page/衬线.md "wikilink")[字体](../Page/字体.md "wikilink")，是卡罗·吐温布里（Carol
Twombly）在1989年为[Adobe公司设计制作的](../Page/Adobe.md "wikilink")。

Trajan一词本身，是[罗马帝国](../Page/罗马帝国.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")[图拉真名字的](../Page/图拉真.md "wikilink")[英文拼写](../Page/英文.md "wikilink")。该字体的设计基于“古罗马方块大写字母”（capitalis
monumentalis）的风格，常用于公元[一世纪](../Page/一世纪.md "wikilink")[古罗马石柱石碑雕刻使用](../Page/古罗马.md "wikilink")。由于罗马时代还没有使用[小写字母](../Page/小写字母.md "wikilink")，因此Trajan字体全部都是[大写字母](../Page/大写字母.md "wikilink")。代替小写字母的，是一种[小型大写字母](../Page/小型大写字母.md "wikilink")。

现在该字体的电子版本在2001年复刻，改名为Trajan
Pro，作为新型的[OpenType字体](../Page/OpenType.md "wikilink")
(.otf ) 通过Adobe公司发售。该字体也随Adobe [Creative
Suite安装盘发布](../Page/Creative_Suite.md "wikilink")。字体家族包括正体（Regular）和粗体（Bold），版本号为1.004，[字符集涵盖了Adobe](../Page/字符集.md "wikilink")
Western 2 和 Adobe CE，包含[中欧字符和小型大写字母](../Page/中欧.md "wikilink")。

这种古风字体广泛用于古罗马石碑上。近来一些[电影](../Page/电影.md "wikilink")[海报](../Page/海报.md "wikilink")、[电视和](../Page/电视.md "wikilink")[书籍封面也经常使用这种字体](../Page/书籍.md "wikilink")。

## 外部链接

  - [1](http://www.adobe.com/type/browser/P/P_1724.html?PID=1788659)
    Adobe字体官方网页
  - [Typowiki: Trajan](http://typophile.com/wiki/trajan)
  - [Trajan's
    Column](http://penelope.uchicago.edu/~grout/encyclopaedia_romana/imperialfora/trajan/column.html)，包括*capitalis
    monumentalis*文本字体的细节
  - [Etched in
    Stone](https://web.archive.org/web/20080409215637/http://www.veer.com/ideas/etched/)
    在电影标题中使用Trajan的一个动画
  - [Trajan is the Movie
    Font](http://www.youtube.com/watch?v=t87QKdOJNv8)，a satirical video
    poking fun at the omnipresence of Trajan in the movie world.

[Category:Adobe字体](../Category/Adobe字体.md "wikilink")
[Category:衬线字体](../Category/衬线字体.md "wikilink")