**沱江**，古称江沱，又名外江、中江，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[四川省中部的一条河流](../Page/四川省.md "wikilink")，为[长江的一条较小的一级支流](../Page/长江.md "wikilink")。

沱江有三个源头。

  - 东源为[绵远河](../Page/绵远河.md "wikilink")，源自[阿坝藏族羌族自治州的](../Page/阿坝藏族羌族自治州.md "wikilink")[茂县](../Page/茂县.md "wikilink")[茶坪山](../Page/茶坪山.md "wikilink")，南流经过[绵竹市](../Page/绵竹市.md "wikilink")，再向西南流经[德阳市](../Page/德阳市.md "wikilink")、[广汉市](../Page/广汉市.md "wikilink")。长180km
  - 中源为[石亭江](../Page/石亭江.md "wikilink")，源自[什邡市北和茂县南的九顶山南麓](../Page/什邡市.md "wikilink")，沿什邡和绵竹的边界南流，流经广汉，在[金堂县境内汇合东源](../Page/金堂县.md "wikilink")。长141km
  - 西源[湔江](../Page/湔江.md "wikilink")，出自[彭州市北茶坪山南麓](../Page/彭州市.md "wikilink")。长139km。

三源在[金堂县](../Page/金堂县.md "wikilink")[赵镇汇合后成为沱江的干流](../Page/赵镇_\(金堂县\).md "wikilink")。一般认为东源是正源。

另外此处又有[岷江分出的](../Page/岷江.md "wikilink")[毗河和](../Page/毗河.md "wikilink")[清白江汇入](../Page/清白江.md "wikilink")。

干流从[金堂县](../Page/金堂县.md "wikilink")[赵镇向南流经金堂县](../Page/赵镇_\(金堂县\).md "wikilink")、[简阳市](../Page/简阳市.md "wikilink")、[资阳市雁江区](../Page/雁江区.md "wikilink")，[资中县](../Page/资中县.md "wikilink")、[内江市市中区](../Page/市中区_\(内江市\).md "wikilink")、[富顺县](../Page/富顺县.md "wikilink")，在[泸州市注入长江](../Page/泸州市.md "wikilink")，长522km，河道弯曲系数2.1。左岸较大的一级支流有濑溪河、大清流河和阳化河；右岸有威远河和球溪河。

全长700公里，流域面积2.79万平公里。

金堂县赵家渡以上为上游，[绵竹市以上为山区河道](../Page/绵竹市.md "wikilink")，以下河谷宽敞平坦。赵家渡至[资中为中游](../Page/资中.md "wikilink")，穿越丘陵地带，有一系列的峡谷，包括金堂峡长13km；月亮峡长21km；青山峡长2km。下游流经盆地丘陵区，河道深而弯曲，河口谷宽300-500米。

流域内是四川省的工业集中地区，人口密度大，森林覆盖率很低，有水土流失和严重的水污染的问题。

## 航运

目前通航河段为[泸州河口至自贡](../Page/泸州.md "wikilink")[牛佛镇](../Page/牛佛镇.md "wikilink")，为[VI级航道](../Page/中国内河航道等级.md "wikilink")，全长145.4km，其中自贡市101.9km，泸州市43.5km。航道升级改造工程包括新建小市、幺滩、银蛇滩3个梯级枢纽，改造黄葛灏、黄泥滩、流滩坝3梯级航电枢纽的通航建筑物，整治库尾航道，提升至[IV级航道](../Page/中国内河航道等级.md "wikilink")，常年通航500吨级。

## 沱江特大水污染事故

2004年3月，[四川化工股份有限公司高浓度氨氮超标废水](../Page/四川化工股份有限公司.md "wikilink")（2611－7618mg/L）经支流[毗河排入沱江](../Page/毗河.md "wikilink")，造成沿线简阳、资阳、资中和内江近100万群众从3月2日到到3月26日饮水中断25天，死鱼100万公斤，直接经济损失2.19亿多元。\[1\]之后沱江干流全面禁止网箱养鱼，沿岸城市纷纷另辟新的饮用水源地。沿岸仍是消化系统类癌症高发区。\[2\]

## 画廊

[File:LuzhouTuoRiver.JPG|沱江泸州段](File:LuzhouTuoRiver.JPG%7C沱江泸州段)
<File:Festival> scene on Tuo River.jpg|沱江上的节日景

## 註釋

{{-}}

[沱江水系](../Category/沱江水系.md "wikilink")
[Category:四川河流](../Category/四川河流.md "wikilink")
[Category:泸州地理](../Category/泸州地理.md "wikilink")
[Category:阿坝地理](../Category/阿坝地理.md "wikilink")
[Category:德阳地理](../Category/德阳地理.md "wikilink")
[Category:内江地理](../Category/内江地理.md "wikilink")
[Category:自贡地理](../Category/自贡地理.md "wikilink")

1.  [国家环保总局
    《2004年中国环境状况公报》](http://www.sepa.gov.cn/plan/zkgb/04zkgb/200506/t20050602_67173.htm)

2.  [南方都市报 《四川污染最严重的河流
    沱江“劣五类”之困》](http://www.nddaily.com/A/html/2007-11/07/content_301644.htm)