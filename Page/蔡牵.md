[光明](../Page/光明_\(蔡牽\).md "wikilink") |時代 = |主君 = |屆數 = |最高職務 = 鎮海威武王
|前任 = |繼任 = |圖片名稱 = |圖片大小 = |圖片說明 = |字 = |號= |族裔 =
[河洛人](../Page/河洛人.md "wikilink") |信仰 = |出生日期 = 1761年 |出生地點 =
[福建](../Page/福建.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[同安縣](../Page/同安縣.md "wikilink")
|婚年 = |逝世日期 = 1809年 |逝世地點 =
[浙江省](../Page/浙江省.md "wikilink")[台州府](../Page/台州府.md "wikilink")[渔山列島](../Page/渔山列島.md "wikilink")
|經歷 = |著作 = |注釋 = }}

**蔡牵**（），一作**蔡騫**，[福建](../Page/福建.md "wikilink")[同安出身的](../Page/同安.md "wikilink")[河洛人](../Page/河洛人.md "wikilink")。[清乾嘉年間的一名](../Page/清.md "wikilink")[海賊領袖](../Page/海賊.md "wikilink")，活躍並稱霸於[台灣海峽](../Page/台灣海峽.md "wikilink")，受眾人尊稱為**「大出海」**。後來遂於[滬尾](../Page/淡水區.md "wikilink")（今[臺灣](../Page/臺灣.md "wikilink")[臺北](../Page/臺北.md "wikilink")[淡水區](../Page/淡水區.md "wikilink")）建立政權，年號**[光明](../Page/光明_\(蔡牽\).md "wikilink")**，號**鎮海威武王**，並持有「光明正大」[玉璽](../Page/玉璽.md "wikilink")。建國之後，長期與清軍交戰，然先盛後衰，最終在與清將[王得祿](../Page/王得祿.md "wikilink")、[邱良功的一場海戰中徹底敗陣](../Page/邱良功.md "wikilink")，以自己的[火炮炸船自殺](../Page/火炮.md "wikilink")。\[1\]

蔡牽對當時[華南與](../Page/華南.md "wikilink")[台灣的社會有著重大的影響力](../Page/台灣.md "wikilink")，許多地方[土豪](../Page/土豪.md "wikilink")、[世族都向他繳交](../Page/世族.md "wikilink")[保護費](../Page/保護費.md "wikilink")，保持著密切的關係；例如[艋舺的](../Page/艋舺.md "wikilink")[張得寶](../Page/張得寶.md "wikilink")、[清水的](../Page/清水區_\(臺中市\).md "wikilink")[蔡源順](../Page/蔡源順.md "wikilink")、[臺南府城的](../Page/台灣府城.md "wikilink")[林朝英](../Page/林朝英_\(清朝\).md "wikilink")、[金門的](../Page/金門.md "wikilink")[黃俊以及](../Page/黃俊.md "wikilink")[鼓浪嶼的](../Page/鼓浪嶼.md "wikilink")[黃旭齋](../Page/黃旭齋.md "wikilink")（黃勗齋）等地方首富皆持有蔡牽給予的通行憑證，或者是署名蔡牽題的「[媽祖令牌](../Page/媽祖.md "wikilink")」，或者黃色「免劫令旗」，頗具有統治者諭令的意味。\[2\]

## 生平

### 早年

家境貧寒，父母早逝，蔡牽自幼便自食其力，以「彈[棉被](../Page/棉被.md "wikilink")」（把棉被的棉花彈鬆、修復）及[補破網](../Page/補破網.md "wikilink")（修補漁網），勉強餬口。[乾隆五十九年](../Page/乾隆.md "wikilink")（1794年），其因[饑荒犯法而亡命入海](../Page/饑荒.md "wikilink")，成為一名[海盜](../Page/海盜.md "wikilink")。由於當時閩浙海域的鳳尾與水澳兩幫海盜，以及[安南的](../Page/越南.md "wikilink")[艇匪等相繼瓦解消亡](../Page/艇匪.md "wikilink")，蔡牽因此靠著重整這批殘餘勢力而迅速崛起。\[3\]至[嘉慶二年](../Page/嘉慶.md "wikilink")（1797年），蔡牽已成為統領百艘船艦和兩萬餘人的海上霸主，眾人尊稱他為「大出海」。\[4\]他所領導的艦隊馳騁於[中國](../Page/中國.md "wikilink")[閩浙及](../Page/閩浙.md "wikilink")[兩廣的沿海洋面](../Page/兩廣.md "wikilink")，劫船越貨、限制航道、收取「出洋稅」，與[香港的](../Page/香港.md "wikilink")[張保仔相互支援呼應](../Page/張保仔.md "wikilink")。

### 攻台

[嘉庆七年](../Page/嘉庆.md "wikilink")（1802年），率船队攻[厦门海口的大](../Page/厦门.md "wikilink")、小担山登岸夺炮。[嘉庆九年](../Page/嘉庆.md "wikilink")（1804年）蔡牵的艦隊抵達[臺南](../Page/臺南.md "wikilink")[鹿耳门](../Page/鹿耳门.md "wikilink")，在浮鹰洋-{}-面破[温州镇水师](../Page/温州.md "wikilink")。

[浙江提督](../Page/浙江.md "wikilink")[李長庚率兵镇压](../Page/李長庚.md "wikilink")\[5\]，在[定海洋](../Page/定海.md "wikilink")-{}-面击败蔡牵。次年称「镇海王」，船队驶入凤山（今[高雄市](../Page/高雄市.md "wikilink")[鳳山區](../Page/鳳山區.md "wikilink")），包围[台湾府城](../Page/台湾府城.md "wikilink")。[嘉庆十二年冬十二月](../Page/嘉庆.md "wikilink")（1807年）李長庚与福建[水师提督](../Page/水师提督.md "wikilink")[张见陞在](../Page/张见陞.md "wikilink")[廣東黑水外洋合击蔡牵](../Page/廣東.md "wikilink")，蔡牵在只剩大船三艘的情况下于船尾发炮，狙殺了李長庚。

### 敗亡

[嘉庆十四年](../Page/嘉庆.md "wikilink")（1809年）李長庚部将[王得祿](../Page/王得祿.md "wikilink")、[邱良功分接任](../Page/邱良功.md "wikilink")[福建](../Page/福建.md "wikilink")、[浙江提督](../Page/浙江.md "wikilink")，合兵围攻蔡牵于[浙江](../Page/浙江.md "wikilink")[台州渔山外洋](../Page/台州.md "wikilink")，血戰一晝夜，蔡牽之妻也是海盜，能督造炮彈。蔡牽彈丸射盡竟以[銀元為彈](../Page/銀元.md "wikilink")。\[6\]次日寡不敌众，开炮自炸座船，与妻小及部众250余人沉海而死（一說蔡牽自摟[金](../Page/金.md "wikilink")[錨投海自盡](../Page/錨.md "wikilink")），餘眾四千人降服。

## 逸事

  - 據說，[馬祖天后宮的許多建築](../Page/馬祖天后宮.md "wikilink")，為蔡牽居[馬祖時](../Page/馬祖.md "wikilink")，捐資蓋建，以尊奉曾葬於馬祖的[海神](../Page/海神.md "wikilink")[媽祖](../Page/媽祖.md "wikilink")，蔡牽並把[黃金](../Page/黃金.md "wikilink")、[白銀等寶藏](../Page/白銀.md "wikilink")，藏於[馬祖天后宮附近的海底](../Page/馬祖天后宮.md "wikilink")，至今尚未尋獲，幸運者可在每年[中秋節自海上特殊角度](../Page/中秋節.md "wikilink")，在明月照耀之下，會發現水下有特別明亮的珠光反射。

<!-- end list -->

  - 據說某日蔡牽停泊於[馬祖](../Page/馬祖.md "wikilink")[東引島北澳港內](../Page/東引島.md "wikilink")，發現北澳嶺上有農夫牽黃牛正將青綠未出穗的麥子犁掉，蔡牽見後十分訝異：麥子尚未出穗又怎麼要犁掉？遂派手下前往查看，眾人到後均不見農夫、黃牛、麥田，就在納悶之際，發現遠處有數十艘[清廷](../Page/清廷.md "wikilink")[官船駛來](../Page/官船.md "wikilink")，連忙回船通知蔡牽，蔡牽便下令船隊揚起船帆離開，逃過一劫。蔡牽為感念神靈化現農夫警示，便封之為「犁麥大王」，將其供奉於[東引天后宮](../Page/東引天后宮.md "wikilink")[媽祖旁](../Page/媽祖.md "wikilink")。\[7\]

<!-- end list -->

  - 據說臺北富豪張秉鵬為商行[夥計時](../Page/夥計.md "wikilink")，一次帶領船隊駛入[滬尾港口](../Page/淡水區.md "wikilink")，卻於入港時遭[蔡牽襲擊](../Page/蔡牽.md "wikilink")。這天，恰巧是[大龍峒](../Page/大龍峒.md "wikilink")[大道公廟落成](../Page/大龍峒保安宮.md "wikilink")、[大道公神像](../Page/大道公.md "wikilink")[開光點眼之日](../Page/開光點眼.md "wikilink")，自祖廟[白礁慈濟宮](../Page/白礁慈濟宮.md "wikilink")[進香歸返的神像](../Page/進香.md "wikilink")，從滬尾準備入港，居民紛紛前往[禮佛](../Page/禮佛.md "wikilink")；沿途[燒香](../Page/燒香.md "wikilink")、放[鞭炮](../Page/鞭炮.md "wikilink")，蔡牽誤以為是在歡迎自己，大喜，遂下令所有[海賊不准殺害住民](../Page/海賊.md "wikilink")，張秉鵬槍下得生，但被俘虜，跪在蔡牽船中，遇到蔡牽的妻子，蔡妻與之閒聊，發現自己是張秉鵬的[同鄉](../Page/同鄉.md "wikilink")，與張相談甚歡。蔡牽的妻子於是要求蔡牽放過張秉鵬，蔡牽授予張秉鵬「免劫」令旗，以[海盜船將張秉鵬與他的貨物送回](../Page/海盜船.md "wikilink")。而張秉鵬也因此而發跡，而後創立了「[張得寶商號](../Page/張得寶.md "wikilink")」。

<!-- end list -->

  - 據說，[黃旭齋一次出海](../Page/黃旭齋.md "wikilink")，被蔡牽部下的海賊俘虜，海賊們正在想要[槍殺他還是把他丟入海中](../Page/槍殺.md "wikilink")，恰好遇到蔡牽親自來視導，蔡牽當即命令，把黃旭齋放了，並宴請了黃旭齋之後，送給黃旭齋一個令牌，上書「[天后聖母之寶](../Page/媽祖.md "wikilink")，沐恩信士蔡牽敬題」，並交代黃旭齋，這個[靈符可以保佑行船平安](../Page/符咒.md "wikilink")（免被蔡牽打劫），以船將黃旭齋與他的貨物送回。黃被放走之後，蔡牽才告訴其他小嘍囉，原來昔年蔡牽早年逃亡時，到一個麵館喫麵，身上的[銀兩卻因故失落](../Page/銀兩.md "wikilink")，麵攤老闆要把蔡牽捉到[官府](../Page/官府.md "wikilink")，吵鬧間，蔡牽本來要拔出暗藏的[匕首對抗](../Page/匕首.md "wikilink")，黃旭齋經過，就直接付錢，救了蔡牽，蔡牽臨走前曾經許諾，「黃頭家，小姓蔡，日後必定相報」。

<!-- end list -->

  - 傳說[高雄港](../Page/高雄港.md "wikilink")[漁人碼頭外的兩座山丘](../Page/漁人碼頭.md "wikilink")([中山大學旁出港口](../Page/中山大學.md "wikilink")，其中一座上建[旗後燈塔](../Page/旗後燈塔.md "wikilink"))，蔡牽為逃避[臺灣府兵卒](../Page/臺灣府.md "wikilink")，後有官兵追擊甚緊，而前有山丘擋路，情急之下，向上天祈禱脫身，它用大刀插入地面，山丘被斬為兩半，成為通路，遂從此逃逸，官兵追之不及，只能望船興嘆，而這兩座山丘被稱為**夾岸石**，為[漁人碼頭四大景觀之一](../Page/漁人碼頭.md "wikilink")。

<!-- end list -->

  - 傳說蔡牽在海上勢力強大，並且能自製[砲彈](../Page/砲彈.md "wikilink")。後來受到清朝政府派兵圍剿，由於戰況激烈，彈盡援絕，蔡牽竟然用[銀元做為彈丸](../Page/銀元.md "wikilink")，敗局已定時，最後用火砲炸沈了自己的座船，船上數百人一起陣亡。故當年而後，[閩南語有俗諺](../Page/閩南語.md "wikilink")「蔡牽提錢考水披」（，「考水披」指「擲入水中」）\[8\]，是形容一個人揮霍無度。「蔡牽造砲炸自己」（）：引申為自作自受之意。

<!-- end list -->

  - 傳說蔡牽[伏法後](../Page/伏法.md "wikilink")，清朝政府打算將他[滅族](../Page/滅族.md "wikilink")，蔡牽之[林姓手下將蔡牽唯一後人](../Page/林姓.md "wikilink")，帶至現[臺灣](../Page/臺灣.md "wikilink")[彰化縣](../Page/彰化縣.md "wikilink")[竹塘鄉附近定居](../Page/竹塘鄉_\(臺灣\).md "wikilink")，其子孫為逃避滿清剿滅皆改姓林，但是祖先[牌位上姓氏依然寫為](../Page/牌位.md "wikilink")「蔡姓」。

## 註解

<div class="references-small">

<references />

</div>

## 參考文獻

  - 《福建通志》
  - 《連江縣志》
  - 《[台灣通史](../Page/台灣通史.md "wikilink")》

[C](../Category/清朝海盜.md "wikilink")
[C](../Category/台灣清治時期人物.md "wikilink")
[C](../Category/同安人.md "wikilink")
[Category:蔡姓](../Category/蔡姓.md "wikilink")

1.  [蔡牽 - 臺灣記憶 Taiwan
    Memory--國家圖書館](http://memory.ncl.edu.tw/tm_cgi/hypage.cgi?HYPAGE=toolbox_figure_detail.hpg&project_id=&dtd_id=&subject_name=&subject_url=&xml_id=0000009397&who=%E8%94%A1%E7%89%BD)

2.  [鹿港意樓的再生 - 國史館臺灣文獻館](http://w3.th.gov.tw/files/643/06439.pdf)

3.  [吳建昇 嘉慶十年(1805)海盜蔡牽攻台行動之研究 -
    崑山科技大學](http://ir.lib.ksu.edu.tw/bitstream/987654321/12218/2/12.p143~p166_D13%E8%94%A1%E7%89%BD%E6%94%BB%E5%8F%B0%E8%A1%8C%E5%8B%95.pdf)

4.
5.  據說蔡自言：「不怕千萬兵，只怕李長庚」

6.  張師誠《殲除海洋積年首逆蔡牽摺》該匪因不得鉛丸接濟，以番銀作為砲彈點放。

7.  [這個神明很少見／犁麥大王後台是航海王蔡牽？](https://www.nownews.com/news/20180808/2798496)

8.  黃申如.《淺論閩臺民間故事與俗諺》.國立清華大學歷史所