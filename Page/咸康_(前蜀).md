**咸康**（925年正月-十一月）是[前蜀后主](../Page/前蜀.md "wikilink")[王衍的](../Page/王衍_\(前蜀\).md "wikilink")[年号](../Page/年号.md "wikilink")，共计1年。

## 紀年

| 咸康                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 925年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 其他政权使用的[咸康年号](../Page/咸康.md "wikilink")
  - 同期存在的其他政权年号
      - [同光](../Page/同光.md "wikilink")（923年四月至926年四月）：[後唐](../Page/後唐.md "wikilink")—[李存勗之年號](../Page/李存勗.md "wikilink")
      - [順義](../Page/順義_\(楊溥\).md "wikilink")（921年二月至927年十月）：[吳](../Page/吳_\(十國\).md "wikilink")—[楊溥之年號](../Page/楊溥_\(吳\).md "wikilink")
      - [宝大](../Page/宝大.md "wikilink")（924年至925年）：[吳越](../Page/吳越.md "wikilink")—[錢鏐之年號](../Page/錢鏐.md "wikilink")
      - [乾亨](../Page/乾亨_\(劉龑\).md "wikilink")（917年七月至925年十一月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [天贊](../Page/天贊.md "wikilink")（922年二月至926年二月）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機之年號](../Page/耶律阿保機.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年—933年）：[高麗](../Page/高麗.md "wikilink")—[高麗太祖王建之年號](../Page/高麗太祖.md "wikilink")
      - [延長](../Page/延長_\(醍醐天皇\).md "wikilink")（923年閏四月十一日至931年四月二十六日）：日本醍醐天皇、[朱雀天皇年号](../Page/朱雀天皇.md "wikilink")

[Category:前蜀年号](../Category/前蜀年号.md "wikilink")
[Category:920年代中国政治](../Category/920年代中国政治.md "wikilink")
[Category:925年](../Category/925年.md "wikilink")