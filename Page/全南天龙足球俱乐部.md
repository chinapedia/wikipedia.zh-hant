**全南天龍足球俱樂部**（；[英语](../Page/英语.md "wikilink")：**Jeonnam
Dragons**）是韩国职业足球俱乐部，位于[光阳市](../Page/光阳市.md "wikilink")。俱乐部创建于1994年，球队1995赛季和[全羅道竞争对手](../Page/全羅道.md "wikilink")[全北现代同时加入](../Page/全北现代.md "wikilink")[K联赛](../Page/K联赛.md "wikilink")。

全南天龙战绩最佳的赛季是1997赛季，赛季中球队取得了联赛亚军并且将[韩国足总杯冠军收入囊中](../Page/韩国足总杯.md "wikilink")，球队是韩国联赛中的一支劲旅。

## 俱乐部荣誉

### 国内赛事

  - [经典K联赛](../Page/经典K联赛.md "wikilink")
      - 亚军: 1次（）
  - [韩国足总杯](../Page/韩国足总杯.md "wikilink")
      - 冠军：3次（, ，）
      - 亚军：1次（）
  - [韩国联赛杯](../Page/韩国联赛杯.md "wikilink")
      - 亚军：2次（，）

### 国际赛事

  - [亚洲优胜者杯](../Page/亚洲优胜者杯.md "wikilink")
      - 亚军：1次（[1998-1999年](../Page/1998–99年亞洲盃賽冠軍盃.md "wikilink")）

## 赞助商

**球衣供应商**

  - 1995-1996: Ludis
  - 1997: [茵宝](../Page/茵宝.md "wikilink")
  - 1998: [阿迪达斯](../Page/阿迪达斯.md "wikilink")
  - 1999: [锐步](../Page/锐步.md "wikilink")
  - 2000: [茵宝](../Page/茵宝.md "wikilink")
  - 2001: [阿迪达斯](../Page/阿迪达斯.md "wikilink")
  - 2002-2003: [茵宝](../Page/茵宝.md "wikilink")
  - 2004-2005: [Hummel](../Page/Hummel.md "wikilink")
  - 2007-2009: [Astore Korea](../Page/Astore_Korea.md "wikilink")
  - 2010-2011: [雅科](../Page/雅科.md "wikilink")
  - 2012-现在:

[Category:韩国足球俱乐部](../Category/韩国足球俱乐部.md "wikilink")
[Category:1994年建立的足球俱樂部](../Category/1994年建立的足球俱樂部.md "wikilink")