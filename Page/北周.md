**北周**（557年—581年）是[中國歷史上](../Page/中國歷史.md "wikilink")[南北朝的北朝之一](../Page/南北朝.md "wikilink")。又称**後周**（[宋朝以后鲜用](../Page/宋朝.md "wikilink")），由**宇文**氏建立，定都[長安](../Page/長安.md "wikilink")，北周自建國後，統治實權一直在[霸府](../Page/霸府.md "wikilink")[宇文護身上](../Page/宇文護.md "wikilink")，皇帝無力與之抗阻，為了擺脫宇文護的束縛，經過一連串的計畫與鬥爭，[北周武帝終於殺死了宇文護](../Page/北周武帝.md "wikilink")，掌握大權，並以德施政，人民安樂，在位時更成功滅北齊，統一北朝。但他死後三年，北周便被[楊堅的](../Page/楊堅.md "wikilink")[隋所取代](../Page/隋.md "wikilink")，後由[隋滅陳](../Page/隋滅陳.md "wikilink")，[統一中國](../Page/統一中國.md "wikilink")。

## 歷史

北周由宇文泰奠定根基。[北魏在](../Page/北魏.md "wikilink")[六鎮之亂時](../Page/六鎮之亂.md "wikilink")，宇文泰投靠權臣[爾朱榮](../Page/爾朱榮.md "wikilink")，隨其入[關中討伐叛逆](../Page/關中.md "wikilink")，後來投于以關中隴西為根據地的大將[賀拔岳的麾下](../Page/賀拔岳.md "wikilink")，並漸漸受重用。

控制[洛陽的另一權臣](../Page/洛陽.md "wikilink")[高歡認為賀拔岳有不臣之心](../Page/高歡.md "wikilink")，故使隴西[秦州軍人刺殺賀拔岳](../Page/秦州.md "wikilink")。賀拔岳所屬將領在賀遇刺後，擁立宇文泰為統帥。宇文泰只是表面上服從高歡，其實控制關隴。

[北魏孝武帝在討伐高歡失敗後](../Page/北魏孝武帝.md "wikilink")，逃奔關中。宇文泰雖收容了他。但不久就將孝武帝殺害，改擁立[西魏文帝建立](../Page/西魏文帝.md "wikilink")[西魏](../Page/西魏.md "wikilink")（535年）。而東方的高歡在孝武帝逃入關中後擁立[東魏孝靜帝](../Page/東魏孝靜帝.md "wikilink")，把朝廷遷到[河北](../Page/河北.md "wikilink")[鄴城](../Page/鄴城.md "wikilink")，建立[東魏](../Page/東魏.md "wikilink")（534年）。

[西魏建立後](../Page/西魏.md "wikilink")，[宇文泰成為大](../Page/宇文泰.md "wikilink")[丞相](../Page/丞相.md "wikilink")。宇文泰在三次戰役中大敗東魏，奠定宇文氏在關中的基礎。宇文泰任用[蘇綽等人改革](../Page/蘇綽.md "wikilink")，使西魏進一步強盛。進而攻入[南梁的](../Page/南梁.md "wikilink")[成都](../Page/成都.md "wikilink")，奪取[西川地盤](../Page/益州.md "wikilink")。

[西魏恭帝三年](../Page/西魏恭帝.md "wikilink")（556年），宇文泰病死，由嫡长子[宇文觉承袭为安定郡公](../Page/宇文觉.md "wikilink")、[太师](../Page/太师.md "wikilink")、[大冢宰](../Page/大冢宰.md "wikilink")。次年，宇文泰之侄[宇文護迫西魏恭帝](../Page/宇文護.md "wikilink")[禪讓](../Page/禪讓.md "wikilink")，由[宇文覺即位](../Page/宇文覺.md "wikilink")[天王](../Page/天王_\(君主\).md "wikilink")，建立北周，建都[長安](../Page/長安.md "wikilink")（即今[陝西](../Page/陝西.md "wikilink")[西安](../Page/西安.md "wikilink")）。

[宇文覺不滿](../Page/宇文覺.md "wikilink")[宇文護專權](../Page/宇文護.md "wikilink")，企圖剷除宇文護，但反被其所殺。宇文護擁立其庶兄宇文毓，是為[北周明帝](../Page/北周明帝.md "wikilink")。幾年後，明帝被殺，又擁立其兄弟宇文邕為[北周武帝](../Page/北周武帝.md "wikilink")。宇文護執掌政權十五年，成為北周實際上的主宰。他承繼宇文泰、[蘇綽的政策](../Page/蘇綽.md "wikilink")，消滅威脅政權的[軍閥](../Page/軍閥.md "wikilink")，使北周政權更鞏固。北周武帝年間，宇文護的兒子亂政害民，宇文護的威望大降。天和七年（572年）三月，北周武帝乘機[刺殺了宇文護](../Page/刺殺.md "wikilink")，重奪政權。

北周武帝執政後，積極推廣漢化並勵精圖治。575年發兵征[北齊](../Page/北齊.md "wikilink")，577年，北周滅[北齊](../Page/北齊.md "wikilink")，統一[華北](../Page/華北.md "wikilink")。北周統一華北後國力一度興盛，但北周武帝英年早逝，其繼位者北周宣帝宇文贇奢侈浮華，沉緬酒色，政治腐敗。周宣帝生前即传位年幼的儿子[北周靜帝](../Page/北周靜帝.md "wikilink")[宇文闡](../Page/宇文闡.md "wikilink")。580年6月8日宣帝病死，外戚[楊堅以大丞相身份輔政](../Page/楊堅.md "wikilink")，乘機將北周重臣外遣，進而把持朝政。相州總管[尉遲迥](../Page/尉遲迥.md "wikilink")、鄖州總管[司馬消難與益州總管](../Page/司馬消難.md "wikilink")[王謙等人不滿楊堅專權](../Page/王谦_\(北周\).md "wikilink")，聯合叛變反抗楊堅，爆發[尉遲迥之亂](../Page/尉遲迥之亂.md "wikilink")，但被楊堅所派的[韋孝寬](../Page/韋孝寬.md "wikilink")、[王誼與](../Page/王誼.md "wikilink")[高熲等人平定](../Page/高熲.md "wikilink")。期间杨坚亦诛杀北周明帝长子太师雍州牧毕王[宇文贤及尚在人世的宇文泰五子](../Page/宇文贤.md "wikilink")。581年3月4日，北周靜帝禪讓帝位於[楊堅](../Page/楊堅.md "wikilink")，[楊堅受禅称帝](../Page/楊堅.md "wikilink")，改國號[隋](../Page/隋朝.md "wikilink")，北周享國二十四年而亡。[杨坚建国不久](../Page/杨坚.md "wikilink")，就将北周近支宗室诛杀殆尽，将[宇文洛封为介国公作为北周奉祀](../Page/宇文洛.md "wikilink")。

## 文化

此時期佛教藝術創作，大多數位於長安。印度[笈多王朝雕像為許多大型佛像的原型](../Page/笈多王朝.md "wikilink")。在敦煌千佛洞則有一些北周風格的壁畫，在這些壁畫中，山水畫固然重要，不過仍遜於人物畫。\[1\]

## 人口

北周人口盛时约1250万。

## 君主

## 大冢宰

[大冢宰为北周六官之首](../Page/大冢宰.md "wikilink")，相当于丞相。

  - 北周立國至572年，[宇文護掌權](../Page/宇文護.md "wikilink")。
  - 572年至578年，[北周武帝親政](../Page/北周武帝.md "wikilink")。
  - 578年至580年，[北周宣帝親政](../Page/北周宣帝.md "wikilink")。
  - 580年至北周滅亡，外戚[楊堅掌權](../Page/楊堅.md "wikilink")。

## 藩王

## 参考文献

## 外部链接

  - 《[中国大百科全书](../Page/中国大百科全书.md "wikilink")》：[北周](http://www.white-collar.net/02-lib/01-zg/03-guoxue/%C6%E4%CB%FB%C0%FA%CA%B7%CA%E9%BC%AE/%C0%FA%CA%B7%B9%A4%BE%DF%C0%E0/%D6%D0%B9%FA%B4%F3%B0%D9%BF%C6%C8%AB%CA%E9%D6%D0%B9%FA%C0%FA%CA%B7/Resource/Book/Edu/JXCKS/TS011096/0095_ts011096.htm)

{{-}}

[北周](../Category/北周.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")
[Category:557年建立](../Category/557年建立.md "wikilink")

1.  [藝術與建築索引典—北周](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300018407)
    於2011 年4 月1 日查閱