[Taipei_101_from_afar.jpg](https://zh.wikipedia.org/wiki/File:Taipei_101_from_afar.jpg "fig:Taipei_101_from_afar.jpg")
[Physiognomy_of_forest_in_Xiangshan,_Taipei_20060628.jpg](https://zh.wikipedia.org/wiki/File:Physiognomy_of_forest_in_Xiangshan,_Taipei_20060628.jpg "fig:Physiognomy_of_forest_in_Xiangshan,_Taipei_20060628.jpg")
**象山**位於[台北市](../Page/台北市.md "wikilink")[信義區](../Page/信義區_\(臺北市\).md "wikilink")，位於[台北盆地東緣的](../Page/台北盆地.md "wikilink")[南港山系西側](../Page/南港山_\(台北市\).md "wikilink")，因山形似[象而得名](../Page/象.md "wikilink")，與鄰近的[獅山](../Page/獅山_\(台北市\).md "wikilink")、[虎山](../Page/虎山_\(台北市\).md "wikilink")、[豹山](../Page/豹山_\(台北市\).md "wikilink")、合稱為[四獸山](../Page/四獸山.md "wikilink")。山頂海拔183公尺，高度雖然不高，但因臨近台北市區，交通方便，視野良好，成為台北市民眾重要的休閒場所。

## 地質及質地

目前象山所露出的地層，是由2,100萬年至1,950萬年前的[海相沈積](../Page/海相沈積.md "wikilink")[大寮層與](../Page/大寮層.md "wikilink")1,950萬年前至1,800萬年前的[濱海相沈積](../Page/濱海相沈積.md "wikilink")[石底層所組成](../Page/石底層.md "wikilink")，皆是由[華南古陸塊沈積而來的地層](../Page/華南古陸塊.md "wikilink")，以石底層地層的分佈較廣。後來[造山運動將古地層推擠出地面](../Page/造山運動.md "wikilink")，因山的西北側巨大[節理崩落](../Page/節理.md "wikilink")，裸露出石底層底部壯觀的厚層[砂岩](../Page/砂岩.md "wikilink")，形成近垂直坡度的陡峭岩壁，極具觀賞價值。

象山山稜地區的植物相多為中海拔、岩石地的[向陽性植物](../Page/向陽性植物.md "wikilink")，常見的植物有[黃杞](../Page/黃杞.md "wikilink")、[青剛櫟](../Page/青剛櫟.md "wikilink")、[大明橘](../Page/大明橘.md "wikilink")…等。西側山腰則是一遮陰多濕的區域，以[筆筒樹](../Page/筆筒樹.md "wikilink")、[菲律賓榕較常見](../Page/菲律賓榕.md "wikilink")；此外，以[金狗毛厥西側步道陡壁上的主要植物](../Page/金狗毛厥.md "wikilink")。山谷地區則有[山黃麻及](../Page/山黃麻.md "wikilink")[海南實蕨等植物](../Page/海南實蕨.md "wikilink")。

## 登山及遊憩

象山主要的登山口共有三個，包括自北側的[永春高中](../Page/永春高中.md "wikilink")，南側的松仁路／莊敬路口，西側的[臺北市立聯合醫院松德院區上爬都可到達](../Page/臺北市立聯合醫院松德院區.md "wikilink")，均約半小時即可登頂，並可進一步向東往[南港山稜線上的南港山主峰](../Page/南港山.md "wikilink")、[九五峰](../Page/九五峰.md "wikilink")、[拇指山以及](../Page/拇指山.md "wikilink")[中華科技大學等地](../Page/中華科技大學.md "wikilink")。象山自然步道沿著象山西側山腰及山稜，成一完整的循環路線，涵蓋沿線多樣之自然生態景觀。在象山山頂附近並設有涼亭及廁所。此外，由於象山臨近台北市[東區](../Page/東區_\(台北市\).md "wikilink")，因此成為觀賞拍攝[台北101大樓的良好景點](../Page/台北101.md "wikilink")。\[1\]

## 交通

  - 由莊敬路登山
      - 捷運：[板南線](../Page/板南線.md "wikilink")[市政府站](../Page/市政府站.md "wikilink")，由2號出口轉搭公車「藍5」至[吳興國小站下車](../Page/吳興國小.md "wikilink")。
      - 公車: 1（[欣欣客運](../Page/欣欣客運.md "wikilink")）
        、22、33、藍5([大都會客運](../Page/大都會客運.md "wikilink"))、32、37（[東南客運](../Page/東南客運.md "wikilink")）、226([首都客運](../Page/首都客運.md "wikilink"))、266、288([大南汽車](../Page/大南汽車.md "wikilink"))在吳興國小站下車。

<!-- end list -->

  - 由松山路方向登山
      - 20、46、277（[大都會客運](../Page/大都會客運.md "wikilink")）、37、555、612（[東南客運](../Page/東南客運.md "wikilink")）在松德站下車。
      - 20（[大都會客運](../Page/大都會客運.md "wikilink")）、33、299[永春高中站下車](../Page/臺北市立永春高級中學.md "wikilink")。
      - 信義幹線（[大有巴士](../Page/大有巴士.md "wikilink")）、263在[松山商職站下車](../Page/臺北市立松山高級商業家事職業學校.md "wikilink")。

<!-- end list -->

  - 由信義路五段登山（150巷22弄）
      - 捷運：[信義線](../Page/信義線.md "wikilink")[象山站](../Page/象山站.md "wikilink")，
        由2號出口直走進入信義路5段150巷。
      - 33、266（[大南汽車](../Page/大南汽車.md "wikilink")）、32（含區間）（[東南客運](../Page/東南客運.md "wikilink")）在[信義國中](../Page/信義國中.md "wikilink")(松仁)站下車。

## 參見

  - [台北捷運](../Page/台北捷運.md "wikilink")[象山站](../Page/象山站.md "wikilink")

## 參考資料

## 外部連結

  - [休閒遊憩主題網 象山親山步道
    臺北市政府工務局大地工程處](http://gisweb.taipei.gov.tw/release/?p=2-3-2)
  - [象山步道 - 自然步道協會](https://www.naturetrail.org.tw/trail_view.php?id=3)
  - [南港山系_象山親山步道
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/185)

[Category:信義區 (臺北市)](../Category/信義區_\(臺北市\).md "wikilink")
[Category:台北市山峰](../Category/台北市山峰.md "wikilink")

1.  [6個象山最佳「看煙火地點」 還有飲水機不怕渴](https://udn.com/news/story/7266/2890336),
    [聯合報](../Page/聯合報.md "wikilink"), 2017/12/22