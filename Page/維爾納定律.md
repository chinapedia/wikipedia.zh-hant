**維爾納定律**（Verner's
law），由[卡爾·維爾納於](../Page/卡爾·維爾納.md "wikilink")1875年提出，該定律描述了發生在[原始日耳曼語](../Page/原始日耳曼語.md "wikilink")（PGmc）中的一次歷史[音變](../Page/音變學.md "wikilink")(sound
change)，指出了，出現在非重讀[音節的末尾的清](../Page/音節.md "wikilink")[擦音](../Page/擦音.md "wikilink")\*f,
\*þ, \*s和\*x，經過[濁化轉變成了](../Page/濁輔音.md "wikilink")\*b, \*d, \*z和\*g。

## 疑團

發現了[格林定律以後](../Page/格林定律.md "wikilink")，在運用過程中出現了一系列的不規則現象。[原始印歐語](../Page/原始印歐語.md "wikilink")（PIE）的輕[塞音](../Page/塞音.md "wikilink")\*p,
\*t, \*k按照格林定律本應該在原始日爾曼語中分別轉變成\*f,
\*þ（齒間擦音）和\*x（舌根擦音），通常情況下的確是這樣的。但是，在一大批的同源詞中，以其在[拉丁語](../Page/拉丁語.md "wikilink")、[古希臘語](../Page/古希臘語.md "wikilink")、[梵語](../Page/梵語.md "wikilink")、和[波羅的語中的形態可斷定其PIE](../Page/波羅的語.md "wikilink")[音素是](../Page/音素.md "wikilink")\*p,
\*t, \*k，在日爾曼語中卻表現爲濁塞音\*b, \*đ, \*g。

起先，少許的「異常」並未引起過多的關注，學者們更熱心於發現更多「規則」的實例。然而，終究還是有越來越多的語言學家，不再滿足於這些「聽話」子集，而是決心要構建出普遍適用的「無例外」的音變規則體系。

一個由PIE的\*t到PGmc的\*d的典型例子是\*ph₂tēr（「父」，表示[喉音](../Page/喉音.md "wikilink")，e上的一橫是長音記號）與\*fađēr的對映（而不是預期的\*faþēr
)。有趣的是，同爲親屬稱謂且結構也相似的PIE：（「兄弟」）所對映的PGmc：brōþēr則完全符合格林定律。更有意思的是，我們經常會發現，與PIE：\*t對映的\*þ和\*đ還可以分別出現在同一詞根的不同形態中，如\*werþ-（「轉」）字的[單數](../Page/單數.md "wikilink")[第三人稱](../Page/第三人稱.md "wikilink")[過去時爲](../Page/過去時.md "wikilink")\*warþ，而[複數型和](../Page/複數.md "wikilink")[過去分詞卻作](../Page/過去分詞.md "wikilink")\*wurđ-（加相應的屈折詞尾）。

## 解答

卡爾·維爾納第一個開始去探求，究竟是甚麼因決定了這兩種結果的分配。通過觀察，他發現，這些發生了不按「規則」的濁化的清擦音不會出現在詞首，而且前臨的元音在PIE中都是非重音。在現代日爾曼語中重讀音節多固定在詞首，但是原始的PIE重音位置很多都在希臘語和早期梵語中保留了下來。和之間最要緊的區別就在於，前者的重音在第二音節，後者卻是位於詞首（cf.梵語的pitā́和bhrā́tā）。

類似的，\**werþ-*和\**wurđ-*的差異也因重音位詞幹和屈折詞尾（首音節輕讀）的不同而得以解釋。還有其他一些符合維爾納定律的例子，比如：現代[德語的ziehen](../Page/德語.md "wikilink")
| (ge)zogen（「拉」）\< PGmc. \*tiux- | \*tug- \< PIE  | （「引」）.

維爾納定律還有一個伴隨產物：即在此規則下，PIE中的s在PGmc的某些詞中轉變成了z。繼而，在[斯堪的納維亞語和](../Page/斯堪的納維亞語.md "wikilink")[西部日爾曼語支的德語](../Page/西部日爾曼語支.md "wikilink")、[荷蘭語](../Page/荷蘭語.md "wikilink")、[英語和](../Page/英語.md "wikilink")[弗里斯蘭語中](../Page/弗里斯蘭語.md "wikilink")，z又轉變成了r，維爾納定律解釋了某些屈折變化中/s/和/r/的交替。比如，[古英語動詞ceosan](../Page/古英語.md "wikilink")（「選」，現代英語作「choose」），複數過去時爲curon過去分詞爲
(ge)coren \< \**kius-* | \**kuz-* \<  |
（「嘗，試」）。假如聲母未發生轉變的話，coren的詞形可能會一直保存在英語中（cf.
kiesen :
gekoren（choose，古語））。但是維爾納的/r/在「were」（現代英語[系動詞](../Page/系動詞.md "wikilink")「是」的複數過去時）中就沒有被磨滅——were
\< PGmc. \*wēz-
與was（「是」的單數過去時）對立。類似的lose(英語lost「丟失」的弱化形）也有一個forlorn與之相配（cf.荷蘭語verliezen
: verloren；在德語的對映詞中，/s/已經磨滅，lose對映爲verlieren，forlorn對映verloren）。

## 維爾納定律的時限

日爾曼語言發生了重移至詞首的轉變之後維爾納定律就不再適用了。因為古語的重音位置纔是導致此類濁化的必要條件，從清輔音向相應濁化變體轉變所依賴的環境被重音的移動取消了。然而最近有觀點認為維爾納定律在「後」[格林定律時代仍然有效](../Page/格林定律.md "wikilink")。专家指出，在一定条件下，即使转化方向相反最终结果也有可能是一样的。

## 意義

卡爾·維爾納於1876年在[歷史語言探索雜志上發表了題爲](../Page/歷史語言探索.md "wikilink")*Eine
Ausnahme der ersten
Lautverschiebung*（「一個音變特例」）論文中闡述了他的發現。但是早在一年前在他寫給[Vilhelm
Thomsen](../Page/Vilhelm_Thomsen.md "wikilink")（維爾納的朋友和導師）的一封私信中他就已經簡要地講述了這一理論。

維爾納的發現在年青一代比較語言學家——所謂[新語法學家](../Page/新語法學家.md "wikilink")——中間激起了極大的熱情。因為它爲新語法學家們所追求的無例外的音變規則（"die
Ausnahmslosigkeit der Lautgesetze"）提供了有力的理論依據。

## 延伸閱讀

  -
  - \[Language and Origin of the Germanic Peoples — Compendium of the
    [Proto-Germanic Language prior to First Sound
    Shift](../Page/Germanic_Parent_Language.md "wikilink"){{)\!}}, 244
    p., , London/Hamburg 2009

  - [Kortlandt, Frederik](../Page/Frederik_Kortlandt.md "wikilink"),
    *Proto-Germanic obstruents.* — in: ** 27, p. 3–10 (1988).

  - Koivulehto, Jorma /  118, p. 163–182 (esp. 170–174) (1996)

  - Noske, Roland, *The Grimm–Verner Chain Shift and Contrast
    Preservation Theory.* — in: Botma, Bert & Roland Noske (eds.),
    Phonological Explorations. Empirical, Theoretical and Diachronic
    Issues ( 548), p. 63–86. Berlin: De Gruyter, 2012.

  - — in:  106, p. 1–45 (1984)

## 參見

  - [格林定律](../Page/格林定律.md "wikilink")
  - [高地德語子音推移](../Page/高地德語子音推移.md "wikilink")

## 外部連結

  - [A Reader in Nineteenth Century Historical IE Linguistics
    Ch.11](https://web.archive.org/web/20060830031338/http://www.utexas.edu/cola/centers/lrc/books/read11.html)
    *"An exception to the first sound shift"* by Winfred P. Lehmann -
    From the Linguistics Research Center at the University of Texas at
    Austin
  - [The original
    article](http://www.archive.org/stream/zeitschriftfrve08unkngoog#page/n149/mode/1up)
    (German)

[Category:印歐語系語言學](../Category/印歐語系語言學.md "wikilink")
[Category:語音定律](../Category/語音定律.md "wikilink")