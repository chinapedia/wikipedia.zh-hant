**點心衛視**是一個地區性的[粵語](../Page/粵語.md "wikilink")[衛星電視頻道](../Page/衛星電視.md "wikilink")，於2007年3月30日首播。以資訊娛樂電視節目為主，目標是大[珠三角地區觀眾和](../Page/珠三角.md "wikilink")[海外華人](../Page/海外華人.md "wikilink")，創辦人為香港[傳媒人](../Page/傳媒.md "wikilink")[司徒傑以及](../Page/司徒傑.md "wikilink")[香港演藝學院](../Page/香港演藝學院.md "wikilink")[校董會成員](../Page/校董會.md "wikilink")[龐俊怡](../Page/龐俊怡.md "wikilink")。

## 收看頻道

### 香港

現在點心衛視在香港[now寬頻電視第](../Page/now寬頻電視.md "wikilink")539頻道播放。

### 澳門

現在點心衛視在[澳門有線電視第](../Page/澳門有線電視.md "wikilink")16頻道播放。

## 旗艦常規節目

（按播映時間排列）

  - 點心名人榜　週一至週五 20:00
  - 解碼珠三角　週一至週五 20:30
  - 流行點心　　週一至週五 21:30
  - 超級消費王　週六 20:00
  - 心中有數　　週日 20:00

## 外部連結

  - [點心電視](http://www.dim-sum-tv.com/)
  - [點心網](http://www.dimsumtv.net/home/frontpage)
  - [節目表](http://www.dim-sum-tv.com/schedule.php)

[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:2007年成立的電視台或電視頻道](../Category/2007年成立的電視台或電視頻道.md "wikilink")