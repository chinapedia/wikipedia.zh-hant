**喬治·華盛頓大橋**（[英語](../Page/英語.md "wikilink")：），簡稱**華盛頓大橋**（），是一座位於[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")[曼哈頓與](../Page/曼哈頓.md "wikilink")[紐澤西州李堡之間](../Page/紐澤西州.md "wikilink")，跨越[哈德遜河之收費](../Page/哈德遜河.md "wikilink")[懸索橋](../Page/懸索橋.md "wikilink")，及紐約市的重要交通要道。[95號州際公路](../Page/95號州際公路.md "wikilink")、[1號美國國道](../Page/1號美國國道.md "wikilink")、[9號美國國道](../Page/9號美國國道.md "wikilink")、[46號美國國道等重要](../Page/46號美國國道.md "wikilink")[高速公路均行經此處](../Page/高速公路.md "wikilink")。喬治·華盛頓大橋的車流量使之成為世界上最繁忙的橋梁\[1\]。在2004年之間，喬治·華盛頓大橋總車流量為108,404,000輛車，現今大約每日車流量有300,000輛車，與[舊金山-奧克蘭海灣大橋相似](../Page/舊金山-奧克蘭海灣大橋.md "wikilink")。在2006年時，喬治·華盛頓大橋為全美排名第四長的懸索橋。

喬治·華盛頓大橋總共分為兩層，上層有雙向共8線道，下層有雙向共6線道，總共有14線道。除了車行道之外，另外在大橋的兩側還有兩道人行道。橋上的速限為每[小時](../Page/小時.md "wikilink")70[公里](../Page/公里.md "wikilink")
（每小時45[英里](../Page/英里.md "wikilink")）。

## 参考文献

<div class="references-small">

<references />

</div>

## 相关条目

  - [世界悬索桥列表](../Page/世界悬索桥列表.md "wikilink")

[Category:美国悬索桥](../Category/美国悬索桥.md "wikilink")
[G](../Category/紐約市橋樑.md "wikilink")
[Category:1931年完工橋梁](../Category/1931年完工橋梁.md "wikilink")
[Category:桁架橋](../Category/桁架橋.md "wikilink")
[Category:冠以人名的桥梁](../Category/冠以人名的桥梁.md "wikilink")
[Category:美国公路桥](../Category/美国公路桥.md "wikilink")
[Category:桥梁之最](../Category/桥梁之最.md "wikilink")
[Category:曼哈頓交通](../Category/曼哈頓交通.md "wikilink")
[Category:曼哈顿建筑物](../Category/曼哈顿建筑物.md "wikilink")
[Category:新澤西州邊界](../Category/新澤西州邊界.md "wikilink")
[Category:紐約州邊界](../Category/紐約州邊界.md "wikilink")
[Category:新泽西州桥梁](../Category/新泽西州桥梁.md "wikilink")
[Category:哈德逊河桥梁](../Category/哈德逊河桥梁.md "wikilink")

1.  [George Washington Bridge turns 75 years old: Huge flag, cake part
    of
    celebration](http://www.recordonline.com/apps/pbcs.dll/article?AID=/20061024/BIZ/610240312/-1/NEWS03),
    *[Times Herald-Record](../Page/Times_Herald-Record.md "wikilink")*,
    [October 24](../Page/October_24.md "wikilink"), 2006. "The party,
    however, will be small in comparison to the one that the Port
    Authority of New York and New Jersey organized for 5,000 people to
    open the bridge to traffic in 1931. And it won't even be on **what
    is now the world's busiest bridge** for fear of snarling traffic."