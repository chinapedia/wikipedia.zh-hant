**迦密愛禮信中學**（），位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[葵涌華景山道](../Page/葵涌.md "wikilink")4號\[1\]，於1982年由[基督教興學會有限公司](../Page/基督教興學會有限公司.md "wikilink")\[2\]創辦，各級有4班\[3\]。

## 辦學宗旨

明道律己忠主善群

## 校訓

明道律己．忠主善群

## 著名校友

  - [張文慈](../Page/張文慈.md "wikilink")：藝人
  - 伍倩文：美國國家癌症硏究中心
  - 刁美霞：香港公開大學助理教授
  - 林冠峰：英國倫敦大學講師
  - 陳佩：香港大學生物醫學工程副教授
  - [胡兆康](../Page/胡兆康.md "wikilink")：[香港首席](../Page/香港.md "wikilink")[保齡球運動員](../Page/保齡球.md "wikilink")，首位香港保齡球世界冠軍
  - [葉珮兒](../Page/葉珮兒.md "wikilink")：奥運射擊選手
  - 林秋霞：香港體育舞蹈總會董事，拉丁舞冠軍
  - [崔錦棠](../Page/崔錦棠.md "wikilink")：前香港三項鐵人代表隊成員及教練、演員、體育節目主持
  - 王港政：香港大學藥理及藥劑學系助理講

## 參見

  - 基督教興學會有限公司

<!-- end list -->

  - [迦密柏雨中學](../Page/迦密柏雨中學.md "wikilink")
  - [迦密唐賓南紀念中學](../Page/迦密唐賓南紀念中學.md "wikilink")
  - [迦密主恩中學](../Page/迦密主恩中學.md "wikilink")
  - [迦密聖道中學](../Page/迦密聖道中學.md "wikilink")

<!-- end list -->

  - 基督教興學會小學

<!-- end list -->

  - [迦密愛禮信小學](../Page/迦密愛禮信小學.md "wikilink")
  - [迦密梁省德學校](../Page/迦密梁省德學校.md "wikilink")

## 参考文献

## 外部連結

  - [迦密愛禮信中學官方網頁](http://www.calfss.edu.hk)
  - [迦密愛禮信中學信泉堂官方網頁](http://www.efccfaith.org/default.aspx)

[Category:基督教興學會學校](../Category/基督教興學會學校.md "wikilink")
[Category:華景](../Category/華景.md "wikilink")
[C](../Category/葵青區中學.md "wikilink")
[Category:1982年創建的教育機構](../Category/1982年創建的教育機構.md "wikilink")

1.
2.
3.