《**典當商**》（英語：）是一部由 [Edward Lewis
Wallant](../Page/Edward_Lewis_Wallant.md "wikilink")
所寫的一部小說，講述一名猶太人[典當商在](../Page/典當商.md "wikilink")[納粹集中營逃出後在](../Page/納粹.md "wikilink")[哈萊姆經營一家](../Page/哈萊姆.md "wikilink")[當鋪時去維持生計](../Page/當鋪.md "wikilink")。

1965年，這部小說在美國被拍成電影，由[希德尼·鲁迈特執導](../Page/希德尼·鲁迈特.md "wikilink")，由 [Morton
S. Fine](../Page/Morton_S._Fine.md "wikilink") 和 [David
Friedkin](../Page/David_Friedkin.md "wikilink")
編劇，由[洛·史泰格](../Page/洛·史泰格.md "wikilink")、[Geraldine
Fitzgerald](../Page/Geraldine_Fitzgerald.md "wikilink")
和[布罗克·彼得斯s](../Page/布罗克·彼得斯s.md "wikilink") 等影星演出。

此部電影曾經獲[奧斯卡金像獎提名](../Page/奧斯卡金像獎.md "wikilink")，提名項目為[最佳男主角獎](../Page/奥斯卡最佳男主角奖.md "wikilink")。

## 外部參考

  -
[Category:1964年电影](../Category/1964年电影.md "wikilink")
[Category:英语电影](../Category/英语电影.md "wikilink")
[Category:美国剧情片](../Category/美国剧情片.md "wikilink")
[Category:1960年代剧情片](../Category/1960年代剧情片.md "wikilink")
[Category:纽约市背景电影](../Category/纽约市背景电影.md "wikilink")
[Category:犹太电影](../Category/犹太电影.md "wikilink")
[Category:犹太人大屠杀电影](../Category/犹太人大屠杀电影.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")
[P](../Category/美國電影作品.md "wikilink")
[Category:西德尼·鲁迈特电影](../Category/西德尼·鲁迈特电影.md "wikilink")