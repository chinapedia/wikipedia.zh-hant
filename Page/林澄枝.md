**林澄枝**（），[台灣政治人物](../Page/台灣.md "wikilink")，[台灣本省人](../Page/台灣本省人.md "wikilink")，[高雄](../Page/高雄.md "wikilink")[橋頭人](../Page/橋頭區.md "wikilink")，故[副總統](../Page/副總統.md "wikilink")[謝東閔長媳](../Page/謝東閔.md "wikilink")，是[中國國民黨史上首位](../Page/中國國民黨.md "wikilink")[女性副主席](../Page/女性.md "wikilink")（2000年－2006年）。曾任[行政院](../Page/行政院.md "wikilink")[文建會主委](../Page/文建會.md "wikilink")、[實踐大學副校長](../Page/實踐大學.md "wikilink")，現任[中華民國婦女聯合會常委](../Page/中華民國婦女聯合會.md "wikilink")。廿三歲時嫁給前[中華民國副總統](../Page/中華民國副總統.md "wikilink")[謝東閔的兒子謝孟雄](../Page/謝東閔.md "wikilink")。三十九歲時擔任謝東閔創辦的[實踐家政專科學校](../Page/實踐家政專科學校.md "wikilink")[校長](../Page/校長.md "wikilink")，是當時台灣最年輕的大專女校長。1989年，50歲的林澄枝得到[中國國民黨中央委員會的賞識](../Page/中國國民黨中央委員會.md "wikilink")，由學術界步入政壇，曾出任出任中國國民黨中央委員會婦女工作會副主任（1989年－1993年）主任（1993年－1996年）、[行政院文化建設委員會主任委員](../Page/行政院文化建設委員會.md "wikilink")（1996年－2000年）、[實踐大學副校長](../Page/實踐大學.md "wikilink")、[中國國民黨副主席](../Page/中國國民黨副主席.md "wikilink")（2000年－2006年）、[中華民國總統府資政等職](../Page/中華民國總統府資政.md "wikilink")。
{{-}}

[中國國民黨副主席](../Category/中國國民黨副主席.md "wikilink")
[Category:中國國民黨中常委](../Category/中國國民黨中常委.md "wikilink")
[Category:中國國民黨副主席](../Category/中國國民黨副主席.md "wikilink")
[Category:中華民國總統府資政](../Category/中華民國總統府資政.md "wikilink")
[中華民國行政院文化建設委員會主任委員](../Category/中華民國行政院文化建設委員會主任委員.md "wikilink")
[Category:第3屆中華民國國民大會代表](../Category/第3屆中華民國國民大會代表.md "wikilink")
[Category:第2屆中華民國國民大會代表](../Category/第2屆中華民國國民大會代表.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:實踐大學校友](../Category/實踐大學校友.md "wikilink")
[Category:國立臺南女子高級中學校友](../Category/國立臺南女子高級中學校友.md "wikilink")
[Category:臺灣女性政治人物](../Category/臺灣女性政治人物.md "wikilink")
[Category:橋頭人](../Category/橋頭人.md "wikilink")
[C](../Category/林姓.md "wikilink")