[Carl_ritter.jpg](https://zh.wikipedia.org/wiki/File:Carl_ritter.jpg "fig:Carl_ritter.jpg")

**卡爾·李特爾**（Carl
Ritter，），為[德國地理學家](../Page/德國.md "wikilink")，出生在[奎德林堡](../Page/奎德林堡.md "wikilink")，後世稱之為[人文地理學之父](../Page/人文地理學.md "wikilink")。

1796年，他入讀了[哈雷大學](../Page/哈雷大學.md "wikilink")，讀[自然科學和](../Page/自然科學.md "wikilink")[文史等課程](../Page/文史.md "wikilink")。1819年，他擔任[法蘭克福大學](../Page/法蘭克福大學.md "wikilink")[歷史學](../Page/歷史學.md "wikilink")[教授](../Page/教授.md "wikilink")，1820年擔任[柏林洪堡大學首任](../Page/柏林洪堡大學.md "wikilink")[地理學](../Page/地理學.md "wikilink")[教授](../Page/教授.md "wikilink")，直到1859年9月28日他逝世。

他主张在地理学中贯彻因果联系观点，运用经验法（实地观察）和比较法，对区域地理的研究强调各种地理现象（有机的、无机的、人文的、非人文的）的因果关系，从而揭示区域个性；在具体研究工作中，偏重人文现象，把自然作为人文的基本原因，认为自然决定人类历史的发展；声称每一大陆各有其特定的形状与位置，对人类发展各自起着特定作用，用自然解释人文。

雖然他也注意自然現象及它們之間互相存在的關係，但最終他關注的是人與自然界之間的關係。他認為[地理學就是研究](../Page/地理學.md "wikilink")「空間分布的各種架構原理」的科學。

李特爾著名的學術界名著叫《地學通論》，又名《地球科學與自然和人類歷史》。

## 外部連結

  - [Matt Rosenberg on
    Ritter](http://geography.about.com/library/weekly/aa011000a.htm)
  - [Carl Ritter and Elisée
    Reclus](http://cybergeo.revues.org/index22981.html)
  - [Comparative Geography by Carl Ritter translated by W.L. Gage at
    google
    books](https://books.google.com/books?id=LXgDAAAAQAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false)

[Category:德国地理学家](../Category/德国地理学家.md "wikilink")
[Category:德国历史学家](../Category/德国历史学家.md "wikilink")
[Category:哈雷大學校友](../Category/哈雷大學校友.md "wikilink")
[Category:法蘭克福大學教師](../Category/法蘭克福大學教師.md "wikilink")
[Category:柏林洪堡大學教師](../Category/柏林洪堡大學教師.md "wikilink")