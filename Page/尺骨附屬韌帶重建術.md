**尺骨附屬韌帶重建術**（UCL, Ulnar Collateral Ligament Reconstruction）又稱**Tommy
John韌帶重建手術**，此手術把受傷[手肘](../Page/手肘.md "wikilink")[尺骨的](../Page/尺骨.md "wikilink")[韌帶用身上其他部位的韌帶替換](../Page/韌帶.md "wikilink")（通常是從病人的[前臂](../Page/前臂.md "wikilink")、大腿後方、[腳部](../Page/腳.md "wikilink")）。1974年，[Frank
Jobe醫生在一位](../Page/Frank_Jobe.md "wikilink")[洛杉磯道奇的](../Page/洛杉磯道奇.md "wikilink")[投手](../Page/投手.md "wikilink")[Tommy
John身上实施这项手术](../Page/Tommy_John.md "wikilink")，并获得成功。Tommy
John成為第1位成功進行這項手術的職業運動員，這一項手術也因此得名。

近年來接受手術後康復的機率約為85%到90%。1974年，Tommy John要手術時，醫師Frank Jobe估計康復機率為1%。Tommy
John花了18個月復健，在1976年球季重回[大聯盟](../Page/大聯盟.md "wikilink")，一直擔任投手直到1989年（45歲）。隨著技術的進步，現在這樣的手術只須要花1個小時。手術後的投手若要完整[復健約要](../Page/復健.md "wikilink")1年，其他守位的選手約要6個月。

有不少投手在受傷接受手術之後，投得比手術前更多更快。但這與手術本身無關，而是選手在術前的球速早已因為韌帶耗損而變慢，手術只是幫助他們回到應有的球速。反而是大量的復健訓練內容常常使他們又要再次手術。

在10到18歲的[少棒](../Page/少棒.md "wikilink")、[青少棒選手受傷接受手術的例子也漸漸增加](../Page/青少棒.md "wikilink")，應該和[球季拉長與投球數提高有關](../Page/球季.md "wikilink")。

肘韌帶撕裂，簡稱為"dead arm
injury"，是許多投手進行此手術的主要原因。有很多因素都造成肘韌帶撕裂，在投手身上最常見的是投得太大力或過度使用。

## 曾經接受Tommy John韌帶重建手術的部份知名投手

  - 美國職棒大聯盟投手[Tommy John](../Page/Tommy_John.md "wikilink")（1974年）
  - 美國職棒大聯盟投手[A·J·布耐特](../Page/A·J·布耐特.md "wikilink")
  - 中華職棒味全龍隊投手[黃平洋](../Page/黃平洋.md "wikilink")（1995年）
  - 美國職棒大聯盟芝加哥小熊隊投手[郭泓志](../Page/郭泓志.md "wikilink")（2000年）
  - 美國職棒大聯盟科羅拉多落磯隊投手[曹錦輝](../Page/曹錦輝.md "wikilink")（2001年）
  - 中華職棒La new熊隊投手[許志昌](../Page/許志昌.md "wikilink")（2003年）
  - 中華職棒兄弟象隊投手[張士凱](../Page/張士凱.md "wikilink")（2003年）
  - 中華職棒中信鯨隊投手[孟憲政](../Page/孟憲政.md "wikilink")（2003年）
  - 中華職棒統一獅隊投手[沈柏蒼](../Page/沈柏蒼.md "wikilink")（2004年）
  - 韓國職棒起亞老虎隊投手[林昌勇](../Page/林昌勇.md "wikilink")（2004年）
  - 韓國職棒三星獅隊投手[權奕](../Page/權奕.md "wikilink")（2005年）
  - 中華職棒統一獅隊外野手[郭岱琦](../Page/郭岱琦.md "wikilink")（2006年）
  - 美國職棒大聯盟巴爾的摩金鶯隊[陳偉殷](../Page/陳偉殷.md "wikilink")（2006年）
  - 美國職棒大聯盟波士頓紅襪隊投手[林旺億](../Page/林旺億.md "wikilink")（2007年）
  - 美國職棒大聯盟華盛頓國民隊投手[史蒂芬·史特拉斯堡](../Page/史蒂芬·史特拉斯堡.md "wikilink")（2010年）
  - 美國職棒大聯盟紐約洋基隊投手[喬巴·張伯倫](../Page/喬巴·張伯倫.md "wikilink")（2011年）
  - 美國職棒大聯盟德州遊騎兵隊投手[達比修有](../Page/達比修有.md "wikilink")（2015年）
  - [中華職棒大聯盟](../Page/中華職業棒球大聯盟.md "wikilink")[中信兄弟隊新人投手](../Page/中信兄弟.md "wikilink")[黃恩賜](../Page/黃恩賜_\(1996年出生\).md "wikilink")（2017年）
  - 美國職棒大聯盟洛杉磯天使隊投手/打者[大谷翔平](../Page/大谷翔平.md "wikilink")（2018年）

## 外部連結

  -
[Category:手術](../Category/手術.md "wikilink")
[Category:運動醫學](../Category/運動醫學.md "wikilink")