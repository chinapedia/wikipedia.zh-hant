[大麵焿.JPG](https://zh.wikipedia.org/wiki/File:大麵焿.JPG "fig:大麵焿.JPG")
**大麵羹**（亦為**大麵焿**）是[臺中市獨有的特色](../Page/臺中市.md "wikilink")[小吃](../Page/小吃.md "wikilink")。它是由「大麵條」、加[油蔥酥](../Page/油蔥酥.md "wikilink")、[蝦米](../Page/蝦米.md "wikilink")、碎[蘿蔔乾和切段的](../Page/蘿蔔乾.md "wikilink")[韮菜拌著吃的一種](../Page/韮菜.md "wikilink")[麵食](../Page/麵食.md "wikilink")，[麵條口感滑溜滑溜](../Page/麵條.md "wikilink")，麵湯濃濃稠稠，而所謂的大麵比一般[黃麵](../Page/黃麵.md "wikilink")、[白麵都要粗](../Page/白麵.md "wikilink")；因大麵下滾水煮熟後，麵湯稠稠的，嘗起來有點黏又不會太黏，所以稱作為「[羹](../Page/羹.md "wikilink")」，還帶著特殊的鹼味，但與一般小吃裡所謂的羹不同，一般為早餐主食。

## 歷史

[台灣戰後初期](../Page/台灣戰後.md "wikilink")，就已經出現大麵羹，當時只是窮人家餬口的食物，因此也沒加什麼配料，粗大的麵條下肚只求溫飽，因此加入大量的[鹼粉](../Page/鹼粉.md "wikilink")（同「[福建炒麵](../Page/福建炒麵.md "wikilink")」\[1\]\[2\]），使其煮起來時會脹大許多。後來許多店家加入了一些[菜脯](../Page/菜脯.md "wikilink")、[蝦米](../Page/蝦米.md "wikilink")、[肉燥](../Page/肉燥.md "wikilink")、[韭菜等等配料調味](../Page/韭菜.md "wikilink")，簡單的香味加上大麵羹特有的「鹼味」，漸漸受到許多普羅大眾的喜愛，而發展成一道便宜又具有特色的地方小吃。

## 流傳地區

這種黃色加了鹼粉的特製粗[麵條](../Page/麵條.md "wikilink")，味道較濃，由於它是屬於濕麵條，常溫下只能保存一至兩天，發跡時並沒完善的冷凍保存設備，[交通也不發達](../Page/交通.md "wikilink")，才成為地域性強的傳統小吃，也就因為如此，看的到大麵羹的區域，集中在[大肚溪以北](../Page/大肚溪.md "wikilink")、[豐原區以南](../Page/豐原區.md "wikilink")、[草屯鎮以西](../Page/草屯鎮.md "wikilink")、[大雅區以及](../Page/大雅區.md "wikilink")[梧棲區以東](../Page/梧棲區.md "wikilink")，換句話說，大麵羹就成了多數在[臺中市](../Page/臺中市.md "wikilink")\[3\]\[4\]的[東區](../Page/東區_\(台中市\).md "wikilink")、[西區](../Page/西區_\(台中市\).md "wikilink")、[南區](../Page/南區_\(台中市\).md "wikilink")、[北區以及](../Page/北區_\(台中市\).md "wikilink")[中區才吃的到](../Page/中區_\(台中市\).md "wikilink")。後有移居外縣市的人，所以在[南投縣與](../Page/南投縣.md "wikilink")[新竹市偶有發現販賣以解鄉愁](../Page/新竹市.md "wikilink")。

## 特色

大麵是[麵粉加](../Page/麵粉.md "wikilink")[鹼粉製成的一種特製粗](../Page/鹼.md "wikilink")[麵條](../Page/麵條.md "wikilink")，能夠久煮不爛、越煮越香Q，所以吃起來有種特殊的味道，而且顏色為黃褐色至深褐色不等，因此之所以稱為「[羹](../Page/羹.md "wikilink")」，其實應該是「[鹼](../Page/鹼.md "wikilink")」的[臺灣閩南語發音](../Page/臺灣閩南語.md "wikilink")，意指吃起來的那股特殊「鹼味」。\[5\]\[6\]

## 相關台中特有小吃

  - [麻芛](../Page/麻芛.md "wikilink")（麻薏、蔴薏、麻穎、麻薏）
  - [羅氏秋水茶](../Page/羅氏秋水茶.md "wikilink")

## 參考資料

  - 腳注

<!-- end list -->

  - 引用

<!-- end list -->

  - 書目

<!-- end list -->

  -
[羹](../Category/台灣麵條.md "wikilink")
[Category:麵條食品](../Category/麵條食品.md "wikilink")
[食](../Category/台中市文化.md "wikilink")
[餐](../Category/台灣發明.md "wikilink")

1.
2.
3.
4.
5.
6.