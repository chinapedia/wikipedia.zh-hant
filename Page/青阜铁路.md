**青阜铁路**从[安徽省](../Page/安徽省.md "wikilink")[淮北青龙山起](../Page/淮北.md "wikilink")，向南直至[阜阳止](../Page/阜阳.md "wikilink")，全长152千米，途经百善、海孜、临涣、青町、涡阳、江集、西潘楼、插花，至阜阳，与[阜淮铁路接轨](../Page/阜淮铁路.md "wikilink")，是连结[淮南](../Page/淮南.md "wikilink")、淮北两大煤炭基地的重要线路。

## 与其它铁路的连接

  - 阜阳：[漯阜铁路](../Page/漯阜铁路.md "wikilink")、[阜淮铁路](../Page/阜淮铁路.md "wikilink")、[京九铁路](../Page/京九铁路.md "wikilink")。
  - 青町：[青芦铁路](../Page/青芦铁路.md "wikilink")。
  - 青龙山：[符夹铁路](../Page/符夹铁路.md "wikilink")。

[Category:安徽铁路](../Category/安徽铁路.md "wikilink")
[Category:中国铁路线](../Category/中国铁路线.md "wikilink")
[Category:中国铁路上海局集团有限公司](../Category/中国铁路上海局集团有限公司.md "wikilink")