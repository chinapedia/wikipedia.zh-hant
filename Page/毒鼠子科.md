**毒鼠子科**共有3[属](../Page/属.md "wikilink")160余[种](../Page/种.md "wikilink")，分布在全球[热带和](../Page/热带.md "wikilink")[亚热带地带](../Page/亚热带.md "wikilink")，主要生长在干旱地区，[中国有](../Page/中国.md "wikilink")2种—*[Dichapetalum
hainanense](../Page/Dichapetalum_hainanense.md "wikilink")*（Hance）Engler和
*[Dichapetalum
gelonioides](../Page/Dichapetalum_gelonioides.md "wikilink")*（Hook. f.
）Engler，全部生长在[海南](../Page/海南.md "wikilink")。

本科[植物为小](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")、[灌木或](../Page/灌木.md "wikilink")[藤本](../Page/藤本.md "wikilink")；单[叶互生](../Page/叶.md "wikilink")，有托叶早落；[花小](../Page/花.md "wikilink")，[花瓣](../Page/花瓣.md "wikilink")2；[果实为](../Page/果实.md "wikilink")[核果](../Page/核果.md "wikilink")。

## 属

  - [毒鼠子属](../Page/毒鼠子属.md "wikilink") *Dichapetalum*
  - *[Stephanopodium](../Page/Stephanopodium.md "wikilink")*
  - *[Tapura](../Page/Tapura.md "wikilink")*

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[卫矛目中](../Page/卫矛目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该放在](../Page/APG_分类法.md "wikilink")[金虎尾目中](../Page/金虎尾目.md "wikilink")，2003年经过修订的[APG
II
分类法认为可以选择性地与](../Page/APG_II_分类法.md "wikilink")[金壳果科](../Page/金壳果科.md "wikilink")、[大戟科和](../Page/大戟科.md "wikilink")[三角果科合并](../Page/三角果科.md "wikilink")。

### 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[毒鼠子科](http://delta-intkey.com/angio/www/dichapet.htm)
  - [APG网站中的毒鼠子科](http://www.mobot.org/MOBOT/Research/APWeb/orders/malpighialesweb.htm)
  - [NCBI中的毒鼠子科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=4313)

[\*](../Category/毒鼠子科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")