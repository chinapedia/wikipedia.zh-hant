以下是《**笑傲江湖**》的相關作品：

## 原著小說

  - [金庸所著的](../Page/金庸.md "wikilink")[武俠小說](../Page/武俠小說.md "wikilink")《[笑傲江湖](../Page/笑傲江湖.md "wikilink")》，1967年開始在《[明報](../Page/明報.md "wikilink")》連載。

## 改編的影視作品

### 電視劇

| 劇名                                                 | 首播年份  | 所在地                              | 播映平台                                 |
| -------------------------------------------------- | ----- | -------------------------------- | ------------------------------------ |
| [笑傲江湖](../Page/笑傲江湖_\(1984年電視劇\).md "wikilink")    | 1984年 | [香港](../Page/香港.md "wikilink")   | [無線電視](../Page/無線電視.md "wikilink")   |
| [笑傲江湖](../Page/笑傲江湖_\(1985年電視劇\).md "wikilink")    | 1985年 | [台灣](../Page/台灣.md "wikilink")   | [台視](../Page/台視.md "wikilink")       |
| [笑傲江湖](../Page/笑傲江湖_\(1996年電視劇\).md "wikilink")    | 1996年 | [香港](../Page/香港.md "wikilink")   | [無線電視](../Page/無線電視.md "wikilink")   |
| [笑傲江湖](../Page/笑傲江湖_\(2000年新加坡电视剧\).md "wikilink") | 2000年 | [新加坡](../Page/新加坡.md "wikilink") | [新傳媒電視](../Page/新傳媒電視.md "wikilink") |
| [笑傲江湖](../Page/笑傲江湖_\(2000年台灣電視劇\).md "wikilink")  | 2000年 | [台灣](../Page/台灣.md "wikilink")   | [中視](../Page/中視.md "wikilink")       |
| [笑傲江湖](../Page/笑傲江湖_\(2001年电视剧\).md "wikilink")    | 2001年 | [中國](../Page/中國.md "wikilink")   | [中央电视台](../Page/中央电视台.md "wikilink") |
| [笑傲江湖](../Page/笑傲江湖_\(2013年電視劇\).md "wikilink")    | 2013年 | [中國](../Page/中國.md "wikilink")   | [湖南卫视](../Page/湖南卫视.md "wikilink")   |
| [笑傲江湖](../Page/笑傲江湖_\(2018年電視劇\).md "wikilink")    | 2018年 | [中國](../Page/中國.md "wikilink")   | [优酷](../Page/优酷.md "wikilink")       |
|                                                    |       |                                  |                                      |

### 電影

| 片名                                             | 首映年份  | 攝製地區                           |
| ---------------------------------------------- | ----- | ------------------------------ |
| [笑傲江湖](../Page/笑傲江湖_\(1978年電影\).md "wikilink") | 1978年 | [香港](../Page/香港.md "wikilink") |
| [笑傲江湖](../Page/笑傲江湖_\(1990年電影\).md "wikilink") | 1990年 | [香港](../Page/香港.md "wikilink") |
| [笑傲江湖二之東方不敗](../Page/笑傲江湖二之東方不敗.md "wikilink") | 1992年 | [香港](../Page/香港.md "wikilink") |
| [東方不敗之風雲再起](../Page/東方不敗之風雲再起.md "wikilink")   | 1993年 | [香港](../Page/香港.md "wikilink") |

### 微電影

| 劇名                                             | 首演年份  |
| ---------------------------------------------- | ----- |
| [笑傲江湖Online](../Page/笑傲江湖Online.md "wikilink") | 2013年 |

### 歌舞劇

<table>
<thead>
<tr class="header">
<th><p>劇名</p></th>
<th><p>首演年份</p></th>
<th><p>主辦單位</p></th>
<th><p>編舞</p></th>
<th><p>飾演演員</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>令狐沖</p></td>
<td><p>任盈盈</p></td>
<td><p>東方不敗</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/笑傲江湖_(歌舞劇).md" title="wikilink">笑傲江湖</a></p></td>
<td><p>2006年</p></td>
<td><p><a href="../Page/香港舞蹈團.md" title="wikilink">香港舞蹈團</a></p></td>
<td><p><a href="../Page/梁國城.md" title="wikilink">梁國城</a><br />
<a href="../Page/楊雲濤.md" title="wikilink">楊雲濤</a></p></td>
<td><p><a href="../Page/劉迎宏.md" title="wikilink">劉迎宏</a></p></td>
</tr>
</tbody>
</table>

### 漫畫

  - 《[笑傲江湖](../Page/笑傲江湖_\(漫畫\).md "wikilink")》，[李志清](../Page/李志清_\(香港\).md "wikilink")

### 遊戲

  - [笑傲江湖](../Page/笑傲江湖_\(遊戲\).md "wikilink")：單機遊戲，[智冠科技製作](../Page/智冠科技.md "wikilink")，1993年3月29日發表。
  - [笑傲江湖之日月神教](../Page/笑傲江湖之日月神教.md "wikilink")：單機遊戲，[昱泉國際製作](../Page/昱泉國際.md "wikilink")，2000年發行。
  - [笑傲江湖貳](../Page/笑傲江湖貳.md "wikilink")：單機遊戲，[昱泉國際製作](../Page/昱泉國際.md "wikilink")，2001年發行。
  - [笑傲江湖網路版](../Page/笑傲江湖網路版.md "wikilink")：昱泉國際製作，2011年4月營運。
  - [笑傲江湖Online](../Page/笑傲江湖Online.md "wikilink")：[完美世界研發](../Page/完美世界.md "wikilink")，在臺灣由[艾玩天地發行](../Page/艾玩天地.md "wikilink")，2013年11月20日封測。

### 綜藝節目

  - [笑傲江湖
    (電視節目)](../Page/笑傲江湖_\(電視節目\).md "wikilink")，是[東方衛視推出的素人喜劇綜藝節目](../Page/東方衛視.md "wikilink")
      - [笑傲江湖第一季 (綜藝節目)](../Page/笑傲江湖第一季_\(綜藝節目\).md "wikilink")
      - [笑傲江湖第二季 (綜藝節目)](../Page/笑傲江湖第二季_\(綜藝節目\).md "wikilink")
      - [笑傲江湖第三季 (綜藝節目)](../Page/笑傲江湖第三季_\(綜藝節目\).md "wikilink")

[\*](../Category/笑傲江湖.md "wikilink")