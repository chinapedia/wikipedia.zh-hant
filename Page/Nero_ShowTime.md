**Nero ShowTime**是[德國](../Page/德國.md "wikilink")[Nero
AG公司所開發出的一套多媒體播放軟體](../Page/Nero_AG.md "wikilink")（Media
Player），這套播放軟體並沒有單獨銷售，而是隨附在Nero AG公司的其他軟體產品中一同銷售，例如[Nero Burning
ROM](../Page/Nero_Burning_ROM.md "wikilink")\[1\]、[Nero
Digital等](../Page/Nero_Digital.md "wikilink")。

NeroShow
Time的功效及定位類似於[Microsoft的](../Page/Microsoft.md "wikilink")[Windows
Media
Player](../Page/Windows_Media_Player.md "wikilink")、[Apple的](../Page/苹果电脑.md "wikilink")[QuickTime
Player](../Page/QuickTime#QuickTime_播放程式.md "wikilink")、Real的[RealPlayer](../Page/RealPlayer.md "wikilink")，用戶多半用其來播放[DVD視訊](../Page/DVD.md "wikilink")（DVD-Video）影片、MPEG-4或H.264格式的影片。

## ShowTime 2

ShowTime 2隨附在[Nero 6
Reloaded中](../Page/Nero_6_Reloaded.md "wikilink")，主要的功能特色是：

  - 可播放H.264/AVC格式的視訊內容
  - 可播放用Nero MediaHome軟體製作出來的音視訊內容

## ShowTime 3

ShowTime 3隨附在[Nero 7
Premium中](../Page/Nero_7_Premium.md "wikilink")，主要的功能特色是：

  - 極佳的新高畫質程式操作介面（Skin）設計。
  - 新的播放微調（Jog-Shuttle，俗稱：飛梭功能）。
  - 增加了「最小化模式」。
  - 新的視訊強化型態檔（video enhancement profiles），使用者可自行定義型態檔來強化視訊品質。
  - 支援[NVIDIA公司的](../Page/NVIDIA.md "wikilink")[PureVideo功能](../Page/PureVideo.md "wikilink")，可使H.264格式的視訊播放獲得加速。

## ShowTime Mobile

[ShowTime-Mobile-Beta-2.jpg](https://zh.wikipedia.org/wiki/File:ShowTime-Mobile-Beta-2.jpg "fig:ShowTime-Mobile-Beta-2.jpg")
ShowTime Mobile目前尚未完成，仍處在外部測試版（Beta
2）的階段，是針對掌上型、可攜式的媒體播放裝置而設計的播放軟體，ShowTime
Mobile也隨附在[Nero Digital
Everywhere中](../Page/Nero_Digital_Everywhere.md "wikilink")。已知的功能特色有：

  - 可安裝在[XScale架構或](../Page/XScale.md "wikilink")[ARM架構處理器及](../Page/ARM架構.md "wikilink")[Windows
    CE作業系統的應用裝置內](../Page/Windows_CE.md "wikilink")，包括[智慧型手機](../Page/智能手机.md "wikilink")、[個人數位助理等](../Page/个人数码助理.md "wikilink")。
  - 支援的音訊格式：Nero Digital™ Audio (MPEG-4
    Audio)、[AAC](../Page/AAC.md "wikilink")、[HE-AAC](../Page/HE-AAC.md "wikilink")、[HE-AAC](../Page/HE-AAC.md "wikilink")
    v2、[MP3](../Page/MP3.md "wikilink")、[Ogg](../Page/Ogg.md "wikilink")
    Vorbis、[WAV](../Page/WAV.md "wikilink")
  - 支援的視訊格式：Nero Digital™（MPEG-4視訊，包括AVC/H.264）
  - 支援的檔案格式： .mp4、 .m4a、 .mp3、 .wav、 .ogg
  - 支援[通用隨插即用](../Page/UPnP.md "wikilink")（UPnP™），可自動發現網路上的UPnP裝置
  - 完全支援Nero MediaHome軟體

## 附註

<references/>

## 另見

  - [Nero Digital](../Page/Nero_Digital.md "wikilink")

## 外部連結

[de:Nero Burning
ROM\#Funktionen](../Page/de:Nero_Burning_ROM#Funktionen.md "wikilink")

[Category:媒体播放器](../Category/媒体播放器.md "wikilink")

1.  Nero Burning ROM已無單獨銷售，而是隨附在Nero 7、Nero 6系列的套裝軟體內。