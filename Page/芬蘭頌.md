《**芬蘭頌**》，作品26（*Finlandia,
Op.26*）乃是[芬蘭作曲家](../Page/芬蘭.md "wikilink")[西貝流士所創作的](../Page/让·西贝柳斯.md "wikilink")[交響詩](../Page/交響詩.md "wikilink")，全長約9分半鐘。1899年西貝流士完成第一個版本，以作為愛國戲劇《自古以來的景象》中的配樂。及後在1900年稍事修正，編成《芬蘭頌》。

## 創作背景與概要

由於彼時[俄羅斯帝國侵佔芬蘭](../Page/俄羅斯帝國.md "wikilink")，廢除其自治權，作者為了一個對抗俄羅斯帝國的愛國慶典而寫成此曲。這首交響詩採用大量激昂憤概、氣勢磅礡的[音樂](../Page/音樂.md "wikilink")，向芬蘭人民表達政局危機，企圖喚起大家的愛國之心。樂曲末段裡西貝流士卻選用較溫和的旋律，後來這段寧靜詳和的旋律被作者獨立抽出成另一首樂曲
《自由之詩》（Finlandia
Hymn）。1941年芬蘭[詩人](../Page/詩人.md "wikilink")替《自由之詩》填入歌詞，成為芬蘭最重要的愛國樂曲之一\[1\]。

## 其他

  - 1967年成立的[非洲](../Page/非洲.md "wikilink")[國家](../Page/國家.md "wikilink")[比亞法拉](../Page/比亞法拉.md "wikilink")，國祚雖短，但其曾以《芬蘭頌》为[國歌](../Page/國歌.md "wikilink")，重新填詞后命名為《旭日東升的土地（Land
    of the Rising Sun）》。
  - [美國](../Page/美國.md "wikilink")[小說家](../Page/小說家.md "wikilink")對《芬蘭頌》相當推崇，他曾形容自己首度聽到這首樂曲時「自椅子上跳起，將桌子推倒，敲下牆壁的部分泥塊，並且大喊：『天哪！這位作者是誰？』」\[2\]。1935年當他拜訪[赫爾辛基時](../Page/赫爾辛基.md "wikilink")，曾與西貝流士會面。
  - 芬蘭籍[電影導演](../Page/電影導演.md "wikilink")[雷尼·哈林曾在](../Page/雷尼·哈林.md "wikilink")1990年作品《[終極警探2](../Page/終極警探2.md "wikilink")》裡採用一段《芬蘭頌》的曲調。
  - 位於[香港](../Page/香港.md "wikilink")[新界的](../Page/新界.md "wikilink")[荃灣官立中學](../Page/荃灣官立中學.md "wikilink")，其[校歌亦採用了一段](../Page/校歌.md "wikilink")《芬蘭頌》的曲調。
  - 前述的《自由之詩》換上另一首歌詞後，也是[基督教的](../Page/基督教.md "wikilink")[聖詩](../Page/聖詩.md "wikilink")《Be
    Still, My Soul》。

## 參考資料

  - 《Jean Sibelius and His World》，Daniel M. Grimley編著，Princeton
    University Press，2011年，ISBN 978-0-691-15281-3。
  - [西貝流士：芬蘭頌](https://web.archive.org/web/20070310235647/http://cdhi.audionet.com.tw/200105/Vision/Vision-%E8%A5%BF%E8%B2%9D%E6%B5%81%E5%A3%AB%E8%8A%AC%E8%98%AD%E9%A0%8C.htm)

## 外部連結

  - [西貝流士樂譜](http://www.dlib.indiana.edu/variations/scores/cag4960/index.html)

[Category:交響詩](../Category/交響詩.md "wikilink")
[Category:西貝流士作品](../Category/西貝流士作品.md "wikilink")
[Category:1900年樂曲](../Category/1900年樂曲.md "wikilink")

1.  芬蘭的[國歌為](../Page/國歌.md "wikilink")《[我們的國家](../Page/我們的國家.md "wikilink")》。
2.  參看《Jean Sibelius and His World》，Daniel M. Grimley編著，Princeton
    University Press，2011年，ISBN 978-0-691-15281-3，頁168。