**花垣县**位于中国[湖南西部边缘](../Page/湖南.md "wikilink")、[花垣河流域](../Page/花垣河.md "wikilink")，是[湘西土家族苗族自治州下辖的一个县](../Page/湘西土家族苗族自治州.md "wikilink")。面积1,111平方公里；人口272,646人（2004年），非农业人口40956人，农业人口231690人。[1](https://web.archive.org/web/20051126233702/http://www.hntj.gov.cn/tjgb/xqgb/xxgb/default.htm)。县政府驻[花垣镇](../Page/花垣镇.md "wikilink")。

## 历史

清代设[永绥直隶厅](../Page/永绥直隶厅.md "wikilink")，1913年改为永绥县。1952年由[永绥县更名花垣县](../Page/永绥县.md "wikilink")，以县治花垣得名。

## 地理

花垣县地处湖南、[重庆和](../Page/重庆.md "wikilink")[贵州三](../Page/贵州.md "wikilink")[省](../Page/省.md "wikilink")[市交界处](../Page/直辖市.md "wikilink")。相邻的[县级行政区](../Page/县级行政区.md "wikilink")，北接[保靖县](../Page/保靖.md "wikilink")，东临[吉首市](../Page/吉首.md "wikilink")，南界[凤凰县](../Page/凤凰县.md "wikilink")，西部和重庆[秀山土家族苗族自治县](../Page/秀山.md "wikilink")、贵州[松桃苗族自治县相连](../Page/松桃.md "wikilink")。

## 区划人口

2004年花垣县辖8个镇、13个乡。2005年进行乡镇行政区划调整，将原21个乡镇调整为18个，将全县的8镇13乡调整为8镇10乡，即将原大河坪乡整体与茶峒镇合并更名为边城镇，原窝勺乡整体与花垣镇合并组成新的花垣镇，原大龙洞乡整体与补抽乡合并组成新的补抽乡。

  - [镇](../Page/镇.md "wikilink")：花垣镇（窝勺乡和原花垣镇合并组合而成）、边城镇（由茶洞镇和大河坪乡合并组合而成）、团结镇、民乐镇、龙潭镇、吉卫镇和麻栗场镇；
  - [乡](../Page/乡.md "wikilink")：长乐乡、道二乡、雅酉乡、雅桥乡、排碧乡、猫儿乡、排料乡、补抽乡（大龙洞乡和原补抽乡）、排吾乡、两河乡和董马库乡。

主要民族为[苗族](../Page/苗族.md "wikilink")、汉族和[土家族](../Page/土家族.md "wikilink")，其中苗族207,430人，汉族47,246人，土家族17,294人。当地苗族使用[苗语湘西方言](../Page/苗语湘西方言.md "wikilink")。

## 文化

花垣县被[文化部授予](../Page/文化部.md "wikilink")“文化之乡”。浓烈的苗族风情令人陶醉。古老的苗族山民月月有节日，尤以“四月八”歌会、“七月八”赶秋节最为隆重热烈。花垣县茶峒镇，古朴清秀，因[沈从文笔下的](../Page/沈从文.md "wikilink")《[边城](../Page/边城.md "wikilink")》而驰名中外。

## 旅游

境内有[古苗河](../Page/古苗河.md "wikilink")、[边城茶洞](../Page/边城茶洞.md "wikilink")、[小龙洞和](../Page/小龙洞.md "wikilink")[大龙洞等历史文化古迹和自然景观](../Page/大龙洞.md "wikilink")。

[湘](../Page/分类:国家级贫困县.md "wikilink")

[花垣县](../Category/花垣县.md "wikilink") [县](../Category/湘西县市.md "wikilink")
[湘西](../Category/湖南省县份.md "wikilink")
[Category:苗族](../Category/苗族.md "wikilink")
[Category:土家族](../Category/土家族.md "wikilink")