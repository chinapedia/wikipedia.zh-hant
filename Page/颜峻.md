**颜峻**（），[甘肃](../Page/甘肃.md "wikilink")[兰州人](../Page/兰州.md "wikilink")。他是[中国大陆的音乐家](../Page/中国大陆.md "wikilink")、诗人和声音艺术家，独立厂牌[撒把芥末的发起人](../Page/撒把芥末.md "wikilink")。曾经是乐评人。

## 生平

1973年生于[甘肃省](../Page/甘肃.md "wikilink")[兰州市](../Page/兰州.md "wikilink")，就读于[西北师范大学中文系](../Page/西北师范大学.md "wikilink")，毕业后在兰州晚报工作，并兼职在当地电台作[摇滚乐节目](../Page/摇滚乐.md "wikilink")。

1999年从报社辞职，移居[北京](../Page/北京.md "wikilink")。

## 作为乐评人

1996年开始在《[音乐生活报](../Page/音乐生活报.md "wikilink")》上开设专栏以来，在《[音乐天堂](../Page/音乐天堂.md "wikilink")》、《[摩登天空](../Page/摩登天空.md "wikilink")》、《[通俗歌曲](../Page/通俗歌曲.md "wikilink")》、《[非音乐](../Page/非音乐.md "wikilink")》、《[自由音乐](../Page/自由音乐.md "wikilink")》、《[口袋音乐](../Page/口袋音乐.md "wikilink")》、《[读书](../Page/读书.md "wikilink")》、《[东方](../Page/东方.md "wikilink")》、《[视界](../Page/视界.md "wikilink")》、《[今日先锋](../Page/今日先锋.md "wikilink")》、《[书城](../Page/书城.md "wikilink")》等上百家杂志上发表大量文章，并策划和组织国内和国外乐队和艺人的演出，包括[舌头](../Page/舌头.md "wikilink")、[左小祖咒](../Page/左小祖咒.md "wikilink")、[大友良英](../Page/大友良英.md "wikilink")、[灰野敬二](../Page/灰野敬二.md "wikilink")、[Elliott
Sharp](../Page/Elliott_Sharp.md "wikilink")、等。

2014年，在撒把芥末厂牌发表《[野兽档案](http://www.subjam.org/archives/2938)》（PDF版），系重新校改过的大部分与中国地下音乐有关的音乐文章。其他结集出版的乐评、访谈和音乐随笔，参看
著作。

## 作为音乐家

2003-2004年，开始田野录音创作。并和[王凡](../Page/王凡.md "wikilink")、[FM3](../Page/FM3.md "wikilink")、[武权合作进行实验性的演出](../Page/武权.md "wikilink")，使用田野录音、人声和电子乐器。

2005年创建铁观音乐队，开始主持实验音乐和即兴音乐活动[水陆观音](../Page/水陆观音.md "wikilink")，共持续167期。主持[Mini
MIDI音乐节](../Page/Mini_MIDI.md "wikilink")，并为[大山子国际艺术节](../Page/大山子国际艺术节.md "wikilink")、[大声展等艺术活动担任策划](../Page/大声展.md "wikilink")。

2008年以来，活跃在国内外音乐演出中。并与[这个店](../Page/这个店.md "wikilink")（[维他命艺术空间](../Page/维他命艺术空间.md "wikilink")）等艺术机构合作，创作声音艺术相关作品。参加过[2014年上海双年展](../Page/2014年上海双年展.md "wikilink")。目前是[大友良英领导的](../Page/大友良英.md "wikilink")[FEN乐队成员](../Page/FEN.md "wikilink")。并与[小河](../Page/小河_\(歌手\).md "wikilink")、[巫娜](../Page/巫娜.md "wikilink")、李戴菓等人组成了[茶博士五重奏乐队](../Page/茶博士五重奏.md "wikilink")。

## 作为诗人

1996年，发表诗集《诗49首》。复印，50册。

2001年，在撒把芥末厂牌发表诗集《次声波》。印数600册。

2006年8月，在撒把芥末厂牌发表个人第三本诗集《不可能》。印数1000册。\[1\]。

2011年6月，参加鹿特丹国际诗歌节。

2012年，在澳大利亚/日本的出版社 Vagabond 出版英文诗集 You Jump Into Another Dream。由美国诗人
Glenn Stowell 翻译。

2012年，参加柏林国际诗歌节。

## 著作

1999年，《[北京新声](../Page/北京新声.md "wikilink")》（与[欧宁](../Page/欧宁.md "wikilink")、[聂筝合作](../Page/聂筝.md "wikilink")，湖南文艺出版社）ISBN
7-5404-2184-3\[2\]

1999年，《[铁血摇滚](../Page/铁血摇滚.md "wikilink")》（成都音像出版社）

2001年，《[内心的噪音](../Page/内心的噪音.md "wikilink")》（外文出版社）ISBN
7-119-02543-0\[3\]

2001年7月，《次声波--sub jam b003》（自印，铁托出品）ISBN 9787080314142\[4\]

2002年，《[地地下——新音乐潜行记](../Page/地地下——新音乐潜行记.md "wikilink")》（文化艺术出版社）ISBN
9787503921964\[5\]

2004年6月，《[燃烧的噪音](../Page/燃烧的噪音.md "wikilink")》（江苏人民出版社）ISBN
721403669X\[6\]

2005年9月1日，《我在我不在的地方》（接力出版社）ISBN 9787806799987\[7\]

2006年，《灰飞烟灭--一个人的摇滚乐观察》（花城出版社）ISBN 9787536047358\[8\]

2006年8月，《不可能》（出版：SUBJAM）\[9\]

## 参考资料

## 外部链接

  - [颜峻的个人网站](http://www.yanjun.org)
  - [颜峻的Blog](http://www.yanjun.org/blog)
  - [SUB JAM 撒把芥末官方网站](http://www.subjam.org/)
  - [人物: 颜峻](https://www.goethe.de/ins/cn/zh/kul/mag/20629090.html)
    歌德学院（中国）
  - [颜峻的Bandcamp主页](https://yanjun.bandcamp.com/)

[Category:西北师范大学校友](../Category/西北师范大学校友.md "wikilink")
[Category:中国音乐家](../Category/中国音乐家.md "wikilink")
[Category:中国诗人](../Category/中国诗人.md "wikilink")
[Category:实验音乐家](../Category/实验音乐家.md "wikilink")
[J峻](../Category/颜姓.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.