**三好市**（）是位於[德島縣西部的行政區劃](../Page/德島縣.md "wikilink")，同時與[四國地區的另外三縣](../Page/四國地區.md "wikilink")[香川縣](../Page/香川縣.md "wikilink")、[愛媛縣](../Page/愛媛縣.md "wikilink")、[高知縣接鄰](../Page/高知縣.md "wikilink")\[1\]，佔全德島縣面積的六分之一，也是四國地區面積最大的市町村級行政區劃，但由於位於山區，轄內近90%的土地為山地\[2\]，實際上可居住地區的面積僅有13%，主要市區位於轄區北部[吉野川沿岸](../Page/吉野川.md "wikilink")。

## 歷史

現在的三好市轄區在令制國時原屬[阿波國](../Page/阿波國.md "wikilink")[美馬郡](../Page/美馬郡.md "wikilink")，在9世紀時被劃出為新設立的[三好郡](../Page/三好郡.md "wikilink")，但南部山區的祖谷地區仍屬[美馬郡](../Page/美馬郡.md "wikilink")。

12世紀末的[源平合戰中](../Page/源平合戰.md "wikilink")，[平家在](../Page/平家.md "wikilink")[屋島之戰戰敗後](../Page/屋島之戰.md "wikilink")，傳說有許多[平家落人逃至位於現三好市西南部山區的祖谷地區避難](../Page/平家落人.md "wikilink")。

13世紀[清和源氏的分支](../Page/清和源氏.md "wikilink")開始擔任阿波國守護，其被分配到三好郡的子孫後來以本地地名作為姓氏，成為[三好氏](../Page/三好氏.md "wikilink")，之後三好氏便以此為根據地逐漸將勢力擴張至了整個阿波國，其勢力並往北發展進入當時日本的政治中心[近畿地區](../Page/近畿.md "wikilink")，在16世紀中甚至一度控制了[室町幕府](../Page/室町幕府.md "wikilink")；但隨後三好氏的勢力開始衰退，三好郡此地也被來自[土佐國的](../Page/土佐國.md "wikilink")[長宗我部氏佔領](../Page/長宗我部氏.md "wikilink")，長宗我部氏也一度以此地的作為統治整個[四國的根據地](../Page/四國.md "wikilink")。但在[豐臣秀吉發動](../Page/豐臣秀吉.md "wikilink")[四國征伐後](../Page/四國征伐.md "wikilink")，其戰後的獎賞分配中，隨整個阿波國被封給[蜂須賀正勝之子](../Page/蜂須賀正勝.md "wikilink")[蜂須賀家政](../Page/蜂須賀家政.md "wikilink")，此地也一同成為[德島藩](../Page/德島藩.md "wikilink")[蜂須賀氏的領地](../Page/蜂須賀氏.md "wikilink")，蜂須賀氏的統治一直維持到[江戶時代結束](../Page/江戶時代.md "wikilink")。

1871年實施[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，曾先後隸屬[德島縣](../Page/德島縣.md "wikilink")、[名東縣](../Page/名東縣.md "wikilink")、[高知縣](../Page/高知縣.md "wikilink")，直到1880年後恢復隸屬德島縣至今。

1879年實施，現在的三好市範圍在當時分屬：三好郡、、、、、、[池田村](../Page/池田町_\(德島縣\).md "wikilink")、、、美馬郡[西祖谷山村](../Page/西祖谷山村.md "wikilink")、[東祖谷山村](../Page/東祖谷山村.md "wikilink")，共11個村。經歷1950年代的[昭和大合併後](../Page/市町村合併.md "wikilink")，這11個村被整併為[池田町](../Page/池田町_\(德島縣\).md "wikilink")、、、4町及未曾參與任何整併的西祖谷山村、東祖谷山村2村。\[3\]

在21世紀初的平成大合併中，當時屬三好郡下的6町2村原被規劃為分成東西兩個區域分別合併，但經過多次討論後，最終原屬東部區域的井川町和三野町決定加入西部區域的整併，成為4町2村的合併\[4\]，三加茂町與三好町另外合併為東三好町，位於東側的三野町，因東三好町阻隔形成飛地；合併後的新名稱原本有取自三好郡郡名的「三好市」，和取自人口最多的行政區劃池田町町名的「阿波池田市」兩個方案，但最終經投票後決定以「三好市」作為新名稱。

在2010年，位於[愛知縣](../Page/愛知縣.md "wikilink")[西加茂郡的三好町改制為市時](../Page/西加茂郡.md "wikilink")，原本希望直接使用原町名「三好」作為改制後的市名，但由於當時德島縣的三好市已經成立，較晚成立的城市要使用相同名市必須經過先成立城市的同意；在德島縣三好市的反對下，愛知縣三好町改制後改使用「三好」的平假名「」做為城市名稱，但該市的中文譯名仍然是相同的「[三好市](../Page/三好市_\(愛知縣\).md "wikilink")」。而[廣島縣](../Page/廣島縣.md "wikilink")[三次市的日語發音同為](../Page/三次市.md "wikilink")「」。

### 年表

  - 1889年10月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：\[5\]
      - [三好郡](../Page/三好郡.md "wikilink")：三野村、池田村、箸藏村、佐馬地村、三繩村、三名村、山城谷村、井川村、井內谷村。
      - [美馬郡](../Page/美馬郡.md "wikilink")：[東祖谷山村](../Page/東祖谷山村.md "wikilink")、[西祖谷山村](../Page/西祖谷山村.md "wikilink")。
  - 1905年10月1日：池田村改制為[池田町](../Page/池田町_\(德島縣\).md "wikilink")。
  - 1907年11月1日：井川村改制並改名為[辻町](../Page/辻町_\(德島縣\).md "wikilink")
  - 1924年1月26日：三野村改制為[三野町](../Page/三野町_\(德島縣\).md "wikilink")。
  - 1950年10月1日：東祖谷山村和西祖谷山村改隸屬三好郡。
  - 1956年9月30日：
      - 箸藏村被併入池田町。
      - 山城谷村和三名村[合併為](../Page/市町村合併.md "wikilink")[山城町](../Page/山城町_\(德島縣\).md "wikilink")。
  - 1959年3月31日：池田町、三繩村、佐馬地村合併為新設置的池田町。
  - 1959年4月1日：辻町和井內谷村合併為[井川町](../Page/井川町_\(德島縣\).md "wikilink")。
  - 2006年3月1日：三野町、池田町、山城町、井川町、東祖谷山村、西祖谷山村合併為**三好市**。\[6\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年10月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>三野村</p></td>
<td><p>1924年1月26日<br />
三野町</p></td>
<td><p>2006年3月1日<br />
合併為三好市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>箸藏村</p></td>
<td><p>1956年9月30日<br />
併入池田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>池田村</p></td>
<td><p>1905年10月1日<br />
池田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佐馬地村</p></td>
<td><p>1959年3月31日<br />
併入池田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>三繩村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>三名村</p></td>
<td><p>1956年9月30日<br />
併入山城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>山城谷村</p></td>
<td><p>1956年9月30日<br />
山城町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>井川村</p></td>
<td><p>1907年11月1日<br />
辻町</p></td>
<td><p>1959年4月1日<br />
合併為井川町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>井內谷村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>東祖谷山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西祖谷山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任市長

<table>
<thead>
<tr class="header">
<th><p>任</p></th>
<th><p>姓名</p></th>
<th><p>上任年月日</p></th>
<th><p>卸任年月日</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>2006年4月16日</p></td>
<td><p>2013年7月23日</p></td>
<td><p>於第二任任期中辭職</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>2013年7月24日</p></td>
<td><p>現任</p></td>
<td><p>已連任一次</p></td>
</tr>
</tbody>
</table>

## 交通

空中交通方面由於位於四國的中間區域，與位於香川縣[高松機場](../Page/高松機場.md "wikilink")、德島縣[德島飛行場](../Page/德島飛行場.md "wikilink")、高知縣[高知機場的交通時間差異不大](../Page/高知機場.md "wikilink")，都約在60至75分鐘的開車車程。

鐵路方面，轄內的主要車站為[阿波池田車站](../Page/阿波池田車站.md "wikilink")，由於[四國旅客鐵道的](../Page/四國旅客鐵道.md "wikilink")[土讚線及](../Page/土讚線.md "wikilink")[德島線也在轄內的](../Page/德島線.md "wikilink")[佃車站交會](../Page/佃車站.md "wikilink")，因此阿波池田車站也成為從土讚線轉乘德島線列車的起點車站。由阿波池田車站搭乘特急列車往[德島市](../Page/德島市.md "wikilink")[德島車站](../Page/德島車站.md "wikilink")、[高知市](../Page/高知市.md "wikilink")[高知車站](../Page/高知車站.md "wikilink")、[高松市](../Page/高松市.md "wikilink")[高松車站](../Page/高松車站_\(香川縣\).md "wikilink")、[岡山市](../Page/岡山市.md "wikilink")[岡山車站的車程都約在](../Page/岡山車站_\(日本\).md "wikilink")75分鐘。

而[大步危車站由於周邊有往](../Page/大步危車站.md "wikilink")[大步危](../Page/大步危.md "wikilink")、[吉野川](../Page/吉野川.md "wikilink")、[祖谷溪](../Page/祖谷溪.md "wikilink")、[祖谷溫泉](../Page/祖谷溫泉.md "wikilink")、[蔓橋](../Page/蔓橋.md "wikilink")、小便小僧等熱門旅遊景點，也成為觀光上的主要車站。

[德島自動車道也以東西向通過轄內北部的主要市區](../Page/德島自動車道.md "wikilink")。

### 鐵路

  - [四國旅客鐵道](../Page/四國旅客鐵道.md "wikilink")
      - [土讚線](../Page/土讚線.md "wikilink")：（←香川縣[三豐市](../Page/三豐市.md "wikilink")）
        - [坪尻車站](../Page/坪尻車站.md "wikilink") -
        [箸藏車站](../Page/箸藏車站.md "wikilink") - （東三好町） -
        [佃車站](../Page/佃車站.md "wikilink") -
        [阿波池田車站](../Page/阿波池田車站.md "wikilink") -
        [三繩車站](../Page/三繩車站.md "wikilink") -
        [祖谷口車站](../Page/祖谷口車站.md "wikilink") -
        [阿波川口車站](../Page/阿波川口車站.md "wikilink") -
        [小步危車站](../Page/小步危車站.md "wikilink") -
        [大步危車站](../Page/大步危車站.md "wikilink") -
        （高知縣[大豐町](../Page/大豐町.md "wikilink")→）
      - [德島線](../Page/德島線.md "wikilink")：佃車站 -
        [辻車站](../Page/辻車站.md "wikilink") -
        （[東三好町](../Page/東三好町.md "wikilink")→）

[File:JR-Shikoku-Awa-IkedaStation.jpg|阿波池田車站](File:JR-Shikoku-Awa-IkedaStation.jpg%7C阿波池田車站)
<File:JRS> tsukuda sta.jpg|佃車站 <File:JRS> tsuji sta.jpg|辻車站
[File:Tsuboriji-Station20100525-02.jpg|坪尻車站](File:Tsuboriji-Station20100525-02.jpg%7C坪尻車站)
<File:JRS> minawa sta.jpg|三繩車站 <File:JRS> awa-kawaguchi sta.jpg|阿波川口車站
<File:Oboke> Station 20110906.jpg|大步危車站 <File:JRS> koboke sta.jpg|小步危車站

### 道路

  - 高速道路

<!-- end list -->

  - [德島自動車道](../Page/德島自動車道.md "wikilink")：（←東三好町） -
    [井川池田交流道](../Page/井川池田交流道.md "wikilink") -
    [池田休息區](../Page/池田停车区.md "wikilink") - （愛媛縣四國中央市）

## 觀光資源

[Iya_Valley_03.jpg](https://zh.wikipedia.org/wiki/File:Iya_Valley_03.jpg "fig:Iya_Valley_03.jpg")
[Unpenji_11.JPG](https://zh.wikipedia.org/wiki/File:Unpenji_11.JPG "fig:Unpenji_11.JPG")

  - [東祖谷落合聚落](../Page/東祖谷落合.md "wikilink")：位於東祖谷的「重要傳統建物群保存地區」。

  - [劍山](../Page/劍山.md "wikilink")：標高1955m，為[西日本第二高峰](../Page/西日本.md "wikilink")。

  - 下影的梯田：被列入[日本梯田百選](../Page/日本梯田百選.md "wikilink")。

  - [祖谷溪](../Page/祖谷溪.md "wikilink")：被列入[日本清流百選](../Page/日本清流百選.md "wikilink")、德島88景。

  - [蔓橋](../Page/蔓橋.md "wikilink")

  - [祖谷溫泉](../Page/祖谷溫泉.md "wikilink")

  - [四國八十八箇所](../Page/四國八十八箇所.md "wikilink")

      - 66番[札所](../Page/札所.md "wikilink") -
        巨鼇山・[雲邊寺](../Page/雲邊寺.md "wikilink")

  - [黑澤濕原](../Page/黑澤濕原.md "wikilink")。

  - 金剛瀑布、龍頭瀑布。

  - [鹽塚高原](../Page/鹽塚高原.md "wikilink")

  - [中央構造線的露出地帶](../Page/中央構造線.md "wikilink")。

  - 遺址

  - 遺址

  - 遺址

## 教育

  - 大學設施

<!-- end list -->

  - [京都大學防災研究所附屬斜面災害研究中心德島山崩觀測所](../Page/京都大學.md "wikilink")

<!-- end list -->

  - 高等學校

<!-- end list -->

  - [德島縣立池田高等學校](../Page/德島縣立池田高等學校.md "wikilink")
  - [德島縣立三好高等學校](../Page/德島縣立三好高等學校.md "wikilink")
  - [德島縣立辻高等學校](../Page/德島縣立辻高等學校.md "wikilink")

## 姊妹、友好城市

### 日本

為2006年合併前各町村的姊妹、友好城市

  - [池田町](../Page/池田町_\(北海道\).md "wikilink")（[北海道](../Page/北海道.md "wikilink")[中川郡](../Page/中川郡_\(十勝國\).md "wikilink")）
  - [池田町](../Page/池田町_\(長野縣\).md "wikilink")（[長野縣](../Page/長野縣.md "wikilink")[北安曇郡](../Page/北安曇郡.md "wikilink")）
  - [池田町](../Page/池田町_\(福井縣\).md "wikilink")（[福井縣](../Page/福井縣.md "wikilink")[今立郡](../Page/今立郡.md "wikilink")）
  - [池田町](../Page/池田町_\(岐阜縣\).md "wikilink")（[岐阜縣](../Page/岐阜縣.md "wikilink")[揖斐郡](../Page/揖斐郡.md "wikilink")）
  - [池田町](../Page/池田町_\(香川縣\).md "wikilink")（[香川縣](../Page/香川縣.md "wikilink")[小豆郡](../Page/小豆郡.md "wikilink")）
  - [池田市](../Page/池田市.md "wikilink")（[大阪府](../Page/大阪府.md "wikilink")）
  - [椎葉村](../Page/椎葉村.md "wikilink")（[宮崎縣](../Page/宮崎縣.md "wikilink")[東臼杵郡](../Page/東臼杵郡.md "wikilink")）
  - [八代市](../Page/八代市.md "wikilink")（[熊本縣](../Page/熊本縣.md "wikilink")）
  - [十津川村](../Page/十津川村.md "wikilink")（[奈良縣](../Page/奈良縣.md "wikilink")[吉野郡](../Page/吉野郡.md "wikilink")）

### 海外

  - [塔克維拉](../Page/塔克維拉_\(華盛頓州\).md "wikilink")（[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[金郡](../Page/金郡_\(華盛頓州\).md "wikilink")）

      -
        於1979年與[井川町締結](../Page/井川町_\(德島縣\).md "wikilink")。\[7\]

  - [達爾斯](../Page/達爾斯.md "wikilink")（美國[俄勒冈州](../Page/俄勒冈州.md "wikilink")[沃斯科縣](../Page/沃斯科縣.md "wikilink")）

      -
        於2003年與[池田町締結](../Page/池田町_\(德島縣\).md "wikilink")。

## 本地出身之名人

  - [三好長慶](../Page/三好長慶.md "wikilink")：[戰國時代](../Page/戰國時代_\(日本\).md "wikilink")[大名](../Page/大名.md "wikilink")。
  - [鶴田常吉](../Page/鶴田常吉.md "wikilink")：教育學者、四國女子短期大學（現[四國大学](../Page/四國大学.md "wikilink")）第一任校長。
  - [高井美穗](../Page/高井美穗.md "wikilink")：前[眾議院](../Page/眾議院_\(日本\).md "wikilink")[議員](../Page/議員.md "wikilink")。
  - [竹葉多重子](../Page/竹葉多重子.md "wikilink")：飛靶射擊選手。
  - [今井ゆうぞう](../Page/今井ゆうぞう.md "wikilink")：歌手。
  - [龜長友義](../Page/龜長友義.md "wikilink")：前[參議院議員](../Page/參議院_\(日本\).md "wikilink")。
  - [山口俊一](../Page/山口俊一.md "wikilink")：眾議院議員。
  - [富士正晴](../Page/富士正晴.md "wikilink")：[作家](../Page/作家.md "wikilink")、詩人。
  - [園尾隆司](../Page/園尾隆司.md "wikilink")：法官。
  - [山下菊二](../Page/山下菊二.md "wikilink")：[畫家](../Page/畫家.md "wikilink")。
  - [大田正](../Page/大田正.md "wikilink")：前[德島縣知事](../Page/德島縣知事.md "wikilink")。
  - [細田雄一](../Page/細田雄一.md "wikilink")：[三项全能選手](../Page/三项全能.md "wikilink")。
  - [近藤利一](../Page/近藤利一.md "wikilink")：[馬主](../Page/馬主.md "wikilink")

## 參考資料

## 相關條目

  - [三好市](../Page/三好市.md "wikilink")
  - [三好郡](../Page/三好郡.md "wikilink")

## 外部連結

  -

  - [三好市觀光站](https://web.archive.org/web/20170711110520/http://www.miyoshinavi.jp/)

  - [三好市觀光協會 Blog](http://kankoumiyoshi.blog24.fc2.com/)

  - [三好市觀光協會](https://miyoshi-tourism.jp/cn/)

  -
<!-- end list -->

1.

2.
3.

4.

5.
6.
7.