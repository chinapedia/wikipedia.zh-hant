**頭孢他啶**是第三代的[頭孢菌素](../Page/頭孢菌素.md "wikilink")[抗生素](../Page/抗生素.md "wikilink")。與其他第三代的先鋒霉素相似，有著較廣泛的反應對抗[革蘭氏陽性菌及](../Page/革蘭氏陽性菌.md "wikilink")[革蘭氏陰性菌](../Page/革蘭氏陰性菌.md "wikilink")。不同的是，它能有效對抗[綠膿桿菌](../Page/綠膿桿菌.md "wikilink")，卻對革蘭氏陽性菌的抗力較弱，所以不會用作這種感染。頭孢噻甲羧肟是以包括「覆達欣®」在內的作為商標出售。

## 臨床使用

  -

頭孢他啶經常用於治療因[綠膿桿菌引致的感染](../Page/綠膿桿菌.md "wikilink")。它亦與其他[抗生素一同被用作](../Page/抗生素.md "wikilink")[嗜中性白血球過低症的經驗治療](../Page/嗜中性白血球過低症.md "wikilink")。雖然劑量是因徵狀、感染的嚴重性及病人的[腎臟功能而決定](../Page/腎臟.md "wikilink")，但一般的劑量是每8－12小時1－2克（靜脈注射／肌注）。

## 化學結構

除了[亞胺](../Page/亞胺.md "wikilink")[旁鏈外](../Page/旁鏈.md "wikilink")，與其他第三代[頭孢菌素比較](../Page/頭孢菌素.md "wikilink")，頭孢他啶有著複雜的一端（包括兩個[甲基及一個](../Page/甲基.md "wikilink")[羧基](../Page/羧基.md "wikilink")）能提升對[革蘭氏陰性菌所分泌的](../Page/革蘭氏陰性菌.md "wikilink")[β內酰胺酶的穩定性](../Page/β內酰胺酶.md "wikilink")。這種特別的穩定性能增加頭孢他啶對抗革蘭氏陰性菌，如[綠膿桿菌的阻力](../Page/綠膿桿菌.md "wikilink")。另一端帶電荷的[吡啶則能增加水溶性](../Page/吡啶.md "wikilink")。

## 參看

  - [頭孢菌素](../Page/頭孢菌素.md "wikilink")

[Category:頭孢菌素](../Category/頭孢菌素.md "wikilink")
[Category:噻唑](../Category/噻唑.md "wikilink")
[Category:吡啶鎓鹽](../Category/吡啶鎓鹽.md "wikilink")
[Category:两性离子](../Category/两性离子.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")