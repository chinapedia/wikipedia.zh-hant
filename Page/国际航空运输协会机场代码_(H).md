| 代碼  | 機場                                                     | 城市                                                | 国家和地區                                            |
| --- | ------------------------------------------------------ | ------------------------------------------------- | ------------------------------------------------ |
| HAA | 哈什維克機場                                                 | [哈什維克](../Page/哈什維克.md "wikilink")                | [挪威](../Page/挪威.md "wikilink")                   |
| HAB | 蘭金菲特機場                                                 | [漢密爾頓](../Page/漢密爾頓_\(阿拉巴馬州\).md "wikilink")      | [美國](../Page/美國.md "wikilink")                   |
| HAC | [八丈島機場](../Page/八丈島機場.md "wikilink")                   | [八丈島](../Page/八丈島.md "wikilink")                  | [日本](../Page/日本.md "wikilink")                   |
| HAD | 哈爾姆斯塔德機場                                               | [哈爾姆斯塔德](../Page/哈爾姆斯塔德.md "wikilink")            | [瑞典](../Page/瑞典.md "wikilink")                   |
| HAE | [海州機場](../Page/海州機場.md "wikilink")                     | [海州市](../Page/海州市.md "wikilink")                  | [朝鲜民主主义人民共和国](../Page/朝鲜民主主义人民共和国.md "wikilink") |
| HAF | 半月灣機場}                                                 | [半月灣](../Page/半月灣_\(加利福尼亞州\).md "wikilink")       | [美國](../Page/美國.md "wikilink")                   |
| HAH | [賽義德·易卜拉欣王子國際機場](../Page/賽義德·易卜拉欣王子國際機場.md "wikilink") | [莫羅尼](../Page/莫羅尼.md "wikilink")                  | [葛摩聯盟](../Page/葛摩聯盟.md "wikilink")               |
| HAJ | [漢諾威機場](../Page/漢諾威機場.md "wikilink")                   | [漢諾威](../Page/漢諾威.md "wikilink")                  | [德國](../Page/德國.md "wikilink")                   |
| HAK | [海口美兰国际机场](../Page/海口美兰国际机场.md "wikilink")             | [海口市](../Page/海口市.md "wikilink")                  | [中国](../Page/中華人民共和國.md "wikilink")              |
| HAL | 哈拉立機場                                                  | [奧希科托區](../Page/奧希科托區.md "wikilink")              | [納米比亞](../Page/納米比亞.md "wikilink")               |
| HAM | [漢堡機場](../Page/漢堡機場.md "wikilink")                     | [漢堡](../Page/漢堡.md "wikilink")                    | [德國](../Page/德國.md "wikilink")                   |
| HAN | [內排國際機場](../Page/內排國際機場.md "wikilink")                 | [河內](../Page/河內.md "wikilink")                    | [越南](../Page/越南.md "wikilink")                   |
| HAU | 海于格松機場                                                 | [海于格松](../Page/海于格松.md "wikilink")                | [挪威](../Page/挪威.md "wikilink")                   |
| HAV | [何塞·马蒂国际机场](../Page/何塞·马蒂国际机场.md "wikilink")           | [哈瓦那](../Page/哈瓦那.md "wikilink")                  | [古巴](../Page/古巴.md "wikilink")                   |
| HBA | [荷巴特國際機場](../Page/霍巴特國際機場.md "wikilink")               | [荷巴特](../Page/荷巴特.md "wikilink")                  | [澳洲](../Page/澳洲.md "wikilink")                   |
| HCN | [恆春機場](../Page/恆春機場.md "wikilink")                     | [屏東縣](../Page/屏東縣.md "wikilink")                  | [台灣](../Page/台灣.md "wikilink")                   |
| HDB | [海德堡機場](../Page/海德堡機場.md "wikilink")                   | [海德堡](../Page/海德堡.md "wikilink")                  | [德國](../Page/德國.md "wikilink")                   |
| HDG | [邯郸机场](../Page/邯郸机场.md "wikilink")                     | [邯郸市](../Page/邯郸市.md "wikilink")                  | [中國](../Page/中華人民共和國.md "wikilink")              |
| HDN | 海頓機場                                                   | [海頓](../Page/海登_\(科羅拉多州\).md "wikilink")          | [美國](../Page/美國.md "wikilink")                   |
| HDY | [合艾國際機場](../Page/合艾國際機場.md "wikilink")                 | [合艾](../Page/合艾.md "wikilink")                    | [泰國](../Page/泰國.md "wikilink")                   |
| HEL | [赫爾辛基-萬塔機場](../Page/赫爾辛基-萬塔機場.md "wikilink")           | [赫爾辛基](../Page/赫爾辛基.md "wikilink")                | [芬蘭](../Page/芬蘭.md "wikilink")                   |
| HEM | [赫爾辛基-馬爾米機場](../Page/赫爾辛基-馬爾米機場.md "wikilink")         | [赫爾辛基](../Page/赫爾辛基.md "wikilink")                | [芬蘭](../Page/芬蘭.md "wikilink")                   |
| HER | [伊拉克利翁機場](../Page/伊拉克利翁尼科斯·卡贊察基斯國際機場.md "wikilink")    | [伊拉克利翁](../Page/伊拉克利翁.md "wikilink")              | [希臘](../Page/希臘.md "wikilink")                   |
| HET | [呼和浩特白塔國際機場](../Page/呼和浩特白塔國際機場.md "wikilink")         | [呼和浩特市](../Page/呼和浩特市.md "wikilink")              | [中國](../Page/中華人民共和國.md "wikilink")              |
| HFA | [海法機場](../Page/海法機場.md "wikilink")                     | [海法](../Page/海法.md "wikilink")                    | [以色列](../Page/以色列.md "wikilink")                 |
| HFE | [合肥新橋國際機場](../Page/合肥新橋國際機場.md "wikilink")             | [合肥市](../Page/合肥市.md "wikilink")                  | [中國](../Page/中華人民共和國.md "wikilink")              |
| HFT | 亨墨菲斯機場                                                 | [亨墨菲斯](../Page/亨墨菲斯.md "wikilink")                | [挪威](../Page/挪威.md "wikilink")                   |
| HGH | [杭州蕭山國際機場](../Page/杭州蕭山國際機場.md "wikilink")             | [杭州市](../Page/杭州市.md "wikilink")                  | [中國](../Page/中華人民共和國.md "wikilink")              |
| HGR | 黑格斯敦地方機場                                               | [黑格斯敦](../Page/黑格斯敦_\(馬里蘭州\).md "wikilink")       | [美國](../Page/美國.md "wikilink")                   |
| HGU | [芒特哈根机场](../Page/芒特哈根机场.md "wikilink")                 | [芒特哈根](../Page/芒特哈根.md "wikilink")                | [巴布亞紐幾內亞](../Page/巴布亞紐幾內亞.md "wikilink")         |
| HHH | 希爾頓黑德島機場                                               | [希爾頓黑德島](../Page/希爾頓黑德島_\(南卡羅來納州\).md "wikilink") | [美國](../Page/美國.md "wikilink")                   |
| HIJ | [廣島機場](../Page/廣島機場.md "wikilink")                     | [廣島縣](../Page/廣島縣.md "wikilink")                  | [日本](../Page/日本.md "wikilink")                   |
| HIN | [泗川機場](../Page/泗川機場.md "wikilink")                     | [泗川市](../Page/泗川市.md "wikilink")                  | [韓國](../Page/韓國.md "wikilink")                   |
| HKD | [函館機場](../Page/函館機場.md "wikilink")                     | [函館市](../Page/函館市.md "wikilink")                  | [日本](../Page/日本.md "wikilink")                   |
| HKG | [香港國際機場](../Page/香港國際機場.md "wikilink")                 | [赤鱲角](../Page/赤鱲角.md "wikilink")                  | [香港](../Page/香港.md "wikilink")                   |
| HKT | [布吉國際機場](../Page/布吉國際機場.md "wikilink")                 | [普吉島](../Page/普吉島.md "wikilink")                  | [泰國](../Page/泰國.md "wikilink")                   |
| HMI | [哈密机场](../Page/哈密机场.md "wikilink")                     | [哈密市](../Page/哈密市.md "wikilink")                  | [中国](../Page/中華人民共和國.md "wikilink")              |
| HLD | [呼倫貝爾海拉爾機場](../Page/呼倫貝爾海拉爾機場.md "wikilink")           | [呼倫貝爾市](../Page/呼倫貝爾市.md "wikilink")              | [中國](../Page/中國.md "wikilink")                   |
| HNA | [花卷機場](../Page/花卷機場.md "wikilink")                     | [岩手縣](../Page/岩手縣.md "wikilink")                  | [日本](../Page/日本.md "wikilink")                   |
| HND | [羽田國際機場](../Page/羽田國際機場.md "wikilink")                 | [東京都](../Page/東京都.md "wikilink")                  | [日本](../Page/日本.md "wikilink")                   |
| HNL | [丹尼爾·井上國際機場](../Page/丹尼爾·井上國際機場.md "wikilink")         | [檀香山](../Page/檀香山.md "wikilink")                  | [美國](../Page/美國.md "wikilink")                   |
| HPH | [吉悲機場](../Page/吉悲機場.md "wikilink")                     | [海防市](../Page/海防市.md "wikilink")                  | [越南](../Page/越南.md "wikilink")                   |
| HRB | [哈尔滨太平国际机场](../Page/哈尔滨太平国际机场.md "wikilink")           | [哈尔滨市](../Page/哈尔滨市.md "wikilink")                | [中国](../Page/中華人民共和國.md "wikilink")              |
| HRE | [哈拉雷國際機場](../Page/哈拉雷國際機場.md "wikilink")               | [哈拉雷](../Page/哈拉雷.md "wikilink")                  | [辛巴威](../Page/辛巴威.md "wikilink")                 |
| HRK | [哈爾科夫國際機場](../Page/哈爾科夫國際機場.md "wikilink")             | [哈爾科夫](../Page/哈爾科夫.md "wikilink")                | [烏克蘭](../Page/烏克蘭.md "wikilink")                 |
| HTN | [和田机场](../Page/和田机场.md "wikilink")                     | [和田市](../Page/和田市.md "wikilink")                  | [中国](../Page/中華人民共和國.md "wikilink")              |
| HUI | [符牌機場](../Page/符牌機場.md "wikilink")                     | [順化市](../Page/順化市.md "wikilink")                  | [越南](../Page/越南.md "wikilink")                   |
| HUN | [花蓮機場](../Page/花蓮機場.md "wikilink")                     | [花蓮縣](../Page/花蓮縣.md "wikilink")                  | [台灣](../Page/台灣.md "wikilink")                   |
| HUZ | [惠州平潭机场](../Page/惠州平潭机场.md "wikilink")                 | [惠州市](../Page/惠州市.md "wikilink")                  | [中国](../Page/中華人民共和國.md "wikilink")              |
| HYA | 海恩尼斯機場                                                 | 海恩尼司                                              | [美國](../Page/美國.md "wikilink")                   |
| HYD | [拉吉夫·甘地國際機場](../Page/拉吉夫·甘地國際機場.md "wikilink")         | [海德拉巴](../Page/海得拉巴_\(印度\).md "wikilink")         | [印度](../Page/印度.md "wikilink")                   |
| HYG | 海達堡水用機場                                                | [海達堡](../Page/海達堡_\(阿拉斯加州\).md "wikilink")        | [美國](../Page/美國.md "wikilink")                   |
| HZG | [漢中機場](../Page/漢中機場.md "wikilink")                     | [漢中市](../Page/漢中市.md "wikilink")                  | [中國](../Page/中華人民共和國.md "wikilink")              |
| HIB | 藍傑地區機場                                                 | [希賓](../Page/希賓.md "wikilink")                    | [美國](../Page/美國.md "wikilink")                   |
| HIR | [霍尼亞拉國際機場](../Page/霍尼亞拉國際機場.md "wikilink")             | [霍尼亞拉](../Page/霍尼亞拉.md "wikilink")                | [所羅門群島](../Page/所羅門群島.md "wikilink")             |
| HIS | Hayman Island Airport                                  | Hayman Island                                     | [澳大利亞](../Page/澳大利亞.md "wikilink")               |
| HKK | 霍基蒂卡機場                                                 | 霍基蒂卡                                              | [新西蘭](../Page/新西蘭.md "wikilink")                 |
| HKN | [霍斯金斯機場](../Page/霍斯金斯機場.md "wikilink")                 | 霍斯金斯                                              | [巴布亞新幾內亞](../Page/巴布亞新幾內亞.md "wikilink")         |
| HKY | 希科里地區機場                                                | [希科里](../Page/希科里.md "wikilink")                  | [美國](../Page/美國.md "wikilink")                   |
| HLN | [海倫娜區域機場](../Page/海倫娜區域機場.md "wikilink")               | [海倫娜](../Page/赫勒拿_\(蒙大拿州\).md "wikilink")         | [美國](../Page/美國.md "wikilink")                   |
| HLZ | [漢米爾頓國際機場](../Page/漢米爾頓國際機場.md "wikilink")             | [漢密爾頓](../Page/漢密頓_\(紐西蘭\).md "wikilink")         | [新西蘭](../Page/新西蘭.md "wikilink")                 |
| HMO | 埃莫西約國際機場                                               | [埃莫西約](../Page/埃莫西約.md "wikilink")                | [墨西哥](../Page/墨西哥.md "wikilink")                 |
| HNH | 霍納機場                                                   | [霍納](../Page/霍納_\(阿拉斯加州\).md "wikilink")          | [美國](../Page/美國.md "wikilink")                   |
| HNS | 海恩斯機場                                                  | [海恩斯](../Page/海恩斯_\(阿拉斯加州\).md "wikilink")        | [美國](../Page/美國.md "wikilink")                   |
| HOB | 利郡區域機場                                                 | [利郡](../Page/利县_\(新墨西哥州\).md "wikilink")          | [美國](../Page/美國.md "wikilink")                   |
| HOM | 荷馬機場                                                   | 荷馬                                                | [美國](../Page/美國.md "wikilink")                   |
| HON | 休倫地區機場                                                 | 休倫湖                                               | [美國](../Page/美國.md "wikilink")                   |
| HOT | 紀念場機場                                                  | [溫泉城](../Page/溫泉城.md "wikilink")                  | [美國](../Page/美國.md "wikilink")                   |
| HOU | [威廉·佩特斯·霍比機場](../Page/威廉·佩特斯·霍比機場.md "wikilink")       | [休士頓](../Page/休士頓.md "wikilink")                  | [美國](../Page/美國.md "wikilink")                   |
| HPB | Hopper Bay Aiport                                      | 胡珀灣                                               | [美國](../Page/美國.md "wikilink")                   |
| HRG | 洪加達國際機場                                                | 洪加達                                               | [埃及](../Page/埃及.md "wikilink")                   |
| HRL | Valley 國際機場                                            | [哈靈根](../Page/哈靈根.md "wikilink")                  | [美國](../Page/美國.md "wikilink")                   |
| HRO | Boone County Airport                                   | 哈里森                                               | [美國](../Page/美國.md "wikilink")                   |
| HSI | Hastings Municipal Airport                             | 黑斯廷斯                                              | [美國](../Page/美國.md "wikilink")                   |
| HSV | 亨茨維爾國際機場                                               | [亨茨維爾](../Page/亨茨維爾.md "wikilink")                | [美國](../Page/美國.md "wikilink")                   |
| HTI | 漢密爾頓島機場                                                | 漢密爾頓島                                             | [澳大利亞](../Page/澳大利亞.md "wikilink")               |
| HTS | 三城地區機場                                                 | [三城](../Page/三城.md "wikilink")                    | [美國](../Page/美國.md "wikilink")                   |
| HUX | 華渡口國際機場                                                | 華土哥                                               | [墨西哥](../Page/墨西哥.md "wikilink")                 |
| HUY | 亨伯賽德機場                                                 | 亨伯賽德                                              | [聯合王國](../Page/聯合王國.md "wikilink")               |
| HVB | 赫維灣機場                                                  | 赫維灣                                               | [澳大利亞](../Page/澳大利亞.md "wikilink")               |
| HVR | 哈芙市-縣機場                                                | [哈佛](../Page/哈佛.md "wikilink")                    | [美國](../Page/美國.md "wikilink")                   |
| HWN | 萬基國家公園機場                                               | [萬基國家公園](../Page/萬基國家公園.md "wikilink")            | [辛巴威](../Page/辛巴威.md "wikilink")                 |
| HYS | Hays Municipal Airport                                 | Hays                                              | [美國](../Page/美國.md "wikilink")                   |
| HYN | [台州路桥机场](../Page/台州路桥机场.md "wikilink")                 | [台州](../Page/台州.md "wikilink")                    | [中国](../Page/中华人民共和国.md "wikilink")              |

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")