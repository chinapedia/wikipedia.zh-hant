**巨大遊覽遠足徑**是[歐洲一個龐大](../Page/歐洲.md "wikilink")[遠足徑網絡](../Page/遠足徑.md "wikilink")，主要覆蓋了[法國](../Page/法國.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[荷蘭及](../Page/荷蘭.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")。法國段由[法國步行遊覽聯盟所管理](../Page/法國步行遊覽聯盟.md "wikilink")。西班牙段則由[西班牙登山及攀爬運動聯盟所管理](../Page/西班牙登山及攀爬運動聯盟.md "wikilink")。

## 法國段

  - [GR 1](../Page/GR_1.md "wikilink")
  - [GR 2](../Page/GR_2.md "wikilink"): [卢昂](../Page/卢昂.md "wikilink") -
    [巴黎](../Page/巴黎.md "wikilink") - [第戎](../Page/第戎.md "wikilink")
  - [GR 3](../Page/GR_3.md "wikilink"): [拉博勒](../Page/拉博勒.md "wikilink")
    - [梭缪](../Page/梭缪.md "wikilink")(Saumur) -
    [奧爾良](../Page/奧爾良.md "wikilink")(Orléans) -
    [尼維斯](../Page/尼維斯.md "wikilink")(Nevers) - [mont
    Mézenc](../Page/mont_Mézenc.md "wikilink")
  - [GR 4](../Page/GR_4.md "wikilink"):
    [Royen](../Page/Royen.md "wikilink") -
    [利摩日](../Page/利摩日.md "wikilink") -
    [克莱蒙费朗](../Page/克莱蒙费朗.md "wikilink") -
    [Saint-Flour](../Page/Saint-Flour,_Cantal.md "wikilink") -
    [Pont-Saint-Esprit](../Page/Pont-Saint-Esprit.md "wikilink") -
    [Grasse](../Page/Grasse.md "wikilink")
  - [GR 5](../Page/GR_5.md "wikilink"):
    [荷蘭](../Page/荷蘭.md "wikilink")[貝爾根](../Page/貝爾根.md "wikilink")
    - [比利時](../Page/比利時.md "wikilink")[列日](../Page/列日.md "wikilink") -
    [盧森堡](../Page/盧森堡.md "wikilink")[迪基希區](../Page/迪基希區.md "wikilink")
    -
    [法國](../Page/法國.md "wikilink")[摩澤爾省](../Page/摩澤爾省.md "wikilink")[利沃頓市](../Page/利沃頓市.md "wikilink")
    [瑞士](../Page/瑞士.md "wikilink")[日內瓦湖](../Page/日內瓦湖.md "wikilink") -
    [法國](../Page/法國.md "wikilink")[尼斯](../Page/尼斯.md "wikilink")
  - [GR 6](../Page/GR_6.md "wikilink")
  - [GR 7](../Page/GR_7_\(France\).md "wikilink"):
    [阿尔萨斯鹤巴隆](../Page/阿尔萨斯鹤巴隆.md "wikilink") -
    [第戎](../Page/第戎.md "wikilink") -
    [Saint-Étienne](../Page/Saint-Étienne.md "wikilink") - [Andorra La
    Vella](../Page/Andorra_La_Vella.md "wikilink")
  - [GR 9](../Page/GR_9.md "wikilink")
  - [GR 10](../Page/GR_10.md "wikilink"):
    [Pyrenees](../Page/Pyrenees.md "wikilink") between
    [Hendaye](../Page/Hendaye.md "wikilink") and [Banyuls Sur
    Mer](../Page/Banyuls_Sur_Mer.md "wikilink")
  - [GR 11](../Page/GR_11.md "wikilink"):
    [Pyrenees](../Page/Pyrenees.md "wikilink") between Hondarrabia (near
    [Irún](../Page/Irún.md "wikilink")) and Cabo de Creus (near
    [Cadaqués](../Page/Cadaqués.md "wikilink"))
  - [GR 12](../Page/GR_12.md "wikilink")
  - [GR 14](../Page/GR_14.md "wikilink")
  - [GR 15](../Page/GR_15.md "wikilink")
  - [GR 20](../Page/GR_20.md "wikilink"):
    [Calenzana](../Page/Calenzana.md "wikilink") -
    [Conca](../Page/Conca.md "wikilink")
  - [GR 21](../Page/GR_21.md "wikilink")
  - [GR 26](../Page/GR_26.md "wikilink")
  - [GR 34](../Page/GR_34.md "wikilink")
  - [GR 34A](../Page/GR_34A.md "wikilink")
  - [GR 35](../Page/GR_35.md "wikilink")
  - [GR 36](../Page/GR_36.md "wikilink")
  - [GR 37](../Page/GR_37.md "wikilink")
  - [GR 38](../Page/GR_38.md "wikilink")
  - [GR 39](../Page/GR_39.md "wikilink")
  - [GR 41](../Page/GR_41.md "wikilink")
  - [GR 43](../Page/GR_43.md "wikilink")
  - [GR 44](../Page/GR_44.md "wikilink")
  - [GR 46](../Page/GR_46.md "wikilink")
  - [GR 56](../Page/GR_56.md "wikilink")
  - [GR 57](../Page/GR_57.md "wikilink"):
    [Liège](../Page/Liège_\(city\).md "wikilink")
    ([比利時](../Page/比利時.md "wikilink")) -
    [Diekirch](../Page/Diekirch.md "wikilink")
    ([盧森堡](../Page/盧森堡.md "wikilink"))
  - [GR 60](../Page/GR_60.md "wikilink")
  - [GR 65](../Page/GR_65.md "wikilink")
  - [GR 68](../Page/GR_68.md "wikilink")
  - [GR 70](../Page/GR_70.md "wikilink")
  - [GR 71](../Page/GR_71.md "wikilink")
  - [GR 72](../Page/GR_72.md "wikilink")
  - [GR 121](../Page/GR_121.md "wikilink")
  - [GR 122](../Page/GR_122.md "wikilink")
  - [GR 123](../Page/GR_123.md "wikilink")
  - [GR 125](../Page/GR_125.md "wikilink")
  - [GR 126](../Page/GR_126.md "wikilink")
  - [GR 128](../Page/GR_128.md "wikilink")
  - [GR 129](../Page/GR_129.md "wikilink")
  - [GR 130](../Page/GR_130.md "wikilink")
  - [GR 211](../Page/GR_211.md "wikilink")
  - [GR 212](../Page/GR_212.md "wikilink")
  - [GR 223](../Page/GR_223.md "wikilink")
  - [GR 341](../Page/GR_341.md "wikilink")
  - [GR 412](../Page/GR_412.md "wikilink")
  - [GR 380](../Page/GR_380.md "wikilink")
  - [GR 512](../Page/GR_512.md "wikilink")
  - [GR 561](../Page/GR_561.md "wikilink")
  - [GR 563](../Page/GR_563.md "wikilink")
  - [GR 564](../Page/GR_564.md "wikilink")
  - [GR 565](../Page/GR_565.md "wikilink")
  - [GR 571](../Page/GR_571.md "wikilink")
  - [GR 573](../Page/GR_573.md "wikilink")
  - [GR 575](../Page/GR_575.md "wikilink")
  - [GR 576](../Page/GR_576.md "wikilink")
  - [GR 577](../Page/GR_577.md "wikilink"): Tour de la Famenne
  - [GR 579](../Page/GR_579.md "wikilink"):
    [Liège](../Page/Liège_\(city\).md "wikilink") -
    [布魯塞爾](../Page/布魯塞爾.md "wikilink")
  - [GR 652](../Page/GR_652.md "wikilink")

## 西班牙段

  - [GR 7](../Page/GR_7.md "wikilink"): Algeciras - Andorra
  - [GR-71](../Page/GR-71.md "wikilink"):
    [Corconte](../Page/Corconte.md "wikilink")
    ([坎塔布里亚](../Page/坎塔布里亚.md "wikilink")) -
    [Sotres](../Page/Sotres.md "wikilink")
    ([阿斯图里亚斯](../Page/阿斯图里亚斯.md "wikilink"))
  - [GR-72](../Page/GR-72.md "wikilink"): [Santillana del
    Mar](../Page/Santillana_del_Mar.md "wikilink") -
    [Reinosa](../Page/Reinosa.md "wikilink")
    ([坎塔布里亚](../Page/坎塔布里亚.md "wikilink")).
  - [GR-73](../Page/GR-73.md "wikilink"): [Herrera de
    Pisuerga](../Page/Herrera_de_Pisuerga.md "wikilink")
    ([Palencia](../Page/Palencia.md "wikilink")) -
    [Suances](../Page/Suances.md "wikilink")([坎塔布里亚](../Page/坎塔布里亚.md "wikilink"))
  - [GR-74](../Page/GR-74.md "wikilink"): [Ramales de la
    Victoria](../Page/Ramales_de_la_Victoria.md "wikilink") -
    [Reinosa](../Page/Reinosa.md "wikilink")
    ([坎塔布里亚](../Page/坎塔布里亚.md "wikilink")).
  - [GR 142](../Page/GR_142.md "wikilink"): Alpujarras route

## 外部链接

  - ,[在法国和比利时长途远足(GR)路线，行程，图片，住宿和信息](http://www.gr-infos.com/gr-cn.htm)

  - ,[Fédération Française de la Randonnée
    Pédestre](http://www.ffrandonnee.fr/)

  - ,[Website with Belgian GR-routes](http://www.grsentiers.org/)

  - ,[Federacion espanola de deportes de montaña y
    escalada](https://web.archive.org/web/20060503051537/http://www.fedme.es/FEDME/Senderos/principal.asp)

  - ,[Map with the Spanish
    GRs](https://web.archive.org/web/20060516213617/http://www.fedme.es/FEDME/Senderos/mapaEspana.htm)

  - ,[Grote Routepaden in
    Belgium](https://web.archive.org/web/20060903224059/http://www.groteroutepaden.be/grlf/)

[category:歐洲遠足徑](../Page/category:歐洲遠足徑.md "wikilink")