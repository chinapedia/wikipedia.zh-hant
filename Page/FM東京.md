[Tokyo_fm_headquarter.jpg](https://zh.wikipedia.org/wiki/File:Tokyo_fm_headquarter.jpg "fig:Tokyo_fm_headquarter.jpg")[千代田區](../Page/千代田區.md "wikilink")[麴町](../Page/麴町.md "wikilink")（[半藏門](../Page/半藏門.md "wikilink")）的FM東京總部大樓。\]\]
[TOKYO_FM_Spainzaka_studio.jpg](https://zh.wikipedia.org/wiki/File:TOKYO_FM_Spainzaka_studio.jpg "fig:TOKYO_FM_Spainzaka_studio.jpg")的FM東京[西班牙坂錄音室](../Page/西班牙坂.md "wikilink")。攝於2010年1月11日。2016年8月7日起暫時關閉，預計2019年重新啟用。\]\]

**FM東京**（；，常縮寫為「TFM」）是[日本一家](../Page/日本.md "wikilink")[民營](../Page/商業廣播.md "wikilink")[廣播電台](../Page/廣播電台.md "wikilink")，以[東京都為廣播範圍](../Page/東京都.md "wikilink")，為[JFN聯播網的](../Page/JFN.md "wikilink")[核心局](../Page/核心局.md "wikilink")。開播於1970年4月26日，是日本第三個以身份設立的[FM廣播電台](../Page/FM廣播.md "wikilink")，次於及。目前為其最大股東。

## 頻率

  - [東京都心](../Page/東京都心.md "wikilink")（透過[東京鐵塔發射訊號](../Page/東京鐵塔.md "wikilink")）：80.0MHz
  - [新島](../Page/新島.md "wikilink")：76.7MHz
  - [八王子](../Page/八王子.md "wikilink")：80.5MHz
  - [東京都西部及周圍諸縣](../Page/東京都.md "wikilink")：86.6MHz

## 外部連結

  - [TOKYO FM](https://www.tfm.co.jp/)
  - [JAPAN FM NETWORK](http://www.jfn.co.jp/)

[FM東京](../Category/FM東京.md "wikilink")
[Category:日本广播电台](../Category/日本广播电台.md "wikilink")
[Category:1970年開播的電台](../Category/1970年開播的電台.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:麴町](../Category/麴町.md "wikilink")