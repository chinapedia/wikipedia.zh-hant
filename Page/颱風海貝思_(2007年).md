**颱風海貝思**
（[英語](../Page/英語.md "wikilink")：****，菲律賓大氣地理天文部門：**Lando**，國際編號：**0724**，[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")：**23W**）是[2007年太平洋颱風季的一個](../Page/2007年太平洋颱風季.md "wikilink")[熱帶氣旋](../Page/熱帶氣旋.md "wikilink")。

「海貝思」是[菲律賓於](../Page/菲律賓.md "wikilink")[世界氣象組織颱風委員會為西太平洋熱帶氣旋提供的名字](../Page/世界氣象組織.md "wikilink")（不同於菲律賓氣象單位自行命名），其意為「快捷」。

## 路徑

11月18日，一個[熱帶低氣壓於菲律賓南部形成](../Page/熱帶低氣壓.md "wikilink")，初時向西移動，翌日登陸[菲律賓](../Page/菲律賓.md "wikilink")[萊特島南部](../Page/萊特島.md "wikilink")。11月20日，該熱帶低壓增強為[熱帶風暴](../Page/熱帶風暴.md "wikilink")，並被命名為海貝思，它在當晚迅速增強為[強烈熱帶風暴](../Page/強烈熱帶風暴.md "wikilink")，進入[南海](../Page/南海.md "wikilink")。11月23日午夜海貝思增強為[颱風](../Page/颱風.md "wikilink")，並達到顛峰，中心風力每小時130公里。

當晚，海貝思受東面的[颱風米娜影響而開始減弱及在](../Page/颱風米娜.md "wikilink")[南海中部徘徊](../Page/南海.md "wikilink")，翌日清晨減弱為強烈熱帶風暴轉向東移動，再度趨近菲律賓。[11月25日](../Page/11月25日.md "wikilink")，海貝思進一步減弱為一個熱帶風暴，[11月27日於菲律賓](../Page/11月27日.md "wikilink")[民都洛島登陸](../Page/民都洛島.md "wikilink")。翌日早上減弱為熱帶低氣壓，當日下午進一步減弱為[低壓區](../Page/低壓區.md "wikilink")。

## 影響

###

海貝思吹襲菲律賓時，造成10人喪生，由於海貝思兩度吹襲菲律賓時強度都較弱，損害較少，但受[颱風米娜影響](../Page/颱風米娜.md "wikilink")，造成大規模損害。

### [南沙海域](../Page/南沙.md "wikilink")

颱風海貝思在南海中部徘徊時，[中國](../Page/中國.md "wikilink")、菲律賓、[越南的](../Page/越南.md "wikilink")300多名漁民被困中國[南沙海域](../Page/南沙.md "wikilink")，當中一艘菲律賓漁船在南海沉沒，有25名船員失蹤。

2007年11月22日凌晨4時，當時強度為[強烈熱帶風暴的海貝思襲擊](../Page/強烈熱帶風暴.md "wikilink")[美濟礁](../Page/美濟礁.md "wikilink")，礁內養殖試驗漁排上的7名漁工和瓊澤漁820船上5名漁民落海。黃東東、林紹明和林聖平等3名緊抱飄浮物、堅持七天的漁工被外國貨船救起；王國雄、王高明、邱小亮、桂良明、王國永、黃永進、陳金龍、陳明發、林韶劇等9人失蹤遇難。在美濟礁的第一次生產性漁業養殖全軍覆沒。2009年5月30日「中國南沙美濟礁養殖遇難者紀念碑」在美濟礁上建成。

## 圖片

Hagibis 23 nov 2007 0213Z.jpg|11月23日 Mitag and Hagibis 25 nov 2007
0245Z.jpg|11月25日的海貝思（左）、[米娜](../Page/颱風米娜_\(2007年\).md "wikilink")（右）

## 外部連結

  - <https://web.archive.org/web/20090826230250/http://metocph.nmci.navy.mil/jtwc.php>
    [聯合颱風警報中心首頁](../Page/聯合颱風警報中心.md "wikilink")

  - <http://www.jma.go.jp/jma/index.html>
    [日本氣象廳首頁](../Page/日本氣象廳.md "wikilink")

  - <http://www.cwb.gov.tw> [中央氣象局首頁](../Page/中央氣象局.md "wikilink")

  - <http://www.pagasa.dost.gov.ph/>
    [菲律賓大氣地球物理和天文管理局首頁](../Page/菲律賓大氣地球物理和天文管理局.md "wikilink")

[Category:2007年太平洋台风季](../Category/2007年太平洋台风季.md "wikilink")
[台](../Category/一级热带气旋.md "wikilink")