## 大事记

## 出生

### 1月

  - [1月12日](../Page/1月12日.md "wikilink")——[安瑞賢](../Page/安瑞賢.md "wikilink")，韓國童星、女演員
  - [1月15日](../Page/1月15日.md "wikilink")——李英恩(이영은)，童星、[韓國女演員](../Page/韓國.md "wikilink")
  - [1月21日](../Page/1月21日.md "wikilink")－[英格麗·亞莉珊德拉](../Page/英格丽·亚莉珊德拉_\(挪威\).md "wikilink")，[挪威公主](../Page/挪威.md "wikilink")

### 2月

  - [2月7日](../Page/2月7日.md "wikilink")——陳璽達，TF家族
  - [2月9日](../Page/2月9日.md "wikilink")——懷特米雪兒，[偶像學校](../Page/偶像學校.md "wikilink")
  - [2月12日](../Page/2月12日.md "wikilink")——陳泗旭，TF家族
  - [2月13日](../Page/2月13日.md "wikilink")——柳翰庇，韓國女演員

### 3月

  - [3月4日](../Page/3月4日.md "wikilink")——宋亞軒，中國男子團體[颱風少年團成員](../Page/颱風少年團.md "wikilink")
  - [3月9日](../Page/3月9日.md "wikilink")——劉恩美(유은미)，[韓國女演員](../Page/韓國.md "wikilink")

### 4月

  - [4月4日](../Page/4月4日.md "wikilink")——李天澤，TF家族
  - [4月6日](../Page/4月6日.md "wikilink")——[李柔蓁](../Page/李柔蓁.md "wikilink")，[台灣童星](../Page/台灣.md "wikilink")，女演員
  - [4月8日](../Page/4月8日.md "wikilink")——李珠賢，[The
    Unit](../Page/The_Unit.md "wikilink")

### 5月

  - [5月25日](../Page/5月25日.md "wikilink")——朴宣，[偶像學校](../Page/偶像學校.md "wikilink")

### 6月

  - [6月14日](../Page/6月14日.md "wikilink")——李宥姃，[PRODUCE
    48](../Page/PRODUCE_48.md "wikilink")
  - [6月15日](../Page/6月15日.md "wikilink")——賀峻霖，TF家族

### 7月

  - [7月30日](../Page/7月30日.md "wikilink")——欧阳娣娣

### 8月

  - [8月31日](../Page/8月31日.md "wikilink")——[張元英](../Page/張元英.md "wikilink")，韓國女子團體[IZONE成員](../Page/IZONE.md "wikilink")

### 9月

  - [9月13日](../Page/9月13日.md "wikilink")——李真宇，[PRODUCE X
    101](../Page/PRODUCE_X_101.md "wikilink")

### 10月

  - [10月30日](../Page/10月30日.md "wikilink")——李河恩，[PRODUCE
    48](../Page/PRODUCE_48.md "wikilink")

### 11月

  - [11月16日](../Page/11月16日.md "wikilink")——May，韓國女子團體[Cherry
    Bullet成員](../Page/Cherry_Bullet.md "wikilink")

## 逝世

## [诺贝尔奖](../Page/诺贝尔奖.md "wikilink")

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：
      - [戴维·格罗斯](../Page/戴维·格罗斯.md "wikilink")（[美国](../Page/美国.md "wikilink")）、[休·波利策](../Page/休·波利策.md "wikilink")（美国）和[弗朗克·韋爾切克](../Page/弗朗克·韋爾切克.md "wikilink")（美国），
          - 發現[強交互作用理論中的](../Page/強交互作用.md "wikilink")[漸近自由](../Page/漸近自由.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：
      - [阿龍·切哈諾沃](../Page/阿龍·切哈諾沃.md "wikilink")（[以色列](../Page/以色列.md "wikilink")）、[阿夫拉姆·赫什科](../Page/阿夫拉姆·赫什科.md "wikilink")（以色列）和[欧文·罗斯](../Page/欧文·罗斯.md "wikilink")（美国）
          - 对[泛素为媒介的](../Page/泛素.md "wikilink")[蛋白质降解过程的发现](../Page/蛋白质.md "wikilink")
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：
      - [理查德·阿克塞尔](../Page/理查德·阿克塞尔.md "wikilink")（美國）和[琳达·巴克](../Page/琳达·巴克.md "wikilink")（美國）
          - 发现了[气味受体和](../Page/气味.md "wikilink")[嗅觉系统的组织方式](../Page/嗅觉.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：
      - [耶利内克](../Page/耶利内克.md "wikilink")（[奥地利](../Page/奥地利.md "wikilink")）
          - [小说和](../Page/小说.md "wikilink")[戏剧具有](../Page/戏剧.md "wikilink")[音乐般的韵律](../Page/音乐.md "wikilink")，充满激情的语言揭示了[社会上陈腐现象](../Page/社会.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：
      - [旺加里·马塔伊](../Page/旺加里·马塔伊.md "wikilink")（[肯尼亚](../Page/肯尼亚.md "wikilink")）
          - 在[可持续发展](../Page/可持续发展.md "wikilink")、[民主与](../Page/民主.md "wikilink")[和平方面作出的贡献](../Page/和平.md "wikilink")
  - [经济](../Page/诺贝尔经济学奖.md "wikilink")：
      - [芬恩·基德兰德](../Page/芬恩·基德兰德.md "wikilink")（[挪威](../Page/挪威.md "wikilink")）和[爱德华·普雷斯科特](../Page/爱德华·普雷斯科特.md "wikilink")（美国）
          - 他们的理论不仅广泛运用于经济分析，还影响了许多国家的[货币政策](../Page/货币政策.md "wikilink")

## [奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")

（第77屆，[2005年頒發](../Page/2005年.md "wikilink")）

  - [奧斯卡最佳影片獎](../Page/奧斯卡最佳影片獎.md "wikilink")——《[-{zh-hans:百万美元宝贝;zh-hk:擊情;zh-tw:登峰造擊;}-](../Page/百万美元宝贝.md "wikilink")》
  - [奧斯卡最佳导演獎](../Page/奧斯卡最佳导演獎.md "wikilink")——
    [-{zh-hans:克林特·伊斯特伍德;zh-hk:奇連·伊士活;zh-tw:克林·伊斯威特;}-](../Page/克林特·伊斯特伍德.md "wikilink")（《-{zh-hans:百万美元宝贝;zh-hk:擊情;zh-tw:登峰造擊;}-》）
  - [奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")——[-{zh-hans:杰米·福克斯;zh-hk:占美·霍士;zh-tw:傑米·福克斯;}-](../Page/杰米·福克斯.md "wikilink")（《[-{zh-hans:灵魂歌王;zh-hk:Ray;zh-tw:雷之心靈傳奇;}-](../Page/灵魂歌王.md "wikilink")》）
  - [奧斯卡最佳女主角獎](../Page/奧斯卡最佳女主角獎.md "wikilink")——[-{zh-hans:希拉里·斯万克;zh-hk:希拉莉·詩韻;zh-tw:希拉蕊·史旺;}-](../Page/希拉里·斯万克.md "wikilink")（《-{zh-hans:百万美元宝贝;zh-hk:擊情;zh-tw:登峰造擊;}-》）
  - [奧斯卡最佳男配角獎](../Page/奧斯卡最佳男配角獎.md "wikilink")——[-{zh-hans:摩根·弗里曼;
    zh-hk:摩根·費曼;}-](../Page/摩根·弗里曼.md "wikilink")（《-{zh-hans:百万美元宝贝;zh-hk:擊情;zh-tw:登峰造擊;}-》）
  - [奧斯卡最佳女配角獎](../Page/奧斯卡最佳女配角獎.md "wikilink")——[-{zh-hans:凯特·布兰切特;zh-hk:姬蒂·白蘭芝;zh-tw:凱特·布蘭琪;}-](../Page/凯特·布兰切特.md "wikilink")（《[-{zh-hans:飞行者;zh-hk:娛樂大亨;zh-tw:神鬼玩家;}-](../Page/飞行者.md "wikilink")》）

## 世界足球先生

<table>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>球员</p></th>
<th><p>得分</p></th>
<th><p>所在球队</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>|1</p></td>
<td><p><a href="../Page/罗纳尔迪尼奥.md" title="wikilink">-{zh-hans:小罗纳尔多; zh-hk:朗拿甸奴;}-</a></p></td>
<td><p>620</p></td>
<td><p><a href="../Page/巴塞羅那足球會.md" title="wikilink">巴塞羅那</a></p></td>
</tr>
<tr class="even">
<td><p>|2</p></td>
<td><p><a href="../Page/蒂埃里·亨利.md" title="wikilink">蒂埃里·亨利</a></p></td>
<td><p>552</p></td>
<td><p><a href="../Page/阿仙奴.md" title="wikilink">阿仙奴</a></p></td>
</tr>
<tr class="odd">
<td><p>|3</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg" title="fig:Flag_of_Ukraine.svg">Flag_of_Ukraine.svg</a> <a href="../Page/安德烈·舍甫琴科.md" title="wikilink">-{zh-hans:安德烈·舍甫琴科; zh-hk:舒夫真高;}-</a></p></td>
<td><p>253</p></td>
<td><p><a href="../Page/AC米蘭.md" title="wikilink">AC米蘭</a></p></td>
</tr>
</tbody>
</table>

[\*](../Category/2004年.md "wikilink")
[4年](../Category/2000年代.md "wikilink")
[0](../Category/21世纪各年.md "wikilink")