**弗雷德里克**（）是位于[美国](../Page/美国.md "wikilink")[马里兰州](../Page/马里兰州.md "wikilink")[弗雷德里克县的一座城市](../Page/弗雷德里克县_\(马里兰州\).md "wikilink")，也是该县的县治所在。弗雷德里克是仅次于[巴尔的摩和](../Page/巴尔的摩_\(马里兰州\).md "wikilink")[罗克维尔的马里兰州第三大城市](../Page/罗克维尔_\(马里兰州\).md "wikilink")。根据[美国人口调查局](../Page/美国人口调查局.md "wikilink")2000年统计，共有人口52,767人，其中[白人占](../Page/白人.md "wikilink")77.04%、[非裔美国人占](../Page/非裔美国人.md "wikilink")14.74%、[亚裔美国人占](../Page/亚裔美国人.md "wikilink")3.15%。2006年估計人口為58,882
人。\[1\]

## 参考文献

[F](../Category/马里兰州城市.md "wikilink")

1.  [Frederick city, Maryland - Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=ChangeGeoContext&geo_id=16000US2430325&_geoContext=01000US%7C04000US24%7C16000US2431175&_street=&_county=Frederick&_cityTown=Frederick&_state=04000US24&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=010)