**岸上红娘鱼**（[学名](../Page/学名.md "wikilink")：）为[鲂鮄科](../Page/鲂鮄科.md "wikilink")[红娘鱼属的](../Page/红娘鱼属.md "wikilink")[鱼类](../Page/鱼类.md "wikilink")，又稱**岸上氏角魚**、**尖鰭紅娘魚**。分布于[日本以及中国](../Page/日本.md "wikilink")[东海](../Page/东海.md "wikilink")、广东沿岸等海域，属于底层海鱼。该物种的模式产地在Kagoshima。\[1\]

## 分布

本魚分布於[日本](../Page/日本.md "wikilink")、[琉球及](../Page/琉球.md "wikilink")[台灣等海域](../Page/台灣.md "wikilink")。

## 深度

可達水深100公尺以上的深海。

## 特徵

大致與[日本紅娘魚同](../Page/日本紅娘魚.md "wikilink")，但胸鰭較短，最長的游離軟條不達腹鰭後端，尾鰭為向內凹，體呈[紅色](../Page/紅色.md "wikilink")，胸鰭內側上半[黑色](../Page/黑色.md "wikilink")，帶有[白色小斑點](../Page/白色.md "wikilink")；下半部有一[暗藍色的大斑塊](../Page/暗藍色.md "wikilink")。

## 生態

本魚棲息於深海沙泥底海域，利用胸鰭下方的游離鰭條搜索捕食。

## 經濟利用

可食，但多充作下雜魚、[魚粉](../Page/魚粉.md "wikilink")。

## 参考文献

  -
## 扩展阅读

[kishinouyi](../Category/红娘鱼属.md "wikilink")

1.