**丝粉藻科**共有5[属](../Page/属.md "wikilink")16[种](../Page/种.md "wikilink")，都是生活在暖[温带到](../Page/温带.md "wikilink")[热带海洋中的咸水生](../Page/热带.md "wikilink")[植物](../Page/植物.md "wikilink")，在[澳洲附近尤其繁盛](../Page/澳洲.md "wikilink")。[中国南方海域有](../Page/中国.md "wikilink")3属几种分布。

## 分類

1981年的[克朗奎斯特分类法将其列入](../Page/克朗奎斯特分类法.md "wikilink")[茨藻目](../Page/茨藻目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该归入](../Page/APG_分类法.md "wikilink")[泽泻目](../Page/泽泻目.md "wikilink")。

## 属

  - [根枝草属](../Page/根枝草属.md "wikilink") *Amphibolis* C.Agardh（同
    *Pectinella* J.M.Black）
  - [丝粉藻属](../Page/丝粉藻属.md "wikilink") *Cymodocea* K.Koenig
  - [二药藻属](../Page/二药藻属.md "wikilink") *Halodule* Endl.
  - [针叶藻属](../Page/针叶藻属.md "wikilink") *Syringodium* Kutz.（同
    *Phycoschoenus* Asch. Nakai）
  - [全楔草属](../Page/全楔草属.md "wikilink") *Thalassodendron* Hartog

## 外部链接

  - 在[L. Watson和M.J.
    Dallwitz（1992年）《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[丝粉藻科](http://delta-intkey.com/angio/www/Cymodoce.htm)
  - [e-flora中的丝粉藻科](http://www.efloras.org/florataxon.aspx?flora_id=1200&taxon_id=10244)
  - [《北美植物》中的丝粉藻科](http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=10244)
  - [*Syringodium
    filiforme*](https://web.archive.org/web/20060713230510/http://www.floridaoceanographic.org/environ/seagrass2.htm)
  - [NCBI中的丝粉藻科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=25926&lvl=3&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL中的丝粉藻科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Cymodoceaceae)

[\*](../Category/丝粉藻科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")