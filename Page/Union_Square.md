[Union_Square_Overview_201008.jpg](https://zh.wikipedia.org/wiki/File:Union_Square_Overview_201008.jpg "fig:Union_Square_Overview_201008.jpg")上蓋的Union
Square\]\]
[Union_square.svg](https://zh.wikipedia.org/wiki/File:Union_square.svg "fig:Union_square.svg")
[Union_SquarePlaza_20070922.jpg](https://zh.wikipedia.org/wiki/File:Union_SquarePlaza_20070922.jpg "fig:Union_SquarePlaza_20070922.jpg")
[UnionSqaureview1_20070922.jpg](https://zh.wikipedia.org/wiki/File:UnionSqaureview1_20070922.jpg "fig:UnionSqaureview1_20070922.jpg")

**Union
Square**位於[香港](../Page/香港.md "wikilink")[西九龍](../Page/西九龍.md "wikilink")[柯士甸道西](../Page/柯士甸道西.md "wikilink")1號[港鐵](../Page/港鐵.md "wikilink")[九龍站上蓋](../Page/九龍站_\(港鐵\).md "wikilink")，佔地13.54[公頃](../Page/公頃.md "wikilink")，分別被[佐敦道](../Page/佐敦道.md "wikilink")、[連翔道](../Page/連翔道.md "wikilink")、[柯士甸道西及](../Page/柯士甸道西.md "wikilink")[雅翔道](../Page/雅翔道.md "wikilink")4條道路包圍。Union
Square為港鐵九龍站一個綜合商住發展計劃，由[港鐵公司聯同](../Page/港鐵公司.md "wikilink")[九龍倉集團有限公司](../Page/九龍倉集團.md "wikilink")、[恒隆地產有限公司](../Page/恒隆地產.md "wikilink")、[永泰控股有限公司及](../Page/永泰控股.md "wikilink")[新鴻基地產發展有限公司合作發展](../Page/新鴻基地產.md "wikilink")。

## 簡介

Union
Square總面積達109萬[平方米](../Page/平方米.md "wikilink")，整個發展計劃分為7個發展項目組合。第一期發展項目（[漾日居](../Page/漾日居.md "wikilink")）、第二期發展項目（[擎天半島](../Page/擎天半島.md "wikilink")）、第三期發展項目（[凱旋門](../Page/凱旋門_\(香港\).md "wikilink")）、第四期發展項目（[君臨天下](../Page/君臨天下.md "wikilink")）、第五、六及七期發展項目組合（[圓方](../Page/圓方.md "wikilink")）、（[天璽](../Page/天璽_\(香港\).md "wikilink")）及[環球貿易廣場於](../Page/環球貿易廣場.md "wikilink")2000年至2011年初分階段落成及入伙。

## 設施

Union
Square是一個綜合發展計劃，集合[高尚住宅](../Page/高尚住宅.md "wikilink")、[寫字樓](../Page/寫字樓.md "wikilink")、[商場](../Page/商場.md "wikilink")、[觀景台和](../Page/觀景台.md "wikilink")[酒店設施於一體](../Page/酒店.md "wikilink")，當中包括：

  - 18座[住宅大廈及](../Page/住宅.md "wikilink")2座作混合用途的建築（包括住宅、[酒店及](../Page/酒店.md "wikilink")[服務式住宅](../Page/服務式住宅.md "wikilink")），合共提供約5866伙住宅單位、一間豪華酒店及約72472平方米服務式住宅；
  - 一幢118層高的[環球貿易廣場](../Page/環球貿易廣場.md "wikilink")，包括約231778平方米的甲級寫字樓及[香港麗思卡爾頓酒店](../Page/香港麗思卡爾頓酒店.md "wikilink")，並附設[天際100觀景台](../Page/天際100.md "wikilink")；
  - 面積達100萬平方呎的大型購物商場：[圓方](../Page/圓方.md "wikilink")；
  - 平台有一所1,050平方米的栢基國際幼稚園（九龍）；
  - [巴士](../Page/巴士.md "wikilink")、[小巴](../Page/小巴.md "wikilink")、[的士的公共交通總站](../Page/的士.md "wikilink")、酒店穿梭巴士、旅遊巴士、過境巴士及私家車交通交匯處；
  - 約5,600個車位；
  - 頂層平台設佔地70萬呎廣闊的公共空間及私人休憩綠化區，公眾部份設多種主題園景地帶亦設少量康樂設施，包括兒童遊樂場及多用途運動場。而平台亦設由港鐵管理的「九龍站平台體育館」，設乒乓球室。但公眾只能從電話預約設施，而其隱閉的入口令公眾以為是幼稚園的一部分。\[1\]

<File:Union> Square Garden View 201302.jpg|魏香園 <File:Union> Square
Landscape Garden 2007.jpg|主題園景地帶 <File:Union> Square Fountain
201302.jpg|水池 <File:Union> Square Children Playground 2013.jpg|兒童遊樂場
<File:Elements> Civic Square 2010.jpg|位於天台的演薈廣場

## 七個發展項目組合資料

### 第一期

[201901_The_Waterfront_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:201901_The_Waterfront_Hong_Kong.jpg "fig:201901_The_Waterfront_Hong_Kong.jpg")\]\]

  - 名稱：[漾日居](../Page/漾日居.md "wikilink")
  - 發展財團成員：
      - [永泰控股有限公司](../Page/永泰控股.md "wikilink")
      - [淡馬錫控股有限公司](../Page/淡馬錫控股.md "wikilink")
      - [新加坡置地有限公司](../Page/新加坡置地.md "wikilink")
      - [吉寶置業有限公司](../Page/吉寶置業.md "wikilink")
      - [麗新發展有限公司](../Page/麗新發展.md "wikilink")
      - [環球投資](../Page/環球投資.md "wikilink")（百慕達）有限公司
  - 用途：[住宅](../Page/住宅.md "wikilink")
  - 樓宇幢數：6 (Tower 9, 10, 11, 12, 13, 14)
  - 公共設施及公眾休憩用地的資料：
  - 公共交通總站(G/F)︰[九龍站公共運輸交匯處](../Page/九龍站公共運輸交匯處.md "wikilink")
  - 日間托兒所(1/F 104室)︰現址為仁愛堂陳鄭玉而幼稚園暨幼兒園
  - 長者聯誼中心(1/F 103室)︰現址為循道衛理楊震社會服務處油尖長者鄰舍中心
  - 家庭支援中心(1/F 102室)︰現址為香港復康力量賽馬會漾日居中心(總辦事處)
  - 及青少年中心(1/F 101室)︰現址為香港中華基督教青年會佐敦會所賽馬會綜合青少年服務中心
  - 單位數目：1288
  - 單位面積：約77-232[平方米](../Page/平方米.md "wikilink")
  - 車位數目：1332
  - 狀況：已入伙

### 第二期

[Sorrento.jpg](https://zh.wikipedia.org/wiki/File:Sorrento.jpg "fig:Sorrento.jpg")\]\]

  - 名稱：[擎天半島](../Page/擎天半島.md "wikilink")
  - 發展財團成員：
      - [九龍倉集團有限公司](../Page/九龍倉集團.md "wikilink")
      - [會德豐有限公司](../Page/會德豐.md "wikilink")
      - [新亞置業信託有限公司](../Page/新亞置業信託.md "wikilink")
      - [聯邦地產有限公司](../Page/聯邦地產.md "wikilink")
      - [海港企業有限公司](../Page/海港企業.md "wikilink")
  - 用途：[住宅](../Page/住宅.md "wikilink")
  - 樓宇幢數：5 (Tower 15, 16, 17, 18, 19)
  - 單位數目：2126
  - 單位面積：約77-252[平方米](../Page/平方米.md "wikilink")
  - 車位數目：1270
  - 狀況：已入伙

### 第三期

[HK_The_Arch_Overview.jpg](https://zh.wikipedia.org/wiki/File:HK_The_Arch_Overview.jpg "fig:HK_The_Arch_Overview.jpg")\]\]

  - 名稱：[凱旋門](../Page/凱旋門_\(香港\).md "wikilink")
  - 發展財團成員：
      - [新鴻基地產發展有限公司](../Page/新鴻基地產.md "wikilink")
  - 用途：[住宅](../Page/住宅.md "wikilink")
  - 樓宇幢數：4 (Tower 7, 8)
  - 建築面積：100000[平方米](../Page/平方米.md "wikilink")
  - 公共設施及公眾休憩用地的資料：園景區
  - 狀況：已入伙

### 第四期

[Harbourside.jpg](https://zh.wikipedia.org/wiki/File:Harbourside.jpg "fig:Harbourside.jpg")\]\]

  - 名稱：[君臨天下](../Page/君臨天下_\(香港\).md "wikilink")
  - 發展財團成員：
      - [恒隆地產有限公司](../Page/恒隆地產.md "wikilink")
  - 用途：[住宅](../Page/住宅.md "wikilink")
  - 樓宇幢數：3 (Tower 4, 5, 6)
  - 單位數目：1122
  - 單位面積：約95-271[平方米](../Page/平方米.md "wikilink")
  - 車位數目：864
  - 狀況：已入伙

### 第五至第七期

第五至第七期為一個綜合發展項目，由[新鴻基地產發展有限公司全資發展](../Page/新鴻基地產.md "wikilink")。包括甲級寫字樓、兩間六星級[酒店](../Page/酒店.md "wikilink")、[豪華住宅](../Page/豪華住宅.md "wikilink")、[服務式住宅及一個](../Page/服務式住宅.md "wikilink")[商場](../Page/商場.md "wikilink")，總樓面面積約550萬[平方呎](../Page/平方呎.md "wikilink")，於2007年底至2011年初分期落成。

  - 第五期：[圓方](../Page/圓方.md "wikilink")
      - 組成：Union Square基座
      - 用途：商場
      - 面積：約100萬[平方呎](../Page/平方呎.md "wikilink")
      - 公共設施及公眾休憩用地的資料：園景區，公眾康樂設施
      - 狀況：已開幕
      - 落成年份：2007年10月

<!-- end list -->

  - 第六期：[天璽](../Page/天璽_\(香港\).md "wikilink")
      - 組成：兩座約270米，高62層的雙子大廈 (Tower 20, 21)
      - 用途：五星級[酒店](../Page/酒店.md "wikilink")（[W
        Hotel](../Page/W_Hotel.md "wikilink")）、服務式出租[住宅](../Page/住宅.md "wikilink")（[港景匯](../Page/港景匯.md "wikilink")）及豪華[住宅](../Page/住宅.md "wikilink")
      - 面積：200萬[平方呎](../Page/平方呎.md "wikilink")
      - 特色：其中一幢是全[香港最高的純](../Page/香港.md "wikilink")[住宅建築物](../Page/住宅.md "wikilink")。
      - 狀況：已開幕 (港景匯及[香港W酒店](../Page/香港W酒店.md "wikilink"))、已入-{伙}-(豪宅部分)
      - 落成年份：2008年至2009年

<!-- end list -->

  - 第七期：[環球貿易廣場](../Page/環球貿易廣場.md "wikilink")
      - 組成：一幢118層高的摩天大樓 (Tower 1)
      - 用途：甲級寫字樓、[天際100觀景台及](../Page/天際100.md "wikilink")[香港麗思卡爾頓酒店](../Page/香港麗思卡爾頓酒店.md "wikilink")
      - 面積：2,822,039 [平方呎](../Page/平方呎.md "wikilink")（262,176
        [平方米](../Page/平方米.md "wikilink")）
      - 特色：落成時是全世界第三高、中港地區最高的摩天大樓，至2018年8月為全世界第十五高、中港地區第九高的摩天大樓。
      - 狀況：已開幕
      - 落成年份：2011年初

<File:ElementsVOIDview1> 20071001.jpg|[圓方](../Page/圓方.md "wikilink")
<File:Union> SquarePlase5
20070922.jpg|[天璽](../Page/天璽_\(香港\).md "wikilink") <File:W>
Hotel Hong Kong Ground Floor
Lobby.jpg|[香港W酒店](../Page/香港W酒店.md "wikilink")
<File:International> Commerce Centre
201008.jpg|[環球貿易廣場](../Page/環球貿易廣場.md "wikilink")

## 特色

  - Union Square內大部分建築物都高於200米，組成一個摩天大樓建築群。
  - Union
    Square內設有甲級寫字樓、商場、住宅及酒店等設施，提供工作、居住、消閒及娛樂的環境，構成一個多元化的獨立社區，是一個規模龐大的綜合商住發展計劃。
  - Union
    Square發展完成後，成為[香港的新地標](../Page/香港.md "wikilink")，並進一步鞏固[香港作為國際大都會的地位](../Page/香港.md "wikilink")。

## 影響

[Union_Square_Overview_201205.jpg](https://zh.wikipedia.org/wiki/File:Union_Square_Overview_201205.jpg "fig:Union_Square_Overview_201205.jpg")

### 正面

[DSC_4117_VIC.JPG](https://zh.wikipedia.org/wiki/File:DSC_4117_VIC.JPG "fig:DSC_4117_VIC.JPG")」的構思。左方為中環國際金融中心二期，右方為九龍環球貿易廣場\]\]
Union
Square發展完成後，成為[香港的新地標](../Page/香港.md "wikilink")，吸引外地遊客和[投資者](../Page/投資者.md "wikilink")，從而提升[香港旅遊業及國際](../Page/香港旅遊業.md "wikilink")[競爭力](../Page/競爭力.md "wikilink")，其中環球貿易廣場與維多利亞港對岸[中環的](../Page/中環.md "wikilink")[國際金融中心二期形成統稱](../Page/國際金融中心_\(香港\).md "wikilink")「[維港門廊](../Page/維港門廊.md "wikilink")」[摩天大樓](../Page/摩天大樓.md "wikilink")，構成一道獨特的海港摩天建築風景線。而發展計劃把[九龍站打造成另一個如對岸中環般的](../Page/九龍站.md "wikilink")[核心商業區](../Page/核心商業區.md "wikilink")，把商業活動由港島區伸延到九龍。項目亦可以把早已飽和的九龍中部及東部地帶的人流分散到[西九龍的新填海區](../Page/西九龍.md "wikilink")，舒緩九龍中部及東部的擠迫情況。

### 負面

Union
Square位於[西九龍海濱](../Page/西九龍.md "wikilink")，樓宇建得甚高，阻礙西九龍內陸地區的[維多利亞港景觀](../Page/維多利亞港.md "wikilink")，亦使維多利亞港對岸的視野受阻，該項目亦採用密度高和[蛋糕式平台設計](../Page/蛋糕樓.md "wikilink")，造成[屏風效應](../Page/屏風效應.md "wikilink")，阻擋海風進入內陸，使西九龍內陸地區的空氣質素惡化。
Union Square的樓宇吸引大量[大陸人購入](../Page/大陸人.md "wikilink")，使區內樓價持續上升。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{東涌綫色彩}}">█</font>[東涌綫](../Page/東涌綫.md "wikilink")、<font color={{機場快綫色彩}}>█</font>[機場快綫](../Page/機場快綫.md "wikilink")：[九龍站](../Page/九龍站_\(港鐵\).md "wikilink")
  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[柯士甸站](../Page/柯士甸站.md "wikilink")
  - <font color="{{高速鐵路色彩}}">█</font>[廣深港高速鐵路](../Page/廣深港高速鐵路.md "wikilink")：[香港西九龍站](../Page/香港西九龍站.md "wikilink")

<!-- end list -->

  - [西區海底隧道收費廣場](../Page/西區海底隧道收費廣場.md "wikilink")

<!-- end list -->

  - [九龍站公共運輸交匯處](../Page/九龍站公共運輸交匯處.md "wikilink")/[柯士甸道西](../Page/柯士甸道西.md "wikilink")/[雅翔道](../Page/雅翔道.md "wikilink")

<!-- end list -->

  - 鄰近西九龍站

<!-- end list -->

  - 除上述路線外，鄰近西九龍站亦有以下公共交通途經:

<!-- end list -->

  - 佐敦道公共小巴

<!-- end list -->

  - 步行设施

<!-- end list -->

  - 西九龍站至九龍站及柯士甸站行人天橋及行人隧道

</div>

</div>

## 圖集

<File:HK> Union SquareOverview 20041024.jpg|2004年10月的Union Square
<File:UnionSquareview> 20070829.jpg|2007年8月的Union Square <File:Union>
Square Overview 200809.jpg|2008年9月的Union
Square（左起：[環球貿易廣場](../Page/環球貿易廣場.md "wikilink")、[君臨天下](../Page/君臨天下.md "wikilink")、[凱旋門](../Page/凱旋門_\(香港\).md "wikilink")、[漾日居](../Page/漾日居.md "wikilink")、[擎天半島](../Page/擎天半島.md "wikilink")）
<File:International> Commerce Centre on Victoria
Harbour.jpg|晚上從[灣仔遠眺Union](../Page/灣仔.md "wikilink")
Square <File:Union> Square view 201807.jpg|從高處航拍Union Square

## 參見

  - [九龍站](../Page/九龍站.md "wikilink")
  - [漾日居](../Page/漾日居.md "wikilink")
  - [擎天半島](../Page/擎天半島.md "wikilink")
  - [凱旋門](../Page/凱旋門_\(香港\).md "wikilink")
  - [君臨天下](../Page/君臨天下.md "wikilink")
  - [圓方（Elements）](../Page/圓方.md "wikilink")
  - [天璽](../Page/天璽_\(香港\).md "wikilink")
  - [環球貿易廣場](../Page/環球貿易廣場.md "wikilink")
  - [蛋糕樓](../Page/蛋糕樓.md "wikilink")
  - [奧運三寶](../Page/奧運三寶.md "wikilink")
    ([奧運站上蓋物業發展項目](../Page/奧運站.md "wikilink"))

## 外部連結

  - [Union
    Square官方網站（英文）](https://web.archive.org/web/20070521213056/http://union-square.com.hk/)
  - [Union
    Square的衛星圖](http://maps.google.com/maps?f=q&hl=zh-TW&geocode=&time=&date=&ttype=&q=kowloon&sll=40.744526,-73.990774&sspn=0.02354,0.05785&ie=UTF8&ll=22.304622,114.161661&spn=0.005003,0.007253&t=k&z=17&om=1)
  - [Union
    Square的地圖](http://www.centamap.com/gc/centamaplocation.aspx?x=834646&y=818334&sx=834646.604&sy=818334.216&z=4)

[Category:Union Square](../Category/Union_Square.md "wikilink")
[Category:西九龍](../Category/西九龍.md "wikilink")

1.