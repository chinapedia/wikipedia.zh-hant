[Air-ftd-md90-02-ar-8.jpg](https://zh.wikipedia.org/wiki/File:Air-ftd-md90-02-ar-8.jpg "fig:Air-ftd-md90-02-ar-8.jpg")[bmi.a320-200.g-midt.800pix.jpg](https://zh.wikipedia.org/wiki/File:bmi.a320-200.g-midt.800pix.jpg "fig:bmi.a320-200.g-midt.800pix.jpg")空中巴士
A320-232\]\]

**V2500**為雙軸[高函道比](../Page/涵道比.md "wikilink")[渦輪引擎](../Page/涡轮发动机.md "wikilink")，主要用於[空中巴士](../Page/空中巴士.md "wikilink")[A320家族](../Page/A320.md "wikilink")（包括A320、A321、A319、A318、空中巴士商務機），及[麦克唐纳-道格拉斯公司](../Page/麦克唐纳-道格拉斯公司.md "wikilink")[MD-90上](../Page/麥道-80.md "wikilink")。

## 發展

[國際航空發動機公司](../Page/國際航空發動機公司.md "wikilink")（IAE, International Aero
Engines AG）是於1983年，由四間航空發動機生產商合資而成，包括：

  - [普惠](../Page/普惠.md "wikilink")（Pratt &
    Whitney）：[燃燒室及](../Page/燃燒室.md "wikilink")[高壓渦輪](../Page/渦輪.md "wikilink")（combustor、high
    pressure turbine）
  - [勞斯萊斯股份有限公司](../Page/勞斯萊斯股份有限公司.md "wikilink")（Rolls-Royce
    plc）：[高壓压气机](../Page/压气机.md "wikilink")（high pressure
    compressor）
  - [日本航空發動機](../Page/日本航空發動機.md "wikilink")（Japanese Aero Engines
    Corporation）：風扇及低压压气机（Fan and low pressure compressor）
  - [MTU航空發動機](../Page/MTU航空發動機.md "wikilink")（MTU Aero
    Engines）：低壓渦輪（Low pressure turbine）

V2500於1988年獲得[美國聯邦航空局](../Page/美國聯邦航空局.md "wikilink")（FAA）的飛行認證。\[1\]

V2500發動機擁有各合資公司的技術，包括勞斯萊斯的[中空闊弦](../Page/中空闊弦.md "wikilink")（wide-chord）扇葉及普惠的浮牆燃燒室（"floatwall"
combustor）。發動機所使用的十段高壓壓縮器是由1960年代勞欺萊斯RC34B壓縮器研究計劃所衍生出來。

日本航空發動機公司曾於1970年代與勞斯萊斯合作，發展供[波音737-300使用](../Page/波音737.md "wikilink")，推力為20,000磅（89千牛噸）的RJ500發動機，但測試兩台試驗品後，RJ500計劃於80年代初中止。

1982年，兩公司轉向發展供150座席級，推力達25,000磅（111千牛噸）的發動機。該發動機起初名為RJ-500-35，隨著普惠、MTU、快意等公司加入，後來改名為V2500。V
意味原先五個合作伙伴，2500代表設計推力為25,000磅。其後，[快意退出國際航空發動機公司](../Page/快意.md "wikilink")。

V2500比RJ500更為先進，擁有較大直徑的扇葉，為了提高整體[壓縮比](../Page/壓縮比.md "wikilink")，於扇葉後新增了零段（zero-stage）及三個增壓段（booster
stage）。因其高函道比設計，加了兩個額外的低壓渦輪段，總共有五個。

國際航空發動機公司為超過1,400架飛機提供了超過3,100副V2500發動機供超過135個客戶，累積達5000萬個飛行小時。\[2\]
現今，連同已落訂，但未付運的，訂單總數超過5,500台。\[3\]
國際航空發動機公司的客戶包括大型國際航空公司及支線航空公司。2003年，V2500發動機於A320市場率為83%。對手的CFM56-6則為130個客戶，大約1,600部A320系列客機提供動力，累積達5000萬個飛行小時、3000萬個飛行旅程。空中巴士給予國際航空發動機的代號為'3'，例如：A319-1<u>3</u>2
or A321-2<u>3</u>1.

國際航空發動機公司並沒有為空中巴士A318客機提供發動機，航空公司只能選擇CFM
International的CFM56-5或普惠的PW6122A。

## 型號

### V2500-A1

最初生產的型號，1989年5月投入服務，推力25,000磅，用於A320上。被業界稱為問題多多的發動機，比A5有更多零件。

### V2530-A5

1993年3月投入服務，推力31,400磅，推動較大的A321-200客機。

### V2527-A5/V2527E-A5

1993年11月投入服務，供A320-200客機使用，推力增至27,000磅（120千牛噸）

### V2525-D5/V2528-D5

1995年4月投入服務，作為MD90的發動機，推力分別為25,000磅，及28,000磅（125千牛噸）

### V2533-A5

1997年4月投入服務，引入了第四增壓段，以增加中心流量。更大的扇葉及更多的空氣流量令推力增至33,000磅（147千牛噸），以配合空中巴士推出有更大航程的A321-200客機。

### V2522-A5

為配合A319而推出，於1997年6月投入服務，推力只有22,000磅

### V2524-A5

1998年10月投入服務，供A319使用，推力增至24,000磅

### V2500Select®

2005年10月10日，國際航空發動機公司宣佈推出V2500Select®，[IndiGo
Airlines為啟始客戶](../Page/IndiGo_Airlines.md "wikilink")，供其100部A320家族客機使用。V2500Select®
包括V2500Select®
Services，提供優良的售後服務。以及預期於2008年第三季推出的SelectOne™，改良了不少部份，增加發動機性能。

## 參考

## 外部連結

  - [Concourse Article on the
    V2500](https://web.archive.org/web/20041204105337/http://www.iaenews.com/media/2003-06_Concourse.pdf)
  - [Home Page of the V2500
    series](https://web.archive.org/web/19981201080917/http://www.v2500.com/)
  - [Rolls Royce V2500
    page](http://www.rolls-royce.com/civil_aerospace/products/airlines/v2500/default.jsp)
  - [Pratt & Whitney V2500
    page](https://web.archive.org/web/20070812084801/http://www.pw.utc.com/vgn-ext-templating/v/index.jsp?vgnextrefresh=1&vgnextoid=726ee64a5d7db010VgnVCM1000000881000aRCRD)
  - [MTU V2500
    page](https://web.archive.org/web/20070825011232/http://www.mtu.de/en/products_services/commercial_mro/programs/v2500/index.html)

[Category:高涵道比渦扇發動機](../Category/高涵道比渦扇發動機.md "wikilink")

1.
2.
3.