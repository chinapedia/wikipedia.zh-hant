[Lichtbogen_3000_Volt.jpg](https://zh.wikipedia.org/wiki/File:Lichtbogen_3000_Volt.jpg "fig:Lichtbogen_3000_Volt.jpg")
**電弧**（），又稱**弧放電**（），是由于[电场过强](../Page/电场.md "wikilink")，[气体发生](../Page/气体.md "wikilink")[電擊穿而持续形成](../Page/電擊穿.md "wikilink")[等离子体](../Page/等离子体.md "wikilink")，使得[电流通过了通常状态下的](../Page/电流.md "wikilink")[绝缘](../Page/绝缘.md "wikilink")[介质](../Page/介质.md "wikilink")（例如[空氣](../Page/空氣.md "wikilink")）的現象，或者说当通电的高电压电路出现导体与导体的分开时，两段就会出现电弧
。1808年[漢弗里·戴維](../Page/漢弗里·戴維.md "wikilink")（Humphry
Davy）利用此一現象發明第一盞「電燈」—[電弧燈](../Page/電弧燈.md "wikilink")（voltaic
arc lamp）。

## 利用

[Arcing_pickup_shoe.jpg](https://zh.wikipedia.org/wiki/File:Arcing_pickup_shoe.jpg "fig:Arcing_pickup_shoe.jpg")間的電弧\]\]
[Xenon_short_arc_1.jpg](https://zh.wikipedia.org/wiki/File:Xenon_short_arc_1.jpg "fig:Xenon_short_arc_1.jpg")
[SMAW.welding.navy.ncs.jpg](https://zh.wikipedia.org/wiki/File:SMAW.welding.navy.ncs.jpg "fig:SMAW.welding.navy.ncs.jpg")

電弧產生的高熱可以融化或是汽化所有的金屬\[1\]，工業上利用電弧來[焊接](../Page/焊接.md "wikilink")、[融化或是切割金屬](../Page/融化.md "wikilink")，例如機（plasma
cutting）、[放電加工機](../Page/放電加工.md "wikilink")（electrical discharge
machining）、煉鋼廠的[電弧爐](../Page/電弧爐.md "wikilink")。

電弧燈、電影院用的[電影放映機也是利用電弧原理的一些設備](../Page/電影放映機.md "wikilink")。

## 危害

非控制下產生的電弧對會對[輸電系統](../Page/輸電系統.md "wikilink")、[配電系統以及電子設備造成損害](../Page/配電系統.md "wikilink")\[2\]。例如，在配電電路中，開關觸點間發生的電弧會融化開關觸點\[3\]。因此在工業及民用電氣中，都有很多防止或者消除電弧的設計。

## 相關資訊

  - [焊接](../Page/焊接.md "wikilink")
  - [電弧燈](../Page/弧光燈.md "wikilink")
  - [瑞士航空111號班機空難調查](../Page/瑞士航空111號班機空難#事故原因.md "wikilink")
  - [加拿大航空797號班機空難調查](../Page/加拿大航空797號班機空難#調查.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部參考

  - [Unusual Arcing
    Photos](http://www.rmcybernetics.com/research/resonance/plasma.htm)

[Category:电弧](../Category/电弧.md "wikilink")

1.  [Carbon arc melting](http://arcmelt.velp.info/).
2.  [unintentional arc](http://205.243.100.155/frames/longarc.htm)
3.  [Arc Flash Hazards Study](http://www.arcadvisor.com/index.html)