**南海神庙**又称**波罗庙**，位于[中国](../Page/中华人民共和国.md "wikilink")[广州市](../Page/广州市.md "wikilink")[黄埔区](../Page/黄埔区.md "wikilink")[庙头村](../Page/庙头村.md "wikilink")（旧称扶胥村），是古代[皇帝](../Page/皇帝.md "wikilink")[祭祀](../Page/祭祀.md "wikilink")[海神](../Page/海神.md "wikilink")[洪聖的场所](../Page/洪聖.md "wikilink")，也是[海上丝绸之路的始发地](../Page/海上丝绸之路.md "wikilink")。始建于[隋朝](../Page/隋朝.md "wikilink")[开皇年间](../Page/开皇.md "wikilink")，至今已有1400多[年历史](../Page/年.md "wikilink")，是中国[四海神庙中唯一完整保存下来的建筑遗迹](../Page/四海神庙.md "wikilink")。2013年3月，南海神庙被列入[第七批全国重点文物保护单位](../Page/第七批全国重点文物保护单位.md "wikilink")。

## 历史

南海神庙始建于[隋朝](../Page/隋朝.md "wikilink")[开皇十四年](../Page/开皇.md "wikilink")（594年），[唐朝扩建](../Page/唐朝.md "wikilink")，[宋至](../Page/宋朝.md "wikilink")[明](../Page/明朝.md "wikilink")、[清均有重修](../Page/清朝.md "wikilink")、扩建。因地处广州之东，宋代时又称**东庙**。

在[唐玄宗时册尊](../Page/唐玄宗.md "wikilink")[南海神为](../Page/南海神.md "wikilink")**[广利大王](../Page/洪聖.md "wikilink")**；[南汉时封为](../Page/南汉.md "wikilink")**昭明帝**；[宋朝](../Page/宋朝.md "wikilink")、[元朝两代屡有加封](../Page/元朝.md "wikilink")，合称**南海广利洪圣昭顺威显灵孚王**，配以明顺夫人。

以前廣州有兩座南海神廟，除了[黃埔的這座東廟以外](../Page/黃埔.md "wikilink")，還有一座在[西關](../Page/西關.md "wikilink")[大觀河德興橋附近](../Page/西關涌.md "wikilink")（今[廣州酒家北側](../Page/廣州酒家.md "wikilink")），稱為南海西廟（又名洪聖西廟）。宋朝詩人[楊萬里有一首詩](../Page/楊萬里_\(南宋\).md "wikilink")：「大海更在小海東，西廟不如東廟雄。南來若不到東廟，西京未睹建章宮。」由此推測西廟在宋代經已存在。

根据民间传说，[贞观二十一年](../Page/贞观_\(唐太宗\).md "wikilink")（647年）[达奚司空从](../Page/达奚司空.md "wikilink")[印度](../Page/印度.md "wikilink")[摩揭陀国乘船到广州](../Page/摩揭陀国.md "wikilink")，带来[波罗树种在庙前](../Page/波罗树.md "wikilink")，后化为神，所以此庙俗称**波罗庙**，庙前[珠江称为](../Page/珠江.md "wikilink")**波罗江**。

古时每年都有[官员代表](../Page/官员.md "wikilink")[皇帝到该庙祭奠南海神](../Page/皇帝.md "wikilink")，商船、[渔船出海](../Page/渔船.md "wikilink")，亦向南海神祭拜，祈求一帆风顺、平安大吉。1962年7月该庙被公布为[广东省](../Page/广东省.md "wikilink")[文物保护单位](../Page/文物保护单位.md "wikilink")。

1966年，广东省人民政府将神庙移交给广州海运局，改成学校与波罗庙航修站。[文化大革命期间](../Page/文化大革命.md "wikilink")，南海神庙的大殿被毁，神像全部被砸碎，三分之二古碑刻遭推倒砸碎，其中明太祖[朱元璋御碑被推倒](../Page/朱元璋.md "wikilink")，断成两截。

1983年，广州海运局将神庙保护范围的土地全部移交给广州市文化局，重修动工典礼在1986年年初举行。1989年，全木结构的大殿与洪武碑亭落成，此后礼亭、廊庑、后殿、碑匾等修复、重建工作也陆续完成。
\[1\]

2006年，[瑞典仿古商船](../Page/瑞典.md "wikilink")[哥德堡号重走](../Page/哥德堡号.md "wikilink")[海上丝绸之路](../Page/海上丝绸之路.md "wikilink")，最后的终点站就是广州的南海神庙。

## 建筑

南海神庙坐[北向](../Page/北.md "wikilink")[南](../Page/南.md "wikilink")，占地3万[平方米](../Page/平方米.md "wikilink")，是[明代建筑风格](../Page/明代.md "wikilink")。其主体建筑沿中轴线从南到北依次为头门、仪门、礼亭、大殿、昭灵宫共五进，一进高于一进，其他附属建筑均以五进为中心，左右对称，是典型的中国传统[庙宇建筑](../Page/庙宇.md "wikilink")。

### 头门

[Guangzhou_Nanhaishen_Miao_2013.10.01_10-22-15.jpg](https://zh.wikipedia.org/wiki/File:Guangzhou_Nanhaishen_Miao_2013.10.01_10-22-15.jpg "fig:Guangzhou_Nanhaishen_Miao_2013.10.01_10-22-15.jpg")
庙外有“海不扬波”的石[牌坊](../Page/牌坊.md "wikilink")。过了石牌坊，是建于[清代头门的庭院](../Page/清代.md "wikilink")，庭院东、西侧各有一对青石[华表](../Page/华表.md "wikilink")，一对石[狮子](../Page/狮子.md "wikilink")。两边分立着[千里眼和](../Page/千里眼.md "wikilink")[顺风耳](../Page/顺风耳.md "wikilink")，大门两边分画有[秦琼与](../Page/秦琼.md "wikilink")[尉迟恭的彩绘像](../Page/尉迟恭.md "wikilink")。门上方有“南海神庙”的横匾，两侧附有一副[对联](../Page/对联.md "wikilink")。[东侧有韩愈碑亭](../Page/东.md "wikilink")，有[韩愈撰写的](../Page/韩愈.md "wikilink")[南海神广利王庙碑](../Page/南海神广利王庙碑.md "wikilink")，尤为珍贵，它是[循州](../Page/循州.md "wikilink")[刺史](../Page/刺史.md "wikilink")[陈谏手书](../Page/陈谏.md "wikilink")，著名[石刻工](../Page/石刻.md "wikilink")[李叔齐镌刻](../Page/李叔齐.md "wikilink")。碑高2.47[米](../Page/米.md "wikilink")、宽1.13米，立于[唐朝](../Page/唐朝.md "wikilink")[元和十五年](../Page/元和_\(唐宪宗\).md "wikilink")（820年）[十月初一](../Page/十月初一.md "wikilink")。西侧有[北宋的开宝碑亭](../Page/北宋.md "wikilink")。

### 仪门

第二进为仪门，门口有一对石鼓，鼓脚用石头雕刻了鸟雀、[梅花鹿](../Page/梅花鹿.md "wikilink")、[蜜蜂和](../Page/蜜蜂.md "wikilink")[猴子四种](../Page/猴子.md "wikilink")[动物](../Page/动物.md "wikilink")，谐言是“爵禄封侯”。仪门上方是一刻有“圣德咸沾”的横匾，两侧附有对联。从仪门庭院到大殿的东西两侧，各有一条复廊，廊中陈列了自[唐](../Page/唐朝.md "wikilink")、[宋](../Page/宋朝.md "wikilink")、[元](../Page/元朝.md "wikilink")、[明](../Page/明朝.md "wikilink")、[清各代](../Page/清朝.md "wikilink")[碑刻共](../Page/碑刻.md "wikilink")45块，著名的有明洪武碑，清康熙碑等。

### 古码头

[Nam_Hoi_King_Temple2.jpg](https://zh.wikipedia.org/wiki/File:Nam_Hoi_King_Temple2.jpg "fig:Nam_Hoi_King_Temple2.jpg")
2006年为迎接哥德堡号，在“海不扬波”牌坊前修建广场，并新立“扶胥古埗”牌坊，建设期间发现古码头，证明以前的珠江水仍到达旧牌坊前，而现时海岸线已在新牌坊前，并且因黄埔发电厂的阻挡而形成一个港湾。

### 礼亭

[Nam_Hoi_King_Temple.jpg](https://zh.wikipedia.org/wiki/File:Nam_Hoi_King_Temple.jpg "fig:Nam_Hoi_King_Temple.jpg")
出仪门，就进入礼亭，是古代官员拜祭南海神的地方。礼亭是一座木结构建筑，单檐歇山顶，面阔与进深各3间。礼亭原建于[明代](../Page/明代.md "wikilink")，1990年仿明代风格重建。庭园西侧有康熙御碑亭，碑上“万里波澄”四字，为[康熙四十二年](../Page/康熙.md "wikilink")（1703年）由[康熙帝亲笔书写](../Page/康熙帝.md "wikilink")。东侧是明洪武御碑，该碑立于明朝[洪武三年](../Page/洪武.md "wikilink")（1370年），由明太祖[朱元璋授意](../Page/朱元璋.md "wikilink")，[礼部](../Page/礼部.md "wikilink")[侍郎](../Page/侍郎.md "wikilink")[王玮撰文](../Page/王玮.md "wikilink")。

### 大殿

礼亭的背后是南海神庙最重要的建筑物——大殿，它是仿明代木结构琉璃瓦歇山顶建筑。屋顶用绿色的琉璃瓦覆盖；中间有双凤飞翔、鳌鱼倒悬等纹饰的琉璃瓦脊；上部有两条躯体弯曲作腾飞疾走状的苍龙造型，正在争夺当中的宝珠。大殿原为明代建筑，在[文化大革命期间被毁](../Page/文化大革命.md "wikilink")，1989年重建。大殿正中安放了3.8米高的南海神祝融。左右两旁有六侯塑像，分别是[达奚司空的助利侯](../Page/达奚司空.md "wikilink")、[杜公司空的助惠侯](../Page/杜公司空.md "wikilink")、巡海曹将军的济应侯、巡海提点使的顺应侯、王子一郎的辅灵侯、王子二郎的赞宁侯。祝融像的背后有一块照壁。大殿东侧有一个[东汉大铜](../Page/东汉.md "wikilink")[鼓和明代的铁钟](../Page/鼓.md "wikilink")。

### 昭灵宫

第五进名为昭灵宫，也称**后殿**，是南海神夫人的[寝宫](../Page/寝宫.md "wikilink")，相传她是[妇女](../Page/妇女.md "wikilink")、[儿童的保护神](../Page/儿童.md "wikilink")。是20世纪30年代[陈济棠时期改建](../Page/陈济棠.md "wikilink")。

### 浴日亭

[Nam_Hoi_King_Temple3.jpg](https://zh.wikipedia.org/wiki/File:Nam_Hoi_King_Temple3.jpg "fig:Nam_Hoi_King_Temple3.jpg")
在南海神庙西侧，有一座小山丘，古时叫做**章丘**。山上有一座小亭，名为**浴日亭**，是观望海上[日出之地](../Page/日出.md "wikilink")。亭内有[苏轼及](../Page/苏轼.md "wikilink")[陈献章诗碑各一块](../Page/陈献章.md "wikilink")。该处以“扶胥浴日”列入[宋](../Page/宋朝.md "wikilink")、[元](../Page/元朝.md "wikilink")、[清三代的](../Page/清朝.md "wikilink")[羊城八景之一](../Page/羊城八景.md "wikilink")。[林则徐在](../Page/林则徐.md "wikilink")[虎门销烟前曾到该庙祭海](../Page/虎门销烟.md "wikilink")，[孙中山与同僚亦曾来此参观](../Page/孙中山.md "wikilink")。

现存的浴日亭为1986年重修，是广东省重点文物保护单位（1985年8月立）。\[2\]

## 习俗

每年[农历](../Page/农历.md "wikilink")[二月十三为](../Page/二月十三.md "wikilink")[南海神诞又称](../Page/南海神诞.md "wikilink")**波罗诞**。届时，当地民众都到庙里进香礼拜，祈求平安，

## 交通

  - [地鐵](../Page/廣州地鐵.md "wikilink")：[南海神廟站](../Page/南海神廟站.md "wikilink")
  - 公交：
      - BRT南海神庙站：B1、B1快线、B26、B28、B29、B30、B31
      - 南海神庙夜班公交站：夜41、夜73

## 参见

  - [洪圣庙](../Page/洪圣庙.md "wikilink")

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《广州历史·古代部分》，广东教育出版社，1996年6月版。ISBN 7-5406-0625-8

<!-- end list -->

  - 网页

<!-- end list -->

  - [中国广州网](https://web.archive.org/web/20070428190339/http://www.guangzhou.gov.cn/node_392/node_393/node_394/node_591/2005-06/111877744553091.shtml)

## 外部链接

  - [南海神庙文物管理所](https://web.archive.org/web/20080210053632/http://hpnhsm.net.8hy.cn/indexsc.html)
  - [《南方都市报》：南海神庙与“波罗”丝路起点扶胥港　百国千帆来扶胥　碧海万里不扬波](https://web.archive.org/web/20140514063559/http://www.gzlib.gov.cn/shequ_info/ndgz/NDGZDetail.do?id=18147)
    - [广州图书馆](../Page/广州图书馆.md "wikilink")

{{-}}

[Category:广东全国重点文物保护单位](../Category/广东全国重点文物保护单位.md "wikilink")
[N南](../Category/隋朝建築.md "wikilink")
[Category:羊城八景](../Category/羊城八景.md "wikilink")
[N南](../Category/黄埔区.md "wikilink")
[Category:广州古迹](../Category/广州古迹.md "wikilink")
[Category:洪聖廟](../Category/洪聖廟.md "wikilink")

1.
2.