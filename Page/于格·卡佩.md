**雨果·卡佩**（，），一译**于格·卡佩**，法兰西国王。巴黎伯爵[偉大雨果的兒子](../Page/伟大的于格.md "wikilink")。祖父為[西法兰克国王](../Page/法國君主列表.md "wikilink")[羅貝爾一世](../Page/羅貝爾一世_\(西法蘭克\).md "wikilink")。956年繼承父親為法蘭西公爵。
987年被貴族正式選舉成為[法蘭克人的國王](../Page/法國君主列表.md "wikilink")，建立[法國歷史上的](../Page/法國.md "wikilink")[卡佩王朝](../Page/卡佩王朝.md "wikilink")。

## 王朝的建立

自9世紀開始，北歐[諾曼人頻繁地入侵西法蘭克王國](../Page/諾曼人.md "wikilink")，巴黎屢遭洗劫。885年，諾曼人再次進攻[巴黎](../Page/巴黎.md "wikilink")，遭到[強者羅貝爾之子巴黎伯爵](../Page/強者羅貝爾.md "wikilink")[厄德率領的巴黎軍隊的阻擊](../Page/厄德.md "wikilink")。由於厄德在巴黎保衛戰中聲威大震，887年被推選為西法蘭克國王。

但諾曼人的威脅緩解後，厄德的地位不穩，遭到法國一些大封建主的反對。在經歷了893年—898年的6年內戰後，厄德死去，其兄弟[羅貝爾放棄了鬥爭](../Page/羅貝爾一世_\(西法蘭克\).md "wikilink")，臣服於那些大封建主擁立的[卡洛林王朝](../Page/卡洛林王朝.md "wikilink")[純樸的查理](../Page/查理三世_\(西法蘭克\).md "wikilink")，因而接受「法蘭西公爵」的稱號，領有巴黎、奧爾良、圖爾、夏爾特爾等地，內戰終止。

純樸的查理統治晚年，內訌重啟。羅貝爾被西部貴族擁戴為王，聯合[勃艮第公爵](../Page/勃艮第公爵.md "wikilink")[魯道夫和](../Page/魯道夫.md "wikilink")[韋芒杜瓦伯爵赫伯特](../Page/韋芒杜瓦伯爵.md "wikilink")，923年在[蘇瓦松擊敗了查理的軍隊](../Page/蘇瓦松.md "wikilink")，但羅貝爾也在此役中陣亡。查理後被赫伯特囚禁，羅貝爾之婿[拉烏爾被推選為王](../Page/拉烏爾_\(西法蘭克國王\).md "wikilink")（923年—936年）。拉烏爾死後無嗣，羅貝爾之子[偉大的于格憑其實力完全可以繼承王位](../Page/偉大的于格.md "wikilink")，但他效仿昔日法蘭克[宮相故伎](../Page/宮相.md "wikilink")，專事幕後操縱，仍讓查理之子[路易四世繼位](../Page/路易四世_\(西法蘭克\).md "wikilink")。直到987年末代國王路易四世之子[路易五世死去](../Page/路易五世_\(西法蘭克\).md "wikilink")，偉大的于格之子**于格·卡佩**被貴族和高級僧侶推選為國王，是為法蘭西王國卡佩王朝的開端。

## 卡佩王朝初期

于格·卡佩知道自己的王位不穩，卡洛林王朝的（[路易四世的兒子](../Page/路易四世_\(西法蘭克\).md "wikilink")）是與他競爭王位的最大對手，卡佩一即位，查理便反對選舉結果及繼承權。查理得到了卡洛林王朝庶系[韋爾芒杜瓦伯爵及忠於卡洛林王朝的](../Page/韋爾芒杜瓦伯爵.md "wikilink")[弗蘭德伯爵所支持](../Page/弗蘭德伯爵.md "wikilink")。查理取下[拉昂](../Page/拉昂.md "wikilink")－皇室的寶座。于格·卡佩和他的兒子羅貝爾圍城兩次，但每次都被迫撤回。因此于格·卡佩決定與[神聖羅馬帝國的攝政](../Page/神聖羅馬帝國.md "wikilink")結盟（皇帝[奧托三世的母親](../Page/奧托三世_\(神聖羅馬帝國\).md "wikilink")），但她並未答覆。

此時，支持于格·卡佩的[蘭斯大主教阿達爾貝隆逝世](../Page/蘭斯_\(法國城市\).md "wikilink")，于格·卡佩決定由前任卡洛林王朝國王[洛泰爾的私生子](../Page/洛泰爾_\(西法蘭克國王\).md "wikilink")繼任成為蘭斯大主教以爭取卡洛林王朝一些勢力所支持。雖然阿努爾夫向于格·卡佩宣誓效忠，但是他仍是因為血緣關係改而支持叔父查理。阿努爾夫將蘭斯的貴族騙到城堡，然後開城門迎接查理。

當于格·卡佩質疑自己能否戰勝查理及阿努爾夫時，因被查理遂出而投靠于格·卡佩。在991年的[濯足節](../Page/濯足節.md "wikilink")，阿代爾伯倫假裝為雙方和解，前去會見查理及阿努爾夫，查理歡迎他，但是要求他發誓若有違誓會被詛咒，阿代爾伯倫便立下誓言，當晚，阿代爾伯倫在查理及阿努爾夫睡著時，把他們抓住，將他們交給了于格·卡佩。查理被囚禁在[奧爾良直到他於](../Page/奧爾良.md "wikilink")993年或之前逝世\[1\]。他的兒子，在監獄裡出生後被釋放。

為鞏固卡佩王朝，即位同年的聖誕節，便立即為兒子[羅貝爾加冕](../Page/羅貝爾二世_\(法蘭西\).md "wikilink")。王權仍然衰微，貴族各自為政。

## 與教皇的爭議

于格·卡佩在991年因廢除蘭斯大主教阿努爾夫而與當時的教皇[若望十五世有分歧](../Page/若望十五世.md "wikilink")，教皇呼籲西法蘭克的主教們在國王的領地外的[亞琛舉行獨立的主教會議](../Page/亞琛.md "wikilink")，但遭西法蘭克的主教們反對。其後，教宗又派特使在[穆松召開一個由西法蘭克和](../Page/穆松.md "wikilink")[東法蘭克的主教參加的主教會議](../Page/東法蘭克.md "wikilink")，但因卡佩阻撓，最後只得東法蘭克的主教出現。

教宗通過特使最終裁定廢黜阿努爾夫是違法。于格·卡佩去世後，阿努爾夫從監禁中被釋放，並很快得到平反。

996年10月24日，于格·卡佩在巴黎逝世，並葬於[聖德尼修道院大教堂](../Page/聖德尼聖殿.md "wikilink")。西法蘭克由其子羅貝爾二世繼續執政，開創卡佩王朝。

## 家庭

因為[阿基坦公爵](../Page/阿基坦公爵.md "wikilink")為了與于格·卡佩講和，所以於969年將女兒[阿基坦的阿德萊德嫁給于格](../Page/阿基坦的阿德萊德.md "wikilink")·卡佩\[2\]。二人誕下兩女一子：

  - 長女：吉斯拉，嫁給。
  - 次女：哈德維希，嫁給。
  - 幼子：[羅貝爾二世](../Page/羅貝爾二世_\(法蘭西\).md "wikilink")，在父親逝世後成為國王。

據說于格·卡佩還有許多其他女兒，但是來源的可靠性比較差\[3\]。

## 參考

## 參考文獻

  - Bordenove, Georges. *Les Rois qui ont fait la France: Hugues Capet,
    le Fondateur.* Paris: Marabout, 1986.
  - Gauvard, Claude. *La France au Moyen Âge du Ve au XVe siècle.*
    Paris: PUF, 1996. 2-13-054205-0
  - James, Edward. *The Origins of France: From Clovis to the Capetians
    500–1000.* London: Macmillan, 1982.
  - Riché, Pierre. *Les Carolingiens: Une famille qui fit l'Europe.*
    Paris: Hachette, 1983. 2-012-78551-0
  - Theis, Laurent. *Histoire du Moyen Âge français: Chronologie
    commentée 486–1453.* Paris: Perrin, 1992. 2-87027-587-0
  - Lewis, Anthony W. "[Anticipatory Association of the Heir in Early
    Capetian France.](https://www.jstor.org/stable/1867651)" *The
    American Historical Review*, Vol. 83, No. 4. (Oct., 1978), pp
    906–927.

[Category:法國君主](../Category/法國君主.md "wikilink")
[Category:卡佩王朝](../Category/卡佩王朝.md "wikilink")
[Category:巴黎人](../Category/巴黎人.md "wikilink")
[Category:開國君主](../Category/開國君主.md "wikilink")

1.  Pierre Riché, ''The Carolingians; A Family Who Forged Europe",
    trans. Michael Idomir Allen (Philadelphia: University of
    Pennsylvania Press, 1983), p. 279
2.
3.  Thus Gauvard, p. 531.