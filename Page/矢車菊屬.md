**矢車菊屬**（**Centaurea**），屬於[菊科](../Page/菊科.md "wikilink")，是大約350至500種[草本植物和](../Page/草本植物.md "wikilink")[薊的開花植物之](../Page/薊.md "wikilink")[屬](../Page/屬.md "wikilink")，大多都生長在[舊大陸中](../Page/舊大陸.md "wikilink")。同是矢車菊屬，另外種類包括[星薊](../Page/星薊.md "wikilink")、[矢車菊](../Page/矢車菊.md "wikilink")、黑矢車菊屬等等。一些種類培養作裝飾植物，栽種在庭院裡。

## 種類

矢車菊屬的種類有：

  - *[Centaurea adpressa](../Page/Centaurea_adpressa.md "wikilink")*
  - *[Centaurea aggregata](../Page/Centaurea_aggregata.md "wikilink")*
  - *[Centaurea alpestris](../Page/Centaurea_alpestris.md "wikilink")*
  - *[Centaurea alpina](../Page/Centaurea_alpina.md "wikilink")*
  - *[Centaurea americana](../Page/Centaurea_americana.md "wikilink")* –
    **American Star-thistle**
  - *[Centaurea argentea](../Page/Centaurea_argentea.md "wikilink")*
  - *[Centaurea aspera](../Page/Centaurea_aspera.md "wikilink")* –
    **Rough Star-thistle**
  - *[Centaurea
    atropurpurea](../Page/Centaurea_atropurpurea.md "wikilink")*
  - *[Centaurea babylonica](../Page/Centaurea_babylonica.md "wikilink")*
  - *[Centaurea behen](../Page/Centaurea_behen.md "wikilink")*
  - *[Centaurea bella](../Page/Centaurea_bella.md "wikilink")*
  - *[Centaurea bovina](../Page/Centaurea_bovina.md "wikilink")*
  - *[Centaurea bulbosa](../Page/Centaurea_bulbosa.md "wikilink")*
  - *[Centaurea
    cachinalensis](../Page/Centaurea_cachinalensis.md "wikilink")*
  - *[Centaurea calcitrapa](../Page/Centaurea_calcitrapa.md "wikilink")*
    – [紫星薊](../Page/紫星薊.md "wikilink")（**Purple Star Thistle** or
    **Caltrop** - Europe）
  - *[Centaurea
    calcitrapoides](../Page/Centaurea_calcitrapoides.md "wikilink")*
  - *[Centaurea chilensis](../Page/Centaurea_chilensis.md "wikilink")*
  - *[Centaurea cineraria](../Page/Centaurea_cineraria.md "wikilink")* –
    **Dusty Miller**
  - *[Centaurea clementei](../Page/Centaurea_clementei.md "wikilink")*
  - *[Centaurea cyanus](../Page/Centaurea_cyanus.md "wikilink")* –
    [矢車菊](../Page/矢車菊.md "wikilink")（**Cornflower**）
  - *[Centaurea dealbata](../Page/Centaurea_dealbata.md "wikilink")*
  - *[Centaurea debeauxii](../Page/Centaurea_debeauxii.md "wikilink")*
  - *[Centaurea depressa](../Page/Centaurea_depressa.md "wikilink")*
  - *[Centaurea diffusa](../Page/Centaurea_diffusa.md "wikilink")* –
    [白矢車菊](../Page/白矢車菊.md "wikilink")（**Diffuse Knapweed**）
  - *[Centaurea diluta](../Page/Centaurea_diluta.md "wikilink")*
  - *[Centaurea
    dschungarica](../Page/Centaurea_dschungarica.md "wikilink")*
  - *[Centaurea eriophora](../Page/Centaurea_eriophora.md "wikilink")*
  - *[Centaurea floccosa](../Page/Centaurea_floccosa.md "wikilink")*
  - *[Centaurea gayana](../Page/Centaurea_gayana.md "wikilink")*
  - *[Centaurea
    glastifolia](../Page/Centaurea_glastifolia.md "wikilink")*
  - *[Centaurea grinensis](../Page/Centaurea_grinensis.md "wikilink")*
  - *[Centaurea horrida](../Page/Centaurea_horrida.md "wikilink")*
  - *[Centaurea hypoleuca](../Page/Centaurea_hypoleuca.md "wikilink")*
  - *[Centaurea iberica](../Page/Centaurea_iberica.md "wikilink")* – A
    spiny [Mediterranean](../Page/Mediterranean.md "wikilink") species
    thought to be the "thistle" mentioned in *Genesis*
  - *[Centaurea imperialis](../Page/Centaurea_imperialis.md "wikilink")*
  - *[Centaurea jacea](../Page/Centaurea_jacea.md "wikilink")*
  - *[Centaurea kasakorum](../Page/Centaurea_kasakorum.md "wikilink")*
  - *[Centaurea
    kopetaghensis](../Page/Centaurea_kopetaghensis.md "wikilink")*
  - *[Centaurea kotschyana](../Page/Centaurea_kotschyana.md "wikilink")*
  - *[Centaurea
    leucophylla](../Page/Centaurea_leucophylla.md "wikilink")*
  - *[Centaurea
    macrocephala](../Page/Centaurea_macrocephala.md "wikilink")*
  - *[Centaurea maculosa](../Page/Centaurea_maculosa.md "wikilink")* –
    **Spotted Knapweed**; eastern Europe; introduced in North America,
    now an invasive weed which releases a toxin that reduces growth of
    forage species.
  - *[Centaurea
    marschalliana](../Page/Centaurea_marschalliana.md "wikilink")*
  - *[Centaurea melitensis](../Page/Centaurea_melitensis.md "wikilink")*
  - *[Centaurea moschata](../Page/Centaurea_moschata.md "wikilink")* -
    **Sweet Sultan**
  - *[Centaurea
    monocephala](../Page/Centaurea_monocephala.md "wikilink")*
  - *[Centaurea montana](../Page/Centaurea_montana.md "wikilink")* –
    **Perennial Cornflower**
  - *[Centaurea nigra](../Page/Centaurea_nigra.md "wikilink")* – **Black
    Knapweed** or **Common Knapweed**
  - *[Centaurea nigrescens](../Page/Centaurea_nigrescens.md "wikilink")*
    – [提洛爾矢車菊](../Page/提洛爾矢車菊.md "wikilink")（**Tyrol Knapweed**）;
    southern and eastern Europe
  - *[Centaurea orientalis](../Page/Centaurea_orientalis.md "wikilink")*
  - *[Centaurea ovina](../Page/Centaurea_ovina.md "wikilink")*
  - *[Centaurea paniculata](../Page/Centaurea_paniculata.md "wikilink")*
  - *[Centaurea phrygia](../Page/Centaurea_phrygia.md "wikilink")*
  - *[Centaurea pindicola](../Page/Centaurea_pindicola.md "wikilink")*
  - *[Centaurea
    polypodiifolia](../Page/Centaurea_polypodiifolia.md "wikilink")*
  - *[Centaurea
    pulcherrima](../Page/Centaurea_pulcherrima.md "wikilink")*
  - *[Centaurea ragusina](../Page/Centaurea_ragusina.md "wikilink")*
  - *[Centaurea repens](../Page/Centaurea_repens.md "wikilink")* –
    **Russian Knapweed** (also **Turkestan Thistle**) is a perennial,
    native to southern [Russia](../Page/Russia.md "wikilink") and [Asia
    Minor](../Page/Asia_Minor.md "wikilink") to [Altay
    Mountains](../Page/Altay_Mountains.md "wikilink") and
    [Afghanistan](../Page/Afghanistan.md "wikilink"). It is a weed in
    parts of its native range as well as in places where it has been
    accidentally naturalized. The flowerhead is lilac and not spiny.
  - *[Centaurea rothrockii](../Page/Centaurea_rothrockii.md "wikilink")*
  - *[Centaurea ruthenica](../Page/Centaurea_ruthenica.md "wikilink")*
  - *[Centaurea rutifolia](../Page/Centaurea_rutifolia.md "wikilink")*
  - *[Centaurea sadleriana](../Page/Centaurea_sadleriana.md "wikilink")*
    – **Pannonian knapweed**
  - *[Centaurea scabiosa](../Page/Centaurea_scabiosa.md "wikilink")* –
    [市郊矢車菊](../Page/市郊矢車菊.md "wikilink")（**Greater Knapweed**）
  - *[Centaurea seridis](../Page/Centaurea_seridis.md "wikilink")*
  - *[Centaurea sibirica](../Page/Centaurea_sibirica.md "wikilink")*
  - *[Centaurea
    simplicicaulis](../Page/Centaurea_simplicicaulis.md "wikilink")*
  - *[Centaurea
    solstitialis](../Page/Centaurea_solstitialis.md "wikilink")* –
    [黃星薊](../Page/黃星薊.md "wikilink")（**Yellow Star Thistle**）;
    Europe.
  - *[Centaurea squarrosa](../Page/Centaurea_squarrosa.md "wikilink")*
  - *[Centaurea stenolepis](../Page/Centaurea_stenolepis.md "wikilink")*
  - *[Centaurea stoebe](../Page/Centaurea_stoebe.md "wikilink")*
  - *[Centaurea sulphurea](../Page/Centaurea_sulphurea.md "wikilink")*
  - *[Centaurea
    transalpina](../Page/Centaurea_transalpina.md "wikilink")*
  - *[Centaurea
    tchihatcheffii](../Page/Centaurea_tchihatcheffii.md "wikilink")* -
    Endangered species, Mogan Lake, Ankara, Turkey

[Centaurea_tchihatcheffii_yanardoner_sevgi_05668.jpg](https://zh.wikipedia.org/wiki/File:Centaurea_tchihatcheffii_yanardoner_sevgi_05668.jpg "fig:Centaurea_tchihatcheffii_yanardoner_sevgi_05668.jpg")

  - *[Centaurea
    trichocephala](../Page/Centaurea_trichocephala.md "wikilink")*
  - *[Centaurea
    triniifolia](../Page/Centaurea_triniifolia.md "wikilink")*
  - *[Centaurea
    triumfettii](../Page/Centaurea_triumfettii.md "wikilink")*
  - *[Centaurea uniflora](../Page/Centaurea_uniflora.md "wikilink")*
  - *[Centaurea virgata](../Page/Centaurea_virgata.md "wikilink")*

## 花蜜

矢車菊屬生產豐富的[花蜜](../Page/花蜜.md "wikilink")，特別是在高石灰土壤中，主要得益者是收採[蜂蜜的](../Page/蜂蜜.md "wikilink")[蜂農](../Page/蜂農.md "wikilink")。星薊種類蜂蜜味淡而有濃郁的香味。[美國是生產豐富最純正星薊蜂蜜的生產地](../Page/美國.md "wikilink")，但一些奸商以欺騙手法，將之當作銷售作[阿巴拉契亞山脈缺乏](../Page/阿巴拉契亞山脈.md "wikilink")，珍貴的[酸模樹蜂蜜](../Page/酸模樹.md "wikilink")。

高花蜜出產量使得對[昆蟲](../Page/昆蟲.md "wikilink")，如[蝴蝶](../Page/蝴蝶.md "wikilink")、[飛蛾](../Page/飛蛾.md "wikilink")、[六點榆蟲而言非常有吸引力](../Page/六點榆蟲.md "wikilink")，其化昆蟲也以矢車菊屬植物為食物。

<File:Yellow> star thistle.jpg|黃星薊 *Centaurea solstitialis*
<File:CentaureaCyanus-bloem-kl.jpg>|*Centaurea cyanus*
[File:Star_thistle4708-1-.jpg|紫星薊](File:Star_thistle4708-1-.jpg%7C紫星薊)

## 參考書目

  - Mabberley, D.J. 1987. *The Plant Book. A portable dictionary of the
    higher plants*. Cambridge University Press, Cambridge. 706 p. ISBN
    0-521-34060-8.
  - Robbins, W.W., M. K. Bellue, and W. S. Ball. 1970. *Weeds of
    California*. State of California, Dept. of Agriculture. 547 p.

[\*](../Category/矢车菊属.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")