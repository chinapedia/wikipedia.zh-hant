**比尔·拉塞尔NBA总决赛最有价值球员奖**（**Bill Russell NBA Finals Most Valuable Player
Award**），原名NBA总决赛最有价值球员奖，是從1968-69球季開始，授予在每年度[NBA](../Page/NBA.md "wikilink")[总决赛中表现最突出的球员的奖项](../Page/NBA总决赛.md "wikilink")，通常该奖项都授予获得总冠军的球队的成员。这个奖项是由一个小组投票决定的，这个小组由9位媒体成员组成，他们根据总决赛系列赛的比赛中球员的表现进行投票，获得最高分的球员将获得该奖项。至少有一次NBA总决赛MVP的评选中，球迷是可以在NBA网站上进行投票，票选结果作为第10票计入了最终的结果（注：不容许同时投票给两位球员）。2009年2月14日，在菲尼克斯举办2009年NBA全明星周末的间隙，NBA总裁[大卫·斯特恩宣布](../Page/大卫·斯特恩.md "wikilink")，NBA总决赛最有价值球员奖正式更名为“比尔·拉塞尔NBA总决赛最有价值球员奖”，以纪念[比尔·拉塞尔](../Page/比尔·拉塞尔.md "wikilink")——这位11次获得NBA总冠军的球员。

## 圖例

|        |                                                    |
| ------ | -------------------------------------------------- |
| ^      | 現役球員                                               |
| \*     | 入选[奈史密斯籃球名人纪念堂](../Page/奈史密斯籃球名人纪念堂.md "wikilink") |
| 球員 (X) | 獲得NBA总决赛最有价值球员的次數                                  |

## 得獎者

<table>
<thead>
<tr class="header">
<th><p>年分</p></th>
<th><p>得獎球員</p></th>
<th><p>位置</p></th>
<th><p>国籍</p></th>
<th><p>所屬球隊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1969年</p></td>
<td><p><a href="../Page/杰里·韦斯特.md" title="wikilink">杰里·韦斯特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a>（亚军）</p></td>
</tr>
<tr class="even">
<td><p>1970年</p></td>
<td><p><a href="../Page/威利斯·里德.md" title="wikilink">威利斯·里德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/纽约尼克斯队.md" title="wikilink">-{zh-hans:纽约尼克斯队;zh-hk:紐約人隊;zh-tw:紐約尼克隊;}-</a></p></td>
</tr>
<tr class="odd">
<td><p>1971年</p></td>
<td><p><a href="../Page/卡里姆·阿卜杜勒·贾巴尔.md" title="wikilink">卡里姆·阿卜杜勒·贾巴尔</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/密尔沃基雄鹿队.md" title="wikilink">密尔沃基雄鹿队</a></p></td>
</tr>
<tr class="even">
<td><p>1972年</p></td>
<td><p><a href="../Page/沃尔特·张伯伦.md" title="wikilink">沃尔特·张伯伦</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="odd">
<td><p>1973年</p></td>
<td><p><a href="../Page/威利斯·里德.md" title="wikilink">威利斯·里德</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/纽约尼克斯队.md" title="wikilink">-{zh-hans:纽约尼克斯队;zh-hk:紐約人隊;zh-tw:紐約尼克隊;}-</a></p></td>
</tr>
<tr class="even">
<td><p>1974年</p></td>
<td><p><a href="../Page/约翰·哈夫利切克.md" title="wikilink">约翰·哈夫利切克</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士頓凱爾特人隊.md" title="wikilink">波士顿凯尔特人队</a></p></td>
</tr>
<tr class="odd">
<td><p>1975年</p></td>
<td><p><a href="../Page/里克·巴里.md" title="wikilink">里克·巴里</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/金州勇士队.md" title="wikilink">金州勇士队</a></p></td>
</tr>
<tr class="even">
<td><p>1976年</p></td>
<td><p><a href="../Page/Jo·Jo·懷特.md" title="wikilink">Jo·Jo·懷特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人队.md" title="wikilink">波士顿凯尔特人队</a></p></td>
</tr>
<tr class="odd">
<td><p>1977年</p></td>
<td><p><a href="../Page/比尔·沃尔顿.md" title="wikilink">比尔·沃尔顿</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波特兰开拓者队.md" title="wikilink">波特兰开拓者队</a></p></td>
</tr>
<tr class="even">
<td><p>1978年</p></td>
<td><p><a href="../Page/韦斯·昂塞尔德.md" title="wikilink">韦斯·昂塞尔德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/华盛顿子弹队.md" title="wikilink">华盛顿子弹队</a></p></td>
</tr>
<tr class="odd">
<td><p>1979年</p></td>
<td><p><a href="../Page/丹尼斯·強森.md" title="wikilink">-{zh-hans:丹尼斯·约翰逊;zh-hk:丹尼士·莊遜;zh-tw:丹尼斯·詹森;}-</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/西雅图超音速队.md" title="wikilink">西雅图超音速队</a></p></td>
</tr>
<tr class="even">
<td><p>1980年</p></td>
<td><p><a href="../Page/魔术师约翰逊.md" title="wikilink">魔术师约翰逊</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="odd">
<td><p>1981年</p></td>
<td><p><a href="../Page/塞德里克·麦克斯维尔.md" title="wikilink">塞德里克·麦克斯维尔</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人队.md" title="wikilink">波士顿凯尔特人队</a></p></td>
</tr>
<tr class="even">
<td><p>1982年</p></td>
<td><p><a href="../Page/魔术师约翰逊.md" title="wikilink">魔术师约翰逊</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="odd">
<td><p>1983年</p></td>
<td><p><a href="../Page/摩西·马龙.md" title="wikilink">摩西·马龙</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/费城76人队.md" title="wikilink">费城76人队</a></p></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">拉里·伯德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人队.md" title="wikilink">波士顿凯尔特人队</a></p></td>
</tr>
<tr class="odd">
<td><p>1985年</p></td>
<td><p><a href="../Page/卡里姆·阿卜杜勒·贾巴尔.md" title="wikilink">卡里姆·阿卜杜勒·贾巴尔</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="even">
<td><p>1986年</p></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">拉里·伯德</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人队.md" title="wikilink">波士顿凯尔特人队</a></p></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p><a href="../Page/魔术师约翰逊.md" title="wikilink">魔术师约翰逊</a>（3）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p><a href="../Page/詹姆斯·沃西.md" title="wikilink">詹姆斯·沃西</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/乔·杜马斯.md" title="wikilink">乔·杜马斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/底特律活塞队.md" title="wikilink">底特律活塞队</a></p></td>
</tr>
<tr class="even">
<td><p>1990年</p></td>
<td><p><a href="../Page/伊塞亚·托马斯.md" title="wikilink">伊塞亚·托马斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/底特律活塞队.md" title="wikilink">底特律活塞队</a></p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛队.md" title="wikilink">芝加哥公牛队</a></p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛队.md" title="wikilink">芝加哥公牛队</a></p></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a>（3）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛队.md" title="wikilink">芝加哥公牛队</a></p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/阿基姆·奥拉朱旺.md" title="wikilink">阿基姆·奥拉朱旺</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/休斯敦火箭队.md" title="wikilink">休斯敦火箭队</a></p></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p><a href="../Page/阿基姆·奥拉朱旺.md" title="wikilink">阿基姆·奥拉朱旺</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/休斯敦火箭队.md" title="wikilink">休斯敦火箭队</a></p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a>（4）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛队.md" title="wikilink">芝加哥公牛队</a></p></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a>（5）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛队.md" title="wikilink">芝加哥公牛队</a></p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a>（6）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛队.md" title="wikilink">芝加哥公牛队</a></p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/蒂姆·邓肯.md" title="wikilink">蒂姆·邓肯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/圣安东尼奥马刺队.md" title="wikilink">圣安东尼奥马刺队</a></p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/沙奎尔·奥尼尔.md" title="wikilink">沙奎尔·奥尼尔</a>（3）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/蒂姆·邓肯.md" title="wikilink">蒂姆·邓肯</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/圣安东尼奥马刺队.md" title="wikilink">圣安东尼奥马刺队</a></p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/昌西·比卢普斯.md" title="wikilink">昌西·比卢普斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/底特律活塞队.md" title="wikilink">底特律活塞队</a></p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/蒂姆·邓肯.md" title="wikilink">蒂姆·邓肯</a>（3）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/圣安东尼奥马刺队.md" title="wikilink">圣安东尼奥马刺队</a></p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/德韦恩·韦德.md" title="wikilink">德韦恩·韦德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/迈阿密热火队.md" title="wikilink">迈阿密热火队</a></p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/托尼·帕克.md" title="wikilink">托尼·帕克</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/圣安东尼奥马刺队.md" title="wikilink">圣安东尼奥马刺队</a></p></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/保罗·皮尔斯.md" title="wikilink">保罗·皮尔斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人队.md" title="wikilink">波士顿凯尔特人队</a></p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/科比·布莱恩特.md" title="wikilink">科比·布莱恩特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/科比·布莱恩特.md" title="wikilink">科比·布莱恩特</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人队.md" title="wikilink">洛杉矶湖人队</a></p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/德克·诺维茨基.md" title="wikilink">德克·诺维茨基</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/達拉斯小牛队.md" title="wikilink">達拉斯小牛队</a></p></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/勒布朗·詹姆斯.md" title="wikilink">勒布朗·詹姆斯</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/邁阿密熱火隊.md" title="wikilink">邁阿密熱火隊</a></p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/勒布朗·詹姆斯.md" title="wikilink">勒布朗·詹姆斯</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/邁阿密熱火隊.md" title="wikilink">邁阿密熱火隊</a></p></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/科怀·伦纳德.md" title="wikilink">科怀·伦纳德</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/圣安东尼奥马刺队.md" title="wikilink">圣安东尼奥马刺队</a></p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/安德烈·伊格达拉.md" title="wikilink">安德烈·伊格达拉</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/金州勇士队.md" title="wikilink">金州勇士队</a></p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><a href="../Page/勒布朗·詹姆士.md" title="wikilink">勒布朗·詹姆士</a>（3）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/克里夫兰骑士队.md" title="wikilink">克里夫兰骑士队</a></p></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/凯文·杜兰特.md" title="wikilink">凯文·杜兰特</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/金州勇士队.md" title="wikilink">金州勇士队</a></p></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td><p><a href="../Page/凯文·杜兰特.md" title="wikilink">凯文·杜兰特</a>（2）</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/金州勇士队.md" title="wikilink">金州勇士队</a></p></td>
</tr>
</tbody>
</table>

## 附註

  - [杰里·韦斯特是唯一一个所在球队并没有获得当年NBA总冠军的得獎者](../Page/杰里·韦斯特.md "wikilink")。
  - 获得三次或以上FMVP的球员共五位，分别是：
      - [迈克尔·乔丹](../Page/迈克尔·乔丹.md "wikilink")，（1991-93，1996-98，共6次）
      - [埃尔文·约翰逊](../Page/埃尔文·约翰逊.md "wikilink")，（1980，1982，1987），而他是第一位也是唯一一位获得总决赛MVP的当年新秀。
      - [沙奎尔·奥尼尔](../Page/沙奎尔·奥尼尔.md "wikilink")，（2000-02）
      - [蒂姆·邓肯](../Page/蒂姆·邓肯.md "wikilink")，（1999，2003，2005）
      - [雷霸龍·詹姆士](../Page/雷霸龍·詹姆士.md "wikilink")，（2012，2013，2016）
  - [2007年](../Page/2007年NBA总决赛.md "wikilink")，[托尼·帕克是首位获得该奖项的欧洲球员](../Page/托尼·帕克.md "wikilink")。
  - [2011年](../Page/2011年NBA总决赛.md "wikilink")，[德克·诺维茨基是第二位獲得該獎項的歐洲球員](../Page/德克·诺维茨基.md "wikilink")。
  - [2015年](../Page/2015年NBA总决赛.md "wikilink")，[安德烈·伊格达拉是唯一在例行賽全無先發過卻拿到FMVP的球員](../Page/安德烈·伊格达拉.md "wikilink")

## 参见

  - [NBA最有价值球员](../Page/NBA最有价值球员.md "wikilink")
  - [NBA全明星赛最有价值球员](../Page/NBA全明星赛最有价值球员.md "wikilink")
  - [NBA总决赛](../Page/NBA总决赛.md "wikilink")
  - [NBA](../Page/NBA.md "wikilink")

## 參考資料

  - 全文

<!-- end list -->

  -
  -
<!-- end list -->

  - 細節

[\*](../Category/NBA总决赛.md "wikilink")
[Category:NBA獎項](../Category/NBA獎項.md "wikilink")
[Category:最有价值球员](../Category/最有价值球员.md "wikilink")