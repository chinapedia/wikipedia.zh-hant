**郭利民**\[1\]<span style="font-size:smaller;">，[MBE](../Page/MBE.md "wikilink")</span>（****，
），[葡萄牙籍](../Page/葡萄牙.md "wikilink")，生於[香港](../Page/香港.md "wikilink")，人稱**Uncle
Ray**或者「樂壇教父」\[2\]，著名[唱片騎師](../Page/唱片騎師.md "wikilink")，是[健力士世界紀錄最長壽的DJ](../Page/健力士世界紀錄.md "wikilink")。

## 簡介

郭利民曾於[聖若瑟書院受教育](../Page/聖若瑟書院.md "wikilink")，1946年至1949年於[匯豐銀行擔任文員](../Page/匯豐銀行.md "wikilink")。1949年加入[麗的呼聲任撰稿員](../Page/麗的呼聲.md "wikilink")，後成為唱片騎師，1960年加入[香港電台英文台](../Page/香港電台.md "wikilink")。1964年被港台派往[英國](../Page/英國.md "wikilink")[BBC修讀](../Page/BBC.md "wikilink")3個月課程，並令他有兩次機會訪問[披頭四樂隊](../Page/披頭四.md "wikilink")。1970年主持節目《All
the Way with
Ray》，成為香港最長壽的電台節目。早年曾與[金牌大風創辦人](../Page/金牌大風.md "wikilink")[鄭東漢及香港](../Page/鄭東漢.md "wikilink")[IFPI總裁](../Page/IFPI.md "wikilink")[馮添枝等人夾band](../Page/馮添枝.md "wikilink")，司職鼓手。1987年曾獲英女皇頒發[英帝國員佐勳章](../Page/MBE.md "wikilink")（Member
of Order of the British Empire；縮寫是MBE）。2008年，Uncle
Ray獲香港特區政府頒發[銅紫荊星章（the Bronze Bauhinia
Star）](../Page/銅紫荊星章.md "wikilink")，以表彰他長期為流行音樂所作出的貢獻。2012年，獲[香港演藝學院頒發榮譽院士](../Page/香港演藝學院.md "wikilink")。

雖然Uncle
Ray並非華裔，也非[土生葡人](../Page/土生葡人.md "wikilink")，但他在香港生活了數十年，講得一口流利的[粵語](../Page/粵語.md "wikilink")。其家人都在[澳門定居](../Page/澳門.md "wikilink")。

Uncle
Ray曾接受[TVB節目](../Page/電視廣播有限公司.md "wikilink")《[港生活·港享受](../Page/港生活·港享受.md "wikilink")》(Dolce
Vita)節目訪問。\[3\]

## 名下馬匹

郭利民是[香港賽馬會會員](../Page/香港賽馬會.md "wikilink")，曾育有馬匹我知幾時、應知幾時。

## 曾主持節目

  - 1949年 - 1960年 Progressive Jazz (麗的呼聲)
  - 1950年 - 1956年 Talent Time (麗的呼聲/現場)
  - 1950年 - 1956年 Rumpus Time (麗的呼聲/現場)
  - 1964年 - 1969年 Just For You (香港電台)
  - 1964年 - 1969年 From Me To You (香港電台)
  - 1964年 - 1969年 Hit Parade (香港電台)
  - 1964年 - 1969年 Lucky Dip (香港電台)
  - 1970年 - All the Way with Ray (香港電台)

## 參考資料

## 外部链接

  - <http://www.rthk.org.hk/channel/radio3/r3/r3_90_profile.htm>
  - [香港電台第3台 郭利民 主持 **All the Way with Ray**
    網上節目重播](http://www.rthk.org.hk/rthk/radio3/alltheway)
  - [Uncle Ray's Official
    Homepage](http://www.uncleraydj.com/index.html)

[Category:香港電台主持人](../Category/香港電台主持人.md "wikilink")
[Category:葡萄牙裔香港人](../Category/葡萄牙裔香港人.md "wikilink")
[Category:吉尼斯世界纪录保持者](../Category/吉尼斯世界纪录保持者.md "wikilink")
[Category:香港電台](../Category/香港電台.md "wikilink")
[C](../Category/聖若瑟書院校友.md "wikilink")
[C](../Category/MBE勳銜.md "wikilink")
[Category:香港馬主](../Category/香港馬主.md "wikilink")
[Category:香港的世界之最](../Category/香港的世界之最.md "wikilink")

1.
2.
3.  [港生活·港享受
    - 2009-10-15](http://mytv.tvb.com/lifestyle/dolcevita/101328#page-1)