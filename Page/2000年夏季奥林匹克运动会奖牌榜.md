**2000年夏季奥林匹克运动会奖牌榜**是一个按照[2000年夏季奥林匹克运动会所获奖牌数排序的](../Page/2000年夏季奥林匹克运动会.md "wikilink")[国家奥委会列表](../Page/国家奥委会.md "wikilink")。该届奥运会于2000年9月15日到10月1日在[澳大利亚](../Page/澳大利亚.md "wikilink")[悉尼举行](../Page/悉尼.md "wikilink")，全球199个国家和地区的10651名运动员（其中有4名[东帝汶运动员以个人名义参赛](../Page/东帝汶.md "wikilink")）参加了28个大项、300个小项的角逐。

<center>

</center>

<span style="font-size:smaller;">\*<span style="font-size:smaller;">此排名以参赛国家或地区获得的金牌数量为排名顺序依据，金牌之后依次以银牌，铜牌的获得数为排名顺序依据。如有多于一个以上的国家或地区所获同类奖牌数目相同，则依照这些国家或地区的英文名称字母顺序在同一名次内进行排名。本表所引用之数据以[國際奧林匹克委員會网站之历史数据为依据](../Page/國際奧林匹克委員會.md "wikilink")。</span></span>

## 赛后奖牌榜的变更

2007年10月5日，五枚奥运金牌得主、[美国选手](../Page/美国.md "wikilink")[马里恩·琼斯承认在比赛中服用](../Page/马里恩·琼斯.md "wikilink")[类固醇类兴奋剂](../Page/类固醇类.md "wikilink")，导致她在本届比赛中获得的3枚金牌和2枚铜牌都被[国际奥委会剥夺](../Page/国际奥委会.md "wikilink")。\[1\]
其中，女子100米金牌没有补发给其他选手，因为排在成绩第二位的[希腊选手](../Page/希腊.md "wikilink")在[雅典奥运会前未参加规定的药检而被处以两年禁赛](../Page/2004年夏季奥林匹克运动会.md "wikilink")。\[2\]
同时，琼斯在接力赛中队友的奖牌在通过上诉后得以保留。\[3\]

2008年8月2日，由于选手[Antonio
Pettigrew承认服用了](../Page/Antonio_Pettigrew.md "wikilink")[EPO和](../Page/红细胞生成素.md "wikilink")[生长激素](../Page/生长激素.md "wikilink")，国际奥委会剥夺了美国队男子4x400米接力项目的金牌。2012年7月21日，国际奥委会将金银铜牌分别授予尼日利亚、牙买加和巴哈马（原列第2至4名）。\[4\]\[5\]

2010年2月25日，[美联社报道](../Page/美联社.md "wikilink")，一名中国体操队队员在当时未满参赛的最低年龄限制16岁。[国际体操联合会证实](../Page/国际体操联合会.md "wikilink")[董芳霄在参赛时的年龄为](../Page/董芳霄.md "wikilink")14岁，因而在4月28日，国际奥委会取消了中国体操队所获得的铜牌，改授予排名第4位的美国队。\[6\]\[7\]

2013年1月17日，國際奧委會因[藍斯·阿姆斯壯長期使用禁藥的事件確實並認定UCI褫奪他在](../Page/藍斯·阿姆斯壯.md "wikilink")1998年8月1日之後成績的決定，褫奪他自行車男子公路計時賽的銅牌，而該獎牌没有补发给其他选手，因當時自行車界普遍使用禁藥，且已無法查出當時其他選手是否使用禁藥。\[8\]\[9\]\[10\]

## 参考资料

[Category:夏季奥林匹克运动会奖牌榜](../Category/夏季奥林匹克运动会奖牌榜.md "wikilink")
[Category:2000年夏季奥林匹克运动会](../Category/2000年夏季奥林匹克运动会.md "wikilink")

1.
2.
3.
4.
5.  [IOC Executive Board meets ahead of London Games - Olympic
    News](http://www.olympic.org/news/ioc-executive-board-meets-ahead-of-london-games/168640)
6.
7.
8.
9.  [(BBC)](http://www.bbc.co.uk/sport/0/cycling/21062496)
10. [(AP via *Los Angeles
    Times*)](http://www.latimes.com/sports/sportsnow/la-sp-sn-armstrong-reportedly-stripped-of-bronze-20130117,0,662993.story)