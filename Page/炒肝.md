**炒肝儿**是[北京的一种传统](../Page/北京.md "wikilink")[小吃](../Page/小吃.md "wikilink")，以[猪的](../Page/猪.md "wikilink")[肝脏](../Page/肝脏.md "wikilink")，[大肠等为主料](../Page/大肠.md "wikilink")，以[蒜等为辅料](../Page/蒜.md "wikilink")，以[淀粉](../Page/淀粉.md "wikilink")[勾芡做成的北京](../Page/勾芡.md "wikilink")[小吃](../Page/小吃.md "wikilink")。最初吃炒肝时讲究沿碗周围抿并要求搭配着小[包子一块吃](../Page/包子.md "wikilink")，但现在吃炒肝早已没有那么多讲究了。

## 起源

[Tianxingju.JPG](https://zh.wikipedia.org/wiki/File:Tianxingju.JPG "fig:Tianxingju.JPG")

北京的炒肝是由[宋代民间食品](../Page/宋代.md "wikilink")“熬肝”和“炒肺”发展而来的。但是更常见的说法是，炒肝是[前门](../Page/前门.md "wikilink")[鲜鱼口胡同的](../Page/鲜鱼口胡同.md "wikilink")[会仙居在](../Page/会仙居.md "wikilink")1900年前后\[1\]在白汤杂碎的基础上，去掉猪心、猪肺，并用淀粉勾芡而发明的。因此，会仙居被认为是炒肝的创制者。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，会仙居与[天兴居合并](../Page/天兴居.md "wikilink")。1997年12月，北京[天兴居制作的炒肝被](../Page/天兴居.md "wikilink")[中国烹饪协会授予](../Page/中国烹饪协会.md "wikilink")[中华名小吃称号](../Page/中华名小吃.md "wikilink")。

因炒肝的配料中有猪的[心脏和](../Page/心脏.md "wikilink")[肺](../Page/肺.md "wikilink")，故而当时京城曾流传“炒肝不勾芡——熬心熬肺”的[歇后语](../Page/歇后语.md "wikilink")，这句歇后语引起会仙居掌柜的不悦，遂从炒肝的配料中出去心脏和肺，却又因此落下“会仙居的炒肝——没心没肺”的歇后语，至今炒肝的配料中仍然没有心和肺。

另一种说法认为，炒肝源自[满族按照](../Page/满族.md "wikilink")[萨满教习俗杀猪祭神](../Page/萨满教.md "wikilink")、并分食祭肉后，将肠、肚等内脏烩成一锅由众人分食的习惯。满族入关建立清朝后，将这一习惯带至北京。由于满洲王公贵族、[八旗佐领等大户人家按习俗每日均杀猪](../Page/八旗.md "wikilink")，因此剩余大量祭肉及肝、肠、肚、肺等下水，无法食尽，遂出售给小贩，逐渐发展出[白煮肉](../Page/白煮肉.md "wikilink")、炒肝、[卤煮火烧等多种小吃](../Page/卤煮火烧.md "wikilink")。炒肝中的“炒”字并非汉语中用油翻炒之意，而是源于满语“colambi”，此字虽译自汉语“炒”字，但意思更为广泛，烹、炒、煎、熬均称之为“炒”。炒肝虽为水煮，但最终熬为浓汁，余汤极少，因此使用“炒”字而不用“煮”字（满语为bujumbi）。另一种北京传统小吃“炒红果”也是将红果炖熟、熬为浓汁而成。\[2\]

## 制作方法

[Chaogan.JPG](https://zh.wikipedia.org/wiki/File:Chaogan.JPG "fig:Chaogan.JPG")

先将猪大肠用[碱和](../Page/碱.md "wikilink")[醋洗净](../Page/醋.md "wikilink")，盘成捆用绳扎好后放在凉水锅中，旺火煮至[筷子能扎透时捞到凉水中洗去肠表皮的油](../Page/筷子.md "wikilink")，切成小段备用；再将猪肝洗净切片备用；将熟猪油倒入炒锅内，用旺火烧热放入[大料](../Page/大料.md "wikilink")，再依次放入[黄酱](../Page/黄酱.md "wikilink")、[姜末](../Page/姜.md "wikilink")、[酱油及](../Page/酱油.md "wikilink")[蒜泥](../Page/蒜泥.md "wikilink")，炒成熟蒜泥备用；将猪骨汤烧热，放入猪肠在一起炖，最后放入猪肝、酱油和熟蒜泥、生蒜泥等调味料搅匀，待汤沸后，立即用[淀粉匀芡](../Page/淀粉.md "wikilink")，再煮沸即成。

## 炒肝食俗

北京人傳統吃炒肝並不用湯匙筷子，而是整碗端起來喝。所以從前若有人吃炒肝用匙筷，老北京人一看就知道此人必為外地人。

## 以炒肝为说辞的北京歇后语、俏皮话

  - 猪八戒吃炒肝——自残骨肉
  - 你这人跟炒肝似的——没心没肺
  - 炒肝不勾芡——熬心熬肺
  - 会仙居的炒肝——没心没肺

## 相关历史事件

  - 2011年8月18日，[美国驻中国大使](../Page/美国驻华大使列表.md "wikilink")[骆家辉陪同在北京访问的](../Page/骆家辉.md "wikilink")[美国副总统](../Page/美国副总统.md "wikilink")[拜登在北京](../Page/拜登.md "wikilink")[鼓楼东侧的](../Page/鼓楼.md "wikilink")“姚记**炒肝**店”吃[炸酱面](../Page/炸酱面.md "wikilink")5人花79元。
  - 2013年12月28日，[中共中央總書記](../Page/中共中央總書記.md "wikilink")[習近平到北京](../Page/習近平.md "wikilink")[月壇](../Page/月壇.md "wikilink")[慶豐包子鋪點了二兩](../Page/慶豐包子鋪.md "wikilink")[包子](../Page/包子.md "wikilink")（六個）、一碗**炒肝**、一份[芥菜](../Page/芥菜.md "wikilink")，一共消費了21元，此事成为广为传播的[事件](../Page/習近平吃包子事件.md "wikilink")。习在用餐时，曾询问“炒肝怎么这么稠”\[3\]。

## 参考文献

{{-}}

[Category:京津小吃](../Category/京津小吃.md "wikilink")
[Category:豬內臟食品](../Category/豬內臟食品.md "wikilink")
[Category:動物肝臟食品](../Category/動物肝臟食品.md "wikilink")

1.  北京市崇文区饮食资料汇编.p163
2.  《京城旧俗》，[爱新觉罗·瀛生](../Page/爱新觉罗·瀛生.md "wikilink")，北京燕山出版社 ISBN
    7540209690
3.