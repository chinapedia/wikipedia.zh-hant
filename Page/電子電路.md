[Intel_8742_153056995.jpg](https://zh.wikipedia.org/wiki/File:Intel_8742_153056995.jpg "fig:Intel_8742_153056995.jpg")
8742, an 8-bit [microcontroller](../Page/microcontroller.md "wikilink")
that includes a [CPU](../Page/CPU.md "wikilink"), 128
[bytes](../Page/byte.md "wikilink") of
[RAM](../Page/隨機存取記憶體.md "wikilink"), 2048 bytes of
[EPROM](../Page/EPROM.md "wikilink"), and
[I/O](../Page/Input/output.md "wikilink") in the same chip.\]\]
[PExdcr01CJC.jpg](https://zh.wikipedia.org/wiki/File:PExdcr01CJC.jpg "fig:PExdcr01CJC.jpg")
(PCB)所做的電路。\]\] **電子電路**（**Electronic
circuit**）：將各式各樣的[電子元件](../Page/電子元件.md "wikilink")，形成一迴路[電路](../Page/電路.md "wikilink")，進行[電信號的](../Page/電信號.md "wikilink")[運算](../Page/运算.md "wikilink")，電子元件形成電路為電子電路。

## 關於不同時期的電子電路

  - 研發與測試時期，為了方便進行測試電路的可行性，常使用[麵包板](../Page/麵包板.md "wikilink")（Breadboard）、、，來進行電路的組裝。
  - 量產時期，常使用[生產線用以大量製造](../Page/生產線.md "wikilink")[印刷電路板的電路](../Page/印刷電路板.md "wikilink")，來加速生產效能。
  - 改良製程時，常使用[半導體技術產生的IC](../Page/半導體.md "wikilink")（[積體電路](../Page/積體電路.md "wikilink")），來大幅減少電路的體積，經由大幅減少所使用電子元件，通常亦可改善耗電與散熱問題。

## 電子元件

  - [被動元件](../Page/被動元件.md "wikilink")，即[電阻](../Page/電阻.md "wikilink")、[電容](../Page/電容.md "wikilink")、[電感](../Page/電感.md "wikilink")、[二極體](../Page/二極體.md "wikilink")、[發光二極體](../Page/發光二極體.md "wikilink")··等，為電子電路一般常用元件。
  - [主動元件](../Page/主動元件.md "wikilink")，即[電晶體](../Page/電晶體.md "wikilink")、[積體電路](../Page/積體電路.md "wikilink")（矽片）··等，為電子電路一般常用元件。

## 電子電路的種類

### 類比電路

[Common_Base_amplifier.png](https://zh.wikipedia.org/wiki/File:Common_Base_amplifier.png "fig:Common_Base_amplifier.png")。\]\]
[Simple_electrical_schematic_with_Ohms_law.png](https://zh.wikipedia.org/wiki/File:Simple_electrical_schematic_with_Ohms_law.png "fig:Simple_electrical_schematic_with_Ohms_law.png")

### 數位電路

### 混合型電路

## 相關

  - [電路圖](../Page/電路圖.md "wikilink")
  - [電路設計](../Page/電路設計.md "wikilink")

## 參考書籍

[es:Circuito eléctrico](../Page/es:Circuito_eléctrico.md "wikilink")

[Category:電路](../Category/電路.md "wikilink")