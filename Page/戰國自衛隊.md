『**戰國自衛隊**』（せんごくじえいたい）是[半村良的](../Page/半村良.md "wikilink")[SF小說和](../Page/科幻小說.md "wikilink")1979年以此為原作，由[千葉真一主演的](../Page/千葉真一.md "wikilink")[電影](../Page/#電影.md "wikilink")，[票房達到](../Page/票房.md "wikilink")23億圓\[1\]。還有[劇畫](../Page/#劇畫.md "wikilink")、2005年的電影『[戰國自衛隊1549](../Page/戰國自衛隊1549.md "wikilink")』、2006年的[電視劇](../Page/電視劇.md "wikilink")『[戰國自衛隊・關原之戰](../Page/#電視劇.md "wikilink")』的[重製版](../Page/重製.md "wikilink")。

## 原作

「以近代兵器武裝的現代軍隊」和「以弓矢或刀武裝的戰國時代鎧武者軍團」作戰的話將會如何？」的[架空戰記](../Page/架空戰記.md "wikilink")。

## 電影

電影版本於1979年12月5日在日本首映，由[千葉真一主演兼武術指導](../Page/千葉真一.md "wikilink")。並於1980年8月28日至9月4日期間在香港及澳門的金公主院線上映\[2\]\[3\]，並以粵語配音播出。

## 劇畫

## 電視劇

## 脚注

## 關連項目

  - [自衛隊](../Page/自衛隊.md "wikilink")
  - 《》（2005年韓國電影）

## 外部連結

  - [戰國自衛隊
    (角川映畫)](http://www.kadokawa-pictures.jp/official/sengoku_jieitai/)
  - [陸上自衛隊](http://www.mod.go.jp/gsdf/)
  - [戰國自衛隊・關原之戰](http://www.ntv.co.jp/sengoku/)
  - [戰國自衛隊 | 半文居 | 半村良](http://hanmura.com/sengoku/)

[Category:日本科幻小說](../Category/日本科幻小說.md "wikilink")
[Category:1971年長篇小說](../Category/1971年長篇小說.md "wikilink")
[Category:日本自衛隊題材作品](../Category/日本自衛隊題材作品.md "wikilink")
[Category:戰國時代背景作品
(日本)](../Category/戰國時代背景作品_\(日本\).md "wikilink")
[Category:時間旅行題材作品](../Category/時間旅行題材作品.md "wikilink")
[Category:平行世界題材小說](../Category/平行世界題材小說.md "wikilink")
[Category:書籍改編電影](../Category/書籍改編電影.md "wikilink")
[Category:日本科幻片](../Category/日本科幻片.md "wikilink")
[Category:時間旅行電影](../Category/時間旅行電影.md "wikilink")
[Category:1979年電影](../Category/1979年電影.md "wikilink")
[Category:日本特摄电影](../Category/日本特摄电影.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink") [Category:DRAMA
COMPLEX](../Category/DRAMA_COMPLEX.md "wikilink")
[Category:2006年日本電視劇集](../Category/2006年日本電視劇集.md "wikilink")

1.
2.  [香港電影票房 1980
    \[外語電影](https://www.facebook.com/permalink.php?story_fbid=117628638922510&id=117441668941207)\]
3.  本港新聞第三張第四頁.華僑日報.1980年8月28日