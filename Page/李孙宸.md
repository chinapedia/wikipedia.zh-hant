**李孙宸**（），[字](../Page/表字.md "wikilink")**代玄**，一字**伯襄**，[號](../Page/號.md "wikilink")**小灣**\[1\]，[广东](../Page/广东.md "wikilink")[香山](../Page/香山.md "wikilink")（今[中山市](../Page/中山市.md "wikilink")）[小榄人](../Page/小欖鎮.md "wikilink")，[明朝政治人物](../Page/明朝.md "wikilink")，[進士出身](../Page/進士.md "wikilink")。

## 生平

李孙宸生于[明](../Page/明.md "wikilink")[万历四年](../Page/万历.md "wikilink")（1576年）。其家族為[小榄望族](../Page/小欖鎮.md "wikilink")。[万历四十一年](../Page/万历.md "wikilink")（1613年）登癸丑科[进士](../Page/进士.md "wikilink")，選[翰林院庶吉士](../Page/翰林院庶吉士.md "wikilink")。[泰昌元年](../Page/泰昌.md "wikilink")（1620年）掌[内书堂](../Page/内书堂.md "wikilink")，晋[詹事府](../Page/詹事府.md "wikilink")[春坊](../Page/春坊.md "wikilink")[左庶子](../Page/左庶子.md "wikilink")。[天启五年](../Page/天启_\(明朝\).md "wikilink")（1625年）进[南京](../Page/南京.md "wikilink")[國子監祭酒](../Page/國子監祭酒.md "wikilink")。次年晋[侍读学士](../Page/侍读学士.md "wikilink")，教习[庶吉士](../Page/庶吉士.md "wikilink")，晋南京[礼部右侍郎](../Page/礼部右侍郎.md "wikilink")，摄[礼](../Page/禮部.md "wikilink")、[户两部](../Page/戶部.md "wikilink")[尚书事](../Page/尚书.md "wikilink")。[天启六年](../Page/天启_\(明朝\).md "wikilink")（1626年）为了抵抗[清军入侵](../Page/清军.md "wikilink")，被任命为南京[兵部左右](../Page/兵部.md "wikilink")[侍郎](../Page/侍郎.md "wikilink")。[崇祯初年](../Page/崇祯.md "wikilink")（1628年）晋[礼部侍郎](../Page/礼部侍郎.md "wikilink")，掌[翰林院察典](../Page/翰林院.md "wikilink")。[崇祯三年](../Page/崇祯.md "wikilink")（1630年）回[礼部视事](../Page/礼部.md "wikilink")，晋[南京礼部尚书](../Page/南京礼部尚书.md "wikilink")。[崇祯六年](../Page/崇祯.md "wikilink")（1633年）三上[疏乞退归隐](../Page/奏疏.md "wikilink")，奉旨慰留。[崇祯七年](../Page/崇祯.md "wikilink")（1634年）八月病故于任，终年五十五岁\[2\]。

## 著作

著有《建霞楼集》十七卷\[3\]。其所撰之《两榄风景地势图说》文，对当时他的家乡[小榄有](../Page/小欖鎮.md "wikilink")“五松六路三丫水，洞梅花十二桥，岁岁菊花看不尽，诗坛酌酒赏花村”的赞美之句。

## 註釋

## 參考文獻

  - 張昇，《四庫全書提要稿輯存》，北京圖書館出版社，2006
  - 昌彼得、喬衍琯、宋常廉等，《明人傳記資料索引》，中華書局出版社，1987年

[Category:明朝庶吉士](../Category/明朝庶吉士.md "wikilink")
[Category:明朝南京國子監祭酒](../Category/明朝南京國子監祭酒.md "wikilink")
[Category:明朝翰林院侍讀學士](../Category/明朝翰林院侍讀學士.md "wikilink")
[Category:南京禮部侍郎](../Category/南京禮部侍郎.md "wikilink")
[Category:南京禮部尚書](../Category/南京禮部尚書.md "wikilink")
[Category:南京戶部尚書](../Category/南京戶部尚書.md "wikilink")
[Category:南京兵部侍郎](../Category/南京兵部侍郎.md "wikilink")
[Category:明朝禮部侍郎](../Category/明朝禮部侍郎.md "wikilink")
[Category:明朝书法家](../Category/明朝书法家.md "wikilink") [Category:香山县人
(中山)](../Category/香山县人_\(中山\).md "wikilink")
[S](../Category/李姓.md "wikilink")

1.  [小榄镇史事](http://www.2499cn.com/xiaolan.htm)
2.  《漢籍電子文獻資料庫-明人傳記資料索引》2187：“李孫宸，字伯襄，香山人。少警敏淹博，登萬曆四十一年進士，選庶起士，崇禎間屢官南京禮部尚書。性孝友廉介，詩祖三百篇，字祖晉魏，草隸篆楷皆工。有《建霞樓集》。”
3.  《江蘇採輯遺書目錄·集部》，頁48；張昇編本冊4，頁461