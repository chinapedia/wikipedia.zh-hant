[Makkaraperunat2.jpg](https://zh.wikipedia.org/wiki/File:Makkaraperunat2.jpg "fig:Makkaraperunat2.jpg")\]\]
[French's_classic_yellow_mustard.jpg](https://zh.wikipedia.org/wiki/File:French's_classic_yellow_mustard.jpg "fig:French's_classic_yellow_mustard.jpg")黃色芥末醬，美式芥末的一种\]\]
**芥末醬**，也稱**芥末**、**芥辣**或**芥辣醬**，芥末醬為一種芥末色稠狀物，具有強烈鮮明的味道，由[芥菜类蔬菜的](../Page/芥菜类蔬菜.md "wikilink")[籽研磨摻水](../Page/芥菜籽.md "wikilink")、醋或酒類調製而成，亦會添加香料或是其它添加劑藉以增香或是增色，如添加姜黃（增色及增香）。芥菜类蔬菜的三个種類的籽，包括白或黃芥末籽、褐色芥末籽或稱印度芥末、黑芥末籽都可以用于制作芥末。濃烈的芥末醬多會引起口腔以及呼吸道流暢不適，具一定之刺激性。

## 芥末醬種類

### 藍蕉芥末

在[山西](../Page/山西省.md "wikilink")、[陕西等地的芥末的味道較强烈](../Page/陕西省.md "wikilink")，以芥末籽研磨成粉加水蒸过，即可食用。一般用于凉面、凉粉的调味之用。

明朝劉伯溫在其著作「多能鄙事」裡，撰有【魚膾】一章，說明[魚生料理](../Page/魚生.md "wikilink")，已提及芥辣做為生魚片之蘸料，內文茲摘錄如下；

魚不拘大小，以鮮活為上。去頭尾肚皮，薄切，攤白紙上晾片時，細切如絲。以蘿蔔細剁，布紐作汁，薑絲伴魚入碟，雜以生菜、胡荽、芥辣、醋澆。

言中芥辣之稱呼現代尚使用中。

### 美式芥末（Prepared Mustard）

在美國最容易買到的芥末醬種類，一般含有芥末子、水、鹽、匈牙利辣椒粉、薑黃粉、大蒜粉、醋等成份。

### 第戎芥末醬（Dijon mustard）

[第戎芥末醬又稱法式芥末醬](../Page/第戎芥末醬.md "wikilink")，其名字並未受到歐盟保護或是AOP或是AOC等保護措施，芥末醬工廠位在法國[第戎](../Page/第戎.md "wikilink")（）城郊，但大部分命名為第戎的芥末醬多不是在法國第戎所製造，第戎芥末醬源於1856年，在法國第戎一叫Jean
Naigeon的人以尚未成熟的酸葡萄汁替代傳統芥末醬配方所用的酸醋。因位於第戎故稱第戎芥末醬。現今正統第戎芥末醬皆摻以[白葡萄酒和](../Page/白葡萄酒.md "wikilink")[博根地](../Page/勃艮第.md "wikilink")（）酒，而一般第戎芥末醬以一種或是兩種酒做為副原料。

### 顆粒芥末醬（Wholegrain）

顆粒芥末醬其內含未研磨芥末籽，以不同芥末籽混以香料達到不同的氣味與口感，如添加日曬番茄乾，以及辣椒等。

### 蜂蜜芥末（Honey Mustard）

因摻以蜂蜜而得名，該種類芥末醬多用於[三明治](../Page/三明治.md "wikilink")，以及作為[炸薯條](../Page/馬鈴薯條.md "wikilink")、[炸洋蔥圈和其它小食等的調味沾醬](../Page/炸洋蔥圈.md "wikilink")，其亦可與醋、橄欖油拌勻作為沙拉淋醬的基醬（Base）。一般蜂蜜芥末醬可由等份量蜂蜜及芥末醬混和製成，蜂蜜芥末醬有著多種變化，藉由添加其他作料以增味、调节稠度，或是其他性質。

## 近似芥末的调味品

[山葵及其代用品](../Page/山葵.md "wikilink")[辣根常被称为](../Page/辣根.md "wikilink")“绿芥末\[1\]”或“”，這是因為山葵的气味或味道與芥末类似；但实际上，山葵属于[山萮菜屬](../Page/山萮菜屬.md "wikilink")，辣根则属于[辣根属](../Page/辣根属.md "wikilink")，與芥末是不相關的。

## 用途

芥末經常用作[肉類的調味品](../Page/肉類.md "wikilink")，特別如[火腿的](../Page/火腿.md "wikilink")[冷盤](../Page/冷盤.md "wikilink")。[法國人喜歡吃](../Page/法國人.md "wikilink")[牛排時用強烈的](../Page/牛排.md "wikilink")[第戎芥末](../Page/第戎.md "wikilink")。[廣東地區則為](../Page/廣東.md "wikilink")[燒肉的佐料](../Page/燒肉.md "wikilink")。芥末醬也可用作[蛋黃醬](../Page/蛋黃醬.md "wikilink")、[醋](../Page/醋.md "wikilink")、[滷汁或](../Page/滷汁.md "wikilink")[烤肉汁的成份](../Page/烤肉汁.md "wikilink")。

## 參考文獻

[Category:調味料](../Category/調味料.md "wikilink")
[Category:辣酱](../Category/辣酱.md "wikilink")

1.