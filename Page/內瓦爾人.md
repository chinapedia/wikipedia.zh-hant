**內瓦爾人**是一群位於[尼泊爾之](../Page/尼泊爾.md "wikilink")[加德滿都峽谷的](../Page/加德滿都.md "wikilink")[原住民](../Page/原住民.md "wikilink")。根據尼泊爾的2001年的[人口普查](../Page/人口普查.md "wikilink")，內瓦爾人的總人口為1,245,232人，是該國的第六大民族，占全國人口的5.48％。

## 歷史

尼瓦尔人史前就生活在[加德滿都](../Page/加德滿都.md "wikilink")[峽谷](../Page/峽谷.md "wikilink")，他们与不同时期来到的藏缅语族及[雅利安人混合](../Page/雅利安人.md "wikilink")。有人說他們的祖先是來自南印度[喀拉拉邦的](../Page/喀拉拉.md "wikilink")[納雅爾人](../Page/納雅爾.md "wikilink")（屬[首陀羅種姓](../Page/首陀羅.md "wikilink")）。

中世纪統治的[尼波羅國](../Page/尼波羅國.md "wikilink")（約公元400年至750年）就是加德滿都，尼瓦尔就是尼泊尔。後來內瓦爾人建立了[馬拉王朝](../Page/馬拉王朝.md "wikilink")（公元12世紀至公元18世紀）。到了1769年時，內瓦爾人在加德滿都山谷與邊地區的勢力終結，被[沙阿王朝征服](../Page/沙阿王朝.md "wikilink")。

## 語言

雖然[尼瓦尔语是屬於](../Page/尼瓦尔语.md "wikilink")[藏緬語系的一個](../Page/藏緬語族.md "wikilink")[語族](../Page/語族.md "wikilink")，但它受到[印度-伊朗語系的各種方言例如](../Page/印度-伊朗语族.md "wikilink")[梵語](../Page/梵語.md "wikilink")、[巴利語及](../Page/巴利語.md "wikilink")[孟加拉語之影響相當大](../Page/孟加拉語.md "wikilink")。內瓦爾語也包含某些[南亞語系語詞](../Page/南亞語系.md "wikilink")，有82萬尼泊爾人把內瓦爾語視為母語。

## 宗教

內瓦爾人主要信仰[佛教和](../Page/佛教.md "wikilink")[印度教](../Page/印度教.md "wikilink")。根據2001年的人口調查，84.13％的尼瓦爾人信印度教，15.31％信佛教。他们佛教受[密宗很大影响](../Page/密宗.md "wikilink")，很多人同时信仰两教，他们佛教徒内也有种姓。

## 音樂

大部分尼瓦爾樂器是屬於[打擊樂器之類](../Page/打擊樂器.md "wikilink")。[吹奏樂器如](../Page/吹奏樂器.md "wikilink")[長笛等也較普遍](../Page/長笛.md "wikilink")，而[弦樂器非常罕見](../Page/弦樂器.md "wikilink")。有些歌曲為特定季節或節日專用。

## 烹調

尼瓦爾的典型[烹飪是較為特殊](../Page/烹飪.md "wikilink")，包含非素食和[素食物品以及非](../Page/素食主義.md "wikilink")[酒精飲料](../Page/酒精.md "wikilink")。[芥末油](../Page/芥末.md "wikilink")、有很多的調料、如[孜然](../Page/孜然.md "wikilink")、[芝麻](../Page/芝麻.md "wikilink")、[薑黃](../Page/薑黃.md "wikilink")、[蒜](../Page/蒜.md "wikilink")、[生薑](../Page/生薑.md "wikilink")、[丁香](../Page/丁香.md "wikilink")、[肉桂](../Page/肉桂.md "wikilink")、[辣椒](../Page/辣椒.md "wikilink")、[芥末](../Page/芥末.md "wikilink")、[醋等等都用於烹調](../Page/醋.md "wikilink")。

## 建築物

尼瓦爾的建築包括[佛塔風格](../Page/佛塔.md "wikilink")、利塔風格、喜克拉式風格等等。[帕坦被称为佛教藝術之都](../Page/帕坦.md "wikilink")。

## 参见

  - [尼瓦尔灯节](../Page/尼瓦尔灯节.md "wikilink")

## 參考資料

  - [尼泊爾民族博物館](https://web.archive.org/web/20041009221339/http://www.welcomenepal.com/emuseum.asp)

[Category:尼泊爾民族](../Category/尼泊爾民族.md "wikilink")
[Category:南亞原住民](../Category/南亞原住民.md "wikilink")