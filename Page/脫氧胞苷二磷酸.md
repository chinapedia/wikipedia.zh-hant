**去氧胞苷二磷酸**（**Deoxycytidine
diphosphate**，**dCDP**）是一種[核苷酸](../Page/核苷酸.md "wikilink")，是[胞苷三磷酸](../Page/胞苷三磷酸.md "wikilink")（CTP）的衍生物，比CTP少了一個位於[五碳糖](../Page/五碳糖.md "wikilink")2號碳上的-OH基，以及一個[磷酸根](../Page/磷酸根.md "wikilink")。




















[Category:核苷酸](../Category/核苷酸.md "wikilink")