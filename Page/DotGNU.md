**DotGNU**計畫是[GNU為了提供](../Page/GNU.md "wikilink")[Microsoft
.NET一份](../Page/Microsoft_.NET.md "wikilink")[自由软件替代品的一部份](../Page/自由软件.md "wikilink")。這個計畫其它的目標包含了支援非[Windows平台和支援更多的](../Page/Microsoft_Windows.md "wikilink")[中央处理器](../Page/中央处理器.md "wikilink")。

DotGNU主要的目標是要提供一個百分之百相容於[通用语言架构的類別庫](../Page/通用语言架构.md "wikilink")。然而矛盾的是，另外一個開放源始碼的實作，[Mono提供了百分之百相容於CLS規範的類別庫並且提供了目前由微軟所釋出的商業版本](../Page/Mono.md "wikilink").NET的類別庫。DotGNU計畫則宣稱這些由微軟所釋出的商業版本之類別庫並沒有完全遵循在[ECMA所發行的CLS標準規範](../Page/ECMA.md "wikilink")。

## 內部連結

  - [C\#](../Page/C_Sharp.md "wikilink")

  - [F\#](../Page/F_Sharp.md "wikilink")

  - [Mono](../Page/Mono.md "wikilink")

  -
  - [.NET](../Page/.NET.md "wikilink")

[Category:计算平台](../Category/计算平台.md "wikilink")
[Category:GNU](../Category/GNU.md "wikilink")
[Category:GNU計劃軟體](../Category/GNU計劃軟體.md "wikilink")