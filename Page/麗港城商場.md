\-{H|晒草灣}-

[Laguna_Plaza.JPG](https://zh.wikipedia.org/wiki/File:Laguna_Plaza.JPG "fig:Laguna_Plaza.JPG")
[HK_Kwun_Tong_麗港城商場_Laguna_Plaza_main_Entrance.JPG](https://zh.wikipedia.org/wiki/File:HK_Kwun_Tong_麗港城商場_Laguna_Plaza_main_Entrance.JPG "fig:HK_Kwun_Tong_麗港城商場_Laguna_Plaza_main_Entrance.JPG")入口\]\]
[Laguna_Plaza_overlook.JPG](https://zh.wikipedia.org/wiki/File:Laguna_Plaza_overlook.JPG "fig:Laguna_Plaza_overlook.JPG")

**麗港城商場**（）為[香港極少數單幢式商場](../Page/香港.md "wikilink")，位於[藍田](../Page/藍田_\(香港\).md "wikilink")[麗港城](../Page/麗港城.md "wikilink")，總樓面面積約
15,200平方米，共有5層，地庫二層為停車場。

麗港城商場內有多間[連鎖式](../Page/連鎖店.md "wikilink")[快餐店及](../Page/快餐店.md "wikilink")[酒樓等食肆](../Page/酒樓.md "wikilink")，諸如[美心MX](../Page/美心MX.md "wikilink")、[大快活](../Page/大快活.md "wikilink")、[一粥麵](../Page/一粥麵.md "wikilink")、[麥當勞](../Page/麥當勞.md "wikilink")、[八方雲集](../Page/八方雲集.md "wikilink")、魚米、東南加、尚點、西佑定食屋、唐記包點、PHD（薄餅博士）、魚味市場壽司刺身專門店、[鴻福堂](../Page/鴻福堂.md "wikilink")、[Pacific
Coffee](../Page/Pacific_Coffee.md "wikilink")、CoCo都可、[美心西餅](../Page/美心西餅.md "wikilink")、[東海堂](../Page/東海堂.md "wikilink")、豪宴海鮮酒家等，為附近居民及上班人士服務。

## 歷史

  - 業權：商場發展商為[長江實業](../Page/長江實業.md "wikilink")、[蜆殼石油與](../Page/蜆殼石油.md "wikilink")[和記黃埔地產](../Page/和記黃埔地產.md "wikilink")，[恒隆地產在](../Page/恒隆地產.md "wikilink")1992年斥資7.17億元購入商場。\[1\]2012年8月3日，[里昂證券旗下CLSA](../Page/里昂證券.md "wikilink")
    Capital Partners的房地產基金LGF Investment
    Limited斥資15億元向[恒隆地產購入麗港城商場連](../Page/恒隆地產.md "wikilink")165個車位，以商場總樓面約16.36萬方呎計，呎價約9169元。\[2\]2014年12月，[置富產業信託落實以](../Page/置富產業信託.md "wikilink")19.185億元作價向里昂購入麗港城商場，與同是置富旗下的第3期商場[城中薈形成協同效應](../Page/城中薈.md "wikilink")。\[3\]

<!-- end list -->

  - 地庫一層發展：麗港城商場最初於地庫一層設有街市和千島美食美食廣場。街市結業後，由街市旁的[百佳超級市場承接街市面積成濕貨部](../Page/百佳超級市場.md "wikilink")，結合原有百佳超級市場面積，第一次擴張並升格為[百佳超級廣場](../Page/百佳超級市場.md "wikilink")，多年後更將乾貨部與濕貨部位置對調。其後千島美食美食廣場結業後，也被原為美食廣場食肆之一的大快活及[日本城全面取代](../Page/日本城.md "wikilink")，大快活更成為其商標改革成「大」字形後第一間翻新的分店，以及曾經成為全港唯一一間設有[遊樂場設施](https://www.youtube.com/watch?v=zoZ5segUkmU)的分店，後來大快活遊樂場設施位置被[大昌食品專門店及PHD](../Page/大昌食品專門店.md "wikilink")（薄餅博士）取代。其後[大快活遷往](../Page/大快活.md "wikilink")2樓後，由百佳超級廣場承接前大快活面積，並將濕貨部換遷至該位置，結合原有百佳超級廣場面積，第二次擴張並升格為[fusion
    by PARKnSHOP](../Page/百佳超級市場.md "wikilink")。

<!-- end list -->

  - 二樓發展：麗港城商場最初於二樓全層設有美心大酒樓及潮江春。兩間酒樓結業後二樓全層曾一度封閉。其後潮江春位置被分拆成[柏斯琴行](../Page/柏斯琴行.md "wikilink")、ABC
    Pathways
    School等多間商舖，而美心大酒樓位置經過多番酒樓轉手至稻香超級漁港。其後稻香超級漁港結業後被分拆成豪宴海鮮酒家及真開心樂園等多間商舖，而柏斯琴行縮細後餘下的面積也被分拆成[759阿信屋等多間商舖](../Page/759阿信屋.md "wikilink")，兩大舖位縮細後更滕出近商場中庭位置以增建商場走廊。

## 連鎖商戶

  - 地庫1層：
      - [fusion by PARKnSHOP](../Page/百佳.md "wikilink")(1992年開業，其後兩度擴張)
      - [大昌食品專門店](../Page/大昌食品市場.md "wikilink")
      - 唐記包點
      - PHD（薄餅博士）
      - [日本城](../Page/日本城.md "wikilink")
      - 名果店
  - 地下：
      - [7-11便利店](../Page/7-11便利店.md "wikilink")(24小時營業)(1992年開業至今)
      - [美心西餅](../Page/美心西餅.md "wikilink")(1992年開業，其後搬遷)
      - [東亞銀行](../Page/東亞銀行.md "wikilink")(1992年開業，其後擴張)
      - [麥當勞](../Page/麥當勞.md "wikilink")
      - [Pacific Coffee](../Page/Pacific_Coffee.md "wikilink")
      - [鴻福堂](../Page/鴻福堂.md "wikilink")
      - 魚米
      - 東南加
      - 魚味市場壽司刺身專門店
      - 八方雲集
      - CoCo都可
      - [萬寧](../Page/萬寧.md "wikilink")
      - 美康
      - HKTVmall
      - 和順堂
  - 1樓：
      - [美心MX](../Page/美心MX.md "wikilink")(1992年開業至今)
      - [屈臣氏](../Page/屈臣氏.md "wikilink")(1992年開業，其後縮細)
      - [一粥麵](../Page/一粥麵.md "wikilink")
      - [東海堂](../Page/東海堂.md "wikilink")
      - 尚點
      - 西佑定食屋
      - [恒生銀行自助理財中心](../Page/恒生銀行.md "wikilink")
      - Perfect Ten單剪
      - 景品捕屋
  - 2樓：
      - [大快活](../Page/大快活.md "wikilink")(1992年開業，其後擴張及搬遷)
      - [柏斯琴行](../Page/柏斯琴行.md "wikilink")(1992年開業，其後搬遷及縮細)
      - [759阿信屋](../Page/759阿信屋.md "wikilink")
      - 豪宴海鮮酒家
      - 真開心樂園
      - ABC Pathways School

## 非連鎖商戶

  - 飲食：麵飽西餅、藥房(中西)。
  - 禮品：服飾、眼鏡。
  - 服務：家居佈置、電腦維修、攝影、洗衣、理髮、美容、醫療中心、補習社、僱傭中心、地產代理等。

## 已結業連鎖商戶

  - 地庫：千島美食
  - 地下：[必勝客](../Page/必勝客.md "wikilink")、TCBY(全線結業)、[海天堂](../Page/海天堂.md "wikilink")(搬遷至麗港城第三期麗港中心)、[聖安娜餅屋](../Page/聖安娜餅屋.md "wikilink")、[南洋商業銀行](../Page/南洋商業銀行.md "wikilink")、[北京同仁堂](../Page/北京同仁堂.md "wikilink")、[位元堂](../Page/位元堂.md "wikilink")、香港18、[味千拉麵](../Page/味千拉麵.md "wikilink")、[華潤堂](../Page/華潤堂.md "wikilink")
  - 1樓：[豐澤電器](../Page/豐澤電器.md "wikilink")、POPI、[地利店](../Page/地利店.md "wikilink")(全線結業)、[優之良品](../Page/優之良品.md "wikilink")、[恒生銀行](../Page/恒生銀行.md "wikilink")、[實惠](../Page/實惠.md "wikilink")、仙跡岩、阿波羅、家得路、[商務印書館](../Page/商務印書館.md "wikilink")、[快圖美](../Page/快圖美.md "wikilink")、[波仔飯](../Page/波仔飯.md "wikilink")、[肯德基](../Page/肯德基.md "wikilink")、四海遊龍
  - 2樓：[美心大酒樓](../Page/美心大酒樓.md "wikilink")(全線結業)、[潮江春](../Page/潮江春.md "wikilink")、雅詩家居設計、[三聯書店](../Page/三聯書店.md "wikilink")、[椰林閣餐廳](../Page/椰林閣餐廳.md "wikilink")、君臨海鮮酒家、[稻香超級漁港](../Page/稻香.md "wikilink")、百樂門囍宴、Ingrid
    Millet Paris

## 設施

  - 出入口：8個(地庫1層(扶手電梯及樓梯)、地面(斜道/樓梯)、1樓(有蓋天橋))
  - 樓梯：1條24小時對外開放(來往地面及1樓有蓋天橋的樓梯(4號樓梯))
  - 升降機：3部客用(地庫1層至2樓)(只有最左一部可到達地庫2層)
  - 扶手電梯：7部(地庫1層至2樓)
  - 客戶服務中心：地面天幕中庭側
  - 洗手間/殘疾人士洗手間：3個對外開放(地庫1層、地面、2樓)
  - 停車場：地庫2層
  - 卸貨區: 地庫1層

## 電視廣告

[麗港城商場](https://www.youtube.com/watch?v=NELOXqlDAh4)

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[藍田站D](../Page/藍田站.md "wikilink")1出口（設有行人天橋接駁步行5至10分鐘）

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小型巴士.md "wikilink")

<!-- end list -->

  - 停車場

商場設有時租及月租車位

</div>

</div>

## 參考資料

## 相關條目

  - [藍田](../Page/藍田_\(香港\).md "wikilink")
  - [晒草灣](../Page/晒草灣.md "wikilink")
  - [麗港城](../Page/麗港城.md "wikilink")
      - [城中薈](../Page/城中薈.md "wikilink")（麗港城第三期）
      - 麗港中心（麗港城第三期）
      - [麗港公園](../Page/麗港公園.md "wikilink")

{{-}}

[Category:觀塘區商場](../Category/觀塘區商場.md "wikilink")
[Category:晒草灣](../Category/晒草灣.md "wikilink")
[Category:置富產業信託物業](../Category/置富產業信託物業.md "wikilink")

1.  [CY密友陳啟宗反常態散貨
    恒隆44億洽售商場商廈](http://hk.apple.nextmedia.com/financeestate/art/20120629/16470107)
    蘋果日報，2012年6月29日
2.  [蘋果日報：里昂炒港樓　首注買麗港城商場](http://hk.apple.nextmedia.com/financeestate/art/20120821/16621808)，2012年8月21日
3.  [置富產業信託以19.185億港元收購麗港城商場](http://www.fortunereit.com/upload/press/79/Turbo%202%20-%20press%20release%20C%20FINAL.pdf)
    置富新聞稿，2014年12月8日