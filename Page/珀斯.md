**珀斯**（）是[西澳大利亞州的](../Page/西澳大利亞州.md "wikilink")[首府](../Page/首府.md "wikilink")，也是[澳大利亞](../Page/澳大利亞.md "wikilink")[第四大城市](../Page/澳大利亚城市人口列表.md "wikilink")。根據2008年6月的[人口統計](../Page/澳大利亞人口.md "wikilink")，珀斯[都會區的人口共有](../Page/都會區.md "wikilink")202萬人，是澳洲第四大城市，人口增长率高於國家統計的平均水準。

由於地處[澳洲大陸西岸](../Page/澳大利亞洲.md "wikilink")[地中海氣候地區](../Page/地中海氣候.md "wikilink")，擁有溫和的氣候與[天鵝河岸的別緻景色](../Page/天鵝河_\(西澳\).md "wikilink")，使珀斯得以成為非常受歡迎的觀光[旅遊目的地](../Page/旅遊.md "wikilink")。

珀斯擁有廣闊的居住空間及高水準的生活質素，於每年的[世界最佳居住城市評選中都是名列前茅](../Page/世界最佳居住城市.md "wikilink")，反映出珀斯無論居住環境，生活質素及社會福利等都是极佳的城市。珀斯人的友善态度世界公认，曾於2003年獲得世界最友善城市称号\[1\]，得到世界性的讚賞及認同。

## 歷史

[Flag_of_Western_Australia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Western_Australia.svg "fig:Flag_of_Western_Australia.svg")
[The_Foundation_of_Perth.jpg](https://zh.wikipedia.org/wiki/File:The_Foundation_of_Perth.jpg "fig:The_Foundation_of_Perth.jpg")

雖然在珀斯之前，[英軍早就在](../Page/英國陸軍.md "wikilink")1826年時在西澳洲南面海岸的（後來改名為「[奧班尼](../Page/奧班尼.md "wikilink")」）建立了一個基地，以避免傳言中[法國要兼併澳大利亞西部的可能](../Page/法國.md "wikilink")，但珀斯仍然是整個澳大利亞西部第一個建立的殖民城鎮。1829年建城的珀斯剛開始時是自由屯墾殖民地──的首府。珀斯是由爵士所命名，並選擇此作為新城鎮。史特靈是[蘇格蘭航海家](../Page/蘇格蘭.md "wikilink")，與當時英國殖民地大臣──[美利爵士](../Page/美利爵士.md "wikilink")（Sir
George
Murray）的願望一致，決議以墨瑞的出生地和他在[英國下議院裡](../Page/英國下議院.md "wikilink")，議會席位的所在地──[珀斯郡](../Page/珀斯-金罗斯.md "wikilink")（）命名為珀斯。該殖民地在1850年之後改名為「西澳大利亞」（簡稱「西澳」），並開始成為被[英國放逐海外的](../Page/英國.md "wikilink")[囚犯所寄居的第二故鄉](../Page/囚犯.md "wikilink")，以補足當地農業與商業發展過程中非常欠缺的人力資源。

1900年，經公民投票，西澳在1901年正式加入了[澳洲聯邦](../Page/澳洲聯邦.md "wikilink")，亦是澳大利亞最後一個同意加入聯邦的[殖民地](../Page/殖民地.md "wikilink")。當時其它前殖民地為了說服西澳洲願意加入，作出了各種讓步，包括從東部建築[鐵路幹線](../Page/鐵路.md "wikilink")（經Kalgoorlie）連接到珀斯等。

1933年，在[公民](../Page/公民.md "wikilink")[投票下](../Page/投票.md "wikilink")，西澳表決脫離澳洲聯邦，當時以大比數傾向於贊成獨立。但是，表決前不久剛好正值國會[選舉](../Page/選舉.md "wikilink")，結果當時的政府下台。而新上任的政府並不贊成獨立行動。但鑒於公民投票的結果，新政府仍然向[英國訴請獨立](../Page/英國.md "wikilink")，只是申請被駁回。

在發展歷程中，珀斯得以繁榮的主要原因，是成為[天然資源產業的一個重要服務中心](../Page/天然資源.md "wikilink")。作為一個最接近對[金](../Page/金.md "wikilink")、[鐵礦](../Page/鐵礦.md "wikilink")、[鎳](../Page/鎳.md "wikilink")、[鋁土](../Page/鋁土.md "wikilink")、[金剛石](../Page/金剛石.md "wikilink")、[礦物沙](../Page/礦物沙.md "wikilink")、[煤炭](../Page/煤炭.md "wikilink")、[油以及](../Page/油.md "wikilink")[天然氣龐大儲備的城市](../Page/天然氣.md "wikilink")，大多世界的主要能源和工程企業在珀斯皆設有辦公室。

踏入1990年代，大多數時間都是由所領導的自由黨政府當政，珀斯亦從這時期開始步向繁榮發展。

## 地理

[Perth_locator-MJC.png](https://zh.wikipedia.org/wiki/File:Perth_locator-MJC.png "fig:Perth_locator-MJC.png")的位置（紅點所示）\]\]
珀斯中心坐标为[南緯](../Page/南緯.md "wikilink")31度52分48秒，[東經](../Page/東經.md "wikilink")115度52分58秒。全市面積5,386平方千米。西面濒临[印度洋](../Page/印度洋.md "wikilink")，距非洲海岸约7350千米；東面是內陸沙漠地帶。

珀斯属[地中海气候](../Page/地中海气候.md "wikilink")，四季较为分明。夏季干燥,较为炎热；冬季溫和湿润，在天气晴好时气温不会太低，只有清晨及晚間可能比較冷凉，气温日较差有時可高達20℃，但气温降至0℃及以下的情况极其少见。最熱月為2月，平均气温24.5℃，极端最高气温46.2℃（1991年2月23日）；最冷月为7月，平均气温13.0℃，极端最低气温-0.7℃（2006年6月17日）。年平均降水量746.9毫米。

## 政府體制

[governmentWesternAustralia.jpg](https://zh.wikipedia.org/wiki/File:governmentWesternAustralia.jpg "fig:governmentWesternAustralia.jpg")
根據澳大利亞憲法，西澳是依照[西敏制](../Page/西敏制.md "wikilink")（即[英國式](../Page/英國.md "wikilink")[議會制](../Page/議會.md "wikilink")）的形式來施行[三權分立的管治的](../Page/三權分立.md "wikilink")。除了被聯邦分割了些立法權和司法權外，其它方面保持完全獨立，其中：

  - [立法權取決於西澳的議會](../Page/立法權.md "wikilink")，即西澳立法委員會與西澳立法局。
  - [行政權取決於行政委員會](../Page/行政權.md "wikilink")，成員包括州長和資深部長。行政權由西澳州長和內閣行使，由西澳總督代表英國女皇任命。
  - [司法權由西澳最高法院和次級法院體系行使](../Page/司法權.md "wikilink")，但澳大利亞高等法院有最後的司法權。在澳大利亞憲法授予聯邦政府權力制定為法律的情況下，其它聯邦法庭亦有一定的司法權。

西澳州議會位於首府珀斯，隸屬西澳州議會的[都會區劃分為超過三十多個](../Page/都會區.md "wikilink")[地方政府區域](../Page/西澳大利亞州地方政府區域.md "wikilink")。

## 經濟

[City-of-Perth.jpg](https://zh.wikipedia.org/wiki/File:City-of-Perth.jpg "fig:City-of-Perth.jpg")\]\]
在过去，相比於[澳洲其他主要大](../Page/澳洲.md "wikilink")[城市](../Page/城市.md "wikilink")，珀斯有着較高的生活水準。原因在于，珀斯[都會區各行各業的一般工資虽略低於其他大城市](../Page/都會區.md "wikilink")，但[房地產價格遠低於](../Page/房地產.md "wikilink")[東岸城市](../Page/澳洲東岸.md "wikilink")。但自2005年至2006年的兩年間，情況卻正在轉變，珀斯的[房地產價格大幅度上升](../Page/房地產.md "wikilink")，而其他東岸城市的升幅卻很輕微，[悉尼更有下跌的趨勢](../Page/悉尼.md "wikilink")，使得珀斯與各城市的生活水準日漸拉近。

珀斯一如其他東岸的城市，一般屬於都會區內的職位都能提供，惟職位供應的數量普遍較[悉尼及](../Page/悉尼.md "wikilink")[墨爾本略少](../Page/墨爾本.md "wikilink")。

[西澳擁有極豐盛的](../Page/西澳大利亞.md "wikilink")[天然資源](../Page/天然資源.md "wikilink")——[煤及](../Page/煤.md "wikilink")[金屬矿藏](../Page/金屬.md "wikilink")。許多採礦業及與[礦物有關的行業](../Page/礦物.md "wikilink")，在珀斯設有[總部](../Page/總部.md "wikilink")。雖然採礦的工程都在珀斯以外的區域進行，但珀斯是提供工程師及專業人材技術支援的主要來源。距離珀斯30公里以南之[奎那那市的一所龐大](../Page/奎那那.md "wikilink")[煉油廠](../Page/煉油廠.md "wikilink")，所需的工程及化學專業人員是來自珀斯，亦為珀斯帶動了就業市場。

此外，[農業及](../Page/農業.md "wikilink")[旅遊業亦在珀斯的經濟中占有相当份额](../Page/旅遊業.md "wikilink")。

## 居住概況

[ScarborougHouse.jpg](https://zh.wikipedia.org/wiki/File:ScarborougHouse.jpg "fig:ScarborougHouse.jpg")
珀斯主要分為240多個[區域](../Page/郊區.md "wikilink")，基本上每個區也有住宅。由於土地遼闊，房子一般都寬敞舒適，大部份設有[花園](../Page/花園.md "wikilink")，[當地政府更規定業主要在花園種植花草以美化環境](../Page/西澳洲政府.md "wikilink")。房屋一般以紅瓦屋頂的[平房](../Page/平房.md "wikilink")，或比較新式的特色樓房，或再高尚些的[別墅為主](../Page/別墅.md "wikilink")，亦有少數的[大廈](../Page/公寓大廈.md "wikilink")（Apartment）座落於某些比較城市的區域，如珀斯，、[Subiaco等](../Page/:en:Subiaco,_Western_Australia.md "wikilink")。一般来说日落海岸、西区和天鹅河附近的郊区房价较贵，近年来内城区的一些郊区的房价呈高速上涨趋势，如
West Leederville、East
Perth等。房价较低地区则是一些离海滩，天鹅河及市中心相对较偏远的郊区，如东面的Midland、Guildford；南面的Armadale。由于矿产资源丰富，珀斯房价过去十多年间高速上涨，但是由于最近几年矿业不景气，珀斯房地产进入调整期。

## 旅遊

### 費利曼圖

[FremantleMonument(alfeewusy).jpg](https://zh.wikipedia.org/wiki/File:FremantleMonument\(alfeewusy\).jpg "fig:FremantleMonument(alfeewusy).jpg")海軍紀念碑\]\]
[費利曼圖](../Page/費利曼圖.md "wikilink")(Fremantle)距離珀斯市以南約19公里（12英里）的一個海港城市，澳洲西海岸天鵝河入海口。費利曼圖於1829年成為天鵝河畔的第一個居民點。在1929年被設立為市，現約有2.6萬人口。

費利曼圖是一個古意盎然的海港城市。該市的大部分建築物已被[當地政府列入保護古蹟](../Page/西澳洲政府.md "wikilink")，故整個城市也仍舊保留著一百年前的風貌。当地值得参觀的景點有：

  - 西澳軍事博物館（[Army Museum of Western
    Australia](../Page/:en:Army_Museum_of_Western_Australia.md "wikilink")）
  - 費利曼圖舊監獄（[Fremantle
    Prison](../Page/:en:Fremantle_Prison.md "wikilink")）
  - 費利曼圖市場（[Fremantle
    Market](../Page/:en:Fremantle_Markets.md "wikilink")）

等等。

### 海灘

[ScarboroughBeach(alfeewusy).jpg](https://zh.wikipedia.org/wiki/File:ScarboroughBeach\(alfeewusy\).jpg "fig:ScarboroughBeach(alfeewusy).jpg")
珀斯有很多著名的[海灘](../Page/海灘.md "wikilink")，其景色之優美程度足以和澳洲東岸的[黃金海岸相媲美](../Page/黄金海岸_\(澳大利亚\).md "wikilink")。由眾多著名海灘連接而成之海岸線名為[日落海岸](../Page/日落海岸.md "wikilink")，該海岸覆蓋範圍包括南面的費利曼圖海岸以北至[君达乐](../Page/君达乐.md "wikilink")（Joondalup），當中包括的海灘多達20幾個，如：

  - 布萊頓海灘（[Brighton
    Beach](../Page/:en:Brighton,_Western_Australia.md "wikilink")）
  - 科茨洛海灘（[Cottesloe
    Beach](../Page/:en:Cottesloe,_Western_Australia.md "wikilink")）
  - 斯旺伯恩海灘（[Swanbourne
    Beach](../Page/:en:Swanbourne,_Western_Australia.md "wikilink")，著名[天體沙灘](../Page/天體.md "wikilink")）
  - 洛里厄特海灘（[Floreat
    Beach](../Page/:en:Floreat,_Western_Australia.md "wikilink")）
  - 斯卡伯勒海灘（[Scarborough
    Beach](../Page/:en:Scarborough,_Western_Australia.md "wikilink")，著名[衝浪聖地](../Page/衝浪.md "wikilink")）
  - 城市海灘（[City Beach](../Page/:en:City_Beach.md "wikilink")）
  - 北部海灘（[North
    Beach](../Page/:en:North_Beach,_Western_Australia.md "wikilink")）
  - 沃特曼海灘（[Waterman
    Beach](../Page/:en:Waterman,_Western_Australia.md "wikilink")）
  - 索倫托海灘（[Sorrento
    Beach](../Page/:en:Sorrento,_Western_Australia.md "wikilink")）

等等。

### 國王公園

[WildFlowers.jpg](https://zh.wikipedia.org/wiki/File:WildFlowers.jpg "fig:WildFlowers.jpg")
國王公園（[Kings
Park](../Page/:en:Kings_Park,_Western_Australia.md "wikilink")）座落於天鵝河畔的伊萊山（[Mt.
Eliza](../Page/:en:Mount_Eliza,_Western_Australia.md "wikilink")）之上，佔地相當廣闊達400公頃，盤踞整個山頭，公園內茂樹林立，樹高比天，壯麗非常。園內的設計一方面盡量保持著原始大自然氣息，有古老的觀光電車穿梭往來園內，及騎[駱駝遊覽公園等項目](../Page/駱駝.md "wikilink")，都令人覺得有回歸原始的感覺.另一方面又增加了不少[現代化設施以方便](../Page/現代化.md "wikilink")[遊客](../Page/遊客.md "wikilink")，如設有在柏斯非常著名的5星級餐廳Frasers
Restaurant、燒烤爐，[溫室及](../Page/溫室.md "wikilink")[觀景台更是俯瞰全珀斯市](../Page/觀景台.md "wikilink")[全景的最佳位置](../Page/天際線.md "wikilink")。原始與現代化交織而成的Kings
Park,卻有著以想不到和諧與協調。特別一提的是每年的9\~11月是珀斯的[春天](../Page/春天.md "wikilink")，是觀賞野花盛開的季節，每年那個時候都會在Kings
Park舉行野花節給遊客觀賞，無需他們跑到遙遠的西澳郊區才可欣賞到。[公園內原本廣大的綠色草原](../Page/公園.md "wikilink")，頓然變成一片色彩萬千的花海，非常漂亮悅目。
[Perth_foreshore_panorama.jpg](https://zh.wikipedia.org/wiki/File:Perth_foreshore_panorama.jpg "fig:Perth_foreshore_panorama.jpg")的前濱全景\]\]

### 中心商業區

[Trinity_Arcade_in_St_Georges_Terrace.jpg](https://zh.wikipedia.org/wiki/File:Trinity_Arcade_in_St_Georges_Terrace.jpg "fig:Trinity_Arcade_in_St_Georges_Terrace.jpg")
珀斯的[中心商業區](../Page/中心商業區.md "wikilink")（）是該市的購物中心，主要由以下幾條街道組成：

  - [海伊街](../Page/海伊街.md "wikilink")
  - [穆雷街](../Page/穆雷街.md "wikilink")
  - [巴拉克街](../Page/巴拉克街.md "wikilink")
  - [William Street](../Page/:en:William_Street,_Perth.md "wikilink")
  - [King Street](../Page/:en:King_Street,_Perth.md "wikilink")
  - [聖喬治露台](../Page/聖喬治露台.md "wikilink")

在中央商業區，值得參觀的地方有[柏斯錢幣鑄造局](../Page/珀斯铸币厂.md "wikilink")、倫敦閣（[London
Court](../Page/:en:London_Court,_Perth.md "wikilink")）、珀斯皇冠賭場（[Crown
Perth](../Page/:en:Crown_Perth.md "wikilink")）、天鵝鐘樓（[Swan
Bells](../Page/:en:Swan_Bells.md "wikilink")）以及西澳博物館等。相對於[悉尼或](../Page/悉尼.md "wikilink")[墨爾本](../Page/墨爾本.md "wikilink")，珀斯的規模雖較小，但卻不失繁盛及充滿著優雅的城市氣息。全市最高的摩天大樓是位於聖佐治臺的[中央公園](../Page/中央公園_\(摩天大樓\).md "wikilink")。

## 著名學府

珀斯一共有四所國立[大學](../Page/澳大利亞大學.md "wikilink")，包括學生人數最多的[科廷大學](../Page/科廷大學.md "wikilink")（Curtin
University）、歷史最悠久聞名的[西澳大學](../Page/西澳大學.md "wikilink")（University of
Western Australia）、面積最廣的[莫道克大學](../Page/莫道克大學.md "wikilink")（Murdoch
University）、校區最多的[伊迪斯科文大學](../Page/伊迪斯科文大學.md "wikilink")（ECU - Edith
Cowan
University），及一所私立的[澳大利亞聖母大學](../Page/澳大利亞聖母大學.md "wikilink")（University
of Notre Dame）。

4所[公立大學](../Page/公立大學.md "wikilink")：

  - [西澳大學](../Page/西澳大學.md "wikilink")
  - [科廷大學](../Page/科廷大學.md "wikilink")
  - [莫道克大學](../Page/莫道克大學.md "wikilink")
  - [伊迪斯科文大學](../Page/伊迪斯科文大學.md "wikilink")

1所[私立大學](../Page/私立大學.md "wikilink")：

  - [澳大利亞聖母大學](../Page/澳大利亞聖母大學.md "wikilink")

## 人口

1980年代早期時，珀斯超過[阿德萊德](../Page/阿德萊德.md "wikilink")，成為澳洲人口第四多的城市。2014年珀斯人口為2,021,200人。市內人口主要為歐裔（英裔、愛爾蘭裔、意大利裔），約3%是華人，一大部分是近30年來從東南亞（馬來西亞、新加坡、越南、印尼）移民到珀斯的華人。也有一定數量的印度裔和南非（白人）裔移民。

## 交通

[PerthRailway.jpg](https://zh.wikipedia.org/wiki/File:PerthRailway.jpg "fig:PerthRailway.jpg")
[PerthRailwayStationUndergroundCentral.jpg](https://zh.wikipedia.org/wiki/File:PerthRailwayStationUndergroundCentral.jpg "fig:PerthRailwayStationUndergroundCentral.jpg")
[Aerial_view_of_Fremantle.JPG](https://zh.wikipedia.org/wiki/File:Aerial_view_of_Fremantle.JPG "fig:Aerial_view_of_Fremantle.JPG")

珀斯的國際及國內[機場都在市中心以東的](../Page/珀斯機場.md "wikilink")[Redcliffe區](../Page/:en:Redcliffe,_Western_Australia.md "wikilink")。距離市中心的商業區分別達十七與十二公里。另一個名為[詹達科特機場在市中心以南的詹達科特郊區](../Page/:en:Jandakot_Airport.md "wikilink")，專為一般航空學而設。道路方面，珀斯有一套完善公路網與三條[高速公路](../Page/高速公路.md "wikilink")、九條[都會區高速公路](../Page/:en:List_of_Highways_in_Perth.md "wikilink")，並未設有收費站。

市區的公共運輸－巴士、城郊列车和小型邮轮，由[Transperth提供](../Page/:en:Transperth.md "wikilink")。其中包括免費的[CAT巴士](../Page/:en:Perth_Central_Area_Transit.md "wikilink")（**Central
Area
Transit**，中央區域運輸交通），方便人們在市中心的活動。四條CAT巴士路線（紅、藍、黃、綠）貫穿珀斯市東西、南北。珀斯為降低污染，除CAT以外，在繁華的市中心亦提供免費的公共運輸系統，搭其他公車只要上車和下車都是在市中心範圍內完全免費。

位於市中心的珀斯鐵路總站，是富有古西方建築風格的[地標](../Page/地標.md "wikilink")，亦是中途轉乘站，提供五條幹線共六十九個站，大部份主要地區也能到達。由總站珀斯開出至五個端點站分別為西南面的[費利曼圖](../Page/費利曼圖.md "wikilink")，北面的[君達樂及](../Page/君達樂.md "wikilink")[畢拿](../Page/畢拿_\(西澳大利亞\).md "wikilink")，東面的[米德兰](../Page/:en:Midland_railway_line,_Perth.md "wikilink")、東南面的[阿馬代爾及南面的](../Page/:en:Armadale_railway_line,_Perth.md "wikilink")[曼杜拉](../Page/曼杜拉.md "wikilink")。若無火車到達的地方亦有巴士行駛，以確保盡量提供公共運輸服務於各區；惟一般巴士班次較少。

在市中心有两个大型公车中转站，珀斯巴士埠（**Perth Busport**）和伊丽莎白港巴士总站（**Elizabeth Quay Bus
Station**），其中珀斯巴士埠和[珀斯鐵路總站是相連的](../Page/:en:Perth_Train_Station.md "wikilink")。另外在費利曼圖的公车转運站，亦與費利曼圖鐵路總站連接。

另外，999路公车和998路公车是环城车，路途经过大多珀斯境内的学校，其中包括珀斯市內的四所大學，主要为方便学生而设立的学校公车线。

珀斯機場有公共巴士到達，40號線以及935號線可以到達三號及四號客運大樓，而380號線則可以到達一號及二號客運大樓。

珀斯的车票是时间制，一般车票可以在指定時間内无限换乘同區間的公车或者铁路，時效為两小时至三小时不等。

## 唐人街

[Northbridge1.jpg](https://zh.wikipedia.org/wiki/File:Northbridge1.jpg "fig:Northbridge1.jpg")
北橋區（[Northbridge](../Page/:en:Northbridge,_Western_Australia.md "wikilink")）是珀斯著名的[唐人街](../Page/唐人街.md "wikilink")，这里有很多华人餐厅和华人商店，是华人聚集最多的地方之一。

北橋區除了是珀斯的唐人街，該區亦是當地人的文化及娛樂場所的集中地。離市中心步行只需數分鐘路程。每逢週末或假日北橋區的酒吧、夜總會和咖啡廳便擠滿了客人，非常熱鬧。是澳洲其中一個最大的各式餐廳集中地之一。超過一百間餐廳可供選擇，特別集中在Lake
Street及James
Street，種類由[中國菜](../Page/中國菜.md "wikilink")、[印度菜](../Page/印度菜.md "wikilink")、[希臘菜](../Page/希臘菜.md "wikilink")、[埃及菜](../Page/埃及菜.md "wikilink")、[泰國菜](../Page/泰國菜.md "wikilink")、[越南菜至](../Page/越南菜.md "wikilink")[意大利菜等](../Page/意大利菜.md "wikilink")。此外，[西澳博物館](../Page/西澳博物館.md "wikilink")、[西澳藝術館](../Page/西澳藝術館.md "wikilink")、以及位於亞歷山大圖書館大樓（[Alexander
Library
Building](../Page/:en:Alexander_Library_Building.md "wikilink")）内的西澳州立圖書館（[Western
Australia State
Library](../Page/:en:State_Library_of_Western_Australia.md "wikilink")）等文化設施亦集中在此地區。

## 友好城市

目前與珀斯締結為「[姊妹市](../Page/姊妹城市.md "wikilink")」的城市有：

<table>
<thead>
<tr class="header">
<th><p>協定年份</p></th>
<th><p>協定城市</p></th>
<th><p>所在國家/地區</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1974年</p></td>
<td><p><a href="../Page/鹿兒島市.md" title="wikilink">鹿兒島市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p><a href="../Page/休士頓.md" title="wikilink">休士頓</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td><p>羅茲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p><a href="../Page/迈伊斯蒂.md" title="wikilink">迈伊斯蒂</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p><a href="../Page/聖地牙哥_(加利福尼亞州).md" title="wikilink">聖地牙哥</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1989年</p></td>
<td><p><a href="../Page/瓦斯托.md" title="wikilink">瓦斯托</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/南京市.md" title="wikilink">南京市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/臺北市.md" title="wikilink">臺北市</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/成都市.md" title="wikilink">成都市</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考文献

  - 人口統計 - [Australian Bureau of
    Statistics](http://www.abs.gov.au/AUSSTATS/abs@.nsf/Latestproducts/3218.0Main%20Features91996%20to%202006?opendocument&tabname=Summary&prodno=3218.0&issue=1996%20to%202006&num=&view=/)
  - 政府體制 - [Government of Western Australia
    website](http://www.wa.gov.au/)
  - 主要旅遊景點 -
    [西澳觀光局官方網站](http://www.westernaustralia.com/TCN/Pages/Welcome_to_Western_Australia.aspx/)
  - 氣候數據 - [Auinfo The Way To Go
    Website](https://web.archive.org/web/20061205035433/http://auinfo.com/)
  - [澳大利亞統計局官方網站](http://www.abs.gov.au/)
  - Suburbs資料 - [Australia's Migration
    Network](http://www.aussiemove.com/)
  - 姊妹市的歷史及協議–[珀斯市政府官方網頁](http://www.perth.wa.gov.au/)
  - 公共運輸–[Transperth Official Website](http://www.transperth.wa.gov.au/)

## 外部链接

  - [珀斯市政府官方網頁](http://www.perth.wa.gov.au/)
  - [西澳線上資訊網](https://web.archive.org/web/20060918173926/http://www.onlinewa.com.au/enhanced/)
  - [西澳大利亚旅游局――西澳大利亚旅游官方网站](http://www.westernaustralia.com/TCN/Pages/Welcome_to_Western_Australia.aspx/)
  - [西澳大利亞州政府官方網頁](http://www.wa.gov.au/)
  - [Experience Perth Website](http://www.experienceperth.com/)

{{-}}

[珀斯](../Category/珀斯.md "wikilink")
[1](../Category/西澳大利亞州城市.md "wikilink")

1.  [2003年世界最友善城市](http://economy.enorth.com.cn/system/2002/08/16/000398323.shtml)