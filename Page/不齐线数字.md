[`Mediaevalziffern.png`](https://zh.wikipedia.org/wiki/File:Mediaevalziffern.png "fig:Mediaevalziffern.png")`是一套使用不齊線數字的現代字體。]]`

**不齊線數字**（也稱**小寫數字**、**古風體數字**和**中世紀數字**）是[阿拉伯數字的一種款式](../Page/阿拉伯數字.md "wikilink")，設計配合[小寫字母](../Page/小寫字母.md "wikilink")，營造視覺上的和諧。

現在阿拉伯數字通常是**齊線數字**（也稱為**大寫數字**、**現代體數字**和**標題數字**），與[大寫字母高度一樣](../Page/大寫字母.md "wikilink")，沒有下降筆劃。與此相比，小寫阿拉伯數字的形狀和位置都有變化，就像小寫字母：一些沒有上升和下降筆劃，與字母x同高（例如0、1和2）；一些有上升筆劃，像字母h（6和8）；一些有下降筆劃，像字母g（3、4、5、7和9）。雖然這是最常見的設計安排，但也有其他可能安排。例如在18世紀後期到19世紀早期，[法國](../Page/法國.md "wikilink")[狄多家族的印刷設計員使用有上升筆劃的](../Page/狄多.md "wikilink")3。這種形式保留在後來的一些法國字體中。另外一些字體還有不同的安排。

在[中世紀](../Page/中世紀.md "wikilink")，自從12世紀阿拉伯數字於[歐洲開始流行](../Page/歐洲.md "wikilink")，這種款式的數字就在使用，故此也稱為**中世紀數字**。19世紀字體設計師設計的齊線數字，大幅取代了不齊線數字，尤其是報紙和廣告字體。[機械排鑄](../Page/機械排鑄.md "wikilink")（例如[蒙納和](../Page/蒙納公司.md "wikilink")[麗那排鑄機](../Page/麗那公司.md "wikilink")）用的精細的書本字體到了20世紀仍然用不齊線數字，但隨著[照相排字時代的來臨](../Page/照相排字.md "wikilink")，它們幾乎消失。[數碼排字令它們蓬勃復甦](../Page/數碼排字.md "wikilink")。

高品質[排字偏向在內文採用不齊線數字](../Page/排字.md "wikilink")：它們與小寫字母和小型大寫字母較融和，而且它們形狀較多變化，便利閱讀。而齊線數字則用於大寫字母中，有認為它們使用在表格和試算表效果更好。

雖然很多傳統字體都包含一套完整的齊線和不齊線數字，以用於不同場合；多數標準電腦字體（除了專業印刷使用的字體）都只有一種數字。齊線數字較常見，使用不齊線數字的常用字體有[Georgia和](../Page/Georgia_\(字型\).md "wikilink")[Hoefler
Text](../Page/Hoefler_Text.md "wikilink")。

## 參考

  - Robert Bringhurst. *The Elements of Typographic Style*. Vancouver:
    Hartley & Marks, 1992. ISBN 0-88179-132-6.

[Category:数字](../Category/数字.md "wikilink")