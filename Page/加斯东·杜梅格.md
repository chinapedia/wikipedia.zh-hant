**皮埃尔-保罗-亨利-加斯东·杜梅格**（**Pierre-Paul-Henri-Gaston
Doumer**，，），1924年6月13日出任[法蘭西第三共和國第](../Page/法蘭西第三共和國.md "wikilink")12位總統。任内经常出现政治不稳定局面。

## 生平

杜梅格1863年8月1日出生于[法国](../Page/法国.md "wikilink")[加尔省](../Page/加尔省.md "wikilink")[艾格维沃](../Page/艾格维沃_\(加尔省\).md "wikilink")(Aigues-Vives)一个[新教家庭](../Page/新教.md "wikilink")。1884年—1893年在印度支那和北非任文官，1893年以[激進社會黨人身分當選眾議員](../Page/激进党.md "wikilink")。1902年6月第一次任部长职务，1910年被选入参议院，1913年12月13日组阁，不到7个月即倒台。但他仍担任各种部长职务，直至1917年3月。后返回参议院任议长，至1924年6月13日当选为共和国总统为止。任内经常发生内阁危机，先后换过15个内阁。1934年2月，在他卸任总统职务3个月后，又组织新政府。但他建立一个有广泛基础的各党派联盟计划和宪法修改方案均告失败，同年11月8日辞职，完全退出政界。

[Doumergue_4953635257_cea812010b_o.jpg](https://zh.wikipedia.org/wiki/File:Doumergue_4953635257_cea812010b_o.jpg "fig:Doumergue_4953635257_cea812010b_o.jpg")

## 参考

[Category:法国总统](../Category/法国总统.md "wikilink")
[Category:法国总理](../Category/法国总理.md "wikilink")
[Category:法國新教徒](../Category/法國新教徒.md "wikilink")