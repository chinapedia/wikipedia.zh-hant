**打比道**
（）是[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[九龍仔南部的一條單程路](../Page/九龍仔.md "wikilink")，西接[志士達道](../Page/志士達道.md "wikilink")，北接[蘭開夏道](../Page/蘭開夏道.md "wikilink")。

## 道路命名

打比道的命名有兩個說法。第一個說法是以19世紀[英國著名的政治家](../Page/英國.md "wikilink")[愛德華·史密斯-斯坦利命名](../Page/愛德華·史密斯-斯坦利，第十四代德比伯爵.md "wikilink")。打比伯爵自小愛好[賽馬](../Page/賽馬.md "wikilink")，曾在三歲時贏得賭注，後來不少的賽馬賽事都以「[打比](../Page/打比.md "wikilink")」命名。除了賽馬外，還有不少的足球和其他比賽都有以打比作命名的記錄。1860年，英國和中國簽署《[北京條約](../Page/北京條約.md "wikilink")》，在前一年他剛任[英國首相](../Page/英國首相.md "wikilink")，六年後他再任首相，為此政府以他的名字為街道命名，以作紀念。\[1\]

第二個說法則是與[九龍仔大部份道路一樣](../Page/九龍仔.md "wikilink")，也是以英國地名命名，即是[打比郡](../Page/打比郡.md "wikilink")\[2\]。

## 著名建築物

  - [九龍塘宣道小學](../Page/九龍塘宣道小學.md "wikilink")

## 參考資料

<references />

[Category:九龍仔街道](../Category/九龍仔街道.md "wikilink")

1.  《香港歷史文化小百科16－趣談九龍街道》 爾東 著，第60頁，明報出版社，2004年11月，ISBN 962-8871-46-3
2.  [以英國地名命名的街道](http://programme.rthk.org.hk/channel/radio/programme.php?name=radio5/hkstreet&d=2012-10-16&p=5629&e=190142&m=episode)，香港電台