**何國星**（****，\[1\]），前[香港](../Page/香港.md "wikilink")[男](../Page/男.md "wikilink")[配音員](../Page/配音員.md "wikilink")。

## 概要

  - 父親為有「香港遮王」之稱的[何洪希](../Page/何洪希.md "wikilink")（1928-2015.7）\[2\]。

<!-- end list -->

  - 曾於1988年參加[無綫電視及](../Page/電視廣播有限公司.md "wikilink")[華星唱片合辦的第七屆](../Page/華星唱片.md "wikilink")[新秀歌唱大賽奪得銀獎而成為歌手](../Page/新秀歌唱大賽.md "wikilink")。

<!-- end list -->

  - 1989年入讀第15期[無綫電視藝員訓練班](../Page/無綫電視藝員訓練班.md "wikilink")。

<!-- end list -->

  - 1994年入讀第2期[無綫電視粵語配音藝員訓練班](../Page/無綫電視粵語配音藝員訓練班.md "wikilink")，同年5月加入[無綫電視配音組](../Page/無綫電視.md "wikilink")。

<!-- end list -->

  - 2001年約滿後退出配音界。

<!-- end list -->

  - 現為配音班、戲劇班、糾正懶音口才訓練班、中文流行曲歌唱班導師。

## 配音作品

※<span style="font-size:smaller;">**粗體**表示的角色為作品中的主角或要角</span>

### 電視動畫／OVA

<table>
<thead>
<tr class="header">
<th><p>首播年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1994</strong></p></td>
<td><p><a href="../Page/勇者傳說.md" title="wikilink">勇者傳說</a></p></td>
<td><p>泰丹</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美少女戰士_(第一輯).md" title="wikilink">美少女戰士</a></p></td>
<td><p>拿拉達<br />
浦和良</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/幻法雙子星.md" title="wikilink">幻法雙子星</a></p></td>
<td><p>高村正榭</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1995</strong></p></td>
<td><p><a href="../Page/元氣爆發.md" title="wikilink">元氣爆發</a></p></td>
<td><p>愛路狄藍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/幽遊白書.md" title="wikilink">幽遊白書</a></p></td>
<td><p>陣<br />
樹<br />
是流<br />
東王<br />
朱雀</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/為食龍少爺.md" title="wikilink">為食龍少爺</a></p></td>
<td><p><strong>高倉川太</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/忍者亂太郎.md" title="wikilink">忍者亂太郎</a></p></td>
<td><p><strong>土井半藏</strong></p></td>
<td><p>第2季<br />
接替離職的<a href="../Page/蘇強文.md" title="wikilink">蘇強文</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美少女戰士R.md" title="wikilink">美少女戰士R</a></p></td>
<td><p>迪文王子</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/城市風雲兒.md" title="wikilink">反斗劍神</a></p></td>
<td><p>鬼丸猛</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1996</strong></p></td>
<td><p><a href="../Page/熱血最強.md" title="wikilink">熱血最強</a></p></td>
<td><p>長田秀三<br />
藤古雅夫</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/十二生肖爆烈戰士.md" title="wikilink">十二守護戰士</a></p></td>
<td><p>巴加立斯</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/忍者亂太郎.md" title="wikilink">忍者亂太郎</a></p></td>
<td><p><strong>土井半藏</strong></p></td>
<td><p>第3季</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/足球小將.md" title="wikilink">新足球小將</a></p></td>
<td><p>三杉淳（中學）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龍珠二世.md" title="wikilink">龍珠二世</a></p></td>
<td><p><strong><a href="../Page/孫悟飯.md" title="wikilink">孫悟飯</a>（成人）</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1997</strong></p></td>
<td><p><a href="../Page/貓狗寵物街.md" title="wikilink">貓狗寵物街</a></p></td>
<td><p>菠蘿</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美少女戰士S.md" title="wikilink">美少女戰士S</a></p></td>
<td><p>藥師寺角錐</p></td>
<td><p>第16回</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美少女戰士SS.md" title="wikilink">美少女戰士SS</a></p></td>
<td><p>虎之眼</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/婚紗小天使.md" title="wikilink">婚紗小天使</a></p></td>
<td><p>普雷</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/特種變形勇士.md" title="wikilink">特種變形勇士</a></p></td>
<td><p><strong>捷達</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1998</strong></p></td>
<td><p><a href="../Page/忍者亂太郎.md" title="wikilink">忍者亂太郎</a></p></td>
<td><p><strong>土井半藏</strong></p></td>
<td><p>第4季</p></td>
</tr>
<tr class="odd">
<td><p><strong>1999</strong></p></td>
<td><p><a href="../Page/機動戰士GUNDAM0083.md" title="wikilink">機動戰士GUNDAM0083</a></p></td>
<td><p>史哥特<br />
荷比爾</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/機動新世紀GUNDAM_X.md" title="wikilink">機動新世紀GUNDAM X</a></p></td>
<td><p>加迪斯</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/勇者王.md" title="wikilink">勇者王</a></p></td>
<td><p><strong>獅子王凱</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奧運高手.md" title="wikilink">體操小子</a></p></td>
<td><p>新堂雄一</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2000</strong></p></td>
<td><p><a href="../Page/百變小櫻Magic咭.md" title="wikilink">百變小櫻Magic咭</a></p></td>
<td><p>寺田良幸<br />
久里子的父親</p></td>
<td><p>只配至第19集<br />
第15集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吹波糖危機2040.md" title="wikilink">吹波糖危機2040</a></p></td>
<td><p>馬生</p></td>
<td><p>前期</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 特攝片

| 首播年份     | 作品名稱                                        | 配演角色            | 角色演員                                   | 備註 |
| -------- | ------------------------------------------- | --------------- | -------------------------------------- | -- |
| **1995** | [異空超能人](../Page/異空超能人.md "wikilink")        | **阿森**          | [馬修·勞倫斯](../Page/馬修·勞倫斯.md "wikilink") |    |
| **1997** | [電光超能人](../Page/電光超人古立特.md "wikilink")      | **傑德文**         |                                        |    |
| **1998** | [星空戰隊](../Page/地球戰隊五人組.md "wikilink")       | **星川文矢（星空黑戰士）** |                                        |    |
| **1999** | [重甲戰隊](../Page/:ja:重甲ビーファイター.md "wikilink") | **片霧大作**        |                                        |    |
|          |                                             |                 |                                        |    |

### 日劇

  - 三澤一也（[三顆恨嫁的心](../Page/三顆恨嫁的心.md "wikilink")）
  - **柏木文也**（[同一屋簷下](../Page/同一屋簷下.md "wikilink")）

### 電影

| 首播年份     | 作品名稱                                               | 配演角色 | 角色演員                                     | 備註 |
| -------- | -------------------------------------------------- | ---- | ---------------------------------------- | -- |
| **有待確認** | [貓狗也瘋狂之三藩市流浪記](../Page/看狗在说话之旧金山历险记.md "wikilink") | 阿積   | [邁克·瑞斯波利](../Page/邁克·瑞斯波利.md "wikilink") |    |
| **2001** | [驕陽似我](../Page/驕陽似我.md "wikilink")                 | 比利   | [科爾·豪瑟](../Page/科爾·豪瑟.md "wikilink")     |    |
|          |                                                    |      |                                          |    |

### 廣告／宣傳片

<span style="font-size:smaller;">**`*只記錄商業廣告和機構宣傳片，電影、電視節目的廣告和宣傳片不記錄在內。`**</span>

| 首播年份     | 相關機構                               | 主要目的           | 主要配演 | 觀看廣告                                                                       |
| -------- | ---------------------------------- | -------------- | ---- | -------------------------------------------------------------------------- |
| **2015** | [香港明愛](../Page/香港明愛.md "wikilink") | 提供**香港明愛**服務資訊 | 旁白   | [按此觀看](https://www.facebook.com/memoryofhongkong/videos/1325036240855961/) |
|          |                                    |                |      |                                                                            |

## 外部連結

<references/>

[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:香港配音員](../Category/香港配音員.md "wikilink")
[Category:前無綫電視配音員](../Category/前無綫電視配音員.md "wikilink")
[Category:何姓](../Category/何姓.md "wikilink")

1.
2.  on.cc東網專訊：[何國星斥6位數圓開騷夢](http://hk.on.cc/hk/bkn/cnt/entertainment/20151024/bkn-20151024060606906-1024_00862_001.html)