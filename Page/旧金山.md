[SF_From_Marin_Highlands3.jpg](https://zh.wikipedia.org/wiki/File:SF_From_Marin_Highlands3.jpg "fig:SF_From_Marin_Highlands3.jpg")\]\]

**舊金山**（），正式名稱為**舊金山市郡**（），是位於[美國](../Page/美國.md "wikilink")[加利福尼亚州北部的](../Page/加利福尼亚州.md "wikilink")[都市](../Page/都市.md "wikilink")，為加州唯一[市郡合一的](../Page/市郡合一.md "wikilink")[行政區](../Page/加利福尼亞州行政區劃.md "wikilink")，中文-{zh-hans:又音译为**三藩市**和**圣弗朗西斯科**;
zh-hk:又名**舊金山**，另有音譯**聖·弗朗西斯科**;
zh-tw:又音譯為**三藩市**和**聖弗朗西斯科**}-，亦別名「**金門城市**」、「**灣邊之城**」、「**霧城**」等。位於[舊金山半島的北端](../Page/舊金山半島.md "wikilink")，東臨[舊金山灣](../Page/舊金山灣.md "wikilink")、西臨[太平洋](../Page/太平洋.md "wikilink")，人口約86萬，為加州第四大城；其與灣邊各都市組成的[舊金山灣區](../Page/舊金山灣區.md "wikilink")，人口總數達768萬，是僅次於[大洛杉磯地區的](../Page/大洛杉磯地區.md "wikilink")[美國西岸第二大都會區](../Page/美國西岸.md "wikilink")\[1\]。

舊金山是[北加州與舊金山灣區的核心都市](../Page/北加州.md "wikilink")，也是文化，商業和金融中心。當地住有很多艺术家、作家和演员，在20世紀及21世紀初一直是美國[嬉皮士文化和近代](../Page/嬉皮士.md "wikilink")[自由主義](../Page/自由主義.md "wikilink")、[進步主義的中心之一](../Page/進步主義.md "wikilink")。舊金山也是[聯合國的誕生地](../Page/联合国.md "wikilink")\[2\]\[3\]\[4\]。舊金山是受歡迎的旅遊目的地\[5\]，以其涼爽的夏季、多霧、綿延的丘陵地形、，以及[金門大橋](../Page/金門大橋.md "wikilink")、[纜車](../Page/舊金山纜車系統.md "wikilink")、[惡魔島監獄及](../Page/阿爾卡特拉斯島.md "wikilink")[唐人街等景點聞名](../Page/舊金山唐人街.md "wikilink")。此外，舊金山也是五大主要銀行及許多大型公司、机构的總部所在，其中又以[互聯網產業為最](../Page/互聯網.md "wikilink")，包括[盖璞](../Page/盖璞.md "wikilink")、[太平洋瓦電公司](../Page/太平洋瓦電公司.md "wikilink")、[Yelp](../Page/Yelp.md "wikilink")、[Dropbox](../Page/Dropbox.md "wikilink")、[Pinterest](../Page/Pinterest.md "wikilink")、[Twitter](../Page/Twitter.md "wikilink")、[优步](../Page/优步.md "wikilink")、[来福车](../Page/来福车.md "wikilink")、[爱彼迎](../Page/爱彼迎.md "wikilink")、[Mozilla](../Page/Mozilla.md "wikilink")、[維基媒體基金會](../Page/維基媒體基金會.md "wikilink")、[克雷格列表](../Page/克雷格列表.md "wikilink")、[Salesforce.com等](../Page/Salesforce.com.md "wikilink")。

## 名稱

聖弗朗西斯科（）是[西班牙語常見地名](../Page/西班牙語.md "wikilink")，得名自[方濟會創始人](../Page/方濟會.md "wikilink")[亞西西的方濟各](../Page/亞西西的方濟各.md "wikilink")（）。

### 漢譯名稱

19世纪这里是美国[加州淘金潮的中心地区](../Page/加州淘金潮.md "wikilink")，早期[华工到美国淘金後多居住于此](../Page/华工.md "wikilink")，称之为“金山”，但直到在[澳大利亚的](../Page/澳大利亚.md "wikilink")[墨尔本发现金矿后](../Page/墨尔本.md "wikilink")，為了與被稱作“[新金山](../Page/新金山.md "wikilink")”的墨尔本區別，而改称這個城市为“**舊-{}-金山**”。

至於「**三-{}-藩市**」之名，則是取自该市[英語名称](../Page/英語.md "wikilink")「」的[粵語音譯](../Page/粵語.md "wikilink")，是居住於此地為數不少的[粵語族群及團體較常用譯名](../Page/粵語.md "wikilink")，例如出身[廣東的](../Page/廣東.md "wikilink")[中華民國國父](../Page/中華民國.md "wikilink")[孫中山先生在著作與書信中提及該市時](../Page/孫中山.md "wikilink")，使用此名。

[日語則多以](../Page/日語.md "wikilink")[片假名表示](../Page/片假名.md "wikilink")（），但亦可用[漢字寫作](../Page/日語漢字.md "wikilink")「桑港」（）。

### 譯名

長期以來，舊-{}-金山的中文譯名沒有明確的官方政策，用法稍显混亂，但主要以「舊-{}-金山」和「三-{}-藩市」為主。
[美国之音和](../Page/美国之音.md "wikilink")[美国国务院都使用](../Page/美国国务院.md "wikilink")「舊-{}-金山」\[6\]。大部分駐於該市、擁有中文正式稱呼的外事單位，都是使用「舊-{}-金山」之稱呼，例如[中華民國外交部的](../Page/中華民國外交部.md "wikilink")「駐舊-{}-金山[台北經濟文化辦事處](../Page/臺北經濟文化代表處.md "wikilink")\[7\]」，以及「中华人民共和国驻舊-{}-金山总领事馆\[8\]」。

因中国大陆的地名的翻译习惯多为音译，在中华人民共和国的官方文献及地图中，多称其为“聖弗朗西斯科”，并括号加注“旧-{}-金山”\[9\]。但实际上中华人民共和国外交部網站美国概况页上使用的译名为「舊-{}-金山」\[10\]。香港政府則以「三-{}-藩市」為正式名稱，例如「駐三-{}-藩市經濟貿易辦事處」。另外，在港澳地區，除涉及歷史的場合外，一般只使用「三-{}-藩市」這一譯名，與中國大陸及台灣有別。

區內的華語媒體普遍依讀者、觀眾或聽眾的方言習慣分別稱為「-{三藩市}-」（粵語）和「舊-{}-金山」（國語/普通話）。舊金山市政府及下屬各級市政單位的官方網站中文版上，分別使用了「舊-{}-金山\[11\]」與「-{三藩市}-\[12\]」這兩種不同的譯名，沒有統一為單一譯名。

另外，當地老一輩华裔中常有稱舊金山為「大埠」（至於二埠與三埠則分別是指加州首府[-{zh-hant:沙加緬度;zh-hans:萨克拉门托}-與](../Page/沙加緬度.md "wikilink")[史塔克頓](../Page/史塔克頓_\(加利福尼亞州\).md "wikilink")），但近年新移民已較少用這名稱。

## 歷史

[美洲原住民在西元前三千年前就到了北加州地區](../Page/美洲原住民.md "wikilink")。證據顯示[美洲原住民](../Page/美洲原住民.md "wikilink")[歐隆尼族群](../Page/歐隆尼.md "wikilink")（）早在六世紀時已居住在[舊金山灣區](../Page/舊金山灣區.md "wikilink")，「歐隆尼」在他們自己的語言中是“西部人”的意思。

### 早期開發（1542-1847）

歐洲人在1542年到[舊金山灣區](../Page/舊金山灣區.md "wikilink")，舊金山區域在16世紀屬於[西班牙的](../Page/西班牙.md "wikilink")[上加利福尼亞省領土](../Page/上加利福尼亞省.md "wikilink")。西班牙將領（Don
Gaspar de
Portola）於1769年帶領他的屬下在舊金山灣區探索地形。1776年3月28日，[西班牙](../Page/西班牙.md "wikilink")[探險家](../Page/探險家.md "wikilink")（Juan
Bautista de Anza）在當地選了兩個可建築地點，一個作為軍事用途，另一個則作為傳教用途，但實質的施工卻由安扎的兩名同伴Jose
Joaquin Moraga中尉及Francisco Palóu神父監督。Jose Joaquin
Moraga負責建立的是軍事基地（Presidio of San
Francisco，現已改為要塞公園）。Francisco
Palóu則負責建立的是教會，他將該地命名為[圣方济各传教站](../Page/圣方济各传教站.md "wikilink")，俗稱多洛雷斯传教站（Misión
Dolores）。該區所有的一切行政事務皆經由教會管理。

1821年，[墨西哥自西班牙獨立](../Page/墨西哥.md "wikilink")，上加利福尼亞也成為墨西哥領土。獨立之後的墨西哥教會管轄的勢力逐漸薄弱，教會所屬的土地也逐漸規劃成私人所擁有。來自[英國的](../Page/英國.md "wikilink")（）於1835年在半島的西部（即現今位於唐人街與金融區交界的[花園角](../Page/花園角.md "wikilink")）建立了第一棟私人別宅。李察森經過他人幫助，開始規劃住宅附近的街市，並將此區域稱為**（意為）。[墨西哥和美國戰争爆發後](../Page/美墨戰爭.md "wikilink")，[美國海軍](../Page/美國海軍.md "wikilink")[准將](../Page/准將.md "wikilink")（John
D.
Sloat）带領手下来到這裡，1846年以美國名義佔領此城。1847年1月30日，海軍[上校](../Page/上校.md "wikilink")（John
B. Montgomery）將*Yerba Buena*更名為*San Francisco*。

### 加州淘金潮（1848-1855）

1848年1月，在於加州東北部的[蘇特坊](../Page/蘇特坊.md "wikilink")（）發現[黄金後](../Page/黄金.md "wikilink")，消息很快的就傳開來，同年8月[加州發現](../Page/加州.md "wikilink")[金礦的消息傳到東部的](../Page/金礦.md "wikilink")[紐約](../Page/紐約.md "wikilink")，各地許多懷著想一夕致富的人開始湧入舊金山港口。1849年起，[加州淘金潮真正展開](../Page/加利福尼亞淘金潮.md "wikilink")，從美國其他各地和其他國家搭船或者是從內陸來到加州夢想實現掏金夢的人絡繹不絕，船上的的旅客、水手甚至船長下了船馬上就前仆後繼的前往蘇特坊，當時舊金山港口的帆船到處可見，而市區的人口一夕間暴漲。這也讓舊金山居民從1847到1870年之間，從500人口增加到15萬人口。後來所謂舊金山四十九人即是那些於1849年從各地來到舊金山想一夕致富的人。

當時除了世界各地前來實現掏金夢的投機者外，許多人在舊金山開店提供掏金者需要的補給品，當時的公司有些至今存在，包括製造[李維斯牛仔褲](../Page/李維斯.md "wikilink")（1873年）的[李維·史特勞斯](../Page/李維·史特勞斯.md "wikilink")、[吉德利巧克力店](../Page/吉德利巧克力.md "wikilink")（Ghirardelli，1852年）、FOLGERS
咖啡（1850年）和[富國銀行](../Page/富國銀行.md "wikilink")（1852年）、[加州銀行](../Page/加州銀行.md "wikilink")（Bank
of
California，1864年）。1848年發現黃金之前加州仍屬於墨西哥領土，同年二月墨西哥於[美墨戰爭中戰敗後](../Page/美墨戰爭.md "wikilink")，加州變成美國領土。加利福尼亞於1850年正式成美國聯邦政府第31個州，舊金山是起初的一個縣。[舊金山聯邦造幣廠於](../Page/舊金山造幣廠.md "wikilink")1854年成立，主要打造金幣用來流通市場。掏金熱使得舊金山成為當時美國[密西西比河以西最大的城市](../Page/密西西比河.md "wikilink")。

### 美利堅帝國（1859-1880）

### 20世紀至今

#### 1906年舊金山大地震

1906年4月18日的凌晨五點零二分，舊金山發生八級的大地震。地震造成各地的瓦斯管斷裂而引起大火，由於水源不足使得蔓火延燒數日，當時舊金山市區內四分之三的房屋幾乎完全損毀，遠在東灣的區民都可明顯看到火災在市區產生的煙霧。這次地震造成舊金山區民30萬人無家可歸，許多人只能露宿公園和街頭。當時的記錄上只有498人喪生，直到2005年市政府才正式更改喪生人數為超過三千人。舊金山大地震徹底摧毀了舊金山的街景，但也使得舊金山能獲得機會浴火重生而改造成現代化的都市。當時無家可歸的居民大量遷移至東灣的奧克蘭和柏克萊地區，也促進[東灣地區和舊金山往來的聯繫](../Page/東灣.md "wikilink")，讓東灣開始蓬勃發展。

受1882年《[排華法案](../Page/排華法案.md "wikilink")》的影響，許多從19世紀中葉到20世紀初的華人和其他亞裔移民在美國倍受歧視。舊金山地震引起的大火燒毀了當時市政廳的檔案紀錄，讓許多華人有了機會，利用購買的偽造出生紙，以假造美籍華人兒女的身份進入美國，即是後人所稱的[紙兒子](../Page/紙兒子.md "wikilink")。據估計，1910到1940年之間，在進入美國的大約17萬5千華人中，有百分之80多的人是以「紙兒子」或「紙女兒」的身份進入美國。紙兒子的出現也造成許多華人後裔無法回歸真實的姓氏和家族史，成為特殊的華人移民史。

地震後的舊金山很快的就準備重建計劃，由於各地都需要貨幣購買材料，但是地震後的火災使得許多銀行的保險櫃過熱，必需等數週保險櫃退溫後才能當打開，許多銀行因此無法提供貨幣給大眾急需的重建貸款。當時才成立不久的是少數火災後仍能夠正常運作的銀行。義大利銀行創辦人吉亞尼尼在地震後很快的就將銀行保險櫃送往郊區，避免了受到大火的影響。由於資金充足，吉亞尼尼對任何需要貸款重建的人全部給予借貸，這使得義大利銀行反而在舊金山大地震後獨霸當地的金融市場。義大利銀行後來於1929年和南加州規模較小的美國銀行合併，改名為[美國銀行](../Page/美國銀行.md "wikilink")。

#### 巴拿馬-太平洋萬國博覽會（1915年）

舊金山為了慶祝連接大西洋和太平洋的巴拿馬運河竣工，另一方面顯示在1906年大地震後重建的新市貌，而於1915年在現今的[濱港區舉行了巴拿馬](../Page/濱港區.md "wikilink")-太平洋萬國博覽會。地震後的舊金山重建非常迅速，地震時被震毀市政廳，也趕在1915年於原地重建完畢，在萬國博覽會舉行時已經完全看不到地震災後的影響。舊金山當時為了加速重建工程，許多建築物省略了防震設計，忽略了當初地震所帶來的警惕，這也導致後來1989年大地震造成許多建築物損毀。萬國博覽會從1915年二月二十日到十二月四日，展地範圍從東邊現在梅森碉堡往西延伸到[藝術宮](../Page/藝術宮.md "wikilink")。當時博覽會金碧輝煌的建築物在展後幾乎全部拆除，目前只剩下藝術宮和其後面[科博館的建築物](../Page/舊金山科博館.md "wikilink")、[日本茶園](../Page/日本茶園.md "wikilink")、還有市政廳旁的[比爾·格雷厄姆市政禮堂](../Page/比爾·格雷厄姆市政禮堂.md "wikilink")。

#### 重要歷史

1936年連接舊金山和奧克蘭的[海灣大橋竣工](../Page/海灣大橋.md "wikilink")。

1937年連接舊金山和馬林郡的[金門大橋竣工](../Page/金門大橋.md "wikilink")。

1939年於金銀島舉行金門萬國博覽會。

1945年聯合國51個創始成員在舊金山簽訂[聯合國憲章](../Page/聯合國憲章.md "wikilink")。

1951年同盟國與[日本簽訂](../Page/日本.md "wikilink")《[舊金山和約](../Page/舊金山和約.md "wikilink")》，正式終止對日本的戰爭。

1978年當時市長馬斯可尼和首位同性戀市議員[哈維·米爾克在市政廳內遭到槍殺](../Page/哈維·米爾克.md "wikilink")。

#### 洛馬-普雷塔大地震（1989年）

1989年10月17日下午五點零四分，位於舊金山南邊97公里的[聖塔克魯茲山區的聖安地列斯斷層滑動](../Page/聖塔克魯茲.md "wikilink")，發生6.9級地震，位於同地震帶的舊金山灣區亦造成許多傷亡。

舊金山的濱港區源為1906年大地震後填土造地所建，地震發生時引起了泥土液態化，土地隨著震波上下搖擺，造成許多房屋損毀並引起大火。雙層通行的[海灣大橋位於奧克蘭段的上層道路斷裂](../Page/海灣大橋.md "wikilink")，最嚴重的位於[奧克蘭市](../Page/奧克蘭.md "wikilink")，連接海灣大橋的雙層尼米茲高速公路的上層完全震垮，造成42人在車內被壓死。當時舊金山南邊的[燭台球場正準備舉行](../Page/燭台球場.md "wikilink")[舊金山巨人隊和](../Page/舊金山巨人.md "wikilink")[奧克蘭運動家的美國職棒](../Page/奧克蘭運動家.md "wikilink")[世界大賽](../Page/世界大賽.md "wikilink")，此次地震經由電視轉播即時傳遍了世界各地，世界大賽因此延後了十日，這次地震後來又稱為世界大賽大地震或是89年大地震。

此次地震結果造成[880號州際公路](../Page/880號州際公路.md "wikilink")（尼米茲高速公路）改道重建，經由11年後直到2000年才竣工完畢，海灣大橋雖然在地震後一個月重建通行，但經過安全考量，海灣大橋奧克蘭段必須改建新橋段，新橋段將於2013年建造完成。橫跨舊金山碼頭區，連接海灣大橋和金門大橋的[480號加州州道](../Page/480號加州州道.md "wikilink")（濱海高速公路）在地震後拆除，該公路拆除後讓港口碼頭區重獲新生，目前開放式的空間規劃讓港口碼頭區重新恢復20世紀初繁榮的景象。此次在地震中唯一沒受影響的[灣區捷運在地震後成為連接東灣和舊金山的主要交通工具](../Page/灣區捷運.md "wikilink")，災後頭兩個月的每日載客量從22萬名旅客陡增到33萬名旅客。當時已取消了多年的灣區接駁運輸船，在地震後重新開闢[阿拉米達](../Page/阿拉米達.md "wikilink")、奧克蘭、[柏克萊連接舊金山的路線](../Page/柏克萊.md "wikilink")。此次地震造成各地共57人死亡，一萬兩千棟房屋損毀，舊金山往後的都市計畫因此大為改變，許多建築物必須加強防震措施或是改建。大地震的影響一直延續到今日。

#### 同性婚姻

2004年2月12日到3月11日間，時任市長[加文·纽森和其他官员开始发布無法律效力的](../Page/加文·纽森.md "wikilink")[同性婚姻证书](../Page/同性婚姻.md "wikilink")，成为[美国同性婚姻问题的焦点](../Page/美国同性婚姻.md "wikilink")。加州當時並未承認同性婚姻，在2008年加州最高法院裁決禁止同性婚姻違法前，共有4161对同性伴侣领取了结婚证書。

## 地理

[GoldenGateBridge_and_SanFransiscoDowntown.jpg](https://zh.wikipedia.org/wiki/File:GoldenGateBridge_and_SanFransiscoDowntown.jpg "fig:GoldenGateBridge_and_SanFransiscoDowntown.jpg")與舊金山市中心\]\]
[San_Francisco_20111223.JPG](https://zh.wikipedia.org/wiki/File:San_Francisco_20111223.JPG "fig:San_Francisco_20111223.JPG")

### 地形

舊金山位於加州的[聖安地列斯斷層上](../Page/聖安地列斯斷層.md "wikilink")。歷史上最大的地震是在1906年，之前在1851年，1858年，1865年和1868年也发生过大型地震。最近的地震是1989年，市中心的街道出了几尺宽的裂口，其中包括上[海湾大桥的路线](../Page/海湾大桥.md "wikilink")。

市區是典型的[丘陵地带](../Page/丘陵.md "wikilink")，城内有很多直上直下的街道。市内有名的山丘有：[諾布山](../Page/諾布山.md "wikilink")，[太平洋高地](../Page/太平洋高地.md "wikilink")（Pacific
Heights）、[雙峰](../Page/雙峰_\(三藩市\).md "wikilink")（Twin
Peaks）、[俄羅斯山和](../Page/俄羅斯山.md "wikilink")[電報山](../Page/電報山.md "wikilink")。市區內最高的[大衛森山](../Page/大衛森山.md "wikilink")（，282公尺）上面柱立有31公尺高的十字像，附近另外有紅色的[蘇特洛訊號塔](../Page/蘇特洛塔.md "wikilink")（Sutro
Tower），塔高298公尺。

### 水道和橋樑

舊金山市内没有河流，城市用水主要是靠海岸山脈的積雪流入山脈附近的 [Hetch
Hetchy](../Page/Hetch_Hetchy.md "wikilink")
水庫，經由管路運輸到城內。城内有兩座[橋樑](../Page/橋樑.md "wikilink")，連接北部101號高速公路到[馬林郡的橘色](../Page/馬林郡.md "wikilink")[金門大橋](../Page/金門大橋.md "wikilink")，和580號高速公路通往東灣[奧克蘭和](../Page/屋崙.md "wikilink")[柏克萊](../Page/柏克萊.md "wikilink")，銀灰色的雙節式[海灣大橋](../Page/海灣大橋.md "wikilink")。舊金山另有地鐵經由海底隧道通往東灣奧克蘭。

### 氣候

舊金山半島三面環水，并受太平洋[加利福尼亚寒流影响](../Page/加利福尼亚寒流.md "wikilink")，舊金山是典型的凉夏型[地中海式气候](../Page/地中海式气候.md "wikilink")。因長期受海風影響，舊金山夏天的日高溫通常只有20[摄氏度左右](../Page/摄氏度.md "wikilink")，一年大约只有一个星期会因強勁陸風影響而超過攝氏30度。9月是最暖和的月份。同時因太平洋水溫長年在攝氏10至15度間，夏天半夜也可能下降到10度以下。舊金山因臨近海邊和金門海峽，下半夜和早上多受霧的影響，但夏季降雨極少，雨季為一月到四月間，冬天雖冷，但鲜有降雪或霜。市中心上一次或以下的气温为1990年12月24日；\[13\]最近一次降小雪是在2011年2月25日，而在此之前上一次降雪则是在1976年2月5日。

根据1981至2010年在旧金山市中心的数据，日最低气温低于或等于的平均日数为7.9天；日最高气温超过的日数年均只有7.4天，以上的气温为罕见。\[14\]最冷月（12月）均温，极端最低气温（1932年12月11日）。\[15\]最热月（9月）均温，极端最高气温（1987年10月5日）。\[16\]年均降水量约，2013年降水量最少（只有），1983年降水量最多（当年降水量为）。\[17\]

## 文化區域

整個三藩市以市場街為主幹分為南北兩端，市場街從東邊舊金山灣的[內河碼頭](../Page/渡船頭區_\(三藩市\).md "wikilink")（The
Embarcadero）和[三藩市渡輪大廈為起點經過](../Page/三藩市渡輪大廈.md "wikilink")[金融區
(三藩市)](../Page/金融區_\(三藩市\).md "wikilink")、[田德隆區](../Page/田德隆區.md "wikilink")、[市政中心一直通到西南邊的](../Page/市政中心_\(三藩市\).md "wikilink")[卡斯楚街和諾谷區](../Page/卡斯楚街.md "wikilink")。

  - [田德隆區](../Page/田德隆區.md "wikilink")（The
    Tenderloin）為市内景象差異最大的區域，區內有很多色情商店和色情電影院。路上也常看遊民和乞丐。不過區內也有許多印度、巴基斯坦和東南亞口味的餐館，口味道地價格也實在。田德隆夾在市政中心和繁榮的[聯合廣場之間](../Page/聯合廣場_\(三藩市\).md "wikilink")，兩邊的變化相當明顯。

<!-- end list -->

  - [市場街南區](../Page/市場街南區_\(三藩市\).md "wikilink")（South of
    Market，簡稱SOMA），以往曾是舊金山的工廠和碼頭卸貨所在，1980年代後在市政府規劃下，許多地方已呈現不同的面貌。[馬斯科尼會議中心](../Page/馬斯科尼會議中心.md "wikilink")、[芳草地藝術中心](../Page/芳草地藝術中心.md "wikilink")、[新力複合式電影廣場](../Page/新力複合式電影廣場.md "wikilink")、[現代藝術博物館和猶太藝術博物館皆位於此區](../Page/三藩市現代藝術博物館.md "wikilink")。SOMA
    西南邊氣溫較暖活的[米慎區主要為](../Page/米慎區.md "wikilink")[拉丁美洲人聚集的區域](../Page/拉丁美洲.md "wikilink")，除了有地道的[墨西哥和](../Page/墨西哥.md "wikilink")[中南美洲餐廳外](../Page/中南美洲.md "wikilink")，附近也有不少夜店和新型餐廳進駐。市區最南端的[波特雷羅山](../Page/波特雷羅山.md "wikilink")（Potrero
    Hill）和[灣景獵人角主要為港口的工業區和內地的住宅區](../Page/灣景獵人角.md "wikilink")。

<!-- end list -->

  - 市場街北邊為三藩市主要商業和觀光文化景點，主要以[聯合廣場為中心](../Page/聯合廣場_\(三藩市\).md "wikilink")，從聯合廣場往北經過板街的[法國區](../Page/貝爾登巷.md "wikilink")，通過都板街（Grant
    Avenue）「天下為公」的牌坊下進入[中國城](../Page/三藩市中國城.md "wikilink"),
    是北美洲最古老的中國城，貼近中國城旁為小意大利的[北灘](../Page/北灘_\(舊金山\).md "wikilink")。小義大利最著名的餐館為「[臭玫瑰](../Page/臭玫瑰.md "wikilink")」（The
    Stinking
    Rose），所有的菜肴都有大量的蒜。[科伊特塔](../Page/科伊特塔.md "wikilink")（電報塔）及[榛子街均位於區內的](../Page/榛子街.md "wikilink")[電報山上](../Page/電報山.md "wikilink")。觀光客聚集的[漁人碼頭則位於中國城的北邊](../Page/漁人碼頭.md "wikilink")，內有[39號碼頭及](../Page/39號碼頭.md "wikilink")[吉爾德利廣場](../Page/吉爾德利廣場.md "wikilink")（Ghirardelli
    Square），遊客可從39號碼頭搭船前往惡魔島和天使島。

<!-- end list -->

  - 三藩市西邊的[太平洋高地與](../Page/太平洋高地.md "wikilink")[諾布山是全市最貴的住宅區](../Page/諾布山.md "wikilink")，更由於位處高地，所以可以看到全三藩市的全景。三藩市最有名的酒店之一[費爾蒙特酒店即位於諾布山丘上](../Page/三藩市費爾蒙特酒店.md "wikilink")。[俄羅斯山是](../Page/俄羅斯山.md "wikilink")[九曲花街的所在地](../Page/九曲花街.md "wikilink")，亦是著名舊金山作家[亞米斯德·莫平的](../Page/亞米斯德·莫平.md "wikilink")《[城市故事](../Page/城市故事_\(小說\).md "wikilink")》（*Tales
    of the City*）系列中人物之居處。

<!-- end list -->

  - 三藩市西邊由[金門公園分隔成南北兩部分](../Page/金門公園.md "wikilink")。北邊為[列治文區](../Page/列治文區_\(三藩市\).md "wikilink")，主要為[俄羅斯裔及](../Page/俄羅斯.md "wikilink")[阿拉伯裔集中地](../Page/阿拉伯.md "wikilink")，但因該區的[克萊門街](../Page/克萊門街.md "wikilink")（Clement
    Street）有許多華人商店，所以該區亦被稱為“新華埠”。南邊的[日落區早期多為沙丘地](../Page/日落區.md "wikilink")，在二次大戰左右建造，以住宅為主，早期因為地價比其他區域便宜，目前還有很多在30年代或40年代就搬來的退休人士。日落區的爾文街和20街一帶有許多中國商店，由於靠近舊金山加大和州立大學，而且也有很多華人留學生，所以有人認為這區域是新興的中國城。金門公園東邊即[海特-阿什伯利](../Page/海特-阿什伯利.md "wikilink")（Haight-Ashbury），是1960年代[嬉皮士的發源地](../Page/嬉皮士.md "wikilink")。

[San_Francisco_from_Pier_7_September_2013_panorama_edit.jpg](https://zh.wikipedia.org/wiki/File:San_Francisco_from_Pier_7_September_2013_panorama_edit.jpg "fig:San_Francisco_from_Pier_7_September_2013_panorama_edit.jpg")

## 經濟

[旅遊業是舊金山經濟的骨幹](../Page/旅遊業.md "wikilink")，也常被描述在歌曲、電影、與流行文化裡，同時也使得舊金山這個城市和它的地標得以舉世聞名。舊金山吸引了全美國第五多的旅客\[18\]，市政府也聲稱北岸靠近[漁人碼頭的](../Page/三藩市漁人碼頭.md "wikilink")[39號碼頭吸引了在全國任何景點裡第三多的旅客](../Page/39號碼頭.md "wikilink")\[19\]。2007年時共有超過1600萬旅客來訪，注入超過82億[美元到舊金山的經濟](../Page/美元.md "wikilink")\[20\]。在擁有大型旅館、良好餐飲服務、和世界等級[馬斯科尼展覽館的優勢下](../Page/馬斯科尼展覽館.md "wikilink")，舊金山是北美舉行展覽會和大會的最受歡迎地點前十名之一\[21\]

舊金山於19世紀中的[加州淘金潮時逐漸發展成美國西岸的金融中心](../Page/加州淘金潮.md "wikilink")。在金融商業區的[蒙哥馬利街有](../Page/蒙哥馬利街.md "wikilink")「西部的華爾街」之稱，位於這條街上的有[聯邦儲備銀行舊金山分行和](../Page/聯邦儲備銀行舊金山分行.md "wikilink")[太平洋證券交易所](../Page/太平洋證券交易所.md "wikilink")（）舊址。全美最大銀行[美國銀行即創始於舊金山](../Page/美國銀行.md "wikilink")。許多其他國際金融機構、跨國銀行、以及創投基金都在舊金山創立或設有地區總分部\[22\]，六家[財星500公司](../Page/財星500.md "wikilink")\[23\]、和許多包含[法律](../Page/法律.md "wikilink")、[公共關係](../Page/公共關係.md "wikilink")、[建築](../Page/建築學.md "wikilink")、和[平面設計的專業服務公司都設立在市中心](../Page/平面設計.md "wikilink")。舊金山是[二級全球城市的成員之一](../Page/全球城市.md "wikilink")。

舊金山合宜的氣候和開放的風氣，加上市政府為了吸引科技公司進駐和鼓勵投資創業，提供了許多減免優惠，讓許多世界級科技總公司願意落腳在舊金山。邁向21世紀的舊金山朝向成為生化科技和生物醫學重鎮為主，目前在[AT\&T球場南端新開闢了米慎灣科學園區](../Page/AT&T球場.md "wikilink")，由[舊金山加大醫學院和](../Page/舊金山加利福尼亞大學.md "wikilink")[基因科技公司的合作帶領下](../Page/基因科技公司.md "wikilink")，成立了世界數一數二的再生醫學研究中心注重於幹細胞方面的研究。原來為火車倉儲轉運站的米慎灣經過21世紀第一個十年改造後，在AT\&T球場和米慎灣科學園區的帶領下，吸引了許多專業人士和新興商業進駐，重新活化為商業住宅區。舊金山將於2013年在米慎灣南邊舉行美國杯帆船比賽。

### 著名公司和網站

  - [Twitter](../Page/Twitter.md "wikilink")

<!-- end list -->

  -
    社交网站、微博客服务、互联网媒体。用户可以经由SMS、实时通信、电邮、Twitter网站或Twitter第三方应用发布更新（称为tweets），输入最多280字的更新。

<!-- end list -->

  - [craigslist.org](../Page/Craigslist.md "wikilink")

<!-- end list -->

  -
    為世界最大的免費買賣和社談交換網站，總部設於舊金山

<!-- end list -->

  - [維基媒體基金會](../Page/維基媒體基金會.md "wikilink")

<!-- end list -->

  -
    [維基百科經營者](../Page/維基百科.md "wikilink")，總部設於舊金山

<!-- end list -->

  - [BitTorrent](../Page/BitTorrent.md "wikilink")
  - [Citycarshare.org](../Page/Citycarshare.org.md "wikilink")/[Zipcar.com](../Page/Zipcar.com.md "wikilink")

<!-- end list -->

  -
    仿效歐洲市內汽車出租模式，個人只要在網站上申請帳號，就可以在特定地點即時領取車輛，方便迅速。

<!-- end list -->

  - [GAP](../Page/Gap_\(服飾\).md "wikilink")

<!-- end list -->

  -
    1969年創立於舊金山，目前包含了[香蕉共和國](../Page/香蕉共和國_\(服飾\).md "wikilink")、[舊海軍](../Page/舊海軍.md "wikilink")（Old
    Navy）、[Piperlime](../Page/Piperlime.md "wikilink")、[Athleta](../Page/Athleta.md "wikilink")
    共五大服飾專賣店，約有3465間店面分布於全世界。

<!-- end list -->

  - [太平洋瓦電公司](../Page/太平洋瓦電公司.md "wikilink") (PG\&E)

<!-- end list -->

  -
    是一家成立於1905年，負責提供加州北部至中部約五百萬戶天然瓦斯和電力等的[公用事業公司](../Page/公用事業.md "wikilink")。1993年的[鉻金屬污染事件曾改編為電影](../Page/鉻.md "wikilink")《[永不妥協](../Page/永不妥協.md "wikilink")》。

<!-- end list -->

  - [杜比實驗室](../Page/杜比實驗室.md "wikilink")

<!-- end list -->

  -
    創立於[倫敦](../Page/倫敦.md "wikilink")，目前總部設於舊金山

<!-- end list -->

  - [VISA總部](../Page/VISA.md "wikilink")
  - [富國銀行總部](../Page/富國銀行.md "wikilink")
  - [美國銀行總部](../Page/美國銀行.md "wikilink")
  - Sharffen Berger 巧克力-主要製造高純度可可巧克力
  - [吉德利巧克力](../Page/吉德利巧克力.md "wikilink")（Ghirardelli）
  - [李維斯牛仔褲](../Page/李維斯.md "wikilink")

<!-- end list -->

  -
    旗艦店位於[聯合廣場](../Page/聯合廣場_\(舊金山\).md "wikilink")

<!-- end list -->

  - [盧卡斯電影](../Page/盧卡斯電影.md "wikilink")（Lucasfilm）

<!-- end list -->

  -
    位於[要塞園區](../Page/三藩市要塞.md "wikilink")

<!-- end list -->

  - [McKesson Corporation](../Page/McKesson_Corporation.md "wikilink")

<!-- end list -->

  -
    世界最大醫療企業

<!-- end list -->

  - [Peet's Coffee & Tea](../Page/Peet's_Coffee_&_Tea.md "wikilink")

<!-- end list -->

  -
    1966年創立於[柏克萊](../Page/柏克萊.md "wikilink")，為舊金山灣區獨特咖啡和品茶聯鎖店，[星巴克早期創業曾向其購買咖啡成品並仿效經營模式](../Page/星巴克.md "wikilink")。

<!-- end list -->

  - [阿米巴音樂城](../Page/阿米巴音樂城.md "wikilink")（Amoeba Music）

<!-- end list -->

  -
    美西最著名的[二手音樂交換店](../Page/二手.md "wikilink")，[海特-阿什伯利分店內佔地兩千平方公尺](../Page/海特-阿什伯利.md "wikilink")，將近有10萬張各類的音樂CD、卡帶、傳統唱片和DVD，規模僅次於其[好萊塢分店](../Page/好萊塢.md "wikilink")。

<!-- end list -->

  - [優步](../Page/優步.md "wikilink")（Uber）

## 人口

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>舊金山市與縣<br />
年度人口[24]</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1860</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1870</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1880</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1890</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1900</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1910</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1920</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1930</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1940</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1950</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1960</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1970</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1980</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1990</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2000</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2010</p></td>
</tr>
</tbody>
</table>

舊金山的人口在2010年人口普查為805,235人，是全美第13大城市。舊金山每平方英里超過一萬人，人口密度是全美國大城市第二高，僅次於[紐約市](../Page/紐約市.md "wikilink")\[25\]。舊金山是[舊金山灣區傳統上的商業和文化中心](../Page/舊金山灣區.md "wikilink")，也是「[聖荷西](../Page/聖荷西_\(美國加州\).md "wikilink")—舊金山—[奧克蘭](../Page/奥克兰_\(加州\).md "wikilink")」[綜合統計區的構成部份之一](../Page/綜合統計區.md "wikilink")，總人口有超過700萬人，於2000年美國人口普查時為第五大的統計區\[26\]。

舊金山是個適合移民的城市，因為非拉丁裔[白人約佔](../Page/白人.md "wikilink")42%的總人口；[亞裔佔了](../Page/亞裔美國人.md "wikilink")33.3%的總人口，特別是那些在矽谷的印度籍工程師或來自東亞的科技人才；各[拉丁裔族群總共僅佔](../Page/拉丁美裔人.md "wikilink")15%的總人口；[非洲裔僅佔](../Page/非裔美國人.md "wikilink")6%的總人口，少於全美國非裔人口比例\[27\]。終身都居住在舊金山的市民並不多，而有34.4%的市民在外國出生。

舊金山有全美國各州縣份裡最高比例的[LGBT人口及](../Page/LGBT.md "wikilink")[同性伴侶家庭](../Page/同性伴侶.md "wikilink")，並且舊金山灣區有比美國其他都會區都高的同性伴侶家庭比例\[28\]。男同性戀多於女同性戀；在一個估計裡，五個35歲以上的成年男性人口中就有一個是同性戀\[29\]。

舊金山家庭的中間年收入在2009年時為70247美元，是在全美國大城市裡排名第五高\[30\]。與美國其他地方趨勢一樣，中收入家庭的往外遷移造成了舊金山不小的貧富差距\[31\]，同時也造成了兒童僅佔14.5%的人口比例，比美國其他大城市都還少\[32\]。舊金山有11.7%的家庭在貧窮線下，比全國平均要少，也是在人口普查局統計裡最少的大城市之一\[33\]。

[街頭遊民是舊金山自](../Page/街頭遊民.md "wikilink")1980年代起時常面對的問題之一，也是個具有爭議性的問題。一般相信舊金山是美國大城市裡遊民比例最高的城市\[34\]。2003年的刑案和產物案[犯罪率為每](../Page/犯罪率.md "wikilink")10萬人裡各別有742和4943件\[35\]，比全國平均要高\[36\]。舊金山在這個兩個犯罪案統計裡是美國50大城市裡的排名分別是第32和第38名\[37\]。

## 政府

[Sfcityhall.jpeg](https://zh.wikipedia.org/wiki/File:Sfcityhall.jpeg "fig:Sfcityhall.jpeg")\]\]
舊金山是[加州自](../Page/加州.md "wikilink")1856年起唯一的[合併市縣](../Page/合併市縣.md "wikilink")。[舊金山市長是也是縣政系統裡的最高的行政長官](../Page/舊金山市長.md "wikilink")，[市議會也同時兼具](../Page/市議會.md "wikilink")[縣監事會議](../Page/監事會議.md "wikilink")(board
of
supervisors)的功能。由於舊金山的特殊定位，舊金山也能在市區外市府擁有土地執法，如[舊金山國際機場](../Page/舊金山國際機場.md "wikilink")。舊金山國際機場座落於舊金山南邊的[聖馬刁縣內](../Page/聖馬刁縣.md "wikilink")，但卻由舊金山市政府擁有和管理。舊金山也擁有[優勝美地國家公園](../Page/優勝美地國家公園.md "wikilink")[Hetch
Hetchy谷集水區的專用權](../Page/Hetch_Hetchy谷.md "wikilink")，這個權力是由1913年的[Raker法案所賦予的](../Page/Raker法案.md "wikilink")。

市憲章規定市政府是由兩個平等的部門組成。行政部門的主要首長為市長和其他全市民選官員；行政部門也包含市民服務如警察局和消防隊。11名議員組成的[舊金山市監事會議](../Page/舊金山市監事會議.md "wikilink")(San
Francisco Board of Supervisors)為舊金山的立法部門，現任議長為Aaron
Peskin(2006年)；監事會議負責監督所有市政府運作情況。議員由各個選區分別選出\[38\]。假如市長死亡或無法行事，監事會議議長自動繼任市長職務；這個情況在1978年發生過一次，當時的監事會議議長[黛安·范士丹在市長](../Page/黛安·范士丹.md "wikilink")[乔治·莫斯科尼遭到刺殺後繼任為市長](../Page/乔治·莫斯科尼.md "wikilink")。現在的舊金山年度預算為$50億之多\[39\]。

美國聯邦政府在舊金山設有許多代表聯邦政府利益的各種機關組織，如[美國第九巡迴上訴法庭](../Page/美國第九巡迴上訴法庭.md "wikilink")
(Ninth Circuit U.S. Court of
Appeals)、[聯邦儲備銀行舊金山分行](../Page/聯邦儲備銀行舊金山分行.md "wikilink")
、與[美國鑄幣局](../Page/美國鑄幣局.md "wikilink")
。舊金山直到1990年初期還保有三個大型軍事設施：[舊金山要塞](../Page/舊金山要塞.md "wikilink")
(Presidio of San
Francisco)、[金銀島](../Page/金銀島_\(舊金山\).md "wikilink")、和[舊金山海軍造船廠](../Page/舊金山海軍造船廠.md "wikilink")
(San Francisco Naval Shipyard)。年度的[艦隊周](../Page/艦隊周.md "wikilink")
(Fleet Week)
慶祝活動為這些軍事設施的遺留產物之一。加州州政府以舊金山為[加利福尼亞最高法院](../Page/加利福尼亞最高法院.md "wikilink")
(Supreme Court of California)
和一些州府機關的總部所在地。多達30個國家在舊金山設有[領事館](../Page/領事館.md "wikilink")。\[40\]

### 博物館及表演藝術

  - [比爾·格雷厄姆市政禮堂](../Page/比爾·格雷厄姆市政禮堂.md "wikilink")
  - [戰爭紀念歌劇院](../Page/戰爭紀念歌劇院.md "wikilink")
  - [戴維斯交響音樂廳](../Page/戴維斯交響音樂廳.md "wikilink")（Davies Symphony Hall）
  - [奧芬劇院](../Page/奧芬劇院_\(三藩市\).md "wikilink")（）-主要為百老匯歌劇表演
  - [金門劇院](../Page/金門劇院.md "wikilink")（Golden Gate Theater）-主要為百老匯歌劇表演
  - [克恩劇院](../Page/克恩劇院.md "wikilink")（）-主要為百老匯歌劇表演

## 教育

[三藩市聯合校區是旧金山的](../Page/三藩市聯合校區.md "wikilink")[教育局](../Page/教育局.md "wikilink")。

市内著名的[大學包括](../Page/大學.md "wikilink")：

  - [舊金山加利福尼亞大學](../Page/舊金山加利福尼亞大學.md "wikilink")-建立於1873年，注重於醫學和生化科技研究，學校包含醫學系、牙醫系，藥學系、護理系和學士後研究所，主要校區位於金門公園附近的帕納塞斯街
  - [舊金山州立大學](../Page/舊金山州立大學.md "wikilink")-建立於1899年，為加州23個州立大學之一，主要校區位於南舊金山靠近戴立市一帶
  - [加州大學黑斯廷斯法律學院](../Page/加州大學黑斯廷斯法律學院.md "wikilink")-成立於1878年，為加州大學系統之一，校區位於市政中心和田德隆區之間
  - [金門大學](../Page/金門大學_\(美國\).md "wikilink")-源自於1896年建立的YMCA私立夜間大學，2009年自田德隆區的金門街搬遷到目前
    SOMA 區的米慎街
  - [舊金山大學](../Page/舊金山大學.md "wikilink")-於1855年建校，為私立大學，是美國西部最老的學校之一。校園位於金門公園东邊的高丘上，很遠可以看到。學校的[法學院是世界馳名的學校](../Page/法學院.md "wikilink")。
  - [舊金山藝術大學](../Page/舊金山藝術大學.md "wikilink")-成立於1929
    年，為私立藝術大學，授予藝術學士、大學學士、藝術副學士、藝術碩士、碩士、建築碩士、專業證書課程或個人進修知識充實等藝術與設計領域之學位或課程，內容包括30多項領域的專業課程。

### 体育

| 球队                                   | 项目                             | 联盟                               |
| ------------------------------------ | ------------------------------ | -------------------------------- |
| [金州勇士](../Page/金州勇士.md "wikilink")   | [籃球](../Page/篮球.md "wikilink") | [NBA](../Page/NBA.md "wikilink") |
| [舊金山巨人](../Page/舊金山巨人.md "wikilink") | [棒球](../Page/棒球.md "wikilink") | [MLB](../Page/MLB.md "wikilink") |
|                                      |                                |                                  |

## 旅遊

[thumb](../Page/文件:Golden_gate2.jpg.md "wikilink")
[Sanfran_61_bg_032605.jpg](https://zh.wikipedia.org/wiki/File:Sanfran_61_bg_032605.jpg "fig:Sanfran_61_bg_032605.jpg")\]\]
[Alcatraz11.JPEG](https://zh.wikipedia.org/wiki/File:Alcatraz11.JPEG "fig:Alcatraz11.JPEG")\[41\]\]\]

### 城市景點列表

市場街

  - [舊金山碼頭](../Page/舊金山碼頭.md "wikilink")（Port of San Francisco）
  - [聯合廣場](../Page/聯合廣場_\(舊金山\).md "wikilink")
  - [卡斯楚區](../Page/卡斯楚區.md "wikilink")
  - [F號古老電車](../Page/F號古老電車.md "wikilink")
  - [路面纜車](../Page/舊金山纜車系統.md "wikilink")

市場街北邊

  - [九曲花街](../Page/九曲花街.md "wikilink")：舊金山最有名的彎曲街道
  - [渔人碼頭](../Page/三藩市漁人碼頭.md "wikilink")
      - [39號碼頭](../Page/39號碼頭.md "wikilink")、[吉爾德利巧克力廣場](../Page/吉爾德利巧克力廣場.md "wikilink")、[海灣水族館](../Page/海灣水族館.md "wikilink")
  - [中國城](../Page/舊金山中國城.md "wikilink")
  - [北灘](../Page/北灘_\(舊金山\).md "wikilink")（義大利區）
  - [電報塔](../Page/電報塔.md "wikilink")：野生鸚鵡
  - [舊金山大學會](../Page/舊金山大學會.md "wikilink")
  - [帕潘尼多號博物館](../Page/帕潘尼多號博物館.md "wikilink")（USS Pampanito Museum）

市場街南邊 SOMA

  - [圣方济各传教站](../Page/圣方济各传教站.md "wikilink")（都勒教會） / 米慎區

舊金山西區

  - [金門大橋](../Page/金門大橋.md "wikilink")-起點為普西迪高地北端，遊客可行走於橋上，觀望舊金山灣景觀，橋的對岸為[索薩利托](../Page/索薩利托.md "wikilink")（）小鎮，為假日休閑去處
  - [金門公園](../Page/金門公園.md "wikilink")
    內有[日本茶園](../Page/日本茶園.md "wikilink")，[狄楊博物館](../Page/狄楊博物館.md "wikilink")
  - [貝克海灘](../Page/貝克海灘.md "wikilink")——[天體海灘](../Page/裸體海灘.md "wikilink")
  - [懸巖餐廳](../Page/懸巖餐廳.md "wikilink") Cliff House
  - [海灘小屋啤酒廠及餐廳](../Page/海灘小屋啤酒廠及餐廳.md "wikilink") Beach Chalet Brewery
    & Restaurant
  - [海特-阿什伯利](../Page/海特-阿什伯利.md "wikilink")（）
    [嘻皮發源地](../Page/嘻皮.md "wikilink")
  - [阿拉莫廣場](../Page/阿拉莫廣場.md "wikilink")
  - [要塞公園](../Page/舊金山要塞.md "wikilink")（Presidio Park）
  - [碉堡角](../Page/碉堡角.md "wikilink")（）/ [Crissy
    Field](../Page/Crissy_Field.md "wikilink")
  - [太平洋高地](../Page/太平洋高地.md "wikilink")
  - [日本街](../Page/三藩市日本街.md "wikilink") /
    [費爾摩街](../Page/費爾摩街_\(三藩市\).md "wikilink")

### 自然景觀

  - [金門公園](../Page/金門公園.md "wikilink")
  - [天使島](../Page/天使島.md "wikilink") 島上曾為19世紀及20世紀早期移民通關處 有移民歷史館
  - [惡魔島](../Page/惡魔島.md "wikilink")
  - [金銀島](../Page/金銀島_\(加州\).md "wikilink") 觀看舊金山夜景最佳地點
  - [大洋灘](../Page/三藩市大洋灘.md "wikilink")（）
  - [雙峰](../Page/雙峰_\(三藩市\).md "wikilink")（Twin Peaks）可以遙望整個舊金山
  - [花崙島嶼](../Page/花崙島嶼.md "wikilink")（Farallon
    Islands）可在漁人碼頭附近搭乘觀光船前往島嶼周圍賞鯨
  - [Lincoln Park](../Page/Lincoln_Park.md "wikilink") 位於舊金山西北端
  - [49里景觀道路](../Page/49里景觀道路.md "wikilink") 49-Mile Scenic Drive
  - [39號碼頭](../Page/39號碼頭.md "wikilink") 海狮休息區

### 節慶

  - [Cinco de
    Mayo](../Page/Cinco_de_Mayo.md "wikilink")-五月五日墨西哥獨立紀念日，當日在教會區有紀念遊行和街頭遊會。
  - [Bay to
    Breakers賽跑](../Page/Bay_to_Breakers.md "wikilink")-起於1912年，於每年的五月第三個週日舉行，全長12公里（7.5英里），起點自市中心碼頭（BAY）經過
    金門公園，終點為浪花拍打（BREAKERS）的[大洋灘](../Page/三藩市大洋灘.md "wikilink")。除了有正規比賽的參加者，另有許多打扮多采多姿，或是裸體的參賽者，每年吸引來自各地將近七萬名參賽者。
  - [同性戀大遊行](../Page/舊金山同性戀大遊行.md "wikilink")-起於1970年，於每年六月份最後一個週末舉行，通常吸引各地的同性戀、雙性戀、變性人和國內外數十萬遊客前來參與，為全世界最有名的同性戀遊行。週六通常是圍繞市政廳和卡斯楚區的週邊慶祝活動，週日才是遊行主軸。遊行隊伍從碼頭港口市場街直到第八街。

## 交通

[San_Francisco_Cable_Car_at_Chinatown.jpg](https://zh.wikipedia.org/wiki/File:San_Francisco_Cable_Car_at_Chinatown.jpg "fig:San_Francisco_Cable_Car_at_Chinatown.jpg")
[San_Francisco_International_Airport_at_night.jpg](https://zh.wikipedia.org/wiki/File:San_Francisco_International_Airport_at_night.jpg "fig:San_Francisco_International_Airport_at_night.jpg")\]\]

  - [舊金山國際機場](../Page/舊金山國際機場.md "wikilink")（SFO）在城南21公里處，可以從市區搭[灣區捷運和SAM](../Page/灣區捷運.md "wikilink")
    TRANS 直達巴士前往，通行時間約30分鐘。
  - 市區灰狗巴士站有直達車前往加州其他城市。

市内有數種公共交通系统：

  - [灣區捷運](../Page/灣區捷運.md "wikilink")：簡稱BART，為連接舊金山和整個東灣地區主要交通工具，旅客可搭乘地鐵前往舊金山國際機場或是奧克蘭國際機場，灣區捷運和CALTRAIN連結整個灣區成為一個大眾運輸環繞系統。
  - [加州鐵道](../Page/加州鐵道.md "wikilink")（[網站](http://www.caltrain.org/)）：簡稱Caltrain，從舊金山AT\&T棒球場為起點，經過舊金山國際機場、舊金山半島、南下至聖荷西，為南灣和半島的通勤人士前往舊金山主要交通工具。

[San_Francisco_Nob_Hill_2.jpg](https://zh.wikipedia.org/wiki/File:San_Francisco_Nob_Hill_2.jpg "fig:San_Francisco_Nob_Hill_2.jpg")\]\]

  - [旧金山城市铁路](../Page/旧金山城市铁路.md "wikilink")（MUNI）: 是舊金山最主要,
    也是全美第七大的大衆交通運輸系統
      - [路面纜車](../Page/舊金山纜車系統.md "wikilink")：為適應舊金山特有的山丘地形，而發展了獨特地底纜繩牽引式的纜車，俗稱「叮噹車」。舊金山纜車最早出現於1873年克雷街，後來逐漸成為20世紀初市區主要交通工具。1980年代因纜車行駛速度過慢，而且系統老舊又缺乏維修，市政府曾考慮要廢止纜車，後來保留了目前三條線路：東西向的加利福尼亞街線、南北向的跑華街-海德街線（Powell-Hyde）及跑華街-梅森街線（Powell-Mason）。纜車穿過主要旅遊地點，為舊金山最有名的特色之一。
      - [有轨电车](../Page/舊金山輕軌.md "wikilink")：線路以英文字母命名。目前有F、J、K、L、M、N、T線。F線為世界各地收集而來的古老電車，經改裝後行使於卡斯楚區、市場街、經由碼頭港口後前往漁人碼頭，非常有特色。其他線路經過市區後前往城市的各社區。
      - [無軌電車及](../Page/無軌電車.md "wikilink")[巴士](../Page/巴士.md "wikilink")：以數字排名。有些线路在早晚上下班时间加不同快班车“Express”或者短途车“Limited"。半夜（约1点到4点）有不同的晚班路線，成为猫頭鹰路線（Owl
        Service）。

## 著名人物

  - [安塞爾·亞當斯](../Page/安塞爾·亞當斯.md "wikilink")（1902-1984） 攝影師
  - [威利·布朗](../Page/威利·布朗.md "wikilink")（Willie Brown，1934-）
    前任市長（1996-2004）
  - [黛安·范士丹](../Page/黛安·范士丹.md "wikilink")（1933-）舊金山首位女市長（1978-1988），參議員（1992-）
  - [傑利·布朗](../Page/傑利·布朗.md "wikilink")（1938-）加州州長（1975-1983），奧克蘭市長（1999-2007），加州州檢察長（2007-2011），加州州长（2011-）
  - [南西·普洛西](../Page/南西·普洛西.md "wikilink")（1940-）联邦眾議院議長（2007-2011），联邦众议院民主党领袖（2011-）
  - [哈维·米尔克](../Page/哈维·米尔克.md "wikilink")（1930-1978），旧金山市监督委员会第五区的委员（1978），美国同性恋运动人士，美国政坛中首位公开同性恋身份的人士。
  - [貝瑞·邦茲](../Page/貝瑞·邦茲.md "wikilink")（1964-）
    [巨人隊職棒球員](../Page/舊金山巨人.md "wikilink")
  - [威利·梅斯](../Page/威利·梅斯.md "wikilink")（1931-）舊金山巨人隊職棒球員　
  - [Keith Hernandez](../Page/Keith_Hernandez.md "wikilink")
    （1953-）[大都會職棒球員](../Page/大聯盟.md "wikilink")
  - [O·J·辛普森](../Page/O·J·辛普森.md "wikilink")（1947-） 美式足球球員
  - [唐納·費雪](../Page/唐納·費雪.md "wikilink")（Donald Fisher，1928-2009）
    [GAP創始人](../Page/Gap_\(服飾\).md "wikilink")
  - Craig Newmark（1952）
    [Craigslist網站創始人](../Page/Craigslist.md "wikilink")
  - [高登·摩爾](../Page/高登·摩爾.md "wikilink")（1921-）[英代爾公司創辦人之一](../Page/英代爾.md "wikilink")，提出
    [摩爾定律](../Page/摩爾定律.md "wikilink")
  - [李小龍](../Page/李小龍.md "wikilink")（1940-1973）
  - [克林·伊斯威特](../Page/克林·伊斯威特.md "wikilink")（1930-）電影演員 導演
  - [羅賓·威廉斯](../Page/羅賓·威廉斯.md "wikilink")（1951-2014）電視電影喜劇演員 脫口秀主持人
  - [丹尼·葛洛佛](../Page/丹尼·葛洛佛.md "wikilink")（1946-）電影演員
  - [班傑明·布萊特](../Page/班傑明·布萊特.md "wikilink")（1963-）電視電影演員
  - [喬·迪馬喬](../Page/喬·迪馬喬.md "wikilink") （1914-1999）
    職棒[洋基隊球員](../Page/紐約洋基隊.md "wikilink")，為[瑪麗蓮·夢露第二任丈夫](../Page/瑪麗蓮·夢露.md "wikilink")
  - [傑克·倫敦](../Page/傑克·倫敦.md "wikilink")（1876-1916）
    [野性的呼喚](../Page/野性的呼喚.md "wikilink") 作家
  - [傑瑞·賈西亞](../Page/傑瑞·賈西亞.md "wikilink")（Jerry
    Garcia，1942－1995）搖滾歌手、吉他手
  - [羅伯·佛斯特](../Page/羅伯·佛斯特.md "wikilink")（1874-1963）詩人，四度[普立茲獎得主](../Page/普立茲獎.md "wikilink")
  - [艾倫·金斯堡](../Page/艾倫·金斯堡.md "wikilink")（1926-1997）詩人，普立茲獎得主
  - [譚恩美](../Page/譚恩美.md "wikilink")（1952-）華裔小說作家
  - [鄭嘉穎](../Page/鄭嘉穎.md "wikilink")（1969-）香港著名影視演員及歌手
  - [楊婉儀](../Page/楊婉儀.md "wikilink") (1970-) 香港商家.
    前[無綫女藝員](../Page/TVB.md "wikilink").
  - [日野聰](../Page/日野聰.md "wikilink") (1978-)
    日本[聲優](../Page/聲優.md "wikilink")
  - [林書豪](../Page/林書豪.md "wikilink") (1988-)
    首位[台灣裔](../Page/台灣.md "wikilink")[美國職業籃球球星](../Page/NBA.md "wikilink")，現效力於[多倫多暴龍隊](../Page/多倫多暴龍隊.md "wikilink")，成長於[灣區](../Page/舊金山.md "wikilink")，高中畢業於[灣區西南方的](../Page/灣區.md "wikilink")[聖塔克拉拉郡](../Page/聖塔克拉拉縣_\(加利福尼亞州\).md "wikilink")[帕羅奧圖高中](../Page/帕羅奧圖.md "wikilink")。
  - [鄭秀妍](../Page/Jessica_\(韓國歌手\).md "wikilink") (1989-)
    [韓國女子](../Page/韓國.md "wikilink")-{zh-tw:團體;zh-cn:组合;zh-hk:組合;}-[少女時代前成員](../Page/少女時代.md "wikilink")，是一-{zh-tw:團體;zh-cn:组合;zh-hk:組合;}-「f(x)」成員鄭秀晶的姊姊。
  - [黃美英](../Page/Tiffany_\(少女時代\).md "wikilink") (1989-)
    [韓國女子](../Page/韓國.md "wikilink")-{zh-tw:團體;zh-cn:组合;zh-hk:組合;}-[少女時代成員](../Page/少女時代.md "wikilink")
  - [禹成賢](../Page/禹成賢.md "wikilink") (1991-)
    韓國男子-{zh-tw:團體;zh-cn:组合;zh-hk:組合;}-[U-KISS成員](../Page/U-KISS.md "wikilink")。
  - [鄭秀晶](../Page/鄭秀晶.md "wikilink") (1994-)
    韓國女子-{zh-tw:團體;zh-cn:组合;zh-hk:組合;}-[f(x)成員](../Page/f\(x\)_\(組合\).md "wikilink")，是一-{zh-tw:團體;zh-cn:组合;zh-hk:組合;}-「少女時代」前成員鄭秀妍的妹妹。

## 参考资料

## 外部链接

  - [市政府官方网站](http://www.sfgov.org/)（英文）
  - [旅游地点网站 SF Gate News Site (英文)](http://www.sfgate.com/)
  - [金门大学 Golden Gate University 英文](http://www.ggu.edu/)
  - [奥克兰突袭者队中文网頁 Oakland Raiders Official Chinese Site
    英文，中文](http://www.raiders.com/newsroom/chinese.jsp)
  - [美國職棒大聯盟舊金山巨人隊官方網站 the San Francisco Giants Official Site
    英文](http://sanfrancisco.giants.mlb.com/index.jsp?c_id=sf)

[三藩市](../Category/三藩市.md "wikilink")
[Category:舊金山灣區城市](../Category/舊金山灣區城市.md "wikilink")
[Category:加利福尼亞州行政區劃](../Category/加利福尼亞州行政區劃.md "wikilink")
[Category:太平洋沿海城市](../Category/太平洋沿海城市.md "wikilink")
[Category:旧金山湾区行政区划](../Category/旧金山湾区行政区划.md "wikilink")

1.

2.   United Nations|accessdate=2017-01-13|work=www.un.org}}

3.   United Nations|accessdate=2017-01-13|work=www.un.org}}

4.

5.  [Top U.S. Destinations for International
    Visitors](http://www.hotel-price-index.com/2013/fall/american-travel-habits/top-locations-for-americans.html).
    The Hotel Price Index. Retrieved on April 12, 2014.

6.  [美國之音報導內容](http://www.voachinese.com/content/bilingual-20130710/1698966.html)

7.  [駐舊-{}-金山台北經濟文化辦事處](http://www.taiwanembassy.org/ussfo/)

8.  [中华人民共和国驻舊-{}-金山总领事馆](http://www.chinaconsulatesf.org/chn/)

9.

10. [美國國家概況—中華人民共和國外交部](http://www.fmprc.gov.cn/chn/pds/gjhdq/gj/bmz/1206_22/)

11.

12.

13.
14.
15.
16.
17.
18. [Overseas Visitors To Select U.S. Cities/Hawaiian
    Islands 2002-2001](http://www.tinet.ita.doc.gov/view/f-2002-45-561/index.html)
     美國商業部旅行和旅遊業辦公室(U.S. Department of Commerce, Office of Travel &
    Tourism Industries)；2006年8月27日造訪

19. [City and County of San Francisco: Sights in San
    Francisco.](http://www.sfgov.org/site/mainpages_page.asp?id=18191)
    舊金山市縣；2006年9月4日造訪

20. Raine, George (2006年5月13日) [Tourism dollars add up: San Francisco
    seeing more visitors, more cash -- it's our No. 1
    industry](http://sfgate.com/cgi-bin/article.cgi?f=/c/a/2006/05/13/BUG0TIR3FC37.DTL)
    [舊金山紀事報](../Page/舊金山紀事報.md "wikilink")；2006年8月23日造訪

21. Spain, William (2004年11月13日) [Cost factors: Top convention cities
    boast most-affordable
    lodging](http://www.marketwatch.com/News/Story/Story.aspx?guid=%7B708E799D-2F2A-4AE8-9814-096BDF4F98DA%7D&siteid=mktw&dist=)
    CBS市場展望(CBS MarketWatch)；2006年9月3日造訪

22. [San Francisco:
    Economy](http://www.city-data.com/us-cities/The-West/San-Francisco-Economy.html)
    city-data.com；2006年9月30造訪

23. [Fortune 500 2006](http://money.cnn.com/magazines/fortune/fortune500/full_list/)
    CNNMoney.com；2006年8月31造訪

24.

25.

26. [Census 2000 PHC-T-29. Ranking Tables for Population of Metropolitan
    Statistical Areas, Micropolitan Statistical Areas, Combined
    Statistical Areas, New England City and Town Areas, and Combined New
    England City and Town Areas: 1990
    and 2000](http://www.census.gov/population/cen2000/phc-t29/tab06.xls)(Microsoft
    Excel) 美國人口普查局；於2006年8月31日造訪

27. [Table DP-1; Profile of General Demographic Characteristics: 2010
    Geographic Area: San Francisco County,
    California](http://www.bayareacensus.ca.gov/counties/SanFranciscoCounty.pdf)(PDF)
    美國人口普查局；於2006年8月31日造訪

28.

29.

30. [Income, Earnings, and Poverty Data from the 2005 American Community
    Survey](http://www.mercurynews.com/multimedia/mercurynews/news/mn_census_report01_083006.pdf)(PDF)
    美國人口普查局；於2005年9月5日造訪

31. Hendricks, Tyche (2006年6月22日) [RICH CITY POOR CITY: Middle-class
    neighborhoods are disappearing from the nation's cities, leaving
    only high- and low-income districts, new study
    says](http://www.sfgate.com/cgi-bin/article.cgi?file=/c/a/2006/06/22/MNG6HJIDMM1.DTL)*舊金山紀事報*
    於2006年9月5日造訪

32.

33. [The 2006 Statistical Abstract: Income, Expenditures, & Wealth,
    Table 691 - Household Income, Family Income, Per Capita Income, and
    Individuals and Families Below Poverty Level by
    City: 2003](http://www.census.gov/compendia/statab/tables/06s0691.xls)(Microsoft
    Excel) 美國人口普查局；於2006年9月5日造訪

34.

35. [Uniform Crime Reports: Table 8 - Offenses Known to Law Enforcement,
    by City 10,000 and over in
    Population, 2003](http://www.fbi.gov/ucr/cius_03/xl/03tbl08.xls)
    聯邦調查局；於2006年9月5日造訪

36. [Uniform Crime Reports: Table 1 - Crime in the United States, by
    Volume and
    Rate, 1984-2003](http://www.fbi.gov/ucr/cius_03/xl/03tbl01.xls)
    聯邦調查局；於2006年9月6日造訪

37.
38.

39.

40. [francisco\&st=CA\&q=Consulates Search for consulates in San
    Francisco,
    CA](http://www.yellowpages.com/sp/yellowpages/ypresults.jsp?t=0&v=3&s=2&p=1&q=Consulates&ci=san)
    Yellowpages.com, 2006年8月27日讀取

41. [Wildlife Field Guids: Wildlife Habitats in
    GGNRA](http://www.nps.gov/archive/goga/parklabs/library/wildlifeguide/wildlife_habitats.htm)
    美國國家公園處國家公園實驗室；2006年9月4日造訪