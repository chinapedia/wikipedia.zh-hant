[缩略图](https://zh.wikipedia.org/wiki/File:2P2B1268.jpg "fig:缩略图")

**英皇集團**（），[創辦人](../Page/創辦人.md "wikilink")[楊成](../Page/楊成_\(商人\).md "wikilink")，現任[董事局主席為其子](../Page/董事局主席.md "wikilink")[楊受成](../Page/楊受成.md "wikilink")，成立於1942年，從[上海街一間](../Page/上海街.md "wikilink")[鐘錶零售商](../Page/鐘錶.md "wikilink")[成安記表行發展成為綜合企業](../Page/成安記表行.md "wikilink")，業務包括[金融](../Page/金融.md "wikilink")、[地產](../Page/地產.md "wikilink")、[鐘錶珠寶](../Page/鐘錶珠寶.md "wikilink")、[娛樂](../Page/娛樂.md "wikilink")、[酒店](../Page/酒店.md "wikilink")、[傳媒](../Page/傳媒.md "wikilink")、[傢俬及室內佈置](../Page/傢俬及室內佈置.md "wikilink")、[餐飲](../Page/餐飲.md "wikilink")、電子競技及共享工作空間等。

旗下共有六間公司於香港聯合交易所主板上市，分別為從事地産業務的[英皇集團(國際)有限公司](../Page/英皇集團\(國際\)有限公司.md "wikilink")()、經營鐘錶的[英皇鐘錶珠寶有限公司](../Page/英皇鐘錶珠寶有限公司.md "wikilink")()、酒店及博彩業的[英皇娛樂酒店有限公司](../Page/英皇娛樂酒店有限公司.md "wikilink")()、證券經紀服務的[英皇證券集團有限公司](../Page/英皇證券集團有限公司.md "wikilink")()、從事娛樂、媒體及文化業務的[英皇文化產業集團有限公司](../Page/英皇文化產業.md "wikilink")()及經營歐洲名牌傢俬連鎖零售的[歐化國際有限公司](../Page/歐化國際有限公司.md "wikilink")()。

## 業務

### 金融

英皇金融證券集團於1978年起發展金融服務，提供[外匯](../Page/外匯.md "wikilink")、[黃金](../Page/黃金.md "wikilink")、[證券](../Page/證券.md "wikilink")、[基金](../Page/基金.md "wikilink")、[期貨及其他衍生工具的](../Page/期貨.md "wikilink")[經紀服務](../Page/經紀.md "wikilink")、[財務](../Page/財務.md "wikilink")、[融資及](../Page/融資.md "wikilink")[投資等服務](../Page/投資.md "wikilink")。

### 地產

[英皇集團（國際）負責地產業務](../Page/英皇集團（國際）.md "wikilink")，1990年上市，自2000年代中期起在[澳門收購物業](../Page/澳門.md "wikilink")，2006年於北京[發展房地產](../Page/發展.md "wikilink")。其項目包括[淺水灣](../Page/淺水灣.md "wikilink")「the
pulse」、「南灣御苑」、舂坎角「御濤灣」、大埔的低密度住宅大樓「[龍成堡](../Page/龍成堡.md "wikilink")」及「映月灣」、
西營盤「維港峰」、長沙灣「喜遇 」及屯門「珀居」等等。公司亦購入倫敦牛津街181-183號全幢商廈作投資，拓展海外市場。

### 鐘錶珠寶

[英皇鐘錶珠寶於](../Page/英皇鐘錶珠寶.md "wikilink")1942年成立，是英皇集團最早的業務，最初只出售鐘錶，60年代起兼營珠寶，並取得如[勞力士](../Page/勞力士.md "wikilink")、[百達翡麗](../Page/百達翡麗.md "wikilink")、[愛彼錶](../Page/愛彼錶.md "wikilink")、[卡地亞](../Page/卡地亞.md "wikilink")、[蕭邦](../Page/蕭邦.md "wikilink")、[伯爵](../Page/伯爵.md "wikilink")、以及[帝舵等品牌的中港澳地區代理權](../Page/帝舵.md "wikilink")；2008年7月於[香港聯合交易所主板上市](../Page/香港聯合交易所.md "wikilink")。

該公司於1996年在[北京賽特購物中心開設中國大陸首家分店](../Page/北京.md "wikilink")，分店現已擴至[廣州](../Page/廣州.md "wikilink")、[上海](../Page/上海.md "wikilink")、[重慶](../Page/重慶.md "wikilink")、[昆明](../Page/昆明.md "wikilink")、[成都](../Page/成都.md "wikilink")、[天津](../Page/天津.md "wikilink")、[瀋陽](../Page/瀋陽.md "wikilink")、[蘇州](../Page/蘇州.md "wikilink")、[衡陽](../Page/衡陽.md "wikilink")、[鄭州](../Page/鄭州.md "wikilink")、[無錫及](../Page/無錫.md "wikilink")[澳門等地](../Page/澳門.md "wikilink")；[香港分店則位於](../Page/香港.md "wikilink")[中環](../Page/中環.md "wikilink")、[灣仔](../Page/灣仔.md "wikilink")、[銅鑼灣及](../Page/銅鑼灣.md "wikilink")[尖沙咀等購物區內](../Page/尖沙咀.md "wikilink")。2009年又於尖沙咀的歷史建築物「[1881](../Page/1881.md "wikilink")」成立首間珠寶旗艦店。

### 娛樂

[英皇娛樂成立於](../Page/英皇娛樂.md "wikilink")1999年，主要業務包括製作及發展唱片、藝人管理、籌辦演唱會及舞台劇、電影及電視劇等。[英皇電影為娛樂媒體投資公司](../Page/英皇電影.md "wikilink")，業務包括電影製作、電影和影碟發行、廣告製作等，同時以品牌「英皇電影城」及「英皇UA電影城」於中國內地拓展電影院業務。2017年成立[英皇文化產業集團有限公司](../Page/英皇文化產業.md "wikilink")（），同時因應電子競技市場發展，於2017年9月成立附屬公司「英皇電競」。

### 酒店

英皇集團旗下現有酒店包括位於澳門的[英皇娛樂酒店](../Page/英皇娛樂酒店.md "wikilink")、香港灣仔的[英皇駿景酒店](../Page/英皇駿景酒店.md "wikilink")，以及港澳各一間的盛世酒店。另於香港經營兩間服務式住宅MORI
MORI及The Unit。

### 傳媒

英皇集團旗下的[新傳媒集團於](../Page/新傳媒集團.md "wikilink")1999年成立，從事出版及發行，每周出版不同刊物，五本刊物分別為《[東方新地](../Page/東方新地.md "wikilink")》、《[新假期](../Page/新假期.md "wikilink")》、《[新Monday](../Page/新Monday.md "wikilink")》及《[經濟一週](../Page/經濟一週.md "wikilink")》。另有[新誠豐柯式印刷有限公司](../Page/新誠豐柯式印刷有限公司.md "wikilink")，，隨著[免費報章出現](../Page/免費報紙.md "wikilink")，傳統報章近年銷量大幅下降，加上讀者讀報習慣有所改變，《[新報](../Page/新報.md "wikilink")》長期處於虧蝕狀態，決定2015年7月12日起停刊，所有受影響員工獲合理賠償及妥善安排。\[1\]\[2\]

### 傢俬及室內佈置

[歐化傢俬於](../Page/歐化傢俬.md "wikilink")1975年成立，專門代理歐洲高級傢俬及承接傢俬工程項目，為歐化國際有限公司（股份代號：1711）所管轄。旗下的歐化寶提供床褥、
梳化及床品批發及零售。

### 餐飲

英皇集團旗下經營多家食店，包括連續7年奪得米芝蓮一星的食府駿景軒、領導潮流的消閒熱點[dragon-i及](../Page/dragon-i.md "wikilink")[TAZMANIA
BALLROOM](../Page/TAZMANIA_BALLROOM.md "wikilink")。

### 電子競技

[英皇電競](https://ees.zone/)於2017年成立，透過舉辦電競聯賽、電競隊伍管理及開辦專業電競學院，全方位推動電競運動的健康發展。HKU
Space與其合作開辦全港首個政府認可的電競文憑課程。英皇電競旗下有三隊電競職業隊伍，分別是G-REX(PUBG) 、G-REX
Infinite
PUBG及G-REX(LOL)，而戰隊總監是香港首位「[英雄聯盟](../Page/英雄联盟.md "wikilink")」世界冠軍Toyz（劉偉健）。

### 共享工作空間

英皇集團的首個共享工作空間項目[Mustard
Seed](https://www.mustardseed.space/pricing?utm_source=AsiaPac_google&utm_medium=cpc&utm_term=mustard%20seed&utm_content=211859192144&utm_campaign=)位於灣仔英皇集團中心，為年輕創業者提供資源、合作機遇以及人際網絡，亦以不多於市值租金一半的優惠價租出辦公位。此項目參與了2017年施政報告提出「青年共享空間計劃」，支持初創企業及青年人創業。

## 相關連結

  - [英皇集團(國際)有限公司](../Page/英皇集團\(國際\)有限公司.md "wikilink")
  - [英皇鐘錶珠寶有限公司](../Page/英皇鐘錶珠寶有限公司.md "wikilink")
  - [英皇娛樂酒店有限公司](../Page/英皇娛樂酒店有限公司.md "wikilink")
  - [英皇證券集團有限公司](../Page/英皇證券集團有限公司.md "wikilink")
  - [英皇文化產業集團有限公司](../Page/英皇文化產業集團有限公司.md "wikilink")
  - [歐化國際有限公司](../Page/歐化國際有限公司.md "wikilink")

## 外部連結

  - [英皇集團官方網址](http://www.emperorgroup.com)
  - [英皇集團（國際）有限公司](http://www.emperorint.com)
  - [英皇鐘錶珠寶有限公司](http://www.emperorwatchjewellery.com)
  - [英皇娛樂酒店有限公司](http://www.emp296.com)
  - [英皇證券集團有限公司](http://www.emperorcapital.com)
  - [英皇文化產業集團有限公司](http://www.empculture.com)

## 參考資料

<references/>

[Category:香港綜合企業公司](../Category/香港綜合企業公司.md "wikilink")
[Category:英皇集團](../Category/英皇集團.md "wikilink")
[Category:經營赌業綜合集團](../Category/經營赌業綜合集團.md "wikilink")
[Category:1942年成立的公司](../Category/1942年成立的公司.md "wikilink")
[Category:家族式企業](../Category/家族式企業.md "wikilink")

1.
2.