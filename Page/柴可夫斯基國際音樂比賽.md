**柴可夫斯基國際音樂比賽**（）是世界知名的[古典音樂大賽](../Page/古典音樂.md "wikilink")，是為了紀念[俄羅斯](../Page/俄羅斯.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")[柴可夫斯基而設立](../Page/彼得·伊里奇·柴可夫斯基.md "wikilink")，每四年在[莫斯科舉辦一次](../Page/莫斯科.md "wikilink")，第一屆比賽始於1958年。最近一次的比賽是在2015年舉辦。

剛成立時，大賽只分鋼琴組與小提琴組，後來於1962年第二屆比賽時加入大提琴組，再於1966年第三屆時進一步新增聲樂組，此後便一直保持同樣的分組，並未再有任何增添。

柴可夫斯基大賽的委員會是由知名俄國音樂家所組成，管理則是由負責。至於[評審則遴選自世界各地的音樂人](../Page/評審.md "wikilink")，包括著名的[演奏家](../Page/演奏家.md "wikilink")、音樂學者[教授](../Page/教授.md "wikilink")、[指揮以及之前的大賽得獎者](../Page/指揮家.md "wikilink")，由他們負責評判參賽者的表現，並選出得獎者。

每屆比賽都於當年六月舉行，為三階段的[淘汰賽](../Page/淘汰賽.md "wikilink")。目前大賽共頒發26個獎項，其中每個器樂獨奏項目各六個獎，聲樂部分則男女各四個獎。不過實際頒發的數目可能會少於26個，因為有時金牌會從缺，有時也可能由多人共享獎項。至於在早期的大賽中，最多只會頒發八個獎項。

## 大獎得主

每屆比賽各項目獲得大獎的得主（此處僅列出金牌得主，若有特殊情況則註明於後）。

### 鋼琴

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th style="text-align: center;"><p><strong>金牌</strong></p></th>
<th style="text-align: center;"><p><strong>銀牌</strong></p></th>
<th style="text-align: center;"><p><strong>銅牌</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1958</p></td>
<td style="text-align: center;"><p><a href="../Page/范·克萊本.md" title="wikilink">范·克萊本</a>（美國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Lev_Vlassenko.md" title="wikilink">Lev Vlassenko</a>（蘇聯）<br />
<a href="../Page/劉詩昆.md" title="wikilink">劉詩昆</a>（中國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Naum_Shtarkman.md" title="wikilink">Naum Shtarkman</a>（蘇聯）</p></td>
</tr>
<tr class="even">
<td><p>1962</p></td>
<td style="text-align: center;"><p><a href="../Page/阿胥肯納吉.md" title="wikilink">阿胥肯納吉</a>（蘇聯）<br />
<a href="../Page/約翰·奧格東.md" title="wikilink">約翰·奧格東</a>（John Ogdon）（英國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Susan_Starr.md" title="wikilink">Susan Starr</a>（美國）<br />
<a href="../Page/殷承宗.md" title="wikilink">殷承宗</a>（中國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Eliso_Virsaladze.md" title="wikilink">Eliso Virsaladze</a>（蘇聯）</p></td>
</tr>
<tr class="odd">
<td><p>1966</p></td>
<td style="text-align: center;"><p><a href="../Page/格里戈里·利普马诺维奇·索科洛夫.md" title="wikilink">索科洛夫</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Misha_Dichter.md" title="wikilink">Misha Dichter</a>（美國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Victor_Eresko.md" title="wikilink">Victor Eresko</a>（蘇聯）</p></td>
</tr>
<tr class="even">
<td><p>1970</p></td>
<td style="text-align: center;"><p><a href="../Page/凱爾涅夫.md" title="wikilink">凱爾涅夫</a>（Vladimir Krainev）（蘇聯）<br />
<a href="../Page/John_Lill.md" title="wikilink">John Lill</a> （英國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Horacio_Gutiérrez.md" title="wikilink">Horacio Gutiérrez</a>（古巴）</p></td>
<td style="text-align: center;"><p><a href="../Page/Arthur_Moreira_Lima.md" title="wikilink">Arthur Moreira Lima</a>（巴西）<br />
<a href="../Page/Viktoria_Postnikova.md" title="wikilink">Viktoria Postnikova</a>（蘇聯）</p></td>
</tr>
<tr class="odd">
<td><p>1974</p></td>
<td style="text-align: center;"><p><a href="../Page/安卓·加伏里洛夫.md" title="wikilink">安卓·加伏里洛夫</a>（Andrei Gavrilov）（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/鄭明勛.md" title="wikilink">鄭明勛</a>（南韓）<br />
<a href="../Page/Stanislav_Igolinsky.md" title="wikilink">Stanislav Igolinsky</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Youri_Egorov.md" title="wikilink">Youri Egorov</a>（蘇聯）</p></td>
</tr>
<tr class="even">
<td><p>1978</p></td>
<td style="text-align: center;"><p><a href="../Page/米哈伊尔·普列特涅夫.md" title="wikilink">普列特涅夫</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Pascal_Devoyon.md" title="wikilink">Pascal Devoyon</a>（法國）<br />
<a href="../Page/André_Laplante.md" title="wikilink">André Laplante</a>（加拿大）</p></td>
<td style="text-align: center;"><p><a href="../Page/Nikolai_Demidenko.md" title="wikilink">Nikolai Demidenko</a>（蘇聯）<br />
<a href="../Page/Evgeny_Ryvkin.md" title="wikilink">Evgeny Ryvkin</a>（蘇聯）</p></td>
</tr>
<tr class="odd">
<td><p>1982</p></td>
<td style="text-align: center;"><p>從缺</p></td>
<td style="text-align: center;"><p>（英國）<br />
（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/小山實稚惠.md" title="wikilink">小山實稚惠</a>[1]（日本）</p></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td style="text-align: center;"><p><a href="../Page/貝瑞·道格拉斯.md" title="wikilink">貝瑞·道格拉斯</a>（英國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Natalia_Trull.md" title="wikilink">Natalia Trull</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Irina_Plotnikova.md" title="wikilink">Irina Plotnikova</a>（蘇聯）</p></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td style="text-align: center;"><p><a href="../Page/鲍里斯·瓦季莫维奇·别列佐夫斯基.md" title="wikilink">别列佐夫斯基</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Vladimir_Mischouk.md" title="wikilink">Vladimir Mischouk</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Kevin_Kenner.md" title="wikilink">Kevin Kenner</a>（美國）<br />
<a href="../Page/Johan_Schmidt.md" title="wikilink">Johan Schmidt</a>（德國）<br />
<a href="../Page/Anton_Mordasov.md" title="wikilink">Anton Mordasov</a>（蘇聯）</p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td style="text-align: center;"><p>從缺</p></td>
<td style="text-align: center;"><p><a href="../Page/尼古拉·卢甘斯基.md" title="wikilink">尼古拉·卢甘斯基</a> （俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Vadim_Rudenko.md" title="wikilink">Vadim Rudenko</a>（俄羅斯）<br />
<a href="../Page/白惠善.md" title="wikilink">白惠善</a>（南韓）</p></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td style="text-align: center;"><p><a href="../Page/丹尼斯·馬祖耶夫.md" title="wikilink">丹尼斯·馬祖耶夫</a>（Denis Matsuev）（俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Vadim_Rudenko.md" title="wikilink">Vadim Rudenko</a> （俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Freddy_Kempf.md" title="wikilink">Freddy Kempf</a>（英國）</p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td style="text-align: center;"><p><a href="../Page/上原彩子.md" title="wikilink">上原彩子</a>（日本）</p></td>
<td style="text-align: center;"><p><a href="../Page/Alexei_Nabiulin.md" title="wikilink">Alexei Nabiulin</a>（俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Tszyuy_Tszin.md" title="wikilink">Tszyuy Tszin</a>（中國）<br />
<a href="../Page/Andrey_Ponochevny.md" title="wikilink">Andrey Ponochevny</a> （俄羅斯）</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td style="text-align: center;"><p>從缺</p></td>
<td style="text-align: center;"><p><a href="../Page/Miroslav_Kultyshev.md" title="wikilink">Miroslav Kultyshev</a>（俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Alexander_Lubyantsev.md" title="wikilink">Alexander Lubyantsev</a> （俄羅斯）</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td style="text-align: center;"><p><a href="../Page/丹尼尔·奥列戈维奇·特里福诺夫.md" title="wikilink">特里福诺夫</a>（俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/孫悅音.md" title="wikilink">孫悅音</a>（南韓）</p></td>
<td style="text-align: center;"><p><a href="../Page/趙成珍.md" title="wikilink">趙成珍</a>（南韓）</p></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td style="text-align: center;"><p>迪米特·馬斯里夫（Dmitry Masleev）（俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Lucas_Geniušas.md" title="wikilink">Lucas Geniušas</a>（拉脫維亞/ 俄羅斯）<br />
<a href="../Page/黎卓宇.md" title="wikilink">黎卓宇</a>（美國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Sergei_Redkin.md" title="wikilink">Sergei Redkin</a> （俄羅斯）<br />
<a href="../Page/Daniel_Kharitonov.md" title="wikilink">Daniel Kharitonov</a> （俄羅斯）</p></td>
</tr>
<tr class="even">
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 小提琴

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th style="text-align: center;"><p><strong>金牌</strong></p></th>
<th style="text-align: center;"><p><strong>銀牌</strong></p></th>
<th style="text-align: center;"><p><strong>銅牌</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1958</p></td>
<td style="text-align: center;"><p>克林莫夫（<a href="../Page/:en:Valery_Klimov.md" title="wikilink">Valery Klimov</a>）（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Victor_Pikaizen.md" title="wikilink">Victor Pikaizen</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Ştefan_Ruha.md" title="wikilink">Ştefan Ruha</a>（羅馬尼亞）</p></td>
</tr>
<tr class="even">
<td><p>1962</p></td>
<td style="text-align: center;"><p>布里斯·谷特尼科夫（<a href="../Page/:en:Boris_Gutnikov.md" title="wikilink">Boris Gutnikov</a>）（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Shmuel_Ashkenasi.md" title="wikilink">Shmuel Ashkenasi</a>（以色列）<br />
<a href="../Page/Irina_Bochkova.md" title="wikilink">Irina Bochkova</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Nina_Beilina.md" title="wikilink">Nina Beilina</a>（蘇聯）<br />
<a href="../Page/久保陽子.md" title="wikilink">久保陽子</a>（日本）</p></td>
</tr>
<tr class="odd">
<td><p>1966</p></td>
<td style="text-align: center;"><p><a href="../Page/維克托·特列季亞科夫.md" title="wikilink">維克托·特列季亞科夫</a>（Viktor Tretiakov）（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/潮田益子.md" title="wikilink">潮田益子</a>（日本）<br />
<a href="../Page/Oleg_Kagan.md" title="wikilink">Oleg Kagan</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/佐藤容子.md" title="wikilink">佐藤容子</a>（日本）<br />
<a href="../Page/Oleh_Krysa.md" title="wikilink">Oleh Krysa</a>（蘇聯）</p></td>
</tr>
<tr class="even">
<td><p>1970</p></td>
<td style="text-align: center;"><p><a href="../Page/基東·克雷默.md" title="wikilink">基東·克雷默</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Vladimir_Spivakov.md" title="wikilink">Vladimir Spivakov</a>（蘇聯）<br />
<a href="../Page/藤川真弓.md" title="wikilink">藤川真弓</a>（日本）</p></td>
<td style="text-align: center;"><p><a href="../Page/Liana_Isakadze.md" title="wikilink">Liana Isakadze</a>（蘇聯）</p></td>
</tr>
<tr class="odd">
<td><p>1974</p></td>
<td style="text-align: center;"><p>從缺</p></td>
<td style="text-align: center;"><p>佛多（<a href="../Page/:en:Eugene_Fodor.md" title="wikilink">Eugene Fodor</a>）（美國）<br />
<a href="../Page/Ruben_Agaronyan.md" title="wikilink">Ruben Agaronyan</a>（蘇聯）<br />
<a href="../Page/Rusudan_Gvasaliya.md" title="wikilink">Rusudan Gvasaliya</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Marie-Annick_Nicolas.md" title="wikilink">Marie-Annick Nicolas</a>（法國）<br />
<a href="../Page/Vanya_Milanova.md" title="wikilink">Vanya Milanova</a>（保加利亞）</p></td>
</tr>
<tr class="even">
<td><p>1978</p></td>
<td style="text-align: center;"><p>埃爾馬·奧利維拉（<a href="../Page/:en:Elmar_Oliveira.md" title="wikilink">Elmar Oliveira</a>）（蘇聯）<br />
伊里亞·葛魯伯特（<a href="../Page/:en:Ilya_Grubert.md" title="wikilink">Ilya Grubert</a>）（美國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Mihaela_Martin.md" title="wikilink">Mihaela Martin</a>（羅馬尼亞）<br />
<a href="../Page/Dylana_Jenson.md" title="wikilink">Dylana Jenson</a>（美國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Irina_Medvedeva.md" title="wikilink">Irina Medvedeva</a>（蘇聯）<br />
<a href="../Page/Alexandr_Vinnitsky.md" title="wikilink">Alexandr Vinnitsky</a>（蘇聯）</p></td>
</tr>
<tr class="odd">
<td><p>1982</p></td>
<td style="text-align: center;"><p>維克托娃·慕洛娃（<a href="../Page/:en:Viktoria_Mullova.md" title="wikilink">Viktoria Mullova</a>）（蘇聯）<br />
謝爾蓋·史達德勒（<a href="../Page/:en:Sergei_Stadler.md" title="wikilink">Sergei Stadler</a>）（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/加藤知子.md" title="wikilink">加藤知子</a>（日本）</p></td>
<td style="text-align: center;"><p><a href="../Page/Stephanie_Chase.md" title="wikilink">Stephanie Chase</a>（美國）<br />
<a href="../Page/Andres_Cardenes.md" title="wikilink">Andres Cardenes</a>（美國）</p></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td style="text-align: center;"><p>卡勒（<a href="../Page/:en:Ilya_Kaler.md" title="wikilink">Ilya Kaler</a>）（蘇聯）<br />
<a href="../Page/:en:Raphaël_Oleg.md" title="wikilink">Raphaël Oleg</a>（法國）</p></td>
<td style="text-align: center;"><p><a href="../Page/薛偉.md" title="wikilink">薛偉</a>（中國）<br />
<a href="../Page/Maxim_Fedotov.md" title="wikilink">Maxim Fedotov</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Jane_Peters.md" title="wikilink">Jane Peters</a>（澳洲）</p></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td style="text-align: center;"><p><a href="../Page/諏訪內晶子.md" title="wikilink">諏訪內晶子</a>（日本）</p></td>
<td style="text-align: center;"><p><a href="../Page/Evgeny_Bushkov.md" title="wikilink">Evgeny Bushkov</a>（蘇聯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Alyssa_Park.md" title="wikilink">Alyssa Park</a>（美國）</p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td style="text-align: center;"><p>從缺</p></td>
<td style="text-align: center;"><p><a href="../Page/:en:Anastasiya_Chebotareva.md" title="wikilink">Anastasiya Chebotareva</a> （俄羅斯）<br />
<a href="../Page/:en:Jennifer_Koh.md" title="wikilink">Jennifer Koh</a>（美國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Graf_Murzh.md" title="wikilink">Graf Murzh</a>（俄羅斯）<br />
<a href="../Page/Marco_Rizzi.md" title="wikilink">Marco Rizzi</a>（義大利）</p></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td style="text-align: center;"><p>尼古拉·薩琴科（<a href="../Page/:en:Nikolay_Sachenko.md" title="wikilink">Nikolay Sachenko</a>）（俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/Latica_Honda-Rosenberg.md" title="wikilink">Latica Honda-Rosenberg</a>（德國）</p></td>
<td style="text-align: center;"><p><a href="../Page/Ichun_Pan.md" title="wikilink">Ichun Pan</a> （中國）</p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td style="text-align: center;"><p>從缺</p></td>
<td style="text-align: center;"><p><a href="../Page/川久保賜紀.md" title="wikilink">川久保賜紀</a>（日本/美國）<br />
陳曦 （中國）</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td style="text-align: center;"><p><a href="../Page/神尾真由子.md" title="wikilink">神尾真由子</a>（日本）</p></td>
<td style="text-align: center;"><p><a href="../Page/Nikita_Boriso-Glebsky.md" title="wikilink">Nikita Boriso-Glebsky</a> （俄羅斯）</p></td>
<td style="text-align: center;"><p><a href="../Page/有希·曼紐拉·洋克.md" title="wikilink">有希·曼紐拉·洋克</a>（Yuki Manuela Janke）（德國）</p></td>
</tr>
<tr class="even">
<td><p>2011[2]</p></td>
<td style="text-align: center;"><p>從缺</p></td>
<td style="text-align: center;"><p><a href="../Page/Sergey_Dogadin.md" title="wikilink">Sergey Dogadin</a>（俄羅斯）<br />
<a href="../Page/:en:Itamar_Zorman.md" title="wikilink">Itamar Zorman</a>（以色列）</p></td>
<td style="text-align: center;"><p><a href="../Page/李珍惠.md" title="wikilink">李珍惠</a>（南韓）</p></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td style="text-align: center;"><p>從缺</p></td>
<td style="text-align: center;"><p><a href="../Page/曾宇謙.md" title="wikilink">曾宇謙</a>（台灣）</p></td>
<td style="text-align: center;"><p><a href="../Page/Alexandra_Conunova.md" title="wikilink">Alexandra Conunova</a>（摩尔多瓦）<br />
<a href="../Page/Haik_Kazazyan.md" title="wikilink">Haik Kazazyan</a>（俄羅斯）<br />
<a href="../Page/Pavel_Milyukov.md" title="wikilink">Pavel Milyukov</a> （俄羅斯）</p></td>
</tr>
<tr class="even">
<td></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 大提琴

  - 1962：[Natalia
    Shakhovskaya](../Page/:en:Natalia_Shakhovskaya.md "wikilink")
  - 1966：[Karine Georgian](../Page/:en:Karine_Georgian.md "wikilink")
  - 1970：蓋林格斯（[David
    Geringas](../Page/:en:David_Geringas.md "wikilink")）
  - 1974：[Boris
    Pergamenshchikov](../Page/:en:Boris_Pergamenshchikov.md "wikilink")
  - 1978：羅森（[Nathaniel
    Rosen](../Page/:en:Nathaniel_Rosen.md "wikilink")）
  - 1982：曼尼塞斯（[Antonio
    Meneses](../Page/:en:Antonio_Meneses.md "wikilink")）
  - 1986：布倫尼洛（[Mario
    Brunello](../Page/:en:Mario_Brunello.md "wikilink")）
  - 1990：[Gustav Rivinius](../Page/:en:Gustav_Rivinius.md "wikilink")
  - 1994：金、銀、銅牌均從缺，第四名由 [Eileen
    Moon](../Page/:en:Eileen_Moon.md "wikilink") 與 [Georgi
    Gorjunov](../Page/:en:Georgi_Gorjunov.md "wikilink") 同獲
  - 1998：[Denis Shapovalov](../Page/:en:Denis_Shapovalov.md "wikilink")
  - 2002：金牌從缺，銀牌由 [Johannes
    Moser](../Page/:en:Johannes_Moser.md "wikilink") 獲得
  - 2007：[Sergey Antonov](../Page/:en:Sergey_Antonov.md "wikilink")
  - 2011：[arek
    Hakhnazaryan](../Page/:en:Narek_Hakhnazaryan.md "wikilink")
  - 2015：Andrei Ionuț Ioniță（Romania）

### 聲樂－男

  - 1966：[Vladimir
    Atlantov](../Page/:en:Vladimir_Atlantov.md "wikilink")
  - 1970：涅斯捷連科（[Yevgeny
    Nesterenko](../Page/:en:Yevgeny_Nesterenko.md "wikilink")）
  - 1974：[Ivan Ponomarenko](../Page/:en:Ivan_Ponomarenko.md "wikilink")
  - 1978：金牌從缺，銀牌由 [Valentin
    Pivovarov](../Page/:en:Valentin_Pivovarov.md "wikilink") 與 [Nikita
    Storozhev](../Page/:en:Nikita_Storozhev.md "wikilink") 同獲
  - 1982：[Paata
    Burchchuladze](../Page/:en:Paata_Burchchuladze.md "wikilink")
  - 1986：[Grigory Gritsyuk](../Page/:en:Grigory_Gritsyuk.md "wikilink")
  - 1990：[Hans Choi](../Page/:en:Hans_Choi.md "wikilink")（韓國）
  - 1994：
  - 1998：[Besik
    Gabitashvili](../Page/:en:Besik_Gabitashvili.md "wikilink")
  - 2002：[Mikhail Kazakov](../Page/:en:Mikhail_Kazakov.md "wikilink")
  - 2007：Alexander Tzimbaluk
  - 2011：Jong Min Park
  - 2015：Ariunbaatar Ganbaatar（蒙古）

### 聲樂－女

  - 1966：[Jane Marsh](../Page/:en:Jane_Marsh.md "wikilink")
  - 1970：奧布拉茨索娃（[Yelena
    Obraztsova](../Page/:en:Yelena_Obraztsova.md "wikilink")）
  - 1974：金牌從缺，銀牌由 [Lyudmila
    Sergienko](../Page/:en:Lyudmila_Sergienko.md "wikilink")、薩斯（[Sylvia
    Sass](../Page/:en:Sylvia_Sass.md "wikilink")）與 [Stefka
    Evstatieva](../Page/:en:Stefka_Evstatieva.md "wikilink") 同獲
  - 1978：[Lyudmila
    Shemchuk](../Page/:en:Lyudmila_Shemchuk.md "wikilink")
  - 1982：[Lidiya
    Zabilyasta](../Page/:en:Lidiya_Zabilyasta.md "wikilink")
  - 1986：[Natalia Erasova](../Page/:en:Natalia_Erasova.md "wikilink")
  - 1990：佛格特（[Deborah Voigt](../Page/:en:Deborah_Voigt.md "wikilink")）
  - 1994：[Marina Lapina](../Page/:en:Marina_Lapina.md "wikilink")。首獎由
    [Hibra Gerzmawa](../Page/Hibra_Gerzmawa.md "wikilink") 獲得
  - 1998：
  - 2002：[Aitalina
    Afanasieva-Adamova](../Page/:en:Aitalina_Afanasieva-Adamova.md "wikilink")
  - 2007：[Albina
    Shagimuratova](../Page/:en:Albina_Shagimuratova.md "wikilink")
  - 2011：Sun Young Seo
  - 2015：Yulia Matochkina（Russia）

## 参考文献

## 外部链接

  - [Official website for the 2007
    competition](http://www.xiiitc.ru/start.asp?en)
  - [piano
    competitors 2007](http://www.xiiitc.ru/view.asp?competitors/piano)
  - [violin
    competitors 2007](http://www.xiiitc.ru/view.asp?competitors/violin)
  - [cello
    competitors 2007](http://www.xiiitc.ru/view.asp?competitors/cello)
  - [voice
    competitors 2007](http://www.xiiitc.ru/view.asp?competitors/solo)
  - [Directory of International Piano
    Competitions](http://masamizuno.com)
  - [HP](http://www.xiiitc.ru/view.asp?competitors/solo)
  - [Tchaikovsky
    competition 2011](http://www.tchaikovsky-competition.com/en/International)

## 参见

  - [古典音樂大賽列表](../Page/古典音樂大賽列表.md "wikilink")
  - [演奏家列表](../Page/演奏家列表.md "wikilink")

{{-}}

[彼得·伊里奇·柴可夫斯基](../Category/彼得·伊里奇·柴可夫斯基.md "wikilink")
[Category:钢琴比赛](../Category/钢琴比赛.md "wikilink")
[Category:小提琴比赛](../Category/小提琴比赛.md "wikilink")
[Category:大提琴比赛](../Category/大提琴比赛.md "wikilink")
[Category:音乐比赛](../Category/音乐比赛.md "wikilink")
[Category:歌唱比賽](../Category/歌唱比賽.md "wikilink")
[Category:俄羅斯音樂](../Category/俄羅斯音樂.md "wikilink")
[Category:蘇聯音樂](../Category/蘇聯音樂.md "wikilink")
[Category:1958年俄羅斯建立](../Category/1958年俄羅斯建立.md "wikilink")

1.  [1](http://www.pmf.or.jp/en/artist/2014/post-13.html) Pacific Music
    Festival（PMF） - Artist description section
2.  [Чао,
    Чайковский\!](http://www.rg.ru/2011/06/30/zavershenie-site.html)
    в *[Российской газете](http://www.rg.ru/)*