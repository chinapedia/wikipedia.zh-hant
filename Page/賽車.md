{{ infobox sport | name = 赛车 | image = Lap 1, Turn 1 Canada 2008.jpg |
imagesize = 275px | caption =
2008年[一级方程式](../Page/一级方程式.md "wikilink")（F1）比赛
| union = [FIA](../Page/国际汽车联盟.md "wikilink") | nickname = | first =
1887年4月28日 | registered = | clubs = | contact = | team = | mgender = 是 |
category = 室外 | ball = | olympic = }}
**賽車**（）是使用[汽車作](../Page/汽車.md "wikilink")[速度競賽的運動](../Page/速度.md "wikilink")。在1895年，这项运动第一次在[法国出现](../Page/法国.md "wikilink")。\[1\]如今，它已经成为了全世界吸引最多观众观看的[体育赛事之一](../Page/體育.md "wikilink")。\[2\]

而現時賽車也可以特指一些尃門制造或改裝供參加賽車的車輛，這類車不一定可以符合交通法規直接開上街上當普通汽車代步，而接近賽車的普通[量產型汽車稱為](../Page/量產.md "wikilink")[跑車](../Page/跑車.md "wikilink")。

## 历史

### 初始

赛车比赛早在第一台汽油驱动的汽车发明后不久便出现。据记载，1887年4月28日[巴黎](../Page/巴黎.md "wikilink")《自行车》（Le
Vélocipède）杂志主编Monsieur Fossier首次开办有组织的赛车竞赛。\[3\]赛程总长，起点为西北大桥（Neuilly
Bridge），终点为布洛涅森林（Bois de Boulogne）。这次比赛的冠军为来自公司的乔治斯·布通（Georges
Bouton），但由于他是唯一一位到场的参赛者，因此对于这是否算是比赛存在争议。\[4\]

1891年，又有一场单人赛事，来自[標緻汽車公司的](../Page/標緻汽車.md "wikilink")和路易斯·瑞格勒特（Louis
Rigoulot）驾驶着标致III型车参加了巴黎－布雷斯特自行车折返赛（Paris–Brest–Paris），等到他们到达[布雷斯特的时候](../Page/布雷斯特_\(法国\).md "wikilink")，冠军Charles
Terront已经返回到巴黎了。\[5\]\[6\]

#### 巴黎-鲁昂：世界第一场赛车比赛

[1894_paris-rouen_-_albert_lemaître_(peugeot_3hp)_1st.jpg](https://zh.wikipedia.org/wiki/File:1894_paris-rouen_-_albert_lemaître_\(peugeot_3hp\)_1st.jpg "fig:1894_paris-rouen_-_albert_lemaître_(peugeot_3hp)_1st.jpg")

1894年7月23日，巴黎刊物《[法国画报](../Page/法国画报.md "wikilink")》举办了从世界第一场赛车比赛，赛程从巴黎到鲁昂。\[7\]该报编辑皮埃尔·杰发尔特（Pierre
Giffard）用「无马车辆大赛」（Concours des Voitures sans
Chevaux）来宣传此比赛，称其「不危险，驾驶简易，旅程无需破费」。共有102名参赛者参与，报名费为10法郎。\[8\]

69辆赛车进入赛程为的资格赛，角逐进入正赛的名额，正赛长度为。参赛的既有大型汽车生产商[標緻汽車](../Page/標緻汽車.md "wikilink")、潘哈德和德·迪昂·布通，也有业余汽车所有者，最终只有25人进入了正赛。\[9\]

比赛从[马约门站出发并经过布洛涅森林](../Page/马约门站.md "wikilink")。最终，朱尔斯·阿尔伯特·德·迪昂是第一个到达鲁昂的，他用时6小时48分钟，平均时速为19km/h。比Albert
Lemaître（标致）时间为3分30秒，多里奥特（标致）快16分30秒，勒內·潘哈德（潘哈德）快33分30秒，Émile
Levassor（潘哈德）快55分30秒。但是官方的获胜者是标致和潘哈德，因为获胜与否取决于车辆的时速、操作和安全性能，德·迪昂的蒸汽汽车需要司炉，这是比赛不允许的。\[10\]

### 早期比赛

1895年6月的巴黎-波尔多折返赛曾被认为是世界上「首届汽车比赛」。\[11\]第一名为Émile
Levassor，他共用时48小时零47分钟完成了赛程（全长1,178km或732英里），比第二名快了将近6个小时。官方第一名为Paul
Koechlin，他驾驶的是标致的汽車。\[12\]22名參賽者中有9名完成了賽程\[13\]

第一届美国赛车比赛是在1895年11月28日[感恩节举行的芝加哥先驅報杯汽车赛](../Page/感恩节.md "wikilink")。\[14\]由于媒体的报道，使得不少美国人对这次比赛充满了兴趣。\[15\]比赛全长，路线是从城南出发，向北沿[埃文斯顿湖边行进](../Page/埃文斯顿_\(伊利诺伊州\).md "wikilink")，再折返。弗兰克·杜耶（Frank
Duryea）以总用时10小时23分钟完成了比赛，打败了其他5名参赛者。\[16\]

## 分類

  - 參見[賽車車輛和賽事分組](../Page/:EN:List_of_motorsport_championships.md "wikilink")

賽車的分類主要可以依使用的車輛及場地分類：[單座賽車是專門用在賽車跑道的賽車](../Page/單座賽車.md "wikilink")，低底盤，重心低，是完全考量賽車跑道上的性能，其速度也最快，但不考慮街道實用性。[跑車可在一般街道行駛](../Page/跑車.md "wikilink")，但仍以性能為優先，常見是兩個車門和兩個座位的汽車，或是有四個座位，但後方座位空間非常有限的汽車。房車則為一般量產的轎車。[卡丁車是類似單座賽車](../Page/卡丁車.md "wikilink")，但尺寸馬力數縮小許多的車種。

如賽事而論最著名的國際性巡迴比賽是以下四種，對賽車的規格皆影響了其他各種賽事。

  - [國際汽車聯合會世界一級方程式錦標賽](../Page/一級方程式.md "wikilink")
  - [國際汽車聯合會世界拉力錦標賽](../Page/世界拉力錦標賽.md "wikilink")
  - [國際汽車聯合會世界房車錦標賽](../Page/世界房車錦標賽.md "wikilink")
  - [國際汽車聯合會世界耐力錦標賽](../Page/世界耐力錦標賽.md "wikilink")

### 方程式賽車

[Formel3_racing_car_amk.jpg](https://zh.wikipedia.org/wiki/File:Formel3_racing_car_amk.jpg "fig:Formel3_racing_car_amk.jpg")\]\]
[一級方程式賽車](../Page/一級方程式賽車.md "wikilink")（F1）是最廣為人知的賽車活動，是由[國際汽車聯盟](../Page/國際汽車聯盟.md "wikilink")（FIA）每年舉辦，最高規格的賽車比賽，正式名稱為「國際汽車聯盟世界一級方程式錦標賽」。[方程式賽車](../Page/方程式賽車.md "wikilink")(不限於一級)更繼承了[格蘭披治大賽的名號](../Page/格蘭披治大賽.md "wikilink")，原型出於義大利在一戰前的[ITALA公司推出了一種時速近二百公里車的GRAND](../Page/:EN:Itala.md "wikilink")
PRIX(意為大獎賽事)，可其構造很特殊化和高粍油變成了專業化的[方程式賽車的始祖](../Page/方程式賽車.md "wikilink")，而[格蘭披治大賽則沿襲其名號](../Page/格蘭披治大賽.md "wikilink")，也可以說成為了最早專為比賽而開發的車。

方程式賽車對於賽車的限制較多，例如一度對於[渦輪增壓設備的限制等](../Page/渦輪增壓.md "wikilink")。也有其他比賽也是用單座賽車，但允許加裝渦輪增壓設備。

一級方程式賽車賽車使用單一座位的四輪汽車，為了公平性與安全性，賽車運動的主辦者會制訂賽車的統一「規格」（formula，或也可解釋為車輛設定的「公式」），只有依照規格製造的賽車才能參賽。故有方程式賽車的名稱\[17\]。

方程式賽車除了一級方程式賽車外，還包括以下的比賽：

  - [二級方程式賽車](../Page/二級方程式賽車.md "wikilink")（因花費接近F1比賽已取消）
    （2009年又開始新的賽季）
  - [三級方程式賽車](../Page/三級方程式賽車.md "wikilink")（簡稱F3，這項比賽的車手冠軍常能因此進入F1比賽，在歐洲比賽名稱已改為[GP3](../Page/GP3.md "wikilink")，F3車隊開銷比F1少許多，車隊彼此機械硬體上差異小，更能突顯車手本身的操控實力）
  - 美國[印地賽車](../Page/印地賽車.md "wikilink")（IndyCar）也是使用單座並開輪的賽車的比賽，每年的[印第安那波里斯500大赛是該比賽最著名的一站](../Page/印第安那波里斯500大赛.md "wikilink")，但不屬於FIA賽事和規格有別，為方便也不列入方程式賽事中。

### 原型賽車比賽

這類車本和方程式賽車有所重疊，即刻意被設計制造出來參賽的汽車，而且以前同樣是開放的單座賽車，和方程式車唯一分別是輪胎被沙板覆蓋。舊稱為**第六組賽車**，在八十年代改稱為[C組賽車](../Page/C組賽車.md "wikilink")，像早年的[勒芒24小时耐力赛是每年在法國名叫Le](../Page/勒芒24小时耐力赛.md "wikilink")
Mans的小鎮所舉辦的二十四小時耐久賽。

但九十年代起改稱為[勒芒原型車](../Page/勒芒原型車.md "wikilink")，需要有像轎車的車頂車門車窗等構造，並有撞風玻璃，原因是首先在勒芒24小时耐力赛使用和推廣。

原型賽車和方程式車同樣有分級別，但因為比賽時間很長所以其實參與者較少，故在同一場賽事中使用，並且在同場中也使[改裝車](../Page/改裝車.md "wikilink")，而除了全場總名次和賽項外，同時設有不同級別類型賽車的賽項和名次。

因為賽事對於賽車的考驗特別嚴酷，故成為了世界各大廠兵家必爭之地。後來各國的耐力賽車活動都統一並和其他賽事一樣有國際性巡迴比賽，稱為[世界耐力錦標賽](../Page/世界耐力錦標賽.md "wikilink")。\[18\]。

但有些耐力賽是房車賽或跑車賽，即只允使用普通[轎車做的](../Page/轎車.md "wikilink")[改裝車](../Page/改裝車.md "wikilink")，反之世界耐力錦標賽既有原型車又有改裝車，所以耐力賽不可等同原型賽車的賽事，如[斯帕24小時耐力賽和](../Page/斯帕24小時耐力賽.md "wikilink")[紐柏林賽道的](../Page/紐柏林賽道.md "wikilink")[:EN:24
Hours
Nürburgring](../Page/:EN:24_Hours_Nürburgring.md "wikilink")，雖然這兩場和勒芒同樣是全日賽制並有歐洲三大耐力賽之稱，但並不允許使用原型車，不屬於世界耐力錦標賽的分站賽事。

### 房車比賽

[2012_WTCC_Race_of_Japan_(Race_1)_opening_lap.jpg](https://zh.wikipedia.org/wiki/File:2012_WTCC_Race_of_Japan_\(Race_1\)_opening_lap.jpg "fig:2012_WTCC_Race_of_Japan_(Race_1)_opening_lap.jpg")\]\]

也是賽車活動的一種，但是比賽用車輛是用量產車輛所衍生而來。由於各車輛的速度差距不大，競爭也更加激烈。所用的車和跑車賽一樣是普通的汽車改裝，但對於改裝程度和基型車的限制較大。

主要的房車比賽包括[世界房車錦標賽](../Page/世界房車錦標賽.md "wikilink")（**W**orld **T**ouring
**C**ar
**C**hampionship，簡稱：WTCC）、[德國房車賽](../Page/德國房車賽.md "wikilink")（**D**eutsche
**T**ourenwagen **M**asters，簡稱：DTM）、日本房車錦標賽（日本選手權）JGTC 後改名稱為SUPER
GT、澳洲V8超級房車錦標賽（ [V8 Supercars](../Page/V8_Supercars.md "wikilink")
）等。

北美的房車比賽以的及GT锦标赛為主。不過1966年開始的Trans-Am Series目前仍是美國歷史最悠久的房車比賽。

### 跑車比賽

跑車比賽是用由[跑車衍生的車型](../Page/跑車.md "wikilink")（也稱為性能房車，grand
tourer，簡稱GT），在封閉跑道的比賽，其實和房車賽有重疊，但對於車的規格較寬鬆，對於車的改裝程度和被改裝的基型車限制較小。更有一部分可以接受特製的。主要的國際比賽是，其他比賽有、。

有部分耐力賽是把普通跑車或轎車改裝的賽車與原型賽車一同參賽，如上文的勒芒24小時耐力賽，但同樣24小時耐力賽的紐柏林和斯帕耐力賽則屬純粹的跑車賽。

### 拉力賽

[Cuoq_and_Latvala_-_2008_Monte_Carlo_Rally_2.jpg](https://zh.wikipedia.org/wiki/File:Cuoq_and_Latvala_-_2008_Monte_Carlo_Rally_2.jpg "fig:Cuoq_and_Latvala_-_2008_Monte_Carlo_Rally_2.jpg")\]\]
[拉力赛](../Page/拉力赛.md "wikilink")，是采用公共或者私人[道路](../Page/道路.md "wikilink")，使用改装过的或者是特别制造的[汽车进行的](../Page/汽车.md "wikilink")[比赛](../Page/比赛.md "wikilink")。和其他比赛不同之处在于，拉力赛不是在[环形赛道上](../Page/环形赛道.md "wikilink")，而是在点到点之间的[赛道上比赛](../Page/赛道.md "wikilink")。

拉力赛分為[赛段拉力赛和](../Page/赛段拉力赛.md "wikilink")[道路拉力赛](../Page/道路拉力赛.md "wikilink")。1960年代起，赛段拉力赛成为这种运动的专业形式。这种比赛在接近和其他道路交汇处的直道延伸速度来决定胜负。比赛用路面非常多样，从砂石山路到林间路，从冰雪路到沙漠道路，这些道路类型对参赛队员提供了很大的挑战，并且也是对参赛车辆性能和可靠性的考验。

著名的拉力赛有[世界拉力錦標賽](../Page/世界拉力錦標賽.md "wikilink")（World Rally
Championship,WRC）及[達卡拉力賽等](../Page/達卡拉力賽.md "wikilink")。

### 汽車特技

比起像[賽馬易欣賞和參與的競速比賽](../Page/賽馬.md "wikilink")，較小眾化的是就像[馬術或](../Page/馬術.md "wikilink")[馬戲般](../Page/馬戲.md "wikilink")，以汽車或電單車來表演超高難度的[特技動作](../Page/特技.md "wikilink")，展示車手的驚人技術和車輛優雅的姿勢與驚險的表演等。因為不是以衝線的方式分勝負，而是對於指定的動作和自選動作的完成程度分勝負。而大車廠常設有自己的汽車特技表演車隊。

### 其他

[NASCAR_practice.jpg](https://zh.wikipedia.org/wiki/File:NASCAR_practice.jpg "fig:NASCAR_practice.jpg")\]\]
[TruckRacing.jpg](https://zh.wikipedia.org/wiki/File:TruckRacing.jpg "fig:TruckRacing.jpg")\]\]

  - 太陽能賽車
  - [全國運動汽車競賽協會](../Page/全國運動汽車競賽協會.md "wikilink")（**N**ational
    **A**ssociation for **S**tock **C**ar **A**uto
    **R**acing，簡稱：NASCAR）舉辦的相關比賽。
  - [卡車賽](../Page/卡車賽.md "wikilink")
  - [小型賽車](../Page/小型賽車.md "wikilink")（卡丁車）
  - [直线竞速赛](../Page/直线竞速赛.md "wikilink")

## 旗语

在许多赛车运动当中，尤其是在封闭赛道上进行的比赛，往往会使用旗帜来表示赛道基本状况或者同参赛者交流指示信息。尽管不同的赛事可能有不同的规则，而且现在的旗语含义已经同早年不同（例如，红旗用来表示比赛开始），但目前已经形成广泛接受的旗语。

<table>
<thead>
<tr class="header">
<th><p>旗帜</p></th>
<th><p>出现在发令塔上</p></th>
<th><p>出现在观察站上</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auto_Racing_Green.svg" title="fig:Auto_Racing_Green.svg">Auto_Racing_Green.svg</a></p></td>
<td><p>比赛在全赛道警示或中断之后开始或恢复。</p></td>
<td><p>赛道上的危险解除。</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auto_Racing_Yellow.svg" title="fig:Auto_Racing_Yellow.svg">Auto_Racing_Yellow.svg</a></p></td>
<td><p>全赛道警示。在路面赛段中，它表示该赛道的警示。根据比赛种类不同，可能会使用两面黄旗表示全赛道警示或者“SC”标志要求赛车跟随<a href="../Page/安全车.md" title="wikilink">安全车前行</a>，不许超车。</p></td>
<td><p>赛道警示状况——在出示黄旗的弯道处不允许超车。静止时表示威胁不在赛道上，摇动时则表示威胁在赛道上。</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auto_Racing_Oil.svg" title="fig:Auto_Racing_Oil.svg">Auto_Racing_Oil.svg</a></p></td>
<td><center>
<p>在赛道表面有碎片、液体或者其他威胁。</p>
</center></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auto_Racing_Black.svg" title="fig:Auto_Racing_Black.svg">Auto_Racing_Black.svg</a></p></td>
<td><p>指定编号的赛车必须进站接受询问。</p></td>
<td><p>比赛暂停，所有在赛道上的车辆都要进入维修站。有时可能同绿旗配合使用，表明赛道上有油。</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:F1_black_flag_with_orange_circle.svg" title="fig:F1_black_flag_with_orange_circle.svg">F1_black_flag_with_orange_circle.svg</a></p></td>
<td><p>指定编号的赛车有机械故障，必须进站检修。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auto_Racing_Black_White.svg" title="fig:Auto_Racing_Black_White.svg">Auto_Racing_Black_White.svg</a></p></td>
<td><p>指定编号赛车的车手因为违反规则被处罚。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auto_Racing_White_Cross.svg" title="fig:Auto_Racing_White_Cross.svg">Auto_Racing_White_Cross.svg</a></p></td>
<td><p>指定编号赛车的车手已被取消参赛资格，或者在未向维修站汇报前不会记录成绩。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auto_Racing_Blue.svg" title="fig:Auto_Racing_Blue.svg">Auto_Racing_Blue.svg</a></p></td>
<td><p>车辆要给快车让行，根据比赛不同，这可能是指令也可能仅仅是建议。</p></td>
<td><p>某辆赛车被建议给逼近的快车让行。</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:F1_red_flag.svg" title="fig:F1_red_flag.svg">F1_red_flag.svg</a></p></td>
<td><p>比赛暂停。所有车辆都必须停在赛道上或者返回维修站。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auto_Racing_White.svg" title="fig:Auto_Racing_White.svg">Auto_Racing_White.svg</a></p></td>
<td><p>根据比赛不同，可能意味着还剩一圈或者赛道上有慢车。</p></td>
<td><p>赛道上有慢车。</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:F1_chequered_flag.svg" title="fig:F1_chequered_flag.svg">F1_chequered_flag.svg</a></p></td>
<td><p>比赛已结束。</p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 相關條目

  - [賽車場](../Page/賽車場.md "wikilink")
  - [賽車遊戲](../Page/賽車遊戲.md "wikilink")

[賽車](../Category/賽車.md "wikilink")
[Category:车辆](../Category/车辆.md "wikilink")

1.

2.

3.

4.
5.  [Peugeot Fan Club. History. 1890 - 1895 From Steam to
    Petrol](http://peugeot.mainspot.net/hist03.shtml)

6.  [UCAPUSA. Peugeot
    Heritage](http://www.ucapusa.com/heritage_peugeot.htm)

7.
8.
9.
10.
11.

12.

13.
14.
15. Michael L. Berger. *The automobile in American history and culture:
    a reference guide*,
    [pg. 278](http://books.google.com/books?id=oRwMv8iNP-MC&pg=PA278)

16. [Profile of Frank
    Duryea](http://www.historicracing.com/driversSearch.cfm?keyword=duryea&search_type=A&search_full=N&x=0&y=0),
    Historic Racing

17.

18.