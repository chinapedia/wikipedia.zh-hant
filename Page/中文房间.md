**中文房间**（），是由[约翰·希尔勒提出的一个](../Page/约翰·希尔勒.md "wikilink")[思想實驗](../Page/思想實驗.md "wikilink")，借以反驳[强人工智能的观点](../Page/强人工智能.md "wikilink")。根据[强人工智能的观点](../Page/人工智能.md "wikilink")，只要[计算机拥有了适当的程序](../Page/计算机.md "wikilink")，理论上就可以说计算机拥有它的认知状态並且可以像人一样地进行理解活动。

該實驗出自[约翰·罗杰斯·希尔勒的論文](../Page/约翰·罗杰斯·希尔勒.md "wikilink")《心靈、大腦和程序》（*Minds,
Brains, and Programs*）中，發表於1980年的《》。\[1\]

## 實驗概要

中文房间的實驗过程可表述如下：

[约翰·希尔勒认为](../Page/约翰·希尔勒.md "wikilink")，尽管房里的人可以以假乱真，让房外的人以为他确确实实说汉语，他却压根不懂汉语。在上述过程中，房外人的角色相当于[程序员](../Page/程序员.md "wikilink")，房中人相当于[计算机](../Page/计算机.md "wikilink")，而手册则相当于[计算机程序](../Page/计算机程序.md "wikilink")：每当房外人给出一个输入，房内的人便依照手册给出一个答复（输出）。而正如房中人不可能通过手册理解中文一样，计算机也不可能通过程序来获得理解力。既然计算机没有理解能力，所谓“计算机于是便有智能”便更无从谈起了。

## 反面观点

對此，有相反觀點被提出，其內容大致如下：

### 反面观点的論據

所有人都认为人是拥有[智能的](../Page/智能.md "wikilink")，而人的智能决策来自于脑细胞的电信号转换，每一个脑细胞并不理解单词的意义，只是简单的缓冲、传递或抑制一个电信号，脑细胞创造了[语法](../Page/语法.md "wikilink")[规则](../Page/规则.md "wikilink")，创造了[决策](../Page/决策.md "wikilink")[策略](../Page/策略.md "wikilink")（相当于规则书与不懂中文的人），但是它们并不懂每个单词的意义。而人类却显示出与人沟通的能力。如果按照希尔勒的观点，那麼人类也不存在[认知能力](../Page/认知.md "wikilink")，但这与事实是不符的。所以依然可以认为若某段计算机程序，能够完成[图灵测试](../Page/图灵测试.md "wikilink")，则说明该段计算机程序具有认知能力。

### 反面观点的批評

然而，此一觀點也被提出存在兩項根源性謬誤，以至甚至被認為錯誤理解「中文房間」概念。其一為此論過於倚賴「智能决策来自于脑细胞的电信号转换」此一前題，並將人類作出智能决策時，涉及脑细胞电信号转换的現象，錯誤地演繹為「智能决策『唯獨』由脑细胞的电信号转换『所產生』」。此演繹不但從未被證明過，也無足夠證據支持。其次，此說只能推導出「單獨一个脑细胞的缓冲、传递或抑制一个电信号，不能使其理解单词的意义」而已，至於脑细胞（集體）如何创造[语法](../Page/语法.md "wikilink")[规则](../Page/规则.md "wikilink")、[决策](../Page/决策.md "wikilink")[策略](../Page/策略.md "wikilink")，是否單純倚靠個別脑细胞的缓冲、传递或抑制电信号等等，並未作出任何合理推論；同時也忽略了「單獨一个脑细胞」與一個擁有智能的人類之間，後者包涵前者、前者與眾多他者組成後者等複雜關係，便直接將「單獨一个脑细胞不理解单词的意义而人類擁有智能」此一現實，用作否定「機器不能透過程序獲得理解能力」與及「智能直接關係於理解能力」的根據，其邏輯難以明白，推演也過於草率。

## 參考文獻

## 参见

  - [人工智能](../Page/人工智能.md "wikilink")
  - [圖靈測試](../Page/圖靈測試.md "wikilink")
  - [他心問題](../Page/他心問題.md "wikilink")
  - [哲學殭屍](../Page/哲學殭屍.md "wikilink")

## 延伸阅读

  -
  - [The Chinese Room
    Argument](http://globetrotter.berkeley.edu/people/Searle/searle-con4.html),
    part 4 of the September 2, 1999 interview with Searle [Philosophy
    and the Habits of Critical
    Thinking](http://globetrotter.berkeley.edu/people/Searle/searle-con0.html)
    in the [Conversations With
    History](../Page/Conversations_With_History.md "wikilink") series

  - [Understanding the Chinese
    Room](http://www.zompist.com/searle.html), [Mark
    Rosenfelder](../Page/Mark_Rosenfelder.md "wikilink")

  - [A Refutation of John Searle's "Chinese Room
    Argument"](http://www.anti-state.com/article.php?article_id=247), by
    [Bob Murphy](../Page/Robert_P._Murphy.md "wikilink")

  - , [PDF at author's
    homepage](http://www.cs.bc.edu/~kugel/Publications/Searle%206.pdf),
    critical paper based on the assumption that the CR cannot use its
    inputs (which are in Chinese) to change its program (which is in
    English).

  -
  - John Preston and Mark Bishop, "Views into the Chinese Room", Oxford
    University Press, 2002. Includes chapters by [John
    Searle](../Page/John_Searle.md "wikilink"), [Roger
    Penrose](../Page/Roger_Penrose.md "wikilink"), [Stevan
    Harnad](../Page/Stevan_Harnad.md "wikilink") and [Kevin
    Warwick](../Page/Kevin_Warwick.md "wikilink").

  - [Margaret Boden](../Page/Margaret_Boden.md "wikilink"), "Escaping
    from the Chinese room", Cognitive Science Research Papers No. CSRP
    092, University of Sussex, School of Cognitive Sciences, 1987, ,
    [online PDF](http://doi.library.cmu.edu/10.1184/OCLC/19297071), "an
    excerpt from a chapter" in the then unpublished "Computer Models of
    Mind: : Computational Approaches in Theoretical Psychology",
    (1988); reprinted in Boden (ed.) "The Philosophy of Artificial
    Intelligence"  (1989) and  (1990); Boden "Artificial Intelligence in
    Psychology: Interdisciplinary Essays" , MIT Press, 1989, chapter 6;
    reprinted in Heil, pp. 253–266 (1988) (possibly abridged); J. Heil
    (ed.) "Philosophy of Mind: A Guide and Anthology", Oxford University
    Press, 2004, pages 253–266 (same version as in "Artificial
    Intelligence in Psychology")

  - [John R. Searle](../Page/John_R._Searle.md "wikilink"), “What Your
    Computer Can’t Know” (review of [Luciano
    Floridi](../Page/Luciano_Floridi.md "wikilink"), *The Fourth
    Revolution: How the Infosphere Is Reshaping Human Reality*, Oxford
    University Press, 2014; and [Nick
    Bostrom](../Page/Nick_Bostrom.md "wikilink"), *Superintelligence:
    Paths, Dangers, Strategies*, Oxford University Press, 2014), *[The
    New York Review of
    Books](../Page/The_New_York_Review_of_Books.md "wikilink")*, vol.
    LXI, no. 15 (October 9, 2014), pp. 52–55.

{{-}}

[C](../Category/人工智能.md "wikilink") [C](../Category/思想实验.md "wikilink")
[C](../Category/心灵哲学.md "wikilink")

1.