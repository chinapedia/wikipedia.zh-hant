****（**），是一家生產發售[電子遊戲的公司](../Page/電子遊戲.md "wikilink")。旗下有[Leaf品牌](../Page/Leaf.md "wikilink")，專責[十八禁遊戲的開發販售](../Page/十八禁遊戲.md "wikilink")；一般向遊戲則使用**AQUAPLUS**品牌行銷。除本業外，還跨足經營[音樂](../Page/音樂.md "wikilink")、[汽車](../Page/汽車.md "wikilink")、[飲食](../Page/飲食.md "wikilink")（僅為[加盟](../Page/加盟.md "wikilink")）等相關行業。2013年9月20日，成為梦之空（）的全資子公司。

## 事業概況

  - 遊戲軟體開發、販售
      - [Leaf](../Page/Leaf.md "wikilink")・AQUAPLUS
  - CD製作販售、藝人經紀
      - 音樂事業部、關係企業：（位在本社大樓內）
  - 音樂工作室
      - STUDIO AQUA（位在本社七樓）
  - 汽車販售
      - 由[下川直哉的父親下川隆治於](../Page/下川直哉.md "wikilink")[兵庫縣所創立的汽車店](../Page/兵庫縣.md "wikilink")「AQUA」。社名也是從此名而來的。
  - 餐飲業
      - 在兵庫縣經營加盟的「」。

## 作品列表

<span style="font-size:smaller;">這裡僅列出以**AQUAPLUS**名義所發行的一般向遊戲。關於以Leaf名義所發行的十八禁遊戲，請參閱[Leaf](../Page/Leaf.md "wikilink")。</span>

  - 1999年3月25日－[To
    Heart](../Page/To_Heart.md "wikilink")（[Playstation版](../Page/Playstation.md "wikilink")）
      - 2003年6月27日－To Heart
        PSE（[Windows版](../Page/Microsoft_Windows.md "wikilink")）
  - 2001年8月9日－[漫畫派對](../Page/漫画派对_\(游戏\).md "wikilink")（[Dreamcast版](../Page/Dreamcast.md "wikilink")）
      - 2003年5月30日－漫畫派對 DCE（Windows）
      - 2005年12月29日－漫畫派對 PORTABLE（[PSP版](../Page/PSP.md "wikilink")）
  - 2003年1月30日－[Tenerezza](../Page/Tenerezza.md "wikilink")（[XBOX版](../Page/XBOX.md "wikilink")）
      - 2003年3月28日－Tenerezza（Windows版）
  - 2004年12月28日－[To Heart
    2](../Page/To_Heart_2.md "wikilink")（[PS2版](../Page/PlayStation_2.md "wikilink")）
      - 2009年7月30日－[To Heart 2
        PORTABLE](../Page/To_Heart_2.md "wikilink")（PSP版）
      - 2011年9月22日 - ToHeart2 DX PLUS（PS3版）
  - 2005年11月25日－（Windows版）
  - 2006年10月26日－[傳頌之物 給逝者的搖籃曲](../Page/傳頌之物.md "wikilink")（PS2版）
      - 2009年5月28日－[傳頌之物PORTABLE](../Page/傳頌之物.md "wikilink")（PSP版）
  - 2007年1月25日－Routes PE（プレイステーション2）、Routes PORTABLE（PSP版）
  - 2007年3月23日 - 傳頌之物 デスクトップキャラクターズ（Windows版）
  - 2008年7月17日－[提亞拉之淚
    花冠的大地](../Page/提亞拉之淚.md "wikilink")（[PS3版](../Page/PS3.md "wikilink")）
      - 2010年11月25日 - 提亞拉之淚 花冠の大地 PORTABLE（PSP版）
  - 2009年9月17日－花冠之淚外傳（PS3版）
      - 2010年12月16日 - 花冠之淚外傳 PORTABLE（PSP版）
  - 2010年6月24日－（PS3版）
      - 2012年3月30日 - WHITE ALBUM -綴られる冬の想い出-（Windows版）
  - 2011年6月22日－[AQUAPAZZA](../Page/AQUAPAZZA.md "wikilink")（[AC版](../Page/街機.md "wikilink")）
      - 2012年8月30日 - AQUAPAZZA（PS3版）
  - 2011年6月30日 - ToHeart2 ダンジョントラベラーズ（PSP版）
      - 2015年4月30日 - ToHeart2
        ダンジョントラベラーズ（[PSVita版](../Page/PSVita.md "wikilink")）
  - 2012年10月25日 - ハートフルシミュレーター PACHISLOT ToHeart2（PS3版）
  - 2012年12月20日 - WHITE ALBUM2 幸せの向こう側（PS3版）
      - 2013年11月28日 - WHITE ALBUM2 幸せの向こう側（PSVita版）
  - 2013年3月28日 - ダンジョントラベラーズ2 王立図書館とマモノの封印（PSP版）
      - 2014年9月25日 - ダンジョントラベラーズ2 王立図書館とマモノの封印（PSVita版）
  - 2013年10月31日 - ティアーズ・トゥ・ティアラII 覇王の末裔（PS3版）
  - 2014年12月24日\[1\] - WHITE ALBUM2 ミニアフターストーリー（Windows版）
  - 2015年9月24日 - [傳頌之物
    虛偽的假面](../Page/傳頌之物_虛偽的假面.md "wikilink")（[PS4版](../Page/PS4.md "wikilink")、PS3版、PSVita版）
  - 2016年9月21日 - [傳頌之物
    二人的白皇](../Page/傳頌之物_二人的白皇.md "wikilink")（PS3版、PS4版、PSVita版）
  - 2017年4月20日 - ダンジョントラベラーズ2-2 闇堕ちの乙女とはじまりの書（PSVita版）
  - 2018年4月26日 - 傳頌之物 給逝者的搖籃曲（PS4版、PSVita版）

## 注釋

## 外部連結

  - [官方網站](http://www.aquaplus.co.jp)

[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")
[Category:1994年成立的公司](../Category/1994年成立的公司.md "wikilink")
[Category:1994年日本建立](../Category/1994年日本建立.md "wikilink")

1.