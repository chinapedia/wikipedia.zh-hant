**黑船来航**（，又稱**黑船事件**）是指[日本](../Page/日本.md "wikilink")[嘉永六年](../Page/嘉永.md "wikilink")（1853年）[美國海軍](../Page/美國海軍.md "wikilink")[准将](../Page/准将.md "wikilink")[馬休·佩里率](../Page/馬休·佩里.md "wikilink")[艦隊駛入](../Page/艦隊.md "wikilink")[江戶湾](../Page/東京灣.md "wikilink")[浦賀海面的事件](../Page/浦賀.md "wikilink")，佩里帶著[美國總統](../Page/美國總統.md "wikilink")[米勒德·菲爾莫爾的](../Page/米勒德·菲爾莫爾.md "wikilink")[國書向](../Page/國書.md "wikilink")[江戶幕府致意](../Page/江戶幕府.md "wikilink")，最後雙方于次年（1854年）簽定《[神奈川條約](../Page/神奈川條約.md "wikilink")》（《日美和亲条约》）。

此事件被視為日本歷史中[幕末時期的濫觴](../Page/幕末.md "wikilink")。

## 背景

[Japanese_1854_print_Commodore_Perry.jpg](https://zh.wikipedia.org/wiki/File:Japanese_1854_print_Commodore_Perry.jpg "fig:Japanese_1854_print_Commodore_Perry.jpg")
1846年，美國海軍准將[詹姆斯·贝特尔率領三艘美國軍艦來日要求開國](../Page/詹姆斯·贝特尔.md "wikilink")，但被[江戶幕府拒絕](../Page/江戶幕府.md "wikilink")。之後的1852年，長崎荷蘭商館館長庫修斯聽說美國艦隊即將來日本的消息，便把此事告知幕府，並勸說簽訂日荷通商條約當作對策，江戶幕府仍不為所動\[1\]。

## 經過

佩里率領的四艘舰船——、[薩斯喀那號巡洋艦](../Page/薩斯喀那號_\(1850\).md "wikilink")、和——一共载炮六十三门，1852年11月24日由美國東岸[諾福克出發](../Page/諾福克_\(維吉尼亞州\).md "wikilink")，橫渡大西洋，繞過非洲好望角北上進入印度洋，1853年5月4日到上海，5月26日到[琉球国](../Page/琉球国.md "wikilink")，7月8日（嘉永6年6月3日）到[浦賀](../Page/浦賀.md "wikilink")。這些船隻由於船體被塗上有防止生鏽的黑色[柏油](../Page/柏油.md "wikilink")，而被[日本人稱為](../Page/日本人.md "wikilink")「[黑船](../Page/黑船.md "wikilink")」。佩里要求将[美国总统的國書递交日本官方](../Page/美国总统.md "wikilink")。黑船的出現，为日本带来莫大的骚动。當晚，[江戶城一片混亂](../Page/江戶城.md "wikilink")。

當時日本民間流傳著一首，對此事件有描述：
「」是一種[日本茶](../Page/茶.md "wikilink")，與「蒸汽船」諧音；整句的意思是說：「上喜撰（蒸汽船）唤醒太平梦，喝上四杯便再難眠。」德川幕府不敢拒絕美國的要求，此時[幕府將軍](../Page/幕府將軍.md "wikilink")[德川家慶病死](../Page/德川家慶.md "wikilink")，幕府[老中](../Page/老中.md "wikilink")[阿部正弘藉口要得到](../Page/阿部正弘.md "wikilink")[孝明天皇的批准方可接受條約](../Page/孝明天皇.md "wikilink")。佩里因還有他事，没有轻易动武；不過，佩里声言明年再来，並且率舰绕航江户湾，方才扬长而去。艦隊於7月17日（嘉永6年6月12日）離開江戶，返回英國殖民地香港。

1854年2月13日（嘉永7年1月16日），佩里率领九艘[军舰駛入江户灣](../Page/军舰.md "wikilink")；這一次[幕府諸臣无计可施](../Page/幕府.md "wikilink")，被迫与美国缔结《[神奈川條約](../Page/神奈川條約.md "wikilink")》，打开国门。佩里艦隊於1854年6月25日（嘉永7年6月1日）離開[下田](../Page/下田.md "wikilink")。

## 影響

  - 江户幕府解除禁止诸藩建造大船的禁令，[中岛三郎助的](../Page/中岛三郎助.md "wikilink")《[嘉永上书](../Page/嘉永上书.md "wikilink")》提到：“为了防卫江户湾……应当添置军舰三十艘，其中三分之二分配给负责守卫江户的诸藩，其余的三分之一则配属浦贺奉行所……。”两个月后，[凤凰丸竣工](../Page/凤凰丸.md "wikilink")。

## 黑船照片

[File:Kurofune.jpg|萨斯奎哈纳号](File:Kurofune.jpg%7C萨斯奎哈纳号) <File:Kurofune>
2.jpg|萨拉托加号

## 參考文獻與資料

1.  《日本遠征記》土屋喬雄、玉城肇譯　[岩波文庫](../Page/岩波文庫.md "wikilink")（原著為*Narrative of
    the Expedition of an American Squadron to the China Seas and
    Japan.*　1856年　美利堅合衆国第33議會第2會期中特殊刊行物第97）
2.  [豬口孝](../Page/豬口孝.md "wikilink") 《」》 NTT出版 1999年 ISBN 475714010X
3.  小島敦夫 《》 [講談社](../Page/講談社.md "wikilink")（講談社現代新書） 2005年 ISBN
    4061498223
4.  Cullen, L.M. (2003). *A History of Japan, 1582-1941: Internal and
    External Worlds.* Cambridge:
    [劍橋大學出版社](../Page/劍橋大學出版社.md "wikilink").
    ISBN 0-521-82115-X (cloth) ISBN 0-521-529918-2 (paper)
5.  Hawks, Francis. (1856). *Narrative of the Expedition of an American
    Squadron to the China Seas and Japan Performed in the Years 1852,
    1853 and 1854 under the Command of Commodore M.C. Perry, United
    States Navy,* Washington: A.O.P. Nicholson by order of Congress,
    1856; originally published in *Senate Executive Documents*, No. 34
    of 33rd Congress, 2nd Session. \[reprinted by London: Trafalgar
    Square, 2005. ISBN 1-8458-8026-9 (paper)\]
6.  Sewall, John S. (1905). *The Logbook of the Captain's Clerk:
    Adventures in the China Seas,* Bangor, Maine: Chas H. Glass & Co.
    \[reprint by Chicago: R.R. Donnelly & Sons, 1995\] ISBN
    0-5482-0912-X

## 注解

## 相關條目

  - [日美關係](../Page/日美關係.md "wikilink")
  - [神奈川條約](../Page/神奈川條約.md "wikilink")
  - [幕末](../Page/幕末.md "wikilink")
  - [黑船](../Page/黑船.md "wikilink")
  - [第一次鴉片戰爭](../Page/第一次鴉片戰爭.md "wikilink")
  - [第二次鴉片戰爭](../Page/第二次鴉片戰爭.md "wikilink")
  - [明治維新](../Page/明治維新.md "wikilink")

[Category:幕末外交事件](../Category/幕末外交事件.md "wikilink")
[Category:19世纪美国外交事件](../Category/19世纪美国外交事件.md "wikilink")
[Category:19世纪美国军事事件](../Category/19世纪美国军事事件.md "wikilink")
[Category:19世纪日本军事事件](../Category/19世纪日本军事事件.md "wikilink")
[Category:1850年代日本](../Category/1850年代日本.md "wikilink")
[Category:1850年代美国](../Category/1850年代美国.md "wikilink")
[Category:1853年](../Category/1853年.md "wikilink")
[Category:日美關係](../Category/日美關係.md "wikilink")
[Category:美国海军事件](../Category/美国海军事件.md "wikilink")
[Category:海軍外交](../Category/海軍外交.md "wikilink")

1.