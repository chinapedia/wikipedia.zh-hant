{{ infobox writer | image = | imagesize = | alt = | caption = |
pseudonym = | birthname = | birthdate =  | birthplace =
[英格蘭](../Page/英格蘭.md "wikilink")
[西约克郡](../Page/西约克郡.md "wikilink")
[莫利](../Page/莫利_\(西約克郡\).md "wikilink") | deathdate = |
deathplace = | occupation =
[小說作家](../Page/小說家.md "wikilink")、[電影編劇](../Page/電影編劇.md "wikilink")
| language = 英語 | nationality =  | ethnicity = | citizenship = |
education = | alma_mater =
[牛津大学](../Page/牛津大学.md "wikilink")[牛津大學聖安妮學院](../Page/牛津大學聖安妮學院.md "wikilink")
| period = | genre =
[愛情小說](../Page/愛情小說.md "wikilink")/[雞仔文學](../Page/雞仔文學.md "wikilink")
| subject = | movement = | notableworks =
《[布里吉特·琼斯的日记](../Page/布里吉特·琼斯的日记.md "wikilink")》
| spouse = | partner = Kevin Curran（約1999年－2009年） | children = 一子一女 |
relatives = | influences = | influenced = | awards = | signature = |
signature_alt = | website = | portaldisp = }}
**海伦·菲尔丁**（，），[英国女](../Page/英国.md "wikilink")[作家](../Page/作家.md "wikilink")，代表作《[布里吉特·琼斯的日记](../Page/布里吉特·琼斯的日记.md "wikilink")》，现在给多家英国报纸撰稿。2003年被《[觀察家報](../Page/觀察家報.md "wikilink")》选为50个最有趣的英国[喜剧作家之一](../Page/喜剧.md "wikilink")。

出生于英國[约克郡](../Page/约克郡.md "wikilink")，毕业于[牛津大学](../Page/牛津大学.md "wikilink")[牛津大學聖安妮學院](../Page/牛津大學聖安妮學院.md "wikilink")，主修英语文学，后来在[伦敦生活](../Page/伦敦.md "wikilink")。她在[BBC工作多年](../Page/BBC.md "wikilink")，曾製作过一些非洲题材的[记录片](../Page/记录片.md "wikilink")。

《[布里吉特·琼斯的日记](../Page/布里吉特·琼斯的日记.md "wikilink")》最初只是在《[独立报](../Page/独立报.md "wikilink")》及《[每日电讯报](../Page/每日电讯报.md "wikilink")》连载出版的专栏，但在1996年結集成書後，先於1998年获[英國圖書大獎](../Page/英國圖書大獎.md "wikilink")[年度圖書獎](../Page/英國圖書大獎年度圖書獎.md "wikilink")（British
Book of the Year, British Book
Awards），然后更於2001年被改编成[同名电影](../Page/BJ单身日记_\(电影\).md "wikilink")。此書的續集《[B.J.的單身日記2─都是男人惹的禍](../Page/B.J.的單身日記2─都是男人惹的禍.md "wikilink")》亦曾被原班人馬改編成[電影](../Page/BJ單身日記：愛你不愛你.md "wikilink")。2013年秋，第三本[布里吉特·琼斯小說](../Page/布里吉特·琼斯.md "wikilink")*Mad
About the Boy*出版。

約1999年開始和電視[編劇及](../Page/編劇.md "wikilink")[監製](../Page/監製.md "wikilink")[Kevin
Curran在一起](../Page/:en:Kevin_Curran_\(writer\).md "wikilink")，兩人育有一子一女，卻已在2009年分手。\[1\]

## 作品

  - 《[名流風暴](../Page/名流風暴.md "wikilink")》（*Cause Celeb* 1994）：諷刺小說
  - 《[布里吉特·琼斯的日记](../Page/布里吉特·琼斯的日记.md "wikilink")》（*Bridget Jones's
    Diary*，1996）
  - 《[B.J.的單身日記2─都是男人惹的禍](../Page/B.J.的單身日記2─都是男人惹的禍.md "wikilink")》（*Bridget
    Jones: The Edge of Reason*，1999）

<!-- end list -->

  - 《[OJ愛情大冒險](../Page/OJ愛情大冒險.md "wikilink")》（*Olivia Joules and The
    Overactive Imagination*，2003），搞笑間諜小說，背景為邁阿密、洛杉磯、英格蘭及蘇丹
  - *Bridget Jones: Mad About the Boy* (2013)

<!-- end list -->

  - 短篇

<!-- end list -->

  - *Ox-tales* (2009) a collection of short stories in aid of Oxfam\[2\]

## 參考

## 外部連結

  - 《[衛報](../Page/衛報.md "wikilink")》上有關菲爾丁的[文章及特寫](http://books.guardian.co.uk/authors/author/0,,-203,00.html)

  -
[category:英格蘭女性作家](../Page/category:英格蘭女性作家.md "wikilink")

[Category:牛津大學聖安妮學院校友](../Category/牛津大學聖安妮學院校友.md "wikilink")

1.
2.  [Ox-tales](http://www.oxfam.org.uk/shop/content/books/books_oxtales.html)
     on the [Oxfam](../Page/Oxfam.md "wikilink") website, retrieved
    December 2009