[Cristales_cueva_de_Naica.JPG](https://zh.wikipedia.org/wiki/File:Cristales_cueva_de_Naica.JPG "fig:Cristales_cueva_de_Naica.JPG")內的石膏水晶。從圖中的探測者，可以估計水晶柱尺寸大小。\]\]
**石膏**是一种[礦物名](../Page/礦物.md "wikilink")，主要[化学成分是](../Page/化学.md "wikilink")[硫酸钙](../Page/硫酸钙.md "wikilink")（），主要是古代[盐湖或](../Page/盐湖.md "wikilink")[潟湖的沉积物](../Page/潟湖.md "wikilink")。

石膏用作一种农业[肥料](../Page/肥料.md "wikilink")，可以改良碱性土壤\[1\]，用于一般中性或酸性土壤，可以改善土壤结构，供给钙和硫成分\[2\]。

石膏广泛用于[工业](../Page/工业.md "wikilink")[材料](../Page/材料.md "wikilink")、醫學材料和[建筑材料](../Page/建筑材料.md "wikilink")\[3\]：其可用于[水泥缓凝剂](../Page/水泥.md "wikilink")、石膏建筑制品、[模型制作](../Page/模型.md "wikilink")、医用食品添加剂、[硫酸生产](../Page/硫酸.md "wikilink")、纸张填料、油漆填料、[骨折固定等](../Page/骨折.md "wikilink")，也能做為[黑板用的](../Page/黑板.md "wikilink")[粉筆](../Page/粉筆.md "wikilink")。

天然二水石膏（CaSO<sub>4</sub>·2H<sub>2</sub>O）又称为「生石膏」，经过煅烧、磨细可得β型半水石膏（CaSO<sub>4</sub>·1/2H<sub>2</sub>O），即「[建筑石膏](../Page/灰泥.md "wikilink")」，又称熟石膏、灰泥。若煅烧温度为190 °C可得「[模型石膏](../Page/模型石膏.md "wikilink")」，其细度和白度均比建筑石膏高。若将生石膏在400～500 °C或高于800 °C下煅烧，即得「[地板石膏](../Page/地板石膏.md "wikilink")」，其凝结、硬化较慢，但硬化后强度、耐磨性和耐水性均较普通建筑石膏为好。

工業級和食品級的石膏僅有製造過程上嚴謹度的差別，因此造成其純度的不同。

## 中藥

又稱作**白虎**、**冰石**、**細理石**。

產於[中國大陸](../Page/中國大陸.md "wikilink")[新疆](../Page/新疆.md "wikilink")、[山東](../Page/山東.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[廣東](../Page/廣東.md "wikilink")、[雲南](../Page/雲南.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[四川](../Page/四川.md "wikilink")、[河南](../Page/河南.md "wikilink")、[甘肅](../Page/甘肅.md "wikilink")。

中药采用纤维状生石膏生用或[炮製後入药](../Page/炮製.md "wikilink")\[4\]。[性味归经](../Page/性味.md "wikilink")：辛、甘，大寒归肺、胃经\[5\]。功效：清热泻火，除烦止渴，收敛生肌\[6\]。

## 用途

  - 一种农业[肥料](../Page/肥料.md "wikilink")，可以改良[碱性土壤](../Page/碱性.md "wikilink")，用于一般中性或酸性土壤，可以改善[土壤结构](../Page/土壤.md "wikilink")，供给[钙和](../Page/钙.md "wikilink")[硫成分](../Page/硫.md "wikilink")。
  - 工业材料，可用于[模型制作](../Page/模型.md "wikilink")、[硫酸生产](../Page/硫酸.md "wikilink")、[纸张填料](../Page/纸.md "wikilink")、[油漆](../Page/油漆.md "wikilink")、黑板用的[粉筆](../Page/粉筆.md "wikilink")。
  - 醫學材料，可用于医用食品添加剂、骨折固定等。
  - 建筑材料，可用于[水泥缓凝剂](../Page/水泥.md "wikilink")、石膏建筑制品。
  - [食品添加剂](../Page/食品添加剂.md "wikilink")，製作[豆花](../Page/豆花.md "wikilink")、[豆腐的過程中的添加物](../Page/豆腐.md "wikilink")。
  - 繪畫所用的[石膏底料也有很大一部分成分是石膏](../Page/石膏底料.md "wikilink")。

## 參考文獻

## 外部連結

  - [石膏
    Shigao](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00412)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [石膏 Shi
    Gao](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00452)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [石膏 - WebMineral](http://webmineral.com/data/Gypsum.shtml)
  - [石膏 - Mindat](http://www.mindat.org/min-1784.html)

[Category:含钙矿物](../Category/含钙矿物.md "wikilink")
[Category:硫酸鹽礦物](../Category/硫酸鹽礦物.md "wikilink")
[Category:矿物药](../Category/矿物药.md "wikilink")
[Category:建筑材料](../Category/建筑材料.md "wikilink")
[Category:化學肥料](../Category/化學肥料.md "wikilink")

1.

2.

3.

4.
5.

6.