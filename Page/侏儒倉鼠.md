**侏儒倉鼠**，學名*Phodopus*，又稱**毛足鼠**，是[哺乳綱](../Page/哺乳綱.md "wikilink")[囓齒目](../Page/囓齒目.md "wikilink")[倉鼠科的一屬](../Page/倉鼠科.md "wikilink")，同科的動物尚有[大倉鼠屬](../Page/大倉鼠屬.md "wikilink")（大倉鼠）、中倉鼠屬（羅馬尼亞中倉鼠）、原倉鼠屬（原倉鼠）等之數種[哺乳動物](../Page/哺乳動物.md "wikilink")。

侏儒倉鼠屬有三種小型[倉鼠物種](../Page/倉鼠.md "wikilink")，這三種倉鼠腳背與腳底都布滿毛，其他倉鼠則只有腳背有毛，因為這個特點所以侏儒倉鼠屬又被稱為毛足屬。

## 物種

  - [坎貝爾侏儒倉鼠](../Page/坎貝爾侏儒倉鼠.md "wikilink")(*Phodopus campbelli*)
  - [沙漠侏儒倉鼠或](../Page/沙漠侏儒倉鼠.md "wikilink")[羅伯羅夫倉鼠](../Page/羅伯羅夫倉鼠.md "wikilink")(*Phodopus
    roborovski*)
  - [短尾侏儒倉鼠](../Page/短尾侏儒倉鼠.md "wikilink")(*Phodopus sungorus*)

## 參考資料

  - Nowak, Ronald M. 1999. *Walker's Mammals of the World*, 6th edition.
    Johns Hopkins University Press, 1936 pp. ISBN 0-801-85789-9

[Category:侏儒倉鼠屬](../Category/侏儒倉鼠屬.md "wikilink")