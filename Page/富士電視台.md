**株式會社富士電視**（，英語譯名：），一般稱為「**富士電視台**」（），簡稱「**CX**」（源自其[識別呼號JO](../Page/無線電台呼號.md "wikilink")**CX**-DTV），為[日本一間以](../Page/日本.md "wikilink")[關東地方為主要播放區域的](../Page/關東地方.md "wikilink")[電視台](../Page/電視台.md "wikilink")，為日本五大[電視聯播網的](../Page/電視聯播網.md "wikilink")[核心台之一](../Page/核心局.md "wikilink")。[G-Code指引台號為](../Page/易錄系統.md "wikilink")0264\[1\]。1959年3月1日以[東京第](../Page/東京.md "wikilink")4號電視台為呼號開播。富士電視台是目前[東亞最具影響力的](../Page/東亞.md "wikilink")[商業電視媒體之一](../Page/商業媒體.md "wikilink")，不少著名的[日劇及](../Page/日本電視劇.md "wikilink")[綜藝節目皆是富士電視台的出品](../Page/綜藝節目.md "wikilink")。2004年至2011年，富士電視台曾經連續八年實現日本電視[收視率的](../Page/收視率.md "wikilink")「三冠王」（[全日](../Page/全日.md "wikilink")、[晚間時段](../Page/晚間時段.md "wikilink")、[黃金時段](../Page/黃金時段.md "wikilink")）。此外，富士電視台還是日本第一家播出[電視動畫的電視台](../Page/日本動畫.md "wikilink")\[2\]。

## 公司概要

[Fuji_Telecasting_1982_title.jpg](https://zh.wikipedia.org/wiki/File:Fuji_Telecasting_1982_title.jpg "fig:Fuji_Telecasting_1982_title.jpg")
[Fuji_Television_1961.jpg](https://zh.wikipedia.org/wiki/File:Fuji_Television_1961.jpg "fig:Fuji_Television_1961.jpg")[河田町的富士電視台舊總部](../Page/河田町.md "wikilink")，今已被拆除。\]\]
[Former_Fuji_Television_Headquarters.JPG](https://zh.wikipedia.org/wiki/File:Former_Fuji_Television_Headquarters.JPG "fig:Former_Fuji_Television_Headquarters.JPG")
[Fujitv_kawadacho_kanban.jpg](https://zh.wikipedia.org/wiki/File:Fujitv_kawadacho_kanban.jpg "fig:Fujitv_kawadacho_kanban.jpg")
[FujiTVStudioOdaiba.jpg](https://zh.wikipedia.org/wiki/File:FujiTVStudioOdaiba.jpg "fig:FujiTVStudioOdaiba.jpg")的[富士電視台本社大樓](../Page/富士電視台本社大樓.md "wikilink")，由名建築師[丹下健三設計](../Page/丹下健三.md "wikilink")，為東京的著名地標之一。\]\]
[Wangan_Studio.jpg](https://zh.wikipedia.org/wiki/File:Wangan_Studio.jpg "fig:Wangan_Studio.jpg")的[富士電視台灣岸攝影棚](../Page/富士電視台灣岸攝影棚.md "wikilink")\]\]
[Fuji_Television_Outside_broadcasting_van.jpg](https://zh.wikipedia.org/wiki/File:Fuji_Television_Outside_broadcasting_van.jpg "fig:Fuji_Television_Outside_broadcasting_van.jpg")前\]\]
[FujiTV,crew2007-3-1.jpg](https://zh.wikipedia.org/wiki/File:FujiTV,crew2007-3-1.jpg "fig:FujiTV,crew2007-3-1.jpg")
[めざましテレビ関連施設-地図-広域.jpg](https://zh.wikipedia.org/wiki/File:めざましテレビ関連施設-地図-広域.jpg "fig:めざましテレビ関連施設-地図-広域.jpg")
[めざましテレビ関連施設-地図-詳細.jpg](https://zh.wikipedia.org/wiki/File:めざましテレビ関連施設-地図-詳細.jpg "fig:めざましテレビ関連施設-地図-詳細.jpg")

  - 電視台擁有兩個主要[聯播網](../Page/電視聯播網.md "wikilink")：分布於日本各地的28家電視台，提供節目的[富士電視網](../Page/富士電視網.md "wikilink")（Fuji
    Network System），以及提供新聞取材的[富士新聞網](../Page/富士新聞網.md "wikilink")（Fuji
    News Network）。
  - 除了陸上的無線電視廣播之外，富士電視台還透過[衛星電視業者](../Page/衛星電視.md "wikilink")「[Sky
    PerfecTV\!](../Page/Sky_PerfecTV!.md "wikilink")」（東經124、128度[CS廣播](../Page/通信衛星.md "wikilink")），與經廣播事業者「[Satellite
    Service](../Page/Satellite_Service.md "wikilink")」（[:ja:サテライト・サービス](../Page/:ja:サテライト・サービス.md "wikilink")）委託的SKY
    PerfecTV\!
    e2（，東經110度CS廣播），播放[富士電視ONE](../Page/:ja:フジテレビONE.md "wikilink")（原「富士電視739」（<font lang="ja">フジテレビ739</font>））、[富士電視TWO](../Page/:ja:フジテレビTWO.md "wikilink")（原「富士電視721」（<font lang="ja">フジテレビ721</font>））與[富士電視NEXT](../Page/富士電視NEXT.md "wikilink")（<font lang="ja">フジテレビNEXT</font>）等三個頻道。除此之外，也有提供一部份的節目供地方性[有線電視頻道播放](../Page/有線電視.md "wikilink")。

## 沿革

### 大事紀

  - 1957年6月 -
    以[日本放送及](../Page/日本放送.md "wikilink")[文化放送兩家](../Page/文化放送_\(日本\).md "wikilink")[廣播電台為首](../Page/廣播電台.md "wikilink")，加上[東寶](../Page/東寶株式會社.md "wikilink")、[松竹和](../Page/松竹.md "wikilink")[大映等](../Page/角川映畫.md "wikilink")[電影公司](../Page/電影.md "wikilink")，共同提出申請成立無線電視台。
  - 1957年11月18日 - 成立**株式會社富士電視**（）。之前曾暫定命名為「中央電視台」（）。
  - 1958年12月 - 名稱中「**富士**」二字改以[片假名表示](../Page/片假名.md "wikilink")，即改為「」。
  - 1959年1月9日 - 獲[郵政省批准廣播](../Page/郵政省.md "wikilink")。
  - 1959年2月8日 - 開始以地面類比訊號試播。
  - 1959年3月1日 - 正式以地面類比訊號開播。
  - 1959年6月 -
    以富士電視台為首，[東京](../Page/東京.md "wikilink")、[名古屋](../Page/名古屋.md "wikilink")、[大阪及](../Page/大阪.md "wikilink")[福岡四家區域電視台簽訂網絡協助體制](../Page/福岡.md "wikilink")，確立了[富士電視網](../Page/富士電視網.md "wikilink")（FNS）的基礎。
  - 1964年9月3日 -
    隨著[東京奧運的開始](../Page/1964年夏季奧林匹克運動會.md "wikilink")，開始試播[彩色電視節目](../Page/彩色電視.md "wikilink")，為東京第三家開始播出彩色電視節目的電視台。
  - 1964年9月7日 -
    開播彩色電視節目，第一個彩色節目為[英國](../Page/英國.md "wikilink")[科幻](../Page/科幻.md "wikilink")[人偶](../Page/人偶.md "wikilink")[影集](../Page/影集.md "wikilink")《[霹靂艇](../Page/霹靂艇.md "wikilink")》（Stingray）。
  - 1966年1月 -
    入標競投香港第一個免費電視牌照，但最後由邵氏、利氏家族為首的香港電視集團（今[無綫電視](../Page/無綫電視.md "wikilink")）中標。
  - 1966年10月3日 - 正式成立FNN。
  - 1969年10月1日 - 正式成立FNS。
  - 1969年10月5日 - 長壽節目《[海螺小姐](../Page/海螺小姐.md "wikilink")》啟播。
  - 1971年 -
    裁撤製作部，將製作部獨立為[子公司](../Page/子公司.md "wikilink")，並委託製作除新聞報導、體育及現場直播之外的節目。
  - 1978年10月2日 - 開始試播多聲道節目。
  - 1985年12月8日 - 開始多重文字播放服務。
  - 1986年4月1日 -
    導入富士產經集團的統一企業識別，即更改為現在使用的「眼珠標誌」及徽號。同時廢止使用[龟仓雄策設計的](../Page/龟仓雄策.md "wikilink")「8標誌」（）。
  - 1987年10月 - 首次實施24小時不收播。
  - 1988年 - 為紀念開台30週年而把[東方快車引入日本行走](../Page/東方快車.md "wikilink")。
  - 1989年8月 - 開始播放第一代清晰畫面訊號。
  - 1990年1月 - 長壽節目《[櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")》啟播。
  - 1990年11月 - 開始試播高畫質訊號。
  - 1993年6月30日 -
    華人傳奇樂隊[Beyond主唱](../Page/Beyond.md "wikilink")[黃家駒在富士電視台意外跌死](../Page/黃家駒.md "wikilink")。
  - 1994年11月 - 取得高畫質訊號實用化實驗台許可。
  - 1995年4月 - 合併「株式会社富士產經集團本社」（）。
  - 1995年9月 - 開始播放寬屏幕高畫質訊號（[EDTV](../Page/EDTV.md "wikilink")-Ⅱ）。
  - 1997年3月 -
    位於[東京都](../Page/東京都.md "wikilink")[台場的新企業總部](../Page/台場.md "wikilink")[富士電視台本社大樓落成啟用](../Page/富士電視台本社大樓.md "wikilink")。
  - 1997年3月10日 -
    正式由[新宿區河田町遷往新總部大樓](../Page/新宿區.md "wikilink")，並更改營業登記地址為新總部大樓地址，同時開始拆除河田町舊總部。
  - 1997年8月 - 正式在[東京證券交易所上市](../Page/東京證券交易所.md "wikilink")。
  - 1998年4月 -
    依據日本《放送法》（即廣播法），核可成為兩[有線](../Page/有線電視.md "wikilink")[數位電視頻道的委託播送業者](../Page/數位電視.md "wikilink")。
  - 1998年4月 - 有線數位電視頻道「富士電視721」開播。
  - 1999年4月 - 有線數位電視頻道「富士電視739」開播。
  - 1999年8月2日 -
    一名在人事部工作的職員因持有[興奮劑而被捕](../Page/興奮劑.md "wikilink")，翌日隨即被解雇。
  - 2000年12月1日 -
    上午11時，同系列的衛星數位頻道「[BS富士](../Page/BS富士.md "wikilink")」（BS數位電視第8頻道）開播。
  - 2003年12月1日 - 上午11時，數位電視訊號正式開播。
  - 2005年1月17日 -
    收購同屬富士產經集團的[日本放送](../Page/日本放送.md "wikilink")，並取得36.47%的股權。
  - 2005年5月23日 -
    購併[活力門](../Page/活力門.md "wikilink")（Livedoor）旗下的「活力門夥伴」（Livedoor
    Partners）公司，該公司改名為「LF控股」（LF Holdings）。
  - 2005年9月1日 - 與[日本放送交換股權](../Page/日本放送.md "wikilink")，並將其子公司化。
  - 2006年3月15日 - [Podcasting服務](../Page/Podcasting.md "wikilink")「Fuji
    Pod」開始提供。
  - 2006年3月16日 -
    在活力門社長[堀江貴文因](../Page/堀江貴文.md "wikilink")[活力門事件而被搜索住處之際](../Page/活力門事件.md "wikilink")，富士電視台擔心權益受損，宣佈出脫所持有的活力門股票、解除合作夥伴關係，並決定向活力門求償345萬日幣。
  - 2006年4月1日 -
    [1seg正式開播](../Page/1seg.md "wikilink")。同日，包含部分[主播在內的](../Page/主播.md "wikilink")47名日本放送員工移職至富士電視台。
  - 2006年4月3日 -
    所屬的富士產經集團進行內部整合，富士電視台因應日前[日本放送的子公司化](../Page/日本放送.md "wikilink")，將原屬[日本放送子集團內的關係企業全部納入旗下](../Page/日本放送.md "wikilink")。
  - 2007年3月26日 -
    富士電視台正式向[東京地方裁判所提起對活力門的求償訴訟](../Page/東京地方裁判所.md "wikilink")。
  - 2007年3月29日 -
    取得[波麗佳音及](../Page/波麗佳音.md "wikilink")[扶桑社在市場上流通的所有股份](../Page/扶桑社.md "wikilink")，完成子公司化，並同時提高在BS富士的出資比率。
  - 2007年9月1日 -
    開始進行專供[高畫質電視播出的報導採訪](../Page/高畫質電視.md "wikilink")（體育新聞的部份報導已先進行高畫質化）。
  - 2007年9月14日 -
    位於東京都[江東區青海](../Page/江東區_\(東京都\).md "wikilink")、專供錄製[數位電視節目之用的](../Page/數位電視.md "wikilink")「灣岸攝影棚」（）啟用。
  - 2008年4月1日 - CS數位電視頻道「富士電視CSHD」開播。
  - 2008年10月1日 -
    富士電視台進行業務分割，原富士電視株式會社更名為「[富士媒體控股株式會社](../Page/富士媒體控股.md "wikilink")」（），成為日本第一家實行「認定放送持株會社」（廣播控股公司）制度的媒體企業，各項廣播業務則改交由新成立之子公司「富士電視株式會社」繼承。
  - 2011年7月24日 - 中午，結束[模擬廣播](../Page/模擬廣播.md "wikilink")。
  - 2012年8月19日 -
    富士電視台與台灣[靖天傳播合作](../Page/靖天傳播.md "wikilink")，富士電視台外銷節目給靖天傳播，靖天傳播規劃「Fuji
    Time」播出富士電視台節目，這是富士電視台首度外銷[時代劇](../Page/時代劇.md "wikilink")。\[3\]
  - 2013年6月1日 -
    富士電視台進行人事變動，原本由豐田皓擔任的富士電視台社長職務，改由原富士電視台電影製作局的製作人[龜山千廣接任](../Page/龜山千廣.md "wikilink")。龜山千廣上任後，表明會以「續集」的形式拯救持續低迷的電視劇收視率，綜藝方面也會加大改編力度，集中於週五、六、日的低收視率綜藝。
  - 2013年10月22日 - 播放長達32年的健力士記錄綜藝節目《[森田一義時間
    笑一笑又何妨！](../Page/森田一義時間_笑一笑又何妨！.md "wikilink")》宣布將於2014年3月31日停播。
  - 2014年3月1日 -
    為紀念富士電視台開台55周年，招牌綜藝節目《[帥呆了！](../Page/帥呆了！.md "wikilink")》播出長達4小時的特別節目，節目邀請到搞笑藝人界「」：塔摩利、北野武、明石家秋刀魚，以及小堺一機、山村紅葉、菊川怜等知名藝人。

### 重大事件

富士電視台為[台灣電視公司](../Page/台灣電視公司.md "wikilink")（台視）的創始股東之一。2005年12月24日，富士電視台再聯同台視、[中國時報集團及其他香港及海外投資者一同在](../Page/中國時報集團.md "wikilink")[香港組成財團](../Page/香港.md "wikilink")，收購原由[中國國民黨間接擁有的](../Page/中國國民黨.md "wikilink")[中國電視公司](../Page/中國電視公司.md "wikilink")、[中國廣播公司及](../Page/中國廣播公司.md "wikilink")[中央電影公司](../Page/中央電影公司.md "wikilink")（中視、中廣、中影，合稱「三中」），富士電視台因而取得「三中」的部分權益。但在2006年12月，由於富士電視台悉數出售其持有的台視股份予台灣的[非凡電視台](../Page/非凡電視台.md "wikilink")，根據入股「三中」時的交易協議，富士電視台將不能再持有「三中」的任何股權，而台視亦必須放棄其持有「三中」股權裡的四分之一（換言之，當台視其餘三個日資股東都退股台視，「三中」就會隨之由其香港大股東全資擁有），兩者逐立即將各自的「三中」股權售予「三中」的香港大股東。此外，富士電視台曾於1960年代參與競逐[香港](../Page/香港.md "wikilink")[地面電視牌照](../Page/地面電視.md "wikilink")，但是落敗。

2005年3月，日本三大[入口網站之一的](../Page/入口網站.md "wikilink")[livedoor](../Page/livedoor.md "wikilink")（[活力門](../Page/活力門.md "wikilink")）宣佈掌握了富士電視台母公司[日本放送四成的股份](../Page/日本放送.md "wikilink")，企圖購併富士電視台，引發富士電視台「公司派」大股東與livedoor的股權爭奪戰；最後在同年5月，兩方達成互相結盟合作的協議。

2011年8月7日、8月21日、9月17日、10月15日，[日本右翼团体聯合發動遊行向富士電視台抗議](../Page/日本右翼团体.md "wikilink")，指控富士電視台「[偏頗報導](../Page/偏頗報導.md "wikilink")」、「偏袒[韓流](../Page/韓流.md "wikilink")」、「[反日](../Page/反日.md "wikilink")」、「辱日」。

## 節目

### 新聞、情報節目

#### 新聞

  - [晨間新聞](../Page/晨間新聞.md "wikilink")：《[鬧鐘電視](../Page/鬧鐘電視.md "wikilink")》（）
    - 星期一至星期五 05:25 - 08:00 播出
      - 周六：《[週六鬧鐘新聞](../Page/週六鬧鐘新聞.md "wikilink")》（）（該節目與[鬧鐘電視由情報制作中心制作](../Page/鬧鐘電視.md "wikilink")。下面的節目則是由報道局制作）-
        星期六 06:00 - 08:30 播出
      - 周日：《[產經電視新聞FNN](../Page/產經電視新聞FNN.md "wikilink")》（） - 星期日 06:00
        - 06:15 播出

<!-- end list -->

  - 午間新聞：《[FNN Speak](../Page/FNN_Speak.md "wikilink")》（）- 星期一至五 11:30 -
    11:55 播出
      - 周日：《[產經電視新聞FNN](../Page/產經電視新聞FNN.md "wikilink")》（）

<!-- end list -->

  - 富士電視台[晚間聯播新聞](../Page/晚間新聞.md "wikilink")（）
      - 《[FNN大家的新聞](../Page/FNN大家的新聞.md "wikilink")》（） - 星期一至星期五 16:50 -
        19:00 播出
      - 《[FNN周末大家的新聞](../Page/FNN周末大家的新聞.md "wikilink")》（） -
        [周六及](../Page/周六.md "wikilink")[周日](../Page/周日.md "wikilink")
        17:30 - 18:00 播出
  - [夜間新聞](../Page/晚間新聞.md "wikilink")：《[您的時間](../Page/您的時間.md "wikilink")》（）
    - 星期一至四 23:30 - 00:25 ; 星期五 23:58 - 00:55 播出
  - 《[今夜的新聞](../Page/今夜的新聞.md "wikilink")》（） - 星期一至五 20:54 - 21:00 播出

#### 情報、政論、紀實

  - 《[獨家新聞](../Page/獨家新聞.md "wikilink")》（　每星期一至星期五 8:00-9:50 播出
  - 《[NON-STOP\!](../Page/NON-STOP!.md "wikilink")》（　每星期一至星期五 9:50－11:25
    播出
  - 《新‧週刊 富士電視台的批評》 - 每星期六 05:00 - 06:00 播出
  - 《[皇室一家](../Page/皇室一家.md "wikilink")》（）每星期日 05:45 - 06:00 播出
  - 《ボクらの時代》 每星期日 07:00 - 07:30 播出
  - 《[新報道2001](../Page/新報道2001.md "wikilink")》（；） - 每星期日 07:30 - 08:55
    播出
  - 《[The Non-fiction](../Page/:jp:ザ・ノンフィクション.md "wikilink")》 每星期日
    14:00-15:00 播出
  - 《[Mr.星期日](../Page/:ja:Mr.サンデー.md "wikilink")》每星期日 22:00-23:15 播出

### 體育節目

  - [每個人的競馬](../Page/每個人的競馬.md "wikilink") 每星期日15:00 -16:00 播出
  - [世界花樣滑冰錦標賽](../Page/世界フィギュアスケート選手権.md "wikilink") 每年3月末
  - [東京馬拉松](../Page/東京マラソン.md "wikilink")
  - [女子足球阿爾加夫盃](../Page/アルガルベカップ.md "wikilink")
  - [BASEBALL
    SPECIAL～棒球道～](../Page/:ja:BASEBALL_SPECIAL～棒球道～.md "wikilink")（[職業棒球](../Page/日本職業棒球.md "wikilink")）
  - 春季高中排球大會（）
  - [一級方程式賽車](../Page/一級方程式賽車.md "wikilink") -
    富士電視台自1987年起直播該賽事，它於1987年至2009年冠名贊助[日本大獎賽](../Page/日本大獎賽.md "wikilink")。

#### 體育新聞

  - 《[SPORT](../Page/SPORT.md "wikilink")》（　每星期一至星期日　※高畫質播出）

#### 單發特別節目

  - 《池上彰緊急スペシャル！》
  - 《世界法廷ミステリー》
  - 《あなたの知るかもしれない世界》
  - 《禁断の事実！知らなきゃよかったNEWS》
  - 《草なぎ剛の女子アナスペシャル》

#### 播放結束節目

  - 《[職業棒球新聞](../Page/職業棒球新聞.md "wikilink")》（）
  - 《[戎克體育](../Page/戎克體育.md "wikilink")》（）

### 綜藝節目（2014年1月）

  - 《[森田一義時間 笑一笑又何妨！](../Page/森田一義時間_笑一笑又何妨！.md "wikilink")》（每星期一至星期五
    12:00－13:00　※高畫質播出）
      - 《[笑一笑又何妨！增刊號](../Page/:ja:笑っていいとも!増刊号.md "wikilink")》（每星期日
        10:00-11:45　※高畫質播出）
  - 《[LION祝您一切順利](../Page/:ja:ライオンのごきげんよう.md "wikilink")》（每星期一至星期五
    13:00－13:30　※高畫質播出）

<!-- end list -->

  - 《[NEP LEAGUE](../Page/:ja:ネプリーグ.md "wikilink")》（每星期一
    19:00－20:00　※高畫質播出）
  - 《[衝擊！三世代天堂](../Page/:ja:ジェネレーション天国.md "wikilink")》 （每星期一 20:00 -
    20:54 ※高畫質播出）
  - 《[SMAP×SMAP](../Page/SMAP×SMAP.md "wikilink")》（每星期一
    22:00－22:54　※高畫質播出
    ※[關西電視台共同製作](../Page/關西電視台.md "wikilink")）
  - 《[Terrace House 排屋公寓](../Page/:jp:テラスハウス.md "wikilink")》 （每星期一 23:00
    - 23:30 ※高畫質播出）

<!-- end list -->

  - 《[星期二特番](../Page/:ja:カスペ!.md "wikilink")》（每星期二 19:00－20:54　※高畫質播出）
  - 《[有吉弘行的都市傳說\!?](../Page/:ja:有吉弘行のダレトク!?.md "wikilink")》（每星期二
    23:00－23:30　※高畫質播出 ※[關西電視台共同製作](../Page/關西電視台.md "wikilink")）

<!-- end list -->

  - 《[Ojamap](../Page/:ja:おじゃマップ.md "wikilink")》 （每星期三 19:00 - 19:57
    ※高畫質播出）
  - 《[逛世界看真相！？](../Page/:ja:世界行ってみたらホントはこんなトコだった！？.md "wikilink")》（每星期三
    19:57-20:54　※高畫質播出）
  - 《[真的嗎！？TV](../Page/:ja:ホンマでっか！？TV.md "wikilink")》（每星期三
    21:00-21:54　※高畫質播出）
  - 《[TOKIO X](../Page/:ja:TOKIOカケル.md "wikilink")》 （每星期三 23:00 - 23:30
    ※高畫質播出）

<!-- end list -->

  - 《[VS嵐](../Page/VS嵐.md "wikilink")》（每星期四 19:00－19:57　※高畫質播出）
  - 《[奇蹟體驗\!難以相信的故事](../Page/:ja:奇跡体験!アンビリバボー.md "wikilink")》（每星期四
    19:57－20:54　※高畫質播出）
  - 《[TUNNELS的託大家的福](../Page/:ja:とんねるずのみなさんのおかげでした.md "wikilink")》（每星期四
    21:00-21:54　※高畫質播出）
  - 《[OUT x DELUXE](../Page/:ja:アウト×デラックス.md "wikilink")》（4月4日（木）起 每星期四
    23:00-23:30　※高畫質播出）

<!-- end list -->

  - 《[PEKE×PON](../Page/:ja:ペケ×ポン.md "wikilink")》（每星期五
    19:00－19:57　※高畫質播出）
  - 《[說麼生\!說破\!](../Page/:ja:クイズ・ソモサン⇔セッパ!.md "wikilink")》 （每星期五 23:00 -
    23:30 ※高畫質播出）
  - 《[Our Music 我們的音樂](../Page/:jp:僕らの音楽.md "wikilink")》 （每星期五 23:30 -
    23:58 ※高畫質播出）

<!-- end list -->

  - 《[にじいろジーン](../Page/:jp:にじいろジーン.md "wikilink")》（每星期六 08:30-09:55
    ※高畫質播出）
  - 《[有吉的誠實散步](../Page/:jp:ぶらぶらサタデー.md "wikilink")》（每星期六 12:00-13:30
    ※高畫質播出）
  - 《[秋刀魚的部屋](../Page/:jp:さんまのまんま.md "wikilink")》（每星期六 17:00-17:30
    ※高畫質播出 ※[關西電視台製作](../Page/關西電視台.md "wikilink")）
  - 《[MUSIC FAIR](../Page/:jp:MUSIC_FAIR.md "wikilink")》（每星期六
    18:00-18:30 ※高畫質播出）
  - 《[旅遊巴士](../Page/:jp:もしもツアーズ.md "wikilink")》（每星期六 18:30-19:00 ※高畫質播出）
  - 《[潛入\!我們不知道的實際範圍](../Page/:ja:超潜入！リアルスコープハイパー.md "wikilink")》（每星期六
    19:00－19:57　※高畫質播出）
  - 《[最高倍笑劇](../Page/:ja:めちゃ2イケてるッ！.md "wikilink")》（每星期六
    19:57－20:54　※高畫質播出）

<!-- end list -->

  - 《[清晨起來...](../Page/:ja:はやく起きた朝は….md "wikilink")》（每星期日 06:30-07:00
    ※高畫質播出）
  - 《[來我家吧\!?](../Page/:ja:ウチくる!?.md "wikilink")》（每星期日
    12:00-13:00　※高畫質播出）
  - 《[日本語探究綜藝 Quiz\! 真的嗎\!?
    日本](../Page/:ja:日本語探Ｑバラエティ_クイズ！それマジ！？ニッポン.md "wikilink")》（每星期日
    19:00-19:58　※高畫質播出）
  - 《[學到的教訓](../Page/:ja:教訓のススメ.md "wikilink")》（每星期日 19:58－20:54　※高畫質播出）
  - 《[全力教室](../Page/:ja:全力教室.md "wikilink")》（每星期日 21:00－21:54　※高畫質播出）
  - 《[新堂本兄弟](../Page/:ja:新堂本兄弟.md "wikilink")》（每星期日 23:15－23:45 ※高畫質播出）

#### 單發特別節目

  - 《爆笑全明星模仿紅白歌會SPECIAL》（）
  - 《滑稽演員歌很好寶座決定戰SPECIAL》（）
  - 《[逃走中](../Page/逃走中.md "wikilink")》
  - 《さんま＆くりぃむの芸能界（秘）個人情報グランプリ》
  - 《[人志松本不不及格的話](../Page/人志松本不不及格的話.md "wikilink")》（）
  - 《[平成教育委員會](../Page/平成教育委員會.md "wikilink")》
  - 《[爆笑紅地毯](../Page/爆笑紅地毯.md "wikilink")》（）
  - 《[初詣\!爆笑HIT PARADE](../Page/初詣!爆笑HIT_PARADE.md "wikilink")》（）
  - 《[FNS日](../Page/FNS日.md "wikilink")》（。毎年7月）

FNN Vector 1953年 - 1966年

FNNフジニュースネットワーク 1967年 - 1969年

FNSフジネットワーク 1970年 - 1986年

FNSスーパースペシャルテレビ夢列島 1987年 - 1991年

FNS平成教育テレビ 1992年 - 1994年

FNS夢列島 1995年 - 1996年

FNS27時間テレビ 1997年 -

FNN→FNS

(土)21:00 1953年 - 1988年

(土)20:00 1989年

(土)21:00 1990年 - 1991年

(土)19:00 1992年

(土)21:00 1993年 - 1995年

(土)18:00 1996年 - 2003年

(土)18:30 2004年 -

  - 《[明石家聖誕人的史上最大聖誕禮物展覽](../Page/:ja:明石家サンタの史上最大のクリスマスプレゼントショー.md "wikilink")》（每年12月24日
    深夜）
  - 《[人志松本的○○是吧話](../Page/:ja:人志松本の○○な話.md "wikilink")》

#### 播放結束節目

  - 《[逛世界看真相！？](../Page/:ja:世界行ってみたらホントはこんなトコだった！？.md "wikilink")》
  - 《[世界是由言語組成的](../Page/:ja:世界は言葉でできている.md "wikilink")》
  - 《[奧德薩階梯](../Page/:ja:オデッサの階段.md "wikilink")》
  - 《[QUIZ\!HEXAGON II](../Page/:ja:クイズ!ヘキサゴンII.md "wikilink")》
  - 《TOKIO@5LDK》
  - 《平成教育學院》（{{[平成教育學院](../Page/:ja:1年1組_平成教育学院.md "wikilink")}}）
  - 《明星一千一夜晚》（）
  - 《大人的漫畫》（）
  - 《[National價格猜謎](../Page/Panasonic.md "wikilink") 直截了當碰吧》（）
  - 《小故事55號的笑世界》（）
  - 《》
  - 《如果是笑喲》（）
  - 《我們雹子不來到族》（）
  - 《原來如此\!The世界》（）
  - 《LION那就開始吃了》（）
  - 《通宵富士》（）
  - 《晚霞Nyan-Nyang》（）
  - 《Down Town的厲害地好的感覺》（）
  - 《森田一義的語彙天國》（）
  - 《[料理鐵人](../Page/料理鐵人.md "wikilink")》
  - 《[戀愛巴士](../Page/戀愛巴士.md "wikilink")》
  - 《[猜謎$百萬富翁](../Page/:en:Quiz_$_Millionaire.md "wikilink")》（）
  - 《[最弱一環☆最終勝利者的法則](../Page/:en:The_Weakest_Link.md "wikilink")》（）
  - 《雑多的泉～極好的浪費知識～》（）
  - 《新春掩蓋技藝大會》（）
  - 《》
  - 《[HEY\!HEY\!HEY\!MUSIC
    CHAMP](../Page/HEY!HEY!HEY!MUSIC_CHAMP.md "wikilink")》
  - 《[The名曲選播節目](../Page/:ja:ザ・ヒットパレード_\(テレビ番組\).md "wikilink")》
  - 《[夜之HIT STUDIO](../Page/夜之HIT_STUDIO.md "wikilink")》（）
  - 《明星家族對抗賽詩會戰》（）
  - 《[桑田佳祐的音樂寅](../Page/:ja:桑田佳祐の音楽寅さん.md "wikilink")》
  - 《[日本歌謡大獎](../Page/日本歌謡大獎.md "wikilink")》
  - 《新堂本兄弟》

### 電視劇

富士電視台電視劇製作中心（原第一製作部）負責電視劇的製作。共有以下五個播出時段：

  - [富士電視台週一晚間九點連續劇](../Page/富士電視台週一晚間九點連續劇.md "wikilink")（簡稱「月九」）
  - [關西電視台製作週二晚間十點連續劇](../Page/關西電視台週二晚間十點連續劇.md "wikilink")（簡稱「火十」）
  - [富士電視台週三晚間十點連續劇](../Page/富士電視台週三晚間十點連續劇.md "wikilink")（簡稱「水十」）
  - [富士電視台週四連續劇](../Page/富士電視台週四連續劇.md "wikilink")（簡稱「木曜劇場」）
  - [東海電視台製作大人的六劇](../Page/六劇.md "wikilink")（簡稱「土Dora」）

#### 單發劇集

  - 《淺見光彥系列》 主演：中村俊介
  - 《世界奇妙物語》 每年春季、秋季
  - 《ほんとにあった怖い話》 每年夏季
  - 《西村京太郎サスペンス 十津川捜査班》 主演：高嵨政伸
  - 《西村京太郎スペシャル　警部補・佐々木丈太郎》 主演：寺脇康文
  - 《山村美紗サスペンス 赤い霊柩車》 主演：片平渚
  - 《山村美紗サスペンス 小京都連続殺人事件》 主演：淺野溫子
  - 《山村美紗サスペンス 推理作家・池加代子》 主演：名取裕子
  - 《山村美紗サスペンス 黒の滑走路》 主演：淺野優子
  - 《森村誠一サスペンス》 主演：木村佳乃
  - 《夏樹静子サスペンス 検事 霞夕子》主演：澤口靖子
  - 《事件屋稼業》 主演：寺尾聰
  - 《医療捜査官 財前一二三》 主演：高島禮子
  - 《鬼刑事 米田耕作》 主演：中村梅雀
  - 《女医・倉石祥子》 主演：片平渚
  - 《剣客商売》 年末時代劇 主演：北大路欣也
  - 《警視庁練馬署強行犯係・魚住久江》 主演：松下由樹
  - 《外科医 鳩村周五郎》 主演：船越英一郎
  - 《ヤバい検事 矢場健》 主演：館廣
  - 《更生人・土門恭介の再犯ファイル》 主演：渡瀨恒彥
  - 《堂場瞬一サスペンス執着～捜査一課》 主演：反町隆史
  - 《名探偵 神津恭介～影なき女～》 主演：片岡愛之助

### 動畫

  - 《[noitaminA](../Page/noitaminA.md "wikilink")》 每星期四 24:45 - 25:45 播出

  -
  - 《[ONE PIECE](../Page/ONE_PIECE.md "wikilink")》每星期日 09:30 - 10:00 播出

  - 《[櫻桃小丸子](../Page/櫻桃小丸子_\(電視劇\).md "wikilink")》 每星期日 18:00 - 18:30 播出

  - 《[海螺一家](../Page/:ja:サザエさん_\(テレビアニメ\).md "wikilink")》 每星期日 18:30 -
    19:00 播出

#### 播放結束節目

  - 《[鐵臂阿童木](../Page/鐵臂阿童木.md "wikilink")》
      - [Astro Boy 鐵臂阿童木](../Page/Astro_Boy.md "wikilink")
  - 《[鐵人28號](../Page/鐵人28號.md "wikilink")》
  - 《[时间飞船](../Page/时间飞船.md "wikilink")》
  - 《[阿尔卑斯山的少女](../Page/阿尔卑斯山的少女.md "wikilink")》
  - 《[鬼太郎](../Page/鬼太郎.md "wikilink")》
  - 《[魔神Z](../Page/魔神Z.md "wikilink")》
      - [金剛大魔神](../Page/金剛大魔神.md "wikilink")
  - 《[蓋特機器人](../Page/蓋特機器人.md "wikilink")》→蓋特機器人G
  - 《》
  - 《[銀河鐵道999](../Page/銀河鐵道999.md "wikilink")》
  - 《[IQ博士Arale](../Page/IQ博士.md "wikilink")》
  - 《[奇天烈大百科](../Page/奇天烈大百科.md "wikilink")》
  - 《[四月是你的謊言](../Page/四月是你的謊言.md "wikilink")》
  - 《[DRAGON BALL](../Page/七龙珠.md "wikilink")》
      - [七龙珠Z](../Page/七龙珠Z.md "wikilink")
      - [七龙珠GT](../Page/七龙珠GT.md "wikilink")
      - [七龍珠超每星期日](../Page/七龍珠超.md "wikilink") 09:00 - 09:30 播出
  - 《[世界名作劇場](../Page/世界名作劇場.md "wikilink")》
  - [东映动画](../Page/东映动画.md "wikilink")
      - 《[足球风云](../Page/足球风云.md "wikilink")》
      - 《[流星花园](../Page/流星花园.md "wikilink")》

其他多數

### 兒童節目

  - 《[PONKIKKI](../Page/:ja:ポンキッキ.md "wikilink")》

### 其他類型

  - 《[星期五Prestige](../Page/:ja:金曜プレステージ.md "wikilink")》
  - 《[Premium Saturday](../Page/:ja:土曜プレミアム.md "wikilink")》

## 廣播員

### 男性廣播員

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>須田哲夫</li>
<li>福井謙二</li>
<li>小野浩慈</li>
<li>向坂樹興</li>
<li>牧原俊幸</li>
</ul></td>
<td><ul>
<li>桜庭亮平</li>
<li>川端健嗣</li>
<li>軽部真一</li>
<li>三宅正治</li>
<li>笠井信輔</li>
</ul></td>
<td><ul>
<li>塩原恒夫</li>
<li>青嶋達也</li>
<li>境鶴丸</li>
<li>野島卓</li>
<li>吉田伸男</li>
</ul></td>
<td><ul>
<li>福原直英</li>
<li>松元真一郎</li>
<li>奥寺健</li>
<li>佐野瑞樹</li>
<li>伊藤利尋</li>
</ul></td>
<td><ul>
<li>森昭一郎</li>
<li>竹下陽平</li>
<li>西岡孝洋</li>
<li>福永一茂</li>
<li>渡辺和洋</li>
</ul></td>
<td><ul>
<li>田中大貴</li>
<li>鈴木芳彦</li>
<li>倉田大誠</li>
<li>田淵裕章</li>
<li>中村光宏</li>
</ul></td>
<td><ul>
<li>榎並大二郎</li>
<li>立本信吾</li>
<li>福井慶仁</li>
<li>木下康太郎</li>
<li>谷岡慎一</li>
</ul></td>
<td><ul>
<li><a href="../Page/生田龍聖.md" title="wikilink">生田龍聖</a></li>
<li>酒主義久</li>
<li>木村拓也</li>
</ul></td>
</tr>
</tbody>
</table>

### 女性廣播員

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>益田由美</li>
<li>松尾紀子</li>
<li>阿部知代</li>
<li>佐藤里佳</li>
<li>田代尚子</li>
</ul></td>
<td><ul>
<li>西山喜久恵</li>
<li>川野良子</li>
<li>武田祐子</li>
<li>佐佐木恭子</li>
<li>藤村沙織</li>
</ul></td>
<td><ul>
<li>春日由實</li>
<li>島田彩夏</li>
<li>梅津弥英子</li>
<li>森本彩佳</li>
<li><a href="../Page/中村仁美.md" title="wikilink">中村仁美</a></li>
</ul></td>
<td><ul>
<li>石本沙織</li>
<li>戶部洋子</li>
<li>斉藤舞子</li>
<li>遠藤玲子</li>
</ul></td>
<td><ul>
<li><a href="../Page/平井理央.md" title="wikilink">平井理央</a></li>
<li><a href="../Page/秋元優里.md" title="wikilink">秋元優里</a></li>
<li>大島由香里</li>
</ul></td>
<td><ul>
<li><a href="../Page/生野陽子.md" title="wikilink">生野陽子</a></li>
<li><a href="../Page/加藤綾子.md" title="wikilink">加藤綾子</a></li>
<li>椿原慶子</li>
<li><a href="../Page/松村未央.md" title="wikilink">松村未央</a></li>
<li>山中章子</li>
</ul></td>
<td><ul>
<li>細貝沙羅</li>
<li><a href="../Page/山﨑夕貴.md" title="wikilink">山﨑夕貴</a></li>
<li><a href="../Page/三田友梨佳.md" title="wikilink">三田友梨佳</a></li>
<li><a href="../Page/竹內友佳.md" title="wikilink">竹內友佳</a></li>
<li>久代萌美</li>
</ul></td>
<td><ul>
<li>宮澤智</li>
</ul></td>
</tr>
</tbody>
</table>

## 廣播設施

### 攝影棚

### 訊號發射站

  - [東京晴空塔](../Page/東京晴空塔.md "wikilink") 8ch

## 週邊副業

### 電影

  - 富士電視台製作電影

<!-- end list -->

  - [清須會議](../Page/清須會議.md "wikilink")
  - [誰調換了我的父親](../Page/誰調換了我的父親.md "wikilink")
  - [我和妻子的1778個故事](../Page/我和妻子的1778個故事.md "wikilink")
  - [有頂天酒店](../Page/有頂天酒店.md "wikilink")
  - [南極物語](../Page/南極物語.md "wikilink")
  - [開心直航](../Page/開心直航.md "wikilink")
  - [愛的捆綁](../Page/愛的捆綁.md "wikilink")
  - [帝都物語](../Page/帝都物語.md "wikilink")
  - [土龍之歌](../Page/土龍之歌.md "wikilink")
  - [幕末高校生](../Page/幕末高校生.md "wikilink")
  - [審判](../Page/審判_\(電影\).md "wikilink")
  - [溫哥華的朝日](../Page/溫哥華的朝日.md "wikilink")
  - [愛上了你的謊話](../Page/愛上了你的謊話.md "wikilink")
  - [羅馬浴場](../Page/羅馬浴場_\(漫畫\)#電影.md "wikilink") 第一、二輯

<!-- end list -->

  - 電視劇改編電影

<!-- end list -->

  - [大搜查線](../Page/大搜查線.md "wikilink")
  - [挪威的森林](../Page/挪威的森林.md "wikilink")
  - [交響情人夢](../Page/交響情人夢.md "wikilink")
  - [草莓之夜](../Page/草莓之夜.md "wikilink")
  - [大奧](../Page/大奧.md "wikilink")
  - [白色榮光](../Page/白色榮光.md "wikilink")
  - [推理要在晚餐後](../Page/推理要在晚餐後.md "wikilink")
  - [任俠耆兵](../Page/任俠耆兵.md "wikilink")
  - [女神的報酬](../Page/女神的報酬.md "wikilink")
  - [電車男](../Page/電車男.md "wikilink")
  - [HERO](../Page/HERO.md "wikilink")
  - [海猿](../Page/海猿.md "wikilink")
  - [神探伽俐略](../Page/神探伽俐略.md "wikilink") 嫌疑犯X的獻身、真夏的方程式
  - [SP](../Page/SP.md "wikilink")
  - [詐欺遊戲](../Page/詐欺遊戲.md "wikilink")
  - [非關正義Unfair](../Page/非關正義Unfair.md "wikilink")

<!-- end list -->

  - 動畫電影

<!-- end list -->

  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")
  - [海賊王](../Page/海賊王.md "wikilink")
  - [龍珠](../Page/龍珠.md "wikilink")
  - [鬼太郎](../Page/鬼太郎.md "wikilink")

### 活動

  - [御台場冒險王](../Page/御台場冒險王.md "wikilink")
  - [太陽馬戲團日本公演](../Page/太陽馬戲團.md "wikilink")

## 關聯企業

  - [關西電視台](../Page/關西電視台.md "wikilink")（富士產經傳媒集團，[大阪](../Page/大阪.md "wikilink")）
  - [東海電視台](../Page/東海電視台.md "wikilink")（富士產經傳媒集團，[名古屋](../Page/名古屋.md "wikilink")）
  - [日本放送](../Page/日本放送.md "wikilink")（富士產經傳媒集團，[AM](../Page/振幅調變.md "wikilink")[廣播電台](../Page/廣播電台.md "wikilink")）
  - [產業經濟新聞社](../Page/產經新聞.md "wikilink")（富士產經傳媒集團，[報社](../Page/報社.md "wikilink")）
  - [波麗佳音](../Page/波麗佳音.md "wikilink")（PONY
    CANYON；富士產經傳媒集團，[唱片公司](../Page/唱片公司.md "wikilink")）
  - [養樂多燕子](../Page/養樂多燕子.md "wikilink")（[日本職棒](../Page/日本職棒.md "wikilink")[中央聯盟球隊](../Page/中央聯盟.md "wikilink")）
  - [STUDIO艾爾塔](../Page/STUDIO艾爾塔.md "wikilink")（STUDIO
    ALTA；[三越伊勢丹控股](../Page/三越伊勢丹控股.md "wikilink")，東京[新宿](../Page/新宿.md "wikilink")）

## 參考資料

## 外部連結

  - [富士電視台](http://www.fujitv.co.jp/)

      - [](http://www.fujitv.co.jp/)
      - [FUJI TELEVISION NETWORK, INC.](http://www.fujitv.co.jp/cn/)
      - [FUJI TELEVISION NETWORK, INC.](http://www.fujitv.co.jp/en/)
      - [FUJI TELEVISION NETWORK, INC.](https://www.fujitv.co.jp/tc/)
        (繁體中文)

  - [富士產經傳媒集團](http://www.fujisankei-g.co.jp)

  - [富士電視台英文Facebook粉絲頁](https://www.facebook.com/fujitelevision.eng/)

  - [富士電視台繁體中文Facebook粉絲頁](https://www.facebook.com/fujitelevision.tc/)

  -
[Category:1957年成立的公司](../Category/1957年成立的公司.md "wikilink")
[Category:1959年成立的电视台或电视频道](../Category/1959年成立的电视台或电视频道.md "wikilink")
[Category:日本電視台](../Category/日本電視台.md "wikilink")
[\*](../Category/富士电视台.md "wikilink")
[Category:富士產經集團](../Category/富士產經集團.md "wikilink")
[Category:港區公司 (東京都)](../Category/港區公司_\(東京都\).md "wikilink")
[Category:富士新聞網](../Category/富士新聞網.md "wikilink")
[Category:御台場](../Category/御台場.md "wikilink")
[Category:芙蓉集團](../Category/芙蓉集團.md "wikilink")

1.  [シャープ](http://www.sharp.co.jp/support/av/dvd/doc/dvhrw40_connect.pdf)
2.  參見[原子小金剛](../Page/原子小金剛.md "wikilink")。
3.