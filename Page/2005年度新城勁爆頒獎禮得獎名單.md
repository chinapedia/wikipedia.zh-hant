**恆生信用卡
新城勁爆頒獎禮2005**已於2005年12月25日假[香港會議展覽中心頒發](../Page/香港會議展覽中心.md "wikilink")，本屆主題為「樂壇指標
獎星鼓勵」。

## 歌曲獎項

### 新城勁爆歌曲

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>歌曲</p></th>
<th><p>歌手</p></th>
<th><p>作曲</p></th>
<th><p>填詞</p></th>
<th><p>編曲</p></th>
<th><p>監製</p></th>
<th><p>唱片</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>#1</p></td>
<td><p>越唱越強</p></td>
<td><p><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a></p></td>
<td><p><a href="../Page/Nukiko_Nakamaru.md" title="wikilink">Nukiko Nakamaru</a><br />
<a href="../Page/Sonria.md" title="wikilink">Sonria</a></p></td>
<td><p><a href="../Page/夏至.md" title="wikilink">夏至</a></p></td>
<td><p><a href="../Page/舒文.md" title="wikilink">舒文</a></p></td>
<td><p><a href="../Page/舒文.md" title="wikilink">舒文</a></p></td>
<td><p>《<a href="../Page/Bi-Heart.md" title="wikilink">Bi-Heart</a>》</p></td>
</tr>
<tr class="even">
<td><p>#2</p></td>
<td><p>救生圈</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins</a></p></td>
<td><p><a href="../Page/陳光榮.md" title="wikilink">陳光榮</a></p></td>
<td><p><a href="../Page/Fiona_Fung.md" title="wikilink">Fiona Fung</a></p></td>
<td><p>陳光榮</p></td>
<td><p>陳光榮</p></td>
<td><p>《<a href="../Page/Fiona_Fung.md" title="wikilink">Fiona Fung</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#3</p></td>
<td><p>浮誇</p></td>
<td><p><a href="../Page/陳奕迅.md" title="wikilink">陳奕迅</a></p></td>
<td><p><a href="../Page/C.Y._Kong.md" title="wikilink">C.Y. Kong</a></p></td>
<td><p><a href="../Page/黃偉文.md" title="wikilink">黃偉文</a></p></td>
<td><p>C.Y. in Londonl</p></td>
<td><p><a href="../Page/Alvin_Leong.md" title="wikilink">Alvin Leong</a></p></td>
<td><p>《<a href="../Page/U87.md" title="wikilink">U87</a>》</p></td>
</tr>
<tr class="even">
<td><p>#4</p></td>
<td><p>明日恩典</p></td>
<td><p><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a></p></td>
<td><p><a href="../Page/舒文.md" title="wikilink">舒文</a><br />
Trad. arr from "Amazing Grace"（John Newton）</p></td>
<td><p><a href="../Page/黃偉文.md" title="wikilink">黃偉文</a></p></td>
<td><p><a href="../Page/舒文.md" title="wikilink">舒文</a></p></td>
<td><p><a href="../Page/舒文.md" title="wikilink">舒文</a></p></td>
<td><p>《<a href="../Page/喜歡‧祖兒3.md" title="wikilink">喜歡‧祖兒3</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#5</p></td>
<td><p>勁歌金曲</p></td>
<td><p><a href="../Page/古巨基.md" title="wikilink">古巨基</a></p></td>
<td><p>眾作曲人</p></td>
<td><p>眾填詞人</p></td>
<td><p><a href="../Page/雷頌德.md" title="wikilink">雷頌德</a></p></td>
<td><p><a href="../Page/雷頌德.md" title="wikilink">雷頌德</a></p></td>
<td><p>《<a href="../Page/勁歌金曲.md" title="wikilink">勁歌金曲</a>》</p></td>
</tr>
<tr class="even">
<td><p>#6</p></td>
<td><p>天才與白痴</p></td>
<td><p><a href="../Page/古巨基.md" title="wikilink">古巨基</a></p></td>
<td><p><a href="../Page/張佳添.md" title="wikilink">張佳添</a><br />
<a href="../Page/鄭汝森.md" title="wikilink">鄭汝森</a></p></td>
<td><p><a href="../Page/林夕.md" title="wikilink">林夕</a></p></td>
<td><p>張佳添<br />
BC<br />
鄭汝森</p></td>
<td><p>張佳添<br />
古巨基</p></td>
<td><p>《<a href="../Page/星戰.md" title="wikilink">星戰</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#7</p></td>
<td><p>情非首爾</p></td>
<td><p><a href="../Page/李克勤.md" title="wikilink">李克勤</a></p></td>
<td><p><a href="../Page/伍仲衡.md" title="wikilink">伍仲衡</a></p></td>
<td><p><a href="../Page/李克勤.md" title="wikilink">李克勤</a></p></td>
<td><p><a href="../Page/Ronald_Fu.md" title="wikilink">Ronald Fu</a></p></td>
<td><p><a href="../Page/金培達.md" title="wikilink">金培達</a><br />
<a href="../Page/趙增熹.md" title="wikilink">趙增熹</a></p></td>
<td><p>《<a href="../Page/李克勤演奏廳_II.md" title="wikilink">李克勤演奏廳 II</a>》</p></td>
</tr>
<tr class="even">
<td><p>#8</p></td>
<td><p>烈女</p></td>
<td><p><a href="../Page/楊千嬅.md" title="wikilink">楊千嬅</a></p></td>
<td><p><a href="../Page/雷頌德.md" title="wikilink">雷頌德</a></p></td>
<td><p><a href="../Page/林夕.md" title="wikilink">林夕</a></p></td>
<td><p><a href="../Page/雷頌德.md" title="wikilink">雷頌德</a></p></td>
<td><p><a href="../Page/雷頌德.md" title="wikilink">雷頌德</a></p></td>
<td><p>《<a href="../Page/Single.md" title="wikilink">Single</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#9</p></td>
<td><p>無賴</p></td>
<td><p><a href="../Page/鄭中基.md" title="wikilink">鄭中基</a></p></td>
<td><p><a href="../Page/李峻一.md" title="wikilink">李峻一</a></p></td>
<td><p><a href="../Page/李峻一.md" title="wikilink">李峻一</a></p></td>
<td><p><a href="../Page/Ted_Lo.md" title="wikilink">Ted Lo</a></p></td>
<td><p><a href="../Page/Ted_Lo.md" title="wikilink">Ted Lo</a></p></td>
<td><p>《<a href="../Page/Before_After.md" title="wikilink">Before After</a>》</p></td>
</tr>
<tr class="even">
<td><p>#10</p></td>
<td><p>夕陽無限好</p></td>
<td><p><a href="../Page/陳奕迅.md" title="wikilink">陳奕迅</a></p></td>
<td><p><a href="../Page/Eric_Kwok.md" title="wikilink">Eric Kwok</a></p></td>
<td><p><a href="../Page/林夕.md" title="wikilink">林夕</a></p></td>
<td><p><a href="../Page/Eric_Kwok.md" title="wikilink">Eric Kwok</a></p></td>
<td><p><a href="../Page/Eric_Kwok.md" title="wikilink">Eric Kwok</a></p></td>
<td><p>《<a href="../Page/U87.md" title="wikilink">U87</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#11</p></td>
<td><p>阿拉伯市場</p></td>
<td><p><a href="../Page/鄭希怡.md" title="wikilink">鄭希怡</a></p></td>
<td><p><a href="../Page/Eric_Kwok.md" title="wikilink">Eric Kwok</a></p></td>
<td><p><a href="../Page/黃偉文.md" title="wikilink">黃偉文</a></p></td>
<td><p><a href="../Page/Eric_Kwok.md" title="wikilink">Eric Kwok</a></p></td>
<td><p><a href="../Page/Eric_Kwok.md" title="wikilink">Eric Kwok</a></p></td>
<td><p>《<a href="../Page/Yumiko&#39;s_Space.md" title="wikilink">Yumiko's Space</a>》</p></td>
</tr>
<tr class="even">
<td><p>#12</p></td>
<td><p>ABC君</p></td>
<td><p><a href="../Page/方力申.md" title="wikilink">方力申</a></p></td>
<td><p><a href="../Page/雷頌德.md" title="wikilink">雷頌德</a></p></td>
<td><p><a href="../Page/方杰_(填詞人).md" title="wikilink">-{方杰}-</a></p></td>
<td><p><a href="../Page/雷頌德.md" title="wikilink">雷頌德</a></p></td>
<td><p><a href="../Page/雷頌德.md" title="wikilink">雷頌德</a></p></td>
<td><p>《<a href="../Page/Be_Good.md" title="wikilink">Be Good</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#13</p></td>
<td><p>化蝶</p></td>
<td><p><a href="../Page/何韻詩.md" title="wikilink">何韻詩</a></p></td>
<td><p><a href="../Page/何韻詩.md" title="wikilink">何韻詩</a></p></td>
<td><p><a href="../Page/黃偉文.md" title="wikilink">黃偉文</a></p></td>
<td><p><a href="../Page/李端嫻.md" title="wikilink">李端嫻</a>＠人山人海<br />
<a href="../Page/英師傅.md" title="wikilink">英師傅</a></p></td>
<td><p>青山大樂隊<br />
hocc</p></td>
<td><p>《<a href="../Page/梁祝下世傳奇.md" title="wikilink">梁祝下世傳奇</a>》</p></td>
</tr>
<tr class="even">
<td><p>#14</p></td>
<td><p>戰友</p></td>
<td><p><a href="../Page/劉浩龍.md" title="wikilink">劉浩龍</a></p></td>
<td><p><a href="../Page/藍奕邦.md" title="wikilink">藍奕邦</a></p></td>
<td><p><a href="../Page/藍奕邦.md" title="wikilink">藍奕邦</a></p></td>
<td><p><a href="../Page/韋懋騰.md" title="wikilink">韋懋騰</a><br />
<a href="../Page/曹雪芬.md" title="wikilink">曹雪芬</a></p></td>
<td><p><a href="../Page/藍奕邦.md" title="wikilink">藍奕邦</a><br />
<a href="../Page/李漢金.md" title="wikilink">李漢金</a><br />
<a href="../Page/張佳添.md" title="wikilink">張佳添</a></p></td>
<td><p>《<a href="../Page/Past_&amp;_Present.md" title="wikilink">Past &amp; Present</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#15</p></td>
<td><p>我們</p></td>
<td><p><a href="../Page/草蜢_(組合).md" title="wikilink">草蜢</a></p></td>
<td><p><a href="../Page/王雙駿.md" title="wikilink">王雙駿</a></p></td>
<td><p><a href="../Page/林夕.md" title="wikilink">林夕</a></p></td>
<td><p><a href="../Page/王雙駿.md" title="wikilink">王雙駿</a></p></td>
<td><p>王雙駿</p></td>
<td><p>《<a href="../Page/Forever_Grasshopper_Collection.md" title="wikilink">Forever Grasshopper Collection</a>》</p></td>
</tr>
<tr class="even">
<td><p>#16</p></td>
<td><p>佳節</p></td>
<td><p><a href="../Page/李克勤.md" title="wikilink">李克勤</a></p></td>
<td><p><a href="../Page/Edmond_Tsang.md" title="wikilink">Edmond　Tsang</a></p></td>
<td><p><a href="../Page/陳少琪.md" title="wikilink">陳少琪</a></p></td>
<td><p><a href="../Page/張人傑.md" title="wikilink">張人傑</a></p></td>
<td><p><a href="../Page/趙增熹.md" title="wikilink">趙增熹</a><br />
<a href="../Page/陳少琪.md" title="wikilink">陳少琪</a></p></td>
<td><p>《<a href="../Page/空中飛人.md" title="wikilink">空中飛人</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#17</p></td>
<td><p>繼續談情</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a></p></td>
<td><p><a href="../Page/徐繼宗.md" title="wikilink">徐繼宗</a></p></td>
<td><p><a href="../Page/徐繼宗.md" title="wikilink">徐繼宗</a></p></td>
<td><p><a href="../Page/馮翰銘.md" title="wikilink">馮翰銘</a></p></td>
<td><p><a href="../Page/陳德建.md" title="wikilink">陳德建</a></p></td>
<td><p>《<a href="../Page/繼續談情.md" title="wikilink">繼續談情</a>》</p></td>
</tr>
<tr class="even">
<td><p>#18</p></td>
<td><p>希望</p></td>
<td><p><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳</a></p></td>
<td><p><a href="../Page/任世現.md" title="wikilink">任世現</a></p></td>
<td><p><a href="../Page/鄭櫻綸.md" title="wikilink">鄭櫻綸</a></p></td>
<td><p><a href="../Page/劉志遠.md" title="wikilink">劉志遠</a></p></td>
<td><p><a href="../Page/鄭櫻綸.md" title="wikilink">鄭櫻綸</a></p></td>
<td><p>《<a href="../Page/Grace_&amp;_Charm.md" title="wikilink">Grace &amp; Charm</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#19</p></td>
<td><p>南方舞廳</p></td>
<td><p><a href="../Page/達明一派.md" title="wikilink">達明一派</a></p></td>
<td><p><a href="../Page/達明一派.md" title="wikilink">達明一派</a></p></td>
<td><p><a href="../Page/周耀輝.md" title="wikilink">周耀輝</a></p></td>
<td><p><a href="../Page/劉以達.md" title="wikilink">劉以達</a><br />
<a href="../Page/GayBird.md" title="wikilink">GayBird</a></p></td>
<td><p>達明一派<br />
GayBird</p></td>
<td><p>《<a href="../Page/The_Party.md" title="wikilink">The Party</a>》</p></td>
</tr>
<tr class="even">
<td><p>#20</p></td>
<td><p>雌雄同體</p></td>
<td><p><a href="../Page/麥浚龍.md" title="wikilink">麥浚龍</a></p></td>
<td><p><a href="../Page/Otherside.md" title="wikilink">Otherside</a></p></td>
<td><p><a href="../Page/周耀輝.md" title="wikilink">周耀輝</a></p></td>
<td><p><a href="../Page/王雙駿.md" title="wikilink">王雙駿</a></p></td>
<td><p><a href="../Page/王雙駿.md" title="wikilink">王雙駿</a></p></td>
<td><p>《<a href="../Page/Otherside.md" title="wikilink">Otherside</a>》</p></td>
</tr>
<tr class="odd">
<td><p>#21</p></td>
<td><p>感覺蘇豪</p></td>
<td><p><a href="../Page/吳浩康.md" title="wikilink">吳浩康</a></p></td>
<td><p><a href="../Page/吳國恩.md" title="wikilink">吳國恩</a></p></td>
<td><p><a href="../Page/周耀輝.md" title="wikilink">周耀輝</a></p></td>
<td><p><a href="../Page/吳國恩.md" title="wikilink">吳國恩</a></p></td>
<td><p><a href="../Page/周初晨.md" title="wikilink">周初晨</a></p></td>
<td><p>《<a href="../Page/Deep_Inside.md" title="wikilink">Deep Inside</a>》</p></td>
</tr>
</tbody>
</table>

＊**排名依頒獎先後序**

### 新城勁爆合唱歌曲

  - 自欺欺人 -
    [方力申](../Page/方力申.md "wikilink")、[傅穎](../Page/傅穎.md "wikilink")
  - 不可沒友 -
    [Cream](../Page/Cream_\(香港樂隊\).md "wikilink")、[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")
  - 他約我去迪士尼 -
    [陳慧琳](../Page/陳慧琳.md "wikilink")、[Kellyjackie](../Page/Kellyjackie.md "wikilink")

### 新城勁爆跳舞歌曲

  - 瞬間看地球 - [陳苑淇](../Page/陳苑淇.md "wikilink")
  - 愛得耐 - [鄭融](../Page/鄭融.md "wikilink")
  - 霎眼嬌 - [鄧穎芝](../Page/鄧穎芝.md "wikilink")
  - Da Da Da - [梁洛施](../Page/梁洛施.md "wikilink")
  - 新世界 - [2R](../Page/2R.md "wikilink")
  - 愛情倒轉 - [葉文輝](../Page/葉文輝.md "wikilink")

### 新城勁爆原創歌曲

  - 繼續談情 - [劉德華](../Page/劉德華.md "wikilink")
  - 明日恩典 - [容祖兒](../Page/容祖兒.md "wikilink")
  - 漢城沉沒了 - [周國賢](../Page/周國賢.md "wikilink")
  - Blessing - [張敬軒](../Page/張敬軒.md "wikilink")
  - 說中了 - [吳日言](../Page/吳日言.md "wikilink")
  - 名牌 - [余文樂](../Page/余文樂.md "wikilink")

### 新城勁爆國語歌曲

  - 消失 - [南拳媽媽](../Page/南拳媽媽.md "wikilink")
  - 童話 - [光良](../Page/光良.md "wikilink")
  - 夜曲 - [周杰倫](../Page/周杰倫.md "wikilink")

### 新城勁爆卡拉OK歌曲

  - 無賴 - [鄭中基](../Page/鄭中基.md "wikilink")
  - 大哥 - [衛蘭](../Page/衛蘭.md "wikilink")
  - 烈女 - [楊千嬅](../Page/楊千嬅.md "wikilink")
  - 藍鞋子 - [鄧麗欣](../Page/鄧麗欣.md "wikilink")
  - 小黑與我 - [薛凱琪](../Page/薛凱琪.md "wikilink")
  - 甚麼甚麼 - [官恩娜](../Page/官恩娜.md "wikilink")
  - 別怪他 - [吳卓羲](../Page/吳卓羲.md "wikilink")
  - 自欺欺人 -
    [方力申](../Page/方力申.md "wikilink")、[傅穎](../Page/傅穎.md "wikilink")

### 新城勁爆網絡歌曲

  - 老鼠愛大米 - [王啟文](../Page/王啟文.md "wikilink")

## 大碟獎項

### 新城勁爆大碟

  - 《[U87](../Page/U87.md "wikilink")》
    ——[陳奕迅](../Page/陳奕迅.md "wikilink")
  - 《[李克勤演奏廳](../Page/李克勤演奏廳.md "wikilink")》
    ——[李克勤](../Page/李克勤.md "wikilink")
  - 《[星戰](../Page/星战_\(古巨基\).md "wikilink")》
    ——[古巨基](../Page/古巨基.md "wikilink")
  - 《[The Party](../Page/The_Party.md "wikilink")》
    ——[達明一派](../Page/達明一派.md "wikilink")

### 新城勁爆創作大碟

  - 十一月的蕭邦 ——（[周杰倫](../Page/周杰倫.md "wikilink")）

## 創作獎項

### 新城勁爆新一代創作歌手

  - [王菀之](../Page/王菀之.md "wikilink")
  - [側田](../Page/側田.md "wikilink")

### 新城勁爆創作歌手

  - [李克勤](../Page/李克勤.md "wikilink")
  - [何韻詩](../Page/何韻詩.md "wikilink")

## 新人獎項

### 新城勁爆新登場男歌手

  - [葉宇澄](../Page/葉宇澄.md "wikilink")
  - [應昌佑](../Page/應昌佑.md "wikilink")
  - [張繼聰](../Page/張繼聰.md "wikilink")

### 新城勁爆新登場女歌手

  - [梁靖琪](../Page/梁靖琪.md "wikilink")
  - [賈立怡](../Page/賈立怡.md "wikilink")
  - [周麗淇](../Page/周麗淇.md "wikilink")

### 新城勁爆新登場組合獎

  - [Don & Mandy](../Page/Don_&_Mandy.md "wikilink")
  - [Krusty](../Page/Krusty.md "wikilink")

### 新城勁爆新登場海外歌手

  - [林苑](../Page/林苑.md "wikilink")
  - [廖雋嘉](../Page/廖雋嘉.md "wikilink")
  - [王啟文](../Page/王啟文.md "wikilink")

### I-Tech新城勁爆新人王

  - [關智斌](../Page/關智斌.md "wikilink")
  - [側田](../Page/側田.md "wikilink")
  - [王菀之](../Page/王菀之.md "wikilink")
  - [衛蘭](../Page/衛蘭.md "wikilink")

### I-Tech新城勁爆新人王（組合）

  - [Soler](../Page/Soler.md "wikilink")

### I-Tech新城勁爆新人王海外歌手

  - [楊丞琳](../Page/楊丞琳.md "wikilink")

### 全國投選新城勁爆新人王大獎

  - [李宇春](../Page/李宇春.md "wikilink")

## 歌手獎項

### 新城勁爆人氣歌手

  - [鄭中基](../Page/鄭中基.md "wikilink")
  - [鄧麗欣](../Page/鄧麗欣.md "wikilink")
  - [薛凱琪](../Page/薛凱琪.md "wikilink")

### 新城勁爆國語歌手

  - [光良](../Page/光良.md "wikilink")
  - [羅志祥](../Page/羅志祥.md "wikilink")

### 新城勁爆國語樂隊

  - [五月天](../Page/五月天.md "wikilink")

### 新城勁爆國語組合

  - [Twins](../Page/Twins.md "wikilink")

### 新城勁爆跳唱歌手大獎

  - [真命天子](../Page/真命天子_\(羅志祥歌曲\).md "wikilink")
    ——（[羅志祥](../Page/羅志祥.md "wikilink")）

## 全國獎項

### 新城全國勁爆人氣歌手大獎

  - [何潔](../Page/何潔.md "wikilink")
  - [周筆暢](../Page/周筆暢.md "wikilink")
  - [李宇春](../Page/李宇春.md "wikilink")

### 新城全國樂迷投選勁爆歌手大獎

  - [陳慧琳](../Page/陳慧琳.md "wikilink")
  - [李宇春](../Page/李宇春.md "wikilink")
  - [周杰倫](../Page/周杰倫.md "wikilink")

### 新城全國樂迷投選勁爆組合大獎

  - [Twins](../Page/Twins.md "wikilink")

## 全球獎項

### 新城全球勁爆舞台大獎

  - [容祖兒](../Page/容祖兒.md "wikilink")

### 新城全球勁爆歌曲

  - 明日恩典 - [容祖兒](../Page/容祖兒.md "wikilink")

### 新城全球勁爆歌手

  - [楊千嬅](../Page/楊千嬅.md "wikilink")
  - [陳慧琳](../Page/陳慧琳.md "wikilink")
  - [周杰倫](../Page/周杰倫.md "wikilink")
  - [劉德華](../Page/劉德華.md "wikilink")

## 十五周年歌手獎項

### 十五周年新城勁爆冠軍歌手大獎

  - [劉德華](../Page/劉德華.md "wikilink")

### 十五周年新城勁爆突破歌手大獎

  - [方力申](../Page/方力申.md "wikilink")

### 十五周年中國搖擺歌手大獎

  - [田震](../Page/田震.md "wikilink")

### 十五周年香港搖擺歌手大獎

  - [黃家強](../Page/黃家強.md "wikilink")

## 我最欣賞獎項

### 新城勁爆我最欣賞歌曲

  - 浮誇 - [陳奕迅](../Page/陳奕迅.md "wikilink")

### 新城勁爆我最欣賞男歌手

  - [李克勤](../Page/李克勤.md "wikilink")

### 新城勁爆我最欣賞女歌手

  - [容祖兒](../Page/容祖兒.md "wikilink")

### 新城勁爆我最欣賞組合

  - [Twins](../Page/Twins.md "wikilink")

## 新城勁爆男歌手

  - [陳奕迅](../Page/陳奕迅.md "wikilink")
  - [古巨基](../Page/古巨基.md "wikilink")
  - [劉德華](../Page/劉德華.md "wikilink")
  - [李克勤](../Page/李克勤.md "wikilink")

## 新城勁爆女歌手

  - [容祖兒](../Page/容祖兒.md "wikilink")
  - [何韻詩](../Page/何韻詩.md "wikilink")
  - [楊千嬅](../Page/楊千嬅.md "wikilink")

## 新城勁爆組合

  - [Twins](../Page/Twins.md "wikilink")
  - [草蜢](../Page/草蜢.md "wikilink")
  - [達明一派](../Page/達明一派.md "wikilink")

## 新城勁爆亞洲歌手大獎

  - [容祖兒](../Page/容祖兒.md "wikilink")
  - [陳慧琳](../Page/陳慧琳.md "wikilink")

## 新城勁爆年度歌曲大獎

  - 夕陽無限好 - [陳奕迅](../Page/陳奕迅.md "wikilink")

## 新城勁爆播放指數大獎

  - 天才與白痴 - [古巨基](../Page/古巨基.md "wikilink")

## 四台聯頒音樂大獎 - 卓越表現獎

  - 金獎：[薛凱琪](../Page/薛凱琪.md "wikilink")
  - 銀獎：[劉浩龍](../Page/劉浩龍.md "wikilink")
  - 銅獎：[王菀之](../Page/王菀之.md "wikilink")

## 參閱

[Category:新城勁爆流行榜](../Category/新城勁爆流行榜.md "wikilink")