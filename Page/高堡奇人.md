《**高堡奇人**》（），是[科幻小說作家](../Page/科幻小說.md "wikilink")[菲利普·迪克於](../Page/菲利普·K·迪克.md "wikilink")1962年所寫的[架空歷史小說](../Page/架空歷史小說.md "wikilink")。故事發生於1962年的[美國](../Page/美國.md "wikilink")，設定15年前[軸心國在](../Page/軸心國.md "wikilink")[第二次世界大戰擊敗了](../Page/第二次世界大戰.md "wikilink")[同盟國](../Page/同盟國.md "wikilink")，美國向[納粹德國和](../Page/納粹德國.md "wikilink")[大日本帝國投降](../Page/大日本帝國.md "wikilink")。

《高堡奇人》雖不是第一本架空歷史小說，但該小說確立了這種故事形式為一種文學類型。它于1963年獲得[雨果奖最佳长篇小说奖](../Page/雨果奖最佳长篇小说奖.md "wikilink")，而且使得菲利普·迪克在科幻小說圈中出名。它是菲利普·迪克所著小說中結構最緊密、角色最清晰者之一，而且几乎没有使用如科技創新和星际旅行等常见的科幻小說題材。

2015年改编为[电视剧](../Page/高堡奇人_\(电视剧\).md "wikilink")，于2015年1月15日在亞馬遜影片播出。\[1\]

## 情節

### 故事背景

《高堡奇人》書中的世界與真實歷史的分歧點，在於1933年美國總統[小羅斯褔被刺殺一事](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")。在書中，他的繼任人是副總統[約翰·南斯·迦納](../Page/約翰·南斯·迦納.md "wikilink")，其後由[约翰·W·布萊克](../Page/约翰·W·布萊克.md "wikilink")（John
W.
Bricker）所取代。兩人皆無法使美國從經濟[大蕭條中復甦過來](../Page/大蕭條.md "wikilink")，而對即將到來的戰爭仍墨守[孤立主義](../Page/孤立主義.md "wikilink")。

由於美國經濟不景氣以及實行孤立主義，[英國及](../Page/英國.md "wikilink")[歐洲其餘地區落入](../Page/歐洲.md "wikilink")[軸心國手中](../Page/軸心國.md "wikilink")。[蘇聯在](../Page/蘇聯.md "wikilink")1941年崩潰及被[納粹德國佔領](../Page/納粹德國.md "wikilink")，多數[斯拉夫人被滅絕](../Page/斯拉夫人.md "wikilink")。戰爭中倖存的斯拉夫人被限制在一個類似保留區的封閉區域中。日本猛攻[珍珠港](../Page/珍珠港.md "wikilink")，完全摧毀美國的[太平洋艦隊](../Page/太平洋艦隊.md "wikilink")。由於日本軍力擴張，日軍在1940年代初期攻佔了[夏威夷](../Page/夏威夷.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[新西蘭及西南太平洋](../Page/新西蘭.md "wikilink")。在此以後，美國落入軸心國之手，許多重要城市受到嚴重損毀。

[Man_In_The_High_Castle_map.PNG](https://zh.wikipedia.org/wiki/File:Man_In_The_High_Castle_map.PNG "fig:Man_In_The_High_Castle_map.PNG")
1947年，同盟國向軸心國投降。美國東岸被德國控制，[加州及其餘西部各州由日本管治](../Page/加州.md "wikilink")。美國南部各州以一種假獨立形式存在，這些州分大多是猶如[維希法國般的傀儡政權](../Page/維希法國.md "wikilink")。[洛磯山脈各州及中西部大部分地區維持自治](../Page/洛磯山脈.md "wikilink")，因為德日雙方認為它們不重要，也可作為緩衝區。英國領導人及將領在戰後被送上法庭接受戰爭罪行審訊（例如對德國城市實施地氈式轟炸）。

在[希特勒因](../Page/希特勒.md "wikilink")[梅毒而癱瘓後](../Page/梅毒.md "wikilink")，納粹黨黨務部長[馬丁·鮑曼成為領導德國的人](../Page/馬丁·鮑曼.md "wikilink")。納粹創造了一個殖民帝國，繼續大肆屠殺那些被他們認為是低等[種族的人民](../Page/種族.md "wikilink")，屠殺在他們控制區內的[猶太人](../Page/猶太人.md "wikilink")，並在[非洲展開大量](../Page/非洲.md "wikilink")[種族滅絕屠殺](../Page/種族滅絕.md "wikilink")。日本人與納粹德國不同，他們沒有打算清除佔領區內「不需要的」種族。

納粹德國繼續其[火箭計劃](../Page/火箭.md "wikilink")，到1962年，他們已有用於[洲際旅行的火箭](../Page/洲際旅行.md "wikilink")，並發射火箭上月球及火星，進行[太空探索](../Page/太空探索.md "wikilink")。小說也提到，德國已在使用彩色[電視](../Page/電視.md "wikilink")。

在同一時間，日本在大部分亞洲和太平洋的領土上繼續較溫和但獨裁的管治。像現實中在二次大戰後的美國和蘇聯一樣，日本和德國在戰後互不信任，雙方皆擁有[核武及陷入](../Page/核武.md "wikilink")[冷戰](../Page/冷戰.md "wikilink")。

在小說中，鮑曼死後，其他納粹黨人如[戈培爾及](../Page/約瑟夫·戈培爾.md "wikilink")[海德-{里}-希爭奪帝國總理一職](../Page/莱因哈德·特里斯坦·尤根·海德里希.md "wikilink")。不同的納粹派系有的欲與日本開戰，有的對太陽系殖民更感興趣。

### 故事情節

本作品沒有一個中心情節，而是圍繞幾個有點關聯的故事情節：

  - 贝尼斯（Baynes）：表面上是一位瑞典貿易商人，實質是德國反間諜機關[阿勃維爾的特務](../Page/阿勃維爾.md "wikilink")。他旅行到[舊金山](../Page/舊金山.md "wikilink")，與當地日本商業委員會的首領田上信介商談。但是他必須拖延，以完成真正的任務，同時也要避免被捕，直到神袐的谷田部先生（真实身份是
    Tedeki 将军）的到來。
  - 田上信介
    （Tagomi）：日本驻旧金山的贸易代表，奉命接待贝尼斯。他對當代[日本和](../Page/日本.md "wikilink")[德國社會的核心原則的正當性及自己的](../Page/德國.md "wikilink")[佛教信仰出現動搖](../Page/佛教.md "wikilink")。
  - 罗伯特·奇尔丹（Robert
    Childan）：舊金山一間商店的東主，售賣日本人感興趣的美國古物和文化工藝品。在迎合佔領者時試圖保持榮譽與尊嚴。雖然經常對他們奉承，卻與自己對戰爭及佔領者的感覺有矛盾（同時憎恨和尊敬他們），奇尔丹最終找到一種文化自豪感。在日本人對“真正”美國[文物的興趣提升時](../Page/文物.md "wikilink")，他也調查古物市場中廣泛出現的贗品。
  - 弗兰克·弗林科（Frank Frink）：與朋友艾德·麦卡锡（Ed
    McCarthy）開始做首饰制作生意。他们在自创门户前在维德汉姆-马松（Wyndham-Matson）的公司制作仿造的美國收藏品。他們自己设计制作的首饰作品對於见過的美國人和日本人有异常的影響力。弗林科試圖向當地警方隱藏他的[猶太人血統](../Page/猶太人.md "wikilink")，因为企圖破壞维德汉姆-马松的生意而被捕，但由于日德的冲突而田上信介指令其被释放。
  - 朱丽安娜·弗林科（Juliana
    Frink）：弗兰克的前妻，住在[科羅拉多州](../Page/科羅拉多州.md "wikilink")，與一個自稱是大戰中[義大利老兵的卡車司機乔伊](../Page/義大利.md "wikilink")（Joe）發展關係。乔伊想去見有「高堡奇人」稱號的豪索尔尼·阿本德森，那人是《沉重的蚂蚱》的作者。朱丽安娜和他一起旅行，卻發現乔伊其實是一個[德國](../Page/德國.md "wikilink")[党卫队](../Page/党卫队.md "wikilink")[刺客](../Page/刺客.md "wikilink")，奉命去刺杀该作家。朱丽安娜嘗試逃走，被乔伊阻止。朱丽安娜用[刮鬍刀片切開乔伊的](../Page/刮鬍刀.md "wikilink")[喉嚨](../Page/喉嚨.md "wikilink")，然後獨自繼續旅程，終於見到阿本德森。

## 书中书

《高堡奇人》中幾個角色讀過一本雖然在纳粹地区被禁止但卻仍相當流行的小說，名字是《沉重的蚂蚱》（The Grasshopper Lies
Heavy），作者豪索尔尼·阿本德森（Hawthorne
Abendsen）。这是一部[书中书](../Page/戏中戏.md "wikilink")，描述一個軸心國戰败的[架空歷史](../Page/架空.md "wikilink")，儘管这个歷史和《高堡奇人》中的历史相比，与現實歷史更为相似。

在阿本德森的小說中，美國總統[富蘭克林·德拉諾·羅斯福在暗殺中倖存下來](../Page/富蘭克林·德拉諾·羅斯福.md "wikilink")，並在做了两届总统之后，于1940年放弃競選連任，以示对[乔治·华盛顿做两届总统先例的尊重](../Page/乔治·华盛顿.md "wikilink")。下屆總統[雷克斯福德·特格韋爾](../Page/雷克斯福德·特格韋爾.md "wikilink")（真实历史上为罗斯福的经济顾问，曾任波多黎各总督，但並未當過總統）命令美國太平洋艦隊驶离港口，使得[珍珠港轟炸損失減少](../Page/珍珠港.md "wikilink")，所以美國加入大戰時有更多海軍艦隊武力。

在小說中，英國對勝利做出的貢獻比歷史記載的多，而美國和蘇聯則較少。大戰扭轉點是英國在非洲戰勝德國將軍[埃爾溫·隆美爾所率領的納粹軍隊](../Page/埃爾溫·隆美爾.md "wikilink")，英國前鋒穿過[高加索山脈和蘇聯剩餘軍隊協調](../Page/高加索山脈.md "wikilink")，指导苏军在[史達林格勒勝利](../Page/史達林格勒.md "wikilink")。[義大利反水加入同盟国對抗軸心國](../Page/義大利.md "wikilink")。英國、苏联在戰爭最後攻占[柏林](../Page/柏林.md "wikilink")。

戰爭過後，仍然由[邱吉爾領導的英國並沒失去它的帝國](../Page/温斯顿·丘吉尔.md "wikilink")，而美國与由[蔣介石統治的](../Page/蔣介石.md "wikilink")[中國建立了广泛的贸易关系](../Page/中國.md "wikilink")。當美國1950年解決了造成緊張關係的兩大族群的種族爭議時，大英帝國仍維持[種族主義](../Page/種族主義.md "wikilink")。民主的美國挑戰集权的英國，然而英國最終克服美國成為世界上唯一的霸权。

《高堡奇人》中，一般民众傳言《沉重的蚂蚱》的作者阿本德森住在一個高度戒備的堡壘中，其綽號就叫「高堡奇人」。这正是《高堡奇人》名稱的由來。

## 相關資訊

### 引用《易經》

作者迪克聲稱他寫高堡奇人時，用了古代中國的哲學書《[易經](../Page/易經.md "wikilink")》來決定情節發展。他甚至在一次訪問中不高興地抱怨《易經》使得情節太詳細瑣碎了。
\[2\]

《易經》貫穿整部《高堡奇人》。书中描述，在日本佔領美国之後，《易經》的影響力穿過了大洋洲。許多書中角色，包括日本人和美國人，都用它[卜卦來做重要決定](../Page/卜卦.md "wikilink")。书中的作家阿本德森用易經來寫《沉重的蚂蚱》。书中朱丽安娜在最后用《易經》卜卦，询问为什么《易經》要写《沉重的蚂蚱》，人们可以从中领悟什么，得到的卦象是[中孚](../Page/周易六十四卦列表#中孚.md "wikilink")，表示诚信，即《沉重的蚂蚱》中的情节轴心国战败是真实的，而《高堡奇人》中的[世界是虚幻的](../Page/模擬現實.md "wikilink")。

### 話題討論

《高堡奇人》最突出的話題，就是真實的現實進入虛假的現實穿透深度的問題。這可以從小說中幾個方面看到：

  - 罗伯特·奇尔丹發現他的許多古物都是假的，然而假冒的收藏品可以满足制造者、商家、收藏者的需要，甚至比原物更合用。
  - 书中一些角色是間諜，或者是使用假名。
  - 雖然沒有描述現實歷史，小說裡的小說《沉重的蚂蚱》描寫的歷史，比《高堡奇人》作品本身更接近於真實歷史。
  - 弗林科和麦卡锡做的首饰，更像是現實1960年代美國通俗作品，而不像日本或德國的工藝品。這情節和更深處的真實集合間的連結本身通過影響這些片斷影響了許多角色。
  - 《沉重的蚂蚱》，實質上就是《高堡奇人》互相對應的錯亂歷史。對於存在小說世界的人，《沉重的蚂蚱》的世界就是一部小說。這暗示穿透兩個虛假現實，意味著甚至連兩個真實現實或一真一假的現實的觀念意見都是不真實的，但這又加倍真實。
  - 書名《高堡奇人》源于《沉重的蚂蚱》的作者阿本德森居住的地方，但實際上阿本德森住在正常標準的房子。
  - 故事的結尾，暗示有幾個角色透過《易經》的交談，發現他們的世界是虛構的。
  - 田上信介這個角色，似乎短暫變成真實世界所熟悉知道的。在對包含“悟”（一種隱晦的事實的外表）的小釘經過深思熟慮之後，他發現自己在那個讀者生活和熟知的真實世界。

透過這些話題，迪克暗示這個問題，誰或者什麼是造成這種現實裡層穿透的原因？為什麼這原因要將我們所熟知的現實變成欺騙？這些話題在迪克後來幾部小說中提出。

高堡奇人也有關於正義與不正義（通過弗林科對納粹迫害的感覺），性別與權力（通過朱丽安娜和乔伊的關係），差恥心與身分（通過奇尔丹對美國文化的新的自信心与從前有限的对美国文物的懷舊和著迷的对比），和文化上[法西斯主義和種族主義的影響](../Page/法西斯主義.md "wikilink")（通過小說，尤其是在處理納粹統治世界後，尾隨而來生存價值的缺乏，和種族優越主義和種族歧視，在日本人、美國人和德國人這幾個角色中都能看到）等話題。

納粹德國贏得第二世界大戰這個構想，在其他作品包含小說《[Fatherland](../Page/祖國_\(小說\).md "wikilink")》《Star
Trek: Enterprise (Storm Front I & Storm Front II)》兩集中和《Star Trek: The
Original Series (The City on the Edge of Forever)》的一小節中，也有探究討論。

### 軼事

  - 在錯亂世界的舊金山，一種可以改變心情的大麻香煙叫「天籁」（Heavenly Music）。而歌手[Brian
    Eno和](../Page/Brian_Eno.md "wikilink")[Robert
    Fripp即以此來命名專輯](../Page/Robert_Fripp.md "wikilink")《No
    Pussyfooting》中的歌曲：《The Heavenly Music Corporation》。

## 續集

迪克在1967年訪問\[[https://web.archive.org/web/20061025180942/http://www.philipkdickfans.com/frank/hour25.htm\]中表示](https://web.archive.org/web/20061025180942/http://www.philipkdickfans.com/frank/hour25.htm%5D中表示)：「計畫要寫高堡奇人的續集。所以在高堡奇人沒有真正的結局。我想要把它當作是開放式結局，將會繼續到日後的續集。」他明白指出自己他將數次開始來寫續集，但是從沒有持續很長，因為他太心煩於原先對高堡奇人的探究，而且沒有辦法堅持回頭再讀一次納粹。

他也暗示那被提議的續集要找其他作者一起合作：「有人必須來到而且幫助我制作高堡奇人的續集。有人要有能忍受從一堆線去找出它們的源頭的體力；例如，假如你要開始寫關於[萊因哈德·特里斯坦·尤根·海德里希的故事](../Page/萊因哈德·特里斯坦·尤根·海德里希.md "wikilink")，你必須要進入他的思維。你可以想像進入海德里希的思維嗎？

## 参考资料

## 外部連結

  - [Review and
    analysis](http://www.infinityplus.co.uk/nonfiction/highcastle.htm)
  - [Review and
    analysis](https://web.archive.org/web/20090110164546/http://pages.prodigy.net/aesir/mhc.htm)

[Category:平行世界題材小說](../Category/平行世界題材小說.md "wikilink")
[Category:科幻小说](../Category/科幻小说.md "wikilink")
[Category:美國小說](../Category/美國小說.md "wikilink")
[Category:1962年長篇小說](../Category/1962年長篇小說.md "wikilink")
[Category:雨果奖获奖作品](../Category/雨果奖获奖作品.md "wikilink")

1.
2.