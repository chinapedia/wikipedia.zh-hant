[缩略图](https://zh.wikipedia.org/wiki/File:HK_Mongkok_西洋菜南街_Sai_Yeung_Choi_Street_South_蘇寧鐳射_Suning_shop_night_July-2011.jpg "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Kaesong_familymart.jpg "fig:缩略图")開城特區的全家超商\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Kfc_taco_bell.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Rt-Mart_Suzhou_Donghuan.jpg "fig:缩略图")連鎖量販\]\]
**連鎖店**是一組以同一[品牌的](../Page/品牌.md "wikilink")[零售商](../Page/零售商.md "wikilink")，通常是含有標準化的商業方法及實習的中央集團管理。它們是[連鎖商業的一種](../Page/連鎖商業.md "wikilink")。這種店舖可能是由該[公司擁有](../Page/公司類別.md "wikilink")，或可能是個人或[中小企公司由母公司取得](../Page/中小企.md "wikilink")[經營權在](../Page/經營權.md "wikilink")[加盟連鎖合約下經營](../Page/加盟連鎖.md "wikilink")。通常連鎖店的特色是包括中央的[营销及](../Page/营销.md "wikilink")[交易](../Page/交易.md "wikilink")，從[經濟比例中](../Page/經濟比例.md "wikilink")，較低的[成本可以獲得更高的](../Page/成本.md "wikilink")[盈利](../Page/盈利.md "wikilink")。

這種特色亦應用至連鎖餐廳和一些以服務為本的連鎖商業。有些認為對於標準化中所產生的標準化產品從[文化角度中有不度影響](../Page/文化.md "wikilink")；例如，如果連鎖[音樂店排斥知名度較低](../Page/音樂店.md "wikilink")（通常是獨立的歌手），通常會存入較多[流行音乐的作品](../Page/流行音乐.md "wikilink")。有評論指出連鎖店會在經濟角度上損害社會，因為它們可從其它的地方經濟中提取資本，在本地經濟及獨立自主商業之間不斷循環。

獨立行業被連鎖店取代在很多的國家中引起爭議，因此產生出獨立行業之間的共同合作，以防止連鎖店的擴張。在這些因素下一些組織因此產生，例如及，同時亦有民間的聯盟，好像。對於國家上的有及在美國中展示實力。在英國中，創立民間為本的經濟及獨立的所有權。

連鎖店的形式可以包括以下的行業，好像從[批發](../Page/批發.md "wikilink")、[零售等](../Page/零售.md "wikilink")，以至[飲食及服務行業都可以以連鎖式策略經營](../Page/飲食.md "wikilink")。

在全球中，[麥當勞](../Page/麥當勞.md "wikilink")、[漢堡王](../Page/漢堡王.md "wikilink")、[必勝客](../Page/必勝客.md "wikilink")、[吉野家等是全球知名的連鎖快餐店](../Page/吉野家.md "wikilink")。另外[7-Eleven是全球最大的連鎖](../Page/7-Eleven.md "wikilink")[便利店](../Page/便利商店.md "wikilink")。此外，[家樂福](../Page/家樂福.md "wikilink")、[沃爾瑪](../Page/沃爾瑪.md "wikilink")、[玩具反斗城等亦是全球較知名的連鎖銷售店](../Page/玩具反斗城.md "wikilink")。

## 連鎖店在香港的發展

在[香港裏](../Page/香港.md "wikilink")，一些較知名的連鎖式超級市場有[百佳及](../Page/百佳.md "wikilink")[惠康](../Page/惠康.md "wikilink")，而[屈臣氏及](../Page/屈臣氏.md "wikilink")[萬寧是香港知名的連鎖式保健及美容專門店](../Page/萬寧.md "wikilink")。另外連鎖式百貨公司包括[馬莎百貨](../Page/馬莎百貨.md "wikilink")、[Aeon等](../Page/Aeon.md "wikilink")。7-Eleven和[OK便利店是香港裏知名的便利店](../Page/OK便利店.md "wikilink")。另外[大家樂](../Page/大家樂.md "wikilink")、[大快活](../Page/大快活.md "wikilink")、[美心快餐則為較為知名的本地連鎖快餐店](../Page/美心快餐.md "wikilink")。[優の良品](../Page/優之良品.md "wikilink")、[零食物語等店舖是在香港知名的零食連鎖專賣店](../Page/零食物語.md "wikilink")。此外，在香港亦都有一些集團以連鎖式酒樓經營。

## 連鎖店在臺灣的發展

在[臺灣](../Page/臺灣.md "wikilink")，連鎖事業極為發達，從食、衣、住、行、育、樂，樣樣都有連鎖店，由於市場多元性夠，因此，除了本土業者發展連鎖事業外，也有不少國外業者進入臺灣市場從事連鎖經營，連鎖的方式也是相當多樣性的，有直營連鎖、委託加盟、特許加盟等不一而足，就以[便利商店而言](../Page/便利商店.md "wikilink")，目前在[臺灣就](../Page/臺灣.md "wikilink")7000家以上，其中，由[統一超商所經營的](../Page/統一超商.md "wikilink")[7-11在市場上佔有率達一半以上](../Page/7-11.md "wikilink")，近4200家，其它依序是[全家](../Page/全家便利商店.md "wikilink")、[萊爾富](../Page/萊爾富.md "wikilink")、[OK及](../Page/OK便利店.md "wikilink")[福客多等](../Page/福客多便利商店.md "wikilink")，其他的連鎖業者中，較為知名的本土業者有[誠品書店](../Page/誠品書店.md "wikilink")、[康是美藥妝店](../Page/康是美.md "wikilink")、[遠東百貨](../Page/遠東百貨.md "wikilink")、[大潤發量販店](../Page/大潤發.md "wikilink")、[NET服飾](../Page/NET.md "wikilink")、[生活工場雜貨](../Page/生活工場.md "wikilink")、[光南大批發百貨等](../Page/光南大批發.md "wikilink")，在外商業者有：從香港來臺的[惠康超市](../Page/惠康.md "wikilink")（在臺灣稱為頂好超市）、[屈臣氏](../Page/屈臣氏.md "wikilink")、[必勝客](../Page/必勝客.md "wikilink")、[佐丹奴服飾](../Page/佐丹奴.md "wikilink")、[HANG
TEN服飾等](../Page/HANG_TEN.md "wikilink")；從法國來臺[家樂福量販店](../Page/家樂福.md "wikilink")、[法雅客書店](../Page/法雅客書店.md "wikilink")；從美國引進的[星巴克](../Page/星巴克.md "wikilink")、[麥當勞](../Page/麥當勞.md "wikilink")、[肯德基](../Page/肯德基.md "wikilink")、[7-11](../Page/7-11.md "wikilink")；從日本引進的[FamilyMart](../Page/FamilyMart.md "wikilink")、[吉野家](../Page/吉野家.md "wikilink")、[三越百貨](../Page/三越百貨.md "wikilink")（在臺灣稱為[新光三越百貨](../Page/新光三越百貨.md "wikilink")）、[崇光百貨](../Page/崇光百貨.md "wikilink")、[松青超市](../Page/松青超市.md "wikilink")、[儂特利等](../Page/儂特利.md "wikilink")，近年來在[臺北世界貿易中心都會有](../Page/臺北世界貿易中心.md "wikilink")[臺北國際連鎖加盟大展](../Page/臺北國際連鎖加盟大展.md "wikilink")，國內外許多知名業者都會群聚相互交流，並希望藉此機會吸引更多人加盟連鎖事業。
在2004年，全球最大的零售連鎖店[沃爾瑪是世界上總銷售量最大的公司](../Page/沃爾瑪.md "wikilink")。

## 连锁商店的类型

1、从成员店的分布地区划分，连锁商店有地区性连锁、全国性连锁和国际性连锁3种类型。地区性连锁即总部及所有成员店集中于同一城市或地区；若所有成员店分布于全国各地则为全国性连锁；成员店的发展已跨国界分布，称为[国际性连锁](../Page/国际性.md "wikilink")。

2、根据总部所所在地区划分，连锁商店可分为地方连锁和中央连锁。地方连锁是指连锁商店总部位于地方城市；中央连锁是指连锁商店总部位于全国政治、经济、文化中心——首都，而成员店遍布全国乃至国外。

3、按照经营形式划分，连锁经营可以分为正规连锁、特许连锁和自由连锁3种；这主要是从所有权和经营权的集中程度来划分的，是连锁商店最基本的分类方式

## 相关条目

  - [超市列表](../Page/超市列表.md "wikilink")
  - [便利商店](../Page/便利商店.md "wikilink")
  - [加盟連鎖](../Page/加盟連鎖.md "wikilink")

## 外部連結

  - [台灣連鎖加盟協會](http://www.tcfa.org.tw/)
  - [台灣連鎖加盟促進會](http://www.franchise.org.tw/)
  - [日本連鎖店協會](http://www.jcsa.gr.jp/)（日文）
  - [日本連鎖藥物店協會](http://www.jacds.gr.jp/)（日文）
  - [美國獨立商業聯盟](http://amiba.net)（英文）
  - [新統治計劃](http://newrules.org)（英文）
  - [新經濟基金](http://www.neweconomics.org)（英文）

獨立貿易組織

  - [美國圖書商協會](http://bookweb.org)（英文）

[\*](../Category/連鎖店.md "wikilink")