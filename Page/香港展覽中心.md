[Hong_Kong_Exhibition_Centre_2009.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Exhibition_Centre_2009.jpg "fig:Hong_Kong_Exhibition_Centre_2009.jpg")
[HK_Wan_Chai_China_Resource_Building_HK_Exhibition_Centre_PRC_Event_1.JPG](https://zh.wikipedia.org/wiki/File:HK_Wan_Chai_China_Resource_Building_HK_Exhibition_Centre_PRC_Event_1.JPG "fig:HK_Wan_Chai_China_Resource_Building_HK_Exhibition_Centre_PRC_Event_1.JPG")

**香港展覽中心**（）為[香港一間歷史悠久及於](../Page/香港.md "wikilink")2013年年底被拆卸的[商業](../Page/商業.md "wikilink")[貿易](../Page/貿易.md "wikilink")[展覽館](../Page/展覽館.md "wikilink")，遺址為[香港島](../Page/香港島.md "wikilink")[灣仔北](../Page/灣仔北.md "wikilink")[港灣道](../Page/港灣道.md "wikilink")[華潤大廈低座](../Page/華潤大廈.md "wikilink")，正重建為[香港瑞吉酒店](../Page/香港瑞吉酒店.md "wikilink")。

## 概況

香港展覽中心於1982年9月落成啓用\[1\]，原名中國出口商品陳列館，由[華潤集團的子公司中國廣告展覽有限公司經營](../Page/華潤集團.md "wikilink")。香港展覽中心為非官方營運，歷史較由[香港貿易發展局營運的](../Page/香港貿易發展局.md "wikilink")[香港會議展覽中心長久](../Page/香港會議展覽中心.md "wikilink")。香港展覽中心總面積共25,000平方尺，分為展覽廳及展覽館。\[2\]

展覽廳設於3樓，為長方型設計，總面積約2,100平方米，地面以乳白色人造[雲石鋪設](../Page/雲石.md "wikilink")，天花則為雪白色。展覽廳設有[無線上網](../Page/無線上網.md "wikilink")，地面承重每平方米1.5噸\[3\]。展覽館則設於4樓，同為長方型設計，面積約450平方米，主要供舉辦文化藝術活動之用\[4\]。

## 拆卸重建

2010年，[華潤物業為華潤大廈高座進行翻新重建工程](../Page/華潤物業.md "wikilink")，造價為6億港元，計劃包括將低座的香港展覽中心重建成18層高的[香港瑞吉酒店](../Page/香港瑞吉酒店.md "wikilink")，提供129間房間\[5\]。項目由劉榮廣伍振民建築事務所負責\[6\]。拆卸工程於2013年年底展開。

## 交通

<div class="NavFrame collapsed" style="color:black; background:Yellow; margin:0 auto; padding:0 10px; text-align:left;">

<div class="NavHead" style="background:Yellow; margin:0 auto; padding:0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background:Yellow; margin:0 auto; padding:0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[灣仔站A](../Page/灣仔站.md "wikilink")5出入口

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 参考文献

[Category:灣仔北](../Category/灣仔北.md "wikilink")
[Category:香港展覽場地](../Category/香港展覽場地.md "wikilink")
[Category:香港表演場館](../Category/香港表演場館.md "wikilink")
[Category:香港已不存在的建築物](../Category/香港已不存在的建築物.md "wikilink")
[Category:華潤](../Category/華潤.md "wikilink")

1.  大公報，1986年11月10日，第9版

2.  [香港展覽中心](http://www.crae.com.hk/sc_webcat/ecat/cms_view.php?lang=2&web_id=21)
    中國廣告展覽有限公司

3.  [展覽廳及展覽館](http://www.crae.com.hk/sc_webcat/ecat/cms_view.php?lang=2&web_id=30)
    中國廣告展覽有限公司

4.
5.  [華潤大廈重建
    低座變酒店](http://news.stheadline.com/dailynews/content_hk/2010/03/24/105880.asp)
    頭條日報，2010年3月24日

6.  [華潤大廈高座六億元翻新](http://orientaldaily.on.cc/cnt/news/20091216/00176_051.html)
    東方日報，2009年12月16日