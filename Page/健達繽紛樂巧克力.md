[缩略图](https://zh.wikipedia.org/wiki/File:Kinder_Bueno_alt.gif "fig:缩略图")
**健達繽紛樂巧克力**，或稱**健达缤纷乐**（英语：Kinder
Bueno，1990年～），是巧克力制造商[费列罗](../Page/费列罗.md "wikilink")（Ferrero）出品的一种[牛奶](../Page/牛奶.md "wikilink")[榛果威化](../Page/榛果.md "wikilink")[巧克力](../Page/巧克力.md "wikilink")。缤纷乐是牛奶巧克力加上威化外层，内含牛奶榛果酱，销售包装分为2条和6条两种。

## 品名及起源

  - **Kinder**是[德语中](../Page/德语.md "wikilink")“孩子”的意思，**Bueno**是[西班牙语中](../Page/西班牙语.md "wikilink")“好、好吃的、美味的”的意思。
  - 缤纷乐原产国为[意大利](../Page/意大利.md "wikilink")，销售始于1990年，第一站是1991年在[德国](../Page/德国.md "wikilink")。

<!-- end list -->

  - 20世纪90年代中期，健达缤纷乐开始进入[拉丁美洲](../Page/拉丁美洲.md "wikilink")（[巴西](../Page/巴西.md "wikilink")、[阿根廷和](../Page/阿根廷.md "wikilink")[墨西哥](../Page/墨西哥.md "wikilink")）、[马来西亚](../Page/马来西亚.md "wikilink")、[以色列和](../Page/以色列.md "wikilink")[希腊](../Page/希腊.md "wikilink")。从1999年开始[法国和](../Page/法国.md "wikilink")[德国开始销售](../Page/德国.md "wikilink")，[英国和](../Page/英国.md "wikilink")[加拿大则从](../Page/加拿大.md "wikilink")2004年开始变为常见商品。
    [Kinder_Bueno_ak.jpg](https://zh.wikipedia.org/wiki/File:Kinder_Bueno_ak.jpg "fig:Kinder_Bueno_ak.jpg")

## 配料

[牛奶巧克力](../Page/牛奶巧克力.md "wikilink")31.5%（白砂糖，可可脂，可可浆，脱脂乳粉，无水奶油，大豆磷脂，香兰素），[白砂糖](../Page/白砂糖.md "wikilink")，[植物油](../Page/植物油.md "wikilink")，[小麦面粉](../Page/小麦面粉.md "wikilink")，[榛子](../Page/榛子.md "wikilink")10.5%，脱脂乳粉，全脂乳粉，[黑巧克力](../Page/黑巧克力.md "wikilink")（白砂糖，可可浆，可可脂，大豆磷脂，香兰素），低脂[可可粉](../Page/可可粉.md "wikilink")，大豆磷脂，[膨松剂](../Page/膨松剂.md "wikilink")，[食盐](../Page/食盐.md "wikilink")，[香兰素](../Page/香兰素.md "wikilink")。牛奶成分為19.5%。\[1\]

## 参看

  - [健達出奇蛋](../Page/健達出奇蛋.md "wikilink")
  - [健達巧克力](../Page/健達巧克力.md "wikilink")

## 参考资料

[Category:巧克力糖](../Category/巧克力糖.md "wikilink")
[Category:糖果](../Category/糖果.md "wikilink")
[Category:意大利食品](../Category/意大利食品.md "wikilink")

1.  摘自在中国内地销售的健达缤纷乐配料表