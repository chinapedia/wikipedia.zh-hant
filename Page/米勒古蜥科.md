**米勒古蜥科**（Millerettidae）是種已滅絕[無孔類](../Page/無孔類.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")，生存於[二疊紀中晚期的](../Page/二疊紀.md "wikilink")[南非](../Page/南非.md "wikilink")\[1\]。牠們是小型食蟲性動物，外表與生活方式可能類似現代[蜥蜴](../Page/蜥蜴.md "wikilink")。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [柏克萊大學的網頁](http://www.ucmp.berkeley.edu/anapsids/millerettidae.html)

[\*](../Category/無孔亞綱.md "wikilink")

1.