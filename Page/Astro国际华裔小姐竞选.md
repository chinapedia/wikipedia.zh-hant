《**Astro国际华裔小姐竞选**》，是[马来西亚](../Page/马来西亚.md "wikilink")[Astro华丽台举办的](../Page/Astro华丽台.md "wikilink")。首届1998年冠军是[吴天瑜](../Page/吴天瑜.md "wikilink")，现任冠军是[彭嘉伊](../Page/彭嘉伊.md "wikilink")(2018年Astro国际华裔小姐)。由於选美合约有规定，每届[冠军会代表](../Page/冠军.md "wikilink")[马来西亚参加](../Page/马来西亚.md "wikilink")[TVB举办的](../Page/TVB.md "wikilink")[国际中华小姐竞选](../Page/国际中华小姐竞选.md "wikilink")。\[1\]

## 历史

《**Astro国际华裔小姐竞选**》的前身是《**马来西亚华裔小姐**》（Miss Malaysia Chinatown
），香港TVB于1988年创办国际华裔小姐竞选并邀请世界各大城市代表佳丽参赛，冠军亦自此被委派前往香港代表马来西亚角逐该项选美活动。1998年之前参加国际华裔小姐竞选的代表并不一定是获得马来西亚华裔小姐冠军的选手，也可以从代表各自所在城市的身份去参加。到1998年，是项选美会被[Astro华丽台接办](../Page/Astro华丽台.md "wikilink")，並改名为《**马来西亚国际华裔小姐竞选**》(Miss
Malaysia Chinese International
Pageant)，2004年再易名为《**Astro国际华裔小姐竞选**》(Miss
Astro Chinese International Pageant)。

1989年马来西亚华裔小姐冠军 [邱明珠](../Page/邱明珠.md "wikilink")（槟城）
:\*'''1989年国际华裔小姐

:\*\*[邱明珠](../Page/邱明珠.md "wikilink")（槟城） 1990年马来西亚华裔小姐冠军
[张丝媚](../Page/张丝媚.md "wikilink")（柔佛）
:\*'''1991年国际华裔小姐

:\*\*[张丝媚](../Page/张丝媚.md "wikilink")（柔佛） 1991年马来西亚华裔小姐冠军
[林美貞](../Page/林美貞.md "wikilink")（怡保）
:\*'''1992年国际华裔小姐

:\*\*[林美貞](../Page/林美貞.md "wikilink")（怡保） 1992年马来西亚华裔小姐冠军
[张慧仪](../Page/张慧仪.md "wikilink")（怡保）
:\*'''1993年国际华裔小姐

:\*\*[张慧仪](../Page/张慧仪.md "wikilink")（怡保）

:\*\*[林赞蕊](../Page/林赞蕊.md "wikilink")（[古晋](../Page/古晋.md "wikilink")）
1993年马来西亚华裔小姐冠军 [杨佩君](../Page/杨佩君.md "wikilink")（亚罗士打）
:\*'''1994年国际华裔小姐

:\*\*[杨佩君](../Page/杨佩君.md "wikilink")（[亚罗士打](../Page/亚罗士打.md "wikilink")）

:\*\*[陈金金](../Page/陈金金.md "wikilink")（[亞庇](../Page/亞庇.md "wikilink")）
1994年马来西亚华裔小姐冠军 [范绮雯](../Page/范绮雯.md "wikilink")（怡保）
:\*'''1995年国际华裔小姐

:\*\*[范绮雯](../Page/范绮雯.md "wikilink")（怡保）

:\*\*[秦顺薏](../Page/秦顺薏.md "wikilink")（吉隆坡） 1995年马来西亚华裔小姐冠军
[李笑美](../Page/李笑美.md "wikilink")（吉隆坡）
:\*'''1996年国际华裔小姐

:\*\*[李笑美](../Page/李笑美.md "wikilink")（吉隆坡）

:\*\*[李燕玲](../Page/李燕玲.md "wikilink")（新山） 1996年马来西亚华裔小姐冠军
[严慧贤](../Page/严慧贤.md "wikilink")（吉隆坡）
:\*'''1997年国际华裔小姐

:\*\*[严慧贤](../Page/严慧贤.md "wikilink")（吉隆坡）

:\*\*[郭秀娴](../Page/郭秀娴.md "wikilink")（怡保） 1997年马来西亚华裔小姐冠军
[陈英丽](../Page/陈英丽.md "wikilink")（吉隆坡）
:\*'''1998年国际华裔小姐

:\*\*[陈英丽](../Page/陈英丽.md "wikilink")（吉隆坡）

## 历届得奖佳丽

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>冠军</strong></p></td>
<td><p><strong>亚军</strong></p></td>
<td><p><strong>季军</strong></p></td>
<td><p><strong>最上鏡小姐</strong></p></td>
<td><p><strong>优雅小姐</strong></p></td>
<td><p><strong>完美体态小姐</strong></p></td>
<td><p><strong>亲善小姐</strong></p></td>
<td><p><strong>才艺小姐</strong></p></td>
<td><p><strong>其他奖项</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>1998年</strong></p></td>
<td><p><a href="../Page/吴天瑜.md" title="wikilink">吴天瑜</a></p></td>
<td><p><a href="../Page/李亦甄.md" title="wikilink">李亦甄</a></p></td>
<td></td>
<td><p><a href="../Page/吴天瑜.md" title="wikilink">吴天瑜</a></p></td>
<td><p><a href="../Page/吴天瑜.md" title="wikilink">吴天瑜</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/李亦甄.md" title="wikilink">李亦甄</a></p></td>
<td><p>-</p></td>
<td><p>最受观众欢迎小姐: <a href="../Page/吴天瑜.md" title="wikilink">吴天瑜</a><br />
最受传媒欢迎小姐: <a href="../Page/吴天瑜.md" title="wikilink">吴天瑜</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>1999年</strong></p></td>
<td><p><a href="../Page/何赞妮.md" title="wikilink">何赞妮</a></p></td>
<td><p><a href="../Page/黄慧诗.md" title="wikilink">黄慧诗</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/何赞妮.md" title="wikilink">何赞妮</a></p></td>
<td><p>|-</p></td>
<td><p>|-</p></td>
<td><p>迷人小姐: <a href="../Page/黄慧诗.md" title="wikilink">黄慧诗</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2000年</strong></p></td>
<td><p><a href="../Page/刘晓萍.md" title="wikilink">刘晓萍</a></p></td>
<td><p><a href="../Page/林佩盈.md" title="wikilink">林佩盈</a></p></td>
<td><p><a href="../Page/颜辉雯.md" title="wikilink">颜辉雯</a></p></td>
<td><p><a href="../Page/刘晓萍.md" title="wikilink">刘晓萍</a></p></td>
<td><p><a href="../Page/林佩盈.md" title="wikilink">林佩盈</a></p></td>
<td><p>-</p></td>
<td><p>劉淑娟</p></td>
<td><p>-</p></td>
<td><p>美腿小姐: <a href="../Page/刘晓萍.md" title="wikilink">刘晓萍</a></p>
<p>健美小姐: 刘淑娟</p>
<p>网上超人气小姐: <a href="../Page/颜辉雯.md" title="wikilink">颜辉雯</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2001年</strong></p></td>
<td><p><a href="../Page/符愫娱.md" title="wikilink">符愫娱</a></p></td>
<td><p><a href="../Page/黄春霞.md" title="wikilink">黄春霞</a></p></td>
<td><p><a href="../Page/曹诗萍.md" title="wikilink">曹诗萍</a></p></td>
<td><p><a href="../Page/李芝忆.md" title="wikilink">李芝忆</a></p></td>
<td><p><a href="../Page/符愫娱.md" title="wikilink">符愫娱</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/郑燕燕.md" title="wikilink">郑燕燕</a></p></td>
<td><p>-</p></td>
<td><p>明眸小姐: 洪立仪</p>
<p>美腿小姐: <a href="../Page/曹诗萍.md" title="wikilink">曹诗萍</a></p>
<p>清新可人小姐: 方丽丽</p>
<p>健康肌肤小姐: 梅钏婷</p>
<p>最受观众欢迎小姐: <a href="../Page/符愫娱.md" title="wikilink">符愫娱</a></p>
<p>网上超人气小姐: <a href="../Page/符愫娱.md" title="wikilink">符愫娱</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2002年</strong></p></td>
<td><p><a href="../Page/陈泳锦.md" title="wikilink">陈泳锦</a></p></td>
<td><p><a href="../Page/林思佳.md" title="wikilink">林思佳</a></p></td>
<td><p><a href="../Page/陈玲珑.md" title="wikilink">陈玲珑</a></p></td>
<td><p><a href="../Page/陈泳锦.md" title="wikilink">陈泳锦</a></p></td>
<td><p>赖佩芳</p></td>
<td><p><a href="../Page/郑秀珍.md" title="wikilink">郑秀珍</a></p></td>
<td><p>吴爱仪</p></td>
<td><p>|清新可人小姐: 林思佳</p>
<p>网上超人气小姐: 鲍欣瑜</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2003年</strong></p></td>
<td><p><a href="../Page/杨秀惠.md" title="wikilink">杨秀惠</a></p></td>
<td><p><a href="../Page/唐慧珍.md" title="wikilink">唐慧珍</a></p></td>
<td><p><a href="../Page/孙雪娟.md" title="wikilink">孙雪娟</a></p></td>
<td><p><a href="../Page/杨秀惠.md" title="wikilink">杨秀惠</a></p></td>
<td><p><a href="../Page/杨秀惠.md" title="wikilink">杨秀惠</a></p></td>
<td><p><a href="../Page/杨秀惠.md" title="wikilink">杨秀惠</a></p></td>
<td><p>方慧敏</p></td>
<td><p><a href="../Page/刘丽君.md" title="wikilink">刘丽君</a></p></td>
<td><p>清新可人小姐: <a href="../Page/杨秀惠.md" title="wikilink">杨秀惠</a></p>
<p>网上超人气小姐: 林美玲</p></td>
</tr>
<tr class="even">
<td><p><strong>2004年</strong></p></td>
<td><p><a href="../Page/陈影雯.md" title="wikilink">陈影雯</a></p></td>
<td><p><a href="../Page/谢德蓉.md" title="wikilink">谢德蓉</a></p></td>
<td><p><a href="../Page/秦雯彬.md" title="wikilink">秦雯彬</a></p></td>
<td><p><a href="../Page/陈莹卿.md" title="wikilink">陈莹卿</a></p></td>
<td><p><a href="../Page/陈影雯.md" title="wikilink">陈影雯</a></p></td>
<td><p><a href="../Page/陈莹卿.md" title="wikilink">陈莹卿</a></p></td>
<td><p><a href="../Page/陈影雯.md" title="wikilink">陈影雯</a></p></td>
<td><p><a href="../Page/陈苇珍.md" title="wikilink">陈苇珍</a></p></td>
<td><p>网上超人气小姐: <a href="../Page/陈瑞琳.md" title="wikilink">陈瑞琳</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2005年</strong></p></td>
<td><p><a href="../Page/江佩莹.md" title="wikilink">江佩莹</a></p></td>
<td><p><a href="../Page/林佩琦.md" title="wikilink">林佩琦</a></p></td>
<td><p><a href="../Page/林苑君.md" title="wikilink">林苑君</a></p></td>
<td><p><a href="../Page/江佩莹.md" title="wikilink">江佩莹</a></p></td>
<td><p><a href="../Page/林佩琦.md" title="wikilink">林佩琦</a></p></td>
<td><p><a href="../Page/林佩琦.md" title="wikilink">林佩琦</a></p></td>
<td><p><a href="../Page/康敏君.md" title="wikilink">康敏君</a></p></td>
<td><p><a href="../Page/戴淑怡.md" title="wikilink">戴淑怡</a></p></td>
<td><p>第四名: <a href="../Page/康敏君.md" title="wikilink">康敏君</a><br />
第五名: <a href="../Page/戴淑怡.md" title="wikilink">戴淑怡</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2006年</strong></p></td>
<td><p><a href="../Page/林秀婷.md" title="wikilink">林秀婷</a></p></td>
<td><p><a href="../Page/林美伶.md" title="wikilink">林美伶</a></p></td>
<td><p><a href="../Page/吳俐璇.md" title="wikilink">吳俐璇</a></p></td>
<td><p><a href="../Page/林秀婷.md" title="wikilink">林秀婷</a></p></td>
<td><p>|<a href="../Page/林美伶.md" title="wikilink">林美伶</a></p></td>
<td><p>倪于婷</p></td>
<td><p><a href="../Page/林美伶.md" title="wikilink">林美伶</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2007年</strong></p></td>
<td><p><a href="../Page/尤凤音.md" title="wikilink">尤凤音</a></p></td>
<td><p><a href="../Page/丘婉铤.md" title="wikilink">丘婉铤</a></p></td>
<td><p><a href="../Page/潘碧宁.md" title="wikilink">潘碧宁</a></p></td>
<td><p><a href="../Page/林嘉妮.md" title="wikilink">林嘉妮</a></p></td>
<td><p>|<a href="../Page/吴妍艳.md" title="wikilink">吴妍艳</a></p></td>
<td><p>马舒鸾</p></td>
<td><p><a href="../Page/丘涴鋌.md" title="wikilink">丘涴鋌</a></p></td>
<td><p>第四名: 连碧鸾</p>
<p>第五名: 郑淑萍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2008年</strong></p></td>
<td><p><a href="../Page/谭嘉丽.md" title="wikilink">谭嘉丽</a></p></td>
<td><p><a href="../Page/林绿郁.md" title="wikilink">林绿郁</a></p></td>
<td><p><a href="../Page/林丽娜.md" title="wikilink">林丽娜</a></p></td>
<td><p><a href="../Page/吴诗敏.md" title="wikilink">吴诗敏</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/林绿郁.md" title="wikilink">林绿郁</a></p></td>
<td><p>邱慧姗</p></td>
<td><p><a href="../Page/谭嘉丽.md" title="wikilink">谭嘉丽</a></p></td>
<td><p>第四名: 钟佳伶</p>
<p>第五名: 陈婉倩</p>
<p>iFEEL网上人气大奖: 梁君美</p></td>
</tr>
<tr class="odd">
<td><p><strong>2009年</strong></p></td>
<td><p><a href="../Page/陈美妤.md" title="wikilink">陈美妤</a></p></td>
<td><p><a href="../Page/何蒉町.md" title="wikilink">何蒉町</a></p></td>
<td><p><a href="../Page/陈爱华.md" title="wikilink">陈爱华</a></p></td>
<td><p><a href="../Page/何蒉町.md" title="wikilink">何蒉町</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/陈美妤.md" title="wikilink">陈美妤</a></p></td>
<td><p>杨淑仪</p></td>
<td><p><a href="../Page/陈爱华.md" title="wikilink">陈爱华</a></p></td>
<td><p>第4名: 陀韵妮</p>
<p>第5名: 郭俐伶</p>
<p>SMS我最喜爱佳丽: 郭俐伶</p></td>
</tr>
<tr class="even">
<td><p><strong>2010年</strong></p></td>
<td><p><a href="../Page/叶伊秀.md" title="wikilink">叶伊秀</a></p></td>
<td><p><a href="../Page/郑雁涵.md" title="wikilink">郑雁涵</a></p></td>
<td><p><a href="../Page/张惠虹.md" title="wikilink">张惠虹</a></p></td>
<td><p><a href="../Page/钟佳恩.md" title="wikilink">钟佳恩</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/郑雁涵.md" title="wikilink">郑雁涵</a></p></td>
<td><p>陈家琪</p></td>
<td><p><a href="../Page/叶伊秀.md" title="wikilink">叶伊秀</a></p></td>
<td><p>第4名: 陈家琪</p>
<p>第5名: <a href="../Page/钟佳恩.md" title="wikilink">钟佳恩</a></p>
<p>SMS我最喜爱佳丽: <a href="../Page/钟佳恩.md" title="wikilink">钟佳恩</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2011年</strong></p></td>
<td><p><a href="../Page/林家冰.md" title="wikilink">林家冰</a></p></td>
<td><p><a href="../Page/黄慧洁.md" title="wikilink">黄慧洁</a></p></td>
<td><p><a href="../Page/张祖儿.md" title="wikilink">张祖儿</a></p></td>
<td><p><a href="../Page/林家冰.md" title="wikilink">林家冰</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/黄慧洁.md" title="wikilink">黄慧洁</a></p></td>
<td><p>郑念诗</p></td>
<td><p><a href="../Page/张祖儿.md" title="wikilink">张祖儿</a></p></td>
<td><p>殿軍：<a href="../Page/温慧茵.md" title="wikilink">温慧茵</a><br />
第5名：<a href="../Page/郑淑樱.md" title="wikilink">郑淑樱</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2012年</strong></p></td>
<td><p><a href="../Page/陈楚寰.md" title="wikilink">陈楚寰</a></p></td>
<td><p><a href="../Page/张佩晶.md" title="wikilink">张佩晶</a></p></td>
<td><p><a href="../Page/萧美婷.md" title="wikilink">萧美婷</a></p></td>
<td><p><a href="../Page/陈楚寰.md" title="wikilink">陈楚寰</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/萧美婷.md" title="wikilink">萧美婷</a></p></td>
<td><p>张伊宁</p></td>
<td><p><a href="../Page/陈楚寰.md" title="wikilink">陈楚寰</a></p></td>
<td><p>第4名：<a href="../Page/李蕙伶.md" title="wikilink">李蕙伶</a> 第5名：<a href="../Page/陈翠媚.md" title="wikilink">陈翠媚</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2013年</strong></p></td>
<td><p><a href="../Page/黄之豫.md" title="wikilink">黄之豫</a></p></td>
<td><p><a href="../Page/吴胤婷.md" title="wikilink">吴胤婷</a></p></td>
<td><p><a href="../Page/陈秋萍.md" title="wikilink">陈秋萍</a></p></td>
<td><p><a href="../Page/黄之豫.md" title="wikilink">黄之豫</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/黄之豫.md" title="wikilink">黄之豫</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/王钶媃.md" title="wikilink">王钶媃</a></p></td>
<td><p>第4名: <a href="../Page/王钶媃.md" title="wikilink">王钶媃</a><br />
第5名: <a href="../Page/叶小庆.md" title="wikilink">叶小庆</a><br />
最佳笑容: <a href="../Page/颜慧芬.md" title="wikilink">颜慧芬</a><br />
最佳人氣女神獎: <a href="../Page/黄之豫.md" title="wikilink">黄之豫</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2014年</strong></p></td>
<td><p><a href="../Page/许愫恩.md" title="wikilink">许愫恩</a></p></td>
<td><p><a href="../Page/郭秀文.md" title="wikilink">郭秀文</a></p></td>
<td><p><a href="../Page/钟欣燕.md" title="wikilink">钟欣燕</a></p></td>
<td><p><a href="../Page/许愫恩.md" title="wikilink">许愫恩</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/许愫恩.md" title="wikilink">许愫恩</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/余晓薇.md" title="wikilink">余晓薇</a></p></td>
<td><p>第4名: <a href="../Page/吴为媚.md" title="wikilink">吴为媚</a><br />
第5名: <a href="../Page/黄潤媚.md" title="wikilink">黄潤媚</a><br />
最佳艳发奖: <a href="../Page/许愫恩.md" title="wikilink">许愫恩</a><br />
最佳人氣獎: <a href="../Page/许愫恩.md" title="wikilink">许愫恩</a><br />
LifeTV网络投选我最喜爱佳丽獎: <a href="../Page/许愫恩.md" title="wikilink">许愫恩</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2015年</strong></p></td>
<td><p><a href="../Page/鍾素敏.md" title="wikilink">鍾素敏</a></p></td>
<td><p><a href="../Page/魏欣宜.md" title="wikilink">魏欣宜</a></p></td>
<td><p><a href="../Page/陈赐倪.md" title="wikilink">陈赐倪</a></p></td>
<td><p><a href="../Page/魏欣宜.md" title="wikilink">魏欣宜</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/鍾素敏.md" title="wikilink">鍾素敏</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>殿军：<a href="../Page/颜韵芝.md" title="wikilink">颜韵芝</a> 第5名：<a href="../Page/林淑裕.md" title="wikilink">林淑裕</a></p>
<p>全民女神: <a href="../Page/李星仪.md" title="wikilink">李星仪</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2016年</strong></p></td>
<td><p><a href="../Page/林宣妤.md" title="wikilink">林宣妤</a></p></td>
<td><p><a href="../Page/程爱玲.md" title="wikilink">程爱玲</a></p></td>
<td><p><a href="../Page/徐苑绮.md" title="wikilink">徐苑绮</a></p></td>
<td><p><a href="../Page/林宣妤.md" title="wikilink">林宣妤</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/程爱玲.md" title="wikilink">程爱玲</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>殿軍：<a href="../Page/苏凯璇.md" title="wikilink">苏凯璇</a> 全民女神: <a href="../Page/林宣妤.md" title="wikilink">林宣妤</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2017年</strong></p></td>
<td><p><a href="../Page/陈玉娥_(馬來西亞).md" title="wikilink">陈玉娥</a></p></td>
<td><p><a href="../Page/郑亦庭.md" title="wikilink">郑亦庭</a></p></td>
<td><p><a href="../Page/彭静盈.md" title="wikilink">彭静盈</a></p></td>
<td><p><a href="../Page/彭静盈.md" title="wikilink">彭静盈</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/郑亦庭.md" title="wikilink">郑亦庭</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/郑亦庭.md" title="wikilink">郑亦庭</a></p></td>
<td><p>殿軍: <a href="../Page/宋紫薇.md" title="wikilink">宋紫薇</a><br />
光彩女神: <a href="../Page/刘洁琳.md" title="wikilink">刘洁琳</a><br />
人气女神: <a href="../Page/彭静盈.md" title="wikilink">彭静盈</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2018年</strong></p></td>
<td><p><a href="../Page/彭嘉伊.md" title="wikilink">彭嘉伊</a></p></td>
<td><p><a href="../Page/张吟.md" title="wikilink">张吟</a></p></td>
<td><p><a href="../Page/刘恺欣.md" title="wikilink">刘恺欣</a></p></td>
<td><p><a href="../Page/彭嘉伊.md" title="wikilink">彭嘉伊</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/彭嘉伊.md" title="wikilink">彭嘉伊</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>人气女神: <a href="../Page/彭嘉伊.md" title="wikilink">彭嘉伊</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参选名单

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>01</strong></p></td>
<td><p><strong>02</strong></p></td>
<td><p><strong>03</strong></p></td>
<td><p><strong>04</strong></p></td>
<td><p><strong>05</strong></p></td>
<td><p><strong>06</strong></p></td>
<td><p><strong>07</strong></p></td>
<td><p><strong>08</strong></p></td>
<td><p><strong>09</strong></p></td>
<td><p><strong>10</strong></p></td>
<td><p><strong>11</strong></p></td>
<td><p><strong>12</strong></p></td>
<td><p><strong>其他编号/附注</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>1998年</strong></p></td>
<td><p><a href="../Page/陈美新.md" title="wikilink">陈美新</a></p></td>
<td><p><a href="../Page/刘雪玲.md" title="wikilink">刘雪玲</a></p></td>
<td><p><a href="../Page/李亦甄.md" title="wikilink">李亦甄</a></p></td>
<td><p><a href="../Page/陈慧莹.md" title="wikilink">陈慧莹</a></p></td>
<td><p><a href="../Page/谢袁玲.md" title="wikilink">谢袁玲</a></p></td>
<td><p><a href="../Page/黄业慧.md" title="wikilink">黄业慧</a></p></td>
<td><p><a href="../Page/王文莉.md" title="wikilink">王文莉</a></p></td>
<td><p><a href="../Page/陈英丽.md" title="wikilink">陈英丽</a></p></td>
<td><p><a href="../Page/吴天瑜.md" title="wikilink">吴天瑜</a></p></td>
<td><p><a href="../Page/王嫣.md" title="wikilink">王嫣</a></p></td>
<td><p><a href="../Page/魏美恩.md" title="wikilink">魏美恩</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1999年</strong></p></td>
<td><p><a href="../Page/李碧霞.md" title="wikilink">李碧霞</a></p></td>
<td><p><a href="../Page/潘秀燕.md" title="wikilink">潘秀燕</a></p></td>
<td><p><a href="../Page/何赞妮.md" title="wikilink">何赞妮</a></p></td>
<td><p><a href="../Page/培乐.md" title="wikilink">培乐</a></p></td>
<td><p><a href="../Page/刘欣欣.md" title="wikilink">刘欣欣</a></p></td>
<td><p><a href="../Page/黄秀贞.md" title="wikilink">黄秀贞</a></p></td>
<td><p><a href="../Page/王婷婷.md" title="wikilink">王婷婷</a></p></td>
<td><p><a href="../Page/黄晓妮.md" title="wikilink">黄晓妮</a></p></td>
<td><p><a href="../Page/苏滢萍.md" title="wikilink">苏滢萍</a></p></td>
<td><p><a href="../Page/潘依姗.md" title="wikilink">潘依姗</a></p></td>
<td><p><a href="../Page/林韵删.md" title="wikilink">林韵删</a></p></td>
<td><p><a href="../Page/苏贞.md" title="wikilink">苏贞</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2000年</strong></p></td>
<td><p><a href="../Page/刘秀娟.md" title="wikilink">刘秀娟</a></p></td>
<td><p><a href="../Page/刘淑娟.md" title="wikilink">刘淑娟</a></p></td>
<td><p><a href="../Page/颜辉雯.md" title="wikilink">颜辉雯</a></p></td>
<td><p><a href="../Page/林佩盈.md" title="wikilink">林佩盈</a></p></td>
<td><p><a href="../Page/杨淑玲.md" title="wikilink">杨淑玲</a></p></td>
<td><p><a href="../Page/杨婷慧.md" title="wikilink">杨婷慧</a></p></td>
<td><p><a href="../Page/刘倩函.md" title="wikilink">刘倩函</a></p></td>
<td><p><a href="../Page/陈秀云.md" title="wikilink">陈秀云</a></p></td>
<td><p><a href="../Page/廖仁绵.md" title="wikilink">廖仁绵</a></p></td>
<td><p><a href="../Page/刘雨温.md" title="wikilink">刘雨温</a></p></td>
<td><p><a href="../Page/邱晓雁.md" title="wikilink">邱晓雁</a></p></td>
<td><p><a href="../Page/陈晓翠.md" title="wikilink">陈晓翠</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2001年</strong></p></td>
<td><p><a href="../Page/洪立仪.md" title="wikilink">洪立仪</a></p></td>
<td><p><a href="../Page/周艳萍.md" title="wikilink">周艳萍</a></p></td>
<td><p><a href="../Page/郑燕燕.md" title="wikilink">郑燕燕</a></p></td>
<td><p><a href="../Page/黄春霞.md" title="wikilink">黄春霞</a></p></td>
<td><p><a href="../Page/梅钏婷.md" title="wikilink">梅钏婷</a></p></td>
<td><p><a href="../Page/阮丽仪.md" title="wikilink">阮丽仪</a></p></td>
<td><p><a href="../Page/方丽丽.md" title="wikilink">方丽丽</a></p></td>
<td><p><a href="../Page/符愫娱.md" title="wikilink">符愫娱</a></p></td>
<td><p><a href="../Page/李芝忆.md" title="wikilink">李芝忆</a></p></td>
<td><p><a href="../Page/潘铃烟.md" title="wikilink">潘铃烟</a></p></td>
<td><p><a href="../Page/曹诗萍.md" title="wikilink">曹诗萍</a></p></td>
<td><p><a href="../Page/陈丽霞.md" title="wikilink">陈丽霞</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2002年</strong></p></td>
<td><p><a href="../Page/刘家霓.md" title="wikilink">刘家霓</a></p></td>
<td><p><a href="../Page/林思佳.md" title="wikilink">林思佳</a></p></td>
<td><p><a href="../Page/郑秀珍.md" title="wikilink">郑秀珍</a></p></td>
<td><p><a href="../Page/周甄美.md" title="wikilink">周甄美</a></p></td>
<td><p><a href="../Page/赖小玉.md" title="wikilink">赖小玉</a></p></td>
<td><p><a href="../Page/赖佩芳.md" title="wikilink">赖佩芳</a></p></td>
<td><p><a href="../Page/鲍欣瑜.md" title="wikilink">鲍欣瑜</a></p></td>
<td><p><a href="../Page/陈泳锦.md" title="wikilink">陈泳锦</a></p></td>
<td><p><a href="../Page/杨淑婷.md" title="wikilink">杨淑婷</a></p></td>
<td><p><a href="../Page/陈玲珑.md" title="wikilink">陈玲珑</a></p></td>
<td><p><a href="../Page/吴爱仪.md" title="wikilink">吴爱仪</a></p></td>
<td><p><a href="../Page/陈思俐.md" title="wikilink">陈思俐</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2003年</strong></p></td>
<td><p><a href="../Page/黄晓韵.md" title="wikilink">黄晓韵</a></p></td>
<td><p><a href="../Page/李美雪.md" title="wikilink">李美雪</a></p></td>
<td><p><a href="../Page/林美玲.md" title="wikilink">林美玲</a></p></td>
<td><p><a href="../Page/董芯芯.md" title="wikilink">董芯芯</a></p></td>
<td><p><a href="../Page/陈丽娜.md" title="wikilink">陈丽娜</a></p></td>
<td><p><a href="../Page/方慧敏.md" title="wikilink">方慧敏</a></p></td>
<td><p><a href="../Page/唐慧珍.md" title="wikilink">唐慧珍</a></p></td>
<td><p><a href="../Page/孙雪娟.md" title="wikilink">孙雪娟</a></p></td>
<td><p><a href="../Page/陈盈晖.md" title="wikilink">陈盈晖</a></p></td>
<td><p><a href="../Page/刘丽君.md" title="wikilink">刘丽君</a></p></td>
<td><p><a href="../Page/杨秀惠.md" title="wikilink">杨秀惠</a></p></td>
<td><p><a href="../Page/伍曼盈.md" title="wikilink">伍曼盈</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2004年</strong></p></td>
<td><p><a href="../Page/陈苇珍.md" title="wikilink">陈苇珍</a></p></td>
<td><p><a href="../Page/秦雯彬.md" title="wikilink">秦雯彬</a></p></td>
<td><p><a href="../Page/陈瑞琳.md" title="wikilink">陈瑞琳</a></p></td>
<td><p><a href="../Page/房淑慧.md" title="wikilink">房淑慧</a></p></td>
<td><p><a href="../Page/陈影雯.md" title="wikilink">陈影雯</a></p></td>
<td><p><a href="../Page/彭雪菁.md" title="wikilink">彭雪菁</a></p></td>
<td><p><a href="../Page/陈莹卿.md" title="wikilink">陈莹卿</a></p></td>
<td><p><a href="../Page/王文娟.md" title="wikilink">王文娟</a></p></td>
<td><p><a href="../Page/谢德容.md" title="wikilink">谢德容</a></p></td>
<td><p><a href="../Page/朱宛燕.md" title="wikilink">朱宛燕</a></p></td>
<td><p><a href="../Page/覃媲媛.md" title="wikilink">覃媲媛</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2005年</strong></p></td>
<td><p><a href="../Page/曾燕平.md" title="wikilink">曾燕平</a></p></td>
<td><p><a href="../Page/林佩琦.md" title="wikilink">林佩琦</a></p></td>
<td><p><a href="../Page/林淑玲.md" title="wikilink">林淑玲</a></p></td>
<td><p><a href="../Page/戴淑怡.md" title="wikilink">戴淑怡</a></p></td>
<td><p><a href="../Page/林岫英.md" title="wikilink">林岫英</a></p></td>
<td><p><a href="../Page/郑美念.md" title="wikilink">郑美念</a></p></td>
<td><p><a href="../Page/林苑君.md" title="wikilink">林苑君</a></p></td>
<td><p><a href="../Page/李彩霞.md" title="wikilink">李彩霞</a></p></td>
<td><p><a href="../Page/黃绣霜.md" title="wikilink">黃绣霜</a></p></td>
<td><p><a href="../Page/江佩莹.md" title="wikilink">江佩莹</a></p></td>
<td><p><a href="../Page/丘贤慧.md" title="wikilink">丘贤慧</a></p></td>
<td><p><a href="../Page/吕思艺.md" title="wikilink">吕思艺</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2006年</strong></p></td>
<td><p><a href="../Page/黃意晶.md" title="wikilink">黃意晶</a></p></td>
<td></td>
<td><p><a href="../Page/方美施.md" title="wikilink">方美施</a></p></td>
<td><p><a href="../Page/林美伶.md" title="wikilink">林美伶</a></p></td>
<td><p><a href="../Page/吳俐旋.md" title="wikilink">吳俐旋</a></p></td>
<td><p><a href="../Page/潘姵鸰.md" title="wikilink">潘姵鸰</a></p></td>
<td><p><a href="../Page/赖颖琪.md" title="wikilink">赖颖琪</a></p></td>
<td><p><a href="../Page/林秀婷.md" title="wikilink">林秀婷</a></p></td>
<td><p><a href="../Page/倪于婷.md" title="wikilink">倪于婷</a></p></td>
<td><p><a href="../Page/赵攸真.md" title="wikilink">赵攸真</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2007年</strong></p></td>
<td><p><a href="../Page/郑淑萍.md" title="wikilink">郑淑萍</a></p></td>
<td><p><a href="../Page/尤凤音.md" title="wikilink">尤凤音</a></p></td>
<td><p><a href="../Page/潘碧宁.md" title="wikilink">潘碧宁</a></p></td>
<td><p><a href="../Page/连碧鸾.md" title="wikilink">连碧鸾</a></p></td>
<td><p><a href="../Page/马舒鸾.md" title="wikilink">马舒鸾</a></p></td>
<td><p><a href="../Page/林嘉妮.md" title="wikilink">林嘉妮</a></p></td>
<td><p><a href="../Page/吕淑嫔.md" title="wikilink">吕淑嫔</a></p></td>
<td><p><a href="../Page/吴妍艳.md" title="wikilink">吴妍艳</a></p></td>
<td><p><a href="../Page/汪巧斌.md" title="wikilink">汪巧斌</a></p></td>
<td><p><a href="../Page/丘涴铤.md" title="wikilink">丘涴铤</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2008年</strong></p></td>
<td><p><a href="../Page/林绿郁.md" title="wikilink">林绿郁</a></p></td>
<td><p><a href="../Page/林嘉雯.md" title="wikilink">林嘉雯</a></p></td>
<td><p><a href="../Page/林丽娜.md" title="wikilink">林丽娜</a></p></td>
<td><p><a href="../Page/钟佳伶.md" title="wikilink">钟佳伶</a></p></td>
<td><p><a href="../Page/邱慧珊.md" title="wikilink">邱慧珊</a></p></td>
<td><p><a href="../Page/梁君美.md" title="wikilink">梁君美</a></p></td>
<td><p><a href="../Page/陈婉倩.md" title="wikilink">陈婉倩</a></p></td>
<td><p><a href="../Page/吴诗敏.md" title="wikilink">吴诗敏</a></p></td>
<td><p><a href="../Page/黃淑平.md" title="wikilink">黃淑平</a></p></td>
<td><p><a href="../Page/谭嘉丽.md" title="wikilink">谭嘉丽</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2009年</strong></p></td>
<td><p><a href="../Page/陈玉薇.md" title="wikilink">陈玉薇</a></p></td>
<td><p><a href="../Page/陀韵妮.md" title="wikilink">陀韵妮</a></p></td>
<td><p><a href="../Page/郭俐伶.md" title="wikilink">郭俐伶</a></p></td>
<td><p><a href="../Page/全莉芳.md" title="wikilink">全莉芳</a></p></td>
<td><p><a href="../Page/杨淑怡.md" title="wikilink">杨淑怡</a></p></td>
<td><p><a href="../Page/杨琇灵.md" title="wikilink">杨琇灵</a></p></td>
<td><p><a href="../Page/叶宝励.md" title="wikilink">叶宝励</a></p></td>
<td><p><a href="../Page/何蕢町.md" title="wikilink">何蕢町</a></p></td>
<td><p><a href="../Page/陈爱华.md" title="wikilink">陈爱华</a></p></td>
<td><p><a href="../Page/陈美妤.md" title="wikilink">陈美妤</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2010年</strong></p></td>
<td><p><a href="../Page/张惠虹.md" title="wikilink">张惠虹</a></p></td>
<td><p><a href="../Page/王臆阳.md" title="wikilink">王臆阳</a></p></td>
<td><p><a href="../Page/陈敏仪(2010年astro华姐).md" title="wikilink">陈敏仪</a></p></td>
<td><p><a href="../Page/钟佳恩.md" title="wikilink">钟佳恩</a></p></td>
<td><p><a href="../Page/郑雁涵.md" title="wikilink">郑雁涵</a></p></td>
<td><p><a href="../Page/陈滇金.md" title="wikilink">陈滇金</a></p></td>
<td><p><a href="../Page/陈家琪(2010年astro华姐).md" title="wikilink">陈家琪</a></p></td>
<td><p><a href="../Page/叶伊秀.md" title="wikilink">叶伊秀</a></p></td>
<td><p><a href="../Page/黄雪彬.md" title="wikilink">黄雪彬</a></p></td>
<td><p><a href="../Page/江巧儿.md" title="wikilink">江巧儿</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2011年</strong></p></td>
<td><p><a href="../Page/郑淑樱.md" title="wikilink">郑淑樱</a></p></td>
<td><p><a href="../Page/张祖儿.md" title="wikilink">张祖儿</a></p></td>
<td><p><a href="../Page/刘诗荟.md" title="wikilink">刘诗荟</a></p></td>
<td><p><a href="../Page/郑念诗.md" title="wikilink">郑念诗</a></p></td>
<td><p><a href="../Page/黄秋萍.md" title="wikilink">黄秋萍</a></p></td>
<td><p><a href="../Page/蔡香琴.md" title="wikilink">蔡香琴</a></p></td>
<td><p><a href="../Page/林家冰.md" title="wikilink">林家冰</a></p></td>
<td><p><a href="../Page/黄慧洁.md" title="wikilink">黄慧洁</a></p></td>
<td><p><a href="../Page/温慧茵.md" title="wikilink">温慧茵</a></p></td>
<td><p><a href="../Page/林蔚妏.md" title="wikilink">林蔚妏</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2012年</strong></p></td>
<td><p><a href="../Page/黄彦.md" title="wikilink">黄彦</a></p></td>
<td><p><a href="../Page/张琪玮.md" title="wikilink">张琪玮</a></p></td>
<td><p><a href="../Page/郑淑茹.md" title="wikilink">郑淑茹</a></p></td>
<td><p><a href="../Page/张伊宁.md" title="wikilink">张伊宁</a></p></td>
<td><p><a href="../Page/陈翠媚.md" title="wikilink">陈翠媚</a></p></td>
<td><p><a href="../Page/陈楚寰.md" title="wikilink">陈楚寰</a></p></td>
<td><p><a href="../Page/李蕙伶.md" title="wikilink">李蕙伶</a></p></td>
<td><p><a href="../Page/郑佩溢.md" title="wikilink">郑佩溢</a></p></td>
<td><p><a href="../Page/萧美婷.md" title="wikilink">萧美婷</a></p></td>
<td><p><a href="../Page/张佩晶.md" title="wikilink">张佩晶</a></p></td>
<td><p><a href="../Page/方芮芊.md" title="wikilink">方芮芊</a></p></td>
<td><p><a href="../Page/冯意玲.md" title="wikilink">冯意玲</a></p></td>
<td><p>复活赛翻身佳丽：<a href="../Page/方芮芊.md" title="wikilink">方芮芊</a>、<a href="../Page/冯意玲.md" title="wikilink">冯意玲</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2013年</strong></p></td>
<td><p><a href="../Page/黄美莲.md" title="wikilink">黄美莲</a></p></td>
<td><p><a href="../Page/吴胤婷.md" title="wikilink">吴胤婷</a></p></td>
<td><p><a href="../Page/黄之豫.md" title="wikilink">黄之豫</a></p></td>
<td><p><a href="../Page/叶小庆.md" title="wikilink">叶小庆</a></p></td>
<td><p><a href="../Page/颜慧芬.md" title="wikilink">颜慧芬</a></p></td>
<td><p><a href="../Page/陈玢奾.md" title="wikilink">陈玢奾</a></p></td>
<td><p><a href="../Page/何丽莎.md" title="wikilink">何丽莎</a></p></td>
<td><p><a href="../Page/王钶媃.md" title="wikilink">王钶媃</a></p></td>
<td><p><a href="../Page/陈秋萍.md" title="wikilink">陈秋萍</a></p></td>
<td><p><a href="../Page/陈美婷.md" title="wikilink">陈美婷</a></p></td>
<td></td>
<td></td>
<td><p>十二强：<a href="../Page/吴胤婷.md" title="wikilink">吴胤婷</a>、<a href="../Page/叶小庆.md" title="wikilink">叶小庆</a><br />
十五强：<a href="../Page/颜慧芬.md" title="wikilink">颜慧芬</a>、<a href="../Page/陈玢奾.md" title="wikilink">陈玢奾</a>、<a href="../Page/陈美婷.md" title="wikilink">陈美婷</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2014年</strong></p></td>
<td><p><a href="../Page/余晓薇.md" title="wikilink">余晓薇</a></p></td>
<td><p><a href="../Page/朱婉婷.md" title="wikilink">朱婉婷</a></p></td>
<td><p><a href="../Page/陈玤妤.md" title="wikilink">陈玤妤</a></p></td>
<td><p><a href="../Page/郭秀文.md" title="wikilink">郭秀文</a></p></td>
<td><p><a href="../Page/王淇.md" title="wikilink">王淇</a></p></td>
<td><p><a href="../Page/许愫恩.md" title="wikilink">许愫恩</a></p></td>
<td><p><a href="../Page/钟欣燕.md" title="wikilink">钟欣燕</a></p></td>
<td><p><a href="../Page/吴为媚.md" title="wikilink">吴为媚</a></p></td>
<td><p><a href="../Page/张嘉文.md" title="wikilink">张嘉文</a></p></td>
<td><p><a href="../Page/黄潤媚.md" title="wikilink">黄潤媚</a></p></td>
<td><p><a href="../Page/许楚瑜.md" title="wikilink">许楚瑜</a></p></td>
<td></td>
<td><p>十二强：<a href="../Page/廖慧君.md" title="wikilink">廖慧君</a>、<a href="../Page/刘晓蓓.md" title="wikilink">刘晓蓓</a><br />
十五强：<a href="../Page/朱荟茗.md" title="wikilink">朱荟茗</a>、<a href="../Page/黄思嘉.md" title="wikilink">黄思嘉</a>、<a href="../Page/陈玤妤.md" title="wikilink">陈玤妤</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2015年</strong></p></td>
<td><p><a href="../Page/林淑裕.md" title="wikilink">林淑裕</a></p></td>
<td><p><a href="../Page/张紫燕.md" title="wikilink">张紫燕</a></p></td>
<td><p><a href="../Page/颜韵芝.md" title="wikilink">颜韵芝</a></p></td>
<td><p><a href="../Page/钟素敏.md" title="wikilink">钟素敏</a></p></td>
<td><p><a href="../Page/李星仪.md" title="wikilink">李星仪</a></p></td>
<td><p><a href="../Page/魏欣宜.md" title="wikilink">魏欣宜</a></p></td>
<td><p><a href="../Page/陈赐倪.md" title="wikilink">陈赐倪</a></p></td>
<td><p><a href="../Page/李俞频.md" title="wikilink">李俞频</a></p></td>
<td><p><a href="../Page/陈怡静.md" title="wikilink">陈怡静</a></p></td>
<td><p><a href="../Page/叶维君.md" title="wikilink">叶维君</a></p></td>
<td></td>
<td></td>
<td><p>十五强：<a href="../Page/陈俪今.md" title="wikilink">陈俪今</a>、<a href="../Page/黎雪仪.md" title="wikilink">黎雪仪</a>、<a href="../Page/李佳恩(2015年astro华姐).md" title="wikilink">李佳恩</a>、<a href="../Page/王祖铟.md" title="wikilink">王祖铟</a>、<a href="../Page/陈欣仪.md" title="wikilink">陈欣仪</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2016年</strong></p></td>
<td><p><a href="../Page/林宣妤.md" title="wikilink">林宣妤</a></p></td>
<td><p><a href="../Page/程爱玲.md" title="wikilink">程爱玲</a></p></td>
<td><p><a href="../Page/罗安妮.md" title="wikilink">罗安妮</a></p></td>
<td><p><a href="../Page/苏诗僡.md" title="wikilink">苏诗僡</a></p></td>
<td><p><a href="../Page/王钰美.md" title="wikilink">王钰美</a></p></td>
<td><p><a href="../Page/苏凯璇.md" title="wikilink">苏凯璇</a></p></td>
<td><p><a href="../Page/蔡咏彬.md" title="wikilink">蔡咏彬</a></p></td>
<td><p><a href="../Page/徐苑绮.md" title="wikilink">徐苑绮</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2017年</strong></p></td>
<td><p><a href="../Page/郑亦庭.md" title="wikilink">郑亦庭</a></p></td>
<td><p><a href="../Page/彭静盈.md" title="wikilink">彭静盈</a></p></td>
<td><p><a href="../Page/宋紫薇.md" title="wikilink">宋紫薇</a></p></td>
<td><p><a href="../Page/陈咏诗(2017年astro华姐).md" title="wikilink">陈咏诗</a></p></td>
<td><p><a href="../Page/刘洁莹.md" title="wikilink">刘洁莹</a></p></td>
<td><p><a href="../Page/陳馷佳.md" title="wikilink">陳馷佳</a></p></td>
<td><p><a href="../Page/劉潔琳.md" title="wikilink">劉潔琳</a></p></td>
<td><p><a href="../Page/陳玉娥_(馬來西亞).md" title="wikilink">陳玉娥</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2018年</strong></p></td>
<td><p><a href="../Page/陈舒君.md" title="wikilink">陈舒君</a></p></td>
<td><p><a href="../Page/刘嘉怡.md" title="wikilink">刘嘉怡</a></p></td>
<td><p><a href="../Page/刘恺欣.md" title="wikilink">刘恺欣</a></p></td>
<td><p><a href="../Page/叶津妗.md" title="wikilink">叶津妗</a></p></td>
<td><p><a href="../Page/彭嘉伊.md" title="wikilink">彭嘉伊</a></p></td>
<td><p><a href="../Page/李格莹.md" title="wikilink">李格莹</a></p></td>
<td><p><a href="../Page/李诗琦.md" title="wikilink">李诗琦</a></p></td>
<td><p><a href="../Page/张吟.md" title="wikilink">张吟</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 國際華裔小姐(1999－2006)／ 國際中華小姐(2007－現在)

冠军得主会代表[马来西亚參加](../Page/马来西亚.md "wikilink")[TVB举办的](../Page/TVB.md "wikilink")[国际中华小姐竞选](../Page/国际中华小姐竞选.md "wikilink")，与來自世界不同地区的佳丽争妍斗丽。

|           |                                  |                        |
| --------- | -------------------------------- | ---------------------- |
| **年份**    | **代表马来西亚**                       | **奖项及附注**              |
| **1999年** | [吴天瑜](../Page/吴天瑜.md "wikilink") | 五强之一                   |
| **2000年** | [何赞妮](../Page/何赞妮.md "wikilink") | \-                     |
| **2001年** | [刘晓萍](../Page/刘晓萍.md "wikilink") | 五强之一                   |
|           |                                  |                        |
| **2002年** | [符愫娱](../Page/符愫娱.md "wikilink") | 季军                     |
|           |                                  |                        |
| **2003年** | [陈泳锦](../Page/陈泳锦.md "wikilink") | **冠军**                 |
| **2004年** | [杨秀惠](../Page/杨秀惠.md "wikilink") | 五强之一                   |
| **2005年** | [陈影雯](../Page/陈影雯.md "wikilink") | 友谊小姐                   |
|           |                                  |                        |
| **2006年** | [江佩莹](../Page/江佩莹.md "wikilink") | 季军, 友谊小姐               |
| **2007年** | [林秀婷](../Page/林秀婷.md "wikilink") | \-                     |
| **2008年** | [尤凤音](../Page/尤凤音.md "wikilink") | \-                     |
| **2009年** | [谭嘉丽](../Page/谭嘉丽.md "wikilink") | \-                     |
|           |                                  |                        |
| **2010年** | [陈美妤](../Page/陈美妤.md "wikilink") | 亚军, 中华俪影小姐             |
| **2012年** | [叶伊秀](../Page/叶伊秀.md "wikilink") | \-                     |
|           |                                  |                        |
| **2012年** | [林家冰](../Page/林家冰.md "wikilink") | 季军, 大都会魅力小姐, 亚太区时尚风采大奖 |
|           |                                  |                        |
| **2013年** | [陈楚寰](../Page/陈楚寰.md "wikilink") | 亚军                     |
| **2014年** | [黄之豫](../Page/黄之豫.md "wikilink") | 五强之一                   |
| **2015年** | [许愫恩](../Page/许愫恩.md "wikilink") | 五强之一, 最佳民族服装演绎         |
| **2016年** | [鍾素敏](../Page/鍾素敏.md "wikilink") | \-                     |
|           |                                  |                        |
| **2017年** | [林宣妤](../Page/林宣妤.md "wikilink") | 亞軍, 馬來西亞風情大獎           |
| **2018年** | [郑亦庭](../Page/郑亦庭.md "wikilink") | \-                     |
| **2019年** | [彭嘉伊](../Page/彭嘉伊.md "wikilink") | \-                     |
|           |                                  |                        |

## 历届司仪及嘉宾

|           |                                    |                                                                                                                                                                                  |                                                                                                                                                                                    |
| --------- | ---------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **日期**    | **地点**                             | **司仪**                                                                                                                                                                           | 评审                                                                                                                                                                                 |
| **1998年** |                                    | [王志强](../Page/王志强.md "wikilink")、[丘采桦](../Page/丘采桦.md "wikilink")                                                                                                                | [年少](../Page/年少.md "wikilink")                                                                                                                                                     |
| **1999年** |                                    |                                                                                                                                                                                  |                                                                                                                                                                                    |
| **2000年** | [云顶云星剧场](../Page/云顶.md "wikilink") |                                                                                                                                                                                  | [陈妙瑛](../Page/陈妙瑛.md "wikilink")                                                                                                                                                   |
| **2001年** | 云顶云星剧场                             | [窦文涛](../Page/窦文涛.md "wikilink")、[郭燕燕](../Page/郭燕燕.md "wikilink")                                                                                                                | [任达华](../Page/任达华.md "wikilink")                                                                                                                                                   |
| **2002年** | 云顶云星剧场                             | [胡一虎](../Page/胡一虎.md "wikilink")、[郭燕燕](../Page/郭燕燕.md "wikilink")                                                                                                                |                                                                                                                                                                                    |
| **2003年** | 云顶云星剧场                             |                                                                                                                                                                                  |                                                                                                                                                                                    |
| **2004年** | 云顶云星剧场                             |                                                                                                                                                                                  |                                                                                                                                                                                    |
| **2005年** | 双威度假酒店                             | [谷德昭](../Page/谷德昭.md "wikilink")、[林佩盈](../Page/林佩盈.md "wikilink")                                                                                                                | [邓萃雯](../Page/邓萃雯.md "wikilink")                                                                                                                                                   |
| **2006年** | 云顶国际歌剧院                            | [林德荣](../Page/林德荣.md "wikilink")、[郭燕燕](../Page/郭燕燕.md "wikilink")、[谢德蓉](../Page/谢德蓉.md "wikilink")、[秦雯彬](../Page/秦雯彬.md "wikilink")                                              |                                                                                                                                                                                    |
| **2007年** | 云顶国际歌剧院                            | [谷德昭](../Page/谷德昭.md "wikilink")、[吴天瑜](../Page/吴天瑜.md "wikilink")                                                                                                                |                                                                                                                                                                                    |
| **2008年** | 云顶云星剧场                             | [林德荣](../Page/林德荣.md "wikilink")、[王翠玲](../Page/王翠玲.md "wikilink")、[陈泳锦](../Page/陈泳锦.md "wikilink")                                                                               |                                                                                                                                                                                    |
| **2009年** | 云顶云星剧场                             | [林德荣](../Page/林德荣.md "wikilink")、[林佩盈](../Page/林佩盈.md "wikilink")                                                                                                                |                                                                                                                                                                                    |
| **2010年** | 双威水上乐园人造沙滩                         | [林德荣](../Page/林德荣.md "wikilink")、[林佩盈](../Page/林佩盈.md "wikilink")                                                                                                                |                                                                                                                                                                                    |
| **2011年** | 云顶云星剧场                             | [林德荣](../Page/林德荣.md "wikilink")、[郑靖婷](../Page/郑靖婷.md "wikilink")                                                                                                                | [詹瑞文](../Page/詹瑞文.md "wikilink")                                                                                                                                                   |
| **2012年** | 云顶云星剧场                             | [林德荣](../Page/林德荣.md "wikilink")、[郑靖婷](../Page/郑靖婷.md "wikilink")、[陈志康](../Page/陈志康\(My_fm_DJ\).md "wikilink")、[颜薇恩](../Page/颜薇恩.md "wikilink")、[温慧茵](../Page/温慧茵.md "wikilink") | [伍咏薇](../Page/伍咏薇.md "wikilink")                                                                                                                                                   |
| **2013年** | Zebra Square Kuala Lumpur          | [陈浩然](../Page/陈浩然\(My_fm_DJ\).md "wikilink")、[颜薇恩](../Page/颜薇恩.md "wikilink")、[温慧茵](../Page/温慧茵.md "wikilink")、[林家冰](../Page/林家冰.md "wikilink")、[李惠伶](../Page/李惠伶.md "wikilink") |                                                                                                                                                                                    |
| **2014年** | Mega Star Arena Kuala Lumpur       | [颜江瀚](../Page/颜江瀚.md "wikilink")、[萧慧敏](../Page/萧慧敏.md "wikilink")、[温慧茵](../Page/温慧茵.md "wikilink")                                                                               | [李世平](../Page/李世平.md "wikilink")、[颜慧萍](../Page/颜慧萍.md "wikilink")、[赵洁莹](../Page/赵洁莹.md "wikilink")、[赖淞凤](../Page/赖淞凤.md "wikilink")、[徐凯](../Page/徐凯.md "wikilink")                 |
| **2015年** | Mega Star Arena Kuala Lumpur       | [陈志康](../Page/陈志康\(My_DJ_Royce\).md "wikilink")、[温慧茵](../Page/温慧茵.md "wikilink")                                                                                                 | [Josh Kua 柯信捷](../Page/Josh_Kua_柯信捷.md "wikilink")、[徐凯](../Page/徐凯.md "wikilink")、[Daniel 丹尼尔](../Page/Daniel_丹尼尔.md "wikilink")                                                   |
| **2016年** | Mega Star Arena Kuala Lumpur       | [陈浩然](../Page/陈浩然\(My_fm_DJ\).md "wikilink")、[林盛斌](../Page/林盛斌.md "wikilink")                                                                                                    | [陈凯琳](../Page/陈凯琳.md "wikilink")、[Juwei Teoh](../Page/Juwei_Teoh.md "wikilink")、[吳俐璇](../Page/吳俐璇.md "wikilink")、[林佩盈](../Page/林佩盈.md "wikilink")、[陳泳錦](../Page/陳泳錦.md "wikilink") |
| **2017年** | Menara PGRM                        | [陈浩然](../Page/陈浩然\(My_fm_DJ\).md "wikilink")、[林震前](../Page/林震前.md "wikilink")                                                                                                    | [杜汶泽](../Page/杜汶泽.md "wikilink")、[周柏豪](../Page/周柏豪.md "wikilink")、[赖淞凤](../Page/赖淞凤.md "wikilink")、[钟瑾桦](../Page/钟瑾桦.md "wikilink")                                                |
| **2018年** | 国际贸易与展览中心                          | [陈志康](../Page/陈志康\(My_DJ_Royce\).md "wikilink") 、[萧慧敏](../Page/萧慧敏.md "wikilink")                                                                                                | [李津梅](../Page/李津梅.md "wikilink")、[林德荣](../Page/林德荣.md "wikilink")、[劉心悠](../Page/劉心悠.md "wikilink")、[陈炯江](../Page/陈炯江.md "wikilink")、[杨雁雁](../Page/杨雁雁.md "wikilink")               |
|           |                                    |                                                                                                                                                                                  |                                                                                                                                                                                    |

## 参见

## 外部連結

  - [Astro國際華裔小姐競選 Facebook專頁](https://www.facebook.com/astro.macip)
  - [Astro國際華裔小姐競選 Instagram專頁](https://www.instagram.com/astro.macip/)

[Category:國際中華小姐競選](../Category/國際中華小姐競選.md "wikilink")
[Category:馬來西亞華人社會](../Category/馬來西亞華人社會.md "wikilink")
[Category:女性選美活動](../Category/女性選美活動.md "wikilink")
[Category:华裔选美活动](../Category/华裔选美活动.md "wikilink")
[Category:1998年建立](../Category/1998年建立.md "wikilink")

1.  [【馬來西亞華裔小姐競選】專訪八強佳麗：靚女唔只係得個樣！](https://www.hk01.com/%E7%9F%A5%E6%80%A7%E5%A5%B3%E7%94%9F/235607/%E9%A6%AC%E4%BE%86%E8%A5%BF%E4%BA%9E%E8%8F%AF%E8%A3%94%E5%B0%8F%E5%A7%90%E7%AB%B6%E9%81%B8-%E5%B0%88%E8%A8%AA%E5%85%AB%E5%BC%B7%E4%BD%B3%E9%BA%97-%E9%9D%9A%E5%A5%B3%E5%94%94%E5%8F%AA%E4%BF%82%E5%BE%97%E5%80%8B%E6%A8%A3)