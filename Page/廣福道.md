[Kwong_Fok_Road_view_2018.jpg](https://zh.wikipedia.org/wiki/File:Kwong_Fok_Road_view_2018.jpg "fig:Kwong_Fok_Road_view_2018.jpg")
**廣福道**（）是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[大埔墟的主要道路](../Page/大埔墟.md "wikilink")，東接[大埔公路](../Page/大埔公路.md "wikilink")[元洲仔段](../Page/元洲仔.md "wikilink")，西接[北盛街及跨越](../Page/北盛街.md "wikilink")[林村河的](../Page/林村河.md "wikilink")[廣福橋](../Page/廣福橋.md "wikilink")，而林村河對岸近[大埔政府合署也有一小段掘頭路直入](../Page/大埔政府合署.md "wikilink")[太和邨](../Page/太和邨.md "wikilink")。

廣福道本來是[大埔公路的一部份](../Page/大埔公路.md "wikilink")，西北連接大埔公路大窩段通往上水、粉嶺，東南連接大埔公路元洲仔段，為新界東的交通主幹道，後來被改為現稱。1980年代政府發展[大埔新市鎮時](../Page/大埔新市鎮.md "wikilink")，橫跨林村河的一段行車橋（廣福橋）被拆卸，政府另建[寶鄉橋連接林村河兩岸](../Page/寶鄉橋.md "wikilink")，使廣福道分為兩部份。其後因興建太和邨，而使林村河以北之一段廣福道與大埔公路大窩段分隔開，成為一條掘頭路。

[廣福邨並非位於廣福道一帶](../Page/廣福邨.md "wikilink")，而是位於其東北面的[寶湖道東面盡頭](../Page/寶湖道.md "wikilink")。

廣福道近廣角頭里為鷺鳥家園，屬於具特殊科學價值地點（SSSI），屬全港第二大鷺鳥林，香港觀鳥會曾發現約有151個巢，其中約有58個屬[夜鷺](../Page/夜鷺.md "wikilink")，亦有[小白鷺](../Page/小白鷺.md "wikilink")、[大白鷺在該區築巢](../Page/大白鷺.md "wikilink")。2017年6月，康文署在該處強行剪樹，令棲身樹上的幼鷺鳥從高處墮下傷亡。\[1\]

Kwong Fuk Road west end.jpg|廣福道西端 Kwong Fuk Road eastern
ending.jpg|廣福道東端
TaiPo.JPG|廣福道近[運頭街](../Page/運頭街.md "wikilink")（2005年）

## 著名地點

  - [六鄉新村](../Page/六鄉新村.md "wikilink")
  - [大埔墟四里](../Page/大埔墟四里.md "wikilink")
  - [廣福球場](../Page/廣福球場.md "wikilink")
  - [廣福橋](../Page/廣福橋.md "wikilink")
  - [大埔政府合署](../Page/大埔政府合署.md "wikilink")
  - [太和邨](../Page/太和邨.md "wikilink")
  - [王肇枝中學](../Page/王肇枝中學.md "wikilink")

[Category:大埔區街道](../Category/大埔區街道.md "wikilink")

1.