[Magnolia_liliiflora_Desr.jpg](https://zh.wikipedia.org/wiki/File:Magnolia_liliiflora_Desr.jpg "fig:Magnolia_liliiflora_Desr.jpg")

**辛夷**是[中藥材](../Page/中藥材.md "wikilink")，又名**木筆**、**辛夷花**、**木筆花**、**望春花**，主要是[木蘭科落葉小灌木植物](../Page/木蘭科.md "wikilink")[紫玉兰](../Page/紫玉兰.md "wikilink")（*Magnolia
liliiflora*
Desr.）或[望春玉兰](../Page/望春玉兰.md "wikilink")、[玉兰](../Page/玉兰.md "wikilink")、[武当玉兰的](../Page/武当玉兰.md "wikilink")[花蕾](../Page/花蕾.md "wikilink")，也可以指其他木兰属植物的花蕾。主產於[中国](../Page/中国.md "wikilink")[河南](../Page/河南.md "wikilink")、[陝西](../Page/陝西.md "wikilink")、[四川](../Page/四川.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[湖北](../Page/湖北.md "wikilink")。生用。河南省南召县被誉为“辛夷之乡”。

## 性味歸經

味辛，性微溫，入肺,胃經

## 功效應用

袪[風寒](../Page/風寒.md "wikilink")，通鼻竅：本品辛溫升散，能上行頭面風寒而通鼻竅，用於[感冒風寒所致之鼻塞](../Page/感冒.md "wikilink")、頭痛、尤為治鼻淵之頭痛、鼻塞、不聞香臭，或流濁涕等病的要藥，常配[蒼耳子](../Page/蒼耳子.md "wikilink")、[白芷](../Page/白芷.md "wikilink")、[防風](../Page/防風.md "wikilink")、[藁本等同用](../Page/藁本.md "wikilink")。一般感冒少用。由於本品溫性不大，亦可用於鼻淵偏熱者，多與[蒼耳子](../Page/蒼耳子.md "wikilink")、[薄荷](../Page/薄荷.md "wikilink")、[神農本草經](../Page/黃芩_*亦治齒痛,面腫,瘡毒_*本品能收縮鼻粘膜血管可代麻黃礆_==_用量用法_==_*6-12克_==_禁忌_==_陰虛火盛及汗多忌用_==_參考資料_==_*《神農本草經》_*《中藥方劑學》_==_文獻研究_==_{{refbegin}}_*《[[神農本草經.md "wikilink")》謂辛夷花「主五臟身體發熱，頭風腦痛。」

  - 據藥理研究，本品所含的[揮發油](../Page/揮發油.md "wikilink")，有收縮鼻粘膜血管作用，可代替[麻黃碱](../Page/麻黃碱.md "wikilink")，有通鼻竅消炎之功效，可治各種鼻炎，尤其對過敏性鼻炎效果較釨，中醫臨床主要用於鼻部疾患。其非揮發性成分，有收縮[子宮和降血作用](../Page/子宮.md "wikilink")，是否可作縮宮藥及降壓藥使用，有待進一步研究。

## 外部链接

  - [玉蘭
    Yulan](http://libproject.hkbu.edu.hk/was40/detail?channelid=1288&searchword=herb_id=D00652)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [辛夷
    Xinyi](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00314)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [辛夷 Xin
    Yi](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00384)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [連翹苷元
    Phillygenin](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00487)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[X辛](../Category/中药.md "wikilink")
[Category:木兰属](../Category/木兰属.md "wikilink")