**广西大学**，简称**西大**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[广西壮族自治区的一所综合性教学研究型大学](../Page/广西壮族自治区.md "wikilink")，是廣西歷史最悠久及規模最大的大學。1928年由首任校长[马君武创建于广西](../Page/马君武.md "wikilink")[梧州](../Page/梧州.md "wikilink")。广西大学现为由[中华人民共和国教育部和](../Page/中华人民共和国教育部.md "wikilink")[广西壮族自治区人民政府共同建设的](../Page/广西壮族自治区人民政府.md "wikilink")“[部区合建](../Page/部省合建.md "wikilink")”大学。

## 历史

1928年10月，教育家[马君武在梧州创办省立广西大学](../Page/马君武.md "wikilink")。1930年，创办理学院，1932年，开设农学院，工学院。

1936年，合并广西省立师范专科学校（位于今[桂林良丰村](../Page/桂林.md "wikilink")[雁山公园](../Page/雁山公园.md "wikilink")）；5月，设立文法学院（后改法商学院），[朱佛定为广西大学秘书长兼文法学院](../Page/朱佛定.md "wikilink")（校本部）院长统辖各学院，朱佛定成為实际上的广西大学校长。后法商学院与广西省立医学院合并，开设医学院（1937年改为广西军医学校，今[广西医科大学](../Page/广西医科大学.md "wikilink")），并将理学院、工学院合并为理工学院，院址迁往今桂林良丰村雁山公园；农学院迁往[柳州](../Page/柳州.md "wikilink")。

1939年，改名为国立广西大学。

1946年，在先后迁址至[贵州](../Page/贵州.md "wikilink")[榕江](../Page/榕江县.md "wikilink")、柳州后，农学院迁往桂林良丰雁山公园，理工及法商学院迁往[将军桥](../Page/白龙桥.md "wikilink")。

1948年，设置文学院。

1950年，合并国立南宁师范学院，设师范学院（后改文教学院）。1952年，广西大学农学院独立为广西农学院（后迁南宁，并改名为广西农业大学）。同年，[毛泽东为广西大学题写校名](../Page/毛泽东.md "wikilink")。1953年，停办，文、理、师范科，另设广西师范学院（今[广西师范大学](../Page/广西师范大学.md "wikilink")）。1958年春，在[南宁复办广西大学](../Page/南宁.md "wikilink")；另办广西工学院（后并入）。1985年，位于柳州的分校独立为新[广西工学院](../Page/广西工学院.md "wikilink")。

1997年，与[广西农业大学合并](../Page/广西农业大学.md "wikilink")，组成新的广西大学至今。同年12月，进入中国国家[211工程重点建设高校名单](../Page/211工程.md "wikilink")。

廣西大學一直是廣西地區排名第一的大學\[1\]。

## 现状

[BiYun_Lake,_Guangxi_University.jpg](https://zh.wikipedia.org/wiki/File:BiYun_Lake,_Guangxi_University.jpg "fig:BiYun_Lake,_Guangxi_University.jpg")
学校现设27个学院，学科涵盖哲、经、法、教、文、理、工、农、医、管、艺等11大学科门类，有98个本科专业（2017年招生79个，其中有24个专业按9个大类招生），36个一级学科硕士点，16个二级学科硕士点（不含一级学科硕士点已覆盖的二级学科硕士点），8个一级学科博士点，3个二级学科博士点（不含一级学科博士点已覆盖的二级学科博士点），16个硕士专业学位类别和10个博士后科研流动站。有1个国家“双一流”建设学科群，2个国家重点学科，1个国家重点（培育）学科；工程学、材料科学、农业科学进入ESI学科全球前1%行列。有1个国家重点实验室和1个省部共建国家重点实验室培育基地，3个教育部重点实验室和工程研究中心，1个教育部国别和区域研究基地，1个国家林业局重点实验室，和一批广西重点建设的实验室、工程技术研究中心、研究基地。有4个广西“2011协同创新中心和培育基地”，1个广西人文社会科学重点研究基地、6个广西高校人文社会科学重点研究基地\[2\]。

## 院系设置

  - 机械工程学院
  - 电气工程学院
  - 土木建筑工程学院
  - 化学化工学院
  - 资源环境与材料学院
  - 轻工与食品工程学院
  - 计算机与电子信息学院
  - 海洋学院
  - 生命科学与技术学院
  - 农学院
  - 动物科学技术学院
  - 林学院
  - 数学与信息科学学院
  - 物理科学与工程技术学院
  - 文学院
  - 新闻传播学院
  - 外国语学院
  - 艺术学院
  - 公共管理学院
  - 商学院
  - 法学院
  - 马克思主义学院
  - 体育学院
  - 医学院
  - 中加国际学院
  - 国际教育学院
  - 继续教育学院

## 知名校友

  - [马君武](../Page/马君武.md "wikilink")
  - [朱佛定](../Page/朱佛定.md "wikilink")
  - [梁漱溟](../Page/梁漱溟.md "wikilink")
  - [陈寅恪](../Page/陈寅恪.md "wikilink")
  - [李四光](../Page/李四光.md "wikilink")
  - [千家驹](../Page/千家驹.md "wikilink")
  - [黄现璠](../Page/黄现璠.md "wikilink")
  - [李达](../Page/李达.md "wikilink")
  - [陈望道](../Page/陈望道.md "wikilink")
  - [施汝为](../Page/施汝为.md "wikilink")
  - [何康](../Page/何康.md "wikilink")
  - [李兆焯](../Page/李兆焯.md "wikilink")
  - [鲍方](../Page/鲍方.md "wikilink")
  - [李崇道](../Page/李崇道.md "wikilink")
  - [陈梧桐](../Page/陈梧桐.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [广西大学官方网站](http://www.gxu.edu.cn)

[\*](../Category/广西大学.md "wikilink")
[Category:211工程](../Category/211工程.md "wikilink")
[Category:1928年創建的教育機構](../Category/1928年創建的教育機構.md "wikilink")
[Category:一流学科建设高校](../Category/一流学科建设高校.md "wikilink")
[Category:南宁高等院校](../Category/南宁高等院校.md "wikilink")

1.  [廣西區大學前四名](http://edu.people.com.cn/BIG5/8216/9320/76888/78036/5356975.html)《人民網》
2.  [学校概况](http://www.gxu.edu.cn/Category_68/Index.aspx)