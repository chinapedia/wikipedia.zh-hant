**兴光**（454年七月-455年六月）是[北魏文成帝](../Page/北魏.md "wikilink")[拓跋濬的年號](../Page/拓跋濬.md "wikilink")，歷時年餘。

## 纪年

| 兴光                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 454年                           | 455年                           |
| [干支](../Page/干支纪年.md "wikilink") | [甲午](../Page/甲午.md "wikilink") | [乙未](../Page/乙未.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [孝建](../Page/孝建.md "wikilink")（454年正月—456年十二月）：[南朝宋](../Page/南朝宋.md "wikilink")[宋孝武帝刘骏的年号](../Page/宋孝武帝.md "wikilink")
      - [承平](../Page/承平_\(沮渠無諱\).md "wikilink")（443年-460年）：[北凉政权](../Page/北凉.md "wikilink")[沮渠無諱](../Page/沮渠無諱.md "wikilink")、[沮渠安周年号](../Page/沮渠安周.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")