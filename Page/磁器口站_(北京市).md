**磁器口站**位于[北京市](../Page/北京市.md "wikilink")[东城区](../Page/东城区_\(北京市\).md "wikilink")，是[北京地铁](../Page/北京地铁.md "wikilink")[5号线和](../Page/北京地铁5号线.md "wikilink")[7号线的一个换乘车站](../Page/北京地铁7号线.md "wikilink")。

## 位置

这个站位于[崇文门外大街和](../Page/崇文门外大街.md "wikilink")[珠市口东大街](../Page/珠市口东大街.md "wikilink")、[广渠门内大街交汇处](../Page/广渠门内大街.md "wikilink")。

## 车站设计

### 车站楼层

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td><p>街道</p></td>
<td><p>A－H出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一层</strong></p></td>
<td><p>5号线站厅</p></td>
<td><p>安全检查、自动售票机、客服中心等</p></td>
</tr>
<tr class="odd">
<td><p><strong>地下二层<br />
5号线站台</strong></p></td>
<td><p>7号线站厅</p></td>
<td><p>安全检查、自动售票机、客服中心等</p></td>
</tr>
<tr class="even">
<td><p>北行</p></td>
<td><p>列车往方向<small>（）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，左边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南行</p></td>
<td><p>　列车往<a href="../Page/宋家庄站_(北京).md" title="wikilink">宋家庄方向</a> <small>（）</small> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地下三层<br />
7号线站台</strong></p></td>
<td><p>西行</p></td>
<td><p>列车往<a href="../Page/北京西站.md" title="wikilink">北京西站方向</a> <small>（<a href="../Page/桥湾站_(北京).md" title="wikilink">桥湾</a>）</small></p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/島式月台.md" title="wikilink">島式月台</a>，左边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>东行</p></td>
<td><p>　列车往方向 <small>（）</small> </p></td>
<td></td>
</tr>
</tbody>
</table>

### 站厅

[Dream_of_the_Red_Chamber_mural_at_L7_Ciqikou_Station_(20170406131308).jpg](https://zh.wikipedia.org/wiki/File:Dream_of_the_Red_Chamber_mural_at_L7_Ciqikou_Station_\(20170406131308\).jpg "fig:Dream_of_the_Red_Chamber_mural_at_L7_Ciqikou_Station_(20170406131308).jpg")
5号线站厅位于崇文门外大街地下，东面为换乘通道口。7号线站厅位于广渠门内大街地下，西侧通过2组双向扶梯及楼梯与5号线站厅连接，北面设有由李化吉、陈晓林、陈达什创作的《[红楼梦](../Page/红楼梦.md "wikilink")》主题壁画\[1\]，以纪念本站附近因拓宽两广路而被拆除的广渠门内大街207号（蒜市口16号）曹雪芹故居。
{{ multiple image | align = center | direction = vertical | width = 600
| image1 = Concourse panorama of L5 Ciqikou Station (20170406131503).jpg
| caption1 =

<center>

5号线站厅全景（2017年4月）

</center>

}}

### 站台

[Platform_for_L5_at_Ciqikou_Station_(20170406131545).jpg](https://zh.wikipedia.org/wiki/File:Platform_for_L5_at_Ciqikou_Station_\(20170406131545\).jpg "fig:Platform_for_L5_at_Ciqikou_Station_(20170406131545).jpg")
5号线站台为岛式站台，位于崇文门外大街地底。7号线站台同样是岛式站台，位于广渠门内大街地底，轨道下穿5号线站台。\[2\]

### 出口

5号线磁器口站目前开放A、D两个出入口，7号线磁器口站则开放F、G、H三个出入口。

<table>
<thead>
<tr class="header">
<th><p>编号</p></th>
<th><p>建议前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>珠市口东大街（北侧）、新成文化大厦</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>珠市口东大街（南侧）、磁器口大街</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>广渠门内大街（北侧）、崇文门外大街</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>广渠门内大街（南侧）、崇文门外大街、石板胡同、南河槽胡同、新裕家园</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>广渠门内大街（南侧）、北京大都市大厦</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:北京市东城区地铁车站](../Category/北京市东城区地铁车站.md "wikilink")

1.
2.