在[语言学中](../Page/语言学.md "wikilink")，**分词**（、，是[希腊语](../Page/希腊语.md "wikilink")(“分担”)的直译）是一种[非定式](../Page/非定式动词.md "wikilink")[动词形式](../Page/动词.md "wikilink")，可以用于构成复合[时态](../Page/时态.md "wikilink")、[语态或作为](../Page/语态.md "wikilink")[修饰语](../Page/修饰语.md "wikilink")。分词常常有其他词类的特征，特别是[形容词和](../Page/形容词.md "wikilink")[名词的特征](../Page/名词.md "wikilink")。

有分词的语系有[印欧语系](../Page/印欧语系.md "wikilink")、[闪-含语系](../Page/闪-含语系.md "wikilink")、[乌拉尔语系](../Page/乌拉尔语系.md "wikilink")、[阿尔泰语系](../Page/阿尔泰语系.md "wikilink")、等。[汉藏语系的诸语言则没有分词这一语法范畴](../Page/汉藏语系.md "wikilink")。

在多数现代欧洲语言中，分词有**现在分词**（）和**过去分词**（）两种。在[德语中](../Page/德语.md "wikilink")，这两种分词更习惯叫作**第一分词**（）和**第二分词**（）。

## 分詞的種類

分詞常常用[時態來區別](../Page/時態.md "wikilink")，例如[英語裏有](../Page/英語.md "wikilink")[現在分詞和](../Page/現在分詞.md "wikilink")[過去分詞](../Page/過去分詞.md "wikilink")。但這只是約定俗成；現在分詞不一定就是表示現在，過去分詞也不一定表示過去。

分詞也可能用[語態區別](../Page/語態.md "wikilink")。有的語言（如[拉丁語和](../Page/拉丁語.md "wikilink")[俄語](../Page/俄語.md "wikilink")）的[主動語態和](../Page/主動語態.md "wikilink")[被動語態有不同分詞](../Page/被動語態.md "wikilink")。英語中的現在分詞本質上是主動分詞，而過去分詞兼具主動、被動用法，就如以下例子所展示的：

  - I saw John **eating** his dinner.
    我看見約翰吃飯。（eating是主動分詞，而被修飾名詞John是[實施者](../Page/實施者.md "wikilink")）
  - I have **eaten** my dinner. 我吃過飯了。（完成時結構；eaten是主动分词）
  - John was **eaten** by lions.
    約翰被獅子吃了。（eaten是被動分詞，John是[承受者](../Page/承受者.md "wikilink")）

有時也區分**[形容詞性分詞](../Page/形容詞.md "wikilink")**和**[副詞性分詞](../Page/副詞.md "wikilink")**。副词性分词（或分詞短語、基於這種分詞的從句）在句中扮演[狀語](../Page/狀語.md "wikilink")，而形容詞性分詞（或分詞短語、基於這種分詞的從句）扮演[定語或](../Page/定語.md "wikilink")[表語](../Page/表語.md "wikilink")。有些語言裏這兩種分詞形式不同，比如俄語和其餘[斯拉夫語言](../Page/斯拉夫語族.md "wikilink")、[匈牙利語](../Page/匈牙利語.md "wikilink")，以及許多[愛斯基摩語](../Page/東加拿大因紐特語.md "wikilink")。

## 参见

  - [语法](../Page/语法.md "wikilink")
  - [垂悬分词](../Page/垂悬分词.md "wikilink")
  - [动名词](../Page/动名词.md "wikilink")
  - [現在式](../Page/現在式.md "wikilink")
  - [過去式](../Page/過去式.md "wikilink")
  - [未來式](../Page/未來式.md "wikilink")
  - [過去完成式](../Page/過去完成式.md "wikilink")
  - [完成式](../Page/完成式.md "wikilink")

## 参考文献

  - [Participles](http://www.bartleby.com/64/C001/047.html) from the
    *American Heritage Book of English Usage* (1996).

[Category:动词](../Category/动词.md "wikilink")