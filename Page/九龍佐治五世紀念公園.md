[King_George_V_Memorial_Park_Entrance_2013.jpg](https://zh.wikipedia.org/wiki/File:King_George_V_Memorial_Park_Entrance_2013.jpg "fig:King_George_V_Memorial_Park_Entrance_2013.jpg")
[King_George_V_Memorial_Park_Pavilion_2013.jpg](https://zh.wikipedia.org/wiki/File:King_George_V_Memorial_Park_Pavilion_2013.jpg "fig:King_George_V_Memorial_Park_Pavilion_2013.jpg")
[King_George_V_Memorial_Park_View_2013.jpg](https://zh.wikipedia.org/wiki/File:King_George_V_Memorial_Park_View_2013.jpg "fig:King_George_V_Memorial_Park_View_2013.jpg")
[King_George_V_Memorial_Park_Playground_2013.jpg](https://zh.wikipedia.org/wiki/File:King_George_V_Memorial_Park_Playground_2013.jpg "fig:King_George_V_Memorial_Park_Playground_2013.jpg")
[King_George_V_Memorial_Park_pavilion_tap_case.JPG](https://zh.wikipedia.org/wiki/File:King_George_V_Memorial_Park_pavilion_tap_case.JPG "fig:King_George_V_Memorial_Park_pavilion_tap_case.JPG")

**九龍佐治五世紀念公園**（，原名：）是位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[官涌一帶的一個公園](../Page/官涌.md "wikilink")，位置為[佐敦道以南](../Page/佐敦道.md "wikilink")，[廣東道以東](../Page/廣東道.md "wikilink")，[港景峰及](../Page/港景峰.md "wikilink")[官涌市政大廈以北及](../Page/官涌市政大廈.md "wikilink")[上海街以西](../Page/上海街.md "wikilink")。鄰近西南面有私人屋苑[The
Austin及](../Page/The_Austin.md "wikilink")[港鐵](../Page/港鐵.md "wikilink")[柯士甸站](../Page/柯士甸站.md "wikilink")。

## 歷史

九龍佐治五世紀念公園於1940年8月興建，並於1941年6月11日的下午6時，由當年的[香港輔政司](../Page/香港輔政司.md "wikilink")[史美代表因足疾不能到臨的](../Page/史美.md "wikilink")[總督](../Page/總督.md "wikilink")[羅富國](../Page/羅富國.md "wikilink")[爵士主持開幕](../Page/爵士.md "wikilink")，[公園從](../Page/公園.md "wikilink")1940年10月動土，承包商禮記公司（Lai
Kee Company）以8個月時間，耗資港幣7萬元建成，初期佔地僅為8,742平方米，正門原為 Raoul Bigazzi
制作的精致中國牌坊式青銅門，頗為壯觀。當年園裡中央豎立英國已故[國王](../Page/英國君主.md "wikilink")[佐治五世的銅像](../Page/佐治五世.md "wikilink")，並闢有遊戲場，由「兒童遊樂場委員會」捐獻[鞦韆等設備](../Page/鞦韆.md "wikilink")。公園名稱紀念英國已故國王[佐治五世](../Page/佐治五世.md "wikilink")，佔地94,000[平方呎](../Page/平方呎.md "wikilink")，是當時九龍最具規模的公園。公園以仿中國官殿牌樓園林設計，中央設有英國國王[佐治五世的](../Page/佐治五世.md "wikilink")[銅像](../Page/銅像.md "wikilink")，兩旁則為兒童遊樂設施。

[香港日治時期](../Page/香港日治時期.md "wikilink")，公園設施被毀壞，至1954年3月21日方修復完成重新開放。重建後的公園，設有3個中國式的[涼亭](../Page/涼亭.md "wikilink")，並增設[足球場及](../Page/足球.md "wikilink")[籃球場等設施](../Page/籃球.md "wikilink")。公園正門的紅簷綠瓦的[門樓](../Page/門樓.md "wikilink")，一直是該公園的特色之一。

1970年代，政府為使[廣東道接駁](../Page/廣東道.md "wikilink")[渡船街](../Page/渡船街.md "wikilink")，將前者向西移，使公園多出一塊土地。在1980年代後期，因在公園東南部興建[官涌市政大廈而有所改建](../Page/官涌市政大廈.md "wikilink")。園內有球場及遊樂場。

## 途經之公共交通服務

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{荃灣綫色彩}}">█</font><a href="../Page/荃灣綫.md" title="wikilink">荃灣綫</a>：<a href="../Page/佐敦站.md" title="wikilink">佐敦站</a></li>
<li><font color="{{東涌綫色彩}}">█</font><a href="../Page/東涌綫.md" title="wikilink">東涌綫</a>、<font color={{機場快綫色彩}}>█</font><a href="../Page/機場快綫.md" title="wikilink">機場快綫</a>：<a href="../Page/九龍站_(港鐵).md" title="wikilink">九龍站</a></li>
<li><font color="{{西鐵綫色彩}}">█</font><a href="../Page/西鐵綫.md" title="wikilink">西鐵綫</a>：<a href="../Page/柯士甸站.md" title="wikilink">柯士甸站</a></li>
<li><a href="../Page/廣深港高速鐵路.md" title="wikilink">廣深港高速鐵路</a>：<a href="../Page/香港西九龍站.md" title="wikilink">香港西九龍站</a></li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 禁煙安排

此場地（除吸煙區外）禁止[吸煙](../Page/吸煙.md "wikilink")。

## 參見

  - [香港佐治五世紀念公園](../Page/香港佐治五世紀念公園.md "wikilink")（較晚建成，同樣紀念英國國王[佐治五世的公園](../Page/佐治五世.md "wikilink")）；

## 參考資料

  - 1941年6月11日，香港的中文報紙《[大公報](../Page/大公報.md "wikilink")》，第六版
  - 1941年6月12日，香港的英文報紙《[士蔑報](../Page/士蔑報.md "wikilink") (Hong Kong
    Telegraph)》，第2頁

## 外部連結

[九龍佐治五世紀念公園(官方網站](https://www.lcsd.gov.hk/tc/parks/kp/index.html)

  - [九龍佐治五世紀念公園位置](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=835336&cy=818466&zm=4&mx=835336&my=818466&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)
  - [九龍佐治五世紀念公園簡介](http://www.hk-place.com/view.php?id=154)

[Category:油尖旺區公園](../Category/油尖旺區公園.md "wikilink")
[Category:官涌](../Category/官涌.md "wikilink")
[Category:香港中式園林](../Category/香港中式園林.md "wikilink")
[Category:以英国君主命名的事物](../Category/以英国君主命名的事物.md "wikilink")
[Category:以君主命名的地名](../Category/以君主命名的地名.md "wikilink")