**神龍翼龍超科**（[学名](../Page/学名.md "wikilink")：）屬於[翼龍目](../Page/翼龍目.md "wikilink")[翼手龍亞目的一个演化支](../Page/翼手龍亞目.md "wikilink")。

## 分類

神龙翼龙类由大衛·安文于2003年进行系统发生学定义。安文的定义为：包含风神翼龙和古神翼龙的最近共同祖先，以及其后代。\[1\]

对于神龙翼龙类的关系，有出现过不同的竞争观点。Felipe
Pinheiro和其同事于2011年最先提出古神翼龙科是单系群，包含了掠海翼龙科和朝阳翼龙科。\[2\]

以下列表是根據[大衛·安文](../Page/大衛·安文.md "wikilink")（David Unwin）在2006年的研究：

  - **神龍翼龍超科 Azhdarchoidea**
      - [湖氓翼龍](../Page/湖氓翼龍.md "wikilink") *Lacusovagus*\[3\]
      - [黎明神龍翼龍](../Page/黎明神龍翼龍.md "wikilink") *Aurorazhdarcho*\[4\]
      - [鳥嘴翼龍](../Page/鳥嘴翼龍.md "wikilink") *Ornithostoma*\[5\]
      - [朝陽翼龍科](../Page/朝陽翼龍科.md "wikilink") Chaoyangopteridae\[6\]
          - [朝陽翼龍](../Page/朝陽翼龍.md "wikilink") *Chaoyangopterus*
          - [始神龍翼龍](../Page/始神龍翼龍.md "wikilink") *Eoazhdarcho*
          - [始無齒翼龍](../Page/始無齒翼龍.md "wikilink") *Eopteranodon*
          - [吉大翼龍](../Page/吉大翼龍.md "wikilink") *Jidapterus*
          - [神州翼龍](../Page/神州翼龍.md "wikilink") *Shenzhoupterus*
      - [槍嘴翼龍科](../Page/槍嘴翼龍科.md "wikilink") Lonchodectidae
          - [槍嘴翼龍](../Page/槍嘴翼龍.md "wikilink") *Lonchodectes*
      - [古神翼龍科](../Page/古神翼龍科.md "wikilink") Tapejaridae
          - [古神翼龍亞科](../Page/古神翼龍亞科.md "wikilink") Tapejarinae
              - [華夏翼龍](../Page/華夏翼龍.md "wikilink") *Huaxiapterus*
              - [中國翼龍](../Page/中國翼龍.md "wikilink") *Sinopterus*
              - [古神翼龍](../Page/古神翼龍.md "wikilink") *Tapejara*
              - [雷神翼龍](../Page/雷神翼龍.md "wikilink") *Tupandactylus*\[7\]
      - [掠海翼龍科](../Page/掠海翼龍科.md "wikilink") Thalassodromidae \[8\]
          - [掠海翼龍](../Page/掠海翼龍.md "wikilink") *Thalassodromeus*
          - [妖精翼龍](../Page/妖精翼龍.md "wikilink") *Tupuxuara*
      - [神龍翼龍科](../Page/神龍翼龍科.md "wikilink") Azhdarchidae
          - [鹹海神翼龍](../Page/鹹海神翼龍.md "wikilink") *Aralazhdarcho*\[9\]
          - [阿氏翼龍](../Page/阿氏翼龍.md "wikilink") *Arambourgiania*
          - [神龍翼龍](../Page/神龍翼龍.md "wikilink") *Azhdarcho*
          - [包科尼翼龍](../Page/包科尼翼龍.md "wikilink") *Bakonydraco*
          - [哈特茲哥翼龍](../Page/哈特茲哥翼龍.md "wikilink") *Hatzegopteryx*
          - [蒙大拿神翼龍](../Page/蒙大拿神翼龍.md "wikilink") *Montanazhdarcho*
          - [磷礦翼龍](../Page/磷礦翼龍.md "wikilink") *Phosphatodraco*
          - [風神翼龍](../Page/風神翼龍.md "wikilink") *Quetzalcoatlus*
          - [浙江翼龍](../Page/浙江翼龍.md "wikilink") *Zhejiangopterus*
      - 可能屬於神龍翼龍類的屬：
          - [波氏翼龍](../Page/波氏翼龍.md "wikilink") *Bogolubovia*
          - [班尼特翼龍](../Page/班尼特翼龍.md "wikilink") *Bennettazhia*
          - [矛嘴翼龍](../Page/矛嘴翼龍.md "wikilink") *Doratorhynchus*
          - 本氏鳥掌翼龍 *Ornithocheirus bunzeli*

## 種系發生學

以下[演化樹是根據](../Page/演化樹.md "wikilink")[呂君昌](../Page/呂君昌.md "wikilink")、[大衛·安文等人在](../Page/大衛·安文.md "wikilink")2008年的研究，認為[古神翼龍科是個原始神龍翼龍類的](../Page/古神翼龍科.md "wikilink")[並系群](../Page/並系群.md "wikilink")。親緣關係接近於[神龍翼龍](../Page/神龍翼龍.md "wikilink")，而離神龍翼龍較遠的物種被歸類於*新神龍翼龍類*（Neoazhdarchia）\[10\]：

以下演化樹則是根據Felipe
Pinheiro等人在2011年提出的研究，認為古神翼龍科是個[單系群](../Page/單系群.md "wikilink")，包含[掠海翼龍亞科](../Page/掠海翼龍亞科.md "wikilink")、[朝陽翼龍亞科](../Page/朝陽翼龍亞科.md "wikilink")\[11\]：

## 參考資料

[神龍翼龍超科](../Category/神龍翼龍超科.md "wikilink")

1.  Unwin, D. M., (2003). "On the phylogeny and evolutionary history of
    pterosaurs." Pp. 139-190. in Buffetaut, E. & Mazin, J.-M., (eds.)
    (2003). *Evolution and Palaeobiology of Pterosaurs*. Geological
    Society of London, Special Publications 217, London, 1-347.

2.  Pinheiro, F.L., Fortier, D.C., Schultz, C.L., De Andrade, J.A.F.G.
    and Bantim, R.A.M. (in press). "New information on *Tupandactylus
    imperator*, with comments on the relationships of Tapejaridae
    (Pterosauria)." *Acta Palaeontologica Polonica*, in press, available
    online 03 Jan 2011.

3.

4.  Frey, E., Meyer, C.A. and Tischlinger, H. (2011). "The oldest
    azhdarchoid pterosaur from the Late Jurassic Solnhofen Limestone
    (Early Tithonian) of Southern Germany." *Swiss Journal of
    Geosciences*, (advance online publication)

5.  Averianov, A.O. (2012). "*Ornithostoma sedgwicki* – valid taxon of
    azhdarchoid pterosaurs." *Proceedings of the Zoological Institute
    RAS*, **316**(1): 40–49.

6.  Lü, J., Unwin, D.M., Xu, L., and Zhang, X. (2008). "A new
    azhdarchoid pterosaur from the Lower Cretaceous of China and its
    implications for pterosaur phylogeny and evolution."
    *Naturwissenschaften*,

7.

8.
9.  Averianov, 2007

10.
11. Pinheiro, F.L., Fortier, D.C., Schultz, C.L., De Andrade, J.A.F.G.
    and Bantim, R.A.M. (in press). "New information on *Tupandactylus
    imperator*, with comments on the relationships of Tapejaridae
    (Pterosauria)." *Acta Palaeontologica Polonica*, in press, available
    online 03 Jan 2011.