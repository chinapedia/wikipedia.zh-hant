**佛光寺**位于[山西省](../Page/山西省.md "wikilink")[五台县](../Page/五台县.md "wikilink")[豆村镇佛光村](../Page/豆村镇.md "wikilink")，正殿（东大殿）建于[唐](../Page/唐朝.md "wikilink")[大中十一年](../Page/大中_\(唐朝\).md "wikilink")（857年），是中国现存第二古老的[木结构建筑](../Page/木结构建筑.md "wikilink")，仅次于五台县[南禅寺大殿](../Page/南禅寺_\(五台\).md "wikilink")。佛光寺大殿是中国现代最早发现的唐代木结构建筑，其内还有唐代塑像、壁画和墨书题记，集四大唐代遗物于一身，因此被建筑学家[梁思成称为](../Page/梁思成.md "wikilink")“中国第一国宝”\[1\]。

1961年，佛光寺被列为[第一批全国重点文物保护单位](../Page/第一批全国重点文物保护单位.md "wikilink")。2009年6月26日，佛光寺连同其他[五台山景观被列入](../Page/五台山.md "wikilink")《[世界遗产名录](../Page/世界遗产名录.md "wikilink")》名目。

## 历史

[Detail_of_Foguang_Temple_from_Mogao_Cave_61_2013-12.JPG](https://zh.wikipedia.org/wiki/File:Detail_of_Foguang_Temple_from_Mogao_Cave_61_2013-12.JPG "fig:Detail_of_Foguang_Temple_from_Mogao_Cave_61_2013-12.JPG")[莫高窟第](../Page/莫高窟.md "wikilink")61窟五台山图之大佛光寺（五代時期）\]\]
佛光寺始建于[北魏孝文帝时期](../Page/北魏孝文帝.md "wikilink")。唐朝时期曾是[五台山](../Page/五台山.md "wikilink")“十大寺”之一，法兴禅师在寺内兴建了高达三十二米的弥勒大阁。[唐武宗](../Page/唐武宗.md "wikilink")[会昌五年](../Page/会昌.md "wikilink")（
845年），大举灭佛，佛光寺因此被毁，仅一座祖师塔幸存。847年，[唐宣宗](../Page/唐宣宗.md "wikilink")[李忱继位](../Page/李忱.md "wikilink")，佛教再兴，佛光寺得以重建，大殿即此时在原弥勒大阁遗址上重建。之后，宋、金、明、清，均对佛光寺进行了修葺。1937年建筑学家[梁思成](../Page/梁思成.md "wikilink")、[林徽因对佛光寺进行了考察](../Page/林徽因.md "wikilink")、测绘。

## 建筑

### 布局

佛光寺位置在一个向西的山坡上，轴线采取东西向，寺院适应着地形处理成三个平台，第一层平台较宽阔，北部有[金](../Page/金.md "wikilink")[天会十五年](../Page/天会.md "wikilink")（1137年）建造的文殊殿，南侧为明代重建的伽蓝殿；第三层平台以高峻的挡土墙砌成，上建有正殿。

### 山门和影壁

### 文殊殿

文殊殿建于金[天会十五年](../Page/天会.md "wikilink")（1137年），元[至正十一年](../Page/至正.md "wikilink")（1351年）重修，[悬山顶](../Page/悬山顶.md "wikilink")。

### 大殿

佛光寺大殿為[中国现存规模最大的](../Page/中华人民共和国.md "wikilink")[唐代木构建筑](../Page/唐代.md "wikilink")。

大殿单层[四阿顶](../Page/四阿顶.md "wikilink")，面阔四间，进深八架橼。由内外两圈[柱组成平面柱网](../Page/柱.md "wikilink")，内外柱等高，[檐柱有侧脚及升起](../Page/檐.md "wikilink")。檐口曲线平缓，出檐深远。[斗栱尺度雄大](../Page/斗栱.md "wikilink")，形式古朴。脊槫下不施侏儒柱，仅用叉手，是现存已发现古代木建筑中的构造孤例。殿内遗有[释迦](../Page/释迦.md "wikilink")、[弥勒](../Page/弥勒.md "wikilink")、[普贤](../Page/普贤.md "wikilink")、[观音等唐代塑像](../Page/观音.md "wikilink")，以及唐、[宋壁画和题记](../Page/宋朝.md "wikilink")。

大殿坐东朝西，最东的高地高出前部地面约十二三米。大殿面阔为七间，长为34米；进深为四间，长为17.66米；单檐庑殿顶。殿内设有一圈内柱，后部设有“扇面墙”。

### 祖师塔

东大殿南侧有六角形砖塔一座，名祖师塔，是[北魏时期建筑的墓塔](../Page/北魏.md "wikilink")。塔两层，总高12米余，底层空心，内置六角小室，门洞外作莲瓣及火焰形券面，塔檐[叠涩砌筑](../Page/叠涩.md "wikilink")，上层塔身作假券洞式门及破子棂窗，塔刹设仰覆莲座、覆钵和宝珠。佛光寺祖師塔和[崇福寺北魏石塔是中國僅存的兩座北魏多層石塔](../Page/崇福寺_\(朔州\).md "wikilink")。

### 经幢

佛光寺内有两座[唐代](../Page/唐代.md "wikilink")[石经幢](../Page/石经幢.md "wikilink")，一座在前庭，为唐[乾符四年](../Page/乾符.md "wikilink")（877年）建造，高4.9米。另一座在大殿前，为唐[大中十一年](../Page/大中_\(唐朝\).md "wikilink")（857年）建造，高3.2米，幢身刻有“女弟子佛殿主宁公遇”之名，与东大殿内墨书题记可相印证，是该殿建造年代之依据。

### 其他建筑

佛光寺内还有明末所建的珈蓝殿、大殿两侧配殿：万善堂和关帝庙以及厢房等建筑。

佛光寺外还有六座墓塔，其中寺东山坡上三座：无垢净光塔，唐天宝十一年建；大德方便和尚塔，唐贞元十一年建；智远和尚塔，晚唐建。寺西北有塔三座：解脱禅师塔，唐长庆四年建；杲公和尚塔，金泰和五年建；无名砖塔，年代不详，疑为明塔。

## 参考文献

[Category:五台县](../Category/五台县.md "wikilink")
[Category:五台山](../Category/五台山.md "wikilink")
[Category:忻州佛寺](../Category/忻州佛寺.md "wikilink")
[Category:唐朝建筑](../Category/唐朝建筑.md "wikilink")
[Category:9世紀完工建築物](../Category/9世紀完工建築物.md "wikilink")
[Category:山西全国重点文物保护单位](../Category/山西全国重点文物保护单位.md "wikilink")

1.  [CCTV纪录片《梁思成
    林徽因》第三集：佛光](http://jishi.cntv.cn/C21323/classpage/video/20101018/100340.shtml)