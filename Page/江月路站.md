**江月路站**位于[上海](../Page/上海.md "wikilink")[闵行区](../Page/闵行区.md "wikilink")[浦江镇](../Page/浦江镇.md "wikilink")[浦星公路](../Page/浦星公路.md "wikilink")[江月路东北角](../Page/江月路.md "wikilink")，为[上海轨道交通8号线二期的高架岛式车站](../Page/上海轨道交通8号线.md "wikilink")，于2009年7月5日启用。

## 车站出口

  - 主出口：浦星公路东侧，站体西侧，江月路北
  - 次出口：浦星公路东侧，站体东侧，江月路北

## 公交换乘

163、915、浦江1路、浦江3路、浦江12路、浦江16路、闵行11路、闵行社区巴士、江五线、川浦线、南申专线

## 车站周边

[浦江世博家园](../Page/浦江世博家园.md "wikilink")

## 参考资料

1.  [江月路站建设用地规划许可证](https://web.archive.org/web/20070927193048/http://www.shghj.gov.cn/Ct_3.aspx?ct_id=00060404E04922)
2.  [东方网上海频道(2007年12月11日) - 浦东轨交"新四线"2009年底全建成
    动迁已基本完成](http://xwwb.eastday.com/x/20071211/u1a382283.html)
3.  [文新传媒：轨交8号线还要往南延伸到航天公园站](http://topic.news365.com.cn/jjstxg_4090/200712/t20071205_1675008.htm)

[Category:闵行区地铁车站](../Category/闵行区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")