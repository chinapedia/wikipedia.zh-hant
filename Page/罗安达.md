**罗安达**（，原稱：）或译**卢安达**，位于[安哥拉西部](../Page/安哥拉.md "wikilink")[大西洋沿岸](../Page/大西洋.md "wikilink")，是该国首都和第一大城市，也是重要的港口和是全國[政治](../Page/政治.md "wikilink")、[文化](../Page/文化.md "wikilink")、[經濟和](../Page/經濟.md "wikilink")[交通的中心](../Page/交通.md "wikilink")。

羅安達是安哥拉的首都，是瀕臨大西洋海岸中段的一座歐洲式港口城市，具有濃厚的葡萄牙風味。它位於[撒哈拉以南](../Page/撒哈拉沙漠以南的非洲.md "wikilink")，是歐洲移民最早在安哥拉境內所建立的城市，市內有鐵路通往[马兰热](../Page/马兰热.md "wikilink")。

经过内战后的重建，该市开始建起较为现代的商业大楼、酒店等现代化设施；但是城区建筑布局较为混乱，交通极为拥堵。一座本只应容纳50万人口的城市，都會區如今容纳了将近500万人，从而造成一系列社会问题，例如交通拥挤，治安恶化，卫生問題等。

## 歷史

### 葡萄牙統治時期

[Civitas_Loandae_S._Pauli.jpg](https://zh.wikipedia.org/wiki/File:Civitas_Loandae_S._Pauli.jpg "fig:Civitas_Loandae_S._Pauli.jpg")
[Right](https://zh.wikipedia.org/wiki/File:Luanda1883.jpg "fig:Right")
[葡萄牙探險家](../Page/葡萄牙.md "wikilink")[保羅·迪亞士·德·諾發伊斯在](../Page/保羅·迪亞士·德·諾發伊斯.md "wikilink")1576年1月25日發現了羅安達，遂命名此地為“São
Paulo da Assumpção de Loanda”，並有數百個海員和移民家庭定居到此。1618年，葡萄牙人建立了名為“Fortaleza
São Pedro da
Barra”的堡壘，後來在1634年建造了[聖米格爾堡](../Page/聖米格爾堡.md "wikilink")，1765年至1766年間建立了Forte
de São Francisco do Penedo。其中圣米格爾堡還保存甚好。\[1\]

1550年至1836年之間的大部分時間，羅安達都是葡萄牙向[巴西](../Page/殖民地巴西.md "wikilink")[奴隸貿易的橋頭堡](../Page/安哥拉奴隸貿易.md "wikilink")，大批商人從中獲益。只有在1640年到1648年的短暫幾年中處於荷蘭人的管理下，這段時期羅安達被稱作艾倫堡（Fort
Aardenburgh）。\[2\]\[3\]

17世紀時，[姆班加拉人代替](../Page/姆班加拉人.md "wikilink")[安本杜人成為奴隸的主要提供者](../Page/安本杜人.md "wikilink")，1750年代每年都有五千至一萬奴隸在羅安達市場上銷售。\[4\]

1889年，安哥拉總督[埃梅內希多·卡佩羅建造了給城市輸水的水槽](../Page/埃梅內希多·卡佩羅.md "wikilink")，為羅安達的迅速發展製造了有利條件。像其他[葡屬安哥拉地區一樣](../Page/葡屬安哥拉.md "wikilink")，羅安達並沒有受到[葡萄牙殖民地戰爭的影響](../Page/葡萄牙殖民地戰爭.md "wikilink")\[5\]，因此羅安達在這段時期的發展極快。在葡萄牙[新國家時期](../Page/新國家_\(葡萄牙\).md "wikilink")，羅安達都會人口從1940年時61,208人、且14.6%是白人，上升至1970年的475,328人、且有26.3%為歐洲人，另有50,000人為混血\[6\]\[7\]。

### 獨立

安哥拉在[安哥拉獨立戰爭中脫離](../Page/安哥拉獨立戰爭.md "wikilink")[葡萄牙獨立](../Page/葡萄牙.md "wikilink")，國家由外族（葡萄牙人）統治的狀況也從此不復存在。1974年，葡萄牙里斯本爆發[康乃馨革命](../Page/康乃馨革命.md "wikilink")，再加上後來發生於1975年至2002年間的[安哥拉內戰](../Page/安哥拉內戰.md "wikilink")，大多數葡萄牙人都離開了羅安達\[8\]。這導致了當地居民因缺乏管理城市經驗而使得城市陷入混亂，一支[古巴軍隊被派往羅安達以支持](../Page/古巴.md "wikilink")[安哥拉人民解放運動](../Page/安哥拉人民解放運動.md "wikilink")，并提供了大量技術工人。然而由於長期內戰，羅安達貧民窟的數量也逐年增長，而社會不公也成了很嚴重的問題。2002年內戰停止後，通過出口石油和鑽石，經濟開始復甦，羅安達的大規模重建工作隨之開始\[9\]。

## 地理

### 人文

[Luanda_13.23266E_8.80761S.jpg](https://zh.wikipedia.org/wiki/File:Luanda_13.23266E_8.80761S.jpg "fig:Luanda_13.23266E_8.80761S.jpg")
[Igreja_de_Nossa_Senhora_dos_Remédios_(19929834976).jpg](https://zh.wikipedia.org/wiki/File:Igreja_de_Nossa_Senhora_dos_Remédios_\(19929834976\).jpg "fig:Igreja_de_Nossa_Senhora_dos_Remédios_(19929834976).jpg")
羅安達是[羅安達省的七個](../Page/羅安達省.md "wikilink")[自治市之一](../Page/安哥拉自治市列表.md "wikilink")，可以分為老城區“Baixa
de Luanda”和新城區“Cidade Alta”，老城區靠近羅安達港，充滿狹窄的街道和舊式殖民地建築
\[10\]。但一些現代化的建築也開始出現，2012年還開始了衛星城南羅安達（Luanda
Sul）的建造。

羅安達屬於[天主教羅安達總教區所轄](../Page/天主教羅安達總教區.md "wikilink")，有一位[羅馬天主教大主教](../Page/羅馬天主教.md "wikilink")，而當地也有教會創辦的大學[安哥拉天主教大學](../Page/安哥拉天主教大學.md "wikilink")。公立大學則為[阿戈什蒂紐内圖大學](../Page/阿戈什蒂紐内圖大學.md "wikilink")。安哥拉的主要體育場[城堡體育場也位於羅安達](../Page/城堡體育場.md "wikilink")，該體育場可容納六萬人。\[11\]

### 氣候

羅安達處於[半乾旱氣候區](../Page/半乾旱氣候.md "wikilink")（[柯本氣候分類法](../Page/柯本氣候分類法.md "wikilink")：BSh），由於[本吉拉寒流的影響](../Page/本吉拉寒流.md "wikilink")，常年氣候炎熱乾旱。六月到十月間夜間有頻繁大霧，而氣溫也因此難以下降。羅安達的年降雨量為，但可變性居全球前列\[12\]。其觀測記錄自1858年開始，較低值有1958年的，較高值則有1916年的。三月至四月間為短暫的雨季，受強弱不同的本吉拉寒流影響，降雨量之間的差距可達六倍\[13\]\[14\]。

## 人口

羅安達以非洲人居多，主要是[安本杜人](../Page/安本杜人.md "wikilink")、[奧文本杜人和](../Page/奧文本杜人.md "wikilink")[巴剛果人](../Page/巴剛果人.md "wikilink")。官方的和最廣泛使用的語言是[葡萄牙語](../Page/葡萄牙語.md "wikilink")，此外數個[班圖語支的語言](../Page/班圖語支.md "wikilink")，例如[金邦杜語](../Page/金邦杜語.md "wikilink")、[姆班杜語和](../Page/姆班杜語.md "wikilink")[剛果語](../Page/剛果語.md "wikilink")，也比較流行。羅安達也有一定數量的歐洲人，例如[葡萄牙人就有約](../Page/葡萄牙人.md "wikilink")120,000，此外[巴西人和](../Page/巴西人.md "wikilink")[拉丁美洲人也有一些](../Page/拉丁美洲人.md "wikilink")。在20世紀末，[華人社區開始形成](../Page/華人.md "wikilink")，[南非等非洲國家也有一定量的移民到此](../Page/南非.md "wikilink")。2000年代中期，因為葡萄牙經濟危機，移民至此的葡萄牙人開始增多。\[15\]

因為羅安達相對其他城市較為安全，本國人也會移民至此\[16\]。但實際上除去以移民為主的都市中心，羅安達其他地區的犯罪率近年正在上升\[17\]。

## 經濟

[Avenida_Amilcar_Cabral_Luanda_March_2013_03.JPG](https://zh.wikipedia.org/wiki/File:Avenida_Amilcar_Cabral_Luanda_March_2013_03.JPG "fig:Avenida_Amilcar_Cabral_Luanda_March_2013_03.JPG")
[Bairro_Marçal_Luanda_01.JPG](https://zh.wikipedia.org/wiki/File:Bairro_Marçal_Luanda_01.JPG "fig:Bairro_Marçal_Luanda_01.JPG")
大約三分之一的安哥拉人都生活在羅安達，其中53%處於貧困線之下，因而大多數人的生活條件也很差，電力和水資源依舊短缺，交通狀況也堪憂\[18\]。而另一方面，少數富有者卻建造了很多奢華的建築。對外國人來說，羅安達是世界上[生活費用最昂貴城市之一](../Page/外派員工生活費用最昂貴城市列表.md "wikilink")\[19\]。

2014年3月採用新的進口關稅，而物價更是飛漲。半升香草冰激凌的價格可達31美元，實際上，從蒜到汽車價格無一不高。雖然初衷是保護本地工業和農業，但實際上這種高關稅只造成了人民生活困苦。\[20\]

製造業主要生產便利食品、飲料、織物和衣物、水泥的等建築材料、塑料產品、金屬器皿、香煙等。羅安達有石油精煉廠，雖然石油工廠在內戰中受到破壞，石油業從業人員工資仍然高達平均工資的20倍，為5400美元\[21\]。羅安達港是天然港，主要在此出口咖啡、棉花、糖、鑽石、鐵和鹽等，隨著經濟發展，建築業日益興隆。\[22\]

## 交通

羅安達是[羅安達鐵路的起始點](../Page/羅安達鐵路.md "wikilink")，這條鐵路通往東部的[馬蘭哲](../Page/馬蘭哲.md "wikilink")。內戰期間鐵路被毀，但現在經過修復已經可以重新通往馬蘭哲了\[23\]。不同於鐵路，羅安達的公路較為破敗，政府正在修繕中。\[24\]

羅安達的主要機場為[二月四日國際機場](../Page/二月四日國際機場.md "wikilink")，此機場也是安哥拉最大的機場。新的國際機場[安哥拉國際機場的第一期工程已在](../Page/安哥拉國際機場.md "wikilink")2010年完工\[25\]，本来預計2015/2016年即可交付使用\[26\]，但因資金糾紛而延期至2020年左右\[27\]。羅安達港是安哥拉最大的港口，大部分的進出進口貿易都在此完成，其規模也在擴大。

## 参考文献

## 外部連結

  - [Portal da Cidade de Luanda](http://www.portalluanda.com)
  - [www.cidadeluanda.com - Luanda, city map, History,
    Photos](http://arquivo.pt/wayback/20081022131423/http%3A//www.cidadeluanda.com/)

[羅安達](../Category/羅安達.md "wikilink")
[Category:安哥拉城市](../Category/安哥拉城市.md "wikilink")
[Category:安哥拉港口](../Category/安哥拉港口.md "wikilink")
[Category:非洲首都](../Category/非洲首都.md "wikilink")
[Category:大西洋沿海城市](../Category/大西洋沿海城市.md "wikilink")

1.

2.  See Joseph Miller, *Way of Death: Merchant Capitalism and the
    Angolan Slave Trade*, London & Madison/Wis,: James Currey &
    University of Wisconsin Press, 1988

3.

4.

5.

6.  [Angola antes da
    Guerra](http://www.youtube.com/watch?v=RsD7bjz6ILY), a film of
    Luanda, [Portuguese Angola](../Page/Portuguese_Angola.md "wikilink")
    (before 1975), youtube.com

7.  [LuandaAnosOuro.wmv](http://www.youtube.com/watch?v=U-PY1Z0_Ee8), a
    film of Luanda, Portuguese Angola (before 1975), youtube.com

8.  [Flight from
    Angola](http://www.economist.com/world/mideast-africa/displayStory.cfm?story_id=12079340),
    [The Economist](../Page/The_Economist.md "wikilink") (August 16,
    1975).

9.  [The Economist: Marching towards riches and
    democracy?](http://www.economist.com/node/12009946?story_id=12009946)
    August 28, 2008

10. [MBRO%20de%202010.pdf Streets of Luanda from the Luanda Provincial
    Government
    website](http://www.gpl.gv.ao/Revista/VIAS%20DE%20LUANDA%20-%20Relat%C3%B3rio%20de%20Actividades%20n%C2%BA29%20-%20NOVEMBRO%20e%20DEZEom)
    new pictures from Luanda City (Portuguese)

11. [Estádio da Cidadela](http://www.zerozero.pt/estadio.php?id=1064)

12. Dewar, Robert E. and Wallis, James R; "Geographical patterning in
    interannual rainfall variability in the tropics and near tropics: An
    L-moments approach"; in *Journal of Climate*, 12; pp. 3457-3466

13. [Bank Quantum website with citing Geography & Climate conditions in
    Angola](http://www.bancoquantum.com/en/angola/overview/geography-a-climate)


14. [Video from heavy rain falls in
    Luanda](http://traveltoafrica.info/chuva-em-luanda-angola.html)
    December 28, 2010

15.

16.

17.

18. [Keeping the flow in Angola's
    slums](http://www.dfid.gov.uk/Media-Room/Case-Studies/2009/Keeping-the-flow-in-Angolas-slums/)
    , Department for International Development (DFID), United Kingdom
    (February 13, 2009)

19.

20.

21.
22.

23.

24.

25. [Novo aeroporto orgulha
    África](http://jornaldeangola.sapo.ao/20/0/novo_aeroporto_orgulha_africa)
    Jornal de Angola Online, 12 May 2012 (en)

26. [Interview with Pimentel Araujo about the Angola air
    traffic](http://www.aeroscope.de/magazin-nachrichten/items/taag-durchstarter-aus-angola#.UFxb6Y2TuV9)
    , aeroscope.com, 21 August 2012 (de)

27. [New Luanda international airport in Angola expected to start
    operating
    in 2019/2020](https://macauhub.com.mo/2017/10/31/pt-novo-aeroporto-internacional-de-luanda-angola-em-funcionamento-em-20192020/),
    2017-11-01