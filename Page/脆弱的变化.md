《**脆弱的变化**》（）是一部探讨[九一一袭击事件的纪录片](../Page/九一一袭击事件.md "wikilink")，由迪伦·埃弗里执导，科里·罗和贾森·伯默斯制作。影片认为该起袭击事件的历史纪录并不寻常，并据此宣称袭击是由[美国政府内部人员策划并实行的](../Page/美国政府.md "wikilink")。

影片最初经由制作者的公司“”发表，在[纽约州](../Page/纽约州.md "wikilink")[宾厄姆顿一家隶属](../Page/宾厄姆顿_\(纽约州\).md "wikilink")[福克斯广播公司的电视台](../Page/福克斯广播公司.md "wikilink")播出后获得广泛关注\[1\]。

一些媒体\[2\]\[3\]、独立研究者\[4\]\[5\]以及科学和工程界的重要人物\[6\]认为影片中的理论有误。但一些与[九一一陰謀論相关的学者则认同影片中的部分理论](../Page/九一一陰謀論.md "wikilink")\[7\]。

## 参见

  - [九一一阴谋论](../Page/九一一阴谋论.md "wikilink")

## 注释

## 外部链接

  - [官方网站](http://www.loosechange911.com/)

  - [官方网站最后剪辑版本](https://web.archive.org/web/20080516223354/http://lc911finalcut.com/)

  - [官方论坛](http://s1.zetaboards.com/LooseChangeForums/index/)

  -
  -
  -
  -
[Category:2005年电影](../Category/2005年电影.md "wikilink")
[Category:九一一事件陰謀論](../Category/九一一事件陰謀論.md "wikilink")
[Category:美國纪录片](../Category/美國纪录片.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  参考以下2个网站：[Scholars for 911 Truth website
    homepage](http://911scholars.org/)、[Scholars for 911 Truth and
    Justice website homepage](http://stj911.org/)