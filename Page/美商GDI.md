**美商GDI**的全名是**Global Domains
International.inc**（**全球網域國際公司**），是一個採[直銷方式的網路服務公司](../Page/直銷.md "wikilink")。在1999年在[加州成立](../Page/加州.md "wikilink")。提供[網域](../Page/網域.md "wikilink")（主要是[.ws網域](../Page/.ws.md "wikilink")，也提供其他網域）和[主機空間服務](../Page/主機空間.md "wikilink")。

## 沿革

  - 2002年，报導，GDI被列入2002年全美500大快速成长的私人企业的第37名，是加州的第五名\[1\]。

<!-- end list -->

  - 2007年，美商GDI正式成为会员\[2\]。

## 爭議

### 中國大陸

  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")[湖北省](../Page/湖北省.md "wikilink")[工商行政管理局发布的](../Page/工商行政管理局.md "wikilink")《湖北省[工商局](../Page/工商局.md "wikilink")2009年打击传销1号警示》中称，美商GDI为湖北省“网络传销案例，需要警惕。”\[3\]
  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[工商行政管理局局长信箱对于关于美商GDI的疑问的的答复是](../Page/工商行政管理局.md "wikilink")：“从该网站奖金方案的说明中可知，涉嫌团队计酬，是国务院《禁止传销条例》中禁止的一种传销行为。请勿加入。”\[4\]

### 臺灣

## 參考資料

## 外部連結

  - [GDI官方網站](http://www.website.ws/)

[Category:美国公司](../Category/美国公司.md "wikilink")

1.
2.
3.
4.