**小亨利·华伦·“汉克”·哈特斯菲尔德**（，）曾是一位[美国国家航空航天局的](../Page/美国国家航空航天局.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")，执行过[STS-3以及](../Page/STS-3.md "wikilink")[STS-51-F任务](../Page/STS-51-F.md "wikilink")。

## 外部链接

  - [美国国家航空航天局网站的哈特斯菲尔德介绍](http://www.jsc.nasa.gov/Bios/htmlbios/hartsfield-hw.html)

[H](../Category/美国宇航员.md "wikilink")
[H](../Category/美國空軍上校.md "wikilink")
[H](../Category/第七组宇航员.md "wikilink")