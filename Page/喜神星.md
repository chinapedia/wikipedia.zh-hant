****（Laetitia）是第39颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1856年2月8日发现。的[直径为](../Page/直径.md "wikilink")149.5千米，[质量为](../Page/质量.md "wikilink")3.5×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1682.713天。

## 參考文獻

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1856年发现的小行星](../Category/1856年发现的小行星.md "wikilink")