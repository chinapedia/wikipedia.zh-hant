**毛公鼎**是[西周](../Page/西周.md "wikilink")[宣王年間](../Page/周宣王.md "wikilink")（[前828年](../Page/前828年.md "wikilink")－[前782年](../Page/前782年.md "wikilink")）所鑄造的[青銅](../Page/青銅.md "wikilink")[鼎](../Page/鼎.md "wikilink")，腹內刻有500字[金文冊命書](../Page/金文.md "wikilink")，字數為舉世銘文[青銅器中最多](../Page/青銅器.md "wikilink")\[1\]\[2\]。是西周[散文](../Page/散文.md "wikilink")[代表作](../Page/代表作.md "wikilink")，其[書法也是](../Page/書法.md "wikilink")[金文中最高等級](../Page/金文.md "wikilink")，故有「抵得一篇《[尚書](../Page/尚书_\(书籍\).md "wikilink")》」\[3\]、晚[清](../Page/清朝.md "wikilink")「四大[國寶](../Page/國寶.md "wikilink")」、「青銅三寶」之譽。現收藏於[臺北](../Page/臺北.md "wikilink")[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")，被列為[中華民國國寶](../Page/中華民國國寶.md "wikilink")。

## 形制

[缩略图](https://zh.wikipedia.org/wiki/File:Mao_Gong_ding_inscription.png "fig:缩略图")

### 外觀

《毛公鼎》通高53.8[公分](../Page/公分.md "wikilink")，腹深27.2公分，口徑47.9公分，重34.7[公斤](../Page/公斤.md "wikilink")，大口圓腹，口沿有一道重環紋飾，上有兩隻大耳，腹下三隻獸蹄形足。\[4\]\[5\]

### 銘文原文

《毛公鼎》腹內鑄有32行、共500字[銘文](../Page/銘文.md "wikilink")，是西周晚期一篇完整的冊命書。文中提到[周宣王在位初期](../Page/周宣王.md "wikilink")，想要振興朝政，遂命[叔父毛公瘖](../Page/叔父.md "wikilink")（有一說是毛公歆）處理國家大小事務，又命毛公一族擔任[禁衛軍](../Page/禁衛軍.md "wikilink")，保衛王家，並賜酒食、輿服、兵器。毛公感念周王，於是鑄鼎紀事，由子孫永寶永享。\[6\]\[7\]

## 流傳

據[贺世明考证](../Page/贺世明.md "wikilink")，《毛公鼎》於[清宣宗](../Page/清宣宗.md "wikilink")[道光二十三年](../Page/道光.md "wikilink")（1843年）在[陝西](../Page/陝西.md "wikilink")[岐山](../Page/岐山.md "wikilink")[周原](../Page/周原.md "wikilink")（今[陕西省](../Page/陕西省.md "wikilink")[岐山县](../Page/岐山县.md "wikilink")）\[8\]被[董家村村民董春生在村西地裡所挖掘](../Page/董家村.md "wikilink")，[北京](../Page/北京.md "wikilink")[永和齋古董商蘇兆年](../Page/永和齋.md "wikilink")、蘇億年兄弟聞訊而來\[9\]，以[白银](../Page/白银.md "wikilink")300[两购得](../Page/两.md "wikilink")，但運輸之際，被另一村民董治官所阻，买卖没有做成。古董商以重金行賄知縣，董治官被逮下獄，以私藏国宝治罪。此鼎最後運到縣府，被古董商人悄悄運走，密藏於[西安](../Page/西安.md "wikilink")。\[10\][张燕昌之子](../Page/张燕昌.md "wikilink")[张石瓠曾巧見此鼎](../Page/张石瓠.md "wikilink")，便把鼎内銘文摹繪成[雙鈎圖](../Page/雙鈎圖.md "wikilink")，寄給[浙江](../Page/浙江.md "wikilink")[嘉興名士](../Page/嘉興.md "wikilink")[徐同柏](../Page/徐同柏.md "wikilink")，寫了《[周毛公鼎考釋](../Page/周毛公鼎考釋.md "wikilink")》文章。

[清文宗](../Page/清文宗.md "wikilink")[咸丰二年](../Page/咸丰_\(年号\).md "wikilink")（1852年），[北京](../Page/北京.md "wikilink")[金石学家](../Page/金石学家.md "wikilink")、[收藏家](../Page/收藏家.md "wikilink")[陳介祺又從蘇億年手中購得](../Page/陳介祺.md "wikilink")\[11\]，並賞給蘇億年1,000[两](../Page/两.md "wikilink")，此鼎深藏於密室，鮮為人知。陈介祺病故後，其孫陳孝笙於[宣統二年](../Page/宣統.md "wikilink")（1910年）以萬兩白銀將鼎轉售給[兩江總督](../Page/兩江總督.md "wikilink")[端方](../Page/端方.md "wikilink")，後端方被派到[四川镇压](../Page/四川.md "wikilink")[保路运动](../Page/保路运动.md "wikilink")，被[革命军所杀](../Page/革命军.md "wikilink")。端方之後人因家道中落，将此鼎典押給[天津](../Page/天津.md "wikilink")[俄国人开办的](../Page/俄国人.md "wikilink")[华俄道胜银行](../Page/华俄道胜银行天津分行大楼.md "wikilink")。[英国记者](../Page/英国.md "wikilink")[辛浦森出](../Page/辛浦森.md "wikilink")5萬美元\[12\]向端家購買，端家嫌錢太少，不肯割愛。当时有爱国人士極力呼籲保護國寶，《毛公鼎》輾轉至当时担任[北洋政府](../Page/北洋政府.md "wikilink")[交通总长的大收藏家](../Page/交通总长.md "wikilink")、后来[国学馆馆长](../Page/国学馆.md "wikilink")[葉恭綽手中](../Page/葉恭綽.md "wikilink")\[13\]，存入[大陆银行](../Page/大陆银行.md "wikilink")。

民國二十六年（1937年）[抗日戰爭爆發](../Page/抗日戰爭.md "wikilink")，葉恭綽避走[香港](../Page/香港.md "wikilink")，此鼎則藏在[上海寓所未能帶走](../Page/上海.md "wikilink")。由于叶恭绰是用假名購得此鼎，日本人无法查知它的下落。\[14\]叶恭绰嘱咐其侄[葉公超](../Page/葉公超.md "wikilink")：「美国人和日本人两次出高价购买《毛公鼎》，我都没有答应。现在我把它托付给你，不得变卖，不得典质，更不能让它出国。有朝一日，可以献给国家。」\[15\]之後葉公超被[日本軍方逼問時堅持不供出](../Page/日本.md "wikilink")《毛公鼎》的下落，叶恭绰为救侄子，制造了一只假鼎上交日军。叶公超被释放后，于1941年密携《毛公鼎》逃往香港。不久，香港被[日军攻占](../Page/日军.md "wikilink")，叶家托[德国友人辗转將鼎運回上海](../Page/德国.md "wikilink")。后来因生活困頓而将鼎典押給银行，但巨商[陈詠仁出资赎出](../Page/陈詠仁.md "wikilink")，使《毛公鼎》不至於流浪他鄉。民國三十五年（1946年）陈詠仁将此鼎捐献给[國民政府](../Page/國民政府.md "wikilink")，次年由上海运至[南京](../Page/南京.md "wikilink")，收藏於[中央博物院](../Page/中央博物院.md "wikilink")（今[南京博物院](../Page/南京博物院.md "wikilink")）。\[16\]

1948年，大量故宮珍贵文物隨[中華民國政府至](../Page/中華民國政府.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，此鼎亦在其中，现收藏於-{zh-cn:台;
zh-tw:臺;}-北[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")。\[17\]近年來與[翠玉白菜](../Page/翠玉白菜.md "wikilink")、[肉形石並列合稱](../Page/肉形石.md "wikilink")「[故宮三寶](../Page/故宮三寶.md "wikilink")」。

## 價值

《毛公鼎》腹內500字金文是全世界鑄銘青銅器中最多者，冊命文訓誥辭華美且古奧艱深，是西周散文代表作，[郭沫若認為](../Page/郭沫若.md "wikilink")《毛公鼎》：「銘全體氣勢頗宏大，泱泱然存宗周宗主之風烈，此於宣王之時爲宜」，並譽「抵得一篇《尚書》」。\[18\]其書法也是金文中最高等級，[李瑞清在](../Page/李瑞清.md "wikilink")《毛公鼎》銘拓跋文說道：「《毛公鼎》為周廟堂文字，其文則《尚書》也，學書不學《毛公鼎》，猶[儒生不讀](../Page/儒生.md "wikilink")《尚書》也。」故列名晚清「四大國寶」、「海內三寶」之一。\[19\]\[20\]

## 相關

  - [宣王中興](../Page/宣王中興.md "wikilink")
  - [金文](../Page/金文.md "wikilink")
  - [訓詁學](../Page/訓詁學.md "wikilink")

## 參考資料

### 注釋

<div class="references-small">

### 文獻

<references/>

</div>

## 参见

  - [散氏盤](../Page/散氏盤.md "wikilink")
  - [大盂鼎](../Page/大盂鼎.md "wikilink")
  - [大克鼎](../Page/大克鼎.md "wikilink")
  - [虢季子白盤](../Page/虢季子白盤.md "wikilink")

[銅](../Category/国立故宫博物院藏品.md "wikilink")
[Category:中華民國國寶](../Category/中華民國國寶.md "wikilink")
[Category:西周青铜器](../Category/西周青铜器.md "wikilink")
[Category:宝鸡文物](../Category/宝鸡文物.md "wikilink")
[Category:鼎](../Category/鼎.md "wikilink")
[Category:中国文化之最](../Category/中国文化之最.md "wikilink")

1.

2.

3.

4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.

16.
17.
18.
19.
20.