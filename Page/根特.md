**根特**（ ，；，；，；过去在英语中为*Gaunt*
）（舊譯岡城、崗城、崗省）是[比利时](../Page/比利时.md "wikilink")[弗拉芒大区的一个](../Page/弗拉芒大区.md "wikilink")[自治市](../Page/自治市.md "wikilink")，也指这个自治市中的根特城。

根特源於[利斯河和](../Page/利斯河.md "wikilink")[斯海尔德河匯合的](../Page/斯海尔德河.md "wikilink")[凱爾特定居點](../Page/凯尔特人.md "wikilink")。中世纪時，因為羊毛和亚麻產业發展蓬勃，根特成為欧洲最大的城市之一。當時根特也具有儲存糧食的權利。16至18世纪末根特遇上了經濟方面的挫折。棉花产业卻讓根特變成歐洲的第一座工业城市之一。

根特自治市据2015年4月统计有人口25万4千，为比利时第二大自治市，它地处交通要冲，历史悠久，现在仍为旅游胜地。

## 地理位置

根特自治市由根特城和周围的一些小镇组成，其中的根特市是[东弗兰德省的最大的城市与省会](../Page/东弗兰德省.md "wikilink")。根特位于交通要道，为[斯海尔德河和](../Page/斯海尔德河.md "wikilink")[利斯河汇合处](../Page/利斯河.md "wikilink")，并通过[根特-泰尔讷曾运河](../Page/根特-泰尔讷曾运河.md "wikilink")（Kanaal
Gent-Terneuzen）与海相连，处于两条[欧洲高速公路](../Page/欧洲高速公路.md "wikilink")[E17和](../Page/欧洲E17公路.md "wikilink")[E40的交界点上](../Page/欧洲E40公路.md "wikilink")，并有比利时第三大的火车站。

## 历史

考古发现在[石器时代与](../Page/石器时代.md "wikilink")[铁器时代根特地区就已经有人类活动](../Page/铁器时代.md "wikilink")，有些学者认为根特一词即来源于[凯尔特语中的汇合一词](../Page/凯尔特语.md "wikilink")。尽管没有书面记载，但是[考古学家已经确定在](../Page/考古学家.md "wikilink")[古罗马时期根特已经形成了城市](../Page/古罗马.md "wikilink")。4世纪末到5世纪初，[法兰克人侵入罗马](../Page/法兰克人.md "wikilink")，使得当地语言从凯尔特语和[拉丁语变成了古](../Page/拉丁语.md "wikilink")[荷兰语](../Page/荷兰语.md "wikilink")。

约在公元650年左右，圣徒[阿芒](../Page/阿芒.md "wikilink")（Saint
Amand）在根特建立了两所隐修院——[圣彼得修道院](../Page/圣伯多禄隐修院_\(根特\).md "wikilink")（Sint-Pietersabdij）和[圣巴夫修道院](../Page/圣巴夫修道院.md "wikilink")（Sint-Baafsabdij）。由于交通便利，根特逐渐发展起来，成为隐修院众多的地方及商业中心。大约公元800年时，[查理大帝的儿子](../Page/查理大帝.md "wikilink")，后来的[路易一世命令查理大帝的传记作者](../Page/虔誠者路易.md "wikilink")[艾因哈德掌管根特所有的](../Page/艾因哈德.md "wikilink")[修道院](../Page/修道院.md "wikilink")。851年和859年根特曾两次遭到[维京](../Page/維京人.md "wikilink")[海盗的攻击和洗劫](../Page/海盗.md "wikilink")。

根特从11世纪开始复兴，到13世纪时，根特已经成为仅次于[巴黎的欧洲最大最繁荣的城市之一](../Page/巴黎.md "wikilink")。城市人口达到65000人。今天仍然矗立在根特市中心的[钟楼和](../Page/根特钟楼.md "wikilink")[圣尼古拉教堂就是根特兴盛时期的建筑遗留](../Page/圣尼古拉教堂_\(根特\).md "wikilink")。根特的繁荣主要源于[羊毛](../Page/羊毛.md "wikilink")[纺织业](../Page/纺织业.md "wikilink")，由于地处河流的交汇处，根特的很多地域经常遭到水的淹没，形成独特的多草而又潮湿的地域，很适合放牧绵羊。获得的羊毛就用于生产纺织品，随后大量出口到[英国](../Page/英国.md "wikilink")，所以在英法[百年战争中根特的经济受到了严重的打击](../Page/百年战争.md "wikilink")。

根特在百年战争之后通过和[勃艮第公爵领下的其他属地建立贸易联盟而得到了复苏](../Page/勃艮第公爵.md "wikilink")，但好景不长，过重的税收引起了根特人民的反抗，最终导致了[哈弗尔战役](../Page/哈弗尔战役.md "wikilink")（Slag
bij
Gavere），根特的反抗被镇压，有16000居民死亡。[低地国家的中心也从](../Page/低地国家.md "wikilink")[布鲁日](../Page/布鲁日.md "wikilink")－**根特**为代表城市的[弗兰德转移到](../Page/弗兰德.md "wikilink")[安特卫普](../Page/安特卫普.md "wikilink")－[布鲁塞尔为代表城市的](../Page/布鲁塞尔.md "wikilink")[布拉班特公爵领地](../Page/布拉班特公爵.md "wikilink")。1539年根特发生反抗[神圣罗马帝国皇帝和](../Page/神聖羅馬皇帝.md "wikilink")[西班牙国王](../Page/西班牙国王.md "wikilink")[查理五世的](../Page/查理五世.md "wikilink")[根特反抗](../Page/根特反抗.md "wikilink")（Gentse
Opstand），查理五世亲临自己的故乡根特进行镇压，并命令根特的贵族赤脚带镣走过他的面前。16世纪末到17世纪，根特因为是[加尔文宗的城市而卷入](../Page/加尔文宗.md "wikilink")[宗教战争](../Page/宗教战争.md "wikilink")，被[西班牙帝国给予毁灭性打击](../Page/西班牙帝国.md "wikilink")，并改奉[天主教](../Page/天主教.md "wikilink")，从此根特失去了国际化商业中心的地位。

[Lieven_Bauwens_statue_in_Ghent.jpg](https://zh.wikipedia.org/wiki/File:Lieven_Bauwens_statue_in_Ghent.jpg "fig:Lieven_Bauwens_statue_in_Ghent.jpg")
1800年[比利时工业](../Page/比利时.md "wikilink")[间谍](../Page/间谍.md "wikilink")[利芬·鲍文斯](../Page/利芬·鲍文斯.md "wikilink")（Lieven
Bauwens）将[英国的机密纺织技术带到根特](../Page/英国.md "wikilink")，使得根特再次因[纺织工业而复兴](../Page/纺织工业.md "wikilink")，鲍文斯也曾担任一年的根特市长，他所带来的纺织机器，至今仍然保存在根特城东北部的[工业博物馆](../Page/工业博物馆.md "wikilink")（Industriemuseum）里。1814年12月24日，[英国和](../Page/英国.md "wikilink")[美国关于停止](../Page/美国.md "wikilink")[1812年战争的](../Page/1812年战争.md "wikilink")[根特条约在此签订](../Page/根特条约.md "wikilink")。[滑铁卢战役后根特被划归](../Page/滑铁卢战役.md "wikilink")[荷兰](../Page/荷兰.md "wikilink")15年，其间根特拥有了自己的[根特大学和入海通道](../Page/根特大学.md "wikilink")。

比利时独立后，在根特建立了第一个比利时贸易同盟。1913年[世界博览会在根特举行](../Page/世界博览会.md "wikilink")，为准备这次博览会，1912年根特修建了[圣彼得火车站](../Page/圣彼得火车站.md "wikilink")（Station
Gent-Sint-Pieters）。

## 建筑

### 宗教建筑

  - [圣巴夫主教座堂](../Page/圣巴夫主教座堂.md "wikilink")（Sint-Baafskathedraal）
  - [圣尼古拉教堂](../Page/圣尼古拉教堂_\(根特\).md "wikilink")（Sint-Nkilaaskerk）
  - [圣米迦勒教堂](../Page/聖米迦勒教堂_\(根特\).md "wikilink")（Sint-Michielskerk）
  - [圣雅各教堂](../Page/圣雅各教堂_\(根特\).md "wikilink")（Sint-Jacobskerk）
  - [圣亚纳教堂](../Page/圣亚纳教堂_\(根特\).md "wikilink")（Sint-Annakerk）
  - [露德圣母圣殿](../Page/露德圣母圣殿_\(乌斯塔克\).md "wikilink")（Onze-Lieve-Vrouw van
    Lourdes Basiliek）
  - [圣巴夫修道院](../Page/圣巴夫修道院.md "wikilink")（Sint-Baafsabdij）
  - [圣彼得修道院](../Page/圣彼得修道院_\(根特\).md "wikilink")（Sint-Pietersabdij）
  - [小贝居安会院](../Page/小贝居安会院_\(根特\).md "wikilink")（Klein Begijnhof）
  - [圣伊撒伯尔大贝居安会院](../Page/圣伊撒伯尔大贝居安会院.md "wikilink")（Groot Begijnhof
    Sint-Elisabeth）
  - [大贝居安会院](../Page/大贝居安会院_\(圣阿芒斯堡\).md "wikilink")（Groot Begijnhof
    (Sint-Amandsberg)）

### 世俗建筑

  - [根特市政厅](../Page/根特市政厅.md "wikilink")
  - [根特钟楼](../Page/根特钟楼.md "wikilink")（Belfort van Gent）
  - [伯爵城堡](../Page/伯爵城堡.md "wikilink")（Gravensteen）
  - [恶魔城堡](../Page/恶魔城堡.md "wikilink")（Duivelsteen）
  - [拉伯特](../Page/拉伯特.md "wikilink")（[Rabot](../Page/:nl:Rabot_\(monument\).md "wikilink")）
  - [书塔](../Page/书塔.md "wikilink")（Boekentoren）
  - [亲王庭院](../Page/亲王庭院.md "wikilink")（Prinsenhof）[查理五世诞生之处](../Page/查理五世.md "wikilink")
  - [汉达港](../Page/汉达港.md "wikilink")（[Portus
    Ganda](../Page/:nl:Portus_Ganda.md "wikilink")）
  - [前进](../Page/前进_\(根特建筑\).md "wikilink")（[Vooruit](../Page/:en:Vooruit.md "wikilink")）
  - [Campo Santo根特的一座大公墓](../Page/Campo_Santo_\(根特\).md "wikilink")

### 博物馆

  - [美术博物馆](../Page/美术博物馆_\(根特\).md "wikilink")（Museum voor Schone
    Kunsten）
  - [根特设计博物馆](../Page/根特设计博物馆.md "wikilink")（Design Museum Gent）
  - [阿莱恩住宅](../Page/阿莱恩住宅.md "wikilink")（Huis van Alijn）
  - [工业博物馆](../Page/工业博物馆.md "wikilink")（Industriemuseum）
  - [根特市立博物馆](../Page/根特市立博物馆.md "wikilink")（Stadmuseum Gent）
  - [基娜的世界](../Page/基娜的世界.md "wikilink")（De wereld van
    Kina）一座自然博物馆，包括主馆和花园
  - [科学史博物馆](../Page/科学史博物馆_\(根特\).md "wikilink")（Museum voor de
    Geschiedenis van de Wetenschappen）
  - [市立当代艺术博物馆](../Page/市立当代艺术博物馆.md "wikilink")（Stedelijk Museum voor
    Actuele Kunst，简写作S.M.A.K.）
  - [珍石博物馆](../Page/珍石博物馆.md "wikilink")（Lapidary
    Museum）位于原[圣巴夫隐修院](../Page/圣巴夫隐修院.md "wikilink")，收藏从根特及其附近各处遗址收集来的墓石等。
  - [根特城市博物馆](../Page/根特城市博物馆.md "wikilink")（Stadsmuseum Gent）
  - [根特大学植物园](../Page/根特大学植物园.md "wikilink")（Ghent University Botanical
    Garden）
  - [阿诺德·范德·哈根博物馆](../Page/阿诺德·范德·哈根博物馆.md "wikilink")（Museum Arnold
    Vander Haeghen）
  - [吉兰医生博物馆](../Page/吉兰医生博物馆.md "wikilink")（Dr. Guislain
    Museum）一家有关精神病治疗的博物馆
  - [医史博物馆](../Page/医史博物馆_\(根特\).md "wikilink")（Museum voor Geschiedenis
    van de Geneeskunde）

### 广场和街道

  - [圣巴夫广场](../Page/圣巴夫广场.md "wikilink")（[Sint-Baafsplein](../Page/:nl:Sint-Baafsplein.md "wikilink")）
  - [埃米尔·布劳恩广场](../Page/埃米尔·布劳恩广场.md "wikilink")（[Emile Braun
    Plein](../Page/:nl:Emile_Braun_Plein.md "wikilink")）以比利时贵族[埃米尔·布劳恩](../Page/埃米尔·布劳恩.md "wikilink")（Emile
    Braun）的名字命名
  - [熟田广场](../Page/熟田广场.md "wikilink")（[Kouter](../Page/:nl:Kouter_\(Gent\).md "wikilink")）
  - [伍德罗·威尔逊广场](../Page/伍德罗·威尔逊广场.md "wikilink")（[W. Wilson
    Plein](../Page/:nl:W._Wilson_Plein.md "wikilink")）以美国总统[伍德罗·威尔逊名字命名](../Page/伍德罗·威尔逊.md "wikilink")
  - [弗兰德伯爵广场](../Page/弗兰德伯爵广场.md "wikilink")（[Graaf van
    Vlaanderenplein](../Page/:nl:Graaf_van_Vlaanderenplein.md "wikilink")）
  - [弗朗索瓦·洛朗广场](../Page/弗朗索瓦·洛朗广场.md "wikilink")（François
    Laurentplein）以卢森堡历史学家[弗朗索瓦·洛朗的名字命名](../Page/弗朗索瓦·洛朗.md "wikilink")
  - [圣法拉意弟广场](../Page/圣法拉意弟广场.md "wikilink")（Sint-Veerleplein）
  - [圣米迦勒广场](../Page/圣米迦勒广场.md "wikilink")（Sint-Michielsplein）
  - [圣彼得广场](../Page/圣彼得广场_\(根特\).md "wikilink")（Sint-Pietersplein）

<!-- end list -->

  - [圣米迦勒斜坡](../Page/圣米迦勒斜坡.md "wikilink")（Sint-Michielshelling）
  - [草巷](../Page/草巷.md "wikilink")（Graslei）
  - [玉米巷](../Page/玉米巷.md "wikilink")（Korenlei）
  - [神父洞](../Page/神父洞.md "wikilink")（Patershol）

### 公园

  - [阿尔伯特国王公园](../Page/阿尔伯特国王公园.md "wikilink")（Koning Albert Park）
  - [城塞公园](../Page/城塞公园.md "wikilink")（Citadel Park）

### 火车站

  - [根特圣彼得站](../Page/根特圣彼得站.md "wikilink")（Gent-Sint-Pieters）
  - [根特堤门站](../Page/根特堤门站.md "wikilink")（Gent-Dampoort）

## 与根特有关的人物

在根特出生，生活，去世的名人包括

  - [查理五世](../Page/查理五世_\(神圣罗马帝国\).md "wikilink")－西班牙国王，神圣罗马帝国皇帝，生于根特。
  - [莫里斯·梅特林克](../Page/莫里斯·梅特林克.md "wikilink")－比利时剧作家
  - [居伊·伏思达](../Page/居伊·伏思达.md "wikilink")－比利时首相
  - [雅克.羅格](../Page/雅克·罗格.md "wikilink")—前國際奧委會主席

## 注释

[Category:东佛兰德省市镇](../Category/东佛兰德省市镇.md "wikilink")