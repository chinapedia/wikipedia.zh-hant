**里夫共和国**(全名為'''里夫部落聯邦共和國，[阿拉伯語](../Page/阿拉伯語.md "wikilink"):
''')是居住在[西屬摩洛哥的](../Page/西屬摩洛哥.md "wikilink")[柏柏爾人建立的國家](../Page/柏柏爾人.md "wikilink")。1921年9月18日宣佈獨立。
共和國首都[阿杰迪尔](../Page/阿杰迪尔.md "wikilink")，貨幣為Riffan，[國慶日為](../Page/國慶日.md "wikilink")[9月18日](../Page/9月18日.md "wikilink")，[人口估計有](../Page/人口.md "wikilink")18,350人。獨立時[埃米爾為](../Page/埃米爾.md "wikilink")[阿卜杜·克里姆](../Page/阿卜杜·克里姆.md "wikilink")
。但共和國正式的建立為1923年2月1日，总统為阿卜杜·克里姆，1923年7月至1926年5月27日的[总理為ben](../Page/总理.md "wikilink")
Hajj
Hatmi。1926年5月27日在[法國和](../Page/法國.md "wikilink")[西班牙的干涉下解散](../Page/西班牙.md "wikilink")。

## 歷史

[柏柏爾人曾積極地抵抗](../Page/柏柏爾人.md "wikilink")[西班牙人和](../Page/西班牙.md "wikilink")[法國人對沙漠的侵略](../Page/法國.md "wikilink")。但是他們無法團結，部落之間不斷內訌和分裂，1912年，由於部落聯盟的破碎，他們對法國的殖民抗爭徹底失敗。

Abd el-Krim
是其中一個部落的一位軍閥和領導者，他利用其對西班牙的勝利去嘗試創造一個有實權的獨立國家，他先把西班牙人趕回幾個沿海前哨基地，再把法國人驅趕至南部。

1925年末，法國和西班牙人組成聯合軍隊反擊，1926年5月，聯邦滅亡。

法國國內的知识分子譴責法國統治階級的帝國主義。遊擊隊與法軍一直持續戰鬥直到 1927年。

[Category:已不存在的非洲國家](../Category/已不存在的非洲國家.md "wikilink")
[Category:摩洛哥](../Category/摩洛哥.md "wikilink")
[Category:已不存在的共和國](../Category/已不存在的共和國.md "wikilink")
[R](../Category/短命國家.md "wikilink")