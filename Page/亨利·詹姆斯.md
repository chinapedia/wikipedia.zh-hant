[Portrait_of_Henry_James_1913.jpg](https://zh.wikipedia.org/wiki/File:Portrait_of_Henry_James_1913.jpg "fig:Portrait_of_Henry_James_1913.jpg")
**亨利·詹姆斯**，[OM](../Page/功績勳章_\(英國\).md "wikilink")（**Henry
James**，），[英国以及](../Page/英国.md "wikilink")[美国的](../Page/美国.md "wikilink")[作家](../Page/作家.md "wikilink")。

他出身于[纽约的上层知识分子家庭](../Page/纽约.md "wikilink")，父亲老亨利·詹姆斯是著名学者，兄长[威廉·詹姆斯是知名的](../Page/威廉·詹姆斯.md "wikilink")[哲学家和](../Page/哲学家.md "wikilink")[心理学家](../Page/心理学家.md "wikilink")。詹姆斯本人长期旅居欧洲，对19世纪末美国和欧洲的上层生活有细致入微的观察。私生活方面，他终身未婚，有史学家认为他是[同性恋者](../Page/同性恋.md "wikilink")，他与同时代的[美国女](../Page/美国.md "wikilink")[作家](../Page/作家.md "wikilink")[伊迪丝·华顿保持着长期的友谊](../Page/伊迪丝·华顿.md "wikilink")。

20世纪末，詹姆斯的不少作品被搬上银幕，如《[仕女圖](../Page/仕女圖_\(小說\).md "wikilink")》、《[华盛顿广场](../Page/华盛顿广场_\(小说\).md "wikilink")》等，受到了广泛的关注。亨利·詹姆斯于1911年、1912年以及1916年多次获得[诺贝尔文学奖提名](../Page/诺贝尔文学奖.md "wikilink")。\[1\]

## 评价

“对亨利·詹姆斯的伟大我从未怀疑过，但在对其人其书了解之前，我无法揣测他究竟有多么伟大。”；“他早期的小说尽管精妙——但就完美而言，没有一部能比得上《仕女圖》”；“他属于旧式的美国”——[伊迪丝·华顿](../Page/伊迪丝·华顿.md "wikilink")

## 作品列表

  - *Roderick Hudson* (1875)

  - *Transatlantic Sketches* (1875)

  - 美国人，*The American* (1877)

  - [戴茜·米勒](../Page/戴茜·米勒.md "wikilink")，*Daisy Miller* (1878)

  - ，*The Europeans* (1878)

  - [仕女圖](../Page/仕女圖_\(小說\).md "wikilink")，*The Portrait of a Lady*
    (1881)

  - [华盛顿广场](../Page/华盛顿广场_\(小说\).md "wikilink")，*Washington Square*
    (1881)

  - *A Little Tour in France* (1884)

  - 波士顿人，*The Bostonians* (1886)

  - *The Princess Casamassima* (1886)

  - *The Aspern Papers* (1888)

  - *The Tragic Muse* (1890)

  - *Guy Domville* (play, 1895)

  - *The Spoils of Poynton* (1897)

  - *What Maisie Knew* (1897)

  - [碧廬冤孽](../Page/碧廬冤孽.md "wikilink")，*The Turn of the Screw* (1898)

  - *In the Cage* (1898)

  - *The Awkward Age* (1899)

  - [鴿翼](../Page/鴿翼.md "wikilink")，*The Wings of the Dove* (1902)

  - (或譯：奉使記)，*The Ambassadors* (1903)

  - *The Beast in the Jungle* (1903)

  - ，*The Golden Bowl* (1904)

  - *English Hours* (1905)

  - *The American Scene* (1907)

  - *Italian Hours* (1909)

## 参见

  - [美国文学](../Page/美国文学.md "wikilink")

## 外部链接

  - [英文小传](https://web.archive.org/web/20040404030719/http://www.kirjasto.sci.fi/hjames.htm)

[Category:美國小說家](../Category/美國小說家.md "wikilink")
[Category:英國小說家](../Category/英國小說家.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:愛爾蘭裔美國人](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")

1.