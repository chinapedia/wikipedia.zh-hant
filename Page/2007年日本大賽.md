2007年**[日本大賽](../Page/日本大賽.md "wikilink")**，是[日本職棒第](../Page/日本職棒.md "wikilink")58屆日本大賽，將由[中央聯盟的](../Page/中央聯盟.md "wikilink")[中日龍對](../Page/中日龍.md "wikilink")[太平洋聯盟的](../Page/太平洋聯盟.md "wikilink")[北海道日本火腿鬥士](../Page/北海道日本火腿鬥士.md "wikilink")，比賽在10月27日舉行一場比賽。一如以往，比賽採取七場四勝制。此外，兩隊球隊都是[2006年日本大賽的參賽隊伍](../Page/2006年日本大賽.md "wikilink")。兩隊均是由季後賽擊敗對手進入日本大賽。最後由[中日龍奪得總冠軍](../Page/中日龍.md "wikilink")，場數以4勝1負擊敗對手，自53年來奪得首個日本大賽冠軍。

## 比賽

### 第一場

日本火腿鬥士的達比修·有在比賽中共三振十三名球員，追平了[廣島東洋鯉魚的](../Page/廣島東洋鯉魚.md "wikilink")[外木場義郎在](../Page/外木場義郎.md "wikilink")[1975年日本大賽的單場三振記錄](../Page/1975年日本大賽.md "wikilink")（共4名球員曾平定此記錄）。而[Fernando
Seguignol第一局把握](../Page/Fernando_Seguignol.md "wikilink")[川上憲伸的失誤](../Page/川上憲伸.md "wikilink")，打出一支三分全壘打。而中日龍在進攻方面[森本將彥打出](../Page/森本將彥.md "wikilink")[犧牲打取得](../Page/犧牲打.md "wikilink")1分。而勝投和敗投的投手均以[完投結束第一場比賽](../Page/完投.md "wikilink")。\[1\]\[2\]。

### 第二場

第二場比賽，中日龍在第一局憑犧牲打搶得分數。第四局Andy
Green投球開始失準，連續保送三名球員，讓中日隊制造得分的機會，[中村紀洋為中日取得兩分後](../Page/中村紀洋.md "wikilink")，日本火腿更換投手。第四局下，[Fernando
Seguignol取得一支全壘打](../Page/Fernando_Seguignol.md "wikilink")。第六局及第七局由[李炳圭和](../Page/李炳圭.md "wikilink")[森野將彥打出兩分全壘打](../Page/森野將彥.md "wikilink")。讓中日龍隊取得勝利。\[3\]

### 第三場

中日龍的[朝倉健太在這場比賽中投了七局](../Page/朝倉健太.md "wikilink")，失了一分以及四個三振。在第一局下，中日的猛烈攻勢使[武田勝在領先](../Page/武田勝.md "wikilink")3-0後被迫換走，之後[Brian
Sweeney入替](../Page/Brian_Sweeney.md "wikilink")，在Sweeney多失兩分下，又被換走。在第三局，鬥士隊防守才能穩定下來。中日隊在這場比賽中的先發球員除了森野將彥外，皆能取得安打。\[4\]

### 第四場

日本火腿的[吉川光夫是自](../Page/吉川光夫.md "wikilink")1992年以來第一名新秀先發投手。但他在第一局造成滿壘，被李炳圭打出犧牲打，讓中日隊先取兩分。在第7局，中村紀洋取得安打並讓隊友得分，確保了中日隊的勝利。最後[岩瀨仁紀完成了第一次在日本大賽的](../Page/岩瀨仁紀.md "wikilink")[救援的工作](../Page/救援成功.md "wikilink")。\[5\]\[6\]

### 第五場

中日龍先發投手山井大介這場比賽投了八局，沒有擊出安打。而替補他的終結者[岩瀨仁紀在最後一局亦將最後三名打者出局](../Page/岩瀨仁紀.md "wikilink")，達成了日本職棒史上第一場繼投[完全比賽](../Page/完全比賽.md "wikilink")，不過根據日本職棒的記錄，[無安打比賽及完全比賽以繼投方式完成](../Page/無安打比賽.md "wikilink")，是不會被官方承認。而日本火腿的達比修有投了七局，三振11名打者。他在第二局被[Tyrone
Woods擊出一支安打](../Page/Tyrone_Woods.md "wikilink")，之後中村紀洋擊出二壘打，之後雖然三振了[李炳圭](../Page/李炳圭.md "wikilink")，但中日龍憑[平田良介的高飛犧牲打讓Woods返回本壘得分](../Page/平田良介.md "wikilink")。最後中日龍憑這一分壓過日本火腿鬥士奪得總冠軍。\[7\]\[8\]

## 備忘

  - 最有價值球員：中村紀洋（中日）

## 資料來源

<div class="references-small">

<references />

</div>

[Category:日本大賽](../Category/日本大賽.md "wikilink")
[Category:2007年棒球](../Category/2007年棒球.md "wikilink")
[Category:中日龍](../Category/中日龍.md "wikilink")
[Category:北海道日本火腿鬥士](../Category/北海道日本火腿鬥士.md "wikilink")
[Category:2007年日本體育](../Category/2007年日本體育.md "wikilink")

1.  [Darvish, Seguignol key Hammies'
    win](http://search.japantimes.co.jp/cgi-bin/sb20071028j1.html)日本時報
2.  [](https://archive.is/20120804090918/www.mainichi.jp/enta/sports/baseball/pro/news/20071028k0000m050096000c.html)
    每日新聞
3.  [](https://archive.is/20120907004006/www.mainichi.jp/enta/sports/baseball/news/20071029k0000m050097000c.html)
    每日新聞
4.  [Dragons score seven in 1st, nab series
    lead](http://search.japantimes.co.jp/cgi-bin/sb20071031j1.html) 日本時報
5.  [日職總冠軍賽
    中日龍隊聽牌](http://tw.news.yahoo.com/article/url/d/a/071031/2/negi.html)
6.  [Dragons close in on Japan
    Series](http://search.japantimes.co.jp/cgi-bin/sb20071101j1.html)
    日本時報
7.  [Dragons clinch Japan
    Series](http://search.japantimes.co.jp/cgi-bin/sb20071102j1.html)
    日本時報
8.  [](https://archive.is/20120906220009/www.mainichi.jp/enta/sports/baseball/pro/news/20071102k0000m050082000c.html)
    每日新聞