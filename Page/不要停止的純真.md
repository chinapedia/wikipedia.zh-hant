**不要停止的純真**（**,PURE**）是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")6張[單曲](../Page/單曲.md "wikilink")。於1999年2月24日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

作為單曲A面歌曲來說，是一張首次音調比較重的作品。[Oricon所統計的首批銷量已經不及出道單曲](../Page/Oricon.md "wikilink")「[玻璃少年](../Page/玻璃少年.md "wikilink")」的40萬張，銷售顯然不佳。不過因為成為了[電視劇的主題曲](../Page/電視劇.md "wikilink")，所以也能說大眾對這首歌曲的印象甚高。

由於之前連續兩張單均為「兩A面」單曲，所以就連Oricon銷量榜當初也曾錯誤地以『不要停止的純真／BABY
LOVE』這種「兩A面」的形式去標記這張單曲，不過現在已經修正過來。另外，唱片的[日語名稱](../Page/日語.md "wikilink")「**,PURE**」，中間應以[英語的](../Page/英語.md "wikilink")[逗號](../Page/逗號.md "wikilink")「，」分隔，而非日語的逗號「、」。所以這張單曲正確應該單A面單曲，唱片名稱方面應以「**,PURE**」方為正確。

主打歌『不要停止的PURE』是以[電吉他演奏的強勁節奏作序幕為中心](../Page/電吉他.md "wikilink")，把一向來以在單曲A面歌曲的流行樂曲原素巧妙地蘊含於曲中。

## 名稱

  - 日語原名：**,PURE**
  - 中文意思：**請不要停止，純真**
  - [香港](../Page/香港.md "wikilink")[正東譯名](../Page/正東唱片.md "wikilink")：**不要停止的純真**
  - [台灣](../Page/台灣.md "wikilink")[豐華譯名](../Page/豐華唱片.md "wikilink")：**不要停止的純真**
  - [台灣](../Page/台灣.md "wikilink")[艾迴譯名](../Page/艾迴唱片.md "wikilink")：**不要停止的純真**

## 收錄歌曲

1.  **不要停止的純真**（****，**PURE**）
      - ※堂本剛參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『為了與你的未來～I'll
        be back～』主題曲。
      - 作曲：[筒美京平](../Page/筒美京平.md "wikilink")
      - 作詞：[伊達步](../Page/伊達步.md "wikilink")
      - 編曲：[WACKY KAKI](../Page/WACKY_KAKI.md "wikilink")
2.  **BABY LOVE**
      - 作曲：[飯田建彥](../Page/飯田建彥.md "wikilink")
      - 作詞：[山本英美](../Page/山本英美.md "wikilink")
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
3.  **不要停止的純真 (Instrumental)**
      - 作曲：筒美京平
      - 編曲：WACKY KAKI
4.  **BABY LOVE (Instrumental)**
      - 作曲：飯田建彥
      - 編曲：船山基紀

[Category:近畿小子歌曲](../Category/近畿小子歌曲.md "wikilink")
[Category:日本電視台週六連續劇主題曲](../Category/日本電視台週六連續劇主題曲.md "wikilink")
[Category:1999年單曲](../Category/1999年單曲.md "wikilink")
[Category:1999年Oricon單曲週榜冠軍作品](../Category/1999年Oricon單曲週榜冠軍作品.md "wikilink")