**立川北站**（）是一個位於[東京都](../Page/東京都.md "wikilink")[立川市](../Page/立川市.md "wikilink")二丁目，屬於[多摩都市單軌電車線的](../Page/多摩都市單軌電車線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。[車站編號是](../Page/車站編號.md "wikilink")**TT12**\[1\]。位於[JR東日本](../Page/東日本旅客鐵道.md "wikilink")[立川站的北側](../Page/立川站.md "wikilink")。與[立川南站在車費計算上視為同一站](../Page/立川南站.md "wikilink")。但是，如果只乘搭此站－立川南站間，會徵收首次乘搭車費100日圓。

## 歷史

  - 1999年（[平成](../Page/平成.md "wikilink")10年）11月27日 - 開業。
  - 2000年（[平成](../Page/平成.md "wikilink")12年）1月10日 -
    [多摩都市單軌立川北](../Page/多摩都市單軌.md "wikilink")－[多摩中心間開業](../Page/多摩中心站.md "wikilink")。

## 車站構造

[側式月台](../Page/側式月台.md "wikilink")2面2線。

### 月台配置

| 月台 | 路線                                                                                                                                              | 目的地                                    |
| -- | ----------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| 1  | [Tama_Toshi_Monorail_Logo.png](https://zh.wikipedia.org/wiki/File:Tama_Toshi_Monorail_Logo.png "fig:Tama_Toshi_Monorail_Logo.png") 多摩都市單軌電車線 | [玉川上水](../Page/玉川上水站.md "wikilink")、方向 |
| 2  | [高幡不動](../Page/高幡不動站.md "wikilink")、[多摩中心方向](../Page/多摩中心站.md "wikilink")                                                                       |                                        |

## 可供轉乘巴士路線

### 立川站北口

  - 1番乘降場
      - 立10-2系統：[瑞穂營業所行](../Page/立川巴士瑞穂營業所.md "wikilink")
      - 立11-1系統：三藤住宅行
      - 立11-2系統：[武蔵村山市民會館行](../Page/武蔵村山市.md "wikilink")
      - 立12-1系統：[箱根崎站行](../Page/箱根崎站.md "wikilink")
      - 立90系統：柏町四丁目經由[玉川上水站南口行](../Page/玉川上水站.md "wikilink")
      - 立93系統：懸鈴木循環
  - 2番乘降場
      - 立17系統：[東中神站北行](../Page/東中神站.md "wikilink")
      - 立17-3系統：大山[團地折返所行](../Page/團地.md "wikilink")
      - 立18系統：柏町、青柳循環
      - 立18-1系統：玉川上水站南口行
  - 3番乘降場
      - 立14系統：松中團地操車場行
      - 立15系統：[拜島站北入口行](../Page/拜島站.md "wikilink")
  - 4番乘降場
      - 立22系統：村山團地行
      - 立20-1系統：砂川七番經由玉川上水站南口行
      - 立27、立27-1系統：[立飛企業行](../Page/立飛企業.md "wikilink")
      - 立93系統：懸鈴大循環
      - 立28系統：柏町、青柳循環
      - 立21系統：熊野[神社前經由玉川上水站南口行](../Page/神社.md "wikilink")
  - 5番乘降場
      - 立31系統：若葉町三丁目經由若葉町團地行
      - 立31-2系統：若葉町交差點經由若葉町團地行
  - 6番乘降場
      - 立39系統：南街行
      - 立39-2系統：村山團地行
      - 立45系統：芝中團地行
      - 深夜巴士：[東大和市站行](../Page/東大和市站.md "wikilink")
      - [小平營業所行](../Page/西武巴士小平營業所.md "wikilink")
  - 7番乘降場
      - 立34系統：[久米川站行](../Page/久米川站.md "wikilink")
  - 8番乘降場
      - 立35系統：[東村山站西口行](../Page/東村山站.md "wikilink")
      - 立37系統：金刚石街行
      - [立川營業所行](../Page/西武巴士立川營業所.md "wikilink")
  - 9番乘降場
      - 立40系統：幸町團地行
  - 10番乘降場
      - 立80系統：[拜島營業所行](../Page/立川巴士拜島營業所.md "wikilink")
      - 立82系統：拜島站行
      - 立81系統：[昭島站南口行](../Page/昭島站.md "wikilink")
      - 立85系統：東中神站行
  - 11番乘降場
      - 立72系統：立川站南口行
  - 12番乘降場
      - 立53系統：北町行
      - 高速夜行：JR[堺市站行](../Page/堺市站.md "wikilink")
      - 高速夜行：、
  - 13番乘降場
      - 立32系統：[第八小學校行](../Page/立川市立第八小學校.md "wikilink")
  - 14番乘降場
      - 立73系統：[日野站行](../Page/日野站_\(東京都\).md "wikilink")
      - 立64系統：[高幡不動站行](../Page/高幡不動站.md "wikilink")
  - 15番乘降場
      - 立51系統：榉台團地行
  - 16番乘降場
      - 國15系統：[國立站南口行](../Page/國立站.md "wikilink")
      - [羽田空港行](../Page/東京國際空港.md "wikilink")
  - 27番乘降場（饭店前）
      - [成田空港行](../Page/成田空港.md "wikilink")

## 相鄰車站

  - [Tama_Toshi_Monorail_Logo.png](https://zh.wikipedia.org/wiki/File:Tama_Toshi_Monorail_Logo.png "fig:Tama_Toshi_Monorail_Logo.png")
    多摩都市單軌電車
    [Tama_Toshi_Monorail_Logo.png](https://zh.wikipedia.org/wiki/File:Tama_Toshi_Monorail_Logo.png "fig:Tama_Toshi_Monorail_Logo.png")
    多摩都市單軌電車線
      -

        （TT13）－**立川北（TT12）**－[立川南](../Page/立川南站.md "wikilink")（TT11）

## 外部連結

  - [多摩單軌電車
    立川北站](http://www.tama-monorail.co.jp/monorail/station/tachikawa-kita/index.html)

[Chikawakita](../Category/日本鐵路車站_Ta.md "wikilink")
[Category:立川市鐵路車站](../Category/立川市鐵路車站.md "wikilink")
[Category:多摩都市單軌電車車站](../Category/多摩都市單軌電車車站.md "wikilink")
[Category:1998年啟用的鐵路車站](../Category/1998年啟用的鐵路車站.md "wikilink")

1.  [多摩モノレール全駅に「駅ナンバリング」を導入します！！多摩都市モノレール(PDF)](http://www.tama-monorail.co.jp/info/list/mt_img/290630_press_station_code.pdf)