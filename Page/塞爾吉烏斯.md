塞爾吉烏斯可以指：

  - 聖徒謝爾吉，西元三世紀[羅馬帝國士兵](../Page/羅馬帝國.md "wikilink")。連同聖徒[巴克斯共同被遵奉為](../Page/巴克斯.md "wikilink")[基督教](../Page/基督教.md "wikilink")[殉道者和](../Page/殉道者.md "wikilink")[聖徒](../Page/聖徒.md "wikilink")。
  - [馬庫斯·塞爾吉烏斯](../Page/馬庫斯·塞爾吉烏斯.md "wikilink")
  - [盧西烏斯·塞爾吉烏斯·喀提林](../Page/盧西烏斯·塞爾吉烏斯·喀提林.md "wikilink")，[羅馬共和國末期之](../Page/羅馬共和國.md "wikilink")[軍人和](../Page/軍人.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")。
  - [塞爾吉烏斯·保盧斯](../Page/塞爾吉烏斯·保盧斯.md "wikilink")
  - [卡帕多细亚的塞尔吉乌斯](../Page/卡帕多细亚的塞尔吉乌斯.md "wikilink")
  - 教皇[思齊一世](../Page/思齊一世.md "wikilink")
  - 教宗[思齊二世](../Page/思齊二世.md "wikilink")
  - 教宗[思齊三世](../Page/思齊三世.md "wikilink")
  - 教宗[思齊四世](../Page/思齊四世.md "wikilink")
  - [君士坦丁堡牧首](../Page/君士坦丁堡牧首.md "wikilink")[塞吉阿斯一世](../Page/塞吉阿斯一世.md "wikilink")
  - 君士坦丁堡牧首[塞吉阿斯二世](../Page/塞吉阿斯二世.md "wikilink")
  - [聖塞爾吉烏斯和聖巴克烏斯](../Page/聖塞爾吉烏斯和聖巴克烏斯.md "wikilink")
  - [拉多涅茲的塞爾吉烏斯](../Page/拉多涅茲的塞爾吉烏斯.md "wikilink")
  - [撒馬爾罕的薛里吉思](../Page/馬·薛里吉思.md "wikilink")

[de:Serge](../Page/de:Serge.md "wikilink")