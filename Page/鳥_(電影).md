《**鳥**》（），[英國](../Page/英國.md "wikilink")[導演](../Page/導演.md "wikilink")[亞佛烈德·希區考克所執導的一部](../Page/亞佛烈德·希區考克.md "wikilink")[電影](../Page/電影.md "wikilink")，於1963年上映，根據[達夫妮·杜穆里埃](../Page/達夫妮·杜穆里埃.md "wikilink")（Daphne
du
Maurier）的[小說所改編](../Page/小說.md "wikilink")，被認為是希區考克代表作之一。获[AFI百年百大电影提名](../Page/AFI百年百大电影.md "wikilink")。

## 剧情

任性的富家女米兰妮是一家报社老板的女儿，一天她与律师米契在旧金山一家鸟店里邂逅，米契认出了曾经刁蛮的米兰妮，戏弄了她。米兰妮却没有气恼，反而记下了米契的车牌，还订购了米契要找的一對鸚鵡（lovebird）。第二天，米兰妮提着鸚鵡找到米契的住处，但却得知米契到海边家中度周末去了。一心想捉弄米契的米兰妮驱车来到海边小镇波德加湾，得知米契有个妹妹凯茜后，本想报复的她把鸚鵡悄悄留在米契家中送给了凯茜。
　　

米契见到鸟笼追上米兰妮，她在归途中被一只海鸥啄伤，米契就留米兰妮晚上在家吃饭。米兰妮见到了米契的母亲莉莉安、妹妹凯西，以及他的前任女友安妮，她与米契的感情也在顺利发展。就在这时，灾难发生了。
　　

先是一群海鸥突然袭击了米契妹妹的生日野宴，接着从烟囱中飞进的一群麻雀又把米契的家搅得天翻地覆。第二天一早，莉莉安就发现一位镇民在家里被群鸟杀死，体无完肤，眼睛也被啄掉了。米兰妮照顾莉莉安睡下后，到学校去接凯西回家，却发现校园里密密麻麻的已经聚满了烏鴉。孩子们迅速被疏散回家，疯狂的鸟群在路上袭击了他们。
　　

把凯西安置在安妮家后，米兰妮来到镇上餐馆，把消息告诉了父亲。很快，先是加油站遭到袭击，酿成了一场可怕的火灾。整个镇子都被笼罩在被群鸟袭击的恐怖之中，安妮为了保护凯茜，惨死在屋前。
　　
[Caw\!_Caw\!_(7674112888).jpg](https://zh.wikipedia.org/wiki/File:Caw!_Caw!_\(7674112888\).jpg "fig:Caw!_Caw!_(7674112888).jpg")
当晚，无法离去的四个人在家中再次遭到鸟群的袭击，米契奋力维持住屋子不被鸟群攻进。一轮攻击过后，米兰妮来到楼上查看，被闯入的群鸟啄成重伤，米契拼命把她救出。
　　

凌晨，一家人决定冒险闯出鸟群到旧金山去。四个人蹑手蹑脚地走出屋子，成功进入车中，米契缓慢地把车子驶向道路的远方……

## 角色

  - [蒂比·海德倫](../Page/蒂比·海德倫.md "wikilink") - Melanie Daniels
  - [羅德·泰勒](../Page/羅德·泰勒.md "wikilink") - Mitch Brenner
  - [潔西卡·坦迪](../Page/潔西卡·坦迪.md "wikilink") - Lydia Brenner
  - [Suzanne Pleshette](../Page/Suzanne_Pleshette.md "wikilink") - Annie
    Hayworth
  - [Veronica Cartwright](../Page/Veronica_Cartwright.md "wikilink") -
    Cathy Brenner
  - [Ethel Griffies](../Page/Ethel_Griffies.md "wikilink") - Mrs. Bundy
  - [Charles McGraw](../Page/Charles_McGraw.md "wikilink") - Sebastian
    Sholes
  - [Ruth McDevitt](../Page/Ruth_McDevitt.md "wikilink") - Mrs.
    MacGruder
  - [Malcolm Atterbury](../Page/Malcolm_Atterbury.md "wikilink") -
    Deputy Al Malone
  - [Elizabeth Wilson](../Page/Elizabeth_Wilson.md "wikilink") - Helen
    Carter
  - [Lonny Chapman](../Page/Lonny_Chapman.md "wikilink") - Deke Carter

## 外部連結

  -
  -
  -
  -
  - [Monograph](https://web.archive.org/web/20101225103054/http://www.sensesofcinema.com/2009/51/hitchcock-birds-synoptic-account/)
    on *The Birds* at *[Senses of
    Cinema](../Page/Senses_of_Cinema.md "wikilink")*

  - [Analytical summary by Tim Dirks at AMC
    Filmsite](http://www.filmsite.org/bird.html)

  - [Complete script of the
    film](https://web.archive.org/web/20051210174531/http://www.script-o-rama.com/movie_scripts/b/the-birds-script-screenplay.html)

  -
**Streaming audio**

  - [*The
    Birds*](http://ia700208.us.archive.org/21/items/Lux18/Lux-530720-52m26s-839hrt-Birds-HMarshallBLGerson.mp3)
    on [Lux Radio Theater](../Page/Lux_Radio_Theater.md "wikilink"):
    July 20, 1953
  - [*The
    Birds*](http://ia600808.us.archive.org/25/items/OTRR_Escape_Singles/Escape_54-07-10_-217-_The_Birds.mp3)
    on [Escape](../Page/Escape_\(radio_program\).md "wikilink"): July
    10, 1954

[Category:1963年電影](../Category/1963年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:亞弗列德·希區考克電影](../Category/亞弗列德·希區考克電影.md "wikilink")
[Category:美国驚悚片](../Category/美国驚悚片.md "wikilink")
[Category:美国恐怖片](../Category/美国恐怖片.md "wikilink")
[Category:鳥類電影](../Category/鳥類電影.md "wikilink")
[Category:英国小说改编电影](../Category/英国小说改编电影.md "wikilink")
[Category:旧金山背景电影](../Category/旧金山背景电影.md "wikilink")
[Category:加利福尼亚州取景电影](../Category/加利福尼亚州取景电影.md "wikilink")
[Category:环球影业电影](../Category/环球影业电影.md "wikilink")