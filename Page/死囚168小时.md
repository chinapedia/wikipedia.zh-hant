《**死囚168小时**》（）是一部[1995年首映的](../Page/1995年电影.md "wikilink")[美国劇情片](../Page/美国电影.md "wikilink")，由[蒂姆·罗宾斯執導](../Page/蒂姆·罗宾斯.md "wikilink")，[蘇珊·莎蘭登及](../Page/蘇珊·莎蘭登.md "wikilink")[西恩·潘等主演](../Page/西恩·潘.md "wikilink")。

电影以[死刑為主題](../Page/死刑.md "wikilink")，基于[修女](../Page/修女.md "wikilink")[海伦·培贞同名书改编](../Page/海伦·培贞.md "wikilink")，原书描写的是真实事件。电影将两个死刑犯的故事改编得更容易被观众接受。

## 剧情

马修·庞塞尔特在死囚牢房里已经待了六年了，他请求修女海伦·培贞的帮助来使得他的案子再审。海伦去看望了他。海伦试图将死刑改为[无期徒刑](../Page/无期徒刑.md "wikilink")，在这个过程中两人之间的人际关系越来越强烈。在这个过程中海伦既结识了受害者和罪犯的亲属。有些人要求正义，对海伦的作为无法理解。最后在电影的结尾证明马修的确是[杀人犯](../Page/杀人犯.md "wikilink")，他的死刑被执行。但是电影也显示了许多与死刑相关的问题。

## 製作

其制作费用为1100万美元。

## 原聲帶

原聲帶因有[布鲁斯·斯布林斯顿為此片而創作的新曲](../Page/布鲁斯·斯布林斯顿.md "wikilink")，所以亦相當熱賣。

## 提名與獲獎

1996年[第68屆](../Page/第68屆奧斯卡金像獎.md "wikilink")[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")：

  - [最佳女主角奖](../Page/奧斯卡最佳女主角獎.md "wikilink")：[蘇珊·莎蘭登](../Page/蘇珊·莎蘭登.md "wikilink")
  - 提名[最佳男主角奖](../Page/奧斯卡最佳男主角獎.md "wikilink")：[西恩·潘](../Page/西恩·潘.md "wikilink")
  - 提名[最佳导演奖](../Page/奧斯卡最佳導演獎.md "wikilink")：[蒂姆·罗宾斯](../Page/蒂姆·罗宾斯.md "wikilink")
  - 提名[最佳歌曲奖](../Page/奧斯卡最佳歌曲獎.md "wikilink")：[布鲁斯·斯布林斯顿](../Page/布鲁斯·斯布林斯顿.md "wikilink")

## 外部連結

  -
  -
  - *[死囚168小时](https://web.archive.org/web/20060613212800/http://artsandfaith.com/t100/2005/entry.php?film=18)*
    at the [Arts & Faith Top100 Spiritually Significant
    Films](https://web.archive.org/web/20060720142105/http://artsandfaith.com/top100/)
    list

  - *[Interview with Sister Helen
    Prejean](http://www.pbs.org/wgbh/pages/frontline/angel/interviews/hprejean.html)*

  - *["Entertainment Watch: Dead Man Walking" from AmericanCatholic.org,
    April 1996](http://www.americancatholic.org/Messenger/Apr1996/Entertainment_Watch.asp#F1)*
    James Arnold's Catholic view on the film

  - *[Sister Helen Prejean: The Real Woman Behind "Dead Man Walking" by
    John Bookser
    Feister](http://www.americancatholic.org/Messenger/Apr1996/feature1.asp)*
    from AmericanCatholic.org

  - [Lyrics and detailed info for the theme song "Dead Man
    Walkin'"](http://www.springsteenlyrics.com/lyrics/d/deadmanwalkin.php)

[Category:1995年电影](../Category/1995年电影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1990年代劇情片](../Category/1990年代劇情片.md "wikilink")
[Category:美國劇情片](../Category/美國劇情片.md "wikilink")
[Category:路易斯安那州背景電影](../Category/路易斯安那州背景電影.md "wikilink")
[Category:監獄電影](../Category/監獄電影.md "wikilink")
[Category:死刑題材電影](../Category/死刑題材電影.md "wikilink")
[Category:非虛構書籍改編電影](../Category/非虛構書籍改編電影.md "wikilink")
[Category:美國演員工會獎電影類最佳女主角獲獎作品](../Category/美國演員工會獎電影類最佳女主角獲獎作品.md "wikilink")
[Category:奧斯卡最佳女主角獲獎電影](../Category/奧斯卡最佳女主角獲獎電影.md "wikilink")