**8月29日**是[阳历年的第](../Page/阳历.md "wikilink")241天（[闰年是](../Page/闰年.md "wikilink")242天），离一年的结束还有124天。

## 大事记

### 16世紀

  - [1526年](../Page/1526年.md "wikilink")：[第一次摩哈赤战役](../Page/第一次摩哈赤战役.md "wikilink")：[苏莱曼一世率奥斯曼帝国军队击溃](../Page/苏莱曼一世_\(奥斯曼帝国\).md "wikilink")[匈牙利军队](../Page/匈牙利.md "wikilink")，匈牙利国王[拉约什二世战死](../Page/拉约什二世.md "wikilink")，[亚盖隆王朝告终](../Page/亚盖隆王朝.md "wikilink")。
  - [1533年](../Page/1533年.md "wikilink")：[西班牙](../Page/西班牙.md "wikilink")[征服者](../Page/征服者.md "wikilink")[弗朗西斯科·皮萨罗于](../Page/弗朗西斯科·皮萨罗.md "wikilink")[卡哈马卡杀害最后一名](../Page/卡哈马卡.md "wikilink")[萨帕·印卡](../Page/萨帕·印卡.md "wikilink")[阿塔瓦尔帕](../Page/阿塔瓦尔帕.md "wikilink")。

### 18世紀

  - [1756年](../Page/1756年.md "wikilink")：[普鲁士](../Page/普鲁士.md "wikilink")[腓特烈二世率领军队突然入侵](../Page/腓特烈二世_\(普鲁士\).md "wikilink")[萨克森](../Page/萨克森.md "wikilink")，[七年战争爆发](../Page/七年战争.md "wikilink")。
  - [1786年](../Page/1786年.md "wikilink")：[美国](../Page/美国.md "wikilink")[马萨诸塞州农民因不满极高税务压力与基本权利遭到剥夺](../Page/马萨诸塞州.md "wikilink")，在[丹尼尔·谢司率领下发起](../Page/丹尼尔·谢司.md "wikilink")[谢司叛乱](../Page/谢司叛乱.md "wikilink")。

### 19世紀

  - [1825年](../Page/1825年.md "wikilink")：[葡萄牙承认](../Page/葡萄牙.md "wikilink")[巴西独立](../Page/巴西.md "wikilink")。
  - [1831年](../Page/1831年.md "wikilink")：[英国](../Page/英国.md "wikilink")[物理学家](../Page/物理学.md "wikilink")[法拉第通过](../Page/迈克尔·法拉第.md "wikilink")[实验发现](../Page/实验.md "wikilink")[电磁感应定律](../Page/法拉第电磁感应定律.md "wikilink")。
  - [1833年](../Page/1833年.md "wikilink")：英国立法废除[奴隶制](../Page/奴隶制.md "wikilink")。
  - [1842年](../Page/1842年.md "wikilink")：[中国](../Page/中国.md "wikilink")[清朝代表](../Page/清朝.md "wikilink")[耆英](../Page/耆英.md "wikilink")、[伊-{里}-布和英國代表](../Page/伊里布.md "wikilink")[璞鼎查在停泊于](../Page/璞鼎查.md "wikilink")[南京的英国军舰上签订](../Page/南京.md "wikilink")《[南京条约](../Page/南京条约.md "wikilink")》，[香港岛被](../Page/香港岛.md "wikilink")[割让给英国](../Page/香港割讓.md "wikilink")。
  - [1862年](../Page/1862年.md "wikilink")：[第二次馬納沙斯之役爆發](../Page/第二次馬納沙斯之役.md "wikilink")，[北維吉尼亞軍團得勝而受鼓舞](../Page/北維吉尼亞軍團.md "wikilink")，主動向北方發動侵略，展開[馬里蘭會戰](../Page/馬里蘭會戰.md "wikilink")。
  - [1871年](../Page/1871年.md "wikilink")：[明治维新](../Page/明治维新.md "wikilink")：[日本宣布](../Page/日本.md "wikilink")[廢藩置縣](../Page/廢藩置縣.md "wikilink")。
  - [1875年](../Page/1875年.md "wikilink")：清廷派遣[郭嵩焘率领使团就](../Page/郭嵩焘.md "wikilink")[马嘉理事件去英国道歉](../Page/马嘉理事件.md "wikilink")。
  - [1885年](../Page/1885年.md "wikilink")：[德国发明家](../Page/德国.md "wikilink")[戴姆勒制造的世界首部](../Page/戈特利布·戴姆勒.md "wikilink")[摩托车获得](../Page/摩托车.md "wikilink")[专利](../Page/专利.md "wikilink")。
  - [1898年](../Page/1898年.md "wikilink")：[美國](../Page/美國.md "wikilink")[輪胎製造商](../Page/輪胎.md "wikilink")[固特異成立](../Page/固特異.md "wikilink")。

### 20世紀

  - [1910年](../Page/1910年.md "wikilink")：日本命令将[大韓帝國国名改为](../Page/大韓帝國.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")，并任命第一任[朝鲜总督](../Page/朝鲜总督.md "wikilink")。
  - [1930年](../Page/1930年.md "wikilink")：英国[圣基尔达岛最后](../Page/圣基尔达岛.md "wikilink")36位居民志愿迁往[苏格兰本土](../Page/苏格兰.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：第二次世界大战：[斯洛伐克民族起义](../Page/斯洛伐克民族起义.md "wikilink")，六万名[斯洛伐克官兵宣布起义](../Page/斯洛伐克.md "wikilink")，与德国军队开战。
  - [1946年](../Page/1946年.md "wikilink")：美国海军[内华达号战列舰退役](../Page/内华达号战列舰.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[苏联成功試爆第一顆](../Page/苏联.md "wikilink")[原子弹](../Page/原子弹.md "wikilink")，打破了[美国的核垄断地位](../Page/美国.md "wikilink")。
  - [1952年](../Page/1952年.md "wikilink")：[美國](../Page/美國.md "wikilink")[前衛](../Page/前衛.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")[約翰·凱吉名作](../Page/約翰·凱吉.md "wikilink")《[4分33秒](../Page/4分33秒.md "wikilink")》首度公開演奏。
  - [1958年](../Page/1958年.md "wikilink")：[中共中央决定在全国范围内推广](../Page/中共中央.md "wikilink")[人民公社](../Page/人民公社.md "wikilink")。
  - [1966年](../Page/1966年.md "wikilink")：[披頭四樂團在](../Page/披頭四樂團.md "wikilink")[美國](../Page/美國.md "wikilink")[舊金山進行最後一次演唱會](../Page/舊金山.md "wikilink")。
  - 1966年：[越南战争](../Page/越南战争.md "wikilink")：美军飞机在[北部湾炸毁中华人民共和国援助](../Page/北部湾.md "wikilink")[越南民主共和国的货轮](../Page/越南民主共和国.md "wikilink")1018轮的事件，[南海1018轮事件发生](../Page/南海1018轮事件.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[毛主席纪念堂全部建成](../Page/毛主席纪念堂.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：[联邦德国科学家人工合成](../Page/联邦德国.md "wikilink")109号元素[䥑成功](../Page/䥑.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：[韓國发生](../Page/韓國.md "wikilink")[五大洋集體自殺事件](../Page/五大洋集體自殺事件.md "wikilink")，32人死亡。
  - [1991年](../Page/1991年.md "wikilink")：[苏联最高苏维埃宣布禁止](../Page/苏联最高苏维埃.md "wikilink")[苏联共产党的所有活动](../Page/苏联共产党.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[香港特別行政區自宣佈](../Page/香港特別行政區.md "wikilink")「[八萬五計劃](../Page/八萬五建屋計劃.md "wikilink")」以來恢復賣地。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink")：[網路瀏覽器](../Page/網路瀏覽器.md "wikilink")[Netscape
    7推出](../Page/Netscape_7.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[飓风卡特里娜在](../Page/2005年颶風卡特里娜.md "wikilink")[美国](../Page/美国.md "wikilink")[墨西哥湾沿岸的](../Page/美國墨西哥灣沿岸地區.md "wikilink")[路易斯安那州和](../Page/路易斯安那州.md "wikilink")[密西西比州登陆](../Page/密西西比州.md "wikilink")，造成至少1836人死亡。
  - [2010年](../Page/2010年.md "wikilink")：[美国职棒大联盟名将](../Page/美国职棒大联盟.md "wikilink")[法兰克·汤玛斯退役](../Page/法兰克·汤玛斯.md "wikilink")。
  - [2012年](../Page/2012年.md "wikilink")：[2012年夏季残疾人奥林匹克运动会在英国首都](../Page/2012年夏季残疾人奥林匹克运动会.md "wikilink")[伦敦开幕](../Page/伦敦.md "wikilink")。

## 出生

  - [1593年](../Page/1593年.md "wikilink")：[豐臣秀賴](../Page/豐臣秀賴.md "wikilink")，[豐臣秀吉之子](../Page/豐臣秀吉.md "wikilink")，側室[淀殿所生](../Page/淀殿.md "wikilink")。官至從一位[右大臣](../Page/右大臣.md "wikilink")。（[1615年去世](../Page/1615年.md "wikilink")）
  - [1619年](../Page/1619年.md "wikilink")：[让-巴普蒂斯特·柯尔贝尔](../Page/让-巴普蒂斯特·柯尔贝尔.md "wikilink")，[法国](../Page/法国.md "wikilink")[政治家](../Page/政治家.md "wikilink")，[路易十四时代担任财政大臣和海军国务大臣](../Page/路易十四.md "wikilink")。（[1683年去世](../Page/1683年.md "wikilink")）
  - [1632年](../Page/1632年.md "wikilink")：[约翰·洛克](../Page/约翰·洛克.md "wikilink")，[英國](../Page/英國.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")，[經驗主義的代表人物](../Page/經驗主義.md "wikilink")。（[1704年去世](../Page/1704年.md "wikilink")）
  - [1780年](../Page/1780年.md "wikilink")：[让·奥古斯特·多米尼克·安格尔](../Page/让·奥古斯特·多米尼克·安格尔.md "wikilink")，法国画家，[新古典主义画派的最后一位领导人](../Page/新古典主义画派.md "wikilink")。（[1867年去世](../Page/1867年.md "wikilink")）
  - [1809年](../Page/1809年.md "wikilink")：[老奥利弗·温德尔·霍姆斯](../Page/老奥利弗·温德尔·霍姆斯.md "wikilink")，[美国作家](../Page/美国.md "wikilink")，毕业于美国[哈佛大学医学院](../Page/哈佛大学.md "wikilink")，被誉为美国[19世纪最佳诗人之一](../Page/19世纪.md "wikilink")。（[1894年去世](../Page/1894年.md "wikilink")）
  - [1862年](../Page/1862年.md "wikilink")：[安德魯·費希爾](../Page/安德魯·費希爾.md "wikilink")，[澳洲第](../Page/澳洲.md "wikilink")5任[總理](../Page/總理.md "wikilink")。（[1928年去世](../Page/1928年.md "wikilink")）
  - [1862年](../Page/1862年.md "wikilink")：[莫里斯·梅特林克](../Page/莫里斯·梅特林克.md "wikilink")，[比利时诗人](../Page/比利时.md "wikilink")、剧作家、散文家，[1911年](../Page/1911年.md "wikilink")[诺贝尔文学奖得主](../Page/诺贝尔文学奖.md "wikilink")。（[1949年去世](../Page/1949年.md "wikilink")）
  - [1871年](../Page/1871年.md "wikilink")：[阿尔贝·勒布伦](../Page/阿尔贝·勒布伦.md "wikilink")，法國[政治家](../Page/政治家.md "wikilink")，[法蘭西第三共和国的最後一位總統](../Page/法蘭西第三共和国.md "wikilink")。（[1950年去世](../Page/1950年.md "wikilink")）
  - [1871年](../Page/1871年.md "wikilink")：[金九](../Page/金九.md "wikilink")，[韓國獨立運動家](../Page/韓國.md "wikilink")、抗日英雄，被現代韓國人尊稱為「韓國的國父」。（[1949年去世](../Page/1949年.md "wikilink")）
  - [1888年](../Page/1888年.md "wikilink")：[三川軍一](../Page/三川軍一.md "wikilink")，[日本海軍戰將](../Page/日本.md "wikilink")。（[1981年去世](../Page/1981年.md "wikilink")）
  - [1904年](../Page/1904年.md "wikilink")：[沃纳·福斯曼](../Page/沃纳·福斯曼.md "wikilink")，[德國醫生](../Page/德國.md "wikilink")，[心臟導管的發明人](../Page/心臟.md "wikilink")，[1954年](../Page/1954年.md "wikilink")[諾貝爾生理學或醫學獎得主](../Page/諾貝爾生理學或醫學獎.md "wikilink")。（[1979年去世](../Page/1979年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[查爾斯·阿弗雷德·塔里亞菲羅](../Page/查爾斯·阿弗雷德·塔里亞菲羅.md "wikilink")，美國[漫畫家](../Page/漫畫家.md "wikilink")，以[迪士尼](../Page/迪士尼.md "wikilink")[卡通人物](../Page/卡通.md "wikilink")[唐老鴨的創作者聞名](../Page/唐老鴨.md "wikilink")。（[1969年去世](../Page/1969年.md "wikilink")）
  - [1912年](../Page/1912年.md "wikilink")：[孫基禎](../Page/孫基禎.md "wikilink")，[朝鮮日治時期北朝鲜男子](../Page/朝鮮日治時期.md "wikilink")[馬拉松運動員](../Page/馬拉松.md "wikilink")。[1936年夏季奧林匹克運動會男子](../Page/1936年夏季奧林匹克運動會.md "wikilink")[馬拉松項目冠軍](../Page/馬拉松.md "wikilink")，首位在[奧林匹克運動會奪得金牌的](../Page/奧林匹克運動會.md "wikilink")[朝鲜族運動員](../Page/朝鲜族.md "wikilink")，曾擔任[韓國](../Page/韓國.md "wikilink")[田徑聯盟主席及韓國體育協會會長](../Page/田徑.md "wikilink")。（2002年去世）
  - [1915年](../Page/1915年.md "wikilink")：[英格丽·褒曼](../Page/英格丽·褒曼.md "wikilink")，[瑞典演员](../Page/瑞典.md "wikilink")。曾獲得過三次[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")，兩次[艾美獎以及一次](../Page/艾美獎.md "wikilink")[托尼獎](../Page/托尼獎.md "wikilink")。（[1982年去世](../Page/1982年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[吳冠中](../Page/吳冠中.md "wikilink")，[中國著名](../Page/中國.md "wikilink")[畫家](../Page/畫家.md "wikilink")。（2010年去世）
  - [1920年](../Page/1920年.md "wikilink")：[查利·帕克](../Page/查利·帕克.md "wikilink")，美國著名黑人[爵士樂手](../Page/爵士樂.md "wikilink")。（[1955年去世](../Page/1955年.md "wikilink")）
  - [1923年](../Page/1923年.md "wikilink")：[李察·艾登堡祿男爵](../Page/李察·艾登堡祿.md "wikilink")，資深英國演員、導演及監製。曾獲兩座[奧斯卡獎及四座](../Page/奧斯卡獎.md "wikilink")[BAFTA獎](../Page/BAFTA.md "wikilink")。（[2014年去世](../Page/2014年.md "wikilink")）
  - [1933年](../Page/1933年.md "wikilink")：[潔罕·薩達特](../Page/潔罕·薩達特.md "wikilink")，[埃及前任第一夫人](../Page/埃及.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink")：[約翰·麥凱恩](../Page/約翰·麥凱恩.md "wikilink")，美国[政治家](../Page/政治家.md "wikilink")，[共和黨資深](../Page/共和黨.md "wikilink")[參議員](../Page/參議員.md "wikilink")。（[2018年去世](../Page/2018年.md "wikilink")）
  - [1937年](../Page/1937年.md "wikilink")：[樂蒂](../Page/樂蒂.md "wikilink")，[香港國語片演員](../Page/香港.md "wikilink")。（[1968年去世](../Page/1968年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：[羅伯特·愛德華·魯賓](../Page/羅伯特·愛德華·魯賓.md "wikilink")，美國[銀行家](../Page/銀行家.md "wikilink")，第70任[美國財政部部長](../Page/美國財政部.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[季米特里斯·赫里斯托菲亚斯](../Page/季米特里斯·赫里斯托菲亚斯.md "wikilink")，[賽普勒斯](../Page/賽普勒斯.md "wikilink")[共產黨](../Page/共產黨.md "wikilink")[勞動人民進步黨總書記](../Page/勞動人民進步黨.md "wikilink")，第6任[賽普勒斯總統](../Page/賽普勒斯總統.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[天寶·葛蘭汀](../Page/天寶·葛蘭汀.md "wikilink")，畜產學學者、暢銷作家。知名的[亞斯伯格症候群患者](../Page/亞斯伯格症候群.md "wikilink")，致力於宣導[自閉症](../Page/自閉症.md "wikilink")、並發明了Hug
    machine 給過度敏感的人。
  - [1951年](../Page/1951年.md "wikilink")：[黃鶯鶯](../Page/黃鶯鶯.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[鷺巢詩郎](../Page/鷺巢詩郎.md "wikilink")，日本[作曲家](../Page/作曲家.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：[連尼·亨利](../Page/連尼·亨利.md "wikilink")，英國[喜劇演員](../Page/喜劇.md "wikilink")、[棟篤笑演員](../Page/棟篤笑.md "wikilink")。
  - [1958年](../Page/1958年.md "wikilink")：[迈克尔·杰克逊](../Page/迈克尔·杰克逊.md "wikilink")，美国[流行音樂歌手](../Page/流行音樂.md "wikilink")，[作曲家](../Page/作曲家.md "wikilink")，舞蹈家，[唱片製作人](../Page/唱片製作人.md "wikilink")。（[2009年去世](../Page/2009年.md "wikilink")）
  - [1959年](../Page/1959年.md "wikilink")：[史蒂芬·沃爾夫勒姆](../Page/史蒂芬·沃爾夫勒姆.md "wikilink")，英國[物理學家](../Page/物理學.md "wikilink")、[數學家](../Page/數學.md "wikilink")。數學軟件[Mathematica的發明者之一](../Page/Mathematica.md "wikilink")；對計算型知識引擎[Wolfram
    Alpha上的工作而聞名](../Page/Wolfram_Alpha.md "wikilink")。在學術上以[粒子物理學](../Page/粒子物理學.md "wikilink")、[元胞自動機](../Page/元胞自動機.md "wikilink")、[宇宙學](../Page/宇宙學.md "wikilink")、[複雜性理論](../Page/複雜性理論.md "wikilink")、計算[代數上的研究成果聞名於世](../Page/代數.md "wikilink")。
  - [1960年](../Page/1960年.md "wikilink")：[渡邊多惠子](../Page/渡邊多惠子.md "wikilink")，日本漫畫家。
  - [1967年](../Page/1967年.md "wikilink")：[纪瑞·于泽克](../Page/纪瑞·于泽克.md "wikilink")，[捷克](../Page/捷克.md "wikilink")[摄影师](../Page/摄影师.md "wikilink")。
  - [1969年](../Page/1969年.md "wikilink")：[椎名愛弓](../Page/椎名愛弓.md "wikilink")，日本漫畫家。
  - [1971年](../Page/1971年.md "wikilink")：[卡拉·古奇諾](../Page/卡拉·古奇諾.md "wikilink")，美國女演員。
  - [1972年](../Page/1972年.md "wikilink")：[裴勇俊](../Page/裴勇俊.md "wikilink")，韓國演員。
  - [1976年](../Page/1976年.md "wikilink")：[容·达尔·托马森](../Page/容·达尔·托马森.md "wikilink")，[丹麥職業](../Page/丹麥.md "wikilink")[足球運動員](../Page/足球.md "wikilink")，亦是[2002年及](../Page/2002年.md "wikilink")[2004年](../Page/2004年.md "wikilink")[丹麥足球先生得主](../Page/丹麥足球先生.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[謝霆鋒](../Page/謝霆鋒.md "wikilink")，香港歌手，演員。
  - [1981年](../Page/1981年.md "wikilink")：[卡洛斯·德尔菲诺](../Page/卡洛斯·德尔菲诺.md "wikilink")，[阿根廷職業](../Page/阿根廷.md "wikilink")[籃球運動員](../Page/籃球.md "wikilink")，密爾沃基雄鹿隊球員。2004、2008年[夏季奧林匹克運動會獎牌得主](../Page/夏季奧林匹克運動會.md "wikilink")。
  - 1981年：[丹尼斯·吴](../Page/丹尼斯·吴.md "wikilink")，韩裔美国人，為[模特兒和](../Page/模特兒.md "wikilink")[演员](../Page/演员.md "wikilink")。
  - 1989年：[蘇炳添](../Page/蘇炳添.md "wikilink")，第一位以9秒台完成100公尺短跑的亞洲人（不含兩位奈及利亞歸化卡達的田徑運動員）。
  - [1993年](../Page/1993年.md "wikilink")：[吳品潔](../Page/吳品潔.md "wikilink")，台灣女演員

## 逝世

  - [310年](../Page/310年.md "wikilink")：[刘渊](../Page/刘渊.md "wikilink")，汉王。（出生年代不详）
  - [1799年](../Page/1799年.md "wikilink")：[庇护六世](../Page/庇护六世.md "wikilink")，教宗。（生于[1717年](../Page/1717年.md "wikilink")）
  - [1926年](../Page/1926年.md "wikilink")：[何福](../Page/何福_\(香港\).md "wikilink")，[香港企業家及慈善家](../Page/香港.md "wikilink")（生于[1863年](../Page/1863年.md "wikilink")）
  - [1935年](../Page/1935年.md "wikilink")：[阿斯特里德](../Page/阿斯特里德王后_\(比利时\).md "wikilink")，[比利时王后](../Page/比利时.md "wikilink")。（生于[1905年](../Page/1905年.md "wikilink")）
  - [1955年](../Page/1955年.md "wikilink")：[洪深](../Page/洪深.md "wikilink")，戏剧家。（生于[1894年](../Page/1894年.md "wikilink")）
  - [1982年](../Page/1982年.md "wikilink")：[英格丽·褒曼](../Page/英格丽·褒曼.md "wikilink")，演员。（生于[1915年](../Page/1915年.md "wikilink")）
  - [1987年](../Page/1987年.md "wikilink")：[菲利普·史密斯](../Page/菲利普·史密斯.md "wikilink")，美国电子工程师，[史密斯圆图的发明者](../Page/史密斯圆图.md "wikilink")。（生于[1905年](../Page/1905年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[謝默斯·希尼](../Page/謝默斯·希尼.md "wikilink")，諾貝爾文學獎得主。（生于[1939年](../Page/1939年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[布鲁斯·穆雷](../Page/布鲁斯·穆雷_\(行星科學家\).md "wikilink")，美國行星科學家、地質學家。（生于[1931年](../Page/1931年.md "wikilink")）

## 节假日和习俗

  - [古埃及历法的](../Page/古埃及历法.md "wikilink")[元旦](../Page/元旦.md "wikilink")。
  - [斯洛伐克国家起义日](../Page/斯洛伐克.md "wikilink")。
  - [聖若翰洗者殉道日](../Page/聖若翰洗者.md "wikilink")。
  - [禁止核試驗國際日](../Page/全面禁止核试验条约.md "wikilink")。[1](http://www.un.org/zh/events/againstnucleartestsday/)

## 參考資料

## 外部連結