**大卫·德西雷·马赫·吉诺拉**（，）生於[法國南部的](../Page/法國.md "wikilink")[加桑](../Page/加桑.md "wikilink")，是前足球運動員，司職[左翼](../Page/中場.md "wikilink")，吉诺拉身高1.85米及樣子俊俏，兼職[模特兒](../Page/模特兒.md "wikilink")，2002年退役後轉投[電影演員工作](../Page/電影演員.md "wikilink")。1992年與科羅琳（Coraline）結婚，現時育有一女及一子。

## 球員生涯

### \-{zh-hans:俱乐部; zh-hk:球會;}-

真路拿曾經效力多家[法國及](../Page/法國.md "wikilink")[英格蘭的俱乐部](../Page/英格蘭.md "wikilink")，包括[圖盧茲](../Page/图卢兹足球俱乐部.md "wikilink")（1985-88年）、（1988-90年）、[-{zh-hans:布雷斯特瓦;
zh-hk:碧斯圖華;}-](../Page/布雷斯特足球俱乐部.md "wikilink")（1990-92年）、[巴黎聖日耳門](../Page/巴黎圣日耳曼足球俱乐部.md "wikilink")（1992-95年）、[-{zh-hans:纽卡斯尔联;
zh-hk:紐卡素;}-](../Page/纽卡斯尔联足球俱乐部.md "wikilink")（1995-97年）、[托特纳姆热刺](../Page/托特纳姆足球俱乐部.md "wikilink")（1997-2000年）、[-{zh-hans:阿斯顿维拉;
zh-hk:阿士東維拉;}-](../Page/阿士東維拉足球俱樂部.md "wikilink")（2001-02年）及[-{zh-hans:埃弗顿;
zh-hk:愛華頓;}-](../Page/埃弗顿足球俱乐部.md "wikilink")（2002年）。在巴黎聖日門期間奪得包括[法國甲組聯賽冠軍](../Page/法國甲組足球聯賽.md "wikilink")、[法國盃](../Page/法國盃.md "wikilink")（兩屆）及[法國聯賽盃等重要錦標](../Page/法國聯賽盃.md "wikilink")。轉戰英倫後，協助熱刺奪得1999年的[英格兰联赛杯](../Page/英格兰联赛杯.md "wikilink")，同年更獲選[英格蘭足球先生及](../Page/英格蘭足球先生.md "wikilink")[PFA足球先生雙料榮譽](../Page/PFA足球先生.md "wikilink")。

### 國家隊

由於真路拿成為[法國於](../Page/法國國家足球隊.md "wikilink")未能晉級決賽周的代罪羔羊，故他只曾17次代表國家隊出賽。1993年11月17日帶著兩分優勢在歐洲第6組-{zh-hans:資格賽;
zh-hk:外圍賽;}-最後一場比賽，主場的[法國只要打和](../Page/法國國家足球隊.md "wikilink")[保加利亞即可出線到美國參加世界盃決賽周](../Page/保加利亞國家足球隊.md "wikilink")，比賽未段比數1-1平手，因真路拿傳中失誤而導致[保加利亞展開反攻](../Page/保加利亞國家足球隊.md "wikilink")，[-{zh-hans:埃米爾·科斯塔迪諾夫;
zh-hk:哥斯達甸諾夫;}-](../Page/埃米爾·科斯塔迪諾夫.md "wikilink")（）射入決勝球，[法國連續兩屆世界盃](../Page/法國國家足球隊.md "wikilink")-{zh-hans:资格赛;
zh-hk:外圍賽;}-出局。[法國接任的新教練](../Page/法國國家足球隊.md "wikilink")[-{zh-hans:埃米·雅凯;
zh-hk:積基特;}-](../Page/埃米·雅凯.md "wikilink")（）再沒有選用真路拿，從此在國家隊中淡出。

## 模特兒

真路拿在他拍攝的[萊雅美髮產品廣告中用獨特的法國](../Page/萊雅.md "wikilink")[口音說出](../Page/口音.md "wikilink")：“歐萊雅，你值得擁有”（），使其英俊瀟灑的形象更深入人心。

## 生涯統計

<table>
<tbody>
<tr class="odd">
<td><p><strong>年度</strong></p></td>
<td><p><strong>效力球會</strong></p></td>
<td><p><strong>級別</strong></p></td>
<td style="text-align: center;"><p><strong>國內比賽</strong><br />
出場/入球</p></td>
<td style="text-align: center;"><p><strong>歐洲比賽</strong><br />
出場/入球</p></td>
</tr>
<tr class="even">
<td><p>1985-86年</p></td>
<td><p>土伦 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>14 / 0</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="odd">
<td><p>1986-87年</p></td>
<td><p>土伦 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>35 / 0</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="even">
<td><p>1987-88年</p></td>
<td><p>土伦 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>33 / 4</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="odd">
<td><p>1988-89年</p></td>
<td><p>巴黎競賽 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>29 / 7</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="even">
<td><p>1989-90年</p></td>
<td><p>巴黎競賽 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>32 / 1</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="odd">
<td><p>1990-91年</p></td>
<td><p>布雷斯特 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>33 / 6</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="even">
<td><p>1991-92年</p></td>
<td><p>布雷斯特 ( 法国 )</p></td>
<td><p><strong>法乙</strong></p></td>
<td style="text-align: center;"><p>17 / 8</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="odd">
<td><p>1992-92年</p></td>
<td><p>巴黎聖日門 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>15 / 3</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="even">
<td><p>1992-93年</p></td>
<td><p>巴黎聖日門 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>34 / 6</p></td>
<td style="text-align: center;"><p>9 / 2</p></td>
</tr>
<tr class="odd">
<td><p>1993-94年</p></td>
<td><p>巴黎聖日門 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>38 / 13</p></td>
<td style="text-align: center;"><p>8 / 2</p></td>
</tr>
<tr class="even">
<td><p>1994-95年</p></td>
<td><p>巴黎聖日門 ( 法国 )</p></td>
<td><p><strong>法甲</strong></p></td>
<td style="text-align: center;"><p>28 / 11</p></td>
<td style="text-align: center;"><p>10 / 1</p></td>
</tr>
<tr class="odd">
<td><p>1995-96年</p></td>
<td><p>紐卡素 ( 英格兰 )</p></td>
<td><p><strong>英超</strong></p></td>
<td style="text-align: center;"><p>34 / 5</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="even">
<td><p>1996-97年</p></td>
<td><p>紐卡素 ( 英格兰 )</p></td>
<td><p><strong>英超</strong></p></td>
<td style="text-align: center;"><p>24 / 1</p></td>
<td style="text-align: center;"><p>7 / 1</p></td>
</tr>
<tr class="odd">
<td><p>1997-98年</p></td>
<td><p>熱刺 （英格兰）</p></td>
<td><p><strong>英超</strong></p></td>
<td style="text-align: center;"><p>34 / 6</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="even">
<td><p>1998-99年</p></td>
<td><p>熱刺 （英格兰）</p></td>
<td><p><strong>英超</strong></p></td>
<td style="text-align: center;"><p>30 / 3</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="odd">
<td><p>1999-00年</p></td>
<td><p>熱刺 （英格兰）</p></td>
<td><p><strong>英超</strong></p></td>
<td style="text-align: center;"><p>34 / 4</p></td>
<td style="text-align: center;"><p>2 / 0</p></td>
</tr>
<tr class="even">
<td><p>2000-01年</p></td>
<td><p>阿士東維拉 （英格兰）</p></td>
<td><p><strong>英超</strong></p></td>
<td style="text-align: center;"><p>26 / 3</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
<tr class="odd">
<td><p>2001-02年</p></td>
<td><p>阿士東維拉 （英格兰）</p></td>
<td><p><strong>英超</strong></p></td>
<td style="text-align: center;"><p>5 / 0</p></td>
<td style="text-align: center;"><p>2 / 0</p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p>愛華頓 （英格兰）</p></td>
<td><p><strong>英超</strong></p></td>
<td style="text-align: center;"><p>5 / 0</p></td>
<td style="text-align: center;"><p>-</p></td>
</tr>
</tbody>
</table>

## 榮譽

  - **[法國甲組足球聯賽](../Page/法國甲組足球聯賽.md "wikilink")**

<!-- end list -->

  -
    冠軍：1994年（[-{zh-hans:巴黎圣日耳曼;
    zh-hk:巴黎聖日門;}-](../Page/巴黎圣日耳曼足球俱乐部.md "wikilink")）

<!-- end list -->

  - **[法國盃](../Page/法國盃.md "wikilink")**

<!-- end list -->

  -
    冠軍：1993年, 1995年（[-{zh-hans:巴黎圣日耳曼;
    zh-hk:巴黎聖日門;}-](../Page/巴黎圣日耳曼足球俱乐部.md "wikilink")）
    亞軍：1990年（）

<!-- end list -->

  - **[法國聯賽盃](../Page/法國聯賽盃.md "wikilink")**

<!-- end list -->

  -
    冠軍：1995年（[-{zh-hans:巴黎圣日耳曼;
    zh-hk:巴黎聖日門;}-](../Page/巴黎圣日耳曼足球俱乐部.md "wikilink")）

<!-- end list -->

  - **[英格兰联赛杯](../Page/英格兰联赛杯.md "wikilink")**

<!-- end list -->

  -
    冠軍：1999年（[-{zh-hans:热刺;
    zh-hk:熱刺;}-](../Page/托特纳姆足球俱乐部.md "wikilink")）

<!-- end list -->

  - 1990-95年間17次代表[法國及射入](../Page/法國國家足球隊.md "wikilink")3球

[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:圖盧茲球員](../Category/圖盧茲球員.md "wikilink")
[Category:巴黎聖日門球員](../Category/巴黎聖日門球員.md "wikilink")
[Category:紐卡素球員](../Category/紐卡素球員.md "wikilink")
[Category:熱刺球員](../Category/熱刺球員.md "wikilink")
[Category:阿士東維拉球員](../Category/阿士東維拉球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:比斯特球員](../Category/比斯特球員.md "wikilink")
[Category:瓦爾省人](../Category/瓦爾省人.md "wikilink")
[Category:義大利裔法國人](../Category/義大利裔法國人.md "wikilink")