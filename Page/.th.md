**.th**為[泰國](../Page/泰国.md "wikilink")[國家及地區頂級域](../Page/國家及地區頂級域.md "wikilink")（ccTLD）的[域名](../Page/域名.md "wikilink")。

目前泰国并不允许申请.th域名，可以申请的域名为二级的.co.th。

## 参考

## 外部連結

  - [IANA .th whois information](http://www.iana.org/root-whois/th.htm)
  - [域名后缀](http://blog.tancee.com/yumingzhonglei.html)

[sv:Toppdomän\#T](../Page/sv:Toppdomän#T.md "wikilink")

[th](../Category/國家及地區頂級域.md "wikilink")
[Category:泰國](../Category/泰國.md "wikilink")