**长江学者奖励计划**是[中华人民共和国教育部与](../Page/中华人民共和国教育部.md "wikilink")[香港](../Page/香港.md "wikilink")[長江實業](../Page/長江實業.md "wikilink")[創辦人](../Page/創辦人.md "wikilink")[李嘉诚为提高中国](../Page/李嘉诚.md "wikilink")[高等学校学术地位](../Page/高等学校.md "wikilink")、振兴[中国高等教育](../Page/中华人民共和国高等教育.md "wikilink")，共同筹资设立的专项计划\[1\]。该计划包括实行特聘长江学者（包括[特聘教授与](../Page/特聘教授.md "wikilink")[讲座教授](../Page/讲座教授.md "wikilink")）和[长江学者成就奖两项内容](../Page/长江学者成就奖.md "wikilink")，2015年增加青年学者项目\[2\]。长江学者的头衔被视为中国文科学者的最高荣誉\[3\]。

## 相关事件

[北京大学中文系副主任](../Page/北京大学中文系.md "wikilink")、教授[沈阳在自传](../Page/沈阳_\(教授\).md "wikilink")《一直在路上——六十年人生风景一瞥》中提及，2011年自己在[北京大学申请学者特聘教授未获学术委员会通过](../Page/北京大学.md "wikilink")。受[南京大学文学院邀请](../Page/南京大学文学院.md "wikilink")，通过跨校申请获得这一头衔，2013年由北京大学转至[南京大学任教](../Page/南京大学.md "wikilink")。2018年4月，[沈阳被控性侵女学生的丑闻爆发后](../Page/沈阳事件.md "wikilink")，南京大学文学院前院长[丁帆否认撤换他人](../Page/丁帆.md "wikilink")，以长江学者“挖角”沈阳一事\[4\]。

## 参见

  - [长江学者讲座教授列表](../Page/长江学者讲座教授列表.md "wikilink")
  - [长江学者特聘教授列表](../Page/长江学者特聘教授列表.md "wikilink")
  - [海外高层次人才引进计划](../Page/海外高层次人才引进计划.md "wikilink")
  - [中國人才簽證優惠制度](../Page/中國人才簽證優惠制度.md "wikilink")

## 参考文献

## 外部链接

  - [长江学者奖励计划-中国教育在线](http://cksp.eol.cn/)

[Category:中华人民共和国科技计划](../Category/中华人民共和国科技计划.md "wikilink")
[Category:中华人民共和国人事制度](../Category/中华人民共和国人事制度.md "wikilink")
[长江学者奖励计划](../Category/长江学者奖励计划.md "wikilink")
[Category:中华人民共和国教育部](../Category/中华人民共和国教育部.md "wikilink")
[Category:中华人民共和国高等教育](../Category/中华人民共和国高等教育.md "wikilink")
[Category:學術榮譽](../Category/學術榮譽.md "wikilink")

1.

2.

3.

4.