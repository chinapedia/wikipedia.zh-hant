[物理學中](../Page/物理學.md "wikilink")，**卡魯扎-克萊因理論**（**Kaluza–Klein
theory**，有時簡稱為**KK theory**）
是一個試圖統一[重力與](../Page/重力.md "wikilink")[電磁兩大基本力的理論模型](../Page/電磁學.md "wikilink")。此理論最初由數學家[西奧多·卡魯扎於](../Page/西奧多·卡魯扎.md "wikilink")1921年所發表。他將[廣義相對論推廣到五維的時空](../Page/廣義相對論.md "wikilink")。
所得方程式可以分成好幾組方程式，其中一個與等價於[愛因斯坦場方程式](../Page/愛因斯坦場方程式.md "wikilink")，另外一組方程式則等價於描述[電磁場的](../Page/電磁場.md "wikilink")[馬克士威方程組](../Page/馬克士威方程組.md "wikilink")。
此外，還多出一個[純量場](../Page/純量場.md "wikilink")——五維[度規張量之分量](../Page/度規張量.md "wikilink")
\(g_{55}\)，其對應粒子稱之為「[輻子](../Page/輻子.md "wikilink")（暫譯）」（radion）。

## 概論

將五維[時空分開成四維的](../Page/時空.md "wikilink")[愛因斯坦方程式以及](../Page/愛因斯坦方程式.md "wikilink")[麦克斯韦方程組是首先由](../Page/麦克斯韦方程組.md "wikilink")[古納爾·諾德斯特諾姆於](../Page/贡纳尔·诺斯特朗姆.md "wikilink")1914年所發現，出現在[他的重力理論內文中](../Page/諾德斯特諾姆重力理論.md "wikilink")，但隨後就被世人遺忘。在1926年，[奧斯卡·克萊因](../Page/奧斯卡·克萊因.md "wikilink")（Oskar
Klein）提議了第四個空間維度捲曲成一個[半徑非常小的](../Page/半徑.md "wikilink")[圓](../Page/圓.md "wikilink")，所以[粒子沿著這個軸移動很短的距離](../Page/基本粒子.md "wikilink")，就會回到起始點。粒子在回到起始點前所能行進的距離則稱作是該維度的大小。這個**額外維度**（extra
dimension）是一個[緊集](../Page/緊集.md "wikilink")，而時空具有緊緻維度的現象則稱作是[紧化](../Page/紧化_\(物理学\).md "wikilink")。
[Dimensions_enroulées_(cercles).PNG](https://zh.wikipedia.org/wiki/File:Dimensions_enroulées_\(cercles\).PNG "fig:Dimensions_enroulées_(cercles).PNG")

現代幾何學中，額外的第五維度可以被理解為[圓群](../Page/圓群.md "wikilink")*[U(1)](../Page/U\(1\).md "wikilink")*，而基本上，[電磁學可以用在](../Page/電磁學.md "wikilink")[纖維叢上](../Page/纖維叢.md "wikilink")[規範群](../Page/規範群.md "wikilink")*U*（1）的[規範場論來詮釋](../Page/規範場論.md "wikilink")。一旦這樣的幾何詮釋能被理解，則將*U*（1）換成廣義的[李群就顯得容易而直觀](../Page/李群.md "wikilink")。這樣的推廣常稱作是[楊-米爾斯理論](../Page/楊-米爾斯理論.md "wikilink")。若要提到兩者的差異，則可說楊–米爾斯理論是在平坦時空的場合處理，而卡魯扎-克萊因理論則是在更具一般性的[彎曲時空中處理](../Page/彎曲時空.md "wikilink")。卡魯扎-克萊因理論的底空間不一定是四維時空，而可以是任何的（[偽](../Page/偽黎曼流形.md "wikilink")）[黎曼流形](../Page/黎曼流形.md "wikilink")，或者甚至是[超對稱流形](../Page/超對稱.md "wikilink")、[轨形或](../Page/轨形.md "wikilink")[非交換空間](../Page/非交換空間.md "wikilink")。

## 時間-空間-物質理論

卡魯扎-克萊因理論的一個特別的變形是所謂的**[時間](../Page/時間.md "wikilink")-[空間](../Page/空間.md "wikilink")-[物質理論](../Page/物質.md "wikilink")**（**space-time-matter
theory**）或稱**引生物質理論**（**induced matter theory**），主要是由Paul
Wesson及其他人所推廣，他們組成所謂的[Space-Time-Matter
Consortium](https://web.archive.org/web/20070905183943/http://astro.uwaterloo.ca/~wesson/)。在這理論版本中，值得注意的是下面方程式所得的解：

\[R_{AB}=0\]，

其中\(R_{AB}\)是五維[里奇曲率](../Page/里奇曲率.md "wikilink")，也可以在四維中重新表述，這樣的解滿足[愛因斯坦方程式](../Page/愛因斯坦方程式.md "wikilink")：

\[G_{\mu\nu} = 8\pi T_{\mu\nu}\]，

其中\(T_{\mu\nu}\)的精準形式來自於五維空間中的[里奇平坦条件](../Page/里奇平坦条件.md "wikilink")（Ricci-flat
condition）。既然[能量-動量張量](../Page/能量-動量張量.md "wikilink")\(T_{\mu\nu}\)常被了解為四維空間中的物質密度，上面的結果則被詮釋成：四維物質是引生自五維空間中的幾何。

特別是\(R_{AB}=0\)的[孤立子](../Page/孤立子.md "wikilink")（soliton）解可被展示：其包含了輻射主導形式（早期宇宙）與物質主導形式（晚期宇宙）中的[羅伯遜-沃爾克度規](../Page/羅伯遜-沃爾克度規.md "wikilink")（Robertson-Walker
metric）。一般方程式則可被展示與古典範疇的[重力理論測試相符](../Page/廣義相對論的測試.md "wikilink")，在物理學原則上可以被接受，而其仍留有相當多的[自由度可提供一些有趣的](../Page/自由度.md "wikilink")[宇宙學模型](../Page/宇宙學模型.md "wikilink")。

## 幾何詮釋

## 相關條目

  - [DGP模型](../Page/DGP模型.md "wikilink")
  - [超重力](../Page/超重力.md "wikilink")
  - [紧致化](../Page/紧化_\(物理学\).md "wikilink")
  - [卡拉比-丘流形](../Page/卡拉比-丘流形.md "wikilink")

[K](../Category/廣義相對論.md "wikilink") [K](../Category/重力理论.md "wikilink")