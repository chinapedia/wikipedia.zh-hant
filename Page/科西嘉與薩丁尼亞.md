**科西嘉與薩丁尼亞行省**（Corsica et
Sardinia）是[羅馬帝國的一個](../Page/羅馬帝國.md "wikilink")[行省](../Page/罗马行省.md "wikilink")。

[腓尼基人是第一批在該地建立商業據點的人](../Page/腓尼基人.md "wikilink")。[希臘人後來也在該地建立了殖民地](../Page/希臘人.md "wikilink")。西元前535年，迦太基人協助[伊特拉斯坎人征服了希臘人在](../Page/伊特拉斯坎人.md "wikilink")[科西嘉島上的殖民地阿拉利亞](../Page/科西嘉島.md "wikilink")（Alalia或Aléria）。[薩丁尼亞島與科西嘉島都落入了迦太基人的控制之中](../Page/薩丁尼亞島.md "wikilink")。

[第一次布匿戰爭之後](../Page/第一次布匿戰爭.md "wikilink")，[羅馬人在西元前](../Page/羅馬人.md "wikilink")259年攻擊薩丁尼亞但未成功。西元前241年迦太基人被打敗後，為迦太基人服務的薩丁尼亞的傭兵趁機反抗迦太基，並於西元前238年向羅馬人求助。於是羅馬與迦太基之間的戰端再啟，羅馬派艦隊攻擊[迦太基](../Page/迦太基.md "wikilink")。西元前229年，迦太基割讓科西嘉島與薩丁尼亞島給羅馬，失去了在地中海上兩個重要的島嶼。[第二次布匿戰爭之後](../Page/第二次布匿戰爭.md "wikilink")，迦太基曾試圖奪回薩丁尼亞但未成功。

控制海岸與平原的羅馬人用了將近一個世紀的時間試圖征服島上內陸地區的原住民。一直到了西元前27年，由於情勢一度大致平定下來，該省變成一個[元老院行省](../Page/元老院行省.md "wikilink")（法語province
sénatoriale）。然而後來情勢又動蕩使得搶劫掠奪盛行，66年左右羅馬又把該省改為[元首行省](../Page/元首行省.md "wikilink")（法語province
impériale）並派遣軍團。由於當地生活艱苦，該省多次充當[羅馬帝國的流刑地](../Page/羅馬帝國.md "wikilink")。

[汪達爾人入侵北非之後](../Page/汪達爾人.md "wikilink")，在456年左右佔領了科西嘉島與薩丁尼亞島。

[Category:罗马帝国行省](../Category/罗马帝国行省.md "wikilink")
[Category:意大利行政区划史](../Category/意大利行政区划史.md "wikilink")
[Category:法国行政区划史](../Category/法国行政区划史.md "wikilink")
[Category:科西嘉大区](../Category/科西嘉大区.md "wikilink")