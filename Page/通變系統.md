**通變系統**（**Diasystem**）是一個[語言學的名詞](../Page/語言學.md "wikilink")，指同一種語言因為地理、政治、文字或其他非語言學的原因，而被認為是兩種不同的語言系統。它不同於[方言](../Page/方言.md "wikilink")：方言可能是因為長久的分隔，而使語言間產生了語音上的變化，繼而發展成為方言。但對於通變系統內的語言，卻沒有這種語音上的變化；不過亦有可能因為政治或地理環境的變化，而使用詞有所不同。

## 例子

  - [印度斯坦語](../Page/印度斯坦語.md "wikilink")（[印地語](../Page/印地語.md "wikilink")-[烏爾都語](../Page/烏爾都語.md "wikilink")）
  - [塞克語](../Page/塞尔维亚-克罗地亚语.md "wikilink")（[波斯尼亞語](../Page/波斯尼亞語.md "wikilink")-[克羅地亞語](../Page/克羅地亞語.md "wikilink")-[塞爾維亞語](../Page/塞爾維亞語.md "wikilink")）
  - [丹挪語](../Page/丹挪語.md "wikilink")（[丹麥語](../Page/丹麥語.md "wikilink")-[書面挪威語](../Page/書面挪威語.md "wikilink")-[新挪威語](../Page/新挪威語.md "wikilink")）
  - [奧克羅曼語](../Page/奥克-罗曼语支.md "wikilink")（[奥克语](../Page/奥克语.md "wikilink")-[加泰罗尼亚语](../Page/加泰罗尼亚语.md "wikilink")）
  - [葡萄牙-加利西亞語](../Page/葡萄牙-加利西亞語.md "wikilink")（[葡萄牙語](../Page/葡萄牙語.md "wikilink")-[加里西亞語](../Page/加里西亞語.md "wikilink")）
  - [馬來語](../Page/馬來語.md "wikilink")（[馬來西亞語](../Page/馬來西亞語.md "wikilink")-[印尼語](../Page/印尼語.md "wikilink")）
  - [波斯語](../Page/波斯語.md "wikilink")-[塔吉克語](../Page/塔吉克語.md "wikilink")-[達利語](../Page/達利語.md "wikilink")
  - [羅馬尼亞語](../Page/羅馬尼亞語.md "wikilink")-[摩爾多瓦語](../Page/摩爾多瓦語.md "wikilink")
  - [西北官話](../Page/西北官話.md "wikilink")-[東干語](../Page/東干語.md "wikilink")
  - [英語](../Page/英語.md "wikilink")（[英语](../Page/英式英语.md "wikilink")-[美語](../Page/美語.md "wikilink")）
  - [泰语](../Page/泰语.md "wikilink")（[泰语](../Page/泰语.md "wikilink")-[老挝语](../Page/老挝语.md "wikilink")）

## 参见

  - （Dachsprache）

  - [方言连续体](../Page/方言连续体.md "wikilink")

  - [多中心語言](../Page/多中心語言.md "wikilink")

  - [标准语](../Page/标准语.md "wikilink")

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - .

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 延伸閱讀

  -
  -
  -
  -
  -
  -
  -
  -
  -
[Category:語言種類與風格](../Category/語言種類與風格.md "wikilink")