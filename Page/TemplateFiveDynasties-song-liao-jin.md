[后唐](../Page/后唐.md "wikilink"){{.w}}[后晋](../Page/后晋.md "wikilink"){{.w}}[後漢](../Page/後漢.md "wikilink"){{.w}}[后周](../Page/后周.md "wikilink")

|group2 = 十国 |list2 =
[前蜀](../Page/前蜀.md "wikilink"){{.w}}[後蜀](../Page/後蜀.md "wikilink"){{.w}}[南唐](../Page/南唐.md "wikilink"){{.w}}[吴](../Page/杨吴.md "wikilink"){{.w}}[吴越](../Page/吴越国.md "wikilink"){{.w}}[閩](../Page/闽_\(十国\).md "wikilink"){{.w}}[楚](../Page/马楚.md "wikilink"){{.w}}[南汉](../Page/南汉.md "wikilink"){{.w}}[南平](../Page/荆南.md "wikilink")（荆南）{{.w}}[北漢](../Page/北汉.md "wikilink")

|group3 = 皇朝 |list3 =
[宋](../Page/宋朝.md "wikilink"){{.w}}[金](../Page/金朝.md "wikilink")

|group4 = 华北 |list4 =
[燕](../Page/燕_\(五代\).md "wikilink"){{.w}}[晋](../Page/晉_\(五代\).md "wikilink"){{.w}}[岐](../Page/岐.md "wikilink"){{.w}}[北平](../Page/北平_\(五代\).md "wikilink"){{.w}}[成德军](../Page/成德節度使.md "wikilink"){{.w}}[朔方军](../Page/朔方节度使.md "wikilink"){{.w}}[赵](../Page/赵_\(五代\).md "wikilink"){{.w}}[义武军](../Page/义武军节度使.md "wikilink"){{.w}}[李梁](../Page/李從益.md "wikilink")（[李從益](../Page/李從益.md "wikilink")）{{.w}}[刘齐](../Page/刘齐.md "wikilink"){{.w}}[辽](../Page/辽朝.md "wikilink"){{.w}}

|group5 = 东北 |list5 =
[渤海](../Page/渤海国.md "wikilink"){{.w}}[東丹](../Page/东丹.md "wikilink"){{.w}}[定安](../Page/定安国.md "wikilink"){{.w}}[后渤海](../Page/后渤海.md "wikilink")（渤海[大光显](../Page/大光显.md "wikilink")）{{.w}}[兴遼](../Page/兴辽.md "wikilink"){{.w}}[北遼](../Page/北辽.md "wikilink"){{.w}}[东夏](../Page/东夏.md "wikilink")（大真、东真）{{.w}}[东辽](../Page/东辽国.md "wikilink"){{.w}}[后辽](../Page/后辽.md "wikilink")

|group6 = 西北 |list6 =
[定难军](../Page/定难节度使.md "wikilink"){{.w}}[归义军](../Page/归义军.md "wikilink"){{.w}}[河西军](../Page/河西节度使.md "wikilink"){{.w}}[-{于}-阗](../Page/于阗.md "wikilink")（金玉国）{{.w}}[畏兀儿](../Page/畏兀儿.md "wikilink")（西州回鹘、高昌回鹘、畏吾儿){{.w}}[甘州回鶻](../Page/甘州回鶻.md "wikilink"){{.w}}[龟兹回鹘](../Page/龟兹回鹘.md "wikilink"){{.w}}[葛逻禄](../Page/葛逻禄.md "wikilink"){{.w}}[九姓乌护](../Page/九姓烏古斯.md "wikilink"){{.w}}[样磨](../Page/样磨.md "wikilink"){{.w}}[仲云](../Page/仲云国.md "wikilink"){{.w}}[喀喇汗](../Page/喀喇汗国.md "wikilink")（[东喀喇汗](../Page/东喀喇汗.md "wikilink")、[西喀喇汗](../Page/西喀喇汗.md "wikilink")）{{.w}}[西遼](../Page/西辽.md "wikilink"){{.w}}[定難軍](../Page/定难节度使.md "wikilink"){{.w}}[西夏](../Page/西夏.md "wikilink")

|group7 = 东南 |list7 =
[殷](../Page/闽_\(十国\).md "wikilink"){{.w}}[清源军](../Page/清源軍.md "wikilink")（平海军）{{.w}}[武平军](../Page/武平军节度使.md "wikilink"){{.w}}[静海军](../Page/静海军节度使.md "wikilink"){{.w}}[張楚](../Page/大楚_\(張邦昌\).md "wikilink"){{.w}}[吴蜀](../Page/吴曦_\(南宋\).md "wikilink")

|group8 = 西南 |list8 =
[南詔](../Page/南诏.md "wikilink"){{.w}}[大长和](../Page/大長和.md "wikilink"){{.w}}[大天兴](../Page/大天兴.md "wikilink"){{.w}}[大义宁](../Page/大義寧.md "wikilink"){{.w}}[大理](../Page/大理国.md "wikilink"){{.w}}[大中](../Page/大中_\(國家\).md "wikilink"){{.w}}[後大理](../Page/大理国.md "wikilink"){{.w}}[昆明](../Page/昆明大鬼主.md "wikilink"){{.w}}[牂柯蛮](../Page/牂柯郡.md "wikilink")

|group9 = 青藏高原诸部 |list9 =
[唃厮啰](../Page/唃厮啰国.md "wikilink")（青唐羌、邈川吐蕃、黄头回纥）{{.w}}[六-{谷}-部](../Page/六谷部.md "wikilink")（河西吐蕃）{{.w}}[吐蕃诸部](../Page/吐蕃分裂时期.md "wikilink")（[拉萨王系](../Page/拉萨王系.md "wikilink")、[雅隆觉阿王系](../Page/雅隆觉阿王系.md "wikilink")、[拉达克王系](../Page/拉达克王系.md "wikilink")、[古格王系](../Page/古格王朝.md "wikilink")、[普兰王系](../Page/普兰王系.md "wikilink")、[亚泽王系](../Page/亚泽王朝.md "wikilink")、[贡塘王系](../Page/贡塘王系.md "wikilink")）

|group10 = 蒙古草原诸部 |list10 =
[乞颜](../Page/乞颜.md "wikilink")（[蒙兀国](../Page/蒙兀国.md "wikilink"){{.w}}[主儿乞](../Page/主儿乞部.md "wikilink"){{.w}}[泰赤乌](../Page/泰赤乌部.md "wikilink")）{{.w}}[札达兰](../Page/札达兰部.md "wikilink"){{.w}}[巴魯剌思](../Page/巴魯剌思氏.md "wikilink"){{.w}}[札剌亦兒](../Page/札剌亦兒.md "wikilink"){{.w}}[弘吉剌](../Page/弘吉剌.md "wikilink"){{.w}}[蔑兒乞](../Page/蔑兒乞.md "wikilink"){{.w}}[塔塔儿](../Page/塔塔儿部.md "wikilink"){{.w}}[克烈](../Page/克烈.md "wikilink"){{.w}}[乃蛮](../Page/乃蛮.md "wikilink"){{.w}}[汪古](../Page/汪古部.md "wikilink")（白鞑靼）{{.w}}[黑鞑靼](../Page/黑鞑靼.md "wikilink")
}}<noinclude> </noinclude>

[Category:中国历史导航模板](../Category/中国历史导航模板.md "wikilink")