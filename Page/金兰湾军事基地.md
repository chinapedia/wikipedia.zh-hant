''[Cam_Ranh_Bay_Landsat_elevation_image.png](https://zh.wikipedia.org/wiki/File:Cam_Ranh_Bay_Landsat_elevation_image.png "fig:Cam_Ranh_Bay_Landsat_elevation_image.png")
''
[Cam_Ranh_Naval_base_(concept).JPEG](https://zh.wikipedia.org/wiki/File:Cam_Ranh_Naval_base_\(concept\).JPEG "fig:Cam_Ranh_Naval_base_(concept).JPEG")
[Cam_Ranh_Bay.jpg](https://zh.wikipedia.org/wiki/File:Cam_Ranh_Bay.jpg "fig:Cam_Ranh_Bay.jpg")

**金兰湾**（），是[越南中南部](../Page/越南.md "wikilink")[慶和省境內的一個](../Page/慶和省.md "wikilink")[港灣](../Page/港灣.md "wikilink")、面向[南中國海的一個優良港](../Page/南中國海.md "wikilink")。[位置在](../Page/位置.md "wikilink")[东经](../Page/东经.md "wikilink")109度、[北緯](../Page/北緯.md "wikilink")12度。金兰湾群山环抱，南北都有陆地包围，港湾深入内陆17公里，由两个半岛合抱成葫芦形的内外两个海湾，内港**金兰**，面积60平方公里，水深1-15米，湾口仅宽1300米，外港**平巴**，水深10-22米，湾口宽约4000米，口外水深30米以上。水深可停泊[航空母舰](../Page/航空母舰.md "wikilink")，被认为是世界上最好的[深水港之一](../Page/深水港.md "wikilink")，它同时位于沟通[太平洋和](../Page/太平洋.md "wikilink")[印度洋的重要水路上](../Page/印度洋.md "wikilink")，具有极其重要的战略价值。

該港灣內建有**金兰湾军事基地**，是[越南的重要](../Page/越南.md "wikilink")[军港](../Page/军港.md "wikilink")，建于[慶和省境內](../Page/慶和省.md "wikilink")、越南东南部海岸向前突出的弧形顶点。

## 概要

在[法属印度支那時期](../Page/法属印度支那.md "wikilink")，此地多作為[軍事基地](../Page/軍事基地.md "wikilink")。[日俄战争中](../Page/日俄战争.md "wikilink")，[波羅的海艦隊停靠於此](../Page/波羅的海艦隊.md "wikilink")。[太平洋戰爭中](../Page/太平洋戰爭.md "wikilink")，[日本海軍也使用此軍港](../Page/日本海軍.md "wikilink")。

1960年代[越南战争期间](../Page/越南战争.md "wikilink")，[南越政府将金兰湾租给](../Page/越南共和国.md "wikilink")[美国作为军事基地](../Page/美国.md "wikilink")，美国对其进行了全面的扩建，在战争中使用该基地出动飞机对[北越地区进行轰炸](../Page/越南社会主义共和国.md "wikilink")。越战结束后，美军被迫撤离该基地。

1970年代中期，由於[中蘇交惡](../Page/中蘇交惡.md "wikilink")，越南當局開始倒向蘇聯，並導致[中越戰爭爆發](../Page/中越戰爭.md "wikilink")。1979年，[苏联同越南签订协议](../Page/苏联.md "wikilink")，无偿租用金兰湾25年，用于控制[东南亚地区和其沿海一带](../Page/东南亚.md "wikilink")。1980年代，苏联进一步对金兰湾进行了扩建，使其成为苏联海外最大的军事基地。

1992年5月，[苏联解体后](../Page/苏联解体.md "wikilink")，俄罗斯因財政困難，不願繳交每年2亿[美元的租金](../Page/美元.md "wikilink")。2002年5月2日，[俄罗斯海軍駐越單位撤出金兰湾](../Page/俄罗斯海軍.md "wikilink")\[1\]。
之后美国和[中国都对该军事基地产生兴趣](../Page/中国.md "wikilink")，有意租用。

2003年，該地的空軍基地被改建為民用的機場。

2010年3月，越南總理[阮晉勇宣布該軍港將進行](../Page/阮晉勇.md "wikilink")3年升級計劃，完成後將開放予外國軍船使用。隨著[南海諸島主權爭議升溫](../Page/南海諸島.md "wikilink")，加上[中国人民解放軍航空母艦](../Page/中国人民解放軍.md "wikilink")[辽宁号投入使用的潛在衝突](../Page/辽宁号航空母舰.md "wikilink")，使得越南當局大感緊張，並準備展開與[美國海軍的一連串合作](../Page/美國海軍.md "wikilink")，其中包含金蘭灣軍港租用事宜。

2016年3月，该基地的一部分被改建为专门服务外国舰船的国际新港。该港专门用于为世界各国的船只补给、维修和人员休整，同时可以储存燃料。同年10月，俄罗斯国防部发布消息称正在考虑在金兰湾建立军事基地。越南对此作出回应，称不会把金兰湾租给俄方使用。越南外交部发言人黎海平在记者会上表示：“越南的一贯立场是坚持军事不结盟，不与其他国家结盟对抗第三方。我们不会允许任何国家在越南设立军事基地。”越南海军中将阮国烁称，越南奉行独立自主的外交政策，保持与许多国家的友好联系，因此越方不会向某一个国家整体租借金兰湾，但欢迎各国使用金兰湾的商业化港口服务，所以俄罗斯只可以和其他国家一样使用金兰湾国际新港。

## 相关条目

  - （今[金蘭國際機場](../Page/金蘭國際機場.md "wikilink")）

  - [駐越美軍](../Page/駐越美軍.md "wikilink")

  - [南越軍](../Page/南越軍.md "wikilink")

  - [岘港](../Page/岘港.md "wikilink")

## 参考

  - [谁在觊觎金兰湾军事基地](http://www.people.com.cn/digest/200106/15/gj061505.html)
  - [越美关系升温
    美军舰访越试图重返金兰湾？](http://www.phoenixtv.com/phoenixtv/76571093564915712/20050504/544863.shtml)
  - [美越军事关系迅速回暖
    美军能否重返金兰湾？](http://www.people.com.cn/GB/junshi/1078/2199885.html)
  - [俄罗斯年内撤出越南金兰湾
    美国开出条件想取而代之](https://web.archive.org/web/20050309130325/http://mil.qianlong.com/27/2002-3-28/54@107577.htm)
  - [中国拟租用越南金兰湾军事基地](http://www.peacehall.com/news/gb/china/2005/11/200511011001.shtml)

[Category:越南军事](../Category/越南军事.md "wikilink")
[Category:越南港口](../Category/越南港口.md "wikilink")
[Category:俄罗斯和苏联海军基地](../Category/俄罗斯和苏联海军基地.md "wikilink")
[Category:越南海灣](../Category/越南海灣.md "wikilink")
[Category:南中國海](../Category/南中國海.md "wikilink")
[Category:蘇越關係](../Category/蘇越關係.md "wikilink")
[Category:俄越關係](../Category/俄越關係.md "wikilink")
[Category:美越關係](../Category/美越關係.md "wikilink")
[Category:越南戰爭美軍基地](../Category/越南戰爭美軍基地.md "wikilink")

1.  「[ロシアの声](../Page/ロシアの声.md "wikilink")」日本語課"[カムラン湾にロシア海軍基地復活か](http://japanese.ruvr.ru/2010/10/08/24844075.html)"\<[ウェブ魚拓](http://megalodon.jp/2012-0603-2206-41/japanese.ruvr.ru/2010/10/08/24844075.html)\>2010年10月8日（2012年6月3日閲覧。）