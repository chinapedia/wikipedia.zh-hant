**托莫特**
（）是[俄罗斯](../Page/俄罗斯.md "wikilink")[萨哈共和国的一个城市](../Page/萨哈共和国.md "wikilink")。位于[雅库茨克以南](../Page/雅库茨克.md "wikilink")450公里处。人口8057人（2010年人口普查）。\[1\]

[缩略图](https://zh.wikipedia.org/wiki/File:Yakutia_notext.svg "fig:缩略图")

## 詞源

這個城鎮的名字來自[雅庫特語，意思是非凍結的](../Page/雅库特语.md "wikilink")。

## 地理

該鎮是阿穆爾雅庫茨克乾線的客運列車的終點站。2011年11月，鐵路延伸至Nizhny Bestyakh ;
它最終將達到[雅库茨克](../Page/雅库茨克.md "wikilink")\[2\]。鐵路和[勒拿公路交會在](../Page/勒拿公路.md "wikilink")[阿爾丹河在這一點上](../Page/阿爾丹河.md "wikilink")。

## 經濟

城鎮附近的流被發現雲母之後。雲母礦產於1942年開發。

## 註釋

<references />

## 參考資料

  - Official website of the Sakha Republic. *Registry of the
    Administrative-Territorial Divisions of the Sakha Republic*.
    [Aldansky District](https://www.sakha.gov.ru/aldanskiy-ulus-rayon).
  - Государственное Собрание (Ил Тумэн) Республики Саха
    (Якутия). Закон №173-З №353-III от 30 ноября 2004 г.
    «Об установлении границ и о наделении статусом городского и
    сельского поселений муниципальных образований Республики
    Саха (Якутия)», в ред. Закона №1058-З №1007-IV от 25 апреля 2012
    г. «О внесении изменений в Закон Республики Саха (Якутия) "Об
    установлении границ и о наделении статусом городского и
    сельского поселений муниципальных образований Республики Саха
    (Якутия)"». Вступил в силу со дня официального опубликования.
    Опубликован: "Якутия", №245, 31 декабря 2004
    г. (. Law \#173-Z No. 353-III of November 30, 2004 *On
    Establishing the Borders and on Granting the Urban and Rural
    Settlement Status to the Municipal Formations of the Sakha (Yakutia)
    Republic*, as amended by the Law \#1058-Z No. 1007-IV of April 25,
    2012 *On Amending the Law of the Sakha (Yakutia) Republic "On
    Establishing the Borders and on Granting the Urban and Rural
    Settlement Status to the Municipal Formations of the Sakha (Yakutia)
    Republic"*. Effective as of the day of the official publication.).

[State Assembly (Il Tumen) of the Sakha (Yakutia)
Republic](../Page/en:State_Assembly_of_the_Sakha_Republic.md "wikilink")

[Category:萨哈共和国城市](../Category/萨哈共和国城市.md "wikilink")

1.
2.