**AIBO**為[Sony所研發的電子](../Page/Sony.md "wikilink")[寵物](../Page/寵物.md "wikilink")[狗](../Page/狗.md "wikilink")，常出現於一些[機器人展覽中](../Page/機器人.md "wikilink")。
[RUNSWift_AIBOS.jpg](https://zh.wikipedia.org/wiki/File:RUNSWift_AIBOS.jpg "fig:RUNSWift_AIBOS.jpg")[索尼公司開發的電子狗愛寶在踢球](../Page/索尼公司.md "wikilink")\]\]

## 相關條目

  - [QRIO](../Page/QRIO.md "wikilink")：Sony研發的[機器人](../Page/機器人.md "wikilink")
  - [ASIMO](../Page/ASIMO.md "wikilink")：[本田研發的機器人](../Page/本田.md "wikilink")

## 外部連結

  - [aibo](https://aibo.sony.jp)

[Category:動物型機器人](../Category/動物型機器人.md "wikilink")
[Category:索尼产品](../Category/索尼产品.md "wikilink")
[Category:日本機器人](../Category/日本機器人.md "wikilink")