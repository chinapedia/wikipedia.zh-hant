**戟龍屬**（[學名](../Page/學名.md "wikilink")：*Styracosaurus*），又名**刺盾角龍**，在[希臘文意為](../Page/希臘文.md "wikilink")「有尖刺的[蜥蜴](../Page/蜥蜴.md "wikilink")」\[1\]，是[角龍下目](../Page/角龍下目.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，生存於[白堊紀](../Page/白堊紀.md "wikilink")[坎潘階](../Page/坎潘階.md "wikilink")，約7650萬年前到7500萬年前。戟龍的頭盾延伸出四或六個長角，兩頰各有一個較小的角，以及一個從鼻部延伸出的角，這個單獨的角約60公分長、15公分寬。這些角狀物與頭盾的功能已經爭論很多年。

戟龍是種大型恐龍，身長5.5公尺，高度約1.8公尺，重量約3噸。戟龍擁有短四肢，以及笨重的身體。戟龍的尾巴相當短。牠們有喙狀嘴，以及平坦的頰齒，顯示牠們是[草食性恐龍](../Page/草食性.md "wikilink")。如同其他角龍類，戟龍可能是[群居動物](../Page/群居動物.md "wikilink")，以大群體方式遷徙，這理論從[屍骨層可以透露出來](../Page/屍骨層.md "wikilink")。

戟龍是由[勞倫斯·賴博在](../Page/勞倫斯·賴博.md "wikilink")1913年命名，是[尖角龍亞科的物種](../Page/尖角龍亞科.md "wikilink")。戟龍目前只有一個種：**亞伯達戟龍**（*S.
albertensis*）。而帕克氏戟龍（*S.
parksi*）常被認為是亞伯達戟龍的[異名](../Page/異名.md "wikilink")。其他曾被列入戟龍的種，例如︰卵圓戟龍（*S.
ovatus*），已經改列為其他屬。

## 敘述

戟龍的[成年個體身長約](../Page/成年禮.md "wikilink")5.5公尺，重達2.7噸\[2\]。頭顱非常巨大，擁有大型鼻孔、高大的鼻角，頭盾上有四到六個尖角，數量依個體而不同。頭盾上的四個最長角，每個幾乎跟鼻部的角一樣長，約50到55公分\[3\]。[正模標本的鼻角有部分殘缺](../Page/正模標本.md "wikilink")，根據估計，完整的鼻角至少有57公分長\[4\]。根據其他戟龍標本、與[尖角龍的鼻角](../Page/尖角龍.md "wikilink")，這個角應該從中間微彎，而非筆直\[5\]。
[Human-styracosaurus_size_comparison.svg](https://zh.wikipedia.org/wiki/File:Human-styracosaurus_size_comparison.svg "fig:Human-styracosaurus_size_comparison.svg")
除了大型鼻角與頭盾上的尖角，戟龍的[頭部裝飾物非常多變](../Page/头.md "wikilink")。有些個體的頭盾的臉頰兩側位置有較小的角，類似[尖角龍頭盾兩側的小角](../Page/尖角龍.md "wikilink")，但較小；某些個體的臉頰兩側位置，則是具有突出物。某些個體具有三對頭盾尖角，例如正模標本。戟龍的最內側一對角則往外彎曲。戟龍的頭盾邊緣有許多小型突起，但並非每個標本都有\[6\]。如同大部分角龍科恐龍，戟龍頭盾上有大型洞孔。嘴部前方是缺乏牙齒的喙狀嘴。

戟龍的龐大體型類似[犀牛的體型](../Page/犀牛.md "wikilink")。戟龍的強壯肩膀可能用在物種內的打鬥中。戟龍有相當短的尾巴。每個腳趾有蹄狀爪，由角質包覆\[7\]。臀部有10節癒合的[薦椎](../Page/薦椎.md "wikilink")，數量超過其他恐龍（不包含鳥類）\[8\]。

戟龍以及角龍科恐龍的四肢姿勢有過不同的假設，包括前肢直立於身體之下，或是前肢呈現往兩側伸展姿勢。最近的研究提出戟龍最有可能採取兩種說法中間的蹲伏姿勢\[9\]。古生物學家[格里高利·保羅](../Page/格里高利·保羅.md "wikilink")（Gregory
S.
Paul），以及[丹麥](../Page/丹麥.md "wikilink")[哥本哈根大學動物博物館的](../Page/哥本哈根大學.md "wikilink")[佩爾·克里斯坦森](../Page/佩爾·克里斯坦森.md "wikilink")（Per
Christiansen），基於可能由角龍類留下的非兩側伸展式[足跡化石](../Page/足跡化石.md "wikilink")，提出大型角龍類如戟龍能夠以[大象的方式奔跑](../Page/大象.md "wikilink")\[10\]。

## 分類

[Styracosaurus_BW.jpg](https://zh.wikipedia.org/wiki/File:Styracosaurus_BW.jpg "fig:Styracosaurus_BW.jpg")
戟龍屬於[角龍科的](../Page/角龍科.md "wikilink")[尖角龍亞科](../Page/尖角龍亞科.md "wikilink")，尖角龍亞科生存於[北美洲](../Page/北美洲.md "wikilink")，特徵是突出的鼻角、不明顯的額角、短頭盾與短[鱗骨](../Page/鱗骨.md "wikilink")、高長的臉部、以及往後方延伸的鼻部洞孔\[11\]。尖角龍亞科演化支的其他物種有：[尖角龍](../Page/尖角龍.md "wikilink")\[12\]\[13\]、[厚鼻龍](../Page/厚鼻龍.md "wikilink")\[14\]\[15\]、[愛氏角龍](../Page/愛氏角龍.md "wikilink")\[16\]、[野牛龍](../Page/野牛龍.md "wikilink")\[17\]\[18\]、[亞伯達角龍](../Page/亞伯達角龍.md "wikilink")\[19\]、[河神龍](../Page/河神龍.md "wikilink")\[20\]、[短角龍](../Page/短角龍.md "wikilink")\[21\]、以及[獨角龍](../Page/獨角龍.md "wikilink")\[22\]，但最後兩個是[疑名](../Page/疑名.md "wikilink")。因為尖角龍亞科的不同種、甚至不同個體的差異性，所以一直有爭論哪些屬、種是有效的；尤其是[尖角龍與](../Page/尖角龍.md "wikilink")[獨角龍是否有效屬](../Page/獨角龍.md "wikilink")，還是相同物種的不同性別。在1996年，[彼得·達德森](../Page/彼得·達德森.md "wikilink")（Peter
Dodson）發現尖角龍、戟龍、獨角龍之間有足夠的差異性可成立獨立的屬，而戟龍與尖角龍的關係較親近，而離獨角龍關係較遠。達德森認為獨角龍中的角鼻獨角龍（*M.
nasicornis*）可能是雌性戟龍\[23\]。他的論點只有部分人採納，其他研究人員並不接受角鼻獨角龍是雌性戟龍的觀點，或獨角龍為有效屬\[24\]。較早的角龍類恐龍[原角龍被假設具有](../Page/原角龍.md "wikilink")[兩性異形](../Page/兩性異形.md "wikilink")\[25\]，但沒有證據顯示角龍科恐龍為[兩性異形動物](../Page/兩性異形.md "wikilink")\[26\]\[27\]\[28\]。

在1992年的一份研究，根據發現於蒙大拿州[雙麥迪遜組的眾多頭顱骨](../Page/雙麥迪遜組.md "wikilink")，將戟龍、[野牛龍](../Page/野牛龍.md "wikilink")、[河神龍](../Page/河神龍.md "wikilink")、[厚鼻龍歸類於同一](../Page/厚鼻龍.md "wikilink")[演化支](../Page/演化支.md "wikilink")\[29\]。由於戟龍的部分標本，目前被建立為獨立屬，[刺叢龍](../Page/刺叢龍.md "wikilink")（*Rubeosaurus*），戟龍在這個演化支的位置，目前仍有爭議\[30\]。

### 演化起源

戟龍的演化起源，因為早期角龍下目恐龍的化石非常稀少，所以很多年來無法確定。在1922年發現的[原角龍](../Page/原角龍.md "wikilink")，稍微顯示出與早期[角龍科恐龍的關係](../Page/角龍科.md "wikilink")\[31\]。在90年代晚期發現的[祖尼角龍](../Page/祖尼角龍.md "wikilink")，是已知第一種有額角的角龍下目恐龍。而[隱龍是已知第一種](../Page/隱龍.md "wikilink")[侏儸紀角龍下目恐龍](../Page/侏儸紀.md "wikilink")。這些新發現非常重要，並描繪出角龍類恐龍的起源，牠們起源於侏儸紀的亞洲，而真正有角的角龍類出現在晚白堊紀之初的北美洲\[32\]。

## 發現與種

戟龍的第一個化石是由[查爾斯·斯騰伯格](../Page/查爾斯·斯騰伯格.md "wikilink")（Charles Mortram
Sternberg）在[加拿大](../Page/加拿大.md "wikilink")[亞伯達省的](../Page/亞伯達省.md "wikilink")[恐龍公園組所發現](../Page/恐龍公園組.md "wikilink")，並由[勞倫斯·賴博](../Page/勞倫斯·賴博.md "wikilink")（Lawrence
Lambe）在1913年所命名。在1935年，[皇家安大略博物館的工作人員重新來到恐龍公園組](../Page/皇家安大略博物館.md "wikilink")，並發現遺失的下頜與骨骸的大部分。這些化石顯示**亞伯達戟龍**（*S.
albertensis*）身長約5.5到5.8公尺，臀部高度為1.65公尺\[33\]。這個正模標本的特徵是頭盾左側的最小尖角，與相鄰尖角的基部相連。這個戟龍生前可能遭到傷害，使得頭盾斷裂，短了約6公分。由於沒有發現頭盾的右側，這個部份的正確形狀仍無法得知\[34\]。
[Styracosaurus_skeleton.jpg](https://zh.wikipedia.org/wiki/File:Styracosaurus_skeleton.jpg "fig:Styracosaurus_skeleton.jpg")
在1915年，任職於[紐約](../Page/紐約.md "wikilink")[美國自然歷史博物館的](../Page/美國自然歷史博物館.md "wikilink")[巴納姆·布郎](../Page/巴納姆·布郎.md "wikilink")（Barnum
Brown）與與[埃里希·馬蘭·史萊克](../Page/埃里希·馬蘭·史萊克.md "wikilink")（Erich Maren
Schlaikjer），挖掘了一個接近完整的骨骸與一個部份頭顱骨。這些化石也是在恐龍公園組所發現，接近亞伯達省史蒂夫維爾鎮附近。布郎與史萊克比對這兩個發現於同一地點的化石，牠們認為這些標本與戟龍的[正模標本在外表上有顯著的不同](../Page/正模標本.md "wikilink")，因此建立新種，帕克氏戟龍（*S.
parksi*），以[威廉·帕克斯](../Page/威廉·帕克斯.md "wikilink")（William
Parks）為名\[35\]。布朗與史萊克所根據的標本差異包含：[顴骨與亞伯達戟龍有相當差異](../Page/顴骨.md "wikilink")、較小的[尾椎](../Page/尾椎.md "wikilink")。帕克氏戟龍也擁有更結實的頜部、較短的[齒骨](../Page/齒骨.md "wikilink")、頭盾形狀與亞伯達戟龍的不同\[36\]。然而，該頭顱骨大部分是由石膏重建，而1937年的最初研究並沒有敘述實際的頭顱骨樣貌\[37\]。直到保存狀態更好的標本被發現，關於帕克氏戟龍有效性的疑問才重新展開。帕克氏戟龍目前被認為是亞伯達戟龍的[異名](../Page/異名.md "wikilink")\[38\]\[39\]。
[Styracosaurus_body.jpg](https://zh.wikipedia.org/wiki/File:Styracosaurus_body.jpg "fig:Styracosaurus_body.jpg")自然史博物館\]\]
在2006年夏季，[亞伯達省](../Page/亞伯達省.md "wikilink")[得蘭勒赫市](../Page/得蘭勒赫市_\(亞伯達省\).md "wikilink")[泰瑞爾古生物博物館的Darren](../Page/泰瑞爾古生物博物館.md "wikilink")
Tanke重新尋找帕克氏戟龍的發現位置\[40\]。這些由1915年挖掘團隊所發現的大量頭顱骨碎片，是從採石場中發現的。泰瑞爾古生物博物館希望能夠發現更多的化石，以重新敘述頭顱骨，並檢驗亞伯達戟龍與帕克氏戟龍是否為同一個種。他們在[恐龍省立公園發現了數個戟龍部分頭顱骨](../Page/恐龍省立公園.md "wikilink")，以及兩個屍骨層\[41\]。其中一個屍骨層發現了眾多的頭顱骨碎片，例如角鞘、下頜、以及頭盾碎片。\[42\]

戟龍的第三個種，卵圓戟龍（*S.
ovatus*），化石是發現於[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[雙麥迪遜組](../Page/雙麥迪遜組.md "wikilink")，並由[查爾斯·懷特尼·吉爾摩爾](../Page/查爾斯·懷特尼·吉爾摩爾.md "wikilink")（Charles
W.
Gilmore）在1930年所命名。卵圓戟龍的化石材料有限，其中保存最好的是部分[頂骨](../Page/頂骨.md "wikilink")，牠們的特徵是接近中央線的一對尖刺往中央線集中，而非遠離中央線，例如亞伯達戟龍。牠們頭盾兩側分別有兩對尖刺，而非三對。與亞伯達戟龍的尖刺相比，卵圓戟龍的尖刺相當短，期中最長的僅有29.5公分\[43\]。2010年的重新研究認為，卵圓戟龍是個獨立的屬\[44\]。同年，[傑克·霍納](../Page/傑克·霍納.md "wikilink")（John
R. Horner）、Andrew T.
McDonald將卵圓戟龍建立為獨立屬，[刺叢龍](../Page/刺叢龍.md "wikilink")（*Rubeosaurus*）\[45\]。

另外有數個種曾被歸類於戟龍，但目前已經被歸類於其他屬。在1890年，[愛德華·德林克·科普](../Page/愛德華·德林克·科普.md "wikilink")（Edward
Drinker
Cope）將一個[鼻骨](../Page/鼻骨.md "wikilink")、類似戟龍的斷裂鼻角，歸類於[獨角龍的一個種](../Page/獨角龍.md "wikilink")，而後在1915年重新歸類於戟龍，名為*S.
sphenocerus*\[46\]。在1990年，業餘古生物學家[史蒂芬·切爾卡斯](../Page/史蒂芬·切爾卡斯.md "wikilink")（Stephen
Czerkas）與[西爾維婭·柯瑞克斯](../Page/西爾維婭·柯瑞克斯.md "wikilink")（Sylvia
Czerkas）夫婦非正式地敘述了麥氏戟龍（*S.
makeli*），並標明於一個圖解上，後來成為[野牛龍的一個標本](../Page/野牛龍.md "wikilink")\[47\]。北戟龍（"S.
borealis"）則是帕克氏戟龍的早期非正式名稱\[48\]。

## 古生物學

在大眾讀物中，戟龍與其他角龍類常以[群居動物的形象出現](../Page/群居動物.md "wikilink")。在[亞伯達省](../Page/亞伯達省.md "wikilink")[恐龍公園組發現了兩個戟龍的屍骨層](../Page/恐龍公園組.md "wikilink")。這些屍骨層由不同形式的河相沉積層所構成\[49\]；但是，近年的研究只承認其中一個屍骨層，另一個被歸類於[梅杜莎角龍](../Page/梅杜莎角龍.md "wikilink")\[50\]。證據顯示這個環境當時是季節性乾旱或半乾旱環境，所以這些大量死亡的戟龍可能是非群居動物，而在乾旱時期聚集到水坑中\[51\]。

戟龍的相關資訊比牠們的近親[尖角龍還多](../Page/尖角龍.md "wikilink")，顯示戟龍在環境改變的時候取代了尖角龍\[52\]。

[Styracosaurussk.JPG](https://zh.wikipedia.org/wiki/File:Styracosaurussk.JPG "fig:Styracosaurussk.JPG")\]\]
[Styracosaurus.jpg](https://zh.wikipedia.org/wiki/File:Styracosaurus.jpg "fig:Styracosaurus.jpg")

### 齒列與食性

戟龍是[草食性恐龍](../Page/草食性.md "wikilink")；因為牠們的頭部高度，戟龍可能主要以低高度[植被為食](../Page/植被.md "wikilink")。然而，牠們也可能用頭角、喙狀嘴、以及身體，撞倒較高的植物\[53\]\[54\]。戟龍的頜部前端具有長、狹窄的喙狀嘴，被認為較適合抓取、拉扯，而非咬合\[55\]。

[角龍科的牙齒排列成](../Page/角龍科.md "wikilink")[齒系](../Page/齒系.md "wikilink")（Tooth
batteries）。在上方的較老牙齒被下方的年輕牙齒所取代；這個取代方式在動物的一生中不斷地進行。角龍科的齒系是用來切割，而[鴨嘴龍科的齒系是用來磨碎](../Page/鴨嘴龍科.md "wikilink")\[56\]。有些科學家認為角龍科是以[棕櫚科或](../Page/棕櫚科.md "wikilink")[蘇鐵為食](../Page/蘇鐵.md "wikilink")\[57\]，而其他科學家則認為牠們以[蕨類為食](../Page/蕨類.md "wikilink")\[58\]。達德森則假設晚[白堊紀的](../Page/白堊紀.md "wikilink")[角龍類撞倒](../Page/角龍類.md "wikilink")[開花植物](../Page/開花植物.md "wikilink")，並以它們的[樹葉與](../Page/樹葉.md "wikilink")[樹枝為食](../Page/樹枝.md "wikilink")\[59\]。

### 角與頭盾

戟龍的大型鼻角與頭盾，是恐龍之中最特殊的面部裝飾物之一。自從首次被發現有角恐龍之後，牠們的角與頭盾功能長久以來都是爭論的主題之一。

在20世紀早期，[古生物學家](../Page/古生物學家.md "wikilink")[理察·史旺·魯爾](../Page/理察·史旺·魯爾.md "wikilink")（Richard
Swann
Lull）提出一個[假設](../Page/假設.md "wikilink")，他認為[角龍類的頭盾是用來提供頜部肌肉的附著點](../Page/角龍類.md "wikilink")\[60\]。他稍後注意到戟龍頭盾上的尖刺，使牠們看起來較為恐怖\[61\]。在1996年，達德森支持魯爾的肌肉附著點理論，並製作了戟龍與[開角龍頭盾的可能肌肉附著點圖示](../Page/開角龍.md "wikilink")，但他並不贊同頭盾的洞孔充滿了頜部肌肉\[62\]。然而，[凱薩琳·福斯特](../Page/凱薩琳·福斯特.md "wikilink")（C.A.
Forster）則認為沒有證據顯示這些頭盾上有大型肌肉附著點\[63\]。

長久以來，角龍類恐龍被認為使用牠們的角與頭盾來抵抗同時代的大型掠食恐龍。角龍科頭顱骨上的凹洞與其他損傷，常被認為是打鬥所造成的傷害，然而一個2006年的研究則認為沒有證據可以顯示這些傷痕是因為打鬥而留下的，也沒有感染或復原的痕跡。而[骨質流失](../Page/骨質流失.md "wikilink")、或不明的骨頭病理，被認為是這些凹洞與損傷的成因\[64\]。

2009年的一份研究，比較[三角龍](../Page/三角龍.md "wikilink")、[尖角龍的頭顱骨損傷比率](../Page/尖角龍.md "wikilink")，指出三角龍會使用頭角互相打鬥，並使用頭盾作為保護；而尖角龍的頭盾有較少的損傷，顯示牠們較少將頭角、頭盾作為打鬥的武器，可能主要作為視覺展示物的功能，尖角龍也可能用身體作為物種內打鬥的武器，而非頭角、頭盾\[65\]。由於尖角龍是戟龍的近親，兩者都具有長鼻角，戟龍也可能主要將頭角、頭盾用作視覺展示物，而較少用來打鬥、保護。研究人員也指出，三角龍的頭角、頭盾損傷，太過局限性，因此不太可能是骨頭疾病的痕跡\[66\]。

戟龍與其近親的大型頭盾也有可能有助於增加身體的表面積，以利調節體溫\[67\]，如同[大象的耳朵](../Page/大象.md "wikilink")。另一個類似的理論也認為[劍龍的骨板具有體溫調節功能](../Page/劍龍.md "wikilink")\[68\]，但這些理論並沒有考慮[角龍科不同物種的頭盾](../Page/角龍科.md "wikilink")，所擁有的不同變化性\[69\]。

在1961年，L.
Davitashvili首次提出這些頭盾是作為求偶展示物的理論，而這個理論獲得越來越多贊同\[70\]\[71\]。不同種的有角恐龍，擁有不同形狀的裝飾物，這個證據支持了頭盾作為求偶或其他社會行為的視覺辨識物。此外，現代擁有角狀物或裝飾物的動物，也將它們作為視覺辨識物使用\[72\]。

[Styracosaurus_Baltow_20051003_1315.jpg](https://zh.wikipedia.org/wiki/File:Styracosaurus_Baltow_20051003_1315.jpg "fig:Styracosaurus_Baltow_20051003_1315.jpg")Bałtów侏儸紀公園\]\]

## 大眾文化

因為戟龍的角、尖刺與頭盾形狀特殊，使牠們很容易辨認。戟龍曾出現在早期的電影中，例如1933年的《[金剛之子](../Page/金剛之子.md "wikilink")》（*The
Son of Kong*）\[73\]、以及1969年的《[暴龍關吉](../Page/暴龍關吉.md "wikilink")》（*The
Valley of
Gwangi*），在《暴龍關吉》中一隻戟龍與一隻[肉食性恐龍發生打鬥](../Page/肉食性.md "wikilink")\[74\]。在1975年的電影《[被時間遺忘的土地](../Page/被時間遺忘的土地.md "wikilink")》，[德國](../Page/德國.md "wikilink")[U型潛艇裝載兩隻戟龍帶走](../Page/U型潛艇.md "wikilink")。而[迪士尼在](../Page/華特迪士尼影業.md "wikilink")2000年推出的[電腦動畫電影](../Page/電腦動畫.md "wikilink")《恐龍》（*Dinosaur*）中，則有一隻名為「Eema」的擬人化戟龍\[75\]。而在《[侏羅紀公園](../Page/侏羅紀公園_\(小說\).md "wikilink")》的小說中，戟龍則在公園所有恐龍名單之中，但並沒有在電影版中出現。

戟龍也出現在許多電視動畫，例如：《[金剛戰士](../Page/金剛戰士.md "wikilink")》、《[古代王者
恐龍王](../Page/古代王者_恐龍王.md "wikilink")》\[76\]，以及《侏羅紀公園》、《[冰原歷險記](../Page/冰原歷險記.md "wikilink")》的周邊電子遊戲。

## 參考資料

[Category:尖角龍亞科](../Category/尖角龍亞科.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")

1.

2.  Lambert, D. (1993). *The Ultimate Dinosaur Book.* Dorling
    Kindersley: New York, 152–167. ISBN 978-1-56458-304-8.

3.

4.

5.

6.
7.
8.

9.

10.

11.

12.

13.

14.
15.

16.
17.
18.
19.

20.
21. Dodson, P., Forster, C. A, and Sampson, S. D. (2004) *Ceratopsidae*.
    In: Weishampel, D. B., Dodson, P., and Osmólska, H. (eds.), *The
    Dinosauria* (second edition). University of California
    Press:Berkeley, pp. 494–513. ISBN 978-0-520-24209-8.

22.
23. Dodson, P. (1996). *The Horned Dinosaurs: A Natural History*.
    Princeton University Press: Princeton, New Jersey, p. 197–199. ISBN
    978-0-691-02882-8.

24.

25.

26. Forster, C. A. (1990). The cranial morphology and systematics of
    *Triceratops*, with a preliminary analysis of ceratopsian phylogeny.
    Ph.D. Dissertation. University of Pennsylvania, Philadelphia. 227
    pp. OCLC 61500040

27.

28.

29.

30.
31. Dodson, P. (1996). *The Horned Dinosaurs: A Natural History*.
    Princeton University Press: Princeton, New Jersey, p. 244. ISBN
    978-0-691-02882-8.

32. Dodson, P., Forster, C. A, and Sampson, S. D. (2004) *Ceratopsidae*.
    In: Weishampel, D. B., Dodson, P., and Osmólska, H. (eds.), *The
    Dinosauria* (second edition). University of California
    Press:Berkeley, pp. 494–513. ISBN 978-0-520-24209-8.

33.
34.
35.

36.
37.
38.
39.
40.
41.
42.
43.

44.
45. Andrew T. McDonald & John R. Horner, (2010). "New Material of
    "Styracosaurus" ovatus from the Two Medicine Formation of Montana",
    In: Michael J. Ryan, Brenda J. Chinnery-Allgeier, and David A.
    Eberth (eds), *New Perspectives on Horned Dinosaurs: The Royal
    Tyrrell Museum Ceratopsian Symposium*, Indiana University Press, 656
    pp.

46.

47.

48.

49.

50.
51.

52.

53.

54.
55.

56.
57.

58.

59. Dodson, P. (1996). *The Horned Dinosaurs: A Natural History*.
    Princeton University Press: Princeton, New Jersey, p. 266. ISBN
    978-0-691-02882-8.

60.

61.

62. Dodson, P. (1996). *The Horned Dinosaurs: A Natural History*.
    Princeton University Press: Princeton, New Jersey, p. 269. ISBN
    978-0-691-02882-8.

63.
64. Tanke, D. H, and Farke, A. A. (2006). Bone resorption, bone lesions,
    and extracranial fenestrae in ceratopsid dinosaurs: a preliminary
    assessment. in: Carpenter, K. (ed.). *Horns and Beaks: Ceratopsian
    and Ornithopod Dinosaurs* Indiana University Press: Bloomington. pp.
    319–347. ISBN 978-0-253-34817-3.

65.

66.

67.

68.

69.
70.

71.
72.

73.

74.

75.

76.