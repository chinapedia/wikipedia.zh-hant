[Russian_prisoners_tannenberg.jpg](https://zh.wikipedia.org/wiki/File:Russian_prisoners_tannenberg.jpg "fig:Russian_prisoners_tannenberg.jpg")

**1914年坦能堡會戰**，又稱為**坦嫩貝格戰役**，是[第一次世界大戰東部戰綫的一次戰役](../Page/第一次世界大戰.md "wikilink")。這次戰役是在1914年8月26日至8月30日爆發的。

在1914年8月17日，[俄羅斯第一和第二集团军侵入](../Page/俄羅斯.md "wikilink")[東普魯士](../Page/東普魯士.md "wikilink")，向首府[哥尼斯堡進發](../Page/哥尼斯堡.md "wikilink")。俄軍成功地進入[德國](../Page/德國.md "wikilink")，直到德國第八集团军在8月20日反擊。德軍設計了一個陷阱，讓由南向上的俄國第二集团军提前進入德國，隨後德軍从该集团军两翼实施钳形攻势并将其主力围歼，俄第二集团军司令员萨姆索诺夫兵败自杀。在9月2日，俄軍放棄了整個任務。俄軍從坦能堡會戰後，沒有再向德國領土進攻。

雖然德軍在坦能堡會戰得勝，但德軍原先並沒有料到俄羅斯會在8月中開戰，因此德軍用了兩支部隊來抵抗俄軍，造成德軍的資源被分散，而影響在西線對英法聯軍的戰事。

## 延伸閱讀

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - [Hastings, Max](../Page/Max_Hastings.md "wikilink")(2013),
    Catastrophe 1914: Europe Goes to War. London, Knopf Press, Release
    Date 24 September 2013,

  - [Martin van
    Creveld](../Page/Martin_van_Creveld.md "wikilink")(2004), Supplying
    War: Logistics from Wallenstein to Patton,Cambridge University
    Press,

## 參見

  - [坦能堡防線戰役](../Page/坦能堡防線戰役.md "wikilink")

## 外部連結

[Category:1914年一战战役](../Category/1914年一战战役.md "wikilink")
[Category:俄罗斯帝国战役](../Category/俄罗斯帝国战役.md "wikilink")
[Category:第一次世界大戰德國參與戰役](../Category/第一次世界大戰德國參與戰役.md "wikilink")
[Category:波兰战役](../Category/波兰战役.md "wikilink")
[Category:1914年8月](../Category/1914年8月.md "wikilink")