**印尼華人國籍問題**，泛指在[印尼獨立後](../Page/印尼.md "wikilink")，由於[排華政策](../Page/排華.md "wikilink")，以及[中华人民共和国国籍法的改變](../Page/中华人民共和国国籍法.md "wikilink")，造成的[國籍不明現象](../Page/國籍.md "wikilink")，這現象令各地[印尼華人](../Page/印尼華人.md "wikilink")，都面臨各種不同的法律問題。

## 海外華人國籍認定

### 中華民國

根據《[中華民國國籍法](../Page/中华民国国民.md "wikilink")》，其出生時父或母為中華民國國民或出生於父或母死亡後，其父或母死亡時為中華民國國民者，皆屬中華民國籍。

### 中華人民共和國

《[中华人民共和国国籍法](../Page/中华人民共和国国籍法.md "wikilink")》第五条规定，父母双方或一方为中国公民，本人出生在外国，具有中国国籍；但父母双方或一方为中国公民并定居在外国，本人出生时即具有外国国籍的，不具有中国国籍。\[1\]

## 歷史

### 印尼獨立前

[清朝時代](../Page/清朝.md "wikilink")，只要是來自中國的移民者及其後裔，都會被認定為[大清國的子民](../Page/大清國.md "wikilink")。[中華民國成立後](../Page/中華民國.md "wikilink")，中華民國政府繼承以屬人主義為主的國籍認定法律，並以此制定《[中華民國國籍法](../Page/中華民國國民.md "wikilink")》。直至今天，許多印尼華人仍然被界定為[中華民國籍的僑民](../Page/中華民國.md "wikilink")\[2\]。

但與此同時，統治印尼的[荷蘭王國](../Page/荷蘭王國.md "wikilink")，採用[出生地主義國籍法](../Page/出生地主義.md "wikilink")，任何在[印度尼西亞出生的人](../Page/印度尼西亞.md "wikilink")，都會被認定為荷蘭臣民（），而[清政府與荷蘭政府之間](../Page/清政府.md "wikilink")，曾就這問題所引發的領事保護權問題發生糾紛。印尼獨立前出生的華人，有權同時申領[中華民國護照](../Page/中華民國護照.md "wikilink")，以及荷蘭發出的護照。

### 印尼獨立後

#### 中華人民共和國与印尼的雙重國籍協定

由於《[中华民国国籍法](../Page/s:中華民國國籍法.md "wikilink")》遺留的問題，令印尼華人在印尼獨立後，身份非常尷尬。當時中華人民共和國政府，為了爭取印尼的支持，在1955年[萬隆會議後](../Page/萬隆會議.md "wikilink")，於印尼[萬隆簽署](../Page/萬隆.md "wikilink")《中華人民共和國和印度尼西亞共和國關於雙重國籍問題的條約》，容許持有雙重國籍的華人，在條約簽署後20年內，成年時選擇印尼國籍或中國國籍\[3\]。由於中國當局的鼓勵，不少印尼華人的子女，都選擇了印尼國籍。

1965年[印尼排華後](../Page/印尼九三零事件.md "wikilink")，印尼單方面終止有關條約，令印尼華人國籍問題懸而不決，直至中國與印尼復交的聯合聲明中，作有限度的處理。

#### 《1958年第62號政令》

#### 1981年版的《中華人民共和國國籍法》

#### 2006年的新印尼國籍法

2006年7月11日，印尼國會通過新的國籍法，《1958年第62號政令》徹底廢除，不再具有法律效力，華裔終享國民待遇\[4\]。《2006年第12號政令》修改了印尼[原住民定義](../Page/原住民.md "wikilink")。根據新定義，任何人在印尼出生，並未有自願地接受其他國家國籍，都會成為印尼原住民，並且以原住民身份獲得印尼國籍。而新國籍法亦以1945年憲法作為基礎，而不是以1950年的臨時憲法作基礎。

由於新國籍法的定義，除了廢除華人在印尼被歧視的地位，而且持有印尼出生證明，而未有接受第三國國籍的印尼華人，都會重獲印尼國籍，印尼華人國籍問題，在印尼本土地區，大致告一段落。而新的印尼國籍法，亦徹底取代了歧視華人的《1958年第62號政令》，自此以後，印尼國籍證（）的法定地位，亦正式廢除\[5\]。但有消息指，一些政府官员并不了解新法律的条款，一些印尼华裔仍受歧视性的待遇\[6\]。

## 註釋

[Category:印尼华人历史](../Category/印尼华人历史.md "wikilink")
[Category:中華人民共和國-印度尼西亞關係](../Category/中華人民共和國-印度尼西亞關係.md "wikilink")
[Category:中華民國-印度尼西亞關係](../Category/中華民國-印度尼西亞關係.md "wikilink")

1.  <http://www.gov.cn/banshi/2005-05/25/content_843.htm>
2.  [中華民國國籍法，台北：內政部，2005年](http://www.immigration.gov.tw/aspcode/LawList1.asp?NodeID=87)
3.  [中華人民共和國和印度尼西亞共和國關於雙重國籍問題的條約，北京：全國人民代表大會，1957年](http://www.npc.gov.cn/zgrdw/common/zw.jsp?label=WXZLK&id=688&pdmc=rdgb)
4.  [印尼新國籍法取消歧視條款
    華裔終享國民待遇，香港：星島環球網，2006年8月11日](http://www.singtaonet.com:82/euro_asia/t20060811_298362.html)
5.  [印尼將頒行新國籍法
    華裔不再受歧視，華盛頓：大紀元時報，引用中央社報導，2006年8月7日](http://tw.epochtimes.com/bt/6/7/8/n1378565.htm)
6.  [《印尼舆论视窗》节目文字稿，新加坡国际广播电台，2008年2月12日](http://www.rsi.sg/chinese/indonesiamediawatch_c/view/20080212211500/1/gb/.html)