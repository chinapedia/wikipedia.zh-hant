[Unua_libro_per_russi_-_1887_-_1a_edizione_-_copertina_fronte.jpg](https://zh.wikipedia.org/wiki/File:Unua_libro_per_russi_-_1887_-_1a_edizione_-_copertina_fronte.jpg "fig:Unua_libro_per_russi_-_1887_-_1a_edizione_-_copertina_fronte.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Unua_libro_per_russi_-_1887_-_1a_edizione_-_frontespizio.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Unua_libro_per_russi_-_1887_-_1a_edizione_-_introduzione.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Unua_libro_per_russi_-_1887_-_1a_edizione_-_pagina_23.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Unua_libro_per_russi_-_1887_-_1a_edizione_-_dizionario.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Unua_libro_per_russi_-_1887_-_1a_edizione_-_copertina_retro.jpg "fig:缩略图")
**Unua Libro**（《**第一本書**》）是國際語（*Lingvo
Internacia*，[世界語的原名](../Page/世界語.md "wikilink")）的基礎教科書。此書是由[柴門霍夫以](../Page/柴門霍夫.md "wikilink")「希望者醫師」（*Doktoro
Esperanto*）\[1\]的筆名在1887年7月26日出版的。

## 印刷語言

此書最初是以[俄語印刷的](../Page/俄語.md "wikilink")，而在1887年末時被翻譯為[波蘭語](../Page/波蘭語.md "wikilink")、[德語](../Page/德語.md "wikilink")、[法語及](../Page/法語.md "wikilink")[英語](../Page/英語.md "wikilink")。在1888年末，《第二本書》及《第二本書的附錄》印刷完成。在1889年，《第一本書》被翻譯為[希伯來語](../Page/希伯來語.md "wikilink")。

## 內容

此書有40頁，內含有：

  - 前言（28頁），內有第一份世界語的文本：
      - Patro Nia（「我們之父」，即[主禱文](../Page/主禱文.md "wikilink")）
      - 聖經
      - 信件範本
      - 「我的想法」（詩）
      - [海因里希·海涅作品的翻譯](../Page/海因里希·海涅.md "wikilink")
      - 「啊，我的心」（詩）

## 參考文獻

## 外部連結

[Category:世界语](../Category/世界语.md "wikilink")

1.  這個筆名後來成為這種語言的名稱。