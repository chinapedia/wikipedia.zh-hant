**祈愿行**（**PledgeBank**）是一个[英国](../Page/英国.md "wikilink")[网站](../Page/网站.md "wikilink")，它支持用户针对任意主题提出自己的愿望。愿望采用这样的形式：“如果有y个人同意同样去做，我将去做x这件事情”网站于2005年6月13日上线，由[mySociety创建](../Page/mySociety.md "wikilink")。

在PledgeBank.com上由愿望发起或者直接建立起来的著名项目，包括为[英国开放权利集团](../Page/英国开放权利集团.md "wikilink")（UK
Open Rights
Group）发起的[种子基金](../Page/种子基金.md "wikilink")，以及为“[NO2ID运动](../Page/NO2ID运动.md "wikilink")”宣传和合法募集基金的计划。

另外一些有创意的慈善筹款活动也在实施。例如，一个愿望寻求对[世界纸牌系列赛的财务支持](../Page/世界纸牌系列赛.md "wikilink")，条件是一定比例的获胜奖金将捐助给一个[非赢利的](../Page/非赢利.md "wikilink")[慈善团体](../Page/慈善团体.md "wikilink")。

此外，它还被用于支持全球性的[网络抗议活动](../Page/网络抗议.md "wikilink")，最近的一起，由[自由文化组织](../Page/自由文化.md "wikilink")（Freeculture.org）发起，抵制带[数字版权管理](../Page/数字版权管理.md "wikilink")（DRM）的[CD销售](../Page/CD.md "wikilink")。

[英国首相](../Page/英国首相.md "wikilink")[托尼·布莱尔最近启动了一个自己的愿望](../Page/托尼·布莱尔.md "wikilink")，如果有100位著名人士同样参与，他将成为一个社区体育俱乐部的资助人。

成功实现愿望的范例包括目前由[格雷汉姆·伍兹发起的](../Page/格雷汉姆·伍兹.md "wikilink")“[拒绝媒体](../Page/拒绝媒体.md "wikilink")”（NOMEDIA）运动，他祈愿，只要有另外10个人参与，他将完全抵制任何形式的[媒体两周时间](../Page/媒体.md "wikilink")。成功的范例还有由达伦·格鲁夫发起的为非洲捐助课本项目。

## 外部链接

  - <http://www.mySociety.org>
  - <https://web.archive.org/web/20141201002643/http://www.pledgebank.com/>

[Category:英国网站](../Category/英国网站.md "wikilink")