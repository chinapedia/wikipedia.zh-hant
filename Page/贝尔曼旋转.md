[Kim_2008SAgala2_by_Carmichael.jpg](https://zh.wikipedia.org/wiki/File:Kim_2008SAgala2_by_Carmichael.jpg "fig:Kim_2008SAgala2_by_Carmichael.jpg")
**贝尔曼旋转**（Biellmann
spin）是[花样滑冰运动中的一种旋转动作](../Page/花样滑冰.md "wikilink")。完成这个旋转时，运动员单腿站立，另一条腿从背后弯起至头顶，双手从身前升起抓住弯起到头顶的脚，整个身体形成一个水滴的形状。

贝尔曼旋转要求运动员有非常好的柔韧性和旋转能力，因此通常只有女运动员可以完成。[俄罗斯运动员](../Page/俄罗斯.md "wikilink")[普鲁申科](../Page/普鲁申科.md "wikilink")，[日本運動員](../Page/日本.md "wikilink")[羽生結弦是世界上少有的几个可以完成贝尔曼旋转的男运动员之一](../Page/羽生結弦.md "wikilink")。

## 歷史

[塞西莉亚·考利奇](../Page/塞西莉亚·考利奇.md "wikilink")（Cecilia
Colledge）可能在1937年[世界花样滑冰錦標賽就曾演出一個後仰贝尔曼旋转](../Page/世界花样滑冰錦標賽.md "wikilink")。最早完成贝尔曼旋转的溜冰者是[塔玛拉·莫斯科维娜](../Page/塔玛拉·莫斯科维娜.md "wikilink")（Tamara
Moskvina）。她看[體操比賽後受到啟發](../Page/體操.md "wikilink")，在1960年[歐洲花样滑冰錦標賽中演出](../Page/歐洲花样滑冰錦標賽.md "wikilink")\[1\]。她也在1965年歐洲花样滑冰錦標賽中演出。其他早期的包括珍妮特、Slavka
Kohout、Karin
Iten\[2\]。在20世紀70年代末，瑞士選手[丹尼絲·貝爾曼使這種動作普及全球](../Page/丹尼絲·貝爾曼.md "wikilink")。她是第一個以這種動作贏得了世界花样滑冰錦標賽的選手。丹尼絲·貝爾曼在雜技學校學習到這項技術。

在1990年代初，塔利婭Mishkutenok和[阿圖爾·德米特里耶夫將贝尔曼旋转納入](../Page/阿圖爾·德米特里耶夫.md "wikilink")[死亡螺旋中](../Page/死亡螺旋.md "wikilink")。

[伊琳娜·斯盧茨卡婭是首位進行換腿贝尔曼旋转的運動員](../Page/伊琳娜·斯盧茨卡婭.md "wikilink")\[3\]。

## 參考

## 導航

[Category:花样滑冰的技术动作](../Category/花样滑冰的技术动作.md "wikilink")

1.
2.  [Videoportal](http://www.srf.ch/player/tv/sportaktuell/video/credit-suisse-sports-awards-serie-teil-2?id=9a90f583-ec43-49b8-89ec-94197064e682)
3.