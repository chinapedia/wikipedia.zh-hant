**上加丹加省**（[法語](../Page/法語.md "wikilink")：****）是位于[刚果民主共和国南部的一个省](../Page/刚果民主共和国.md "wikilink")，首府[卢本巴希](../Page/卢本巴希.md "wikilink")（Lubumbashi），人口？（1998年），面積132,425
km²。

## 上加丹加省的镇

  - [卢本巴希](../Page/卢本巴希.md "wikilink")（Lubumbashi）
  - [利卡西](../Page/利卡西.md "wikilink")（Likasi）

## 参见

  - [姆韋魯湖](../Page/姆韋魯湖.md "wikilink")（Lac Moero）

[Category:刚果民主共和国行政区划](../Category/刚果民主共和国行政区划.md "wikilink")