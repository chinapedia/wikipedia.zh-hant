**哈特縣**（**Hart County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州中部的一個縣](../Page/肯塔基州.md "wikilink")。面積1,082平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口17,445人。縣治[曼福德維爾](../Page/曼福德維爾.md "wikilink")（Munfordville）。

成立於1819年1月28日。縣名紀念在[1812年戰爭中陣亡的](../Page/1812年戰爭.md "wikilink")[納瑟尼爾·G·S·哈特](../Page/納瑟尼爾·G·S·哈特.md "wikilink")
（Nathaniel G. S. Hart）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[H](../Category/肯塔基州行政区划.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.