**伊灵**（**Ealing**）是[英格兰](../Page/英格兰.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")[伊灵区的中心地区](../Page/伊灵区.md "wikilink")。它在[查令十字以西](../Page/查令十字.md "wikilink")12.9公里，距[伦敦市约](../Page/伦敦市.md "wikilink")19.3公里，是[伦敦计划中定义的主要都市中心之一](../Page/伦敦计划.md "wikilink")。\[1\]历史上它是[米德尔塞克斯郡的一个乡村](../Page/米德尔塞克斯郡.md "wikilink")，组成一个古牧区。\[2\]

## 伊灵片场

位于伊灵区的一个电影制片厂，在20世纪五十年代出品了很多著名的喜剧电影，也称作[伊灵喜剧](../Page/伊灵喜剧.md "wikilink")。著名的有《[仁心与冠冕](../Page/仁心与冠冕.md "wikilink")》(Kind
Hearts and Coronets)、《买路钱》(Passport to Pimlico)、《师奶杀手》（Lady Killers -
后由[汤姆·汉克斯翻拍](../Page/汤姆·汉克斯.md "wikilink"))等。

## 参考资料

[Category:倫敦地區](../Category/倫敦地區.md "wikilink")

1.
2.  {{ cite book | first=Frederic |last=Youngs | title=Guide to the
    Local Administrative Units of England | volume=I: Southern England |
    year=1979 | publisher=[Royal Historical
    Society](../Page/Royal_Historical_Society.md "wikilink") |
    location=London | isbn=0901050679}}