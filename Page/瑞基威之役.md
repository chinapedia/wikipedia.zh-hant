**瑞基威之役**（）於1866年6月2日爆發，當時一些從[美國出發至](../Page/美國.md "wikilink")[安大略省附近的](../Page/安大略省.md "wikilink")[瑞基威](../Page/瑞基威.md "wikilink")，與當地的[加拿大士兵發生衝突](../Page/加拿大.md "wikilink")。這亦是所謂的“芬尼安暴動”。最後很多芬尼安人不敵而降。

## 参考资料

[Category:1866年](../Category/1866年.md "wikilink")
[Category:安大略省歷史](../Category/安大略省歷史.md "wikilink")
[Category:加拿大軍事史](../Category/加拿大軍事史.md "wikilink")
[Category:加拿大戰役](../Category/加拿大戰役.md "wikilink")
[Category:芬尼兄弟會](../Category/芬尼兄弟會.md "wikilink")
[Category:加拿大军事冲突](../Category/加拿大军事冲突.md "wikilink")