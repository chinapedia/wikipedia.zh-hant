**稻垣吾郎**（），[日本](../Page/日本.md "wikilink")[男演員及](../Page/男演員.md "wikilink")[男歌手](../Page/男歌手.md "wikilink")，已解散的日本偶像組合[SMAP的前成員](../Page/SMAP.md "wikilink")。與[木村拓哉](../Page/木村拓哉.md "wikilink")、[香取慎吾同日進](../Page/香取慎吾.md "wikilink")[傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")。因组合SMAP于2016年12月31日解散而以个人身份进行活动。\[1\]
2017年9月8日正式与杰尼斯事务所解约。\[2\] 2017年9月22日，宣佈與新經紀公司CULEN
Inc.簽約，并開設個人全新官網。\[3\]

## 重要紀事

  - 2001年8月24日 ※吾郎被逮捕事件（違規停車拒絕盤查，欲駛離時撞傷女警。）
    1.  東京都[澀谷區內](../Page/澀谷區.md "wikilink")，被依妨害公務、違反道路交通、傷害罪，以現行犯逮捕。
    2.  三天後，開記者會向大眾道歉。
    3.  同年9月21日不起訴處分。
    4.  被[傑尼斯事務所懲罰在家自我反省共](../Page/傑尼斯事務所.md "wikilink")143天。
    5.  SMAP夏季演唱會後期變成四人開演。
    6.  [SMAPXSMAP節目暫停](../Page/SMAPXSMAP.md "wikilink")2個月演出。
  - 2002年1月14日 ※[SMAPXSMAP節目中宣佈復出](../Page/SMAPXSMAP.md "wikilink")
  - 2006年9月14日
    ※吾郎公佈首次身為[朝日世界杯水上芭蕾應援團團長](../Page/朝日世界杯水上芭蕾.md "wikilink")
    1.  首次被朝日封為『[人魚王子](../Page/人魚王子.md "wikilink")』，帶領觀眾呈現美的現場感。
    2.  以「敏感」和「柔軟度」進行為期共三天的美的賽事。
    3.  比賽場地為「橫濱國際泳池」。
  - 2016年1月13日，日刊體育報導，出道25年的SMAP可能將會解散，除了木村拓哉續留傑尼斯事務所，其他4位成員中居正廣、稻垣吾郎、草彅剛、香取慎吾將隨女性主管離開，惟此事事務所方面並未證實\[4\]。
  - 2016年1月18日，SMAP全員於節目《SMAP×SMAP》以現場直播方式公開鞠躬道歉，表示對於造成騷動感到很抱歉，並感謝粉絲的支持，稱今後也請多多指教。\[5\]
  - 2016年12月31日，SMAP正式解散。
  - 2017年9月9日，稲垣吾郎、香取慎吾、草彅剛於尊尼事務所退所。同年9月22日稲垣、香取、草彅三人共同開設官方粉絲網站，取名為「全新地圖(日文為:新しい地図)」，網站上刊登3人的訊息以及加入粉絲俱樂部的方法等。10月16日正式啟用，同時宣佈三人加入新事務所「CULEN」。
  - 2017年11月，透過網路電視台 Abema TV，於11月2日晚間9時至11月5日晚間9時進行長達 72
    小時的直播節目――《72小時真心話電視》。

## 相關作品

### 戲劇

大多數屬於個人主演為主、SMAP共演與個人客串為副

#### 連續劇

  - 1988年4月 NHK 『[青春家族](../Page/青春家族.md "wikilink")』錄製 【吾郎人生第一部電視劇】
    ※與[中居正廣共演](../Page/中居正廣.md "wikilink")
  - 1991年4月8日 FUJI『[校園威鳳](../Page/校園威鳳.md "wikilink")』(學校へ行こう)
    ※與[中居正廣共演](../Page/中居正廣.md "wikilink")
  - 1992年10月12日 FUJI『[逆火青春](../Page/逆火青春.md "wikilink")』(二十歲の約束)
  - 1993年10月23日 NTV『[愛無謊言](../Page/愛無謊言.md "wikilink")』(噓でもいいから)
  - 1994年10月10日 朝日『[東京大學物語](../Page/東京大學物語.md "wikilink")』
    ※[瀨戶朝香共演](../Page/瀨戶朝香.md "wikilink")
  - 1995年4月17日 朝日『[理想愛人](../Page/理想愛人.md "wikilink")』(最高の戀人)
    ※[高橋由美子共演](../Page/高橋由美子.md "wikilink")
  - 1997年1月3日 朝日『[理想與友情](../Page/理想與友情.md "wikilink")』(僕が僕であるために)
    ※[SMAP全員共演](../Page/SMAP.md "wikilink")
  - 1997年1月7日 KTV『[美髮貴公子](../Page/美髮貴公子.md "wikilink")』(彼KARE)
    ※[野際陽子共演](../Page/野際陽子.md "wikilink")
  - 1997年10月15日
    NTV『[愛情的單程車票](../Page/愛情的單程車票.md "wikilink")』(戀の片道切符)／森慎一
    ※[江角真紀子共演](../Page/江角真紀子.md "wikilink")
  - 1998年10月13日 KTV『[美酒貴公子](../Page/美酒貴公子.md "wikilink")』(主演)
    ※[菅野美穗共演](../Page/菅野美穗.md "wikilink")
  - 1999年10月14日 FUJI『[危險關係](../Page/危險關係.md "wikilink")』
    ※[藤原紀香](../Page/藤原紀香.md "wikilink")、[豐川悅司共演](../Page/豐川悅司.md "wikilink")
  - 2000年7月9日 TBS『[催眠](../Page/催眠.md "wikilink")』(主演)／嵯峨敏也
    ※[瀨戶朝香共演](../Page/瀨戶朝香.md "wikilink")
  - 2001年4月3日 NHK『[陰陽師](../Page/陰陽師.md "wikilink")』(陰陽師おんみょうじ)
    (主演)／[安倍晴明](../Page/安倍晴明.md "wikilink")
  - 2002年4月14日 TBS『[馬屁精之男](../Page/馬屁精之男.md "wikilink")』(ヨイショの男)
    (主演)／櫻井孝太郎
  - 2002年7月4日 FUJI『[戀愛偏差值](../Page/戀愛偏差值.md "wikilink")』單元2 PARTY／夏目勇作
  - 2005年1月16日 TBS『[Ｍの悲劇](../Page/Ｍの悲劇.md "wikilink")』(主演)／安藤衛
  - 2006年4月11日 FUJI
    火曜日10時『[愛上醜女的眼睛](../Page/愛上醜女的眼睛.md "wikilink")』(ブスの瞳に恋してる)(主演)／山口おさむ
  - 2007年8月28日 FUJI
    火曜日9時『[花樣少年少女](../Page/花樣少年少女_\(2007年電視劇\).md "wikilink")』(花ざかりの君たちへ)
    客串第九集／北濱昇
  - 2008年1月20日 TBS 『[佐佐木之战](../Page/佐佐木之战.md "wikilink")』(佐々木夫妻の仁義なき戦い)
    (主演)／佐佐木法伦
  - 2009年1月6日 KTV 『[三角效應](../Page/三角效應.md "wikilink")』(トライアングル)／黑木舜
  - 2009年9月5日 TBS
    『[烏龍派出所](../Page/烏龍派出所.md "wikilink")』(こちら葛飾区亀有公園前派出所)／白鳥麗次
  - 2010年10月18日 FUJI『[流星
    (電視劇)](../Page/流星_\(電視劇\).md "wikilink")』(流れ星)／槇原修一
  - 2011年7月6日 NTV 『[Bull Doctor](../Page/Bull_Doctor.md "wikilink")』／名倉
    潤之助
  - 2012年1月10日 KTV
    『[Hungry！](../Page/Hungry！.md "wikilink")』(ハングリー\!)／麻生時男
  - 2013年4月19日 TBS 『[TAKE
    FIVE～我們能盜取愛嗎～](../Page/TAKE_FIVE～我們能盜取愛嗎～.md "wikilink")』（TAKE
    FIVE〜俺たちは愛を盗めるか〜）／岩月 櫂
  - 2013年 EX 『[信長的主廚](../Page/信長的主廚.md "wikilink")』／明智光秀
  - 2014年 EX 『[信長的主廚2](../Page/信長的主廚2.md "wikilink")』／明智光秀
  - 2014年1月14日 FUJI
    火曜夜9時『[福家警部補的問候](../Page/福家警部補的問候.md "wikilink")』（福家警部補の挨拶）／石松和夫
  - 2016年4月29日 朝日 『[不愉快的果實](../Page/不愉快的果實.md "wikilink")』（不機嫌な果実）／水越 航一

#### 單元劇

  - 1992年2月27日 大人は判ってくれない～1992年のバタフライ～
  - 1995年9月29日 さっちゃんウソついてごめんね
  - 1995年10月19日 木曜の怪談～午前０時の血
  - 1996年1月3日 素晴らしき家族旅行
  - 1996年3月20日 恋愛前夜いちどだけ～ファーストキス～
  - 1996年4月8日 愛が叫んでる 幸福を捜す女
  - 1996年10月4日 最後的家族旅行
  - 1997年12月30日 大捜査線歳末警戒篇
  - 1998年3月7日 名偵探明智小五郎～江戸川亂步之陰獸
  - 1998年9月25日 世界奇妙物語～教師
  - 1998年10月31日 さよなら五つのカプチーノ
  - 1998年11月14日 心室細動
  - 1999年1月3日 古畑任三郎v.s.ＳＭＡＰ
  - 1999年8月27日 真實的恐怖故事～白天的鈴聲
  - 2000年2月26日 名偵探明智小五郎～エレベーター密室殺人
  - 2001年1月1日 世界奇妙物語ＳＭＡＰ特別編～我要去旅行
  - 2002年3月3日 結婚的條件
  - 2003年3月24日 世界奇妙物語～被盜走的臉
  - 2004年3月26日 向星星許願
  - 2004年4月3日 金田一耕助～犬神家一族
  - 2004年9月11日 911 ※和久井映見、石黑賢主演，吾郎客串
  - 2004年10月1日 金田一耕助～八墓村
  - 2004年12月25日 Xsmap 老虎、獅子與五個男人
  - 2005年10月10日 給飛鳥和未見面的孩子 ※平成17年文化廳藝術祭參展作品
  - 2006年1月6日 金田一耕助～女王蜂
  - 2006年12月26日 ブスの瞳に戀してる(愛上醜女的眼睛)之聖誕節特別篇
  - 2007年1月5日 金田一耕助～惡魔前來吹笛
  - 2009{{\#switch:}}年1月5日 金田一耕助～惡魔的手毬歌
  - 2009年1月31日 假面騎士G／吾郎

#### 類戲劇

  - 吾郎館主和小學生共同主持富士電視台《[真實的恐怖故事](../Page/真實的恐怖故事.md "wikilink")》簡稱『[真恐](../Page/真恐.md "wikilink")』

每集從不同角度看鬼故事，請大師解析心靈寫真照。

  - 緯來日本台譯名【毛骨悚然撞鬼經驗】自2004年起每年夏季中國農曆年鬼月播出。

※此劇獲得2004亞洲電視大賞最佳實況劇。

\*\# 2004年1月10日到3月20日{I}

\*\# 2004年9月7日 夏季特別篇

\*\# 2004年10月11日到2005年3月7日(Ⅱ)

\*\# 2005年8月23日 京都特別篇2小時

\*\# 2006年8月22日 鎌倉特別篇2小時

\*\# 2007年8月28日 夏季特別篇2小時

#### 電視電影

SMAP共同演出,輪流當主角

\*\# 心之鏡(吾郎主演)

\*\# 藍天五人組 (木村&香取主演) 六人時期作品

\*\# 足球風雲(中居主演)

\*\# 理想與友情

\*\# X'SMAP

### 舞台劇

  - 1991年 聖闘士星矢 ※smap主演
  - 1992年 ドラゴンクエスト(Dragon QUEST)
  - 1993年 ANOTHER-少年の島編-
  - 1996年 夜曲-tsutomu-
  - 1997年 広島に原爆を落とす日
  - 1998年 広島に原爆を落とす日・再演
  - 1999年 月晶島綺譚
  - 2000年 七色鸚哥
  - 2003年 謎の下宿人～サンセット・アパート(Sun set apartment)～
  - 2006年 ヴァージニア・ウルフなんかこわくない？(Who's Afraid of Virginia Woolf?)
  - 2007年 魔法の万年筆
  - 2010年 象
  - 2011年 ぼっちゃま、泣き虫なまいき石川啄木
  - 2012年 恋と音楽
  - 2013年 ヴィーナス・イン・ファー (VENUS IN FUR)
  - 2014年 恋と音楽II 〜僕と彼女はマネージャー〜
  - 2015年 No.9 -不滅の旋律
  - 2016年 恋と音楽FINAL 〜時間劇場の奇跡〜
  - 2018年 FREE TIME, SHOW TIME『君の輝く夜に』
  - 2018年 No.9 -不滅の旋律〈再演〉
  - 2019年 LIFE LIFE LIFE～人生の3つのヴァージョン～

### 電影

  - 1990年11月23日 さらば愛しのやくざ (Good-bye dearly ruffian)
  - 1993年3月6日 プライベート・レッスン Private Lesson ※中居正廣共演
  - 1994年3月12日 シュート 足球風雲 shoot/馬堀圭吾 ※SMAP6人共演
  - 1996年8月26日 スーパースキャンダル Super Scandal ※藤谷美和子共演
  - 1997年2月1日 パラサイト・イヴ 寄生體夏娃/大野達郎(客串) ※葉月里緒菜共演
  - 1999年6月5日
    [催眠](../Page/催眠.md "wikilink")、([東寶](../Page/東寶.md "wikilink"))/[嵯峨敏也](../Page/嵯峨敏也.md "wikilink")
    ※[菅野美穗](../Page/菅野美穗.md "wikilink")(主演)
  - 2004年10月30日 笑的大學 ※役所廣司共演
  - 2010年9月25日 [十三刺客](../Page/十三刺客_\(2010年電影\).md "wikilink")
    ※[松平齊韶](../Page/松平齊韶.md "wikilink") 役
  - 2013年4月6日 [櫻花下的約定](../Page/櫻花下的約定.md "wikilink") ※廣末涼子共演
  - 2013年10月12日 [阿信](../Page/阿信.md "wikilink")
  - 2016年10月8日 [少女](../Page/少女.md "wikilink")
  - 2019年2月15日 半世界

## 音樂作品

### 單曲

#### SMAP

請參看[SMAP的音樂條目](../Page/SMAP.md "wikilink")。

#### 個人單曲作品

  - 1993年2月3日 「[IF You Give Your
    Heart](../Page/IF_You_Give_Your_Heart.md "wikilink")」

以本名發行個人首張單曲，為美國電影『[Private
Lesson](../Page/Private_Lesson.md "wikilink")』主題曲。

  - 2004年3月10日 「[Wonderful Life](../Page/Wonderful_Life.md "wikilink")」

以化名**\&G**發行的第2張單曲，為[草彅剛主演](../Page/草彅剛.md "wikilink")『[我和她和她的生存之道](../Page/我和她和她的生存之道.md "wikilink")』主題曲。

#### 專輯

以下只記載他個人於專輯內的獨唱歌曲

  - SMAP 004：September Rain (九月雨)
  - SMAP 006 \~SEXY SIX\~： (更想知道妳)
  - SMAP 009： (在夏天的某一天)
  - SMAP 011 \~ス\~： (自由的情感)
  - La Festa：Beautiful (美麗)
  - SMAP 015 \~Drink\! Smap\!\~： (停止吧 時間)
  - SMAP 016 \~MIJ\~：Thousand Nights (一千零一夜)
  - SAMPLE BANG\!：To Be Continued
  - Pop Up\!SMAP：L-O-V-E
  - super.modern.artistic.performance：Life Walker
  - We are SMAP\!：愛や恋や
  - Gift of SMAP：Special Thanks
  - Mr.S：Dramatic Starlight

## 影像作品

請參看[SMAP的影像作品條目](../Page/SMAP.md "wikilink")。

## 節目主持

### SMAP

  - [富士電視台](../Page/富士電視台.md "wikilink")“『[SMAPXSMAP](../Page/SMAPXSMAP.md "wikilink")』SMAP下廚料理
    短劇 歌手專訪演唱” 每週一 PM 22:00播出 (1996年4月15日\~2016年12月26日)
  - [TOKYO FM](../Page/TOKYO_FM.md "wikilink")、JFN
    [おはようSMAP](../Page/おはようSMAP.md "wikilink")（1994年4月1日 -
    2016年12月30日）
  - [文化放送](../Page/文化放送.md "wikilink") [STOP THE
    SMAP](../Page/STOP_THE_SMAP.md "wikilink")（1997年10月7日 \~ 2012年3月）

### 個人

  - [富士電視台](../Page/富士電視台.md "wikilink")
    “『[忘文](../Page/忘文.md "wikilink")』 替人唸信傳遞心意” 每週日 AM5:45播出
  - [朝日電視台](../Page/朝日電視台.md "wikilink") “SMASTATION
    單元主持『[月一吾郎影評](../Page/月一吾郎.md "wikilink")』” 週六
    PM23:00播出(每月最後一週)
  - [TBS](../Page/TBS.md "wikilink") “『[GORO'S
    BAR](../Page/GORO'S_BAR.md "wikilink")』 以酒吧老闆身份面試女嘉賓 ” 每週四 PM23:55播出
  - [TBS](../Page/TBS.md "wikilink")
    “『[哀愁探偵1756](../Page/哀愁探偵1756.md "wikilink")』
    每週四 PM0:55播出
  - [文化放送](../Page/文化放送.md "wikilink") [稲垣吾郎のSTOP THE
    SMAP](../Page/稲垣吾郎のSTOP_THE_SMAP.md "wikilink")（2012年4月 \~
    2016年12月29日）
  - [TBS](../Page/TBS.md "wikilink") “『』每週四 PM0:41播出
  - [文化放送](../Page/文化放送.md "wikilink") [編集長
    稲垣吾郎](../Page/編集長_稲垣吾郎.md "wikilink")
    (2017年1月5日 \~)

## 配音

  - 2006年3月4日上映『[ONE PIECE](../Page/ONE_PIECE.md "wikilink") 劇場版
    機關城的機械巨兵』為大反派一角配音
  - 2007年10月 Arctic Tale 日語版旁述
  - 2019年6月7日上映 海獣の子供 安海正明

## 著作

  - 2001年5月25日 發行『馬耳東風』寫真書

## 專欄

現在連載中

  - 2001年1月至今 女性雜誌『[AN.AN](../Page/AN.AN.md "wikilink")』影評特稿

過去曾連載

  - 1995年 『PLAY BOY週刊』
  - 1996年4月至2000年11月號 「COSMOPOLITAN 」日本版『馬耳東風』書籍出版前身

## 注釋

## 官方網站

  - [稲垣吾郎官方粉絲俱樂部](https://atarashiichizu.com)

  -
  - [稲垣吾郎個人官方博客](https://ameblo.jp/inagakigoro-official)

[Category:SMAP的成员](../Category/SMAP的成员.md "wikilink")
[Category:CULEN](../Category/CULEN.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:日本男性偶像](../Category/日本男性偶像.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:每日電影獎最佳男配角得主](../Category/每日電影獎最佳男配角得主.md "wikilink")
[Category:日刊體育電影大獎最佳男配角得主](../Category/日刊體育電影大獎最佳男配角得主.md "wikilink")
[Category:前傑尼斯事務所所屬藝人](../Category/前傑尼斯事務所所屬藝人.md "wikilink")
[Category:勝利娛樂旗下藝人](../Category/勝利娛樂旗下藝人.md "wikilink")
[Category:假面騎士系列主演演員](../Category/假面騎士系列主演演員.md "wikilink")

1.
2.
3.
4.  [SMAP傳將解散
    僅木村拓哉留在傑尼斯](http://www.cna.com.tw/news/firstnews/201601130023-1.aspx)，中央社，2016年1月13日
5.