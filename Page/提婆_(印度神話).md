[Awatdevatasupperlevel01.JPG](https://zh.wikipedia.org/wiki/File:Awatdevatasupperlevel01.JPG "fig:Awatdevatasupperlevel01.JPG")

**提婆**（，[IPA](../Page/IPA.md "wikilink")：），在[梵語中本意是天](../Page/梵語.md "wikilink")，在[印度教神话体系中转指居住在天界的男性](../Page/印度教神话.md "wikilink")[神明](../Page/神明.md "wikilink")，故亦可译为**天**、**天人**，女性天人称[提毗](../Page/提毗.md "wikilink")。

## 印度教

印度教世间分为天界（提婆界）、空界、地界，天界即提婆界，位于[须弥山在早期的](../Page/须弥山.md "wikilink")[婆罗门教](../Page/婆罗门教.md "wikilink")（印度教前身）中。提婆界的天人是比人“高級”的[生命存在](../Page/生命.md "wikilink")，可能是[神明](../Page/神明.md "wikilink")、[聖人](../Page/聖人.md "wikilink")、或半人半神。女性的神稱為[提毗](../Page/提毗.md "wikilink")（一作岱薇）。惡神[阿修羅則為諸善神提婆的敵對陣營](../Page/阿修羅.md "wikilink")。

[因陀罗](../Page/因陀罗.md "wikilink")（佛教称[帝释天](../Page/帝释天.md "wikilink")）是空界之王，同时是三界之王。随着印度教的發展，对[湿婆](../Page/湿婆.md "wikilink")、[毗湿奴的信仰兴起](../Page/毗湿奴.md "wikilink")，印度教中提婆界[三相神皆成了信徒顶礼膜拜的神祇](../Page/三相神.md "wikilink")。

## 佛教中的提婆神

[佛教对于相關信仰](../Page/佛教.md "wikilink")，認為**提婆**乃是居住在[天界的](../Page/天界.md "wikilink")[有情眾生](../Page/有情.md "wikilink")，即是[天人眾](../Page/天人_\(佛教\).md "wikilink")。他們壽命很長且有大能，一動念，萬般華衣美食隨處湧出，享受多種快樂。但是佛教相信，天人壽命雖然很長但也有盡頭，之後還要進入[輪迴](../Page/輪迴.md "wikilink")，甚至会墮至[人間](../Page/人間.md "wikilink")、[阿修羅乃至](../Page/阿修羅.md "wikilink")[惡道](../Page/惡道.md "wikilink")。\[1\]

## 註释

[D](../Category/印度教神祇.md "wikilink") [D](../Category/梵语词汇.md "wikilink")
[Category:巴利語詞彙](../Category/巴利語詞彙.md "wikilink")

1.  《佛學大辭典》【天】：（界名）梵名提婆Deva，又名素羅Sura，光明之義，自然之義，清淨之義，自在之義，最勝之義。受人間以上勝妙果報之所，其一分在須彌山中，其一分遠在蒼空，總名之為天趣。六趣之一也。又不拘其住處指一切之鬼神，名為天，如鬼子母神謂之鬼母天。又一切好妙之物名為天，如人中之好華謂之天華。大乘義章六末曰：「天者如雜心釋有光明故名之為天，此隨相釋。又云天者淨故名天，天報清淨故名為淨。若依持地所受自然，故名為天。」義林章六本曰：「神用光潔自在名天。」法華文句四曰：「天者天然自然勝、樂勝、身勝、故天為勝，眾事悉勝餘趣，常以光自照，故名為天。」止觀四曰：「自然果報名為天。」婆娑論百七十二曰：「於諸趣中彼趣最勝最樂最善，最善最妙高故名為天。」嘉祥金光明經疏曰：「外國呼神亦名為天。」智度論曰：「天竺國法，名諸好物皆為天物。」