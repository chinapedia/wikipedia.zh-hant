**埃德·德胡耶**(**Ed de Goey**，1966年12月20日 -
)，前[荷蘭足球守門員](../Page/荷蘭.md "wikilink")。

## 生平

球員生涯的-{zh-hans:德胡耶;
zh-hant:迪高爾;}-曾在荷蘭班霸[飛燕諾效力](../Page/飛燕諾.md "wikilink")，除贏過一屆荷蘭甲組聯賽冠軍(1993)外，還贏過四屆荷蘭杯(1991、1992、1994、1995)。至1997年他以二百二十五萬[英鎊轉會費轉投](../Page/英鎊.md "wikilink")[英超球會](../Page/英超.md "wikilink")[切尔西](../Page/切尔西足球俱乐部.md "wikilink")，為球會正選。當年車路士在聯賽只屬中游，德胡耶剛加盟，便為車路士贏得了[歐洲杯賽冠軍杯](../Page/歐洲杯賽冠軍杯.md "wikilink")。2000年[英格蘭足總杯](../Page/英格蘭足總杯.md "wikilink")，德胡耶也順利為切尔西取得冠軍。

由於當時當時車路士實力未足以挑戰聯賽冠軍，德胡耶為切尔西取得的獎項僅限與杯賽有關。後來車路士出現人事變動，德胡耶也於2003年轉投實力較弱的低組別聯賽球會[斯托克城](../Page/斯托克城足球俱樂部.md "wikilink")，至2006年5月退役。

2007年7月6日，德胡耶加盟[女王公园巡游者](../Page/女王公园巡游者足球俱乐部.md "wikilink")，為該會教練團成員。

## 榮譽

  - 荷蘭甲組聯賽：1993
  - 荷蘭杯：1991、1992、1994、1995
  - 歐洲杯賽冠軍杯：1998
  - 英格蘭足總杯：2000

[Category:荷蘭足球主教練](../Category/荷蘭足球主教練.md "wikilink")
[Category:荷蘭足球運動員](../Category/荷蘭足球運動員.md "wikilink")
[Category:飛燕諾球員](../Category/飛燕諾球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:1966年出生](../Category/1966年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:鹿特丹斯巴達球員](../Category/鹿特丹斯巴達球員.md "wikilink")
[Category:史篤城球員](../Category/史篤城球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:1994年世界盃足球賽球員](../Category/1994年世界盃足球賽球員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")