[K線的意義
中国大陆及台湾採用「紅升綠跌」，歐美及香港採用「綠升紅跌」](https://zh.wikipedia.org/wiki/File:Candlestick.svg "fig:K線的意義 中国大陆及台湾採用「紅升綠跌」，歐美及香港採用「綠升紅跌」")

**K线**（）又稱“-{zh-hans:阴阳烛;zh-hk:K線;zh-tw:陰陽燭;}-”，是反映[价格走势的一种图线](../Page/价格.md "wikilink")，其特色在於一個線段內記錄了多項訊息，相當易讀易懂且實用有效，广泛用于[股票](../Page/股票.md "wikilink")、[期貨](../Page/期貨.md "wikilink")、[貴金屬](../Page/貴金屬.md "wikilink")、[数字货币等行情的](../Page/数字货币.md "wikilink")[技术分析](../Page/技术分析.md "wikilink")，称为**K线分析**。

## 由來

据传K线為[日本](../Page/日本.md "wikilink")[江户时代的](../Page/江户时代.md "wikilink")[白米商人](../Page/白米.md "wikilink")[本間宗久所发明](../Page/本間宗久.md "wikilink")，用來記錄每日的米市行情，研析[期貨市場](../Page/期貨.md "wikilink")。日语中K线称为「蠟燭足（）」。

## 說明

K線可分「陽線」、「陰線」和「中立線」三種，**陽線**代表[收盘价大於](../Page/收盘价.md "wikilink")[开盘价](../Page/开盘价.md "wikilink")，**陰線**代表开盘价大於收盘价，**中立線**則代表开盘价等於收盘价。

最早陽線以[紅色表示](../Page/紅色.md "wikilink")，陰線則以[黑色表示](../Page/黑色.md "wikilink")，但由於[彩色印刷成本高](../Page/彩色印刷.md "wikilink")，所以後來陽線常改以[白色空心方塊表示](../Page/白色.md "wikilink")。在中国大陆、台湾，以及日本和韩国，为了配合傳統習慣，陽線以紅色表示，陰線以[綠色表示](../Page/綠色.md "wikilink")，即是紅升綠跌。至於中立線的顏色則不一而足，難以卒論，但以異於其他兩線為原則。在香港和歐美，習慣則正好相反，陰線以紅色表示，陽線以綠色表示，綠升紅跌。

[Candlestick_Chart.png](https://zh.wikipedia.org/wiki/File:Candlestick_Chart.png "fig:Candlestick_Chart.png")

每一K線皆由「實體」和「影線」兩部份組成，實體較影線為粗，影線則依附於實體的上下兩端。其中实体部份記錄当天的開盤價和收盤價，影线部份則記錄当天的最高價和最低价。影線又分為「上影線」和「下影線」兩種，**陽線**的上影線表示最高價和收盘价的差距，下影線表示最低價和開盘价的差距；**陰線**的上影線則表示最高價和开盘价的差距，下影線表示最低價和收盘价的差距。

K線的長短取決於價差，若开盘价和收盘价的差距越大，則實體部份就會越長，所以开盘价等於收盘价的中立線的形狀必定類似「一」、「十」、「丄」、「丅」等文字。影線部份也是一樣的道理，價差越大影線就越長，無價差就無影線。

將某段[時間內的各單位時間下的K線綜合起來便可繪出](../Page/時間.md "wikilink")「K線圖」，其橫軸為\[時間\]，縱軸為\[價格\]。

## 美國線

美國線（，**OHLC**
chart），以豎立的線條表現股價變化，也可以呈現「開盤價、最高價、最低價、收盤價」，豎線呈現最高價和最低價間的價差間距，左側橫線代表開盤價，右側橫線代表收盤價，繪製上較K線簡單。另有一種美國線僅呈現「最高價、最低價、收盤價」（**HLC**）三項訊息。

## 另見

  - [美國線](../Page/OHLC_chart.md "wikilink")
  - [移動平均](../Page/移動平均.md "wikilink")
  - [KD線](../Page/随机指标.md "wikilink")

## 外部链接

  - [K線变种](http://www.authorstream.com/Presentation/nofriends-1388352/)
  - [《陰陽燭戰術》](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626786499)
  - [陰陽燭形態速查](https://web.archive.org/web/20101113105046/http://www.mspandora.com/index.php?option=com_content&view=article&id=94%3Acandlestick-summary&catid=42%3Acandlestick&Itemid=89)
  - [怎样看K线图](http://finance.anhuinews.com/system/2002/03/30/000003989.shtml)
  - [日本模式](https://web.archive.org/web/20080626102731/http://stock-market-strategy.s-f9.com/)
  - [投資知識家：解析美國線、收盤線與K線異同](https://web.archive.org/web/20110616073755/http://news.wearn.com/article.asp?id=740)

[de:Chartanalyse\#Candlestick-Chart
(Kerzenchart)](../Page/de:Chartanalyse#Candlestick-Chart_\(Kerzenchart\).md "wikilink")

[Category:技术分析](../Category/技术分析.md "wikilink")
[Category:统计图表](../Category/统计图表.md "wikilink")