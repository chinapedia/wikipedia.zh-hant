**遣明使**（1401－1547年）是[室町幕府第三代將軍](../Page/室町幕府.md "wikilink")[足利義滿](../Page/足利義滿.md "wikilink")（1368－1394年在位）時期開始的[明朝與日本之間的貿易往來](../Page/明朝.md "wikilink")（[明日貿易](../Page/明日貿易.md "wikilink")）。

日本[南北朝合一之後](../Page/南北朝_\(日本\).md "wikilink")，足利義滿試圖以「日本國王臣源義滿」的名義與明朝建立往來。然而明朝的冊封體制之中，唯一認可的日本的國交往來對象是[南朝的](../Page/南朝_\(日本\).md "wikilink")[懷良親王](../Page/懷良親王.md "wikilink")（名義為「日本國王良懷」），故義滿的計畫未被接受。之後的1401年，義滿以「日本國准三后源道義」的名義再度試圖與明朝建立關係，並派遣了商人[肥富](../Page/肥富.md "wikilink")、僧人[祖阿等](../Page/祖阿.md "wikilink")。1402年被[永樂帝封為](../Page/永樂帝.md "wikilink")「日本國王」，1404年開始了勘合貿易（[明日貿易](../Page/明日貿易.md "wikilink")）。此時期[李氏朝鮮](../Page/李氏朝鮮.md "wikilink")（1392－1910年）也被置於冊封體制之中，東亞的形勢較為安定。

[桂庵玄樹](../Page/桂庵玄樹.md "wikilink")（[臨濟宗僧侶](../Page/臨濟宗.md "wikilink")，[薩南學派](../Page/薩南學派.md "wikilink")，1427－1508年）、[雪舟等楊](../Page/雪舟等楊.md "wikilink")（[水墨畫家](../Page/水墨畫.md "wikilink")、禪僧，1420－1502或1506年）等亦隨行渡明。

## 遣明使年表

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>日本出發的年份</p></th>
<th style="text-align: center;"><p>到達明朝的年份</p></th>
<th style="text-align: center;"><p>正使</p></th>
<th style="text-align: center;"><p><a href="../Page/遣明船.md" title="wikilink">遣明船的船主</a></p></th>
<th style="text-align: center;"><p>備注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>1401年</p></td>
<td style="text-align: center;"><p>1401年</p></td>
<td style="text-align: center;"><p><a href="../Page/祖阿.md" title="wikilink">祖阿</a></p></td>
<td style="text-align: center;"><p><a href="../Page/室町幕府.md" title="wikilink">室町幕府</a></p></td>
<td style="text-align: center;"><p>送交<a href="../Page/國書.md" title="wikilink">國書給明朝</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1403年</p></td>
<td style="text-align: center;"><p>1403年</p></td>
<td style="text-align: center;"><p><a href="../Page/堅中圭密.md" title="wikilink">堅中圭密</a></p></td>
<td style="text-align: center;"><p>室町幕府</p></td>
<td style="text-align: center;"><p>送交國書給明朝</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1404年</p></td>
<td style="text-align: center;"><p>1404年</p></td>
<td style="text-align: center;"><p>？</p></td>
<td style="text-align: center;"><p>室町幕府</p></td>
<td style="text-align: center;"><p><a href="../Page/勘合貿易.md" title="wikilink">勘合貿易開始</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1405年</p></td>
<td style="text-align: center;"><p>1405年</p></td>
<td style="text-align: center;"><p><a href="../Page/明室楚亮.md" title="wikilink">明室楚亮</a></p></td>
<td style="text-align: center;"><p>室町幕府</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1406年</p></td>
<td style="text-align: center;"><p>1407年</p></td>
<td style="text-align: center;"><p><a href="../Page/源通賢.md" title="wikilink">源通賢</a></p></td>
<td style="text-align: center;"><p>室町幕府</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1408年</p></td>
<td style="text-align: center;"><p>1408年</p></td>
<td style="text-align: center;"><p><a href="../Page/昌宣.md" title="wikilink">昌宣</a></p></td>
<td style="text-align: center;"><p>室町幕府</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1410年</p></td>
<td style="text-align: center;"><p>1410年</p></td>
<td style="text-align: center;"><p>堅中圭密</p></td>
<td style="text-align: center;"><p>室町幕府</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1432年</p></td>
<td style="text-align: center;"><p>1433年</p></td>
<td style="text-align: center;"><p><a href="../Page/龍室道淵.md" title="wikilink">龍室道淵</a></p></td>
<td style="text-align: center;"><p>室町幕府、<a href="../Page/相國寺.md" title="wikilink">相國寺</a>、<br />
<a href="../Page/山名氏.md" title="wikilink">山名氏</a>、大名寺院十三家、<br />
三十三間堂</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1434年</p></td>
<td style="text-align: center;"><p>1435年</p></td>
<td style="text-align: center;"><p><a href="../Page/恕中中誓.md" title="wikilink">恕中中誓</a></p></td>
<td style="text-align: center;"><p>室町幕府、相國寺、<br />
山名氏、<a href="../Page/大乘院.md" title="wikilink">大乘院</a>、<br />
三十三間堂</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1451年</p></td>
<td style="text-align: center;"><p>1453年</p></td>
<td style="text-align: center;"><p><a href="../Page/東洋允澎.md" title="wikilink">東洋允澎</a></p></td>
<td style="text-align: center;"><p><a href="../Page/天龍寺.md" title="wikilink">天龍寺</a>、伊勢法樂舍、<br />
<a href="../Page/九州探題.md" title="wikilink">九州探題</a>、<a href="../Page/大友氏.md" title="wikilink">大友氏</a>、<br />
<a href="../Page/大內氏.md" title="wikilink">大內氏</a>、大和</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1465年</p></td>
<td style="text-align: center;"><p>1468年</p></td>
<td style="text-align: center;"><p><a href="../Page/天與清啓.md" title="wikilink">天與清啓</a></p></td>
<td style="text-align: center;"><p>室町幕府、<a href="../Page/細川氏.md" title="wikilink">細川氏</a>、大內氏</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1476年</p></td>
<td style="text-align: center;"><p>1477年</p></td>
<td style="text-align: center;"><p><a href="../Page/笠芳妙茂.md" title="wikilink">笠芳妙茂</a></p></td>
<td style="text-align: center;"><p>室町幕府、相國寺勝鬘院</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1483年</p></td>
<td style="text-align: center;"><p>1484年</p></td>
<td style="text-align: center;"><p><a href="../Page/子璞周璋.md" title="wikilink">子璞周璋</a></p></td>
<td style="text-align: center;"><p>室町幕府、<a href="../Page/日本天皇.md" title="wikilink">朝廷</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1493年</p></td>
<td style="text-align: center;"><p>1495年</p></td>
<td style="text-align: center;"><p><a href="../Page/堯夫壽黃.md" title="wikilink">堯夫壽黃</a></p></td>
<td style="text-align: center;"><p>室町幕府、細川氏</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1506年</p></td>
<td style="text-align: center;"><p>1511年</p></td>
<td style="text-align: center;"><p><a href="../Page/了庵圭吾.md" title="wikilink">了庵圭吾</a></p></td>
<td style="text-align: center;"><p>大內氏</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/宋素卿.md" title="wikilink">宋素卿</a></p></td>
<td style="text-align: center;"><p>細川氏</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1520年</p></td>
<td style="text-align: center;"><p>1523年</p></td>
<td style="text-align: center;"><p><a href="../Page/宗設謙道.md" title="wikilink">宗設謙道</a></p></td>
<td style="text-align: center;"><p>大內氏</p></td>
<td style="text-align: center;"><p>大內氏和細川氏的<a href="../Page/遣明船.md" title="wikilink">遣明船在</a><a href="../Page/寧波.md" title="wikilink">寧波港發生衝突</a>，是為<a href="../Page/寧波之亂.md" title="wikilink">寧波之亂</a>。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/鸞岡瑞佐.md" title="wikilink">鸞岡瑞佐</a></p></td>
<td style="text-align: center;"><p>細川氏</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1538年</p></td>
<td style="text-align: center;"><p>1540年</p></td>
<td style="text-align: center;"><p><a href="../Page/湖心碩鼎.md" title="wikilink">湖心碩鼎</a></p></td>
<td style="text-align: center;"><p>大內氏</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1547年</p></td>
<td style="text-align: center;"><p>1549年</p></td>
<td style="text-align: center;"><p><a href="../Page/策彥周良.md" title="wikilink">策彥周良</a></p></td>
<td style="text-align: center;"><p>大內氏</p></td>
<td style="text-align: center;"><p>最後的遣明使</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 參見

  - [遣明船](../Page/遣明船.md "wikilink")
  - [朝貢體系](../Page/朝貢體系.md "wikilink")
  - [遣隋使](../Page/遣隋使.md "wikilink")
  - [小野妹子](../Page/小野妹子.md "wikilink")
  - [遣唐使](../Page/遣唐使.md "wikilink")
  - [宋日貿易](../Page/宋日貿易.md "wikilink")
  - [明日貿易](../Page/明日貿易.md "wikilink")

[Category:明朝政治](../Category/明朝政治.md "wikilink")
[Category:室町時代](../Category/室町時代.md "wikilink")
[Category:明朝與日本關係](../Category/明朝與日本關係.md "wikilink")