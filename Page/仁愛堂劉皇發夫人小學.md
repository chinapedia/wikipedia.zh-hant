[Yan_Oi_Tong_Madam_Lau_Wong_Fat_Primary_School_(sky-blue_version).JPG](https://zh.wikipedia.org/wiki/File:Yan_Oi_Tong_Madam_Lau_Wong_Fat_Primary_School_\(sky-blue_version\).JPG "fig:Yan_Oi_Tong_Madam_Lau_Wong_Fat_Primary_School_(sky-blue_version).JPG")
**仁愛堂劉皇發夫人小學**（簡稱**劉皇發小學**；）位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[屯門](../Page/屯門.md "wikilink")[安定邨](../Page/安定邨.md "wikilink")，為一所由[仁愛堂主辦](../Page/仁愛堂.md "wikilink")，首屆主席及現任立法會議員[劉皇發](../Page/劉皇發.md "wikilink")[太平紳士以其夫人名義贊助全部經費所創立之小學](../Page/太平紳士.md "wikilink")。上午校於1984年1月4日啟用，下午校於同年9月1日開課，直至1994年開始轉為[全日制小學](../Page/全日制.md "wikilink")。現任校長為劉小慧女士。

## 學校設施

課室22間，亦設有多間科目輔助室。另外，地下雨天操場前後通道分別稱為「前通道」及「後通道」，為供學生休憩的地方。

## 著名/傑出校友

  - [楊愛瑾](../Page/楊愛瑾.md "wikilink")：香港歌手及演員，歌唱組合[Cookies成員](../Page/Cookies.md "wikilink")

## 相關

  - [劉皇發](../Page/劉皇發.md "wikilink")

## 外部連結

  - [學校網址](http://www.yotps.edu.hk)

[Category:屯門區小學](../Category/屯門區小學.md "wikilink")
[Category:屯門市中心](../Category/屯門市中心.md "wikilink")
[Category:仁愛堂學校](../Category/仁愛堂學校.md "wikilink")
[Category:1984年創建的教育機構](../Category/1984年創建的教育機構.md "wikilink")