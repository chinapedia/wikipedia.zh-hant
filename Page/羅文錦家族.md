**羅文錦家族**，始興於20世紀20年代，是[殖民地時期的](../Page/香港殖民地時期.md "wikilink")[香港](../Page/香港.md "wikilink")[四大家族之一](../Page/香港四大家族.md "wikilink")，其餘為[何啟東家族](../Page/何啟東家族.md "wikilink")、[高可寧家族和](../Page/高可寧家族.md "wikilink")[利希慎家族](../Page/利希慎家族.md "wikilink")（一說為[傅老榕家族](../Page/傅老榕家族.md "wikilink")）。

## 家族特色

羅文錦家族中之各種特色：

  - [香港歐亞混血兒家族](../Page/香港歐亞混血兒.md "wikilink")
  - 以華人家族自居，家族籍貫跟隨母系的廣東寶安
  - [法律世家](../Page/法律.md "wikilink")
      - [法官](../Page/法官.md "wikilink")：羅文灝、[烈顯倫](../Page/烈顯倫.md "wikilink")
      - [律師](../Page/律師.md "wikilink")：羅文錦、羅文惠、羅文灝、[羅德丞](../Page/羅德丞.md "wikilink")、羅德璋、羅偉拔
  - 與[何啟東家族有五段姻親關係](../Page/何啟東家族.md "wikilink")：
      - 羅絮才＝[何啟福](../Page/何啟福.md "wikilink")
      - 羅文錦＝何錦姿
      - 羅巧貞＝何世奇
      - 羅雪貞＝何世亮
      - 羅文灝＝何堯姿
  - 與[陳啟明家族有一段姻親關係](../Page/陳啟明家族.md "wikilink")：
      - 羅燕嫦＝Cecil Hynes Lyson
  - 與[張椿錦家族有一段姻親關係](../Page/張椿錦家族.md "wikilink")：
      - 羅德丞＝張慧瑜

<!-- end list -->

  - 與[施炳光家族](../Page/施炳光家族.md "wikilink")(Andrew Zimmern)有一段姻親關係：
      - 羅長肇＝施湘卿

## 興起方法

羅文錦家族中各人之興起歷程：

  - **羅文錦**的興起歷程：先任職律師，及後歷任立法局非官守議員及行政局非官守議員
  - **羅文惠**的興起歷程：先任職律師，及後歷任立法局非官守議員及行政局非官守議員
  - **羅德丞**的興起歷程：先任職律師，及後歷任立法局非官守議員及行政局非官守議員

## 家訓格言

羅文錦家族中各人的家訓格言：

  - **羅長肇**的家訓格言：「行人頭好過跟鬼尾！」（譯文：(香港歐亞混血兒族群定位上)寧可當個優秀的中國人，不要當個卑微的歐美白人！）\[1\]

## 家族成員

羅文錦家族的家族成員：

  - [羅富華](../Page/羅富華.md "wikilink")＝[曾有](../Page/曾有.md "wikilink")\[2\]
      - [羅瑞彩](../Page/羅瑞彩.md "wikilink")＝[何啟福](../Page/何啟福.md "wikilink")
          - [何寶姿](../Page/何寶姿.md "wikilink")
          - [何世耀](../Page/何世耀.md "wikilink")
              - [何鴻堅](../Page/何鴻堅.md "wikilink")
              - [何鴻超](../Page/何鴻超.md "wikilink")
                  - [何英進](../Page/何英進.md "wikilink")
          - [何世光](../Page/何世光.md "wikilink")
              - [何鴻恩](../Page/何鴻恩.md "wikilink")
                  - [何猷富](../Page/何猷富.md "wikilink")
                  - [何猷財](../Page/何猷財.md "wikilink")
              - [何婉和](../Page/何婉和.md "wikilink")
              - [何鴻展](../Page/何鴻展.md "wikilink")
                  - [何猷強](../Page/何猷強.md "wikilink")
              - [何鴻威](../Page/何鴻威.md "wikilink")
              - [何婉璋](../Page/何婉璋.md "wikilink")
              - [何婉文](../Page/何婉文.md "wikilink")
              - [何鴻韜](../Page/何鴻韜.md "wikilink")
              - [何婉鴻](../Page/何婉鴻.md "wikilink")
              - [何鴻燊](../Page/何鴻燊.md "wikilink")＝[黎婉華](../Page/黎婉華.md "wikilink")（元配）、[藍瓊纓](../Page/藍瓊纓.md "wikilink")（妾侍）、[陳婉珍](../Page/陳婉珍.md "wikilink")（三姨太）、[梁安琪](../Page/梁安琪.md "wikilink")（四姨太）
                  - [何超英](../Page/何超英.md "wikilink")（[黎婉華所出](../Page/黎婉華.md "wikilink")）
                  - [何猷光](../Page/何猷光.md "wikilink")（[黎婉華所出](../Page/黎婉華.md "wikilink")）
                      - [何家文](../Page/何家文.md "wikilink")
                      - [何家华](../Page/何家华.md "wikilink")
                  - [何超賢](../Page/何超賢.md "wikilink")（[黎婉華所出](../Page/黎婉華.md "wikilink")）
                  - [何超雄](../Page/何超雄.md "wikilink")（[黎婉華所出](../Page/黎婉華.md "wikilink")）
                  - [何超瓊](../Page/何超瓊.md "wikilink")（[藍瓊纓所出](../Page/藍瓊纓.md "wikilink")）＝[許晉亨](../Page/許晉亨.md "wikilink")（離婚）
                  - [何超鳳](../Page/何超鳳.md "wikilink")（[藍瓊纓所出](../Page/藍瓊纓.md "wikilink")）＝[何志堅](../Page/何志堅.md "wikilink")
                  - [何超蕸](../Page/何超蕸.md "wikilink")（[藍瓊纓所出](../Page/藍瓊纓.md "wikilink")）
                  - [何超儀](../Page/何超儀.md "wikilink")（[藍瓊纓所出](../Page/藍瓊纓.md "wikilink")）＝[陳子聰](../Page/陳子聰.md "wikilink")
                  - [何猷龍](../Page/何猷龍.md "wikilink")（[藍瓊纓所出](../Page/藍瓊纓.md "wikilink")）＝[羅秀茵](../Page/羅秀茵.md "wikilink")
                      - [何開梓](../Page/何開梓.md "wikilink")
                  - [何超云](../Page/何超云.md "wikilink")（[陈婉珍所出](../Page/陈婉珍.md "wikilink")）
                  - [何超莲](../Page/何超莲.md "wikilink")（[陈婉珍所出](../Page/陈婉珍.md "wikilink")）
                  - [何猷启](../Page/何猷启.md "wikilink")（[陈婉珍所出](../Page/陈婉珍.md "wikilink")）
                  - [何超盈](../Page/何超盈.md "wikilink")（[梁安琪所出](../Page/梁安琪.md "wikilink")）
                  - [何猷亨](../Page/何猷亨.md "wikilink")（[梁安琪所出](../Page/梁安琪.md "wikilink")）
                  - [何猷君](../Page/何猷君.md "wikilink")（[梁安琪所出](../Page/梁安琪.md "wikilink")）
                  - [何超欣](../Page/何超欣.md "wikilink")（[梁安琪所出](../Page/梁安琪.md "wikilink")）
              - [何婉琪](../Page/何婉琪.md "wikilink")（Winnie，十姑娘）＝[麥志偉](../Page/麥志偉.md "wikilink")（離婚）、
                [杜紹能](../Page/杜紹能.md "wikilink")
                  - [麥慧貞](../Page/麥慧貞.md "wikilink")（[麥志偉所出](../Page/麥志偉.md "wikilink")）
                  - [麥家興](../Page/麥家興.md "wikilink")（[麥志偉所出](../Page/麥志偉.md "wikilink")）
                  - [麥舜銘](../Page/麥舜銘.md "wikilink")（更名[何東舜銘](../Page/何東舜銘.md "wikilink")，與[何鴻章私生](../Page/何鴻章.md "wikilink")）＝[陳復生](../Page/陳復生.md "wikilink")
                  - [麥慧玉](../Page/麥慧玉.md "wikilink")（與[何鴻章私生](../Page/何鴻章.md "wikilink")）
              - [何婉婉](../Page/何婉婉.md "wikilink")（Susie，十一姑娘）＝[葉德利](../Page/葉德利.md "wikilink")
              - [何鴻端](../Page/何鴻端.md "wikilink")
              - [何婉颖](../Page/何婉颖.md "wikilink")
          - [何世亮](../Page/何世亮.md "wikilink")
          - [何世全](../Page/何世全.md "wikilink")
          - [何世焯](../Page/何世焯.md "wikilink")
          - [何寶容](../Page/何寶容.md "wikilink")
          - [何世吉](../Page/何世吉.md "wikilink")
          - [何寶蓮](../Page/何寶蓮.md "wikilink")
          - [何宝芝](../Page/何宝芝.md "wikilink")
          - [何麗瓊](../Page/何麗瓊.md "wikilink")
      - [羅長業](../Page/羅長業.md "wikilink")（羅福基，Cheung Ip
        Lo）\[3\]＝[何群芳](../Page/何群芳.md "wikilink")（Kwan Fong
        Ho）
          - [羅燕嫦](../Page/羅燕嫦.md "wikilink")（Violet In Sheung Lo）＝Cecil
            Hynes Lyson
              - Olive Lyson
          - [羅燕茹](../Page/羅燕茹.md "wikilink")（羅淑芝，Laura In Yu
            Lo）＝[蔡寶綿](../Page/蔡寶綿.md "wikilink")（Po Min Choa）
              - [蔡慧嫻](../Page/蔡慧嫻.md "wikilink")
              - [蔡永禧](../Page/蔡永禧.md "wikilink")
              - [蔡永超](../Page/蔡永超.md "wikilink")
          - [羅文彬](../Page/羅文彬_\(香港\).md "wikilink")（Man Pun
            Lo）＝[戴瑞賢](../Page/戴瑞賢.md "wikilink")（Nellie Sui
            Yin Tai）
              - [羅國慶](../Page/羅國慶.md "wikilink")
              - [羅志芬](../Page/羅志芬.md "wikilink")（Daphne Chi Fun Lo）
              - [羅國雄](../Page/羅國雄.md "wikilink")
              - [羅國衡](../Page/羅國衡.md "wikilink")（Kenneth Horace Kwok
                Hang Lo）
          - [羅燕端](../Page/羅燕端.md "wikilink")（Lily In Duen Lo）
          - [羅燕德](../Page/羅燕德.md "wikilink")＝Thomas Symons
          - [羅燕安](../Page/羅燕安.md "wikilink")（Anna In On
            Lo）＝[陳維衡](../Page/陳維衡.md "wikilink")（Wai Hang
            Chan）
              - [陳志堅](../Page/陳志堅.md "wikilink")
              - [陳珍莉](../Page/陳珍莉.md "wikilink")
              - [陳珠莉](../Page/陳珠莉.md "wikilink")
          - [羅文輝](../Page/羅文輝.md "wikilink")＝[郭杏焱](../Page/郭杏焱.md "wikilink")
              - [羅國華](../Page/羅國華.md "wikilink")
              - [羅國良](../Page/羅國良.md "wikilink")
      - [羅長肇](../Page/羅長肇.md "wikilink")（羅世基，Cheung Shiu
        Lo）\[4\]＝[施湘卿](../Page/施湘卿.md "wikilink")
          - [羅雪貞](../Page/羅雪貞.md "wikilink")
          - [羅文錦](../Page/羅文錦.md "wikilink")＝[何錦姿](../Page/何錦姿.md "wikilink")
              - [羅佩筠](../Page/羅佩筠.md "wikilink")
              - [羅德權](../Page/羅德權.md "wikilink")
              - [羅佩堯](../Page/羅佩堯.md "wikilink")
              - [羅佩堅](../Page/羅佩堅.md "wikilink")＝[許賢根](../Page/許賢根.md "wikilink")
              - [羅佩賢](../Page/羅佩賢.md "wikilink")＝[邱建江](../Page/邱建江.md "wikilink")
              - [羅德丞](../Page/羅德丞.md "wikilink")＝[張慧瑜](../Page/張慧瑜.md "wikilink")
                  - [羅克平](../Page/羅克平.md "wikilink")
                  - [羅信優](../Page/羅信優.md "wikilink")
                  - [羅國倫](../Page/羅國倫.md "wikilink")
                  - [羅孔君](../Page/羅孔君.md "wikilink")
          - [羅文顯](../Page/羅文顯.md "wikilink")＝[林惠姬](../Page/林惠姬.md "wikilink")
          - [羅文惠](../Page/羅文惠.md "wikilink")＝[洪奇芳](../Page/洪奇芳.md "wikilink")（Margaret
            Hung）
              - [羅德韜](../Page/羅德韜.md "wikilink")
              - [羅德明](../Page/羅德明.md "wikilink")
              - [羅德璋](../Page/羅德璋.md "wikilink")
                  - [羅偉勳](../Page/羅偉勳.md "wikilink")
                  - [羅偉彬](../Page/羅偉彬.md "wikilink")
                  - [羅舜玲](../Page/羅舜玲.md "wikilink")
                  - [羅偉豪](../Page/羅偉豪.md "wikilink")
                  - [羅偉拔](../Page/羅偉拔.md "wikilink")
              - [羅德輝](../Page/羅德輝.md "wikilink")
                  - [羅佩筠](../Page/羅佩筠.md "wikilink")
          - [羅巧貞](../Page/羅巧貞.md "wikilink")＝何世奇
              - [何鴻政](../Page/何鴻政.md "wikilink") (Algernon)
              - [何鴻嘉](../Page/何鴻嘉.md "wikilink") (Ronald)
              - [何婉揚](../Page/何婉揚.md "wikilink") (Pamela)
              - [何婉倫](../Page/何婉倫.md "wikilink") (Cecilia)
              - [何鴻鑾](../Page/何鴻鑾.md "wikilink") (Eric Peter)
          - [羅寶貞](../Page/羅寶貞.md "wikilink")
          - [羅文灝](../Page/羅文灝.md "wikilink")＝[何堯姿](../Page/何堯姿.md "wikilink")（前妻）
              - [羅佩嫻](../Page/羅佩嫻.md "wikilink")＝[潘通理](../Page/潘通理.md "wikilink")
          - [羅德貞](../Page/羅德貞.md "wikilink")＝John L. Litton
              - [列顯倫大法官](../Page/列顯倫.md "wikilink")
          - [羅琪貞](../Page/羅琪貞.md "wikilink")＝[周國榮](../Page/周國榮.md "wikilink")

## 參見章節

  - [英帝國勳章](../Page/英帝國勳章.md "wikilink")
  - [香港授勳及嘉獎制度](../Page/香港授勳及嘉獎制度.md "wikilink")
  - [施炳光家族](../Page/施炳光家族.md "wikilink")

## 注釋

[Category:香港家族](../Category/香港家族.md "wikilink")

1.  見《中西融和：羅何錦姿》
2.
3.
4.