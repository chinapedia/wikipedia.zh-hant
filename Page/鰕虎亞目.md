**鰕虎亞目**（[学名](../Page/学名.md "wikilink")：）為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鰕虎目](../Page/鰕虎目.md "wikilink")（或传统分类法[鱸形目](../Page/鱸形目.md "wikilink")\[1\]）的其中一個[亞目](../Page/亞目.md "wikilink")。

## 分類

### 内部分类

依2016年Deepfin《[硬骨鱼支序分类法](../Page/硬骨鱼支序分类法.md "wikilink")》第4版，本亚目包含8科：

  - [嵴塘鳢科](../Page/嵴塘鳢科.md "wikilink") Butidae
  - [塘鳢科](../Page/塘鳢科.md "wikilink")
    Eleotridae（包含原有的[扁头鰕虎科](../Page/扁头鰕虎科.md "wikilink")
    Xenisthmidae）
  - [鰕虎科](../Page/鰕虎科.md "wikilink")
    Gobiidae（包含原有的[蚓鰕虎科](../Page/蚓鰕虎科.md "wikilink")
    Microdesmidae、[沙鰕虎科](../Page/沙鰕虎科.md "wikilink")
    Kraemeriidae、[凹尾塘鳢科](../Page/凹尾塘鳢科.md "wikilink")
    Ptereleotridae及[辛氏鱼科](../Page/辛氏鱼科.md "wikilink") Schindleriidae）
  - [沙塘鳢科](../Page/沙塘鳢科.md "wikilink") Odontobutidae
  - [背眼鰕虎科](../Page/背眼鰕虎科.md "wikilink") Oxudercidae
  - [澳洲大口塘鳢科](../Page/澳洲大口塘鳢科.md "wikilink") Milyeringidae
  - [溪鳢科](../Page/溪鳢科.md "wikilink") Rhyacichthyidae
  - [海塘鳢科](../Page/海塘鳢科.md "wikilink") Thalasseleotrididae

## 参考文献

[\*](../Category/鰕虎亞目.md "wikilink")
[Category:虾虎鱼目](../Category/虾虎鱼目.md "wikilink")

1.  [《中国动物志. 硬骨鱼纲. 鲈形目(五).
    虾虎鱼亚目》](http://reading.sciencepress.cn/shop/book/Booksimple/show.do?id=B3669CEAFBB8C441B856CB4F1B6711350000)，伍汉霖，钟俊生等编著，科学出版社，2008年，ISBN
    9787030213334