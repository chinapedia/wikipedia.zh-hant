**D**,
**d**是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")4个[字母](../Page/字母.md "wikilink")。

[D_cursiva.gif](https://zh.wikipedia.org/wiki/File:D_cursiva.gif "fig:D_cursiva.gif")

[闪族语的](../Page/闪族语.md "wikilink")可能是从[雨或](../Page/雨.md "wikilink")[门的缩略图中发展而来的](../Page/门.md "wikilink")。

## 字母D的含意

## 字符编码

| 字符编码                              | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| --------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写D](../Page/大写字母.md "wikilink") | 68                                   | 0044                                     | 196                                    | `-··`                              |
| [小写d](../Page/小写字母.md "wikilink") | 100                                  | 0064                                     | 132                                    |                                    |

## 参看

### D的变体

  - （eth）

  - （d with stroke）

  - （d with tail，又名African D，因為主要用於非洲的語言）

:\* 留意以上3个字母，大写的形状完全一样，仅小写有分别。

  - [∂](../Page/∂.md "wikilink")（数学上的部分微分符号）

### 其他字母中的相近字母

  - （[希腊字母Delta](../Page/希腊字母.md "wikilink")）

  - （[西里尔字母De](../Page/西里尔字母.md "wikilink")）

## 其他

## 另見

  -
[Category:拉丁字母](../Category/拉丁字母.md "wikilink")