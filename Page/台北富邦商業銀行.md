**臺北富邦商業銀行**，簡稱**臺北富邦銀行**、**北富銀**，為[臺灣的大型](../Page/臺灣.md "wikilink")[商業銀行之一](../Page/商業銀行.md "wikilink")，隸屬於[富邦金控旗下](../Page/富邦金融控股公司.md "wikilink")，2005年1月1日由**臺北銀行**及**富邦商業銀行**合併而成。國內營業據點共有127間，海外營業據點共5間。

## 沿革

  - 台北富邦銀行的前身分別為**台北銀行**和**富邦銀行**。
  - 1969年4月21日，[台北市政府成立](../Page/台北市政府.md "wikilink")**台北市銀行**，為公營銀行。
  - 1992年，富邦銀行則由[富邦集團創立](../Page/富邦集團.md "wikilink")。
  - 1993年1月1日，台北市銀行更名為**台北銀行**，仍為公營銀行。
  - 1997年7月23日，台北銀行[股票上市並開始](../Page/股票上市.md "wikilink")[民營化](../Page/民營化.md "wikilink")。
  - 2002年，富邦集團標下台北市政府所擁有的台北銀行股權。
  - 2005年，與旗下原有的富邦銀行合併。為省去台北銀行員工年資結算金，富邦集團設定台北銀行為存續銀行，故現在的台北富邦銀行延續使用台北銀行的[金融機構代號](../Page/金融機構代號.md "wikilink")**012**；而富邦銀行因為為消滅銀行，故其金融機構代號**813**已經消失。
  - 北富銀總行設於[台北市](../Page/台北市.md "wikilink")[大安區](../Page/大安區_\(臺北市\).md "wikilink")[仁愛路四段](../Page/仁愛路_\(台北市\).md "wikilink")169號[富邦金融中心](../Page/富邦金融中心.md "wikilink")1樓，但其公司登記地址及營業部仍位於台北市[中山區](../Page/中山區_\(臺北市\).md "wikilink")[中山北路二段](../Page/中山北路.md "wikilink")50號「台北富邦銀行中山大樓」（原[台北銀行大廈](../Page/台北銀行大廈.md "wikilink")）。

[Fubon_Banking_Center_20131015.jpg](https://zh.wikipedia.org/wiki/File:Fubon_Banking_Center_20131015.jpg "fig:Fubon_Banking_Center_20131015.jpg")
[Taipei_Fubon_Bank_headquarters_20160723.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Fubon_Bank_headquarters_20160723.jpg "fig:Taipei_Fubon_Bank_headquarters_20160723.jpg")
[Taipei_Fubon_Bank_Zhongshan_Building.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Fubon_Bank_Zhongshan_Building.jpg "fig:Taipei_Fubon_Bank_Zhongshan_Building.jpg")
[Advent-of-the-Phoenix-by-Yuyu-Yang_introduction_1993-01-01.jpg](https://zh.wikipedia.org/wiki/File:Advent-of-the-Phoenix-by-Yuyu-Yang_introduction_1993-01-01.jpg "fig:Advent-of-the-Phoenix-by-Yuyu-Yang_introduction_1993-01-01.jpg")[雕塑作品](../Page/雕塑.md "wikilink")「鳳凰來儀」複製品解說牌，設立於1993年1月1日，上有台北銀行中文及英文商標。\]\]

### 台北銀行

  - 1967年：台北市升格為[直轄市](../Page/直轄市.md "wikilink")，為了支援市政建設及代理[市庫業務等目的](../Page/臺北市市庫.md "wikilink")，[台北市政府開始籌設市營金融機構](../Page/臺北市政府.md "wikilink")。
  - 1969年4月21日：**台北市銀行**正式成立（簡稱**市銀**；金融代號：012），[資本額約](../Page/資本額.md "wikilink")[新台幣一億二千萬元](../Page/新台幣.md "wikilink")。當時之業務範圍僅限於台北市轄區，且未具[法人身份](../Page/法人.md "wikilink")。
  - 1983年7月4日：台北市銀行總行遷入[台北市](../Page/台北市.md "wikilink")[中山區](../Page/中山區_\(臺北市\).md "wikilink")[中山北路二段](../Page/中山北路.md "wikilink")50號的[台北銀行大廈](../Page/台北銀行大廈.md "wikilink")。
  - 1984年7月1日：台北市銀行依照《銀行法》改組為[股份有限公司](../Page/股份有限公司.md "wikilink")，全名為**台北市銀行股份有限公司**（英文名稱：City
    Bank of Taipei），資本額增加至新台幣三十億元。
  - 1993年1月1日：台北市銀行更名為**台北銀行**，全名為**台北銀行股份有限公司**（簡稱**北銀**，英文名稱：TaipeiBank）。
  - 1994年：台北銀行在[高雄市設立高雄分行](../Page/高雄市.md "wikilink")，這是台北銀行首度跨區設立分行。
  - 1995年1月20日：台北銀行獲[財政部核准](../Page/中華民國財政部.md "wikilink")，由區域性銀行改制為全國性銀行。
  - 1997年7月23日：台北銀行完成現金增資並於公開發行股票上市，代號5836。
  - 1999年11月30日：台北市政府持有的台北銀行股份降至50%以下，台北銀行在名義上成為[民營企業](../Page/民營企業.md "wikilink")，但台北市政府仍持有多數股份。
  - 2002年：台北市政府標售其持有之台北銀行[股票共](../Page/股票.md "wikilink")9億7,236萬餘股（股權比例約45％），由[富邦金控標下](../Page/富邦金控.md "wikilink")。同年12月23日，[富邦金控與台北市政府完成股份轉換作業](../Page/富邦金控.md "wikilink")，富邦金控持有台北銀行百分之百股權，使其成為富邦金控旗下之子公司，並終止股票之公開發行；台北市政府則轉為持有富邦金控11億5,900萬餘股，佔其股權比例約15％。台北市政府並與富邦金控約定，非經同意，台北銀行在合併五年之內不得更名。合約遭到輿論強烈質疑，民進黨分別於2004、2008、2011反覆舉發，檢方查無不法。

### 台北銀行歷年繳庫盈餘明細表

|                                                    |           |
| -------------------------------------------------- | --------- |
| 年度                                                 | 繳庫盈餘/千元   |
| 1992                                               | 1,499,920 |
| 1993                                               | 1,499,920 |
| 1994                                               | 1,499,920 |
| 1995                                               | 1,499,920 |
| 1996                                               | 1,535,918 |
| 1997                                               | 1,535,918 |
| [1998](../Page/1998年中華民國直轄市市長暨市議員選舉.md "wikilink") | 959,949   |
| 1999                                               | 僅配發股票股利   |
| 2000                                               | 894,931   |
| 2001                                               | 972,369   |

  - 1998年12月5日舉行[台北市長第二屆選舉後](../Page/1998年中華民國直轄市市長暨市議員選舉.md "wikilink")，市長由[陳水扁轉為](../Page/陳水扁.md "wikilink")[馬英九繼任](../Page/馬英九.md "wikilink")。

### 富邦銀行

  - 1992年4月20日：富邦商業銀行成立，全名為**富邦商業銀行股份有限公司**（Fubon Commercial
    Bank，簡稱**富邦銀行**，金融代號：813）。
  - 2000年：[美國金融巨擘](../Page/美國.md "wikilink")[花旗集團宣布與富邦集團](../Page/花旗集團.md "wikilink")[策略聯盟](../Page/策略聯盟.md "wikilink")，並購下富邦銀行部分股權（後改為富邦金控股權），至2004年底結束。

### 台北富邦銀行

  - 2005年1月1日：台北銀行股份有限公司與富邦商業銀行股份有限公司合併，台北銀行為存續公司，富邦銀行為消滅公司。經台北市政府同意，台北銀行更名為**台北富邦商業銀行**。
  - 2010年3月：合併於2009年標下的[慶豐銀行於](../Page/慶豐銀行.md "wikilink")[越南](../Page/越南.md "wikilink")[河內及](../Page/河內.md "wikilink")[胡志明市的兩家分行](../Page/胡志明市.md "wikilink")\[1\]。

## 參考資料

  -
  - [台北富邦銀行官方網站](http://www.taipeifubon.com.tw)

  - [台灣金融自由化前史(1950\~1990)--從國家獨占到金融自由化](http://nccuir.lib.nccu.edu.tw/handle/140.119/35738)
    劉揚銘 政大機構典藏

  - [台灣開放民營銀行的經驗教訓](http://www.china-review.com/sao.asp?id=6086) 徐滇慶 中評網

## 參見

  - [富邦金融控股公司](../Page/富邦金融控股公司.md "wikilink")
  - [富邦銀行（香港）](../Page/富邦銀行（香港）.md "wikilink")
  - [台北市政府](../Page/臺北市政府.md "wikilink")
  - [台灣彩券](../Page/台灣彩券.md "wikilink")
  - [台北富邦國際馬拉松賽](../Page/台北富邦國際馬拉松賽.md "wikilink")
  - 體育機構：[中華職棒](../Page/中華職棒.md "wikilink")[富邦悍將隊](../Page/富邦悍將隊.md "wikilink")（前身為中職之[俊國熊](../Page/俊國熊.md "wikilink")、[興農牛和](../Page/興農牛.md "wikilink")[義大犀牛](../Page/義大犀牛.md "wikilink")）、[SBL富邦籃球隊等](../Page/SBL.md "wikilink")。
  - [富邦华一銀行](../Page/富邦华一銀行.md "wikilink")

## 外部連結

  - [台北富邦銀行](http://www.fubon.com/financial/home/index.htm)

  -
  -
[T](../Category/臺灣的銀行.md "wikilink") [T](../Category/富邦金控.md "wikilink")
[0](../Category/臺灣證券交易所上市公司.md "wikilink")
[Category:亞洲萬里通](../Category/亞洲萬里通.md "wikilink")
[Category:1969年台灣建立](../Category/1969年台灣建立.md "wikilink")
[Category:1969年成立的银行](../Category/1969年成立的银行.md "wikilink")
[Category:2005年台灣建立](../Category/2005年台灣建立.md "wikilink")
[Category:2005年開業銀行](../Category/2005年開業銀行.md "wikilink")

1.  [正式合併慶豐越南分行！北富銀積極開發大中華台商市場](http://www.nownews.com/n/2010/03/08/757831)，NOWnews新聞網，2010-0-08