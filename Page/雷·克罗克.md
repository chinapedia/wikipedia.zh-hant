**雷蒙德·阿爾伯特·“雷”·克洛克**（，），美国企业家，生于[伊利诺伊州](../Page/伊利诺伊州.md "wikilink")。1955年，他接管了当时规模很小的[麦当劳公司的特许权](../Page/麦当劳.md "wikilink")，将其发展成全球最成功的[快餐集团之一](../Page/快餐.md "wikilink")。他被《[时代](../Page/时代.md "wikilink")》杂志列为全球最有影响力的企业创始人之一。\[1\]1974年开始，他成为[聖地牙哥教士棒球队老板](../Page/聖地牙哥教士.md "wikilink")。

## 早年生活

1902年，克洛克出生在[伊利诺斯州](../Page/伊利诺斯州.md "wikilink")[芝加哥市的一个](../Page/芝加哥市.md "wikilink")[捷克裔家庭](../Page/捷克.md "wikilink")。[一战期间](../Page/一战.md "wikilink")，他受训成为一名[救护车](../Page/救护车.md "wikilink")[司机](../Page/司机.md "wikilink")。可还没轮到他上[战场立功](../Page/战场.md "wikilink")，[战争就结束了](../Page/战争.md "wikilink")。一战结束后至50年代初，他先后在多个[行业谋生](../Page/行业.md "wikilink")，如[纸杯](../Page/纸杯.md "wikilink")[推销员](../Page/推销员.md "wikilink")，[钢琴演奏](../Page/钢琴.md "wikilink")，[爵士乐手](../Page/爵士乐.md "wikilink")，[乐队成员](../Page/乐队.md "wikilink")，以及在芝加哥[电台工作](../Page/电台.md "wikilink")。最后他成了一名综合[奶昔机推销员](../Page/奶昔.md "wikilink")，[行销全美国](../Page/行销.md "wikilink")。这个工作让他认识了[理察和莫里斯·麦当劳兄弟](../Page/理察和莫里斯·麦当劳兄弟.md "wikilink")。

## 購買麥當勞公司

1948年，麦当劳兄弟在[加州](../Page/加州.md "wikilink")[圣伯那地诺开设了第一家](../Page/聖貝納迪諾縣_\(加利福尼亞州\).md "wikilink")[麦当劳餐厅](../Page/麦当劳餐厅.md "wikilink")。他们独创的汉堡餐厅生意非常好，居然同时使用八台奶昔搅拌机。在确认能销售大批搅拌机给每家新餐厅后，雷在1955年开始和[麦当劳兄弟联手](../Page/理察和莫里斯·麥當勞兄弟.md "wikilink")，著手开发麦当劳餐厅的[特许加盟](../Page/特许加盟.md "wikilink")[业务](../Page/业务.md "wikilink")。由于麦当劳兄弟不愿大規模擴張，这让克洛克最终变得很沮丧。於是在1961年，克洛克決定从麦当劳兄弟手中买下公司，他们达成一项[协议](../Page/协议.md "wikilink")，即麦当劳兄弟一次性地获得270万美元出让麦当劳连锁餐厅，每年还可以得到公司总营业额的1%作为[专营权](../Page/专营权.md "wikilink")[使用费](../Page/使用费.md "wikilink")。

協議中专营权使用费的部分最後變成以[握手协议的方式達成](../Page/握手协议.md "wikilink")，因为克洛克坚称，他找來為本協議出资的人不希望专营权使用费的文字條款出現在合約中。在談判末尾，麦当劳兄弟堅持把他們的首間麥當勞餐廳的[房地产](../Page/房地产.md "wikilink")[产权移轉给他們的雇員](../Page/产权.md "wikilink")，不肯移交给克洛克，这让克洛克心懷怨恨。後來，克洛克拒绝承认或履行握手协议的专营权使用费部分，他还在首間麥當勞餐厅（當時改名为“大M餐廳”——因为在協議中麦当劳兄弟喪失了麥當勞的商標權）附近开了一家新的麦当劳餐厅，導致真正的第一家麥當勞餐厅关门歇业。

## 逝世

1984年1月14日，克洛克因[心脏病在加州](../Page/心脏病.md "wikilink")[圣地亚哥的](../Page/聖地牙哥_\(加利福尼亞州\).md "wikilink")[斯克里普斯纪念医院逝世](../Page/斯克里普斯纪念医院.md "wikilink")，享年81岁。他的遗孀是第三任妻子[琼·克洛克](../Page/琼·克洛克.md "wikilink")（Joan
Kroc）。他曾有过两次[婚姻](../Page/婚姻.md "wikilink")，分别是[艾瑟爾·佛萊明](../Page/艾瑟爾·佛萊明.md "wikilink")(，1922年
- 1961年）和[珍·杜賓斯·葛林](../Page/珍·杜賓斯·葛林.md "wikilink")(，1963年 -
1968年）时任[約翰·韋恩](../Page/約翰·韋恩.md "wikilink")()[秘书](../Page/秘书.md "wikilink")\[2\]。

## 參見

  - [速食遊戲](../Page/速食遊戲.md "wikilink")

## 参考资料

## 外部链接

  - [Obituary, New York Times, January 15, 1984 *Ray Kroc Dies at 81;
    Built McDonald's
    Chain*](http://www.nytimes.com/learning/general/onthisday/bday/1005.html)
  - [Obituary, New York Times, July 16, 1998 *Richard McDonald, 89,
    Fast-Food
    Revolutionary*](http://query.nytimes.com/gst/fullpage.html?res=9D0DE7D81630F935A25754C0A96E958260)
  - [The Burger That Conquered the Country, TIME,
    Sep. 17, 1973](http://www.time.com/time/magazine/article/0,9171,907911,00.html)
  - [*Ray Kroc, McDonald's, and the Fast-Food Industry*, Forbes Greatest
    Business Stories of All Time, by Daniel
    Gross](http://www.wiley.com/legacy/products/subject/business/forbes/sample.html)
  - [Ray Kroc quotes at
    Thinkexist.com](http://www.en.thinkexist.com/quotes/ray_kroc/)
  - [Famous Entrepreneurs at motivational website
    EvanCarmichael.com](https://web.archive.org/web/20080502133159/http://www.evancarmichael.com/Famous-Entrepreneurs/756/Ray-Kroc-Quotes.html)

[K](../Category/美国企业家.md "wikilink")
[Category:麦当劳人物](../Category/麦当劳人物.md "wikilink")
[Category:捷克裔美國人](../Category/捷克裔美國人.md "wikilink")
[Category:芝加哥人](../Category/芝加哥人.md "wikilink")
[Category:加利福尼亚州共和党人](../Category/加利福尼亚州共和党人.md "wikilink")

1.  [《TIME 100：Ray
    Kroc》](http://www.time.com/time/time100/builder/profile/kroc.html)
2.  The former Dire Straits guitarist and lead vocalist Mark Knopfler
    released a song about Ray Kroc on his 2004 album Shangri-La. It was
    inspired by Ray Kroc's autobiography Grinding It Out and the
    starting of McDonald's, using many of Mr. Kroc's exact words."Kroc
    style, boom like that"