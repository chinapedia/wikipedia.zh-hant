**DAPI**即**4',6-二脒基-2-苯基吲哚**(4',6-**d**i**a**midino-2-**p**henyl**i**ndole)，是一種能夠與[DNA強力結合的](../Page/DNA.md "wikilink")[熒光](../Page/熒光.md "wikilink")[染料](../Page/染料.md "wikilink")，常用於螢光[顯微鏡觀測](../Page/顯微鏡.md "wikilink")\[1\]。因為DAPI可以透過完整的[細胞膜](../Page/細胞膜.md "wikilink")，它可以用于活[細胞和固定細胞的](../Page/細胞.md "wikilink")[染色](../Page/染色_\(生物學\).md "wikilink")。

## 历史

1971年DAPI在一项关于制抗锥虫病的药物的研究中第一次被合成于Otto
Dann的实验室。虽然它不是一种成功的药物，但更深入的研究表明它在与DNA强力结合时发出更强的荧光，因此在1975年它被用在[超速离心分析法中以标记](../Page/离心机.md "wikilink")[线粒体DNA](../Page/线粒体DNA.md "wikilink")。与DNA结合时激发的强荧光使它被广泛用于[荧光显微镜技术中对DNA的染色](../Page/荧光显微镜.md "wikilink")。上世纪70年代末证实DAPI可以被用来在[植物](../Page/植物.md "wikilink")，[微生物](../Page/微生物.md "wikilink")，[多细胞动物和](../Page/多细胞动物.md "wikilink")[细菌细胞中追踪DNA](../Page/细菌.md "wikilink")，1977年证实它可以被用来定量给细胞中的DNA染色，同时也证实DAPI可在[流式细胞术中用作DNA染料](../Page/流式细胞术.md "wikilink")\[2\]

## 荧光属性

當DAPI與雙股DNA結合時,在[螢光顯微鏡觀察下](../Page/螢光顯微鏡.md "wikilink")，DAPI染劑在358 nm
([紫外线](../Page/紫外线.md "wikilink")) 处有一个最大吸收峰，并在461 nm
([蓝](../Page/蓝.md "wikilink"))处有一个最大发射峰，其發射光的波長範圍含蓋了藍色至青綠色，因此在荧光显微技术中DAPI被[紫外线照亮且被蓝或青色滤镜检出](../Page/紫外线.md "wikilink")。DAPI也可以和[RNA結合](../Page/RNA.md "wikilink")，但產生的螢光強度不及與DNA結合的結果，其發射光的波長範圍約在400nm左右。\[3\]\[4\]
\[5\]

DAPI的發射光為藍色，且DAPI和[荧光素](../Page/荧光素.md "wikilink")、[綠色螢光蛋白](../Page/綠色螢光蛋白.md "wikilink")（Green
fluorescent protein, GFP）或Texas
Red染劑（紅色螢光染劑）的發射波長，僅有少部分重疊，研究員可以善用這項特性在單一的樣品上進行多重螢光染色。如果需要十分精确的分析，用光谱去混合(spectral
unmixing)可以度量这一影响
[1D30_DNA_DAPI.png](https://zh.wikipedia.org/wiki/File:1D30_DNA_DAPI.png "fig:1D30_DNA_DAPI.png")

Outside of analytical fluorescence light microscopy DAPI is also popular
for labeling of [cell cultures](../Page/cell_cultures.md "wikilink") to
detect the DNA of contaminating
[mycoplasma](../Page/mycoplasma.md "wikilink") or
[virus](../Page/virus.md "wikilink"). The labelled mycoplasma or virus
particles in the [growth medium](../Page/growth_medium.md "wikilink")
fluoresce once stained by DAPI making them easy to detect.\[6\]

## 吸收模型和荧光属性

这一DNA最近被有效地刻画\[7\] This DNA fluorescent probe has recently been
effectively modeled \[8\]using the Time-dependent density functional
theory, coupled with the IEF version of the Polarizable continuum model.
This quantum-mechanical modeling has rationalized the absorption and
fluorescence behavior given by minor groove binding and intercalation in
the DNA pocket, in term of a reduced structural flexibility and
polarization.

## 活细胞毒性

DAPI可用于固定细胞染色，但是因为活细胞染色对DAPI浓度有严格要求，它很少被用于活细胞染色。\[9\]DAPI能快速進入活細胞中與DNA結合，因此DAPI對生物體而言，也被視為一種毒性物質與[致癌物](../Page/致癌物.md "wikilink")。然而在[MSDS中它被标记为无毒](../Page/材料安全性数据表.md "wikilink")。\[10\]尽管DAPI没有显现出对[E.coli的诱变性](../Page/E.coli.md "wikilink")，\[11\]它在机器制造商提供的信息上被认为是一种DNA诱变剂\[12\]。由于DAPI可以掺入DNA螺旋中，它很可能具有底层的DNA诱变性，在使用過程中應注意配制、使用與拋棄的處理程序。

## 替代

[FluorescentCells.jpg](https://zh.wikipedia.org/wiki/File:FluorescentCells.jpg "fig:FluorescentCells.jpg")(红)染色且使用[免疫荧光法通过](../Page/免疫荧光法.md "wikilink")[异硫氰酸荧光素结合](../Page/异硫氰酸荧光素.md "wikilink")[抗体](../Page/抗体.md "wikilink")(FITC)(绿)的内皮细胞\]\]

类似于DAPI技术，[赫斯特染色是一种既可以用于活细胞也可以用于固定细胞的蓝色荧光染料](../Page/赫斯特染色.md "wikilink")，并且可以使用与DAPI相同的机器滤镜设置

## 参见

[DAPI](https://en.wikipedia.org/wiki/DAPI)

## 參考資料

[Category:染色着色剂](../Category/染色着色剂.md "wikilink")
[Category:荧光染料](../Category/荧光染料.md "wikilink")
[Category:DNA结合物质](../Category/DNA结合物质.md "wikilink")
[Category:吲哚](../Category/吲哚.md "wikilink")
[Category:脒](../Category/脒.md "wikilink")

1.

2.

3.  Scott Prahl,
    [DAPI](http://omlc.ogi.edu/spectra/PhotochemCAD/html/dapi\(H2O\).html).
    accessed 2009-12-08.

4.  Kapuscinski J., [1](https://www.ncbi.nlm.nih.gov/pubmed/1696951).
    accessed 2013-02-25.

5.  Invitrogen, [DAPI Nucleic Acid
    Stain](http://probes.invitrogen.com/media/pis/mp01306.pdf) .
    accessed 2009-12-08.

6.

7.

8.
9.

10. <http://www.kpl.com/docs/msds/710301.pdf>

11.

12. Invitrogen, [DAPI Nucleic Acid
    Stain](http://probes.invitrogen.com/media/pis/mp01306.pdf) .
    accessed 2009-12-08.