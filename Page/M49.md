**M49**（也稱為NGC
4472）是一個位於室女座的[橢圓星系](../Page/橢圓星系.md "wikilink")／[透鏡狀星系](../Page/透鏡狀星系.md "wikilink")，距離地球大約有5300萬光年遠，是由[查爾斯·梅西耶於](../Page/查爾斯·梅西耶.md "wikilink")1771年所發現。M49是室女座星系團中最明亮的星系之一，目前在M49內只有發現一顆超新星（[SN
1969Q](../Page/SN_1969Q.md "wikilink")）。

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [**SEDS**: Elliptical Galaxy
    M49](https://web.archive.org/web/20071120101544/http://seds.org/MESSIER/m/m049.html)
  - [WIKISKY.ORG SDSS image of
    M49](http://www.wikisky.org/?object=M49&img_source=SDSS&zoom=8)
  - [Black hole found in a star cluster in
    M49](http://www.msnbc.msn.com/id/16456987/)

[Category:橢圓星系](../Category/橢圓星系.md "wikilink")
[Category:室女座星系團](../Category/室女座星系團.md "wikilink")
[Category:室女座](../Category/室女座.md "wikilink")
[049](../Category/梅西耶天體.md "wikilink")
[Category:室女座NGC天體](../Category/室女座NGC天體.md "wikilink")
[134](../Category/阿普天體.md "wikilink")
[Category:1771年發現的天體](../Category/1771年發現的天體.md "wikilink")