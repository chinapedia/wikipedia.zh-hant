**恒生中國H股金融行業指數**，簡稱**H股金融指數**，由[恒生銀行屬下](../Page/恒生銀行.md "wikilink")[恒生指數有限公司負責計算](../Page/恒生指數有限公司.md "wikilink")，於2006年11月27日推出，反映在[香港交易所上市的中國大型金融股的表現](../Page/香港交易所.md "wikilink")。指數採用流通調整市值加權法計算，每隻股票的比重上限設定為15%。

這個指數的設立時間正值2006年香港中資金融股熱潮，[國企](../Page/國企股.md "wikilink")[銀行股和](../Page/銀行.md "wikilink")[保險股被熱炒](../Page/保險.md "wikilink")，恒指服務有限公司因而設立指數供投資者及[基金參考](../Page/基金.md "wikilink")。

恒生中國H股金融行業指數基準日為2004年3月5日，當時指數為5033.14點，但指數正式公佈前已超過10000點。

## 恒生中國H股金融行業指數成份股

  - 0939 [建設銀行](../Page/建設銀行.md "wikilink")
  - 0998 [中信銀行](../Page/中信銀行.md "wikilink")
  - 1216 [中原銀行](../Page/中原銀行.md "wikilink")
  - 1288 [農業銀行](../Page/中国农业银行.md "wikilink")
  - 1336 [新華保險](../Page/新華保險.md "wikilink")
  - 1339 [中國人民保險集團](../Page/中國人民保險集團.md "wikilink")
  - 1359 [中國信達](../Page/中國信達.md "wikilink")
  - 1375 [中州證券](../Page/中州證券.md "wikilink")
  - 1398 [工商銀行](../Page/中国工商银行.md "wikilink")
  - 1456 [國聯證券](../Page/國聯證券.md "wikilink")
  - 1461 [魯証期貨](../Page/魯証期貨.md "wikilink")
  - 1508 [中國再保險](../Page/中國再保險.md "wikilink")
  - 1551 [廣州農商銀行](../Page/廣州農商銀行.md "wikilink")
  - 1577 [匯鑫小貸](../Page/匯鑫小貸.md "wikilink")
  - 1658 [郵儲銀行](../Page/中國郵政儲蓄銀行.md "wikilink")
  - 1697 [山東國信](../Page/山東國信.md "wikilink")

<!-- end list -->

  - 1776 [廣發証券](../Page/廣發証券.md "wikilink")
  - 1963 [重慶銀行](../Page/重慶銀行.md "wikilink")
  - 1988 [民生銀行](../Page/民生銀行.md "wikilink")
  - 2016 [浙商銀行](../Page/浙商銀行.md "wikilink")
  - 2066 [盛京銀行](../Page/盛京銀行.md "wikilink")
  - 2318 [中國平安](../Page/中國平安.md "wikilink")
  - 2328 [中國財險](../Page/中国人民财产保险.md "wikilink")
  - 2601 [中國太保](../Page/中國太保.md "wikilink")
  - 2611 [國泰君安](../Page/國泰君安.md "wikilink")
  - 2628 [中國人壽](../Page/中国人寿保险.md "wikilink")
  - 2799 [中國華融](../Page/中國華融.md "wikilink")
  - 3328 [交通銀行](../Page/交通銀行.md "wikilink")
  - 3618 [重慶農村商業銀行](../Page/重慶農村商業銀行.md "wikilink")
  - 3698 [徽商銀行](../Page/徽商銀行.md "wikilink")
  - 3903 [瀚華金控](../Page/瀚華金控.md "wikilink")
  - 3908 [中金公司](../Page/中金公司.md "wikilink")

<!-- end list -->

  - 3958 [東方證券](../Page/東方證券.md "wikilink")
  - 3968 [招商銀行](../Page/招商銀行.md "wikilink")
  - 3988 [中國銀行](../Page/中國銀行.md "wikilink")
  - 6030 [中信證券](../Page/中信證券.md "wikilink")
  - 6060 [眾安在綫](../Page/眾安在綫.md "wikilink")
  - 6066 [中信建投証券](../Page/中信建投証券.md "wikilink")
  - 6099 [招商證券](../Page/招商證券.md "wikilink")
  - 6122 [九台農商銀行](../Page/九台農商銀行.md "wikilink")
  - 6138 [哈爾濱銀行](../Page/哈爾濱銀行.md "wikilink")
  - 6178 [光大證券](../Page/光大證券.md "wikilink")
  - 6818 [中國光大銀行](../Page/中國光大銀行.md "wikilink")
  - 6837 [海通證券](../Page/海通證券.md "wikilink")
  - 6866 [佐力小貸](../Page/佐力小貸.md "wikilink")
  - 6881 [中國銀河](../Page/中國銀河.md "wikilink")
  - 6886 [華泰證券](../Page/華泰證券.md "wikilink")

以上資料截至2017年9月4日

以往恒生中國H股金融行業指數成份股曾被傳媒和金融界人士統稱為「[七行四保](../Page/七行四保.md "wikilink")」，但隨着越來越多中國金融企業來港上市，使七行四保這名稱平淡化。

### 歷來變動

<table>
<thead>
<tr class="header">
<th><p>生效日期</p></th>
<th><p>加入</p></th>
<th><p>刪除</p></th>
<th><p>成份股數量</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2018年3月5日</p></td>
<td><p>01216 中原銀行<br />
01697 山東國信<br />
06060 眾安在綫</p></td>
<td><p>00416 <a href="../Page/錦州銀行.md" title="wikilink">錦州銀行</a><br />
06196 <a href="../Page/鄭州銀行.md" title="wikilink">鄭州銀行</a></p></td>
<td><p>47</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年9月4日</p></td>
<td><p>01551 廣州農商銀行<br />
02611 國泰君安<br />
06122 九台農商銀行</p></td>
<td><p>01606 <a href="../Page/國銀租賃.md" title="wikilink">國銀租賃</a></p></td>
<td><p>46</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年3月6日</p></td>
<td><p>01577 匯鑫小貸<br />
01606 國銀租賃<br />
01658 郵儲銀行<br />
03958 東方證券<br />
06066 中信建投証券<br />
06099 招商證券<br />
06178 光大證券</p></td>
<td><p>01476 <a href="../Page/恒投證券.md" title="wikilink">恒投證券</a><br />
03678 <a href="../Page/弘業期貨.md" title="wikilink">弘業期貨</a></p></td>
<td><p>44</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年9月5日</p></td>
<td><p>02016 浙商銀行</p></td>
<td><p>01543 <a href="../Page/中盈盛達融資擔保.md" title="wikilink">中盈盛達融資擔保</a><br />
03866 <a href="../Page/青島銀行.md" title="wikilink">青島銀行</a></p></td>
<td><p>39</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年3月14日</p></td>
<td><p>00416 錦州銀行<br />
01456 國聯證券<br />
01461 魯証期貨<br />
01476 恒投證券<br />
01508 中國再保險<br />
01543 中盈盛達融資擔保<br />
02799 中國華融<br />
03678 弘業期貨<br />
03866 青島銀行<br />
03908 中金公司<br />
06196 鄭州銀行</p></td>
<td></td>
<td><p>40</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年9月14日</p></td>
<td><p>01766 廣發証券<br />
03903 瀚華金控<br />
06866 佐力小貸<br />
06886 華泰證券</p></td>
<td></td>
<td><p>29</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年3月9日</p></td>
<td><p>02066 盛京銀行</p></td>
<td><p>03903 <a href="../Page/瀚華金控.md" title="wikilink">瀚華金控</a></p></td>
<td><p>25</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年9月8日</p></td>
<td><p>01375 中州證券<br />
03903 瀚華金控<br />
06138 哈爾濱銀行</p></td>
<td></td>
<td><p>25</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年3月10日</p></td>
<td><p>01359 中國信達<br />
01963 重慶銀行<br />
03698 徽商銀行<br />
06818 中國光大銀行</p></td>
<td></td>
<td><p>22</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年9月9日</p></td>
<td><p>06881 中國銀河</p></td>
<td></td>
<td><p>18</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年3月4日</p></td>
<td><p>01339 中國人民保險集團</p></td>
<td></td>
<td><p>17</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年9月10日</p></td>
<td><p>06837 海通證券</p></td>
<td></td>
<td><p>16</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年3月5日</p></td>
<td><p>01336 新華保險<br />
06030 中信証券</p></td>
<td></td>
<td><p>15</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年3月7日</p></td>
<td><p>01288 農業銀行<br />
03618 重慶農村商業銀行</p></td>
<td></td>
<td><p>13</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年3月8日</p></td>
<td><p>01988 民生銀行<br />
02601 中國太保</p></td>
<td></td>
<td><p>11</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年9月10日</p></td>
<td><p>00998 中信銀行</p></td>
<td></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年11月27日</p></td>
<td><p>00939 建設銀行<br />
01398 工商銀行<br />
02318 中國平安<br />
02328 中國財險<br />
02628 中國人壽<br />
03328 交通銀行<br />
03968 招商銀行<br />
03988 中國銀行</p></td>
<td></td>
<td><p>8</p></td>
<td><p>本指數推出</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [恒指服務公司對恒生中國H股金融行業指數的介紹](https://web.archive.org/web/20070108011644/http://main.hsi.com.hk/hsicom/hcorner/chi/hfin.html)
  - [最新指數及各成份股對指數的影響](https://web.archive.org/web/20070104023505/http://www.hsi.com.hk/c_family/hshfi_c.html)
  - [香港股票即市報價](https://web.archive.org/web/20070901054951/http://hkstock.selfip.com/)

[Category:恒生指數系列](../Category/恒生指數系列.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:上市公司列表](../Category/上市公司列表.md "wikilink")