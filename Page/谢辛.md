**谢辛亲王**（；），[柬埔寨著名](../Page/柬埔寨.md "wikilink")[政治家](../Page/政治家.md "wikilink")，[人民党领袖](../Page/柬埔寨人民党.md "wikilink")；曾任柬埔寨国民议会议长。

## 生平

谢辛为[柴桢省](../Page/柴桢省.md "wikilink")（Svay Rieng）罗梅县（Romeas
Hek）人，祖籍[中國](../Page/中國.md "wikilink")[廣東](../Page/廣東.md "wikilink")[潮州](../Page/潮州.md "wikilink")。

## 政治生涯

1952年参加抗法运动。1971年任[柬埔寨共产党博涅克列县委员会书记](../Page/柬埔寨共产党.md "wikilink")。1976年当选为[民主柬埔寨人民代表大会常务委员会委员](../Page/民主柬埔寨.md "wikilink")，柬埔寨共产党东部大区第二十区党委书记。1978年同[韩桑林](../Page/韩桑林.md "wikilink")、[洪森逃至柬越边境组织反](../Page/洪森.md "wikilink")[红色高棉武装力量](../Page/红色高棉.md "wikilink")。1981年起任柬埔寨人民共和国国民议会议长，1991年10月任人民党主席；1993年7月任柬埔寨制宪议会副主席；1993年10月至1998年10月任柬埔寨王国国民议会议长；1999年3月至今任[柬埔寨](../Page/柬埔寨.md "wikilink")[参议院议长](../Page/参议院.md "wikilink")。

谢辛曾多次临时受任代理[国家元首或过度](../Page/国家元首.md "wikilink")[政府首脑](../Page/政府首脑.md "wikilink")。柬埔寨恢复[君主立宪制以前](../Page/君主立宪制.md "wikilink")，1992年4月6日－1993年6月14日任过渡政府的國家元首。1993年、1994年、1995年和2004年在[诺罗敦·西哈努克国王治病时曾多次临时代理国家元首](../Page/诺罗敦·西哈努克.md "wikilink")。最近的一次代理国家元首为2004年10月6日至10月14日即西哈努克正式退位，直至[诺罗敦·西哈莫尼任新国王](../Page/诺罗敦·西哈莫尼.md "wikilink")。谢辛因在柬埔寨政界的特殊地位和对国家的贡献，1993年被西哈努克国王[授封](../Page/封号.md "wikilink")“亲王”。

谢辛于2015年6月8日在柬埔寨[金边逝世](../Page/金边.md "wikilink")，享年82岁。\[1\]

## 相关

  - [韩桑林](../Page/韩桑林.md "wikilink")
  - [洪森](../Page/洪森.md "wikilink")
  - [苏庆](../Page/苏庆.md "wikilink")
  - [杨启秋](../Page/杨启秋.md "wikilink")
  - [曹云德](../Page/曹云德.md "wikilink")

## 參考资料

|-    |-    |-    |-    |-     |-

[C](../Category/柬埔寨政治人物.md "wikilink")
[C](../Category/柬埔寨華人.md "wikilink")

1.  [Cambodian People’s Party president Chea Sim dies
    aged 82](http://asiancorrespondent.com/133412/cambodian-peoples-party-president-chea-sim-dies-aged-82/)