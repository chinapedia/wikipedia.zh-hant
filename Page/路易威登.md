[HK_CH_LV_Landmark_60421.jpg](https://zh.wikipedia.org/wiki/File:HK_CH_LV_Landmark_60421.jpg "fig:HK_CH_LV_Landmark_60421.jpg")

**Louis Vuitton Malletier**（，發音：\[lwi
vɥiˈtɔ̃\]\[1\]，通常缩写为**LV**,是法国的一个时尚品牌。由[路易·威登于](../Page/路易·威登.md "wikilink")1854年在[巴黎创立](../Page/巴黎.md "wikilink")。一个世纪之后，路易威登成為皮箱與皮件領域數一數二的品牌，并且如今路易·威登这一品牌已经不仅限于设计和出售高档皮具和箱包，而是成为涉足[时装](../Page/时装.md "wikilink")、[首飾](../Page/首飾.md "wikilink")、[太陽眼鏡](../Page/太陽眼鏡.md "wikilink")、[皮鞋](../Page/皮鞋.md "wikilink")、[行李箱](../Page/行李箱.md "wikilink")、[手提包](../Page/手提包.md "wikilink")、[珠寶](../Page/珠寶.md "wikilink")、[手錶](../Page/手錶.md "wikilink")、[腕錶](../Page/腕錶.md "wikilink")、[名酒](../Page/名酒.md "wikilink")、[化妝品](../Page/化妝品.md "wikilink")、[香水](../Page/香水.md "wikilink")、[書籍等领域](../Page/書籍.md "wikilink")，以其品牌名字的兩個字母首字**LV**當[奢侈品](../Page/奢侈品.md "wikilink")[標示聞名於世](../Page/標示.md "wikilink")。2017年6月6日，《2017年BrandZ最具价值全球品牌100强》公布，路易威登名列第29位。其總部設在法國巴黎[香榭麗舍大道上](../Page/香榭麗舍大道.md "wikilink")。

## 历史

### 早期 (1854-1892)

“路易威登”作为一个品牌，迎来巨大成功则是在其创始人离世之后。尽管在[路易·威登在世的时候](../Page/路易·威登.md "wikilink")，这一品牌的发展已经富有传奇色彩。路易·威登在世的时候曾经设计并推出过一款皮箱，名为“Gris
Trianon”。这款皮箱重量很轻，外表以灰色[帆布装饰](../Page/帆布.md "wikilink")。这款皮箱推出之后，很快受到[拿破仑三世的妻子](../Page/拿破仑三世.md "wikilink")[欧仁妮皇后的青睐](../Page/欧仁妮皇后.md "wikilink")。1854年，路易·威登娶了同为33岁的艾米莉·克勒芒斯·巴利沃为妻。三年之后，路易和艾米莉的儿子[乔治·威登出生](../Page/乔治·威登.md "wikilink")。
1860年，为了适应越来越强烈的市场需求，路易将他的工厂由[巴黎迁移至](../Page/巴黎.md "wikilink")[阿斯涅尔](../Page/阿斯涅尔.md "wikilink")。1867年，路易·威登在[世界博览会上赢得一枚铜质奖章](../Page/世界博览会.md "wikilink")，这使得原本就已经受欢迎的“路易·威登”品牌更加声名大噪，并且其影响力开始超出[法国](../Page/法国.md "wikilink")，成为国际知名品牌。1869年，[埃及帕夏](../Page/埃及.md "wikilink")（总督）伊斯梅尔订购了一套路易·威登的皮箱；1877年，[俄国皇储尼古拉也订购了一套](../Page/俄国.md "wikilink")；[西班牙国王](../Page/西班牙.md "wikilink")[阿尔封斯十二世也曾专门订购路易](../Page/阿尔封斯十二世.md "wikilink")·威登的皮箱。

1871年，路易·威登再次迁移至斯格里布大街1号。路易·威登将帆布用于装饰皮箱表面是非常高明的，因为它可以有效的保护箱内物品不被偷窃，然而这种设计风格也非常容易被人仿效和剽窃。1872年，路易·威登开始大规模改进这种风格，包括自创了一种风格独特的帆布。这种新的[帆布的颜色为褐红相间](../Page/帆布.md "wikilink")，被称为“有斑纹的帆布”。这一设计再次获得巨大成功，然而很快又再次被人剽窃，[赝品大行其道](../Page/赝品.md "wikilink")。

路易·威登的皮具仍然销售火爆。在事业节节高升的同时，路易·威登的家庭也发生了许多变化。1880年，他的儿子乔治和约瑟芬·帕特莱尔结婚。在他们的婚礼当天，老路易将位于斯格里布大街的总店正式移交给乔治。三年之后，乔治的儿子[贾斯通-路易·威登出生](../Page/贾斯通-路易·威登.md "wikilink")。

1885年，路易·威登在[伦敦开了第一家海外分店](../Page/伦敦.md "wikilink")。1888年，乔治·威登推出威登皮箱的新设计。他将箱子的表面设计成[西洋跳棋棋盘风格](../Page/西洋跳棋.md "wikilink")，颜色则是棕色和栗色相间。此外，在皮箱上还印有“路易·威登品牌验证”的标示。即使如此，这一新设计还是被人大量仿制。在1889年的[巴黎万国博览会上](../Page/1889年世界博覽會.md "wikilink")，这一产品为路易·威登公司赢得了金奖。

1892年，路易·威登开始推出手提包产品，並发行了第一份包含皮箱、手提包和床具的产品名录。同年2月27日，老路易在家中去世，他的儿子乔治正式成为路易·威登公司继承人。

### 黄金时期 (1893-1936)

老路易去世之后，乔治将路易·威登打造为一个享誉世界的品牌。1893年，乔治在美国[芝加哥的世界博览会上展示了路易](../Page/芝加哥.md "wikilink")·威登的产品，从此这一品牌正式登陆[美国](../Page/美国.md "wikilink")。此后，乔治一直致力于提高品牌的国际知名度。1894年，他出版了新书《旅程》。

1896年，路易·威登正式推出了具有品牌标示功能的帆布设计，被称为“字母组合帆布”，因为这款帆布在风格上应用了大量具有象征含义的符号以及路易·威登的标示“LV”。随后，乔治再次远赴美国，游历纽约、费城和芝加哥等大城市，推行路易·威登品牌。

在世纪之交的1900年，乔治被授权组织和设计[巴黎世界博览会的](../Page/巴黎.md "wikilink")“旅行用品及皮具”板块。1901年，路易·威登公司正式推出“汽船手提包”，这是一款体积非常小巧的手提包，可以置于路易·威登皮箱之中。

1904年，乔治担任[圣路易世界博览会主席一职](../Page/圣路易.md "wikilink")。同年，路易·威登公司推出一系列新的皮箱设计，增加了皮箱内部的储物单元，用于放置[香水](../Page/香水.md "wikilink")、服装以及其他物品。

1906年，乔治的儿子贾斯通-路易迎娶芮妮·维尔赛。同年，路易·威登推出车用皮箱。

1914年，位于[香榭丽舍大街的路易](../Page/香榭丽舍大街.md "wikilink")·威登大厦竣工。当时，这是全世界最大的皮具销售中心。在[一战爆发之前](../Page/一战.md "wikilink")，路易·威登在[纽约](../Page/纽约.md "wikilink")、[孟买](../Page/孟买.md "wikilink")、[华盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")、[伦敦](../Page/伦敦.md "wikilink")、[亚历山大](../Page/亚历山大港.md "wikilink")、[布宜诺斯艾利斯等地的分公司纷纷开张](../Page/布宜诺斯艾利斯.md "wikilink")。

1924年，名为Keepall的新箱包正式面世。这款箱包引领了轻型防水旅行式箱包的时尚。它适合短途旅行，因为只能存放少量的必要物品。

1929年，即品牌诞生75周年，路易·威登推出一款化妆包，并以“献给歌剧演员[玛瑟·舍纳尔](../Page/玛瑟·舍纳尔.md "wikilink")”为名面世。这款设计主要用于存放女性化妆用品，包括[香水](../Page/香水.md "wikilink")、镜子、粉盒等物品。

1931年，路易·威登开始推出具有异域色彩的设计，包括[鳄鱼皮手包](../Page/鳄鱼.md "wikilink")、[大象皮手包等](../Page/大象.md "wikilink")。这些设计在殖民展览会上大放异彩。

1932年，路易·威登推出“Nóe
bag”。这款箱包的主要消费者是[香槟酒的酿造者](../Page/香槟酒.md "wikilink")，用于装载和保存[香槟酒](../Page/香槟酒.md "wikilink")。

1933年，路易·威登运动型箱包问世。

1936年，[乔治·威登去世](../Page/乔治·威登.md "wikilink")，标誌着路易·威登高速发展的黄金时代结束。乔治·威登总共设计并推出了超过700款新产品，绝大部分都取得了成功。乔治去世后，他的儿子贾斯通-路易·威登继承了家业。

[Louis_Vuitton_The_Landmark_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Louis_Vuitton_The_Landmark_Hong_Kong.jpg "fig:Louis_Vuitton_The_Landmark_Hong_Kong.jpg")[中環](../Page/中環.md "wikilink")[置地廣場的專賣店](../Page/置地廣場.md "wikilink")\]\]
[HK_Museum_of_Art_TST_Fondation_LV_Passion_Creation_3.JPG](https://zh.wikipedia.org/wiki/File:HK_Museum_of_Art_TST_Fondation_LV_Passion_Creation_3.JPG "fig:HK_Museum_of_Art_TST_Fondation_LV_Passion_Creation_3.JPG")作[當代藝術展覽](../Page/當代藝術.md "wikilink")\]\]

["_(_Louis_Vuitton_Fashion_boutique,_MADRID_Spain_2016,_antique,_on_display,_real,_antique_Louis_Vuitton_Louis_Vuitton_was_a_French_box-maker_and_packer_who_founded_the_luxury_brand_of_the_same_name_over_150_years_ago.jpg](https://zh.wikipedia.org/wiki/File:"_\(_Louis_Vuitton_Fashion_boutique,_MADRID_Spain_2016,_antique,_on_display,_real,_antique_Louis_Vuitton_Louis_Vuitton_was_a_French_box-maker_and_packer_who_founded_the_luxury_brand_of_the_same_name_over_150_years_ago.jpg "fig:\"_(_Louis_Vuitton_Fashion_boutique,_MADRID_Spain_2016,_antique,_on_display,_real,_antique_Louis_Vuitton_Louis_Vuitton_was_a_French_box-maker_and_packer_who_founded_the_luxury_brand_of_the_same_name_over_150_years_ago.jpg")

## 二戰後

1946年，为了抵制第二次世界大戰後经济萧条的局面，路易·威登开始超越箱包设计一行，向其他领域投资。

1951年，路易·威登为[法国总统](../Page/法国.md "wikilink")[奥里奥尔的](../Page/奥里奥尔.md "wikilink")[美国之行提供了全部旅行物品](../Page/美国.md "wikilink")。

1959年，帆布工业技术的改革为箱包生产提供了更加丰富的原料，路易·威登再次推出一系列“字母组合”风格的帆布箱包。

从1959年到1965年，每年都有25款，总计175款新产品问世。

1968年，路易·威登在[日本](../Page/日本.md "wikilink")[东京设立销售中心](../Page/东京.md "wikilink")。

1969年，[贾斯通-路易·威登去世](../Page/贾斯通-路易·威登.md "wikilink")。

1977年，路易·威登在[沙特阿拉伯开设分公司](../Page/沙特阿拉伯.md "wikilink")。

1978年，路易·威登在日本的[东京和](../Page/东京.md "wikilink")[大阪开设分店](../Page/大阪市.md "wikilink")。

1983年，路易·威登与[美洲杯划艇比赛合作](../Page/美洲杯.md "wikilink")，创立了“路易·威登杯划艇赛”。在路易·威登杯划艇赛中，经过淘汰赛而胜出的选手与前一年美洲杯的冠军选手比赛定胜负。同年路易·威登在[臺北市開設](../Page/臺北市.md "wikilink")[臺灣第一間分店](../Page/臺灣.md "wikilink")。

1984年，路易·威登在[首爾开设分店](../Page/首爾.md "wikilink")。

1985年，“Epi皮具”系列产品问世。

1987年，路易·威登公司和大名鼎鼎的[轩尼诗公司合并组成](../Page/轩尼诗.md "wikilink")“[酩悅軒尼詩路易威登集团](../Page/酩悅軒尼詩路易威登集团.md "wikilink")”（Moët
Hennessy - Louis Vuitton, LVMH Group），正式成为跨越名包、名酒的奢华时尚品牌。

1988年，冠名路易·威登的年度汽车锦标赛在[巴黎举行](../Page/巴黎.md "wikilink")。

1992年，路易·威登在[北京开设第一家分店](../Page/北京.md "wikilink")。

1993年，LVMH推出“Taiga”系列箱包，为黑色真皮设计，主要针对男性消费者。

1996年是“字母组合”系列帆布箱包诞生一百周年纪念，LVMH开始为自己旗下风格各异的设计师大力宣传。

1997年，LVMH推出了一系列新的[钢笔设计](../Page/钢笔.md "wikilink")。

1998年，美国著名设计师[马克·雅各布斯加盟路易](../Page/马克·雅各布斯.md "wikilink")·威登，他主持设计了“字母组合清漆系列”皮包。同年，LVMH推出了自己编写的全球主要城市旅行手册。

2001年，路易·威登推出“涂鸦系列”箱包，其著名的“魅力手镯”也于同年面世。

2002年，LVMH正式推出一系列[手表设计](../Page/手表.md "wikilink")。

2002年，在澳門的[澳門文華東方酒店開設澳門首間專門店](../Page/金麗華酒店.md "wikilink")。

2003年，[日本著名画家](../Page/日本.md "wikilink")[村上隆和路易](../Page/村上隆.md "wikilink")·威登的首席设计师马克·雅各布斯合作推出了限量珍藏版的“樱桃”、“樱花”系列，以色彩绚烂而著称。

2004年，路易·威登在世界四大城市巴黎、[紐約](../Page/紐約.md "wikilink")、[東京及](../Page/東京.md "wikilink")[香港慶祝成立](../Page/香港.md "wikilink")150周年。

2005年，巴黎旗艦店在重新裝修後，於10月9日傍晚6時重新開幕，並於於同日於[小皇宮](../Page/小皇宮.md "wikilink")（[Petit
Palais](../Page/小皇宮.md "wikilink")）舉行[時裝表演](../Page/時裝表演.md "wikilink")，亦有開幕慶祝派對，邀請世界各地的嘉賓參加。

2006年9月5日，在澳門的[永利澳門開設澳門第二間專門店](../Page/永利澳門.md "wikilink")。

2008年8月28日，在澳門的[四季酒店 （澳門）開設澳門第三間專門店](../Page/四季酒店_（澳門）.md "wikilink")。

2009年12月1日，在澳門的[壹號廣場開設全球第八間旗艦店](../Page/壹號廣場.md "wikilink")。

2013年10月2日LVMH集團主席Bernard
Arnault證實與LV合作長達十六年的創意總監[馬克·雅各布斯Marc](../Page/馬克·雅各布斯.md "wikilink")
Jacobs10月約滿後將離任，專注發展其個人品牌，準備上市。

2018年，傳出Louis Vuitton聘請了一名能夠控制天氣的巴西巫師為時裝展驅雨的傳聞，亦傳出該巫師負責為上個月英國哈利王子與Meghan
Markle大婚施法，保持天氣良好\[2\]。
[Louis_Vuitton_or_shortened_to_LV,_is_a_French_fashion_house_founded_in_1854_by_Louis_Vuitton,_photography_by_david_adam_kess,_madrid_2016.jpg](https://zh.wikipedia.org/wiki/File:Louis_Vuitton_or_shortened_to_LV,_is_a_French_fashion_house_founded_in_1854_by_Louis_Vuitton,_photography_by_david_adam_kess,_madrid_2016.jpg "fig:Louis_Vuitton_or_shortened_to_LV,_is_a_French_fashion_house_founded_in_1854_by_Louis_Vuitton,_photography_by_david_adam_kess,_madrid_2016.jpg")
(2016)\]\]

2018年，傳出與創意總監Nicolas Ghesquière續約的消息\[3\]

2018年6月，LV推出首個由[維吉爾·阿伯拉赫入主後所推出的男裝設計系列](../Page/維吉爾·阿伯拉赫.md "wikilink")\[4\]。

2018年，Louis Vuitton
最新的秋冬系列以穿越時空的「時代錯亂」為設計主題，年初在巴黎羅浮宮博物館舉行女裝發布，同年7月起，Louis
Vuitton將於香港置地廣場開設為期一個月的香港首間女裝限定店，呈獻 2018 秋冬女裝系列\[5\]。

2018年，LVMH宣布上半年有機增長達12%(約218億歐羅)，淨收益升了41%，而當中最能賺錢的仍然是Louis Vuitton\[6\]。

2018年，Louis Vuitton推出新穎的陀螺型小手袋及外表如暖水袋的鏈帶斜揹袋\[7\]。

## 路易·威登产品

<File:HK> CH LV Landmark 60421 2.jpg <File:HK> CH LV Landmark 60421
1.jpg <File:HK> CH LV Landmark 60421 3.jpg

## 爭議和糾紛

### 香港版權

法國名牌LV（Louis
Vuitton）指稱多間小商戶「侵權」而發信索償事件，觸發網民發起「千人圍LV」活動，但2013年3月10日活動開始時僅得3人現身，包括兩名戴上「V煞面具」的男子，於廣東道LV旗艦店門外示威，呼籲市民罷買其產品，並批評LV與小商戶和解只為防止激起民憤，要求LV向公眾道歉。律師翁靜晶亦到場聲援，要求LV公開以往曾賠償的小商戶名單與涉及金額。
律師翁靜晶稱，過去曾協助3名客戶處理被LV索償的信件，其中一名新加坡客戶由於在報章上刊登展覽會抽獎廣告，列出二獎LV手袋的相片，也收到LV的信件，指其行為侵權，要求賠償數萬新加坡幣。翁認為LV在未判先定「罰款」做法不合理。

## 競爭品牌

  - [愛馬仕](../Page/愛馬仕.md "wikilink")
  - [香奈兒](../Page/香奈兒.md "wikilink")
  - [古馳](../Page/古馳.md "wikilink")

## 參考

## 外部链接

  - [Louis Vuitton官方網站](http://www.louisvuitton.com)

  -
[Category:法國服裝品牌](../Category/法國服裝品牌.md "wikilink")
[Category:法國服裝公司](../Category/法國服裝公司.md "wikilink")
[Category:高級時尚品牌](../Category/高級時尚品牌.md "wikilink")
[Category:奢侈品牌](../Category/奢侈品牌.md "wikilink")
[Category:时尚设计师](../Category/时尚设计师.md "wikilink")
[Category:法国企业家](../Category/法国企业家.md "wikilink")
[Category:行李背包皮箱品牌](../Category/行李背包皮箱品牌.md "wikilink")
[Category:LVMH時裝品牌](../Category/LVMH時裝品牌.md "wikilink")
[Category:手提包](../Category/手提包.md "wikilink")
[Category:1854年成立的公司](../Category/1854年成立的公司.md "wikilink")

1.  [Forvo網站上的Louis
    Vuitton的法語發音](http://zh.forvo.com/word/louis_vuitton/#fr)
2.  [傳Louis
    Vuitton重金禮聘巫師為早春騷驅雨…〡難道外國人比中國人更迷信？](https://mings.mpweekly.com/mings-fashion/20180608-82271)
3.  [不續約才是新聞吧〡就是Nicolas Ghesquière 的Louis
    Vuitton，讓品牌手袋起死回生](https://mings.mpweekly.com/daily/20180524-80980)
4.  [Virgil Abloh 首個 Louis Vuitton
    男裝設計系列發布在即，首位合作的造型師是何方神聖？](https://mings.mpweekly.com/mings-fashion/20180619-83841)
5.  [如果有穿越時空的機會，你會想回到哪個年代？｜Louis Vuitton
    於中環置地廣場首設女裝期間限定店](https://mings.mpweekly.com/mings-fashion/20180713-88397)
6.  [Louis Vuitton
    帶動，LVMH 2018上半年收益勁升 41%，下年會更強？](https://mings.mpweekly.com/mings-fashion/20180725-90237)
7.  [Louis Vuitton︰續約後 Ghesquière
    更顯未來主義才華｜巴黎春夏時裝周 2019](https://www.mings-fashion.com/louis-vuitton-%E6%99%82%E8%A3%9D%E5%91%A8-%E6%98%A5%E5%A4%8F-211983/)