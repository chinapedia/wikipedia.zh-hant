**[香港](../Page/香港.md "wikilink")**位於[中國南方](../Page/中國.md "wikilink")[珠江口的東南側](../Page/珠江.md "wikilink")，南臨[南海](../Page/南海.md "wikilink")，北邊與[廣東省](../Page/廣東省.md "wikilink")[深圳經濟特區接壤](../Page/深圳經濟特區.md "wikilink")。陸地面積為1,104[平方公里](../Page/平方公里.md "wikilink")，包括與大陸相連的[新界和](../Page/新界.md "wikilink")[九龍半島](../Page/九龍半島.md "wikilink")，以及[大嶼山島](../Page/大嶼山.md "wikilink")、[香港島等大小](../Page/香港島.md "wikilink")200多個[島嶼](../Page/島嶼.md "wikilink")。香港是典型的島海環境，擁有綿長的[海岸綫](../Page/海岸綫.md "wikilink")，地勢山巒起伏，近45%的陸地高出[海平面](../Page/海平面.md "wikilink")100[米以上](../Page/米.md "wikilink")，只有零星的[平原分布於沿海地帶](../Page/平原.md "wikilink")。

香港主要由[火成岩](../Page/火成岩.md "wikilink")、[沉積岩及](../Page/沉積岩.md "wikilink")[变质岩構成](../Page/变质岩.md "wikilink")，火成岩的出露面積最廣泛，包括[火山岩](../Page/喷出岩.md "wikilink")（50%）及[侵入岩](../Page/侵入岩.md "wikilink")（35%）約佔據香港面積85%，其中[花崗岩更佔香港總面積約三分之一](../Page/花崗岩.md "wikilink")\[1\]，主要分佈在[香港島](../Page/香港島.md "wikilink")、[九龍半島](../Page/九龍半島.md "wikilink")、[青山和](../Page/青山.md "wikilink")[大嶼山北部](../Page/大嶼山.md "wikilink")；沉積岩及变质岩分佈局限，主要集中在[新界北部地區及零星散佈在](../Page/新界.md "wikilink")[赤門海峽兩岸和](../Page/赤門海峽.md "wikilink")[大嶼山島的南端](../Page/大嶼山.md "wikilink")。

## 斷層

[中國南方的](../Page/中國.md "wikilink")[蓮花山斷裂帶主宰香港的地質結構](../Page/蓮花山斷裂帶.md "wikilink")，斷裂帶總體為東北-西南走向，由100多條大型[斷層構成](../Page/斷層.md "wikilink")。香港的地質走向同樣也是以大型的連續東北-西南斷層為主，以「赤門海峽-沙田-荔枝角斷裂帶」最具代表性；西北東南走向的斷層規模較小，當中以張性斷裂（tension
fault）為主，而且並不連貫。這樣一種構造格局控制著現今香港的山脈分布和基本地形。

## 地質演化

相對於45億歲的地球，[香港的岩石年紀尚輕](../Page/香港.md "wikilink")，境內缺失[中生代](../Page/中生代.md "wikilink")[三疊紀](../Page/三疊紀.md "wikilink")、早[古生代及更早時期的地層和岩石記錄](../Page/古生代.md "wikilink")，香港目前發現的最古老的岩石是4億年前[泥盆紀的沉積岩](../Page/泥盆紀.md "wikilink")。當時香港由大片河谷平原覆蓋，河水流向東南方，而河水氾濫帶來的軟泥則有利原始植物生長，海岸地區也將海洋生物的殘骸沉積到沿岸海洋軟泥之中。這些沖積物逐漸形成新界東北部[船灣](../Page/船灣.md "wikilink")、[黃竹角咀和](../Page/黃竹角咀.md "wikilink")[赤門北岸一帶的岩石](../Page/赤門.md "wikilink")。至3億5,000萬年前的[石炭紀早期](../Page/石炭紀.md "wikilink")，熱帶淺海淹沒香港，水面下降後的沿岸沼澤地區出現植物，軟泥地面則有枯萎的植物。這些物質經過掩埋、遇熱及擠壓，分別成為埋在地表下的[大理岩](../Page/大理岩.md "wikilink")，以及含有[石墨的](../Page/石墨.md "wikilink")[片岩](../Page/片岩.md "wikilink")，分布於香港西北部。2億9,000萬年前的[二疊紀時期](../Page/二疊紀.md "wikilink")，香港再次被淺海所淹，沉積物混入砂礫、海洋動物及珊瑚的殘骸，經過沉埋、壓迫，岩石被巨大的地殼力量扭曲及變形，形成摺曲、拱起或斷層的狀態。這類岩石分布於[吐露港的](../Page/吐露港.md "wikilink")[馬屎洲和](../Page/馬屎洲.md "wikilink")[丫洲](../Page/丫洲.md "wikilink")。

[Tung_Ping_Chau_4.jpg](https://zh.wikipedia.org/wiki/File:Tung_Ping_Chau_4.jpg "fig:Tung_Ping_Chau_4.jpg")海蝕平臺，約於8,000萬至5,000萬年前逐漸形成\]\]
1億6,400萬至1億4,000萬年前的[侏羅紀時期](../Page/侏羅紀.md "wikilink")，香港發生多次火山爆炸，噴出大量熔岩及火山灰。香港的岩石約有一半由[火山岩所形成](../Page/火山岩.md "wikilink")。1億4,000萬年前，[糧船灣超級火山爆發](../Page/糧船灣超級火山.md "wikilink")，噴至火山口邊的火山灰冷卻凝固後，成為切面是六邊形的石柱，可見於[果洲群島及](../Page/果洲群島.md "wikilink")[破邊洲](../Page/破邊洲.md "wikilink")。而未有噴出的熔岩則在地底深處慢慢冷卻凝固，形成藏有粗大礦物顆粒的[花崗岩](../Page/花崗岩.md "wikilink")。1億年前白堊紀的香港則是一片乾燥荒蕪，中國大陸的洪流偶爾會把砂石和軟泥沖積到香港，形成紅色的岩石層，見於東北部的[赤洲和](../Page/赤洲.md "wikilink")[烏蛟騰](../Page/烏蛟騰.md "wikilink")。考古學家發現這個時期的岩石並無生物痕跡，但廣東省內發現的白堊紀[恐龍](../Page/恐龍.md "wikilink")[化石則反映鄰近地區可能有恐龍出沒](../Page/化石.md "wikilink")。

直至8,000萬至5,000萬年前的白堊紀晚期和[第三紀早期](../Page/第三紀.md "wikilink")，氣候變得潮濕，香港逐漸形成遼闊的淺湖。雨季和旱季循環交替，為湖底的軟泥舖上一層又一層的鹽分凝固物，形成[東坪洲所見的](../Page/東坪洲.md "wikilink")[粉砂岩薄層](../Page/粉砂岩.md "wikilink")，是香港最年輕的岩石。湖泊周邊的植物昆蟲則形成化石。到了5,000萬年前的第三紀，氣候和現時一樣溫暖潮濕，陸地隆起，風化侵蝕形成今日地理景觀。200萬年前[第四紀的冰河時期](../Page/第四紀.md "wikilink")，大片[冰原由南北極推進](../Page/冰原.md "wikilink")，全球水平面下降；到了1萬8,000年前的最後一次冰期，海面比現時低130米，香港陸地面積會在南邊多120公里。及後氣候回暖，海平面回升，淹沒山谷形成彎彎曲曲的海岸線，以及兩百多個島嶼。\[2\]\[3\]

## 香港地層表

根據各岩層的岩石特徵和它們形成的時代，香港的地質工作者把香港地區的地層柱劃分為17個地層組和4個火山岩群。

<table>
<thead>
<tr class="header">
<th><p>代</p></th>
<th><p>統／系</p></th>
<th><p>群／組</p></th>
<th><p>厚度<br />
（米）</p></th>
<th><p>分佈</p></th>
<th><p>形成環境</p></th>
<th><p>年齡<br />
（百萬年）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>新生代</strong></p></td>
<td><p><strong>全新統</strong></p></td>
<td><p>沖積層（未分組）</p></td>
<td><p>21</p></td>
<td><p>新界北部</p></td>
<td><p>沖積平原</p></td>
<td><p>0.6-0</p></td>
</tr>
<tr class="even">
<td><p>坡積／沖積層（未分組）</p></td>
<td><p>0-15</p></td>
<td><p>全香港各處山坡及山間盆地</p></td>
<td><p>山坡堆積及洪積</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>坑口組</p></td>
<td><p>31</p></td>
<td><p>香港海域</p></td>
<td><p>海相砂泥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>上更新統</strong></p></td>
<td><p>山下村組（陸上）</p></td>
<td><p>10-36</p></td>
<td><p>新界北部</p></td>
<td><p>河流階地；<br />
山坡及山間盆地堆積及洪積</p></td>
<td><p>1.4-0.6</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>赤鱲角組上部（海區）</p></td>
<td><p>60</p></td>
<td><p>香港海域</p></td>
<td><p>海相砂泥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>中更新統</strong></p></td>
<td><p>黃崗山組（陸上）</p></td>
<td><p>17-30</p></td>
<td><p>新界北部</p></td>
<td><p>河流階地；<br />
山坡及山間盆地堆積及洪積</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>赤鱲角組下部（海區）</p></td>
<td><p>30</p></td>
<td><p>香港海域</p></td>
<td><p>海相砂泥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>下第三系</strong></p></td>
<td><p>平洲組</p></td>
<td><p>450</p></td>
<td><p>平洲島一帶</p></td>
<td><p>淡水湖泊沉積</p></td>
<td><p>53-33.7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>中生代</strong></p></td>
<td><p><strong>上白堊統</strong></p></td>
<td><p>吉澳組</p></td>
<td><p>&gt;100</p></td>
<td><p>吉澳、鴨洲、長排頭及流浮山</p></td>
<td></td>
<td><p>96-65</p></td>
</tr>
<tr class="even">
<td><p>赤洲組</p></td>
<td><p>&gt;1,200</p></td>
<td><p>赤洲、石牛洲及大鵬灣海底</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>下白堊統</strong></p></td>
<td><p>八仙嶺組</p></td>
<td><p>&gt;500</p></td>
<td><p>八仙嶺等地</p></td>
<td></td>
<td><p>135-96</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>滘西洲火山岩群</p></td>
<td><p>&gt;1,000</p></td>
<td><p>新界東西貢、清水灣及牛尾海一帶</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>淺水灣火山岩群</p></td>
<td><p>&gt;2,000</p></td>
<td><p>大嶼山島、嶂上、西貢及香港島</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>下白堊統</strong><br />
至<br />
<strong>上侏羅統</strong></p></td>
<td><p>大嶼山火山岩群</p></td>
<td><p>1,500</p></td>
<td><p>大嶼山島</p></td>
<td></td>
<td><p>154-96</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>荃灣火山岩群</p></td>
<td><p>&gt;500</p></td>
<td><p>元朗、上水、大埔及沙田</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>中下侏羅統</strong></p></td>
<td><p>大澳組</p></td>
<td><p>800</p></td>
<td><p>大澳</p></td>
<td></td>
<td><p>203-160</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>下侏羅統</strong></p></td>
<td><p>屯門組</p></td>
<td><p>400</p></td>
<td><p>屯門至天水圍一帶</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>赤門海峽組</p></td>
<td><p>120</p></td>
<td><p>鳳凰笏、白角山南岸、泥涌、深涌及元朗</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>上古生代</strong></p></td>
<td><p><strong>中下二叠統</strong></p></td>
<td><p>丫洲組</p></td>
<td><p>&gt;100</p></td>
<td><p>大埔海丫洲島</p></td>
<td></td>
<td><p>290-260</p></td>
</tr>
<tr class="even">
<td><p><strong>下二叠統</strong></p></td>
<td><p>大埔海組</p></td>
<td><p>500</p></td>
<td><p>馬屎洲及中文大學</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>下石炭統</strong></p></td>
<td><p>落馬洲組（新田群）</p></td>
<td><p>300-400</p></td>
<td><p>羅湖、大石磨及落馬洲；老鼠嶺、米埔、橫洲及虎地上村</p></td>
<td></td>
<td><p>355-325</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>元朗組（新田群）</p></td>
<td><p>350</p></td>
<td><p>馬田、元朗及朗屏</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>泥盆系</strong></p></td>
<td><p>馬鞍山組</p></td>
<td><p>&gt;500</p></td>
<td><p>馬鞍山</p></td>
<td></td>
<td><p>400</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>黃竹角咀組</p></td>
<td><p>300-600</p></td>
<td><p>馬鞍山、白沙頭洲至黃竹角咀、泥涌及大洞等地</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 岩石

香港擁有多類岩石：[火山岩約佔](../Page/火山岩.md "wikilink")50%；由岩漿侵入活動形成的花崗質岩石、酸性和基性岩脈等佔35%；此外還有15%的[沉積岩和極少數的](../Page/沉積岩.md "wikilink")[變質岩](../Page/變質岩.md "wikilink")。本港一些古老岩層蘊藏着海洋及陸棲生物的[化石](../Page/化石.md "wikilink")，年份界於4億年前的[泥盆紀](../Page/泥盆紀.md "wikilink")（[黃竹角咀](../Page/黃竹角咀.md "wikilink")）和數千萬年前的[第三紀](../Page/第三紀.md "wikilink")（[東平洲](../Page/平洲.md "wikilink")）之間。

香港的各類岩石：

  - 噴出火成岩
      - 由火山物質形成的火山岩主要分為兩種：一是由噴出和流出[岩漿形成的](../Page/岩漿.md "wikilink")[熔岩](../Page/熔岩.md "wikilink")；二是由火山爆發噴出的[火山灰](../Page/火山灰.md "wikilink")、火山彈和岩漿所構成，當這些物質降落[地表後](../Page/地表.md "wikilink")，經過壓實冷卻而形成[火山碎屑岩](../Page/火山碎屑岩.md "wikilink")。
  - 侵入火成岩
      - 當[岩漿由](../Page/岩漿.md "wikilink")[地幔侵入](../Page/地幔.md "wikilink")[地殼](../Page/地殼.md "wikilink")，緩慢冷卻後便形成礦物晶體顆粒較大的花崗質岩石。[花崗岩佔香港總面積](../Page/花崗岩.md "wikilink")30%，主要組成礦物包括有[石英及](../Page/石英.md "wikilink")[長石](../Page/長石.md "wikilink")，並分佈有[黑雲母的黑點](../Page/黑雲母.md "wikilink")，香港的花崗岩大部份分佈於市區，包括[香港島北部及東南部](../Page/香港島.md "wikilink")、[九龍半島](../Page/九龍半島.md "wikilink")、[新界的](../Page/新界.md "wikilink")[葵涌](../Page/葵涌.md "wikilink")、[荃灣](../Page/荃灣.md "wikilink")、[沙田](../Page/沙田_\(香港\).md "wikilink")、[大埔](../Page/大埔_\(香港\).md "wikilink")、[屯門](../Page/屯門.md "wikilink")、[元朗及](../Page/元朗.md "wikilink")[離島的](../Page/香港離島.md "wikilink")[大嶼山東北和南部](../Page/大嶼山.md "wikilink")、[長洲及](../Page/長洲.md "wikilink")[南丫島等](../Page/南丫島.md "wikilink")。
  - 沉積岩
      - 是古老岩層受[風化侵蝕後沉落大海](../Page/風化.md "wikilink")、湖泊和河口三角洲，經歷數百萬年脫水壓實成岩。沉積物包含大小不一的顆粒，形成[泥岩](../Page/泥岩.md "wikilink")、[粉砂岩](../Page/粉砂岩.md "wikilink")、[砂岩](../Page/砂岩.md "wikilink")、[礫岩或](../Page/礫岩.md "wikilink")[角礫岩](../Page/角礫岩.md "wikilink")，有時還含有動植物屍體和殘留物質。
  - 變質岩
      - 岩石受到地底巨大壓力的壓迫及/或高溫烘烤而產生結構變化，亦可能重新[結晶](../Page/結晶.md "wikilink")，形成變質岩。香港地區的變質岩出露不多，主要在[新界西北部如](../Page/新界.md "wikilink")[新田](../Page/新田.md "wikilink")、[落馬洲等地](../Page/落馬洲.md "wikilink")，由原本的砂岩及粉砂岩形成不同變質程度的[千枚岩及](../Page/千枚岩.md "wikilink")[片岩](../Page/片岩.md "wikilink")，由於抗蝕力弱，只能組成一些較矮的山丘。

## 相關條目

  - [香港聯合國教科文組織世界地質公園](../Page/香港聯合國教科文組織世界地質公園.md "wikilink")
  - [香港地理](../Page/香港地理.md "wikilink")

## 資料來源

## 站外鏈結

  - [香港地質：四億年的旅程](http://hkss.cedd.gov.hk/hkss/eng/education/GS/tc/hkg/indexc.htm)
  - [香港的地理與地質歷史](http://www.rocks.org.hk/Folders/Knowledge/HK-Geo-History.pdf)
  - [香港的韌性剪切帶](http://www.geolsoc.org.hk/geologists_articles/kwLai/2008_Ductile%20Shear%20Zone%20of%20HK%20-KWlai.pdf)

[Category:香港地理](../Category/香港地理.md "wikilink")
[Category:地质学](../Category/地质学.md "wikilink")

1.  何耀生（2005年）：《集體回憶之維多利亞港》，香港：明報出版社。ISBN 962-8872-68-0
2.  香港歷史博物館：[「香港故事」常設展資料](http://www.lcsd.gov.hk/CE/Museum/History/download/the_hk_story_exhibition_materials_c.pdf)
    （PDF），展區一：自然生態環境，第3至12頁。
3.  杜德俊、高力行著，陳慧聰譯（2004年）：《香港生態情報》第24～30頁，香港：三聯書店。ISBN 962-04-2386-0