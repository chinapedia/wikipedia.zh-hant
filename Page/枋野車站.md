[Fangye_2nd_Bridge_in_the_green_mountain_forest.jpg](https://zh.wikipedia.org/wiki/File:Fangye_2nd_Bridge_in_the_green_mountain_forest.jpg "fig:Fangye_2nd_Bridge_in_the_green_mountain_forest.jpg")
**枋野車站**，位於[台灣](../Page/台灣.md "wikilink")[屏東縣](../Page/屏東縣.md "wikilink")[獅子鄉](../Page/獅子鄉_\(台灣\).md "wikilink")，位於[枋山車站至](../Page/枋山車站.md "wikilink")[中央號誌站間](../Page/中央號誌站.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[南迴線的鐵路車站](../Page/南迴線.md "wikilink")。目前為[三等站](../Page/三等站_\(台鐵\).md "wikilink")。主要的功能為負責列車的[待避或交會](../Page/待避.md "wikilink")，並無提供售票與旅客上下車相關服務。由於[恆春半島地區](../Page/恆春半島.md "wikilink")[落山風強勁](../Page/落山風.md "wikilink")，本站還需負責監測枋野二號橋路段風速，當風速超過時，通知列車減速通過、或是停車待命\[1\]。本站是南迴線中最高的一站，為海拔135公尺\[2\]。

## 車站構造

  - 混凝土造站房，分為行車室、機房設備與員工休息室，並無旅客服務設施。
  - 未設立月台。

## 利用狀況

  - 本站為三等站。
  - 站內設置正線四股，可進行列車交會。
  - [中央號誌站的控制照明盤設置於本站](../Page/中央號誌站.md "wikilink")，可遙控該站設備運作。
  - 本站於**枋野二號橋**上設置風速監視儀，當偵測到有強烈陣風（[落山風](../Page/落山風.md "wikilink")），為考量行車安全，將限制列車通過速度，或是停駛。
  - 設站目的為辦理行車業務，因此並未設置旅客月台，但每日均有排定列車停靠，以供員工上下班以及遞送業務文件之用\[3\]。除部分[郵輪式列車會停靠本站](../Page/郵輪式列車.md "wikilink")，停靠時需架設鐵梯，供旅客上下車之外，沒有其他客運業務\[4\]。
  - 站內設有「鐵路之旅 - 小站巡禮紀念章」之戳章，可供旅客蓋戳留念（需洽副站長索取）

## 車站週邊

  - 兩端緊臨枋野一號隧道東口及[枋野二號隧道西口](../Page/枋野二號隧道.md "wikilink")。
  - 典型的[秘境車站](../Page/秘境車站.md "wikilink")：位處深山暨行動通訊死角地帶，有蚊蟲、毒蛇出沒，附近無住家、商店，故進出時有安全之虞及補給之慮；一有意外，恐危及生命，聯外交通只有鄉道[屏147-1線](../Page/屏東縣鄉道列表.md "wikilink")（已解編）一條道路，距最近有人聚落[枋山村約](../Page/枋山村.md "wikilink")10公里遠，且沿路上路況惡劣，救醫時後送困難，若逢山崩、水漲，道路中斷時無法脫困。

## 歷史

  - 1992年10月5日：設站。

## 攝影集

<File:Fangye> in SouthLinkLine.jpg|該站上方掛著「南迴鐵路枋野號誌站」九字的站名 <File:Train>
transfor at Fangye.jpg|在枋野站進行[列車交會](../Page/列車交會.md "wikilink")
<File:TRA> thermal paper ticket Taitung-Fangye 20060608.jpg|一張台東到枋野的車票
<File:TRA> Fangye Signal Station rear 20160228.jpg|車站後方
[File:Fangyesignalstation02.jpg|車站逆行方向之枋野二號隧道口](File:Fangyesignalstation02.jpg%7C車站逆行方向之枋野二號隧道口)

## 參見

  - [秘境車站](../Page/秘境車站.md "wikilink")
  - [南迴線](../Page/南迴線.md "wikilink")

## 參考文獻

  - 《南迴鐵路工程輯要：國家十四項重要建設》，臺灣省政府交通處南迴鐵路工程處，1992年。
  - 《鐵道情報 209期》，〈南迴秘境踏查I：枋野站的一天〉，李威漢，p.84-p.89，中華民國鐵道文化協會，2012年。

## 鄰近車站

[Category:獅子鄉 (台灣)](../Category/獅子鄉_\(台灣\).md "wikilink")
[Category:屏東縣鐵路車站](../Category/屏東縣鐵路車站.md "wikilink")
[Category:1992年啟用的鐵路車站](../Category/1992年啟用的鐵路車站.md "wikilink")
[Category:南迴線車站](../Category/南迴線車站.md "wikilink")
[Category:台灣鐵道旅遊景點](../Category/台灣鐵道旅遊景點.md "wikilink")

1.

2.
3.
4.