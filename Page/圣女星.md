****（Miriam）是第102颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1868年8月22日发现。的[直径为](../Page/直径.md "wikilink")83.0千米，[质量为](../Page/质量.md "wikilink")6.0×10<sup>17</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1586.949天。

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1868年发现的小行星](../Category/1868年发现的小行星.md "wikilink")