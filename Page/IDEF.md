**IDEF**(**ICAM Definition
Languages**)是20世纪70年代由[美国](../Page/美国.md "wikilink")[空军发明](../Page/空军.md "wikilink")，最早用于描述企业内部运作的一套[建模方法](../Page/建模.md "wikilink")。经过不断的完善改进，其用途变广泛，现在可以适用于一般的[软件开发](../Page/软件.md "wikilink")。目前，IDEF的方法共十六种--从IDEF0到IDEF14（包括IDEF1X在内）。

## IDEF方法

  - IDEF0 : 功能建模（Function Modeling[1](http://www.idef.com/IDEF0.html)）
    IDEF1 : [信息建模](../Page/信息.md "wikilink")（Information
    Modeling[2](http://www.idef.com/IDEF1.html)）
    IDEF1X : 数据建模（Data Modeling[3](http://www.idef.com/IDEF1x.html)）
    IDEF2 : 仿真建模设计 (Simulation Model Design)
    IDEF3 : 过程描述获取（Process Description Capture
    [4](http://www.idef.com/IDEF3.html)）
    IDEF4 : [面向对象的设计](../Page/面向对象.md "wikilink")（OO设计）（Object-Oriented
    Design [5](http://www.idef.com/IDEF4.html)）
    IDEF5 : 实体描述获取（Ontology Description Capture
    [6](http://www.idef.com/IDEF5.html)）
    IDEF6 : 设计理论获取（Design Rationale Capture
    [7](https://web.archive.org/web/20070402035120/http://stinet.dtic.mil/oai/oai?&verb=getRecord&metadataPrefix=html&identifier=ADA261594)）
    IDEF7 : 信息系统审核（Information System Auditing）
    IDEF8 : 人－系统交互界面设计（User Interface Modeling）
    IDEF9 : 场景驱动信息系统设计（Scenario-Driven IS Design）
    IDEF10 : 实施体系结构建模（Implementation Architecture Modeling）
    IDEF11 : 信息制品建模（Information Artifact Modeling）
    IDEF12 : [组织建模](../Page/组织.md "wikilink")（Organization Modeling）
    IDEF13 : 三模式映射设计（Three Schema Mapping Design）
    IDEF14 : [网络设计方法](../Page/网络设计.md "wikilink")（Network Design）

## IDEF发展史

## 外部链接

<http://www.idef.com/>

[Category:建模语言](../Category/建模语言.md "wikilink")
[Category:数据建模](../Category/数据建模.md "wikilink")
[Category:标准](../Category/标准.md "wikilink")