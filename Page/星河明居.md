[Galaxia_201807.jpg](https://zh.wikipedia.org/wiki/File:Galaxia_201807.jpg "fig:Galaxia_201807.jpg")
[HK_Galaxia.JPG](https://zh.wikipedia.org/wiki/File:HK_Galaxia.JPG "fig:HK_Galaxia.JPG")及星河明居\]\]
[Diamond_Hill_Ling_Liang_Kindergarten_2018.jpg](https://zh.wikipedia.org/wiki/File:Diamond_Hill_Ling_Liang_Kindergarten_2018.jpg "fig:Diamond_Hill_Ling_Liang_Kindergarten_2018.jpg")
**星河明居**（**Galaxia**）是位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[黃大仙區](../Page/黃大仙區.md "wikilink")[鑽石山龍蟠街](../Page/鑽石山.md "wikilink")3號的私人屋苑，建於[港鐵](../Page/港鐵.md "wikilink")[鑽石山站上蓋](../Page/鑽石山站.md "wikilink")，基座為購物商場[荷里活廣場](../Page/荷里活廣場.md "wikilink")，毗鄰[夾屋](../Page/夾屋.md "wikilink")[悅庭軒及](../Page/悅庭軒.md "wikilink")[居屋](../Page/居者有其屋.md "wikilink")[龍蟠苑](../Page/龍蟠苑.md "wikilink")。

## 歷史

星河明居於1998年落成入伙，由[會德豐](../Page/會德豐.md "wikilink")、[九龍倉](../Page/九龍倉.md "wikilink")、[新亞置業及](../Page/新亞置業.md "wikilink")[夏利文地產聯合發展](../Page/夏利文地產.md "wikilink")。由設計，[其士建築有限公司承建](../Page/其士集團.md "wikilink")，現時管理公司改為置佳物業服務有限公司管理。現時屋苑主席為黃大仙區區議員[譚香文](../Page/譚香文.md "wikilink")。

## 概況

星河明居基座為大型商場[-{荷里活}-廣場和香港靈糧堂幼稚園](../Page/荷里活廣場.md "wikilink")。住宅共有5座（A至E座）樓高46（A及E座）及47（B、C、D座）層，合共提供1,684個單位。各座49、50樓為4個相連及複式單位，總數40個。複式單位為20伙。\[1\]與標準單位不同的是，大廳改設弧形落地玻璃窗，大大增加室內空間感，少量角位設計，較易放置大型家具。除以梯形開則的三間睡房，另設一個多用途或儲物室。

前排單位面向[新蒲崗工廠區](../Page/新蒲崗.md "wikilink")，後排單位面向[大老山及](../Page/大老山.md "wikilink")[志蓮淨苑](../Page/志蓮淨苑.md "wikilink")。

## 屋苑設施

屋苑附設18萬方呎平台花園、兒童遊樂場、室外游泳池及20萬呎會所。設施包括網球場、羽毛球場、壁球場、健身室、自修室、桌球室、迷你影院、活動室、桑拿浴室、酒吧及綜合康樂室、地中海庭院）及停車場。

基座2樓設香港靈糧堂幼稚園。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[鑽石山站](../Page/鑽石山站.md "wikilink")

<!-- end list -->

  - [鑽石山站公共運輸交匯處](../Page/鑽石山站公共運輸交匯處.md "wikilink")

<!-- end list -->

  - [龍蟠街](../Page/龍蟠街.md "wikilink")

<!-- end list -->

  - [鳳德街](../Page/鳳德街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/公共小巴.md "wikilink")

<!-- end list -->

  - [荃灣至](../Page/荃灣.md "wikilink")[黃大仙](../Page/黃大仙.md "wikilink")/[九龍城線](../Page/九龍城.md "wikilink")\[2\]

<!-- end list -->

  - [大磡道](../Page/大磡道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/公共小巴.md "wikilink")

<!-- end list -->

  - [觀塘至](../Page/觀塘.md "wikilink")[落馬洲線](../Page/落馬洲.md "wikilink")\[3\]

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  - [藍田站至](../Page/藍田站.md "wikilink")[皇崗口岸線](../Page/皇崗口岸.md "wikilink")

<!-- end list -->

  - [龍翔道](../Page/龍翔道.md "wikilink")

<!-- end list -->

  - [上元街](../Page/上元街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/公共小巴.md "wikilink")

<!-- end list -->

  - 荃灣至[坪石線](../Page/坪石.md "wikilink")\[4\]

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  - [藍田站至](../Page/藍田站.md "wikilink")[皇崗口岸線](../Page/皇崗口岸.md "wikilink")

</div>

</div>

## 外部連結

  - [星河明居](http://www.galaxia.hk/)

[Category:黃大仙區私人屋苑](../Category/黃大仙區私人屋苑.md "wikilink")
[Category:鑽石山](../Category/鑽石山.md "wikilink")
[Category:會德豐地產(香港)物業](../Category/會德豐地產\(香港\)物業.md "wikilink")
[Category:九龍倉置業物業](../Category/九龍倉置業物業.md "wikilink")
[Category:大磡](../Category/大磡.md "wikilink")

1.
2.  [荃灣荃灣街市街—黃大仙及九龍城](http://www.16seats.net/chi/rmb/r_kn45.html)
3.  [觀塘同仁街—落馬洲](http://www.16seats.net/chi/rmb/r_kn77.html)
4.  [荃灣荃灣街市街—新蒲崗及坪石](http://www.16seats.net/chi/rmb/r_kn42.html)