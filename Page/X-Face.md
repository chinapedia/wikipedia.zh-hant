**X-Face**是一种加在[Usenet或](../Page/Usenet.md "wikilink")[电子邮件消息上的小](../Page/电子邮件.md "wikilink")[位图](../Page/位图.md "wikilink")（48×48像素，黑白单色），典型的用法是显示發信者的脸，類似今日的[用戶造型](../Page/用戶造型.md "wikilink")\[1\]或[即時通訊軟體](../Page/即時通訊.md "wikilink")[好友圖示](../Page/好友圖示.md "wikilink")\[2\]功能。图像数据[編碼成文字](../Page/編碼.md "wikilink")，被附在所发文章的[信头](../Page/信头.md "wikilink")。*X-Face*這個名稱是因為[RFC](../Page/RFC.md "wikilink")822\[3\]建議非標準的信頭標籤使用"X-"作為開頭。它的發明者是[James
Ashton](https://web.archive.org/web/20091024084358/http://users.rsise.anu.edu.au/~jaa/)。

X-Face是[贝尔实验室](../Page/贝尔实验室.md "wikilink")1980年代开发[Vismon程序的副产品](../Page/Vismon.md "wikilink")。有很多支持X-Face的软件。它们中的大部分是[UNIX及其变种平台上的](../Page/UNIX.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")。也有在线的，网络平台上的X-Face产生器以及流行的如[KMail或](../Page/KMail.md "wikilink")[Sylpheed这样的程序用来产生X](../Page/Sylpheed.md "wikilink")-Face。

并非所有的[電子郵件客戶端都支持X](../Page/電子郵件客戶端.md "wikilink")-Face信头。大部分安装在域内或是商用电脑内的邮件程序对此无能为力。即使是在大学或是在研究机构这样经常使用UNIX的地方，也只有很少*完全*支持X-Face的邮件客户端安装。

## 参见

  - [用戶造型](../Page/用戶造型.md "wikilink")

## 外部链接

  - [Online X-Face Converter](http://www.dairiki.org/xface/xface.php) -
    線上X-Face轉換器
  - [X-Faces](http://www.xs4all.nl/~ace/X-Faces/) - X-Face圖集
  - [MessageFaces](http://tecwizards.de/mozilla/messagefaces/) -
    電子郵件軟體[Thunderbird的X](../Page/Mozilla_Thunderbird.md "wikilink")-Face擴充套件

## 註解

<references/>

[Category:网络文化](../Category/网络文化.md "wikilink")
[Category:Usenet](../Category/Usenet.md "wikilink")

1.  「用戶造型」即[Avatar](../Page/头像.md "wikilink")
2.  「好友圖示」即，又稱為頭像
3.  [RFC822](http://www.ietf.org/rfc/rfc822.txt)