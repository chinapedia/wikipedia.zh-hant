**中尾
明慶**（），出生於[東京都的](../Page/東京都.md "wikilink")[日本男](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")，所屬[經紀公司為](../Page/經紀公司.md "wikilink")[Horipro](../Page/Horipro.md "wikilink")。[身高](../Page/身高.md "wikilink")171[公分](../Page/公分.md "wikilink")，[血型A型](../Page/血型.md "wikilink")。專長是[職業棒球選手的形態模仿](../Page/職業棒球.md "wikilink")。

## 主要演出

粗體為主演作品

### 電視劇

  - 冠軍大胃王（）（2000年、[NTV](../Page/日本電視台.md "wikilink")）
  - [3年B組金八先生](../Page/3年B組金八先生.md "wikilink")（第6輯）（2001年，[TBS](../Page/TBS電視台.md "wikilink")）-
    飾演：山越祟行
  - [明智小五郎對怪人二十面相](../Page/明智小五郎對怪人二十面相.md "wikilink")（2001年，TBS）-
    飾演：小林少年
  - [夢想飛行Good Luck](../Page/夢想飛行Good_Luck.md "wikilink")（2003年，TBS）-
    飾演：新海誠
  - [水男孩2](../Page/五個撲水的少年#水男孩2.md "wikilink")（2004年，[CX](../Page/富士电视台.md "wikilink")）-
    飾演：山本洋介
  - [H2](../Page/H2.md "wikilink")（2005年，TBS）- 飾演：野田敦
  - [東大特訓班](../Page/東大特訓班.md "wikilink")（2005年，TBS）- 飾演：奥野一郎
  - Brother Beat（2005年，TBS）- 飾演：三男・桜井純平
  - 戀愛小說（2006年，TBS）
  - [偵探學園Q](../Page/偵探學園Q.md "wikilink")（2006年，NTV）- 飾演：三郎丸豊
  - [水手服與機關槍](../Page/水手服與機關槍.md "wikilink")（2006年，TBS）- 飾演：酒井健次
  - **拜求舖橫丁始末記**（2006年，[EX](../Page/朝日電視台.md "wikilink")）- 飾演：市川正太郎
  - 三日遲滯的Happy new year！（2007年，NTV）- 飾演：田中純太
  - 我腦海中的橡皮擦（2007年，NTV）- 飾演：香野智史
  - **學問的推薦**（2007年，[ABC](../Page/朝日放送.md "wikilink")） - 飾演：津吹淳一
  - [ROOKIES](../Page/ROOKIES.md "wikilink")（2008年4月，TBS）- 飾演：関川秀太
  - 裸體的大將流浪記〜因為冒牌貨出現在富士山？（2008年10月、CX）- 飾演：佐野隼人
  - [我无法恋爱的理由](../Page/我无法恋爱的理由.md "wikilink")（2011年10月，CX）- 飾演：高桥健太
  - [王牌大律師2](../Page/王牌大律師.md "wikilink")（リーガル・ハイ 2）（2013年10月，CX）-
    飾演：土屋秀典
  - [永遠的0](../Page/永遠的0#電視劇.md "wikilink")（2015年，[TX](../Page/東京電視台.md "wikilink")）-
    飾演：長谷川梅男
  - [戀愛時代](../Page/戀愛時代_\(2015年電視劇\).md "wikilink")（2015年，[讀賣電視台](../Page/讀賣電視台.md "wikilink")）-
    飾演：海江田護
  - [黑服物語](../Page/黑服物語.md "wikilink") (2015年)
  - [嬌妻出沒注意](../Page/嬌妻出沒注意.md "wikilink")（2017年，[日本電視台](../Page/日本電視台.md "wikilink")）-飾演：佐藤涉

### 電影

  - [戰國自衛隊1549](../Page/戰國自衛隊1549.md "wikilink")（2005年，[東寶](../Page/東寶.md "wikilink")）

  - （2007年）飾 ニッタ

  - [ROOKIES－卒業－](../Page/ROOKIES#電影.md "wikilink")（2009年5月，東寶）

  - [穿越時空的少女](../Page/穿越時空的少女.md "wikilink") (2010年)

  - [痞子英雄首部曲：全面開戰(配音)](../Page/痞子英雄首部曲：全面開戰.md "wikilink")(2012年，[東映](../Page/東映.md "wikilink"))

  - [逆轉裁判](../Page/逆轉裁判#電影.md "wikilink") 飾 矢張政志

  - [鄰居同居](../Page/鄰居同居.md "wikilink") 飾 佐藤亮介

  - [東京難民](../Page/東京難民.md "wikilink") 飾 小次郎

## 外部連結

  - [HORIPRO
    官方網站](https://web.archive.org/web/20080505064651/http://www.horipro.co.jp/hm/nakao/)
  - [HORIPRO 中尾明慶的資料](http://www.horipro.co.jp/talent/PM033/)
  - [中尾明慶 Official Blog 「一笑」](http://ameblo.jp/nakaoakiyoshi/)

[Nakao Akiyoshi](../Category/日本男演員.md "wikilink") [Nakao
Akiyoshi](../Category/Horipro.md "wikilink")