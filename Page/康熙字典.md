《**康熙字典**》是一部成書於[清朝](../Page/清朝.md "wikilink")[康熙五十五年](../Page/康熙.md "wikilink")（1716年）的[漢字](../Page/漢字.md "wikilink")[字典](../Page/字典.md "wikilink")，記錄了當時[漢語中漢字的點畫寫法](../Page/漢語.md "wikilink")、音讀和義訓。作爲自漢代《[說文解字](../Page/說文解字.md "wikilink")》以來的字典之集大成者，於[康熙年間由](../Page/康熙.md "wikilink")[文華殿](../Page/文華殿.md "wikilink")[大學士兼](../Page/大學士.md "wikilink")[户部](../Page/户部.md "wikilink")[尚書](../Page/尚書.md "wikilink")[張玉書及](../Page/張玉書.md "wikilink")[經筵講官](../Page/經筵.md "wikilink")、[文淵閣大學士兼](../Page/文淵閣.md "wikilink")[吏部尚書](../Page/吏部.md "wikilink")[陳廷敬擔任主编](../Page/陳廷敬.md "wikilink")，参考[明代的](../Page/明代.md "wikilink")《[字汇](../Page/字彙_\(梅膺祚\).md "wikilink")》、《[正字通](../Page/正字通.md "wikilink")》两书而經愈六年編纂完成，重印至今不輟。

## 歷史

### 敕編至刊行

康熙四十九年三月乙亥（初十）下詔始修《康熙字典》\[1\]，設總閱官張玉書、陳廷敬，另有[史夔](../Page/史夔.md "wikilink")、[吳世燾](../Page/吳世燾.md "wikilink")、[萬經](../Page/萬經.md "wikilink")、[劉巗](../Page/劉巗.md "wikilink")、[周起渭](../Page/周起渭.md "wikilink")、[蔣廷錫](../Page/蔣廷錫.md "wikilink")、[汪漋](../Page/汪漋.md "wikilink")、[勵廷儀](../Page/勵廷儀.md "wikilink")、[張逸少](../Page/張逸少.md "wikilink")、[趙熊詔](../Page/趙熊詔.md "wikilink")、[涂天相](../Page/涂天相.md "wikilink")、[王雲錦](../Page/王雲錦.md "wikilink")、[賈國維](../Page/賈國維.md "wikilink")、[劉灝](../Page/劉灝.md "wikilink")、[梅之珩](../Page/梅之珩.md "wikilink")、[陳璋](../Page/陳璋.md "wikilink")、[陳邦彥](../Page/陳邦彥.md "wikilink")、[王景曾](../Page/王景曾.md "wikilink")、[淩紹雯等二十八人任纂修官](../Page/淩紹雯.md "wikilink")。康熙五十年張玉書病逝、賈國維因“行止不端”被革職，劉巗亦因《[南山集](../Page/南山集.md "wikilink")》案被「革職僉妻，流三千里」，陳廷敬於五十一年四月逝世，五十二年淩紹雯、史夔先後辭世，五十三年周起渭去世。康熙五十五年（1716年）頒行，歷時六年。

### 王錫侯《字貫》案

[乾隆四十二年](../Page/乾隆.md "wikilink")（1777年），[王锡侯著](../Page/王锡侯.md "wikilink")《[字贯](../Page/字贯.md "wikilink")》一书，首次指出《康熙字典》在引证、释义等方面的缺點，然因對皇帝私名未做避諱缺筆處理，照大逆律處斬，其著作也被付之一炬。

### 傳入日本

康熙五十五年刊行後，即傳入日本\[2\]。日本安永九年（1780年，乾隆四十五年），[都賀庭鍾](../Page/都賀庭鍾.md "wikilink")、[都賀枚春著有](../Page/都賀枚春.md "wikilink")《[字典琢屑](../Page/字典琢屑.md "wikilink")》。

### 王引之《字典考證》引起摘謬之風

至[道光七年](../Page/道光.md "wikilink")（1827年）[王引之奉皇帝之命](../Page/王引之.md "wikilink")，著《[字典考证](../Page/字典考证.md "wikilink")》[校正了部分](../Page/校對.md "wikilink")《康熙字典》引书方面的错误，當中引用書籍字勾訛誤共2588條。[日本](../Page/日本.md "wikilink")[明治初期學者](../Page/明治.md "wikilink")[渡部温著有](../Page/渡部温.md "wikilink")《康熙字典考异正误》，創新的以[段玉裁之](../Page/段玉裁.md "wikilink")《說文解字注》為依據，考異1,930餘條、訂誤4,000餘條（但和王引之所訂正者部分重複）。

[Kangxi_Dictionary_-_Chinese_Dictionary_Museum_2.JPG](https://zh.wikipedia.org/wiki/File:Kangxi_Dictionary_-_Chinese_Dictionary_Museum_2.JPG "fig:Kangxi_Dictionary_-_Chinese_Dictionary_Museum_2.JPG")展出的《康熙字典》。\]\]

## 構成

《康熙字典》共载47,035字目。書按[地支分為十二集](../Page/地支.md "wikilink")，每集再分为上、中、下3卷，構成正文共36卷。以214个[部首分类](../Page/部首.md "wikilink")，一一列出《[廣韻](../Page/廣韻.md "wikilink")》、《[集韻](../Page/集韻.md "wikilink")》、《[韻會](../Page/韻會.md "wikilink")》、《[唐韻](../Page/唐韻.md "wikilink")》等韵书的音切，并注有「[反切](../Page/反切.md "wikilink")」、「[直音](../Page/直音.md "wikilink")」兩種注音、出處、及参考等，多引《[尚書](../Page/尚書.md "wikilink")》、《[孟子](../Page/孟子_\(著作\).md "wikilink")》、《[莊子](../Page/庄子_\(书\).md "wikilink")》、《[荀子](../Page/荀子_\(書\).md "wikilink")》、《[世紀](../Page/世紀.md "wikilink")》、《[左傳](../Page/左傳.md "wikilink")》等經、史、子、集之書為證。書中並按[韻母](../Page/韻母.md "wikilink")、[聲調以及](../Page/聲調.md "wikilink")[音節分類排列韻母表及其對應](../Page/音節.md "wikilink")[漢字](../Page/漢字.md "wikilink")，另外附有《字母切韻要法》和《等韻切音指南》。

序中稱本書「古今形體之辨，方言聲氣之殊，部分班列，開卷了然。無一義之不詳，一音之不備矣。」

## 價值及影響

康熙字典承自有字書以來的歷史學術成果，又開先例，是爲中國古代第一部收字宏富、規模巨大、價值極高、影響廣泛的大型漢字[字典](../Page/字典.md "wikilink")，被稱爲中國辭書史上的一座豐碑，對中國辭書編纂史的研究有重大意義。且爲世上第一部以「字典」命名的字書。此書注重對[汉字结构的](../Page/汉字结构.md "wikilink")[分析和](../Page/分析.md "wikilink")[詞義的辨析](../Page/詞義.md "wikilink")。

由於不接納[简化字註冊公司名稱](../Page/简化字.md "wikilink")，[香港公司名稱須在此書或](../Page/香港.md "wikilink")1936年的[辭海內找到](../Page/辭海.md "wikilink")\[3\]。依照清代法律规定，凡读书人策应科举考试，书写字形必须以此書为正誤标准。因此，该书对学术界影响很大，成書之後，流行極廣，至今仍不失为一本有价值的语文工具书。其影響至今仍在漢字文化圈仍有體現，至今各國辭書乃至於[統一碼仍有使用康熙字典的部首順序排列](../Page/統一碼.md "wikilink")。康熙字典所載字體成爲印刷字體標準，至今的計算機字體仍有康熙字典的痕跡。《康熙字典》中收錄的[部首偏旁也統一碼中被單獨編碼](../Page/康熙部首.md "wikilink")。

## 版本

《康熙字典》清朝[內務府所發行的初版](../Page/內務府.md "wikilink")，稱爲**內府本**或殿版、武英殿版等。此外清代有各種木刻版。1780年，在日本出現木版翻刻版康熙詞典，稱爲**安永本**。[晚清时](../Page/晚清.md "wikilink")，[上海出现了好幾種](../Page/上海.md "wikilink")[影印本](../Page/影印本.md "wikilink")，[中华书局过去曾用同文书局的影印本为底本制成锌版](../Page/中华书局.md "wikilink")，现在利用存版重印，并附[王引之](../Page/王引之.md "wikilink")《字典考证》於后，以供参考。

<File:Kangxi_Dictionary_1827.JPG>|[道光七年](../Page/道光.md "wikilink")（1827年）重刊版
[File:K'ang_Hsi_Dict.png|20世纪初](File:K'ang_Hsi_Dict.png%7C20世纪初)[廣益書局版](../Page/廣益書局.md "wikilink")
<File:K'ang> Hsi
Dictionary.jpg|2005年[香港中華書局版](../Page/香港中華書局.md "wikilink")
<File:K'ang> Hsi Dictionary
China.JPG|2007年北京[中華書局版](../Page/中華書局.md "wikilink")

## 目录

  - 御製序
  - 凡例
  - 等韻
  - 總目
  - 檢字
  - 正文

<!-- end list -->

  - 子集
      - 上：一丨丶丿乙亅二亠
      - 中：人
      - 下：-{儿}-入八冂冖冫-{几}-凵刀力勹匕匚匸十卜卩-{厂}-厶又
  - 丑集
      - 上：口囗
      - 中：土士夂
      - 下：夊夕大女
  - 寅集：
      - 上：子宀寸小尢-{尸}-屮
      - 中：山巛工己巾
      - 下：干幺-{广}-廴廾弋弓彐彡彳
  - 卯集：
      - 上：心
      - 中：戈户手
      - 下：支攴文斗斤方无
  - 辰集：
      - 上：日曰月
      - 中：木
      - 下：欠止歹殳毋比毛氏-{气}-
  - 巳集：
      - 上：水
      - 中：火爪父爻爿片牙
      - 下：牛犬
  - 午集：
      - 上：玄玉瓜瓦甘生用田疋
      - 中：疒癶白皮皿目矛矢
      - 下：石示禸禾穴立
  - 未集：
      - 上：竹米
      - 中：糸缶-{网}-羊羽老而耒耳聿
      - 下：肉臣自至臼舌舛舟艮色
  - 申集：
      - 上：艸
      - 中：虍-{虫}-
      - 下：血行衣襾
  - 酉集：
      - 上：-{見角言}-
      - 中：-{谷豆豕豸貝赤走足身}-
      - 下：-{車辛辰辵邑酉采里}-
  - 戌集：
      - 上：-{金長門}-
      - 中：-{阜隸隹雨青非面革韋韭音}-
      - 下：-{頁風飛食首香}-
  - 亥集：
      - 上：-{馬骨高髟鬥鬯鬲鬼}-
      - 中：-{魚鳥}-
      - 下：-{鹵鹿麥麻黃黍黑黹黽鼎鼓鼠鼻齊齒龍龜龠}-

<!-- end list -->

  - 補遺
  - 備考
  - 後記
  - 考證

## 参考文献

## 外部連結

  - [康熙字典网上版](http://www.kangxizidian.com)
  - [漢典（供查詢單字在康熙字典中的內容）](http://www.zdic.net)
  - [在線康熙字典查詢](http://zi.artx.cn/zi/)
  - [康熙字典](http://kx.cdict.info/) 線上康熙字典快速查詢
  - [開放康熙字典](https://web.archive.org/web/20130330095758/http://kangxi.adcs.org.tw/index.html)
  - [訂正康熙字典 EPUB版](https://wakufactory.jp/densho/ndl/kouki/)
  - [TypeLand
    康熙字典體](http://wytype.com/typeface/earlier/)（由中国字体设计师厉向晨基于道光版《康熙字典》而開發的字體）

{{-}}

[Category:康熙](../Category/康熙.md "wikilink")
[Category:汉语字典](../Category/汉语字典.md "wikilink")
[Category:漢語音韻學](../Category/漢語音韻學.md "wikilink")
[Category:中古漢語](../Category/中古漢語.md "wikilink")
[Category:清朝官修典籍](../Category/清朝官修典籍.md "wikilink")
[Category:漢字標準](../Category/漢字標準.md "wikilink")

1.

2.
3.  [香港公司名稱註冊指引](https://www.cr.gov.hk/sc/companies_ordinance/docs/Guide_RegCompName-c.pdf).
    [公司註冊處](../Page/公司註冊處.md "wikilink"). 2014年1月.