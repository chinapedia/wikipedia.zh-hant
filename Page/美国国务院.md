border |logo_width = 175px |logo_caption = 美国国务院旗帜 |seal = US
Department of State official seal.svg |seal_width = 175px
|seal_caption = 美国国务院徽章 |picture = United States Department of State
headquarters.jpg |picture_caption = |formed =  |date1 = 1789年9月15日
|date1_name = 更名 |date2 = |date2_name = |preceding1 = 美国外交部
|preceding2 = |dissolved = |superseding = |headquarters =
[华盛顿哥伦比亚特区](../Page/华盛顿哥伦比亚特区.md "wikilink")
[西北区C大街](../Page/西北区_\(华盛顿哥伦比亚特区\).md "wikilink")2201号
[哈里·S·杜鲁门大楼](../Page/哈里·S·杜鲁门大楼.md "wikilink") |latd = 38 |latm = 53
|lats = 39 |latNS = N |longd = 77 |longm = 2 |longs = 54 |longEW = W
|employees = 13,000名外务人员
11,000名公务人员
45,000外务地方雇员\[1\] |budget = 475亿[美元](../Page/美元.md "wikilink")（FY
2015；包括265亿美元国务院预算及210亿美元国际援助）\[2\] |chief1 =
[麥克·蓬佩奧](../Page/麥克·蓬佩奧.md "wikilink") |chief1_title
= [国务卿](../Page/美国国务卿.md "wikilink") |chief2 = 小托马斯·A·沙农 |chief2_title
= [美国副国务卿](../Page/美国副国务卿.md "wikilink") |parent_agency =
|child1_agency = |child2_agency = |website =
[state.gov](https://www.state.gov) |footnotes = }}

**美国国务院**（，有时亦称）\[3\]，直译为**美国国务部**，是[美國聯邦政府負責](../Page/美國聯邦政府.md "wikilink")[外交事務的](../Page/外交.md "wikilink")[行政部门](../Page/美国联邦行政部门.md "wikilink")，前身为[美国外交部](../Page/美国外交部.md "wikilink")，目前亦相當於其他國家的[外交部](../Page/外交部.md "wikilink")\[4\]。美国国务院是美国最龐大的政府機構之一；其行政首長为[国务卿](../Page/美国国务卿.md "wikilink")，现任的国务卿是[麦克·蓬佩奥](../Page/麦克·蓬佩奥.md "wikilink")。

美国国务部总部设在美国首都[华盛顿](../Page/华盛顿DC.md "wikilink")[雾谷地區C街](../Page/雾谷.md "wikilink")2201号的杜鲁门大楼，离白宫只有几个街区距离。因此「霧谷」（Foggy
Bottom）有時也作為國務院的代稱。

美国国务部长是[美国内阁中仅次于](../Page/美国内阁.md "wikilink")[副总统後的成员](../Page/美国副总统.md "wikilink")。如果总统无法有效行使总统权力能力或者意外死亡，按照[美国总统继任顺序](../Page/美国总统继任顺序.md "wikilink")，国务部长排在第四位，即排在[美国副总统](../Page/美国副总统.md "wikilink")、[美国众议院议长和](../Page/美国众议院议长.md "wikilink")[参议院临时议长之后](../Page/参议院临时议长.md "wikilink")。但由于副總統祇是充當總統的後備因而位高權不重，而参议院临时议长象征意義較大，美國政治權力當中，国务部长的真正實權相比副總統和参议院临时议长還要大，故国务卿的真正權力僅次於總統和美国众议院议长。

## 历史

根据1787年在宾州费城起草并在1788年获得各州批准的美国宪法，美国总统负责外交事务。1789年7月21日，美国众参两院批准立法，成立第一个联邦外交机构——**外交部**（），7月27日由华盛顿总统签字批准生效。1789年9月，美国又通过立法将该机构的名称改为国务院，讓該機構也處理聯邦各個政治實體之間的工作，并赋予其各种各样的国内事务。这些责职包括管理[美国鑄幣局](../Page/美国鑄幣局.md "wikilink")，掌管美国国玺，负责人口普查。后来，国务院的大多数国内事务最终转交19世纪建立起来的联邦各部和机构，但名稱保留至今。

## 职能

[美國國務卿是美國總統外交政策的主要顧問](../Page/美國國務卿.md "wikilink")，負責策定美國政府的所有外交政策。美國國務院同時也對於海外所有對美的所有權益進行策略評估，並研擬美國未來外交上的行動。另外，國務院會向美國總統提出各邦交國的關係，承認新國家政府等相關事宜提出建言，在國際場合上代表美國締結邦交、簽署協議條約。美国国务院为美国其他部门的商业活动提供支持，如商务部和美国国际开发署。同时，美国国务院也为美国公民和外国人来美参观和移民提供各种重要服务。根据规定，国务院负责所有外交活动，诸如：美国国外代表，对外援助项目，打击国际犯罪以及对外军事培训项目等。

## 組織單位

[Mike_Pompeo_official_CIA_portrait.jpg](https://zh.wikipedia.org/wiki/File:Mike_Pompeo_official_CIA_portrait.jpg "fig:Mike_Pompeo_official_CIA_portrait.jpg")\]\]
[Rex_Tillerson_official_Transition_portrait.jpg](https://zh.wikipedia.org/wiki/File:Rex_Tillerson_official_Transition_portrait.jpg "fig:Rex_Tillerson_official_Transition_portrait.jpg")\]\]
[John_Kerry_official_Secretary_of_State_portrait.jpg](https://zh.wikipedia.org/wiki/File:John_Kerry_official_Secretary_of_State_portrait.jpg "fig:John_Kerry_official_Secretary_of_State_portrait.jpg")\]\]
[Hillary_Rodham_Clinton.jpg](https://zh.wikipedia.org/wiki/File:Hillary_Rodham_Clinton.jpg "fig:Hillary_Rodham_Clinton.jpg")\]\]
[Condoleezza_Rice.jpg](https://zh.wikipedia.org/wiki/File:Condoleezza_Rice.jpg "fig:Condoleezza_Rice.jpg")\]\]
[Henry_Kissinger.jpg](https://zh.wikipedia.org/wiki/File:Henry_Kissinger.jpg "fig:Henry_Kissinger.jpg")\]\]

国务卿是国务院的最高行政官，直接对美国总统负责。

國務院官銜一般分為：[美国國務卿](../Page/美国國務卿.md "wikilink")（部长级）、[美国副國務卿](../Page/美国副國務卿.md "wikilink")（副部长级）、[美國國務次卿](../Page/美國國務次卿.md "wikilink")（次长级，Under
Secretary）、[美國助理國務卿](../Page/美國助理國務卿.md "wikilink")（助卿，司局级，Assistant
Secretary）和[美國副助理國務卿](../Page/美國副助理國務卿.md "wikilink")（副助卿，副司局级，Deputy
Assistant Secretary）。

  - [美国副國務卿](../Page/美国副國務卿.md "wikilink")—為次於國務卿之官員，下轄各有專職的六位次卿與下列職務或單位：

      - 幕僚長（Chief of Staff）

      - （Executive Secretariat）

      - 全球政府间事务办公室（Office of Global Intergovernmental Affairs）

      - 反恐协调办公室（Office of the Coordinator for Counterterrorism）

      - （Office of the Coordinator for Reconstruction and Stabilization）

      - （National Foreign Affairs Training Center）

      - [國際資訊計劃局](../Page/國際資訊計劃局.md "wikilink")（International
        Information Programs）

      - （Office of the Legal Adviser）

      - 管理政策办公室（Office of Management Policy）

      - （Office of Protocol）

      - 科學技術顧問办公室（Office of the Science and Technology Adviser）

      - 战争犯罪办公室（Office of War Crimes Issues）

      - （Bureau of Intelligence and Research）

      - （Bureau of Legislative Affairs）

      - （Bureau of Resource Management）

  - —第三級重量官員，是國務卿與副國務卿缺席或有事時之代理人，負責以下政務單位：

      - [東亞暨太平洋事務局](../Page/美國國務院東亞暨太平洋事務局.md "wikilink")（Bureau of East
        Asian and Pacific Affairs）

      - （Bureau of African Affairs）

      - （Bureau of Near Eastern Affairs）

      - （Bureau of South and Central Asian Affairs）

      - （Bureau of European and Eurasian Affairs）

      - （Bureau of Western Hemisphere Affairs）

  - ：

      - （Bureau of Administration）

          - 預算處（Office of Allowances）
          - 驗證處（Office of Authentications）
          - 編譯處（Language Services）
          - 備勤管理處（Office of Logistics Management）
          - 海外學校處（Office of Overseas Schools）
          - 弱小企業利用處（Office of Small and Disadvantaged Business
            Utilization）
          - 多媒體服務處（Office of Multi-Media Services）
          - 領導管理處（Office of Directives Management）
          - 福利暨休憩事務處（Office of Commissary and Recreation Affairs）
          - 出納處（Office of the Procurement Executive）

      - （Bureau of Consular Affairs）

          - （Office of Children's Issues）

      - （Bureau of Diplomatic Security）

          - （U.S. Diplomatic Security Service；DSS）

          - （Office of Foreign Missions；OFM）

      - 人力資源局（Bureau of Human Resources）

      - （Bureau of Information Resource Management；IRM）

      - （Bureau of Overseas Buildings Operations；S/OBO）

      - 外交接待室主任（Director of Diplomatic Reception Rooms）

      - （Foreign Service Institute；FSI）

      - 政策管理暨整編創新办公室（Office of Management Policy, Rightsizing, and
        Innovati）

      - 醫療服務办公室（Office of Medical Services）

      - 白宮聯絡办公室（Office of White House Liaison）

  - ：

      - 经济能源和商业事务局（Bureau of Economic, Energy, and Business Affairs）

  - －該職原主管**美國新聞署**（U.S. Information Agency），但在1999年併入國務院：

      - （Bureau of Educational and Cultural Affairs；ECA）

          - （Internet Access and Training Program；IATP）

      - （Bureau of Public Affairs）

          - [歷史文獻辦公室](../Page/歷史文獻辦公室.md "wikilink")（Office of The
            Historian）

      - [-{zh-hans:国际信息局;
        zh-hant:國際資訊局;}-](../Page/美国国务院国际信息局.md "wikilink")（Bureau
        of International Information Programs）

  - —扮演國務院在美國軍事體系中的協調角色：

      - （Bureau of International Security and Nonproliferation）

      - （Bureau of Political-Military Affairs）

      - （Bureau of Verification, Compliance, and Implementation）

  - 平民安全暨民主人權次卿—全球事務办公室是由柯林頓辦公室為了能掌握新興外交影響力所建立的，2005年改名為民主暨全球事務办公室，以增加民主在美國外交政策中的份量：

      - （Bureau of Democracy, Human Rights, and Labor）

      - （Bureau of Oceans and International Environmental and Scientific
        Affairs）

      - （Bureau of Population, Refugees, and Migration）

      - （Office to Monitor and Combat Trafficking in Persons）

  - 国务顾问（Counselor）－次卿，国务卿和副国务卿的特别顾问，对重大问题的外交政策顾问。

  - （Office of Global AIDS Coordinator）

## 参考文献

## 外部链接

  - [美国国务院官方网站](http://www.state.gov/)

  - [美国国务院历史](https://archive.is/20120805161554/www.state.gov/www/about_state/history/dephis.html)


  - [IIP Digital](http://iipdigital.usembassy.gov/chinese/)

  -
  -
  -
  -
  -
  - [美国国务院](https://plus.google.com/u/0/102630068213960289352#102630068213960289352/posts)的[Google+官方部落格](../Page/Google+.md "wikilink")

  - [美国国务院](http://statedept.tumblr.com/)的[Tumblr官方帳戶](../Page/Tumblr.md "wikilink")

## 参见

  - [美国外交](../Page/美国外交.md "wikilink")
      - [美国外交部](../Page/美国外交部.md "wikilink")
      - [美国国务卿](../Page/美国国务卿.md "wikilink")、[美国副国务卿](../Page/美国副国务卿.md "wikilink")
      - [美國駐外機構列表](../Page/美國駐外機構列表.md "wikilink")
  - [美国国家安全委员会](../Page/美国国家安全委员会.md "wikilink")

{{-}}

[State](../Category/美國聯邦行政部門.md "wikilink")
[美国国务院](../Category/美国国务院.md "wikilink")
[Category:美國外交](../Category/美國外交.md "wikilink") [United
States](../Category/各国外交部门.md "wikilink")
[States](../Category/1789年建立政府機構.md "wikilink")
[Category:1789年美國建立](../Category/1789年美國建立.md "wikilink")

1.
2.
3.
4.