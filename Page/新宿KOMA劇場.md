**新宿KOMA劇場**（）是一間於1956年12月在[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[新宿区](../Page/新宿区.md "wikilink")[歌舞伎町一丁目開幕](../Page/歌舞伎町.md "wikilink")，由[阪急阪神東寶集團旗下公司](../Page/阪急阪神東寶集團.md "wikilink")「Koma
Stadium」（）經營，座席數2092席的[劇場](../Page/劇場.md "wikilink")，簡稱。名稱中的「KOMA」在日文為[陀螺之意](../Page/陀螺.md "wikilink")，其[識別標誌也以陀螺為概念設計](../Page/標誌.md "wikilink")。劇場模仿[希臘時代的劇場式様](../Page/希臘.md "wikilink")，採用圓形舞台。同心圓狀配三重環廻的舞台，可以作回轉與上下運動以造出多種多様的舞台効果。開幕以來經過了多次改良、改修。開幕時名為「新宿Koma・Stadium（）」。與大阪梅田的「梅田Koma・Stadium（）」是姊妹劇場。

此劇場有『演歌的殿堂』之稱，表演項目有[北島三郎](../Page/北島三郎.md "wikilink")、[小林幸子](../Page/小林幸子.md "wikilink")、[冰川清志等](../Page/冰川清志.md "wikilink")[演歌歌手的特別公演](../Page/演歌.md "wikilink")，[松平健特別公演](../Page/松平健.md "wikilink")，各種[歌劇公演](../Page/歌劇.md "wikilink")，毎年大晦日有[東京電視台的](../Page/東京電視台.md "wikilink")。外國音樂劇如『[How
to Succeed in Business Without Really
Trying](../Page/:en:How_to_Succeed_in_Business_Without_Really_Trying.md "wikilink")』『[南太平洋](../Page/:en:South_Pacific_\(musical\).md "wikilink")』『[彼得潘](../Page/彼得潘.md "wikilink")』等作品亦是在新宿KOMA劇場作日本初演。

近年由於來客數減少，於2008年5月決定閉館，同年12月31日舉辦第41屆「」節目後正式走入歷史，劇場建物被拆除，與周邊土地一同進行再開發\[1\]，改建為「」，於2015年4月17日完工啟用。

## 關連項目

  - [梅田藝術劇場](../Page/梅田藝術劇場.md "wikilink") -
    大阪・梅田的阪急阪神東寶集團的劇場。又名「劇場飛天」・「梅田KOMA劇場」。

## 參考資料

## 外部連結

  - [新宿コマ劇場](https://web.archive.org/web/20100504101751/http://www.koma-sta.co.jp/)

[Category:東京都劇院](../Category/東京都劇院.md "wikilink")
[Category:東京都已不存在的建築物](../Category/東京都已不存在的建築物.md "wikilink")
[Category:阪急阪神東寶集團](../Category/阪急阪神東寶集團.md "wikilink")
[Category:歌舞伎町](../Category/歌舞伎町.md "wikilink")

1.  [](http://www.nikkei.co.jp/news/shakai/20090101AT1G3101031122008.html)　[日本經濟新聞](../Page/日本經濟新聞.md "wikilink")　2009年1月4日查閱