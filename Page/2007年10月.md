## 10月31日

  - 一名男孩供认：他玩火柴而引起了[加利福尼亚的山火](../Page/2007年10月加州山火.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7070000/newsid_7070800/7070824.stm)
  - [西班牙反恐法庭对](../Page/西班牙.md "wikilink")[马德里“3·11”爆炸案做出宣判](../Page/马德里三一一爆炸案.md "wikilink")，28名被告中有21人被判定有罪。
    [新华网](http://news.xinhuanet.com/newscenter/2007-11/01/content_6986185.htm)
  - [美国国务院负责公共外交和公共事务的副国务卿](../Page/美国国务院.md "wikilink")[卡伦·休斯辞职](../Page/卡伦·休斯.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2007-11/01/content_6989331.htm)
  - [俄罗斯](../Page/俄罗斯.md "wikilink")[萨马拉州陶里亚蒂市公交车上发生人为](../Page/萨马拉州.md "wikilink")[爆炸](../Page/爆炸.md "wikilink")，共导致8死56伤。[新浪网](http://news.sina.com.cn/w/2007-10-31/210914206008.shtml)

## 10月30日

  - [国际足联宣布](../Page/国际足联.md "wikilink")[巴西获得](../Page/巴西.md "wikilink")[2014年世界杯足球赛决赛阶段比赛的主办权](../Page/2014年世界杯足球赛.md "wikilink")，[德国成为](../Page/德国.md "wikilink")2011年女足世界杯承办国。[东方网](http://sports.eastday.com/s/20071030/u1a3197840.html)[东方网](http://sports.eastday.com/s/20071031/u1a3198162.html)

## 10月28日

  - [阿根廷现任总统](../Page/阿根廷.md "wikilink")[基什内尔的夫人](../Page/内斯托尔·基什内尔.md "wikilink")[克里斯蒂娜·费尔南德斯·德基什内尔宣布在](../Page/克里斯蒂娜·费尔南德斯·德基什内尔.md "wikilink")[总统选举中获胜](../Page/阿根廷总统.md "wikilink")。[CNET](https://web.archive.org/web/20071031025421/http://www.cnetnews.com.cn/2007/1029/589037.shtml)
    [新华网](http://news.xinhuanet.com/overseas/2007-10/29/content_6968793.htm)

## 10月27日

  - [苏丹政府与反政府派别关于](../Page/苏丹.md "wikilink")[达尔富尔问题的最后阶段和平谈判开始在利比亚](../Page/达尔富尔问题.md "wikilink")[苏尔特举行](../Page/苏尔特.md "wikilink")；同时，苏丹政府宣布单方面在达尔富尔地区停火。[新华网1](http://news.xinhuanet.com/newscenter/2007-10/28/content_6957391.htm)
    [新华网2](http://news.xinhuanet.com/newscenter/2007-10/28/content_6957403.htm)

## 10月26日

  - [2007年亞洲室內運動會在](../Page/2007年亞洲室內運動會.md "wikilink")[澳門開幕](../Page/澳門.md "wikilink")，[國際奧委會主席](../Page/國際奧委會主席.md "wikilink")[羅格特意由](../Page/羅格.md "wikilink")[北京到澳門出席](../Page/北京.md "wikilink")，進行旋風式訪澳。
  - [香港恒生指數突破](../Page/恒生指數.md "wikilink")30,000點收市。

## 10月25日

  - [乌克兰最高行政法院宣布](../Page/乌克兰最高行政法院.md "wikilink")[乌克兰国会选举的结果有效](../Page/乌克兰.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2007-10/26/content_6950819.htm)
  - [菲律宾](../Page/菲律宾.md "wikilink")[总统](../Page/菲律宾总统.md "wikilink")[格洛丽亚·阿罗约签署特赦令](../Page/格洛丽亚·阿罗约.md "wikilink")，赦免因盗窃国家财产罪而被判处终身监禁的前总统[约瑟夫·埃斯特拉达](../Page/约瑟夫·埃斯特拉达.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2007-10/25/content_6946408.htm)
  - [新加坡航空首架](../Page/新加坡航空.md "wikilink")[空中巴士A380客机首航](../Page/空中巴士A380.md "wikilink")，由[新加坡](../Page/新加坡.md "wikilink")[樟宜機場飛往](../Page/樟宜機場.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")[京斯福特·史密斯機場](../Page/京斯福特·史密斯機場.md "wikilink")，成为全球第一架投入商业运营的A380客机。[CNA](http://www.channelnewsasia.com/stories/singaporelocalnews/view/307655/1/.html)[Sydney
    Morning
    Herald](http://www.smh.com.au/news/travel/superjumbo-takes-off/2007/10/25/1192941196528.html?s_cid=rss_news)[新华网](http://news.xinhuanet.com/fortune/2007-10/25/content_6943456.htm)
  - [美國遺傳學家](../Page/美國.md "wikilink")、[DNA雙螺旋結構的發現者之一](../Page/DNA.md "wikilink")[詹姆斯·沃森從工作了](../Page/詹姆斯·沃森.md "wikilink")43年的[科爾德斯普林實驗室辭職退休](../Page/科爾德斯普林實驗室.md "wikilink")，原因是由於先前所發表的爭議性[種族言論](../Page/種族.md "wikilink")。[新華網](http://news.sina.com.tw/tech/sinacn/cn/2007-10-26/173238201313.shtml)[The
    New York
    Times](http://www.nytimes.com/2007/10/25/science/25cnd-watson.html?_r=1&ex=1351051200&en=dfc9fdcebb4c22e2&ei=5088&partner=rssnyt&emc=rss&oref=slogin)

## 10月24日

  - [中国首颗绕](../Page/中国.md "wikilink")[月](../Page/月球.md "wikilink")[人造卫星](../Page/人造卫星.md "wikilink")[嫦娥一号在](../Page/嫦娥一号.md "wikilink")[西昌卫星发射中心搭乘](../Page/西昌卫星发射中心.md "wikilink")[长征三号甲运载火箭升空](../Page/长征三号甲运载火箭.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2007-10/24/content_6936690.htm)
  - [缅甸联邦国家和平与发展委员会任命](../Page/缅甸联邦国家和平与发展委员会.md "wikilink")[登盛为新任](../Page/登盛.md "wikilink")[总理](../Page/缅甸总理.md "wikilink")，接替早前病逝的[梭温](../Page/梭温.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2007-10/24/content_6939596.htm)

## 10月23日

  - [发现号航天飞机从佛罗里达州](../Page/发现号航天飞机.md "wikilink")[肯尼迪航天中心升空](../Page/肯尼迪航天中心.md "wikilink")，飞往[国际空间站](../Page/国际空间站.md "wikilink")。[中国日报](http://www.chinadaily.com.cn/hqgj/2007-10/23/content_6200904.htm)
  - [俄罗斯空军全面换装](../Page/俄罗斯空军.md "wikilink")300架[米-28N](../Page/米-28.md "wikilink")[武装直升机](../Page/武装直升机.md "wikilink")。[人民网](http://world.people.com.cn/GB/42032/6425922.html)

## 10月22日

  - [胡锦涛当选为新一任的](../Page/胡锦涛.md "wikilink")[中国共产党中央委员会总书记](../Page/中国共产党中央委员会总书记.md "wikilink")，胡锦涛、[吴邦国](../Page/吴邦国.md "wikilink")、[温家宝](../Page/温家宝.md "wikilink")、[贾庆林](../Page/贾庆林.md "wikilink")、[李长春](../Page/李长春.md "wikilink")、[习近平](../Page/习近平.md "wikilink")、[李克强](../Page/李克强.md "wikilink")、[贺国强](../Page/贺国强.md "wikilink")、[周永康当选为](../Page/周永康.md "wikilink")[中央政治局常委](../Page/中国共产党中央政治局常务委员会.md "wikilink")。[新浪网](http://news.sina.com.cn/c/2007-10-22/113214139788.shtml)
  - [中国香港选手](../Page/中国香港.md "wikilink")[傅家俊在](../Page/傅家俊.md "wikilink")[英国](../Page/英国.md "wikilink")[阿伯丁获得](../Page/阿伯丁.md "wikilink")2007年[皇家伦敦斯诺克大奖赛冠军](../Page/皇家伦敦斯诺克大奖赛.md "wikilink")。[新浪网](http://sports.sina.com.cn/o/2007-10-22/05473240085.shtml)
  - [波兰国家选举委员会公布的新一届议会选举初步结果显示](../Page/波兰.md "wikilink")，[公民纲领党击败执政党](../Page/公民纲领党.md "wikilink")[法律与公正党赢得](../Page/法律与公正党.md "wikilink")[众议院和](../Page/众议院.md "wikilink")[参议院的多数席位](../Page/参议院.md "wikilink")。[华夏经纬网](http://www.huaxia.com/xw/gj/2007/00701376.html)[新华网](http://news.xinhuanet.com/newscenter/2007-10/22/content_6924761.htm)
  - [瑞士联邦议会國民院選舉開票結束](../Page/瑞士.md "wikilink")，[瑞士人民党獲得](../Page/瑞士人民党.md "wikilink")62席，維持了國民院最大黨的地位。[讀賣新聞](http://www.yomiuri.co.jp/world/news/20071022id21.htm)

## 10月21日

  - [奇米·雷克南获得](../Page/奇米·雷克南.md "wikilink")[2007年世界一级方程式锦标赛年度总冠军](../Page/2007年世界一级方程式锦标赛.md "wikilink")。[新浪网](http://sports.sina.com.cn/f1/2007-10-22/01333239614.shtml)
  - [瑞士舉行議會上下兩院選舉](../Page/瑞士.md "wikilink")。[NHK](https://web.archive.org/web/20071022072428/http://www3.nhk.or.jp/news/2007/10/21/k20071021000009.html)
    [新浪](http://news.sina.com.cn/w/2007-10-21/145914134297.shtml)
  - [波蘭舉行議會上下兩院選舉](../Page/波蘭.md "wikilink")。[CNN](http://edition.cnn.com/2007/WORLD/europe/10/20/poland.poll/index.html)
    [新华网](http://news.xinhuanet.com/newscenter/2007-10/21/content_6917643.htm)
  - [中國共產黨第十七次全國代表大會閉幕](../Page/中國共產黨第十七次全國代表大會.md "wikilink")。[NHK](http://www3.nhk.or.jp/news/2007/10/21/k20071021000071.html)
    [新华社](http://www.cpcnews.cn/GB/100798/6410325.html)
  - [土耳其部隊與越界進入土耳其的](../Page/土耳其.md "wikilink")[库尔德工人党游擊隊激戰](../Page/库尔德工人党.md "wikilink")
    雙方44人喪生。[中央廣播電台](https://archive.is/20070516225920/http://www.rti.org.tw/News/NewsContentHome.aspx?NewsID=87499&t=1)
  - [美国加州大火失控](../Page/2007年10月加州山火.md "wikilink")，州长[阿诺德·施瓦辛格宣布从](../Page/阿诺德·施瓦辛格.md "wikilink")[圣巴巴拉至美墨边境以北的](../Page/圣巴巴拉.md "wikilink")7个县进入紧急状态。[新华网](http://news.xinhuanet.com/newscenter/2007-10/23/content_6924777.htm)

## 10月20日

  - [2007年](../Page/2007年.md "wikilink")[世界杯欖球賽決賽](../Page/世界杯欖球賽.md "wikilink")，[南非隊以](../Page/南非欖球國家隊.md "wikilink")15-6擊敗[英格蘭奪得冠軍](../Page/英格蘭欖球國家隊.md "wikilink")。[BBC
    Sport](http://news.bbc.co.uk/sport2/hi/rugby_union/7052822.stm)
    [新华网](http://news.xinhuanet.com/sports/2007-10/21/content_6918079.htm)
  - [缅甸的](../Page/缅甸.md "wikilink")[仰光和](../Page/仰光.md "wikilink")[曼德勒两市取消因](../Page/曼德勒.md "wikilink")[2007年緬甸反軍政府示威而采取的宵禁](../Page/2007年緬甸反軍政府示威.md "wikilink")。[CNN](http://edition.cnn.com/2007/WORLD/asiapcf/10/20/myanmar.ap/index.html)
    [新华网](http://news.xinhuanet.com/world/2007-10/20/content_6915909.htm)
  - [伊朗首席核谈判代表](../Page/伊朗.md "wikilink")[拉里贾尼辞职](../Page/拉里贾尼.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2007-10/20/content_6914279.htm)
    [CNN](http://edition.cnn.com/2007/WORLD/meast/10/20/larijani.ap/index.html)
  - 前[苏联总统](../Page/苏联.md "wikilink")[戈尔巴乔夫创建](../Page/戈尔巴乔夫.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")“[社会民主同盟](../Page/社会民主同盟.md "wikilink")”，其本人被推举为该组织领导人。[21cn](https://web.archive.org/web/20071023050237/http://news.21cn.com/world/guojisaomiao/2007/10/21/3762048.shtml)

## 10月19日

  - [巴基斯坦发生针对前](../Page/巴基斯坦.md "wikilink")[总理](../Page/巴基斯坦总理.md "wikilink")[贝娜齐尔·布托的自杀式爆炸袭击](../Page/贝娜齐尔·布托.md "wikilink")，造成124人死亡，近400人受伤。[新华网](http://news.xinhuanet.com/newscenter/2007-10/19/content_6905053.htm)
  - [欧盟非正式首脑会议通过了欧盟新条约](../Page/欧盟.md "wikilink")《[里斯本条约](../Page/里斯本条约.md "wikilink")》，从而结束了欧盟长达6年的制宪进程。[新华网](http://news.xinhuanet.com/newscenter/2007-10/19/content_6907596.htm)

## 10月18日

  - [中國一度封鎖美国網路公司的](../Page/中國.md "wikilink")[搜尋引擎](../Page/搜尋引擎.md "wikilink")。[yam天空](http://news.yam.com/afp/entertain/200710/20071019871832.html)

## 10月17日

  - [2007年緬甸反軍政府示威](../Page/2007年緬甸反軍政府示威.md "wikilink")：
      - 緬甸軍政府承認有近3000人被捕，500人仍被拘留。[美聯社](https://web.archive.org/web/20071019070710/http://ap.google.com/article/ALeqM5iy-MfhLN9Q7MwtQ1VlrvexLjr2dAD8SANN300)
      - [加拿大向緬甸民主運動領袖](../Page/加拿大.md "wikilink")[昂山素季授與](../Page/昂山素季.md "wikilink")[榮譽公民榮譽](../Page/榮譽公民.md "wikilink")。[環球郵報](http://www.theglobeandmail.com/servlet/story/RTGAM.20071017.wsuukyi1017/BNStory/Front)
  - [俄罗斯恢复生产](../Page/俄罗斯.md "wikilink")[图-160](../Page/图-160.md "wikilink")[战略轰炸机](../Page/战略轰炸机.md "wikilink")。[中新网](http://www.chinanews.com.cn/gj/qqjs/news/2007/10-17/1051394.shtml)
  - [英國外交部宣佈向](../Page/英國.md "wikilink")[聯合國提出申請](../Page/聯合國.md "wikilink")，將其[南極領土面積延伸一百萬平方公里](../Page/南極.md "wikilink")，涵蓋英屬南極地區[海床](../Page/海床.md "wikilink")。[自由時報](https://web.archive.org/web/20071019073605/http://www.libertytimes.com.tw/2007/new/oct/18/today-int1.htm)[文匯報](http://paper.wenweipo.com/2007/10/18/YO0710180018.htm)[路透社](http://cnt.reuters.com/article/europeNews/idCNTChina-58920071018)[BBC新聞](http://news.bbc.co.uk/chinese/trad/hi/newsid_7040000/newsid_7049700/7049703.stm)
  - [美國國會舉行儀式](../Page/美國.md "wikilink")，為[西藏精神領袖](../Page/西藏.md "wikilink")[達賴喇嘛頒授勛章](../Page/十四世達賴喇嘛.md "wikilink")，總統[喬治布殊出席授勛儀式](../Page/喬治布殊.md "wikilink")。[星島](https://web.archive.org/web/20071018054639/http://hk.news.yahoo.com/071016/60/2ho9h.html)[自由電子報](https://web.archive.org/web/20071118123448/http://www.libertytimes.com.tw/2007/new/oct/18/today-fo1.htm)
  - [美国](../Page/美国.md "wikilink")[国家反恐中心主任](../Page/国家反恐中心.md "wikilink")[雷德辞职](../Page/雷德.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2007-10/18/content_6898432.htm)
  - [泰国](../Page/泰国.md "wikilink")[皇家空军向](../Page/泰国皇家空军.md "wikilink")[瑞典订购](../Page/瑞典.md "wikilink")12架[JAS-39](../Page/JAS-39.md "wikilink")“鹰狮”战斗机。[新华网](http://news.xinhuanet.com/newscenter/2007-10/18/content_6898489.htm)

## 10月16日

  - [2007年緬甸反軍政府示威](../Page/2007年緬甸反軍政府示威.md "wikilink")：
      - [日本宣布中止對緬援助](../Page/日本.md "wikilink")。[路透社](http://in.reuters.com/article/southAsiaNews/idINIndia-30009320071016)
  - [新西蘭發生](../Page/新西蘭.md "wikilink")[黎克特制](../Page/黎克特制.md "wikilink")6.7級[地震](../Page/地震.md "wikilink")，無人受傷。[明報](https://web.archive.org/web/20071018030928/http://hk.news.yahoo.com/071016/12/2hmh3.html)
  - [越南共产党中央总书记](../Page/越南共产党.md "wikilink")[农德孟访问](../Page/农德孟.md "wikilink")[朝鲜](../Page/朝鲜.md "wikilink")，為該黨最高領導人50年来首次。[解放网](http://www.jfdaily.com/gb/jfxww/xinwen/node1220/node33099/userobject1ai1820471.html)
  - [聯合國安全理事會五個非常任理事國產生](../Page/聯合國安全理事會.md "wikilink")：[利比亞](../Page/利比亞.md "wikilink")、[越南](../Page/越南.md "wikilink")、[布基納法索](../Page/布基納法索.md "wikilink")、[克羅地亞和](../Page/克羅地亞.md "wikilink")[哥斯大黎加](../Page/哥斯大黎加.md "wikilink")。[泰晤士報](http://www.timesonline.co.uk/tol/news/world/africa/article2674164.ece)
    [新华网](http://news.xinhuanet.com/photo/2007-10/17/content_6894569.htm)
  - 《[传统医学国际标准名词术语](../Page/传统医学国际标准名词术语.md "wikilink")》在[世界卫生组织和](../Page/世界卫生组织.md "wikilink")[国家中医药管理局联合召开的新闻发布会上颁布](../Page/国家中医药管理局.md "wikilink")。[新华网](http://news.xinhuanet.com/health/2007-10/17/content_6893241.htm)
  - [台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[華光社區召開公展說明會](../Page/華光社區.md "wikilink")

## 10月15日

  - [2007年緬甸反軍政府示威](../Page/2007年緬甸反軍政府示威.md "wikilink")：
      - 聯合國特使[易卜拉西姆·甘巴里警告必須停止拘捕異見人士](../Page/易卜拉西姆·甘巴里.md "wikilink")。[路透社](http://ca.today.reuters.com/news/newsArticle.aspx?type=topNews&storyID=2007-10-15T082807Z_01_BKK229851_RTRIDST_0_NEWS-MYANMAR-COL.XML)
  - [中国共产党](../Page/中国共产党.md "wikilink")[第十七次全国代表大会于](../Page/中国共产党第十七次全国代表大会.md "wikilink")[北京市](../Page/北京市.md "wikilink")[人民大会堂正式开幕](../Page/人民大会堂.md "wikilink")。
    [人民网](http://cpc.people.com.cn/GB/104019/104098/6377437.html)[新华网](http://www.xinhuanet.com/zhibo/20071015/)
  - 2007年[诺贝尔经济学奖揭晓](../Page/诺贝尔经济学奖.md "wikilink")，[里奥尼德·赫维克兹](../Page/里奥尼德·赫维克兹.md "wikilink")（Leonid
    Hurwicz）、[埃克里·马斯金](../Page/埃克里·马斯金.md "wikilink")（Eric S.
    Maskin）和[罗杰·梅尔森](../Page/罗杰·梅尔森.md "wikilink")（Roger B.
    Myerson）三位美国学者获得此奖项。[中国新闻网](http://www.chinanews.com.cn/cj/kong/news/2007/10-15/1049628.shtml)
  - [俄羅斯總統](../Page/俄羅斯.md "wikilink")[普京面對暗殺威脅](../Page/普京.md "wikilink")，堅持訪問[伊朗](../Page/伊朗.md "wikilink")。[法新社](https://web.archive.org/web/20071023213321/http://afp.google.com/article/ALeqM5jK1FPqicx7dLIVfUBcq5TOGqh3OA)
  - [中國共產黨第十七次全國代表大會](../Page/中國共產黨第十七次全國代表大會.md "wikilink")

## 10月14日

  - [澳洲總理](../Page/澳洲.md "wikilink")[約翰·霍華德宣布在](../Page/約翰·霍華德.md "wikilink")[11月24日舉行](../Page/11月24日.md "wikilink")[大選](../Page/2007年澳洲大選.md "wikilink")。[澳洲廣播公司](http://www.abc.net.au/news/stories/2007/10/14/2059084.htm)
  - [緬甸恢復部份](../Page/緬甸.md "wikilink")[互聯網服務](../Page/互聯網.md "wikilink")，但[英國廣播公司](../Page/英國廣播公司.md "wikilink")、[有線電視新聞網絡](../Page/有線電視新聞網絡.md "wikilink")、[博客及異議人士等網站除外](../Page/博客.md "wikilink")。[美聯社](https://web.archive.org/web/20071016071414/http://ap.google.com/article/ALeqM5iy-MfhLN9Q7MwtQ1VlrvexLjr2dAD8S95SQG0)

## 10月13日

  - [2007年緬甸反軍政府示威](../Page/2007年緬甸反軍政府示威.md "wikilink")：
      - 在聯合國特使[易卜拉西姆·甘巴里離開緬甸後](../Page/易卜拉西姆·甘巴里.md "wikilink")，數萬人參加在最大城市[仰光舉行的集會](../Page/仰光.md "wikilink")，支持軍政府。[美聯社](http://www.abc.net.au/news/stories/2007/10/13/2058865.htm)

## 10月12日

  - [美国前副总统](../Page/美国.md "wikilink")[艾伯特·戈尔和](../Page/艾伯特·戈尔.md "wikilink")[政府間氣候變化專門委員會获得](../Page/政府間氣候變化專門委員會.md "wikilink")2007年[诺贝尔和平奖](../Page/诺贝尔和平奖.md "wikilink")。[纽约时报](http://www.nytimes.com/aponline/world/AP-Nobel-Peace.html?_r=1&hp&oref=slogin)
    [明镜](http://www.spiegel.de/politik/ausland/0,1518,511041,00.html)
    [新华网](http://news.xinhuanet.com/newscenter/2007-10/12/content_6871217.htm)
  - 在教宗[本篤十六世發表批判](../Page/本篤十六世.md "wikilink")[伊斯蘭教演說一周年之際](../Page/伊斯蘭教.md "wikilink")，超過130名[穆斯林學者向其與多位基督宗教領袖發出公開信](../Page/穆斯林.md "wikilink")，呼籲促進兩教間的了解。[BBC](http://news.bbc.co.uk/2/hi/europe/7038992.stm)

## 10月11日

  - [2007年世界夏季特殊奥林匹克运动会于](../Page/2007年世界夏季特殊奥林匹克运动会.md "wikilink")[中国](../Page/中国.md "wikilink")[上海市闭幕](../Page/上海市.md "wikilink")。[新浪网](http://news.sina.com.cn/c/p/2007-10-11/210214065999.shtml)
  - [英国女作家](../Page/英国.md "wikilink")[多丽丝·莱辛获得](../Page/多丽丝·莱辛.md "wikilink")2007年[诺贝尔文学奖](../Page/诺贝尔文学奖.md "wikilink")。[中国新闻网](http://www.chinanews.com.cn/cul/news/2007/10-11/1046793.shtml)
  - [台灣總統](../Page/台灣.md "wikilink")[陳水扁接任](../Page/陳水扁.md "wikilink")[民進黨主席](../Page/民進黨.md "wikilink")。[自由時報](https://web.archive.org/web/20071013034324/http://www.libertytimes.com.tw/2007/new/oct/12/today-fo1.htm)
  - [土耳其决定召回土驻美大使](../Page/土耳其.md "wikilink")，以抗议[美国众议院外交事务委员会](../Page/美国众议院.md "wikilink")10日通过的有关把土耳其[奥斯曼帝国大批杀害](../Page/奥斯曼帝国.md "wikilink")[亚美尼亚人认定为](../Page/亚美尼亚人.md "wikilink")“[种族屠杀](../Page/种族屠杀.md "wikilink")”的议案。[新华网](http://news.xinhuanet.com/world/2007-10/12/content_6867078.htm)
  - 中國政府估計，在未來十至十五年再有四百萬人因[三峽工程要遷移他處](../Page/三峽工程.md "wikilink")。[紐約時報](http://www.nytimes.com/2007/10/12/world/asia/12china.html?ref=world)

## 10月10日

  - [俄罗斯载人飞船载着](../Page/俄罗斯.md "wikilink")3名宇航员飞向[国际空间站](../Page/国际空间站.md "wikilink")，[马来西亚外科医生](../Page/马来西亚.md "wikilink")[谢赫·穆扎法尔·舒库尔成为该国首位进入太空的](../Page/谢赫·穆扎法尔·舒库尔.md "wikilink")[宇航员](../Page/宇航员.md "wikilink")。[新浪新闻](http://tech.sina.com.cn/d/2007-10-11/16481787256.shtml)
  - 德國科學家[格哈德·埃特尔獲](../Page/格哈德·埃特尔.md "wikilink")[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")。[中国新闻网](http://www.chinanews.com.cn/gj/sjkj/news/2007/10-10/1045670.shtml)
  - [台湾庆祝](../Page/台湾.md "wikilink")[双十节](../Page/双十节.md "wikilink")，台北总统府外举行16年来首次大型[阅兵仪式](../Page/阅兵.md "wikilink")。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_7030000/newsid_7036900/7036927.stm)[自由電子報](https://web.archive.org/web/20071012235505/http://www.libertytimes.com.tw/2007/new/oct/11/today-fo4-3.htm)

## 10月9日

  - 法国科学家[阿尔贝·费尔和德国科学家](../Page/阿尔贝·费尔.md "wikilink")[彼得·格林贝格共同获得](../Page/彼得·格林贝格.md "wikilink")2007年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2007-10/09/content_6853269.htm)

## 10月8日

  - 2007年[诺贝尔生理学或医学奖揭晓](../Page/诺贝尔生理学或医学奖.md "wikilink")，两名美国科学家[马里奥·R·卡佩奇](../Page/马里奥·R·卡佩奇.md "wikilink")、[奥利弗·史密斯和英国科学家](../Page/奥利弗·史密斯.md "wikilink")[马丁·J·伊文思获得此奖项](../Page/马丁·J·伊文思.md "wikilink")。[中国新闻网](http://www.chinanews.com.cn/gj/ywdd/news/2007/10-08/1043315.shtml)
  - [联合国教科文组织官员在](../Page/联合国教科文组织.md "wikilink")[丽江宣布](../Page/丽江.md "wikilink")，[世界文化遗产丽江古城遗产保护民居修复项目荣获](../Page/世界文化遗产.md "wikilink")“联合国教科文组织亚太地区2007年遗产保护优秀奖”。[新华网](http://news.xinhuanet.com/photo/2007-10/09/content_6853989.htm)
  - [玻利维亚政府在该国北部的巴耶格兰德小镇举行盛大活动](../Page/玻利维亚.md "wikilink")，隆重纪念拉美著名革命家[切·格瓦拉牺牲](../Page/切·格瓦拉.md "wikilink")40周年。[新华网](http://news.xinhuanet.com/newscenter/2007-10/09/content_6854102.htm)

## 10月6日

  - [巴基斯坦举行](../Page/巴基斯坦.md "wikilink")[总统选举](../Page/巴基斯坦总统.md "wikilink")，现任总统[佩尔韦兹·穆沙拉夫获得压倒性票数胜出](../Page/佩尔韦兹·穆沙拉夫.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2007-10-06/215914029888.shtml)
    [新华网](http://news.xinhuanet.com/photo/2007-10/06/content_6837007.htm)
  - 英國首相[白高敦宣布不會提前大選](../Page/白高敦.md "wikilink")。[BBC](http://news.bbc.co.uk/2/hi/uk_news/politics/7031749.stm)

## 10月5日

  - [美国短跑运动员](../Page/美国.md "wikilink")[马里恩·琼斯首次承认自己在](../Page/马里恩·琼斯.md "wikilink")[2000年悉尼奥运会前服用了类固醇类](../Page/2000年悉尼奥运会.md "wikilink")[兴奋剂](../Page/兴奋剂.md "wikilink")，并宣布退役。[新浪网](http://sports.sina.com.cn/o/2007-10-06/05183210353.shtml)[中国新闻网](http://www.chinanews.com.cn/ty/kong/news/2007/10-06/1042060.shtml)
  - [全日空448號班機在大阪](../Page/全日空448號班機.md "wikilink")[伊丹機場疑因聽從航空管制指示降落錯誤跑道](../Page/伊丹機場.md "wikilink")，差點與[日本航空的飛機相撞](../Page/日本航空.md "wikilink")。[朝日新闻](https://web.archive.org/web/20071011021619/http://www.asahi.com/national/update/1006/TKY200710050416.html)

## 10月4日

  - [朝鲜最高领导人](../Page/朝鲜.md "wikilink")[金正日和](../Page/金正日.md "wikilink")[韩国总统](../Page/韩国.md "wikilink")[卢武铉在](../Page/卢武铉.md "wikilink")[朝韩首脑会晤中签署了](../Page/朝韩首脑会晤.md "wikilink")《[北南关系发展与和平繁荣宣言](../Page/北南关系发展与和平繁荣宣言.md "wikilink")》。[新华网](http://news.xinhuanet.com/photo/2007-10/04/content_6830634.htm)
  - 2007年[搞笑諾貝爾獎颁发](../Page/搞笑諾貝爾獎.md "wikilink")。[维基新闻](../Page/n:2007年搞笑諾貝爾獎.md "wikilink")

## 10月3日

  - 10月3日至2007年11月1日[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[華光社區公告公開展覽](../Page/華光社區.md "wikilink")
  - [南非](../Page/南非.md "wikilink")[约翰内斯堡以西的](../Page/约翰内斯堡.md "wikilink")[埃兰兹兰金矿发生重大事故](../Page/埃兰兹兰金矿.md "wikilink")，致使大约3200名矿工被困在井下约2200米深处。[BBC](http://news.bbc.co.uk/2/hi/africa/7027122.stm)
    [新华网](http://news.xinhuanet.com/newscenter/2007-10/04/content_6829812.htm)
  - 第六轮[朝核六方会谈第二阶段会议在北京闭幕](../Page/朝核六方会谈.md "wikilink")，会议制定并通过了《[落实共同声明第二阶段行动](../Page/落实共同声明第二阶段行动.md "wikilink")》共同文件。[新华网](http://news.xinhuanet.com/newscenter/2007-10/03/content_6828986.htm)

## 10月2日

  - [2007年緬甸反軍政府示威](../Page/2007年緬甸反軍政府示威.md "wikilink")：
      - [缅甸最高领导人](../Page/缅甸.md "wikilink")、[国家和平与发展委员会主席](../Page/国家和平与发展委员会.md "wikilink")[丹瑞大将在](../Page/丹瑞.md "wikilink")[内比都会见](../Page/内比都.md "wikilink")[联合国秘书长特使](../Page/联合国.md "wikilink")[易卜拉西姆·甘巴里](../Page/易卜拉西姆·甘巴里.md "wikilink")。[衛報](http://www.guardian.co.uk/international/story/0,,2181434,00.html)
      - [聯合國人權委員會一致通過由](../Page/聯合國人權委員會.md "wikilink")[歐盟草擬的](../Page/歐盟.md "wikilink")、強烈譴責緬甸持續使用暴力鎮壓和平示威者，包括毆打；殺害、任意拘押和強制失蹤的議案。[明報即時新聞](https://web.archive.org/web/20071025111057/http://www.mpinews.com/htm/INews/20071002/ta22353c.htm)
  - [2007年世界夏季特殊奥林匹克运动会在](../Page/2007年世界夏季特殊奥林匹克运动会.md "wikilink")[上海举行](../Page/上海.md "wikilink")。[新浪网](http://news.sina.com.cn/c/2007-10-02/191814016458.shtml)[新浪网2](http://news.sina.com.cn/c/2007-10-02/220914016667.shtml)
  - [大韩民国總統](../Page/大韩民国.md "wikilink")[盧武鉉經陸路訪問](../Page/盧武鉉.md "wikilink")[朝鲜民主主义人民共和国](../Page/朝鲜民主主义人民共和国.md "wikilink")，將和朝鲜最高领导人[金正日進行第二次](../Page/金正日.md "wikilink")[朝韩首脑会晤](../Page/朝韩首脑会晤.md "wikilink")。[中國經濟網](http://big5.ce.cn/xwzx/gjss/gdxw/200710/02/t20071002_13116825.shtml)
  - [巴基斯坦](../Page/巴基斯坦.md "wikilink")[總統選舉前夕](../Page/2007年巴基斯坦總統選舉.md "wikilink")，前[總理](../Page/巴基斯坦總理.md "wikilink")[貝娜齊爾·布托獲特赦](../Page/貝娜齊爾·布托.md "wikilink")。另外情報部門主管[基亞尼獲委任為陸軍副總參謀長](../Page/基亞尼.md "wikilink")。[法新社](http://afp.google.com/article/ALeqM5ha_7pgfWFIlYYUUhiYmnz0AwiB0Q)[洛杉磯時報](http://www.latimes.com/news/nationworld/world/la-fg-pakistan3oct03,1,3231164.story?coll=la-headlines-world)

## 10月1日

  - [2007年緬甸反軍政府示威](../Page/2007年緬甸反軍政府示威.md "wikilink")：
      - 4000多名被捕的僧侶將被送離最大城市[仰光](../Page/仰光.md "wikilink")。[BBC](http://news.bbc.co.uk/2/hi/asia-pacific/7022437.stm)
      - 根據一位變節的軍官表示，示威已經造成數千人死亡。外交界人士指出示威已經失敗。[每日郵報](http://www.dailymail.co.uk/pages/live/articles/news/worldnews.html?in_article_id=484903&in_page_id=1811&ct=5)
      - 總部設於[奧斯陸的](../Page/奧斯陸.md "wikilink")「[緬甸民主之音](../Page/緬甸民主之音.md "wikilink")」發放了據稱攝於周日仰光的片段，畫面可見一具身穿僧袍、遍體鱗傷的屍體伏屍河中。[美聯社](http://hosted.ap.org/dynamic/stories/M/MYANMAR_DEAD__MISSING?SITE=SCAND&SECTION=HOME&TEMPLATE=DEFAULT)
  - [泰国](../Page/泰国.md "wikilink")[国家安全委员会主席](../Page/国家安全委员会.md "wikilink")[颂提辞职](../Page/颂提.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2007-10-01/202814012960.shtml)
  - 澳門過千輛[電單車及遊行人士到街頭慢駛及](../Page/電單車.md "wikilink")[2007年澳門十一遊行](../Page/2007年澳門十一遊行.md "wikilink")。[雅虎香港](https://archive.is/20130105231049/http://hk.news.yahoo.com/070930/12/2gmqd.html)
  - [瑞銀](../Page/瑞銀.md "wikilink")（UBS）10月1日發表報告稱，受美國次級住房抵押貸款市場危機影響，銀行虧損額超過40億瑞士法郎（30.4億美元）。[BBC
    中文網](http://news.bbc.co.uk/chinese/trad/hi/newsid_7020000/newsid_7021800/7021881.stm)
  - [日本郵政公社实施民营化](../Page/日本郵政公社.md "wikilink")，分拆成[控股公司](../Page/控股公司.md "wikilink")[日本邮政株式会社](../Page/日本邮政株式会社.md "wikilink")，及其旗下邮政事业公司、邮局公司、邮政储蓄银行、简易生命保险公司共五家公司。[讀賣新聞](http://www.yomiuri.co.jp/atmoney/news/20070930it17.htm)[中国新闻网](http://www.chinanews.com.cn/gj/yt/news/2007/10-01/1040833.shtml)
  - [烏克蘭](../Page/烏克蘭.md "wikilink")[國會選舉](../Page/2007年烏克蘭國會選舉.md "wikilink")，由前總理[季莫申科領導的](../Page/季莫申科.md "wikilink")[季莫申科集團和總統](../Page/季莫申科集團.md "wikilink")[尤先科領導的](../Page/尤先科.md "wikilink")[我們的烏克蘭暫時領先於](../Page/我們的烏克蘭.md "wikilink")[亞努科維奇的](../Page/亞努科維奇.md "wikilink")[地區黨和](../Page/地區黨.md "wikilink")[烏克蘭共產黨的聯盟](../Page/烏克蘭共產黨.md "wikilink")。[彭博](http://www.bloomberg.com/apps/news?pid=20601085&sid=aI5gcWG1L5fE&refer=europe)[烏克蘭中央選舉委員會](https://web.archive.org/web/20071027154333/http://www.cvk.gov.ua/vnd2007/w6p300pt001f01%3D600.html)
  - [俄羅斯總統](../Page/俄羅斯.md "wikilink")[普京同意以](../Page/普京.md "wikilink")[聯合俄羅斯黨名義參加](../Page/聯合俄羅斯.md "wikilink")12月舉行的[2007年俄羅斯國家杜馬選舉](../Page/2007年俄羅斯國家杜馬選舉.md "wikilink")，並不排除任[總理的可能性](../Page/俄羅斯總理.md "wikilink")。[BBC中文網](http://news.bbc.co.uk/chinese/simp/hi/newsid_7020000/newsid_7022600/7022641.stm)

[Category:2007年](../Category/2007年.md "wikilink")
[Category:10月](../Category/10月.md "wikilink")