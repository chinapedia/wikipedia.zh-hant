**羅曼·费奥多罗维奇·馮·恩琴－史登伯格**[男爵](../Page/男爵.md "wikilink")（，），[波罗的海德意志人](../Page/波罗的海德意志人.md "wikilink")，[沙俄陸軍](../Page/俄羅斯帝國陸軍.md "wikilink")[中將](../Page/中將.md "wikilink")，於[俄国内战期間為](../Page/俄国内战.md "wikilink")[白军作戰](../Page/俄国白军.md "wikilink")，被敵軍稱為「瘋狂男爵」。

## 生平

恩琴出生于[奥地利](../Page/奥地利.md "wikilink")[格拉茨](../Page/格拉茨.md "wikilink")，自幼在[爱沙尼亚首府](../Page/爱沙尼亚.md "wikilink")[列威利由](../Page/塔林.md "wikilink")[俄國继父抚养长大](../Page/俄罗斯帝国.md "wikilink")。1905年在[圣彼得堡接受军事教育后](../Page/圣彼得堡.md "wikilink")，恩琴被派往[西伯利亚服役](../Page/西伯利亚.md "wikilink")，並参加[日俄战争](../Page/日俄战争.md "wikilink")，期間逐渐习惯于[游牧民族的生活方式](../Page/游牧民族.md "wikilink")。

1917年[十月革命爆发](../Page/十月革命.md "wikilink")，在[俄國內戰期间](../Page/俄國內戰.md "wikilink")，恩琴随其长官[格里戈里·米哈伊洛维奇·谢苗诺夫拒绝参加](../Page/格里戈里·米哈伊洛维奇·谢苗诺夫.md "wikilink")[高尔察克的白军](../Page/高尔察克.md "wikilink")，而獨自與[红军作战](../Page/苏联红军.md "wikilink")。恩琴因其对敌我雙方的残酷手段，而得到了「血腥男爵」和「疯狂男爵」的外号。他宣稱要「灭绝所有的[犹太人和](../Page/犹太人.md "wikilink")[共产党人](../Page/共产党.md "wikilink")」，支持[米哈伊尔大公重建](../Page/米哈伊尔大公.md "wikilink")[罗曼诺夫王朝](../Page/罗曼诺夫王朝.md "wikilink")。在这段期間，谢苗诺夫希望能在[日本支持下](../Page/日本.md "wikilink")，於[贝加尔湖以东地区建立一個独立王国](../Page/贝加尔湖.md "wikilink")，甚至为此而抢劫[协约国通过](../Page/协约国.md "wikilink")[西伯利亚大铁路送给高尔察克的補给](../Page/西伯利亚大铁路.md "wikilink")，间接导致了高尔察克的溃败。

1920年，恩琴同谢苗诺夫决裂。他有一个疯狂的计划：先扶植[清朝](../Page/清朝.md "wikilink")[宣统帝复辟](../Page/宣统帝.md "wikilink")，再将[远东统一在其领导下](../Page/远东.md "wikilink")，繼而以此作基地反攻西方世界。

1921年2月，应[博克多汗邀请](../Page/博克多汗.md "wikilink")，恩琴率800人[亚洲骑兵师进入](../Page/亚洲骑兵师.md "wikilink")[蒙古](../Page/占領外蒙古.md "wikilink")。在日本支持下，他很快击败[高在田率领的中国军](../Page/高在田.md "wikilink")，进軍外蒙古首都[库伦](../Page/乌兰巴托.md "wikilink")，扶持博克多汗复位，重建[大蒙古国](../Page/博克多汗国.md "wikilink")。博克多汗册封恩琴为[亲王](../Page/親王_\(東亞\).md "wikilink")，然而此时期的博克多汗没有实权，大蒙古国实际上由恩琴率领的俄国白军控制。在其5個月的統治下，他以高壓手段鎮壓其反對者，尤其是[蘇共支持者](../Page/苏联共产党.md "wikilink")。恩琴自称[成吉思汗转世](../Page/成吉思汗.md "wikilink")，并被十三世[达赖喇嘛](../Page/达赖喇嘛.md "wikilink")[土登嘉措认定为](../Page/土登嘉措.md "wikilink")[大黑天的转世](../Page/大黑天.md "wikilink")。他還提出[泛蒙古主義](../Page/泛蒙古主義.md "wikilink")，主張使博克多汗重建[蒙古帝國](../Page/蒙古帝国.md "wikilink")。1921年7月，他着手襲擊俄国和外蒙古的边疆，尤其是[蒙古人民革命党控制的](../Page/蒙古人民革命党.md "wikilink")[恰克圖](../Page/恰克图.md "wikilink")，但其攻势不久後就在红军的进攻下失败，並使他在7月6日被逐出[库伦](../Page/库伦.md "wikilink")。8月21日，他被其蒙古盟友出卖、逮捕，隨之被送交红军。

1921年9月15日，经过6小時的[擺樣子公審后](../Page/摆样子公审.md "wikilink")，恩琴被判以[反革命罪](../Page/反革命.md "wikilink")，在[新西伯利亚被处决](../Page/新西伯利亚.md "wikilink")。

## 注釋

## 参考文献

## 参见

  - [蒙古國歷史](../Page/蒙古國歷史.md "wikilink")

{{-}}

[Category:蒙古国历史](../Category/蒙古国历史.md "wikilink")
[Category:俄國貴族](../Category/俄國貴族.md "wikilink")
[Category:俄罗斯军事人物](../Category/俄罗斯军事人物.md "wikilink")
[Category:軍閥](../Category/軍閥.md "wikilink")
[Category:俄羅斯反共主義者](../Category/俄羅斯反共主義者.md "wikilink")
[Category:白俄](../Category/白俄.md "wikilink")
[Category:德國裔俄羅斯人](../Category/德國裔俄羅斯人.md "wikilink")
[Category:波罗的海德意志人](../Category/波罗的海德意志人.md "wikilink")
[Category:被蘇聯處決者](../Category/被蘇聯處決者.md "wikilink")
[Category:被處決的俄羅斯人](../Category/被處決的俄羅斯人.md "wikilink")
[Category:塔林人](../Category/塔林人.md "wikilink")
[Category:格拉茨人](../Category/格拉茨人.md "wikilink")