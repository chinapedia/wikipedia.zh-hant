[Chocolate_brownies.jpg](https://zh.wikipedia.org/wiki/File:Chocolate_brownies.jpg "fig:Chocolate_brownies.jpg")
**巧克力布朗尼**（）又稱為**布朗尼**、**[波士頓布朗尼](../Page/波士頓.md "wikilink")**，是一種小塊、濃味、像[餅乾的](../Page/餅乾.md "wikilink")[巧克力](../Page/巧克力.md "wikilink")[蛋糕](../Page/蛋糕.md "wikilink")，以它富含的[巧克力色](../Page/巧克力.md "wikilink")（brown）而得名。有時會覆上一層[乳脂軟糖](../Page/乳脂軟糖.md "wikilink")（fudge），或是在[蛋糕體中混著](../Page/蛋糕.md "wikilink")[核桃一類的乾果](../Page/核桃.md "wikilink")，和碎片（[巧克力](../Page/巧克力.md "wikilink")、[奶油](../Page/奶油.md "wikilink")[糖果](../Page/糖果.md "wikilink")、[花生奶油或其他](../Page/花生.md "wikilink")）。而一般相信布朗尼首次問世是在1897年[Sears
and Roebuck百貨公司目錄中](../Page/Sears_and_Roebuck.md "wikilink").
布朗尼通常一層巧克力糖衣或糖霜，也可能是增添[香草或](../Page/香草.md "wikilink")[薄荷香氣的](../Page/薄荷.md "wikilink")[糖衣](../Page/糖衣.md "wikilink")。

布朗尼的發明就像很多的美食，很可能只是一個意外的結果，也許是製作巧克力蛋糕時太過粗心，忘記加入像是[發粉](../Page/發粉.md "wikilink")、[泡打粉一類的](../Page/泡打粉.md "wikilink")[發酵劑](../Page/發酵劑.md "wikilink")，今日的製作[食譜則有許多變化](../Page/食譜.md "wikilink")，而比較簡單的版本通常被用作烹飪教學或美食入門導引使用。

有些場合上桌方式是將布朗尼加熱後，配合[冰淇淋成盤](../Page/冰淇淋.md "wikilink")（這種吃法又稱為「時尚的布朗尼」，''），有時會加上打發的[稀奶油](../Page/稀奶油.md "wikilink")，在[餐廳的餐後](../Page/餐廳.md "wikilink")[甜點尤其常見](../Page/甜點.md "wikilink")。今日全美國的布朗尼已蔚為風潮，因此在[歐洲各大](../Page/歐洲.md "wikilink")[烘焙店都可尋得蹤跡](../Page/烘焙.md "wikilink")，在[日本甚至成為某些](../Page/日本.md "wikilink")[主題餐廳的招牌新秀](../Page/主題餐廳.md "wikilink")。

另一種[美國的點心](../Page/美國.md "wikilink")**Blondies**是以[黑糖或](../Page/黑糖.md "wikilink")[紅糖取代](../Page/紅糖.md "wikilink")[可可粉](../Page/可可粉.md "wikilink")，有時會改用[白巧克力](../Page/白巧克力.md "wikilink")、碎片或乾果。

## 參見

  - [棕精灵](../Page/棕精灵.md "wikilink")

## 外部連結

  - [Brownie recipe](http://www.myhomecooking.net/brownies) 食谱，With lots
    of pictures and step-by-step instructions

  - [Cooking For Engineers: Dark Chocolate
    Brownies](http://www.cookingforengineers.com/article.php?id=158)

[Category:美國食品](../Category/美國食品.md "wikilink")
[Category:西方甜食](../Category/西方甜食.md "wikilink")
[Category:零食](../Category/零食.md "wikilink")
[Category:英語借詞](../Category/英語借詞.md "wikilink")