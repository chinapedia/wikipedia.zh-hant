**不傷害**（，，），又譯**不殺**、**不殺生**、**不害**，原意是指对一切[有情不加以伤害](../Page/有情.md "wikilink")，為[非暴力之意](../Page/非暴力.md "wikilink")。源自古印度宗教，由[印度教](../Page/印度教.md "wikilink")、[耆那教及](../Page/耆那教.md "wikilink")[佛教承襲下來](../Page/佛教.md "wikilink")。佛教的[五戒中](../Page/五戒.md "wikilink")，以不害為首。

## 詞源

「不害」的原詞 *ahiṃsā* 源於[梵文的字根](../Page/梵文.md "wikilink") *hiṃs*，意思就是擊打；而
*hiṃsā* 則指受傷或傷害，再加上相反前綴 *a*- 就成為了
*ahiṃsā*，用以表示「不害」或「[非暴力](../Page/非暴力.md "wikilink")」\[1\]\[2\]。

## 概論

不害代表了不[杀害和不伤害](../Page/杀害.md "wikilink")。不害理念同时尊重[生命](../Page/生命.md "wikilink")，尊重[思想](../Page/思想.md "wikilink")、[言语和](../Page/言语.md "wikilink")[行为的正直](../Page/行为.md "wikilink")，为人类自然和万物服务。有關「不害」的概念通行於[印度次大陸的諸宗教](../Page/印度次大陸.md "wikilink")，包括：其對象亦不只限於人類，並包對所有非人類的動物及其他眾生仁慈與非暴力：它尊重生命為一整全體，並相信所有生物均相通。
不害理念被圣雄[甘地教导给他的追随者](../Page/甘地.md "wikilink")\[3\]。
不害的理念同時包括了避免在語言上或行為上的暴力，除非是基於[自衛的需要](../Page/自衛.md "wikilink")，以彰顯靈性的強壯，和避免所有各種暴力所帶來的相隨[業力](../Page/業力.md "wikilink")。

起源自吠陀時代，在《[夜柔吠陀](../Page/夜柔吠陀.md "wikilink")》等婆羅門教經典開始使用這個字。婆羅門教《[广林奥义书](../Page/奧義書.md "wikilink")》所指不害意为断灭轮回的五种解脱方法之一。

## 佛教

說一切有部將不害視為是一種善的[心所](../Page/心所.md "wikilink")，列入[大善地法中](../Page/大善地法.md "wikilink")\[4\]。

《[大乘广五蕴论](../Page/大乘广五蕴论.md "wikilink")》曰：“-{云}-何不害？谓害对治，以悲为性。谓由悲故，不害群生，是无瞋分，不损恼为业。”

## 注释

## 參看

  - [非暴力](../Page/非暴力.md "wikilink")

[Category:佛教哲學](../Category/佛教哲學.md "wikilink")
[Category:心所](../Category/心所.md "wikilink")
[Category:印度教哲學概念](../Category/印度教哲學概念.md "wikilink")
[Category:大善地法](../Category/大善地法.md "wikilink")
[Category:佛教術語](../Category/佛教術語.md "wikilink")

1.  {{ cite
    web|url=<http://www.sanskrit.org/www/Hindu%20Primer/nonharming_ahimsa.html>
    |title=? |publisher=A Hindu Primer
    |author=\[<http://www.sanskrit.org/www/shukavak.htm> Shukavak N.
    Dasa |deadurl=yes
    |archiveurl=<https://web.archive.org/web/20110408135457/http://www.sanskrit.org/www/Hindu%20Primer/nonharming_ahimsa.html>
    |archivedate=2011-04-08 }}
2.  [Sanskrit dictionary
    reference](http://www.sanskrit-lexicon.uni-koeln.de/cgi-bin/monier/serveimg.pl?file=/scans/MWScan/MWScanjpg/mw0125-ahalyA.jpg)
3.
4.  玄奘譯《阿毘達磨大毘婆沙論》卷42：「大善地法有十種：一信，二精進，三慚，四愧，五無貪，六無瞋，七輕安，八捨，九不放逸，十不害。」