**SkyOS** 是一个[操作系统](../Page/操作系统.md "wikilink")，由[Robert
Szeleney](../Page/Robert_Szeleney.md "wikilink")
开发。它开始於1996年，但是在版本5.0时完全重新改写。现在软件还在开发和测试中。SkyOS原先為[自由軟體](../Page/自由軟體.md "wikilink")，使用大多数的开源软件，但是后来变成[私有](../Page/專有軟體.md "wikilink")。作者已于2009年1月30日宣布暂停开发。

上个月，Szeleney突然将版本号6947的 SkyOS 5.0 beta免费放出下载，提供了一个用户名和序列号（user: public /
serial:
4Q7W5-HTRRW-6WYHW-45KW7-XQLXL）。但目前网站无法访问，所有感兴趣的人只能从镜像网站[下载](https://web.archive.org/web/20131010224918/http://0c24f36ecb279f29ad14-1f21e6e3949f54414246574a4c17e5a1.r93.cf1.rackcdn.com/skyos_6947.rar)（439MB，[BT地址](https://dl.dropboxusercontent.com/u/3906923/skyos_6947.torrent)）了。

## 外部链接

  - [SkyOS Home Page](http://www.skyos.org/) — SkyOS 官方站

  - [SkyOS Tour](http://www.skyos.org/?q=node/414) — SkyOS 教程

  - [Interview](http://www.osnews.com/story.php?news_id=185) — Interview
    with Robert Szeleney (2001)

  - [Interview
    \#2](https://web.archive.org/web/20060913222751/http://www.techimo.com/articles/index.pl?photo=154)
    — Interview with SkyOS Staff (2004)

  - [Interview \#3](http://www.osnews.com/story.php?news_id=7748) —
    Interview with Robert Szeleney (2004)

  - [SkyOS Wiki](http://www.sky-apps.info) — SkyOS Wiki

  - [News about SkyOS at
    OSNews.com](http://www.osnews.com/search.php?search=skyos)

  -
[Category:作業系統](../Category/作業系統.md "wikilink")