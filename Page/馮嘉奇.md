**馮嘉奇**（**Fung Ka
Ki**，1977年9月19日－），生於[香港](../Page/香港.md "wikilink")，已退役[香港足球運動員](../Page/香港.md "wikilink")，前[香港足球代表隊成員](../Page/香港足球代表隊.md "wikilink")，司職[後衛](../Page/後衛_\(足球\).md "wikilink")，畢業於[英國華威大學工商管理碩士學位](../Page/華威大學.md "wikilink")，[香港大學運動科學及康樂管理學士學位](../Page/香港大學.md "wikilink")。

## 球員生涯

馮嘉奇生於[香港](../Page/香港.md "wikilink")，就讀居所[屯門](../Page/屯門.md "wikilink")[友愛邨的](../Page/友愛邨.md "wikilink")[順德聯誼總會何日東小學](../Page/順德聯誼總會何日東小學.md "wikilink")，其後獲小學體育老師，推薦入讀[賽馬會體藝中學](../Page/賽馬會體藝中學.md "wikilink")。馮嘉奇中學四年級被挑選進入[香港體育學院](../Page/香港體育學院.md "wikilink")「可口可樂足球精英運動員」訓練。於1996年馮嘉奇獲得[香港體育學院最佳足球運動員](../Page/香港體育學院.md "wikilink")。在1997年[香港體育學院足球部解散](../Page/香港體育學院.md "wikilink")，馮嘉奇與[黃龍輝](../Page/黃龍輝.md "wikilink")、[楊偉傑及](../Page/楊偉傑.md "wikilink")[黃雅樂一同加盟](../Page/黃雅樂.md "wikilink")[五一七](../Page/五一七足球會.md "wikilink")，展開其職業足球員生涯，同年入選[香港奧運代表隊先後參加](../Page/香港奧運足球代表隊.md "wikilink")[中國全運會](../Page/中華人民共和國全國運動會.md "wikilink")、[亞運會及](../Page/亞洲運動會.md "wikilink")[奧運足球預賽並且擔任隊長](../Page/奧林匹克運動會.md "wikilink")，先後效力[快譯通](../Page/花花足球會.md "wikilink")、[流浪](../Page/香港流浪足球會.md "wikilink")、[傑志](../Page/傑志.md "wikilink")、[四海](../Page/四海體育會.md "wikilink")、[淦源等球隊](../Page/淦源足球會.md "wikilink")，以隊長身份帶領球隊在季前熱身賽擊敗[意大利球隊](../Page/意大利.md "wikilink")[AC米蘭及](../Page/AC米蘭.md "wikilink")[英超球隊](../Page/英超.md "wikilink")[紐卡素](../Page/紐卡素足球會.md "wikilink")。\[1\]

## 退役生涯

由於馮嘉奇曾為[香港代表隊成員加上於英國華威大學工商管理碩士學](../Page/香港足球代表隊.md "wikilink")，退役後於2010年8月獲薦成為當時[英超球會](../Page/英格蘭超級足球聯賽.md "wikilink")[伯明翰的財務總監](../Page/伯明翰足球會.md "wikilink")，到2005年8月與[香港有線電視簽約成為足球評述員](../Page/香港有線電視.md "wikilink")，\[2\]2015年10月，馮嘉奇擔任在中、港兩地主辦[英超大師賽的賽事搞手](../Page/英超.md "wikilink")\[3\]。2016年夏天，馮嘉奇在大學同學推薦下回港發展，擔任[香港超級聯賽球會](../Page/香港超級聯賽.md "wikilink")[理文流浪行政總監](../Page/香港流浪足球會.md "wikilink")。\[4\]到2016/17球季，理文集團取得[流浪足球會管理權直到季尾把會籍交還](../Page/香港流浪足球會.md "wikilink")[流浪自立門戶續戰](../Page/香港流浪足球會.md "wikilink")[港超聯](../Page/港超聯.md "wikilink")，當中馮嘉奇兼任領隊和行政總裁，在教練團採英式領隊制下設計訓練教案，由教練團成員在場上執行。

## 個人學歷

馮嘉奇第一次中學會考只得8分，體育拿D級，其餘各科獲合格，他沒有就此放棄，選擇重讀中五。
馮嘉奇第二次會考得12分，由於運動成績優異，經常為校增光，獲校長[張灼祥批准](../Page/張灼祥.md "wikilink")，原校升讀中六。同年，隨即為[賽馬會體藝中學奪得首個](../Page/賽馬會體藝中學.md "wikilink")[全港學屆精英足球賽冠軍](../Page/香港學界足球賽事.md "wikilink")，決賽對手為傳統足球名校[聖若瑟書院](../Page/聖若瑟書院.md "wikilink")。[香港高級程度會考](../Page/香港高級程度會考.md "wikilink")，馮嘉奇英文與經濟兩科合格，得[香港專業教育學院柴灣分校取錄](../Page/香港專業教育學院柴灣分校.md "wikilink")，選修讀旅遊管理高級文憑課程。在學期間，為[柴灣科技學院奪得首個](../Page/香港專業教育學院柴灣分校.md "wikilink")[香港大專體育協會](../Page/香港大專體育協會.md "wikilink")(1998-1999年度)最佳男子運動員獎項。2000年，馮嘉奇完成旅遊管理高級文憑課程。畢業後，馮嘉奇獲院長[劉偉成引薦到](../Page/劉偉成.md "wikilink")[英國華威大學攻讀工商管理碩士](../Page/華威大學.md "wikilink")，同時獲得[香港足球總會主席](../Page/香港足球總會.md "wikilink")[許晉奎主動資助](../Page/許晉奎.md "wikilink")。原本已被[香港城市大學](../Page/香港城市大學.md "wikilink")(市場學管理學系)收取的馮嘉奇，選擇負笈[英倫](../Page/英倫.md "wikilink")。2002年，馮嘉奇完成[英國華威大學攻讀工商管理碩士](../Page/華威大學.md "wikilink")。畢業返港，馮嘉奇獲得[香港大學](../Page/香港大學.md "wikilink")「運動員獎學金計劃」身份，入讀運動科學及康樂管理學士課程。2005年，馮嘉奇完成[香港大學運動科學及康樂管理學士](../Page/香港大學.md "wikilink")。

## 幕前工作

2002年，馮嘉奇擔任[2002年世界盃客席評述](../Page/2002年世界盃足球賽.md "wikilink")。2005年，馮嘉奇正式加入[香港有線電視](../Page/香港有線電視.md "wikilink")，主要評述[德甲賽事](../Page/德甲.md "wikilink")。[2006年德國世界盃](../Page/2006年世界杯足球赛.md "wikilink")，馮嘉奇為[有線電視世界盃節目](../Page/有線電視.md "wikilink")80日環遊世界《四小強四圍碌》擔任主持，到訪晉級世界盃決賽週之32個參賽國家，走遍及全球6大洲，進行拍攝工作。不過，事前與[傑志提前解約](../Page/傑志.md "wikilink")，而一度惹起爭論。[2008年北京奧運會](../Page/2008年夏季奥林匹克运动会.md "wikilink")，馮嘉奇為[有線電視節目](../Page/有線電視.md "wikilink")《四小強四圍碌繼續碌》擔任主持，到訪全[中國各個省份](../Page/中國.md "wikilink")，拍攝火炬在[中國傳遞的花絮消息](../Page/中國.md "wikilink")。

## 個人著作

  - 《馮奇的足球旅程》，經濟日報 WHY，2008年，ISBN 978-962-678-532-4

## 家庭生活

馮嘉奇太太是前[無綫電視新聞主播](../Page/無綫電視.md "wikilink")[盤翠瑩](../Page/盤翠瑩.md "wikilink")。

## 參考資料

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
## 外部連結

  - [馮嘉奇 Facebook](http://www.facebook.com/fungkaki)
  - [馮奇的足球旅程](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626785324)
  - [香港足總球員資料：馮嘉奇](http://www.hkfa.com/zh-hk/player_view.php?player_id=65)
  - [壴學堂名人專訪：波牛碩士馮嘉奇](https://web.archive.org/web/20081009042615/http://education.atnext.com/index.php?fuseaction=Article.View&articleID=6587134&issueID=20061201)

[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港有線電視足球評述員](../Category/香港有線電視足球評述員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:馮姓](../Category/馮姓.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:香港體院足球運動員](../Category/香港體院足球運動員.md "wikilink")
[Category:五一七球員](../Category/五一七球員.md "wikilink")
[Category:流浪球員](../Category/流浪球員.md "wikilink")
[Category:傑志球員](../Category/傑志球員.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")
[Category:快譯通球員](../Category/快譯通球員.md "wikilink")
[Category:四海球員](../Category/四海球員.md "wikilink")
[Category:淦源球員](../Category/淦源球員.md "wikilink")
[Category:香港專業教育學院校友](../Category/香港專業教育學院校友.md "wikilink")
[Category:賽馬會體藝中學校友](../Category/賽馬會體藝中學校友.md "wikilink")
[Category:香港足球主教練](../Category/香港足球主教練.md "wikilink")
[F](../Category/華威大學校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")

1.  [【港超專版】馮奇英超啟示：要搞賭波合化法](http://hk.on.cc/hk/bkn/cnt/sport/20171025/bkn-20171025000626042-1025_00882_001.html)
    on.cc東網 2017年10月25日
2.  [流浪變革組千萬大軍](http://www.metrohk.com.hk/pda/pda_detail.php?section=daily&id=311020)
    [都市日報](../Page/都市日報.md "wikilink") 6/6/2016
3.  [英超大師賽　史高斯科拿入世界波
    粉絲癲晒](http://hk.on.cc/hk/bkn/cnt/sport/20151015/bkn-20151015213854683-1015_00882_001.html)
    on.cc東網 2015年10月15日
4.  [【港足日與夜．馮嘉奇】嘆香港球圈十年如一日　要為球員尋回尊嚴](https://www.hk01.com/%E9%AB%94%E8%82%B2/85339/-%E6%B8%AF%E8%B6%B3%E6%97%A5%E8%88%87%E5%A4%9C-%E9%A6%AE%E5%98%89%E5%A5%87-%E5%98%86%E9%A6%99%E6%B8%AF%E7%90%83%E5%9C%88%E5%8D%81%E5%B9%B4%E5%A6%82%E4%B8%80%E6%97%A5-%E8%A6%81%E7%82%BA%E7%90%83%E5%93%A1%E5%B0%8B%E5%9B%9E%E5%B0%8A%E5%9A%B4)
    [香港01](../Page/香港01.md "wikilink") 2017-04-19