|                                                                               |
| :---------------------------------------------------------------------------: |
|                                 **巴黎券商公會指數**                                  |
|                                     ****                                      |
| [CAC_40.png](https://zh.wikipedia.org/wiki/File:CAC_40.png "fig:CAC_40.png") |
|                                      概要                                       |
|                                   **發行日期**                                    |
|                **[識別代碼](../Page/:en:ISO_10383.md "wikilink")**                |
|                 **[FX識別代碼](../Page/金融資訊交換協定.md "wikilink")**                  |
|                                     交易所資訊                                     |
|                                   **交易所全銜**                                   |
|             **[交易所BIC識別代碼](../Page/:en:ISO_9362.md "wikilink")**              |
|                                                                               |

**CAC40指數**（；中文又稱「**巴黎券商公會指數**」）是[法国](../Page/法国.md "wikilink")[巴黎证券交易所市值前](../Page/巴黎证券交易所.md "wikilink")40大企業的股票报价指数，為法國首要之[股價指數](../Page/股價指數.md "wikilink")，與[德国DAX指数並列為歐洲](../Page/德国DAX指数.md "wikilink")2大指標股價指數。其原文中的「」為「」的簡寫，意為「連續輔助報價」，是指巴黎證券交易所在1980至1990年代間所採用的電子交易系統。

## 上市公司

CAC40指數由巴黎證券交易所的市值前40大上市企業之股票組成。現有成分股包括\[1\]：

  - [雅高集团](../Page/雅高集团.md "wikilink")（）

  - [液化空气集团](../Page/液化空气集团.md "wikilink")（）

  - [空中客车集团](../Page/空中客车集团.md "wikilink")（）

  - [安赛乐米塔尔](../Page/安赛乐米塔尔.md "wikilink")（）

  - [源讯](../Page/源讯.md "wikilink")（）

  - [安盛](../Page/安盛.md "wikilink")（）

  - [法国巴黎銀行](../Page/法国巴黎銀行.md "wikilink")（）

  - （）

  - [凱捷](../Page/凱捷.md "wikilink")（）

  - [家樂福](../Page/家樂福.md "wikilink")（）

  - [法国农业信贷银行](../Page/法国农业信贷银行.md "wikilink")（）

  - [达能](../Page/达能.md "wikilink")（）

  - （；原名法國天然氣蘇伊士集團 / ）

  - （）

  - [开云集团](../Page/开云集团.md "wikilink")（）

  - （）

  - （）

  - [萊雅](../Page/萊雅.md "wikilink")（）

  - [酩悅·軒尼詩－路易·威登集團](../Page/酩悅·軒尼詩－路易·威登集團.md "wikilink")（）

  - [米其林](../Page/米其林.md "wikilink")（）

  - [Orange](../Page/Orange_\(公司\).md "wikilink")（原名[法國電信](../Page/法國電信.md "wikilink")
    / ）

  - [保乐力加](../Page/保乐力加.md "wikilink")（）

  - [标致雪铁龙集团](../Page/标致雪铁龙集团.md "wikilink")（）

  - [阳狮](../Page/阳狮.md "wikilink")（）

  - [雷诺](../Page/雷诺.md "wikilink")（）

  - [賽峰集團](../Page/賽峰集團.md "wikilink")（）

  - [圣戈班](../Page/圣戈班.md "wikilink")（）

  - [赛诺菲](../Page/赛诺菲.md "wikilink")（）

  - [施耐德电气](../Page/施耐德电气.md "wikilink")（）

  - [法国兴业银行](../Page/法国兴业银行.md "wikilink")（）

  - （）

  - [索爾維](../Page/索尔维集团.md "wikilink")（）

  - [意法半导体](../Page/意法半导体.md "wikilink")（）

  - （）

  - [道达尔](../Page/道达尔.md "wikilink")（）

  - （）

  - [法雷奧](../Page/法雷奧.md "wikilink")（）

  - [威立雅環境](../Page/威立雅環境.md "wikilink")（）

  - （）

  - [维旺迪](../Page/维旺迪.md "wikilink")（）

## 參考資料

## 外部鏈結

  - [CAC40指數介紹](https://www.euronext.com/en/products/indices/FR0003500008-XPAR/market-information)
    - [泛歐交易所網站](../Page/泛歐交易所.md "wikilink")

[CAC_40](../Category/CAC_40.md "wikilink")
[Category:欧洲股市指数](../Category/欧洲股市指数.md "wikilink")
[Category:法国经济](../Category/法国经济.md "wikilink")
[歐](../Category/上市公司列表.md "wikilink")

1.