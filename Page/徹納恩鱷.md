**徹納恩鱷**（屬名：*Chenanisuchus*）是[鱷形超目](../Page/鱷形超目.md "wikilink")[森林鱷科的一屬](../Page/森林鱷科.md "wikilink")，化石發現於[摩洛哥](../Page/摩洛哥.md "wikilink")[西迪徹納恩的晚](../Page/西迪徹納恩.md "wikilink")[古新世](../Page/古新世.md "wikilink")[坦尼特階地層](../Page/坦尼特階.md "wikilink")、以及[馬里的](../Page/馬里.md "wikilink")[白堊紀晚期地層](../Page/白堊紀.md "wikilink")。

牠們是由[古生物學家Stéphane](../Page/古生物學家.md "wikilink") Jouve、Baâdi
Bouya、以及Mbarek
Amaghzaz在2000年所敘述。[模式種是](../Page/模式種.md "wikilink")*C.
lateroculi*，種名意指朝後方的眼睛。

徹納恩鱷是目前已知最原始的[森林鱷科動物](../Page/森林鱷科.md "wikilink")\[1\]。

## 化石材料

目前已有兩個徹納恩鱷的標本被發現。[正模標本](../Page/正模標本.md "wikilink")（編號OCP DEK-GE
262）是一個接近完整的頭顱骨與下頜碎片，另一個標本（編號OCP DEK-GE
61）是一個接近完整的頭顱骨，都發現於[摩洛哥](../Page/摩洛哥.md "wikilink")[西迪徹納恩的晚](../Page/西迪徹納恩.md "wikilink")[古新世](../Page/古新世.md "wikilink")[坦尼特階地層](../Page/坦尼特階.md "wikilink")。近年在[馬里的](../Page/馬里.md "wikilink")[馬斯垂克階地層](../Page/馬斯垂克階.md "wikilink")，也發現徹納恩鱷的化石，顯示徹納恩鱷在[白堊紀-第三紀滅絕事件後存活下來](../Page/白堊紀-第三紀滅絕事件.md "wikilink")\[2\]。

## 分類系統

Jouve等人在2005年的研究中將*C. lateroculi*歸類於森林鱷科，根據以下三個形態上的特徵：

  - [枕骨粗隆部的出現](../Page/枕骨.md "wikilink")。
  - 前外側[眶後突的出現](../Page/眶後突.md "wikilink")。
  - [方顴骨與](../Page/方顴骨.md "wikilink")[上隅骨之間](../Page/上隅骨.md "wikilink")（頜關節）的連接部份大。

## 古生物學

*C. lateroculi*的成年個體身長估計為4到4.5公尺之間，這是根據60公分長的頭顱骨而來的。牠們擁有森林鱷科中最短的口鼻部。

## 參考資料

  - Jouve, S., Bouya, B. & Amaghzaz, M., (2005): A short-snouted
    dyrosaurid (Crocodyliformes, Mesoeucrocodylia) from the Palaeocene
    of Marocco. –Palaeontology: Vol. 48, \#2, pp. 359-369

## 外部連結

  - [Jouve等人的2005年研究](http://www.blackwell-synergy.com/doi/pdf/10.1111/j.1475-4983.2005.00442.x)
    (Pdf檔)

[Category:森林鱷科](../Category/森林鱷科.md "wikilink")
[Category:白堊紀爬行動物](../Category/白堊紀爬行動物.md "wikilink")
[Category:古新世爬行動物](../Category/古新世爬行動物.md "wikilink")

1.
2.