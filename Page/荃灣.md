[Tsuen_Wan_overview_2018.jpg](https://zh.wikipedia.org/wiki/File:Tsuen_Wan_overview_2018.jpg "fig:Tsuen_Wan_overview_2018.jpg")
[Tsuen_Wan_Overview_201406.jpg](https://zh.wikipedia.org/wiki/File:Tsuen_Wan_Overview_201406.jpg "fig:Tsuen_Wan_Overview_201406.jpg")隔海相望，攝於2014年6月\]\]
**荃灣**（）位於[香港](../Page/香港.md "wikilink")[新界西南](../Page/新界西.md "wikilink")\[1\]，位於[規劃署定義的](../Page/規劃署.md "wikilink")[香港都會區範圍之內](../Page/香港市區.md "wikilink")。

## 簡史

據前[香港市政局出版的](../Page/香港市政局.md "wikilink")《香港文物》，荃灣內的[柴灣角街等地曾發掘到](../Page/柴灣角街.md "wikilink")[漢代文物](../Page/漢代.md "wikilink")。

荃灣，古稱**淺灣**，據說因此地海灣水淺而得名。淺灣亦屢見於[宋朝](../Page/宋朝.md "wikilink")、[明朝和](../Page/明朝.md "wikilink")[清朝的地圖和文獻](../Page/清朝.md "wikilink")，包括填海錄，厓山集，宋史新編，南宋史和[新安縣志等](../Page/寶安縣.md "wikilink")。《[粵大記](../Page/粵大記.md "wikilink")》中淺灣的記載在「葵涌」以西。

[南宋](../Page/南宋.md "wikilink")[景炎二年](../Page/景炎.md "wikilink")（1277年）九月至十一月[宋端宗由古瑾來到淺灣](../Page/宋端宗.md "wikilink")，當中一名曹姓大臣在橫過水潭時不慎滑倒溺斃，後人便將該潭命名為[曹公潭以作紀念](../Page/曹公潭.md "wikilink")。十月或十一月[元朝元帥](../Page/元朝.md "wikilink")[劉深帶舟師到淺灣](../Page/劉深.md "wikilink")。而另有傳說劉深由城門一路攻入，而鄉民築起石城抗擊元軍，因有城門此稱。

清初，[清朝追擊南明至荃灣](../Page/清朝.md "wikilink")，城門谷有南明[李萬榮擁永曆年號](../Page/李萬榮.md "wikilink")，據針山石城抗清。

明末清初，荃灣已改名為荃灣約。後來有人改寫成全灣約，如[新安縣志等](../Page/新安縣.md "wikilink")。

荃灣舊時因[海盜猖獗](../Page/海盜.md "wikilink")，曾稱為**賊灣**。而荃灣近[藍巴勒海峽一帶](../Page/藍巴勒海峽.md "wikilink")，舊稱**三百錢**，因傳說經過該地時，需給三百過路錢，也是因為這些原因，加上商船到這水域在退潮時常常[擱淺](../Page/擱淺.md "wikilink")，還有當時還沒開發，商人必死無疑，所以對[客家商人的忠告是](../Page/客家.md "wikilink")“發達去[金山](../Page/金山區_\(上海市\).md "wikilink")，要死來荃灣”，這說法一直流傳到第二次世界大戰後，因為荃灣的水源來自[大帽山的](../Page/大帽山.md "wikilink")[泉水](../Page/泉水.md "wikilink")，但山坑佈滿有毒的[馬錢草](../Page/馬錢屬.md "wikilink")，加上[瘧蚊為患](../Page/瘧蚊.md "wikilink")，令居民[腹瀉](../Page/腹瀉.md "wikilink")，[發冷](../Page/發冷.md "wikilink")。

1950年代，荃灣已經是香港[紡織業中心](../Page/紡織業.md "wikilink")，全港最大的紗廠和染廠也座落荃灣，後來更帶動[輕工業的發展](../Page/輕工業.md "wikilink")。

1961年，港府刊憲宣布把荃灣發展成為新界第一個[衞星城市](../Page/香港新市鎮.md "wikilink")，當時範圍遠至附近的[青衣島](../Page/青衣島.md "wikilink")、[葵涌](../Page/葵涌.md "wikilink")、[荔景一帶](../Page/荔景.md "wikilink")。

## 地理

荃灣位於[大帽山以南](../Page/大帽山.md "wikilink")，東南面向[藍巴勒海峽](../Page/藍巴勒海峽.md "wikilink")，為[青衣島對岸](../Page/青衣島.md "wikilink")，後借用作鄰近地方的名字。1950年代，荃灣面積約有
0.5
[平方公里](../Page/平方公里.md "wikilink")（只計[荃灣市中心](../Page/荃灣市中心.md "wikilink")）。當時，荃灣西部（[柴灣角至](../Page/柴灣角.md "wikilink")[木棉下一帶](../Page/木棉下.md "wikilink")）的海岸線是在[青山公路](../Page/青山公路.md "wikilink")，而東部（[木棉下至](../Page/木棉下.md "wikilink")[鹹田一帶](../Page/鹹田_\(荃灣\).md "wikilink")）的海岸線則約在[沙咀道](../Page/沙咀道.md "wikilink")。填海之前，荃灣有一個海角名，名為[沙咀](../Page/沙咀_\(荃灣\).md "wikilink")，[海壩村舊址](../Page/海壩村.md "wikilink")、[鱟地坊](../Page/鱟地坊.md "wikilink")、大屋圍、部分[福來邨也是在此海角之上](../Page/福來邨.md "wikilink")。

[港英政府為了發展荃灣](../Page/港英政府.md "wikilink")，1950年代起在荃灣大規模[填海](../Page/填海.md "wikilink")，首次填海令[柴灣角消失](../Page/柴灣角.md "wikilink")。荃灣的海岸線亦移至海盛路及[楊屋道](../Page/楊屋道.md "wikilink")。1980年代，介乎[德士古道及馬頭壩道之海域亦被填為陸地](../Page/德士古道.md "wikilink")。

1990年代為了興建九廣西鐵（今[西鐵綫](../Page/西鐵綫.md "wikilink")）的[荃灣西站](../Page/荃灣西站.md "wikilink")，快速公路[荃灣路以西海旁](../Page/荃灣路.md "wikilink")，再次進行填海，多次填海令荃灣的海岸線拉直，藍巴勒海峽再度收窄，成為現今的模樣。

## 年表

[San_Tsuen_Street_View_201105.jpg](https://zh.wikipedia.org/wiki/File:San_Tsuen_Street_View_201105.jpg "fig:San_Tsuen_Street_View_201105.jpg")
[Tsuen_Wan_Overview_2008.jpg](https://zh.wikipedia.org/wiki/File:Tsuen_Wan_Overview_2008.jpg "fig:Tsuen_Wan_Overview_2008.jpg")
[TsuenWan_atNight.jpg](https://zh.wikipedia.org/wiki/File:TsuenWan_atNight.jpg "fig:TsuenWan_atNight.jpg")
[File-2011HKGames_TsuenWanTeam.JPG](https://zh.wikipedia.org/wiki/File:File-2011HKGames_TsuenWanTeam.JPG "fig:File-2011HKGames_TsuenWanTeam.JPG")中，啦啦隊以「荃灣50金禧」為題表演\]\]

  - 荃灣古稱為「淺灣」，據說是因該處海灣水淺而得名。「淺灣」一名亦屢見於[明朝和](../Page/明朝.md "wikilink")[清朝的地圖和文獻](../Page/清朝.md "wikilink")，包括[新安縣志等](../Page/新安縣志.md "wikilink")。
  - 荃灣於[宋朝或之前已有人聚居](../Page/宋朝.md "wikilink")(
    荃湾的[柴湾角曾發現新石器時代及東漢文物](../Page/柴湾角.md "wikilink")
    )。
  - 荃灣也是傳統上講[客家話的地方](../Page/香港客家話.md "wikilink")，因荃灣的[原居民村落主要是講](../Page/新界原居民.md "wikilink")[客家話的](../Page/香港客家話.md "wikilink")[客家民系村落](../Page/客家民系.md "wikilink")。
  - [秦至](../Page/秦朝.md "wikilink")[東晉初年](../Page/東晉.md "wikilink")，今天的荃灣地區轄於[南海郡](../Page/南海郡.md "wikilink")[番禺縣](../Page/番禺縣.md "wikilink")。
  - [東晉](../Page/東晉.md "wikilink")[咸和六年](../Page/咸和.md "wikilink")（331年），設[寶安縣](../Page/寶安縣.md "wikilink")，轄地包括今天的荃灣和葵青地區。
  - [南宋](../Page/南宋.md "wikilink")[景炎二年九月](../Page/景炎.md "wikilink")（1277年），[宋端宗的流亡朝廷從](../Page/宋端宗.md "wikilink")[官富場逃到淺灣](../Page/官富場.md "wikilink")，同年十一月元將[劉深襲淺灣](../Page/劉深.md "wikilink")，朝廷被逼再逃亡。當地居民曾於荃灣北部的[城門谷建石城抵抗劉深](../Page/城門谷.md "wikilink")。
  - [明朝](../Page/明朝.md "wikilink")[萬曆元年](../Page/萬曆.md "wikilink")（1573年），[新安縣設立](../Page/寶安縣.md "wikilink")，轄地包括今天的荃灣和葵青地區。
  - [南明](../Page/南明.md "wikilink")[永曆年間](../Page/永曆.md "wikilink")，參將[李萬榮曾於荃灣北部的](../Page/李萬榮.md "wikilink")[城門谷至](../Page/城門谷.md "wikilink")[針山一帶建石城抗清](../Page/針山.md "wikilink")。
  - [清初時](../Page/清朝.md "wikilink")，荃灣曾經有多個名稱，包括「淺灣」、「荃灣約」、「全灣約」，（完灣），（團灣），（月兒灣灣）和「全灣」。
  - [清](../Page/清.md "wikilink")[康熙](../Page/康熙.md "wikilink")8年（1669年），[遷界令一度放寬時](../Page/遷界.md "wikilink")，大批操[客家話的](../Page/香港客家話.md "wikilink")[客家人遷入](../Page/客家人.md "wikilink")[新安縣](../Page/寶安縣.md "wikilink")，其包括今天的荃灣，並沿青山道成立大約26個村落，其中最早建立的有[老圍村](../Page/老圍_\(荃灣\).md "wikilink")。
  - 清[乾隆](../Page/乾隆.md "wikilink")51年（1786年），[三棟屋由陳姓](../Page/三棟屋.md "wikilink")[客家人在廣東的支系所建立](../Page/客家人.md "wikilink")，成為香港最古老的圍村之一。[海壩是區內另一條較大型和知名的](../Page/海壩村.md "wikilink")[客家](../Page/客家.md "wikilink")[原居民村落](../Page/新界原居民.md "wikilink")，為一雜姓村，建於18世紀至19世紀。
  - 1898年，[清政府和英國簽訂](../Page/清朝.md "wikilink")[展拓香港界址專條](../Page/展拓香港界址專條.md "wikilink")，將深圳河以南一帶納入英國殖民地，荃灣便脫離[新安縣轄地](../Page/寶安縣.md "wikilink")，成為[香港的範圍](../Page/香港.md "wikilink")。英國租借「新界」後，把新界分成「八約」管治，當時荃灣屬於「九龍約」。
  - 19世紀末，當時約有3000人的荃灣分為4個「約」：[海壩](../Page/海壩村.md "wikilink")、葵涌、青衣及[石圍角](../Page/石圍角.md "wikilink")，4約首領組成「荃灣全安局」（荃灣鄉事委員會前身），維持荃灣地區的治安。
  - 1906年，政府開始成立[理民府制度來管治新界](../Page/理民府.md "wikilink")，把新界分成南北兩約，荃灣以北之地歸**北約**，荃灣則屬**南約**。
  - 1941年12月，[日本領佔香港](../Page/日本.md "wikilink")，荃灣被歸入[九龍](../Page/九龍.md "wikilink")，自成一區稱為**荃灣區**。
  - 1948年，[立法局通過](../Page/香港立法會.md "wikilink")《新界行政法例》，荃灣再次歸入南約管轄。
  - 1950年代，由於中華人民共和國的建立引來大量的[中國大陸居民湧入香港](../Page/中國大陸.md "wikilink")，與此同時引入大量資金、技術及廉價勞工，荃灣亦在[德士古道](../Page/德士古道.md "wikilink")、[楊屋道及](../Page/楊屋道.md "wikilink")[柴灣角一帶興建多間工廠](../Page/柴灣角.md "wikilink")，令當時的荃灣工業發展蓬勃，有「[小曼徹斯特城](../Page/曼彻斯特.md "wikilink")」之稱\[2\]。
  - 1959年，由[香港房屋協會興建的荃灣區首個公共屋邨](../Page/香港房屋協會.md "wikilink")—[四季大廈正式入伙](../Page/四季大廈.md "wikilink")。
  - 約1960年代，[荃灣運動場開幕](../Page/楊屋道球場.md "wikilink")，為區內首個有田徑運動場的體育場地。
  - 1961年，政府正式刊憲，發展新界首個[新市鎮](../Page/香港新市鎮.md "wikilink")—[荃灣新市鎮](../Page/荃灣新市鎮.md "wikilink")，而進行填海計劃。
  - 1961年，當時仍屬荃灣區的[大窩口邨落成](../Page/大窩口邨.md "wikilink")，是當時區內首個[徙置區](../Page/徙置區.md "wikilink")，也是今天[葵青區的首個徙置區](../Page/葵青區.md "wikilink")。
  - 1963年，[福來邨落成](../Page/福來邨.md "wikilink")，是[香港屋宇建設委員會興建的十個](../Page/香港屋宇建設委員會.md "wikilink")[廉租屋邨之一](../Page/廉租屋邨.md "wikilink")，亦是荃灣區首個廉租屋邨。
  - 1964年，同為房協興建的出租屋邨—[寶石大廈及](../Page/寶石大廈.md "wikilink")[滿樂大廈落成](../Page/滿樂大廈.md "wikilink")。
  - 1966年，從南約獨立出來，成立荃灣區，並由荃灣[理民府管轄](../Page/理民府.md "wikilink")。
  - 1973年，香港政府通過荃灣發展計劃，並將進行大規模填海工程，興建多個[公共屋邨及基本建設](../Page/香港公共屋邨.md "wikilink")。[仁濟醫院啟用](../Page/仁濟醫院_\(香港\).md "wikilink")。
  - 1974年，荃灣公共圖書館啟用。
  - 1977年，政府宣佈在新界成立不同地區的諮詢委員會，在荃灣設立「荃灣地區諮詢委員會」，為[荃灣區議會前身](../Page/荃灣區議會.md "wikilink")。
  - 1978年，區內第一個大型私人屋苑[荃威花園開始入伙](../Page/荃威花園.md "wikilink")。
  - 1980年，[荃灣大會堂啟用](../Page/荃灣大會堂.md "wikilink")，成為香港第三個大會堂。
  - 1981年，荃灣區議會成立。[屯門公路](../Page/屯門公路.md "wikilink")[深井段通車](../Page/深井.md "wikilink")。
  - 1982年，[地鐵](../Page/香港地鐵.md "wikilink")（現稱[港鐵](../Page/港鐵.md "wikilink")）[荃灣綫於](../Page/荃灣綫.md "wikilink")5月10日正式投入服務，並取代[觀塘綫直通](../Page/觀塘綫.md "wikilink")[尖沙咀站及](../Page/尖沙咀站.md "wikilink")[中環站而成為主線](../Page/中環站.md "wikilink")。
  - 1983年，連接[屯門藍地及荃灣柴灣角的屯門公路全線通車](../Page/屯門.md "wikilink")。
  - 1985年，[葵涌及青衣區議會成立](../Page/葵青區議會.md "wikilink")，[葵涌及](../Page/葵涌.md "wikilink")[青衣正式脫離荃灣區](../Page/青衣.md "wikilink")。
  - 1987年，[三棟屋博物館完成修建工程](../Page/三棟屋博物館.md "wikilink")，加設了博物館設施，供市民免費參觀，同年2月11日，城門隧道動工興建。
  - 1990年4月20日，連接[沙田和荃灣的](../Page/沙田.md "wikilink")[城門隧道通車](../Page/城門隧道.md "wikilink")，至今仍為新界的最主要隧道，往返機場的巴士都會途經城門隧道、[象鼻山路和](../Page/象鼻山路.md "wikilink")[德士古道前往青衣和機場](../Page/德士古道.md "wikilink")，而非[青沙公路](../Page/青沙公路.md "wikilink")。
  - 1991年，[荃灣海濱公園啟用](../Page/荃灣海濱公園.md "wikilink")。
  - 1992年，[荃灣廣場落成](../Page/荃灣廣場.md "wikilink")。
  - 1995年，[區域市政局宣布區內泳灘受到污染](../Page/區域市政局.md "wikilink")，不宜游泳。
  - 1996年，[德華公園展覽廳正式啟用](../Page/德華公園.md "wikilink")。
  - 1997年，[城門谷公園啟用](../Page/城門谷公園.md "wikilink")。
  - 1998年，[香港國際機場及](../Page/香港國際機場.md "wikilink")[大欖隧道落成啟用](../Page/大欖隧道.md "wikilink")，由於鄰近機場和大欖隧道而間接加速荃灣的發展。[愉景新城建成](../Page/愉景新城.md "wikilink")。[楊屋道球場關閉](../Page/楊屋道球場.md "wikilink")。
  - 2000年7月，荃灣經青衣到中環的小輪航綫，因客量少而停辦。
  - 2003年，[西鐵綫](../Page/西鐵綫.md "wikilink")[荃灣西站通車](../Page/荃灣西站.md "wikilink")。
  - 2005年，[香港迪士尼樂園落成](../Page/香港迪士尼樂園.md "wikilink")，地產商紛紛將多項高級酒店及住宅項目亦選擇區內興建，同年港鐵欣澳站啟用，使愉景灣居民更快直達各區。
  - 2007年，[如心廣場建成](../Page/如心廣場.md "wikilink")，是當時全香港第五高的樓宇。現時則是全香港第六高樓，但仍保持著全[新界第一高樓的位置](../Page/新界.md "wikilink")。
  - 2008年7月31日，[荃新天地開幕](../Page/荃新天地.md "wikilink")。同年10月，[如心廣場商場對外開放](../Page/如心廣場.md "wikilink")。
  - 2009年11月底，「荃灣行人天橋網絡擴充計劃」\[3\]
    動工，連接[港鐵荃灣西站及荃灣站一帶的行人天橋網絡](../Page/港鐵.md "wikilink")，並延伸至荃灣東部關門口街及至西部的大涌道，2013年中啟用，成為全港規模最大及最長的行人天橋系統。
  - 2011年9月16日 政府慶祝荃灣新市鎮成立50週年，在藍巴勒海峽舉行了歷時20分鐘的煙火匯演。
  - 2012年2月7日，[鱟地坊露天小販市場最後一天營業](../Page/鱟地坊.md "wikilink")，後於暑假期間拆卸重建，經重新規劃成有蓋的「鱟地坊小販市場」，於2013年啟用。但因使用率不足而被傳媒批評為浪費公帑\[4\]。
  - 2018年7月28日，[南豐紗廠 The
    Mills六廠紡織文化藝術館](../Page/南豐紗廠_The_Mills.md "wikilink")（CHAT六廠）首次對外開放。同年10月11日[荃灣體育館啟用](../Page/荃灣體育館.md "wikilink")。

## 氣候

<div style="width:80%;">

<div style="width:80%;">

## 市中心

[Tsuen_Wan_Residential_Buildings_2017.jpg](https://zh.wikipedia.org/wiki/File:Tsuen_Wan_Residential_Buildings_2017.jpg "fig:Tsuen_Wan_Residential_Buildings_2017.jpg")
荃灣並不和同為第一代新市鎮的[沙田和](../Page/沙田新市鎮.md "wikilink")[屯門般規劃有固定的](../Page/屯門新市鎮.md "wikilink")「[市中心](../Page/市中心.md "wikilink")」地帶。市中心範圍大約在[港鐵](../Page/港鐵.md "wikilink")[荃灣站範圍](../Page/荃灣站.md "wikilink")，[大涌道以東](../Page/大涌道.md "wikilink")、[海盛路及](../Page/海盛路.md "wikilink")[楊屋道以北](../Page/楊屋道.md "wikilink")、[關門口街以西及](../Page/關門口街.md "wikilink")[青山公路荃灣段以南一帶](../Page/青山公路.md "wikilink")。最重要的公共設施都在市中心範圍；[荃灣政府合署位於](../Page/荃灣政府合署.md "wikilink")[荃灣站巴士總站旁](../Page/荃灣站巴士總站.md "wikilink")、[荃灣大會堂和](../Page/荃灣大會堂.md "wikilink")[沙咀道遊樂場等都在](../Page/沙咀道遊樂場.md "wikilink")[大河道旁邊](../Page/大河道.md "wikilink")，而大河道亦是連接幾個重要交通設施[荃灣站和](../Page/荃灣站.md "wikilink")[荃灣西站的道路](../Page/荃灣西站.md "wikilink")；至於主要商業區，[川龍街和](../Page/川龍街.md "wikilink")[眾安街一帶由於以售賣金飾店成行成市而得名](../Page/眾安街.md "wikilink")「荃灣珠寶金飾坊」，此外還有[鱟地坊的小販市場](../Page/鱟地坊.md "wikilink")；以及後來的大型商場和商業大廈如[荃灣廣場](../Page/荃灣廣場.md "wikilink")\[5\]、[如心廣場](../Page/如心廣場.md "wikilink")、[荃新天地](../Page/荃新天地.md "wikilink")、有線電視大樓等。交通設施有地鐵港鐵[荃灣站](../Page/荃灣站.md "wikilink")、大型巴士總站、[公共小巴等](../Page/香港小巴.md "wikilink")。新市鎮開發早期，連青衣、葵涌[徙置區居民的購物](../Page/徙置區.md "wikilink")、商業和娛樂活動都是在荃灣市中心。

<File:Tsuen> Wan Bridge Access
201306.jpg|荃灣有43條行人天橋，[其龐大的行人天橋網絡](../Page/荃灣行人天橋網絡.md "wikilink")，可接通20個大小商場，近年被稱為「天橋之城」
<File:Tsuen> Cheong Centre Access
2015.jpg|荃灣是「商場之城」，有不少小型商場成為年青人和外籍家庭傭工流連的地方，圖為荃昌中心
<File:Tai> Ho
Road.jpg|荃灣大河道一帶，可見的主要公共設施有荃灣大會堂、遠處的[荃灣政府合署](../Page/荃灣政府合署.md "wikilink")（連[荃灣公共圖書館](../Page/荃灣公共圖書館.md "wikilink")）
<File:Tsuen> Wan Station 2013 10
part2.JPG|[港鐵](../Page/港鐵.md "wikilink")[荃灣站](../Page/荃灣站.md "wikilink")，是區內最主要車站

## 交通

荃灣是[港鐵](../Page/港鐵.md "wikilink")[荃灣綫的終點站](../Page/荃灣綫.md "wikilink")，[荃灣站附近設有](../Page/荃灣站.md "wikilink")[荃灣鐵路站巴士總站](../Page/荃灣鐵路站巴士總站.md "wikilink")，站內有多條往來新界西北的巴士路線，並有往[深井及](../Page/深井.md "wikilink")[青龍頭的專線小巴路線](../Page/青龍頭.md "wikilink")。在[汀九橋及九廣西鐵](../Page/汀九橋.md "wikilink")（今[西鐵綫](../Page/西鐵綫.md "wikilink")）開通前，荃灣是九龍往來新界西北的必經之路，荃灣站及巴士總站每天都有大量轉乘地鐵及巴士往來[屯門及](../Page/屯門.md "wikilink")[元朗的乘客](../Page/元朗.md "wikilink")。

### 市中心（北）交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>
    [荃灣綫](../Page/荃灣綫.md "wikilink")：[荃灣站](../Page/荃灣站.md "wikilink")

<!-- end list -->

  - [荃灣鐵路站巴士總站](../Page/荃灣鐵路站巴士總站.md "wikilink")

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  - 荃灣至[皇崗口岸線](../Page/皇崗口岸.md "wikilink") (通宵班次)

<!-- end list -->

  - [西樓角路](../Page/西樓角路.md "wikilink")

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  - 荃灣至皇崗口岸線

<!-- end list -->

  - 西樓角公共運輸交匯處

<!-- end list -->

  - [美環街](../Page/美環街.md "wikilink")

<!-- end list -->

  - [荃灣（愉景新城）巴士總站](../Page/荃灣（愉景新城）巴士總站.md "wikilink")

<!-- end list -->

  - 跨境巴士
      - 荃灣至[皇崗口岸線](../Page/皇崗口岸.md "wikilink") (日間時段)

<!-- end list -->

  - [青山公路](../Page/青山公路.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 元朗至佐敦道線 (24小時服務)\[6\]
  - 荃灣至[元朗線](../Page/元朗.md "wikilink") (通宵服務)\[7\]

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  -   - 荃灣至[皇崗口岸線](../Page/皇崗口岸.md "wikilink") (通宵班次)

<!-- end list -->

  - [大河道北](../Page/大河道北.md "wikilink")

<!-- end list -->

  - [蕙荃路](../Page/蕙荃路.md "wikilink")

<!-- end list -->

  - [海壩街](../Page/海壩街.md "wikilink")／[曹公街](../Page/曹公街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港公共小巴.md "wikilink")

<!-- end list -->

  - 荃灣至[灣仔線](../Page/灣仔.md "wikilink")\[8\]
  - [元朗至](../Page/元朗.md "wikilink")[旺角](../Page/旺角.md "wikilink")／[佐敦道線](../Page/佐敦道.md "wikilink")
    (24小時服務)\[9\]
  - 荃灣至旺角/佐敦道線 (24小時服務)\[10\]\[11\]
  - [青山道至荃灣線](../Page/青山道.md "wikilink")\[12\]

<!-- end list -->

  - [大河道](../Page/大河道.md "wikilink")

<!-- end list -->

  - [沙咀道](../Page/沙咀道.md "wikilink")

<!-- end list -->

  - [關門口街](../Page/關門口街.md "wikilink")

<!-- end list -->

  - [荃灣街市街](../Page/荃灣街市街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 荃灣至[觀塘線](../Page/觀塘.md "wikilink") (24小時服務)\[13\]
  - 荃灣至[藍田](../Page/藍田.md "wikilink")／[油塘線](../Page/油塘.md "wikilink")
    (上午及下午服務)\[14\]
  - 荃灣至九龍灣線 (上午服務)\[15\]
  - 荃灣至[坪石線](../Page/坪石邨.md "wikilink")\[16\]
  - 荃灣至[西環線](../Page/西環.md "wikilink")\[17\]
  - 荃灣至[黃大仙](../Page/黃大仙.md "wikilink")／[九龍城線](../Page/九龍城.md "wikilink")\[18\]
  - 荃灣至[何文田線](../Page/何文田.md "wikilink") (平日上午及上午服務)\[19\]

<!-- end list -->

  - [川龍街](../Page/川龍街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 荃灣至石籬線\[20\]
  - 荃灣至紅磡／土瓜灣線\[21\]
  - 荃灣至沙田馬場線 (沙田馬場賽馬日服務)\[22\]

<!-- end list -->

  - [海壩街](../Page/海壩街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 荃灣至石圍角／象山線\[23\]
  - 荃灣至慈雲山線\[24\]

<!-- end list -->

  - [眾安街](../Page/眾安街.md "wikilink")

<!-- end list -->

  - [兆和街](../Page/兆和街.md "wikilink")

<!-- end list -->

  - [荃富街](../Page/荃富街.md "wikilink")／[荃華街](../Page/荃華街.md "wikilink")

<!-- end list -->

  - [大涌道](../Page/大涌道.md "wikilink")

</div>

</div>

### 市中心（南）交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{西鐵綫色彩}}">█</font>
    [西鐵綫](../Page/西鐵綫.md "wikilink")：[荃灣西站](../Page/荃灣西站.md "wikilink")

<!-- end list -->

  - [沙咀道](../Page/沙咀道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港公共小巴.md "wikilink")

<!-- end list -->

  - 荃灣至[旺角](../Page/旺角.md "wikilink")/[佐敦道線](../Page/佐敦道.md "wikilink")
    (24小時服務)\[25\]

<!-- end list -->

  - [鹹田街](../Page/鹹田街.md "wikilink")／[河背街](../Page/河背街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 楊屋道街市至石圍角/三棟屋線 (上午服務)\[26\]
  - 楊屋道街市至麗城花園線 (上午服務)\[27\]

<!-- end list -->

  - [楊屋道](../Page/楊屋道.md "wikilink")

<!-- end list -->

  - [聯仁街](../Page/聯仁街.md "wikilink")

<!-- end list -->

  - 萬景峯紅色小巴總站
    [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 荃灣至梨木樹線\[28\]
  - 荃灣萬景峯至佐敦道線\[29\]

<!-- end list -->

  - [荃灣（如心廣場）巴士總站](../Page/荃灣（如心廣場）巴士總站.md "wikilink")

<!-- end list -->

  - 跨境巴士

<!-- end list -->

  - 荃灣[如心廣場至](../Page/如心廣場.md "wikilink")[深圳寶安國際機場線](../Page/深圳寶安國際機場.md "wikilink")

<!-- end list -->

  - [大河道](../Page/大河道.md "wikilink")

<!-- end list -->

  - [川龍街](../Page/川龍街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 荃灣至石籬線\[30\]
  - 荃灣至土瓜灣線\[31\]
  - 荃灣至沙田馬場線 (沙田馬場賽馬日服務)\[32\]

<!-- end list -->

  - [德士古道](../Page/德士古道.md "wikilink")

<!-- end list -->

  - [荃富街](../Page/荃富街.md "wikilink")／[荃華街](../Page/荃華街.md "wikilink")

</div>

</div>

## 街道

荃灣新市鎮發展時，區內多條街道均以當時鄰近的地名或村落名稱命名，如[沙咀道](../Page/沙咀道.md "wikilink")、[川龍街等](../Page/川龍街.md "wikilink")，當中部分地名今天已消失，這些街道名稱遂成為荃灣發展重要的歷史見証。以下為這些街道的資料：

| 地區                               | 街道名稱                                  | 引用地名／村名                                    | 地名／村名描述                                                                         | 備註                                                                                                                                           |
| -------------------------------- | ------------------------------------- | ------------------------------------------ | ------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| 荃灣西                              | [大涌道](../Page/大涌道.md "wikilink")      | 大涌                                         | [曹公潭下游一帶](../Page/曹公潭.md "wikilink")                                            | 原為曹公潭西側一道路，連接[青山公路及柴灣角街](../Page/青山公路#荃灣段.md "wikilink")，與香車街隔河相望；1990年代，曹公潭被鋪上鋼板，從此改為暗渠，橋面則開闢為新的大涌道。                                        |
| 白田壩街                             | 白田壩                                   | 現[美環街一帶](../Page/美環街.md "wikilink")        | 柴灣角工業區主要道路。                                                                     |                                                                                                                                              |
| 柴灣角街                             | [柴灣角](../Page/柴灣角.md "wikilink")      |                                            |                                                                                 |                                                                                                                                              |
| 半山街                              | 半山村                                   | [麗城花園二期對上山坡](../Page/麗城花園.md "wikilink")   |                                                                                 |                                                                                                                                              |
| 海壩街                              | 海壩                                    | 現[德華公園](../Page/德華公園.md "wikilink")        |                                                                                 |                                                                                                                                              |
| 曹公街                              | 曹公潭                                   |                                            |                                                                                 |                                                                                                                                              |
| 曹公坊                              |                                       |                                            |                                                                                 |                                                                                                                                              |
| 香車街                              | 香車村                                   | 曹公潭下游一帶                                    |                                                                                 |                                                                                                                                              |
| 芙蓉街                              | [芙蓉山](../Page/芙蓉山.md "wikilink")      |                                            |                                                                                 |                                                                                                                                              |
| 大壩街                              | 大壩                                    | 不詳                                         |                                                                                 |                                                                                                                                              |
| 圓墩圍                              | [圓墩](../Page/圓墩村.md "wikilink")       |                                            |                                                                                 |                                                                                                                                              |
| 荃灣中                              | [大河道](../Page/大河道.md "wikilink")      | 大河瀝                                        | [三疊潭下游](../Page/三疊潭.md "wikilink")                                              | 最初其南面盡頭止於楊屋道；1983年起延長至[荃灣運輸大樓](../Page/荃灣運輸大樓.md "wikilink")；2000年代再因[西鐵工程延長至](../Page/西鐵綫.md "wikilink")[荃灣西站](../Page/荃灣西站.md "wikilink")。 |
| 大屋街                              | 大屋圍                                   | 現[協和廣場](../Page/協和廣場_\(荃灣\).md "wikilink") | 唯一以荃灣新市鎮中建築命名的街道。                                                               |                                                                                                                                              |
| [川龍街](../Page/川龍街.md "wikilink") | [川龍](../Page/川龍.md "wikilink")        |                                            | 其南端原為[掘頭路](../Page/掘頭路.md "wikilink")，2011年因應楊屋道擴闊工程而接通楊屋道。                     |                                                                                                                                              |
| [沙咀道](../Page/沙咀道.md "wikilink") | [沙咀](../Page/沙咀_\(荃灣\).md "wikilink") | 現雅麗珊社區中心一帶                                 | 其西面盡頭原止於青山公路；2000年代，沙咀道被延長並興建一行車天橋橫跨青山公路與[荃景圍連接](../Page/荃景圍.md "wikilink")。    |                                                                                                                                              |
| [楊屋道](../Page/楊屋道.md "wikilink") | 楊屋                                    | 即沙咀                                        |                                                                                 |                                                                                                                                              |
| 禾笛街                              | 禾笛壩                                   | 現[麗城花園第三期一帶](../Page/麗城花園.md "wikilink")   |                                                                                 |                                                                                                                                              |
| 西樓街                              | 西樓角                                   | 現[南豐中心](../Page/南豐中心.md "wikilink")        | 原為一條連接大河道及禾笛街的內街；2000年代因應「荃灣七街重建項目」而消失，現為[荃新天地一部分](../Page/荃新天地.md "wikilink")。 |                                                                                                                                              |
| 河背街                              | 河背                                    | 現路德圍附近                                     | 其西端原止於大河道；2000年代因應「荃灣七街重建項目」而縮短至禾笛街。                                            |                                                                                                                                              |
| 新村街                              | 新村                                    |                                            |                                                                                 |                                                                                                                                              |
| 大陂坊                              | 二陂圳                                   | 現石圍角邨                                      |                                                                                 |                                                                                                                                              |
| 二陂坊                              |                                       |                                            |                                                                                 |                                                                                                                                              |
| 三陂坊                              |                                       |                                            |                                                                                 |                                                                                                                                              |
| 四陂坊                              |                                       |                                            |                                                                                 |                                                                                                                                              |
| [鱟地坊](../Page/鱟地坊.md "wikilink") | 鱟地                                    | 現鱟地坊附近                                     |                                                                                 |                                                                                                                                              |
| 鹹田街                              | 鹹田                                    | 現荃灣考評局一帶                                   |                                                                                 |                                                                                                                                              |
| 荃灣東                              | 關門口街                                  | 關門口                                        | 現[仁濟醫院一帶](../Page/仁濟醫院.md "wikilink")                                           |                                                                                                                                              |
| 古坑道                              | 古坑                                    | 現[城門谷公園](../Page/城門谷公園.md "wikilink")      | 德士古工業區主要道路。                                                                     |                                                                                                                                              |
| 馬頭壩道                             | 馬頭壩                                   | 現[荃灣站](../Page/荃灣站.md "wikilink")          |                                                                                 |                                                                                                                                              |
| 橫窩仔街                             | 橫窩仔                                   | 不詳                                         |                                                                                 |                                                                                                                                              |
| 灰窰角街                             | 灰窰角                                   | 現[麗瑤邨附近](../Page/麗瑤邨.md "wikilink")        |                                                                                 |                                                                                                                                              |
| 馬角街                              | 馬角山                                   | 現荃灣華人永遠墳場                                  |                                                                                 |                                                                                                                                              |
| 馬角里                              |                                       |                                            |                                                                                 |                                                                                                                                              |
| 橫龍街                              | 橫龍                                    | 川龍附近                                       |                                                                                 |                                                                                                                                              |

荃灣以地名／村名命名街道資料

## 飲食

荃灣的飲食發展歷史悠久，種類繁多，各式經營於地舖的食店時常為多方媒體所報導，當中幾個較熱門的區域／地點包括：

  - [路德圍](../Page/路德圍.md "wikilink")（包括運通街、安榮街、荃興徑至海壩街一帶）的小食店、甜品店、米線及各式泰／越／川味餐廳
  - 四個坡坊（大坡坊、二坡坊、三坡坊及四坡坊）內的潮州菜（打冷）及各式火鍋（如雞煲火鍋）
  - [戴麟趾夫人分科診所背後鄰近鱟地坊以台灣夜市為藍本發展的](../Page/戴麟趾#以他命名事物.md "wikilink")「士林食街」
  - 香車街街市熟食中心的小炒及海鮮菜館

除地舖的食店外，各大購物商場的連鎖經營食店亦時常吸引不少人流：如[大鴻輝(荃灣)中心的各式韓式燒烤及火鍋店](../Page/大鴻輝\(荃灣\)中心.md "wikilink")，輪侯升降機的食客往往擠得商場地下大堂水洩不通。

## 醫療

  - [仁濟醫院](../Page/仁濟醫院_\(香港\).md "wikilink")，設有24小時急症室
  - [戴麟趾夫人普通科門診診所](../Page/戴麟趾夫人普通科門診診所.md "wikilink")

## 寺廟

荃灣近[芙蓉山南麓一帶地方幽靜](../Page/芙蓉山.md "wikilink")，背靠香港最高的[大帽山](../Page/大帽山.md "wikilink")，風水甚佳，故有不少寺廟臨立。特別是20世紀初，當時不少[道人](../Page/道人.md "wikilink")、[法師為躲避戰禍來到香港](../Page/法師.md "wikilink")，一部份便在此帶開宗建寺。其中大部分寺廟集中在[荃灣老圍附近的千佛山](../Page/荃灣老圍.md "wikilink")。

## 社區問題

### 海旁臭味問題

根據荃灣區議會文件顯示，荃灣西海旁有3個雨水暗渠排水口，分佈荃灣麗城花園、荃灣碼頭及[荃灣海濱公園對出](../Page/荃灣海濱公園.md "wikilink")，唯荃灣區內不少舊樓的污水渠駁錯，使海旁臭味困擾周邊居民多年。區議員指區議會已通過動議，並已列入工務工程，惟非優先處理項目，排隊多年，遲遲未能落實。

渠務署發言人回覆指，荃灣區四個旱季截流器工程（即將目前4條排水道污水接駁至污水處理廠）的工務工程項目已獲撥款進行勘測及設計，在聘請顧問、勘測及設計完成後，已於2015年底開展工程。\[33\]

### 頗嚴重的飛機噪音問題

由於[香港國際機場的二號](../Page/香港國際機場.md "wikilink")[跑道](../Page/跑道.md "wikilink")（北跑道）剛好指向荃灣，所以當有[飛機在](../Page/飛機.md "wikilink")[汀九](../Page/汀九.md "wikilink")、[柴灣角](../Page/柴灣角.md "wikilink")、[荃景圍](../Page/荃景圍.md "wikilink")、[油柑頭](../Page/油柑頭.md "wikilink")，以及荃灣以西一帶的上空經過（通常每2至3[分鐘左右便有一架](../Page/分鐘.md "wikilink")[飛機掠過](../Page/飛機.md "wikilink")，準備降落北跑道﹚的時候，會為當地居民帶來[噪音污染](../Page/噪音污染.md "wikilink")。有居民還因此睡眠不太充足，並囑咐人們遷居選擇樓盤時要精打細算，否則就會影響生活質素。\[34\]

## 未來發展

[荃灣西站上蓋物業發展項目鄰近海邊](../Page/荃灣西站.md "wikilink")，但項目自2008年起被地區人士批評會形成[屏風樓效應](../Page/屏風樓.md "wikilink")。時任[發展局局長](../Page/發展局局長.md "wikilink")[林鄭月娥書面回應表示該項目的代理人港鐵已委託獨立顧問公司評估](../Page/林鄭月娥.md "wikilink")，結果顯示，在空氣流通及景觀通透度方面均有改善；在善用土地滿足房屋需求和把項目盡早發展的大前提下，已無空間作進一步的修訂。\[35\]

不過荃灣區議會曾經「先行先試」，於2011年自資拜托[香港科技大學](../Page/香港科技大學.md "wikilink")，為整個荃灣區將來樓宇開展停止風洞測試研討。經過數據證明，發現荃灣西站上蓋物業發展項目將會令荃灣空氣質素更為惡化，並嚴重影響荃灣社區的景觀。\[36\]但政府最終沒有理會區議會以至地區人士反對聲音，如期讓項目的代理人港鐵為物業發展項目進行招標工作。

該區現時像[將軍澳以及](../Page/將軍澳.md "wikilink")[大圍一樣](../Page/大圍.md "wikilink")，形成一列屏風樓，對區內的光線及規劃佈局造成破壞。

## 區議會議席分佈

為方便比較，以下列表以[香港九號幹線荃灣段](../Page/香港9號幹線.md "wikilink")（即[屯門公路](../Page/屯門公路.md "wikilink")[柴灣角出入口經](../Page/柴灣角.md "wikilink")[青山公路荃灣段](../Page/青山公路.md "wikilink")、[象鼻山路](../Page/象鼻山路.md "wikilink")）、[荃錦交匯處](../Page/荃錦交匯處.md "wikilink")、[德士古道至海岸線所包圍的地區為範圍](../Page/德士古道.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/青山公路－荃灣段.md" title="wikilink">青山公路－荃灣段以南</a>、<a href="../Page/大涌道.md" title="wikilink">大涌道以西</a><br />
（即<a href="../Page/柴灣角.md" title="wikilink">柴灣角工業區</a>）</p></td>
<td><p><a href="../Page/荃灣西_(選區).md" title="wikilink">麗興選區</a></p></td>
<td><p><a href="../Page/荃灣西_(選區).md" title="wikilink">荃灣西選區</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大涌道以東、青山公路－荃灣段、西樓角路以南、<a href="../Page/大河道.md" title="wikilink">大河道以西</a></p></td>
<td><p><a href="../Page/祈德尊新邨.md" title="wikilink">祈德尊選區及</a><a href="../Page/福來邨.md" title="wikilink">福來選區</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大河道以東、<a href="../Page/荃錦交匯處.md" title="wikilink">荃錦交匯處</a>、<a href="../Page/德士古道.md" title="wikilink">德士古道以西</a>、青山公路－荃灣段、西樓角路以北</p></td>
<td><p><a href="../Page/綠楊_(選區).md" title="wikilink">綠楊選區</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大河道以東、西樓角路、青山公路－荃灣段以南、德士古道以西、<a href="../Page/荃灣路.md" title="wikilink">荃灣路以北</a></p></td>
<td><p><a href="../Page/德華.md" title="wikilink">德華選區及</a><a href="../Page/楊屋道_(選區).md" title="wikilink">楊屋道選區</a></p></td>
<td><p><a href="../Page/德華.md" title="wikilink">德華選區</a>、<a href="../Page/楊屋道_(選區).md" title="wikilink">楊屋道選區及</a><a href="../Page/荃灣西_(選區).md" title="wikilink">荃灣西選區</a></p></td>
<td><p><a href="../Page/德華.md" title="wikilink">德華選區</a>、<a href="../Page/楊屋道_(選區).md" title="wikilink">楊屋道選區及</a><a href="../Page/荃灣南.md" title="wikilink">荃灣南選區</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>德士古道以南、荃灣路以西</p></td>
<td><p><a href="../Page/海濱_(選區).md" title="wikilink">海濱選區</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 参考文献

## 外部链接

  - [口述歷史 -
    荃灣](http://sunzi1.lib.hku.hk/hkoh/search_theme.jsp?theme_code=T803)
    - 香港大學圖書館數碼化項目, 香港口述歷史檔案

## 參見

  - [荃灣新市鎮](../Page/荃灣新市鎮.md "wikilink")：包括荃灣、[葵涌及](../Page/葵涌.md "wikilink")[青衣島](../Page/青衣島.md "wikilink")
  - [荃灣區](../Page/荃灣區.md "wikilink")：包括荃灣、[馬灣及北](../Page/馬灣.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")（[香港迪士尼渡假區一帶](../Page/香港迪士尼渡假區.md "wikilink")）

{{-}}

[荃灣](../Category/荃灣.md "wikilink")
[Category:香港工業區](../Category/香港工業區.md "wikilink")
[Category:香港商業區](../Category/香港商業區.md "wikilink")

1.  香港[規劃署](../Page/規劃署.md "wikilink")[地圖](http://www.pland.gov.hk/pland_tc/info_serv/tp_plan/images/sta_plan.jpg)
2.  [工商日報社論 1967年12月29日](http://1967riot.wordpress.com/2012/12/29/ksyp-19671229/)
3.  [立法會交通事務委員會 145TB
    號工程計劃－荃灣行人天橋網絡擴充工程](http://www.legco.gov.hk/yr07-08/chinese/panels/tp/papers/tpcb1-266-1-c.pdf)
4.
5.
6.  [佐敦道白加士街　—　屯門及元朗](http://www.16seats.net/chi/rmb/r_kn68.html)
7.  [荃灣荃灣千色匯　＞　屯門及元朗](http://www.16seats.net/chi/rmb/r_n53.html)
8.  [荃灣福來邨　—　灣仔站](http://www.16seats.net/chi/rmb/r_hn40.html)
9.  [佐敦道白加士街　—　屯門及元朗](http://www.16seats.net/chi/rmb/r_kn68.html)
10. [佐敦道寧波街　—　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn33.html)
11. [旺角朗豪坊　—　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn47.html)
12. [深水埗福榮街　＞　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn43.html)
13. [荃灣荃灣街市街　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kn40.html)
14. [荃灣荃灣街市街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_kn14.html)
15. [荃灣荃灣街市街　—　九龍灣](http://www.16seats.net/chi/rmb/r_kn24.html)
16. [荃灣荃灣街市街　—　新蒲崗及坪石](http://www.16seats.net/chi/rmb/r_kn42.html)
17. [荃灣荃灣街市街　＞　西環荷蘭街,
    上環信德中心　＞　荃灣荃灣街市街](http://www.16seats.net/chi/rmb/r_hn30.html)
18. [荃灣荃灣街市街　—　黃大仙及九龍城](http://www.16seats.net/chi/rmb/r_kn45.html)
19. [荃灣荃灣街市街　—　何文田](http://www.16seats.net/chi/rmb/r_kn41.html)
20. [荃灣川龍街　—　石梨貝](http://www.16seats.net/chi/rmb/r_n31.html)
21. [荃灣川龍街　—　土瓜灣欣榮花園](http://www.16seats.net/chi/rmb/r_kn30.html)
22. [荃灣川龍街　—　沙田馬場](http://www.16seats.net/chi/rmb/r_n48.html)
23. [荃灣海壩街　—　石圍角及象山](http://www.16seats.net/chi/rmb/r_n32.html)
24. [荃灣海壩街　—　慈雲山](http://www.16seats.net/chi/rmb/r_kn38.html)
25. [旺角朗豪坊　—　荃灣福來邨](http://www.16seats.net/chi/rmb/r_kn47.html)
26. [荃灣楊屋道街市　＞　石圍角及老圍](http://www.16seats.net/chi/rmb/r_n81.html)
27. [荃灣楊屋道街市　—　麗城花園](http://www.16seats.net/chi/rmb/r_n34.html)
28. [荃灣萬景峯　—　梨木樹](http://www.16seats.net/chi/rmb/r_n36.html)
29. [荃灣萬景峯　＞　佐敦道](http://www.16seats.net/chi/rmb/r_kn53.html)
30. [荃灣川龍街　—　石梨貝](http://www.16seats.net/chi/rmb/r_n31.html)
31. [荃灣川龍街　—　土瓜灣欣榮花園](http://www.16seats.net/chi/rmb/r_kn30.html)
32. [荃灣川龍街　—　沙田馬場](http://www.16seats.net/chi/rmb/r_n48.html)
33. [蘋果日報
    駁錯屎渠　改動工程未上馬 2014年4月24日](http://hk.apple.nextmedia.com/financeestate/art/20140424/18698601)
34.
35. [荃灣西站上蓋物業設計無修訂
    (2011年6月11日)](http://news.sina.com.hk/news/20110615/-1-2357108/1.html)

36. [據理力爭：荃灣區會握數據 反屏風樓有理由](http://hkgoodplace.emm3.net/?p=19313)