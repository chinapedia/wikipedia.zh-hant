[Lycia_locator_map.svg](https://zh.wikipedia.org/wiki/File:Lycia_locator_map.svg "fig:Lycia_locator_map.svg")
**吕基亚**（[古希腊语](../Page/古希腊语.md "wikilink")：Λυκία）[安纳托利亚历史上的一个地区](../Page/安纳托利亚.md "wikilink")，位于今[土耳其](../Page/土耳其.md "wikilink")[安塔利亚省境内](../Page/安塔利亚省.md "wikilink")。在[罗马帝国时期](../Page/罗马帝国.md "wikilink")，这里曾是帝国在[亚洲的一个](../Page/亚洲.md "wikilink")[行省](../Page/罗马行省.md "wikilink")。

## 地理

吕基亚位于土耳其西南部海岸，是一个多山而且植被茂盛的地区。它的西面和西北面与[卡里亚交界](../Page/卡里亚.md "wikilink")，东面与[潘菲利亚接壤](../Page/潘菲利亚.md "wikilink")，东北面是[皮西迪亚](../Page/皮西迪亚.md "wikilink")。

在古代，吕基亚地区的主要城市是[克桑托斯](../Page/克桑托斯.md "wikilink")，[帕塔拉](../Page/帕塔拉.md "wikilink")，[米拉和](../Page/米拉_\(土耳其\).md "wikilink")[法塞利斯](../Page/法塞利斯.md "wikilink")。

## 居民

吕基亚地区在史前时代就有人类居住。当地的土著是[吕基亚人](../Page/吕基亚人.md "wikilink")，他们说一种属于[印欧语系的](../Page/印欧语系.md "wikilink")[语言](../Page/语言.md "wikilink")（[吕基亚语](../Page/吕基亚语.md "wikilink")）。与吕基亚语关系最近的语言是[卢维语](../Page/卢维语.md "wikilink")，它或许是吕基亚语的直接祖先。后来[希腊移民逐渐取代了当地居民成为吕基亚的主人](../Page/希腊.md "wikilink")。希腊人在吕基亚建起了一系列殖民城邦。直到[土耳其统治时代](../Page/土耳其.md "wikilink")，该地区仍有希腊人居住；最后一批希腊居民是因1919年～1922年的[希土战争而被迫离开](../Page/希土战争.md "wikilink")。

## 历史

[古埃及文献告诉我们吕基亚人是](../Page/古埃及.md "wikilink")[赫梯人的盟友](../Page/赫梯.md "wikilink")。在赫梯帝国崩溃后，吕基亚作为一个独立王国发展起来。按照[希罗多德的说法](../Page/希罗多德.md "wikilink")，“吕基亚”这个名字来源于[雅典国王](../Page/雅典.md "wikilink")[潘狄翁二世的儿子](../Page/潘狄翁二世.md "wikilink")[吕科斯的名字](../Page/吕科斯.md "wikilink")。在古典时代，吕基亚地区从未被统一于一个政权之下，而是由许多独立[城邦组成的联合体](../Page/城邦.md "wikilink")。

[荷马在](../Page/荷马.md "wikilink")《[伊利亚特](../Page/伊利亚特.md "wikilink")》中多次提到吕基亚，并说它是[特洛伊人的盟友](../Page/特洛伊.md "wikilink")。在《伊利亚特》里，参战的吕基亚人由[萨尔珀冬](../Page/萨尔珀冬.md "wikilink")（[宙斯与](../Page/宙斯.md "wikilink")[拉俄达弥亚之子](../Page/拉俄达弥亚.md "wikilink")）和[格劳科斯](../Page/格劳科斯.md "wikilink")（[希波格科斯之子](../Page/希波格科斯.md "wikilink")）两位英勇善战的英雄领导。\[1\]非常值得注意的是，在其它一些神话中提到过另一位[萨尔珀冬](../Page/萨尔珀冬.md "wikilink")（宙斯与[欧罗巴之子](../Page/欧罗巴.md "wikilink")）的故事，他因与兄长、[克里特国王](../Page/克里特.md "wikilink")[米诺斯争权失败而被迫流亡吕基亚](../Page/米诺斯.md "wikilink")，并在那里建立了一个王朝。\[2\]古代人经常把这两个[萨尔珀冬相混同](../Page/萨尔珀冬.md "wikilink")。这些神话可能暗示了[克里特文明与吕基亚](../Page/克里特文明.md "wikilink")（或[安纳托利亚](../Page/安纳托利亚.md "wikilink")）之间的某种未知联系。

前546年，[居鲁士大帝的将领](../Page/居鲁士大帝.md "wikilink")[哈尔帕古斯征服了吕基亚](../Page/哈尔帕古斯.md "wikilink")，将其并入[波斯帝国](../Page/波斯帝国.md "wikilink")。哈尔帕古斯的后裔世袭了小亚细亚总督的职位，他们统治吕基亚直到前468年，那年雅典人将波斯帝国的势力赶出了这一地区。波斯人在前387年又夺回了吕基亚，并占据该地直到整个波斯帝国被[亚历山大大帝征服](../Page/亚历山大大帝.md "wikilink")。亚历山大死后，其帝国迅速瓦解。[塞琉古一世在](../Page/塞琉古一世.md "wikilink")[继业者战争中几乎夺得了帝国在](../Page/继业者战争.md "wikilink")[亚洲的全部领地](../Page/亚洲.md "wikilink")，包括吕基亚；他在亚洲建立起最大的一个[希腊化国家](../Page/希腊化国家.md "wikilink")[塞琉古帝国](../Page/塞琉古帝国.md "wikilink")。前189年，塞琉古国王[安条克三世被新兴强国](../Page/安条克三世.md "wikilink")[罗马打败](../Page/罗马共和国.md "wikilink")，包括吕基亚在内的大片土地被割让给罗马。随着罗马的发展，整个[地中海地区都被并入这个帝国](../Page/地中海.md "wikilink")。公元43年，[罗马皇帝](../Page/罗马皇帝.md "wikilink")[克劳狄一世将吕基亚与](../Page/克劳狄一世.md "wikilink")[潘菲利亚合并为罗马的一个](../Page/潘菲利亚.md "wikilink")[行省](../Page/羅馬行省.md "wikilink")，后又单独建省。

在[西罗马帝国灭亡后](../Page/西罗马帝国.md "wikilink")，吕基亚属于东部帝国[拜占廷](../Page/拜占廷帝国.md "wikilink")，因而保留了古典文化。[奥斯曼帝国兴起后征服了拜占廷在亚洲的全部领土并最终灭亡了拜占廷](../Page/奥斯曼帝国.md "wikilink")，吕基亚亦被并入奥斯曼帝国；从此吕基亚即成为土耳其的一部分。

吕基亚在古代是一个重要的宗教活动中心，以崇拜[勒托](../Page/勒托.md "wikilink")、[阿波罗和](../Page/阿波罗.md "wikilink")[阿耳忒弥斯为主](../Page/阿耳忒弥斯.md "wikilink")。

## 吕基亚联盟

吕基亚联盟是吕基亚各城邦在前168年组成的一个政治同盟，它包括已知的23个成员。吕基亚在安条克三世失败后一度被[罗得岛所控制](../Page/罗得岛.md "wikilink")，但罗马人在[第三次马其顿战争结束后决定让吕基亚各城邦在罗马庇护下获得独立](../Page/第三次马其顿战争.md "wikilink")。这些城邦于是建立了一个联邦式的政府，并组建了由各邦成员组成的元老院来选举联盟执政官（执政官的任期为一年）。这个联盟的主要成员是克桑托斯，帕塔拉，[皮纳拉](../Page/皮纳拉.md "wikilink")，[奥林波斯](../Page/奥林波斯.md "wikilink")，米拉和[特洛斯](../Page/特洛斯.md "wikilink")。后来法塞利斯也加入了联盟。有趣的是，在吕基亚被正式并入罗马之后，这个联盟仍然存在并发挥着功能，直到4世纪。

## 注释

<references/>

## 资料来源

  - 部分资料来自古埃及法老和赫梯国王的铭文。J. H. Breasted，《Ancient Records of Egypt》，Vol.
    III；J. B. Pritchard，《Ancient Near Eastern Texts》
  - R. D. Barnett，《剑桥古代史》，362\~366页
  - T. Bryce，Lukka Revisited，《Journal of Near Eastern Studies》，Vol.
    51，121\~130页
  - T. Bryce，J. Zahle，《吕基亚人》

## 外部链接

  - [Lycian
    Turkey](https://web.archive.org/web/20120204045045/http://www.lycianturkey.com/)
  - [Etching of a Lycian
    tomb](http://etc.usf.edu/clipart/10200/10264/lycia_10264.htm)

[Category:罗马帝国行省](../Category/罗马帝国行省.md "wikilink")
[Category:土耳其历史](../Category/土耳其历史.md "wikilink")
[Category:土耳其地理](../Category/土耳其地理.md "wikilink")

1.  荷马，《伊利亚特》，第6卷
2.  希罗多德，I