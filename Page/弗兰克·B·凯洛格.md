**弗兰克·比林斯·凯洛格**（**Frank Billings
Kellogg**，），[美国政治家](../Page/美国.md "wikilink")，曾任[美国国务卿](../Page/美国国务卿.md "wikilink")。1927年，和法國外交部長[阿里斯蒂德·白里安發起](../Page/阿里斯蒂德·白里安.md "wikilink")[非戰公約](../Page/非戰公約.md "wikilink")。凱洛格因此於1930年獲得1929年度的[诺贝尔和平奖](../Page/诺贝尔和平奖.md "wikilink")。

[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:美国国务卿](../Category/美国国务卿.md "wikilink")
[Category:常设国际法院法官](../Category/常设国际法院法官.md "wikilink")
[Category:美國駐英國大使](../Category/美國駐英國大使.md "wikilink")
[Category:美国共和党联邦参议员](../Category/美国共和党联邦参议员.md "wikilink")
[Category:美國共和黨黨員](../Category/美國共和黨黨員.md "wikilink")
[Category:美國律師](../Category/美國律師.md "wikilink")
[Category:律師出身的政治人物](../Category/律師出身的政治人物.md "wikilink")
[Category:紐約州人](../Category/紐約州人.md "wikilink")