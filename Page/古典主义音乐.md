**古典主义音乐**指的是1750至1825年时期的[欧洲主流音乐](../Page/欧洲.md "wikilink")。[海顿](../Page/海顿.md "wikilink")、[莫扎特和](../Page/莫扎特.md "wikilink")[贝多芬是古典主义音乐的杰出代表](../Page/贝多芬.md "wikilink")，由于他们都在维也纳度过自己的创作成熟时期，因而也称作“**维也纳古典乐派**”。也有說法是1750至1820年。但古典時期的結束是以貝多芬第九號交響曲為分水嶺，當代認為在貝多芬第九號交響曲的古典時期特色已消失。

## 歷史

古典主义音乐承繼著[巴洛克音樂的发展](../Page/巴洛克音樂.md "wikilink")，是欧洲音乐史上的一种音乐风格或者一个时代。這個時代出現了多樂章的[交響曲](../Page/交響曲.md "wikilink")、獨奏[協奏曲](../Page/協奏曲.md "wikilink")、[弦樂四重奏](../Page/弦樂四重奏.md "wikilink")、多樂章[奏鳴曲等等體裁](../Page/奏鳴曲.md "wikilink")。而[奏鳴曲式和](../Page/奏鳴曲式.md "wikilink")[輪旋曲式成為古典時期和浪漫時期最常見的曲式](../Page/輪旋曲式.md "wikilink")，影響之深遠直至20世紀。[樂團編制比巴洛克時期增大](../Page/樂團.md "wikilink")，樂團由[指揮帶領逐漸變成一種常規](../Page/指揮.md "wikilink")。現代[鋼琴在古典時期出現](../Page/鋼琴.md "wikilink")，逐漸取代了[大鍵琴的地位](../Page/大鍵琴.md "wikilink")。

隨著[法國大革命對社會造成的衝擊](../Page/法國大革命.md "wikilink")，[作曲家的生計也受到影響](../Page/作曲家.md "wikilink")，由最初依賴[宮廷](../Page/宮廷.md "wikilink")、[教會聘僱轉變為獨立的創作者](../Page/教會.md "wikilink")。

### 早期

  - [海頓](../Page/约瑟夫·海顿.md "wikilink") （Franz Joseph Haydn, 1732-1809）
  - [萨马丁尼](../Page/乔凡尼.萨马丁尼.md "wikilink")
  - [C·P·E·巴赫](../Page/卡爾·菲利普·埃曼努埃爾·巴赫.md "wikilink")
  - [格魯克](../Page/克里斯托夫·維利巴爾德·格魯克.md "wikilink")

### 中期

  - [莫扎特](../Page/沃尔夫冈·阿马多伊斯·莫扎特.md "wikilink") （Wolfgang Amadeus
    Mozart, 1756-1791）
  - [克雷门蒂](../Page/穆齊奧·克萊門蒂.md "wikilink")

### 晚期

  - [貝多芬](../Page/路德维希·范·贝多芬.md "wikilink") （Ludwig van Beethoven,
    1770-1827）
  - [舒伯特](../Page/弗朗茨·舒伯特.md "wikilink")
  - [庫勞](../Page/弗雷德里希·库劳.md "wikilink")
  - [帕格尼尼](../Page/尼可罗·帕格尼尼.md "wikilink")
  - [胡梅尔](../Page/约翰·尼波默克·胡梅尔.md "wikilink")

## 参考文献

## 参见

  - [古典音乐](../Page/古典音乐.md "wikilink")
  - [巴洛克音乐](../Page/巴洛克音乐.md "wikilink")

{{-}}

[Category:古典音樂](../Category/古典音樂.md "wikilink")
[Category:音樂史](../Category/音樂史.md "wikilink")
[Category:古典主義音樂](../Category/古典主義音樂.md "wikilink")