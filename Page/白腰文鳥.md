**白腰文鳥**（[學名](../Page/學名.md "wikilink")：**）是[梅花雀科](../Page/梅花雀科.md "wikilink")[文鸟属的一种](../Page/文鸟属.md "wikilink")，俗名白背文鸟、尖尾文鳥、十姐妹、白腰算命鸟、禾谷，成群出現於海岸濕地、平地到低海拔的草叢中，多见于农作区及山脚地带、也见于小溪、池塘边、庭院内的灌木丛或竹林间、巢多筑于山旁、村庄附近以及溪沟边或庭园内的竹丛、灌丛或针、阔叶树上，分布于[日本](../Page/日本.md "wikilink")（引进种）、[老挝](../Page/老挝.md "wikilink")、[孟加拉国](../Page/孟加拉国.md "wikilink")、[不丹](../Page/不丹.md "wikilink")、[中国](../Page/中国.md "wikilink")（[四川](../Page/四川.md "wikilink")、[云南以东的华南各省](../Page/云南.md "wikilink")）、[越南](../Page/越南.md "wikilink")、[印度](../Page/印度.md "wikilink")、[台湾](../Page/台湾.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[印度尼西亚](../Page/印度尼西亚.md "wikilink")、[缅甸](../Page/缅甸.md "wikilink")、[斯里兰卡](../Page/斯里兰卡.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[尼泊尔](../Page/尼泊尔.md "wikilink")、[新加坡和](../Page/新加坡.md "wikilink")[柬埔寨](../Page/柬埔寨.md "wikilink")。该物种的模式产地在斯里兰卡\[1\]，保护状况被评为[无危](../Page/无危.md "wikilink")。

## 特徵

白腰文鳥平均体重约为12.3克，體長只有約11公分，身上顏色大致都是褐色，腰為白色。嘴[喙成三角錐狀](../Page/喙.md "wikilink")，又厚又尖，上嘴喙黑色，下嘴喙灰色。腰部有條白色寬橫紋帶，尾部又長而尖，鳴叫時發出「啾--啾--」的聲音。

## 棲息地

通常活動於草叢，稻田中，在[榕樹](../Page/榕樹.md "wikilink")、相思樹等上築巢，以草葉編織成球狀鳥巢。

## 繁殖

繁殖期於每年的四月至七月。

## 亞種

共有三个已知亞種：

## 亚种

  - **白腰文鸟云南亚种**（[学名](../Page/学名.md "wikilink")：）。分布于缅甸、印度、马来西亚以及[中国大陆的](../Page/中国大陆.md "wikilink")[云南](../Page/云南.md "wikilink")、[海南等地](../Page/海南.md "wikilink")。该物种的模式产地在缅甸的Bankasoon,Tenasserim\[2\]。
  - **白腰文鸟华南亚种**（[学名](../Page/学名.md "wikilink")：）。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[四川](../Page/四川.md "wikilink")、[云南](../Page/云南.md "wikilink")、广西以东的南部各省、直至台湾、北抵陕西、[河南](../Page/河南.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[江苏](../Page/江苏.md "wikilink")、[浙江等地](../Page/浙江.md "wikilink")。该物种的模式产地在中国\[3\]。
  - **[十姊妹](../Page/十姊妹.md "wikilink")**（[学名](../Page/学名.md "wikilink")：）
  - *L. s. striata*（指名亞種）
  - *L. s. acuticauda*

## 参考文献

## 外部連結

  - [白腰文鳥(Lonchura
    striata)鳴聲(1)](http://www.hkbws.org.hk/web/chi/birdcall/Smunia.wav)香港觀鳥會鳥鳴集
  - [白腰文鳥(Lonchura
    striata)鳴聲(2)](http://www.hkbws.org.hk/web/chi/birdcall/Smunia1.wav)香港觀鳥會鳥鳴集

[Category:文鳥屬](../Category/文鳥屬.md "wikilink")
[Category:寵物鳥](../Category/寵物鳥.md "wikilink")

1.
2.

3.