《**迷宮塔**》（；又譯[龍之塔或](../Page/龍之塔.md "wikilink")[德魯亞加之塔](../Page/德魯亞加之塔.md "wikilink")）是[Namco](../Page/Namco.md "wikilink")（現為Bandai
Namco
Games）於1984年6月發售的动作角色扮演[街機遊戲](../Page/街機遊戲.md "wikilink")，以及其遊戲舞台的塔名。英語名稱為「**The
Tower of Druaga**」。本游戏与[Dragon
Slayer系列](../Page/Dragon_Slayer系列.md "wikilink")（Dragon
Slayer）被认为是最早的[动作角色扮演游戏](../Page/动作角色扮演游戏.md "wikilink")。\[1\]

## 概要

「迷宮塔」具有

  - 尋找出隱藏在各樓層的寶箱的[冒險遊戲要素](../Page/冒險遊戲.md "wikilink")
  - 主角基爾會依裝備不同而成長的[角色扮演遊戲要素](../Page/角色扮演遊戲.md "wikilink")
  - 以古代[美索不達米亞神話為背景](../Page/美索不達米亞.md "wikilink")，表現出劍與魔法的世界以及《[龍與地下城](../Page/龍與地下城.md "wikilink")》等怪物登場的[奇幻要素](../Page/奇幻小說.md "wikilink")
  - 存在強制性的結局

等當時其他街機所沒有的特徵。

作品發表當時由於其獨特的世界設定與遊戲性，得到部分玩家的熱情支持，不過因為複雜難解的寶箱出現條件等緣故，適合狂熱玩家的印象較為強烈。後來移植到[FC遊戲機以及出版](../Page/FC遊戲機.md "wikilink")[攻略本而獲得廣泛的人氣](../Page/攻略本.md "wikilink")，成為1980年代Namco的代表作之一。易於記憶的背景音樂也深得好評。是最早標榜角色扮演遊戲的[電視遊戲之一](../Page/電視遊戲.md "wikilink")。

本作品的遊戲設計者是亦以《[鐵板陣](../Page/鐵板陣.md "wikilink")》（ゼビウス／XEVIOUS）知名的[遠藤雅伸](../Page/遠藤雅伸.md "wikilink")。

## 動畫

《迷宮塔》是由GDH
旗下的GONZO公司所製作電視動畫作品，第一季動畫是《迷宮塔﹣烏魯克之盾﹣》，第二季是《迷宮塔﹣烏魯克之劍﹣》。以古代美索不達米亞神話為背景的《迷宮塔》系列充滿著魔法與怪物，除了冒險元素之外，還有幽默的風格與風趣的對白。故事圍繞著勇者「基爾」為救回被惡魔邪神德魯亞加抓走的戀人「卡依」，化身成黃金騎士，登上六十層樓的「迷宮塔」中進行的冒險；一邊與怪物作戰，一邊層層推進，勇者在一個巨大的塔中展開的故事。

### 迷宮塔 ～烏魯克之盾～（～the Aegis of URUK～）

2006年3月22日，[GDH發佈消息指出取得本作品](../Page/GDH.md "wikilink")[動畫化的權利](../Page/動畫.md "wikilink")、由GDH旗下的[GONZO公司製作動畫版](../Page/GONZO.md "wikilink")《》（[迷宮塔
～烏魯克之盾～](../Page/迷宮塔_\(電視動畫\).md "wikilink")），於2008年春開始播放。製作方面由[Bandai
Namco Games全面監修](../Page/Namco.md "wikilink")，遠藤雅伸並以指導員身分參與製作。

### 迷宮塔 ～烏魯克之劍～（～the Sword of URUK～）

於動畫版《迷宮塔
～烏魯克之盾～》正在播放的同時，[GONZO發表了消息](../Page/GONZO.md "wikilink")，第二季之《》（[迷宮塔
～烏魯克之劍～](../Page/迷宮塔_\(電視動畫\)#迷宮塔_～烏魯克之劍～.md "wikilink")）於2009年一月推出。

## 遊戲

  - 各平台與發售日

<!-- end list -->

  - [街机](../Page/街机.md "wikilink")：1984年7月20日
  - [FC游戏机](../Page/FC游戏机.md "wikilink")：1985年8月6日
  - [MSX](../Page/MSX.md "wikilink")：1986年10月27日
  - FM-7：1987年
  - [GB](../Page/Game_Boy.md "wikilink")：1990年12月31日
  - [PCE](../Page/PC_Engine.md "wikilink")：1992年6月25日
  - [Wii](../Page/Wii.md "wikilink")：2007年9月25日（線上配送版本）
  - 3DS：2012年12月19日（線上配送版本）

### 網路遊戲

  - 迷宮塔：復興巴比倫（The Recovery of BABYLYM）

GDH在動畫製作權利外亦取得了電腦[網路遊戲製作權](../Page/網路遊戲.md "wikilink")，由旗下公司[GONZO
Rosso製作](../Page/GONZO_Rosso.md "wikilink")、經營的《》（迷宮塔：復興巴比倫），於2007年下半年開始服務，於2009年10月1日停止營運。

## 關連作品

  - [太鼓之達人](../Page/太鼓之達人.md "wikilink")：「ドルアーガの塔メドレー」（迷宮塔組曲）就是以本遊戲背景音樂作為組曲，在大台與家用版中登場；另外，「太鼓のマーチ」（太鼓進行曲）、「ナムコットメドレー」（NAMCON組曲）、「未来への鍵」也曾插入本遊戲的背景音樂。

## 注释

## 外部連結

  - [ドルアーガ博物館](http://www.druaga-online.jp/archive/index.html) (遊戲畫面回顧)

  - [DRUAGA ONLINE -THE STORY OF AON-
    迷宮塔官方網站](http://www.druaga-online.jp/)

  - [History of Druaga games from 1UP.com
    迷宮塔的演進史](https://archive.is/20130101163728/http://www.1up.com/do/feature?pager.offset=0&cId=3135870)

[Category:1984年电子游戏](../Category/1984年电子游戏.md "wikilink")
[Category:街機遊戲](../Category/街機遊戲.md "wikilink")
[Category:红白机游戏](../Category/红白机游戏.md "wikilink") [Category:PC
Engine遊戲](../Category/PC_Engine遊戲.md "wikilink") [Category:Game
Boy遊戲](../Category/Game_Boy遊戲.md "wikilink")
[Category:電腦遊戲](../Category/電腦遊戲.md "wikilink")
[Category:PlayStation
(游戏机)游戏](../Category/PlayStation_\(游戏机\)游戏.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:任天堂DS遊戲](../Category/任天堂DS遊戲.md "wikilink")
[Category:行動電話遊戲](../Category/行動電話遊戲.md "wikilink")
[Category:電子角色扮演遊戲](../Category/電子角色扮演遊戲.md "wikilink")
[Category:南梦宫游戏](../Category/南梦宫游戏.md "wikilink")
[Category:MSX游戏](../Category/MSX游戏.md "wikilink")
[Category:迷宮遊戲](../Category/迷宮遊戲.md "wikilink")

1.