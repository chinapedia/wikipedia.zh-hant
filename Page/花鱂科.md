**花鱂科**（[学名](../Page/学名.md "wikilink")：）是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鯉齒目中的一个](../Page/鯉齒目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，它包含200多个[种和亚种](../Page/种_\(生物\).md "wikilink")。

## 分布

花鱂科中的[鱼都比较小](../Page/鱼.md "wikilink")，大多数生活在[淡水中](../Page/淡水.md "wikilink")，只有少数几种生活在[咸水中](../Page/咸水.md "wikilink")。最大的花鱂科的鱼是[墨西哥魣鳉](../Page/墨西哥魣鳉.md "wikilink")（*Belonesox
belizanus*），只达15厘米，一般花鱂科的鱼只到10厘米。这些鱼有两个经济价值：首先它们是很好的控制[蚊子的生物](../Page/蚊子.md "wikilink")，其次它们是比较顽强的，色彩鲜艳的观赏鱼。作为控制蚊虫的生物它们从它们在[美洲的](../Page/美洲.md "wikilink")[热带和](../Page/热带.md "wikilink")[亚热带家乡被人类移居到几乎所有的地区](../Page/亚热带.md "wikilink")。花鱂科本来只在美洲大陆上出现说明这个科是一个比较新的科，是在美洲大陆与其它大陆分隔后才形成的。

## 特徵

花鱂科的鱼的性别的区分非常明显：一般雄性小一些，比雌性活泼。花鱂科的雄性生殖器官是[生殖苞](../Page/生殖苞.md "wikilink")，是一个向外伸长了的[输精管](../Page/输精管.md "wikilink")。生殖苞在雄性鱼的青年时代出现，交配时雄性鱼冲到雌性鱼附近并试图将其生殖苞挂钩到雌性鱼的生殖口上，精子的生命期很长，可以在雌性鱼体内存活很长时间，因此一次交配的精子往往可以用来使多次生殖的卵受精。花鱂科直接生小鱼，仔在雌鱼体内孵化。

有人报道说花鱂科的雌性鱼可以转化为雄性，但从未有人报道说输过精的雄性鱼会变为能生产的雌性鱼，因此也有人认为这个“变化”实际上是成熟比较晚的雄性鱼，它们的生殖苞出现得比较晚，因此被误认为是雌性。

## 生活習性

花鱂科的各个种的形态、生活环境可以非常不同，从浑浊的泥潭到清晰的小溪裡都有不同种类的花鱂科鱼生活。它们大多数是肉食动物，主要食物是水面的小[昆虫和蚊子的幼虫](../Page/昆虫.md "wikilink")。此外它们也吃[藻类或](../Page/藻类.md "wikilink")[水草的幼叶](../Page/水草.md "wikilink")。

## 分類

**花鱂科**下分3亞科44個屬，如下：

### 燈鱂亞科（Aplocheilichthyinae）

#### 燈鱂屬（*Aplocheilichthys*）

  - [恩氏燈鱂](../Page/恩氏燈鱂.md "wikilink")（*Aplocheilichthys antinorii*）
  - [黑鰭燈鱂](../Page/黑鰭燈鱂.md "wikilink")（*Aplocheilichthys atripinnis*）
  - [橘色燈鱂](../Page/橘色燈鱂.md "wikilink")（*Aplocheilichthys bukobanus*）
  - [秀麗燈鱂](../Page/秀麗燈鱂.md "wikilink")（*Aplocheilichthys centralis*）
  - [菲勒燈鱂](../Page/菲勒燈鱂.md "wikilink")（*Aplocheilichthys fuelleborni*）
  - [哈氏燈鱂](../Page/哈氏燈鱂.md "wikilink")（*Aplocheilichthys hutereaui*）
  - [珍妮燈鱂](../Page/珍妮燈鱂.md "wikilink")（*Aplocheilichthys jeanneli*）
  - [約氏燈鱂](../Page/約氏燈鱂.md "wikilink")（*Aplocheilichthys johnstoni*）
  - [條紋燈鱂](../Page/條紋燈鱂.md "wikilink")（*Aplocheilichthys katangae*）
  - [金氏燈鱂](../Page/金氏燈鱂.md "wikilink")（*Aplocheilichthys kingii*）
  - [康果燈鱂](../Page/康果燈鱂.md "wikilink")（*Aplocheilichthys kongoranensis*）
  - [坦尚尼亞燈鱂](../Page/坦尚尼亞燈鱂.md "wikilink")（*Aplocheilichthys lacustris*）
  - [剛果燈鱂](../Page/剛果燈鱂.md "wikilink")（*Aplocheilichthys lualabaensis*）
  - [大尾燈鱂](../Page/大尾燈鱂.md "wikilink")（*Aplocheilichthys macrurus*）
  - [馬哈燈鱂](../Page/馬哈燈鱂.md "wikilink")（*Aplocheilichthys mahagiensis*）
  - [邁氏燈鱂](../Page/邁氏燈鱂.md "wikilink")（*Aplocheilichthys meyburghi*）
  - [莫呂燈鱂](../Page/莫呂燈鱂.md "wikilink")（*Aplocheilichthys moeruensis*）
  - [南非燈鱂](../Page/南非燈鱂.md "wikilink")（*Aplocheilichthys myaposae*）
  - [矮燈鱂](../Page/矮燈鱂.md "wikilink")（*Aplocheilichthys pumilus*）
  - [粗燈鱂](../Page/粗燈鱂.md "wikilink")（*Aplocheilichthys rudolfianus*）
  - [項斑燈鱂](../Page/項斑燈鱂.md "wikilink")（*Aplocheilichthys spilauchen*）
  - [藍身燈鱂](../Page/藍身燈鱂.md "wikilink")（*Aplocheilichthys
    vitschumbaensis*）

#### 雨鱂屬（*Hylopanchax*）

  - [小雨鱂](../Page/小雨鱂.md "wikilink")(*Hylopanchax leki*）
  - [微雨鱂](../Page/微雨鱂.md "wikilink")（*Hylopanchax moke*）
  - [兄弟雨鱂](../Page/兄弟雨鱂.md "wikilink")（*Hylopanchax ndeko*）
  - [剛果雨鱂](../Page/剛果雨鱂.md "wikilink")（*Hylopanchax silvestris*）
  - [胸斑雨鱂](../Page/胸斑雨鱂.md "wikilink")（*Hylopanchax stictopleuron*）

#### 孔燈鱂屬（*Poropanchax*）

  - [布氏孔燈鱂](../Page/布氏孔燈鱂.md "wikilink")（*Poropanchax brichardi*）
  - [哈氏孔燈鱂](../Page/哈氏孔燈鱂.md "wikilink")（*Poropanchax hannerzi*）
  - [盧氏孔燈鱂](../Page/盧氏孔燈鱂.md "wikilink")（*Poropanchax luxophthalmus*）
  - [梅氏孔燈鱂](../Page/梅氏孔燈鱂.md "wikilink")（*Poropanchax myersi*）
  - [諾門孔燈鱂](../Page/諾門孔燈鱂.md "wikilink")（*Poropanchax normani*）
  - [崙氏孔燈鱂](../Page/崙氏孔燈鱂.md "wikilink")（*Poropanchax rancureli*）
  - [西非孔燈鱂](../Page/西非孔燈鱂.md "wikilink")（*Poropanchax stigmatopygus*）

### 花鱂亞科（Poeciliinae）

#### 鱗鱂屬（*Alfaro*）

  - [刀鱗鱂](../Page/刀鱗鱂.md "wikilink")（*Alfaro cultratus*）
  - [尼加拉瓜鱗鱂](../Page/尼加拉瓜鱗鱂.md "wikilink")（*Alfaro huberi*）

#### 魣鱂屬（*Belonesox*）

  - [墨西哥魣鱂](../Page/墨西哥魣鱂.md "wikilink")（*Belonesox belizanus*）

#### 短脊鱂屬（*Brachyrhaphis*）

  - [巴拿馬短脊鱂](../Page/巴拿馬短脊鱂.md "wikilink")（*Brachyrhaphis
    cascajalensis*）
  - [埃氏短棒鱂](../Page/埃氏短棒鱂.md "wikilink")（*Brachyrhaphis episcopi*）
  - [哈氏短脊鱂](../Page/哈氏短脊鱂.md "wikilink")（*Brachyrhaphis hartwegi*）
  - [赫氏短脊鱂](../Page/赫氏短脊鱂.md "wikilink")（*Brachyrhaphis hessfeldi*）
  - [霍氏短脊鱂](../Page/霍氏短脊鱂.md "wikilink")（*Brachyrhaphis holdridgei*）
  - [紫帶短脊鱂](../Page/紫帶短脊鱂.md "wikilink")（*Brachyrhaphis olomina*）
  - [哥斯達黎加短脊鱂](../Page/哥斯達黎加短脊鱂.md "wikilink")（*Brachyrhaphis
    parismina*）
  - [螫短脊鱂](../Page/螫短脊鱂.md "wikilink")（*Brachyrhaphis punctifer*）
  - [棒狀短脊鱂](../Page/棒狀短脊鱂.md "wikilink")（*Brachyrhaphis rhabdophora*）
  - [羅森氏短脊鱂](../Page/羅森氏短脊鱂.md "wikilink")（*Brachyrhaphis roseni*）
  - [羅斯氏短脊鱂](../Page/羅斯氏短脊鱂.md "wikilink")（*Brachyrhaphis roswithae*）
  - [特拉巴短脊鱂](../Page/特拉巴短脊鱂.md "wikilink")（*Brachyrhaphis terrabensis*）

#### 卡爾花鱂屬（*Carlhubbsia*）

  - [瓜地馬拉卡爾花鱂](../Page/瓜地馬拉卡爾花鱂.md "wikilink")（*Carlhubbsia kidderi*）
  - [斯氏卡爾花鱂](../Page/斯氏卡爾花鱂.md "wikilink")（*Carlhubbsia stuarti*）

#### 銼齒鱂屬（*Cnesterodon*）

  - [短吻銼齒鱂](../Page/短吻銼齒鱂.md "wikilink")（*Cnesterodon brevirostratus*）
  - [卡氏銼齒鱂](../Page/卡氏銼齒鱂.md "wikilink")（*Cnesterodon carnegiei*）
  - [十斑銼齒鱂](../Page/十斑銼齒鱂.md "wikilink")（*Cnesterodon decemmaculatus*）
  - [全翼銼齒鱂](../Page/全翼銼齒鱂.md "wikilink")（*Cnesterodon holopteros*）
  - [文鰩銼齒鱂](../Page/文鰩銼齒鱂.md "wikilink")（*Cnesterodon hypselurus*）
  - [蜥形銼齒鱂](../Page/蜥形銼齒鱂.md "wikilink")（*Cnesterodon iguape*）
  - [巴西銼齒鱂](../Page/巴西銼齒鱂.md "wikilink")（*Cnesterodon omorgmatos*）
  - [尖吻銼齒鱂](../Page/尖吻銼齒鱂.md "wikilink")（*Cnesterodon pirai*）
  - [拉氏銼齒鱂](../Page/拉氏銼齒鱂.md "wikilink")（*Cnesterodon raddai*）
  - [北方銼齒鱂](../Page/北方銼齒鱂.md "wikilink")（*Cnesterodon septentrionalis*）

#### 食蚊魚屬（*Gambusia*）

  - [食蚊魚](../Page/食蚊魚.md "wikilink")（*Gambusia affinis*）
  - [阿氏食蚊魚](../Page/阿氏食蚊魚.md "wikilink")（*Gambusia alvarezi*）
  - [德克薩斯食蚊魚](../Page/德克薩斯食蚊魚.md "wikilink")（*Gambusia amistadensis*）
  - [黑食蚊魚](../Page/黑食蚊魚.md "wikilink")（*Gambusia atrora*）
  - [金黃食蚊魚](../Page/金黃食蚊魚.md "wikilink")（*Gambusia aurata*）
  - [古巴食蚊魚](../Page/古巴食蚊魚.md "wikilink")（*Gambusia baracoana*）
  - [畢比氏食蚊魚](../Page/畢比氏食蚊魚.md "wikilink")（*Gambusia beebei*）
  - [布克氏食蚊魚](../Page/布克氏食蚊魚.md "wikilink")（*Gambusia bucheri*）
  - [克拉克氏食蚊魚](../Page/克拉克氏食蚊魚.md "wikilink")（*Gambusia clarkhubbsi*）
  - [多明戈食蚊魚](../Page/多明戈食蚊魚.md "wikilink")（*Gambusia dominicensis*）
  - [埃氏食蚊魚](../Page/埃氏食蚊魚.md "wikilink")（*Gambusia echeagarayi*）
  - [寬口食蚊魚](../Page/寬口食蚊魚.md "wikilink")（*Gambusia eurystoma*）
  - [大彎食蚊魚](../Page/大彎食蚊魚.md "wikilink")（*Gambusia gaigei*）
  - [蓋氏食蚊魚](../Page/蓋氏食蚊魚.md "wikilink")（*Gambusia geiseri*）
  - [喬氏食蚊魚](../Page/喬氏食蚊魚.md "wikilink")（*Gambusia georgei*）
  - [異鰭食蚊魚](../Page/異鰭食蚊魚.md "wikilink")（*Gambusia heterochir*）
  - [希氏食蚊魚](../Page/希氏食蚊魚.md "wikilink")（*Gambusia hispaniolae*）
  - [霍氏食蚊魚](../Page/霍氏食蚊魚.md "wikilink")（*Gambusia holbrooki*）
  - [赫氏食蚊魚](../Page/赫氏食蚊魚.md "wikilink")（*Gambusia hurtadoi*）
  - [克氏食蚊魚](../Page/克氏食蚊魚.md "wikilink")（*Gambusia krumholzi*）
  - [萊氏食蚊魚](../Page/萊氏食蚊魚.md "wikilink")（*Gambusia lemaitrei*）
  - [長刺食蚊魚](../Page/長刺食蚊魚.md "wikilink")（*Gambusia longispinis*）
  - [野食蚊魚](../Page/野食蚊魚.md "wikilink")（*Gambusia luma*）
  - [曼氏食蚊魚](../Page/曼氏食蚊魚.md "wikilink")（*Gambusia manni*）
  - [馬氏食蚊魚](../Page/馬氏食蚊魚.md "wikilink")（*Gambusia marshi*）
  - [黑胸食蚊魚](../Page/黑胸食蚊魚.md "wikilink")（*Gambusia melapleura*）
  - [蒙的考拉食蚊魚](../Page/蒙的考拉食蚊魚.md "wikilink")（*Gambusia monticola*）
  - [邁爾斯食蚊魚](../Page/邁爾斯食蚊魚.md "wikilink")（*Gambusia myersi*）
  - [尼加拉瓜食蚊魚](../Page/尼加拉瓜食蚊魚.md "wikilink")（*Gambusia nicaraguensis*）
  - [珍食蚊魚](../Page/珍食蚊魚.md "wikilink")（*Gambusia nobilis*）
  - [墨西哥食蚊魚](../Page/墨西哥食蚊魚.md "wikilink")（*Gambusia panuco*）
  - [擬斑食蚊魚](../Page/擬斑食蚊魚.md "wikilink")（*Gambusia pseudopunctata*）
  - [藍斑食蚊魚](../Page/藍斑食蚊魚.md "wikilink")（*Gambusia punctata*）
  - [珍珠食蚊魚](../Page/珍珠食蚊魚.md "wikilink")（*Gambusia puncticulata*）
  - [雷根氏食蚊魚](../Page/雷根氏食蚊魚.md "wikilink")（*Gambusia regani*）
  - [曼格食蚊魚](../Page/曼格食蚊魚.md "wikilink")（*Gambusia rhizophorae*）
  - [斑食蚊魚](../Page/斑食蚊魚.md "wikilink")（*Gambusia senilis*）
  - [六輻食蚊魚](../Page/六輻食蚊魚.md "wikilink")（*Gambusia sexradiata*）
  - [燦食蚊魚](../Page/燦食蚊魚.md "wikilink")（*Gambusia speciosa*）
  - [飾帶食蚊魚](../Page/飾帶食蚊魚.md "wikilink")（*Gambusia vittata*）
  - [牙買加食蚊魚](../Page/牙買加食蚊魚.md "wikilink")（*Gambusia wrayi*）
  - [黃體食蚊魚](../Page/黃體食蚊魚.md "wikilink")（*Gambusia xanthosoma*）
  - [尤卡坦食蚊魚](../Page/尤卡坦食蚊魚.md "wikilink")（*Gambusia yucatana*）
  - [紮氏食蚊魚](../Page/紮氏食蚊魚.md "wikilink")（*Gambusia zarskei*）

#### 吉拉德食蚊魚屬（*Girardinus*）

  - [中美洲吉拉德食蚊魚](../Page/中美洲吉拉德食蚊魚.md "wikilink")（*Girardinus creolus*）
  - [古巴吉拉德食蚊魚](../Page/古巴吉拉德食蚊魚.md "wikilink")（*Girardinus cubensis*）
  - [小齒吉拉德食蚊魚](../Page/小齒吉拉德食蚊魚.md "wikilink")（*Girardinus
    denticulatus*）
  - [鐮形吉拉德食蚊魚](../Page/鐮形吉拉德食蚊魚.md "wikilink")（*Girardinus falcatus*）
  - [吉拉德食蚊魚](../Page/吉拉德食蚊魚.md "wikilink")（*Girardinus metallicus*）
  - [小指吉拉德食蚊魚](../Page/小指吉拉德食蚊魚.md "wikilink")（*Girardinus
    microdactylus*）
  - [單背吉拉德食蚊魚](../Page/單背吉拉德食蚊魚.md "wikilink")（*Girardinus uninotatus*）

#### 異小鱂屬（*Heterandria*）

  - [安氏異小鱂](../Page/安氏異小鱂.md "wikilink")（*Heterandria anzuetoi*）
  - [狹頭異小鱂](../Page/狹頭異小鱂.md "wikilink")（*Heterandria attenuata*）
  - [雙斑異小鱂](../Page/雙斑異小鱂.md "wikilink")（*Heterandria bimaculatus*）
  - [瓜地馬拉異小鱂](../Page/瓜地馬拉異小鱂.md "wikilink")（*Heterandria cataractae*）
  - [喜蔭異小鱂](../Page/喜蔭異小鱂.md "wikilink")（*Heterandria dirempta*）
  - [美麗異小鱂](../Page/美麗異小鱂.md "wikilink")（*Heterandria formosa*）
  - [瓊斯氏異小鱂](../Page/瓊斯氏異小鱂.md "wikilink")（*Heterandria jonesii*）
  - [熱帶異小鱂](../Page/熱帶異小鱂.md "wikilink")（*Heterandria litoperas*）
  - [黑珍珠異小鱂](../Page/黑珍珠異小鱂.md "wikilink")（*Heterandria tuxtlaensis*）

#### 異筆鱂屬（*Heterophallus*）

  - [米氏異筆鱂](../Page/米氏異筆鱂.md "wikilink") （*Heterophallus milleri*）
  - [雷氏異筆鱂](../Page/雷氏異筆鱂.md "wikilink")（*Heterophallus rachovii*）

#### 泥鱂屬（*Limia*）

  - [開曼島泥鱂](../Page/開曼島泥鱂.md "wikilink")（*Limia caymanensis*）
  - [聖多明各泥鱂](../Page/聖多明各泥鱂.md "wikilink")（*Limia dominicensis*）
  - [棕斑泥鱂](../Page/棕斑泥鱂.md "wikilink")（*Limia fuscomaculata*）
  - [加氏泥鱂](../Page/加氏泥鱂.md "wikilink")（*Limia garnieri*）
  - [厚身泥鱂](../Page/厚身泥鱂.md "wikilink")（*Limia grossidens*）
  - [委內瑞拉花鱂](../Page/委內瑞拉花鱂.md "wikilink")（''Limia heterandria）
  - [無斑泥鱂](../Page/無斑泥鱂.md "wikilink")（*Limia immaculata*）
  - [黑腹泥鱂](../Page/黑腹泥鱂.md "wikilink")（*Limia melanogaster*）
  - [黑背泥鱂](../Page/黑背泥鱂.md "wikilink")（*Limia melanonotata*）
  - [米拉貢湖泥鱂](../Page/米拉貢湖泥鱂.md "wikilink")（*Limia miragoanensis*）
  - [黑帶泥鱂](../Page/黑帶泥鱂.md "wikilink")（*Limia nigrofasciata*）
  - [綠泥鱂](../Page/綠泥鱂.md "wikilink")（*Limia ornata*）
  - [少輻泥鱂](../Page/少輻泥鱂.md "wikilink")（*Limia pauciradiata*）
  - [佩羅泥鱂](../Page/佩羅泥鱂.md "wikilink")（*Limia perugiae*）
  - [里氏花鱂](../Page/里氏花鱂.md "wikilink")（（*Limia rivasi*）
  - [多明尼加泥鱂](../Page/多明尼加泥鱂.md "wikilink")（*Limia sulphurophila*）
  - [叉齒泥鱂](../Page/叉齒泥鱂.md "wikilink")（*Limia tridens*）
  - [變色泥鱂](../Page/變色泥鱂.md "wikilink")（*Limia versicolor*）
  - [古巴泥鱂](../Page/古巴泥鱂.md "wikilink")（*Limia vittata*）
  - [亞氏泥鱂](../Page/亞氏泥鱂.md "wikilink")（*Limia yaguajali*）
  - [帶泥鱂](../Page/帶泥鱂.md "wikilink")（*Limia zonata*）

#### 小花鱂屬（*Micropoecilia*）

  - [雙叉小花鱂](../Page/雙叉小花鱂.md "wikilink")（*Micropoecilia bifurca*）
  - [布氏小花鱂](../Page/布氏小花鱂.md "wikilink")（*Micropoecilia branneri*）
  - [巴西小花鱂](../Page/巴西小花鱂.md "wikilink")（*Micropoecilia minima*）
  - [斑尾小花鱂](../Page/斑尾小花鱂.md "wikilink")（*Micropoecilia picta*）

#### 新異鱂屬（*Neoheterandria*）

  - [犬新異鱂](../Page/犬新異鱂.md "wikilink")（*Neoheterandria cana*）
  - [美麗新異鱂](../Page/美麗新異鱂.md "wikilink")（*Neoheterandria elegans*）
  - [三峰新異鱂](../Page/三峰新異鱂.md "wikilink")（*Neoheterandria tridentiger*）

#### 賊胎鱂屬（*Pamphorichthys*）

  - [巴西賊胎鱂](../Page/巴西賊胎鱂.md "wikilink")（*Pamphorichthys araguaiensis*）
  - [哈氏賊胎鱂](../Page/哈氏賊胎鱂.md "wikilink")（*Pamphorichthys hasemani*）
  - [霍氏賊胎鱂](../Page/霍氏賊胎鱂.md "wikilink")（*Pamphorichthys hollandi*）
  - [亞馬遜河賊胎鱂](../Page/亞馬遜河賊胎鱂.md "wikilink")（'' Pamphorichthys minor''）
  - [南美賊胎鱂](../Page/南美賊胎鱂.md "wikilink")（*Pamphorichthys pertapeh*）
  - [刀形賊胎鱂](../Page/刀形賊胎鱂.md "wikilink")（*Pamphorichthys scalpridens*）

#### 莖鱂屬（*Phallichthys*）

  - [黑肛莖鱂](../Page/黑肛莖鱂.md "wikilink")（*Phallichthys amates*）
  - [費氏莖鱂](../Page/費氏莖鱂.md "wikilink")（*Phallichthys fairweatheri*）
  - [四斑莖鱂](../Page/四斑莖鱂.md "wikilink")（*Phallichthys quadripunctatus*）
  - [哥斯大黎加莖鱂](../Page/哥斯大黎加莖鱂.md "wikilink")（*Phallichthys tico*）

#### 角莖鱂屬（*Phalloceros*）

  - [亞歷山大角莖鱂](../Page/亞歷山大角莖鱂.md "wikilink")（*Phalloceros alessandrae*）
  - [茴角莖鱂](../Page/茴角莖鱂.md "wikilink")（*Phalloceros anisophallos*）
  - [盾角莖鱂](../Page/盾角莖鱂.md "wikilink")（*Phalloceros aspilos*）
  - [巴氏角莖鱂](../Page/巴氏角莖鱂.md "wikilink")（*Phalloceros buckupi*）
  - [尾點角莖鱂](../Page/尾點角莖鱂.md "wikilink")（*Phalloceros caudimaculatus*）
  - [小角莖鱂](../Page/小角莖鱂.md "wikilink")（*Phalloceros elachistos*）
  - [九背鰭條角莖鱂](../Page/九背鰭條角莖鱂.md "wikilink")（*Phalloceros enneaktinos*）
  - [巴拉圭角莖鱂](../Page/巴拉圭角莖鱂.md "wikilink")（*Phalloceros harpagos*）
  - [七背鰭條角莖鱂](../Page/七背鰭條角莖鱂.md "wikilink")（*Phalloceros heptaktinos*）
  - [鉤器角莖鱂](../Page/鉤器角莖鱂.md "wikilink")（*Phalloceros leptokeras*）
  - [佳美角莖鱂](../Page/佳美角莖鱂.md "wikilink")（*Phalloceros leticiae*）
  - [銀光角莖鱂](../Page/銀光角莖鱂.md "wikilink")（*Phalloceros lucenorum*）
  - [馬氏角莖鱂](../Page/馬氏角莖鱂.md "wikilink")（*Phalloceros malabarbai*）
  - [大孔角莖鱂](../Page/大孔角莖鱂.md "wikilink")（*Phalloceros megapolos*）
  - [纖細角莖鱂](../Page/纖細角莖鱂.md "wikilink")（*Phalloceros mikrommatos*）
  - [眼點角莖鱂](../Page/眼點角莖鱂.md "wikilink")（*Phalloceros ocellatus*）
  - [暗色角莖鱂](../Page/暗色角莖鱂.md "wikilink")（*Phalloceros pellos*）
  - [賴斯角莖鱂](../Page/賴斯角莖鱂.md "wikilink")（*Phalloceros reisi*）
  - [斑尾角莖鱂](../Page/斑尾角莖鱂.md "wikilink")（*Phalloceros spiloura*）
  - [巴西角莖鱂](../Page/巴西角莖鱂.md "wikilink")（*Phalloceros titthos*）
  - [變色角莖鱂](../Page/變色角莖鱂.md "wikilink")（*Phalloceros tupinamba*）
  - [烏氏角莖鱂](../Page/烏氏角莖鱂.md "wikilink")（*Phalloceros uai*）

#### 褶莖鱂屬（*Phalloptychus*）

  - [艾氏褶莖鱂](../Page/艾氏褶莖鱂.md "wikilink")（*Phalloptychus eigenmanni*）
  - [巴拉圭河褶莖鱂](../Page/巴拉圭河褶莖鱂.md "wikilink")（*Phalloptychus januarius*）

#### 亮花鱂屬（*Phallotorynus*）

  - [側斑亮花鱂](../Page/側斑亮花鱂.md "wikilink")（*Phallotorynus dispilos*）
  - [巴西亮花鱂](../Page/巴西亮花鱂.md "wikilink")（*Phallotorynus fasciolatus*）
  - [愉亮花鱂](../Page/愉亮花鱂.md "wikilink")（*Phallotorynus jucundus*）
  - [頷帶亮花鱂](../Page/頷帶亮花鱂.md "wikilink")（*Phallotorynus pankalos*）
  - [多肋亮花鱂](../Page/多肋亮花鱂.md "wikilink")（*Phallotorynus psittakos*）
  - [維多利亞亮花鱂](../Page/維多利亞亮花鱂.md "wikilink")（*Phallotorynus vitoriae*）

#### 花鱂屬（*Poecilia*）

  - [鮑氏花鱂](../Page/鮑氏花鱂.md "wikilink")（*Poecilia boesemani*）
  - [巴氏花鱂](../Page/巴氏花鱂.md "wikilink")（*Poecilia butleri*）
  - [胭脂花鱂](../Page/胭脂花鱂.md "wikilink")（*Poecilia catemaconis*）
  - [南美花鱂](../Page/南美花鱂.md "wikilink")（*Poecilia caucana*）
  - [尾帶花鱂](../Page/尾帶花鱂.md "wikilink")（*Poecilia caudofasciata*）
  - [稀卡花鱂](../Page/稀卡花鱂.md "wikilink")（*Poecilia chica*）
  - [多氏花鱂](../Page/多氏花鱂.md "wikilink")（*Poecilia dauli*）
  - [多明尼加花鱂](../Page/多明尼加花鱂.md "wikilink")（*Poecilia
    dominicensis*）：又稱灘棲花鱂。
  - [美麗花鱂](../Page/美麗花鱂.md "wikilink")（*Poecilia elegans*）
  - [秀美花鱂](../Page/秀美花鱂.md "wikilink")（*Poecilia formosa*）
  - [吉氏花鱂](../Page/吉氏花鱂.md "wikilink")（*Poecilia gillii*）
  - [伊斯帕尼奧拉花鱂](../Page/伊斯帕尼奧拉花鱂.md "wikilink")（*Poecilia hispaniolana*）
  - [宏都拉斯花鱂](../Page/宏都拉斯花鱂.md "wikilink")（*Poecilia
    hondurensis*）：又稱燦爛花鱂。
  - [肯普氏花鱂](../Page/肯普氏花鱂.md "wikilink")(*Poecilia kempkesi*)
  - [科氏花鱂](../Page/科氏花鱂.md "wikilink")（*Poecilia koperi*）
  - [紅邊花鱂](../Page/紅邊花鱂.md "wikilink")（*Poecilia kykesis*）
  - [茉莉花鱂](../Page/茉莉花鱂.md "wikilink")（*Poecilia latipinna*）
  - [側斑花鱂](../Page/側斑花鱂.md "wikilink")（*Poecilia latipunctata*）
  - [馬塞林花鱂](../Page/馬塞林花鱂.md "wikilink")（*Poecilia marcellinoi*）
  - [梅氏花鱂](../Page/梅氏花鱂.md "wikilink")（*Poecilia maylandi*）
  - [哥倫比亞花鱂](../Page/哥倫比亞花鱂.md "wikilink")（*Poecilia mechthildae*）
  - [短鰭花鱂](../Page/短鰭花鱂.md "wikilink")（*Poecilia mexicana*）：又稱短帆鱂。
  - [尼氏花鱂](../Page/尼氏花鱂.md "wikilink")（*Poecilia nicholsi*）
  - [暗花鱂](../Page/暗花鱂.md "wikilink")（*Poecilia obsucra*）
  - [奧氏花鱂](../Page/奧氏花鱂.md "wikilink")（*Poecilia orri*）
  - [雙點花鱂](../Page/雙點花鱂.md "wikilink")（*Poecilia parae*）
  - [佩藤花鱂](../Page/佩藤花鱂.md "wikilink")（*Poecilia petenensis*）
  - [孔雀花鱂](../Page/孔雀花鱂.md "wikilink")（*Poecilia reticulata*）：又稱虹鱂或孔雀魚。
  - [瓜地馬拉花鱂](../Page/瓜地馬拉花鱂.md "wikilink")（*Poecilia rositae*）
  - [薩爾瓦多花鱂](../Page/薩爾瓦多花鱂.md "wikilink")（*Poecilia salvatoris*）
  - [薩氏花鱂](../Page/薩氏花鱂.md "wikilink")（*Poecilia sarrafae*）
  - [黑花鱂](../Page/黑花鱂.md "wikilink")（*Poecilia sphenops*）
  - [野花鱂](../Page/野花鱂.md "wikilink")（*Poecilia sulphuraria*）
  - [特里薩花鱂](../Page/特里薩花鱂.md "wikilink")（*Poecilia teresae*）
  - [范氏花鱂](../Page/范氏花鱂.md "wikilink")（*Poecilia vandepolli*）
  - [帆鰭花鱂](../Page/帆鰭花鱂.md "wikilink")（*Poecilia velifera*）
  - [胎花鱂](../Page/胎花鱂.md "wikilink")（*Poecilia vivipara*）
  - [瓦氏花鱂](../Page/瓦氏花鱂.md "wikilink")（*Poecilia waiapi*）
  - [旺德花鱂](../Page/旺德花鱂.md "wikilink")（*Poecilia wandae*）
  - [溫氏花鱂](../Page/溫氏花鱂.md "wikilink")（*Poecilia wingei*）

#### 若花鱂屬（*Poeciliopsis*）

  - [貝氏若花鱂](../Page/貝氏若花鱂.md "wikilink")（*Poeciliopsis baenschi*）
  - [墨西哥若花鱂](../Page/墨西哥若花鱂.md "wikilink")（*Poeciliopsis balsas*）
  - [卡特馬可若花鱂](../Page/卡特馬可若花鱂.md "wikilink")（*Poeciliopsis catemaco*）
  - [長身若花鱂](../Page/長身若花鱂.md "wikilink")（*Poeciliopsis elongata*）
  - [條紋若花鱂](../Page/條紋若花鱂.md "wikilink")（*Poeciliopsis fasciata*）
  - [若花鱂](../Page/若花鱂.md "wikilink")（*Poeciliopsis gracilis*）
  - [尼氏若花鱂](../Page/尼氏若花鱂.md "wikilink")（*Poeciliopsis hnilickai*）
  - [啞若花鱂](../Page/啞若花鱂.md "wikilink")（*Poeciliopsis infans*）
  - [側齒若花鱂](../Page/側齒若花鱂.md "wikilink")（*Poeciliopsis latidens*）
  - [光若花鱂](../Page/光若花鱂.md "wikilink")（*Poeciliopsis lucida*）
  - [盧茨氏若花鱂](../Page/盧茨氏若花鱂.md "wikilink")（*Poeciliopsis lutzi*）
  - [孤若花鱂](../Page/孤若花鱂.md "wikilink")（*Poeciliopsis monacha*）
  - [西域若花鱂](../Page/西域若花鱂.md "wikilink")（*Poeciliopsis occidentalis*）
  - [少斑若花鱂](../Page/少斑若花鱂.md "wikilink")（*Poeciliopsis paucimaculata*）
  - [胸斑若花鱂](../Page/胸斑若花鱂.md "wikilink")（*Poeciliopsis pleurospilus*）
  - [中美洲若花鱂](../Page/中美洲若花鱂.md "wikilink")（*Poeciliopsis presidionis*）
  - [多育若花鱂](../Page/多育若花鱂.md "wikilink")（*Poeciliopsis prolifica*）
  - [彎鰭若花鱂](../Page/彎鰭若花鱂.md "wikilink")（*Poeciliopsis retropinna*）
  - [寶蓮若花鱂](../Page/寶蓮若花鱂.md "wikilink")（*Poeciliopsis santaelena*）
  - [斯氏若花鱂](../Page/斯氏若花鱂.md "wikilink")（*Poeciliopsis scarlli*）
  - [沙諾氏若花鱂](../Page/沙諾氏若花鱂.md "wikilink")（*Poeciliopsis sonoriensis*）
  - [特氏若花鱂](../Page/特氏若花鱂.md "wikilink")（*Poeciliopsis turneri*）
  - [哥斯達黎加若花鱂](../Page/哥斯達黎加若花鱂.md "wikilink")（*Poeciliopsis
    turrubarensis*）
  - [壯體若花鱂](../Page/壯體若花鱂.md "wikilink")（*Poeciliopsis viriosa*）

#### 鋸花鱂屬（*Priapella*）

  - [墨西哥鋸花鱂](../Page/墨西哥鋸花鱂.md "wikilink")（*Priapella bonita*）
  - [胭脂鋸花鱂](../Page/胭脂鋸花鱂.md "wikilink")（*Priapella chamulae*）
  - [窄身鋸花鱂](../Page/窄身鋸花鱂.md "wikilink")（*Priapella compressa*）
  - [中間鋸花鱂](../Page/中間鋸花鱂.md "wikilink")（*Priapella intermedia*）
  - [拉氏鋸花鱂](../Page/拉氏鋸花鱂.md "wikilink")（*Priapella
    lacandonae*）：又稱琥珀鋸花鱂。
  - [奧氏鋸花鱂](../Page/奧氏鋸花鱂.md "wikilink")（*Priapella olmecae*）

#### 艷花鱂屬（*Priapichthys*）

  - [艷花鱂](../Page/艷花鱂.md "wikilink")（*Priapichthys annectens*）
  - [卡利艷花鱂](../Page/卡利艷花鱂.md "wikilink")（*Priapichthys caliensis*）
  - [哥倫比亞艷花鱂](../Page/哥倫比亞艷花鱂.md "wikilink")（*Priapichthys chocoensis*）
  - [達里安艷花鱂](../Page/達里安艷花鱂.md "wikilink")（''Priapichthys darienensis）
  - [黑腹艷花鱂](../Page/黑腹艷花鱂.md "wikilink")（*Priapichthys nigroventralis*）
  - [巴拿馬艷花鱂](../Page/巴拿馬艷花鱂.md "wikilink")（*Priapichthys panamensis*）
  - [皮氏艷花鱂](../Page/皮氏艷花鱂.md "wikilink")（*Priapichthys puetzi*）

#### 擬花鱂屬（*Pseudopoecilia*）

  - [哥倫比亞擬花鱂](../Page/哥倫比亞擬花鱂.md "wikilink")（*Pseudopoecilia
    austrocolumbiana*）
  - [費氏擬花鱂](../Page/費氏擬花鱂.md "wikilink")（*Pseudopoecilia festae*）
  - [厄瓜多擬花鱂](../Page/厄瓜多擬花鱂.md "wikilink")（'' Pseudopoecilia fria''）

#### 擬劍尾魚屬(*Pseudoxiphophorus*)

  - [斜帶擬劍尾魚](../Page/斜帶擬劍尾魚.md "wikilink")（*Pseudoxiphophorus obliqua*）

#### 美花鱂屬（*Quintana*）

  - [美花鱂](../Page/美花鱂.md "wikilink")（*Quintana atrizona*）

#### 司考花鱂屬（*Scolichthys*）

  - [格氏司考花鱂](../Page/格氏司考花鱂.md "wikilink")（*Scolichthys greenwayi*）
  - [瓜地馬拉考花鱂](../Page/瓜地馬拉考花鱂.md "wikilink")（*Scolichthys iota*）

#### 尖尾鱂屬（*Tomeurus*）

  - [細尖尾鱂](../Page/細尖尾鱂.md "wikilink")（*Tomeurus gracilis*）

#### 花奇鱂屬（*Xenodexia*）

  - [花奇鱂](../Page/花奇鱂.md "wikilink")（*Xenodexia ctenolepis*）

#### 莖奇鱂屬（*Xenophallus*）

  - [蟄棲新異鱂](../Page/蟄棲新異鱂.md "wikilink")（*Xenophallus umbratilis*）

#### [劍尾魚屬](../Page/劍尾魚屬.md "wikilink")（*Xiphophorus*）

### 絲足鱂亞科（Procatopodinae）

#### 溪花鱂屬（*Fluviphylax*）

  - [暗色溪花鱂](../Page/暗色溪花鱂.md "wikilink")（*Fluviphylax obscurum*）
  - [帕氏溪花鱂](../Page/帕氏溪花鱂.md "wikilink")（*Fluviphylax palikur*）
  - [溪花鱂](../Page/溪花鱂.md "wikilink")（*Fluviphylax pygmaeus*）
  - [簡溪花鱂](../Page/簡溪花鱂.md "wikilink")（*Fluviphylax simplex*）
  - [帶溪花鱂](../Page/帶溪花鱂.md "wikilink")（*Fluviphylax zonatus*）

#### 花高鱂屬（*Hypsopanchax*）

  - [蓮花高鱂](../Page/蓮花高鱂.md "wikilink")（*Hypsopanchax catenatus*）
  - [紮伊爾花高鱂](../Page/紮伊爾花高鱂.md "wikilink")（*Hypsopanchax deprimozi*）
  - [喬氏花高鱂](../Page/喬氏花高鱂.md "wikilink")（*Hypsopanchax jobaerti*）
  - [朱氏花高鱂](../Page/朱氏花高鱂.md "wikilink")（*Hypsopanchax jubbi*）
  - [花高鱂](../Page/花高鱂.md "wikilink")（*Hypsopanchax platysternus*）
  - [條紋花高鱂](../Page/條紋花高鱂.md "wikilink")（*Hypsopanchax zebra*）

#### 湖棲花鱂屬（*Lacustricola*）

  - [斑湖棲花鱂](../Page/斑湖棲花鱂.md "wikilink")（*Lacustricola maculatus*）
  - [馬氏湖棲花鱂](../Page/馬氏湖棲花鱂.md "wikilink")（*Lacustricola matthesi*）
  - [側條湖棲花鱂](../Page/側條湖棲花鱂.md "wikilink")（*Lacustricola
    mediolateralis*）
  - [暗側湖棲花鱂](../Page/暗側湖棲花鱂.md "wikilink")（*Lacustricola
    nigrolateralis*）
  - [背眼湖棲花鱂](../Page/背眼湖棲花鱂.md "wikilink")（*Lacustricola omoculatus*）
  - [烏沙湖棲花鱂](../Page/烏沙湖棲花鱂.md "wikilink")（*Lacustricola usanguensis*）

#### 亮麗鱂屬（*Lamprichthys*）

  - [坦干伊喀亮麗鱂](../Page/坦干伊喀亮麗鱂.md "wikilink")（*Lamprichthys
    tanganicanus*）

#### 卉鱂屬（*Micropanchax*）

  - [多哥卉鱂](../Page/多哥卉鱂.md "wikilink")（*Micropanchax bracheti*）
  - [喀麥隆卉鱂](../Page/喀麥隆卉鱂.md "wikilink")（*Micropanchax camerunensis*）
  - [伊氏卉鱂](../Page/伊氏卉鱂.md "wikilink")（*Micropanchax ehrichi*）
  - [凱氏卉鱂](../Page/凱氏卉鱂.md "wikilink")（*Micropanchax keilhacki*）
  - [洛氏卉鱂](../Page/洛氏卉鱂.md "wikilink")（*Micropanchax loati*）
  - [大眼卉鱂](../Page/大眼卉鱂.md "wikilink")（*Micropanchax macrophthalmus*）
  - [亞伯特湖卉鱂](../Page/亞伯特湖卉鱂.md "wikilink")（*Micropanchax pelagicus*）
  - [帕氏卉鱂](../Page/帕氏卉鱂.md "wikilink")（*Micropanchax pfaffi*）
  - [施氏卉鱂](../Page/施氏卉鱂.md "wikilink")（*Micropanchax scheeli*）

#### 豹齒鱂屬（*Pantanodon*）

  - [馬達加斯加豹齒鱂](../Page/馬達加斯加豹齒鱂.md "wikilink")（*Pantanodon
    madagascariensis*）
  - [豹齒鱂](../Page/豹齒鱂.md "wikilink")（*Pantanodon podoxys*）

#### 扁花鱂屬（*Plataplochilus*）

  - [卡賓扁花鱂](../Page/卡賓扁花鱂.md "wikilink")（*Plataplochilus cabindae*）
  - [蜥紋扁花鱂](../Page/蜥紋扁花鱂.md "wikilink")（*Plataplochilus chalcopyrus*）
  - [大眼扁花鱂](../Page/大眼扁花鱂.md "wikilink")（*Plataplochilus loemensis*）
  - [赭帶扁花鱂](../Page/赭帶扁花鱂.md "wikilink")（*Plataplochilus miltotaenia*）
  - [加彭扁花鱂](../Page/加彭扁花鱂.md "wikilink")（*Plataplochilus mimus*）
  - [扁花鱂](../Page/扁花鱂.md "wikilink")（*Plataplochilus ngaensis*）
  - [秀麗扁花鱂](../Page/秀麗扁花鱂.md "wikilink")（*Plataplochilus pulcher*）
  - [特氏扁花鱂](../Page/特氏扁花鱂.md "wikilink")（*Plataplochilus terveri*）

#### 平花鱂屬（*Platypanchax*）

  - [平花鱂](../Page/平花鱂.md "wikilink")（*Platypanchax modestus*）

#### 絲足鱂屬（*Procatopus*）

  - [異常絲足鱂](../Page/異常絲足鱂.md "wikilink")（*Procatopus aberrans*）
  - [背紋絲足鱂](../Page/背紋絲足鱂.md "wikilink")（*Procatopus nototaenia*）
  - [類絲足鱂](../Page/類絲足鱂.md "wikilink")（*Procatopus similis*）
  - [韋氏絲足鱂](../Page/韋氏絲足鱂.md "wikilink")（*Procatopus websteri*）

#### 裂燈鱂屬（*Rhexipanchax*）

  - [卡巴裂燈鱂](../Page/卡巴裂燈鱂.md "wikilink")（*Rhexipanchax kabae*）
  - [蘭氏裂燈鱂](../Page/蘭氏裂燈鱂.md "wikilink")（*Rhexipanchax lamberti*）
  - [寧巴裂燈鱂](../Page/寧巴裂燈鱂.md "wikilink")（*Rhexipanchax nimbaensis*）
  - [希氏裂燈鱂](../Page/希氏裂燈鱂.md "wikilink")（*Rhexipanchax schioetzi*）

## 参考文献

## 外部链接

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[\*](../Category/花鳉科.md "wikilink")