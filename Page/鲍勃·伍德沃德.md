**-{zh-hans:罗伯特·厄普舍;zh-hant:羅伯特·厄普舍;zh-tw:勞勃·厄普蘇爾;zh-hk:羅拔·厄普舍;}-·伍德沃德**（，），美國記者，是揭穿[水門事件醜聞的兩名](../Page/水門事件.md "wikilink")《[華盛頓郵報](../Page/華盛頓郵報.md "wikilink")》記者之一。

伍德沃德出生于[美国](../Page/美国.md "wikilink")[伊利诺伊州](../Page/伊利诺伊州.md "wikilink")。1965年，从[耶鲁大学毕业后进入](../Page/耶鲁大学.md "wikilink")[海军服役](../Page/海军.md "wikilink")，在1970年退役后进入《[华盛顿邮报](../Page/华盛顿邮报.md "wikilink")》成为了一名实习[记者](../Page/记者.md "wikilink")，从而开始了他的记者生涯。

1972年，鲍勃·伍德沃德与另一名记者[卡尔·伯恩斯坦通过其内线](../Page/卡尔·伯恩斯坦.md "wikilink")「[深喉](../Page/深喉_\(水門事件\).md "wikilink")」（即[马克·费尔特](../Page/马克·费尔特.md "wikilink")）的情报及协助，率先披露了[水门事件丑闻](../Page/水门事件.md "wikilink")，从而迫使总统[尼克松下台](../Page/尼克松.md "wikilink")，名噪新闻界。两人也因此获得了1973年的[普利策新闻奖](../Page/普利策新闻奖.md "wikilink")，隨後他們把經過合集成書本《[總統的人馬](../Page/總統的人馬.md "wikilink")》（*All
the President's
Men*），在1974年出版。有關此事件，兩人還合著了另外兩本書：第一本是《[最後的日子](../Page/最後的日子.md "wikilink")》（*The
Final
Days*），有關[尼克遜下台前的日子](../Page/尼克遜.md "wikilink")，在1976年出版；第二本是在「深喉」的身分於2005年曝光後出版，名為*The
Secret Man*。

除了给报纸供稿外，伍德沃德也先后独立撰写和与人合作出版过十几部[政治题材的著作](../Page/政治.md "wikilink")。

## 相關影視形象

  - 1976年電影《[驚天大陰謀](../Page/驚天大陰謀.md "wikilink")》，由[羅拔·烈福飾演鲍勃](../Page/羅拔·烈福.md "wikilink")·伍德沃德。
  - 2017年電影《[推倒白宮的男人](../Page/推倒白宮的男人.md "wikilink")》，由[儒利安·莫里斯飾演鲍勃](../Page/儒利安·莫里斯.md "wikilink")·伍德沃德。

## 著作

  - 《[總統的人馬](../Page/總統的人馬.md "wikilink")》（*All the President's
    Men*，1974）
  - 《[最後的日子](../Page/最後的日子.md "wikilink")》（*The Final Days*，1976）
  - 《[法官们](../Page/法官们.md "wikilink")》（*The Brethren*，1979）
  - 《[窃听](../Page/窃听_\(书本\).md "wikilink")》（*Wired*，1984）
  - 《[帷幕](../Page/帷幕.md "wikilink")》（*Veil*，1987）
  - 《[指挥官](../Page/指挥官.md "wikilink")》（*The Commanders*，1991）
  - 《[议程](../Page/议程.md "wikilink")》（*The Agenda*，1994）
  - 《[选择](../Page/选择_\(书本\).md "wikilink")》（*The Choice*，1996）
  - 《[陰影](../Page/陰影_\(書本\).md "wikilink")》（*Shadow*，1999）
  - *Maestro*, 2000, ISBN 0743204123
    －有關[艾伦·格林斯潘](../Page/艾伦·格林斯潘.md "wikilink")
  - 《[战争中的布什](../Page/战争中的布什.md "wikilink")》（*Bush at War*，2002）
  - 《[进攻计划](../Page/进攻计划.md "wikilink")》（*Plan of Attack*，2004）
  - *The Secret Man*, 2005
  - *State of Denial: Bush at War, Part III*, 2006
  - *The War Within: A Secret White House History (2006–2008)*, 2008,
    ISBN 1-4165-5897-7

## 外部連結

  -
  - [NNDB](http://www.nndb.com/people/316/000022250)

  - [The Clash of Paradigms Articles About
    Ombudsmanship](https://web.archive.org/web/20091009095016/http://www.newsombudsmen.org/elliott.html)

  - 《[華盛頓郵報](../Page/華盛頓郵報.md "wikilink")》[生平](http://www.washingtonpost.com/wp-dyn/content/linkset/2006/07/18/LI2006071800574.html)

  - 《[GQ](../Page/GQ.md "wikilink")》[訪問](https://web.archive.org/web/20090710152331/http://men.style.com/gq/features/landing?id=content_5039)

  - [Ubben Lecture at DePauw
    University](http://www.depauw.edu/news/index.asp?id=13937)

  - [Biography of Bob Woodward and Carl
    Bernstein](https://web.archive.org/web/20140105045734/http://woodwardandbernstein.net/)

  - [Woodward and Bernstein papers at the Harry Ransom
    Center](http://www.hrc.utexas.edu/research/fa/woodstein.folderlist.html/)

  - [Woodward and Bernstein online exhibition at the Harry Ransom
    Center](http://www.hrc.utexas.edu/exhibitions/web/woodstein/)

[W](../Page/category:普利茲獎獲獎者.md "wikilink")

[W](../Category/美国记者.md "wikilink")
[W](../Category/耶鲁大学校友.md "wikilink")
[W](../Category/伊利諾州人.md "wikilink")
[Category:水門事件](../Category/水門事件.md "wikilink")
[W](../Category/华盛顿邮报人物.md "wikilink")