**留守**是古代[漢字文化圈國家的官職之一](../Page/漢字文化圈.md "wikilink")，本指「留置駐守」之意。[中国](../Page/中国.md "wikilink")[隋代以後](../Page/隋代.md "wikilink")，臨時駐守[京師](../Page/京師.md "wikilink")、[陪都或險要地區之職](../Page/陪都.md "wikilink")，以其義稱「留守」，其業務即以[軍事](../Page/軍事.md "wikilink")[武力鎮守](../Page/武力.md "wikilink")，並被賦予臨時的行政權責。

## 中國

[中國歷史上](../Page/中國歷史.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")、[諸侯或](../Page/諸侯.md "wikilink")[君主出外時](../Page/君主.md "wikilink")，常以[太子](../Page/太子.md "wikilink")、[親王或](../Page/親王.md "wikilink")[大臣](../Page/大臣.md "wikilink")、[官員等留守](../Page/官員.md "wikilink")[京師](../Page/京師.md "wikilink")，或稱留守、留台、居守，無固定名稱。

[隋煬帝時](../Page/隋煬帝.md "wikilink")，始設置「留守」、「副留守」，遂成為官名。

[唐高祖李淵曾任](../Page/唐高祖.md "wikilink")[太原留守](../Page/太原.md "wikilink")。[唐太宗征討高句麗時](../Page/唐太宗征討高句麗.md "wikilink")，[李世民以](../Page/李世民.md "wikilink")[房玄齡為京城留守](../Page/房玄齡.md "wikilink")；[武則天時常設](../Page/武則天.md "wikilink")[西京](../Page/西京.md "wikilink")[長安留守](../Page/長安.md "wikilink")，[唐玄宗時常設東都留守](../Page/唐玄宗.md "wikilink")，[開元十一年](../Page/開元.md "wikilink")（723年）以[太原為北都](../Page/太原.md "wikilink")，合[東京](../Page/東京.md "wikilink")[洛陽](../Page/洛陽.md "wikilink")、[西京](../Page/西京.md "wikilink")[長安共稱為三都留守](../Page/長安.md "wikilink")。

[北宋時](../Page/北宋.md "wikilink")，留守與[樞密使等同](../Page/樞密使.md "wikilink")，稱「使相」。《[宋史](../Page/宋史.md "wikilink")》記載：「[親王](../Page/親王.md "wikilink")、[樞密使](../Page/樞密使.md "wikilink")、[留守](../Page/留守.md "wikilink")、[節度使兼](../Page/節度使.md "wikilink")[侍中](../Page/侍中.md "wikilink")、[中書令](../Page/中書令.md "wikilink")、[同平章事者](../Page/同平章事.md "wikilink")，皆謂之使相」。《[水滸傳](../Page/水滸傳.md "wikilink")》說“上馬管軍，下馬管民”，可謂集民政和軍事大權為一體。北宋[宗澤曾是東京](../Page/宗澤.md "wikilink")[汴梁留守](../Page/汴梁.md "wikilink")。

[南宋](../Page/南宋.md "wikilink")[建炎三年](../Page/建炎.md "wikilink")，[兀朮率軍南下](../Page/兀朮.md "wikilink")，[建康](../Page/建康.md "wikilink")[行轅留守](../Page/行轅.md "wikilink")[杜充竟不戰而降](../Page/杜充.md "wikilink")。

[清代](../Page/清代.md "wikilink")「盛京將軍」鎮守[滿族發跡地](../Page/滿族.md "wikilink")[奉天](../Page/奉天.md "wikilink")，相當於[留守一職](../Page/留守.md "wikilink")。

循古，[民國初年有設留守一職](../Page/民國.md "wikilink")，不久即廢。

## 日本

日本古代在定都[平安京前](../Page/平安京.md "wikilink")，曾實施過[複都制](../Page/複都制.md "wikilink")，在天皇駐錫以外之都城設置「留守官」（；又稱為「留守司」）。1869年（[明治](../Page/明治.md "wikilink")2年）3月，[明治天皇二度從](../Page/明治天皇.md "wikilink")[京都行幸至](../Page/京都.md "wikilink")[東京](../Page/東京.md "wikilink")，實際上是將[首都遷往東京](../Page/日本首都.md "wikilink")（雅稱為「[東京奠都](../Page/東京奠都.md "wikilink")」）；為了安撫多居住在京都的[公家階層](../Page/公家_\(日本\).md "wikilink")、以及京都居民的不滿，[明治新政府在同時在京都設置](../Page/明治新政府.md "wikilink")「留守官」，等同[太政官在京都的分身](../Page/太政官.md "wikilink")。但1871年（明治4年）3月發生[公卿謀反的](../Page/公卿.md "wikilink")「」後，公家階層對遷都東京的反對聲浪逐漸降低，明治新政府遂於同年8月23日廢止京都的留守官職位，至此首都機能完全轉移至東京。

## 相關條目

  - [留後](../Page/留後.md "wikilink")

[留守](../Category/留守.md "wikilink")
[Category:中国古代地方官制](../Category/中国古代地方官制.md "wikilink")
[Category:日本古代官制](../Category/日本古代官制.md "wikilink")