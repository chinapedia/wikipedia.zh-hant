**义乌市**位于[中国](../Page/中国.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[金华市中部](../Page/金华市.md "wikilink")。义乌为[浙江省综合实力第三大](../Page/浙江省.md "wikilink")[县市](../Page/县级行政区.md "wikilink")，也是中国经济发达县市之一，其综合竞争力[2010年位居浙江省第三位，全国百强县（市）第八位](../Page/中国百强县#县域经济论坛评价.md "wikilink")。2005年，[联合国与](../Page/联合国.md "wikilink")[世界银行](../Page/世界银行.md "wikilink")、[摩根士丹利公司等多家世界权威机构联合发布](../Page/摩根士丹利.md "wikilink")《震惊世界的中国数字》报告中，[义乌市场被称为](../Page/义乌国际商贸城.md "wikilink")“全球最大的小商品批发市场”。[义乌小商品博览会已成为](../Page/义乌小商品博览会.md "wikilink")[广交会](../Page/广交会.md "wikilink")、[华交会之后国内第三大贸易类展会](../Page/中国华东进出口商品交易会.md "wikilink")。2010年，义乌被设为国际贸易方面的[国家综合配套改革试验区](../Page/国家综合配套改革试验区.md "wikilink")。\[1\]

## 历史沿革

  - [春秋时属](../Page/春秋.md "wikilink")[越国](../Page/越国.md "wikilink")。前621年，越侯[无壬把都城迁到](../Page/无壬.md "wikilink")[嶕岘](../Page/嶕岘.md "wikilink")，据考其位于义乌稠城。越侯[允常在前](../Page/允常.md "wikilink")538年迁出到[勾嵊](../Page/勾嵊.md "wikilink")。
  - [秦王](../Page/秦.md "wikilink")[嬴政](../Page/嬴政.md "wikilink")（秦始皇）二十五年（前222年）建县名乌伤，属[会稽郡](../Page/会稽.md "wikilink")。传说秦时有个颜乌，事亲至孝，父死后负土筑坟，一群乌鸦衔土相助，结果乌鸦嘴喙皆伤，故称乌伤县。[新莽时](../Page/新莽.md "wikilink")（9年）改县名乌孝。
  - [东汉](../Page/东汉.md "wikilink")[建武初年复称乌伤](../Page/建武.md "wikilink")。曾为[会稽西部都尉治](../Page/会稽.md "wikilink")。初平三年（192年）分割西部辖境，设置长山县（即后之金华县）。
  - [三国](../Page/三国.md "wikilink")[吴](../Page/吴.md "wikilink")[赤乌八年](../Page/赤乌.md "wikilink")（245年）分南境，置永康县。宝鼎元年（266年），分会稽郡西部设东阳郡（郡治长山），乌伤县属东阳郡。
  - [隋](../Page/隋.md "wikilink")[开皇九年](../Page/开皇.md "wikilink")（589年），分割吴州置[婺州](../Page/婺州.md "wikilink")。
  - [唐](../Page/唐.md "wikilink")[武德四年](../Page/武德.md "wikilink")（621年）于乌伤县置稠州，并分置乌孝、华川二县。[武德七年](../Page/武德.md "wikilink")（624年）废稠州，合乌孝、华川为一县，改名义乌县。稠州以稠山（德胜岩）而得名。华川又名绣川，以[绣湖得名](../Page/绣湖.md "wikilink")。义乌其义与乌伤、乌孝同。唐[垂拱二年](../Page/垂拱.md "wikilink")（686年）析义乌县东境设东阳县。[天宝十三年](../Page/天宝.md "wikilink")（754年），又分县境北部及兰溪、富阳各一部分，设浦阳县（今浦江县）。
  - [元代](../Page/元.md "wikilink")，义乌隶属婺州路总管府。[至正十八年](../Page/至正.md "wikilink")（1358年），吳王[朱元璋部攻取婺州](../Page/朱元璋.md "wikilink")，改婺州路为宁越府。[至正二十二年](../Page/至正.md "wikilink")（1362年）又改名金华府。
  - [明](../Page/明.md "wikilink")[嘉靖三十八年](../Page/嘉靖.md "wikilink")（1559年）[戚继光的](../Page/戚继光.md "wikilink")[戚家军成军于义乌](../Page/戚家军.md "wikilink")、[永康一帶](../Page/永康市.md "wikilink")，总兵力四千人，主力是义乌、永康的农民和矿工。自成军起，大小数百战未尝败绩。
  - 明清仍旧，义乌隶属关系未变。
  - [辛亥革命后](../Page/辛亥革命.md "wikilink")，废府制代以道制，义乌属[金华道](../Page/金华道.md "wikilink")。1927年废道制改为[省县两级制](../Page/省县两级制.md "wikilink")，义乌直属[浙江省](../Page/浙江.md "wikilink")。后设行政督察专员公署，义乌属金华专区或浙江省第四专区。
  - 1949年5月8日义乌[解放](../Page/解放.md "wikilink")。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，义乌属金华专区。
  - 1959年[浦江县并入义乌](../Page/浦江县.md "wikilink")，1967年仍析出。
  - 1988年撤销义乌县，设立义乌市（县级）。\[2\]

## 行政区划

义乌市辖8个[街道](../Page/街道_\(行政區劃\).md "wikilink")、6个[镇](../Page/行政建制鎮.md "wikilink")

  - [稠城街道](../Page/稠城街道.md "wikilink")、[北苑街道](../Page/北苑街道_\(義烏市\).md "wikilink")、[稠江街道](../Page/稠江街道.md "wikilink")、[江东街道](../Page/江東街道_\(義烏市\).md "wikilink")、[后宅街道](../Page/后宅街道.md "wikilink")、[城西街道](../Page/城西街道_\(義烏市\).md "wikilink")、[廿三里街道](../Page/廿三里街道.md "wikilink")、[福田街道](../Page/福田街道（義烏市）.md "wikilink")
  - [上溪镇](../Page/上溪镇.md "wikilink")、[义亭镇](../Page/义亭镇.md "wikilink")、[佛堂镇](../Page/佛堂镇.md "wikilink")、[赤岸镇](../Page/赤岸鎮_\(義烏市\).md "wikilink")、[苏溪镇](../Page/蘇溪鎮_\(義烏市\).md "wikilink")、[大陈镇](../Page/大陳鎮_\(義烏市\).md "wikilink")

## 地理

义乌地处[金衢盆地的东侧边缘](../Page/金衢盆地.md "wikilink")，境内主要河流有[义乌江](../Page/义乌江.md "wikilink")。

## 交通

[缩略图](https://zh.wikipedia.org/wiki/File:義烏機場.jpg "fig:缩略图")\]\]
重要公路有[320国道](../Page/320国道.md "wikilink")、[沪昆高速公路](../Page/沪昆高速公路.md "wikilink")。

过境铁路有[沪昆铁路](../Page/沪昆铁路.md "wikilink")、[沪昆客运专线](../Page/沪昆客运专线.md "wikilink")，在城北设有[义乌站](../Page/义乌站.md "wikilink")，另有规划中[甬金铁路](../Page/甬金铁路.md "wikilink")（宁波-金华）过境。2014年12月9日，第一趟连接义乌和欧洲的“义新欧”货运列车抵达[西班牙](../Page/西班牙.md "wikilink")[马德里](../Page/马德里.md "wikilink")。义乌至欧洲的货运铁路服务宣告贯通，不仅让沿线的12个国家成为[新丝绸之路经济带建设的参与者和受益者](../Page/新丝绸之路经济带.md "wikilink")，也将带来大量的贸易机遇。\[3\]

城北有[义乌机场](../Page/义乌机场.md "wikilink")，是浙中地区唯一的航空港。

## 人口

义乌市2010年[第六次全国人口普查全市常住人口为](../Page/第六次全国人口普查.md "wikilink")123.40万人，较2000年[第五次全国人口普查的](../Page/第五次全国人口普查.md "wikilink")91.27万人增加32.13万人。全市常住人口中市外流入人口为58.58万人，占47.47%。全市常住人口中，男性人口为64.71万人，占52.44%。女性人口为58.69万人，占47.56%。总人口性别比为110.25。\[4\]

2014年末全市户籍人口766604人。\[5\]

## 语言

义乌市通用[方言为](../Page/方言.md "wikilink")[义乌话](../Page/义乌话.md "wikilink")，属于[吴语](../Page/吴语.md "wikilink")[金衢片](../Page/金衢片.md "wikilink")。由于义乌交通方便，与外地交往频繁，因此义乌话受邻近方言的影响较大，内部差异也较明显，故有“义乌十八腔，隔溪不一样”的说法。\[6\]

## 经济

[缩略图](https://zh.wikipedia.org/wiki/File:Yiwu_futian_market.jpg "fig:缩略图")
义乌在2008年中国综合实力[百强县排名第八](../Page/百强县.md "wikilink")，2008年义乌城镇居民人居收入达到全国县级市第一。义乌城市规模与经济实力等多方面均已超过了金华市区，而且和周边的[东阳](../Page/东阳.md "wikilink")、[永康等县市联系密切](../Page/永康市.md "wikilink")。2010年，义乌被设为国际贸易方面的国家综合配套改革试验区。成立于1992年的[义乌经济技术开发区在](../Page/义乌经济技术开发区.md "wikilink")2012年3月成为[国家级经济技术开发区](../Page/国家级经济技术开发区.md "wikilink")。2014年全市完成地区生产总值968.6亿元，人均生产总值达126907元。\[7\]

### 农业

义乌农业发达，主产[水稻](../Page/水稻.md "wikilink")、[甘蔗等](../Page/甘蔗.md "wikilink")，特产主要有[南枣](../Page/南枣.md "wikilink")、[蜜枣](../Page/蜜枣.md "wikilink")、[红糖](../Page/红糖.md "wikilink")、[火腿](../Page/火腿.md "wikilink")、[豆腐皮](../Page/豆腐皮.md "wikilink")、[腐竹](../Page/腐竹.md "wikilink")、[白字酒](../Page/白字酒.md "wikilink")、[顶陈酒](../Page/顶陈酒.md "wikilink")、[山花梨](../Page/山花梨.md "wikilink")，也是[金华火腿主要产地之一](../Page/金华火腿.md "wikilink")。

### 工业

依托于小商品市场的发展，义乌建立起了较多的制造业企业，所生产的产品大都通过小商品市场销往中国国内和海外，实现了“贸工联动”。2014年全市实现规模以上工业总产值847.1亿元，销售产值805.1亿元。\[8\]

### 第三产业

义乌拥有闻名遐迩的小商品市场，目前已成为世界上最大的小商品集散地，其市场的规模和成交量均居中国各专业市场之首。\[9\]

## 教育

全市常住人口中，具有大学（指大专以上）程度的人口为8.09万人，约占全部常住人口的6.5%。\[10\]

2014年全市有1所高职院校，1所技师学院，中小学114所(含2所市属职业高中)，幼儿园355所。共有在校学生23.1万人,其中高中（职）2.6万人，义务教育段学生12.4万人（小学9.5万；初中2.9万；含外来建设者子女5.3万人，占42.7%），在园幼儿7.1万人。\[11\]

  - 高等教育：[义乌工商职业技术学院](../Page/义乌工商职业技术学院.md "wikilink")、[浙江广播电视大学义乌学院](../Page/浙江广播电视大学.md "wikilink")
  - 高级中学：[义乌中学](../Page/义乌中学.md "wikilink")、[义乌市第二中学](../Page/义乌市第二中学.md "wikilink")、[义乌市第三中学](../Page/义乌市第三中学.md "wikilink")、[义乌市第四中学](../Page/义乌市第四中学.md "wikilink")
  - 中等职业教育：义乌城镇职业技术学校、义乌市国际商贸学校，浙江省机电技师学院

## 旅游景点

[缩略图](https://zh.wikipedia.org/wiki/File:Guyue_Bridge_\(Yiwu\),_Song_Dynasty,_China.jpg "fig:缩略图")，宋嘉定癸酉（公元1213年）建造。全國重點文物。\]\]

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[古月桥](../Page/古月桥.md "wikilink")、[黄山八面厅](../Page/黄山八面厅.md "wikilink")
  - [浙江省文物保护单位](../Page/浙江省文物保护单位.md "wikilink")：[双林铁塔](../Page/双林铁塔.md "wikilink")、[朱丹溪墓](../Page/朱丹溪墓.md "wikilink")、[螃蟹形山墓群](../Page/螃蟹形山墓群.md "wikilink")、[冯雪峰故居](../Page/冯雪峰故居.md "wikilink")、[吴晗故居](../Page/吴晗故居.md "wikilink")、[大安寺塔](../Page/大安寺塔.md "wikilink")、[朱店朱宅](../Page/朱店朱宅.md "wikilink")、[雅端容安堂](../Page/雅端容安堂.md "wikilink")、[塘下方大宗祠](../Page/塘下方大宗祠.md "wikilink")、[陈望道故居](../Page/陈望道故居.md "wikilink")、[佛堂吴宅](../Page/佛堂吴宅.md "wikilink")
  - [中国历史文化名镇](../Page/中国历史文化名镇.md "wikilink")：[佛堂镇](../Page/佛堂镇.md "wikilink")
  - [义乌国际商贸城被](../Page/义乌国际商贸城.md "wikilink")[国家旅游局授予中国首个AAAA级购物旅游区荣誉称号](../Page/国家旅游局.md "wikilink")。“小商品海洋，购物者天堂”已成为义乌市的代名词。

## 名人

  - 南北朝佛学大家[傅大士](../Page/傅大士.md "wikilink")
  - 唐代诗人[骆宾王](../Page/骆宾王.md "wikilink")
  - 宋代名将[宗泽](../Page/宗泽.md "wikilink")
  - 元代名医[朱丹溪](../Page/朱丹溪.md "wikilink")
  - 近代[法學家](../Page/法學家.md "wikilink")[朱獻文](../Page/朱獻文.md "wikilink")
  - 近代[教育家](../Page/教育家.md "wikilink")[陈望道](../Page/陈望道.md "wikilink")
  - 近代[历史学家](../Page/历史学家.md "wikilink")[吴晗](../Page/吴晗.md "wikilink")
  - 中国科学院院士[朱位秋](../Page/朱位秋.md "wikilink")
  - 民义学堂的创办者[吴源](../Page/吴源.md "wikilink")
  - [湖畔诗人](../Page/湖畔诗人.md "wikilink")[冯雪峰](../Page/冯雪峰.md "wikilink")
  - IT业知名人士[方兴东](../Page/方兴东.md "wikilink")
  - 中华人民共和国财政部长[楼继伟](../Page/楼继伟.md "wikilink")
  - [细菌战受害者诉讼原告团团长](../Page/细菌战.md "wikilink")[王选](../Page/王选_\(女\).md "wikilink")
  - [中国中央电视台主持人季小军](../Page/中国中央电视台.md "wikilink")
  - [中华民国立法委員](../Page/中华民国.md "wikilink")[丁守中](../Page/丁守中.md "wikilink")
  - 台湾艺人[王力宏](../Page/王力宏.md "wikilink")

## 友好城市（城区、县）

  - [韩国](../Page/韩国.md "wikilink")[首尔](../Page/首尔.md "wikilink")[中区](../Page/中區_\(首爾\).md "wikilink")
  - [上海市](../Page/上海市.md "wikilink")[杨浦区](../Page/杨浦区.md "wikilink")
  - [海南省](../Page/海南省.md "wikilink")[三亚市](../Page/三亚市.md "wikilink")
  - [江苏省](../Page/江苏省.md "wikilink")[沭阳县](../Page/沭阳县.md "wikilink")
  - [黑龙江](../Page/黑龙江.md "wikilink")[绥芬河市](../Page/绥芬河市.md "wikilink")
  - [廣東省](../Page/廣東省.md "wikilink")[普寧市](../Page/普寧市.md "wikilink")

## 参考文献

## 外部链接

  - [中国·义乌市政府官网](http://www.yw.gov.cn/)
  - [Google地图 - 义乌市](https://www.google.com/maps/place/Yiwu)

{{-}}

[金华](../Page/category:浙江省县级市.md "wikilink")
[浙](../Page/category:中国中等城市.md "wikilink")

[义乌市](../Category/义乌市.md "wikilink")
[市](../Category/金华区县市.md "wikilink")
[Category:中国国家级生态市区县](../Category/中国国家级生态市区县.md "wikilink")
[浙](../Category/国家卫生城市.md "wikilink")
[Y](../Category/中国的世界之最.md "wikilink")

1.  [义乌获批国际贸易综合改革试点成为我国第十个经济新特区](http://news.sina.com.cn/c/2011-03-10/054322085145.shtml)

2.  [义乌历史沿革](http://www.yw.gov.cn/glb/ywgl/lsyg/200901/t20090107_166758.html)


3.  [1.3万公里横跨欧亚
    首趟"义新欧"班列抵达马德里](http://www.guancha.cn/Project/2014_12_10_303024.shtml)

4.  [义乌市2010年第六次全国人口普查主要数据公报](http://www.yw.gov.cn/zfmhwzxxgk/001/10/02/201106/P020130320755503902681.doc)


5.
6.

7.
8.
9.

10.
11.