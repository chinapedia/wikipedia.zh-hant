**温宿县**（）是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[阿克苏地区所辖的一个](../Page/阿克苏地区.md "wikilink")[县](../Page/县.md "wikilink")。总面积为14309平方公里，2014年人口为23万。

## 历史

地处新疆维吾尔自治区西部，与[哈萨克斯坦接壤处](../Page/哈萨克斯坦.md "wikilink")。古为[姑墨地](../Page/姑墨.md "wikilink")。唐为[姑墨州](../Page/姑墨州.md "wikilink")。清光绪九年(1883)，始置[温宿直隶州](../Page/温宿直隶州.md "wikilink")，属[阿克苏道](../Page/阿克苏道.md "wikilink")[温宿府](../Page/温宿府.md "wikilink")；光绪二十八年(1902)，置温宿县，取古国名为县名。“温宿”系突厥语，意为“白水”。一说为onsu的音译，意为“十水”\[1\]。1958年，并入[阿克苏县](../Page/阿克苏县.md "wikilink")。1962年，恢复\[2\]。

## 行政区划

下辖5个[镇](../Page/行政建制镇.md "wikilink")、4个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")：

。

## 工业

耕地面积82716公顷，境内北部有雪带冰川197条，有国家级保护动物有[野骆驼](../Page/野骆驼.md "wikilink")、[雩豹](../Page/雩豹.md "wikilink")、[中华秋沙鸭](../Page/中华秋沙鸭.md "wikilink")、[黑颈鹤](../Page/黑颈鹤.md "wikilink")、[黑熊](../Page/黑熊.md "wikilink")、[猞猁](../Page/猞猁.md "wikilink")、[马鹿](../Page/马鹿.md "wikilink")、[棕熊等](../Page/棕熊.md "wikilink")。药用植物有[手掌参](../Page/手掌参.md "wikilink")、[党参](../Page/党参.md "wikilink")、[黄芪](../Page/黄芪.md "wikilink")、[甘草](../Page/甘草.md "wikilink")、[麻黄](../Page/麻黄.md "wikilink")、[独活](../Page/独活.md "wikilink")、[当归](../Page/当归.md "wikilink")、[霄莲等](../Page/霄莲.md "wikilink")；矿产资源有煤、石膏、[岩盐](../Page/岩盐.md "wikilink")、[磷](../Page/磷.md "wikilink")、[铁](../Page/铁.md "wikilink")、铀、石墨、粘土、白灰石等。旅游景点有天山神木园和托木尔峰神奇大峡谷；属温带大陆性干旱气候。主要农产品有棉花、油料、甜菜\[3\]。

## 荣誉

  - 全国计划生育先进县
  - 全国体育先进县
  - 全国农村水电初级电气化先进县
  - 全国农牧区教育先进县
  - 中国棉花产量强县市
  - 全国城市环境综合整治优秀县城
  - 自治区双拥模范县
  - “自治区精神文明建设先进县”两连冠
  - 自治区卫生红旗县城
  - 自治区“天山杯”竞赛优秀县城

## 外部链接

  - [温宿县简介](https://web.archive.org/web/20080510222444/http://www.wensu.gov.cn/)

## 参考资料

[温宿县](../Category/温宿县.md "wikilink")
[县](../Category/阿克苏县市.md "wikilink")
[阿克苏](../Category/新疆县份.md "wikilink")

1.
2.
3.