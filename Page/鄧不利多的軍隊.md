**鄧不利多的軍隊**（），-{zh-hans:台湾译名为邓不利多的军队;
zh-hant:大陸譯名為鄧布利多軍;}-，英文简称“”，是小說《[哈利·波特](../Page/哈利·波特.md "wikilink")》里面由[赫敏·格兰杰所发起的](../Page/妙麗·格蘭傑.md "wikilink")[學生社團](../Page/學生社團.md "wikilink")，由[哈利·波特擔任領導者](../Page/哈利波特_\(小說角色\).md "wikilink")。在第七集中，由于哈利、榮恩和妙麗逃亡在外，所以金妮、奈威和露娜成了代理的領導。

這組織原来是為對抗來自[魔法部的](../Page/魔法部.md "wikilink")[多洛雷斯·乌姆里奇而設立](../Page/桃樂絲·恩不里居.md "wikilink")，但由於魔法部害怕[鄧不利多組織軍隊](../Page/阿不思·鄧不利多.md "wikilink")，所以于该社团成立后禁止任何社團的成立，故算是非法社團。社團活動主要是暗中學習[黑魔法防禦術](../Page/黑魔法防禦術.md "wikilink")（由於魔法部禁止實際演練咒語），所以该组织也被称为“**-{zh-hans:防御协会;
zh-hant:防禦聯盟;}-**”（Defense
Association，英文缩写同样是**D.A.**）。由于是非法學會，所以妙麗在金加隆钱币上-{zh-hans:施变化咒;
zh-hant:施展多身咒;}-，大家便用这些假魔法錢幣連絡，並將社團設立在萬應室。

D.A.成員都来自[格兰芬多](../Page/葛來分多.md "wikilink")、[赫奇帕奇和](../Page/赫夫帕夫.md "wikilink")[拉文克劳](../Page/雷文克勞.md "wikilink")，而且大都是哈利的朋友和同學；在第五集结束时成員有29人（西莫中途加入）。哈利的葛來分多同年生全都是D.A.成員，除了丹尼外全是4年級至7年級。然而，西莫才刚加入，學會中就有人（[毛莉·邊坑](../Page/毛莉·邊坑.md "wikilink")，[張秋的朋友](../Page/張秋.md "wikilink")）把他們出賣給恩不里居，所以在學期中期停止了活動，還迫使鄧不利多教授暫時離開學校。

在第六集中，由於哈利發現[马尔福有不尋常行動](../Page/跩哥·馬份.md "wikilink")，並且發現在他的計謀即將得逞時要榮恩和妙麗召集D.A.成員制止他，但只有[金妮·衛斯理](../Page/金妮·衛斯理.md "wikilink")、[纳威·隆巴顿和](../Page/奈威·隆巴頓.md "wikilink")[卢娜·洛夫古德三人即時回應](../Page/露娜·羅古德.md "wikilink")。

但在第七集中，於金妮、奈威和露娜的帶領下，D.A.成員都重新團結起來，更多反食死人的學生們加入。在霍格華茲大戰時，由于事先受到哈利的警报，D.A.及時通知鳳凰會成員一起戰鬥。

## 已知成員名單

  -
    年級以（哈利波特系列第五集）為參考

| 序號 | 中文名                                                                 | 英文名                    | 性別 | 年級  | 學院   | 備註                                                                  |
| -- | ------------------------------------------------------------------- | ---------------------- | -- | --- | ---- | ------------------------------------------------------------------- |
| 1  | [弗雷·衛斯理](../Page/弗雷和喬治·衛斯理.md "wikilink")                           | Fred Weasley           | 男  | 7年级 | 葛來分多 | 在霍格華茲大戰時陣亡                                                          |
| 2  | [喬治·衛斯理](../Page/弗雷和喬治·衛斯理.md "wikilink")                           | George Weasley         | 男  | 7年级 | 葛來分多 |                                                                     |
| 3  | [李·喬丹](../Page/李·喬丹.md "wikilink")                                  | Lee Jordan             | 男  | 7年级 | 葛來分多 |                                                                     |
| 4  | \-{zh-hans:安吉丽娜·约翰逊; zh-hant:莉娜·強生;}-                               | Angelina Johnson       | 女  | 7年级 | 葛來分多 | 魁地奇隊長                                                               |
| 5  | \-{zh-hans:艾丽娅·斯平内特; zh-hant:西亞．史賓特;}-                              | Alicia Spinnet         | 女  | 7年级 | 葛來分多 |                                                                     |
| 6  | \-{zh-hans:凯蒂·贝尔; zh-hant:凱娣·貝爾;}-                                  | Katie Bell             | 女  | 6年级 | 葛來分多 |                                                                     |
| 7  | [哈利·波特](../Page/哈利·波特_\(小说角色\).md "wikilink")                       | Harry Potter           | 男  | 5年级 | 葛來分多 | 魁地奇隊長，創立者兼首任領袖                                                      |
| 8  | [罗恩·韦斯莱](../Page/榮恩·衛斯理.md "wikilink")                              | Ronald Weasley         | 男  | 5年级 | 葛來分多 | 級長，創立者兼副領袖                                                          |
| 9  | [赫敏·格兰杰](../Page/妙麗·格蘭傑.md "wikilink")                              | Hermione Granger       | 女  | 5年级 | 葛來分多 | 級長，發起人兼創立者兼副領袖                                                      |
| 10 | [纳威·隆巴顿](../Page/奈威·隆巴頓.md "wikilink")                              | Neville Longbottom     | 男  | 5年级 | 葛來分多 | 第二任領袖                                                               |
| 11 | [-{zh-hans:迪安·托姆斯; zh-hant:丁·湯瑪斯;}-](../Page/丁·湯瑪斯.md "wikilink")   | Dean Thomas            | 男  | 5年级 | 葛來分多 |                                                                     |
| 12 | [-{zh-hans:帕瓦蒂·佩蒂尔; zh-hant:芭蒂·巴提;}-](../Page/芭蒂·巴提.md "wikilink")  | Parvati Patil          | 女  | 5年级 | 葛來分多 |                                                                     |
| 13 | [-{zh-hans:拉文德·布朗; zh-hant:文妲·布朗;}-](../Page/文妲·布朗.md "wikilink")   | Lavender Brown         | 女  | 5年级 | 葛來分多 |                                                                     |
| 14 | [-{zh-hans:西莫·斐尼甘; zh-hant:西莫·斐尼干;}-](../Page/西莫·斐尼干.md "wikilink") | Seamus Finnigan        | 男  | 5年级 | 葛來分多 | 中途加入                                                                |
| 15 | [金妮·韦斯莱](../Page/金妮·衛斯理.md "wikilink")                              | Ginevra Weasley        | 女  | 4年级 | 葛來分多 | 起名者兼第二任領袖                                                           |
| 16 | \-{zh-hans:科林·克里维; zh-hant:柯林·克利維;}-                                | Colin Creevey          | 男  | 4年级 | 葛來分多 | 在霍格沃茨大战时阵亡                                                          |
| 17 | \-{zh-hans:丹尼斯·克里维; zh-hant:丹尼·克利維;}-                               | Dennis Creevey         | 男  | 2年级 | 葛來分多 | \-{zh-hans:科; zh-hant:柯;}-林的弟弟，與其哥哥一樣崇拜哈利                           |
| 18 | [-{zh-hans:秋·张; zh-hant:張秋;}-](../Page/張秋.md "wikilink")            | Cho Chang              | 女  | 6年级 | 雷文克勞 | 縮寫起名者，（在電影中）在吐真劑驅使下将其出卖给[多洛雷斯·乌姆里奇](../Page/桃樂絲·恩不里居.md "wikilink") |
| 19 | \-{zh-hans:玛丽埃塔·艾克莫; zh-hant:毛莉·邊坑;}-                               | Marietta Edgecombe     | 女  | 6年级 | 雷文克勞 | （在小说中）将其出卖给[多洛雷斯·乌姆里奇](../Page/桃樂絲·恩不里居.md "wikilink")              |
| 20 | \-{zh-hans:帕德玛·佩蒂尔; zh-hant:芭瑪·巴提;}-                                | Padma Patil            | 女  | 5年级 | 雷文克勞 | 級長                                                                  |
| 21 | \-{zh-hans:安东尼·戈德斯坦; zh-hant:安東尼·金坦;}-                              | Anthony Goldstein      | 男  | 5年级 | 雷文克勞 | 級長                                                                  |
| 22 | \-{zh-hans:迈科尔·科纳; zh-hant:麥可·寇那;}-                                 | Michael Corner         | 男  | 5年级 | 雷文克勞 |                                                                     |
| 23 | 泰瑞·布特                                                               | Terry Boot             | 男  | 5年级 | 雷文克勞 |                                                                     |
| 24 | [卢娜·洛夫古德](../Page/露娜·羅古德.md "wikilink")                             | Luna Lovegood          | 女  | 4年级 | 雷文克勞 | 第二任副領袖，金妮好友                                                         |
| 25 | \-{zh-hans:汉娜·艾博; zh-hant:漢娜·艾寶;}-                                  | Hannah Abbott          | 女  | 5年级 | 赫夫帕夫 | 級長                                                                  |
| 26 | \-{zh-hans:苏珊·博恩斯; zh-hant:蘇珊·波恩;}-                                 | Susan Bones            | 女  | 5年级 | 赫夫帕夫 | \-{zh-hans:博恩斯夫人; zh-hant:愛蜜莉‧波恩;}-侄女                               |
| 27 | \-{zh-hans:厄尼·麦克米兰; zh-hant:阿尼·麥米蘭;}-                               | Ernie Macmillan        | 男  | 5年级 | 赫夫帕夫 | 級長                                                                  |
| 28 | \-{zh-hans:贾斯廷·芬列里; zh-hant:賈斯汀·方列里;}-                              | Justin Finch-Fletchley | 男  | 5年级 | 赫夫帕夫 |                                                                     |
| 29 | \-{zh-hans:扎卡拉斯·史密斯; zh-hant:災來耶·史密;}-                              | Zacharias Smith        | 男  | 5年级 | 赫夫帕夫 | 經常對哈利有意見                                                            |

[cs:Albus Brumbál\#Brumbálova
armáda](../Page/cs:Albus_Brumbál#Brumbálova_armáda.md "wikilink")
[fr:Poudlard\#Armée de
Dumbledore](../Page/fr:Poudlard#Armée_de_Dumbledore.md "wikilink")
[he:הארי פוטר - דמויות משנה\#צבא
דמבלדור](../Page/he:הארי_פוטר_-_דמויות_משנה#צבא_דמבלדור.md "wikilink")

[Category:哈利波特](../Category/哈利波特.md "wikilink")
[Category:虛構祕密結社](../Category/虛構祕密結社.md "wikilink")