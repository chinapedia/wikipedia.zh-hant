**白駒榮**（1892年—1974年），原名**陳榮**，號少波，[廣東](../Page/廣東.md "wikilink")[順德](../Page/順德.md "wikilink")[杏坛](../Page/杏坛.md "wikilink")[龍潭村人](../Page/龍潭村.md "wikilink")，\[1\]著名[粵劇小生表演藝術家](../Page/粵劇.md "wikilink")，粵劇[四大天王之一](../Page/四大天王_\(粵劇\).md "wikilink")。\[2\]他是著名花旦[白雪仙之父](../Page/白雪仙.md "wikilink")。白駒榮父親是一位普通粵劇演員。\[3\]白駒榮與[金山炳](../Page/金山炳.md "wikilink")、[靚榮](../Page/靚榮.md "wikilink")、[千里駒](../Page/千里駒.md "wikilink")、[靚次伯等人同是把戲台演唱語言從官話改為粵語的前軀](../Page/靚次伯.md "wikilink")。二十年代初期已有**小生王**之稱的白駒榮發展了「四門頭二黃」（又名和尚腔和參禪腔）、「八字二黃」等的腔調和板式，使唱腔更富特色，對[薛覺先](../Page/薛覺先.md "wikilink")、[馬師曾的演唱方法有較大的影響](../Page/馬師曾.md "wikilink")。\[4\]他更以真嗓(平喉)代替假嗓演唱，音色優美清潤，自創“白派”的演唱方式。

## 演出經歷

1911年(19歲)才在演天台班學戲，拜師男花旦[鄭君可](../Page/鄭君可.md "wikilink")、[吳有山](../Page/吳有山.md "wikilink")。翌年，加入了由吴友山（吳有山？）所組成的[民壽年班](../Page/民壽年.md "wikilink")，擔任第二小生，並且跟從[扎脚文學習](../Page/扎脚文.md "wikilink")，與他合演《仕林祭塔》、《閨留學廣》兩齣戲。不久，提為正印小生，並開始改藝名為白駒榮。演出連台本戲《太平天國》。後來曾先後加入[漢天樂](../Page/漢天樂.md "wikilink")、[華天樂](../Page/華天樂.md "wikilink")、[周豐年](../Page/周豐年.md "wikilink")、[人壽年等戲班](../Page/人壽年.md "wikilink")，後來成為省港名班[國豐年台柱](../Page/國豐年.md "wikilink")，與[小生聰](../Page/小生聰.md "wikilink")、[千里駒齊名](../Page/千里駒.md "wikilink")。

1946年54歲的白駒榮因視神經萎縮雙目失明輟演。1954年白駒榮出任[廣州粵劇工作團團長](../Page/廣州粵劇工作團.md "wikilink")。期間他克服雙目失明的障礙，刻苦鍛煉，依靠聽覺以及與同台演員的密切配合，終於重登舞臺，繼續演出達10年之久。1958年後任[廣東粵劇院藝術總指導](../Page/廣東粵劇院.md "wikilink")，兼任[廣東粵劇學校校長](../Page/廣東粵劇學校.md "wikilink")。

## 八字二黄

「八字二黄」與長句二黃、長句二流等，都從原來的[慢板和](../Page/慢板.md "wikilink")[流水板變化而來的](../Page/流水板.md "wikilink")。粵劇的[二黃腔是由](../Page/二黃腔.md "wikilink")[徽班傳入的](../Page/徽班.md "wikilink")，它的基本板式結構和曲調特徵與鄰省劇種之南路二黃腔比較接近，特別如「慢板」、「首板」、「流水」等保留傳入時的面貌較多。\[5\]

## 代表作

  - 風流天子
  - 再生緣
  - 泣荊花
  - 二堂放子
  - 龜山起禍
  - 選女婿
  - 金生桃盒
  - 客途秋恨
  - 落霞孤鶩
  - 海角尋香

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [香港粵劇面面觀講座-1](https://web.archive.org/web/20080120005216/http://yuensiufai.com/news-talk-1.htm)

[category:粵劇演員](../Page/category:粵劇演員.md "wikilink")
[category:1892年出生](../Page/category:1892年出生.md "wikilink")
[category:1974年逝世](../Page/category:1974年逝世.md "wikilink")
[R榮](../Page/category:陳姓.md "wikilink")

[Category:順德人](../Category/順德人.md "wikilink")

1.  [佛山古鎮文化](http://61.145.69.8:8080/was40/detail?record=5&channelid=21650)
2.  [白駒榮 -
    客途秋恨/男燒衣](http://blog.roodo.com/muzikland/archives/1178429.html)
3.  [客途秋恨](http://www.taiwanaudio.org.tw/main.php?Page=SA2B1&KeyID=158129655465d2ec323bde)
4.
5.