**AMD 580X
CrossFire**（代号為**RD580**）是[ATI的一款](../Page/ATI.md "wikilink")[晶片組](../Page/晶片組.md "wikilink")，支援[AMD平台](../Page/AMD.md "wikilink")。該晶元組前稱**Xpress
3200**，隨著AMD收購ATI，所以被易名580X
CrossFire。它支援[AMD64處理器](../Page/AMD64.md "wikilink")，有兩條全速的PCI-E
16x，可以完全發揮[CrossFire的效能](../Page/CrossFire.md "wikilink")。此外，對手[nVidia的](../Page/nVidia.md "wikilink")[nForce
4](../Page/nForce_4.md "wikilink") SLi X16的兩條全速PCI-E
16x各由南北橋提供，所以雙卡加速效率比單晶片提供的580X
CrossFire差。

580X CrossFire的北橋支援Xpress Route技術。原理是北橋中有專用的通道，供兩條PCI-E
16x互相傳輸數據，提升CrossFire的效率。

南橋方面，ATI推出了SB600與之配合。由於[ULi已被](../Page/ULi.md "wikilink")[nVidia收購](../Page/nVidia.md "wikilink")，迫使ATI須加緊研發自家更好的南橋。最後，SB600的推出，修正了SB450硬碟效能差的問題。

## XPRESS 3200晶元組列表

  - **RD550** - 只支援一條PCI-E 16x和一條PCI-E 8x
  - **RD580** - 580X CrossFire - 能支援真正的雙PCI-E
    16x，可將[CrossFire效能完全發揮](../Page/CrossFire.md "wikilink")。由於是單晶片双PCI-E
    16x，效率比[nVidia](../Page/nVidia.md "wikilink") [NForce4 SLI
    X16的双晶片双PCI](../Page/NForce4_SLI_X16.md "wikilink")-E
    16x高。
  - **RD600** -
    [英特尔處理器版本](../Page/英特尔.md "wikilink")，功能與RD580相同，分別在於北橋提供記憶體控制器，[HyperTransport控制器換成](../Page/HyperTransport.md "wikilink")[前端匯流排](../Page/前端匯流排.md "wikilink")
    (FSB)控制器，與及使用舊式品牌名稱"Radeon Xpress 3200 CrossFire"。它可以支援[Intel Core
    2處理器](../Page/Intel_Core_2.md "wikilink")。

## SB600規格

  - 支援1个PATA通道
  - 支援4个SATA II接口
  - 支援RAID 0,RAID 1,RAID 5和RAID 10
  - 支援10个USB2.0接口
  - 支援[HD Audio](../Page/HD_Audio.md "wikilink")

[Category:冶天科技](../Category/冶天科技.md "wikilink")
[Category:主板](../Category/主板.md "wikilink")
[Category:2006年面世的產品](../Category/2006年面世的產品.md "wikilink")