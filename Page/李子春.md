**李子春**（），蒙古名**吾魯思不花**（），[本贯](../Page/本贯.md "wikilink")[全州](../Page/全州.md "wikilink")，[朝鲜王朝开国之君](../Page/朝鲜王朝.md "wikilink")[李成桂之父](../Page/李成桂.md "wikilink")。

## 生平

[元朝](../Page/元朝.md "wikilink")[斡东千户所](../Page/斡东千户所.md "wikilink")（今[朝鲜](../Page/朝鲜.md "wikilink")[咸镜北道](../Page/咸镜北道.md "wikilink")[恩德郡](../Page/恩德郡.md "wikilink")）千户[李椿次子](../Page/李椿.md "wikilink")，[元顺帝](../Page/元顺帝.md "wikilink")[至正三年](../Page/至正.md "wikilink")（1343年）正月袭职。至正十六年（1356年），[高丽恭愍王遣](../Page/高丽恭愍王.md "wikilink")[枢密院副使](../Page/枢密院.md "wikilink")[柳仁雨攻取](../Page/柳仁雨.md "wikilink")[元](../Page/元.md "wikilink")[双城总管府](../Page/双城总管府.md "wikilink")（今朝鲜[咸镜南道](../Page/咸镜南道.md "wikilink")[金野郡](../Page/金野郡.md "wikilink")）。柳仁雨至安边（今[朝鲜](../Page/朝鲜.md "wikilink")[江原道](../Page/江原道.md "wikilink")[安边郡](../Page/安边郡.md "wikilink")），距双城两百余里，逗留不进。恭愍王闻之，遣兵马判官丁臣桂授李子春试[少府尹](../Page/少府.md "wikilink")，赐[紫金鱼袋](../Page/紫金鱼袋.md "wikilink")，进阶中显大夫。李子春即归附[高丽](../Page/高丽.md "wikilink")，起兵助柳仁雨攻破双城总管府。恭愍王授李子春司仆卿，进阶大中大夫，赐[松都](../Page/松都.md "wikilink")（今朝鲜[黄海北道](../Page/黄海北道.md "wikilink")[开城市](../Page/开城市.md "wikilink")）府第一座。时[倭寇入侵](../Page/倭寇.md "wikilink")，京城戒严，于是以判[军器监事李子春为](../Page/军器监.md "wikilink")[西江兵马使](../Page/西江兵马使.md "wikilink")，进阶通议大夫。后授[千牛卫](../Page/千牛卫.md "wikilink")[上将军](../Page/上将军.md "wikilink")，进阶正顺大夫。至正二十一年（1361年）春，[荣禄大夫判](../Page/荣禄大夫.md "wikilink")[将作监事李子春](../Page/将作监.md "wikilink")，授[朔方道](../Page/朔方道.md "wikilink")[万户兼](../Page/万户.md "wikilink")[兵马使](../Page/兵马使.md "wikilink")。御史台上书反对说：“李子春本东北面人，而又其界千戶也，不可以为兵马使而镇守。”恭愍王不从，设宴践行。李子春出发之后，又升为[户部尚书](../Page/户部尚书.md "wikilink")。一到北方，李子春就遣使上报说：“
本国人入彼土者，皆顺命出來（本国高丽人侨居于中国的，都听从命令回国了）。”四月，李子春病卒，葬于[咸州](../Page/咸州.md "wikilink")（今朝鲜咸镜南道[咸兴市](../Page/咸兴市.md "wikilink")）。恭愍王甚为惋惜，遣使吊祭。[朝鲜太祖元年](../Page/朝鲜太祖.md "wikilink")（1392年）七月，谥为“[桓王](../Page/桓王.md "wikilink")”；八月，定陵号为“[定陵](../Page/定陵.md "wikilink")”。太宗十一年（1411年）四月，定庙号为“[桓祖](../Page/桓祖.md "wikilink")”，改谥“**渊武圣桓大王**”。

## 家庭

### 妻妾

<table>
<thead>
<tr class="header">
<th><p>身分</p></th>
<th><p>稱號</p></th>
<th><p>生卒年</p></th>
<th><p>父母</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>正室</p></td>
<td><p><a href="../Page/懿惠王后.md" title="wikilink">懿惠王后崔氏</a></p></td>
<td><p>？－？</p></td>
<td><p>崔閑奇<br />
李氏</p></td>
<td><p>中國山东半岛登州人。</p></td>
</tr>
<tr class="even">
<td><p>側室</p></td>
<td><p><a href="../Page/定嬪金氏.md" title="wikilink">定嬪金氏</a><br />
（金古音加）</p></td>
<td><p>？－1404年</p></td>
<td><p>？</p></td>
<td><p>生年不詳，本為李家婢女，高麗朝時封貞﻿安宅主，後封定安翁主，太宗四年元月九日逝世。高宗九年改封為定嬪。</p></td>
</tr>
<tr class="odd">
<td><p>側室</p></td>
<td><p>李內隱</p></td>
<td><p>？－？</p></td>
<td><p>？</p></td>
<td><p>李家婢女。</p></td>
</tr>
</tbody>
</table>

### 子女

#### 子

| 序   | 稱號                                 | 姓名  | 生卒年         | 生母     | 備註           |
| --- | ---------------------------------- | --- | ----------- | ------ | ------------ |
| 庶長子 | [完豐大君](../Page/完豐大君.md "wikilink") | 李元桂 | 1330年－？     | 李氏     | 太祖庶兄。        |
| 嫡長子 | [朝鮮太祖](../Page/朝鮮太祖.md "wikilink") | 李成桂 | 1335年－1408年 | 懿惠王后崔氏 | 李氏朝鮮開國君主。    |
| 庶次子 | [義安大君](../Page/義安大君.md "wikilink") | 李和  | 1348年－1408年 | 定嬪金氏   | 贈諡襄昭，配享太祖廟廷。 |

#### 女

| 序   | 稱號                                 | 姓名 | 生卒年 | 生母     | 備註                                                                |
| --- | ---------------------------------- | -- | --- | ------ | ----------------------------------------------------------------- |
| 嫡長女 | [貞和公主](../Page/貞和公主.md "wikilink") |    | ？－？ | 懿惠王后崔氏 | 下嫁龍源府院君[趙仁璧](../Page/趙仁璧.md "wikilink")（？－1393年）。高麗朝時封貞和宅主，後改為公主。 |

## 参考资料

  - 《[朝鲜王朝实录](../Page/朝鲜王朝实录.md "wikilink")》

<references/>

[Ja-chun](../Category/李姓.md "wikilink")
[桓祖](../Category/朝鮮追尊君主.md "wikilink")
[Category:高麗裔元朝人](../Category/高麗裔元朝人.md "wikilink")
[Category:歸化韓國公民](../Category/歸化韓國公民.md "wikilink")