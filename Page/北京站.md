**北京站**，亦可称**北京火車站**或**北京车站**，是[北京局集团辖下的铁路客运](../Page/中国铁路北京局集团有限公司.md "wikilink")[特等站之一](../Page/特等站_\(中国大陆\).md "wikilink")。车站坐落于[北京市](../Page/北京市.md "wikilink")[东城区](../Page/东城区_\(北京市\).md "wikilink")[东二环以内](../Page/北京二环路.md "wikilink")、[东长安街以南](../Page/长安街.md "wikilink")、原北京内城城墙以北、[崇文门与](../Page/崇文门.md "wikilink")[东便门之间](../Page/东便门_\(北京\).md "wikilink")。车站占地面积25万平方米，总建筑面积8万多平方米。车站站台为地上站台，共有8座[站台](../Page/站台.md "wikilink")，站内股道16条。

北京站是[北京铁路枢纽的一个重要部分](../Page/北京铁路枢纽.md "wikilink")，一直以来北京站始发的旅客列车发往全国各地，而[北京西站完工啟用之后分担了大部分](../Page/北京西站.md "wikilink")[京广](../Page/京广铁路.md "wikilink")、[京九](../Page/京九铁路.md "wikilink")、[京原线方向列车的运输任务](../Page/京原铁路.md "wikilink")。目前，北京站主要负责接发经由[京沪线](../Page/京沪铁路.md "wikilink")、[京哈线往](../Page/京哈铁路.md "wikilink")[华东](../Page/华东.md "wikilink")、[东北方向的列车](../Page/中国东北地区.md "wikilink")。另外，北京站也负担接发往[俄罗斯](../Page/俄罗斯.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")、[蒙古](../Page/蒙古国.md "wikilink")[乌兰巴托和](../Page/乌兰巴托.md "wikilink")[朝鲜](../Page/朝鲜民主主义人民共和国.md "wikilink")[平壤的国际联运旅客列车](../Page/平壤.md "wikilink")\[1\]。

## 历史

[Beijingchezhan1959.jpg](https://zh.wikipedia.org/wiki/File:Beijingchezhan1959.jpg "fig:Beijingchezhan1959.jpg")
[Beijing_Railway_Station_1959.jpg](https://zh.wikipedia.org/wiki/File:Beijing_Railway_Station_1959.jpg "fig:Beijing_Railway_Station_1959.jpg")
[Beijing_Railway_Station_1959_(2).jpg](https://zh.wikipedia.org/wiki/File:Beijing_Railway_Station_1959_\(2\).jpg "fig:Beijing_Railway_Station_1959_(2).jpg")
[China_Railways_NY1_0001_in_Beijing.jpg](https://zh.wikipedia.org/wiki/File:China_Railways_NY1_0001_in_Beijing.jpg "fig:China_Railways_NY1_0001_in_Beijing.jpg")
[Beijingrailwaystation_night.jpg](https://zh.wikipedia.org/wiki/File:Beijingrailwaystation_night.jpg "fig:Beijingrailwaystation_night.jpg")

### 规划

为迎接[中华人民共和国建国十周年](../Page/中华人民共和国.md "wikilink")，[中共中央在](../Page/中共中央.md "wikilink")1958年决定在[首都北京规划建设](../Page/首都.md "wikilink")10个大型项目，为“[北京十大建筑](../Page/北京十大建筑.md "wikilink")”，以展示国家焕然一新的面貌，北京火车站是为其中一项。1958年10月下旬，根据1950年代初的北京市政建设规划开始了新车站的设计安排，站场设计由铁道部第三设计院（今铁道第三勘察设计院集团有限公司）负责，站房大楼由著名建筑师[杨廷宝](../Page/杨廷宝.md "wikilink")、[陈登鏊主持](../Page/陈登鏊.md "wikilink")，国家建工部第一建筑设计院和[南京工学院合作承担设计](../Page/南京工学院.md "wikilink")。车站设计进度迅速，同年12月上旬完成全部设计方案，12月10日中央批准设计方案。时任[国务院总理](../Page/国务院总理.md "wikilink")[周恩来也十分关心新北京站的设计建造工作](../Page/周恩来.md "wikilink")，由站名的位置方向，到贵宾室的环境布置，均曾经过问\[2\]。在审定设计方案时，周恩来提出在主楼两翼各增加一座[角楼的建议也被采纳](../Page/角楼.md "wikilink")\[3\]。

### 建设

1959年1月20日，新站建设工程正式动工。施工人员最多时达2万余人，工程投资5782万元[人民币](../Page/人民币.md "wikilink")，[苏联专家也给予了现场技术指导](../Page/苏联.md "wikilink")。在仅仅7个多月以后的1959年9月10日，工程竣工。新的北京站总占地面积达250000平方米，站舍大楼建筑面积46700平方米，广场面积40000平方米。

1959年9月13日到14日，北京站开通运营前夕，[中国国家主席](../Page/中华人民共和国主席.md "wikilink")[刘少奇](../Page/刘少奇.md "wikilink")、[国务院总理](../Page/中华人民共和国国务院总理.md "wikilink")[周恩来](../Page/周恩来.md "wikilink")、[全国人大常委会委员长](../Page/全国人民代表大会常务委员会委员长.md "wikilink")[朱德曾先后到站视察](../Page/朱德.md "wikilink")\[4\]。9月15日零时10分到2时20分，[中共中央主席](../Page/中共中央主席.md "wikilink")[毛泽东在时任北京市市长](../Page/毛泽东.md "wikilink")[彭真](../Page/彭真.md "wikilink")、[铁道部部长](../Page/中华人民共和国铁道部.md "wikilink")[吕正操](../Page/吕正操.md "wikilink")、副部长[武竟天等的陪同下视察了新北京站](../Page/武竟天.md "wikilink")。毛泽东来到售票厅，走到一个售票窗口前并向售票员贾根柱询问了有关售票工作的情况，并向贾拿了一张由北京开往[普兰店的](../Page/普兰店站.md "wikilink")129次列车加快票观看\[5\]。随后，毛泽东应北京站工程总指挥长李岳林的请求，为北京站题写了‘北京站’三个字的站名。当时题字写了两个版本，而现在采用的版本是毛泽东亲自选定的\[6\]。毛泽东视察北京站的两小时后，第一列旅客列车从北京站发出，标志着北京站正式投入使用。同时，原北京站（[前门东火车站](../Page/正陽門東站.md "wikilink")）关闭。1959年10月4日，当时[中共中央书记处总书记](../Page/中国共产党中央书记处总书记.md "wikilink")[邓小平视察新北京站](../Page/邓小平.md "wikilink")\[7\]。

### 建成初期

新北京站建成使用后，运输能力比以前的前门东火车站大大提高，开始担负越来越繁重的运输任务，并一直是北京市内最重要的铁路客运站，也是全中国客流量最大的车站。开行列车对数和客流量逐年增加，1959年开行列车33对；1966年开行列车40对；1978年开行列车61对；1985年开行列车78对；1993年已达到82对。运送旅客人数由1950年代末的每年600多万人、1960年代末的800多万人、1970年代末的1500万人，到[改革开放后猛增到](../Page/改革开放.md "wikilink")3000万人以上\[8\]。为缓解北京站的运输压力，北京市政府和铁道部在1980年代末开始规划建设北京第二个铁路客运站，至1996年[北京西站](../Page/北京西站.md "wikilink")（西客站）建成，为北京站分担了[京广线及同年](../Page/京广铁路.md "wikilink")9月1日新开的[京九线等线路的运输压力](../Page/京九铁路.md "wikilink")，北京站的年客流量才回落至2000万人以下\[9\]。

### 多次改建

1976年以来，北京站开始对客运服务设施进行现代化改造，相继建立电脑制票、电视监控、无线通信、自动广播等系统，并历次进行站台延长施工，站台有效长分别增至497米至603米，以适应列车编组扩大的需要。1988年，由上海电钟厂生产的中国第一块大屏幕显示系统安装在北京站广场\[10\]。

1996年[暑运期间](../Page/暑运.md "wikilink")，北京站更一度试行磁卡[自动售票](../Page/自动售票机.md "wikilink")，但效果并不理想。\[11\]\[12\]

1998年5月至1999年9月，铁道部和北京市实施北京站抗震加固大修改造工程，实现“风格依旧、面貌一新、功能齐全、科技领先”的构想，引进使用中央空调系统、客运引导揭示系统、客运多功能广播系统等功能\[13\]。2003年6月18日，北京站开始进行扩能技术改造，北京站新建了两个站台（七站台、八站台）、三股列车到发线、一个面积20,513平方米、专门存放行李包裹的大型行包库\[14\]，并拆除原有混凝土雨棚，改建為[中国第一个鋼結構無站臺柱大跨度連續拱式雨棚](../Page/中国.md "wikilink")，同时改建原有1至6站台並抬高到1.2米，向西延長兩條到發線，向南延長高架候車通廊等\[15\]\[16\]。2004年7月12日，北京站扩能改造主体工程通過了初步驗收并交付使用\[17\]。

2008年6月，為迎接[北京奧運會的來臨](../Page/2008年夏季奥林匹克运动会.md "wikilink")，北京火车站全面增加双语标志以方便外国游客出行，除了首次掛上英文站名「Beijing
Railway
Station」，同时在出站口、售票厅等引导標誌牌也加上英文對照\[18\]\[19\]。2008年7月改建完成的[北京南站成为京津城际铁路的始发站](../Page/北京南站.md "wikilink")，继续分担了北京站始发城际列车的运输压力。

2005年12月，连接北京站和[北京西站的](../Page/北京西站.md "wikilink")[北京地下直径线动工](../Page/北京地下直径线.md "wikilink")，2013年7月顺利贯通\[20\]。开通运营后，北京站将由[尽头式车站转成](../Page/港灣式月台.md "wikilink")[通过式车站](../Page/通过式车站.md "wikilink")，北京站与北京西站同时具备京哈、京广等多方向的干线旅客列车接发能力。

2012年，“北京站”被列为[北京市东城区普查登记文物](../Page/北京市东城区文物保护单位.md "wikilink")。

## 车站结构

### 站房

北京站是当时[中国大陆规模最大](../Page/中国大陆.md "wikilink")、设备最先进的[铁路车站](../Page/铁路车站.md "wikilink")，也是第一个现代化大型铁路客运站。站房大楼坐南朝北，东西长218米，南北宽88米，总建筑面积8万多平方米。

站房同时具有浓郁的[中华民族传统风格和苏联的](../Page/中华民族.md "wikilink")[斯大林社会主义建筑风格](../Page/斯大林式建筑.md "wikilink")\[21\]。车站中央以高34米的双曲扁壳屋顶大厅和东西两座34米高的[琉璃钟楼为主轴](../Page/琉璃.md "wikilink")，并以[女儿墙连接位于站房东西两侧的两座高](../Page/女儿墙.md "wikilink")34米的[琉璃塔楼侧轴](../Page/琉璃.md "wikilink")，体现其“首都大门”的建设意图\[22\]。车站站房楼顶装设了两座[大理石钟面的四面大钟](../Page/大理石.md "wikilink")，会在每天早上7点到晚上9点的正点时分敲响钟声并播放乐曲“[东方红](../Page/东方红_\(歌曲\).md "wikilink")”，成为北京站的一大特色。

#### 内部

北京站的候车室全部在站场北侧（站房内），且两层均有候车室，12个旅客候车室总面积达14000平方米，可以同时容纳14000名旅客候车。其中车站一层为1-3候车室，二层则有4-7候车室\[23\]。其中第二候车室为软卧候车室，由原售票大厅改建而成\[24\]。除了候车室外，大楼内旅客候车室、母子候车室、电影厅、游艺厅、旅客餐厅、邮局、医务室等设施一应俱全。

| 候车室\[25\] | 方向               | 候车室        | 方向              |
| --------- | ---------------- | ---------- | --------------- |
| 第一软席候车室   | 辽宁、黑龙江           | 第二软席候车室    | 辽宁、黑龙江、吉林、长沙、上海 |
| 第三候车室     | 黑龙江、内蒙古、辽宁、吉林、长沙 | 第四候车室      | 其它及国际列车         |
| 第五候车室     | 其它               | 第六软席和谐号候车室 | 江苏、河北、辽宁、吉林、黑龙江 |
| 第七候车室     | 其它               |            |                 |

站场上方的通廊横跨整个站场，是为中央检票厅（又被称为8号候车室），连接2-14站台。中央检票厅除了为部分列车进行正常的检票工作之外，还会在每班列车开车前20分钟开放给晚到的旅客作为应急进站通道\[26\]。中央检票厅内不设有候车坐席，但设有部分店铺\[27\]。中央检票厅的穹顶于2007年9月新增五幅巨大的展示五大洲不同地域风情、建筑文化和体育健儿奔向北京、心向[奥运的壁画](../Page/2008年夏季奥林匹克运动会.md "wikilink")\[28\]。8号候车室末端设有一个收费休息室\[29\]。

北京站内的设备也相当先进，进站大厅内安装了4台[自动扶梯](../Page/自动扶梯.md "wikilink")（AC2-59型），由[上海交通大学和](../Page/上海交通大学.md "wikilink")[上海电梯厂联合设计生产](../Page/上海电梯厂.md "wikilink")，是中国自行研制的第一批自动扶梯\[30\]。这4台扶梯先后在1980年代和1990年代两次更换，第一代的电梯已经被上海电梯厂收藏，最近的一次的更换是在2005年初，现在使用的电梯是[奥的斯产品](../Page/奧的斯電梯公司.md "wikilink")\[31\]。另一方面，北京火车站也在旅客候车室、贵宾室等地方安装了当时少见的[空调设备](../Page/空调.md "wikilink")。

### 站场

北京站的站场位于站房南侧，南临[北京明城墙遗址公园](../Page/北京明城墙遗址公园.md "wikilink")，设16条股道和8个站台。其中包括一座侧式站台和五座岛式站台，编号1-14。站台和站房间有两座天桥、两座地下通道和中央检票通廊连接。原先北京站的全部站台为[港灣式月台](../Page/港灣式月台.md "wikilink")，在建设[北京地下直径线时曾考虑将所有站台改为通过式站台](../Page/北京地下直径线.md "wikilink")。但由于车站西侧为文保单位[丁香小学教学楼](../Page/丁香小学.md "wikilink")，铁路部门经过与学校协商后决定仅将7-14站台（8-16道）接入直径线，1-6站台继续保留为港灣式月台\[32\]。

北京站站台为全[中国铁路系统内第一个设有无站台柱雨棚的车站](../Page/中国铁路.md "wikilink")，被当时的[铁道部领导称为铁路跨越式发展的](../Page/中华人民共和国铁道部.md "wikilink")“开篇之作”。北京站的无站台柱雨棚覆盖面积81,887平方米，柱距大约33至34米，最高高度为13米。雨棚以横跨站场、长170米的桁架为基础，通过以悬挑部分、中间下挂部分、中间上部3种形式的檩条连接波浪形的钢板，达至站台视觉通透的效果，实现包括基本站台在内的所有站台无柱\[33\]。

北京站为有需要的送站人士发放爱心卡。站台票于进站口处1至4号验证验票口出售，爱心卡上印有根据需要接送旅客的乘车信息而生成的[二维码](../Page/二维码.md "wikilink")\[34\]。

## 使用情况

### 始发列车

#### 动车组列车

北京站始发的动车组列车均为往河北东北部、东北地区方向的列车。2017年12月31日开通的[北京市郊铁路城市副中心线](../Page/北京市郊铁路城市副中心线.md "wikilink")，北京站亦办理相关列车办客业务\[35\]。2018年7月1日起，原运行于至的[D321/322次列车改在北京站始发终到](../Page/D321/322次列车.md "wikilink")\[36\]。2019年1月5日起，北京站开行由[复兴号CR200J型电力动车组担当的动卧列车](../Page/复兴号CR200J型电力动车组.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>列车所属路局</p></th>
<th><p>车次</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center></td>
<td><p><a href="../Page/京沈动车组列车.md" title="wikilink">D15</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/京沈动车组列车.md" title="wikilink">D17</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>D25</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>D41、D45</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/D335/336次列车.md" title="wikilink">D335</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/京沪卧铺动车组列车.md" title="wikilink">D701、D705</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>D717</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>D6611、D6615</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>D6619</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>S101</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p><a href="../Page/京哈动车组列车.md" title="wikilink">D27</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>D29</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>D711</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>D6635</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p><a href="../Page/京沈动车组列车.md" title="wikilink">D1、D13</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/京沈动车组列车.md" title="wikilink">D5、D7、D9、D11、D51</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/京长动车组列车.md" title="wikilink">D19、D23</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>D21</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>D33</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>D37</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>D73</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/京哈动车组列车.md" title="wikilink">D101</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 普速列车

北京站的始发列车主要是[京沪](../Page/京沪铁路.md "wikilink")、[京哈](../Page/京哈铁路.md "wikilink")、[京承线方向的列车](../Page/京承铁路.md "wikilink")，也有一部分[丰沙](../Page/丰沙铁路.md "wikilink")、[京原线方向的列车](../Page/京原铁路.md "wikilink")。开往东北方向的列车基本在该站始发，而开往华东地区的列车大部分亦在该站始发；同时也始发少数经由[京广铁路](../Page/京广铁路.md "wikilink")、[京九铁路运行的列车](../Page/京九铁路.md "wikilink")。此外，[K3/4次列车](../Page/K3/4次列车.md "wikilink")、[K19/20次列车](../Page/K19/20次列车.md "wikilink")、[K27/28次列车等须在边境车站出境的国际列车也在北京站始发](../Page/K27/28次列车.md "wikilink")\[37\]。

<table>
<thead>
<tr class="header">
<th><p>列车所属路局</p></th>
<th><p>车次</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Z71/72次列车.md" title="wikilink">Z71</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Z83/84次列车.md" title="wikilink">Z83</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Z139/140次列车.md" title="wikilink">Z139</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Z159/160次列车.md" title="wikilink">Z159</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/T109/110次列车.md" title="wikilink">T109</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/T215/218、T217/216次列车.md" title="wikilink">T215</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>T5685、T5687</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/K27/28次列车.md" title="wikilink">K27</a></p></td>
<td><p>、</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K101/102次列车.md" title="wikilink">K101</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K411</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>K473</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K5215</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>K7707</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K7711</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Y509</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Y535、Y537</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K3/4次列车.md" title="wikilink">K3</a>（周三开行）</p></td>
<td><p>、<a href="../Page/雅羅斯拉夫利站.md" title="wikilink">莫斯科-雅罗斯拉夫尔</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/K23/24次列车.md" title="wikilink">K23</a>（周二开行）</p></td>
<td><p>、</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K19/20次列车.md" title="wikilink">K19</a>（周六开行）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p><a href="../Page/雅羅斯拉夫利站.md" title="wikilink">莫斯科-雅罗斯拉夫尔</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>K967</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Z15/16次列车.md" title="wikilink">Z15</a>、<a href="../Page/Z17/18次列车.md" title="wikilink">Z17</a>、Z203</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Z17/18次列车.md" title="wikilink">Z601</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/T17/18次列车.md" title="wikilink">T17</a>、<a href="../Page/T297/298次列车.md" title="wikilink">T297</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>经由<a href="../Page/京沪铁路.md" title="wikilink">京沪</a>、<a href="../Page/津山铁路.md" title="wikilink">津山</a>、<a href="../Page/沈山铁路.md" title="wikilink">沈山</a>、<a href="../Page/大郑铁路.md" title="wikilink">大郑</a>、<a href="../Page/通让铁路.md" title="wikilink">通让</a>、<a href="../Page/平齐线.md" title="wikilink">平齐线</a>：<a href="../Page/T39/40次列车.md" title="wikilink">T39</a><br />
经由<a href="../Page/京哈铁路.md" title="wikilink">京哈</a>、<a href="../Page/滨洲铁路.md" title="wikilink">滨洲</a>、平齐线：<a href="../Page/T47/48次列车.md" title="wikilink">T47</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K265/266次列车.md" title="wikilink">K265</a>、<a href="../Page/K339/340次列车.md" title="wikilink">K339</a>、<a href="../Page/K349/350次列车.md" title="wikilink">K349</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K497</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K1301/1302次列车.md" title="wikilink">K1302</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K1303</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p><a href="../Page/Z317/318次列车.md" title="wikilink">Z317</a>、K263</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p><a href="../Page/K285/286次列车.md" title="wikilink">K285</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>K1785</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K1901</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p><a href="../Page/K41/44、K43/42次列车.md" title="wikilink">K41</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/K1177/1178次列车.md" title="wikilink">K1177</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p><a href="../Page/K45/46次列车.md" title="wikilink">K45</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/T145/148、T147/146次列车.md" title="wikilink">T145</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p><a href="../Page/Z29/30次列车.md" title="wikilink">Z29</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Z51/52次列车.md" title="wikilink">Z51</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Z225/228、Z227/226次列车.md" title="wikilink">Z225</a>、<a href="../Page/T63/64次列车.md" title="wikilink">T63</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K1613</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/T63/64次列车.md" title="wikilink">K7771</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1461/1462次列车.md" title="wikilink">1461</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Z61/62次列车.md" title="wikilink">Z61</a>、<a href="../Page/Z63/64次列车.md" title="wikilink">Z63</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>经由<a href="../Page/京沪铁路.md" title="wikilink">京沪</a>、<a href="../Page/津山铁路.md" title="wikilink">津山</a>、<a href="../Page/沈山铁路.md" title="wikilink">沈山</a>、<a href="../Page/沟海铁路.md" title="wikilink">沟海</a>、<a href="../Page/沈大线.md" title="wikilink">沈大线</a>：<a href="../Page/Z81/80、Z79/82次列车.md" title="wikilink">Z79</a><br />
经由京哈、盘五、沟海、沈大线：K681</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Z117/118次列车.md" title="wikilink">Z117</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K53/54次列车.md" title="wikilink">K53</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K95</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K215/216次列车.md" title="wikilink">K215</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K429</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/K1023/1024次列车.md" title="wikilink">K1023</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K1189</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>K2559</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2251/2253/2256/2257、2258/2255/2254/2252次列车.md" title="wikilink">2257</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2549</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2589</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p><a href="../Page/K601/602次列车.md" title="wikilink">K601</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/K603/604次列车.md" title="wikilink">K603</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>K609</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>K615</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p><a href="../Page/Z179/180次列车.md" title="wikilink">Z179</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>K183</p></td>
<td><p><a href="../Page/南阳站_(河南).md" title="wikilink">南阳</a></p></td>
</tr>
</tbody>
</table>

### 过路列车

[201606_Z622_arrived_at_Beijing_Station.jpg](https://zh.wikipedia.org/wiki/File:201606_Z622_arrived_at_Beijing_Station.jpg "fig:201606_Z622_arrived_at_Beijing_Station.jpg")
北京站的过路列车主要包括以下两种情形：进北京站折角运行或是经由[北京地下直径线运行](../Page/北京地下直径线.md "wikilink")\[38\]。

<table>
<thead>
<tr class="header">
<th><p>列车所属路局</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center></td>
<td><p>进北京站折角：、（K7757）、、、（K7713）、、<br />
经由北京地下直径线：、（K7753）、（K7751）、（市郊铁路副中心线北京西始发列车）</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、</p></td>
</tr>
</tbody>
</table>

## 周邊交通

北京站也是北京的一个交通枢纽，站前街有诸多公交车的经停站与起点站，另外，还有长途汽车开往北京周边地区；[北京地铁2号线途经北京站](../Page/北京地铁2号线.md "wikilink")。

### 地铁

### 公交

## 友好车站

[Shangye_Yi_Bainian_Song.jpg](https://zh.wikipedia.org/wiki/File:Shangye_Yi_Bainian_Song.jpg "fig:Shangye_Yi_Bainian_Song.jpg")》附诗《上野驿百年颂》，展示于JR[上野站新干线检票口内](../Page/上野站.md "wikilink")\]\]

  - ：北京站时任北京站站长的施萃贤于1980年曾作为团长带领北京站职工一行6人前往日本访问，并与[东京站和](../Page/東京站.md "wikilink")[上野站结为友好车站](../Page/上野站.md "wikilink")\[39\]。目前上野站新干线剪票口内设有中国陶瓷画艺术家[王学仲于](../Page/王学仲.md "wikilink")1983年为纪念上野站与北京站结为友好车站2周年纪念所做《[上野四季繁荣图](../Page/上野四季繁荣图.md "wikilink")》\[40\]。

  - ：北京站与2007年4月和[首爾站结为友好车站](../Page/首爾站.md "wikilink")\[41\]。

## 重大事故

  - 1969年5月29日晚上10时19分，北京铁路局北京内燃机务段的[东风型](../Page/东风型柴油机车.md "wikilink")1210号机车，牵引由[兰州开往北京的](../Page/兰州站.md "wikilink")44次特快旅客列车（后演变为今[K41/4/3/2次列车](../Page/K41/44、K43/42次列车.md "wikilink")）到达[双沙铁路](../Page/双沙铁路.md "wikilink")[东郊站](../Page/北京东站.md "wikilink")，因当日北京工程段在内燃机务段扩建施工时铲断了地下电缆，造成东郊至北京站间[半自动闭塞系统故障](../Page/閉塞_\(鐵路\).md "wikilink")，改用[电话闭塞法行车](../Page/电话电报闭塞.md "wikilink")，因此44次列车在东郊站临时停车接过路票，并于晚上10时20分从东郊站开出。当列车接近北京站时，司机和副司机两人看见[进站信号机显示红灯](../Page/铁路信号机.md "wikilink")，但司机盲目认为路票有效范围包含进站，并不顾副司机劝阻，执意越过进站信号机开往北京站，当发现前方进路有列车迎面开来已停车不及，于晚上10时27分在北京站57号[道岔处](../Page/道岔.md "wikilink")，与由北京开往[太原的](../Page/太原站.md "wikilink")87次旅客快车（后演变为今[K601/602次列车](../Page/K601/602次列车.md "wikilink")）发生正面冲突（由北京铁路局保定机务段的[人民型](../Page/人民型蒸汽機車.md "wikilink")1224号蒸汽机车牵引）。事故造成87次列车司机和司炉当场丧生，旅客和列车乘务人员重伤10人、轻伤457人，机车大破3台、客车大破4辆、小破19辆。5月30日，时任[国务院总理](../Page/中华人民共和国国务院总理.md "wikilink")[周恩来亲自到北京站调查处理这一事故](../Page/周恩来.md "wikilink")。\[42\]。

<!-- end list -->

  - 1980年10月29日晚上18時15分\[43\]，北京站二樓南走廊發生爆炸，造成10死81傷\[44\]\[45\]，為[北京自](../Page/北京市.md "wikilink")1966年後的首次爆炸事件，[公安局公佈事故由一名來自](../Page/公安局.md "wikilink")[山西省的](../Page/山西省.md "wikilink")30歲工人放置炸彈引爆造成，兇徒亦在爆炸中身亡。

## 相邻车站

|-   |-    |-

## 參見

  - [北京站 (地铁)](../Page/北京站_\(地铁\).md "wikilink")
  - [北京西站](../Page/北京西站.md "wikilink")
  - [北京北站](../Page/北京北站.md "wikilink")
  - [北京南站](../Page/北京南站.md "wikilink")
  - [北京东站](../Page/北京东站.md "wikilink")
  - [北京地铁](../Page/北京地铁.md "wikilink")
  - [中国铁路北京局集团有限公司](../Page/中国铁路北京局集团有限公司.md "wikilink")
  - [中国铁路总公司](../Page/中国铁路总公司.md "wikilink")
  - [中华人民共和国铁道部](../Page/中华人民共和国铁道部.md "wikilink")

## 参考文献

## 外部链接

  - [北京站官方网站](https://web.archive.org/web/20070715193201/http://www.bjrailwaystation.com.cn/)

  -
  -
[Category:北京市交通枢纽](../Category/北京市交通枢纽.md "wikilink")
[Category:1959年啟用的鐵路車站](../Category/1959年啟用的鐵路車站.md "wikilink")
[Category:1959年完工建築物](../Category/1959年完工建築物.md "wikilink")
[Category:北京市东城区铁路车站](../Category/北京市东城区铁路车站.md "wikilink")
[Category:北京市东城区普查登记文物](../Category/北京市东城区普查登记文物.md "wikilink")

1.
2.

3.

4.

5.

6.
7.

8.
9.
10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.
38.

39.

40.

41.

42.

43.

44.

45.