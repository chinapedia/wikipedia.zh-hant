**I album
-iD-**是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")9張[專輯](../Page/音樂專輯.md "wikilink")。於2006年12月13日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

本專輯是與上張專輯『[H album
-H·A·N·D-](../Page/H_album_-H·A·N·D-.md "wikilink")』相隔約1年零一個月的第九張原創專輯。由於成員兩人各自忙於個人活動之中，所以在本年的前半年並沒有任何以團體名義推出的作品。踏入下半年，才在7月推出本年度首張單曲『[夏日風情](../Page/夏日風情.md "wikilink")』。結果在11月再次推出新單曲『[十二月禮讚](../Page/Harmony_of_December.md "wikilink")』時，便同時間公佈了會在12月推出年度專輯。

本專輯已經是連續第三張專輯附有副題，與近幾年的年度專輯都放在年末推出一樣成為了慣例。副題的“iD”包含了眾多意義：成員二人的身份証明（identity）、本能上的衝動（[拉丁語裡id的意思](../Page/拉丁語.md "wikilink")）、正進行個人活動中的二人亦會回歸自己的『原點』繼續KinKi
Kids的團體活動、[音樂才是二人的](../Page/音樂.md "wikilink")[ID](../Page/ID.md "wikilink")。是一張把KinKi
Kids的存在重新審視的作品。

另外據[堂本光一表示](../Page/堂本光一.md "wikilink")，副題之所以選定為“iD”二字，「因為把我們兩個的名字以[羅馬字書寫的話](../Page/羅馬字.md "wikilink")，就會是『Koich**i
D**omoto・Tsuyosh**i D**omoto』。就是說名字的最後一個字與姓氏的第一個字就是iD二字。」

本專輯共收錄了三張單曲的A面曲。分別是『H album -H・A・N・D-』推出時未有收錄的『[SNOW\! SNOW\!
SNOW\!](../Page/SNOW!_SNOW!_SNOW!.md "wikilink")』、「[夏日風情](../Page/夏日風情.md "wikilink")」以及僅在專輯發售前兩星期推出的『[十二月禮讚](../Page/Harmony_of_December.md "wikilink")』。這是KinKi
Kids名義上首次發行的專輯先行單曲。

與上張專輯一樣，分開了初回版和通常版兩個版本。初回版隨碟附送收錄了「Harmony of December」PV、PV製作特輯和在『KinKi
Kids H TOUR -Have A Nice Day-』演唱會上獻唱「SNOW\! SNOW\!
SNOW\!」片段的DVD碟片，以及32頁的歌詞本。而通常版除了封面不同外，隨碟附送20頁的歌詞本，以及多收錄一首歌曲。在封面設計上，無論初回版或通常版都使用了把兩人的照片不斷重覆作背景的設計。

## 收錄歌曲

### 初回版收錄歌曲

1.  **嚴冬的思想**（****）
      - 作曲：[石塚知生](../Page/石塚知生.md "wikilink")
      - 作詞：[淺田信一](../Page/淺田信一.md "wikilink")
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
2.  **藍色的晚風**（****）
      - 作曲：[本間昭光](../Page/本間昭光.md "wikilink")
      - 作詞：[上田Kenji](../Page/上田Kenji.md "wikilink")
      - 編曲：[家原正樹](../Page/家原正樹.md "wikilink")
3.  **SNOW\! SNOW\! SNOW\!**
      - ※第22張單曲。
      - 作曲：[伊秩弘將](../Page/伊秩弘將.md "wikilink")
      - 作詞：[秋元　康](../Page/秋元康.md "wikilink")
      - 編曲：[家原正樹](../Page/家原正樹.md "wikilink")
4.  **iD -the World of Gimmicks-**
      - 作曲：[大智](../Page/大智.md "wikilink")、[宮崎　誠](../Page/宮崎誠.md "wikilink")
      - 作詞：[篠崎隆一](../Page/篠崎隆一.md "wikilink")
      - 編曲：[十川知司](../Page/十川知司.md "wikilink")
      - 和音編排：[ko-saku](../Page/ko-saku.md "wikilink")
5.  **Love is the mirage...**
      - ※堂本剛獨唱作品。
      - 作曲：[松本良喜](../Page/松本良喜.md "wikilink")
      - 作詞：[Satomi](../Page/Satomi.md "wikilink")
      - 編曲：十川知司
6.  **futari**
      - ※KinKi Kids二人自己作曲作詞的合唱作品。
      - 作曲：[堂本　剛](../Page/堂本剛.md "wikilink")
      - 作詞：[堂本光一](../Page/堂本光一.md "wikilink")
      - 編曲：[吉田　建](../Page/吉田建.md "wikilink")
      - 銅樂編排：[下神龍哉](../Page/下神龍哉.md "wikilink")
      - 弦樂編排：[一徹　源](../Page/一徹源.md "wikilink")
7.  **Get it on**
      - ※堂本光一獨唱作品。
      - 作曲：[井手功二](../Page/井手功二.md "wikilink")
      - 作詞：[白井裕紀](../Page/白井裕紀.md "wikilink")、[新美香](../Page/新美香.md "wikilink")
      - 編曲：[鈴木雅也](../Page/鈴木雅也.md "wikilink")
8.  **Black Joke**
      - 作曲：[U-SKE](../Page/U-SKE.md "wikilink")
      - 作詞：[前田隆弘](../Page/前田隆弘.md "wikilink")
      - 編曲：U-SKE
      - 和音編排：ko-saku
9.  **夏日風情**（****）
      - ※第23張單曲。
      - 作曲：[林田健司](../Page/林田健司.md "wikilink")
      - 作詞：[Satomi](../Page/Satomi.md "wikilink")
      - 編曲：[佐久間　誠](../Page/佐久間誠.md "wikilink")
      - 弦樂編排：[佐藤泰將](../Page/佐藤泰將.md "wikilink")
10. **Parental Advisory Explicit Content**
      - 作曲：[林田健司](../Page/林田健司.md "wikilink")
      - 作詞：Satomi
      - 編曲：吉田　建
11. **Night+Flight**
      - 作曲：Gajin
      - 作詞：Gajin
      - 編曲：[佐久間　誠](../Page/佐久間誠.md "wikilink")
      - 和音編排：[高橋哲也](../Page/高橋哲也.md "wikilink")
12. **十二月禮讚**（**Harmony of December**）
      - ※第24張單曲。
      - 作曲：[Mashiko Tatsuro](../Page/Mashiko_Tatsuro.md "wikilink")
      - 作詞：Mashiko Tatsuro
      - 編曲：[ha-j](../Page/ha-j.md "wikilink")

### 通常版收錄歌曲

1.  **嚴冬的思想**
2.  **藍色的晚風**
3.  **SNOW\! SNOW\! SNOW\!**
4.  **iD -the World of Gimmicks-**
5.  **Love is the mirage...**
6.  **futari**
7.  **Get it on**
8.  **Black Joke**
9.  **夏日風情**
10. **Parental Advisory Explicit Content**
11. **Night+Flight**
12. **十二月禮讚**
13. **Love is... ～因何時都有你在身旁～**（**Love is... **）
      - 作曲：[成海Gazuto](../Page/成海Gazuto.md "wikilink")
      - 作詞：成海Gazuto
      - 編曲：[garamonn](../Page/garamonn.md "wikilink")
      - 和音編排：[岩田雅之](../Page/岩田雅之.md "wikilink")

[Category:近畿小子專輯](../Category/近畿小子專輯.md "wikilink")
[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:2006年Oricon專輯週榜冠軍作品](../Category/2006年Oricon專輯週榜冠軍作品.md "wikilink")