**德川慶喜**是[NHK於](../Page/日本放送協會.md "wikilink")1998年播出的[大河劇](../Page/NHK大河劇.md "wikilink")，播出期間是1998年1月4日～12月13日，原作是[司馬遼太郎](../Page/司馬遼太郎.md "wikilink")、主角演員是[本木雅弘](../Page/本木雅弘.md "wikilink")。

本劇不但有[菅原文太](../Page/菅原文太.md "wikilink")、[若尾文子](../Page/若尾文子.md "wikilink")、[杉良太郎等資深演員](../Page/杉良太郎.md "wikilink")，也起用了像是[石田光](../Page/石田光.md "wikilink")、[深津繪里等年輕演員](../Page/深津繪里.md "wikilink")，以維持大河劇的人氣，而一些架空虛構的人事物也增添了本劇的故事性。但是由於幕末時代各方面的人物關係與對立關係都很複雜，關於將軍慶喜的部份也顯得頗為貧乏，使得收視率也難以提升。早期[緯來日本台曾以](../Page/緯來日本台.md "wikilink")《**最後的將軍**》片名播放。

本劇是少數幕末片中沒有[坂本龍馬登場的電視劇](../Page/坂本龍馬.md "wikilink")。

## 工作人員

  - 原作：司馬遼太郎《最後的將軍》
  - 脚本：[田向正健](../Page/田向正健.md "wikilink")
  - 音樂：[湯淺讓二](../Page/湯淺讓二.md "wikilink")
  - 導演：[放送日程記載](../Page/#放送日程.md "wikilink")
  - 主題音樂演奏：[NHK交響樂團](../Page/NHK交響樂團.md "wikilink")
  - 主題音樂指揮：[岩城宏之](../Page/岩城宏之.md "wikilink")
  - 旁白：[大原麗子](../Page/大原麗子.md "wikilink")

## 演員

### 德川將軍家

  - [德川慶喜](../Page/德川慶喜.md "wikilink")：[若葉龍也](../Page/若葉龍也.md "wikilink")→[崎本大海](../Page/崎本大海.md "wikilink")→[本木雅弘](../Page/本木雅弘.md "wikilink")
  - [德川美賀](../Page/一條美賀子.md "wikilink")：[石田光](../Page/石田光.md "wikilink")
  - [德川齊昭](../Page/德川齊昭.md "wikilink")：[菅原文太](../Page/菅原文太.md "wikilink")
  - [吉子女王](../Page/登美宮吉子.md "wikilink")→貞芳院：[若尾文子](../Page/若尾文子.md "wikilink")
  - [德川慶篤](../Page/德川慶篤.md "wikilink")：[笹川謙](../Page/笹川謙.md "wikilink")→[内野聖陽](../Page/内野聖陽.md "wikilink")
  - [德川昭武](../Page/德川昭武.md "wikilink")：[中村橋之助
    (3代目)](../Page/中村橋之助_\(3代目\).md "wikilink")（取自[獅子的時代影像片段](../Page/獅子的時代_\(大河劇\).md "wikilink")）
  - [德信院](../Page/德信院.md "wikilink")：[鶴田真由](../Page/鶴田真由.md "wikilink")
  - [德川家慶](../Page/德川家慶.md "wikilink")：[鈴木瑞穗](../Page/鈴木瑞穗.md "wikilink")
  - [德川家定](../Page/德川家定.md "wikilink")：[大出真也](../Page/大出真也.md "wikilink")
  - [天璋院](../Page/天璋院.md "wikilink")：[深津繪里](../Page/深津繪里.md "wikilink")
  - [德川家茂](../Page/德川家茂.md "wikilink")：[木田健太](../Page/木田健太.md "wikilink")→[水橋研二](../Page/水橋研二.md "wikilink")→[進藤健太郎](../Page/進藤健太郎.md "wikilink")
  - [和宮](../Page/和宮.md "wikilink")：[小橋惠](../Page/小橋惠.md "wikilink")
  - [觀行院](../Page/橋本經子.md "wikilink")：[山本陽子](../Page/山本陽子.md "wikilink")
  - [田安龜之助](../Page/德川家達.md "wikilink")：[鎌田佳裕](../Page/鎌田佳裕.md "wikilink")

### 德川慶喜側近

  - [平岡円四郎](../Page/平岡円四郎.md "wikilink")：[新井康弘](../Page/新井康弘.md "wikilink")
  - [土岐朝義](../Page/土岐朝義.md "wikilink")：[小野武彦](../Page/小野武彦.md "wikilink")
  - [西周](../Page/西周_\(啓蒙家\).md "wikilink")：[小日向文世](../Page/小日向文世.md "wikilink")

### 幕閣

  - [井伊直弼](../Page/井伊直弼.md "wikilink")：[杉良太郎](../Page/杉良太郎.md "wikilink")
  - [酒井忠績](../Page/酒井忠績.md "wikilink")：[御友公喜](../Page/御友公喜.md "wikilink")
  - [阿部正弘](../Page/阿部正弘.md "wikilink")：[大橋吾郎](../Page/大橋吾郎.md "wikilink")
  - [阿部正外](../Page/阿部正外.md "wikilink")：[小川隆市](../Page/小川隆市.md "wikilink")
  - [久世廣周](../Page/久世廣周.md "wikilink")：[江上雅彥](../Page/江上雅彥.md "wikilink")→[大和田伸也](../Page/大和田伸也.md "wikilink")
  - [小笠原長行](../Page/小笠原長行.md "wikilink")：[若松武史](../Page/若松武史.md "wikilink")
  - [脇坂安宅](../Page/脇坂安宅.md "wikilink")：[佐佐木功](../Page/佐佐木功.md "wikilink")
  - [堀田正睦](../Page/堀田正睦.md "wikilink")：[有川博](../Page/有川博.md "wikilink")
  - [間部詮勝](../Page/間部詮勝.md "wikilink")：[石井愃一](../Page/石井愃一.md "wikilink")
  - [内藤信思](../Page/内藤信思.md "wikilink")：[真實一路](../Page/真實一路.md "wikilink")
  - [松平乘全](../Page/松平乘全.md "wikilink")：[内山森彥](../Page/内山森彥.md "wikilink")
  - [松平宗秀](../Page/松平宗秀.md "wikilink")：[坂西良太](../Page/坂西良太.md "wikilink")
  - [松平康英](../Page/松平康英.md "wikilink")：[武發史朗](../Page/武發史朗.md "wikilink")
  - [松平忠固](../Page/松平忠固.md "wikilink")：[吾羽七朗](../Page/吾羽七朗.md "wikilink")
  - [松平乘謨](../Page/松平乘謨.md "wikilink")：[小栗健伸](../Page/小栗健伸.md "wikilink")
  - [松平信義](../Page/松平信義.md "wikilink")：[博多章敬](../Page/博多章敬.md "wikilink")
  - [水野忠精](../Page/水野忠精.md "wikilink")：[赤羽秀之](../Page/赤羽秀之.md "wikilink")
  - [稻葉正邦](../Page/稻葉正邦.md "wikilink")：[井上倫宏](../Page/井上倫宏.md "wikilink")
  - [稻葉正巳](../Page/稻葉正巳.md "wikilink")：[勝光德](../Page/勝光德.md "wikilink")
  - [板倉勝靜](../Page/板倉勝靜.md "wikilink")：[小林宏史](../Page/小林宏史.md "wikilink")
  - [本多忠民](../Page/本多忠民.md "wikilink")：[森下哲夫](../Page/森下哲夫.md "wikilink")
  - [諏訪忠誠](../Page/諏訪忠誠.md "wikilink")：[鹿内孝](../Page/鹿内孝.md "wikilink")
  - [牧野忠恭](../Page/牧野忠恭.md "wikilink")：[樋浦勉](../Page/樋浦勉.md "wikilink")
  - [牧野忠雅](../Page/牧野忠雅.md "wikilink")：[堀内正美](../Page/堀内正美.md "wikilink")
  - [井上正直](../Page/井上正直.md "wikilink")：[丹治大吾](../Page/丹治大吾.md "wikilink")
  - [安藤信正](../Page/安藤信正.md "wikilink")：[佐佐木敏](../Page/佐佐木敏.md "wikilink")
  - [太田資始](../Page/太田資始.md "wikilink")：[村上幹夫](../Page/村上幹夫.md "wikilink")

### 諸大名

  - [松平慶永](../Page/松平慶永.md "wikilink")：[林隆三](../Page/林隆三.md "wikilink")
  - [松平容保](../Page/松平容保.md "wikilink")：[畠中洋](../Page/畠中洋.md "wikilink")
  - [松平定敬](../Page/松平定敬.md "wikilink")：[渡邊穰](../Page/渡邊穰.md "wikilink")
  - [伊達宗城](../Page/伊達宗城.md "wikilink")：[大門正明](../Page/大門正明.md "wikilink")
  - [德川慶勝](../Page/德川慶勝.md "wikilink")：[野村祐人](../Page/野村祐人.md "wikilink")
  - [德川茂榮](../Page/德川茂榮.md "wikilink")：[石丸謙二郎](../Page/石丸謙二郎.md "wikilink")
  - [山内容堂](../Page/山内容堂.md "wikilink")：[鹽屋俊](../Page/鹽屋俊.md "wikilink")
  - [島津齊彬](../Page/島津齊彬.md "wikilink")：[立川三貴](../Page/立川三貴.md "wikilink")
  - [島津忠義](../Page/島津忠義.md "wikilink")：[田代泰司](../Page/田代泰司.md "wikilink")→[佐藤旭](../Page/佐藤旭.md "wikilink")
  - [島津久光](../Page/島津久光.md "wikilink")：[江守徹](../Page/江守徹.md "wikilink")
  - [酒井忠義](../Page/酒井忠義.md "wikilink")：[矢島健一](../Page/矢島健一.md "wikilink")
  - [立花種恭](../Page/立花種恭.md "wikilink")：[伊丹幸雄](../Page/伊丹幸雄.md "wikilink")
  - [松前崇廣](../Page/松前崇廣.md "wikilink")：[山内敏男](../Page/山内敏男.md "wikilink")
  - [毛利敬親](../Page/毛利敬親.md "wikilink")：[仲恭司](../Page/仲恭司.md "wikilink")
  - [毛利元德](../Page/毛利元德.md "wikilink")：[天間信紘](../Page/天間信紘.md "wikilink")
  - [淺野長勳](../Page/淺野長勳.md "wikilink")：[岸本光正](../Page/岸本光正.md "wikilink")
  - [有馬道純](../Page/有馬道純.md "wikilink")：[梁瀨哲](../Page/梁瀨哲.md "wikilink")

### 幕臣

  - [川路聖謨](../Page/川路聖謨.md "wikilink")：[勝部演之](../Page/勝部演之.md "wikilink")
  - [勝海舟](../Page/勝海舟.md "wikilink")：[坂東八十助
    (5代目)](../Page/坂東八十助_\(5代目\).md "wikilink")
  - [山岡鐵太郎](../Page/山岡鐵舟.md "wikilink")：[伊武雅刀](../Page/伊武雅刀.md "wikilink")
  - [岩瀨忠震](../Page/岩瀨忠震.md "wikilink")：[富家規政](../Page/富家規政.md "wikilink")
  - [永井尚志](../Page/永井尚志.md "wikilink")：[阿部雅彥](../Page/阿部雅彥.md "wikilink")→[早川純一](../Page/早川純一.md "wikilink")
  - [近藤勇](../Page/近藤勇.md "wikilink")：[勝野洋](../Page/勝野洋.md "wikilink")
  - [土方歲三](../Page/土方歲三.md "wikilink")：[橋爪淳](../Page/橋爪淳.md "wikilink")
  - [沖田總司](../Page/沖田總司.md "wikilink")：[小澤征悅](../Page/小澤征悅.md "wikilink")

### 各藩藩士

  - [西鄉吉之助](../Page/西鄉隆盛.md "wikilink")：[渡邊徹](../Page/渡邊徹.md "wikilink")
  - [大久保利通](../Page/大久保利通.md "wikilink")：[池田成志](../Page/池田成志.md "wikilink")
  - [小松帶刀](../Page/小松帶刀.md "wikilink")：[安藤一夫](../Page/安藤一夫.md "wikilink")
  - [藤田小四郎](../Page/藤田小四郎.md "wikilink")：[田邊誠一](../Page/田邊誠一.md "wikilink")
  - [藤田東湖](../Page/藤田東湖.md "wikilink")：[渡邊裕之](../Page/渡邊裕之.md "wikilink")
  - [安島帶刀](../Page/安島帶刀.md "wikilink")：[井田州彥](../Page/井田州彥.md "wikilink")
  - [武田耕雲齋](../Page/武田耕雲齋.md "wikilink")：[金内喜久夫](../Page/金内喜久夫.md "wikilink")
  - [會澤正志齋](../Page/會澤正志齋.md "wikilink")：[飯沼慧](../Page/飯沼慧.md "wikilink")
  - [中根雪江](../Page/中根雪江.md "wikilink")：[石立鐵男](../Page/石立鐵男.md "wikilink")
  - [横井小楠](../Page/横井小楠.md "wikilink")：[高橋長英](../Page/高橋長英.md "wikilink")
  - [宮部鼎藏](../Page/宮部鼎藏.md "wikilink")：[冷泉公裕](../Page/冷泉公裕.md "wikilink")
  - [桂小五郎](../Page/桂小五郎.md "wikilink")：[黑田亞瑟](../Page/黑田亞瑟.md "wikilink")
  - [吉田寅次郎](../Page/吉田松陰.md "wikilink")：[俊藤光利](../Page/俊藤光利.md "wikilink")
  - [久坂玄銳](../Page/久坂玄銳.md "wikilink")：[小井塚登](../Page/小井塚登.md "wikilink")
  - [吉田稔磨](../Page/吉田稔磨.md "wikilink")：[加納詞桂章](../Page/加納詞桂章.md "wikilink")
  - [品川彌二郎](../Page/品川彌二郎.md "wikilink")：[佐藤壹史](../Page/佐藤壹史.md "wikilink")
  - [後藤象二郎](../Page/後藤象二郎.md "wikilink")：[小木茂光](../Page/小木茂光.md "wikilink")
  - [佐久間象山](../Page/佐久間象山.md "wikilink")：[秋間登](../Page/秋間登.md "wikilink")
  - [真木和泉](../Page/真木和泉.md "wikilink")：[山口嘉三](../Page/山口嘉三.md "wikilink")

### 皇室、公家

  - [孝明天皇](../Page/孝明天皇.md "wikilink")：[花柳錦之輔
    (3代目)](../Page/花柳錦之輔_\(3代目\).md "wikilink")
  - [睦仁親王](../Page/明治天皇.md "wikilink")：[安藤一平](../Page/安藤一平.md "wikilink")
  - [有栖川宮熾仁親王](../Page/有栖川宮熾仁親王.md "wikilink")：[佐藤節次](../Page/佐藤節次.md "wikilink")
  - [中川宮](../Page/中川宮.md "wikilink")：[近藤誠人](../Page/近藤誠人.md "wikilink")→[木下浩之](../Page/木下浩之.md "wikilink")
  - [岩倉具視](../Page/岩倉具視.md "wikilink")：[寺脇康文](../Page/寺脇康文.md "wikilink")
  - [大原重德](../Page/大原重德.md "wikilink")：[岡村喬生](../Page/岡村喬生.md "wikilink")
  - [姉小路公知](../Page/姉小路公知.md "wikilink")：[大澤健](../Page/大澤健.md "wikilink")
  - [正親町三條實愛](../Page/正親町三條實愛.md "wikilink")：[森田順平](../Page/森田順平.md "wikilink")
  - [三条實美](../Page/三条實美.md "wikilink")：[宇津木真](../Page/宇津木真.md "wikilink")
  - [中山忠能](../Page/中山忠能.md "wikilink")：[真夏龍](../Page/真夏龍.md "wikilink")
  - [近衛忠房](../Page/近衛忠房.md "wikilink")：[渡邊規生](../Page/渡邊規生.md "wikilink")→[小倉雄三](../Page/小倉雄三.md "wikilink")
  - [島田左近](../Page/島田左近.md "wikilink")：[竹本孝之](../Page/竹本孝之.md "wikilink")

### 各國公使

  - [马休·佩里](../Page/马休·佩里.md "wikilink")：ジェームズ・バワーズ(声：[岸野一彦](../Page/岸野一彦.md "wikilink"))
  - [湯森·赫禮士](../Page/湯森·赫禮士.md "wikilink")：トム・キロー
  - [亨利·休斯肯](../Page/亨利·休斯肯.md "wikilink")：マーティン・スロット
  - [亨利·帕克斯](../Page/亨利·帕克斯.md "wikilink")：ダン・レショー(声：[小川真司](../Page/小川真司.md "wikilink"))
  - [恩斯特·薩道](../Page/恩斯特·薩道.md "wikilink")：コリン・リーチ
  - [李昂·羅修](../Page/李昂·羅修.md "wikilink")：ユベール・ジョアニアン(声：[青野武](../Page/青野武.md "wikilink"))

### 其他

  - [新門辰五郎](../Page/新門辰五郎.md "wikilink")：[堺正章](../Page/堺正章.md "wikilink")
  - 戀：[大原麗子](../Page/大原麗子.md "wikilink")
  - 芳：[三國純楓](../Page/三國純楓.md "wikilink")
    →[清水美砂](../Page/清水美砂.md "wikilink")
  - [瀧山](../Page/瀧山.md "wikilink")：[佐佐木澄江](../Page/佐佐木澄江.md "wikilink")
  - [池田屋主人](../Page/池田屋.md "wikilink")：[加藤治](../Page/加藤治.md "wikilink")

### 虛構人物

  - ガンツム：[山下真司](../Page/山下真司.md "wikilink")
  - 民：[水野真紀](../Page/水野真紀.md "wikilink")
  - 美代：[一色紗英](../Page/一色紗英.md "wikilink")
  - 村田新三郎：[泉本央](../Page/泉本央.md "wikilink")→[內野謙太](../Page/內野謙太.md "wikilink")→[藤木直人](../Page/藤木直人.md "wikilink")
  - カンヌキ：[肥後克廣](../Page/肥後克廣.md "wikilink")（[鴕鳥俱樂部](../Page/鴕鳥俱樂部.md "wikilink")）
  - 半次：[上島龍兵](../Page/上島龍兵.md "wikilink")（鴕鳥俱樂部）
  - 義經：[寺門ジモン](../Page/寺門ジモン.md "wikilink")（鴕鳥俱樂部）
  - 櫻：[小林美香](../Page/小林美香.md "wikilink")→[野村知沙](../Page/野村知沙.md "wikilink")→[池脇千鶴](../Page/池脇千鶴.md "wikilink")→[田中伸子](../Page/田中伸子.md "wikilink")
  - 中山五郎左衛門：[藤岡琢也](../Page/藤岡琢也.md "wikilink")
  - 松島：[岸田今日子](../Page/岸田今日子.md "wikilink")

[Category:1999年日本電視劇集](../Category/1999年日本電視劇集.md "wikilink")
[Category:司馬遼太郎小說改編電視劇](../Category/司馬遼太郎小說改編電視劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:茨城縣背景作品](../Category/茨城縣背景作品.md "wikilink")
[Category:江戶背景電視劇](../Category/江戶背景電視劇.md "wikilink")
[Category:幕末背景電視劇](../Category/幕末背景電視劇.md "wikilink")