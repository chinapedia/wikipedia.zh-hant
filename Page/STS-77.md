****是历史上第七十六次航天飞机任务，也是[奋进号航天飞机的第十一次太空飞行](../Page/奮進號太空梭.md "wikilink")。

## 任务成员

  - **[约翰·卡斯帕](../Page/约翰·卡斯帕.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[柯提斯·布朗](../Page/柯提斯·布朗.md "wikilink")**（，曾执行、、、、以及任务），飞行员
  - **[丹尼尔·博斯奇](../Page/丹尼尔·博斯奇.md "wikilink")**（，曾执行、、、、以及任务），任务专家
  - **[马里奥·伦科](../Page/马里奥·伦科.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[马克·加诺](../Page/马克·加诺.md "wikilink")**（，[加拿大宇航员](../Page/加拿大.md "wikilink")，曾执行、以及任务），任务专家
  - **[安德鲁·托马斯](../Page/安德鲁·托马斯.md "wikilink")**（，曾执行、、[和平号](../Page/和平號太空站.md "wikilink")、、以及任务），任务专家

[Category:1996年佛罗里达州](../Category/1996年佛罗里达州.md "wikilink")
[Category:奋进号航天飞机任务](../Category/奋进号航天飞机任务.md "wikilink")
[Category:1996年科學](../Category/1996年科學.md "wikilink")
[Category:1996年5月](../Category/1996年5月.md "wikilink")