**曹節**（），[东汉末年權臣](../Page/东汉.md "wikilink")[曹操的女儿](../Page/曹操.md "wikilink")，[曹丕](../Page/曹丕.md "wikilink")、[曹憲之妹](../Page/曹憲_\(漢獻帝\).md "wikilink")，[曹華之姐](../Page/曹華.md "wikilink")。東漢末代皇帝[汉献帝劉協的次任](../Page/汉献帝.md "wikilink")[皇后](../Page/皇后.md "wikilink")。

## 生平

[建安十八年](../Page/建安_\(东汉\).md "wikilink")（213年）時，曹操將三個女兒[曹憲](../Page/曹憲_\(東漢\).md "wikilink")、曹節和[曹華送入汉献帝后宫为夫人](../Page/曹華.md "wikilink")，年纪最小的曹华在国待年。次年，三女都被封為[貴人](../Page/贵人.md "wikilink")。建安十九年十一月，獻帝皇后[伏壽被曹操弒殺](../Page/伏壽.md "wikilink")，建安二十年正月（215年），因曹操和群臣的壓力下，改立曹貴人為皇后。

建安二十五年（220年），[曹丕篡位自立](../Page/曹丕.md "wikilink")，派遣使者索取玺绶，曹皇后憤怒不給。就這樣來了幾個使者後，曹皇后便將使者叫來，要親自交出玉璽，但卻是將玉璽從丟擲至軒下，大罵：「上天不會保祐你們！」旁人皆不敢抬頭仰視她\[1\]。

漢獻帝襌位後被封為山陽公，曹皇后為山陽公夫人。魏文帝黃初四年十二月丙寅日（西元224年1月20日）賜山陽公夫人[汤沐邑](../Page/汤沐邑.md "wikilink")，[食邑五百戶](../Page/領地.md "wikilink")，[劉曼為](../Page/劉曼.md "wikilink")[長樂郡公主](../Page/長樂郡.md "wikilink")。

[景元元年六月己未日](../Page/景元.md "wikilink")\[2\]（260年7月2日\[3\]），曹節過世，谥号献穆皇后，合葬於漢獻帝劉協的禪陵，所用的車服禮儀皆尊從漢制。

## 家庭

### 父亲

  - [曹操](../Page/曹操.md "wikilink")

###

  - [卞夫人](../Page/卞夫人.md "wikilink")

### 丈夫

  - [漢獻帝劉協](../Page/汉献帝.md "wikilink")

### 兄弟

  - [曹昂](../Page/曹昂.md "wikilink")、[曹丕](../Page/曹丕.md "wikilink")、[曹彰](../Page/曹彰.md "wikilink")、[曹植](../Page/曹植.md "wikilink")、[曹熊](../Page/曹熊.md "wikilink")、[曹冲](../Page/曹冲.md "wikilink")、[曹鑠](../Page/曹鑠.md "wikilink")、[曹據](../Page/曹據.md "wikilink")、[曹宇](../Page/曹宇.md "wikilink")、[曹林](../Page/曹林.md "wikilink")、[曹衮](../Page/曹袞.md "wikilink")、[曹玹](../Page/曹玹.md "wikilink")、[曹峻](../Page/曹峻.md "wikilink")、[曹矩](../Page/曹矩.md "wikilink")、[曹幹](../Page/曹幹.md "wikilink")、[曹上](../Page/曹子上.md "wikilink")、[曹彪](../Page/曹彪.md "wikilink")、[曹勤](../Page/曹子勤.md "wikilink")、[曹乘](../Page/曹子乘.md "wikilink")、[曹整](../Page/曹子整.md "wikilink")、[曹京](../Page/曹子京.md "wikilink")、[曹均](../Page/曹均.md "wikilink")、[曹棘](../Page/曹棘.md "wikilink")、[曹徽](../Page/曹徽.md "wikilink")、[曹茂](../Page/曹茂.md "wikilink")。

### 姐妹

[清河长公主](../Page/清河公主_\(曹魏\).md "wikilink")、[曹宪](../Page/曹憲_\(東漢\).md "wikilink")、[曹華](../Page/曹華.md "wikilink")、[安阳公主](../Page/安陽公主_\(曹魏\).md "wikilink")、[金乡公主](../Page/金鄉公主_\(曹魏\).md "wikilink")、[临汾公主](../Page/临汾公主.md "wikilink")

###

長樂郡公主[劉曼](../Page/劉曼.md "wikilink")

## 评价

  - [毛宗岗](../Page/毛宗崗.md "wikilink")：“[吕雉王](../Page/吕雉.md "wikilink")[产](../Page/呂產.md "wikilink")、[禄](../Page/呂祿.md "wikilink")，而刘几化吕；[武曌宠](../Page/武则天.md "wikilink")[三思](../Page/武三思.md "wikilink")，而[周几代](../Page/武周.md "wikilink")[唐](../Page/唐朝.md "wikilink")。若曹-{zh-hans:后;
    zh-hant:后}-者，诚过之矣。曹-{zh-hans:后;
    zh-hant:后}-之骂[曹丕](../Page/曹丕.md "wikilink")，比之王-{zh-hans:后;
    zh-hant:后}-之骂[王莽](../Page/王莽.md "wikilink")，庶几相似乎？然以-{zh-hans:后;
    zh-hant:后}-之贵而贵其族者，王-{zh-hans:后; zh-hant:后}-也；以族之贵而贵为-{zh-hans:后;
    zh-hant:后}-者，曹-{zh-hans:后; zh-hant:后}-也。族以-{zh-hans:后;
    zh-hant:后}-之故而得贵，则-{zh-hans:后; zh-hant:后}-之斥之也易；-{zh-hans:后;
    zh-hant:后}-因族之故而得立，则-{zh-hans:后; zh-hant:后}-之不党其族也难。推曹-{zh-hans:后;
    zh-hant:后}-之心，使其身非曹操之所出，我知其必与父兄同谋讨贼，如伏-{zh-hans:后;
    zh-hant:后}-、[董妃之事耳](../Page/董貴人.md "wikilink")。伏完有女而曹操亦有女，[董承有妹而曹丕亦有妹](../Page/董承.md "wikilink")。曹-{zh-hans:后;
    zh-hant:后}-之贤，殆将与伏-{zh-hans:后; zh-hant:后}-、董妃并列为三云。”（汇评三国演义）

## 艺术形象

### 三國演義

  - 曹节生平与正史大体一致，本为汉献帝贵人，曹操命人杖杀伏皇后之後，汉献帝封其为皇后。后曹丕逼迫汉献帝禅位，汉献帝将此事告知曹节，此时曹洪、曹休带剑入内请汉献帝出殿，在《[三国志通俗演义（罗本）](../Page/三国演义.md "wikilink")》曹节对汉献帝破口大骂，说：“你说我兄长是篡国之贼，你先祖汉高祖[刘邦只是丰沛一嗜酒匹夫](../Page/刘邦.md "wikilink")，无籍小辈，尚且劫夺秦朝天下。我父亲扫清海内，我兄长屡有大功，有何不可为帝？你即位三十多年，如果没有我父亲、兄长，你早就粉身碎骨了！”说完要上车出外。而在《[三国演义（毛本）](../Page/三国演义.md "wikilink")》中则对[曹休](../Page/曹休.md "wikilink")、[曹洪破口大骂](../Page/曹洪.md "wikilink")：“俱是汝等乱贼，希图富贵，共造逆谋！吾父功盖寰区，威震天下，然且不敢篡窃神器。今吾兄嗣位未几，辄思篡汉，皇天必不祚尔！”言罢，痛哭入宫。

### 影视形象

  - 1994年电视剧《[三国演义](../Page/三国演义_\(电视剧\).md "wikilink")》：扮演者不明；
  - 1999年电影《[一代枭雄曹操](../Page/一代枭雄曹操.md "wikilink")》：[茹萍饰演曹节](../Page/茹萍.md "wikilink")
  - 1999年电视剧《[曹操](../Page/曹操_\(1999年电视剧\).md "wikilink")》：[聂小雪饰演曹节](../Page/聂小雪.md "wikilink")
  - 2010年电视剧《[三国](../Page/三国_\(电视剧\).md "wikilink")》：[刘梓娇饰演曹节](../Page/刘梓娇.md "wikilink")
  - 2013年电视剧《[新洛神](../Page/新洛神.md "wikilink")》：李秀饰演曹节
  - 2018年電視劇《[三國機密之潛龍在淵](../Page/三国机密之潜龙在渊.md "wikilink")》：[王玉雯飾演曹節](../Page/王玉雯.md "wikilink")

### 漫畫形象

  - 《[蒼天航路](../Page/蒼天航路.md "wikilink")》（[王欣太](../Page/王欣太.md "wikilink")）

## 注释

## 参考资料

  - 《后汉书·皇后纪下》
  - 《三国志·魏书·武帝本纪》
  - 《三國志·魏書·文帝本纪》

[C曹節](../Category/東漢皇后.md "wikilink")
[Category:曹操女儿](../Category/曹操女儿.md "wikilink")
[J節](../Category/曹姓.md "wikilink")

1.  《后汉书》卷十《皇后纪》:魏受禅，遣使求玺绶，后怒不与。如此数辈，后乃呼使者入，亲数让之，以玺抵轩下，因涕泣横流曰：“天不祚尔！”左右皆莫能仰视。
2.  《三國志·魏志·陳留王奐紀》：（六月）己未，故汉献帝夫人节薨，帝临于华林园，使使持节追谥夫人为献穆皇后。
3.  [兩千年中西曆換算](http://sinocal.sinica.edu.tw)