**華晨中國汽車控股有限公司**（、），簡稱**華晨中國**，是一家在[香港交易所上市的](../Page/香港交易所.md "wikilink")[工業公司](../Page/工業.md "wikilink")，主要業務為製造及銷售輕型[客車](../Page/客車.md "wikilink")、[轎車及](../Page/轎車.md "wikilink")[汽車](../Page/汽車.md "wikilink")[零部件](../Page/零部件.md "wikilink")，為[中國汽车製造商](../Page/中国汽车制造业.md "wikilink")[华晨汽车集团控股有限公司旗下的](../Page/华晨汽车集团控股有限公司.md "wikilink")[控股公司](../Page/控股公司.md "wikilink")。

2010年，華晨中國旗下的華晨寶馬與[寶馬成立汽車金融公司寶馬汽車金融](../Page/BMW.md "wikilink")（中國），註冊資本為5億元[人民幣](../Page/人民幣.md "wikilink")，兩方分別佔42%和58%的股權。\[1\]

2017年7月4日華晨中國與[雷諾訂立框架合作協議](../Page/雷諾.md "wikilink")，雷諾同意以1元人民幣收購，瀋陽華晨金杯汽車有限公司之49%股本權益

2018年10月11日[宝马集团斥資](../Page/BMW.md "wikilink")36億歐元（約326億港元）（約290億元人民幣）增持與[華晨中國汽車組成的合資企業](../Page/華晨中國汽車.md "wikilink")[華晨寶馬](../Page/華晨寶馬.md "wikilink")25%股權，交易完成後[宝马集团的持股比例將由現時的](../Page/BMW.md "wikilink")50%增至75%，此外，合資公司的合同期限將從原來的2028年延長至2040年。

## 參考

## 外部連結

  - [華晨中國汽車控股有限公司網站](http://www.brillianceauto.com)

[Category:总部在沈阳的中华人民共和国国有公司](../Category/总部在沈阳的中华人民共和国国有公司.md "wikilink")
[Category:香港上市工業公司](../Category/香港上市工業公司.md "wikilink")
[Category:恒生香港中資企業指數成份股](../Category/恒生香港中資企業指數成份股.md "wikilink")
[Category:华晨汽车](../Category/华晨汽车.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")
[Category:汽車股](../Category/汽車股.md "wikilink")

1.  [華晨與寶馬組合營
    曾升8.6%](http://hk.apple.nextmedia.com/realtime/art_main.php?iss_id=20100917&sec_id=7015342&subsec=6996645&art_id=14460553)