**建滔積層板控股有限公司**（）是一家在香港交易所上市的工業公司，現為全球最大的[積層板生產商之一](../Page/積層板.md "wikilink")。2006年12月7日，[建滔化工集團把本身的積層板業務分拆上市](../Page/建滔化工集團.md "wikilink")，建滔化工為公司現時最大[股東](../Page/股東.md "wikilink")。

**建滔積層板**的主要業務為生產[覆銅面板](../Page/覆銅面板.md "wikilink")，覆銅面板可用於生產印刷線路板，而印刷線路應用範圍廣泛，可適用於生產各類的電子產品。

建滔積層板自上市股價高見$9.93港元後，便一直下跌，股價長期低於招股價$7.73港元，很多投資者都[損手](../Page/套牢股票.md "wikilink")，被戲稱「**見高即沉板**」，意思即是股價當升至高位便立即下跌。不過股價到2017年已升回當年招股價之上。

2012年4月，建滔積層板購入兩幅[江蘇省](../Page/江蘇省.md "wikilink")[昆山市地皮用作住宅發展](../Page/昆山市.md "wikilink")。\[1\]

## 參考

## 外部連線

  - [建滔積層板控股有限公司](http://www.kblaminates.com/)

[CATEGORY:中國房地產開發公司](../Page/CATEGORY:中國房地產開發公司.md "wikilink")

[Category:香港電子公司](../Category/香港電子公司.md "wikilink")
[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市工業公司](../Category/香港上市工業公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:深圳公司](../Category/深圳公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:1988年成立的公司](../Category/1988年成立的公司.md "wikilink")

1.  [建滔板再添食買兩地](http://hk.apple.nextmedia.com/financeestate/art/20120427/16286488)
    蘋果日報，2012年4月27日