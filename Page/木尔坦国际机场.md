**木尔坦国际机场**（[IATA](../Page/IATA.md "wikilink")：**MUX**）位於[巴基斯坦](../Page/巴基斯坦.md "wikilink")[旁遮普省城市](../Page/旁遮普省.md "wikilink")[木爾坦以西](../Page/木爾坦.md "wikilink")10公里。有[混凝土跑道一條](../Page/混凝土.md "wikilink")。

可以到達的主要城市有：[喀拉蚩](../Page/喀拉蚩.md "wikilink")、[拉合爾](../Page/拉合爾.md "wikilink")、[伊斯蘭堡](../Page/伊斯蘭堡.md "wikilink")、[費薩爾巴德和](../Page/費薩爾巴德.md "wikilink")[杜拜](../Page/杜拜.md "wikilink")。

## 航線

## 交通運輸

### 一般車輛

木爾坦國際機場坐落在兵營附近，如果要駕車進入便需機場經過卡西姆道。機場的地底有一個停車場，可容納400輛汽車。

### 的士

旅客可透過電台直接從機場電召的士，同時亦提供私人租車服務。

### 三輪車

木尔坦国际机场的停車場和入口也有一些它當地相當流的人力車可供旅客選擇。

### 鐵路

[木尔坦兵營站在機場的](../Page/:en:Multan_Cantonment_railway_station.md "wikilink")3公里範圍內，旅客可乘搭鐵路到各地。

## 統計

下表顯示了木尔坦国际机场的乘客人數、飛機起降架次、貨物以及郵件的流量。數據由[巴基斯坦民航局收集](../Page/巴基斯坦民航局.md "wikilink"):\[1\]

| 年度      | 飛機起降架次 (商業) | 乘客流量 (國際及本地) | 貨運（噸） | 郵件處理（噸） |
| ------- | ----------- | ------------ | ----- | ------- |
| 2006-07 | 3,910       | 240,573      | 1,273 | 49.52   |
| 2007-08 | 3,943       | 250,661      | 932   | 31.966  |
| 2008-09 | 2,474       | 237,358      | 1,006 | 27.59   |

## 參考資料

[Category:巴基斯坦機場](../Category/巴基斯坦機場.md "wikilink")
[Category:旁遮普省](../Category/旁遮普省.md "wikilink")

1.  [Statistical Information of CAA
    Pakistan](http://www.caapakistan.com.pk/AviationStatisticsView.aspx)
     [CAA Pakistan](../Page/CAA_Pakistan.md "wikilink"), assessed 8
    March 2009