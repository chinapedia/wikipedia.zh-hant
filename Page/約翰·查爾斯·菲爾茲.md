**約翰·查爾斯·菲爾茲**（，），[加拿大](../Page/加拿大.md "wikilink")[數學家](../Page/數學家.md "wikilink")。他設立了[菲爾茲獎](../Page/菲爾茲獎.md "wikilink")，頒給成就傑出的數學家。

## 生平

菲爾茲生於[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")[哈密頓市一皮革店東主的家庭](../Page/哈密尔顿_\(安大略省\).md "wikilink")。他在1880年於哈密頓學院（Hamilton
Collegiate
Institute）畢業，1884年於[多倫多大學畢業](../Page/多倫多大學.md "wikilink")，再往[美國](../Page/美國.md "wikilink")[約翰斯·霍普金斯大學深造](../Page/約翰斯·霍普金斯大學.md "wikilink")，1887年獲博士學位。他的博士論文《d<sup>n</sup>y/dx<sup>n</sup>
= x<sup>m</sup>y方程的符號有限解和定積分解》（*Symbolic Finite Solutions and Solutions
by Definite Integrals of the Equation d<sup>n</sup>y/dx<sup>n</sup> =
x<sup>m</sup>y*）在1886年的《[美國數學雜誌](../Page/美國數學雜誌.md "wikilink")》（American
Journal of Mathematics）上刊登。

菲爾茲在[約翰斯·霍普金斯大學任教兩年](../Page/約翰斯·霍普金斯大學.md "wikilink")，隨後轉職[賓夕法尼亞州的](../Page/賓夕法尼亞州.md "wikilink")[阿勒格尼大學](../Page/阿勒格尼大學.md "wikilink")（Allegheny
College）。因為對當時[北美的數學研究環境失望](../Page/北美.md "wikilink")，1891年他前往歐洲，在[柏林](../Page/柏林.md "wikilink")、[巴黎](../Page/巴黎.md "wikilink")、[哥廷根與當時的大數學家共事](../Page/哥廷根.md "wikilink")。這些數學家有[魏爾施特拉斯](../Page/卡尔·魏尔施特拉斯.md "wikilink")、[克萊因](../Page/菲利克斯·克莱因.md "wikilink")、[弗洛比紐斯](../Page/费迪南德·格奥尔格·弗罗贝尼乌斯.md "wikilink")、[普朗克等](../Page/马克斯·普朗克.md "wikilink")。他還與瑞典數學家Gösta
Mittag-Leffler結為世交。他開始在新領域[代數函數論發表論文](../Page/代數函數.md "wikilink")，這是他一生成就最豐的領域。

1902年他返回加拿大，在[多倫多大學任教](../Page/多倫多大學.md "wikilink")。他孜孜不倦地工作，以提升數學在加拿大學術界和公眾的地位。他游說了安大略省議會，提供多倫多大學每年75,000元研究經費，並協助成立加拿大研究委員會。這委員會是[加拿大自然科學及工程研究委員會](../Page/加拿大自然科學及工程研究委員會.md "wikilink")（National
Science and Engineering Research Council of
Canada）和[安大略研究基金會](../Page/安大略研究基金會.md "wikilink")（Ontario
Research
Foundation）前身。從1919年至1925年他出任[加拿大皇家学院主席](../Page/加拿大皇家学院.md "wikilink")，致力使其成為科研的領導中心。雖然不太成功，他的努力卻促使多倫多得以舉辦1924年[國際數學家大會](../Page/國際數學家大會.md "wikilink")。

菲爾茲最為人所知的成就是他設立的菲爾茲獎。這獎項被譽為數學界的[諾貝爾獎](../Page/諾貝爾獎.md "wikilink")，縱使兩者不同。菲爾茲於1920年代末開始籌備這獎項，但因健康日差，生前未能看到獎項成事。他患病三個月後去世，遺囑中捐了47,000元給獎項基金。

菲爾茲分別於1907年和1913年獲選為[加拿大皇家學院和](../Page/加拿大皇家學院.md "wikilink")[倫敦皇家學院院士](../Page/皇家学会.md "wikilink")\[1\]。

## 参考资料

## 深度了解

  -
## 外部链接

  - [Fields Institute
    Biography](http://www.fields.utoronto.ca/aboutus/jcfields/)

  -
  -
[Category:加拿大数学家](../Category/加拿大数学家.md "wikilink")
[Category:約翰·霍普金斯大學校友](../Category/約翰·霍普金斯大學校友.md "wikilink")
[Category:多倫多大學校友](../Category/多倫多大學校友.md "wikilink")
[Category:約翰·霍普金斯大學教授](../Category/約翰·霍普金斯大學教授.md "wikilink")
[Category:多倫多大學教師](../Category/多倫多大學教師.md "wikilink")
[Category:安大略省人](../Category/安大略省人.md "wikilink")
[Category:加拿大皇家学会院士](../Category/加拿大皇家学会院士.md "wikilink")
[Category:英国皇家学会院士](../Category/英国皇家学会院士.md "wikilink")

1.