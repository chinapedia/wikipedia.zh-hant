[Bamboo_Garden_in_Yangzhou.JPG](https://zh.wikipedia.org/wiki/File:Bamboo_Garden_in_Yangzhou.JPG "fig:Bamboo_Garden_in_Yangzhou.JPG")
**-{个}-園**\[1\]，位于[江苏省](../Page/江苏省.md "wikilink")[扬州城内的东关街](../Page/扬州市.md "wikilink")\[2\]，园名源于“月映竹成千-{个}-字”的意象。园内竹石兰桂，景甚美；除竹外，最富盛名者四季假山，以石垒成四季之景，徜徉间即遍历春夏秋冬，是中国十大名园之一。1988年列为第三批[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。

\-{个}-園乃由[嘉慶年間](../Page/嘉慶.md "wikilink")（1818年），[兩淮](../Page/兩淮.md "wikilink")[鹽商](../Page/鹽商.md "wikilink")[黃至筠在明代](../Page/黃至筠.md "wikilink")[壽芝園舊址上重建](../Page/壽芝園.md "wikilink")，同治初年（1862年）賣給[鎮江](../Page/鎮江.md "wikilink")[丹徒鹽商](../Page/丹徒.md "wikilink")[李文安](../Page/李文安.md "wikilink")。後軍閥[徐寶山在揚州掌權](../Page/徐寶山.md "wikilink")，逼迫各富商認捐，金額要徐滿意才准回家。徐寶山覬覦-{个}-園已久，迫使李家認捐鉅額款項，李家無力支付，於是被徐逼用-{个}-園抵債。徐寶山1913年被革命黨炸死，1926年李家讓出-{个}-園，徐氏後-{个}-園又易主蔣氏、朱氏等，直至1949年被收歸國有。李家入主-{个}-園達60年左右，時間最久，其間將-{个}-園逐步完善，乃-{个}-園最完整的時期，大多假山乃以李家船隊的壓艙石所建。

## 历史

清嘉庆二十三年（1818年），[兩淮](../Page/兩淮.md "wikilink")[鹽商](../Page/鹽商.md "wikilink")[黃至筠在明代](../Page/黃至筠.md "wikilink")[壽芝園舊址的基础上将其重建并命名为](../Page/壽芝園.md "wikilink")个园。\[3\]
[Bamboo_garden_zxjc.jpg](https://zh.wikipedia.org/wiki/File:Bamboo_garden_zxjc.jpg "fig:Bamboo_garden_zxjc.jpg")

## 注

<references group="注" />

## 参考来源

  - <small>[段玉裁](../Page/段玉裁.md "wikilink")：《[說文解字注](http://www.guoxuedashi.com/kangxi/pic.php?f=sw&p=775)》，頁775-776
    </small>
  - <small>《[康熙字典](http://www.guoxuedashi.com/kangxi/pic.php?f=kangxi2&p=237)》頁237</small>

## 外部链接

  - [官方网站](http://php.ge-garden.net/index.php)

[Category:扬州文物保护单位](../Category/扬州文物保护单位.md "wikilink")
[Category:扬州园林](../Category/扬州园林.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")
[Category:京杭大运河世界遗产点](../Category/京杭大运河世界遗产点.md "wikilink")

1.  “个”和“箇”為“個”的本字。今使用傳統漢字的地區多用“個”這一字形。个是量词。《[六書本義](../Page/六書本義.md "wikilink")》：“个，竹一枝也。”《[方言](../Page/輶軒使者絕代語釋別國方言.md "wikilink")》：“箇、枚也。”《[說文解字注](../Page/說文解字注.md "wikilink")》：“箇，或作个。半竹也。...竹字象林立之形，一莖則一个也。...竹曰个。木曰枚。”[鄭康成](../Page/鄭康成.md "wikilink")《[儀禮](../Page/儀禮.md "wikilink")》註：“俗呼个爲個。”《[康熙字典](../Page/康熙字典.md "wikilink")》：“個爲後人增加。从个、箇爲正。”

2.

3.