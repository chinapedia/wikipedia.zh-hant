**Windows
3.x家族**是[微软于](../Page/微软.md "wikilink")1990年到1994年间所發行的[Windows系列操作平台](../Page/Windows.md "wikilink")。其中的3.0版是第一个在世界上获得成功的Windows版本。使得微软的[操作系统可以和](../Page/操作系统.md "wikilink")[苹果电脑公司的](../Page/苹果电脑.md "wikilink")[麦金塔电脑以及在](../Page/麦金塔电脑.md "wikilink")[圖形使用者界面的](../Page/圖形使用者界面.md "wikilink")[Commodore的](../Page/康懋达国际.md "wikilink")[Amiga竞争](../Page/Amiga.md "wikilink")。

Windows
3.x需從[MS-DOS操作系统執行](../Page/MS-DOS.md "wikilink")。MS-DOS的文件管理程序被基于图标的程序管理程序（icon-based
program Manager）以及基于列表的文件管理程序（list-based File
Manager，Winfile）取代，由此简化了程序的启动。[控制面板作为系统设置的中心](../Page/控制面板.md "wikilink")，包括了诸如界面颜色主题的有限控制功能。一些捆绑的应用程序也包括在Windows内，例如“写字板”、“计算器”等。

## 沿革

3.0版发行于1990年5月22日，这个系统既包含了对[用户界面的重要改善](../Page/用户界面.md "wikilink")，也包含了对Intel
[80286和](../Page/80286.md "wikilink")[80386对内存管理技术的改进](../Page/80386.md "wikilink")。为命令行式操作系统编写的[MS-DOS下的程序可以在窗口中运行](../Page/MS-DOS.md "wikilink")，使得[程序可以在](../Page/程序.md "wikilink")[多任务基础上可以使用](../Page/多任务.md "wikilink")，虽然这个版本只是为家庭用户设计的，很多游戏和娱乐程序仍然要求[DOS存取](../Page/DOS.md "wikilink")，Windows
3.0是最後一版支援[Intel
8088及](../Page/Intel_8088.md "wikilink")[8086處理器的Windows操作系统](../Page/8086.md "wikilink")。[任务管理器和图标都起源于Windows](../Page/任务管理器.md "wikilink")
3.0。

Windows
3.1发行于1992年3月18日，添加了对声音输入输出的基本[多媒体的支持和一个CD音频播放器](../Page/多媒体.md "wikilink")，以及对[桌面出版很有用的](../Page/桌面出版.md "wikilink")[TrueType字体](../Page/TrueType.md "wikilink")，并开始有了[虚拟内存和](../Page/虚拟内存.md "wikilink")[屏幕保护程序](../Page/屏幕保护程序.md "wikilink")。TrueType取代了曾在Windows
3.0版非常流行的[Adobe Type
Manager](../Page/Adobe_Type_Manager.md "wikilink")（ATM）的市場地位，Windows
3.1是第一版沒有提供真實模式的Windows操作系统。这是微软视窗涉足简体中文的第一个版本，其简体中文版发布于1993年，多基于对英文版的简体中文化，提供了两种常用的输入法——[拼音和GB](../Page/拼音输入法.md "wikilink")2313区位用于录入汉字。

Windows 3.11于不久后发布，作了一些小更新。

1993年微软发行了在Windows 3.1x的基础上支持一些[网络功能的Windows](../Page/网络.md "wikilink")
for Workgroup的3.1和3.11版。除Windows for Workgroups系列外，在Windows
3.x中，[网络功能](../Page/网络.md "wikilink")[TCP/IP依靠于第三方软件](../Page/TCP/IP.md "wikilink")，例如[Trumpet
Winsock](../Page/Trumpet_Winsock.md "wikilink")。

Windows
3.2是微软专为[中国大陆市场开发的视窗版本](../Page/中国大陆.md "wikilink")，于1994年发布，仅提供简体中文版\[1\]。相比Windows
3.1，该版本在图标和界面中文化程度上更加成熟，并且它新加入了[智能ABC和](../Page/智能ABC.md "wikilink")[郑码两种输入法用于录入汉字](../Page/郑码.md "wikilink")。这个版本是当年学校、企事业机关普遍使用的版本，中国大陆的软件开发商也纷纷基于此平台开发出第一代视窗下的软件，例如[WPS视窗版1.0和](../Page/WPS_Office.md "wikilink")[科利华电脑家庭教师](../Page/科利华电脑家庭教师.md "wikilink")。

16位的Windows 3.x最终被32位/16位混合核心的[Windows
95](../Page/Windows_95.md "wikilink")、[98取代](../Page/Windows_98.md "wikilink")。此外，微软还对Windows
3.x开发了一个叫[Win32s的附加组件](../Page/Win32s.md "wikilink")，对[Win32](../Page/Win32.md "wikilink")
[API提供有限的支持](../Page/API.md "wikilink")。

## 藍底白字

Windows藍底白字的畫面，是由前微軟執行長[-{zh-hans:史蒂芬·鲍尔默;zh-hk:鮑爾默;zh-tw:史蒂芬·鮑默;}-](../Page/史蒂夫·巴爾默.md "wikilink")（Steve
Ballmer）親手操刀的，微軟資深工程師「Raymond Chen」在自己的部落格中寫下了整個事件的過程。\[2\]

## 参看

  - [Microsoft Windows的历史](../Page/Microsoft_Windows的历史.md "wikilink")
  - [操作系统](../Page/操作系统.md "wikilink")
  - [操作系统列表](../Page/操作系统列表.md "wikilink")
  - [微軟操作系统列表](../Page/微軟操作系统列表.md "wikilink")
  - [微軟](../Page/微軟.md "wikilink")

## 腳注

[Category:磁盘操作系统](../Category/磁盘操作系统.md "wikilink") [Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink")
[Category:已停止開發的作業系統](../Category/已停止開發的作業系統.md "wikilink")

1.  [中文 (简体的) 3.2 升级的 Microsoft Windows
    可用](https://support.microsoft.com/zh-cn/kb/129451)
2.