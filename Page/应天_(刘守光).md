**应天**（911年八月-913年十一月）是[后梁时期自立为大燕皇帝的](../Page/后梁.md "wikilink")[刘守光的](../Page/刘守光.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|应天||元年||二年||三年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||911年||912年||913年 |-
style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[辛未](../Page/辛未.md "wikilink")||
[壬申](../Page/壬申.md "wikilink")|| [癸酉](../Page/癸酉.md "wikilink") |}

## 参见

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他政权使用的[应天年号](../Page/应天.md "wikilink")
  - 同期存在的其他政权年号
      - [乾化](../Page/乾化.md "wikilink")（911年五月至913年正月、913二月至915年十月）：後梁—朱全忠、[朱友貞之年號](../Page/朱友貞.md "wikilink")
      - [鳳曆](../Page/鳳曆_\(朱友珪\).md "wikilink")（913年正月至二月）：後梁—[朱友珪之年號](../Page/朱友珪.md "wikilink")
      - [天佑](../Page/天祐_\(李存勗\).md "wikilink")（909年至923年）：晉—[李存勗之年號](../Page/李存勗.md "wikilink")
      - [天佑](../Page/天祐_\(李茂貞\).md "wikilink")（907年四月至924年）：[岐](../Page/岐.md "wikilink")—[李茂貞之年號](../Page/李茂貞.md "wikilink")
      - [天祐](../Page/天祐_\(楊行密\).md "wikilink")（904年月至919年）：吳—楊行密、[楊渥之年號](../Page/楊渥.md "wikilink")
      - [天寳](../Page/天宝_\(钱镠\).md "wikilink")（908年正月至912年十二月）：[吳越](../Page/吳越.md "wikilink")—[錢鏐之年號](../Page/錢鏐.md "wikilink")
      - [永平](../Page/永平_\(王建\).md "wikilink")（911年正月至915年十二月）：前蜀—王建之年號
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [聖冊](../Page/聖冊.md "wikilink")（905年—911年）：後高句麗—金弓裔之年號
      - [永德萬歲](../Page/永德萬歲.md "wikilink")（911年—914年）：後高句麗—金弓裔之年號
      - [延喜](../Page/延喜.md "wikilink")（901年-922年）：平安時代[醍醐天皇之年號](../Page/醍醐天皇.md "wikilink")

[Category:五代十国年号](../Category/五代十国年号.md "wikilink")
[Category:10世纪中国年号](../Category/10世纪中国年号.md "wikilink")
[Category:910年代中国政治](../Category/910年代中国政治.md "wikilink")