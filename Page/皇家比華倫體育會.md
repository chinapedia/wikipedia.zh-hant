**皇家比華倫體育會**（[荷兰语](../Page/荷兰语.md "wikilink")：**Koninklijke Sportkring
Beveren**，一般稱為**比華倫**）是比利时足球俱乐部，位于[东弗拉芒省小城](../Page/东弗拉芒省.md "wikilink")[贝弗伦](../Page/贝弗伦.md "wikilink")。俱乐部的守门员学校十分知名，培养出[让-马里·普法夫](../Page/让-马里·普法夫.md "wikilink")、[菲利普·德维尔德](../Page/菲利普·德维尔德.md "wikilink")、[热尔·德弗利格](../Page/热尔·德弗利格.md "wikilink")、[埃尔文·莱门斯和](../Page/埃尔文·莱门斯.md "wikilink")[特里斯坦·皮尔斯曼这样一批出色的](../Page/特里斯坦·皮尔斯曼.md "wikilink")「[比利时国门](../Page/比利时国家足球队.md "wikilink")」，这些球员的职业生涯都起自贝弗伦。

2010年，球會與華斯蘭紅星合併，成為[華斯蘭比華倫](../Page/華斯蘭比華倫.md "wikilink")，意味著原比華倫球會將完成其歷史任務。

## 球队荣誉

  - **[比利时足球甲级联赛](../Page/比利时足球甲级联赛.md "wikilink")：**
      - **冠军（2次）:** 1978-79, 1983-84
  - **[比利时足球乙级联赛](../Page/比利时足球乙级联赛.md "wikilink")：**
      - **冠军（4次）:** 1966-67, 1972-73, 1990-91, 1996-97
  - **[比利时杯](../Page/比利时杯.md "wikilink")：**
      - **冠军（2次）:** 1977-78, 1982-83
      - **亚军（3次）:** 1979-80, 1984-85, 2003-04
  - **[比利时超级杯](../Page/比利时超级杯.md "wikilink")：**
      - **冠军（1次）:** 1984
      - **亚军（2次）:** 1980, 1983
  - [守门员](../Page/守门员.md "wikilink")
    [巴里·布巴卡尔](../Page/巴里·布巴卡尔.md "wikilink")（[科特迪瓦](../Page/科特迪瓦国家足球队.md "wikilink")）和
    [后卫](../Page/后卫_\(足球\).md "wikilink")／[中场](../Page/中场.md "wikilink")
    [考米·阿格布](../Page/考米·阿格布.md "wikilink")（[多哥](../Page/多哥国家足球队.md "wikilink")）参加了[2006年世界杯](../Page/2006年世界杯.md "wikilink")。

## 欧战纪录

  -
    *更新至2006年，3月5日：*

| 赛事                                     | A | B  | C | D | E  | F  | G  |
| -------------------------------------- | - | -- | - | - | -- | -- | -- |
| [欧洲冠军联赛](../Page/欧洲冠军联赛.md "wikilink") | 2 | 6  | 2 | 2 | 2  | 11 | 8  |
| [欧洲优胜者杯](../Page/欧洲优胜者杯.md "wikilink") | 2 | 12 | 6 | 3 | 3  | 17 | 9  |
| [欧洲联盟杯](../Page/欧洲联盟杯.md "wikilink")   | 4 | 22 | 9 | 3 | 10 | 30 | 30 |
| [国际托托杯](../Page/国际托托杯.md "wikilink")   | 1 | 4  | 1 | 1 | 2  | 6  | 8  |

A = 参赛次数，B = 比赛场次，C = 胜，D = 平，E = 负，F = 进球，G = 失球

## 参考资料

  -   [Official website](http://www.kskbeveren.be/)

  - [UEFA
    page](http://www.uefa.com/footballEurope/Club=52764/domestic.html)

[Category:比利时足球俱乐部](../Category/比利时足球俱乐部.md "wikilink")
[Category:1934年建立的足球俱樂部](../Category/1934年建立的足球俱樂部.md "wikilink")