《**Nakayoshi**》（）是[日本](../Page/日本.md "wikilink")[講談社發行的月刊](../Page/講談社.md "wikilink")[少女漫畫](../Page/少女漫畫.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")。1954年12月創刊（1955年1月號）。《Nakayoshi》是日本三大少女漫畫雜誌之一。主要競爭對手是[集英社的](../Page/集英社.md "wikilink")《[Ribon](../Page/Ribon.md "wikilink")》和[小學館的](../Page/小學館.md "wikilink")《[Ciao](../Page/Ciao.md "wikilink")》。該雜誌的名字在[日語中意為](../Page/日語.md "wikilink")「**好朋友**」。

《Nakayoshi》的讀者對象和《Ribon》相似，即[小學中學年至](../Page/小學.md "wikilink")[初中](../Page/初中.md "wikilink")3年的少女，另外也不乏[高中以上的讀者](../Page/高級中學.md "wikilink")（以[漫畫愛好者居多](../Page/漫畫.md "wikilink")）。

## 歷史

2004年12月迎來創刊50周年。《Nakayoshi》為日本現存漫畫雜誌中歷史最久的刊物。 2014年12月迎來創刊60周年。

## 發行

1990年代初發行量開始增長。1994年《[美少女戰士](../Page/美少女戰士.md "wikilink")》系列連載時為鼎盛期，發行数達到200萬冊。1995年以後行銷量大幅度縮減，2006年末發行量為42萬冊。

## 主要連載作品

### 過去

  - 《[藍寶石王子](../Page/藍寶石王子.md "wikilink")》（[手塚治虫](../Page/手塚治虫.md "wikilink")，本作被認為是史上首次採用模式的少女向漫畫作品）
  - 《[甜甜小公主](../Page/甜甜小公主.md "wikilink")》 (倉金章介)
  - 《[天使之丘](../Page/天使之丘.md "wikilink")》（[手塚治虫](../Page/手塚治虫.md "wikilink")）
  - 《[小甜甜](../Page/小甜甜.md "wikilink")》(五十嵐優美子、[水木杏子](../Page/名木田惠子.md "wikilink"))
  - 《[TAMA-TAN](../Page/TAMA-TAN.md "wikilink")》（[小夏鈍帆](../Page/小夏鈍帆.md "wikilink")）
  - 《[秋葉原電腦組](../Page/秋葉原電腦組.md "wikilink")》（）
  - 《[同班同學](../Page/小紅豆.md "wikilink")》（[木村千歌](../Page/木村千歌.md "wikilink")）
  - 《[奇蹟女孩](../Page/奇蹟女孩.md "wikilink")》（[秋元奈美](../Page/秋元奈美.md "wikilink")）
  - 《[製造王子計劃](../Page/王子育成法.md "wikilink")》（桃雪琴梨）
  - 《[美少女戰士](../Page/美少女戰士.md "wikilink")》（[武內直子](../Page/武內直子.md "wikilink")）
  - 《[魔法騎士](../Page/魔法騎士.md "wikilink")》（CLAMP）
  - 《[怪盜St.
    Tail](../Page/怪盜St._Tail.md "wikilink")》（[立川惠](../Page/立川惠.md "wikilink")）
  - 《[金魚注意報](../Page/金魚注意報.md "wikilink")》（[猫部猫](../Page/猫部猫.md "wikilink")）
  - 《[Da\!Da\!Da\!](../Page/Da!Da!Da!.md "wikilink")》（[川村美香](../Page/川村美香.md "wikilink")）
  - 《[優女夢遊仙境](../Page/優女夢遊仙境.md "wikilink")》（花森小桃）
  - 《[麗佳公主](../Page/麗佳公主.md "wikilink")》（征海未亞）
  - 《[夢之蠟筆王國](../Page/夢之蠟筆王國.md "wikilink")》
  - 《[夢幻傳說](../Page/夢幻傳說.md "wikilink")》（[立川惠](../Page/立川惠.md "wikilink")）
  - 《[小魔女Doremi](../Page/小魔女Doremi.md "wikilink")》（[高梨靜江](../Page/高梨靜江.md "wikilink")）
  - 《[庫洛魔法使](../Page/庫洛魔法使.md "wikilink")》（[CLAMP](../Page/CLAMP.md "wikilink")）
  - 《[東京喵喵](../Page/東京喵喵.md "wikilink")》（[征海未亞](../Page/征海未亞.md "wikilink")）
  - 《[點心公主](../Page/點心公主.md "wikilink")》/《[美食小公主](../Page/美食小公主.md "wikilink")》([小林深雪](../Page/小林深雪.md "wikilink")、[安藤夏美](../Page/安藤夏美.md "wikilink"))
  - 《[魔女的考驗](../Page/魔女的考驗.md "wikilink")》/《[魔界女王候補生](../Page/魔界女王候補生.md "wikilink")》（[安野夢洋子](../Page/安野夢洋子.md "wikilink")）
  - 《[真珠美人鱼](../Page/真珠美人鱼.md "wikilink")》（[花森小桃](../Page/花森小桃.md "wikilink")）
  - 《[小女神花鈴Chu](../Page/花鈴的魔法戒.md "wikilink")》（[小夏鈍帆](../Page/小夏鈍帆.md "wikilink")）
  - 《[純愛粉色系](../Page/桃色純愛.md "wikilink")》（[桃雪琴梨](../Page/桃雪琴梨.md "wikilink")）
  - 《[守護甜心\!](../Page/守護甜心!.md "wikilink")》（[PEACH-PIT](../Page/PEACH-PIT.md "wikilink")）
  - 《[妖界領航員.露娜](../Page/妖界領航員.露娜.md "wikilink")》（[菊田珠智代](../Page/菊田珠智代.md "wikilink")）
  - 《[地狱少女](../Page/地狱少女.md "wikilink")》（[永遠幸](../Page/永遠幸.md "wikilink")）
  - 《[ARISA～雙子迷情～](../Page/ARISA～雙子迷情～.md "wikilink")》（[安藤夏美](../Page/安藤夏美.md "wikilink")）
  - 《[要你對我×××！](../Page/要你對我×××！.md "wikilink")》（[遠山繪麻](../Page/遠山繪麻.md "wikilink")）
  - 《[百鬼戀亂](../Page/百鬼戀亂.md "wikilink")》（[鳥海佩卓](../Page/鳥海佩卓.md "wikilink")/）
  - 《[戀愛雙子與青君的眼鏡](../Page/戀愛雙子與青君的眼鏡.md "wikilink")》（[山田Daisy](../Page/山田Daisy.md "wikilink")）
  - 《[魔導少年
    藍色季風](../Page/魔導少年_藍色季風.md "wikilink")》（[渡辺留衣](../Page/渡辺留衣.md "wikilink")、原作：[真島浩](../Page/真島浩.md "wikilink")）

### 現在

  - 《[光之美少女系列](../Page/光之美少女系列.md "wikilink")》（[上北雙子](../Page/上北雙子.md "wikilink")、原作：東堂泉）
  - 《[出口為零](../Page/出口為零.md "wikilink")》（[瀬田春日](../Page/瀬田春日.md "wikilink")）
  - 《[生存遊戲社](../Page/生存遊戲社.md "wikilink")》（[松本ひで吉](../Page/松本ひで吉.md "wikilink")）
  - 《[小學生的秘密](../Page/小學生的秘密.md "wikilink")》（[中江美佳世](../Page/中江美佳世.md "wikilink")）
  - 《[玩偶騎士防衛隊](../Page/玩偶騎士防衛隊.md "wikilink")》（[星野莉莉](../Page/星野莉莉.md "wikilink")）
  - 《[青葉同學請告訴我](../Page/青葉同學請告訴我.md "wikilink")》（[遠山繪麻](../Page/遠山繪麻.md "wikilink")）
  - 《[庫洛魔法使
    透明牌篇](../Page/庫洛魔法使_透明牌篇.md "wikilink")》（[CLAMP](../Page/CLAMP.md "wikilink")）

## 外部連結

  - [官方網站](http://kc.kodansha.co.jp/nakayosi)

[\*](../Category/Nakayoshi.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少女漫畫雜誌](../Category/少女漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[Category:講談社的漫畫雜誌](../Category/講談社的漫畫雜誌.md "wikilink")