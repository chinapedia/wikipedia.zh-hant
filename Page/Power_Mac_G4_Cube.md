**Power Mac G4 Cube**（也稱為：**Power Macintosh G4
Cube**）是[蘋果電腦公司所推出的一部](../Page/苹果电脑.md "wikilink")[麦金托什](../Page/麦金塔电脑.md "wikilink")[个人电脑](../Page/个人电脑.md "wikilink")，這部個人電腦標榜寧靜（因該機完全不用電動風扇）、外觀與構型紮實緊緻，該機款發表於2000年8月，並於2001年7月停售\[1\]。

## 特點

Power Mac G4 Cube（以下簡稱：G4 Cube）的本體僅是個8 x 8 x
8[英吋的](../Page/英吋.md "wikilink")[立方體](../Page/立方體.md "wikilink")，且置於10英吋高的[透明合成樹脂](../Page/聚甲基丙烯酸甲酯.md "wikilink")材質的護殼內。機內所用的[微處理器為](../Page/微處理器.md "wikilink")[Motorola公司的](../Page/摩托罗拉.md "wikilink")[PowerPC
G4](../Page/PowerPC_G4.md "wikilink")，運作時脈為450[MHz或](../Page/MHz.md "wikilink")500MHz。G4
Cube在[光驱的設計上一反傳統](../Page/光盘驱动器.md "wikilink")，捨棄常見常用的托盤式入片法（Tray
Loading），而改採槽縫式（吸入式）入片法（Slot Loading），而且採垂直90度的方式入片，有別於過往的水平0度式入片。G4
Cube用的是一部[DVD-ROM光碟機](../Page/DVD-ROM.md "wikilink")。

[Powermac_g4_lcd.png](https://zh.wikipedia.org/wiki/File:Powermac_g4_lcd.png "fig:Powermac_g4_lcd.png")
在顯示器方面，G4
Cube可選擇搭配[平板薄型的數位液晶顯示器](../Page/薄膜電晶體液晶顯示器.md "wikilink")（TFT-LCD），也可選擇搭配傳統[VGA的](../Page/VGA.md "wikilink")[阴极射线管顯示器](../Page/阴极射线管.md "wikilink")（CRT）。電腦機身與顯示器分離的設計在[IBM
PC兼容機的市場中相當常見](../Page/IBM_PC兼容机.md "wikilink")，但對麥金塔電腦而言則稍有特別。對麥金塔電腦而言，凡是平價訴求的機種多半採行整機一體（All-In-One）的設計，即是顯示器與電腦機身為一體成形，如1983年的[LISA](../Page/Apple_LISA.md "wikilink")、1995年的[Performa](../Page/Performa.md "wikilink")、1998年的[iMac等](../Page/iMac.md "wikilink")；然而G4
Cube則屬例外。

G4 Cube另外一個不同於iMac系列的是，G4
Cube機內具有標準的[AGP插槽](../Page/AGP.md "wikilink")（2X的介面傳輸率、可放置全長尺寸的顯示卡），如此日後仍可升級換裝新的显卡。此外G4
Cube具有2個[FireWire埠與](../Page/IEEE_1394.md "wikilink")2個[USB埠](../Page/通用串行总线.md "wikilink")，以此來擴充、連接週邊裝置。在音源輸出方面，G4
Cube本體並不具備[喇叭](../Page/揚聲器.md "wikilink")，必須通过USB埠連接外部的音效放大器，再連接喇叭，才能發聲。G4
Cube配屬的是一對[Harman
Kardon的喇叭](../Page/Harman_Kardon.md "wikilink")。USB型的音效放大器上也設有一個頭戴式耳機的標準型迷你音源接孔，不過卻缺乏音源輸入功能。

更重要的，G4 Cube全機沒有使用任何一個散熱風扇，全機完全倚賴[對流原理散熱](../Page/對流.md "wikilink")，使G4
Cube運作時相當寧靜。相近時間第二代的iMac G3電腦也採行槽縫式入片的光碟機與無風扇設計。

## 設計

[PowerMac_Cube.jpg](https://zh.wikipedia.org/wiki/File:PowerMac_Cube.jpg "fig:PowerMac_Cube.jpg")
G4 Cube的外觀造型出自蘋果電腦的工業設計師：[乔纳森·埃维](../Page/乔纳森·埃维.md "wikilink")（Jonathan
Ive），G4 Cube的作品使他贏得了多項國際性的工業設計獎，同時G4
Cube也出現在許多設計相關的報導中，以及成為一些科技博物館的館藏（館內收藏）。另外美國喜劇電視影集：《[The
Drew Carey Show](../Page/The_Drew_Carey_Show.md "wikilink")》
中，劇中的人物：Drew Carey是個**麥克迷**，他在片中所用的電腦正是G4 Cube。

## 歷史及銷售

蘋果電腦對G4 Cube的市場設定是介於[iMac](../Page/iMac.md "wikilink") G3與[Power
Macintosh G4間](../Page/Power_Macintosh_G4.md "wikilink")，雖然G4
Cube設計創新，然而卻有不少人抱怨它的售價過高，G4 Cube的初始價格就比更具擴充性的Power Mac
G4貴上200美元（1,799美元），但兩機的規格卻相近，皆為450MHz的處理器、64MB容量的記憶體、20GB的硬碟、顯示器價格另計，以上原因致使G4
Cube的銷售量增加緩慢。更麻煩的是，G4 Cube在初期出貨中發生了製造瑕疵問題，其透明塑膠部分出現了龜裂的隙痕。

在銷售反應不佳後，蘋果電腦嘗試用增加隨附軟體的方式來刺激銷售，並且推出更低價格的基礎配備型，基礎配備型的G4
Cube將DVD-ROM光碟機換裝成[CD-RW光碟機](../Page/CD-RW.md "wikilink")、將處理器提升至500MHz時脈、並增加新的選購項，可選購使用[NVIDIA](../Page/NVIDIA.md "wikilink")[繪圖晶片的顯示卡](../Page/GPU.md "wikilink")。不過這些調整與修正依舊無法改變G4
Cube發表之初在「價格配備比」上不如iMac與Power Mac G4的印象。

到了2001年7月3日，蘋果電腦發表了簡短、輕描、且不易看出意涵的新聞稿\[2\]，發表中表示蘋果電腦把G4
Cube「放到冰上」（英文原文寫著"on
ice"），儘管這個發佈不算是「正式的停產聲明」，但從某種角度看確屬無聲性的產品停休宣告。

此消息一佈達後，蘋果電腦公司過去因iMac
G3機種暢銷所揚升、飆高的股價自此滑跌，跌到了iMac尚未發表前的低點，此一跌落即使之後蘋果電腦發表[iPod](../Page/iPod.md "wikilink")
MP3播放器（數位隨身聽）都無法回補、恢復。值得注意的是，蘋果電腦的發表說明中意有所指，表示日後將可能推出更短小輕薄的機種來接替G4
Cube，而這應當就是之後推出的[Mac mini](../Page/Mac_mini.md "wikilink")，Mac
mini比G4 Cube更輕小，且與G4 Cube一樣採行「與顯示器分立的獨立機身」設計。此若屬實，則「G4 Cube停休、Mac
mini接手」等於是「[Newton](../Page/Apple_Newton.md "wikilink")
停休、[iPhone](../Page/iPhone.md "wikilink") 接手」的重演、翻版。

在隱性停休宣言後的兩年（2003年），由於[Wired雜誌一系列的專文報導使G](../Page/Wired雜誌.md "wikilink")4
Cube再次受到些許關注，文中內容主要在介紹個別消費者、零售店家（如[Kemplar公司](../Page/Kemplar公司.md "wikilink")）對G4
Cube的升級安裝，使的G4 Cube的續產復銷價值突然陡升。不過，隨著平價訴求的Mac mini發表，使G4 Cube再次黯淡退讓。

## 不便之處

由於G4 Cube基本上是極力爭取外觀視覺上的吸引力，但也因奪目的設計導致它有數項實效運用上的限制，其中最受人詬病的是排熱，一旦排熱口遭封堵G4
Cube就會開始過熱並自動關機，且很不幸的，排熱口正好位在G4
Cube的上端，而機頂的平直設計使人很直覺且容易地將書或紙張放於其上，如此就形成了排熱口封堵的情況。

再者，G4
Cube的接線與其他麥金塔電腦相較並沒有收斂減少，這意味著電腦下方或周遭各處將容易有線路糾結的困擾，此有違「線路簡潔」的風氣潮流，加上有些製造商推出G4
Cube所用的升級機內升級組件，如處理器、光碟機等，但是G4 Cube機內的空間非常狹窄，使機內升級的程序相當繁瑣、困難。

## 改裝與升級

在G4 Cube停產後有一群狂熱迷將他們的G4
Cube進行改裝，其中較常見的是將顯示卡升級，換裝成具有風扇散熱的顯示卡，然後再運用通風管的設置，使升級的顯示卡依然能在G4
Cube的狹窄空間內正常運轉，而顯示卡升級中又以升級成[NVIDIA公司的](../Page/NVIDIA.md "wikilink")[GeForce
2
MX繪圖晶片層級的為多](../Page/GeForce_2#GeForce_2_MX.md "wikilink")，此款繪圖晶片可說是特別為G4
Cube升級而推出的。

此外也有協力業者推出處理器升級卡，至2005年8月為止可換裝到1.8GHz的PowerPC G4處理器，甚至有少數的人將他們的G4
Cube改裝成雙處理器的系統。其餘也有人進行[機殼改裝](../Page/機殼改裝.md "wikilink")，改裝成具有燈光或額外散熱機制等等。\[3\]

## 規格

  - 處理器
      - 編號：[PowerPC 7400](../Page/PowerPC_G4.md "wikilink")
      - 時脈：450MHz或500MHz
      - [浮點運算器](../Page/浮點運算器.md "wikilink")：內建
      - 第一階快取記憶體：32KB（資料）、32KB（指令）
      - 第二階快取記憶體：1MB（位在背端，與處理器時脈比為1：2）

<!-- end list -->

  - RAM記憶體
      - 時脈：100MHz
      - 寬度：64-bit
      - 型態：PC-100
      - 最大記憶容量：1.5GB
      - 記憶體模組插槽：3組
      - 內建於主機板上的容量：無

<!-- end list -->

  - ROM記憶體
      - 組態：1MB＋3MB toolbox ROM（工具箱，開機後載入到RAM記憶體中）

<!-- end list -->

  - 主機板
      - [AGP插槽](../Page/AGP.md "wikilink")：1組（2X傳輸，出廠前已用佔）

<!-- end list -->

  - 视频
      - 顯示卡／[繪圖晶片](../Page/GPU.md "wikilink")：[ATI公司的](../Page/ATI技術公司.md "wikilink")[Rage
        128 Pro](../Page/Rage_128_Pro.md "wikilink")
      - 視訊記憶體：16MB
      - 視訊輸出：[VGA](../Page/VGA.md "wikilink")（Video Graphics
        Array）、[DVI](../Page/DVI.md "wikilink")（Digital Visual
        Interface）、[ADC](../Page/ADC.md "wikilink")（Apple Display
        Connector）

<!-- end list -->

  - 儲存
      - [硬碟](../Page/硬碟.md "wikilink")：20GB或30GB或60GB
      - 硬碟介面：[UltraATA/66](../Page/ATA.md "wikilink")
      - [光碟](../Page/光碟.md "wikilink")：[DVD-ROM或](../Page/DVD.md "wikilink")[CD-RW](../Page/CD-RW.md "wikilink")

<!-- end list -->

  - 輸入／輸出
      - [USB埠](../Page/USB.md "wikilink")：2組
      - FireWire（[IEEE 1394](../Page/IEEE_1394.md "wikilink")）埠：2組
      - 音效輸出：[雙聲道](../Page/雙聲道.md "wikilink")、16-bit[解析度](../Page/解析度.md "wikilink")、迷你型接孔（透過USB介面的音效放大器）

<!-- end list -->

  - 網路
      - 數據機速率：56kbps
      - [乙太網路](../Page/以太网.md "wikilink")：10/100Base-T
      - [AirPort](../Page/AirPort.md "wikilink")（[WiFi](../Page/WiFi.md "wikilink")、IEEE
        802.11b）：選用

<!-- end list -->

  - 其他
      - 研發代號：Trinity
      - 型款編號：406
      - 供電瓦數：200[瓦](../Page/瓦特.md "wikilink")
      - 實體尺寸：10英吋（高）x 8英吋（寬）x 8英吋（深）
      - 重量：28.7英磅
      - 作業系統最低要求：[Mac OS 9.0.4](../Page/Mac_OS_9.md "wikilink")
      - 作業系統最後支援：[Mac OS X
        10.4.11](../Page/Mac_OS_X_Tiger.md "wikilink") 及 Mac OS 9.2.2

## 附註

1.  此種材質也俗稱：**壓克力**。

2.  光碟入片法主要有三種：護匣法（Caddy Loading）、托盤法（Tray Loading）、槽縫法（Slot
    Loading），最早出現的是護匣法，但因成本過高而少使用，目前仍用在部分類型的光碟片中，如[MD](../Page/MiniDisc.md "wikilink")／[Hi-MD](../Page/Hi-MD.md "wikilink")／[MD
    Data](../Page/MD_Data.md "wikilink")、[DVD-RAM](../Page/DVD-RAM.md "wikilink")、[UDO](../Page/UDO.md "wikilink")、[PDD](../Page/PDD.md "wikilink")、[UMD等](../Page/UMD.md "wikilink")，目前的大宗主流為托盤法，最晚推出的是槽縫法，不過未能流行普及，且不適合小直徑光碟的入片。

3.  因為立方體的外觀設計再加上光碟片自機體頂端垂直進出，也因此該機被人取了個綽號，叫**[烤麵包機](../Page/烤麵包機.md "wikilink")**（或**冰塊**），因為能烘烤片狀吐司的烤麵包機也是讓吐司自上端垂直進出。事實上幾乎每部蘋果電腦的外觀都有過戲稱，如[iBook被稱為](../Page/iBook.md "wikilink")**馬桶蓋**、後一代iBook被稱為**麻將粒**、iMac
    G3被稱為**安全帽**（或**軟糖**）、iMac G4被稱為**檯燈**、iMac G5被稱為**相片框**、Mac
    mini被稱為**便當盒**、iPod Shuffle被稱為**口香糖**等等。

4.  無風扇設計有其動機，由於蘋果電腦的用戶有許多是從事內容創作、設計的工作，為了靈感經常在深夜使用電腦，深夜中電腦風扇的噪音格外明顯與刺耳，反而影響了靈感與工作心情，因此期望能有更寧靜的散熱設計，此外風扇運轉需要[軸承的機械組件配合](../Page/軸承.md "wikilink")，機械組件的使用壽限多低於電子組件，此也成為電腦維修保養上的經常困擾。

5.  原文是英文的****，意思是蘋果麥金塔系列電腦的忠實用戶、忠實愛用者。

6.  由於使用PowerPC G5處理器的麥金塔電腦在成本上過高，在平價版G5機種遲遲無法推出的情況下，蘋果電腦改推出Mac
    mini以補足平價市場的新機需求，Mac mini機內用的處理器為[PowerPC
    G4處理器](../Page/PowerPC_G4.md "wikilink")，之後也換成Intel x86處理器。

## 參考引據

<references/>

## 相關參見

  - 同業者的相關機種

| 條目                                         | 說明                                                                               |
| ------------------------------------------ | -------------------------------------------------------------------------------- |
| [Mac mini](../Page/Mac_mini.md "wikilink") | 某種程度上可視為**Power Mac G4 Cube**的接替機種。 |-                                           |
| [Mac Pro](../Page/Mac_Pro.md "wikilink")   | [2013年6月](../Page/2013年6月.md "wikilink") 發表的 Mac Pro 的外觀與Power Macintosh G5外型相似。 |

  - 相似形體的資訊硬體

| 條目                                               | 說明                                                                                                                                                                                                    |
| ------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [GameCube](../Page/GameCube.md "wikilink")       | [日本](../Page/日本.md "wikilink")[任天堂公司所推出的立方體造型](../Page/任天堂.md "wikilink")[电子游戏机](../Page/游戏机.md "wikilink")。 |-                                                                                       |
| [Cobalt Qube](../Page/Cobalt_Qube.md "wikilink") | [美國](../Page/美國.md "wikilink")[Cobalt Networks公司](../Page/Cobalt_Networks.md "wikilink")（之後由[Sun公司收併](../Page/昇陽電腦.md "wikilink")）推出的藍色立方體型[伺服應用機](../Page/伺服應用機.md "wikilink")（Server Appliance）。 |- |

## 外部連結

  - [Apple-History.com網站上對Power Mac G4
    Cube的介紹](http://www.apple-history.com/frames/body.php?page=gallery&model=g4cube)

  - [G4 Cube用戶的入口網站（Cube Owner
    Portal）](https://web.archive.org/web/20050107021129/http://www.cubeowner.com/)

  - [G4
    Cube的特選圖片集](https://web.archive.org/web/20050401051840/http://www.cubeowner.com/photopost/)

  - [過去G4 Cube銷售時的電視廣告（QuickTime的
    .mov格式）](https://web.archive.org/web/20051219075743/http://pulsar.esm.psu.edu/Faculty/Gray/graphics/movies/cube_T1.mov)

  - [G4
    Cube的技術規格與升級信息](https://web.archive.org/web/20070303114949/http://www.g4cube.com/)


[Category:麦金塔](../Category/麦金塔.md "wikilink")

1.  銷售起迄時間參考自[Apple-History.com](http://www.apple-history.com)網站。
2.  停產停售聲明參考自蘋果電腦官方網站的[該日新聞稿](http://www.apple.com/pr/library/2001/jul/03cube.html)

3.  主體說明參考自英文系維基百科的相同條目