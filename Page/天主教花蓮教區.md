**天主教花蓮教區**是[天主教會在](../Page/天主教會.md "wikilink")[台灣](../Page/台灣.md "wikilink")[東部設置的](../Page/東台灣.md "wikilink")[教區](../Page/教區.md "wikilink")，成立於1963年，涵蓋範圍包括[花蓮縣](../Page/花蓮縣.md "wikilink")、[台東縣等東部](../Page/台東縣.md "wikilink")[縣市](../Page/中華民國行政區劃.md "wikilink")。現任花蓮[主教為](../Page/主教.md "wikilink")[黃兆明主教](../Page/黃兆明.md "wikilink")，[輔理主教為](../Page/輔理主教.md "wikilink")[曾建次主教](../Page/曾建次.md "wikilink")，教區內共有約55位[神父及](../Page/神父.md "wikilink")120位[修女服務](../Page/修女.md "wikilink")。由於[東台灣的](../Page/東台灣.md "wikilink")[原住民比例較高](../Page/臺灣原住民.md "wikilink")，故本教區的教友以原住民居多。

## 沿革

1949年12月31日，[教廷將台灣的天主教會劃為](../Page/羅馬教廷.md "wikilink")[台北監牧區](../Page/天主教台北總教區.md "wikilink")、[高雄監牧區兩大管區](../Page/天主教高雄教區.md "wikilink")，台灣東部分屬兩個監牧區管轄。1952年8月7日，台灣的[聖統制度正式建立](../Page/聖統制.md "wikilink")，[教廷將東部地區的](../Page/教廷.md "wikilink")[花蓮縣及](../Page/花蓮縣.md "wikilink")[台東縣獨立出來](../Page/台東縣.md "wikilink")，改設置**花蓮監牧區**，由[法籍的](../Page/法國.md "wikilink")[營口教區主教](../Page/營口.md "wikilink")[費聲遠擔任](../Page/費聲遠.md "wikilink")[宗座監牧](../Page/宗座監牧.md "wikilink")。1963年7月15日，教廷正式將花蓮監牧區升格為正式教區，原任監牧的費聲遠主教成為教區首任主教，領署理之銜。**花蓮教區**正式成立。

在歷任主教及[巴黎外方傳教會等外籍](../Page/巴黎外方傳教會.md "wikilink")[修會的努力之下](../Page/修會.md "wikilink")，使許多[原住民領洗成為教友](../Page/臺灣原住民.md "wikilink")，更讓[東台灣成為全台少數天主教徒占人口比例較高的地區之一](../Page/東台灣.md "wikilink")。1998年5月22日，[教宗](../Page/教宗.md "wikilink")[若望保祿二世任命](../Page/若望保祿二世.md "wikilink")[卑南族的](../Page/卑南族.md "wikilink")[曾建次神父擔任花蓮教區](../Page/曾建次.md "wikilink")[輔理主教](../Page/輔理主教.md "wikilink")，同年8月29日正式就職，成為第一位[原住民族籍的主教](../Page/臺灣原住民.md "wikilink")。

## 堂區

花蓮教區目前共管轄47個[堂區](../Page/堂區.md "wikilink")、143個兼管區（非獨立堂區）。此外，整個教區另劃分為10個[總鐸區](../Page/總鐸區.md "wikilink")（Deanery）與牧靈區，其中總鐸區由區內各堂區的主任司鐸（[Pastor](../Page/牧师.md "wikilink")）之中選出一位[總鐸](../Page/總鐸.md "wikilink")，以利於教務管理，並作為各堂區與教區之間的連繫橋樑。

### 花蓮市總鐸區

  - 進教之佑主教座堂（[花蓮縣](../Page/花蓮縣.md "wikilink")[花蓮市](../Page/花蓮市.md "wikilink")；美崙天主堂）
  - 耶穌聖心堂（花蓮縣花蓮市；北濱天主堂）
  - 聖方濟沙勿略堂（花蓮縣花蓮市；佐倉天主堂）
  - 聖保祿堂（花蓮縣花蓮市；民國路天主堂）
  - 聖方濟亞西西天主堂（花蓮縣花蓮市；德安天主堂）
  - 聖母聖心堂（花蓮縣[吉安鄉](../Page/吉安鄉.md "wikilink")；田埔天主堂）
  - 基督君王堂（花蓮縣[壽豐鄉](../Page/壽豐鄉.md "wikilink")；壽豐天主堂）

### 秀林總鐸區

  - 聖母元后堂（花蓮縣[新城鄉](../Page/新城鄉_\(台灣\).md "wikilink")；新城天主堂）
  - 聖若瑟堂（花蓮縣新城鄉；嘉里天主堂）
  - 聖母升天堂（花蓮縣[秀林鄉](../Page/秀林鄉.md "wikilink")；秀林天主堂）
  - 聖彌額爾堂（花蓮縣秀林鄉；銅門天主堂）
  - 聖伯納堂（花蓮縣秀林鄉；崇德天主堂）

### 光復總鐸區

  - 耶穌聖心堂（花蓮縣[鳳林鎮](../Page/鳳林鎮_\(台灣\).md "wikilink")；鳳林天主堂）
  - 聖母諸惠中保堂（花蓮縣[光復鄉](../Page/光復鄉.md "wikilink")；富田天主堂）
  - 聖保祿歸化堂（花蓮縣光復鄉；馬太鞍天主堂）

### 玉里牧靈區

  - 耶穌君王堂（花蓮縣[瑞穗鄉](../Page/瑞穗鄉.md "wikilink")；瑞穗天主堂）
  - 吾樂聖母堂（花蓮縣瑞穗鄉；富民天主堂）
  - 七苦聖母堂（花蓮縣[萬榮鄉](../Page/萬榮鄉.md "wikilink")；馬遠天主堂）
  - 天主聖母堂（花蓮縣萬榮鄉；萬榮天主堂）
  - 聖女德蘭堂（花蓮縣萬榮鄉；紅葉天主堂）
  - 聖母玫瑰堂（花蓮縣[玉里鎮](../Page/玉里鎮.md "wikilink")；[玉里天主堂](../Page/玉里天主堂.md "wikilink")）
  - 聖母領報堂（花蓮縣玉里鎮；春日天主堂）
  - 露德聖母堂（花蓮縣玉里鎮；[東豐天主堂](../Page/東豐天主堂.md "wikilink")）
  - 露德聖母堂（花蓮縣[卓溪鄉](../Page/卓溪鄉.md "wikilink")；卓溪天主堂）
  - 聖安德堂（花蓮縣[富里鄉](../Page/富里鄉.md "wikilink")；富里天主堂）

### 濱海牧靈區

  - 聖母堂（花蓮縣[豐濱鄉](../Page/豐濱鄉.md "wikilink")；豐濱天主堂）
  - 聖家堂（台東縣[長濱鄉](../Page/長濱鄉.md "wikilink")；長濱天主堂）
  - 聖若望堂（台東縣[蘭嶼鄉](../Page/蘭嶼鄉.md "wikilink")；紅頭天主堂）

### 台東市總鐸區

  - 中華殉教烈士堂（[台東縣](../Page/臺東縣.md "wikilink")[台東市](../Page/臺東市.md "wikilink")；福建路天主堂）
  - 宗徒之后堂（台東縣台東市；寶桑路天主堂）
  - 聖若瑟堂（台東縣台東市；馬蘭天主堂）

### 卑南牧靈區

  - 聖德範堂（台東縣台東市；南王天主堂）
  - 聖母無原罪堂（台東縣台東市；知本天主堂）
  - 東興天主堂（台東縣[卑南鄉](../Page/卑南鄉.md "wikilink")）
  - 初鹿天主堂（台東縣卑南鄉）

### 關山牧靈區

  - 救世主堂（台東縣[關山鎮](../Page/關山鎮_\(台灣\).md "wikilink")；鹿野天主堂）
  - 聖保祿堂（台東縣[延平鄉](../Page/延平鄉.md "wikilink")；桃源天主堂）
  - 聖母堂（台東縣關山鎮；關山天主堂）
  - 善牧堂（台東縣[池上鄉](../Page/池上鄉.md "wikilink")；池上天主堂）

### 成功牧靈區

  - 耶穌君王堂（台東縣[東河鄉](../Page/東河鄉_\(台灣\).md "wikilink")；[都蘭天主堂](../Page/都蘭_\(台東縣\).md "wikilink")）
  - 聖嘉俾額爾堂（台東縣東河鄉；東河天主堂）
  - 小德蘭堂（台東縣東河鄉；泰源天主堂）
  - 中華聖母堂（台東縣[成功鎮](../Page/成功鎮.md "wikilink")；成功天主堂）
  - 和平之后堂（台東縣成功鎮；宜灣天主堂）

### 大武牧靈區

  - 聖加祿堂（台東縣[太麻里鄉](../Page/太麻里鄉.md "wikilink")；太麻里天主堂）
  - 聖若瑟堂（台東縣太麻里鄉；金崙天主堂）
  - 聖母堂（台東縣[大武鄉](../Page/大武鄉_\(臺灣\).md "wikilink")；大武天主堂）

## 歷任主教列表

<table style="width:127%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 16%" />
<col style="width: 44%" />
<col style="width: 37%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div align="center">
<p><strong>任</strong></p>
</div></td>
<td><div align="center">
<p><strong>中文姓名</strong></p>
</div></td>
<td><div align="center">
<p><strong>英文姓名</strong></p>
</div></td>
<td><div align="center">
<p><strong>任期</strong></p>
</div></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><a href="../Page/費聲遠.md" title="wikilink">費聲遠</a><a href="../Page/主教.md" title="wikilink">主教</a></p></td>
<td></td>
<td><p>1963年7月15日－1973年7月15日<small>（署理）</small></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><a href="../Page/賈彥文.md" title="wikilink">賈彥文主教</a></p></td>
<td></td>
<td><p>1974年12月14日－1978年11月15日</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="../Page/單國璽.md" title="wikilink">單國璽</a><a href="../Page/樞機.md" title="wikilink">樞機</a></p></td>
<td></td>
<td><p>1979年11月15日－1991年3月4日</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><a href="../Page/錢志純.md" title="wikilink">錢志純主教</a></p></td>
<td></td>
<td><p>1992年1月23日－2001年11月19日</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="../Page/黃兆明.md" title="wikilink">黃兆明主教</a></p></td>
<td></td>
<td><p>2001年11月19日－迄今</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 教區各單位

### 主教公署

花蓮教區的最高統轄機構為花蓮教區主教公署，本部位於[花蓮縣](../Page/花蓮縣.md "wikilink")[花蓮市中美路](../Page/花蓮市.md "wikilink")168號，與主教座堂共用同一建築，並於[台東市設有辦事處](../Page/台東市.md "wikilink")。主教公署內設[秘書長](../Page/秘書長.md "wikilink")、教區參議等職務，總主教公署之下共設有8個委員會、司鐸諮議會、教區法庭、原住民牧靈區等單位，共同肩負起教區內教務的運作。

### 相關機構

<table>
<tbody>
<tr class="odd">
<td><dl>
<dt>教區附屬修道機構</dt>

</dl>
<ul>
<li><a href="../Page/花蓮市.md" title="wikilink">花蓮市聖若瑟修院</a></li>
<li><a href="../Page/聖瑪爾大女修會.md" title="wikilink">聖瑪爾大女修會</a></li>
</ul>
<dl>
<dt><strong>各級學校</strong>：</dt>

</dl>
<ul>
<li><a href="../Page/花蓮縣私立海星高級中學.md" title="wikilink">花蓮縣私立海星高級中學</a></li>
<li><a href="../Page/台東縣私立公東高級工業職業學校.md" title="wikilink">台東縣私立公東高級工業職業學校</a></li>
<li><a href="../Page/花蓮市私立海星國民小學.md" title="wikilink">花蓮市私立海星國民小學</a></li>
</ul>
<dl>
<dt><strong>牧靈機構</strong>：</dt>

</dl>
<ul>
<li><a href="../Page/花東新聞雜誌社.md" title="wikilink">花東新聞雜誌社</a></li>
<li>花蓮市保祿牧靈中心</li>
<li><a href="../Page/鹽寮簡樸靈修中心.md" title="wikilink">鹽寮簡樸靈修中心</a></li>
<li><a href="../Page/台東市.md" title="wikilink">台東市貞德文教中心</a></li>
</ul></td>
<td><dl>
<dt><strong>社福機構</strong>：</dt>

</dl>
<ul>
<li>天主教聲遠之家</li>
<li>小樹之家</li>
<li>財團法人天主教仁愛啟智中心</li>
<li><a href="../Page/財團法人天主教善牧社會福利基金會.md" title="wikilink">善牧基金會太巴塱關懷中心</a></li>
<li>台東市救星教養院</li>
<li><a href="../Page/玉里鎮.md" title="wikilink">玉里安德啟智中心</a></li>
<li><a href="../Page/關山鎮_(臺灣).md" title="wikilink">關山聖十字療養院</a></li>
<li>天主教希望職工中心</li>
<li>台東市<a href="../Page/聖母醫院_(台東).md" title="wikilink">聖母醫院</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料

### 腳註

<div class="references-small">

<references />

</div>

### 書目

  - 《台灣天主教手冊》2012年版，台灣地區主教團秘書處編譯，天主教教務協進會出版社出版

### 網站

  - [Giga-Catholic Information - Diocese of Hwalien 花連,
    Taiwan](http://www.gcatholic.com/dioceses/diocese/hwal0.htm)

## 相關條目

  - [天主教台灣地區主教團](../Page/天主教台灣地區主教團.md "wikilink")
  - [台灣天主教](../Page/台灣天主教.md "wikilink")

## 外部連結

  - [花蓮教區入口網站](https://web.archive.org/web/20071027063426/http://www.catholic.org.tw/hualien/)

[花](../Category/台灣天主教教區.md "wikilink")
[Category:花蓮縣文化](../Category/花蓮縣文化.md "wikilink")
[Category:臺東縣文化](../Category/臺東縣文化.md "wikilink")
[Category:1952年台灣建立](../Category/1952年台灣建立.md "wikilink")