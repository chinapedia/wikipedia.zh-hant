**柯霍氏法則**，又稱**柯霍假說、亨勒－柯霍法則**，是由四項標準組成的一套研究思維，用以建立疾病和微生物之間的因果關係，此法則由[柯霍和](../Page/柯霍.md "wikilink")[勒夫勒共同於](../Page/勒夫勒.md "wikilink")1884年將理論公式化，並於1890年由柯霍獨立修正後公諸於世，並以此為基礎建立[炭疽和](../Page/炭疽.md "wikilink")[結核的](../Page/結核.md "wikilink")[病原學](../Page/病原學.md "wikilink")，但這套思維已被廣泛運用在其他許多疾病上。

## 柯霍氏法則

柯霍氏法則主要分為四個步驟：\[1\]

1.  病體罹病部位經常可以找到大量的[病原體](../Page/病原體.md "wikilink")，而在健康活體中找不到這些病原體。
2.  病原菌可被分離並在[培養基中進行培養](../Page/培養基.md "wikilink")，並記錄各項特徵。
3.  純粹培養的病原菌應該接種至與病株相同品種的健康植株，並產生與病株相同的病徵。
4.  從接種的病株上以相同的分離方法應能再分離出病原，且其特徵與由原病株分離者應完全相同。

應用上之限制

1.
## 理論缺陷

1893年發現有些[霍亂](../Page/霍亂.md "wikilink")[帶原者以及](../Page/帶原者.md "wikilink")[傷寒瑪莉等案例並無任何](../Page/傷寒瑪莉.md "wikilink")[症狀表現](../Page/症狀.md "wikilink")，因此柯霍後來又將第一條原則後半刪去。後來在[小兒麻痺](../Page/小兒麻痺.md "wikilink")、[疱疹](../Page/疱疹.md "wikilink")、[愛滋病](../Page/愛滋病.md "wikilink")、[C型肝炎都有類似發現](../Page/C型肝炎.md "wikilink")，甚至今日幾乎所有醫師和病毒學家都認同小兒麻痺病毒只會對少數感染者造成癱瘓。

第三條原則也同樣不盡完美，柯霍本身也在1884年發現霍亂、結核等疾病未必能在不同個體產生相同表現，以今日之觀點，愛滋病毒無法感染[CCR5
Δ32基因刪除的個體](../Page/CCR5_Δ32.md "wikilink")。

柯霍氏法則發展於[十九世紀](../Page/十九世紀.md "wikilink")，是以當時技術水準能用來辨認病原體的技術通則。\[2\]但柯霍生活的年代，已有許多疾病明顯和某些物質相關，卻無法符合這套法則的檢驗。\[3\]而過於信任這套研究方法的學界，也在無法透過[培養基分離培養病毒的情況下](../Page/培養基.md "wikilink")，導致[病毒學發展窒礙難行](../Page/病毒學.md "wikilink")。\[4\]\[5\]目前，已有不少疾病證實為某種病原體造成，卻不能滿足柯霍提出的理論。\[6\]因此，雖然柯霍氏法則在史上占有一席之地，也持續對微生物診斷有所幫助，但目前研究已無要求必須完全符合四項準則考驗。

## 參考文獻

<div class="references-small">

<references/>

</div>

  - Koch R. *Über die Ätiologie der Tuberkulose*. In: "*Verhandlungen
    des Kongresses für Innere Medizin. Erster Kongress, Wiesbaden
    1882*".
  - Koch R. (1893) *J. Hyg. Inf.* **14**, 319-333

[Category:流行病學](../Category/流行病學.md "wikilink")

1.  《作物病蟲與防治》，柯勇 編，藝軒圖書出版社，13頁～15頁，1998年3月，ISBN：957-616-482-6
2.
3.  Koch R. (1884) *Mitt Kaiser Gesundh* **2**, 1-88; Koch R. (1893) *J.
    Hyg. Inf.* **14**, 319-333
4.  Brock TD (1999) Robert Koch: a life in medicine and bacteriology.
    American Society of Microbiology Press, Washington
5.  Evans AS (1976) Causation and disease: the Henle-Koch postulates
    revisited. Yale J Biol Med 49:175–195
6.