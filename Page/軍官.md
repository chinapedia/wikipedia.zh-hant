**軍官**，本義為國家授權至軍中管理軍隊的[官員](../Page/官員.md "wikilink")，軍官往往象徵著國家軍隊的威權性，是軍隊最高的管理**幹部**，一般由**尉級**至**將級**的人員擔任，在軍中擔任領導軍隊，指揮領導或技術幕僚（或稱參謀）的職務，前者擔任的職務名稱有[排長](../Page/排長.md "wikilink")、[連長](../Page/連長.md "wikilink")、[營長或](../Page/營長.md "wikilink")[旅長等](../Page/旅長.md "wikilink")，後者幕僚職則有**人事官**、**訓練官**或**情報官**等等，一般在現代國家的[軍隊體制](../Page/軍隊.md "wikilink")，[軍人分為](../Page/軍人.md "wikilink")**軍官**、**[士官](../Page/士官.md "wikilink")**、**[士兵](../Page/士兵.md "wikilink")**三大體系，各有其任務執掌，而軍官在位階上高於士官及士兵，擔任領導及幕僚的職務。

在古中國各個朝代，對軍官的稱呼不一，有武將、武官、[武吏等稱呼](../Page/吏.md "wikilink")，軍官一詞，最早出現在漢朝，《漢書·百官公卿表》載：「騪粟都尉，武帝軍官，不常置。」元朝，
官員分為民官、軍官。
在皇帝的詔、敕和公事行文中，對軍官的升、降、襲職、退休等已有規定。如，在邊界地區「鎮守三載無虞者，……軍官升一階”（《元史》卷二十四）;“軍官有功而升职者，舊以其子弟襲職，陣亡者許令承襲，若罷去者，以有功者代之。
[29_Bn_(AWM_E02790).jpg](https://zh.wikipedia.org/wiki/File:29_Bn_\(AWM_E02790\).jpg "fig:29_Bn_(AWM_E02790).jpg")的指揮官正在命令[澳軍](../Page/澳洲軍隊.md "wikilink")29營一個排的士兵們。|alt=A
line of soldiers in battle equipment face another soldier who is
addressing them on a gentle slope. Behind them smoke or fog obscures the
rest of the terrain\]\]

## 軍銜

1912年8月，[北洋政府公布](../Page/北洋政府.md "wikilink")《陆军官佐士兵等级表》，将军官分为[将官](../Page/将官.md "wikilink")、[校官](../Page/校官.md "wikilink")、[尉官三等](../Page/尉官.md "wikilink")，各等又分上、中、少三级。[中華民國國軍軍階和](../Page/中華民國國軍軍階.md "wikilink")[中国人民解放军军衔亦沿用这一体系](../Page/中国人民解放军军衔.md "wikilink")，并以此对应翻译各国軍官军衔。

各国军官中，除了通常分為尉官、校官以及將官三級外，从低到高包含[准尉](../Page/准尉.md "wikilink")（介於[士官與軍官之間](../Page/士官.md "wikilink")）、[少尉](../Page/少尉.md "wikilink")、[中尉](../Page/中尉.md "wikilink")、[上尉](../Page/上尉.md "wikilink")、[少校](../Page/少校.md "wikilink")、[中校](../Page/中校.md "wikilink")、[上校](../Page/上校.md "wikilink")、[少將](../Page/少將.md "wikilink")、[中將](../Page/中將.md "wikilink")、[二級上將以及](../Page/二級上將.md "wikilink")[一級上將](../Page/一級上將.md "wikilink")。部分國家還設有[特級上將](../Page/特級上將.md "wikilink")（如[中華民國](../Page/中華民國.md "wikilink")，已於西元2000年廢除）、[准將](../Page/准將.md "wikilink")（[中華人民共和國等部份國家稱為](../Page/中華人民共和國.md "wikilink")[大校](../Page/大校.md "wikilink")）或[大尉](../Page/大尉.md "wikilink")（如[朝鮮民主主義人民共和國](../Page/朝鮮民主主義人民共和國.md "wikilink")）軍銜。一部分國家非常時期在將官之上還設有[元帥](../Page/元帥.md "wikilink")（[美國及](../Page/美國.md "wikilink")[利比里亚稱為](../Page/利比里亚.md "wikilink")[五星上將](../Page/五星上將.md "wikilink")），甚至[大元帥](../Page/大元帥.md "wikilink")（專門授予[國家元首或最高軍事長官等一國軍隊最高統帥](../Page/國家元首.md "wikilink")）等最高級軍官軍銜。

軍官的任用多半需要依軍階，實際運用上，可以用較低[軍階的人員擔任高階職務](../Page/軍階.md "wikilink")，稱為「[佔缺](../Page/佔缺.md "wikilink")」。如一般情況，[連長編制是由](../Page/連.md "wikilink")[上尉或](../Page/上尉.md "wikilink")[少校擔任](../Page/少校.md "wikilink")，但是[中尉可以](../Page/中尉.md "wikilink")「佔缺」擔任連長。

## 執掌

軍官在部隊中，具有領導和管理的責任，軍官負責戰略的制定與決策，以及命令的傳達，相當於企業裡的中高階主管；而士官則較偏重在如何讓戰略落實，命令如何執行，相當於企業裡的基層幹部。簡單來說，軍官是決策者；士官士兵是執行者。

在職權上，士官士兵需要服從[軍官的領導](../Page/軍官.md "wikilink")，但是在基層連隊上，為防止軍官在地方待過久導致控制軍隊，形成地方上的[軍閥割據](../Page/軍閥.md "wikilink")，因此大多會採取[輪調的制度](../Page/輪調.md "wikilink")，每兩到三年輪調一次，而士官則無此制度，較熟悉地方上連隊運作，因此大多數軍官在領導連隊上仍須與當地士官相互合作，才能妥善使連隊正常運行，也因此有發生過菜鳥[少尉](../Page/少尉.md "wikilink")[排長遭到老鳥](../Page/排長.md "wikilink")[上士](../Page/上士.md "wikilink")[班長或](../Page/班長.md "wikilink")[士官長欺負的情形](../Page/士官長.md "wikilink")。

## 特殊情況

一般現代國家，軍官的任命分為三大體系：[軍校畢業生](../Page/軍校.md "wikilink")、地方大專學院招募（如[大學儲備軍官訓練團](../Page/大學儲備軍官訓練團.md "wikilink")（[ROTC](../Page/ROTC.md "wikilink")））及士官向上升遷得來。

但在某些有[義務役](../Page/義務役.md "wikilink")[兵役制度的](../Page/兵役.md "wikilink")[國家](../Page/國家.md "wikilink")，會擇優准許[義務役軍人成為軍官](../Page/義務役.md "wikilink")，但多半只能擔任最低層的[少尉](../Page/少尉.md "wikilink")。如[臺灣有](../Page/臺灣.md "wikilink")[預官考試](../Page/預官考試.md "wikilink")，招考[大學](../Page/大學.md "wikilink")（[學士](../Page/學士.md "wikilink")）以上學歷的役男，如被錄取為預備軍官者，在受訓結束後，授予[少尉軍官階級](../Page/少尉.md "wikilink")。

大致上[日語沒有](../Page/日語.md "wikilink")“軍官”一詞，稱軍官為[士官](../Page/士官.md "wikilink")，俗稱「將校」。[現代漢語語境的](../Page/現代漢語.md "wikilink")“[士官](../Page/士官.md "wikilink")”階級在[日本則稱為](../Page/日本.md "wikilink")“下士官”，包含[軍曹及曹長等職務](../Page/軍曹.md "wikilink")。而在[日本自衛隊中](../Page/日本自衛隊.md "wikilink")，相當於軍官的名詞為「幹部自衛官」或「幹部」，包含將官、佐官及尉官。

## 参考文献

## 外部链接

  - [U.S. DoD Officer Rank
    Insignia](http://www.defenselink.mil/specials/insignias/officers.html)

[军官](../Category/军官.md "wikilink")
[Category:军事术语](../Category/军事术语.md "wikilink")
[Category:特定人群称谓](../Category/特定人群称谓.md "wikilink")