**梁灵光**（），[福建省](../Page/福建省.md "wikilink")[永春县人](../Page/永春县.md "wikilink")。中华人民共和国官员。原[中华人民共和国轻工业部部长](../Page/中华人民共和国轻工业部.md "wikilink")、中共广东省顾问委员会主任、中共广东省委原书记、广东省原省长。夫人[朱含章](../Page/朱含章.md "wikilink")。

## 生平

### 民国时期

1935年，梁灵光参加[一二·九运动](../Page/一二·九运动.md "wikilink")，积极投身抗日救亡工作。1936年2月，参加中共秘密组织上海抗日青年团，同年6月，赴马来西亚[吉隆坡尊孔中学任教](../Page/吉隆坡尊孔中学.md "wikilink")，组建了“雪兰俄邦反帝大同盟”、“华侨抗日救国会”、“左翼作家联盟”三个进步团体并担任主席\[1\]。[七七事变后回国](../Page/七七事变.md "wikilink")，在苏北参加抗战。1940年8月加入中国共产党。历任苏中三分区[如皋县县长兼警卫团团长](../Page/如皋县.md "wikilink")，苏中军区四分区游击指挥部政治部主任，[南通县抗日民主政府第一任县长兼保安旅旅长](../Page/南通县.md "wikilink")、警卫团团长，四分区专署专员\[2\]。

[第二次国共内战期间](../Page/第二次国共内战.md "wikilink")，历任[新四军华中第](../Page/新四军.md "wikilink")9军9分区司令员兼专员、[华东野战军第](../Page/华东野战军.md "wikilink")11纵队33旅旅长，[中国人民解放军第29军参谋长](../Page/中国人民解放军第29军.md "wikilink")，率部率先攻入厦门\[3\]。

### 共和国时期

中华人民共和国成立后，梁灵光历任[厦门市第一任市长](../Page/厦门市.md "wikilink")、市委书记，[福建省工业厅厅长兼省财委副主任](../Page/福建省工业厅.md "wikilink")、省委工交部部长。1956年3月，后任[福建省副省长](../Page/福建省副省长.md "wikilink")，书记处候补书记、书记。[文化大革命期问受到冲击](../Page/文化大革命.md "wikilink")。1975年初恢复工作，任[福建省革委会副主任](../Page/福建省革委会.md "wikilink")\[4\]。

1977年11月，调任[中华人民共和国轻工业部部长](../Page/中华人民共和国轻工业部.md "wikilink")。1979年4月，中共中央决定广东率先进行改革开放\[5\]。1980年11月，梁灵光调任[中共广东省委书记兼广州市委第一书记](../Page/中共广东省委.md "wikilink")、市长\[6\]。1983年3月，任广东省委书记（当时第一书记[任仲夷](../Page/任仲夷.md "wikilink")）、省长，并兼任[暨南大学校长](../Page/暨南大学.md "wikilink")（至1991年6月）。1985年7月任广东省顾委主任。1986至1988年，兼任[香港中旅集团董事长](../Page/香港中旅集团.md "wikilink")\[7\]。1988年5月，被选为第七届全国人大常委会委员、全国人大华侨委员会副主任委员。此外，他还是中共十二大、十三大代表，第十二届中央委员，第二、五、六、七届全国人大代表\[8\]。

2006年2月25日，梁灵光在[广州逝世](../Page/广州.md "wikilink")，终年90岁\[9\]。

## 参考

[Category:新四军人物](../Category/新四军人物.md "wikilink")
[Category:中国人民解放军人物](../Category/中国人民解放军人物.md "wikilink")
[1](../Category/中华人民共和国厦门市市长.md "wikilink")
[Category:中共廈門市委書記](../Category/中共廈門市委書記.md "wikilink")
[Category:福建省人民政府组成部门负责人](../Category/福建省人民政府组成部门负责人.md "wikilink")
[Category:福建省革命委员会副主任](../Category/福建省革命委员会副主任.md "wikilink")
[Category:中華人民共和國輕工業部部長](../Category/中華人民共和國輕工業部部長.md "wikilink")
[Category:中共廣州市委書記](../Category/中共廣州市委書記.md "wikilink")
[Category:中华人民共和国广州市市长](../Category/中华人民共和国广州市市长.md "wikilink")
[Category:中共广东省委书记](../Category/中共广东省委书记.md "wikilink")
[Category:中华人民共和国广东省省长](../Category/中华人民共和国广东省省长.md "wikilink")
[Category:暨南大學校長](../Category/暨南大學校長.md "wikilink")
[Category:中共十二大代表](../Category/中共十二大代表.md "wikilink")
[Category:中共十三大代表](../Category/中共十三大代表.md "wikilink")
[Category:中国共产党第十二届中央委员会委员](../Category/中国共产党第十二届中央委员会委员.md "wikilink")
[Category:第二届全国人大代表](../Category/第二届全国人大代表.md "wikilink")
[Category:第五届全国人大代表](../Category/第五届全国人大代表.md "wikilink")
[Category:第六届全国人大代表](../Category/第六届全国人大代表.md "wikilink")
[Category:第七届全国人大常委会委员](../Category/第七届全国人大常委会委员.md "wikilink")
[Category:廣東省全國人民代表大會代表](../Category/廣東省全國人民代表大會代表.md "wikilink")
[Category:福建省全國人民代表大會代表](../Category/福建省全國人民代表大會代表.md "wikilink")
[Category:中国共产党党员
(1940年入党)](../Category/中国共产党党员_\(1940年入党\).md "wikilink")
[Category:永春人](../Category/永春人.md "wikilink")
[L](../Category/梁姓.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.