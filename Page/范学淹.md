**范学淹**主教（，），得到[圣座认可但是未得到](../Page/圣座.md "wikilink")[中华人民共和国政府认可的](../Page/中华人民共和国政府.md "wikilink")[天主教保定教区](../Page/天主教保定教区.md "wikilink")[主教](../Page/主教.md "wikilink")（1951年4月12日－1992年4月16日）。

## 早年

1907年（清光绪三十三年）2月11日（农历12月29日），范学淹生于中国河北省[保定以东](../Page/保定.md "wikilink")30华里的[清苑县小望亭村](../Page/清苑县.md "wikilink")，名玉贵，父母都是虔诚的天主教信徒，经历了1900年的[义和团运动而幸存下来](../Page/义和团运动.md "wikilink")，他的父母家境富裕，为人慷慨，对天主教的信仰坚定。

范学淹在出生后第八天从本堂神父手中接受了洗礼，取圣名伯多禄。

## 修道

### 保定：备修院与小修院

范学淹受到父母和本堂神父的栽培，自幼立志修道。1918年，11岁的范学淹进入保定西关[备修院学习](../Page/备修院.md "wikilink")，1920年，转入圣若瑟[小修院](../Page/小修院.md "wikilink")。

1927年，20岁的范学淹修士和表弟杨慕时（洗名玛窦）一同被送到北京[西直门外大栅栏圣文生石门](../Page/西直门.md "wikilink")[大修院](../Page/大修院.md "wikilink")，学习哲学与神学。这时他的母亲希望他还俗回家，并作主为他订下了一门婚事，但范学淹修士不为所动，继续他的修道生活\[1\]。

### 北京

就在范学淹修士进入北京石门大修院的这一年，[梵蒂冈教廷驻华代表](../Page/天主教会.md "wikilink")[刚恒毅主教前去视察修院](../Page/刚恒毅.md "wikilink")，发现了他们的才华，很是欣赏，于是选派他们到罗马[宗座传信大学深造](../Page/宗座传信大学.md "wikilink")。

### 罗马

1934年年底，范学淹修士修完了神学、[哲学课程](../Page/哲学.md "wikilink")，并取得[教会法博士学位](../Page/教会法.md "wikilink")。同年12月22日，范学淹修士在罗马晋升为[神父](../Page/神父.md "wikilink")。他在首祭[弥撒中把自己特别奉献给中国教会主保](../Page/弥撒.md "wikilink")[大圣若瑟](../Page/大圣若瑟.md "wikilink")，希望象圣若瑟一样毕生服侍[耶稣](../Page/耶稣.md "wikilink")，热爱[圣母](../Page/圣母.md "wikilink")，并在自己原来的洗名伯铎后面又加上了若瑟，从此他的圣名改为伯铎·若瑟（Peter
Joseph）。

## 当神父的时期

### 保定教区

1935年，范学淹神父同表弟杨慕时神父一同离开罗马，回国传教。杨慕时神父在[南京教区担任](../Page/南京教区.md "wikilink")[于斌主教的秘书](../Page/于斌.md "wikilink")，而范学淹神父则回到保定教区，在环境艰苦的山区传教。

### 徐州教区

1940年，保定教区应徐州教区邰铁欧主教的邀请，派遣范学淹神父与本教区的朱友三、安比约、赵保禄、陈比尧、候鸿佑等九位神父前往徐州工作，范学淹被分配在徐州市担任教会中学的历史教师。1941年12月7日，[太平洋战争爆发](../Page/太平洋战争.md "wikilink")，徐州教区的40位外籍神父全被集中到上海[徐家汇大堂](../Page/徐家汇大堂.md "wikilink")，许多堂区缺少神父，于是范学淹神父被调往苏、鲁边界的[砀山县候庄村担任本堂神父](../Page/砀山县.md "wikilink")。战争期间，候庄是日军、[汪精卫政权军队](../Page/汪精卫政权.md "wikilink")、[国民革命军](../Page/国民革命军.md "wikilink")、[八路军各路人马出没的地方](../Page/八路军.md "wikilink")，范学淹神父在复杂的环境中竭力维持教友的正常信仰生活，同时还需要小心谨慎地应付各路军队的骚扰。

### 湖北

1944年，范学淹神父被调离徐州。1945年，[中国抗日战争结束](../Page/中国抗日战争.md "wikilink")，范学淹神父转往湖北宜昌从事救助难民和牧灵工作。1949年，中国大陆政局变化，范学淹神父的许多亲友，以及不少神职人员均在这时撤离中国大陆，但他却执意留在中国大陆，在[汉口管理教会房产](../Page/汉口.md "wikilink")。范学淹神父说：“我是中国人，我的圣召在中国，相信我即使是死在中国，也是对中华民族的祝福，是天主对我们民族的认定，因为他接纳了这块土地上的祭品，接纳了这个民族的儿子。”

## 当主教的时期

### 祝圣

1951年4月12日，罗马教廷任命范学淹神父为[保定教区主教](../Page/保定教区.md "wikilink")。6月24日（圣若翰诞辰瞻礼），范学淹神父在[汉口圣若瑟主教座堂接受祝圣](../Page/汉口圣若瑟主教座堂.md "wikilink")，发誓绝对服从圣座、服从教宗，至死不離開中國，誓死不離開他的教區。

保定教区自从[周济世主教于](../Page/周济世.md "wikilink")1946年升任江西省[南昌教区的总主教后](../Page/南昌教区.md "wikilink")，已经有近5年时间主教职位出缺。范学淹主教就职后，便着手恢复保定教区瘫痪数年的教区体制。

1958年初春，范学淹主教在保定天主教爱国会的筹备会议上表示拒绝脱离教宗，同年5月，范学淹在[中华人民共和国政府策划的批斗大会上被批斗](../Page/中华人民共和国政府.md "wikilink")，并被判处十年劳改（在黄骅县劳改农场和安新县大坨子劳改厂）。與此同時，[天主教愛國會任命已离职数年并有妻子和孩子的](../Page/天主教愛國會.md "wikilink")[王其威接替范學淹的主教職位并由献县教区赵振声予以祝圣](../Page/王其威.md "wikilink")，有關任命未得到[梵蒂冈教廷接納](../Page/天主教会.md "wikilink")。1969年年底，范学淹主教刑满获释，回到原籍小望亭村，继续作为“黑四类”接受监管，。

### 文化大革命后

1976年，教宗[保禄六世亲笔致书给范学淹主教](../Page/保禄六世.md "wikilink")，表达对他的关怀与安慰、嘉奖和祝福。1977年11月，范学淹主教因为受到教宗的嘉奖，惹怒政府，再次被拘捕。1978年4月15日，又“以范学淹为首的天主教反革命集团”为罪名第二次被捕。

1980年1月4日，[中华人民共和国的法院重审范学淹案](../Page/中华人民共和国.md "wikilink")，判决范学淹主教无罪而获释。1980年1月20日，范学淹主教由南大冉看守所获释回原籍小望亭。1月26日，。

1981年1月1日，范学淹不顾徐水县安庄村青年[安树新对天主教学识的不足而祝圣他为神父](../Page/安树新.md "wikilink")，同年的圣枝主日，范学淹将清苑县田各庄村慈幼会修士[苏志民也祝圣为神父](../Page/苏志民.md "wikilink")，范学淹表示：现阶段我们不可能有那么完善的修院，我们的神父也不可能都如同圣多玛斯那样学问高深，但我们的司铎都该具备圣维亚奈般的圣德。知识不是最重要的，唯有德行占首位。范学淹主教还说：“知识不够，圣了神父还可以再学，唯有品行方面不能含糊。”范学淹在此后还将[正定教区神父](../Page/正定教区.md "wikilink")[贾治国](../Page/贾治国.md "wikilink")、[易县教区神父](../Page/易县教区.md "wikilink")[周善夫和](../Page/周善夫.md "wikilink")[天水教区神父王弥禄分别祝圣为正定教区](../Page/天水教区.md "wikilink")、易县教区和天水教区的主教。并委托贾主教祝圣赵县教区的闵神父为主教。由于联系不便，范学淹主教的举动事先并未请示罗马教宗，事后才将自己私自祝圣主教的事情呈报教廷，表示愿意接受处分。但教宗[若望·保禄二世不但没有处分范学淹主教](../Page/若望·保禄二世.md "wikilink")，反而在致函中授予范学淹特权：“在一切事上，你可先行裁决处理，而后再向我汇报\[2\]。”此后，[中华人民共和国河北省的天主教地下教会有了一定的发展](../Page/中华人民共和国.md "wikilink")，到1995年末，河北省天主教地下教会的主教累计已达27名（其中已故13名），而忠于[中国天主教爱国会主教累计仅](../Page/中国天主教爱国会.md "wikilink")10名（其中已故2名）。河北省天主教地下教会控制了保定、张家口、石家庄、邢台、邯郸5市的部分圣堂。

1982年4月13日，范学淹主教因祝圣主教司铎写信请示[教宗](../Page/教宗.md "wikilink")，而第三次被捕，并以“里通外国罪、私自祝圣主教、神父”的罪名被判刑十年，在河北省第二监狱服刑。

1987年11月17日，[中华人民共和国政府迫于国际社会舆论](../Page/中华人民共和国政府.md "wikilink")，将81岁的范学淹主教假释出狱，并将他软禁在保定市旧道大堂内。

1988年1月3日，范学淹主教向一位来访者（正定教区的牛伯铎教友）回答关于天主教爱国会的问题答问被整理成《十三条》，在中国天主教会内（不仅在地下教会）广泛流传，部分[中华人民共和国天主教信徒进行了响应并进行了抵制](../Page/中华人民共和国.md "wikilink")“三自爱国会”的运动，他们声称“三自爱国会”的神父是分裂教會，已经丧失神权，天主教友不可參加由愛國會神父主持的[弥撒或向他們領聖體](../Page/弥撒.md "wikilink")。受其影响，甘肃平凉教区[马骥主教也发表](../Page/马骥.md "wikilink")《我的声明》，。

1989年11月20日，中国大陆天主教各地忠于[梵蒂冈教会的主教及主教代表聚集在陕西省](../Page/天主教会.md "wikilink")[三原教区高陵县张二册村秘密举行会议](../Page/三原教区.md "wikilink")，决定成立“完全接受教宗的领导，维持其与整个天主教会的彻底共融”的[中国大陆主教团](../Page/中国大陆主教团.md "wikilink")，当时范学淹主教由于被软禁而未能亲自出席，众教长一致推举，选举范学淹主教为第一任团长。范学淹主教成为公认的地下天主教会领袖。\[3\]

[中国大陆主教团的成立震动了](../Page/中国大陆主教团.md "wikilink")[中华人民共和国政府](../Page/中华人民共和国政府.md "wikilink")\[4\]，由于范学淹主教的强大影响力，1990年11月3日，范学淹主教在保定主教府被政府秘密带走，在[石家庄烈士陵园內住了一个月后](../Page/石家庄烈士陵园.md "wikilink")，被秘密关押在河北省承德宽城县潘家水库内一小岛上。1992年4月，84岁的范学淹主教死亡。1992年4月13日，在公安机关人民警察和[中国人民武装警察部队的警卫下将被绿色塑料布包裹的范学淹主教遗体放在河北省清苑县小望亭圣堂门口](../Page/中国人民武装警察部队.md "wikilink")|time=2013-12-14T06:38:36+00:00}}。几天内，从全国各地前来吊唁可敬的范學淹主教。范学淹的遗体直到4月24日下午才安葬，有4万名教友前去送葬。2001年4月24日，范学淹主教的坟墓被政府用推土机推平。

## 参考文献

## 外部链接

  - [catholic-hierarchy网站上对范学淹的介绍](http://www.catholic-hierarchy.org/diocese/dpaot.html)

## 参见

  - [中华人民共和国天主教地下教会](../Page/中国天主教地下教会.md "wikilink")

{{-}}

[Fan](../Category/中华人民共和国天主教主教.md "wikilink")
[Fan](../Category/保定人.md "wikilink")
[Xueyan](../Category/范姓.md "wikilink")

1.  [基督的忠仆
    一代伟人—范学淹主教](http://www.tianya.cn/publicforum/Content/no01/1/308345.shtml)

2.  记者师范君：台北《教友生活周刊》，1984年1月16日
3.  <http://www.rthk.hk/tv/dtt31/programme/hkcc/episode/547867?lang=zh-hant>
4.  <http://www.rthk.hk/tv/dtt31/programme/hkcc/episode/547867?lang=zh-hant>