[1958_CPA_2262.jpg](https://zh.wikipedia.org/wiki/File:1958_CPA_2262.jpg "fig:1958_CPA_2262.jpg")
**關漢卿**，[號](../Page/號.md "wikilink")**已齋**（一作**一齋**）、**已齋叟**，[解州](../Page/解州.md "wikilink")（今[山西](../Page/山西.md "wikilink")[运城](../Page/运城.md "wikilink")）人，其籍贯还有[大都](../Page/大都.md "wikilink")（今[北京市](../Page/北京市.md "wikilink")）人，祁州（今[河北](../Page/河北.md "wikilink")[安国](../Page/安国市.md "wikilink")）人等说法，为「[元曲四大家](../Page/元曲四大家.md "wikilink")」之首。\[1\]\[2\]
生平事跡不詳，根據《[錄鬼簿](../Page/錄鬼簿.md "wikilink")》、《[青樓集](../Page/青樓集.md "wikilink")》、《[南村輟耕錄](../Page/南村輟耕錄.md "wikilink")》一些零碎的資料來看，他是[金末](../Page/金朝.md "wikilink")[元初人](../Page/元朝.md "wikilink")，活躍於約1210年至約1300年（[元成宗](../Page/元成宗.md "wikilink")[大德](../Page/大德_\(元朝\).md "wikilink")）間。

## 生平

關漢卿可能是[太醫院的一個醫生](../Page/太醫院.md "wikilink")，另有一說是先祖或父兄為[太醫院醫生](../Page/太醫院.md "wikilink")，故關漢卿為[醫戶](../Page/醫戶.md "wikilink")，而不為醫生。\[3\]
主要在[大都](../Page/大都.md "wikilink")（今[北京](../Page/北京.md "wikilink")）附近活動，也曾到過[汴梁](../Page/汴梁.md "wikilink")、[臨安](../Page/臨安.md "wikilink")（今[杭州](../Page/杭州.md "wikilink")）等地。以[雜劇的成就最大](../Page/雜劇.md "wikilink")，一生寫了60多種，今存18種，\[4\]
最著名的有《[竇娥冤](../Page/竇娥冤.md "wikilink")》；關漢卿也寫了不少歷史劇，如：《[單刀會](../Page/關大王獨赴單刀會.md "wikilink")》、《[單鞭奪槊](../Page/尉遲恭單鞭奪槊.md "wikilink")》、《[西蜀夢](../Page/關張雙赴西蜀夢.md "wikilink")》等；散曲今在小令40多首、套數10多首。

關漢卿塑造的「我是箇蒸不爛、煮不熟、搥不扁、炒不爆、響璫璫一粒銅豌豆」（〈[不伏老](../Page/s:不伏老_\(關漢卿\).md "wikilink")〉）的形象也廣為人稱，被譽“曲家聖人”。《析津志辑佚·名宦》曰：“关一斋，字汉卿，燕人。生而倜傥，博学能文。滑稽多智，蕴藉风流，为一时之冠。是时文翰晦盲，不能独振，淹于辞章者久矣。”

## 作品风格

### 雜劇

關漢卿[雜劇題材和形式都廣泛而多樣化](../Page/雜劇.md "wikilink")，有[悲劇](../Page/悲劇.md "wikilink")，有[喜劇](../Page/喜劇.md "wikilink")，有壯烈的英雄，有戀愛故事，有家庭婦女問題，有官場[公案](../Page/公案.md "wikilink")。[雜劇題材大多反映現實](../Page/雜劇.md "wikilink")，生活面非常廣闊，真實具體，揭示了社會各方面的矛盾，對不幸者寄多深厚同情，高度結合思想性與藝術性。

關漢卿[雜劇劇本能根據主題而剪裁取捨](../Page/雜劇.md "wikilink")，情節安排緊湊，布局引人入勝，主線清晰，節奏緊湊，不全採用大團圓結局的慣例。

關漢卿[雜劇塑造的人物個性鮮明](../Page/雜劇.md "wikilink")，有血有肉，如[竇娥等人物形象均栩栩如生](../Page/竇娥.md "wikilink")，成功地塑造各種典型人物。

關漢卿善於駕馭語言，語言風格與題材互相配合，吸收民間文學的土語[方言](../Page/方言.md "wikilink")，以及古典詩詞的鮮活字詞，並加以提煉，能恰如其分地反映劇中人物的身分性格，又善於烘托渲染，充分表現元劇「[本色](../Page/本色.md "wikilink")」。

关汉卿的杂剧内容具有强烈的现实性和弥漫着昂扬的战斗精神，关汉卿生活的时代，政治黑暗腐败，社会动荡不安，他的剧作深刻地再现了社会现实，充满着浓郁的时代气息。既有对官场黑暗的无情揭露，又热情讴歌了人民的反抗斗争。慨慷悲歌，乐观奋争，构成关汉卿剧作的基调。

### 散曲

關漢卿[散曲寫男女戀情的作品最多](../Page/散曲.md "wikilink")，對婦女心理的刻劃細緻入微，寫離愁別恨則真切動人。關漢卿[散曲風格豪放](../Page/散曲.md "wikilink")，曲詞潑辣風趣；語言通俗而口語化，生動自然，很能表現曲的本色。他喜用白描手法，善於寫景，所用比喻，形象生動。

## 重要作品

  - 《[關張雙赴西蜀夢](../Page/關張雙赴西蜀夢.md "wikilink")》
  - 《[閨怨佳人拜月亭](../Page/閨怨佳人拜月亭.md "wikilink")》
  - 《[錢大尹智寵謝天香](../Page/錢大尹智寵謝天香.md "wikilink")》
  - 《[杜蕊娘智賞金線池](../Page/杜蕊娘智賞金線池.md "wikilink")》
  - 《[望江亭中秋切鱠](../Page/望江亭中秋切鱠.md "wikilink")》
  - 《[山神廟裴度還帶](../Page/山神廟裴度還帶.md "wikilink")》
  - 《[趙盼兒風月救風塵](../Page/趙盼兒風月救風塵.md "wikilink")》
  - 《[鄧夫人苦痛哭存孝](../Page/鄧夫人苦痛哭存孝.md "wikilink")》
  - 《[關大王獨赴單刀會](../Page/關大王獨赴單刀會.md "wikilink")》
  - 《[溫太真玉鏡臺](../Page/溫太真玉鏡臺.md "wikilink")》
  - 《[錢大尹智勘緋衣夢](../Page/錢大尹智勘緋衣夢.md "wikilink")》
  - 《[詐妮子調風月](../Page/詐妮子調風月.md "wikilink")》
  - 《[感天動地竇娥冤](../Page/竇娥冤.md "wikilink")》
  - 《[狀元堂陳母教子](../Page/狀元堂陳母教子.md "wikilink")》
  - 《[包待制三勘蝴蝶夢](../Page/包待制三勘蝴蝶夢.md "wikilink")》
  - 《[劉夫人慶賞五侯宴](../Page/劉夫人慶賞五侯宴.md "wikilink")》
  - 《[包待制智斬魯齋郎](../Page/包待制智斬魯齋郎.md "wikilink")》
  - 《[尉遲恭單鞭奪槊](../Page/尉遲恭單鞭奪槊.md "wikilink")》

## 影響

關漢卿雜劇為後世留下不少著名的故事作藍本，如「六月飛霜」（見於《感天動地竇娥冤》）的故事便膾炙人口。其雜劇情節布局緊湊，使後世明清戲曲的劇本情節，更符合[舞台演出的要求](../Page/舞台.md "wikilink")。

關漢卿雜劇之語言成為[本色派雜劇的典範](../Page/本色.md "wikilink")，影響[南戲和明初戲曲的發展](../Page/南戲.md "wikilink")。其雜劇塑造的人物深入民心，為後世劇作家留下典範，影響明清戲曲寫人的藝術特色。

## 評價

劉大-{杰}-在《中國文學發展史》第二十三章中，曾將關漢卿在[中國戲曲史上的地位](../Page/中國戲曲史.md "wikilink")，媲美[英國的](../Page/英國.md "wikilink")[劇作家](../Page/劇作家.md "wikilink")[莎士比亞](../Page/莎士比亞.md "wikilink")。关汉卿是位伟大的戏曲家，后世称关汉卿为“曲圣”。1958年，被世界和平大会理事会定为世界文化名人，在中外展开了关汉卿创作700周年纪念活动。同年6月28日晚，国内至少100种不同的戏剧形式，1500个职业剧团，同时上演关汉卿的剧本。他的剧作被译为英文、法文、德文、日文等，在世界各地广泛传播,外国人称他为“东方的莎士比亚”。

一般認為，將前代未完成的[戲曲加以](../Page/戲曲.md "wikilink")[改革](../Page/改革.md "wikilink")，完成[元雜劇的體裁者](../Page/元雜劇.md "wikilink")，實非關漢卿莫屬。《[錄鬼簿](../Page/錄鬼簿.md "wikilink")》列關漢卿於雜劇之首，[朱權](../Page/朱權.md "wikilink")《[太和正音譜](../Page/太和正音譜.md "wikilink")》如此評價：「觀其詞語，乃可上可下之才，蓋所以取者，初為雜劇之始，故卓以前列。」[王國維](../Page/王國維.md "wikilink")《[宋元戲曲史](../Page/宋元戲曲史.md "wikilink")》：「《竇娥冤》列入世界大悲劇亦無愧色。」\[5\]

[水星上有](../Page/水星.md "wikilink")[一座環形山以他的名字命名](../Page/關漢卿撞擊坑.md "wikilink")\[6\]。

## 参考文献

## 外部链接

  - [關漢卿與元雜劇](http://hk.chiculture.net/0414/html/0414a00/0414a00.html)
  - 金文京：〈[关汉卿身世考](http://www.nssd.org/articles/article_read.aspx?id=1002361741)〉。（1999）
  - 彭镜禧：〈[计将安出？－－浅说关汉卿编剧的一项特色](http://www.nssd.org/articles/article_read.aspx?id=7081662)〉。

[G关](../Category/元朝剧作家.md "wikilink")
[G关](../Category/曲作家.md "wikilink")
[G关](../Category/北京人.md "wikilink")

1.  <http://www.ebaomonthly.com/window/liter/chlit/ch6/ch6_5.htm>
2.  <http://www.epochtimes.com/b5/6/8/8/n1415124.htm>
3.  <http://paper.udn.com/udnpaper/POC0004/97364/web/>
4.  <http://140.133.6.14:9000/cpedia/Content.asp?ID=63764>
5.  劉大-{杰}-《中國文學發展史》
6.