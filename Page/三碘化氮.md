**三碘化氮**，或称**碘化氮**，是[化学式为NI](../Page/化学式.md "wikilink")<sub>3</sub>的[无机化合物](../Page/无机化合物.md "wikilink")。它是对接触极其敏感的[爆炸物](../Page/爆炸物.md "wikilink")，少量即可引发爆炸而发出噼啪声，伴随有紫色[碘蒸汽的生成](../Page/碘.md "wikilink")。NI<sub>3</sub>可与[NH<sub>3</sub>等生成一系列的](../Page/氨.md "wikilink")[加合物](../Page/加合物.md "wikilink")，结构复杂，但由于稳定性不强，因此研究不多。其中典型的如NI<sub>3</sub>·NH<sub>3</sub>，含有-N-I-骨架，是个[聚合物](../Page/聚合物.md "wikilink")。

## 分解

NI<sub>3</sub>的[分解反应为](../Page/分解反应.md "wikilink")：

  -
    2NI<sub>3</sub>(s) → N<sub>2</sub>(g) + 3I<sub>2</sub>(g); ΔH = –290
    kJ/mol

## NI<sub>3</sub>及衍生物的结构

三碘化氮是深红色固体，1990年首次用[X射线晶体学研究了其结构](../Page/X射线晶体学.md "wikilink")。当时的制备路线不含氨，是以[氮化硼和](../Page/氮化硼.md "wikilink")[一氟化碘在](../Page/一氟化碘.md "wikilink")−30 °C和[三氯一氟甲烷溶剂中反应制得的](../Page/三氯一氟甲烷.md "wikilink")，产物纯但产率不高。\[1\]
和[氨及其他氮的三卤化物一样](../Page/氨.md "wikilink")，NI<sub>3</sub>为三角锥型结构，[分子对称性C](../Page/分子对称性.md "wikilink")<sub>3v</sub>。\[2\]

一般以[碘和](../Page/碘.md "wikilink")[氨反应制备](../Page/氨.md "wikilink")“三碘化氮”。于低温无水氨中反应时，反应的初始产物是NI<sub>3</sub>·(NH<sub>3</sub>)<sub>5</sub>，温度升高时该物质失去氨生成1:1的[加合物NI](../Page/加合物.md "wikilink")<sub>3</sub>·NH<sub>3</sub>。而以碘和[氨水反应时](../Page/氨水.md "wikilink")，首先生成的是碘代胺：

  -

碘浓度足够大时，可析出黑色NI<sub>3</sub>·NH<sub>3</sub>固体：

  -
    NH<sub>2</sub>I + 2I<sub>2</sub> + 3NH<sub>3</sub> →
    2NH<sub>4</sub><sup>+</sup> + 2I<sup>−</sup> +
    NI<sub>3</sub>·NH<sub>3(s)</sub>

这个加合物是法国化学家[伯纳德·库图瓦在](../Page/伯纳德·库图瓦.md "wikilink")1812年首先发现的，但化学式却直到1905年才由斯尔波拉德（Silberrad）搞清楚。\[3\]
它在固态时含有由类似[硅氧四面体](../Page/硅酸盐.md "wikilink")\[4\]
的NI<sub>4</sub>四面体联结而成的 - NI<sub>2</sub> - I -
NI<sub>2</sub> - I - NI<sub>2</sub> - I -
长链，氨分子位于链间。此外，在(NI<sub>3</sub>·NH<sub>3</sub>)<sub>n</sub>结构中，N-I'-N'三原子为线形或接近线形，对称性很强，它们之间的作用力可以用[分子轨道理论中的](../Page/分子轨道理论.md "wikilink")[三中心四电子键来描述](../Page/三中心四电子键.md "wikilink")。\[5\]

NI<sub>3</sub>·NH<sub>3</sub>在暗处和用氨润湿时是稳定的，乾燥時按下列反应爆炸性分解：\[6\]

  -
    8NI<sub>3</sub>·NH<sub>3</sub> → 5 N<sub>2</sub> + 6 NH<sub>4</sub>I
    + 9 I<sub>2</sub>

热力学上，NI<sub>3</sub>·NH<sub>3</sub>的不稳定性可归咎于特别稳定的产物N<sub>2</sub>，氮分子中[键级为](../Page/键级.md "wikilink")3；然而其爆炸性亦可能与碘原子相对较大的半径导致的[位阻有关](../Page/位阻.md "wikilink")。

除NI<sub>3</sub>·NH<sub>3</sub>外，三碘化氮还可与[路易斯鹼生成其他加合物](../Page/路易斯鹼.md "wikilink")，如：NI<sub>3</sub>·3
NH<sub>3</sub>、NI<sub>3</sub>·\~5
NH<sub>3</sub>、NI<sub>3</sub>·[C<sub>5</sub>H<sub>5</sub>N](../Page/吡啶.md "wikilink")、NI<sub>3</sub>·CH<sub>3</sub>C<sub>5</sub>H<sub>4</sub>N、NI<sub>3</sub>·\~5.5
C<sub>5</sub>H<sub>5</sub>NO和NI<sub>3</sub>·\~2.5
C<sub>4</sub>H<sub>4</sub>S等。\[7\]

## 演示爆炸反应

  - 高中课堂中，常以少量的三碘化氮来演示“化学炸弹”，\[8\]“引爆剂”则通常是羽毛或空气气流，以突出其[感度之强](../Page/感度.md "wikilink")。此外，三碘化氮还是唯一已知的可被[α粒子和](../Page/α粒子.md "wikilink")[核裂变产物引爆的炸药](../Page/核裂变.md "wikilink")。\[9\]
  - NI<sub>3</sub>·NH<sub>3</sub>爆炸后留下的橙黄-紫色的[碘迹可用](../Page/碘.md "wikilink")[硫代硫酸钠溶液洗去](../Page/硫代硫酸钠.md "wikilink")。
  - 三碘化氮是[Brainiac科学系列节目](../Page/Brainiac科学系列.md "wikilink")（*Brainiac:
    Science Abuse*）"Peter Logan's Exploding
    Paste"一集中所用的爆炸物。由于安全原因，节目中并未透露制备三碘化氮的细节。
  - 在[罗伯特·海因莱因的小说](../Page/罗伯特·海因莱因.md "wikilink")《[法汉的赎身契](../Page/法汉的赎身契.md "wikilink")》（*Farnham's
    Freehold*）中，同名的休·法汉将三碘化氮（由氨和碘制备）作为爆炸火药使用。

## 参考资料

<references />

## 外部链接

  - [三碘化氮爆炸影片1000fps高速攝影](http://www.youtube.com/watch?v=49JfhJsdTqY)
  - [三碘化氮爆炸反应（需要Quicktime软件）](http://jchemed.chem.wisc.edu/JCESoft/CCA/CCA0/MOVIES/NI3IOD.html)
  - [对NI<sub>3</sub>爆炸原因的解释](http://www.chm.bris.ac.uk/motm/ni3/ni3j.htm)
  - [三碘化氮爆炸反应](http://www.youtube.com/watch?v=2KlAf936E90)

[Category:无机胺](../Category/无机胺.md "wikilink")
[Category:碘化物](../Category/碘化物.md "wikilink")
[Category:氮卤化物](../Category/氮卤化物.md "wikilink")
[Category:爆炸物](../Category/爆炸物.md "wikilink")
[Category:烟火化学品](../Category/烟火化学品.md "wikilink")

1.  Tornieporth-Oetting, I.; Klapötke, T. *Angewandte Chemie
    International Edition in English,* 1990, volume 29, pages 677-679.
    [1](http://dx.doi.org/10.1002/anie.199006771)

2.  Holleman, A. F.; Wiberg, E. "Inorganic Chemistry" Academic Press:
    San Diego, 2001. ISBN 0-12-352651-5.

3.  Silberrad, O. "On the Constitution of Nitrogen Triiodide" *Journal
    of the Chemistry Society* 1905, volume 87, pages 55-66. DOI:
    10.1039/CT9058700055

4.  张青莲等．《无机化学丛书》第四卷．北京：科学出版社．

5.
6.
7.
8.  Ford, L. A. and Grundmeier, E. W. *Chemical Magic*. Dover, **1993**,
    p. 76. ISBN 0-486-67628-5

9.  Bowden, F. P. Initiation of explosion by neutrons, α-particles, and
    fission products. *Proc. Roy. Soc.* (London) **1958**, *A246*,
    216-19.