**魯伯特·亞歷山大·洛伊德·葛林特**（，）或簡稱**鲁伯特·葛林特**（），是一名[英國男演員](../Page/英國.md "wikilink")。出生於[英格蘭](../Page/英格蘭.md "wikilink")[埃塞克斯郡](../Page/埃塞克斯郡.md "wikilink")，以出演《[哈利·波特](../Page/哈利·波特.md "wikilink")》系列影片中的[榮恩·衛斯理](../Page/榮恩·衛斯理.md "wikilink")（Ron
Weasley）這一角色而聞名。2009[泰晤士報公佈了一個英國富翁排行榜](../Page/泰晤士報.md "wikilink")，魯伯特進入三十歲以下富翁前一百名的排行榜，擁有高達900萬英鎊的身價。

## 生平

魯伯特是家裡五個孩子中的老大。在贏得[榮恩·衛斯理這一角色之前](../Page/榮恩·衛斯理.md "wikilink")，他參加過學校表演隊和當地劇​​團合作演出。包括在《安妮》中飾演小流氓Rooster，參演《彼得·潘》。以及在《格林童話》(Grimm
Tales) 中扮演Rumplestiltskin
。在學校裡他曾在《格里姆傳說》中飾演角色“高蹺皺皮”，還曾在一次才藝表演中出演了“神秘梅格”。

## 職業生涯

葛林特在BBC電視台節目《[Newsround](../Page/Newsround.md "wikilink")》上看到了《哈利波特》的公開選角啟事之後，魯伯特便參加了角色[榮恩·衛斯理的選拔](../Page/榮恩·衛斯理.md "wikilink")。

當數以萬計的英國男孩競爭哈利波特的角色的時候，他以一段為[榮恩·衛斯理量身定做的](../Page/榮恩·衛斯理.md "wikilink")[rap影片征服了評委](../Page/饒舌.md "wikilink")\[1\]。這個英國小男孩完全是憑藉那部眾所周知的童話而出名的，而在此之前，他僅僅參加過一次學校舞台劇演出，但他還是勇敢地參加了這部他非常喜歡的電影的試鏡，毫無疑問，他是最符合榮恩人物性格的小演員，他是非常幸運的，因為他說他當時一次次試鏡時擊敗的幾百個競爭對手看上去都比他漂亮。\[2\]

最初，葛林特的理想是成為一名冰淇淋小販，而現在，他發現自己其實是一名演員。和榮恩一樣，葛林特不愛學習。《哈利·波特與阿茲卡班的逃犯》的導演在電影開拍前要求三人組每人寫一篇關於自己角色的文章，[艾瑪·華森寫了十二頁](../Page/艾瑪·華森.md "wikilink")，[丹尼爾·雷德克里夫寫了一頁](../Page/丹尼爾·雷德克里夫.md "wikilink")，而葛林特則把這事拋在腦後，一個字也沒寫。導演說在他們身上看見了和角色驚人的相似之處。

在影片《哈利波特-火杯的考驗》中，榮恩的頭髮長了一些，這只是因為葛林特懶得剪頭髮—「拍完上一部時，我一直懶的去找理髮師，當製片人看到我的新髮型時就要求我繼續保持，他們覺得這更符合榮恩的個性。因為我懶得剪頭髮，所以這個決定對我來說也挺好。」

他在2009年7月，確診感染[人類豬流感](../Page/人類豬流感.md "wikilink")，一度引起劇迷擔憂，其後康復。

2016年8月22日，葛林特確認於《[偷拐搶騙](../Page/偷拐搶騙.md "wikilink")》的[電視劇版中擔任主演和兼任執行製片人](../Page/偷拐搶騙_\(電視劇\).md "wikilink")。該劇定於2017年推出。

## 作品列表

### 電影

|        |                                                               |                                        |        |
| ------ | ------------------------------------------------------------- | -------------------------------------- | ------ |
| **年份** | **標題**                                                        | **角色**                                 | **备注** |
| 2001年  | 《[哈利·波特-神秘的魔法石](../Page/哈利·波特與魔法石_\(電影\).md "wikilink")》      | [榮恩·衛斯理](../Page/羅恩·韋斯萊.md "wikilink") |        |
| 2002年  | 《[Thunderpants](../Page/Thunderpants.md "wikilink")》          | Alan A. Allen                          |        |
| 2002年  | 《[哈利·波特-消失的密室](../Page/哈利·波特與密室_\(電影\).md "wikilink")》        | [榮恩·衛斯理](../Page/羅恩·韋斯萊.md "wikilink") |        |
| 2004年  | 《[哈利·波特-阿茲卡班的逃犯](../Page/哈利·波特與阿茲卡班的囚徒_\(電影\).md "wikilink")》 | [榮恩·衛斯理](../Page/羅恩·韋斯萊.md "wikilink") |        |
| 2005年  | 《[哈利·波特-火杯的考驗](../Page/哈利·波特與火焰杯_\(電影\).md "wikilink")》       | [榮恩·衛斯理](../Page/羅恩·韋斯萊.md "wikilink") |        |
| 2006年  | 《[人生駕駛課](../Page/人生駕駛課.md "wikilink")》                        | 班·馬歇爾                                  |        |
| 2007年  | 《[哈利·波特-鳳凰會的密令](../Page/哈利·波特與鳳凰社_\(電影\).md "wikilink")》      | [榮恩·衛斯理](../Page/羅恩·韋斯萊.md "wikilink") |        |
| 2009年  | 《[櫻桃炸彈](../Page/櫻桃炸彈.md "wikilink")》                          | Malachy                                |        |
| 2009年  | 《[哈利·波特-混血王子的背叛](../Page/哈利·波特與混血王子_\(電影\).md "wikilink")》    | [榮恩·衛斯理](../Page/羅恩·韋斯萊.md "wikilink") |        |
| 2010年  | 《[狂野標靶](../Page/狂野標靶.md "wikilink")》                          | Tony                                   |        |
| 2010年  | 《[哈利波特：死神的聖物Ⅰ](../Page/哈利波特：死神的聖物Ⅰ.md "wikilink")》\[3\]       | [榮恩·衛斯理](../Page/羅恩·韋斯萊.md "wikilink") |        |
| 2011年  | 《[哈利波特：死神的聖物Ⅱ](../Page/哈利波特：死神的聖物Ⅱ.md "wikilink")》\[4\]       | [榮恩·衛斯理](../Page/羅恩·韋斯萊.md "wikilink") |        |
| 2012年  | 《[歃雪為盟](../Page/歃雪為盟.md "wikilink")》                          | Robert Smith                           |        |
| 2013年  | 《[查理必死](../Page/查理必死.md "wikilink")》                          | Karl                                   |        |
| 2013年  | 《[龐克地下城](../Page/龐克地下城.md "wikilink")》                        | Cheetah Chrome                         |        |
| 2013年  | 《[挑战者联盟](../Page/挑战者联盟.md "wikilink")》                        | Amadeo                                 | 配音     |
| 2014年  | 《[郵差帕特](../Page/郵差帕特.md "wikilink")》                          | Josh                                   | 配音     |
| 2015年  | 《Enemy of Man》                                                | Rosse                                  |        |
| 2015年  | 《[嗨咖上月球](../Page/嗨咖上月球.md "wikilink")》                        | Jonny                                  |        |

### 電視劇

|        |                                                        |                                  |             |
| ------ | ------------------------------------------------------ | -------------------------------- | ----------- |
| **年份** | **標題**                                                 | **角色**                           | **备注**      |
| 2005年  | 《Happy Birthday, Peter Pan》                            | [彼得潘](../Page/彼得潘.md "wikilink") | 配音；電視特別節目   |
| 2010年  | 《[伴我双飞](../Page/伴我双飞.md "wikilink")》                   | 他自己                              | 1集          |
| 2012年  | 《[特工老爹](../Page/特工老爹.md "wikilink")》                   | Liam                             | 配音；1集       |
| 2013年  | 《Super Clyde》                                          | Clyde                            | 試播集         |
| 2016年  | 《The Artist》                                           | August “Gustl” Kubizek           | 短劇          |
| 2016年  | 《[特蕾西·厄爾曼秀](../Page/特蕾西·厄爾曼秀.md "wikilink")》           | 他自己                              | 1集          |
| 2017年  | 《[Sick Note](../Page/Sick_Note.md "wikilink")》         | Daniel Glass                     |             |
| 2017年  | 《[偷拐搶騙](../Page/偷拐搶騙_\(電視劇\).md "wikilink")》           | Charlie Cavendish                | 兼執行製片人\[5\] |
| 2017年  | 《[Imperial City](../Page/Imperial_City.md "wikilink")》 | TBA (main role)                  |             |

## MV作品

2011年11月，[紅髮艾德的](../Page/紅髮艾德.md "wikilink")《Lego
House》全英金榜排行Top5、邀請電影《[哈利·波特](../Page/哈利·波特.md "wikilink")》的演員魯伯特·葛林演出瘋狂歌迷一角引起全面迴響，音樂錄影帶在YouTube創下逾1.87億次的點播率。

紅髮艾德透露，是[湯姆·費爾頓介紹魯伯特](../Page/湯姆·費爾頓.md "wikilink")·葛林來擔任此角。並且他表示：「魯伯特是你所能遇見過最好的人，他很可愛而且他免費的出演這個角色，這表明了他是個多麼親切友善的人。」

## 参見

  - [丹尼爾·域基夫](../Page/丹尼爾·域基夫.md "wikilink")
  - [愛瑪·華生](../Page/愛瑪·華生.md "wikilink")
  - [湯姆·費爾頓](../Page/湯姆·費爾頓.md "wikilink")

## 參考資料

## 外部链接

  -
  - [Ice Cream Man: Rupert Grint Fansite](http://www.rupert-grint.us)

  - [RupertGrint.net](http://www.rupertgrint.net)

[Category:21世紀英國男演員](../Category/21世紀英國男演員.md "wikilink")
[Category:英國男演員](../Category/英國男演員.md "wikilink")
[Category:英國電影演員](../Category/英國電影演員.md "wikilink")
[Category:英國電視演員](../Category/英國電視演員.md "wikilink")
[Category:英格蘭男電影演員](../Category/英格蘭男電影演員.md "wikilink")
[Category:英格蘭男電視演員](../Category/英格蘭男電視演員.md "wikilink")
[Category:英格蘭舞台演員](../Category/英格蘭舞台演員.md "wikilink")
[Category:哈利·波特演员](../Category/哈利·波特演员.md "wikilink")
[Category:英國前兒童演員](../Category/英國前兒童演員.md "wikilink")
[Category:艾塞克斯郡人](../Category/艾塞克斯郡人.md "wikilink")

1.
2.
3.
4.
5.