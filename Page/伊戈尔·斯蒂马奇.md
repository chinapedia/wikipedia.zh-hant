**-{zh-hans:伊戈尔·斯蒂马奇; zh-hant:史迪馬;}-**（**Igor
Štimac**，），是一名已退役的[克羅地亞职业足球運動員](../Page/克羅地亞.md "wikilink")，乃[1998年世界杯季軍隊成員](../Page/1998年世界杯.md "wikilink")。

\-{zh-hans:斯蒂马奇;
zh-hant:史迪馬;}-早年在克羅地亞国内效力。1995年他轉到[英國加盟當時處於英甲](../Page/英國.md "wikilink")《第二級》的[打比郡](../Page/德比郡足球俱樂部.md "wikilink")。他在效力第一季便創出聯賽連續二十場不敗紀錄，協助球會升上[英超](../Page/英超.md "wikilink")。他在球會效力四年，最後因公開批評球會而出售到[韋斯咸](../Page/韋斯咸.md "wikilink")。其後於2001年重返克羅地亞，一直踢到2006年掛靴為止。

自1992年起-{zh-hans:斯蒂马奇;
zh-hant:史迪馬;}-已是國家隊常規成員，曾随國家隊於[1998年世界杯取得季軍](../Page/1998年世界杯.md "wikilink")。

目前他於烏克蘭球會[顿涅茨克擔任球員經理人](../Page/顿涅茨克矿工.md "wikilink")。

## 榮譽

  - 世界杯季軍：1998

[Category:克羅地亞足球運動員](../Category/克羅地亞足球運動員.md "wikilink")
[Category:哈伊杜克球員](../Category/哈伊杜克球員.md "wikilink")
[Category:打比郡球員](../Category/打比郡球員.md "wikilink")
[Category:韋斯咸球員](../Category/韋斯咸球員.md "wikilink")
[Category:加的斯球員](../Category/加的斯球員.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")