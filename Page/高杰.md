**高杰**（），字英吾，[明末](../Page/明.md "wikilink")[陕西](../Page/陕西.md "wikilink")[米脂人](../Page/米脂.md "wikilink")，後降[明](../Page/明.md "wikilink")，封**興平伯**，與[黃得功](../Page/黃得功.md "wikilink")、[劉澤清](../Page/劉澤清.md "wikilink")、[劉良佐合稱](../Page/劉良佐.md "wikilink")[南明四鎮](../Page/南明.md "wikilink")。後遭降[清的睢州總兵](../Page/清.md "wikilink")[許定國誘殺](../Page/許定國.md "wikilink")。

原是[闖王](../Page/闖王.md "wikilink")[李自成的部將](../Page/李自成.md "wikilink")，一說其部曾殺明總兵[許定國一家](../Page/許定國.md "wikilink")\[1\]，但可能僅僅是明末清初文人的揣測（后來許派二子降清、其妻刑氏上書清廷乞還老家，說明許全家被殺之事不可信）。[崇祯七年](../Page/崇祯.md "wikilink")（1634年）十月，李自成派高杰向[贺人龙佯降](../Page/贺人龙.md "wikilink")。崇祯八年（1635年）高杰带着李自成妻邢氏投降了贺人龙\[2\]，被授予[游击之职](../Page/游击.md "wikilink")，累功升任总兵。崇祯十七年正月初，李自成出兵[潼关](../Page/潼关.md "wikilink")，高杰部兵败，南下徐州。\[3\]後投靠凤阳总督[马士英](../Page/马士英.md "wikilink")，屯驻[徐州](../Page/徐州.md "wikilink")\[4\]。四月下旬，离開徐州。\[5\]

[弘光帝政权成立时](../Page/弘光帝.md "wikilink")，高杰在南明[江北四鎮中兵力最強](../Page/江北四鎮.md "wikilink")，最受[史可法器重](../Page/史可法.md "wikilink")。但高杰性氣乖張，難以節制，抢掠焚杀，素為江南士紳不喜，他們指责高杰的军队“杀人则积尸盈野，淫污则辱及幼女”\[6\]。人稱“番山鹞子”。但他有樸直憨厚之處，能向史可法盡心效力，敢拒清肅親王[豪格](../Page/豪格.md "wikilink")“大者王，小者侯，世世茅土”的誘降\[7\]。後駐兵[泗州](../Page/泗州.md "wikilink")，負責開封、歸德（今河南商丘）一路招討，冒大雪，沿[黃河築牆](../Page/黃河.md "wikilink")。

明[弘光元年](../Page/弘光.md "wikilink")（清[顺治二年](../Page/顺治.md "wikilink")，1645年）清兵下[河南府](../Page/河南府.md "wikilink")，高杰約[睢州](../Page/睢州.md "wikilink")（今睢县）總兵[許定國](../Page/許定國.md "wikilink")，互相聯絡。然此時許定國早已暗降[清廷](../Page/清廷.md "wikilink")，並送二子[许尔安](../Page/许尔安.md "wikilink")、[许尔吉渡河為人質](../Page/许尔吉.md "wikilink")。十二日，许定国在睢州兵部尚書[袁可立府第大摆筵席](../Page/袁可立.md "wikilink")，为高杰、[越其-{杰}-](../Page/越其杰.md "wikilink")、[陈潜夫](../Page/陈潜夫.md "wikilink")、参政[袁枢等接风洗尘](../Page/袁枢.md "wikilink")。越其杰疑有詐，高杰卻不以為意。正月十二日，高杰率三百名亲兵至營中，十三日夜许定国在設宴召妓，把高杰灌醉，一刀砍下頭顱，持首級北渡黄河，向清將[豪格報功](../Page/豪格.md "wikilink")\[8\]。史可法聞讯大哭说：“中原不可复为矣！”\[9\]史可法立高杰之子[高元照为兴平世子](../Page/高元照.md "wikilink")，高杰遗孀邢氏以子年幼，想让史可法收元照为義子，史可法以其為流賊出身而拒之，遂命元照拜太监[高起潜为義父](../Page/高起潜.md "wikilink")。\[10\]

## 注釋

[G](../Page/category:米脂人.md "wikilink")

[J傑](../Category/高姓.md "wikilink") [G](../Category/大顺人物.md "wikilink")
[G](../Category/南明军事人物.md "wikilink")
[G](../Category/南明人物.md "wikilink")

1.  [吳偉業](../Page/吳偉業.md "wikilink")《鹿樵紀聞》卷上，〔高黃二鎮〕一節。
2.  [姚雪垠](../Page/姚雪垠.md "wikilink")《李自成》描写高杰勾引了李自成的老婆邢氏，所以在投降明朝。
3.  《明实录·崇祯实录》载：“崇祯十七年正月癸巳（初四），高杰南下，江北大震。”
4.  《淮城纪事》云：“三月初九日，黄得功、刘良佐、刘泽清、高杰四家兵皆南下”，“高杰兵在徐州”。
5.  《扬州变略》云：“广昌（刘良佐）自宿迁南行，驻兵瓜州，而兴平（高杰）亦垂涎维扬之盛，尾刘而来。”
6.  [文秉](../Page/文秉.md "wikilink")：《[甲乙事案](http://ctext.org/library.pl?if=gb&file=23776&page=13)》，第39页。
7.  林洛：《明朝滅亡真相》
8.  [戴笠](../Page/戴笠.md "wikilink")、[吴乔](../Page/吴乔.md "wikilink")：《流寇长编》卷十九、康熙《睢州志》皆記十三日。[郑廉](../Page/郑廉.md "wikilink")《豫变纪略》卷八记正月十二日。
9.  [徐鼒](../Page/徐鼒.md "wikilink")：《[小腆纪传](../Page/小腆纪传.md "wikilink")》卷十《列传》三《史可法》
10. [应廷吉](../Page/应廷吉.md "wikilink")：《青燐屑》