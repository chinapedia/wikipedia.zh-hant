**阿莱奇冰川**（）位于[瑞士南部](../Page/瑞士.md "wikilink")，是[阿尔卑斯山最大的](../Page/阿尔卑斯山.md "wikilink")[冰川](../Page/冰川.md "wikilink")，从[僧侶峰的南面一直延伸到上罗纳山谷](../Page/僧侶峰.md "wikilink")，面积超过120平方公里。[阿莱奇峰位于其西面](../Page/阿莱奇峰.md "wikilink")，[罗讷河在山峰的南侧流淌](../Page/罗讷河.md "wikilink")。

[Aletsch_Glacier_(Jungfraufirn).jpg](https://zh.wikipedia.org/wiki/File:Aletsch_Glacier_\(Jungfraufirn\).jpg "fig:Aletsch_Glacier_(Jungfraufirn).jpg")

## 全景图

[070915_Panorama_Aletschgletscher_von_Konkordiahütte.jpg](https://zh.wikipedia.org/wiki/File:070915_Panorama_Aletschgletscher_von_Konkordiahütte.jpg "fig:070915_Panorama_Aletschgletscher_von_Konkordiahütte.jpg")

## 全球变暖的影响

[8.06468E_46.45776N.gif](https://zh.wikipedia.org/wiki/File:8.06468E_46.45776N.gif "fig:8.06468E_46.45776N.gif")
2005至2006年间，阿莱奇冰川消融了100米。科学家们预测，到2100年，全球气温将升高2-4.5[摄氏度](../Page/摄氏度.md "wikilink")，整个阿尔卑斯山脉将发生巨大变化，只有海拔4000米以上的冰川才能幸存下来。\[1\]

2007年8月18日，为了引起公众对[全球变暖和冰川消融的的关注](../Page/全球变暖.md "wikilink")，摄影师[斯潘塞·图尼克在阿莱奇冰川组织了](../Page/斯潘塞·图尼克.md "wikilink")600多人的[裸体摄影活动](../Page/裸体.md "wikilink")。\[2\]\[3\]

## 参见

  - [阿尔卑斯山脉](../Page/阿尔卑斯山脉.md "wikilink")
  - [阿尔卑斯山脉山峰列表](../Page/阿尔卑斯山脉山峰列表.md "wikilink")

## 参考资料

## 外部链接

  - [Jungfrau-Aletsch-Bietschhorn official
    website](https://web.archive.org/web/20080423132819/http://www.welterbe.ch/en.html?no_cache=1)

  - [Grosser Aletschgletscher on Glaciers
    online](http://www.swisseduc.ch/glaciers/alps/grosser_aletschgletscher/index-en.html)

  - [Panoramic drawing of area including
    hikes](https://web.archive.org/web/20060202232610/http://www.bettmeralp.ch/d/sommer/wanderkarte.html)

[Category:歐洲冰河](../Category/歐洲冰河.md "wikilink")
[Category:瑞士地形](../Category/瑞士地形.md "wikilink")
[J](../Category/瑞士世界遺產.md "wikilink")
[Category:阿爾卑斯山脈](../Category/阿爾卑斯山脈.md "wikilink")

1.  Swissinfo:
    [《瑞士专家预测：2100年冰川有可能全部消融》](http://www.swissinfo.org/chi/swissinfo.html?siteSect=881&sid=8031096)，2007年7月20日发表，2008年3月2日查阅。
2.  搜狐旅游：[《600多人瑞士冰川“裸拍”》](http://travel.sohu.com/20070820/n251668097.shtml)，2007年08月20日发表，2008年3月2日查阅。
3.  Swissinfo: [《Volunteers strip off to fight climate
    change》](http://www.swissinfo.org/eng/front/detail/Volunteers_strip_down_to_fight_climate_change.html?siteSect=105&sid=8116840)，2007年8月18日发表，2008年3月2日查阅。