**蒿属**（[学名](../Page/学名.md "wikilink")：），又名**艾属**，是[菊科中一个大](../Page/菊科.md "wikilink")[属](../Page/属.md "wikilink")，约有200—400[种](../Page/种.md "wikilink")，其中包括许多种常见的[药草](../Page/药草.md "wikilink")，以1、2年生[草本植物为主](../Page/草本植物.md "wikilink")，多有浓烈的香味，葉上多具白色絨毛。蒿属主要生长在[亚洲和](../Page/亚洲.md "wikilink")[欧洲以及](../Page/欧洲.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")，少数分布在非洲北部和大洋洲地区，纬度跨度大，在温带、寒温带及亚热带地区均有分布。

## 名稱來源

學名 Artemisia 來源於古希臘女神[阿耳忒弥斯](../Page/阿耳忒弥斯.md "wikilink")。\[1\]亦有說法指於公元前
350 年間去世的一名著名植物學家、醫學研究者[阿爾特米西亞二世](../Page/阿爾特米西亞二世.md "wikilink")。\[2\]

## 部分物種

  - [鹼蒿](../Page/鹼蒿.md "wikilink") *Artemisia abrotanum*
  - [中亞苦蒿](../Page/中亞苦蒿.md "wikilink") *Artemisia absinthium*
  - [東北絲裂蒿](../Page/東北絲裂蒿.md "wikilink") *Artemisia adamsii*
  - [黃花蒿](../Page/黃花蒿.md "wikilink") *Artemisia annua*
  - [沙篙](../Page/沙篙.md "wikilink") *Artemisia arenaria*
  - [艾草](../Page/艾草.md "wikilink") *Artemisia argyi*
  - [銀葉蒿](../Page/銀葉蒿.md "wikilink") *Artemisia argyrophylla*
  - [褐頭蒿](../Page/褐頭蒿.md "wikilink") *Artemisia aschurbajewii*
  - [銀蒿](../Page/銀蒿.md "wikilink") *Artemisia austriaca*
  - [巴爾古津蒿](../Page/巴爾古津蒿.md "wikilink") *Artemisia bargusinensis*
  - [矮叢蒿](../Page/矮叢蒿.md "wikilink") *Artemisia caespitosa*
  - [荒野蒿](../Page/荒野蒿.md "wikilink") *Artemisia campestris*
  - [茵蔯蒿](../Page/茵蔯蒿.md "wikilink") *Artemisia capillaris*
  - [青蒿](../Page/青蒿.md "wikilink") *Artemisia carvifolia*
  - [蛔蒿](../Page/蛔蒿.md "wikilink") *Artemisia cina*
  - [纖杆蒿](../Page/纖杆蒿.md "wikilink") *Artemisia demissa*
  - [中亞草原蒿](../Page/中亞草原蒿.md "wikilink") *Artemisia depauperata*
  - [沙蒿](../Page/沙蒿.md "wikilink") *Artemisia desertorum*
  - [龍蒿](../Page/龍蒿.md "wikilink") *Artemisia dracunculus*
  - [牛尾蒿](../Page/牛尾蒿.md "wikilink") *Artemisia dubia*
  - [綠櫛齒葉蒿](../Page/綠櫛齒葉蒿.md "wikilink") *Artemisia freyniana*
  - [冷蒿](../Page/冷蒿.md "wikilink") *Artemisia frigida*
  - [亮綠蒿](../Page/亮綠蒿.md "wikilink") *Artemisia glabella*
  - [細裂葉蓮蒿](../Page/細裂葉蓮蒿.md "wikilink") *Artemisia gmelinii*
  - [纖細絹蒿](../Page/纖細絹蒿.md "wikilink") *Artemisia gracilescens*
  - [鹽蒿](../Page/鹽蒿.md "wikilink") *Artemisia halodendron*
  - [五月艾](../Page/五月艾.md "wikilink") *Artemisia indica*
  - [柳葉蒿](../Page/柳葉蒿.md "wikilink") *Artemisia integrifolia*
  - [牡蒿](../Page/牡蒿.md "wikilink") *Artemisia japonica*
  - [新疆絹蒿](../Page/新疆絹蒿.md "wikilink") *Artemisia kaschgarica*
  - [菴閭](../Page/菴閭.md "wikilink") *Artemisia keiskeana*
  - [掌裂蒿](../Page/掌裂蒿.md "wikilink") *Artemisia kuschakewiczii*
  - [白苞蒿](../Page/白苞蒿.md "wikilink") *Artemisia lactiflora*
  - [白山蒿](../Page/白山蒿.md "wikilink") *Artemisia lagocephala*
  - [寬葉蒿](../Page/寬葉蒿.md "wikilink") *Artemisia latifolia*
  - [白葉蒿](../Page/白葉蒿.md "wikilink") *Artemisia leucophylla*
  - [濱海牡蒿](../Page/濱海牡蒿.md "wikilink") *Artemisia littoricola*
  - [細杆沙蒿](../Page/細杆沙蒿.md "wikilink") *Artemisia macilenta*
  - [亞洲大花蒿](../Page/亞洲大花蒿.md "wikilink") *Artemisia macrantha*
  - [大花蒿](../Page/大花蒿.md "wikilink") *Artemisia macrocephala*
  - [中亞旱蒿](../Page/中亞旱蒿.md "wikilink") *Artemisia marschalliana*
  - [尖櫛齒葉蒿](../Page/尖櫛齒葉蒿.md "wikilink") *Artemisia medioxima*
  - [墊型蒿](../Page/墊型蒿.md "wikilink") *Artemisia minor*
  - [蒙古蒿](../Page/蒙古蒿.md "wikilink") *Artemisia mongolica*
  - [矮濱蒿](../Page/矮濱蒿.md "wikilink") *Artemisia nakaii*
  - [玉山艾](../Page/玉山艾.md "wikilink") *Artemisia niitakayamensis*
  - [南亞蒿](../Page/南亞蒿.md "wikilink") *Artemisia nilagirica*
  - [藏旱蒿](../Page/藏旱蒿.md "wikilink") *Artemisia nortonii*
  - [鈍裂蒿](../Page/鈍裂蒿.md "wikilink") *Artemisia obtusiloba*
  - [川西腺毛蒿](../Page/川西腺毛蒿.md "wikilink") *Artemisia
    occidentalisichuanensis*
  - [黑沙蒿](../Page/黑沙蒿.md "wikilink") *Artemisia ordosica*
  - [滇東蒿](../Page/滇東蒿.md "wikilink") *Artemisia orientaliyunnanensis*
  - [黑蒿](../Page/黑蒿.md "wikilink") *Artemisia palustris*
  - [伊朗蒿](../Page/伊朗蒿.md "wikilink") *Artemisia persica*
  - [纖梗蒿](../Page/纖梗蒿.md "wikilink") *Artemisia pewzowii*
  - [褐苞蒿](../Page/褐苞蒿.md "wikilink") *Artemisia phaeolepis*
  - [西北蒿](../Page/西北蒿.md "wikilink") *Artemisia pontica*
  - [魁蒿](../Page/魁蒿.md "wikilink") *Artemisia princeps*
  - [灰苞蒿](../Page/灰苞蒿.md "wikilink") *Artemisia roxburghiana*
  - [紅足蒿](../Page/紅足蒿.md "wikilink") *Artemisia rubripes*
  - [岩蒿](../Page/岩蒿.md "wikilink") *Artemisia rupestris*
  - [香葉蒿](../Page/香葉蒿.md "wikilink") *Artemisia rutifolia*
  - [白蓮蒿](../Page/白蓮蒿.md "wikilink") *Artemisia sacrorum*
  - [崑崙沙蒿](../Page/崑崙沙蒿.md "wikilink") *Artemisia saposhnikovii*
  - [豬毛蒿](../Page/豬毛蒿.md "wikilink") *Artemisia scoparia*
  - [絹毛蒿](../Page/絹毛蒿.md "wikilink") *Artemisia sericea*
  - [大籽蒿](../Page/大籽蒿.md "wikilink") *Artemisia sieversiana*
  - [準噶爾沙蒿](../Page/準噶爾沙蒿.md "wikilink") *Artemisia songarica*
  - [寬葉山蒿](../Page/寬葉山蒿.md "wikilink") *Artemisia stolonifera*
  - [俄羅斯肉質葉蒿](../Page/俄羅斯肉質葉蒿.md "wikilink") *Artemisia succulenta*
  - [陰地蒿](../Page/陰地蒿.md "wikilink") *Artemisia sylvatica*
  - [裂葉蒿](../Page/裂葉蒿.md "wikilink") *Artemisia tanacetifolia*
  - [濕地蒿](../Page/濕地蒿.md "wikilink") *Artemisia tournefortiana*
  - [三齒蒿](../Page/三齒蒿.md "wikilink") *Artemisia tridentata*
  - [北艾](../Page/北艾.md "wikilink") *Artemisia vulgaris*
  - [藏龍蒿](../Page/藏龍蒿.md "wikilink") *Artemisia waltonii*
  - [烏丹蒿](../Page/烏丹蒿.md "wikilink") *Artemisia wudanica*
  - [內蒙古旱蒿](../Page/內蒙古旱蒿.md "wikilink") *Artemisia xerophytica*
  - [亞東蒿](../Page/亞東蒿.md "wikilink") *Artemisia yadongensis*
  - [藏白蒿](../Page/藏白蒿.md "wikilink") *Artemisia younghusbandii*
  - [察隅蒿](../Page/察隅蒿.md "wikilink") *Artemisia zayuensis*
  - [蒔蘿蒿](../Page/蒔蘿蒿.md "wikilink") *Artemisia zhaodongensis*
  - [中甸艾](../Page/中甸艾.md "wikilink") *Artemisia zhongdianensis*

### 原本歸類於蒿屬的物種

  - [石胡荽](../Page/石胡荽.md "wikilink") *Centipeda minima*
  - *Eupatorium capillifolium*
  - [線葉菊](../Page/線葉菊.md "wikilink") *Filifolium sibiricum*
  - [田基黃](../Page/田基黃.md "wikilink") *Grangea maderaspatana*
  - [同花母菊](../Page/同花母菊.md "wikilink") *Matricaria discoidea*

## 參考資料

## 扩展阅读

  -
[\*](../Category/蒿属.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.
2.