**防巳**在[神農本草經中記載之名稱為](../Page/神農本草經.md "wikilink")**防巳**（[漢語拼音](../Page/漢語拼音.md "wikilink")：si），[晉](../Page/晉朝.md "wikilink")、[唐](../Page/唐.md "wikilink")、[宋等均以](../Page/宋朝.md "wikilink")**防巳**為名。**防<font color=brown>已</font>**（漢語拼音：yi）則為[明朝](../Page/明朝.md "wikilink")[李時珍之](../Page/李時珍.md "wikilink")[本草綱目](../Page/本草綱目.md "wikilink")（1578年）始誤植，至[民國復刻之本草綱目進一步錯植為](../Page/民國紀元.md "wikilink")**防<font color=red>己</font>**（漢語拼音：ji）。\[1\]

## 异名

汉防巳《儒门事亲》，瓜防巳《本草原始》

## 基原

为防巳科千金藤属植物粉防巳的乾燥块根。通常在秋季採揭，洗淨去皮、曬至半乾狀態再切段、乾燥。

## 原植物

[粉防巳Stephania](../Page/粉防己.md "wikilink") tetrandra Moore

## 性味与归经

苦、辛，寒。归膀胱、肺、脾经。

## 功效

祛除风湿，利水消肿。

## 註釋

## 参考资料

  - 南京中医药大学编著，《中药大辞典》\[M\]，上海：上海科学技术出版社，2006.3:490. ISBN 7-5323-8271-0

## 外部連結

  - [粉防己
    Fenfangji](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01391)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [防己
    Fangji](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00055)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [防己 Fang
    Ji](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00731)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

[Category:中草藥](../Category/中草藥.md "wikilink")

1.  [防巳、防已、防己之種種問題及其相關生藥製劑之應用](http://www.ohayoo.com.tw/防巳、防已、防己之種種問題及其相關生藥製劑之應用.htm)