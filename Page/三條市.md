**三條市**（）為一位于[日本](../Page/日本.md "wikilink")[新潟縣中央的城市](../Page/新潟縣.md "wikilink")。市區面積432.01平方公里，總人口103,554人。為新潟縣內少數的幾個工業城市。目前是縣內人口第五位的城市。施行市制于1934年1月1日。2005年5月1日，三條市、南蒲原郡榮町和南蒲原郡下田村合併為新「三條市」。

## 出身者

  - [金子千尋](../Page/金子千尋.md "wikilink") -
    日本職業棒球選手，效力於[歐力士野牛](../Page/歐力士野牛.md "wikilink")
  - [酒井高德](../Page/酒井高德.md "wikilink") -
    職業足球選手(出生於[美國](../Page/美國.md "wikilink"))
  - [水野久美](../Page/水野久美.md "wikilink") - 女演員
  - [巨人馬場](../Page/巨人馬場.md "wikilink") - 日本前職業摔角選手

## 外部連結

  - [三條市](http://www.city.sanjo.niigata.jp/)
  - [三條・榮・下田合併協議會](https://web.archive.org/web/20070810101410/http://www.city.sanjo.niigata.jp/~gappei-kyo/)

[Category:三條市](../Category/三條市.md "wikilink")