[Omayyad_mosque.jpg](https://zh.wikipedia.org/wiki/File:Omayyad_mosque.jpg "fig:Omayyad_mosque.jpg")
[Mosque.jpg](https://zh.wikipedia.org/wiki/File:Mosque.jpg "fig:Mosque.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Shrine_of_John_the_Baptist,_Great_Umayyid_Mosque,_Damascus.jpg "fig:缩略图")
**倭马亚大清真寺**（始建于705年）是[伊斯兰教最主要的](../Page/伊斯兰教.md "wikilink")[清真寺之一](../Page/清真寺.md "wikilink")，位于[叙利亚首都](../Page/叙利亚.md "wikilink")[大马士革](../Page/大马士革.md "wikilink")。该寺被认为是伊斯兰教的第四大圣寺。

倭马亚清真寺最早是[罗马帝国时期的](../Page/罗马帝国.md "wikilink")[朱庇特神殿](../Page/朱庇特.md "wikilink")，后来在罗马帝国定[基督教为国教后改为](../Page/基督教.md "wikilink")**圣约翰大教堂**（纪念[施洗约翰](../Page/施洗約翰.md "wikilink")），为正教会[安提阿牧首区下大马士革主教的主教座堂](../Page/安提阿牧首.md "wikilink")。叙利亚于7世纪为[阿拉伯人征服后](../Page/阿拉伯人.md "wikilink")，圣约翰大教堂一度仍归基督教徒做[礼拜之用](../Page/礼拜.md "wikilink")。705年，[倭马亚王朝的](../Page/倭马亚王朝.md "wikilink")[哈里发](../Page/哈里发.md "wikilink")[瓦利德一世接收了这座教堂](../Page/瓦利德一世.md "wikilink")，将其改为清真寺。瓦利德一世从[拜占庭](../Page/拜占庭帝国.md "wikilink")、叙利亚、[埃及等地招集工匠](../Page/埃及.md "wikilink")，历时十年将清真寺建成。

在改造过程中，教堂的长方形布局被保存下来，但其余部分有多少被留用则不得而知。可以肯定的是，倭马亚清真寺有两座尖塔原来是教堂的望楼，第三座光塔则为瓦利德一世所建。

历史上，倭马亚清真寺曾经三次被焚毁。第一次是在1069年清真寺发生火灾；第二次是在1400年，[中亚的征服者](../Page/中亚.md "wikilink")[帖木儿纵火烧毁了清真寺](../Page/帖木儿.md "wikilink")；第三次是在1893年，清真寺再度发生火灾。

今天，倭马亚清真寺中同时保存有[施洗约翰和](../Page/施洗約翰.md "wikilink")[萨拉丁的陵墓](../Page/萨拉丁.md "wikilink")。

## 参考文献

[Category:敘利亞清真寺](../Category/敘利亞清真寺.md "wikilink")
[Category:国家清真寺](../Category/国家清真寺.md "wikilink")
[Category:由非穆斯林宗教場所轉變而成的清真寺](../Category/由非穆斯林宗教場所轉變而成的清真寺.md "wikilink")
[葬](../Category/施洗约翰.md "wikilink") [葬](../Category/萨拉丁.md "wikilink")
[Category:东正教主教座堂](../Category/东正教主教座堂.md "wikilink")