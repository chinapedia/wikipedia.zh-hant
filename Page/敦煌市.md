**敦煌市**位于中国[甘肃省西北部](../Page/甘肃省.md "wikilink")，是[酒泉市代管的一个](../Page/酒泉市.md "wikilink")[县級市](../Page/县級市.md "wikilink")，为[国家历史文化名城](../Page/国家历史文化名城.md "wikilink")，历来为[丝绸之路上的重镇](../Page/丝绸之路.md "wikilink")。以[敦煌石窟及](../Page/敦煌石窟.md "wikilink")[敦煌壁画而闻名天下](../Page/敦煌壁画.md "wikilink")，是[世界文化遗产](../Page/世界文化遗产.md "wikilink")[莫高窟和汉](../Page/莫高窟.md "wikilink")[长城边陲](../Page/长城.md "wikilink")[玉门关及](../Page/玉门关.md "wikilink")[阳关的所在地](../Page/阳关.md "wikilink")。
[Jiucenglou_of_Mogao_Caves.jpg](https://zh.wikipedia.org/wiki/File:Jiucenglou_of_Mogao_Caves.jpg "fig:Jiucenglou_of_Mogao_Caves.jpg")\]\]

## 历史

[Trade_in_silkroad.jpg](https://zh.wikipedia.org/wiki/File:Trade_in_silkroad.jpg "fig:Trade_in_silkroad.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Dunhuang-06.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:DunhuangHistorisierendeTanzaufführung.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Dunhuang_Gisela_Brantl_12.JPG "fig:缩略图")
[春秋时](../Page/春秋.md "wikilink")，此地因“地产好瓜”得名瓜州。[战国时](../Page/战国.md "wikilink")，月氏逐渐强大，吞并羌人，赶走乌孙，这里属[大月氏国](../Page/大月氏.md "wikilink")。

秦汉之际，雄踞漠北的[匈奴崛起](../Page/匈奴.md "wikilink")，打败月氏，占据敦煌。[西汉武帝时](../Page/西汉.md "wikilink")，经过[反击匈奴的战争](../Page/漢匈戰爭.md "wikilink")，迫使匈奴“远循”，[河西地区归入汉朝版图](../Page/河西走廊.md "wikilink")；元鼎六年（前111年）设置[敦煌郡](../Page/敦煌郡.md "wikilink")，意“敦，大也，煌，盛也”，为[河西四郡之一](../Page/河西四郡.md "wikilink")。[张骞通西域的](../Page/张骞.md "wikilink")“凿空”之行，开通了影响深远的[丝绸之路](../Page/丝绸之路.md "wikilink")。新莽时一度改称敦德。[东汉建立](../Page/东汉.md "wikilink")，复名敦煌郡。[窦融率河西归服](../Page/窦融.md "wikilink")。由於[北匈奴控制了](../Page/北匈奴.md "wikilink")[西域](../Page/西域.md "wikilink")，敦煌郡既担负着防御[匈奴进攻的重任](../Page/匈奴.md "wikilink")。这一时期，敦煌经济发展、地位提高，中央主管西域事务的护西域副尉长驻敦煌，这里成为统辖西域的军政中心。

[魏文帝](../Page/魏文帝.md "wikilink")[曹丕篡位以後](../Page/曹丕.md "wikilink")，派兵消灭了河西的割据势力，委派[尹奉为敦煌太守](../Page/尹奉.md "wikilink")。[西晋时敦煌出现了](../Page/西晋.md "wikilink")[索靖](../Page/索靖.md "wikilink")、[索袭](../Page/索袭.md "wikilink")、宋纤、氾腾等一批名儒。[前凉以这里地处大漠之中](../Page/前凉.md "wikilink")，而置沙州，后来成为[西凉国都](../Page/西凉.md "wikilink")。

[东晋前凉时将敦煌](../Page/东晋.md "wikilink")、晋昌、高昌三郡和西域都护、戊巳校尉、玉门大护军三营合设沙州；前秦二年（366年）始开凿[莫高窟](../Page/莫高窟.md "wikilink")；隆安四年（400年）[李暠建西凉国](../Page/李暠.md "wikilink")，初都敦煌。

[北魏初](../Page/北魏.md "wikilink")（439年）置敦煌镇，526年置瓜州，敦煌为治所。汉魏之际多战乱，但敦煌经济日渐繁荣，中原文化传播，佛教东渐，成为五凉文化中心。[北周初置沙州](../Page/北周.md "wikilink")，564年为鸣沙县，属敦煌郡，因鸣沙山得名。

[隋朝初废郡置瓜州](../Page/隋朝.md "wikilink")，初期[敦煌被游牧民族](../Page/敦煌.md "wikilink")[突厥及](../Page/突厥.md "wikilink")[吐谷浑占据](../Page/吐谷浑.md "wikilink")。[隋炀帝即位后](../Page/隋炀帝.md "wikilink")，出兵击破[吐谷浑](../Page/吐谷浑.md "wikilink")，[西突厥也被迫投降](../Page/西突厥.md "wikilink")，重新控制敦煌，[丝绸之路再度打通](../Page/丝绸之路.md "wikilink")。大业三年（607年）复置敦煌郡，罢鸣沙县复名敦煌县。

[唐朝武德二年](../Page/唐朝.md "wikilink")（619年）置沙州，敦煌进入兴盛时期。建中二年（781年），为[吐蕃占领](../Page/吐蕃.md "wikilink")，[唐宣宗](../Page/唐宣宗.md "wikilink")[大中二年](../Page/大中_\(唐朝\).md "wikilink")（848年），沙州人士[张议潮募兵集众起义](../Page/张议潮.md "wikilink")，经过血战夺回敦煌城，使河西地区重归唐王朝，大中五年（851年）唐廷在敦煌设置归义军，並任命张议潮为敦煌归义军[节度使](../Page/节度使.md "wikilink")。

[後梁末帝](../Page/後梁.md "wikilink")[朱友贞](../Page/朱友贞.md "wikilink")[乾化二年](../Page/乾化.md "wikilink")（914年）敦煌迎來一位新的統治者[曹議金](../Page/曹議金.md "wikilink")。他主張與近鄰修好並大力農牧業，使敦煌恢復了一派繁華的景象，[莫高窟的开始了一个以大洞窟开凿为主的時代](../Page/莫高窟.md "wikilink")。

[北宋景祐三年](../Page/北宋.md "wikilink")（1036年）敦煌为西夏占领，统治敦煌达191年。元灭西夏后，于至元十四年（1277年）复设沙州。1280年升为沙州路总管府，隶属甘肃行中书省。[明朝永乐三年](../Page/明朝.md "wikilink")（1405年）改为沙州卫，后增设罕东左卫。[嘉靖七年](../Page/嘉靖.md "wikilink")（1528年）因葉爾羌汗國攻擊，关闭[嘉峪关](../Page/嘉峪关.md "wikilink")，从此瓜州、沙州成為葉爾羌汗國領土。

[清朝雍正元年](../Page/清朝.md "wikilink")（1723年）设沙州所，旋升为沙州卫。雍正四年（1726年），从甘肃56州县移民2400多户到沙州屯垦。乾隆三十五年（1760年）改为敦煌县。1949年9月28日敦煌為[中國共產黨控制](../Page/中國共產黨.md "wikilink")，10月7日成立敦煌县人民政府，属酒泉地区。

1986年被[国务院列为](../Page/国务院.md "wikilink")[国家历史文化名城](../Page/国家历史文化名城.md "wikilink")。1987年9月28日撤县设敦煌市（县级），隶属于地级[酒泉市](../Page/酒泉市.md "wikilink")。

敦煌位于古代中国通往[西域](../Page/西域.md "wikilink")、[中亚和](../Page/中亚.md "wikilink")[欧洲的交通要道](../Page/欧洲.md "wikilink")——[丝绸之路上](../Page/丝绸之路.md "wikilink")，曾经拥有繁荣的商贸活动。境内有汉代所筑的两座[长城关隘](../Page/长城.md "wikilink")[玉门关和](../Page/玉门关.md "wikilink")[阳关](../Page/阳关.md "wikilink")，均为军事要塞，是汉唐王朝和西域的分界点。后人诗句“春风不度玉门关”、“西出阳关无故人”，以及[古琴名曲](../Page/古琴.md "wikilink")“阳关三叠”等，都是指此处。今两关及长城遗址尚存。附近还有[悬泉置遗址](../Page/悬泉置遗址.md "wikilink")，为汉晋时期的驿站遗址。

## 地理

敦煌地处[青藏高原北部边缘](../Page/青藏高原.md "wikilink")、[河西走廊的西端](../Page/河西走廊.md "wikilink")。[党河穿越境内](../Page/党河.md "wikilink")，两岸有[绿洲](../Page/绿洲.md "wikilink")，此外的四周均被[塔克拉玛干沙漠所环绕](../Page/塔克拉玛干沙漠.md "wikilink")。全市年平均气温9.5
℃，降水量42毫米。主要的作物是[棉花及](../Page/棉花.md "wikilink")[小麦](../Page/小麦.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[水果等](../Page/水果.md "wikilink")，为甘肃省重要产棉区之一。

### 气候

## 行政区划

下辖9个[镇](../Page/镇.md "wikilink")：

。

## 文化和旅游

  - [国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")：[鸣沙山](../Page/鸣沙山_\(敦煌\).md "wikilink")、[月牙泉](../Page/月牙泉.md "wikilink")
  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[莫高窟](../Page/莫高窟.md "wikilink")、[玉门关及长城烽燧遗址](../Page/玉门关及长城烽燧遗址.md "wikilink")、[悬泉置遗址](../Page/悬泉置遗址.md "wikilink")
  - [中国国家地质公园](../Page/中国国家地质公园.md "wikilink")：[敦煌雅丹国家地质公园](../Page/敦煌雅丹国家地质公园.md "wikilink")
  - [敦煌十八怪](../Page/敦煌十八怪.md "wikilink")：第一怪：[香水梨要放黑卖](../Page/香水梨.md "wikilink")；第二怪：[驴肉黄面拽门外](../Page/驴肉黄面.md "wikilink")；第三怪：[浆水面条解暑快](../Page/浆水面.md "wikilink")；第四怪：泡儿[油糕美味在](../Page/油糕.md "wikilink")；第五怪：三九[锁阳人参赛](../Page/锁阳.md "wikilink")；第六怪：[酒枣新鲜放不坏](../Page/酒枣.md "wikilink")；第七怪：[罗布麻茶人人爱](../Page/罗布麻.md "wikilink")；第八怪：[榆钱也是一道菜](../Page/榆钱.md "wikilink")。
  - [陽關博物館](../Page/陽關博物館.md "wikilink")

### 莫高窟

[缩略图](https://zh.wikipedia.org/wiki/File:Dunhuang_grottoes_\(Mogao_cave\).jpg "fig:缩略图")
[Sand_dunes_(1).jpg](https://zh.wikipedia.org/wiki/File:Sand_dunes_\(1\).jpg "fig:Sand_dunes_(1).jpg")
举世瞩目的[莫高窟则位于敦煌城区东南方向的鸣沙山上](../Page/莫高窟.md "wikilink")，于1987年被列入[世界遗产名录](../Page/世界遗产.md "wikilink")。莫高窟俗称“千佛洞”，开凿于[前秦](../Page/前秦.md "wikilink")，在以后的一千多年里均有续建，直至元朝，是世界上现存规模最大、保存最完好的[石窟寺](../Page/石窟.md "wikilink")。有洞窟近500座，[壁画数万平方米](../Page/壁画.md "wikilink")，内容以[佛教为主](../Page/佛教.md "wikilink")，美轮美奂，被称为“石窟艺术宝库”。20世纪初曾发现藏经洞，内藏4万多件经卷等文物，但在20世纪初大量流失至欧美国家。莫高窟西南的[西千佛洞以及相邻](../Page/西千佛洞.md "wikilink")[瓜州县](../Page/瓜州县.md "wikilink")[榆林窟亦相类似](../Page/榆林窟.md "wikilink")，附近相似的石窟艺术被统称为“[敦煌石窟](../Page/敦煌石窟.md "wikilink")”。目前，国际上已经兴起了以研究敦煌石窟及相关文化、历史为主的“[敦煌学](../Page/敦煌学.md "wikilink")”。中国政府亦在这里建立了国家级的[敦煌研究院](../Page/敦煌研究院.md "wikilink")。

### 鸣沙山和月牙泉

敦煌城南的[鸣沙山](../Page/鸣沙山.md "wikilink")—[月牙泉也是一处著名风景区](../Page/月牙泉.md "wikilink")。鸣沙山沙分五色，当有大量人群从沙山上下滑时可轰鸣作响。山中环抱有月牙泉，其形状宛若一弯新月，水色蔚蓝，清澈如镜。但一度因过度开发当地地下水资源，而导致水位不断下降。近年来，当地政府采取了一些措施，目前正在缓慢恢复中。

## 能源

[Dunhuang.champs.de.panneaux.solaires.jpg](https://zh.wikipedia.org/wiki/File:Dunhuang.champs.de.panneaux.solaires.jpg "fig:Dunhuang.champs.de.panneaux.solaires.jpg")
甘肃敦煌太阳能公园是约9平方公里的大型太阳能光伏电站，并产生10兆瓦的电力，地处西南部。这是中国最大的当时在2009年7月落成的750千伏的输电线路。

## 交通

  - [敦煌機場](../Page/敦煌機場.md "wikilink")，可達[兰州](../Page/兰州.md "wikilink")，[乌鲁木齐和](../Page/乌鲁木齐.md "wikilink")[西安](../Page/西安.md "wikilink")，[北京](../Page/北京.md "wikilink")，[杭州](../Page/杭州.md "wikilink")，[郑州](../Page/郑州.md "wikilink")，[天津](../Page/天津.md "wikilink")，[西宁](../Page/西宁.md "wikilink")，[库尔勒](../Page/库尔勒.md "wikilink")，[广州](../Page/广州.md "wikilink")，[上海](../Page/上海.md "wikilink")，[花土沟](../Page/花土沟.md "wikilink")，[香港](../Page/香港.md "wikilink")。\[1\]
  - 2006年3月3日[敦煌鐵路货车通车](../Page/敦煌鐵路.md "wikilink")，同年8月5日旅客列车通车。\[2\]
  - [敦煌站](../Page/敦煌站.md "wikilink")

## 参见

  - [莫高窟](../Page/莫高窟.md "wikilink")
  - [敦煌学](../Page/敦煌学.md "wikilink")

## 参考资料

## 外部連結

  - [中国敦煌市人民政府门户网站](http://www.dunhuang.gov.cn/)
  - [敦煌旅游网](http://www.dunhuangtour.com/)
  - [国际敦煌项目](https://web.archive.org/web/20070125225940/http://idp.nlc.gov.cn/idp.a4d)
  - [考證古籍史料：古代從長安到敦煌，要走多久？商隊和軍情的時間差多少？](http://wechatinchina.com/thread-683775-1-1.html)

[Category:敦煌](../Category/敦煌.md "wikilink")
[市](../Category/酒泉区县市.md "wikilink")
[酒泉](../Category/甘肃省县级市.md "wikilink")
[甘](../Category/国家历史文化名城.md "wikilink")

1.
2.