[CASA_880.JPG](https://zh.wikipedia.org/wiki/File:CASA_880.JPG "fig:CASA_880.JPG")
[HK_North_Point_King_s_Road_CASA_880.JPG](https://zh.wikipedia.org/wiki/File:HK_North_Point_King_s_Road_CASA_880.JPG "fig:HK_North_Point_King_s_Road_CASA_880.JPG")
[HK_Central_Wyndham_Street_Taxi_CASA_880_adv.jpg](https://zh.wikipedia.org/wiki/File:HK_Central_Wyndham_Street_Taxi_CASA_880_adv.jpg "fig:HK_Central_Wyndham_Street_Taxi_CASA_880_adv.jpg")
**CASA
880**是[香港一座](../Page/香港.md "wikilink")37層高的單幢式住宅，位於[香港島](../Page/香港島.md "wikilink")[東區](../Page/東區_\(香港\).md "wikilink")[鰂魚涌](../Page/鰂魚涌.md "wikilink")\[1\]，鄰近[港鐵](../Page/港鐵.md "wikilink")[鰂魚涌站](../Page/鰂魚涌站.md "wikilink")\[2\]，亦是該區六年來的新屋苑。「Casa」一詞來自[西班牙語](../Page/西班牙語.md "wikilink")，意思為房子，發展商希望以西班牙名字突顯該物業的[南歐風格特色](../Page/南歐.md "wikilink")；「880」則象徵英皇道880號，為該大廈的所在門牌號碼。CASA
880因此得名\[3\]。

值得留意是屋苑前臨繁忙的英皇道，位於高層的單位也要飽受車聲困擾。而且，屋苑屬於單橦樓，發水面積相當高，做成客房細小但窗台大的現象。

## 物業資料

CASA
880是香港少數採用[玻璃幕牆的住宅](../Page/玻璃幕牆.md "wikilink")\[4\]，發展商為[長江實業](../Page/長江實業.md "wikilink")，在2008年5月入伙\[5\]，管理公司為高衞物業管理公司。CASA
880之前身為一座名麗城大廈的私人樓宇，全座為單一業主擁有，在1978年落成，於2003年拆卸，長實於2004年初以2.4億元購入，2007年重建後提供108單位。

CASA
880的位置鄰近鰂魚涌[甲級寫字樓臨立的商業區](../Page/甲級寫字樓.md "wikilink")[太古坊](../Page/太古坊.md "wikilink")，附近建築有[香港殯儀館](../Page/香港殯儀館.md "wikilink")、公共屋村[模範村](../Page/模範村.md "wikilink")、[御皇臺和](../Page/御皇臺.md "wikilink")[北角官立小學等](../Page/北角官立小學.md "wikilink")。由於該區一帶住宅樓齡較高，且大部分為[啟德機場搬遷前落成](../Page/啟德機場.md "wikilink")，當時建築物受高度限制，因此CASA
880比周邊其他大廈存在一定高度差距，故亦有較佳的景觀。

### 結構

CASA
880的樓層由3樓至45樓，當中不設4、13、14、24、34、44字樓，一共有37層。每個單位面積界乎1,066至1,366[平方呎](../Page/平方呎.md "wikilink")，間隔為3至4房連[主人套房](../Page/主人套房.md "wikilink")。另外，最頂3層設計成[複式單位](../Page/複式單位.md "wikilink")，並命名為CASA
CASTELLINO\[6\]，面積由2,133至2,732平方呎不等，間隔為4至6房連雙套房。

### 設施

設有約10,000平方呎住客會所，名為Club Casa\[7\]，設施包括：

  - 1,500呎酒店式 Function Room
  - 25米戶外[泳池](../Page/泳池.md "wikilink")
  - pool-side lounge
  - 私人[健身室](../Page/健身室.md "wikilink")
  - 住客停車場

## 参考文献

[Category:東區單幢式住宅 (香港)](../Category/東區單幢式住宅_\(香港\).md "wikilink")
[Category:鰂魚涌](../Category/鰂魚涌.md "wikilink")
[Category:長江實業物業](../Category/長江實業物業.md "wikilink")

1.  <http://www.hkea.com.hk/pub/TransServ?leftEstateGo=Y&district=HQB%2C%E9%B0%82%E9%AD%9A%E6%B6%8C&estate=CASA+880>
2.
3.
4.  有線電視 《樓盤傳真》，24/5/2008
5.
6.
7.