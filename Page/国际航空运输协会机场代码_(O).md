| 代碼  | 機場                                                                 | 城市                                          | 国家和地区                                |
| --- | ------------------------------------------------------------------ | ------------------------------------------- | ------------------------------------ |
| OAK | [奧克蘭國際機場](../Page/奧克蘭國際機場.md "wikilink")                           | [奥克兰](../Page/奥克蘭_\(加利福尼亞州\).md "wikilink") | [美國](../Page/美國.md "wikilink")       |
| OBO | [帶廣機場](../Page/帶廣機場.md "wikilink")                                 | [帶廣市](../Page/帶廣市.md "wikilink")            | [日本](../Page/日本.md "wikilink")       |
| ODS | [敖德薩國際機場](../Page/敖德薩國際機場.md "wikilink")                           | [敖德薩](../Page/敖德薩.md "wikilink")            | [烏克蘭](../Page/烏克蘭.md "wikilink")     |
| OIT | [大分機場](../Page/大分機場.md "wikilink")                                 | [大分縣](../Page/大分縣.md "wikilink")            | [日本](../Page/日本.md "wikilink")       |
| OIR | [奧尻機場](../Page/奧尻機場.md "wikilink")                                 | [奧尻町](../Page/奧尻町.md "wikilink")            | [日本](../Page/日本.md "wikilink")       |
| OLM | [奧林匹亞機場](../Page/奧林匹亞機場.md "wikilink")                             | [奧林匹亞](../Page/奧林匹亞_\(華盛頓州\).md "wikilink") | [美國](../Page/美國.md "wikilink")       |
| OKA | [那霸機場](../Page/那霸機場.md "wikilink")                                 | [那霸市](../Page/那霸市.md "wikilink")            | [日本](../Page/日本.md "wikilink")       |
| OKJ | [岡山機場](../Page/岡山機場.md "wikilink")                                 | [岡山縣](../Page/岡山縣.md "wikilink")            | [日本](../Page/日本.md "wikilink")       |
| ORD | [芝加哥奥黑尔国际机场](../Page/芝加哥奥黑尔国际机场.md "wikilink")                     | [芝加哥](../Page/芝加哥.md "wikilink")            | [美國](../Page/美國.md "wikilink")       |
| ORF | [諾福克國際機場](../Page/諾福克國際機場.md "wikilink")                           | [諾福克](../Page/諾福克_\(維吉尼亞州\).md "wikilink")  | [美國](../Page/美國.md "wikilink")       |
| OAG | Springhill                                                         | Orange                                      | [澳大利亞](../Page/澳大利亞.md "wikilink")   |
| OAX | Xoxocotlan                                                         | Oaxaca                                      | [墨西哥](../Page/墨西哥.md "wikilink")     |
| ODE | Odense                                                             | Odense                                      | [丹麥](../Page/丹麥.md "wikilink")       |
| ODW |                                                                    | Oak Harbor                                  | [美國](../Page/美國.md "wikilink")       |
| OGS |                                                                    | Ogdensburg                                  | [美國](../Page/美國.md "wikilink")       |
| OHD | Ohrid                                                              | Ohrid                                       | [馬其頓](../Page/馬其頓.md "wikilink")     |
| OKC | Will Rogers World Airport                                          | 奧克拉荷馬市                                      | [美國](../Page/美國.md "wikilink")       |
| OMA | Eppley Airfield                                                    | 歐瑪哈                                         | [美國](../Page/美國.md "wikilink")       |
| OMR | Oradea                                                             | Oradea                                      | [羅馬尼亞](../Page/羅馬尼亞.md "wikilink")   |
| ONT | 安大略 International                                                  | 安大略                                         | [美國](../Page/美國.md "wikilink")       |
| ORB | Orebro                                                             | Orebro                                      | [瑞典](../Page/瑞典.md "wikilink")       |
| ORN | Es Senia                                                           | Oran                                        | [阿爾及利亞](../Page/阿爾及利亞.md "wikilink") |
| OSD | Froesoe                                                            | Ostersund                                   | [瑞典](../Page/瑞典.md "wikilink")       |
| OSH | Wittman Field                                                      | Oshkosh                                     | [美國](../Page/美國.md "wikilink")       |
| OTM | Ottumwa Industrial Airport                                         | Ottumwa                                     | [美國](../Page/美國.md "wikilink")       |
| OTP | [布加勒斯特奧托佩尼-亨利·科安德國際機場](../Page/布加勒斯特奧托佩尼-亨利·科安德國際機場.md "wikilink") | [布加勒斯特](../Page/布加勒斯特.md "wikilink")        | [羅馬尼亞](../Page/羅馬尼亞.md "wikilink")   |
| OUA | Ouagadougou                                                        | Ouagadougou                                 | [伯基納法索](../Page/伯基納法索.md "wikilink") |
| OUL | Oulu                                                               | Oulu                                        | [芬蘭](../Page/芬蘭.md "wikilink")       |
| OWB |                                                                    | Owensboro                                   | [美國](../Page/美國.md "wikilink")       |
| OXR | Oxnard Airport                                                     | Oxnard / Ventura                            | [美國](../Page/美國.md "wikilink")       |
| OZZ | Ouarzazate                                                         | Ouarzazate                                  | [摩洛哥](../Page/摩洛哥.md "wikilink")     |

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")