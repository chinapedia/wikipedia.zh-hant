**格蘭特縣**（**Grant County,
Washington**）是[美國](../Page/美國.md "wikilink")[華盛頓州中部偏東的一個縣](../Page/華盛頓州.md "wikilink")，[哥倫比亞河在構成了西部邊界](../Page/哥倫比亞河.md "wikilink")。根據[美國2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，共有人口89,120。\[1\]縣治[埃夫拉塔](../Page/埃夫拉塔_\(華盛頓州\).md "wikilink")。

於1909年2月24日從[道格拉斯縣分出](../Page/道格拉斯縣_\(華盛頓州\).md "wikilink")。\[2\]縣名紀念[南北戰爭北軍將領](../Page/南北戰爭.md "wikilink")、後來成為第十八任總統的[尤里西斯·格蘭特](../Page/尤里西斯·格蘭特.md "wikilink")。

## 地理

根據[美国人口调查局](../Page/美国人口调查局.md "wikilink")，本郡的總面積為，其中陸地面積占、水域面積占 （4.0%）。

### 地理特色

  - [哥倫比亞河](../Page/哥倫比亞河.md "wikilink")

  - [摩西湖](../Page/摩西湖.md "wikilink")

  -
  - [大古力](../Page/大古力.md "wikilink")

### 主要公路

  - [I-90.svg](https://zh.wikipedia.org/wiki/File:I-90.svg "fig:I-90.svg")
    [90號州際公路](../Page/90號州際公路.md "wikilink")
  - [US_2.svg](https://zh.wikipedia.org/wiki/File:US_2.svg "fig:US_2.svg")
    [美國國道2](../Page/美國國道2.md "wikilink")
  - [WA-17.svg](https://zh.wikipedia.org/wiki/File:WA-17.svg "fig:WA-17.svg")
    [17號華盛頓州州道](../Page/17號華盛頓州州道.md "wikilink")
  - [WA-28.svg](https://zh.wikipedia.org/wiki/File:WA-28.svg "fig:WA-28.svg")
    [28號華盛頓州州道](../Page/28號華盛頓州州道.md "wikilink")

### 毗鄰郡

  - 北－[道格拉斯縣](../Page/道格拉斯縣_\(華盛頓州\).md "wikilink")
  - 東北－[奧卡諾根縣](../Page/奧卡諾根縣.md "wikilink")
  - 東－[林肯縣](../Page/林肯縣_\(華盛頓州\).md "wikilink")
  - 東－[亚当斯县](../Page/亚当斯县_\(华盛顿州\).md "wikilink")
  - 東南－[富蘭克林縣](../Page/富蘭克林縣_\(華盛頓州\).md "wikilink")
  - 南－[本頓縣](../Page/本頓縣_\(華盛頓州\).md "wikilink")
  - 西南－[雅基馬縣](../Page/雅基馬縣.md "wikilink")
  - 西－[基帝塔什縣](../Page/基帝塔什縣.md "wikilink")

### 國家保護區

  - （部分）

  - （部分）

  - （部分）

  - （部分）

## 資料來源

## 外部連結

  - [格蘭特縣](http://www.co.grant.wa.us/)

  - [格蘭特縣相片集](http://ncwportal.com/grant/thumbs)

  - [格蘭特縣交通局](http://www.gta-ride.com/)

[Grant County](../Category/华盛顿州行政区划.md "wikilink")

1.
2.