**胡锦涛**（）\[1\]\[2\]
，男，汉族，[中国共产党党员](../Page/中国共产党.md "wikilink")，[中华人民共和国政治家](../Page/中华人民共和国.md "wikilink")、水利工程师。祖籍[安徽](../Page/安徽.md "wikilink")[绩溪](../Page/绩溪.md "wikilink")[龍川村](../Page/龍川村.md "wikilink")，1942年12月出生于[江苏省](../Page/江苏省.md "wikilink")[泰縣](../Page/泰县_\(江苏县份\).md "wikilink")（今属[泰州市](../Page/泰州市.md "wikilink")）\[3\]，1964年4月加入中国共产党，1965年7月参加工作，毕业于[清华大学水利工程系河川枢纽电站专业](../Page/清华大学.md "wikilink")。曾任[中国共产党和](../Page/中国共产党.md "wikilink")[中華人民共和國的](../Page/中華人民共和國.md "wikilink")[主要领导职务及](../Page/党和国家领导人.md "wikilink")[最高领导人](../Page/中华人民共和国最高领导人.md "wikilink")，2002年11月至2012年11月间担任第[十六](../Page/中国共产党第十六届中央委员会.md "wikilink")、[十七届](../Page/中国共产党第十七届中央委员会.md "wikilink")[中国共产党中央委员会总书记](../Page/中国共产党中央委员会总书记.md "wikilink")。此外，胡锦涛在1992年11月至2012年11月间担任第[十四至](../Page/中国共产党第十四届中央委员会.md "wikilink")[十七届](../Page/中国共产党第十七届中央委员会.md "wikilink")[中共中央政治局常委](../Page/中国共产党中央政治局常务委员会.md "wikilink")，也是第十四和十五届[中共中央书记处排名第一的书记](../Page/中国共产党中央书记处.md "wikilink")（1992年10月－2002年11月）。国家职务上，于1998年3月至2013年3月间先后担任[中华人民共和国副主席](../Page/中华人民共和国副主席.md "wikilink")（1998-2003）、[主席](../Page/中华人民共和国主席.md "wikilink")（2003-2013）职务。军队方面于1999至2013年间先后担任[中共中央军委及](../Page/中国共产党中央军事委员会.md "wikilink")[国家军委副主席](../Page/中华人民共和国中央军事委员会.md "wikilink")、主席。中共官方将胡锦涛在任期间的[中央领导集体称为](../Page/中国共产党领导人世代.md "wikilink")“以胡锦涛同志为[总书记的](../Page/中国共产党中央委员会总书记.md "wikilink")[党中央](../Page/中国共产党中央委员会.md "wikilink")”\[4\]，及党和国家第四代领导集体。

其理论著作有[科学发展观](../Page/科学发展观.md "wikilink")、《[胡锦涛文选](../Page/胡锦涛文选.md "wikilink")》等。

夫人[刘永清](../Page/刘永清.md "wikilink")，育有一子一女。自身有胞妹两人。

## 家族背景

胡锦涛祖上曾出过[明朝](../Page/明朝.md "wikilink")[户部尚书](../Page/户部尚书.md "wikilink")[胡富](../Page/胡富.md "wikilink")、[兵部尚书](../Page/兵部尚书.md "wikilink")[胡宗宪等历史名人](../Page/胡宗宪.md "wikilink")，属[龙川胡氏一系](../Page/龙川村.md "wikilink")。胡锦涛的[高祖父胡允源](../Page/高祖父.md "wikilink")\[5\]来到江苏泰州后，开了一个小店。到胡锦涛[曾祖父胡树铭时已小有规模](../Page/曾祖父.md "wikilink")，又在上海、浙江等地开设了7个分店，并从事茶叶出口生意，生意兴隆时曾聘用多名[英语](../Page/英语.md "wikilink")[翻译](../Page/翻译.md "wikilink")\[6\]。

## 生平

### 早年

民國31年（1942年），胡锦涛出生於[江苏省](../Page/江苏省_\(中华民国\).md "wikilink")[泰縣](../Page/泰县_\(江苏县份\).md "wikilink")\[7\]\[8\]。7岁时，胡锦涛母[李文瑞去世](../Page/李文瑞.md "wikilink")，其为胡家独子，其下有妹妹[胡锦蓉与](../Page/胡锦蓉.md "wikilink")[胡锦莱](../Page/胡锦莱.md "wikilink")\[9\]。

1949年，中华人民共和国成立后，胡静之的茶庄随[公私合营被划归当地供销社](../Page/公私合营.md "wikilink")\[10\]。

他最初就读于五巷小学，后转入泰州大埔小学，1953年进入私立泰州中学（现泰州二中），1956年进入[江苏省泰州中学借读](../Page/江苏省泰州中学.md "wikilink")。根据其家族人回忆，胡锦涛小时很文静、喜爱读书、性格随和。他曾在履历表籍贯栏中填过“泰州”，受到父亲训导后从中学开始就一直填“安徽绩溪”\[11\]。胡锦涛在绩溪家乡的堂姐[胡锦霞表示](../Page/胡锦霞.md "wikilink")：胡锦涛看到她的籍贯填的也是“安徽绩溪”，父亲[胡增钰对他说](../Page/胡增钰.md "wikilink")：“我们老家是安徽绩溪，你长在江苏，祖籍是安徽。”此后，胡锦涛在一次全国团代会上，他对来自安徽的代表说：“我也是安徽人，我的老家在[徽州地区](../Page/徽州地区.md "wikilink")，我是绩溪人”\[12\]\[13\]。

### 大学时期

从1959年9月起，胡锦涛在[清华大学开启了大学生涯](../Page/清华大学.md "wikilink")，期间，他结识了之后的夫人[刘永清](../Page/刘永清.md "wikilink")\[14\]，他和夫人刘永清当时是水利工程系同年级学生中年龄最小的两位。在校期间，胡锦涛是出名的高材生，大学6年，除一门功课4分外，其余全是满分5分\[15\]。

胡锦涛在清华大学期间曾任学生文工团舞蹈队团支部书记\[16\]、水利系[政治辅导员](../Page/政治辅导员.md "wikilink")。1964年4月加入[中国共产党](../Page/中国共产党.md "wikilink")。同年，为庆祝中华人民共和国成立15周年，胡锦涛作为清华大学选派的100名学生参加了大型音乐舞蹈史诗《[东方红](../Page/东方红_\(音乐舞蹈史诗\).md "wikilink")》。在10月1日当天，胡锦涛又被选入清华大学游行队伍，参加了国庆大游行。之后，胡锦涛把参加《东方红》排练和游行的感受写成一篇文章，题为《上了生动的一课
毛泽东思想的颂歌——工人农民战士学生座谈音乐舞蹈史诗〈东方红〉》，发表在1964年10月6日的《[人民日报](../Page/人民日报.md "wikilink")》上。\[17\]

1965年大学毕业后，胡锦涛被学校留在水利系参加科研工作、任教，并继续担任政治辅导员。文革时期參與和傾向的是相对清華大學井岡山派更溫和保守的造反派──「四‧一四派」\[18\]。

### 步入政坛

1968年至1969年，胡锦涛在位于[甘肃的](../Page/甘肃.md "wikilink")[水电部](../Page/水电部.md "wikilink")[刘家峡工程局房建队参加了工作](../Page/刘家峡.md "wikilink")。1969年至1974年，在[水利部第四工程局八一三分局历任技术员](../Page/中华人民共和国水利部.md "wikilink")、秘书、机关党支部副书记\[19\]。1974年调任甘肃建设委员会秘书，因受到时任[中共甘肃省委书记](../Page/中共甘肃省委.md "wikilink")[宋平的赏识而进入政坛](../Page/宋平.md "wikilink")，此后历任[甘肃省建委设计管理处副处长](../Page/甘肃省.md "wikilink")、甘肃省建委副主任。1982年9月，胡锦涛调任[共青团甘肃省委书记](../Page/中国共产主义青年团.md "wikilink")。数月后担任[共青团中央书记处书记](../Page/中国共产主义青年团中央委员会.md "wikilink")、[全国青联主席](../Page/中华全国青年联合会.md "wikilink")。曾于1985年率青年團訪問[日本](../Page/日本.md "wikilink")，受到禮敬。1984年至1985年11月任共青团中央书记处第一书记。

### 主政地方

#### 贵州改革

当时中共一度考虑由胡锦涛出任[中共中央宣传部部长](../Page/中共中央宣传部.md "wikilink")，但考虑到当时中央的[团派成员不少](../Page/团派.md "wikilink")，即将[朱厚泽与胡锦涛互调位置](../Page/朱厚泽.md "wikilink")。1985年，胡锦涛调任[中共贵州省委书记](../Page/中共贵州省委.md "wikilink")。当时的贵州是中国最贫困的省份，1986年的贵州人均生产总值是461元人民币，在中国省级行政区中排名倒数第一。胡锦涛上任后，对贵州省开始大规模的机构改革，先后撤消了省人事局、劳动局、统计局、审计局、工商局、物价局等11个部门的党组，政府系统逐步建立行政首长负责制\[20\]。

1988年10月14日，[贵州大学发生进修学员殴打致伤本校学生事件](../Page/贵州大学.md "wikilink")。后来又发生贵州大学数千名学生上街游行示威，胡锦涛连夜召开会议，與示威大學生進行商討並進行協調，最终化解这一事件。

#### 西藏维稳

时任[中共西藏自治区党委书记的](../Page/中共西藏自治区党委.md "wikilink")[伍精华因为劳累过重](../Page/伍精华.md "wikilink")，在1988年6月、11月连续病倒。随后，中共中央调遣当时最年轻的（四十七岁）省委书记胡锦涛，担任中共西藏自治区党委书记。

而在胡锦涛上任不久之后，[第十世班禅额尔德尼在](../Page/第十世班禅额尔德尼.md "wikilink")1989年1月28日因为突发[心肌梗死于](../Page/心肌梗死.md "wikilink")[日喀则病逝](../Page/日喀则.md "wikilink")，享年51岁，西藏时局顿时异常复杂。在面对复杂的政治环境和极端事件的处理上，他的作为受到[邓小平等人的赞许](../Page/邓小平.md "wikilink")，也为此后的政治生涯，奠定了丰富的经验基础\[21\]。

### 政治局常委

1992年，50岁的胡锦涛进入[中共中央负责组织工作](../Page/中共中央.md "wikilink")。同年10月经中央政治局常委[宋平推荐及](../Page/宋平.md "wikilink")[邓力群提名](../Page/邓力群.md "wikilink")，在[中共十四大上当选](../Page/中共十四大.md "wikilink")[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")、[中共中央书记处书记](../Page/中共中央书记处.md "wikilink")（负责常务工作），晋升[正国级](../Page/國家級正職.md "wikilink")[党和国家领导人](../Page/党和国家领导人.md "wikilink")，正式成为[第三代领导集体的重要成员](../Page/中国共产党领导人世代#第三代.md "wikilink")。1993年2月，接替[乔石](../Page/乔石.md "wikilink")，任[中共中央党校校长](../Page/中共中央党校.md "wikilink")。

1998年3月15日，胡锦涛当选[中华人民共和国副主席](../Page/中华人民共和国副主席.md "wikilink")，1999年再任[中国共产党中央军事委员会副主席和](../Page/中国共产党中央军事委员会副主席.md "wikilink")[中华人民共和国中央军事委员会副主席](../Page/中华人民共和国中央军事委员会副主席.md "wikilink")\[22\]。1998年，胡锦涛兼任中央关于军队武警部队政法机关不再从事经商活动工作领导小组组长。2001年[911事件发生后](../Page/911事件.md "wikilink")，中国成立国家反恐怖工作协调小组，胡锦涛兼任组长\[23\]。

### 第四代领导人

[BRIC_leaders_in_2008.jpg](https://zh.wikipedia.org/wiki/File:BRIC_leaders_in_2008.jpg "fig:BRIC_leaders_in_2008.jpg")领导人，左起[辛格](../Page/曼莫汉·辛格.md "wikilink")、[梅德韦杰夫](../Page/梅德韦杰夫.md "wikilink")、[卢拉](../Page/路易斯·伊纳西奥·卢拉·达席尔瓦.md "wikilink")
\]\]
[Hu_Jintao_and_Barack_Obama_2009.jpg](https://zh.wikipedia.org/wiki/File:Hu_Jintao_and_Barack_Obama_2009.jpg "fig:Hu_Jintao_and_Barack_Obama_2009.jpg")在[2009年匹兹堡峰会](../Page/2009年二十国集团匹兹堡峰会.md "wikilink")\]\]

2002年11月，胡锦涛在[中共十六届一中全会上当选为第十六届](../Page/中国共产党第十六次全国代表大会.md "wikilink")[中国共产党中央委员会总书记](../Page/中国共产党中央委员会总书记.md "wikilink")，成为[中共中央主要负责人](../Page/中共中央主要负责人.md "wikilink")。2003年3月，在[第十届全国人民代表大会第一次会议上当选第六届](../Page/第十届全国人民代表大会.md "wikilink")[中华人民共和国主席](../Page/中华人民共和国主席.md "wikilink")，2004年和2005年在中共十六届四中全会和第十届全国人大第三次会议上分別当选[中共中央军委主席和](../Page/中共中央军委主席.md "wikilink")[国家军委主席](../Page/国家军委主席.md "wikilink")。2007年10月在[中共十七届一中全会上连任中央委员会总书记和中央军事委员会主席](../Page/中国共产党第十七次全国代表大会.md "wikilink")，2008年3月在[第十一屆全国人民代表大会上连任国家主席及国家军委主席](../Page/第十一屆全国人民代表大会.md "wikilink")。

胡锦涛在上任后的首次中共中央政治局常委会议强调“宪法的不可侵犯性”。中国政府也首次发布了关于中国社会动荡的数据，并赞同人民有知情权。在2003年[非典疫情中](../Page/SARS事件.md "wikilink")[胡锦涛政府行动果断](../Page/胡温体制.md "wikilink")，及时免去了卫生部部长[张文康和北京市市长](../Page/张文康.md "wikilink")[孟学农的官职](../Page/孟学农.md "wikilink")。以平民领导人的姿态受到了人们的称赞。在经济方面，取消了农民农业税，对民工采取更宽松的政策，率先扶持内陆城市，实施基本医保制度，增加城市地区的最低工资金额，建设经济适用房项目。\[24\]

#### 经济政策

[President_George_W._Bush_shakes_hands_with_President_Hu_Jintao.jpg](https://zh.wikipedia.org/wiki/File:President_George_W._Bush_shakes_hands_with_President_Hu_Jintao.jpg "fig:President_George_W._Bush_shakes_hands_with_President_Hu_Jintao.jpg")[布什握手](../Page/乔治·沃克·布什.md "wikilink")\]\]
胡锦涛注重社会经济的平衡和中国内陆地区资源的合理配置\[25\]。在其任内，[中国经济整体呈现出](../Page/中国经济.md "wikilink")“国进民退”的趋势。在民航、[钢铁](../Page/钢铁.md "wikilink")、[汽车](../Page/汽车.md "wikilink")、[房地产等领域](../Page/房地产.md "wikilink")，一些国有大型企业靠[政府](../Page/政府.md "wikilink")[政策倾斜及直接注资](../Page/政策.md "wikilink")，大规模吞并[民营企业](../Page/民营企业.md "wikilink")。08-09年间，政府为抵御[金融危机推出了](../Page/2007-2008年環球金融危機.md "wikilink")[扩大内需十项措施](../Page/扩大内需十项措施.md "wikilink")，但巨额的投资大部分流向地方政府和[国企](../Page/国企.md "wikilink")，使得[民营企业在危机中受到](../Page/民营企业.md "wikilink")[市场和国企的双重夹击](../Page/市场.md "wikilink")。\[26\]\[27\]

#### 民族问题

2009年9月29日，[国务院第五次全国民族团结进步表彰大会在北京举行](../Page/中华人民共和国国务院.md "wikilink")。胡锦涛以[党和国家领导人的身份发表了针对民族问题的讲话](../Page/党和国家领导人.md "wikilink")。\[28\]

#### 言论自由

执政期间，[工业和信息化部在](../Page/中华人民共和国工业和信息化部.md "wikilink")2009年7月起，对全国新销售的个人电脑预装上网过滤软件[绿坝·花季护航](../Page/绿坝·花季护航.md "wikilink")\[29\]，而后迫于民众压力，于同年8月取消大部分强制安装\[30\]。

#### 两岸关系

执政期间的2005年3月14日，[全国人民代表大会投票通过了](../Page/全国人民代表大会.md "wikilink")《[反分裂国家法](../Page/反分裂国家法.md "wikilink")》，被大多数中华人民共和国民众认为是从法理上维护了国家统一和领土完整\[31\]。

#### 人事变动

在胡锦涛任总书记期间下，有两位[中共中央政治局委员级别的官员被处理](../Page/中共中央政治局.md "wikilink")。2006年9月，原[中共上海市委书记](../Page/中共上海市委.md "wikilink")[陈良宇被免去职务并接受调查](../Page/陈良宇.md "wikilink")\[32\]。2012年3月，原[中共重庆市委书记](../Page/中共重庆市委.md "wikilink")[薄熙来被免去职务并接受调查](../Page/薄熙来.md "wikilink")\[33\]。

#### 外交政策

胡锦涛推行睦邻友好政策，主张全方位外交，重视对[东南亚的中国周边邻国改善关系](../Page/东南亚.md "wikilink")\[34\]。

2012年，针对日本政府“购买”[钓鱼岛](../Page/钓鱼岛.md "wikilink")，中国及时果断地采取了一系列反制措施；在[南海问题上](../Page/南海.md "wikilink")，中国适时妥善应对，同时积极推动与[东盟国家开展海上对话与合作](../Page/东盟.md "wikilink")，切实地维护了地区稳定。胡锦涛表示：「在钓鱼岛问题上，中方立场是一贯的、明确的。日方采取任何方式‘购岛’都是非法的、无效的，中方坚决反对。中国政府在维护领土主权问题上立场坚定不移。日方必须充分认识事态的严重性，不要作出错误的决定，同中方一道，维护中日关系发展大局。」\[35\]

#### 思想政治

[CPChonorAndShame.jpg](https://zh.wikipedia.org/wiki/File:CPChonorAndShame.jpg "fig:CPChonorAndShame.jpg")（后半部分）標語\]\]

继[毛泽东思想](../Page/毛泽东思想.md "wikilink")、[邓小平理论](../Page/邓小平理论.md "wikilink")、[江泽民提出的](../Page/江泽民.md "wikilink")[三个代表之后](../Page/三个代表.md "wikilink")，胡锦涛提出包括[科学发展观](../Page/科学发展观.md "wikilink")、[社会主义荣辱观](../Page/社会主义荣辱观.md "wikilink")、[和谐社会等理论](../Page/和谐社会.md "wikilink")，其中科学发展观已在[中共十七大上被写入](../Page/中共十七大.md "wikilink")[中国共产党章程](../Page/中国共产党章程.md "wikilink")，在[中共十八大上](../Page/中共十八大.md "wikilink")，科学发展观更被列为党的重要“指导思想”之一\[36\]。2018年3月「科學發展觀」寫入「中華人民共和國憲法」。

2008年1月15日，胡锦涛在[中国共产党第十七届中央纪律检查委员会第二次全体会议上发表讲话](../Page/中国共产党第十七届中央纪律检查委员会.md "wikilink")，聲稱今后会把反腐败工作的成效看作是取信于民的重要指标，这是中国进入转型发展近30年来第一次明确提出来的关于反腐败目标的看法\[37\]。执政期间，社会问题越来越尖锐，收入差距也不断扩大\[38\]。

## 卸任职务

[2017-10-19_习近平与胡锦涛握手.jpg](https://zh.wikipedia.org/wiki/File:2017-10-19_习近平与胡锦涛握手.jpg "fig:2017-10-19_习近平与胡锦涛握手.jpg")\]\]
根据中共確立的领导人最多连任两届的原则，胡锦涛在2012年11月召开的[中国共产党第十八次全国代表大会选出新一届](../Page/中国共产党第十八次全国代表大会.md "wikilink")[中央委员会后](../Page/中国共产党第十八届中央委员会.md "wikilink")\[39\]，卸任[中共中央总书记和](../Page/中共中央总书记.md "wikilink")[中共中央军委主席的职务](../Page/中共中央军委主席.md "wikilink")，由[习近平接任](../Page/习近平.md "wikilink")。胡锦涛成为[中華人民共和國成立后](../Page/中華人民共和國成立.md "wikilink")，首位一次交出所有权力的非终身制[最高领导人](../Page/中华人民共和国最高领导人.md "wikilink")\[40\]。在2013年3月召开的[十二届全国人民代表大会第一次会议上](../Page/第十二届全国人大.md "wikilink")，胡锦涛卸任[中华人民共和国主席和](../Page/中华人民共和国主席.md "wikilink")[国家中央军委主席的职务](../Page/国家中央军委主席.md "wikilink")，正式退休。

## 榮譽

  - 2008年，[德国Stern雜誌的lifestyle版](../Page/德国.md "wikilink")，評選为最有影响力100人，名列榜首\[41\]。

<!-- end list -->

  - 2010年，[美國](../Page/美國.md "wikilink")[福布斯雜誌公佈](../Page/福布斯.md "wikilink")[2010年全球最具权力人物排行榜](../Page/2010年全球最具权力人物排行榜.md "wikilink")，胡锦涛排名第一\[42\]。

## 评价

### 正面

  - 2012年6月5日，[人民日報海外版以](../Page/人民日報.md "wikilink")《黃金10年強國路》為題，稱過去的十年，是“黃金發展期，矛盾凸顯期”。文章還將[胡溫執政的十年定調為](../Page/胡溫體制.md "wikilink")“光榮綻放的十年”、“多難興邦的十年”、“人字大寫的十年”。\[43\]\[44\]

<!-- end list -->

  - [習近平剛上任中共中央總書記時表示](../Page/習近平.md "wikilink")，以胡錦濤同志為總書記的黨中央，團結帶領全國各族人民，取得了舉世矚目的輝煌成就。為了黨和人民事業的繼往開來，胡錦濤同志，以及[吳邦國](../Page/吳邦國.md "wikilink")、[溫家寶](../Page/溫家寶.md "wikilink")、[賈慶林](../Page/賈慶林.md "wikilink")、[李長春](../Page/李長春.md "wikilink")、[賀國強](../Page/賀國強.md "wikilink")、[周永康等同志](../Page/周永康.md "wikilink")，帶頭退出黨中央領導崗位，體現了崇高品德和高風亮節。\[45\]

<!-- end list -->

  - 美国第43任总统[乔治·沃克·布什在个人自传](../Page/乔治·沃克·布什.md "wikilink")《抉择时刻》评价世界各主要国家领导人时写到“他处事冷静、思维敏捷”、“是一位专注国家内部事务的实干家”\[46\]。

<!-- end list -->

  - [台北市長](../Page/台北市長.md "wikilink")[柯文哲認為](../Page/柯文哲.md "wikilink")，胡錦濤是[中國歷史上第一個](../Page/中國歷史.md "wikilink")，像[華盛頓一樣當完了](../Page/乔治·华盛顿.md "wikilink")[國家領導人就回家](../Page/國家領導人.md "wikilink")，現在不見了的人物\[47\]\[48\]。

### 負面

  - 美国[杂志](../Page/杂志.md "wikilink")《大观》认为总书记胡锦涛为首的[中国共产党领导层控制](../Page/中国共产党.md "wikilink")[舆论与强行镇压被中共官方所认定的](../Page/舆论.md "wikilink")[邪教组织](../Page/邪教.md "wikilink")\[49\]，在其2005年的十大[独裁者排名栏目中](../Page/独裁者.md "wikilink")，将胡列第四名，2006年降至第六，其后数年均名列十名内\[50\]。
  - [無國界記者指責胡錦濤以](../Page/無國界記者.md "wikilink")「[和諧社會](../Page/和諧社會.md "wikilink")」為藉口阻止[中國出現自由](../Page/中國.md "wikilink")[媒體](../Page/傳播媒體.md "wikilink")，把胡錦濤列入「[新聞自由掠奪者](../Page/新聞自由掠奪者.md "wikilink")」名單\[51\]。2012年，「無國界記者」报道，中共中央總書記胡錦濤下令囚禁68名網民和30名記者，是囚禁新聞自由捍衛者最多的領導人之一\[52\]。

### 其他

  - [悉尼大学中国政治学教授克里](../Page/悉尼大学.md "wikilink")·布朗认为，胡锦涛在其任内社会政治改革停步不前，但专注于经济方面的增长\[53\]。

## 家族

  - 妻子：[刘永清](../Page/刘永清.md "wikilink")（1940年10月3日 －），两人育有一子一女。\[54\]
      - 儿子：[胡海峰](../Page/胡海峰.md "wikilink")（1972年11月
        －），现任[中国共产党](../Page/中国共产党.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[丽水市市委书记](../Page/丽水市.md "wikilink")。
          - 儿媳：[王珺](../Page/王珺.md "wikilink")（1973年－），胡海峰的大学同学，1995年毕业于北方交通大学。1998年获得北京大学经济学硕士。2008年获得清华大学经济学博士。2009年6月，出任由前总理朱镕基担任创院院长的清华大学经济管理学院院长助理。
      - 女儿：[胡海清](../Page/胡海清.md "wikilink")（1973年12月 －）。

## 影视形象

电视剧《[历史转折中的邓小平](../Page/历史转折中的邓小平.md "wikilink")》第44集第36分钟时，出现一个未标明姓名的新任[中国共产党中央委员会候补委员](../Page/中国共产党中央委员会候补委员.md "wikilink")，根据他戴黑框眼镜、面型、发型等特征判断，相信是前[中共中央总书记胡锦涛](../Page/中共中央总书记.md "wikilink")\[55\]。有网民在微博说：“没提名字的年轻候补委员，几位元老都和他握手。演员有点胖了，不过这个造型倒是很容易让人想到是他。”\[56\]

## 著作

  - 《论构建社会主义和谐社会》，2013年4月发行
  - 《[胡锦涛文选](../Page/胡锦涛文选.md "wikilink")》，三卷本，2016年9月发行\[57\]

## 注释

## 參考文獻

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
  -
  -
## 外部链接

  - [中国共产党新闻网胡锦涛简介（多国语言）](http://cpc.people.com.cn/GB/64192/64197/5544641.html)
  - [中国领导干部资料库-胡锦涛](https://web.archive.org/web/20160611124341/http://gbzl.people.com.cn/grzy.php?id=121000277)
  - [新华网－领导人活动报道集－胡锦涛](http://www.xinhuanet.com/politics/leaders/hujintao/index.htm)
  - [新意文化网—胡锦涛专刊](http://www.newconcept.com/jixi/mingren/hujintao/index.html)
  - [BBC <News:China's> leader shows his
    stripes](http://news.bbc.co.uk/2/hi/asia-pacific/4165209.stm)
  - [胡主席指揮我們向前進（軍旅歌曲）](https://web.archive.org/web/20160423154605/http://www.cenews.eu/?p=21967)

## 参见

  - [胡温体制](../Page/胡温体制.md "wikilink")、[團派](../Page/團派.md "wikilink")、[令计划](../Page/令计划.md "wikilink")
  - [科学发展观](../Page/科学发展观.md "wikilink")、[和谐社会](../Page/和谐社会.md "wikilink")、[社会主义荣辱观](../Page/社会主义荣辱观.md "wikilink")
  - [胡六点](../Page/胡六点.md "wikilink")、《[反分裂国家法](../Page/反分裂国家法.md "wikilink")》、[胡连会](../Page/胡连会.md "wikilink")
  - 《[胡锦涛文选](../Page/胡锦涛文选.md "wikilink")》、《[胡锦涛传](../Page/胡锦涛传.md "wikilink")》

{{-}}                    |-

[胡锦涛](../Category/胡锦涛.md "wikilink")
[胡锦涛家族](../Category/胡锦涛家族.md "wikilink")
[Jintao](../Category/胡姓.md "wikilink")
[Category:姜堰人](../Category/姜堰人.md "wikilink")
[Category:绩溪人](../Category/绩溪人.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:中华人民共和国工程师](../Category/中华人民共和国工程师.md "wikilink")
[Category:中国共产党中央委员会总书记](../Category/中国共产党中央委员会总书记.md "wikilink")
[Category:中华人民共和国主席](../Category/中华人民共和国主席.md "wikilink")
[Category:中华人民共和国副主席](../Category/中华人民共和国副主席.md "wikilink")
[Category:中共中央党校校长](../Category/中共中央党校校长.md "wikilink")
[Category:中国共产党中央军事委员会主席](../Category/中国共产党中央军事委员会主席.md "wikilink")
[Category:中国共产党中央军事委员会副主席](../Category/中国共产党中央军事委员会副主席.md "wikilink")
[Category:中华人民共和国中央军事委员会主席](../Category/中华人民共和国中央军事委员会主席.md "wikilink")
[Category:中华人民共和国中央军事委员会副主席](../Category/中华人民共和国中央军事委员会副主席.md "wikilink")
[Category:中共西藏自治区党委书记](../Category/中共西藏自治区党委书记.md "wikilink")
[Category:中共贵州省委书记](../Category/中共贵州省委书记.md "wikilink")
[Category:中國共產主義青年團甘肅省委員會书记](../Category/中國共產主義青年團甘肅省委員會书记.md "wikilink")
[Category:第六届全国政协常务委员](../Category/第六届全国政协常务委员.md "wikilink")
[Category:第七届全国人大代表](../Category/第七届全国人大代表.md "wikilink")
[Category:第八届全国人大代表](../Category/第八届全国人大代表.md "wikilink")
[Category:第九届全国人大代表](../Category/第九届全国人大代表.md "wikilink")
[Category:第十届全国人大代表](../Category/第十届全国人大代表.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:江蘇省全國人民代表大會代表](../Category/江蘇省全國人民代表大會代表.md "wikilink")
[Category:西藏自治區全國人民代表大會代表](../Category/西藏自治區全國人民代表大會代表.md "wikilink")
[Category:貴州省全國人民代表大會代表](../Category/貴州省全國人民代表大會代表.md "wikilink")
[Category:时代百大人物](../Category/时代百大人物.md "wikilink")
[Category:中国红十字会名誉会长](../Category/中国红十字会名誉会长.md "wikilink")
[Category:中国共产党第十二届中央委员会委员](../Category/中国共产党第十二届中央委员会委员.md "wikilink")
[Category:中国共产党第十三届中央委员会委员](../Category/中国共产党第十三届中央委员会委员.md "wikilink")
[Category:中国共产党第十四届中央政治局常务委员会委员](../Category/中国共产党第十四届中央政治局常务委员会委员.md "wikilink")
[Category:中国共产党第十四届中央书记处书记](../Category/中国共产党第十四届中央书记处书记.md "wikilink")
[Category:中国共产党第十五届中央政治局常务委员会委员](../Category/中国共产党第十五届中央政治局常务委员会委员.md "wikilink")
[Category:中国共产党第十五届中央书记处书记](../Category/中国共产党第十五届中央书记处书记.md "wikilink")
[Category:中国共产党第十六届中央政治局常务委员会委员](../Category/中国共产党第十六届中央政治局常务委员会委员.md "wikilink")
[Category:中国共产党第十七届中央政治局常务委员会委员](../Category/中国共产党第十七届中央政治局常务委员会委员.md "wikilink")
[Category:全国青联主席](../Category/全国青联主席.md "wikilink")

1.

2.
3.

4.

5.  [北京派人取走了胡锦涛的所有档案](https://www.peacehall.com/news/gb/china/2010/04/201004261252.shtml)
    .博訊.

6.

7.
8.

9.

10.

11.
12.

13.

14.
15.

16. [Willy Wo-Lap Lam (2006)，第4页](../Page/#WWL.md "wikilink")

17.
18. [宋永毅：文革派別、思潮和中南海領導人](http://www.chengmingmag.com/t303/select/303sel20.html)


19.
20.
21.

22.

23.

24.

25.
26. [国进民退：激荡2009](http://www.infzm.com/content/39154) ，南方周末，2009年12月23日

27. [童大焕：国进民退的速度可能超乎想象](http://news.ifeng.com/opinion/economics/200912/1230_6437_1492280.shtml)
    ，凤凰网，2009年12月30日

28.

29. [7月1日起新售个人电脑将预装上网过滤软件](http://news.ifeng.com/mainland/200906/0609_17_1194067.shtml)
    ，凤凰网，2009年06月09日

30.

31. [中国通过《反分裂国家法》](http://tw.people.com.cn/GB/26741/42246/index.html)
    ，人民网

32.

33.

34.
35.

36.

37. [任建明：总书记强调把反腐工作成效看作取信于民的重要指标](http://cpc.people.com.cn/GB/66888/77791/6783889.html)
    ，中国共产党新闻网，2008年1月16日

38.
39.

40.

41. [Stern](http://www.stern.de/lifestyle/leute/:Park-Avenue-Top-100-Die-Menschen-Welt/618373.html)
     2009年4月10日查阅

42.

43.

44. \[<http://paper.people.com.cn/rmrbhwb/html/2012-06/05/content_1062431.htm>:
    黄金十年强国路（科学发展　成就辉煌）\]

45. \[<http://news.cntv.cn/2013/03/17/VIDE1363484763091596.shtml>:
    习近平：向胡锦涛同志表示衷心感谢和崇高敬意\]

46. \[<http://news.xinhuanet.com/world/2010-11/11/c_12760815.htm>:
    布什新书评各国领导人:胡锦涛实干普京冷血(图)\]

47.

48.

49. [独裁者排行榜](http://www.parade.com/dictators/2009/)

50.

51. [胡錦濤再上[新聞自由](../Page/新聞自由.md "wikilink")「掠奪者」名單](http://www.bbc.co.uk/zhongwen/trad/china/2010/05/100503_rsf_press_freedom.shtml)
     BBC 2010年5月3日

52.

53.

54.

55.

56.

57.