[Chen_Shufan.jpg](https://zh.wikipedia.org/wiki/File:Chen_Shufan.jpg "fig:Chen_Shufan.jpg")
**陈树藩**（），字**柏森**（柏生），[陕西](../Page/陕西.md "wikilink")[安康人](../Page/安康.md "wikilink")。民国[皖系军阀](../Page/皖系军阀.md "wikilink")，曾任陕西督军。

## 生平

陈树藩祖籍[湖南省](../Page/湖南省.md "wikilink")[長沙府](../Page/長沙府.md "wikilink")[宁乡县](../Page/宁乡县.md "wikilink")，高祖辈迁居[陕西省](../Page/陕西省.md "wikilink")[兴安府安康县](../Page/兴安府.md "wikilink")。1905年，他入[陝西陸軍小学](../Page/陝西陸軍小学.md "wikilink")，1906年他被保送入[保定陸軍速成学堂砲兵科学习](../Page/保定陸軍軍官学校.md "wikilink")。1910年（[宣統](../Page/宣統.md "wikilink")2年）他毕业返回陕西，被分配到陕西陆军第三十九混成协炮兵营当排长，不久调任軍械官。1911年，他加入[中国同盟会](../Page/中国同盟会.md "wikilink")。[辛亥革命之際](../Page/辛亥革命.md "wikilink")，1911年10月22日，他参加西安起义。此后他任陝西東路招討使，驻[同州](../Page/同州.md "wikilink")，不久调任河東節度使。

1912年（[民国元年](../Page/民国紀元.md "wikilink")），他任[陝西陸軍第](../Page/陝西省_\(中華民国\).md "wikilink")4混成旅旅長。1915年（民国4年），他署陝南鎮守使。同年12月[袁世凱称帝](../Page/袁世凱.md "wikilink")，他被封为三等男。

[護国战争爆发後的](../Page/護国战争.md "wikilink")1916年（民国5年）1月，他任陝北鎮守使（兼任渭北剿匪总司令）。此后，其部下[胡景翼活捉了陝西将軍](../Page/胡景翼.md "wikilink")[陸建章之子](../Page/陸建章.md "wikilink")[陸承武](../Page/陸承武.md "wikilink")（陝西第1混成旅旅長）。同年5月，他任陝西護国軍总司令，宣布陕西独立。他以交还陸承武作为条件，换取陸建章将将軍（督軍）之位交给他。袁世凱死後的1916年6月，他获[北洋将军府封为漢武将軍](../Page/北洋将军府.md "wikilink")。7月，他任陝西督軍兼省長（1918年（民国7年）3月他辞任省长）。

袁世凱死後，陳樹藩加入[皖系](../Page/皖系.md "wikilink")。陳樹藩获得陝西省的統治权后，和胡景翼的矛盾加深。1918年（民国7年）1月，胡景翼组织陝西[靖国軍](../Page/靖国軍.md "wikilink")，响应[護法战争](../Page/護法战争.md "wikilink")，开始和陳樹藩作战。陈引河南[镇嵩军统领](../Page/镇嵩军.md "wikilink")[刘镇华入陕援助](../Page/刘镇华.md "wikilink")，刘从此留在陕西不走。胡景翼迎来了護法軍政府（南方政府）的[于右任](../Page/于右任.md "wikilink")，和陳樹藩的战争处于胶着状態。胡景翼接受[姜宏模的建议](../Page/姜宏模.md "wikilink")，同陳樹藩展开和谈。陈树藩却乘机将胡景翼囚禁起来，通过要挟劝诱，企图使胡景翼屈服。但胡景翼拒绝易帜。

1920年（民国9年）7月，[直皖战争爆发](../Page/直皖战争.md "wikilink")，陳樹藩为了得到省内的人心而释放了胡景翼。其后，皖系敗北，陳樹藩丧失后盾，势力大减。民国10年（1921年）5月，[直系的](../Page/直系.md "wikilink")[閻相文](../Page/閻相文.md "wikilink")、[馮玉祥将陳樹藩从陝西督軍的位子上駆逐下去](../Page/馮玉祥.md "wikilink")。1921年5月25日，北京政府宣布免去陈树藩职务，由直系将领第二十师师长[阎相文出任陕西督军](../Page/阎相文.md "wikilink")。陈树藩率部逃往汉中。8月23日，阎相文在督署内突然吞服鸦片烟自杀，第十一师师长[冯玉祥接任陕西督军](../Page/冯玉祥.md "wikilink")。冯玉祥攻汉中府，陈树藩仓皇逃往四川。

此後，陳樹藩率軍在陝西省、[湖北省](../Page/湖北省_\(中華民国\).md "wikilink")、[四川省游荡](../Page/四川省_\(中華民国\).md "wikilink")，成为一个流浪的军事集团。同年12月，他被川軍击败，他逃到[天津](../Page/天津.md "wikilink")。此後，他在津、沪、杭等地当寓公，一心研究[佛教](../Page/佛教.md "wikilink")，脱离了軍事、政治舞台。抗日战争时期，他拒绝当汉奸，避走四川。抗日战争胜利后，他返回杭州居住。[第二次国共内战时期](../Page/第二次国共内战.md "wikilink")，反对[蒋介石发动内战](../Page/蒋介石.md "wikilink")。

1949年（民国38年）11月2日，他在[杭州去世](../Page/杭州市.md "wikilink")。享年65岁。

## 参考文献

  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;">徐輝琪「胡景翼」</span>

| （[北京政府](../Page/北京政府.md "wikilink")） |
| ------------------------------------ |

[Category:陆军第三十九混成协将领](../Category/陆军第三十九混成协将领.md "wikilink")
[Category:皖军将领](../Category/皖军将领.md "wikilink")
[C陈](../Category/辛亥革命人物.md "wikilink")
[Category:護國军将领](../Category/護國军将领.md "wikilink")
[Category:中华民国陕西省省长](../Category/中华民国陕西省省长.md "wikilink")
[C陈](../Category/北洋将军府冠字将军.md "wikilink")
[C陈](../Category/安康人.md "wikilink")
[S树](../Category/陈姓.md "wikilink")