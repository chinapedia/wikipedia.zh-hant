**王永民**（），[河南省](../Page/河南省.md "wikilink")[南阳人](../Page/南阳.md "wikilink")，[汉族](../Page/汉族.md "wikilink")，[五笔字型](../Page/五笔字型.md "wikilink")[汉字输入法的发明人](../Page/汉字输入法.md "wikilink")，全国劳动模范，教授级高级工程师，享受国务院特殊津贴，[北京王码创新网络技术有限公司](../Page/北京王码创新网络技术有限公司.md "wikilink")[总裁](../Page/总裁.md "wikilink")。\[1\]

## 个人简历

  - 1943年，出生于河南南阳[南召县](../Page/南召.md "wikilink")。
  - 1968年，毕业于[中国科学技术大学](../Page/中国科学技术大学.md "wikilink")[无线电](../Page/无线电.md "wikilink")[电子学系](../Page/电子学.md "wikilink")。
  - 1978年—1983年，发明了[五笔字型输入法](../Page/五笔字型输入法.md "wikilink")（[王码](../Page/王码.md "wikilink")）。
  - 1984年，来到[北京](../Page/北京.md "wikilink")，将五笔字型移到[PC机上](../Page/PC.md "wikilink")。
  - 1989年，成立北京王码电脑公司的前身：[王码电脑工程开发部](../Page/王码电脑工程开发部.md "wikilink")。
  - 1995年，更新五笔字型为**[95王码](../Page/95王码.md "wikilink")**。
  - 1997年，发明[五笔数码汉字输入法](../Page/五笔数码.md "wikilink")。
  - 1998年，更新五笔字型为**[王码98版](../Page/王码98版.md "wikilink")**。
  - 2008年，更新五笔字型为**[王码新世纪版](../Page/王码新世纪版.md "wikilink")**。\[2\]

## 荣誉

  - 1988年，[全国劳动模范](../Page/全国劳动模范.md "wikilink")
  - 1994年，[五一劳动奖章](../Page/五一劳动奖章.md "wikilink")

## 参考资料

## 外部链接

  - [王永民个人主页](http://www.wangma.com.cn/wym.asp)
  - [北京王码创新网络技术有限公司](http://www.wangma.com.cn)

[W王](../Page/category:程序员.md "wikilink")

[W](../Category/河南企业家.md "wikilink")
[Category:南召人](../Category/南召人.md "wikilink")
[W王](../Category/中国科学技术大学校友.md "wikilink")
[Y永](../Category/王姓.md "wikilink")
[Category:中国发明家](../Category/中国发明家.md "wikilink")
[Category:全国劳动模范](../Category/全国劳动模范.md "wikilink")
[Category:改革先锋称号获得者](../Category/改革先锋称号获得者.md "wikilink")
[Category:中国共产党党员](../Category/中国共产党党员.md "wikilink")

1.
2.