**古代雕塑展览馆**（）是德国[慕尼黑的一座博物馆](../Page/慕尼黑.md "wikilink")，是[巴伐利亚国王](../Page/巴伐利亚王国.md "wikilink")[路德维希一世收藏的古代](../Page/路德维希一世_\(巴伐利亚\).md "wikilink")[希腊和](../Page/希腊.md "wikilink")[罗马](../Page/罗马.md "wikilink")[雕塑作品](../Page/雕塑.md "wikilink")。建筑由设计，属于新古典主义风格，建于1816-1830年。目前，该博物馆是慕尼黑[艺术区的一部分](../Page/艺术区.md "wikilink")。
[Glyptothek_Munich.jpg](https://zh.wikipedia.org/wiki/File:Glyptothek_Munich.jpg "fig:Glyptothek_Munich.jpg")

## 历史

古代雕塑展览馆及其周围的一批工程——[国王广场和州立文物博物馆](../Page/国王广场.md "wikilink")（Staatliche
Antikensammlung）所在建筑，都是巴伐利亚王储（后来成为国王）路德维希一世所建的古希腊文化的纪念建筑。他想象一个“德国雅典”以留下古希腊文化的记忆，他在慕尼黑的大门前建造了这座建筑。

国王广场是由Karl von Fischer和Leo von Klenze于1815年设计，

## 建筑

## 收藏

[Aphaia_pediment_5_central_Glyptothek_Munich.jpg](https://zh.wikipedia.org/wiki/File:Aphaia_pediment_5_central_Glyptothek_Munich.jpg "fig:Aphaia_pediment_5_central_Glyptothek_Munich.jpg")

### 古典时期(490–323 BC)

[Barberini_Faun_front_Glyptothek_Munich_218_n1.jpg](https://zh.wikipedia.org/wiki/File:Barberini_Faun_front_Glyptothek_Munich_218_n1.jpg "fig:Barberini_Faun_front_Glyptothek_Munich_218_n1.jpg")

### 希腊化时期(323–146 BC)

### 罗马雕塑 (150 BC - 400 AD)

## 参见

  - [古希腊](../Page/古希腊.md "wikilink")
  - [古罗马](../Page/古罗马.md "wikilink")

## 外部链接

  - [古代雕塑展览馆主页（德文）](http://www.antike-am-koenigsplatz.mwn.de/glyptothek/)

[Category:慕尼黑博物館](../Category/慕尼黑博物館.md "wikilink")