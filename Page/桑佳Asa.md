**桑佳Asa**（，）。[宮崎縣](../Page/宮崎縣.md "wikilink")[小林市出身](../Page/小林市.md "wikilink")。日本女性[漫畫家](../Page/漫畫家.md "wikilink")。舊名義為**藤野Moyamu**（）。
代表作是《[召梦童子](../Page/召梦童子.md "wikilink")》、《[賢者長久不在](../Page/賢者長久不在.md "wikilink")》。

## 作品

### 連載

  - 《 》（「」2014年7月号 - 2015年9月号、[講談社](../Page/講談社.md "wikilink")、全2巻）
  - 《老婆婆少女日向妹妹》 ，原名《》（「月刊COMIC ZENON」2014年12月号 -
    連載中、[徳間書店](../Page/徳間書店.md "wikilink")、既刊5巻）
  - 《骷髏先生在看著你》，原名《 》（「[月刊COMIC
    CUNE](../Page/COMIC_CUNE.md "wikilink")」2015年10月号 -
    2017年12月号、[KADOKAWA](../Page/KADOKAWA.md "wikilink")・[Media
    Factory](../Page/Media_Factory.md "wikilink")、全3巻）
  - 《》（「」2016年11月号 - 2017年11月号、全3巻）
  - 《》（、「[月刊Comic Gene](../Page/月刊Comic_Gene.md "wikilink")」2018年5月号 -
    連載中）
  - 《硝銨的日常》，原名《》 （「月刊COMIC CUNE」2018年10月号 - 連載中、KADOKAWA）

### 讀切

  - 《》 （「月刊COMIC ZENON」2014年1月号、徳間書店）
  - 《》 （「月刊COMIC ZENON」2014年5月号、徳間書店）
  - 《 》（「[The Dessert](../Page/The_Dessert.md "wikilink")」2014年11月号、講談社）

### 藤野Moyamu名義

#### 漫画

  - 《[賢者長久不在](../Page/賢者長久不在.md "wikilink")》、全8冊、原名《》（月刊COMIC BLADE）
  - 《[方舟白書](../Page/方舟白書.md "wikilink")》、全7冊、原名《》（2005年10月 -
    2008年4月、月刊COMIC BLADE）
  - 《[源氏少女](../Page/源氏少女.md "wikilink")》、全1冊、原名《》
  - 《[喚夢童子](../Page/喚夢童子.md "wikilink")》、全5冊、原名《》（1999年5月号 -
    2001年11月号、月刊GanGanWING）
  - 《[喚夢童子外傳─公主與邪演貓](../Page/喚夢童子外傳─公主與邪演貓.md "wikilink")》、全1冊、原名《》
  - 《[櫻花戀人](../Page/櫻花戀人.md "wikilink")》、全1冊、原名《》\[1\]
  - 《》
  - 《[被遺忘的孤島](../Page/被遺忘的孤島.md "wikilink")》、原名《》（2008年12月 -
    2011年3月、全5冊、COMIC BLADE BROWNIE→月刊COMIC BLADE AVARUS）
  - 《》（2008年12月 - 连载中、COMIC BLADE BROWNIE）

#### 文集

  - L的季节 A piece of memories Premium Stories「告白」

  - 4格漫画劇場（只是封皮）

  - Kanon ～Premium Stories～ 「」

  - Kanon ～Premium Stories～ 第二集「」

## 參考資料

## 外部連結

  -
  - [『ゆるゆり』
    なもりさん大感激！　漫画家になるきっかけとなった憧れの人との対談が実現](https://ddnavi.com/news/298834/a/)

  - [ジーンで五感が優れた5兄弟描く「5★G★B」始動、付録は「SERVAMP」コースター](https://natalie.mu/comic/news/277912)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:宮崎縣出身人物](../Category/宮崎縣出身人物.md "wikilink")

1.