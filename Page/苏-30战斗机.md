**Su-30戰鬥機**（蘇-30戰鬥機，[北約命名稱為側衛C](../Page/北約命名.md "wikilink")，Flanker-C），是[俄羅斯](../Page/俄羅斯.md "wikilink")[蘇霍伊航空公司基於Su](../Page/蘇霍伊.md "wikilink")-27設計的[第四代](../Page/第四代战机.md "wikilink")[多用途戰機](../Page/多用途戰機.md "wikilink")（按照美國與俄羅斯標準，這些改良航電飛機被稱作三代半），是著重對標蘇-27假想對手F-15升級版[F-15E的產品](../Page/F-15E.md "wikilink")，同時航程也有所提高。Su-30主要由[阿穆尔河畔共青城加加林飞机制造厂以及伊爾庫茨克飛機製造廠生產](../Page/阿穆尔河畔共青城加加林飞机制造厂.md "wikilink")，前者的產品主要外銷中國，後者主要則供貨給印度空軍，這款戰機也獲得許多其他國家的訂單。

## 發展

### Su-27PU長程攔截構想

因為少數戰機必須防衛廣大區域，雖然原本的Su-27已經有很遠的航程，但還是不能符合蘇聯的空中防衛計畫（簡稱“PVO”來自*Protivo-Vozdushnaya
Oborona*）。所以Su-27PU研發案於1986年開始，這種強化型Su-27有更遠航程。雙座版Su-27UB教練機被選為改裝Su-27PU的母體，因為它的性能類似單座[Su-27又能提供長程作戰的雙人駕駛機艙](../Page/Su-27.md "wikilink")。概念驗證機於1987年7月6日首飛，該機的成果引致後續又生產有兩架Su-27PU原型機。第一架Su-27PU在1989年12月31日首飛於庫爾茲克，三架量產機的第一架首飛於1992年4月14日。

  - 為了適應新角色該架原型Su-27UB在機鼻左邊加裝了空中加油管以延長航程；IRST（紅外線追跡裝置）則移到右邊。該機座艙後部還有氣動減速板。
  - 航電系統也有改變，增加了特別通訊和導引裝備以領導一群[Su-27機隊](../Page/Su-27.md "wikilink")。後座加裝了一具大型[CRT顯示器同時顯示機隊編組資訊和戰術目標資訊](../Page/CRT.md "wikilink")。導航和[線傳飛控也升級了](../Page/線傳飛控.md "wikilink")。雷達升為NIIP
    N001雷達，有一些空對地攻擊力和多目標接戰能力。

蘇霍伊提供Su-27PU使用“戰機控制者”，這種短程AWACS空中預警機系統，可以讓後座透過资料链控制其他戰機。然而，PVO還是沒興趣購買Su-27PU。全部五架Su-27PU，都改成後續“Su-30”的交接教練機，1996起進入PVO第54空中攔截聯隊於Savostleyka的訓練基地。

## 設計

[Sukhoi_Su-30MK_at_MAKS05_Ryabtsev.jpg](https://zh.wikipedia.org/wiki/File:Sukhoi_Su-30MK_at_MAKS05_Ryabtsev.jpg "fig:Sukhoi_Su-30MK_at_MAKS05_Ryabtsev.jpg")
[Су-30МКМ_(МАКС-2007)_(02).jpg](https://zh.wikipedia.org/wiki/File:Су-30МКМ_\(МАКС-2007\)_\(02\).jpg "fig:Су-30МКМ_(МАКС-2007)_(02).jpg")
一架雙座版Su-30M曾應蘇聯要求於90年代生產出來作為測試機。

  - 外銷版稱為Su-30MK，其中“MK”表示“модернизированный,
    коммерческий”（現代化商業版）。首架Su-30MK於1993巴黎航空展發表。
  - 更多優化的Su-30MK，多是從Su-27PU改裝來，於1994年發表。

### 高彈性多用途

Su-30MK可在远离基地的情况下完成多种作战任务，并能在各种天气及[干扰较严重的环境下不分昼夜作战](../Page/干扰.md "wikilink")。

這款多功能戰機能够承担全范围的战术打击任务，从夺取空中优势，防空任务，空中巡逻和护航，再到[对地攻击](../Page/对地攻击.md "wikilink")，[压制敌方防空系统](../Page/SEAD.md "wikilink")，[空中拦截](../Page/空中拦截.md "wikilink")，[近距空中支援和空对海攻击](../Page/近距空中支援.md "wikilink")。此外，Su-30MK还能执行[ECCM](../Page/ECCM.md "wikilink")（电子反对抗）任务，进行空中早期预警，指挥和调控己方机群进行联合空中攻击任务。

### 高攻角

Su-30MK型的氣動設計採縱軸不安定三翼式，可以增加升力和機動性，[前翼是最大改變](../Page/前翼.md "wikilink")。它可以在[高攻角下自動偏斜保持飛機動能](../Page/高攻角.md "wikilink")。然而，只有少數Su-30構型有前翼，例如Su-30MKI型。

### 發動機

飛機引擎採兩具[AL-31FP後燃低通量](../Page/AL-31.md "wikilink")[渦輪扇葉發動機](../Page/渦輪扇葉發動機.md "wikilink")。總推力25,000
kgf（245千牛頓）極速2馬赫，低海拔時速1,350 km/h，爬升率也有230 m/s。

在標準油箱5,270kg的燃料下Su-30MK有能力飛行4.5小時達到3,000 km作戰半徑。空中加油可以延長飛行到10小時達到8,000
km作戰半徑，作戰最高高度可達11到13km。

長航程特性增加了許多戰術運用。包含邊界巡邏和護航轟炸機或是深入對地打擊。對角式2D[向量推力噴嘴可以達到](../Page/向量推力.md "wikilink")±15度非對稱偏斜（兩引擎各自可以互相偏離總角度32度）。

### 雙座艙

後座者可以分擔戰鬥和轟炸任務中的雷達操縱和目標辨識，前後座的操作功能可以透過軟體互換。

### 航電系統

[MAKS-2007-Su-30MK-1.jpg](https://zh.wikipedia.org/wiki/File:MAKS-2007-Su-30MK-1.jpg "fig:MAKS-2007-Su-30MK-1.jpg")

  - [雷達](../Page/雷達.md "wikilink")：有多種雷達可以選配；有N001VE或Phazotron N010
    Zhuk-27或N011M
    BARS多普勒脈衝被動電子掃描陣列雷達，都有能力同時追蹤15個空中目標，同時攻擊其中四個。其中N011M多普勒脈衝雷達（有20-[m](../Page/m.md "wikilink")
    (65.6 ft）解析度)可以在400km（248.5 mi）外偵測到大型海上船艦，小船艦也能在120km（74.5 mi）偵測到。
  - 其他航電包含整體光學照準導航系統搭配大型陀螺儀系統；[面板顯示器](../Page/面板顯示器.md "wikilink")、[抬頭顯示器](../Page/抬頭顯示器.md "wikilink")，還有多功能彩色[LCD搭配影像複合能力](../Page/LCD.md "wikilink")；內建[GPS系統](../Page/GPS.md "wikilink")（[GLONASS](../Page/GLONASS.md "wikilink")/[NAVSTAR相容型](../Page/NAVSTAR.md "wikilink")）。
  - 加掛[紅外線和](../Page/紅外線.md "wikilink")[雷射瞄準萊艙可以鎖定小型地面車輛](../Page/雷射.md "wikilink")。
  - 本機還有ECCM可以反反制敵方電子、光學防禦系統，有幾種機型的兩翼端有巨大的各一具電子艙，能使用「交叉注視」型欺騙干擾對抗來襲的先進空對空飛彈。
  - 所有機型都有特製[自動駕駛可以在低空沿地形飛行](../Page/自動駕駛.md "wikilink")，還能獨立和團體接戰陸海空目標。自動駕駛系統和導航系統相連線，能目標導向飛行和自動模式回航與降落。

## 延伸型

[KS-172_NTW_-_94.jpg](https://zh.wikipedia.org/wiki/File:KS-172_NTW_-_94.jpg "fig:KS-172_NTW_-_94.jpg")
[World_operators_of_the_Su-30.png](https://zh.wikipedia.org/wiki/File:World_operators_of_the_Su-30.png "fig:World_operators_of_the_Su-30.png")

[NellisIAFSu30MKI.jpg](https://zh.wikipedia.org/wiki/File:NellisIAFSu30MKI.jpg "fig:NellisIAFSu30MKI.jpg")

  - Su-27PU
    Su-27UB雙座教練機改裝來的長程攔截戰機。改名為Su-30。
  - Su-30
    傳言中的測試機。
  - Su-30K
    Su-30商業版。50架賣給印度後又升級成Su-30MKI。
  - Su-30KI
    從Su-27S升級而來。只外銷給[印尼](../Page/印尼.md "wikilink")，但是24架訂單於1997被取消\[1\]，是一種單座版Su-30。
  - Su-30KN
    從Su-27UB，Su-30和Su-30K，雙座版升級
  - Su-30M
    升級自Su-27PU，可以說是第一架[Su-27家族中的多用途戰機](../Page/Su-27.md "wikilink")。
  - Su-30MK
    商業版Su-30M，首次發表於1993年。
  - Su-30M2
    Su-30MK升級版。
  - [Su-30MKI](../Page/Su-30MKI戰鬥機.md "wikilink")
    和印度航太共同研發的印度空軍版。包含[向量推力](../Page/向量推力.md "wikilink")，裝有[以色列研發的多國相容航電](../Page/以色列.md "wikilink")，可以和印度、俄羅斯和法國飛機互通。\[2\]
  - Su-30MKM
    基於MKI的[馬來西亞專用版](../Page/馬來西亞.md "wikilink")，機體一樣但是融合使用法國、南非、俄羅斯相容航電。備有抬頭顯示器（HUD），導航前視IR系統（NAVFLIR）法國製雷射導引萊艙（LDP），南非SAAB
    AVITRONICS生產的MAW-300飛彈警報系統（MAWS）、雷射警報系統（LWS）[1](https://web.archive.org/web/20080409155137/http://www.armada.ch/07-6/02_Aircraft_Self_Protection.pdf)，採用蘇聯NIIP
    N011M
    BARS被動陣列雷達、電子閃光系統（EW）、光學定位儀（OLS）和全數位駕駛艙。[2](https://web.archive.org/web/20080307022036/http://www.irkut.com/en/news/press_release_archives/index.php?id48=252)
  - Su-30MKV
    [委內瑞拉外銷版很類似Su](../Page/委內瑞拉.md "wikilink")-30MK2。由兩台KNAAPO實驗機改裝的（No.
    0460和No. 1259）有參加2006年七月卡拉卡斯的國慶閱兵。後來證實委內瑞拉買的確實是列為Su-30MK2型。
  - Su-30MK2V
    Su-30MK2 [越南專用版](../Page/越南.md "wikilink")，略有修改。
  - Su-30MK3
    Su-30MKK使用Zhuk MSE雷達以支援[Kh-59MK反艦飛彈](../Page/Kh-59.md "wikilink")。
  - Su-30MKA
    [阿爾及利亞有一種高專用版類似MKI](../Page/阿爾及利亞.md "wikilink")，但是裝備蘇聯和法國航電。有[法國](../Page/法國.md "wikilink")[Thales和](../Page/Thales.md "wikilink")[Sagem生產的多用途抬頭顯示器](../Page/Sagem.md "wikilink")。
  - Su-30MKN
    [馬來西亞外銷版](../Page/馬來西亞.md "wikilink")。
  - [Su-30MKK](../Page/Su-30MKK.md "wikilink")
    [中華人民共和國外銷版](../Page/中華人民共和國.md "wikilink")。
  - [Su-30MK2](../Page/Su-30MK2.md "wikilink")
    Su-30MKK升級電戰系統和[反艦飛彈相容力](../Page/反艦飛彈.md "wikilink")，日後又再改款成中製[殲-16](../Page/殲-16.md "wikilink")。
  - [Su-30SM](../Page/Su-30SM.md "wikilink")
    Su-30的最新版。基于Su-30MKI与Su-30MKM的经验设计的最新版。俄罗斯空军装备了30架，哈萨克斯坦空军也有装备\[3\]。

## 使用国与潜在客户

使用国：

  - ：

      - [俄羅斯空軍](../Page/俄羅斯空軍.md "wikilink") - 至2017年初至少裝備84架
      - [俄羅斯海軍航空兵](../Page/俄羅斯海軍航空兵.md "wikilink") - 至2017年初裝備15架
      - [俄羅斯勇士飛行表演隊](../Page/俄羅斯勇士飛行表演隊.md "wikilink") -
        2016年10月31日，表演队接收4架Su-30SM。\[4\]

  - ：訂購97架

      - [中國人民解放軍空軍](../Page/中國人民解放軍空軍.md "wikilink") - Su-30MKK型73架
      - [中國人民解放軍海軍航空兵](../Page/中國人民解放軍海軍航空兵.md "wikilink") - MK2型24架

  - ：

      - [哈薩克斯坦空軍](../Page/哈薩克斯坦空軍.md "wikilink") -
        4架（首批）Su-30SM，于2015年早期到位，现共8架

  - ：

      - [印度空軍](../Page/印度空軍.md "wikilink") -
        242架[Su-30MKI戰鬥機](../Page/Su-30MKI戰鬥機.md "wikilink")

  - ：Su-30MKA，2017年会再接收6架同型飞机，共52架。

  - ：訂購24架

      - [委內瑞拉空軍](../Page/委內瑞拉空軍.md "wikilink") - 24架Su-30MKV

  - ：

      - [越南空軍](../Page/越南空軍.md "wikilink") - 35架Su-30MK2V，共41架（2017年初）

  - ：

      - [馬來西亞空軍](../Page/馬來西亞空軍.md "wikilink") - 18架Su-30MKM

  - ：9架Su-30MK2，2架Su-30MK

      - [印度尼西亞空軍](../Page/印度尼西亞空軍.md "wikilink") - 共11架

  - ：6架Su-30MK2

  - ：於2018年正式訂購6架Su-30

  - ：

      - [安哥拉空軍](../Page/安哥拉空軍.md "wikilink") - 18架Su-30K\[5\]

潜在客户：

  -
  - ：[伊朗空軍可能訂購](../Page/伊朗空軍.md "wikilink")60架Su-30MK系列用以替換[MiG-29和](../Page/MiG-29.md "wikilink")[F-14戰鬥機](../Page/F-14戰鬥機.md "wikilink")\[6\]\[7\]。

  - ：\[8\]。

## 事故

<table>
<thead>
<tr class="header">
<th><p>日期</p></th>
<th><p>所屬國家</p></th>
<th><p>地點</p></th>
<th><p>傷亡</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>12.06.1999年</p></td>
<td></td>
<td><p><a href="../Page/巴黎.md" title="wikilink">巴黎</a></p></td>
<td><p>0/2</p></td>
<td><p>在Le Bourget航展上的一次示範飛行中墜毀，在特技飛行後，尾部撞到了地面[9][10].</p></td>
</tr>
<tr class="even">
<td><p>30.04.2009年</p></td>
<td></td>
<td></td>
<td><p>1/2</p></td>
<td><p>墜毀</p></td>
</tr>
<tr class="odd">
<td><p>30.11.2009年</p></td>
<td></td>
<td><p><a href="../Page/Джайсалмер.md" title="wikilink">Джайсалмер</a></p></td>
<td><p>0/2</p></td>
<td><p>墜毀</p></td>
</tr>
<tr class="even">
<td><p>13.12.2011年</p></td>
<td></td>
<td><p><a href="../Page/Пуна.md" title="wikilink">Пуна</a></p></td>
<td><p>0/2</p></td>
<td><p>墜毀[11].</p></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td></td>
<td></td>
<td><p>0/2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>28.02.2012年</p></td>
<td><p>|</p></td>
<td></td>
<td><p>1/2</p></td>
<td><p>在國防部JSC KnAAPO代表的驗收測試中，這架飛機在阿穆爾河畔共青城以北130公里處墜毀。當在3公里的高度加速到儀器的最大速度時，右發動機著火，導致飛機控制系統損壞。在飛行控制中心的指揮下，飛行員使用彈射座椅逃離。由於擊中樹木，機組指揮官在著陸時受輕傷。</p></td>
</tr>
<tr class="odd">
<td><p>19.02.2013年</p></td>
<td></td>
<td><p><a href="../Page/Раджастхан.md" title="wikilink">Раджастхан</a></p></td>
<td><p>0/2</p></td>
<td><p>Разбился в тренировочном полёте. Пострадавших нет, пилотам удалось катапультироваться[12].</p></td>
</tr>
<tr class="even">
<td><p>14.10.2014</p></td>
<td><p>SB050</p></td>
<td><p><a href="../Page/Пуна.md" title="wikilink">Пуна</a></p></td>
<td><p>0/2</p></td>
<td><p>Истребитель упал во время тренировочного полёта вскоре после взлёта.[13].</p></td>
</tr>
<tr class="odd">
<td><p>19.05.2015年</p></td>
<td></td>
<td><p><a href="../Page/阿薩姆.md" title="wikilink">阿薩姆</a></p></td>
<td><p>0/2</p></td>
<td><p>Су-30МКИ потерпел катастрофу из-за технической неисправности, пилоты катапультировались. Из-за взрыва на земле двое человек получили ранения.[14]</p></td>
</tr>
<tr class="even">
<td><p>17.09.2015</p></td>
<td><p>AMB-0460</p></td>
<td></td>
<td><p>2/2</p></td>
<td><p>蘇-30MK2在追捕國界邊界違規者的飛機中墜毀。[.[15][16]</p></td>
</tr>
<tr class="odd">
<td><p>14.06.2016</p></td>
<td><p>8585</p></td>
<td></td>
<td><p>1/2</p></td>
<td><p>Су-30МК2 ВВС Вьетнама упал в море.[17]</p></td>
</tr>
<tr class="even">
<td><p>15.03.2017</p></td>
<td></td>
<td><p><a href="../Page/Раджастхан.md" title="wikilink">Раджастхан</a></p></td>
<td><p>0/2</p></td>
<td><p>Су-30МКИ потерпел катастрофу из-за технической неисправности, пилоты катапультировались.[18]</p></td>
</tr>
</tbody>
</table>

## 性能

[Szu-30.svg](https://zh.wikipedia.org/wiki/File:Szu-30.svg "fig:Szu-30.svg")

  - '''总共12个挂点

## 參見

**設計時序**

[Su-27](../Page/Su-27.md "wikilink"){{.w}}[Su-30](../Page/Su-30.md "wikilink"){{.w}}[Su-32](../Page/Su-32.md "wikilink"){{.w}}[Su-33](../Page/Su-33.md "wikilink"){{.w}}[Su-34](../Page/Su-34.md "wikilink"){{.w}}[Su-35](../Page/Su-35.md "wikilink"){{.w}}[Su-37](../Page/Su-37.md "wikilink"){{.w}}[Su-47](../Page/Su-47.md "wikilink"){{.w}}[Su-50](../Page/Su-50.md "wikilink"){{.w}}

**相關列表**

[飛機列表](../Page/飛機列表.md "wikilink")

## 參考文獻

  - Gordon, Yefim and Davidson, Peter. *Sukhoi Su-27 Flanker*, Specialty
    Press, 2006. ISBN 978-1-58007-091-1.

[Category:苏霍伊飞机](../Category/苏霍伊飞机.md "wikilink")
[Su](../Category/蘇聯戰鬥機.md "wikilink")
[Su](../Category/俄羅斯戰鬥機.md "wikilink")
[Category:雙發噴射機](../Category/雙發噴射機.md "wikilink")

1.  <http://www.milavia.net/aircraft/su-27/su-27_ops.htm>
2.  [SU30MKI](http://www.aircraftinaction.co.uk/2007/Waddington.htm)
3.  Kazakhstan Has Received Four Russian SU-30SM
    Fighters[3](https://www.bellingcat.com/news/rest-of-world/2015/06/30/kazakhstan-has-received-four-russian-su-30sm-fighters/)
4.
5.
6.  [Иран проявил интерес к российским истребителям
    Су-30](https://lenta.ru/news/2016/02/10/iran1/)
7.  [イラン国防相、「Su-30戦闘機購入契約を締結」](http://japanese.irib.ir/news/latest-news/item/62220)
8.  [Belarus to buy new Russian Su-30SM fighter jets
    after 2020](http://www.upi.com/Business_News/Security-Industry/2015/10/21/Belarus-to-buy-new-Russian-Su-30SM-fighter-jets-after-2020/1841445435211/)
9.  [Авария Су-30МК в Ле
    Бурже 12.06.1999](http://www.youtube.com/watch?v=Yh-kuztsE1s)
10.
11. [Air Force’s Sukhoi jet crashes near Pune, pilots
    safe](http://www.ndtv.com/article/india/air-force-plane-crashes-near-pune-pilots-eject-safely-157576?pfrom=home-topstories)
12. [В Индии разбился самолёт
    Су-30](http://top.rbc.ru/incidents/20/02/2013/845928.shtml)
13. [На западе Индии разбился истребитель ВВС
    страны](http://itar-tass.com/proisshestviya/1507408)
14. [IAF Su-30MKI crashes in
    Assam](http://www.thestatesman.com/news/latest-headlines/iaf-su-30mki-crashes-in-assam/64170.html)
15. [Investigarán causas del siniestro del Sukhoi de la Fanb en la
    frontera con
    Colombia](http://www.noticias24.com/venezuela/noticia/296558/investigaran-causas-del-siniestro-del-sukhoi-de-la-fanb-en-la-frontera-con-colombia/)
16. [Maduro lamentó la muerte de los pilotos del Sukhoi
    siniestrado](http://www.eluniversal.com/nacional-y-politica/150918/maduro-lamento-la-muerte-de-los-pilotos-del-sukhoi-siniestrado)
17. [Hai phi công kịp bung dù khi máy bay Su-30MK2 bị sự cố, một phi
    công vẫn mất
    tích](http://viettimes.vn/quoc-phong/hai-phi-cong-kip-bung-du-khi-may-bay-su30mk2-bi-su-co-mot-phi-cong-van-mat-tich-61826.html)
18. [Истребитель Су-30 ВВС Индии разбился на северо-западе страны Далее:
    <https://news.rambler.ru/incidents/36339435/?utm_content=news&utm_medium=read_more&utm_source=copylink>](https://news.rambler.ru/incidents/36339435-su-30-razbilsya-na-severo-zapade-indii/)