[2007Computex_WelcomeReception_Evergreen.jpg](https://zh.wikipedia.org/wiki/File:2007Computex_WelcomeReception_Evergreen.jpg "fig:2007Computex_WelcomeReception_Evergreen.jpg")歡迎酒會中演奏\]\]
**長榮交響樂團**（****，[縮寫](../Page/縮寫.md "wikilink")：**ESO**）是由[台灣的](../Page/台灣.md "wikilink")[長榮集團](../Page/長榮集團.md "wikilink")[張榮發基金會於](../Page/張榮發.md "wikilink")2001年所創立的[交響樂團](../Page/交響樂團.md "wikilink")。此樂團召集了在台灣許多優秀的演奏家以及帶動台灣民族音樂的發展。是目前台灣唯一也是國際間少數由民間法人機構經營的職業交響樂團。

其成立宗旨為提供一個優質的表演舞台，全方位培養屬於台灣的音樂人才，期以向下扎根的方式耕耘出具有台灣特色的音樂園地，既關懷本土也放眼世界。\[1\]

長榮交響樂團所演奏的曲目，通常會在[長榮航空的班機上](../Page/長榮航空.md "wikilink")，於旅客上下飛機的時候播放；而有時候也會在[臺灣桃園國際機場第二航廈的長榮候機室播放](../Page/臺灣桃園國際機場.md "wikilink")，歡迎旅客登機。

## 歷史

2001年4月，財團法人張榮發基金會邀集多位[華人音樂家及國際知名的藝術顧問](../Page/華人.md "wikilink")，合力促成20人編制的長榮樂團，又於2002年中擴編為70人編制的交響樂團，並聘請知名華人音樂家[林克昌擔任樂團第一任音樂總監及首席指揮](../Page/林克昌.md "wikilink")。2004年7月，聘請曾獲「長榮音樂助學金」赴美留學的[王雅蕙擔任第二任音樂總監兼首席指揮](../Page/王雅蕙.md "wikilink")。2007年1月正式聘請曾任慕尼黑室內獨奏樂團指揮及擔任慕尼黑愛樂、斯圖加特廣播樂團…等樂團客席指揮的[葛諾·舒馬富斯](../Page/葛諾·舒馬富斯.md "wikilink")（Gernot
Schmalfuss）擔任音樂總監及首席指揮。

長榮交響樂團曾分別於2005及2006年入圍台灣第十六、十七屆金曲獎「最佳古典音樂專輯」及「最佳演奏獎」。

從2004年9月起赴海外巡迴演出，曾連續兩年（2005年、2006年）受邀參與[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[貝多芬及](../Page/貝多芬.md "wikilink")[莫札特](../Page/莫札特.md "wikilink")「」音樂祭演出，以及赴[中國](../Page/中國.md "wikilink")[上海及](../Page/上海.md "wikilink")[北京巡演](../Page/北京.md "wikilink")。2005年3月份，樂團首度舉辦國際性的音樂教學活動，邀請世界知名小提琴泰斗[查克哈·布隆](../Page/查克哈·布隆.md "wikilink")（Zakhar
Bron）到台灣舉辦大師班及音樂會首演，2006年獲得[行政院](../Page/行政院.md "wikilink")[文化建設委員會扶植團隊之肯定](../Page/文化建設委員會.md "wikilink")。

## 音樂總監兼首席指揮

  - [林克昌](../Page/林克昌.md "wikilink") 2002年-2004年
  - 王雅蕙 2004年-2006年
  - [葛諾‧舒馬富斯](../Page/葛諾‧舒馬富斯.md "wikilink") 2007年-迄今

## 樂團成員

2015年團員名冊\[2\]

  - 樂團首席

[郭維斌](../Page/郭維斌.md "wikilink")

  - 助理樂團首席

[林世昕](../Page/林世昕.md "wikilink")

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 0%" />
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>第一<a href="../Page/小提琴.md" title="wikilink">小提琴</a></dt>

</dl>
<p><a href="../Page/洪上筑.md" title="wikilink">洪上筑</a>、<a href="../Page/陳致千.md" title="wikilink">陳致千</a>、<a href="../Page/李惠怡.md" title="wikilink">李惠怡</a>、<a href="../Page/曲靜家.md" title="wikilink">曲靜家</a>、<a href="../Page/林以茗.md" title="wikilink">林以茗</a>、<a href="../Page/李念慈.md" title="wikilink">李念慈</a>、<a href="../Page/楊琬茜.md" title="wikilink">楊琬茜</a>、<a href="../Page/張奕若.md" title="wikilink">張奕若</a>、<a href="../Page/莫書涵.md" title="wikilink">莫書涵</a></p>
<dl>
<dt>第二小提琴</dt>

</dl>
<p><a href="../Page/黃郁婷.md" title="wikilink">黃郁婷</a>、<a href="../Page/簡紹宇.md" title="wikilink">簡紹宇</a>、<a href="../Page/王重凱.md" title="wikilink">王重凱</a>、<a href="../Page/林蓓瑩.md" title="wikilink">林蓓瑩</a>、<a href="../Page/黃郁盛.md" title="wikilink">黃郁盛</a>、<a href="../Page/高維濃.md" title="wikilink">高維濃</a>、<a href="../Page/郭己溫.md" title="wikilink">郭己溫</a>、<a href="../Page/薛媛云.md" title="wikilink">薛媛云</a>、<a href="../Page/劉映秀.md" title="wikilink">劉映秀</a>、<a href="../Page/陳俊志.md" title="wikilink">陳俊志</a></p>
<dl>
<dt><a href="../Page/中提琴.md" title="wikilink">中提琴</a></dt>

</dl>
<p><a href="../Page/蕭雨沛.md" title="wikilink">蕭雨沛</a>、<a href="../Page/吳媛蓉.md" title="wikilink">吳媛蓉</a>、<a href="../Page/謝婷妤.md" title="wikilink">謝婷妤</a>、<a href="../Page/陳可倪.md" title="wikilink">陳可倪</a>、<a href="../Page/李沛怡.md" title="wikilink">李沛怡</a>、<a href="../Page/呂佳旻.md" title="wikilink">呂佳旻</a>、<a href="../Page/楊佩蓉.md" title="wikilink">楊佩蓉</a>、<a href="../Page/凃瓔真.md" title="wikilink">凃瓔真</a></p>
<dl>
<dt><a href="../Page/大提琴.md" title="wikilink">大提琴</a></dt>

</dl>
<p><a href="../Page/曾韻澄.md" title="wikilink">曾韻澄</a>、<a href="../Page/楊培詩.md" title="wikilink">楊培詩</a>、<a href="../Page/王婉儀.md" title="wikilink">王婉儀</a>、<a href="../Page/許朋妘.md" title="wikilink">許朋妘</a>、<a href="../Page/丁莉齡.md" title="wikilink">丁莉齡</a>、<a href="../Page/劉慧芬.md" title="wikilink">劉慧芬</a></p>
<dl>
<dt><a href="../Page/低音提琴.md" title="wikilink">低音提琴</a></dt>

</dl>
<p><a href="../Page/宋家齊.md" title="wikilink">宋家齊</a>、<a href="../Page/羅伊純.md" title="wikilink">羅伊純</a>、<a href="../Page/許雅君.md" title="wikilink">許雅君</a>、<a href="../Page/邱歆詒.md" title="wikilink">邱歆詒</a>、<a href="../Page/汪星諭.md" title="wikilink">汪星諭</a></p></td>
<td></td>
<td><dl>
<dt><a href="../Page/長笛.md" title="wikilink">長笛</a></dt>

</dl>
<p><a href="../Page/許佑佳.md" title="wikilink">許佑佳</a>、<a href="../Page/林靜旻.md" title="wikilink">林靜旻</a>、<a href="../Page/堤由佳.md" title="wikilink">堤由佳</a></p>
<dl>
<dt><a href="../Page/雙簧管.md" title="wikilink">雙簧管</a></dt>

</dl>
<p><a href="../Page/蔡采璇.md" title="wikilink">蔡采璇</a>、<a href="../Page/高維謙.md" title="wikilink">高維謙</a></p>
<dl>
<dt><a href="../Page/英國管.md" title="wikilink">英國管</a></dt>

</dl>
<p><a href="../Page/劉雅蘭.md" title="wikilink">劉雅蘭</a></p>
<dl>
<dt><a href="../Page/單簧管.md" title="wikilink">單簧管</a></dt>

</dl>
<p><a href="../Page/莊蕙竹.md" title="wikilink">莊蕙竹</a>、<a href="../Page/蘇怡方.md" title="wikilink">蘇怡方</a></p>
<dl>
<dt><a href="../Page/低音管.md" title="wikilink">低音管</a></dt>

</dl>
<p><a href="../Page/許家華.md" title="wikilink">許家華</a>、<a href="../Page/吳婉菁.md" title="wikilink">吳婉菁</a></p>
<dl>
<dt><a href="../Page/倍低音管.md" title="wikilink">倍低音管</a></dt>

</dl>
<p><a href="../Page/林彥君.md" title="wikilink">林彥君</a></p>
<dl>
<dt><a href="../Page/法國號.md" title="wikilink">法國號</a></dt>

</dl>
<p><a href="../Page/林筱玲.md" title="wikilink">林筱玲</a>、<a href="../Page/吳汧潁.md" title="wikilink">吳汧潁</a>、<a href="../Page/崔文瑜.md" title="wikilink">崔文瑜</a>、<a href="../Page/黃盈樺.md" title="wikilink">黃盈樺</a>、<a href="../Page/黃哲筠.md" title="wikilink">黃哲筠</a></p></td>
<td><dl>
<dt><a href="../Page/小號.md" title="wikilink">小號</a></dt>

</dl>
<p><a href="../Page/杉木馨.md" title="wikilink">杉木馨</a>、<a href="../Page/何忠謀.md" title="wikilink">何忠謀</a>、<a href="../Page/高信譚.md" title="wikilink">高信譚</a>、<a href="../Page/賴怡蒨.md" title="wikilink">賴怡蒨</a></p>
<dl>
<dt><a href="../Page/長號.md" title="wikilink">長號</a></dt>

</dl>
<p><a href="../Page/李昆穎.md" title="wikilink">李昆穎</a>、<a href="../Page/林勁谷.md" title="wikilink">林勁谷</a></p>
<dl>
<dt><a href="../Page/低音長號.md" title="wikilink">低音長號</a></dt>

</dl>
<p><a href="../Page/馬萬銓.md" title="wikilink">馬萬銓</a></p>
<dl>
<dt><a href="../Page/低音號.md" title="wikilink">低音號</a></dt>

</dl>
<p><a href="../Page/蔡孟昕.md" title="wikilink">蔡孟昕</a></p>
<dl>
<dt><a href="../Page/定音鼓.md" title="wikilink">定音鼓</a>｜<a href="../Page/打擊樂器.md" title="wikilink">打擊樂器</a></dt>

</dl>
<p><a href="../Page/鄭雅琪.md" title="wikilink">鄭雅琪</a>、<a href="../Page/賈雯豪.md" title="wikilink">賈雯豪</a>、<a href="../Page/陳昶嘉.md" title="wikilink">陳昶嘉</a></p></td>
</tr>
</tbody>
</table>

## 參見

  - [台灣管弦樂團列表](../Page/台灣管弦樂團列表.md "wikilink")
  - [長榮海運](../Page/長榮海運.md "wikilink")
  - [長榮航空](../Page/長榮航空.md "wikilink")

## 參考

## 外部連結

  - [長榮交響樂團官方網站](http://www.evergreensymphony.org/)

[Category:台灣管弦樂團](../Category/台灣管弦樂團.md "wikilink")
[社教](../Category/長榮集團.md "wikilink")
[Category:2001年台灣建立](../Category/2001年台灣建立.md "wikilink")

1.
2.  [關於我們-樂團成員](https://www.evergreensymphony.org/servlet/PUF1_ControllerServlet.do?lang=zh-TW&menu=PBI1&func=MEMBER&action=VIEW_INDEX)，團員名冊。