**光復鄉**（[阿美語](../Page/阿美語.md "wikilink")：**Fata'an**）位於[台灣](../Page/台灣.md "wikilink")[花蓮縣東部中段](../Page/花蓮縣.md "wikilink")，北臨[鳳林鎮](../Page/鳳林鎮_\(台灣\).md "wikilink")，東及東南鄰[豐濱鄉](../Page/豐濱鄉.md "wikilink")，西鄰[萬榮鄉](../Page/萬榮鄉.md "wikilink")，南接[瑞穗鄉](../Page/瑞穗鄉.md "wikilink")，在[日治時代以製糖為主業](../Page/台灣日治時期.md "wikilink")，當前境內尚保留許多與糖業相關的建築及文物(如:[花蓮觀光糖廠](../Page/花蓮觀光糖廠.md "wikilink")、[花糖文物館等](../Page/花糖文物館.md "wikilink"))，與[鳳林鎮同為中花蓮地區的發展核心](../Page/鳳林鎮.md "wikilink")。有許多台灣著名的[棒球選手出身於本鄉](../Page/棒球.md "wikilink")。
本鄉地處[花東縱谷](../Page/花東縱谷.md "wikilink")，為一狹長[平原](../Page/平原.md "wikilink")，全境有60%為山地，另40%為[嘉農溪及](../Page/嘉農溪.md "wikilink")[馬鞍溪之](../Page/馬鞍溪.md "wikilink")[沖積平原](../Page/沖積平原.md "wikilink")。西有[中央山脈](../Page/中央山脈.md "wikilink")，東有[海岸山脈](../Page/海岸山脈.md "wikilink")，有[馬太鞍溪](../Page/馬太鞍溪.md "wikilink")、[花蓮溪](../Page/花蓮溪.md "wikilink")、[光復溪等流經鄉境](../Page/光復溪.md "wikilink")，氣候屬[副熱帶季風氣候](../Page/副熱帶季風氣候.md "wikilink")。

鄉內居民[漢人與](../Page/漢人.md "wikilink")[原住民大約各半](../Page/臺灣原住民族.md "wikilink")，原住民則以[阿美族為主](../Page/阿美族.md "wikilink")，產業以[農業為主](../Page/農業.md "wikilink")，地方通行語為[阿美語](../Page/阿美語.md "wikilink")\[1\]與[台灣客家語](../Page/台灣客家語.md "wikilink")\[2\]。

## 歷史

光復鄉主要有三大聚落：歷史悠久的原住民聚集地**[馬太鞍](../Page/馬太鞍.md "wikilink")**與**[太巴塱](../Page/太巴塱.md "wikilink")**，以及[日治時代所立的](../Page/台灣日治時期.md "wikilink")**大和**聚落。日治中期再將馬太鞍因位於大和之北，更名**上大和**，太巴塱則易名**富田**。其中大和屬-{[瑞穗庄](../Page/瑞穗庄.md "wikilink")}-，而上大和、富田屬[鳳林街](../Page/鳳林街.md "wikilink")。

戰後1947年3月將三聚落自原屬鄉鎮分出，成立**光復鄉**（意指「[光復後新設的鄉鎮](../Page/臺灣光復.md "wikilink")」）迄今。今日馬太鞍一般稱**光復**（戰後初期稱為**台安**）、富田名稱不變、大和改名**大富**。而今日[光復車站在](../Page/光復車站.md "wikilink")1947\~1951年間曾一度改稱「台安」，則係取自馬太鞍之名。

2010年鄉長跟鄉代表選舉，則爆發出台灣歷史上最大宗的貪汙賄選案，包含鄉長跟鄉代表12人，全都涉賄遭到羈押獲准，鄉公所一度面臨停擺，2011年3月重新進行鄉長鄉代選舉，結果由[中國國民黨報准參選的獅子會會長謝忠淵當選](../Page/中國國民黨.md "wikilink")。\[3\]

2014年馬太鞍、太巴塱兩部落族人，要求將光復鄉正名為馬太鄉，時任光復鄉長謝忠淵表示鄉內還有一半的族群是閩南、客家人，應彙整全鄉鄉民意見，非鄉長一人可決定。[阿美語](../Page/阿美語.md "wikilink")(Pangcah)稱光復一地為
Fata'an（意思是「樹豆」）。\[4\]北富國民小學，已於1994年恢復原名太巴塱

## 行政區劃

[Guangfu_villages2.svg](https://zh.wikipedia.org/wiki/File:Guangfu_villages2.svg "fig:Guangfu_villages2.svg")
光復鄉共分14村：大安村、大同村、大平村、大馬村、大進村、大全村、大華村、東富村、西富村、南富村、北富村、大富村、大豐村、大興村

## 人口

## 交通

### 鐵路

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [臺東線](../Page/臺東線.md "wikilink")：[光復車站](../Page/光復車站.md "wikilink")
    - [大富車站](../Page/大富車站.md "wikilink")

### 公路

  - [TW_PHW9.svg](https://zh.wikipedia.org/wiki/File:TW_PHW9.svg "fig:TW_PHW9.svg")[台9線](../Page/台9線.md "wikilink")
  - [TW_PHW11a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW11a.svg "fig:TW_PHW11a.svg")[台11甲線](../Page/台11線.md "wikilink")
  - [TW_CHW193.svg](https://zh.wikipedia.org/wiki/File:TW_CHW193.svg "fig:TW_CHW193.svg")[縣道193號](../Page/縣道193號.md "wikilink")

### 客運

  - [花蓮客運](../Page/花蓮客運.md "wikilink")
  - [太魯閣客運](../Page/太魯閣客運.md "wikilink")([台灣好行](../Page/台灣好行.md "wikilink"))

## 教育

### 高級中等學校

  - [國立光復高級商工職業學校](../Page/國立光復高級商工職業學校.md "wikilink")

### 國民中學

  - [花蓮縣立光復國民中學](http://www.kfjhs.hlc.edu.tw/)
  - [花蓮縣立富源國民中學](http://www.fyjh.hlc.edu.tw/)

### 國民小學

  - [花蓮縣光復鄉光復國民小學](http://www.kfps.hlc.edu.tw/)
  - [花蓮縣光復鄉大進國民小學](http://www.dchps.hlc.edu.tw/)
  - [花蓮縣光復鄉大興國民小學](http://www.dsps.hlc.edu.tw/)
  - [花蓮縣光復鄉太巴塱國民小學](http://www.tplps.hlc.edu.tw/)

## 旅遊

[花蓮糖廠.jpg](https://zh.wikipedia.org/wiki/File:花蓮糖廠.jpg "fig:花蓮糖廠.jpg")
[Taiwan_2009_GuangFu_Sugar_Factory_Historical_Train_Exhibition_RD_6173.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_2009_GuangFu_Sugar_Factory_Historical_Train_Exhibition_RD_6173.jpg "fig:Taiwan_2009_GuangFu_Sugar_Factory_Historical_Train_Exhibition_RD_6173.jpg")

  - [花蓮糖廠](../Page/花蓮糖廠.md "wikilink")
  - 馬太鞍溼地
  - 芙登溪自行車專用道(馬太鞍濕地)
  - 大興村土石流紀念公園
  - [大農大富平地森林園區](../Page/大農大富平地森林園區.md "wikilink")
  - 拉索埃湧泉
  - 吉利潭
  - [馬太鞍部落豐年舞祭](../Page/馬太鞍部落.md "wikilink")
  - [太巴塱部落豐年舞祭](../Page/太巴塱部落.md "wikilink")
  - 太巴塱文化發祥地
  - 太巴塱天主堂
  - 馬太鞍教會
  - [保安寺](../Page/保安寺.md "wikilink")
  - 鍾家古厝

## 特產

  - [水稻](../Page/水稻.md "wikilink")
  - [紅糯米](../Page/紅糯米.md "wikilink")、[黑糯米](../Page/黑糯米.md "wikilink")
  - [箭竹筍](../Page/箭竹筍.md "wikilink")
  - [檳榔](../Page/檳榔.md "wikilink")
  - [樹豆](../Page/樹豆.md "wikilink")
  - [蕗蕎](../Page/蕗蕎.md "wikilink")（火蔥）
  - 冰品（光復糖廠）

## 生活機能

  - [全聯福利中心光復店](../Page/全聯福利中心.md "wikilink")
  - [7-Eleven蓮復店](../Page/7-Eleven.md "wikilink")、馬太鞍店
  - [全家便利商店光復車站店](../Page/全家便利商店.md "wikilink")
  - [統冠超市](../Page/統冠超市.md "wikilink")
  - 光豐(光復及豐濱)[農會超市](../Page/農會超市.md "wikilink")

## 知名體育人物

光復鄉培育出不少知名職棒球員，尤其是太巴塱地區為眾。

  - [王光輝](../Page/王光輝_\(棒球選手\).md "wikilink")
  - [林文城](../Page/林文城.md "wikilink")
  - [王光熙](../Page/王光熙.md "wikilink")
  - [陳義信](../Page/陳義信.md "wikilink")
  - [鄭兆行](../Page/鄭兆行.md "wikilink")
  - [余文彬](../Page/余文彬.md "wikilink")
  - [曹錦輝](../Page/曹錦輝.md "wikilink")
  - [徐余偉](../Page/徐余偉.md "wikilink")
  - [張賢智](../Page/張賢智.md "wikilink")
  - [周思齊](../Page/周思齊.md "wikilink")
  - [黃浩然](../Page/黃浩然_\(棒球運動員\).md "wikilink")
  - [鄭承浩](../Page/鄭承浩.md "wikilink")
  - [黃恩賜](../Page/黃恩賜_\(1988年出生\).md "wikilink")
  - [高國輝](../Page/高國輝.md "wikilink")
  - [江忠城](../Page/江忠城.md "wikilink")
  - [羅國龍](../Page/羅國龍.md "wikilink")
  - [高國麟](../Page/高國麟.md "wikilink")
  - [林政賢](../Page/林政賢.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [光復鄉公所](http://www.guangfu.gov.tw)

[Category:平地原住民鄉鎮市](../Category/平地原住民鄉鎮市.md "wikilink")
[花](../Category/臺灣客家文化重點發展區.md "wikilink")
[光復鄉](../Category/光復鄉.md "wikilink")

1.  55公所
    原民「地方通行語」公告了[1](http://news.ltn.com.tw/news/politics/breakingnews/2220153),自由時報
    2017-10-12.
2.  客家基本法修定 客家人逾1/3列通行語[2](http://titv.ipcf.org.tw/news-33794),原視
    2017-10-19.
3.
4.