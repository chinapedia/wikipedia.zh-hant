**孟村回族自治县**位于[沧州市东南](../Page/沧州市.md "wikilink")，1955年11月24日由[盐山](../Page/盐山.md "wikilink")、[黄骅](../Page/黄骅.md "wikilink")、[沧县部分地区合并建立](../Page/沧县.md "wikilink")，是[中华人民共和国最早设立的](../Page/中华人民共和国.md "wikilink")[民族自治县之一](../Page/民族自治县.md "wikilink")。

## 地理

全县基本处于[渤海滨海平原范围内](../Page/渤海.md "wikilink")，地势平坦，海拔平均只有6－10米。年平均气温12.1℃，1月份平均气温-4.7℃，7月份为26.4℃。气候温和。

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、2个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 产业

农业生产以发展果树和蔬菜为主，主要为适应干旱的[枣和以](../Page/枣.md "wikilink")“天津鸭梨”闻名的[梨](../Page/梨.md "wikilink")。

[孟村回族自治县](../Page/category:孟村回族自治县.md "wikilink")
[县/自治县](../Page/category:沧州区县市.md "wikilink")

[Category:河北省民族自治县](../Category/河北省民族自治县.md "wikilink")
[冀](../Category/中国回族自治县.md "wikilink")
[冀](../Category/国家级贫困县.md "wikilink")