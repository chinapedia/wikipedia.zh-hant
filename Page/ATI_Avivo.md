**AVIVO**此技術能提高2D影像質素。在TV Card ATi Theater 550出現过，現加入到顯示卡中。[Radeon X1
Series全系列都會支援此技術](../Page/Radeon_X1_Series.md "wikilink")。AVIVO是[nVidia](../Page/nVidia.md "wikilink")
[Pure Video的对手](../Page/Pure_Video.md "wikilink")。

AVIVO技術是針对影像播放中的Capture、Encode、Decode、Process及Display五個項目。

  - Capture方面

強化訊號來源，自動調整影像的亮度及對比度；採用12-bit轉換器，減少轉換時的資料損失；硬體去雜訊可以呈現更純淨的畫面。

  - Decode方面

核心硬件支援H.264、VC-1、WMV9、MPEG-4等格式解碼，降低CPU的使用率。

  - Encode方面

VPU搭配了一个附屬軟體，用來執行影片轉碼，这个軟體支援H.264、VC-1、WMV9、WMV9
PMC、MPEG-2、MPEG-4、DivX等格式轉碼。

  - Process方面

AVIVO支援De-Interlacing、Video Scaling,Spatial Temporal

  -   - Vector Adaptive de-interlacing

減低影像鋸齒，和畫面上的一條一條線

  -   - advanced video scaling

按比例縮放來源影像，減少鋸齒及毛邊

  - Display方面
      - Radeon X1800XT 內建兩組Dual Link TMDS Transmitters，解像度最高支援2560 x
        1600，因此Radeon X1800XT配合AVIVO能支援兩個30吋LCD顯示器。
      - 支援10Bit顯示引擎
      - 支援双DVI輸出
      - Gamma校正
      - 色彩校正

ATi Avivo Video Converter:

Catalyst控制中心集成了這个软件，是一个视频转换软件，支持硬件压缩，所有使用[Radeon X1
Series系列顯示卡的用户都可以使用](../Page/Radeon_X1_Series.md "wikilink")。它支持相当多多媒体格式，包括PSP和IPod的影音格式。

Avivo技術的競爭對手是[nVidia的](../Page/nVidia.md "wikilink")[Pure
Video技術](../Page/Pure_Video.md "wikilink")、[矽統科技的](../Page/SIS.md "wikilink")[Real
Video技術](../Page/Real_Video.md "wikilink")，和[Intel的](../Page/Intel.md "wikilink")[Clear
Video技術](../Page/Clear_Video.md "wikilink")。

## 参见

[Category:ATI顯示卡](../Category/ATI顯示卡.md "wikilink")
[Category:视訊加速](../Category/视訊加速.md "wikilink")