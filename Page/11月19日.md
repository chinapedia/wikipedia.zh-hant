**11月19日**是[阳历一年中的第](../Page/阳历.md "wikilink")323天（[闰年第](../Page/闰年.md "wikilink")324天），离全年的结束还有42天。

## 大事记

### 5世紀

  - [461年](../Page/461年.md "wikilink")：[利比乌斯·塞维鲁被权臣](../Page/利比乌斯·塞维鲁.md "wikilink")[里西默推上的](../Page/里西默.md "wikilink")[西羅馬帝國皇帝的宝座](../Page/西羅馬帝國.md "wikilink")。而国家实际的权力，仍然掌握在里西默手中。

### 12世紀

  - [1148年](../Page/1148年.md "wikilink")：（[金熙宗](../Page/金熙宗.md "wikilink")[皇统八年十月初七](../Page/皇统.md "wikilink")）[金朝太师](../Page/金朝.md "wikilink")、领三省事、都元帅、越国王[完颜宗弼](../Page/完颜宗弼.md "wikilink")（金兀术）逝世。

### 15世紀

  - [1493年](../Page/1493年.md "wikilink")：[欧洲航海家](../Page/欧洲.md "wikilink")[哥伦布率领的船队在第二次横渡](../Page/克里斯托弗·哥伦布.md "wikilink")[大西洋的远航中在](../Page/大西洋.md "wikilink")[波多黎各登陆](../Page/波多黎各.md "wikilink")，成为首批到来的[欧洲人](../Page/欧洲人.md "wikilink")。

### 19世紀

  - [1816年](../Page/1816年.md "wikilink")：[华沙大学建校](../Page/华沙大学.md "wikilink")。
  - [1831年](../Page/1831年.md "wikilink")：[大哥伦比亚共和国解体](../Page/大哥伦比亚共和国.md "wikilink")。
  - [1850年](../Page/1850年.md "wikilink")：[丁尼生就任](../Page/亞弗烈·但尼生.md "wikilink")[英國桂冠詩人](../Page/桂冠詩人.md "wikilink")
  - [1863年](../Page/1863年.md "wikilink")：在[南北战争期间](../Page/南北战争.md "wikilink")，[美国总统](../Page/美国总统.md "wikilink")[林肯在](../Page/亞伯拉罕·林肯.md "wikilink")[賓夕法尼亞州](../Page/賓夕法尼亞州.md "wikilink")[蓋茲堡发表](../Page/蓋茲堡.md "wikilink")[蓋茲堡演说](../Page/蓋茲堡演说.md "wikilink")。
  - [1867年](../Page/1867年.md "wikilink")：东[捻军受挫于](../Page/捻军.md "wikilink")[江苏](../Page/江苏省.md "wikilink")[赣榆](../Page/赣榆县.md "wikilink")，首领[任化邦被部下](../Page/任化邦.md "wikilink")[潘贵升刺杀](../Page/潘贵升.md "wikilink")。

### 20世紀

  - [1924年](../Page/1924年.md "wikilink")：[中国共产党发表](../Page/中国共产党.md "wikilink")《第四次对时局主张》，支持[中華民國國父孙中山的](../Page/孫中山.md "wikilink")《北上宣言》。
  - [1931年](../Page/1931年.md "wikilink")：[中國航空一架](../Page/中國航空.md "wikilink")型郵政飛機在大霧中[濟南](../Page/濟南.md "wikilink")[開山](../Page/開山_\(濟南\).md "wikilink")，包括[徐志摩在內多人罹難](../Page/徐志摩.md "wikilink")。
  - [1935年](../Page/1935年.md "wikilink")：[红二军团和](../Page/红二军团.md "wikilink")[红六军团开始](../Page/红六军团.md "wikilink")[长征](../Page/长征.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：在[斯大林格勒战役中](../Page/斯大林格勒战役.md "wikilink")，[苏联元帅](../Page/苏联元帅.md "wikilink")[朱可夫率领军队向](../Page/朱可夫.md "wikilink")[德国军队发动](../Page/德国.md "wikilink")[天王星行动](../Page/天王星行动.md "wikilink")'''进行反攻。
  - [1945年](../Page/1945年.md "wikilink")：[中国](../Page/中国.md "wikilink")[重庆文化界](../Page/重庆市.md "wikilink")、工商界代表举行反[内战大会](../Page/国共内战.md "wikilink")，成立“重庆各界人民反内战联合会”。
  - [1954年](../Page/1954年.md "wikilink")：欧洲历史最悠久的私人电视频道[蒙地卡罗电视台创立](../Page/蒙地卡罗电视台.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")：[哈尔滨医科大学第二附属医院狗头移植实验成功](../Page/哈尔滨医科大学第二附属医院.md "wikilink")，存活5天半。\[1\]
  - [1967年](../Page/1967年.md "wikilink")：[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")（無綫電視）正式在[香港启播](../Page/香港.md "wikilink")，成為香港第一間免費電視。
  - [1969年](../Page/1969年.md "wikilink")：[巴西](../Page/巴西.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")[贝利在](../Page/贝利.md "wikilink")[里约热内卢代表](../Page/里约热内卢.md "wikilink")[桑托斯队迎战](../Page/桑托斯足球俱乐部.md "wikilink")[达伽马队的比赛中踢入他足球生涯的第](../Page/華斯高.md "wikilink")1000颗进球。
  - [1971年](../Page/1971年.md "wikilink")：[国际民航组织承认](../Page/国际民航组织.md "wikilink")[中华人民共和国的代表为中国唯一合法代表](../Page/中华人民共和国.md "wikilink")。
  - 1971年：[麥理浩出任](../Page/麥理浩.md "wikilink")[香港總督](../Page/香港總督.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[中壢事件](../Page/中壢事件.md "wikilink")。
  - 1977年：[埃及总统](../Page/埃及总统.md "wikilink")[安瓦尔·萨达特访问](../Page/萨达特.md "wikilink")[以色列](../Page/以色列.md "wikilink")，成为首位访问以色列的[阿拉伯国家元首](../Page/阿拉伯国家.md "wikilink")。
  - 1977年：[TAP葡萄牙航空](../Page/TAP葡萄牙航空.md "wikilink")在[马德拉群岛降落時衝出跑道墜毀](../Page/馬德拉.md "wikilink")。機上156名乘客及8名組員中，共131人不幸罹難。
  - [1980年](../Page/1980年.md "wikilink")：[中國大陸第一支时装表演队诞生](../Page/中国大陆.md "wikilink")。
  - 1980年：。
  - 1980年：[大韩航空015号班机在汉城](../Page/大韓航空015號班機空難.md "wikilink")[金浦国际机场降落时坠毁](../Page/金浦国际机场.md "wikilink")，15人遇难。
  - [1982年](../Page/1982年.md "wikilink")：[第九届亚运会在](../Page/1982年亚洲运动会.md "wikilink")[印度首都](../Page/印度.md "wikilink")[新德里举行](../Page/新德里.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[墨西哥城](../Page/墨西哥城.md "wikilink")[石油气大爆炸](../Page/石油气.md "wikilink")，500余人丧生。
  - 1984年：[中国第一支](../Page/中国.md "wikilink")[南极考察队乘](../Page/南极考察队.md "wikilink")“[向阳红10号](../Page/向阳红系列海洋调查船.md "wikilink")”出发。
  - [1985年](../Page/1985年.md "wikilink")：[美国和](../Page/美國.md "wikilink")[苏联在](../Page/苏联.md "wikilink")[日内瓦举行裁军会谈](../Page/日内瓦.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：中国在[南盘江上兴建大型](../Page/南盘江.md "wikilink")[水电站](../Page/水力發電.md "wikilink")。
  - 1986年：中国野生[东北虎已不足三十只](../Page/东北虎.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：台灣准许滯留大陸之台籍[国軍老兵返故乡](../Page/国軍.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：中國[廣東](../Page/广东省.md "wikilink")[深圳由香港商人投資的](../Page/深圳市.md "wikilink")[致麗玩具廠發生大火](../Page/致麗玩具廠.md "wikilink")，造成87人死亡，47人受傷的慘劇。
  - [1998年](../Page/1998年.md "wikilink")：經[英女皇御准的](../Page/伊利沙伯二世.md "wikilink")《[1998年蘇格蘭法案](../Page/1998年蘇格蘭法案.md "wikilink")》正式生效。
  - [1999年](../Page/1999年.md "wikilink")：[拉萨百货大楼正式投入运营](../Page/拉萨百货大楼.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink"):
    [中國](../Page/中國.md "wikilink")[神舟飛船首艘實驗太空船](../Page/神舟飛船.md "wikilink")[神舟一號自](../Page/神舟一號.md "wikilink")[內蒙古自治區](../Page/內蒙古自治區.md "wikilink")[阿拉善盟](../Page/阿拉善盟.md "wikilink")[酒泉衛星發射中心發射升空](../Page/中國酒泉衛星發射中心.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")：[澳門格蘭披治大賽車發生嚴重意外](../Page/澳門格蘭披治大賽車.md "wikilink")，一輛賽車在練習期間[衝出跑道並撞倒途人](../Page/2000年澳門大賽車衝出賽道意外.md "wikilink")，造成1死3傷。

### 21世紀

  - [2004年](../Page/2004年.md "wikilink"):
    [美國](../Page/美國.md "wikilink")[籃球球隊](../Page/籃球球隊.md "wikilink")[底特律活塞和](../Page/底特律活塞.md "wikilink")[印第安納溜馬球員發生](../Page/印第安納溜馬.md "wikilink")[NBA史上最嚴重的](../Page/NBA.md "wikilink")[鬥毆事件](../Page/鬥毆事件.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：效力[曼聯長達](../Page/曼联足球俱乐部.md "wikilink")12年的34歲隊長[堅尼](../Page/罗伊·基恩.md "wikilink")（Roy
    Keane）與球會提前解約。
  - [2014年](../Page/2014年.md "wikilink")：不獲[免費電視發牌的](../Page/香港免費電視牌照爭議.md "wikilink")[香港電視網絡選擇在香港電視業大哥大](../Page/香港電視網絡.md "wikilink")——[無綫電視的台慶日當日透過互聯網開台](../Page/無綫電視.md "wikilink")，以網絡電視形式播放。

## 出生

[Hongwu1.jpg](https://zh.wikipedia.org/wiki/File:Hongwu1.jpg "fig:Hongwu1.jpg")[袞龍袍像](../Page/袞龍袍.md "wikilink")\]\]

  - [1168年](../Page/1168年.md "wikilink")：[宋宁宗赵扩](../Page/宋宁宗.md "wikilink")，[南宋皇帝](../Page/南宋.md "wikilink")。（[1224年逝世](../Page/1224年.md "wikilink")）
  - [1328年](../Page/1328年.md "wikilink")：[明太祖朱元璋](../Page/明太祖.md "wikilink")，[明朝開國皇帝](../Page/明朝.md "wikilink")。（[1398年逝世](../Page/1398年.md "wikilink")）
  - [1600年](../Page/1600年.md "wikilink")：[查理一世](../Page/查理一世_\(英國\).md "wikilink")，英格蘭、蘇格蘭、與愛爾蘭國王。（[1649年逝世](../Page/1649年.md "wikilink")）
  - [1711年](../Page/1711年.md "wikilink")：[米哈伊尔·瓦西里耶维奇·罗蒙诺索夫](../Page/米哈伊尔·瓦西里耶维奇·罗蒙诺索夫.md "wikilink")，[俄罗斯学者](../Page/俄罗斯.md "wikilink")、诗人。（逝於[1765年](../Page/1765年.md "wikilink")）
  - [1831年](../Page/1831年.md "wikilink")：[詹姆斯·艾布拉姆·加菲爾德](../Page/詹姆斯·艾布拉姆·加菲爾德.md "wikilink")，[美國第](../Page/美國.md "wikilink")20任總統。（逝於[1881年](../Page/1881年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[米哈伊尔·伊万诺维奇·加里宁](../Page/米哈伊尔·伊万诺维奇·加里宁.md "wikilink")，[苏联领导人](../Page/苏联.md "wikilink")、政治家。（逝於[1946年](../Page/1946年.md "wikilink")）
  - [1907年](../Page/1907年.md "wikilink")：[邵逸夫](../Page/邵逸夫.md "wikilink")，香港電影製作者，娛樂業大亨。（逝於[2014年](../Page/2014年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[英迪拉·甘地](../Page/英迪拉·甘地.md "wikilink")，[印度前总理](../Page/印度.md "wikilink")，国大党主席。（逝於[1984年](../Page/1984年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[秦厚修](../Page/秦厚修.md "wikilink")，[中華民國第十二任](../Page/中華民國.md "wikilink")、第十三任總統[馬英九之母](../Page/馬英九.md "wikilink")（逝於[2014年](../Page/2014年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[李遠哲](../Page/李遠哲.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[化学家](../Page/化学家.md "wikilink")，[1986年](../Page/1986年.md "wikilink")[諾貝爾化學獎得主](../Page/诺贝尔化学奖.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[卡尔文·克莱因](../Page/卡爾文·克雷恩_\(設計師\).md "wikilink")，[美國時尚設計師](../Page/美國.md "wikilink")、企業家。
  - [1944年](../Page/1944年.md "wikilink")：[歐晉德](../Page/歐晉德.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[台灣](../Page/台灣.md "wikilink")[台北市前副市長](../Page/台北市.md "wikilink")，[台灣高鐵公司前董事長](../Page/台灣高鐵.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[徐風](../Page/徐風.md "wikilink")，[台灣男藝人](../Page/台灣.md "wikilink")。（逝於[2013年](../Page/2013年.md "wikilink")）
  - [1950年](../Page/1950年.md "wikilink")：[邵曉鈴](../Page/邵曉鈴.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[台灣](../Page/台灣.md "wikilink")[台中市前市長](../Page/台中市.md "wikilink")[胡志強夫人](../Page/胡志強.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[潘越雲](../Page/潘越雲.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[梅格·瑞安](../Page/梅格·瑞安.md "wikilink")，[美國女演員](../Page/美國.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：[朱迪·福斯特](../Page/茱迪·科士打.md "wikilink")，[美国女演员](../Page/美國.md "wikilink")，兩度榮獲[奧斯卡最佳女主角獎](../Page/奥斯卡最佳女主角奖.md "wikilink")。
  - [1962年](../Page/1962年.md "wikilink")：[江華](../Page/江華_\(演員\).md "wikilink")，[香港男演員](../Page/香港.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[羅百吉](../Page/羅百吉.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：[龍騎士07](../Page/龍騎士07.md "wikilink")，[日本遊戲](../Page/日本.md "wikilink")[製作人](../Page/製作人.md "wikilink")、[插畫家](../Page/插畫家.md "wikilink")、[小說家](../Page/小說家.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：[柴田淳](../Page/柴田淳.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[希娜·拉巴尼·哈尔](../Page/希娜·拉巴尼·哈尔.md "wikilink")，[巴基斯坦女政治人物](../Page/巴基斯坦.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[陳凱欣](../Page/陳凱欣.md "wikilink")，現任香港立法會議員、前香港新聞主播、前任[食物及衛生局政治助理](../Page/食物及衛生局.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[大澤千秋](../Page/大澤千秋.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/日本配音員.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：[黛莉亞·維寶莉](../Page/黛莉亞·維寶莉.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")[超模](../Page/超模.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[蘭茜·艾靈臣](../Page/蘭茜·艾靈臣.md "wikilink")，[美國著名模特兒](../Page/美國.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[李睿紳](../Page/李睿紳.md "wikilink")，[台灣著名模特兒](../Page/台灣.md "wikilink")、演員。
  - [1991年](../Page/1991年.md "wikilink")：
    [武磊](../Page/武磊.md "wikilink")，中国足球运动员
  - [1991年](../Page/1991年.md "wikilink")：[周梓盈](../Page/周梓盈.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")。
  - [1995年](../Page/1995年.md "wikilink")：[瓦妮莎·艾森特](../Page/瓦妮莎·艾森特.md "wikilink")，[匈牙利女性模特兒](../Page/匈牙利.md "wikilink")。
  - [1997年](../Page/1997年.md "wikilink")：[柳洙正](../Page/柳洙正.md "wikilink")，[韓國女子組合](../Page/韓國.md "wikilink")[Lovelyz成員](../Page/Lovelyz.md "wikilink")

## 逝世

  - [496年](../Page/496年.md "wikilink")：[教宗哲拉修一世](../Page/教宗哲拉修一世.md "wikilink")，教宗
  - [498年](../Page/498年.md "wikilink")：[教宗亚纳大削二世](../Page/教宗亚纳大削二世.md "wikilink")，教宗
  - [930年](../Page/930年.md "wikilink")：[嚴可求](../Page/嚴可求.md "wikilink")，中國官員
  - [1092年](../Page/1092年.md "wikilink")：[梅利克·沙阿一世](../Page/梅利克·沙阿一世_\(塞尔柱帝国\).md "wikilink")，[塞爾柱帝國蘇丹](../Page/塞爾柱帝國.md "wikilink")
  - [1148年](../Page/1148年.md "wikilink")：[完颜宗弼](../Page/完颜宗弼.md "wikilink")（[金兀术](../Page/金兀术.md "wikilink")），[金朝太师](../Page/金朝.md "wikilink")、领三省事、都元帅、越国王，[金太祖](../Page/金太祖.md "wikilink")[完顏阿骨打第四子](../Page/完顏阿骨打.md "wikilink")。
  - [1577年](../Page/1577年.md "wikilink")：[松永久秀](../Page/松永久秀.md "wikilink")，日本戰國時期大名（生於1510年）
  - [1665年](../Page/1665年.md "wikilink")：[尼古拉·普桑](../Page/尼古拉·普桑.md "wikilink")，法國[畫家](../Page/畫家.md "wikilink")（生於1594年）
  - [1828年](../Page/1828年.md "wikilink")：[舒伯特](../Page/舒伯特.md "wikilink")，[奥地利作曲家](../Page/奥地利.md "wikilink")（生於1791年）
  - [1887年](../Page/1887年.md "wikilink")：，[美国诗人](../Page/美国诗人.md "wikilink")（生於1849年）
  - [1931年](../Page/1931年.md "wikilink")：[徐志摩](../Page/徐志摩.md "wikilink")，中国诗人（生於1897年）
  - [1942年](../Page/1942年.md "wikilink")：[布鲁诺·舒尔茨](../Page/布鲁诺·舒尔茨.md "wikilink")，[波蘭作家](../Page/波蘭.md "wikilink")（生於1892年）
  - [1949年](../Page/1949年.md "wikilink")：[詹姆斯·恩索爾](../Page/詹姆斯·恩索爾.md "wikilink")，比利時畫家（生於1860年）
  - [1987年](../Page/1987年.md "wikilink")：[李秉喆](../Page/李秉喆.md "wikilink")，[三星集团创始人兼首任会长](../Page/三星集团.md "wikilink")（生於1910年）
  - [1990年](../Page/1990年.md "wikilink")：[孫立人](../Page/孫立人.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[陸軍](../Page/陸軍.md "wikilink")[總司令](../Page/總司令.md "wikilink")（生於1900年）
  - [1998年](../Page/1998年.md "wikilink")：[藤田哲也](../Page/藤田哲也_\(气象学家\).md "wikilink")，美籍日本氣象學家（生於1920年）
  - [2003年](../Page/2003年.md "wikilink")：[李九龙](../Page/李九龙.md "wikilink")，[成都军区原司令员](../Page/成都军区.md "wikilink")，上将（生於1929年）
  - 2003年：[施蛰存](../Page/施蛰存.md "wikilink")，[作家](../Page/作家.md "wikilink")、[诗人](../Page/诗人.md "wikilink")、[文学翻译家](../Page/文学翻译家.md "wikilink")（生於1905年）
  - [2005年](../Page/2005年.md "wikilink")：，法国賽車手\[2\]（生於1960年）
  - [2009年](../Page/2009年.md "wikilink")：[鮑偉華](../Page/鮑偉華.md "wikilink")，汶萊上訴法庭庭長，香港前首席大法官（生於1929年）
  - [2013年](../Page/2013年.md "wikilink")：[弗雷德里克·桑格](../Page/弗雷德里克·桑格.md "wikilink")，英國生物化學家（生於1918年）
  - [2014年](../Page/2014年.md "wikilink")：[麥克·尼可斯](../Page/麥克·尼可斯.md "wikilink")，[美國著名](../Page/美國.md "wikilink")[導演](../Page/導演.md "wikilink")\[3\]（生於1931年）

## 节假日和习俗

  - [2001年起](../Page/2001年.md "wikilink")，[世界廁所組織定每年的](../Page/世界廁所組織.md "wikilink")11月19日為[世界廁所日](../Page/世界廁所日.md "wikilink")。
  - [解放日](../Page/解放日.md "wikilink")（[馬里](../Page/馬里.md "wikilink")）
  - [摩納哥](../Page/摩納哥.md "wikilink")：[國慶節](../Page/國慶節.md "wikilink")
  - [国际男人节](../Page/国际男人节.md "wikilink")
  - [國際女企業家日](../Page/國際女企業家日.md "wikilink")

## 参考资料

1.
2.
3.