**丸龜藩**（）為[日本古代領有](../Page/日本.md "wikilink")[讚岐國](../Page/讚岐國.md "wikilink")（[香川縣](../Page/香川縣.md "wikilink")）西部，以[丸龜城](../Page/丸龜城.md "wikilink")（現[丸龜市](../Page/丸龜市.md "wikilink")）為本城的[藩](../Page/藩.md "wikilink")。藩主至[廢藩置縣為止](../Page/廢藩置縣.md "wikilink")，歷任[生駒氏](../Page/生駒氏.md "wikilink")、[山崎氏](../Page/山崎氏.md "wikilink")、[京極氏](../Page/京極氏.md "wikilink")。

不過[生駒氏是以](../Page/生駒氏.md "wikilink")[高松城為本城領有](../Page/高松城_\(讚岐國\).md "wikilink")[讚岐一國的關係](../Page/讚岐國.md "wikilink")，將[生駒氏領有時的西讚岐也稱為](../Page/生駒氏.md "wikilink")**丸龜藩**似乎也不太適當，現在將當時的狀況也列入丸龜藩歷史的一部分，此處也包含在內。
[Keep_of_Marugame_Castle.jpg](https://zh.wikipedia.org/wiki/File:Keep_of_Marugame_Castle.jpg "fig:Keep_of_Marugame_Castle.jpg")

## 歷史

  - 1587年
    [織田信長與](../Page/織田信長.md "wikilink")[豐臣秀吉時代有功的](../Page/豐臣秀吉.md "wikilink")[生駒親正封於](../Page/生駒親正.md "wikilink")[讚岐國](../Page/讚岐國.md "wikilink")。
  - 1597年 開始於龜山修築[丸龜城](../Page/丸龜城.md "wikilink")。
  - 1602年
    [丸龜城完成](../Page/丸龜城.md "wikilink")，[生駒親正](../Page/生駒親正.md "wikilink")[嫡長子](../Page/嫡長子.md "wikilink")[生駒一正將之作為居城](../Page/生駒一正.md "wikilink")。
  - 1615年
    [播州](../Page/播磨國.md "wikilink")[赤穂人開始於本地進行製鹽業](../Page/赤穗市.md "wikilink")。[一國一城令發布後](../Page/一國一城令.md "wikilink")，[丸龜城陷入拆毀危機](../Page/丸龜城.md "wikilink")。
  - 1640年
    [生駒騷動發生](../Page/生駒騷動.md "wikilink")，[生駒氏遭](../Page/生駒氏.md "wikilink")[改易](../Page/改易.md "wikilink")。
  - 1641年
    [山崎家治由](../Page/山崎家治.md "wikilink")[肥後](../Page/肥後國.md "wikilink")[富岡以](../Page/富岡藩.md "wikilink")5萬石改封至西讚岐，並以[丸龜城為本城設立](../Page/丸龜城.md "wikilink")**丸龜藩**。[生駒氏](../Page/生駒氏.md "wikilink")[改易至](../Page/改易.md "wikilink")[山崎氏入封為止](../Page/山崎氏.md "wikilink")，[讚岐國由隣國](../Page/讚岐國.md "wikilink")[伊予國](../Page/伊予國.md "wikilink")[西條藩](../Page/西條藩.md "wikilink")、[大洲藩](../Page/大洲藩.md "wikilink")、[今治藩分割統治](../Page/今治藩.md "wikilink")。
  - 1642年 開始改建[丸龜城](../Page/丸龜城.md "wikilink")。
  - 1658年
    [山崎氏經](../Page/山崎氏.md "wikilink")3代而斷絕。代替[改易的](../Page/改易.md "wikilink")[山崎氏](../Page/山崎氏.md "wikilink")，[京極高和由](../Page/京極高和.md "wikilink")[播磨國](../Page/播磨國.md "wikilink")[龍野藩以](../Page/龍野藩.md "wikilink")6萬石轉封至此。
  - 1660年
    現在[丸龜城的](../Page/丸龜城.md "wikilink")[天守完成](../Page/天守.md "wikilink")。
  - 1670年 丸龜城大手第一門、第二門由南往北移築。

[Nakatsubansyoen.jpg](https://zh.wikipedia.org/wiki/File:Nakatsubansyoen.jpg "fig:Nakatsubansyoen.jpg")

  - 1688年 在下金倉村中洲建設別邸，名為[中津萬象園](../Page/中津萬象園.md "wikilink")。
  - 1694年
    [京極高通得到多度津的](../Page/京極高通_\(多度津藩主\).md "wikilink")1萬石領地，建立[多度津藩](../Page/丸龜藩#支藩.md "wikilink")。
  - 1705年 首次發行[藩札](../Page/藩札.md "wikilink")。
  - [天明年間](../Page/天明.md "wikilink")
    開始獎勵[藩士製造](../Page/藩士.md "wikilink")[團扇作為副業](../Page/團扇.md "wikilink")。
  - 1794年 設立藩校正明館。
  - 1858年 完成[西讚府志](../Page/西讚府志.md "wikilink")。
  - 1871年
    [廢藩置縣後成為](../Page/廢藩置縣.md "wikilink")[丸龜縣](../Page/丸龜縣.md "wikilink")。

## 藩政

[Noboru_Segawa-KonpiraKouTourou.jpg](https://zh.wikipedia.org/wiki/File:Noboru_Segawa-KonpiraKouTourou.jpg "fig:Noboru_Segawa-KonpiraKouTourou.jpg")
**丸龜藩**為往[金刀比羅宮的](../Page/金刀比羅宮.md "wikilink")[參道](../Page/參道.md "wikilink")[丸龜街道](../Page/丸龜街道.md "wikilink")、[多度津街道的起點所在](../Page/多度津街道.md "wikilink")，以進香客為目標的觀光業是藩內收入的重心。

幕末時因為財政逐漸緊迫，駐紮於[江戶的藩士們與](../Page/江戶.md "wikilink")[大村藩](../Page/大村藩.md "wikilink")（兩藩在[江戶的辦公室剛好是鄰居](../Page/江戶.md "wikilink")）藩士們學習[團扇的製作方法](../Page/團扇.md "wikilink")。返鄉後以此作為副業，並將[團扇當作本地土産販賣](../Page/團扇.md "wikilink")，也因此拯救了藩內經濟。之後還把[團扇的製作方法在一般民眾間廣為傳授](../Page/團扇.md "wikilink")，從此成為丸龜的名產。

## 歷代藩主

### 生駒氏

[外樣大名](../Page/外樣大名.md "wikilink") 17萬3千石（1587年－1640年）

1.  [生駒親正](../Page/生駒親正.md "wikilink")〔[從四位下](../Page/從四位.md "wikilink")、雅樂頭〕
2.  [生駒一正](../Page/生駒一正.md "wikilink")〔[從四位下](../Page/從四位.md "wikilink")、讚岐守〕
3.  [生駒正俊](../Page/生駒正俊.md "wikilink")〔[從四位下](../Page/從四位.md "wikilink")、讚岐守〕
4.  [生駒高俊](../Page/生駒高俊.md "wikilink")〔[從四位下](../Page/從四位.md "wikilink")、壱岐守〕

### 山崎氏

[外樣大名](../Page/外樣大名.md "wikilink") 5萬石（1641年－1658年）

1.  [山崎家治](../Page/山崎家治.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、甲斐守〕
2.  [山崎俊家](../Page/山崎俊家.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、志摩守〕
3.  [山崎治賴](../Page/山崎治賴.md "wikilink")〔夭折之故無官職〕

### 京極氏

[外樣大名](../Page/外樣大名.md "wikilink") 6萬石 → 5萬1千石（1658年－1871年）

1.  [京極高和](../Page/京極高和.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、刑部少輔〕
2.  [京極高豐](../Page/京極高豐.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、備中守〕
3.  [京極高或](../Page/京極高或.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、若狹守〕分封支藩後成為5萬1千石
4.  [京極高矩](../Page/京極高矩.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、佐渡守〕
5.  [京極高中](../Page/京極高中.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、若狹守〕
6.  [京極高朗](../Page/京極高朗.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、長門守〕
7.  [京極朗徹](../Page/京極朗徹.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、佐渡守〕

## 支藩

### 多度津藩

**多度津藩**（[日文](../Page/日文.md "wikilink")：*たどつはん*）為丸龜藩的[支藩](../Page/支藩.md "wikilink")。領有多度津（[香川縣](../Page/香川縣.md "wikilink")[仲多度郡](../Page/仲多度郡.md "wikilink")[多度津町](../Page/多度津町.md "wikilink")）周圍的1萬石，於多度津設立[陣屋](../Page/陣屋.md "wikilink")（早期是在丸龜城内設置居館）。

1658年[山崎氏](../Page/山崎氏.md "wikilink")[改易後改封至](../Page/改易.md "wikilink")**丸龜藩**的[京極高和將多度津收歸領地](../Page/京極高和.md "wikilink")。

由於丸龜藩第3代藩主[京極高或](../Page/京極高或.md "wikilink")3歳時就任藩主之位，恐其夭折的緣故故向[德川幕府申請分封其庶兄](../Page/德川幕府.md "wikilink")[京極高通為分家](../Page/京極高通_\(多度津藩主\).md "wikilink")。
1694年6月18日，、1萬石分封受到許可，設立多度津藩。
不過因為[京極高通本身也才](../Page/京極高通_\(多度津藩主\).md "wikilink")4歳的關係，在[丸龜城內設置了](../Page/丸龜城.md "wikilink")[陣屋居住](../Page/陣屋.md "wikilink")。至1711年
[京極高通才正式以多度津藩主之身管理政務](../Page/京極高通_\(多度津藩主\).md "wikilink")。

第4代藩主[京極高賢在](../Page/京極高賢.md "wikilink")1827年向幕府提出設立[陣屋的申請](../Page/陣屋.md "wikilink")，並於該年在多度津建設[陣屋](../Page/陣屋.md "wikilink")，從此搬離丸龜城。

第6代藩主[京極高典追隨本家丸龜藩主](../Page/京極高典.md "wikilink")[京極朗徹加入尊王派](../Page/京極朗徹.md "wikilink")，並於[戊辰戰爭與](../Page/戊辰戰爭.md "wikilink")[土佐藩](../Page/土佐藩.md "wikilink")、丸龜藩一起攻擊佐幕派的[高松藩](../Page/高松藩.md "wikilink")。

1871年[廢藩置縣後成為](../Page/廢藩置縣.md "wikilink")[倉敷縣](../Page/倉敷縣.md "wikilink")，之後經由[名東縣編入](../Page/名東縣.md "wikilink")[香川縣](../Page/香川縣.md "wikilink")。

#### 歷代藩主

  - 京極氏

[外樣大名](../Page/外樣大名.md "wikilink") 1萬石

1.  [京極高通](../Page/京極高通_\(多度津藩主\).md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、壱岐守〕
2.  [京極高慶](../Page/京極高慶_\(多度津藩主\).md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、出羽守〕
3.  [京極高文](../Page/京極高文.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、壱岐守〕
4.  [京極高賢](../Page/京極高賢.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、壱岐守〕
5.  [京極高琢](../Page/京極高琢.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、壱岐守〕
6.  [京極高典](../Page/京極高典.md "wikilink")〔[從五位下](../Page/從五位.md "wikilink")、壱岐守〕

## 相關條目

  - [生駒氏](../Page/生駒氏.md "wikilink")
  - [山崎氏](../Page/山崎氏.md "wikilink")
  - [京極氏](../Page/京極氏.md "wikilink")
  - [丸龜市](../Page/丸龜市.md "wikilink")
  - [藩列表](../Page/藩列表.md "wikilink")

[Category:藩](../Category/藩.md "wikilink")
[Category:近江山崎氏](../Category/近江山崎氏.md "wikilink")
[Category:讚岐國](../Category/讚岐國.md "wikilink")
[Category:丸龜藩](../Category/丸龜藩.md "wikilink")
[Category:京極氏](../Category/京極氏.md "wikilink")
[Category:生駒氏](../Category/生駒氏.md "wikilink")