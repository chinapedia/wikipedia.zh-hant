**日經CNBC**（日語：**日経CNBC**）是一家[日本的](../Page/日本.md "wikilink")[財經](../Page/財經.md "wikilink")[新聞台](../Page/新聞台.md "wikilink")，由[CNBC亞洲台與](../Page/CNBC亞洲台.md "wikilink")[日本經濟新聞社共同持有](../Page/日本經濟新聞社.md "wikilink")。

## 節目

### 現行播放節目

日經CNBC也會聯播[CNBC美國台的節目](../Page/CNBC美國台.md "wikilink")，其中[Worldwide
Exchange和](../Page/Worldwide_Exchange.md "wikilink")[Squawk on the
Street節目會同步](../Page/Squawk_on_the_Street.md "wikilink")[口譯成](../Page/口譯.md "wikilink")[日文](../Page/日文.md "wikilink")，[Power
Lunch](../Page/Power_Lunch.md "wikilink")、[Street
Signs和](../Page/Street_Signs.md "wikilink")[Closing
Bell節目則沒有](../Page/Closing_Bell.md "wikilink")。

### 過去節目

  - *[Tokyo Squawk Box](../Page/Tokyo_Squawk_Box.md "wikilink")*

## 記者群與主播群

### 現任者

### 離職者

## 日經CNBC口號

## 參見

  - [CNBC](../Page/CNBC.md "wikilink")

## 外部連結

  - [日經CNBC官方網站](http://www.nikkei-cnbc.co.jp/)

[Category:1999年成立的电视台或电视频道](../Category/1999年成立的电视台或电视频道.md "wikilink")
[Category:日本經濟新聞](../Category/日本經濟新聞.md "wikilink")
[Category:日本衛星電視頻道](../Category/日本衛星電視頻道.md "wikilink")