**东湖区**是[中国](../Page/中国.md "wikilink")[江西省](../Page/江西省.md "wikilink")[南昌市的一个市辖区](../Page/南昌市.md "wikilink")。总面积为22.3平方公里，2003年人口为44.4万。
2015年到2016年初区政府陆续搬迁到三经路附近\[1\]\[2\]。

## 行政区划

2013年，东湖区辖10个街道：

  - 街道：[公园街道](../Page/公园街道.md "wikilink")、[滕王阁街道](../Page/滕王阁街道.md "wikilink")、[八一桥街道](../Page/八一桥街道.md "wikilink")、[百花洲街道](../Page/百花洲街道.md "wikilink")、[墩子塘街道](../Page/墩子塘街道.md "wikilink")、[大院街道](../Page/大院街道.md "wikilink")、[豫章街道](../Page/豫章街道.md "wikilink")、[董家窑街道](../Page/董家窑街道.md "wikilink")、[彭家桥街道](../Page/彭家桥街道.md "wikilink")、[沙井街道](../Page/沙井街道.md "wikilink")（红谷滩新区）

## 参考文献

## 外部链接

  - [南昌东湖区政府网站](http://www.ncdh.gov.cn/)

[东湖区](../Category/东湖区.md "wikilink") [区](../Category/南昌区县.md "wikilink")
[南昌](../Category/江西市辖区.md "wikilink")

1.  [我局提前做好区行政服务中心新大楼搬迁相关工作](http://www.ncdh.gov.cn/zh_cn/News.shtml?p5=5300501)
2.