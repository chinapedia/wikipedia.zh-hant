**Market Place by Jasons**（香港地區）或**Jasons Market
Place**（新加坡及台灣）是一家在香港、新加坡、北京及台灣的高端[連鎖](../Page/連鎖.md "wikilink")[超市](../Page/超級市場.md "wikilink")。該超市屬於[牛奶國際公司](../Page/牛奶國際.md "wikilink")，亦是[惠康超市](../Page/惠康.md "wikilink")、[Jasons
Food & Living及Jasons](../Page/Jasons_Food_&_Living.md "wikilink")
Ichiba的姐妹公司。\[1\]\[2\]

[Market_Place_Telford_Plaza_Store.jpg](https://zh.wikipedia.org/wiki/File:Market_Place_Telford_Plaza_Store.jpg "fig:Market_Place_Telford_Plaza_Store.jpg")分店\]\]
[The_Repulse_Bay_Market_Place_by_Jasons.JPG](https://zh.wikipedia.org/wiki/File:The_Repulse_Bay_Market_Place_by_Jasons.JPG "fig:The_Repulse_Bay_Market_Place_by_Jasons.JPG")[影灣園分店](../Page/影灣園.md "wikilink")\]\]
[Daily_Farm_The_Peak_Shopping_Centre_2016.jpg](https://zh.wikipedia.org/wiki/File:Daily_Farm_The_Peak_Shopping_Centre_2016.jpg "fig:Daily_Farm_The_Peak_Shopping_Centre_2016.jpg")[牛奶公司購物中心分店](../Page/牛奶公司購物中心.md "wikilink")\]\]
[K11_Market_Place_by_Jasons.jpg](https://zh.wikipedia.org/wiki/File:K11_Market_Place_by_Jasons.jpg "fig:K11_Market_Place_by_Jasons.jpg")分店\]\]
[BHG_Market_Place_Sanlitun_Village_Store.jpg](https://zh.wikipedia.org/wiki/File:BHG_Market_Place_Sanlitun_Village_Store.jpg "fig:BHG_Market_Place_Sanlitun_Village_Store.jpg")[北京](../Page/北京.md "wikilink")[三里屯Village分店](../Page/三里屯Village.md "wikilink")\]\]
[Market_Place_by_Jasons_in_Taipei_Q_square.JPG](https://zh.wikipedia.org/wiki/File:Market_Place_by_Jasons_in_Taipei_Q_square.JPG "fig:Market_Place_by_Jasons_in_Taipei_Q_square.JPG")[台北](../Page/台北.md "wikilink")[京站時尚廣場店](../Page/京站時尚廣場.md "wikilink")\]\]
[Market_Place_by_Jasons_in_Taipei_101_Mall.JPG](https://zh.wikipedia.org/wiki/File:Market_Place_by_Jasons_in_Taipei_101_Mall.JPG "fig:Market_Place_by_Jasons_in_Taipei_101_Mall.JPG")[台北101店](../Page/台北101.md "wikilink")\]\]
[Market_Place_by_Jasons_in_Kaohsiung_Dream_Mall_2015.JPG](https://zh.wikipedia.org/wiki/File:Market_Place_by_Jasons_in_Kaohsiung_Dream_Mall_2015.JPG "fig:Market_Place_by_Jasons_in_Kaohsiung_Dream_Mall_2015.JPG")[高雄市](../Page/高雄市.md "wikilink")[夢時代店](../Page/統一夢時代購物中心.md "wikilink")\]\]

## 向外擴展

早於1975年，牛奶公司在[新加坡以](../Page/新加坡.md "wikilink")**Jasons Market
Place**品牌開業。早期專售外地進口特產，針對當地外籍人士市場，至今已經有20多間分店。至2003年，Jason's
Market Place
向[台灣進發](../Page/台灣.md "wikilink")，先後在[台北](../Page/台北.md "wikilink")[台北101](../Page/台北101.md "wikilink")、[大葉高島屋](../Page/大葉高島屋.md "wikilink")、[林口三井Outlet](../Page/MITSUI_OUTLET_PARK_林口.md "wikilink")、[新竹](../Page/新竹市.md "wikilink")[巨城](../Page/Big_City遠東巨城購物中心.md "wikilink")、[豐原太平洋](../Page/太平洋百貨.md "wikilink")、[臺中大遠百](../Page/大遠百.md "wikilink")、屏東太平洋\[3\]等地開設分店。\[4\]\[5\]\[6\]至2007年，以**Market
Place by
Jasons**在[香港經營共有](../Page/香港.md "wikilink")20間分店，九龍灣分店更由著名日本設計師[新田昭三設計](../Page/新田昭三.md "wikilink")。

2017年5月，專售英國品牌[Sainsbury's](../Page/Sainsbury's.md "wikilink")200多款產品，包括經典英式餅乾、咖啡、果醬、100%果汁、急凍蔬菜，以至美味小食及健康穀物食品等。\[7\]

## 銷售與競爭

Market Place by
Jasons走中檔連鎖超級市場的路線，其主要對手有[Gourmet](../Page/Gourmet.md "wikilink")、[Taste](../Page/Taste.md "wikilink")、[fusion
by
PARKnSHOP及](../Page/fusion_by_PARKnSHOP.md "wikilink")[U購Select等](../Page/U購Select.md "wikilink")。

## [香港分店](../Page/香港.md "wikilink")

[香港共有](../Page/香港.md "wikilink")42間分店。首間分店於2007年6月在[渣甸山白建時道開幕](../Page/渣甸山.md "wikilink")。

[香港島](../Page/香港島.md "wikilink")

  - [渣甸山](../Page/渣甸山.md "wikilink")[白建時道](../Page/白建時道.md "wikilink")5號一樓（前[惠康超級市場](../Page/惠康.md "wikilink")）佔地10,000平方呎
  - [山頂道](../Page/山頂道.md "wikilink")100及104號[牛奶公司購物中心](../Page/牛奶公司購物中心.md "wikilink")（前[惠康超級市場](../Page/惠康.md "wikilink")）佔地9,000平方呎
  - [北角雲景道](../Page/北角.md "wikilink")33號牛奶公司購物中心一樓（前[惠康超級市場](../Page/惠康.md "wikilink")）
  - [中環](../Page/中環.md "wikilink")[盈置大廈](../Page/盈置大廈.md "wikilink")
    地庫（於2009年2月20日開幕）佔地8,000平方呎
  - [淺水灣](../Page/淺水灣.md "wikilink")[影灣園](../Page/影灣園.md "wikilink")
    （前[惠康超級市場](../Page/惠康.md "wikilink")）
  - [西營盤](../Page/西營盤.md "wikilink")[縉城峰](../Page/縉城峰.md "wikilink")，於2011年11月4日開幕
  - [北角](../Page/北角.md "wikilink")[模範里](../Page/模範里.md "wikilink")（前[惠康超級市場](../Page/惠康.md "wikilink")），於2012年2月開幕
  - [半山](../Page/半山.md "wikilink")[梅道](../Page/梅道.md "wikilink")12號嘉富麗園地庫（前[惠康超級市場](../Page/惠康.md "wikilink")），於2012年11月開幕
  - [淺水灣海灘道](../Page/淺水灣.md "wikilink")28號 The Pulse，於2014年8月29日開幕
  - [跑馬地黃泥涌道](../Page/跑馬地.md "wikilink")1號永光苑，於2014年9月5日開幕
  - [中環](../Page/中環.md "wikilink")[怡和大廈一樓](../Page/怡和大廈.md "wikilink")101-109及120號舖，於2014年12月19日開幕
  - [天后廟道](../Page/天后廟道.md "wikilink")4號（前[惠康超級市場](../Page/惠康.md "wikilink")）於2015年12月開幕，24小時營業
  - [半山](../Page/半山.md "wikilink")[西摩道](../Page/西摩道.md "wikilink")（前[惠康超級市場](../Page/惠康.md "wikilink")），於2016年8月26日開幕
  - [上環](../Page/上環.md "wikilink")[中央大廈](../Page/中央大廈.md "wikilink")（前[惠康超級市場](../Page/惠康.md "wikilink")），於2016年9月開幕
  - [赤柱](../Page/赤柱.md "wikilink")[馬坑邨](../Page/馬坑邨.md "wikilink")[美利樓](../Page/美利樓.md "wikilink")（臨時店，2017年5月-9月）

[九龍](../Page/九龍.md "wikilink")

  - [九龍灣](../Page/九龍灣.md "wikilink")[德福廣場](../Page/德福廣場.md "wikilink")（前[百佳超級廣場](../Page/百佳.md "wikilink"),於2007年8月17日開幕）佔地13,200平方呎
  - [旺角](../Page/旺角.md "wikilink")[朗豪坊](../Page/朗豪坊.md "wikilink")，於2007年12月7日開幕，佔地8,000平方呎
  - [尖沙嘴](../Page/尖沙嘴.md "wikilink")[柯士甸道](../Page/柯士甸道.md "wikilink")140至142號瑞信集團大廈，於2008年9月開幕，佔地3,202方呎
  - [美孚](../Page/美孚.md "wikilink")[曼克頓山商場曼坊](../Page/曼克頓山.md "wikilink")，於2009年4月17日開幕、2015年4月結業
  - [尖沙嘴](../Page/尖沙嘴.md "wikilink")[K11](../Page/K11_\(香港\).md "wikilink")，於2009年12月10日開幕，佔地18,000平方呎
  - [尖沙嘴](../Page/尖沙嘴.md "wikilink")[國際廣場](../Page/國際廣場.md "wikilink")，於2010年2月5日開幕
  - [黃大仙](../Page/黃大仙.md "wikilink")[現崇山商場二樓](../Page/現崇山.md "wikilink")，於2012年11月16日開幕，佔地約6000平方呎
  - [牛津道](../Page/牛津道.md "wikilink")5號（前[惠康超級市場](../Page/惠康.md "wikilink")），於2012年11月16日開幕
  - [尖沙嘴](../Page/尖沙嘴.md "wikilink")[漢口道](../Page/漢口道.md "wikilink")28號亞太中心G/F及1/F（前[南洋商業銀行](../Page/南洋商業銀行.md "wikilink")），佔地約6000平方呎，於2012年12月21日開幕
  - [西九龍](../Page/西九龍.md "wikilink")[御金·國峯](../Page/御金·國峯.md "wikilink")[中港薈UG層](../Page/御金·國峯.md "wikilink")，於2013年5月25日開幕
  - [奧海城](../Page/奧海城.md "wikilink")1期地下，佔地約4000平方呎，於2013年9月6日開幕
  - [尖沙嘴](../Page/尖沙嘴.md "wikilink")[中港城](../Page/中港城.md "wikilink")1樓離境層，於2013年9月13日開幕
  - [何文田](../Page/何文田.md "wikilink")[何文田廣場](../Page/何文田廣場.md "wikilink")3樓303號AB鋪（臨時店，2017年4月-7日）
  - [何文田](../Page/何文田.md "wikilink")[何文田廣場](../Page/何文田廣場.md "wikilink")3樓305號鋪，於2017年7月7日開幕
  - [奧海城](../Page/奧海城.md "wikilink")2期北翼中庭（臨時店，2017年7月-12月）
  - [奧海城](../Page/奧海城.md "wikilink")2期地下，於2017年12月開幕，佔地26,000平方呎

[新界](../Page/新界.md "wikilink")

  - [清水灣清水灣地段](../Page/清水灣.md "wikilink")253號牛奶公司購物商場地（前[惠康超級市場](../Page/惠康.md "wikilink")）
  - [屯門](../Page/屯門.md "wikilink")[香港黃金海岸購物商場](../Page/香港黃金海岸.md "wikilink")（前[惠康超級市場](../Page/惠康.md "wikilink")）
  - [火炭](../Page/火炭.md "wikilink")[御龍山商場](../Page/御龍山.md "wikilink")，於2010年7月2日開幕
  - [將軍澳](../Page/將軍澳.md "wikilink")[PopCorn
    2](../Page/PopCorn_2.md "wikilink") G51號舖，於2013年1月11日開幕
  - [荃灣](../Page/荃灣.md "wikilink")[荃新天地第二期](../Page/荃新天地.md "wikilink")，於2013年11月15日開幕
  - [落禾沙](../Page/落禾沙.md "wikilink")[迎海商場](../Page/迎海.md "wikilink")[迎海薈地下](../Page/迎海薈.md "wikilink")，於2014年5月30日開幕
  - [東涌海濱路](../Page/東涌.md "wikilink")12號[藍天海岸商場](../Page/藍天海岸.md "wikilink")（前[惠康超級市場](../Page/惠康.md "wikilink")）
  - [錦田映河路](../Page/錦田.md "wikilink")1號[爾巒商場](../Page/爾巒.md "wikilink")，佔地約8500平方呎，於2014年12月1日開幕
  - [元朗](../Page/元朗.md "wikilink")[溱柏商場](../Page/溱柏.md "wikilink")，於2015年3月13日開幕
  - [大埔](../Page/大埔.md "wikilink")[白石角](../Page/白石角.md "wikilink")[逸瓏灣商場](../Page/逸瓏灣.md "wikilink")，於2015年11月27日開幕
  - [東涌](../Page/東涌.md "wikilink")[昇薈商場](../Page/昇薈.md "wikilink")，於2016年7月15日開幕
  - [大埔](../Page/大埔.md "wikilink")[八號花園商場](../Page/八號花園.md "wikilink")，(前[惠康超級市場](../Page/惠康.md "wikilink")），於2016年7月22日開幕，24小時營業
  - [西貢](../Page/西貢.md "wikilink")[匡湖居購物商場](../Page/匡湖居.md "wikilink")，（前[惠康超級市場](../Page/惠康.md "wikilink")），於2016年12月16日開幕
  - [荃灣](../Page/荃灣.md "wikilink")[中國染廠大廈](../Page/中國染廠大廈.md "wikilink")[8咪半](../Page/8咪半.md "wikilink")，於2017年8月4日開幕
  - [東涌](../Page/東涌.md "wikilink")[富東邨](../Page/富東邨.md "wikilink")[富東廣場](../Page/富東廣場.md "wikilink")（前[惠康超級市場](../Page/惠康.md "wikilink")），於2017年11月10日開幕
  - [馬鞍山](../Page/馬鞍山.md "wikilink")[新港城中心](../Page/新港城中心.md "wikilink")，於2017年11月10日開幕

## [中國大陆分店](../Page/中國大陆.md "wikilink")

[中國大陆地区分店由](../Page/中國大陆.md "wikilink")[北京](../Page/北京.md "wikilink")[華聯集團擁有](../Page/華聯集團.md "wikilink")，旗下分店命名為BHG
Market Place。现在以下地点拥有分店：

[北京市](../Page/北京市.md "wikilink")

  - 五道口
  - 大望路北京SKP
  - [三里屯Village](../Page/三里屯Village.md "wikilink")
  - 蓝色港湾
  - 颐堤港

## [台灣分店](../Page/台灣.md "wikilink")

[台灣共有](../Page/台灣.md "wikilink")26間分店，旗下分店命名Jasons Market Place
時尚超市，分店如下。

  - [台北市](../Page/台北市.md "wikilink")：[台北](../Page/台北.md "wikilink")[京站店](../Page/京站時尚廣場.md "wikilink")（2009年12月開幕）、[台北101店](../Page/台北101.md "wikilink")（2003年11月開幕）、天母[大葉高島屋店](../Page/大葉高島屋.md "wikilink")（2004年9月開幕）、台北[美麗華店](../Page/美麗華百樂園.md "wikilink")、台北[寶慶店](../Page/遠東百貨.md "wikilink")、台北[天母店](../Page/天母.md "wikilink")、台北[政大店](../Page/政大.md "wikilink")（2015年11月6日開幕）、安和店、大安店、和平店、林森[欣欣百貨店](../Page/欣欣百貨.md "wikilink")
  - [新北市](../Page/新北市.md "wikilink")：[中和環球店](../Page/環球購物中心.md "wikilink")（2014年11月開幕）、林口三井店（2016年1月開幕）、板橋店（2016年9月開幕）、永和比漾店（2017年6月開幕）、三峽北大店(2017年7月28日開幕)
  - [桃園市](../Page/桃園市.md "wikilink")：桃園中茂新天地店（2015年12月30日開幕）
  - [新竹市](../Page/新竹市.md "wikilink")：新竹[大遠百店](../Page/大遠百.md "wikilink")（2007年1月開幕）
  - [新竹縣](../Page/新竹縣.md "wikilink")：竹北高鐵店（2018年9月開幕）
  - [台中市](../Page/台中市.md "wikilink")：台中[中友店](../Page/中友百貨.md "wikilink")（2007年9月開幕）、豐原店
  - [台南市](../Page/台南市.md "wikilink")：台南[夢時代店](../Page/南紡夢時代.md "wikilink")（2014年12月開幕）
  - [高雄市](../Page/高雄市.md "wikilink")：高雄[漢神店](../Page/漢神百貨.md "wikilink")（2005年12月開幕）、高雄[夢時代店](../Page/統一夢時代購物中心.md "wikilink")、高雄[漢神巨蛋店](../Page/漢神巨蛋.md "wikilink")（2015年4月開幕）、高雄大統店
  - [屏東市](../Page/屏東市.md "wikilink")：屏東太平洋百貨（2016年4月開幕）

## 貨品

  - [新鮮](../Page/新鮮.md "wikilink")[熟食](../Page/熟食.md "wikilink")（旺角朗豪坊除外）
  - [亞洲熟食](../Page/亞洲熟食.md "wikilink")（旺角朗豪坊除外）
  - [麵包](../Page/麵包.md "wikilink")（香港地區由[美心](../Page/美心.md "wikilink")[西餅主理](../Page/西餅.md "wikilink")）
  - [燒味](../Page/燒味.md "wikilink")（香港地區由[美心主理](../Page/美心.md "wikilink")；旺角朗豪坊除外）
  - [壽司](../Page/壽司.md "wikilink")（由東京築地[中島水產主理](../Page/中島水產.md "wikilink")）
  - [有機蔬果](../Page/有機蔬果.md "wikilink")
  - 自助散賣乾果
  - 全球各地進口雜貨

## 參考資料

## 外部連結

  - [Market Place Delivers
    網上品味超市](https://www.marketplacebyjasons.com/mpj2shop/zh/html/index.html?utm_source=Wikipedia&utm_medium=referral&utm_campaign=Wikipedia_MPJ&utm_term=Wikipedia&utm_content=HK-chinese)
      -
  - [JASONS Market Place（台灣）](http://www.jasons.com.tw/)

[Category:牛奶公司](../Category/牛奶公司.md "wikilink")
[Category:超級市場](../Category/超級市場.md "wikilink")
[Category:台灣超級市場](../Category/台灣超級市場.md "wikilink")
[Category:新加坡公司](../Category/新加坡公司.md "wikilink")
[Category:1975年成立的公司](../Category/1975年成立的公司.md "wikilink")

1.
2.
3.
4.
5.
6.
7.