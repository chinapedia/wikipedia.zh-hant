**萬錦**（；），又譯**麥咸**\[1\]或**麥城**，中文又音譯為**马卡姆**，是一個位於[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")[約克區的城市](../Page/約克區.md "wikilink")\[2\]。萬錦市2012年七月一日由「市镇」（Town）升格为「城市」（City），该市曾是加拿大人口最多的镇，位於[多倫多市以北](../Page/多倫多.md "wikilink")，為大多伦多地區的一部分。在2011年，萬錦的人口為301,709人\[3\]，是[大多倫多地區第四大城鎮](../Page/大多倫多地區.md "wikilink")，排在多倫多、[密西沙加及](../Page/密西沙加.md "wikilink")[賓頓市之後](../Page/賓頓.md "wikilink")。

多家高科技公司均將其加拿大分公司總部設於萬錦，因此萬錦常被喻為「加拿大的高科技首都」。

## 歷史

[德國裔移民](../Page/德國.md "wikilink")於1793年將現萬錦一帶劃為一個，而[上加拿大首任總督](../Page/上加拿大.md "wikilink")則將之名為「萬錦」以紀念時任[約克大主教的](../Page/約克大主教.md "wikilink")。柏茲再於1794年率領75個德裔家庭從[紐約上州遷居至萬錦](../Page/紐約上州.md "wikilink")，每個家庭並獲分配的土地；他們定居之處現為萬錦的區。然而，殖民者往後數年生活艱苦，因此部分遷居至（現多倫多市）和[尼亞加拉](../Page/尼亞加拉區.md "wikilink")，日耳曼妙斯也逐漸變成死城一個。部分原居於[賓夕法尼亞州的德國裔](../Page/賓夕法尼亞州.md "wikilink")[門諾會教徒則從](../Page/門諾會.md "wikilink")1803年至1812年間遷至萬錦；這次移民行動則較為成功，而他們聚居之處亦以其中一個移民若瑟·利沙（Joseph
Reesor）為名，稱為利沙村（Reesorville）。\[4\]到1825年，利沙村再以萬錦鄉之名，改稱萬錦村。到1830年，大批愛爾蘭裔、蘇格蘭裔和英裔家庭移居上加拿大，當中不少到萬錦定居。\[5\]

萬錦鄉内有多條溪流，居民也藉水流之利建立水動鋸木場和穀物磨坊。隨著[央街等要道陸續落成](../Page/央街.md "wikilink")，萬錦的人口繼續上升，該帶也開始都市化。萬錦鄉於1850年正式設立地方行政架構。\[6\]到1857年，萬錦鄉的大部分土地已被開墾，而[湯山](../Page/湯山_\(安大略\).md "wikilink")、[於人村和萬錦村等村落也大幅擴展](../Page/於人村.md "wikilink")。\[7\][多倫多與尼披盛鐵路於](../Page/:en:Toronto_and_Nipissing_Railway.md "wikilink")1871年開通至萬錦，刺激了當地的經濟（現時其走綫由[GO通勤鐵路使用](../Page/GO運輸.md "wikilink")）。

隨著多倫多於二戰後迅速發展，[城市延伸現象令包括萬錦在内的外圍郊區也進一步都市化](../Page/城市延伸.md "wikilink")。萬錦鄉的大部分範圍亦於1971年改設萬錦鎮。\[8\]南通多倫多的於1970年代開通後，萬錦的人口增長更為顯著。現時萬錦的農地已買少見少，但仍可見於[麥更西少校大道](../Page/麥更西少校大道.md "wikilink")（）以北的地帶。

## 地理及氣候

萬錦的總面積為212.47[平方公里](../Page/平方公里.md "wikilink")，而其市中心位於43°53′N 79°15′W。

### 邊界

萬錦的鄰市如下：

  - 西面：[旺市](../Page/旺市.md "wikilink")（以[央街為界](../Page/央街.md "wikilink")）、[列治文山](../Page/列治文山.md "wikilink")（以[7號公路和](../Page/安大略7號省道.md "wikilink")[404號公路為界](../Page/:en:Ontario_Highway_404.md "wikilink")）
  - 南面：[多倫多](../Page/多倫多.md "wikilink")（以[士刁士大道為界](../Page/士刁士大道.md "wikilink")）
  - 北面：[懷特教堂-斯提夫維爾](../Page/:en:Whitchurch-Stouffville.md "wikilink")（界綫位於19街和[斯提夫維爾路之間](../Page/:en:York_Regional_Road_14.md "wikilink")）
  - 東面：[碧谷靈](../Page/:en:Pickering,_Ontario.md "wikilink")（以[約克－杜林線為界](../Page/:en:York_Regional_Road_30.md "wikilink")）

### 地形

萬錦的平均高度為海拔200米。鎮內有兩條河流，分別為[當河](../Page/:en:Don_River_\(Ontario\).md "wikilink")（Don
River）和[紅河](../Page/:en:Rouge_River_\(Ontario\).md "wikilink")（Rouge
River）；兩者的部分支流也流經萬錦。萬錦市大部分都是平地，但位於萬錦北部的[橡樹山冰磧則比鎮内大部分地區為高](../Page/:en:Oak_Ridges_Moraine.md "wikilink")。

### 氣候

因為萬錦鎮在[多倫多市的附近](../Page/多倫多.md "wikilink")，萬錦鎮的氣候跟多倫多的氣候一模一樣，只是日常天氣有些微差別。

## 人口

根據2006年人口普查得出的結果為：\[9\]

|               |                                        |
| ------------- | -------------------------------------- |
| 人口            | 261,573                                |
| 人口自2001年的變動   | \+25.4 %                               |
| 人口密度          | 每平方公里1,230.5人                          |
| 全國的人口排名       | 第16名                                   |
| 年齡中位數         | 38.1 歲 （男：37歲；女：38.9歲）                 |
| 私人物業總數        | 81,181                                 |
| 固定居民所住的私人物業總數 | 77,191                                 |
| 家庭收入中位數       | [加幣](../Page/加幣.md "wikilink") $77,163 |

### 移民

按移民分類 (2006)：

  - 加拿大出生者：42.6%
  - 外國出生者：56.5%
  - 非固定人口：0.9%

### 種族人口分佈

[缩略图](https://zh.wikipedia.org/wiki/File:BankofChinaMarkham.JPG "fig:缩略图")

<table>
<thead>
<tr class="header">
<th><p>加拿大2011年的人口普查</p></th>
<th><p>人口</p></th>
<th><p>占總人口的比例（%）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/族群.md" title="wikilink">族群</a><br />
<small>來源: <a href="http://www12.statcan.gc.ca/nhs-enm/2011/dp-pd/prof/details/page.cfm?Lang=E&amp;Geo1=CSD&amp;Code1=3519036&amp;Data=Count&amp;SearchText=Markham&amp;SearchType=Begins&amp;SearchPR=01&amp;A1=All&amp;B1=All&amp;GeoLevel=PR&amp;GeoCode=3519036&amp;TABID=1">NHS 2011 Profile</a></p></td>
<td><p><a href="../Page/高加索人种.md" title="wikilink">高加索人</a></p></td>
<td><p>82,560</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/華裔加拿大人.md" title="wikilink">华裔</a></p></td>
<td><p>114,950</p></td>
<td><p>38.3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南亞裔.md" title="wikilink">南亞裔</a></p></td>
<td><p>57,375</p></td>
<td><p>19.1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑人.md" title="wikilink">黑人</a></p></td>
<td><p>9,715</p></td>
<td><p>3.2</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/菲律賓人.md" title="wikilink">菲律賓人</a></p></td>
<td><p>9,020</p></td>
<td><p>3.0</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西亞.md" title="wikilink">西亞人</a></p></td>
<td><p>6,185</p></td>
<td><p>2.1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿拉伯.md" title="wikilink">阿拉伯人</a></p></td>
<td><p>3,400</p></td>
<td><p>1.1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韓裔加拿大人.md" title="wikilink">韓國裔</a></p></td>
<td><p>3,160</p></td>
<td><p>1.0</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東南亞.md" title="wikilink">東南亞人</a></p></td>
<td><p>2,750</p></td>
<td><p>0.9</p></td>
</tr>
<tr class="even">
<td><p>其他<a href="../Page/可见少数族裔.md" title="wikilink">可见少数族裔</a></p></td>
<td><p>1,995</p></td>
<td><p>0.7</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/拉丁美洲.md" title="wikilink">拉丁美洲人</a></p></td>
<td><p>1,600</p></td>
<td><p>0.5</p></td>
</tr>
<tr class="even">
<td><p>多元<a href="../Page/可见少数族裔.md" title="wikilink">可见少数族裔</a></p></td>
<td><p>5,805</p></td>
<td><p>1.9</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/加拿大原住民.md" title="wikilink">原住民</a></p></td>
<td><p>485</p></td>
<td><p>0.2</p></td>
</tr>
<tr class="even">
<td><p><strong><em>總人口</em></strong></p></td>
<td><p><strong><em>300,140</em></strong></p></td>
<td><p><strong><em>100</em></strong></p></td>
</tr>
</tbody>
</table>

少數族裔構成72.5%萬錦市人口，成爲加拿大最大少數族裔構成的城市。

### 語言

2011年人口普查的[母語回答](../Page/母語.md "wikilink")\[10\]：

  - [英語](../Page/英語.md "wikilink")：38.5%
  - [法語](../Page/法語.md "wikilink")：0.7%
  - 非官方語言：57.2%
      - [廣州話](../Page/廣州話.md "wikilink")：16.4%
      - [中文](../Page/中文.md "wikilink")（沒指定）：10.8%
      - [泰米爾語](../Page/泰米爾語.md "wikilink")：5.3%
      - [官話](../Page/官話.md "wikilink")：4.9%
      - [烏爾都語](../Page/烏爾都語.md "wikilink")：2.4%
  - 多數回答：3.7%

### 宗教信仰

按宗教信仰分類 (2011)：

  - [基督教](../Page/基督教.md "wikilink")：44.1%
  - [印度教](../Page/印度教.md "wikilink")：10.1%
  - [回教](../Page/回教.md "wikilink")：7.3%
  - [佛教](../Page/佛教.md "wikilink")：4.4%
  - [猶太教](../Page/猶太教.md "wikilink")：2.4%
  - [錫克教](../Page/錫克教.md "wikilink")：1.4%
  - 其他宗教信仰：\>0.4%
  - [無宗教](../Page/無宗教信仰.md "wikilink")：29.9%

## 經濟

[AMDmarkham5.jpg](https://zh.wikipedia.org/wiki/File:AMDmarkham5.jpg "fig:AMDmarkham5.jpg")

### 高科技行業

[华为](../Page/华为.md "wikilink")、[IBM](../Page/IBM.md "wikilink")、[摩托羅拉](../Page/摩托羅拉.md "wikilink")、[東芝](../Page/東芝.md "wikilink")、[阿爾卡特-朗訊](../Page/阿爾卡特-朗訊.md "wikilink")、[昇陽](../Page/昇陽.md "wikilink")、[蘋果電腦](../Page/蘋果電腦.md "wikilink")、[美國運通及](../Page/美國運通.md "wikilink")[AMD的加拿大分公司總部均位於萬錦](../Page/AMD.md "wikilink")，因此萬錦常被喻為加拿大的高科技首都。

### 零售行業

由於不少華人聚居於此，因此萬錦市亦有數個華人商場：

  - [太古廣場](../Page/太古廣場_\(大多倫多\).md "wikilink")（）
  - [城市廣場](../Page/城市廣場_\(大多倫多\).md "wikilink")（）
  - [萬錦廣場](../Page/萬錦廣場.md "wikilink")（）
  - [大都會廣場](../Page/大都會廣場.md "wikilink")，又名小台北。（）
  - [旺角廣場](../Page/旺角廣場.md "wikilink")（）
  - [新旺角廣場](../Page/新旺角廣場.md "wikilink")（）

其他的商場包括

  - [萬維廣場](../Page/萬維廣場.md "wikilink")（Markville Mall）
  - [士刁士夾404商場](http://www.theshops.ca/)（The Shops on Steeles and 404）
  - [康山廣場](../Page/康山廣場.md "wikilink")（Thornhill Square）

市內還有一些正在興建的商場，例如[朗豪坊](../Page/朗豪坊_\(加拿大\).md "wikilink")（South
Unionville Square）。

## 交通

### 公共交通

[WardenVIVA6.JPG](https://zh.wikipedia.org/wiki/File:WardenVIVA6.JPG "fig:WardenVIVA6.JPG")
[萬錦公車局從](../Page/:en:Markham_Transit.md "wikilink")1973年至2001年間為萬錦提供公車服務。該機構於2001年與列治文山、紐馬克及旺市的公車局合併成[約克區公車局](../Page/:en:York_Region_Transit.md "wikilink")（YRT），為整個約克區提供公車服務。YRT旗下的[活力巴士](../Page/:en:Viva_\(bus_rapid_transit\).md "wikilink")（Viva）亦駛經萬錦，當中[粉紅綫和](../Page/:en:Viva_Pink.md "wikilink")[綠綫分別接駁](../Page/:en:Viva_Green.md "wikilink")[多倫多地鐵的](../Page/多倫多地鐵.md "wikilink")[芬治站和](../Page/芬治站.md "wikilink")[當妙斯站](../Page/當妙斯站.md "wikilink")。

[多倫多公車局](../Page/多倫多公車局.md "wikilink")（TTC）的部分巴士綫亦跨越市界連接多倫多和萬錦，如沃顿路（Warden
Avenue）、貝治芒路（Birchmount Road）、麥高雲路（McCowan Road）及萬錦路（Markham
Road）等路綫，但跨區乘客須付雙程車費。

[GO運輸公司提供由萬錦前往多倫多的通勤鐵路服務](../Page/GO運輸.md "wikilink")，萬錦以内的車站為[於人村](../Page/:en:Unionville_GO_Station.md "wikilink")、[森田尼爾](../Page/:en:Centennial_GO_Station.md "wikilink")、[萬錦和](../Page/:en:Markham_GO_Station.md "wikilink")[喜樂山](../Page/:en:Mount_Joy_GO_Station.md "wikilink")。

### 道路

通過萬錦的公路[安省404公路和](../Page/安省404公路.md "wikilink")[安省407公路](../Page/安省407公路.md "wikilink")。前者呈南北向連接多倫多和紐馬克；後者則是呈東西向的收費公路，繞過多倫多北部連接萬錦、旺市、賓頓及布靈頓。

安省407公路和約克7號區道平行。約克7號區道是一條東西貫通的通道，由於發展商不斷在公路两旁發展，令此通道經常塞車。其他東西貫通的道路有約克73號區道（16街）、約克25區道（麥更西少校大道）及士刁士大道等。

### 空中服務

多倫多巴頓維爾機場是加拿大十大機場之一，提供前往[渥太華及](../Page/渥太華.md "wikilink")[蒙特利爾的服務](../Page/蒙特利爾.md "wikilink")。
服務公司有：

  - NexJet Aviation
  - Million Air
  - Executive Edge Air Charter
  - Aviation Limited
  - Canadian Flyers International

## 教育

萬錦市擁有四個公立教育制度:[約克郡教育局](../Page/約克郡教育局.md "wikilink")（York Region
District School
Board）經營世俗英語學校、[約克天主教教育局](../Page/約克天主教教育局.md "wikilink")（York
Catholic District School Board）經營天主教英語學校、Conseil scolaire Viamonde
經營世俗法語學校、及Conseil scolaire de district catholique Centre-Sud
經營天主教法語學校。

  - [約克郡教育局](../Page/約克郡教育局.md "wikilink")（York Region District School
    Board）
      - [比爾克羅瑟斯中學](../Page/比爾克羅瑟斯中學.md "wikilink")（Bill Crothers
        Secondary School）
      - [大果櫟中學](../Page/大果櫟中學.md "wikilink")（Bur Oak Secondary School）
      - [萬錦區中學](../Page/萬錦區中學.md "wikilink")（Markham District High
        School）
      - [馬克維爾高級中學](../Page/馬克維爾高級中學.md "wikilink")（Markville Secondary
        School）
      - [米德爾中學](../Page/米德爾中學.md "wikilink")（Middlefield Collegiate
        Institute）
      - [米利肯米爾斯中學](../Page/米利肯米爾斯中學.md "wikilink")（Milliken Mills High
        School）
      - [皮埃爾·埃利奧特·特魯多中學](../Page/皮埃爾·埃利奧特·特魯多中學.md "wikilink")（Pierre
        Elliott Trudeau High School）
      - [桑希爾中學](../Page/桑希爾中學.md "wikilink")（Thornhill Secondary School）
      - [桑利中學](../Page/桑利中學.md "wikilink")（Thornlea Secondary School）
      - [於人村中學](../Page/於人村中學.md "wikilink")（Unionville High School）
  - [約克天主教教育局](../Page/約克天主教教育局.md "wikilink")（York Catholic District
    School Board）
      - [聖安德烈修士天主教中學](../Page/聖安德烈修士天主教中學.md "wikilink")（St. Brother
        André Catholic High School）
      - [聖奧古斯丁天主教中學](../Page/聖奧古斯丁天主教中學.md "wikilink")（St. Augustine
        Catholic High School）
      - [聖羅伯特天主教中學](../Page/聖羅伯特天主教中學.md "wikilink")（St. Robert Catholic
        High School）
      - [麥吉富尼神父天主教學院](../Page/麥吉富尼神父天主教學院.md "wikilink")（Father Michael
        McGivney Catholic Academy）

## 知名人士

  - [張雪芹](../Page/張雪芹.md "wikilink")
  - [海登·克里斯滕森](../Page/海登·克里斯滕森.md "wikilink")

## 友好城市

  - [臺灣省](../Page/臺灣省.md "wikilink")[新竹](../Page/新竹縣.md "wikilink")

  - [廣東省](../Page/廣東省.md "wikilink")[佛山](../Page/佛山.md "wikilink")

  - [山東省](../Page/山東省.md "wikilink")[淄博](../Page/淄博.md "wikilink")

  - [湖北省](../Page/湖北省.md "wikilink")[武汉](../Page/武汉.md "wikilink")

## 姐妹城市

  - [北卡羅萊納州](../Page/北卡羅萊納州.md "wikilink")[卡里](../Page/卡瑞_\(北卡羅萊納州\).md "wikilink")

  - [得克薩斯州](../Page/得克薩斯州.md "wikilink")[珍珠蘭](../Page/:en:Pearland,_Texas.md "wikilink")

  - [諾凌根](../Page/:en:Nördlingen.md "wikilink")

  - [廣東省](../Page/廣東省.md "wikilink")[花都](../Page/花都.md "wikilink")

  - [拉斯皮納斯市](../Page/拉斯皮納斯市.md "wikilink")

  - [廣東省](../Page/廣東省.md "wikilink")[江门](../Page/江门.md "wikilink")\[11\]

## 註腳

<references />

## 外部連結

  - [萬錦市官方網站](http://www.markham.ca/) (英文)

[Category:約克區](../Category/約克區.md "wikilink")
[Category:安大略省城市](../Category/安大略省城市.md "wikilink")
[Category:1794年建立](../Category/1794年建立.md "wikilink")

1.

2.

3.
4.  [Markham Village - A Brief
    History 1800-1919](http://www.markham.ca/mpl/subjguides/HotTopics/Heritage_Week.asp)
    , Markham Public Library (website).

5.  For a complete history of Markham's early years, cf. Isabel
    Champion, ed.,
    [Markham: 1793-1900](http://www.ourroots.ca/e/toc.aspx?id=12398)
    (Markham, ON: Markham Historical Society, 1979).

6.  Cf. C.P. Mulvany, et al, [The Township of
    Markham](http://www.archive.org/stream/historyoftoronto01mulvuoft#page/114/mode/2up),
    *History of Toronto and County of York, Ontario* (Toronto: C.B.
    Robinson, 1885), 114ff.

7.  Cf. the detailed 1878 map, [Township of
    Markham](http://digital.library.mcgill.ca/countyatlas/images/maps/townshipmaps/yor-m-markham.jpg),
    *Illustrated historical atlas of the county of York and the township
    of West Gwillimbury & town of Bradford in the county of Simcoe,
    Ont.* (Toronto : Miles & Co., 1878).

8.  [Town of Markham / Markham Public Library: Integrated Leisure Master
    Plan](http://www.markham.ca/NR/rdonlyres/1FA7404A-7DA7-4438-896E-082DC838EB45/0/leisureplan_final041310.pdf)
    , p.1

9.

10.

11.