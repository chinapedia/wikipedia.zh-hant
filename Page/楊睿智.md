**楊睿智**（），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink")，但目前已遭到釋出。

## 經歷

  - [台北市東園國小少棒隊](../Page/台北市.md "wikilink")
  - [台北市大理國中青少棒隊](../Page/台北市.md "wikilink")
  - [臺北縣穀保家商青棒隊](../Page/新北市.md "wikilink")
  - [合作金庫棒球隊](../Page/合作金庫.md "wikilink")
  - 國軍棒球隊
  - [台灣大聯盟](../Page/台灣大聯盟.md "wikilink")[台中金剛隊](../Page/台中金剛.md "wikilink")（2002年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰太陽隊](../Page/誠泰太陽.md "wikilink")（2003年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰Cobras隊](../Page/誠泰Cobras.md "wikilink")（2004年－2007年）

## 職棒生涯成績

|       |                                            |     |      |     |     |    |     |     |    |     |     |       |
| ----- | ------------------------------------------ | --- | ---- | --- | --- | -- | --- | --- | -- | --- | --- | ----- |
| 年度    | 球隊                                         | 出賽  | 打數   | 安打  | 全壘打 | 打點 | 四死  | 三振  | 盜壘 | 壘打數 | 雙殺打 | 打擊率   |
| 2003年 | [誠泰太陽](../Page/誠泰太陽.md "wikilink")         | 31  | 36   | 4   | 0   | 1  | 0   | 7   | 3  | 4   | 1   | 0.111 |
| 2004年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 96  | 359  | 99  | 0   | 19 | 10  | 42  | 42 | 127 | 3   | 0.261 |
| 2005年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 96  | 318  | 83  | 0   | 27 | 4   | 29  | 35 | 103 | 4   | 0.261 |
| 2006年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 80  | 266  | 70  | 0   | 17 | 6   | 26  | 30 | 77  | 5   | 0.263 |
| 2007年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 90  | 231  | 74  | 0   | 19 | 7   | 19  | 29 | 85  | 3   | 0.320 |
| 合計    | 5年                                         | 393 | 1210 | 330 | 47  | 0  | 143 | 119 | 27 | 396 | 16  | 0.272 |

## 特殊事蹟

## 外部連結

[R](../Category/楊姓.md "wikilink") [Y](../Category/在世人物.md "wikilink")
[Y](../Category/1980年出生.md "wikilink")
[Y](../Category/台灣棒球選手.md "wikilink")
[Y](../Category/台中金剛隊球員.md "wikilink")
[Y](../Category/誠泰COBRAS隊球員.md "wikilink")