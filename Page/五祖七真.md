**五祖七真**是道教[全真教對教內五位](../Page/全真教.md "wikilink")[祖師和七位](../Page/祖師.md "wikilink")[真人的合稱](../Page/真人.md "wikilink")。原為[北五祖與](../Page/北五祖.md "wikilink")[北七真的合稱](../Page/北七真.md "wikilink")。指[王玄甫](../Page/王玄甫.md "wikilink")、[钟离权](../Page/钟离权.md "wikilink")、[吕洞宾](../Page/吕洞宾.md "wikilink")、[刘海蟾](../Page/刘海蟾.md "wikilink")、[王重阳五祖師](../Page/王重阳.md "wikilink")，加上[馬丹陽](../Page/馬丹陽.md "wikilink")、[譚處端](../Page/譚處端.md "wikilink")、[劉處玄](../Page/劉處玄.md "wikilink")、[丘處機](../Page/丘處機.md "wikilink")、[王處一](../Page/王處一.md "wikilink")、[郝大通](../Page/郝大通.md "wikilink")、[孫不二七位](../Page/孫不二.md "wikilink")[真人](../Page/真人.md "wikilink")。

隨著[元代中後期流傳南方](../Page/元代.md "wikilink")[金丹派的與北方的](../Page/金丹派.md "wikilink")[全真教結合](../Page/全真教.md "wikilink")，原來的[全真教被奉為](../Page/全真教.md "wikilink")「[北宗](../Page/北宗.md "wikilink")」，而南方的金丹派則被奉為[全真教的](../Page/全真教.md "wikilink")「[南宗](../Page/南宗.md "wikilink")」，“五祖七真”亦衍生出新意涵。原本[全真教的](../Page/全真教.md "wikilink")**五祖七真**被稱為「[北五祖](../Page/北五祖.md "wikilink")」和「[北七真](../Page/北七真.md "wikilink")」，而南方內丹派的[張伯端](../Page/張伯端.md "wikilink")、[石泰](../Page/石泰.md "wikilink")、[薛式](../Page/薛式.md "wikilink")、[陳楠及](../Page/陳楠.md "wikilink")[白玉蟾被奉為](../Page/白玉蟾.md "wikilink")「[南五祖](../Page/南五祖.md "wikilink")」，若果再加上張伯端的弟子[劉永年和白玉蟾的弟子](../Page/劉永年.md "wikilink")[彭鶴林](../Page/彭鶴林.md "wikilink")，則合稱「[南七真](../Page/南七真.md "wikilink")」。

## 北五祖

[全真教尊奉北宗五位祖師](../Page/全真教.md "wikilink")[王玄甫](../Page/王玄甫.md "wikilink")、[钟离权](../Page/钟离权.md "wikilink")、[吕洞宾](../Page/吕洞宾.md "wikilink")、[刘海蟾](../Page/刘海蟾.md "wikilink")、[王重阳为](../Page/王重阳.md "wikilink")[全真五祖](../Page/全真五祖.md "wikilink")，为与[南五祖区分](../Page/南五祖.md "wikilink")，故称「**北五祖**」或「**五阳祖師**」。。

## 北七真

**北七真**是[中國](../Page/中國.md "wikilink")[道教重要派別](../Page/道教.md "wikilink")[全真道的開山祖師](../Page/全真道.md "wikilink")[王重陽的七個弟子](../Page/王重陽.md "wikilink")：[馬鈺](../Page/馬鈺.md "wikilink")、[譚處端](../Page/譚處端.md "wikilink")、[劉處玄](../Page/劉處玄.md "wikilink")、[丘處機](../Page/丘處機.md "wikilink")、[王處一](../Page/王處一.md "wikilink")、[郝大通](../Page/郝大通.md "wikilink")、[孫不二](../Page/孫不二.md "wikilink")。因其對全真道的傳播和發展有著重大的貢獻，因此獲稱[真人](../Page/真人.md "wikilink")。

## 南五祖

**張伯端**宣稱於[神宗皇帝](../Page/神宗.md "wikilink")[熙寧二年](../Page/熙寧.md "wikilink")（1069年）在[成都遇到](../Page/成都.md "wikilink")[廣陽真人](../Page/廣陽真人.md "wikilink")[劉海蟾](../Page/劉海蟾.md "wikilink")，海蟾並授予張伯端「金液還丹訣」，因此修煉得道。張將此訣傳給石泰，石泰又傳給薛式、薛式傳陳楠、陳楠傳白玉蟾。

  - [張伯端](../Page/張伯端.md "wikilink")
  - [石泰](../Page/石泰.md "wikilink")
  - [薛式](../Page/薛式.md "wikilink")
  - [陈楠](../Page/陈楠.md "wikilink")
  - [白玉蟾](../Page/白玉蟾.md "wikilink")

## 南七真

“南五祖”再加上張伯端的弟子[劉永年和白玉蟾的弟子](../Page/劉永年.md "wikilink")[彭鶴林](../Page/彭鶴林.md "wikilink")，則被奉為“南七真”。

  - [張伯端](../Page/張伯端.md "wikilink")
      - [劉永年](../Page/劉永年.md "wikilink")
  - [石泰](../Page/石泰.md "wikilink")
  - [薛式](../Page/薛式.md "wikilink")
  - [陈楠](../Page/陈楠.md "wikilink")
  - [白玉蟾](../Page/白玉蟾.md "wikilink")
      - [彭鶴林](../Page/彭鶴林.md "wikilink")

## 参考文献

## 外部链接

  - [道教學術資訊站](https://web.archive.org/web/20100728134443/http://www.ctcwri.idv.tw/)

{{-}}

[Category:道教人物并称](../Category/道教人物并称.md "wikilink")
[Category:全真道](../Category/全真道.md "wikilink")