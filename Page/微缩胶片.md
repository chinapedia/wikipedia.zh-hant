[Microfiche.jpg](https://zh.wikipedia.org/wiki/File:Microfiche.jpg "fig:Microfiche.jpg")
[Positive_roll_film.jpg](https://zh.wikipedia.org/wiki/File:Positive_roll_film.jpg "fig:Positive_roll_film.jpg")
**微縮膠片**，是[數碼相機時代之前的當代科技產物](../Page/數碼相機.md "wikilink")，人類利用[膠卷](../Page/膠卷.md "wikilink")[攝影技術](../Page/攝影.md "wikilink")，[複製](../Page/複製.md "wikilink")[书籍](../Page/书籍.md "wikilink")、[報紙](../Page/報紙.md "wikilink")、[雜誌等](../Page/雜誌.md "wikilink")[出版物上的](../Page/出版物.md "wikilink")[文字和](../Page/文字.md "wikilink")[圖片之类](../Page/圖片.md "wikilink")，汇集制作为一个小胶片。该胶片有16-[mm和](../Page/公釐.md "wikilink")35-mm两种型号。
因为以當時的[科技極限](../Page/科技.md "wikilink")，微缩胶片比原物可以保存較長的[时间](../Page/时间.md "wikilink")，便于查阅，方便分类。此种方法大量的应用于以前的[图书馆](../Page/图书馆.md "wikilink")、档案馆等机构中(事實上現在還有應用)。

## 材料与规格

微缩胶片通常是用[聚酯制成](../Page/聚酯.md "wikilink")，也有的用[乙酰代替](../Page/乙酰.md "wikilink")。

16-mm 的微缩胶片的微缩[标准](../Page/标准.md "wikilink")：1:20, 1:24, 1:32, 1:40,
1:42, 1:48, 1:96

35-mm 的微缩胶片的微缩标准：1:7.5, 1:10.5, 1:14.8, 1:21, 1:29.7

## 保存

微缩胶片在[温度](../Page/温度.md "wikilink")21
[°C](../Page/°C.md "wikilink")，[湿度](../Page/湿度.md "wikilink")50
%下可以至少保存500[年](../Page/年.md "wikilink")。

## 取代品

隨著現代[數碼技術的發展](../Page/數碼.md "wikilink")，[膠卷和微缩胶片逐漸被淘汰](../Page/膠卷.md "wikilink")，因為[閱讀微缩胶片](../Page/閱讀.md "wikilink")，必須借用[參考圖書館內的特製](../Page/參考圖書館.md "wikilink")[儀器](../Page/儀器.md "wikilink")
－
櫃式膠卷機。它在[購買](../Page/購買.md "wikilink")、維修、貯藏上的[成本很貴](../Page/成本.md "wikilink")。現今科技中，取代微縮膠片的產品是[數碼攝影](../Page/數碼攝影.md "wikilink")、[PDF等](../Page/PDF.md "wikilink")。

但[新加坡政府為了防止已電子化的檔案因](../Page/新加坡.md "wikilink")[儲存裝置及](../Page/儲存裝置.md "wikilink")[檔案格式迅速變遷](../Page/檔案格式.md "wikilink")，而無法被未來的電腦所讀取及處理，除了電子化以外仍繼續使用微縮膠片保存重要的政府檔案\[1\]。

## 參考資料

[Category:圖書資訊科學](../Category/圖書資訊科學.md "wikilink")
[Category:加拿大发明](../Category/加拿大发明.md "wikilink")

1.