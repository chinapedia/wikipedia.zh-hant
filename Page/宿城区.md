**宿城区**是[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[宿迁市所辖的](../Page/宿迁市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，是宿迁市主城区，地处[淮海经济区中部](../Page/淮海经济区.md "wikilink")，属陇海经济带、沿海经济带、沿江经济带交叉辐射区。[京杭大运河](../Page/京杭大运河.md "wikilink")、[古黄河穿越城区](../Page/古黄河.md "wikilink")，境内有[骆马湖](../Page/骆马湖.md "wikilink")、[洪泽湖](../Page/洪泽湖.md "wikilink")。目前擔任宿城区委书记的是裴承前\[1\]。

## 历史

[东晋时置](../Page/东晋.md "wikilink")[宿预县](../Page/宿预县.md "wikilink")，[县城遗址在今宿城区](../Page/宿预城遗址.md "wikilink")[郑楼镇古城山南麓](../Page/郑楼镇.md "wikilink")\[2\]。

原为地级宿迁市市委市政府所在地宿城镇，1996年宿迁单列为地级市后，更名为宿城区并下设有井头乡、双庄镇；2004年3月在新一波的行政区划中，宿城区将原属于[泗阳的](../Page/泗阳.md "wikilink")[郑楼](../Page/郑楼.md "wikilink")、[屠园](../Page/屠园.md "wikilink")、[洋河镇](../Page/洋河镇.md "wikilink")、[仓集](../Page/仓集.md "wikilink")、[中扬五乡镇及原属泗洪县的](../Page/中扬.md "wikilink")[陈集镇以及其周边原属宿豫县的部分乡镇划归旗下](../Page/陈集镇.md "wikilink")，使之一跃成为一个拥有854平方公里土地，89万人口的大区。

## 行政区划

2004年3月区划调整，辖14个乡镇、4个街道和1个省级经济开发区。

  - 镇：[双庄镇](../Page/双庄镇.md "wikilink")、[耿车镇](../Page/耿车镇.md "wikilink")、[埠子镇](../Page/埠子镇.md "wikilink")、[龙河镇](../Page/龙河镇.md "wikilink")、[洋北镇](../Page/洋北镇.md "wikilink")、[仓集镇](../Page/仓集镇.md "wikilink")、[中扬镇](../Page/中扬镇.md "wikilink")、[郑楼镇](../Page/郑楼镇.md "wikilink")、[陈集镇](../Page/陈集镇.md "wikilink")。
  - 乡：[罗圩乡](../Page/罗圩乡.md "wikilink")、[南蔡乡](../Page/南蔡乡.md "wikilink")、[屠园乡](../Page/屠园乡.md "wikilink")、[三棵树乡](../Page/三棵树乡.md "wikilink")。
  - 街道：[幸福街道](../Page/幸福街道.md "wikilink")、[项里街道](../Page/项里街道.md "wikilink")、[河滨街道](../Page/河滨街道.md "wikilink")、[古城街道](../Page/古城街道.md "wikilink")、[黄河街道](../Page/黄河街道.md "wikilink")、[古楚街道](../Page/古楚街道.md "wikilink")。

## 名勝古跡

项王故里、极乐律院、嶂山森林公园、骆马湖。

## 名人

[項羽](../Page/项羽.md "wikilink")、[魏胜](../Page/魏胜.md "wikilink")。

## 参考文献

[宿迁](../Category/江苏市辖区.md "wikilink")
[区](../Category/宿迁区县.md "wikilink")
[宿城区](../Category/宿城区.md "wikilink")

1.
2.