[Farinelli_engraving.jpg](https://zh.wikipedia.org/wiki/File:Farinelli_engraving.jpg "fig:Farinelli_engraving.jpg")
**法里内利**（）（），原名**卡洛尔·玛利亚·布罗斯基**（Carlo Maria
Broschi），為[十八世紀](../Page/十八世紀.md "wikilink")[義大利著名](../Page/義大利.md "wikilink")[阉伶](../Page/阉伶.md "wikilink")。

生長於[那不勒斯](../Page/那不勒斯.md "wikilink")，唱假[女高音](../Page/女高音.md "wikilink")，號稱「女神的顫音」。
先從[尼古拉·波尔波拉學唱](../Page/尼古拉·波尔波拉.md "wikilink")，後拜[Bernachi為師](../Page/Bernachi.md "wikilink")。
1733年在[倫敦演唱](../Page/倫敦.md "wikilink")，被譽為「一個上帝、一個法里内利」（One God, One
Farnelli），其生平事跡被拍成電影《[魅影魔聲](../Page/魅影魔聲.md "wikilink")》（Farinelli
，臺灣譯《[絕代艷姬](../Page/絕代艷姬.md "wikilink")》），
以片中原聲唱碟並電子合成的方式以男女藝術歌唱家融合模擬出這種現代人難以唱出的獨特歌聲。

## 早年

生于意大利南部[安德里亞音乐世家](../Page/安德里亞.md "wikilink")。区别于很多因贫寒而沦为阉伶的歌唱家，法里内利的家世算得上优越，父母双方都跟贵族家庭有关系。他的母亲是那不勒斯人。当时那不勒斯最显赫的贵族[安德里亞公爵曾参加他的受洗](../Page/安德里亞.md "wikilink")。他的父亲是作曲家。法里内利两岁时，父亲任[马拉泰亚城长官](../Page/马拉泰亚.md "wikilink")，之后迁任[泰尔利齐长官](../Page/泰尔利齐.md "wikilink")。

1712年，法里内利的哥哥[里卡多·布罗斯基进入圣雷多圣母堂音乐学院](../Page/里卡多·布罗斯基.md "wikilink")（[那不勒斯音乐学院前身之一](../Page/那不勒斯音乐学院.md "wikilink")）学习作曲。作为童声就天赋难掩的法里内利被引荐给那不勒斯最著名的声乐老师,巴洛克作曲家[尼古拉·波尔波拉](../Page/尼古拉·波尔波拉.md "wikilink")。波尔波拉的弟子包括众多著名阉伶（[Felice
Salimbeni](../Page/Felice_Salimbeni.md "wikilink"), [Gaetano
Majorano](../Page/Caffarelli_\(castrato\).md "wikilink")）和女歌唱家(Regina
Mingotti, [Vittoria Tesi](../Page/Vittoria_Tesi.md "wikilink"))。

1717年，法里内利的父亲年仅36岁突然离世。也许是因为失去了家庭经济来源的保障，法里内利被阉割，以保存完美的嗓音。此决定通常被认为是由他的哥哥所作（通俗演义中甚至被描绘为他的哥哥亲手执行）。即使在当时，手术也需要一个掩人耳目的理由——此处归咎为一次坠马事故。

## 崭露头角

在波普拉门下，法里内利进步神速。十五岁时他初演由波尔波拉为著名歌剧作家/诗人[Pietro
Metastasio所作](../Page/Pietro_Metastasio.md "wikilink")“[安杰丽卡与梅多罗](../Page/Angelica_e_Medoro_\(Porpora\).md "wikilink")”谱曲的[小夜曲](../Page/小夜曲.md "wikilink")。此作也是Metastasio的第一个作品。俩人成为挚友，更因同时出道而互称孪生兄弟。

## 參考資料

  - 李英，*自然唱法*（圣弗朗西斯科 1978）, pp 10

[Category:意大利男歌手](../Category/意大利男歌手.md "wikilink")
[Category:那不勒斯人](../Category/那不勒斯人.md "wikilink")