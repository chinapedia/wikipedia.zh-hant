**3Com**公司是一個全球的企業和小型企業聯網解決方案供應商，提供[網絡交換機](../Page/網絡交換機.md "wikilink")，[路由器](../Page/路由器.md "wikilink")，[無線接取器](../Page/無線接取器.md "wikilink")，IP語音系統和[入侵預防系統等產品](../Page/入侵預防系統.md "wikilink")。3Com由[羅伯特·梅特卡夫博士於](../Page/羅伯特·梅特卡夫.md "wikilink")1979年創立。目前公司總部位於[美國](../Page/美國.md "wikilink")[麻薩諸塞州](../Page/麻薩諸塞州.md "wikilink")[馬爾堡](../Page/馬爾堡.md "wikilink")。3Com名稱取自[電腦](../Page/電腦.md "wikilink")（）、[通訊](../Page/通訊.md "wikilink")（）與[相容性](../Page/相容性.md "wikilink")（）。

[3Com_3c905-tx_NIC.jpg](https://zh.wikipedia.org/wiki/File:3Com_3c905-tx_NIC.jpg "fig:3Com_3c905-tx_NIC.jpg")（[PCI介面](../Page/PCI.md "wikilink")）\]\]

## 公司簡介

2007年，3Com收購先前中國合資事業H3C Technologies Co.,
Limited（H3C）的百分之百股權。但公司的骨干都來自於華為，而被收購前的3Com，有高达95%的利润来自H3C\[1\]。H3C的創立原意為對抗[思科](../Page/思科.md "wikilink")，現屬於[惠普](../Page/惠普.md "wikilink")，反而成為[華為在全球数据市场的對手](../Page/華為.md "wikilink")。

3Com年營收13億美元（2008年3月30日為止的2008會計年度），在超過40個國家擁有約6,000員工。

該公司報告它擁有超過2,700名工程師，有超过1400项美國专利以及将近180项中國專利。他们有1050多项正在申请中的中国专利，在中国之外还有35个独立创新正在申请专利，内容涵蓋範圍廣泛的網絡技術。\[2\]

## 公司歷史

在[施樂公司](../Page/施樂公司.md "wikilink")[帕洛阿爾托研究中心](../Page/帕洛阿爾托研究中心.md "wikilink")，羅伯特·梅特卡夫發明了以太網，並隨後於1979年共同創辦了3Com公司。3Com公司開始製作[以太網適配卡](../Page/以太網適配卡.md "wikilink")80年代初的許多計算機系統，包括LSI-
11、IBM 個人電腦以及VAX -
11。在80年代中期，3Com公司將他們的以太網技術，品牌為EtherSeries，同時引入一系列軟件和個人電腦為基礎的設備共享服務提供較局域網使用XNS協議。這些協議是品牌[EtherShare](../Page/EtherShare.md "wikilink")（用於文件共享）、[EtherPrint](../Page/EtherPrint.md "wikilink")（打印）、[EtherMail](../Page/EtherMail.md "wikilink")（用於電子郵件
）以及以太3270（為IBM的主機仿真）。

3Com公司的網絡軟件產品包括：

  - 3+Share file and printer sharing.
  - 3+Mail e-mail.
  - 3+Remote for routing XNS over a PC serial port.
  - NetConnect for routing XNS between Ethernets.
  - (MultiConnect?) was a chassis-based multi-port 10Base2 Ethernet
    repeater.
  - 3Server, a server-grade PC for running 3+ services.
  - 3Station, a diskless workstation.
  - 3+Open file and printer sharing (based on Microsoft's LAN Manager).
  - Etherterm terminal emulation.
  - Etherprobe LAN analysis software.
  - DynamicAccess software products for Ethernet load balancing,
    response time, and RMON II distributed monitoring.

在1987年開始合併時3Com公司擴張其原有的精簡電腦和以太網產品[橋接器](../Page/橋接器.md "wikilink")。
在[摩托羅拉](../Page/摩托羅拉.md "wikilink")
[68000處理器和使用](../Page/68000處理器.md "wikilink")[XNS協議的基礎上提供了一系列的設備](../Page/XNS.md "wikilink")，與3Com的Etherterm
PC軟件。

  - CS/1, CS/200 communication servers ("terminal servers")
  - Ethernet bridges and XNS routers
  - GS/1-X.25 X.25 gateway
  - CS/1-SNA SNA gateway
  - NCS/1 network control software running on a Sun2.

1997年，3Com公司合併U.S. Robotics和Palm, Inc.

1998年8月，Bruce Claflin被任命為3Com首席運營官 。

1999年，3Com公司收購位於波士頓的NBX公司，為小型和中小型企業提供基於以太網的電話系統。

2000年3月，因為與思科系統激烈的競爭，3Com公司退出了高端路由器業務。
在伺服器用網絡卡業務方面，在計算市場份額後，3Com公司排名第二，緊接英特爾之後
。

2001年1月，Bruce Claflin代替Eric Benhamou成為首席執行官。

2003年5月，3Com公司總部從[矽谷](../Page/矽谷.md "wikilink")[聖克拉拉遷至馬薩諸塞州莫爾伯勒](../Page/聖克拉拉.md "wikilink")。

2009年11月11日，美國惠普科技公司宣佈，將以總價約27億美元的價格[現金](../Page/現金.md "wikilink")[收購](../Page/收購.md "wikilink")3Com公司。

2010年4月3日，惠普完成收购3Com。

## 參考資料

## 外部連結

  - [裕益科技 YU-YI
    Technology](http://www.yuyiplc.com/index.php?unit=products&lang=cht&act=list&parent=0&id=47)

[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")
[Category:網絡硬件公司](../Category/網絡硬件公司.md "wikilink")
[Category:惠普](../Category/惠普.md "wikilink")

1.  [以太网巨人3Com退出历史
    H3C借船惠普出击全球](http://news.mydrivers.com/1/162/162402.htm)
2.  [3Com Reports Results for First Quarter
    Fiscal 2009](http://phx.corporate-ir.net/phoenix.zhtml?c=61382&p=irol-newsArticle&ID=1199528&highlight=)