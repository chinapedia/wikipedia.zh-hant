**国际資訊奥林匹亞**（**I**nternational **O**lympiad in
**I**nformatics，**IOI**），是面向中学生的一年一度的[資訊学科竞赛](../Page/信息学.md "wikilink")。第一届國際資訊奧林匹亞于1989年在[保加利亚的](../Page/保加利亚.md "wikilink")[布拉维茨举行](../Page/普拉韋茨.md "wikilink")。

這項競賽包含兩天的電腦程式設計，主要以[c++為主](../Page/c++.md "wikilink")，解决[演算法问题](../Page/算法.md "wikilink")。選手以個人為單位，每個國家最多可選派4名選手參加（2016年共有83个国家参赛）。参赛选手从各国相应计算机竞赛中选拔。

## 竞赛规则

[IOI_2006_competition_room.png](https://zh.wikipedia.org/wiki/File:IOI_2006_competition_room.png "fig:IOI_2006_competition_room.png")

在两天的比赛日中，一般来说参赛选手每一天都需要在5小时内一台[计算机的帮助下独立解决](../Page/计算机.md "wikilink")3道题目。比赛中不允许任何形式的交流或是查阅参考资料。通常，参赛选手都要为每一道题目编写一个[程序](../Page/程序.md "wikilink")（编程语言为[C](../Page/C.md "wikilink")、[C++或是](../Page/C++.md "wikilink")[Pascal](../Page/Pascal.md "wikilink")）并在比赛结束之前提交。随后大赛的主办方将采用10组或者20组测试数据对程序进行测试。如果根据给定的某一组输入数据，程序可以在限定的[内存空间和时间内正确的得到输出结果](../Page/内存.md "wikilink")，那么该名选手即获得这一组数据的相应分数。\[1\]近年出现了一种新的比赛题型（提交答案题）——在比赛开始之间参赛选手将获得各组测试数据，他们要提交的是相应的计算结果，而非程序本身。选手可以通过编写程序、手工计算或者两者结合来完成这类题目。

每名参赛选手的各题得分之和即为总得分。在颁奖仪式上，参赛选手将根据他们的得分获得相应的奖项。排名前50%的参赛选手（平均每个国家2名）将获得奖牌。获得金、银、铜牌和未获奖牌人数之比约为1:2:3:6。

与其他学科的奥林匹克竞赛不同，國際信息奧林匹亞章程严格禁止对参赛国家的排名\[2\]但是每场比赛之后仍然会有不少非官方的排名出现。

[IOI_Zagreb_1.JPG](https://zh.wikipedia.org/wiki/File:IOI_Zagreb_1.JPG "fig:IOI_Zagreb_1.JPG")

## 历届比赛地点和网站链接

  - **IOI 2020**即將於2020年在[新加坡舉行](../Page/新加坡.md "wikilink")。

  - **IOI
    2019**即將於2019年在[亞塞拜然的](../Page/亞塞拜然.md "wikilink")[巴庫舉行](../Page/巴庫.md "wikilink")。

  - **IOI
    2018**已於2018年9月1日—9月8日在[日本的](../Page/日本.md "wikilink")[筑波舉行](../Page/筑波.md "wikilink")。[網站](http://ioi2018.jp/)（[比賽結果](http://stats.ioinformatics.org/results/2018))

  - **IOI
    2017**已於2017年7月28日—8月4日在[伊朗的](../Page/伊朗.md "wikilink")[德黑蘭舉行](../Page/德黑蘭.md "wikilink")。[網站](http://ioi2017.org/)([比賽結果](http://stats.ioinformatics.org/results/2017))

  - **IOI
    2016**已於2016年8月12日—8月19日在[俄羅斯的](../Page/俄羅斯.md "wikilink")[喀山舉行](../Page/喀山.md "wikilink")。[网站](https://web.archive.org/web/20160211185940/http://ioi2016.ru/)
    ([比賽結果](http://stats.ioinformatics.org/results/2016))

  - **IOI
    2015**已於2015年7月26日—8月2日在[哈薩克斯坦的](../Page/哈薩克斯坦.md "wikilink")[阿拉木圖舉行](../Page/阿拉木圖.md "wikilink")。[网站](http://ioi2015.kz/)
    ([比賽結果](http://stats.ioinformatics.org/results/2015))

  - **IOI
    2014**已於2014年7月13日—7月20日在[臺灣的](../Page/臺灣.md "wikilink")[臺北市舉行](../Page/臺北市.md "wikilink")。[网站](https://web.archive.org/web/20121001155553/http://ioi2014.org/)([比賽結果](http://stats.ioinformatics.org/results/2014))

  - **IOI
    2013**已於2013年7月6日—7月13日在[澳大利亞的](../Page/澳大利亞.md "wikilink")[布里斯班舉行](../Page/布里斯班.md "wikilink")。[网站](http://ioi2013.org/)（[比赛结果](http://www.ioi2013.org/competition/results/)）

  - **IOI
    2012**已於2012年9月23日—9月30日在[意大利的](../Page/意大利.md "wikilink")[米兰举行](../Page/米兰.md "wikilink")。[网站](http://www.ioi2012.org/)（[比赛结果](http://www.ioi2012.org/competition/results-2/)）

  - **IOI
    2011**已於2011年7月22日—7月29日在[泰国的](../Page/泰国.md "wikilink")[芭达亚举行](../Page/芭达亚.md "wikilink")。[网站](https://web.archive.org/web/20100904044842/https://www.ioi2011.or.th/)（[比赛结果](http://www.ioi2011.or.th/results)）

  - **IOI
    2010**已於2010年8月14日—8月21日在[加拿大的](../Page/加拿大.md "wikilink")[滑铁卢举行](../Page/滑铁卢_\(安大略省\).md "wikilink")。[网站](http://www.ioi2010.org/)（[比赛结果](http://www.ioi2010.org/FinalResults.shtml)）

  - **IOI
    2009**已於2009年8月8日—8月14日在[保加利亚的](../Page/保加利亚.md "wikilink")[普罗夫迪夫举行](../Page/普罗夫迪夫.md "wikilink")。[网站](https://web.archive.org/web/20100304221049/http://www.ioi2009.org/)（[比赛结果](http://www.ioi2009.org/index.jsp?id=364)）

  - **IOI
    2008**已於2008年8月16日—8月23日在[埃及的](../Page/埃及.md "wikilink")[開羅举行](../Page/開羅.md "wikilink")。[网站](http://www.ioi2008.org/)([比赛结果](http://stats.ioinformatics.org/results/2008))

  - **IOI
    2007**已於2007年8月15日—8月22日在[克羅埃西亞的](../Page/克羅埃西亞.md "wikilink")[札格拉布举行](../Page/札格拉布.md "wikilink")。[网站](http://www.hsin.hr/ioi2007/)（[比赛结果](http://stats.ioinformatics.org/results/2007)）

  - **IOI
    2006**已於2006年8月13日—8月20日在[墨西哥的](../Page/墨西哥.md "wikilink")[梅里达举行](../Page/梅里达.md "wikilink")。([比赛结果](http://olympiads.win.tue.nl/ioi/ioi2006/contest/results.html))

  - **IOI
    2005**已於2005年8月17日—8月25日在[波兰的](../Page/波兰.md "wikilink")[新松奇举行](../Page/新松奇.md "wikilink")。[网站](https://web.archive.org/web/20041012073141/http://www.ioi2005.pl/)（[比赛结果](http://stats.ioinformatics.org/results/2005)）

  - **IOI
    2004**已於2004年9月11日—9月18日在[希腊的](../Page/希腊.md "wikilink")[雅典举行](../Page/雅典.md "wikilink")。[网站](http://www.ioi2004.org/)（[比赛结果](http://ioinformatics.org/locations/ioi04/contest/results.pdf)）

  - **IOI
    2003**已於2003年8月16日—8月23日在[美国的](../Page/美国.md "wikilink")[威斯康星举行](../Page/威斯康星.md "wikilink")。
    [网站](https://web.archive.org/web/20070504211414/http://www.ioinformatics.org/ioi2003/)（[比赛结果](http://olympiads.win.tue.nl/ioi/ioi2003/medals.html)）

  - **IOI
    2002**已於2002年8月18日—8月25日在[韩国](../Page/韩国.md "wikilink")[京畿道的](../Page/京畿道.md "wikilink")[龍仁举行](../Page/龍仁市.md "wikilink")。[网站](http://www.ioi2002.or.kr/)([比赛结果](http://stats.ioinformatics.org/results/2002))

  - **IOI
    2001**已於2001年7月14日—7月21日在[芬兰的](../Page/芬兰.md "wikilink")[坦佩雷举行](../Page/坦佩雷.md "wikilink")。[网站](https://web.archive.org/web/20080821104254/http://www.ioi2001.edu.fi/)([比赛结果](http://stats.ioinformatics.org/results/2001))

  - **IOI
    2000**已於2000年9月23日—9月30日在[中国的](../Page/中国.md "wikilink")[北京举行](../Page/北京.md "wikilink")。[网站](https://web.archive.org/web/20111101015833/http://www.ioi2000.org.cn/)([比赛结果](http://stats.ioinformatics.org/results/2000))

  - **IOI
    1999**已於1999年9月9日—9月16日在[土耳其的](../Page/土耳其.md "wikilink")[安塔利亚举行](../Page/安塔利亚.md "wikilink")。[网站](https://web.archive.org/web/20010516022837/http://www.ioi99.org.tr/)([比赛结果](http://stats.ioinformatics.org/results/1999))

  - **IOI
    1998**已於1998年9月5日—9月12日在[葡萄牙的](../Page/葡萄牙.md "wikilink")[塞图巴尔举行](../Page/塞图巴尔.md "wikilink")。([比赛结果](http://stats.ioinformatics.org/results/1998))

  - **IOI
    1997**已於1997年11月30日—12月7日在[南非的](../Page/南非.md "wikilink")[開普敦举行](../Page/開普敦.md "wikilink")。([比赛结果](http://stats.ioinformatics.org/results/1997))

  - **IOI
    1996**已於1996年7月25日—8月2日在[匈牙利的](../Page/匈牙利.md "wikilink")[维斯普雷姆举行](../Page/维斯普雷姆.md "wikilink")。[网站](http://olympiads.win.tue.nl/ioi/ioi96/)([比赛结果](http://stats.ioinformatics.org/results/1996))

  - **IOI
    1995**已於1995年6月26日—7月3日在[荷兰的](../Page/荷兰.md "wikilink")[埃因霍温举行](../Page/埃因霍温.md "wikilink")。[网站](http://olympiads.win.tue.nl/ioi95/)([比赛结果](http://stats.ioinformatics.org/results/1995))

  - **IOI
    1994**已於1994年7月3日—7月10日在[瑞典的](../Page/瑞典.md "wikilink")[哈寧厄市举行](../Page/哈寧厄市.md "wikilink")。([比赛结果](http://stats.ioinformatics.org/results/1994))

  - **IOI
    1993**已於1993年10月16日—10月25日在[阿根廷的](../Page/阿根廷.md "wikilink")[门多萨举行](../Page/门多萨.md "wikilink")。[网站](https://web.archive.org/web/20040105002728/http://www.ioi.org.ar/mendoza/)([比赛结果](http://stats.ioinformatics.org/results/1993))

  - **IOI
    1992**已於1992年7月11日—7月21日在[德国的](../Page/德国.md "wikilink")[波恩举行](../Page/波恩.md "wikilink")。([比赛结果](http://stats.ioinformatics.org/results/1992))

  - **IOI
    1991**已於1991年5月19日—5月25日在[希腊的](../Page/希腊.md "wikilink")[雅典举行](../Page/雅典.md "wikilink")。([比赛结果](http://stats.ioinformatics.org/results/1991))

  - **IOI
    1990**已於1990年7月15日—7月21日在[前苏联](../Page/前苏联.md "wikilink")[白俄羅斯的](../Page/白俄羅斯.md "wikilink")[明斯克举行](../Page/明斯克.md "wikilink")。([比赛结果](http://stats.ioinformatics.org/results/1990))

  - **IOI
    1989**已於1989年5月16日—5月19日在[保加利亚的](../Page/保加利亚.md "wikilink")[普拉韋茨举行](../Page/普拉韋茨.md "wikilink")。([比赛结果](http://stats.ioinformatics.org/results/1989))

## 歷屆得獎者

這是一份歷屆最優秀得獎者的名單。\*號代表滿分，這在国际信息学奥林匹克中是十分罕見的。另外，金獎中的第一名，第二名和第三名也有標示。
這份名單只包含那些容許選拔後的國家隊成員多次參與国际信息学奥林匹克的國家。

<center>

<table class="wikitable">

<tr>

<th>

名字

</th>

<th>

國家

</th>

<th colspan=7 align=center>

年份

</th>

</tr>

<tr>

<td>

  

</td>

<td>

 白俄羅斯 

</td>

<td>

 **金（第二名）** 2012 

</td>

<td>

 **金\*（第一名）** 2011 

</td>

<td>

 **金（第一名）** 2010 

</td>

<td>

 **金（第一名）** 2009 

</td>

<td>

 **金** 2008 

</td>

<td>

 **金** 2007  

</td>

<td>

 **銀** 2006  

</td>

</tr>

<tr>

<td>

 Filip Wolski 

</td>

<td>

 波蘭 

</td>

<td>

 **金（第一名）** 2006 

</td>

<td>

 **金** 2005 

</td>

<td>

 **金** 2004 

</td>

<td>

 **金** 2003

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Rumen Hristov 

</td>

<td>

 保加利亞 

</td>

<td>

 **金** 2012 

</td>

<td>

 **金** 2011 

</td>

<td>

 **金(第二名)** 2010  

</td>

<td>

 **銀** 2009  

</td>

<td>

 **銀** 2008  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Martin Pettai 

</td>

<td>

 愛沙尼亞 

</td>

<td>

 **金** 2002 

</td>

<td>

 **金** 2001 

</td>

<td>

 **金** 2000 

</td>

<td>

 **銀** 1999  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Andrzej Gąsienica-Samek 

</td>

<td>

 波蘭 

</td>

<td>

 **金** 1999 

</td>

<td>

 **金** 1998 

</td>

<td>

 **金** 1997 

</td>

<td>

 **銀** 1996  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Владимир Мартинов 

</td>

<td>

 俄羅斯 

</td>

<td>

 **金** 1999 

</td>

<td>

 **金\*（第一名）** 1998 

</td>

<td>

 **金（第一名）** 1997  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Martin Mareš 

</td>

<td>

 捷克 

</td>

<td>

 **金** 1995 

</td>

<td>

 **金** 1994  

</td>

<td>

 **金** 1993  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 John Pardon 

</td>

<td>

 美國 

</td>

<td>

 **金** 2007 

</td>

<td>

 **金** 2006  

</td>

<td>

 **金** 2005  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Marcin Andrychowicz 

</td>

<td>

 波蘭 

</td>

<td>

 **金** 2008 

</td>

<td>

 **金** 2007  

</td>

<td>

 **金** 2006  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Neal Wu 

</td>

<td>

 美國 

</td>

<td>

 **金** 2010 

</td>

<td>

 **金** 2009  

</td>

<td>

 **金** 2008  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Alex Schwendner 

</td>

<td>

 美國 

</td>

<td>

 **金** 2005 

</td>

<td>

 **金** 2003  

</td>

<td>

 **銀** 2004  

</td>

<td>

 **銀** 2002  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Wolfgang Thaller 

</td>

<td>

 奧地利 

</td>

<td>

 **金** 1997 

</td>

<td>

 **金** 1996 

</td>

<td>

 **銀** 1999 

</td>

<td>

 **銀** 1998  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Bruce Merry 

</td>

<td>

 南非 

</td>

<td>

 **金** 2001 

</td>

<td>

 **金** 2000  

</td>

<td>

 **銀** 1999  

</td>

<td>

 **銅** 1998  

</td>

<td>

 **銅** 1997  

</td>

<td>

 **銅** 1996  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Goran Žužić 

</td>

<td>

 克羅埃西亞 

</td>

<td>

 **金** 2008 

</td>

<td>

 **金** 2007  

</td>

<td>

 **銀** 2009 

</td>

<td>

 **銅** 2006 

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Виктор Баргачев 

</td>

<td>

 俄羅斯 

</td>

<td>

 **金（第一名）** 1995 

</td>

<td>

 **金（第一名）** 1994 

</td>

<td>

 **銀** 1993  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

  

</td>

<td>

 羅馬尼亞 

</td>

<td>

 **金（第二名）** 2001 

</td>

<td>

 **金** 2000 

</td>

<td>

 **銀** 1999  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Роман Пастоков 

</td>

<td>

 俄羅斯 

</td>

<td>

 **金** 2000 

</td>

<td>

 **金（第二名）** 1999 

</td>

<td>

 **銀** 2001  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Piotr Zieliński 

</td>

<td>

 波蘭 

</td>

<td>

 **金** 1997 

</td>

<td>

 **金（第三名）** 1996 

</td>

<td>

 **銀** 1995  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Miroslav Dudík 

</td>

<td>

 斯洛伐克 

</td>

<td>

 **金** 1997 

</td>

<td>

 **金** 1996  

</td>

<td>

 **銀** 1995  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Richard Královič 

</td>

<td>

 斯洛伐克 

</td>

<td>

 **金** 1999 

</td>

<td>

 **金** 1998  

</td>

<td>

 **銀** 1997  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Tomasz Czajka 

</td>

<td>

 波蘭（1998, 2000）， 英國（1999） 

</td>

<td>

 **金** 2000 

</td>

<td>

 **金** 1999  

</td>

<td>

 **銀** 1998  

</td>

<td>

   

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

  

</td>

<td>

 俄羅斯 

</td>

<td>

 **金** 2002 

</td>

<td>

 **金** 2000 

</td>

<td>

 **銀** 2001  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Luka Kalinovčić 

</td>

<td>

 克羅埃西亞 

</td>

<td>

 **金** 2004 

</td>

<td>

 **金** 2003  

</td>

<td>

 **銀** 2002  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Rostislav Rumenov 

</td>

<td>

 保加利亞 

</td>

<td>

 **金** 2007 

</td>

<td>

 **金** 2006  

</td>

<td>

 **銀** 2005  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Владислав Епифанов 

</td>

<td>

 俄羅斯 

</td>

<td>

 **金** 2008 

</td>

<td>

 **金** 2007  

</td>

<td>

 **銀** 2009  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Cosmin Gheorghe 

</td>

<td>

 羅馬尼亞 

</td>

<td>

 **金** 2009 

</td>

<td>

 **金** 2008  

</td>

<td>

 **銀** 2007  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Pasin Manurangsi 

</td>

<td>

 泰國 

</td>

<td>

 **金** 2011 

</td>

<td>

 **金** 2010  

</td>

<td>

 **銀** 2009  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Wenyu Cao 

</td>

<td>

 美國 

</td>

<td>

 **金** 2011 

</td>

<td>

 **金** 2010  

</td>

<td>

 **銀** 2009  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Tzvetomir Petrov 

</td>

<td>

 保加利亞 

</td>

<td>

 **金（第一名）** 1990 

</td>

<td>

 **金** 1993

</td>

<td>

 **銅** 1991 

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 David Arthur 

</td>

<td>

 加拿大 

</td>

<td>

 **金（第二名）** 2000 

</td>

<td>

 **金** 1999

</td>

<td>

 **銅** 1998 

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Janis Sermulins 

</td>

<td>

 拉脫維亞 

</td>

<td>

 **金** 1999 

</td>

<td>

 **金（第二名）** 1997

</td>

<td>

 **銅** 1998 

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Teodor Tonchev 

</td>

<td>

 保加利亞 

</td>

<td>

 **金（第一名）** 1989 

</td>

<td>

 **金（第三名）** 1990  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 村井翔悟 

</td>

<td>

 日本 

</td>

<td>

 **金** 2010 

</td>

<td>

 **金**2012 

</td>

<td>

 **金** 2011 

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 陈宏 

</td>

<td>

 中國 

</td>

<td>

 **金（第二名）** 2000 

</td>

<td>

 **金（第一名）** 1999  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 胡伟栋 

</td>

<td>

 中國 

</td>

<td>

 **金（第一名）** 2005 

</td>

<td>

 **金（第二名）** 2004  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Johnny Ho 

</td>

<td>

 美國 

</td>

<td>

 **金\*（第一名）** 2012 

</td>

<td>

 **金** 2011  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

  

</td>

<td>

 美國 

</td>

<td>

 **金（第一名）** 2001 

</td>

<td>

 **金** 2000  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 保坂和宏 

</td>

<td>

 日本 

</td>

<td>

 **金（第二名）** 2009 

</td>

<td>

 **金** 2008  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

<tr>

<td>

 Velin Tzanov 

</td>

<td>

 保加利亞 

</td>

<td>

 **金（第三名）** 2002 

</td>

<td>

 **金** 2001  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

<td>

  

</td>

</tr>

</table>

</center>

## 参考文献

## 外部链接

  - [APIO-亞太資訊奧林匹亞](https://web.archive.org/web/20071123114822/http://www.apio.olympiad.org/)
  - [NOI Chief Technology Officer - Jerry.
    Lee](https://web.archive.org/web/20070216014006/http://www.iultra.cn/)
  - [IOI 2014](https://web.archive.org/web/20121001155553/http://ioi2014.org/)
  - [IOI 2013](http://www.ioi2013.org/)
  - [IOI 2006](http://www.ioi2006.org/)
  - [IOI 2007](http://ioi2007.hsin.hr/)
  - [IOI 2008](http://www.ioi2008.org/)
  - [ACM International Collegiate Programming
    Contest](http://icpc.baylor.edu/icpc/)
  - [國際資訊奧林匹亞（IOI）競賽簡介](http://www.nioerar.edu.tw/basis3/16/gj17.htm)
    ([何榮桂](../Page/何榮桂.md "wikilink")，[國立臺灣師範大學資訊教育系所教授](../Page/國立臺灣師範大學.md "wikilink"))
  - [TOI-臺灣資訊奧林匹亞研習營](http://toi.csie.ntnu.edu.tw)

## 参见

  - [全国青少年信息学奥林匹克竞赛](../Page/全国青少年信息学奥林匹克竞赛.md "wikilink")（中国）
  - [ACM国际大学生程序设计竞赛](../Page/ACM国际大学生程序设计竞赛.md "wikilink")
  - [美國電腦奧林匹亞競賽](../Page/美國電腦奧林匹亞競賽.md "wikilink")（美国）
  - [国际科学奥林匹克](../Page/国际科学奥林匹克.md "wikilink")

{{-}}

[Category:信息学奥林匹克竞赛](../Category/信息学奥林匹克竞赛.md "wikilink")

1.  <http://www.ioinformatics.org/rules/reg08.pdf> IOI2008
    Reculations，2008年10月18日查阅
2.  <http://olympiads.win.tue.nl/ioi/rules/index.html> IOI
    Regulations，2008年10月18日查阅。