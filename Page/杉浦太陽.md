**杉浦
太陽**（），[日本男演員](../Page/日本.md "wikilink")，[大阪府](../Page/大阪府.md "wikilink")[寢屋川市人](../Page/寢屋川市.md "wikilink")，出生於[岡山縣](../Page/岡山縣.md "wikilink")[玉野市](../Page/玉野市.md "wikilink")。是前[早安少女組成員](../Page/早安少女組.md "wikilink")[辻希美的丈夫](../Page/辻希美.md "wikilink")。现为[SKY
CORPORATION艺能事务所旗下艺人](../Page/SKY_CORPORATION.md "wikilink")。

## 簡歷

  - 1998年在[朝日電視系連續劇](../Page/朝日電視.md "wikilink")《[戀愛Y世代](../Page/戀愛Y世代.md "wikilink")》（）中首次登台。

  - 2001年主演[每日放送系連續劇](../Page/每日放送.md "wikilink")《[超人力霸王高斯](../Page/超人力霸王高斯.md "wikilink")》（）。

  - 在《超人高斯》播放時，原本是以與本名發音相同的「杉浦太陽」（Sugiura Takayasu）之名進行活動。

  - 2002年6月14日，仍在《超人高斯》播放的期間，因為被懷疑在2000年10月17日時對弟弟的友人進行恐嚇與傷害的行為，而被大阪府警方以涉嫌人身份逮捕。雖然事後[大阪地方檢查廳以證據不足為由不予](../Page/大阪地方檢查廳.md "wikilink")[起訴](../Page/起訴.md "wikilink")，但已對杉浦在影集中的演出造成影響。根據辯方（杉浦方）辯護[律師提供的陳述書](../Page/律師.md "wikilink")，聲明事件起因於弟弟的友人盜取杉浦家的錢財，在要求對方歸還錢財的過程中發生肢體衝突，除了傷勢輕微之外，事件發生時當事雙方皆未滿成年。電視影集的製作單位在杉浦被逮捕後立刻停播了該片，且在恢復播映後將後續五集內容中杉浦出現的片段全部剪除，相關片段僅保留在之後上映的電影版中放映\[1\]\[2\]。

  - 2002年，受被捕事件的影響，將藝名改為與本名發音不同的「杉浦太陽」（Sugiura
    Taiyou），並與同樣也是演員的親生弟弟[杉浦太雄合組音樂團體](../Page/杉浦太雄.md "wikilink")「Ex.Bold」，但該團體已於2005年之後停止活動。

  - 2007年6月21日，與前[早安少女組。成員](../Page/早安少女組。.md "wikilink")[辻希美結婚](../Page/辻希美.md "wikilink")。

  - ，長女杉浦希空（）誕生。

  - ，長男杉浦青空（）誕生。

  - ，次男杉浦昊空（）誕生。

## 演出

### 電視劇

  - [晨間小說連續劇系列](../Page/晨間小說連續劇.md "wikilink")《[鬼太郎之妻](../Page/鬼太郎之妻.md "wikilink")》（2010年5月24日
    - 9月、NHK）- 飾演 浦木克夫

## 外部連結

  - [杉浦太陽官方網站](http://taiyosugiura.com/)

  -
## 参考及註釋

<references/>

[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:超人力霸王系列主演演員](../Category/超人力霸王系列主演演員.md "wikilink")

1.
2.