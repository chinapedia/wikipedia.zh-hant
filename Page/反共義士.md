[Loberty_Day_Commemorative_0.40Yuan_Stamp_in_taiwan.Jpg](https://zh.wikipedia.org/wiki/File:Loberty_Day_Commemorative_0.40Yuan_Stamp_in_taiwan.Jpg "fig:Loberty_Day_Commemorative_0.40Yuan_Stamp_in_taiwan.Jpg")於1955年所發行的「反共義士」[郵票](../Page/郵票.md "wikilink")\]\]

**反共義士**是[中華民國遷往](../Page/中華民國.md "wikilink")[臺灣後使用的一个](../Page/臺灣.md "wikilink")[历史](../Page/历史.md "wikilink")[名词](../Page/名词.md "wikilink")，指以特定方式脫離[中華人民共和國投奔中華民國的](../Page/中華人民共和國.md "wikilink")[軍人或者平民](../Page/軍人.md "wikilink")。

## 時代背景

[韓戰期間](../Page/韓戰.md "wikilink")，屬於[中國人民志願軍的](../Page/中國人民志願軍.md "wikilink")[戰俘中](../Page/戰俘.md "wikilink")，有一萬四千多名選擇來到臺灣，其中第一批於1954年1月23日由[韓國](../Page/韓國.md "wikilink")[仁川港搭](../Page/仁川.md "wikilink")[美國軍艦抵達](../Page/美國.md "wikilink")[基隆](../Page/基隆.md "wikilink")（此日是為[123自由日](../Page/123自由日.md "wikilink")，後來該紀念日於1993年正式改名為「世界自由日」），至1月29日為止，共計有1萬4千多人到臺灣，從此「反共義士」這一名詞在[臺灣正式大量使用](../Page/臺灣.md "wikilink")。此後，[中華民國政府設置各種安置](../Page/中華民國政府.md "wikilink")、獎勵義士的辦法，特別是針對駕機、駕艦脫離中華人民共和國的反共義士，更採取巨額獎金的獎勵措施。其中最受關注的還是20世紀50年代到7、80年代駕機到台灣「投誠」的「反共義士」，共有16名解放軍空軍飛行員與機組人員（其中有人不願投誠、有人迫降身亡）、駕著13架飛機投奔台灣。

這些人被中華民國政府刻意捧為對大陸「統戰」的樣板與對象，因此發出的獎金格外豐厚。他們來台後得到的黃金數量，最少的是1961年9月駕AN-2型民用螺旋槳飛機，從山東膠縣起飛，降落於韓國濟州島，其後輾轉抵達台灣的[邵希彥](../Page/邵希彥.md "wikilink")、[高佑宗兩人](../Page/高佑宗.md "wikilink")，共獲500兩黃金；最多的是1983年8月7日駕[米格-21戰機逃台的](../Page/米格-21.md "wikilink")[孫天勤](../Page/孫天勤.md "wikilink")，獲7000兩黃金。據統計，中華民國政府前後發出的黃金多達5萬兩。\[1\]\[2\]\[3\]

1991年，中華民國政府終止[動員戡亂後](../Page/動員戡亂.md "wikilink")，不再視中共為叛亂組織，反共義士也漸次成為歷史名詞，而對於一些劫持民航機來臺者不僅不再有相關獎勵，並為了配合國際反劫機公約與本國反劫機法律，而改以[劫機罪名判刑入獄](../Page/劫機.md "wikilink")。

## 義士分類

### 韓戰戰俘

韓戰戰俘營中有1萬4千名選擇投奔「復興基地」台灣，拒返[中國大陸的](../Page/中國大陸.md "wikilink")「反共義士」，這些人來台後，多半仍舊編入軍隊。劉儒裕、陳煌、胡輔仁、岳瑞武、蔣春松、彭七珍、吳祿安、雷勵夫、胡仁祥、郭守勤、張迪生、陳永華、王北山、汪金鏡、劉玉敬等人即屬於此。

### 中國大陸難民

1965年5月，中國大陸難民潮時期，從中國大陸逃往[香港](../Page/香港.md "wikilink")，再輾轉赴臺灣的很多是年轻學生，經由[中國大陸災胞救濟總會接至臺灣](../Page/中國大陸災胞救濟總會.md "wikilink")，並且接受大學教育。葉遠翔和蕭玉井等人即屬於此。

### 駕機

合計共13架飛機，16名中共[解放军空軍飛行員](../Page/中国人民解放军.md "wikilink")：

  - 1960年1月12日：[楊德才駕](../Page/楊德才.md "wikilink")[米格-15戰鬥機](../Page/米格-15戰鬥機.md "wikilink")，從[浙江](../Page/浙江.md "wikilink")[路橋起飛](../Page/路橋.md "wikilink")，欲降落於[臺灣](../Page/臺灣.md "wikilink")[宜蘭](../Page/宜蘭縣.md "wikilink")，因地形不熟，降落時失事而死亡。

<!-- end list -->

  - 1961年9月15日：邵希彥及高佑宗利用噴灑農藥的機會，駕[AN-2型民用](../Page/AN-2.md "wikilink")[螺旋槳飛機](../Page/螺旋槳.md "wikilink")，從[山東](../Page/山東.md "wikilink")[膠縣起飛](../Page/膠縣.md "wikilink")，降落於[韓國](../Page/韓國.md "wikilink")[濟州島](../Page/濟州島.md "wikilink")。1961年10月7日抵達臺灣。

<!-- end list -->

  - 1962年3月3日：[劉承司駕米格](../Page/劉承司.md "wikilink")-15戰鬥機，從浙江路橋起飛，降落於[桃園](../Page/桃園市.md "wikilink")。

<!-- end list -->

  - 1965年11月11日：[李顯斌](../Page/李顯斌.md "wikilink")、[李才旺及](../Page/李才旺.md "wikilink")[廉保生駕](../Page/廉保生.md "wikilink")[伊爾-28轟炸機](../Page/Il-28轟炸機.md "wikilink")（[伊留申型噴射](../Page/伊留申.md "wikilink")[轟炸機](../Page/轟炸機.md "wikilink")），從[杭州](../Page/杭州.md "wikilink")[筧橋起飛](../Page/筧橋.md "wikilink")，降落於[桃園](../Page/桃園市.md "wikilink")。降落時偏離跑道，李才旺雙腿骨折，廉保生則在著陸後[自殺身亡](../Page/自殺.md "wikilink")。（此機經修復之後現存於中華民國空軍官校空軍軍史館廣場供參觀。)
      - 李才旺1977年自空軍退伍，並舉家移民[美國](../Page/美國.md "wikilink")。1983年返回[中國大陸](../Page/中國大陸.md "wikilink")，並向中共指稱，當時是受到李顯斌挾持，而廉保生是在射手艙中自殺。由於李才旺的說法，使得廉保生被中國大陸追認為[革命烈士](../Page/革命烈士.md "wikilink")。後來正式投共，中華民國政府除名「反共義士」稱號。
      - 1991年12月，李顯斌自[加拿大返回](../Page/加拿大.md "wikilink")[山東老家探親](../Page/山東.md "wikilink")，在[青島公安局被捕](../Page/青島.md "wikilink")，被判15年有期徒刑。關了11年，2002年5月14日，因患胃癌假釋出獄，同年12月17日病逝[上海](../Page/上海.md "wikilink")，享年66歲。
      - 廉保生[自盡後埋在桃園空軍基地外的公墓](../Page/自殺.md "wikilink")，2016年9月28日其[姪子廉成剛](../Page/姪.md "wikilink")、廉野來台將遺骨揀骨後火化，並將[骨灰迎回天津安葬](../Page/骨灰.md "wikilink")\[4\]。

<!-- end list -->

  - 1977年7月7日：[范園焱駕](../Page/范園焱.md "wikilink")[米格-19戰鬥機](../Page/米格-19.md "wikilink")，從[福建](../Page/福建.md "wikilink")[晉江起飛](../Page/晉江.md "wikilink")，降落於[臺南](../Page/臺南市.md "wikilink")，获黄金4000两，后加入[中華民國空軍](../Page/中華民國空軍.md "wikilink")，獲授中校军衔，後娶台灣小姐[彭啟鈺](../Page/彭啟鈺.md "wikilink")。

<!-- end list -->

  - 1982年10月16日：[吳榮根駕](../Page/吳榮根.md "wikilink")[殲偵-6](../Page/殲-6.md "wikilink")（米格-19）戰鬥機，從山東[文登起飛](../Page/文登.md "wikilink")，降落於[韓國漢城](../Page/韓國.md "wikilink")（今[首爾](../Page/首爾.md "wikilink")）。1982年10月31日抵達臺灣。

<!-- end list -->

  - 1983年8月7日：孫天勤（當時46歲，為[陝西人](../Page/陝西人.md "wikilink")）駕米格-21戰鬥機，於[遼寧](../Page/遼寧.md "wikilink")[大連起飛](../Page/大連.md "wikilink")，降落於[韓國漢城](../Page/韓國.md "wikilink")。1983年8月24日抵達臺灣。
      - 孫天勤在[仁川外海投下副油箱以減輕重量](../Page/仁川.md "wikilink")，被誤以為投擲炸彈，使韓國空防當局發佈真正的「[空襲警報](../Page/空襲警報.md "wikilink")」，在漢城、[京畿一帶造成一場大驚慌](../Page/京畿道.md "wikilink")，有數十名民眾為緊急避難而遭踩傷。2017年9月30日因肺炎併發敗血病，病逝於台北市三軍總醫院。

<!-- end list -->

  - 1983年11月14日：[王學誠駕](../Page/王學誠.md "wikilink")[殲-5](../Page/殲-5.md "wikilink")（[米格-17](../Page/米格-17.md "wikilink")）戰鬥機，於浙江[岱山起飛](../Page/岱山.md "wikilink")，降落於[桃園縣](../Page/桃園市.md "wikilink")[中正機場](../Page/中正機場.md "wikilink")。

<!-- end list -->

  - 1985年8月25日：[蕭天潤駕](../Page/蕭天潤.md "wikilink")[轟-5轟炸機](../Page/轟-5.md "wikilink")，從山東膠縣起飛，降落於韩国[全羅北道](../Page/全羅北道.md "wikilink")[裡里市](../Page/裡里市.md "wikilink")。1985年9月20日抵達臺灣。
      - 駕駛員蕭天潤駕機到韩国，因油料耗盡迫降在裡里市郊外的稻田，撞死韓國農夫裴奉煥（），機員一死二傷，另一名傷者痊癒後帶著死者骨灰被送回中國大陸。蕭天潤則送台灣。

<!-- end list -->

  - 1986年2月21日：[陳寶忠駕殲偵](../Page/陳寶忠.md "wikilink")-6（米格-19）戰鬥機，從[遼寧](../Page/遼寧.md "wikilink")[瀋陽起飛](../Page/瀋陽.md "wikilink")，降落於韩国京畿道。1986年4月30日抵達臺灣。

<!-- end list -->

  - 1986年10月24日：[鄭菜田駕](../Page/鄭菜田.md "wikilink")[殲-6](../Page/殲-6.md "wikilink")（米格-19）戰鬥機，從山東[煙臺起飛](../Page/煙臺.md "wikilink")，降落於韩国[忠清北道](../Page/忠清北道.md "wikilink")[清州](../Page/清州.md "wikilink")。1986年12月19日抵達臺灣。

<!-- end list -->

  - 1987年11月19日：[劉志遠駕殲](../Page/劉志遠.md "wikilink")-6（米格-19）戰鬥機，從[福建](../Page/福建.md "wikilink")[龍溪起飛](../Page/龍溪.md "wikilink")，降落於[臺中市](../Page/臺中市.md "wikilink")[清泉崗机场](../Page/清泉崗机场.md "wikilink")。

<!-- end list -->

  - 1989年9月16日：[蔣文浩駕殲](../Page/蔣文浩.md "wikilink")-6（米格-19）戰鬥機，從福建龍溪起飛，降落於[金門](../Page/金門.md "wikilink")。1989年9月6日，空軍航空兵第49師145團2大隊飛行員蔣文浩中尉（時年尚不滿­24歲）駕駛40307號殲-6型戰鬥機從福建漳州龍溪機場起飛，降落在金門尚義­機場。

隨著兩岸局勢、國際環境的巨大變化，90年代以後，再無80年代兩岸對飛的盛況，蔣文­浩成為迄今最後一位投奔中華民國的中國解放軍飛行員。

### 六義士

「[六義士](../Page/六義士.md "wikilink")」指1983年5月5日自中國大陸劫持民航機降落到南韓漢城[春川美軍飛彈基地](../Page/春川.md "wikilink")，被中華民國政府稱為反共義士的[卓長仁](../Page/卓長仁.md "wikilink")、[姜洪軍](../Page/姜洪軍.md "wikilink")、[高東萍](../Page/高東萍.md "wikilink")、[王豔大](../Page/王豔大.md "wikilink")、[安偉建及](../Page/安偉建.md "wikilink")[吳雲飛等](../Page/吳雲飛.md "wikilink")6人。南韓當時以違反[國際反劫機公約與國內法的罪名判處徒刑](../Page/國際反劫機公約.md "wikilink")，將其羈押一年三個月後驅逐出境，遣送到台灣。該航機原本是由[瀋陽飛往](../Page/瀋陽.md "wikilink")[上海的](../Page/上海.md "wikilink")[中国民航296号航班](../Page/中国民航296号航班劫机事件.md "wikilink")。機上有9名機組人員，96名乘客，其中3名[日本人](../Page/日本.md "wikilink")。

  - 後來在台灣，卓長仁、姜洪軍與[施小寧因投資失敗](../Page/施小寧.md "wikilink")，涉嫌於1991年8月16日綁架並殺害前[國泰醫院副院長](../Page/國泰醫院.md "wikilink")[王欲明之獨子](../Page/王欲明.md "wikilink")[王俊傑](../Page/王俊傑.md "wikilink")，而分別被判處[死刑與](../Page/死刑.md "wikilink")[無期徒刑](../Page/無期徒刑.md "wikilink")。2001年8月10日，卓長仁、姜洪軍遭槍決，結束生命。

<!-- end list -->

  - 歷史名詞：1988年後，為尊重國際反劫機的行為標準，對劫機者，中華民國政府採取人機分離的方式，將劫機犯留下來服刑，服刑屆滿或假釋後遣返原國或原地。「劫機義士」從此成為歷史名詞。

### 駕船

  - 1985年9月30日：施小寧、[張木珠帶了大批武器](../Page/張木珠.md "wikilink")，從福建沿海的蘇澳乘12匹馬力之漁船到[馬祖](../Page/馬祖.md "wikilink")。施小寧後來因涉前述案件而被判處無期徒刑。
  - 1986年6月[馬曉濱](../Page/馬曉濱.md "wikilink")、[劉德金等](../Page/劉德金.md "wikilink")19名青年從[廈門共同駕舢舨叛逃至韓國](../Page/廈門.md "wikilink")[瑞山郡](../Page/瑞山.md "wikilink")[鶴岩浦港](../Page/鶴岩浦.md "wikilink")，原本意圖前往謀生，但同年7月8日被遣送到台，並被冠以『反共義士』的名號。中華民國情治單位於翌日將其送至[澎湖](../Page/澎湖.md "wikilink")[中南半島難民營](../Page/中南半島難民營.md "wikilink")，予以隔離偵訊調查。1989年11月17日晚間：馬曉濱夥同[華裔](../Page/華裔.md "wikilink")[越南難胞](../Page/越南.md "wikilink")[唐龍](../Page/唐龍_\(華裔越南難胞\).md "wikilink")、[長榮海運公司離職警衛](../Page/長榮海運.md "wikilink")[王士杰](../Page/王士杰.md "wikilink")，共同綁架長榮老闆[張榮發的長子張國明](../Page/張榮發.md "wikilink")，並向其勒索[新台幣五千萬元](../Page/新台幣.md "wikilink")。後來三人被捕，全被判處死刑，於1990年7月20日凌晨執行槍決。由於事後張國明安全歸來，當時台灣社會許多人認為馬曉濱應罪不及於死，因此積極發起「救援馬曉濱」行動，但沒成功。

## 参考文献

### 引用

### 来源

  - 書籍

<!-- end list -->

  - 《戰後外交史料彙編：韓戰與反共義士篇（一）》：2005年，[行政院原住民委員會](../Page/行政院原住民委員會.md "wikilink")，ISBN
    9860012598.

## 外部連結

  - [一個荒謬時代的結束](http://www.rickchu.net/detail.php?rc_id=1229&rc_stid=14)
  - [母親的懷抱-返鄉之旅](http://www.pts.org.tw/~web01/faces/p6.htm)
  - [王北山歷經坎坷生命路─手著韓戰生死戀說從頭](https://web.archive.org/web/20110820072431/http://lov.vac.gov.tw/Memory/Content.aspx?Para=233&Control=6)
  - [在野黨掀大陸熱，昔日反共義士誰給交代？](http://www.ettoday.com/2005/04/25/301-1781778.htm)
  - [駕著13架飛機投奔自由的反共義士](http://home.myemage.com/personal/MsgBoard/Img_Subject_View.aspx?S_ID=200504251106705&Eus_Account=dreamweaver)
  - [「反共義士」活躍的年代](http://203.66.167.137/e-radio/200404/10-01.htm)
  - [用錢買來的反共義士－1986年的回顧](http://taiwantt.org.tw/books/cryingtaiwan4/content/1980/19860322a-53.htm)
  - [歷年反共義士駕機來台之各型戰機展示](https://web.archive.org/web/20110722081349/http://www.ttvs.cy.edu.tw/kcc/9412air/a5.htm)
  - [大陸歷年來投奔臺灣之軍、客機資料](https://web.archive.org/web/20080416213133/http://www.mac.gov.tw/big5/rpir/2nd2_8.htm)
  - [繁花過眼．刻痕長存　反共義士投奔來台的真相](http://203.66.167.137/e%2Dradio/200405/13-01.htm)
  - [義士李顯斌走了，前妻張美雲百感交集](https://web.archive.org/web/20111203090340/http://www.libertytimes.com.tw/2002/new/dec/18/today-c1.htm)
  - [昔日英雄平凡老退
    賞金千兩境遇不同](http://72.14.235.104/search?q=cache:pM-xMzmDrLwJ:www.cdn.com.tw/daily/2001/08/29/text/900829d6.htm+%E9%82%B5%E5%B8%8C%E5%BD%A5+%E9%AB%98%E4%BD%91%E5%AE%97&hl=zh-TW&ct=clnk&cd=4&gl=tw&lr=lang_zh-TW)
  - [反共義士吳榮根掉入桃色陷阱
    千萬泡湯](http://72.14.235.104/search?q=cache:8eP07vUegqMJ:www.tts.idv.tw/html/8C/%25A4%25CF%25A6%40%25B8q%25A4h.htm+%E5%B0%8D%E5%8C%AA%E9%99%B8%E6%B5%B7%E7%A9%BA%E8%BB%8D%E9%87%8D%E8%B3%9E%E6%8B%9B%E9%99%8D%E8%BE%A6%E6%B3%95&hl=zh-TW&ct=clnk&cd=5&gl=tw&lr=lang_zh-TW)
  - [蔣文浩義士（中共空軍中尉）駕機來歸自由祖國記者會](https://www.youtube.com/watch?v=LkjhZKo_oz8&list=PLAA219801DE54CAB4&index=5)
  - [施小寧檔案（上）](http://mypaper.pchome.com.tw/fld/post/4131618)
  - [施小寧檔案（下）](http://mypaper.pchome.com.tw/fld/post/4131622)
  - [馬曉濱檔案（上）](http://mypaper.pchome.com.tw/fld/post/4131595)
  - [馬曉濱檔案（下）](http://mypaper.pchome.com.tw/fld/post/4131603)
  - [再見阿里郎](http://www.rickchu.net/download/book_03.pdf#search=%22%E9%A6%AC%E6%9B%89%E6%BF%B1%22)
  - [專家分析廈門劫機事件：劫機飛臺北將被遣返](http://news.big5.anhuinews.com/system/2004/12/27/001085852.shtml)
  - [曾經…千兩黃金換起義來歸](https://web.archive.org/web/20121020180757/http://www.libertytimes.com.tw/2007/new/jan/31/today-p2.htm)
  - [行政院國軍退除役官兵輔導委員會反共義士輔導中心組織規程](http://law.moj.gov.tw/LawClass/LawAll.aspx?PCode=F0000023)
  - [大陸來臺義士義胞現任公職人員任用資格考試規則](http://db.lawbank.com.tw/FLAW/FLAWDAT01.asp?lsid=FL020247)
  - [大陸來臺反共學人義士義胞應考試及任用資歷審查辦法](http://db.lawbank.com.tw/FLAW/FLAWDAT01.asp?lsid=FL020250)

## 参见

  - [朝鲜战争的战俘遣返问题](../Page/朝鲜战争的战俘遣返问题.md "wikilink")、[世界自由日](../Page/世界自由日.md "wikilink")
  - [中國大陸災胞救濟總會](../Page/中國大陸災胞救濟總會.md "wikilink")
  - [國軍與解放軍間的駕機叛逃事件](../Page/國軍與解放軍間的駕機叛逃事件.md "wikilink")
  - [脫北者](../Page/脫北者.md "wikilink")
  - [逃港](../Page/逃港.md "wikilink")
  - [脫東者](../Page/脫東者.md "wikilink")

{{-}}

[Category:台灣戰後時期人物](../Category/台灣戰後時期人物.md "wikilink")
[Category:中華民國軍事史](../Category/中華民國軍事史.md "wikilink")
[+](../Category/中國反共主義者.md "wikilink")
[Category:臺灣海峽兩岸關係術語](../Category/臺灣海峽兩岸關係術語.md "wikilink")
[Category:中华民国政治人物并称](../Category/中华民国政治人物并称.md "wikilink")
[Category:中華人民共和國脫逃者](../Category/中華人民共和國脫逃者.md "wikilink")
[Category:冷戰](../Category/冷戰.md "wikilink")

1.  [赴台獲黃金「反共義士」現狀](http://news.wenweipo.com/2013/02/17/IN1302170041.htm)
2.  [孫天勤李天慧結婚、吳榮根執教鞭
    他們不愛出名](http://theme.udn.com/theme/story/6202/218959)
3.  [兩岸史話－一二三自由日與反共義士](http://www.chinatimes.com/newspapers/20150124001084-260306)
4.  [聯合報：叛逃變烈士 廉保生的回鄉路走了51年](http://udn.com/news/story/4/1989760)。