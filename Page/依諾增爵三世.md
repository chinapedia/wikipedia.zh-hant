[教宗](../Page/教宗.md "wikilink")**諾森三世**（   約1161年—1216年7月16日），本名**罗塔里奥**，
**Lotario dei Conti di
Segni**）於1198年1月8日—1216年7月16日岀任教宗。在位的時候，中世紀的[教廷權威與影響到達登峰造極的狀態](../Page/教廷.md "wikilink")。而諾森三世他完成增加教宗權力的最後工作，在其工作及改革以後，教廷的結構堅固不搖，直到十三世紀的盡頭。

## 经历

諾森三世出身[羅馬](../Page/羅馬.md "wikilink")[貴族](../Page/貴族.md "wikilink")[門閥](../Page/門閥.md "wikilink")，深研各種[法律](../Page/法律.md "wikilink")，經過[教會法與](../Page/教會法.md "wikilink")[羅馬法的多重訓練](../Page/羅馬法.md "wikilink")，精通[辯論](../Page/辯論.md "wikilink")，再加上[神學的造就](../Page/神學.md "wikilink")，後來出任[紅衣主教](../Page/紅衣主教.md "wikilink")。擔任教宗之後，處理了當時的宗教與現世問題。他擅長把[抽象的觀念用在具體情況上](../Page/抽象.md "wikilink")，集合中世紀當時所進展一切有利的傾向，轉化成為[教廷大而統一的組織](../Page/教廷.md "wikilink")，其組織使教宗有普遍的權力。諾森三世擅長外交的手腕，使整個[基督教世界裡將教廷的權威發揮到出人意表的程度](../Page/基督教世界.md "wikilink")。他決意要把教廷在[意大利建成一個強大國家](../Page/意大利.md "wikilink")，世俗君主不能隨便使用[經濟力量來逼迫](../Page/經濟.md "wikilink")[羅馬教會就範](../Page/羅馬教會.md "wikilink")。

諾森三世的觀念是深信教宗是[基督的代理人](../Page/基督.md "wikilink")，也是[聖彼得的繼承人](../Page/聖彼得.md "wikilink")，具有至高無上的權威，所以教宗能操掌[僧](../Page/教會.md "wikilink")、[俗兩界的權力](../Page/政治.md "wikilink")，故可以隨意廢立世俗的君主，當時[神聖羅馬帝國皇帝](../Page/神聖羅馬帝國皇帝.md "wikilink")[腓特烈二世幼年即被諾森三世](../Page/腓特烈二世.md "wikilink")[監護](../Page/監護.md "wikilink")，並在諾森三世的扶持下[登基](../Page/登基.md "wikilink")。同時諾森三世設立教宗[欽差的總機關](../Page/欽差.md "wikilink")，完全歸屬教宗管轄。

諾森三世曾勸誘[法國國王發動](../Page/法國國王.md "wikilink")[十字軍](../Page/十字軍.md "wikilink")（1208-1213），去攻擊[阿爾比派](../Page/阿爾比派.md "wikilink")。在[十字軍運動上](../Page/十字軍.md "wikilink")，原來[赦免的法則是](../Page/赦免.md "wikilink")「只有親自參加十字軍運動的人，得免除犯罪的刑罰」，但諾森三世應許那些送軍人到戰場上去的人，也能得完全赦免。

諾森三世在晚年1215年召開[第四次拉特蘭會議](../Page/第四次拉特蘭會議.md "wikilink")，這是第12屆[大公會議](../Page/大公會議.md "wikilink")，也是整個[中世紀規模最大的宗教會議](../Page/中世紀.md "wikilink")，這會議成為他任職的頂峰。這次的大公會議象徵教廷權力已經完全支配拉丁基督教界的每個層面。並且，僧侶與教宗對社會的領導地位在會議中重新再得到得到肯定，從參加會議者範圍廣闊上就可以看見：有[大主教](../Page/大主教.md "wikilink")、[主教](../Page/主教.md "wikilink")、[修道院院長](../Page/修道院.md "wikilink")、副院長共1200多名主教和院長參加，還有宗教團體領袖和世俗的統治者代表等等。會議規定了一些改革的法令，目的要提高教會內部的紀律，並且抵抗當時各種產生的[異端](../Page/異端.md "wikilink")。會議文獻中第13條禁止創立新的[修會](../Page/修會.md "wikilink")；第21條則要求所有信徒每年需要[告解一次](../Page/告解.md "wikilink")，且在每年[復活節領受](../Page/復活節.md "wikilink")[聖體](../Page/聖體.md "wikilink")。另外，會議中決定把[猶太人排除於一般社會之外](../Page/猶太人.md "wikilink")，還規定猶太人必須配戴一種特殊標記。故此猶太人越發只能住在[貧民窟裡](../Page/貧民窟.md "wikilink")。\[1\]\[2\]\[3\]

诺森三世得以樹立教宗威權的有利背景一共有五個因素：

1.  [額我略七世](../Page/額我略七世.md "wikilink")（即希勒得布蘭）的榜樣：貴格利七世一生為建立教宗無上權威的努力雖然失敗，卻給繼任者帶來前進的動力。
2.  偽文件「[君士坦丁献土諭](../Page/君士坦丁献土.md "wikilink")」：偽文件則是為當時爭取教宗權威提供合法而有利的基礎。
3.  [十字軍東征](../Page/十字軍東征.md "wikilink")：十字軍東征使得教宗得以「基督教世界之首」自居。
4.  「藉著罪名」或譯「由於罪」原則：「藉著罪名」的原則讓教宗成為在道德上可以制裁君王的人。
5.  [義大利政情對教廷有利](../Page/義大利.md "wikilink")：當時[北義大利的權貴願意與教宗聯合對抗皇帝](../Page/北義大利.md "wikilink")。\[4\]

## 参考文献

<div class="references-small">

<references />

</div>

## 譯名列表

  - 依諾增爵三世：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)作依諾增爵。
  - 因諾森特三世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作因諾森特。
  - 英諾森三世：[《大英簡明百科知識庫》2005年版作英諾森](http://wordpedia.britannica.com/concise/content.aspx?id=4166&hash=S4qRyNX7zlhlyoEjrdWypA%3D%3D&t=3)。
  - 諾森三世：[香港天主教教區檔案　歷任教宗作諾森](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)。
  - 意諾增爵三世：[天主教天津教区](https://web.archive.org/web/20070311132024/http://www.catholic.tj.cn/p/Article/Jhls/Lsjs/200507/7285.html)作意諾增爵。

[I](../Category/教宗.md "wikilink") [I](../Category/阿納尼人.md "wikilink")
[I](../Category/1216年逝世.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")

1.  陶理，《基督教兩千年史》，李伯明、林牧野合譯， (香港：海天書樓，2001)，262-264。
2.  古勒本，《教會歷史》，李少蘭譯， (台北：道聲出版社，2000)，226,236-237。
3.  華爾麥爾(Bihlmeyer) 等編著，《中世紀教會史》，雷立柏譯， (北京：宗教文化出版社，2010)，230。
4.  郝伯爾著，李林靜之譯《歷史的軌跡－－二千年教會史》，（台北市：校園書房，1999年），148-9。