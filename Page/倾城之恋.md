《**傾城之戀**》，是作家[張愛玲创作于](../Page/張愛玲.md "wikilink")1943年的[短篇小說作品](../Page/短篇小說.md "wikilink")，也是张爱玲的成名作与代表作之一。创作当年即在上海于《杂志》首次发表（1943年九月、十月的第十一卷、第十二卷第一期）\[1\]。后收录于张爱玲1944年出版的小说集《传奇》，同时收录的包括中短篇小说〈留情〉、〈鴻鸞禧〉、〈紅玫瑰與白玫瑰〉、〈等〉、〈桂花蒸
阿小悲秋〉、〈金鎖記〉還有〈傾城之戀〉。

## 情節概要

故事發生在[上海和](../Page/上海.md "wikilink")[香港](../Page/香港.md "wikilink")。來自上海大戶人家的小姐**白流蘇**，在經歷一次失敗的婚姻後，回歸娘家，數年間，她耗盡資財，變得身無長物，在親戚間備受冷嘲熱諷，歷盡世態炎涼。白流蘇前夫親戚徐太太本想為七小姐寶絡作媒人，介紹給多金瀟灑的單身漢**范柳原**相識，流蘇作陪客，並因而認識了范柳原。

白流蘇隨徐太太來到香港，與范柳原相知日深，她便拿自己當做賭注，博取范柳原的愛情。在白流蘇看似無望，即將返回上海，范柳原將離開香港時，日軍對香港展開轟炸，范柳原折回保護白流蘇，在生死關頭時，兩人才得以真心相見，許下天長地久的諾言。在滬港交通回復後，他兩返回上海生活。

香港的淪陷成全了他們。篇名中的「傾城」既指香港淪陷，亦借喻白范二人打破猜算隔膜，真心相愛，一語雙關。

## 改編作品

### 電影

  - [邵氏電影公司在](../Page/邵氏電影公司.md "wikilink")1984年發行電影版，由[許鞍華導演](../Page/許鞍華.md "wikilink")，[周潤發](../Page/周潤發.md "wikilink")、[繆騫人主演](../Page/繆騫人.md "wikilink")。此電影曾被批評荷李活化，缺少原作的蒼涼感。

### 電視劇

  - [中國中央電視台改編電視劇](../Page/中國中央電視台.md "wikilink")，由[陳數](../Page/陳數.md "wikilink")、[黃覺](../Page/黃覺.md "wikilink")、[王學兵](../Page/王學兵.md "wikilink")、[劉一含等人主演](../Page/劉一含.md "wikilink")。在台灣由[年代MUCH台播出](../Page/年代MUCH台.md "wikilink")，香港在2010年3月1日由[亞洲電視](../Page/亞洲電視.md "wikilink")[本港台播出](../Page/本港台.md "wikilink")。

## 延伸閱讀

  - 邝可怡：〈[上海跟香港的“对立”——读《时代姑娘》、《倾城之恋》和《香港情与爱》](http://www.nssd.org/articles/article_read.aspx?id=25061756)〉。

[Category:張愛玲作品](../Category/張愛玲作品.md "wikilink")
[Category:中文短篇小说](../Category/中文短篇小说.md "wikilink")
[Category:1943年中国小说](../Category/1943年中国小说.md "wikilink")
[Category:香港島背景小說](../Category/香港島背景小說.md "wikilink")
[Category:上海背景小說](../Category/上海背景小說.md "wikilink")
[Category:上海市背景作品](../Category/上海市背景作品.md "wikilink")
[Category:上海背景电影](../Category/上海背景电影.md "wikilink")
[Category:抗日战争背景小说](../Category/抗日战争背景小说.md "wikilink")
[Category:愛情小說](../Category/愛情小說.md "wikilink")

1.