**琵嘴鸭**（[學名](../Page/双名法.md "wikilink")：*Spatula
clypeata*），又名琵琶嘴鸭、铲土鸭、杯凿、广味凫。

## 生态环境

栖息于开阔地区的湖泊河流等处，也见于山区以及高原上的水域，偶尔也会在村镇附近的污水池塘中。琵嘴鸭不喜欢在长满[挺水植物的水域中觅食](../Page/挺水植物.md "wikilink")，在沿海也不常见，但是在咸水水域却也可以看到。琵嘴鸭非常机警，如果发现敌害会迅速逃跑，虽然飞行能力不强，有时却能飞得很快。

## 分布地域

[繁殖在](../Page/繁殖.md "wikilink")[中国](../Page/中国.md "wikilink")[新疆西部以及](../Page/新疆.md "wikilink")[东北北部](../Page/中国东北.md "wikilink")，[欧洲](../Page/欧洲.md "wikilink")、[西伯利亚](../Page/西伯利亚.md "wikilink")、[蒙古大部以及](../Page/蒙古.md "wikilink")[北美洲西部](../Page/北美洲.md "wikilink")，还有少数在[日本](../Page/日本.md "wikilink")[北海道北部繁殖](../Page/北海道.md "wikilink")；[迁徙途径东北南部](../Page/迁徙.md "wikilink")，[内蒙古](../Page/内蒙古.md "wikilink")、[青海](../Page/青海.md "wikilink")、新疆以及[华北各省](../Page/华北.md "wikilink")，越冬在[长江中下游以南各省](../Page/长江.md "wikilink")、[台湾和](../Page/台湾.md "wikilink")[西藏南部地区](../Page/西藏.md "wikilink")，以及[英国](../Page/英国.md "wikilink")、[爱尔兰](../Page/爱尔兰.md "wikilink")、[欧洲南部](../Page/欧洲.md "wikilink")、[亚洲大陆南部](../Page/亚洲大陆.md "wikilink")、日本和[菲律宾](../Page/菲律宾.md "wikilink")、[非洲北部北美洲南部和墨西哥](../Page/非洲.md "wikilink")，部分个体甚至被发现在[密克罗尼西亚群岛越冬](../Page/密克罗尼西亚群岛.md "wikilink")。

## 特征

[Pzy.jpg](https://zh.wikipedia.org/wiki/File:Pzy.jpg "fig:Pzy.jpg")
[Spatula_clypeata_MHNT.ZOO.2010.11.18.2.jpg](https://zh.wikipedia.org/wiki/File:Spatula_clypeata_MHNT.ZOO.2010.11.18.2.jpg "fig:Spatula_clypeata_MHNT.ZOO.2010.11.18.2.jpg")
体大嘴长，末端宽大有如铲子因为其喙形如琵琶，故而得名琵嘴鸭。雄鸟的腹部栗色，胸白，头颈深绿色两侧具金属光泽；雌鸟褐色斑驳，尾近白色，贯眼纹深色，色彩似雌绿头鸭但嘴形清楚可辨。飞行时浅灰蓝色的翼上覆羽与深色飞羽及绿色翼镜成对比。雄鸟虹膜金黄色，雌鸟褐色；繁殖期雄鸟的上嘴近黑色，雌鸟橘黄褐色；脚橙红色，脚爪兰黑色。

## 食物

琵嘴鸭的食性偏动物性，常在浅水河岸附近的水中用铲子形的嘴捞取水面或水边的食物，由于潜水能力不强琵嘴鸭很少潜入水中觅食。据研究琵嘴鸭主要的食物为小型[软体动物](../Page/软体动物.md "wikilink")，[浮游生物](../Page/浮游生物.md "wikilink")、[甲壳类](../Page/甲壳类.md "wikilink")，[昆虫幼虫](../Page/昆虫.md "wikilink")、小[鱼](../Page/鱼.md "wikilink")、[蛙等冬季也吃一些植物型食物](../Page/蛙.md "wikilink")，包括[蓼科植物的种子](../Page/蓼科.md "wikilink")。[茄科植物的](../Page/茄科.md "wikilink")[浆果](../Page/浆果.md "wikilink")，[莎草科植物的](../Page/莎草科.md "wikilink")[瘦果](../Page/瘦果.md "wikilink")、[眼子菜科](../Page/眼子菜科.md "wikilink")、[浮萍科](../Page/浮萍科.md "wikilink")、[槐叶萍科植物等](../Page/槐叶萍科.md "wikilink")。

## 繁殖与保护

琵嘴鸭未列入濒危名单，但受到栖息地破坏的威胁，而且由于狩猎和环境条件恶化，目前数量较少。

## 参考文献

## 外部连接

  -
[AC](../Category/IUCN無危物種.md "wikilink")
[clypeata](../Category/鸭属.md "wikilink")