[Rambutan_fruit_and_seed.jpg](https://zh.wikipedia.org/wiki/File:Rambutan_fruit_and_seed.jpg "fig:Rambutan_fruit_and_seed.jpg")

**紅毛丹**（[学名](../Page/学名.md "wikilink")：**），為[東南亞原產之](../Page/東南亞.md "wikilink")[無患子科大型熱帶果樹](../Page/無患子科.md "wikilink")\[1\]，马来文称之“rambutan”，意为“毛茸茸之物”。成熟的红毛丹果并非都是红色的，也有黄色的果子。有的红毛丹的核的大小近似于[芝麻](../Page/芝麻.md "wikilink")。红毛丹的味道类似于[荔枝](../Page/荔枝.md "wikilink")。

除東南亞之外，中國也有部分種植。與中國所產的[韶子](../Page/韶子.md "wikilink")（**）、[海南韶子](../Page/海南韶子.md "wikilink")（**）同屬[韶子屬](../Page/韶子屬.md "wikilink")
\[2\]\[3\]。

紅毛丹株高可達15公尺，葉互生，偶數羽狀複葉，小葉卵形或長橢圓形，先端尖，全緣，革質。雌雄同株或異株，圓錐花序，花冠黃綠色。核果球形或卵圓形，果表密被細長軟次，熟果鮮紅或帶黃色，中央具1凹溝，表面有龜甲紋。果肉(假種皮)白色，肉質，透明多汁，酸甜可口，內藏種子1粒，扁卵形。

在東南亞產地，紅毛丹每年開花2次，4\~5月、9\~10月開花，7\~8、12\~1月採收果實，成株每株能結果2000\~3000粒。

## 註釋

## 參考文獻

  - 《台灣蔬果實用百科第三輯》，薛聰賢 著，薛聰賢出版社，2003年

## 外部链接

  -
[lappaceum](../Category/韶子属.md "wikilink")
[Category:水果](../Category/水果.md "wikilink")
[Category:中國植物](../Category/中國植物.md "wikilink")
[Category:1767年描述的植物](../Category/1767年描述的植物.md "wikilink")
[Category:原產於亞洲的水果](../Category/原產於亞洲的水果.md "wikilink")

1.
2.  [红毛丹](http://www.cvh.org.cn/lsid/index.php?lsid=urn:lsid:cvh.org.cn:names:cnpc_72568&vtype=tax,img,spm,ref,link)\[DB\]．：[中国数字植物标本馆](../Page/中国数字植物标本馆.md "wikilink")，2012-11-1

3.  罗献瑞，陈德昭．[红毛丹](http://v2.cvh.org.cn/zhiwuzhi/page/47%281%29/038.PDF)\[A\]．见：中国科学院中国植物志编辑委员会．[中国植物志](../Page/中国植物志.md "wikilink")
    第四十七卷 第一分册\[M\]．北京：科学出版社，1985：38-39