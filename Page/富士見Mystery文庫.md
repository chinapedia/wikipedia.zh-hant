**富士見Mystery文庫**（，），是[日本](../Page/日本.md "wikilink")[出版社](../Page/出版社.md "wikilink")[富士見書房出版的](../Page/富士見書房.md "wikilink")[文庫系列](../Page/文庫.md "wikilink")。内容以[輕小說為主](../Page/輕小說.md "wikilink")，2000年11月自同出版社的[富士見Fantasia文庫獨立](../Page/富士見Fantasia文庫.md "wikilink")。

2003年12月開始，企圖跳脫所謂Mystery（本格推理小說）的既有觀念而大幅更新。封面插圖的佔有量增加、加入裏封面（封面和一張插畫），黑色調封底也改為柔和色調。內容增加戀愛要素，並在此類作品書腰寫上大字的「L·O·V·E！」。

以建立和現有[推理小說不同的](../Page/推理小說.md "wikilink")「角色推理」為目標。此外，有獨自的新人獎[富士見Young
Mystery大獎](../Page/富士見Young_Mystery大獎.md "wikilink")。

已於2007年廢刊。不再出版新作，但仍有出版原先未完結作品（也有部份作品版權被轉移）。

## 作品一覽

  -
    由於未出版書籍居多，完整列表請見日語版條目。

### 台灣角川

  - [GOSICK](../Page/GOSICK.md "wikilink")（[櫻庭一樹](../Page/櫻庭一樹.md "wikilink")/[武田日向](../Page/武田日向.md "wikilink")）
  - [糖果子彈 A Lollypop or A
    Bullet](../Page/糖果子彈_A_Lollypop_or_A_Bullet.md "wikilink")（櫻庭一樹/）
  - [ROOM
    NO.1301](../Page/ROOM_NO.1301.md "wikilink")（[新井輝](../Page/新井輝.md "wikilink")/）
  - [SHI-NO](../Page/SHI-NO.md "wikilink")（[上月雨音](../Page/上月雨音.md "wikilink")/）
  - [我的親愛主人\!?](../Page/我的親愛主人!?.md "wikilink")（[鷹野祐希](../Page/鷹野祐希.md "wikilink")/）

### 尖端出版

  - [增血鬼果林](../Page/增血鬼果林.md "wikilink")（[甲斐透](../Page/甲斐透.md "wikilink")/[影崎由那](../Page/影崎由那.md "wikilink")）

### 東立出版社

  - [靜流姊與執拗的死者們](../Page/靜流姊與執拗的死者們.md "wikilink")（[上遠野浩平](../Page/上遠野浩平.md "wikilink")/[椋本夏夜](../Page/椋本夏夜.md "wikilink")）

### 未代理

  - [魔法使的條件](../Page/魔法使的條件.md "wikilink")（[枯野瑛](../Page/枯野瑛.md "wikilink")/，台灣角川代理漫畫版）

## 關聯條目

  - [Light Novel Award](../Page/Light_Novel_Award.md "wikilink")
      - [富士見Fantasia文庫](../Page/富士見Fantasia文庫.md "wikilink")
      - [角川Sneaker文庫](../Page/角川Sneaker文庫.md "wikilink")
      - [電擊文庫](../Page/電擊文庫.md "wikilink")
      - [Fami通文庫](../Page/Fami通文庫.md "wikilink")
  - [輕小說作家列表](../Page/輕小說作家列表.md "wikilink")

## 外部連結

  - [富士見Mystery文庫官方網站](https://web.archive.org/web/20150208024349/http://www.fujimishobo.co.jp/top.php)
  - [富士見Mystery文庫](https://web.archive.org/web/20120130040325/http://www.kadokawa.co.jp/fujimi/mystery/)（舊）

[F](../Category/富士見書房.md "wikilink")
[F](../Category/富士見Mystery文庫.md "wikilink")