[Two_red_dice_01.svg](https://zh.wikipedia.org/wiki/File:Two_red_dice_01.svg "fig:Two_red_dice_01.svg")一次，會獲得一個從1到6的隨機數。\]\]
**随机数生成器**（Random number
generator）是通过一些[算法](../Page/算法.md "wikilink")、物理訊號、環境[噪音等来产生看起來似乎沒有關聯性的](../Page/噪音.md "wikilink")[數列的方法或裝置](../Page/數列.md "wikilink")。丟[硬幣](../Page/硬幣.md "wikilink")、丟[骰子](../Page/骰子.md "wikilink")、[洗牌就是生活上常見的隨機數產生方式](../Page/洗牌.md "wikilink")。

大部分[计算机上的](../Page/计算机.md "wikilink")[偽随机数](../Page/偽随机数.md "wikilink")，并不是真正的随机数，只是重复的周期比较大的數列，是按一定的[算法和种子值生成的](../Page/算法.md "wikilink")。

## 另見

  - [百萬亂數表](../Page/百萬亂數表.md "wikilink")
  - [密码学安全伪随机数生成器](../Page/密码学安全伪随机数生成器.md "wikilink")

## 進階閱讀

  -
  -
  -
  - [NIST SP800-90A, B, C series on random number
    generation](http://csrc.nist.gov/publications/PubsSPs.html)

## 外部連結

  - [Random and Pseudorandom on In Our Time at the
    BBC.](http://www.bbc.co.uk/programmes/b00x9xjb)

[Category:信息论](../Category/信息论.md "wikilink")
[\*](../Category/随机数生成.md "wikilink")