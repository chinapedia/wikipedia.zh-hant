**3月30日**是[公历一年中的第](../Page/公历.md "wikilink")89天（[闰年第](../Page/闰年.md "wikilink")90天），离全年的结束还有276天。

## 大事记

### 19世紀

  - [1856年](../Page/1856年.md "wikilink")：[奥斯曼土耳其帝国](../Page/奥斯曼土耳其帝国.md "wikilink")、[俄罗斯](../Page/俄罗斯.md "wikilink")、[萨丁尼亚王国](../Page/萨丁尼亚王国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[英国](../Page/英国.md "wikilink")、[奥地利和](../Page/奥地利.md "wikilink")[普鲁士签署](../Page/普鲁士.md "wikilink")《[巴黎条约](../Page/1856年巴黎條約.md "wikilink")》，[克里米亚战争结束](../Page/克里米亚战争.md "wikilink")。
  - [1867年](../Page/1867年.md "wikilink")：[美国国务卿](../Page/美国国务卿.md "wikilink")[威廉·西華德与俄国代表签订协议](../Page/威廉·西華德.md "wikilink")，以720万[美元从俄国手中](../Page/美元.md "wikilink")[購買](../Page/阿拉斯加易手.md "wikilink")[阿拉斯加](../Page/阿拉斯加.md "wikilink")。
  - [1879年](../Page/1879年.md "wikilink")：[日本侵占](../Page/日本.md "wikilink")[琉球](../Page/琉球.md "wikilink")，废[琉球王国](../Page/琉球王国.md "wikilink")，改置为[冲绳县](../Page/冲绳县.md "wikilink")。

### 20世紀

  - [1912年](../Page/1912年.md "wikilink")：[法国与](../Page/法国.md "wikilink")[摩洛哥签署](../Page/摩洛哥.md "wikilink")《[非斯条约](../Page/非斯条约.md "wikilink")》，摩洛哥正式沦为法国的[被保护国](../Page/保护国.md "wikilink")。
  - [1940年](../Page/1940年.md "wikilink")：日本扶持下的[傀儡政权](../Page/傀儡政权.md "wikilink")[汪精卫政权在](../Page/汪精卫政权.md "wikilink")[南京成立](../Page/南京.md "wikilink")，[汪精卫就任该政权的国民政府主席和行政院长](../Page/汪精卫.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：《[王子复仇记](../Page/王子复仇记.md "wikilink")》获得第21届[奥斯卡金像奖的最佳影片](../Page/奥斯卡金像奖.md "wikilink")，最佳男主角大奖。
  - [1950年](../Page/1950年.md "wikilink")：[中华人民共和国教育部发出通告](../Page/中华人民共和国教育部.md "wikilink")，规定宣布6月1日[国际儿童节为中国儿童节](../Page/儿童节.md "wikilink")，废除中华民国订立的4月4日儿童节。
  - [1961年](../Page/1961年.md "wikilink")：旨在反对违法[麻醉品制造和走私的](../Page/麻醉品.md "wikilink")[国际公约](../Page/国际公约.md "wikilink")《[麻醉品单一公约](../Page/麻醉品单一公约.md "wikilink")》在[美国](../Page/美国.md "wikilink")[纽约的国际会议上被签署](../Page/纽约.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：[中华人民共和国政府任命](../Page/中华人民共和国政府.md "wikilink")[黄镇为](../Page/黄镇.md "wikilink")[中国驻美联络处主任](../Page/中国驻美联络处.md "wikilink")，[韩叙为副主任](../Page/韩叙.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：[邓小平在](../Page/邓小平.md "wikilink")[中共中央理论](../Page/中共中央.md "wikilink")[务虚会上提出了](../Page/务虚会.md "wikilink")「[四项基本原则](../Page/四项基本原则.md "wikilink")」。
  - 1979年：[香港政府收回](../Page/香港殖民地時期#香港政府.md "wikilink")[中環](../Page/中環.md "wikilink")[域多利兵房](../Page/域多利兵房.md "wikilink")，而[駐港英軍司令部則搬遷至](../Page/駐港英軍.md "wikilink")[添馬艦](../Page/添馬艦_\(香港\).md "wikilink")[威爾斯親王軍營](../Page/威爾斯親王軍營.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[美国总统](../Page/美国总统.md "wikilink")[罗纳德·威尔逊·里根在](../Page/罗纳德·威尔逊·里根.md "wikilink")[华盛顿希尔顿饭店门前受到精神病患者](../Page/华盛顿哥伦比亚特区.md "wikilink")[约翰·欣克利的](../Page/约翰·欣克利.md "wikilink")[刺杀而受重伤](../Page/雷根遇刺案.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：[荣毅仁辞去](../Page/荣毅仁.md "wikilink")[中信公司董事长](../Page/中国中信集团.md "wikilink")，就任[国家副主席](../Page/中华人民共和国副主席.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[德国](../Page/德国.md "wikilink")[宝马汽车制造公司买下英国](../Page/宝马.md "wikilink")的[劳斯莱斯汽车制造公司](../Page/劳斯莱斯.md "wikilink")。

## 21世紀

[2011年](../Page/2011年.md "wikilink"):[緬甸國防軍大將](../Page/緬甸國防軍.md "wikilink")[丹瑞解散](../Page/丹瑞.md "wikilink")[國家和平與發展委員會](../Page/國家和平與發展委員會.md "wikilink")，把政權移交民選政府。

## 出生

  - [892年](../Page/892年.md "wikilink")：[石敬瑭](../Page/石敬瑭.md "wikilink")，[五代十国時期的](../Page/五代十国.md "wikilink")[后晋开国皇帝](../Page/后晋.md "wikilink")([942年逝世](../Page/942年.md "wikilink"))
  - [1432年](../Page/1432年.md "wikilink")：[穆罕默德二世](../Page/穆罕默德二世_\(奥斯曼帝国\).md "wikilink")，[鄂圖曼帝國蘇丹](../Page/鄂圖曼帝國.md "wikilink")，也被稱為征服者穆罕默德（[1481年逝世](../Page/1481年.md "wikilink")）
  - [1746年](../Page/1746年.md "wikilink")：[弗朗西斯科·戈雅](../Page/弗朗西斯科·戈雅.md "wikilink")，[西班牙画家](../Page/西班牙.md "wikilink")（[1828年逝世](../Page/1828年.md "wikilink")）
  - [1844年](../Page/1844年.md "wikilink")：[保尔·魏尔伦](../Page/保尔·魏尔伦.md "wikilink")，[法国诗人](../Page/法国.md "wikilink")（[1896年逝世](../Page/1896年.md "wikilink")）
  - [1853年](../Page/1853年.md "wikilink")：[梵谷](../Page/梵谷.md "wikilink")，[荷兰画家](../Page/荷兰.md "wikilink")（[1890年逝世](../Page/1890年.md "wikilink")）
  - [1895年](../Page/1895年.md "wikilink")：[蔡和森](../Page/蔡和森.md "wikilink")，[中國共產黨早期的著名領導人之一](../Page/中國共產黨.md "wikilink")（[1931年逝世](../Page/1931年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[织田干雄](../Page/织田干雄.md "wikilink")，[日本田径运动员](../Page/日本.md "wikilink")，[亞洲第一位](../Page/亞洲.md "wikilink")[奧運會個人金牌得主](../Page/奧運會.md "wikilink")（[1998年逝世](../Page/1998年.md "wikilink")）
  - [1926年](../Page/1926年.md "wikilink")：[英格瓦·坎普拉](../Page/英格瓦·坎普拉.md "wikilink")，[宜家家居創辦人](../Page/宜家家居.md "wikilink")（[2018年逝世](../Page/2018年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[曾近榮](../Page/曾近榮.md "wikilink")，香港演員（[2011年逝世](../Page/2011年.md "wikilink")）
  - [1933年](../Page/1933年.md "wikilink")：[董驃](../Page/董驃.md "wikilink")，香港演員（[2006年逝世](../Page/2006年.md "wikilink")）
  - [1945年](../Page/1945年.md "wikilink")：[Eric
    Clapton](../Page/埃里克·克莱普顿.md "wikilink")，著名英国摇滚和布鲁斯吉他手，歌手和作曲家。他是唯一一个三次被引入摇滚名人堂的乐手。
  - [1950年](../Page/1950年.md "wikilink")：[薛家燕](../Page/薛家燕.md "wikilink")，[香港艺人](../Page/香港.md "wikilink")、演員
  - [1967年](../Page/1967年.md "wikilink")：[林原惠](../Page/林原惠.md "wikilink")，日本女性[聲優](../Page/聲優.md "wikilink")、歌手
  - [1968年](../Page/1968年.md "wikilink")：[席琳·狄翁](../Page/席琳·狄翁.md "wikilink")，[加拿大女歌手](../Page/加拿大.md "wikilink")，曾創下每3秒就售出一張專輯的紀錄
  - [1972年](../Page/1972年.md "wikilink")：[黃子佼](../Page/黃子佼.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[主持人](../Page/主持人.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[揚·科勒](../Page/扬·科勒.md "wikilink")，[捷克](../Page/捷克.md "wikilink")[足球選手](../Page/足球.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[小松未步](../Page/小松未步.md "wikilink")，[日本女性創作歌手](../Page/日本.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[川澄綾子](../Page/川澄綾子.md "wikilink")，日本女性聲優
  - 1976年：[Obadele
    Thompson](../Page/奧巴德里·湯普森.md "wikilink")，巴貝多男子田徑運動員，1995年夏季世界大學運動會100公尺亞軍
  - [1979年](../Page/1979年.md "wikilink")：[諾拉·瓊絲](../Page/諾拉·瓊絲.md "wikilink")，[美國](../Page/美国.md "wikilink")[創作歌手](../Page/創作歌手.md "wikilink")、鋼琴家、鍵盤手、吉他手及演員
  - 1979年：[李雨陽](../Page/李雨陽.md "wikilink")，香港演員
  - [1979年](../Page/1979年.md "wikilink")：[何嘉文](../Page/何嘉文.md "wikilink")，台灣女藝人
  - [1980年](../Page/1980年.md "wikilink")：[戴心怡](../Page/戴心怡.md "wikilink")，台灣電視[主播](../Page/主播.md "wikilink")、舞台活動主持人
  - [1981年](../Page/1981年.md "wikilink")：[朴智星](../Page/朴智星.md "wikilink")，[韓國足球員](../Page/韓國.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[田馥甄](../Page/田馥甄.md "wikilink")，S.H.E成員，台灣流行女歌手、演員及綜藝節目主持人
  - [1984年](../Page/1984年.md "wikilink")：[安契奇](../Page/安契奇.md "wikilink")，[克羅埃西亞](../Page/克羅埃西亞.md "wikilink")[網球選手](../Page/網球.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[塞爾希奧·拉莫斯](../Page/塞尔希奥·拉莫斯.md "wikilink")，[西班牙足球選手](../Page/西班牙.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[蕭敬騰](../Page/蕭敬騰.md "wikilink")，台灣男歌手、演員
  - 1987年：[郭建邦](../Page/郭建邦.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")
  - 1987年：[阿米娜·汗](../Page/阿米娜·汗.md "wikilink")，[巴基斯坦女演員及模特兒](../Page/巴基斯坦.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[王琳](../Page/王琳_\(羽毛球运动员\).md "wikilink")，[中國女子](../Page/中國.md "wikilink")[羽毛球運動員](../Page/羽毛球.md "wikilink")
  - 生年不詳：[陸惠玲](../Page/陸惠玲.md "wikilink")，香港女性配音員
  - [1990年](../Page/1990年.md "wikilink")：[李起光](../Page/李起光.md "wikilink")，[韓國歌手](../Page/韓國.md "wikilink")，組合[HIGHLIGHT成員](../Page/HIGHLIGHT.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[謝翔雅](../Page/謝翔雅.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[金志洙](../Page/金志洙.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[宋閔浩](../Page/宋閔浩.md "wikilink")，韓國歌手、組合[WINNER成員](../Page/WINNER.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[島崎遙香](../Page/島崎遙香.md "wikilink")，日本女子偶像團體[AKB48成員](../Page/AKB48.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[車銀優](../Page/車銀優_\(韓國歌手\).md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[ASTRO成員](../Page/ASTRO.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")：[徐子軒](../Page/徐子軒.md "wikilink")，中國女子組合[SNH48成員](../Page/SNH48.md "wikilink")
  - [2002年](../Page/2002年.md "wikilink")：[丁真成](../Page/丁真成.md "wikilink")，韓國男子組合[1THE9成員](../Page/1THE9.md "wikilink")

## 逝世

  - [1925年](../Page/1925年.md "wikilink")：[魯道夫·斯坦納](../Page/魯道夫·斯坦納.md "wikilink")，[奧地利哲學家](../Page/奧地利.md "wikilink")（[1861年出生](../Page/1861年.md "wikilink")）
  - [1949年](../Page/1949年.md "wikilink")：[弗里德里希·貝吉烏斯](../Page/弗里德里希·貝吉烏斯.md "wikilink")，德國化學家，1931年獲[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")（[1884年出生](../Page/1884年.md "wikilink")）
  - [1967年](../Page/1967年.md "wikilink")：[姚敏](../Page/姚敏.md "wikilink")，國語時代曲作曲家（[1917年出生](../Page/1917年.md "wikilink")）
  - [1979年](../Page/1979年.md "wikilink")：[童第周](../Page/童第周.md "wikilink")，中國著名生物學家、实验胚胎学家（[1902年出生](../Page/1902年.md "wikilink")）
  - [1981年](../Page/1981年.md "wikilink")：[德威特·华莱士](../Page/德威特·华莱士.md "wikilink")，[美国](../Page/美国.md "wikilink")《[读者文摘](../Page/读者文摘.md "wikilink")》杂志的创刊人（[1889年出生](../Page/1889年.md "wikilink")）
  - [2002年](../Page/2002年.md "wikilink")：[伊麗莎白·鮑斯-萊昂](../Page/伊麗莎白·鮑斯-萊昂.md "wikilink")，[英國皇太后](../Page/英國.md "wikilink")（[1900年出生](../Page/1900年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[羅張](../Page/羅張.md "wikilink")，前[臺灣](../Page/臺灣.md "wikilink")[警政署長](../Page/內政部警政署.md "wikilink")，陸軍二級上將\[1\]（[1923年出生](../Page/1923年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[高飛](../Page/高飛_\(演員\).md "wikilink")，[香港武打演員](../Page/香港.md "wikilink")（[1952年出生](../Page/1952年.md "wikilink")）

## 节日、风俗习惯

  - ：[土地日](../Page/土地日.md "wikilink")（[1976年](../Page/1976年.md "wikilink")）

## 參考資料

## 外部連結

1.  [臺灣TVBS新聞 - 前警政署長羅張逝世
    享年83歲](http://www.tvbs.com.tw/news/news_list.asp?no=arieslu20060330200524)