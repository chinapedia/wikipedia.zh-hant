**朱健鈞**（****，），[香港人](../Page/香港.md "wikilink")，祖籍[上海](../Page/上海.md "wikilink")，為前[無綫電視及](../Page/無綫電視.md "wikilink")[亞洲電視男藝員](../Page/亞洲電視.md "wikilink")，中學時就讀於[屯門](../Page/屯門區.md "wikilink")[佛教沈香林紀念中學](../Page/佛教沈香林紀念中學.md "wikilink")。中學畢業後參加第一屆寶麗金卡拉OK模特兒大賽獲季軍，其後經模特兒公司鼓勵下，到[TVB為](../Page/TVB.md "wikilink")《[少年五虎](../Page/少年五虎.md "wikilink")》試鏡，憑其酷似[譚詠麟的年輕時的外貌](../Page/譚詠麟.md "wikilink")，以新人的身份擔崗男主角演出《[少年五虎](../Page/少年五虎.md "wikilink")》，飾演譚詠麟一角。\[1\]

## 作品列表

### 电影作品

  - 1995年：《[那有一天不想你](../Page/那有一天不想你.md "wikilink")》（飾演朱亞仔）
  - 1996年：《[玩火](../Page/玩火.md "wikilink")》（飾演三八仔）
  - 2014年：《[硬漢奶爸](../Page/硬漢奶爸.md "wikilink")》
  - 2015年：《[華麗上班族](../Page/華麗上班族.md "wikilink")》（飾演沈凱）
  - 2015年：《[陀地驅魔人](../Page/陀地驅魔人.md "wikilink")》（飾演戀童癖社工）
  - 2016年：《[三人行](../Page/三人行_\(電影\).md "wikilink")》

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 1993年：《[少年五虎](../Page/少年五虎.md "wikilink")》（飾演：譚鉅倫）
  - 1994年：《[開心華之里](../Page/開心華之里.md "wikilink")》（飾演：楊滿月）
  - 1995年：《[包青天之弒父奇案](../Page/包青天_\(無綫電視劇\)#.E5.96.AE.E5.85.83.E5.8D.81.E4.B8.89.EF.BC.9A.E5.BC.91.E7.88.B6.E5.A5.87.E6.A1.88.EF.BC.88.E7.AC.AC61-65.E9.9B.86.EF.BC.89.md "wikilink")》（飾演：雲曉白）
  - 1995年：《[邊緣故事](../Page/邊緣故事.md "wikilink")》（飾演：邦）
  - 1995年：《[廉政英雌之火槍柔情](../Page/廉政英雌之火槍柔情.md "wikilink")》（飾演：徐正安Stephen）
  - 1995年：《[新同居關係](../Page/新同居關係.md "wikilink")》（飾演：梁立志）
  - 1997年：《[圓月彎刀](../Page/圓月彎刀.md "wikilink")》（飾演：[荊無命](../Page/荊無命.md "wikilink")）
  - 1999年：《[命轉情真](../Page/命轉情真.md "wikilink")》（飾演：小鬼司爺）
  - 1999年：《[金玉滿堂](../Page/金玉滿堂.md "wikilink")》（飾演：小初子）
  - 2000年：《[無業樓民](../Page/無業樓民.md "wikilink")》（飾演：Charles）
  - 2000年：《[妙手仁心II](../Page/妙手仁心II.md "wikilink")》（飾演：李若愚）
  - 2000年：《[京城教一](../Page/京城教一.md "wikilink")》（飾演：方丈）
  - 2001年：《[美味情緣](../Page/美味情緣.md "wikilink")》（飾演：助廚）
  - 2001年：《[封神榜](../Page/封神榜_\(2001年電視劇\).md "wikilink")》（飾演：姜環）
  - 2001年：《[皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink")》（飾演：阿旺）
  - 2001年：《[勇探實錄](../Page/勇探實錄.md "wikilink")》（飾演：趙志豪）
  - 2002年：《[撲水冤家](../Page/撲水冤家.md "wikilink")》（飾演:大義）
  - 2003年：《[衛斯理](../Page/衛斯理_\(無綫電視劇集\).md "wikilink")》（飾演：小展）
  - 2004年：《[廉政行動2004](../Page/廉政行動2004.md "wikilink")》（飾演：Joe）
  - 2004年：《[青出於藍](../Page/青出於藍_\(電視劇\).md "wikilink")》（飾演：郭志勤）
  - 2007年：《[鄭板橋](../Page/鄭板橋_\(電視劇\).md "wikilink")》（飾演：鄭墨）

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 2007年：《[靈舍不同](../Page/靈舍不同.md "wikilink")》（飾演：溫文濤）
  - 2008年：《[十六不搭喜趣來](../Page/十六不搭喜趣來.md "wikilink")》（飾演：韓源志）
  - 2008年：《[今日法庭](../Page/今日法庭.md "wikilink") - 消防員殺人勒索》（飾演：警察）
  - 2008年：《[今日法庭](../Page/今日法庭.md "wikilink") - 消防員殺人勒索2》（飾演：警察）
  - 2008年：《[今日法庭](../Page/今日法庭.md "wikilink") - 正義之師》（飾演：學生資助辦事署主任）
  - 2008年：《[今日法庭](../Page/今日法庭.md "wikilink") - 失明保險騙案》（飾演：失明騙徒）
  - 2008年：《[今日法庭](../Page/今日法庭.md "wikilink") - 失明保險騙案2》（飾演：失明騙徒）

### 電視電影（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 1995年：《[夜之女](../Page/夜之女.md "wikilink")》（飾演：達）
  - 2003年：《[熱血追擊](../Page/熱血追擊.md "wikilink")》（飾演：輝手下）

### 香港電台劇集

  - 2003年：香港電台《[非常平等任務](../Page/非常平等任務.md "wikilink")》（飾演：阿棠）
  - 2009年：香港電台《[毒海浮生](../Page/毒海浮生.md "wikilink")》（飾演：訓導主任）

### 中國大陸劇集

  - 2005年：《[春蠶織夢](../Page/春蠶織夢.md "wikilink")（[歡樂桑田](../Page/歡樂桑田.md "wikilink")）》（飾演：陳家寶）
  - 2011年：《[一觸即發](../Page/一觸即發.md "wikilink")》（飾演：夏躍春院長）
  - 2017年：《[孤芳不自賞](../Page/孤芳不自賞.md "wikilink")》（飾演：晉太尉謝恒）

### 馬來西亞劇集

  - 1998年:《星輝閃閃OFF-LINE》

### 亞視電視節目

  - 2008年:《[方草尋源III](../Page/方草尋源III.md "wikilink")》

### 台灣電視節目

  - 1997年:《綜藝星期天》之"龍虎兄弟"

### MV

  - [Beyond](../Page/Beyond.md "wikilink")：《[無語問蒼天](../Page/無語問蒼天.md "wikilink")》
  - [Beyond](../Page/Beyond.md "wikilink")：《[命運是你家](../Page/命運是你家.md "wikilink")》
  - [張學友](../Page/張學友.md "wikilink")：《[日出時讓戀愛終結](../Page/日出時讓戀愛終結.md "wikilink")》
  - [黎瑞恩](../Page/黎瑞恩.md "wikilink")：《[雨季不再來](../Page/雨季不再來.md "wikilink")》
  - [鍾漢良](../Page/鍾漢良.md "wikilink")：《[一千種不放心](../Page/一千種不放心.md "wikilink")》

### 旅遊特輯

  - 1995年：康泰最緊要好玩之南非、-{津巴布韋}-特輯
  - 1996年：寰宇風情 瑞士特輯
  - 1999年：新華旅遊反斗俱樂部：紐西蘭
  - 情迷地中海
  - 麗星郵輪雙魚星號之海南三亞
  - 南非點解咁好玩
  - 北美點解咁好玩

### 廣告

  - 麗星郵輪雙魚星號

### 曾任

  - 藝能唱片公司主辦台語歌比賽表演嘉賓(高雄)
  - 小城巨星挑戰賽表演嘉賓
  - 油尖區交通安全青年大使
  - 沙田區交通安全青年大使

## 參考

## 外部連結

  - [搜刮娛樂圈失蹤人口 朱健鈞有新戲拍 - Yahoo
    娛樂圈2014年7月20日](https://hk.lifestyle.yahoo.com/%E6%90%9C%E5%88%AE%E5%A8%9B%E6%A8%82%E5%9C%88%E5%A4%B1%E8%B9%A4%E4%BA%BA%E5%8F%A3-%E6%9C%B1%E5%81%A5%E9%88%9E%E6%9C%89%E6%96%B0%E6%88%B2%E6%8B%8D-031448860.html)
  - [《華麗上班族》辦“入職” -
    深圳新聞網2014-04-30](https://web.archive.org/web/20140728033342/http://www.sznews.com/ent/content/2014-04/30/content_9432139.htm)
  - [港男星胡枫收14个干儿子干女儿 张学友黎明在列 -
    新華網2013年6月15日](http://news.xinhuanet.com/gangao/2013-06/15/c_124858405.htm)
  - [朱健鈞 Mickey Chu粉絲專頁](http://www.facebook.com/mickeychukinkwan/)

[KIN](../Category/朱姓.md "wikilink")
[Category:佛教沈香林紀念中學校友](../Category/佛教沈香林紀念中學校友.md "wikilink")
[Category:香港專業教育學院校友](../Category/香港專業教育學院校友.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:香港電台](../Category/香港電台.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港男性模特兒](../Category/香港男性模特兒.md "wikilink")

1.  [1](https://www.hk01.com/%E5%A8%9B%E6%A8%82/32509/-%E4%B8%89%E4%BA%BA%E8%A1%8C-%E6%9D%9CSir%E7%99%BC%E5%8A%9F-%E5%B0%91%E5%B9%B4%E4%BA%94%E8%99%8E-%E9%8D%BE%E6%BC%A2%E8%89%AF%E6%9C%B1%E5%81%A5%E9%88%9E%E9%86%AB%E9%99%A2-%E8%81%9A%E9%A6%96-)