**莫尔多瓦共和国**（；[莫克沙語](../Page/莫克沙語.md "wikilink")：；[厄尔兹亚语](../Page/厄尔兹亚语.md "wikilink")：）位於[東歐平原東部](../Page/東歐平原.md "wikilink")，是[俄羅斯聯邦主體之一](../Page/俄羅斯聯邦主體.md "wikilink")，屬[伏爾加聯邦管區](../Page/伏爾加聯邦管區.md "wikilink")。面積26,200平方公里，人口888,766（2002年）/834,755（2010年）。當中[莫尔多维亚人佔](../Page/莫尔多维亚人.md "wikilink")32%。俄語、莫克沙語和厄爾茨亞語同為官方語言。首府[薩蘭斯克](../Page/薩蘭斯克.md "wikilink")。

莫尔多瓦與[摩爾多瓦](../Page/摩爾多瓦.md "wikilink")（）或其俄語名稱摩爾達維亞（）是兩個完全不同的概念。

## 历史

### 早期历史

莫尔多瓦地区人类最早的考古学迹象来自[新石器时代](../Page/新石器时代.md "wikilink")。芬兰-乌戈尔[莫尔多维亚人在](../Page/莫尔多维亚人.md "wikilink")6世纪的书面来源中被提及。后来，莫尔多维亚人受到[伏尔加保加利亚和](../Page/伏尔加保加利亚.md "wikilink")[基辅罗斯的影响](../Page/基辅罗斯.md "wikilink")。莫尔多维亚王子有时袭击了穆罗马和伏尔加保加利亚，经常掠夺对方的财物。

## 人口

### 民族構成

莫尔多瓦人是一个芬兰语族，讲两种相关语言，[莫克沙语和厄尔兹亚语](../Page/莫克沙语.md "wikilink")。莫尔多维亚人认为自己是独立的民族：\[1\]厄尔兹亚人和[莫克沙人](../Page/莫克沙人.md "wikilink")。在所有使用莫尔多维亚语的人中，只有三分之一的人住在莫尔多维亚共和国。在苏联时期，每种语言都出版了学校教科书。\[2\]

|                                       | 2002統計         | 2010統計         |
| ------------------------------------- | -------------- | -------------- |
| [俄羅斯族](../Page/俄羅斯族.md "wikilink")    | 54 0717（60.8%） | 44 3737（53.4%） |
| [莫尔多瓦族](../Page/莫爾多維亞人.md "wikilink") | 28 3861（31.9%） | 33 3112（40.0%） |
| [韃靼族](../Page/韃靼族.md "wikilink")      | 4 6261（5.2%）   | 4 3392（5.2%）   |
| [乌克兰族](../Page/乌克兰族.md "wikilink")    | 4801（0.5%）     | 4801（0.5%）     |
|                                       |                |                |

## 注释

## 参考资料

[莫爾多瓦共和國](../Category/莫爾多瓦共和國.md "wikilink")
[Category:伏爾加聯邦管區](../Category/伏爾加聯邦管區.md "wikilink")

1.  [Encyclopædia
    Britannica](http://www.britannica.com/EBchecked/topic/391993/Mordvin-language)
2.  Barbara A. Anderson and Brian D. Silver, "Equality, Efficiency, and
    Politics in Soviet Bilingual Education Policy, 1934-1980," *American
    Political Science Review* 78 (December 1984): 1019-1039.