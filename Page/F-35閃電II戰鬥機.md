**F-35閃電II式**（）是一款由[美国](../Page/美国.md "wikilink")[洛克希德·馬丁公司设计及生产的单座单发动机](../Page/洛克希德·馬丁公司.md "wikilink")[三軍通用](../Page/三軍.md "wikilink")[多用途戰機](../Page/多用途戰機.md "wikilink")，[劳斯莱斯股份有限公司參與了引擎設計](../Page/劳斯莱斯股份有限公司.md "wikilink")\[1\]，是遠、近距離空對空戰鬥能力僅次於[F-22的戰鬥機種](../Page/F-22.md "wikilink")\[2\]，2011年7月14日，首架F-35A正式交付美国空军開始服役。開發廠商洛克希德·馬丁以[X-35验证机競標](../Page/X-35验证机.md "wikilink")[聯合攻擊戰鬥機計畫](../Page/聯合攻擊戰鬥機計畫.md "wikilink")（JSF）並獲選成為續存設計，進而開發出F-35。此機種主要用於[密接支援](../Page/密接支援.md "wikilink")、目标[轰炸](../Page/轰炸.md "wikilink")、防空截击等多种任务，並因此發展出3种主要的衍生版本，包括采用传统[跑道起降的F](../Page/跑道.md "wikilink")-35A，短距離起飛／[垂直起降机种F](../Page/垂直起降.md "wikilink")-35B，與作為[航空母舰舰载机的F](../Page/舰载机.md "wikilink")-35C。

F-35属于具有[匿蹤設計的](../Page/低可偵測性技術.md "wikilink")[第五代戰鬥機](../Page/第五代戰鬥機.md "wikilink")，作戰半徑超過1,000公里，具備有限的類[超音速巡航能力](../Page/超音速巡航.md "wikilink")。\[3\]
F-35將是美國和其盟國在21世紀的空戰主力，美国[空軍](../Page/美國空軍.md "wikilink")、[海軍](../Page/美國海軍.md "wikilink")、[海軍陸戰隊總共將裝備](../Page/美國海軍陸戰隊.md "wikilink")2,443架\[4\]，取代[F-16系列](../Page/F-16.md "wikilink")、[A-10攻擊機](../Page/A-10攻擊機.md "wikilink")、[F/A-18系列](../Page/F/A-18.md "wikilink")、[AV-8B等](../Page/AV-8B.md "wikilink")，其他共同聯合研發國家則裝備710架。[英国](../Page/英国.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[澳大利亚](../Page/澳大利亚.md "wikilink")、[日本](../Page/日本.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[丹麦](../Page/丹麦.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[以色列等均参与研发并可能装备](../Page/以色列.md "wikilink")。\[5\]\[6\]\[7\]\[8\]\[9\]\[10\]

## 命名

當初[JSF計畫是採](../Page/联合打击战斗机计划.md "wikilink")[實驗機](../Page/X-飛機.md "wikilink")（X）競標，而非原型實驗機（XF），得標時尚未賦予戰機正式編號，照現行《[美軍航空器命名系統](../Page/1962美國三軍航空器命名系統.md "wikilink")》，流水號應在[F-22](../Page/F-22猛禽戰鬥機.md "wikilink")/[23之後為F](../Page/YF-23戰鬥機.md "wikilink")-24/25。偏偏在2001年10月26日宣布[X-35得標記者會上長官個個狀況外](../Page/X-35验证机.md "wikilink")，主持的國防部官員Edward
Aldridge就戰機編號被記者問倒，求助在場的空軍部長James
Roche，部長不知所措又問一旁JSF計畫主持人[海陸Michael](../Page/美国海军陆战队.md "wikilink")
Hough少將，少將回答「F-35」，部長等人隨之附和；記者質疑編號是如何決定、哪來的，少將及主持官員則含混帶過<ref>命名「F-35」相關資訊：

  -
  -
  - </ref>。即使決策過程草率，但已昭告周知就此定案。

2006年7月7日，美國空軍宣布其名稱「閃電II」以承繼[P-38閃電](../Page/P-38閃電式戰鬥機.md "wikilink")\[11\]、也呼應英國[閃電戰鬥機的威名](../Page/閃電戰鬥機.md "wikilink")\[12\]。

## 设计

F-35外型像似[F-22猛禽戰鬥機的單引擎缩小版](../Page/F-22猛禽戰鬥機.md "wikilink")，而且它的確從中吸取了一些元素。F-35B的「三軸旋管噴嘴」（the
three-bearing swivel duct
nozzle，3BSD）架構，則從[康維爾在](../Page/康維爾.md "wikilink")1972年6月設計Model
200機型方案所得到的靈感，並購自俄國[Yak-141性能及少量設計數據以供](../Page/Yak-141.md "wikilink")[X-35參考](../Page/X-35.md "wikilink")\[13\]。

F-35與它前一代戰機相比有如下進步：

  - 廉價耐用的隱身技術；
  - 综合的航電設備與感應器融合可以结合從機載與非機載的感應器得到的訊息。這樣不但可以增加駕駛員的狀況感知，目標識別與武器投射的能力，還可快速地傳輸訊息到其他的指揮及控制（C2）節點；
  - 包含[IEEE-1394b與](../Page/IEEE_1394.md "wikilink")[光纖的數據交換網路](../Page/光纖.md "wikilink")；
  - 較低的维護成本；

[頭盔顯示器已經整合到了像](../Page/頭盔顯示器.md "wikilink")[JAS
39](../Page/JAS_39獅鷲戰鬥機.md "wikilink")、“[幼狮](../Page/幼狮战斗机.md "wikilink")”這样的第四代戰機上，而F-35已經利用頭盔顯示器完全替代[抬頭顯示器的戰機](../Page/抬頭顯示器.md "wikilink")，例如聯合頭盔顯示系統（Joint
Helmet Mounted Cueing System，JHMCS）。

### 配備

  - APG-81主動陣列雷達（單元模組天線1676個）。\[14\]

<!-- end list -->

  - 「分散式孔徑系統」（Distributed Aperture System,
    DAS），環繞飛機前後上下裝設了6個紅外線感應裝置，即時提供駕駛員接近己方的飛行體資料。F-35駕駛員配戴的嵌入顯示幕的頭盔，可以從分散式孔徑系統中蒐集到全方位的目標資料。

<!-- end list -->

  - 無邊界層隔板超音速進氣道（Divert less Supersonic Inlet,
    DSI）可以改善飛機的隐身特性，並有利於進氣道-機身一體化設計。取消了附面層隔道、放氣系統和旁通系統，減少結構重量和降低了生產和使用費用，惟會降低高马赫数进气效率。

### 推重比

早期的F-35B設計重量比项目预期的重量超出3000磅，導致飛機的推重比下降，使該機型在航程、速度、機動性等各項指標均不合格。洛克希德·馬丁公司為了解決“過重”而导致推重比下降的問題，缩薄了機身蒙皮，缩小了武器倉與垂尾，內部管線配置，重新設計了機翼構造與航電系统等，终於缩减了2205磅的重量。

### Z-13塗裝

F-35 原本使用的塗裝是灰色鋸齒狀的網格，這種像窗櫺般的網格覆蓋 F-35
的大部分機體，從遠處看起來就像是在機身上打了一層補丁一樣。以往生產這種符合產品生產要求以及採用「補丁」塗裝的
F-35 戰機時，需要耗費大量的勞動力和時間，在製作過程中要非常注重細節，因為複雜的塗裝問題等其他原因，使 F-35
最初的製作時間和成本超過原定預算的兩倍。為了改變這種狀況，「Z13」塗裝應運而生。這種被稱作「Z13」
的新塗裝自2012年開始研發，2017年開始投入使用，首次嘗試使用「Z13」塗裝的是編號為 14-5103（AF-104）的 F-35
戰機（近日網路上最新曝光在「洛克希德‧馬丁」公司廠房內為澳洲生產的第三架 F-35A 也同樣採用了「Z13」塗裝。）。

目前，F-35 的新塗裝所佔比例正在逐漸提升，這種塗裝除了節約最後組裝階段的時間以及精力、提高 F-35
組裝工廠的生產效率並降低生產成本外，也減少戰機出現瑕疵返廠修復所需的工時。與原有塗裝在視覺上的主要區別是，使用了「Z13」塗裝的
F-35 戰機表面塗層顯得更加光滑和均勻，看起來也更加美觀。\[15\]

### 與F-22比較

F-35雖為[F-22的低階輔助機種](../Page/F-22.md "wikilink")。然而因後發優勢，像航電系統，甚至飛機結構隱形（特別是低可偵測[DSI進氣道設計](../Page/DSI進氣道.md "wikilink")）反而都比[F-22還先進](../Page/F-22.md "wikilink")。

## 試飛

第一架進行試飛的F-35是空軍型F-35A，編號AA-1。試飛時間是2006年12月15日在[德州](../Page/德州.md "wikilink")[沃斯堡展開](../Page/沃斯堡.md "wikilink")。

第二架F-35A（編號AF-1）於2009年1月14日進行第一次試飛。

第三架F-35A（編號AF-2）於2010年4月20日試飛。

## 子型号

[F-35_Lightning_II_variants_in_flight_near_Eglin_AFB_in_2014.jpg](https://zh.wikipedia.org/wiki/File:F-35_Lightning_II_variants_in_flight_near_Eglin_AFB_in_2014.jpg "fig:F-35_Lightning_II_variants_in_flight_near_Eglin_AFB_in_2014.jpg")的风扇，座舱玻璃较短。F-35C的机翼比F-35A大，并且可以折叠，另外雖然沒看見起落架，但為了上艦在短距起落架的部分還是有差別的。\]\]

雖然只有F-35B才算是垂直起降型，但相對於傳統的戰鬥機，即使是A和C型的起降距離都算短距起降的。另外，A、B、C三型結構差異比外表來得大\[16\]。

[美國國防部與](../Page/美國國防部.md "wikilink")[洛克希德·馬丁於](../Page/洛克希德·馬丁.md "wikilink")2013年5月31日交付給國會一份IOC報告指出，F-35三種版本在[初始作戰能力](../Page/初始作戰能力.md "wikilink")（Initial
Operational Capability, IOC）測試結束之時間分別為：\[17\] \[18\]

  - F-35A：目標時間訂為2016年8月至同年12月間
  - F-35B：目標時間訂為2015年7月至同年12月間
  - F-35C：目標時間訂為2018年8月至2019年2月間

### F-35A

F-35A，為[美國空軍與其他國使用型號](../Page/美國空軍.md "wikilink")，屬傳統起降型。價值1.116億美元，[F-16和](../Page/F-16戰隼戰鬥機.md "wikilink")[A-10的後繼機種](../Page/A-10雷霆二式攻擊機.md "wikilink")，[F-22猛禽式戰鬥機的輔助型號](../Page/F-22猛禽戰鬥機.md "wikilink")。

2016年8月2日[美國空軍](../Page/美國空軍.md "wikilink")[空戰司令部司令](../Page/空戰司令部.md "wikilink")[赫伯特·卡萊爾上將宣佈](../Page/赫伯特·卡萊爾.md "wikilink")，美國空軍首支具備12－24架F-35A的作戰中隊具備初始作戰能力\[19\]。

2017年1月23日至2月10日舉行的第17-1號[紅旗演習](../Page/紅旗演習.md "wikilink")，駐紮的美國空軍第338戰機聯隊與及[預備役第](../Page/預備役.md "wikilink")419戰機聯隊的F-35A首次參與，演習期間共執行了226次原定飛行架次中的207架次飛行任務，妥善率達90%。空戰表現共擊落敵機145架，7架F-35A皆在視距內被擊落，經換算後交換比為1:20。對地方面成功執行了壓制敵方[防空系統的任務](../Page/防空系統.md "wikilink")，對51座中的49座[地對空飛彈投下了模擬彈藥](../Page/地對空飛彈.md "wikilink")\[20\]\[21\]\[22\]。

2017年4月15日隸屬於[美國空軍](../Page/美國空軍.md "wikilink")[希爾空軍基地第](../Page/希爾空軍基地.md "wikilink")388戰機聯隊第34戰機中隊及預備役第419戰機聯隊第466戰機中隊的F-35A，橫越[大西洋前往](../Page/大西洋.md "wikilink")[英國的](../Page/英國.md "wikilink")參與訓練任務，成為F-35A自宣佈具備初始作戰能力後首次的境外佈署及跨越大西洋至歐洲的訓練佈署紀錄\[23\]\[24\]。

### F-35B

F-35B，[美國海軍陸戰隊及](../Page/美國海軍陸戰隊.md "wikilink")[英國皇家海軍採用的型號](../Page/英國皇家海軍.md "wikilink")，是垂直／短場起降型。單座設計，搭載3台電腦。两级对转升力风扇是F136发动机之外新增加的装置，是F-35B动力系统的重要组成部分。它安装在驾驶舱后部，可提供44.5千牛的附加推力，所以使主发动机能在较低温度下以较小的负荷运转，从而提高了可靠性和使用寿命。F-35B的垂直升力主要靠机上装置的两级对转升力风扇提供，它的进气道自然就可以设计得比较小。價值1.094億美元，為[AV-8B獵鷹式垂直](../Page/AV-8攻擊機.md "wikilink")／短場起降型戰鬥機的後繼機。可以進行[垂直起降的F](../Page/垂直起降.md "wikilink")-35B（編號BF-1）於2008年6月11日進行第一次試飛，不過起飛的過程仍是採用傳統的滑行方式。BF-1同時也是19架系統發展與展示（System
Development and Demostration）機組當中的第二架，以及第一架採用重量最佳化生產程序的飛機。

<File:F-35B> Joint Strike Fighter (thrust vectoring nozzle and lift
fan).PNG|F-35B（[推力向量](../Page/推力向量.md "wikilink")）垂直起降動力解剖圖 <File:F-35>
compilation.ogg|F-35B垂直起降 <File:F-35> vertical landing.ogg|F-35B垂直起降
<File:F-35B> Lighting II training flights
170203-M-VF398-0001.jpg|thumb|美國F-35B進駐日本[岩國航空基地](../Page/岩國航空基地.md "wikilink")

第二架F-35B（編號BF-2）於2009年2月25日試飛。

第三架F-35B（編號BF-3）於2010年2月2日試飛。

第四架F-35B（編號BF-4）於2010年4月7日試飛。

[2011年1月](../Page/2011年1月.md "wikilink")，美国防部长[罗伯特·盖茨宣布将进行为期两年的F](../Page/罗伯特·盖茨.md "wikilink")-35B生产暂停，以期重新设计以提高表现，如果不成功将会取消该子型号。\[25\]

洛克希德·馬丁副总裁[Tom
Burbage曾说大部分的F](../Page/Tom_Burbage.md "wikilink")-35发展延误都是由于B型造成的。\[26\]而陆战队打算先一步不经足够的验收部署F-35B初步作战能力。<ref>

`Majumdar, Dave. "U.S. Military May Deploy F-35 Before Formal IOC." Defense News, 24 May 2011. `</ref>

2012年1月20日，继任盖茨的[Leon
Panetta称](../Page/Leon_Panetta.md "wikilink")“所有人都相信F-35B已取得了足够的进展”而恢复生产计划。\[27\]

2012年8月8日，1架F-35B試驗機BF-3完成首次空中武器投放。BF-3於馬里蘭・大西洋測試區從內部武器艙試驗投放了1枚1000磅的GBU-32[聯合直接攻擊彈藥](../Page/聯合直接攻擊彈藥.md "wikilink")（JDAM）。\[28\]

2016年11月18－20日[美國海軍陸戰隊進行F](../Page/美國海軍陸戰隊.md "wikilink")-35B出海登艦測試，將12架F-35B搭載於[美利堅號兩棲攻擊艦上](../Page/美利堅號兩棲攻擊艦.md "wikilink")，驗證[閃電航母](../Page/閃電航母.md "wikilink")(Lighting
carrier)概念，測試目的為了解F-35B的開發程度及如何將F-35B融入海軍陸戰隊與海軍的作戰結構之中。（VMX-1）指揮官Colonel
George
Rowell上校指美利堅號兩棲攻擊艦原計劃搭載F-35B的上限為20架，認為可增加搭載至22架F-35B使能為（MAGTF）提供最大的空中支援能力\[29\]。

### F-35C

[F-35C_landing_on_USS_Nimitz_(CVN-68)_in_November_2014_(01).JPG](https://zh.wikipedia.org/wiki/File:F-35C_landing_on_USS_Nimitz_\(CVN-68\)_in_November_2014_\(01\).JPG "fig:F-35C_landing_on_USS_Nimitz_(CVN-68)_in_November_2014_(01).JPG")上進行測試\]\]
[F-35C_First_Carrier_Landing_1.webm](https://zh.wikipedia.org/wiki/File:F-35C_First_Carrier_Landing_1.webm "fig:F-35C_First_Carrier_Landing_1.webm")\]\]
[F-35_hovering.webm](https://zh.wikipedia.org/wiki/File:F-35_hovering.webm "fig:F-35_hovering.webm")
F-35C，[美國海軍考慮使用的型號](../Page/美國海軍.md "wikilink")，屬於艦載型。為確保低速時的安全性，主翼及垂直尾翼的面積加大。两翼可折叠。用於在傳統起降型航空母艦起降的尾勾給強化了。價值1.429億美元。為[F-18的後繼機種](../Page/F/A-18黃蜂式戰鬥攻擊機.md "wikilink")，而燃料搭載量還要比[F-22猛禽戰鬥機更大](../Page/F-22猛禽戰鬥機.md "wikilink")。海軍艦載型F-35C於2010年6月6日在沃斯堡進行第一次飛行。這架編號CF-01的F-35C一共飛行57分鐘，飛行員是洛克希德-馬丁公司的試飛員Jeff
Knowles，Jeff同時也擔任過[F-117戰鬥機的首席試飛員](../Page/F-117戰鬥機.md "wikilink")，翼面積和尾翼面積比A/B型大，將裝備在[福特級核動力航空母艦上](../Page/福特級核動力航空母艦.md "wikilink")。

CF-01是在2009年7月28日出廠，然而一些組件運交延遲，加上設計方面的修改，導致第一次試飛的時間從2009年底延後到2010年6月。初期試飛計畫包括15次飛行，接著進行結構偶合和震動測試。接著在2010年10月會將CF-01送到[馬里蘭州帕特森河海軍基地與F](../Page/馬里蘭州.md "wikilink")-35B一同進行後續的試飛工作。\[30\]

2016年8月23日（VX-23）及帕德森河整合測試組，於[喬治·華盛頓號航空母艦上完成F](../Page/喬治·華盛頓號航空母艦.md "wikilink")-35C的第三期開發測試(DT-III)，測試進行了41飛行架次完成所有的測試項目，測試比預期早一週完成，測試中發現當F-35C輕掛載彈射起飛時會遭遇突然且猛烈的震動，引致飛行員頭盔顯示器(HMD)以及氧氣面罩移位，導致飛行員升空後無法判讀頭盔內的投影資訊需飛行員自行調整後才能正常閱讀\[31\]\[32\]\[33\]。

### F-35I

[IAF-F-35I-2016-12-13.jpg](https://zh.wikipedia.org/wiki/File:IAF-F-35I-2016-12-13.jpg "fig:IAF-F-35I-2016-12-13.jpg")的F-35I首飛，2016年12月13日\]\]

F-35I，為[以色列空軍所使用的型號](../Page/以色列空軍.md "wikilink")，暱稱為ADIR，[希伯來文的意思為](../Page/希伯來文.md "wikilink")「全能者」。機體原型為F-35A，內部航電系統則由[洛克希德馬丁出廠時加裝](../Page/洛克希德馬丁.md "wikilink")[以色列航太工業公司所開發的專用C](../Page/以色列航太工業.md "wikilink")4I模組，加強並獨立於美國且先進的指管資訊處理能力;電戰系統亦加裝由[埃爾比特系統公司](../Page/埃爾比特系統公司.md "wikilink")
(Elbit Systems)開發的Elisra電戰系統;
武器及作戰支援系統方面也經修改整合至能夠使用以色列自製的各式武器系統\[34\]\[35\]\[36\]。

## 國際參與

[參與**聯合攻擊戰鬥機計劃**的國家：
](https://zh.wikipedia.org/wiki/File:F-35_potential_buyers_tc.png "fig:參與聯合攻擊戰鬥機計劃的國家：      ")

雖然美國是主要的購買國與資金提供者，但英國、意大利、荷蘭、加拿大、挪威、丹麥、澳洲和土耳其也為開發計劃提供了43.75億美元經費。總開發經費預估將超過400億美元，主要由美國買單，購買2400架戰機預計將另外再花費美國2000億美元\[37\]。九個主要參與國計畫在2035年前取得超過3,100架F-35\[38\]。2009年，以色列、土耳其和新加坡也在就採購F-35與美國進行談判。進一步的樂觀估計、F-35型戰機未來的總銷售量將會突破6千架，使F-35成為數量最多的噴射戰鬥機之一\[39\]。

JSF國際參與分為三級，等級大致反應出對此計畫的財務支援、轉移的科技數量、可競標的分包合約和國家取得飛機的順序。英國是唯一的「一級」合夥人，貢獻25億美元，約研發經費的10%根據1995年英美簽定使英國加入此計畫的[瞭解備忘錄](../Page/瞭解備忘錄.md "wikilink")\[40\]\[41\]。「二級」合夥人是[義大利](../Page/義大利.md "wikilink")，貢獻10億美元，和[荷蘭](../Page/荷蘭.md "wikilink")，八億美元。「三級」合夥人是[加拿大四億四千萬](../Page/加拿大.md "wikilink")，[土耳其一億七千五百萬](../Page/土耳其.md "wikilink")，[澳洲一億四千四百萬](../Page/澳洲.md "wikilink")，[挪威一億二千二百萬和](../Page/挪威.md "wikilink")[丹麥一億一千萬](../Page/丹麥.md "wikilink")。[以色列與](../Page/以色列.md "wikilink")[新加坡是安全合作成員](../Page/新加坡.md "wikilink")。\[42\]

一些合夥國家對JSF計畫的公開支持出現動搖，並暗示或威脅除非他們穫得更多分包合約或技術轉移，他們會放棄JSF轉採用[颱風戰鬥機](../Page/颱風戰鬥機.md "wikilink")、[獅鷲戰鬥機](../Page/JAS_39獅鷲戰鬥機.md "wikilink")、[達梭飆風戰鬥機或干脆僅升級他們現有的飛機](../Page/陣風戰鬥機.md "wikilink")。挪威已多次威脅會停止他們的支援除非顯著增加他們的工業配額，儘管如此挪威仍簽署了所有瞭解備忘錄，包括最近一次關於JSF計畫未來生產階段細節的備忘錄，不過，他們也表明，他們會強化與JSF對手，颱風和獅鷲的合作。\[43\]

<table>
<thead>
<tr class="header">
<th><p>合作國</p></th>
<th><p>購買量</p></th>
<th><p>型號</p></th>
<th><p>預計服役年</p></th>
<th><p>變動</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><br />
<a href="../Page/澳洲皇家空軍.md" title="wikilink">澳洲皇家空軍</a><br />
<a href="../Page/澳洲皇家海軍.md" title="wikilink">澳洲皇家海軍</a></p></td>
<td><p>72</p></td>
<td><p>F-35A</p></td>
<td><p>2018</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>80</p></td>
<td><p>F-35A</p></td>
<td><p>2017</p></td>
<td><p>取消採購</p></td>
</tr>
<tr class="odd">
<td><p><br />
<a href="../Page/美國空軍.md" title="wikilink">美國空軍</a><br />
<a href="../Page/美國海軍.md" title="wikilink">美國海軍</a><br />
<a href="../Page/美國海軍陸戰隊.md" title="wikilink">美國海軍陸戰隊</a></p></td>
<td><p><br />
1763<br />
340<br />
340/80</p></td>
<td><p><br />
F-35A<br />
F-35C<br />
F-35B/C</p></td>
<td><p><br />
2013<br />
2012<br />
2016/2019</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/以色列空軍.md" title="wikilink">以色列空軍</a></p></td>
<td><p>50</p></td>
<td><p>F-35I</p></td>
<td><p><br />
2016</p></td>
<td><p>+50架選購</p></td>
</tr>
<tr class="odd">
<td><p><br />
<a href="../Page/義大利空軍.md" title="wikilink">義大利空軍</a><br />
<a href="../Page/義大利海軍.md" title="wikilink">義大利海軍</a></p></td>
<td><p><br />
60<br />
15<br />
</p></td>
<td><p>F-35A<br />
F-35B<br />
</p></td>
<td><p><br />
2015<br />
2015</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>48</p></td>
<td><p>F-35A</p></td>
<td><p>2015</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>85</p></td>
<td><p>F-35A</p></td>
<td><p>2016</p></td>
<td><p>+15架選購</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>138</p></td>
<td><p>F-35B</p></td>
<td><p>2015</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>116→50</p></td>
<td><p>F-35A</p></td>
<td><p>2014</p></td>
<td><p>削減一半</p></td>
</tr>
<tr class="even">
<td><p><br />
<a href="../Page/航空自衛隊.md" title="wikilink">航空自衛隊</a></p></td>
<td><p>42</p></td>
<td><p>F-35A</p></td>
<td><p>2019</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>40(+20)</p></td>
<td><p>F-35A</p></td>
<td><p>2018-2021</p></td>
<td><p>增購20架F-35[44]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 英國

[英國計劃為](../Page/英國.md "wikilink")[英國皇家空軍和](../Page/英國皇家空軍.md "wikilink")[英國皇家海軍](../Page/英國皇家海軍.md "wikilink")採買各型F-35。

英國越來越對美國缺乏意願授權英國可無須依賴美國獨自維修和升級它的F-35的技術感到洩氣，據了解這主要是關於此飛機的軟體。在長達五年的時間，英國官員尋求一項軍武管制科技讓渡以獲得更多技術轉移，這個要求，雖有布希政府加持，但履次被美國聯邦眾議院外委會主席[亨利·海德](../Page/亨利·海德.md "wikilink")（Henry
Hyde）眾議員阻擋，他認為英國必須加強關於轉移美國最先進技術給第三方的法律。\[45\]

BAE系統[總裁](../Page/總裁.md "wikilink")[麥可·透納](../Page/麥可·透納.md "wikilink")（Mike
Turner）抗議美國拒絕讓他公司取得此飛機的[原始碼](../Page/原始碼.md "wikilink")。在2005年11月21日，《[格拉斯哥先驅報](../Page/格拉斯哥先驅報.md "wikilink")》一篇文章引述下議院國防委員會主席言論說：「若未獲授權取得飛控軟體原始碼，英國可能必須考慮是否繼續此計畫」\[46\]。[保羅·德瑞森](../Page/保羅·德瑞森.md "wikilink")（Paul
Drayson）上議員，[英國軍購局局長](../Page/英國軍購局.md "wikilink")，在2006年3月訪問華盛頓時立場堅定的表示：「我們要求此[軟體技術轉移](../Page/軟體.md "wikilink")，若無法取得軟體原始碼我們將放棄購買此型戰機。」他還說假若交易失敗英國政府已有「B計畫」\[47\]。這可能是指海軍版的[歐洲戰機](../Page/歐洲戰機.md "wikilink")。\[48\]

在2006年5月27日，[布希總統和](../Page/喬治·沃克·布希.md "wikilink")[布萊爾首相宣佈](../Page/布萊爾.md "wikilink")「雙方政府皆同意英國將有順利操作、升級、佈署和維修聯合打擊機的能力也就是英國保有此飛機的獨立操作權」\[49\]。儘管如此，遲至2006年12月仍有對缺少技術轉移表達憂慮的聲音，不過，在2006年12月12日，德瑞森上議員簽署了一份符合英國對後續參與提出的要求的合約，例如取得軟體原始碼和獨立操作權，此合約提供「連貫的英國指揮鏈」操作此飛機。德瑞森說英國將「不須要有任何一位美國國民在我們的指揮鏈之中」\[50\]。不過，德瑞森還說，英國仍在考慮未公開的「B計畫」替代F-35。

在2007年7月25日，[英國國防大臣證實他們已下單購買兩艘](../Page/英國國防部.md "wikilink")[伊利沙白女皇級航空母艦](../Page/伊利沙白女皇級航空母艦.md "wikilink")，這使F-35B次型獲得購買機會。\[51\]

2016年6月29日，歸屬[英國編號為BK](../Page/英國.md "wikilink")-3的F-35B與另兩架[美國海軍陸戰隊的F](../Page/美國海軍陸戰隊.md "wikilink")-35B護航下，飛行十個小時橫越[大西洋飛抵](../Page/大西洋.md "wikilink")[英國](../Page/英國.md "wikilink")[費爾福德空軍基地](../Page/費爾福德空軍基地.md "wikilink")，並將於2018年編入[英國皇家空軍和](../Page/英國皇家空軍.md "wikilink")[英國皇家海軍服役](../Page/英國皇家海軍.md "wikilink")\[52\]\[53\]\[54\]。

### 義大利

[義大利原先計畫為](../Page/義大利.md "wikilink")[義大利空軍購買](../Page/義大利空軍.md "wikilink")109架F-35A，並為[義大利海軍購買](../Page/義大利海軍.md "wikilink")22架F-35B以部署於[加富爾號航空母艦](../Page/加富爾號航空母艦.md "wikilink")，取代[龍捲風戰鬥轟炸機](../Page/龍捲風戰鬥轟炸機.md "wikilink")、[AMX](../Page/AMX.md "wikilink")、[AV-8](../Page/AV-8.md "wikilink")。\[55\]
最終義大利決定購買共90架F-35，包括空軍60架F-35A、15架F-35B及[義大利海軍](../Page/義大利海軍.md "wikilink")15架F-35B。\[56\]

2015年3月12日，義大利[卡梅里組裝工廠展示義大利第一架F](../Page/卡梅里.md "wikilink")-35A(編號為AL-1)。\[57\]
同年12月3日，該架F-35A(編號為AL-1)正式交付予義大利空軍\[58\]，該架F-35A為首架海外製造的F-35\[59\]。

2016年2月5日，隸屬[義大利空軍的F](../Page/義大利空軍.md "wikilink")-35A，飛行7小時橫越[大西洋飛抵美國](../Page/大西洋.md "wikilink")[帕圖森河海軍基地](../Page/帕圖森河海軍基地.md "wikilink")，途中義大利空軍派出[KC-767A空中加油機為F](../Page/KC-767A.md "wikilink")-35A空中加油，成為F-35首次由非[美國空軍加油機空中加油認證試驗的紀錄](../Page/美國空軍.md "wikilink")\[60\]\[61\]。該架F-35A並駐紮於[亞利桑那州](../Page/亞利桑那州.md "wikilink")[路克空軍基地作訓練飛行員的任務](../Page/路克空軍基地.md "wikilink")\[62\]。

2016年12月12日，[義大利空軍第](../Page/義大利空軍.md "wikilink")32聯隊第13中隊接收由意大利自行生產編號為AL-5及AL-6的F-35A，該兩架F-35A由意大利空軍飛行員駕駛，由北部產地飛往南部空軍基地，使義大利空軍成為世界首支得到美國海外可作戰之F-35的軍隊\[63\]。

2017年5月5日，意大利首架由卡梅F-35最終組裝廠組裝的F-35B舉行出廠儀式，該架F-35B將於2017年8月底進行首飛，該機亦是美國本土以外首架組裝完工的F-35B\[64\]。

### 荷蘭

[荷蘭計畫为](../Page/荷蘭.md "wikilink")[荷蘭皇家空軍购买](../Page/荷蘭皇家空軍.md "wikilink")85架F-35A，用于取代老化中的[F-16AM機群](../Page/F-16戰隼戰鬥機.md "wikilink")，荷蘭政府預計這將包括55億歐元的采购预算和91億歐元的30年使用预算。\[65\]

2016年5月23日，兩架歸屬[荷蘭皇家空軍編號分別為AN](../Page/荷蘭皇家空軍.md "wikilink")-1及AN-2的F-35A，飛抵[荷蘭本土](../Page/荷蘭.md "wikilink")\[66\]\[67\]\[68\]。

### 澳大利亚

[Brendan_Nelson_signing_the_JSF_Production,_Sustainment_and_Follow-on_Development_Memorandum_of_Understanding_for_Australia.jpg](https://zh.wikipedia.org/wiki/File:Brendan_Nelson_signing_the_JSF_Production,_Sustainment_and_Follow-on_Development_Memorandum_of_Understanding_for_Australia.jpg "fig:Brendan_Nelson_signing_the_JSF_Production,_Sustainment_and_Follow-on_Development_Memorandum_of_Understanding_for_Australia.jpg")國防部部長[布蘭登·尼爾森](../Page/布蘭登·尼爾森.md "wikilink")（Brendan
Nelson）正在簽署JSF生產，維修和後續研發瞭解備忘錄。\]\]
在2005年5月，澳大利亚政府宣佈它將購買JSF的決定由2006年延至2008年，因而超過當時政府的任期。澳大利亚和英国一样，堅持它必須能取得所有修改和修理飛機所需的軟體。[澳洲皇家空軍做的研究顯示F](../Page/澳洲皇家空軍.md "wikilink")-35是「最適合澳洲所需的飛機」。\[69\]

澳洲內部對F-35是否是最適合澳洲皇家空军的飛機存在爭論，據一些媒體報導，遊說團體和政治家質疑F-35可能不能即時取代澳洲皇家空军老化中的[F-111攻擊機和](../Page/F-111.md "wikilink")[F/A-18大黃蜂戰鬥機](../Page/F/A-18.md "wikilink")。一些評論家認為更昂貴的[F-22或](../Page/F-22.md "wikilink")[歐洲戰機可能是更好的選擇](../Page/歐洲戰機.md "wikilink")，在可能不會比F-35貴很多的價錢提供更佳的航程、[空戰能力和](../Page/空戰.md "wikilink")[超音速巡航](../Page/超音速巡航.md "wikilink")。\[70\]

在2006年8月初發佈的一份聲明中，當時的國防部長[布蘭登·尼爾森博士披露雖然F](../Page/布蘭登·尼爾森.md "wikilink")-35仍受政府支持，澳洲已開始研究其他替代方案以備F-35不可行時\[71\]。2006年10月，空軍副總司令，[約翰·布拉克邦恩](../Page/約翰·布拉克邦恩.md "wikilink")（John
Blackburn）中將，公開表明澳洲皇家空军已否決在F-35出現延誤時購買過度時期的打擊機，並相信F-35是合適的\[72\]。不過在2007年3月6日，尼爾森部長宣佈澳洲政府將向波音購買24架[F/A-18F超級大黃蜂填補](../Page/F/A-18.md "wikilink")[F-111退役留下的空隙](../Page/F-111.md "wikilink")，預計花費60億澳幣\[73\]。不過，尼爾森部長仍然贊成澳洲購買F-35，2007年3月在澳洲電視台的發言，尼爾森部長表示F-35有5%的性能是機密，他聲稱，「這5%真的很有價值」。\[74\]

在2006年12月13日，尼爾森部長簽署JSF生產、維修和後續研發瞭解備忘錄，此合約為澳洲對JSF的取得和後續支援提供了合作架構\[75\]。澳洲最初預計花費160億澳幣購買100架F-35A。\[76\]

2009年，[陸克文](../Page/陸克文.md "wikilink")（Kevin Rudd）政府批准斥資32亿澳元購買14架F-35。

2014年4月30日，澳洲總理[托尼·阿博特正式宣佈該國將購買](../Page/托尼·阿博特.md "wikilink")72架F-35。採購计划总成本为124亿澳元（或约116亿美元，或人民币717.8亿元），是澳洲有史以来最大規模的軍備采购之一，預計前14架將於2018年交付并于2020年时投入裝備\[77\]。

### 土耳其

在2002年7月12日，土耳其成為JSF計畫第七個國際合夥人，加入英國、意大利、荷蘭、加拿大、丹麥和挪威。在2007年1月25日，土耳其簽署關於F-35生產的瞭解備忘錄，據報導土耳其預計花費110億美元訂購100架F-35A「CTOL／空軍版」\[78\]。

土耳其航空工業公司（TAI）和美國承包商[諾格公司在](../Page/诺斯洛普·格鲁门.md "wikilink")2007年2月6日簽署購買意向書，根據此購買意向書，TAI將成為F-35低速初期生產（LRIP）兩階段時期中央機身的第二來源，自LRIP-1開始生產，並在LRIP-2生產最少400個中央機身。\[79\]在2007年12月10日，開始為兩架F-35製造零組件，即合成元件和飛機出入口，這將用在F-35中央機身，諾格公司在洛馬領導下為此飛機生產的重要部分。\[80\]
本型戰機預計取代[F-4](../Page/F-4.md "wikilink")、[F-5等老舊機種](../Page/F-5.md "wikilink")。

### 加拿大

[加拿大國防部尋求在](../Page/加拿大國防部.md "wikilink")2020年前替換老化中的[CF-18機群並對F](../Page/F/A-18.md "wikilink")-35抱持很大興趣，國防部官員表示預架生產80架將花費38億美元，但此數額未包括訓練、維修和任何後續費用。加拿大可待2012年再決定是否購買F-35，不過他們已投資一億五千萬美元在JSF計畫。\[81\]
[缩略图](https://zh.wikipedia.org/wiki/File:An_F-35_Lightning_II_completes_a_flyover_of_USS_Zumwalt_\(DDG_1000\)._\(29774535153\).jpg "fig:缩略图")\]\]
在2010年7月16日，加拿大国防部长[彼得·麦凯宣布](../Page/彼得·麦凯.md "wikilink")，加拿大政府决定斥资90亿加元（约合86亿美元）购买65架美国生产的第五代F－35闪电2型联合攻击战斗机。\[82\]

加拿大政府在2012年12月12日宣布，决定停购美国洛克希德·马丁公司生产的F-35战机，重新启动采购程序，招标时将考虑所有货源，寻找新的替代战机。

### 其他國家

#### 以色列

在2003年，以色列簽署價值近2千萬美元的同意書，正式做為「安全合作成員」加入F-35系統研發和驗證\[83\]。[以色列空軍在](../Page/以色列空軍.md "wikilink")2006年表示F-35是IAF重整計畫的關鍵部份，並且以色列計畫花費50億美元購買超過100架F-35A戰機替換他們的F-16戰機\[84\]。在對中國軍售喊停後，以色列在2006年7月31日暫時回復F-35發展夥伴的身份。\[85\]

在2007年9月3日，[以色列國防軍參謀總長Gabi](../Page/以色列國防軍.md "wikilink")
Ashkenazi中將宣佈購買一中隊F-35並將在2014年開始陸續接收，不過，美國國防官員後來同意讓以色列最早在2012年開始接收\[86\]。每架F-35預計高達五千到六千萬美元。\[87\]2008年9月美國政府同意向以色列出售25架的F-35A，日後並追加50架的F-35A或是F-35B。2016年12月12日，美國國防部長卡特訪問以色列，共同慶祝第一批兩架F-35I戰機運抵以色列。2017年1月16日晚上兩架僅接收後一個月的F-35I執行首次夜間飛行任務，為[以色列空軍所有機型當中最快執行夜間飛行任務的機種](../Page/以色列空軍.md "wikilink")\[88\]。2017年4月23日第二批次共三架F-35I戰機飛抵位於[內蓋夫沙漠的](../Page/內蓋夫沙漠.md "wikilink")，連同第一批共5架F-35I編入別稱金鷹中隊的，預計2017年內達到初始作戰能力\[89\]\[90\]。

#### 日本

日本原本希望獲得[F-22戰機](../Page/F-22.md "wikilink")，曾向美國提出過購買意向。但是美國政府為避免技術外洩為由1998年立法規定F-22禁止出口，日本只能退而求其次選擇F-35。[日本防衛省在於](../Page/日本防衛省.md "wikilink")2012年初召開機型選定委員會，F-35即使延遲交機，但仍將是[航空自衛隊下一代戰機的首選](../Page/航空自衛隊.md "wikilink")，2012年5月4日新聞，美國國防部日前向國會提交報告稱，日本計劃購買的42架F-35戰機總價預計將高達到100億美元，以汰換掉老舊的F-4J機隊\[91\]。

2011年12月20日，日本政府召開安全保障會議和內閣會議，決定以F-35汰換航空自衛隊老舊的[F-4S](../Page/F-4.md "wikilink")「幽靈二式」戰鬥機、[三菱F-1戰鬥機](../Page/三菱F-1戰鬥機.md "wikilink")，作為下一代密接支援戰機，以負責對地對海打擊任務。防衛省將於2012年度預算編列採購經費，期望美國能在2016年交機。防衛省起初將F-35、美國波音公司F/A-18「超級大黃蜂式」戰鬥機以及歐洲「颱風式」戰鬥機作為下一代主力戰機候選機種，而一如預期，匿蹤性能出色、與自衛隊現有戰機在資訊分享方面匹配的F-35終於出線，因應中國研發中的[殲-20隱形戰機所帶來的衝擊平衡壓力](../Page/殲-20.md "wikilink")。

2013年12月17日通過《國家安全保障戰略》與新的《防衛計劃大綱》等文件，闡明未來五年把國防開支增加5%，再增購100架F-35。\[92\]。

2015年12月，日本於[名古屋組裝第一架F](../Page/名古屋.md "wikilink")-35A戰鬥機授權生產型，AX-5聯合打擊戰鬥機。該架F-35A將於2017年進入日本[航空自衛隊服役](../Page/航空自衛隊.md "wikilink")。而且日本訂購42架F-35A之中，其中4架由[洛克希德·馬丁於](../Page/洛克希德·馬丁.md "wikilink")[美國](../Page/美國.md "wikilink")[沃斯堡組裝](../Page/沃斯堡.md "wikilink")，剩下38架則於日本名古屋工場組裝。\[93\]

2016年9月23日，[洛克希德·馬丁於](../Page/洛克希德·馬丁.md "wikilink")[美國](../Page/美國.md "wikilink")[沃斯堡組裝的第](../Page/沃斯堡.md "wikilink")200架F-35A舉行出廠典禮\[94\]，該架戰機將歸屬於[日本](../Page/日本.md "wikilink")[航空自衛隊](../Page/航空自衛隊.md "wikilink")\[95\]，並於2017年1月12日首先交付至[亞利桑那州](../Page/亞利桑那州.md "wikilink")[路克空軍基地的國際訓練團隊作為培訓之用](../Page/路克空軍基地.md "wikilink")\[96\]。

2017年1月18日，首批兩架[美國陸戰隊F](../Page/美國陸戰隊.md "wikilink")-35B戰鬥機，飛抵日本山口縣[岩國基地](../Page/岩國基地.md "wikilink")。這是F-35B首度部署在美國境外\[97\]。

2017年6月5日，在位於[愛知縣](../Page/愛知縣.md "wikilink")[豐山町的](../Page/豐山町.md "wikilink")[三菱重工業公司小牧南工廠舉行首架在日本組裝的F](../Page/三菱重工業公司.md "wikilink")-35A出廠儀式，該工廠負責生產F-35的引擎等部分零部件、最終組裝及性能檢測\[98\]\[99\]。

#### 韓國

[韓國空軍正在研製下一代戰機](../Page/韓國空軍.md "wikilink")[KF-X計畫](../Page/KF-X.md "wikilink")，受到日本採購F-35以及中國研發中的[殲-20戰機的影響](../Page/殲-20.md "wikilink")，在對[北韓的軍事保有優勢之下](../Page/北韓.md "wikilink")，目前招標採購60架先進戰機，具備隱形能力，也有可能採購F-35或是波音公司[F-15SE沉默鷹](../Page/F-15SE.md "wikilink")，也是F-35的潛在客戶，預估為108億美元。\[100\]\[101\]\[102\]\[103\]此型戰機預計取代類似用途的[F-4](../Page/F-4.md "wikilink")、[F-5戰鬥機](../Page/F-5.md "wikilink")。美國國防部也已同意讓[洛克希德·馬丁公司的F](../Page/洛克希德·馬丁.md "wikilink")-35匿蹤戰鬥機，或[波音的](../Page/波音.md "wikilink")[F-15SE](../Page/F-15SE.md "wikilink")「沉默鷹」戰鬥機出售給韓方，最後F-15SE出局，韓國採購40架F-35同時將繼續自行研發該國五代機\[104\]。
2017年12月大韓民國宣布增購20架F-35\[105\]。2018年3月28日韓國空軍接收了首批2架跟美國訂購的F-35A型戰鬥機\[106\]。

#### 丹麥

2016年6月9日，[丹麥政府宣佈將採購](../Page/丹麥.md "wikilink")27架F-35A戰機，取代現行服役的F-16戰機\[107\]\[108\]\[109\]。

#### 挪威

挪威政府探購的52架F-35A，依照計劃將於2017年接收6架，當中3架將首先交付至美國[亞利桑那州](../Page/亞利桑那州.md "wikilink")[路克空軍基地作為培訓之用](../Page/路克空軍基地.md "wikilink")，其餘3架會於2017年11月初抵達挪威本土。2018年其後交付的F-35A將以每年6架的速度接收，並將直接飛抵挪威本土。[挪威皇家空軍預計於](../Page/挪威皇家空軍.md "wikilink")2019年宣布已交付的F-35A具備作戰能力，並於2021年底取代現役的F-16機隊\[110\]\[111\]。

### 新加坡

2019年3月2日新加坡宣布展開採購美製F-35戰機程序，預期先採購4架，保留8架選擇權。\[112\]

## 潛在客戶

### 印度

截至2007年7月F-35也是印度空軍的潛在選擇，這被理解成向IAF販賣F-16多用途戰機策略的一部份。\[113\]

### [台灣](../Page/台灣.md "wikilink")

雖然不具發展夥伴身分，但台灣曾多次於不同場合向美國表達對F-35B的高度興趣，並有意借鑒其部分功能為其下一代戰機研發計畫的發展要點——尤其是短場起降、匿蹤、垂直起降、超視距作戰等優異性能。雖然國際軍評專家均認為以其現況及未來衡量，非常適合布署F-35B以應付目前逐漸向[中國人民解放軍傾斜的空優](../Page/中國人民解放軍.md "wikilink")，以及解放軍對台湾大量佈署短程彈道飛彈摧毀其機場跑道，美方建議[台灣採購F](../Page/台灣.md "wikilink")-35B垂直起降型戰機；但是對於是否能在未來順利取得或轉移F-35B技術以取代其現役的[F-16等主力戰機](../Page/F-16.md "wikilink")，因牽涉到[中華民國與美國關係](../Page/中華民國與美國關係.md "wikilink")、《[台灣關係法](../Page/台灣關係法.md "wikilink")》、台海軍力平衡、[中美關係](../Page/中美關係.md "wikilink")、及[兩岸關係發展等](../Page/兩岸關係.md "wikilink")，而尙無定論。
\[114\]\[115\]\[116\]\[117\]

2018年3月15日，國防部長[嚴德發上午在立法院外交及國防委員會證實](../Page/嚴德發.md "wikilink")，[中華民國國防部確實在](../Page/中華民國國防部.md "wikilink")2018年對美軍購清單上，對美國政府提出採購F-35戰機的需求。
\[118\] \[119\]

## 事故

  - 2018年9月28日上午11時一架美國「F-35B」於[南卡羅來納州進行飛行訓練時](../Page/南卡羅來納州.md "wikilink")，因不明原因墜毀，該機型也成為F35系列戰機研發到量產飛行以來首架墜毀事故（Class
    A mishap）。
  - 2019年4月9日一架[日本](../Page/日本.md "wikilink")[航空自衛隊](../Page/航空自衛隊.md "wikilink")「F-35A」於青森線三澤基地起飛，進行夜間訓練，後從雷達上消失，於[青森縣外海墜毀](../Page/青森縣.md "wikilink")，已發現部分尾翼殘骸。飛行員仍失蹤，這一次則是F-35A首度失事\[120\]\[121\]

## 流行文化

### 電影情節

  - 在動作电影《[終極警探4.0](../Page/終極警探4.0.md "wikilink")》中，一架F-35B奉命前往追擊挾持麥克連女兒的蓋伯爾一行人，飛行員卻被蓋伯爾誤導，反而將麥克連（[-{zh-hans:布鲁斯·威利斯;zh-hk:布斯·韋利士;zh-tw:布魯斯·威利;}-飾](../Page/布魯斯·威利.md "wikilink")）當作目標。
  - 在電影《[復仇者聯盟](../Page/復仇者聯盟.md "wikilink")》中用作分散綠巨人的注意、並被擊毀，後來更向城市發射核彈。
  - 為電影《[綠光戰警](../Page/綠光戰警.md "wikilink")》主角的駕駛機
  - 在電影《[超人：鋼鐵英雄](../Page/超人：鋼鐵英雄.md "wikilink")》中，一批F-35對克利普頓星人的行星改造機具進行攻擊，但都被機具干擾而紛紛墜毀。
  - 在電影《[變形金剛5](../Page/變形金剛5.md "wikilink")》中，主力由F-35組成的機群向斯比頓星發動攻擊，並掩護電影主角身處的直升機空降斯比頓星表面。

### 电子游戏

  - 在2005年[第一人称射击游戏](../Page/第一人称射击游戏.md "wikilink")《[戰地風雲2](../Page/战地2.md "wikilink")》以及2011年續作《[戰地風雲3](../Page/戰地風雲3.md "wikilink")》、2013年續作《[戰地風雲4](../Page/戰地風雲4.md "wikilink")》中，F-35三度以美國海軍陸戰隊戰鬥機之姿出現。
  - 在2011年[射擊遊戲](../Page/射擊遊戲.md "wikilink")《[皇牌空戰：突擊地平線](../Page/皇牌空戰：突擊地平線.md "wikilink")》中，F-35是可使用機體之一。
  - 在2012年第一人稱射擊遊戲《[-{zh-hans:使命召唤：黑色行动2; zh-hk:使命召喚：黑色行動2;
    zh-tw:決勝時刻：黑色行動2;}-](../Page/決勝時刻：黑色行動2.md "wikilink")》中以「FA38」的名字出现，外形修改为无尾布局雙引擎垂直起降戰機。

## 图集

<File:First> F-35A on taxiway upon arrival at new
home.jpg|停在艾格林空軍基地滑行道上的F-35A <File:X-35B> in
NA\&SM.jpg|美國國家航空及太空博物館展出的X-35 <File:F-35> Casco Piloto.jpg|頭盔顯示器
<File:F-35B> cutaway with LiftFan.jpg|F-35B構造解剖圖 <File:F-35B> Joint
Strike Fighter (thrust vectoring nozzle and lift fan).PNG|F-35B動力解剖圖
<File:Engine> of
F-35.jpg|垂直舉升扇的動力透過引擎傳動軸傳送，該軸承受的壓力極大可比巡洋艦的車葉軸，所以是高度[材料工程產物](../Page/材料工程.md "wikilink")
[File:F35ctolstores.jpg|F-35A和C型的武器掛載](File:F35ctolstores.jpg%7CF-35A和C型的武器掛載)
<File:F-35B> (STOVL) weapon stores.jpg|F-35B型的武器掛載 <File:F-35> weapon
layout.jpg|武器掛載磅數 <File:F-35> EOTS.jpeg|電子・光学式照準 <File:F-35> weapons
bay.jpeg|机体内置武器倉

## 参考文献

## 參見

  - [聯合攻擊戰鬥機計劃](../Page/聯合攻擊戰鬥機計劃.md "wikilink")
  - [F-22](../Page/F-22猛禽戰鬥機.md "wikilink")
  - [J-20](../Page/殲-20.md "wikilink")
  - [J-31](../Page/殲-31.md "wikilink")
  - [T-50](../Page/T-50.md "wikilink")
  - [ATD-X](../Page/ATD-X.md "wikilink")

## 外部链接

  - [F-35机队官方网站](http://f35.com/)
  - [英国JSF页面](http://www.jsf.org.uk/)
  - [F-35
    –英国皇家空军](http://www.raf.mod.uk/equipment/f35jointstrikefighter.cfm)
  - [US Navy Research, Development & Acquisition, F-35
    page](http://acquisition.navy.mil/programs/air/f_35_jsf)
  - [F-35 profile on Scramble Dutch Aviation
    Society](http://wiki.scramble.nl/index.php/Lockheed_Martin_F-35_Lightning_II)

{{-}}

[Category:美國戰鬥機](../Category/美國戰鬥機.md "wikilink")
[Category:美國攻擊機](../Category/美國攻擊機.md "wikilink")
[Category:洛克希德·马丁飞机](../Category/洛克希德·马丁飞机.md "wikilink")
[Category:艦載機](../Category/艦載機.md "wikilink")
[Category:垂直起降機](../Category/垂直起降機.md "wikilink")

1.

2.  ["Capabilities". (archived
    version)](https://web.archive.org/web/20100724021638/http://www.lockheedmartin.com/products/f35/f-35-capabilities.html)
    *[Lockheed Martin](../Page/Lockheed_Martin.md "wikilink")*.
    Retrieved 24 July 2010.

3.

4.

5.  ["F-35 Global
    Partnerships."](http://www.lockheedmartin.com/us/products/f35/global-partnerships.html)
    *Lockheed Martin.* Retrieved: 31 October 2012.

6.  Dudley, Richard. ["Program Partners Confirm Support for F-35 Joint
    Strike
    Fighter."](http://defense-update.com/20120305_program-partners-confirm-support-for-f-35-joint-strike-fighter.html)
    *Defence Update,* 5 March 2012. Retrieved: 18 March 2012.

7.  ["Israel plans for second F-35
    squadron."](http://www.upi.com/Business_News/Security-Industry/2012/02/27/Israel-plans-for-second-F-35-squadron/UPI-41711330368531/)
    *[United Press
    International](../Page/United_Press_International.md "wikilink")*,
    27 February 2012. Retrieved: 18 March 2012.

8.  ["US Lockheed Martin F-35 chosen as Japan fighter
    jet."](http://www.bbc.co.uk/news/world-asia-16259895) *BBC News,* 20
    December 2011. Retrieved: 20 December 2011.

9.  Fukue, Natsuko. ["U.S.-made F-35 is chosen for
    ASDF"](http://www.japantimes.co.jp/text/nn20111220x2.html).
    *[日本時報](../Page/日本時報.md "wikilink")*, 21 December 2011, p.
    1.

10. Wall, Robert, ["Lockheed Dismisses Korea F-35 Schedule
    Issue."](http://www.aviationweek.com/aw/generic/story_channel.jsp?channel=defense&id=news/asd/2012/02/20/05.xml)
    *Aviation Week*. 21 February 2012.

11.

12.

13.

14. [レーダー性能](http://f35jsf.wiki.fc2.com/wiki/%E3%83%AC%E3%83%BC%E3%83%80%E3%83%BC%E6%80%A7%E8%83%BD)

15. [世界尖端武器觀測站](https://www.facebook.com/ng.wawos/photos/a.190612724410628.45904.168565909948643/1025784680893424/?type=3&theater)

16.

17.

18. [http://zh.scribd.com/doc/144894824/F-35-IOC-Joint-Report-FINAL-1-pdf|title=F-35](http://zh.scribd.com/doc/144894824/F-35-IOC-Joint-Report-FINAL-1-pdf%7Ctitle=F-35)
    Initial Operational Capability

19.

20.

21.

22.

23.

24.

25. Trimble, Stephen. "US military unveils possible F-35B redesign in
    sweeping budget reforms." Flight International, 6 January 2011.

26. Cox, Bob. "F-35 started with recipe for trouble, analysts say." Star
    Telegram, 29 January 2011.

27. [Panetta Lifts F-35B
    Probation](http://www.aviationweek.com/aw/generic/story_generic.jsp?channel=defense&id=news/awx/2012/01/20/awx_01_20_2012_p0-416683.xml&headline=Panetta)
    20 Jan 2012

28. [1](http://www.f-16.net/news_article4608.html)"F-35 completes first
    airborne weapons separation" August 8, 2012 (by NAVAIR News)

29.

30. Carol Richards，Naval F-35C Carrier Variant Makes Maiden
    Flight，AirForces Monthly，August，2010，p4

31.
32.

33.

34.

35.

36.

37. Merle, Renae. ["GAO Questions Cost Of Joint Strike
    Fighter."](http://www.washingtonpost.com/wp-dyn/articles/A38236-2005Mar15.html)
    *Washington Post*, 15 March 2005. Retrieved: 15 July 2007.

38. [Estimated JSF Air Vehicle Procurement
    Quantities](http://www.jsf.mil/downloads/documents/ANNEX%20A%20Revision_April%202007.pdf),
    JSF.mil, April 2007.

39. [F-35聯合攻擊機未來市場高達六千架](http://news.chinatimes.com/2007Cti/2007Cti-Focus/2007Cti-Focus-Content/0,4518,9806250071+0+0+1+0,00,focus.html)，[中國時報](../Page/中國時報.md "wikilink")，2009-06-25.

40. [JSF Global
    Partners.](https://www.teamjsf.com/jsf/data.nsf/75public/07CF737749FA9E5585256F3900720288?OpenDocument)
    Retrieved: 30 March 2007.

41. "US, UK sign JAST agreement." *Aerospace Daily* New York:
    McGraw-Hill, 25 November 1995. p. 451.

42. Schnasi, Katherine V. ["Joint Strike Fighter Acquisition:
    Observations on the Supplier
    Base."](http://www.gao.gov/new.items/d04554.pdf) *US Accounts
    Office*. Retrieved: 8 February 2006.

43. *Defense Industry Daily*. [F-35 Lightning II Faces Continued
    Dogfights in
    Norway](http://www.defenseindustrydaily.com/2007/02/f35-lightning-ii-faces-continued-dogfights-in-norway/index.php)

44.

45. ["UK denied waiver on US arms
    technology."](http://www.ft.com/cms/s/881df2c4-5b7f-11da-b221-0000779e2340.html)
    *Financial Times*. Retrieved: 11 October 2006.

46. "UK Defence Committee Statement." *UK Parliament*. [MoD 'slippage'
    set to leave forces with reduced capability, says
    committee](http://www.parliament.uk/parliamentary_committees/defence_committee/def051220___no__13.cfm).
    Retrieved: 8 February 2006.

47. Chapman. Matt. ["Britain warns US over jet software
    codes."](http://web.archive.org/web/20070930200303/http://www.vnunet.com/vnunet/news/2152035/joint-strike-fighter)
    *vunet.com*. Retrieved: 16 March 2006.

48. [Evidence to UK Defence Select
    Committee](http://www.publications.parliament.uk/pa/cm200506/cmselect/cmdfence/uc824-iii/uc82402.htm).
    Retrieved: 1 April 2006.

49. ["Bush gives way over stealth
    fighter."](http://news.ft.com/cms/s/7de7925a-ecf3-11da-a307-0000779e2340.html)
    *Financial Times*. Retrieved: 27 May 2006.

50. "Update 2—UK signs memo with US on Joint Strike Fighter." *Reuters*,
    12 December 2006. [Reuters UK Joint Strike
    Fighter](http://www.reuters.com/article/companyNewsAndPR/idUSL1278309720061212).
    Retrieved: 13 December 2006.

51. BBC News Online. ["MoD confirms £3.8bn carrier
    order."](http://news.bbc.co.uk/1/hi/scotland/6914788.stm)
    *news.bbc.co.uk*.

52.
53.

54.

55. ["STOVL JSF future
    debated"](http://www.navytimes.com/news/2007/04/defense_stovl_jsf_070430/),
    *Navy Times*, 1 May 2007, URL accessed on 29 December 2007.

56. [The F-35 for
    Italy](https://www.f35.com/global/participation/italy-f-35-for-italy),F-35
    LightningII, Lockheed Martin. (12 January 2016)

57. [First ever F-35 assembled internationally destined to Italy rolled
    out of Cameri
    facility](http://theaviationist.com/2015/03/12/first-italian-f35a-rolled-out/),David
    Cenciotti, The Aviationist, 12 March 2015.

58. First Italian F-35A Delivered, Combat Aircraft, February 2016, P.15.

59.
60.
61.

62.
63.
64.

65. ["Stand van zaken september 2006, Monitoring verwerving Joint Strike
    Fighter"](http://www.rekenkamer.nl/9282000/d/p396_rapport.pdf),
    *Algemene Rekenkamer*, p. 22. 2006, October.

66.
67.

68.

69. , The Hon. Dr. "Joint Strike Fighter." **, 1 February 2007. [Press
    release](http://www.minister.defence.gov.au/NelsonMintpl.cfm?CurrentId=6341)
    Retrieved: 1 February 2007.

70. Related discussions and analyses on [Air Power Australia web
    site](http://www.ausairpower.net) although the F-22 is not for sale
    internationally.

71. ["US decisions 'threaten' fighter
    project."](http://www.theage.com.au/news/National/US-decisions-threaten-fighter-project/2006/08/04/1154198330441.html)
    *[The Age](../Page/The_Age.md "wikilink")*, 4 August 2006.
    Retrieved: 19 August 2006.

72. Blenkin, Max. .["RAAF 'won't need' interim
    jet."](http://www.news.com.au/story/0,23599,20556293-31037,00.html),
    **. Retrieved: 10 October 2006.

73. ["Australia to buy 24 F-18 Super Hornets from
    Boeing."](http://www.thanhniennews.com/worlds/?catid=9&newsid=25777)
    *Thanhnien News*, 6 March 2007. Retrieved: 6 March 2007.

74. Bartlett, Liam. ["Transcript of '60 Minutes' broadcast 'Project
    Joint Strike
    Fighter'."](http://sixtyminutes.ninemsn.com.au/sixtyminutes/stories/2007_03_18/story_1879.asp)
    *Ninemsn*, (18 March 2006). Retrieved: 18 March 2007.

75. ["Australia commits to F-35 strike
    fighter."](http://www.theage.com.au/news/WORLD/Australia-commits-to-F35-strike-fighter/2006/12/13/1165685710399.html)
    *[The Age](../Page/The_Age.md "wikilink")*, 13 December 2006.

76. Elliot, Geoff. ["Troubled stealth fighter tackles first test
    flight."](http://www.theaustralian.news.com.au/story/0,20867,20943857-31477,00.html)
    *[The Australian](../Page/The_Australian.md "wikilink")*, 18
    December 2006.

77.

78. [Turkey Signs F-35 Production
    MoU](http://www.defenseindustrydaily.com/2007/01/turkey-signs-f35-production-mou/index.php)

79. [2](http://www.tai.com.tr/en_haber_guncel.aspx?id=84)

80. [3](http://live.defenseworld.net:9080/go/defensenews.jsp?catid=1&id=2116)

81. ["Canada’s military eyeing futuristic fighter
    jets"](http://www.canada.com/topics/technology/story.html?id=7a365ab7-22b5-4d06-9de7-7a6177af4b62)

82. [4](http://mil.news.sina.com.cn/2010-08-26/1034607898.html)

83. ["Israel inks LOA to join Joint Strike Fighter
    program"](http://www.f-16.net/news_article5.html).

84. ["Israel Plans to Buy Over 100
    F-35s."](http://www.defenseindustrydaily.com/2006/06/israel-plans-to-buy-over-100-f35s/index.php)
    *DefenseIndustryDaily.com*, 27 June 2006.

85. ["Israel, US battling over sale of
    jets"](http://fr.jpost.com/servlet/Satellite?cid=1148287839455&pagename=JPost/JPArticle/ShowFull).

86. [US to speed up stealth fighter delivery to
    Israel](http://fr.jpost.com/servlet/Satellite?cid=1192380645400&pagename=JPost/JPArticle/ShowFull),
    Jerusalem Post.

87. [IDF chief presents budget and procurement
    plan](http://fr.jpost.com/servlet/Satellite?cid=1188392524293&pagename=JPost/JPArticle/ShowFull),
    *Jerusalem Post*.

88.

89.

90.

91.

92.

93. [First Japanese-built F-35 Begins
    Assembly](http://www.defensenews.com/story/defense/air-space/2015/12/15/first-japanese-built-f-35-begins-assembly/77378304/)
    , Lara Seligman, Defense News, 4:52 p.m. EST December 15, 2015.

94.
95.
96.

97.

98.

99.

100. ["不能輸給人！　南韓受日本刺激也考慮買F-35"](http://www.nownews.com/2011/12/20/11490-2768155.htm)

101. ["防北韓作亂南韓擬採購60架隱形戰機"](http://www.nownews.com/2012/02/03/11490-2781324.htm)

102. ["韓國下代戰機專家稱已鎖定F-35"](http://www.nownews.com/2012/02/19/11623-2786892.htm)

103. ["韓欲向美購F-35戰機美開價108億美元"](http://news.chinatimes.com/realtime/110104/112013040400562.html)

104.

105.

106. [南韓接收首批美隱形戰機
     恐刺激北韓](https://udn.com/news/story/6809/3728123?from=udn-catebreaknews_ch2)

107.
108.

109.

110.

111.

112. [新加坡宣布採購4架F-35](https://tw.news.yahoo.com/%E6%96%B0%E5%8A%A0%E5%9D%A1%E5%AE%A3%E5%B8%83%E6%8E%A1%E8%B3%BC4%E6%9E%B6f-35-160000866.html)

113. ["US wants India's fighter jet order, dangles F-35
     carrot"](http://www.ibnlive.com/news/us-wants-indias-fighter-jet-order-dangles-f35-carrot/45214-3.html).

114. ["Taiwan Sends Mixed Message on
     Fighters"](http://www.defensenews.com/story.php?i=4626494&c=ASI&s=AIR)

115. ["美國稱將評估台灣戰機再決定是否出售F-35"](http://ny.stgloballink.com/taiwan/201005/t20100522_1329014.html)

116. ["台媒：台灣應以F-35為軍購目標"](http://taiwan.dwnews.com/big5/news/2011-08-11/58002614.html)

117. ["台灣軍購鎖定F-35戰機美智庫稱可能性極低"](http://big5.huaxia.com/js/th/2007/00660530.html)

118. [嚴德發證實 我已向美提出購買F–35 20180315 公視中晝新聞 公視新聞網
     發佈日期：2018年3月14日](https://www.youtube.com/watch?v=EiDLPuzthBU)

119. [聯合新聞網 我對美軍購提出採購F-35戰機 國防部長嚴德發證實 2018-03-15 09:53聯合報
     記者洪哲政╱即時報導](https://udn.com/news/story/10930/3032110)

120.

121. [「日本製造」就是好？砸大錢自己裝F-35
     試駕卻慘摔](https://udn.com/news/story/6809/3751213)