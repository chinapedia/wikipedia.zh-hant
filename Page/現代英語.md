**現代英語**（），又稱**新興英語**（，相對於[中古英語與](../Page/中古英語.md "wikilink")[古英語](../Page/古英語.md "wikilink")），是指[現代使用的](../Page/現代.md "wikilink")[英語](../Page/英語.md "wikilink")，其產生開始于[元音大推移](../Page/元音大推移.md "wikilink")，大致完成于1550年。在[語言學](../Page/語言學.md "wikilink")[歷史角度來看](../Page/英語史.md "wikilink")，現代英語包含了[中古英語後期的英語](../Page/中古英語.md "wikilink")。撇除個別的[詞彙差異](../Page/詞彙.md "wikilink")，諸如[威廉·莎士比亞作品及](../Page/威廉·莎士比亞.md "wikilink")《[欽定版聖經](../Page/欽定版聖經.md "wikilink")》等17世紀初的讀物，可視為現代英語；若作更確切劃分，則屬于[近代英語](../Page/近代英語.md "wikilink")。大多數通曉21世紀初英語的人大致讀懂這些書籍
。

現代英語有多種[方言](../Page/方言.md "wikilink")，分佈於全球多個[國家及地區](../Page/國家.md "wikilink")。它們分別是[英國英語](../Page/英國英語.md "wikilink")、[愛爾蘭英語](../Page/愛爾蘭英語.md "wikilink")、[美國英語](../Page/美國英語.md "wikilink")、[加拿大英語](../Page/加拿大英語.md "wikilink")、[澳洲英語](../Page/澳洲英語.md "wikilink")、[紐西蘭英語](../Page/紐西蘭英語.md "wikilink")、[加勒比英語](../Page/加勒比英語.md "wikilink")、[印巴英語及](../Page/印巴英語.md "wikilink")[南非英語](../Page/南非英語.md "wikilink")，當中大多數可互通。這些英語方言可應用於不同環境，譬如，[美國一些演出](../Page/美國.md "wikilink")[好莱坞](../Page/好莱坞电影.md "wikilink")[歷史或](../Page/歷史.md "wikilink")[神話](../Page/神話.md "wikilink")[史詩的](../Page/史詩.md "wikilink")[演員](../Page/演員.md "wikilink")，往往會使用來自[英國](../Page/英國.md "wikilink")[口音](../Page/口音.md "wikilink")；同時，很多[美國](../Page/美國.md "wikilink")、[澳洲及非以英語為](../Page/澳洲.md "wikilink")[母語的](../Page/母語.md "wikilink")[國際](../Page/國際.md "wikilink")[流行歌手會訴諸於](../Page/流行音樂.md "wikilink")[國際](../Page/國際.md "wikilink")[人口統計學](../Page/人口統計學.md "wikilink")，以「[工業中立](../Page/工業.md "wikilink")」的[美國](../Page/美國.md "wikilink")[口音演唱](../Page/口音.md "wikilink")。

根據[民族語](../Page/民族語.md "wikilink")[網站](../Page/網站.md "wikilink")，1999年有超過五億八百萬人以[英語作為](../Page/英語.md "wikilink")[第一或第二](../Page/母語.md "wikilink")[語言](../Page/語言.md "wikilink")，其使用者數目在全[世界僅少於](../Page/世界.md "wikilink")[漢語的使用者](../Page/漢語.md "wikilink")。然而漢語的使用範圍較小，主要侷限於[大中华地区及华人间的交流](../Page/大中华地区.md "wikilink")。相對於其使用範圍來說，使用英語的地區遍及[美國](../Page/美國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、[英國](../Page/英國.md "wikilink")、[愛爾蘭](../Page/愛爾蘭.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[印度](../Page/印度.md "wikilink")、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[南非等地](../Page/南非.md "wikilink")。\[1\]長久以來，龐大的使用人口，加上全[球的普及應用](../Page/世界.md "wikilink")，讓英語成為一種[國際語言](../Page/國際語言.md "wikilink")。現今不論[文化交流](../Page/文化.md "wikilink")、[資訊傳播](../Page/資訊.md "wikilink")、[商業活動](../Page/商業.md "wikilink")，還是[國際](../Page/國際.md "wikilink")[外交等領域](../Page/外交.md "wikilink")，英語都必不可少。

## 歷史

現代英語源於[伊麗莎白時期的](../Page/伊麗莎白時期.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")，時值[英國大](../Page/英國.md "wikilink")[詩人](../Page/詩人.md "wikilink")[威廉·莎士比亞的創作時期](../Page/威廉·莎士比亞.md "wikilink")。\[2\]

憑着[大英帝國的](../Page/大英帝國.md "wikilink")[殖民](../Page/殖民.md "wikilink")，[英語在世界各地](../Page/英語.md "wikilink")，譬如[美國](../Page/美國.md "wikilink")、[印度及](../Page/印度.md "wikilink")[澳洲沿用下來](../Page/澳洲.md "wikilink")。又因為大不列顛手在[北美洲](../Page/北美洲.md "wikilink")、[亞洲及](../Page/亞洲.md "wikilink")[非洲建立殖民地](../Page/非洲.md "wikilink")，英語與風俗、思想等藉此傳播到世界。這被視為[哥倫布交換](../Page/哥倫布交換.md "wikilink")（Columbian
Exchange）的一部分。

## 所受影響

[早期現代英語沒有統一的拼法](../Page/早期現代英語.md "wikilink")，及至1755年，[塞繆爾·約翰遜](../Page/塞繆爾·約翰遜.md "wikilink")[博士於](../Page/博士.md "wikilink")[英格蘭出版](../Page/英格蘭.md "wikilink")[詞典](../Page/詞典.md "wikilink")，定下關鍵性的單詞標準拼法。另外，[諾亞·韋伯斯特](../Page/諾亞·韋伯斯特.md "wikilink")1828年在[美國出版了詞典著作](../Page/美國.md "wikilink")。（參見[美國英語\#美國英語和英國英語的差異](../Page/美國英語#美國英語和英國英語的差異.md "wikilink")）

19世紀，隨着公眾教育與[公共圖書館日漸普及](../Page/公共圖書館.md "wikilink")，人們有更多機會閱讀書籍，以及接觸標準語言。透過貿易與定居，不少人容易接觸到外來文化，加上大量移民湧入[美國](../Page/美國.md "wikilink")，[外來詞亦隨之輸入了英語](../Page/外來詞.md "wikilink")。[第一次及](../Page/第一次世界大戰.md "wikilink")[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，不同背景的人在短時間內匯集，而且社會流動性加強，無形中縮窄了社會[口音的差異](../Page/口音.md "wikilink")，尤其是[美國](../Page/美國.md "wikilink")。20世紀初，[電台廣播發展](../Page/電台.md "wikilink")，人們對於外地口音與[詞彙更加熟悉](../Page/詞彙.md "wikilink")，一些更是他們前所未聞的。今時今日，這種現象在[電影與](../Page/電影.md "wikilink")[電視仍屢見不鮮](../Page/電視.md "wikilink")。（參見[英語借詞](../Page/英語借詞.md "wikilink")）

### 西語化

### 印地語化

### 歐化

### 中文化

### 葡語化

## 變化概述

下文概述現代英語自[中古英語以來的主要變化](../Page/中古英語.md "wikilink")。注意，這些概說未必完全適用於個別[英語](../Page/英語.md "wikilink")[方言](../Page/方言.md "wikilink")：

### 音系

參見[英語音系學](../Page/英語音系學.md "wikilink")、[約1600年-1725年語音變化及](../Page/:en:Phonological_history_of_the_English_language#Up_to_the_American/British_split_\(c._AD_1600–1725\).md "wikilink")[約1725年-1900年語音變化](../Page/:en:Phonological_history_of_the_English_language#After_American/British_split,_up_to_the_20th_century_\(c._AD_1725–1900\).md "wikilink")。

### 語法

  - [亲疏尊卑称谓](../Page/親疏尊卑稱謂.md "wikilink")（TV-distinction，即[thou](../Page/wikt:thou.md "wikilink")\[3\]、[ye](../Page/wikt:ye.md "wikilink")\[4\]）不再使用。

<!-- end list -->

  - [助動詞強制使用於](../Page/助動詞.md "wikilink")[疑問句](../Page/疑問句.md "wikilink")。

<!-- end list -->

  - [規範語法相繼出現](../Page/規範語法學.md "wikilink")。

<!-- end list -->

  - 受到《[文體要素](../Page/文體要素.md "wikilink")》（The Elements of
    Style）與《[今日美國](../Page/今日美國.md "wikilink")》\[5\]兩者的[散文體影響](../Page/散文.md "wikilink")。

## 參見

  - [網路英語](../Page/網路英語.md "wikilink")
  - [簡易英語](../Page/簡易英語.md "wikilink")
  - [英語史](../Page/英語史.md "wikilink")
  - [英語種類列表](../Page/英語種類列表.md "wikilink")
  - [古英語](../Page/古英語.md "wikilink")
  - [中古英语](../Page/中古英语.md "wikilink")
  - [近代英語](../Page/近代英語.md "wikilink")

## 参考

## 外部連結

  - [英語：英國的語言](http://www.ethnologue.com/show_language.asp?code=ENG)（[民族語網站](../Page/民族語.md "wikilink")）

  - [世界英語組織](http://www.world-english.org/)
  - [莎士比亞與早期中古英語：現代英語發展](http://www.nosweatshakespeare.com/shakespeare_early_middle_english.htm)（NoSweatShakespeare網站，講述[莎士比亞對](../Page/莎士比亞.md "wikilink")[早期現代英語的影響](../Page/早期現代英語.md "wikilink")）

  - [古英語、中古英語、後期現代英語彙編](https://web.archive.org/web/20070217230712/http://uk.geocities.com/hashanayobel/o/oldeng.htm)


[Category:英语史](../Category/英语史.md "wikilink")
[Category:英语](../Category/英语.md "wikilink")

1.  [英語：英國的語言](http://www.ethnologue.com/show_language.asp?code=ENG)（[民族語網站](../Page/民族語.md "wikilink")）


2.
3.  《[牛津高階英漢雙解詞典](../Page/牛津英語詞典.md "wikilink")》（第四版修訂增補本）「thou」詞條：「**thou**
    /ðaʊ; ðau/ *pers pron*
    [人稱](../Page/人稱.md "wikilink")[代詞](../Page/代詞.md "wikilink")（*arch*
    古）（used as the second person singular subject of a *v*
    用作第二人稱[單數](../Page/單數_\(語言學\).md "wikilink")[動詞的主體](../Page/動詞.md "wikilink")）you
    汝；你：*Who art thou?* 汝何人也？」

4.  《牛津高階英漢雙解詞典》（第四版修訂增補本）「ye」詞條：「**ye**<sup>1</sup> /ji:; ji/ *pers
    pron* 人稱代詞（*arch* 古）（*pl of thou* ☆
    thou的[複數](../Page/眾數.md "wikilink")） you 汝等；君等。」

5.  [今日美國](http://www.usatoday.com)