《**機動戰士GUNDAM SEED C.E.73
STARGAZER**》，一般翻译为“观星者”，為C.E.纪元的[ONA](../Page/原創網路動畫.md "wikilink")（原创网络动画）作品，亦是[X
plosion GUNDAM
SEED企劃的第一部作品](../Page/X_plosion_GUNDAM_SEED.md "wikilink")。共三话构成，每话约15分钟的短篇，通过收费网络配信发布。

## 故事简介

本作是发生在与[機動戰士GUNDAM SEED
DESTINY相同时间](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")(C.E.73年)，但不同地点、視點的故事。三集內容主要講述自引發大战的尤尼奧斯7號坠落事件起，到柏林之战前夕以及洛德·吉布列潛逃月球時發生的三段事件；从侧面戰場描寫这场大战，讓觀眾認識TV版SEED
DESTINY看不到的殘酷的另一面。

## 人物

### [D.S.S.D.](../Page/D.S.S.D..md "wikilink")

  -
    28歲。調整者。D.S.S.D成員，研究開發[觀星者鋼彈](../Page/GSX-401FW_Stargazer.md "wikilink")（GSX-401FW
    Stargazer）的人員之一，其後與索爾共同駕駛觀星者鋼彈（GSX-401FW
    Stargazer），官方设定中，她的位置是通信员席位，由索爾负责操纵席。在與[漆黑攻擊鋼彈](../Page/GAT-X105E_Strike_E.md "wikilink")（GAT-X105E
    STRIKE NOIR）戰鬥時，打算將觀星者鋼彈廢棄，捨身被攻擊與漆黑攻擊鋼彈（GAT-X105E STRIKE
    NOIR）同歸於盡。最後漂流至地球與[金星的軌道間](../Page/金星.md "wikilink")，與史威恩往回D.S.S.D.漂流，生死不明。漫畫中生還。

<!-- end list -->

  -
    16歲，調整者。D.S.S.D成員，研究開發觀星者鋼彈（GSX-401FW
    Stargazer）人員之一，觀星者高达的駕駛者。在击坠5部敌机后，本想尽可能全灭敌军，但是賽雷妮打算自己和对方同归于尽并考虑到敌方母舰和后援的存在，于是让他脫離机体，回到D.S.S.D的阿波羅A指揮攻击并击沉敌人的母舰。
    官方设定中，索爾是觀星者高达自机体诞生以来的实际驾驶员，也是D.S.S.D为驾驶机体而专门培育的第一世代调整人。观星者高达的机体最初被叫作「401（ヨンマルイチ）」，是索爾在OVA第三集中为自己的机体取名“观星者”，即本片的名字STARGAZER。

<!-- end list -->

  - （[KIA](../Page/陣亡.md "wikilink")）
    37歲，
    自然人。D.S.S.D技術開發中心保安副部長，曾任在地球联合陸軍戰車小隊指揮官，官至少佐。因為戰場中表現勇猛，有「傳說之鬼車長」稱號。第二次埃金．杜維戰役終結時時退役，轉而加入D.S.S.D.。因為昔日在戰事中，與埃德蒙共同在D.S.S.D任職的姐姐以及其丈夫身亡，便收養兩人的兒子索爾。
    最後與已升階為軍曹的舊部，為迎戰到處破壞的基恩（Z.A.F.T.派屈克·薩拉側的殘黨駕駛）而戰死。其在D.S.S.D的工作，後來為史威恩所繼承。

### [地球联合](../Page/地球联合.md "wikilink")

  -
    20歲，自然人。地球軍幻痛部隊之華金隊成員，軍階中尉。最初駕駛由幻痛再生產的[攻擊鋼彈](../Page/GAT-X105_Strike.md "wikilink")（GAT-X105
    STRIKE）「I.W.S.P.實裝型」，其後轉為前者改修版本「[漆黑攻擊鋼彈](../Page/GAT-X105E_Strike_E.md "wikilink")（GAT-X105E
    STRIKE
    NOIR）」。原本是一名愛好觀測天體的平凡少年，雙親在[藍色宇宙／藍波斯菊的襲擊中死去後](../Page/藍色宇宙聯盟.md "wikilink")，被[藍色宇宙／藍波斯菊營運的孤兒院居住](../Page/藍色宇宙聯盟.md "wikilink")，並接受消滅調整者的精英教育，成為特殊戰鬥用的MS駕駛員，與接受同一機構培育的穆笛·霍科洛芙特、沙姆斯·科薩組成小隊。最後與賽雷妮漂流至地球與[金星的軌道間](../Page/金星.md "wikilink")，生死不明。漫畫中被救獲，最後加入D.S.S.D.組織。與以往利用強力藥物加上強化手術催谷戰鬥力，引致需要透過不斷服藥，才可維持生命的[生物CPU不同](../Page/生物CPU.md "wikilink")，史溫等人主要接受電擊及藥物進行洗腦教育，除此之外，身體機能與普通人並無分別，雖然如此，史溫本身擁有超越部隊其他成員的戰鬥力，更試過在模擬戰中，僅以五分鐘時間，便將作為對手的兩名經驗老到的同僚打敗。性格冷靜，不輕易流露感情，大部份時間都是沉默寡言。被過酷的訓練及洗腦教育後，對非人道的任務反應平淡，甚至被要求殺害非戰鬥人員，也只會猶豫一會便立即答應，唯獨在穆笛·霍科洛芙特、沙姆斯·科薩先後戰死時，才流露出憤慨與動搖的表情。而在對兩人的回憶中，亦曾出現開朗的笑容。

<!-- end list -->

  - （[KIA](../Page/陣亡.md "wikilink")）
    18歲，地球軍幻痛部隊之華金隊成員、[蔚藍決鬥鋼彈](../Page/GAT-X1022_蔚藍決鬥高達.md "wikilink")（GAT-X1022
    BLU
    DUEL）駕駛員，最後在護送[破滅鋼彈](../Page/GFAS-X1_Destroy.md "wikilink")（GFAS-X1
    Destroy）途中，被地獄三頭犬型巴庫（TMF/A-802W2 Kerberos BuCUE Hound）的雷射尖牙刺入駕駛艙而慘死。

<!-- end list -->

  - （[KIA](../Page/陣亡.md "wikilink")）
    19歲，地球軍幻痛部隊之華金隊成員、[翠綠暴風鋼彈](../Page/GAT-X103AP_Verde_Buster.md "wikilink")（GAT-X103AP
    VERDE
    BUSTER）駕駛員，與穆蒂是情人，對調整者的仇恨相當激進。最後在強攻D.S.S.D總部時，因為機體能源耗盡，PS裝甲失去機能而被民用異端鋼彈D.S.S.D.特裝型（UT-1D
    Civilian Astray DSSD Custom）圍攻至死。

<!-- end list -->

  -
    史威恩等人的上司，為了達成目標會使用一些較「骯髒」的手段，但使用的理由都能讓人信服。

#### 客串人物

  - （回憶）

在史温的面前走过，只露出侧脸和背影，一旁跟随的是史汀格和被医护人员推着的史黛拉。

  -

在史温的面前走过，只露出侧脸和背影，跟随着尼欧和被医护人员推着的史黛拉。

  -

躺在推床上，在史温面前经过，头上还系着绷带。

### [Z.A.F.T.](../Page/Z.A.F.T..md "wikilink")

#### 客串人物

  -

## 登场机体

### [D.S.S.D.](../Page/D.S.S.D..md "wikilink")

  - [GSX-401FW Stargazer
    Gundam](../Page/GSX-401FW_Stargazer.md "wikilink")
  - [UT-1D Civilian Astray DSSD
    Custom](../Page/UT-1D_Civilian_Astray_DSSD_Custom.md "wikilink")

### [地球联合](../Page/地球联合.md "wikilink")／[幻痛](../Page/幻痛_\(GUNDAM\).md "wikilink")

  - [GAT-X105E Strike Noir
    Gundam](../Page/GAT-X105E_Strike_E.md "wikilink")
  - [GAT-X1022 Blu Duel
    Gundam](../Page/GAT-X1022_Blu_Duel.md "wikilink")
  - [GAT-X103AP Verde Buster
    Gundam](../Page/GAT-X103AP_Verde_Buster.md "wikilink")
  - [GAT-01A2R 105 Slaughter
    Dagger](../Page/GAT-01A1_Dagger#GAT-01A2R_+_AQM/E-X01_Slaughter_Dagger.md "wikilink")

### [Z.A.F.T.](../Page/Z.A.F.T..md "wikilink")

  - [ZGMF-1017 GINN Insurgent
    Type](../Page/ZGMF-1017_GINN.md "wikilink")
  - [TMF/A-802W2 Kerberos BuCUE
    Hound](../Page/TMF/A-802_巴古.md "wikilink")
  - [TMF/A-802 BaCUE](../Page/TMF/A-802_BaCUE.md "wikilink")

## 各话內容

  - STAGE 1：2006年7月14日，劇中時間點：尤尼乌斯7號坠落。
  - STAGE 2：2006年8月18日，劇中時間點：柏林之战前夕。
  - STAGE 3：2006年9月29日，劇中時間點：洛德·吉布列潛逃月球。

## 制作人员

  - 企划：[SUNRISE](../Page/SUNRISE.md "wikilink")
  - 原作：[矢立肇](../Page/矢立肇.md "wikilink")、[富野由悠季](../Page/富野由悠季.md "wikilink")
  - 监督：西泽晋
  - 脚本：森田繁
  - 人物设定：大贯健一
  - 机械设定：[大河原邦男](../Page/大河原邦男.md "wikilink")、[山根公利](../Page/山根公利.md "wikilink")、[BEECRAFT](../Page/BEECRAFT.md "wikilink")、[藤冈健机](../Page/藤冈健机.md "wikilink")
  - Design Works：シンクポート
  - 色彩设计：柴田亚纪子／安部なぎさ
  - 特殊设定：森田繁
  - 美术监督：池田繁美
  - 摄影监督：田中唯
  - 编集：ウィンズ
  - 音响监督：藤野贞义
  - 音乐：大桥惠
  - 音乐Producer：野崎圭一（Victor Entertainment）、佐保歌名世（SUNRISE音乐出版）
  - Producer：池谷浩臣（SUNRISE）、桑园裕子（BANDAI VISUAL）
  - 企划Producer：佐々木新（SUNRISE）
  - 制作协力：BANDAI Hobby事业部／BANDAI VISUAL
  - 制作：SUNRISE

## 音乐

### 片尾曲

  - 歌：

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - [官方网站](http://www.seed-stargazer.net/)
  - [“X”plosion GUNDAMSEED](http://www.xg-seed.net/)

[Category:GUNDAM SEED](../Category/GUNDAM_SEED.md "wikilink")
[Category:日本原創網路動畫](../Category/日本原創網路動畫.md "wikilink")
[Category:2006年日本網絡動畫](../Category/2006年日本網絡動畫.md "wikilink") [Seed
C.E. 73: Stargazer](../Category/GUNDAM系列.md "wikilink")