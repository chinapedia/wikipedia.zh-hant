**HTML编辑器**是一種功能主要用來編輯[HTML的](../Page/HTML.md "wikilink")[文本编辑器](../Page/文本编辑器.md "wikilink")[軟件](../Page/軟件.md "wikilink")。其實HTML本身只是一個[文本文件](../Page/文本文件.md "wikilink")，就算普通的文本編輯器都可以勝任。不過，专用的HTML编辑器一般都有[語法高亮顯示](../Page/語法高亮.md "wikilink")、[自动完成等特性](../Page/自动完成.md "wikilink")，让用户不用強記一大堆HTML指令和更方便的编写HTML。

一些的HTML编辑器還具有[所见即所得](../Page/所见即所得.md "wikilink")（**WYSIWYG**／**W**hat
**Y**ou **S**ee **I**s **W**hat **Y**ou
**G**et.），用户可以使用编辑器提供的[类](../Page/类.md "wikilink")，在编辑界面上通过拖放等手段创建对象，调整属性，非常简便。而且，有些编辑器还具有脚本编写功能，如Dreamweaver，支持[JavaScript](../Page/JavaScript.md "wikilink")，[VBScript](../Page/VBScript.md "wikilink")，[PHP](../Page/PHP.md "wikilink")，[Perl](../Page/Perl.md "wikilink")、[Python等](../Page/Python.md "wikilink")[编程语言](../Page/编程语言.md "wikilink")，并对连接[数据库等高级服务器操作提供更简单便捷的编写支持](../Page/数据库.md "wikilink")。这样在整个过程中，用户有可能不必写一句代码，仅通过可视的操作就做出功能复杂、界面精美的[网站](../Page/网站.md "wikilink")。

以下是一些常見的HTML编辑器：

## 本地编辑器

  - [Adobe Dreamweaver](../Page/Adobe_Dreamweaver.md "wikilink")
  - [BlueGriffon](../Page/BlueGriffon.md "wikilink")
  - [Bluefish](../Page/Bluefish.md "wikilink")
  - [Microsoft SharePoint
    Designer](../Page/Microsoft_SharePoint_Designer.md "wikilink")（原[Microsoft
    Frontpage](../Page/Microsoft_Frontpage.md "wikilink")）
  - [SeaMonkey](../Page/SeaMonkey.md "wikilink")
    （[Mozilla基金会](../Page/Mozilla基金会.md "wikilink")）
  - [Namo WebEditor](../Page/Namo_WebEditor.md "wikilink")
  - [記事本](../Page/記事本.md "wikilink")

### 停止更新

  - [Adobe Golive](../Page/Adobe_Golive.md "wikilink")
  - [KompoZer](../Page/KompoZer.md "wikilink")
  - [Microsoft FrontPage](../Page/Microsoft_FrontPage.md "wikilink")
  - [Microsoft Frontpage
    Express](../Page/Microsoft_Frontpage_Express.md "wikilink")
  - [Nvu](../Page/Nvu.md "wikilink")

## 在线部署

  - [CKEditor](../Page/CKEditor.md "wikilink")
  - [TinyMCE](../Page/TinyMCE.md "wikilink")

[HTML编辑器](../Category/HTML编辑器.md "wikilink")