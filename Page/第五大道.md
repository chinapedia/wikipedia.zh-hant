[Museum_Mile_Sign.jpg](https://zh.wikipedia.org/wiki/File:Museum_Mile_Sign.jpg "fig:Museum_Mile_Sign.jpg")

**第五大道**（），是[美国](../Page/美国.md "wikilink")[纽约市](../Page/纽约市.md "wikilink")[曼哈顿一條重要的南北向幹道](../Page/曼哈顿.md "wikilink")，南起[华盛顿广场公园](../Page/华盛顿广场公园.md "wikilink")，北抵[第138街](../Page/第138街.md "wikilink")。由於第五大道位於曼哈頓島的中心地帶，因此曼哈頓島上东西走向的街道有時會以这条街道为界而加以东西的称呼。（例如第五大道以西的十街就称为西十街）。

第五大道上景點眾多，由南至北有[帝国大厦](../Page/帝国大厦.md "wikilink")、[纽约公共图书馆](../Page/纽约公共图书馆.md "wikilink")、[洛克菲勒中心](../Page/洛克菲勒中心.md "wikilink")、[特朗普大厦](../Page/特朗普大厦.md "wikilink")、[圣帕特里克教堂以及](../Page/圣帕特里克教堂.md "wikilink")[中央公园等](../Page/中央公園_\(紐約市\).md "wikilink")。此外，由於中央公园附近有[大都會藝術博物館](../Page/大都會藝術博物館.md "wikilink")、[惠特尼美术馆](../Page/惠特尼美术馆.md "wikilink")、[索羅門·古根漢美術館](../Page/索羅門·古根漢美術館.md "wikilink")、[库珀·休伊特设计博物馆等著名的美术博物馆等](../Page/库珀·休伊特设计博物馆.md "wikilink")，因此被称为“艺术馆大道”（Museum
Mile）。在之间的第五大道，則被稱為「夢之街」，因為這裡聚集了许多著名的品牌商店，是高级购物街区。据統計，第五大道的租金在世界上可以算是數一數二的昂貴\[1\]\[2\]\[3\]。

第五大道也是纽约市民举行庆祝活动的传统途径路线，在夏季的星期日是禁止汽车通行的步行街。

## 歷史

第五大道在19世紀初不過是片空曠的農地，經過擴建後，逐漸變成紐約的高級住宅區，及名媛仕紳聚集的場所，高級購物商店也開始出現。進入20世紀後，第五大道變成了[摩天大樓](../Page/摩天大樓.md "wikilink")「爭高」的場所，其中以1931年落成的[帝國大廈為最高樓](../Page/帝國大廈.md "wikilink")。

## 具歷史意義的地標

[紐約市地標保護委員會](../Page/紐約市地標保護委員會.md "wikilink")，隨着1964年舊[賓夕法尼亞車站因修建](../Page/賓夕法尼亞車站.md "wikilink")[麥迪遜廣場花園而被拆除所引發的抗議活動而成立](../Page/麥迪遜廣場花園.md "wikilink")。它會根據地標的歷史意義，加以識別和保護：

<table>
<thead>
<tr class="header">
<th><p>地標名稱</p></th>
<th><p>識別日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/第五大道500號.md" title="wikilink">第五大道500號</a></p></td>
<td><p>2010年12月14日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>2002年12月10日</p></td>
</tr>
<tr class="odd">
<td><p>喬治·W·范德比爾特公寓</p></td>
<td><p>1977年3月22日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>1992年1月14日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1998年12月15日</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅德與泰勒.md" title="wikilink">羅德與泰勒</a></p></td>
<td><p>2007年12月</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1985年4月23日</p></td>
</tr>
<tr class="even">
<td><p>里佐利大廈</p></td>
<td><p>1985年1月29日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/薩克斯第五大道.md" title="wikilink">薩克斯第五大道</a></p></td>
<td><p>1984年12月20日</p></td>
</tr>
<tr class="even">
<td><p>邊路時鐘（第五大道200及522號）</p></td>
<td><p>1981年8月25日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>1988年11月1日</p></td>
</tr>
</tbody>
</table>

## 經濟

在介乎第49街至第60街之第五大道，為著名奢侈品的聚集點，包括了：

  - [香奈兒](../Page/香奈兒.md "wikilink")
  - [路易威登](../Page/路易威登.md "wikilink")
  - [古馳](../Page/古馳.md "wikilink")
  - [卡地亞](../Page/卡地亞.md "wikilink")
  - [菲拉格慕](../Page/薩瓦托·菲拉格慕.md "wikilink")
  - [范思哲](../Page/范思哲.md "wikilink")

## 圖片集

<File:(King1893NYC)> pg319 BIRD'S-EYE VIEW OF FIFTH AVENUE; NORTH OF
51ST STREET.jpg|從51街上空遠眺的鳥瞰圖（約1893年攝） <File:New> York NY 5th Ave Presby
PHS821.jpg|從51街北望的街景（約1895年攝） <File:5> Av 51 St North March 2015b
jeh.jpg|從51街北望的街景（2015年攝）

<File:Christmasonfifthavenue> 1896.jpg|1896年聖誕節
[File:5thavenue1.jpg|1918年](File:5thavenue1.jpg%7C1918年)

<File:Washington> Square by Matthew
Bisanz.JPG|[華盛頓廣場公園內的](../Page/華盛頓廣場公園.md "wikilink")[華盛頓廣場拱門](../Page/華盛頓廣場拱門.md "wikilink")
<File:Manhattan> Central Park Richard Morris Hunt
Memorial.JPG|美國建築師[理查德·莫里斯·亨特的紀念牌坊](../Page/理查德·莫里斯·亨特.md "wikilink")
<File:Plaza> Hotel.jpg|[廣場飯店](../Page/廣場飯店.md "wikilink")（約1907年攝）

## 參考

## 相關條目

  -
## 外部链接

  - [照片的第五大道](http://www.hanifworld.com/Fifth-Avenue.htm)
  - [第五大道橱窗秀图片](http://news.xinhuanet.com/newmedia/2005-03/29/content_2758649.htm)

{{-}}

[Category:紐約南北向街道](../Category/紐約南北向街道.md "wikilink")
[Category:曼哈頓文化](../Category/曼哈頓文化.md "wikilink")
[Category:曼哈頓街道](../Category/曼哈頓街道.md "wikilink")
[Category:紐約市經濟](../Category/紐約市經濟.md "wikilink")
[Category:曼哈顿博物馆](../Category/曼哈顿博物馆.md "wikilink")
[Category:曼哈顿旅游景点](../Category/曼哈顿旅游景点.md "wikilink")
[Category:美术馆区](../Category/美术馆区.md "wikilink")
[Category:博物馆区](../Category/博物馆区.md "wikilink")

1.
2.
3.