**村上真紀**。日本女性[漫畫家](../Page/漫畫家.md "wikilink")，出身於[北海道](../Page/北海道.md "wikilink")[小樽市](../Page/小樽市.md "wikilink")。

## 作品

  - 原名《》
  - [萬有引力](../Page/萬有引力_\(漫畫\).md "wikilink")（原名《》、[幻冬舍連載](../Page/幻冬舍.md "wikilink")、全12冊）
  - [万有引力EX.](../Page/万有引力EX..md "wikilink")（原名《》、[幻冬舍](../Page/幻冬舍.md "wikilink")、[Web
    Comic、SPICA](../Page/Web_Comic、SPICA.md "wikilink")、连载中）
  - [護魔童子](../Page/護魔童子.md "wikilink")（原名《》、[幻冬舍](../Page/幻冬舍.md "wikilink")、连载、2冊待續、目前休載中）
  - [遊戲者天堂](../Page/遊戲者天堂.md "wikilink")（原名《》、[Mag
    Garden](../Page/Mag_Garden.md "wikilink")、[月刊Comic
    Blade连载](../Page/月刊Comic_Blade.md "wikilink")、4冊待續、目前休載中）
  - [新堂家的事情](../Page/新堂家的事情.md "wikilink")（原名《》、連載、7話完結、未出單行本。）

## 外部連結

  - [](http://crocodile.jeez.jp/)

[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:日本女性漫画家](../Category/日本女性漫画家.md "wikilink")
[Category:BL漫画家](../Category/BL漫画家.md "wikilink")
[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")