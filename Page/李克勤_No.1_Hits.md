《**李克勤 No.1
Hits**》是[香港歌手](../Page/香港歌手.md "wikilink")[李克勤於](../Page/李克勤.md "wikilink")2007年11月23日發行的個人[音樂專輯](../Page/音樂專輯.md "wikilink")，是[DSD製作的](../Page/DSD.md "wikilink")4張[CD套裝](../Page/CD.md "wikilink")，收錄了李克勤三首新曲加四歷程\[1\]的金曲共68首。CD
1與CD 4是[李克勤自選的曲目](../Page/李克勤.md "wikilink")，而CD 2與CD
3則是排行榜上的冠軍曲目。三首新曲則分別是〈再見演奏廳〉、〈嘆息門〉與〈Mr.
Children〉。而[電視劇](../Page/電視劇.md "wikilink")《[賭場風雲](../Page/賭場風雲.md "wikilink")》的主題曲{{〈}}先賭為快{{〉}}則是首次被收錄至李克勤的個人專輯中。

## 曲目

### CD 1

1.  再見演奏廳
2.  相愛無門（前名為嘆息門）
3.  Mr. Children
4.  歲月風雲（與[小剛合唱](../Page/小剛.md "wikilink")）
5.  紙婚
6.  花落誰家
7.  先賭為快
8.  婚前的女人
9.  心計
10. 大時代
11. 刻不容緩（與[容祖兒合唱](../Page/容祖兒.md "wikilink")）
12. 六呎風雲
13. 自作自受
14. 左右手
15. 只想您會意
16. 一生不變

### CD 2

1.  公主太子
2.  天水、圍城
3.  我著十號
4.  勝情中人
5.  情非首爾
6.  佳節
7.  空中飛人
8.  吻別的位置
9.  仰慕者（與[譚詠麟合唱](../Page/譚詠麟.md "wikilink")）
10. 我不會唱歌（[郎朗鋼琴演奏](../Page/郎朗.md "wikilink")）
11. 左鄰右里（與[譚詠麟合唱](../Page/譚詠麟.md "wikilink")）
12. 愛一個人（與[陳慧琳合唱](../Page/陳慧琳.md "wikilink")）
13. 合久必婚（與[陳苑淇合唱](../Page/陳苑淇.md "wikilink")）
14. 愛不釋手
15. 高妹
16. Victory（featuring Bond）
17. 前後腳
18. 飛花

### CD 3

1.  再一次想你
2.  紅日（與[譚詠麟合唱](../Page/譚詠麟.md "wikilink")）
3.  櫻花
4.  請你早睡早起
5.  當找到你
6.  心甘情願
7.  為妳流淚
8.  依依不捨
9.  偷偷摸摸
10. 希望
11. 只懂得對你好
12. 回首
13. 破曉時份
14. 一生掛念妳
15. 一千零一夜
16. 藍月亮
17. 深深深
18. 夏日之神話

### CD 4

1.  仍是老地方
2.  十年前後
3.  世界末日的早上
4.  嘭門
5.  信心保證
6.  有了你世界更美
7.  傷寒
8.  月半小夜曲（）
9.  想妳的舊名字
10. 一個人飛
11. 好戲之人
12. 你是我的太陽
13. 萬千寵愛在一身（與[周慧敏合唱](../Page/周慧敏.md "wikilink")）
14. 寂寞煙雨天
15. 大會堂演奏廳
16. 誰願分手

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [《李克勤 No.1
    Hits》](http://www.umg.com.hk/chinese_detail.php?newrelease_no=932&company_id=1)於[環球唱片](http://www.umg.com.hk/)
  - [李克勤官方網頁](https://web.archive.org/web/20050317051540/http://www.hackenleenet.com/)

[N](../Category/2007年音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:李克勤音樂專輯](../Category/李克勤音樂專輯.md "wikilink")

1.  [李克勤待過的四家唱片公司](../Page/李克勤.md "wikilink")，分別是[寶麗金](../Page/寶麗金.md "wikilink")、[星光唱片](../Page/星光唱片.md "wikilink")、[藝能動音與](../Page/藝能動音.md "wikilink")[環球唱片](../Page/環球唱片_\(香港\).md "wikilink")。故稱「四歷程」