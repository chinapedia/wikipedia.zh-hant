}}

</div>

|image_map = Nordic countries orthographic.svg |map_caption =
|admin_center_type = 首都/首府 |admin_center =  |membership =  |

<hr/>

5个国家

<hr/>

||||| |

<hr/>

3个领土

<hr/>

||| }} |languages_type = 语言 |languages =  |area_rank = 第7位
|area_magnitude = |area_km2 = 3425804 |percent_water =
|population_estimate = 26,091,396 |population_estimate_year = 2013年
|population_estimate_rank = 第46位 |population_census = 25,478.559
|population_census_year = 2000年 |population_density_km2 = 7.62
|population_density_sq_mi = |population_density_rank = 第225位
|GDP_PPP = $1.117万亿 |GDP_PPP_year = 2013年 |GDP_PPP_rank = 第17位
|GDP_PPP_per_capita = $42,850 |GDP_PPP_per_capita_rank = 第6位
|GDP_nominal = $1.621,658万亿 |GDP_nominal_year = 2011年
|GDP_nominal_rank = 第12位 |GDP_nominal_per_capita = $63,647
|GDP_nominal_per_capita_rank = 第6位 |Failed state index = |Gini_year
= |Gini_change = |Gini = |Gini_ref = |Gini_rank = |HDI_year =
|HDI_change = |HDI = |HDI_ref = |HDI_rank = |currency =  |time_zone
= |utc_offset = }}

**北歐五國**是位於[北歐的](../Page/北歐.md "wikilink")[丹麥](../Page/丹麥.md "wikilink")、[芬蘭](../Page/芬蘭.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[挪威和](../Page/挪威.md "wikilink")[瑞典及其附屬領土如](../Page/瑞典.md "wikilink")[法羅群島](../Page/法羅群島.md "wikilink")、[格陵蘭和](../Page/格陵蘭.md "wikilink")[奧蘭群島的統稱](../Page/奧蘭群島.md "wikilink")。

該五國及三自治地區的歷史背景緊密連繫，社會和政治制度也相近。政治上雖然不是共同體，但都參與[北歐理事會](../Page/北歐理事會.md "wikilink")；語言上有三種語系，分別為[印歐語系的](../Page/印歐語系.md "wikilink")[斯堪的納維亞語支](../Page/斯堪的納維亞語支.md "wikilink")、[烏拉爾語系的](../Page/烏拉爾語系.md "wikilink")[芬蘭-烏戈爾語族和](../Page/芬蘭-烏戈爾語族.md "wikilink")[薩米語](../Page/薩米語.md "wikilink")，以及[愛斯基摩-阿留申語系的](../Page/愛斯基摩-阿留申語系.md "wikilink")[格陵蘭語](../Page/格陵蘭語.md "wikilink")。北歐五國共佔地350萬平方公里（格陵蘭佔其中60%的土地），人口約2,500萬。

近年，普遍被歸類為[波羅的海三國的](../Page/波羅的海三國.md "wikilink")[愛沙尼亞爭取躋身成為北歐國家一員](../Page/愛沙尼亞.md "wikilink")。北歐五國是愛沙尼亞重要的投資和貿易夥伴，而愛沙尼亞在語言、種族和文化上與芬蘭有關聯，文化上亦與瑞典和丹麥有相似的地方。

## 與斯堪地納維亞的比較

[thumb|250px |**紅色部分：**根據最嚴格的定義之斯堪地納維亞國家（三個君主立憲國）；
**橙色部分：**可被視為屬於斯堪地納維亞的國家；
**黃色部分：**最寬鬆的定義，與紅色和橙色部分統稱北歐五國。](../Page/File:Scandinavia_location_map_definitions.PNG.md "wikilink")

通常**斯堪地納維亞**指的是丹麥、挪威與瑞典，而**北歐五國**則清楚說明為丹麥、芬蘭、冰島、挪威和瑞典這五個國家，再加上各自的海外屬地（格陵蘭、法羅群島與奧蘭群島）\[1\]，因此斯堪地納維亞即是北歐五國的一部分。另外尚有一種分類是[芬諾斯堪底亞](../Page/芬諾斯堪底亞.md "wikilink")，其指的是斯堪地納維亞、芬蘭和[卡累利阿](../Page/卡累利阿.md "wikilink")，但不包括丹麥與其他海外屬地。不過此種劃分方式僅是[地質學的專有名詞](../Page/地質學.md "wikilink")，亦指[芬挪斯堪地盾](../Page/波羅的地盾.md "wikilink")（或稱作波羅的地盾）。

斯堪地那維亞國家：

  - （[內閣](../Page/議會制.md "wikilink")[君主立憲制](../Page/君主立憲制.md "wikilink")。)

  - （內閣君主立憲制，並於1905年獨立成為[君主國](../Page/君主國.md "wikilink")。）

  - （內閣君主立憲制。）

北歐五國：

  - 斯堪地那維亞三國

  - （[議會共和制](../Page/議會共和制.md "wikilink")，於1917年脫離[俄羅斯帝國獨立](../Page/俄羅斯帝國.md "wikilink")。）

  - （議會共和制，雖於1918年成立冰島共和國，但一直到1944年正式獨立前仍與丹麥組[共主邦聯](../Page/共主邦聯.md "wikilink")。）

與其海外屬地：

  - （丹麥領土，於1948年獲丹麥授與[高度自治](../Page/高度自治.md "wikilink")。）

  - （丹麥領土，於1979年獲自治權。）

  - （芬蘭領土，自1920年起成為芬蘭的自治省。）

## 歷史

北歐五國有共同的歷史，在社會和文化上關係密切。在[黑暗時代](../Page/歐洲黑暗時代.md "wikilink")，現時挪威、瑞典、丹麥和冰島一帶有着相似的文化、語言（[古諾爾斯語](../Page/古諾爾斯語.md "wikilink")）和[宗教](../Page/宗教.md "wikilink")（[北歐神話](../Page/北歐神話.md "wikilink")）。基督教於1000年傳入後，本土化使丹麥、瑞典、挪威形成三個獨立王國。由12世紀起，現時是芬蘭的地區（語言上屬[芬蘭-烏戈爾語族](../Page/芬蘭-烏戈爾語族.md "wikilink")）開始融入瑞典，形成了瑞典王國；而冰島、法羅群島、[設德蘭群島](../Page/設德蘭群島.md "wikilink")、[奧克尼群島](../Page/奧克尼群島.md "wikilink")、格陵蘭和大部分[蘇格蘭和](../Page/蘇格蘭.md "wikilink")[愛爾蘭則屬於挪威](../Page/愛爾蘭.md "wikilink")。所有北歐五國跟隨[宗教改革並接納](../Page/宗教改革.md "wikilink")[路德宗](../Page/路德宗.md "wikilink")。

14世紀，丹麥、挪威（連同冰島）和瑞典（連同芬蘭）組成[卡爾馬聯盟](../Page/卡爾馬聯盟.md "wikilink")，由同一[君主統治](../Page/君主.md "wikilink")。丹麥很快主導了聯盟，但在16世紀初期，瑞典重新成立獨立王國；而丹麥對挪威的支配，直至1814年被迫將挪威割讓予瑞典國王才完結；冰島、格陵蘭和法羅群島仍屬丹麥。17世紀，瑞典躋身成為歐洲[大國之一](../Page/大國.md "wikilink")，但其後逐一失去領土，甚至到1809年失去芬蘭；芬蘭成為附庸[俄羅斯](../Page/俄羅斯.md "wikilink")[沙皇的自治](../Page/沙皇.md "wikilink")[芬蘭大公國](../Page/芬蘭大公國.md "wikilink")。

1905年，瑞典與挪威自19世紀開始的聯盟隨着挪威不滿而瓦解。與此同時，[斯堪地那維亞主義在斯堪地那維亞冒起](../Page/斯堪地那維亞主義.md "wikilink")，旨在統一瑞、挪、丹三國，但成效不大。[一戰及俄國](../Page/一戰.md "wikilink")[十月革命期間](../Page/十月革命.md "wikilink")，芬蘭成為獨立國家，組成北歐國家共同體的想法開始出現。[二戰期間的](../Page/二戰.md "wikilink")1944年，冰島由丹麥獨立出來，至此，其後在1952年組成的北歐理事會的成員才全部成立。

二戰後，北歐五國的政策有共同的特色，如所有北歐國家都有由稅收支付的龐大公共福利體系，以及[社會民主主義的立法政體](../Page/社會民主主義.md "wikilink")，主要由在兩戰期間上台的社會民主主義者主導。

### 北歐國家組成時序

| 世纪                               | 北欧政治实体                                                                         |
| -------------------------------- | ------------------------------------------------------------------------------ |
| [丹麦人](../Page/丹麦人.md "wikilink") | [冰岛人](../Page/冰岛人.md "wikilink")                                               |
| 12世纪                             | <span style="color: white;">丹麦</span>                                          |
| 13世纪                             |                                                                                |
| 14世纪                             |                                                                                |
| 15世纪                             | [<span style="color: white;">卡尔马联盟</span>](../Page/卡尔马联盟.md "wikilink")</span> |
| 16世纪                             | [<span style="color: white;">丹麦-挪威</span>](../Page/丹麦-挪威.md "wikilink")</span> |
| 17世纪                             |                                                                                |
| 18世纪                             |                                                                                |
| 19世纪                             | <span style="color: white;">丹麦</span>                                          |
| 20世纪                             | <span style="color: white;">丹麦</span>                                          |
| 21世纪                             | [丹麦](../Page/丹麦.md "wikilink")（欧盟）                                             |

## 北欧五国国旗与国徽

### 丹麦

### 挪威

### 瑞典

### 冰岛

### 芬兰

## 参考文献

## 参见

  - [北歐](../Page/北歐.md "wikilink")
  - [斯堪的纳维亚](../Page/斯堪的纳维亚.md "wikilink")
  - [波罗的海国家](../Page/波罗的海国家.md "wikilink")

[&](../Category/欧洲国家.md "wikilink") [北欧](../Category/北欧.md "wikilink")

1.