**郊区**可以指：

  - [郊区](../Page/郊区.md "wikilink")：[城市周围属该城市管辖的地区](../Page/城市.md "wikilink")。

## 中华人民共和国的市辖区

### 现存的市辖区

  - [郊区
    (阳泉市)](../Page/郊区_\(阳泉市\).md "wikilink")：[山西省](../Page/山西省.md "wikilink")[阳泉市下辖的](../Page/阳泉市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。
  - [郊区
    (佳木斯市)](../Page/郊区_\(佳木斯市\).md "wikilink")：[黑龙江省](../Page/黑龙江省.md "wikilink")[佳木斯市下辖的市辖区](../Page/佳木斯市.md "wikilink")。
  - [郊区
    (铜陵市)](../Page/郊区_\(铜陵市\).md "wikilink")：[安徽省](../Page/安徽省.md "wikilink")[铜陵市下辖的市辖区](../Page/铜陵市.md "wikilink")。

### 已撤销的市辖区

#### 河北省

  - [郊区
    (石家庄市)](../Page/郊区_\(石家庄市\).md "wikilink")：[河北省](../Page/河北省.md "wikilink")[石家庄市曾设立的市辖区](../Page/石家庄市.md "wikilink")，2001年撤销，辖境现属[桥西区](../Page/桥西区_\(石家庄市\).md "wikilink")、[新华区](../Page/新华区_\(石家庄市\).md "wikilink")、[裕华区](../Page/裕华区.md "wikilink")。
  - [郊区
    (唐山市)](../Page/郊区_\(唐山市\).md "wikilink")：河北省[唐山市曾设立的市辖区](../Page/唐山市.md "wikilink")，1982年更名为[开平区](../Page/开平区.md "wikilink")。
  - [郊区
    (秦皇岛市)](../Page/郊区_\(秦皇岛市\).md "wikilink")：河北省[秦皇岛市曾设立的市辖区](../Page/秦皇岛市.md "wikilink")，1984年撤销，并入[海港区](../Page/海港区_\(秦皇岛市\).md "wikilink")。
  - [郊区
    (邯郸市)](../Page/郊区_\(邯郸市\).md "wikilink")：河北省[邯郸市曾设立的市辖区](../Page/邯郸市.md "wikilink")，1986年撤销，并入[邯山区](../Page/邯山区.md "wikilink")、[丛台区](../Page/丛台区.md "wikilink")、[复兴区](../Page/复兴区_\(邯郸市\).md "wikilink")。
  - [郊区
    (邢台市)](../Page/郊区_\(邢台市\).md "wikilink")：河北省[邢台市曾设立的市辖区](../Page/邢台市.md "wikilink")，1988年撤销，并入[桥东区](../Page/桥东区_\(邢台市\).md "wikilink")、[桥西区](../Page/桥西区_\(邢台市\).md "wikilink")。
  - [郊区
    (保定市)](../Page/郊区_\(保定市\).md "wikilink")：河北省[保定市曾设立的市辖区](../Page/保定市.md "wikilink")，1987年撤销，辖境现属[竞秀区](../Page/竞秀区.md "wikilink")、[莲池区](../Page/莲池区.md "wikilink")。
  - [郊区
    (沧州市)](../Page/郊区_\(沧州市\).md "wikilink")：河北省[沧州市曾设立的市辖区](../Page/沧州市.md "wikilink")，1997年撤销，并入[新华区](../Page/新华区_\(沧州市\).md "wikilink")、[运河区](../Page/运河区.md "wikilink")。

#### 山西省

  - [郊区
    (长治市)](../Page/郊区_\(长治市\).md "wikilink")：山西省[长治市曾设立的市辖区](../Page/长治市.md "wikilink")，2018年与[城区合并设立](../Page/城区_\(长治市\).md "wikilink")[潞州区](../Page/潞州区.md "wikilink")。
  - [郊区
    (晋城市)](../Page/郊区_\(晋城市\).md "wikilink")：山西省[晋城市曾设立的市辖区](../Page/晋城市.md "wikilink")，1996年撤销，改设[泽州县](../Page/泽州县.md "wikilink")。

#### 内蒙古自治区

  - [郊区
    (呼和浩特市)](../Page/郊区_\(呼和浩特市\).md "wikilink")：[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[呼和浩特市曾设立的市辖区](../Page/呼和浩特市.md "wikilink")，2000年更名为[赛罕区](../Page/赛罕区.md "wikilink")。
  - [郊区
    (包头市)](../Page/郊区_\(包头市\).md "wikilink")：内蒙古自治区[包头市曾设立的市辖区](../Page/包头市.md "wikilink")，1999年更名为[九原区](../Page/九原区.md "wikilink")。
  - [郊区
    (赤峰市)](../Page/郊区_\(赤峰市\).md "wikilink")：内蒙古自治区[赤峰市曾设立的市辖区](../Page/赤峰市.md "wikilink")，1993年更名为[松山区](../Page/松山区_\(赤峰市\).md "wikilink")。

#### 辽宁省

  - [郊区
    (鞍山市)](../Page/郊区_\(鞍山市\).md "wikilink")：[辽宁省](../Page/辽宁省.md "wikilink")[鞍山市曾设立的市辖区](../Page/鞍山市.md "wikilink")，1983年更名为[旧堡区](../Page/旧堡区.md "wikilink")。
  - [郊区
    (抚顺市)](../Page/郊区_\(抚顺市\).md "wikilink")：辽宁省[抚顺市曾设立的市辖区](../Page/抚顺市.md "wikilink")，1988年更名为[顺城区](../Page/顺城区.md "wikilink")。
  - [郊区
    (丹东市)](../Page/郊区_\(丹东市\).md "wikilink")：辽宁省[丹东市曾设立的市辖区](../Page/丹东市.md "wikilink")，1981年更名为[振安区](../Page/振安区.md "wikilink")。
  - [郊区
    (锦州市)](../Page/郊区_\(锦州市\).md "wikilink")：辽宁省[锦州市曾设立的市辖区](../Page/锦州市.md "wikilink")，1982年更名为[太和区](../Page/太和区.md "wikilink")。
  - [郊区
    (营口市)](../Page/郊区_\(营口市\).md "wikilink")：辽宁省[营口市曾设立的市辖区](../Page/营口市.md "wikilink")，1984年更名为[老边区](../Page/老边区.md "wikilink")。
  - [郊区
    (阜新市)](../Page/郊区_\(阜新市\).md "wikilink")：辽宁省[阜新市曾设立的市辖区](../Page/阜新市.md "wikilink")，1984年更名为[细河区](../Page/细河区.md "wikilink")。
  - [郊区
    (辽阳市)](../Page/郊区_\(辽阳市\).md "wikilink")：辽宁省[辽阳市曾设立的市辖区](../Page/辽阳市.md "wikilink")，1984年更名为[太子河区](../Page/太子河区.md "wikilink")。
  - [郊区
    (盘锦市)](../Page/郊区_\(盘锦市\).md "wikilink")：辽宁省[盘锦市曾设立的市辖区](../Page/盘锦市.md "wikilink")，1986年撤销，改设[盘山县](../Page/盘山县.md "wikilink")。

#### 吉林省

  - [郊区
    (长春市)](../Page/郊区_\(长春市\).md "wikilink")：[吉林省](../Page/吉林省.md "wikilink")[长春市曾设立的市辖区](../Page/长春市.md "wikilink")，1995年撤销，辖境现属[南关区](../Page/南关区.md "wikilink")、[宽城区](../Page/宽城区.md "wikilink")、[朝阳区](../Page/朝阳区_\(长春市\).md "wikilink")、[二道区](../Page/二道区.md "wikilink")、[绿园区](../Page/绿园区.md "wikilink")。
  - [郊区
    (吉林市)](../Page/郊区_\(吉林市\).md "wikilink")：吉林省[吉林市曾设立的市辖区](../Page/吉林市.md "wikilink")，1992年更名为[丰满区](../Page/丰满区.md "wikilink")。

#### 黑龙江省

  - [郊区
    (齐齐哈尔市)](../Page/郊区_\(齐齐哈尔市\).md "wikilink")：[黑龙江省](../Page/黑龙江省.md "wikilink")[齐齐哈尔市曾设立的市辖区](../Page/齐齐哈尔市.md "wikilink")，1980年更名为[梅里斯区](../Page/梅里斯区.md "wikilink")。
  - [郊区
    (牡丹江市)](../Page/郊区_\(牡丹江市\).md "wikilink")：黑龙江省[牡丹江市曾设立的市辖区](../Page/牡丹江市.md "wikilink")，1997年撤销，并入[东安区](../Page/东安区.md "wikilink")、[阳明区](../Page/阳明区.md "wikilink")、[爱民区](../Page/爱民区.md "wikilink")、[西安区](../Page/西安区_\(牡丹江市\).md "wikilink")。

#### 江苏省

  - [郊区
    (无锡市)](../Page/郊区_\(无锡市\).md "wikilink")：[江苏省](../Page/江苏省.md "wikilink")[无锡市曾设立的市辖区](../Page/无锡市.md "wikilink")，2000年更名为[滨湖区](../Page/滨湖区.md "wikilink")。
  - [郊区
    (徐州市)](../Page/郊区_\(徐州市\).md "wikilink")：江苏省[徐州市曾设立的市辖区](../Page/徐州市.md "wikilink")，1993年更名为[泉山区](../Page/泉山区.md "wikilink")。
  - [郊区
    (常州市)](../Page/郊区_\(常州市\).md "wikilink")：江苏省[常州市曾设立的市辖区](../Page/常州市.md "wikilink")，2002年更名为[新北区](../Page/新北区_\(常州市\).md "wikilink")。
  - [郊区
    (苏州市)](../Page/郊区_\(苏州市\).md "wikilink")：江苏省[苏州市曾设立的市辖区](../Page/苏州市.md "wikilink")，2000年更名为[虎丘区](../Page/虎丘区.md "wikilink")。
  - [郊区
    (南通市)](../Page/郊区_\(南通市\).md "wikilink")：江苏省[南通市曾设立的市辖区](../Page/南通市.md "wikilink")，1991年更名为[港闸区](../Page/港闸区.md "wikilink")。
  - [郊区
    (连云港市)](../Page/郊区_\(连云港市\).md "wikilink")：江苏省[连云港市曾设立的市辖区](../Page/连云港市.md "wikilink")，1983年与[盐区合并设立](../Page/盐区.md "wikilink")[连云区](../Page/连云区.md "wikilink")。
  - [郊区
    (盐城市)](../Page/郊区_\(盐城市\).md "wikilink")：江苏省[盐城市曾设立的市辖区](../Page/盐城市.md "wikilink")，1996年撤销，改设[盐都县](../Page/盐都县.md "wikilink")。
  - [郊区
    (扬州市)](../Page/郊区_\(扬州市\).md "wikilink")：江苏省[扬州市曾设立的市辖区](../Page/扬州市.md "wikilink")，2002年更名为[维扬区](../Page/维扬区.md "wikilink")。
  - [郊区
    (镇江市)](../Page/郊区_\(镇江市\).md "wikilink")：江苏省[镇江市曾设立的市辖区](../Page/镇江市.md "wikilink")，1984年更名为[润州区](../Page/润州区.md "wikilink")。

#### 浙江省

  - [郊区
    (杭州市)](../Page/郊区_\(杭州市\).md "wikilink")：[浙江省](../Page/浙江省.md "wikilink")[杭州市曾设立的市辖区](../Page/杭州市.md "wikilink")，1977年撤销，并入[西湖区](../Page/西湖区_\(杭州市\).md "wikilink")。
  - [郊区
    (宁波市)](../Page/郊区_\(宁波市\).md "wikilink")：浙江省[宁波市曾设立的市辖区](../Page/宁波市.md "wikilink")，1984年撤销，并入[江北区](../Page/江北区_\(宁波市\).md "wikilink")。
  - [郊区
    (嘉兴市)](../Page/郊区_\(嘉兴市\).md "wikilink")：浙江省[嘉兴市曾设立的市辖区](../Page/嘉兴市.md "wikilink")，1999年更名为[秀洲区](../Page/秀洲区.md "wikilink")。
  - [郊区
    (湖州市)](../Page/郊区_\(湖州市\).md "wikilink")：浙江省[湖州市曾设立的市辖区](../Page/湖州市.md "wikilink")，1988年撤销，辖境现属[南浔区](../Page/南浔区.md "wikilink")。

#### 安徽省

  - [郊区
    (合肥市)](../Page/郊区_\(合肥市\).md "wikilink")：[安徽省](../Page/安徽省.md "wikilink")[合肥市曾设立的市辖区](../Page/合肥市.md "wikilink")，2002年更名为[包河区](../Page/包河区.md "wikilink")。
  - [郊区
    (芜湖市)](../Page/郊区_\(芜湖市\).md "wikilink")：安徽省[芜湖市曾设立的市辖区](../Page/芜湖市.md "wikilink")，1990年撤销，辖境现属[镜湖区](../Page/镜湖区.md "wikilink")、[弋江区](../Page/弋江区.md "wikilink")、[鸠江区](../Page/鸠江区.md "wikilink")。
  - [郊区
    (蚌埠市)](../Page/郊区_\(蚌埠市\).md "wikilink")：安徽省[蚌埠市曾设立的市辖区](../Page/蚌埠市.md "wikilink")，2004年更名为[淮上区](../Page/淮上区.md "wikilink")。
  - [郊区
    (马鞍山市)](../Page/郊区_\(马鞍山市\).md "wikilink")：安徽省[马鞍山市曾设立的市辖区](../Page/马鞍山市.md "wikilink")，1979年撤销，辖境现属[花山区](../Page/花山区.md "wikilink")。
  - [郊区
    (淮北市)](../Page/郊区_\(淮北市\).md "wikilink")：安徽省[淮北市曾设立的市辖区](../Page/淮北市.md "wikilink")，1984年撤销，并入[杜集区](../Page/杜集区.md "wikilink")、[相山区](../Page/相山区.md "wikilink")、[烈山区](../Page/烈山区.md "wikilink")。
  - [郊区
    (安庆市)](../Page/郊区_\(安庆市\).md "wikilink")：安徽省[安庆市曾设立的市辖区](../Page/安庆市.md "wikilink")，2005年更名为[宜秀区](../Page/宜秀区.md "wikilink")。

#### 福建省

  - [郊区
    (福州市)](../Page/郊区_\(福州市\).md "wikilink")：[福建省](../Page/福建省.md "wikilink")[福州市曾设立的市辖区](../Page/福州市.md "wikilink")，1995年更名为[晋安区](../Page/晋安区.md "wikilink")。
  - [郊区
    (厦门市)](../Page/郊区_\(厦门市\).md "wikilink")：福建省[厦门市曾设立的市辖区](../Page/厦门市.md "wikilink")，1987年更名为[集美区](../Page/集美区.md "wikilink")。

#### 江西省

  - [郊区
    (南昌市)](../Page/郊区_\(南昌市\).md "wikilink")：[江西省](../Page/江西省.md "wikilink")[南昌市曾设立的市辖区](../Page/南昌市.md "wikilink")，2002年更名为[青山湖区](../Page/青山湖区.md "wikilink")。

#### 山东省

  - [郊区
    (济南市)](../Page/郊区_\(济南市\).md "wikilink")：[山东省](../Page/山东省.md "wikilink")[济南市曾设立的市辖区](../Page/济南市.md "wikilink")，1987年与[历城县合并设立](../Page/历城县.md "wikilink")[历城区](../Page/历城区.md "wikilink")。
  - [郊区
    (济宁市)](../Page/郊区_\(济宁市\).md "wikilink")：山东省[济宁市曾设立的市辖区](../Page/济宁市.md "wikilink")，1993年更名为[任城区](../Page/任城区.md "wikilink")。
  - [郊区
    (泰安市)](../Page/郊区_\(泰安市\).md "wikilink")：山东省[泰安市曾设立的市辖区](../Page/泰安市.md "wikilink")，2000年更名为[岱岳区](../Page/岱岳区.md "wikilink")。

#### 河南省

  - [郊区
    (郑州市)](../Page/郊区_\(郑州市\).md "wikilink")：[河南省](../Page/河南省.md "wikilink")[郑州市曾设立的市辖区](../Page/郑州市.md "wikilink")，1987年撤销，辖境现属[惠济区](../Page/惠济区.md "wikilink")。
  - [郊区
    (开封市)](../Page/郊区_\(开封市\).md "wikilink")：河南省[开封市曾设立的市辖区](../Page/开封市.md "wikilink")，2005年更名为[金明区](../Page/金明区.md "wikilink")。
  - [郊区
    (洛阳市)](../Page/郊区_\(洛阳市\).md "wikilink")：河南省[洛阳市曾设立的市辖区](../Page/洛阳市.md "wikilink")，2000年更名为[洛龙区](../Page/洛龙区.md "wikilink")。
  - [郊区
    (平顶山市)](../Page/郊区_\(平顶山市\).md "wikilink")：河南省[平顶山市曾设立的市辖区](../Page/平顶山市.md "wikilink")，1994年更名为[湛河区](../Page/湛河区.md "wikilink")。
  - [郊区
    (安阳市)](../Page/郊区_\(安阳市\).md "wikilink")：河南省[安阳市曾设立的市辖区](../Page/安阳市.md "wikilink")，2002年撤销，并入[文峰区](../Page/文峰区.md "wikilink")、[北关区](../Page/北关区.md "wikilink")、[殷都区](../Page/殷都区.md "wikilink")、[龙安区](../Page/龙安区.md "wikilink")。
  - [郊区
    (鹤壁市)](../Page/郊区_\(鹤壁市\).md "wikilink")：河南省[鹤壁市曾设立的市辖区](../Page/鹤壁市.md "wikilink")，2001年更名为[淇滨区](../Page/淇滨区.md "wikilink")。
  - [郊区
    (新乡市)](../Page/郊区_\(新乡市\).md "wikilink")：河南省[新乡市曾设立的市辖区](../Page/新乡市.md "wikilink")，2003年更名为[牧野区](../Page/牧野区.md "wikilink")。
  - [郊区
    (焦作市)](../Page/郊区_\(焦作市\).md "wikilink")：河南省[焦作市曾设立的市辖区](../Page/焦作市.md "wikilink")，1990年更名为[山阳区](../Page/山阳区.md "wikilink")。
  - [郊区
    (濮阳市)](../Page/郊区_\(濮阳市\).md "wikilink")：河南省[濮阳市曾设立的市辖区](../Page/濮阳市.md "wikilink")，1987年撤销，改设[濮阳县](../Page/濮阳县.md "wikilink")。

#### 湖北省

  - [郊区
    (黄石市)](../Page/郊区_\(黄石市\).md "wikilink")：[湖北省](../Page/湖北省.md "wikilink")[黄石市曾设立的市辖区](../Page/黄石市.md "wikilink")，1985年撤销，辖境现属[黄石港区](../Page/黄石港区.md "wikilink")、[西塞山区](../Page/西塞山区.md "wikilink")、[下陆区](../Page/下陆区.md "wikilink")。
  - [郊区
    (襄樊市)](../Page/郊区_\(襄樊市\).md "wikilink")：湖北省[襄樊市](../Page/襄樊市.md "wikilink")（今[襄阳市](../Page/襄阳市.md "wikilink")）曾设立的市辖区，1995年撤销，辖境现属[襄城区](../Page/襄城区.md "wikilink")、[樊城区](../Page/樊城区.md "wikilink")。

#### 湖南省

  - [郊区
    (长沙市)](../Page/郊区_\(长沙市\).md "wikilink")：[湖南省](../Page/湖南省.md "wikilink")[长沙市曾设立的市辖区](../Page/长沙市.md "wikilink")，1996年撤销，辖境现属[芙蓉区](../Page/芙蓉区.md "wikilink")、[天心区](../Page/天心区.md "wikilink")、[岳麓区](../Page/岳麓区.md "wikilink")、[开福区](../Page/开福区.md "wikilink")、[雨花区](../Page/雨花区.md "wikilink")。
  - [郊区
    (株洲市)](../Page/郊区_\(株洲市\).md "wikilink")：湖南省[株洲市曾设立的市辖区](../Page/株洲市.md "wikilink")，1997年撤销，辖境现属[荷塘区](../Page/荷塘区.md "wikilink")、[芦淞区](../Page/芦淞区.md "wikilink")、[石峰区](../Page/石峰区.md "wikilink")、[天元区](../Page/天元区.md "wikilink")。
  - [郊区
    (湘潭市)](../Page/郊区_\(湘潭市\).md "wikilink")：湖南省[湘潭市曾设立的市辖区](../Page/湘潭市.md "wikilink")，1992年撤销，辖境现属[雨湖区](../Page/雨湖区.md "wikilink")、[岳塘区](../Page/岳塘区.md "wikilink")。
  - [郊区
    (衡阳市)](../Page/郊区_\(衡阳市\).md "wikilink")：湖南省[衡阳市曾设立的市辖区](../Page/衡阳市.md "wikilink")，2001年撤销，辖境现属[珠晖区](../Page/珠晖区.md "wikilink")、[雁峰区](../Page/雁峰区.md "wikilink")、[石鼓区](../Page/石鼓区.md "wikilink")、[蒸湘区](../Page/蒸湘区.md "wikilink")。
  - [郊区
    (邵阳市)](../Page/郊区_\(邵阳市\).md "wikilink")：湖南省[邵阳市曾设立的市辖区](../Page/邵阳市.md "wikilink")，1997年撤销，辖境现属[双清区](../Page/双清区.md "wikilink")、[大祥区](../Page/大祥区.md "wikilink")、[北塔区](../Page/北塔区.md "wikilink")。
  - [郊区
    (岳阳市)](../Page/郊区_\(岳阳市\).md "wikilink")：湖南省[岳阳市曾设立的市辖区](../Page/岳阳市.md "wikilink")，1996年撤销，辖境现属[岳阳楼区](../Page/岳阳楼区.md "wikilink")、[君山区](../Page/君山区.md "wikilink")。

#### 广东省

  - [郊区
    (广州市)](../Page/郊区_\(广州市\).md "wikilink")：[广东省](../Page/广东省.md "wikilink")[广州市曾设立的市辖区](../Page/广州市.md "wikilink")，1987年更名为[白云区](../Page/白云区_\(广州市\).md "wikilink")。
  - [郊区
    (汕头市)](../Page/郊区_\(汕头市\).md "wikilink")：广东省[汕头市曾设立的市辖区](../Page/汕头市.md "wikilink")，1991年撤销，辖境现属[龙湖区](../Page/龙湖区.md "wikilink")、[金平区](../Page/金平区.md "wikilink")。
  - [郊区
    (江门市)](../Page/郊区_\(江门市\).md "wikilink")：广东省[江门市曾设立的市辖区](../Page/江门市.md "wikilink")，1994年更名为[蓬江区](../Page/蓬江区.md "wikilink")。
  - [郊区
    (湛江市)](../Page/郊区_\(湛江市\).md "wikilink")：广东省[湛江市曾设立的市辖区](../Page/湛江市.md "wikilink")，1994年更名为[麻章区](../Page/麻章区.md "wikilink")。
  - [郊区
    (河源市)](../Page/郊区_\(河源市\).md "wikilink")：广东省[河源市曾设立的市辖区](../Page/河源市.md "wikilink")，1993年撤销，改设[东源县](../Page/东源县.md "wikilink")。

#### 广西壮族自治区

  - [郊区
    (南宁市)](../Page/郊区_\(南宁市\).md "wikilink")：[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[南宁市曾设立的市辖区](../Page/南宁市.md "wikilink")，2001年撤销，辖境现属[兴宁区](../Page/兴宁区.md "wikilink")、[青秀区](../Page/青秀区.md "wikilink")、[江南区](../Page/江南区_\(南宁市\).md "wikilink")、[西乡塘区](../Page/西乡塘区.md "wikilink")。
  - [郊区
    (柳州市)](../Page/郊区_\(柳州市\).md "wikilink")：广西壮族自治区[柳州市曾设立的市辖区](../Page/柳州市.md "wikilink")，2002年撤销，并入[城中区](../Page/城中区_\(柳州市\).md "wikilink")、[鱼峰区](../Page/鱼峰区.md "wikilink")、[柳南区](../Page/柳南区.md "wikilink")、[柳北区](../Page/柳北区.md "wikilink")。
  - [郊区
    (桂林市)](../Page/郊区_\(桂林市\).md "wikilink")：广西壮族自治区[桂林市曾设立的市辖区](../Page/桂林市.md "wikilink")，1996年更名为[雁山区](../Page/雁山区.md "wikilink")。
  - [郊区 (梧州市)](../Page/郊区_\(梧州市\).md "wikilink"):
    广西壮族自治区[梧州市曾设立的市辖区](../Page/梧州市.md "wikilink")，2003年撤销，辖境现属[万秀区](../Page/万秀区.md "wikilink")、[长洲区](../Page/长洲区.md "wikilink")。
  - [郊区 (北海市)](../Page/郊区_\(北海市\).md "wikilink"):
    广西壮族自治区[北海市曾设立的市辖区](../Page/北海市.md "wikilink")，1994年撤销，辖境现属[海城区](../Page/海城区.md "wikilink")、[银海区](../Page/银海区.md "wikilink")。

#### 四川省

  - [郊区
    (自贡市)](../Page/郊区_\(自贡市\).md "wikilink")：四川省[自贡市曾设立的市辖区](../Page/自贡市.md "wikilink")，1983年更名为[沿滩区](../Page/沿滩区.md "wikilink")。
  - [郊区
    (渡口市)](../Page/郊区_\(渡口市\).md "wikilink")：四川省[渡口市](../Page/渡口市.md "wikilink")（今[攀枝花市](../Page/攀枝花市.md "wikilink")）曾设立的市辖区，1981年更名为[仁和区](../Page/仁和区.md "wikilink")。

#### 陕西省

  - [郊区
    (西安市)](../Page/郊区_\(西安市\).md "wikilink")：[陕西省](../Page/陕西省.md "wikilink")[西安市曾设立的市辖区](../Page/西安市.md "wikilink")，1980年撤销，辖境现属[灞桥区](../Page/灞桥区.md "wikilink")、[未央区](../Page/未央区.md "wikilink")、[雁塔区](../Page/雁塔区.md "wikilink")。
  - [郊区
    (铜川市)](../Page/郊区_\(铜川市\).md "wikilink")：陕西省[铜川市曾设立的市辖区](../Page/铜川市.md "wikilink")，2000年更名为[印台区](../Page/印台区.md "wikilink")。

#### 青海省

  - [郊区
    (西宁市)](../Page/郊区_\(西宁市\).md "wikilink")：[青海省](../Page/青海省.md "wikilink")[西宁市曾设立的市辖区](../Page/西宁市.md "wikilink")，1986年撤销，并入[城东区](../Page/城东区_\(西宁市\).md "wikilink")、[城中区](../Page/城中区_\(西宁市\).md "wikilink")、[城西区](../Page/城西区.md "wikilink")、[城北区](../Page/城北区_\(西宁市\).md "wikilink")。

#### 宁夏回族自治区

  - [郊区
    (银川市)](../Page/郊区_\(银川市\).md "wikilink")：[宁夏回族自治区](../Page/宁夏回族自治区.md "wikilink")[银川市曾设立的市辖区](../Page/银川市.md "wikilink")，2002年撤销，辖境现属[兴庆区](../Page/兴庆区.md "wikilink")、[西夏区](../Page/西夏区.md "wikilink")、[金凤区](../Page/金凤区.md "wikilink")。
  - [郊区
    (石嘴山市)](../Page/郊区_\(石嘴山市\).md "wikilink")：宁夏回族自治区[石嘴山市曾设立的市辖区](../Page/石嘴山市.md "wikilink")，1987年撤销，改设[惠农县](../Page/惠农县.md "wikilink")。

### 名称含“郊区”的市辖区

  - [东郊区
    (天津市)](../Page/东郊区_\(天津市\).md "wikilink")：[天津市曾设立的市辖区](../Page/天津市.md "wikilink")，1992年更名为[东丽区](../Page/东丽区.md "wikilink")。
  - [南郊区
    (天津市)](../Page/南郊区_\(天津市\).md "wikilink")：天津市曾设立的市辖区，1992年更名为[津南区](../Page/津南区.md "wikilink")。
  - [西郊区
    (天津市)](../Page/西郊区_\(天津市\).md "wikilink")：天津市曾设立的市辖区，1992年更名为[西青区](../Page/西青区.md "wikilink")。
  - [北郊区
    (天津市)](../Page/北郊区_\(天津市\).md "wikilink")：天津市曾设立的市辖区，1992年更名为[北辰区](../Page/北辰区.md "wikilink")。
  - [南郊区
    (太原市)](../Page/南郊区_\(太原市\).md "wikilink")：山西省[太原市曾设立的市辖区](../Page/太原市.md "wikilink")，1997年撤销，辖境现属[小店区](../Page/小店区.md "wikilink")、[迎泽区](../Page/迎泽区.md "wikilink")、[杏花岭区](../Page/杏花岭区.md "wikilink")、[晋源区](../Page/晋源区.md "wikilink")。
  - [北郊区
    (太原市)](../Page/北郊区_\(太原市\).md "wikilink")：山西省太原市曾设立的市辖区，1997年撤销，辖境现属[杏花岭区](../Page/杏花岭区.md "wikilink")、[尖草坪区](../Page/尖草坪区.md "wikilink")、[万柏林区](../Page/万柏林区.md "wikilink")。
  - [南郊区
    (大同市)](../Page/南郊区_\(大同市\).md "wikilink")：山西省[大同市曾设立的市辖区](../Page/大同市.md "wikilink")，2018年撤销，辖境现属[新荣区](../Page/新荣区.md "wikilink")、[平城区](../Page/平城区.md "wikilink")、[云冈区](../Page/云冈区.md "wikilink")。
  - [北郊区
    (大同市)](../Page/北郊区_\(大同市\).md "wikilink")：山西省大同市曾设立的市辖区，1972年更名为[新荣区](../Page/新荣区.md "wikilink")。
  - [清郊区](../Page/清郊区.md "wikilink")：广东省[清远市曾设立的市辖区](../Page/清远市.md "wikilink")，1992年撤销，改设[清新县](../Page/清新县.md "wikilink")。