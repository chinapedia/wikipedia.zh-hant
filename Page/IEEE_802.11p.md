**IEEE 802.11p**（又稱**WAVE**，**Wireless Access in the Vehicular
Environment**）是一個由[IEEE
802.11](../Page/IEEE_802.11.md "wikilink")[標準擴充的](../Page/標準.md "wikilink")[通訊協定](../Page/通訊協定.md "wikilink")。這個通訊協定主要用在車用電子的[無線通訊上](../Page/無線通訊.md "wikilink")。它設定上是從[IEEE
802.11來擴充延伸](../Page/IEEE_802.11.md "wikilink")，來符合[智慧型運輸系統](../Page/智慧型運輸系統.md "wikilink")（Intelligent
Transportation
Systems，ITS）的相關應用。應用的層-{面}-包括高速率的车辆之间以及车辆与5.9千兆赫（5.85-5.925千兆赫）波段的標準ITS路边基础设施之間的資料数据交换。[IEEE
1609標準則是以IEEE](../Page/IEEE_1609.md "wikilink")
802.11p通訊協定為基礎的高[層標準](../Page/OSI模型#.E5.B1.82.E6.AC.A1.E5.88.92.E5.88.86.md "wikilink")。\[1\]

802.11p將被用在[車載通訊](../Page/車載通訊.md "wikilink")（或稱[專用短距離通訊](../Page/專用短距離通訊.md "wikilink")，Dedicated
Short Range
Communications，DSRC）系統中，這是一個[美國交通部](../Page/美國交通部.md "wikilink")（U.S.
Department of
Transportation）基於[歐洲針對車輛的通訊網路](../Page/歐洲.md "wikilink")，特別是[電子道路收費系統](../Page/電子道路收費系統.md "wikilink")、车辆安全服务與車上的商業交易系統等等的應用而設計[中長距繼續傳播隔空接口](../Page/中長距繼續傳播空氣介面.md "wikilink")（Continuous
Air interfaces - Long and Medium
Range，CALM）系統的計畫。該計畫最終的願景是建立一個允許車輛與路邊[無線接取器或是其他車輛間的通訊的全国性网络](../Page/無線接取器.md "wikilink")。这项工作是建立在其前導的[ASTM
E2213-03計畫的基础上](../Page/ASTM_E2213-03.md "wikilink")。\[2\]

## 目前狀態

该802.11p工作小组仍然是活動中的。按照官方正式的802.11工作计划時間表，预定将于2009年4月公布核定的802.11p修正案。\[3\]

## 其他地區

香港2001年起進行電子道路收費可行性研究，有計劃使用802.11p作為香港的電子道路收費標淮；計劃於2010年起在所有新申領或續領行車證的車輛中加設有關裝置
\[4\]。

## 參考文獻

## 延伸閱讀

  - IEEE Draft Std P802.11p/D1.0, February 2006 , Part 11:wireless LAN
    Medium
  - 曹燿麟，《互補碼車用高速移動無線網路之性能分析》，[國立中山大學碩士論文](../Page/國立中山大學.md "wikilink")，2007年
  - 呂懿慧，《通訊技術於智慧型運輸系統（ITS）之應用硏究：以DSRC為例》[新竹縣](../Page/新竹縣.md "wikilink")[竹東鎮](../Page/竹東鎮.md "wikilink")：[財團法人工業技術研究院產業經濟與資訊服務中心出版](../Page/財團法人工業技術研究院.md "wikilink")，2003年
    ISBN 957-774-580-6
  - 鄒典齊，《移動性正交頻分多工系統之有效的通道追蹤方法》，[元智大學碩士論文](../Page/元智大學.md "wikilink")，2006年
  - 陳翰緯，《利用辨碼指引解調及一階最小均方差頻域等化器為基礎之正交頻分多工接收器架構應用於短距無線通訊之性能分析》，[元智大學碩士論文](../Page/元智大學.md "wikilink")，2005年
  - 周俊祥，《車載通訊系統基頻處理平台研究》，[元智大學碩士論文](../Page/元智大學.md "wikilink")，2004年
  - 林盟翔，《利用一階最小均方差頻域等化器之正交頻分多工接收器架構應用於短距無線通訊之性能表現》，[元智大學碩士論文](../Page/元智大學.md "wikilink")，2005年
  - 劉如傑，《適用於高速移動環境下的無線區域網路通道追蹤技術》，[中正大學碩士論文](../Page/中正大學.md "wikilink")，2006年

## 外部連結

  - [802.11p計畫的狀態](http://grouper.ieee.org/groups/802/11/Reports/tgp_update.htm)
    IEEE Task Group TGp

<!-- end list -->

  - [什麼是車載通訊(DSRC)?](https://web.archive.org/web/20070426092145/http://grouper.ieee.org/groups/scc32/dsrc/)
    IEEE的文章，有demo可看

  - {{〈}}[Intelligent Transportation
    gets 802.11p](http://dailywireless.org/modules.php?name=News&file=article&sid=2815&src=rss09){{〉}}《Daily
    Wireless》2004年7月15日

  - Dan Benjamin，{{〈}}[Could 802.11p spell the end for cellular in the
    automobile?](https://archive.is/20060324124446/http://www.abiresearch.com/products/insight/Could_80211p_spell_the_end_for_cellular_in_the_automobile){{〉}}《ABI
    Research》2004年7月14日

  - Adam Stone，{{〈}}[When Wi-Fi Will
    Drive](http://www.wi-fiplanet.com/columns/article.php/3422251){{〉}}
    《Wi-Fi Planet》2004年10月15日

  - 鄧友清，{{〈}}[車用規格IEEE 802.11p介紹與未來發展](http://www.itri.org.tw/chi/services/ieknews/200509151040044D6BE-0.doc){{〉}}（doc檔），工研院IEK-IT
    IS計畫，2005年

  - [WAVE 與
    IEEE 1609](http://3fire.blogspot.com/2007/06/wave-ieee-1609.html)，2007年7月2日

  - [车载通信DSRC技术和通信机制研究](http://202.108.83.68/autoinfo_cn/pkdgnc/ztfx/webinfo/2007/09/05/1188558955208087.htm)

  - [車際通訊技術與願景](http://angle-zulu.blogspot.com/2005/01/blog-post_15.html)

  - [IEEE 802.11p日形完備　車載資通訊網路環環相扣](http://www.2cm.com.tw/technologyshow_content.asp?sn=0906220012)

### 相關新聞報導

  - {{〈}}[802.11p将受汽车厂商欢迎，蜂窝和无线宽带仍有市场](http://www.esmchina.com/ART_8800057225_1300_2604_0_0_f8223f4d.HTM){{〉}}於
    [國際電子商情](http://www.esmchina.com/)，2004年8月9日
  - {{〈}}[IEEE完成四项汽车环境中无线通信标准](http://www.cww.net.cn/tech/html/2007/10/10/200710101111146560.htm){{〉}}，[通信世界網](http://www.cww.net.cn/)，2007年10月10日
  - {{〈}}[IEEE批准通過IEEE1609.3汽車環境無線通信標準](http://210.64.8.60/china/news/mobi/mobi200710250111.html){{〉}}，[技術在線](https://web.archive.org/web/20071128025230/http://china.nikkeibp.co.jp/)，2007年10月25日
  - {{〈}}[IEEE四标准试用
    批准通过有关汽车无线通信系列标准](http://www.standardcn.com/article/show.asp?id=14449){{〉}}，[標準網](http://www.standardcn.com/)，2007年11月14日

## 参见

  - [IEEE 802.11](../Page/IEEE_802.11.md "wikilink")
  - [IEEE 802.1](../Page/IEEE_802.1.md "wikilink")
  - [IEEE 802](../Page/IEEE_802.md "wikilink")

{{-}}

[Category:網路標準](../Category/網路標準.md "wikilink") [Category:IEEE
802.11](../Category/IEEE_802.11.md "wikilink")
[Category:Wi-Fi](../Category/Wi-Fi.md "wikilink")
[Category:无线通信](../Category/无线通信.md "wikilink")

1.
2.
3.
4.