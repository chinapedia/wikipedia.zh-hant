[Reusable_metal_chopsticks.JPG](https://zh.wikipedia.org/wiki/File:Reusable_metal_chopsticks.JPG "fig:Reusable_metal_chopsticks.JPG")
[Reusable_metal_chopsticks_closeup.JPG](https://zh.wikipedia.org/wiki/File:Reusable_metal_chopsticks_closeup.JPG "fig:Reusable_metal_chopsticks_closeup.JPG")
**環保筷**，相對一次性的[便利筷](../Page/便利筷.md "wikilink")，就是可以重複使用的筷子，一般是指外出用餐時，自備的筷子。環保筷在中國又稱折疊筷、便攜筷、隨身筷。

很多的餐廳為了方便、衛生及節省成本等理由，為客人提供一次性的免洗筷（或稱棄木筷）。但是，其耗用量對環境造成很大的傷害。製造全球每年所消耗的800億對免洗筷就需要砍掉264萬棵樹。\[1\]除了對環境造成傷害外，在製作免洗筷的過程中，可能會在筷子上殘留過量的二氧化硫，危害健康。\[2\]因此，基於環保及健康之考慮，環保團體及一些宗教團體積極鼓勵民眾隨身攜帶環保筷。\[3\]

## 型制

## 參見

  - [筷子](../Page/筷子.md "wikilink")

## 参考文獻

[Category:餐具](../Category/餐具.md "wikilink")
[Category:東亞飲食文化](../Category/東亞飲食文化.md "wikilink")

1.  全球年耗800億雙免洗筷。香港明報，2008年8月3日。轉載自苦勞網。[1](http://www.coolloud.org.tw/node/24790)
2.
3.  環保筷隨身宣言。佛教慈濟基金會，2008年4月22日。