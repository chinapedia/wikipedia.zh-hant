**新城勁爆電視大獎頒獎禮**由[新城娛樂台舉辦](../Page/新城電台.md "wikilink")，於2005年開始舉行，前為《點正娛樂黑白新視『角』嘉獎大典》，用作表揚過去一年的電視藝員，大部份都由[電視廣播有限公司及](../Page/電視廣播有限公司.md "wikilink")[亚洲電視的藝員獲獎](../Page/亚洲電視.md "wikilink")，為[香港地區第五個電視頒獎禮](../Page/香港.md "wikilink")，只舉辦了兩年。

## 歷屆頒獎禮

*為免條目冗長，歷屆得獎名單將在下列按年分述。本條目只顯示最近一屆的簡單概況*

### 最近一屆

  - 名稱：新城勁爆電視大獎頒獎禮2005
  - 舉辦日期：2005年
  - 得獎名單

<table>
<caption><em>註：這裡只顯示最近一屆的得獎名單</em></caption>
<tbody>
<tr class="odd">
<td><p>新城勁爆電視演員</p></td>
<td><p><a href="../Page/馬德鐘.md" title="wikilink">馬德鐘</a>、<a href="../Page/郭晉安.md" title="wikilink">郭晉安</a>、<a href="../Page/郭可盈.md" title="wikilink">郭可盈</a><br />
<a href="../Page/汪明荃.md" title="wikilink">汪明荃</a>、<a href="../Page/佘詩曼.md" title="wikilink">佘詩曼</a>、<a href="../Page/林保怡.md" title="wikilink">林保怡</a><br />
<a href="../Page/陳豪.md" title="wikilink">陳豪</a>、<a href="../Page/黎姿.md" title="wikilink">黎姿</a>、<a href="../Page/苗僑偉.md" title="wikilink">苗僑偉</a>、<a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a><br />
<a href="../Page/胡杏兒.md" title="wikilink">胡杏兒</a>、<a href="../Page/吳卓羲.md" title="wikilink">吳卓羲</a></p></td>
</tr>
<tr class="even">
<td><p>新城勁爆電視劇</p></td>
<td><p>《<a href="../Page/我的野蠻奶奶.md" title="wikilink">我的野蠻奶奶</a>》</p></td>
</tr>
<tr class="odd">
<td><p>新城勁爆男主角</p></td>
<td><p>《<a href="../Page/阿旺新傳.md" title="wikilink">阿旺新傳</a>》 ——<a href="../Page/郭晉安.md" title="wikilink">郭晉安</a></p></td>
</tr>
<tr class="even">
<td><p>新城勁爆女主角</p></td>
<td><p>《<a href="../Page/酒店風雲.md" title="wikilink">酒店風雲</a>》 ——<a href="../Page/郭可盈.md" title="wikilink">郭可盈</a><br />
《<a href="../Page/我的野蠻奶奶.md" title="wikilink">我的野蠻奶奶</a>》 ——<a href="../Page/汪明荃.md" title="wikilink">汪明荃</a></p></td>
</tr>
<tr class="odd">
<td><p>新城勁爆男配角</p></td>
<td><p>《<a href="../Page/阿旺新傳.md" title="wikilink">阿旺新傳</a>》 ——<a href="../Page/秦沛.md" title="wikilink">秦沛</a></p></td>
</tr>
<tr class="even">
<td><p>新城勁爆女配角</p></td>
<td><p>《<a href="../Page/阿旺新傳.md" title="wikilink">阿旺新傳</a>》 ——<a href="../Page/湯盈盈.md" title="wikilink">湯盈盈</a></p></td>
</tr>
<tr class="odd">
<td><p>新城勁爆飛躍藝員</p></td>
<td><p><a href="../Page/葉璇.md" title="wikilink">葉璇</a></p></td>
</tr>
<tr class="even">
<td><p>新城勁爆男人氣王</p></td>
<td><p><a href="../Page/黃宗澤.md" title="wikilink">黃宗澤</a></p></td>
</tr>
<tr class="odd">
<td><p>新城勁爆女人氣王</p></td>
<td><p><a href="../Page/廖碧兒.md" title="wikilink">廖碧兒</a></p></td>
</tr>
<tr class="even">
<td><p>新城勁爆新人王</p></td>
<td><p><a href="../Page/薛凱琪.md" title="wikilink">薛凱琪</a></p></td>
</tr>
<tr class="odd">
<td><p>新城勁爆節目主持</p></td>
<td><p>《<a href="../Page/繼續無敵獎門人.md" title="wikilink">繼續無敵獎門人</a>》 ——<a href="../Page/曾志偉.md" title="wikilink">曾志偉</a></p></td>
</tr>
</tbody>
</table>

### 其他年度

  - [2004年](../Page/2004年度點正娛樂黑白新視『角』嘉獎大典得獎名單.md "wikilink")
  - [2005年](../Page/2005年度新城勁爆電視大獎頒獎禮得獎名單.md "wikilink")

## 參見

  - [新城電台](../Page/新城電台.md "wikilink")

## 外部連結

[Category:新城勁爆電視大獎頒獎禮](../Category/新城勁爆電視大獎頒獎禮.md "wikilink")
[Category:香港電視頒獎典禮](../Category/香港電視頒獎典禮.md "wikilink")