[Yeosu_Airport.JPG](https://zh.wikipedia.org/wiki/File:Yeosu_Airport.JPG "fig:Yeosu_Airport.JPG")\]\]
[Inauguraci%C3%B3n_Expo_Yeosu_2012.jpg](https://zh.wikipedia.org/wiki/File:Inauguraci%C3%B3n_Expo_Yeosu_2012.jpg "fig:Inauguraci%C3%B3n_Expo_Yeosu_2012.jpg")
**麗水市**（）是[大韓民國](../Page/大韓民國.md "wikilink")[全羅南道的一个](../Page/全羅南道.md "wikilink")[市级行政区](../Page/市.md "wikilink")，面积503.33
平方公里，人口超30万，是[2012年世界博覽會主办城市](../Page/2012年世界博覽會.md "wikilink")。

## 历史

[百济時代稱為源村縣以及突山縣](../Page/百济.md "wikilink")，[統一新羅時代的](../Page/新羅.md "wikilink")757年開始有麗山之名。[高麗時代的](../Page/高麗.md "wikilink")940年起使用麗水作為地名。

### 麗水市、麗川市

  - 1949年8月13日 - 麗水郡麗水邑昇格为**麗水府**。
  - 1949年8月15日 - 麗水府改称为**麗水市**。
  - 1986年1月1日 - 麗川郡三日邑和双鳳面合并建立**麗川市**。
  - 1990年4月 -
    麗水～[博多之间的](../Page/博多港.md "wikilink")[博麗輪渡开始启航](../Page/博麗輪渡.md "wikilink")。
  - 1992年 - 博麗輪渡因經營不善而暫停航運。
  - 1998年4月1日 - 麗水市、麗川市、麗川郡合併為**麗水市**（1邑6面）。政府大楼設置在旧麗川市廳。

### 麗川郡（麗水郡）

  - 1897年 - [順天郡的一部份地區另設為麗水郡](../Page/順天市.md "wikilink")。
  - 1914年4月1日 -
    郡面合併，[突山郡的一部分](../Page/突山郡.md "wikilink")（斗南面、南面、華蓋面、三山面以及玉井面的大部分、太仁面的猫島）編入**麗水郡**。麗水郡成立以下的幾個面（10面）。
      - 麗水面、召羅面、栗村面、三日面、双鳳面、華陽面、斗南面、南面、華井面、三山面
  - 1917年4月1日 - 斗南面改称為突山面（10面）。
  - 1930年12月20日 -
    [川崎汽船的](../Page/川崎汽船.md "wikilink")[關麗連絡船起航](../Page/關麗連絡船.md "wikilink")。
  - 1930年12月25日 -
    [南朝鮮鐵道在麗水至](../Page/南朝鮮鐵道.md "wikilink")[光州之間開通鐵道](../Page/光州広域市.md "wikilink")。
  - 1931年4月1日 - 麗水面昇格為麗水邑（1邑9面）。
  - 1945年5月 - 川崎汽船的[關麗航路暫停營運](../Page/下關市.md "wikilink")。
  - 1948年10月 -
    [国防警備隊在麗水發生暴動](../Page/国防警備隊.md "wikilink")，并擴大到[順天](../Page/順天市.md "wikilink")（[麗水-順天事件](../Page/麗水-順天事件.md "wikilink")）。
  - 1949年8月13日（9面）
      - 麗水邑昇格為**麗水府**。
      - 麗水郡改稱為**麗川郡**。
  - 1973年7月1日 - [光陽郡骨若面的一部分](../Page/光陽市.md "wikilink")（松里）編入栗村面（9面）。
  - 1980年12月1日（2邑7面）
      - 三日面昇格為三日邑。
      - 突山面昇格為突山邑。
  - 1986年1月1日 - 三日邑、双鳳面合併為**麗川市**（1邑6面）。
  - 1998年4月1日 - 麗川郡與麗水市、麗川市合併為**麗水市**。麗川郡消失。

## 气候

  - 最高气温极值37.1℃（1994年7月20日）
  - 最低气温极值-12.6℃（1977年2月16日）

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/唐津市.md" title="wikilink">唐津市</a></p></li>
<li><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a><a href="../Page/杭州市.md" title="wikilink">杭州市</a></p></li>
<li><p><a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a><a href="../Page/威海市.md" title="wikilink">威海市</a></p></li>
<li><p><a href="../Page/菲律宾.md" title="wikilink">菲律宾</a><a href="../Page/宿霧市.md" title="wikilink">宿霧市</a></p></li>
<li><p><a href="../Page/美利坚合众国.md" title="wikilink">美利坚合众国</a><a href="../Page/纽波特比奇_(加利福尼亚州).md" title="wikilink">纽波特比奇</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/美利坚合众国.md" title="wikilink">美利坚合众国</a><a href="../Page/賽克斯頓_(密蘇里州).md" title="wikilink">賽克斯頓</a></p></li>
<li><p><a href="../Page/俄罗斯.md" title="wikilink">俄罗斯</a><a href="../Page/瓦尼诺.md" title="wikilink">瓦尼诺</a></p></li>
<li><p><a href="../Page/特里尼达和多巴哥.md" title="wikilink">特里尼达和多巴哥</a><a href="../Page/西班牙港.md" title="wikilink">西班牙港</a></p></li>
<li><p><a href="../Page/墨西哥.md" title="wikilink">墨西哥</a><a href="../Page/克雷塔罗.md" title="wikilink">克雷塔罗</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部链接

  - [丽水市政府主页](http://www.yeosu.go.kr/)

[麗水市_(韓國)](../Category/麗水市_\(韓國\).md "wikilink")