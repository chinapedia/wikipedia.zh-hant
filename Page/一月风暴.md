**一月风暴**，是中国大陆[文化大革命期间的重要事件](../Page/文化大革命.md "wikilink")，事件于1967年1月在[上海开始](../Page/上海.md "wikilink")，由于[毛泽东的认可而进一步推动了全国的](../Page/毛泽东.md "wikilink")[夺权运动](../Page/夺权运动.md "wikilink")。一月风暴这一称呼出自《[人民日报](../Page/人民日报.md "wikilink")》社论，文革期间也有称“一月革命”，是[张春桥等意欲与](../Page/张春桥.md "wikilink")[苏俄](../Page/苏俄.md "wikilink")[十月革命相应](../Page/十月革命.md "wikilink")、比肩的对称，后来也直称“一月夺权”。

## 背景

1966年底，上海工总司负责人[王洪文等人制造了矛头直接指向上海市委的](../Page/王洪文.md "wikilink")“[安亭事件](../Page/安亭事件.md "wikilink")”、“[解放日报事件](../Page/解放日报事件.md "wikilink")”和“[康平路事件](../Page/康平路事件.md "wikilink")”，造成上海党政系统的瘫痪和社会生活极度混乱。1967年1月1日，《人民日报》、《[红旗](../Page/紅旗雜誌.md "wikilink")》杂志发表了题为《[把无产阶级文化大革命进行到底](../Page/把无产阶级文化大革命进行到底.md "wikilink")》的[元旦](../Page/元旦.md "wikilink")[社论](../Page/社论.md "wikilink")，提出1967年“将是全国开展阶级斗争的一年”，“将是[无产阶级联合其他革命群众向党内一小撮走](../Page/无产阶级.md "wikilink")[资本主义道路的当权派和社会上的](../Page/资本主义.md "wikilink")[牛鬼蛇神](../Page/牛鬼蛇神.md "wikilink")，开展总进攻的一年”。在这种思想和社会十分混乱的情况下，1967年1月初，张春桥、[姚文元以](../Page/姚文元.md "wikilink")“[中央文革小组调查员](../Page/中央文革小组调查员.md "wikilink")”的身份到上海策划夺权\[1\]。

## 经过

1967年1月4日《[文匯報](../Page/文匯報_\(上海\).md "wikilink")》的权被夺，并发表夺权宣言。5日，又夺了《[解放日报](../Page/解放日报.md "wikilink")》的权，声明不再是中共上海市委的机关报。6日，张春桥、姚文元借工总司等全市各造反组织名义，召开“打倒市委大会”，批斗了[中共中央华东局](../Page/中共中央华东局.md "wikilink")、中共上海市委、市人委的主要负责人[陈丕显](../Page/陈丕显.md "wikilink")、[曹荻秋](../Page/曹荻秋.md "wikilink")、[魏文伯等](../Page/魏文伯.md "wikilink")，并将全市几百名局级以上干部揪到会场陪鬥。大会发出三项通令：

  - 宣布不再承认[曹荻秋为上海市市长](../Page/曹荻秋.md "wikilink")；
  - 勒令上海市委书记[陈丕显交代所谓](../Page/陈丕显.md "wikilink")“[反革命罪行](../Page/反革命罪行.md "wikilink")”；
  - 要求[中共中央彻底改组上海市委](../Page/中共中央.md "wikilink")。

会后，[中共上海市委](../Page/中共上海市委.md "wikilink")、[上海市人民委员会所有机构被迫停止办公](../Page/上海市人民委员会.md "wikilink")，权力落到张、姚、王等人手里。这次大会是上海“一月革命”的开端和标志。

1月8日，在张春桥、姚文元指挥下，成立“上海市抓革命促生产火线指挥部”成为全市生产的实际领导机构。张春桥称之为“经济[苏维埃](../Page/苏维埃.md "wikilink")”。随后又成立“[无产阶级文化大革命保卫委员会](../Page/无产阶级.md "wikilink")”，取代公安、司法机构。还企图用上海市“造反组织联络站”，取代[中共上海市委](../Page/中共上海市委.md "wikilink")。同日，[毛泽东对上海造反派的夺权活动表示支持](../Page/毛泽东.md "wikilink")，表示“这是一个阶级推翻一个阶级，这是一场大革命”，“两个报纸夺权，这是全国性的问题。我们要支持他们造反”，“上海革命力量起来，全国就有希望。它不能不影响整个[华东](../Page/华东.md "wikilink")，影响全国各省市”。9日，《人民日报》发表上海造反组织《告上海全市人民书》，并加《编者按》传达了毛泽东的意见\[2\]。

11日，[中央文革小组为中共中央](../Page/中央文革小组.md "wikilink")、国务院起草了致上海各“革命造反团体”的贺电，并号召全国学习上海“[造反派](../Page/造反派.md "wikilink")”的经验。22日，《人民日报》发表[社论](../Page/社论.md "wikilink")，认为“一月风暴”是“今年展开全国全面[阶级斗争的一个伟大开端](../Page/阶级斗争.md "wikilink")”，号召“从党内[一小撮](../Page/一小撮.md "wikilink")[走资本主义道路当权派和坚持](../Page/走资派.md "wikilink")[资产阶级](../Page/资产阶级.md "wikilink")[反动路线的顽固分子手里](../Page/反动.md "wikilink")，自下而上地夺权”。在这一时期“上海市[红卫兵](../Page/红卫兵.md "wikilink")[革命委员会](../Page/革命委员会.md "wikilink")”（简称“红革会”）等其他组织曾经先后发动过四次全市性夺权，都被[张春桥等人分化](../Page/张春桥.md "wikilink")、搞垮、镇压。2月5日，正式成立[上海人民公社](../Page/上海人民公社.md "wikilink")\[3\]。后来由于毛泽东不赞成用[公社之名](../Page/公社.md "wikilink")，于24日改名为[上海市革命委员会](../Page/上海市革命委员会.md "wikilink")。张春桥为主任，姚文元、王洪文、徐景贤等为副主任。

## 影响

上海“一月风暴”在全国产生了广泛而强烈的影响，引起一系列连锁反应，[山西](../Page/山西.md "wikilink")（1月14日）、[山东](../Page/山东.md "wikilink")（2月3日）、[黑龙江](../Page/黑龙江省.md "wikilink")（1月31日）、[贵州](../Page/贵州.md "wikilink")（1月25日）等省纷纷夺权。从此，“文化大革命”进入了全面夺权的新阶段。夺权引起的震荡，革委会筹备委员会的权力争夺中进一步搞乱了全国，造成派性分裂，迫害了大批干部；同时[林彪集团和](../Page/林彪.md "wikilink")[中央文革小组支持的造反派在各地乘机膨胀](../Page/中央文革小组.md "wikilink")，扶植党羽，攫取了相当部分党政大权。

1967年[天马电影制片厂](../Page/天马电影制片厂.md "wikilink")“造反派”拍摄完成纪录片《一月革命》（5本）。同年创作、排演了话剧《一月风暴》，演员[章非饰张主任一角](../Page/章非.md "wikilink")。1970年创作了电影剧本《一月风暴》并投入拍摄。

## 参考文献

  - 席宣、金春明：《“文化大革命”简史》，北京：中共党史出版社，1996年
  - 廖盖隆等 主编：《中华人民共和国编年史》，郑州：河南人民出版社，2000年
  - 王年一：《对上海“一月革命”的几点看法》，《党史通讯》1986 年第 2 期

## 外部链接

  - [董光中，未出笼的反动影片“一月风暴”，联谊报2006年7月15日](http://www.lybs.com.cn/gb/node2/node802/node327871/node327874/userobject15ai5324428.html)

  - [香皂纸也有“一月风暴”](http://www.cb-h.com/shshshow.asp?n_id=27988)

  -
  -
{{-}}

[Category:文革名词](../Category/文革名词.md "wikilink")
[Category:1967年中国](../Category/1967年中国.md "wikilink")
[Category:1967年上海](../Category/1967年上海.md "wikilink")

1.
2.
3.