[Leiothrix_lutea_calipyga_MHNT_227_Jiangsu_Chine.jpg](https://zh.wikipedia.org/wiki/File:Leiothrix_lutea_calipyga_MHNT_227_Jiangsu_Chine.jpg "fig:Leiothrix_lutea_calipyga_MHNT_227_Jiangsu_Chine.jpg")

**红嘴相思鸟**（[学名](../Page/学名.md "wikilink")：**）又稱相思鳥、紅嘴玉、五彩相思鳥、紅嘴鳥等，[画眉科](../Page/画眉科.md "wikilink")[相思鸟属两种鸟之一](../Page/相思鸟属.md "wikilink")，是海拔迁徙的[候鸟](../Page/鸟类迁徙.md "wikilink")。

## 特征

红嘴相思鸟的平均体重约为21.4克\[1\]，体长约15厘米；背羽暗绿色，头部略带黄色，胸部黄色，翼羽色彩鲜艳，尾部具有小叉；两性羽色近似。

## 分布

栖息地包括亚热带或热带的（低地）湿润疏灌丛、亚热带或热带的湿润低地林、亚热带或热带的高海拔疏灌丛、种植园、温带疏灌丛、温带森林和亚热带或热带的湿润山地林。分布于[美国](../Page/美国.md "wikilink")（引进种）、[日本](../Page/日本.md "wikilink")（引进种）、[香港](../Page/香港.md "wikilink")、[留尼汪](../Page/留尼汪.md "wikilink")（引进种）、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[缅甸](../Page/缅甸.md "wikilink")、[不丹](../Page/不丹.md "wikilink")、[中国大陆](../Page/中国大陆.md "wikilink")（西自西藏昌都地区，东到东南沿海各地，北自甘肃、陕西等省的南部，南至云南、广西、广东各省）、[意大利](../Page/意大利.md "wikilink")（引进种）、[印度](../Page/印度.md "wikilink")、[越南](../Page/越南.md "wikilink")、[法国](../Page/法国.md "wikilink")（引进种）和[尼泊尔](../Page/尼泊尔.md "wikilink")。\[2\]

## 参考文献

## 外部链接

  - [Pc-Zoo上的红嘴相思鸟照片](https://web.archive.org/web/20100417005321/http://www.pc-zoo.com/birds/pekin_robin.htm)

[Category:相思鸟属](../Category/相思鸟属.md "wikilink")
[Category:寵物鳥](../Category/寵物鳥.md "wikilink")
[Category:中國鳥類](../Category/中國鳥類.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:尼泊尔動物](../Category/尼泊尔動物.md "wikilink")
[Category:云南鸟类](../Category/云南鸟类.md "wikilink")
[Category:印度鸟类](../Category/印度鸟类.md "wikilink")

1.

2.