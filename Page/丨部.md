**丨部**，是為漢字索引中的[部首之一](../Page/部首.md "wikilink")，[康熙字典](../Page/康熙字典.md "wikilink")214個部首中的第二個（一劃的則為第二個）。就[繁體和](../Page/繁體.md "wikilink")[簡體中文中](../Page/簡體.md "wikilink")，丨部歸於一劃部首。丨部只以中間為部字。且無其他部首可用者將部首歸為丨部。

## 部首單字解釋

上下貫通。見[說文](../Page/說文.md "wikilink")。

  - 補充：今書寫用漢文不多見，一般字典難檢字索引亦未收錄。

## 字形

[File:丨-bigseal.svg|大篆](File:丨-bigseal.svg%7C大篆)
[File:丨-seal.svg|小篆](File:丨-seal.svg%7C小篆)

## 各國讀音

  - 中國：丨部（拼音： 注音：ㄍㄨㄣˇ ㄅㄨˋ）
  - 朝鲜：뚫을곤부 (ttureul gon bu、贯通 丨部)
  - 日本：ぼう（bo u）、たてぼう（ta te bo u）
  - 越南：bộ cổn（部丨）
  - 歐美：Radical Line

## 部首字读音

  - 中古漢語
      - [廣韻](../Page/廣韻.md "wikilink") -
        古本[切](../Page/反切.md "wikilink")、混韻
      - [詩韻](../Page/詩韻.md "wikilink") -
        阮韻、[上声](../Page/上声.md "wikilink")
      - [三十六字母](../Page/三十六字母.md "wikilink") - 見母
  - [現代漢語](../Page/中文.md "wikilink")
      - [普通話](../Page/普通話.md "wikilink") -
        <small>[汉语拼音](../Page/汉语拼音.md "wikilink")</small>：
        <small>[注音](../Page/注音字母.md "wikilink")</small>：
        <small>[國際式](../Page/國際式.md "wikilink")</small>：kun<sup>3</sup>
      - [廣東話](../Page/廣東話.md "wikilink") -
        <small>[粵拼](../Page/香港語言學學會粵語拼音方案.md "wikilink")</small>
        gwan2 滾
  - [朝鮮語](../Page/朝鮮語.md "wikilink") - <small>音</small>：(gon)
    <small>訓</small>：(ttureul、貫通)
  - [日語](../Page/日語.md "wikilink") -
    <small>[音](../Page/音讀.md "wikilink")</small>：コン（kon）

## 部首字元及變形

在[Unicode和文字區的相關部首字元只有一個](../Page/Unicode.md "wikilink")，就是[康熙部首](../Page/康熙部首.md "wikilink")「<span style="font-size:x-large;">⼁</span>」（KANGXI
RADICAL LINE
\[U+2F01\]）、文字區「<span style="font-size:x-large;">丨</span>」（CJK
UNIFIED IDEOGRAPH-4E28）。

## 字例

| 除部首外之筆劃 | 字例 -{ |
| ------- | ----- |
| 0       | 丨     |
| 1       | 丩     |
| 2       | 个丫㐃㐄丬 |
| 3       | 中丮丰丯  |
| 4       | 丱     |
| 6       | 𠁨串    |
| 7       | 鼡丳𠁪   |
| 9       | 丵     |
| 11      | 𠁵     |
| 14      | 𠁸     |
| 17      | 𠁹     |
| 18      | 𠁺     |
| 21      | 𠁻 }-  |

## 注釋

[Category:部首](../Category/部首.md "wikilink")