首場[F1](../Page/F1.md "wikilink")**中國大獎賽**於2004年9月26日舉行。此赛事已经连续举办了15届。并在于2019年举办的F1大奖赛中的第1000场。\[1\]

F1中國站使用[上海國際賽車場作為比賽賽道](../Page/上海國際賽車場.md "wikilink")，該賽道共耗資2.4億[美元興建](../Page/美元.md "wikilink")，是当时所有F1賽道中成本最高的。为了举办该赛事，上海市政府每年要向[国际汽联支付](../Page/国际汽联.md "wikilink")3000万美元，而相对的，[巴林赛道只需](../Page/巴林赛道.md "wikilink")1800万美元，马来西亚[雪邦赛道在](../Page/雪邦赛道.md "wikilink")1500万美元，日本[铃鹿赛道](../Page/铃鹿赛道.md "wikilink")950万美元，而欧美地区都约为1000万美元。\[2\]位於[上海的中國站特色為整條賽道是](../Page/上海.md "wikilink")「上」字形。

目前在中国境内还未有第二条专用赛道能够替代位于嘉定的上海国际赛车场承办F1中国大奖赛赛事。

## 冠軍

### 多次奪冠車手

<table>
<thead>
<tr class="header">
<th><p>次數</p></th>
<th><p>車手</p></th>
<th><p>年度</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>6</p></td>
<td><p><strong><a href="../Page/路易斯·漢米爾頓.md" title="wikilink">路易斯·漢米爾頓</a></strong></p></td>
<td><p><a href="../Page/2008年中國大獎賽.md" title="wikilink">2008</a>、<a href="../Page/2011年中國大獎賽.md" title="wikilink">2011</a>、<a href="../Page/2014年中國大獎賽.md" title="wikilink">2014</a>、<a href="../Page/2015年中國大獎賽.md" title="wikilink">2015</a>、<a href="../Page/2017年中國大獎賽.md" title="wikilink">2017、</a>2019</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/費爾南多·阿隆索.md" title="wikilink">費爾南多·阿隆索</a></p></td>
<td><p><a href="../Page/2005年中國大獎賽.md" title="wikilink">2005</a>、<a href="../Page/2013年中國大獎賽.md" title="wikilink">2013</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尼可·羅斯堡.md" title="wikilink">尼可·羅斯堡</a></p></td>
<td><p><a href="../Page/2012年中國大獎賽.md" title="wikilink">2012</a>、<a href="../Page/2016年中國大獎賽.md" title="wikilink">2016</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 多次奪冠車隊

<table>
<thead>
<tr class="header">
<th><p>次數</p></th>
<th><p>車隊</p></th>
<th><p>年度</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>6</p></td>
<td><p><strong><a href="../Page/梅賽德斯車隊.md" title="wikilink">梅賽德斯</a></strong></p></td>
<td><p><a href="../Page/2012年中國大獎賽.md" title="wikilink">2012</a>、<a href="../Page/2014年中國大獎賽.md" title="wikilink">2014</a>、<a href="../Page/2015年中國大獎賽.md" title="wikilink">2015</a>、<a href="../Page/2016年中國大獎賽.md" title="wikilink">2016</a>、<a href="../Page/2017年中國大獎賽.md" title="wikilink">2017、2019</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><strong><a href="../Page/法拉利車隊.md" title="wikilink">法拉利</a></strong></p></td>
<td><p><a href="../Page/2004年中國大獎賽.md" title="wikilink">2004</a>、<a href="../Page/2006年中國大獎賽.md" title="wikilink">2006</a>、<a href="../Page/2007年中國大獎賽.md" title="wikilink">2007</a>、<a href="../Page/2013年中國大獎賽.md" title="wikilink">2013</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><strong><a href="../Page/麥拉倫車隊.md" title="wikilink">麥拉倫</a></strong></p></td>
<td><p><a href="../Page/2008年中國大獎賽.md" title="wikilink">2008</a>、<a href="../Page/2010年中國大獎賽.md" title="wikilink">2010</a>、<a href="../Page/2011年中國大獎賽.md" title="wikilink">2011</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><strong><a href="../Page/紅牛車隊.md" title="wikilink">紅牛</a></strong></p></td>
<td><p><a href="../Page/2009年中國大獎賽.md" title="wikilink">2009</a>、<a href="../Page/2018年中國大獎賽.md" title="wikilink">2018</a></p></td>
</tr>
</tbody>
</table>

### 歷屆冠軍

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>车手</p></th>
<th><p>车队</p></th>
<th><p>赛道</p></th>
<th><p>报告</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/2019年中國大獎賽.md" title="wikilink">2019年</a></p></td>
<td><p><a href="../Page/路易斯·咸美頓.md" title="wikilink">路易斯·漢米爾頓</a></p></td>
<td><p><a href="../Page/梅赛德斯车队.md" title="wikilink">梅賽德斯</a></p></td>
<td><p><a href="../Page/上海国际赛车场.md" title="wikilink">上海國際赛道</a></p></td>
<td><p><a href="../Page/2019年中國大獎賽.md" title="wikilink">报告</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年世界一級方程式錦標賽.md" title="wikilink">2018年</a></p></td>
<td><p><a href="../Page/丹尼爾·里卡多.md" title="wikilink">丹尼爾·里卡多</a></p></td>
<td><p><a href="../Page/紅牛車隊.md" title="wikilink">紅牛</a>-<a href="../Page/豪雅錶.md" title="wikilink">豪雅</a></p></td>
<td><p><a href="../Page/2018年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2017年世界一級方程式錦標賽.md" title="wikilink">2017年</a></p></td>
<td><p><a href="../Page/路易斯·汉密尔顿.md" title="wikilink">路易斯·汉密尔顿</a></p></td>
<td><p><a href="../Page/梅赛德斯车队.md" title="wikilink">梅賽德斯</a></p></td>
<td><p><a href="../Page/2017年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年世界一級方程式錦標賽.md" title="wikilink">2016年</a></p></td>
<td><p><a href="../Page/尼可·羅斯堡.md" title="wikilink">尼可·羅斯堡</a></p></td>
<td><p><a href="../Page/梅赛德斯车队.md" title="wikilink">梅賽德斯</a></p></td>
<td><p><a href="../Page/2016年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015年世界一级方程式锦标赛.md" title="wikilink">2015年</a></p></td>
<td><p><a href="../Page/路易斯·漢米爾頓.md" title="wikilink">路易斯·漢米爾頓</a></p></td>
<td><p><a href="../Page/梅赛德斯车队.md" title="wikilink">梅賽德斯</a></p></td>
<td><p><a href="../Page/2015年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年世界一级方程式锦标赛.md" title="wikilink">2014年</a></p></td>
<td><p><a href="../Page/路易斯·漢米爾頓.md" title="wikilink">路易斯·漢米爾頓</a></p></td>
<td><p><a href="../Page/梅赛德斯车队.md" title="wikilink">梅賽德斯</a></p></td>
<td><p><a href="../Page/2014年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年世界一级方程式锦标赛.md" title="wikilink">2013年</a></p></td>
<td><p><a href="../Page/费尔南多·阿隆索.md" title="wikilink">费尔南多·阿隆索</a></p></td>
<td><p><a href="../Page/法拉利车队.md" title="wikilink">法拉利</a></p></td>
<td><p><a href="../Page/2013年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012年世界一级方程式锦标赛.md" title="wikilink">2012年</a></p></td>
<td><p><a href="../Page/尼可·羅斯堡.md" title="wikilink">尼可·羅斯堡</a></p></td>
<td><p><a href="../Page/梅赛德斯車隊.md" title="wikilink">梅賽德斯</a></p></td>
<td><p><a href="../Page/2012年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011年世界一级方程式锦标赛.md" title="wikilink">2011年</a></p></td>
<td><p><a href="../Page/路易斯·漢米爾頓.md" title="wikilink">路易斯·漢米爾頓</a></p></td>
<td><p><a href="../Page/邁凱倫車隊.md" title="wikilink">麥拉倫</a>-<a href="../Page/梅赛德斯.md" title="wikilink">梅赛德斯</a></p></td>
<td><p><a href="../Page/2011年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年世界一级方程式锦标赛.md" title="wikilink">2010年</a></p></td>
<td><p><a href="../Page/简森·巴顿.md" title="wikilink">简森·巴顿</a></p></td>
<td><p><a href="../Page/邁凱倫車隊.md" title="wikilink">麥拉倫</a>-<a href="../Page/梅赛德斯.md" title="wikilink">梅赛德斯</a></p></td>
<td><p><a href="../Page/2010年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2009年世界一级方程式锦标赛.md" title="wikilink">2009年</a></p></td>
<td><p><a href="../Page/賽巴斯蒂安·維特爾.md" title="wikilink">賽巴斯蒂安·維特爾</a></p></td>
<td><p><a href="../Page/红牛车队.md" title="wikilink">红牛</a>-<a href="../Page/雷諾車隊.md" title="wikilink">雷諾</a></p></td>
<td><p><a href="../Page/2009年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年世界一级方程式锦标赛.md" title="wikilink">2008年</a></p></td>
<td><p><a href="../Page/路易斯·漢米爾頓.md" title="wikilink">路易斯·漢米爾頓</a></p></td>
<td><p><a href="../Page/邁凱倫車隊.md" title="wikilink">麥拉倫</a>-<a href="../Page/梅赛德斯.md" title="wikilink">梅赛德斯</a></p></td>
<td><p><a href="../Page/2008年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年世界一级方程式锦标赛.md" title="wikilink">2007年</a></p></td>
<td><p><a href="../Page/奇米·雷克南.md" title="wikilink">奇米·雷克南</a></p></td>
<td><p><a href="../Page/法拉利车队.md" title="wikilink">法拉利</a></p></td>
<td><p><a href="../Page/2007年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006年世界一级方程式锦标赛.md" title="wikilink">2006年</a></p></td>
<td><p><a href="../Page/麥可·舒馬克.md" title="wikilink">麥可·舒馬克</a></p></td>
<td><p><a href="../Page/法拉利车队.md" title="wikilink">法拉利</a></p></td>
<td><p><a href="../Page/2006年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2005年世界一级方程式锦标赛.md" title="wikilink">2005年</a></p></td>
<td><p><a href="../Page/费尔南多·阿隆索.md" title="wikilink">费尔南多·阿隆索</a></p></td>
<td><p>|<a href="../Page/雷诺车队.md" title="wikilink">雷诺</a></p></td>
<td><p><a href="../Page/2005年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2004年世界一级方程式锦标赛.md" title="wikilink">2004年</a></p></td>
<td><p><a href="../Page/鲁宾斯·巴里切罗.md" title="wikilink">鲁宾斯·巴里切罗</a></p></td>
<td><p><a href="../Page/法拉利车队.md" title="wikilink">法拉利</a></p></td>
<td><p><a href="../Page/2004年中国大奖赛.md" title="wikilink">报告</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部链接

  - [F1中国大奖赛官方网站](http://www.racing-china.com) 锐速中国赛车网——F1中国大奖赛官方网站
  - [F1中国大奖赛官方论坛](http://www.f1-zone.net) 锐速车迷会——F1地带
  - [一级方程式赛车网](https://web.archive.org/web/20111210132724/http://f1home.cn/)
    F1赛车网，车迷家园

[Category:一級方程式大獎賽](../Category/一級方程式大獎賽.md "wikilink")
[Category:中國大獎賽](../Category/中國大獎賽.md "wikilink")

1.
2.  [F1上海站连年巨额亏损
    骑虎难下仍将续约七年](http://sports.cqnews.net/zhty/zhgj/201004/t20100419_4274901.htm)