神奈川条約|kana=にちべいわしんじょうやく{{·}}かながわじょうやく|romaji=Nichibei Washin
Jōyaku{{·}}Kanagawa Jōyaku}}
《**神奈川條約**》（，）為1854年3月31日（[嘉永](../Page/嘉永.md "wikilink")7年舊曆[三月初三](../Page/三月初三.md "wikilink")）[江戶幕府與](../Page/江戶幕府.md "wikilink")[美國所締結的](../Page/美利堅合眾國.md "wikilink")[條約](../Page/條約.md "wikilink")，[日本通稱為](../Page/日本.md "wikilink")《日美和親條約》（）。簽約代表分別為日本方面全權代表[林復齋](../Page/林復齋.md "wikilink")（[大学頭](../Page/大学頭.md "wikilink")）和美國方面全權代表[東印度艦隊司令](../Page/東印度艦隊.md "wikilink")[馬休·佩里](../Page/馬休·佩里.md "wikilink")。條約中主要規定日本必須開放[下田與](../Page/下田市.md "wikilink")[箱館](../Page/函館市.md "wikilink")（今函館市）兩港口與美國通商，并向遇难船只的美国船员提供援助。\[1\]\[2\]\[3\]

## 概要

從[幕末的混亂期到](../Page/幕末.md "wikilink")[明治開頭時期](../Page/明治.md "wikilink")，《神奈川條約》是日本不可避免與列強所定立[不平等條約中的一部](../Page/不平等條約.md "wikilink")。依此締結的條約，日本開啟了下田及箱館兩港口，[日本鎖國體制就此崩解](../Page/日本鎖國.md "wikilink")。

當時日本幕府只曾與[南蠻貿易](../Page/南蠻貿易.md "wikilink")、從未跟英美正式交涉過，因此艦隊航途中從[澳門找來](../Page/澳門.md "wikilink")[衛三畏負責](../Page/衛三畏.md "wikilink")[漢文書寫溝通](../Page/漢文.md "wikilink")，至[上海則用](../Page/上海.md "wikilink")[荷蘭出身的波特曼](../Page/荷蘭.md "wikilink")（Anton
L. C.
Portman）當艦隊書記參謀口頭談判。於是美日交涉時採[荷蘭語與](../Page/荷蘭語.md "wikilink")[日語相互轉譯](../Page/日語.md "wikilink")，並以漢文、[荷蘭文譯為](../Page/荷蘭文.md "wikilink")[日文確認](../Page/日文.md "wikilink")，而形成荷蘭文、英文、漢文、日文條約書。\[4\]\[5\]\[6\]<ref>

  - [](http://jp.jnocnews.jp/news/show.aspx?id=53921)
  - [](http://jp.jnocnews.jp/news/show.aspx?id=53973)
  - [](http://jp.jnocnews.jp/news/show.aspx?id=54061)
  - [](http://jp.jnocnews.jp/news/show.aspx?id=54136)
  - [](http://jp.jnocnews.jp/news/show.aspx?id=54206)</ref>

日本方面條約批准書原本在幕末[江戶城火災中被燒毀](../Page/江戶城.md "wikilink")；不過美國方面批准書原本仍在，保管於[美国国家档案管理局](../Page/国家档案和记录管理局.md "wikilink")。2004年（[平成](../Page/平成.md "wikilink")16年）日美交流150周年記念之際，美國將條約批准書精密複製品致贈日本。\[7\]\[8\]

條約日文原名所謂「日米」，「日」指日本、「米」指美國（美利堅合眾國）。條約締結時僅名「」，稱美國為「」\[9\]；後正式全名《****》。

## 事件經過

1853年（嘉永6年），在[馬休·佩里之前一年](../Page/馬休·佩里.md "wikilink")，美國第十三任總統[菲爾莫爾就親筆寫信給幕府要求開國並通商](../Page/米勒德·菲爾莫爾.md "wikilink")。幕府方面在要求後猶豫了一年，美方面也因此一度打消主意。不過，翌年2月13日（嘉永7年[正月十六](../Page/正月十六.md "wikilink")），美國船隻由[江戶灣](../Page/江戶灣.md "wikilink")（[東京灣](../Page/東京灣.md "wikilink")）入港，再訪日本。幕府在[武藏國](../Page/武藏國.md "wikilink")[久良岐郡横濱村字駒形](../Page/久良岐郡.md "wikilink")（[神奈川縣](../Page/神奈川縣.md "wikilink")[横濱市](../Page/横濱市.md "wikilink")[中區](../Page/中區_\(橫濱市\).md "wikilink")[神奈川縣廳附近](../Page/神奈川縣.md "wikilink")、現在位於[横濱開港資料館所在地](../Page/横濱開港資料館.md "wikilink")）設置招待所，協議從開始到終了大約有1個月之久，締結了神奈川條約，內文全部有12條。

之後交渉場所移到[伊豆國下田](../Page/伊豆國.md "wikilink")（現[静岡縣](../Page/静岡縣.md "wikilink")[下田市](../Page/下田市.md "wikilink")）[了仙寺](../Page/了仙寺.md "wikilink")，同年5月25日釐定了神奈川條約細則，據此締結下田條約（內文全部有13條）。佩里艦隊在同年6月1日從下田歸國。進一步，歸國途中停留在[琉球並與](../Page/琉球.md "wikilink")[琉球國締結通商條約](../Page/琉球國.md "wikilink")。

## 美國方面之目的

据说当时美国在[太平洋上进行捕鲸活动以取得价格昂贵的鲸油作为灯油和其他用途](../Page/太平洋.md "wikilink")，为此需要在日本进行燃料补给，故为了控制燃料价格稳定而签订该条约。

另一个目的是为了保障食物和淡水的补给。在那个没有冰箱和其他食物保鲜手段的时代，为了避免[腳氣病及](../Page/腳氣病.md "wikilink")[壞血病病发以及丰富船员就餐口味](../Page/壞血病.md "wikilink")，新鲜蔬菜和肉类的补充就显得尤其重要。

除此之外，建立美国与大清国之间贸易航线的补给港，也是一个重要的原因。

## 條約部份内容

《神奈川條約》之内容如下所訂定：

  - 美國船隻需要的物資補給（付款購買）是在開港後的下田、函館二地（[通商口岸的設定](../Page/商埠.md "wikilink")）。
  - 漂流民的救助、引渡
  - 美國人的居留地設定在下田
  - 片面的[最惠國待遇](../Page/最惠國待遇.md "wikilink")

除此之外，下田條約也定立了如下的細則條款：

  - 美國人活動的可能範圍是以下田為中心7[里內](../Page/市里.md "wikilink")、及與以[函館為中心](../Page/函館.md "wikilink")5里內之範圍為限，禁止進入武家、町家之範圍。
  - 對於美國人暫時的休息所設置在[了仙寺](../Page/了仙寺.md "wikilink")、[玉泉寺](../Page/玉泉寺_\(下田市\).md "wikilink")，美國人的墳墓設置在玉泉寺內。
  - 美國人禁止從事獵取鳥獸等狩獵活動。

## 相關圖片

<File:Yokohama> kaigan
church04s3200.jpg|日美神奈川條約定約地（[横濱市](../Page/横濱市.md "wikilink")[關内](../Page/關内.md "wikilink")）
<File:Ryosenji> temple shimoda 2007-02-24.jpg|締結下田條約時、暫時的美國人休息所設置在了仙寺
[File:PerryBustShimoda.jpg|位於日本下田市的](File:PerryBustShimoda.jpg%7C位於日本下田市的)[馬休·佩里銅像](../Page/馬休·佩里.md "wikilink")

## 相關條目

  - [日本史出来事列表](../Page/日本史出来事列表.md "wikilink")
  - [條約列表](../Page/條約列表.md "wikilink")
  - [不平等條約](../Page/不平等條約.md "wikilink")
  - [日美修交記念館](../Page/日美修交記念館.md "wikilink")
  - [安政條約](../Page/安政條約.md "wikilink")
  - [琉美修好條約](../Page/琉美修好條約.md "wikilink")
  - [黑船](../Page/黑船.md "wikilink")
  - [黑船來航](../Page/黑船來航.md "wikilink")
  - [幕末](../Page/幕末.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - The Convention of Kanagawa, 1854 (full text)
  - \[<http://crd.ndl.go.jp/reference/modules/d3ndlcrdentry/index.php?page=ref_view&id=1000066588>


{{-}}

[Category:日美關係](../Category/日美關係.md "wikilink")
[Category:不平等條約](../Category/不平等條約.md "wikilink")
[Category:江戶時代外交](../Category/江戶時代外交.md "wikilink")
[Category:幕末外交](../Category/幕末外交.md "wikilink")
[Category:日本條約](../Category/日本條約.md "wikilink")
[Category:美國條約](../Category/美國條約.md "wikilink")
[Category:1854年日本](../Category/1854年日本.md "wikilink")
[Category:1849至1865年的美国历史](../Category/1849至1865年的美国历史.md "wikilink")
[Category:1854年条约](../Category/1854年条约.md "wikilink")

1.  [慶應三年法令全書附錄第四](../Page/慶應.md "wikilink")，“日本國米利堅合衆國和親條約”，1854年3月31日（[嘉永七年](../Page/嘉永.md "wikilink")[3月3日
    (舊曆)](../Page/三月初三.md "wikilink")）
2.  *Imposed Treaties and International Law*，Stuart S. Malawer.
    \[1977\]. ISBN 0-930342-07-0
3.  Preston, Peter Wallace. \[1998\]，Blackwell Publishing，Pacific Asia
    in the Global System: An Introduction. ISBN 0-631-20238-2
4.  [明治维新的国度 -
    黑船来航事件(4)](http://v.book.ifeng.com/book/ts/62652/7307923.htm)
5.
6.  \]
7.  [](http://www.mofa.go.jp/mofaj/annai/honsho/shiryo/qa/bakumatsu_01.html)
8.   .
9.   .