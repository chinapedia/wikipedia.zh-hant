**沙敦府**（，）是泰国南部府份之一，邻近府份为[泰国的](../Page/泰国.md "wikilink")[董里府](../Page/董里府.md "wikilink")、[博他仑府](../Page/博他仑府.md "wikilink")、[宋卡府和](../Page/宋卡府.md "wikilink")[马来西亚的最小一州](../Page/马来西亚.md "wikilink")[玻璃市](../Page/玻璃市.md "wikilink")。

沙敦府名字由来于[马来语中](../Page/马来语.md "wikilink")“Setul”，是一种[树的名字](../Page/树.md "wikilink")，这种树曾在本府广为种植。

## 地理位置

[Satun_Ko_Yang.jpg](https://zh.wikipedia.org/wiki/File:Satun_Ko_Yang.jpg "fig:Satun_Ko_Yang.jpg")
本府位于[马来亚](../Page/马来亚.md "wikilink")[半岛上](../Page/半岛.md "wikilink")，是[泰国的最西南端](../Page/泰国.md "wikilink")，西临[安达曼海](../Page/安达曼海.md "wikilink")，东面离[宋卡府](../Page/宋卡府.md "wikilink")[合艾市最近处仅二十多](../Page/合艾.md "wikilink")[公里](../Page/公里.md "wikilink")。除了一些零星的[山以外其余是](../Page/山.md "wikilink")[平原](../Page/平原.md "wikilink")。在沙敦府的[公路边总是种着](../Page/公路.md "wikilink")[椰子树](../Page/椰子.md "wikilink")。

[塔如濤群島](../Page/塔如濤群島.md "wikilink")（Koh
Tarutao）[国家海洋公园由离岸的一群](../Page/海洋公园.md "wikilink")[小岛组成](../Page/岛.md "wikilink")，主岛离[马来西亚的](../Page/马来西亚.md "wikilink")[浮罗交怡](../Page/浮罗交怡.md "wikilink")（或譯[兰卡威岛](../Page/浮罗交怡.md "wikilink")，Pulau
Langkawi）仅五公里，两岛每天有[渡船对开](../Page/渡轮.md "wikilink")。还有一个大岛贝德拉岛（Koh
Phetra）今天也是[泰国的](../Page/泰国.md "wikilink")[国家海洋公园](../Page/海洋公园.md "wikilink")。

## 历史渊源

直到1813年以前沙敦府隶属[马来西亚的](../Page/马来西亚.md "wikilink")[吉打州](../Page/吉打州.md "wikilink")，
当时[马来文名字是Mukim](../Page/马来文.md "wikilink") Setul。 1813年
[暹罗](../Page/暹罗.md "wikilink")[那空是贪玛叻府特派官员将她接管](../Page/那空是贪玛叻府.md "wikilink")，1897年沙敦府转属暹罗载武里道（Monthon
Triburi），该道辖区包括今天马来西亚的整个吉打州。

1909年英国同暹罗签订条约，根据条约暹罗几乎将整个吉打送归[英属马来亚管辖](../Page/马来亚.md "wikilink")。沙敦府则转归泰国普吉道管辖，1933年泰国化道为府，沙敦府始成为独立的行政机构至今。

## 人口组成

沙敦府人口虽然仅百分之9.9承认自己为[马来民族](../Page/马来族.md "wikilink")，但至少百分之九十人家都可寻出与[马来族的亲缘关系和会说](../Page/马来族.md "wikilink")[马来语](../Page/马来语.md "wikilink")。百分之67.8人民信奉[回教](../Page/回教.md "wikilink")，百分之31.9人民信奉[佛教](../Page/佛教.md "wikilink")。

在吉打苏丹统治年代，沙吞府与[大城王国及后来延续至今的](../Page/大城王国.md "wikilink")[却克里王朝都有极密切联系](../Page/却克里王朝.md "wikilink")。这里占人口多数的[回教民与相对少数的](../Page/回民.md "wikilink")[佛教民自由通婚](../Page/佛教.md "wikilink")，一家人妈妈信[佛](../Page/佛教.md "wikilink")，爸爸信[真主的](../Page/真主.md "wikilink")，或者兄弟姐妹信不同教的比比皆是。长时间以来思想互相影响，各取所长，相安无事。虽然[地理位置相近](../Page/地理位置.md "wikilink")，但是沙吞府向来一派[和平气氛](../Page/和平.md "wikilink")，社会安定团结气氛远超过邻近的[泰南三府](../Page/泰国南部三府.md "wikilink")。

## 府徽与府花

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p>府徽画着旭日东升的海边的一尊<a href="../Page/佛像.md" title="wikilink">佛像</a>，该佛名为Samut Thewaa是泰国佛经中大海的守护神，他每日坐在圣石上四处飘游。那海即是本府西临的<a href="../Page/安达曼海.md" title="wikilink">安达曼海</a>。</p>
<p>府树是：泰国红木 <a href="../Page/Thai_Rosewood.md" title="wikilink">Thai Rosewood</a>，泰文名：<em>Pha-yungklaep</em> ，学名：<em><a href="../Page/Dalbergia_bariensis.md" title="wikilink">Dalbergia bariensis</a></em>。<br />
府花是： <a href="../Page/Snowy_Orchid_Tree.md" title="wikilink">Snowy Orchid Tree</a>，学名：<em><a href="../Page/Bauhinia_acuminata.md" title="wikilink">Bauhinia acuminata</a></em>。<br />
本府标语：和平与纯洁无瑕 （泰语：สตูล สงบ สะอาด ธรรมชาติบริสุทธิ์ ）。</p></td>
</tr>
</tbody>
</table>

## 行政与管理

本府行政上被划分为6个县（ampoe）1个次级县（king
ampoe），又进一步被划分为36个区（tambon）及277个村（muban）。府内共有五座小城镇。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>中文名</p></th>
<th><p>泰文名</p></th>
<th><p>英文名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>เมืองสตูล</p></td>
<td><p>Mueang Satun</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>ควนโดน</p></td>
<td><p>Khuan Don</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>ควนกาหลง</p></td>
<td><p>Khuan Kalong</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>ท่าแพ</p></td>
<td><p>Tha Phae</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>ละงู</p></td>
<td><p>La-ngu</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>ทุ่งหว้า</p></td>
<td><p>Thung Wa</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>มะนัง</p></td>
<td><p>Manang</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Amphoe_Satun.svg" title="fig:Amphoe_Satun.svg">Amphoe_Satun.svg</a></p></td>
</tr>
</tbody>
</table>

沙敦府原是个人口很稀少的小府份，1933年[泰国](../Page/泰国.md "wikilink")「化道为府」时仅分为玛南、童哇两个县，后来人口渐渐增加，所以不断分化，成为今天6县1次级县。

## 外部链接

  - [泰国皇家旅游局沙敦府专页](https://web.archive.org/web/20051210170610/http://www.tourismthailand.org/destinationguide/list.aspx?provinceid=71)

<!-- end list -->

  - [泰国沙敦府旅游专页](http://www.satunprovince.net/eng_satun_travel_place.html)
  - [沙敦府地图](http://www.thailex.info/THAILEX/THAILEXENG/LEXICON/Copy%20of%20Satun.htm)

[Category:泰國府份](../Category/泰國府份.md "wikilink")
[Category:沙敦府](../Category/沙敦府.md "wikilink")