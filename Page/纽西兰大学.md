**纽西兰大学**作为[纽西兰历史上最大的](../Page/纽西兰.md "wikilink")[大学联盟成立于](../Page/大学联盟.md "wikilink")1870年，并于1961年完成了其历史使命而退出了[纽西兰的历史舞台](../Page/纽西兰.md "wikilink")。**纽西兰大学**作为一所联盟性质的[大学](../Page/大学.md "wikilink")，在纽西兰各地都有着他的分校。

## 历史

纽西兰大学成立于1870年\[1\]。

纽西兰大学解散于1961年，并将其学位授予权授予其的各加盟大学。

[奥塔哥大学在](../Page/奥塔哥大学.md "wikilink")1874年加盟纽西兰大学的时候成功的保留住了其“大学”的称谓。但是[奥塔哥大学的学位授予权却是属于纽西兰大学](../Page/奥塔哥大学.md "wikilink")'''的。

1961年纽西兰大学解散之前共有6所大学和学院从属于**纽西兰大学**：[奥塔哥大学](../Page/奥塔哥大学.md "wikilink")，[坎特伯雷大学](../Page/坎特伯雷大学.md "wikilink")，[奥克兰大学和](../Page/奥克兰大学.md "wikilink")[惠灵顿维多利亚大学以及](../Page/惠灵顿维多利亚大学.md "wikilink")[林肯大学的前身坎特伯雷农学院和](../Page/林肯大学.md "wikilink")[梅西大学的前身梅西农学院](../Page/梅西大学.md "wikilink")。

## 录取条件

纽西兰大学有自己的录取考试系统，同时通过这个考试给学生在学习中发放奖学金。

## 參考

<references/>

## 連結

  - [New Zealand Vice-Chancellors' Committee](http://www.nzvcc.ac.nz/)

[category:紐西蘭大學](../Page/category:紐西蘭大學.md "wikilink")

[Category:1870年創建的教育機構](../Category/1870年創建的教育機構.md "wikilink")

1.