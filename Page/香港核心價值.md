**香港核心價值**一詞並無嚴格和明確的定義，通常用來泛指[香港社會所貫徹並賴以成功的基本](../Page/香港社會.md "wikilink")[價值觀](../Page/價值觀.md "wikilink")。

2004年6月，近300位來自不同[香港](../Page/香港.md "wikilink")[專業人士在報章聯署](../Page/專業人士.md "wikilink")《香港核心價值宣言》，列舉香港的核心價值觀，例如[自由](../Page/自由.md "wikilink")、[民主](../Page/民主.md "wikilink")、[人權及](../Page/人權.md "wikilink")[法治等](../Page/法治.md "wikilink")，引起社會討論何謂「香港核心價值」。

## 背景

部分人認為由[香港回归起](../Page/香港回归.md "wikilink")，[香港政府連串的施政措施都與香港核心價值背道而馳](../Page/香港政府.md "wikilink")，使到[香港市民害怕](../Page/香港市民.md "wikilink")[香港社會會以更快速度](../Page/香港社會.md "wikilink")「[中國大陸化](../Page/中國大陸.md "wikilink")」。為了維護香港與[中國大陸之間的特別性和優勢](../Page/中國大陸.md "wikilink")，不少市民發起了捍衛香港核心價值的運動，藉以保存香港本地的獨有[文化及](../Page/文化.md "wikilink")[競爭上的優勢](../Page/競爭.md "wikilink")。

另一方面也有人認為，港人對「香港核心價值」的看法由於回歸前港人大多對政治關注度較低和參與較少，實際有回歸前後之異。如以香港核心價值作為香港在回歸前的成功之因素來說，努力、勤奮和自強不息等「獅子山下精神」才是港人祖父輩們不可或缺的要素，而非回歸後[泛民和各界人士宣揚的](../Page/泛民.md "wikilink")[自由](../Page/自由.md "wikilink")，[民主和](../Page/民主.md "wikilink")[人權](../Page/人權.md "wikilink")。故此如果硬要把想追求的理想，和從一個小漁港發展成國際大都市的民眾生活及精神上所依仗的要素劃上等號，實在過於牽強。但無論如何，多數市民贊同這宣言，並支持保護這些核心價值。

## 核心價值元素

而香港[主流傳媒認為香港核心價值包括以下元素](../Page/香港傳媒.md "wikilink")：

  - [法治](../Page/法治.md "wikilink")
  - [人權](../Page/人權.md "wikilink")
  - [誠信](../Page/誠信.md "wikilink")
  - [廉潔](../Page/廉潔.md "wikilink")
  - [新聞自由](../Page/新聞自由.md "wikilink")
  - [言論自由](../Page/言論自由.md "wikilink")
  - [集會自由](../Page/集會自由.md "wikilink")

前任[香港行政長官](../Page/香港行政長官.md "wikilink")[梁振英於](../Page/梁振英.md "wikilink")[2012年香港行政長官選舉的勝利宣言説出以上核心價值](../Page/2012年香港行政長官選舉.md "wikilink")\[1\]\[2\]。

前[政務司司長](../Page/政務司司長.md "wikilink")[唐英年在](../Page/唐英年.md "wikilink")2012年初曾表示：「『捍衛核心價值』是最核心的核心價值。」\[3\]

[香港中文大學](../Page/香港中文大學.md "wikilink")[香港亞太研究所在](../Page/香港亞太研究所.md "wikilink")2014年10月30日（「[雨傘运動](../Page/雨傘运動.md "wikilink")」開始後第33日）發表一份民調報告，以電話調查訪問804位成年市民，指出他們對11項不同核心價值的同意程度：\[4\]

| 核心價值                                                         | 同意程度  | 最重要   |
| ------------------------------------------------------------ | ----- | ----- |
| [法治](../Page/法治.md "wikilink")                               | 92.7% | 22.9% |
| [公正](../Page/公正.md "wikilink")[廉潔](../Page/廉潔.md "wikilink") | 92.3% | 15.3% |
| 追求社會穩定                                                       | 88.2% | 8.3%  |
| [自由](../Page/自由.md "wikilink")                               | 88.1% | 20.8% |
| [和平仁愛](../Page/和平.md "wikilink")                             | 87.4% | 5.5%  |
| 保障私人財產                                                       | 86.5% | 1.9%  |
| 重視家庭責任                                                       | 84.5% | 0.6%  |
| [民主](../Page/民主.md "wikilink")                               | 83.2% | 11.1% |
| 多元包容                                                         | 79.8% | 2.5%  |
| 市場經濟                                                         | 76.5% | 3.5%  |

上述數據差距不太大，沒有一項最重要核心價值的比例超過半數，反映在受訪者心目中，香港的核心價值其實多元多樣，而非只有單一的壓倒性核心價值。這情況與香港融匯華洋中西、強調[多元包容的歷史和文化十分一致](../Page/多元.md "wikilink")\[5\]。

## 香港核心價值宣言

2004年6月7日，近300位來自[香港](../Page/香港.md "wikilink")42個不同專業、學術界人士在報章聯署《香港核心價值宣言》，列舉香港的核心價值是**「[自由](../Page/自由.md "wikilink")[民主](../Page/民主.md "wikilink")、[人權](../Page/人權.md "wikilink")[法治](../Page/法治.md "wikilink")、[公平](../Page/公平.md "wikilink")[公義](../Page/公義.md "wikilink")、[和平仁愛](../Page/和平.md "wikilink")、誠信[透明](../Page/透明.md "wikilink")、[多元](../Page/多元.md "wikilink")[包容](../Page/包容.md "wikilink")、尊重個人和恪守專業。」**宣言直指近來香港核心價值備受衝擊，與港人所追求的目標愈來愈遠，呼籲市民齊以言論和行動維護香港的核心價值，以免令香港變成「失去靈魂的軀殼」。然而，[親共媒體](../Page/親共媒體.md "wikilink")《[文匯報](../Page/文匯報.md "wikilink")》拒絕刊登宣言\[6\]\[7\]。

發起活動的[香港城市大學公共及社會行政系教授](../Page/香港城市大學.md "wikilink")、新力量網絡主席、曾任民主黨副主席的[張炳良指出](../Page/張炳良.md "wikilink")，近月的政治爭拗、[人大釋法否決](../Page/人大釋法.md "wikilink")[0708年雙普選](../Page/0708年雙普選.md "wikilink")、[維港巨星匯及](../Page/維港巨星匯.md "wikilink")[平機會解僱事件等](../Page/平機會.md "wikilink")，都衝擊香港核心價值，令市民產生無力及挫敗感，削弱了香港的管治質素及營商環境，亦破壞了社會的制度[理性與](../Page/理性.md "wikilink")[凝聚力](../Page/团结.md "wikilink")，呼籲不同階層與黨派的人士以言論及行動，共同維護這些核心價值。他強調這是針對整個香港社會，他認為每一個社會都有一套價值觀，這是社會團結的重要基礎。我們「不能失去香港」，這個香港是有括號的，不僅是地理上，還有文化面貌、生活方式，以及制度獨特層面的香港。[基本法](../Page/香港基本法.md "wikilink")[四十五條關注組成員](../Page/四十五條關注組.md "wikilink")[梁家傑表示](../Page/梁家傑.md "wikilink")，[中國政府近期以很高姿態](../Page/中华人民共和国政府.md "wikilink")、霸道地介入香港事務，令香港出現空前嚴重的分化，市民無論對中國政府及[香港政府都不信任](../Page/香港特別行政區政府.md "wikilink")\[8\]\[9\]\[10\]\[11\]。連署者包括導演[徐克等人](../Page/徐克.md "wikilink")。\[12\]

同年[牛棚書展亦以](../Page/牛棚書展.md "wikilink")《香港生活價值》為書展主題，反思香港之為香港的基本價值觀與特質。

## 其他观点

《[大公報](../Page/大公報.md "wikilink")》認為：[一國兩制是香港最重要](../Page/一國兩制.md "wikilink")、最切实、最可贵的核心价值，並認為「其他普遍的價值只有放到一定的歷史條件或特定的環境中去，才能真正顯示出其不可取代的價值」\[13\]\[14\]。

## 参考文献

## 外部链接

  - [香港核心價值](https://web.archive.org/web/20051107023419/http://www.hkcorevalues.net/b5_declar.htm)
  - [維護香港核心價值宣言](http://hkspeech.wordpress.com/2004/06/06/維護香港核心價值宣言/)
  - [Standing Firm on Hong Kong's Core
    Value](http://blog.yahoo.com/albertlai/articles/428663)

## 参见

  - [香港人](../Page/香港人.md "wikilink")、 [香港文化](../Page/香港文化.md "wikilink")
  - 《[狮子山下](../Page/狮子山下_\(歌曲\).md "wikilink")》、《[海闊天空](../Page/海闊天空_\(Beyond歌曲\).md "wikilink")》
  - [普世價值](../Page/普世價值.md "wikilink")、[價值觀外交](../Page/價值觀外交.md "wikilink")
  - [香港人權](../Page/香港人權.md "wikilink")、[香港言論自由](../Page/香港言論自由.md "wikilink")
  - [香港政治制度改革](../Page/香港政治制度改革.md "wikilink")
  - 城市精神：[上海城市精神](../Page/上海城市精神.md "wikilink")、[北京精神](../Page/北京精神.md "wikilink")

[Category:价值观](../Category/价值观.md "wikilink")
[Category:香港文化](../Category/香港文化.md "wikilink")
[Category:香港社會](../Category/香港社會.md "wikilink")
[Category:2004年香港](../Category/2004年香港.md "wikilink")

1.  [梁振英當選行政長官](http://archive.news.gov.hk/tc/categories/admin/html/2012/03/20120325_105540.shtml).
    2012年3月25日. 香港政府新聞處

2.  [梁振英 :
    珍惜香港核心價值](http://www.news.gov.hk/tc/categories/admin/html/2012/04/20120401_170241.shtml).
    2012年4月1日. 香港政府新聞處

3.

4.

5.
6.

7.

8.

9.

10.

11. [民主派團結度
    香港民主發展關鍵](http://www.libertytimes.com.tw/2004/new/jul/1/today-fo2.htm),
    [自由時報](../Page/自由時報.md "wikilink"), 2004年7月1日

12.
13.

14.