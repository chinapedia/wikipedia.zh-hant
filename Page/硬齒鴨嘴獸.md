**硬齒鴨嘴獸**（*Steropodon
galmani*）是一[種史前的](../Page/種.md "wikilink")[單孔目](../Page/單孔目.md "wikilink")，或是產卵的[哺乳動物](../Page/哺乳動物.md "wikilink")，生存於[下白堊紀](../Page/下白堊紀.md "wikilink")[阿爾布階](../Page/阿爾布階.md "wikilink")。牠是[鴨嘴獸的最早的親屬](../Page/鴨嘴獸.md "wikilink")。

硬齒鴨嘴獸的[化石只有一個](../Page/化石.md "wikilink")[蛋白石化的](../Page/蛋白石.md "wikilink")[顎骨及三顆](../Page/顎骨.md "wikilink")[臼齒](../Page/臼齒.md "wikilink")，於[澳洲](../Page/澳洲.md "wikilink")[新南威爾士](../Page/新南威爾士.md "wikilink")[閃電山脊發現](../Page/閃電山脊.md "wikilink")。牠是[中生代最大的哺乳動物](../Page/中生代.md "wikilink")，約有40-50厘米長。下臼齒長約5-7毫米，闊3-4毫米，而其他中世代的哺乳動物臼齒只有1-2毫米長。牠的臼齒像[獸亞綱的三尖齒](../Page/獸亞綱.md "wikilink")，但牠沒有下內錐，且上臼齒沒有原尖。

## 外部連結

  - [Australia's Lost
    Kingdoms](http://www.lostkingdoms.com/facts/factsheet8.htm)

## 參考

  -
[Category:硬齒鴨嘴獸屬](../Category/硬齒鴨嘴獸屬.md "wikilink")