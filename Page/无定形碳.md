[Amorphous_Carbon.png](https://zh.wikipedia.org/wiki/File:Amorphous_Carbon.png "fig:Amorphous_Carbon.png")
**无定形碳**（Amorphous
carbon）一種非[構造](../Page/晶體結構.md "wikilink")、富含反應性的[碳](../Page/碳.md "wikilink")。所谓无定形碳是指其内部结构而言。部分無定型炭的末端可能與[氫以](../Page/氫.md "wikilink")結合，形成**氫化無定形碳**。

## 現代科學

## 礦物學

在[礦物學上的定義](../Page/礦物學.md "wikilink")，无定形碳泛指除了[石墨與](../Page/石墨.md "wikilink")[鑽石之外的不純碳物質](../Page/鑽石.md "wikilink")\[1\]
，包括了：

  - [煤炭](../Page/煤炭.md "wikilink")

  - [煤煙](../Page/煤煙.md "wikilink")

  - [焦炭](../Page/焦炭.md "wikilink")

  - [木炭](../Page/木炭.md "wikilink")

  - [骨炭](../Page/骨炭.md "wikilink")

  -
## 参看

  - [碳](../Page/碳.md "wikilink")
  - [同素异形体](../Page/同素异形体.md "wikilink")

## 參考資料

<references/>

[Category:碳的同素异形体](../Category/碳的同素异形体.md "wikilink")

1.