**金棕榈奖**（）是[法國](../Page/法國.md "wikilink")[戛纳电影节正式競賽項目的獎項之一](../Page/戛纳电影节.md "wikilink")，頒發給坎城影展的最佳電影\[1\]，也是坎城影展的最高榮譽。金棕榈奖的獎杯是138克重純金棕櫚葉，共19片分葉，放在水晶基座底上，象徵坎城影展最佳影片獎項。

## 歷屆獲獎者

以下是所有獲得此獎的電影與導演：

<table>
<thead>
<tr class="header">
<th><p>年分</p></th>
<th><p>電影</p></th>
<th><p>電影原名</p></th>
<th><p>導演</p></th>
<th><p>製作國</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1955年</p></td>
<td><p>《<a href="../Page/君子好逑_(美國電影).md" title="wikilink">君子好逑</a>》</p></td>
<td><p><em>Marty</em></p></td>
<td><p><a href="../Page/德爾伯特·曼.md" title="wikilink">德爾伯特·曼</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1956年</p></td>
<td><p>《》</p></td>
<td><p><em>Le Monde du silence</em></p></td>
<td><p><a href="../Page/雅克-伊夫·庫斯托.md" title="wikilink">雅克-伊夫·庫斯托</a><br />
<a href="../Page/路易·馬盧.md" title="wikilink">路易·馬盧</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1957年</p></td>
<td><p>《》</p></td>
<td><p><em>The Friendly Persuasion</em></p></td>
<td><p><a href="../Page/威廉·惠勒.md" title="wikilink">威廉·惠勒</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1958年</p></td>
<td><p>《<a href="../Page/雁南飞.md" title="wikilink">雁南飞</a>》</p></td>
<td><p><em>Летят журавли</em></p></td>
<td><p><a href="../Page/米哈伊爾·卡拉托佐夫.md" title="wikilink">米哈伊爾·卡拉托佐夫</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1959年</p></td>
<td><p>《》</p></td>
<td><p><em>Camus Orfeu Negro</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1960年</p></td>
<td><p>《<a href="../Page/甜蜜的生活.md" title="wikilink">甜蜜的生活</a>》</p></td>
<td><p><em>La Dolce Vita</em></p></td>
<td><p><a href="../Page/費德里柯·費里尼.md" title="wikilink">費德里柯·費里尼</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1961年</p></td>
<td><p>《》</p></td>
<td><p><em>Viridiana</em></p></td>
<td><p><a href="../Page/路易斯·布努埃爾.md" title="wikilink">路易斯·布纽尔</a></p></td>
<td><p><br />
</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Une aussi longue absence</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1962年</p></td>
<td><p>《<a href="../Page/諾言.md" title="wikilink">諾言</a>》</p></td>
<td><p><em>O pagador de Promessas</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1963年</p></td>
<td><p>《<a href="../Page/氣蓋山河.md" title="wikilink">浩氣蓋山河</a>》</p></td>
<td><p><em>II Gattopardo</em></p></td>
<td><p><a href="../Page/盧奇諾·維斯孔蒂.md" title="wikilink">盧契諾·維斯康堤</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1964年</p></td>
<td><p>《》</p></td>
<td><p><em>Les Parapluies de Cherbourg</em></p></td>
<td><p><a href="../Page/雅克·德米.md" title="wikilink">雅克·德米</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1965年</p></td>
<td><p>《》</p></td>
<td><p><em>The Knack ...and How to Get It</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1966年</p></td>
<td><p>《<a href="../Page/男欢女爱.md" title="wikilink">男欢女爱</a>》</p></td>
<td><p><em>Un Homme et une Femme</em></p></td>
<td><p><a href="../Page/克勞德·勞路許.md" title="wikilink">克勞德·勞路許</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Signore e Signori</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1967年</p></td>
<td><p>《<a href="../Page/春光乍現.md" title="wikilink">春光乍現</a>》</p></td>
<td><p><em>Blow-up</em></p></td>
<td><p><a href="../Page/米開朗基羅·安東尼奧尼.md" title="wikilink">米開朗基羅·安東尼奧尼</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1968年</p></td>
<td><p>colspan = 4|影展未舉辦</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1969年</p></td>
<td><p>《<a href="../Page/如果….md" title="wikilink">如果…</a>》</p></td>
<td><p><em>If....</em></p></td>
<td><p><a href="../Page/林赛·安德森.md" title="wikilink">林赛·安德森</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1970年</p></td>
<td><p>《<a href="../Page/外科醫師_(電影).md" title="wikilink">外科醫師</a>》</p></td>
<td><p><em>MASH</em></p></td>
<td><p><a href="../Page/勞勃·阿特曼.md" title="wikilink">勞勃·阿特曼</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1971年</p></td>
<td><p>《》</p></td>
<td><p><em>The Go-between</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1972年</p></td>
<td><p>《》</p></td>
<td><p><em>La classe operaia va in paradiso</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1973年</p></td>
<td><p>《<a href="../Page/稻草人_(電影).md" title="wikilink">稻草人</a>》</p></td>
<td><p><em>Scarecrow</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>The Hireling</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1974年</p></td>
<td><p>《》</p></td>
<td><p><em>The Conversation</em></p></td>
<td><p><a href="../Page/法蘭西斯·柯波拉.md" title="wikilink">法蘭西斯·柯波拉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td><p>《》</p></td>
<td><p><em>وقائع سنين الجمر</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1976年</p></td>
<td><p>《<a href="../Page/出租车司机_(電影).md" title="wikilink">計程車司機</a>》</p></td>
<td><p><em>Taxi Driver</em></p></td>
<td><p><a href="../Page/马丁·斯科西斯.md" title="wikilink">马丁·斯科西斯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1977年</p></td>
<td><p>《<a href="../Page/我父我主.md" title="wikilink">我父我主</a>》</p></td>
<td><p><em>Padre, Padrone</em></p></td>
<td><p><a href="../Page/塔維亞尼兄弟.md" title="wikilink">保羅·塔維亞尼</a><br />
<a href="../Page/塔維亞尼兄弟.md" title="wikilink">維托里奧·塔維亞尼</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1978年</p></td>
<td><p>《》</p></td>
<td><p><em>L'Albero degli zoccoli</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1979年</p></td>
<td><p>《<a href="../Page/现代启示录.md" title="wikilink">现代启示录</a>》</p></td>
<td><p><em>Apocalypse Now</em></p></td>
<td><p><a href="../Page/法蘭西斯·柯波拉.md" title="wikilink">法蘭西斯·柯波拉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/錫鼓_(電影).md" title="wikilink">錫鼓</a>》</p></td>
<td><p><em>Die Blechtrommel</em></p></td>
<td><p><a href="../Page/沃克·施隆多夫.md" title="wikilink">沃克·施隆多夫</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1980年</p></td>
<td><p>《<a href="../Page/影武者_(電影).md" title="wikilink">影武者</a>》</p></td>
<td><p><em>影武者</em></p></td>
<td><p><a href="../Page/黑泽明.md" title="wikilink">黑泽明</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/爵士春秋.md" title="wikilink">爵士春秋</a>》</p></td>
<td><p><em>All That Jazz</em></p></td>
<td><p><a href="../Page/鮑勃·福斯.md" title="wikilink">鮑勃·福斯</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td><p>《》</p></td>
<td><p><em>Człowiek z żelaza</em></p></td>
<td><p><a href="../Page/安德烈·華依達.md" title="wikilink">安德烈·華依達</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1982年</p></td>
<td><p>《》</p></td>
<td><p><em>Missing</em></p></td>
<td><p><a href="../Page/科斯塔·加夫拉斯.md" title="wikilink">科斯塔·加夫拉斯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Yol</em></p></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1983年</p></td>
<td><p>《<a href="../Page/楢山节考_(1983年电影).md" title="wikilink">楢山节考</a>》</p></td>
<td><p><em>楢山節考</em></p></td>
<td><p><a href="../Page/今村昌平.md" title="wikilink">今村昌平</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p>《<a href="../Page/巴黎，德州.md" title="wikilink">巴黎，德州</a>》</p></td>
<td><p><em>Paris, Texas</em></p></td>
<td><p><a href="../Page/溫韋德斯.md" title="wikilink">文·溫德斯</a></p></td>
<td><p><br />
</p></td>
</tr>
<tr class="odd">
<td><p>1985年</p></td>
<td><p>《<a href="../Page/爸爸出差时.md" title="wikilink">爸爸出差时</a>》</p></td>
<td><p><em>Отац на службеном путу</em></p></td>
<td><p><a href="../Page/艾米爾·庫斯杜力卡.md" title="wikilink">艾米爾·庫斯杜力卡</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1986年</p></td>
<td><p>《<a href="../Page/战火浮生.md" title="wikilink">-{zh-hans:战火浮生;zh-hk:戰火浮生;zh-tw:教會;}-</a>》</p></td>
<td><p><em>The Mission</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p>《》</p></td>
<td><p><em>Sous le soleil de Satan</em></p></td>
<td><p><a href="../Page/莫里斯·皮亞拉.md" title="wikilink">莫里斯·皮亞拉</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p>《<a href="../Page/比利小英雄.md" title="wikilink">比利小英雄</a>》</p></td>
<td><p><em>Pelle erobreren</em></p></td>
<td><p><a href="../Page/比利·奥古斯特.md" title="wikilink">比利·奥古斯特</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p>《<a href="../Page/性、謊言、錄影帶.md" title="wikilink">性、謊言、錄影帶</a>》</p></td>
<td><p><em>Sex, Lies, and Videotape</em></p></td>
<td><p><a href="../Page/史蒂文·索德伯格.md" title="wikilink">史蒂芬·索德柏</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1990年</p></td>
<td><p>《》</p></td>
<td><p><em>Wild at Heart</em></p></td>
<td><p><a href="../Page/大衛·林區.md" title="wikilink">大衛·林區</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p>《<a href="../Page/巴顿·芬克.md" title="wikilink">巴顿·芬克</a>》</p></td>
<td><p><em>Barton Fink</em></p></td>
<td><p><a href="../Page/柯恩兄弟.md" title="wikilink">喬爾·柯恩</a><br />
<a href="../Page/柯恩兄弟.md" title="wikilink">伊森·柯恩</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p>《》</p></td>
<td><p><em>Den Goda viljan</em></p></td>
<td><p><a href="../Page/比利·奥古斯特.md" title="wikilink">比利·奥古斯特</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p>《<a href="../Page/鋼琴師和她的情人.md" title="wikilink">-{zh-tw:鋼琴師和她的情人; zh-hk:鋼琴別戀; zh-cn:钢琴课;}-</a>》</p></td>
<td><p><em>The Piano</em></p></td>
<td><p><a href="../Page/珍·康萍.md" title="wikilink">-{zh-hans:简·坎皮思; zh-hant:珍·康萍;}-</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/霸王別姬_(電影).md" title="wikilink">霸王别姬</a>》</p></td>
<td><p>Farewell My Concubine</p></td>
<td><p><a href="../Page/陳凱歌.md" title="wikilink">陳凱歌</a></p></td>
<td><p><br />
</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p>《<a href="../Page/黑色追緝令.md" title="wikilink">黑色追緝令</a>》</p></td>
<td><p><em>Pulp Fiction</em></p></td>
<td><p><a href="../Page/昆廷·塔伦蒂诺.md" title="wikilink">昆汀·塔倫提諾</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p>《<a href="../Page/地下社會_(電影).md" title="wikilink">-{zh-hans:地下; zh-hant:地下社會;}-</a>》</p></td>
<td><p><em>Подземље</em></p></td>
<td><p><a href="../Page/艾米爾·庫斯杜力卡.md" title="wikilink">艾米爾·庫斯杜力卡</a></p></td>
<td><p><a href="../Page/南斯拉夫聯盟共和國.md" title="wikilink">南斯拉夫</a></p></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p>《<a href="../Page/秘密与谎言.md" title="wikilink">秘密与谎言</a>》</p></td>
<td><p><em>Secrets &amp; Lie</em></p></td>
<td><p><a href="../Page/麥克·李.md" title="wikilink">麥克·李</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p>《<a href="../Page/鰻魚_(電影).md" title="wikilink">鰻魚</a>》</p></td>
<td><p><em>うなぎ</em></p></td>
<td><p><a href="../Page/今村昌平.md" title="wikilink">今村昌平</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/樱桃的滋味.md" title="wikilink">樱桃的滋味</a>》</p></td>
<td><p><em>طعم گيلاس</em></p></td>
<td><p><a href="../Page/阿巴斯·奇亞羅斯塔米.md" title="wikilink">阿巴斯·奇亞羅斯塔米</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p>《<a href="../Page/永恆與一天.md" title="wikilink">永遠的一天</a>》</p></td>
<td><p><em>Μια αιωνιότητα και μια μέρα</em></p></td>
<td><p><a href="../Page/泰奧·安哲羅普洛斯.md" title="wikilink">狄奧·安哲羅普洛斯</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p>《<a href="../Page/美麗蘿賽塔.md" title="wikilink">-{zh-hans:罗塞塔; zh-hant:美麗蘿賽塔;}-</a>》</p></td>
<td><p><em>Rosetta</em></p></td>
<td><p><a href="../Page/達頓兄弟.md" title="wikilink">尚-皮耶·達頓</a><br />
<a href="../Page/達頓兄弟.md" title="wikilink">盧·達頓</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p>《<a href="../Page/在黑暗中漫舞.md" title="wikilink">-{zh-hans:黑暗中的舞者;zh-hk:天黑黑;zh-tw:在黑暗中漫舞;}-</a>》</p></td>
<td><p><em>Dancer in the Dark</em></p></td>
<td><p><a href="../Page/拉斯·馮·提爾.md" title="wikilink">拉斯·馮·提爾</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p>《<a href="../Page/人间有情天.md" title="wikilink">人间有情天</a>》</p></td>
<td><p><em>La stanza del figlio</em></p></td>
<td><p><a href="../Page/南尼·莫瑞提.md" title="wikilink">南尼·莫瑞提</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p>《<a href="../Page/戰地琴人.md" title="wikilink">戰地琴人</a>》</p></td>
<td><p><em>The Pianist</em></p></td>
<td><p><a href="../Page/罗曼·波兰斯基.md" title="wikilink">罗曼·波兰斯基</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>《<a href="../Page/大象_(電影).md" title="wikilink">大象</a>》</p></td>
<td><p><em>Elephant</em></p></td>
<td><p><a href="../Page/葛斯·范·桑.md" title="wikilink">葛斯·范·桑</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p>《<a href="../Page/華氏911.md" title="wikilink">華氏911</a>》</p></td>
<td><p><em>Fahrenheit 9/11</em></p></td>
<td><p><a href="../Page/迈克尔·摩尔.md" title="wikilink">迈克尔·摩尔</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p>《<a href="../Page/孩子_(电影).md" title="wikilink">孩子</a>》</p></td>
<td><p><em>L'Enfant</em></p></td>
<td><p><a href="../Page/達頓兄弟.md" title="wikilink">尚-皮耶·達頓</a><br />
<a href="../Page/達頓兄弟.md" title="wikilink">盧·達頓</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p>《<a href="../Page/吹動大麥的風.md" title="wikilink">吹動大麥的風</a>》</p></td>
<td><p><em>The Wind That Shakes the Barley</em></p></td>
<td><p><a href="../Page/肯·洛區.md" title="wikilink">肯·洛區</a></p></td>
<td><p><br />
</p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>《<a href="../Page/4月3周又2天.md" title="wikilink">4月3周又2天</a>》</p></td>
<td><p><em>4 luni, 3 săptămâni şi 2 zile</em></p></td>
<td><p><a href="../Page/克里斯蒂安·蒙久.md" title="wikilink">克里斯汀·穆基</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p>《<a href="../Page/我和我的小鬼們.md" title="wikilink">我和我的小鬼們</a>》</p></td>
<td><p><em>Entre les murs</em></p></td>
<td><p><a href="../Page/洛宏·康鐵.md" title="wikilink">洛宏·康鐵</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p>《<a href="../Page/白色緞帶.md" title="wikilink">白色緞帶</a>》</p></td>
<td><p><em>Das weiße Band</em></p></td>
<td><p><a href="../Page/迈克尔·哈内克.md" title="wikilink">麥可·漢內克</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p>《<a href="../Page/波米叔叔的前世今生.md" title="wikilink">波米叔叔的前世今生</a>》</p></td>
<td><p><em>ลุงบุญมีระลึกชาติ</em></p></td>
<td><p><a href="../Page/阿比查邦·魏拉希沙可.md" title="wikilink">阿比查邦·韋拉斯塔古</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>《<a href="../Page/生命樹_(電影).md" title="wikilink">永生樹</a>》</p></td>
<td><p><em>The Tree of Life</em></p></td>
<td><p><a href="../Page/泰伦斯·马力克.md" title="wikilink">泰伦斯·马力克</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第65届戛纳电影节.md" title="wikilink">2012年</a></p></td>
<td><p>《<a href="../Page/愛·慕.md" title="wikilink">愛·慕</a>》</p></td>
<td><p><em>Amour</em></p></td>
<td><p><a href="../Page/迈克尔·哈内克.md" title="wikilink">麥可·漢內克</a></p></td>
<td><p><br />
<br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第66届戛纳电影节.md" title="wikilink">2013年</a></p></td>
<td><p>《<a href="../Page/阿黛尔的生活.md" title="wikilink">藍色是最溫暖的顏色</a>》</p></td>
<td><p><em>La vie d'Adèle</em></p></td>
<td><p><a href="../Page/阿布戴·柯西胥.md" title="wikilink">阿布戴·柯西胥</a><br />
(與演員<a href="../Page/阿黛爾·艾薩柯普洛斯.md" title="wikilink">阿黛爾·艾薩柯普洛斯</a><br />
和<a href="../Page/蕾雅·瑟杜.md" title="wikilink">蕾雅·瑟杜共享</a>)[2]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第67届戛纳电影节.md" title="wikilink">2014年</a></p></td>
<td><p>《<a href="../Page/冬眠_(电影).md" title="wikilink">冬日甦醒</a>》</p></td>
<td><p><em>Kış Uykusu</em></p></td>
<td><p><a href="../Page/努瑞·貝其·錫蘭.md" title="wikilink">努瑞·貝其·錫蘭</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第68届戛纳电影节.md" title="wikilink">2015年</a></p></td>
<td><p>《<a href="../Page/流浪的迪潘.md" title="wikilink">流離者之歌</a>》</p></td>
<td><p><em>Dheepan</em></p></td>
<td><p><a href="../Page/贾克·欧迪亚.md" title="wikilink">贾克·欧迪亚</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第69届戛纳电影节.md" title="wikilink">2016年</a></p></td>
<td><p>《<a href="../Page/我是布莱克.md" title="wikilink">我是布莱克</a>》</p></td>
<td><p><em>I, Daniel Blake</em></p></td>
<td><p><a href="../Page/肯·洛區.md" title="wikilink">肯·洛區</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第70屆坎城影展.md" title="wikilink">2017年</a></p></td>
<td><p>《<a href="../Page/自由廣場_(2017年電影).md" title="wikilink">抓狂美術館</a>》</p></td>
<td><p><em>The Square</em></p></td>
<td><p><a href="../Page/魯本·奧斯特倫德.md" title="wikilink">魯本·奧斯倫</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第71屆坎城影展.md" title="wikilink">2018年</a></p></td>
<td><p>《<a href="../Page/小偷家族.md" title="wikilink">小偷家族</a>》</p></td>
<td><p><em>-{万引き家族}-</em></p></td>
<td><p><a href="../Page/是枝裕和.md" title="wikilink">是枝裕和</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 多次獲獎者

  - 2次 - [法蘭西斯·柯波拉](../Page/法蘭西斯·柯波拉.md "wikilink") (1974, 1979)
  - 2次 - [比利·奥古斯特](../Page/比利·奥古斯特.md "wikilink") (1988, 1992)
  - 2次 - [艾米爾·庫斯杜力卡](../Page/艾米爾·庫斯杜力卡.md "wikilink") (1985, 1995)
  - 2次 - [今村昌平](../Page/今村昌平.md "wikilink") (1983, 1997)
  - 2次 -
    [尚-皮耶·達頓](../Page/達頓兄弟.md "wikilink")、[盧·達頓](../Page/達頓兄弟.md "wikilink")
    (1999、2005)
  - 2次 - [麥可·漢內克](../Page/麥可·漢內克.md "wikilink") (2009, 2012)
  - 2次 - [肯·洛區](../Page/肯·洛區.md "wikilink") (2006, 2016)

## 榮譽金棕櫚獎

榮譽金棕櫚獎是非常設獎項，主要是授予其電影作品有極大影響力，但從未獲得金棕櫚獎的導演。\[3\]

<table>
<thead>
<tr class="header">
<th><p>年</p></th>
<th><p>獲獎者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/伍迪·艾倫.md" title="wikilink">伍迪·艾倫</a></p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/克林·伊斯威特.md" title="wikilink">克林·伊斯威特</a></p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/貝納多·貝托魯奇.md" title="wikilink">貝納多·貝托魯奇</a></p></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/阿涅斯·瓦尔达.md" title="wikilink">-{zh-cn:阿涅斯·瓦尔达;zh-tw:安妮·華達;zh-hk:艾麗絲·華妲;}-</a></p></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p><a href="../Page/尚盧·高達.md" title="wikilink">-{zh-hans: 让·吕克·戈达尔; zh-hant:尚盧·高達;}-</a></p></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部連結

  - [Palme d'Or
    Winners](http://www.boxofficemojo.com/genres/chart/?id=cannes.htm),
    1976 to present, by gross box-office.
  - [All the Palmes
    d'Or](http://www.festival-cannes.com/palmares/prix.php?langue=6002&categorie=lms).
    [Festival-cannes.com](http://www.festival-cannes.com/).
  - [Cannes Film
    Festival](http://www.imdb.com/Sections/Awards/Cannes_Film_Festival/).
    [IMDB](../Page/The_Internet_Movie_Database.md "wikilink").

{{-}}

[Category:戛纳影展奖项](../Category/戛纳影展奖项.md "wikilink")
[金棕櫚獎獲獎電影](../Category/金棕櫚獎獲獎電影.md "wikilink")
[Category:1955年歐洲建立](../Category/1955年歐洲建立.md "wikilink")
[Category:1955年建立的奖项](../Category/1955年建立的奖项.md "wikilink")

1.  <http://www.festival-cannes.com/en/about/palmeHistory.html>
2.
3.  [A Palme d’honneur to Agnès
    Varda](http://www.festival-cannes.fr/en/article/61348.html)