**西方公司**（，WEI），為[美國派駐於](../Page/美國.md "wikilink")[臺灣的機構之一](../Page/臺灣.md "wikilink")，於1951年2月在美國[賓夕法尼亞州](../Page/賓夕法尼亞州.md "wikilink")[匹茲堡正式註冊成立](../Page/匹茲堡.md "wikilink")，由最早開始與[中華民國政府進行秘密接觸的查理](../Page/中華民國政府.md "wikilink")·詹斯頓（Charles
S. Johnston）擔任[董事長](../Page/董事長.md "wikilink")，則負責主持西方公司駐台北辦事處的業務。\[1\]

西方公司表面上是民間公司，但實際上隸屬於美國[中央情報局](../Page/中央情報局.md "wikilink")，是一個秘密機構。其成員為自[美軍各](../Page/美軍.md "wikilink")[戰鬥單位選出的七十多位精銳軍官團組成](../Page/戰鬥單位.md "wikilink")，負責訓練[中華民國國軍](../Page/中華民國國軍.md "wikilink")。1955年初，西方公司結束運作，將業務轉移給另一個中央情報局派駐台灣的秘密單位
**美國海軍輔助通訊中心**（，NACC）。

## 歷史

[韓戰爆發後](../Page/韓戰.md "wikilink")，美國為了牽制[中华人民共和国政府以免其抽調](../Page/中华人民共和国政府.md "wikilink")[中國人民解放軍沿海兵力到](../Page/中國人民解放軍.md "wikilink")[朝鲜半岛作戰](../Page/朝鲜半岛.md "wikilink")，故同意[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣中正夫人](../Page/蔣中正.md "wikilink")[宋美齡與](../Page/宋美齡.md "wikilink")[陳納德的提議](../Page/陳納德.md "wikilink")，招募[第二次世界大戰時的情報老手與](../Page/第二次世界大戰.md "wikilink")[特種作戰專家](../Page/特種作戰.md "wikilink")，以名義上經營進出口[貿易的西方公司為掩護](../Page/貿易.md "wikilink")，在臺灣訓練[游擊隊](../Page/游擊隊.md "wikilink")，策畫並執行突擊中國大陸與蒐集情報的工作。西方公司也放縱台灣對中國大陸沿海[海運的騷擾行為](../Page/海運.md "wikilink")，讓[中華民國海軍在](../Page/中華民國海軍.md "wikilink")[公海攔截扣押前往中國大陸的船隻](../Page/公海.md "wikilink")，如[陶普斯號事件等](../Page/陶普斯號事件.md "wikilink")。

西方公司實際上隸屬於美國[中央情報局主管全球地下活動的](../Page/中央情報局.md "wikilink")，總部設於臺北市[中山北路](../Page/中山北路.md "wikilink")[圓山附近](../Page/圓山.md "wikilink")，活動基地則在[大陳島](../Page/大陳島.md "wikilink")、[金門](../Page/金門.md "wikilink")、[馬祖等外島](../Page/馬祖.md "wikilink")。\[2\]
為了兼顧美國的中立化政策與行動彈性，雙方外交、美國軍事單位（包含[駐台美軍](../Page/駐台美軍.md "wikilink")）並未直接參與西方公司對中國大陸的行動。

1951年3月，中情局人員開始大批湧入台灣，與中情局人員一同抵達台灣的還包括供應外島游擊隊使用的第一批中情局軍火物資，包括1740箱高爆炸藥、680支[卡賓槍](../Page/卡賓槍.md "wikilink")、420挺輕機槍、200把[手槍](../Page/手槍.md "wikilink")、25箱[無線電設備](../Page/無線電.md "wikilink")、70架火箭發射器以及37萬9000發彈藥。當時[浙江外海由國府控制的一連串小島](../Page/浙江.md "wikilink")，包括漁山、披山、南麂、北麂、[一江山以及上](../Page/一江山.md "wikilink")、下[大陳島等地](../Page/大陳島.md "wikilink")，首先被選定為西方公司人員進行整編、訓練游擊隊，並且實施沿海秘密突襲任務的行動基地。\[3\]\[4\]

西方公司培訓了駐於金門與大陳島的[南海集訓總隊](../Page/南海集訓總隊.md "wikilink")，1952年爆發的[南日島戰役即是由南海集訓總隊執行](../Page/南日島戰役.md "wikilink")。另一方面的行動則是由[李彌留在](../Page/李彌.md "wikilink")[緬甸](../Page/緬甸.md "wikilink")[泰國邊界的](../Page/泰國.md "wikilink")[泰北孤軍執行](../Page/泰北孤軍.md "wikilink")。

西方公司的全盛時期是在[朝鲜战争期間](../Page/朝鲜战争.md "wikilink")（1951—1953）。韓戰結束後，美國降低對中國大陸滲透與游擊行動的支持，西方公司的階段性任務也告一段落，其遺留業務由海軍輔助通訊中心（，簡稱NACC）接管，但中華民國國軍各軍種與美方的特種任務合作計畫仍然習稱對方為西方公司。

## 參看

  - [IQT電信](../Page/IQT電信.md "wikilink")
  - [85點行動](../Page/85點行動.md "wikilink")(85 point campaign/14 april
    1958/Matsu/馬祖/)\[5\]

## 參考來源

  - 傳記

<!-- end list -->

  -
  -
<!-- end list -->

  - 引用

[Category:美國已結業公司](../Category/美國已結業公司.md "wikilink")
[Category:美國中央情報局](../Category/美國中央情報局.md "wikilink")
[Category:中華民國與美國關係](../Category/中華民國與美國關係.md "wikilink")
[Category:1951年成立的公司](../Category/1951年成立的公司.md "wikilink")
[Category:1953年解散的組織](../Category/1953年解散的組織.md "wikilink")

1.

2.

3.

4.
5.