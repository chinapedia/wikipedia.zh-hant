**陈执中**（990年－1059年），字昭誉，[北宋](../Page/北宋.md "wikilink")[洪州](../Page/洪州.md "wikilink")[南昌人](../Page/南昌.md "wikilink")。

出生於官宦世家，其父[陈恕官至副相的](../Page/陈恕.md "wikilink")“盐铁使”，真宗时以父荫入仕秘书省正字，累迁卫尉寺丞，後任梧州知縣。真宗時，上《復古要道》三篇和《演要》三篇，“以蚤（早）定天下根本为说，真宗异而召之”。仁宗宝元元年（1038年）同知枢密院事\[1\]。庆历元年（1041年）以同治枢院事出知青州，轉運使[沈邈很輕視他](../Page/沈邈.md "wikilink")，常批評他擾民，改永兴军，拜右谏议大夫、同知枢密院事。庆历四年（1044年）召拜参知政事，知河南府，改尚书工部侍郎、陕西同经略安抚招讨使。庆历五年入相，同平章事、集贤殿大学士兼枢密使。皇佑元年（1049年）以足疾辞职，出知陈州。皇佑五年（1054年）再入相。至和二年（1055年）充镇海军节度使判亳州\[2\]。以[司徒致仕](../Page/司徒.md "wikilink")。

执中在官场颇有作为，以为官清廉、不徇私闻名，有一次他的女婿求差遣，执中說：“官职是国家的，非卧房笼箧中物，婿安得有之？”最後沒給官。深得仁宗皇帝的垂青。陈执中家中的小妾张氏暴戾，三次殺婢\[3\]，被言官彈劾，御史中丞[孫抃和殿中侍御史](../Page/孫抃.md "wikilink")[赵抃聯名参奏执中八条罪状](../Page/赵抃.md "wikilink")，又以[欧阳修在](../Page/欧阳修.md "wikilink")《论台谏官言事未蒙听允书》言辞最为激烈，至和三年（1056年）春天大旱，知諫院[范镇及殿中侍御史](../Page/范镇.md "wikilink")[赵林等劾执中](../Page/赵林.md "wikilink")，遂以以镇海军节度使兼亳州知州，不久以[司徒退休](../Page/司徒.md "wikilink")。嘉佑四年（1059年）卒\[4\]。谥恭。

執中之子[陳世儒是小妾所生](../Page/陳世儒.md "wikilink")，官至国子博士，曾出任舒州（今安徽潛山）太湖縣（今安徽太湖）知縣。御史台判其參與毒殺生母。宋神宗感念陳執中，說：“止一子，留以存祭祀何如？”但[蔡黃裳之子御史中丞](../Page/蔡黃裳.md "wikilink")[蔡確堅持依法處刑](../Page/蔡確.md "wikilink")，陳世儒終被殺。陳世儒案發時，[蘇頌當時知開封府事](../Page/蘇頌.md "wikilink")，主管陳世儒案件，被御史[舒亶彈劾](../Page/舒亶.md "wikilink")“故縱”，降為[秘書監](../Page/秘書監.md "wikilink")。苏颂因陈世儒案也被下狱.
御史曰：“公速自言，毋重困辱。”苏颂严词拒绝：“诬人，死不可为。”面对执政者一意孤行，苏颂留诗“深冤终有辨明时”。

## 注釋

## 參考書目

  - 《[宋史](../Page/宋史.md "wikilink")》卷二八五
  - 《江西通志》卷六十六

[Z](../Page/category:陈姓.md "wikilink")
[C](../Page/category:南昌人.md "wikilink")

[C](../Category/宋朝宰相.md "wikilink")

1.  《宋宰辅编年录》卷四
2.  《宋宰辅编年录》卷五
3.  [司马光](../Page/司马光.md "wikilink")：《涑水记闻》
4.  《乐全集》卷三七《陈公神道碑铭》