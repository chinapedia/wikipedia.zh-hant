**广宁县**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[肇庆市下辖的一个](../Page/肇庆市.md "wikilink")[县](../Page/县.md "wikilink")。面积2380平方千米，人口54万。邮政编码526300。

## 行政区划

下辖15个[镇](../Page/镇.md "wikilink")：

。

## 歷史

[明](../Page/明.md "wikilink")[嘉靖三十八年十月戊戌](../Page/嘉靖.md "wikilink")（1559年10月30日）置廣寧縣，取「廣泛安寧」之意。据《今县释名》：“明嘉靖三十八年（1559年）平大罗山贼，因置县。”取广泛安宁之意，即“广宁”寓“广泛安宁”义。1952年5月，广宁、四会合并为广四县；1954年7月分设；1958年10月，两县又合并为广四县；1961年4月再分设至今，建县以来一直隶属肇庆管辖。

## 地理

廣寧縣屬[綏江中游](../Page/綏江.md "wikilink")[谷地](../Page/谷地.md "wikilink")，[綏江從西北向東南流縣境](../Page/綏江.md "wikilink")，形成兩邊高中間低的斜凹地形。

## 氣候

1月均溫12.5℃，7月均溫28.5℃，年降水量1,734毫米。

## 自然資源

  - 礦藏：[鉭鈮](../Page/鉭鈮.md "wikilink")、[黃金](../Page/黃金.md "wikilink")、[廣綠玉石](../Page/廣綠玉石.md "wikilink")、[瓷土](../Page/瓷土.md "wikilink")、[花岩岩等廿多種](../Page/花岩岩.md "wikilink")，其中[橫山](../Page/橫山.md "wikilink")[鉭鈮是全國少有的高](../Page/鉭鈮.md "wikilink")[鉭礦](../Page/鉭.md "wikilink")。
  - 林木：[青皮竹](../Page/青皮竹.md "wikilink")、[箭竹](../Page/箭竹.md "wikilink")、[茶杆竹等用材竹](../Page/茶杆竹.md "wikilink")，有[文筍](../Page/文筍.md "wikilink")、[甜筍等食用竹](../Page/甜筍.md "wikilink")，亦有[吊絲竹](../Page/吊絲竹.md "wikilink")、[扁竹](../Page/扁竹.md "wikilink")、[佛肚竹等觀賞竹](../Page/佛肚竹.md "wikilink")；因此廣寧縣在全國有「竹子之鄉」美譽，拥有竹林108万亩，年产竹子25万吨。

## 經濟

  - 農業：主種[稻](../Page/稻.md "wikilink")，次為[木薯](../Page/木薯.md "wikilink")、[番薯](../Page/番薯.md "wikilink")、[花生](../Page/花生.md "wikilink")、[黃菸](../Page/黃菸.md "wikilink")、[甘蔗等](../Page/甘蔗.md "wikilink")；是[廣東省主要林產區之一](../Page/廣東省.md "wikilink")，建立了[竹子](../Page/竹子.md "wikilink")、[竹筍](../Page/竹筍.md "wikilink")、[松脂](../Page/松脂.md "wikilink")、木薯、[水果](../Page/水果.md "wikilink")、[藥材](../Page/藥材.md "wikilink")、[茶葉](../Page/茶葉.md "wikilink")、[蠶桑等基地](../Page/蠶桑.md "wikilink")。
  - 工業：林產、[化工](../Page/化工.md "wikilink")、[澱粉](../Page/澱粉.md "wikilink")、藥用[葡萄糖](../Page/葡萄糖.md "wikilink")、建築[陶瓷](../Page/陶瓷.md "wikilink")、[油墨](../Page/油墨.md "wikilink")、[塑料](../Page/塑料.md "wikilink")、[造紙](../Page/造紙.md "wikilink")、竹器工藝等。

2002年廣寧縣[國內生產總值](../Page/國內生產總值.md "wikilink")34億8千萬元[人民幣](../Page/人民幣.md "wikilink")。

## 交通

[广贺高速公路](../Page/广贺高速公路.md "wikilink")、[贵广高速铁路](../Page/贵广高速铁路.md "wikilink")。

## 旅遊名勝

[Huaiji2.JPG](https://zh.wikipedia.org/wiki/File:Huaiji2.JPG "fig:Huaiji2.JPG")
[Huaiji3.JPG](https://zh.wikipedia.org/wiki/File:Huaiji3.JPG "fig:Huaiji3.JPG")
有[萬竹園當代書法碑林](../Page/萬竹園.md "wikilink")、宝锭山、深坑蛇影、羅鍋竹海等。

最近两年，广宁大力发展旅游业，目前最受欢迎的景点包括“竹海大观”，“碧翠湖”吸引了中外游客前来游览。

## 特產

[廣寧竹](../Page/廣寧竹.md "wikilink")、[廣綠玉石](../Page/廣綠玉石.md "wikilink")、[清桂茶等](../Page/清桂茶.md "wikilink")。

[廣綠玉石與](../Page/廣綠玉石.md "wikilink")[雞血石](../Page/雞血石.md "wikilink")、[壽山石](../Page/壽山石.md "wikilink")、[林西石](../Page/林西石.md "wikilink")、[青田石並稱](../Page/青田石.md "wikilink")[中國五大佳玉](../Page/中國五大佳玉.md "wikilink")。

## 參考資料

## 外部連結

  - [廣寧縣人民政府网站](http://www.gdgn.gov.cn/)

[广宁县](../Category/广宁县.md "wikilink")
[县](../Category/肇庆区县市.md "wikilink")
[肇庆](../Category/广东省县份.md "wikilink")
[Category:中国十大竹子之乡](../Category/中国十大竹子之乡.md "wikilink")