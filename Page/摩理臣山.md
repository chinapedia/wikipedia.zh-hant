[Morrison_Hill_2010.jpg](https://zh.wikipedia.org/wiki/File:Morrison_Hill_2010.jpg "fig:Morrison_Hill_2010.jpg")
**摩理臣山**（，又譯作**摩利臣山**）是[香港一座已被夷平的小山丘](../Page/香港.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[活道一帶](../Page/活道.md "wikilink")。

摩理臣山的名字取自19世紀來華傳道的[馬禮遜牧師](../Page/馬禮遜.md "wikilink")。摩理臣山曾經是一個[石礦場](../Page/石礦場.md "wikilink")。在灣仔[填海之前](../Page/填海.md "wikilink")，該地處於海旁。

## 歷史

### 開埠之初

1842年，中英簽訂《[南京條約](../Page/南京條約.md "wikilink")》，條約內容包括[清廷割讓](../Page/清廷.md "wikilink")[香港島予](../Page/香港島.md "wikilink")[英國](../Page/英國.md "wikilink")。開埠之初，到香港的[傳教士認定這是個傳教大好機會](../Page/傳教士.md "wikilink")，當時，[倫敦教會在](../Page/倫敦會.md "wikilink")1842年2月通過決議，並致電當時香港的英國代表[砵甸乍](../Page/砵甸乍.md "wikilink")（後來1843年成為第一任[港督](../Page/港督.md "wikilink")），向他要一塊土地用作建造教堂和學校。結果，教會在當時的摩利臣山興建了[馬禮遜學堂](../Page/馬禮遜學堂.md "wikilink")
，是香港最早的西式學校，也是第一間英國人的教會學校。該校於1842年11月在今灣仔摩利臣山山頂建成。（Morrison
Hill，今譯「摩利臣山」即因馬禮遜而得名）。馬禮遜英華學校只辦了六年於1849年關閉，原因是摩利臣山含有石礦及政府發展交通網需要，該山數十年後經已剷平。[Hong_Kong_Wan_Chai_Morrison_Hill_Ambulance_Depot.JPG](https://zh.wikipedia.org/wiki/File:Hong_Kong_Wan_Chai_Morrison_Hill_Ambulance_Depot.JPG "fig:Hong_Kong_Wan_Chai_Morrison_Hill_Ambulance_Depot.JPG")\]\]

### 夷平發展

石礦開採完畢後，摩理臣山亦被夷平。現在[香港專業教育學院摩理臣山分校至今仍有一角石牆為紀念](../Page/香港專業教育學院摩理臣山分校.md "wikilink")。就現時的街道分佈來看，[摩理臣山游泳池就是原摩理臣山的中心點](../Page/摩理臣山游泳池.md "wikilink")，而圍繞著摩理臣山的有[愛群道](../Page/愛群道.md "wikilink")、幾所[中學](../Page/中學.md "wikilink")、[醫院和](../Page/醫院.md "wikilink")[伊利莎伯體育館](../Page/伊利莎伯體育館.md "wikilink")。摩理臣山東面則有[摩理臣山道](../Page/摩理臣山道.md "wikilink")。

香港人口統計中摩理臣山代表灣仔摩理臣山周邊一帶。

## 參見

  - [香港山峰](../Page/香港山峰.md "wikilink")
  - [灣仔](../Page/灣仔.md "wikilink")
  - [黃泥涌村](../Page/黃泥涌村.md "wikilink")
  - [摩理臣山道](../Page/摩理臣山道.md "wikilink")

## 外部連結

  - [香港掌故【街道名稱的由來】](http://hk.epochtimes.com/7/5/18/44897.htm)

{{-}}

[摩理臣山](../Category/摩理臣山.md "wikilink")
[Category:香港已消失山峰](../Category/香港已消失山峰.md "wikilink")
[Category:灣仔](../Category/灣仔.md "wikilink")