[Map-TurkicLanguages.png](https://zh.wikipedia.org/wiki/File:Map-TurkicLanguages.png "fig:Map-TurkicLanguages.png")

**突厥语族**（Turkic
languages）是[阿尔泰语系中最大的一个语族](../Page/阿尔泰语系.md "wikilink")，语族内部包括40多种语言。**突厥语族语言**简称突厥语，突厥语的使用人口在1.65亿\[1\]至2亿人\[2\]之间，主要分布在[欧亚大陆上](../Page/欧亚大陆.md "wikilink")。东起中国[甘肃](../Page/甘肃.md "wikilink")、[青海](../Page/青海.md "wikilink")，西至[東歐](../Page/東歐.md "wikilink")，北自[西伯利亞](../Page/西伯利亞.md "wikilink")，南抵[伊朗](../Page/伊朗.md "wikilink")。
突厥语是一种[黏着语](../Page/黏着语.md "wikilink")，[元音和谐是该语言的显著特征之一](../Page/元音和谐律.md "wikilink")。突厥语有悠久的书面语历史，有文字的记载最早可追溯至公元6世纪[突厥汗国时期的](../Page/突厥汗国.md "wikilink")[古突厥语碑铭](../Page/古突厥语.md "wikilink")，并在各历史时期作为不同国家和地区的[官方语言与国际交流用语](../Page/官方语言.md "wikilink")。现在，[土耳其语是突厥语中使用人口最多的一种](../Page/土耳其语.md "wikilink")。在[土耳其语中](../Page/土耳其语.md "wikilink")，突厥语和[土耳其语是同一个词](../Page/土耳其语.md "wikilink")。

## 语言特征

### 语音

突厥语语音系统的标志是数对前后对立的元音和清浊对立的辅音依语音和谐规律相搭配。

#### 元音

突厥语的元音大多由4对构成前后对立的元音组成，并在词汇及其附加成分上保持严整的元音和谐规律。

|     |   |   |   |   |
| --- | - | - | - | - |
| 前元音 | ä | i | ö | ü |
| 后元音 | a | ı | o | u |

学者还普遍认为，从[古突厥语时期即存在一个央元音e](../Page/古突厥语.md "wikilink")，至今依然保留在许多现在突厥语中。许多现代突厥语也有不完整的长元音存在，但许多长元音是第二性的。同时，[古突厥语存在长短元音的对立的观点也逐渐被普遍接受](../Page/古突厥语.md "wikilink")（但在长元音是第一性的还是第二性的问题上尚无共识）。

|      |    |    |    |    |
| ---- | -- | -- | -- | -- |
| 长前元音 | ä: | i: | ö: | ü: |
| 长后元音 | a: | ı: | o: | u: |
| 短前元音 | ä  | i  | ö  | ü  |
| 短后元音 | a  | ı  | o  | u  |

个别现代突厥语失去了ö和ü，如[乌兹别克语](../Page/乌兹别克语.md "wikilink")。所以现代突厥语的元音音位系统是由6至9个元音构成的。

#### 辅音

#### 音节

### 形态

### 句法

### 词汇

## 历史演变

### 古代前期

### 古代后期

### 中古时期

### 近代的分化

### 现代时期

## 语组的划分

由于现代突厥语各民族语言之间的差别远小于[印欧语系](../Page/印欧语系.md "wikilink")、[汉藏语系等语系中同语族语言之间的差别](../Page/汉藏语系.md "wikilink")，这本身就不易于对他们语言的划分。加之[突厥人在](../Page/突厥人.md "wikilink")[欧亚大陆的分布区域极广](../Page/欧亚大陆.md "wikilink")，又彼此长期混居，同时还分别与操其他语言的民族相邻，这就使对突厥语内部诸语言（方言）的划分有多种不同的看法。

### 依部落名称划分

### 依地理位置划分

分为[北突厥语支](../Page/北突厥语支.md "wikilink")（包括[阿尔泰语](../Page/阿尔泰语.md "wikilink")、[图瓦语](../Page/图瓦语.md "wikilink")）、[东突厥语支](../Page/东突厥语支.md "wikilink")（包括[维吾尔语](../Page/维吾尔语.md "wikilink")、[乌兹别克语](../Page/乌兹别克语.md "wikilink")）、[南突厥语支](../Page/南突厥语支.md "wikilink")（包括[阿塞拜疆语](../Page/阿塞拜疆语.md "wikilink")、[土耳其语](../Page/土耳其语.md "wikilink")、[土库曼语](../Page/土库曼语.md "wikilink")）和[西突厥语支](../Page/西突厥语支.md "wikilink")（包括[哈萨克语](../Page/哈萨克语.md "wikilink")、[吉尔吉斯语](../Page/吉尔吉斯语.md "wikilink")）。

## 与其他语言的交流

### 与阿尔泰语系其他语言

#### 蒙古语族

#### 满—通古斯语族

### 与印欧语系语言的交流

#### 塞语

#### 吐火罗语

#### 粟特语

#### 梵语

#### 波斯语

#### 英语、意大利语、法语、德语和俄语

### 与闪—含语系语言的交流

#### 阿拉伯语

### 与汉语的交流

### 与希腊语的交流

## 历史上使用过的文字

### 现代诸突厥语列表

  - 共同书面语
      - [共同突厥语](../Page/共同突厥语.md "wikilink")——[突厥如尼文](../Page/突厥文.md "wikilink")（7～10世纪）、[回鹘文](../Page/回鹘文.md "wikilink")（8～13世纪）
      - [哈卡尼亚语](../Page/哈卡尼亚语.md "wikilink")——[哈卡尼亚文](../Page/哈卡尼亚文.md "wikilink")（10～13世纪）
      - [花剌子模语](../Page/花剌子模语.md "wikilink")——[花剌子模文](../Page/花剌子模文.md "wikilink")（12～15世纪）
      - （东部使用）[察合台语](../Page/察合台语.md "wikilink")——[察合台文](../Page/察合台文.md "wikilink")（15～20世纪）
      - （西部使用）[奥斯曼语](../Page/奥斯曼语.md "wikilink")——[奥斯曼文](../Page/奥斯曼文.md "wikilink")（15～20世纪）
  - 现代活语言（不涉及语支分类问题，仅按语组列出）
      - [回鹘—葛逻禄语组](../Page/回鹘—葛逻禄语组.md "wikilink")（[维吾尔—葛逻禄语组](../Page/维吾尔—葛逻禄语组.md "wikilink")）
          - [维吾尔语](../Page/维吾尔语.md "wikilink")（中国、哈萨克斯坦、乌兹别克斯坦、吉尔吉斯斯坦、巴基斯坦）
          - [乌兹别克语](../Page/乌兹别克语.md "wikilink")（乌兹别克斯坦、吉尔吉斯斯坦、哈萨克斯坦、阿富汗、塔吉克斯坦、中国）
          - [艾努语](../Page/艾努语.md "wikilink")（有争议，中国新疆[喀什](../Page/喀什.md "wikilink")）
          - 图兰语（有争议，中国内蒙古自治区）
      - [欽察语组](../Page/欽察语组.md "wikilink")
          - [哈萨克语](../Page/哈萨克语.md "wikilink")（哈萨克斯坦、中国、蒙古）
          - [吉尔吉斯语](../Page/吉尔吉斯语.md "wikilink")（[柯尔克孜语](../Page/柯尔克孜语.md "wikilink")，吉尔吉斯斯坦、中国）
          - [塔塔尔语](../Page/塔塔尔语.md "wikilink")、[鞑靼语](../Page/鞑靼语.md "wikilink")（鞑靼斯坦、哈萨克斯坦、中国）
          - [诺盖语](../Page/诺盖语.md "wikilink")（[车臣](../Page/车臣.md "wikilink")）
          - [巴什基尔语](../Page/巴什基尔语.md "wikilink")（巴什基尔斯坦）
          - [卡拉卡尔帕克语](../Page/卡拉卡尔帕克语.md "wikilink")（乌兹别克斯坦）
          - [犹太-克里米亚鞑靼语](../Page/犹太-克里米亚鞑靼语.md "wikilink")（乌兹别克斯坦）
          - [卡拉恰伊-巴尔卡尔语](../Page/卡拉恰伊-巴尔卡尔语.md "wikilink")（俄罗斯卡拉切、巴尔卡尔）
          - [库梅克语](../Page/库梅克语.md "wikilink")（俄罗斯达吉斯坦）
          - [卡拉伊姆语](../Page/卡拉伊姆语.md "wikilink")（立陶宛）
      - [烏古斯语组](../Page/烏古斯语组.md "wikilink")
          - 土耳其语（土耳其、德国、巴尔干地区、北塞浦路斯共和国）
          - [阿塞拜疆语](../Page/阿塞拜疆语.md "wikilink")（伊朗、阿塞拜疆）
          - [土库曼语](../Page/土库曼语.md "wikilink")（土库曼斯坦、乌兹别克斯坦、伊朗）
          - [哈拉吉语](../Page/哈拉吉语.md "wikilink")（伊朗）
          - [卡什加语](../Page/卡什加语.md "wikilink")（伊朗）
          - [塞尔柱语](../Page/塞尔柱语.md "wikilink")（伊朗）
          - [克里米亚鞑靼语](../Page/克里米亚鞑靼语.md "wikilink")（[克里米亞](../Page/克里米亞.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、乌兹别克斯坦）
          - [撒拉语](../Page/撒拉语.md "wikilink")（中国青海循化）
          - [加告兹语](../Page/加告兹语.md "wikilink")（摩尔多瓦）
          - [呼罗珊突厥语](../Page/呼罗珊突厥语.md "wikilink")（伊朗）
          - [乌鲁姆语](../Page/乌鲁姆语.md "wikilink")（格鲁吉亚）
          - [伊犁土尔克语](../Page/伊犁土尔克语.md "wikilink")（中国新疆[伊犁](../Page/伊犁.md "wikilink")）
      - [西伯利亚语组](../Page/西伯利亚语组.md "wikilink")
          - [阿尔泰语](../Page/阿尔泰语.md "wikilink")（阿尔泰）
          - [索尔语](../Page/索尔语.md "wikilink")（阿尔泰）
          - [卡拉嘎斯语](../Page/卡拉嘎斯语.md "wikilink")（伊尔库茨克）
          - [哈卡斯语](../Page/哈卡斯语.md "wikilink")（哈卡斯）
          - [雅库特语](../Page/雅库特语.md "wikilink")（萨哈）
          - [图瓦语](../Page/图瓦语.md "wikilink")（图瓦）
          - [多尔干语](../Page/多尔干语.md "wikilink")（多尔干）
          - [富裕柯爾克孜語](../Page/富裕柯爾克孜語.md "wikilink") (中國黑龍江)
          - [楚利姆语](../Page/楚利姆语.md "wikilink")（楚利姆河）
          - [西部裕固语](../Page/西部裕固语.md "wikilink")（中国甘肃）
      - [保加尔语组](../Page/保加尔语组.md "wikilink")
          - [楚瓦什语](../Page/楚瓦什语.md "wikilink")（俄罗斯楚瓦什）

## 与其他语言关系

突厥语族的各个语言在不同时期都受到周边民族语言的影响，上古时期受[中古波斯语](../Page/中古波斯语.md "wikilink")、[东胡语和](../Page/东胡语.md "wikilink")[粟特语](../Page/粟特语.md "wikilink")、[梵语](../Page/梵语.md "wikilink")、[吐火罗语](../Page/吐火罗语.md "wikilink")、[塞种语影响比较大](../Page/塞种语.md "wikilink")，中古时期受[波斯语](../Page/波斯语.md "wikilink")、[阿拉伯语](../Page/阿拉伯语.md "wikilink")、[汉语](../Page/汉语.md "wikilink")、[蒙古语](../Page/蒙古语.md "wikilink")、[希腊语](../Page/希腊语.md "wikilink")、[意大利语影响比较大](../Page/意大利语.md "wikilink")，近现代时期受[法语](../Page/法语.md "wikilink")、[俄语](../Page/俄语.md "wikilink")、[英语](../Page/英语.md "wikilink")、[汉语影响比较大](../Page/汉语.md "wikilink")。同时，突厥语族诸语亦对其他民族的语言产生影响，如[中古汉语](../Page/中古汉语.md "wikilink")、[蒙古语](../Page/蒙古语.md "wikilink")、[塔吉克语](../Page/塔吉克语.md "wikilink")（[波斯语](../Page/波斯语.md "wikilink")）、埃及[阿拉伯语](../Page/阿拉伯语.md "wikilink")、[库尔德语等](../Page/库尔德语.md "wikilink")。

## 参考文献

### 引用

### 来源

  - 网页

<!-- end list -->

  - [Ethnologue:Turkic
    Langauges](http://www.ethnologue.com/show_family.asp?subid=90010)
  - [Turkish-Chinese Translators in China
    中国的突厥语-汉语翻译](https://web.archive.org/web/20070622183721/http://www.yeminlitercuman.com/dunya/)

## 外部連結

  - [青城驿站：蒙古文化论坛：阿尔泰语系民族、分布、人口](http://www.qingis.com/bbs/ShowAnnounce.asp?boardID=2&RootID=8039&ID=8039)

## 参见

  - [古突厥语](../Page/古突厥语.md "wikilink")
  - [突厥人](../Page/突厥人.md "wikilink")
  - [察合台语](../Page/察合台语.md "wikilink")
  - [中國語言列表\#突厥語族](../Page/中國語言列表#突厥語族.md "wikilink")

{{-}}

[突厥語族](../Category/突厥語族.md "wikilink")
[Category:阿爾泰語系](../Category/阿爾泰語系.md "wikilink")
[Category:突厥](../Category/突厥.md "wikilink")

1.  [Turkic
    Peoples](http://www.joshuaproject.net/affinity-blocs.php?rop1=A015)
2.  [Turkic Language family
    tree](http://www.ethnologue.com/show_family.asp?subid=90010) entries
    provide the information on the Turkic-speaking populations and
    regions.