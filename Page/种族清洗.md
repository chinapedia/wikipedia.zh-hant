**種族清洗**（英語：**Ethnic
cleansing**），又稱**民族清洗**，通常指的是某個[國家或某個](../Page/國家.md "wikilink")[地區的強勢集團](../Page/地區.md "wikilink")，為了自己的[政治目的](../Page/政治.md "wikilink")、[經濟目的或者](../Page/經濟.md "wikilink")[宗教目的而動用](../Page/宗教.md "wikilink")[軍隊](../Page/軍隊.md "wikilink")、[警察或者非法組織成員](../Page/警察.md "wikilink")，對特定的一個或者若干個[民族的所有成員實施的無差別](../Page/民族.md "wikilink")[屠殺或](../Page/屠殺.md "wikilink")[強制遷徙的活動](../Page/強制遷徙.md "wikilink")。例如[南斯拉夫戰爭](../Page/南斯拉夫戰爭.md "wikilink")（1991─1999年）期間在[波士尼亞與](../Page/波士尼亞.md "wikilink")[科索沃的](../Page/科索沃.md "wikilink")[種族屠殺](../Page/種族屠殺.md "wikilink")。

## 曾遭到刻意清洗的民族/族群

### 近代

#### 歐洲

  - [西班牙的](../Page/西班牙.md "wikilink")[摩尔人](../Page/摩尔人.md "wikilink")
  - [犹太人](../Page/犹太人.md "wikilink")
  - [亚美尼亚人](../Page/亚美尼亚人.md "wikilink")
  - [罗姆人](../Page/罗姆人.md "wikilink")（“[吞灭](../Page/吞灭.md "wikilink")”行动）
  - [波兰人](../Page/波兰人.md "wikilink")
  - [乌克兰人](../Page/乌克兰人.md "wikilink")（[乌克兰大饥荒](../Page/乌克兰大饥荒.md "wikilink")，是否刻意针对乌克兰民族有争议）
  - 东欧的[德意志人](../Page/德意志人.md "wikilink")
  - [色雷斯和](../Page/色雷斯.md "wikilink")[小亚细亚的](../Page/小亚细亚.md "wikilink")[希腊人](../Page/希腊人.md "wikilink")
  - 希腊和[保加利亚的](../Page/保加利亚.md "wikilink")[土耳其人](../Page/土耳其人.md "wikilink")
  - [波斯尼亚人](../Page/波斯尼亚人.md "wikilink")
  - [塞尔维亚人](../Page/塞尔维亚人.md "wikilink")

#### 亞洲

[Kutupalong_Refugee_Camp_(John_Owens-VOA).jpg](https://zh.wikipedia.org/wiki/File:Kutupalong_Refugee_Camp_\(John_Owens-VOA\).jpg "fig:Kutupalong_Refugee_Camp_(John_Owens-VOA).jpg")
\]\]

  - [不丹的](../Page/不丹.md "wikilink")[洛昌人](../Page/洛昌人.md "wikilink")
  - [印尼的华人](../Page/印尼.md "wikilink")（[九三零事件](../Page/九三零事件.md "wikilink")）
  - [緬甸的](../Page/緬甸.md "wikilink")[羅興亞人](../Page/羅興亞人.md "wikilink")
  - [孟加拉国的](../Page/孟加拉.md "wikilink")[朱瑪人](../Page/朱瑪人.md "wikilink")（[吉大港山區衝突](../Page/吉大港山區衝突.md "wikilink")）
  - [柬埔寨的](../Page/柬埔寨.md "wikilink")[越南人](../Page/越南.md "wikilink")、[占人和华人](../Page/占人.md "wikilink")（[柬埔寨種族滅絕](../Page/柬埔寨種族滅絕.md "wikilink")，發生於[红色高棉时期](../Page/红色高棉.md "wikilink")）
  - [阿拉伯之冬中](../Page/阿拉伯之冬.md "wikilink")[伊斯兰国](../Page/伊斯兰国.md "wikilink")，针对除[逊尼派外](../Page/逊尼派.md "wikilink")，包括[阿拉伯基督徒](../Page/阿拉伯基督徒.md "wikilink")、[雅兹迪人](../Page/雅兹迪人.md "wikilink")、[什叶派在内的所有教派](../Page/什叶派.md "wikilink")。
  - [阿富汗的](../Page/阿富汗.md "wikilink")[哈扎拉族](../Page/哈扎拉族.md "wikilink")。

#### 非洲

  - [卢旺达的](../Page/卢旺达.md "wikilink")[图西族](../Page/图西族.md "wikilink")
  - [阿尔及利亚和](../Page/阿尔及利亚.md "wikilink")[突尼斯的欧洲人](../Page/突尼斯.md "wikilink")（“[黑脚](../Page/黑脚.md "wikilink")”）和犹太人
  - [尼日尔的马哈米德族](../Page/尼日尔.md "wikilink")[阿拉伯人](../Page/阿拉伯人.md "wikilink")
  - [桑给巴尔的阿拉伯人和](../Page/桑给巴尔.md "wikilink")[印度人](../Page/印度.md "wikilink")
  - [烏干達的印度人](../Page/烏干達.md "wikilink")
  - [蘇丹](../Page/蘇丹.md "wikilink")[達爾富爾地區的黑人](../Page/達爾富爾.md "wikilink")

#### 美洲

  - 北美[拉科塔人](../Page/拉科塔.md "wikilink")（[伤膝河大屠杀](../Page/伤膝河大屠杀.md "wikilink")）

### 古代

  - [苗人](../Page/苗人.md "wikilink")：[明英宗](../Page/明英宗.md "wikilink")[天顺年间](../Page/天顺.md "wikilink")，明政府为了在[湘西取得足够的土地安置汉族流民](../Page/湘西.md "wikilink")，对湘西的苗族人实施了种族清洗，据《[凤凰厅志](../Page/凤凰县.md "wikilink")》和《[泸溪县志](../Page/泸溪县.md "wikilink")》载：苗区人口“大经草剃，存不满百”、“几经绝种”、“经过挞伐征剿，村寨十室九空，人迹灭绝”。[郭子章](../Page/郭子章.md "wikilink")《黔记》卷五十九则记载，明代[万历年间平定](../Page/万历.md "wikilink")[播州土司](../Page/播州.md "wikilink")[杨应龙叛乱后](../Page/杨应龙.md "wikilink")，[贵州的明军曾发布赏格](../Page/贵州.md "wikilink")，凡生擒苗人一名赏银五两，杀一苗人赏银三两。
  - [切尔克斯人](../Page/切尔克斯人.md "wikilink")：[俄羅斯帝國在佔領](../Page/俄羅斯帝國.md "wikilink")[切爾卡斯亞等](../Page/切爾卡斯亞.md "wikilink")[高加索國家後](../Page/高加索.md "wikilink")，便屠殺和驅逐了約40-50萬北高加索人([切爾克斯人](../Page/切爾克斯人.md "wikilink"))。
  - [百越人](../Page/百越.md "wikilink")：[秦平百越](../Page/秦平百越.md "wikilink")
  - [台湾原住民](../Page/台湾原住民.md "wikilink")
  - 中国[新疆的](../Page/新疆.md "wikilink")[准噶尔部蒙古人](../Page/准噶尔部.md "wikilink")（[準噶爾滅族](../Page/準噶爾滅族.md "wikilink")，[魏源的](../Page/魏源.md "wikilink")《圣武记》有记载）

## 参见

  - [種族滅絕](../Page/種族滅絕.md "wikilink")
  - [恐怖組織](../Page/恐怖組織.md "wikilink")

[種族清洗](../Category/種族清洗.md "wikilink")
[Category:战争犯罪](../Category/战争犯罪.md "wikilink")
[-](../Category/种族.md "wikilink")
[Category:屠殺](../Category/屠殺.md "wikilink")