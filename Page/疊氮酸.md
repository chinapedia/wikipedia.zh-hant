**疊氮酸**，分子式[H](../Page/氫.md "wikilink")[N<sub>3</sub>](../Page/氮.md "wikilink")，在常溫常壓下為一種無色、具揮發性、有刺激臭、高[爆炸性的液體](../Page/爆炸.md "wikilink")。

## 應用

疊氮酸主要用於保存[貯存溶液](../Page/貯存溶液.md "wikilink")（stock
solution），以及作為一種[試劑](../Page/試劑.md "wikilink")。

## 歷史

疊氮酸在1890年首先由[Theodor
Curtius單離出來](../Page/:en:Theodor_Curtius.md "wikilink")。(*Berichte*,
1890, 23, p. 3023).

## 化學性質

可溶於水，且其溶液可溶解許多金屬（如[鋅](../Page/鋅.md "wikilink")、[鐵](../Page/鐵.md "wikilink")），放出[氫氣並形成](../Page/氫.md "wikilink")[疊氮化物的鹽類](../Page/疊氮化物.md "wikilink")。

其所有的鹽類皆具爆炸性，並極易與[碘代烃反應](../Page/卤代烷.md "wikilink")。疊氮酸的某些性質類似氫鹵酸，如與[鉛](../Page/鉛.md "wikilink")、[銀](../Page/銀.md "wikilink")、亞[汞离子形成難溶於水的鹽類](../Page/汞.md "wikilink")。金屬鹽在無水形式下皆可形成結晶，並受熱分解，產生純金屬的殘渣。疊氮酸是一種[弱酸](../Page/弱酸.md "wikilink")（pKa
4.6-4.7）。

## 製造

疊氮酸通常由疊氮化物，如[疊氮化鈉等鹽類酸化產生](../Page/疊氮化鈉.md "wikilink")。一般而言，疊氮化鈉的水溶液含有極少量疊氮酸，與疊氮酸鹽形成平衡，加入強酸後則大部分轉化為疊氮酸。再由[分餾法即可製得純品](../Page/蒸餾.md "wikilink")。

## 毒性

疊氮酸具揮發性及劇毒，毒性接近[氰化氫](../Page/氰化氫.md "wikilink")，會對皮膚與黏膜造成嚴重刺激。其具有嗆鼻的氣味，蒸氣能引發強烈的頭痛。此化合物為非累積性的毒物。

## 參考資料

1.  Dictionary of inorganic and organometallic compounds, Chapman & Hall

## 外部連結

  - [OSHA:
    疊氮酸（英文）](http://www.osha.gov/dts/chemicalsampling/data/CH_245950.html)

[Category:疊氮化合物](../Category/疊氮化合物.md "wikilink")
[疊](../Category/爆炸物.md "wikilink")
[Category:无机酸](../Category/无机酸.md "wikilink")