在[电视剧或为](../Page/电视剧.md "wikilink")[电视制作的电影中](../Page/电视.md "wikilink")，涉及[同性恋](../Page/同性恋.md "wikilink")、[雙性戀與](../Page/雙性戀.md "wikilink")／或[跨性別话题以及](../Page/跨性別.md "wikilink")/或塑造重要同志[角色和](../Page/角色.md "wikilink")／或把同性恋、雙性戀以及／或跨性別身份或关系作为一个重要的剧情的有（依開播年代排序）：

## 1930年代

  - 《[指路明燈](../Page/指路明燈.md "wikilink")》（*Guiding
    Light*），美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1937–2009）

## 1950年代

  - *As the World
    Turns*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1956–2010）

## 1960年代

  - 《[加冕街](../Page/加冕街.md "wikilink")》（*Coronation
    Street*），英国[ITV](../Page/獨立電視台.md "wikilink")（1960–）
  - *General Hospital*，美国[ABC](../Page/美国广播公司.md "wikilink")（1963–）
  - 《[我們的日子](../Page/我們的日子.md "wikilink")》（*Days of our
    Lives*），美国[NBC](../Page/全国广播公司.md "wikilink")（1965–）
  - *One Life to Live*，美国[ABC](../Page/美国广播公司.md "wikilink")（1968–2012）

## 1970年代

  - *All My Children*，美国[ABC](../Page/美国广播公司.md "wikilink")（1970-2011）
  - *Are you being served?*，英国[BBC1](../Page/BBC1.md "wikilink")（1972）
  - *The Corner Bar*，美国[ABC](../Page/美国广播公司.md "wikilink")（1972–1973）
  - *Number 96*，美国[Network
    Ten](../Page/Network_Ten.md "wikilink")（1972–1977）
  - *Emmerdale*，美国[ITV](../Page/獨立電視台.md "wikilink")（1972–）
  - *The Young and the
    Restless*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1973–）
  - *The Box*，美国[Network
    Ten](../Page/Network_Ten.md "wikilink")（1974–1977）
  - *Hot l Baltimore*，美国[ABC](../Page/美国广播公司.md "wikilink")（1975）
  - *Barney Miller*，美国[ABC](../Page/美国广播公司.md "wikilink")（1975–1982）
  - *Snip*，美国[NBC](../Page/全国广播公司.md "wikilink")（1976）
  - *The Nancy Walker Show*，美国[ABC](../Page/美国广播公司.md "wikilink")（1976）
  - *Ball Four*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1976）
  - *All That Glitters*，美国（1977）
  - *Soap*，美国[ABC](../Page/美国广播公司.md "wikilink")（1978–1981）
  - *Agony*，英国[ITV](../Page/獨立電視台.md "wikilink")（1978–1981）
  - 《[指](../Page/指_\(松本清張\).md "wikilink")》，日本[TBS](../Page/TBS電視台.md "wikilink")，[NTV](../Page/日本電視台.md "wikilink")（1979，1984，2006）

## 1980年代

  - 《[故園風雨後](../Page/故園風雨後.md "wikilink")》（*Brideshead
    revisited*），英国[Granada小型电视剧](../Page/Granada.md "wikilink")（1981）
  - 《[濕濡的心](../Page/濕濡的心.md "wikilink")》（），日本[ABC](../Page/朝日放送.md "wikilink")（1981）
  - *Love, Sidney*，美国[NBC](../Page/全国广播公司.md "wikilink")（1981–1983）
  - 《[希尔街的布鲁斯](../Page/希尔街的布鲁斯.md "wikilink")》（*Hill Street
    Blues*），美国[NBC](../Page/全国广播公司.md "wikilink")（1981–1987）
  - 《[豪門恩怨](../Page/豪門恩怨.md "wikilink")》*Dynasty*，美国[ABC](../Page/美国广播公司.md "wikilink")（1981–1989）
  - *Brookside*，英国[Channel
    4](../Page/Channel_4.md "wikilink")（1982–2003）
  - *One Summer*，英国[Channel 4](../Page/Channel_4.md "wikilink")（1983）
  - *Celebrity*，美国[NBC](../Page/全国广播公司.md "wikilink")（1984）
  - *Dream Stuffing*，英国[Channel
    4](../Page/第四台_\(英國\).md "wikilink")（1984）
  - 《[魔性](../Page/魔性.md "wikilink")》（），日本（1984）
  - 《[紅](../Page/紅_\(電視劇\).md "wikilink")》（），日本[MBS](../Page/每日放送.md "wikilink")，[YTV](../Page/讀賣電視台.md "wikilink")（1984，1988）
  - *Brothers*，美国[Showtime](../Page/Showtime.md "wikilink")（1984–1989）
  - *Santa Barbara*，美国[NBC](../Page/全国广播公司.md "wikilink")（1984–1993）
  - *CBS Schoolbreak Special－What If I'm
    Gay?*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1984–1996）
  - *Hollywood Beat*，美国[NBC](../Page/全国广播公司.md "wikilink")（1984–1996）
  - *Sara*，美国[Showtime](../Page/Showtime.md "wikilink")（1985）
  - 《[東區人](../Page/東區人.md "wikilink")》（*EastEnders*），英国[BBC1](../Page/BBC1.md "wikilink")（1985–）
  - *Lindenstraße*，德国[ARD](../Page/德國公共廣播聯盟.md "wikilink")（1985–）
  - *Neighbours*，澳大利亞Eleven（1985–）
  - *Dress Gray*，美国[NBC](../Page/全国广播公司.md "wikilink")（1986）
  - 《[洛城法网](../Page/洛城法网.md "wikilink")》（*L.A.
    Law*），美国[NBC](../Page/全国广播公司.md "wikilink")（1986–1994）
  - *Women in Prison*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（1987–1988）
  - *Hooperman*，美国[ABC](../Page/美国广播公司.md "wikilink")（1987–1989）
  - *Thirtysomething*，美国[ABC](../Page/美国广播公司.md "wikilink")（1987–1991）
  - *The Bold and the
    Beautiful*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1987–）
  - 《[肥媽日記](../Page/肥媽日記.md "wikilink")》（*Roseanne*），美国（1988-1997）
  - *Home and Away*，美国[Seven
    Network](../Page/七號電視網.md "wikilink")（1988–）
  - *HeartBeat*，美国[ABC](../Page/美国广播公司.md "wikilink")（1988–1989）
  - *Doctor
    Doctor*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1989–1991）
  - *Byker Grove*，英国[BBC](../Page/BBC.md "wikilink")（1989-2006）
  - *Fair City*，愛爾蘭[RTÉ](../Page/愛爾蘭廣播電視.md "wikilink")（1989–）

## 1990年代

  - *Oranges Are Not the Only
    Fruit*，英国[BBC](../Page/BBC.md "wikilink")（1990）
  - *Portrait of a Marriage*，英国[BBC
    Two](../Page/BBC_Two.md "wikilink")（1990）
  - *Drop the Dead Donkey*，英国[Channel
    4](../Page/第四台_\(英國\).md "wikilink")（1990–1998）
  - 《[法律与秩序](../Page/法律与秩序.md "wikilink")》（*Law &
    Order*），美国[NBC](../Page/全国广播公司.md "wikilink")（1990–2010）
  - 《[豪門](../Page/豪門_\(電視劇\).md "wikilink")》，香港[亞洲](../Page/亞洲電視.md "wikilink")（1991）
  - *The Brittas Empire*，英国[BBC](../Page/BBC.md "wikilink")（1991–1997）
  - 《[約束之夏](../Page/約束之夏.md "wikilink")》（），日本[THK](../Page/THK.md "wikilink")（1992）
  - 《[是谁偷走我的心](../Page/是谁偷走我的心.md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（1992）
  - *The Larry Sanders
    Show*，美国[HBO](../Page/HBO.md "wikilink")（1992–1998）
  - *Melrose
    Place*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（1992–1999，2009–2010）
  - 《[城市故事](../Page/城市故事_\(美國電視短劇\).md "wikilink")》（*Tales of the
    City*），美国[PBS美国小型电视剧](../Page/公共電視網_\(美國\).md "wikilink")（1993）
  - *Cutters*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1993）
  - *Wild Palms*，美国[ABC](../Page/美国广播公司.md "wikilink")（1993）
  - 《[愛情白皮書](../Page/愛情白皮書.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（1993）
  - 《[惡魔之吻](../Page/惡魔之吻.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（1993）
  - 《[同窗會](../Page/同窗會_\(電視劇\).md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（1993）
  - *The John Larroquette
    Show*，美国[NBC](../Page/全国广播公司.md "wikilink")（1993–1996）
  - *Sisters*，美国[NBC](../Page/全国广播公司.md "wikilink")（1993–1996）
  - 《[紐約重案組](../Page/紐約重案組.md "wikilink")》*NYPD
    Blue*，美国[ABC](../Page/美国广播公司.md "wikilink")（1993–2005）
  - *Daddy's Girls*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1994）
  - 《[人間·失格～假如我死的話](../Page/人間·失格～假如我死的話.md "wikilink")》（），日本[TBS](../Page/TBS電視台.md "wikilink")（1994）
  - 《[青春組曲](../Page/青春組曲.md "wikilink")》*My So-Called
    Life*，美国[ABC](../Page/美国广播公司.md "wikilink")（1994–1995）
  - *Ellen*，美国（1994-1998）
  - *The Vicar of Dibley*，美国（1994–1999）
  - *Party of Five*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（1994–2000）
  - 《[老友记](../Page/老友记.md "wikilink")》（*Friends*），美国[NBC](../Page/全国广播公司.md "wikilink")（1994–2004）
  - 《[急诊室的故事](../Page/急诊室的故事.md "wikilink")》（*ER*），美国[NBC](../Page/全国广播公司.md "wikilink")（1994–2009）
  - *Agony Again*，英国[BBC](../Page/BBC.md "wikilink")（1995）
  - *The Pursuit of
    Happiness*，美国[NBC](../Page/全国广播公司.md "wikilink")（1995）
  - *Courthouse*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1995）
  - *Muscle*，美国[The WB](../Page/WB電視網.md "wikilink")（1995）
  - 《[皇甫少華與孟麗君](../Page/皇甫少華與孟麗君.md "wikilink")》，台灣[華視](../Page/華視.md "wikilink")（1995）
  - *Live Shot*，美国[UPN](../Page/UPN.md "wikilink")（1995–1996）
  - *The Crew*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（1995–1996）
  - *High Society*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1995–1996）
  - *Murder One*，美国[ABC](../Page/美国广播公司.md "wikilink")（1995–1997）
  - 《[被禁止的爱](../Page/被禁止的爱.md "wikilink")》（*Verbotene Liebe*），德国[Das
    erste](../Page/Das_erste.md "wikilink")（1995-）
  - *Lush Life*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（1996）
  - *Party Girl*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（1996）
  - *Public Morals*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1996）
  - *Matt Waters*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（1996）
  - 《[幸福的預感](../Page/幸福的預感.md "wikilink")》（），日本[THK](../Page/THK.md "wikilink")（1996）
  - 《[變](../Page/變_\(漫畫\).md "wikilink")》（），日本[EX](../Page/朝日電視台.md "wikilink")（1996）
  - *Relativity*，美国[ABC](../Page/美国广播公司.md "wikilink")（1996–1997）
  - *Profiler*，美国[NBC](../Page/全国广播公司.md "wikilink")（1996–2000）
  - 《[政界小人物](../Page/政界小人物.md "wikilink")》（*Spin
    City*），美国[ABC](../Page/美国广播公司.md "wikilink")（1996–2002）
  - 《[告白](../Page/告白_\(1997年電視劇\).md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（1997）
  - *Fired Up*，美国[NBC](../Page/全国广播公司.md "wikilink")（1997-1998）
  - *Veronica's Closet*，美国[NBC](../Page/全国广播公司.md "wikilink")（1997-2000）
  - 《[监狱风云](../Page/监狱风云_\(美国电视剧\).md "wikilink")》（*Oz*），美国[HBO](../Page/HBO.md "wikilink")（1997–2003）
  - 《[吸血鬼猎人巴菲](../Page/吸血鬼猎人巴菲.md "wikilink")》（*Buffy the Vampire
    Slayer*），美国[WB](../Page/WB電視網.md "wikilink")、[UPN](../Page/UPN.md "wikilink")（1997–2003）
  - 《[慾望城市](../Page/慾望城市.md "wikilink")》（*Sex and the
    City*），美国[HBO](../Page/HBO.md "wikilink")（1997–2004）
  - *More Tales of the
    City*，美国[Showtime](../Page/Showtime.md "wikilink")、英国[Channel
    4](../Page/第四台_\(英國\).md "wikilink")（1998）
  - 《[花木蘭 (無綫電視劇)](../Page/花木蘭_\(無綫電視劇\).md "wikilink")》，香港（1998）
  - 《[穆桂英挂帥
    (1998年電視劇)](../Page/穆桂英挂帥_\(1998年電視劇\).md "wikilink")》，中國（1998）
  - 《[還珠格格](../Page/還珠格格.md "wikilink")》，台灣中視/中國[湖南經視](../Page/湖南經視頻道.md "wikilink")（1998）
  - 《[恋爱时代](../Page/恋爱时代_\(美国电视剧\).md "wikilink")》*Dawson's
    Creek*，美国[WB](../Page/WB電視網.md "wikilink")（1998–2003）
  - 《[大学生费莉希蒂](../Page/大学生费莉希蒂.md "wikilink")》（*Felicity*），美国[WB](../Page/WB電視網.md "wikilink")（1998–2002）
  - 《[威尔与格蕾丝](../Page/威尔与格蕾丝.md "wikilink")》（*Will &
    Grace*），美国[NBC](../Page/全国广播公司.md "wikilink")（1998-2006）
  - *All Saints*，澳大利亚[Seven
    Network](../Page/Seven_Network.md "wikilink")（1998–2009）
  - *Oh, Grow Up*，美国[ABC](../Page/美国广播公司.md "wikilink")（1999）
  - *Wasteland*，美国[ABC](../Page/美国广播公司.md "wikilink")（1999）
  - *What Makes a Family*，美国（1999）
  - 《[花木蘭
    (1999年電視劇)](../Page/花木蘭_\(1999年電視劇\).md "wikilink")》，台灣中視（1999）
  - 《[悲傷誘惑](../Page/悲傷誘惑.md "wikilink")》（），韩国[KBS](../Page/KBS.md "wikilink")（1999）
  - 《[羅曼史](../Page/羅曼史_\(1999年電視劇\).md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（1999）
  - *Strangers with Candy*，美国[Comedy
    Central](../Page/喜劇中心.md "wikilink")（1999–2000）
  - 《[同志亦凡人 (英版)](../Page/同志亦凡人_\(英國電視劇\).md "wikilink")》（*Queer as folk
    (British version)*），英国[Channel
    4](../Page/第四台_\(英國\).md "wikilink")（1999–2000）
  - 《[七世夫妻之梁山伯與祝英台](../Page/七世夫妻之梁山伯與祝英台.md "wikilink")》，台灣民視（1999–2000）
  - *Beggars and
    Choosers*，美国[Showtime](../Page/Showtime.md "wikilink")（1999–2001）
  - *Los
    Beltrán*，美国[Telemundo](../Page/Telemundo.md "wikilink")（1999–2001）
  - *Once and Again*，美国[ABC](../Page/美国广播公司.md "wikilink")（1999–2002）
  - *Bad Girls*，英国[ITV](../Page/獨立電視台.md "wikilink")（1999–2006）
  - 《[黑道家族](../Page/黑道家族.md "wikilink")》（*The
    Sopranos*），美国[HBO](../Page/HBO.md "wikilink")（1999–2007）
  - 《[法律与秩序：特殊受害者](../Page/法律与秩序：特殊受害者.md "wikilink")》（*Law & Order:
    Special Victims Unit*），美国[NBC](../Page/全国广播公司.md "wikilink")（1999–）

## 2000年代

  - *Frank Herbert's Dune*，美国[Sci Fi
    Channel](../Page/Syfy.md "wikilink")（2000）
  - *Normal, Ohio*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（2000）
  - 《[29歲的憂鬱](../Page/29歲的憂鬱.md "wikilink")》（），日本[EX](../Page/朝日電視台.md "wikilink")（2000）
  - 《[戀愛症候群](../Page/戀愛症候群.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2000）
  - *Grosse Pointe*，美国[WB](../Page/WB電視網.md "wikilink")（2000–2001）
  - *My Family*，英國[BBC1](../Page/英國廣播公司第一台.md "wikilink")（2000–2011）
  - 《[末世黑天使](../Page/末世黑天使.md "wikilink")》（*Dark
    Angel*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2000–2002）
  - 《[同志亦凡人 (美版)](../Page/同志亦凡人_\(美國電視劇\).md "wikilink")》Queer as folk
    (American version),
    美国[Showtime](../Page/Showtime.md "wikilink")（2000–2005）
  - *Some of My Best
    Friends*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2001）
  - 《[逆女](../Page/逆女.md "wikilink")》，台灣台視（2001）
  - 《[穆桂英挂帥 (2001年電視劇)](../Page/穆桂英挂帥_\(2001年電視劇\).md "wikilink")》，中國
  - 《[R-17](../Page/R-17.md "wikilink")》，日本[NTV](../Page/日本電視台.md "wikilink")（2001）
  - 《[Red](../Page/Red_\(電視劇\).md "wikilink")》（），日本[THK](../Page/THK.md "wikilink")（2001）
  - 《[西洋骨董洋菓子店](../Page/西洋骨董洋菓子店_\(電視劇\).md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2001）
  - *Metrosexuality*，英国[Channel
    4](../Page/第四台_\(英國\).md "wikilink")（2001）
  - *Further Tales of the
    City*，美国[Showtime](../Page/Showtime.md "wikilink")、英国[Channel
    4](../Page/第四台_\(英國\).md "wikilink")（2001）
  - *Dice*，加拿大/英国[The Movie
    Network](../Page/The_Movie_Network.md "wikilink")（2001）
  - *100 Centre
    Street*，美国[A\&E](../Page/A+E電視網.md "wikilink")（2001-2002）
  - *Leap Years*，美国[Showtime](../Page/Showtime.md "wikilink")（2001-2002）
  - *The Education of Max
    Bickford*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2001-2002）
  - *The Ellen
    Show*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2001-2002）
  - 《[24](../Page/24_\(电视剧\).md "wikilink")》（*24*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2001–2005）
  - 《[身前身後](../Page/身前身後.md "wikilink")》（*Six feet
    under*），美国[HBO](../Page/HBO.md "wikilink")（2001–2005）
  - *Degrassi: The Next
    Generation*，加拿大[CTV](../Page/CTV電視網.md "wikilink")（2001–）
  - 《》（*Tipping the Velvet*），英国[BBC
    Two](../Page/BBC_Two.md "wikilink")（2002）
  - 《[火线](../Page/火线_\(电视剧\).md "wikilink")》（*The
    Wire*），美国[HBO](../Page/HBO.md "wikilink")（2002）
  - *That '80s Show*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2002）
  - 《[再生緣](../Page/再生緣_\(2002年電視劇\).md "wikilink")》，香港無綫（2002）
  - 《[鋼鐵天使](../Page/鋼鐵天使.md "wikilink")》（），日本[EX](../Page/朝日電視台.md "wikilink")（2002）
  - *Stingers*，澳大利亚[Nine
    Network](../Page/Nine_Network.md "wikilink")（2002–2004）
  - *Everwood*，美国[WB](../Page/WB電視網.md "wikilink")（2002–2006）
  - *Kath & Kim*，美国[ABC1](../Page/美国广播公司.md "wikilink")、[Seven
    Network](../Page/七號電視網.md "wikilink")（2002–2007）
  - 《[盾牌](../Page/盾牌_\(电视剧\).md "wikilink")》（*The
    Shield*），美国[FX](../Page/FX_\(電視頻道\).md "wikilink")（2002–2008）
  - 《[天使在美国](../Page/天使在美国_\(连续短剧\).md "wikilink")》（*Angels in
    America*），美国[HBO](../Page/HBO.md "wikilink")（2003）
  - 《[孽子](../Page/孽子_\(電視劇\).md "wikilink")》，台灣公視（2003）
  - 《[大望](../Page/大望.md "wikilink")》（），韩国[SBS](../Page/SBS株式會社.md "wikilink")（2003）
  - 《[五個撲水的少年](../Page/五個撲水的少年.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2003）
  - *It's All Relative*，美国[ABC](../Page/美国广播公司.md "wikilink")（2003–2004）
  - 《[奇幻嘉年华](../Page/奇幻嘉年华.md "wikilink")》（*Carnivàle*），美国[HBO](../Page/HBO.md "wikilink")（2003–2005）
  - 《[小不列顛](../Page/小不列顛.md "wikilink")》（*Little Britain*），英國（2003–2006）
  - 《[發展受阻](../Page/發展受阻.md "wikilink")》（*Arrested
    Development*），美國[Fox](../Page/福斯国际电视网.md "wikilink")、[Netflix](../Page/Netflix.md "wikilink")（2003–2006，2013–）
  - *Aquí no hay quien viva*， 西班牙Antena 3（2003–2006）
  - 《[橘郡风云](../Page/橘郡风云.md "wikilink")》（*The
    O.C.*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2003–2007）
  - *Reno 911\!*，美国[喜劇中心](../Page/喜劇中心.md "wikilink")（2003–2009）
  - 《[整容室](../Page/整容室.md "wikilink")》（*Nip/Tuck*），美国[FX](../Page/FX_\(電視頻道\).md "wikilink")（2003–2010）
  - 《[篮球兄弟](../Page/篮球兄弟.md "wikilink")》（*One Tree
    Hill*），美国[WB](../Page/WB電視網.md "wikilink")（2003–2011）
  - 《[好汉两个半](../Page/好汉两个半.md "wikilink")》（*Two and a Half
    Men*），美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2003–）
  - *Jack & Bobby*，美国[WB](../Page/WB電視網.md "wikilink")（2004）
  - *Wonderfalls*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（2004）
  - 《[牡丹與薔薇](../Page/牡丹與薔薇.md "wikilink")》（），日本[THK](../Page/THK.md "wikilink")（2004）
  - *Hex*，澳大利亚Arena/英国Sky One（2004-2005）
  - 《[美眉校探](../Page/美眉校探.md "wikilink")》（*Veronica
    Mars*），美国[UPN](../Page/UPN.md "wikilink")（2004–2007）
  - 《[迷失](../Page/迷失.md "wikilink")》（*Lost*），美国[ABC](../Page/美国广播公司.md "wikilink")（2004–2009）
  - 《[拉字至上](../Page/拉字至上.md "wikilink")》（*The L
    Word*），美国[Showtime](../Page/Showtime.md "wikilink")（2004–2009）
  - 《[太空堡垒卡拉狄加](../Page/太空堡垒卡拉狄加.md "wikilink")》（*Battlestar
    Galactica*），美国[Sci Fi
    Channel](../Page/Syfy.md "wikilink")（2004–2009）
  - 《[我家也有大明星](../Page/我家也有大明星.md "wikilink")》（*Entourage*），美国[HBO](../Page/HBO.md "wikilink")（2004–2011）
  - *Rescue Me*，美国[FX](../Page/FX_\(電視頻道\).md "wikilink")（2004-2011）
  - 《[绝望的主妇](../Page/绝望的主妇.md "wikilink")》（*Desperate
    Housewives*），美国[ABC](../Page/美国广播公司.md "wikilink")（2004–2012）
  - 《[无耻之徒](../Page/无耻之徒.md "wikilink")》（*Shameless*），英国[Channel
    4](../Page/第四台_\(英國\).md "wikilink")（2004–）
  - 《[豪斯医生](../Page/豪斯医生.md "wikilink")》（*House*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2004–2012）
  - *Eyes*，美国 [ABC](../Page/美国广播公司.md "wikilink")（2005）
  - *Inconceivable*，美国[NBC](../Page/全国广播公司.md "wikilink")（2005）
  - *The Book of Daniel*，美国[NBC](../Page/全国广播公司.md "wikilink")（2005）
  - *Floored by Love*，加拿大（2005）
  - 《[指匠情挑](../Page/指匠情挑_\(电视剧\).md "wikilink")》（*Fingersmith*），英国[BBC](../Page/BBC.md "wikilink")（2005）
  - 《[三军统帅](../Page/三军统帅.md "wikilink")》（*Commander in
    Chief*），美国[ABC](../Page/美国广播公司.md "wikilink")（2005）
  - *It's
    Me...Gerald*，加拿大[Showcase](../Page/Showcase_\(电视频道\).md "wikilink")（2005）
  - *The Comeback*，美国[HBO](../Page/HBO.md "wikilink")（2005）
  - *All About George*，英国[ITV](../Page/獨立電視台.md "wikilink")（2005）
  - 《[驿动的心](../Page/驿动的心.md "wikilink")》（），韩国[MBC](../Page/MBC.md "wikilink")（2005）
  - *Noah's Arc*，美国Logo（2005–2006）
  - 《[青春糖伴](../Page/青春糖伴.md "wikilink")》（*Sugar Rush*），英国[Channel
    4](../Page/第四台_\(英國\).md "wikilink")（2005–2006）
  - *Sleeper
    Cell*，美国[Showtime](../Page/Showtime.md "wikilink")（2005–2006）
  - *Twins*，美国[WB電視網](../Page/WB電視網.md "wikilink")（2005–2006）
  - *Out of
    Practice*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2005–2006）
  - 《[家庭戰爭](../Page/家庭戰爭.md "wikilink")》（*The War At
    Home*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2005–2007）
  - *Dante's Cove*，美国[The N](../Page/The_N.md "wikilink")（2005-2008）
  - 《[心倾何处](../Page/心倾何处.md "wikilink")》（*South of Nowhere*），美国The
    N（2005–2008）
  - *Clara Sheller*，法国[France
    2](../Page/法國電視二台.md "wikilink")（2005–2008）
  - 《[愚人善事](../Page/愚人善事.md "wikilink")》（*My Name Is
    Earl*），美国[NBC](../Page/全国广播公司.md "wikilink")（2005–2009）
  - 《[单身毒妈](../Page/单身毒妈.md "wikilink")》（*Weeds*），美国[Showtime](../Page/Showtime.md "wikilink")（2005–2012）
  - 《[辦公室](../Page/辦公室_\(美國電視劇\).md "wikilink")》（*The
    Office*），美国[NBC](../Page/全国广播公司.md "wikilink")（2005–2013）
  - 《[实习医生格蕾](../Page/實習醫生_\(電視劇\).md "wikilink")》（*Grey's
    Anatomy*），美国[ABC](../Page/美国广播公司.md "wikilink")（2005–）
  - 《[识骨寻踪](../Page/识骨寻踪.md "wikilink")》（*Bones*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2005–）
  - 《[神秘博士](../Page/神秘博士.md "wikilink")》（*Doctor
    Who*），英国[BBC](../Page/BBC.md "wikilink")（2005–）
  - *Sinchronicity*，英国[BBC3](../Page/英國廣播公司第三台.md "wikilink")（2006）
  - *Crumbs*，美国[ABC](../Page/美国广播公司.md "wikilink")（2006）
  - *So Notorious*，美国[VH1](../Page/VH1.md "wikilink")（2006）
  - 《[美丽线条](../Page/美丽线条.md "wikilink")》（*The Line of
    Beauty*），英国[BBC](../Page/BBC.md "wikilink")，三集电视连续剧（2006）
  - 《[花樣少年少女
    (2006年電視劇)](../Page/花樣少年少女_\(2006年電視劇\).md "wikilink")》，台灣華視（2006）
  - 《[變身公主](../Page/變身公主.md "wikilink")》（），日本[EX](../Page/朝日電視台.md "wikilink")（2006）
  - 《[我比誰都愛媽媽](../Page/我比誰都愛媽媽.md "wikilink")》（），日本[TBS](../Page/TBS電視台.md "wikilink")（2006）
  - 《[我就是我](../Page/我就是我.md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（2006）
  - 《[三年二班](../Page/三年二班.md "wikilink")》（*The
    Class*），美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2006–2007）
  - *Cover Girl*，加拿大[SRC](../Page/加拿大法語頻道.md "wikilink")（2006–2007）
  - *The
    Business*，美国[IFC](../Page/IFC_\(美国电视网\).md "wikilink")（2006–2007）
  - 《[丑女贝蒂](../Page/丑女贝蒂.md "wikilink")》（*Ugly
    Betty*），美国[ABC](../Page/美国广播公司.md "wikilink")（2006–2010）
  - *Big Love*，美国[HBO](../Page/HBO.md "wikilink")（2006–2011）
  - 《[家族風雲](../Page/兄弟姐妹_\(美国电视剧\).md "wikilink")》（*Brothers &
    Sisters*），美国[ABC](../Page/美国广播公司.md "wikilink")（2006–2011）
  - 《[灵异之城](../Page/灵异之城.md "wikilink")》（*Eureka*），美国[Syfy](../Page/Syfy.md "wikilink")（2006–2012）
  - 《[超級製作人](../Page/超級製作人.md "wikilink")》（*30
    Rock*），美国[NBC](../Page/全国广播公司.md "wikilink")（2006–2013）
  - 《[火炬木小组](../Page/火炬木小组.md "wikilink")》（*Torchwood*），英国[BBC](../Page/BBC.md "wikilink")、
    [Starz](../Page/Starz_\(電視頻道\).md "wikilink")（2006–）
  - *Waterloo Road*，英国[BBC1](../Page/BBC1.md "wikilink")（2006–）
  - 《[嗜血法医](../Page/嗜血法医.md "wikilink")》（*Dexter*），美国[Showtime](../Page/Showtime.md "wikilink")（2006–）
  - 《[变身怪医](../Page/变身怪医_\(电视剧\).md "wikilink")》（*Jekyll*），英国[BBC
    One](../Page/BBC_One.md "wikilink")（2007）
  - *Kick*，澳大利亞[SBS](../Page/Special_Broadcasting_Service.md "wikilink")（2007）
  - 《[至尊玻璃鞋](../Page/至尊玻璃鞋.md "wikilink")》，台灣華視（2007）
  - 《[咖啡王子1號店](../Page/咖啡王子1號店.md "wikilink")》（），韩国[MBC](../Page/MBC.md "wikilink")（2007）
  - 《[美麗的鬼](../Page/美麗的鬼.md "wikilink")》（），日本[THK](../Page/THK.md "wikilink")（2007）
  - 《[30沒戀人](../Page/30沒戀人.md "wikilink")》（），日本[TBS](../Page/TBS電視台.md "wikilink")（2007）
  - 《[花樣少年少女
    (2007年電視劇)](../Page/花樣少年少女_\(2007年電視劇\).md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2007）
  - 《[父女變變變](../Page/父女變變變.md "wikilink")》（），日本[TBS](../Page/TBS電視台.md "wikilink")（2007）
  - 《[戀愛診斷](../Page/戀愛診斷.md "wikilink")》（），日本[TX](../Page/東京電視台.md "wikilink")（2007）
  - 《[錢作怪](../Page/錢作怪.md "wikilink")》（*Dirty Sexy
    Money*），美国[ABC](../Page/美国广播公司.md "wikilink")（2007–2008）
  - *Dirt*，美国[FX](../Page/FX_\(電視頻道\).md "wikilink")（2007–2008）
  - 《[英雄](../Page/英雄_\(电视剧\).md "wikilink")》（*Heroes*），美国[NBC](../Page/全国广播公司.md "wikilink")（2007–2009）
  - *The Best
    Years*，加拿大[Global](../Page/環球電視_\(加拿大\).md "wikilink")/美国[The
    N](../Page/The_N.md "wikilink")（2007–2009）
  - 《[兽穴俱乐部](../Page/兽穴俱乐部.md "wikilink")》（*The
    Lair*），美国[here\!](../Page/here!.md "wikilink")（2007–2009）
  - 《[捉鬼男](../Page/捉鬼男.md "wikilink")》（*Reaper*），美国[The
    CW](../Page/CW電視網.md "wikilink")（2007–2009）
  - 《[灵指神探](../Page/灵指神探.md "wikilink")》（*Pushing
    Daisies*），美国[ABC](../Page/美国广播公司.md "wikilink")（2007–2009）
  - 《[都铎王朝](../Page/都铎王朝_\(电视剧\).md "wikilink")》（*The
    Tudors*），美国[Showtime](../Page/Showtime.md "wikilink")/英国[BBC](../Page/BBC.md "wikilink")/加拿大[CBC](../Page/加拿大廣播公司電視.md "wikilink")（2007–2010）
  - *The Sarah Silverman
    Program*，美国[喜劇中心](../Page/喜劇中心.md "wikilink")（2007–2010）
  - *Greek*，美国[ABC Family](../Page/ABC家庭.md "wikilink")（2007–2011）
  - 《[绯闻女孩](../Page/绯闻女孩.md "wikilink")》（*Gossip Girl*），美国[The
    CW](../Page/CW電視網.md "wikilink")（2007–2012）
  - 《[广告狂人](../Page/广告狂人.md "wikilink")》（*Mad
    Men*），美国[AMC](../Page/AMC_\(電視頻道\).md "wikilink")（2007–）
  - 《[法網裂痕](../Page/法網裂痕.md "wikilink")》（*Damages*），美国[FX](../Page/FX_\(電視頻道\).md "wikilink")、[The
    101 Network](../Page/The_101_Network.md "wikilink")（2007–）
  - 《[皮囊](../Page/皮囊.md "wikilink")》（英版）（*Skins*），英国[E4](../Page/E4_\(電視頻道\).md "wikilink")（2007-）
  - *Sordid Lives: The
    Series*，英国[E4](../Page/E4_\(電視頻道\).md "wikilink")（2007-）
  - *Benidorm*，美国Logo（2007-）
  - 《[艾草](../Page/艾草_\(公視人生劇展\).md "wikilink")》，台灣公視人生劇展（2008）
  - 《[公寓春光](../Page/公寓春光_\(公視人生劇展\).md "wikilink")》，台灣公視人生劇展（2008）
  - 《[風之畫師](../Page/風之畫師.md "wikilink")》（），韩国[SBS](../Page/SBS株式會社.md "wikilink")（2008）
  - 《[最後的朋友](../Page/最後的朋友.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2008）
  - 《[Men☆dol
    ～帥男偶像～](../Page/Men☆dol_～帥男偶像～.md "wikilink")》（），日本[TX](../Page/東京電視台.md "wikilink")（2008）
  - *The No. 1 Ladies' Detective
    Agency*，美国[HBO](../Page/HBO.md "wikilink")（2008–2009）
  - *Raising the Bar*，美国[TNT](../Page/特纳电视网.md "wikilink")（2008–2009）
  - 《[小不列顛USA](../Page/小不列顛USA.md "wikilink")》（*Little Britain
    USA*），美国（2008–2009）
  - *Sophie*，加拿大[CBC](../Page/加拿大廣播公司電視.md "wikilink")（2008–2009）
  - 《[就诊](../Page/就诊.md "wikilink")》（*In
    Treatment*），美国[HBO](../Page/HBO.md "wikilink")（2008–2010）
  - 《[探索者传说](../Page/探索者传说.md "wikilink")》（*Legend of the
    Seeker*），美国（2008–2010）
  - *Física o Química*，西班牙Antena 3（2008–2011）
  - 《[真爱如血](../Page/真爱如血.md "wikilink")》（*True
    Blood*），美国[HBO](../Page/HBO.md "wikilink")（2008–）
  - *Sons of Anarchy*，美国[FX](../Page/FX_\(電視頻道\).md "wikilink")（2008–）
  - 《[青春密语](../Page/青春密语.md "wikilink")》（*The Secret Life of the
    American Teenager*），美国ABC Family（2008–）
  - *Kings*，美国[NBC](../Page/全国广播公司.md "wikilink")（2009）
  - *The Prisoner*，美国[AMC](../Page/AMC_\(電視頻道\).md "wikilink")（2009）
  - 《[原來是美男](../Page/原來是美男_\(韓國電視劇\).md "wikilink")》（），韩国[SBS](../Page/SBS株式會社.md "wikilink")（2009）
  - 《[媽媽是人妖](../Page/媽媽是人妖.md "wikilink")》（），日本[TX](../Page/東京電視台.md "wikilink")（2009）
  - 《[粉紅系男孩](../Page/粉紅系男孩.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2009）
  - 《[MW- 第0章
    〜惡魔的遊戲〜](../Page/MW_\(漫畫\).md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（2009）
  - 《[媽媽以前是爸爸](../Page/媽媽以前是爸爸.md "wikilink")》（），日本[WOWOW](../Page/WOWOW.md "wikilink")（2009）
  - 《[未来闪影](../Page/未来闪影.md "wikilink")》（*FlashForward*），美国[ABC](../Page/美国广播公司.md "wikilink")（2009–2010）
  - *Caprica*，美国[Syfy](../Page/Syfy.md "wikilink")（2009-2010）
  - 《[扶伤](../Page/扶伤.md "wikilink")》（*Trauma*），美国[NBC](../Page/全国广播公司.md "wikilink")（2009–2010）
  - 《[星际之门：宇宙](../Page/星际之门：宇宙.md "wikilink")》（*Stargate
    Universe*），美国[Syfy](../Page/Syfy.md "wikilink")（2009–2011）
  - 《[倒错人生](../Page/倒错人生.md "wikilink")》（*United States of
    Tara*），美国[Showtime](../Page/Showtime.md "wikilink")（2009–2011）
  - *Being
    Erica*，加拿大[CBC](../Page/加拿大廣播公司電視.md "wikilink")/美国SOAPNet（2009-2011）
  - *Make It or Break It*，美国ABC Family（2009-2012）
  - 《[南国警察](../Page/南国警察.md "wikilink")》（*Southland*），美国[NBC](../Page/全国广播公司.md "wikilink")、[TNT](../Page/特纳电视网.md "wikilink")
    （2009–）
  - 《[妙警贼探](../Page/妙警贼探.md "wikilink")》（*White
    Collar*），美国[USA電視台](../Page/USA電視台.md "wikilink")（2009–）
  - 《[傲骨贤妻](../Page/傲骨贤妻.md "wikilink")》（*The Good
    Wife*），美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2009–）
  - 《[第十三号仓库](../Page/第十三号仓库.md "wikilink")》（*Warehouse
    13*），美国[Syfy](../Page/Syfy.md "wikilink")（2009–）
  - 《[吸血鬼日记](../Page/吸血鬼日记.md "wikilink")》（*The Vampire Diaries*），美国[The
    CW](../Page/CW電視網.md "wikilink")（2009–）
  - 《[摩登家庭](../Page/摩登家庭.md "wikilink")》（*Modern
    Family*），美国[ABC](../Page/美国广播公司.md "wikilink")（2009 - ）
  - 《[歡樂合唱團](../Page/歡樂合唱團.md "wikilink")》（*Glee*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2009
    - ）
  - *Nurse Jackie*，美国[Showtime](../Page/Showtime.md "wikilink")（2009 - ）

## 2010年代

  - *The Whole Truth*，美国[ABC](../Page/美国广播公司.md "wikilink")（2010）
  - *Rubicon*，美国[AMC](../Page/AMC_\(电视频道\).md "wikilink")（2010）
  - *Outlaw*，美国[NBC](../Page/全国广播公司.md "wikilink")（2010）
  - *Undercovers*，美国[NBC](../Page/全国广播公司.md "wikilink")（2010）
  - *Thorne*，英国Sky1 HD（2010）
  - *Running Wilde*，美国[Fox](../Page/福斯国际电视网.md "wikilink")（2010）
  - 《[人生多美丽](../Page/人生多美麗.md "wikilink")》（），韩国[SBS](../Page/SBS株式會社.md "wikilink")「創社20周年
    特別企劃」（2010）
  - 《[成均館緋聞](../Page/成均館緋聞.md "wikilink")》（），韩国[KBS](../Page/KBS.md "wikilink")（2010）
  - 《[秘密花園](../Page/秘密花園_\(韓國電視劇\).md "wikilink")》（），韩国[SBS](../Page/SBS株式會社.md "wikilink")（2010）
  - 《[娼婦與淑女](../Page/娼婦與淑女.md "wikilink")》（），日本[THK](../Page/THK.md "wikilink")（2010）
  - 《[農大菌物語](../Page/農大菌物語.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2010）
  - 《[愛在旅途](../Page/愛在旅途.md "wikilink")》（），泰國[第5電視台](../Page/泰國第5電視台.md "wikilink")（2010）
  - *Hellcats*，美国[The CW](../Page/CW電視網.md "wikilink")（2010–2011）
  - 《[法律与秩序：洛杉矶](../Page/法律与秩序：洛杉矶.md "wikilink")》（*Law & Order:
    LA*），美国[NBC](../Page/全国广播公司.md "wikilink")（2010-2011）
  - *Gigantic*，美国TeenNick（2010–2011）
  - *$h\*\! My Dad
    Says*，美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2010–2011）
  - *Blue Mountain State*，美国Spike（2010–2011）
  - *The Hard Times of RJ Berger*，美国MTV（2010–2011）
  - *Upstairs Downstairs*，英国[BBC](../Page/BBC.md "wikilink")（2010–2012）
  - *Lip Service*，英国[BBC
    Three](../Page/BBC_Three.md "wikilink")（2010–2012）
  - 《[如果还有明天](../Page/如果还有明天_\(电视剧\).md "wikilink")》（*The Big
    C*），美国[Showtime](../Page/Showtime.md "wikilink")（2010-2013）
  - 《[斯巴达克斯](../Page/斯巴达克斯_\(2010年电视剧\).md "wikilink")》（*Spartacus*），美国[Starz](../Page/Starz_\(電視頻道\).md "wikilink")（2010-2013）
  - *Mistresses*，美国[ABC](../Page/美国广播公司.md "wikilink")（2010-2013）
  - 《[海滨帝国](../Page/海滨帝国.md "wikilink")》（*Boardwalk
    Empire*），美国[HBO](../Page/HBO.md "wikilink")（2010-）
  - 《[美少女的谎言](../Page/美少女的谎言.md "wikilink")》（*Pretty Little
    Liars*），美国ABC Family（2010-）
  - 《[屍行者](../Page/屍行者.md "wikilink")》（*The Walking
    Dead*），美国[AMC](../Page/AMC_\(电视频道\).md "wikilink")（2010-）
  - 《[妖女迷行](../Page/妖女迷行.md "wikilink")》（*Lost
    Girl*），加拿大[Showcase](../Page/Showtime.md "wikilink")（2010-）
  - 《[唐顿庄园](../Page/唐顿庄园.md "wikilink")》（*Downton
    Abbey*），英国[ITV](../Page/獨立電視台.md "wikilink")（2010-）
  - *Dance Academy*，美国ABC1、ABC3（2010–）
  - *Strike Back*，美国Sky1、[Cinemax](../Page/Cinemax.md "wikilink")（2010–）
  - *Grandma's House*，英国BBC Two（2010-）
  - 《[魅力克利夫蘭](../Page/魅力克利夫蘭.md "wikilink")》（*Hot in Cleveland*），美国TV
    Land（2010-）
  - *Sirens*，英国Channel 4（2011）
  - 《[皮囊](../Page/Skins_\(美国电视剧\).md "wikilink")》（美版）（*Skins*），美国[MTV](../Page/MTV.md "wikilink")（2011）
  - *The Playboy Club*，美国[NBC](../Page/全国广播公司.md "wikilink")（2011）
  - *Death Valley*，美国[MTV](../Page/MTV.md "wikilink")（2011）
  - 《[比利提斯俱乐部的女儿们](../Page/比利提斯俱乐部的女儿们.md "wikilink")》（），韩国[KBS](../Page/KBS.md "wikilink")（2011）
  - 《[十七號出入口](../Page/十七號出入口.md "wikilink")》，台灣公視人生劇展（2011）
  - 《[男女生了沒](../Page/男女生了沒_\(電視劇\).md "wikilink")》，台灣中視（2011）
  - 《[刁蠻俏御醫](../Page/刁蠻俏御醫.md "wikilink")》，中國（2011）
  - 《[帝錦](../Page/帝錦.md "wikilink")》，中國（2011）
  - 《[穆桂英掛帥](../Page/穆桂英掛帥_\(2011年電視劇\).md "wikilink")》，中國（2011）
  - 《[新還珠格格](../Page/還珠格格_\(2011年電視劇\).md "wikilink")》，中國[湖南](../Page/湖南衛視.md "wikilink")（2011）
  - 《[暴風雨](../Page/暴風雨_\(池上永一\).md "wikilink")》（），日本[NHK](../Page/NHK.md "wikilink")（2011）
  - 《[IS
    上帝的惡作劇](../Page/IS_上帝的惡作劇.md "wikilink")》（），日本[TX](../Page/東京電視台.md "wikilink")（2011）
  - 《[櫻蘭高校男公關部](../Page/櫻蘭高校男公關部.md "wikilink")》（），日本[TBS](../Page/TBS電視台.md "wikilink")（2011）
  - 《[花樣少年少女2011](../Page/花樣少年少女_\(2011年電視劇\).md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2011）
  - 《[原來是美男
    (日本電視劇)](../Page/原來是美男_\(日本電視劇\).md "wikilink")》（），日本[TBS](../Page/TBS電視台.md "wikilink")（2011）
  - 《[亂馬½](../Page/亂馬½.md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（2011）
  - 《[替身姐妹](../Page/替身姐妹.md "wikilink")》（*Ringer*），美国[The
    CW](../Page/CW電視網.md "wikilink")（2011-2012）
  - *Bedlam*，英国Sky Living（2011-2012）
  - 《[律海佳人](../Page/律海佳人.md "wikilink")》（*Fairly
    Legal*），美国[USA](../Page/USA電視台.md "wikilink")（2011-2012）
  - *The Borgias*，美国[USA](../Page/USA電視台.md "wikilink")（2011-2013）
  - *Happily Divorced*，美国[TV
    Land](../Page/TV_Land.md "wikilink")（2011-2013）
  - 《[幸福結局](../Page/幸福结局.md "wikilink")》（*Happy
    Endings*），美国[ABC](../Page/美国广播公司.md "wikilink")（2011-2013）
  - 《[少狼](../Page/少狼_\(電視劇\).md "wikilink")》（*Teen
    Wolf*），美国[MTV](../Page/MTV.md "wikilink")（2011-）
  - *Shameless*，美国[Showtime](../Page/Showtime.md "wikilink")（2011-）
  - 《[复仇](../Page/復仇_\(電視劇\).md "wikilink")》（*Revenge*），美国[ABC](../Page/美国广播公司.md "wikilink")（2011-）
  - 《[童话镇](../Page/童话镇.md "wikilink")》（*Once Upon a
    Time*），美国[ABC](../Page/美国广播公司.md "wikilink")（2011-）
  - *The
    Killing*，美国[AMC](../Page/AMC_\(电视频道\).md "wikilink")、[Netflix](../Page/Netflix.md "wikilink")（2011-）
  - *Hell on Wheels*，美国[AMC](../Page/AMC_\(电视频道\).md "wikilink")（2011-）
  - 《[权力的游戏](../Page/权力的游戏_\(电视剧\).md "wikilink")》（*Game of
    Thrones*），美国（2011-）
  - *Borgia*，美国[Canal+](../Page/Canal+.md "wikilink")（2011-）
  - *Being Human*，美国[Syfy](../Page/Syfy.md "wikilink")（2011-）
  - 《[美国恐怖故事](../Page/美国恐怖故事.md "wikilink")》（*American Horror
    Story*），美国[FX](../Page/FX_\(電視頻道\).md "wikilink")（2011-）
  - 《[囧女珍娜](../Page/囧女珍娜.md "wikilink")》（*Awkward.*），美国MTV（2011-）
  - 《[郊区故事](../Page/郊区故事.md "wikilink")》（*Suburgatory*），美国[ABC](../Page/美国广播公司.md "wikilink")（2011-）
  - *Old Dogs & New Tricks*，美国（2011-）
  - *GCB*，美国[ABC](../Page/美国广播公司.md "wikilink")（2012）
  - 《[幸福三顆星](../Page/幸福三顆星.md "wikilink")》，中國安徽（2012）
  - 《[東西宮略](../Page/東西宮略.md "wikilink")》，香港無綫（2012）
  - 《[愛上巧克力](../Page/愛上巧克力.md "wikilink")》，台灣三立（2012）
  - 《[K-POP最強生死戰](../Page/K-POP最強生死戰.md "wikilink")》（），韩国[Channel
    A](../Page/Channel_A.md "wikilink")（2012）
  - 《[屋塔房王世子](../Page/屋塔房王世子.md "wikilink")》（），韩国[SBS](../Page/SBS株式會社.md "wikilink")（2012）
  - 《[致美麗的你](../Page/花樣少年少女_\(2012年電視劇\).md "wikilink")》（），韩国[SBS](../Page/SBS株式會社.md "wikilink")（2012）
  - 《[MY
    BOY](../Page/MY_BOY_\(韓劇\).md "wikilink")》（），韩国[Tooniverse](../Page/Tooniverse.md "wikilink")（2012）
  - 《[嗚啦啦夫婦](../Page/嗚啦啦夫婦.md "wikilink")》（），韩国[KBS](../Page/KBS.md "wikilink")（2012）
  - 《[理想的兒子](../Page/理想的兒子.md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（2012）
  - 《[埃及艷后般的女人們](../Page/埃及艷后般的女人們.md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（2012）
  - 《[大奧
    ～誕生【有功·家光篇】](../Page/大奧_～誕生【有功·家光篇】.md "wikilink")》（），日本[TBS](../Page/TBS電視台.md "wikilink")（2012）
  - 《[變性雙面殺手](../Page/變性雙面殺手.md "wikilink")》（*Hit & Miss*），英国Sky
    Atlantic（2012）
  - 《[名聲大噪](../Page/名聲大噪.md "wikilink")》（*Smash*），美国[NBC](../Page/全国广播公司.md "wikilink")（2012-2013）
  - 《[医缘](../Page/医缘.md "wikilink")》（*Emily Owens, M.D.*），美国[The
    CW](../Page/CW電視網.md "wikilink")（2012-2013）
  - *Underemployed*，美国[MTV](../Page/MTV.md "wikilink")（2012-2013）
  - 《[直弯好基友](../Page/直弯好基友.md "wikilink")》（*Partners*），美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2012-2013）
  - 《[三年之癢](../Page/三年之癢.md "wikilink")》（*Whitney*），美国[NBC](../Page/全国广播公司.md "wikilink")（2012-2013）
  - 《[另類家庭](../Page/另类家庭.md "wikilink")》（*The New
    Normal*），美国[NBC](../Page/全国广播公司.md "wikilink")（2012-2013）
  - 《[生活向前冲](../Page/生活向前冲.md "wikilink")》（*Go
    On*），美国[NBC](../Page/全国广播公司.md "wikilink")（2012-2013）
  - 《[心灵旋律](../Page/心灵旋律.md "wikilink")》（*Malibu
    Country*），美国[ABC](../Page/美国广播公司.md "wikilink")（2012-2013）
  - 《[丑闻](../Page/丑闻_\(电视剧\).md "wikilink")》（*Scandal*），美国[ABC](../Page/美国广播公司.md "wikilink")（2012-）
  - 《[重-{案}-組](../Page/重案组_\(美国电视剧\).md "wikilink")》（*Major
    Crimes*），美国[TNT](../Page/特纳电视网.md "wikilink")（2012-）
  - 《[拯救希望](../Page/拯救希望.md "wikilink")》（*Saving
    Hope*），加拿大[CTV](../Page/CTV電視網.md "wikilink")/美国[NBC](../Page/全国广播公司.md "wikilink")（2012-）
  - 《[魂归故里](../Page/魂归故里.md "wikilink")》（*Les
    Revenants*），法国[Canal+](../Page/Canal+.md "wikilink")（2012-）
  - 《[女孩我最大](../Page/女孩我最大.md "wikilink")》（*Girls*），美国[HBO](../Page/HBO.md "wikilink")（2012-）
  - 《[芝加哥烈焰](../Page/芝加哥烈焰.md "wikilink")》（*Chicago
    Fire*），美国[NBC](../Page/全国广播公司.md "wikilink")（2012-）
  - *Bomb Girls*，美国[Global](../Page/環球電視_\(加拿大\).md "wikilink")（2012-）
  - 《[音乐之乡](../Page/音乐之乡.md "wikilink")》（*Nashville*），美国[ABC](../Page/美国广播公司.md "wikilink")（2012-）
  - *House of Lies*，美国Showtime（2012-）
  - 《[憤怒管理](../Page/愤怒管理_\(电视剧\).md "wikilink")》（*Anger
    Management*），美国[FX](../Page/FX_\(电视频道\).md "wikilink")（2012-）
  - *House Husbands*，美国Nine Network（2012-）
  - *The Girl's Guide to
    Depravity*，美国[Cinemax](../Page/Cinemax.md "wikilink")（2012-）
  - *Camp*，美国[NBC](../Page/全国广播公司.md "wikilink")（2013）
  - 《[花木蘭傳奇](../Page/花木蘭傳奇.md "wikilink")》，中國（2013）
  - 《[巾幗大將軍](../Page/巾幗大將軍.md "wikilink")》，中國（2013）
  - 《[落跑甜心](../Page/落跑甜心.md "wikilink")》，中國湖南（2013）
  - 《[戲說台灣－金娘仔掠翁](../Page/金娘仔掠翁.md "wikilink")》，台灣三立（2013）
  - 《[城市·獵人](../Page/城市·獵人.md "wikilink")》，台灣公視（2013）
  - 《[海倫她媽](../Page/海倫她媽.md "wikilink")》，台灣公視學生劇展（2013）
  - 《[梁祝西湖蝶夢](../Page/梁祝西湖蝶夢.md "wikilink")》，台灣霹靂（2013）
  - 《[戲說台灣－錢來也](../Page/錢來也.md "wikilink")》，台灣三立（2013）
  - 《[親愛的 我想告訴你](../Page/親愛的_我想告訴你.md "wikilink")》，台灣緯來關懷人生劇展（2013）
  - 《[双全](../Page/双全.md "wikilink")》，台灣緯來關懷人生劇展（2013）
  - 《[原來是美男 (台灣電視劇)](../Page/原來是美男_\(台灣電視劇\).md "wikilink")》，台灣民視（2013）
  - 《[世間情](../Page/世間情.md "wikilink")》，台灣三立（2013）
  - 《[歐若拉公主](../Page/歐若拉公主.md "wikilink")》（），韩国[MBC](../Page/MBC.md "wikilink")（2013）
  - 《[火之女神井兒](../Page/火之女神井兒.md "wikilink")》（），韩国[MBC](../Page/MBC.md "wikilink")（2013）
  - 《[奇皇后](../Page/奇皇后_\(電視劇\).md "wikilink")》（），韩国[MBC](../Page/MBC.md "wikilink")（2013）
  - 《[驚豔的她](../Page/驚豔的她.md "wikilink")》（），韩国[tvN](../Page/tvN.md "wikilink")（2013）
  - 《[美甲店Paris](../Page/美甲店Paris.md "wikilink")》（），韩国[MBC
    QueeN](../Page/文化廣播_\(韓國\).md "wikilink")（2013）
  - 《[好好長大的女兒荷娜](../Page/好好長大的女兒荷娜.md "wikilink")》（），韩国[SBS](../Page/SBS株式會社.md "wikilink")（2013）
  - 《[合宿的戀人](../Page/合宿的戀人.md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（2013）
  - 《[佐藤家的早飯，鈴木家的晚飯](../Page/佐藤家的早飯，鈴木家的晚飯.md "wikilink")》（），日本[BS
    JAPAN](../Page/BS_JAPAN.md "wikilink")（2013）
  - 《[二丁目的福爾摩斯](../Page/二丁目的福爾摩斯.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2013）
  - 《[女信長](../Page/女信長.md "wikilink")》（），日本（2013）
  - 《[山田君与七个魔女](../Page/山田君与七个魔女.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2013）
  - 《[Girl's Talk
    薔薇組](../Page/Girl's_Talk_薔薇組.md "wikilink")》（），日本[EX](../Page/朝日電視台.md "wikilink")（2013）
  - 《[父親的瘋狂世界](../Page/父親的瘋狂世界.md "wikilink")》（*Sean Saves the
    World*），美国[NBC](../Page/全国广播公司.md "wikilink")（2013-2014）
  - 《[达芬奇恶魔](../Page/达芬奇恶魔.md "wikilink")》（*Da Vinci's
    Demons*），英国、美国[Starz](../Page/Starz.md "wikilink")（2013-）
  - 《[杀手之王](../Page/杀手之王.md "wikilink")》（*The
    Following*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2013-）
  - 《[黑吃黑](../Page/黑吃黑_\(美国电视剧\).md "wikilink")》（*Banshee*），美国[Cinemax](../Page/Cinemax.md "wikilink")（2013-）
  - 《[黑色孤儿](../Page/黑色孤儿.md "wikilink")》（*Orphan
    Black*），加拿大[SPACE](../Page/SPACE_\(電視頻道\).md "wikilink")/美国[BBC
    America](../Page/BBC_America.md "wikilink")（2013-）
  - 《[凯莉日记](../Page/凯莉日记.md "wikilink")》（*The Carrie Diaries*），美国[The
    CW](../Page/CW電視網.md "wikilink")（2013-）
  - 《[如此一家人](../Page/如此一家人.md "wikilink")》（*The Fosters）*，美国ABC
    Family（2013-）
  - 《[穹顶之下](../Page/穹顶之下_\(电视剧\).md "wikilink")》（*Under the
    Dome*），美国[CBS](../Page/CBS_\(電視網\).md "wikilink")（2013-）
  - 《[女子監獄](../Page/女子监狱_\(美国电视剧\).md "wikilink")》（*Orange Is the New
    Black*），美国[Netflix](../Page/Netflix.md "wikilink")（2013-）
  - *Devious Maids*，美国Lifetime（2013-）
  - 《[清道夫](../Page/清道夫_\(电视剧\).md "wikilink")》（*Ray
    Donovan*），美国[Showtime](../Page/Showtime.md "wikilink")（2013-）
  - *The Bridge*，美国[FX](../Page/FX_\(電視頻道\).md "wikilink")（2013-）
  - 《[始祖家族](../Page/始祖家族.md "wikilink")》（*The Originals*），美国[The
    CW](../Page/CW電視網.md "wikilink")（2013-）
  - 《[性爱大师](../Page/性爱大师.md "wikilink")》（*Masters of
    Sex*），美国[Showtime](../Page/Showtime.md "wikilink")（2013-）
  - 《[德古拉](../Page/德古拉_\(电视剧\).md "wikilink")》（*Dracula*），美国[NBC](../Page/全国广播公司.md "wikilink")（2013-）
  - 《[始祖家族](../Page/始祖家族.md "wikilink")》（*The
    Originals*），美国[HBO](../Page/HBO.md "wikilink")（2013-）
  - *Please Like Me*，美国[ABC2](../Page/美国广播公司.md "wikilink")（2013-）
  - 《[神煩警察](../Page/神煩警察.md "wikilink")》（*Brooklyn
    Nine-Nine*），美国[Fox](../Page/福斯国际电视网.md "wikilink")（2013-）
  - 《[我爱男闺蜜](../Page/我爱男闺蜜.md "wikilink")》，中国[安徽等](../Page/安徽卫视.md "wikilink")（2014）
  - 《[犀利仁師](../Page/犀利仁師.md "wikilink")》，中国[浙江](../Page/浙江电视台.md "wikilink")（2014）
  - 《[女人俱樂部](../Page/女人俱樂部.md "wikilink")》，香港無綫（2014）
  - 《[水髮胭脂](../Page/水髮胭脂.md "wikilink")》，香港無綫（2014）
  - 《[我的小公關S〜新人公關業主奇蹟滯留6個月〜](../Page/我的小公關.md "wikilink")》（），日本[EX](../Page/朝日電視台.md "wikilink")（2014）
  - 《[實在性百萬亞瑟王](../Page/實在性百萬亞瑟王.md "wikilink")》（），日本[tvk等](../Page/tvk.md "wikilink")（2014）
  - 《[對不起青春！](../Page/對不起青春！.md "wikilink")》（），日本[TBS](../Page/TBS電視台.md "wikilink")（2014）
  - *Getting On*，美国[HBO](../Page/HBO.md "wikilink")（2014-）
  - *Sirens*，美国[USA](../Page/USA電視台.md "wikilink")（2014-）
  - *Transparent*，美国USA[Amazon.com](../Page/亞馬遜公司.md "wikilink")（2014-）
  - 《[善巖女高偵探團](../Page/善巖女高偵探團.md "wikilink")》（） 韩国 （2014-2015）
  - 《[仙剑客栈](../Page/仙剑客栈_\(网剧\).md "wikilink")》，中國（2015）
  - 《[太子妃升职记](../Page/太子妃升职记.md "wikilink")》，中國（2015）
  - 《[戲說台灣－天公點鴛鴦](../Page/天公點鴛鴦.md "wikilink")》，台灣三立（2015）
  - 《[戲說台灣－白馬穴亂陰陽](../Page/白馬穴亂陰陽.md "wikilink")》，台灣三立（2015）
  - 《[愛上哥們](../Page/愛上哥們.md "wikilink")》，台灣三立（2015）
  - 《[戀愛鄰距離](../Page/戀愛鄰距離.md "wikilink")》，台灣三立（2015）
  - 《[夜行書生](../Page/夜行書生.md "wikilink")》（），韩国[MBC](../Page/MBC.md "wikilink")（2015）
  - 《[問題餐廳](../Page/問題餐廳.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2015）
  - 《[瀨在丸紅子之事件簿〜黑猫的三角〜](../Page/瀨在丸紅子之事件簿〜黑猫的三角〜.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2015）
  - 《[朝5晚9](../Page/朝5晚9.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2015）
  - 《[Transit
    Girls](../Page/Transit_Girls.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2015）
  - 《[獨眼龍♥花嫁道中](../Page/獨眼龍♥花嫁道中.md "wikilink")》（），日本[NHK](../Page/NHK.md "wikilink")（2015）
  - 《[薄櫻鬼SSL 〜sweet school
    life〜](../Page/薄櫻鬼SSL_〜sweet_school_life〜.md "wikilink")》（），日本[TOKYO
    MX2](../Page/TOKYO_MX2.md "wikilink")（2015）
  - 《[偽裝的夫婦](../Page/偽裝的夫婦.md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（2015）
  - 《[武士老師](../Page/武士老師.md "wikilink")》（），日本[EX](../Page/朝日電視台.md "wikilink")（2015）
  - 《[上癮](../Page/上癮_\(網路劇\).md "wikilink")》，中國（2016）
  - 《[今晚，你想點什麼？](../Page/今晚，你想點什麼？.md "wikilink")》，台灣[公視](../Page/公視.md "wikilink")（2016）
  - 《[讓我稱呼岳父大人](../Page/讓我稱呼岳父大人.md "wikilink")》（），日本[KTV](../Page/關西電視台.md "wikilink")（2016）
  - 《[MARS～只是愛著你～](../Page/戰神_\(電視劇\)#日本電視劇.md "wikilink")》（），日本[NTV](../Page/日本電視台.md "wikilink")（2016）
  - 《[胖子老師](../Page/胖子老師.md "wikilink")》（），日本NTV（2016）
  - 《[逃避雖可恥但有用](../Page/逃避雖可恥但有用.md "wikilink")》（），日本TBS（2016）
  - 《[闇影獵人](../Page/闇影獵人_\(電視劇\).md "wikilink")》（），美國[Freeform](../Page/Freeform.md "wikilink")（2016）
  - *Zoe Ever After*，美國[BET](../Page/黑人娛樂電視台.md "wikilink")（2016）
  - 《[警魂](../Page/警魂.md "wikilink")》（），美國NBC（2016）
  - 《[財富之戰](../Page/財富之戰.md "wikilink")》（），美國Showtime（2016）
  - 《[明日傳奇](../Page/明日傳奇.md "wikilink")》（），美國CW（2016）
  - 《[魔法師](../Page/魔法師.md "wikilink")》（），美國Syfy（2016）
  - 《[沙娜拉傳奇](../Page/沙娜拉傳奇.md "wikilink")》（），美國[MTV](../Page/音樂電視網.md "wikilink")（2016）
  - *Recovery Road*，美國Freeform（2016）
  - 《[不完美家庭](../Page/不完美家庭.md "wikilink")》（），美國ABC（2016）
  - 《[陌生血親](../Page/陌生血親.md "wikilink")》（），美國ABC（2016）
  - *Heartbeat*，美國NBC（2016）
  - 《[應召女友](../Page/應召女友.md "wikilink")》（），美國[Starz](../Page/Starz_\(电视频道\).md "wikilink")（2016）
  - 《[致命誘惑](../Page/致命誘惑.md "wikilink")》（），美國ABC（2016）
  - *Hap and Leonard*，美國SundanceTV（2016）
  - *Damien*，美國A\&E（2016）
  - *Hunters*，美國Syfy （2016）
  - 《[鮮血淋漓](../Page/鮮血淋漓.md "wikilink")》（），[加拿大Super](../Page/加拿大.md "wikilink")
    Channel（2016）
  - *Wynonna Earp*，加拿大CHCH-DT（2016）
  - *Crashing*，英國Channel 4（2016）
  - *Marcella*，英國ITV（2016）
  - *The Durrells*，英國ITV（2016）
  - 《[夜班經理](../Page/夜班經理.md "wikilink")》（），英國[BBC
    One](../Page/英國廣播公司第一台.md "wikilink")、美國AMC（2016）
  - *Buscando el norte*，[西班牙](../Page/西班牙.md "wikilink")[Antena
    3](../Page/天线3.md "wikilink")（2016）
  - *Santa Bárbara*，[葡萄牙TVI](../Page/葡萄牙.md "wikilink")（2016）
  - 《[HIStory系列](../Page/HIStory_\(2017年電視劇\).md "wikilink")》，台灣[CHOCO
    TV](../Page/CHOCO_TV.md "wikilink")（2017）
  - 《[搖滾畢業生](../Page/搖滾畢業生.md "wikilink")》，台灣CHOCO TV（2017）
  - 《[紅色氣球](../Page/紅色氣球.md "wikilink")》，台灣[KKTV](../Page/KKTV.md "wikilink")（2017）
  - 《[下北澤Die
    Hard](../Page/下北澤Die_Hard.md "wikilink")》（），日本[TX](../Page/東京電視台.md "wikilink")（2017）
  - 《[戰國快跑女高中生](../Page/戰國快跑女高中生.md "wikilink")》（），日本[NHK](../Page/NHK.md "wikilink")（2017）
  - 《[單戀](../Page/單戀_\(小說\).md "wikilink")》（），日本[WOWOW](../Page/WOWOW.md "wikilink")（2017）
  - 《[黑色孤兒](../Page/黑色孤兒.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2017）
  - 《[深藍與月光](../Page/深藍與月光.md "wikilink")》，台灣[酷瞧](../Page/酷瞧.md "wikilink")（2018）
  - 《[朝鮮美人別傳](../Page/朝鮮美人別傳.md "wikilink")》（），韓國[KBS](../Page/KBS.md "wikilink")（2018）
  - 《[海月姬](../Page/海月姬.md "wikilink")》（），日本[CX](../Page/富士電視台.md "wikilink")（2018）
  - 《[女子的生活](../Page/女子的生活.md "wikilink")》（），日本[NHK](../Page/NHK.md "wikilink")（2018）
  - 《[弟弟的丈夫](../Page/弟弟的丈夫.md "wikilink")》（），日本[NHK](../Page/NHK.md "wikilink")（2018）
  - 《[大叔之愛](../Page/大叔之愛.md "wikilink")》（），日本EX（2018）

## 另見

  - [LGBT相關電影列表](../Page/LGBT相關電影列表.md "wikilink")

## 外部链接

  - <https://web.archive.org/web/20030710083900/http://www.shergoodforest.com/tv/shows.html>

[LGBT相關電視節目](../Category/LGBT相關電視節目.md "wikilink")
[Category:LGBT相關列表](../Category/LGBT相關列表.md "wikilink")
[Category:電視劇列表](../Category/電視劇列表.md "wikilink")
[Category:媒体对LGBT的描绘](../Category/媒体对LGBT的描绘.md "wikilink")