**藤原 飛呂**(也稱 **藤原
尋**)\[1\]，日本[女性](../Page/女性.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。血型B型。

目前活躍於《[白泉社](../Page/白泉社.md "wikilink")》的日本少女漫畫雜誌《[LaLa](../Page/LaLa.md "wikilink")》。

[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[印度尼西亞](../Page/印度尼西亞.md "wikilink")、[德國](../Page/德國.md "wikilink")、[美國皆有授權出版她的](../Page/美國.md "wikilink")[漫畫作品](../Page/漫畫.md "wikilink")《[學生會長是女僕](../Page/學生會長是女僕.md "wikilink")》。

## 經歷

  - 2004年，以《帰り道、雪の熱》獲得第144回LMS　ベストルーキー賞（最佳新人獎）。
  - 2004年，以《紅い夢》獲得第36回LMG　フレッシュデビュー賞（初次亮相獎）。
  - 2006年，以《[學生會長是女僕](../Page/學生會長是女僕.md "wikilink")》獲得第31回[白泉社雅典娜新人大賞新作優秀者賞](../Page/白泉社雅典娜新人大賞.md "wikilink")。
  - 2008年5月在大阪舉行簽名會，同年8月在[香港動漫電玩節舉行第二次簽名會](../Page/香港動漫電玩節.md "wikilink")。\[2\]

## 作品

### 長篇作品

  - 《[學生會長是女僕](../Page/學生會長是女僕.md "wikilink")》（『LaLa』2006年 - 2013年、全18卷）
  - 《ユキは地獄に堕ちるのか》(雪紀你難道要墜入地獄了嗎)（『LaLa』2014年 - 2016年、全6卷）

### 短篇作品

  - 《鏡界の死神》 (刊登於黑LaLa)
  - 《透明な世界》（透明的世界）（刊登於2005/10/8　LaLaDX
    11月號，收錄於[學生會長是女僕單行本第一集](../Page/學生會長是女僕.md "wikilink")）
  - 《少年スクランブル》（刊登於2005/04/23　LaLa 6月號）
  - 《君の陽だまり》（刊登於2005/02/10　LaLaDX 3月號）
  - 《半熟ウルフ》（刊登於2004/11/10　LaLa　Special）

### 投稿作品

  - 《紅い夢》　 第36回LMG　フレッシュデビュー賞（初次亮相獎）（刊登於2004/10/09　LaLaDX 11月號）
  - 《帰り道、雪の熱》　第144回LMS　ベストルーキー賞（最佳新人獎）（刊登於2004/04/10　LaLaDX 5月號）

### 其它

  - 《会長はメイド様\!公式ファンブックご主人様も大満足》

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [ZERO](http://zero.mods.jp/index.html)（公式網站）

  - [](http://www.amazon.co.jp/exec/obidos/search-handle-url?%5Fencoding=UTF8&search-type=ss&index=books-jp&field-author=%E8%97%A4%E5%8E%9F%20%E3%83%92%E3%83%AD)

  - [博客來網路書店 - 目前您搜尋的關鍵字為:
    藤原飛呂](http://search.books.com.tw/exep/prod_search_author.php?cat=all&key=%C3%C3%AD%EC%AD%B8%A7f)

[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:日本女性漫画家](../Category/日本女性漫画家.md "wikilink")
[Category:兵库县出身人物](../Category/兵库县出身人物.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")

1.
2.  [會長是女僕大人單行本第七卷附頁](../Page/會長是女僕大人.md "wikilink")。