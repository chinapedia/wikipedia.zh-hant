在[抽象代數裡](../Page/抽象代數.md "wikilink")，**原群**是一種基本的[代數結構](../Page/代數結構.md "wikilink")。具體地說，原群有一個[集合](../Page/集合.md "wikilink")
*M* 和一個 *M* 上的[二元運算](../Page/二元運算.md "wikilink") *M* × *M* → *M*
。此二元運算依定義是[封閉的](../Page/閉包_\(數學\).md "wikilink")，且除此之外便沒有其他[公理被加在此運算中](../Page/公理.md "wikilink")。

## 類型

原群並不常被研究；相對地，存在一些不同類型的原群，依據其運算需符合公理的不同。一般常被研究的原群類型有：

  - [擬群](../Page/擬群.md "wikilink")－[可除總是可能的](../Page/可除.md "wikilink")[非空原群](../Page/空集.md "wikilink")；
  - [環群](../Page/環群.md "wikilink")－有[單位元的擬群](../Page/單位元.md "wikilink")；
  - [半群](../Page/半群.md "wikilink")－運算為[可結合的原群](../Page/結合律.md "wikilink")；
  - [-{zh-cn:幺半群;
    zh-tw:么半群;}-](../Page/么半群.md "wikilink")－有[單位元的半群](../Page/單位元.md "wikilink")；
  - [群](../Page/群.md "wikilink")－有[逆元的](../Page/逆元.md "wikilink")-{zh-cn:幺半群;
    zh-tw:么半群;}-，或等價地說，可結合的環群；
  - [阿貝爾群](../Page/阿貝爾群.md "wikilink")－運算為[可交換的群](../Page/交換律.md "wikilink")。

[MiQSdaNGLe.PNG](https://zh.wikipedia.org/wiki/File:MiQSdaNGLe.PNG "fig:MiQSdaNGLe.PNG")的存在。\]\]

## 原群的態射

原群的[態射是一個函數](../Page/態射.md "wikilink") \(f:M\to N\) ，將原群 *M* 映射至原群 *N*
上，並保留其二元運算：

\[f(x \; *_M \;y) = f(x) \; *_N\; f(y)\]

其中的 \(*_M\) 和 \(*_N\) 分別代表著在 *M* 和 *N* 上的二元運算。

## 自由原群

在一集合 *X* 上的**自由原群** \(M_X\) 是指由集合 *X*
產生出的「最一般可能的」自由原群（並沒有任何的關係或公理在產生子上；詳見[自由對象](../Page/自由對象.md "wikilink")）。自由原群可以用[計算機科學中熟悉的詞彙來描述](../Page/計算機科學.md "wikilink")，如同其樹葉被
*X*
內的元素標示的[二元樹的原群](../Page/二元樹.md "wikilink")，其運算是將樹在樹根上連結。因此，自由原群在[語法學中有著很基本的重要性](../Page/語法學.md "wikilink")。

自由原群有個[泛性質](../Page/泛性質.md "wikilink")，其內容為：若 \(f:X\to N\) 是一個從集合 *X*
映射至任一原群 *N* 的函數，則會存在唯一一個 \(f\) 至原群態射 \(f^\prime\) 的擴張。其中，

\[f^\prime:M_X \to N\]

## 另見

  - [自由半群](../Page/自由半群.md "wikilink")
  - [自由群](../Page/自由群.md "wikilink")
  - [自由李群](../Page/自由李群.md "wikilink")

## 參考文獻

<references />

  -
  -
  -
[U](../Category/二元运算.md "wikilink") [U](../Category/非结合代数.md "wikilink")
[Category:代数结构](../Category/代数结构.md "wikilink")