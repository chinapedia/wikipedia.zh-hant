**秋海棠属**（[學名](../Page/學名.md "wikilink")：）是[葫蘆目](../Page/葫蘆目.md "wikilink")[秋海棠科肉质](../Page/秋海棠科.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")。秋海棠屬植物多數為[多年生肉質草本](../Page/多年生植物.md "wikilink")，常生於潮濕溫暖的環境，在中國大陸，主要分布於[長江流域以南各省](../Page/長江流域.md "wikilink")，以[雲南](../Page/雲南.md "wikilink")、[廣西最多](../Page/廣西.md "wikilink")，主要分[大花秋海棠](../Page/大花秋海棠.md "wikilink")、[多花秋海棠](../Page/多花秋海棠.md "wikilink")、[垂枝多海棠三类](../Page/垂枝多海棠.md "wikilink")，有很多品种，如[珊瑚秋海棠](../Page/珊瑚秋海棠.md "wikilink")、[四季海棠](../Page/四季海棠.md "wikilink")、[球根海棠](../Page/球根海棠.md "wikilink")、[丽格海棠](../Page/丽格海棠.md "wikilink")、等等，在台灣原生的秋海棠有18種，其中14種為台灣特有種，3種為天然雜交種，台灣的秋海棠主要分布於中、低[海拔地區](../Page/海拔.md "wikilink")，生於較為潮濕的土壤或岩壁上，常可作為潮濕環境的指標物種；其中有些種類分布廣泛，有些種類則僅見於某些特定地區，台灣原生秋海棠植物物種之水平地理分布與台灣的自然地理分區有著極為密切的關聯。在植物的命名上，植物學者常以發現地來為植物取名。

[HK_CWB_銅鑼灣_Causeway_Bay_Hysan_Place_plant_秋海棠屬_Begonia_flowers_October_2017_IX1_flowers_01.jpg](https://zh.wikipedia.org/wiki/File:HK_CWB_銅鑼灣_Causeway_Bay_Hysan_Place_plant_秋海棠屬_Begonia_flowers_October_2017_IX1_flowers_01.jpg "fig:HK_CWB_銅鑼灣_Causeway_Bay_Hysan_Place_plant_秋海棠屬_Begonia_flowers_October_2017_IX1_flowers_01.jpg")\]\]

## 外部連結

  -
  - [W. S. Hoover et al. 2004, Notes on the geography of South-East
    Asian Begonia and species diversity in montane
    forests](http://rbgsyd.nsw.gov.au/__data/assets/pdf_file/0005/72752/Tel10Hoo749.pdf)

  - [四季秋海棠1](https://web.archive.org/web/20060105140721/http://person.dfes.tpc.edu.tw/ab579/plant/n04003.htm)

  - [四季秋海棠2](https://web.archive.org/web/20070707044044/http://www.yctsayl.idv.tw/yctsayl/html/8a/8a000.htm)

  - [秋海棠1](https://web.archive.org/web/20071114043721/http://www.tbg.org.tw/~tbgweb/cgi-bin/topic.cgi?forum=80&topic=3&show=0)

  - [秋海棠2](http://www.ttvs.cy.edu.tw/kcc/9522ch/ch.htm)

  - [心動了嗎？想更深入了解秋海棠的美嗎？](http://digiarch.sinica.edu.tw/content.jsp?option_id=2442&index_info_id=941)

[秋海棠属](../Category/秋海棠属.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")
[Category:園藝植物](../Category/園藝植物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")