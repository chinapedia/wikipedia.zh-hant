**加文·纽森**（[英語](../Page/英語.md "wikilink")：，），又譯**纽斯萨姆**、**紐瑟姆**或**纽萨姆**，[美國](../Page/美國.md "wikilink")[民主黨籍政治人物](../Page/民主黨.md "wikilink")。現任（第40任）[加利福尼亚州州长](../Page/加利福尼亚州.md "wikilink")，曾任三藩市市長、第49任加州副州長。

[2018年美國期中選舉](../Page/2018年美國期中選舉.md "wikilink")，加州副州長紐森代表民主黨參選加州州長並成功當選，紐森在2019年1月7日宣誓就職，成為第40任加州州長。

## 生平

生于[三藩市湾区](../Page/三藩市湾区.md "wikilink")，圣塔克拉拉大学政治科学学士，[民主党人](../Page/民主党_\(美國\).md "wikilink")。他在2003年11月當選[三藩市第](../Page/三藩市.md "wikilink")42届市长，2004年1月8日就職，上任後他推動[同性婚姻合法化](../Page/同性婚姻.md "wikilink")，曾給予伴侶註冊。

2010年11月当选美国加利福尼亚州副州长。2011年1月10日，加文·纽森提前一年结束任期，就任美国加利福尼亚州副州长。

## 醜聞

2007年1月31日晚間《[舊金山紀事報](../Page/舊金山紀事報.md "wikilink")》揭露纽森在[離婚前便與其競選經理兼好友艾力克斯](../Page/離婚.md "wikilink")·圖爾克（Alex
Tourk）的妻子露比（Ruby Rippey-Tourk）有染，纽森隨後公開承認並向社會道歉。

[Category:旧金山市长](../Category/旧金山市长.md "wikilink")
[Category:加利福尼亚州民主党人](../Category/加利福尼亚州民主党人.md "wikilink")
[Category:美國LGBT權利運動家](../Category/美國LGBT權利運動家.md "wikilink")
[Category:美國天主教徒](../Category/美國天主教徒.md "wikilink")
[Category:爱尔兰裔美国人](../Category/爱尔兰裔美国人.md "wikilink")
[Category:失读症患者](../Category/失读症患者.md "wikilink")