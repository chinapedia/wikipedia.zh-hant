**更登確吉尼瑪**（又译为「根敦曲吉尼玛」\[1\]，，），[藏族人](../Page/藏族人.md "wikilink")，生於[中華人民共和國](../Page/中華人民共和國.md "wikilink")[西藏自治區](../Page/西藏自治區.md "wikilink")[那曲地區](../Page/那曲地區.md "wikilink")[嘉黎縣](../Page/嘉黎縣.md "wikilink")。

在[第十世班禅额尔德尼圓寂后](../Page/第十世班禅额尔德尼·确吉坚赞.md "wikilink")，[第十四世達賴喇嘛在](../Page/第十四世達賴喇嘛.md "wikilink")“[金瓶掣签](../Page/金瓶掣签.md "wikilink")”仪式前认定其為班禪的轉世[靈童](../Page/靈童.md "wikilink")，但這項認定未被[中華人民共和國政府承認](../Page/中華人民共和國政府.md "wikilink")。中國政府按照清朝传统“金瓶掣签”仪式冊立[確吉傑布作為第十一世班禪](../Page/確吉傑布.md "wikilink")。[西藏流亡政府部分成员称未經達賴喇嘛認證](../Page/西藏流亡政府.md "wikilink")、且動手腳抽出的是「名不正言不順」，称[確吉傑布為](../Page/確吉傑布.md "wikilink")「漢班禪」、「中共班禪」、「政治上的假班禪」，更登確吉尼瑪則被他们俗称為“**藏班禅**”、“**博班禅**”、或“宗教上的真班禪”。\[2\]\[3\]\[4\]\[5\]\[6\]\[7\]\[8\]\[9\]\[10\]\[11\]

[西藏流亡政府等組織](../Page/西藏流亡政府.md "wikilink")\[12\]認為更登確吉尼瑪在1995年5月17日被中國政府[軟禁後](../Page/軟禁.md "wikilink")，下落不明、生死未卜，從此未再公開露面。一些流亡藏人和國際人權組織認為他遭到非法拘禁與限制人身自由，並稱其為“世界上年紀最小的[政治犯](../Page/政治犯.md "wikilink")”\[13\]\[14\]，但這項指控被中國政府否認，稱其生活正常，未遭拘禁、限制\[15\]。2018年4月，[第十四世达赖喇嘛承认中华人民共和国政府的说法属实](../Page/第十四世达赖喇嘛.md "wikilink")，间接承认了之前无论是西方媒体还是西藏流亡政府，在对待此问题上都不约而同的说了谎。\[16\]

## 背景

[圖博-西藏人於西藏人民起義日-抗暴紀念日在聯合國外抗議中國拘禁大批政治犯_Tibetans_and_Supporters_Protest_Against_China_for_Political_Prisoners_@_UN_in_NYC_on_March_10th_Tibetan_Uprising_Day.jpg](https://zh.wikipedia.org/wiki/File:圖博-西藏人於西藏人民起義日-抗暴紀念日在聯合國外抗議中國拘禁大批政治犯_Tibetans_and_Supporters_Protest_Against_China_for_Political_Prisoners_@_UN_in_NYC_on_March_10th_Tibetan_Uprising_Day.jpg "fig:圖博-西藏人於西藏人民起義日-抗暴紀念日在聯合國外抗議中國拘禁大批政治犯_Tibetans_and_Supporters_Protest_Against_China_for_Political_Prisoners_@_UN_in_NYC_on_March_10th_Tibetan_Uprising_Day.jpg")於[西藏抗暴紀念日在](../Page/西藏抗暴紀念日.md "wikilink")[聯合國外抗議中國拘禁包括第十一世班禪在內的大批西藏政治犯](../Page/聯合國.md "wikilink")\]\]
[Where_is_he?_(5676598240).jpg](https://zh.wikipedia.org/wiki/File:Where_is_he?_\(5676598240\).jpg "fig:Where_is_he?_(5676598240).jpg")
十世班禪[班禪額爾德尼·確吉堅贊](../Page/班禪額爾德尼·確吉堅贊.md "wikilink")1989年於西藏[圓寂後](../Page/圓寂.md "wikilink")，[中國政府組織靈童尋訪小組尋訪轉世靈童](../Page/中華人民共和國中央人民政府.md "wikilink")，該靈童尋訪小組同時並與流亡中的[第十四世達賴喇嘛保持聯繫](../Page/第十四世達賴喇嘛.md "wikilink")\[17\]\[18\]（雙方在此之前曾經有過共同指認同一個候選人為轉世靈童的先例，即[第十七世噶玛巴](../Page/第十七世噶玛巴.md "wikilink")）。靈童尋訪小組經過長達數年的尋訪，初步找到了五名男童，更登确吉尼玛即為其中之一。1995年，[全國政協委員](../Page/全國政協委員.md "wikilink")、靈童尋訪小組組長、[扎什倫布寺住持](../Page/扎什倫布寺.md "wikilink")[恰扎·强巴赤列將尋訪結果通信告知達賴喇嘛後](../Page/恰扎·强巴赤列.md "wikilink")，達賴喇嘛於1995年5月14日单方面宣佈更登確吉尼瑪是十世班禪喇嘛的[轉世靈童](../Page/轉世靈童.md "wikilink")，中国政府隨即宣佈[恰扎·强巴赤列](../Page/恰扎·强巴赤列.md "wikilink")「叛國」，並且不承認更登確吉尼瑪的靈童候選資格。恰扎·强巴赤列於次年被判處有期徒刑6年。

靈童尋訪小組另外挑選五名男童中的三名即[坚赞诺布](../Page/坚赞诺布.md "wikilink")、贡桑旺堆、阿旺南追作為第十世班禅转世灵童候选对象參加[金瓶掣簽儀式](../Page/金瓶掣簽.md "wikilink")，更登确吉尼玛不在這三名男童當中。1995年11月29日經過金瓶掣簽，中国政府将坚赞诺布确认為第十世班禪額爾德尼的转世灵童。坚赞诺布于1995年12月8日在扎什伦布寺举行[坐床典礼](../Page/坐床儀式.md "wikilink")，经师[波米·强巴洛珠活佛为其剃度](../Page/波米·强巴洛珠.md "wikilink")，并为之取法名为吉尊·洛桑强巴伦珠确吉杰布·白桑布。

更登確吉尼瑪的父親是貢確平措，母親是德慶曲珍，還有一名大哥和一名姐姐。西藏流亡政府方面称，在更登確吉尼瑪被第十四世達賴喇嘛確認為第十世班禪喇嘛的轉世靈童後，他的家人被中国政府秘密劫持，至今已有18年之久。\[19\]

## 失蹤

[中华人民共和国中央政府稱](../Page/中华人民共和国中央政府.md "wikilink")，當時只有六歲的男童更登確吉尼瑪只是「普通小孩」，為了避免其陷入政治鬥爭中，政府並於1995年5月17日將其從家中帶走「保護起來」（1996年中国政府稱），但自此之後，更登確吉尼瑪與其家人被西藏流亡政府认定为下落不明。[西藏流亡政府稱他是世界上年紀最小的](../Page/西藏流亡政府.md "wikilink")[政治犯](../Page/政治犯.md "wikilink")，又呼籲國際社會關注；2006年中华人民共和国中央政府則稱他状况良好，家人只想過平靜生活。\[20\]

在西藏流亡政府所在地的[達蘭薩拉](../Page/達蘭薩拉.md "wikilink")，僧人們會特地在4月25日更登雀吉尼瑪生日當天在佛寺裡替其舉行平安法會，[西藏流亡政府也將這天定為官方紀念日](../Page/西藏流亡政府.md "wikilink")。\[21\]

[西藏自治區副主席](../Page/西藏自治區.md "wikilink")[尼瑪次仁在](../Page/尼瑪次仁.md "wikilink")2007年7月28日曾強調，將更登確吉尼瑪稱為班禪是不合法的，同時強調更登確吉尼瑪目前已在西藏上[高中](../Page/高中.md "wikilink")，兄弟姐妹中既有學生亦有已工作的社會人士。中國政府表示中央政府主持金瓶掣签仪式确定十一世班禅后，一直注意保护其他候选灵童的隐私，但一些外国势力不断传播有关谣言。更登確吉尼瑪是愛國的公民，这名“灵童”本身就是一个受害者，目前像普通人民一样生活，不希望外界打扰。政府無權干涉其生活，並尊重其意願\[22\]。

2013年，[藏人行政中央和](../Page/藏人行政中央.md "wikilink")[西藏人民議會發布聯合聲明表示](../Page/西藏人民議會.md "wikilink")，5月17日是第十一世班禪更登確吉尼瑪被秘密劫持之日，因而將2013年5月17日定為「國際聲援西藏日」，以紀念因中共政府侵占西藏所導致非正常死亡的100多萬藏人與2009年後為爭取自由而自焚的藏人。\[23\]

2018年4月，[第十四世达赖喇嘛承认中华人民共和国政府的说法属实](../Page/第十四世达赖喇嘛.md "wikilink")，更登確吉尼瑪仍健康生活，各项人权未受任何侵犯，并接受正常的教育。\[24\]

## 觀點

據中國官媒报導，第十世班禪額爾德尼在世时，对于活佛转世问题，提出应“先找出三个候选灵童，然后逐一进行调查”，“我想在[释迦牟尼像前](../Page/释迦牟尼.md "wikilink")，采取‘金瓶掣签’的办法来确定。”
\[25\]\[26\]

[藏族作家](../Page/藏族.md "wikilink")、詩人[唯色與其夫](../Page/唯色.md "wikilink")[王力雄](../Page/王力雄.md "wikilink")（《天葬：西藏的命運》等書作者）表示：讓假班禪德珠仁波切的轉世靈童“金瓶掣籤”是為將來由作為木偶的他“金瓶掣籤”第十五世達賴喇嘛做準備。中國政府本身是無神論的，沒有任何宗教信仰，是個沒有資格認定活佛的世俗政權，但要來控制藏傳佛教的活佛轉世。\[27\]\[28\]

第八世[阿嘉呼圖克圖](../Page/阿嘉呼圖克圖.md "wikilink")[阿嘉·洛桑圖旦·久美嘉措](../Page/阿嘉·洛桑圖旦·久美嘉措.md "wikilink")（阿嘉仁波切）於其自傳《順水逆風》（Surviving
the
Dragon）中声称中共宗教局長[葉小文在儀式後返回北京的專機上親口向阿嘉仁波切](../Page/葉小文.md "wikilink")、[嘉木樣活佛](../Page/嘉木樣活佛.md "wikilink")、與[李鐵映承認如何動手腳以確保抽出屬意的人選](../Page/李鐵映.md "wikilink")：「我們在選定靈童過程中力求做到萬無一失，（我們）在一個裝寫有候
選人姓名的象牙簽的錦套中塞了棉花，讓這個候選人的簽高出一截，因此才順利的選中了合適的候選人。」\[29\]\[30\]

## 参考文献

## 外部連結

  - [流亡藏人慶祝班禪更登確吉尼瑪年滿18歲](http://www.savetibet.org/cn/news/newsitem.php?id=918)
  - [BBC:达赖指定班禅转世灵童年满18岁](http://news.bbc.co.uk/chinese/simp/hi/newsid_6590000/newsid_6590400/6590423.stm)

## 參見

  - [班禪額爾德尼](../Page/班禪額爾德尼.md "wikilink")
  - [第十世班禪額爾德尼](../Page/第十世班禪額爾德尼.md "wikilink")
  - [班禅额尔德尼·确吉杰布](../Page/班禅额尔德尼·确吉杰布.md "wikilink")
  - [轉世傳承](../Page/轉世傳承.md "wikilink")
  - [金瓶掣签](../Page/金瓶掣签.md "wikilink")

{{-}}

[Category:中华人民共和国藏传佛教人物](../Category/中华人民共和国藏传佛教人物.md "wikilink")
[Category:中华人民共和国下落不明者](../Category/中华人民共和国下落不明者.md "wikilink")
[?](../Category/班禪喇嘛.md "wikilink")
[Category:那曲人](../Category/那曲人.md "wikilink")
[Category:藏族人](../Category/藏族人.md "wikilink")

1.

2.  [轉世－－達賴喇嘛尊者的聲明](http://www.tibet.org.tw/uploads/TSAT-005s.pdf),
    《西藏的天空》The Sky Above Tibet Quarterly 5, 達賴喇嘛西藏宗教基金會,
    2011-11-15

3.  [噶廈在第十一世班禪仁波切二十五歲生日慶祝會上的講話](http://xizang-zhiye.org/噶廈在第十一世班禪仁波切二十五歲生日慶祝會上-3/),
    [西藏流亡政府](../Page/西藏流亡政府.md "wikilink"), 2014-4-25

4.  [班禪喇嘛今何在？](http://www.open.com.hk/content.php?id=994#.VQtRKo7F8Vs),
    [開放雜誌](../Page/開放雜誌.md "wikilink"), 2012-10-6

5.  《[尋訪班禪喇嘛 The Search for the Panchen
    Lama](http://www.readingtimes.com.tw/ReadingTimes/ProductPage.aspx?gp=productdetail&cid=mcca\(SellItems\)&id=BC0160&p=excerpt&exid=31669)》,時報文化,
    2004-1-15

6.  [排除政治干擾 達賴結束轉世](http://www.cna.com.tw/news/acn/201409080219-1.aspx),
    [中央通訊社](../Page/中央通訊社.md "wikilink"), 2014-9-8

7.  [誰有權決定達賴喇嘛轉世制度的存廢？](http://www.rfa.org/mandarin/yataibaodao/shaoshuminzu/cyl-12192014101247.html),
    [自由亞洲電台](../Page/自由亞洲電台.md "wikilink"), 2014-12-19

8.  [遠離中國](http://www.newtaiwan.com.tw/bulletinview.jsp?bulletinid=46180)
    , [新台灣新聞周刊](../Page/新台灣新聞周刊.md "wikilink"), 2001-10-25

9.  [活佛轉世要共產黨批准](http://www.open.com.hk/old_version/0710p64.html),
    [開放雜誌](../Page/開放雜誌.md "wikilink"), 2009-9-25

10. [藏人拒絕迎請「假班禪」](http://www.tibet.org.tw/news_detail.php?news_id=2133),
    [西藏之聲](../Page/西藏之聲.md "wikilink"), 2011-8-2

11. [中共試圖利用假班禪來控制藏人遭指責](http://www.tibet.org.tw/news_detail.php?news_id=2197),
    [西藏之聲](../Page/西藏之聲.md "wikilink"), 2011-8-25

12. <http://www.appledaily.com.tw/appledaily/article/international/20051002/2097778/>

13.

14.
15. [中国称达赖指定的班禅尼玛非政治犯生活正常](http://www.stnn.cc/global/china/t20060428_203002.html)

16.

17.

18.

19. [班禪喇嘛父母被軟禁在家](http://vot.org/cn/%E7%8F%AD%E7%A6%85%E5%96%87%E5%98%9B%E7%88%B6%E6%AF%8D%E8%A2%AB%E8%BD%AF%E7%A6%81%E5%9C%A8%E5%AE%B6%E4%B9%A1/),
    [西藏之聲](../Page/西藏之聲.md "wikilink"), 2013-4-24

20.
21.
22. [尼玛次仁：达赖认定的“班禅”是无效的非法的](http://news.enorth.com.cn/system/2007/07/29/001793509.shtml)

23. [藏人行政中央：请参与5月17日国际声援西藏日](http://vot.org/cn/%E8%97%8F%E4%BA%BA%E8%A1%8C%E6%94%BF%E4%B8%AD%E5%A4%AE%EF%BC%9A%E8%AF%B7%E5%8F%82%E4%B8%8E5%E6%9C%8817%E6%97%A5%E5%9B%BD%E9%99%85%E5%A3%B0%E6%8F%B4%E8%A5%BF%E8%97%8F%E6%97%A5/),
    [西藏之聲](../Page/西藏之聲.md "wikilink"), 2013-5-1

24.
25.

26.

27. [達賴喇嘛慶生中國民眾關注](http://www.rfa.org/mandarin/yataibaodao/da-07072010144539.html),
    [自由亞洲電台](../Page/自由亞洲電台.md "wikilink"), 2010-7-7

28.
29. [阿嘉仁波切揭露指定班禅喇嘛「金瓶掣籤」丑闻](http://www.rfa.org/mandarin/yataibaodao/gangtai/all-09132013095215.html),
    [自由亞洲電台](../Page/自由亞洲電台.md "wikilink"), 2013-9-13

30.