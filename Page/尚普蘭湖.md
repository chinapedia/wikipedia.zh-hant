**尚普蘭湖**（**Lake
Champlain**）是一個位於[北美洲的淡水湖](../Page/北美洲.md "wikilink")，主要位於[美國境內](../Page/美國.md "wikilink")（[佛蒙特州與](../Page/佛蒙特州.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")），但有一部份跨越了美國與[加拿大的邊界](../Page/加拿大.md "wikilink")。

## 地理

尚普蘭湖位於佛蒙特州的[綠山山脈與紐約州的](../Page/綠山山脈.md "wikilink")[阿第倫達克山脈之間的尚普蘭河谷中](../Page/阿第倫達克山脈.md "wikilink")，往北經由里舍盧河（[Richelieu
River](../Page/:en:Richelieu_River.md "wikilink")）在[蒙特婁附近注入](../Page/蒙特婁.md "wikilink")[聖勞倫斯河](../Page/聖勞倫斯河.md "wikilink")。

尚普蘭湖主要位於鄉村地帶，但是距離[波士頓-華盛頓城市帶只有約三小時車程](../Page/波士頓-華盛頓城市帶.md "wikilink")。湖畔最大的城市是[佛蒙特州的](../Page/佛蒙特州.md "wikilink")[伯靈頓](../Page/伯靈頓_\(佛蒙特州\).md "wikilink")（2000年時有38,889人）。

## 神秘生物

[尚普](../Page/尚普.md "wikilink")（Champ、Champy）是一種傳聞中生活在尚普蘭湖的[水怪](../Page/水怪.md "wikilink")，雖然已經有超過300件目擊案例被記錄下來，但目前為止沒有任何科學證據支持牠的存在。

[Category:加拿大湖泊](../Category/加拿大湖泊.md "wikilink")
[Category:美國湖泊](../Category/美國湖泊.md "wikilink")
[Category:魁北克省地理](../Category/魁北克省地理.md "wikilink")
[Category:紐約州地理](../Category/紐約州地理.md "wikilink")
[Category:佛蒙特州地理](../Category/佛蒙特州地理.md "wikilink")
[Category:紐約州邊界](../Category/紐約州邊界.md "wikilink")
[Category:佛蒙特州邊界](../Category/佛蒙特州邊界.md "wikilink")
[Category:美加邊界](../Category/美加邊界.md "wikilink")
[Category:淡水湖](../Category/淡水湖.md "wikilink")
[Category:北美洲跨國湖泊](../Category/北美洲跨國湖泊.md "wikilink")