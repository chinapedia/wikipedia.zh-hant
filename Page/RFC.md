**请求意见稿**（，縮寫：**RFC**）是由[互联网工程任务组](../Page/互联网工程任务组.md "wikilink")（IETF）发布的一系列[備忘錄](../Page/備忘錄.md "wikilink")。文件收集了有關[網際網路相關資訊](../Page/網際網路.md "wikilink")，以及[UNIX和網際網路](../Page/UNIX.md "wikilink")[社群的軟體文件](../Page/社群.md "wikilink")，以編號排定。目前RFC文件是由[網際網路協會](../Page/網際網路協會.md "wikilink")（ISOC）[贊助發行](../Page/贊助.md "wikilink")。

RFC始于1969年，由當時就讀加州大学洛杉矶分校（UCLA）的[斯蒂芬·克罗克](../Page/斯蒂芬·克罗克.md "wikilink")（Stephen
D.
Crocker）用来记录有关[ARPANET开发的非正式文档](../Page/ARPANET.md "wikilink")，他是第一份RFC文档的撰寫者。最终演变为用来记录互联网规范、协议、过程等的标准文件。基本的網際網路通訊協定都有在RFC文件內詳細說明。RFC文件還額外加入許多的論題在標準內，例如對於網際網路新開發的協定及發展中所有的記錄。

## RFC的历史

RFC文件格式最初作为[ARPA网计划的基础起源于](../Page/ARPA网.md "wikilink")1969年。如今，它已经成为IETF、[Internet
Architecture
Board](../Page/Internet_Architecture_Board.md "wikilink")（IAB）还有其他一些主要的公共网络研究社区的正式出版物发布途径。

在RFC诞生之时，[互联网还不存在](../Page/互联网.md "wikilink")，只有4大研究中心的4台计算机连接成的原始网络：[加州大学洛杉矶分校](../Page/加州大学洛杉矶分校.md "wikilink")，[斯坦福研究所](../Page/斯坦福大学.md "wikilink")，[加州大学圣塔芭芭拉分校](../Page/圣塔芭芭拉加利福尼亚大学.md "wikilink")，和盐湖城的[犹他大学](../Page/犹他大学.md "wikilink")。\[1\]最初的RFC作者使用打字机撰写文档，并在[美国国防部](../Page/美国国防部.md "wikilink")[国防前沿研究项目署](../Page/国防前沿研究项目署.md "wikilink")（ARPA）研究成员之间传阅。1969年12月，他们开始通过ARPANET途径来发布新的RFC文档。第一份在1969年4月7日公开发表的[RFC
1](../Page/rfc:1.md "wikilink")。当初克罗克为了避免打扰他的[室友](../Page/室友.md "wikilink")，是在[浴室里完成这篇文档的](../Page/浴室.md "wikilink")。

在1970年代，很多后来的RFC文档同样来自UCLA，这不仅得益于UCLA的学术质量，同时也因为UCLA是ARPANET第一批[Interface
Message
Processors](../Page/Interface_Message_Processor.md "wikilink")（IMPs）成员之一。

由[Douglas
Engelbart领导的](../Page/Douglas_Engelbart.md "wikilink")，位于[Stanford
Research
Institute的](../Page/SRI_International.md "wikilink")[Augmentation
Research
Center](../Page/Augmentation_Research_Center.md "wikilink")（ARC）是四个最初的ARPANET[结点之一](../Page/Node_\(networking\).md "wikilink")，也是最初的[Network
Information
Centre](../Page/Network_Information_Centre.md "wikilink")，同时被[社会学家](../Page/社会学家.md "wikilink")[Thierry
Bardini记录为早期大量RFC文档的发源地](../Page/Thierry_Bardini.md "wikilink")。

从1969年到1998年，[Jon
Postel一直担任RFC文档的](../Page/Jon_Postel.md "wikilink")[编辑职务](../Page/编辑.md "wikilink")。随着[美国](../Page/美国.md "wikilink")[政府赞助合同的到期](../Page/政府.md "wikilink")，Internet
Society（代表IETF），和[南加州大学](../Page/南加州大学.md "wikilink")（USC）[Information
Sciences
Institute的网络部门合作](../Page/Information_Sciences_Institute.md "wikilink")，（在IAB领导下）负责RFC文档的起草和发布工作。Jon
Postel继续担任RFC编辑直到去世。随后，由[Bob
Braden接任整个项目的领导职务](../Page/Bob_Braden.md "wikilink")，同时[Joyce
Reynolds继续在团队中的担任职务](../Page/Joyce_Reynolds.md "wikilink")。

庆祝RFC的30周年的RFC文件是[RFC 2555](../Page/rfc:2555.md "wikilink")。

## 中文地区的贡献

1996年3月，[清华大学提交的适应不同国家和地区中文编码的汉字统一传输标准被IETF通过为](../Page/清华大学.md "wikilink")[RFC
1922](../Page/rfc:1922.md "wikilink")，成为中国大陆第一个被认可为RFC文件的提交协议。

## RFC文件的架構

RFC文件只有新增，不會有取消或中途停止發行的情形。但是對於同一主題而言，新的RFC文件可以聲明取代舊的RFC文件。RFC文件是純[ASCII文字檔格式](../Page/ASCII.md "wikilink")，可由電腦程式自動轉檔成其他檔案格式。RFC文件有封面、目錄及頁首頁尾和頁碼。RFC的章節是數字標示，但數字的小數點後不補零，例如4.9的順序就在4.10前面，但9的前面並不補零。RFC
1000這份文件就是RFC的指南。

## RFC文件的產生

RFC文件是由Internet
Society審核後給定編號並發行。雖然經過審核，但RFC也並非全部嚴肅而生硬的技術文件，偶有[惡搞之作出現](../Page/惡搞RFC.md "wikilink")，尤其是4月1日[愚人節所發行的](../Page/愚人節.md "wikilink")[惡搞RFC](../Page/惡搞RFC.md "wikilink")，例如[RFC
1606](../Page/rfc:1606.md "wikilink"): A Historical Perspective On The
Usage Of IP Version 9（参见[IPv9](../Page/IPv9.md "wikilink")）、RFC
2324：「[超文字咖啡壺控制協定](../Page/HTCPCP.md "wikilink")」（*Hyper Text
Coffee Pot Control
Protocol*，乍有其事的寫了**HTCPCP**這樣看起來很專業的術語縮寫字）。以及如前面所提到紀念RFC的30周年慶的RFC文件。

## 常见互联网协议的RFC编号

  - [IP](../Page/IP.md "wikilink")：791
  - [TCP](../Page/TCP.md "wikilink")：793
  - [UDP](../Page/UDP.md "wikilink")：768
  - [ICMP](../Page/ICMP.md "wikilink")：792
  - [FTP](../Page/FTP.md "wikilink")：959
  - [SOCK5](../Page/SOCKS#SOCKS5.md "wikilink")：1928
  - [CHAP](../Page/CHAP.md "wikilink")：1994
  - [SMTP](../Page/SMTP.md "wikilink")：2821 5321
  - [POP3](../Page/POP3.md "wikilink")：1957
  - [NTP](../Page/NTP.md "wikilink")：1305
  - [HTTP](../Page/HTTP.md "wikilink")1.1：2616
  - [IMAP](../Page/IMAP.md "wikilink")：2060
  - [PPP](../Page/PPP.md "wikilink")：1661－1663
  - [DHCP](../Page/DHCP.md "wikilink")：2131
  - [OSPF](../Page/OSPF.md "wikilink")：2328
  - [IPSec](../Page/IPSec.md "wikilink")：2401－2412
  - [IPv6](../Page/IPv6.md "wikilink")：2460
  - [SIP](../Page/SIP.md "wikilink")：3261
  - [RTP](../Page/RTP.md "wikilink")：3550
  - [RADIUS](../Page/RADIUS.md "wikilink")：3575，3576，3579，3580
  - [L2TP](../Page/L2TP.md "wikilink")：3931

## 参考文献

## 外部連結

  - [IETF RFC](http://www.ietf.org/rfc.html)
  - [RFC Editor](http://www.rfc-editor.org/)

[Category:互联网标准](../Category/互联网标准.md "wikilink")

1.  [Solidot | Internet
    RFC诞生40周年](http://internet.solidot.org/article.pl?sid=09/04/08/0636241)