**顾纪筠**（，），籍貫[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")，綽號「**顧老闆**」，[香港](../Page/香港.md "wikilink")[主持人](../Page/主持人.md "wikilink")，曾任[香港足總市場及傳訊委員會成員](../Page/香港足總.md "wikilink")。

## 生平

其名字「紀筠」為「紀溫」的諧音，因爲她出生時正值[颱風溫黛吹襲香港](../Page/颱風溫黛_\(1962年\).md "wikilink")，家人藉此來紀念溫黛。

年幼時居於[香港仔](../Page/香港仔.md "wikilink")[田灣邨](../Page/田灣邨.md "wikilink")[徙置大廈](../Page/徙置大廈.md "wikilink")，曾在該屋邨一所[天台小學讀書](../Page/天台小學.md "wikilink")，在中學畢業後加盟[麗的電視工作](../Page/麗的電視.md "wikilink")，主要任主持。一直到麗的電視轉為[亞洲電視](../Page/亞洲電視.md "wikilink")，她一直都是於亞視主持節目。除此之外，也曾演出少量亞視劇集。

她在1990年代末於[無綫電視工作](../Page/無綫電視.md "wikilink")，開始為人所熟悉，多於大型節目中與[曾志偉等拍檔擔任主持工作](../Page/曾志偉.md "wikilink")。近年主要在商界發展，並有出版[散文集](../Page/散文.md "wikilink")。

## 演出作品

### 電視劇（[麗的電視](../Page/麗的電視.md "wikilink")／[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 1980年：[驟雨中的陽光續集](../Page/驟雨中的陽光續集.md "wikilink") 飾 Sue
  - 1981年：[浴血太平山](../Page/浴血太平山.md "wikilink") 飾 周亞蓮
  - 1981年：[再會太平山](../Page/再會太平山.md "wikilink") 飾 周亞蓮
  - 1981年：[阿拉擔梯](../Page/阿拉擔梯.md "wikilink")
  - 1982年：[愛情跑道](../Page/愛情跑道.md "wikilink")
  - 1982年：[天梯](../Page/天梯_\(麗的電視劇\).md "wikilink")
  - 1982年：[凹凸神探](../Page/凹凸神探.md "wikilink") 飾 Angel
  - 1989年：[皇家檔案II之折翼天使](../Page/皇家檔案II.md "wikilink") 飾 Amy

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

  - **1999年**
      - [雙面伊人](../Page/雙面伊人.md "wikilink") 飾 Dr. Ho
  - **2000年**
      - [妙手仁心II](../Page/妙手仁心II.md "wikilink") 飾 溫美君（Fion）
  - **2002年**
      - [洛神](../Page/洛神_\(2002年電視劇\).md "wikilink") 飾
        [劉惜惜](../Page/劉姬_\(曹操\).md "wikilink")
  - **2005年**
      - [學警雄心](../Page/學警雄心.md "wikilink") 飾
        [高慧君](../Page/高慧君.md "wikilink")
  - **2007年**
      - [廉政行動2007](../Page/廉政行動2007.md "wikilink")（單元三：億萬信用）飾 Nora
        Cheung

### 電影

  - **1983年**
      - [瘋血](../Page/瘋血.md "wikilink")
  - **1996年**
      - [四個32A和一個香蕉少年](../Page/四個32A和一個香蕉少年.md "wikilink")

### 節目主持

  - 1980年：[麗的快訊](../Page/麗的快訊.md "wikilink") （麗的電視）
  - 1983年：勁運縱橫 （亞洲電視）
  - 1988年：漢城奧運會（亞洲電視）
  - 1989年: [亞視煙花照萬家](../Page/亞視煙花照萬家.md "wikilink")(亞洲電視)
  - 1989年：[晨早直播室](../Page/晨早直播室.md "wikilink") （亞洲電視）
  - 1992年：巴塞隆拿奧運會（亞洲電視）
  - 1996年：亞特蘭大奧運會（亞洲電視）
  - 1999年：天地最前線 （無線電視）
  - 1999年：活著就是精彩 （無線電視）
  - 1999-2004年：[萬千星輝賀台慶](../Page/萬千星輝賀台慶.md "wikilink") （無線電視）
  - 1999年﹕跨世紀錄香港同心齊共創 （無線電視）
  - 2000年：奧運第一現場 （無線電視）
  - 2001-2003年：[歡樂滿東華](../Page/歡樂滿東華.md "wikilink") （無線電視）
  - 2004年：[獅子山下念霑叔](../Page/獅子山下念霑叔.md "wikilink") （無線電視）
  - 2006年：[貝沙灣：生活、藝術、家](../Page/貝沙灣：生活、藝術、家.md "wikilink") （無線電視）
  - 2008年：[今日法庭](../Page/今日法庭.md "wikilink") （亞洲電視）
  - 2008年：第二十九屆奧林匹克運動會開幕式 （亞洲電視）
  - 2008年：為中國加油 （亞洲電視）
  - 2008年：奧運巨星逐個捉 （亞洲電視）
  - 2008年：第二十九屆奧林匹克運動會閉幕式 （亞洲電視）
  - 2010年：[名人廚房](../Page/名人廚房.md "wikilink") 第8集 （無線電視）
  - 2012年：倫敦奧運開幕大派對 （無綫電視）(顧紀筠代表亞洲電視出席)

## 參見

  - [香港演藝人協會](../Page/香港演藝人協會.md "wikilink")

## 參考

<references/>

[KEI](../Page/category:顧姓.md "wikilink")

[Category:上海人](../Category/上海人.md "wikilink")
[Category:香港电视女演員](../Category/香港电视女演員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港女性作家](../Category/香港女性作家.md "wikilink")
[Category:香港佛教徒](../Category/香港佛教徒.md "wikilink")
[Category:前亞洲電視女藝員](../Category/前亞洲電視女藝員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前麗的電視藝員](../Category/前麗的電視藝員.md "wikilink")