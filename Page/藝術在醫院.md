[HK_Art_in_Hospital_logo.png](https://zh.wikipedia.org/wiki/File:HK_Art_in_Hospital_logo.png "fig:HK_Art_in_Hospital_logo.png")
**藝術在醫院**（）是一[香港藝術團體](../Page/香港.md "wikilink")，在1994年成立，是一個透過藝術家及義工共同合作，在醫院內舉辦藝術活動，令醫院環境更具生氣的非牟利社群藝術活動，於2003年正式註冊為非牟利慈善團體。除了基本的[壁畫創作和藝術工作坊外](../Page/壁畫.md "wikilink")，還有講故事計劃、故事書出版、海外服務及醫院畫廊等。希望以不同形式的藝術活動，達至全人關懷的醫療理念。「藝術在醫院」的辦事處位於香港[灣仔港灣道](../Page/灣仔.md "wikilink")2號[香港藝術中心](../Page/香港藝術中心.md "wikilink")303室。

## 源起與成立

前[香港藝術中心展覽總監](../Page/香港藝術中心.md "wikilink")[何慶基於](../Page/何慶基.md "wikilink")1990年發現得了[鼻咽癌](../Page/鼻咽癌.md "wikilink")，在政府醫院裏接受電療，由於電療一定要在地下室裏進行，環境很差，四面墻壁殘舊，燈光蒼白，而且沒有窗戶，感覺死氣沉沉，如同置身在一個絕望及痛苦的環境。治療期間，何慶基對自己許諾，病癒出院後一定要改變這樣的環境。

1994年由[梁以瑚](../Page/梁以瑚.md "wikilink")\[1\]和何慶基連同過百名藝術及設計系學生、藝術愛好者、熱心人士，一同為[威爾斯親王醫院的放射治療部繪畫上色彩諧悅目的壁畫](../Page/威爾斯親王醫院.md "wikilink")。同年4月由香港藝術中心和[園泉](../Page/園泉.md "wikilink")（香港基督徒藝術家團契）共同協助下，「藝術在醫院」正式成立，開始到各間醫院進行壁畫創作及各類藝術活動。2003年6月「藝術在醫院」正式註冊為本港[慈善團體](../Page/慈善團體.md "wikilink")。「藝術在醫院」運作經費由香港藝術中心、[凱瑟克基金及利希慎基金資助](../Page/凱瑟克基金.md "wikilink")。

## 服務

**壁畫活動**是「藝術在醫院」的主流服務，以色彩將醫院環境美化，讓病者減輕痛楚和焦慮。「藝術在醫院」創作的壁畫約有47個，分佈在全港不同的醫院和護養院。每個計劃，都會因應不同的環境而作出適當的調整。

藝術在醫院又舉行不同形式的**藝術工作坊**，與病者一同創作，發揮創意的同時，也為病者帶來歡樂及重拾自信\[2\]。

自1999年開始在醫院推行**講故事計劃**，並印製了一系列故事書，免費派發到不同醫院，供患病的小朋友閱讀，配合活動。已出版的故事書系列，包括《痛楚、痛楚，快快飛走\!》、《轉轉轉和飄飄飄》、《鞋子的故事》和《太陽寶寶》等。

將醫院藝術的概念推廣到[亞洲不同地區](../Page/亞洲.md "wikilink")。曾經舉辦海外計劃包括[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[印度](../Page/印度.md "wikilink")、[柬埔寨等](../Page/柬埔寨.md "wikilink")，令香港以外地區的病患者同樣受惠。

於2002年起推行的試驗性計劃，透過藝術為醫院增添生氣、把醫院長長的走廊化身成展覽藝術品的空間\[3\]，名為**醫院畫廊**。藝術家的作品可以令氣氛改變，安慰病者和家人的情緒，紓緩醫護人員的壓力。同時在醫院辦展覽可讓醫院更貼近社群，並提供另類展覽空間予藝術家。

## 大·快·人心

「藝術在醫院」與[藝術推廣辦事處攜手協作大型社群藝術計劃](../Page/藝術推廣辦事處.md "wikilink")，目的是透過一連串的藝術活鼓勵社群投入參與及分嘗藝術創作的樂趣，並向大眾傳遞關懷的訊息。展出的作品由不同醫院、療養院、康復中心等病人或康復人士創作，作巡迴展出及送贈到不同的醫院和相關機構\[4\]。

「大·快·人心」壓軸是名為《愛拼大行動》的活動，歌星[鄭秀文獲委任為活動的榮譽大使](../Page/鄭秀文.md "wikilink")\[5\]，是社群藝術計劃的一部分。活動從2006年6月開始，透過所舉辦的106個工作坊，逾100家的醫院、康復中心及學校的參與，用愛心繪製了1500多幅色彩斑斕的圖畫。於2007年1月21日上千名來自不同機構的人士雲集[跑馬地馬場](../Page/跑馬地馬場.md "wikilink")，由[台灣藝術家](../Page/台灣.md "wikilink")[洪易設計拼畫藍圖](../Page/洪易.md "wikilink")，合力將該1500多幅2呎x2呎帆布圖畫拼成了一幅面積達500[平方米的巨畫](../Page/平方米.md "wikilink")\[6\]。

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [藝術在醫院](http://aih.org.hk/c/c_intro.htm)
  - [「大·快·人·心」](http://aih.org.hk/c/ipaint.html)
  - [何慶基](https://web.archive.org/web/20071104163345/http://web.hku.hk:8400/~hkaa/hkaa/artists.php?artist_id=021)
  - [梁以瑚](https://web.archive.org/web/20071024033540/http://web.hku.hk:8400/~hkaa/hkaa/artists.php?artist_id=105)

[Category:香港慈善團體](../Category/香港慈善團體.md "wikilink")
[Category:香港公益金會員社會福利機構](../Category/香港公益金會員社會福利機構.md "wikilink")

1.  [醫療故事：將冰冷醫院變成藝術館](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060828&sec_id=4104&subsec_id=3626368&art_id=6259737)
2.  [藝術助病人釋放內心世界](http://www.metrohk.com.hk/news.php?startDate=22012007&newscat=8)
3.  [當藝術融入了醫院——醫院畫廊](http://hknews.syc.edu.hk/index.php/%E7%95%B6%E8%97%9D%E8%A1%93%E8%9E%8D%E5%85%A5%E4%BA%86%E9%86%AB%E9%99%A2%EF%BC%8D%EF%BC%8D%E9%86%AB%E9%99%A2%E7%95%AB%E5%BB%8A)
4.  [「大‧快‧人心」藝術計劃宣揚關愛訊息](http://www.info.gov.hk/gia/general/200702/14/P200702140179.htm)
5.  [香港举行《艺术在医院》压轴活动「爱拼大行动」](http://www3.chinesenewsnet.com/gb/NewsPics/zhongxin/zxs_P20070121068.html)
6.  [藝術治療釋放病人情緒](http://health.atnext.com/index.php?fuseaction=Article.ListArticle&sec_id=6349036&iss_id=20070119&art_id=6731540)