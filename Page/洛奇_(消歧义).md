## 電影

《**[洛奇](../Page/洛奇.md "wikilink")**》是一部1976年的美國電影，由[史泰龍飾演](../Page/史泰龍.md "wikilink")[洛奇·巴保亞為主角](../Page/洛奇·巴保亞.md "wikilink")，是一位來自[費城的無名](../Page/費城.md "wikilink")[拳手](../Page/拳手.md "wikilink")。

## 人物

  - [:en:Rocky
    Anderson](../Page/:en:Rocky_Anderson.md "wikilink")，現任[美國](../Page/美國.md "wikilink")[猶他州](../Page/猶他州.md "wikilink")[鹽湖城市長](../Page/鹽湖城.md "wikilink")
  - [Rocky Bridges](../Page/Rocky_Bridges.md "wikilink"), former major
    league baseball player and minor league manager
  - [Rocky Colavito](../Page/Rocky_Colavito.md "wikilink"), former major
    league baseball player
  - [Roy L. "Rocky" Dennis](../Page/Roy_L._Dennis.md "wikilink"),
    suffered from [craniodiaphyseal
    dysplasia](../Page/craniodiaphyseal_dysplasia.md "wikilink")
  - [Rocky Elsom](../Page/Rocky_Elsom.md "wikilink"), Australian rugby
    player
  - [Rocky Gray](../Page/Rocky_Gray.md "wikilink"), drummer of the
    popular Grammy Award winning band, Evanescence
  - [Rocky Graziano](../Page/Rocky_Graziano.md "wikilink"), American
    boxer
  - [Rocky Johnson](../Page/Rocky_Johnson.md "wikilink"), Pro wrestler
    and The Rock's father
  - [Rocky Marciano](../Page/Rocky_Marciano.md "wikilink"), American
    boxer
  - [Rocky Marquette](../Page/Rocky_Marquette.md "wikilink"), American
    actor
  - [Rocky Nelson](../Page/Rocky_Nelson.md "wikilink"), former major
    league baseball player
  - [Rockey Vaccarella](../Page/Rockey_Vaccarella.md "wikilink"), a
    Katrina survivor/activist and former Republican candidate for a seat
    on the St. Bernard Parish commission
  - [The Rock
    (entertainer)](../Page/The_Rock_\(entertainer\).md "wikilink"), real
    name Dwayne Johnson, who once had the stage name Rocky Maivia
  - Rocky is a nickname for actress [Raquel
    Welch](../Page/Raquel_Welch.md "wikilink").
  - Rocky, name of [Nikki
    Blonsky](../Page/Nikki_Blonsky.md "wikilink")'s
    dog.[1](http://www.imdb.com/name/nm2284889/bio)

## 虛構人物

  - *[Rocky the Flying
    Squirrel](../Page/Rocky_the_Flying_Squirrel.md "wikilink")* (full
    name Rocket J. Squirrel), from the television series *[The Rocky and
    Bullwinkle
    Show](../Page/The_Rocky_and_Bullwinkle_Show.md "wikilink")*.
  - *[Rocky](../Page/Rocky_and_Mugsy.md "wikilink")*, the gangster from
    Looney Tunes.
  - [Rocky DeSantos](../Page/Rocky_DeSantos.md "wikilink"), a fictional
    character in Power Rangers.
  - [Rocky the Bull](../Page/Rocky_the_Bull.md "wikilink"), the mascot
    at the [University of South
    Florida](../Page/University_of_South_Florida.md "wikilink").
  - [Rocky Kellerman](../Page/Rocky_\(comic_strip\).md "wikilink"),
    Swedish fable cartoon character.
  - Rocky the "Snail," [Patrick
    Star](../Page/Patrick_Star.md "wikilink")'s pet in the [Spongebob
    Squarepants](../Page/Spongebob_Squarepants.md "wikilink") episode
    ["The Great Snail
    Race."](../Page/"The_Great_Snail_Race.".md "wikilink")

## 其他用法

**Rocky** may also mean:

  - 網路遊戲[瑪奇](../Page/瑪奇.md "wikilink")（Mabinogi）的中國大陸譯名。
  - [Rocky, Oklahoma](../Page/Rocky,_Oklahoma.md "wikilink"); the zip
    code is 73661
  - [Rockhampton,
    Queensland](../Page/Rockhampton,_Queensland.md "wikilink")
  - [Rocky Rococo](../Page/Rocky_Rococo.md "wikilink"), a North-American
    fast food chain
  - [Rocky Raccoon](../Page/Rocky_Raccoon.md "wikilink"), a Beatles song
  - [Daihatsu Rocky](../Page/Daihatsu_Rocky.md "wikilink"), an SUV.
  - *[The Rocky Horror Picture
    Show](../Page/The_Rocky_Horror_Picture_Show.md "wikilink")*, a 1975
    film starring Tim Curry.
  - *[Rocky](../Page/Rocky_\(1981_film\).md "wikilink")*, a 1981 film
    starring [Sanjay Dutt](../Page/Sanjay_Dutt.md "wikilink").
  - *[Rocky](../Page/Rocky_\(comic_strip\).md "wikilink")*, a Swedish
    comic strip made by Martin Kellerman.
  - [Rocky road](../Page/Rocky_road.md "wikilink") ice cream
  - Rocky, slang for [hashish](../Page/hashish.md "wikilink")
  - [Colorado Rockies](../Page/Colorado_Rockies.md "wikilink"), a Major
    League Baseball team.
  - "[Rocky](../Page/Rocky_\(song\).md "wikilink")", a song popularized
    in 1975 by [Austin
    Roberts](../Page/Austin_Roberts_\(singer\).md "wikilink") and
    [Dickey Lee](../Page/Dickey_Lee.md "wikilink").
  - "Rocky", a track on the album [Light &
    Shade](../Page/Light_&_Shade.md "wikilink") by [Mike
    Oldfield](../Page/Mike_Oldfield.md "wikilink").
  - "Rocky" was the name of George Harrison's Guitar around the time of
    the [Magical Mystery
    Tour](../Page/Magical_Mystery_Tour.md "wikilink") film.