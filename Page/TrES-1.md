**TrES-1**是一個環繞著[GSC
02652-01324的](../Page/GSC_02652-01324.md "wikilink")[太陽系外行星](../Page/太陽系外行星.md "wikilink")，他跟[太陽系裡的](../Page/太陽系.md "wikilink")[木星一樣](../Page/木星.md "wikilink")，都是[氣體行星](../Page/氣體行星.md "wikilink")，而且都適用[氫和](../Page/氫.md "wikilink")[氦組成的](../Page/氦.md "wikilink")，但跟木星不一樣的地方是它離它的母恆星很近，表面溫度很高，因此天存學家把它規類於“[熱木星](../Page/熱木星.md "wikilink")”

## 參見

  - [GSC 02652-01324](../Page/GSC_02652-01324.md "wikilink")

[Category:太陽系外行星](../Category/太陽系外行星.md "wikilink")