**松崗科技股份有限公司**（[英語譯名](../Page/英語.md "wikilink")：**Unalis**），是[台灣一家從事](../Page/台灣.md "wikilink")[資訊](../Page/資訊.md "wikilink")[服務業的](../Page/服務業.md "wikilink")[公司](../Page/公司.md "wikilink")，於1976年12月成立。初名「松崗電腦圖書資料股份有限公司」，以推廣[資訊教育為宗旨投入台灣資訊業界](../Page/資訊教育.md "wikilink")，成為台灣第一家的電腦圖書資料公司，並曾出版過《CompuLife
Today資訊生活雜誌》；2002年改為現名，負責人為鄭宏森，以[線上遊戲代理與](../Page/線上遊戲.md "wikilink")[PC遊戲](../Page/电脑游戏.md "wikilink")[翻譯為主業](../Page/翻譯.md "wikilink")。在中国大陆最早以其[DOS时代的](../Page/DOS.md "wikilink")《快快乐乐学电脑》系列教学软体而闻名。根據經濟部─公司資料查詢網站資料，松崗科技已於2018年11月20日解散。

## 歷史

[缩略图](https://zh.wikipedia.org/wiki/File:KMall_sign_on_Asia_Plaza_Building_20101010.jpg "fig:缩略图")

  - 1976年12月，松崗電腦圖書資料股份有限公司創立。
  - 1993年，松崗電腦圖書資料公司代理第一款電腦遊戲《[X戰機](../Page/X戰機.md "wikilink")》（*X-Wing*）。
  - 2000年，松崗電腦圖書資料公司代理電腦遊戲《[交通巨人](../Page/交通巨人.md "wikilink")》（*Traffic
    Giant*）。\[1\]、《》（*Ultimate Golf*）\[2\]
  - 2001年，松崗電腦圖書資料公司代理電腦遊戲《[絕對武力](../Page/絕對武力.md "wikilink")》（*Counter-Strike*）\[3\]、《》\[4\]、《[暗黑破壞神Ⅱ](../Page/暗黑破壞神II.md "wikilink")》（*Diablo
    Ⅱ*）\[5\]。二月，與發行商[Codemasters完成合約簽訂](../Page/Codemasters.md "wikilink")，將代理《[閃擊點行動](../Page/閃擊點行動：冷戰危機.md "wikilink")》、《》、《》\[6\]。八月，將休閒軟體銷售部門獨立為[子公司](../Page/子公司.md "wikilink")「桐崗科技」\[7\]。
  - 2002年，松崗電腦圖書資料公司改名為松崗科技股份有限公司，代理電腦遊戲《[魔獸爭霸3](../Page/魔獸爭霸3.md "wikilink")》（*Warcraft
    Ⅲ: Reign of
    Chaos*）與[線上遊戲](../Page/線上遊戲.md "wikilink")《[魔劍Online](../Page/魔劍Online.md "wikilink")》（*Shadowbane*）；同年，松崗電腦圖書資料公司的圖書與商用軟體部門被[文魁資訊買下](../Page/文魁資訊.md "wikilink")。
  - 2004年，松崗科技成立[電子商務部門與海外部門](../Page/電子商務.md "wikilink")。
  - 2005年2月25日，松崗科技、[中華電信與](../Page/中華電信.md "wikilink")[以色列](../Page/以色列.md "wikilink")[Exent
    Technology共同在](../Page/Exent_Technology.md "wikilink")[台北國際電玩展發表](../Page/台北國際電玩展.md "wikilink")[遊戲隨選下載](../Page/遊戲隨選下載.md "wikilink")（Game
    On Demand；GOD）平台《數-{}-位娛玩》（*OTTOPLAY*）。\[8\]
  - 2010年12月，松崗科技推出網路遊戲整合平台《Play168》
  - 2011年，松崗科技成立[社群網站](../Page/社群網站.md "wikilink")《APP01》
  - 2015年11月25日，宣布旗下服務app01+/換多啦/BonusGift將於年底[終止服務](http://www.app01.com.tw/paper/b7c22ae7-f80d-46ad-8c0f-c403aa9f45a1)。會員因大量點數無法兌換，引發對該公司誠信問題之爭議。
  - 2018年4月16日，宣布將於同年6月1日終止營運「極速快車手-電腦版」(DriftCity)。

## 旗下網路遊戲

  - \[<https://web.archive.org/web/20131217221452/http://dco.play168.com.tw/index.aspx>
    極速快車手online

## 已營運網路遊戲

  -
  -
## 事件

### 2001年網咖授權爭議

2001年5月22日，松崗電腦圖書資料公司與其子公司[柏崗科技連同](../Page/柏崗科技.md "wikilink")[警察與](../Page/警察.md "wikilink")[律師至台灣](../Page/律師.md "wikilink")[連鎖](../Page/連鎖.md "wikilink")[網咖](../Page/網咖.md "wikilink")「[戰略高手](../Page/戰略高手.md "wikilink")」直營店，蒐證取締未經[授權的](../Page/授權.md "wikilink")《絕對武力》。
[缩略图](https://zh.wikipedia.org/wiki/File:PineNet_Entertainment_Union_light_box_20110330.jpg "fig:缩略图")
2001年5月23日，戰略高手召開[記者會指控](../Page/記者會.md "wikilink")，除《絕對武力》外，由於松崗與柏崗科技強迫搭售其他不暢銷與未上市產品，戰略高手將控告柏崗科技違反《[公平交易法](../Page/公平交易法.md "wikilink")》；戰略高手副董事長[吳文中宣稱已獲得](../Page/吳文中.md "wikilink")《絕對武力》美國發行商[Harvas
Interactive的證實](../Page/Harvas_Interactive.md "wikilink")，由於松崗與柏崗的不合理[利潤](../Page/利潤.md "wikilink")，戰略高手不需理會其控告。吳文中又說，柏崗從[松網科技承接松崗旗下遊戲的網咖授權業務之後](../Page/松網科技.md "wikilink")，柏崗完全否定戰略高手已經從松網科技取得《[銀河生死鬥](../Page/銀河生死鬥.md "wikilink")》（*Tribes*）授權的合約內容，並要求戰略高手重新付費取得授權。同日，柏崗宣稱，柏崗已多次與[華彩軟體及戰略高手洽談軟體授權業務](../Page/華彩軟體.md "wikilink")，但沒有得到善意回應，而且戰略高手從未取得《絕對武力》的合法授權，被蒐證取締的戰略高手直營店店長也坦承其《絕對武力》軟體是從[網站](../Page/網站.md "wikilink")[下載而來](../Page/下載.md "wikilink")，已構成[侵權行為](../Page/侵權行為.md "wikilink")。柏崗也宣稱，正式合約已載明授權方案有兩種，即「單項產品授權」與「合購產品特惠授權」，後者的產品都是排行榜前十名的知名遊戲如《[暗黑破壞神系列](../Page/暗黑破壞神系列.md "wikilink")》與《[戰慄時空系列](../Page/戰慄時空系列.md "wikilink")》。

## 參考資料

## 外部連結

  - [松崗科技](https://www.unalis.com.tw/)

  - [PC翻譯遊戲列表](https://www.unalis.com.tw/PCGame/PCGameList.aspx)

  -
[Category:臺灣電子遊戲公司](../Category/臺灣電子遊戲公司.md "wikilink")
[Category:網路遊戲運營商](../Category/網路遊戲運營商.md "wikilink")
[Category:1976年開業電子遊戲公司](../Category/1976年開業電子遊戲公司.md "wikilink")
[Category:總部位於臺北市中山區的工商業機構](../Category/總部位於臺北市中山區的工商業機構.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  [松崗「OTTOPLAY」遊戲平台登場](http://gnn.gamer.com.tw/5/19035.html)