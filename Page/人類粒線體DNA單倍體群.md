[World_map_of_prehistoric_human_migrations.jpg](https://zh.wikipedia.org/wiki/File:World_map_of_prehistoric_human_migrations.jpg "fig:World_map_of_prehistoric_human_migrations.jpg")
[Peopling_of_eurasia.jpg](https://zh.wikipedia.org/wiki/File:Peopling_of_eurasia.jpg "fig:Peopling_of_eurasia.jpg")

**人類粒線體DNA單倍體群**（**Human mitochondrial DNA
Haplogroup**）是[遺傳學上依據](../Page/遺傳學.md "wikilink")[粒線體DNA差異而定義出來的](../Page/粒線體DNA.md "wikilink")[單倍體群](../Page/單倍群.md "wikilink")。可使研究者追溯母系遺傳的人類起源，粒線體研究顯示[人類是起源於非洲地區](../Page/人類.md "wikilink")。

## 线性透视

此进化树来自Van Oven 2009 tree\[1\]和随后发表的研究报告是基于此进化树。

  - **[L](../Page/单倍型类群_L_\(mtDNA\).md "wikilink")**（[线粒体夏娃](../Page/线粒体夏娃.md "wikilink")）
      - **[L0](../Page/单倍型类群_L0_\(mtDNA\).md "wikilink")**
      - L1-6
          - **[L1](../Page/单倍型类群_L1_\(mtDNA\).md "wikilink")**
          - L2-6
              - **[L5](../Page/单倍型类群_L5_\(mtDNA\).md "wikilink")**
              - L2'3'4'6
                  - **[L2](../Page/单倍型类群_L2_\(mtDNA\).md "wikilink")**
                  - L3'4'6
                      - **[L6](../Page/单倍型类群_L6_\(mtDNA\).md "wikilink")**
                      - L3'4
                          - **[L4](../Page/单倍型类群_L4_\(mtDNA\).md "wikilink")**
                          - **[L3](../Page/单倍型类群_L3_\(mtDNA\).md "wikilink")**
                              - **[M](../Page/单倍型类群_M_\(mtDNA\).md "wikilink")**
                                  - M8:
                                    **[CZ](../Page/单倍型类群_CZ_\(mtDNA\).md "wikilink")**
                                    (**[C](../Page/单倍型类群_C_\(mtDNA\).md "wikilink")**,
                                    **[Z](../Page/单倍型类群_Z_\(mtDNA\).md "wikilink")**)
                                  - M9:
                                    **[E](../Page/单倍型类群_E_\(mtDNA\).md "wikilink")**
                                  - M12'G:
                                    **[G](../Page/单倍型类群_G_\(mtDNA\).md "wikilink")**
                                  - M29'Q:
                                    **[Q](../Page/单倍型类群_Q_\(mtDNA\).md "wikilink")**
                                  - **[D](../Page/单倍型类群_D_\(mtDNA\).md "wikilink")**
                              - **[N](../Page/单倍型类群_N_\(mtDNA\).md "wikilink")**
                                  - [N1](../Page/单倍型类群_N1_\(mtDNA\).md "wikilink"):
                                    **[I](../Page/单倍型类群_I_\(mtDNA\).md "wikilink")**
                                  - [N2](../Page/单倍型类群_N2_\(mtDNA\).md "wikilink"):
                                    **[W](../Page/单倍型类群_W_\(mtDNA\).md "wikilink")**
                                  - N9:
                                    **[Y](../Page/单倍型类群_Y_\(mtDNA\).md "wikilink")**
                                  - **[A](../Page/单倍型类群_A_\(mtDNA\).md "wikilink")**
                                  - **[S](../Page/单倍型类群_S_\(mtDNA\).md "wikilink")**
                                  - **[X](../Page/单倍型类群_X_\(mtDNA\).md "wikilink")**
                                  - **[R](../Page/单倍型类群_R_\(mtDNA\).md "wikilink")**
                                      - [R0](../Page/单倍型类群_R0_\(mtDNA\).md "wikilink")
                                        (FMKA pre-HV)
                                          - **[HV](../Page/单倍型类群_HV_\(mtDNA\).md "wikilink")**:
                                            (**[H](../Page/单倍型类群_H_\(mtDNA\).md "wikilink")**,
                                            **[V](../Page/单倍型类群_V_\(mtDNA\).md "wikilink")**)
                                      - [pre-JT](../Page/单倍型类群_pre-JT_\(mtDNA\).md "wikilink")
                                        or R2'JT
                                          - **[JT](../Page/单倍型类群_JT_\(mtDNA\).md "wikilink")**:
                                            (**[J](../Page/单倍型类群_J_\(mtDNA\).md "wikilink")**,
                                            **[T](../Page/单倍型类群_T_\(mtDNA\).md "wikilink")**)
                                      - R9:
                                        **[F](../Page/单倍型类群_F_\(mtDNA\).md "wikilink")**
                                      - R11'B:
                                        **[B](../Page/单倍型类群_B_\(mtDNA\).md "wikilink")**
                                      - **[P](../Page/单倍型类群_P_\(mtDNA\).md "wikilink")**
                                      - **[U](../Page/单倍型类群_U_\(mtDNA\).md "wikilink")**
                                        (formerly
                                        [UK](../Page/单倍型类群_UK_\(mtDNA\).md "wikilink"))
                                          - **[U8](../Page/单倍型类群_U8_\(mtDNA\).md "wikilink")**:
                                            **[K](../Page/单倍型类群_K_\(mtDNA\).md "wikilink")**
                                  - **[O](../Page/单倍型类群_O_mtdna.md "wikilink")**

## 参看

  - [遺傳系譜學](../Page/遺傳系譜學.md "wikilink")
  - [人类Y染色体DNA单倍型类群](../Page/人类Y染色体DNA单倍型类群.md "wikilink")
  - [群体遗传学](../Page/群体遗传学.md "wikilink")

## 参考资料

<references/>

## 外部連結

  - [mtDNA .pdf map](http://www.mitomap.org/mitomap-phylogeny.pdf)
  - [Map of macro-单倍型类群
    N](http://freepages.genealogy.rootsweb.com/~jswdna/nmute.gif) by
    John S. Walden
  - [Indian maternal gene
    pool](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=16205836&query_hl=9&itool=pubmed_docsum),
    *Journal of Human Genetics*
  - [The Making of the African mtDNA
    Landscape](http://www.journals.uchicago.edu/AJHG/journal/issues/v71n5/024272/024272.html),
    *American Journal of Human Genetics*
  - [Do the Four Clades of the mtDNA 单倍型类群 L2 Evolve at Different
    Rates?](http://www.journals.uchicago.edu/AJHG/journal/issues/v69n6/013272/013272.html?erFrom=-1022570088355168662Guest),
    *American Journal of Human Genetics*
  - [The Matrilineal Ancestry of Ashkenazi Jewry: Portrait of a Recent
    Founder
    Event](https://web.archive.org/web/20071202030339/http://www.ftdna.com/pdf/43026_Doron.pdf),
    *American Journal of Human Genetics*

[Category:遺傳學](../Category/遺傳學.md "wikilink")
[Category:人類演化](../Category/人類演化.md "wikilink")

1.