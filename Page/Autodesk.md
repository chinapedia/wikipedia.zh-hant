**歐特克股份有限公司**（），為[財富雜誌票選全球前](../Page/財富雜誌.md "wikilink")1000大企業，更為雜誌內100強之公司企業均使用其產品和服務。歐特克一直致力於用戶的創意實現，並在2009年於全球突破擁有超過900萬用戶，提供制造业、傳媒暨娛樂、地理資訊空間、汽車與交通運輸業、建築、工程與施工，以及無線數據服務等領域行業的全球頂尖軟體和服務之企業。

歐特克係由和另外12个人一起在1982年所建立。在美国的[马林县亦設有多个分公司](../Page/马林县_\(加利福尼亚州\).md "wikilink")，且在全球擁有16家研發中心，超過3000名研發人員，而目前則以[圣拉菲尔为总部](../Page/圣拉菲尔_\(加利福尼亚州\).md "wikilink")。其中，位於中國上海的歐特克中國研究院是歐特克全球最大的研發機構，擁有超過1500名研發人員，歐特克每年的研發投入基本維持在全球總收入的20%的比例。縱觀歷史，歐特克從過去[2D設計到現在的](../Page/2D.md "wikilink")[3D建模](../Page/3D.md "wikilink")，從[數位化原型](../Page/數位化原型.md "wikilink")（DP）、[建築資訊模型](../Page/建築資訊模型.md "wikilink")（BIM）到幫助過去十四年歷屆[奧斯卡](../Page/奧斯卡.md "wikilink")[最佳視覺特效獎全部獲獎影片的數位娛樂創作解決方案](../Page/最佳視覺特效獎.md "wikilink")（DEC），歐特克提供數位化設計領域中廣泛且強大的產品組合，幫助使用者輕鬆應對設計流程各個階段的挑戰。

## 各行業解決方案

歐特克於各領域解決方案協助使用者有效率地創造、管理和分享其資料和數據資產，幫助用戶實現創意，並轉為具生產力、高效率、高獲利的競爭優勢。

### 建築、工程與施工業解決方案

歐特克結合頂尖的技術、產業經驗與全球服務，為建築產業提供最完整的系列產品。從最先進的[建築資訊模型技術](../Page/建築資訊模型.md "wikilink")，到最廣為運用的設計解決方案，如[Autodesk
Revit軟體為基礎的軟體解決方案](../Page/Autodesk_Revit.md "wikilink")，近年來在全球各地被建築工程等相關行業均被廣泛應用。歐特克提供[建築生命週期所需的資訊與管理](../Page/建築生命週期.md "wikilink")，不論是[建築師](../Page/建築師.md "wikilink")、[工程師](../Page/工程師.md "wikilink")、[承包商亦或擁有者](../Page/承包商.md "wikilink")，皆需要一個連接相關人員、改進程序以及幫助您建立在整個建築生命週期內使用的智慧型設計資料的解決方案。

歐特克[建築資訊模型是一種逹成建築專案設計和文件化的新方法](../Page/建築資訊模型.md "wikilink")，其不僅可以建構及管理圖形元件，同時也包括了資訊的管理、自動化的產生圖形和報表、設計的分析、工程模擬的排程、設備維護管理等，最終讓建築團隊做出最佳的決策。[建築資訊模型最大優點主要在支援分散式的團隊架構](../Page/建築資訊模型.md "wikilink")，在整個建築工程的生命週期裡，讓工作人員、機具及工作能夠更有效的分享資訊，進而消除資料的多餘、資料的重複輸入、資料的遺失或因溝通不良及輾轉傳達所造成的錯誤。歐特克[建築資訊模型系列軟體主要包括有](../Page/建築資訊模型.md "wikilink")[Autodesk
Revit Architecture](../Page/Autodesk_Revit.md "wikilink")、[Autodesk
Revit Structure](../Page/Autodesk_Revit.md "wikilink")、[Autodesk Revit
MEP](../Page/Autodesk_Revit.md "wikilink")、[AutoCAD Civil
3D](../Page/AutoCAD_Civil_3D.md "wikilink")，以及[Autodesk
Navisworks等軟體解決方案](../Page/Autodesk_Navisworks.md "wikilink")。

著名成功案例有[德國](../Page/德國.md "wikilink")[慕尼克的](../Page/慕尼克.md "wikilink")[寶馬世界](../Page/寶馬世界.md "wikilink")（BMW
Welt）、[梅賽德斯](../Page/梅賽德斯.md "wikilink")－[賓士](../Page/賓士.md "wikilink")[博物館](../Page/博物館.md "wikilink")（Mercedes-Benz
Museum），以及位於[斯圖加特的](../Page/斯圖加特.md "wikilink")[保時捷](../Page/保時捷.md "wikilink")[博物館等許多世界知名案例](../Page/博物館.md "wikilink")，均為使用歐特克[建築資訊模型技術來完成整個設計專案](../Page/建築資訊模型.md "wikilink")。

### 基礎建設業解決方案

歐特克的基礎設施模型解決方案（Infrastructure
Modeling，簡稱IM）是基於資料的、設計和管理大型基礎設施以及配套系統的[3D模型解決方案](../Page/3D.md "wikilink")，無論是進行城市總體規劃還是設計智慧化的電力、煤氣或供水管網，或是安裝和管理複雜的電信網路，並提供繪圖、土木工程與基礎建設管理業解決方案，幫助[地理資訊系統](../Page/地理資訊系統.md "wikilink")（Geographic
Information
System，簡稱GIS）專家與土木工程人員之間的合作與共享繪圖與設計資訊，並協助他們提升生產力、簡化內部流程、增加獲利、提供更好的服務以及進一步發揮設計資料的價值。而[Autodesk
Geospatial解決方案使得組織機構通過跨越工程和](../Page/Autodesk_Geospatial.md "wikilink")[地理資訊系統部門](../Page/地理資訊系統.md "wikilink")、以及組織機構中其他部門之間的溝通障礙，從而能夠完全地支配管理他們的資料功能。從規劃階段到設計、構建以及維護階段，歐特克提供功能強大的資料協同能力，以及一個完全整合的平臺來管理空間資料資產。

歐特克基礎設施模型能説明專業人員在視覺化的環境中整合多維的設計資訊、空間資訊、建築施工資訊和業務管理資訊，並進行類比與分析，用協同化、集成化、智慧化的方式，高效率的開展設計、施工以及維護管理，實現基礎設施專案的可持續化發展。構成歐特克基礎設施模型的主要軟體產品包括[AutoCAD
Map 3D](../Page/AutoCAD_Map_3D.md "wikilink")、[Autodesk
MapGuide和](../Page/Autodesk_MapGuide.md "wikilink")[Autodesk
Topobase等](../Page/Autodesk_Topobase.md "wikilink")。

### 製造業解決方案

歐特克製造業解決方案提供了用於建立設計、連接設計鏈、完成製造程序的強大技術，更涵括機具、電工、工具、產業設備、汽車組件與消費產品，以及可完整模擬及分析塑料工程之模流分析工具。而歐特克針對製造業所推出的[數位化原型解決方案](../Page/數位化原型.md "wikilink")，無論是[Autodesk
SketchBook Pro](../Page/SketchBook_Pro.md "wikilink")、[Autodesk Alias
Design](../Page/Alias.md "wikilink")、[Autodesk
Inventor及](../Page/Autodesk_Inventor.md "wikilink")[Autodesk
Showcase等產品](../Page/Autodesk_Showcase.md "wikilink")，皆可讓製造社群建立單一的數位模型並使用於各個生產階段，用來消彌通常會存在於概念設計團隊、工程團隊與製造團隊之間的隔閡，該項技術賦予製造商在實際做出產品之前，先進行虛擬研究的能力，以讓其可以建立、驗證、優化與管理從概念設計階段到生產製造的整個過程，並得以製作較少實體原型，以減少設計與生產成本。藉由數位化原型，製造商可以視覺化並模擬該設計在真實狀況下的特性，而比較不用倚賴昂貴的實體原型。

綜觀而言之，歐特克[數位化原型解決方案](../Page/數位化原型.md "wikilink")，係支援使用工作組創建一個用於整個生產流程的數位化模型，並為概念設計、工程、機械設計、電氣控制設計和製造團隊搭起一座溝通的橋樑，從而解決可能在概念設計、工程和製造等產品開發流程的三個主要階段中發生的不連貫問題以提升生產力和縮減週期為目標，持續推出一系列滿足市場需求的產品，其中，最為典型之軟體如[Autodesk
Inventor即是數位化原型的基礎](../Page/Autodesk_Inventor.md "wikilink")。

使用歐特克該製造業解決方案，可以使更好的產品以更低成本更快地投放至市場。在國際上亦有許多成功案例展現，如[蘇格蘭的](../Page/蘇格蘭.md "wikilink")[Green
Ocean公司](../Page/Green_Ocean.md "wikilink")，曾採用[Autodesk
Inventor設計出用於海洋發電的浮動裝置](../Page/Autodesk_Inventor.md "wikilink")、[澳大利亞城市藝術項目設計諮詢公司](../Page/澳大利亞城市藝術項目設計諮詢公司.md "wikilink")（Urban
Art
Projects，簡稱UAP），於2010年[上海世博會的部分景觀設計和](../Page/上海.md "wikilink")[澳大利亞](../Page/澳大利亞.md "wikilink")[坎培拉的員警紀念碑](../Page/坎培拉.md "wikilink")，皆使該公司之專案執行得更有效率。據瞭解，除了[上海世博會](../Page/上海.md "wikilink")，其他如[德國](../Page/德國.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")、[澳洲等](../Page/澳洲.md "wikilink")[國家館也採用了歐特克的設計解決方案](../Page/國家館.md "wikilink")。

### 傳媒暨娛樂業解決方案

[Autodesk_headquarters.jpg](https://zh.wikipedia.org/wiki/File:Autodesk_headquarters.jpg "fig:Autodesk_headquarters.jpg")
[Autodesk_Vault_2010.png](https://zh.wikipedia.org/wiki/File:Autodesk_Vault_2010.png "fig:Autodesk_Vault_2010.png")
[Alexroom.jpg](https://zh.wikipedia.org/wiki/File:Alexroom.jpg "fig:Alexroom.jpg")設計的虛擬房間\]\]
無論是電影、電玩遊戲，還是電視節目製作；從塑造[3D模型和製作生動角色的動畫軟體到與遊戲引擎整合的運行時間技術](../Page/3D.md "wikilink")；從完整的動畫片、動態快速的電影預告片、改造為[3D立體版的熱門強片](../Page/3D.md "wikilink")，到一場完美混合各元素的戲劇延伸，歐特克的數位娛樂創作解決方案均可為數位[藝術家和動畫師們提供領先的數位內容創作技術](../Page/藝術家.md "wikilink")，幫助藝術家們更快速完成工作，以保留更多時間探索更多創意、拓展創作空間，創造出更多令人驚歎的作品。如何將一個故事完美呈現在螢幕上，對於數位影音工作者而言，始終都是最大的挑戰。而隨著每年競爭愈顯激烈、排程與預算縮減、消費者期盼日深的狀況下，這個挑戰只會愈來愈艱鉅\!

歐特克數位娛樂創作解決方案是針對電影、遊戲等傳媒暨娛樂業等相關行业所推出，藉由歐特克所提供之[3D專業軟體](../Page/3D.md "wikilink")，包括[Autodesk
Maya](../Page/Maya.md "wikilink")、[Autodesk
MotionBuilder](../Page/MotionBuilder.md "wikilink")、[Autodesk 3ds
Max](../Page/3ds_Max.md "wikilink")、[Autodesk 3ds Max
Design](../Page/3ds_Max.md "wikilink")、[Autodesk
Mudbox](../Page/SkyMatter.md "wikilink")、[Autodesk
Softimage](../Page/Softimage.md "wikilink")、等，以進行建模和後續之[動畫及](../Page/動畫.md "wikilink")[特效製作](../Page/特效.md "wikilink")。

過去十四年榮膺歷屆[奧斯卡](../Page/奧斯卡.md "wikilink")[最佳視覺特效獎的全部獲獎影片](../Page/最佳視覺特效獎.md "wikilink")，均普遍借助歐特克3D軟體解決方案進行設計、視覺化，著名之案例有[變形金剛](../Page/變形金剛.md "wikilink")2：復仇之戰（Transformers:
Revenge of the
Fallen）、[魔鬼終結者：未來救贖](../Page/魔鬼終結者：未來救贖.md "wikilink")（Terminator
Salvation）、[怪獸大戰外星人](../Page/怪獸大戰外星人.md "wikilink")（Monsters vs.
Aliens），以及[星際爭霸戰](../Page/星際爭霸戰.md "wikilink")（Star
Trek）等多部強檔鉅片的炫目特效場景，皆使用該解決方案來執行，且均有良好之成效。

歐特克製作了能幫助用戶創造魔力的工具，並為後期製作、[廣播節目](../Page/廣播.md "wikilink")、[遊戲](../Page/遊戲.md "wikilink")、[動畫](../Page/動畫.md "wikilink")、[Web之數位內容的建立](../Page/Web.md "wikilink")、效果、編輯以及[3D應用程式提供最先進的技術](../Page/3D.md "wikilink")。

## 大事记

  - 1982 歐特克公司成立。同年，[AutoCAD隨即上市](../Page/AutoCAD.md "wikilink")

<!-- end list -->

  - 1985 歐特克上市，成為首家上市的[PC](../Page/PC.md "wikilink")
    [CAD公司](../Page/CAD.md "wikilink")；首次公開募集[（IPO）](../Page/（IPO）.md "wikilink")160萬股，每股發行價11.00[美元](../Page/美元.md "wikilink")。

<!-- end list -->

  - 1987
    歐特克銷售出第10萬套[AutoCAD](../Page/AutoCAD.md "wikilink")，並在第二市場成功發行250萬股。

<!-- end list -->

  - 1989 公司發起人股份共226.5萬股，公司創始人John Walker將管理權移交给Alvar Green。

<!-- end list -->

  - 1990 歐特克成立多媒體部，推出了第一個動畫工作――[3D
    Studio軟件](../Page/3D_Studio.md "wikilink")。

<!-- end list -->

  - 1992 歐特克收購[Micro Engineering
    Solutions公司](../Page/Micro_Engineering_Solutions.md "wikilink")。同一年，卡萝·巴茨被任命為董事會主席、總裁兼首席執行官。

<!-- end list -->

  - 1993
    歐特克收購[Woodbourne的](../Page/Woodbourne.md "wikilink")[Ithaca軟件公司](../Page/Ithaca.md "wikilink")。

<!-- end list -->

  - 1994
    歐特克售出第100萬套[AutoCAD](../Page/AutoCAD.md "wikilink")。同年，歐特克成立中國分公司。

<!-- end list -->

  - 1996 歐特克銷售出第150萬套[AutoCAD](../Page/AutoCAD.md "wikilink")。

<!-- end list -->

  - 1997 歐特克收購[Softdesk公司](../Page/Softdesk.md "wikilink")。

<!-- end list -->

  - 1998 歐特克針對垂直细分市场場推出多種產品。同年，收購[Genius
    CAD軟件公司](../Page/Genius_CAD.md "wikilink")。

<!-- end list -->

  - 1999 歐特克收購[Discreet
    Logic公司](../Page/Discreet_Logic.md "wikilink")。同年，歐特克推出[Autodesk
    Inventor](../Page/Autodesk_Inventor.md "wikilink")（用於創建基於特徵的實體模型）。

<!-- end list -->

  - 2001
    歐特克推出定位與速博服務。同年，收購[Buzzsaw.com](../Page/Buzzsaw.com.md "wikilink")、[Media
    100](../Page/Media_100.md "wikilink")、[Gentry
    Systems等公司](../Page/Gentry_Systems.md "wikilink")。

<!-- end list -->

  - 2002 歐特克收購[Revit
    Technology公司與](../Page/Autodesk_Revit.md "wikilink")[CAiCE軟件公司](../Page/CAiCE.md "wikilink")。

<!-- end list -->

  - 2003
    歐特克收購[truEInnovations](../Page/truEInnovations.md "wikilink")、[Linius
    Technologies](../Page/Linius_Technologies.md "wikilink")、[VIA
    Development等公司](../Page/VIA_Development.md "wikilink")。

<!-- end list -->

  - 2004 歐特克收購MechSoft Technology與Unreal Pictures公司。

<!-- end list -->

  - 2005
    歐特克營業收入超過10億美元。同年，收購[COMPASS](../Page/COMPASS.md "wikilink")、[Colorfront有限公司](../Page/Colorfront.md "wikilink")、[c-plan](../Page/c-plan.md "wikilink")、[Solid
    Dynamics](../Page/Solid_Dynamics.md "wikilink")、[Engineering
    Intent等公司](../Page/Engineering_Intent.md "wikilink")。

<!-- end list -->

  - 2006
    卡爾·巴斯被任命為總裁兼首席執行官。同年，歐特克收購[Alias與](../Page/Alias.md "wikilink")[Constructware公司](../Page/Constructware.md "wikilink")。

<!-- end list -->

  - 2007
    歐特克慶祝成立25周年。同年，收購[NavisWorks](../Page/NavisWorks.md "wikilink")、[OpticoreAB](../Page/OpticoreAB.md "wikilink")、[PlassoTech](../Page/PlassoTech.md "wikilink")、[SkyMatter](../Page/SkyMatter.md "wikilink")、[漢略](../Page/漢略.md "wikilink")（Hanna
    Strategies）等公司。而後，歐特克宣佈擁有1百萬[3D產品用戶](../Page/3D.md "wikilink")。

<!-- end list -->

  - 2008
    歐特克中國研究院在上海成立。同年，收購[Softimage](../Page/Softimage.md "wikilink")（[Avid旗下](../Page/Avid.md "wikilink")）、[Algor
    Technology等公司](../Page/Algor_Technology.md "wikilink")。

## 产品列表

  - [AutoCAD](../Page/AutoCAD.md "wikilink")
  - [AutoCAD Architecture](../Page/AutoCAD_Architecture.md "wikilink")
  - [AutoCAD Civil 3D](../Page/AutoCAD_Civil_3D.md "wikilink")
  - [AutoCAD Electrical](../Page/AutoCAD_Electrical.md "wikilink")
  - [AutoCAD LT](../Page/AutoCAD.md "wikilink")
  - [AutoCAD Map 3D](../Page/AutoCAD_Map_3D.md "wikilink")
  - [AutoCAD Mechanical](../Page/AutoCAD_Mechanical.md "wikilink")
  - [AutoCAD MEP](../Page/AutoCAD_MEP.md "wikilink")
  - [AutoCAD Raster Design](../Page/AutoCAD_Raster_Design.md "wikilink")
  - [AutoCAD WS](../Page/AutoCAD_WS.md "wikilink")（专为移动设备推出）
  - [Autodesk 3ds Max系列](../Page/3ds_Max.md "wikilink")
  - [Autodesk Alias系列](../Page/Alias.md "wikilink")
  - [Autodesk Building Design
    Suite](../Page/Autodesk_Building_Design_Suite.md "wikilink")
  - [Autodesk
    Backburner](../Page/Autodesk_Backburner.md "wikilink")\[1\]\[2\]
  - [Autodesk Design Suite](../Page/Autodesk_Design_Suite.md "wikilink")
  - [Autodesk Entertainment Creation
    Suites](../Page/Autodesk_Entertainment_Creation_Suites.md "wikilink")
  - [Autodesk Factory Design
    Suites](../Page/Autodesk_Factory_Design_Suites.md "wikilink")
  - [Autodesk Homestyler](../Page/Autodesk_Homestyler.md "wikilink")
  - [Autodesk Infrastructure Design
    Suite](../Page/Autodesk_Infrastructure_Design_Suite.md "wikilink")
  - [Autodesk Inferstructure Map
    Server](../Page/Autodesk_Inferstructure_Map_Server.md "wikilink")
  - [Autodesk Inventor系列](../Page/Autodesk_Inventor.md "wikilink")
  - [Autodesk Maya](../Page/Maya.md "wikilink")
  - [Autodesk Moldflow](../Page/Moldflow.md "wikilink")
  - [Autodesk
    MotionBuilder](../Page/Autodesk_MotionBuilder.md "wikilink")
  - [Autodesk Mudbox](../Page/Autodesk_Mudbox.md "wikilink")
  - [Autodesk Navisworks系列](../Page/Navisworks.md "wikilink")
  - [Autodesk Plant Design
    Suite](../Page/Autodesk_Plant_Design_Suite.md "wikilink")
  - [Autodesk Product Design
    Suite](../Page/Autodesk_Product_Design_Suite.md "wikilink")
  - [Autodesk Revit系列](../Page/Autodesk_Revit.md "wikilink")
  - [Autodesk Robot Structural
    Analysis](../Page/Autodesk_Robot_Structural_Analysis.md "wikilink")
  - [Autodesk Sketchbook
    Pro](../Page/Autodesk_Sketchbook_Pro.md "wikilink")
  - [Autodesk Simulation系列](../Page/Autodesk_Simulation.md "wikilink")
  - [Autodesk Vault系列](../Page/Autodesk_Vault.md "wikilink")

## 参见

  - [DWF](../Page/DWF.md "wikilink")
  - [DWG](../Page/DWG.md "wikilink")
  - [DXF](../Page/DXF.md "wikilink")
  - [CAD](../Page/CAD.md "wikilink")
  - [數位化原型](../Page/數位化原型.md "wikilink")
  - [建築信息模型](../Page/建築信息模型.md "wikilink")

## 参考资料

## 外部链接

  - Autodesk Inc.
      - [Autodesk总部](http://www.autodesk.com/)
      - [Autodesk中国大陆](http://www.autodesk.com.cn/)
      - [Autodesk臺灣](http://www.autodesk.com.tw/)
  - Autodesk University
      - [Autodesk Univeristy](http://au.autodesk.com/)
      - [Autodesk
        Univeristy中文技术社区](https://web.archive.org/web/20150905071144/http://au.autodesk.com.cn/)
  - [Autodesk实验室](http://labs.autodesk.com/)
  - [Autodesk学生社区](http://students.autodesk.com/)
  - [Early AutoCAD
    information](http://www.fourmilab.ch/autofile/www/chapter2_36.html)
  - [The Autodesk File, a History of
    Autodesk](http://www.fourmilab.ch/autofile)
  - [Autodesk Sponsorship of CGSociety
    Challenges](http://features.cgsociety.org/challenge/)
  - [Autodesk Middle East Training
    Centre](https://archive.is/20150228180029/http://www.cgpark.com.tr/cgpark/mayakursu.html)

[Category:1982年成立的公司](../Category/1982年成立的公司.md "wikilink")
[Autodesk](../Category/Autodesk.md "wikilink")
[Category:美國軟體公司](../Category/美國軟體公司.md "wikilink")
[Category:加利福尼亞州公司](../Category/加利福尼亞州公司.md "wikilink")

1.  [About Autodesk Backburner | 3ds Max | Autodesk Knowledge
    Network](https://knowledge.autodesk.com/support/3ds-max/troubleshooting/caas/CloudHelp/cloudhelp/2015/ENU/Installation-3DSMax/files/GUID-F6732A30-821C-4547-9FAA-E46BCA13392A-htm.html)
2.  [What Is Autodesk Backburner? |
    Chron.com](http://smallbusiness.chron.com/autodesk-backburner-61233.html)