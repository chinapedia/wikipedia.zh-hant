**波多野秀治**（生年不詳—1579年6月25日）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[安土桃山時代在](../Page/安土桃山時代.md "wikilink")[丹波國的武將](../Page/丹波國.md "wikilink")、[大名](../Page/大名.md "wikilink")。父親是[波多野晴通](../Page/波多野晴通.md "wikilink")。最後的[當主](../Page/當主.md "wikilink")。

## 生平

生年不詳，家中長男。波多野氏在祖父[稙通死後就臣屬於](../Page/波多野稙通.md "wikilink")[三好長慶](../Page/三好長慶.md "wikilink")。最初自身亦仕於[三好氏](../Page/三好氏.md "wikilink")，在[正親町天皇即位儀式時亦有列席](../Page/正親町天皇.md "wikilink")。不過在長慶死後的[永祿](../Page/永祿.md "wikilink")9年（[1566年](../Page/1566年.md "wikilink")），奪回波多野家的居城，並以[戰國大名的身份獨立](../Page/戰國大名.md "wikilink")。而且把女兒嫁予[播磨國的](../Page/播磨國.md "wikilink")[別所長治](../Page/別所長治.md "wikilink")，雙方結成同盟。

永祿11年（[1568年](../Page/1568年.md "wikilink")），在[織田信長奉](../Page/織田信長.md "wikilink")[足利義昭並](../Page/足利義昭.md "wikilink")[上洛後](../Page/上洛.md "wikilink")，臣服於[織田氏](../Page/織田氏.md "wikilink")。[天正](../Page/天正_\(日本\).md "wikilink")3年（[1575年](../Page/1575年.md "wikilink")），加入信長派遣而來的[明智光秀的軍勢](../Page/明智光秀.md "wikilink")，負責討伐[丹波國內對抗織田氏的豪族](../Page/丹波國.md "wikilink")，不過暗中向丹波國的豪族傳達聯手的信息。天正4年（[1576年](../Page/1576年.md "wikilink")）1月，突然背叛並攻擊光秀的軍勢，並將其撃退（[黑井城之戰](../Page/黑井城之戰.md "wikilink")）。

對此激怒的信長令光秀指揮大軍，並命令光秀再度侵攻丹波。對此，在八上城死守，並與[國人眾一同頑強抵抗](../Page/國人眾.md "wikilink")，因為對丹波的山岳地形有相當深的理解和知識，於是巧妙地用兵並迎擊，抵擋織田軍的猛攻長達1年半的時間。不過因為長期的守城戰，兵糧耗盡，己方的丹後、[但馬諸豪族亦相繼被撃破](../Page/但馬.md "wikilink")，而且因為光秀的離間，部份豪族更投向織田氏，於是陷入困境，終於在天正7年（[1579年](../Page/1579年.md "wikilink")），向光秀降伏。

此後，與弟弟[秀尚一同被送到](../Page/波多野秀尚.md "wikilink")[安土城](../Page/安土城.md "wikilink")，被信長命令於6月2日在安土的[淨巖院](../Page/淨巖院.md "wikilink")[慈恩寺中](../Page/慈恩寺.md "wikilink")，被處以[磔刑](../Page/磔刑.md "wikilink")。

辭世句是「」。

## 子孫

根據[篠山市的傳說](../Page/篠山市.md "wikilink")，次男甚藏由[乳母抱走並逃到味間](../Page/乳母.md "wikilink")，後來改名為[波多野定吉](../Page/波多野定吉.md "wikilink")，仕於。

## 家臣

  - [赤井直正](../Page/赤井直正.md "wikilink")
  - [荒木氏纲](../Page/荒木氏纲.md "wikilink")
  - [韧井教业](../Page/韧井教业.md "wikilink")

## 外部連結

  - [武家家伝＿波多野氏](http://www2.harimaya.com/sengoku/html/hatano_k.html)

## 相關條目

  -
[Category:戰國大名](../Category/戰國大名.md "wikilink")
[Category:16世紀出生](../Category/16世紀出生.md "wikilink")
[Category:1579年逝世](../Category/1579年逝世.md "wikilink")
[Category:波多野氏](../Category/波多野氏.md "wikilink")