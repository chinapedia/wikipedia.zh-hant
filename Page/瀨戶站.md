**瀨戶站**（）是位於[日本](../Page/日本.md "wikilink")[岡山縣](../Page/岡山縣.md "wikilink")[赤磐郡](../Page/赤磐郡.md "wikilink")[瀨戶町瀨戶的](../Page/瀨戶町.md "wikilink")[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）[山陽本線的](../Page/山陽本線.md "wikilink")[火車站](../Page/火車站.md "wikilink")，只有普快車停靠。車站附近有2所高中，早晚利用的旅客較多，有不少山陽本線的列車於本站折返。

## 車站構造

[側式月台](../Page/側式月台.md "wikilink")1面1線與[島式月台](../Page/島式月台.md "wikilink")1面2線，合計2面3線的[地面車站](../Page/地面車站.md "wikilink")。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>山陽本線</p></td>
<td><p>上行</p></td>
<td><p>、<a href="../Page/姬路站.md" title="wikilink">姬路方向</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2、3</p></td>
<td><p>下行</p></td>
<td><p><a href="../Page/岡山站_(日本).md" title="wikilink">岡山</a>、<a href="../Page/三原站_(日本).md" title="wikilink">三原方向</a></p></td>
<td><p>2號月台只限此站始發、待避列車</p></td>
<td></td>
</tr>
</tbody>
</table>

## 車站周邊

  - [瀨戶町役場](../Page/瀨戶町_\(岡山縣\).md "wikilink")
  - [LAWSON](../Page/LAWSON.md "wikilink")
  - 備前瀨戶[郵局](../Page/郵局.md "wikilink")
  - [赤磐警察署](../Page/赤磐警察署.md "wikilink")
  - [中國銀行](../Page/中國銀行_\(日本\).md "wikilink")
  - [番茄銀行](../Page/番茄銀行.md "wikilink")
  - [おかやま信用金庫](../Page/おかやま信用金庫.md "wikilink")
  - 岡山縣立瀨戶高等學校
  - [岡山縣道37號西大寺山陽線](../Page/岡山縣道37號西大寺山陽線.md "wikilink")
  - [岡山縣道、兵庫縣道96號岡山赤穂線](../Page/岡山縣道、兵庫縣道96號岡山赤穂線.md "wikilink")
  - [岡山縣道178號瀨戶停車場線](../Page/岡山縣道178號瀨戶停車場線.md "wikilink")

### 巴士

  - [宇野自動車](../Page/宇野自動車.md "wikilink")

## 历史

  - 1891年（明治24年）3月18日 開業。

## 相鄰車站

  - 西日本旅客鐵道

    山陽本線

      -

        －**瀨戶**－

## 關連項目

  - [日本鐵路車站列表](../Page/日本鐵路車站列表.md "wikilink")

## 外部連結

[JR西日本（瀬戶站）](http://www.jr-odekake.net/eki/top.php?id=0650606)

[Category:岡山市鐵路車站](../Category/岡山市鐵路車站.md "wikilink")
[To](../Category/日本鐵路車站_Se.md "wikilink") [Category:山陽本線車站
(西日本旅客鐵道)](../Category/山陽本線車站_\(西日本旅客鐵道\).md "wikilink")
[Category:東區 (岡山市)](../Category/東區_\(岡山市\).md "wikilink")
[Category:1891年啟用的鐵路車站](../Category/1891年啟用的鐵路車站.md "wikilink")