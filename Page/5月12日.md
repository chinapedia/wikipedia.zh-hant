**5月12日**是阳历年的第132天（闰年是133天），离一年的结束还有233天。

## 大事记

### 10世紀

  - [907年](../Page/907年.md "wikilink")，完全控制朝廷的後梁太祖[朱温逼迫](../Page/朱温.md "wikilink")[唐哀帝下诏退位](../Page/唐哀帝.md "wikilink")，结束[唐朝将近](../Page/唐朝.md "wikilink")300年的统治。

### 14世紀

  - [1357年](../Page/1357年.md "wikilink")：[朱元璋攻占](../Page/朱元璋.md "wikilink")[宁国](../Page/宁国市.md "wikilink")。
  - [1364年](../Page/1364年.md "wikilink")：[波兰国王](../Page/波兰君主列表.md "wikilink")[卡齐米日三世在](../Page/卡齐米日三世.md "wikilink")[克拉科夫创建](../Page/克拉科夫.md "wikilink")[亞捷隆大學](../Page/亞捷隆大學.md "wikilink")，为波兰最古老的[大学](../Page/大学.md "wikilink")。

### 16世紀

  - [1588年](../Page/1588年.md "wikilink")：在[法国](../Page/法国.md "wikilink")[胡格诺战争中](../Page/法国宗教战争.md "wikilink")，[吉斯公爵亨利一世率军进入巴黎](../Page/亨利一世·德·洛林，第三代吉斯公爵.md "wikilink")，迫使[国王](../Page/法国君主列表.md "wikilink")[亨利三世逃往](../Page/亨利三世_\(法兰西\).md "wikilink")[沙特尔](../Page/沙特尔.md "wikilink")。

### 18世紀

  - [1797年](../Page/1797年.md "wikilink")：在[法国大革命战争中](../Page/法国大革命战争.md "wikilink")，[拿破仑一世率领](../Page/拿破仑一世.md "wikilink")[法国军队占领](../Page/法国.md "wikilink")[威尼斯共和国](../Page/威尼斯共和国.md "wikilink")。

### 19世紀

  - [1870年](../Page/1870年.md "wikilink")：[曼尼托巴省加入](../Page/曼尼托巴省.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")。

### 20世紀

  - [1925年](../Page/1925年.md "wikilink")：[苏联](../Page/苏联.md "wikilink")[人代会批准](../Page/苏联人民代表大会.md "wikilink")[俄罗斯苏维埃联邦社会主义共和国宪法](../Page/俄罗斯苏维埃联邦社会主义共和国.md "wikilink")。
  - [1933年](../Page/1933年.md "wikilink")：[新政](../Page/新政.md "wikilink"):
    [美國總統](../Page/美國總統.md "wikilink")[羅斯福頒佈](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")《》，透過向農民提供補貼來限制農業生產以穩定農作物價格。
  - [1937年](../Page/1937年.md "wikilink")：[英國國王](../Page/英国君主.md "wikilink")[喬治六世在](../Page/乔治六世.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")[威斯敏斯特教堂举行加冕典礼](../Page/威斯敏斯特教堂.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[苏联宣布解除對](../Page/苏联.md "wikilink")[英](../Page/英国.md "wikilink")、[美](../Page/美国.md "wikilink")、[法三国占领的](../Page/法国.md "wikilink")[西柏林的封鎖](../Page/西柏林.md "wikilink")，[第一次柏林危机結束](../Page/柏林封鎖.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[刚果共和国成立](../Page/刚果共和国.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：。
  - [1978年](../Page/1978年.md "wikilink")：[香港政府宣佈收回](../Page/英屬香港.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[西樓角七座樓宇](../Page/西樓角.md "wikilink")，興建[地下鐵路車廠](../Page/港鐵車廠.md "wikilink")，商戶同業主因為不滿賠償拒絕遷出。事件一直僵持到[1979年業主企圖拆除西樓角新路鐵閘](../Page/1979年.md "wikilink")，釀成警民流血衝突。
  - [1998年](../Page/1998年.md "wikilink")：[印度尼西亚](../Page/印度尼西亚.md "wikilink")[雅加达](../Page/雅加达.md "wikilink")4位大學生，在示威活動中被軍隊開槍射殺，隨後並引發「[黑色五月暴动](../Page/黑色五月暴动.md "wikilink")」大規模排華事件。

### 21世紀

  - [2005年](../Page/2005年.md "wikilink")：[臺灣](../Page/臺灣.md "wikilink")[親民黨](../Page/親民黨.md "wikilink")[主席](../Page/主席.md "wikilink")[宋楚瑜與](../Page/宋楚瑜.md "wikilink")[中国共产党](../Page/中国共产党.md "wikilink")[總書記](../Page/中国共产党中央委员会总书记.md "wikilink")[胡锦涛於](../Page/胡锦涛.md "wikilink")[北京舉行會談](../Page/北京市.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：[中國](../Page/中國.md "wikilink")[四川省](../Page/四川省.md "wikilink")[汶川县發生](../Page/汶川县.md "wikilink")[里氏](../Page/里氏地震規模.md "wikilink")8.0級[强烈地震](../Page/汶川大地震.md "wikilink")，6萬多人死亡、1萬多人失蹤\[1\]。
  - [2010年](../Page/2010年.md "wikilink")：[泛非航空771号班机在降落](../Page/泛非航空771號班機空難.md "wikilink")[的黎波里国际机场时坠毁](../Page/的黎波里国际机场.md "wikilink")，机上104名乘客中，103名当场死亡。
  - [2015年](../Page/2015年.md "wikilink")：[费城一列火车脱轨](../Page/2015年費城火車出軌事故.md "wikilink")，造成8人死亡，200多人受伤。
  - [2015年](../Page/2015年.md "wikilink")：[尼泊尔发生里氏规模](../Page/尼泊尔.md "wikilink")7.3级[地震](../Page/2015年5月尼泊尔地震.md "wikilink")，218人死亡，超过2500人受伤。
  - [2017年](../Page/2017年.md "wikilink")：全球400多万台电脑感染针对英国[国民保健署和](../Page/国民保健署_\(英国\).md "wikilink")[西班牙电信的](../Page/西班牙电信.md "wikilink")[勒索软件](../Page/勒索软件.md "wikilink")[WannaCry](../Page/WannaCry.md "wikilink")。

## 出生

  - [1812年](../Page/1812年.md "wikilink")：[愛德華·利爾](../Page/愛德華·利爾.md "wikilink")，英國詩人、藝術家和音樂家\[2\]（[1888年逝世](../Page/1888年.md "wikilink")）
  - [1820年](../Page/1820年.md "wikilink")：[弗羅倫斯·南丁格爾](../Page/弗羅倫斯·南丁格爾.md "wikilink")，[英国](../Page/英国.md "wikilink")[護士與](../Page/護士.md "wikilink")[統計學家](../Page/统计学家列表.md "wikilink")（[1910年逝世](../Page/1910年.md "wikilink")）
  - [1907年](../Page/1907年.md "wikilink")：[凯瑟琳·赫本](../Page/凯瑟琳·赫本.md "wikilink")，[美国女演員](../Page/美国.md "wikilink")，[美国电影学会](../Page/美国电影学会.md "wikilink")[AFI百年百大明星評為百年來最偉大的女演員第](../Page/AFI百年百大明星.md "wikilink")1名（[2003年逝世](../Page/2003年.md "wikilink")）
  - [1910年](../Page/1910年.md "wikilink")：[多萝西·霍奇金](../Page/多萝西·霍奇金.md "wikilink")，英国女[生物化學家](../Page/生物化學.md "wikilink")，1964年[諾貝爾化學獎得主](../Page/諾貝爾化學獎.md "wikilink")（[1994年逝世](../Page/1994年.md "wikilink")）
  - [1919年](../Page/1919年.md "wikilink")：[吴文俊](../Page/吴文俊.md "wikilink")，中国数学家（[2017年逝世](../Page/2017年.md "wikilink")）
  - [1944年](../Page/1944年.md "wikilink")：[彭定康](../Page/彭定康.md "wikilink")，[牛津大学](../Page/牛津大学.md "wikilink")[校監](../Page/校監.md "wikilink")，第28任[香港總督](../Page/香港總督.md "wikilink")
  - [1945年](../Page/1945年.md "wikilink")：[阿兰·鲍尔](../Page/阿兰·鲍尔.md "wikilink")，[英格兰足球運動員](../Page/英格兰.md "wikilink")（[2007年逝世](../Page/2007年.md "wikilink")）
  - [1949年](../Page/1949年.md "wikilink")：[萩尾望都](../Page/萩尾望都.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")
  - [1959年](../Page/1959年.md "wikilink")：[文·雷姆斯](../Page/文·雷姆斯.md "wikilink")，[美國](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[梁少霞](../Page/梁少霞.md "wikilink")，[香港女](../Page/香港.md "wikilink")[配音員](../Page/配音員.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[山口真弓](../Page/山口真弓.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[郭鐵人](../Page/郭鐵人.md "wikilink")，台灣演員
  - [1977年](../Page/1977年.md "wikilink")：[蔣怡](../Page/蔣怡.md "wikilink")，[中國大陸](../Page/中國大陸.md "wikilink")、[香港女模特](../Page/香港.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[陳譯賢](../Page/陳譯賢.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[近藤隆](../Page/近藤隆.md "wikilink")，日本男性聲優
  - [1985年](../Page/1985年.md "wikilink")：[朱蕾安](../Page/朱蕾安.md "wikilink")，[臺灣女演員](../Page/臺灣.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[李慧詩](../Page/李慧詩.md "wikilink")，[香港女](../Page/香港.md "wikilink")[單車運動員](../Page/自行車.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[马塞洛·维埃拉·达席尔瓦·儒尼奥尔](../Page/马塞洛·维埃拉·达席尔瓦·儒尼奥尔.md "wikilink")，[巴西足球員](../Page/巴西.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[林利豪](../Page/林利豪.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[許名傑](../Page/許名傑.md "wikilink")，臺灣男子團體[SpeXial團員](../Page/SpeXial.md "wikilink")

## 逝世

  - [1003年](../Page/1003年.md "wikilink")：[西尔维斯特二世](../Page/西尔维斯特二世.md "wikilink")，[教宗](../Page/教宗.md "wikilink")
  - [1012年](../Page/1012年.md "wikilink")：[思齊四世](../Page/思齊四世.md "wikilink")，教宗
  - [1155年](../Page/1155年.md "wikilink")：[李清照](../Page/李清照.md "wikilink")，[南宋女詞人](../Page/南宋.md "wikilink")（[1084年出生](../Page/1084年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[胡漢民](../Page/胡漢民.md "wikilink")，[中國國民黨元老](../Page/中國國民黨.md "wikilink")，[中華民國第](../Page/中華民國.md "wikilink")1任[國民政府](../Page/國民政府.md "wikilink")[立法院](../Page/立法院.md "wikilink")[院長](../Page/立法院院長.md "wikilink")（[1879年出生](../Page/1879年.md "wikilink")）
  - [1974年](../Page/1974年.md "wikilink")：[楊慕琦](../Page/楊慕琦.md "wikilink")，第21任[香港總督](../Page/香港總督.md "wikilink")（[1886年出生](../Page/1886年.md "wikilink")）
  - [1996年](../Page/1996年.md "wikilink")：[江兆申](../Page/江兆申.md "wikilink")，書畫、篆刻家，中國書畫研究學者（[1925年出生](../Page/1925年.md "wikilink")）
  - [2001年](../Page/2001年.md "wikilink")：，[俄罗斯](../Page/俄罗斯.md "wikilink")[飛機](../Page/固定翼飛機.md "wikilink")[設計師](../Page/設計師.md "wikilink")（[1925年出生](../Page/1925年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[李莎](../Page/李莎.md "wikilink")，[中國](../Page/中國.md "wikilink")[俄语學家](../Page/俄语.md "wikilink")、已故中國全國政協委員、前中共領導人[李立三之妻](../Page/李立三.md "wikilink")（[1914年出生](../Page/1914年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[吳美雲](../Page/吳美雲.md "wikilink")，臺灣[漢聲雜誌社創辦人之一](../Page/漢聲雜誌社.md "wikilink")\[3\]（[1943年出生](../Page/1943年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[蘇珊娜·瓊斯](../Page/蘇珊娜·瓊斯.md "wikilink")，美國[超級人瑞](../Page/超級人瑞.md "wikilink")（[1899年出生](../Page/1899年.md "wikilink")）

## 节假日和习俗

  - [国际护士节](../Page/国际护士节.md "wikilink")

  - ：[防灾减灾日](../Page/防灾减灾日.md "wikilink")

## 參考資料

1.
2.
3.