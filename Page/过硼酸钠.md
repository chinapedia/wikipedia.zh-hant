**过硼酸钠**（**PBS**）是一个无味的白色固体，可溶于水，化学式为[Na](../Page/钠.md "wikilink")[B](../Page/硼.md "wikilink")[O](../Page/氧.md "wikilink")<sub>3</sub>。它有三种[水合物](../Page/水合物.md "wikilink")：一水NaBO<sub>3</sub>.H<sub>2</sub>O、四水NaBO<sub>3</sub>.4H<sub>2</sub>O及三水NaBO<sub>3</sub>.3H<sub>2</sub>O，其中一水和三水合过硼酸钠在工业上较重要。\[1\]
它是高效氧系漂白剂，也有[杀菌](../Page/杀菌.md "wikilink")、织物保色等功能，广泛应用于[漂白粉](../Page/漂白粉.md "wikilink")、[洗衣粉](../Page/洗衣粉.md "wikilink")、[洗涤剂等日用材料中](../Page/洗涤剂.md "wikilink")。

## 制备及反应

过硼酸钠由[硼砂](../Page/硼砂.md "wikilink")、[过氧化氢和](../Page/过氧化氢.md "wikilink")[氢氧化钠反应制取](../Page/氢氧化钠.md "wikilink")。一水合物可由四水合物加热得到，并且它的[活性氧含量更高](../Page/活性氧.md "wikilink")，在水中的溶解度和溶解速率更大，对热更加稳定。\[2\]
过硼酸钠与水反应[水解](../Page/水解.md "wikilink")，生成[过氧化氢和](../Page/过氧化氢.md "wikilink")[硼酸钠](../Page/硼酸钠.md "wikilink")。\[3\]

过硼酸钠在60°C以上迅速分解放出过氧化氢，因此只有此温度上过硼酸钠才能充分体现漂白活性。低于60°C时常加入[乙二胺四乙酸](../Page/乙二胺四乙酸.md "wikilink")（EDTA）作活化剂。

过硼酸钠是[有机合成中的](../Page/有机合成.md "wikilink")[氧化剂](../Page/氧化剂.md "wikilink")，可将某些取代[烷基硼氧化水解为](../Page/烷基硼.md "wikilink")[醇](../Page/醇.md "wikilink")，[醛氧化为](../Page/醛.md "wikilink")[羧酸](../Page/羧酸.md "wikilink")，[炔烃氧化为](../Page/炔烃.md "wikilink")[酮](../Page/酮.md "wikilink")，等等。\[4\]

## 结构

过硼酸钠与同为[过氧化物的](../Page/过氧化物.md "wikilink")[过碳酸钠和](../Page/过碳酸钠.md "wikilink")[过磷酸钠不同](../Page/过磷酸钠.md "wikilink")，它不是由[过氧化氢形成的](../Page/过氧化氢.md "wikilink")[加合物](../Page/加合物.md "wikilink")，而是含有由两个过氧链组成的环状过硼酸根阴离子（B<sub>2</sub>O<sub>8</sub>H<sub>4</sub><sup>2−</sup>），不含BO<sub>3</sub><sup>−</sup>离子。\[5\]

## 参考资料

[Category:钠化合物](../Category/钠化合物.md "wikilink")
[Category:过硼酸盐](../Category/过硼酸盐.md "wikilink")
[Category:氧化剂](../Category/氧化剂.md "wikilink")
[Category:漂白剂](../Category/漂白剂.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")

1.  B.J Brotherton Boron: Inorganic Chemistry *Encyclopedia of Inorganic
    Chemistry* (1994) Ed. R. Bruce King, John Wiley & Sons ISBN
    0471936200

2.
3.
4.  [过硼酸钠在有机合成中的应用](https://www.organic-chemistry.org/chemicals/oxidations/sodiumperborate.shtm)—Organic-chemistry.org

5.