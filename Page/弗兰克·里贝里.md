**弗兰克·里贝里**（****，），是一名[法國足球運動員](../Page/法國.md "wikilink")，司職中場，目前效力[德甲球會](../Page/德国足球甲级联赛.md "wikilink")[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")。

里贝里在兩歲遇上一宗嚴重車禍，他不幸地由後座拋向擋風玻璃，導致他右邊脸部留下了一條長長的疤痕。2006年改宗[伊斯兰教后改用阿拉伯语经名](../Page/伊斯兰教.md "wikilink")**比拉勒·优素福·默罕默德**（），但「列貝利」一名则仍更为常用。

2014年8月14日凌晨，31岁的里贝里在接受《踢球者》采访时确认，自己将退出法国国家队。\[1\]

## 球會生涯

### 早年、加拉塔萨雷及馬賽

里贝里早年從法國低組別聯賽打起，曾遠赴[土耳其聯賽球會](../Page/土耳其.md "wikilink")[加拉塔萨雷](../Page/加拉塔萨雷.md "wikilink")，效力了半个赛季，2005年重返法國加盟[馬賽](../Page/馬賽足球俱樂部.md "wikilink")。

[2006年世界杯](../Page/2006年世界杯.md "wikilink")，里贝里被主帅[-{zh-hans:多梅内克;zh-hk:杜明尼治;zh-tw:多梅内克;}-選入世界杯大軍](../Page/雷蒙·多梅内克.md "wikilink")，在十六強分組賽射入一球，嶄露頭角，被外界廣泛認為是法國著名球星[-{zh-hans:齐达内;
zh-hant:施丹;zh-tw:齊丹;}-的接班人](../Page/齐内丁·齐达内.md "wikilink")。但事實上兩人的足球風格並不相同，而里贝里的風格較接近[-{zh-hans:罗贝尔·皮雷;
zh-hant:皮里斯;}-](../Page/罗贝尔·皮雷.md "wikilink")，擅长边路的快速冲击，此外他的定位球亦罚的很好。

世界杯後里贝里曾主動要求轉會法甲大球會[里昂](../Page/里昂足球會.md "wikilink")，結果一度被马赛罰停賽幾場，直到後來才重獲上陣。

### 拜仁慕尼黑

2007年夏季他以2500万欧元的身价加盟[德甲豪门](../Page/德国足球甲级联赛.md "wikilink")[拜仁慕尼黑](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")。在拜仁慕尼黑，里贝里成为了球队的组织核心，在边路的冲杀威力十足。

他可胜任边锋与攻击性中场，作为翼锋的他同时兼具有宽阔的视野和出色的传球技术，在比赛中不时送出致命传输。在該球季拜仁2—1擊敗禾夫斯堡的聯賽中，列貝利助攻隊友克洛澤打入首開紀錄的一球。在德国杯的比赛中，里贝里射入两球贡献五次助攻。而他的首颗入球是于2008年2月27日在拜仁战胜同城死敌慕尼黑1860队时于比赛补时最后时刻打入的罚球。

2008年8月7日，當選2007-08年度[德國足球先生](../Page/德國足球先生.md "wikilink")，是第二位贏得這項榮譽的外國球員<small>\[2\]</small><small>\[3\]</small>。

2008—09年球季開始，不斷傳出有关里贝里转会的消息。虽然拜仁屢屢声称里贝里是非卖品，但消息仍言之鑿鑿。为抵制传言，拜仁高層赫内斯甚至宣称除非有球会愿意出资一亿欧元，才會放行。

2009—10年球季，里贝里不得不与左膝盖的跟腱炎斗争，康复后便在对阵多特蒙德的大胜中贡献一颗任意球。十月初，跟腱炎症又一次影响到里贝里的状态并导致里贝里缺席2009年剩余的所有比赛，包括法国对阵爱尔兰的比赛。

里贝里再次归队是于2010年1月23日作为替补登场，此役拜仁3—2战胜云达不莱梅。2月10日，里贝里攻入2010年的首颗入球，帮助球队在德国杯比赛中6—2击败菲尔特。

2010年3月31日，欧冠四分之一决赛首回合拜仁对曼联的比赛中，里贝里攻入一颗扳平比分的任意球，此任意球是打在曼联前锋[朗尼身上折射飞入网窝](../Page/朗尼.md "wikilink")。最终拜仁2—1击败曼联。拜仁于下一场对阵沙尔克04的比赛中里贝里首开纪录，帮助球队2—1获胜。

2013年，里贝里击败C朗拿度及美斯获得欧洲最佳球员。但在該年度金球獎中僅得第三名。

## 軼聞

2010年4月19日，里贝里被法國司法部門傳召問話，傳召內容與里贝里於法國一間夜總會與未成年妓女發生性行為有關\[4\]。

據報導，里貝里曾向媒體自稱祖先為中國人，國內報章更繪影繪聲地指里貝里是明朝開元太祖[朱元璋的後嗣](../Page/朱元璋.md "wikilink")，因[建文帝被](../Page/建文帝.md "wikilink")[朱棣篡位逃亡到歐洲](../Page/朱棣.md "wikilink")，但有關的說法從未獲得證實\[5\]。

里貝里脾氣非常不好，在球會與國家隊都有跟隊友或教練吵架的紀錄。

## 职业生涯统计

### 国际比赛进球

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>日期</p></th>
<th><p>场地</p></th>
<th><p>对阵</p></th>
<th><p>进球比分</p></th>
<th><p>最终比分</p></th>
<th><p>赛事</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>2006-6-27</p></td>
<td><p><a href="../Page/AWD球場.md" title="wikilink">AWD球場</a>, 德國<a href="../Page/漢諾威.md" title="wikilink">漢諾威</a></p></td>
<td></td>
<td><center>
<p>1 – <strong>1</strong></p></td>
<td><center>
<p>1 – 3</p></td>
<td><p><a href="../Page/2006年世界杯足球赛.md" title="wikilink">2006年世界杯</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>2007-6-2</p></td>
<td><p><a href="../Page/法蘭西體育場.md" title="wikilink">法蘭西體育場</a>, 法國<a href="../Page/聖但尼_(塞納-聖但尼省).md" title="wikilink">聖但尼</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>2 – 0</p></td>
<td><p><a href="../Page/2008年欧洲足球锦标赛外围赛.md" title="wikilink">2008年欧洲杯预选赛</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2008-3-26</p></td>
<td><p><a href="../Page/法蘭西體育場.md" title="wikilink">法蘭西體育場</a>, 法國<a href="../Page/聖但尼_(塞納-聖但尼省).md" title="wikilink">聖但尼</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>1 – 0</p></td>
<td><p>友谊赛</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>2008-6-3</p></td>
<td><p><a href="../Page/法蘭西體育場.md" title="wikilink">法蘭西體育場</a>, 法國<a href="../Page/聖但尼_(塞納-聖但尼省).md" title="wikilink">聖但尼</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>1 – 0</p></td>
<td><p>友谊赛</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>2008-10-11</p></td>
<td><p><a href="../Page/法劳体育场.md" title="wikilink">法劳体育场</a>, 罗马尼亚<a href="../Page/康斯坦察.md" title="wikilink">康斯坦察</a></p></td>
<td></td>
<td><center>
<p>2 – <strong>1</strong></p></td>
<td><center>
<p>2 – 2</p></td>
<td><p><a href="../Page/2010年世界杯外围赛_(欧洲区).md" title="wikilink">2010年世界杯预选赛</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2009-3-28</p></td>
<td><p><a href="../Page/大流士和吉雷德克纳普斯体育场.md" title="wikilink">大流士和吉雷德克纳普斯体育场</a>, 立陶宛<a href="../Page/考那斯.md" title="wikilink">考那斯</a></p></td>
<td></td>
<td><center>
<p>0 – <strong>1</strong></p></td>
<td><center>
<p>0 – 1</p></td>
<td><p>2010年世界杯预选赛</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2009-4-1</p></td>
<td><p><a href="../Page/法蘭西體育場.md" title="wikilink">法蘭西體育場</a>, 法國<a href="../Page/聖但尼_(塞納-聖但尼省).md" title="wikilink">聖但尼</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>1 – 0</p></td>
<td><p>2010年世界杯预选赛</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2012-5-27</p></td>
<td><p><a href="../Page/埃诺球场.md" title="wikilink">埃诺球场</a>, 法国<a href="../Page/瓦朗謝訥.md" title="wikilink">瓦朗謝訥</a></p></td>
<td></td>
<td><center>
<p><strong>2</strong> – 2</p></td>
<td><center>
<p>3 – 2</p></td>
<td><p>友谊赛</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2012-5-31</p></td>
<td><p><a href="../Page/奥古斯特·德劳内二世球场.md" title="wikilink">奥古斯特·德劳内二世球场</a>, 法国<a href="../Page/兰斯.md" title="wikilink">兰斯</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>2 – 0</p></td>
<td><p>友谊赛</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2012-6-5</p></td>
<td><p><a href="../Page/MM竞技场.md" title="wikilink">MM竞技场</a>, 法国<a href="../Page/勒芒.md" title="wikilink">勒芒</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>4 – 0</p></td>
<td><p>友谊赛</p></td>
</tr>
</tbody>
</table>

## 榮譽

**[加拉塔沙雷](../Page/加拉塔沙雷.md "wikilink")**

  - [土耳其盃](../Page/土耳其盃.md "wikilink")：2004-05年

'''[馬賽](../Page/馬賽足球俱樂部.md "wikilink")

  - [-{zh-hans:国际托托杯; zh-hk:圖圖盃;}-](../Page/歐洲足協圖圖盃.md "wikilink")：2005年

'''[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")

  - [德國联赛盃](../Page/德國足球協會聯盟盃.md "wikilink") 冠軍：2007年
  - [德國盃](../Page/德國盃.md "wikilink") 冠軍：2008年；2010年；2013年
  - [德國甲組足球聯賽](../Page/德甲.md "wikilink") 冠軍：2007/08年；2009/10年；2012/13年
  - [德國超級盃冠軍](../Page/德國超級盃.md "wikilink")：2008年；2010年
  - [歐洲聯賽冠軍盃冠軍](../Page/歐洲聯賽冠軍盃.md "wikilink")：[2012/2013年](../Page/2012/2013年.md "wikilink")
  - [欧洲超级杯冠军](../Page/欧洲超级杯.md "wikilink")：2013年

**[法國國家隊](../Page/法國國家足球隊.md "wikilink")**

  - [世界盃](../Page/2006年世界盃足球賽.md "wikilink") 亞軍：2006年

**個人**

  - [法國足球先生](../Page/法國足球先生.md "wikilink")：2007年
  - [德國足球先生](../Page/德國足球先生.md "wikilink")：2008年
  - [欧洲足联年度最佳阵容](../Page/欧洲足联年度最佳阵容.md "wikilink")：2008年
  - [国际足联年度最佳阵容](../Page/国际足联年度最佳阵容.md "wikilink")：2008年
  - [欧洲最佳球员](../Page/欧洲最佳球员.md "wikilink")：2013年

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [里贝里官方網站](http://www.ribery-franck.com/)
  - [里贝里球迷網站](http://www.frankribery.azplayers.com/)

[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:法國國家足球隊球員](../Category/法國國家足球隊球員.md "wikilink")
[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:布羅尼球員](../Category/布羅尼球員.md "wikilink")
[Category:比斯特球員](../Category/比斯特球員.md "wikilink")
[Category:梅斯球員](../Category/梅斯球員.md "wikilink")
[Category:加拉塔沙雷球員](../Category/加拉塔沙雷球員.md "wikilink")
[Category:馬賽球員](../Category/馬賽球員.md "wikilink")
[Category:拜仁慕尼黑球員](../Category/拜仁慕尼黑球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:土耳其外籍足球運動員](../Category/土耳其外籍足球運動員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:法國旅外足球運動員](../Category/法國旅外足球運動員.md "wikilink")
[Category:在德國的法國人](../Category/在德國的法國人.md "wikilink")
[Category:在土耳其的法國人](../Category/在土耳其的法國人.md "wikilink")
[Category:法國穆斯林](../Category/法國穆斯林.md "wikilink")
[Category:改信伊斯蘭教者](../Category/改信伊斯蘭教者.md "wikilink")
[Category:加來海峽省人](../Category/加來海峽省人.md "wikilink")

1.
2.  [力压巴拉克和托尼 里贝里当选足球先生](http://www.dfo.cn/dfo/show.asp?id=8158)
3.  [法國籍瑞貝里當選德國年度足球先生](http://hk.sports.yahoo.com/080807/167/2yxxx.html)
4.  [法國腳列貝利涉嫖雛妓被查 罪成囚3年
    阻法軍捧世盃希望](http://hk.news.yahoo.com/article/100419/4/hkya.html)
5.  \[<http://www.hjenglish.com/fr/p528855/新欧洲最佳球员里贝里：朱元璋的后代>？\]