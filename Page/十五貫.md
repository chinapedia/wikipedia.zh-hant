《**十五貫**》是著名[崑劇劇目](../Page/崑劇.md "wikilink")，屬於「[江湖十八本](../Page/江湖十八本.md "wikilink")」的劇目。其故事收录于[明末](../Page/明朝.md "wikilink")[馮夢龍](../Page/馮夢龍.md "wikilink")《[醒世恆言](../Page/醒世恆言.md "wikilink")》的“十五貫戲言成巧禍”，被[清代](../Page/清代.md "wikilink")[劇作家](../Page/劇作家.md "wikilink")[朱素臣改编为傳奇](../Page/朱素臣.md "wikilink")《[雙熊夢](../Page/雙熊夢.md "wikilink")》，後來被多番改編為[京劇](../Page/京劇.md "wikilink")、[秦腔](../Page/秦腔.md "wikilink")、[粵劇](../Page/粵劇.md "wikilink")、[潮劇及](../Page/潮劇.md "wikilink")[歌仔戲的劇目](../Page/歌仔戲.md "wikilink")。因為[崑劇](../Page/崑劇.md "wikilink")《十五貫》的首次演出轟動了全國，所以重振了[崑劇的地位](../Page/崑劇.md "wikilink")。

## 剧情

馮夢龍所記，原是一個宋代的悲劇，強盜殺人，搶奪了[銅錢十五](../Page/銅錢.md "wikilink")[貫](../Page/贯.md "wikilink")，而官吏昏昧不明，造成冤獄，錯殺了兩個好人。經過劇作家的改編，已有較喜樂的結尾。

故事發生於[明代](../Page/明代.md "wikilink")。[屠夫尤葫蘆從皋橋親戚家借得](../Page/屠夫.md "wikilink")[銅錢十五](../Page/銅錢.md "wikilink")[貫](../Page/贯.md "wikilink")，卻無聊[惡作劇](../Page/惡作劇.md "wikilink")，哄騙[繼女蘇戌娟說是她的賣身錢](../Page/繼女.md "wikilink")，以如此少的價格將蘇賣給別人當[婢女](../Page/婢女.md "wikilink")，蘇認為受辱，於是深夜私逃投親。婁阿鼠闖入尤家偷了十五貫銅錢，並用肉斧殺尤滅口。鄰人發現後就一面報官，一面追趕凶手。另一方面，客商陶復朱的伙計熊友蘭，帶十五貫錢往[常州替哥哥熊友蕙交付被誤會的十五貫](../Page/常州.md "wikilink")，遇到蘇戌娟問路，二人順路，因此同行。鄰人與[差役見二人同行](../Page/差役.md "wikilink")，又見十五貫，疑其為凶手，婁阿鼠乘機誣陷，於是二人被押送[無錫縣](../Page/無錫縣.md "wikilink")[衙門](../Page/衙門.md "wikilink")。[無錫](../Page/無錫.md "wikilink")[知縣郭於執](../Page/知縣.md "wikilink")（一作「過於執」，皆假名，指「過於執著」）判蘇、熊二人死刑。[常州](../Page/常州.md "wikilink")[知府](../Page/知府.md "wikilink")[莫愚和](../Page/莫愚.md "wikilink")[應天巡撫](../Page/應天巡撫.md "wikilink")[周忱皆輕信知縣](../Page/周忱.md "wikilink")（史實上莫愚、周忱真有其人，都是以聰明廉潔著稱）。[吳郡](../Page/吳郡.md "wikilink")[太守](../Page/太守.md "wikilink")[況鍾在](../Page/況鍾.md "wikilink")[城隍廟過夜](../Page/城隍廟.md "wikilink")，卻被[城隍爺](../Page/城隍爺.md "wikilink")[示夢](../Page/託夢.md "wikilink")，認為此案有天大的冤屈，連夜趕往都府，求[巡撫](../Page/巡撫.md "wikilink")[周忱緩判](../Page/周忱.md "wikilink")，重啟調查。周忱因迂腐而不為所動。況據理力爭後，周無奈，限期十五日內清查回報，否則上奏[彈劾](../Page/彈劾.md "wikilink")。況鍾冒著丟官的風險，親至[無錫調查](../Page/無錫.md "wikilink")，又改扮私訪，將真凶婁阿鼠捉住，終於案情大白。

## 历史

浙江省《十五貫》整理小組在[田漢支援下](../Page/田漢.md "wikilink")，由[陳靜執筆](../Page/陳靜.md "wikilink")，根據[清代](../Page/清代.md "wikilink")[劇作家](../Page/劇作家.md "wikilink")[朱素臣的傳奇](../Page/朱素臣.md "wikilink")《雙熊夢》改編。

1956年4月17日，由國風蘇劇團（[浙江崑劇團的前身](../Page/浙江崑劇團.md "wikilink")）\[1\]在[北京](../Page/北京.md "wikilink")[中南海懷仁堂首次演出](../Page/中南海.md "wikilink")。[周傳瑛飾況鐘](../Page/周傳瑛.md "wikilink")、[王傳淞飾婁阿鼠](../Page/王傳淞.md "wikilink")、[朱國梁飾過於執](../Page/朱國梁.md "wikilink")、[包傳鐸飾周忱](../Page/包傳鐸.md "wikilink")。在北京的四十六場連演，觀眾高達七萬人次，以其高度的藝術性轟動全國，\[2\]，並且得到[周恩來與](../Page/周恩來.md "wikilink")[毛澤東讚賞](../Page/毛澤東.md "wikilink")\[3\]，讓崑劇重新得到欣賞\[4\]。1956年5月18日，《人民日報》為此發表《從“一齣戲救活了一個劇種”談起》的社論。同年，《十五貫》被攝製成彩色戲曲藝術影片。劇本被收入《戲曲選》第
1卷(1958)與《中國地方戲曲集成·浙江省卷》(1959)。[崑劇著名人物](../Page/崑劇.md "wikilink")[周傳瑛](../Page/周傳瑛.md "wikilink")、[王傳淞也曾於](../Page/王傳淞.md "wikilink")1950年代改編《十五貫》。[京劇版的](../Page/京劇.md "wikilink")《十五貫》由[辜懷群](../Page/辜懷群.md "wikilink")、[李寶春改編](../Page/李寶春.md "wikilink")。\[5\]

## 版本

[朱素臣的原著](../Page/朱素臣.md "wikilink")《十五貫》共分26場：《開場》、《泣別》、《鼠竊》、《得環》、《摧花》、《餌毒》、《陷阱》、《商助》、《竊貫》、《誤拘》、《如詳》、《獄晤》、《夢警》、《阱淚》、《夜訊》、《乞命》、《踏勘》、《廉訪》、《擒奸》、《恩判》、《請罪》……《雙圓》。

浙江省《十五貫》整理小組經過多次刪改，剩下來的場次只有9《竊貫》、10《誤拘》、11《如詳》、15《夜訊》、16《乞命》、17《踏勘》、18《廉訪》、20《恩判》。這就成了現今的《鼠禍》、《受嫌》、《被冤》、《判斬》、《見都》、《疑鼠》、《訪鼠》、《審鼠》。

秦腔《十五貫》共有十六場。京劇《十五貫》（車王府鈔本）則忠於原著。1956年京劇《十五貫》則有《商贈》、《殺尤》、《橋會》、《審案》、《見都》、《踏勘》、《訪鼠》、《審鼠》八場。

河洛歌仔戲《十五貫》共有十場：《夜訪》、《離家》、《鼠禍》、《受嫌》、《被冤》、《判斬》、《見都》、《疑鼠》、《訪鼠》。\[6\]

## 注釋

<references />

## 參考文獻

  - [中國大百科全書](../Page/中國大百科全書.md "wikilink")

## 外部連結

  - [浙江崑蘇劇團
    經典代表劇目《十五貫》](https://web.archive.org/web/20060717034629/http://www.zjkjt.com/daye/jumujieshao.htm)

[category:戏曲作品](../Page/category:戏曲作品.md "wikilink")
[category:京剧剧目](../Page/category:京剧剧目.md "wikilink")
[category:昆曲剧目](../Page/category:昆曲剧目.md "wikilink")
[category:秦腔剧目](../Page/category:秦腔剧目.md "wikilink")

[Category:粵劇劇目](../Category/粵劇劇目.md "wikilink")
[S十](../Category/明朝背景作品.md "wikilink")

1.  [香港康文署：浙江崑劇團簡介](http://www.lcsd.gov.hk/CE/CulturalService/Programme/b5/chinese_opera/apr05/opera1.html)

2.  [浙江崑蘇劇團 經典代表劇目《十五貫》](http://www.zjkjt.com/daye/jumujieshao.htm)
3.  《周恩來選集》，1956年4月19日與1956年5月17日的講話（[周恩來關於崑曲《十五貫》的兩次講話](http://big5.china.com.cn/chinese/zhuanti/211423.htm)）
4.  [昆曲《十五贯》：五十年不老](http://culture.people.com.cn/GB/40473/40476/4386423.html)
5.  [新舞台:2004年新老戲首頁](http://www.novelhall.org.tw/tradition/ntp_2004_main.asp)

6.  [台灣大百科全書:十五貫](http://taipedia.cca.gov.tw/Show/ShowDetail.aspx?TextID=1774)