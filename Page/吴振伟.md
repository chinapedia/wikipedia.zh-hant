**吳振偉**（[英文名](../Page/英文.md "wikilink")：David
Wu，），出生于[台湾](../Page/台湾.md "wikilink")[新竹市](../Page/新竹市.md "wikilink")（當時屬於[新竹縣](../Page/新竹縣.md "wikilink")），[美國](../Page/美國.md "wikilink")[聯邦眾議員](../Page/美國眾議院.md "wikilink")，代表[俄勒岡州第一選區](../Page/俄勒岡州.md "wikilink")（[波特蘭西北](../Page/波特蘭.md "wikilink")），[民主党籍](../Page/民主黨_\(美國\).md "wikilink")。為美國史上首位台裔[國會議員](../Page/美國國會.md "wikilink")。2011年，涉入[性醜聞案後辭職](../Page/性醜聞案.md "wikilink")。

## 簡介

吳振偉在[台灣](../Page/台灣.md "wikilink")[新竹市出生](../Page/新竹市.md "wikilink")，於1961年隨全家移民美國，畢業於[史丹佛大學生物學系](../Page/史丹佛大學.md "wikilink")，後就读于[哈佛醫學院](../Page/哈佛醫學院.md "wikilink")，但未完成學業，其後就讀[耶魯法學院](../Page/耶魯法學院.md "wikilink")，獲耶魯大學[法律博士](../Page/法律博士.md "wikilink")（J.D.）。

1998年期中選舉，吳首度当选联邦众议员，其后又六度连任成功。部分人士認為其立場傾向於親近臺灣\[1\]。吳曾推动美国十年一度的[人口普查中](../Page/人口普查.md "wikilink")，将「[臺灣人](../Page/臺灣人.md "wikilink")」之項目从「[中國人](../Page/中國人.md "wikilink")」中移出，獨立成为一个族群。同时，吴也曾與同黨的[日裔](../Page/日本人.md "wikilink")[加州聯邦眾議員](../Page/加州.md "wikilink")[迈克·本田等人共同推动议案](../Page/迈克·本田.md "wikilink")，要求[日本政府正视](../Page/日本政府.md "wikilink")[慰安妇问题](../Page/慰安妇.md "wikilink")。

吳是「美國國會亞太裔小組」核心成員，並曾於2001年1月至2004年1月間擔任該組織主席。吳同時為聯邦眾議院「新民主黨人聯盟」成員，該組織為[中間派民主黨人組成的國會次團](../Page/中間派.md "wikilink")，相對於左傾的自由派或進步派民主黨人而言，該組織的核心理念為溫和的社會文化立場，以及經濟[新自由主義與財政](../Page/新自由主義.md "wikilink")[保守主義](../Page/保守主義.md "wikilink")。

2011年2月，據[俄岡勒人報報導](../Page/俄岡勒人報.md "wikilink")，在參選[2010年眾議院選舉期間](../Page/2010年美國中期選舉.md "wikilink")，吳振偉出現行為異常，報導指稱工作人員要求他至醫院尋求專業精神治療\[2\]。不久後，包括一直協助他的幕僚長和另一傳訊主任在內的至少六名職員離去\[3\]；稍後，吳在一份聲明中表示他已經尋求了「專業的醫療照顧」，以治療因其政治競選及作為單身父親所帶來的壓力\[4\]\[5\]。

2011年7月22日，《俄勒岡人報》披露，吳振偉涉嫌在2010年[感恩節期間於](../Page/感恩節.md "wikilink")[加州性侵一名](../Page/加州.md "wikilink")18歲青年女性。吳青伟稱該事件為兩廂情願。聯邦眾議院少數黨領袖[蘭希·佩洛西女士要求對這項指控進行調查](../Page/蘭希·佩洛西.md "wikilink")。同月26日吳振偉宣佈將在國會解決目前美國債務危機問題後辭職\[6\]。

## 參考資料

[Category:美国民主党联邦众议员](../Category/美国民主党联邦众议员.md "wikilink")
[Category:美国华裔国会议员](../Category/美国华裔国会议员.md "wikilink")
[Category:美國民主黨員](../Category/美國民主黨員.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:美國新教徒](../Category/美國新教徒.md "wikilink")
[Category:歸化美國公民的中華民國人](../Category/歸化美國公民的中華民國人.md "wikilink")
[Category:移民美國的中華民國人](../Category/移民美國的中華民國人.md "wikilink")
[Category:新竹縣人](../Category/新竹縣人.md "wikilink")
[Zhen振](../Category/吳姓.md "wikilink")

1.

2.

3.

4.
5.

6.  [美國華裔議員陷性醜聞宣佈將辭職](http://www.bbc.co.uk/zhongwen/trad/world/2011/07/110726_us_david_wu.shtml)