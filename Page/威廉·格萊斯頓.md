**威廉·尤尔特·格莱斯顿**（**William Ewart Gladstone** ），英国自由党政治家，曾四度出任首相，以善于理财著称。

## 早年

格莱斯顿的父亲约翰是由[伦敦迁至](../Page/伦敦.md "wikilink")[利物浦从事远洋贸易的商人](../Page/利物浦.md "wikilink")，事业相当成功，透过运输谷物、食糖、奴隸积聚了巨额财富。至1828年，约翰的资产已达502,550英镑。\[1\]致富后，他将金钱转化为地位，于[苏格兰东岸](../Page/苏格兰.md "wikilink")[阿伯丁一带置业](../Page/阿伯丁.md "wikilink")，并且担任了多个[腐败选区的国会议员](../Page/腐败选区.md "wikilink")。1846年，他更在[罗伯特·皮尔爵士推荐之下获封为从男爵](../Page/罗伯特·皮尔.md "wikilink")。\[2\]

## 擔任首相

1868年大選，南蘭開夏選區被[1867年改革法令分開成西南及東南選區](../Page/1867年改革法令.md "wikilink")。格萊斯頓決定同時於西蘭開夏選區及格林威治選區競選，結果出乎意料地在自己以往的選區敗給保守黨的，但於後者當選成功進入國會，開始首屆首相任期。

[Gladstone_debate_on_Irish_Home_Rule_8th_April_1886_ILN.jpg](https://zh.wikipedia.org/wiki/File:Gladstone_debate_on_Irish_Home_Rule_8th_April_1886_ILN.jpg "fig:Gladstone_debate_on_Irish_Home_Rule_8th_April_1886_ILN.jpg")辯論中發言的畫像，繪於1886年4月8日。\]\]

## 参考文献

### 引用

### 来源

  - H. C. G. Matthew, *Gladstone. 1809–1874* (Oxford University Press,
    1988); *Gladstone. 1875–1898* (Oxford University Press, 1995).

{{-}}

[Category:英国首相](../Category/英国首相.md "wikilink")
[Category:英國財政大臣](../Category/英國財政大臣.md "wikilink")
[Category:牛津大学基督堂学院校友](../Category/牛津大学基督堂学院校友.md "wikilink")
[Category:皇家统计学会会长](../Category/皇家统计学会会长.md "wikilink")

1.  Matthew, 1986, p.3
2.  Matthew, 1986, p.4