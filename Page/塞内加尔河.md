**塞内加尔河**（、）位于[非洲西部](../Page/非洲.md "wikilink")，由[巴科伊河和](../Page/巴科伊河.md "wikilink")[巴芬河在](../Page/巴芬河.md "wikilink")[马里的](../Page/马里.md "wikilink")[巴富拉贝汇流而成多腦河](../Page/巴富拉贝.md "wikilink")，向東流淌，构成[毛里塔尼亚和](../Page/毛里塔尼亚.md "wikilink")[塞内加尔的边境](../Page/塞内加尔.md "wikilink")，最终在[圣路易注入](../Page/圣路易_\(塞内加尔\).md "wikilink")[大西洋](../Page/大西洋.md "wikilink")，全长1790[公里](../Page/公里.md "wikilink")。

## 外部链接

  - [Map of the Senegal River basin at Water esources
    eAtlas](https://web.archive.org/web/20080110130106/http://www.waterandnature.org/eatlas/html/af23.html)
  - [the Hydrology of Senegal (powerpoint
    presentation)](https://web.archive.org/web/20070629065341/http://zope0.itcilo.org/delta/lmdd2003/news/1067611432/1068825691/presentation%20ressources%20en%20eau%20senegal.ppt)

[Category:非洲跨國河流](../Category/非洲跨國河流.md "wikilink")
[\*](../Category/塞內加爾河.md "wikilink")
[Category:馬利河流](../Category/馬利河流.md "wikilink")
[Category:塞内加尔河流](../Category/塞内加尔河流.md "wikilink")
[Category:毛里塔尼亚河流](../Category/毛里塔尼亚河流.md "wikilink")
[Category:茅利塔尼亞-塞內加爾邊界](../Category/茅利塔尼亞-塞內加爾邊界.md "wikilink")