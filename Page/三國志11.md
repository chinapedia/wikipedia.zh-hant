**三國志11**是日本[光榮公司研發銷售的一款](../Page/光榮.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")。為[三國志系列的第](../Page/三國志系列.md "wikilink")11作。音樂由[池賴廣擔當](../Page/池賴廣.md "wikilink")。個人電腦用版本發表之后，又發表了[PS2和](../Page/PS2.md "wikilink")[Wii的版本](../Page/Wii.md "wikilink")。與前10作使用[羅馬數字的表記](../Page/羅馬數字.md "wikilink")[名稱不同](../Page/名稱.md "wikilink")，這一作使用[阿拉伯數字來表示](../Page/阿拉伯數字.md "wikilink")。

與三國志X這款[武將角色扮演類遊戲不同](../Page/武將.md "wikilink")，三國志11再次回到了[戰略類遊戲](../Page/戰略.md "wikilink")，如《[三國志IX](../Page/三國志IX.md "wikilink")》一樣十日制。與《[三國志IX](../Page/三國志IX.md "wikilink")》、《[三國志X](../Page/三國志X.md "wikilink")》一樣，這次也是使用一張[地圖來體現全](../Page/地圖.md "wikilink")[中國的風貌](../Page/中國.md "wikilink")，并且采用了[標清電視](../Page/標清電視.md "wikilink")[技術以及](../Page/技術.md "wikilink")[漢語](../Page/漢語.md "wikilink")[配音](../Page/配音.md "wikilink")（在威力加強版中可以選擇[日語](../Page/日語.md "wikilink")）。

武將數量共有670人，刪除了前作武將，如：交州士氏([士燮](../Page/士燮.md "wikilink"))一族人和一些外民族；女性武將也增加。另外也刪除前作存在的交州。

內政的時候如果要增加收益或增加兵種的話，就要建築相應的建築物來獲取，武將的政治數據高，建築工程就很快結束。

本作武將的特技只能擁有一個，甚至有些武將是沒特技的。戰爭時要組成一個部隊的時候，部隊中武將數量最多只能擁有三位，那就是主將一位，副將二位。在單挑的時候，若武將有攜帶副將，副將就會隨主將一起與敵方單挑。

特別的是有些武將有兩個樣子，大多數的武將因為年老而在武將年邁的時候更改樣子，如：[曹操](../Page/曹操.md "wikilink")，[劉備](../Page/劉備.md "wikilink")，[趙雲](../Page/趙雲.md "wikilink")，[孫權等等的人物](../Page/孫權.md "wikilink")，特別的是新增了[夏侯惇左眼還未受傷的樣子](../Page/夏侯惇.md "wikilink")。

## 劇本

帶☆的是虛構劇本。威力加強版中的「女流之戰」必須完成制霸模式后才能進行遊戲。

### 本体劇本

1.  184年 1月　[黄巾之乱](../Page/黄巾之乱.md "wikilink")
2.  190年 1月　反[董卓聯合軍](../Page/董卓.md "wikilink")
3.  194年 6月　群雄割據
4.  200年 1月　[官渡之戰](../Page/官渡之戰.md "wikilink")
5.  207年 9月　[三顧茅廬](../Page/三顧茅廬.md "wikilink")
6.  211年 7月　[劉備入蜀](../Page/劉備.md "wikilink")
7.  225年 7月　[南蠻征伐](../Page/南蠻.md "wikilink")
8.  251年 1月　英雄集結 ☆

### PK劇本

1.  198年 1月　[呂布討伐戰](../Page/呂布.md "wikilink")
2.  203年 1月　袁家之戰
3.  217年 7月　[漢中争奪戰](../Page/漢中.md "wikilink")
4.  187年 4月　[何進包圍網](../Page/何進.md "wikilink") ☆
5.  191年 7月　局勢掌控者 ☆
6.  251年 1月　女流之戰 ☆
7.  208年 10月　赤壁之戰 ☆ 限PS2、Wii威力加強版
8.  251年 1月　英雄亂舞 ☆ 限PS2、Wii威力加強版

## 外部連結

  - [三國志11官方網站](https://web.archive.org/web/20090421024834/http://gamecity.koei.com.tw/sangokushi/11/)
  - [三國志11官方網站](http://www.gamecity.ne.jp/sangokushi/11/)
  - [三國志11用戶網頁](https://web.archive.org/web/20090422183345/http://gamecity.koei.com.tw/user/san11/)
  - [三國志11用戶網頁](http://www.gamecity.ne.jp/regist_c/user/san11/)
  - [三国题材游戏大全 -
    三國志11专区](http://www.sanguogame.com.cn/special/san11/san11.html)

[Category:2006年电子游戏](../Category/2006年电子游戏.md "wikilink")
[11](../Category/三國志系列.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:Wii遊戲](../Category/Wii遊戲.md "wikilink")
[Category:官方中文化游戏](../Category/官方中文化游戏.md "wikilink")