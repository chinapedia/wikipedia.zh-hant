**兴都库什山**（）是位于[中亚](../Page/中亚.md "wikilink")，东西向横贯[阿富汗的山脉](../Page/阿富汗.md "wikilink")。

興都庫什此名稱的由來還沒有統一的說法，有許多學者、作家提出不同的理論。在[亞歷山大大帝的時代](../Page/亞歷山大大帝.md "wikilink")，其被認為是「印度的[高加索山](../Page/高加索山.md "wikilink")」或「[印度河的高加索山](../Page/印度河.md "wikilink")」（相對於裏海與黑海間的[高加索地區](../Page/高加索.md "wikilink")），因此過去的記錄者認為興都庫什這個名稱可能是因此而衍生出來的。Hindū
Kūh（ھندوکوه）與 Kūh-e Hind（کوهِ
ھند）通常用來指稱隔開從喀布爾河、赫爾曼德河到阿姆河中間盆地的整個區域，或者說是指座落在喀布爾河的西北方區域。

东端较高，属于[帕米尔高原的延伸](../Page/帕米尔高原.md "wikilink")，海拔大约在4,500－6,000米之间；西端大约为3,500－4,000米。山脉总长大约966公里。

## 地震

  - [2002年興都庫什山脈地震](../Page/2002年興都庫什山脈地震.md "wikilink")
  - [2005年興都庫什山脈地震](../Page/2005年興都庫什山脈地震.md "wikilink")
  - [2011年興都庫什山脈地震](../Page/2011年興都庫什山脈地震.md "wikilink")
  - [2015年興都庫什山脈地震](../Page/2015年興都庫什山脈地震.md "wikilink")

## 参考文献

## 參見

  - [帕米尔高原](../Page/帕米尔高原.md "wikilink")

{{-}}

[兴都库什山脉](../Category/兴都库什山脉.md "wikilink")
[Category:帕米爾高原](../Category/帕米爾高原.md "wikilink")
[Category:伊朗高原山脉](../Category/伊朗高原山脉.md "wikilink")
[Category:亚洲跨国山脉](../Category/亚洲跨国山脉.md "wikilink")
[Category:巴基斯坦山脈](../Category/巴基斯坦山脈.md "wikilink")
[Category:阿富汗山脈](../Category/阿富汗山脈.md "wikilink")
[Category:南亚山脉](../Category/南亚山脉.md "wikilink")