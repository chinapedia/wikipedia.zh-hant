**IE Tab** 是[网页浏览器](../Page/网页浏览器.md "wikilink")[Mozilla
Firefox和](../Page/Mozilla_Firefox.md "wikilink")[Google
Chrome的一个](../Page/Google_Chrome.md "wikilink")[擴充套件](../Page/Firefox擴充套件列表.md "wikilink")，能实现在Firefox调用[Internet
Explorer的](../Page/Internet_Explorer.md "wikilink")[排版引擎浏览网页](../Page/排版引擎.md "wikilink")。

它解决很多使用不规范语言、包含[ActiveX或者要求](../Page/ActiveX.md "wikilink")[IE
Only的网页浏览问题](../Page/IE_Only.md "wikilink")。不過，隨著越來越多網頁設計師遵循[網頁標準](../Page/網頁標準.md "wikilink")，IE
Tab已經不常使用。

## 運作原理

由於[Microsoft在發行](../Page/Microsoft.md "wikilink")[Windows時已經把](../Page/Windows.md "wikilink")[Internet
Explorer捆綁在作業系統內](../Page/Internet_Explorer.md "wikilink")，使用者亦不能移除，故**IE
Tab**利用這些已捆綁的檔案來執行，模擬使用Internet Explorer。

## 優點和缺點

### 優點

相比起重新開一個[Internet
Explorer視窗](../Page/Internet_Explorer.md "wikilink")，使用**IE
Tab**方便得多\[1\]。除了較為方便之外，亦可佔用較少系統資源。對於網頁設計師而言，這更為方便──一次便可以測試最普及的兩款瀏覽器\[2\]的表現，令希望設計相容所有（或大部份）瀏覽器的網頁設計師能省下很多時間。

### 缺點

绝大多数和網頁有關的[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")-{zh-hans:扩展;
zh-hant:附加元件;}-在使用IE tab浏览的-{zh-hans:标签页;
zh-hant:分頁;}-内无法使用；而採用IE
Tab時按下外部連結亦會使用模擬出來的[Internet
Explorer開啟](../Page/Internet_Explorer.md "wikilink")，要再次切換較麻煩。某些電腦採用IE
Tab容易出現瀏覽器崩潰現象。

## IE Tab Plus

IE Tab Plus是一个从[IE Tab分支出来的附加元件](../Page/IE_Tab.md "wikilink")，属于IE
Tab的改进加強版本，它继承了IE
Tab在Firefox中调用IE核心的功能，对于Web开发人员除錯网页来说，顯示在不同浏览器下的相容性更加方便，而且对于普通的使用者，当遇到Firefox不能正常浏览的网页的时候，也可以很方便的切换到IE来显示。

### 相比IE Tab的改进

1.  支援[Adblock
    Plus](../Page/Adblock_Plus.md "wikilink")，这样在IE引擎裡也可以用Adblock
    Plus过滤广告了。
2.  支援[Cookie](../Page/Cookie.md "wikilink")，这样在Firefox和IE之间切换的时候，不用再重新登录了。

### 相容性

  - 和IE Tab不相容，使用之前须停用或移除IE Tab。

## 參見

  - [Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink")

## 注釋和參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [Firefox Add-ons 上的 IE
    Tab](https://addons.mozilla.org/en-US/firefox/addon/ie-tab/)（已支援
    Firefox 10+ ver:2.0.20120203）

<!-- end list -->

  - [IE Tab 官方網站](http://ietab.mozdev.org/)
  - [IE Tab 2](https://addons.mozilla.org/firefox/addon/92382)（FireFox
    3.6以上版本適用，非原作，係 ietab.net 之分支版本）

[Category:Firefox 附加组件](../Category/Firefox_附加组件.md "wikilink")

1.  用戶只須按下一個按鈕，即可在[Firefox視窗內](../Page/Firefox.md "wikilink")「切換」成為「Firefox」模式或「Internet
    Explorer」模式
2.  參見[Mozilla
    Firefox\#市場接納度](../Page/Mozilla_Firefox#市場接納度.md "wikilink")