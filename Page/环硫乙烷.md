**环硫乙烷**，也称**硫杂环丙烷**、**噻丙环**，是一个三元[杂环化合物](../Page/杂环化合物.md "wikilink")，由[环氧乙烷中的氧原子被](../Page/环氧乙烷.md "wikilink")[硫原子替换生成](../Page/硫.md "wikilink")，是最简单的[环硫化物](../Page/环硫化物.md "wikilink")，分子式为C<sub>2</sub>H<sub>4</sub>S。\[1\]它是有恶臭的无色至淡黄色液体，难溶于水，易溶于[丙酮](../Page/丙酮.md "wikilink")、[氯仿等有机溶剂](../Page/氯仿.md "wikilink")。用作[有机合成原料](../Page/有机合成.md "wikilink")。

## 制备

它可由[碳酸乙烯酯与无水](../Page/碳酸乙烯酯.md "wikilink")[硫氰酸钾反应制备](../Page/硫氰酸钾.md "wikilink")。\[2\]

  -
    KSCN + C<sub>2</sub>H<sub>4</sub>O<sub>2</sub>CO → KOCN +
    C<sub>2</sub>H<sub>4</sub>S + CO<sub>2</sub>

也可由[硫氰酸盐与](../Page/硫氰酸盐.md "wikilink")[环氧化物反应](../Page/环氧化物.md "wikilink")、β-巯基[醇与](../Page/醇.md "wikilink")[光气缩合后](../Page/光气.md "wikilink")[脱羧](../Page/脱羧.md "wikilink")、β-氯代[硫醇用碱处理成环制备](../Page/硫醇.md "wikilink")：

[Episulfide_Synthesis.png](https://zh.wikipedia.org/wiki/File:Episulfide_Synthesis.png "fig:Episulfide_Synthesis.png")

## 反应

环硫乙烷在酸碱存在下[聚合](../Page/聚合.md "wikilink")，也很容易开环发生[加成](../Page/加成.md "wikilink")。它与[硫醇和](../Page/硫醇.md "wikilink")[醇加成得到](../Page/醇.md "wikilink")[乙二硫醇单](../Page/乙二硫醇.md "wikilink")[醚和β](../Page/醚.md "wikilink")-[巯基乙醇单醚](../Page/巯基乙醇.md "wikilink")，与[胺加成得到](../Page/胺.md "wikilink")[半胱胺](../Page/半胱胺.md "wikilink")，\[3\]用作[螯合配体](../Page/螯合.md "wikilink")。

  -
    C<sub>2</sub>H<sub>4</sub>S + R<sub>2</sub>NH →
    R<sub>2</sub>NCH<sub>2</sub>CH<sub>2</sub>SH

## 参见

  - [噻丙烯](../Page/噻丙烯.md "wikilink")，硫杂[环丙烯](../Page/环丙烯.md "wikilink")
  - [环氧乙烷](../Page/环氧乙烷.md "wikilink") -
    [吖丙啶](../Page/吖丙啶.md "wikilink") -
    [环丙烷](../Page/环丙烷.md "wikilink")

## 参考资料

[Category:硫醚](../Category/硫醚.md "wikilink")
[硫杂丙环](../Category/硫杂丙环.md "wikilink")
[Category:二碳有机物](../Category/二碳有机物.md "wikilink")

1.
2.  Searles, S.; Lutz, E. F.; Hays, H. R.; Mortensen, H. E.
    "Ethylenesulfide" Organic Syntheses, 1973, Collective Volume 5, page
    562.
3.  R. J. Cremlyn “An Introduction to Organosulfur Chemistry” John Wiley
    and Sons: Chichester (1996). ISBN 0-471-95512-4.