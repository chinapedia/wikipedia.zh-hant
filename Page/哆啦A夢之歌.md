<noinclude>

**哆啦A夢之歌**是[朝日電視台版](../Page/朝日電視台.md "wikilink")[哆啦A夢動畫自](../Page/哆啦A夢_\(電視動畫\).md "wikilink")1979年到2005年期間的[片頭曲](../Page/片頭曲.md "wikilink")，也是哆啦A夢的片頭曲中最為人所知的。作詞[楠部工](../Page/楠部工.md "wikilink")、補作詞。作曲[菊池俊輔](../Page/菊池俊輔.md "wikilink")。

## 歷史

  - **[大杉久美子版](../Page/大杉久美子.md "wikilink")**

<!-- end list -->

  -
    演唱者為大杉久美子。自1979年4月2日到1992年10月2日止成為短篇動畫片頭曲，自1982年的『[大雄的大魔境](../Page/大雄的大魔境.md "wikilink")』到1988年的『[大雄的平行西遊記](../Page/大雄的平行西遊記.md "wikilink")』期間也是大長篇電影片頭曲。

<!-- end list -->

  - **[山野智子版](../Page/山野智子.md "wikilink")**

<!-- end list -->

  -
    山野智子演唱。自1992年10月9日到2002年9月20日是短篇動畫主題曲。自1989年大長篇電影『[大雄的日本誕生](../Page/大雄的日本誕生.md "wikilink")』到2004年的『[大雄的貓狗時空傳](../Page/大雄的貓狗時空傳.md "wikilink")』期間也是大長篇電影[主題曲](../Page/主題曲.md "wikilink")。

<!-- end list -->

  - **吉川雛乃版**

<!-- end list -->

  -
    1998年大長篇電影『[大雄的南海大冒險](../Page/大雄的南海大冒險.md "wikilink")』片頭曲。

<!-- end list -->

  - **[維也納少年合唱團版](../Page/維也納少年合唱團.md "wikilink")**

<!-- end list -->

  -
    2000年大長篇電影『[大雄的太陽王傳說](../Page/大雄的太陽王傳說.md "wikilink")』主題曲。

<!-- end list -->

  - **[東京布丁版](../Page/東京布丁.md "wikilink")**

<!-- end list -->

  -
    編曲為[岩戶崇](../Page/岩戶崇.md "wikilink")，演唱為組合[東京布丁](../Page/東京布丁.md "wikilink")。自短篇動畫改用數位方式制作后成為片頭曲。使用時間是2002年10月4日到2003年4月11日。

<!-- end list -->

  - **[渡邊美里版](../Page/渡邊美里.md "wikilink")**

<!-- end list -->

  -
    編曲為[金子隆博](../Page/金子隆博.md "wikilink")、演唱為[渡邊美里](../Page/渡邊美里.md "wikilink")。自2003年4月18日到2004年4月23日為短篇動畫片頭曲。

<!-- end list -->

  - **[AJI版](../Page/AJI.md "wikilink")**

<!-- end list -->

  -
    自2004年4月30日到2005年3月18日期間為主題曲。

<!-- end list -->

  - **[女子十二樂坊版](../Page/女子十二樂坊.md "wikilink")**

<!-- end list -->

  -
    由[女子十二樂坊演奏](../Page/女子十二樂坊.md "wikilink")，無人演唱。自2005年4月15日到10月21日止為水田版短篇動畫片頭曲。

<!-- end list -->

  - **[幸運☆星版](../Page/幸運☆星.md "wikilink")**

<!-- end list -->

  -
    2007年由[福原香織](../Page/福原香織.md "wikilink")（柊司）、[遠藤綾](../Page/遠藤綾.md "wikilink")（高良美幸），在第11話的[幸運☆星動畫片尾曲](../Page/幸運☆星.md "wikilink")。

<!-- end list -->

  - **[星野源版](../Page/星野源.md "wikilink")**

<!-- end list -->

  -
    2018年创作，不过并无实际使用。

<!-- end list -->

  - **40週年版**

<!-- end list -->

  -
    事隔14年後在2019年4月水田版再次插放，由水田版演員合唱的片頭曲。

## 改編版本

此曲曾被改編成香港國語與[粵語版](../Page/粵語.md "wikilink")《-{叮噹}-》（後改為《-{多啦A夢}-》）及台灣國語版《-{小叮噹}-》，香港版由[鄭國江填詞](../Page/鄭國江.md "wikilink")，有多位歌手曾經主唱，最近一次的版本由[陳慧琳主唱](../Page/陳慧琳.md "wikilink")；香港改编的国语版《-{多啦A夢}-》（陈慧琳演唱版）也在中国大陆电视动画的上海李晔配音版使用第二段。台灣國語版由[邱芷玲填詞](../Page/邱芷玲.md "wikilink")，由[范曉萱主唱](../Page/范曉萱.md "wikilink")。
[1](https://www.youtube.com/watch?v=4wyQarRZ_sE〡陳慧琳主唱粵語版)
[](../Page/.md "wikilink")

## 二次創作版本

由於此歌曲為人熟知，有人就填上[惡搞新詞](../Page/惡搞文化.md "wikilink")，2003年香港受到[SARS之擾](../Page/SARS.md "wikilink")，當時特首[董建華的太太](../Page/董建華.md "wikilink")[董趙洪娉就以全套防毒武裝落區巡視](../Page/董趙洪娉.md "wikilink")，並呼喻長者「洗手、洗手、洗手」，有網民便以哆啦A夢原曲再為此事譜上新詞創作出[董太認真怕你](http://evchk.wikia.com/wiki/%E6%87%B5%E5%A4%AA%E4%B8%BB%E9%A1%8C%E6%9B%B2)，並在互聯網廣泛流傳。

## 外部連結

[分類:歌曲小作品](../Page/分類:歌曲小作品.md "wikilink")

[Category:哆啦A夢歌曲](../Category/哆啦A夢歌曲.md "wikilink")
[Category:1979年單曲](../Category/1979年單曲.md "wikilink")
[Category:朝日電視台動畫主題曲](../Category/朝日電視台動畫主題曲.md "wikilink")