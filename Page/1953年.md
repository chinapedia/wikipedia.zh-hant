## 大事记

  - [1月20日](../Page/1月20日.md "wikilink")——[德怀特·D·艾森豪威尔就任](../Page/德怀特·D·艾森豪威尔.md "wikilink")[美国第](../Page/美国.md "wikilink")34任总统..
  - [1月26日](../Page/1月26日.md "wikilink")——美国[乔纳斯·索尔克博士制成](../Page/乔纳斯·索尔克.md "wikilink")[小儿麻痹症免疫新](../Page/小儿麻痹症.md "wikilink")[疫苗](../Page/疫苗.md "wikilink")。
  - [1月26日](../Page/1月26日.md "wikilink")——[中華民國總統](../Page/中華民國.md "wikilink")[蔣中正明令公布](../Page/蔣中正.md "wikilink")「[實施耕者有其田條例](../Page/耕者有其田.md "wikilink")」。
  - [2月](../Page/2月.md "wikilink")——美國[艾森豪總統咨詢美國國會](../Page/艾森豪.md "wikilink")，決定解除台灣海峽中立化，下令第七艦隊廢除台灣海峽從事「中立巡邏」。\[1\]蔣介石發表聲明，確認艾森豪此舉乃為合理而光明之舉措。\[2\]
  - [2月24日](../Page/2月24日.md "wikilink")——[中華民國](../Page/中華民國.md "wikilink")[立法院廢止](../Page/立法院.md "wikilink")《[中蘇友好同盟條約](../Page/中蘇友好同盟條約.md "wikilink")》並拒絕承認[蒙古國獨立](../Page/蒙古國.md "wikilink")。
  - [3月](../Page/3月.md "wikilink")——緬甸向聯合國控告台灣支持滇緬邊境反共游擊隊。\[3\]
  - [3月5日](../Page/3月5日.md "wikilink")——[蘇聯官方宣佈](../Page/蘇聯.md "wikilink")，[斯大林因](../Page/斯大林.md "wikilink")[心臟病突發死亡](../Page/心臟病.md "wikilink")，[馬林可夫繼任總理](../Page/馬林可夫.md "wikilink")。\[4\]
  - [3月28日](../Page/3月28日.md "wikilink")——苏联颁布[大赦令](../Page/大赦令.md "wikilink")。
  - [4月25日](../Page/4月25日.md "wikilink")——[科学家指出](../Page/科学家.md "wikilink")[脱氧核糖核酸是](../Page/脱氧核糖核酸.md "wikilink")[双螺旋结构](../Page/双螺旋结构.md "wikilink")。
  - [5月2日](../Page/5月2日.md "wikilink")——[约旦第三任国王](../Page/约旦.md "wikilink")[侯赛因登基](../Page/侯赛因.md "wikilink")。
  - [5月4日](../Page/5月4日.md "wikilink")——《[老人与海](../Page/老人与海.md "wikilink")》获[普利策奖](../Page/普利策.md "wikilink")。
  - [5月18日](../Page/5月18日.md "wikilink")——[杰奎琳·考克伦成为第一位驾驶飞机速度超过](../Page/杰奎琳·考克伦.md "wikilink")[音障的](../Page/音障.md "wikilink")[妇女](../Page/妇女.md "wikilink")。
  - [5月24日](../Page/5月24日.md "wikilink")——[立体电影首次在](../Page/立体电影.md "wikilink")[好莱坞出现](../Page/好莱坞.md "wikilink")。
  - [5月29日](../Page/5月29日.md "wikilink")——[英国人](../Page/英国.md "wikilink")[艾德蒙·希拉里爵士和](../Page/艾德蒙·希拉里.md "wikilink")[夏尔巴人向导](../Page/夏尔巴人.md "wikilink")[丹增·诺盖成为最早登上](../Page/丹增·诺盖.md "wikilink")[珠穆朗玛峰的人](../Page/珠穆朗玛峰.md "wikilink")。
  - [6月](../Page/6月.md "wikilink")——美援第一批噴射機抵達台灣。\[5\]
  - [6月2日](../Page/6月2日.md "wikilink")——[英国](../Page/英国.md "wikilink")[女王](../Page/英国国王.md "wikilink")[伊丽莎白二世加冕](../Page/伊丽莎白二世.md "wikilink")。
  - [6月3日](../Page/6月3日.md "wikilink")——[中国佛教协会成立](../Page/中国佛教协会.md "wikilink")。
  - [6月18日](../Page/6月18日.md "wikilink")——苏联出兵进犯[东德](../Page/东德.md "wikilink")，镇压东德的反共运动。
  - [6月18日](../Page/6月18日.md "wikilink")——[埃及放弃](../Page/埃及.md "wikilink")[君主制](../Page/君主制.md "wikilink")，成为[共和国](../Page/共和国.md "wikilink")。
  - [6月26日](../Page/6月26日.md "wikilink")——[苏联部长会议第一副主席](../Page/苏联.md "wikilink")、内务部长[贝利亚因](../Page/贝利亚.md "wikilink")“反党和反国家”罪行被[苏共中央清除出党并解除政府职务](../Page/苏共.md "wikilink")。
  - [7月](../Page/7月.md "wikilink")——習仲勳調到中共中央工作，任中央宣傳部長（至1954年10月）\[6\]。
  - [7月13日](../Page/7月13日.md "wikilink")——[朝鮮戰爭](../Page/朝鮮戰爭.md "wikilink")[金城戰役](../Page/金城戰役.md "wikilink")。
  - [7月23日](../Page/7月23日.md "wikilink")——[鄧鏡波學校創立](../Page/鄧鏡波學校.md "wikilink")。
  - [7月26日](../Page/7月26日.md "wikilink")——[菲德尔·卡斯特罗率领起义者武装袭击了](../Page/菲德尔·卡斯特罗.md "wikilink")[圣地亚哥东北](../Page/圣地亚哥.md "wikilink")[蒙卡达兵营](../Page/蒙卡达兵营.md "wikilink")，[古巴革命爆发](../Page/古巴革命.md "wikilink")。
  - [7月27日](../Page/7月27日.md "wikilink")——[朝鲜战争停战协定在](../Page/朝鲜战争.md "wikilink")[板门店正式签署](../Page/板门店.md "wikilink")。
  - [8月14日](../Page/8月14日.md "wikilink")——苏联宣布拥有[氢弹](../Page/氢弹.md "wikilink")，这是世界上第一颗武器化氢弹。
  - [9月](../Page/9月.md "wikilink")——習仲勳任「政務院秘書長」\[7\]。
  - [9月7日](../Page/9月7日.md "wikilink")——[赫鲁晓夫出任](../Page/赫鲁晓夫.md "wikilink")[苏联共产党中央委员会第一书记](../Page/苏联共产党中央委员会第一书记.md "wikilink")。
  - [11月9日](../Page/11月9日.md "wikilink")——[柬埔寨宣布独立](../Page/柬埔寨.md "wikilink")。
  - [11月14日](../Page/11月14日.md "wikilink")——[中國國民黨七屆三中全會](../Page/中國國民黨.md "wikilink")，總裁[蔣中正發表歷史重要文獻](../Page/蔣中正.md "wikilink")「民生主義育樂兩篇補述」，完成[三民主義體系](../Page/三民主義.md "wikilink")。民生主義中提到的人民的六大基本需求「[食](../Page/食.md "wikilink")、[衣](../Page/服裝.md "wikilink")、[住](../Page/住宅.md "wikilink")、[行](../Page/交通.md "wikilink")、[育](../Page/教育.md "wikilink")、[樂](../Page/娛樂.md "wikilink")」。
  - [12月](../Page/12月.md "wikilink")——[成人雜誌](../Page/成人雜誌.md "wikilink")《[花花公子](../Page/花花公子.md "wikilink")》[Playboy創刊號的封面及中間拉頁女郎為](../Page/Playboy.md "wikilink")[瑪麗蓮·夢露](../Page/瑪麗蓮·夢露.md "wikilink")。
  - [12月24日](../Page/12月24日.md "wikilink")——[石硤尾大火](../Page/石硤尾大火.md "wikilink")：[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[石硤尾木屋區發生大火](../Page/石硤尾.md "wikilink")，5萬人無家可歸。此事件促成[香港公共房屋政策的發展](../Page/香港公共房屋.md "wikilink")。

## 出生

  - [1月20日](../Page/1月20日.md "wikilink")——[程振隆](../Page/程振隆.md "wikilink")，[台灣教育人物](../Page/台灣.md "wikilink")，任職立法委員期間對教育問題頗多著墨。
  - [1月21日](../Page/1月21日.md "wikilink")——[保羅·艾倫](../Page/保羅·艾倫.md "wikilink")，[美國企業家](../Page/美國.md "wikilink")，[微軟公司創辦人之一](../Page/微軟公司.md "wikilink")。
  - [1月29日](../Page/1月29日.md "wikilink")——[鄧麗君](../Page/鄧麗君.md "wikilink")，[华语地区著名歌手](../Page/华语地区.md "wikilink")。（[1995年逝世](../Page/1995年.md "wikilink")）
  - [2月4日](../Page/2月4日.md "wikilink")——[喜多郎](../Page/喜多郎.md "wikilink")，日本著名[新世纪音乐作曲家](../Page/新世纪音乐.md "wikilink")。
  - [2月11日](../Page/2月11日.md "wikilink")——[傑布·布希](../Page/傑布·布希.md "wikilink")，[美國政治人物](../Page/美國.md "wikilink")，[小布希之弟](../Page/小布希.md "wikilink")。
  - [2月20日](../Page/2月20日.md "wikilink")——[甄妮](../Page/甄妮.md "wikilink")，[香港](../Page/香港.md "wikilink")、[台灣歌手](../Page/台灣.md "wikilink")。
  - [2月23日](../Page/2月23日.md "wikilink")——[鍾鎮濤](../Page/鍾鎮濤.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [2月28日](../Page/2月28日.md "wikilink")——[村下孝藏](../Page/村下孝藏.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")。（[1999年逝世](../Page/1999年.md "wikilink")）
  - [3月16日](../Page/3月16日.md "wikilink")——[李應元](../Page/李應元.md "wikilink")，[台灣政治人物](../Page/台灣.md "wikilink")。
  - [3月26日](../Page/3月26日.md "wikilink")——[趙小蘭](../Page/趙小蘭.md "wikilink")，[美國政治人物](../Page/美國.md "wikilink")，美國史上首位華裔內閣成員。
  - [4月14日](../Page/4月14日.md "wikilink")——[曾志偉](../Page/曾志偉.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [5月5日](../Page/5月5日.md "wikilink")——[王俠軍](../Page/王俠軍.md "wikilink")，[台灣藝術家](../Page/台灣.md "wikilink")。
  - [5月6日](../Page/5月6日.md "wikilink")——[托尼·布萊爾](../Page/托尼·布萊爾.md "wikilink")，[英國](../Page/英國.md "wikilink")[首相](../Page/英國首相.md "wikilink")。
  - [5月16日](../Page/5月16日.md "wikilink")——[皮爾斯·布魯斯南](../Page/皮爾斯·布魯斯南.md "wikilink")，[英國演員](../Page/英國.md "wikilink")。
  - [6月7日](../Page/6月7日.md "wikilink")——[马可·穆勒](../Page/马可·穆勒.md "wikilink")，[意大利电影制片人](../Page/意大利.md "wikilink")、电影史家、影评人。
  - [6月15日](../Page/6月15日.md "wikilink")——[习近平](../Page/习近平.md "wikilink")，[中国国家领导人](../Page/中国国家领导人.md "wikilink")，现任中共中央总书记、国家主席、中央军委主席。
  - [6月30日](../Page/6月30日.md "wikilink")——[林鳳嬌](../Page/林鳳嬌.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")。
  - [7月21日](../Page/7月21日.md "wikilink")——[張艾嘉](../Page/張艾嘉.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")、歌手、導演。
  - [7月31日](../Page/7月31日.md "wikilink")——[古谷徹](../Page/古谷徹.md "wikilink")，[日本](../Page/日本.md "wikilink")[男性](../Page/男性.md "wikilink")[配音員](../Page/配音員.md "wikilink")。
  - [8月4日](../Page/8月4日.md "wikilink")——[魏翰楷](../Page/魏翰楷.md "wikilink")，德國外交官、前駐華[公使](../Page/公使.md "wikilink")。
  - [8月20日](../Page/8月20日.md "wikilink")——[鳳飛飛](../Page/鳳飛飛.md "wikilink")，帽子歌后、演員、主持人。（[2012年逝世](../Page/2012年.md "wikilink")）
  - [8月31日](../Page/8月31日.md "wikilink")——[小林善紀](../Page/小林善紀.md "wikilink")，[日本漫畫家](../Page/日本.md "wikilink")。
  - [10月8日一一](../Page/10月8日.md "wikilink")[麥潤壽](../Page/麥潤壽.md "wikilink")，香港電台第二台DJ
  - [11月1日](../Page/11月1日.md "wikilink")——[謝雪心](../Page/謝雪心.md "wikilink")，[孔子第](../Page/孔子.md "wikilink")76代後人（原名孔令馥），橫跨[香港泳界](../Page/香港.md "wikilink")、[粵劇界](../Page/粵劇.md "wikilink")、影視界、話劇界名人。
  - [11月14日](../Page/11月14日.md "wikilink")——[多米尼克·德维尔潘](../Page/多米尼克·德维尔潘.md "wikilink")，[法國](../Page/法國.md "wikilink")[總理](../Page/法國總理.md "wikilink")。
  - [11月30日](../Page/11月30日.md "wikilink")——[程小東](../Page/程小東.md "wikilink")，[香港著名](../Page/香港.md "wikilink")
    導演 及 武術指導 。
  - [12月18日](../Page/12月18日.md "wikilink")——[尤雅](../Page/尤雅.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")。

## 逝世

  - [1月11日](../Page/1月11日.md "wikilink")——[璧約翰](../Page/璧約翰.md "wikilink")，英國外交官（生於1883年）
  - [3月5日](../Page/3月5日.md "wikilink")——[约瑟夫·斯大林](../Page/约瑟夫·斯大林.md "wikilink")，[蘇共中央總書記](../Page/蘇共中央總書記.md "wikilink")，[苏联最高领导人](../Page/苏联.md "wikilink")（生於1878年）
  - [5月10日](../Page/5月10日.md "wikilink")——[黄金荣](../Page/黄金荣.md "wikilink")，[上海](../Page/上海.md "wikilink")[大亨](../Page/大亨.md "wikilink")，病死（生於1868年）
  - [9月26日](../Page/9月26日.md "wikilink")——[徐悲鸿](../Page/徐悲鸿.md "wikilink")，[中國画家](../Page/中國.md "wikilink")、美术教育家（生于1895年）
  - [9月28日](../Page/9月28日.md "wikilink")——[愛德文·哈勃](../Page/愛德文·哈勃.md "wikilink")，美國天文學家（生於1889年）
  - [10月30日](../Page/10月30日.md "wikilink")——[吳敬恆](../Page/吳敬恆.md "wikilink")
    (稚暉)，([中央研究院院士](../Page/中央研究院院士.md "wikilink"))。（生於1865年）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[弗里茨·塞爾尼克](../Page/弗里茨·塞爾尼克.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[赫爾曼·施陶丁格](../Page/赫爾曼·施陶丁格.md "wikilink")
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[漢斯·阿道夫·克雷布斯](../Page/漢斯·阿道夫·克雷布斯.md "wikilink")、[弗里茨·阿爾貝特·李普曼](../Page/弗里茨·阿爾貝特·李普曼.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[溫斯頓·邱吉爾](../Page/溫斯頓·邱吉爾.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[乔治·卡特莱特·马歇尔](../Page/乔治·卡特莱特·马歇尔.md "wikilink")（George
    Catlett Marshall）

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第26届，[1954年颁发](../Page/1954年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[亂世忠魂](../Page/亂世忠魂.md "wikilink")》（From
    Here to Eternity）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[弗雷德·齐纳曼](../Page/弗雷德·齐纳曼.md "wikilink")（Fred
    Zinnemann）《亂世忠魂》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[威廉·霍尔登](../Page/威廉·霍尔登.md "wikilink")（William
    Holden）《[十七号囚房](../Page/十七号囚房.md "wikilink")》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[奥黛丽·赫本](../Page/奥黛丽·赫本.md "wikilink")（Audrey
    Hepburn）《[罗马假日](../Page/罗马假日.md "wikilink")》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[弗兰克·西纳特拉](../Page/弗兰克·西纳特拉.md "wikilink")（Frank
    Sinatra）《亂世忠魂》
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[唐娜·里德](../Page/唐娜·里德.md "wikilink")（Donna
    Reed）《亂世忠魂》

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖获奖名单.md "wikilink")）

## 參考文獻

[\*](../Category/1953年.md "wikilink")
[3年](../Category/1950年代.md "wikilink")
[5](../Category/20世纪各年.md "wikilink")

1.  [陳-{布}-雷等編著](../Page/陳布雷.md "wikilink")：《蔣介石先生年表》，台北：[傳記文學出版社](../Page/傳記文學.md "wikilink")，1978年6月1日

2.
3.
4.
5.
6.

7.