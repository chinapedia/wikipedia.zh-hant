**楊登魁**（），生于[臺灣屏東](../Page/臺灣.md "wikilink")[新園烏龍村](../Page/新園鄉_\(台灣\).md "wikilink")，是台灣[演藝圈稱作](../Page/演藝圈.md "wikilink")「楊老闆」的[演藝事業](../Page/演藝事業.md "wikilink")[企業家](../Page/企業家.md "wikilink")。

他是[第一媒體國際有限公司的](../Page/第一媒體國際有限公司.md "wikilink")[創辦人](../Page/創辦人.md "wikilink")，以及[八大電視股份有限公司的創辦人](../Page/八大電視股份有限公司.md "wikilink")、[董事長及](../Page/董事長.md "wikilink")[榮譽主席](../Page/榮譽主席.md "wikilink")，並曾擔任勢統合國際股份有限公司的[監察人](../Page/監察人.md "wikilink")、中華民國衛星廣播電視事業商業同業公會（簡稱「STBA」）的[榮譽顧問](../Page/榮譽顧問.md "wikilink")。此外，他也是[天道盟的最初](../Page/天道盟.md "wikilink")[創始人之一](../Page/創始人.md "wikilink")。

## 生平概略

### 早年生活

[1939年](../Page/台灣日治末期.md "wikilink")，楊登魁出生於[高雄州](../Page/高雄州.md "wikilink")[新園](../Page/新園鄉_\(台灣\).md "wikilink")（今屬[屏東縣](../Page/屏東縣.md "wikilink")）；該地是一個位在[臺灣西南](../Page/南臺灣.md "wikilink")[海岸的](../Page/海岸.md "wikilink")[農村](../Page/農村.md "wikilink")。由於家中[經濟清苦](../Page/经济.md "wikilink")，他自小就前往[高雄市謀生](../Page/高雄市.md "wikilink")；此後，他曾[擦過皮鞋](../Page/擦鞋工.md "wikilink")、拉[人力車](../Page/人力車.md "wikilink")、在[舞廳當](../Page/舞廳.md "wikilink")[服務生在新興區南台路七賢二路一帶過吃苦耐勞的生活](../Page/服務生.md "wikilink")，南台路有家[喜水仙大酒家老板娘](../Page/喜水仙大酒家.md "wikilink")[陳鶯見他老實](../Page/陳鶯.md "wikilink")，重義氣，從小時候就很疼他這小弟，時時有難就資助他，鼓勵他。

年轻时候的他投身[黑道](../Page/黑道.md "wikilink")，並因[殺害地方](../Page/殺害.md "wikilink")[角頭而被關入](../Page/角頭.md "wikilink")[監獄](../Page/監獄.md "wikilink")。

### 秀場事業

[假釋出獄後](../Page/假釋.md "wikilink")，他成為了[西北幫老大](../Page/西北幫.md "wikilink")，並在高雄市後火車站（八德路，七賢二路，南台路，自立二路）一帶混，開職業賭場，三教九流聚賭，內還包含聞名的大人物，如市議員[陳進財因涉嫌在楊登魁賭場聚賭](../Page/陳進財.md "wikilink")，被法院判一年徒刑，失去議員資格，還有政商名流參賭，[藍寶石大歌廳董事長](../Page/藍寶石大歌廳.md "wikilink")[蔡有望亦輸不少錢](../Page/蔡有望.md "wikilink")，更有大牌演員及名歌星參與聚賭，麻將賭資以四千底起跳。

裝潢大師[李再富](../Page/李再富.md "wikilink")（人稱[台北仔](../Page/台北仔.md "wikilink")）也輸了好幾百萬，隨即跑路，後來由藍寶石大歌廳董娘[陳鶯出面](../Page/陳鶯.md "wikilink")，幫李再富解圍，向楊登魁商量，能讓李再富回高雄把歌廳重新整修，便可把歌廳重新開幕的第一檔秀讓楊承包，並讓李再富以裝潢費扺賭債，楊登魁欣然同意照辦，[藍寶石經台北仔精心改造](../Page/藍寶石.md "wikilink")，煥然一新，果然開幕後的第一檔由[許不了當主秀](../Page/許不了.md "wikilink")、[廖峻](../Page/廖峻.md "wikilink")、[澎澎](../Page/澎澎_\(藝人\).md "wikilink")、[高凌風與多位大牌藝人演出](../Page/高凌風.md "wikilink")，卻大轟動，幾乎場場客滿，扣除歌星、演員價碼、稅金淨賺數百萬，這是楊登魁進軍娛樂圈賺到第一桶金的處女作。

### 移送管訓

「[一清專案](../Page/一清專案.md "wikilink")」期間，楊登魁在1985年1月9日被[警備總司令部以](../Page/警備總司令部.md "wikilink")[涉嫌違反](../Page/涉嫌.md "wikilink")《[懲治叛亂條例](../Page/懲治叛亂條例.md "wikilink")》的[罪名](../Page/罪名.md "wikilink")[逮捕並](../Page/逮捕.md "wikilink")[羈押](../Page/羈押.md "wikilink")；1985年4月27日，警備總部[軍事檢察官作出](../Page/軍事檢察官.md "wikilink")[不起訴處分](../Page/不起訴處分.md "wikilink")，並在1985年5月16日確定不起訴。

1985年5月8日，楊登魁因另案被提報[流氓而被移送](../Page/流氓.md "wikilink")[綠島](../Page/綠島.md "wikilink")[岩灣職業訓導第二總隊管訓](../Page/法務部矯正署岩灣技能訓練所.md "wikilink")；他在綠島一共被管訓3年3個月\[1\]，並在管訓期間與[羅福助](../Page/羅福助.md "wikilink")、顏清源等人組織[天道盟](../Page/天道盟.md "wikilink")。

1987年12月1日，因警備總部人員管理失當，包括岩灣在內的三個監獄同時發生[暴動](../Page/暴動.md "wikilink")；因為當時處於[戒嚴時期](../Page/戒嚴.md "wikilink")，[岩灣事件並沒有傳到臺北](../Page/岩灣事件.md "wikilink")。為爭取[輿論支持](../Page/輿論.md "wikilink")，楊登魁自願被打斷手骨，爭取[保外就醫的機會](../Page/保外就醫.md "wikilink")，並趁機在手部打[石膏處夾帶](../Page/石膏.md "wikilink")[受刑人的自白](../Page/受刑人.md "wikilink")，把有關消息傳回臺北，讓事件登上[新聞媒體](../Page/新聞媒體.md "wikilink")。在此事件結束後，楊登魁成為備受臺灣黑道各界敬重的[大哥](../Page/大哥.md "wikilink")\[2\]。

### 復出經營

1988年，楊登魁在管訓結束後隨即返回臺灣本島，開始投入[餐廳](../Page/餐廳.md "wikilink")、[證券](../Page/證券.md "wikilink")、[錄影帶](../Page/錄影帶.md "wikilink")、[電影院等事業經營](../Page/電影院.md "wikilink")。

1989年，楊登魁與[邱復生共同](../Page/邱復生.md "wikilink")[投資電影](../Page/投資報酬率.md "wikilink")《[悲情城市](../Page/悲情城市.md "wikilink")》；該片獲得[威尼斯影展金獅獎](../Page/威尼斯影展.md "wikilink")，並得到[行政院新聞局表揚](../Page/行政院新聞局.md "wikilink")。

1990年，楊登魁在「二清專案」期間（又稱「迅雷專案掃黑行動」）涉嫌開設經營職業[賭場而再度被提報為流氓](../Page/賭場.md "wikilink")，並因此被關進監獄管訓2個月。

二清專案出獄後，楊登魁因[有線電視開放而決定與](../Page/有線電視.md "wikilink")[辜啟允合作開辦](../Page/辜啟允.md "wikilink")「巨登育樂股份有限公司」（Golden
Entertainment，[八大電視股份有限公司的前身](../Page/八大電視股份有限公司.md "wikilink")），並在1988年8月22日接任[總經理](../Page/總經理.md "wikilink")；當時，該公司成為臺灣最主要的[秀場錄影帶](../Page/秀場.md "wikilink")[供應商之一](../Page/供應商.md "wikilink")。此外，楊登魁也同時成立「飛梭傳播股份有限公司」經營「飛梭衛星電視台」（FSTV），經營「飛梭國片台」、「飛梭洋片台」、「飛梭綜藝台」、「拉斯維加頻道」等頻道，成為有線[電視頻道經營商](../Page/電視頻道.md "wikilink")。

同時，他也開始投資拍攝《紅粉兵團》、《紅粉遊俠》、《掃蕩大賭場》、《情報販子》、《[功夫皇帝方世玉](../Page/方世玉_\(電影\).md "wikilink")》系列、《[洪熙官之少林五祖](../Page/洪熙官之少林五祖.md "wikilink")》、《[鼠膽龍威](../Page/鼠膽龍威.md "wikilink")》、《[人肉叉燒包](../Page/八仙飯店之人肉叉燒飽.md "wikilink")》等系列共數十部[電影](../Page/電影.md "wikilink")，並捧紅許多[藝人](../Page/藝人.md "wikilink")，如[林青霞](../Page/林青霞.md "wikilink")、[王冠雄](../Page/王冠雄.md "wikilink")、[葉倩文](../Page/葉倩文.md "wikilink")、[楊惠姍](../Page/楊惠姍.md "wikilink")、[李連杰](../Page/李連杰.md "wikilink")
、 [黃秋生](../Page/黃秋生.md "wikilink") 、
[李修賢](../Page/李修賢.md "wikilink")、[彭雪芬](../Page/彭雪芬.md "wikilink")。

1995年1月28日，《戒嚴時期人民受損權利回復條例》公布施行；楊登魁以一清專案及二清專案未經合法審判就將他監禁為由申請[冤獄賠償](../Page/冤獄.md "wikilink")。2003年6月，[臺灣臺北地方法院判決政府須賠償楊登魁](../Page/臺灣臺北地方法院.md "wikilink")[新臺幣四十八萬元整](../Page/新臺幣.md "wikilink")。\[3\]

1996年，楊登魁當選[金馬獎執行委員會主席](../Page/金馬獎.md "wikilink")。同年，拉斯維加頻道[現場直播節目](../Page/現場直播.md "wikilink")《龍虎爭霸戰》涉及公開地下賭博案遭[臺灣臺北地方法院檢察署偵辦](../Page/臺灣臺北地方法院檢察署.md "wikilink")；楊登魁聲稱不信任臺灣[司法並離開臺灣](../Page/司法機構.md "wikilink")，各[銀行也隨即對其實行緊縮](../Page/銀行.md "wikilink")[貸款政策](../Page/貸款.md "wikilink")，因而引發其[財務危機](../Page/財務.md "wikilink")。此時，[王令麟提供他](../Page/王令麟.md "wikilink")[資金協助](../Page/資金.md "wikilink")，並隨後獲得「陽明山有線電視」、「新台北有線電視」、「金頻道有線電視」、「聯群有線電視」等四家有線電視公司經營權。

1997年，楊登魁成立[八大國際傳播](../Page/八大國際傳播.md "wikilink")，下設八大家族頻道（[GTV27](../Page/GTV27.md "wikilink")、[GTV28](../Page/GTV28.md "wikilink")）。

1999年，楊登魁開始投資拍攝[電視劇](../Page/電視劇.md "wikilink")；他的第一部作品為《[小寶與康熙](../Page/小寶與康熙.md "wikilink")》，並特别邀請[張衞健飾演該片男主角](../Page/張衞健.md "wikilink")。

2000年，楊登魁投資拍攝電視劇《新楚留香傳奇》；該片男主角由[任賢齊飾演](../Page/任賢齊.md "wikilink")。

2001年，楊登魁投資拍攝電視劇《[齊天大聖孫悟空](../Page/齊天大聖孫悟空.md "wikilink")》；該片男主角由張衛健飾演。

2002年，楊登魁在[香港成立](../Page/香港.md "wikilink")[第一媒體國際有限公司](../Page/第一媒體國際有限公司.md "wikilink")，以藉之在[大陸及香港地區永續經營](../Page/大陸地區.md "wikilink")[電視產業](../Page/電視.md "wikilink")。

2004年，楊登魁投資開拍古裝連續劇《[天下第一](../Page/天下第一.md "wikilink")》。

2005年，楊登魁代理發行《[風塵三俠之紅拂女](../Page/風塵三俠之紅拂女.md "wikilink")》海外版權。

2011年，楊登魁斥資新臺幣15億元成立柏合麗娛樂傳媒，並將其總部設於臺北市信義區[統一國際大樓](../Page/統一國際大樓.md "wikilink")29樓。\[4\]\[5\]\[6\]

### 離世

2012年12月17日，楊登魁在與[友人](../Page/友人.md "wikilink")[聚餐時因](../Page/聚餐.md "wikilink")[身體不適被緊急送往](../Page/身體.md "wikilink")[臺北市立聯合醫院仁愛院區並被確診為](../Page/臺北市立聯合醫院.md "wikilink")[腦中風](../Page/腦中風.md "wikilink")，再於同月19日對外證實相關[消息](../Page/消息.md "wikilink")。\[7\]同月21日，楊登魁因病情危急被轉送[臺北榮民總醫院搶救並轉入腦中風](../Page/臺北榮民總醫院.md "wikilink")[加護病房觀察治療](../Page/加護病房.md "wikilink")，並由該醫院神經血管科主任陳昌明擔任[主治醫師](../Page/主治醫師.md "wikilink")；其間，他的[親人與影壇朋友陸續前往](../Page/親人.md "wikilink")[探視](../Page/探視.md "wikilink")。同月23日，《[中國時報](../Page/中國時報.md "wikilink")》刊出「截稿後消息，傳楊登魁過世」假消息引起其家屬不滿；[翌日](../Page/翌日.md "wikilink")，中國時報報社以「病故係臆傳」為由在報上刊出道歉[啟事](../Page/啟事.md "wikilink")。\[8\]\[9\]

同月31日[清晨](../Page/清晨.md "wikilink")5點12分，楊登魁[病逝](../Page/病逝.md "wikilink")，[享壽](../Page/壽命.md "wikilink")74歲。

### 葬禮

2013年1月5日，楊登魁的[喪禮在](../Page/喪禮.md "wikilink")[臺北市盛大舉行](../Page/臺北市.md "wikilink")；[豬哥亮](../Page/豬哥亮.md "wikilink")\[10\]、[江蕙](../Page/江蕙.md "wikilink")、[江淑娜](../Page/江淑娜.md "wikilink")、[劉家昌](../Page/劉家昌.md "wikilink")、[劉子千](../Page/劉子千.md "wikilink")、[張菲](../Page/張菲.md "wikilink")、[任賢齊](../Page/任賢齊.md "wikilink")、[洪榮宏](../Page/洪榮宏.md "wikilink")、[黃鴻升](../Page/黃鴻升.md "wikilink")、[林心如](../Page/林心如.md "wikilink")、[大S](../Page/大S.md "wikilink")、[小S](../Page/小S.md "wikilink")、[張小燕](../Page/張小燕.md "wikilink")、[陳美鳳](../Page/陳美鳳.md "wikilink")、[康康](../Page/康康.md "wikilink")、[余天](../Page/余天.md "wikilink")、[陳松勇](../Page/陳松勇.md "wikilink")、[羅時豐](../Page/羅時豐.md "wikilink")、[胡志強](../Page/胡志強.md "wikilink")、[郝龍斌](../Page/郝龍斌.md "wikilink")、[曾志偉等人均有出席](../Page/曾志偉.md "wikilink")。\[11\]此外，[張學友](../Page/張學友.md "wikilink")、[張衛健](../Page/張衛健.md "wikilink")、[王晶和](../Page/王晶.md "wikilink")[劉偉強也專程結伴自](../Page/劉偉強.md "wikilink")[香港前往臺北參加其追思告別儀式](../Page/香港.md "wikilink")\[12\]。喪禮結束後，楊登魁的骨灰入厝[新北市](../Page/新北市.md "wikilink")[萬里區的天祥寶塔](../Page/萬里區.md "wikilink")。

### 身後

2013年2月28日，柏合麗娛樂傳媒宣告解散，其旗下[映畫傳播](../Page/映畫傳播.md "wikilink")、威俠文創、爾傑國際、小山堂餐飲、想亮製作、寶麗來經紀等公司分拆各自獨立營運。\[13\]\[14\]

## 許不了事件

[網路傳聞知名喜劇演員](../Page/網路.md "wikilink")[許不了之死與楊登魁有關](../Page/許不了.md "wikilink")，許多藝人為此發表評論。

### 眾人評論

  - 2012年12月31日，[廖峻表示楊登魁曾與他在電影](../Page/廖峻.md "wikilink")[配音時閒聊](../Page/配音.md "wikilink")，並談及關於[許不了的事情](../Page/許不了.md "wikilink")；楊登魁當時向他表示許不了因[毒癮日漸深陷而被他](../Page/毒癮.md "wikilink")（楊登魁）拖去戒毒，卻因此被外界傳為押著許不了去拍片；對此，廖峻表示：「許不了是可以替他賺錢的人，他怎麼可能把[搖錢樹砍掉](../Page/搖錢樹.md "wikilink")？當然希望他身體好！」。

<!-- end list -->

  - [邢峰表示](../Page/邢峰.md "wikilink")，楊登魁曾在某次得知許不了自[新竹秀場騎乘](../Page/新竹.md "wikilink")[重型機車](../Page/重型機器腳踏車.md "wikilink")，只花費半小時便迅速返回[臺北後](../Page/臺北.md "wikilink")，深感[不滿及](../Page/憤怒.md "wikilink")[擔心](../Page/擔心.md "wikilink")；他表示：「他（楊登魁）很有[義氣](../Page/義氣.md "wikilink")，不然憑什麼讓一堆人對他這麼死忠？」
  - [蔡振南曾表示楊登魁根本不屑做](../Page/蔡振南.md "wikilink")[壓榨人的事](../Page/剝削.md "wikilink")，「因為他的格局太大了！」
  - [康弘表示楊登魁曾指派兩個助理幫許不了戒](../Page/康弘.md "wikilink")[酒](../Page/酒癮.md "wikilink")，許不了卻受到黑道小弟[誘惑而染上](../Page/誘惑.md "wikilink")[毒癮](../Page/毒癮.md "wikilink")。
  - [黃西田曾表示](../Page/黃西田.md "wikilink")：「許不了愛喝酒，這種人誰要幫？只有楊登魁願意釋出善意。」
  - [賀一航曾表示許不了的死絕對不該歸咎在楊登魁身上](../Page/賀一航.md "wikilink")，「楊董（楊登魁）以前是[兄弟](../Page/兄弟.md "wikilink")（黑道），可是他早就改過自新了。」

<!-- end list -->

  - [李再富](../Page/李再富.md "wikilink")(台北仔)楊登魁的一生重情義，黑道日子過得講道理，黑白分明，心胸開闊，對於當時所謂黑道大哥也是無派頭，彬彬有禮的對待跟隨的小弟以禮相待，如今其實痛失一位娛樂界奇才。
    \[15\]

## 軼事

  - 2005年3月22日，[姚采穎](../Page/姚采穎.md "wikilink")、[胡宇威與第一媒體國際有限公司舉行簽約](../Page/胡宇威.md "wikilink")[記者會](../Page/記者會.md "wikilink")；楊登魁在會上建議姚采穎趕快[戒菸](../Page/戒菸.md "wikilink")：“妳只要把假的一面表現給媒體看，真面目擺家裡就好了。”\[16\]

## 註釋

## 外部連結

  - [中文電影資料庫──楊登魁](http://www.dianying.com/ft/person/YangDengkui)

  -
[Category:台灣綜藝界人物](../Category/台灣綜藝界人物.md "wikilink")
[Category:台灣企業家](../Category/台灣企業家.md "wikilink")
[Category:天道盟人物](../Category/天道盟人物.md "wikilink")
[Category:八大電視](../Category/八大電視.md "wikilink")
[Category:台灣罪犯](../Category/台灣罪犯.md "wikilink")
[Category:新園人](../Category/新園人.md "wikilink")
[Deng登魁](../Category/楊姓.md "wikilink")
[Category:死於心血管疾病的人](../Category/死於心血管疾病的人.md "wikilink")

1.
2.  [【人物】楊登魁從黑白到彩色的奇幻人生](http://www.new7.com.tw/NewsView.aspx?i=TXT201212261609072OZ)
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.