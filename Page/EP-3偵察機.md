[EP-3A_EMPASS_VX-1_NAN5-6-83.jpg](https://zh.wikipedia.org/wiki/File:EP-3A_EMPASS_VX-1_NAN5-6-83.jpg "fig:EP-3A_EMPASS_VX-1_NAN5-6-83.jpg")
**EP-3A/B獵戶式**（EP-3A/B Orion）與**EP-3E白羊I/II型**（EP-3E Aries
I/II）是一系列主要由[美國海軍所操作](../Page/美國海軍.md "wikilink")，配備有[渦輪推進引擎的信號偵察機](../Page/渦輪發動機.md "wikilink")。它的機身設計取自同廠的[P-3](../Page/P-3.md "wikilink")「獵戶」海上巡邏機，而P-3的設計則取自民航客機。目前美軍共擁有11架EP-3E型偵察機，最後一架於1997年交付。

## 型號

  - EP-3A
  - EP-3B
  - EP-3
  - EP-3E ARIES
  - EP-3E ARIES II
  - EP-3J

## 使用國

  -

<!-- end list -->

  - [日本航空自衛隊](../Page/日本航空自衛隊.md "wikilink") - 5架EP-3

<!-- end list -->

  -

<!-- end list -->

  - [美國海軍](../Page/美國海軍.md "wikilink")
      - VQ-1 中隊 美國[橡港市 (華盛頓州)基地](../Page/橡港市_\(華盛頓州\).md "wikilink")
      - VQ-2 中隊 西班牙[加的斯省基地](../Page/加的斯省.md "wikilink")

## 規格

## 區域衝突

2001年4月1日，一架從[沖繩基地出發的EP](../Page/沖繩.md "wikilink")-3型偵察機，在距[中國](../Page/中國.md "wikilink")[海南島](../Page/海南島.md "wikilink")110公里對開海面，與[中國人民解放軍一架](../Page/中國人民解放軍.md "wikilink")[殲八戰機相撞](../Page/殲八.md "wikilink")，中方機師[王偉失蹤](../Page/王偉_\(中國軍人\).md "wikilink")，而美軍偵察機則急降海南島陵水機場。

美軍軍機的11名機員隨即被中方扣留，並於同月11日獲得釋放，而該軍機也於同年7月3日，以一架[安-124型運輸機拆件運走](../Page/安-124.md "wikilink")。

## 參閱

  - [中美撞機事件](../Page/中美撞機事件.md "wikilink")

## 參考

  - [P-3C fact
    file](http://www.navy.mil/navydata/fact_display.asp?cid=1100&tid=1400&ct=1)
    and [P-3 history page on U.S. Navy
    site](https://web.archive.org/web/20080410114009/http://www.history.navy.mil/planes/p3.htm)

  -
  - [NASA Wallops Flight Facility Aircraft Office operators of the NASA
    P-3](https://web.archive.org/web/20060103044142/http://wacop.wff.nasa.gov/)

  - [EP-3E on
    CombatAircraft.com](http://combataircraft.com/aircraft/sep3e.aspx)

[Category:美國偵察機](../Category/美國偵察機.md "wikilink")