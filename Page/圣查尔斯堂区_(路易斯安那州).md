**聖查爾斯縣**（**St. Charles Parish,
Louisiana**）是位於[美國](../Page/美國.md "wikilink")[路易斯安那州東南部的一個縣](../Page/路易斯安那州.md "wikilink")。面積1,062平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口48,072人。縣治[哈恩維爾](../Page/哈恩維爾.md "wikilink")
（Hahnville）。

成立於1807年3月31日。縣名來自聖查爾斯[堂區](../Page/堂區.md "wikilink")
（以[米蘭總主教](../Page/米蘭.md "wikilink")[聖嘉祿·伯羅敏](../Page/聖嘉祿·伯羅敏.md "wikilink")*St.
Carlo Borromeo*命名）\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[S](../Category/路易斯安那州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.