**奧地利國旗**是一面由三條橫間所組成的國旗，最上處和最底處都是[紅色](../Page/紅色.md "wikilink")，中間是[白色](../Page/白色.md "wikilink")。現時奧地利的國旗是世界上最古老的國旗之一。

[奧地利國旗的來源有不少](../Page/奧地利.md "wikilink")，而最多的說法是於1192年，[公爵](../Page/公爵.md "wikilink")[雷歐伯德五世帶領](../Page/利奥波德五世_\(奥地利\).md "wikilink")[十字軍東征時](../Page/十字軍.md "wikilink")，雷歐伯德五世在戰爭之中身穿的[白色戰袍被](../Page/白色.md "wikilink")[血染成紅色](../Page/血.md "wikilink")，而中間腰帶的一部份依然是白色。東征返國後，雷歐伯德五世得到[神聖羅馬帝國皇帝的讚頌](../Page/神聖羅馬帝國.md "wikilink")，並頒贈紅、白、紅的勳章，表揚他的功勞。而這赤地中白旗也於這時開始使用，在1804年因成立[奧地利帝國而停用](../Page/奧地利帝國.md "wikilink")。

[一次大戰後](../Page/第一次世界大戰.md "wikilink")，奧匈帝國解體，[奧地利第一共和國採用原來的](../Page/奧地利第一共和國.md "wikilink")[三色旗](../Page/三色旗.md "wikilink")，政府旗幟後來加上了[雄鷹](../Page/奧地利國徽.md "wikilink")。
[Austrian_Flag.jpg](https://zh.wikipedia.org/wiki/File:Austrian_Flag.jpg "fig:Austrian_Flag.jpg")

[Flag-of-Austria-320x240.ogg](https://zh.wikipedia.org/wiki/File:Flag-of-Austria-320x240.ogg "fig:Flag-of-Austria-320x240.ogg")

## 歷代國旗

[A](../Category/国旗.md "wikilink")
[Category:奥地利国家象征](../Category/奥地利国家象征.md "wikilink")
[Category:奥地利旗帜](../Category/奥地利旗帜.md "wikilink")