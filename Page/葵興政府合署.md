[Kwai_Hing_Government_Offices.jpg](https://zh.wikipedia.org/wiki/File:Kwai_Hing_Government_Offices.jpg "fig:Kwai_Hing_Government_Offices.jpg")
[Kwai_Hing_Government_Offices_Lobby.jpg](https://zh.wikipedia.org/wiki/File:Kwai_Hing_Government_Offices_Lobby.jpg "fig:Kwai_Hing_Government_Offices_Lobby.jpg")
**葵興政府合署**（）是[香港其中一座](../Page/香港.md "wikilink")[政府合署](../Page/香港政府合署.md "wikilink")，位處於[葵興興芳路](../Page/葵興.md "wikilink")166-174號，鄰近[葵興港鐵站](../Page/葵興站.md "wikilink")，是[葵青區的政府大樓](../Page/葵青區.md "wikilink")，樓高10層。合署下為購物商場[新葵興廣場及](../Page/新葵興廣場.md "wikilink")[葵興站公共運輸交匯處](../Page/葵興站公共運輸交匯處.md "wikilink")。

## 部門及機構

大樓內設有以下政府部門的辦事處：

  - [民政事務總署](../Page/民政事務總署.md "wikilink")－葵青民政諮詢中心（2樓）、葵青[民政事務處聯絡常務組](../Page/民政事務處.md "wikilink")（5樓、10樓）、葵青[民政事務處](../Page/民政事務處.md "wikilink")、[葵青區議會秘書處](../Page/葵青區議會.md "wikilink")（10樓）
  - [康樂及文化事務署](../Page/康樂及文化事務署.md "wikilink")－[南葵涌公共圖書館](../Page/南葵涌公共圖書館.md "wikilink")（4樓）
  - [社會福利署](../Page/社會福利署.md "wikilink")－東葵涌社會保障辦事處（5樓）、西葵涌綜合家庭服務中心（7樓）、西葵涌/中葵涌社會保障辦事處（8樓）
  - [勞工處](../Page/勞工處.md "wikilink")－勞資關係科（葵涌）、勞工視察科（葵涌/荃灣分區辦事處、葵涌/荃灣特別視察組、屯門特別視察組）、行動科（新界西第3/4分區辦事處、新界西建築地盤第5分區辦事處）（6樓）
  - [在職家庭及學生資助事務處](../Page/在職家庭及學生資助事務處.md "wikilink") -
    [學生資助處](../Page/學生資助處.md "wikilink")
    －幼稚園及幼兒中心學費減免組（7樓713-714室）、核證申請組（7樓704室）、持續進修基金辦事處（9樓916室）
  - [康樂及文化事務署](../Page/康樂及文化事務署.md "wikilink")－葵青區康樂事務辦事處（8樓805室）
  - [食物環境衞生署](../Page/食物環境衞生署.md "wikilink")－葵青區環境衞生辦事處（9樓）
  - [教育局](../Page/教育局.md "wikilink")－特殊學校普通話科目政策組（8-9樓）

[基督教香港信義會的](../Page/基督教.md "wikilink")[新來港人士樂聚軒也在此大樓](../Page/新來港人士.md "wikilink")5樓。大樓跟新葵興花園皆屬於葵興港鐵站上蓋建築群之一，基座地下是葵興港鐵站巴士總站及公廁，上層為新葵興廣場商場。

## 公共交通

## 外部參考

  - [葵興政府合署](https://web.archive.org/web/20070928064611/http://www.tsuenwan.info/content.asp?id=gov_offices)

[Category:香港政府合署](../Category/香港政府合署.md "wikilink")
[Category:葵涌](../Category/葵涌.md "wikilink")