**阿瑟·艾許·米勒**（，），生于[美国](../Page/美国.md "wikilink")[纽约](../Page/纽约.md "wikilink")，[美國](../Page/美國.md "wikilink")[犹太裔劇作家及](../Page/犹太裔.md "wikilink")[瑪麗蓮·夢露的第三任丈夫](../Page/瑪麗蓮·夢露.md "wikilink")，他以劇作《[推銷員之死](../Page/推銷員之死.md "wikilink")》、《[萨勒姆的女巫](../Page/萨勒姆的女巫.md "wikilink")》而聞名，[肯尼迪中心荣誉奖得主](../Page/肯尼迪中心荣誉奖.md "wikilink")。

## 生平

米勒生於[波蘭猶太人移民家庭](../Page/波蘭猶太人歷史.md "wikilink")\[1\]。米勒是[猶太人](../Page/猶太人.md "wikilink")\[2\]\[3\]\[4\]。他的父親伊薩德（Isadore）拥有一家400人的女裝工厂，母親奧嘉斯達（Augusta）是家庭主婦。除亞瑟·米勒外，還育有克密特（Kermit）以及喬恩（Joan）。全家居住在[曼哈頓一幢可以俯瞰](../Page/曼哈頓.md "wikilink")[中央公園的閣樓中](../Page/中央公園_\(紐約市\).md "wikilink")。但1929年[大蕭條使他家变得一贫如洗](../Page/大蕭條.md "wikilink")，于是搬家到布鲁克林区，当时米勒十几岁，每天早起，在上学前送面包挣钱补贴家用。从林肯高中毕业后，米勒在一家倉庫做搬運工和文員以支付大學學費。

1936年米勒發表第一部劇作《[榮譽的真諦](../Page/榮譽的真諦.md "wikilink")》（*Honors at
Dawn*）在[密西根大學公演](../Page/密西根大學.md "wikilink")，為他贏得[霍普渥德獎](../Page/霍普渥德獎.md "wikilink")（Hopwood
Award）。兩年後他自[密歇根大學畢業](../Page/密歇根大學.md "wikilink")，取得[英语學位](../Page/英语.md "wikilink")。1940年時，米勒與在大學中結識的女友瑪莉·史拉托利（Mary
Slattery）結婚。婚後育有珍妮（Jane）與羅伯特（Robert）二子，而因為他曾經在美式足球賽中受過傷，有幸可以在[第二次世界大戰期間免除兵役](../Page/第二次世界大戰.md "wikilink")。

米勒在1949年的劇作《[推銷員之死](../Page/推銷員之死.md "wikilink")》為他贏得了[普立茲獎](../Page/普立茲獎.md "wikilink")、三座[東尼獎](../Page/東尼獎.md "wikilink")，以及[紐約劇評人圈內獎](../Page/紐約劇評人圈內獎.md "wikilink")，《推銷員之死》是一部一次贏得上述三項獎項的劇本。他的下一部劇本《熔爐》(在1995年改編成電影時，在中港台等地分別譯為《薩勒姆的女巫》、《靈欲劫》以及《激情年代》等)1953年1月22日在[百老匯首演](../Page/百老匯.md "wikilink")。1956年時，與妻子仳離，同年6月，美國[眾院非美行為調查委員會調查演藝圈有無](../Page/眾院非美行為調查委員會.md "wikilink")[共產黨顛覆活動](../Page/共產黨.md "wikilink")，被[伊利亞·卡贊](../Page/伊利亞·卡贊.md "wikilink")（Elia
Kazan）指稱參與了共黨集會，在月底（6月29日），他迎娶了八年前透過卡贊所結識的[瑪麗蓮·夢露](../Page/瑪麗蓮·夢露.md "wikilink")。

米勒1953年創作的《煉獄》因反對美國的極右翼[麥卡錫主義被譽為](../Page/麥卡錫主義.md "wikilink")「美國戲劇的良心」，1957年3月31日，米勒因為拒絕提供參與共黨集會者的姓名，被控藐視[國會](../Page/美国国会.md "wikilink")，之後在1958年8月8日由[美國最高法院給予平反](../Page/美國最高法院.md "wikilink")。同年，發表《戲劇集》（*Collected
Plays*）。

他在1960年創作出長篇小說《不合時宜的人》，繼而改編成電影腳本，成為夢露出演的最後一部影片。在1961年1月24日時，他與夢露離婚，隔年2月17日另娶攝影師[英格·莫拉絲](../Page/英格·莫拉絲.md "wikilink")（Inge
Morath）。米勒和莫拉絲是在拍攝電影《[花田錯](../Page/花田錯.md "wikilink")》（*The
Misfits*）時結識，育有瑞貝卡（Rebecca）與丹尼爾（Daniel）二子。根據傳記作家[馬丁·葛登能](../Page/馬丁·葛登能.md "wikilink")（Martin
Gottfried）的說法，丹尼爾在1962年出生時就有[唐氏症](../Page/唐氏症.md "wikilink")。米勒將丹尼爾安置在羅克斯伯的一家醫療機構後，就再也沒有看過他（雖然莫拉絲有）。米勒在他的自傳《時光樞紐》中也沒有提到過丹尼爾，[紐約時報在](../Page/紐約時報.md "wikilink")2005年2月11日的訃聞\[[http://www.nytimes.com/2005/02/11/theater/11cnd-miller.html?hp=\&pagewanted=print\&position=\]中也忽略了這點](http://www.nytimes.com/2005/02/11/theater/11cnd-miller.html?hp=&pagewanted=print&position=%5D中也忽略了這點)。（在[洛杉磯時報以及其他地方](../Page/洛杉磯時報.md "wikilink")，則有相關報導）

莫拉絲逝世於2002年1月30日，同年5月1日，米勒成為[西班牙](../Page/西班牙.md "wikilink")[阿斯圖里亞斯親王獎文學獎得主](../Page/阿斯圖里亞斯親王獎.md "wikilink")（Principe
de Asturias Prize for
Literature），譽為「毋庸置疑的現代戲劇大師」。這個獎項之前的得主包括[多麗絲·萊辛](../Page/多麗絲·萊辛.md "wikilink")（Doris
Lessing）、[君特·格拉斯](../Page/君特·格拉斯.md "wikilink")（Günter
Grass）、[卡洛斯·富安蒂斯](../Page/卡洛斯·富安蒂斯.md "wikilink")（Carlos
Fuentes）等。

2004年12月時，89歲的米勒宣佈他與34歲的藝術家[阿格尼斯·巴萊](../Page/阿格尼斯·巴萊.md "wikilink")（Agnes
Barley）自2002年起已經同居兩年，並且計畫結婚。在2005年2月10日深夜在位於美國康涅狄格州的家中病逝，米勒因心臟衰竭過世
，享年89歲。

## 作品

米勒反對商業化、純娛樂的庸俗戲劇。他的作品著重於家庭、道德和個人責任感，對美國社會產生了極大影響。

### 舞台劇本

  - 《[榮譽的真諦](../Page/榮譽的真諦.md "wikilink")》（，1935）
  - 《[天之驕子](../Page/天之驕子.md "wikilink")》（，1944）
  - 《[吾子吾弟](../Page/吾子吾弟.md "wikilink")》（，1947）
  - 《[推銷員之死](../Page/推銷員之死.md "wikilink")》（，1949）
  - 《[激情年代](../Page/激情年代.md "wikilink")》（，1953）
  - 《[憶兩個星期一](../Page/憶兩個星期一.md "wikilink")》（，1955）
  - 《[橋上一瞥](../Page/橋上一瞥.md "wikilink")》（，1955）
  - 《[墮落之後](../Page/墮落之後.md "wikilink")》（，1964）
  - 《[維琪事件](../Page/維琪事件.md "wikilink")》（，1965）
  - 《[代價](../Page/代價.md "wikilink")》（，1968）
  - 《[創世紀及其他](../Page/創世紀及其他.md "wikilink")》（，1972）
  - 《[大主教之屋](../Page/大主教之屋.md "wikilink")》（，1977）
  - 《[美國鐘](../Page/美國鐘.md "wikilink")》（，1981）
  - 《[一位女士的輓歌](../Page/一位女士的輓歌.md "wikilink")》（，1982）
  - 《[某種愛情故事](../Page/某種愛情故事.md "wikilink")》（，1982）
  - 《[危險：記憶！兩部劇本](../Page/危險：記憶！兩部劇本.md "wikilink")》（，1986）—《我什麼都不記得》（）與《克拉瑞》（*Clara*）
  - 《[騎下摩根山](../Page/騎下摩根山.md "wikilink")》（，1991）
  - 《[最後的洋基](../Page/最後的洋基.md "wikilink")》（，1993）
  - 《[碎玻璃](../Page/碎玻璃.md "wikilink")》（，1994）
  - 《[彼得斯先生的連線](../Page/彼得斯先生的連線.md "wikilink")》（，1998）
  - 《[萊恩面訪](../Page/萊恩面訪.md "wikilink")》（，2000）
  - *[Resurrection Blues](../Page/Resurrection_Blues.md "wikilink")*
    (2004)
  - 《[完成圖畫](../Page/完成圖畫.md "wikilink")》（，2004）

### 電影劇本

  - 《[花田錯](../Page/花田錯.md "wikilink")》，（，[IMDB](http://www.imdb.com/title/tt0055184/))，1961）
  - 《[人民公敵](../Page/人民公敵.md "wikilink")》（，[IMDB](http://www.imdb.com/title/tt0251072/)
    ，改寫自[亨利·易卜生](../Page/亨利·易卜生.md "wikilink")（Henrik Ibsen）原著，1966）
  - 《[時間遊戲](../Page/時間遊戲.md "wikilink")》（，[IMDB](http://imdb.com/title/tt0081344/)
    ，電視劇本 ，1980）
  - 《[人人皆贏](../Page/人人皆贏.md "wikilink")》（，[IMDB](http://www.imdb.com/title/tt0099520/)，1989）

### 其他作品

  - 《焦點》（，1945）
  - 《希望渺茫（但不嚴重）的環境》（）
  - 《黃金歲月》（）
  - 《名聲》（）
  - 《原因》（）
  - 《無家可歸的女孩的生平：以及其他》（）
  - 《亞瑟·米勒劇場文集》（）
  - 自傳《時光樞紐》（）

## 参考资料

## 附註

本詞條主要根據英文版維基百科內容編寫，米勒作品中譯主要根據張靜二《亞瑟·米勒的戲劇研究》（1989年，台北：書林）中所使用的翻譯。

## 外部連結

  - [The Arthur Miller Society](http://www.ibiblio.org/miller/)
  - [Arthur Miller's IMDB Profile](http://www.imdb.com/name/nm0007186/)
  - [New York Times
    Obituary](http://www.nytimes.com/2005/02/11/theater/11cnd-miller.html?hp=&pagewanted=print&position=)
  - [Los Angeles Times
    Obituary](http://www.latimes.com/news/obituaries/la-021105miller_lat,0,4291032.story?coll=la-home-headlines)
  - [BBC News
    Obituary](http://news.bbc.co.uk/1/hi/entertainment/arts/233032.stm)

[Category:美國作家](../Category/美國作家.md "wikilink")
[Category:美國劇作家](../Category/美國劇作家.md "wikilink")
[Category:后现代作家](../Category/后现代作家.md "wikilink")
[Category:普利策戏剧奖得主](../Category/普利策戏剧奖得主.md "wikilink")
[Category:艾美奖获奖者](../Category/艾美奖获奖者.md "wikilink")
[Category:东尼奖得主](../Category/东尼奖得主.md "wikilink")
[Category:肯尼迪中心荣誉奖得主](../Category/肯尼迪中心荣誉奖得主.md "wikilink")
[Category:麥卡錫主義受害者](../Category/麥卡錫主義受害者.md "wikilink")
[Category:密歇根大学校友](../Category/密歇根大学校友.md "wikilink")
[Category:死于心血管疾病的人](../Category/死于心血管疾病的人.md "wikilink")
[Category:美国不可知论者](../Category/美国不可知论者.md "wikilink")
[Category:波兰裔美国人](../Category/波兰裔美国人.md "wikilink")
[Category:美国犹太人](../Category/美国犹太人.md "wikilink")
[Category:康乃狄克州人](../Category/康乃狄克州人.md "wikilink")
[Category:高松宫殿下纪念世界文化奖获得者](../Category/高松宫殿下纪念世界文化奖获得者.md "wikilink")
[Category:阿斯图里亚斯亲王奖获得者](../Category/阿斯图里亚斯亲王奖获得者.md "wikilink")

1.
2.
3.
4.