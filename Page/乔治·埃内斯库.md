**乔治·埃内斯库**（，），[罗马尼亚](../Page/罗马尼亚.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")、[指挥家](../Page/指挥家.md "wikilink")、[小提琴家](../Page/小提琴家.md "wikilink")、[钢琴家](../Page/钢琴家.md "wikilink")。

## 生平

乔治·埃内斯库诞生于罗马尼亚[多罗霍伊郡](../Page/多罗霍伊.md "wikilink")[利文尼村](../Page/利文尼.md "wikilink")。7岁入讀[维也纳音乐学院](../Page/维也纳音乐学院.md "wikilink")，師从[約瑟夫·黑爾梅斯伯格](../Page/約瑟夫·黑爾梅斯伯格.md "wikilink")、[羅伯特·富克斯](../Page/羅伯特·富克斯.md "wikilink")、Sigismond
Bachrich等音乐大师。12岁毕业，获得银质奖章。少年埃内斯库在维也纳音乐会上演奏[勃拉姆斯](../Page/约翰内斯·勃拉姆斯.md "wikilink")、[萨拉萨蒂](../Page/帕布罗·德·萨拉萨蒂.md "wikilink")、[门德尔松的作品](../Page/费利克斯·门德尔松.md "wikilink")。1895年埃内斯库前往[巴黎继续深造](../Page/巴黎.md "wikilink")，从Armand
Marsick学习小提琴，从[儒勒·马斯内和](../Page/儒勒·马斯内.md "wikilink")[福莱学作曲](../Page/福莱.md "wikilink")。

埃内斯库的作品深受[罗马尼亚民族音乐的影响](../Page/罗马尼亚.md "wikilink")，包括他的代表作：《罗马尼亚狂想曲》（1901–1902），歌剧Oédipe和管弦乐组曲。

1923年埃内斯库初次作为乐队指挥，登台[纽约音乐会指挥](../Page/纽约.md "wikilink")[费城交响乐团](../Page/费城交响乐团.md "wikilink")。

1937年至1938年任[纽约交响乐团指挥](../Page/紐約愛樂.md "wikilink")。

[二战前埃内斯库住在](../Page/二战.md "wikilink")[巴黎和罗马尼亚](../Page/巴黎.md "wikilink")，二战后埃内斯库以巴黎为家。

埃内斯库是闻名世界的小提琴教师：著名小提琴家[耶胡迪·梅纽因](../Page/耶胡迪·梅纽因.md "wikilink")、[克里斯蒂安·费拉斯](../Page/克里斯蒂安·费拉斯.md "wikilink")、[艾佛里·纪特里斯和](../Page/艾佛里·纪特里斯.md "wikilink")[亞瑟·葛羅米歐都是他的门生](../Page/亞瑟·葛羅米歐.md "wikilink")。

1955年5月4日埃内斯库在巴黎去世，长眠于巴黎[拉雪茲神父公墓](../Page/拉雪茲神父公墓.md "wikilink")。

[布加勒斯特交响乐团以埃内斯库命名作为纪念](../Page/布加勒斯特交响乐团.md "wikilink")。

[布加勒斯特市内有一所埃内斯库纪念馆](../Page/布加勒斯特.md "wikilink")。

## 作品

  - **[歌剧](../Page/歌剧.md "wikilink")**
      - *俄狄浦斯王*. op.23 (1921-31; UA 1936)
  - **[交响乐](../Page/交响乐.md "wikilink")**
      - 四部早年的交响曲（D 小调 1894, F大调 1895, F大调 1896，降E小调 1898）
      - 降E大调第一交响曲 op.13 (1905)
      - A大调第二交响曲 op.17 (1912-14)
      - C大调第三交响曲 op.21 (1916-18, rev. 1921)
      - E小调第四交响曲（1934；未完成）
      - D大调第5交响曲（1941；未完成）
  - **[管弦乐](../Page/管弦乐.md "wikilink")**
      - 3部序曲 (1891-94)
      - *Tragische Ouvertüre* (1895)
      - *凯旋序曲* (1896)
      - A小调小提琴协奏曲 (1896)
      - 中提琴協奏曲
      - 钢琴与管弦乐幻想曲 (1896)
      - 钢琴协奏曲（1897；未完成）
      - 第一罗马尼亚组曲（1896；未完成）
      - 第二罗马尼亚组曲 (1897)
      - 罗马尼亚音诗 op.1 (1897)
      - Symphonie concertante h-moll für Violoncello und Orchester op.8
        (1901)
      - C大调第一组曲 op.9 (1903)
      - 2 *罗马尼亚狂想曲* op.11 (1901)
      - 2 *Intermezzi* für Streicher op.12 (1902-03)
      - *Suite châtelaine* (1911; Fragment)
      - C大调第二组曲 op.20 (1915)
      - D大调第三组曲 op.27 (1937-38)
      - Konzertouvertüre *sur des thèmes dans le caractère populaire
        roumain* A-Dur op.32 (1948)
  - **[室内乐](../Page/室内乐.md "wikilink")**
      - 钢琴五重奏 (1895)
      - 钢琴三重奏 (1897)
      - D大调第一小提琴演奏鸣曲 op.2 (1897)
      - F小调第一大提琴奏鸣曲 op.26 Nr.1 (1898)
      - *Aubade* für Streichtrio (1899)
      - F小调第二小提琴奏鸣曲 op.6 (1899)
      - *Andante religioso* für 2 Celli und Orgel (1900)
      - C大调弦乐八重奏 op.7 (1900)
      - *Allegro de concert* für Harfe (1904)
      - Bläserdezett D-Dur op.14 (1906)
      - D大调第一钢琴四重奏 op.16 (1909)
      - A小调钢琴三重奏 (1916)
      - 降E大调第一弦乐四重奏 op.22 Nr.1 (1916-20)
      - A小调第三小提琴奏鸣曲 op.25 (1926)
      - C大调第二大提琴演奏鸣曲 op.26 Nr.2 (1935)
      - *童年回忆* 小提琴与钢琴 op.28 (1940)
      - A小调钢琴五重奏 op.29 (1940)
      - 第二D小调钢琴五重奏 Nr.2 d-moll op.30 (1943-44)
      - G大调第二弦乐四重奏 op.22 Nr.2 (1950-53)
      - E 大调 钢琴交响乐，12音乐器 op.33 (1954)
  - **钢琴曲**
      - 第一G大调钢琴组曲 op.3 (1897)
      - Variationen für 2 Klavier op.5 (1898)
      - 第二D大调钢琴组曲 op.10 (1901-03)
      - 序曲与赋格 (1903)
      - 第三钢琴组曲 op.18 (1913-16)
      - 第一降F小调钢琴奏鸣曲 op.24 Nr.1 (1924)
      - 第二D大调钢琴奏鸣曲 op.24 Nr.3 (1933-35)
  - **合唱**
      - *曙光* 女高音、女声合唱、管弦乐 (1897-98)
      - *Vox maris*. Sinfonische Dichtung für Sopran, Tenor, Chor und
        Orchester op.31 (1929-51)
      - *Linişte (Stille)* (1946)
  - **歌曲**
      - *Trois Mélodies* op.4 (1898)
      - *[Légende](../Page/Légende_\(Enescu\).md "wikilink"), for
        trumpet and piano* (1906)
      - *Sept chansons de Clément Marot* op.15 (1908)
      - 3 *melodii* op.19 (1915-16)
      - 其他25首歌曲。

## 参见

  - [古典音乐作曲家列表](../Page/古典音乐作曲家列表.md "wikilink")

[Enescu, George](../Category/羅馬尼亞音樂家.md "wikilink") [Enescu,
George](../Category/羅馬尼亞作曲家.md "wikilink") [Enescu,
George](../Category/指挥家.md "wikilink") [Enescu,
George](../Category/羅馬尼亞小提琴家.md "wikilink") [Enescu,
George](../Category/鋼琴家.md "wikilink") [Enescu,
George](../Category/20世纪作曲家.md "wikilink")
[Category:歐洲紙幣上的人物](../Category/歐洲紙幣上的人物.md "wikilink")
[Category:安葬于拉雪兹神父公墓者](../Category/安葬于拉雪兹神父公墓者.md "wikilink")