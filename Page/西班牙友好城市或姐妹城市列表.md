此列表延伸自[歐洲友好城市列表](../Page/歐洲友好城市列表.md "wikilink")。

|                                                         |                                                                                                                 |
| ------------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------- |
|             〖[Almansa](../Page/Almansa.md "wikilink")〗- | <small>[英國](../Page/英國.md "wikilink")[Lymington](../Page/Lymington.md "wikilink")                               |
|                 〖[埃納雷斯堡](../Page/埃納雷斯堡.md "wikilink")〗- | <small>[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")[阿爾巴尤利亞](../Page/阿爾巴尤利亞.md "wikilink")                                 |
|                                                         | <small>[英國](../Page/英國.md "wikilink")[彼得伯勒](../Page/彼得伯勒.md "wikilink")                                         |
|                 〖[Altea](../Page/Altea.md "wikilink")〗- | <small>[英國](../Page/英國.md "wikilink")[Sherborne](../Page/Sherborne.md "wikilink")                               |
|                   〖[巴塞羅那](../Page/巴塞羅那.md "wikilink")〗- | <small>[德國](../Page/德國.md "wikilink")[科隆](../Page/科隆.md "wikilink") (1984年)                                     |
|                                                         | <small>[波蘭](../Page/波蘭.md "wikilink")[格但斯克](../Page/格但斯克.md "wikilink")                                         |
|                                                         | <small>[波斯尼亞黑塞哥維那](../Page/波斯尼亞黑塞哥維那.md "wikilink")[薩拉熱窩](../Page/薩拉熱窩.md "wikilink") (1996年)                   |
|                     〖[畢爾包](../Page/畢爾包.md "wikilink")〗- | <small>[美國](../Page/美國.md "wikilink")[喬治亞州](../Page/喬治亞州.md "wikilink")[Tbilisi](../Page/Tbilisi.md "wikilink") |
|                     〖[加的斯](../Page/加的斯.md "wikilink")〗- | <small>[法國](../Page/法國.md "wikilink")[布列斯特](../Page/布列斯特.md "wikilink") (1986年)                                 |
|                   〖[格拉納達](../Page/格拉納達.md "wikilink")〗- | <small>[德國](../Page/德國.md "wikilink")[弗赖堡](../Page/弗赖堡.md "wikilink") (1991年                                    |
|                 〖[瓜達拉哈拉](../Page/瓜達拉哈拉.md "wikilink")〗- | <small>[法國](../Page/法國.md "wikilink")[Roanne](../Page/Roanne.md "wikilink") (1980年)                             |
|                     〖[馬德里](../Page/馬德里.md "wikilink")〗- | <small>[德國](../Page/德國.md "wikilink")[柏林](../Page/柏林.md "wikilink") (1988年)                                     |
|                     〖[馬拉加](../Page/馬拉加.md "wikilink")〗- | <small>[德國](../Page/德國.md "wikilink")[帕绍](../Page/帕绍.md "wikilink")                                             |
|                   〖[奧維耶多](../Page/奧維耶多.md "wikilink")〗- | <small>[德國](../Page/德國.md "wikilink")[波庫](../Page/波庫.md "wikilink")                                             |
| 〖[San Sebastian](../Page/San_Sebastian.md "wikilink")〗- | <small>[西撒哈拉](../Page/西撒哈拉.md "wikilink")[Daira de Bojador](../Page/Daira_de_Bojador.md "wikilink")             |
|                                                         | <small>[西班牙](../Page/西班牙.md "wikilink")[Trento](../Page/Trento.md "wikilink")                                   |
|                                                         | <small>[德國](../Page/德國.md "wikilink")[威斯巴登](../Page/威斯巴登.md "wikilink") (1981年)                                 |
|                   〖[塞维利亚](../Page/塞维利亚.md "wikilink")〗- | <small>[美國](../Page/美國.md "wikilink")[密蘇里州](../Page/密蘇里州.md "wikilink")[堪薩斯市](../Page/堪薩斯市.md "wikilink")       |
|             〖[托萊多](../Page/托萊多_\(西班牙\).md "wikilink")〗- | <small>[美國](../Page/美國.md "wikilink")[俄亥俄州](../Page/俄亥俄州.md "wikilink")[托萊多](../Page/托萊多.md "wikilink") (1931年) |
|                       〖[比戈](../Page/比戈.md "wikilink")〗- | <small>[法國](../Page/法國.md "wikilink")[洛里昂](../Page/洛里昂.md "wikilink")                                           |

[Category:姐妹城市列表](../Category/姐妹城市列表.md "wikilink")
[Category:西班牙城市](../Category/西班牙城市.md "wikilink")
[Category:西班牙相关列表](../Category/西班牙相关列表.md "wikilink")