**滹沱河**（），古又作**虖池**（）或**滹池**\[1\]，是[海河水系的主要河流之一](../Page/海河.md "wikilink")。

滹沱河发源于[山西](../Page/山西.md "wikilink")[繁峙县](../Page/繁峙县.md "wikilink")[泰戏山下](../Page/泰戏山.md "wikilink")，向东流至[河北](../Page/河北.md "wikilink")[献县与](../Page/献县.md "wikilink")[滏阳河汇合后成为](../Page/滏阳河.md "wikilink")[子牙河](../Page/子牙河.md "wikilink")。从发源地到子牙河口全长513.3公里，其中山西境内48.75公里。滹沱河流域面积约4.6万平方公里，是[石家庄的主要水源地](../Page/石家庄.md "wikilink")。

滹沱河的主要支流有[阳武河](../Page/阳武河.md "wikilink")、[云中河](../Page/云中河.md "wikilink")、[牧马河](../Page/牧马河.md "wikilink")、[同河](../Page/同河.md "wikilink")、[清水河](../Page/清水河.md "wikilink")、[南坪河](../Page/南坪河.md "wikilink")、[冶河等](../Page/冶河.md "wikilink")，呈羽状排列，主要集中在[黄壁庄镇以上](../Page/黄壁庄镇.md "wikilink")。流域内地势自西向东呈阶梯状倾斜，西部地处山西高原（[黃土高原之一部](../Page/黃土高原.md "wikilink")）东缘山地和盆地，地势高，黄土分布较厚；中部为[太行山背斜形成的山地](../Page/太行山.md "wikilink")，富煤矿；东部为平原。流域内天然植被稀少，水土流失较重。流经山区、山地和丘陵的面积约占全流域面积的86%，河流总落差达1800余米。瑶池以上为上游，沿五台山向西南流淌于带状盆地中，河槽宽自一二[百米至千米不等](../Page/百米.md "wikilink")，水流缓慢。瑶池至岗南为中游，流经太行山区，河谷深切，呈“V”形谷，宽度均在200米以下，落差大，水流湍急。黄壁庄以下为下游，流经平原，河道宽广，最宽可达6000米，水流缓慢，泥沙淤积，渐成地上河或半地上河。两岸筑有堤防。

由于上游筑有[岗南水库与](../Page/岗南水库.md "wikilink")[黄壁庄水库](../Page/黄壁庄水库.md "wikilink")，滹沱河在黄壁庄水库以下流域曾常年处于干涸断流状态。2010年之后，随着石家庄市对城区内滹沱河沿岸地区进行开发，滹沱河在流经石家庄市区一段恢复了常年有水的状态，并建立了[滹沱河城市森林公园](../Page/滹沱河城市森林公园.md "wikilink")。但在流经市区后的其他下游地区，除上游两座水库每年雨季放水时以外，依旧长期断流。

## 注释

<div class="references-small">

<references />

</div>

## 参考资料

  - [石家庄商务之窗编辑部·滹沱河](http://hebei.mofcom.gov.cn/column/print.shtml?/sjdixiansw/200510/20051000666103)
  - [石家庄地区水利志](http://www.sjz.gov.cn/art/2006/10/21/art_17336_134683.html)

[Category:海河水系](../Category/海河水系.md "wikilink")
[Category:山西河流](../Category/山西河流.md "wikilink")
[Category:河北河流](../Category/河北河流.md "wikilink")

1.  《廣韻》歌韻，“池：虖池，水名，在[并州界](../Page/并州.md "wikilink")，出《周禮》，又音馳。”