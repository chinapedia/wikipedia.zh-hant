**数据管理**，即对数据资源的管理。按照[:en:DAMA的定义](../Page/:en:DAMA.md "wikilink")：“数据资源管理，致力于发展处理[企业数据生命周期的适当的建构](../Page/企业.md "wikilink")、策略、实践和程序”。这是一个高层而包含广泛的定义，而并不一定直接涉及数据管理的具体操作（如[关系数据库的技术层次上的管理](../Page/关系数据库.md "wikilink")）。

数据管理的常见内容包括:

  - [数据分析](../Page/数据分析.md "wikilink")
  - [数据建模](../Page/数据建模.md "wikilink")
  - [数据库管理](../Page/数据库管理.md "wikilink")
  - [数据仓库](../Page/数据仓库.md "wikilink")
  - [数据挖掘](../Page/数据挖掘.md "wikilink")
  - [数据安全](../Page/数据安全.md "wikilink")
  - [数据整合](../Page/数据整合.md "wikilink")
  - [Data movement](../Page/Data_movement.md "wikilink")
  - [Data quality
    assurance](../Page/Data_quality_assurance.md "wikilink")
  - [Meta-data management (data repositories, and their
    management)](../Page/Meta-data_management.md "wikilink")
  - [Strategic data
    architecture](../Page/Strategic_data_architecture.md "wikilink")

## 数据管理的主题（领域）

数据管理的主题, 根据 DAMA DMBOK 框架划分, 包括:\[1\]

1.  [数据治理](../Page/数据治理.md "wikilink")
      - [数据资产](../Page/数据资产.md "wikilink")
      - [数据管治](../Page/数据管治.md "wikilink")
      - [数据管家](../Page/数据管家.md "wikilink")
2.  数据架构、数据（模型）分析和设计
      - [数据架构](../Page/数据架构.md "wikilink")
      - [数据分析](../Page/数据分析.md "wikilink")
      - [数据建模](../Page/数据建模.md "wikilink")
3.  数据库管理
      - [数据维护](../Page/数据维护.md "wikilink")
      - [数据库管理](../Page/数据库管理.md "wikilink")
      - [数据库管理系统](../Page/数据库管理系统.md "wikilink")
4.  数据安全管理
      - [数据访问管理](../Page/数据访问管理.md "wikilink")
      - [数据擦除管理](../Page/数据擦除管理.md "wikilink")
      - [数据隐私](../Page/数据隐私.md "wikilink")
      - [数据安全](../Page/数据安全.md "wikilink")
5.  数据质量管理
      - [数据清晰](../Page/数据清晰.md "wikilink")
      - [数据完整性](../Page/数据完整性.md "wikilink")
      - [数据浓缩](../Page/数据浓缩.md "wikilink")
      - [数据质量](../Page/数据质量.md "wikilink")
      - [数据质量保证](../Page/数据质量保证.md "wikilink")
6.  参考和主数据管理
      - [数据集成](../Page/数据集成.md "wikilink")
      - [主数据管理](../Page/主数据管理.md "wikilink")
      - [参考数据](../Page/参考数据.md "wikilink")
7.  数据仓库和商业智能化管理
      - [商业智能](../Page/商业智能.md "wikilink")
      - [数据集市](../Page/数据集市.md "wikilink")
      - [数据挖掘](../Page/数据挖掘.md "wikilink")
      - [数据移动](../Page/数据移动.md "wikilink") (萃取、 转换和加载)
      - [数据仓库](../Page/数据仓库.md "wikilink")
8.  Document, Record and Content Management
      - [Document management
        system](../Page/Document_management_system.md "wikilink")
      - [Records management](../Page/Records_management.md "wikilink")
9.  元数据管理
      - [元数据管理](../Page/元数据管理.md "wikilink")
      - [元数据](../Page/元数据.md "wikilink")
      - [元数据发现](../Page/元数据发现.md "wikilink")
      - [元数据发布](../Page/元数据发布.md "wikilink")
      - [元数据注册](../Page/元数据注册.md "wikilink")
10. 联系人数据管理Contact Data Management
      - [业务连续性规划](../Page/业务连续性规划.md "wikilink")
      - [市场运营](../Page/市场运营.md "wikilink")
      - [客户数据集成](../Page/客户数据集成.md "wikilink")
      - [身份管理](../Page/身份管理.md "wikilink")
      - [身份信息窃取](../Page/身份信息窃取.md "wikilink")
      - [数据被盗](../Page/数据被盗.md "wikilink")
      - [ERP 软件](../Page/ERP_软件.md "wikilink")
      - [客户关系管理软件](../Page/客户关系管理软件.md "wikilink")
      - [地址 （地理）](../Page/地址_（地理）.md "wikilink")
      - [邮编](../Page/邮编.md "wikilink")
      - [Email 地址](../Page/Email_地址.md "wikilink")
      - [电话号码](../Page/电话号码.md "wikilink")

## 参考文献

## 外部链接

  - [DAMA International -
    数据管理协会](https://web.archive.org/web/20041011112836/http://www.dama.org/public/pages/index.cfm?pageid=1)
  - [DM 评论](http://www.dmreview.com/)

[数据管理](../Category/数据管理.md "wikilink")

1.   "DAMA-DMBOK Functional Framework v3"