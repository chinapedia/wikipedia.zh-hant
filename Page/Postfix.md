**Postfix**
是一種[電子郵件](../Page/電子郵件.md "wikilink")[伺服器](../Page/伺服器.md "wikilink")，它是由任職於[IBM](../Page/IBM.md "wikilink")[華生研究中心](../Page/華生研究中心.md "wikilink")（T.J.
Watson Research Center）的荷蘭籍研究員Wietse
Venema為了改良[sendmail郵件伺服器而產生的](../Page/sendmail.md "wikilink")。最早在1990年代晚期出現，是一個[開放源代碼的軟體](../Page/開放源代碼.md "wikilink")。

## 外部連結

  - [Postfix](http://www.postfix.org) 官方網站
  - [Postfix正體中文社群](https://web.archive.org/web/20050707233314/http://postfix.fsftw.org/)
  - [Postfix Mailling List
    archive](https://web.archive.org/web/20090328121201/http://www.pubbs.net/postfix/)
  - [Wietse 先生的个人主页](http://www.porcupine.org/wietse/) 作者的个人主页介绍

[Category:郵件傳送代理](../Category/郵件傳送代理.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:Unix软件](../Category/Unix软件.md "wikilink")