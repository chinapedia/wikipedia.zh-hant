**搜尋引擎行銷**（**Search Engine
Marketing**，縮寫为**SEM**）是一種以透過增加[搜尋引擎結果頁](../Page/搜尋引擎.md "wikilink")（Search
Engine Result
Pages，SERP）能見度的方式，或是透過搜尋引擎的內容聯播網來推銷網站的[網路行銷模式](../Page/網路行銷.md "wikilink")。

根據搜尋引擎行銷專業機構（Search Engine Marketing Professionals
Organization，SEMPO）整理，目前主流的SEM包含：[搜尋引擎最佳化](../Page/搜尋引擎最佳化.md "wikilink")（SEO）、[關鍵字點擊廣告（PPC）及](../Page/每点击付费.md "wikilink")[付費收錄廣告](../Page/付費收錄.md "wikilink")（PFI）\[1\]。

而[紐約時報定義SEM是一種透過購買搜尋列表以達成比免費列表更高排名目標的手段](../Page/紐約時報.md "wikilink")\[2\]\[3\]
當有其他公司購買同一組關鍵字時，它的收費會越貴，這是關鍵字的競價模式。

## 歷史

隨著[網站於](../Page/網站.md "wikilink")20世纪90年代中後期雨後春筍般增加，以及行動裝置的普及，[搜尋引擎開始顯著地幫助人們快速尋找到所要資訊](../Page/搜尋引擎.md "wikilink")。搜尋引擎開發出各種商業模型作為它們提供服務的資金，如
Open Text 公司於1996年提供的[每次點擊付費方案](../Page/每点击付费.md "wikilink")\[4\]以及隨後
Goto.com 公司於1998年的類似方案 \[5\]。Goto.com
隨後於2001年更名\[6\]為商序曲（Overture）公司，並且被
Yahoo\! 於2003年收購。現今則透過[雅虎搜尋行銷提供廣告商付費搜尋機會](../Page/雅虎搜尋行銷.md "wikilink")。
Google 也開始於2000年透過 [Google Ads](../Page/Google_AdWords.md "wikilink")
方案，於搜尋結果頁提供廣告。截至2007年，每次點擊付費方案證明了它對搜尋引擎而言是首要的「印鈔機」\[7\]。

[搜尋引擎最佳化諮詢師隨後擴充他們的生意至幫助客戶學習使用搜尋引擎提供的廣告機會](../Page/搜尋引擎最佳化.md "wikilink")，焦點集中於透過搜尋引擎來做行銷與廣告。至於「搜尋引擎行銷」術語一辭是丹尼蘇利文（Danny
Sullivan）於2001年所倡議\[8\]，以包括執行SEO、管理於搜尋引擎的付費列表、提交網站到網路目錄、以及為商業組織或個人開發線上行銷策略的種種活動。

2006年，北美廣告商花費94億美金於搜尋引擎行銷。相較於2005年增長62%，並相較於2002年增長750%。當時最大的SEM廠商為[Google
AdWords](../Page/Google_AdWords.md "wikilink")、[雅虎搜尋行銷以及](../Page/雅虎搜尋行銷.md "wikilink")[微軟](../Page/微軟.md "wikilink")[adCenter](../Page/adCenter.md "wikilink")\[9\]。光2006一年，SEM成長遠較傳統[廣告行業成長更快](../Page/廣告.md "wikilink")\[10\]。

## 市場結構

  - 目前提供SEM的搜尋引擎

<!-- end list -->

  - [Google](../Page/Google.md "wikilink") — 全球
  - [Yahoo\!](../Page/Yahoo!.md "wikilink") — 全球
  - [Microsoft Live](../Page/Microsoft_Live.md "wikilink") — 全球
  - [Bing](../Page/Bing.md "wikilink") — 全球
  - [Ask.com](../Page/Ask.com.md "wikilink") — 全球
  - [百度](../Page/百度.md "wikilink") — [中國](../Page/中國.md "wikilink")
  - [Yandex](../Page/Yandex.md "wikilink") —
    [俄羅斯](../Page/俄羅斯.md "wikilink")
  - [Rambler](../Page/Rambler.md "wikilink") —
    [俄羅斯](../Page/俄羅斯.md "wikilink")



#### 目前提供SEM諮詢服務的企業

##### 美國

  - [Alexa](../Page/Alexa_Internet.md "wikilink")
  - Ahrefs
  - [SimilarWeb](../Page/SimilarWeb.md "wikilink")

## 道德上的問題

付費搜尋廣告從未停止過爭議。此外，圍繞搜尋引擎該顯示出多少廣告於結果頁面組合才適當的種種問題，已成為來自於[消費者聯盟](../Page/消費者聯盟.md "wikilink")（Consumer
Union）的消費者報導網路觀察之一系列研究報告的目標 \[11\] \[12\]
\[13\]。美國聯邦交易委員會也於2002年發布正式信函\[14\]揭諸於搜尋引擎裡付費廣告的重要性，作為對[Commercial
Alert](../Page/Commercial_Alert.md "wikilink") — 一個以拉夫納德（[Ralph
Nader](../Page/Ralph_Nader.md "wikilink")）為聯絡人的顧客擁護族群 — 控訴的回應。

## 參見

  - 技術

<!-- end list -->

  - [搜尋引擎最佳化](../Page/搜尋引擎最佳化.md "wikilink")（Search Engine
    Optimization，簡稱SEO）

<!-- end list -->

  -
    機構

<!-- end list -->

  - [SEMPO](../Page/SEMPO.md "wikilink")，搜尋引擎專業人員機構，顧名思義是一個非營利專為搜尋引擎行銷人員設立的專門機構。

## 參考文獻

{{-}}

[Category:互聯網術語](../Category/互聯網術語.md "wikilink")
[Category:網路廣告方法](../Category/網路廣告方法.md "wikilink")
[Category:搜尋引擎最佳化](../Category/搜尋引擎最佳化.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.
10.
11.

12.

13.

14.