[Hu_Ruoyu.jpg](https://zh.wikipedia.org/wiki/File:Hu_Ruoyu.jpg "fig:Hu_Ruoyu.jpg")

**胡若愚** (
）\[1\]名**言愚**，[字](../Page/字.md "wikilink")**如愚**，后改字**若愚**，[以字行](../Page/以字行.md "wikilink")，[安徽](../Page/安徽.md "wikilink")[合肥人](../Page/合肥.md "wikilink")。中華民國政治人物。

## 生平

胡若愚生于1895年（清光绪二十一年）。畢業於[国立北京大学](../Page/国立北京大学.md "wikilink")，获[法学士](../Page/法学士.md "wikilink")。他曾任[张作霖镇威上将军公署顾问](../Page/张作霖.md "wikilink")。后来他任[张学良的副官](../Page/张学良.md "wikilink")，并和张学良结拜。

1925年，他任[北洋政府](../Page/北洋政府.md "wikilink")[善后会议](../Page/善后会议.md "wikilink")[张学良的代表](../Page/张学良.md "wikilink")，7月任[临时参政院参政](../Page/临时参政院.md "wikilink")、京师税务监督。1928年6月[皇姑屯事件发生后](../Page/皇姑屯事件.md "wikilink")，張学良派他到[南京同](../Page/南京市.md "wikilink")[南京国民政府的](../Page/南京国民政府.md "wikilink")[蒋介石进行交涉](../Page/蒋介石.md "wikilink")。由于交涉取得好感，張于同年末进行了[东北易幟](../Page/东北易幟.md "wikilink")。

1930年4月，他任[国民政府卫生部政务次长](../Page/国民政府卫生部.md "wikilink")。1930年6月，他任青岛市市长。1931年2月，他兼任北平市代理市长、[国民政府实业部](../Page/国民政府实业部.md "wikilink")[开滦矿务局督办](../Page/开滦矿务局.md "wikilink")。1932年1月，他辞去青岛市长職務。

此後，他闲居天津，脱离政界，成天以书法、念经、观剧度日。[第二次国共内战末期](../Page/第二次国共内战.md "wikilink")，他谢绝了朋友[孔祥熙赴台湾的邀请](../Page/孔祥熙.md "wikilink")。晩年他因病长期静养。

1962年他在天津病逝。享年66岁。

## 注释

## 参考文献

  - <span style="font-size:90%;">[蘇利鵬・王建一「天津小洋楼之胡若愚旧居」](https://web.archive.org/web/20110917064454/http://www.tianjinwe.com/tianjin/tbbd/201001/t20100119_485641.html)天津網（『天津日報』网站）</span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>

[Category:善后会议代表](../Category/善后会议代表.md "wikilink")
[Category:临时参政院参政](../Category/临时参政院参政.md "wikilink")
[Category:奉系人物](../Category/奉系人物.md "wikilink")
[Category:合肥人](../Category/合肥人.md "wikilink")
[R](../Category/胡姓.md "wikilink")
[Category:北京大學校友](../Category/北京大學校友.md "wikilink")

1.  蘇利鵬、王建一，天津小洋楼之胡若愚旧居，天津網 作生于1897年。徐友春主編『民国人物大辞典 増訂版』984頁以及Who's Who
    in China 5th
    ed.,p.104作生于1895年（[清](../Page/清.md "wikilink")[光緒](../Page/光緒.md "wikilink")21年）。