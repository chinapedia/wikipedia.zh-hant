[南岗区](../Page/南岗区.md "wikilink"){{.w}}[道外区](../Page/道外区.md "wikilink"){{.w}}[平房区](../Page/平房区_\(哈尔滨市\).md "wikilink"){{.w}}[香坊区](../Page/香坊区.md "wikilink"){{.w}}[呼兰区](../Page/呼兰区.md "wikilink"){{.w}}[阿城区](../Page/阿城区.md "wikilink"){{.w}}[双城区](../Page/双城区.md "wikilink"){{.w}}[尚志市](../Page/尚志市.md "wikilink"){{.w}}[五常市](../Page/五常市.md "wikilink"){{.w}}[依兰县](../Page/依兰县.md "wikilink"){{.w}}[方正县](../Page/方正县.md "wikilink"){{.w}}[宾县](../Page/宾县.md "wikilink"){{.w}}[巴彦县](../Page/巴彦县.md "wikilink"){{.w}}[木兰县](../Page/木兰县.md "wikilink"){{.w}}[通河县](../Page/通河县.md "wikilink"){{.w}}[延寿县](../Page/延寿县.md "wikilink")
}}

|group3style =text-align: center; |group3 =
[地级市](../Page/地级市.md "wikilink") |list3 =
[龙沙区](../Page/龙沙区.md "wikilink"){{.w}}[铁锋区](../Page/铁锋区.md "wikilink"){{.w}}[昂昂溪区](../Page/昂昂溪区.md "wikilink"){{.w}}[富拉尔基区](../Page/富拉尔基区.md "wikilink"){{.w}}[碾子山区](../Page/碾子山区.md "wikilink"){{.w}}[梅-{里}-斯达斡尔族区](../Page/梅里斯达斡尔族区.md "wikilink"){{.w}}[讷河市](../Page/讷河市.md "wikilink"){{.w}}[龙江县](../Page/龙江县.md "wikilink"){{.w}}[依安县](../Page/依安县.md "wikilink"){{.w}}[泰来县](../Page/泰来县.md "wikilink"){{.w}}[甘南县](../Page/甘南县.md "wikilink"){{.w}}[富裕县](../Page/富裕县.md "wikilink"){{.w}}[克山县](../Page/克山县.md "wikilink"){{.w}}[克东县](../Page/克东县.md "wikilink"){{.w}}[拜泉县](../Page/拜泉县.md "wikilink")

|group4 = [鸡西市](../Page/鸡西市.md "wikilink") |list4 =
[鸡冠区](../Page/鸡冠区.md "wikilink"){{.w}}[恒山区](../Page/恒山区.md "wikilink"){{.w}}[滴道区](../Page/滴道区.md "wikilink"){{.w}}[梨树区](../Page/梨树区.md "wikilink"){{.w}}[城子河区](../Page/城子河区.md "wikilink"){{.w}}[麻山区](../Page/麻山区.md "wikilink"){{.w}}[虎林市](../Page/虎林市.md "wikilink"){{.w}}[密山市](../Page/密山市.md "wikilink"){{.w}}[鸡东县](../Page/鸡东县.md "wikilink")

|group5 = [鹤岗市](../Page/鹤岗市.md "wikilink") |list5 =
[向阳区](../Page/向阳区_\(鹤岗市\).md "wikilink"){{.w}}[工农区](../Page/工农区_\(鹤岗市\).md "wikilink"){{.w}}[南山区](../Page/南山区_\(鹤岗市\).md "wikilink"){{.w}}[兴安区](../Page/兴安区.md "wikilink"){{.w}}[东山区](../Page/东山区_\(鹤岗市\).md "wikilink"){{.w}}[兴山区](../Page/兴山区.md "wikilink"){{.w}}[萝北县](../Page/萝北县.md "wikilink"){{.w}}[绥滨县](../Page/绥滨县.md "wikilink")

|group6 = [双鸭山市](../Page/双鸭山市.md "wikilink") |list6 =
[尖山区](../Page/尖山区.md "wikilink"){{.w}}[岭东区](../Page/岭东区.md "wikilink"){{.w}}[四方台区](../Page/四方台区.md "wikilink"){{.w}}[宝山区](../Page/宝山区_\(双鸭山市\).md "wikilink"){{.w}}[集贤县](../Page/集贤县.md "wikilink"){{.w}}[友谊县](../Page/友谊县.md "wikilink"){{.w}}[宝清县](../Page/宝清县.md "wikilink"){{.w}}[饶河县](../Page/饶河县.md "wikilink")

|group7 = [大庆市](../Page/大庆市.md "wikilink") |list7 =
[萨尔图区](../Page/萨尔图区.md "wikilink"){{.w}}[龙凤区](../Page/龙凤区.md "wikilink"){{.w}}[让胡路区](../Page/让胡路区.md "wikilink"){{.w}}[红岗区](../Page/红岗区.md "wikilink"){{.w}}[大同区](../Page/大同区_\(大庆市\).md "wikilink"){{.w}}[肇州县](../Page/肇州县.md "wikilink"){{.w}}[肇源县](../Page/肇源县.md "wikilink"){{.w}}[林甸县](../Page/林甸县.md "wikilink"){{.w}}[杜尔伯特蒙古族自治县](../Page/杜尔伯特蒙古族自治县.md "wikilink")

|group8 = [伊春市](../Page/伊春市.md "wikilink") |list8 =
[伊春区](../Page/伊春区.md "wikilink"){{.w}}[南岔区](../Page/南岔区.md "wikilink"){{.w}}[友好区](../Page/友好区.md "wikilink"){{.w}}[西林区](../Page/西林区.md "wikilink"){{.w}}[翠峦区](../Page/翠峦区.md "wikilink"){{.w}}[新青区](../Page/新青区.md "wikilink"){{.w}}[美溪区](../Page/美溪区.md "wikilink"){{.w}}[金山屯区](../Page/金山屯区.md "wikilink"){{.w}}[五营区](../Page/五营区.md "wikilink"){{.w}}[乌马河区](../Page/乌马河区.md "wikilink"){{.w}}[汤旺河区](../Page/汤旺河区.md "wikilink"){{.w}}[带岭区](../Page/带岭区.md "wikilink"){{.w}}[乌伊岭区](../Page/乌伊岭区.md "wikilink"){{.w}}[红星区](../Page/红星区.md "wikilink"){{.w}}[上甘岭区](../Page/上甘岭区.md "wikilink"){{.w}}[铁力市](../Page/铁力市.md "wikilink"){{.w}}[嘉荫县](../Page/嘉荫县.md "wikilink")

|group9 = [佳木斯市](../Page/佳木斯市.md "wikilink") |list9 =
[前进区](../Page/前进区.md "wikilink"){{.w}}[向阳区](../Page/向阳区_\(佳木斯市\).md "wikilink"){{.w}}[东风区](../Page/东风区.md "wikilink"){{.w}}[郊区](../Page/郊区_\(佳木斯市\).md "wikilink"){{.w}}[同江市](../Page/同江市.md "wikilink"){{.w}}[富锦市](../Page/富锦市.md "wikilink"){{.w}}[抚远市](../Page/抚远市.md "wikilink"){{.w}}[桦南县](../Page/桦南县.md "wikilink"){{.w}}[桦川县](../Page/桦川县.md "wikilink"){{.w}}[汤原县](../Page/汤原县.md "wikilink")

|group10 = [七台河市](../Page/七台河市.md "wikilink") |list10 =
[桃山区](../Page/桃山区.md "wikilink"){{.w}}[新兴区](../Page/新兴区_\(七台河市\).md "wikilink"){{.w}}[茄子河区](../Page/茄子河区.md "wikilink"){{.w}}[勃利县](../Page/勃利县.md "wikilink")

|group11 = [牡丹江市](../Page/牡丹江市.md "wikilink") |list11 =
[东安区](../Page/东安区_\(牡丹江市\).md "wikilink"){{.w}}[阳明区](../Page/阳明区.md "wikilink"){{.w}}[爱民区](../Page/爱民区.md "wikilink"){{.w}}[西安区](../Page/西安区_\(牡丹江市\).md "wikilink"){{.w}}[绥芬河市](../Page/绥芬河市.md "wikilink"){{.w}}[海林市](../Page/海林市.md "wikilink"){{.w}}[宁安市](../Page/宁安市.md "wikilink"){{.w}}[穆棱市](../Page/穆棱市.md "wikilink"){{.w}}[东宁市](../Page/东宁市.md "wikilink"){{.w}}[林口县](../Page/林口县.md "wikilink")

|group12 = [黑河市](../Page/黑河市.md "wikilink") |list12 =
[爱辉区](../Page/爱辉区.md "wikilink"){{.w}}[北安市](../Page/北安市.md "wikilink"){{.w}}[五大连池市](../Page/五大连池市.md "wikilink"){{.w}}[嫩江县](../Page/嫩江县.md "wikilink"){{.w}}[逊克县](../Page/逊克县.md "wikilink"){{.w}}[孙吴县](../Page/孙吴县.md "wikilink")

|group13 = [绥化市](../Page/绥化市.md "wikilink") |list13 =
[北林区](../Page/北林区.md "wikilink"){{.w}}[安达市](../Page/安达市.md "wikilink"){{.w}}[肇东市](../Page/肇东市.md "wikilink"){{.w}}[海伦市](../Page/海伦市.md "wikilink"){{.w}}[望奎县](../Page/望奎县.md "wikilink"){{.w}}[兰西县](../Page/兰西县.md "wikilink"){{.w}}[青冈县](../Page/青冈县.md "wikilink"){{.w}}[庆安县](../Page/庆安县.md "wikilink"){{.w}}[明水县](../Page/明水县.md "wikilink"){{.w}}[绥棱县](../Page/绥棱县.md "wikilink")
}}

|group4style =text-align: center; |group4 =
[地区](../Page/地区_\(中国行政区划\).md "wikilink") |list4 =
[呼玛县](../Page/呼玛县.md "wikilink"){{.w}}[塔河县](../Page/塔河县.md "wikilink"){{.w}}[加格达奇区](../Page/加格达奇区.md "wikilink")\*{{.w}}[松岭区](../Page/松岭区.md "wikilink")\*{{.w}}[新林区](../Page/新林区.md "wikilink")\*{{.w}}[呼中区](../Page/呼中区.md "wikilink")\*
}} |belowstyle = text-align: left; font-size: 80%; |below =
注1：[哈尔滨市为](../Page/哈尔滨市.md "wikilink")[副省级市](../Page/副省级市.md "wikilink")。
注2：带“\*”属于地方设立的县级行政管理区，并非[中华人民共和国民政部在册的行政区](../Page/中华人民共和国民政部.md "wikilink")。
注3：大兴安岭地区辖下的加格达奇区及松岭区均位于[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[呼伦贝尔市](../Page/呼伦贝尔市.md "wikilink")[鄂伦春自治旗境內](../Page/鄂伦春自治旗.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[黑龙江省乡级以上行政区列表](../Page/黑龙江省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[Category:中华人民共和国二级行政区划导航模板](../Category/中华人民共和国二级行政区划导航模板.md "wikilink")
[\*](../Category/黑龙江行政区划.md "wikilink")
[Category:黑龙江模板](../Category/黑龙江模板.md "wikilink")