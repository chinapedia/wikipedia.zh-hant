[Jing_Wumu.jpg](https://zh.wikipedia.org/wiki/File:Jing_Wumu.jpg "fig:Jing_Wumu.jpg")
**井勿幕**（），乳名**回寅**，名**泉**，字**勿幕**，後以字為名，易字**文淵**。曾用筆名**俠魔**、**大無畏**等。[中國](../Page/中國.md "wikilink")[陝西](../Page/陝西.md "wikilink")[蒲城縣三合鄉人](../Page/蒲城縣.md "wikilink")，祖籍[廣陽鎮](../Page/广阳镇_\(铜川市\).md "wikilink")（今屬[銅川市](../Page/銅川市.md "wikilink")）井家原。中國民主革命家，[中華民國建國初期的政治人物](../Page/中華民國.md "wikilink")，被孫中山譽為革命的「後起之秀」、「西北革命巨柱」。

## 生平

### 家庭及早年

井家祖上由[扶風逃荒到蒲城井家原](../Page/扶風縣.md "wikilink")，原先充人長工，後掘得古墓，以墓中殉葬金銀器物致富。在井家原附近種植槐樹百株，人稱「井百槐」。後又與人合資在[四川](../Page/四川.md "wikilink")[自流井開鹽井](../Page/自流井區.md "wikilink")，更成巨富。

[清](../Page/清.md "wikilink")[道光年間](../Page/道光.md "wikilink")，井勿幕的祖父遷居縣城大什字巷。井勿幕之父**井永汲**，字**綆齋**，因一目失明，人稱「井瞎子」。井永汲樂善好施，[光緒三年](../Page/光緒.md "wikilink")（1877年）西北各省大饑荒，井永汲倡議放賑，蒲城縣令因無法包辦侵吞賑款，便質問：「你有多少錢？敢在蒲城縣放賑！」井永汲回答：「我沒有百萬之富，就不敢幹這活！」從此人稱「井百萬」。井永汲出資數十萬兩白銀，放賑3年，使地方度過荒年，井家家道卻逐漸衰落。

清光緒十四年（1888年）農曆正月初一日（公曆2月12日），井勿幕生於陝西蒲城縣城大什字巷。4歲時父親井永汲逝世，由長他10歲的胞兄[井岳秀撫養長大](../Page/井岳秀.md "wikilink")。每次井家兄弟談論國家大事，井勿幕雖年幼，卻往往能有力地指斥清廷的腐敗無能，有理有據，令兄長井岳秀自嘆不如。井勿幕平時沉默寡言，遇大事策劃，卻可滔滔不絕。自幼景仰[傅介子](../Page/傅介子.md "wikilink")、[班超](../Page/班超.md "wikilink")，時常高歌吟詠，尤其愛唱[漢高祖的](../Page/刘邦.md "wikilink")《[大風歌](../Page/大風歌.md "wikilink")》和荊軻的《[易水歌](../Page/易水歌.md "wikilink")》。

光緒二十八年（1902年），井氏兄弟分家時，井勿幕分得的義源永雜貨鋪破產，無奈之餘，經與[井岳秀商議](../Page/井岳秀.md "wikilink")，赴四川[重慶投靠曾受井永汲周濟的](../Page/重慶.md "wikilink")[川東道道臺](../Page/川東道.md "wikilink")[張鐸](../Page/張鐸_\(清朝\).md "wikilink")，得其助就學於正蒙私塾，結識[楊庶堪](../Page/楊庶堪.md "wikilink")、[朱之洪](../Page/朱之洪.md "wikilink")、[熊克武](../Page/熊克武.md "wikilink")、[但懋辛等人](../Page/但懋辛.md "wikilink")。

### 參加革命

光緒二十九年（1903年）12月，風聞孫中山在海外發展[興中會](../Page/興中會.md "wikilink")，遂不顧張鐸阻攔，東渡[日本](../Page/日本.md "wikilink")，先後在[東京](../Page/東京.md "wikilink")[大成中學及](../Page/大成中學.md "wikilink")[經緯學堂求學](../Page/經緯學堂.md "wikilink")。

光緒三十一年（1905年）8月20日，[同盟會成立](../Page/中國同盟會.md "wikilink")，井勿幕經陝西同鄉[康心孚介紹入會](../Page/康心孚.md "wikilink")，旋自願回國發展組織，被[孫中山委派為同盟會陝西支部長](../Page/孫中山.md "wikilink")。同年冬取道朝鮮回國，在其兄井岳秀協助下，創立中國同盟會陝西支部，在渭北各縣奔走宣傳，祕密發展同盟會員30餘人。

次年（1906年）夏第二次赴日。秋，與[趙世鈺等籌建同盟會東京陝西分會](../Page/趙世鈺.md "wikilink")。此時除負責文字宣傳工作外，也學習製造炸彈，漸為孫中山所器重。同年12月再次回國，聯絡陝西[會黨及](../Page/會黨.md "wikilink")[刀客](../Page/刀客.md "wikilink")（匕首會）以及[河南](../Page/河南.md "wikilink")、[甘肅](../Page/甘肅.md "wikilink")、四川各省同志。

光緒三十三年（1907年）2月，經四川到南方，與[黃興](../Page/黃興.md "wikilink")、[秋瑾等會面商討革命事宜](../Page/秋瑾.md "wikilink")。9月下旬，與[李仲特](../Page/李仲特.md "wikilink")、[高又明等人在](../Page/高又明.md "wikilink")[西安](../Page/西安.md "wikilink")[大雁塔討論貫徹同盟會綱領](../Page/大雁塔.md "wikilink")。10月15日（農曆九月初九日），和李仲特、[焦子靜](../Page/焦子靜.md "wikilink")、[吳虛白等人至](../Page/吳虛白.md "wikilink")[中部縣祭](../Page/黃陵縣.md "wikilink")[黃帝陵](../Page/黃陵.md "wikilink")，當場通過「驅除韃虜，光復故物，掃除專制政體，建立共和國體」的革命綱領。隨後第三度赴日，並向同盟會東京總部負責人[吳玉章匯報國內革命情況](../Page/吳玉章.md "wikilink")。

### 創辦雜誌與領導起義

光緒三十四年（1908年）2月，與其他陝西籍留日學生在東京創辦宣傳革命的《[夏聲](../Page/夏聲.md "wikilink")》雜誌，並用「俠魔」、「大無畏」等筆名發表許多文章。

在《夏聲》第三號上，井勿幕以筆名「俠魔」發表了〈**二十世紀新思潮**〉一文，為迄今所見中國最早介紹馬克思主義的文章。\[1\]

同年秋[蒲案爆發](../Page/蒲案.md "wikilink")，井勿幕在《夏聲》第八期發表〈蒲案感事〉加以揭露，終使蒲城知縣[李體仁革職查辦](../Page/李體仁.md "wikilink")。10月再回國參與領導陝西學生反清運動。

[宣統元年](../Page/宣統.md "wikilink")（1909年）春，[于右任因創辦](../Page/于右任.md "wikilink")《[民呼日報](../Page/民呼日報.md "wikilink")》宣傳革命，被[上海英租界捕房逮捕](../Page/上海公共租界.md "wikilink")，井勿幕立即在《夏聲》為文聲援，喚起海內外注意，于右任最終獲釋。

宣統二年（1910年）春奉令在陝西組織起義，參與促成陝西同盟會和[哥老會聯合](../Page/哥老會.md "wikilink")。同年秋南下[香港](../Page/香港.md "wikilink")，共同籌劃[廣州起義](../Page/廣州起義.md "wikilink")。

宣統三年（1911年）4月廣州起義失敗後，5月間回陝西，與哥老會黨人[張雲山](../Page/張雲山.md "wikilink")、[萬炳南等密謀](../Page/萬炳南.md "wikilink")，準備再次大舉起事。6月，又與[楊叔吉等人赴](../Page/楊叔吉.md "wikilink")[西安滿城觀察敵情](../Page/西安滿城.md "wikilink")。8月，派[鄒子良赴渭北聯絡](../Page/鄒子良.md "wikilink")[刀俠](../Page/刀俠.md "wikilink")，並派[王榮鎮](../Page/王榮鎮.md "wikilink")、[陳得貴分別赴四川](../Page/陳得貴.md "wikilink")、[山西聯絡會黨](../Page/山西.md "wikilink")。9月下旬，收到[張聚庭從南方帶回的同盟會總部命令](../Page/張聚庭.md "wikilink")，確定10月6日（農曆八月十五日）各省同時起義。

同年10月，[武昌起義爆發後](../Page/武昌起義.md "wikilink")，任[陝西軍政府北路安撫招討使](../Page/陝西軍政府.md "wikilink")，率領6萬兵馬搶佔[渭河北部](../Page/渭河.md "wikilink")，與[清軍對抗](../Page/清軍.md "wikilink")，並推舉[張鳳翽為起義總指揮](../Page/張鳳翽.md "wikilink")。

### 民國成立初年

中華民國元年（1912年），被[南京臨時國民政府委任為](../Page/中華民國臨時政府_\(南京\).md "wikilink")[中央稽查局副局長](../Page/中央稽查局.md "wikilink")，以陝西事務糾纏未就職。

不久[袁世凱上臺](../Page/袁世凱.md "wikilink")，同盟會內部出現分化。同年6月23日，張鳳翽、陳樹藩、王錫侯、宋伯魯、郭希仁等人發起成立「共和黨陝西支部」，別立旗幟，倒向袁世凱。

6月25日，同盟會陝西分會改為陝西支部，井勿幕任支部長，張鳳翽任副支部長。

8月25日，同盟會改組為[國民黨](../Page/國民黨_\(宋教仁\).md "wikilink")，不久井勿幕當選為國民黨陝西支部理事長，旋因張鳳翽杯葛，經改選後由張鳳翽任支部長，井勿幕與[馬凌甫共任副支部長](../Page/馬凌甫.md "wikilink")。陝西國民黨面臨分裂局面，井勿幕等人遂倡議「解甲歸田」，與[曹印侯](../Page/曹印侯.md "wikilink")、[宋向宸](../Page/宋向宸.md "wikilink")、[胡景翼等人以](../Page/胡景翼.md "wikilink")「留學」為名，紛紛出國或暫避他省。

同年冬，國民黨陝西支部分裂為河北派（亦稱渭北派，指渭河以北）和咸寧派（亦稱咸長派，指[咸寧](../Page/咸寧縣_\(金朝\).md "wikilink")、[長安](../Page/長安縣.md "wikilink")）。陝西民主革命陷入低潮。

民國二年（1913年）參加[二次革命](../Page/二次革命.md "wikilink")，討伐袁世凱，失敗後避居日本。

民國四年（1915年）再赴雲南參加[護國戰爭](../Page/護國戰爭.md "wikilink")，並任熊克武部參謀長及敵前總指揮，轉戰於四川[瀘州](../Page/瀘州.md "wikilink")、徐府（[宜賓](../Page/宜賓.md "wikilink")）一帶。後為策應陝西護國運動，喬裝成商販徒步回陝。

民國五年（1916年）袁世凱死後，加入[皖系的](../Page/皖系.md "wikilink")[陳樹藩任陝西督軍兼省長](../Page/陳樹藩.md "wikilink")。井勿幕對此深表不滿，便前往[北京聯絡革命志士進行反陳樹藩的鬥爭](../Page/北京.md "wikilink")。

### 護法運動與身亡

民國六年（1917年）3月出任[關中道尹](../Page/關中道.md "wikilink")，同年8月離職，閒居西安。

12月，陝西的國民黨革命派胡景翼、[曹世英等人響應孫中山號召](../Page/曹世英.md "wikilink")，加入[護法戰爭](../Page/護法戰爭.md "wikilink")，次年（1918年）1月成立[陝西靖國軍](../Page/陝西靖國軍.md "wikilink")，胡景翼任右翼總司令，曹世英為任左翼總司令。8月，于右任就任陝西靖國軍總司令，將靖國軍整編為六路，分道進攻陳樹藩。

陳樹藩請井勿幕作調解人前往[三原的靖國軍總部](../Page/三原縣.md "wikilink")，企圖借其聲望分化、瓦解靖國軍。然而井勿幕一到三原，便被推舉為靖國軍總指揮，反使靖國軍士氣大振，陳樹藩大為惱怒。

同年10月，[雲南靖國軍第八軍軍長](../Page/雲南靖國軍.md "wikilink")[葉荃率部援陝](../Page/葉荃.md "wikilink")，到達陝西靖國軍第一路軍[郭堅駐防的](../Page/郭堅.md "wikilink")[鳳翔](../Page/鳳翔縣.md "wikilink")。11月下旬，于右任派井勿幕前往鳳翔慰勞葉部，並會商共同作戰方略。陳樹藩大為驚恐，利用擔任郭堅部參謀長的馬凌甫與井勿幕有隙，密令潛入郭堅部的內奸[李棟材](../Page/李棟材.md "wikilink")（時任營長）聯絡馬凌甫謀殺井勿幕。

井勿幕由鳳翔回三原途中經過[興平時](../Page/興平縣.md "wikilink")，接到李棟材偽造的郭堅來信，約於農曆十一月廿一日（公曆12月23日）赴興平[南仁堡參加軍事會議](../Page/南仁堡.md "wikilink")。12月23日，井勿幕僅帶同4名護兵赴約，被李棟材部下差弁李新生從背後連開兩槍，當即死亡，年僅31歲。井勿幕的首級隨即被李棟材割下，帶往西安，獻給陳樹藩。

井勿幕的屍身被同來的護兵安彥明以棉被包裹，揹回[涇陽](../Page/涇陽縣.md "wikilink")。涇陽駐軍團長[田玉潔向陳樹藩幾次交涉](../Page/田玉潔.md "wikilink")，索回井勿幕被[梟首的頭顱](../Page/梟首.md "wikilink")，連同屍身草葬於蒲城[紫荊原南](../Page/紫荊原.md "wikilink")。

## 紀念與追贈

井勿幕死後，于右任致函[國會眾議院](../Page/中華民國初年國會.md "wikilink")：「天乎何心，壞我長城……惟有誓滅國賊，慰我先烈。」並以陝西靖國軍總司令名義，呈請廣州[護法軍政府以陸軍中將陣亡例追恤](../Page/護法軍政府.md "wikilink")。然而當時軍閥割據，未幾護法運動又告失敗，無暇顧及此事。

民國十五年（1926年）[馮玉祥](../Page/馮玉祥.md "wikilink")[五原誓師後](../Page/五原誓師.md "wikilink")，陝西成為[國民黨勢力範圍](../Page/中國國民黨.md "wikilink")。

民國十八年（1929年）由國民黨員焦子靜、[李桐軒](../Page/李桐軒.md "wikilink")、[劉治洲](../Page/劉治洲.md "wikilink")、[景定成](../Page/景定成.md "wikilink")、[王子中](../Page/王子中.md "wikilink")、[范紫東](../Page/范紫東.md "wikilink")、[蒙俊僧](../Page/蒙俊僧.md "wikilink")、[孫蔚如等](../Page/孫蔚如.md "wikilink")34人樹碑，陝西省政府主席[宋哲元撰文](../Page/宋哲元.md "wikilink")，[毛昌傑書](../Page/毛昌傑.md "wikilink")《井先生紀念碑》，立於西安市[革命公園內](../Page/革命公園.md "wikilink")。

民國三十二年（1943年）8月14日，[國民政府明令褒揚井勿幕並立傳](../Page/國民政府.md "wikilink")。

民國三十四年（1945年）11月19日，[國民政府追贈井勿幕為](../Page/國民政府.md "wikilink")[國民革命軍陸軍上將銜](../Page/國民革命軍.md "wikilink")。同年12月23日（井勿幕遇害27週年），時任監察院長的于右任赴陝西親自主持，將井勿幕靈柩由蒲城迎至西安革命公園內，舉行公祭，同時在三原舉行「三原各界公祭井勿幕先生紀念大會」。會後移靈西安南郊[少陵原](../Page/少陵原.md "wikilink")（今長安區[清涼山公園內](../Page/清涼山公園_\(長安\).md "wikilink")），撥款購地12畝，建立陵園，舉行隆重的安葬儀式，樹立墓碑，並在路口立[蔣中正題](../Page/蔣中正.md "wikilink")「追贈陸軍上將銜井勿幕先生之墓」牌坊。

民國35年2月（1946年），為永久紀念井勿幕，將其故居西安盧進士巷（今蘆盪巷）附近的南北四府街暨琉璃廟街改名「井上將路」，南四府街南端開于民國28年（1939年）的城門正式命名「井上將門」，民國36年（1947年），改稱「勿幕門」，原井上將街亦改名「勿幕街」。\[2\]\[3\]今勿幕街復名「四府街」，勿幕門如故。

[文革期間](../Page/文化大革命.md "wikilink")，井勿幕墓被搗毀。1981年，為紀念辛亥革命70週年，井勿幕墓得到修整恢復。

## 附註

## 參考文獻

  - 胡健國，國史館現藏民國人物傳記史料彙編，臺北：國史館，2001年。
  - 胡景通、嚴佐民
    ，[井勿幕傳略](https://web.archive.org/web/20110504034544/http://www.sanqinji.com/rw/jingwumu/1.htm)，載《中華文史資料文庫·第九卷》（20-9），北京：中國文史出版社，1996年。

[Category:中国同盟会会员](../Category/中国同盟会会员.md "wikilink")
[Category:辛亥革命人物](../Category/辛亥革命人物.md "wikilink")
[J井](../Category/蒲城人.md "wikilink")
[Wu勿幕](../Category/井姓.md "wikilink")
[Category:陕西靖国军将领](../Category/陕西靖国军将领.md "wikilink")

1.  [李子遲、王傅雷，我國最早宣傳馬克思主義的人──井勿幕，中華井氏網，2011-10-02](http://www.chinajing.cn/jnews/2011/10/0218295829.html)
2.
3.