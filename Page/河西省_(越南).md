**河西省**（）是[越南](../Page/越南.md "wikilink")1965年至2008年间的一個省，位於[紅河三角洲上](../Page/紅河三角洲.md "wikilink")。下辖[河東市](../Page/河東市.md "wikilink")、[山西市](../Page/山西市.md "wikilink")2个省辖市和12个縣。

2008年5月29日，[越南国会決定于](../Page/越南国会.md "wikilink")2008年8月1日將河西省并入[河內市](../Page/河內市.md "wikilink")\[1\]。

## 地理

河西省位於紅河三角洲上，與[河內](../Page/河內.md "wikilink")、[海陽](../Page/海陽省.md "wikilink")、[河南省](../Page/河南省_\(越南\).md "wikilink")、[富壽省](../Page/富壽省.md "wikilink")、[和平省相鄰](../Page/和平省.md "wikilink")。此省[越族的人口佔](../Page/越族.md "wikilink")99%，其他為[芒](../Page/芒族.md "wikilink")、[傜等少數民族](../Page/傜族.md "wikilink")。

河西省屬於[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")，受地形因素又細分成三種氣候：平原因海風影響，既熱又潮濕；丘陵區則受西風影響，為[溫帶大陸性氣候](../Page/溫帶大陸性氣候.md "wikilink")；巴位山區為溫帶氣候，全年平均溫18°C。

## 历史

河西省原为[河东省和](../Page/河东省.md "wikilink")[山西省两省](../Page/山西省_\(越南\).md "wikilink")。1965年4月21日，[越南国会常务委员会通过](../Page/越南国会.md "wikilink")103-NQ-TVQH号决议，合并河东省和山西省为河西省\[2\]。7月1日，河西省正式成立。

河西省共辖2市社14县：[山西市社](../Page/山西市社.md "wikilink")、[河东市社](../Page/河东市社.md "wikilink")、[不拔县](../Page/不拔县.md "wikilink")、[从善县](../Page/从善县.md "wikilink")、[石室县](../Page/石室县.md "wikilink")、[国威县](../Page/国威县.md "wikilink")、[福寿县](../Page/福寿县.md "wikilink")、[广威县](../Page/广威县.md "wikilink")（原山西省6县）；[彰美县](../Page/彰美县.md "wikilink")、[丹凤县](../Page/丹凤县.md "wikilink")、[怀德县](../Page/怀德县.md "wikilink")、[美德县](../Page/美德县.md "wikilink")、[富川县](../Page/富川县.md "wikilink")、[青威县](../Page/青威县.md "wikilink")、[常信县和](../Page/常信县.md "wikilink")[应和县](../Page/应和县.md "wikilink")（原河东省8县）。

1968年7月26日，合并广威县、从善县和不拔县为[巴位县](../Page/巴位县.md "wikilink")\[3\]。

1975年12月27日，河西省与和平省合并为[河山平省](../Page/河山平省.md "wikilink")\[4\]。

1978年12月29日，河东市社、山西市社、巴位县、福寿县、石室县、丹凤县、怀德县2市社5县和国威县、彰美县、青威县、常信县的部分社划归[河内市](../Page/河内市.md "wikilink")\[5\]。因河东市社为河山平省省莅，实际并未划归河内市。

1991年8月12日，[河山平省撤销](../Page/河山平省.md "wikilink")，恢复为河西省和和平省，[河内市下辖山西市社](../Page/河内市.md "wikilink")、巴位县、福寿县、石室县、丹凤县、怀德县1市社5县重新划归河西省\[6\]。河西省下辖2市社12县。

2006年12月27日，[河东市社改制为](../Page/河东市社.md "wikilink")[河东市](../Page/河东市.md "wikilink")\[7\]。

2007年8月2日，[山西市社改制为](../Page/山西市社.md "wikilink")[山西市](../Page/山西市.md "wikilink")\[8\]。

2008年5月29日，越南国会通过决议，自2008年8月1日起，河西省整体并入河内市\[9\]。

## 行政區劃

2008年，河西省下轄2市12縣。

  - [河東市](../Page/河東市.md "wikilink")（）
  - [山西市](../Page/山西市.md "wikilink")（）
  - [巴位縣](../Page/巴位縣.md "wikilink")（）
  - [彰美縣](../Page/彰美縣.md "wikilink")（）
  - [丹鳳縣](../Page/丹鳳縣_\(越南\).md "wikilink")（）
  - [懷德縣](../Page/懷德縣.md "wikilink")（）
  - [美德縣](../Page/美德縣.md "wikilink")（）
  - [富川縣](../Page/富川縣_\(越南\).md "wikilink")（）
  - [福壽縣](../Page/福壽縣.md "wikilink")（）
  - [國威縣](../Page/國威縣.md "wikilink")（）
  - [石室縣](../Page/石室縣.md "wikilink")（）
  - [青威縣](../Page/青威縣.md "wikilink")（）
  - [常信縣](../Page/常信縣.md "wikilink")（）
  - [應和縣](../Page/應和縣.md "wikilink")（）

## 旅游

河西省位於首都河內的西南方並為其門戶，著名景點有柴寺（）、香寺（）、香迹寺（）、西方寺（）以及數以百計的窯洞。多數節慶和活動集中於[元旦之後的三個月內](../Page/元旦.md "wikilink")，最有名的是香寺的朝聖之旅及西方寺、柴寺的祭祀活動。

## 腳注

## 外部連結

  - [河西省人民委员会](https://web.archive.org/web/20050614082751/http://www.hatay.gov.vn/)

[Category:越南舊省](../Category/越南舊省.md "wikilink")
[Category:越南当代行政区划史](../Category/越南当代行政区划史.md "wikilink")

1.  [14/2008/QH12：河内市扩大地界](https://thuvienphapluat.vn/van-ban/Bat-dong-san/Nghi-quyet-14-2008-QH12-dieu-chinh-dia-gioi-hanh-chinh-giua-tinh-Ha-Tay-va-tinh-Phu-Tho-giua-tinh-Binh-Phuoc-va-tinh-Dong-Nai-68075.aspx)

2.  [103-NQ-TVQH：河东省和山西省合并为河西省](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Quyet-dinh-103-NQ-TVQH-phe-chuan-thanh-lap-tinh-Bac-Thai-Nam-Ha-Ha-Tay-sap-nhap-xa-An-Hoa-Thach-That-Son-Tay-vao-xa-Tien-xuan-Luong-Son-Hoa-Binh-17889.aspx)

3.  [120-CP：广威县、不拔县和从善县合并为巴位县](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Quyet-dinh-120-CP-hop-nhat-cac-huyen-Quang-Oai-Bat-Bat-va-Tung-Thien-thuoc-tinh-Ha-Tay-thanh-huyen-Ba-Vi-18342.aspx)

4.  [国会决议：河西省和和平省合并为河山平省](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-hop-nhat-mot-so-tinh/42732/noi-dung.aspx)

5.  [国会决议：扩大河内市地界](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-phe-chuan-viec-phan-vach-lai-dia-gioi-thanh-pho-Ha-Noi-TPHCM-cac-tinh-Ha-Son-Binh-Vinh-Phu-Cao-Lang-Bac-Thai-Quang-Ninh-va-Dong-Nai/42744/noi-dung.aspx)

6.  [国会决议：河山平省分设为河西省和和平省](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-quyet-dieu-chinh-dia-gioi-hanh-chinh-mot-so-tinh-thanh-pho-truc-thuoc-Trung-uong-42808.aspx)

7.  [155/2006/NĐ-CP：河东市社改制为河东市](https://thuvienphapluat.vn/van-ban/Bat-dong-san/Nghi-dinh-155-2006-ND-CP-thanh-lap-thanh-pho-Ha-Dong-thuoc-tinh-Ha-Tay-16103.aspx)

8.  [130/2007/NĐ-CP：山西市社改制为山西市](https://thuvienphapluat.vn/van-ban/Bo-may-hanh-chinh/Nghi-dinh-130-2007-ND-CP-thanh-lap-thanh-pho-Son-Tay-thuoc-tinh-Ha-Tay-54146.aspx)

9.