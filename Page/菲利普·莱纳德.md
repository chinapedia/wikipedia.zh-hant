[Phillipp_Lenard_in_1900.jpg](https://zh.wikipedia.org/wiki/File:Phillipp_Lenard_in_1900.jpg "fig:Phillipp_Lenard_in_1900.jpg")
**菲利普·冯·莱纳德**（，），[德国](../Page/德国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，1905年[诺贝尔物理学奖获得者](../Page/诺贝尔物理学奖.md "wikilink")。

莱纳德在研究[阴极射线时曾获得卓越成果](../Page/阴极射线.md "wikilink")，为此获得诺贝尔奖；他用实验发现了[光电效应的重要规律](../Page/光电效应.md "wikilink")；他也提出过一种[原子结构设想](../Page/原子.md "wikilink")。

## 生平

菲利普·莱纳德于1862年6月7日出生在[匈牙利的](../Page/匈牙利.md "wikilink")[普雷斯堡](../Page/普雷斯堡.md "wikilink")（现[斯洛伐克](../Page/斯洛伐克.md "wikilink")[布拉迪斯拉发](../Page/布拉迪斯拉发.md "wikilink")），父母来自[奥地利的](../Page/奥地利.md "wikilink")[蒂罗尔](../Page/蒂罗尔.md "wikilink")。他先后在[布达佩斯大学](../Page/布达佩斯大学.md "wikilink")、[维也纳大学](../Page/维也纳大学.md "wikilink")、[柏林大学和](../Page/柏林大学.md "wikilink")[海德堡大学学习物理学](../Page/海德堡大学.md "wikilink")，曾是[罗伯特·威廉·本生](../Page/罗伯特·威廉·本生.md "wikilink")、[亥姆霍兹](../Page/赫尔曼·冯·亥姆霍兹.md "wikilink")、[莱奥·柯尼希斯贝格尔](../Page/莱奥·柯尼希斯贝格尔.md "wikilink")（Leo
Königsberger）和[格奥尔格·赫尔曼·昆克](../Page/格奥尔格·赫尔曼·昆克.md "wikilink")（Georg
Hermann Quincke）的学生，1886年在海德堡大学获得博士头衔。

1892年起任[波恩大学的讲师和](../Page/波恩大学.md "wikilink")[海因里希·鲁道夫·赫兹教授的助手](../Page/海因里希·鲁道夫·赫兹.md "wikilink")，1894年被任命为[布雷斯劳大学的教授](../Page/布雷斯劳大学.md "wikilink")，1895年成为[亚琛工业大学的物理学教授](../Page/亚琛工业大学.md "wikilink")，1896年海德堡大学[理论物理学教授](../Page/理论物理学.md "wikilink")，1898年为[基尔大学教授](../Page/基尔大学.md "wikilink")。

莱纳德在[希特勒上台后加入了](../Page/希特勒.md "wikilink")[纳粹党籍](../Page/纳粹党.md "wikilink")，在多次在公开场合批判[犹太人科学家](../Page/犹太人.md "wikilink")[爱因斯坦](../Page/爱因斯坦.md "wikilink")，宣扬希特勒的理论，推崇“雅利安物理学”。1945年二战结束后，盟军考虑到莱纳德年事已高，免除了对他的[非纳粹化措施](../Page/非纳粹化.md "wikilink")，但取消了他的海德堡大学终身教授名誉。莱纳德于1947年在德国[梅塞尔豪森去世](../Page/梅塞尔豪森.md "wikilink")，他的遗产现存于[慕尼黑的](../Page/慕尼黑.md "wikilink")[德意志博物馆](../Page/德意志博物馆.md "wikilink")。

## 研究

### 早期研究

莱纳德的第一项研究成果是在[力学领域](../Page/力学.md "wikilink")，他发表了一篇关于坠落水滴的振动及相关问题的论文，1894年整理出版了赫兹的遗作《力学原理》。

莱纳德很快对[磷光和](../Page/磷光.md "wikilink")[荧光现象发生了兴趣](../Page/荧光.md "wikilink")，这种在黑暗中发出的神秘的微弱亮光，从小就吸引了莱纳德，他曾和他的同学加热氟晶体让它发出荧光。莱纳德与天文学家沃尔夫（W.
Wolf）合作开始研究当[焦棓酚与](../Page/焦棓酚.md "wikilink")[碱和](../Page/碱.md "wikilink")[硫磺酸盐混合时的发光现象](../Page/硫磺酸盐.md "wikilink")，以发展照相技术，他们发现[发光的](../Page/发光.md "wikilink")[光度取决于焦棓酚的](../Page/光度.md "wikilink")[氧化程度](../Page/氧化.md "wikilink")。与此同时，莱纳德完成了对[铋的](../Page/铋.md "wikilink")[磁性的研究](../Page/磁性.md "wikilink")，并和克拉特（V.
Klatt）共同发表了关于[硫化钙等自发光物质的研究成果](../Page/硫化钙.md "wikilink")，他们发现硫化钙在接受事先的光照后会在黑暗中发光，但必须在一个条件下，即硫化钙中至少包含少量[重金属](../Page/重金属.md "wikilink")，如[铜和](../Page/铜.md "wikilink")[铋](../Page/铋.md "wikilink")，以形成[晶体](../Page/晶体.md "wikilink")，晶体决定了发光的颜色、强度和持续时间；纯的硫化钙是不会发光的。这项成果开始了莱纳德接下来长达18年阴极射线的研究。

### 阴极射线

1888年莱纳德在海德堡大学昆克手下工作，他完成了他关于[阴极射线的第一项研究成果](../Page/阴极射线.md "wikilink")，他研究了当时赫兹关于阴极射线有与[紫外线相似特性的观点](../Page/紫外线.md "wikilink")，并设计了一个实验，以探究阴极射线是否像紫外线一样，能够通过[放电管壁上的](../Page/放电管.md "wikilink")[石英窗](../Page/石英窗.md "wikilink")，他获得的结果是阴极射线没有这样的特性。但是后来，1892年他在波恩大学做赫兹的助手时，赫兹让他观察了他的新发现，赫兹将一片盖上[铝箔的含](../Page/铝.md "wikilink")[铀玻璃片放入放电管](../Page/铀.md "wikilink")，当用阴极射线轰击铝箔时，铝箔下面发出了亮光。赫兹因此建议可以用铝薄板将放电管内的空间一分为二，在空间的一部分内，阴极射线由常规的方法产生，而在空间的另一部分，可以在[真空的条件下观察阴极射线](../Page/真空.md "wikilink")。赫兹由于过于忙碌，便授权莱纳德做这个实验，他后来因此获得了“[莱纳德窗](../Page/莱纳德窗.md "wikilink")”的重要发现。

在尝试了不同厚度的铝箔后，莱纳德终于在1894年发表了他的重要发现，用于封闭放电管的石英板可以用铝箔代替，铝箔的厚度恰好能够保持放电管内的真空状态，但又必须足够薄以让阴极射线能够通过这样，这样不但能研究阴极射线，也能研究阴极射线在放电管外引起的荧光现象。莱纳德从实验得出结论，阴极射线在空气中只能传播分米级的距离，而在真空中却可以传播数米而不会衰减。在赫兹1892年宣称阴极射线不可能是[粒子](../Page/粒子.md "wikilink")，而只能是一种[以太波的观点后](../Page/以太波.md "wikilink")，莱纳德曾表示赞同，但是后来在[让·佩兰](../Page/让·佩兰.md "wikilink")（1895年）、[约瑟夫·汤姆孙](../Page/约瑟夫·汤姆孙.md "wikilink")（1897年）和[威廉·维恩](../Page/威廉·维恩.md "wikilink")（1897年）的研究成果证明了阴极射线的粒子特性后，莱纳德放弃了这一观点。汤姆孙最后作出了阴极射线是由带负电的电子组成的结论。

### 光电效应

此后，莱纳德又继续拓展赫兹关于[光电效应的研究](../Page/光电效应.md "wikilink")，他分析了在高真空环境下光电效应的特性和本质，证明了当紫外线照射在金属上时，会使电子从金属表面逸出，并在真空中传播，电子在电场中被加速或减速，电子轨迹在磁场中改变。通过精确的实验，他证明发射的电子数量正比于入射光所带的能量，而电子的速度，或者说它们的[动能](../Page/动能.md "wikilink")，却与入射光能量无关，当入射光的波长减小时，电子速度增大。这个事实与当时的理论是相冲突的，[经典物理学无法解释莱纳德的光电效应实验结果](../Page/经典物理学.md "wikilink")。直到1905年[爱因斯坦发表](../Page/爱因斯坦.md "wikilink")[相对论和](../Page/相对论.md "wikilink")[光量子理论](../Page/光量子理论.md "wikilink")，才解释了这一现象，后来又被[罗伯特·密立根所证实](../Page/罗伯特·密立根.md "wikilink")，因为人们把爱因斯坦的名字冠在光量子理论上，莱纳德对爱因斯坦一直耿耿于怀。

在研究过程中，莱纳德还发明了一种[光电管](../Page/光电管.md "wikilink")，以加速电子和测量它们的能量，这种光电管是[三极管最初的雏形](../Page/三极管.md "wikilink")，不同之处在于，在莱纳德的光电管中，电子是由阴极光发射的，而三极管中的阴极是白炽丝，可以向真空发射更高强度的电流。

莱纳德在1902年提出，当[电子通过一种气体时](../Page/电子.md "wikilink")，必须具有一个确定的最小能量，才能产生气体的[电离](../Page/电离.md "wikilink")。

### 动力子原子模型

1903年莱纳德提出了一种原子结构模型的设想，他称之为“[动力子](../Page/动力子.md "wikilink")”（英语：dynamides），它们体积很小分散在广阔的空间中，它们有[质量](../Page/质量.md "wikilink")，由许多[电偶对](../Page/电偶.md "wikilink")（两个带电量相同、带电符号相反的电子相连）组成，它们的数量等于原子的质量，他认为原子中的固体部分只占整个原子体积的十亿分之一，动力子原子模型能解释莱纳德窗的作用，却无法解释更多的事实，由此是一种不成功的原子模型。但莱纳德的研究为[亨德里克·洛伦兹的电子理论还是贡献良多](../Page/亨德里克·洛伦兹.md "wikilink")。

### 后期研究

后期，莱纳德又研究了[光谱线的本质](../Page/光谱线.md "wikilink")，发展了[约翰尼斯·里德伯](../Page/约翰尼斯·里德伯.md "wikilink")、[海因里希·凯泽](../Page/海因里希·凯泽.md "wikilink")（Heinrich
Kayser）和[卡尔·伦格](../Page/卡尔·伦格.md "wikilink")（Carl
Runge）的研究成果，他们提出金属的光谱线可以分为两类或更多类[连续光谱](../Page/连续光谱.md "wikilink")（[谱系](../Page/谱系.md "wikilink")），并且这些谱系的波长之间存在明显的数学关系。莱纳德认为，每个谱系都会存在原子的确定变化，这些变化决定了各个谱系，并且可以按原子失去的电子的数目来区分。

莱纳德是一个天才的[实验物理学家](../Page/實驗物理學.md "wikilink")，他有许多重要的发现，但他宣布这些发现的重要性时却超过了它们的真正价值，不断和别的科学家发生冲突。虽然他获得了众多荣誉，比如[奥斯陆大学](../Page/奥斯陆大学.md "wikilink")（1911年）、[德累斯顿大学](../Page/德累斯顿大学.md "wikilink")（1922年）和[布拉迪斯拉发大学](../Page/布拉迪斯拉发大学.md "wikilink")（1942年）的荣誉博士学位、[富兰克林奖章](../Page/富兰克林奖章.md "wikilink")（1905年）、德意志帝国的鹰盾勋章（1933年），并被选为海德堡的荣誉市民（1933年），他却仍感到自己没有受到足够高的评价，因此会在许多国家攻击其他物理学家。

莱纳德从[反犹太人的](../Page/反犹太.md "wikilink")[种族主义立场出发](../Page/种族主义.md "wikilink")，从1920年起在多次在公开场合批判[犹太人科学家爱因斯坦](../Page/犹太人.md "wikilink")，并鼓吹所谓的“[德意志物理学](../Page/德意志物理学.md "wikilink")”。[希特勒上台后](../Page/希特勒.md "wikilink")，莱纳德加入了[纳粹党籍](../Page/纳粹党.md "wikilink")，成为希特勒无比忠实的科学顾问，宣扬希特勒的[种族主义和排犹主义理论](../Page/种族主义.md "wikilink")。而作为回报，纳粹党将莱纳德作为[雅利安或德国物理学的领袖](../Page/雅利安人種.md "wikilink")，纳粹在物理学界的代理人。

莱纳德的著作包括：《关于以太和材料》（1911年第二版）、《阴极射线的定量分析》（1918年）、《关于相对论》（1918年）和《伟大的自然科学研究者》（1930年第二版）等。

## 参考资料

  - [诺贝尔奖官方网站关于菲利普·莱纳德生平介绍](http://nobelprize.org/nobel_prizes/physics/laureates/1905/lenard-bio.html)
  - Beyerchen, Alan, *Scientists under Hitler: Politics and the physics
    community in the Third Reich* (New Haven, CT: Yale University Press,
    1977).
  - Hentschel, Klaus, ed. *Physics and National Socialism: An anthology
    of primary sources* (Basel: Birkhaeuser, 1996).
  - Walker, Mark, *Nazi science: Myth, truth, and the German atomic
    bomb* (New York: Harper Collins, 1995).
  - Wolff, Stephan L., "Physicists in the 'Krieg der Geister': Wilhelm
    Wien's 'Proclamation'", *Historical Studies in the Physical and
    Biological Sciences* Vol. 33, No. 2 (2003): 337-368.

[Category:德国物理学家](../Category/德国物理学家.md "wikilink")
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:納粹德國人物](../Category/納粹德國人物.md "wikilink")
[Category:基爾大學教師](../Category/基爾大學教師.md "wikilink")
[Category:海德堡大學教師](../Category/海德堡大學教師.md "wikilink")
[Category:阿亨工業大學教師](../Category/阿亨工業大學教師.md "wikilink")
[Category:布雷斯勞大學教師](../Category/布雷斯勞大學教師.md "wikilink")
[Category:波恩大學教師](../Category/波恩大學教師.md "wikilink")
[Category:海德堡大學校友](../Category/海德堡大學校友.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:維也納大學校友](../Category/維也納大學校友.md "wikilink")
[Category:匈牙利德國人](../Category/匈牙利德國人.md "wikilink")
[Category:马泰乌奇奖章获得者](../Category/马泰乌奇奖章获得者.md "wikilink")
[Category:拉姆福德奖章获得者](../Category/拉姆福德奖章获得者.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")