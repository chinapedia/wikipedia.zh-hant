**環境科技**（,
又稱**綠色科技**）指的是[環境科學的應用](../Page/環境科學.md "wikilink")，旨在維護[自然環境與資源以及減低人類活動帶來的負面影響](../Page/自然環境.md "wikilink")。[永續發展是環境科技的核心目標](../Page/永續發展.md "wikilink")，在解決環境問題時，其解決方法必須符合社會公平、經濟允許和環境健全的準則。依據地球高峰會議討論，應具備五項特質：一.對環境友善的包裝；二.無汙染的行銷管道；三.減廢、回收、再利用；四.能源效率；五.汙染及安全偵測、副產品與排放之控制。

## 实例

  - [生物反应器](../Page/生物反应器.md "wikilink")
  - [生物修復](../Page/生物修復.md "wikilink")
  - [海水淡化](../Page/海水淡化.md "wikilink")
  - [節約能源](../Page/節約能源.md "wikilink")
  - [電動載具](../Page/電動載具.md "wikilink")
  - [波浪能](../Page/波浪能.md "wikilink")
  - [绿色计算](../Page/绿色计算.md "wikilink")
  - [水力發電](../Page/水力發電.md "wikilink")
  - [風能](../Page/風能.md "wikilink")
  - [風力發動機](../Page/風力發動機.md "wikilink")
  - [燃料电池](../Page/燃料电池.md "wikilink")
  - [海水溫差發電](../Page/海水溫差發電.md "wikilink")
  - [太陽能發電](../Page/太陽能發電.md "wikilink")
  - [太阳能光伏](../Page/太阳能光伏.md "wikilink")
  - [堆肥式廁所](../Page/堆肥式廁所.md "wikilink")
  - [裂解](../Page/裂解.md "wikilink")

## 相關科技

追求永續發展的環境科技包括：[回收](../Page/回收.md "wikilink")、[水淨化](../Page/水淨化.md "wikilink")、[污水處理](../Page/污水處理.md "wikilink")、(Environmental
remediation)、[煙氣處理](../Page/煙氣.md "wikilink")、[污染物排放控制技術和](../Page/污染物排放控制技術.md "wikilink")[可再生能源](../Page/可再生能源.md "wikilink")。其中某些科技助於[節約能源](../Page/節約能源.md "wikilink")，其他則助於減低人類活動所造成的廢棄物。如[太陽能和](../Page/太陽能.md "wikilink")[风能等的](../Page/风能.md "wikilink")[可再生能源比其他如](../Page/可再生能源.md "wikilink")[煤和](../Page/煤.md "wikilink")[石油等化石燃料能源更能減少對環境的衝擊](../Page/石油.md "wikilink")。

目前，許多[科學家正在研究找尋其他替代性的](../Page/科學家.md "wikilink")[清潔能源](../Page/清潔能源.md "wikilink")，譬如[厭氧消化](../Page/厭氧消化.md "wikilink")(anaerobic
digestion)就是從廢棄物中生產可更新能源。為減低全球[溫室氣體的排放量](../Page/溫室氣體.md "wikilink")，工業產業必須採用能保存能源的科技以及生產清潔能源，如無鉛[汽油](../Page/汽油.md "wikilink")、[太陽能和](../Page/太陽能.md "wikilink")[混合動力車輛](../Page/混合動力車輛.md "wikilink")。

## 範例

  - [可替代能源](../Page/可替代能源.md "wikilink")
  - [可再生能源](../Page/可再生能源.md "wikilink")
  - [綠色建築](../Page/綠色建築.md "wikilink")
  - [綠色無政府主義](../Page/綠色無政府主義.md "wikilink")(Green anarchism)
  - [混合動力車輛](../Page/混合動力車輛.md "wikilink")
  - [太陽能](../Page/太陽能.md "wikilink")
  - [永續設計](../Page/永續設計.md "wikilink")
  - [可持续能源](../Page/可持续能源.md "wikilink")
  - [绿色生活](../Page/绿色生活.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[Category:環境保護](../Category/環境保護.md "wikilink")
[Category:可持续技术](../Category/可持续技术.md "wikilink")