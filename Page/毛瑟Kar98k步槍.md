[Bolt517.jpg](https://zh.wikipedia.org/wiki/File:Bolt517.jpg "fig:Bolt517.jpg")
**毛瑟1898年型卡賓槍**（，簡稱**Kar98k**或**K98k**），由[Gew
98步枪改進演变而成](../Page/Gew_98步槍.md "wikilink")，是[第二次世界大戰時期](../Page/第二次世界大戰.md "wikilink")，[納粹德國軍隊裝備的制式](../Page/納粹德國.md "wikilink")[手動步槍](../Page/手動步槍.md "wikilink")。

## 原型

1898年，毛瑟槍廠研製的7.92毫米口徑[毛瑟M1898步槍成為](../Page/毛瑟M1898步槍.md "wikilink")[德國](../Page/德國.md "wikilink")[陸軍制式步槍](../Page/陸軍.md "wikilink")，德國陸軍命名為Gewehr
98（簡稱：G98）\[1\]。從此開始了「毛瑟98系列步槍」近50年的時間裡作為[德軍制式裝備的歷史](../Page/德軍.md "wikilink")。在[第一次和](../Page/第一次世界大戰.md "wikilink")[第二次世界大戰中被配發給大部分德國](../Page/第二次世界大戰.md "wikilink")[步兵](../Page/步兵.md "wikilink")，在兩次大戰中證明了它的高可靠性，亦成為槍械歷史上的經典。世界各國仿造的更是不計其數，大部分手動步槍幾乎都是根據它的閉鎖機構設計改良而成。

## 演變

在兩次世界大戰期間Gew
98步槍進行了多次改良，还包括在[比利時](../Page/比利時.md "wikilink")、[捷克斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")（例如捷克Vz.24步枪）等國家特許生產長度縮短的多種變型槍。1924年毛瑟公司推出了一種「標準型步槍」，在Gew98步槍的基礎上將槍管縮短為600毫米，全槍長度由1.25米縮短為1.11米，採用新的瞄準具。这种枪最初称为民用型，主要是为绕过当时凡尔赛条约对德国生产武器进行的约束。德国军方对标准型步枪进行了射击测试。事實上這種毛瑟标准型步枪採購數量有限，鮮為人知。[中華民國於](../Page/中華民國.md "wikilink")1935年以標準型步槍為基礎，製造了[中正式步槍](../Page/中正式步槍.md "wikilink")（中正式步槍是國民政府部隊装备的第一種制式步槍，並成為[抗日戰爭時期](../Page/抗日戰爭.md "wikilink")[國軍装备的重要步槍](../Page/國軍.md "wikilink")）。後來經過改進的毛瑟标准型步枪被德國郵政部、海關、鐵路局等准軍事組織採用。

## 設計及歷史

[Bundesarchiv_Bild_101I-216-0417-19,_Russland,_Soldaten_in_Stellung.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_101I-216-0417-19,_Russland,_Soldaten_in_Stellung.jpg "fig:Bundesarchiv_Bild_101I-216-0417-19,_Russland,_Soldaten_in_Stellung.jpg")

20世紀30年代，[納粹德國重整軍備](../Page/納粹德國.md "wikilink")，根据改進的標準型毛瑟步槍，由Gew.98步枪衍生型Kar.98b卡宾枪（虽然称呼“卡宾枪”它的枪长与Gew.98相同，改下弯式拉机柄）的740mm枪管缩短到与标准型步枪同样的600mm，并由德国陆军进行测试，最终被[德國國防軍作為制式步槍](../Page/德國國防軍.md "wikilink")，命名為Karabiner
98k（簡稱：Kar98k或者K98k），尾部的k解為「Kurz」的縮寫（[德文意為](../Page/德文.md "wikilink")「短」）。於1935年正式投產。稱為[卡賓槍只是相對Gew](../Page/卡賓槍.md "wikilink").98步枪以及Kar.98b卡宾枪縮短了，相對卡賓槍其長度還是過長。

Kar98k繼承了毛瑟98系列步槍經典的毛瑟式[旋轉後拉式槍機](../Page/旋轉後拉式槍機.md "wikilink")，槍機尾部是保險裝置。子彈呈雙排交錯排列的內置式彈倉，使用5發彈夾裝填子彈，子彈通過[機匣上方壓入彈倉](../Page/機匣.md "wikilink")，亦可單發裝填。由直形的改為下彎式，便於攜行和安裝[瞄准镜](../Page/瞄准镜.md "wikilink")。Kar98k步槍成為納粹德國軍隊在第二次世界大戰中期間使用最廣泛的步槍，亦是一種可靠而準確的步槍。

Kar98k在戰爭期間為了滿足軍隊裝備步槍數量的需求，縮減成本，經過多次簡化生產工藝的設計更改，簡化木製[槍托](../Page/槍托.md "wikilink")，部分零部件製造與安裝採用沖壓、銲接工藝。1944年當年的年產量達到歷史高峰。戰爭後期納粹德國面臨戰敗物資匱乏，步槍的製作越發簡陋，品質也每況愈下\[2\]。

多用途是Kar98k步槍服役期限如此之廣泛的原因之一。Kar98k射擊精度高，在加裝4倍、6倍光學瞄準鏡後，可作為一種優秀的[狙擊步槍投入使用](../Page/狙擊步槍.md "wikilink")。Kar98k狙擊步槍共生產了近13萬支並裝備部隊，還有相當多精度較好的Kar98k被挑選出來改裝成狙擊步槍，配備的瞄準鏡和鏡架形式有：ZF-39
4X瞄準鏡，ZF-41 1.5X瞄準鏡，ZF-42 4X瞄準鏡。Kar98k更可以加裝槍榴彈發射器以發射槍榴彈。

儘管Kar98k性能優異，但是隨著戰場上的對手裝備[半自動步槍](../Page/半自動步槍.md "wikilink")（蘇軍[SVT-40步槍](../Page/SVT-40半自動步槍.md "wikilink")、美軍[M1加蘭德步槍](../Page/M1加蘭德步槍.md "wikilink")），德國人認識到這種[手動步槍已經過時了](../Page/手動步槍.md "wikilink")，相繼推出了[Gew
43步槍](../Page/Gew_43步槍.md "wikilink")、[StG44突擊步槍](../Page/StG44突擊步槍.md "wikilink")，但是它們的產量及出現時間無法替代Kar98k，Kar98k一直生產到納粹德國戰敗投降。

## 使用及裝備

### 第二次世界大戰

[納粹德軍在](../Page/納粹德國.md "wikilink")[二戰期間廣泛地裝備毛瑟Kar](../Page/二戰.md "wikilink")98k，在所有德軍參戰的戰區如[歐洲](../Page/歐洲.md "wikilink")、[北非](../Page/北非.md "wikilink")、[蘇聯](../Page/蘇聯.md "wikilink")、[芬蘭及](../Page/芬蘭.md "wikilink")[挪威皆可見其蹤影](../Page/挪威.md "wikilink")，當時德軍士兵暱稱為「」，在歐洲的反抗軍亦時常採用捕獲的Kar98k，連[蘇聯紅軍亦有採用Kar](../Page/蘇聯紅軍.md "wikilink")98k及其他捕獲的德軍槍械。蘇聯在二戰初期因為[蘇德互不侵犯條約時購買了版權及生產機器](../Page/蘇德互不侵犯條約.md "wikilink")，所以亦有生產Kar98k，但其後因要提高戰場彈藥通用性及供應補給問題，改為生產[莫辛-納甘步槍](../Page/莫辛-納甘步槍.md "wikilink")。

除了歐洲以外，[中華民國在](../Page/中華民國.md "wikilink")[抗日戰爭爆發後為了補充大量損失的軍火因此向德國訂購了數萬把的Kar](../Page/抗日戰爭.md "wikilink")98k，由於德國方面為了擴軍產能也到達極限因此沒有多餘軍火可以販售，因此將生產不合規格標準的Kar98k販售給中華民國政府。

### 第二次世界大戰後

[Karabiner-98k-nazi-eagle.jpg](https://zh.wikipedia.org/wiki/File:Karabiner-98k-nazi-eagle.jpg "fig:Karabiner-98k-nazi-eagle.jpg")
二戰後，各戰勝國軍隊大量獲得戰敗國的各种軍備，[蘇聯從中獲得了近百萬把德軍的Kar](../Page/蘇聯.md "wikilink")98k並在1940年代後期至1950年代早期加以改裝及使用。

[冷戰初期](../Page/冷戰.md "wikilink")，[莫斯科為了取得其他共產地區的信任而又無需採用其現役槍械](../Page/莫斯科.md "wikilink")，他們把大部份Kar98k送往多個[共產主義國家及地區作免費軍備支援品](../Page/共產主義.md "wikilink")。正因如此，[越戰期間北越及](../Page/越戰.md "wikilink")[越南南方民族解放陣線的軍隊亦有裝備](../Page/越南南方民族解放陣線.md "wikilink")（一部份為法國軍隊戰敗時留下），[美軍及](../Page/美軍.md "wikilink")[南越部隊皆有報告發現戰場上有Kar](../Page/南越部隊.md "wikilink")98k。

另一方面，二戰後多個曾被佔領及入侵的歐洲國家以[納粹德軍的Kar](../Page/納粹德國.md "wikilink")98k作制式步槍，如[法國及](../Page/法國.md "wikilink")[挪威](../Page/挪威.md "wikilink")，多國廠商如[比利時的](../Page/比利時.md "wikilink")[Fabrique
Nationale](../Page/Fabrique_Nationale.md "wikilink")、[南斯拉夫](../Page/南斯拉夫.md "wikilink")[塞爾維亞](../Page/塞爾維亞.md "wikilink")[克拉古耶瓦茨的](../Page/克拉古耶瓦茨.md "wikilink")[Zastava](../Page/Zastava.md "wikilink")（1945年後生產，名為M48）及[捷克斯洛伐克的](../Page/捷克斯洛伐克.md "wikilink")[Česká
Zbrojovka](../Page/Česká_Zbrojovka.md "wikilink")（廠方名為P-18、捷克斯洛伐克軍方名為）亦有生產，[羅馬尼亞愛國護衛隊採用捷克斯洛伐克的](../Page/羅馬尼亞.md "wikilink")制的版本，後來改用[AKM](../Page/AKM.md "wikilink")。在1950年至1965年間，Zastava仿制版名為[毛瑟M48](../Page/毛瑟M48.md "wikilink")，M48比原裝Kar98k的槍機較短。[南斯拉夫亦在](../Page/南斯拉夫.md "wikilink")1950年代至1960年代間售賣了大量M48往[阿爾及利亞](../Page/阿爾及利亞.md "wikilink")、[埃及及](../Page/埃及.md "wikilink")[伊朗](../Page/伊朗.md "wikilink")，近年其他剩餘的M48又賣給了[美國](../Page/美國.md "wikilink")、[澳大利亞及](../Page/澳大利亞.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")。

[伊朗沒有採用過德國原廠毛瑟Kar](../Page/伊朗.md "wikilink")98k，反而採用[捷克斯洛伐克制的](../Page/捷克斯洛伐克.md "wikilink")[vz.24](../Page/vz.24.md "wikilink")（Gew98的短枪管仿制版）作伊朗共和軍制式步槍，名為「布爾諾」（Brno），名稱沿自原產vz.24的[捷克共和國城市](../Page/捷克共和國.md "wikilink")[布爾諾](../Page/布爾諾.md "wikilink")。

### 以色列的毛瑟Kar98k

部份歐洲以外的國家亦有採用Kar98k，如1940年代後期為了建國的[以色列人](../Page/以色列人.md "wikilink")，他們的Kar98k一直服役至1970年代。大量在[巴勒斯坦的](../Page/巴勒斯坦.md "wikilink")[猶太人組織從戰後歐洲地區取得Kar](../Page/猶太人.md "wikilink")98k來對抗[阿拉伯人的進攻及用於伏擊駐巴勒斯坦的英軍部隊](../Page/阿拉伯人.md "wikilink")\[3\]，包括1948年1月14日從[捷克斯洛伐克運往以色列的](../Page/捷克斯洛伐克.md "wikilink")34,500把P-18步槍及50,400,000發彈藥\[4\]，當中[哈加纳](../Page/哈加纳.md "wikilink")（，，中文解為防禦）是其中一個擁有大量Kar98k及[李-恩菲爾德步槍的巴勒斯坦猶太人組織](../Page/李-恩菲爾德步槍.md "wikilink")。而當時用於建國的以色列Kar98k現在成為了收藏家的珍品。

以色列的毛瑟Kar98k與德國原廠的有所不同，分別在於原有印在[機匣頂部的](../Page/機匣.md "wikilink")[德文](../Page/德文.md "wikilink")[納粹德國兵器局](../Page/納粹.md "wikilink")（）標誌被清除，改為印上[希伯來文的](../Page/希伯來文.md "wikilink")[以色列國防軍標誌以避免勾起納粹德國對以色列人傷害的記憶](../Page/以色列國防軍.md "wikilink")，在1948年立國後，印有以色列國防軍標誌的Kar98k由[比利時](../Page/比利時.md "wikilink")[Fabrique
Nationale生產並賣給以色列](../Page/Fabrique_Nationale.md "wikilink")。在1950年代後期，以色列國防軍在1958年裝備[FN
FAL後](../Page/FN_FAL自動步槍.md "wikilink")，把他們的Kar98k由原來[7.92×57毫米口徑改為](../Page/7.92×57mm毛瑟.md "wikilink")[北約](../Page/北約.md "wikilink")[7.62×51毫米口徑](../Page/7.62×51mm_NATO.md "wikilink")，部份更改用櫸木[槍托](../Page/槍托.md "wikilink")。在1960年代至1970年代，[六日戰爭至](../Page/六日戰爭.md "wikilink")[贖罪日戰爭期間以色列軍隊的供應線部隊及補給部隊仍然裝備Kar](../Page/贖罪日戰爭.md "wikilink")98k。

在以色列的Kar98k從軍中退役後，在1970年代至1980年代，他們把Kar98k賣給拉丁美洲國家及世界各地的民間市場。

### 现代

[2june_2007_187.jpg](https://zh.wikipedia.org/wiki/File:2june_2007_187.jpg "fig:2june_2007_187.jpg")
在二戰時期被德軍使用過的Kar98k現在成為世界各地收藏家的珍品，亦是民間射擊活動中一种非常普遍的步槍，大部份保留了當時的7.92毫米口俓（又名「8毫米毛瑟彈」），亦有小量被改成現在流行的[7.62×51
NATO或](../Page/7.62×51_NATO.md "wikilink")[.308
Winchester口俓](../Page/.308_Winchester.md "wikilink")。

[德國聯邦國防軍軍事學院](../Page/德國聯邦國防軍.md "wikilink")（Wachbataillon）至今仍然使用Kar98k作閱兵典禮及表演，而現在仍有不少的[第三世界國家以Kar](../Page/第三世界.md "wikilink")98k作步兵武器裝備。

在1990年代，[南斯拉夫內戰中](../Page/南斯拉夫內戰.md "wikilink")，[波士尼亞戰區亦有出現Kar](../Page/波士尼亞.md "wikilink")98k、M48及M48A（包括加裝瞄準鏡的狙擊版本）。在2003年，美軍在[伊拉克武裝分子手上發現不少](../Page/伊拉克.md "wikilink")7.92毫米口俓加裝瞄準鏡的Kar98k。

在2005年，俄羅斯在二戰及戰後捕獲的大批Kar98k被重新送到[美國及](../Page/美國.md "wikilink")[加拿大民間市場作](../Page/加拿大.md "wikilink")「軍用剩餘品」出售。

在2011年[利比亞內戰中亦有被反卡扎菲部隊採用](../Page/利比亞內戰.md "wikilink")。

### 民用版本

[En-Mauser_98k_based_hunting_rifle.jpg](https://zh.wikipedia.org/wiki/File:En-Mauser_98k_based_hunting_rifle.jpg "fig:En-Mauser_98k_based_hunting_rifle.jpg")的[儒格M77步槍](../Page/儒格M77步槍.md "wikilink")，Kar98k的眾多民用改型之一。\]\]
現在各國民間市場上流行的Kar98k常被加裝瞄準鏡及改裝成多種口俓，如[6.5 x 55 Swedish
Mauser](../Page/6.5x55_Swedish.md "wikilink")、[7 x
57](../Page/7x57_Mauser.md "wikilink")、[7 x
64](../Page/7_x_64.md "wikilink")、[.270
Winchester](../Page/.270_Winchester.md "wikilink")、[.308
Winchester](../Page/.308_Winchester.md "wikilink")、[.30-06
Springfield等](../Page/.30-06春田步槍彈.md "wikilink")，甚至罕見的[6.5 x
68](../Page/6.5_x_68.md "wikilink")、[8 x 68
S及](../Page/8_x_68_S.md "wikilink")[9.3 x
64](../Page/9.3_x_64.md "wikilink")。

## 使用國

[Bundesarchiv_Bild_183-85458-0003,_Berlin,_Mauerbau,_Kampfgruppen,_NVA,_VP.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-85458-0003,_Berlin,_Mauerbau,_Kampfgruppen,_NVA,_VP.jpg "fig:Bundesarchiv_Bild_183-85458-0003,_Berlin,_Mauerbau,_Kampfgruppen,_NVA,_VP.jpg")
[Kampfer's_ZZS_3.png](https://zh.wikipedia.org/wiki/File:Kampfer's_ZZS_3.png "fig:Kampfer's_ZZS_3.png")可說是Kar98k的表親，兩者很相似\]\]

  -
  -
  -
  - ：曾裝備比利時軍隊。

  - ：在南斯拉夫內戰中仍有採用。

  -
  -
  -
  -
  -
  -
  - ：Kar98k、标准型毛瑟步枪以及捷克斯洛伐克仿制版vz.24，曾在抗日戰爭及國共內戰時期廣泛使用。

  - ：國共內戰及[韓戰時使用](../Page/韓戰.md "wikilink")。

  -
  - ：由生產，並裝備捷克斯洛伐克軍隊，後來被[Vz.58所取代](../Page/Vz.58.md "wikilink")。

  -
  - ：部份由蘇联提供及繼承自[納粹德國的Kar](../Page/納粹德國.md "wikilink")98k繼續在東德軍隊中服役，後來被當地生產的[AK步槍所取代](../Page/AK-47.md "wikilink")。

  -
  - ：二戰期間繳獲自[軸心國部隊](../Page/軸心國.md "wikilink")。

  -
  - ：採購自德國。

  - ：來自戰敗的[納粹德國](../Page/納粹德國.md "wikilink")，曾裝備法軍，於法越戰爭時並有採用。

  - ：被德國聯邦國防軍用作儀仗槍。

  - ：繳獲自荷蘭軍隊。

  -
  -
  - ：由1948年起裝備以色列國防軍，直至1970年代。

  - ：1938年引進50,000枝，命名為"毛式步槍"（）。

  -
  -
  - ：在2011年利比亞內戰中被反格達費部隊採用。

  - ：大公國民警衛隊曾在1945年採用繳獲的Kar98k，但在同年被[加拿大製](../Page/加拿大.md "wikilink")[羅斯步槍](../Page/羅斯步槍.md "wikilink")（Ross
    Rifle）所取代。

  -
  -
  - ：於1935年成為德軍制式步槍，直至二戰結束，以取代[Gewehr
    98步槍](../Page/Gewehr_98步槍.md "wikilink")。

  -
  - ：曾裝備挪威軍隊。

  -
  - ：目前仍被巴基斯坦軍隊用作狙擊步槍。

  -
  -
  -
  - ：愛國護衛隊曾裝備捷克斯洛伐克生產的Kar98k，後來被[AKM所取代](../Page/AKM.md "wikilink")。

  - ：至今仍裝備[要塞衛隊](../Page/要塞衛隊.md "wikilink")。

  -
  -
  -
  -
  - ：於二戰期間蘇聯紅軍從德軍手上繳獲了數量非常多的Kar98k，在戰後蘇聯开始把這些Kar98k及其他過時武器輸送到多个[第三世界及](../Page/第三世界.md "wikilink")[東方陣營國家作軍事援助](../Page/東方陣營.md "wikilink")。

  - ：曾於1939年進口五千支Kar98k。

  -
  -
  - ：在烏克蘭危機中被政府支持的民兵與分離主義武裝使用。

  -
  -
  - ：由蘇聯提供及來自[法越戰爭時法軍留下的Kar](../Page/法越戰爭.md "wikilink")98k曾在越戰時被[越共及北越軍隊使用](../Page/越共.md "wikilink")。

  - ：部份繼承自[納粹德國的Kar](../Page/納粹德國.md "wikilink")98k繼續在西德軍隊中服役，後來被[FN
    FAL所取代](../Page/FN_FAL.md "wikilink")，但有些則保留作儀仗槍使用至今。

  - ：包括在當地生產的[M48及M](../Page/M48毛瑟步槍.md "wikilink")48A仿製型，在內戰時期仍有出現，並有出口到其他國家。

## 流行文化

作為納粹德軍的制式武器，常在以第二次世界大戰為背景的作品中的德軍手中出現。而在有關第一次世界大戰的作品中，Kar98k也常作為Gew
98步槍的替代品登場。

### 電視劇

  - 《[CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")》：第12季第九集登場。
  - 《[失蹤現場](../Page/失蹤現場.md "wikilink")》
  - 《[勇士們](../Page/勇士們_\(電視影集\).md "wikilink")》
  - 《[諾曼第大空降](../Page/諾曼第大空降.md "wikilink")》

### 電影

  - 《[-{zh-cn:夺宝奇兵系列; zh-hk:奪寶奇兵系列;
    zh-tw:印第安納瓊斯系列;}-](../Page/夺宝奇兵.md "wikilink")》：登場於《[-{zh-cn:夺宝奇兵;
    zh-hk:奪寶奇兵;
    zh-tw:法櫃奇兵;}-](../Page/法櫃奇兵.md "wikilink")》和《[-{zh-cn:夺宝奇兵3：圣战奇兵;
    zh-hk:奪寶奇兵之聖戰奇兵;
    zh-tw:聖戰奇兵;}-](../Page/聖戰奇兵.md "wikilink")》之中，由德軍士兵所使用。
  - 《[X戰警：金鋼狼](../Page/X戰警：金鋼狼.md "wikilink")》
  - 《[勝利大逃亡](../Page/勝利大逃亡.md "wikilink")》
  - 《[-{zh-tw:大敵當前;zh-cn:兵临城下;}-](../Page/兵臨城下.md "wikilink")》
  - 《[-{zh-cn:帝国的毁灭; zh-tw:帝國毀滅;
    zh-hk:希特拉的最後十二夜;}-](../Page/帝國的毀滅.md "wikilink")》
  - 《[-{zh-cn:狂怒;zh-hk:戰逆豪情;zh-tw:怒火特攻隊;}-](../Page/怒火特攻隊.md "wikilink")》：納粹德國士兵與[武裝親衛隊所使用](../Page/武裝親衛隊.md "wikilink")，武裝親衛隊的狙擊手更依此重創了「戰爸」上士。
  - 《[-{zh-tw:搶救雷恩大兵;zh-cn:拯救大兵瑞恩;
    zh-hk:雷霆救兵;}-](../Page/拯救大兵瑞恩.md "wikilink")》
  - 《[-{zh-tw:地獄怪客; zh-hk:天魔特攻;
    zh-cn:地狱男爵}-](../Page/地獄怪客_\(電影\).md "wikilink")》
  - 《[-{zh-tw:黃金羅盤; zh-hk:魔幻羅盤;
    zh-cn:黑暗物质三部曲之金罗盘}-](../Page/黃金羅盤.md "wikilink")》
  - 《[-{zh-cn:行动目标希特勒; zh-hk:華爾基利暗殺行動;
    zh-tw:行動代號：華爾奇麗雅;}-](../Page/行動代號：華爾奇麗雅.md "wikilink")》

### 動畫、漫畫

  - 《[人狼 JIN-ROH](../Page/人狼_JIN-ROH.md "wikilink")》（動畫電影）
  - 《[Hellsing](../Page/Hellsing.md "wikilink")》：千禧年大隊的士兵所使用，有裝上刺刀。
  - 《[紅豬](../Page/紅豬.md "wikilink")》：空賊集團「曼馬由特隊」所使用。
  - 《[巴爾札的軍靴](../Page/巴爾札的軍靴.md "wikilink")》：懷森王國陸軍的裝備。
  - 《[心之圖書館](../Page/心之圖書館.md "wikilink")》：動畫版第11話登場。
  - 《[人類衰退之後](../Page/人類衰退之後.md "wikilink")》
  - 《[空·之·音](../Page/空·之·音.md "wikilink")》：第1121小隊內被分配到四把。
  - 《[霍爾的移動城堡](../Page/霍爾的移動城堡.md "wikilink")》（動畫電影）
  - 《[非戰特攻隊](../Page/非戰特攻隊.md "wikilink")》：帝國軍的裝備。
  - 《[企業傭兵](../Page/企業傭兵.md "wikilink")》
  - 《[危險調查員](../Page/危險調查員.md "wikilink")》：最終話，於羅馬尼亞的村莊由奇頓等人用來對抗敵人。
  - 《[鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")》：莉莎·霍克愛中尉曾多次使用。
  - 《》：普遍被[納粹德國士兵使用](../Page/納粹德國.md "wikilink")。
  - 《[幼女戰記](../Page/幼女戰記.md "wikilink")》：帝國軍士兵的標準裝備。

### 電子遊戲

  - 《[戰地風雲1942](../Page/戰地風雲1942.md "wikilink")》
  - 《[戰地之王](../Page/戰地之王.md "wikilink")》
  - 《[絕地求生](../Page/絕地求生.md "wikilink")》：游戏中改为可装填[7.62×54mmR子弹的步枪](../Page/7.62×54mmR.md "wikilink")。可裝備配件：全息瞄準器、紅點瞄準器、2、3、4、6、8、15倍瞄準器、子彈袋、槍口消焰、補償、消音器。
  - 《[德軍總部系列](../Page/德軍總部系列.md "wikilink")》
  - 《[決勝時刻系列](../Page/決勝時刻系列.md "wikilink")》
      - 《[決勝時刻](../Page/使命召喚_\(遊戲\).md "wikilink")》
      - 《[決勝時刻：聯合行動](../Page/決勝時刻：聯合行動.md "wikilink")》
      - 《[決勝時刻2](../Page/決勝時刻2.md "wikilink")》
      - 《[決勝時刻3](../Page/使命召唤3.md "wikilink")》
      - 《[決勝時刻：戰爭世界](../Page/決勝時刻：戰爭世界.md "wikilink")》
      - 《[決勝時刻：黑色行動](../Page/決勝時刻：黑色行動.md "wikilink")》
      - 《[決勝時刻：二戰](../Page/使命召唤：二战.md "wikilink")》
  - 《[榮耀戰場系列](../Page/榮耀戰場系列.md "wikilink")》
  - 《[榮譽勳章系列](../Page/榮譽勳章系列.md "wikilink")》
  - 《[越南大戰系列](../Page/越南大戰系列.md "wikilink")》
  - 《[少女前線](../Page/少女前線.md "wikilink")》：遊戲中五星槍，又名「德皇」，身為遊戲主要[看板娘](../Page/看板娘.md "wikilink")，但因技能不實用而被冠上「無用的看板娘」的惡稱。
  - 《[英雄與將軍](../Page/英雄與將軍.md "wikilink")》
  - 《Free Fire -
    我要活下去》：槍口可裝備補償器及消音器，但配備3級補償器射擊敵人三級頭盔只扣198HP(這時敵人還剩2HP未死)
  - 《[荒野行動](../Page/荒野行動.md "wikilink")》
  - 《[狙擊之神2](../Page/狙擊之神2.md "wikilink")》：初回版的特典，以Karabiner 98k的名稱登場。
  - 《[-{zh-hans:战地5; zh-hant:戰地風雲5;}-](../Page/戰地風雲5.md "wikilink")》

## 參考

  - [M1加蘭德步槍](../Page/M1加蘭德步槍.md "wikilink")
  - [M1903春田步槍](../Page/M1903春田步槍.md "wikilink")
  - [莫辛-纳甘步枪](../Page/莫辛-纳甘步枪.md "wikilink")
  - [中正式步槍](../Page/中正式步槍.md "wikilink")
  - [李-恩菲爾德步槍](../Page/李-恩菲爾德步槍.md "wikilink")
  - [三八式步槍](../Page/三八式步槍.md "wikilink")
  - [M48毛瑟步槍](../Page/M48毛瑟步槍.md "wikilink")
  - [儒格M77步槍](../Page/儒格M77步槍.md "wikilink")

## 註釋

## 參考文獻

  - [bellum.nu-Karbiner 98 kurz
    (Kar 98k)](http://www.bellum.nu/armoury/Kar98k.html)
  - [Kar98k刺刀](https://web.archive.org/web/20070403044329/http://www.rememuseum.org.uk/arms/blade/armbay.htm#304)

## 外部链接

  - —[Modern Firearms-毛瑟Kar98](http://world.guns.ru/rifle/rfl02-e.htm)

  - —[D Boy Gun
    World（槍炮世界）—Kar.98k式短卡宾枪](http://firearmsworld.net/german/mauser/rifle/kar98k.htm)

      - [98k的狙击型](http://firearmsworld.net/german/mauser/rifle/zf98k.htm)
      - [战争中的98k](http://firearmsworld.net/german/mauser/rifle/98kpics.htm)

[Category:卡宾枪](../Category/卡宾枪.md "wikilink")
[Category:栓動式步槍](../Category/栓動式步槍.md "wikilink")
[Category:狙擊步槍](../Category/狙擊步槍.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")
[Category:中國二戰槍械](../Category/中國二戰槍械.md "wikilink")
[Category:7.92×57毫米槍械](../Category/7.92×57毫米槍械.md "wikilink")
[Category:德國二戰武器](../Category/德國二戰武器.md "wikilink")
[Category:芬蘭二戰武器](../Category/芬蘭二戰武器.md "wikilink")
[Category:羅馬尼亞二戰武器](../Category/羅馬尼亞二戰武器.md "wikilink")
[Category:抗戰時期中國武器](../Category/抗戰時期中國武器.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")
[Category:韓戰武器](../Category/韓戰武器.md "wikilink")
[Category:1935年面世的产品](../Category/1935年面世的产品.md "wikilink")

1.
2.  [戰國時代討論區-後期型Kar98k圖片及說明](http://artofwar.homeip.net/bboard/viewtopic.php?t=5425)
3.  [以色列從建國至今裝備的的狙擊槍系列](http://club.6park.com/military/messages/gvk54398.html)
4.  [捷克陸軍頁面-捷克斯洛伐克在1948年武器支援以色列的資料](http://www.army.cz/avis/publikace/srdce_armady/osobozeni_totalita/unor_1948_voj_pom_izraelu.pdf)