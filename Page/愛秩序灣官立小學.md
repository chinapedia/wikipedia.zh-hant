[Aldrich_Bay_Government_Primary_School_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Aldrich_Bay_Government_Primary_School_\(Hong_Kong\).jpg "fig:Aldrich_Bay_Government_Primary_School_(Hong_Kong).jpg")
**愛秩序灣官立小學**（**A**ldrich **B**ay **G**overnment **P**rimary
**S**chool，**ABGPS**）位於[香港](../Page/香港.md "wikilink")[港島](../Page/港島.md "wikilink")[筲箕灣](../Page/筲箕灣.md "wikilink")，是一所全日制官立小學，於2000年創校，目前開設小一至小六級，共30班。原為渣華道官立小學上午校及下午校。

## 班別

  - 一年級
      - 1A（一德）、1B（一智）、1C（一體）、1D（一群）,1E（一美）
  - 二年級
      - 2A（二德）、2B（二智）、2C（二體）、2D（二群）,2E（二美）
  - 三年級
      - 3A（三德）、3B（三智）、3C（三體）、3D（三群）,3E（三美）
  - 四年級
      - 4A（四德）、4B（四智）、4C（四體）、4D（四群）,4E（四美）
  - 五年級
      - 5A（五德）、5B（五智）、5C（五體）、5D（五群）,5E（五美）
  - 六年級
      - 6A（六德）、6B（六智）、6C（六體）、6D（六群）,6E（六美）

## 聯繫中學

  - [皇仁書院](../Page/皇仁書院.md "wikilink")
  - [筲箕灣官立中學](../Page/筲箕灣官立中學.md "wikilink")
  - [金文泰中學](../Page/金文泰中學.md "wikilink")
  - [筲箕灣東官立中學](../Page/筲箕灣東官立中學.md "wikilink")

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[筲箕灣站D](../Page/筲箕灣站.md "wikilink")2出口(步行約5分鐘)

## 知名//傑出校友

  - [霍朗齊](../Page/霍朗齊.md "wikilink")：香港音樂創作人
  - [黃凱儀](../Page/黃凱儀.md "wikilink")：香港女主持及意見領袖（2010年畢業）

[Category:香港東區小學](../Category/香港東區小學.md "wikilink")
[A](../Category/香港官立小學.md "wikilink")
[Category:筲箕灣](../Category/筲箕灣.md "wikilink")
[Category:2000年創建的教育機構](../Category/2000年創建的教育機構.md "wikilink")