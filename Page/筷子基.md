[Fai_chi_kei.png](https://zh.wikipedia.org/wiki/File:Fai_chi_kei.png "fig:Fai_chi_kei.png")
**筷子基**（[葡萄牙文](../Page/葡萄牙文.md "wikilink")：）在[澳門半島西北部](../Page/澳門半島.md "wikilink")[半島](../Page/半島.md "wikilink")，是多層大廈林立的基層住宅區，根據澳門區域屬[花地瑪堂區](../Page/花地瑪堂區.md "wikilink")。

## 歷史

筷子基原為[沙洲](../Page/沙洲.md "wikilink")，古時稱為「赤洲」。

  - 1920年代，這裡填成狹長土地，在此地的東邊架了小橋連至[白朗古將軍大馬路](../Page/白朗古將軍大馬路.md "wikilink")，南北水面分別為筷子基南灣、北灣。
  - 1932年，這塊土地上面建了14座兩層的[平房且分成兩排](../Page/平房.md "wikilink")，稱為**五二八坊**，以紀念1926年發生的[五二八革新](../Page/五二八革新.md "wikilink")\[1\]。由於平房構城的形狀好像一雙[筷子](../Page/筷子.md "wikilink")，故這裡又名**筷子基**。
  - 1940年代，筷子基東邊小橋處填平，從此和澳門半島連成一體。
  - 1970年代，筷子基的東北填出了新土地，蓋了十多棟大廈。
  - 1980年代，上述平民房屋相繼被拆且改建成大廈。此外，筷子基也進行了填海工程，與[新橋和](../Page/新橋.md "wikilink")[沙梨頭連在一起](../Page/沙梨頭.md "wikilink")。

位於現時[筷子基衛生中心位置](../Page/筷子基衛生中心.md "wikilink")，1990年代仍是木屋區（約於1997年完全拆卸），木屋沿河而建，而在木屋旁的木廠（截止2010年8月仍在）下方，設有一個大型去水渠，而木屋的另一旁，有一個不知名的小島（約籃球場大小），島上佈滿香蕉和芋頭，中央設有一間小茅屋，當時，從筷子基走向紅街市，並沒有直接道路，往往要繞道而行

## 區內重要設施

  - [逸園賽狗場](../Page/逸園賽狗場_\(澳門\).md "wikilink")

## 交通

[1A](../Page/澳門巴士1A路線.md "wikilink")、[4](../Page/澳門巴士4路線.md "wikilink")、[32](../Page/澳門巴士32路線.md "wikilink")、[33路線巴士都是從](../Page/澳門巴士33路線.md "wikilink")[筷子基總站開出](../Page/筷子基總站.md "wikilink")。

  - [1A](../Page/澳門巴士1A路線.md "wikilink")：[筷子基](../Page/筷子基總站.md "wikilink")
    ↔ [皇朝](../Page/新口岸／科英布拉街總站.md "wikilink")
  - [4](../Page/澳門巴士4路線.md "wikilink")
    ：[筷子基](../Page/筷子基總站.md "wikilink") ↺
    [新馬路](../Page/新馬路.md "wikilink")
  - [32](../Page/澳門巴士33路線.md "wikilink")：[筷子基](../Page/筷子基總站.md "wikilink")
    ↺ [旅遊塔](../Page/澳門旅遊塔.md "wikilink")
  - [33](../Page/澳門巴士33路線.md "wikilink")：[筷子基](../Page/筷子基總站.md "wikilink")
    ↺ [氹仔](../Page/氹仔.md "wikilink")

## 以筷子基命名之街道或地名

  - [筷子基巷](../Page/筷子基巷.md "wikilink")**，葡萄牙文：：介乎**[俾若翰街](../Page/俾若翰街.md "wikilink")**和**[筷子基街](../Page/筷子基街.md "wikilink")'''之間一段巷。
  - [筷子基街](../Page/筷子基街.md "wikilink")**，葡萄牙文：：介乎**[俾若翰街](../Page/俾若翰街.md "wikilink")'''至海邊的一條街。
  - [飛喇士街](../Page/飛喇士街.md "wikilink")/[筷子基南街](../Page/筷子基南街.md "wikilink")**，葡萄牙文：：介乎**[白朗古將軍大馬路](../Page/白朗古將軍大馬路.md "wikilink")**至**[船澳街](../Page/船澳街.md "wikilink")'''的一條街。
  - [高士德街](../Page/高士德街.md "wikilink")/[筷子基中街](../Page/筷子基中街.md "wikilink")**，葡萄牙文：：在**[沙梨頭南街](../Page/沙梨頭南街.md "wikilink")**，**筷子基'''平民大廈內的一條街。
  - [筷子基社屋](../Page/筷子基社屋.md "wikilink")，快富樓、快意樓及快達樓1及2座

## 參考資料

## 外部連結

  - [《就筷子基平民大廈保育問題文物大使協會促特府回應》](http://www.jornalvakio.com/index.php?tn=viewer&ncid=1&nid=153608&dt=20100103&lang=tw)
    - 《[華僑報](../Page/華僑報.md "wikilink")》二零一零年一月三日號
  - [《中國建築文物、亞洲建築師會金獎作臨清拆危機》](http://www.puhuabao.com/portal/news/ptnews/541-2010-01-22-17-19-43)
    - 《[葡華報](../Page/葡華報.md "wikilink")》二零一零年一月廿二日號

[Category:澳門地方](../Category/澳門地方.md "wikilink")

1.