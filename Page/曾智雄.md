**曾智雄**（），籍貫[廣東省](../Page/廣東省.md "wikilink")[梅縣](../Page/梅縣區.md "wikilink")，[香港](../Page/香港.md "wikilink")[商人](../Page/商人.md "wikilink")，[中國](../Page/中國.md "wikilink")[世貿集團主席](../Page/世貿集團.md "wikilink")，[香港青年聯會主席](../Page/香港青年聯會.md "wikilink")，中國[全國人大常委](../Page/全國人大.md "wikilink")[曾憲梓的次子](../Page/曾憲梓.md "wikilink")。

曾智雄1980年代初跟隨其父曾憲梓在中國內地工作，1981年成為金利來中國内地第一任經理。2002年離開家族的[金利來集團生意](../Page/金利來集團.md "wikilink")，創辦中國世貿集團，主力於商務服務，在中國內地經營[世貿中心俱樂部業務](../Page/世貿中心俱樂部.md "wikilink")。中國世貿集團於[美國](../Page/美國.md "wikilink")[納斯達克上市](../Page/納斯達克.md "wikilink")。[2004年9月](../Page/2004年9月.md "wikilink")，在[廣州及](../Page/廣州.md "wikilink")[北京收購中國CEO俱樂部](../Page/北京.md "wikilink")。[2005年5月出任香港青年聯會主席](../Page/2005年5月.md "wikilink")。

曾智雄興趣包括[高爾夫球](../Page/高爾夫球.md "wikilink")，小學時曾參加[劍擊](../Page/劍擊.md "wikilink")、[冰上曲棍球等較劇烈運動](../Page/冰上曲棍球.md "wikilink")。18歲時考獲[花劍國際教練牌照](../Page/花劍.md "wikilink")，曾代表學校參加校際花劍比賽取得冠軍。

曾智雄為人低調，其兄長是[曾智謀](../Page/曾智謀.md "wikilink")，其弟[曾智明的前妻是藝人](../Page/曾智明.md "wikilink")[黎瑞恩](../Page/黎瑞恩.md "wikilink")。

## 婚變事件

曾智雄在1988年1月與[吳嘉寶註冊結婚](../Page/吳嘉寶.md "wikilink")，兩人屬青梅竹馬，其妻婚前從事工商管理。2006年9月18日，曾智雄的妻子吳嘉寶在香港報章刊登一篇「分居啟事」，指其丈夫曾智雄與一位[楊姓女藝人有婚外情](../Page/楊姓.md "wikilink")，令雙方18年婚姻破裂，二人[分居](../Page/分居.md "wikilink")。吳嘉寶在分居後前往[加拿大定居](../Page/加拿大.md "wikilink")。

9月19日，曾智雄從[廣州回港](../Page/廣州.md "wikilink")，並於[沙田金利來集團公司召開](../Page/沙田.md "wikilink")[記者會](../Page/記者會.md "wikilink")，向傳媒解釋事件。他指出與吳嘉寶於2002年5月開始分居，由對方提出，2004年4月2日收到正式分居的[律師信](../Page/律師信.md "wikilink")，而他否認曾發展婚外情。[1](https://web.archive.org/web/20070703104636/http://hk.news.yahoo.com/060918/12/1t8jb.html)
[2](http://hk.news.yahoo.com/060918/60/1t8wl.html)
[3](https://web.archive.org/web/20071208144153/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060920&sec_id=4104&subsec_id=12731&art_id=6330372)
[4](http://hk.news.yahoo.com/060919/60/1tbi1.html)
楊姓女藝人他只認識-{[楊采妮](../Page/楊采妮.md "wikilink")}-。[5](http://hk.news.yahoo.com/060919/197/1tbcb.html)

其父曾憲梓對事件的回應是：「做唔成新抱，做我個女都得」。[6](https://web.archive.org/web/20070514024506/http://hk.news.yahoo.com/060919/12/1tb6j.html)

## 公職

  - 香港[新界工商業總會會長](../Page/新界工商業總會.md "wikilink")
  - 香港青年聯會主席
  - [香港中華總商會會董](../Page/香港中華總商會.md "wikilink")
  - [北京市](../Page/北京市.md "wikilink")[政協委員](../Page/政協.md "wikilink")
  - [廣州市榮譽市民](../Page/廣州市.md "wikilink")
  - [廣東省外商公會副會長](../Page/廣東省.md "wikilink")
  - 廣東省工商聯常委
  - 廣州市工商聯副會長
  - 中國香港（地區）商會－廣東副會長
  - [中華海外聯誼會理事](../Page/中華海外聯誼會.md "wikilink")
  - [中華全國青年聯合會委員](../Page/中華全國青年聯合會.md "wikilink")
  - [天津市青聯副主席](../Page/天津市.md "wikilink")
  - [吉林省](../Page/吉林省.md "wikilink")[四平市榮譽市民](../Page/四平市.md "wikilink")
  - 吉林省海外聯誼會副會長
  - [長春市國際經濟顧問](../Page/長春市.md "wikilink")

## 資料來源

  - [曾智雄：助港青增祖國認同感](https://web.archive.org/web/20060719045526/http://www.wenweipo.com/news.phtml?news_id=AZ0504120001&loc=any&cat=043AZ&no_combo=1&PHPSESSID=896085a32ed600e8384128731fb88eb6)
    文匯報 2005年4月12日
  - [發揮青年優勢
    構建和諧社會](https://web.archive.org/web/20070930061140/http://www.takungpao.com/news/06/08/10/ZM-605900.htm)
    大公報
  - [領帶大王曾憲梓之子--曾智雄率團來漢“探路”](http://www.cnhubei.com/200503/ca743630.htm)
  - [曾智雄：香港青年在內地與東盟各國的聯繫中大有作為](http://www.yn.xinhuanet.com/newscenter/2006-08/10/content_7737848.htm)
  - [曾智雄
    中華全國青年聯合會](http://www.qinglian.org/community/userinfo.jsp?id=100140069)
  - [新增委員](https://web.archive.org/web/20060319065412/http://www.bjzx.gov.cn/news/xzwy/index.htm)
  - [曾常委次子婚變 前妻爆夫與藝人有染](http://hk.news.yahoo.com/060917/12/1t5x7.html)
    明報

[Category:梅縣區人](../Category/梅縣區人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:北京市政協委員](../Category/北京市政協委員.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")
[Category:香港客家人](../Category/香港客家人.md "wikilink")
[Zhi智](../Category/曾憲梓家族.md "wikilink")
[Zhi智](../Category/曾姓.md "wikilink")
[Category:全国青联副主席](../Category/全国青联副主席.md "wikilink")