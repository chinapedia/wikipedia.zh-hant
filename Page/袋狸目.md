**袋狸目**（Peramelemorphia）是包括了[袋狸及](../Page/袋狸.md "wikilink")[兔袋狸的一目](../Page/兔袋狸.md "wikilink")，接近「雜食性有袋類」的主支。其下所有成員都是[澳洲及](../Page/澳洲.md "wikilink")[新畿內亞的](../Page/新畿內亞.md "wikilink")[原住民](../Page/原住民.md "wikilink")，大部份都有袋狸的體態：肥胖、弓背、尖長的吻、很大的[耳朵](../Page/耳朵.md "wikilink")、幼長的腳及幼的尾巴。牠們的體型介乎140克至2公斤，大部份都有約1公斤重。

## 演化關係

袋狸目在[有袋類中的位置一直被受討論](../Page/有袋類.md "wikilink")。現時有兩個形態特徵顯示與其他有袋類是有[演化關連的](../Page/演化.md "wikilink")，包括腳掌的類型及[牙齒](../Page/牙齒.md "wikilink")。

所有袋狸目的成員都是多門齒類動物，共有三對下[門齒](../Page/門齒.md "wikilink")。這顯示牠們是從[肉食性的](../Page/肉食性.md "wikilink")[袋鼬目演化而來](../Page/袋鼬目.md "wikilink")。另一方面，牠們的腳上有不尋常的地方，第二及第三趾是融合在一起的，即[併指症](../Page/併指症.md "wikilink")，是[雙門齒目的特徵](../Page/雙門齒目.md "wikilink")。

一些學者認為袋狸目下的[袋狸類是從肉食性的有袋類演化而來](../Page/袋狸.md "wikilink")，故保留了多門齒的[齒列](../Page/齒列.md "wikilink")，並獨自的演化出併指的後腳。另一些則認為併指是很難從演化兩次而來，估計牠們是從像[負鼠的雙門齒目演化而來](../Page/負鼠.md "wikilink")，再行演化出額外的牙齒。第三種意見指牠們是演化自原始的肉食動物，並發展出適合攀樹的拼指後腳，而雙門齒目從此分裂出來，並演化出兩顆牙齒的顎部。[分子分析研究卻沒有為不明的情況提供解答](../Page/分子.md "wikilink")，但無論牠們與其他有袋類的關係如何，牠們都必然是最遠的親屬。

## 科學分類

以下是袋狸目的有關分類：

  - [微獸目](../Page/微獸目.md "wikilink")（Microbiotheria）：現存只有1個[物種](../Page/物種.md "wikilink")。
  - [袋鼬目](../Page/袋鼬目.md "wikilink")（Dasyuromorphia）：3個[科內共](../Page/科.md "wikilink")71個物種。
  - **袋狸目**（Peramelemorphia）：包括[袋狸及](../Page/袋狸.md "wikilink")[兔袋狸](../Page/兔袋狸.md "wikilink")，共有20個現存物種，3個已[滅絕](../Page/滅絕.md "wikilink")。
      - [兔袋狸科](../Page/兔袋狸科.md "wikilink")（Thylacomyidae）：現存1個物種，1個已滅絕。
      - †[豚足袋狸科](../Page/豚足袋狸科.md "wikilink")（Chaeropodidae）：1個絕滅物種。
      - [袋狸科](../Page/袋狸科.md "wikilink")（Peramelidae）：19個現存物種，1個已滅絕。
          - [袋狸亚科](../Page/袋狸亚科.md "wikilink")（Peramelinae）
          - [新几内亚袋狸亚科](../Page/新几内亚袋狸亚科.md "wikilink")（Peroryctinae）
          - [刺袋狸亚科](../Page/刺袋狸亚科.md "wikilink")（Echymiperinae）
      - †Yaraloidea
          - †Yaralidae：2個已描述的[化石物種](../Page/化石.md "wikilink")
  - [袋鼹目](../Page/袋鼹目.md "wikilink")（Notoryctemorphia）：[袋鼴屬下共](../Page/袋鼴屬.md "wikilink")2個物種。
  - [双门齿目](../Page/双门齿目.md "wikilink")（Diprotodontia）：11個科下約有117個物種，包括[樹熊](../Page/樹熊.md "wikilink")、[袋熊](../Page/袋熊.md "wikilink")、[負鼠](../Page/負鼠.md "wikilink")、[袋鼠等](../Page/袋鼠.md "wikilink")。

## 參考

  -
[袋狸目](../Page/分類:袋狸目.md "wikilink")
[分類:澳洲有袋類](../Page/分類:澳洲有袋類.md "wikilink")