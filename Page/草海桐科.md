**草海桐科**有12[属约](../Page/属.md "wikilink")404[种](../Page/种.md "wikilink")([*Flora
of Australia*, vol.
35](http://www.deh.gov.au/biodiversity/abrs/publications/flora-of-australia/vol35.html))主要分布在[澳大利亚各地](../Page/澳大利亚.md "wikilink")，尤其是干旱和半干旱地带；其中[草海桐属](../Page/草海桐属.md "wikilink")（
*Scaevola*）广泛分布在世界各地的[热带地区](../Page/热带.md "wikilink")。[中国有](../Page/中国.md "wikilink")2属3种，分布在南部沿海和岛屿上。

本科[植物通常为](../Page/植物.md "wikilink")[草本](../Page/草本.md "wikilink")，也有[亚灌木或](../Page/亚灌木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，[花两侧对称](../Page/花.md "wikilink")。

  - 属

<!-- end list -->

  - *[Anthotium](../Page/Anthotium.md "wikilink")*
    [R.Br.](../Page/Robert_Brown_\(botanist\).md "wikilink")
  - *[Brunonia](../Page/Brunonia.md "wikilink")*
    [Sm.](../Page/James_Edward_Smith.md "wikilink")
  - *[Coopernookia](../Page/Coopernookia.md "wikilink")*
    [Carolin](../Page/Carolin.md "wikilink")
  - *[Dampiera](../Page/Dampiera.md "wikilink")*
    [R.Br.](../Page/Robert_Brown_\(botanist\).md "wikilink")
  - *[Diaspasis](../Page/Diaspasis.md "wikilink")*
    [R.Br.](../Page/Robert_Brown_\(botanist\).md "wikilink")
  - [美柱草屬](../Page/美柱草屬.md "wikilink")*Goodenia*
    [Sm.](../Page/James_Edward_Smith.md "wikilink")
  - [花环花属](../Page/花环花属.md "wikilink")
    *[Lechenaultia](../Page/Lechenaultia.md "wikilink")*
    [R.Br.或](../Page/Robert_Brown_\(botanist\).md "wikilink") （
    *[Leschenaultia](../Page/Leschenaultia.md "wikilink")*
    [DC.](../Page/A._P._de_Candolle.md "wikilink")）
  - *[Pentaptilon](../Page/Pentaptilon.md "wikilink")*
    [E.Pritz.](../Page/E.Pritz..md "wikilink")
  - [草海桐属](../Page/草海桐属.md "wikilink") *Scaevola*
    [L.](../Page/L..md "wikilink")
  - *[Selliera](../Page/Selliera.md "wikilink")*
    [Cav.](../Page/Cav..md "wikilink")
  - *[Velleia](../Page/Velleia.md "wikilink")*
    [Sm.](../Page/James_Edward_Smith.md "wikilink")
  - *[Verreauxia](../Page/Verreauxia.md "wikilink")*
    [Benth.](../Page/Benth..md "wikilink")

1981年的[克朗奎斯特分类法将本科列入](../Page/克朗奎斯特分类法.md "wikilink")[桔梗目](../Page/桔梗目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其列入](../Page/APG_分类法.md "wikilink")[菊目](../Page/菊目.md "wikilink")。

## 参考文献

  - [Goodeniaceae](http://delta-intkey.com/angio/www/goodenia.htm) in
    [L. Watson and M.J. Dallwitz (1992 onwards). The families of
    flowering plants: descriptions, illustrations, identification,
    information retrieval.](http://delta-intkey.com/angio/)
    <http://delta-intkey.com>

  - [Goodeniaceae](http://www.mobot.org/MOBOT/Research/APweb/orders/asteralesweb.htm#Goodeniaceae)
    (at the [Angiosperm Phylogeny
    Website](http://www.mobot.org/MOBOT/Research/APweb))

  -
## 外部链接

  - [澳大利亚国家植物园中的草海桐科植物图片](http://www.anbg.gov.au/images/photo_cd/goodeniaceae/)

[\*](../Category/草海桐科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")