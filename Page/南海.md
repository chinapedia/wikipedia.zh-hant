**南海**，又稱為**-{zh-tw:南中國海;zh-cn:南中国海;zh-sg:南海;}-**、**中国南-{}-海**，是位於[东南亚的](../Page/东南亚.md "wikilink")[陸緣海](../Page/陸緣海.md "wikilink")，被[中國大陆](../Page/中國大陆.md "wikilink")、[台灣岛](../Page/台灣岛.md "wikilink")、[菲律宾群岛](../Page/菲律宾群岛.md "wikilink")、[马来群岛及](../Page/马来群岛.md "wikilink")[中南半岛所环绕](../Page/中南半岛.md "wikilink")，為[西太平洋的一部分](../Page/西太平洋.md "wikilink")。

南海海域面积有350万平方公里，其中有超过200个无原住民居住的[島嶼和](../Page/島嶼.md "wikilink")[岩礁](../Page/礁.md "wikilink")，這些島礁被合稱為[南海諸島](../Page/南海諸島.md "wikilink")。除了是主要的海上运输航线外，南海还蕴藏着丰富的[石油和](../Page/石油.md "wikilink")[天然气](../Page/天然气.md "wikilink")\[1\]。南海海域牵涉到许多周邊国家的利益，是一个非常敏感的地区\[2\]。中国于2012年設立[海南省](../Page/海南省.md "wikilink")[三沙市](../Page/三沙市.md "wikilink")，轄西沙群島、中沙群島、南沙群島的島礁及其海域。

## 名称沿革

[中国](../Page/中国.md "wikilink")[漢朝](../Page/漢朝.md "wikilink")、[南北朝時稱其為](../Page/南北朝.md "wikilink")**漲海**、**沸海**，[清朝滅亡後逐漸改稱](../Page/清朝.md "wikilink")**南海**，并延续至今。

十六世紀時，葡萄牙水手把它稱為中國海，後來称为南中國海\[3\]。[國際海道測量組織的使用的名称為](../Page/國際海道測量組織.md "wikilink")“南中國海（南海）”\[4\]。[新加坡](../Page/新加坡.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[日本等從国际上通用的](../Page/日本.md "wikilink")[英文名称](../Page/英文.md "wikilink")「」称之为**南-{}-中国海**（；；；（第二次世界大战以前为「」））。

2011年南海主權爭議升级后，[菲律賓政府開始使用](../Page/菲律賓.md "wikilink")“西菲律賓海”的名稱\[5\]。

## 地理

### 范围

在[國際水文局的定義中](../Page/國際水文局.md "wikilink")，南海為東北—西南走向，其南部邊界在[南緯](../Page/南緯.md "wikilink")3度，位於[印度尼西亚的南](../Page/印度尼西亚.md "wikilink")[蘇門達臘和](../Page/蘇門達臘.md "wikilink")[加里曼丹之間](../Page/加里曼丹.md "wikilink")，北邊至[中国大陆](../Page/中国大陆.md "wikilink")，东北至[台灣本島](../Page/台灣本島.md "wikilink")，東至[菲律賓群島](../Page/菲律賓群島.md "wikilink")，且包含[呂宋海峽西半側](../Page/呂宋海峽.md "wikilink")，西南至[越南與](../Page/越南.md "wikilink")[馬來半島](../Page/馬來半島.md "wikilink")，通過[巴士海峽](../Page/巴士海峽.md "wikilink")、[蘇祿海和](../Page/蘇祿海.md "wikilink")[馬六甲海峽連接](../Page/馬六甲海峽.md "wikilink")[太平洋和](../Page/太平洋.md "wikilink")[印度洋](../Page/印度洋.md "wikilink")\[6\]。整個南海幾乎被大陸、半島和島嶼所包圍。其形狀近似[菱形](../Page/菱形.md "wikilink")，從四周呈階梯狀向中部加深。

南海為世界第三大[陸緣海](../Page/陸緣海.md "wikilink")，僅次於[珊瑚海和](../Page/珊瑚海.md "wikilink")[阿拉伯海](../Page/阿拉伯海.md "wikilink")，面積約356萬平方公里，約等於[渤海](../Page/渤海.md "wikilink")、[黃海和](../Page/黃海.md "wikilink")[東海總面積的](../Page/東海.md "wikilink")3倍，平均水深約為1,212米，最深處為中部的深海平原，達5,567米左右\[7\]。

### 地质

[缩略图](https://zh.wikipedia.org/wiki/File:South_China_Sea.jpg "fig:缩略图")

南海分[大陸架](../Page/大陸架.md "wikilink")、[大陸坡和深海盆等地貌單元](../Page/大陸坡.md "wikilink")。大陸架以西南部最寬，北部陸架寬約285公里，東、西部陸架最窄，[呂宋島以西](../Page/呂宋島.md "wikilink")，島架寬僅5公尺；大陸坡呈階梯狀下降，主要的[南海諸島都在其大陸坡上](../Page/南海諸島.md "wikilink")，東、西部陸坡較陡，並有許多切割峽谷。\[8\]

古南海在台湾-巴拉望岛（palawan）以东，与太平洋板块和菲律宾板块的大洋相邻。在巴拉望岛（palawan）与海南岛之间（今南海）为欧亚大陆的陆壳。3000-3300万年左右，也就是始新世的末期，随着[印度板块与欧亚板块的碰撞](../Page/印度板块.md "wikilink")，整个东亚地区形成了一个挤出-逃逸的扇形构造，华北-华南地块被向东挤出，印支地块被向东南方向挤出，二者之间大致以红河断裂带为界。两者之间的速度和角度差，将巴拉望岛和海南岛之间的陆壳撕裂，地幔物质上涌，形成南海[洋中脊](../Page/洋中脊.md "wikilink")。\[9\]南海洋盆的裂开活动至约1500万年前终止。南海洋盆在3300-1500万年间裂开了大约600-700公里。

### 气候

南海地處低緯度地域，屬於[熱帶深海](../Page/熱帶.md "wikilink")。

### 水温

南海海水表層水溫較高從25℃到28℃左右，年溫差3℃到4℃，鹽度為35‰，潮差平均2米。流入南海主要河流有
[珠江](../Page/珠江.md "wikilink")、[紅河](../Page/紅河.md "wikilink")、[湄公河](../Page/湄公河.md "wikilink")（中国境内水域称[澜沧江](../Page/澜沧江.md "wikilink")）。

## 岛屿和海山

[Karta_CN_SouthChinaSea.PNG](https://zh.wikipedia.org/wiki/File:Karta_CN_SouthChinaSea.PNG "fig:Karta_CN_SouthChinaSea.PNG")

南海有超过200个无原住民居住的[島嶼和](../Page/島嶼.md "wikilink")[岩礁](../Page/礁.md "wikilink")，這些島礁被合稱為[南海諸島](../Page/南海諸島.md "wikilink")。由北向南依次是：[東沙群島](../Page/東沙群島.md "wikilink")
、[西沙群島](../Page/西沙群島.md "wikilink")、[中沙群島](../Page/中沙群島.md "wikilink")、[南沙群岛](../Page/南沙群岛.md "wikilink")、[納土納群島](../Page/納土納群島.md "wikilink")
、[阿南巴斯群島](../Page/阿南巴斯群島.md "wikilink")。

## 資源

### 生物

由於南海屬於熱帶海洋，適於[珊瑚繁殖](../Page/珊瑚.md "wikilink")，海底高台處形成[珊瑚岛](../Page/珊瑚岛.md "wikilink")，南海諸島的東沙群島、西沙群島、中沙群島和南沙群島均為珊瑚島嶼；[水产主要為](../Page/水产.md "wikilink")[海龜](../Page/海龜.md "wikilink")、[海參](../Page/海參.md "wikilink")、[金槍魚](../Page/金槍魚.md "wikilink")、[銀紋笛鯛](../Page/銀紋笛鯛.md "wikilink")、[鯊魚](../Page/鯊魚.md "wikilink")、[龍蝦](../Page/龍蝦.md "wikilink")、[梭子魚](../Page/梭魚.md "wikilink")、[墨魚和](../Page/墨魚.md "wikilink")[魷魚等各種熱帶海產](../Page/魷魚.md "wikilink")。南海的[金絲燕用](../Page/金絲燕.md "wikilink")[海藻和唾液做巢](../Page/海藻.md "wikilink")，這種巢就是珍貴的滋補品[燕窩](../Page/燕窩.md "wikilink")。

2018年有報導稱南海每年魚穫可達1,600萬噸\[10\]。

### 能源

南海海底[石油與](../Page/石油.md "wikilink")[天然氣](../Page/天然氣.md "wikilink")[可燃冰蘊藏豐富](../Page/可燃冰.md "wikilink")，據初步估算海底石油蘊藏量達二百億噸。其中，中方較早期開發的有位於鶯歌海（海南島以南南海水域）的[崖城13-1油氣田](../Page/崖城13-1油氣田.md "wikilink")，由[中國海洋石油及](../Page/中國海洋石油.md "wikilink")[BP合營](../Page/BP.md "wikilink")，向中海油及香港[中華電力供應油氣](../Page/中華電力.md "wikilink")。而越南亦有在南海西部水域建造鑽油台，早期與前蘇聯合作，後期則主要與[西班牙國家石油公司等外資合作](../Page/西班牙國家石油公司.md "wikilink")。[南中國海的一項天然氣項目於](../Page/南中國海.md "wikilink")[2014年首次投產](../Page/2014年.md "wikilink")。\[11\]

## 战略地位

南海為太平洋和印度洋之間重要[航道](../Page/航道.md "wikilink")，四周大部分為半島和島礁。

## 主權爭議

南海領土爭端包括[南沙群島主權爭議](../Page/南沙群島主權爭議.md "wikilink")、[西沙群島和](../Page/西沙群島.md "wikilink")[中沙群島的主權糾紛](../Page/中沙群島.md "wikilink")、[北部灣海上邊界問題以及其他地區的爭議](../Page/北部灣.md "wikilink")。後來有關位於印尼和馬來西亞附近的[納土納群島的主權歸屬問題也被歸類進該爭端範圍中](../Page/納土納群島.md "wikilink")。\[12\]

各個國家出於，希望控制自己所主張擁有主權的島嶼的實際控制權，其中包括島嶼附近的專屬經濟區和捕撈地帶。由於懷疑南中國海的油氣蘊藏十分豐富，當事國也紛紛加入爭奪行列。其他一些爭奪該地區主權的理由有重要的航運通道，戰略控制等。

[香格里拉對話是相關國家為解决該地區爭端和衝突的一個重要平臺](../Page/香格里拉對話.md "wikilink")\[13\]。則是各國間為協調亞太地區的安全問題的另外一個重要平臺\[14\]\[15\]。

## 參考文獻

## 外部链接

  -
  - [ASEAN and the South China Sea: Deepening
    Divisions](http://www.nbr.org/research/activity.aspx?id=262) Q\&A
    with Ian J. Storey (July 2012)

  - [Rising Tensions in the South China
    Sea](http://www.nbr.org/research/activity.aspx?id=151), June 2011
    Q\&A with Ian J. Storey

  - [The South China Sea on Google
    Earth](http://google-latlong.blogspot.com/2008/02/south-china-sea-project.html)
    - featured on Google Earth's Official Blog

  - [South China Sea Virtual
    Library](http://community.middlebury.edu/~scs/) - online resource
    for students, scholars and policy-makers interested in South China
    Sea regional development, environment, and security issues.

  - [Energy Information Administration - The South China
    Sea](https://web.archive.org/web/20101030031639/http://www.eia.doe.gov/emeu/cabs/South_China_Sea/Background.html)

  - [Tropical Research and Conservation Centre - The South China
    Sea](https://web.archive.org/web/20080522024651/http://www.tracc.org.my/Borneocoast/biogeography/SOUTH_CHINA_SEA.html)

  - [Reversing Environmental Degradation Trends in the South China Sea
    and Gulf of Thailand](http://www.unepscs.org/)

  - [UNEP/GEF South China Sea Knowledge
    Documents](http://www.unepscs.org/SCS_Documents/Download/14_-_South_China_Sea_Project_Knowledge_Documents.html)

  - [中国南海网](https://web.archive.org/web/20161215102433/http://www.thesouthchinasea.org/)

## 参见

  - [南海诸岛](../Page/南海诸岛.md "wikilink")
  - [南海问题](../Page/南海问题.md "wikilink")
      - [南沙群岛主权争议](../Page/南沙群岛主权争议.md "wikilink")

{{-}}

[Category:太平洋陸緣海](../Category/太平洋陸緣海.md "wikilink")
[南中國海](../Category/南中國海.md "wikilink")
[Category:有爭議的水域](../Category/有爭議的水域.md "wikilink")
[Category:台灣鄰海](../Category/台灣鄰海.md "wikilink")
[Category:香港海洋](../Category/香港海洋.md "wikilink")
[Category:澳門海域](../Category/澳門海域.md "wikilink")
[Category:廣東地理](../Category/廣東地理.md "wikilink")
[Category:廣西地理](../Category/廣西地理.md "wikilink")
[Category:海南地理](../Category/海南地理.md "wikilink")
[Category:中華民國海域](../Category/中華民國海域.md "wikilink")
[Category:中華人民共和國海域](../Category/中華人民共和國海域.md "wikilink")
[Category:越南海域](../Category/越南海域.md "wikilink")
[Category:柬埔寨海域](../Category/柬埔寨海域.md "wikilink")
[Category:泰國海域](../Category/泰國海域.md "wikilink")
[Category:馬來西亞海域](../Category/馬來西亞海域.md "wikilink")
[Category:新加坡海域](../Category/新加坡海域.md "wikilink")
[Category:印度尼西亞海域](../Category/印度尼西亞海域.md "wikilink")
[Category:汶萊海域](../Category/汶萊海域.md "wikilink")
[Category:菲律賓海域](../Category/菲律賓海域.md "wikilink")
[Category:中華民國爭議地區](../Category/中華民國爭議地區.md "wikilink")
[Category:中華人民共和國爭議地區](../Category/中華人民共和國爭議地區.md "wikilink")
[Category:越南爭議地區](../Category/越南爭議地區.md "wikilink")
[Category:菲律賓爭議地區](../Category/菲律賓爭議地區.md "wikilink")
[Category:馬來西亞爭議地區](../Category/馬來西亞爭議地區.md "wikilink")
[Category:印尼爭議地區](../Category/印尼爭議地區.md "wikilink")
[Category:中越邊界](../Category/中越邊界.md "wikilink")
[Category:中華民國-菲律賓邊界](../Category/中華民國-菲律賓邊界.md "wikilink")
[Category:印度尼西亞-馬來西亞邊界](../Category/印度尼西亞-馬來西亞邊界.md "wikilink")
[Category:印度尼西亞-越南邊界](../Category/印度尼西亞-越南邊界.md "wikilink")
[Category:馬來西亞-越南邊界](../Category/馬來西亞-越南邊界.md "wikilink")
[Category:中國水體](../Category/中國水體.md "wikilink")

1.

2.

3.  Tønnesson, Stein (2005). Locating the South China Sea. In Kratoska,
    Paul et al., eds. *Locating Southeast Asia: geographies of knowledge
    and politics of space*. Singapore: Singapore University Press. p.
    203-233.

4.

5.

6.
7.  Donald G. Groves, Lee M. Hunt: *Ocean World Encyclopedia.* McGraw
    Hill, 1980, ISBN 0-07-025010-3. Kapitel *South China Sea* (Seite
    356–358).

8.

9.  Hall, R., 2002. Cenozoic geological and plate tectonic evolution of
    SE Asia and the SW Pacific: computer-based reconstructions, model
    and animations. Journal of Asian Earth Sciences, 20(4), pp.353-431.

10.

11.

12.


13.

14.

15.