**Google文件編輯器**\[1\]（Google Docs
Editors），是[Google提供的網頁式辦公套件在线服務](../Page/Google.md "wikilink")，包含文書處理（**Google文件**）、電子試算表（**Google試算表**）、簡報演示（**Google簡報**）和互動表單（**Google表單**）。用戶可線上免費創建和編輯檔案，同時與其他用戶共同協作。

## 历史

Google文件来源于两个独立的产品，分别是**Writely**和**Google
Spreadsheets**。Writely是由软件公司[Upstartle创建的基于](../Page/Upstartle.md "wikilink")[Web的独立](../Page/Web.md "wikilink")[字处理软件](../Page/字处理.md "wikilink")，发布于2005年8月。\[2\]原先的功能包括[协同文字编辑套装以及](../Page/协同编辑软件.md "wikilink")[访问控制功能](../Page/访问控制.md "wikilink")。菜单、键盘快捷键和对话框的展现方式与使用[GUI的字处理软件](../Page/GUI.md "wikilink")，例如[Microsoft
Word和](../Page/Microsoft_Word.md "wikilink")[OpenOffice.org
Writer十分相似](../Page/OpenOffice.org_Writer.md "wikilink")。

  - 2006年3月9日，Google公司宣布并购Upstartle。\[3\]在并购发生的时候，Upstartle总共拥有4名员工，\[4\]与此同时Writely停止注册直到整体服务移入Google服务器的操作完成。\[5\]2006年8月，Writely向那些位于申请等待列表上的用户发送帐户邀请，紧接着于8月23日向公众开放。Writely依旧使用自己的用户系统直至2006年9月19日与[Google账户整合完成](../Page/Google账户.md "wikilink")。\[6\]Writely原先运行于[微软公司](../Page/微软公司.md "wikilink")[ASP.NET技术上](../Page/ASP.NET.md "wikilink")，使用[视窗操作系统](../Page/Microsoft_Windows.md "wikilink")。然而从2006年7月开始，Writely的服务器被发现已转移到基于[Linux的操作系统](../Page/Linux.md "wikilink")。\[7\]而与此同时，Google开发了Google
    Spreadsheets。这款产品引入了今天能在Google文件中看到的大多数功能。Google于2006年6月6日正式发布Spreadsheets，最初只有一小部分的用户能够使用Spreadsheets，基于先到先得的原则。随后限制性测试被替换为面向所有Google帐户拥有者的beta版本。
  - 2006年10月10日，Google將[Writely與旗下Google](../Page/Writely.md "wikilink")
    Spreadsheets整合為**Google Docs & Spreadsheets**。
  - 2007年2月，Google向[Google
    Apps中提供了Google文件服务](../Page/Google_Apps.md "wikilink")。
  - 2007年6月，Google在文件的首页的引入文件夹并以此取代了原先的labels，显示于网页的侧边栏。
  - 2007年4月17日，Google併購Tonic Systems公司，以取得其線上文檔管理相關技術。\[8\]
  - 2007年9月17日，Google加入了簡報演示功能。\[9\]
  - 2008年4月1日，Google文档开始通过[Google
    Gears提供离线编辑功能](../Page/Google_Gears.md "wikilink")。\[10\]
  - 2008年年6月，Google文档开始支援[PDF文件](../Page/PDF.md "wikilink")\[11\]。
  - 2009年3月25日，Google文档开始通过[SVG或](../Page/SVG.md "wikilink")[VML支援绘图](../Page/VML.md "wikilink")\[12\]。
  - 2009年7月6日，Google在其官方博客中声明Google文件将被去掉beta标记，成为正式版本。\[13\]
  - 2012年4月24日，Google推出[Google雲端硬碟](../Page/Google雲端硬碟.md "wikilink")，整合了原有的Google文件，結合了線上編輯文件、檔案共享，並提供5GB免費儲存容量。\[14\]。
  - 2012年10月31日，Google加入了互動表單功能。

## 特点

用户可以在Google文件中创建文档、電子製表和演示文件，也可以通过[web界面或](../Page/web.md "wikilink")[电子邮件导入到Google文件中](../Page/电子邮件.md "wikilink")。默认情况下这些文件保存在Google的服务器上，用户也可以将这些文件以多种格式（包括[OpenOffice](../Page/OpenOffice.md "wikilink")、[HTML](../Page/HTML.md "wikilink")、[PDF](../Page/PDF.md "wikilink")、[RTF](../Page/RTF.md "wikilink")、[文本文件](../Page/文本文件.md "wikilink")、[Word](../Page/Word.md "wikilink")）[下载到本地电脑中](../Page/下载.md "wikilink")。正在编辑的文件会被自动保存以防止数据丢失，编辑更新的历史也会被记录在案。为方便组织管理，文件可以存档或加上自定义的[标签](../Page/标签_\(元数据\).md "wikilink")。

Google文件支援多用户协同工作。文档可以同时被多个用户共享，開啟和编辑。在電子製表中，用户可以设置通过[电子邮件提醒任何指定区域的更改](../Page/电子邮件.md "wikilink")。程式支援[ISO标准的](../Page/ISO.md "wikilink")[开放文档格式格式](../Page/开放文档格式.md "wikilink")。支援流行的[Office的文件格式](../Page/Microsoft_Office.md "wikilink")，包括[.doc](../Page/.doc.md "wikilink")，[.docx](../Page/.docx.md "wikilink")，[.xls](../Page/.xls.md "wikilink")(行動應用程式已不支援編輯儲存)和[.xlsx](../Page/.xlsx.md "wikilink")。对[PDF格式仅支援上传和共享](../Page/PDF.md "wikilink")。\[15\]

## 限制

  - Google文件：上限為102萬字元。由文字文件轉換成的Google文件，大小上限則是50[MB](../Page/MB.md "wikilink")\[16\]。
  - Google試算表：上限為500萬個儲存格\[17\]。
  - Google簡報：上限為100[MB](../Page/MB.md "wikilink")\[18\]。

当前用户可以通过[Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink")，[Google
Chrome](../Page/Google_Chrome.md "wikilink")，[Internet Explorer
10和](../Page/Internet_Explorer_10.md "wikilink")[Safari等多种浏览器登入Google文件](../Page/Safari.md "wikilink")\[19\]。（尽管[Opera并不是官方声明支援的浏览器](../Page/Opera電腦瀏覽器.md "wikilink")，但它通过“伪装成Firefox”功能也能够使用Google文件的所有功能。）

## 安全

除了Google账户登录界面默认强制使用[SSL加密连接外](../Page/SSL.md "wikilink")，Google文件亦默认适用[加密的](../Page/加密.md "wikilink")[HTTPS链接](../Page/HTTPS.md "wikilink")。

随着越来越多的用户使用统一的[Google帐户](../Page/Google帐户.md "wikilink")，敏感文件的保密性也逐渐受到更广泛的关注。统一认证在提供一些便利的同时，也意味着对于安全性的一些威胁，例如无需再次密码校验便可访问Google文件。

在2009年3月10日，Google报告了Google文件的一个[程序错误](../Page/程序错误.md "wikilink")，一些私有文件被许可了不可预期的访问。Google相信在他们提供的服务中，只有0.05%的文档受到了这个程序错误的影响，并声明已经修正。\[20\]

## 移动访问

移动版的Google文件\[21\]允许用户在其[移动浏览器中浏览Google文件](../Page/移动浏览器.md "wikilink")。用户可以查看文档，也可以查看和编辑電子製表。Google文件针对[iPhone和](../Page/iPhone.md "wikilink")[Android的版本中](../Page/Google_Android.md "wikilink")，不仅为这些设备单独设计了交互界面，还包括功能强大的電子製表编辑功能和演示查看功能。另一方面，人们在任何移动设备上都无法查看或编辑的开放格式的数据库文件。

## 审查

## 参见

  - [EditGrid](../Page/EditGrid.md "wikilink")
  - [Zoho](../Page/Zoho.md "wikilink")

## 参考资料

## 外部連結

  - [Google文件](https://docs.google.com/document)

  - [Google試算表](https://docs.google.com/spreadsheets)

  - [Google簡報](https://docs.google.com/presentation)

  - [Google表單](https://docs.google.com/forms)

  - [Offical Google Docs Blog](http://googledocs.blogspot.com/)

  - [Tonic
    Systems](https://archive.is/20071219112155/http://www.tonicsystems.com/)（描述了一些Google购并的细节）

[Category:Google](../Category/Google.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")
[Category:网络应用程序](../Category/网络应用程序.md "wikilink")
[Category:云存储](../Category/云存储.md "wikilink")
[Category:数据同步](../Category/数据同步.md "wikilink")
[Category:文件共享服务](../Category/文件共享服务.md "wikilink")
[Category:在线备份服务](../Category/在线备份服务.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")

1.  [Google文件編輯器說明](https://support.google.com/docs?hl=zh-Hant#topic=1382883)

2.

3.  ["Official Google Blog: 'Writely
    so'"](http://googleblog.blogspot.com/2006/03/writely-so.html)

4.

5.
6.  ["The Writely Blog: 'Google Account Sign-in
    LIVE'"](http://writely.blogspot.com/2006/09/google-account-sign-in-live.html)

7.  ["Site report for
    www.writely.com"](http://toolbar.netcraft.com/site_report?url=http://www.writely.com)

8.  ["News from Google: 'Google has acquired Tonic
    Systems'"](https://googlepress.blogspot.com/2007/04/google-has-acquired-tonic-systems.html)

9.  ["Official Google Blog: 'Our feature
    presentation'"](http://googleblog.blogspot.com/2007/09/our-feature-presentation.html)

10. ["Official Google Docs Blog: 'Bringing cloud with
    you'"](http://googledocs.blogspot.com/2008/03/bringing-cloud-with-you.html)

11. {{ cite news | author = Kristen Nicole| title = "Google Docs Adds
    PDF Uploads and Previews" | date = June 13, 2008 | publisher =
    Mashable | url = <http://mashable.com/2008/06/13/google-docs-pdf/> |
    accessdate = 2008-06-14 | language = en}}

12. ["Official Google Docs Blog: 'Drawing on your creativity in
    Docs'"](http://googledocs.blogspot.com/2009/03/drawing-on-your-creativity-in-docs.html)

13.

14. [Introducing Google
    Drive](http://googleblog.blogspot.de/2012/04/introducing-google-drive-yes-really.html)

15. ["Google文件帮助：'上传和导出：上传文件'"](http://docs.google.com/support/bin/answer.py?answer=50092)

16. ["Google文件帮助：'了解Google文档：大小限制'"](http://docs.google.com/support/bin/answer.py?answer=37603)

17.
18.
19. ["文件編輯器說明：'系統需求和瀏覽器'"](https://support.google.com/docs/answer/2375082)

20. [Google software bug shared private online
    documents](http://www.breitbart.com/article.php?id=CNG.54c3200989573ae4c9282658f91276df.481&show_article=1)
    , AFP, March 10, 2009

21. [Google文件移动版](http://docs.google.com/m)