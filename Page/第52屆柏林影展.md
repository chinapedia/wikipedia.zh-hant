**第52届柏林影展**在[德國](../Page/德國.md "wikilink")[柏林當地時間](../Page/柏林.md "wikilink")2002年2月6日到2月16日舉行，[印度](../Page/印度.md "wikilink")[導演](../Page/導演.md "wikilink")[米拉·奈兒擔任評審團主席](../Page/米拉·奈兒.md "wikilink")。
[日本導演](../Page/日本.md "wikilink")[宮崎駿執導的](../Page/宮崎駿.md "wikilink")《[神隱少女](../Page/神隱少女.md "wikilink")》與[英國導演](../Page/英國.md "wikilink")[保羅·葛林葛瑞斯執導的](../Page/保羅·葛林葛瑞斯.md "wikilink")《》
共同獲得金熊獎，《[神隱少女](../Page/神隱少女.md "wikilink")》成為史上首部獲得該獎項的動畫電影，也是三大影展的首次。

## 評審團

  - [米拉·奈兒](../Page/米拉·奈兒.md "wikilink")：導演。(評審團主席)

## 正式競賽

<table style="width:94%;">
<colgroup>
<col style="width: 27%" />
<col style="width: 22%" />
<col style="width: 18%" />
<col style="width: 27%" />
</colgroup>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>原文片名</p></th>
<th><p>導演</p></th>
<th><p>出品國</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>《<a href="../Page/八美圖.md" title="wikilink">八美圖</a>》</p></td>
<td><p><em>8 femmes</em></p></td>
<td><p><a href="../Page/法蘭索瓦·歐容.md" title="wikilink">法蘭索瓦·歐容</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Amen.</em></p></td>
<td><p><a href="../Page/科斯塔·加夫拉斯.md" title="wikilink">科斯塔·加夫拉斯</a></p></td>
<td><p>、、、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Baader</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>나쁜 남자</em></p></td>
<td><p><a href="../Page/金基德.md" title="wikilink">金基德</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Beneath Clouds</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Bloody Sunday</em></p></td>
<td><p><a href="../Page/保羅·葛林葛瑞斯.md" title="wikilink">保羅·葛林葛瑞斯</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Brucio nel vento</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《布麗姬特》</p></td>
<td><p><em>Bridget</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Der Felsen</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Halbe Treppe</em></p></td>
<td><p><a href="../Page/安卓·爵森.md" title="wikilink">安卓·爵森</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Heaven</em></p></td>
<td><p><a href="../Page/湯姆·提克威爾.md" title="wikilink">湯姆·提克威</a></p></td>
<td><p>、、、、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Iris</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Små ulykker</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>KT</em></p></td>
<td></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Laissez-passer</em></p></td>
<td><p><a href="../Page/貝特朗·塔維涅.md" title="wikilink">貝特朗·塔維涅</a></p></td>
<td><p>、、</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/擁抱豔陽天.md" title="wikilink">擁抱豔陽天</a>》</p></td>
<td><p><em>Monster's Ball</em></p></td>
<td><p><a href="../Page/馬克·福斯特.md" title="wikilink">馬克·福斯特</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《》</p></td>
<td><p><em>Lundi matin</em></p></td>
<td><p><a href="../Page/奧達·伊奧塞里安尼.md" title="wikilink">奧達·伊奧塞里安尼</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Piedras</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/天才一族.md" title="wikilink">天才一族</a>》</p></td>
<td><p><em>The Royal Tenenbaums</em></p></td>
<td><p><a href="../Page/魏斯·安德森.md" title="wikilink">魏斯·安德森</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>The Shipping News</em></p></td>
<td><p><a href="../Page/莱塞·霍尔斯道姆.md" title="wikilink">萊思·霍斯壯</a></p></td>
<td><p>、</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/神隱少女.md" title="wikilink">神隱少女</a>》</p></td>
<td><p><em>千と千尋の神隠し</em></p></td>
<td><p><a href="../Page/宮崎駿.md" title="wikilink">宮崎駿</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《》</p></td>
<td><p><em>Δεκαπενταύγουστος</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《誘惑》</p></td>
<td><p><em>Kísértések</em></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 得獎名單

以下為本屆正式競賽單元得獎名單：

  - [金熊獎](../Page/金熊獎.md "wikilink")：
      - [宮崎駿](../Page/宮崎駿.md "wikilink") -
        《[神隱少女](../Page/神隱少女.md "wikilink")》
      - [保羅·葛林葛瑞斯](../Page/保羅·葛林葛瑞斯.md "wikilink") - 《》
  - [評審團特別銀熊獎](../Page/評審團大獎銀熊獎.md "wikilink")：[安卓·爵森](../Page/安卓·爵森.md "wikilink")
    - 《》
  - [最佳導演銀熊獎](../Page/最佳導演銀熊獎.md "wikilink")：[奧達·伊奧塞里安尼](../Page/奧達·伊奧塞里安尼.md "wikilink")
    - 《》
  - [最佳女演員銀熊獎](../Page/最佳女演員銀熊獎.md "wikilink")：[荷莉·貝瑞](../Page/荷莉·貝瑞.md "wikilink")
    - 《[擁抱豔陽天](../Page/擁抱豔陽天.md "wikilink")》
  - [最佳男演員銀熊獎](../Page/最佳男演員銀熊獎.md "wikilink")： - 《》
  - [亞佛雷德鮑爾獎](../Page/亞佛雷德鮑爾獎.md "wikilink")： - 《》
  - [特別藝術成就銀熊獎](../Page/傑出藝術貢獻銀熊獎.md "wikilink")：全體女演員 -
    《[八美圖](../Page/八美圖.md "wikilink")》
  - [最佳音樂銀熊獎](../Page/傑出藝術貢獻銀熊獎.md "wikilink") ： - 《》
  - [費比西國際影評人獎](../Page/柏林影展費比西國際影評人獎.md "wikilink")：[奧達·伊奧塞里安尼](../Page/奧達·伊奧塞里安尼.md "wikilink")
    - 《》

## 外部連結

  - [柏林影展官方網站](http://www.berlinale.de/en/HomePage.html)

[52](../Category/柏林电影节.md "wikilink")
[Category:2002年電影](../Category/2002年電影.md "wikilink")