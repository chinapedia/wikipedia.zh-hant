**陋龍**（[學名](../Page/學名.md "wikilink")"Ugrosaurus"）是[角龍亞科](../Page/角龍亞科.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[上白堊紀的](../Page/上白堊紀.md "wikilink")[麥斯特里希特階](../Page/麥斯特里希特階.md "wikilink")。牠的[化石](../Page/化石.md "wikilink")（編號UCMP128561）被發現於[美國](../Page/美國.md "wikilink")[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[海爾河組](../Page/海爾河組.md "wikilink")，包括了[前上頜骨](../Page/前上頜骨.md "wikilink")、鼻角及一些獨立的化石碎片。模式屬是**奧氏陋龍**（*U.
olsoni*），是由Cobabe及Fastovsky在1986年所敘述、命名\[1\]。

陋龍是[草食性的恐龍](../Page/草食性.md "wikilink")，被估計身長約7公尺。由於牠的化石稀少、都是碎片，所以被認為是[可疑名稱](../Page/可疑名稱.md "wikilink")\[2\]。另有研究提出陋龍不是一個獨立的屬，應被歸入[三角龍屬中](../Page/三角龍屬.md "wikilink")\[3\]。

## 參考資料

## 外部連結

  - [恐龍博物館──陋龍](https://web.archive.org/web/20071109042857/http://www.dinosaur.net.cn/museum/Ugrosaurus.htm)

[Category:無效的恐龍](../Category/無效的恐龍.md "wikilink")

1.
2.
3.