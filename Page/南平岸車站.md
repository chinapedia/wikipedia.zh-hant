**南平岸車站**（）是一位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")[豐平區平岸](../Page/豐平區.md "wikilink")4条13丁目，隸屬於[札幌市交通局的](../Page/札幌市交通局.md "wikilink")[地下鐵](../Page/地下鐵.md "wikilink")[車站](../Page/鐵路車站.md "wikilink")。南平岸車站是[札幌市營地下鐵南北線的沿線車站之一](../Page/札幌市營地下鐵南北線.md "wikilink")，車站編號N13。

在地下鐵南北線通車之初，本站原名為**靈園前**，但在1994年時原本的平岸靈園之火葬場遷址去他處，也同時將車站名稱改為**南平岸**的現名，是札幌市營地下鐵開始運作之後第一個改名的車站。

## 車站結構

1面2線島式月台的高架車站。1樓為閘口、2樓為月台。

### 月台配置

| 月台    | 路線                                                                                                                             | 目的地                                 |
| ----- | ------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------- |
| **1** | [Subway_SapporoNamboku.svg](https://zh.wikipedia.org/wiki/File:Subway_SapporoNamboku.svg "fig:Subway_SapporoNamboku.svg") 南北線 | [真駒內方向](../Page/真駒內站.md "wikilink") |
| **2** | [大通](../Page/大通站_\(北海道\).md "wikilink")、[札幌](../Page/札幌站_\(札幌市營地下鐵\).md "wikilink")、[麻生方向](../Page/麻生站.md "wikilink")          |                                     |

## 相鄰車站

  - [ST_Logo.svg](https://zh.wikipedia.org/wiki/File:ST_Logo.svg "fig:ST_Logo.svg")
    札幌市營地下鐵
    [Subway_SapporoNamboku.svg](https://zh.wikipedia.org/wiki/File:Subway_SapporoNamboku.svg "fig:Subway_SapporoNamboku.svg")
    南北線
      -
        [平岸](../Page/平岸站_\(札幌市\).md "wikilink")（N12）－**南平岸（N13）**－[澄川](../Page/澄川站.md "wikilink")（N14）

[NamiHiragishi](../Category/日本鐵路車站_Mi.md "wikilink")
[Category:豐平區鐵路車站](../Category/豐平區鐵路車站.md "wikilink")
[Category:南北線車站 (札幌市營地下鐵)](../Category/南北線車站_\(札幌市營地下鐵\).md "wikilink")
[Category:1971年啟用的鐵路車站](../Category/1971年啟用的鐵路車站.md "wikilink")