**袁彌明**（；）是[袁彌明生活百貨](../Page/袁彌明生活百貨.md "wikilink")（[彌明生活百貨前身](../Page/彌明生活百貨.md "wikilink")）創辦人，曾參與電影及電視幕前演出。曾經擔任任[香港政黨](../Page/香港政黨.md "wikilink")[人民力量主席](../Page/人民力量.md "wikilink")、人民力量執委會委員、[電訊盈科](../Page/電訊盈科.md "wikilink")[見習](../Page/練習生.md "wikilink")[經理](../Page/經理.md "wikilink")、[時事評論員](../Page/時事評論員.md "wikilink")、[節目主持](../Page/節目主持.md "wikilink")、[作家](../Page/作家.md "wikilink")、[藝員](../Page/藝員.md "wikilink")、[模特等](../Page/模特.md "wikilink")，早年於[香港](../Page/香港.md "wikilink")[赤柱](../Page/赤柱.md "wikilink")[聖士提反書院附屬小學](../Page/聖士提反書院附屬小學.md "wikilink")（1992年畢業）以及[聖士提反書院畢業](../Page/聖士提反書院.md "wikilink")（1997年中五），之後到美國[麻薩諸塞州Northfield](../Page/麻薩諸塞州.md "wikilink")
Mount Hermon
School就讀預科（1999年畢業），並入讀[美國](../Page/美國.md "wikilink")[波士頓](../Page/波士頓.md "wikilink")[塔夫茨大學](../Page/塔夫茨大學.md "wikilink")[經濟系](../Page/經濟學.md "wikilink")，於2003年畢業，祖籍[浙江](../Page/浙江.md "wikilink")[寧波](../Page/寧波.md "wikilink")[鄞縣](../Page/鄞縣.md "wikilink")，是[西門袁氏](../Page/鄞县袁氏.md "wikilink")[家族的後人](../Page/家族.md "wikilink")，祖父是[袁勃](../Page/袁勃.md "wikilink")，父親是[袁弓夷](../Page/袁弓夷.md "wikilink")\[1\]\[2\]。新民黨政策總裁[袁彌昌是她的胞兄](../Page/袁彌昌.md "wikilink")，新民黨常務副主席[容海恩則是她的嫂子](../Page/容海恩.md "wikilink")。

在2005年參加[香港小姐競選](../Page/香港小姐.md "wikilink")，參選期間被視為三甲大熱門\[3\]，於準決賽前獲得「旅遊大使獎」\[4\]，並於決賽晉身五強但大熱倒灶三甲不入\[5\]。2007年離開[無綫電視](../Page/無綫電視.md "wikilink")，其後為報社和雜誌社寫專欄和錄[YouTube短片教美容](../Page/YouTube.md "wikilink")，迅速創立護膚品和健康食品專門店「袁彌明生活百貨」([網站](http://www.mimingmart.com/))\[6\]。2010年因不滿[民主黨支持政改方案而成立政治組織](../Page/民主黨_\(香港\).md "wikilink")[選民力量並成為](../Page/選民力量.md "wikilink")[人民力量執行委員](../Page/人民力量.md "wikilink")。2012年與[陳志全](../Page/陳志全.md "wikilink")（慢必）合組名單代表人民力量參選[立法會](../Page/香港立法会.md "wikilink")（[新界東](../Page/新界東.md "wikilink")）地區直選，獲得38,042票，令拍檔陳志全成功當選。

2016年9月10日，袁彌明宣佈退任人民力量主席，原因過去在社區解釋議會拉布，以及推動全民退保的工作做得不理想\[7\]\[8\]。

2018年，於網上撰寫文章直言女人追求虛榮是一場災難。並表示她們容易被表面風光支配自我價值\[9\]。

亦不時分享個人護膚心得\[10\]。

## 簡歷

袁彌明在學時曾兼任專業[模特兒](../Page/模特兒.md "wikilink")，1997年她參與電影《[初戀無限Touch](../Page/初戀無限Touch.md "wikilink")》的演出，飾演[陳曉東的妹妹](../Page/陳曉東.md "wikilink")、[許冠英的女兒](../Page/許冠英.md "wikilink")，是個有同性戀傾向的女中學生。1998年，她參演《PR
Girls青春援助交際》\[11\]。

2005年，由前歌手好友[陳苑淇](../Page/陳苑淇.md "wikilink")（前稱陳逸璇）提名參選[香港小姐選舉](../Page/香港小姐.md "wikilink")，並進入最後五強，最終獲得「旅遊大使獎」\[12\]。

2007年離開TVB後，在《[蘋果日報](../Page/蘋果日報.md "wikilink")》和《[忽然一周](../Page/忽然一周.md "wikilink")》撰寫專欄。其後又自拍[化妝美容短片上網](../Page/化妝.md "wikilink")，大收宣傳之效，於是在2009年8月創立護膚品和健康食品專門店「袁彌明生活百貨」\[13\]。

2008年6月，推出親自執筆的第一本著作《反．串》。同時，夥拍[蕭定一及](../Page/蕭定一.md "wikilink")[愛明於](../Page/愛明.md "wikilink")[MyRadio主持娛樂新聞評論節目](../Page/MyRadio.md "wikilink")《[大娛樂家](../Page/大娛樂家.md "wikilink")》，亦曾經主持[MyRadio另一個女性節目](../Page/MyRadio.md "wikilink")《[Lady
First](../Page/Lady_First.md "wikilink")》，並且在[亞洲電視客席主持節目](../Page/亞洲電視.md "wikilink")《[生活著數台](../Page/生活著數台.md "wikilink")》。另外她及她的兄長[袁彌昌亦有時以時事評論員身份擔任](../Page/袁彌昌.md "wikilink")[香港電台時事評論節目](../Page/香港電台.md "wikilink")《[左右紅藍綠](../Page/左右紅藍綠.md "wikilink")》針砭時弊\[14\]\[15\]。

2010年9月起，為[香港人網主持時事評論節目](../Page/香港人網.md "wikilink")《[網想最大黨](../Page/網想最大黨.md "wikilink")》。同時，她與[陳志全](../Page/陳志全.md "wikilink")（慢必）、[林雨陽](../Page/林雨陽.md "wikilink")、[劉嘉鴻](../Page/劉嘉鴻.md "wikilink")（劉嗡）、[歐陽英傑](../Page/歐陽英傑.md "wikilink")（星屑醫生）組織非政黨社運組織——[選民力量](../Page/選民力量.md "wikilink")，準備在2011年[區議會選舉中狙擊](../Page/香港區議會.md "wikilink")[民主黨](../Page/民主黨_\(香港\).md "wikilink")。\[16\]但她在提名期前因私人理由棄選。

2012年5月，袁彌明在香港人網宣佈放棄[美國國籍以便參選](../Page/美國國籍.md "wikilink")[立法會](../Page/立法會.md "wikilink")。7月15日在「人民力量陳志全袁彌明見證大會」上宣布和[陳志全](../Page/陳志全.md "wikilink")(慢必)合組名單參選[2012年香港立法會選舉的新界東地區直選](../Page/2012年香港立法會選舉.md "wikilink")。\[17\]該參選宣言一星期有接近十萬收看\[18\]\[19\]。該名單於在同選舉中取得一席，成功把名單首位的陳志全送入立法會。

2013年7月17日，袁彌明當選[人民力量主席](../Page/人民力量.md "wikilink")，接替[劉嘉鴻](../Page/劉嘉鴻.md "wikilink")。

袁彌明已婚，丈夫為[選民力量創黨主席](../Page/選民力量.md "wikilink")、[謎米香港前行政總裁](../Page/謎米香港.md "wikilink")[林雨陽](../Page/林雨陽.md "wikilink")。

### YouTube

袁彌明也會製作[YouTube短片教人化妝](../Page/YouTube.md "wikilink")，而在[2012年香港立法會選舉前](../Page/2012年香港立法會選舉.md "wikilink")，也製作了數集「[政治BB班](https://www.youtube.com/watch?v=kVRBLG-p8MI&list=PLAF0EF5C30B41AF92)」影片，簡單介紹香港政治狀況。第一集一星期亦有接近十萬收看
\[20\]

### 與TVB關係

2007年10月，袁彌明公開被[無綫電視](../Page/電視廣播有限公司.md "wikilink")（TVB）解約\[21\]。她曾接受了電台訪問透露被香港電視廣播有限公司製作資源部總監[樂易玲強迫簽約](../Page/樂易玲.md "wikilink")，因拒絕簽約而與TVB關係變得惡劣，有指合約內容儼如不平等條約，極度苛刻，內容是簽一份3年合約，每年騷錢只加100元，沒有底薪，TVB更是她的全球獨家經理人。她本想拿合約返家與家人商量，豈料有人跟她說「這份合約不能影印，不能拿出房門口」和「你一定要簽這份合約，否則不能走出這個房間」等恐嚇性字句，她自覺猶如被威嚇，所以她不要這種經理人。後來與家人商量後拒絕簽約，發生很多不愉快事件。由於TVB指與袁彌明的合約到2009才屆滿，袁向無綫發出律師信問題才獲解決。

袁彌明經常在公開場合頂撞無綫，例如在2012年5月，政府正準備發出3個新的免費電視牌照，惟在該時候無綫曾建議政府重新審視發牌的必要性，並質疑香港[廣告市場是否足夠容納](../Page/廣告.md "wikilink")3家新的免費電視台。5月27日，袁與慢必公開在[旺角街頭高舉紙牌](../Page/旺角.md "wikilink")，促請政府盡快發牌，讓市民享有更多免費電視頻道節目。2人公開力斥無綫獨攬市場，迫市民看TVB製作的「垃圾劇」，指劇集質素差。\[22\]袁彌明與無綫鬧翻多年，因此一直被無綫列入黑名單，不過在2017年她接受了[無綫財經台專訪](../Page/無綫財經台.md "wikilink")，談論網上營商之道。\[23\]

2013年，袁彌明批評無綫在6時半新聞抽起「[陳凱琳握](../Page/陳凱琳.md "wikilink")17萬選票撐[普選促特首聽](../Page/普選.md "wikilink")[民意](../Page/民意.md "wikilink")」的訪問是[自我審查](../Page/自我審查.md "wikilink")。\[24\]

### 社會責任

袁彌明披露，當年參選[港姐時](../Page/港姐.md "wikilink")，無線多番提醒她們不講政治，以免惹起爭議。但她強調港姐不是花瓶，應該肩負社會責任，亦希望港姐要充份利用這個機會做些有益的事情。「港姐要履行的社會責任是很大的，不是只是說一個漂漂亮亮的女子做了冠軍，然後去拍戲，但對社會的貢獻是不夠。她選出來都有很多傳媒關注，我想她的言論會影響很多人，我想不要浪費這個機會了。」\[25\]

### 參選立法會

2012年5月，袁彌明在香港人網宣佈放棄美國國籍。7月15日在「人民力量陳志全袁彌明見證大會」上和陳志全（慢必）宣布參選2012年立法會選舉。在立法會選舉前，袁彌明製作了幾集「政治BB班」影片，簡單介紹香港政治狀況。第一集一星期亦有接近十萬收看。

### 區議會補選

[Erica_Mi-ming_Yuen.jpg](https://zh.wikipedia.org/wiki/File:Erica_Mi-ming_Yuen.jpg "fig:Erica_Mi-ming_Yuen.jpg")社區會堂演說\]\]
[支持袁彌明參選海怡西.JPG](https://zh.wikipedia.org/wiki/File:支持袁彌明參選海怡西.JPG "fig:支持袁彌明參選海怡西.JPG")
[馮煒光加入香港政府當](../Page/馮煒光.md "wikilink")[新聞統籌專員後](../Page/新聞統籌專員.md "wikilink")，他留下的[南區](../Page/南區.md "wikilink")[海怡西區議會席位](../Page/海怡西區.md "wikilink")，在2014年3月下旬進行補選，身為人民力量主席的袁彌明參與競逐。結果由建制派新民黨的新丁[陳家珮勝出](../Page/陳家珮.md "wikilink")。而[民主黨的](../Page/民主黨.md "wikilink")[單仲偕得票最少](../Page/單仲偕.md "wikilink")，敗了給袁彌明，承認慘敗。袁彌明得悉結果後，興奮得跳起來，喜極而泣。原因並不是她當選，而是她空降該區，已得到一千多票、得票率
26%。有學者分析，民主黨這次補選中慘敗，是因對手袁彌明知名度高又勤力，單仲偕在整個選舉工程不投入，民主黨政黨效應老化以及有太多負面新聞纏身。人民力量高調宣佈，雖然敗選，卻證明了「公民提名」勝利了，「進步民主派大獲全勝，公民提名今次係贏咗」。\[26\]

## 感情生活

2010年3月，袁彌明和男友聯名在灣仔買樓，並透露男友未夠30歲。\[27\]

2011年3月，袁彌明和圈外從事金融界的男友拍拖三年後，因性格不合分手。\[28\]

2014年12月27日與拍拖一年多，曾任職「謎網」行政總裁，公民黨創黨成員的男友[林雨陽舉行婚禮](../Page/林雨陽.md "wikilink")。

## 參演劇集

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/滙通天下_(電視劇).md" title="wikilink">滙通天下</a></p></td>
<td><p>安娜．屠波夫<br />
(<a href="../Page/俄國.md" title="wikilink">俄國公使之</a><a href="../Page/繼女.md" title="wikilink">繼女</a>)</p></td>
<td><p>與<a href="../Page/陳豪.md" title="wikilink">陳　豪</a>、<a href="../Page/馬浚偉.md" title="wikilink">馬浚偉</a>、<a href="../Page/郭羨妮.md" title="wikilink">郭羨妮</a>、<a href="../Page/楊怡.md" title="wikilink">楊　怡合演</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/肥田囍事.md" title="wikilink">肥田囍事</a></p></td>
<td><p>Candy</p></td>
<td><p>與<a href="../Page/許志安.md" title="wikilink">許志安</a>、<a href="../Page/胡杏兒.md" title="wikilink">胡杏兒</a>、<a href="../Page/胡諾言.md" title="wikilink">胡諾言</a>、<a href="../Page/李詩韻.md" title="wikilink">李詩韻合演</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/同事三分親.md" title="wikilink">同事三分親</a></p></td>
<td><p>Karen（第77集）</p></td>
<td><p>與<a href="../Page/蔡淇俊.md" title="wikilink">蔡淇俊</a>、<a href="../Page/王祖藍.md" title="wikilink">王祖藍</a>、<a href="../Page/陳自瑤.md" title="wikilink">陳自瑤</a>、<a href="../Page/歐錦棠.md" title="wikilink">歐錦棠合演</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/通天幹探.md" title="wikilink">通天幹探</a></p></td>
<td><p>戴美芬 (大長芬)<br />
(鍾國邦之下屬)</p></td>
<td><p>與<a href="../Page/元彪.md" title="wikilink">元　彪</a>、<a href="../Page/陳豪.md" title="wikilink">陳　豪</a>、<a href="../Page/黎姿.md" title="wikilink">黎　姿</a>、<a href="../Page/蒙嘉慧.md" title="wikilink">蒙嘉慧</a>、<a href="../Page/邵美琪.md" title="wikilink">邵美琪合演</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p>沒有牆的世界<br />
第5集<br />
說不出的願望</p></td>
<td><p>紹琴</p></td>
<td><p>與<a href="../Page/丁羽.md" title="wikilink">丁　羽</a>、<a href="../Page/康華.md" title="wikilink">康　華</a>、<a href="../Page/阮德鏘.md" title="wikilink">阮德鏘</a>、<a href="../Page/林偉.md" title="wikilink">林　偉合演</a></p></td>
</tr>
<tr class="odd">
<td><p>一屋住家人</p></td>
<td><p>Jessie</p></td>
<td><p>|與<a href="../Page/周俊偉.md" title="wikilink">周俊偉合演</a>[29]</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參演電影

|        |                                              |        |        |
| ------ | -------------------------------------------- | ------ | ------ |
| **年份** | **電影名**                                      | **角色** | **備註** |
| 1997年  | [算死草](../Page/算死草.md "wikilink")             | 妹仔     |        |
|        |                                              |        |        |
| 1997年  | [愛你愛到殺死你](../Page/愛你愛到殺死你.md "wikilink")     | 歌迷     |        |
|        |                                              |        |        |
| 1997年  | [初戀無限touch](../Page/初戀無限touch.md "wikilink") | 陳曉東妹妹  |        |
|        |                                              |        |        |
| 2002年  | [男歌女唱](../Page/男歌女唱.md "wikilink")           |        |        |

## 主持電視節目

|             |                                      |                                          |           |
| ----------- | ------------------------------------ | ---------------------------------------- | --------- |
| **年份**      | **節目名稱**                             | **電視台**                                  | **備註**    |
| 2006年       | [娛樂直播](../Page/娛樂直播.md "wikilink")   | [無線電視](../Page/電視廣播有限公司.md "wikilink")   |           |
| 2008年       | [左右紅藍綠](../Page/左右紅藍綠.md "wikilink") | [香港電台](../Page/香港電台.md "wikilink")       |           |
| 2008年       | [生活着數台](../Page/生活着數台.md "wikilink") | [亞洲電視](../Page/亞洲電視.md "wikilink")       |           |
| 2008年至2009年 | [星期日大班](../Page/星期日大班.md "wikilink") | [now香港台](../Page/now香港台.md "wikilink")   |           |
| 2009年       | [怒人甲乙丙](../Page/怒人甲乙丙.md "wikilink") | [有線娛樂台](../Page/有線娛樂台.md "wikilink")     |           |
| 2009年       | [城市論壇](../Page/城市論壇.md "wikilink")   | [香港電台](../Page/香港電台.md "wikilink")       |           |
| 2010年       | [精算俏佳人](../Page/精算俏佳人.md "wikilink") | [有線娛樂台](../Page/有線娛樂台.md "wikilink")     |           |
| 2012年至2013年 | [周日不講理](../Page/周日不講理.md "wikilink") | [有線財經資訊台](../Page/有線財經資訊台.md "wikilink") | 與多位名人輪流主持 |
| 2013年至今     | [魔鏡我最靚](../Page/魔鏡我最靚.md "wikilink") | [有線奇妙台](../Page/有線電視娛樂台.md "wikilink")   |           |

## 參與電視節目

|                                      |                                        |                                        |           |
| ------------------------------------ | -------------------------------------- | -------------------------------------- | --------- |
| **年份**                               | **節目名稱**                               | **電視台**                                | **備註**    |
| 2013年                                | [賺大錢](../Page/賺大錢.md "wikilink")       | [有線奇妙台](../Page/有線電視娛樂台.md "wikilink") | 嘉賓        |
| [一念之間2](../Page/一念之間2.md "wikilink") | [香港電台](../Page/香港電台.md "wikilink")     | 於第九集飾演Winnie                           |           |
| 2018年                                | [火速救兵IV](../Page/火速救兵IV.md "wikilink") | [香港電台](../Page/香港電台.md "wikilink")     | 於單元三飾演陸紫明 |

## 主持電台節目

|        |                                      |                                          |               |
| ------ | ------------------------------------ | ---------------------------------------- | ------------- |
| **年份** | **節目名稱**                             | **電台**                                   | **備註**        |
| 2008年  | [大娛樂家](../Page/大娛樂家.md "wikilink")   | [MyRadio](../Page/MyRadio.md "wikilink") | 於2010年6月已暫停主持 |
| 2010年  | [網想最大黨](../Page/網想最大黨.md "wikilink") | [香港人網](../Page/香港人網.md "wikilink")       |               |

## 個人著作

  - 《反．串》，袁彌明著／天地圖書有限公司，2008年6月初版 ISBN 978-988-211-924-6。
  - 《袁彌明28日魔幻煥膚全體驗》，ISBN 978-988-192-185-7。

## 參見

  - [彌明生活百貨](../Page/彌明生活百貨.md "wikilink")

## 参考资料

[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[M](../Category/袁勃家族.md "wikilink")
[Category:2005年度香港小姐競選參賽者](../Category/2005年度香港小姐競選參賽者.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:藝人出身的政治人物](../Category/藝人出身的政治人物.md "wikilink")
[Mi](../Category/袁姓.md "wikilink")
[Category:人民力量成員](../Category/人民力量成員.md "wikilink")
[Category:香港泛民主派人士](../Category/香港泛民主派人士.md "wikilink")
[Category:西门袁氏](../Category/西门袁氏.md "wikilink")
[Category:香港宁波人](../Category/香港宁波人.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:聖士提反書院校友](../Category/聖士提反書院校友.md "wikilink")
[Yuen](../Category/香港商人.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:香港YouTuber](../Category/香港YouTuber.md "wikilink")
[Category:放棄美國國籍人士](../Category/放棄美國國籍人士.md "wikilink")

1.

2.

3.  [港姐今晚決戰 整容傳聞纏身
    克勤踩多腳：唔認得(8)葉翠翠](http://the-sun.on.cc/channels/ent/20050820/20050819234150_0003.html)

4.  [組圖：港姐首個獎項產生
    袁彌明獲旅遊大使獎](http://ent.sina.com.cn/s/h/2005-07-11/1012776203.html)

5.  [港姐生電王是非不遜葉翠翠
    袁彌明父欠債3億兩度清盤](https://hk.entertainment.appledaily.com/entertainment/daily/article/20050904/5195120)

6.  ，上傳日期：2009年8月3日。

7.  [袁彌明辭任人民力量主席　陳志全暫代職務](https://hk.news.yahoo.com/%E8%A2%81%E5%BD%8C%E6%98%8E%E8%BE%AD%E4%BB%BB%E4%BA%BA%E6%B0%91%E5%8A%9B%E9%87%8F%E4%B8%BB%E5%B8%AD-%E9%99%B3%E5%BF%97%E5%85%A8%E6%9A%AB%E4%BB%A3%E8%81%B7%E5%8B%99-113300475.html)[星島日報](../Page/星島日報.md "wikilink")
    2016年9月10日

8.  [人民力量明宣佈人事變動　傳袁彌明辭任主席](https://www.thestandnews.com/politics/%E4%BA%BA%E6%B0%91%E5%8A%9B%E9%87%8F%E6%98%8E%E5%AE%A3%E4%BD%88%E4%BA%BA%E4%BA%8B%E8%AE%8A%E5%8B%95-%E5%82%B3%E8%A2%81%E5%BD%8C%E6%98%8E%E8%BE%AD%E4%BB%BB%E4%B8%BB%E5%B8%AD/)[立場新聞](../Page/立場新聞.md "wikilink")
    2016年9月9日

9.  [虛榮女的命運！袁彌明：追求虛榮的人，是我最看不起的。](https://www.mings-fashion.com/%E8%A2%81%E5%BD%8C%E6%98%8E-%E7%B5%90%E5%A9%9A-%E6%84%9B%E6%83%85-7043/)

10. [當時裝編輯遇上袁彌明　敏感肌膚出trip急救法](https://www.mings-fashion.com/%E8%A2%81%E5%BD%8C%E6%98%8E-%E7%9A%AE%E8%86%9A%E6%95%8F%E6%84%9F-%E6%BF%95%E7%96%B9-11773/)

11. [PR Girls青春援助交際
    (1998)](http://hkmdb.com/db/movies/view.mhtml?id=8393&display_set=big5)
    香港影庫

12.

13. [袁彌明生活百貨](http://www.mimingmart.com/)

14. [短加長減，賠了溢價又折客](http://www.rthk.org.hk/rthk/tv/pentaprismII/20081208.html)

15. [評港府處理滯泰港人事件](http://www.rthk.org.hk/rthk/tv/pentaprismII/20081209.html)

16. [選民力量](http://powervoters.wordpress.com/)

17. [新界東競選網站](http://www.pp2012.hk/PW/nte.php)

18. [袁彌明立法會（新界東）參選宣言](http://www.youtube.com/watch?v=EkDNBataCp4&feature=related)

19. [新界東競選網站](http://www.pp2012.hk/PW/nte.php)

20. [政治BB班](http://www.youtube.com/playlist?list=PL2147CA4054D5C149&feature=plcp)

21.

22.

23.
24. [「普選港姐」17萬票vs梁振英689票](http://www.epochtimes.com/b5/13/9/4/n3956082.htm%E3%80%8C%E6%99%AE%E9%81%B8%E6%B8%AF%E5%A7%90%E3%80%8D17%E8%90%AC%E7%A5%A8vs%E6%A2%81%E6%8C%AF%E8%8B%B1689%E7%A5%A8.html),
    [大紀元](../Page/大紀元.md "wikilink"), 2013年9月4日

25. ：渴望成為企業家，賺來的利潤可以用來貢獻社會。

26. [〈海怡西補選：得票遜人力袁彌明，單仲偕慘敗，溫和民主派響警號〉](http://hk.apple.nextmedia.com/news/art/20140325/18667643)，《蘋果日報》，2014年03月25日。

27.

28.

29. [一屋住家人：《老公無話兒》](http://programme.rthk.hk/rthk/tv/programme.php?name=tv/Myhomemyfamily&d=2011-05-31&p=5057&e=140587&m=episode)