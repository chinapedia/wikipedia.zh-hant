**Xpress 200
IE**是ATI的一款整合式晶片組，支援[Intel平台](../Page/Intel.md "wikilink")。其中IE代表Intel
Edition。它在2005年在[CeBIT推出](../Page/CeBIT.md "wikilink")，可以支援[Pentium
D處理器](../Page/Pentium_D.md "wikilink")，1066 MHz
[FSB](../Page/FSB.md "wikilink")。記憶體方面，支援雙通道DDR400/DDR-2
667記憶體。整合式顯示核心屬於X300級別，與[Xpress
200一樣](../Page/Xpress_200.md "wikilink")。相比上一代的[9100
IGP](../Page/9100_IGP.md "wikilink")，新增對DirectX
9的支援。[南橋方面](../Page/南橋.md "wikilink")，可以配搭ATI自家的IXP400/450，若廠商不滿意其效能，可以改為選用[ULi的M](../Page/ULi.md "wikilink")1573。

內建的顯示核心屬於X300級別，有兩條像素流水線。顯示核心的时脈是350
MHz，利用[HyperMemory技術](../Page/HyperMemory.md "wikilink")，可以分享系統記憶體為顯示記憶體。南北橋採用ALink連接，即[PCI-Express](../Page/PCI-Express.md "wikilink")。

下一代的晶片是RS600，與AMD平台的690G屬於同一級別。它可以支援最新的[Intel Core
2](../Page/Intel_Core_2.md "wikilink")，整合式核心屬於X700級別。在[AMD收購](../Page/AMD.md "wikilink")[ATI後](../Page/ATI.md "wikilink")，公司停止開發新的Intel平台晶片組。而此晶片組亦成為絕響。由於在AMD的收購行動前，ATI已製造了一些晶片組。為了儘快將產品賣出，AMD將大量的RS600一次過售給[ABIT和](../Page/ABIT.md "wikilink")[ASRock](../Page/ASRock.md "wikilink")。

## Xpress 200 IE晶元組列表

以下晶元組均支援双核心和1066 MHz外頻处理器，並支持DDRII-667：

  - RD400 -
    支援[双通道記憶体](../Page/Dual_Channel.md "wikilink")，支持[CrossFire](../Page/CrossFire.md "wikilink")，但不支持[Pentium
    Extreme Edition处理器](../Page/Pentium_Extreme_Edition.md "wikilink")
  - RS400 - 支援双通道記憶体，集成X300顯視核心
  - RC400 - 支援单通道記憶体，集成X300顯視核心
  - RC410 - 與RC400相同，但採用0.11微米製程
  - RXC410 - 支援单通道記憶体，支持[Pentium D](../Page/Pentium_D.md "wikilink")

## SB450規格

  - 支援2个PATA通道
  - 支援4个SATA150接口
  - 支援RAID 0和RAID 1
  - 支援8个USB2.0接口
  - 支援HD-Audio

註：SB450没有集成网絡卡

另外，主板廠商可搭配[ULi的](../Page/ULi.md "wikilink")**M1575**南橋

[Category:主板](../Category/主板.md "wikilink")
[Category:冶天科技](../Category/冶天科技.md "wikilink")