[辽河](../Page/辽河.md "wikilink"){{-w}}[海河](../Page/海河.md "wikilink"){{-w}}[黄河](../Page/:Template:黄河.md "wikilink"){{-w}}[淮河](../Page/淮河.md "wikilink"){{-w}}[黑龙江](../Page/:Template:黑龙江.md "wikilink"){{-w}}[珠江](../Page/:Template:珠江水系.md "wikilink")

|group2 = 沿海独立水系
<small></small>

</td>

|list2 =
[广西沿海诸河](../Page/广西沿海诸河流.md "wikilink"){{-w}}[广东沿海诸河](../Page/广东沿海诸河.md "wikilink"){{-w}}[福建沿海诸河](../Page/福建沿海诸河.md "wikilink"){{-w}}[浙江沿海诸河](../Page/浙江河流.md "wikilink"){{-w}}[江苏沿海诸河](../Page/江苏沿海诸河.md "wikilink"){{-w}}[山东沿海诸河](../Page/山东沿海诸河.md "wikilink"){{-w}}[京津冀沿海诸河](../Page/京津冀沿海诸河.md "wikilink"){{-w}}[辽宁沿海诸河](../Page/辽宁河流.md "wikilink"){{-w}}[海南沿海诸河](../Page/海南河流.md "wikilink")

|group3 = 边境跨国河流
<small></small>

</td>

|list3 =
[印度河水系](../Category/印度河水系.md "wikilink"){{-w}}[恒河水系](../Category/恒河水系.md "wikilink"){{-w}}[雅鲁藏布江（布拉马普特拉河）](../Page/雅鲁藏布江.md "wikilink"){{-w}}[伊洛瓦底江](../Page/伊洛瓦底江.md "wikilink"){{-w}}[怒江（萨尔温江）](../Page/怒江.md "wikilink"){{-w}}[澜沧江（湄公河）水系](../Page/澜沧江.md "wikilink"){{-w}}[红河](../Page/红河.md "wikilink")
{{-w}}[伊犁河](../Page/伊犁河.md "wikilink"){{-w}}[额尔齐斯河](../Page/额尔齐斯河.md "wikilink"){{-w}}[黑龙江水系](../Page/:Template:黑龙江水系.md "wikilink"){{-w}}[绥芬河](../Page/绥芬河_\(河流\).md "wikilink"){{-w}}[图们江](../Page/图们江.md "wikilink"){{-w}}[鸭绿江](../Page/鸭绿江.md "wikilink")

|group4 = 内陆河流
<small></small>

</td>

|list4 =
[塔里木河](../Page/塔里木河.md "wikilink"){{-w}}[额济纳河](../Page/额济纳河.md "wikilink"){{-w}}[疏勒河](../Page/疏勒河.md "wikilink"){{-w}}[玛纳斯河](../Page/玛纳斯河.md "wikilink"){{-w}}[乌伦古河](../Page/乌伦古河.md "wikilink"){{-w}}[格尔木河](../Page/格尔木河.md "wikilink")

|group5 = [运河](../Page/运河.md "wikilink") |list5 =
[京杭大运河](../Page/京杭大运河.md "wikilink"){{-w}}[灵渠](../Page/灵渠.md "wikilink"){{-w}}[苏北灌溉总渠](../Page/苏北灌溉总渠.md "wikilink"){{-w}}[胶莱河](../Page/胶莱河.md "wikilink"){{-w}}[胥河](../Page/胥河.md "wikilink"){{-w}}[浙东运河](../Page/浙东运河.md "wikilink")

|group6= [河流列表](../Page/中國河流列表.md "wikilink") |list6 =

` `[`天津`](../Page/天津河流列表.md "wikilink")`{{.w}}`[`河北`](../Page/河北河流列表.md "wikilink")`{{.w}}`[`山西`](../Page/山西河流列表.md "wikilink")`{{.w}}`[`內蒙古`](../Page/內蒙古河流列表.md "wikilink")`{{.w}}`[`遼寧`](../Page/遼寧河流列表.md "wikilink")`{{.w}}`[`吉林`](../Page/吉林河流列表.md "wikilink")`{{.w}}`[`黑龍江`](../Page/黑龍江河流列表.md "wikilink")
` |group2 = `[`華東`](../Page/华东地区.md "wikilink")`、`[`中南`](../Page/中南地区.md "wikilink")
` |list2 = `[`上海`](../Page/上海河流列表.md "wikilink")`{{.w}}`[`江蘇`](../Page/江蘇河流列表.md "wikilink")`{{.w}}`[`浙江`](../Page/浙江河流列表.md "wikilink")`{{.w}}`[`安徽`](../Page/安徽河流列表.md "wikilink")`{{.w}}`[`福建`](../Page/福建河流列表.md "wikilink")`{{.w}}`[`江西`](../Page/江西河流列表.md "wikilink")`{{.w}}`[`山東`](../Page/山东河流列表.md "wikilink")`{{.w}}`[`河南`](../Page/河南河流列表.md "wikilink")`{{.w}}`[`湖北`](../Page/湖北河流列表.md "wikilink")`{{.w}}`[`湖南`](../Page/湖南河流列表.md "wikilink")`{{.w}}`[`廣東`](../Page/广东河流列表.md "wikilink")`{{.w}}`[`廣西`](../Page/广西河流列表.md "wikilink")`{{.w}}`[`海南`](../Page/海南河流列表.md "wikilink")
` |group3 = `[`西南`](../Page/中国西南地区.md "wikilink")`、`[`西北`](../Page/中国西北地区.md "wikilink")
` |list3 = `[`重慶`](../Page/重慶河流列表.md "wikilink")`{{.w}}`[`四川`](../Page/四川河流列表.md "wikilink")`{{.w}}`[`貴州`](../Page/貴州河流列表.md "wikilink")`{{.w}}`[`雲南`](../Page/雲南河流列表.md "wikilink")`{{.w}}`[`西藏`](../Page/西藏河流列表.md "wikilink")`{{.w}}`[`陝西`](../Page/陝西河流列表.md "wikilink")`{{.w}}`[`甘肅`](../Page/甘肅河流列表.md "wikilink")`{{.w}}`[`青海`](../Page/青海河流列表.md "wikilink")`{{.w}}`[`寧夏`](../Page/寧夏河流列表.md "wikilink")`{{.w}}`[`新疆`](../Page/新疆河流列表.md "wikilink")
` |group4 = `[`特別行政区`](../Page/特別行政区.md "wikilink")
` |list4 = `[`香港`](../Page/香港河流列表.md "wikilink")`{{.w}}`[`澳門`](../Page/鴨涌河.md "wikilink")
` }}`

}}<noinclude> </noinclude>

[Category:中国河流模板](../Category/中国河流模板.md "wikilink")