**高城元氣**是[日本](../Page/日本.md "wikilink")[男性](../Page/男性.md "wikilink")[配音員](../Page/配音員.md "wikilink")。目前屬於[I'm
Enterprise](../Page/I'm_Enterprise.md "wikilink")。[神奈川縣出身](../Page/神奈川縣.md "wikilink")，[血型B型](../Page/血型.md "wikilink")。

## 演出作品

### 電視動畫

  - [女神異聞錄
    ～三位一體之魂～](../Page/PERSONA_-trinity_soul-.md "wikilink")（**瀨能壯太郎**）
  - [獵魔戰記](../Page/獵魔戰記.md "wikilink")（**拉基**）
  - [犬夜叉](../Page/犬夜叉.md "wikilink")（弥勒的少年時代）
  - [植木的法則](../Page/植木的法則.md "wikilink")（足立駿夫）
  - [ef - a tale of
    memories.](../Page/ef_-_a_tale_of_memories..md "wikilink")（**麻生蓮治**）
  - [ギャラリーフェイク](../Page/ギャラリーフェイク.md "wikilink")（菊島春雄）
  - [微笑的閃士](../Page/微笑的閃士.md "wikilink")（風龍王）
  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（テッペン／[小池徹平為原型的角色](../Page/小池徹平.md "wikilink")）
  - [蒼天之拳](../Page/蒼天之拳.md "wikilink")（少年期の潘光琳）
  - [それいけ\!ズッコケ三人組](../Page/それいけ!ズッコケ三人組.md "wikilink")（長岡保）
  - [だめっこどうぶつ](../Page/だめっこどうぶつ.md "wikilink")（うる野）
  - [奔向地球](../Page/奔向地球.md "wikilink")（**ジョナ・マツカ 瑪茨卡**）
  - [哆啦A夢](../Page/哆啦A夢.md "wikilink")（茂手もて夫）
  - [飛天小女警Z](../Page/飛天小女警Z.md "wikilink")（セロリモンスター）
  - [パピヨンローゼ New Season](../Page/パピヨンローゼ_New_Season.md "wikilink")（栗本）
  - [旋風管家](../Page/旋風管家.md "wikilink")（クラスメイト男子）
  - [双恋](../Page/双恋.md "wikilink")（**二見望**）
  - [彩夢芭蕾](../Page/彩夢芭蕾.md "wikilink")（ウマズラコーモリノ助　第21話的蝙蝠）
  - [神奇寶貝動畫版](../Page/神奇寶貝動畫版.md "wikilink")（コロク）
  - [LOVELESS](../Page/LOVELESS.md "wikilink")（ミドリ）
  - [BRAVE10](../Page/BRAVE10.md "wikilink")（由利鐮之介）
  - [蟲奉行](../Page/蟲奉行.md "wikilink")（由利鐮之介）
  - [來自新世界](../Page/來自新世界.md "wikilink")（**伊東守**）
  - [魔界王子 devils and
    realist](../Page/魔界王子_devils_and_realist.md "wikilink")（艾薩克·莫頓）
  - [宇宙戰艦大和號2199](../Page/宇宙戰艦大和號2199.md "wikilink")（星名透）※2012年起于电影院先行上映
  - [義呆利 Axis Powers](../Page/義呆利_Axis_Powers.md "wikilink")（香港）
  - [人生諮詢電視動畫「人生」](../Page/人生_\(輕小說\).md "wikilink")（大場陽一）
  - [金田一少年之事件簿R 第2期](../Page/金田一少年之事件簿_\(动画\).md "wikilink")（梨村亮）

**2016年**

  - [在下坂本，有何貴幹？](../Page/在下坂本，有何貴幹？.md "wikilink")（光一）

**2018年**

  - [千銃士](../Page/千銃士.md "wikilink")（故鄉\[1\]）

### OVA

  - [喜歡所以喜歡](../Page/喜歡所以喜歡.md "wikilink")（椎名廉〈大人〉）

### 動畫電影

  - [クレヨンしんちゃん 嵐を呼ぶ 歌うケツだけ爆弾\!](../Page/蜡笔小新#劇場版.md "wikilink")（隊員）

### 網路動畫

**2016年**

  - [哨聲響起 重配版](../Page/哨聲響起.md "wikilink")（椎名翼） \[2\]

### 遊戲

  - \[PC/PS2\]（**亚历山大·帕斯特纳**）
  - \[PS2\]（**霍比尔迪**）
      - \[DS\]绯色流星 DS
      - \[PSP\]绯色流星 携带版
      - \[PS2\]绯色流星 ～复活节的奇迹～
  - \[PC/DC/PS2\]Cherry Blossom（**二之宫遥夏**）
      - \[PSP\]Cherry Blossom 携带版
  - \[PC\]死神修行开始了。（）
  - \[PC\]STAMP OUT（田中純哉）
  - \[PC\]（）
  - \[PS2\]（）
  - \[GC\][瑪利歐足球](../Page/超级马里奥足球.md "wikilink")（）
  - \[wii\]（鐵鎚龜）
  - \[wii\][瑪利歐派對8](../Page/马里奥聚会8.md "wikilink")（鐵鎚龜）
  - \[DS\]（鐵鎚龜）
  - \[wii\]（鐵鎚龜、迴旋鏢龜、火球龜）
  - \[wii\][超級瑪利歐銀河2](../Page/超級瑪利歐銀河2.md "wikilink")（鐵鎚龜、迴旋鏢龜）
  - \[3DS\][超級瑪利歐3D樂園](../Page/超級瑪利歐3D樂園.md "wikilink")（鐵鎚龜、迴旋鏢龜）
  - \[wii\][瑪利歐派對9](../Page/马里奥聚会9.md "wikilink")（鐵鎚龜）
  - \[Wii U\][超級瑪利歐3D世界](../Page/超級瑪利歐3D世界.md "wikilink")（鐵鎚龜、迴旋鏢龜、火球龜）
  - \[3DS\][瑪利歐高爾夫：世界巡迴賽](../Page/瑪利歐高爾夫：世界巡迴賽.md "wikilink")（鐵鎚龜）
  - \[Wii U\][前進！奇諾比奧隊長](../Page/前進！奇諾比奧隊長.md "wikilink")（鐵鎚龜、火球龜）
  - \[Wii U\][瑪利歐派對10](../Page/瑪利歐派對10.md "wikilink")（鐵鎚龜）
  - \[Switch\][超級瑪利歐 奧德賽](../Page/超級瑪利歐_奧德賽.md "wikilink")（鐵鎚龜、火球龜）
  - 風の殺意 - It's a long way round -（笹森至）
  - [サムライスピリッツ
    天下一剣客伝](../Page/サムライスピリッツ_天下一剣客伝.md "wikilink")（緋雨閑丸、首斬り破沙羅）
  - [テイルズオブファンタジア](../Page/テイルズオブファンタジア.md "wikilink")（PSP版）（レアード王子）
  - [テイルズオブザテンペスト](../Page/テイルズオブザテンペスト.md "wikilink")（カイウス･クオールズ）
  - [ヘルミーナとクルス ～リリーのアトリエ
    もう一つの物語～](../Page/ヘルミーナとクルス_～リリーのアトリエ_もう一つの物語～.md "wikilink")（クルス）

### 外語配音

  - [哈利·波特与阿兹卡班的囚徒](../Page/哈利·波特与阿兹卡班的囚徒_\(电影\).md "wikilink")（生徒）

### 廣播劇

  - 元気と達央のMYSTIC OPERATION
    COMPANY（2004年11月23日～2006年6月30日、與[鈴木達央一同擔任](../Page/鈴木達央.md "wikilink")[主持人](../Page/主持人.md "wikilink")）
  - かわ▼コミっ（2006年8月1日～、[三重野瞳とともにパーソナリティを務める](../Page/三重野瞳.md "wikilink")）
  - [VOICE
    CREW](../Page/VOICE_CREW.md "wikilink")（2005年4月3日～9月25日、[倉田雅世とともに](../Page/倉田雅世.md "wikilink")15代目パーソナリティを務める）
  - BL-PARADISE（[一条和矢](../Page/一条和矢.md "wikilink")（2004年4月9日～2005年5月27日）・[緑川光](../Page/緑川光.md "wikilink")（2005年6月3日～2006年7月28日）とともにパーソナリティを務める）

### Vocal CD

  - スマイル

### Drama CD

  - [Apocripha/0～Blue Tail in the
    Cross～](../Page/Apocripha/0.md "wikilink")（アレク）
  - Apocripha/0～Passion Flower～（アレク）
  - 苺王子（ラズ・ベリー）
  - 王子様☆ゲーム（朱牙）
  - オレ様には敵わない（久賀真実）
  - Party（GAYA-代々木歩）
  - 重ねる指先（小宮大輝）
  - 危険な保健医カウンセラー（河合静）
  - 君と僕。 高校生篇1（バスケ部員）
  - [きみには勝てない\!](../Page/きみには勝てない!.md "wikilink")（酉島雄飛）
  - Characters Refine（新城真琴）
  - Candy-Store「Kura Kura\!」（三ッ橋はじめ）
  - [黑執事](../Page/黑執事.md "wikilink")（フィニ）
  - [小鳩町から散弾銃](../Page/小鳩町から散弾銃.md "wikilink")（井上雅俊）
  - さあ恋に落ちたまえ（坂下次男）
      - さあ恋に落ちたまえ2（坂下あゆむ）
      - さあ恋に落ちたまえ3（坂下あゆむ）
      - さあ恋に落ちたまえ4（坂下あゆむ）
  - さよならを言う気はない（周藤睦）
  - [しのぶこころは](../Page/しのぶこころは.md "wikilink")（すばる）
  - [好きなものは好きだからしょうがない\!\!シリーズ](../Page/好きなものは好きだからしょうがない!!#CD.md "wikilink")
    （椎名廉）
  - STAMP OUT（田中）
      - Another World STAMP OUT 田中純哉編（田中純哉）
      - shootist－シューティスト－（田中純哉）
  - Sexy Boy's（堂本晶）
      - 僕と司書さん－Sexy Boy's外伝－（堂本晶）
  - [ZOMBIE-LOAN](../Page/ZOMBIE-LOAN.md "wikilink")（犬走全）
  - たとえこの恋が罪であっても（アレクサンダー・ヴィクター・ステイプルトン）
  - Death\&Angel Miduki's Last Judgment（クルス）
  - 眠れる森の王子様（レオン王子）
  - ニュースセンターの恋人（小島柚希）
  - 花嫁衣装は誰が着る\!?（間宮由月）
  - Vie Durantシリーズ（衛夢）
  - 蜜と十字架～Honey and Cross～（真人）
  - 昔、男ありけり…。（源義秋）
  - 百合ヶ丘学園シリーズ
      - ハートもエースも僕のもの（北条陣）
      - きみだけのプリンスになりたい（北条陣）
      - スペードのキングも僕のもの（北条陣）
  - [LOVELESSシリーズ](../Page/LOVELESS.md "wikilink")（ミドリ）
  - ワガママ王子に御用心（マハティール\[子供時代\]）
  - [義呆利 Axis Powers](../Page/義呆利_Axis_Powers.md "wikilink")（香港）

### 舞台

  - 俺たちは志士じゃない（沖田総司）
  - CALLING（市村鉄之助）
  - FOUR FOR YOU\! 夢と希望をゲットだぜ\!（沢村光）
  - ロックミュージカル[『蒼き王と紅の王～Legend of
    Re:vitalize～』](http://www.legend-of-europa.com/p/index.html)
    2007年[8/4](http://schedule-this-month.seesaa.net/archives/20070804-1.html)～[8/5](http://schedule-this-month.seesaa.net/archives/20070805-1.html)公演
    （蒼き大河の国の王太子カイル役）

### 其他

  - [東京臨海新交通臨海線](../Page/東京臨海新交通臨海線.md "wikilink")・駅構内音声案内アナウンス -
    [船の科学館駅](../Page/船の科学館駅.md "wikilink")
  - [着信ボイス](../Page/着信ボイス.md "wikilink") ぼくとわたしの恋愛事情（アレクセイ）

## 外部連結

  - [I'm Enterprise網站上的簡介](http://www.imenterprise.jp/data.php?id=46)

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")

1.
2.