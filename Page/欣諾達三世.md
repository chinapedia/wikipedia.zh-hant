**亞歷山大教宗欣諾達三世**（[科普特語](../Page/科普特語.md "wikilink")：Ⲡⲁⲡⲁ Ⲁⲃⲃⲁ Ϣⲉⲛⲟⲩⲟϯ
ⲡⲓⲙⲁϩ
ϣⲟⲩⲙⲧ；；，），本名**纳齐尔·杰伊德·鲁法伊尔**（），是第117任[亞歷山大科普特正教會](../Page/亞歷山大科普特正教會.md "wikilink")[亞歷山大教宗和全非洲牧首奉福音書作者聖馬爾谷宗徒聖座](../Page/亞歷山大科普特正教會教宗.md "wikilink")，[亞歷山大神聖會議的科普特正教牧首區的領袖](../Page/亞歷山大神聖會議的科普特正教牧首區.md "wikilink")。

## 生平

欣诺达于1954年成为一名僧侣，并进入[叙利亚修道院](../Page/叙利亚修道院.md "wikilink")，道号为叙利亚的安东尼奥斯。1958年晋为[祭司](../Page/祭司.md "wikilink")。1962年，时名安东尼奥斯的他受[西里尔六世召唤](../Page/西里爾六世_\(亞歷山大教宗\).md "wikilink")，被赋予科普特正教神学院院长及基督教育主教的职务。也是在这期间，安东尼奥斯更名为欣诺达，其名来自埃及基督教圣人[圣欣诺特](../Page/圣欣诺特.md "wikilink")，在他担任院长期间，神学院的学生数量翻了三倍。\[1\]然而在1966年，欣诺达主教遭到除权，原因乃是因为他和他的学生号召通过选举产生主教和祭司。欣诺达和西里尔六世之间的矛盾最终还是得到了缓解。\[2\]\[3\]1971年11月14日，被选为新任[亚历山大教宗](../Page/亞歷山大港牧首.md "wikilink")。1974年，原本从属科普特教会并与之完全共融的[埃塞俄比亚正教会牧首](../Page/埃塞俄比亞正統台瓦西多教會.md "wikilink")[阿布纳·狄奥斐卢斯遭到](../Page/阿布纳·狄奥斐卢斯.md "wikilink")[社会主义埃塞俄比亚临时军政府推翻](../Page/社会主义埃塞俄比亚临时军政府.md "wikilink")，欣诺达拒绝承认由军政府推举的继任者并断绝了正式往来。两教会的关系直至2007年7月13日才恢复。\[4\]1981年，已故埃及总统[萨达特将欣诺达放逐到一个沙漠修道院](../Page/萨达特.md "wikilink")，因欣诺达指责政府没有对穆斯林极端分子采取行动。前埃及总统[穆巴拉克在](../Page/穆巴拉克.md "wikilink")1985年结束了对欣诺达的流放。欣诺达领导科普特教徒渡过了与在埃及人口中占多数的穆斯林的关系紧张时期。伊斯兰强硬派袭击基督教教堂，包括2011年对亚历山大一座科普特教堂进行炸弹袭击，导致23人丧生。\[5\]

欣诺达三世在任期间，北美的科普特正教会有了长足的发展。1971年，全北美仅有4间科普特教堂，而后扩增至两百余间之多。\[6\]\[7\]此外，他也热心于[普世教会合一运动](../Page/普世教會合一運動.md "wikilink")\[8\]，1973年，他成为了一千五百年来首位与[罗马教宗会晤的亚历山大教宗](../Page/教宗.md "wikilink")。欣诺达与[保禄六世在有关](../Page/保祿六世.md "wikilink")[基督论的议题上签署了一份共同宣言并同意继续深入交流](../Page/基督论.md "wikilink")。\[9\]

2012年3月17日欣諾達三世因患[癌症在](../Page/癌症.md "wikilink")[開羅去世](../Page/開羅.md "wikilink")，享年88歲\[10\]。

## 佚事

  - 在[亞歷山卓的亞波塔拉特](../Page/亞歷山卓.md "wikilink")，那邊的一條街道被命名為「教宗欣諾達三世」。

## 参考文献

## 外部連結

  - [Pope Shenouda III – official website
    教宗欣諾達三世的正式網站](https://web.archive.org/web/20101015122835/http://www.copticpope.org/index.php)
  - [Pope Shenouda III – Coptic Orthodox Church Network: Biography,
    Online Books, and Audio
    Sermons](http://www.copticchurch.net/topics/pope/)
  - [Articles by Pope Shenouda III in English from
    coptichymns.net](https://web.archive.org/web/20070927234418/http://coptichymns.net/index.php?module=library&tid=1&filter=core.author:eq:H.H.%20Pope%20Shenouda%20III&pubcnt=100)

  - [More information about the life of Pope Shenouda III – from Saint
    Takla Haymanout the Ethiopian Church, Alexandria,
    Egypt](http://st-takla.org/Pope-1.html)
  - [Common declaration of Pope Shenouda III and Pope Paul VI
    (1973)](http://www.vatican.va/roman_curia/pontifical_councils/chrstuni/anc-orient-ch-docs/rc_pc_christuni_doc_19730510_copti_en.html)

  - [Pope Shenouda's sermons in
    Arabic](http://st-takla.org/Multimedia/04-Sermons-01-Pope-Shenouda.html)

  - [Pope 欣諾達三世的生平](http://coptipedia.com)

## 參見

  - [科普特正教會](../Page/科普特正教會.md "wikilink")
  - [亞歷山大科普特正教會教宗](../Page/亞歷山大科普特正教會教宗.md "wikilink")
  - [亞歷山大科普特正教教宗列表](../Page/亞歷山大科普特正教教宗列表.md "wikilink")
  - [亞歷山大神聖會議的科普特正教牧首區](../Page/亞歷山大神聖會議的科普特正教牧首區.md "wikilink")
  - [亞歷山大宗主教](../Page/亞歷山大宗主教.md "wikilink")

{{-}}

[Category:亞歷山大科普特正教會教宗](../Category/亞歷山大科普特正教會教宗.md "wikilink")
[Category:亞歷山大教理學校總鐸](../Category/亞歷山大教理學校總鐸.md "wikilink")
[Category:埃及宗教领袖](../Category/埃及宗教领袖.md "wikilink")
[Category:亞歷山大宗主教](../Category/亞歷山大宗主教.md "wikilink")
[Category:基督教宗教领袖](../Category/基督教宗教领袖.md "wikilink")
[Category:埃及僧侶](../Category/埃及僧侶.md "wikilink")
[Category:埃及科普特正教徒](../Category/埃及科普特正教徒.md "wikilink")
[Category:開羅大學校友](../Category/開羅大學校友.md "wikilink")

1.
2.
3.  <http://www.zeitun-eg.net/members_contrib/DrGeorgeHBebawi16Nov06.doc>
4.  ["Common Declaration" of Pope Shenoudah III, Catholicos Aram I, and
    Patriarch Paulos – News and Media of the Armenian Orthodox
    Church, 22
    July 2007](http://www.cathcil.org/v04/doc/English/visitseng.htm#11)
5.  [埃及科普特教派大主教谢努达去世](http://www.voanews.com/chinese/news/20120317-obit-pope-143064406.html)
6.
7.  [1](http://www.sacotc.org/docs/2186_CopticChurch_broc.pdf)
8.  [Obituary: Pope Shenouda
    III](https://www.bbc.co.uk/news/world-middle-east-17416731) [BBC
    News Online](../Page/BBC_News_Online.md "wikilink"), 17 March 2012
9.
10.