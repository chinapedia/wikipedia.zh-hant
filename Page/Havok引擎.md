**Havok引擎**，全称为**Havok游戏动力学开发工具包**（**Havok Game Dynamics
SDK**），一般称为**Havok**，是一个用于物理（动力学）效应模拟的[游戏引擎](../Page/游戏引擎.md "wikilink")，为[电子游戏所设计](../Page/电子游戏.md "wikilink")，注重在游戏中对于真实世界的模拟。使用撞击监测功能的Havok引擎可以让更多真实世界的情况以最大的拟真度反映在游戏中。2015年10月2日,[微軟宣佈從](../Page/微軟.md "wikilink")[Intel收購Havok](../Page/Intel.md "wikilink")。
.\[1\]

## 版本演進

Havok的1.0版本是在2000年的游戏开发者大会（GDC）上面发布的；2.0版本在2003年的GDC大会上发布；4.5版本在2007年3月釋出。[原始碼在取得引擎使用授權之後便會收到](../Page/原始碼.md "wikilink")。目前，Havok可以在[微軟的](../Page/微軟.md "wikilink")[Windows作業系統](../Page/Windows.md "wikilink")、[Xbox與](../Page/Xbox.md "wikilink")[Xbox360](../Page/Xbox360.md "wikilink")，[任天堂的](../Page/任天堂.md "wikilink")[GameCube與](../Page/GameCube.md "wikilink")[Wii](../Page/Wii.md "wikilink")、[索尼的](../Page/索尼.md "wikilink")[PS2](../Page/PS2.md "wikilink")、[PS3與](../Page/PS3.md "wikilink")[PSP](../Page/PSP.md "wikilink")、[蘋果電腦的](../Page/蘋果電腦.md "wikilink")[Mac
OS
X](../Page/Mac_OS_X.md "wikilink")、[Linux等](../Page/Linux.md "wikilink")[作業系統或遊戲主機上使用](../Page/作業系統.md "wikilink")。此遊戲引擎是用[C語言](../Page/C語言.md "wikilink")／[C++語言所撰寫而成](../Page/C++語言.md "wikilink")。

最新的5.5版本在2008年2月釋出。新版本的SDK更完善，更人性化。亦加入了新的物理效果，例如布料的擺動效果。在示範中，Havok利用了斗篷來表現這個效果。當人物走動的時候，其背後的斗篷會隨著人的移動來擺動。破壞效果方面，新增了物體的破碎和變形\[2\]。非商業應用的物理引擎是免費提供的，目的是加大普及率。\[3\]。隨後，商業的遊戲應用亦變成免費。不過商業應用但非遊戲，和引擎的全部源代碼，就需要付款。\[4\]

## 使用狀況

自从Havok引擎发布以来，它已经被应用到超过150个游戏之中。最早，使用Havok引擎的游戏大多数都是[第一人称射击类别](../Page/第一人称射击.md "wikilink")，但隨著遊戲開發的複雜度與規模越來越大，其他類型的遊戲也想要有更加真實的物理表現，有越來越多的其他類型的遊戲採用Havok引擎，例如為[即時戰略類型](../Page/即時戰略.md "wikilink")，[Ensemble
Studios的](../Page/Ensemble_Studios.md "wikilink")[世紀帝國III與](../Page/世紀帝國III.md "wikilink")[暴雪娛樂的](../Page/暴雪娛樂.md "wikilink")[星海爭霸II](../Page/星海爭霸II.md "wikilink")；競速類型，[世嘉公司的](../Page/世嘉公司.md "wikilink")[音速小子與索尼發行的](../Page/音速小子.md "wikilink")[摩托風暴](../Page/摩托風暴.md "wikilink")。在软件[3D
Studio
Max和](../Page/3ds_Max.md "wikilink")[Maya](../Page/Maya.md "wikilink")
3D中也能看到已经打包为插件的Havok引擎。同时，文明6也使用了这一物理引擎。

## Havok FX

為了和[PhysX競爭](../Page/PhysX.md "wikilink")，Havok
FX可以使用多繪圖處理器的技術來加速物理計算，包括了[NVIDIA的](../Page/NVIDIA.md "wikilink")[SLI和](../Page/SLI.md "wikilink")[ATI的](../Page/ATI.md "wikilink")[CrossFire](../Page/CrossFire.md "wikilink")。Havok將物理運算分為特效和遊戲運算，特效運算（如爆炸時的煙霧）將會由GPU的[Shader
Model
3.0來進行運算](../Page/高級著色器語言.md "wikilink")，進而減輕CPU的負擔。而遊戲物理運算則仍然由CPU處理。由於[英特尔收購了Havok](../Page/英特尔.md "wikilink")，前者顯然希望物理計算由CPU負責，所以由顯示卡加速Havok
FX的開發似乎已經被取消。

在GDC09展覽中，演示中的Havok引擎使用了[ATI的](../Page/ATI.md "wikilink")[顯示核心作為加速](../Page/顯示核心.md "wikilink")。由於引擎是基於[OpenCL架構進行開發](../Page/OpenCL.md "wikilink")，所以[處理器和顯示核心都可以為其進行計算](../Page/處理器.md "wikilink")。\[5\]

## 使用Havok的著名遊戲

  - 《古剑奇谭三》
  - 《[刺客信条系列](../Page/刺客信条系列.md "wikilink")》
  - 《[伏魔咒](../Page/伏魔咒.md "wikilink")》
  - 《[现代战争4：决战时刻](../Page/现代战争4：决战时刻.md "wikilink")》
  - 《[戰慄時空2](../Page/戰慄時空2.md "wikilink")》
  - 《[星际争霸II](../Page/星际争霸II.md "wikilink")》
  - 《[风暴英雄](../Page/风暴英雄.md "wikilink")》
  - 《[刺客信條：兄弟會](../Page/刺客信條：兄弟會.md "wikilink")》
  - 《[江湖本色2](../Page/江湖本色.md "wikilink")》
  - 《[凯恩与林奇：死人](../Page/凯恩与林奇：死人.md "wikilink")》
  - 《[凯恩与林奇2：伏天](../Page/凯恩与林奇2：伏天.md "wikilink")》
  - 《[榮譽勳章:太平洋突襲](../Page/榮譽勳章:太平洋突襲.md "wikilink")》
  - 《[斬除妖魔](../Page/斬除妖魔.md "wikilink")》
  - 《[世紀帝國III](../Page/世紀帝國III.md "wikilink")》
  - 《[戰慄突擊](../Page/戰慄突擊.md "wikilink")》
  - 《[縱橫諜海：混沌理論](../Page/縱橫諜海.md "wikilink")》
  - 《[最後一戰2](../Page/最後一戰2.md "wikilink")》
  - 《[最後一戰3](../Page/最後一戰3.md "wikilink")》
  - 《[任天堂明星大亂鬥X](../Page/任天堂明星大亂鬥X.md "wikilink")》
  - 《[上古捲軸IV：湮沒](../Page/上古捲軸IV：湮沒.md "wikilink")》
  - 《[上古捲軸V：天际](../Page/上古捲軸V：天际.md "wikilink")》
  - 《[上古卷軸Online](../Page/上古卷軸Online.md "wikilink")》
  - 《[灵魂献祭](../Page/灵魂献祭.md "wikilink")》
  - 《[殺戮地帶](../Page/殺戮地帶.md "wikilink")》
  - 《彩虹六號：圍攻行動》
  - [《黑暗靈魂》](../Page/黑暗靈魂.md "wikilink")
  - 《[黑暗靈魂II](../Page/黑暗靈魂II.md "wikilink")》
  - 《[黑暗靈魂III](../Page/黑暗靈魂III.md "wikilink")》
  - 《[暗黑破壞神III](../Page/暗黑破壞神III.md "wikilink")》（早期开发曾使用Havok，后来转为使用自制引擎

\[6\]）

  - 《[孤膽車神-維加斯](../Page/孤膽車神-維加斯.md "wikilink")》
  - 《[薩爾達傳說 旷野之息](../Page/薩爾達傳說_旷野之息.md "wikilink")》
  - 《[文明VI](../Page/文明VI.md "wikilink")》
  - 《[坦克世界](../Page/坦克世界.md "wikilink")》（1.0版本開始使用Havok)
  - 《[香港秘密警察](../Page/香港秘密警察.md "wikilink")》
  - 《[四海兄弟III](../Page/四海兄弟III.md "wikilink")》
  - 《[血洗墨西哥](../Page/血洗墨西哥.md "wikilink")》

## 使用Havok的软件

  - [3DMark06](../Page/3DMark06.md "wikilink")
  - [3DMark Vantage](../Page/3DMark_Vantage.md "wikilink")
  - [3ds Max](../Page/3ds_Max.md "wikilink")
  - [Adobe](../Page/Adobe.md "wikilink") Atmosphere
  - Adobe Shockwave
  - The Source Engine

## 外部链接

  - [官方主页](http://www.havok.com/)

## 參考

[Category:物理引擎](../Category/物理引擎.md "wikilink")
[Category:虚拟现实](../Category/虚拟现实.md "wikilink")
[Category:并发计算](../Category/并发计算.md "wikilink")
[Category:电脑游戏中间件](../Category/电脑游戏中间件.md "wikilink")

1.
2.  [輝煌是否依舊？Havok 5.5物理引擎發布](http://financenews.sina.com/sinacn/304-000-106-109/2008-02-20/1920709569.html)

3.  [Havok 5.5物理引擎发布 5月免费下载](http://news.mydrivers.com/1/99/99947.htm)
4.  [Intel开放Havok物理引擎全免费下载](http://news.mydrivers.com/1/107/107653.htm)
5.  [Havok引擎](http://www.inpai.com.cn/doc/hard/92898.htm)
6.