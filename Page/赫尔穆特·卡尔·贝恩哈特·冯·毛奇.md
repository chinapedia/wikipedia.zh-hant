**赫尔穆特·卡尔·贝恩哈特·冯·毛奇**（，），普魯士和德意志名將，
[普鲁士和](../Page/普鲁士.md "wikilink")[德意志总参谋长](../Page/德意志帝国.md "wikilink")，军事家，通稱「老毛奇」。[德国](../Page/德国.md "wikilink")[陆军元帅](../Page/德国陆军元帅列表.md "wikilink")。

毛奇生于[梅克伦堡](../Page/梅克伦堡.md "wikilink")[帕尔希姆一破落贵族家庭](../Page/帕尔希姆.md "wikilink")，其家於1805年移居[丹麦](../Page/丹麦.md "wikilink")[石勒苏益格-荷尔斯泰因的](../Page/石勒苏益格-荷尔斯泰因.md "wikilink")[吕贝克](../Page/吕贝克.md "wikilink")（今属德国）。他於1818年毕业于[哥本哈根皇家军校](../Page/哥本哈根.md "wikilink")，进丹麦军队服役。1822年转入普鲁士军队，获少尉衔。1835年至1839年任[奥斯曼土耳其苏丹军事顾问](../Page/奥斯曼土耳其.md "wikilink")。1857年至1888年任普军和德军总参谋长，领导指挥德军参加[普奥战争](../Page/普奥战争.md "wikilink")（1866年）和[普法战争](../Page/普法战争.md "wikilink")（1870年-1871年），在[色当之战中取得决定性胜利](../Page/色當會戰.md "wikilink")，为实现德意志统一作出重大贡献，受封伯爵并于次年晋升元帅。

1867年到1871年毛奇担任在北部德国邦联的议员，从1871年到1891年他是德国國會的成员。1888年毛奇由总参谋长位置上退休，由[阿尔弗雷德·冯·瓦德西继任](../Page/阿尔弗雷德·冯·瓦德西.md "wikilink")。他的侄子[赫尔穆特·约翰內斯·路德维希·冯·毛奇](../Page/赫尔穆特·约翰內斯·路德维希·冯·毛奇.md "wikilink")（小毛奇），則於1906年到1914年擔任德军总参谋长。

1888年8月9日，毛奇於退役后任国防委员会主席。1891年卒于[柏林](../Page/柏林.md "wikilink")。

毛奇上撰寫了一定数量的軍事理論著作。受[卡尔·冯·克劳塞维茨](../Page/卡尔·冯·克劳塞维茨.md "wikilink")《战争论》的影响，他的主要论题是军事战略必须被了解作为选择系统，因为唯一军事操作的起点是plannable。结果，他考虑军事领导主任务包括在所有可能的结果的广泛准备。

为了纪念这位杰出的军事家，1908年[德国造舰计划中的G号重巡洋舰](../Page/德国.md "wikilink")（战列巡洋舰）被命名为[毛奇号巡洋舰](../Page/毛奇级战列巡洋舰.md "wikilink")。该级舰的另外一艘以普奥战争和普法战争中的普鲁士将军[奥古斯特·卡尔·冯·戈本](../Page/奥古斯特·卡尔·冯·戈本.md "wikilink")（1816年—1880年）命名。另外，月球正面位於靜海西南部的一座小撞擊坑，於1935年被國際天文學聯合會批准接受命名為[毛奇隕石坑](../Page/毛奇隕石坑.md "wikilink")（Moltke）。

[Category:德國軍事學家](../Category/德國軍事學家.md "wikilink")
[Category:普魯士將軍](../Category/普魯士將軍.md "wikilink")
[Category:德國元帥](../Category/德國元帥.md "wikilink")
[Category:普法戰爭人物](../Category/普法戰爭人物.md "wikilink")
[Category:德國貴族](../Category/德國貴族.md "wikilink")
[Category:梅克倫堡-前波門人](../Category/梅克倫堡-前波門人.md "wikilink")