**王炬**（），字**子和**，本名**惠林**，筆名**華步庭主**，外號「太極膽」，[山東省](../Page/山東省.md "wikilink")[濰縣人](../Page/濰縣.md "wikilink")，1911年12月10日生，2003年8月7日卒於[台北](../Page/台北.md "wikilink")，享年九十二。王炬一生從事[楊氏太極武藝的傳承及](../Page/楊氏太極.md "wikilink")[國語文教育事業](../Page/國語文.md "wikilink")，對保存[中國傳統文化功不可沒](../Page/中國傳統文化.md "wikilink")，可謂文武兼備。對太極武藝的問題提出見解，曾言：「太極若不現代學術化，沒有方法杜絕它不庸俗化，不把它交到體育學者手裡，沒有希望不看到它江湖化」。以科學性的研究而科學化，並依此著述論文，及整理、充實太極武藝教材。

## 重要事蹟

1937年日軍侵華，先生毅然投筆從戎，隨軍南下參加徐州保衛戰。

1941年拜[楊氏太極拳第三代宗師](../Page/楊氏太極拳.md "wikilink")[楊澄甫之徒](../Page/楊澄甫.md "wikilink")[呂殿臣先生為師](../Page/呂殿臣.md "wikilink")，學習楊家太極武藝。

1945年10月，與魏建功、何容在[台灣組成](../Page/台灣.md "wikilink")[台灣國語推行委員會](../Page/台灣國語推行委員會.md "wikilink")，於電台開設讀音示範、創辦國語日報。

1996年獲頒「全球中華文化藝術薪傳獎、中華武藝獎」，為「武藝獎」的第一位得主，並列入《中華太極拳[名人錄](../Page/名人錄.md "wikilink")》一書。

## 外部連結

  - [王子和先生行述](http://tw.myblog.yahoo.com/one168-168/article?mid=2150&prev=2151&next=-2&page=1)

<!-- end list -->

  - [涵化小築](http://archer.ee.nctu.edu.tw/~hanhua-place/)

<!-- end list -->

  - [楊太極武藝協會](https://web.archive.org/web/20080927090213/http://ytj.org.tw/)

[Category:太極拳](../Category/太極拳.md "wikilink")
[Category:台灣武術家](../Category/台灣武術家.md "wikilink")
[Category:中国武术家](../Category/中国武术家.md "wikilink")
[Category:台灣戰後山東移民](../Category/台灣戰後山東移民.md "wikilink")
[Category:潍坊人](../Category/潍坊人.md "wikilink")
[J炬](../Category/王姓.md "wikilink")