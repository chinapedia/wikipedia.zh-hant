**劉辰駿**（），原名**劉果行**，[字](../Page/表字.md "wikilink")**勷劬**，[號](../Page/號.md "wikilink")**問松**。[江蘇省](../Page/江蘇省.md "wikilink")[常州府](../Page/常州府.md "wikilink")[武進縣](../Page/武進縣.md "wikilink")（今屬[江蘇省](../Page/江蘇省.md "wikilink")[常州市](../Page/常州市.md "wikilink")[武進區](../Page/武進區.md "wikilink")）人，[清朝政治人物](../Page/清朝.md "wikilink")。

## 歷任

  - 邑[庠生](../Page/庠生.md "wikilink")，保舉賢良方正，[主事銜](../Page/主事.md "wikilink")。[戶部山東司學習行走](../Page/戶部.md "wikilink")。
  - 欽命[陝西宣諭化導](../Page/陝西.md "wikilink")，留陝以[知縣任用](../Page/知縣.md "wikilink")。
  - [雍正十二年](../Page/雍正.md "wikilink")（1734年）：捕授[福建](../Page/福建.md "wikilink")[延平府](../Page/延平府.md "wikilink")[將樂縣知縣](../Page/將樂縣.md "wikilink")。
  - 十三年（1735年）：敕授[文林郎](../Page/文林郎.md "wikilink")。
  - [乾隆元年](../Page/乾隆.md "wikilink")（1736年）：調福建[閩縣知縣](../Page/閩縣.md "wikilink")。
  - 五年（1740年）：閩縣知縣，[丁母憂](../Page/丁憂.md "wikilink")。
  - 八年（1743年）：捕授福建[南平知縣](../Page/南平.md "wikilink")。
  - 十年（1745年）：南平知縣，丁父憂。
  - 十四年（1749年）：補授[四川](../Page/四川.md "wikilink")[成都府](../Page/成都府.md "wikilink")[成都縣知縣](../Page/成都縣.md "wikilink")，因閩縣任內造冊遲延，部議革職。後以縣丞用。
  - 十六年（1751年）：[侯官縣](../Page/侯官縣.md "wikilink")[縣丞](../Page/縣丞.md "wikilink")。
  - 十七年（1752年）：以縣丞調署[台灣府](../Page/台灣府.md "wikilink")[台灣縣](../Page/台灣縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")\[1\]。
  - 十八年（1753年）：補台灣府[彰化縣知縣](../Page/彰化縣.md "wikilink")\[2\]。
  - 十九年（1754年）：彰化知縣，署[台灣府淡水撫民同知](../Page/台灣府淡水撫民同知.md "wikilink")\[3\]。

有敕命一道。

## 家庭及關聯

劉辰駿為[武進西營劉氏第十三世](../Page/武進西營劉氏.md "wikilink")。

  - 祖父：劉維寧。
  - 父：劉澐。
  - 母：趙氏，順治十八年進士[趙煃晃之女](../Page/趙煃晃.md "wikilink")。

### 妻妾

  - 正室：岳氏，敕封[孺人](../Page/孺人.md "wikilink")。[監生考授](../Page/監生.md "wikilink")[同知岳炳霱之女](../Page/同知.md "wikilink")。
  - 無側室。

### 子女

  - 長子：劉扶紀（1722年－1769年），監生，安徽[桐城縣](../Page/桐城縣.md "wikilink")[主簿](../Page/主簿.md "wikilink")、[巡檢](../Page/巡檢.md "wikilink")。

<!-- end list -->

  - 長女，適監生誥授[中憲大夫](../Page/中憲大夫.md "wikilink")，[浙江](../Page/浙江.md "wikilink")[溫處兵備道陸繩中](../Page/溫處道.md "wikilink")。
  - 次女，適監生朱琳。
  - 三女，適附監生徐廷鑑。

## 注釋及參考資料

  - 劉琛、劉善強等修，《武進西營劉氏家譜》（1929年）卷三·世表·葉六十四
  - 劉寧顏 編《重修台灣省通志》台北市,台灣省文獻委員會,1994年
  - [中央研究院](../Page/中央研究院.md "wikilink")[歷史語言研究所](http://www.ihp.sinica.edu.tw/)內閣大庫檔案011933號
  - 《[福州府志](../Page/福州府.md "wikilink")》
  - 《彰化縣志》
  - 《淡水廳志》
  - 參見:[台灣縣](../Page/台灣縣.md "wikilink")
  - 參見:[彰化縣](../Page/彰化縣.md "wikilink")
  - 參見:[淡水廳](../Page/淡水廳.md "wikilink")

{{-}}

[Category:清朝臺灣縣知縣](../Category/清朝臺灣縣知縣.md "wikilink")
[Category:清朝彰化縣知縣](../Category/清朝彰化縣知縣.md "wikilink")
[Category:台灣府淡水撫民同知](../Category/台灣府淡水撫民同知.md "wikilink")
[C](../Category/劉姓.md "wikilink") [C](../Category/武進西營劉氏.md "wikilink")

1.  宮中檔,乾隆十七年八月廿二日,[閩浙總督](../Page/閩浙總督.md "wikilink")[喀爾吉善等奏請以曾曰瑛補臺灣府知府摺](../Page/喀爾吉善.md "wikilink")
2.  任內在縣衙內修建[天后廟堂](../Page/媽祖.md "wikilink")。開放彰化義塚自由安葬（《[臺灣通史](../Page/臺灣通史.md "wikilink")》卷二十一）。
3.  《淡水廳志》:「善聽斷，胥役舞弊者，懲治不稍假。」