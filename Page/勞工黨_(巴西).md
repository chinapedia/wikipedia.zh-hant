**勞工黨**（）是[巴西的](../Page/巴西.md "wikilink")[中間偏左的](../Page/中間偏左.md "wikilink")[民主社會主義政党](../Page/民主社會主義.md "wikilink")，2003年至2016年是巴西的[執政黨](../Page/執政黨.md "wikilink")。

## 历史

該黨曾是具有[馬克思主義背景的](../Page/馬克思主義.md "wikilink")[左翼政党](../Page/左翼.md "wikilink")，有不少[托派分子参与了该党的创建](../Page/托派.md "wikilink")。在1964－1985年右翼軍政府獨裁時期遭到封禁。1985年軍政府下台後獲得解禁。

該黨領袖[路易斯·伊納西奧·盧拉·達席爾瓦曾于](../Page/路易斯·伊納西奧·盧拉·達席爾瓦.md "wikilink")1989年、1994年和1998年代表勞工黨和左派聯盟參選總統，但皆失敗。2002年盧拉領導的[中間偏左的聯盟](../Page/中間偏左.md "wikilink")，終於成功當選總統，該黨首次成為[執政黨](../Page/執政黨.md "wikilink")，與其他政黨合作組成執政聯盟，2006年再次當選。

2010年及2014年，[迪爾瑪·羅塞夫兩次當選總統](../Page/迪爾瑪·羅塞夫.md "wikilink")，羅賽芙是巴西首位女總統。

2016年8月31日，罗塞夫被巴西参议院弹劾，由代总统特梅尔接任总统，劳工党失去执政党地位。

## 历任主席

  - [路易斯·伊纳西奥·卢拉·达席尔瓦](../Page/路易斯·伊纳西奥·卢拉·达席尔瓦.md "wikilink") (1980年 -
    1994年)
  - [鲁伊·法尔考](../Page/鲁伊·法尔考.md "wikilink") (1994年)
  - [若泽·迪尔塞乌](../Page/若泽·迪尔塞乌.md "wikilink") (1995年 - 2002年)
  - [若泽·热诺伊诺](../Page/若泽·热诺伊诺.md "wikilink") (2002年 - 2005年)
  - [塔尔索·任罗](../Page/塔尔索·任罗.md "wikilink") (2005年) (临时)
  - [里卡多·贝尔佐伊尼](../Page/里卡多·贝尔佐伊尼.md "wikilink") (2005年 - 2006年)
  - [马尔科·阿乌雷里奥·加西亚](../Page/马尔科·阿乌雷里奥·加西亚.md "wikilink") (2006年10月6日 -
    2007年1月2日) (临时)
  - 里卡多·贝尔佐伊尼 (2007年1月2日 - 2010年2月19日)
  - 若泽·爱德瓦多·杜特拉 (2010年2月19日 - 2011年)
  - 鲁伊·法尔考 (2011年至今)

## 参见

  - [社会主义民主 (巴西)](../Page/社会主义民主_\(巴西\).md "wikilink")
  - [巴西共產黨](../Page/巴西共產黨.md "wikilink")

## 参考资料

  - [MENEGOZZO](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4739869P5),
    Carlos Henrique Metidieri;
    [KAREPOVS](http://lattes.cnpq.br/8715129121601284), Dainis;
    [MACIEL](http://lattes.cnpq.br/9984832119154642), Aline Fernanda;
    [SILVA](http://lattes.cnpq.br/4925098455180178), Patrícia Rodrigues
    da; [CESAR](http://lattes.cnpq.br/7904921324819047), Rodrigo.
    [Partido dos Trabalhadores: bibliografia comentada
    (1978-2002)](https://web.archive.org/web/20140222235722/http://novo.fpabramo.org.br/sites/default/files/pt_bibliografia_1ed-3.pdf).
    São Paulo: Editora Fundação Perseu Abramo, 2013. 413 p.

[Category:巴西政黨](../Category/巴西政黨.md "wikilink")
[Category:社會民主主義政黨](../Category/社會民主主義政黨.md "wikilink")
[Category:1980年建立](../Category/1980年建立.md "wikilink")