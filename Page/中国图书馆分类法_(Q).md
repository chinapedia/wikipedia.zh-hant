## 生物科学

:\*Q-49 生物学的普及读物

:\*Q-61 [物理学词典](../Page/物理学.md "wikilink")

  - Q1 普通[生物学](../Page/生物学.md "wikilink")

:\*Q10 [生命的起源](../Page/生命.md "wikilink")

:\*Q11 生物演化和发展

:\*Q13 [生物形态学](../Page/生物形态学.md "wikilink")

:\*Q14 [生态学](../Page/生态学.md "wikilink")

:\*Q15 生物分布与[生物地理学](../Page/生物地理学.md "wikilink")

:\*Q16 [保护生物学](../Page/保护生物学.md "wikilink")

:\*Q17 [水生生物学](../Page/水生生物学.md "wikilink")

:\*Q18 [寄生生物学](../Page/寄生生物学.md "wikilink")

:\*Q19 [生物分类学](../Page/生物分类学.md "wikilink")

  - Q2 [细胞生物学](../Page/细胞生物学.md "wikilink")

:\*Q21 [细胞的形成与演化](../Page/细胞.md "wikilink")

:\*Q23 [细胞遗传学](../Page/细胞遗传学.md "wikilink")

:\*Q24 [细胞形态学](../Page/细胞形态学.md "wikilink")

:\*Q25 [细胞生理学](../Page/细胞生理学.md "wikilink")

:\*Q26 [细胞生物化学](../Page/细胞生物化学.md "wikilink")

:\*Q27 [细胞生物物理学](../Page/细胞生物物理学.md "wikilink")

::\*Q291 [细胞分子生物学](../Page/细胞分子生物学.md "wikilink")

  - Q3 [遗传学](../Page/遗传学.md "wikilink")

:\*Q31 [遗传与](../Page/遗传.md "wikilink")[变异](../Page/变异.md "wikilink")

:\*Q32 [杂交与](../Page/杂交.md "wikilink")[杂种](../Page/杂种.md "wikilink")

:\*Q33
[人工选择与](../Page/人工选择.md "wikilink")[自然选择](../Page/自然选择.md "wikilink")

:\*Q34 遗传学分支学科

:\*Q36 [微生物遗传学](../Page/微生物遗传学.md "wikilink")

:\*Q37 [植物遗传学](../Page/植物遗传学.md "wikilink")

:\*Q38 [动物遗传学](../Page/动物遗传学.md "wikilink")

:\*Q39 [人类遗传学](../Page/人类遗传学.md "wikilink")

  - Q4 [生理学](../Page/生理学.md "wikilink")

:\*Q41 普通生理学

:\*Q42 [神经生理学](../Page/神经生理学.md "wikilink")

:\*Q43 [分析器生理学](../Page/分析器生理学.md "wikilink")

:\*Q44 [运动器官生理学](../Page/运动器官生理学.md "wikilink")

:\*Q45 [内分泌生理学](../Page/内分泌生理学.md "wikilink")

:\*Q46 [循环生理学](../Page/循环生理学.md "wikilink")

:\*Q47 [呼吸生理学](../Page/呼吸生理学.md "wikilink")

:\*Q48 [消化生理学](../Page/消化生理学.md "wikilink")

::\*Q491 [排泄生理学](../Page/排泄生理学.md "wikilink")

::\*Q492 [生殖生理学](../Page/生殖生理学.md "wikilink")

::\*Q493
[新陈代谢与](../Page/新陈代谢.md "wikilink")[营养](../Page/营养.md "wikilink")

::\*Q494
[特殊环境生理学](../Page/特殊环境生理学.md "wikilink")、[生态生理学](../Page/生态生理学.md "wikilink")

::\*Q495
[比较生理学与](../Page/比较生理学.md "wikilink")[进化生理学](../Page/进化生理学.md "wikilink")

  - Q5 [生物化学](../Page/生物化学.md "wikilink")

:\*Q50 一般性问题

:\*Q51 [蛋白质](../Page/蛋白质.md "wikilink")

:\*Q52 [核酸](../Page/核酸.md "wikilink")

:\*Q53 [糖](../Page/糖.md "wikilink")

:\*Q54 [脂类](../Page/脂类.md "wikilink")

:\*Q55 [酶](../Page/酶.md "wikilink")

:\*Q56 [维生素](../Page/维生素.md "wikilink")

:\*Q57 [激素](../Page/激素.md "wikilink")

:\*Q58 [生物体其他化学成分](../Page/生物体.md "wikilink")

::\*Q591[物质代谢及](../Page/物质代谢.md "wikilink")[能量代谢](../Page/能量代谢.md "wikilink")

::\*Q592 [体液代谢](../Page/体液代谢.md "wikilink")

::\*Q593 [器官生物化学](../Page/器官生物化学.md "wikilink")

::\*Q594 [比较生物化学](../Page/比较生物化学.md "wikilink")

::\*Q595 [应用生物化学](../Page/应用生物化学.md "wikilink")

  - Q6 [生物物理学](../Page/生物物理学.md "wikilink")

:\*Q61 [理论生物物理学](../Page/理论生物物理学.md "wikilink")

:\*Q62 [生物声学](../Page/生物声学.md "wikilink")

:\*Q63 [生物光学](../Page/生物光学.md "wikilink")

:\*Q64 [生物电磁学](../Page/生物电磁学.md "wikilink")

:\*Q65 [生物热学](../Page/生物热学.md "wikilink")

:\*Q66 [生物力学](../Page/生物力学.md "wikilink")

:\*Q67 [物体化学生物学](../Page/物体化学生物学.md "wikilink")

:\*Q68 物理因素对[生物的作用](../Page/生物.md "wikilink")

::\*Q691
[辐射生物学](../Page/辐射生物学.md "wikilink")（[放射生物学](../Page/放射生物学.md "wikilink")）

::\*Q692 [仿生学](../Page/仿生学.md "wikilink")

::\*Q693 [空间生物学](../Page/空间生物学.md "wikilink")

  - Q7 [分子生物学](../Page/分子生物学.md "wikilink")

:\*Q71 [生物大分子的结构和功能](../Page/生物大分子.md "wikilink")

:\*Q73 [生物膜的结构和功能](../Page/生物膜.md "wikilink")

:\*Q74 [生物小分子的结构和功能](../Page/生物小分子.md "wikilink")

:\*Q75 [分子遗传学](../Page/分子遗传学.md "wikilink")

:\*Q77 [生物能的转换](../Page/生物能.md "wikilink")

:\*Q78
[基因工程](../Page/基因工程.md "wikilink")（[遗传工程](../Page/遗传工程.md "wikilink")）

:\*Q81 [生物工程学](../Page/生物工程学.md "wikilink")

::\*Q811 [仿生学](../Page/仿生学.md "wikilink")

::\*Q813 [细胞工程](../Page/细胞工程.md "wikilink")

::\*Q814 [酶工程](../Page/酶工程.md "wikilink")

::\*Q819 生物工程应用

:\*Q89 [环境生物学](../Page/环境生物学.md "wikilink")

:\*Q91 [古生物学](../Page/古生物学.md "wikilink")

:\*Q93 [微生物学](../Page/微生物学.md "wikilink")

:::\*Q939.1 [细菌学](../Page/细菌学.md "wikilink")

:\*Q94 [植物学](../Page/植物学.md "wikilink")

:::\*Q94-49 普及读物

:::\*Q94-61 植物学词典

:\*Q95 [动物学](../Page/动物学.md "wikilink")

:::\*Q95-49 普及读物

:::\*Q95-61 动物学词典

:\*Q96 [昆虫学](../Page/昆虫学.md "wikilink")

:\*Q98 [人类学](../Page/人类学.md "wikilink")

:::\*Q981.1 [人类起源论](../Page/人类起源论.md "wikilink")

::\*Q987 [人类遗传学](../Page/人类遗传学.md "wikilink")

-----

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")