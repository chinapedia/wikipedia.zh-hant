**油尖旺區足球代表隊**（**Yau Tsim Mong
FT**，**YTMFT**）成立於2003年，是香港的一支地區足球隊，由**旺角區文娛康樂體育會有限公司**管理。現時於[香港乙組足球聯賽作賽](../Page/香港乙組足球聯賽.md "wikilink")，前教練是香港老牌球會[東方前隊長](../Page/東方足球隊.md "wikilink")[趙俊明](../Page/趙俊明.md "wikilink")，其弟[趙俊文現則擔任主教練](../Page/趙俊文.md "wikilink")。

## 球隊歷史

[香港足球總會依照](../Page/香港足球總會.md "wikilink")[荷蘭足球顧問報告](../Page/荷蘭.md "wikilink")，與全[香港各區區議會合作](../Page/香港.md "wikilink")，於2002年成立[香港丙組(地區)聯賽](../Page/香港丙組\(地區\)聯賽.md "wikilink")，首屆賽事只有11個區議會派隊參賽，加上[香港學界及](../Page/香港學界足球隊.md "wikilink")[香港08共](../Page/香港08.md "wikilink")13隊參賽\[1\]，翌屆全香港18個區議會均有派隊。

油尖旺區足球代表隊成立於2003年，代表[油尖旺區參加](../Page/油尖旺區.md "wikilink")[香港丙組(地區)足球聯賽](../Page/香港丙組\(地區\)足球聯賽.md "wikilink")。球會宗旨是培訓新一代的足球精英，及安排有系統經常訓練及比賽機會，使年青球員能參與較佳水平的比賽。

油尖旺區足球代表隊在2005-06年度球季，在[香港丙組(地區)足球聯賽獲得亞軍](../Page/香港丙組\(地區\)足球聯賽.md "wikilink")，得以晉身[丙組總決賽](../Page/香港丙組足球聯賽.md "wikilink")，最終獲得丙組總亞軍，首次升上[香港乙組足球聯賽作賽](../Page/香港乙組足球聯賽.md "wikilink")。

不過由於實力所限，油尖旺在2006-07年度[香港乙組足球聯賽](../Page/香港乙組足球聯賽.md "wikilink")，以20戰1勝4和15負只得7分，敬陪末席，在[乙組聯賽只角逐了一個球季](../Page/香港乙組足球聯賽.md "wikilink")，便要降回[丙組作賽](../Page/香港丙組足球聯賽.md "wikilink")。

在2008-09年度球季，油尖旺在[香港丙組(地區)足球聯賽](../Page/香港丙組\(地區\)足球聯賽.md "wikilink")，以14戰5勝4和5負得19分，在15支球隊中排名第7。

2009-10年度[香港丙組(地區)足球聯賽](../Page/香港丙組\(地區\)足球聯賽.md "wikilink")，油尖旺在煞科戰以1-5敗於[深水埗](../Page/深水埗體育會足球隊.md "wikilink")，和上季一樣以第7名完成賽季\[2\]\[3\]。

[香港足球總會於](../Page/香港足球總會.md "wikilink")2012-13年度球季，重組[丙組聯賽](../Page/香港丙組足球聯賽.md "wikilink")，並復辦[丁組聯賽](../Page/香港丁組足球聯賽.md "wikilink")。在上一屆[香港丙組(地區)聯賽](../Page/香港丙組\(地區\)聯賽.md "wikilink")，油尖旺以14戰2勝6和6負只得12分，在8支球隊中排名第7\[4\]，故需降落[丁組聯賽角逐](../Page/香港丁組足球聯賽.md "wikilink")。

2012年4月21日舉行的一場[丁組聯賽](../Page/香港丁組足球聯賽.md "wikilink")，油尖旺以31-0大勝[北區足球會](../Page/北區足球會.md "wikilink")，創下[香港足球總會賽事有紀錄以來最大比數紀錄](../Page/香港足球總會.md "wikilink")。油尖旺最終贏得復辦的首屆[丁組聯賽冠軍](../Page/香港丁組足球聯賽.md "wikilink")，回升[丙組作賽](../Page/香港丙組足球聯賽.md "wikilink")。

## 球隊近況

在2013-14年度球季，油尖旺以22勝3和1負得69分，獲得[丙組聯賽亞軍](../Page/香港丙組足球聯賽.md "wikilink")\[5\]，取得升班資格。由於[香港超級聯賽成立](../Page/香港超級聯賽.md "wikilink")，原本[香港乙組足球聯賽改制為](../Page/香港乙組足球聯賽.md "wikilink")[香港甲組足球聯賽](../Page/香港甲組足球聯賽.md "wikilink")，油尖旺遂由丙組升上甲組作賽。

2014-15年度球季，油尖旺羅致了[陳耀麟](../Page/陳耀麟.md "wikilink")、[巢鵬飛](../Page/巢鵬飛.md "wikilink")、[劉嘉城](../Page/劉嘉城.md "wikilink")、[曾昭達](../Page/曾昭達.md "wikilink")、[李思豪等多名前甲組球員](../Page/李思豪.md "wikilink")，成為爭升班熱門分子。油尖旺於[香港足總盃初賽獲得亞軍](../Page/香港足總盃.md "wikilink")，取得越級挑戰資格參加決賽圈賽事，第一圈面對超級聯賽球隊[和富大埔](../Page/和富大埔.md "wikilink")\[6\]，結果在力戰之下以2比4落敗\[7\]，惟表現可說是雖敗不辱\[8\]。

2015一16年度球季，羅致[迪達](../Page/迪達.md "wikilink")、[蔡國威](../Page/蔡國威.md "wikilink")、[荊騰](../Page/荊騰.md "wikilink")、[蘭特](../Page/蘭特.md "wikilink")、[吳兆輝等球員](../Page/吳兆輝.md "wikilink")，希望在甲組拿到好成績。

2016一17年球季大部份球員加盟黃大仙，實力大減，降班大熱，季中多達11位球員離隊。

2016一17球季,聯賽26戰,3勝，1和，22負,只得10分，14隊排榜尾,降落乙組.

## 2018年-2019年球季

本季流失不少主力球員，2018年-2019年球季淪為乙組護級份子。

## 球隊榮譽

| <font color="white"> **榮譽** </font>           | <font color="white"> **次數** </font> | <font color="white"> **年度** </font>                |
| --------------------------------------------- | ----------------------------------- | -------------------------------------------------- |
| <font color="white"> **本 土 聯 賽** </font>      |                                     |                                                    |
| **[丁組聯賽](../Page/香港丁組足球聯賽.md "wikilink") 冠軍** | **1**                               | [2012–13](../Page/2012年－13年香港丁組足球聯賽.md "wikilink") |
| **[丙組聯賽](../Page/香港丙組足球聯賽.md "wikilink") 亞軍** | **1**                               | 2005–06、2013-14                                    |
| <font color="white"> **本 土 盃 賽** </font>      |                                     |                                                    |
| **[初級組銀牌](../Page/香港初級組銀牌.md "wikilink") 8強** | **1**                               | 2005–06                                            |

## 管理層及職球員名單

### 管理層

<table>
<tbody>
<tr class="odd">
<td><p>名譽領隊：</p></td>
<td><p><a href="../Page/林健康.md" title="wikilink">林健康</a></p></td>
</tr>
<tr class="even">
<td><p>領隊：</p></td>
<td><p><a href="../Page/侯永昌.md" title="wikilink">侯永昌</a></p></td>
</tr>
<tr class="odd">
<td><p>副領隊：</p></td>
<td><p><a href="../Page/羅犖銘.md" title="wikilink">羅犖銘</a></p></td>
</tr>
<tr class="even">
<td><p>醫務顧問：</p></td>
<td><p><a href="../Page/梁華勝.md" title="wikilink">梁華勝</a></p></td>
</tr>
</tbody>
</table>

### 職員名單

<table>
<tbody>
<tr class="odd">
<td><p>主教練：</p></td>
<td><p><a href="../Page/趙俊文.md" title="wikilink">趙俊文</a></p></td>
</tr>
<tr class="even">
<td><p>助教：</p></td>
<td><p><a href="../Page/何紀能.md" title="wikilink">何紀能</a></p></td>
</tr>
<tr class="odd">
<td><p>技術顧問：</p></td>
<td><p><a href="../Page/林傑榮.md" title="wikilink">林傑榮</a></p></td>
</tr>
</tbody>
</table>

### 球員名單（甲組）

<table style="width:180%;">
<colgroup>
<col style="width: 36%" />
<col style="width: 27%" />
<col style="width: 27%" />
<col style="width: 13%" />
<col style="width: 63%" />
<col style="width: 13%" />
</colgroup>
<thead>
<tr class="header">
<th><p><a href="../Page/號碼.md" title="wikilink"><font color="white">號碼</font></a></p></th>
<th><p><a href="../Page/國籍.md" title="wikilink"><font color="white">國籍</font></a></p></th>
<th><p><a href="../Page/球員名字.md" title="wikilink"><font color="white">球員名字</font></a></p></th>
<th><p><a href="../Page/出生日期.md" title="wikilink"><font color="white">出生日期</font></a></p></th>
<th><p><a href="../Page/加盟年份.md" title="wikilink"><font color="white">加盟年份</font></a></p></th>
<th><p><a href="../Page/前屬球會.md" title="wikilink"><font color="white">前屬球會</font></a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/守門員.md" title="wikilink"><font color="white">守門員</font></a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p><small></small></p></td>
<td><p><a href="../Page/蔡佳偉.md" title="wikilink">蔡佳偉</a> ( Tsoi, Kai Wai )</p></td>
<td><p>1998年10月3日</p></td>
<td><p>2015年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td><p><small></small></p></td>
<td><p><a href="../Page/陳頌善.md" title="wikilink">陳頌善</a> ( Chan, Chung Sin )</p></td>
<td><p>1992年2月21日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/屯門足球會.md" title="wikilink">屯門</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><small></small></p></td>
<td><p><a href="../Page/鍾永康.md" title="wikilink">鍾永康</a> (Chung, Wing Hong )</p></td>
<td><p>1980年8月19日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/首飾.md" title="wikilink">首飾</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/後衛.md" title="wikilink"><font color="white">後衛</font></a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p><a href="../Page/何駿笙.md" title="wikilink">何駿笙</a> (Ho, Chun Sang )</p></td>
<td><p>1994年7月22日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/北區足球會.md" title="wikilink">北區</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p><a href="../Page/林傑榮.md" title="wikilink">林傑榮</a> ( Lam Kit Wing )</p></td>
<td><p>1987年1月5日</p></td>
<td><p>2017年</p></td>
<td><p><a href="../Page/光華體育會.md" title="wikilink">光華體育會</a></p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td></td>
<td><p><a href="../Page/馮培志.md" title="wikilink">馮培志</a> ( FUNG, Pui Chi )</p></td>
<td><p>1989年11月2日</p></td>
<td><p>2017年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p><a href="../Page/謝貴華.md" title="wikilink">謝貴華</a> ( Tse Kwai Wah )</p></td>
<td><p>1983年2月4日</p></td>
<td><p>2017年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p><a href="../Page/黎志得.md" title="wikilink">黎志得</a> ( Lai, Chi Tak )</p></td>
<td><p>1996年3月14日</p></td>
<td><p>2016年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td></td>
<td><p><a href="../Page/王建邦.md" title="wikilink">王建邦</a> ( Wong, Kin Pong )</p></td>
<td><p>1998年8月21日</p></td>
<td><p>2017年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td></td>
<td><p><a href="../Page/柯炬廷.md" title="wikilink">柯炬廷</a> ( Or, Kui Ting )</p></td>
<td><p>1994年12月29日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/國強體育會.md" title="wikilink">國強體育會</a></p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p><a href="../Page/李家樂.md" title="wikilink">李家樂</a> ( Lee, Ka Lok )</p></td>
<td><p>1996年5月31日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/深水埗體育會.md" title="wikilink">深水埗</a></p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td></td>
<td><p><a href="../Page/許良發.md" title="wikilink">許良發</a> ( Hui, Leung Fat )</p></td>
<td><p>1996年12月22日</p></td>
<td><p>2016年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中場.md" title="wikilink"><font color="white">中場</font></a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td></td>
<td><p><a href="../Page/李文峯.md" title="wikilink">李文峯</a> ( Lee, Man Fung Felix )</p></td>
<td><p>1983年10月14日</p></td>
<td><p>2015年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td></td>
<td><p><a href="../Page/姜均潤.md" title="wikilink">姜均潤</a> ( Kiang, Kwan Yun )</p></td>
<td><p>1992年8月18日</p></td>
<td><p>2016年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td></td>
<td><p><a href="../Page/俞子鈞.md" title="wikilink">俞子鈞</a> ( Yu, Tsz Kwan Forrest )</p></td>
<td><p>1995年1月16日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/九巴元朗.md" title="wikilink">九巴元朗</a></p></td>
</tr>
<tr class="odd">
<td><p>38</p></td>
<td></td>
<td><p><a href="../Page/楊偉傑.md" title="wikilink">楊偉傑</a> ( Yeung, Wai Kit )</p></td>
<td><p>1978年3月17日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/北區足球會.md" title="wikilink">北區</a></p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td></td>
<td><p><a href="../Page/羅智斌.md" title="wikilink">羅智斌</a> ( Law, Chi Pan )</p></td>
<td><p>1995年11月2日</p></td>
<td><p>2016年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td></td>
<td><p><a href="../Page/鍾易禮.md" title="wikilink">鍾易禮</a> ( Chung, Yik Lai )</p></td>
<td><p>1996年9月6日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/九龍木球會.md" title="wikilink">九龍木球會</a></p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p><a href="../Page/梁震希.md" title="wikilink">梁震希</a> ( Leung, Chun Hei )</p></td>
<td><p>1983年11月26日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/龍城康體.md" title="wikilink">龍城康體</a></p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p><a href="../Page/梁博希.md" title="wikilink">梁博希</a> ( Leung, Pok Hei )</p></td>
<td><p>1995年8月30日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/理文流浪.md" title="wikilink">理文流浪</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p><a href="../Page/歐陽穎豪.md" title="wikilink">歐陽穎豪</a> ( Au Yeung, Wing Ho )</p></td>
<td><p>1995年11月3日</p></td>
<td><p>2016年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td></td>
<td><p><a href="../Page/方偉雄.md" title="wikilink">方偉雄</a> ( Fong Wai Hung  )</p></td>
<td><p>1991年12月4日</p></td>
<td><p>2017年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td></td>
<td><p><a href="../Page/陸迦翹.md" title="wikilink">陸迦翹</a> ( LUK, Ka Kiu Kavin )</p></td>
<td><p>1994年6月14日</p></td>
<td><p>2017年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><font color="white"><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td></td>
<td><p><a href="../Page/李駿緯.md" title="wikilink">李駿緯</a> ( Li, Chun Wai Vincent )</p></td>
<td><p>1994年12月1日</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/北區足球會.md" title="wikilink">北區</a></p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td></td>
<td><p><a href="../Page/謝雲奴.md" title="wikilink">謝雲奴</a>（Gervais Yao Kouassi）</p></td>
<td><p>1987年5月27日</p></td>
<td><p>2018年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td></td>
<td><p><a href="../Page/盧卡斯佩雷斯.md" title="wikilink">盧卡斯佩雷斯</a> ( Lucas Perez )</p></td>
<td><p>1988年9月10日</p></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/阿仙奴足球會.md" title="wikilink">阿仙奴</a></p></td>
</tr>
</tbody>
</table>

### 離隊球員（2016/17）

<table style="width:194%;">
<colgroup>
<col style="width: 36%" />
<col style="width: 45%" />
<col style="width: 36%" />
<col style="width: 45%" />
<col style="width: 13%" />
<col style="width: 18%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">號碼</font></p></th>
<th><p><font color="white">國籍</font></p></th>
<th><p><font color="white">球員名字</font></p></th>
<th><p><font color="white">位置</font></p></th>
<th><p><font color="white">出生日期</font></p></th>
<th><p><font color="white">加盟球會</font></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><font color="white">夏季轉會窗</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>23</strong></p></td>
<td></td>
<td><p><a href="../Page/蔡亮庭.md" title="wikilink">蔡亮庭</a>（Choi Leong Ting）</p></td>
<td><p>守門員</p></td>
<td><p>1981年2月22日</p></td>
<td><p><a href="../Page/東區體育會.md" title="wikilink">東區</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>32</strong></p></td>
<td></td>
<td><p><a href="../Page/邱世添.md" title="wikilink">邱世添</a>（Yau Sai Tim）</p></td>
<td><p>守門員</p></td>
<td><p>1991年8月15日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>3</strong></p></td>
<td></td>
<td><p><a href="../Page/石子峰.md" title="wikilink">石子峰</a>（Shek Tsz Fung）</p></td>
<td><p>後衛</p></td>
<td><p>1993年6月10日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>5</strong></p></td>
<td></td>
<td><p><a href="../Page/陳志輝.md" title="wikilink">陳志輝</a>（Chan Chi Fai）</p></td>
<td><p>後衛</p></td>
<td><p>1981年1月17日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="even">
<td><p><strong>15</strong></p></td>
<td></td>
<td><p><a href="../Page/陳景泰.md" title="wikilink">陳景泰</a>（Chan King Tai）</p></td>
<td><p>後衛</p></td>
<td><p>1977年8月22日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>22</strong></p></td>
<td></td>
<td><p><a href="../Page/黃永森.md" title="wikilink">黃永森</a>（Wong Wing Sum）</p></td>
<td><p>後衛</p></td>
<td><p>1984年10月4日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="even">
<td><p><strong>33</strong></p></td>
<td></td>
<td><p><a href="../Page/迪達（喀麥隆足球員）.md" title="wikilink">迪達</a>（Celistanus Tita Chou）</p></td>
<td><p>後衛</p></td>
<td><p>1985年7月2日</p></td>
<td><p><a href="../Page/東區體育會.md" title="wikilink">東區</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>4</strong></p></td>
<td></td>
<td><p><a href="../Page/戴偉崙.md" title="wikilink">戴偉崙</a>（Tai Wai Lun）</p></td>
<td><p>中場</p></td>
<td><p>1981年4月4日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="even">
<td><p><strong>8</strong></p></td>
<td></td>
<td><p><a href="../Page/何嘉健.md" title="wikilink">何嘉健</a>（Ho Ka Kin）</p></td>
<td><p>中場</p></td>
<td><p>1983年9月17日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>13</strong></p></td>
<td></td>
<td><p><a href="../Page/張偉輝.md" title="wikilink">張偉輝</a>（Cheung Wai Fai）</p></td>
<td><p>中場</p></td>
<td><p>1985年7月5日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="even">
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><a href="../Page/蔡國威.md" title="wikilink">蔡國威</a>（Chow Kwok Wai）</p></td>
<td><p>中場</p></td>
<td><p>1991年12月28日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>16</strong></p></td>
<td></td>
<td><p><a href="../Page/黃翠成.md" title="wikilink">黃翠成</a>（Wong Chui Shing）</p></td>
<td><p>中場</p></td>
<td><p>1991年3月2日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>18</strong></p></td>
<td></td>
<td><p><a href="../Page/鍾漢池.md" title="wikilink">鍾漢池</a>（Chung Hon Chee）</p></td>
<td><p>中場</p></td>
<td><p>1989年1月22日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>21</strong></p></td>
<td></td>
<td><p><a href="../Page/梁烙峰.md" title="wikilink">梁烙峰</a>（Leung Lok Fung）</p></td>
<td><p>中場</p></td>
<td><p>1985年4月23日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="even">
<td><p><strong>26</strong></p></td>
<td></td>
<td><p><a href="../Page/李庭浩.md" title="wikilink">李庭浩</a>（Lee Ting Ho）</p></td>
<td><p>中場</p></td>
<td><p>1984年2月2日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>29</strong></p></td>
<td></td>
<td><p><a href="../Page/吳兆輝.md" title="wikilink">吳兆輝</a>（Ng Siu Fai）</p></td>
<td><p>中場</p></td>
<td><p>1988年9月24日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>30</strong></p></td>
<td></td>
<td><p><a href="../Page/李曉亮.md" title="wikilink">李曉亮</a>（Lee Hiu Leong）</p></td>
<td><p>中場</p></td>
<td><p>1989年5月4日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/余偉鵬.md" title="wikilink">余偉鵬</a></p></td>
<td><p>中場</p></td>
<td></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="even">
<td><p><strong>9</strong></p></td>
<td></td>
<td><p><a href="../Page/陳家俊.md" title="wikilink">陳家俊</a>（Chan Ka Chun）</p></td>
<td><p>前鋒</p></td>
<td><p>1990年12月23日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>20</strong></p></td>
<td></td>
<td><p><a href="../Page/易志豪.md" title="wikilink">易志豪</a>（Yick Chi Ho）</p></td>
<td><p>前鋒</p></td>
<td><p>1988年2月14日</p></td>
<td><p><a href="../Page/灝天黃大仙.md" title="wikilink">灝天黃大仙</a></p></td>
</tr>
<tr class="even">
<td><p><font color="white">非轉會窗時期(夏季轉會窗後)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><font color="white">冬季轉會窗</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/李進榮.md" title="wikilink">李進榮</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/灣仔足球隊.md" title="wikilink">灣仔足球隊</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/余健華.md" title="wikilink">余健華</a>（Yu Kin Wa）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/張煥培.md" title="wikilink">張煥培</a></p></td>
<td></td>
<td><p>1992年8月18日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/盧偉強.md" title="wikilink">盧偉強</a></p></td>
<td></td>
<td><p>1978年12月7日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/羅偉傑.md" title="wikilink">羅偉傑</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/崔子棋.md" title="wikilink">崔子棋</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/蘇敬輝.md" title="wikilink">蘇敬輝</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/趙俊生.md" title="wikilink">趙俊生</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/葉文元.md" title="wikilink">葉文元</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/陳邵軒.md" title="wikilink">陳邵軒</a></p></td>
<td></td>
<td><p>1983年12月11日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>--</strong></p></td>
<td></td>
<td><p><a href="../Page/李祖耀.md" title="wikilink">李祖耀</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><font color="white">非轉會窗時期(冬季轉會窗後)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 2018年一2019年球員陣容(乙組)

  - [梁震希](../Page/梁震希.md "wikilink")

## 著名球員

**本地球員**

<div style="font-size: small">

  - [張春暉](../Page/張春暉.md "wikilink")

  - [吳梓軒](../Page/吳梓軒.md "wikilink")

  - [陳耀麟](../Page/陳耀麟.md "wikilink")

  - [陳豪文](../Page/陳豪文.md "wikilink")

  - [劉志強](../Page/劉志強.md "wikilink")

  - [巢鵬飛](../Page/巢鵬飛.md "wikilink")

  - [劉嘉城](../Page/劉嘉城.md "wikilink")

  - [曾昭達](../Page/曾昭達.md "wikilink")

<!-- end list -->

  - [鄺智軒](../Page/鄺智軒.md "wikilink")

</div>

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [油尖旺足球隊官方網站](http://www.mkcrsa.org.hk/event/ymtft/ymtft.html)
  - [香港足球總會網頁球會資料](http://www.hkfa.com/ch/club/55/detail/)

[Category:香港乙組聯賽球會](../Category/香港乙組聯賽球會.md "wikilink")
[Category:油尖旺區](../Category/油尖旺區.md "wikilink")
[Category:2003年建立的足球俱樂部](../Category/2003年建立的足球俱樂部.md "wikilink")

1.  [丙組「地區聯賽」開幕](http://hk.apple.nextmedia.com/sports/art/20021117/2950895)《[蘋果日報](../Page/蘋果日報.md "wikilink")》
2.  [丙組東方今演煞科戰](http://hk.apple.nextmedia.com/sports/art/20100404/13893839)《[蘋果日報](../Page/蘋果日報.md "wikilink")》
3.  [深水埗地區聯賽奪冠](http://hk.apple.nextmedia.com/sports/art/20100405/13896101)《[蘋果日報](../Page/蘋果日報.md "wikilink")》
4.  [2011/12丙組地區聯賽積分表](http://www.hkfa.com/zh-hk/match_score_table.php?leagueyear=2011-2012&leagueyear_id=456)
5.  [2013/14丙組地區聯賽積分表](http://www.hkfa.com/ch/leaguerank?leagueid=608&year=2013-2014)
6.  [油尖旺越級挑戰大埔](http://paper.wenweipo.com/2015/02/27/SP1502270021.htm)《香港文匯報》
7.  [油尖旺主帥：不敵大埔輸在經驗](https://hk.sports.yahoo.com/news/%E6%B2%B9%E5%B0%96%E6%97%BA%E4%B8%BB%E5%B8%A5-%E4%B8%8D%E6%95%B5%E5%A4%A7%E5%9F%94%E8%BC%B8%E5%9C%A8%E7%B6%93%E9%A9%97-073000332--sow.html)
8.  [香港足總盃
    甲組越級挑戰雖敗不辱](http://beyondnewsnet.com/20150305-hkfa-cup/)《超越新聞網》