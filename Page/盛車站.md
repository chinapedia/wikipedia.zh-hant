**盛車站**（）是一位於[日本](../Page/日本.md "wikilink")[岩手縣](../Page/岩手縣.md "wikilink")[大船渡市盛町東町裏](../Page/大船渡市.md "wikilink")，由[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）、[第三部門鐵道](../Page/日本第三部門鐵道.md "wikilink")[三陸鐵道與地方第三部門貨運業者](../Page/三陸鐵道.md "wikilink")[岩手開發鐵道所共用的](../Page/岩手開發鐵道.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")，也是大船渡市的主要車站之一。盛車站是JR東日本所屬的地方支線[大船渡線與三陸鐵道所屬的](../Page/大船渡線.md "wikilink")[谷灣線](../Page/谷灣線.md "wikilink")（，在轉移民營化之前原稱「盛線」）之交會車站，除了兩條客運路線外，該站也是岩手開發鐵道所屬的兩條貨運路線、[日頃市線與](../Page/日頃市線.md "wikilink")[赤崎線的交會點與起點](../Page/赤崎線.md "wikilink")，日頃市線在1992年之前曾有客運路線的經營，但目前僅保留貨運用途，惟站區內仍可見到當年留下、已被封閉的舊月台。

## 車站結構

[側式月台](../Page/側式月台.md "wikilink")1面1線與[島式月台](../Page/島式月台.md "wikilink")1面2線，合計2面3線的[地面車站](../Page/地面車站.md "wikilink")。因為配線關係，谷灣線於3號月台到發（包括與大船渡線[直通運行的情況](../Page/直通運行.md "wikilink")）。各月台以跨線天橋連絡。2013年4月26日起，1號月台與2、3號月台之間的軌道部分提升為專用道整備以便BRT直通至站內。專用道上設置了橫斷通道，BRT與三陸鐵道使用跨線橋連結。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>大船渡線</p></td>
<td><p>下車專用</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/氣仙沼站.md" title="wikilink">氣仙沼方向</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>三陸鐵道谷灣線</p></td>
<td><p>、<a href="../Page/釜石站.md" title="wikilink">釜石方向</a></p></td>
</tr>
</tbody>
</table>

## 相鄰車站

  - 東日本旅客鐵道

    大船渡線

      -
        [大船渡](../Page/大船渡站.md "wikilink")－**盛**

  - 三陸鐵道

    谷灣線

      -
        **盛**－

  - 岩手開發鐵道
    日頃市線

      -
        **盛**－

    赤崎線

      -
        **盛**－[赤崎](../Page/赤崎站.md "wikilink")

## 外部連結

  - [盛車站（JR東日本）](http://www.jreast.co.jp/estation/station/info.aspx?StationCd=733)

[Kari](../Category/日本鐵路車站_Sa.md "wikilink")
[Category:岩手縣鐵路車站](../Category/岩手縣鐵路車站.md "wikilink")
[Category:大船渡線車站](../Category/大船渡線車站.md "wikilink")
[Category:三陸鐵道車站](../Category/三陸鐵道車站.md "wikilink")
[Category:1935年日本建立](../Category/1935年日本建立.md "wikilink")
[Category:1935年启用的铁路车站](../Category/1935年启用的铁路车站.md "wikilink")
[Category:大船渡市](../Category/大船渡市.md "wikilink")
[Category:赤崎線車站](../Category/赤崎線車站.md "wikilink")
[Category:日頃市線車站](../Category/日頃市線車站.md "wikilink")