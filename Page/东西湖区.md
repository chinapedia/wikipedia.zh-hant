**东西湖区**是[中国](../Page/中国.md "wikilink")[湖北省](../Page/湖北省.md "wikilink")[武汉市所辖的一个](../Page/武汉市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，位于[汉口西部近郊](../Page/汉口.md "wikilink")，与[武汉临空港经济技术开发区合署办公](../Page/武汉临空港经济技术开发区.md "wikilink")，是全国食品工业强区、国家级新型工业化（食品产业）示范基地、国家级食品安全示范区、国家级现代农业示范核心区、国际级生态示范区。\[1\]

## 历史沿革

东西湖区辖区原属[汉阳](../Page/汉阳县.md "wikilink")（今[蔡甸区](../Page/蔡甸区.md "wikilink")）、[汉川](../Page/汉川.md "wikilink")、[孝感](../Page/孝感市.md "wikilink")、[黄陂四县](../Page/黄陂县.md "wikilink")，地势低洼，水灾频繁，[血吸虫病泛滥](../Page/血吸虫病.md "wikilink")。1957年春，经[中共中央](../Page/中共中央.md "wikilink")[国务院批准成立湖北省东西湖围垦工程指挥部展开](../Page/中华人民共和国国务院.md "wikilink")[围垦](../Page/围垦.md "wikilink")，由省长[张体学任总指挥长兼政委](../Page/张体学.md "wikilink")。同年12月成立武汉市国营农场管理局，1958年10月设立武汉市东西湖区，实行区、局合一的体制（1961-1973年曾撤区，武汉市国营农场管理局后更名为武汉市国营东西湖农场管理局）。\[2\]

2010年，东西湖区吴家山经济开发区获得国务院批准并升级为[国家级经济技术开发区](../Page/国家级经济技术开发区.md "wikilink")。2012年，吴家山经济技术开发区与东西湖区实现行政建制并存。2013年3月，武汉吴家山经济技术开发区更名为[武汉临空港经济技术开发区](../Page/武汉临空港经济技术开发区.md "wikilink")。\[3\]

## 政治

### 行政区划

2017年，东西湖区共辖11个[街道办事处](../Page/街道办事处.md "wikilink")、1个社区管理委员会、5个开发区产业园\[4\]：

  - 街道：[吴家山街道](../Page/吴家山街道.md "wikilink")、[长青街道](../Page/长青街道_\(武汉市\).md "wikilink")、[慈惠街道](../Page/慈惠街道.md "wikilink")、[走马岭街道](../Page/走马岭街道.md "wikilink")、[新沟镇街道](../Page/新沟镇街道.md "wikilink")、[径河街道](../Page/径河街道.md "wikilink")、[金银湖街道](../Page/金银湖街道.md "wikilink")、[将军路街道](../Page/将军路街道_\(武汉市\).md "wikilink")、[辛安渡街道](../Page/辛安渡街道.md "wikilink")、[东山街道](../Page/东山街道_\(武汉市\).md "wikilink")、[柏泉街道](../Page/柏泉街道.md "wikilink")
  - 管委会：[常青花园社区](../Page/常青花园新区街道.md "wikilink")
  - 开发区：机电产业园、高新技术产业园、综合保税物流产业园、食品医药产业园、临空产业园

### 现任领导

<table>
<caption>武汉市东西湖区五大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党武汉市东西湖区委员会.md" title="wikilink">中国共产党<br />
武汉市东西湖区委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/武汉市东西湖区人民代表大会.md" title="wikilink">武汉市东西湖区人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/武汉市东西湖区人民政府.md" title="wikilink">武汉市东西湖区人民政府</a><br />
<br />
区长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议武汉市东西湖区委员会.md" title="wikilink">中国人民政治协商会议<br />
武汉市东西湖区委员会</a><br />
主席</p></th>
<th><p><br />
<a href="../Page/武汉市东西湖区监察委员会.md" title="wikilink">武汉市东西湖区监察委员会</a><br />
<br />
主任</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/陈邂馨.md" title="wikilink">陈邂馨</a>[5]</p></td>
<td><p><a href="../Page/李伟凡.md" title="wikilink">李伟凡</a>[6]</p></td>
<td><p><a href="../Page/彭涛_(湖北).md" title="wikilink">彭涛</a>[7]</p></td>
<td><p><a href="../Page/黄华_(江西).md" title="wikilink">黄华</a>[8]</p></td>
<td><p><a href="../Page/李先勇.md" title="wikilink">李先勇</a>[9]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/福建省.md" title="wikilink">福建省</a><a href="../Page/诏安县.md" title="wikilink">诏安县</a></p></td>
<td><p><a href="../Page/湖北省.md" title="wikilink">湖北省</a><a href="../Page/钟祥市.md" title="wikilink">钟祥市</a></p></td>
<td><p>湖北省<a href="../Page/武汉市.md" title="wikilink">武汉市</a></p></td>
<td><p><a href="../Page/江西省.md" title="wikilink">江西省</a><a href="../Page/抚州市.md" title="wikilink">抚州市</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年4月</p></td>
<td><p>2017年12月</p></td>
<td><p>2017年12月</p></td>
<td><p>2016年12月</p></td>
<td><p>2017年12月</p></td>
</tr>
</tbody>
</table>

## 经济

2017年东西湖区实现[地区生产总值](../Page/地区生产总值.md "wikilink")730.6亿元，按可比价计算，比上年增长8.1%。其中，[第一产业增加值](../Page/第一产业.md "wikilink")16.5亿元，增长4.0%；[第二产业增加值](../Page/第二产业.md "wikilink")545.8亿元，增长8.1%；[第三产业增加值](../Page/第三产业.md "wikilink")168.3亿元，增长8.5%，三次产业结构比重调整为2.3：74.7：23.0，按常住人口计算，全区人均生产总值132408元，比上年增加9639元。

2017年东西湖区完成全社会固定资产投资5861210万元，比上年增长17.1%。出口创汇完成4.07亿美元，增长19.1%。全年实现社会消费品零售总额1585754万元，比上年增长12.2%。

## 交通

东西湖区介于武汉市中心与[孝感市之间](../Page/孝感市.md "wikilink")，交通发达，毗邻[武汉天河国际机场](../Page/武汉天河国际机场.md "wikilink")、[汉口火车站](../Page/汉口站.md "wikilink")，区内有[武汉三环线](../Page/武汉三环线.md "wikilink")、[武汉四环线](../Page/武汉四环线.md "wikilink")、[武汉外环线](../Page/武汉绕城高速公路.md "wikilink")、[京港澳高速](../Page/京港澳高速.md "wikilink")、[沪蓉高速](../Page/沪蓉高速.md "wikilink")、[硚孝高速](../Page/硚孝高速.md "wikilink")、[机场高速](../Page/武汉机场高速公路.md "wikilink")、[机场二高速等高速公路或快速路](../Page/武汉机场第二高速.md "wikilink")，区内已营运的[武汉地铁线路有](../Page/武汉地铁.md "wikilink")[1号线](../Page/武汉轨道交通1号线.md "wikilink")、[2号线](../Page/武汉轨道交通2号线.md "wikilink")、[3号线](../Page/武汉轨道交通3号线.md "wikilink")、[6号线](../Page/武汉轨道交通6号线.md "wikilink")、[8号线](../Page/武汉轨道交通8号线.md "wikilink")。

## 参考文献

## 外部链接

  - [武汉市东西湖区政府网站](http://www.dxh.gov.cn/)

{{-}}

[东西湖区](../Category/东西湖区.md "wikilink")
[Category:武汉市辖区](../Category/武汉市辖区.md "wikilink")
[Category:武汉市远城区](../Category/武汉市远城区.md "wikilink")

1.

2.

3.
4.

5.

6.

7.
8.

9.