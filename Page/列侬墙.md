[JohnLennonWall.jpg](https://zh.wikipedia.org/wiki/File:JohnLennonWall.jpg "fig:JohnLennonWall.jpg")

**列侬墙**
（捷克語：），位於[捷克首都](../Page/捷克.md "wikilink")[布拉格](../Page/布拉格.md "wikilink")[修道院大广场](../Page/修道院大广场.md "wikilink")（捷克語：），原本是[医院骑士团](../Page/医院骑士团.md "wikilink")（Ordre
des
Hospitaliers）所有的一面普通的墙，1980年代起人们开始在这面墙上涂写[约翰·列侬](../Page/约翰·列侬.md "wikilink")（John
Lennon）风格的[涂鸦以及](../Page/涂鸦.md "wikilink")[披头士乐队](../Page/披头士乐队.md "wikilink")（Beatles）歌词的片段。

1988年，列侬墙成为[捷克群众发泄对于](../Page/捷克.md "wikilink")[胡萨克共产主义政体愤怒的源头](../Page/胡萨克.md "wikilink")。大批捷克青年在墙上书写不满的标语，最终在[查理大桥附近导致了一场学生](../Page/查理大桥.md "wikilink")、警察之间的大规模冲突，共有几百人被卷入。参与这次行动的学生被讽刺地称为列侬主义者，捷克当局把这些受过教育的和平人士分别诬蔑成酗酒者、精神病患者、反社会分子和西方[资本主义间谍](../Page/资本主义.md "wikilink")。

最初的列侬肖像早就被一些新的涂鸦所覆盖，尽管当局曾经专门找人来进行重绘，但每次就在重绘的第二天，墙面上又会被鲜花图案和诗歌所填满。在当代，列侬墙已经成为表达青年理想——比如爱与和平——的一个象征性符号。

墙面的拥有者慷慨的同意这种形式的涂鸦继续下去，促成了如今可爱的列侬墙的新生。

## 參考文獻

## 外部链接

  - [數碼連儂牆 Digital Lennon Wall](http://www.lennonwall.org) (link)
  - [2005年的约翰·列侬墙](http://www.radio.cz/pictures/praha/lennon_zed.jpg)

## 參見

  - [連儂牆 (香港)](../Page/連儂牆_\(香港\).md "wikilink")

{{-}}

[Category:约翰·列侬](../Category/约翰·列侬.md "wikilink")
[Category:布拉格建筑物](../Category/布拉格建筑物.md "wikilink")
[Category:墙体结构](../Category/墙体结构.md "wikilink")