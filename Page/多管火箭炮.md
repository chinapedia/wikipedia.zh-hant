[ТОС-1А_Буратино_на_репетиции_парада_4.5.2010.jpg](https://zh.wikipedia.org/wiki/File:ТОС-1А_Буратино_на_репетиции_парада_4.5.2010.jpg "fig:ТОС-1А_Буратино_на_репетиции_парада_4.5.2010.jpg")A多管火箭\]\]
[MLRS-system.JPEG](https://zh.wikipedia.org/wiki/File:MLRS-system.JPEG "fig:MLRS-system.JPEG")和[M997載台構成的多管火箭炮陣地](../Page/M997.md "wikilink")\]\]
[MSTAVSGRAD.svg](https://zh.wikipedia.org/wiki/File:MSTAVSGRAD.svg "fig:MSTAVSGRAD.svg")
**多管火箭炮**（[英文](../Page/英文.md "wikilink")：**MRL**—Multiple rocket
launcher）是一種多發射管無制導的[火箭炮](../Page/火箭炮.md "wikilink")，與[火箭炮類似](../Page/火箭炮.md "wikilink")，多管火箭炮精度及裝填速度低，但可在短時間發射大量爆炸性的火箭以命中大範圍目標。

比較著名的有[二戰中的蘇軍](../Page/二戰.md "wikilink")[BM-13卡秋莎](../Page/BM-13卡秋莎.md "wikilink")。

## 一些國家的多管火箭炮

  - —
    [M270多管火箭系統](../Page/M270多管火箭系統.md "wikilink")、[M142高机动性多管火箭系统](../Page/M142高机动性多管火箭系统.md "wikilink")

  - — [BM-13
    喀秋莎火箭炮](../Page/喀秋莎火箭炮.md "wikilink")、[BM-14](../Page/BM-14.md "wikilink")、、、[BM-30](../Page/BM-30火箭炮.md "wikilink")、[TOS-1](../Page/TOS-1火箭炮.md "wikilink")（TOC-1）

  - — [75式130mm自走多管火箭車](../Page/75式130mm自走多管火箭車.md "wikilink")

  - —
    [A3式野战火箭炮](../Page/A3式野战火箭炮.md "wikilink")、[63式火箭炮](../Page/63式火箭炮.md "wikilink")、[70式火箭炮](../Page/70式火箭炮.md "wikilink")、[81式火箭炮](../Page/81式火箭炮.md "wikilink")、[83式火箭炮](../Page/83式火箭炮.md "wikilink")、[89式火箭炮](../Page/89式火箭炮.md "wikilink")、[A100式火箭炮](../Page/A100式火箭炮.md "wikilink")、[03式火箭炮](../Page/03式火箭炮.md "wikilink")、[衛士型火箭炮](../Page/衛士型火箭炮.md "wikilink")、[SR5型多管火箭炮](../Page/SR5型多管火箭炮.md "wikilink")

  - —
    [工蜂四型](../Page/工蜂四型.md "wikilink")、[工蜂六型](../Page/工蜂六型.md "wikilink")、[工蜂七型](../Page/工蜂七型多管火箭.md "wikilink")、[雷霆2000多管火箭系統](../Page/雷霆2000多管火箭系統.md "wikilink")

  - — SLAM

  - — [Astros II MLRS](../Page/Astros_II_MLRS.md "wikilink")

  - — [Pinaka](../Page/Pinaka.md "wikilink")

  - — [LAROM](../Page/LAROM.md "wikilink")

  - — [Valkiri](../Page/Valkiri.md "wikilink")

  - — [TOROS](../Page/TOROS.md "wikilink")

## 民用科技

2013年起中國航天科工二院206所；開始研發[航天與軍用科技轉為](../Page/航天.md "wikilink")[消防用途的計畫](../Page/消防.md "wikilink")，其中就以多管火箭炮科技轉化為一種[消防車](../Page/消防車.md "wikilink")，針對摩天大樓滅火的困難，能發射一種裝滿滅火乾粉的多管火箭射入高樓層，每一枚彈能撲滅60立方公尺火焰\[1\]，為危急受困者提供立即支援。

## 参见

  - [反潜火箭深弹发射器](../Page/反潜火箭深弹发射器.md "wikilink")
  - [央視-明朝多管火箭之謎](https://www.youtube.com/watch?v=Ce6cxb1RS5w)
  - [神機箭](../Page/神機箭.md "wikilink")

[Category:多管火箭炮](../Category/多管火箭炮.md "wikilink")
[Category:火箭炮](../Category/火箭炮.md "wikilink")

1.  [央視-多管火箭消防車](http://tv.cctv.com/2017/03/17/VIDEzmnFdc41h6LjArNWELoc170317.shtml)