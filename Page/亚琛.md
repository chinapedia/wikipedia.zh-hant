[Aachen_Germany_Imperial-Cathedral-01.jpg](https://zh.wikipedia.org/wiki/File:Aachen_Germany_Imperial-Cathedral-01.jpg "fig:Aachen_Germany_Imperial-Cathedral-01.jpg")
[Aachen_BW_2016-07-09_11-56-24.jpg](https://zh.wikipedia.org/wiki/File:Aachen_BW_2016-07-09_11-56-24.jpg "fig:Aachen_BW_2016-07-09_11-56-24.jpg")

**亞琛**（，，）是位於[德國](../Page/德國.md "wikilink")[北萊茵-威斯特法倫州的一個城市](../Page/北萊茵-威斯特法倫.md "wikilink")，靠近[比利時與](../Page/比利時.md "wikilink")[荷蘭邊境](../Page/荷蘭.md "wikilink")，是德國最西部的城市。

亞琛以[溫泉](../Page/溫泉.md "wikilink")（歐洲中部最熱的溫泉）著名，從西元1世紀起这里就是療養地。這個城市是一個重要的[鐵路匯集點](../Page/鐵路.md "wikilink")，也是[工業與集會中心](../Page/工業.md "wikilink")。經濟以[食品工業為主](../Page/食品工業.md "wikilink")，主要生產[杏仁糖](../Page/杏仁糖.md "wikilink")、[巧克力](../Page/巧克力.md "wikilink")、Printen餅。
此外亦有[機械](../Page/機械.md "wikilink")、[鐵路設備及](../Page/鐵路設備.md "wikilink")[紡織業](../Page/紡織業.md "wikilink")。此外，德國約20%的[毛織品在亚琛生產](../Page/毛織品.md "wikilink")。

## 歷史

這個城市早年由[凱爾特人與](../Page/凱爾特人.md "wikilink")[羅馬人相继統治](../Page/羅馬人.md "wikilink")，具有豐富的歷史見證。人們將亞琛視為[查理曼的出生地](../Page/查理曼.md "wikilink")。在查理曼統治的時期（800年－814年），於亞琛建造宮廷禮拜堂，也就是[亞琛大教堂](../Page/亞琛大教堂.md "wikilink")。由於其無與倫比的歷史地位、建築風格，以及收藏於其內的曠世藝術品，亞琛大教堂成為德國第一座列入[聯合國教科文組織](../Page/聯合國教科文組織.md "wikilink")[世界文化遗产的建筑瑰寶](../Page/世界文化遗产.md "wikilink")。

[查理曼將這個城市建造成](../Page/查理曼.md "wikilink")[卡洛林文化的中心](../Page/卡洛林.md "wikilink")，開創了[中世紀早期第一個具代表性的文化復興](../Page/中世紀.md "wikilink")。西元813年至1532年間，有三十二個[神聖羅馬帝國](../Page/神聖羅馬帝國.md "wikilink")[皇帝在此加冕](../Page/神聖羅馬帝國皇帝.md "wikilink")。1166年和1215年兩次獲得[城市特許狀](../Page/城市特許狀.md "wikilink")。1250年左右成為帝國的[自由城市](../Page/自由城市.md "wikilink")。不過由於接近[法國邊境](../Page/法國歷史.md "wikilink")，又遠離[神聖羅馬帝國中心](../Page/神聖羅馬帝國.md "wikilink")，1562年皇帝加冕儀式改至[法蘭克福進行](../Page/法蘭克福.md "wikilink")。1668年起成為數次[和平會議會址](../Page/和平會議.md "wikilink")。

[法國大革命期間](../Page/法國大革命.md "wikilink")（1789年－1797年），亞琛為[法國所佔領](../Page/法國.md "wikilink")，1801年正式割讓給[法國](../Page/法國.md "wikilink")，直到1815年[拿破崙戰敗](../Page/拿破崙.md "wikilink")，根据[维也纳会议](../Page/维也纳会议.md "wikilink")，亞琛由[普魯士王国管理](../Page/普魯士王国.md "wikilink")。這個城市在[第二次世界大战末期](../Page/第二次世界大战.md "wikilink")，成為被盟軍攻佔的第一座德國城市（1944年10月20日），並受創於強大的空襲與陸戰。1966年，[亞琛開始進行重建](../Page/亞琛.md "wikilink")。1999年1月31日為[亞琛大教堂成立一個歐洲基金委員會](../Page/亞琛大教堂.md "wikilink")，在這委員會中，許多國家的市長答應支持大教堂的維護。這個源於[卡洛林時代的建築紀念物](../Page/卡洛林.md "wikilink")，在1978年由[UNESCO列為](../Page/UNESCO.md "wikilink")[世界文化遺產](../Page/世界文化遺產.md "wikilink")。目前[亞琛的人口在](../Page/亞琛.md "wikilink")2001年統計約為24萬4千人。

亞琛國際性的[查理曼獎於](../Page/查理曼獎.md "wikilink")1950年開始舉辦，是最老也是最有名的獎項，頒給在行為和研究機關表現優異，而且對[歐洲國家統一有貢獻者](../Page/歐洲.md "wikilink")，2000年[美國總統](../Page/美國總統.md "wikilink")[比尔·克林顿也曾在此受頒獎項](../Page/比尔·克林顿.md "wikilink")。

世界知名單練和[马术競技活動](../Page/马术.md "wikilink")「CHIO」也是亞珅的民間節日，每年的八月都會舉辦活動，吸引來自世界各地愛馬的朋友來此聚會。

## 景點

值得參觀的景點如[市政廳](../Page/亞琛市政廳.md "wikilink")（Rathaus）、及[亞琛大教堂](../Page/亞琛大教堂.md "wikilink")（Aachener
Dom）。市政廳在1353年從[查理大帝](../Page/查理大帝.md "wikilink")（或）皇宫的廢墟中建起。亞琛大教堂之祭壇建於約13至14世紀左右，其中還有查理大帝的寶座及墳墓。

[報紙博物館是為了紀念名為Paul](../Page/報紙博物館.md "wikilink") Julius von
reuter的德國記者而設置的，其中收集了約三十種語言、超過十二萬的報紙與雜誌。

1870年[亞琛工業大學](../Page/亞琛工業大學.md "wikilink")（RWTH
Aachen）設立，孕育出不少傑出的工程界人士。

[Suermondt
ludwig美術館](https://web.archive.org/web/20050824133827/http://museen.aachen.de/content/mus/slm/index.html)為[威尼斯式](../Page/威尼斯.md "wikilink")[文藝復興建築](../Page/文藝復興.md "wikilink")，在十九世紀中葉為著名的亞琛城市官邸，本為Cassalette住處，其中保留著豪華的入口階梯玄關，裝飾屋頂，及壁畫。1992年到1994年，建築師Bussmann與Haberer在舊建築物一旁設計新的藝術中心，呈現新與舊建築藝術的融合。美術館現為美術展示與美術教育用之空間。

位在[亞琛市中心外北端的](../Page/亞琛.md "wikilink")[盧斯山](../Page/盧斯山.md "wikilink")（Lousberg），不只有著有趣的傳說，亦是值得踏青的地方。亞琛溫泉也是相當有名。

## 嘉年華會

「亚琛位於[萊茵河畔](../Page/萊茵河.md "wikilink")」，此一宣稱並不為眾人所熟悉，因該城距離萊茵河尚有75公里，但當人們認識亚琛嘉年華，就不會對這個地理上的歸屬性感到懷疑，因為除了[科隆](../Page/科隆.md "wikilink")、[杜塞尔多夫和](../Page/杜塞尔多夫.md "wikilink")[美因茲之外](../Page/美因茲.md "wikilink")，亚琛也被視為一個信徒的堡壘，而且早在19世紀，嘉年華的第一個協會就在這裡成立。嘉年華的格言是跟朋友玩笑逗趣，這裡的居民以唱歌、跳舞、喝酒來歡度這個節日。在11月11日的11點11分開始到2月的綠色星期四（grüner
Donnerstag），會有超過五十個嘉年華聚會在亞琛舉行。

每年的復活節的前一個星期到復活節後的一個星期，在阿亨的 Bendplatz 廣场會有各式各樣吸引人的活動和玩樂，
例如[雲霄飛車](../Page/雲霄飛車.md "wikilink")、[碰碰車](../Page/碰碰車.md "wikilink")、迷宮等這些遊戲，除此之外還賣一些應節食品，吸引了很多住在亞琛當地和周圍城市的人前來。

## 友好城市

  - [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")[法国](../Page/法国.md "wikilink")[兰斯](../Page/兰斯.md "wikilink")
  - [Flag_of_the_United_Kingdom.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg "fig:Flag_of_the_United_Kingdom.svg")[英国](../Page/英国.md "wikilink")[哈利法克斯](../Page/哈利法克斯.md "wikilink")
  - [Flag_of_Spain.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Spain.svg "fig:Flag_of_Spain.svg")[西班牙](../Page/西班牙.md "wikilink")[托莱多](../Page/托莱多.md "wikilink")
  - [Flag_of_the_People's_Republic_of_China.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_China.svg "fig:Flag_of_the_People's_Republic_of_China.svg")[中国](../Page/中国.md "wikilink")[宁波](../Page/宁波.md "wikilink")
  - [Flag_of_Germany.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Germany.svg "fig:Flag_of_Germany.svg")[德国](../Page/德国.md "wikilink")[瑙姆堡](../Page/瑙姆堡.md "wikilink")
  - [Flag_of_the_United_States.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_United_States.svg "fig:Flag_of_the_United_States.svg")[美国](../Page/美国.md "wikilink")[阿灵顿县](../Page/阿灵顿县.md "wikilink")
  - [Flag_of_Russia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Russia.svg "fig:Flag_of_Russia.svg")[俄罗斯](../Page/俄罗斯.md "wikilink")[科斯特罗马](../Page/科斯特罗马.md "wikilink")

## 外部連結

  - [亚琛城市入口網站](http://www.aachen.de)
  - [亚琛縣轄區交通查詢](http://www.aseag.de/)
  - [亚琛工業大學](http://www.rwth-aachen.de/)
  - [亚琛應用科技大學](http://www.fh-aachen.de/)
  - [亚琛中国学生学者联合会](http://www.vcwsa.rwth-aachen.de/)
  - [亚琛汤若望协会](http://www.adamschall.de/)
  - [亚琛华人基督徒团契](http://www.caachen.de/)
  - [亚琛动物园](http://www.euregiozoo.de/)
  - [亚琛三国交界](http://www.aachen.de/de/kultur_freizeit/vereine/vereine_suche/index.asp?id_address=8985&id_stadt=0)

[Category:北莱茵-威斯特法伦州市镇](../Category/北莱茵-威斯特法伦州市镇.md "wikilink")
[Category:古都](../Category/古都.md "wikilink")
[Category:帝國自由城市](../Category/帝國自由城市.md "wikilink")
[Category:阿亨](../Category/阿亨.md "wikilink")