**气象卫星**是[人造卫星的一种](../Page/人造卫星.md "wikilink")，其主要作用是观察和监视[地球的](../Page/地球.md "wikilink")[气象和](../Page/气象.md "wikilink")[气候](../Page/气候.md "wikilink")。

气象卫星不只可以观察[云的系统](../Page/云.md "wikilink")，城市灯光、[火灾](../Page/火灾.md "wikilink")、大气和水[污染](../Page/污染.md "wikilink")、[极光](../Page/极光.md "wikilink")、沙暴、冰雪覆盖率、[海流和](../Page/海流.md "wikilink")[能源浪费等等都是气象卫星可以收集到的信息](../Page/能源.md "wikilink")。

## 历史

[TIROS_photo_products_Spac0061.jpg](https://zh.wikipedia.org/wiki/File:TIROS_photo_products_Spac0061.jpg "fig:TIROS_photo_products_Spac0061.jpg")
世界上第一颗气象卫星是1959年2月17日发射的[先锋2号卫星](../Page/先锋2号.md "wikilink")，它本来是打算被用来观察云的，但它的自转轴不稳定，因此它的数据无法被利用。

世界上第一颗成功的气象卫星是[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")1960年4月1日发射的[TIROS
1号](../Page/TIROS-1.md "wikilink")[卫星](../Page/卫星.md "wikilink")，**TIROS**一共运行了78天，它的成功为以后的气象卫星铺平了道路。

## 类型\[1\]

[Himawari-8_true-color_2015-01-25_0230Z.png](https://zh.wikipedia.org/wiki/File:Himawari-8_true-color_2015-01-25_0230Z.png "fig:Himawari-8_true-color_2015-01-25_0230Z.png")（Himawari
8）衛星首張全彩合成PNG雲圖\]\]

气象卫星大致可以分两类：[同步卫星和极轨卫星](../Page/同步卫星.md "wikilink")。

### 同步衛星

同步气象卫星在距[赤道](../Page/赤道.md "wikilink")[海平面](../Page/海平面.md "wikilink")35,786公里高之[地球靜止軌道处环绕地球](../Page/地球靜止軌道.md "wikilink")。它的轨道可以使它环绕地球的公转周期与地球的自转周期相等，因此它可以不断地向地面输送地球表面一个地区的可见光和红外线图片。一般新闻报道使用的图片都是同步卫星的图片。

目前正在运行的同步气象卫星有[中国的](../Page/中国.md "wikilink")[风云2号和](../Page/风云二号气象卫星.md "wikilink")[4号](../Page/风云四号气象卫星.md "wikilink")，美國的[GOES
11和](../Page/GOES_11.md "wikilink")[GOES
12](../Page/GOES_12.md "wikilink")、日本的[MTSTAT
1R](../Page/MTSTAT_1R.md "wikilink")。**GOES
12**位于75°W，[亚马遜河上](../Page/亚马遜河.md "wikilink")，主要为美国提供气象信息。**GOES
11**位于135°W，[太平洋东部](../Page/太平洋.md "wikilink")。**MTSTAT
1R**位於140°E。[欧洲人使用](../Page/欧洲.md "wikilink")[大西洋上的](../Page/大西洋.md "wikilink")[METEOSAT
6](../Page/METEOSAT_6.md "wikilink")、[METEOSAT
7和](../Page/METEOSAT_7.md "wikilink")[METEOSAT
8以及](../Page/METEOSAT_8.md "wikilink")[印度洋上的](../Page/印度洋.md "wikilink")[METEOSAT
5](../Page/METEOSAT_5.md "wikilink")。[俄罗斯的同步气象卫星位于](../Page/俄罗斯.md "wikilink")[莫斯科南部的赤道上](../Page/莫斯科.md "wikilink")，称为[GOMS](../Page/GOMS.md "wikilink")。[印度也有用于观测气象的同步卫星](../Page/印度.md "wikilink")。

### 極軌衛星

极轨卫星在离地面720至800公里的轨道上运行，它们的轨道通过地球的[南](../Page/南极.md "wikilink")[北极](../Page/北极.md "wikilink")，而且它们的轨道是与[太阳同步的](../Page/太阳.md "wikilink")，也就是说，它们每天两次飞越地球表面上的一个点，而且总是在同一个钟点。美国、中国、印度和俄罗斯拥有极轨气象卫星。

气象卫星的图象对于外行者来说也是很容易理解的，云、云的系统、[冷](../Page/冷锋.md "wikilink")[暖鋒](../Page/暖鋒.md "wikilink")、[台风](../Page/台风.md "wikilink")、[湖泊](../Page/湖泊.md "wikilink")、[森林](../Page/森林.md "wikilink")、[山脉](../Page/山脉.md "wikilink")、冰雪、火灾、[烟雾](../Page/烟雾.md "wikilink")、油迹等污染现象都一目了然。假如把一系列的照片连到一起看云的变化和发展，那么连[风都可以看到](../Page/风.md "wikilink")。

有经验的专业人员可以分析气象卫星的红外线图象，通过它他们可以确定云的高度和类型、计算地面和水面的温度，他们可以确定海面的污染、[潮汐和海流](../Page/潮汐.md "wikilink")。对[航海业来说](../Page/航海业.md "wikilink")，海流的信息是非常重要的，因为他们依此可以制订省油的航线。[渔民和](../Page/渔民.md "wikilink")[农民希望知道地面或海面的温度](../Page/农民.md "wikilink")，来保护他们的作物受冻或提高他们的捕获量。连厄尔尼诺现象都可以被转化成图象。红外线图片测量地面的温度，可以用来预报火灾发生的可能性。一般这些红外线图象是灰色的，但通过计算机处理它们可以变成多色的，来提高它们的对比度。

观察山上的冰雪情况可以提供年内河流供水的情报，为防汛和灌溉提供宝贵的预报。除此之外海面上[冰山的情况对航海业来说也是非常重要的](../Page/冰山.md "wikilink")。对海流的观察还可以提供对泄露油毯的发展的预报。

气象卫星对沙暴的观察对人类对这个现象的理解和预报起了非常重要的作用。比如每年[春天中国的沙暴可以一直跨越太平洋到达美国](../Page/春季.md "wikilink")。在[非洲每年](../Page/非洲.md "wikilink")[夏季大量](../Page/夏季.md "wikilink")[撒哈拉沙漠的沙暴被刮进大西洋](../Page/撒哈拉沙漠.md "wikilink")，有时可以到达[南美洲](../Page/南美洲.md "wikilink")。

世界上目前分辨率最高的气象卫星是[美国国防部的气象卫星](../Page/美国国防部.md "wikilink")[DMSP](../Page/DMSP.md "wikilink")。它的飞行高度是720公里。它可以分辨出地面上油车大小的物体。而且它可以在夜里拍可见光的照片，它利用的是[月光来照明](../Page/月球.md "wikilink")。它拍的城市灯光、火山爆发、大火、闪电、[流星](../Page/流星.md "wikilink")、[油田和极光的照片是非常动人的](../Page/油田.md "wikilink")。这些图片可以用来计算一个地区使用能源的量。天文学家用它们来确定一个观察点的[光污染程度](../Page/光污染.md "wikilink")。中華民國由[中研院](../Page/中研院.md "wikilink")、[中科院和幾個頂尖大學研究團隊](../Page/中科院.md "wikilink")(包含[台大](../Page/台大.md "wikilink")、[國立中山大學](../Page/國立中山大學.md "wikilink")、[國立交通大學](../Page/國立交通大學.md "wikilink")、[國立成功大學](../Page/國立成功大學.md "wikilink")、[國立中興大學等](../Page/國立中興大學.md "wikilink"))研發[福爾摩沙衛星系列](../Page/福爾摩沙衛星.md "wikilink")([福衛](../Page/福衛.md "wikilink"))也屬於此種衛星。

## 参考链接

[Category:氣象衛星](../Category/氣象衛星.md "wikilink")
[Category:天氣](../Category/天氣.md "wikilink")
[Category:地球观测卫星](../Category/地球观测卫星.md "wikilink")

1.