**巴伐利亞發動機製造廠股份有限公司**（，，在[英語系國家經常被暱稱為Bimmer](../Page/英語系國家.md "wikilink")，在[中國大陸及](../Page/中國大陸.md "wikilink")[港澳地區称为](../Page/港澳.md "wikilink")**寶-{}-馬**，在[台灣一般使用原稱](../Page/台灣.md "wikilink")「**B-{M}-W**」，台語發音稱「**米漿**」(Bhi-ling)，亦可稱**「寶-{}-馬」**）是[德國一家](../Page/德國.md "wikilink")[跨國](../Page/跨國公司.md "wikilink")[豪华汽車](../Page/豪华汽車.md "wikilink")、[機車和](../Page/摩托車.md "wikilink")[引擎製造商](../Page/引擎.md "wikilink")，總部位於[德国](../Page/德国.md "wikilink")[巴伐利亞州的](../Page/巴伐利亞州.md "wikilink")[慕尼黑](../Page/慕尼黑.md "wikilink")。

宝马集團除了以“B-{M}-W”作為品牌[商標銷售各式汽车与摩托车外](../Page/商標.md "wikilink")，也收購過多家外国汽车公司。目前BMW集团是B-{M}-W、[MINI](../Page/宝马迷你.md "wikilink")、[Rolls-Royce三個品牌的拥有者](../Page/勞斯萊斯.md "wikilink")。2012年宝马集团全品牌共生产1,845,186汽车和117,109摩托车。宝马經常与[奥迪](../Page/奥迪.md "wikilink")、[奔驰一同並列為德国三大豪华汽车制造商](../Page/奔驰.md "wikilink")。\[1\]\[2\]它的标志也是由大自然三大元素的概念制成的。

## 历史

### 萌芽時期

[BMW-HQ-PLANT-BMWWELT.jpg](https://zh.wikipedia.org/wiki/File:BMW-HQ-PLANT-BMWWELT.jpg "fig:BMW-HQ-PLANT-BMWWELT.jpg")北郊的[寶馬公司總部大廈](../Page/寶馬公司總部大廈.md "wikilink")\]\]
[BMW_Classic.JPG](https://zh.wikipedia.org/wiki/File:BMW_Classic.JPG "fig:BMW_Classic.JPG")\]\]
[BMW_model_3_15PS.JPG](https://zh.wikipedia.org/wiki/File:BMW_model_3_15PS.JPG "fig:BMW_model_3_15PS.JPG")
['15_BMW_X6.jpg](https://zh.wikipedia.org/wiki/File:'15_BMW_X6.jpg "fig:'15_BMW_X6.jpg")（2015）\]\]
[BMW_motorbike_F_650_GS_in_Gdansk.jpg](https://zh.wikipedia.org/wiki/File:BMW_motorbike_F_650_GS_in_Gdansk.jpg "fig:BMW_motorbike_F_650_GS_in_Gdansk.jpg")
[Paris_-_Salon_de_la_moto_2011_-_BMW_-_K_1600_GTL_-_002.jpg](https://zh.wikipedia.org/wiki/File:Paris_-_Salon_de_la_moto_2011_-_BMW_-_K_1600_GTL_-_002.jpg "fig:Paris_-_Salon_de_la_moto_2011_-_BMW_-_K_1600_GTL_-_002.jpg")
[Paris_-_Retromobile_2012_-_BMW_2002_Turbo_-_1974_-_005.jpg](https://zh.wikipedia.org/wiki/File:Paris_-_Retromobile_2012_-_BMW_2002_Turbo_-_1974_-_005.jpg "fig:Paris_-_Retromobile_2012_-_BMW_2002_Turbo_-_1974_-_005.jpg")
[BMW_132_engine.JPG](https://zh.wikipedia.org/wiki/File:BMW_132_engine.JPG "fig:BMW_132_engine.JPG")
[BMW_801_engine.JPG](https://zh.wikipedia.org/wiki/File:BMW_801_engine.JPG "fig:BMW_801_engine.JPG")
BMW的創始人卡尔·斐德利希·拉普（Karl Friedrich
Rapp）在1913年4月29日，利用一座[慕尼黑近郊原本是製造](../Page/慕尼黑.md "wikilink")[腳踏車的工厂厂房](../Page/腳踏車.md "wikilink")，設立了**拉普引擎製造廠**（），從事航空用引擎之製造。在同年，[古斯塔夫·奧圖](../Page/古斯塔夫·奧圖.md "wikilink")（）也在附近創立了**古斯塔夫奧圖航空機械製造廠**（），古斯塔夫事實上就是著名的[尼可勞斯·奧古斯特·奧圖](../Page/尼古拉斯·奧托.md "wikilink")（）、四行程汽油引擎（[奧圖循環引擎](../Page/奧圖循環引擎.md "wikilink")）發明者的兒子。

古斯塔夫·奧圖稍後與人合資，在1916年3月7日創立了[巴伐利亞飛機製造廠](../Page/巴伐利亞飛機製造廠.md "wikilink")，並且將自己創立了三年的工廠併入這家新廠。同年，拉普也獲得銀行家[卡米羅·卡斯提李奧尼](../Page/卡米羅·卡斯提李奧尼.md "wikilink")（）與[馬克思·弗利茲](../Page/馬克思·弗利茲.md "wikilink")（）的資助大幅擴張規模，但卻因為評估錯誤過度擴張導致營運不善，致使拉普在11月14日時黯然離開。

他的合伙人找到[奧地利的金融家](../Page/奧地利.md "wikilink")[佛朗茲-約瑟夫·帕普](../Page/佛朗茲-約瑟夫·帕普.md "wikilink")（）合作接下了引擎廠的業務，在1917年7月20日將工廠改名為**巴伐利亞發動機製造股份有限公司**（，縮寫為），由帕普擔任首任的總裁。當時時值[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，身為軍需供應廠商的BMW特別在慕尼黑市郊的[歐伯維森菲爾德](../Page/歐伯維森菲爾德.md "wikilink")（）軍機場附近設置了大型的工廠，持續地替軍方製造軍機引擎直到1918年為止。1918年8月13日BMW改制為[股票公開上市的](../Page/股票.md "wikilink")[股份公司型態](../Page/股份公司.md "wikilink")（BMW
AG），確立了之後蒸蒸日上的公司規模。

1922年時BMW合併了BFW，成為今日我們所熟悉的BMW。但在追溯該公司歷史時，公司的官方說法是以BFW的創廠時間為準，也就是1916年3月7日作為BMW的創廠日。

BMW的第一具航空引擎作品是1917年時投產的Type
IIIa，這是一具[水冷設計的](../Page/水冷.md "wikilink")[直列六缸引擎](../Page/直列.md "wikilink")，使用了公司合夥人之一的弗利茲所開發的高空用[化油器](../Page/化油器.md "wikilink")，縱使在高海拔環境中也能發揮引擎的最大輸出。1919年時，BMW將一具該廠生產的Type
IV直列六缸引擎安裝到一架DFW雙翼飛機上，由佛朗茲·契諾·迪默（）駕駛，在[慕尼黑奧林匹克公園](../Page/慕尼黑奧林匹克公園.md "wikilink")（今日BMW總部所在地）上空創下9,760公尺高的飛行高度紀錄。

### 從天上轉戰兩輪

1918年[第一次世界大戰結束](../Page/第一次世界大戰.md "wikilink")，根據[凡爾賽條約的規定](../Page/凡爾賽條約.md "wikilink")，德國境內被禁止製造飛機，嚴重打擊了正在成長中的德國[航空工業](../Page/航空工業.md "wikilink")，也迫使BMW轉為製造鐵道用的制動器，並開始發展機車用的引擎，為走入造車的第一步。

1920年，由馬丁·史托爾（Martin Stolle）設計的M2 B
15引擎，成為BMW生產的第一具機車用引擎。1923年弗利茲設計了排氣量500
cc.的[R32型機車](../Page/BMW_R32型機車.md "wikilink")，有別於該廠之前幾款機車產品都是採用[鏈條傳動的設計](../Page/鏈條傳動.md "wikilink")，R32是第一輛採用[軸傳動的BMW機車](../Page/軸傳動.md "wikilink")，從它開始軸傳動設計就成了BMW機車最知名的特色之一，一直到今日我們仍可以從該廠的R系列機車上看到此種設計。BMW在R32上首次啟用了代表巴伐利亞邦的藍白方格旗廠徽，并且沿用至今。
1929年9月19日，機車手恩斯特·海納（）騎乘著一輛排氣量750
cc.的新型BMW機車，在慕尼黑創下216.75km/h的世界紀錄。之後他又持續多次打破世界高速紀錄，其中以1937年時創下的279.5km/h最驚人，該紀錄連續保持了12年之久才有人打破。

### 邁向四輪產品領域

1927年時，位於德國[圖林根邦](../Page/圖林根.md "wikilink")[埃森納赫市的埃森納赫車廠](../Page/埃森納赫.md "wikilink")，獲得英國[奧斯汀車廠的授權](../Page/奧斯汀汽車.md "wikilink")，開始製造該廠著名的[Austin
7車款之德國版本](../Page/Austin_7.md "wikilink")，掛上[Dixi](../Page/Dixi.md "wikilink")（迪克西）的品牌銷售。隔年，BMW以1600萬[馬克的價格](../Page/馬克.md "wikilink")，併購了該廠，也因此獲得[Dixi
3/15
PS這款車的生產權利](../Page/Dixi_3/15_PS.md "wikilink")，成為該廠第一輛汽車產品。這輛車在經過BMW修改之後以改良版DA2的身分上市，DA意指“”（德國製造）之意，登場後大受好評，在短短三年左右的生產期中就賣出18,976輛之多。

至於1932年時登場的3/20
PS（又稱為，，「慕尼黑生產、四檔變速」之意），則是第一輛BMW自製的汽車產品。改良自Dixi的這款車搭載一具782
cc.的直列四缸引擎，擁有20hp/3500rpm的最大馬力輸出，與80km/h左右的極速，並且在同年於[巴登-巴登舉行的優雅汽車展](../Page/巴登-巴登.md "wikilink")（）中獲得優勝。

1933年登場的BMW
303，是真正最具有關鍵代表性的一款車，它開創了兩樣BMW一直到今天為止都還維持著的傳統。其一，它是BMW第一款搭載直列六缸引擎的汽車，其二，該車款首度在車頭部份採用了著名的「雙腎」[水箱護柵造型](../Page/水箱護柵.md "wikilink")，雖然經過歷代的改款這雙腎造型也多經修改，但基本的造型基調卻一直到今日都沒有改變過。

BMW對技術研究十分重視，在專業媒體測試中BMW引擎的馬力輸出往往比同級車高，然後又能保持優良的精緻度與低噪音，底盤操控性專業評價也常比同級車傑出，這些駕駛樂趣是BMW能在後來被公認與[賓士並駕齊驅的原因](../Page/賓士.md "wikilink")。

### 戰後至今

在德東的寶馬公司[Eisenacher被](../Page/:en:Eisenacher_Motorenwerk.md "wikilink")[東德國營化](../Page/東德.md "wikilink")，[兩德統一後為](../Page/兩德統一.md "wikilink")[歐寶併購了](../Page/歐寶.md "wikilink")。現在寶馬有意把所有的轎車都赋予跑車色彩，消弭跑車和轎車的分別。

2018年10月11日[宝马集团斥資](../Page/BMW.md "wikilink")36億歐元（約326億港元）（約290億元人民幣）增持與[華晨中國汽車組成的合資企業](../Page/華晨中國汽車.md "wikilink")[華晨寶馬](../Page/華晨寶馬.md "wikilink")25%股權，交易完成後[宝马集团的持股比例將由現時的](../Page/BMW.md "wikilink")50%增至75%，此外，合資公司的合同期限將從原來的2028年延長至2040年。

## 所有車款

### 汽車產品

BMW的汽車產品是以車系作為分類基礎，每個車系之下再依照各車款引擎、驅動系統與配備等級的細節差異，各自有各自的車款名（例如760Li、645Ci、330iX）。由於近代的BMW車系都是以同樣的名稱，一代一代改款传承下去，因此往往會發生車名相同但車代其實前後有差的情況，為了避免混淆一般常以車廠內部的車系代號來輔助稱呼（雖然在商業上，BMW原廠並不直接公開使用車系代號），因此常可聽到E36
318i、E60 M5這樣的稱呼方式來區分BMW的各種車型。

#### 現有车系

[BMW_sixth-generation_7Series_in_Tokyo-MotorShow_2015.jpg](https://zh.wikipedia.org/wiki/File:BMW_sixth-generation_7Series_in_Tokyo-MotorShow_2015.jpg "fig:BMW_sixth-generation_7Series_in_Tokyo-MotorShow_2015.jpg")\]\]
[2018_BMW_520d_xDrive_M_Sport_Automatic_2.0_Front.jpg](https://zh.wikipedia.org/wiki/File:2018_BMW_520d_xDrive_M_Sport_Automatic_2.0_Front.jpg "fig:2018_BMW_520d_xDrive_M_Sport_Automatic_2.0_Front.jpg")（G30）\]\]
[BMW_320d_Sport_Line_(F30)_–_Frontansicht,_26._Februar_2012,_Wülfrath.jpg](https://zh.wikipedia.org/wiki/File:BMW_320d_Sport_Line_\(F30\)_–_Frontansicht,_26._Februar_2012,_Wülfrath.jpg "fig:BMW_320d_Sport_Line_(F30)_–_Frontansicht,_26._Februar_2012,_Wülfrath.jpg")（F30）\]\]
[BMW_Z4_(E89)_front_20100815.jpg](https://zh.wikipedia.org/wiki/File:BMW_Z4_\(E89\)_front_20100815.jpg "fig:BMW_Z4_(E89)_front_20100815.jpg")（E89）\]\]
[2015_BMW_X3_(F25_LCI)_xDrive20d_wagon_(2015-06-27).jpg](https://zh.wikipedia.org/wiki/File:2015_BMW_X3_\(F25_LCI\)_xDrive20d_wagon_\(2015-06-27\).jpg "fig:2015_BMW_X3_(F25_LCI)_xDrive20d_wagon_(2015-06-27).jpg")（F25）\]\]

  - [1系](../Page/BMW_1系列.md "wikilink")（1er-Reihe，1
    Series）：車系代號F20，2011年上市，小型五門[掀背車系](../Page/掀背車.md "wikilink")。（三门版F21）
  - [2系](../Page/BMW_2系列.md "wikilink")（2er-Reihe，2
    Series）：車系代號F22，2014年上市，小型双门[硬顶跑车系](../Page/硬顶跑车.md "wikilink")，取代原来的1系硬顶跑车和[敞篷跑车](../Page/敞篷跑车.md "wikilink")。
  - [3系](../Page/BMW_3系列.md "wikilink")（3er-Reihe，3
    Series）：車系代號G20，2019年上市，小型主管級[轎車系](../Page/轎車.md "wikilink")。
  - [4系](../Page/BMW_4系列.md "wikilink")（4er-Reihe，4
    Series）：車系代號F32，2014年上市，小型主管級双门[硬顶跑车系与双門](../Page/硬顶跑车.md "wikilink")[敞篷跑车F](../Page/敞篷跑车.md "wikilink")33，取代原来的3系硬顶跑车和[敞篷跑车](../Page/敞篷跑车.md "wikilink")。
  - [5系](../Page/BMW_5系列.md "wikilink")（5er-Reihe，5
    Series）：車系代號G30，2017年上市。
  - [6系](../Page/BMW_6系列.md "wikilink")（6er-Reihe，6
    Series）：中大型轎跑車系列，分別有雙門轎跑車（車系代號F13，2011年上市）、雙門敞篷跑車（車系代號F12，2011年上市）、四门轿跑车（車系代號F06，2011年上市）三種車型。
  - [7系](../Page/BMW_7系列.md "wikilink")（7er-Reihe，7
    Series）：大型豪華房車系列，2015年上市，有短轴距型（車系代號G11）與長轴距型（車系代號G12）。
  - [8系](../Page/BMW_8系列.md "wikilink")（8er-Reihe，8 Series）：車系代號G14/G15
    ,預計2019年上市,M8於2018年在Goodwood赛车节上出现。
  - [Z4](../Page/BMW_Z4.md "wikilink")：車系代號E89，2009年上市，小型雙門[雙座敞篷跑車](../Page/雙座敞篷跑車.md "wikilink")。
  - [X1](../Page/BMW_X1.md "wikilink")：車系代號F48，2015年上市，小型SUV。
  - [X2](../Page/BMW_X2.md "wikilink")：車系代號F39，2018年上市，小型SUV。
  - [X3](../Page/BMW_X3.md "wikilink")：車系代號G01，2018年上市，小型五門[休旅車系](../Page/休旅車.md "wikilink")。
  - [X4](../Page/BMW_X4.md "wikilink")：車系代號G02, 2018年上市, 跨界休旅車
  - [X5](../Page/BMW_X5.md "wikilink")：車系代號G05，2018年上市，中型五門休旅車系。
  - [X6](../Page/BMW_X6.md "wikilink")：車系代號F16，2015年上市，[跨界休旅車](../Page/跨界休旅車.md "wikilink")、[運動型多用途車系](../Page/運動型多用途車.md "wikilink")。
  - [X7](../Page/BMW_X7.md "wikilink")：2019年上市，大型SUV。
  - 3系列GT（3er-Gran Turismo，3 Series Gran
    Turismo）：車系代號F34，2013年上市，小型混合運動型轎車及SUV的新型[跨界休旅車](../Page/跨界休旅車.md "wikilink")。
  - 6系列GT（6er-Gran Turismo，6 Series Gran
    Turismo）：車系代號G32，2017年上市，中型混合運動型轎車及SUV的新型[跨界休旅車](../Page/跨界休旅車.md "wikilink")。
  - 概念車：車型Gina，未上市，BMW所開發的新型概念車，又BMW設計的特殊纖維布料所包覆。
  - 油電混合車：車型Efficient Dynamics，未上市，爲了符合現代省油的趨勢而製造。

#### M車系

[The_frontview_of_BMW_M6_Cabriolet_(F12)_with_its_roof_closed.JPG](https://zh.wikipedia.org/wiki/File:The_frontview_of_BMW_M6_Cabriolet_\(F12\)_with_its_roof_closed.JPG "fig:The_frontview_of_BMW_M6_Cabriolet_(F12)_with_its_roof_closed.JPG")
F12 \]\]
[BMW_E89_Z4_sDrive23i.JPG](https://zh.wikipedia.org/wiki/File:BMW_E89_Z4_sDrive23i.JPG "fig:BMW_E89_Z4_sDrive23i.JPG")
Coupé \]\]
[BMW_1M_-_Flickr_-_Alexandre_Prévot_(9).jpg](https://zh.wikipedia.org/wiki/File:BMW_1M_-_Flickr_-_Alexandre_Prévot_\(9\).jpg "fig:BMW_1M_-_Flickr_-_Alexandre_Prévot_(9).jpg")
M車系是BMW旗下的高性能車種家族，由直屬於BMW集團的子公司[BMW
M公司](../Page/BMW_M公司.md "wikilink")（德文：BMW M
GmbH，其獨立設置公司之前原本是BMW
M部門）負責設計生產，除了少數幾款特例外，M車系的產品大都是以BMW原有的各車系車款為基礎，再加上由M公司特別開發的高性能引擎、傳動系統、空氣動力套件與其他方面的性能版零件與調校之後，獨立而成的高性能版本。而除了整車生產的高性能版車型之外，M公司也負責開發製造BMW旗下各車系專用的高性能售後改裝用套件。

現行的M車系產品包括：

  - [1M](../Page/BMW_1M.md "wikilink")
  - [M1](../Page/BMW_M1.md "wikilink")
  - [M2(Concept)](../Page/BMW_M2.md "wikilink")
  - [M3](../Page/BMW_M3.md "wikilink")
  - [M4(Concept)](../Page/BMW_M4.md "wikilink")
  - [M5](../Page/BMW_M5.md "wikilink")
  - [M6](../Page/BMW_M6.md "wikilink")
  - [Z4 M Roadster/M Coupé](../Page/BMW_Z4_M.md "wikilink")
  - [X5 M](../Page/X5_M.md "wikilink")
  - [X6 M](../Page/X6_M.md "wikilink")
  - [M8](../Page/BMW_M8.md "wikilink")

i車系(即[電動車](../Page/電動車.md "wikilink"))

  - [i3](../Page/BMW_i3.md "wikilink")
  - [i8](../Page/BMW_i8.md "wikilink")

#### 歷史車款

  - 3系列掀背車（3er Compact）：屬於車系代號E36的已停產三系列衍生車型之一，採三门掀背設計，是3er
    Compact的第一代產品。
  - 8系列（8er-Reihe）：大型豪華跑車系列，于1989年上市至1998年停產，代號E31，頂級車款為850csi，搭載一具5.6升的V型12氣缸引擎。

## \-{zh-tw:行銷;zh-cn:营销}-活動

### 汽車競技

[Frankfurt_BMW_i8_at_the_Motor_Show_2011_(6143060125).jpg](https://zh.wikipedia.org/wiki/File:Frankfurt_BMW_i8_at_the_Motor_Show_2011_\(6143060125\).jpg "fig:Frankfurt_BMW_i8_at_the_Motor_Show_2011_(6143060125).jpg")\]\]
BMW車廠一直以來都非常積極地參與世界各地的許多[賽車活動](../Page/賽車.md "wikilink")，其中層級與投資最高的莫過於斥資參與[一級方程式賽車活動](../Page/一級方程式賽車.md "wikilink")，該廠曾經是[威廉姆斯車隊](../Page/威廉姆斯车队.md "wikilink")（Team
BMW-Williams）的主要贊助商與引擎供應商，但是2005年時，由於理念上的不同，所以結束了合作關係。在2005年中，BMW買下了[彼得·索伯](../Page/彼得·索伯.md "wikilink")（Peter
Sauber）的私人車隊，並於2006年起改為車廠自有的廠隊[BMW索伯車隊參予角逐](../Page/BMW索伯車隊.md "wikilink")，但已於2009年賽季結束後退出比賽。

### The Hire

另外，BMW在2001年時開始了拍攝《》系列的短片以宣傳自家品牌的汽車，刺激銷量。

於2016年，BMW續拍了《》系列，並取名新一季為《》，重點宣傳。

### 電影贊助

除此之外，BMW也曾大量提供新車協助電影[-{zh-hans:詹姆斯·邦德;zh-hk:占士邦;zh-tw:詹姆斯·龐德;}-系列部份續集的拍攝](../Page/詹姆斯·邦德.md "wikilink")，包括第17集《黃金眼》（GoldenEye）中的BMW
Z3敞篷跑車，第18集《明日帝國》（Tomorrow Never Dies）中的BMW 750i轎車、BMW R1200重型機車與Land
Rover Discovery越野車（當時Land
Rover仍屬於BMW集團一部份），汤姆·克鲁斯主演的《碟中谍4》中的宝马X3、Z4和i8,以及第19集《縱橫天下》（The
World Is Not Enough）的BMW
Z8跑車。BMW此舉被視為是[置入性行銷的經典範例之一](../Page/置入性行銷.md "wikilink")，但也因為在電影中過分強調BMW車輛的存在，遭許多影評及影迷批評過份商業化而扭曲了影片的整體質感。附註一提的是，BMW並沒有繼續贊助007第20集《誰與爭鋒》（Die
Another
Day）的車輛使用，而是由[福特汽車旗下所屬的幾個汽車品牌](../Page/福特汽車.md "wikilink")[福特](../Page/福特.md "wikilink")、[-{zh-hans:美洲虎;
zh-hant:捷豹;}-與](../Page/捷豹.md "wikilink")[阿斯顿·馬丁取而代之](../Page/阿斯顿·馬丁.md "wikilink")。
玩命快遞電影中也用了BMW E38 735iL 來連接整個劇情。

### 合作夥伴

此外，BMW也是[中国奥委会的合作伙伴之一](../Page/中国奥林匹克委员会.md "wikilink")。

## 参考资料

## 外部链接

  - [BMW官方網站](http://www.bmwgroup.com)
      -
      -
      -
  - [BMW全球官方網站](https://web.archive.org/web/20150213013047/http://www.bmw.com/com/en/)
  - [BMW中国官方网站](http://www.bmw.com.cn)
  - [BMW香港官方網站](http://www.bmwhk.com)
      -
  - [BMW台灣總代理汎德官方網站](http://www.bmw.com.tw)
      -
  - [《The Escape》](https://www.youtube.com/watch?v=jzUFCQ-P1Zg)

[Category:德國汽車品牌](../Category/德國汽車品牌.md "wikilink")
[Category:德國機車公司](../Category/德國機車公司.md "wikilink")
[Category:飛行器發動機製造商](../Category/飛行器發動機製造商.md "wikilink")
[Category:德國汽車公司](../Category/德國汽車公司.md "wikilink")
[Category:跨國公司](../Category/跨國公司.md "wikilink")
[B](../Category/慕尼黑公司.md "wikilink")
[主](../Category/宝马汽车.md "wikilink")
[Category:家族式企業](../Category/家族式企業.md "wikilink")
[Category:1916年成立的公司](../Category/1916年成立的公司.md "wikilink")

1.  <https://www.press.bmwgroup.com/global/pressDetail.html?title=bmw-group-cautiously-optimistic-for-2013&outputChannelId=6&id=T0138199EN&left_menu_item=node__804>
2.