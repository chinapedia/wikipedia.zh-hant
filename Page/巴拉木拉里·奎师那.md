[thumb](../Page/文件:Mangalampalli_Balamuralikrishna.jpg.md "wikilink")
**巴拉木拉里·奎师那**（[泰卢固语](../Page/泰卢固语.md "wikilink")：，英语：，）是一名印度[卡那提克音乐演唱家](../Page/卡那提克音乐.md "wikilink")、乐器演奏家、歌手、作曲家和演员。他出生于[印度](../Page/印度.md "wikilink")[安得拉邦](../Page/安得拉邦.md "wikilink")镇。因为他为印度艺术的贡献他获得印度第二级公民荣誉奖[莲花赐勋章](../Page/莲花赐勋章.md "wikilink")。2005年法国政府授予他[法蘭西藝術與文學勳章](../Page/法蘭西藝術與文學勳章.md "wikilink")\[1\]。

## 早年生活

巴拉木拉里·奎师那出生于安得拉邦[東哥達瓦里縣的一个婆罗门家庭](../Page/東哥達瓦里縣.md "wikilink")\[2\]。他的父亲是一名知名的音乐家，会演奏[笛](../Page/笛.md "wikilink")、[小提琴和](../Page/小提琴.md "wikilink")[维纳琴](../Page/维纳琴.md "wikilink")。他的母亲是一名出色的维纳琴演奏家。他还是婴儿的时候他的母亲就去世了，他的父亲把他抚养大。他的父亲注意到他的音乐天才，因此让他跟随[拉玛奎师纳亚·潘图卢学习](../Page/拉玛奎师纳亚·潘图卢.md "wikilink")，潘图卢本人是[蒂亚格拉贾的直系后人](../Page/蒂亚格拉贾.md "wikilink")。

在潘图卢的指导下巴拉木拉里·奎师那学习了卡那提克音乐。他八岁的时候首次在音乐会上演奏。他的才华受到其他音乐家的重视。

因此巴拉木拉里·奎师那在很早年龄就开始了他的音乐生涯。他15岁时已经能够演奏所有72种基础卡那提克音乐格式并使用它们自己作曲。1952年他发表了，这部作品被录制在一系列共九张唱片上发表\[3\]。巴拉木拉里·奎师那不自满于作为卡那提克演唱家的成就而很快开始演奏[坎击拉鼓](../Page/坎击拉鼓.md "wikilink")、[魔力单根鼓](../Page/魔力单根鼓.md "wikilink")、中提琴和小提琴。他用小提琴给不同音乐家伴奏，并举办中提琴独奏音乐会。

## 音乐生涯

巴拉木拉里·奎师那六岁时开始演奏，到今天为止他在世界各地举办了2.5万多场音乐会\[4\]。他伴随过[班智達](../Page/班智達.md "wikilink")[宾山·乔西](../Page/宾山·乔西.md "wikilink")。他和[哈里帕拉沙·乔拉西亚大师等一起举办过](../Page/哈里帕拉沙·乔拉西亚.md "wikilink")[尤伽尔班迪音乐会](../Page/尤伽尔班迪.md "wikilink")。他还普及了[安纳马查拉的作品](../Page/安纳马查拉.md "wikilink")。

## 音乐会

[M._Balamuralikrishna_02.jpg](https://zh.wikipedia.org/wiki/File:M._Balamuralikrishna_02.jpg "fig:M._Balamuralikrishna_02.jpg")
[Balamuralikrishna_in_Kuwait.jpg](https://zh.wikipedia.org/wiki/File:Balamuralikrishna_in_Kuwait.jpg "fig:Balamuralikrishna_in_Kuwait.jpg")
巴拉木拉里·奎师那的音乐会结合了复杂的歌唱技巧、古典音乐的节奏和民众的娱乐需求。他受邀去美国、加拿大、意大利、法国、俄罗斯、斯里兰卡、马来西亚、新加坡、中东和许多其它国家举办音乐会。虽然他的母语是[泰卢固语](../Page/泰卢固语.md "wikilink")，他的作品包括[卡纳达语](../Page/卡纳达语.md "wikilink")、[梵语](../Page/梵语.md "wikilink")、[泰米尔语](../Page/泰米尔语.md "wikilink")、[马拉雅拉姆语](../Page/马拉雅拉姆语.md "wikilink")、[印地语](../Page/印地语.md "wikilink")、[孟加拉语和](../Page/孟加拉语.md "wikilink")[旁遮普語](../Page/旁遮普語.md "wikilink")。

他和一个英国合唱团合作演唱根据[罗宾德拉纳特·泰戈尔获得](../Page/罗宾德拉纳特·泰戈尔.md "wikilink")[诺贝尔奖的](../Page/诺贝尔奖.md "wikilink")《[吉檀枷利](../Page/吉檀枷利.md "wikilink")》改编的歌曲并唱主唱。由于他会说多种语言他受邀把泰戈尔作的歌曲用孟加拉语录制下来，防止它们被遗忘。他也曾经唱法语歌曲，甚至和著名的卡那提克击鼓大师一起演唱[融合爵士樂](../Page/融合爵士樂.md "wikilink")。

巴拉木拉里·奎师那近年来越来越对[音乐治疗感兴趣](../Page/音乐治疗.md "wikilink")，他很少登台演出。

2010年2月他首次在他生涯中在[维沙卡帕特南举办音乐会](../Page/维沙卡帕特南.md "wikilink")\[5\]。

## 电影

巴拉木拉里·奎师那使用泰卢固语、梵语、卡纳达语和泰米尔语作过400多部电影音乐。他的作品包括所有卡那提克音乐的格式。

他1967年首次在电影里自己登台演出，后来还在其它一些电影里出现。

## 更新

[Balamurali-close-up.jpg](https://zh.wikipedia.org/wiki/File:Balamurali-close-up.jpg "fig:Balamurali-close-up.jpg")
巴拉木拉里·奎师那的音乐的特点在于他不拘常规，大胆试验和革新。他在保持卡那提克音乐丰富的传统的同时改革了这个音乐系统。他发明了新的音乐格式\[6\]。

## 参考资料

## 外部链接

[Category:卡那提克音乐](../Category/卡那提克音乐.md "wikilink")
[Category:法國藝術及文學勳章持有人](../Category/法國藝術及文學勳章持有人.md "wikilink")
[Category:印度作曲家](../Category/印度作曲家.md "wikilink")
[Category:印度音樂家](../Category/印度音樂家.md "wikilink")
[Category:安得拉邦人](../Category/安得拉邦人.md "wikilink")

1.

2.

3.

4.  [Balamuralikrishna deserves Bharat Ratna:
    Jayalalithaa](http://www.hindu.com/2005/07/26/stories/2005072617030500.htm).
    Hindu.com (2005年7月26日) 2013年12月21日查询

5.

6.