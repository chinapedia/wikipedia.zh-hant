[День_защиты_детей_-_International_Children's_Day.JPG](https://zh.wikipedia.org/wiki/File:День_защиты_детей_-_International_Children's_Day.JPG "fig:День_защиты_детей_-_International_Children's_Day.JPG")[6月1日](../Page/6月1日.md "wikilink")，[俄罗斯](../Page/俄罗斯.md "wikilink")[北德文斯克的儿童正在庆祝国际儿童节](../Page/北德文斯克.md "wikilink")\]\]
[1950-07-People_P-Page26-Chinese_Children's_Day.png](https://zh.wikipedia.org/wiki/File:1950-07-People_P-Page26-Chinese_Children's_Day.png "fig:1950-07-People_P-Page26-Chinese_Children's_Day.png")
**儿童节**（）是為了宣傳「保障[儿童](../Page/儿童.md "wikilink")[权利](../Page/权利.md "wikilink")」、「反对[虐待儿童](../Page/虐待儿童.md "wikilink")」和「反對毒害[儿童](../Page/儿童.md "wikilink")」等理念的節日。1949年，[國際民主婦女聯合會在](../Page/國際民主婦女聯合會.md "wikilink")[莫斯科舉行會議](../Page/莫斯科.md "wikilink")，決定訂[6月1日為](../Page/6月1日.md "wikilink")**國際兒童節**。1954年12月14日，[聯合國教育科學文化組織定](../Page/聯合國教育科學文化組織.md "wikilink")[11月20日為](../Page/11月20日.md "wikilink")**世界兒童節**（The
Universal Children's
Day）。它的設立是為了鼓勵所有國家建立一個屬於兒童的節日，最先是為促進成人與兒童之間的相互交流和了解，其次則是要人們採取行動以受益促進世界兒童的福利，並以促進確立[聯合國憲章保障兒童福利為目標](../Page/聯合國憲章.md "wikilink")。

## 亞洲

### 中國大陸

同[蘇聯](../Page/蘇聯.md "wikilink")、[東德](../Page/東德.md "wikilink")、[東方集團及其他](../Page/東方集團.md "wikilink")[社会主义国家](../Page/社会主义国家.md "wikilink")，[中央人民政府政务院第十二次政务会议于](../Page/中央人民政府政务院.md "wikilink")[1949年](../Page/1949年.md "wikilink")[12月23日通過了](../Page/12月23日.md "wikilink")《[全国年节及纪念日放假办法](../Page/s:全国年节及纪念日放假办法.md "wikilink")》，其中规定[6月1日为儿童节](../Page/6月1日.md "wikilink")，并将其列为“属于部分人民之节日，为了便于部分人民的群众活动，得放假半天，或只其中一部分人放假，其他一部分人得推代表参加庆祝”。\[1\][1950年](../Page/1950年.md "wikilink")[3月30日](../Page/3月30日.md "wikilink")，[中央人民政府教育部發出通告](../Page/中央人民政府教育部.md "wikilink")，準備慶祝第一個[六一儿童节](../Page/六一儿童节.md "wikilink")。在該通告中，中央人民政府教育部簡述了更改兒童節日期的原因及第一個兒童節的慶祝辦法。

### 香港

在民間約定俗成下，香港慶祝兒童節的日期為[中華民國政府定下的](../Page/中華民國政府.md "wikilink")[4月4日](../Page/4月4日.md "wikilink")，雖非公眾假期，但常與[清明節](../Page/清明節.md "wikilink")（香港公眾假期）重疊，而未特別慶祝。\[2\]另外，香港的一些大型企業亦曾舉辦一些暑期兒童活動，例如[電視廣播有限公司開辦的](../Page/電視廣播有限公司.md "wikilink")[TVB兒童節](../Page/TVB兒童節.md "wikilink")，活動內容包括夏令營、環保活動、繪畫比賽、參觀活動等等。不過由2010年代起，部分機構將兒童節的日期與內地的做法看齊。\[3\]

### 澳門

澳門兒童節的日期定為6月1日，非公眾假期。澳門政府每年都會組織一系列活動慶祝，並於[綜藝館舉行大型園遊會](../Page/綜藝館.md "wikilink")。

### 日本

[Flickr_-_yeowatzup_-_Omihachiman,_Shiga,_Japan.jpg](https://zh.wikipedia.org/wiki/File:Flickr_-_yeowatzup_-_Omihachiman,_Shiga,_Japan.jpg "fig:Flickr_-_yeowatzup_-_Omihachiman,_Shiga,_Japan.jpg")
日本的儿童节，叫做“儿童日”（），是一个传统节日。在每年的[5月5日](../Page/5月5日.md "wikilink")（[明治維新前為農曆](../Page/明治維新.md "wikilink")[五月初五](../Page/五月初五.md "wikilink")，日本的家庭都会庆祝孩子的长大。這天本來是東亞傳統節日“[端午节](../Page/端午节.md "wikilink")”（），在日本端午节只是男孩的节日，並不包括女孩）。[1948年](../Page/1948年.md "wikilink")，当这个节日成为公众假日的时候，便成了庆祝所有儿童幸福和福利的节日。在节日当天，日本的家庭都会在屋顶上悬挂鱼状的标志，用来象征儿童消除厄运，克服困难，顺利成长。全家用[菖蒲水洗熱水澡](../Page/菖蒲.md "wikilink")，祛災除病
。
日本將[3月3日定為單獨的](../Page/3月3日.md "wikilink")[女兒節](../Page/女兒節.md "wikilink")（），自東亞傳統節日“[上巳節](../Page/上巳節.md "wikilink")”（[明治維新前為農曆](../Page/明治維新.md "wikilink")[三月初三](../Page/三月初三.md "wikilink")）發展過來，女儿节以摆放各种玩偶来庆祝。男儿节悬挂[鲤鱼旗是源自中国的](../Page/鲤鱼旗.md "wikilink")“望子成龙”和“[鯉躍龍門](../Page/鯉躍龍門.md "wikilink")”的传说,甚至還有給男孩子穿上這些頭盔鎧甲、持劍背弓的，並擺設鍾馗人偶或懸掛鍾馗畫像。

### 朝鮮

朝鲜的儿童节在6月1日。\[4\]

### 韓國

韩国的儿童节（）开始于[朝鮮日治時期的](../Page/朝鮮日治時期.md "wikilink")[1923年](../Page/1923年.md "wikilink")，是从“男孩节”演变过来的。定在每年的[5月5日](../Page/5月5日.md "wikilink")，和[日本的兒童日日期相同](../Page/日本.md "wikilink")。在韩国制定為公众假日。家长们通常会先準備孩子最想要的禮物之後在这一天带孩子去公园、动物园或者其他游乐设施，让孩子开心地度过假日。

### 蒙古

蒙古的儿童节也在6月1日。\[5\]

### 中華民國

[1931年](../Page/1931年.md "wikilink")，[孔祥熙发起建立的中华慈幼协济会提议](../Page/孔祥熙.md "wikilink")，将[4月4日定为儿童节](../Page/4月4日.md "wikilink")。[行政院在](../Page/行政院.md "wikilink")《紀念日及節日實施辦法》第五條中規定，4月4日兒童節，有關機關、團體、學校舉行慶祝活動\[6\]。[1991年](../Page/1991年.md "wikilink")－[1997年與](../Page/1997年.md "wikilink")[婦女節合併放假一天](../Page/婦女節.md "wikilink")（正式名稱為「[婦女節、兒童節合併假期](../Page/婦女節、兒童節合併假期.md "wikilink")」）。[1998年以後取消放假](../Page/1998年.md "wikilink")，併入週休假期。[1998年實施](../Page/1998年.md "wikilink")[週休二日以前](../Page/五天工作制.md "wikilink")，國小及以下學童放假一天。[2011年起恢復為](../Page/2011年.md "wikilink")[國定假日](../Page/中華民國國定假日.md "wikilink")，全國放假一天。2012年再次修法，如果遇到與[清明節同一日](../Page/清明節.md "wikilink")，則在前一日4月3日放假，如遇週四則於後一日放假。

### 寮國

寮國的儿童节也在6月1日。\[7\]

### 印度

在印度，儿童节的日子訂於开国总理[贾瓦哈拉尔·尼赫鲁的生日](../Page/贾瓦哈拉尔·尼赫鲁.md "wikilink")，也就是每年的[11月14日](../Page/11月14日.md "wikilink")。

### 土耳其

[4月23日](../Page/4月23日.md "wikilink")，是土耳其的“[国家主权及儿童日](../Page/国家主权及儿童日.md "wikilink")”。这个节日来自[土耳其独立战争期间](../Page/土耳其独立战争.md "wikilink")1920年土耳其国民大会的召开日期。[1929年](../Page/1929年.md "wikilink")，根据儿童保护组织的建议，这一天被定为儿童节。从1986年起，土耳其政府开始在4月23日庆祝国际儿童节。

### 泰國

泰国的儿童节在一月份的第二个星期六。

### 新加坡

[新加坡儿童节原定於](../Page/新加坡.md "wikilink")10月1日，于2011年修宪时为“促进家庭生活”，改作每个十月的第一个星期五。\[8\]根據新加坡节假日法规的第一条规定，
10月1日儿童节当天给小朋友放假。另外，新加坡人口很少，这条有关儿童节的法规也有利于促进那些喜爱度假的年轻夫妇，通过多生小孩获得更多的假期。

## 欧洲

### 德國

在冷战期间，东西德国在儿童节上做法迥然不同。首先，日期就不一样：东德定在6月1日，西德定在9月20日\[9\]；其名字也不同：东德称为“国际儿童节”（），西德称为“世界儿童节”（）。另外，节日的传统也有区别。

在东德，儿童节开始于1950年，源於紀念[二戰期間的](../Page/二戰.md "wikilink")1942年6月，[納粹德國鎗殺了捷克](../Page/納粹德國.md "wikilink")[利迪策村](../Page/利迪策.md "wikilink")16歲以上的男性公民140餘人和全部嬰兒，並把婦女和90名兒童押往[集中營的事件](../Page/集中營.md "wikilink")。爲了悼念利迪策村和全世界所有在[法西斯侵略戰爭中死難的兒童](../Page/法西斯.md "wikilink")，反對[帝國主義戰爭販子虐殺和毒害兒童](../Page/帝國主義.md "wikilink")，保障兒童權利，1949年11月[國際民主婦女聯合會遂在](../Page/國際民主婦女聯合會.md "wikilink")[蘇聯首都](../Page/蘇聯.md "wikilink")[莫斯科召開執委會](../Page/莫斯科.md "wikilink")，正式決定每年6月1日爲全世界少年兒童的節日，即國際兒童節。之后每年的这一天，都是孩子们一年中最开心的日子。往往会收到家长的祝福和礼物，在学校裡也会举行特别庆祝活动，如郊遊等。在西德，儿童节并没有特别的意义，许多人甚至都不知道这个节日的存在。

随着1990年[两德统一](../Page/两德统一.md "wikilink")，西德地区的儿童节日期和名称成为全德国官方的统一标准。然而在东德的许多地区，人们仍然遵循旧有的习惯和称呼。因此，在每年的6月1日，前東德地區的许多家长仍然会和孩子一起庆祝儿童节的到来。

### 東歐

6月1日等等

### 北歐

在瑞典芬蘭等國家有男女孩的專屬節

### 西歐

荷蘭

## 美洲

### 美國

在美国，儿童节的概念出现在[父亲节和](../Page/父亲节.md "wikilink")[母亲节之前](../Page/母亲节.md "wikilink")，但长期以来没有全国统一的儿童节。[2000年](../Page/2000年.md "wikilink")，一名4岁女童致信时任[美国总统的](../Page/美国总统.md "wikilink")[比爾·克林顿要求设立一个属于](../Page/比爾·克林顿.md "wikilink")[儿童的节日](../Page/儿童.md "wikilink")。为此，克林顿宣布将当年[10月8日定为儿童节](../Page/10月8日.md "wikilink")。到了2001年，时任总统[乔治·W·布什才将](../Page/乔治·W·布什.md "wikilink")[6月3日定为](../Page/6月3日.md "wikilink")“全国儿童节”，并宣布此后每年6月的第二个星期日都是“全国儿童节”。

但像其他欧洲国家一样，美国儿童节这一天很少组织举行社会性公众性庆祝活动。\[10\]

### 巴西

[10月12日定为儿童节](../Page/10月12日.md "wikilink")
（）\[11\]，與[主保聖人](../Page/主保聖人.md "wikilink")（守護聖者）日、同日慶祝。

## 国际儿童节旗帜

绿色的底色象征着成长，和谐，生气勃勃和丰富。最上方的蓝色人形象征着上帝，他赐予儿童和人类以博爱。红黄黑白四色儿童人形代表着四个人种。儿童是种族，宗教，身体，心理和社会的多样性达致和平与宽容的关键。由人形图像的脚组成的五角星代表着光明，象征着儿童创造的光明的未来。五角星上的五个角代表五大洲。处于正中央的地球形象，象征着我们共有的家园。人形图像伸出双手，代表世界人民的团结友爱。\[12\]

## 参考文献

## 外部链接

  - [International Children's day on the Children's Rights
    Portal](http://childrensrightsportal.org/focus/childrens-day/)

{{-}}

[Category:主题日](../Category/主题日.md "wikilink")
[Category:兒童](../Category/兒童.md "wikilink")

1.  政务院举行十二次会议通过节日放假办法　通令全国各地遵行，人民日報1949年12月24日，第1版
2.
3.
4.  [朝鲜举行联欢活动庆祝儿童节-新华网](http://www.xinhuanet.com/world/2017-06/01/c_1121072056.htm)
5.  [热闹的节日！蒙古的"母子节" | The World's Mother
    Salon](https://wm-salon.com/zh-CN/mongolia-motherchildrensday/)
6.  2012年9月25日內政部修正發布之《紀念日及節日實施辦法》第五條：「下列節日，由有關機關、團體、學校舉行慶祝活動：……四、兒童節：四月四日。」
7.  [特区领导与少年儿童共度“六一”儿童节-新闻动态-老挝人民民主共和国·金三角经济特区·管理委员会](http://www.gtsez.la/xwdt/show/101.html)
8.  [School Terms and Holidays
    for 2012](http://www.moe.gov.sg/schools/terms-and-holidays/)
9.  [childrens day
    , 2014](https://www.google.com/doodles/childrens-day-2014-germany) -
    [谷歌](../Page/谷歌.md "wikilink")
10. [外国儿童节面面观](http://gzdaily.dayoo.com/html/2011-06/01/content_1371470.htm)

11. [childrens day
    , 2014](https://www.google.com/doodles/childrens-day-2014-brazil) -
    [谷歌](../Page/谷歌.md "wikilink")
12. [International Children's Day Flag
    国际儿童节旗帜](http://www.crwflags.com/FOTW/FLAGS/int-chdn.html)