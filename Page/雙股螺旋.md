[DNA_orbit_animated_static_thumb.png](https://zh.wikipedia.org/wiki/File:DNA_orbit_animated_static_thumb.png "fig:DNA_orbit_animated_static_thumb.png")保持在一起双螺旋结构。\]\]
[Dna-split.png](https://zh.wikipedia.org/wiki/File:Dna-split.png "fig:Dna-split.png")

**雙股螺旋**由两条[螺旋](../Page/螺旋.md "wikilink")[曲线相互](../Page/曲线.md "wikilink")[缠绕而成](../Page/缠绕.md "wikilink")'\[1\]。最常见的雙股螺旋是表现生态遗传的[DNA](../Page/DNA.md "wikilink")。核酸复合物的双螺旋结构出现作为其的结果，并且是在确定其的基本组成部分。該術語隨著1968年的出版物《[雙螺旋：發現DNA結構的故事](../Page/雙螺旋：發現DNA結構的故事.md "wikilink")》（The
Double Helix: A Personal Account of the Discovery of the Structure of
DNA）是美國生物學家詹姆斯·杜威·沃森所寫的一本科學研究自傳，進入流行文化。

## 歷史

DNA雙股螺旋的模型于1953年由[華生和](../Page/詹姆斯·杜威·沃森.md "wikilink")[克立克发表首先於](../Page/弗朗西斯·克里克.md "wikilink")《[自然](../Page/自然_\(期刊\).md "wikilink")》\[2\]上發表。

## 核酸雜交

雜交（Hybridization）是互補[鹼基對結合形成雙股螺旋的過程](../Page/鹼基對.md "wikilink")。

## 参考文献

## 外部链接

  -
## 參見

  - [核酸結構](../Page/核酸結構.md "wikilink")
  - [G-四聯體](../Page/G-四聯體.md "wikilink")
  - [DNA纳米技术](../Page/DNA纳米技术.md "wikilink")
  - [核酸分子結構](../Page/核酸分子結構.md "wikilink")

{{-}}

[Category:DNA](../Category/DNA.md "wikilink")
[Category:生物物理学](../Category/生物物理学.md "wikilink")
[Category:分子结构](../Category/分子结构.md "wikilink")
[Category:螺旋](../Category/螺旋.md "wikilink")

1.
2.  <http://www.nature.com/nature/dna50/watsoncrick.pdf>