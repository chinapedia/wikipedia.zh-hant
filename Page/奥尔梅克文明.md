[缩略图](https://zh.wikipedia.org/wiki/File:Olmeca_head_in_Villahermosa.jpg "fig:缩略图")
**奥尔梅克文明**（Olmec）是已知的最古老的[美洲文明之一](../Page/美洲文明.md "wikilink")。它存在和繁盛于公元前1200年到公元前400年的[中美洲](../Page/中美洲.md "wikilink")（现在的[墨西哥中南部](../Page/墨西哥.md "wikilink")）。

「奧爾梅克」一詞源自[納瓦特爾語中用以指奧爾梅克人的單詞Ōlmēcatl](../Page/納瓦特爾語.md "wikilink")（發音：//，此詞在納瓦特爾語中之眾數形為Ōlmēcah，發音//），而在納瓦特爾語中，此詞為*ōlli*（意即「橡膠」，發音//）和*mēcatl*（意即「人」，發音//）的合成詞，故Ōlmēcatl在納瓦特爾語中的意思可解作「膠人」。\[1\]\[2\]

## 历史

奥尔梅克文明于公元前1200年左右产生于[中美洲](../Page/中美洲.md "wikilink")[圣洛伦索高地的热带](../Page/圣罗伦索.md "wikilink")[丛林当中](../Page/丛林.md "wikilink")。[圣洛伦索是早期奥尔梅克文明的中心](../Page/圣罗伦索.md "wikilink")，在繁盛了大约300年后，于公元前900年左右毁于暴力。其后奥尔梅克文明的中心迁移到靠近[墨西哥湾的](../Page/墨西哥湾.md "wikilink")。奥尔梅克文明最终在公元前400年左右消失，为[爱比奥尔梅克文化所取代](../Page/爱比奥尔梅克文化.md "wikilink")。其消失的具体原因尚不得知，但它影响了大量的[中美洲文明](../Page/中美洲文明.md "wikilink")。奥尔梅克文明的许多特征，如[金字塔和](../Page/金字塔.md "wikilink")[宫殿建造](../Page/宫殿.md "wikilink")，[玉器雕琢](../Page/玉器.md "wikilink")，[美洲虎和](../Page/美洲虎.md "wikilink")[羽蛇神崇拜也是后来中美洲各文明的共同元素](../Page/羽蛇神.md "wikilink")。大多数学者认为奥尔梅克文明是[玛雅](../Page/玛雅文明.md "wikilink")、[萨波特克](../Page/萨波特克文明.md "wikilink")、[提奥提华坎等文明的母体](../Page/提奥提华坎.md "wikilink")。但也有人认为奥尔梅克文明和其他中美洲文明的关系是姐妹关系。

一些学者提出假說[殷人东渡美洲论解释奥尔梅克文明突然出现以及奥尔梅克艺术风格和中国](../Page/殷人东渡美洲论.md "wikilink")[殷商时代艺术相像的現象](../Page/殷商.md "wikilink")。

## 艺术和建筑

奥尔梅克人创造了大量的[建筑和](../Page/建筑.md "wikilink")[雕塑作品](../Page/奥尔梅克雕像.md "wikilink")。他们用石头建造巨大的[宫殿和](../Page/宫殿.md "wikilink")[金字塔](../Page/金字塔.md "wikilink")，在[玉石上进行精美的雕刻](../Page/玉石.md "wikilink")，制作了大量的[陶器](../Page/陶器.md "wikilink")。奥尔梅克人最著名的艺术作品莫过于“[奥尔梅克巨石头像](../Page/奥尔梅克巨石头像.md "wikilink")”。这些在花岗岩上雕出的高达10英尺的巨大人头像显示了奥尔梅克人高超的技术水平。人头像都带有古怪的头盔；人脸具有東方人面部特征，部分卻具有[非洲人的面部特征](../Page/非洲.md "wikilink")。奧爾梅克文化奠定了中美洲古文明的基礎。

## 宗教

奥尔梅克人主要崇拜半人半[美洲虎的神](../Page/美洲虎.md "wikilink")，也崇拜[羽蛇神和](../Page/羽蛇神.md "wikilink")[谷神](../Page/谷神.md "wikilink")（The
Man of
Crops）。[宗教](../Page/宗教.md "wikilink")[信仰是奥尔梅克社会的主线](../Page/信仰.md "wikilink")，[圣洛伦索遗址就是一个宗教仪式中心和居民区的复合体](../Page/圣洛伦索.md "wikilink")。

## 文字

部分学者将[拉文塔出土的一些雕刻图案认读为奧爾梅克人的](../Page/拉文塔.md "wikilink")[文字](../Page/文字.md "wikilink")，但尚未得到学界的公认。

## 城市遗址

  - [圣洛伦索遗址](../Page/圣洛伦索.md "wikilink")
  - [拉文塔遗址](../Page/拉文塔.md "wikilink")（[La
    Venta](http://en.wikipedia.org/wiki/La_Venta)）：位于[墨西哥南部的](../Page/墨西哥.md "wikilink")[塔巴斯科州](../Page/塔巴斯科州.md "wikilink")。

## 图集

Image:The Wrestler (Olmec) by DeLange.jpg|摔跤手雕像 Image:Olmec mask
802.jpg| 玉面具 Image:Olmecmask.jpg|玉面具 Image:Olmec Bird jug.jpg|鸟容器
Image:Olmec fish vessel.jpg|鱼容器 Image:Olmec-style bottle 1.jpg|瓶

## 相關條目

  - [印加文明](../Page/印加文明.md "wikilink")
  - [阿茲特克文明](../Page/阿茲特克文明.md "wikilink")
  - [提奧提華坎文明](../Page/提奧提華坎文明.md "wikilink")
  - [瑪雅文明](../Page/瑪雅文明.md "wikilink")
  - [小北史前文明](../Page/小北史前文明.md "wikilink")

## 注釋

## 外部链接

  - [Mother Culture, or Only a
    Sister?](http://www.nytimes.com/2005/03/15/science/15olme.html?hp),
    The New York Times, March 15, 2005.
  - [神秘的古文明：奧爾梅克](http://www.epochtimes.com/b5/10/7/17/n2968230.htm)

<!-- end list -->

1.  *Olmecas* (n.d.). Think Quest. Retrieved September 20, 2012, from
    [link](http://library.thinkquest.org/28059/olmecas.htm)
2.  Coe (1968) p. 42