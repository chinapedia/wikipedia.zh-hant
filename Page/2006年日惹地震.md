**2006年日惹地震**发生在2006年5月27日[印尼当地时间](../Page/印尼.md "wikilink")5时54分（[格林尼治標準時間](../Page/格林尼治標準時間.md "wikilink")26日22时54分），[震中位于](../Page/地震.md "wikilink")[爪哇島南部](../Page/爪哇島.md "wikilink")[日惹市南方的](../Page/日惹.md "wikilink")[印度洋中](../Page/印度洋.md "wikilink")，[震级为](../Page/里氏地震规模地震震级.md "wikilink")6.3級。大约5百万人居住在离震中50公里以内的区域，确认的伤亡在2006年五月28日已达到数千人。受地震影响，[默拉皮火山的活动也加剧了](../Page/默拉皮火山.md "wikilink")。此次地震為爪哇島上的斷層受到2004年印度洋大地震以及2005年蘇門答臘地震劇烈影響，引發地底下斷層水平錯動所致。

## 引用

[de:Java
(Insel)\#Geologie](../Page/de:Java_\(Insel\)#Geologie.md "wikilink")

[Category:印尼地震](../Category/印尼地震.md "wikilink")
[Category:2006年印尼](../Category/2006年印尼.md "wikilink")
[Category:2006年地震](../Category/2006年地震.md "wikilink")
[Category:2006年5月](../Category/2006年5月.md "wikilink")