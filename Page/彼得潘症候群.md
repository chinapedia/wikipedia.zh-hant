**彼得潘症候群**（**Peter Pan
syndrome**）是個流行的心理學名詞，用來敘述一個在社會未成熟的成人。這個詞彙通常用於一般人，但也用於一些心理學的專業人士在普及[心理學上的描述](../Page/心理學.md "wikilink")。這個詞彙是來自[丹·凱利](../Page/丹·凱利.md "wikilink")（Dan
Kiley）於[1983年出版的書](../Page/1983年.md "wikilink")《彼得潘症候群：不曾長大的男人》（*The
Peter Pan Syndrome: Men Who Have Never Grown
Up*）。[丹·凱利也寫了一本夥伴書](../Page/丹·凱利.md "wikilink")《溫蒂窮境》（*The
Wendy
Dilemma*），於[1984年出版](../Page/1984年.md "wikilink")。[精神疾病診斷與統計手冊並沒有列入](../Page/精神疾病診斷與統計手冊.md "wikilink")「彼得潘症候群」這個疾病，而[美國心理學會](../Page/美國心理學會.md "wikilink")（American
Psychiatric
Association）並沒有承認這是一種心理疾病。應要歸類可分入依賴性人格疾患。深受此症困擾的人多半會有逃避大多數形式上的責任，從事一些不成熟的舉動，且仍會眷戀其青少年時期的時光，會設法不斷留住青春。基本上，此候群描述的正正的普遍女人的心理狀態，有人指出，這在是社會對「女性中心主義」的盲目所形社的所謂病症。不為女性犠牲，就是病了。

## 心理學

該症候群雖非精神醫學正式的診斷，卻普遍存在於因家庭、婚姻、社交問題尋求諮商輔導的個案中，患者也往往不自覺。近來，更有學者以新的英文單字「Kidult」，即kid（小孩）與adult（成人）的合體，稱呼這些具兒童心態的成年人。

專家指出，當事者常見的特徵約略如下：

1.  不負責任：表現任性、散漫，過於自我中心，出了差錯老愛怪罪別人。
2.  缺乏自信：恐懼失敗，不敢勇於任事，面對挑戰會找藉口逃避。
3.  依賴心強：害怕孤單、寂寞，希望隨時有人可以幫忙，滿足任何需求。
4.  難於堅持：挫折忍受度低，行事稍有不順或遭批評便易情緒化或放棄。
5.  關係障礙：與異性交往到需給予承諾時，便會臨陣脫逃，故不時更換伴侶，且對象越來越年輕，藉以緩解被要求結婚組織家庭的壓力。
6.  其他：穿著打扮如青少年，與本身年紀有所出入；好奇心強，愛嘗試新奇事物，喜歡熱鬧氣氛等。

患有該症候群者，或許習慣隨心所欲，在職場或人際互動上易受挫，總覺得遭到團體排斥，凡事格格不入，故換工作如家常便飯。即使成家立業，事不關己的特質也常讓配偶負擔沉重，彷彿在照顧另一個孩子般，造成彼此關係惡化。

## 參照

  - [傑斗](../Page/傑斗.md "wikilink")
  - [性副態的幼稚症](../Page/性副態的幼稚症.md "wikilink")（Paraphilic infantilism）
  - [麥可·傑克遜](../Page/麥可·傑克遜.md "wikilink")，時常將自己比作「彼得潘」的樂手
  - [麥田捕手](../Page/麥田捕手.md "wikilink")
  - 〈[What's My Age
    Again?](../Page/What's_My_Age_Again?.md "wikilink")〉，由流行龐克團體[blink-182所唱的歌曲](../Page/blink-182.md "wikilink")，原來的名稱是〈Peter
    Pan Complex〉
  - [邪惡女王症候群](../Page/邪惡女王症候群.md "wikilink")（Wicked queen
    syndrome），描述在社會中競爭，並對她對手的不幸幸災樂禍的女人的辭彙。

## 延伸閱讀

  - Kiley, Dan, Dr. (1983) *The Peter Pan Syndrome: Men Who Have Never
    Grown Up.* ISBN 0-396-08218-1

## 外部連結

  - Evan
    Bailyn，[什麼是彼得潘症候群？](http://www.evanbailyn.com/peterpansyndrome.html)

  - Chris Wayan，[Peter Pan Syndrome in
    reverse](http://www.worlddreambank.org/P/PETERPAN.HTM)

[Category:心理學](../Category/心理學.md "wikilink")
[Category:症候群](../Category/症候群.md "wikilink")