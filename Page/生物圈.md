[The_Earth_seen_from_Apollo_17.jpg](https://zh.wikipedia.org/wiki/File:The_Earth_seen_from_Apollo_17.jpg "fig:The_Earth_seen_from_Apollo_17.jpg")
[Biosphere_system.png](https://zh.wikipedia.org/wiki/File:Biosphere_system.png "fig:Biosphere_system.png")、[水圈](../Page/水圈.md "wikilink")、[岩石圈與](../Page/岩石圈.md "wikilink")[生態圈](../Page/生態圈.md "wikilink")。\]\]
**生物圈**（Biosphere）是指[地球上](../Page/地球.md "wikilink")**所有生態系的統合**整體，是地球的一个外层圈，其範圍大約為[海平面上下垂直約](../Page/海平面.md "wikilink")10公里。它包括地球上有[生命存在和由生命过程变化和转变的](../Page/生命.md "wikilink")[空气](../Page/空气.md "wikilink")、[陆地](../Page/陆地.md "wikilink")、[岩石圈和](../Page/岩石圈.md "wikilink")[水](../Page/水.md "wikilink")。从[地质学的广义角度上来看生物圈是结合所有生物以及它们之间的关系的全球性的生态系统](../Page/地质学.md "wikilink")，包括生物与岩石圈、[水圈和空气的相互作用](../Page/水圈.md "wikilink")。生物圈是一個封閉且能自我調控的系統\[1\]。地球目前是整个[宇宙中唯一已知的有生物生存的地方](../Page/宇宙.md "wikilink")。一般认为生物圈是从35亿年前[生命起源后](../Page/生命起源.md "wikilink")[演化而来的](../Page/演化论.md "wikilink")。\[2\]\[3\]

簡單來說地球上所有的[生物體和賴以生存的](../Page/生物體.md "wikilink")[環境](../Page/生态环境.md "wikilink")，合稱生物圈。

## 词源和使用

地质学家[爱德华·苏威斯于](../Page/爱德华·苏威斯.md "wikilink")1875年最早使用了生物圈一词。该词汇最初是地质学词语。用以显示[查尔斯·罗伯特·达尔文和](../Page/查尔斯·罗伯特·达尔文.md "wikilink")[马修·
方丹·
莫里的理论对](../Page/马修·_方丹·_莫里.md "wikilink")[地球科学的影响](../Page/地球科学.md "wikilink")。1920年代，生物圈一词获得其生态意义。1935年[生态系统这个词被引入](../Page/生态系统.md "wikilink")。[弗拉基米爾·伊萬諾維奇·維爾納茨基将](../Page/弗拉基米爾·伊萬諾維奇·維爾納茨基.md "wikilink")[生态学定义为研究生物圈的科学](../Page/生态学.md "wikilink")。如今，生物圈的概念集合了[天文学](../Page/天文学.md "wikilink")、[地质物理学](../Page/地质物理学.md "wikilink")、[气象学](../Page/气象学.md "wikilink")、[生物地理学](../Page/生物地理学.md "wikilink")、演化论、[地质学](../Page/地质学.md "wikilink")、[地质化学](../Page/地质化学.md "wikilink")、[水文学等多项科学](../Page/水文学.md "wikilink")，可以说它集合了所有与地球和生命有关的科学。

### 狭义定义

一些[生物学家和](../Page/生物学家.md "wikilink")[地质学家使用生物圈这个词时使用一个比较狭义的定义](../Page/地质学家.md "wikilink")。比如地质化学家将生物圈定义为所有生物的总和。「人為定義，隨著人的發現改變」这个定义将生物圈定义为地球的四个圈（另三个为[大气圈](../Page/大气圈.md "wikilink")、[水圈和](../Page/水圈.md "wikilink")[岩石圈](../Page/岩石圈.md "wikilink")）之一，通常认为该圈位于大气圈底部、水圈全部和岩石圈上部。这个狭义的定义是现代科学的专业化导致的。有些科学家更喜欢使用1960年代出现的“生态圈”这个词来代表这个狭义的定义。

### 廣義定義

另外的一些學者則採取廣義的**生物圈**定義：任何包含多個生態系，形成封閉而能自我調控的系統總稱 \[4\]。
例如：1990年代出現的生物圈計畫**[生物圈二號](../Page/生物圈二號.md "wikilink")**、[外太空中的某些具](../Page/外太空.md "wikilink")[生物的星球](../Page/生物.md "wikilink")，即被部份學者視為是一個生物圈。

### 生物環境和小生境

生長環境和小生境是地球生物圈的更小單位。生長環境是植物和動物生長的自然去處。例如，洞穴、山和海洋。在身長環境裡還有小生境。活物在小生境的生物圈內佔據地盤。

## 範圍

生物圈的範圍是陸域環境、淡鹹水區、[低層大氣](../Page/低層大氣.md "wikilink")。 垂直上下各約10000公尺。

## 相关条目

  - [岩石圈](../Page/岩石圈.md "wikilink")
  - [水圈](../Page/水圈.md "wikilink")
  - [大气圈](../Page/大气层.md "wikilink")

## 注釋

## 參考文獻

[he:אקולוגיה\#הביוספרה](../Page/he:אקולוגיה#הביוספרה.md "wikilink")

[Category:海洋学](../Category/海洋学.md "wikilink")
[Category:超个体](../Category/超个体.md "wikilink")
[Category:生物系统](../Category/生物系统.md "wikilink")
[Category:地球科学](../Category/地球科学.md "wikilink")

1.
2.
3.
4.