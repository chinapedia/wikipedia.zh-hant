[EastAsiaInstituteEntrance.jpg](https://zh.wikipedia.org/wiki/File:EastAsiaInstituteEntrance.jpg "fig:EastAsiaInstituteEntrance.jpg")
**东亚学院**（）是隶属于[路德维希港应用技术大学的一个外部机构](../Page/路德维希港.md "wikilink")，他的学科重点是：[日本](../Page/日本.md "wikilink")，[韓国](../Page/韓国.md "wikilink")[经济和](../Page/经济.md "wikilink")[中国经济](../Page/中国.md "wikilink")。这所学院早期被命名为：[东亚](../Page/东亚.md "wikilink")[市场营销](../Page/市场营销.md "wikilink")，是由[汉学家](../Page/汉学家.md "wikilink")[希格弗里德·恩兰特](../Page/希格弗里德·恩兰特.md "wikilink")[教授于](../Page/教授.md "wikilink")1989年建立的。之后于1992年，由[美国人](../Page/美国.md "wikilink")，[日本](../Page/日本.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")[彼得·维兹乐教授将学科重点向日本方向延伸](../Page/彼得·维兹乐.md "wikilink")。

## 名称

[Rhein_Ostasieninstitut.jpg](https://zh.wikipedia.org/wiki/File:Rhein_Ostasieninstitut.jpg "fig:Rhein_Ostasieninstitut.jpg")

  -
    英语：East Asia Institute
    法语：Institut pour l’Asie de l’est
    德语：Ostasieninstitut
    日语：東アジアセンタ—

## 学业

[MoriokaStudentinnen.JPG](https://zh.wikipedia.org/wiki/File:MoriokaStudentinnen.JPG "fig:MoriokaStudentinnen.JPG")\]\]
大学课程和一门以往通常情况下必须通过双学位课程才能学习的学科联系在一起，即：企业经济管理与中文或者日语相结合的形式。大约180名学生在他们毕业之后既不是汉学家，也不是日本语言文学研究者，而是一名拥有附加知识技能：中文，或者日语，及对这两个国家本身文化有一定了解的企业经济学家。因此，一个在中国，或日本，为期至少1年的国外学期是必要的。通过小班学习（平均每班大约40名学生），使师生之间的关系更加密切，融洽，交流更顺畅！

  -
    “当一般情况下，综合大学的学生和讲师正在与挤得满满的教室，坏了的打印机，布置不完善的图书馆或者陈旧的电脑作斗争的时候，与曼海姆大学隔江相对，空间距离仅仅相隔500米的这一边的情况却截然相反。这所坐落在路德维希港，莱茵河畔的东亚学院所具备的条件与一所美国私立学院不相上下。他拥有18名讲师，大约250名学生（其中1/3的外国学生比率），共享一所图书馆。图书馆的质量仅凭馆内不计其数的日，中新闻日报就不言而喻了。”

取自“Meier-Uni extra”，2002夏季学期

## 友好学校

以下分别是东亚学院在中国和日本的友好学校：

### 友好学校-[中国](../Page/中国.md "wikilink")

[VonRedenChina.jpg](https://zh.wikipedia.org/wiki/File:VonRedenChina.jpg "fig:VonRedenChina.jpg")\]\]

1.  [南宁](../Page/南宁.md "wikilink")[广西大学](../Page/广西大学.md "wikilink")，[广西壮族自治区](../Page/广西.md "wikilink")
2.  [桂林](../Page/桂林.md "wikilink") 电子技术大学，广西壮族自治区
3.  [贵阳](../Page/贵阳.md "wikilink")
    [贵州大学](../Page/贵州大学.md "wikilink")，[贵州省](../Page/贵州.md "wikilink")
4.  [张家口](../Page/张家口.md "wikilink")
    [河北北方学院](../Page/河北北方学院.md "wikilink")，[河北省](../Page/河北.md "wikilink")

其次，东亚学院正在与位于莱茵兰-普法茨州友好省市福建的福州大学就新的友好学校关系进行交涉。

### 友好学校-[日本](../Page/日本.md "wikilink")

1.  [高崎经济大学](../Page/高崎.md "wikilink")，[群马县](../Page/群马.md "wikilink")
2.  [枚方市](../Page/枚方.md "wikilink") 关西外国语大学
    [大阪府](../Page/大阪.md "wikilink")
3.  [秋田国际教养大学](../Page/秋田.md "wikilink")，[秋田县](../Page/秋田.md "wikilink")
4.  [名古屋市立大学](../Page/名古屋.md "wikilink")，[爱知县](../Page/爱知.md "wikilink")

学生们允许在以上这些友好大学中免费上学（除名古屋城市大学之外）。其次，学院还为学生在默克集团和戴姆勒克莱斯勒之类的公司提供实习机会。

## 大事年表

[KurtBeckJohannesRau.jpg](https://zh.wikipedia.org/wiki/File:KurtBeckJohannesRau.jpg "fig:KurtBeckJohannesRau.jpg")在东亚学院\]\]

  -
    1988年：东亚学院原身“东亚市场营销”（MO）作为[莱茵兰-普法尔茨州](../Page/莱茵兰-普法尔茨州.md "wikilink")-路德维希港大学试点课程建立
    1991年：第一批学生去中国完成他们的国外学期
    1992年：学科重点向日本方向扩展
    1994年：第一批毕业生，作为：Diplom-Betriebswirt (FH)顺利完成学业
    1997年：新校园在[莱茵河畔建成](../Page/莱茵河.md "wikilink")
    1997年：联邦总统Roman Herzog来访
    2000年：联邦总统[約翰內斯·勞Johannes](../Page/約翰內斯·勞.md "wikilink") Rau来访
    2002年：第一期“秀才”问世（http://xiucai.oai.de/）
    2004年：大学课程转型 从Diplom转向 Bachelor

## 校徽

东亚学院的校徽由5朵风格简易的[牡丹组成](../Page/牡丹.md "wikilink")。牡象征着，不仅在中国，同时也是在日本，这两个大学课程重点都能够顺利发展，蒸蒸日上。

## 黑尔弗里希收藏室／品

东亚学院收藏着诺伊施塔特-东南亚商人埃米尔·黑尔弗里希所有关于亚洲著作的收藏品。埃米尔·黑尔弗里希于1899年至1927年间在[印度尼西亚经商](../Page/印度尼西亚.md "wikilink")。1927年至1972年期间在[汉堡任HAPAG监视会主席](../Page/汉堡.md "wikilink")，兼东亚协会（OAVV）主席，以及国务委员。
黑尔弗里希在其92岁高龄之时迁回了他的故乡法尔茨，并在[葡萄酒之路上的诺伊施塔特度过了余生](../Page/葡萄酒之路上的诺伊施塔特.md "wikilink")。由于他没有孩子，他把财产都赠送给了他的故乡。其中，他的收藏品于1997年以长期租借的形式转到东亚学院。

## 九龙

[9_Drachen_Ostasieninstitut.jpg](https://zh.wikipedia.org/wiki/File:9_Drachen_Ostasieninstitut.jpg "fig:9_Drachen_Ostasieninstitut.jpg")
在东亚学院的图书馆内悬挂着一件很有中国特色的，直径120厘米，镀金雕刻品。这件艺术品上雕有九条形态各异的龙。

中国历史学家，诗人[闻一多先生提出了这一论点](../Page/闻一多.md "wikilink")：龙是中国远古时代不同[图腾结合的产物](../Page/图腾.md "wikilink")。一个民族可能曾经以蛇作为图腾。在他战胜了另一个以四脚兽作为图腾的民族之后，为了不使这个民族（之前的敌人）受到凌辱，可能会在原始的蛇形图腾上添上四个脚，形成新的联合图腾。就顺着这样的趋势不断发展，最终形成了今天我们所看到的龙的形象。

[L](../Category/德國大學.md "wikilink")