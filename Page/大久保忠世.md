**大久保忠世**是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[安土桃山時代的武將](../Page/安土桃山時代.md "wikilink")。[松平氏](../Page/松平氏.md "wikilink")（後來的[德川氏](../Page/德川氏.md "wikilink")）的家臣。父親是[三河國](../Page/三河國.md "wikilink")[額田郡上和田](../Page/額田郡.md "wikilink")（現今[愛知縣](../Page/愛知縣.md "wikilink")[岡崎市](../Page/岡崎市.md "wikilink")）[大久保氏支流的](../Page/大久保氏.md "wikilink")[大久保忠員](../Page/大久保忠員.md "wikilink")。母親是[三條西公條的女兒](../Page/三條西公條.md "wikilink")。[蟹江七本槍和](../Page/蟹江七本槍.md "wikilink")[德川十六神將之一](../Page/德川十六神將.md "wikilink")。通稱**新十郎**、**七郎右衛門**。

## 生平

[天文元年](../Page/天文_\(後奈良天皇\).md "wikilink")（1532年），作為[德川氏家臣](../Page/德川氏.md "wikilink")[大久保忠員的長男出生](../Page/大久保忠員.md "wikilink")。

[大久保氏由](../Page/大久保氏.md "wikilink")[德川家康的祖父](../Page/德川家康.md "wikilink")[松平清康一代開始就仕於](../Page/松平清康.md "wikilink")[松平氏](../Page/松平氏.md "wikilink")，忠世的家族是支流，但是在功勞上已經超越本家。忠世亦在[永祿](../Page/永祿.md "wikilink")6年（1563年）[三河一向一揆和](../Page/三河一向一揆.md "wikilink")[元龜](../Page/元龜.md "wikilink")3年（1573年）12月的[三方原之戰中参陣並立下戰功](../Page/三方原之戰.md "wikilink")。特別在三方原之戰中為了激勵戰敗後意志消沉的德川軍，與[天野康景一同在夜裡用](../Page/天野康景.md "wikilink")[鐵炮射擊](../Page/鐵炮.md "wikilink")[武田氏的陣地](../Page/武田氏.md "wikilink")[犀崖](../Page/犀崖.md "wikilink")（）而令武田軍陷入大混亂，敵方的大將[武田信玄對此說了](../Page/武田信玄.md "wikilink")「哎呀哎呀，即使戰勝了還是可怕的敵人啊」（）的說話來讚賞這次行動（這個逸話被記載在忠世的弟弟[大久保忠教所著的](../Page/大久保忠教.md "wikilink")『[三河物語](../Page/三河物語.md "wikilink")』中，可信性成疑）。

還有在[天正](../Page/天正.md "wikilink")3年（1575年）的[長篠之戰與弟弟](../Page/長篠之戰.md "wikilink")[忠佐](../Page/大久保忠佐.md "wikilink")、與力[成瀨正一](../Page/成瀨正一.md "wikilink")、[日下部定好一同在戰鬥中大活躍](../Page/日下部定好.md "wikilink")，受到[織田信長以](../Page/織田信長.md "wikilink")「就像黏性很強的藥膏一樣，變成完全不離開敵人的藥膏武士」（）的說話讚賞，被家康賞賜[大法螺](../Page/大法螺.md "wikilink")。同年12月，被家康任命為[二俣城城主](../Page/二俣城.md "wikilink")。忠世為防禦武田氏來襲而把城池改修，現在二俣城跡殘留下來的天守台和向著二俣城築起的[鳥羽山城庭園等建築物被認為是忠世所建](../Page/鳥羽山城.md "wikilink")。在天正10年（1582年）6月的[本能寺之變後](../Page/本能寺之變.md "wikilink")，家康在[甲斐和](../Page/甲斐.md "wikilink")[信濃擴張勢力](../Page/信濃.md "wikilink")，忠世成為[信州惣奉行](../Page/奉行.md "wikilink")（信州總奉行）而在[小諸城擔任](../Page/小諸城.md "wikilink")[在番](../Page/在番.md "wikilink")，並擔任[依田康國的](../Page/依田康國.md "wikilink")[後見人](../Page/後見人.md "wikilink")。天正13年（1585年）的[上田合戰中與](../Page/上田合戰.md "wikilink")[鳥居元忠和](../Page/鳥居元忠.md "wikilink")[平岩親吉一同参戰](../Page/平岩親吉.md "wikilink")，不過敗給[真田昌幸](../Page/真田昌幸.md "wikilink")。

另一方面，在政治上亦相當優秀。在同一時期，協助因為反抗家康而被追放的[本多正信回歸](../Page/本多正信.md "wikilink")，以及訓斥年輕時就很有聲望的[井伊直政](../Page/井伊直政.md "wikilink")。

天正18年（1590年）家康因為[後北條氏的滅亡而移封至關東](../Page/後北條氏.md "wikilink")，被[豐臣秀吉命令接收](../Page/豐臣秀吉.md "wikilink")[小田原城](../Page/小田原城.md "wikilink")4萬5千石。

在[文祿](../Page/文祿.md "wikilink")3年（1594年）死去，享年63歲。[大久保家的](../Page/大久保氏.md "wikilink")[家督由嫡男](../Page/家督.md "wikilink")[忠隣繼承](../Page/大久保忠隣.md "wikilink")。

## 家族

### 兄弟

1.  [大久保忠佐](../Page/大久保忠佐.md "wikilink")
2.  [大久保忠包](../Page/大久保忠包.md "wikilink")（1540-1561），1561年陣亡于[藤波繩手之戰](../Page/藤波繩手之戰.md "wikilink")
3.  [大久保忠寄](../Page/大久保忠寄.md "wikilink")（1547-1572），1572年陣亡于[三方原之戰](../Page/三方原之戰.md "wikilink")
4.  [大久保忠核](../Page/大久保忠核.md "wikilink")（1551-1574），1574年陣亡于[遠江國](../Page/遠江國.md "wikilink")[乾城之戰](../Page/乾城.md "wikilink")
5.  [大久保忠為](../Page/大久保忠為.md "wikilink")（1554-1616）
6.  [大久保忠長](../Page/大久保忠長.md "wikilink")（1554-1606）
7.  [大久保忠教](../Page/大久保忠教.md "wikilink")
8.  [大久保忠元](../Page/大久保忠元.md "wikilink")，叔父[大久保忠行養子](../Page/大久保忠行.md "wikilink")

### 子女

#### 兒子

1.  [大久保忠隣](../Page/大久保忠隣.md "wikilink")（1553-1628），母親是[近藤幸正之女兒](../Page/近藤幸正.md "wikilink")，[相模國](../Page/相模國.md "wikilink")[小田原藩主](../Page/小田原藩.md "wikilink")、[老中](../Page/老中.md "wikilink")
2.  [大久保忠康](../Page/大久保忠康.md "wikilink")，母親不詳，早亡
3.  [大久保忠基](../Page/大久保忠基.md "wikilink")，母親不詳，為[德川家康近侍](../Page/德川家康.md "wikilink")，[文祿年間中](../Page/文祿.md "wikilink")，以某種之事觸怒家康，君臣關係解消
4.  [大久保忠成](../Page/大久保忠成.md "wikilink")（1577-1672），母親不詳，[旗本](../Page/旗本.md "wikilink")5000石
5.  [大久保忠高](../Page/大久保忠高.md "wikilink")，母親不詳，早亡
6.  [大久保忠永](../Page/大久保忠永.md "wikilink")（1588-1646），母親是[小山田氏](../Page/小山田氏.md "wikilink")，[旗本](../Page/旗本.md "wikilink")500石

#### 女兒

1.  女，母親不詳，[設樂貞清妻](../Page/設樂貞清.md "wikilink")
2.  女，母親不詳，弟弟[大久保忠長次子](../Page/大久保忠長.md "wikilink")[大久保忠重妻](../Page/大久保忠重.md "wikilink")

## 登場作品

  - 『』（[德永真一郎](../Page/德永真一郎.md "wikilink")，[PHP文庫](../Page/PHP文庫.md "wikilink")，收錄在『』）

## 相關條目

  - [大久保氏](../Page/大久保氏.md "wikilink")
  - [德川十六神將](../Page/德川十六神將.md "wikilink")
  - [蟹江七本槍](../Page/蟹江七本槍.md "wikilink")

[Category:大久保氏](../Category/大久保氏.md "wikilink")
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Category:三河國出身人物](../Category/三河國出身人物.md "wikilink")
[Category:1532年出生](../Category/1532年出生.md "wikilink")
[Category:1594年逝世](../Category/1594年逝世.md "wikilink")