**伍廷芳**（），本名**叙**，字**文爵**，又名**伍才**，号**秩庸**，[齋號](../Page/齋號.md "wikilink")、筆名**觀渡廬**，祖籍[中國](../Page/中國.md "wikilink")[廣東](../Page/廣東.md "wikilink")[新會](../Page/新會.md "wikilink")，生於[南洋的英屬](../Page/南洋.md "wikilink")[馬六甲](../Page/馬六甲.md "wikilink")。[清末](../Page/清.md "wikilink")[民初](../Page/中華民國.md "wikilink")[外交家](../Page/外交家.md "wikilink")、[法學家](../Page/法學家.md "wikilink")、[书法家](../Page/书法家.md "wikilink")。他是首位取得外國**[律師](../Page/律師.md "wikilink")**資格的[華人](../Page/華人.md "wikilink")，也是[香港首名](../Page/香港.md "wikilink")[華人](../Page/華人.md "wikilink")**[大律師](../Page/大律師.md "wikilink")**和首名華人[立法局議員](../Page/香港立法會.md "wikilink")。後於[中國從政](../Page/中國.md "wikilink")，是近代有名的[政治家](../Page/政治家.md "wikilink")，官至中華民國[外交總長](../Page/外交總長.md "wikilink")。

## 殖民地生活

[Ng_Choy.jpg](https://zh.wikipedia.org/wiki/File:Ng_Choy.jpg "fig:Ng_Choy.jpg")

伍廷芳，本貫斗洞[伍氏](../Page/伍姓.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[廣東](../Page/廣東.md "wikilink")[新會陳沖鄉現龍村橋頭里](../Page/新會.md "wikilink")，斗洞伍天麟之後，世居新會城郊西墩官來橋，南宋末年名將伍隆起，正是其同宗。道光廿二年六月初二日生於英属马六甲[峇都安南](../Page/峇都安南_\(马六甲\).md "wikilink")，三歲時隨父親回中國[廣州](../Page/廣州.md "wikilink")[芳村定居](../Page/芳村.md "wikilink")，在[廣州讀書](../Page/廣州.md "wikilink")。十三歲時遭綁架，說服綁匪逃脫，十四歲時在親戚[陳藹亭陪同下以伍才之名往香港求學](../Page/陳藹亭.md "wikilink")，在[聖保羅書院就讀](../Page/聖保羅書院.md "wikilink")，畢業後於1861年任香港的高等審判庭、地方法院等的[翻譯](../Page/翻譯.md "wikilink")\[1\]。
1860年，伍才與[黃勝利用報社廢棄的中文](../Page/黃勝.md "wikilink")[鉛字](../Page/鉛字.md "wikilink")，一同創辦中國第一份中文報紙《[中外新報](../Page/中外新報.md "wikilink")》；同時，又協助陳藹亭創辦《[香港華字報](../Page/香港華字報.md "wikilink")》。1864年與牧師[何進善的長女](../Page/何福堂.md "wikilink")[何妙齡成婚](../Page/何妙齡.md "wikilink")。1871年調任港府巡理署譯員。

1874年與妻子\[2\]\[3\]自費赴[英國留學](../Page/英國.md "wikilink")，在英國[倫敦大學學院攻讀法律](../Page/倫敦大學學院.md "wikilink")，並於1874年入四大訟務律師學院之一的[林肯律師學院受大律師培訓](../Page/林肯律師學院.md "wikilink")\[4\]，1877年畢業，獲得倫敦大學學院所頒[法律博士](../Page/法律博士.md "wikilink")（LLD）學位，1876年通過[大律師資格考試](../Page/大律師.md "wikilink")，同期同學包括後任英國首相的[H·H·阿斯奎斯](../Page/H·H·阿斯奎斯.md "wikilink")。伍於1877年1月26日正式成爲大律師\[5\]。1月23日，拜會駐英公使[郭嵩燾](../Page/郭嵩燾.md "wikilink")，郭氏欲招為己用。

3月乘船離英奔喪，在船上認識候任[香港總督](../Page/香港總督.md "wikilink")[軒尼詩](../Page/軒尼詩_\(香港總督\).md "wikilink")。5月18日，香港政府司法机关决定-{准}-许伍才在香港法庭执行律师业务，香港[律政司](../Page/律政司.md "wikilink")[費立浦正式宣布](../Page/費立浦.md "wikilink")：伍才是第一位获准在[英国殖民地担任律师的中国人](../Page/英国殖民地.md "wikilink")\[6\]。6月22日，郭嵩焘、刘锡鸿联名上书，奏請朝廷委任伍才为驻英使馆三等参赞。[李鴻章得知伍回港](../Page/李鴻章.md "wikilink")，派[黎兆棠到香港洽談](../Page/黎兆棠.md "wikilink")，1877年10月6日，黎兆棠攜伍到天津與李鴻章見面。10月22日，李鴻章與沈葆楨各出黃金三千兩，共六千兩黃金作為伍的年薪。但伍才沒有接受，選擇回香港執業師\[7\]。1878年12月16日獲港督軒尼詩委任為首名華人[太平紳士](../Page/太平紳士.md "wikilink")，1879年，香港律政司返英，軒尼詩提議由伍署理，遭到在港英國人反對，只好作罷。1880年2月19日，港督接受香港各界华人领袖的建議，委任伍為[定例局首位華人](../Page/香港立法局.md "wikilink")[非官守議員](../Page/非官守議員.md "wikilink")\[8\]，同年他曾署任裁判司。在港期間，他積極支持港督軒尼詩的政策，反對歧視華人，並要求廢除公開笞刑和遏止販賣女童等。後因與妻子及妻舅[何添](../Page/何添_\(何福堂子\).md "wikilink")\[9\]\[10\]投資生意失敗，被迫辭去立法局議員一職。1881年12月20日，在特遣分艦隊「」號服役的英國皇孫[艾伯特維克托王子及](../Page/阿尔伯特·维克托王子_\(克拉伦斯和阿文代尔公爵\).md "wikilink")[喬治王子](../Page/喬治五世.md "wikilink")（即後來喬治五世長兄及喬治五世）隨分艦隊訪港，由於艦隊司令[海軍中將](../Page/海軍中將.md "wikilink")（Vice-Admiral
Earl of
Clanwilliam）的拒阻，將伍才等香港縉紳為王室成員籌備的許多歡迎節目省掉\[11\]\[12\]。伍才被嘉蘭威廉伯爵數次冷落，決心北上大陸發展\[13\]。

## 進入中國政壇

[Wu_Tingfang1.jpg](https://zh.wikipedia.org/wiki/File:Wu_Tingfang1.jpg "fig:Wu_Tingfang1.jpg")
1882年10月底，伍才以伍廷芳之名離港北上，到天津接受直隸總督兼北洋大臣李鴻章的邀請，任法律顧問，成為李鴻章的幕僚。其間曾參與1885年[中法新約](../Page/中法新約.md "wikilink")、1886年[长崎事件](../Page/长崎事件.md "wikilink")、1895年[馬關條約等的商議](../Page/馬關條約.md "wikilink")，擔任馬關條約換約全權大臣。亦曾任[中國鐵路公司總辦等職](../Page/中國鐵路公司.md "wikilink")，創辦中國歷史上第一條經政府批准興建使用的鐵路[唐胥鐵路](../Page/唐胥鐵路.md "wikilink")。

1896年11月16日，清廷敕封伍廷芳為出使美日秘国大臣，11月27日進宮覲見[光緒](../Page/光緒.md "wikilink")，光緒恩准伍回新會省親。1897年1月22日，伍省親後回到香港，港督[羅便臣熱烈相迎](../Page/威廉·羅便臣.md "wikilink")。3月13日與三等參贊[張蔭棠等人離開香港](../Page/張蔭棠.md "wikilink")，4月24日到達美國[華盛頓](../Page/華盛頓.md "wikilink")，5月1日正式就任駐美公使。上任後，不斷為華人爭取利益，抗議排華法案。1899年4月17日離開美國，5月3日到達[西班牙](../Page/西班牙.md "wikilink")[馬德里覲見西班牙攝政王太后](../Page/馬德里.md "wikilink")，遞交國書。8月底返回美國。1901年幫助[孔祥熙進入美國](../Page/孔祥熙.md "wikilink")。1902年伍廷芳回中國，與[沈家本同任法律修訂大臣](../Page/沈家本.md "wikilink")。之後至1906年期間，擬訂了中國最早的[商業法](../Page/商業法.md "wikilink")；提出廢除[凌遲等酷刑](../Page/凌遲.md "wikilink")；並按歐美等國辦法起草[訴訟法](../Page/訴訟法.md "wikilink")，建議使用[陪審團制度](../Page/陪審團.md "wikilink")。
伍廷芳曾任驻美公使。1905年，當時美國剛剛通過排華法案，墨西哥隨後亦宣布跟隨。當時，伍廷芳曾為了這件事在美國和墨西哥兩國之間不斷遊走，以解決危機。1907年12月，伍廷芳再次出使美國、[墨西哥](../Page/墨西哥.md "wikilink")、秘魯及[古巴](../Page/古巴.md "wikilink")，1908年2月啟程，經過日本東京，前首相[大隈重信設宴款待](../Page/大隈重信.md "wikilink")，後不歡而散，3月7日抵達華盛頓，後來與美國總統[罗斯福漸成好友](../Page/西奧多·罗斯福.md "wikilink")，伍在美國目睹主張推翻清朝和帝制的高漲革命浪潮，又拜訪科學家、發明家[愛迪生](../Page/愛迪生.md "wikilink")，邀請其到中國遊玩。1909年5月31日，伍開始南美洲之行，先後到[巴拿馬](../Page/巴拿馬.md "wikilink")、[厄瓜多爾](../Page/厄瓜多爾.md "wikilink")、[秘魯慰問華僑](../Page/秘魯.md "wikilink")，與秘魯簽訂《中秘條約》。8月底回美國，不久又到墨西哥、古巴遞交國書。免職後於1910年3月離開美國經歐洲、新加坡、香港，進京辭職，上《奏请剪发不易服折》，後稱病寓居於[上海](../Page/上海.md "wikilink")。

[Tomb_of_Wu_Tingfang.jpg](https://zh.wikipedia.org/wiki/File:Tomb_of_Wu_Tingfang.jpg "fig:Tomb_of_Wu_Tingfang.jpg")
[伍廷芳.jpg](https://zh.wikipedia.org/wiki/File:伍廷芳.jpg "fig:伍廷芳.jpg")
1911年[辛亥革命後](../Page/辛亥革命.md "wikilink")，伍廷芳支持革命，上《奏请监国赞成共和文》、《致清庆邸书》，於12月的[南北議和中代表南方政府](../Page/南北議和.md "wikilink")，出任民軍總代表。1912年1月1日[中華民國建立後](../Page/中華民國.md "wikilink")，任[南京臨時政府司法總長](../Page/南京.md "wikilink")。4月[袁世凱掌權後](../Page/袁世凱.md "wikilink")，伍廷芳離職，再寓居上海。1916年6月6日袁世凱死後，[黎元洪繼任為總統](../Page/黎元洪.md "wikilink")。伍廷芳出任[段祺瑞總理政府的](../Page/段祺瑞.md "wikilink")[外交總長](../Page/外交總長.md "wikilink")。1917年發生「[府院之爭](../Page/府院之爭.md "wikilink")」，伍廷芳反對加入[協約國](../Page/協約國.md "wikilink")，并提出辞职，段祺瑞被黎元洪解除職務後，伍廷芳一度出任代理[國務總理](../Page/國務總理.md "wikilink")。後來黎元洪迫于[张勋的压力](../Page/张勋.md "wikilink")，要伍签署解散国会的命令，伍拒绝，辞职。应孙中山的号召，南下[廣州](../Page/廣州.md "wikilink")，出任[护法军政府的外交部部長](../Page/护法运动.md "wikilink")。1919年，任[廣東省省長及後一路追隨](../Page/廣東.md "wikilink")[孫中山](../Page/孫中山.md "wikilink")。1921年孫中山於廣州就任[非常大總統](../Page/非常大總統.md "wikilink")，伍任外交部部長，兼財政部部長、廣東省省長；更曾一度任代行非常大總統。1922年6月23日病逝廣州。伍廷芳墓1988年遷至广州[越秀公园内](../Page/越秀公园.md "wikilink")。

## 評價

[胡適讚嘆伍廷芳之外交風範](../Page/胡適.md "wikilink")：「他在海外做外交官時，全靠他的古怪行為和古怪的議論，壓倒了西洋人的氣焰，引起了他們的好奇心，居然能使一個弱國代表受到許多外人的敬重。」

鑒於伍廷芳在西法上的造詣和成就，清廷特委任他和沈家本主持變法修律。他以紮實的刑律知識改造《大清現行刑律》，力主廢除凌遲、梟首、戮屍等酷刑，並禁止刑訊，建立符合現代文明的刑法觀。在此基礎上，重新劃定中國法律部門，結束了「諸法合體」的傳統模式，被孫中山稱之為「中國刑法開新紀元」。
雖然伍廷芳真正參與修律的時間只有4年（1903年—1906年），但對自己格外強調刑罰「改重從輕」的目標落實得恰當而到位，主要表現在取消刑訊上。\[14\]

## 家庭

[伍廷芳總長夫人肖像.jpg](https://zh.wikipedia.org/wiki/File:伍廷芳總長夫人肖像.jpg "fig:伍廷芳總長夫人肖像.jpg")
伍廷芳之妻子[何妙齡](../Page/何妙齡.md "wikilink")，妻舅為[何啟爵士](../Page/何啟.md "wikilink")，二人為[何福堂牧師之子女](../Page/何福堂.md "wikilink")\[15\]。伍廷芳與何妙齡育有一子[伍朝樞](../Page/伍朝樞.md "wikilink")，他是民國時期的外交家，曾出任[國民政府駐美國大使](../Page/國民政府.md "wikilink")。曾孫伍浩平為美國聯邦法院第四位華裔法官。
伍廷芳在英國進修其間與一英國女子[玛丽娅誕下一女](../Page/玛丽娅.md "wikilink")[伍麗芳及三歲時將她帶回](../Page/伍麗芳.md "wikilink")[香港撫養](../Page/香港.md "wikilink")，而聚多的私人文物及藝術收藏品由[伍麗芳及其子孫](../Page/伍麗芳.md "wikilink")[鄭月娥及](../Page/鄭月娥_\(伍廷芳孫\).md "wikilink")[趙泰來繼承](../Page/趙泰來.md "wikilink")\[16\]
\[17\] \[18\] \[19\] \[20\] \[21\] \[22\] \[23\]。

  - [伍才](../Page/伍才.md "wikilink")（**伍廷芳**）＝[何玫瑰](../Page/何玫瑰.md "wikilink")\[24\]（[何妹貴](../Page/何妹貴.md "wikilink")\[25\]、[何妙齡](../Page/何妙齡.md "wikilink")）＝[玛丽娅](../Page/玛丽娅.md "wikilink")
      - [伍麗芳](../Page/伍麗芳.md "wikilink")＝[鄭禹](../Page/鄭禹.md "wikilink")\[26\]（父親為[溥儀](../Page/溥儀.md "wikilink")[幕僚](../Page/幕僚.md "wikilink")[鄭孝胥](../Page/鄭孝胥.md "wikilink")）
          - [鄭廣元](../Page/鄭廣元.md "wikilink")\[27\]（[鄭廣淵](../Page/鄭廣淵.md "wikilink")）＝[金欣如](../Page/金欣如.md "wikilink")（[韞龢](../Page/韞龢.md "wikilink")，[溥儀二妹](../Page/溥儀.md "wikilink")）
              - [鄭英才](../Page/鄭英才.md "wikilink")
              - [鄭爽](../Page/鄭爽_\(1936年出生\).md "wikilink")
              - [鄭潔](../Page/鄭潔_\(金欣如之女\).md "wikilink")
              - [鄭大力](../Page/鄭大力.md "wikilink")
          - [鄭月娥](../Page/鄭月娥_\(伍廷芳孫\).md "wikilink")
          - [鄭銀河](../Page/鄭銀河.md "wikilink")＝[趙世民](../Page/趙世民.md "wikilink")（祖父為[袁世凱心腹](../Page/袁世凱.md "wikilink")[趙秉鈞](../Page/趙秉鈞.md "wikilink")）
              - [趙令炎](../Page/趙令炎.md "wikilink")
              - [趙泰來](../Page/趙泰來.md "wikilink")\[28\]（知名文物及藝術收藏家）＝[陳卓宜](../Page/陳卓宜.md "wikilink")
              - [趙志堅](../Page/趙志堅_\(趙泰來妹\).md "wikilink")
              - 另有兩兄四弟妹
          - [鄭日明](../Page/鄭日明.md "wikilink")＝[王菊芳](../Page/王菊芳.md "wikilink")
          - [鄭勇](../Page/鄭勇.md "wikilink")
      - [伍朝樞](../Page/伍朝樞.md "wikilink")＝[何瑞金](../Page/何瑞金.md "wikilink")（[何寶芳](../Page/何寶芳.md "wikilink")\[29\]，[何啟長女](../Page/何啟.md "wikilink")）
          - [伍競仁](../Page/伍競仁.md "wikilink")\[30\]＝[鄭鏡宇](../Page/鄭鏡宇.md "wikilink")\[31\]
              - \[32\]\[33\]\[34\]\[35\]
          - [伍慶培](../Page/伍慶培.md "wikilink")＝[陳瓊惠](../Page/陳瓊惠.md "wikilink")（[陳策之女](../Page/陳策_\(民國\).md "wikilink")）\[36\]\[37\]
          - [伍繼先](../Page/伍繼先.md "wikilink")
          - [伍玉仙](../Page/伍玉仙.md "wikilink")＝[章衣萍](../Page/章衣萍.md "wikilink")\[38\]\[39\]
              - [章念天](../Page/章念天.md "wikilink")

## 著作

  - 《伍廷芳集》
  - 《中华民国图治议》
  - 《美国视察记》
  - 《伍秩庸先生公》

## 参考文献

### 引用

### 来源

  - [高芾](../Page/高芾.md "wikilink")（2002年）：[夕花朝拾：虚拟的墨西哥危机](http://www.nanfangdaily.com.cn/zm/20021212/wh/zl/200212120592.asp)，《[南方周末](../Page/南方周末.md "wikilink")》2002年12月12日號.
  - 孔祥吉、村田雄二郎：〈[日本机密档案中的伍廷芳](http://qsyj.iqh.net.cn/CN/volumn/volumn_1200.shtml)〉.

{{-}}

|         |
| ------- |
| 中華民国軍政府 |

[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:清朝外交官](../Category/清朝外交官.md "wikilink")
[Category:出使美国钦差大臣](../Category/出使美国钦差大臣.md "wikilink")
[Category:出使日斯巴尼亚国钦差大臣](../Category/出使日斯巴尼亚国钦差大臣.md "wikilink")
[Category:出使秘鲁国钦差大臣](../Category/出使秘鲁国钦差大臣.md "wikilink")
[Category:出使墨西哥国钦差大臣](../Category/出使墨西哥国钦差大臣.md "wikilink")
[Category:出使古巴国钦差大臣](../Category/出使古巴国钦差大臣.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:辛亥革命人物](../Category/辛亥革命人物.md "wikilink")
[N](../Category/前香港立法局議員.md "wikilink")
[Category:香港法律界人士](../Category/香港法律界人士.md "wikilink")
[N](../Category/太平紳士.md "wikilink")
[Category:倫敦大學校友](../Category/倫敦大學校友.md "wikilink")
[Category:伦敦大学学院校友](../Category/伦敦大学学院校友.md "wikilink")
[Category:聖保羅書院校友](../Category/聖保羅書院校友.md "wikilink")
[Category:新會人](../Category/新會人.md "wikilink")
[Category:馬六甲州人](../Category/馬六甲州人.md "wikilink")
[Category:马来西亚广府人](../Category/马来西亚广府人.md "wikilink")
[T廷芳](../Category/伍姓.md "wikilink")
[Category:中華民國外交總長](../Category/中華民國外交總長.md "wikilink")
[Category:中国法学家](../Category/中国法学家.md "wikilink")
[Category:中国书法家](../Category/中国书法家.md "wikilink")
[Category:英國律師](../Category/英國律師.md "wikilink")
[Category:中華民國國務總理](../Category/中華民國國務總理.md "wikilink")
[Category:中華民國外交部特派駐滬交涉員](../Category/中華民國外交部特派駐滬交涉員.md "wikilink")

1.

2.

3.

4.

5.  林肯律師學院
    [伍廷芳簡介](http://www.lincolnsinn.org.uk/images/word/Library/WuTingFang.pdf)

6.

7.
8.

9.
10.

11.

12.

13.

14. <https://knews.cc/zh-tw/history/vr422.html>

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.
25.

26. 郭招金，末代皇朝的子孙，北京：团结出版社，1991年，第46页

27.
28.

29.

30.
31.

32.

33.

34.

35.

36.

37.

38.

39.