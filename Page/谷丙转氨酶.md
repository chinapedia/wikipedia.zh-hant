**谷丙转氨酶**（英文：****，缩写**ALT**）是一种[转氨酶](../Page/转氨酶.md "wikilink")（，存在于[血浆及多种身体](../Page/血浆.md "wikilink")[组织中](../Page/组织.md "wikilink")，但最常见与[肝脏关联](../Page/肝脏.md "wikilink")。也叫**血清谷氨酸丙酮酸转氨酶**（）或**丙氨酸转氨酶**（）。

## 功能

谷丙转氨酶能够[催化将](../Page/催化.md "wikilink")[氨基从](../Page/氨基.md "wikilink")[丙氨酸转移到](../Page/丙氨酸.md "wikilink")[α-酮戊二酸上的](../Page/α-酮戊二酸.md "wikilink")[转氨基反应](../Page/转氨基反应.md "wikilink")，这个可逆转氨基反应的产物为[丙酮酸和](../Page/丙酮酸.md "wikilink")[谷氨酸](../Page/谷氨酸.md "wikilink")。

  -
    [丙氨酸](../Page/丙氨酸.md "wikilink") +
    [α-酮戊二酸](../Page/α-酮戊二酸.md "wikilink")
    [丙酮酸](../Page/丙酮酸.md "wikilink") +
    [谷氨酸](../Page/谷氨酸.md "wikilink")

[Alanine_amino_transférase.png](https://zh.wikipedia.org/wiki/File:Alanine_amino_transférase.png "fig:Alanine_amino_transférase.png")

## 临床意义

谷丙转氨酶，和[谷草转氨酶类似](../Page/谷草转氨酶.md "wikilink")，是临床上进行[肝功能测试的一个标准](../Page/肝功能测试.md "wikilink")，来确定肝脏是否健康。诊断上，基本上都是以“单位每升”（U/L）为单位进行测量。

### 含量偏高

谷丙转氨酶的含量升高往往意味着[酒精肝或病毒性](../Page/酒精肝.md "wikilink")[肝炎](../Page/肝炎.md "wikilink")，[充血性心力衰竭](../Page/充血性心力衰竭.md "wikilink")、[肝损伤](../Page/肝损伤.md "wikilink")、[胆管损伤](../Page/胆管.md "wikilink")，传染性[单核细胞增多症或](../Page/单核细胞增多症.md "wikilink")[肌病等疾病的存在](../Page/肌病.md "wikilink")。因此，谷丙转氨酶常常被用来确认肝脏的问题。但是，谷丙转氨酶含量升高并不一定意味着病变的发生。一天内谷丙转氨酶的含量也会有正常的起伏。谷丙转氨酶含量也会由于剧烈运动而升高\[1\]。

当血液中的谷丙转氨酶含量增高时，其潜在的原因可以通过测量其他酶而确定。例如，由于肝细胞损伤导致的谷丙转氨酶含量升高，可以通过测量[碱性磷酸酶与胆管损伤区别](../Page/碱性磷酸酶.md "wikilink")。同样，肌病也可以通过测量[肌氨酸致活酶确定](../Page/肌氨酸致活酶.md "wikilink")。

许多年来，美国红十字会使用谷丙转氨酶测试作为为确保血液供应安全所进行的一系列测试之一，并且推迟谷丙转氨酶升高的捐献者的捐献时间。这样做的目的是为了确认捐献者是否传染上了[丙肝](../Page/丙肝.md "wikilink")，因为在当时还没有专门的测试方法来确认丙肝。随着对丙肝的第二代[酵素免疫分析法的出现](../Page/酵素免疫分析法.md "wikilink")，美国红十字会改变了该政策。在2003年7月，此前因为谷丙转氨酶含量增高被取消捐献资格的人，若没有其它的问题，可以继续捐献血液\[2\]。

## 参见

  - [肝功能测试](../Page/肝功能测试.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  -
[Category:生物标志物](../Category/生物标志物.md "wikilink")
[Category:肝脏功能试验](../Category/肝脏功能试验.md "wikilink")
[Category:酶](../Category/酶.md "wikilink") [Category:EC
2.6.1](../Category/EC_2.6.1.md "wikilink")
[Category:肝脏病学](../Category/肝脏病学.md "wikilink")
[Category:谷氨酸（神經遞質）](../Category/谷氨酸（神經遞質）.md "wikilink")

1.  Paul T. Giboney M.D., [Mildly Elevated Liver Transaminase Levels in
    the Asymptomatic
    Patient](http://www.aafp.org/afp/20050315/1105.html), *American
    Family Physician*.
2.  [美国红十字会献血条件](http://www.tompkins-redcross.org/donor_req.htm)