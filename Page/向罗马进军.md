[March_on_Rome.jpg](https://zh.wikipedia.org/wiki/File:March_on_Rome.jpg "fig:March_on_Rome.jpg")發動不流血政變，成功奪取国家政权。\]\]
**向罗马进军**（），又称**進軍羅馬**，是1922年10月28日，[貝尼托·墨索里尼因為不滿](../Page/貝尼托·墨索里尼.md "wikilink")[法西斯黨在](../Page/法西斯黨.md "wikilink")1921年的[意大利國會選舉中的](../Page/意大利國會.md "wikilink")535席只取得2個議席，號召3萬名支持者（俗稱[黑衫軍](../Page/黑衫軍.md "wikilink")）進入[羅馬示威](../Page/羅馬.md "wikilink")，此事件不但使[墨索里尼成為](../Page/墨索里尼.md "wikilink")[首相](../Page/意大利總理列表.md "wikilink")，也讓全世界瞭解了法西斯的實力，是[国家法西斯党成功夺取意大利政权的标志](../Page/国家法西斯党.md "wikilink")。

## 過程

[March_on_Rome_1922_-_Alle_porte_di_Roma.png](https://zh.wikipedia.org/wiki/File:March_on_Rome_1922_-_Alle_porte_di_Roma.png "fig:March_on_Rome_1922_-_Alle_porte_di_Roma.png")

1922年10月16日，[墨索里尼在此前全国性的一系列演讲后](../Page/墨索里尼.md "wikilink")，决定采取强硬措施进行夺权。同时他害怕继续等待会导致[喬利蒂窃取他的地位](../Page/喬瓦尼·喬利蒂.md "wikilink")。

在10月底墨索里尼安排四万名穿黑衫的法西斯黨眾，組成四個[師](../Page/師.md "wikilink")（俗稱[黑衫軍](../Page/黑衫軍.md "wikilink")，）向罗马挺进。意大利[军队和](../Page/军队.md "wikilink")[警察的指挥官們保持中立](../Page/警察.md "wikilink")，並不驅逐他們。法西斯的报纸《意大利民族报》刊登了大字标题“军队與[黑衫军联合起來](../Page/黑衫军.md "wikilink")！”。

国王[艾曼紐三世听闻自己只有纪律松弛的守军六千人](../Page/维托里奥·埃马努埃莱三世.md "wikilink")，要对抗意志坚定的[法西斯百万雄師的时候](../Page/法西斯.md "wikilink")，立即屈服了。翌日，墨索里尼拜為[宰相](../Page/宰相.md "wikilink")，组建[內閣](../Page/內閣.md "wikilink")。

## 參見

  - [法西斯主義](../Page/法西斯主義.md "wikilink")
  - [納粹主義](../Page/納粹主義.md "wikilink")

[Category:贝尼托·墨索里尼](../Category/贝尼托·墨索里尼.md "wikilink")
[Category:国家法西斯党](../Category/国家法西斯党.md "wikilink")
[Category:義大利歷史](../Category/義大利歷史.md "wikilink")
[Category:1920年代政变及未遂政變](../Category/1920年代政变及未遂政變.md "wikilink")
[Category:法西斯主义](../Category/法西斯主义.md "wikilink")
[Category:1922年](../Category/1922年.md "wikilink")
[Category:一战战争后果](../Category/一战战争后果.md "wikilink")