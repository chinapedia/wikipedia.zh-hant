****（Nemesis）是第128颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1872年11月25日发现。的[直径为](../Page/直径.md "wikilink")188.2千米，[质量为](../Page/质量.md "wikilink")7.0×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1665.175天。

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1872年发现的小行星](../Category/1872年发现的小行星.md "wikilink")