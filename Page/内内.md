**内内**（，原名為，）是一位[巴西职业](../Page/巴西.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，现效力于[美國](../Page/美國.md "wikilink")[NBA聯盟的](../Page/NBA.md "wikilink")[休斯敦火箭](../Page/休斯敦火箭.md "wikilink")。

## 职业生涯

内内在2002年[NBA选秀中于第](../Page/NBA选秀.md "wikilink")1轮第7顺位被[纽约尼克斯选中](../Page/纽约尼克斯.md "wikilink")，之后被交易到[丹佛掘金隊](../Page/丹佛掘金隊.md "wikilink")。内内场均获得27.5分钟出场时间，能得到10.7分、6.3个篮板、1.7次助攻和1.2次抢断。内内生涯[篮板球总数超过](../Page/篮板球.md "wikilink")1000个，单场最高助攻数8次，抢断6次。内内也曾在[2002-03赛季入选最佳新秀一队](../Page/2002-03_NBA赛季.md "wikilink")，并在最佳新秀评选中位列第6。\[1\]

NBA西區準決賽戰成2：2平手後火箭傳出壞消息；今年(2017)季後賽不時有亮眼表現的替補中鋒努內（Nene
Hilario）由於左內收肌撕裂、賽季宣告提前報銷。\[2\]

## 参考资料

[Category:巴西男子篮球运动员](../Category/巴西男子篮球运动员.md "wikilink")
[Category:丹佛掘金队球员](../Category/丹佛掘金队球员.md "wikilink")
[Category:华盛顿奇才队球员](../Category/华盛顿奇才队球员.md "wikilink")
[Category:休斯頓火箭隊球員](../Category/休斯頓火箭隊球員.md "wikilink")
[Category:2012年夏季奧林匹克運動會籃球運動員](../Category/2012年夏季奧林匹克運動會籃球運動員.md "wikilink")
[Category:2016年夏季奧林匹克運動會籃球運動員](../Category/2016年夏季奧林匹克運動會籃球運動員.md "wikilink")
[Category:巴西奥运篮球运动员](../Category/巴西奥运篮球运动员.md "wikilink")

1.  [内内-网易体育资料库](http://data.sports.163.com/athlete/home/0005000EBERE.html)

2.   NBA戰況 {{\!}} NBA 台灣|last=聯合新聞網|newspaper=NBA
    台灣|accessdate=2018-06-05|language=zh-TW}}