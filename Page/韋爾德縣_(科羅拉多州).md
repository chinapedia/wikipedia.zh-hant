**韋爾德县** (**Weld County,
Colorado**)是[美國](../Page/美國.md "wikilink")[科羅拉多州北部的一個縣](../Page/科羅拉多州.md "wikilink")，北鄰[內布拉斯加州和](../Page/內布拉斯加州.md "wikilink")[懷俄明州](../Page/懷俄明州.md "wikilink")。面積8,443平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口180,936人，2005年人口為228,943人。\[1\]
縣治[格里利](../Page/格里利_\(科羅拉多州\).md "wikilink") (Greeley)。

成立於1861年11月1日，是該州最早成立的十七個縣之一。縣名紀念[科羅拉多準州首任州務卿劉易斯](../Page/科羅拉多準州.md "wikilink")·韋爾德
（Lewis Weld）。\[2\]\[3\]

## 参考文献

<div class="references-small">

<references />

</div>

[W](../Category/科罗拉多州行政区划.md "wikilink")

1.
2.
3.  <http://www.colorado.gov/dpa/doit/archives/arcgov.html>