**黄兴路站**位于[上海](../Page/上海.md "wikilink")[杨浦区](../Page/杨浦区.md "wikilink")[控江路](../Page/控江路.md "wikilink")[黄兴路](../Page/黄兴路.md "wikilink")，为[上海轨道交通8号线的地下](../Page/上海轨道交通8号线.md "wikilink")[岛式车站](../Page/岛式站台.md "wikilink")，于2007年12月29日启用。

## 歷史

  - 2007年9月16日，上海举行首届-{[城市公共交通周](../Page/城市公共交通周.md "wikilink")}-及无车日在黄兴路站拉开序幕，260位[杨浦区居民和其他市民參與列车试乘](../Page/杨浦区.md "wikilink")。\[1\]
  - 2007年12月29日，車站正式啟用。

## 公交换乘

6、8、90、169、220、308、522、538、723、819、842、870、975、大桥三线、大桥四线

## 车站出口

  - 1号口：控江路北侧，源泉路西侧
  - 2号口：无
  - 3号口：控江路南侧，靖宇南路西侧
  - 4号口
    [Lift.svg](https://zh.wikipedia.org/wiki/File:Lift.svg "fig:Lift.svg")：控江路北测，靖宇南路东侧

注：[<File:Lift.svg>](https://zh.wikipedia.org/wiki/File:Lift.svg "fig:File:Lift.svg")
设有无障碍电梯

## 参考資料

[Category:杨浦区地铁车站](../Category/杨浦区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")

1.  [东方网：上海首届城市公共交通周及无车日活动启动](http://xwcb.eastday.com/c/20070917/u1a352768.html)