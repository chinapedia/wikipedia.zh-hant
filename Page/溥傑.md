**溥傑**（；），乳名「**譽格**」，[字](../Page/表字.md "wikilink")「**俊之**」，[號](../Page/號.md "wikilink")「**秉藩**」，[清朝末代](../Page/清朝.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")[溥儀的胞弟](../Page/溥儀.md "wikilink")。

## 家世

溥傑的祖父為[道光帝七子](../Page/道光帝.md "wikilink")、[咸豐帝之弟醇賢親王](../Page/咸豐帝.md "wikilink")[奕譞](../Page/奕譞.md "wikilink")，伯父為[光緒帝](../Page/光緒帝.md "wikilink")，父親[載灃繼承醇親王爵位](../Page/載灃.md "wikilink")，後因輔政為攝政王。母親[幼兰是正白旗](../Page/幼兰.md "wikilink")[瓜尔佳氏](../Page/瓜尔佳氏.md "wikilink")[榮祿之女](../Page/榮祿.md "wikilink")。

溥傑有兄弟三人，姐妹七人。依次為[溥儀](../Page/溥儀.md "wikilink")（1906年生）、[韞瑛](../Page/韞瑛.md "wikilink")（1909年生，嫁[潤良](../Page/潤良.md "wikilink")）、[韞龢](../Page/韞龢.md "wikilink")（1911年生，嫁[鄭廣元](../Page/鄭廣元.md "wikilink")）、[韞穎](../Page/韞穎.md "wikilink")（1913年生，嫁[潤麒](../Page/潤麒.md "wikilink")）、[韞嫻](../Page/韞嫻.md "wikilink")（1914年生，嫁[趙琪璠](../Page/趙琪璠.md "wikilink")）、溥倛（1915年生，早夭）、[韞馨](../Page/韞馨.md "wikilink")（1917年生，嫁[萬嘉熙](../Page/萬嘉熙.md "wikilink")）、[溥任](../Page/溥任.md "wikilink")（1918年生）、[韞娛](../Page/韞娛.md "wikilink")（1919年生，嫁[王愛蘭](../Page/王愛蘭.md "wikilink")）、[韞歡](../Page/金志堅_\(皇妹\).md "wikilink")（1921年生，嫁[喬宏治](../Page/喬宏治.md "wikilink")）。

## 生平

溥傑幼年在醇王府作溥仪伴读。1924年，与满族女子唐怡莹成婚。1928年，北伐军进入北京前夕，载沣全家由[张学良安排](../Page/张学良.md "wikilink")，赴天津租界避难，溥杰夫妇住在张学良小妾谷瑞玉公馆中。溥杰秘密前往奉天入讲武堂求学，为溥仪阻止，通过日本警察扣留于大连，十余天后回到天津。

欲图复辟的溥仪另外安排溥杰到日本学习军事，并为其起名金秉藩，於1929年3月，与[润麒一同到](../Page/润麒.md "wikilink")[日本](../Page/日本.md "wikilink")[东京](../Page/东京.md "wikilink")[学习院](../Page/学习院.md "wikilink")、[日本陆军士官学校学习](../Page/日本陆军士官学校.md "wikilink")。1935年，溥杰回到[满洲国](../Page/满洲国.md "wikilink")。在满洲国任军职，任[滿洲國宮內府](../Page/滿洲國宮內府.md "wikilink")[侍从武官](../Page/侍从武官.md "wikilink")。

1937年4月，溥杰与日本华族女子[嵯峨浩成婚](../Page/嵯峨浩.md "wikilink")。数日后，哥哥溥仪新娶[譚玉齡](../Page/譚玉齡.md "wikilink")，与此前所娶[婉容](../Page/婉容.md "wikilink")、[文绣一样皆无生育子女](../Page/文绣.md "wikilink")。在溥仪长期无子的状态下，溥杰与嵯峨浩“未来的儿子”被认为是溥仪的接替者。同年，[满洲国国务院通过](../Page/满洲国国务院.md "wikilink")《帝位继承法》，规定：皇帝死后由子继之，如无子则由孙继之，如无子无孙则由弟继之，如无弟则由弟之子继之。从法律上，为溥杰继承帝位铺平道路。另有说法则认为，日方并未承认溥杰“[帝族](../Page/皇族.md "wikilink")”身份，他无权继承满洲国帝位。根据1937年溥仪与[日本关东军司令签署的备忘录规定](../Page/关东军司令部.md "wikilink")，满洲国皇位继承人由[日本天皇选定](../Page/日本天皇.md "wikilink")，溥仪无权干涉\[1\]。

1943年，溥杰入[日本陸軍大學學習](../Page/日本陸軍大學.md "wikilink")，畢業後回[满洲国](../Page/满洲国.md "wikilink")。1945年8月[二次世界大戰結束](../Page/二次世界大戰.md "wikilink")，日本投降，滿洲國滅亡。溥傑與哥哥溥儀逃至[瀋陽打算改乘飛機前往日本](../Page/瀋陽.md "wikilink")，在瀋陽機場被[蘇聯軍隊俘獲](../Page/蘇聯.md "wikilink")，在蘇聯被拘留了5年。1950年8月被移交给[中華人民共和國政府羁押](../Page/中華人民共和國政府.md "wikilink")，關押在[撫順戰犯管理所](../Page/撫順戰犯管理所.md "wikilink")，接受[中國共產黨的思想改造](../Page/中國共產黨.md "wikilink")，到1960年11月获特赦释放（11月19日签署特赦令，11月28日执行）。之後與妻子浩重逢，並度過[文化大革命](../Page/文化大革命.md "wikilink")，但在文革期間曾被紅衛兵闖入家中而讓他和妻子浩受到不小驚嚇，妻子浩從日本帶到中國的照相機也被紅衛兵給偷走。

1980年代後任[全國人民代表大會常務委員會委員](../Page/全國人民代表大會常務委員會.md "wikilink")、[全國人大民族委員會副主任委員](../Page/全國人大.md "wikilink")。1991年11月28日獲日本立命館大學頒發名譽法學博士學位。

1994年2月28日7时55分因病于北京逝世，享年87歲。死后骨灰一半葬于日本山口县下关市（嵯峨家的神社）的爱新觉罗分社内，另一半葬于北京。

## 家庭

### 妻子、女儿

[Aisin-Gioro_Pǔjié_and_Lady_Hiro_Saga.jpg](https://zh.wikipedia.org/wiki/File:Aisin-Gioro_Pǔjié_and_Lady_Hiro_Saga.jpg "fig:Aisin-Gioro_Pǔjié_and_Lady_Hiro_Saga.jpg")

溥傑的第一任妻子[唐怡莹](../Page/唐怡莹.md "wikilink")（唐石霞，1904年－1993年），姓[他他拉氏](../Page/他他拉氏.md "wikilink")，字怡莹，系[珍妃和](../Page/珍妃.md "wikilink")[瑾妃的哥哥志錡之女](../Page/瑾妃.md "wikilink")。1924年1月12日溥傑17岁时与唐怡莹结婚，婚后夫妇关系不睦。1926年唐怡莹成为[张学良的情妇](../Page/张学良.md "wikilink")，并曾推动溥杰前往[奉天讲武堂求学](../Page/奉天讲武堂.md "wikilink")，此后与溥杰家人的关系破裂。溥杰在日本读书期间，唐怡莹又成为[卢永祥之子](../Page/卢永祥.md "wikilink")[卢筱嘉的情妇](../Page/卢筱嘉.md "wikilink")，并将[北平](../Page/北平.md "wikilink")[醇亲王府的大批财物盗走](../Page/醇亲王.md "wikilink")。随后两人分居，陷入长期离婚争议中。离婚后成为畫家。1949年迁居香港，任教於香港大学东方语言学校。外甥即伊爾根覺羅耆齡之孫惠伊深，其母唐梅是唐石霞姐姐。

1935年溥傑从日本回国后，溥仪曾为其物色了一满族女子王敏彤，但最终未能成婚。

溥傑的第二任妻子[浩](../Page/愛新覺羅浩.md "wikilink")（1914年－1987年），是日军准备为其物色的日本[貴族妻子](../Page/貴族.md "wikilink")，因为溥仪无后，日方希望由一个有日本血统的人即位。嵯峨浩为日本[华族侯爵](../Page/华族.md "wikilink")的长女，1937年4月3日两人在东京结婚（婚后名**爱新觉罗·浩**）。虽被认为是[權宜婚姻](../Page/權宜婚姻.md "wikilink")，但两人婚后幸福圓滿。

兩人誕下兩名女兒，長女[慧生](../Page/慧生.md "wikilink")（1938-1957）、次女[嫮生](../Page/嫮生.md "wikilink")（1940-，其夫福永健治，婚后名福永嫮生）。

1957年12月10日，長女慧生在日本[伊豆半島](../Page/伊豆半島.md "wikilink")[天城山遭槍擊身亡](../Page/天城山.md "wikilink")，現場尚有另一名男大學生陳屍，因此有殉情與遭男方情殺兩種說法，為當時喧騰一時的刑事案件([天城山心中](http://ja.wikipedia.org/wiki/%E5%A4%A9%E5%9F%8E%E5%B1%B1%E5%BF%83%E4%B8%AD))。

次女嫮生現居於日本，並育有五名子女。

## 著作

溥杰擅书法，传世有《溥杰诗词选》。中国文史出版社1994年4月出版有叶祖孚执笔的《溥杰自传》。

## 相关影视作品

2003年，日本[朝日電視台為紀念開播](../Page/朝日電視台.md "wikilink")45週年，拍攝特製的[電視連續劇](../Page/電視連續劇.md "wikilink")《[-{zh-hans:流转的王妃·最后的皇弟;zh-hk:溥傑與王妃;zh-tw:流轉的王妃·最後的皇弟;}-](../Page/流轉的王妃·最後的皇弟.md "wikilink")》，把戰事當作背景，描繪溥傑與夫人之間的感情。溥傑、浩分別由[竹野內豐及](../Page/竹野內豐.md "wikilink")[常盤貴子飾演](../Page/常盤貴子.md "wikilink")。

## 注释

## 参考文献

### 引用

### 书籍

  - 溥杰、叶祖孚：《溥杰自传》，中国文史出版社，2001年
  - 唐德刚、王书君：《张学良世纪传奇口述实录》，山东友谊出版社，2002年
  - 贾英华：《末代皇弟溥杰传》，作家出版社，2002年

## 参见

  - [嵯峨浩](../Page/嵯峨浩.md "wikilink")
  - [溥儀](../Page/溥儀.md "wikilink")
  - [金友之](../Page/金友之.md "wikilink")

{{-}}

[Category:中華人民共和國書法家](../Category/中華人民共和國書法家.md "wikilink")
[Category:第八屆全國人大代表](../Category/第八屆全國人大代表.md "wikilink")
[Category:第七屆全國人大代表](../Category/第七屆全國人大代表.md "wikilink")
[Category:第六屆全國人大代表](../Category/第六屆全國人大代表.md "wikilink")
[Category:第五屆全國人大代表](../Category/第五屆全國人大代表.md "wikilink")
[Category:遼寧省全國人民代表大會代表](../Category/遼寧省全國人民代表大會代表.md "wikilink")
[Category:滿族全國人大代表](../Category/滿族全國人大代表.md "wikilink")
[Category:第四屆全國政協委員](../Category/第四屆全國政協委員.md "wikilink")
[Category:滿族全國政協委員](../Category/滿族全國政協委員.md "wikilink")
[Category:清朝宗室](../Category/清朝宗室.md "wikilink")
[Category:满洲国中央政府官员](../Category/满洲国中央政府官员.md "wikilink")
[Category:日本陆军士官学校校友](../Category/日本陆军士官学校校友.md "wikilink")
[Category:戰俘](../Category/戰俘.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[P](../Category/愛新覺羅氏.md "wikilink")

1.