**鄭汝樺**（****，），前[香港](../Page/香港.md "wikilink")[運輸及房屋局局長兼任](../Page/運輸及房屋局局長.md "wikilink")[香港房屋委員會](../Page/香港房屋委員會.md "wikilink")[主席](../Page/主席.md "wikilink")。

## 簡歷

鄭汝樺先後畢業於[拔萃女書院及](../Page/拔萃女書院.md "wikilink")[香港大學](../Page/香港大學.md "wikilink")，1983年8月起加入[香港政府](../Page/香港政府.md "wikilink")[政務職系](../Page/政務職系.md "wikilink")，曾經於多個[香港政府部門工作](../Page/香港政府部門.md "wikilink")，包括：前[經濟事務科](../Page/經濟事務科.md "wikilink")、[懲教署](../Page/懲教署.md "wikilink")、前[公務員薪俸及服務條件常務委員會秘書處](../Page/公務員薪俸及服務條件常務委員會秘書處.md "wikilink")、前[教育統籌科](../Page/教育統籌科.md "wikilink")、前[運輸科及前](../Page/運輸科.md "wikilink")[財政司辦公室](../Page/財政司辦公室.md "wikilink")。她於2007年4月晉升至首長級甲一級政務官。

鄭汝樺所任的重要職位包括：

  - 1996年4月至1997年4月：[中央政策組副首席顧問](../Page/中央政策組.md "wikilink")
  - 1997年4月至1997年8月：[副行政署長](../Page/行政署.md "wikilink")
  - 1997年8月至1998年12月：[中央政策組副首席顧問](../Page/中央政策組.md "wikilink")
  - 1998年12月至2003年3月：[資訊科技及廣播局副局長](../Page/資訊科技及廣播局.md "wikilink")（2002年[高官問責制實行後改稱](../Page/高官問責制.md "wikilink")[工商及科技局副秘書長](../Page/工商及科技局.md "wikilink")）
  - 2003年3月至2006年3月：[旅遊事務專員](../Page/旅遊事務專員.md "wikilink")
  - 2006年4月至2007年6月30日：[經濟發展及勞工局](../Page/經濟發展及勞工局.md "wikilink")[常任秘書長](../Page/常任秘書長.md "wikilink")（經濟發展）
  - 2007年7月1日至2012年7月1日：[運輸及房屋局局長](../Page/運輸及房屋局局長.md "wikilink")
  - 2014年10月31日至現在:
    [中國銀行（香港）](../Page/中國銀行（香港）.md "wikilink")[獨立非執行董事](../Page/獨立董事.md "wikilink")\[1\]

## 爭議

公職生涯中，鄭汝樺最批評的她在運輸及房屋局局長任內推行[高鐵的言行](../Page/香港高鐵爭議.md "wikilink")。她後來拒絕出席檢討[香港高鐵超支延誤事件的](../Page/香港高鐵超支延誤事件.md "wikilink")[立法會專責委員會聆訊](../Page/香港立法會.md "wikilink")，令委員會對她表示失望。\[2\]

## 家庭

鄭汝樺與商人丈夫[吳楚華育有兩名女兒](../Page/吳楚華.md "wikilink")。

## 注释

## 參考資料

  - [政府公布主要官員名單](http://www.info.gov.hk/gia/general/200706/23/P200706230082.htm)，香港政府新聞公報，2007年6月23日
  - 《[舊日的足跡--鄭汝樺訪問](http://programme.rthk.org.hk/channel/radio/player_popup.php?pid=1144&eid=&d=2010-05-09&player=media&type=archive&channel=radio1)》(香港電台第一台，2010年5月9日)

[Category:中国银行人物](../Category/中国银行人物.md "wikilink")
[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Ru汝樺](../Category/鄭姓.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:拔萃女書院校友](../Category/拔萃女書院校友.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")

1.
2.