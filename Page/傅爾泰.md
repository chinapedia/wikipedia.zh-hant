**傅爾泰**，[中國](../Page/中國.md "wikilink")[清朝官員](../Page/清朝.md "wikilink")，滿洲正白旗人。曾任[台灣縣知縣](../Page/台灣縣.md "wikilink")。

## 履歷

傅爾泰為[監生出身](../Page/監生.md "wikilink")。[乾隆十九年](../Page/乾隆.md "wikilink")（1754年）擔任[台灣府海防補盜同知](../Page/台灣府海防補盜同知.md "wikilink")，次年七月攝[彰化縣知縣](../Page/彰化縣.md "wikilink")。同年年底，回任海防補盜同知。乾隆二十一年（1756年）以同知攝理[台灣縣知縣](../Page/台灣縣.md "wikilink")。乾隆二十四年（1759年）接替[夏昌任](../Page/夏昌.md "wikilink")[延平府知府一职](../Page/延平府.md "wikilink")，乾隆二十五年（1760年）由[于从濂接任](../Page/于从濂.md "wikilink")。

## 紀念

今台灣台南仍有「海防分府傅大老爺榮陞據思碑」之石碑古蹟留存。\[1\]

## 參考文獻

  -
  -
{{-}}

[Category:清朝彰化縣知縣](../Category/清朝彰化縣知縣.md "wikilink")
[Category:清朝臺灣縣知縣](../Category/清朝臺灣縣知縣.md "wikilink")
[Category:台灣府海防補盜同知](../Category/台灣府海防補盜同知.md "wikilink")
[Category:清朝延平府知府](../Category/清朝延平府知府.md "wikilink")
[Category:滿洲正白旗人](../Category/滿洲正白旗人.md "wikilink")

1.  [海防分府傅大老爺榮陞去思碑](http://memory.ncl.edu.tw/tm_new/subject/stela/029_2.htm)