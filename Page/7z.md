在[计算机科学中](../Page/计算机科学.md "wikilink")，**7z**是一种可以使用多种压缩[算法进行](../Page/算法.md "wikilink")[数据压缩的档案格式](../Page/数据压缩.md "wikilink")。该格式最初被[7-Zip实现并采用](../Page/7-Zip.md "wikilink")，但是这种档案格式是公有的，并且7-Zip软件本身亦在[GNU寬通用公共許可證
(GNU
LGPL)协议下开放源代码](../Page/GNU寬通用公共許可證.md "wikilink")。目前[LZMA](../Page/LZMA.md "wikilink")[软件开发工具包的最新版本為v](../Page/软件开发工具包.md "wikilink")15.12。

7z格式的[MIME类型为](../Page/MIME.md "wikilink")`application/x-7z-compressed`.

## 特色与高性能

7z格式的主要特色有：

  - [开源且](../Page/开源.md "wikilink")[模块化的组件结构](../Page/模块化.md "wikilink")（允许使用任何压缩，转换或加密算法）
  - 高压缩比率（使用不同的压缩演算法會有不同的結果）
  - 使用[AES](../Page/高级加密标准.md "wikilink")-256來[加密](../Page/加密.md "wikilink")
  - 支持超大文件（最大支持到16[EB](../Page/exabyte.md "wikilink")）
  - [Unicode文件名支持](../Page/Unicode.md "wikilink")
  - 支持[固实压缩](../Page/固实压缩.md "wikilink")，容許內類的檔案在用一個串流中壓縮，使類似的內容被有效的壓縮。
  - 壓縮檔檔[头壓縮](../Page/信头.md "wikilink")
  - 支援多執行緒壓縮

### 加密

7z格式支持256位键钥[AES算法](../Page/AES.md "wikilink")[加密](../Page/加密.md "wikilink")。键钥则由用户提供的[暗码进行](../Page/暗码.md "wikilink")[SHA-256](../Page/SHA.md "wikilink")[hash算法得到](../Page/hash函数.md "wikilink")（使用大量[迭代以使得对暗码的](../Page/迭代.md "wikilink")[暴力解码更加困难](../Page/暴力破解法.md "wikilink")）.

## 压缩

该格式的开發结构允许添加标准以外的压缩算法。

现在支持以下算法：

  - [LZMA](../Page/LZMA.md "wikilink") -
    改良和优化算法后的[LZMA最新版本](../Page/LZMA_\(算法\).md "wikilink")，使用[马尔可夫链](../Page/马尔可夫链.md "wikilink")／[熵信息编码和](../Page/熵信息编码.md "wikilink")[Patricia
    trie](../Page/Patricia_trie.md "wikilink")。
  - [LZMA2](../Page/LZMA2.md "wikilink") - 經過改良後的LZMA算法，支援更多 CPU 執行緒。
  - [PPMD](../Page/PPMD.md "wikilink") - 基于Dmitry Shkarin之上的算法2002
    PPMdH（PPMII/cPPMII）并加以优化：PPMII是1984年[PPM压缩算法](../Page/PPM压缩算法.md "wikilink")（局部匹配思想是开创）的进阶版本。
  - [BCJ](../Page/BCJ.md "wikilink") -
    32位x86可执行文件转换程序，参见[LZMA](../Page/LZMA.md "wikilink")。对短程jump操作和调用操作的目标地址进行压缩。
  - [BCJ2](../Page/BCJ2.md "wikilink") -
    32位x86可执行文件转换程序，参见[LZMA](../Page/LZMA.md "wikilink")。对jump操作，调用操作和有条件jump操作的目标地址进行单独压缩。
  - [Bzip2](../Page/Bzip2.md "wikilink") -
    标准[BWT算法](../Page/BWT转换.md "wikilink")。Bzip2使用（更快的）[哈夫曼编码和](../Page/哈夫曼编码.md "wikilink")（更强的）熵信息编码。
  - [DEFLATE](../Page/DEFLATE.md "wikilink") - 标准LZ77-based算法。

## 算法的实现

下列压缩软件支持7z文件格式：

  - [7-Zip和](../Page/7-Zip.md "wikilink")[p7zip](../Page/p7zip.md "wikilink")
  - [IZArc](../Page/IZArc.md "wikilink")
  - [PowerArchiver](../Page/PowerArchiver.md "wikilink")
  - [QuickZip](../Page/QuickZip.md "wikilink")
  - [Squeez](../Page/Squeez.md "wikilink")
  - [TUGZip](../Page/TUGZip.md "wikilink")
  - [WinRAR](../Page/WinRAR.md "wikilink")
  - [ZipGenius](../Page/ZipGenius.md "wikilink")
  - [EZ 7z](../Page/EZ_7z.md "wikilink")
  - [Bandizip](../Page/Bandizip.md "wikilink")

## 参见

  - [压缩格式列表](../Page/压缩格式列表.md "wikilink")
  - [自由檔案格式](../Page/自由檔案格式.md "wikilink")

## 參考來源

<references/>

## 外部链接

  - [7z Format](http://www.7-zip.org/7z.html) — 关于7Z格式压缩文件的说明
  - [7-Zip](http://www.7-zip.org/) — the original [file
    archiver](../Page/file_archiver.md "wikilink") for [Microsoft
    Windows](../Page/Microsoft_Windows.md "wikilink")
  - [p7zip](http://p7zip.sourceforge.net/) — a
    [port](../Page/Porting.md "wikilink") of 7-Zip for
    [Unix-like](../Page/Unix-like.md "wikilink") systems
  - [7zX](http://sixtyfive.xmghosting.com/)（[Mac OS
    X](../Page/Mac_OS_X.md "wikilink")）
  - [Dimtry Shkarin (Institute for Dynamics of Geospheres, Moscow,
    Russia) 2002: PPM: One Step to
    Practicality](https://web.archive.org/web/20051210140547/http://datacompression.info/Miscellaneous/PPMII_DCC02.pdf)

[Category:归档格式](../Category/归档格式.md "wikilink")
[Category:无损压缩算法](../Category/无损压缩算法.md "wikilink")
[Category:俄羅斯發明](../Category/俄羅斯發明.md "wikilink")