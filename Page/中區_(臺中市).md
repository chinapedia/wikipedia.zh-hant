**中區**為[臺灣](../Page/台灣.md "wikilink")[臺中市的一個](../Page/臺中市.md "wikilink")[市轄區](../Page/市轄區.md "wikilink")。是臺中市進入現代化城市的主要市區區域（[市中心](../Page/市中心.md "wikilink")），曾是[中心商業區](../Page/中心商業區.md "wikilink")，全區除公共設施用地外，皆為商業用地\[1\]，然而[1980年代後隨著台灣的公路興起](../Page/1980年代.md "wikilink")（[中山高速公路通車](../Page/中山高速公路.md "wikilink")），臺中市的交通重心開始西移，並且市府開始於北屯、西屯、南屯進行[市地重劃](../Page/臺中市市地重劃區.md "wikilink")，使工商業重心也逐漸西移，再加上中區內道路窄小、停車場地缺乏等因素，導致舊市區市況嚴重[衰退](../Page/都市衰退.md "wikilink")\[2\]，中心商業區也移轉至臺中西部的[七期重劃區](../Page/七期重劃區.md "wikilink")。直至2010年代後，在民間企業與文化復興組織一連串的努力下，使人潮逐漸回流，市況亦開始慢慢復興\[3\]。然而中區仍為臺中市及[中部地區東西向主要道路的起點](../Page/中臺灣.md "wikilink")，在交通上尚具重要地位。

中區曾是全台灣人口密度最高的區域，曾高達每平方公里49529人以上(1986年)，但因人口持續衰退，2017年被[北區超越居台中人口密度第二高的市轄區](../Page/北區_\(臺中市\).md "wikilink")。是該市面積最小的市轄區，亦為全國面積最小、唯一一個面積不滿1平方公里的鄉鎮層級行政區\[4\]。

## 歷史

**中區**位於今日的[柳川與](../Page/柳川.md "wikilink")[綠川之間](../Page/綠川.md "wikilink")，早期尚未開發時，此地區到處都是沼澤地，其中有一座小丘陵地，小丘陵地上高凸處叫做「墩」，[清朝時在高凸處設置](../Page/清朝.md "wikilink")[煙墩](../Page/煙墩.md "wikilink")，於是就把小丘陵地稱為「**大墩**」（今[臺中公園砲臺山](../Page/臺中公園.md "wikilink")），鄰近的市街因此稱為「大墩街」，為臺中市區的起源。但實際上中區的大墩是指「東大墩」，因為在西屯另有相對於本地的「[西大墩](../Page/西大墩遺址.md "wikilink")」

[日治時期](../Page/台灣日治時期.md "wikilink")1895年，[臺灣民政支部長](../Page/台灣民政支部長.md "wikilink")[兒玉利國建議將當時臺中市規劃為](../Page/兒玉利國.md "wikilink")「圓形放射狀」都市，但未被[臺灣總督府採納](../Page/臺灣總督府.md "wikilink")。隔年，總督府在準備實施的[市區改正計畫中](../Page/市區改正.md "wikilink")，[民政長官](../Page/民政長官.md "wikilink")[後藤新平採用了](../Page/後藤新平.md "wikilink")[巴爾頓](../Page/巴爾頓.md "wikilink")（W.K
Barton）與[濱野彌四郎的](../Page/濱野彌四郎.md "wikilink")「臺中市街區劃設計報告書」，將臺中市規劃為「棋盤狀」都市。1900年正式公告「臺中市區改正」，為配合整體都市發展也設置了公園預定地，但此地後來被鐵路車站用地取代，也就是[臺中車站現址](../Page/臺中車站.md "wikilink")，而公園預定地為[臺中公園現址](../Page/臺中公園.md "wikilink")。因而整治綠、柳川與開闢棋盤狀道路、興建臺中驛（今臺中車站），將臺中興建成為臺灣新興的現代化都市。

[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，[中華民國接收](../Page/中華民國.md "wikilink")[日本](../Page/日本帝國.md "wikilink")[臺灣總督府所轄之區域](../Page/台灣總督府.md "wikilink")，將原[臺中州下](../Page/臺中州.md "wikilink")[臺中市的橘町](../Page/臺中市_\(州轄市\).md "wikilink")、綠川町、榮町、大正町、寶町、錦町、新富町、柳町、初音町、若松町，合併成立「中區」。

\-{庄}- Taiwan formosa vintage history cities taichung
taipics098.jpg|臺中新盛橋通，今臺中市中山路橫跨綠川的中山綠橋 Taiwan formosa vintage
history cities taichung taipics009.jpg|臺中干城橋通，今成功路 Taiwan formosa
vintage history cities taichung taipics085.jpg|臺中大正橋，今臺中市民權路

### 舊地名

[中區區公所.JPG](https://zh.wikipedia.org/wiki/File:中區區公所.JPG "fig:中區區公所.JPG")中區分館、[市警局第一分局大誠分駐所](../Page/臺中市政府警察局.md "wikilink")\]\]

  - **馬寮**－這是[臺中建府初期出現的地名](../Page/臺灣省城.md "wikilink")，還稱為「馬寮」的時候，一分地賣不到一塊大洋，而現在一坪地大約要值兩三百萬元。它就在[中正路與](../Page/台中港路.md "wikilink")[繼光街口](../Page/繼光街.md "wikilink")，該地曾有一家[吉本洋品店](../Page/吉本洋品店.md "wikilink")，是臺中市第一家最具規模的百貨公司。

<!-- end list -->

  - **鳥竹圍**－現在中華路一段一帶，這個地方以前有很多大樹，成千的白鷺鷥經常在這裡聚集，家家戶戶的屋簷下，也有大量的燕子築巢棲息，因此而得名。由於第八市場是用竹子搭建而成，所以大家都叫它為「**竹廣（管）市仔**」，而「鳥竹圍」的舊名便漸漸被人遺忘。

### 市區沒落

中區曾是臺中的[中心商業區](../Page/中心商業區.md "wikilink")，但因台中市公園路、光復路、
中正路皆為日治時期所規劃，路面狹窄，且隨著[七期發展與百貨公司林立以及一中街](../Page/臺中市第七期市地重劃區.md "wikilink")、逢甲商圈的興起，商機趨於
沒落。

自從中山高速公路通車後，近十年台中市發展就一直往西走，而西南屯有高速公路、快速道路及新市政特區，因此取代中區成為台中的中心商業區。

### 活化再興

2010年代，隨著宮原眼科及原第二信用合作社建物經[台中在地餐飲集團活化經營](../Page/日出乳酪.md "wikilink")，開始帶動人潮回流；其後，亦有其它業者跟進接手翻新周邊建物作旅館或其他用途。臺中市政府及民間團體亦積極推動中區周圍之再興計畫。\[5\]

## 人口

## 行政區劃

共有八個里，包括：
[大誠里](../Page/大誠里_\(臺中市中區\).md "wikilink")、[中華里](../Page/中華里_\(臺中市\).md "wikilink")、[光復里](../Page/光復里_\(臺中市\).md "wikilink")、[綠川里](../Page/綠川里_\(臺中市\).md "wikilink")、大墩里、[公園里](../Page/公園里_\(臺中市\).md "wikilink")、柳川里、繼光里　　

<table>
<thead>
<tr class="header">
<th><p>里名</p></th>
<th><p>面積<br />
km2</p></th>
<th><p>鄰數</p></th>
<th><p>戶數</p></th>
<th><p>人口<br />
2017年10月</p></th>
<th><p>人口密度<br />
人/km2</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/大誠里_(臺中市中區).md" title="wikilink">大誠里</a></p></td>
<td></td>
<td><p>26</p></td>
<td><p>1237</p></td>
<td><p>2886</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>大墩里</p></td>
<td></td>
<td><p>26</p></td>
<td><p>1018</p></td>
<td><p>2758</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中華里_(臺中市).md" title="wikilink">中華里</a></p></td>
<td></td>
<td><p>27</p></td>
<td><p>1098</p></td>
<td><p>2326</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公園里_(臺中市).md" title="wikilink">公園里</a></p></td>
<td></td>
<td><p>21</p></td>
<td><p>775</p></td>
<td><p>1795</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/光復里_(臺中市).md" title="wikilink">光復里</a></p></td>
<td></td>
<td><p>31</p></td>
<td><p>1028</p></td>
<td><p>2523</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>柳川里</p></td>
<td></td>
<td><p>25</p></td>
<td><p>1065</p></td>
<td><p>2203</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/綠川里_(臺中市).md" title="wikilink">綠川里</a></p></td>
<td></td>
<td><p>19</p></td>
<td><p>878</p></td>
<td><p>2078</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>繼光里</p></td>
<td></td>
<td><p>20</p></td>
<td><p>817</p></td>
<td><p>1903</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>總計</p></td>
<td><p>0.8803</p></td>
<td><p>195</p></td>
<td><p>7916</p></td>
<td><p>18472</p></td>
<td><p>20984</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 觀光旅遊

[Taichung_Station_1.JPG](https://zh.wikipedia.org/wiki/File:Taichung_Station_1.JPG "fig:Taichung_Station_1.JPG")
[台中第二市場.jpg](https://zh.wikipedia.org/wiki/File:台中第二市場.jpg "fig:台中第二市場.jpg")
中區自日治時期市區改正以後，擁有許多日治時期的建築，1990年代以前亦曾為臺中市的中心商業區。

### 文化資產

此區目前依[文資法被登錄的有一個](../Page/文資法.md "wikilink")[國定古蹟](../Page/國定古蹟.md "wikilink")（[原臺中車站](../Page/舊臺中車站.md "wikilink")）、一個直轄市定古蹟（[彰化銀行舊總行](../Page/彰化銀行#文化資產.md "wikilink")）、三個[歷史建築](../Page/臺灣歷史建築.md "wikilink")（[臺中州立圖書館](../Page/臺中州立圖書館.md "wikilink")、[柳原教會](../Page/柳原教會.md "wikilink")、[新盛橋](../Page/新盛橋.md "wikilink")、[中區第一任街長宅第](../Page/中區第一任街長宅第.md "wikilink")）。

### 商圈市集

  - [臺中火車站商圈](../Page/台中火車站商圈.md "wikilink")
      - [繼光街徒步區](../Page/繼光街.md "wikilink")
      - [電子街徒步區](../Page/電子街.md "wikilink")
  - [中華路夜市](../Page/中華路夜市.md "wikilink")
  - 自由路糕餅商圈
  - [第二市場](../Page/第二市場.md "wikilink")

### 商場與戲院

中區於全盛期曾為臺灣[百貨公司密集度最高之區域](../Page/百貨公司.md "wikilink")，但目前多數已歇業。\[6\]

  - 營業中

<!-- end list -->

  - [東協廣場](../Page/東協廣場.md "wikilink")
  - 日曜天地OUTLET
  - 龍心商場（原[龍心百貨](../Page/龍心百貨.md "wikilink")、[北屋百貨](../Page/北屋百貨.md "wikilink")、[誠品龍心商場](../Page/誠品.md "wikilink")）
  - [萬代福影城](../Page/萬代福影城.md "wikilink")
  - 日新大戲院\[7\]

<!-- end list -->

  - 已歇業

<!-- end list -->

  - 金沙百貨（整修中，未來將改為[台中李方艾美酒店](../Page/台中李方艾美酒店.md "wikilink")）
  - [舊遠東百貨臺中店](../Page/遠東百貨.md "wikilink")（進行都市更新）\[8\]\[9\]
  - [千越百貨](../Page/千越大樓.md "wikilink")（進行都市更新）\[10\]
  - ATT百貨（今[錢櫃KTV臺中店](../Page/錢櫃KTV.md "wikilink")）
  - <S>青果合作大樓（原大大百貨、上上百貨、青春帝國百貨，綠川東街）
  - 臺中商業大樓（[財神百貨](../Page/財神百貨.md "wikilink")、竹舫百貨，二樓以上閒置中）
  - [永琦東急百貨臺中店](../Page/永琦百貨.md "wikilink")（曾為[臺中市政府第二辦公大樓](../Page/台中市政府.md "wikilink")，閒置中）\[11\]
  - 中英育樂大樓（成功路）
  - 1+1影城（自由路）\[12\]
  - 東海戲院（自由路）
  - 豐中戲院（成功路）
  - 森玉戲院（興民街，已拆除）</S>

### 其它景點

  - [日出乳酪](../Page/日出乳酪.md "wikilink")-[宮原眼科](../Page/宮原眼科.md "wikilink")
  - [日出乳酪](../Page/日出乳酪.md "wikilink")-[第四信用合作社](../Page/第四信用合作社.md "wikilink")
  - [柳原教會](../Page/柳原教會.md "wikilink")
  - [萬春宮](../Page/萬春宮.md "wikilink")
  - [輔順將軍廟](../Page/輔順將軍廟.md "wikilink")
  - [綠川景觀河岸](../Page/綠川.md "wikilink")
  - [柳川景觀河岸](../Page/柳川.md "wikilink")

<File:Taichung> Liu-Yuan Church by Taiwan Presbyterian Church.JPG|歷史建築 -
柳原教會 <File:Lyu> Chuan.jpg|綠川與新盛橋（整治前） <File:Taichung> Downtown Bus
terminal.JPG|[東協廣場旁](../Page/東協廣場.md "wikilink")
[File:黃外科醫院.JPG|繼光街徒步區](File:黃外科醫院.JPG%7C繼光街徒步區)

## 交通

### 鐵路

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [臺中線](../Page/臺中線.md "wikilink")：[臺中車站](../Page/臺中車站.md "wikilink")

### 公路

  - [TW_PHW12.svg](https://zh.wikipedia.org/wiki/File:TW_PHW12.svg "fig:TW_PHW12.svg")[臺12線](../Page/台12線.md "wikilink")：[臺灣大道一段](../Page/臺灣大道.md "wikilink")
  - [建國路](../Page/建國路_\(台中市\).md "wikilink")
  - [自由路](../Page/自由路_\(台中市區\).md "wikilink")
  - [三民路](../Page/三民路_\(台中市區\).md "wikilink")
  - [柳川東、西路](../Page/柳川東、西路.md "wikilink")
  - [中華路](../Page/中華路_\(台中市區\).md "wikilink")
  - [民權路](../Page/民權路_\(台中市\).md "wikilink")

### 公車系統

  - 臺灣大道幹線候車站：臺中火車站、仁愛醫院站

### 客運

  - [豐原客運臺中站](../Page/豐原客運.md "wikilink")
  - [仁友客運仁友東站](../Page/仁友客運.md "wikilink")
  - [東南客運臺中站](../Page/東南客運.md "wikilink")
  - [巨業交通臺中站](../Page/巨業交通.md "wikilink")
  - [南投客運臺中站](../Page/南投客運.md "wikilink")
  - [彰化客運臺中站](../Page/彰化客運.md "wikilink")

### 未來

#### 捷運

  - [臺中捷運](../Page/臺中捷運.md "wikilink")

<!-- end list -->

  - <font color=blue>█</font>[藍線](../Page/臺中捷運藍線.md "wikilink")（規劃中）\[13\]

## 教育

  - [臺中市中區光復國民小學](../Page/臺中市中區光復國民小學.md "wikilink")

<!-- end list -->

  - 公共圖書館

<!-- end list -->

  - [臺中市立圖書館中區分館](../Page/臺中市立圖書館.md "wikilink")

## 醫療

  - 醫療機構

<!-- end list -->

  - [澄清醫院平等院區](../Page/澄清醫院.md "wikilink")
  - [仁愛綜合醫院臺中院區](../Page/仁愛綜合醫院.md "wikilink")
  - [臺中市衛生局中西區衛生所](../Page/臺中市衛生局.md "wikilink")

## 重大建設

  - 柳川汙染整治及環境改善工程（2014年啟動，2016年完成）
  - 新盛綠川水岸廊道計畫[1](http://taichungwrs.github.io/)（2016年啟動，2018年完成）
  - 舊臺中車站鐵道文化園區（綠空鐵道軸線計畫，整建中）
  - 臺中車站周邊都市更新計畫（台中大車站計畫）
  - [台中李方艾美酒店](../Page/台中李方艾美酒店.md "wikilink")（原金沙百貨，2015年整建中）
  - [千越大樓](../Page/千越大樓.md "wikilink")（原[千越百貨](../Page/千越大樓.md "wikilink")，2016年啟動都更）
  - 遠東百貨大樓與綜合大樓（原遠東百貨，2017年啟動都更）

## 外部連結

  - [臺中市中區區公所](http://www.central.taichung.gov.tw/)
  - [中區再生基地 DRF Goodot
    Village](https://zh-tw.facebook.com/GoodotVillage/)
  - [中部共生青年組合](https://www.facebook.com/taichung228/)

## 參考

[市區](../Category/臺中市行政區劃.md "wikilink")
[Category:台灣行政區劃之最](../Category/台灣行政區劃之最.md "wikilink")
[\*](../Category/中區_\(臺中市\).md "wikilink")

1.  [全國土地使用分區資料查詢系統](http://luz.tcd.gov.tw/WEB/)

2.

3.

4.  [誰從無止盡的重劃中獲利？](http://www.cw.com.tw/article/article.action?id=5000133)

5.

6.  趙于婷.林宥汝.[台中市中區的興起與沒落.國立台中女中.](http://www.shs.edu.tw/works/essay/2009/03/2009033110573340.pdf)2009

7.

8.

9.

10.
11.

12.

13.