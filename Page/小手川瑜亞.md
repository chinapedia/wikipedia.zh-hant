**小手川瑜亞**（1月12日－）是一位女性[日本漫畫家](../Page/日本漫畫家.md "wikilink")，出身於[熊本縣](../Page/熊本縣.md "wikilink")[熊本市](../Page/熊本市.md "wikilink")。摩羯座。血型O型。代表作有《[酷哥辣妹搜查網](../Page/酷哥辣妹搜查網.md "wikilink")》及《[死刑囚042](../Page/死刑囚042.md "wikilink")》等。

## 簡介

於1995年9月的5號[週刊Young
Jump增刊・漫革初次刊載](../Page/週刊Young_Jump.md "wikilink")《酷哥辣妹搜查網》。於1996年33號[週刊Young
Jump正式開始連載](../Page/週刊Young_Jump.md "wikilink")，其後轉載到別冊Young
Jump，並於2000年8月的15號完結。

於2000年9月號[月刊少年Ace開始連載](../Page/月刊少年Ace.md "wikilink")《[Anne
Freaks](../Page/Anne_Freaks.md "wikilink")》，同期於「週刊Young
Jump」開始每月連載《[ARCANA\~神秘事件簿～](../Page/ARCANA~神秘事件簿～.md "wikilink")》。其後於2002年於週刊Young
Jump連載《死刑囚042》並於2004年完結。

《酷哥辣妹搜查網》到現時也很受歡迎，是她初次亮相的代表作，而《死刑囚042》是通過死刑犯來繪畫生與死，成為她最新的代表作品。

現在於[SUPER
JUMP連載](../Page/SUPER_JUMP.md "wikilink")[和歌歌手](../Page/和歌.md "wikilink")[枡野浩一的處女小說](../Page/枡野浩一.md "wikilink")《短歌（Short
Song》》。

與[漫畫家](../Page/漫畫家.md "wikilink")[仙道滿壽美](../Page/仙道滿壽美.md "wikilink")、是朋友，於《酷哥辣妹搜查網》及《ARCANA\~神秘事件簿》的書尾中繪畫[四格漫畫](../Page/四格漫畫.md "wikilink")。

## 作品列表

  - [酷哥辣妹搜查網](../Page/酷哥辣妹搜查網.md "wikilink")（おっとり捜査）
  - Silent
  - [Anne Freaks](../Page/Anne_Freaks.md "wikilink")（Anne・Freaks）
  - [ARCANA\~神秘事件簿～](../Page/ARCANA~神秘事件簿～.md "wikilink")（ARCANA）
  - SUSPECTS （Young Jump漫革 Vol.262002年5月1日增刊，未收錄於單行本）
  - [手機自殺預告](../Page/手機自殺預告.md "wikilink")（ライン）
  - [死刑囚042](../Page/死刑囚042.md "wikilink")（）
  - [短歌](../Page/短歌.md "wikilink")（ショートソング）
  - 君之刃（）
  - LICENSE ライセンス
  - 破壞的天才（[王牌大律師2的](../Page/王牌大律師#第二季.md "wikilink")[劇中劇](../Page/劇中劇.md "wikilink")）

## 外部連結

  - [小手川瑜亞的極樂監獄](http://www.kotegawayua.jp/)：**小手川瑜亞**的官方網站

[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:熊本縣出身人物](../Category/熊本縣出身人物.md "wikilink")