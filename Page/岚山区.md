**岚山区**是[中国](../Page/中国.md "wikilink")[山东省](../Page/山东省.md "wikilink")[日照市所辖的一个](../Page/日照市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。地处鲁东南，东临黄海。总面积为759平方千米，2004年人口为42万。
2004年9月国务院批准，设立港口新区岚山，既成为日照市的城市副中心，临港工业基地。
区政府驻地安东卫设立于明洪武17年（公园1384年），时与天津卫，威海卫，灵山卫齐名，四大名卫之一。

## 行政区划

岚山区辖：全区辖2个街道、6个乡，1个镇，共计9个乡镇级行政区，417个行政村。

街道：[岚山头街道](../Page/岚山头街道.md "wikilink")、[安东卫街道](../Page/安东卫街道.md "wikilink")

镇：[虎山镇](../Page/虎山镇_\(日照市\).md "wikilink")、[碑廓镇](../Page/碑廓镇.md "wikilink")、[黄墩镇](../Page/黄墩镇_\(日照市\).md "wikilink")、[中楼镇](../Page/中楼镇.md "wikilink")、[高兴镇](../Page/高兴镇_\(日照市\).md "wikilink")、[巨峰镇](../Page/巨峰镇.md "wikilink")

乡：[前三岛乡](../Page/前三岛乡.md "wikilink")。前三岛乡为边界未决地区，由江苏省连云港市连云区有效控制。

## 外部链接

  - [日照市岚山区政府网站](http://www.rzlanshan.gov.cn/)

## 参考文献

[岚山区](../Category/岚山区.md "wikilink") [区](../Category/日照区县.md "wikilink")
[日照](../Category/山东市辖区.md "wikilink")