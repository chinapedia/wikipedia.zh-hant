**泡芙**（Profiterole）是一种源自[法国的球形糕點](../Page/法国.md "wikilink")。常見的泡芙是蓬松张孔的面皮中包裹[鮮忌廉](../Page/鮮忌廉.md "wikilink")、[巧克力乃至](../Page/巧克力.md "wikilink")[冰淇淋](../Page/冰淇淋.md "wikilink")，里面包裹的材料是通过注射或者将面包顶部撕破后加进去的；後者的顶部往往需要被其他食材取代。在泡芙的包裹的上面还可以撒上[糖粉](../Page/糖粉.md "wikilink")、果冻、水果或者[巧克力](../Page/巧克力.md "wikilink")。而根據充填的餡料不同，泡芙在不同地方也有許多不同異稱。\[1\]\[2\]\[3\]\[4\]

## 歷史

英文中泡芙有*profiterole*、*prophitrole*、*profitrolle*、*profiterolle*等的異稱\[5\]，是借用[法語產生的名詞](../Page/法語.md "wikilink")，這些名字在1604年就已出現，但是原本它們所指涉的意思在英文、法文之中已經無從查考。不過泡芙的雛形是以一種曾搭配湯的鹹麵包出現，內餡是肉泥、[乳酪等等](../Page/乳酪.md "wikilink")\[6\]；十七世紀的食譜又把它稱作「Potage
de
profiteolles」。\[7\]一直到十九世紀，profiterole才被確定用來代表「泡芙」。\[8\]儘管可以確定泡芙最初源自法國，泡芙也是[直布羅陀代表性的美食之一](../Page/直布羅陀.md "wikilink")。\[9\]

而現今常見的「奶油泡芙」最遲在1851年的美國就已出現。\[10\]

## 參見

  - [馬卡龍](../Page/馬卡龍.md "wikilink")（）
  - [闪电泡芙](../Page/闪电泡芙.md "wikilink")
  - [Zakuzaku](../Page/Zakuzaku.md "wikilink")

## 參考資料

[Category:麵包](../Category/麵包.md "wikilink")
[Category:意大利食品](../Category/意大利食品.md "wikilink")
[Category:泡芙](../Category/泡芙.md "wikilink")
[Category:美國飲食](../Category/美國飲食.md "wikilink")
[Category:德國飲食](../Category/德國飲食.md "wikilink")

1.

2.

3.

4.

5.

6.  Prosper Montagné, *Larousse Gastronomique*, 1st edition, 1938,
    *s.v.*

7.  Alfred Franklin, *La vie privée d'autrefois. Arts et métiers, modes,
    mœurs, usages des Parisiens du XII<sup>e</sup> au XVIII<sup>e</sup>
    siècle: La Cuisine*, Paris 1888, quoting from François Pierre La
    Varenne, 1651

8.
9.

10. ["Revere House" restaurant, Boston, menu dated May 18, 1851:
    "Puddings and Pastry. ... Cream
    Puffs"](http://digitalgallery.nypl.org/nypldigital/dgkeysearchdetail.cfm?trg=1&strucID=269316&imageID=476896&total=23&num=0&parent_id=443920&word=&s=&notword=&d=&c=&f=&k=0&sScope=&sLevel=&sLabel=&lword=&lfield=&imgs=20&pos=3&snum=&e=w).
    Digitalgallery.nypl.org. Retrieved on 2011-06-15.