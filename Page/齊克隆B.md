[Zyklon_B_Container.jpg](https://zh.wikipedia.org/wiki/File:Zyklon_B_Container.jpg "fig:Zyklon_B_Container.jpg")
**齊克隆B**（Zyklon B或Cyclon
B），是以[氰化物為基的消毒](../Page/氰化物.md "wikilink")[熏蒸劑和殺蟲劑](../Page/熏蒸劑.md "wikilink")，在[第二次世界大戰期間被](../Page/第二次世界大戰.md "wikilink")[納粹德國用於執行](../Page/納粹德國.md "wikilink")[種族滅絕作戰](../Page/種族滅絕.md "wikilink")。\[1\]\[2\]

</blockquote>

其前身[齊克隆A](../Page/齊克隆A.md "wikilink")（****）系[氰基甲酸甲酯與](../Page/氰基甲酸甲酯.md "wikilink")10%[氯甲酸甲酯的混合物](../Page/氯甲酸甲酯.md "wikilink")，後被齊克隆B所取代。

## 歷史

齊克隆由德國化學家[弗里茨·哈伯](../Page/弗里茨·哈伯.md "wikilink")（Fritz
Haber）所發明，作為殺蟲劑使用。[第一次世界大戰期間由德國的害蟲防治技術委員會](../Page/第一次世界大戰.md "wikilink")（）製造，用來除[蝨](../Page/蝨.md "wikilink")。化学家布鲁诺·特施在汉堡发明了基于齊克隆的二代产品，称为齊克隆B。

1930年，经过股本转让，德意志害蟲防治公司（）改由德國的[IG法本持股](../Page/IG法本.md "wikilink")，并由幾家化工公司合資製造。经销公司则以[易北河分隔两区](../Page/易北河.md "wikilink")，以东由位于汉堡的Tesch
& Stabenow（Testa）公司经销，以西则由位于法兰克福的Heerdt-Linger GmbH （Heli）公司经销。

齊克隆B目前在[捷克的](../Page/捷克.md "wikilink")廠仍有生產，商品名為Uragan D2，用作殺蟲劑使用。

## 成份及包裝

[Zyklon_B_labels.jpg](https://zh.wikipedia.org/wiki/File:Zyklon_B_labels.jpg "fig:Zyklon_B_labels.jpg")、[東方總督轄區](../Page/東方總督轄區.md "wikilink")，以及丹麦、芬兰和挪威），右區印有德國害蟲防治公司的標誌及商標Zyklon。中間區塊印有「有毒氣體！」（Giftgas\!），[骷髏頭兩旁印有](../Page/骷髏頭.md "wikilink")「內含有氰化物！僅由受訓過的人員開啟及使用！」等警語。此標籤紙為[紐倫堡審判的證物之一](../Page/紐倫堡審判.md "wikilink")\]\]
齊克隆B的主要成份為[氰化氫](../Page/氰化氫.md "wikilink")，並混有[矽藻土做為安定劑](../Page/矽藻土.md "wikilink")，以及加入了警示用臭味劑，密封在鐵罐中，當打開鐵罐後，內含的氰化氫會飄散出來。

## 用於人類

[Lethal_gas_crystal_can_&_documents.jpg](https://zh.wikipedia.org/wiki/File:Lethal_gas_crystal_can_&_documents.jpg "fig:Lethal_gas_crystal_can_&_documents.jpg")
1929年時，齊克隆B被美國海關用在貨運車廂及墨西哥入境美國者的衣物滅蟲用。

納粹德國起初用其以消滅[斑疹傷寒的病媒害蟲](../Page/斑疹傷寒.md "wikilink")\[3\]\[4\]。1942年至1944年齊克隆B在德国的销量为729吨，其中56吨（占8%）销往集中营。[奧斯威辛集中營采购了](../Page/奧斯威辛集中營.md "wikilink")23.8吨，仅有6吨真正用于杀虫，其余则用于臭名昭著的毒气室来屠杀犹太人。

## 參閱

  - [氰化氫](../Page/氰化氫.md "wikilink")
  - [滅絕營](../Page/滅絕營.md "wikilink")

## 註釋

[Category:化学武器](../Category/化学武器.md "wikilink")
[Category:納粹德國](../Category/納粹德國.md "wikilink")
[Category:猶太人大屠殺](../Category/猶太人大屠殺.md "wikilink")
[Category:德国发明](../Category/德国发明.md "wikilink")

1.  Höss, pp. 321–322.
2.
3.  Höss, pp. 321–322.
4.