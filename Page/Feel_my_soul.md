**feel my
soul**是[日本唱作女](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")
[YUI](../Page/YUI.md "wikilink")，於2005年2月23日所推出的單曲碟。

## 收錄歌曲

1.  **feel my soul**
      -
        作詞・作曲：YUI　編曲：
        [富士電視台電視連續劇](../Page/富士電視台.md "wikilink")「[不愉快的基因](../Page/不愉快的基因.md "wikilink")」主題曲。作為出道曲一下子被選作[月9連續劇的主題曲](../Page/月9.md "wikilink")，成為了一時的話題。
2.  **Free Bird**
      -
        作詞・作曲：YUI　編曲：鈴木Daichi秀行
3.  **Why me**
      -
        作詞・作曲：YUI　編曲：鈴木Daichi秀行
        YUI初次創作的樂曲。
4.  **feel my soul\~Instrumental\~**

## 銷售紀錄

| 發行日        | 排行榜       | 最高位     | 總銷量 |
| ---------- | --------- | ------- | --- |
| 2005年2月23日 | Oricon 週榜 | 8       |     |
| Oricon 年榜  | 95        | 107,930 |     |

[Category:YUI歌曲](../Category/YUI歌曲.md "wikilink")
[Category:富士月九劇主題曲](../Category/富士月九劇主題曲.md "wikilink")
[Category:2005年單曲](../Category/2005年單曲.md "wikilink")