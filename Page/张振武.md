[Zhang_Zhenwu.jpg](https://zh.wikipedia.org/wiki/File:Zhang_Zhenwu.jpg "fig:Zhang_Zhenwu.jpg")

**张振武**（），原名**尧鑫**，字**春山**，又名**竹山**。[湖北省](../Page/湖北省.md "wikilink")[罗田县人](../Page/罗田县.md "wikilink")，居住在[竹山县](../Page/竹山县.md "wikilink")。[武昌起义首义者之一](../Page/武昌起义.md "wikilink")，被尊为**共和元勋**，和[孙武](../Page/孫武_\(民國\).md "wikilink")、[蒋翊武并称](../Page/蒋翊武.md "wikilink")**辛亥三武**。

## 生平

### 早年事迹

张振武籍贯是湖北罗田人，居住于竹山。1900年考入湖北师范学校，1904年自费到日本[早稻田大学学习法律政治](../Page/早稻田大学.md "wikilink")，并参加体育会操练军事。1905年加入[同盟会](../Page/同盟会.md "wikilink")。1907年毕业回国，于武昌黄鹤楼街小学任教，其间因宣传反清革命几乎被捕。1909年又参加共进会并负责财务。

### [辛亥革命](../Page/辛亥革命.md "wikilink")

[Zhang_Zhenwu_porcelain.jpg](https://zh.wikipedia.org/wiki/File:Zhang_Zhenwu_porcelain.jpg "fig:Zhang_Zhenwu_porcelain.jpg")1911年10月10日在[清政府四处搜捕革命党人的形势下](../Page/清朝.md "wikilink")，张振武四处联络党人，发动了起义，世称**武昌起义**。起义成功后，因为军事机关遭到破坏，将领非死即逃，革命军处于群龙无首的状态，张振武和其他军人共推[黎元洪为都督](../Page/黎元洪.md "wikilink")。但黎还在犹豫。此时张振武和其他党人合议处决黎元洪，后来虽然黎元洪接受了任命，却自此结下了仇怨。

后来[袁世凯的北洋军南下与武昌起义军作战](../Page/袁世凯.md "wikilink")，他通过印发宣传资料（《敬告我军人的白话文》）、亲自督战等方式，提高民心士气，并在[黄兴等人主张退出武汉助守南京的情况下](../Page/黄兴.md "wikilink")，力主坚守。后来黎元洪对战事失去信心出走，张又评论说：“黎某如此畏缩，不如乘此另举贤能。”这使得他和黎元洪的仇怨加深了。

### 被殺

[中华民国建立后](../Page/中华民国.md "wikilink")，1912年8月16日凌晨，他和[方维在北京被](../Page/方维.md "wikilink")[黎元洪](../Page/黎元洪.md "wikilink")、袁世凯以“贪污”罪名杀害。张振武案发后的第三天，国会参议院开会，主要意見认为，副总统黎元洪以非罪要求杀人，大总统袁世凯以命令擅改法律，均是违背约法。

## 参考文献

  - [竹山信息网](http://www.zhushan.gov.cn/bflm/lsmr/200509/2901.html)
  - 丁中江：《北洋军阀史话》
  - [张振武案中的真真假假](http://www.gmw.cn/content/2004-09/20/content_103873.htm)

[Category:被處決的中華民國人](../Category/被處決的中華民國人.md "wikilink")
[Z](../Category/早稻田大學校友.md "wikilink")
[Z](../Category/辛亥革命人物.md "wikilink")
[Z](../Category/羅田人.md "wikilink")
[Z振武](../Category/张姓.md "wikilink")