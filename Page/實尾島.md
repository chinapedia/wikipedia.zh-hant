\-{zh-hant:實尾島風雲;zh-hans:实尾岛风云;zh-hk:實尾島;zh-tw:實尾島風雲}-}}

**實尾島**（）是[黃海上位於](../Page/黃海.md "wikilink")[韓國西岸](../Page/韓國.md "wikilink")、[仁川廣域市轄內的一座無人居住的](../Page/仁川廣域市.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")，面積約為，在[仁川國際機場西南方約](../Page/仁川國際機場.md "wikilink")的地方。實尾島的東邊是有人居住的[舞衣岛](../Page/舞衣岛.md "wikilink")，與本土之間有渡船聯繫。

實尾島在[朴正熙前總統統治期間](../Page/朴正熙.md "wikilink")，曾經是韓國創設的「[684部隊](../Page/684部隊.md "wikilink")」訓練行刺[金日成任務的駐軍基地](../Page/金日成.md "wikilink")。《[-{zh-hant:實尾島風雲;zh-hans:实尾岛风云;zh-hk:實尾島;zh-tw:實尾島風雲}-](../Page/實尾島風雲.md "wikilink")》是根據該部隊發動的[實尾島事件改編而成的電影](../Page/實尾島事件.md "wikilink")。

## 參見

  - [實尾島事件](../Page/實尾島事件.md "wikilink")
  - 電影《[-{zh-hant:實尾島風雲;zh-hans:实尾岛风云;zh-hk:實尾島;zh-tw:實尾島風雲}-](../Page/實尾島風雲.md "wikilink")》

[Category:韓國島嶼](../Category/韓國島嶼.md "wikilink")
[Category:仁川廣域市地理](../Category/仁川廣域市地理.md "wikilink")
[Category:黃海](../Category/黃海.md "wikilink")
[Category:无人岛](../Category/无人岛.md "wikilink")