[Simon-stevin.jpeg](https://zh.wikipedia.org/wiki/File:Simon-stevin.jpeg "fig:Simon-stevin.jpeg")

**西蒙·斯蒂文**（Simon
Stevin，1548年－1620年），[弗蘭德](../Page/弗蘭德.md "wikilink")[數學家](../Page/數學家.md "wikilink")、工程師。

關於他的生平所知不多。或生於[布魯日](../Page/布魯日.md "wikilink")。他曾為[拿骚的莫里茨的老師](../Page/拿骚的莫里茨.md "wikilink")。

他大部分作品都用[荷蘭語寫成](../Page/荷蘭語.md "wikilink")，不只因他認為[荷蘭語是最適合詮釋科學的語言](../Page/荷蘭語.md "wikilink")，更希望能藉此幫助學術界外不懂[拉丁語的人学习](../Page/拉丁語.md "wikilink")[科學](../Page/科學.md "wikilink")。

## 成就

[Stevin_-_Oeuvres_mathematiques,_1634_-_4607786.tif](https://zh.wikipedia.org/wiki/File:Stevin_-_Oeuvres_mathematiques,_1634_-_4607786.tif "fig:Stevin_-_Oeuvres_mathematiques,_1634_-_4607786.tif")

  - 製造了一個「陸地風帆」，能比馬匹走得更快。
  - 編寫[利率表](../Page/利率.md "wikilink")*Tafelen of Interest*（1582年）
  - 分辨了穩定[平衡和不穩定平衡](../Page/平衡.md "wikilink")。
  - 1586年曾做實驗證明兩個重量不同的球同時落下同時到地，時間比[伽里略還早](../Page/伽里略.md "wikilink")\[1\]。
  - 在*De Beghinselen des
    Waterwichts*（《[流體靜力學原理](../Page/流體靜力學.md "wikilink")》，1586年）中，提出[流體靜力學悖論](../Page/流體靜力學悖論.md "wikilink")。
  - 寫一張小冊子*De Thiende*推廣[小數點記號](../Page/小數點.md "wikilink")。
  - 发明了許多數學名詞的荷蘭語譯名。這使荷蘭語成為唯一一種大部分數學名詞的源頭都不是來自拉丁語的西歐語言，例如[數學](../Page/數學.md "wikilink")「」、[直徑](../Page/直徑.md "wikilink")「middellijn」（穿過中心的線）等词。
  - 1585年在*De Spiegheling der
    Singconst*中提出[十二平均律](../Page/十二平均律.md "wikilink")，比[朱載堉稍晚](../Page/朱載堉.md "wikilink")，雖然斯特芬生前沒有公開發表。
  - 在[建築學](../Page/建築學.md "wikilink")、[公共事務上他都有不少見解](../Page/公共事務.md "wikilink")，記錄在*Materiae
    Politicae, Burgherlike Stoffen*（1649年）、*Vita Politica. Het
    Burgherlick leven*等書中。

## 參考

  - <http://users.ugent.be/~gvdbergh/files/publatex/stevinoe.html>

[Stevin](../Page/分类:1620年逝世.md "wikilink")
[Stevin](../Page/分类:荷兰数学家.md "wikilink")

1.  《科学年表》利萨·罗斯纳，P38