[Aristotle-constitutions-2.png](https://zh.wikipedia.org/wiki/File:Aristotle-constitutions-2.png "fig:Aristotle-constitutions-2.png")
《**政治學**》（，）是[古希腊哲学家](../Page/古希腊.md "wikilink")[亚里士多德一部关于](../Page/亚里士多德.md "wikilink")[政治哲学的著作](../Page/政治哲学.md "wikilink")。這本書是他對於[城邦各項政治事物的討論](../Page/希臘城邦.md "wikilink")。\[1\]是古希腊第一部全面而系统地论述政治问题的著作。

## 成书历史

亚里士多德在吕克昂学园对[希腊半岛的](../Page/希腊.md "wikilink")158个[城邦进行了实地考察](../Page/城邦.md "wikilink")，写成了《[雅典政制](../Page/雅典政制.md "wikilink")》（现仅存残篇）。《[政治學](../Page/政治學.md "wikilink")》一书就是在此基础上写就而成。\[2\]

## 重要概念

亚里士多德在书中提出了“[人天生就是政治動物](../Page/人天生就是政治動物.md "wikilink")”的命题，阐述了城邦的起源、性质、目的、任务和活动原则，提出了关于[公共权力](../Page/公共权力.md "wikilink")、[政体](../Page/政体.md "wikilink")、[法治等方面的理论](../Page/法治.md "wikilink")。

亚里士多德将政体分为六种：[寡头制](../Page/寡头制.md "wikilink")、[民主制](../Page/民主制.md "wikilink")、[混合政体](../Page/混合政体.md "wikilink")、[僭主政体](../Page/僭主.md "wikilink")（[專政](../Page/專政.md "wikilink")）。

## 参考文献

[分類:亞里士多德作品](../Page/分類:亞里士多德作品.md "wikilink")

[Category:政治哲學書籍](../Category/政治哲學書籍.md "wikilink")
[Category:古希臘法律](../Category/古希臘法律.md "wikilink")
[Category:古希臘政治哲學](../Category/古希臘政治哲學.md "wikilink")
[Category:倫理學書籍](../Category/倫理學書籍.md "wikilink")

1.
2.