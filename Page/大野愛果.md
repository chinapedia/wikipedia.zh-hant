**大野愛果**（，），[日本](../Page/日本.md "wikilink")[大阪府岸和田出生](../Page/大阪府.md "wikilink")，[作曲家](../Page/作曲家.md "wikilink")、歌手。隶属[GIZA
studio](../Page/GIZA_studio.md "wikilink")，伊麗莎白音樂大學畢業。本名未公開。

## 人物

  - 從小就在充滿音樂的環境中成長，[大阪府立和泉高等学校学生時代把泉佐野市做為中心進行著活動](../Page/大阪府立和泉高等学校.md "wikilink")。1997年把試唱帶交給[Being,Inc.的製片人](../Page/Being,Inc..md "wikilink")，開始了專業作曲活動。
  - 2002年發行專輯，以歌手的身分亮相。
  - 現在除了作曲活動以外，也參加許多歌手歌曲的合聲。在官方網站上自己的專欄也連載中。

## 特色

是兼具合聲及作曲能力的實力派音樂人，同時涉略各個音樂領域，是位製作廣泛的音樂家，也是現在的[日本國內非常新奇的實力派的女性作曲家](../Page/日本.md "wikilink")。在日本被稱為「[織田哲郎的接班人](../Page/織田哲郎.md "wikilink")」。

## 專輯

  - 《Shadows of Dreams》（2002年1月15日发行）
  - 《Secret Garden》（2002年12月4日发行）

## 主要提供樂曲

  - Love, Day After Tomorrow
  - Stay by my side
  - Secret of my heart
  - Simply Wonderful
  - Delicious way
  - Reach for the sky
  - 冷たい海
  - Start in my life
  - Always
  - Can't forget your love
  - PERFECT CRIME
  - Can't forget your love
  - Like a star in the night
  - Time after time～花舞う街で～
  - Love,needing
  - Growing of my heart
  - 白い雪
  - Season of love
  - This is your life
  - Trying To Find My Way
  - YES or NO
  - Brand New Day
  - いつかは あの空に
  - The ROSE～melody in the sky～
  - Loving you…
  - Give me one more chance
  - key to my heart
  - 不思議の国
  - fantasy - Natural
  - Tonight, I feel close to you
  - If I believe
  - Lover Boy
  - through the River
  - Honey,feeling for me
  - Tell me what
  - LOVE SICK
  - I sing a song for you
  - chance for you
  - Seven Nights
  - SAFEST PLACE
  - State of mind

<!-- end list -->

  - Close To Your Heart
  - It's crazy for you
  - Ohh\! Paradise Taste\!\!
  - 恋はスリル、ショック、サスペンス
  - FAITH
  - Run up - Forever You 〜永遠に君と〜
  - Can you feel the POWER OF WORDS?
  - START
  - Boom-Boom-Boom -
  - GLORIOUS/PRECIOUS PLACE
  - MIRACLE
  - Close To Your Heart
  - Black eyes,Blue tears
  - It's crazy for you
  - golden moonlight
  - POWER OF WORDS|SPARK
  - A.I.R
  - double hearted
  - PLAYGIRL
  - DELIGHT

<!-- end list -->

  - 少女の頃に戻ったみたいに
  - Get U're Dream
  - 明日を夢見て
  - 瞳閉じて
  - もっと近くで君の横顔見ていたい
  - かけがえのないもの
  - 今日はゆっくり話そう
  - 星のかがやきよ
  - 夏を待つ帆のように
  - 悲しいほど貴方が好き
  - カラッといこう\!
  - ハートに火をつけて
  - グロリアス マインド
  - 時間の翼
  - hero
  - 天使のような笑顔で
  - 悲しいほど 今日は雨でも
  - かけがえのないもの
  - 君とのふれあい(君とのDistance|)
  - サヨナラまでのディスタンス
  - セパレート・ウェイズ
  - 月に願いを

<!-- end list -->

  - Tears Go By
  - CHU☆TRUE LOVE
  - I shall be released
  - Shocking Blue
  - I'm in love
  - 居心地のいいハニー
  - 眠る君の横顔に微笑みを
  - へこんだ気持ち 溶かすキミ
  - 笑顔でいようよ
  - そっと優しいかぜに吹かれながら
  - Hand to Hand
  - 飛び立てない私にあなたが翼をくれた
  - どんなに明日が見えなくても

<!-- end list -->

  - 静かなるメロディー君を知らない街へ
  - つながり
  - 世界 止めて
  - となり
  - Lost In Paradise
  - きっともう恋にはならない

## 外部链接

  - [大野愛果官網](http://www.aika-ohno.com/)

[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:日本創作歌手](../Category/日本創作歌手.md "wikilink")
[Category:大阪府出身人物](../Category/大阪府出身人物.md "wikilink")
[Category:Being旗下藝人](../Category/Being旗下藝人.md "wikilink")