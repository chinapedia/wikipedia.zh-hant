**蔡憲棠**（）原名**蔡暉鎧**，[台灣職業足球員](../Page/台灣.md "wikilink")，[中華台北男子足球代表隊隊員](../Page/中華台北男子足球代表隊.md "wikilink")，現時效力於[大同足球隊](../Page/大同足球隊.md "wikilink")，司職[中場](../Page/中場.md "wikilink")。2000年以後台灣足球的知名球星，身材挺拔球風剽悍，
被認為是少數具備國際水準的台灣足球員，但是蔡憲棠最知名的是他的堅持努力精神，兩膝蓋共動過6次十字韌帶手術，還於2011年夏天被診斷出心臟的問題，卻從沒有終結他的足球生涯，繼續拼鬥至今。

## 外部連結

  - [蔡憲棠在national-football-teams.com的球員資料](http://www.national-football-teams.com/v2/player.php?id=27323)
  - [蔡憲棠兩膝蓋多次韌帶斷裂，至少動過六次刀](http://paper.takungpao.com/html/2012-12/07/content_40_2.htm)

[Category:蔡姓](../Category/蔡姓.md "wikilink")
[Category:台灣足球運動員](../Category/台灣足球運動員.md "wikilink")
[Category:2013年東亞盃中華台北男子足球代表隊選手](../Category/2013年東亞盃中華台北男子足球代表隊選手.md "wikilink")