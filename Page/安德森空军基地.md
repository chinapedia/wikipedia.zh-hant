**安德森空軍基地**（英語：**Andersen Air Force
Base**）是位於[美國](../Page/美國.md "wikilink")[關島北部的一個空軍基地](../Page/關島.md "wikilink")，為[美國太平洋空軍第](../Page/美國太平洋空軍.md "wikilink")36聯隊的屯駐地。安德森基地上同時有轟炸機大隊。

現時基地主要支援[美軍在](../Page/美軍.md "wikilink")[太平洋及](../Page/太平洋.md "wikilink")[印度洋地區的](../Page/印度洋.md "wikilink")[後勤工作](../Page/後勤.md "wikilink")。
[Andersenafb-parking-ramp-b-52s.jpg](https://zh.wikipedia.org/wiki/File:Andersenafb-parking-ramp-b-52s.jpg "fig:Andersenafb-parking-ramp-b-52s.jpg")轟炸機於跑道邊\]\]
[B-1B_at_ground.jpg](https://zh.wikipedia.org/wiki/File:B-1B_at_ground.jpg "fig:B-1B_at_ground.jpg")\]\]
[JASDF_F-2_fighters.JPG](https://zh.wikipedia.org/wiki/File:JASDF_F-2_fighters.JPG "fig:JASDF_F-2_fighters.JPG")\]\]
{{-}}

## 相關條目

  - [愛德華空軍基地](../Page/愛德華空軍基地.md "wikilink")

## 外部連結

  - [Andersen Air Force Base](http://www.andersen.af.mil/) (官方網站)

[Category:美国空军基地](../Category/美国空军基地.md "wikilink")
[Category:關島機場](../Category/關島機場.md "wikilink")
[Category:越南戰爭美軍基地](../Category/越南戰爭美軍基地.md "wikilink")
[Category:1944年所設軍用機場](../Category/1944年所設軍用機場.md "wikilink")