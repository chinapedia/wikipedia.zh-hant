**台灣東販股份有限公司**（*Taiwan Tohan Co.,
Ltd.*）、簡稱**台灣東販**，為[日本書籍通路經銷商](../Page/日本.md "wikilink")[東販的](../Page/東販.md "wikilink")[台灣](../Page/台灣.md "wikilink")[子公司](../Page/子公司.md "wikilink")，為台灣第一家獲許[投資的國外](../Page/投資.md "wikilink")[出版公司](../Page/出版公司.md "wikilink")，主要業務為[翻譯與發行各類日本](../Page/翻譯.md "wikilink")[書籍](../Page/書籍.md "wikilink")（含[漫畫](../Page/漫畫.md "wikilink")、[小說](../Page/小說.md "wikilink")）。近年來致力於雜誌、流行文化作品與本土原創作品的出版開發，積極拓展商品的類別，期朝全面化、多元化、專業化之目標邁進。

## 主要出版產品

### 雜誌

| 雜誌名稱                                                  | 發售時間  | 每本售價   | 備註  |
| ----------------------------------------------------- | ----- | ------ | --- |
| [HERE](../Page/HERE.md "wikilink")                    | 每月20號 | NT$128 |     |
| [BANG](../Page/BANG.md "wikilink")                    | 每月5號  | NT$120 |     |
| [yappy\!](../Page/yappy!.md "wikilink")               | 雙月刊   | NT$120 | 已停刊 |
| [狗狗の日子](../Page/狗狗の日子.md "wikilink")                  | 季刊    | NT$250 | 已停刊 |
| [足球王者](../Page/足球王者.md "wikilink")                    | 三月刊   | NT$138 |     |
| [BANG\! SPECIAL](../Page/BANG!_SPECIAL.md "wikilink") | 不定    | 不定     |     |

### 輕小說

  - VOCALOID輕小說

<!-- end list -->

  - [七宗罪系列](../Page/七宗罪系列.md "wikilink")
      - [惡之系列](../Page/惡之系列.md "wikilink")
      - 《惡之娘 黃之終曲》、《惡之娘 綠之搖籃曲》、《惡之娘 紅之前奏曲》、《惡之娘 藍之序曲》
      - 《惡之大罪 維諾瑪尼亞公爵的瘋狂》
      - 《惡之大罪 惡食娘康奇塔》
      - 《惡之大罪 安眠公主的贈禮》
      - 《惡之大罪 第五號小丑》
      - 《惡之間奏曲 惡之娘世界大公開》
      - 《惡之圓舞曲 惡之大罪導覽書》
  - [櫻之雨系列](../Page/櫻之雨.md "wikilink")
      - 《櫻之雨》、《櫻之雨 圍繞著我們的奇跡》、《Fire flower 各顯特色的多彩生活》、《櫻之雨 我們在這裡重逢》
  - 幸福安心委員會系列
      - 《這裡是幸福安心委員會》、《這裡是幸福安心委員會 女王陛下與永遠幸福的死刑犯》、《這裡是幸福安心委員會
        女王陛下與快樂的夏日遊戲》、《這裡是幸福安心委員會
        女王陛下與世界線上的災難》
  - [囚人與紙飛行機系列](../Page/囚人與紙飛行機.md "wikilink")
      - 《囚人與紙飛行機 少年悖論》上、下
      - 《囚人與紙飛行機 少女難題》上、下
      - 《囚人與紙飛行機 幕後二律背反》上、下
  - 《[心](../Page/心_\(VOCALOID\).md "wikilink")》
      - 《心》、《心 再會》、《心 奇跡》
  - 《陰暗森林的馬戲團》
  - 《秘蜜 黑之誓約》
  - 《東京電腦偵探團》
  - 《秘密警察》系列
      - 《秘密警察FILE：01 -Escape -》
      - 《秘密警察FILE：02 -Confidential -》
  - 《南極點PIAPIA動畫》
  - 《仙杜瑞拉》
  - 《魔女 Witch Hunt》
  - 《教師與少女風波》
  - 《憂鬱的心情》系列
      - 《憂鬱的心情1》
  - 《Just Be Friends. 只能做朋友》
  - 《愛麗絲音樂仙境 ALICE IN MUSIC LAND》

<!-- end list -->

  - 一般向輕小說

<!-- end list -->

  - [迷宮探索者](../Page/迷宮探索者.md "wikilink")1～4
  - [轉生！白之王國物語](../Page/轉生！白之王國物語.md "wikilink")1～5
  - [狙擊冰風 雪妖精與白色死神上](../Page/狙擊冰風_雪妖精與白色死神.md "wikilink")、下
  - [美少女通通是我的](../Page/美少女通通是我的.md "wikilink")1～2
  - [向森之魔物獻上花束](../Page/向森之魔物獻上花束.md "wikilink")
  - [勇者遭到背叛之後](../Page/勇者遭到背叛之後.md "wikilink")
  - [蒼穹女武神](../Page/蒼穹女武神.md "wikilink")1～2
  - [受虐狂熱](../Page/受虐狂熱.md "wikilink")1～
  - [吃掉死神的少女上](../Page/吃掉死神的少女.md "wikilink")

<!-- end list -->

  - 女性向輕小說

<!-- end list -->

  - [勇者大人突然向我求婚\!?](../Page/勇者大人突然向我求婚!?.md "wikilink")
  - [無法停止的HAPPY END](../Page/無法停止的HAPPY_END.md "wikilink")
  - [異界魔術士](../Page/異界魔術士.md "wikilink")1～
  - [戀天女與四個新郎](../Page/戀天女與四個新郎.md "wikilink")

<!-- end list -->

  - 輕樂讀系列

<!-- end list -->

  - [泰利耶之戰](../Page/泰利耶之戰.md "wikilink")1～4
  - [靈狩者遊戲](../Page/靈狩者遊戲.md "wikilink")1～2
  - [平安勇闖妖魔界](../Page/平安勇闖妖魔界.md "wikilink")1～4
  - [吉石駕到](../Page/吉石駕到.md "wikilink")1
  - BL小說
      - [日輪將軍](../Page/日輪將軍.md "wikilink")

### 漫畫

  - [宮崎駿](../Page/宮崎駿.md "wikilink")（吉卜力）作品系列
  - [手塚治虫作品系列](../Page/手塚治虫.md "wikilink")
  - RIVER作品系列

### 書籍

  - 食譜

<!-- end list -->

  - 第一次做日本料理
  - 正宗台菜料理
  - 在家做義大利美食
  - 醬汁料理百匯

<!-- end list -->

  - 企管書

<!-- end list -->

  - 7-11消費心理學
  - 員工就要這樣訓練

<!-- end list -->

  - 手工藝

<!-- end list -->

  - 創意生活系列

## 相關條目

  - [東販](../Page/東販.md "wikilink")
  - [台灣代理輕小說列表](../Page/台灣代理輕小說列表.md "wikilink")
  - [台灣出版輕小說列表](../Page/台灣出版輕小說列表.md "wikilink")

## 參考資料

## 外部連結

  - [台灣東販](http://www.tohan.com.tw/)

  -
  - [官方噗浪｜東販推廣部](https://www.plurk.com/tohan_tw/)

  - [東販輕小說部落格](http://ranobe.pixnet.net/blog/)

[Category:1989年成立的公司](../Category/1989年成立的公司.md "wikilink")
[Category:總部位於臺北市松山區的工商業機構](../Category/總部位於臺北市松山區的工商業機構.md "wikilink")
[Category:臺灣漫畫出版社](../Category/臺灣漫畫出版社.md "wikilink")