**泰安市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山东省下辖的](../Page/山东省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于山东省中部，[黄河之东](../Page/黄河.md "wikilink")。东南两面临济南市，东南与[临沂市相接](../Page/临沂市.md "wikilink")，南与[济宁市毗邻](../Page/济宁市.md "wikilink")，西与[聊城市及](../Page/聊城市.md "wikilink")[河南省](../Page/河南省.md "wikilink")[濮阳市隔黄河相望](../Page/濮阳市.md "wikilink")。全市总面积7,761平方公里，人口560.08万。泰安之名取“国泰民安”之意，因五岳之首[泰山而驰名](../Page/泰山.md "wikilink")。主城区位于泰山南麓，山城一体，是[国家历史文化名城](../Page/国家历史文化名城.md "wikilink")、[国家卫生城市以及](../Page/国家卫生城市.md "wikilink")[中国优秀旅游城市](../Page/中国优秀旅游城市.md "wikilink")。

## 历史

泰安，因[五岳之](../Page/五岳.md "wikilink")[东岳](../Page/东岳.md "wikilink")──[泰山而得名](../Page/泰山.md "wikilink")，古语云：“泰山安，则四海皆安”，寓“国泰民安”之意。[秦代今境先属](../Page/秦代.md "wikilink")[齐郡](../Page/齐郡.md "wikilink")，后属[济北郡](../Page/济北郡_\(秦朝\).md "wikilink")，并置[博阳县](../Page/博阳县.md "wikilink")，为济北郡治。[西汉初设](../Page/西汉.md "wikilink")[泰山郡](../Page/泰山郡.md "wikilink")，改博阳县为[博县](../Page/博县.md "wikilink")，为泰山郡治。[元封元年](../Page/元封_\(西汉\).md "wikilink")（前110年）置[奉高县](../Page/奉高县.md "wikilink")，治今泰安市东，为泰山郡治。[魏晋因之](../Page/魏晋.md "wikilink")。[北魏年间泰山郡移治博县](../Page/北魏.md "wikilink")，改名[博平县](../Page/博平县.md "wikilink")，治今市境东南。[北齐改泰山郡为](../Page/北齐.md "wikilink")[东平郡](../Page/东平郡.md "wikilink")，博平县复为博县，为郡治。

[隋](../Page/隋朝.md "wikilink")[开皇初废东平郡](../Page/开皇.md "wikilink")，开皇十六年（596年）改博平县为[汶阳县](../Page/汶阳县.md "wikilink")，后改[博城县](../Page/博城县.md "wikilink")。[唐](../Page/唐朝.md "wikilink")[武德五年](../Page/武德.md "wikilink")（622年），于博城县置[东泰州](../Page/东泰州.md "wikilink")。[贞观元年](../Page/贞观.md "wikilink")（627年）废东泰州，省[梁父县入博城县](../Page/梁父县.md "wikilink")。[乾封元年](../Page/乾封.md "wikilink")（666年）改博城县为[乾封县](../Page/乾封县.md "wikilink")。[总章元年](../Page/总章.md "wikilink")（668年），复改博城县。[神龙元年](../Page/神龙.md "wikilink")（705年），复为乾封县，属[兖州](../Page/兖州.md "wikilink")。[北宋](../Page/北宋.md "wikilink")[开宝五年](../Page/开宝.md "wikilink")（972年），乾封县移治[岱岳](../Page/岱岳.md "wikilink")。[大中祥符元年](../Page/大中祥符.md "wikilink")（1008年）改乾封县为[奉符县](../Page/奉符县.md "wikilink")，属[袭庆府](../Page/袭庆府.md "wikilink")。

[金](../Page/金朝.md "wikilink")[天会十四年](../Page/天会_\(金朝\).md "wikilink")（1136年），于奉符县置[泰安军](../Page/泰安军.md "wikilink")，“泰安”之名始此。清[乾隆](../Page/乾隆.md "wikilink")《泰安府志》：“汉人称天下之安如泰山而维之，名盖取诸此。”《山东通志》曰：取“安益求安”之意。[大定二十二年](../Page/大定_\(金朝\).md "wikilink")（1182年）升泰安军为[泰安州](../Page/泰安州.md "wikilink")，治奉符县。[明](../Page/明朝.md "wikilink")[洪武初](../Page/洪武.md "wikilink")，州治奉符县省入泰安州，属[济南府](../Page/济南府.md "wikilink")。[清](../Page/清朝.md "wikilink")[雍正二年](../Page/雍正.md "wikilink")（1724年）升泰安为[直隶州](../Page/直隶州.md "wikilink")，十三年升为[泰安府](../Page/泰安府.md "wikilink")，并设[泰安县](../Page/泰安县.md "wikilink")，为府治。

[民国二年](../Page/民国.md "wikilink")（1913年）废泰安府，辖县分属岱北、岱南、济西三道，次年分別改[济南道](../Page/济南道.md "wikilink")、[济宁道](../Page/济宁道.md "wikilink")、[东临道](../Page/东临道.md "wikilink")。1925年分属泰安、兖济等道。1928年废道制。1940年置[泰西专区](../Page/泰西专区.md "wikilink")，属[鲁西行政区](../Page/鲁西行政区.md "wikilink")；又置[泰山专区](../Page/泰山专区.md "wikilink")。1941年析泰山专区南部置[泰南专区](../Page/泰南专区.md "wikilink")，同属[鲁中行政区](../Page/鲁中行政区.md "wikilink")；后泰西专区改属[冀鲁豫行政区](../Page/冀鲁豫行政区.md "wikilink")。1942年2月，析泰安、宁阳两县置[泰宁县](../Page/泰宁县.md "wikilink")，隶泰南专区。1945年将泰南专区并入[沂蒙专区](../Page/沂蒙专区.md "wikilink")。1948年泰山、沂蒙、泰西三专区均属[鲁中南行政区](../Page/鲁中南行政区.md "wikilink")。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，泰安、泰宁两县隶属泰山专区。1950年将泰山、泰西两专区合并为[泰安专区](../Page/泰安专区.md "wikilink")。1952年9月，泰宁县改名[徂阳县](../Page/徂阳县.md "wikilink")。1956年3月撤销。1958年6月，撤销泰安专区，析泰安县城区和郊区置泰山市；同年11月将泰安县和泰山市合并置泰安市，隶属[济南市](../Page/济南市.md "wikilink")。1961年5月复置泰安专区。1963年5月，改泰安市为泰安县。1967年泰安专区改称[泰安地区](../Page/泰安地区.md "wikilink")。1982年3月，泰安县改为县级泰安市，属泰安地区。1985年5月，撤销泰安地区，设立地级泰安市；原县级泰安市改设[泰山区](../Page/泰山区_\(泰安市\).md "wikilink")、[郊区](../Page/郊区_\(泰安市\).md "wikilink")；辖[宁阳](../Page/宁阳.md "wikilink")、[肥城](../Page/肥城.md "wikilink")、[东平三县](../Page/东平.md "wikilink")，代管[莱芜](../Page/莱芜.md "wikilink")、[新泰二市](../Page/新泰.md "wikilink")；将[汶上](../Page/汶上.md "wikilink")、[泗水两县划归](../Page/泗水县.md "wikilink")[济宁市](../Page/济宁市.md "wikilink")，[平阴县划归济南市](../Page/平阴县.md "wikilink")。1992年11月，莱芜市升为地级市；同年肥城撤县改市。2000年4月，泰安市郊区更名为[岱岳区](../Page/岱岳区.md "wikilink")。

## 地理

地处东经116°02′至117°59′、北纬35°38′至36°28′之间，东西长约176.6千米，南北宽约93.5千米；位于[大汶河上游](../Page/大汶河.md "wikilink")；城区位于泰山脚下，依山而建，山城一体。北距省会济南市66.8千米，南至三孔圣地曲阜市74.6千米，是山东省“一山、一水、一圣人”旅游热线的中点。泰安市最高点为[玉皇顶](../Page/玉皇顶_\(泰山\).md "wikilink")，海拔1532.7米。

### 气候

## 政治

### 现任领导

<table>
<caption>泰安市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党泰安市委员会.md" title="wikilink">中国共产党<br />
泰安市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/泰安市人民代表大会.md" title="wikilink">泰安市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/泰安市人民政府.md" title="wikilink">泰安市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议泰安市委员会.md" title="wikilink">中国人民政治协商会议<br />
泰安市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/王云鹏.md" title="wikilink">王云鹏</a>[1]</p></td>
<td><p><a href="../Page/唐福泉.md" title="wikilink">唐福泉</a>[2]</p></td>
<td><p><a href="../Page/李希信.md" title="wikilink">李希信</a>[3]</p></td>
<td><p><a href="../Page/周桂萍.md" title="wikilink">周桂萍</a>（女）[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/巨野县.md" title="wikilink">巨野县</a></p></td>
<td><p>山东省<a href="../Page/荣成市.md" title="wikilink">荣成市</a></p></td>
<td><p>山东省<a href="../Page/临邑县.md" title="wikilink">临邑县</a></p></td>
<td><p>山东省<a href="../Page/蒙阴县.md" title="wikilink">蒙阴县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年6月</p></td>
<td><p>2017年2月</p></td>
<td><p>2016年7月</p></td>
<td><p>2017年2月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖2个[市辖区](../Page/市辖区.md "wikilink")、2个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管2个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[泰山区](../Page/泰山区_\(泰安市\).md "wikilink")、[岱岳区](../Page/岱岳区.md "wikilink")
  - 县级市：[新泰市](../Page/新泰市.md "wikilink")、[肥城市](../Page/肥城市.md "wikilink")
  - 县：[宁阳县](../Page/宁阳县.md "wikilink")、[东平县](../Page/东平县.md "wikilink")

此外，泰安市还设立以下经济功能区：[泰山风景名胜区](../Page/泰山风景名胜区.md "wikilink")（[国家级](../Page/国家级风景名胜区.md "wikilink")）、[泰安高新技术产业开发区](../Page/泰安高新技术产业开发区.md "wikilink")（[国家级](../Page/国家级高新技术产业开发区.md "wikilink")）。

<table>
<thead>
<tr class="header">
<th><p><strong>泰安市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[5]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>370900</p></td>
</tr>
<tr class="odd">
<td><p>370902</p></td>
</tr>
<tr class="even">
<td><p>370911</p></td>
</tr>
<tr class="odd">
<td><p>370921</p></td>
</tr>
<tr class="even">
<td><p>370923</p></td>
</tr>
<tr class="odd">
<td><p>370982</p></td>
</tr>
<tr class="even">
<td><p>370983</p></td>
</tr>
<tr class="odd">
<td><p>注：泰山区数字包含泰山风景名胜区所辖大津口乡；岱岳区数字包含泰安高新技术产业开发区所辖北集坡街道。</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>泰安市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[6]（2010年11月）</p></th>
<th><p>户籍人口[7]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>泰安市</p></td>
<td><p>5494207</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>泰山区</p></td>
<td><p>760045</p></td>
<td><p>13.83</p></td>
</tr>
<tr class="even">
<td><p>岱岳区</p></td>
<td><p>975380</p></td>
<td><p>17.75</p></td>
</tr>
<tr class="odd">
<td><p>宁阳县</p></td>
<td><p>754647</p></td>
<td><p>13.74</p></td>
</tr>
<tr class="even">
<td><p>东平县</p></td>
<td><p>741566</p></td>
<td><p>13.50</p></td>
</tr>
<tr class="odd">
<td><p>新泰市</p></td>
<td><p>1315942</p></td>
<td><p>23.95</p></td>
</tr>
<tr class="even">
<td><p>肥城市</p></td>
<td><p>946627</p></td>
<td><p>17.23</p></td>
</tr>
<tr class="odd">
<td><p>注：岱岳区的常住人口数据中包含泰安高新技术产业开发区83871人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")549.42万人\[8\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加了15.96万人。增长2.99%，年平均增长0.30%。其中，男性为274.20万人，占总人口的49.91%；女性为275.22万人，占总人口的50.09%，总人口性别比（以女性为100）为99.63。0－14岁的人口为
85.74万人，占15.60%，15－64岁的人口为411.06万人，占74.82%；65岁及以上的人口为52.62万人，占9.58%。

### 民族

以[汉族为主](../Page/汉族.md "wikilink")。主要少数民族为[回族](../Page/回族.md "wikilink")，泰安市回族人口据省内前列（少于[德州市](../Page/德州市.md "wikilink")）。全市回族超过60000人，其中尤以泰山岱岳两区为多。市区内有[清真寺小区](../Page/清真寺.md "wikilink")，并设立回民中学一所。

## 交通

  - 铁路：[泰山站](../Page/泰山站_\(泰安\).md "wikilink")（始建于1909年，2000年前称泰安火车站）为[京沪铁路](../Page/京沪铁路.md "wikilink")、[辛泰铁路](../Page/辛泰铁路.md "wikilink")、[泰肥铁路的交点](../Page/泰肥铁路.md "wikilink")，境内还有[磁莱铁路](../Page/磁莱铁路.md "wikilink")，并计划建设[泰聊铁路和](../Page/泰聊铁路.md "wikilink")[济泰城际轨道](../Page/济泰城际轨道.md "wikilink")。业已通车的的[京沪高铁在境内开设高铁](../Page/京沪高铁.md "wikilink")[泰安站](../Page/泰安站.md "wikilink")。
  - 公路：[104国道](../Page/104国道.md "wikilink")、[103国道](../Page/103国道.md "wikilink")、[莱泰高速公路](../Page/莱泰高速公路.md "wikilink")、[京沪高速公路](../Page/京沪高速公路.md "wikilink")、[京台高速公路](../Page/京台高速公路.md "wikilink")、[济广高速公路等公路交通网四通八达](../Page/济广高速公路.md "wikilink")。在建的[泰聊高速公路](../Page/泰聊高速公路.md "wikilink")，将构成未来[青兰高速公路的一部分](../Page/青兰高速公路.md "wikilink")。

## 经济

泰安是山东省重要的[煤炭](../Page/煤炭.md "wikilink")、[钢铁基地](../Page/钢铁.md "wikilink")，拥有[汽车](../Page/汽车.md "wikilink")（[中国重型汽车集团](../Page/中国重型汽车集团.md "wikilink")、青年莲花）、[煤炭](../Page/煤炭.md "wikilink")、输变电设备、新材料、新能源、[化工](../Page/化学工业.md "wikilink")、纺织服装、[食品等八大主导产业](../Page/食品.md "wikilink")。农业基础稳固，是山东省重要的[粮食产区之一](../Page/粮食.md "wikilink")，国家优质[小麦商品粮生产基地](../Page/小麦.md "wikilink")、畜牧生产基地、蔬菜生产基地和花卉苗木生产基地。

## 物产

  - 矿产资源丰富，是山东省重要的能源和原材料基地。已探明的矿产资源有58种，保有储量500亿吨。包括有[石膏](../Page/石膏.md "wikilink")、天然[硫](../Page/硫.md "wikilink")、[煤](../Page/煤.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[石棉等](../Page/石棉.md "wikilink")，其中硫储量（31.9亿吨）居亚洲第一位。石膏储量在中国居首位。已开采的矿产有煤、[铁](../Page/铁.md "wikilink")、[铜](../Page/铜.md "wikilink")、石膏、[耐火土](../Page/耐火土.md "wikilink")、[钾长石](../Page/钾长石.md "wikilink")、[石棉](../Page/石棉.md "wikilink")、[钾盐](../Page/钾盐.md "wikilink")、[岩盐](../Page/岩盐.md "wikilink")、石灰石、[花岗石](../Page/花岗石.md "wikilink")、[麦饭石](../Page/麦饭石.md "wikilink")、[矿泉水等](../Page/矿泉水.md "wikilink")。
  - 特产有肥城[桃](../Page/桃.md "wikilink")（佛桃）、泰山[赤鳞鱼](../Page/赤鳞鱼.md "wikilink")、泰山[赤灵芝](../Page/赤灵芝.md "wikilink")、泰山[板栗](../Page/板栗.md "wikilink")、葛石[大枣](../Page/大枣.md "wikilink")、河岔山[鸭蛋](../Page/鸭蛋.md "wikilink")、莱芜[黄姜](../Page/黄姜.md "wikilink")、汶河[花生](../Page/花生.md "wikilink")、东平湖[鲤鱼](../Page/鲤鱼.md "wikilink")、四大名药（[何首乌](../Page/何首乌.md "wikilink")、[紫草](../Page/紫草.md "wikilink")、[黄精](../Page/黄精.md "wikilink")、[四叶参](../Page/四叶参.md "wikilink")）。

## 教育

### 高校

  - [山东农业大学](../Page/山东农业大学.md "wikilink")
  - [山东科技大学](../Page/山东科技大学.md "wikilink")
  - [山东第一医科大学](../Page/山东第一医科大学（山东省医学科学院）.md "wikilink")
  - [泰山学院](../Page/泰山学院.md "wikilink")
  - [山东科技大学泰山科技学院](../Page/山东科技大学泰山科技学院.md "wikilink")
  - [山东服装职业学院](../Page/山东服装职业学院.md "wikilink")
  - [泰山职业技术学院](../Page/泰山职业技术学院.md "wikilink")

### 高中

  - [山东省泰安第一中学](../Page/山东省泰安第一中学.md "wikilink")
  - 山东省泰安第二中学
  - 山东省泰安第三中学
  - 山东省泰安第四中学
  - 山东省泰安第五中学
  - 山东省泰山中学
  - 泰安市英雄山中学
  - 山东省泰安长城中学
  - [肥城市第一高级中学](../Page/肥城市第一高级中学.md "wikilink")
  - 肥城市第二高级中学
  - 肥城市第三高级中学
  - 肥城市第六高级中学
  - 肥城市泰西中学
  - 山东省东平高级中学
  - 山东省东平县第一中学
  - [东平县明湖中学](../Page/东平县明湖中学.md "wikilink")
  - 山东省新泰第一中学
  - 山东省新泰第二中学
  - 山东省新泰中学
  - 新泰市新汶中学
  - 新泰市汶城中学

### 初中

  - [山东省泰安第六中学](../Page/山东省泰安第六中学.md "wikilink")
  - [山东省泰安泰山学院附属中学](../Page/山东省泰安泰山学院附属中学.md "wikilink")
  - [山东省泰安东岳中学](../Page/山东省泰安东岳中学.md "wikilink")
  - [山东省泰安迎春学校](../Page/山东省泰安迎春学校.md "wikilink")
  - [山东省泰安泮河中学](../Page/山东省泰安泮河中学.md "wikilink")
  - [山东省泰安省庄第一中学](../Page/山东省泰安省庄第一中学.md "wikilink")
  - [山东省泰安邱家店第二中学](../Page/山东省泰安邱家店第二中学.md "wikilink")
  - [山东省泰安南关中学](../Page/山东省泰安南关中学.md "wikilink")
  - [山东省泰安实验学校（初中部）](../Page/山东省泰安实验学校（初中部）.md "wikilink")
  - [山东省泰安外国语学校（初中部）](../Page/山东省泰安外国语学校（初中部）.md "wikilink")
  - [山东省泰安山东农业大学附属中学](../Page/山东省泰安山东农业大学附属中学.md "wikilink")
  - [山东省泰安市泰山区上高中学](../Page/山东省泰安市泰山区上高中学.md "wikilink")
  - [山东省泰安市泰山区凤台中学](../Page/山东省泰安市泰山区凤台中学.md "wikilink")

### 小学

  - [山东农业大学附属小学](../Page/山东农业大学附属小学.md "wikilink")
  - [泰安市實驗學校](../Page/泰安市實驗學校.md "wikilink")（[北實小](../Page/泰安市實驗學校.md "wikilink")）
  - [泰安市第一實驗學校](../Page/泰安市第一實驗學校.md "wikilink")（[南實小](../Page/泰安市第一實驗學校.md "wikilink")）
  - [山东省泰安文化路小学](../Page/山东省泰安文化路小学.md "wikilink")
  - [山东省泰安南关小学](../Page/山东省泰安南关小学.md "wikilink")
  - [山东省泰安迎春学校（小学部）](../Page/山东省泰安迎春学校（小学部）.md "wikilink")
  - [山东省泰安粮食市小学](../Page/山东省泰安粮食市小学.md "wikilink")
  - [山东省泰安仓库路小学](../Page/山东省泰安仓库路小学.md "wikilink")
  - [山东省泰安雷锋小学](../Page/山东省泰安雷锋小学.md "wikilink")

## 名胜古迹

  - [泰山](../Page/泰山.md "wikilink")
  - [岱庙](../Page/岱庙.md "wikilink")
  - [蒿里山](../Page/蒿里山.md "wikilink")
  - [徂徕山](../Page/徂徕山.md "wikilink")
  - [奈河桥](../Page/奈河桥.md "wikilink")
  - [大汶口遗址](../Page/大汶口遗址.md "wikilink")
  - [齐长城遗址](../Page/齐长城遗址.md "wikilink")
  - [东平故城](../Page/东平故城.md "wikilink")
  - [西磁窑址](../Page/西磁窑址.md "wikilink")
  - [东平湖](../Page/东平湖.md "wikilink")

## 名人

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>先秦
<ul>
<li><a href="../Page/柳下惠.md" title="wikilink">柳下惠</a></li>
<li><a href="../Page/鲍叔牙.md" title="wikilink">鲍叔牙</a></li>
<li><a href="../Page/左丘明.md" title="wikilink">左丘明</a></li>
<li><a href="../Page/钟离春.md" title="wikilink">钟离春</a></li>
<li><a href="../Page/孙膑.md" title="wikilink">孙膑</a></li>
</ul></li>
<li>西汉, 莽新
<ul>
<li><a href="../Page/高堂生.md" title="wikilink">高堂生</a></li>
<li><a href="../Page/胡毋生.md" title="wikilink">胡毋生</a></li>
<li><a href="../Page/子路_(汉朝).md" title="wikilink">子路</a></li>
<li><a href="../Page/刘诩.md" title="wikilink">刘诩</a></li>
<li><a href="../Page/范荆.md" title="wikilink">范荆</a></li>
<li><a href="../Page/范康.md" title="wikilink">范康</a></li>
</ul></li>
</ul></td>
<td><ul>
<li>建安, 魏晋
<ul>
<li><a href="../Page/刘桢.md" title="wikilink">刘桢</a></li>
<li><a href="../Page/于禁.md" title="wikilink">于禁</a></li>
<li><a href="../Page/羊祜.md" title="wikilink">羊祜</a></li>
<li><a href="../Page/王匡.md" title="wikilink">王匡</a></li>
<li><a href="../Page/羊衜.md" title="wikilink">羊衜</a></li>
<li><a href="../Page/叔孙无忌.md" title="wikilink">叔孙无忌</a></li>
<li><a href="../Page/鲍信.md" title="wikilink">鲍信</a></li>
<li><a href="../Page/孙观.md" title="wikilink">孙观</a></li>
<li><a href="../Page/淳于涎.md" title="wikilink">淳于涎</a></li>
</ul></li>
</ul></td>
<td><ul>
<li>唐宋
<ul>
<li><a href="../Page/石介.md" title="wikilink">石介</a></li>
<li><a href="../Page/程咬金.md" title="wikilink">程咬金</a></li>
<li><a href="../Page/罗贯中.md" title="wikilink">罗贯中</a></li>
<li><a href="../Page/孙复.md" title="wikilink">孙复</a></li>
<li><a href="../Page/钱乙.md" title="wikilink">钱乙</a></li>
<li><a href="../Page/梁灏.md" title="wikilink">梁灏</a></li>
<li><a href="../Page/党怀英.md" title="wikilink">党怀英</a></li>
<li><a href="../Page/萧大亨.md" title="wikilink">萧大亨</a></li>
</ul></li>
<li>元明清
<ul>
<li><a href="../Page/赵国麟.md" title="wikilink">赵国麟</a></li>
<li><a href="../Page/黄恩彤.md" title="wikilink">黄恩彤</a></li>
<li><a href="../Page/王祯.md" title="wikilink">王祯</a></li>
<li><a href="../Page/施天裔.md" title="wikilink">施天裔</a></li>
</ul></li>
</ul></td>
<td><ul>
<li>民国和共和国初期
<ul>
<li><a href="../Page/范明枢.md" title="wikilink">范明枢</a></li>
<li><a href="../Page/吕彦直.md" title="wikilink">吕彦直</a></li>
<li><a href="../Page/孙传芳.md" title="wikilink">孙传芳</a></li>
<li><a href="../Page/王耀武.md" title="wikilink">王耀武</a></li>
<li><a href="../Page/万里.md" title="wikilink">万里</a></li>
<li><a href="../Page/田纪云.md" title="wikilink">田纪云</a></li>
<li><a href="../Page/欧阳中石.md" title="wikilink">欧阳中石</a></li>
</ul></li>
<li>现代
<ul>
<li><a href="../Page/马忠臣.md" title="wikilink">马忠臣</a></li>
<li><a href="../Page/张庆黎.md" title="wikilink">张庆黎</a></li>
<li><a href="../Page/翟墨.md" title="wikilink">翟墨</a></li>
<li><a href="../Page/赵丹.md" title="wikilink">赵丹</a></li>
<li><a href="../Page/王峰.md" title="wikilink">王峰</a></li>
<li><a href="../Page/屈中恒.md" title="wikilink">屈中恒</a></li>
<li><a href="../Page/梨衣名.md" title="wikilink">梨衣名</a></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

另传说人物

  - [石敢当](../Page/石敢当.md "wikilink")
  - [碧霞元君（泰山老奶奶）](../Page/碧霞元君.md "wikilink")

与泰安有关

  - [范蠡](../Page/范蠡.md "wikilink")
  - [冯玉祥](../Page/冯玉祥.md "wikilink")
  - [黄巢](../Page/黄巢.md "wikilink")
  - [管辂](../Page/管辂.md "wikilink")
  - [曹植](../Page/曹植.md "wikilink")

## 友好城市

  - [泰安郡](../Page/泰安郡.md "wikilink")

  - [八王子市](../Page/八王子市.md "wikilink")

  - [乌卡亚利](../Page/乌卡亚利.md "wikilink")

  - [桥本市](../Page/桥本市.md "wikilink")

  - [笛吹市](../Page/笛吹市.md "wikilink")

  - [都灵](../Page/都灵.md "wikilink")

  - [弗朗西斯敦](../Page/弗朗西斯敦.md "wikilink")

  - [容迪亚伊](../Page/容迪亚伊.md "wikilink")

  - [洛布尼亚](../Page/洛布尼亚.md "wikilink")

  - [施瓦本行政区](../Page/施瓦本行政区.md "wikilink")

## 参考文献

## 外部链接

  - [泰安统计信息网](http://www.tatj.gov.cn/)
  - [泰安信息港](http://www.tainfo.net/)
  - [泰安人才网](https://web.archive.org/web/20110612202354/http://www.lotai.com/)
  - [中国泰山风景名胜区官方网站](http://www.mount-tai.com.cn/)
  - [泰安旅游政务网](http://www.tata.gov.cn/)

{{-}}

[泰安](../Category/泰安.md "wikilink")
[Category:山东地级市](../Category/山东地级市.md "wikilink")
[鲁](../Category/国家历史文化名城.md "wikilink")
[鲁](../Category/国家卫生城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.