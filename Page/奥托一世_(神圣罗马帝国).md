[Otto_the_Great.jpg](https://zh.wikipedia.org/wiki/File:Otto_the_Great.jpg "fig:Otto_the_Great.jpg")
[Grave_of_Otto_I,_Holy_Roman_Emperor.jpg](https://zh.wikipedia.org/wiki/File:Grave_of_Otto_I,_Holy_Roman_Emperor.jpg "fig:Grave_of_Otto_I,_Holy_Roman_Emperor.jpg")裏的奥托一世之棺\]\]
**奥托一世**（**Otto
I**，），又译**鄂图一世**，[东法兰克国王](../Page/德国君主列表.md "wikilink")（936年—973年在位），[神圣罗马帝国皇帝](../Page/神圣罗马帝国皇帝.md "wikilink")（962年加冕）。史称奥托大帝（Otto
der
Große）。[东法兰克国王](../Page/德国君主列表.md "wikilink")[亨利一世之子](../Page/亨利一世_\(德意志\).md "wikilink")，母为Ringlheim的玛蒂尔达。先当选为[萨克森公爵](../Page/萨克森公爵列表.md "wikilink")，936年由东法兰克部落[公爵选为国王](../Page/公爵.md "wikilink")。936年7月31日，[美因茨](../Page/美因茨.md "wikilink")[大主教](../Page/大主教.md "wikilink")[希尔德贝特在](../Page/希尔德贝特.md "wikilink")[亚琛为奥托一世加冕](../Page/亚琛.md "wikilink")。

奥托一世坚持他对各[部落公国均拥有最高权力](../Page/部落公国.md "wikilink")，公爵们为此常常造反。938年奥托一世镇压了由[法兰克尼亚公爵埃贝哈德和](../Page/弗兰肯的埃贝哈德.md "wikilink")[洛林公爵吉塞尔伯特联合发动的叛乱](../Page/吉塞尔博特_\(洛林公爵\).md "wikilink")。929年—941年镇压其弟[巴伐利亚公爵](../Page/巴伐利亚统治者列表.md "wikilink")[亨利一世的叛乱](../Page/亨利一世_\(巴伐利亚公爵\).md "wikilink")。944年奥托一世封自己的[女婿](../Page/女婿.md "wikilink")[康拉德](../Page/康拉德_\(洛林公爵\).md "wikilink")（“红色”）为[洛林公爵](../Page/洛林公爵.md "wikilink")，此人后来与他反目。946年奥托一世率军干涉[法国内战](../Page/法国.md "wikilink")，先支持反对国王的[法兰西公爵](../Page/法兰西公爵.md "wikilink")（[卡佩家族的成员](../Page/卡佩家族.md "wikilink")），后转为支持[西法兰克国王](../Page/法国君主列表.md "wikilink")[路易四世](../Page/路易四世_\(西法兰克\).md "wikilink")。949年封长子鲁道夫为[士瓦本公爵](../Page/士瓦本公爵.md "wikilink")。

奥托一世是最早侵略[斯拉夫人的](../Page/斯拉夫人.md "wikilink")[条顿君主之一](../Page/条顿.md "wikilink")。950年，他迫使[波希米亚公爵](../Page/捷克君主列表.md "wikilink")[波列斯拉夫一世称臣](../Page/波列斯拉夫一世.md "wikilink")。

951年奥托首次入侵[意大利](../Page/意大利.md "wikilink")，征服[伦巴第](../Page/伦巴第.md "wikilink")，自称意大利国王，并与意大利统治者洛泰尔的遗孀阿德莱德结婚。953年[洛林公爵与士瓦本公爵发动叛乱](../Page/洛林.md "wikilink")，被奥托一世迅速镇压。955年，[马扎尔人入侵](../Page/匈牙利.md "wikilink")[巴伐利亚和](../Page/巴伐利亚.md "wikilink")[士瓦本](../Page/士瓦本.md "wikilink")。奥托一世在[奥格斯堡战役中击败长期侵扰德意志的马扎尔人](../Page/第二次莱希菲尔德之战_\(955\).md "wikilink")，使之转向定居生活，建立起[匈牙利国家](../Page/匈牙利.md "wikilink")。961年，地位虚弱的[教皇](../Page/教皇.md "wikilink")[约翰十二世请求奥托一世进入意大利称王](../Page/约翰十二世.md "wikilink")。962年2月2日，约翰十二世为奥托[加冕](../Page/加冕.md "wikilink")，称颂他为“神圣罗马帝国的皇帝”，是为[神圣罗马帝国之始](../Page/神圣罗马帝国.md "wikilink")。奥托此后因意大利问题长期与[拜占廷帝国冲突](../Page/拜占廷帝国.md "wikilink")。奥托一世巧妙地利用教会力量来扼制德国诸侯，他授予教会著名的[奥托特权](../Page/奥托特权.md "wikilink")（962年），确定了教皇的世俗权力。963年奥托废黜约翰十二世，立[利奥八世为](../Page/利奥八世.md "wikilink")[教皇](../Page/教皇.md "wikilink")。这也开了由皇帝决定教皇人选的先例，成为历史上皇帝与教皇长期斗争的序幕。965年利奥八世去世后，奥托一世又指定[约翰十三世当教皇](../Page/约翰十三世.md "wikilink")，不过终于受到了罗马人的反对。奥托一世于是进军意大利，以武力扶约翰十三世上台。966年至972年，奥托一世一直为意大利问题与[拜占庭帝国处于战争状态](../Page/拜占庭帝国.md "wikilink")。972年，拜占庭帝国皇帝[约翰一世最终承认了奥托的西帝国皇帝头衔](../Page/约翰一世_\(拜占庭\).md "wikilink")。奥托一世在王权无比强大的气氛中去世。

## 家庭

  - 第一个妻子：[威塞克斯的伊迪丝](../Page/威塞克斯的伊迪丝.md "wikilink")，929年结婚
  - 第二个妻子：[勃艮第的阿德莱德](../Page/勃艮第的阿德莱德.md "wikilink")，951年结婚

### 子女

1.  [鲁道夫 (士瓦本公爵)](../Page/鲁道夫_\(士瓦本公爵\).md "wikilink")
2.  路特嘉德（萨克森的）
3.  [奥托二世](../Page/奥托二世.md "wikilink")

|-

[O](../Category/神聖羅馬皇帝.md "wikilink")
[O](../Category/东法兰克国王.md "wikilink")
[O](../Category/萨克森公爵.md "wikilink")
[O](../Category/912年出生.md "wikilink")
[O](../Category/973年逝世.md "wikilink")