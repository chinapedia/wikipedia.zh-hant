**蚕豆**（[学名](../Page/学名.md "wikilink")：**）是一年生或越年生[豆科](../Page/豆科.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")，又名**胡豆**、**佛豆**、**川豆**、**倭豆**、**罗汉豆**，原产[亚洲西南部和](../Page/亚洲.md "wikilink")[非洲北部](../Page/非洲.md "wikilink")。相传[西汉](../Page/西汉.md "wikilink")[张骞出使西域时期传入](../Page/张骞.md "wikilink")[中国](../Page/中国.md "wikilink")，8世纪左右从中国传入[日本](../Page/日本.md "wikilink")。據說為[菩提僊那渡日後交給](../Page/菩提僊那.md "wikilink")[行基種植於](../Page/行基.md "wikilink")[兵庫縣的武庫村](../Page/兵庫縣.md "wikilink")。\[1\]\[2\]\[3\]

## 形态

一年生或二年生[草本](../Page/草本.md "wikilink")；茎方形中空有棱；羽状复叶；花一至数朵腋生，白色或淡紫色花冠带有紫斑。

株高30～180厘米。[子叶不出土](../Page/子叶.md "wikilink")；主根系发达；茎四棱、中空，四角上的维管束较大，有利于直立生长。羽状复叶，顶端小叶退化呈刺状，无卷须。总状花序，每花梗一般着花2～6朵，淡紫红色、紫白色或白色的蝶形花，自下而上开花。每荚2～8粒种子，[荚果肥厚](../Page/荚果.md "wikilink")，外被细茸毛，果壁内层有海绵状茸毛；椭圆扁平的[种子](../Page/种子.md "wikilink")；乳白、资、褐或青色种皮；有大、中、小粒三个变种。

## 名称由来

[元代农学家](../Page/元代.md "wikilink")[王祯在](../Page/王祯.md "wikilink")《农书》中说：“蚕时始熟，故名”；而[明代医学家](../Page/明代.md "wikilink")[李时珍在](../Page/李时珍.md "wikilink")《食物本草》中认为：“豆荚状如老蚕，故名”。

## 用途

蠶豆做為人類栽培用糧食的時間悠久，在[西元前6000年已經在東](../Page/前6千紀.md "wikilink")[地中海區域栽種](../Page/地中海.md "wikilink")，可能是人類最早期種植的作物種類之一。豆類植物具有[固氮作用功能](../Page/固氮作用.md "wikilink")，且蠶豆可在高鹽度土壤中存活，植株也較為耐寒，可作為使用。

蠶豆可以作为[粮食](../Page/粮食.md "wikilink")、[蔬菜](../Page/蔬菜.md "wikilink")、药材、[饲料](../Page/饲料.md "wikilink")、绿肥使用。但患有[G6PD缺乏症](../Page/G6PD缺乏症.md "wikilink")（蠶豆症）者不可接觸、食用。

  - 干燥后的蚕豆可以作为粮食使用
  - 新鲜的蚕豆可以作为蔬菜，有煮、炒、做汤等多种做法
  - 入药，中医认为蚕豆性平味甘，具有益胃、利湿消肿、止血解毒的功效
  - 饲料
  - [绿肥](../Page/绿肥.md "wikilink")
  - 古希臘及羅馬認為蠶豆與[冥界有關](../Page/冥界.md "wikilink")，認為不祥而不愛食用，並將蠶豆用於[葬禮儀式](../Page/葬禮.md "wikilink")。古希臘著名數學家、哲學家[畢達哥拉斯還認為蠶豆裡有亡者的靈魂](../Page/畢達哥拉斯.md "wikilink")。

## 參考文獻

  - 《台灣蔬果實用百科第一輯》，薛聰賢 著，薛聰賢出版社，2001年

## 外部連結

  - [蠶豆，candou](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00562)
    藥用植物圖像數據庫（香港浸會大學中醫藥學院）

[Category:蠶豆](../Category/蠶豆.md "wikilink")
[Category:果菜類](../Category/果菜類.md "wikilink")
[Category:食療](../Category/食療.md "wikilink")

1.
2.
3.