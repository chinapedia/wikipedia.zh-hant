**晋安区**（[闽东语](../Page/闽东语.md "wikilink")：*Céng-ăng*）是[福建省](../Page/福建省.md "wikilink")[福州市所辖的一个区](../Page/福州市.md "wikilink")，位于市区东北部。

## 行政区划

辖3个街道、4个镇、2个乡：

  - 街道办事处：茶园街道、王庄街道、象园街道。
  - 镇：岳峰镇、鼓山镇、新店镇、宦溪镇。
  - 乡：寿山乡、日溪乡。

<!-- end list -->

  - <small> 注：2004年，原鼓岭乡并入宦溪镇，原岭头乡并入寿山乡。</small>

## 参看

  - [晋安河](../Page/晋安河.md "wikilink")
  - [涌泉寺 (福州)](../Page/涌泉寺_\(福州\).md "wikilink")

## 外部链接

  - [晋安区人民政府](http://www.jinan-fz-fj.gov.cn/)

  - [福州市郊区志](http://www.fzdqw.com/ShowBook.asp?BookType=%B8%A3%D6%DD%CA%D0_\(%CF%D8\)%CA%D0%C7%F8%D6%BE&Bookno=1115)

## 参考文献

{{-}}

[晋安区](../Category/晋安区.md "wikilink")
[区](../Category/福州区县市.md "wikilink")
[福州](../Category/福建市辖区.md "wikilink")