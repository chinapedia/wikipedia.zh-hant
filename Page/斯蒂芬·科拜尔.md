**斯蒂芬·蒂龙·科拜尔**（，，原为(科尔伯特)，）中国大陆常译为**扣扣熊**，是一位[美国的](../Page/美国.md "wikilink")[电视节目主持人和](../Page/电视节目主持人.md "wikilink")[喜剧演员](../Page/喜剧.md "wikilink")，[艾美奖获得者](../Page/艾美奖.md "wikilink")。因其[讽刺和](../Page/讽刺.md "wikilink")[扑克脸式的喜剧表演风格在美国广为人知](../Page/扑克.md "wikilink")，主持[喜剧中心频道](../Page/喜剧中心.md "wikilink")[讽刺](../Page/讽刺.md "wikilink")[新闻节目](../Page/新闻节目.md "wikilink")[科拜尔报告](../Page/科拜尔报告.md "wikilink")，科拜尔在节目中扮演[保守派](../Page/保守派.md "wikilink")[政治](../Page/政治.md "wikilink")[评论家](../Page/评论家.md "wikilink")。2015年9月8日，科拜尔接替[大卫·莱特曼接手](../Page/大卫·莱特曼.md "wikilink")[哥伦比亚广播公司旗下深夜节目時段](../Page/哥伦比亚广播公司.md "wikilink")《[史蒂芬·柯貝爾晚間秀](../Page/荷伯報到.md "wikilink")》。\[1\]

## 簡介

科拜尔从[西北大学毕业之后就从事表演工作](../Page/西北大學_\(伊利諾州\).md "wikilink")。1997年，他加入了美国[喜劇中心电视台的时政节目](../Page/喜劇中心.md "wikilink")《[乔恩·斯图尔特](../Page/乔恩·斯图尔特.md "wikilink")[每日秀](../Page/每日秀.md "wikilink")》（The
Daily Show with Jon
Stewart）。在节目中，他扮演了一个虚构的[记者角色](../Page/记者.md "wikilink")，他自己称之为“意图明确却知之甚少的高级[白痴](../Page/白痴.md "wikilink")”。

2005年，他开始主持同一所电视台的另一个节目——《[科拜尔报告](../Page/科拜尔报告.md "wikilink")》。该节目是从《[乔恩·斯图尔特](../Page/乔恩·斯图尔特.md "wikilink")[每日秀](../Page/每日秀.md "wikilink")》衍生出来的。在节目中，科拜尔通过[戏仿](../Page/戏仿.md "wikilink")[右翼人士](../Page/右翼.md "wikilink")——尤其是美国[福克斯电视台的一些时政节目主持人](../Page/福克斯电视台.md "wikilink")——的方式达到讽刺的目的。

2006年4月29日，科拜尔受邀在白宫新闻记者协会晚餐会上进行表演。科拜尔用他一贯的风格当面挖苦了[美国总统](../Page/美国总统.md "wikilink")[乔治·W·布什](../Page/乔治·W·布什.md "wikilink")，这段长达24分钟的表演让他在互联网上轰动起来，2006年和2012年科拜尔被[时代杂志评为](../Page/时代杂志.md "wikilink")100个最有影响力的人之一。\[2\]\[3\]

## 主要作品

  - 《[每日秀](../Page/每日秀.md "wikilink")》（1997年-2004年）The Daily Show
  - 《[冒险兄弟](../Page/冒险兄弟.md "wikilink")》（2003年） The Venture Bros
  - 《[科拜尔报告](../Page/科拜尔报告.md "wikilink")》（2005年-2014年）The Colbert
    Report
  - 《[巨大的惊喜](../Page/巨大的惊喜.md "wikilink")》(2005年) The Great New
    Wonderful
  - 《无人知晓》（2005年）Nobody Knows Anything
  - 《家有仙妻》（2005年）Bewitched
  - 《[有糖果的陌生人](../Page/有糖果的陌生人.md "wikilink")》（2006年）Strangers With
    Candy
  - 《[巴菲特报告](../Page/巴菲特报告.md "wikilink")》（2008年）I.O.U.S.A
  - 《[爱情大师](../Page/爱情大师.md "wikilink")》（2008年）The Love Guru
  - 《[怪兽大战外星人](../Page/怪兽大战外星人.md "wikilink")》（2009年）Monsters Vs Aliens
  - 《》（2015年）The Late Show with Stephen Colbert

## Colbert跑步机

2009年，[美国宇航局为](../Page/美国宇航局.md "wikilink")[国际空间站设计了一个新的](../Page/国际空间站.md "wikilink")[跑步机](../Page/跑步机.md "wikilink")，它被[发现号航天飞机在](../Page/发现号航天飞机.md "wikilink")2009年8月STS-128任务期间带到[国际空间站](../Page/国际空间站.md "wikilink")，这台复杂的机器现在被登上国际空间站航天员和宇航员使用每天使用约8小时，以保持他们在长期零重力环境下的肌肉质量和骨密度。[美国宇航局的工程师在其超过两年的建造时间中将其简单地称为T](../Page/美国宇航局.md "wikilink")-2。在2009年4月14日，[美国宇航局将其改名为联合操作负载外阻跑步机](../Page/美国宇航局.md "wikilink")（Combined
Operational Load-Bearing External Resistance
Treadmill），缩写即为COLBERT。\[4\]\[5\]

科拜尔敦促他的追随者在投票网站发表了名为“科拜尔”的选项，该项投票普查完成后“科拜尔”收到共计230,539票，超过第二位“宁静”约40000票。\[6\]科拜尔预计将持续的“生活”在[国际空间站](../Page/国际空间站.md "wikilink")，并将会旅行约38000英里，虽然[国际空间站将在](../Page/国际空间站.md "wikilink")2020年退休，但其有150000英里寿命，如果需要，可以继续运行到2028年或更长的时间。[宇航员](../Page/宇航员.md "wikilink")[苏尼·威廉姆斯来到](../Page/苏尼·威廉姆斯.md "wikilink")[科拜尔报告宣布](../Page/科拜尔报告.md "wikilink")[美国宇航局将他的名字给跑步机命名时](../Page/美国宇航局.md "wikilink")，科拜尔意识到他是一个非常难得的荣誉接受者，科拜尔是[美国宇航局唯一以一个活生生的人的名字命名的太空工程装备](../Page/美国宇航局.md "wikilink")。\[7\]

## 参考文献

## 參見

  - [2006年科拜尔白宫演说](../Page/2006年科拜尔白宫演说.md "wikilink")

## 外部链接

  -
  - [科拜尔报告官方网站](http://www.comedycentral.com/shows/the_colbert_report/index.jhtml)

  - [科拜尔SuperPac](http://www.colbertsuperpac.com/)

[Category:美國脫口秀主持人](../Category/美國脫口秀主持人.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:美國電視編劇](../Category/美國電視編劇.md "wikilink")
[Category:美国男配音演员](../Category/美国男配音演员.md "wikilink")
[Category:艾美獎獲獎者](../Category/艾美獎獲獎者.md "wikilink")
[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")
[Category:美國西北大學校友](../Category/美國西北大學校友.md "wikilink")
[Category:美國天主教徒](../Category/美國天主教徒.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:21世紀美國小說家](../Category/21世紀美國小說家.md "wikilink")
[Category:皮博迪獎得主](../Category/皮博迪獎得主.md "wikilink")
[Category:美國電視監製](../Category/美國電視監製.md "wikilink")
[Category:美國政治評論家](../Category/美國政治評論家.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")

1.
2.
3.
4.
5.
6.
7.