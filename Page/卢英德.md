**卢英德**，（，），又译**因德拉·努伊**，[百事公司首位亚裔女执行长](../Page/百事公司.md "wikilink")。\[1\]2018年，從百事公司退休。

## 生平

1955年生于[泰米尔纳德邦马德拉斯](../Page/泰米尔纳德邦.md "wikilink")（今[金奈](../Page/金奈.md "wikilink")），[畢業於當地](../Page/畢業.md "wikilink")[基督學院化學系](../Page/基督學院.md "wikilink")，及[耶魯大學](../Page/耶魯大學.md "wikilink")[工商管理碩士](../Page/工商管理碩士.md "wikilink")。1994年加入[百事公司工作](../Page/百事公司.md "wikilink")，2006年升任百事公司[董事長及](../Page/董事長.md "wikilink")[CEO](../Page/CEO.md "wikilink")。在2006年9月，[富比士雜誌將英德拉選為](../Page/富比士雜誌.md "wikilink")[世界上最有權力女性的第四位](../Page/世界百名權威女性.md "wikilink")。她育有兩名[女兒](../Page/女兒.md "wikilink")。\[2\]
[Indra_Nooyi_-_World_Economic_Forum_Annual_Meeting_Davos_2008.jpg](https://zh.wikipedia.org/wiki/File:Indra_Nooyi_-_World_Economic_Forum_Annual_Meeting_Davos_2008.jpg "fig:Indra_Nooyi_-_World_Economic_Forum_Annual_Meeting_Davos_2008.jpg")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:美國企業家](../Category/美國企業家.md "wikilink")
[Category:印度泰米爾人](../Category/印度泰米爾人.md "wikilink")
[Category:归化美国公民的印度人](../Category/归化美国公民的印度人.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:马德拉斯大学校友](../Category/马德拉斯大学校友.md "wikilink")
[Category:百事公司人物](../Category/百事公司人物.md "wikilink")
[Category:强生人物](../Category/强生人物.md "wikilink")
[Category:摩托罗拉人物](../Category/摩托罗拉人物.md "wikilink")
[Category:博思艾伦汉密尔顿控股公司人物](../Category/博思艾伦汉密尔顿控股公司人物.md "wikilink")
[Category:清奈人](../Category/清奈人.md "wikilink")

1.  [百事可樂首位亞裔女執行長 Indra Nooyi 印度裔
    **印德拉‧諾伊**](http://www.epochtimes.com/b5/6/8/29/n1437406.htm)
2.  [陈至立会见百事公司董事长卢英德](http://news.xinhuanet.com/politics/2009-06/30/content_11627763.htm)