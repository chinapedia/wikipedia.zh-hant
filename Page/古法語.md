**古法語**的概念包括[奧依語系的](../Page/奧依語.md "wikilink")[羅曼語族的集合](../Page/羅曼語族.md "wikilink")。通行於約10到14世紀，現今[法國北部的半數地區](../Page/法國.md "wikilink")。
現在的法語只有幾十個字來自高盧語，如果添加高盧詞源，則大約有200個。例如chêne和charrue（Delamarre，2003年，第389-90頁）。

公元前120年，在[羅馬佔領下](../Page/羅馬.md "wikilink")，拉丁語引進了法國南部。古法語的起始，是在[凱撒大帝在公元前](../Page/凱撒大帝.md "wikilink")51年帶領羅馬帝國征服高盧的時期。[普勞圖斯時代](../Page/普勞圖斯.md "wikilink")（公元前254-184年）開始，傳統[拉丁語的音韻結構不斷變化](../Page/拉丁語.md "wikilink")，最終產生通俗拉丁語，即西羅馬帝國常見的口語，這種口語和傳統拉丁語的音韻顯著不同，這是各羅曼語言的始祖，包括古法語。

## 音韻系統

### 輔音

<table>
<caption>古法語輔音</caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/雙唇音.md" title="wikilink">雙唇音</a></p></th>
<th><p><a href="../Page/唇齒音.md" title="wikilink">唇齒音-<br />
齒音</a></p></th>
<th><p><a href="../Page/齒音.md" title="wikilink">齒音</a>/<br />
<a href="../Page/齒齦音.md" title="wikilink">齒齦音</a></p></th>
<th><p><a href="../Page/齒齦後音.md" title="wikilink">齒齦後音</a>/<br />
<a href="../Page/硬顎音.md" title="wikilink">硬顎音</a></p></th>
<th><p><a href="../Page/軟齶音.md" title="wikilink">軟齶音</a></p></th>
<th><p><a href="../Page/聲門音.md" title="wikilink">聲門音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></td>
<td><p>     </p></td>
<td></td>
<td><p>     </p></td>
<td><p>     </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞音.md" title="wikilink">塞音</a></p></td>
<td><p>  </p></td>
<td></td>
<td><p>  </p></td>
<td></td>
<td><p>  </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></td>
<td></td>
<td><p>  </p></td>
<td><p>  </p></td>
<td></td>
<td></td>
<td><p>()</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></td>
<td></td>
<td></td>
<td><p>  </p></td>
<td><p>  </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邊音.md" title="wikilink">邊音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/顫音.md" title="wikilink">顫音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 單元音

<table>
<caption>古法語元音</caption>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p><a href="../Page/前元音.md" title="wikilink">前</a></p></th>
<th><p><a href="../Page/中元音.md" title="wikilink">中</a></p></th>
<th><p><a href="../Page/後元音.md" title="wikilink">後</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/閉元音.md" title="wikilink">閉</a></p></td>
<td><p><small>不帶鼻音</small></p></td>
<td><p>  </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><small>帶鼻音</small></p></td>
<td></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/半閉元音.md" title="wikilink">半閉</a></p></td>
<td><p><small>不帶鼻音</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small>帶鼻音</small></p></td>
<td></td>
<td><p>| </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/半開元音.md" title="wikilink">半開</a></p></td>
<td></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/開元音.md" title="wikilink">開</a></p></td>
<td><p><small>不帶鼻音</small></p></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><small>帶鼻音</small></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 法語使用者所認可的觀點

一般我們所聽到的出現在流行電影，法語使用者普遍認爲是“古法語”的，例如《[探訪者](../Page/探訪者.md "wikilink")》、《庸俗化》裏的，其實並不是古法語。那些講話者能夠一聽就懂的“古法語”的主要是用了些較爲老舊的拼法拼寫的[古典法語甚至現代法語](../Page/古典法語.md "wikilink")。

而未經專門學習的法語使用者是不能理解古法語的。因此，這個句子：*Sçavoir faisons, à tous presens et
advenir, que pour aucunement pourveoir au bien de nostre justice,
abbreviation des proces, et soulaigement de noz subiectz, avons, par
edict perpetuel et irrevocable, statué et ordonné, statuons et ordonnons
les choses qui s'ensuyvent* （節選自Villers-Cotterêts法令，佛朗索瓦
1539年）並不是古法語，而是[前古典法語或者說是十六世紀的](../Page/前古典法語.md "wikilink")[中世紀法語](../Page/中世紀法語.md "wikilink")。像下面這首來自[羅蘭之歌的詩文才是古法語](../Page/羅蘭之歌.md "wikilink")：

*En ceste tere ad asez osteiet / En France, ad Ais, s'en deit ben
repairer / Vos le sivrez a la feste seint Michel / Si recevrez la lei de
chrestiens / Serez ses hom par honur e par ben*

目前已知最古老的法文文獻是[斯特拉斯堡誓言](../Page/斯特拉斯堡誓言.md "wikilink")：

文獻其中一段節錄：

*Pro Deo amur et pro Christian poblo et nostro commun salvament, d'ist
di en avant, in quant Deus savir et podir me dunat, si salvarai eo cist
meon fradre Karlo, et in aiudha et in cadhuna cosa...*

第二古老的法文文獻是公元880年或881年的《》，因為這部文獻用字串法的一致性，對於構擬古法語很有幫助。

[Category:法语](../Category/法语.md "wikilink")