這是一篇關於位於台灣的[國立交通大學著名校友的列表](../Page/國立交通大學.md "wikilink")，遷台之前的著名校友請參見[交通大学校友列表](../Page/交通大学校友列表.md "wikilink")。

## 產業界

  - [施振榮](../Page/施振榮.md "wikilink") -
    電子工程學系57級，電子所58級，現任[宏碁集團董事長](../Page/宏碁.md "wikilink")。
  - [楊裕球](../Page/楊裕球.md "wikilink") -
    土木工程學系32級，曾任[林同棪國際工程公司董事長](../Page/林同棪國際工程.md "wikilink")。
  - [孔毅](../Page/孔毅.md "wikilink") -
    電子物理學系60級，[摩托羅拉公司副總裁](../Page/摩托羅拉.md "wikilink")。
  - [王章清](../Page/王章清.md "wikilink") -
    土木工程學系33級，現任[中信證券股份有限公司董事長](../Page/中信證券.md "wikilink")。
  - [胡定華](../Page/胡定華.md "wikilink") -
    電子研究所53級，曾任[旺宏電子股份有限公司董事長](../Page/旺宏電子.md "wikilink")。
  - [曹興誠](../Page/曹興誠.md "wikilink") -
    管理研究所59級，現任[聯華集團榮譽董事長](../Page/聯華集團.md "wikilink")。
  - [簡明仁](../Page/簡明仁.md "wikilink") -
    電子工程學系58級，現任[大眾電腦股份有限公司董事長](../Page/大眾電腦.md "wikilink")。
  - [林榮生](../Page/林榮生.md "wikilink") - 電子工程學系57級，現任力太電子總經理。
  - [宣明智](../Page/宣明智.md "wikilink") -
    電子工程學系62級，現任聯華電子股份有限公司副董事長、系統科技董事長。
  - [徐弘洋](../Page/徐弘洋.md "wikilink") -
    電子工程學系62級，電子研究所碩士，曾任勤益工專教師，現任逢甲大學資訊工程學系副教授。
  - [邱再興](../Page/邱再興.md "wikilink") -
    電子研究所53級，現任繼業企業股份有限公司董事長、邱再興文教基金會董事長。
  - [孫燕生](../Page/孫燕生.md "wikilink") - 電子物理學系58級，Jeecom董事長。
  - [黃河明](../Page/黃河明.md "wikilink") -
    電子工程學系59級，現任交大思源基金會董事長，曾任惠普泰國分公司總經理、惠普科技公司董事長兼總經理。
  - [林憲銘](../Page/林憲銘.md "wikilink") - 計算與控制學系64級，現任宏碁電腦公司資訊產品事業群總經理。
  - [黃炎松](../Page/黃炎松.md "wikilink") - 電子物理學系58級，電子研究所59級，現任Quickturn
    Design SystemsInc.董事長。
  - [蔡南雄](../Page/蔡南雄.md "wikilink") - 電子研究所58級，現任合晶科技董事長。
  - [葉宏清](../Page/葉宏清.md "wikilink") - 電信工程學系62級，現任新企集團，飛瑞股份有限公司董事長。
  - [曾繁城](../Page/曾繁城.md "wikilink") - 電子研究所57級，現任臺積電副總執行長。
  - [黃少華](../Page/黃少華.md "wikilink") -
    電信工程學系60級，現任宏碁集團資深副總、宏碁電腦董事、元碁公司董事長。
  - [林建昌](../Page/林建昌.md "wikilink") - 電子工程學系57級，頂林企業董事長。
  - [劉英達](../Page/劉英達.md "wikilink") -
    電子工程學系59級，電子研究所59級，現任聯華電子公司第一事業群總經理、聯瞻科技董事長。
  - [石中光](../Page/石中光.md "wikilink") - 土木工程學系37級，現任財團法人中華顧問工程司理事長。
  - [高次軒](../Page/高次軒.md "wikilink") - 電子物理學系61級，現任友訊科技股份有限公司董事長。
  - [邱羅火](../Page/邱羅火.md "wikilink") - 電信工程學系60級，現任富鑫顧問公司總經理。
  - [蔡吉春](../Page/蔡吉春.md "wikilink") -
    電信工程學系62級，英國英達科技集團董事長，英國泰爾福中文學校創校主席，蔡老齊文教公益基金會總裁。
  - [盧超群](../Page/盧超群.md "wikilink") - 現任鈺創科技公司總經理。
  - [黃顯雄](../Page/黃顯雄.md "wikilink") -
    電子物理學系60級，參加聯華電子公司建廠及生產管理，接辦勝華電子公司。
  - [朱順一](../Page/朱順一.md "wikilink") - 電子工程學系63級，現任合勤科技股份有限公司總經理。
  - [陳榮祥](../Page/陳榮祥.md "wikilink") -
    電信工程學系62級，電子研究所64華陽企業、飛瑞通信、唯新科技等公司董事長。
  - [黃洲杰](../Page/黃洲杰.md "wikilink") - 電子工程學系66級，現任凌陽科技股份有限公司董事長兼總經理。
  - [林元闓](../Page/林元闓.md "wikilink") - 電子工程學系62級，現任億訊科技公司總經理。
  - [李進洋](../Page/李進洋.md "wikilink") - 電子工程學系58級，電子研究所61級，現任晶泰科技公司董事長。
  - [邰中和](../Page/邰中和.md "wikilink") - 控制工程學系60級，旭揚理財股份有限公司及電子時報董事長。
  - [杜書伍](../Page/杜書伍.md "wikilink") - 計算與控制學系63級，現任聯強國際公司總裁兼執行長。
  - [盧宏鎰](../Page/盧宏鎰.md "wikilink") - 電子工程學系64級，現任宏碁國際公司董事長兼總經理。
  - [吳錦城](../Page/吳錦城.md "wikilink") - 電信工程學系61級，現任APPLIED HEALTH CARE
    SYSTEM執行長。
  - [蔣澤蔭](../Page/蔣澤蔭.md "wikilink") -
    電子工程學系58級，曾任[臺灣吉悌電信股份有限公司總經理](../Page/臺灣吉悌電信.md "wikilink")。
  - [黃鋕銘](../Page/黃鋕銘.md "wikilink") - 電子物理學系66級，電子研究所68，現職為嘉誠創投董事長。
  - [黃欽明](../Page/黃欽明.md "wikilink") - 電子工程學系62級，現任為致茂電子股份有限公司董事長兼總經理。
  - [黃民奇](../Page/黃民奇.md "wikilink") -
    電子物理學系63級，現任為漢民科技董事長、漢民系統董事長、漢磊半導體董事長兼總經理、漢陽半導體董事長、國際SEMI董事。
  - [許金榮](../Page/許金榮.md "wikilink") - 電子工程學系62級，光電工程研究所74級，現任為合泰半導體總經理。
  - [林寶樹](../Page/林寶樹.md "wikilink") -
    控制工程學系59級，電子研究所62級，現任為飛利浦之亞太研發基地-台北研發創新中心總經理。
  - [林坤禧](../Page/林坤禧.md "wikilink") -
    電子工程學系66級，管理科學研究所62級，現任為台積電企業發展組織資深副總經理。
  - [李廣益](../Page/李廣益.md "wikilink") - 控制工程學系66級，現任為TransMedia
    Communicationa Inc.總經理。
  - [溫清章](../Page/溫清章.md "wikilink") - 計算與控制工程系61級，電子研1級
    現任聯華電子股公司事業群總經理。
  - [張紹堯](../Page/張紹堯.md "wikilink") - 控制工程學系70級，現任冠遠科技公司總裁兼執行長。
  - [張若玫](../Page/張若玫.md "wikilink") - 控制工程學系63級，現任Vitria總裁兼總執行長。
  - [林家和](../Page/林家和.md "wikilink") - 電子工程學系59級，現任國基電子公司董事長。
  - [甘信國](../Page/甘信國.md "wikilink") - 電子工程學系58級，現任貝爾實驗室技術總監。
  - [葉茂林](../Page/葉茂林.md "wikilink") - 電信工程學系63級，現任星通資訊公司總經理。
  - [徐善可](../Page/徐善可.md "wikilink") - 管理科學學系65級，現任裕隆企業集團總管理處副執行長。
  - [林錫銘](../Page/林錫銘.md "wikilink") - 電子物理學系65級，現任偉詮電子股份有限公司董事長兼總經理。
  - [林行憲](../Page/林行憲.md "wikilink") - 電子物理學系60級，現任光寶集團執行長。
  - [林洽民](../Page/林洽民.md "wikilink") - 電子工程學系58級，現任新眾電腦股份有限公司董事長兼總經理。
  - [宋學仁](../Page/宋學仁.md "wikilink") - 管理科學學系64級，現任高盛（亞洲）責任有限公司副董事長。
  - [林文伯](../Page/林文伯.md "wikilink") - 電子物理學系62級，現任矽品精密工業股份有限公司董事長。
  - [焦佑鈞](../Page/焦佑鈞.md "wikilink") - 電信工程研究所67級，現任華邦電子董事長。
  - [陳澧](../Page/陳澧.md "wikilink") - 電子物理學系65級，現任Foundry
    Networks工程部門副總裁。
  - [楊育民](../Page/楊育民.md "wikilink") - 電子工程學系58級，現任Merck’s
    Corporative商業工程部門副總裁。
  - [成建中](../Page/成建中.md "wikilink") - 控制工程學系66級，現任Silicon
    Data總裁與執行長兼創辦人。
  - [李炎松](../Page/李炎松.md "wikilink") -
    電子工程學系60級，資工所博77級，現任[中華電信董事長](../Page/中華電信.md "wikilink")。
  - [蕭瑞洋](../Page/蕭瑞洋.md "wikilink") - 電信工程學系61級，現任來司比企業股份有限公司董事長。
  - [陳錦溏](../Page/陳錦溏.md "wikilink") - 電子工程學系61級，電子研究所61級，現任合邦電子公司總經理。
  - [羅達賢](../Page/羅達賢.md "wikilink") -
    電子工程學系65級，科管所博88級，現任工業技術研究院院長辦公室主任。
  - [鍾祥鳳](../Page/鍾祥鳳.md "wikilink") - 電子物理學系61級，現任加捷科技事業公司總經理。
  - [蔡義泰](../Page/蔡義泰.md "wikilink") -
    計算機科學系66級，計算機工程所68級，資工所博78級，現任蒙恬集團董事長。
  - [姜長安](../Page/姜長安.md "wikilink") - 電子物理學系67級，現任普誠科技董事長。
  - [林銘瑤](../Page/林銘瑤.md "wikilink") -
    電信工程學系61級,電子所63級，資工所博77級，現任緯創資通公司稽核長。
  - [王遵義](../Page/王遵義.md "wikilink") -
    電子工程學系62級，電子研究所64級，光電所博81級，現任光遠科技公司創辦人兼技術總監。
  - [王崇智](../Page/王崇智.md "wikilink") - 電信工程學系67級，現任美國網康公司(3COM)策略規劃副總裁。
  - [吳清源](../Page/吳清源.md "wikilink") - 電子物理系62級，現任大眾電信總經理。
  - [林紹胤](../Page/林紹胤.md "wikilink") -
    計算機科學系65級，計算機工程研究所67級，資訊工程博士73級，現任摩托羅拉資訊家電總經理。
  - [卓志哲](../Page/卓志哲.md "wikilink") -
    電信工程學系67級，電子研究所74級，現任聯發科技股份有限公司總經理。
  - [石靜雲](../Page/石靜雲.md "wikilink") - 應用數學系65級，現任美國SPSS公司首席統計學家及資深副總裁。
  - [拿督](../Page/拿督.md "wikilink")[潘健成](../Page/潘健成.md "wikilink") -
    控制工程學系86級，電機與控制工程研究所88級，現任群聯電子公司董事長兼總經理。
  - [葉儀晧](../Page/葉儀晧.md "wikilink") - 電子研究所73級，現任義隆電子股份有限公司董事長兼總經理。
  - [程天縱](../Page/程天縱.md "wikilink") - 電子工程學系63級，現任德州儀器亞洲區總裁。
  - [陳炫彬](../Page/陳炫彬.md "wikilink") -
    電信工程學系64級，現任友達光電總經理、威力盟董事長，明基電通董事。
  - [陳永正](../Page/陳永正.md "wikilink") - 應用數學系67級，現任微軟公司全球副總裁，大中華區首席執行官。
  - [施振強](../Page/施振強.md "wikilink") - 電信工程學系67級，現任Capella
    Microsystems總裁兼執行長。
  - [施崇棠](../Page/施崇棠.md "wikilink") - 管理科學研究所65級，現任華碩電腦股份有限公司董事長兼總經理。
  - [李鴻裕](../Page/李鴻裕.md "wikilink") - 電信工程學系69級，電信工程研究所碩77級，現任智易科技總經理。
  - [吳文燦](../Page/吳文燦.md "wikilink") - 電子工程學系71級，現任集通科技董事長及總經理。
  - [管國霖](../Page/管國霖.md "wikilink") - 電子工程學系78級，現任花旗銀行董事長。
  - [郭加泳](../Page/郭加泳.md "wikilink") -
    控制工程學系80級，互貴興業生技公司創辦人暨執行長、[交大天使投資俱樂部](http://angelclub.vc/)發起人及創始執行長。
  - [高儷玲](../Page/高儷玲.md "wikilink") -
    控制工程學系86級，[嚮網科技創辦人](../Page/嚮網科技.md "wikilink")。
  - [簡志宇](../Page/簡志宇.md "wikilink") -
    資訊工程學系92級，[無名小站創辦人](../Page/無名小站.md "wikilink")。
  - [曾皇霖](../Page/曾皇霖.md "wikilink") -
    工業工程學系92級，[痞客邦](../Page/痞客邦.md "wikilink")（PIXNET）創辦人、站長。
  - [劉昊恩](../Page/劉昊恩.md "wikilink") - 外文學系94級，痞客邦（PIXNET）創辦人。
  - [陳學群](../Page/陳學群.md "wikilink") -
    電信工程學系70級，[友立資訊創辦人及董事長](../Page/友立資訊.md "wikilink")、[友笙資訊董事長兼總經理](../Page/友笙資訊.md "wikilink")、[宏達電子研發副總經理](../Page/宏達電子.md "wikilink")。
  - [廖敏芳](../Page/廖敏芳.md "wikilink") - 友立資訊創辦人及總經理。
  - [陳偉仁](../Page/陳偉仁.md "wikilink") - 計算機工程碩士74級，友立資訊創辦人及董事長。
  - [劉啟民](../Page/劉啟民.md "wikilink") - 88級，友立資訊創辦人及總經理。
  - [呂學森](../Page/呂學森.md "wikilink") -
    交大資訊科學系80級，[中華網龍四大創辦者之一](../Page/中華網龍.md "wikilink")，現任總經理。\[1\]
  - [郝 挺](../Page/郝_挺.md "wikilink") - 交大控制工程學系66級, 聯傑國際股份有限公司董事長,
    現任交通大學校友總會理事長。

## 學術界

  - [王信文](../Page/王信文.md "wikilink") - 字銘籐，台灣
    國立交通大學管理學院資訊管理研究所，現任[國立彰化師範大學管理學院企業管理學系所教授](../Page/國立彰化師範大學管理學院企業管理學系所.md "wikilink")、[彰師大全球創意創新創業暨管理科技研究中心主任](../Page/彰師大全球創意創新創業暨管理科技研究中心.md "wikilink")、前[龍華科技大學資訊管理系系主任](../Page/龍華科技大學資訊管理系.md "wikilink")、曾榮獲財團法人白沙文教基金會學術研究獎、中華民國科技部研究獎、中華民國創育獎、雲林縣團委會傑出青年獎、雲林縣孝親獎、管理雜誌500大企管講師...等。
  - [郭南宏](../Page/郭南宏.md "wikilink") -
    電子研究所47級，前國立交通大學校長，曾任交通部部長，前[長庚大學校長](../Page/長庚大學.md "wikilink")。
  - [孔金甌](../Page/孔金甌.md "wikilink") -
    電子研究所54級，前[麻省理工學院電磁波理論研究中心主任及電機系教授](../Page/麻省理工學院.md "wikilink")。
  - [陳豫](../Page/陳豫.md "wikilink") - 電機工程學系37級，現任行政院公共工程委員會主任委員。
  - [張懋中](../Page/張懋中.md "wikilink") -
    電子研究所68級，美國UCLA電機系教授、全球聯合通訊董事長，現任國立交通大學校長。
  - [唐揆](../Page/唐揆.md "wikilink") - 管理科學系65級，現任美國普度大學史萊克講座教授兼管理學院副院長。
  - [黃廣志](../Page/黃廣志.md "wikilink") - 電子研究所56級， 現任國立高高雄科學技術學院校長。
  - [張石麟](../Page/張石麟.md "wikilink") - 電子物理學系57級，現任清大物理系教授兼研發長。
  - [王伯群](../Page/王伯群.md "wikilink") - 電機學系58級，電子工程研究所58級
    現任中山科學研究院雄風計畫主持人，中山科學研究院系統發展中心簡任副主任。
  - [易芝玲](../Page/易芝玲.md "wikilink") - 電子工程學系68級，現職交通大學電信工程系教授。
  - [張真誠](../Page/張真誠.md "wikilink") -
    計算機工程研究所博士班71級，現職[國立中正大學](../Page/國立中正大學.md "wikilink")、[逢甲大學](../Page/逢甲大學.md "wikilink")、[中國醫藥大學](../Page/中國醫藥大學.md "wikilink")、[亞洲大學講座教授](../Page/亞洲大學.md "wikilink")。
  - [劉安之](../Page/劉安之.md "wikilink") -
    交大控制工程系學士，曾任[逢甲大學校長](../Page/逢甲大學.md "wikilink")、現任[逢甲大學講座教授](../Page/逢甲大學.md "wikilink")。
  - [陳炘鈞](../Page/陳炘鈞.md "wikilink") - 管理科學系70級，現任亞利桑那大學管理資訊系統學系教授。
  - [劉文泰](../Page/劉文泰.md "wikilink") -
    電子工程學系60級，現任聖塔克魯斯大學電機工程系教授，美國國科會(NSF)仿生微電子系統工程研究中心加州大學聖塔克魯斯分部主任。
  - [張俊彥](../Page/張俊彥.md "wikilink") - 電子研究所，中央研究院院士，前國立交通大學校長。
  - [吳思華](../Page/吳思華.md "wikilink") -
    電信工程學系，前[國立政治大學校長](../Page/國立政治大學.md "wikilink")、財團法人工業技術研究院董事、行政院科技顧問。
  - [季延平](../Page/季延平.md "wikilink") - 海洋運輸學系，現任國立政治大學資訊管理學系專任副教授。
  - [林進燈](../Page/林進燈.md "wikilink") -
    交大控制工程系學士，曾任國立交通大學前任教務長，現為國立交通大學終身講座教授。
  - [鍾子平](../Page/鍾子平.md "wikilink") - 電子工程系73級，加州大學聖地牙哥分校生醫工程學系教授。
  - [柯立偉](../Page/柯立偉.md "wikilink") - 電控所博士，現任交大生資所副教授。
  - [陳振遠](../Page/陳振遠.md "wikilink") -
    管理科學系，前[國立高雄第一科技大學校長](../Page/國立高雄第一科技大學.md "wikilink")，現任[義守大學校長](../Page/義守大學.md "wikilink")
  - [謝清佳](../Page/謝清佳.md "wikilink") -
    交大管理科學系博士，曾任[國立台灣大學資訊管理系系主任兼研究所所長](../Page/國立台灣大學.md "wikilink")，副教授。

## 文化界

  - [楊德昌](../Page/楊德昌.md "wikilink") -
    控制工程學系58級，電影導演(1947年11月6日—2007年6月29日)。
  - [段鍾潭](../Page/段鍾潭.md "wikilink") -
    電子系63級，[滾石唱片創辦人](../Page/滾石唱片.md "wikilink")，總經理。
  - [雷光夏](../Page/雷光夏.md "wikilink") -
    傳播科技研究所碩士，創作歌手及[臺北愛樂電臺節目部副理](../Page/臺北愛樂電臺.md "wikilink")，節目製作、主持。
  - [蘇照彬](../Page/蘇照彬.md "wikilink") - 傳播科技研究所碩士83級，電影編劇、導演。
  - [黃國倫](../Page/黃國倫.md "wikilink") - 管理科學系，音樂製作人及詞曲創作歌手。
  - [柯景騰](../Page/柯景騰.md "wikilink")（[九把刀](../Page/九把刀.md "wikilink")） -
    管理科學系89級，網路作家。
  - [王傳一](../Page/王傳一.md "wikilink") - 土木工程學系，演員
  - [翁雋明](../Page/翁雋明.md "wikilink")（[Joeman](../Page/Joeman.md "wikilink")）
    - 電子系，台灣職業電競賽評、遊戲實況主持人、Youtuber。

## 名譽博士

  - [凌鴻勛](../Page/凌鴻勛.md "wikilink")
  - [方賢齊](../Page/方賢齊.md "wikilink")
  - [郑钧](../Page/郑钧_\(物理学家\).md "wikilink")
  - [林同棪](../Page/林同棪.md "wikilink")
  - [趙曾玨](../Page/趙曾玨.md "wikilink")
  - [孫運璿](../Page/孫運璿.md "wikilink")
  - [王安](../Page/王安.md "wikilink")
  - [李國鼎](../Page/李國鼎.md "wikilink")
  - [方復](../Page/方復.md "wikilink")
  - [高錕](../Page/高錕.md "wikilink")
  - [殷之浩](../Page/殷之浩.md "wikilink")
  - [厲鼎毅](../Page/厲鼎毅.md "wikilink")
  - [施振榮](../Page/施振榮.md "wikilink")
  - [吳大猷](../Page/吳大猷.md "wikilink")
  - [潘文淵](../Page/潘文淵.md "wikilink")
  - [鮑亦興](../Page/鮑亦興.md "wikilink")
  - [韓光渭](../Page/韓光渭.md "wikilink")
  - [楊振寧](../Page/楊振寧.md "wikilink")
  - [李遠哲](../Page/李遠哲.md "wikilink")
  - [丘成桐](../Page/丘成桐.md "wikilink")
  - [張忠謀](../Page/張忠謀.md "wikilink")
  - [沈元壤](../Page/沈元壤.md "wikilink")
  - [葛守仁](../Page/葛守仁.md "wikilink")
  - [曹興誠](../Page/曹興誠.md "wikilink")
  - [張榮發](../Page/張榮發.md "wikilink")
  - [申學庸](../Page/申學庸.md "wikilink")
  - [宣明智](../Page/宣明智.md "wikilink")
  - [胡定華](../Page/胡定華.md "wikilink")
  - [徐旭東](../Page/徐旭東.md "wikilink")
  - [苗豐強](../Page/苗豐強.md "wikilink")
  - [曾繁城](../Page/曾繁城.md "wikilink")
  - [證嚴法師](../Page/證嚴法師.md "wikilink")
  - [高行健](../Page/高行健.md "wikilink")
  - [丁肇中](../Page/丁肇中.md "wikilink")
  - [林懷民](../Page/林懷民.md "wikilink")
  - [辜振甫](../Page/辜振甫.md "wikilink")
  - [Dan Maydan](../Page/Dan_Maydan.md "wikilink")
  - [史欽泰](../Page/史欽泰.md "wikilink")
  - [白文正](../Page/白文正.md "wikilink")
  - [安藤忠雄](../Page/安藤忠雄.md "wikilink")
  - [薩支唐](../Page/薩支唐.md "wikilink")
  - [田家炳](../Page/田家炳.md "wikilink")
  - [沈呂九](../Page/沈呂九.md "wikilink")
  - [宋恭源](../Page/宋恭源.md "wikilink")
  - [Martin Gutzwiller](../Page/Martin_Gutzwiller.md "wikilink")
  - [林義守](../Page/林義守.md "wikilink")

## [國際電機電子工程學會會士](../Page/國際電機電子工程學會.md "wikilink")（IEEE Fellow）

  - [施敏](../Page/施敏.md "wikilink")
  - [張俊彥](../Page/張俊彥.md "wikilink")
  - [張逢猷](../Page/張逢猷.md "wikilink")
  - [彭松村](../Page/彭松村.md "wikilink")
  - [鄒國虎](../Page/鄒國虎.md "wikilink")
  - [李祖添](../Page/李祖添.md "wikilink")
  - [黃調元](../Page/黃調元.md "wikilink")
  - [尉應時](../Page/尉應時.md "wikilink")
  - [魏哲和](../Page/魏哲和.md "wikilink")
  - [吳重雨](../Page/吳重雨.md "wikilink")
  - [莊晴光](../Page/莊晴光.md "wikilink")
  - [李建平](../Page/李建平.md "wikilink")
  - [黃威](../Page/黃威_\(消歧義\).md "wikilink")
  - [杭學鳴](../Page/杭學鳴.md "wikilink")
  - [周景揚](../Page/周景揚.md "wikilink")
  - [曾俊元](../Page/曾俊元.md "wikilink")
  - [林一平](../Page/林一平.md "wikilink")
  - [林進燈](../Page/林進燈.md "wikilink")
  - [林盈達](../Page/林盈達.md "wikilink")
  - [莊紹勳](../Page/莊紹勳.md "wikilink")
  - [張仲儒](../Page/張仲儒.md "wikilink")
  - [方偉騏](../Page/方偉騏.md "wikilink")
  - [林寶樹](../Page/林寶樹.md "wikilink")
  - [謝漢萍](../Page/謝漢萍.md "wikilink")
  - [柯明道](../Page/柯明道.md "wikilink")
  - [王啟旭](../Page/王啟旭.md "wikilink")
  - [王蒞君](../Page/王蒞君.md "wikilink")
  - [吳炳飛](../Page/吳炳飛.md "wikilink")
  - [陳志成](../Page/陳志成.md "wikilink")
  - [曾煜棋](../Page/曾煜棋.md "wikilink")
  - [王文俊](../Page/王文俊.md "wikilink")
  - [郭浩中](../Page/郭浩中.md "wikilink")

## 參考資料

  - [國立交通大學校友名人錄](https://web.archive.org/web/20070205054751/http://www.nctu.edu.tw/alumni/person/index.html)
  - [國立交通大學公共事務委員會](https://web.archive.org/web/20070210084631/http://www.pac.nctu.edu.tw/alumni/great_alumni.php)

[\*](../Category/國立交通大學校友.md "wikilink")
[Category:台灣大專院校校友列表](../Category/台灣大專院校校友列表.md "wikilink")
[Category:中国高校校友列表](../Category/中国高校校友列表.md "wikilink")

1.  [中華網龍公司虧損
    呂學森曾被減薪](http://news.ltn.com.tw/news/business/paper/390881)