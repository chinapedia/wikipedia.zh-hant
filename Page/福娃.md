**福娃**是2008年在[北京舉行的](../Page/北京.md "wikilink")[第29屆奧運會的](../Page/2008年夏季奥林匹克运动会.md "wikilink")[吉祥物](../Page/吉祥物.md "wikilink")，设计者是[吳冠英](../Page/吳冠英.md "wikilink")、[韩美林等人](../Page/韩美林.md "wikilink")。於2005年11月11日、距離該屆奧運會開幕恰好1000天時正式發布問世。

## 象徵

福娃共有五個小夥伴，分別叫做：「福娃貝貝」、「福娃晶晶」、「福娃歡歡」、「福娃迎迎」、「福娃妮妮」，這五個名字連起來即諧音「[北京歡迎你](../Page/北京歡迎你.md "wikilink")」。福娃五夥伴分別代表著[奧運五環的](../Page/奥林匹克五环.md "wikilink")[藍](../Page/藍色.md "wikilink")、[黑](../Page/黑色.md "wikilink")、[紅](../Page/红色.md "wikilink")、[黃](../Page/黄色.md "wikilink")、[綠五色](../Page/綠色.md "wikilink")，並化裝為[鯉魚](../Page/鯉魚.md "wikilink")、[大熊貓](../Page/大熊貓.md "wikilink")、[奧林匹克聖火](../Page/奧林匹克聖火.md "wikilink")、[藏羚羊](../Page/藏羚羊.md "wikilink")、[北京燕子的形象](../Page/燕子.md "wikilink")，並飾有[中國傳統](../Page/中國.md "wikilink")[藝術的圖紋](../Page/中國藝術.md "wikilink")，各象徵「繁榮」、「歡樂」、「激情」、「健康」、「好運」，向世界傳遞友誼、和平、進取之精神及人與自然和諧相處之願望。

除奧運聖火，福娃夥伴的形象都採自中國的特有物產或[中華文化的特有符號](../Page/中華文化.md "wikilink")，並聯繫[海洋](../Page/海洋.md "wikilink")、[森林](../Page/森林.md "wikilink")、[陸地](../Page/陸地.md "wikilink")、[天空等自然要素](../Page/天空.md "wikilink")。福娃五夥伴也依次分別應和[五行中](../Page/五行.md "wikilink")[水](../Page/水.md "wikilink")、[木](../Page/木.md "wikilink")、[火](../Page/火.md "wikilink")、[土](../Page/土.md "wikilink")、[金五個元素](../Page/金.md "wikilink")。

{{\#language:{{\#language:}}}}

| 名字                                                                                                                                                                                                                                        | 性別                            | 顏色                            | 排名 | 形象                                     | 性格   | 寓意 | 代表運動                             | 來自[中華文化的靈感](../Page/中華文化.md "wikilink")                                                                        |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- | ----------------------------- | -- | -------------------------------------- | ---- | -- | -------------------------------- | -------------------------------------------------------------------------------------------------------------- |
| **貝貝**                                                                                                                                                                                                                                    | [女](../Page/女性.md "wikilink") | [藍](../Page/藍色.md "wikilink") | 4  | [鯉魚](../Page/鯉魚.md "wikilink")         | 溫柔純潔 | 繁榮 | 水上項目                             | [中國傳統](../Page/中國.md "wikilink")[年畫](../Page/年畫.md "wikilink")「連年有魚」與[新石器時代的魚紋圖案](../Page/新石器時代.md "wikilink") |
| 在[中国传统文化中](../Page/中國文化.md "wikilink")，鱼和水代表丰收、富足。「鯉魚跳龍門」則表示事業有成、美夢成真。「魚」跟「餘」同音，有「吉慶有餘」、「年年有餘」之意。贝贝的头部纹饰是[新石器时代](../Page/新石器时代.md "wikilink")[半坡遗址中出现的鱼纹图案](../Page/半坡遗址.md "wikilink")。另根據[吳冠英所述](../Page/吳冠英.md "wikilink")，貝貝在福娃中排行老四。 |                               |                               |    |                                        |      |    |                                  |                                                                                                                |
| **晶晶**                                                                                                                                                                                                                                    | [男](../Page/男性.md "wikilink") | [黑](../Page/黑色.md "wikilink") | 2  | [大熊貓](../Page/大熊貓.md "wikilink")       | 憨厚樂觀 | 歡樂 | 力量項目                             | 大熊貓與[宋朝](../Page/宋朝.md "wikilink")[瓷器的](../Page/瓷器.md "wikilink")[蓮花造型](../Page/蓮花.md "wikilink")              |
| 大熊貓是中國國寶。晶晶來自森林，象征人与自然的和谐共存。他的头部纹饰源于宋朝瓷器上常有的莲花瓣形。另根據[吳冠英所述](../Page/吳冠英.md "wikilink")，晶晶在福娃中排行老二。                                                                                                                                        |                               |                               |    |                                        |      |    |                                  |                                                                                                                |
| **歡歡**                                                                                                                                                                                                                                    | [男](../Page/男性.md "wikilink") | [紅](../Page/红色.md "wikilink") | 1  | [奧林匹克聖火](../Page/奧林匹克聖火.md "wikilink") | 外向奔放 | 熱情 | 球類項目                             | [敦煌壁畫的](../Page/敦煌壁畫.md "wikilink")[火焰紋樣](../Page/火焰.md "wikilink")                                            |
| 歡歡象征着运动的激情，传递「更快、更高、更強」的[奥林匹克精神以及北京奧運會對世界的熱情](../Page/奥林匹克运动会.md "wikilink")。他的头部纹饰源于[敦煌壁画](../Page/敦煌.md "wikilink")。另根據[吳冠英所述](../Page/吳冠英.md "wikilink")，歡歡是福娃家族中的老大。                                                                  |                               |                               |    |                                        |      |    |                                  |                                                                                                                |
| **迎迎**                                                                                                                                                                                                                                    | [男](../Page/男性.md "wikilink") | [黃](../Page/黄色.md "wikilink") | 3  | [藏羚羊](../Page/藏羚羊.md "wikilink")       | 機敏靈活 | 健康 | [田徑項目](../Page/田徑.md "wikilink") | 藏羚羊與[藏族繪畫](../Page/藏族.md "wikilink")                                                                           |
| 藏羚羊是[青藏高原特有的保护动物](../Page/青藏高原.md "wikilink")，机敏灵活、驰骋如飞。他的頭部紋飾有[藏族的装饰风格](../Page/藏族.md "wikilink")。另根據[吳冠英所述](../Page/吳冠英.md "wikilink")，迎迎在福娃中排行老三。                                                                                      |                               |                               |    |                                        |      |    |                                  |                                                                                                                |
| **妮妮**                                                                                                                                                                                                                                    | [女](../Page/女性.md "wikilink") | [綠](../Page/綠色.md "wikilink") | 5  | [北京燕子](../Page/燕子.md "wikilink")       | 天真歡快 | 好運 | [體操項目](../Page/體操.md "wikilink") | 燕子與沙燕[風箏](../Page/風箏.md "wikilink")                                                                            |
| 妮妮来自天空，是一只展翅飞翔的燕子，其造型创意来自北京传统的沙燕风筝。“燕”还代表燕京（古代北京的称谓）。妮妮把春天和喜悦带给人们，飞过之处播撒“祝您好运”的美好祝福。另根據[吳冠英所述](../Page/吳冠英.md "wikilink")，妮妮是福娃家族中的小妹。                                                                                                    |                               |                               |    |                                        |      |    |                                  |                                                                                                                |

**五個福娃介紹**

## 外界评论

福娃吉祥物雖然都採用中國娃娃的形式，卻共有五個形體，此數目之多創下[奧運會吉祥物歷史上的紀錄](../Page/奧運會吉祥物.md "wikilink")。历史上只有2000年[悉尼奧運會](../Page/2000年夏季奥林匹克运动会.md "wikilink")、2004年[雅典奧運會](../Page/2004年夏季奥林匹克运动会.md "wikilink")、1988年[卡尔加里冬奧會](../Page/1988年冬季奧林匹克運動會.md "wikilink")、1994年[利勒哈默尔冬奧會](../Page/1994年冬季奧林匹克運動會.md "wikilink")、1998年[長野冬奧會的吉祥物都有多個形象](../Page/1998年冬季奧林匹克運動會.md "wikilink")。

自吉祥物出臺后，不少市民前往搶購，造成部分地区吉祥物供不應求甚至缺貨。但也有不少负面意见認為吉祥物有五個数量太多，設計也過於花哨，或缺乏美感。

### 正面

  -
### 反面

  - 部分网民认为五个福娃太多，而且福娃的价格偏高，普通消费者实在难以承受，更有国外新闻机构评论中国的福娃“铜臭味”过重。\[1\]

## 惡搞

隨著福娃的影響日增與北京奧運日近，眾多福娃的惡搞版本亦出爐，大部分都是改編自動漫人物，包括葫蘆娃\[2\]；黑貓警長\[3\]；哆啦A夢\[4\]；[天線寶寶](../Page/天線寶寶.md "wikilink")；[Keroro軍曹以及](../Page/Keroro軍曹.md "wikilink")[福娃八人版等](../Page/福娃八人版.md "wikilink")。

在「福娃八人版」中，除了原有的五隻福娃外，還增加了头饰为鸭子的“丫丫”、头饰为蜻蜓的“婷婷”和头饰为[出租车](../Page/出租车.md "wikilink")（的士）的“的的”。与前五个福娃连在一起，谐音为京式[粗口](../Page/粗口.md "wikilink")“北京欢迎你丫挺的”。\[5\]

2008年3月21日，[香港](../Page/香港.md "wikilink")[支聯會利用福娃改頭換名以爭取平反](../Page/支聯會.md "wikilink")[六四事件](../Page/六四事件.md "wikilink")。\[6\]

另外，工會組織[公平奧運2008亦把該吉祥物改編以爭取平等勞工待遇](../Page/公平奧運2008.md "wikilink")。\[7\]\[8\]\[9\]

## 動畫製作

2007年10月1日，[中國中央電視台聯同](../Page/中國中央電視台.md "wikilink")[中共深圳市委宣傳部以及](../Page/中共深圳市委.md "wikilink")[深圳市凤凰星影视传媒有限公司製作的同名動畫系列正式播映](../Page/深圳市凤凰星影视传媒有限公司.md "wikilink")；首先是《[福娃之貝貝的諾言](../Page/福娃之貝貝的諾言.md "wikilink")》。陣容上起用了多位著名藝人——包括[趙薇](../Page/趙薇.md "wikilink")、[王志文](../Page/王志文.md "wikilink")、[蒋雯丽](../Page/蒋雯丽.md "wikilink")、[姚晨](../Page/姚晨.md "wikilink")、[王姬](../Page/王姬.md "wikilink")、[刘纯燕](../Page/刘纯燕.md "wikilink")、[董浩等人作配音演員](../Page/董浩.md "wikilink")；同時他們邀請了[郑渊洁擔任編劇兼導演](../Page/郑渊洁.md "wikilink")。可是，由於編劇犯下了多項錯誤（包括角色編排不平衡、故事偏向神話色彩），結果收視率一般，更令郑渊洁引咎辭職。中國中央電視台為了收復失地，決定製作該系列的第二作《[福娃五連環](../Page/福娃五連環.md "wikilink")》。

與此同時，是由[北京電視台聯同旗下的](../Page/北京電視台.md "wikilink")[北京卡酷动画衛視以及](../Page/北京卡酷动画衛視.md "wikilink")[北京水晶石影視動畫科技有限公司製作的](../Page/北京水晶石影視動畫科技有限公司.md "wikilink")《[福娃奧運漫遊記](../Page/福娃奧運漫遊記.md "wikilink")》就獲得了正面評價。同樣起用了多位著名藝人——包括香港人熟悉的[張國立](../Page/張國立.md "wikilink")、[陶虹](../Page/陶虹.md "wikilink")、[梅婷](../Page/梅婷.md "wikilink")、[金海心](../Page/金海心.md "wikilink")、[張靚穎](../Page/張靚穎.md "wikilink")、[曹穎](../Page/曹穎.md "wikilink")、[潘粵明](../Page/潘粵明.md "wikilink")、同時負責音樂一職的[龔格爾](../Page/龔格爾.md "wikilink")、[張楨](../Page/張楨.md "wikilink")、身兼總導演的[曾傳京和](../Page/曾傳京.md "wikilink")[陸建藝擔任配音演員](../Page/陸建藝.md "wikilink")；同時他們還邀請了著名編劇[鄒靜之擔任此職](../Page/鄒靜之.md "wikilink")。故事以輕鬆的手法表達了奧運會的歷史和運動員的奮鬥故事，以及角色間的微妙關係。由於這套動畫符合了公眾的需求，因此收視率節節上升，更獲得了多個獎項。同時，在2008年6月24日更被[中国电影博物馆收入馆藏](../Page/中国电影博物馆.md "wikilink")。\[10\]

## 英文名稱

2006年10月17日，[北京奥组委因受到](../Page/北京奥组委.md "wikilink")[兰州大学资环学院一位姓李的博士和多位網民的質疑而靜悄悄地將福娃的英語名称从](../Page/兰州大学.md "wikilink")「Friendlies」改为[汉语拼音的](../Page/汉语拼音.md "wikilink")「Fuwa」。由于“Friendlies”容易被解读为“friend
lies”（朋友的谎言），再加上“Friendlies”和“friendless”（没有朋友的）发音相近，故作出调整\[11\]\[12\]\[13\]\[14\]

## 注释

## 参见

  - [奧林匹克運動會吉祥物列表](../Page/奧林匹克運動會吉祥物列表.md "wikilink")
  - [吉祥物](../Page/吉祥物.md "wikilink")
  - [北奧戰隊](../Page/北奧戰隊.md "wikilink")
  - [福牛樂樂](../Page/福牛樂樂.md "wikilink")

## 外部链接

  - [官方网站](https://web.archive.org/web/20051124013104/http://www.beijing2008.com/48/01/column211990148.shtml)
  - [公平奧運2008官方網頁](http://www.playfair2008.org/index.php?lang=utf-8)
  - [中國中央電視台：福娃動畫](http://edu.cctv.com/special/C19349/02/index.shtml)
  - [中國北京電視台：福娃奧運漫遊記](http://www.btv.com.cn/btvweb/cyzj/fwindexnew.htm)
  - [搜狐娛樂：福娃奧運漫遊記](http://yule.sohu.com/s2007/fwaymyj)
  - [北京文網：福娃奧運漫遊記](http://www.beijingww.com/5/2007/08/29/Zt82@36017.htm)

[Category:2008年夏季奧林匹克運動會](../Category/2008年夏季奧林匹克運動會.md "wikilink")
[Category:奧林匹克運動會吉祥物](../Category/奧林匹克運動會吉祥物.md "wikilink")

1.  [“福娃之父”韩美林：福娃卖得太贵_网易新闻中心](http://news.163.com/08/0308/08/46GG8MMF0001124J.html)
2.  <http://www.cjn.cn/news/shtp/200611/t230034.htm>
3.  <http://www.cjn.cn/news/shtp/200611/t230034_1.htm>
4.  <http://www.cjn.cn/news/shtp/200611/t230034_5.htm>
5.  <http://edu.qq.com/a/20071106/000183.htm>
6.  [明報：支聯會奧運概念悼六四](http://hk.news.yahoo.com/080321/12/2r1zw.html)
    2008年3月22日
7.  [蘋果日報：生產 NIKE Adidas New
    Balance運動鞋　裕元被指剝削中國勞工](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20080422&sec_id=4104&subsec_id=11867&art_id=11018115)
    2008年4月22日
8.  [明報：裕元被轟血汗鞋廠 國際勞團組「公平奧運」
    關注工人權益](http://hk.news.yahoo.com/article/080421/4/6er1.html)
    2008年4月22日
9.  [明報：維權福娃 爭勞工權益](http://hk.news.yahoo.com/article/080421/4/6er2.html)
    2008年4月22日
10. [北京商報：《福娃奥运漫游记》收入中国电影博物馆馆藏](http://www.bbtnews.com.cn/dmzk/channel/political50065.shtml)
11. [新華網：“福娃”国际译名是否恰当？
    网友提出新建议](http://news.xinhuanet.com/olympics/2005-11/21/content_3811630.htm)
12. [新華網：福娃国际译名正式更名
    新华网民最早曾提建议](http://news.xinhuanet.com/sports/2006-10/16/content_5207039.htm)
13. [新華網：北京奥运福娃英文名正式更改为汉语拼音“Fuwa”](http://news.xinhuanet.com/politics/2006-10/16/content_5207227.htm)
14. [博闻网
    福娃是如何出炉的？](http://olympics.bowenwang.com.cn/olympic-mascot-fuwa1.htm)