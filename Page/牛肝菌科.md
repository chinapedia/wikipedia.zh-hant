**牛肝菌科**（[学名](../Page/学名.md "wikilink")：）是[担子菌门下](../Page/担子菌门.md "wikilink")[伞菌目的一科](../Page/伞菌目.md "wikilink")。担子果肉质，孔状。约50余种，很多种可食用。世界性分布，在湿热带雨季常见于林中。

下分：

  - *Accomodates*
  - *Afroboletus*
  - *Aureoboletus*
  - [南牛肝菌屬](../Page/南牛肝菌屬.md "wikilink") *Austroboletus*
  - [条孢牛肝菌属](../Page/条孢牛肝菌属.md "wikilink") *Boletellus*
  - [大孔牛肝菌属](../Page/大孔牛肝菌属.md "wikilink") *Borofutus*\[1\]
  - *Boletochaete*
  - [牛肝菌屬](../Page/牛肝菌屬.md "wikilink") *Boletus*
  - *Chalciporus*
  - [叶腹菌属](../Page/叶腹菌属.md "wikilink") *Chamonixia*
  - *Fistulinella*
  - *Fuscoboletinus*
  - *Gastroboletus*
  - *Buchwaldoboletus*
  - *Gastroleccinum*
  - *Gastrotylopilus*
  - *Gyrodon*
  - *Gyroporus*
  - *Heimioporus*
  - *Leccinellum*
  - [疣柄牛肝菌屬](../Page/疣柄牛肝菌屬.md "wikilink") *Leccinum*
  - *Mucilopilus*
  - *Paragyrodon*
  - *Paxillogaster*
  - *Phlebopus*
  - *Phylloboletellus*
  - *Phylloporus*
  - *Pulveroboletus*
  - *Retiboletus*
  - *Rhizopogon*
  - *Royoungia*
  - *Setogyroporus*
  - *Singeromyces*
  - *Sinoboletus*
  - [松塔牛肝菌属](../Page/松塔牛肝菌属.md "wikilink") *Strobilomyces*
  - [粘蓋牛肝菌屬](../Page/粘蓋牛肝菌屬.md "wikilink") *Suillus*
  - *Tuboseta*
  - [粉孢牛肝菌屬](../Page/粉孢牛肝菌屬.md "wikilink") *Tylopilus*
  - *Veloporphyrellus*
  - [金孢牛肝菌屬](../Page/金孢牛肝菌屬.md "wikilink") *Xanthoconium*
  - [絨蓋牛肝菌屬](../Page/絨蓋牛肝菌屬.md "wikilink") *Xerocomus*

## 参考文献

[牛肝菌科](../Category/牛肝菌科.md "wikilink")

1.