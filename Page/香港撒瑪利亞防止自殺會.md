{{ Otheruses|subject=**香港撒瑪利亞防止自殺會**|other=另一個國際的志願機構|撒瑪利亞會}}
**香港撒瑪利亞防止自殺會**（**The Samaritan Befrienders Hong
Kong**，簡稱**SBHK**），原名**防止自殺會**，是[香港的一個](../Page/香港.md "wikilink")[志願服務機構](../Page/志願服務機構.md "wikilink")，為意圖[自殺的人提供輔導服務](../Page/自殺.md "wikilink")，是[亞洲第一個同類型組織](../Page/亞洲.md "wikilink")。

雖然機構採用了《[聖經](../Page/聖經.md "wikilink")》中[好撒瑪利亞人的故事而命名](../Page/好撒馬利亞人.md "wikilink")，但其實它本身並無宗教立場。香港撒瑪利亞防止自殺會是本土團體，有別為於英國創立的「[撒瑪利亞會](../Page/撒瑪利亞會.md "wikilink")」（Samaritans）。

## 歷史

[TheSamaritanBefriendersHongKong_PakTin.jpg](https://zh.wikipedia.org/wiki/File:TheSamaritanBefriendersHongKong_PakTin.jpg "fig:TheSamaritanBefriendersHongKong_PakTin.jpg")
香港撒瑪利亞防止自殺會於1960年7月成立，創辦人[杜學魁](../Page/杜學魁.md "wikilink")，原名「防止自殺會」（Suicide
Prevention
Society），在[山林道展開電話輔導服務](../Page/山林道.md "wikilink")。於1963年10月註冊成為社團，易名為「香港撒瑪利亞會」（The
Hong Kong
Samaritans）。1965年因會址拆遷獲[政府撥租](../Page/港英政府.md "wikilink")[老虎岩區第](../Page/樂富.md "wikilink")13座45號（即[樂富邨](../Page/樂富邨.md "wikilink")13座）繼續服務。1975年於[港島](../Page/港島.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[軒尼詩道](../Page/軒尼詩道.md "wikilink")「[水手館](../Page/衛蘭軒.md "wikilink")」開設第二辦事處，提供英語服務。1976年12月更改成現名「香港撒瑪利亞防止自殺會」（The
Samaritan Befrienders Hong
Kong）。1977年11月成為[公益金會員機構](../Page/公益金.md "wikilink")。1978年7月獲[瑪利諾牧民中心以象徵式](../Page/瑪利諾牧民中心.md "wikilink")10元租金撥租[何文田](../Page/何文田.md "wikilink")[公主道牧民中心地下](../Page/公主道.md "wikilink")，何文田會所正式啟用（直到1980年2月因人力資源而結束）。1981年5月將中、英文部分開為兩個分會。1982年6月因行政關係刪除英文部。1985年11月遷入[九龍](../Page/九龍.md "wikilink")[順利邨](../Page/順利邨.md "wikilink")。於1994年轉為註冊公司，並成功獲豁免捐款稅。同年在[社會福利署協助下獲](../Page/社會福利署.md "wikilink")[房屋署撥出](../Page/房屋署.md "wikilink")[彩虹邨金華樓地下作為新會所](../Page/彩虹邨.md "wikilink")、職員辦公室及熱線服務，而順利邨會所則繼續仍作熱線服務及訓練之用。

2002年3月，有見自殺個案日益增加，而社會上又缺乏有關預防自殺及生命教育的服務資源，香港撒瑪利亞防止自殺會獲得香港獎券基金的撥款資助，成立「自殺危機處理中心」，為面臨高、中度危機的人士提供24小時的危機介入或深入輔導的服務。同期，香港撒瑪利亞防止自殺會獲香港賽馬會慈善信託基金贊助，成立「生命教育中心」，向香港市民推廣預防自殺及珍惜生命的訊息。同年10月因租務需要，位於順利邨之熱線訓練中心遷往白田中心繼續進行義工訓練工作，裝修完畢，「自殺危機處理中心」及「生命教育中心」正式投入服務。2002年12月為加強三間中心的形象的獨特性，增添了三款徽號：分別是「Grow」、「Touch」及「Alive」，識別各中心不同服務之性質。

2005年，撒瑪利亞防止自殺會表示，他們接到的求助個案當中，77%求助者年屆20至44歲，主要受事業和感情上的困擾。自2005年起，有經濟困難的求助者漸趨下降，反而受工作壓力和待遇困擾的人士，比往年上升逾10%，反映部份[香港人的工作壓力沉重](../Page/香港.md "wikilink")。2005年向該會求助的個案超過9400宗\[1\]，比2004年增加20%。

2018年，熱線中心的義工接受訪問時表示，活在社會上無權無勢的一群，有時連自己的權益都不知道，沒有接收資訊的渠道，也沒有人知道他們的存在，而現在社會上有很多人都有不同的需要，但他們不知道如何去尋找所需的資源，甚至不知道有這些資源的存在，慢慢就迫到這些人到牆角，會讓人覺得很無助。亦總結出，做接線義工要有熱心，但也要有冷靜的頭腦\[2\]。

### 中心 / 基金

[sbhk.gif](https://zh.wikipedia.org/wiki/File:sbhk.gif "fig:sbhk.gif")

  - 生命教育中心：[九龍](../Page/九龍.md "wikilink")[白田邨](../Page/白田邨.md "wikilink")
  - 熱線中心：九龍[彩虹邨](../Page/彩虹邨.md "wikilink")
  - 自殺危機處理中心：九龍白田邨
  - 關懷身邊人基金：九龍白田邨，於1994年成立，用作資助「關懷身邊人，從聆聽開始」推廣計劃。

## 參考資料

## 外部連結

  - [香港撒瑪利亞防止自殺會官方網頁](http://www.sbhk.org.hk)
  - [公益金：香港撒瑪利亞防止自殺會簡介](https://web.archive.org/web/20060518061422/http://www.commchest.org/chi/agency.cfm?no=259)
  - [facebook：香港撒瑪利亞防止自殺會推廣及宣傳專頁](https://web.archive.org/web/20160119065024/https://www.facebook.com/sbhk.org)

[Category:香港慈善團體](../Category/香港慈善團體.md "wikilink")
[Category:香港公益金會員社會福利機構](../Category/香港公益金會員社會福利機構.md "wikilink")
[Category:facebook](../Category/facebook.md "wikilink")

1.  港自殺率17.2%全球居冠, 大陸新聞, 中央日報, 2005-06-20
2.  [【我們談自殺 6】保持通話
    撒瑪利亞防止自殺會義工︰對求助者多些耐性少些批判](https://bkb.mpweekly.com/cu0001/20180119-64677)