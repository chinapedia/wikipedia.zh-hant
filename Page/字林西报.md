[North_China_Daily_News.jpg](https://zh.wikipedia.org/wiki/File:North_China_Daily_News.jpg "fig:North_China_Daily_News.jpg")
《**字林西报**》（**），又稱《字林報》，前身为《**北華捷報**》(*North China
Herald*)，曾经是在[中国出版的最有影响的一份](../Page/中国.md "wikilink")[英文](../Page/英文.md "wikilink")[报纸](../Page/报纸.md "wikilink")。

## 历史

《北华捷报》由英国侨民[奚安门](../Page/奚安门.md "wikilink")（Henry
Shearman）于1850年8月30日创刊于[上海花园弄](../Page/上海.md "wikilink")（今[南京东路](../Page/南京东路.md "wikilink")[江西中路以东路段](../Page/江西中路.md "wikilink")），是上海境内第一份近代意义上的报纸。初为每周六出版，每星期对开一张，共四页。当时在华的英国侨民称广东为南华，称长江流域及以北为北华。最初为周报，内容有英国快讯、上海英侨动态、中外商务情报、广告及船期公告等。

报馆于1856年增出日刊《每日航运新闻》(*Daily Shipping News*)。1862年改名为《每日航运和商业新闻》(*Daily
Shipping and Commercial
News*，一译《航务商业报》)。1864年6月1日，出版人[字林洋行将原来](../Page/字林洋行.md "wikilink")《北华捷报》的副刊《船务商业日报》改为《字林西报》，原来的《北华捷报》周报改为副刊，偏重于时事政治，随《字林西報》贈送。

亨利·奚安门(Henry Shearman)创办人兼第一任主笔，此后继任者有康普顿(C. S. Compton)、马诗门(S.
Mossan)、詹美森(R. A. Jamieson)、盖德润(R. S. Gandry)、海单(G. W. Haden)、巴尔福(F.
H. Balfour)、麦克李兰(J. W. Maclellan)、李德尔(R. W. Little)、毕尔(H. T. Bell)、葛林(O.
M.
Green)等，均为在华商人。該報主筆[李德立曾三次當選](../Page/李德立.md "wikilink")[上海公共租界工部局董事](../Page/上海公共租界.md "wikilink")。

1901年，上海大地产商[马立斯以](../Page/马立斯.md "wikilink")[外滩17号地块为资本入股报社](../Page/外滩17号.md "wikilink")，占有全部股份的47%，担任董事长。由于《字林西报》办得相当成功，1921年—1924年，将原有3层砖木结构房屋拆除，翻建成9层花岗石外墙的钢筋混凝土结构大厦[字林大楼](../Page/字林大楼.md "wikilink")，当时曾是上海最高建筑。建成后，一部分由报社自用，其他出租，其中最著名的承租人是处于创业阶段的美资[友邦人寿保险公司](../Page/友邦人寿保险公司.md "wikilink")。

1951年3月31日，《字林西报》停刊，結束了長達百年的發行歷史。大楼也被[中华人民共和国](../Page/中华人民共和国.md "wikilink")[上海市人民政府接管](../Page/上海市人民政府.md "wikilink")。1998年美国友邦保险有限公司上海分公司获得该楼房的使用权，在上海人们称此楼为“友邦大厦”。

## 特点

《北华捷报》与《字林西报》有一支很强的作者队伍和通讯员队伍，其中包括长期在华活动的传教士，如[麦都思](../Page/麦都思.md "wikilink")、[裨治文](../Page/裨治文.md "wikilink")、[玛高温](../Page/玛高温.md "wikilink")、[韦烈亚力](../Page/韦烈亚力.md "wikilink")、[艾约瑟等](../Page/艾约瑟.md "wikilink")，内容丰富，常刊有关中国的政治、经济、文化、社会等信息，受中外人士的重视，中国不少官员，包括[李鸿章在内](../Page/李鸿章.md "wikilink")，都非常注意他们的报道和言论动向。

该报后来成为上海最有影响的英文报纸，大到[工部局和](../Page/工部局.md "wikilink")[英国领事馆的公告](../Page/英国驻上海总领事馆.md "wikilink")，小到外商企业的遗失声明，都必须在该报上刊登才算生效。因此该报是記錄上海和研究上海歷史最重要的資料之一。\[1\]

## 外部链接

  - [报纸档案：《字林西报》](https://newspaperarchive.com/chn/shanghai/shanghai/north-china-herald/)

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 参考条目

  - [中国新闻史](../Page/中国新闻史.md "wikilink")
  - [申报](../Page/申报.md "wikilink")
  - [大美晚报](../Page/大美晚报.md "wikilink")
  - [上海犹太纪事报](../Page/上海犹太纪事报.md "wikilink")

[Category:清朝英文报纸](../Category/清朝英文报纸.md "wikilink")
[Category:中华民国大陆时期英文报纸](../Category/中华民国大陆时期英文报纸.md "wikilink")
[Category:中华人民共和国已停刊的英文报纸](../Category/中华人民共和国已停刊的英文报纸.md "wikilink")
[Category:上海已停刊报纸](../Category/上海已停刊报纸.md "wikilink")

1.  Xiaoqun Xu. *Chinese Professionals and the Republican State: The
    Rise of Professional Associations in Shangahai, 1912–1937* Cambridge
    University Press, 2001. p. 45. ISBN 0-521-78071-3