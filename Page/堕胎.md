**墮胎**（abortion），又稱**人工流產**、**人流**、**中止懷孕**、**終止懷孕**、**刮宫**、**終止妊娠**、**誘導性流產**（Induced
abortion），俗稱打胎、落仔、夾娃娃，指用藥物或手術以人工方法終止[懷孕狀態](../Page/懷孕.md "wikilink")。在[胚胎或](../Page/胚胎.md "wikilink")[胎兒自己能自行存活的時候做類似的程序](../Page/胎兒.md "wikilink")，在醫學上稱為「晚期中止妊娠」（late
termination of pregnancy, LTOP）或晚期墮胎（late-term abortion）\[1\]。
相對的，自然發生的中止懷孕現象則通常稱為[流產](../Page/流產.md "wikilink")（miscarriage）。

根據估計，每年全球墮胎案例有4400萬例，將近一半是在不安全的情況下施行\[2\]。据不完全统计，中国由于以往進行的[一孩政策](../Page/一孩政策.md "wikilink")（每對夫婦只容許有一個子女），中国每年人工流产至少1300万例，位居世界第一\[3\]。在接受了幾十年重視[家庭計畫的教育](../Page/家庭計畫.md "wikilink")，和[生育控制及](../Page/生育控制.md "wikilink")[避孕技術提升之後](../Page/避孕.md "wikilink")，墮胎率在2003年到2008年之間有微幅下降\[4\]\[5\]。截至2008年，全世界有40%的女性可以在「無理由限制」的情況下合法進行人工流產\[6\]。然而，懷孕之後何時可以進行人工流產的限制仍然存在\[7\]。

關於墮胎的法律規範、執行的頻率、和它背後的文化種族意涵在世界各處都不盡相同。在中国，人们对堕胎的反对较受[基督教教義影響的西方国家低](../Page/基督教.md "wikilink")，在[中国计划生育政策的背景下](../Page/中国计划生育政策.md "wikilink")，很多人采取自愿堕胎，部分是嬰兒的性別因素（[重男輕女](../Page/重男輕女.md "wikilink")）也有人被[计生部门实行强制堕胎](../Page/中华人民共和国国家人口和计划生育委员会.md "wikilink")（如殘疾等）\[8\]\[9\]\[10\]\[11\]。

墮胎在某些特殊情況下是完全合法的，例如因[亂倫](../Page/近親性交.md "wikilink")、[強姦而懷孕](../Page/強姦.md "wikilink")、[胎兒本身有異常](../Page/先天性障碍.md "wikilink")、社會經濟因素、或是危及母體健康\[12\]。世界上許多地方，關於墮胎的道德、倫理以及合法性等議題仍存在公開而激烈的。反對墮胎者大多認為胚胎或胎兒為具有[生存權的人類](../Page/生存權.md "wikilink")，墮胎等同於[謀殺](../Page/謀殺.md "wikilink")\[13\]\[14\]。而支持的人則強調[女性有決定自己人身健康的權利](../Page/生育权.md "wikilink")\[15\]，以及強調普遍的[人權](../Page/人權.md "wikilink")\[16\]。9月28日是“”（International
Safe Abortion Day）\[17\]。

## 原因

某些國家須要條件和證明墮胎才合法，條件如

1.  孕婦生命或健康：繼續懷孕有損孕婦生命或健康
2.  胎兒生命或健康：胎兒身體殘缺或健康有損、無法生存、無法出生、有遺傳病
3.  懷孕方式：強姦或亂倫
4.  身分：孕婦未成年或單身
5.  經濟：懷孕會令婦女的工作或學業被強制或中止不能負擔撫養孩子的金錢時間
6.  感情：與胎兒生父的感情有變
7.  家庭：家人反對，丈夫或家長
8.  生育控制：不希望生孩子或中國的兩孩証策

需要警方或醫生證明。

  - 遺傳病

遺傳病可用產前儉查，超音波儉查、羊水檢查、血液檢查。常見遺傳病為[唐氏綜合症](../Page/唐氏綜合症.md "wikilink")、[無腦症](../Page/無腦症.md "wikilink")、[茲卡病毒](../Page/茲卡病毒.md "wikilink")

婦女選擇墮胎的原因很多。有可能是為了控制生育，或婦女因為身體狀況而無法承受懷孕，还或因胎儿被诊断为畸形或患有先天性疾病而导致雙親不想生下有缺憾的胎兒等，也存在因與胎兒生父的感情有變，因而希望中止懷孕的例子。
根據[美國](../Page/美國.md "wikilink")[Alan Guttmacher
Institute於](../Page/Alan_Guttmacher_Institute.md "wikilink")2000年所作的統計，美國在2000年總共進行過131萬次墮胎手術，當中有1%與因姦成孕或亂倫有關。
以下另有一個從27個不同國家作出的墮胎報告\[18\]，按原因的高低作排列如下：

  - 27.6% –希望延遲撫養孩子的時間
  - 21.3% –不能負擔撫養孩子的金錢
  - 14.1% –與胎兒生父的感情有變，因而希望中止懷孕
  - 12.2% –孕婦年紀太輕，在家長或其他人的意願下中止懷孕
  - 10.8% –懷孕會令婦女的工作或學業被強制中止
  - 7.9% –不想再懷孕
  - 3.3% –胎兒身體殘缺或健康有損
  - 2.8% –繼續懷孕有損孕婦健康

## 方法

墮胎的歷史相當悠久。自古以來，一直以來流傳有許多方式執行，包含[草藥](../Page/草藥.md "wikilink")、利用尖銳工具、人為創傷，以及其他傳統方法\[19\]。

現代醫學藉由藥物或手術來誘發流產。在[第一孕期](../Page/第一孕期.md "wikilink")（孕期的前三個月），有兩種藥物和手術一樣有效，分別是[RU486和](../Page/RU486.md "wikilink")[前列腺素](../Page/前列腺素.md "wikilink")
\[20\]\[21\]。
而在[第二孕期](../Page/第二孕期.md "wikilink")（孕期第三到第六個月），雖然使用藥物仍能有效導致流產\[22\]，但使用手術產生後遺症的風險較低\[23\]。在墮胎後可以立即開始使用[複合口服避孕藥以及](../Page/複合口服避孕藥.md "wikilink")[子宮避孕器](../Page/子宮避孕器.md "wikilink")[避孕](../Page/避孕.md "wikilink")\[24\]。

  - 月經規則術：懷孕六周內。用一根塑膠細管伸入子宮腔內，以一百西西大針筒產生負壓，將子宮內膜吸出來，即使可能懷孕，因內膜已經破壞，無法著床，也能吸出懷孕初期胚胎。

\[25\]

  - 真空吸引術：孕期12周。將消毒過的管子置于女性的[子宮頸](../Page/子宮頸.md "wikilink")，利用真空的原理，將子宮內的經血或早期胚胎（約二週-七週內）吸除，過程約三十分鐘。[Vacuum-aspiration_(single).svg](https://zh.wikipedia.org/wiki/File:Vacuum-aspiration_\(single\).svg "fig:Vacuum-aspiration_(single).svg")
  - [子宮擴刮術](../Page/子宮擴刮術.md "wikilink")
  - [子宮藥物灌入法](../Page/子宮藥物灌入法.md "wikilink")
  - [子宮內裝置放置法](../Page/子宮內裝置放置法.md "wikilink")
  - [子宮刮除術](../Page/子宮刮除術.md "wikilink")
  - [子宮切開法](../Page/子宮切開法.md "wikilink")
  - [陰道塞劑放置法](../Page/陰道塞劑放置法.md "wikilink")
  - [RU-486投藥](../Page/美服培酮.md "wikilink")：妊娠前50天有95%的成功率，此法必须要在医生的指导下才能保证安全，千万不可自行投药流产。

## 安全性

堕胎的健康風險取決於該程序安全與否，[世界卫生组织把不安全的堕胎定義為由](../Page/世界卫生组织.md "wikilink")「欠專業訓練之人士、有害設備，以及不衛生的設施」行使的堕胎程序\[26\]。在已開發國家，合法的墮胎為醫學上最安全的程序之一\[27\]\[28\]。在美國每100,000宗堕胎才有0.7宗導致當事人[死亡](../Page/死亡率.md "wikilink")\[29\]；其死亡率比孕婦分娩低13倍（每100,000例分娩個案中有8.8宗導致當事人死亡）\[30\]\[31\]。在2000-2009年期間，在美國進行堕胎的死亡率低於在當地進行[整形手術的死亡率](../Page/整形.md "wikilink")\[32\]。跟堕胎有關的死亡風險會隨著孕期而增加，但其仍然低於孕期為21週以上的分娩個案\[33\]\[34\]\[35\]。在孕期的64-70天，门诊堕胎的安全性及有效性跟在57-63天時進行的一樣\[36\]。在孕期低於6週時，藥物誘發流產是安全且有效的\[37\]。
在[已開發國家中](../Page/已開發國家.md "wikilink")，藉由醫學達成安全且合法的墮胎已行之有年\[38\]\[39\]。單純的墮胎並不會造成長期心理衛生或身體的問題\[40\]。
[世界衛生組織提倡全世界的婦女都應該享有一樣好的安全合法墮胎方式](../Page/世界衛生組織.md "wikilink")\[41\]。話雖如此，每年仍在全球發生，仍造成大約47,000例的[產婦死亡及約](../Page/產婦死亡.md "wikilink")500萬名產婦住院\[42\]\[43\]。

## 社會觀念與法律規定

[RussianAbortionPoster.jpg](https://zh.wikipedia.org/wiki/File:RussianAbortionPoster.jpg "fig:RussianAbortionPoster.jpg")[海報](../Page/宣傳.md "wikilink")（大約於1925年）标题翻译：“任何未经过培训或自学的[助产士进行堕胎不仅会残害妇女](../Page/助产士.md "wikilink")，甚至会导致妇女的[死亡](../Page/死亡.md "wikilink")。”\]\]

\[\[<File:Abortion> Laws.svg|thumb|right||

世界各国关于堕胎的法律规定

联合国2013年关于堕胎的法律报告\[44\]

\]\]

堕胎在全世界大部分国家（特别是西方国家）都是备受争议的一种行为，正反两方面的意见争论非常激烈，主要为[道德伦理](../Page/道德.md "wikilink")、[宗教和女性](../Page/宗教.md "wikilink")[身體](../Page/身體.md "wikilink")[健康及](../Page/健康.md "wikilink")[生育权问题](../Page/生育权.md "wikilink")。

道德方面，反對者認為胎兒的生命屬於胎兒，所以其他人不能隨意剝奪胎兒的生存權利；宗教方面，以[基督教](../Page/基督教.md "wikilink")、[伊斯蘭教及](../Page/伊斯蘭教.md "wikilink")[猶太教主導的](../Page/猶太教.md "wikilink")[亞伯拉罕宗教傳統教義認為](../Page/亞伯拉罕宗教.md "wikilink")，生命是神所賜予的，所以只有神才有權取回生命，而非人類。這個觀念在大多數天主教徒佔多數的國家尤為強烈（天主教教義強烈反對墮胎）。在[愛爾蘭](../Page/愛爾蘭.md "wikilink")，过去除了墮胎以外，[避孕行為也是違法的](../Page/避孕.md "wikilink")。至於女性身體的生育權，則認為胎兒是女性身體的延續，而當胎兒的存在對母親的身體健康構成影響，母親有權中止胎兒的生長。也有爭議是胎兒是否定义为人類及享有作為人類的權利，部分支持者認為胎兒若尚未出生或形成人體，其是女性身體的一部分，並不能介定為人類，而反對者認為在胎兒形成時已是人類，胎兒作為生命，因此反對墮胎。

在全世界國家，非合法途径進行墮胎都是違法，它通常被取缔或者限制，但是部分國家，合法的堕胎也是存在的。现在大约世界上2/3的妇女可以合法堕胎（在該國家限制的胎兒周數以內）。各国堕胎法律差别很大，有些国家在任何情况只要有需求及醫生同意下便可以，而世界上不少国家无论如何也不准堕胎。堕胎法律依然在经常改变，也会继續成为社会爭议。

### 古代法

古代的法典中無明確對墮胎設立的條文。[巴比倫王國的](../Page/巴比倫王國.md "wikilink")[漢摩拉比法典](../Page/漢摩拉比法典.md "wikilink")（Hammurabi）中規定「毆打婦女，以致使其喪失胎兒者，根據她的地位課取罰款」，[印度的](../Page/印度.md "wikilink")[摩奴法典](../Page/摩奴法典.md "wikilink")（Manu）有「以劍擊人而殺害胎兒者處......」，皆屬於傷害罪的型式。

### 宗教

  - [基督教在西元](../Page/基督教.md "wikilink")629年的君士坦丁堡會議決議中墮胎等同殺人。而直到現在，[天主教會不單堅決反對任何墮胎行為](../Page/天主教會.md "wikilink")，过去甚至会把曾经墮胎的女性开除。在不少以[天主教為國家主要宗教的國家](../Page/天主教.md "wikilink")，如[愛爾蘭共和國](../Page/愛爾蘭共和國.md "wikilink")，任何墮胎行為（除非胎兒對婦女生命構成威脅以外）曾屬刑事罪行，不論要求墮胎者及執行手術的醫生皆有可能被判處入獄，後來於[2018年5月25日公投通過修憲，准許墮胎](../Page/2018年第三十六條憲法修正案.md "wikilink")。至於其他基督教教派，大多數都引用《[聖經](../Page/聖經.md "wikilink")》內容及教義而反對教徒接受墮胎手術。

<!-- end list -->

  - [猶太教根據他們的](../Page/猶太教.md "wikilink")《[猶太聖法](../Page/猶太聖法.md "wikilink")》經傳中提到，胎兒在頭部或身體的大部份未離開母體之前，被視為生命的一部分，而非獨立的個體\[45\]\[46\]。在1975年的猶太教徒雙年會上宣布與墮胎有關的聲明中提到「......母親的福祉是我們的首要考量。」

<!-- end list -->

  - [伊斯兰教主张在不违背](../Page/伊斯兰教.md "wikilink")[真主的造化的情况下](../Page/阿拉.md "wikilink")，可以为了实现自己、家庭、社会的利益而堕胎。伊斯兰教各家教法学家都不同程度地认为，在四个月以前流产是可以的\[47\]。

### 亞洲地區相關規定

  - ：於1948年頒布的《優生保護法》使日本在亞洲內最快合法墮胎的國家。於1997年改名了《母體健康法》。

      - 可在懷孕22週以內實施人工流產。\[48\]

  - ：中國在一九七八年立法規定，除農村或少數民族外，多數夫婦只允許生育一個小孩。二○一三年實施「單獨二胎」政策，即夫婦其中一方是獨生子女，便可合法生育兩名子女，但若要生第二胎，仍須辦理政府核發的再生育服務證（准生證）。剛過去的五中全會通過全面實施二孩政策，允許每對夫婦可以生育兩個孩子。全国性法律并没有对此做出规定，但少数地方政府法规有相关规定，主要是为了避免性别歧视，使得人口结构合理化。[哈尔滨市政府规定](../Page/哈尔滨市.md "wikilink")，怀孕14周以上做人工流产要经过行政审批。《[贵阳市禁止选择性终止妊娠规定](../Page/贵阳市.md "wikilink")》规定，除一些特殊情形之外，禁止为怀孕14周以上的妇女施行人工流产。否则最高可处以违法所得6倍以下，或3万元以下罚款。在计划外怀孕的，会根据父母的收入实行罚款。在[计劃生育的](../Page/计劃生育.md "wikilink")[一孩政策下](../Page/一孩政策.md "wikilink")，墮胎是常见的强行終止怀孕的做法。并且，堕胎在中华人民共和国合法，由计生办操纵的强行堕胎仍然名义上属于违法行为。

  - ：於1969年頒布《墮胎法》。\[49\]\[50\]

#### 條件限制墮胎國家

  - \[51\]\[52\]\[53\]：根據《侵害人身罪條例》第47A條規定

      - 合法終止懷孕手術必須在懷孕首24週內進行（如繼續懷孕對孕婦構成生命威脅則不受懷孕期限制），且必須獲得2名註冊醫生確實一致認為：
          - 繼續懷孕對孕婦生命或孕婦生理或精神健康的威脅會大於終止懷孕，或
          - 胎兒出生後極可能有身心不健全情形而足以造成嚴重傷殘；或
          - 孕婦年齡不足16歲；或
          - 孕婦在此前3個月內曾向警方報案，自稱是亂倫、強姦、迫姦、誘姦或迷姦罪案之受害者。

  - ：《[中華民國刑法](../Page/s:中華民國刑法.md "wikilink")》自從1934年制定公布，1935年7月1日施行以來，歷次版本第2編第24章規定了「墮胎罪」，但第288條第3項明定，懷胎婦女因疾病或其他防止生命上危險之必要而墮胎者，免除其刑。1984年在台灣制定公布《[優生保健法](../Page/s:優生保健法.md "wikilink")》，1985年1月1日施行以來，在第3章人工流產與結紥手術第九條中，明定人工流產的施行範圍。但同時故此二法之間仍有爭議時，依據《[中央法規標準法](../Page/s:中央法規標準法.md "wikilink")》第16條，「法規對其他法規所規定之同一事項而為特別之規定者，應優先適用之」，就是特別法優於普通法。\[54\]

      - 允許24周以內，有唐氏症等先天重大疾病，符合《優生保健法》規範的胎兒，才能終止懷孕。具體規定如下：
      - 1、雙親之一有「有礙優生」之遺傳性疾病、傳染病或精神疾病者。
      - 2、近親有「有礙優生」之遺傳疾病者。
      - 3、醫學認定，懷孕或分娩危害母親生命、身體或精神健康者。
      - 4、醫學認定胎兒有不正常發育者。
      - 5、因強制性交、誘姦而懷孕，或依法不得結婚而相姦受孕者。
      - 6、如果生小孩會影響母親心理健康或家庭生活者。\[55\]

  - ：於1953年頒布《墮胎法》，

      - 只有當孕婦健康受到嚴重威脅、因強姦、亂倫致孕或胎兒患有嚴重遺傳疾病時，孕婦才可接受墮胎手術。懷孕24週後，一切墮胎手術均屬違法。對非法墮胎的女性處1年以下的有期徒刑，併科200萬韓圓以下罰金。對於實施墮胎手術的醫生等人也將被處以2年以下有期徒刑。\[56\]

  - [印度](../Page/印度.md "wikilink")

      - 懷孕20周前。懷孕是否會對孕婦的身體和精神健康帶來巨大風險或者胎兒出現了異常情況。而懷孕了12周至20周的孕婦墮胎的話，需要有兩名合格的醫生同時批准才行。\[57\]

<!-- end list -->

  - 許多國家允許婦女在母體生命受威脅的情況下墮胎\[58\]，包括[缅甸](../Page/缅甸.md "wikilink")。

#### 完全禁止墮胎國家

  - [馬來西亞刑事法典第](../Page/馬來西亞.md "wikilink")312條文闡明
      - 任何人自願導致（懷孕）女性流產，可被判監禁最高3年，或罰款，或兩者兼施。若胚胎已經成型，該名人士可被判監禁7年或罰款。\[59\]

<!-- end list -->

  - [寮国](../Page/寮王國.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")

### 歐洲地區相關規定

  - [愛爾蘭憲法曾規定禁止婦女墮胎](../Page/愛爾蘭.md "wikilink")，而1992年11月經過公民投票，允許婦女到愛爾蘭以外的國家進行人工流產，2014年通過允許孕婦在健康受到嚴重威脅的情況下可以合法墮胎，2018年依据[第三十六条宪法修正案删除宪法禁止堕胎的修正案](../Page/2018年第三十六条宪法修正案.md "wikilink")。
      - 懷孕12週以內婦女進行人工流產，或在會危急孕婦生命、對孕婦健康構成嚴重傷害等條件下墮胎。新法也允許對可能導致生產前死亡，或出生28天內夭折的異常胎兒進行人工流產。\[60\]
  - [法國](../Page/法國.md "wikilink")1970年代立法，
      - 婦女可在懷孕的前12周內無條件選擇進行人工流產。
  - [瑞典自](../Page/瑞典.md "wikilink")1970年起
      - 懷孕十八週以內的女性可以不需要持任何理由而在醫院選擇墮胎，是對墮胎行為最寬鬆的國家。
  - [瑞士在很長時間中使用](../Page/瑞士.md "wikilink")1937年制定的墮胎法，進行墮胎者會被處以五年牢獄和罰款，直至2001年，瑞士議會在通過允許孕婦在懷孕的前十二周內選擇墮胎，在翌年的全民投票中，72%的選民對墮胎非刑事化的改革提案投了贊成票。
      - 懷孕的前十二周內選擇墮胎
  - [羅馬尼亞在](../Page/羅馬尼亞.md "wikilink")1989年立法通過墮胎法，在此之前該國的非法墮胎造成五十萬個女性的死亡。
  - [義大利](../Page/義大利.md "wikilink")
      - 婦女在妊娠90天內可無條件選擇進行人工流產。
  - [英國在大不列顛及愛爾蘭聯合王國時代](../Page/英國.md "wikilink")，於1803年通過世界第一個反墮胎法案—，將墮胎視為非法的行為，最高可處以死刑。1861年的，則將最高刑期降至無期徒刑，而該法後被英國及愛爾蘭各自繼承。1929年通過嬰兒保護法，視墮胎相當於殺嬰，禁止結束任何可能活存的胎兒的生命。1967年，制定了墮胎法，保護婦女選擇墮胎的權利，允許在懷孕28週內進行有條件的墮胎行為。1990年修正並放寬人類受精和胚胎法中墮胎的規定
      - 懷孕24週內在有條件可以合法進行墮胎手術。需要有兩名醫生的批准或簽紙（緊急情況下只需一名醫生）、懷孕未超過24周、及以下列可能為墮胎理由：挽救孕婦的性命、保護孕婦身體或精神上的健康、胎兒有不正常發展的跡象、社會或經濟理由等。如果孕婦的生命因繼續懷孕而受到嚴重威脅或胎兒有明顯不正常發展的風險，無論懷孕多久，仍容許進行墮胎。\[61\]
  - [西班牙二](../Page/西班牙.md "wikilink")○一○年將未超過十四周的嬰孩墮胎合法化，若因為孕婦有健康風險及胎兒畸形的原因，
      - 懷孕周數在二十二周以下墮胎亦屬合法。

#### 條件限制墮胎國家

  - [波蘭](../Page/波蘭.md "wikilink")
      - 只有在強姦或亂倫導致懷孕、孕婦生命受到嚴重威脅和胎兒嚴重畸形的情況下才允許女性墮胎。\[62\]
  - [塞浦路斯](../Page/塞浦路斯.md "wikilink")
      - 因姦成孕或對母親健康有嚴重威脅才可進行人工流產。\[63\]
  - [葡萄牙墮胎是非法的](../Page/葡萄牙.md "wikilink")，只准許
      - 因強暴懷孕、胚胎畸形或是母體有健康威脅情況下，可在懷孕12周以內實施墮胎。\[64\]

#### 完全禁止堕胎国家

主要信奉[天主教的](../Page/天主教.md "wikilink")[馬其他是唯一一個完全禁止墮胎的歐盟國家](../Page/馬耳他.md "wikilink")，一旦違法，將被判處18個月至3年徒刑。
[安道爾侯國](../Page/安道爾.md "wikilink")、[梵蒂岡和](../Page/梵蒂岡.md "wikilink")[聖馬利諾等不屬於歐盟的歐洲國家也禁止墮胎](../Page/聖馬力諾.md "wikilink")。

### 北美洲地區相關規定

  - [美國](../Page/美國.md "wikilink")

1940年代之前，[康乃狄克州制定墮胎法](../Page/康乃狄克州.md "wikilink")，是第一個通過墮胎法的[美國州份](../Page/美國.md "wikilink")；
1960年代時，美國發生了一位婦女在懷孕過程中，服用某種藥物引起新生兒畸型，和懷孕時罹患[德國麻疹導致新生兒](../Page/德國麻疹.md "wikilink")[重度殘障的事件之後](../Page/重度殘障.md "wikilink")，美國各州開始放寬墮胎的法律規定；於1973年1月22日，[美國聯邦最高法院於](../Page/美國聯邦最高法院.md "wikilink")[羅訴韋德案一案](../Page/羅訴韋德案.md "wikilink")，承認婦女的墮胎權受到憲法隱私權的保護，在懷孕首階段（三個月內）可合法進行手術。該判決至今仍受美国社會爭議，至今美國國內仍有反墮胎組織要求推翻該判決，而主要政黨包括[民主黨等主張尊重和維持最高法院的判決](../Page/民主黨_\(美國\).md "wikilink")。

  -   - [南达科他州州长迈克](../Page/南达科他州.md "wikilink")·郎兹签署一项法案，2006年3月6日，禁止该州内的堕胎行为，\[65\]目的为通过诉讼将案件送上聯邦最高法院，以便当时倾向保守派的最高法院推翻羅訴韋德案的判决，但该州法案在2006年11月选举中被选民公投废止。\[66\]
      - [愛荷華州懷孕](../Page/愛荷華州.md "wikilink")6周後若偵測到胎兒心跳就不可進行人工流產。\[67\]
      - [密西西比州孕期逾](../Page/密西西比州.md "wikilink")15周者墮胎即屬違法，除所懷胎兒嚴重畸形或是母體有危險的婦女。\[68\]
      - [愛阿華州](../Page/愛阿華州.md "wikilink")「心跳法案」(Heartbeat
        bill)的限制墮胎法案，規定一旦檢測到胎兒心跳，墮胎即屬非法，而一般大約懷孕六周胎兒就會有心跳。\[69\]

<!-- end list -->

  - [加拿大](../Page/加拿大.md "wikilink")
      - 懷孕所有階段均是合法的。\[70\]

### 南美洲地區相關規定

  - [墨西哥城](../Page/墨西哥城.md "wikilink")，2007年4月24日，议会通过议案指出，
      - 孕妇在怀孕不到12周时可以自愿决定是否终止妊娠。如果怀孕对孕妇的生命构成了威胁、妇女遭强奸后怀孕、胎儿畸形以及在未经批准的[人工授精等情况下](../Page/人工授精.md "wikilink")，孕妇在怀孕20周前可以实施堕胎。\[71\]

<!-- end list -->

  - 容許自由人工流產的國家包括[古巴](../Page/古巴.md "wikilink")、[圭亞那和](../Page/圭亞那.md "wikilink")[烏拉圭](../Page/烏拉圭.md "wikilink")\[72\]

#### 條件限制墮胎國家

  - [智利](../Page/智利.md "wikilink")2017年9月终止实行数十年的严格堕胎禁令，时任总统巴舍莱（michelle
    bachelet）签署堕胎除罪化法案
      - 胎兒對母體造成危害、胎兒判定不能存活以及母親是因為強暴而懷孕。\[73\]
  - [巴西](../Page/巴西.md "wikilink")《刑法》第124至127條
      - 遭性侵而懷孕，以及沒有其他方式保住母體性命的情況下，墮胎不會遭到判刑\[74\]

<!-- end list -->

  - 允許婦女在母體生命受威脅的情況下墮胎\[75\]，包括

[危地马拉](../Page/危地马拉.md "wikilink")、[孟加拉](../Page/孟加拉国.md "wikilink")、[委内瑞拉](../Page/委內瑞拉.md "wikilink")、[巴拉圭](../Page/巴拉圭.md "wikilink")、[厄瓜多爾](../Page/厄瓜多爾.md "wikilink")

  - 因姦成孕能成為合法人工流產的理由，包括

[阿根廷](../Page/阿根廷.md "wikilink")、[古巴](../Page/古巴.md "wikilink")、[厄瓜多爾](../Page/厄瓜多爾.md "wikilink")、[墨西哥和](../Page/墨西哥.md "wikilink")[巴拿馬](../Page/巴拿馬.md "wikilink")\[76\]

#### 完全禁止堕胎国家

[多明尼加](../Page/多明尼加.md "wikilink")、[萨尔瓦多](../Page/萨尔瓦多.md "wikilink")、[海地](../Page/海地.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[苏利南](../Page/蘇利南.md "wikilink")\[77\]

### 非洲地區相關規定

  - [莫三比克](../Page/莫三比克.md "wikilink")
      - 女性可以在懷胎12個星期內墮胎；如果是因為強暴或者是孕婦健康等因素，可以在懷孕16個星期內墮胎。\[78\]

#### 條件限制墮胎國家

許多國家允許婦女在母體生命受威脅的情況下墮胎\[79\]，包括
[南苏丹](../Page/南蘇丹.md "wikilink")、[乌干达](../Page/乌干达.md "wikilink")、[索马利亚](../Page/索马里.md "wikilink")、[象牙海岸](../Page/科特迪瓦.md "wikilink")

#### 完全禁止堕胎国家

[刚果共和国](../Page/刚果共和国.md "wikilink")、[刚果民主共和国](../Page/刚果民主共和国.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[加彭](../Page/加蓬.md "wikilink")、[几内亚比索共和国](../Page/幾內亞比索.md "wikilink")、[马达加斯加](../Page/马达加斯加.md "wikilink")、[茅利塔尼亚](../Page/茅利塔尼亞地理.md "wikilink")、[塞内加尔](../Page/塞内加尔.md "wikilink")

### 中東地區相關規定

#### 條件限制墮胎國家

  - [以色列](../Page/以色列.md "wikilink")
      - 1977年的以色列刑法指定了專門的 委員會 來決定妊娠終止相關事宜。這個委員會由三名成員組成，其中至少有一人必須是女性：

一名婦產科專家 一名相關醫療科目的專家（可以是婦產科、內科、精神科等） 一名社會工作者\[80\]

  - 許多國家允許婦女在母體生命受威脅的情況下墮胎\[81\]，包括

[阿富汗](../Page/阿富汗.md "wikilink")、[约旦](../Page/约旦.md "wikilink")、[加萨](../Page/加薩.md "wikilink")、[也门](../Page/也门.md "wikilink")、[叙利亚](../Page/叙利亚.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[阿曼王国](../Page/阿曼.md "wikilink")

## 女性主義

墮胎一向是[女性主義有關女性](../Page/女性主義.md "wikilink")[生育权](../Page/生育权.md "wikilink")[自由的重要議題](../Page/自由.md "wikilink")，有所謂“為選擇”（pro-choice）與“為生命”（pro-life）之爭論。

不少女性主義者宣稱要不要繼續懷孕是女性的身體權，屬於基本[人權](../Page/人權.md "wikilink")，懷孕之後是否選擇墮胎也應由女性自行依據身體狀況、經濟狀況、生活形態等等因素，与家人、朋友商量合作出決定，拒絕由國家制定法律逕行介入，也反對由[家長或](../Page/父母.md "wikilink")[丈夫過度參與墮胎與否的決策](../Page/丈夫.md "wikilink")。此外，支持墮胎選擇權的人士亦指出，全面禁止墮胎只會造成更多非法墮胎的事情發生，安全而合法的墮胎，加強[性教育](../Page/性教育.md "wikilink")、[避孕及](../Page/避孕.md "wikilink")[生育控制才能有效減少墮胎的數目](../Page/生育控制.md "wikilink")。

極富爭議性的女性主義心理學家[卡羅爾·吉利根](../Page/卡羅爾·吉利根.md "wikilink")（Carol
Gilligan）質疑以“為選擇”與“為生命”的抽象概念（權利或正義）入手是抽離女性經驗的倫理學。她在《不同的聲音》（In a
Different
Voice）一書中，報告及分析了29位15-33歲、不同種族及背景的女性分別兩次的訪談，希望得知婦女如何去處理懷孕或墮胎的道德處境。吉利根研究認為女性把道德定義為實現關懷和避免傷害的義務問題，與抽象的「[形式邏輯](../Page/形式邏輯.md "wikilink")」成為強烈的對比，對吉利根來說，女性對判斷道德問題的猶疑不是基於缺乏對抽象的權利及正義思考的能力，反而是基於對現實的複雜性的了解。

吉利根對[柯爾伯格道德發展階段論](../Page/柯爾伯格道德發展階段.md "wikilink")（Theory of Moral
Development）及以[正義](../Page/正義.md "wikilink")（Justice）和[權利](../Page/權利.md "wikilink")（Rights）為道德發展最高階段考慮的理論作出了批判。吉利根留意到柯爾伯格研究的對象均為白人男性及男孩，她認為柯爾伯格對道德發展的研究排除了女性的經驗，在她對女性墮胎的研究中，她研究認為女性以關懷（care）、人際關係（relationship）及連繫（connection）在道德判斷中的考慮及其價值。

在一項[瑞典女性討論墮胎的焦點小組研究中](../Page/瑞典.md "wikilink")，有參與者認為墮胎的選擇是女性「在無助中的權利」（right
in a situation of distress, Ekstran, Essen & Tyde'n,
2005），也許這形容正好說明了女性在墮胎的倫理抉擇中的矛盾處境，有助闡釋吉利根強調的「關懷」在對「權利」的思考中的不可或缺。

## 爭議

## 有關墮胎的電影

  - 《[天地孩兒](../Page/天地孩兒.md "wikilink")》
  - 《》
  - 《[四月三周两天](../Page/四月三周两天.md "wikilink")》
  - 《》
  - 《[墮胎](../Page/墮胎_\(電影\).md "wikilink")》
  - 《[Juno少女孕記](../Page/Juno少女孕記.md "wikilink")》
  - 《》
  - 《[地下觀音](../Page/地下觀音.md "wikilink")》
  - 《》
  - 《[天使薇拉卓克](../Page/天使薇拉卓克.md "wikilink")》

## 参考文献

## 引用

## 来源

  -
  -
  -
  - [各國有關墮胎相關規範](http://gsrat.net/news/newsclipDetail.php?ncdata_id=3555)

  - [各國人工流產法例整匯](https://wknews.org/node/1793)

  - [各國墮胎法](https://www.e123.hk/ElderlyPro/details/436866/74)

  - [世衛組織：全球仍有19國全面禁止墮胎 - 新聞 - Rti
    中央廣播電臺](https://www.rti.org.tw/news/view/id/357903)

## 参见

  - [妊娠](../Page/妊娠.md "wikilink")
  - [流产](../Page/流产.md "wikilink")
  - [计划生育](../Page/计划生育.md "wikilink")
  - [生育權](../Page/生育權.md "wikilink")
  - [生育控制](../Page/生育控制.md "wikilink")
  - [反墮胎](../Page/反墮胎.md "wikilink")
  - [人權](../Page/人權.md "wikilink")
  - [女權](../Page/女權.md "wikilink")
  - [卡羅爾·吉利根](../Page/卡羅爾·吉利根.md "wikilink")
  - [儿童安乐死](../Page/儿童安乐死.md "wikilink")
  - [堕胎-乳腺癌假说](../Page/堕胎-乳腺癌假说.md "wikilink")
  - [意外懷孕](../Page/意外懷孕.md "wikilink")

<!-- end list -->

  - [墮胎爭議](../Page/墮胎爭議.md "wikilink")
  - [反墮胎](../Page/反墮胎.md "wikilink")

{{-}}

[堕胎](../Category/堕胎.md "wikilink")
[Category:道德議題](../Category/道德議題.md "wikilink")
[Category:性別研究](../Category/性別研究.md "wikilink")

1.

2.

3.

4.
5.

6.
7.

8.

9.

10.

11.

12.

13.

14.

15.

16.
17. \<

18.

19.

20.

21.
22.

23.
24.

25. [月經規則術？ 人工流產手術的演化
    元氣網](https://health.udn.com/health/story/10561/2927080)

26.

27.
28.

29.
30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.
41.

42.

43.

44.

45.

46.

47.

48. [各國人工流產法例整匯](https://wknews.org/node/1793)

49. [墮胎合法化](http://www.hpa.gov.tw/File/Attach/1319/File_807.pdf)

50. [我们结婚吧，但是…… - 梁利平 - Google
    圖書](https://books.google.com.hk/books?id=mx9YDwAAQBAJ&pg=PA81&lpg=PA81&dq=%E6%96%B0%E5%8A%A0%E5%9D%A1+%E5%A2%AE%E8%83%8E%E6%B3%95&source=bl&ots=3ppURl7qaV&sig=ACfU3U3_rsV7gL3puk2lUYBInk7yNfTSvQ&hl=zh-TW&sa=X&ved=2ahUKEwja0snhwrLgAhUPQN4KHROPBAAQ6AEwDHoECBIQAQ#v=onepage&q=%E6%96%B0%E5%8A%A0%E5%9D%A1%20%E5%A2%AE%E8%83%8E%E6%B3%95&f=false)

51.

52.

53. [立法會十四題：終止妊娠醫療程序](https://www.info.gov.hk/gia/general/201706/28/p2017062800464.htm)

54.

55.
56.
57. [國際縱橫：印度有關墮胎法的爭議 - BBC News
    中文](https://www.bbc.com/zhongwen/trad/world_outlook/2015/08/150815_world_outlook_india_abortion)

58. [不許婦女夾娃娃
    盤點全球反墮胎國家](https://www.swissinfo.ch/chi/afp/不许妇女夹娃娃-盘点全球反堕胎国家/44138584)

59. [我有辦法：在我國，墮胎是合法的嗎？](http://www.orientaldaily.com.my/s/162961)

60. [「愛爾蘭人民的共同決定」：公投200多天後，愛爾蘭墮胎首度合法化](https://hk.thenewslens.com/article/110205)

61. [MPs call for abortion law
    reforms](http://news.bbc.co.uk/2/hi/health/7069011.stm)

62. [波蘭數千人街頭抗議
    呼籲政府放寬墮胎限制](http://world.huanqiu.com/exclusive/2018-01/11534701.html)

63.
64. [各國有關墮胎相關規範](http://gsrat.net/news/newsclipdetail.php?ncdata_id=3555)

65. [美国南达科他州长挑战最高法院签署禁止堕胎法](http://www.chinadaily.com.cn/gb/doc/2006-03/07/content_527616.htm)


66.

67.
68. [密西西比州通過全美嚴墮胎法　禁懷孕15周者人工流產](https://www.hk01.com/%E5%8D%B3%E6%99%82%E5%9C%8B%E9%9A%9B/170172/%E5%AF%86%E8%A5%BF%E8%A5%BF%E6%AF%94%E5%B7%9E%E9%80%9A%E9%81%8E%E5%85%A8%E7%BE%8E%E5%9A%B4%E5%A2%AE%E8%83%8E%E6%B3%95-%E7%A6%81%E6%87%B7%E5%AD%9515%E5%91%A8%E8%80%85%E4%BA%BA%E5%B7%A5%E6%B5%81%E7%94%A2)

69. [胎兒有心跳禁墮胎 愛州全美最嚴](https://udn.com/news/story/6813/3121141)

70.
71. 冷彤，[墨西哥首都议会通过允许堕胎议案](http://news.xinhuanet.com/world/2007-04/25/content_6027686.htm)，新华网

72.
73. [世衛組織：全球仍有19國全面禁止墮胎 - 新聞 - Rti
    中央廣播電臺](https://www.rti.org.tw/news/view/id/357903)

74. [懷孕3個月內墮胎是犯罪嗎？巴西最高法院公聽會激辯
    宗教團體高舉生命權反對墮胎除罪化-風傳媒](https://www.storm.mg/article/473455)

75.
76.
77.
78. [不談女權談成本_天主教國家莫國墮胎合法化-地球圖輯隊
    帶你看透全世界](https://dq.yam.com/post.php?id=3268)

79.
80. [〔法律〕以色列女性合法墮胎後為何可能會成為離婚過錯方？ -
    GetIt01](https://www.getit01.com/p2018021220716103/)

81.