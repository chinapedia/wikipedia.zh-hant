{{ 足球聯賽資料 | name = 希臘足球超級聯賽 | another_name = Superleague Greece | image
= SuperLeagueGreecelogo.jpg | pixels = 150px | country =  | confed =
[UEFA](../Page/歐洲足球協會聯盟.md "wikilink")（[歐洲](../Page/歐洲.md "wikilink")） |
founded = 1927年 | teams = 16 隊 | relegation =
[希臘足球甲級聯賽](../Page/希臘足球甲級聯賽.md "wikilink")
| level = 第 1 級 | domestic_cup = [希臘盃](../Page/希臘盃.md "wikilink") |
international_cup = [歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")
[歐霸盃](../Page/歐足聯歐洲聯賽.md "wikilink") | season = 2018–19 | champions =
[PAOK](../Page/PAOK足球俱樂部.md "wikilink") | most_successful_club =
[奧林比亞高斯](../Page/奧林匹亞科斯足球俱樂部.md "wikilink") | no_of_titles =
44 次 | website = [希超官網](http://www.superleaguegreece.net/) | current =
}}

**希臘足球超級聯賽** （[希臘文](../Page/希臘文.md "wikilink")：**Σούπερ Λίγκα
Ελλάδα**，[英文](../Page/英文.md "wikilink")：*Super League Greece*）
是[希臘](../Page/希臘.md "wikilink")[职业](../Page/职业.md "wikilink")[足球联赛中最高組別聯賽](../Page/足球.md "wikilink")，於2006年从原希腊足球甲级联赛（Alpha
Ethniki）改制而来，希超聯賽现在共有 16
支隊伍角逐，按[欧洲足联积分排名現時排第](../Page/欧洲足联积分排名.md "wikilink")
12 位。

希腊全國聯賽始於
1959–60年球季，成立之前希臘國內有多個地區性聯賽。希甲聯賽成立後便把各地方聯賽制度統一起來。一般希甲聯賽由夏季末月開始至翌年夏季初結束。十六支球隊會互相對疊，各有主場和作客兩部分，採計分制。聯賽榜最底的三支球隊會降到低組別聯賽作賽。

成立至今希超聯賽都是由[奧林比亞高斯和](../Page/奧林匹亞科斯足球俱樂部.md "wikilink")[彭拿典奈高斯等少數球會所壟斷](../Page/帕納辛奈科斯足球俱樂部.md "wikilink")，當中又以奧林比亞高斯奪冠次數最多，有
29 次。改名后的第一届希超联赛的冠军被奥林匹亚克斯获得。

## 參賽球隊

2018–19年希臘足球超級聯賽參賽隊伍共有 16 支。

| 中文名稱                                          | 英文名稱                   | 所在城市                                      | 上季成績     |
| --------------------------------------------- | ---------------------- | ----------------------------------------- | -------- |
| [AEK雅典](../Page/AEK雅典足球會.md "wikilink")       | AEK Athens FC          | [雅典](../Page/雅典.md "wikilink")            | 第 1 位    |
| [PAOK](../Page/PAOK足球俱樂部.md "wikilink")       | P.A.O.K. FC            | [塞薩洛尼基](../Page/塞薩洛尼基.md "wikilink")      | 第 2 位    |
| [奧林比亞高斯](../Page/奧林匹亞科斯足球俱樂部.md "wikilink")   | Olympiacos FC          | [比雷埃夫斯](../Page/比雷埃夫斯.md "wikilink")      | 第 3 位    |
| [艾杜美圖斯](../Page/阿特羅米托斯足球俱樂部.md "wikilink")    | Atromitos FC           | [佩里斯特里](../Page/佩里斯特里.md "wikilink")      | 第 4 位    |
| [卓普利斯](../Page/阿斯特拉斯特里波利斯足球俱樂部.md "wikilink") | Asteras Tripoli FC     | [特里波利](../Page/特里波利.md "wikilink")        | 第 5 位    |
| [薩丁](../Page/薩丁斯柯達體育會.md "wikilink")          | Skoda Xanthi FC        | [克桑西](../Page/克桑西.md "wikilink")          | 第 6 位    |
| [彭尼奧尼奧斯](../Page/帕尼奧尼奧斯足球俱樂部.md "wikilink")   | Panionios GSS          | [雅典](../Page/雅典.md "wikilink")            | 第 7 位    |
| [彭尼杜列高斯](../Page/帕納托利克斯足球俱樂部.md "wikilink")   | Panetolikos FC         | [艾基連奧](../Page/艾基連奧.md "wikilink")        | 第 8 位    |
| [基亞尼拿](../Page/吉安尼納競技俱樂部.md "wikilink")       | PAS Giannina FC        | [約阿尼納](../Page/約阿尼納.md "wikilink")        | 第 9 位    |
| [利華迪亞高斯](../Page/萊瓦迪亞克斯足球俱樂部.md "wikilink")   | Levadiakos FC          | [利瓦迪亞](../Page/利瓦迪亞.md "wikilink")        | 第 10 位   |
| [彭拿典奈高斯](../Page/帕納辛奈科斯足球俱樂部.md "wikilink")   | Panathinaikos FC       | [雅典](../Page/雅典.md "wikilink")            | 第 11 位   |
| [拿尼沙](../Page/拿尼沙體育會.md "wikilink")           | AE Larissa FC          | [拉里薩](../Page/拉里薩.md "wikilink")          | 第 12 位   |
| [拉米亞](../Page/拉米亞1964體育會.md "wikilink")       | PAS Lamia 1964         | [拉米亞](../Page/拉米亞_\(希臘城市\).md "wikilink") | 第 13 位   |
| [艾普隆斯邁恩斯](../Page/斯米爾尼阿波羅足球俱樂部.md "wikilink") | Apollon Smyrni F.C.    | [雅典](../Page/雅典.md "wikilink")            | 第 14 位   |
| [OFI基迪](../Page/OFI基迪足球會.md "wikilink")       | OFI Crete F.C.         | [伊拉克利翁](../Page/伊拉克利翁.md "wikilink")      | 甲組，第 1 位 |
| [阿里斯](../Page/阿瑞斯足球俱樂部.md "wikilink")         | Aris Thessaloniki F.C. | [塞薩洛尼基](../Page/塞薩洛尼基.md "wikilink")      | 甲組，第 2 位 |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[1\]

## 球會奪冠次數

<table>
<thead>
<tr class="header">
<th><p>球會</p></th>
<th><p>冠軍次數</p></th>
<th><p>冠軍年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/奧林匹亞科斯足球俱樂部.md" title="wikilink">奧林比亞高斯</a></p></td>
<td><p>44</p></td>
<td><p>1931, 1933, 1934, 1936, 1937, 1938, 1947, 1948, 1951, 1954,<br />
1955, 1956, 1957, 1958, 1959, 1966, 1967, 1973, 1974, 1975,<br />
1980, 1981, 1982, 1983, 1987, 1997, 1998, 1999, 2000, 2001,<br />
2002, 2003, 2005, 2006, 2007, 2008, 2009, 2011, 2012, 2013,<br />
2014, 2015, 2016, 2017</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/帕納辛奈科斯足球俱樂部.md" title="wikilink">彭拿典奈高斯</a></p></td>
<td><p>20</p></td>
<td><p>1930, 1949, 1953, 1960, 1961, 1962, 1964, 1965, 1969, 1970,<br />
1972, 1977, 1984, 1986, 1990, 1991, 1995, 1996, 2004, 2010</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/AEK雅典足球會.md" title="wikilink">AEK雅典</a></p></td>
<td><p>12</p></td>
<td><p>1939, 1940, 1963, 1968, 1971, 1978, 1979, 1989, 1992, 1993,<br />
1994, 2018</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿瑞斯足球俱樂部.md" title="wikilink">阿里斯</a></p></td>
<td><p>3</p></td>
<td><p>1928, 1932, 1946</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/PAOK足球俱樂部.md" title="wikilink">PAOK</a></p></td>
<td><p>3</p></td>
<td><p>1976, 1985, 2019</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉里薩足球俱樂部.md" title="wikilink">拿尼沙</a></p></td>
<td><p>1</p></td>
<td><p>1988</p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [Super League Greece Official
    Website](http://www.superleaguegreece.net)
  - [Super League
    Statistics](https://web.archive.org/web/20070613222837/http://www.galanissportsdata.com/sdatesgr.asp)
  - [Greek Top
    Scorers](https://web.archive.org/web/20080908192951/http://www.e-soccer.gr/index.php?option=com_content&view=category&id=171&Itemid=507)
  - [Greek
    Champions](http://www.e-soccer.gr/index.php?option=com_wrapper&view=wrapper&Itemid=458)

[\*](../Category/希臘足球超級聯賽.md "wikilink")
[Category:1927年建立的體育聯賽](../Category/1927年建立的體育聯賽.md "wikilink")
[Category:1927年希臘建立](../Category/1927年希臘建立.md "wikilink")

1.