[Hotaru_no_Hikari(Auld_lang_syne_in_Japan).ogg](https://zh.wikipedia.org/wiki/File:Hotaru_no_Hikari\(Auld_lang_syne_in_Japan\).ogg "fig:Hotaru_no_Hikari(Auld_lang_syne_in_Japan).ogg")
《**螢之光**》（）是改編自[蘇格蘭民謠](../Page/蘇格蘭.md "wikilink")《[-{zh-hans:友谊地久天长;zh-hk:友誼地久天長;zh-tw:友誼萬歲;}-](../Page/友誼萬歲.md "wikilink")》（Auld
Lang
Syne）的[日本](../Page/日本.md "wikilink")[歌曲](../Page/歌曲.md "wikilink")，作詞者是[稻垣千穎](../Page/稻垣千穎.md "wikilink")。

歌名《螢之光》，語出中國成語「囊萤映雪」（日本語：蛍雪の功，けいせつのこう）。“囊萤”是指[晋代](../Page/晋代.md "wikilink")[车胤少时家贫](../Page/车胤.md "wikilink")，捕捉螢火蟲照明讀書的故事；“映雪”是指晋代孙康冬天利用雪的反光读书的故事；两则故事用以形容苦学不倦。

## 概要

[明治](../Page/明治.md "wikilink")14年（1881年）成為尋常小學校的唱歌教材，明治28年（1895年）[日本領台後也成為](../Page/台灣日治時期.md "wikilink")[台灣初等教育的唱歌教材](../Page/台灣.md "wikilink")。

[戰後在日本只唱第](../Page/太平洋戰爭.md "wikilink")1、2段，因第3、4段歌詞的愛國主義內容而不再教唱。戰後在台灣則從螢之光，改為相同曲調、中文歌詞的「[驪歌](../Page/驪歌.md "wikilink")」。

## 歌詞

### 原版

明治14年（1881年）刊載於〈小學唱歌集初編〉的歌詞如下。

<table>
<thead>
<tr class="header">
<th></th>
<th><p>日文歌詞</p></th>
<th><p>日语读法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 改編

第4段歌詞在日本領土擴張後，[文部省做過以下的修改](../Page/文部省.md "wikilink")

  - 千島の奥も 沖縄も 八洲の外の 守りなり（明治初期的版本）
  - 千島の奥も 沖縄も 八洲の内の
    守りなり（[樺太千島交換條約](../Page/樺太千島交換條約.md "wikilink")・[琉球處分後](../Page/琉球群島歷史.md "wikilink")）
  - 千島の奥も 台湾も 八洲の内の 守りなり（[日清戰爭後](../Page/日清戰爭.md "wikilink")）
  - 台湾の果ても 樺太も 八洲の内の 守りなり（[日俄戰爭後](../Page/日俄戰爭.md "wikilink")）

## 應用

  - 畢業典禮。
  - [NHK](../Page/NHK.md "wikilink")《[紅白歌唱大賽](../Page/紅白歌唱大賽.md "wikilink")》節目最後的合唱第1段。
  - [日本職棒](../Page/日本職棒.md "wikilink")[阪神虎球迷在對方選手退場時合唱第](../Page/阪神虎.md "wikilink")1段。
  - 公共設施、商業設施在開放時間、營業時間即將結束時播放。

## 相關條目

  - [驪歌](../Page/驪歌.md "wikilink")

[Category:日语歌曲](../Category/日语歌曲.md "wikilink")
[Category:畢業歌曲](../Category/畢業歌曲.md "wikilink")
[Category:螢火蟲題材歌曲](../Category/螢火蟲題材歌曲.md "wikilink")
[Category:光題材歌曲](../Category/光題材歌曲.md "wikilink")
[Category:NHK紅白歌合戰演唱歌曲](../Category/NHK紅白歌合戰演唱歌曲.md "wikilink")