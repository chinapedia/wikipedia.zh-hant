**和歌山城**是一座位於[日本](../Page/日本.md "wikilink")[和歌山縣](../Page/和歌山縣.md "wikilink")[和歌山市市中心的梯郭式](../Page/和歌山市.md "wikilink")[平山城](../Page/平山城.md "wikilink")，因為位於[虎伏山上](../Page/虎伏山.md "wikilink")，所以又稱為虎伏城。[紀之川流經和歌山城的北部](../Page/紀之川.md "wikilink")，為城堡天然的[護城河](../Page/護城河.md "wikilink")。它與[松山城](../Page/松山城.md "wikilink")、[姬路城合稱日本三大連立式平山城](../Page/姬路城.md "wikilink")。

和歌山城曾經為[德川御三家中治理](../Page/德川御三家.md "wikilink")[紀伊藩](../Page/紀伊藩.md "wikilink")（又名紀州藩、和歌山藩）的[紀伊德川家的居所](../Page/紀伊德川家.md "wikilink")，紀伊藩的的第五代藩主吉宗和第十三代藩主慶福，後來分別成為幕府第八代將軍[德川吉宗和第十四代將軍](../Page/德川吉宗.md "wikilink")[德川家茂](../Page/德川家茂.md "wikilink")。和歌山城曾經被[日本指定為國寶](../Page/日本.md "wikilink")，但是後來在[第二次世界大戰期間遭到美軍轟炸而燒毀](../Page/第二次世界大戰.md "wikilink")，現在的和歌山城是於1958年所重建完成的。和歌山縣出身的[松下幸之助對於和歌山城重建提供許多資金](../Page/松下幸之助.md "wikilink")

[德川御三家的城郭非常的遼闊](../Page/德川御三家.md "wikilink")，現在和歌山城的面積只有極盛時期的4分之1而已。曾經是和歌山城內的地方包括了現在的和歌山市政府、和歌山市消防局、和歌山地方法院、和歌山家庭裁判所、和歌山地方檢察廳以前官方組織和學校、商業設施、辦公室街與和歌山中央郵局等地區，是規模非常大城堡，但其實本來預定建造的城堡規模是更巨大的。

## 歷史

[豐臣秀吉的唯一弟弟](../Page/豐臣秀吉.md "wikilink")[豐臣秀長在](../Page/豐臣秀長.md "wikilink")[天正](../Page/天正.md "wikilink")13年（1585年）為紀州征伐的副將而參與布陣，平定之後紀伊國與和泉國這兩個國家被納入。當時豐臣秀吉選定虎伏山這個地方（並稱為吹上峰\[1\]）來建城，命築城高手[藤堂高虎來設計建造和歌山城](../Page/藤堂高虎.md "wikilink")。

隔年[天正](../Page/天正.md "wikilink")14年（1586年），[桑山重晴為俸祿](../Page/桑山重晴.md "wikilink")3萬石的和歌山城主，他也對城堡的中心部份進行了整修。[文祿](../Page/文祿.md "wikilink")5年（1596年）桑山重晴去世後，他的孫子[桑山一晴繼任城主](../Page/桑山一晴.md "wikilink")。

[Wakayama_castle05s3200.jpg](https://zh.wikipedia.org/wiki/File:Wakayama_castle05s3200.jpg "fig:Wakayama_castle05s3200.jpg")

[慶長](../Page/慶長.md "wikilink")5年（1600年）發生[關原之戰](../Page/關原之戰.md "wikilink")，戰後桑山一晴被轉封[大和國的](../Page/大和國.md "wikilink")[新莊藩](../Page/新莊藩.md "wikilink")（舊稱布施藩），屬於東軍的[淺野幸長成為](../Page/淺野幸長.md "wikilink")37萬石的紀伊國[和歌山藩的第一代藩主](../Page/和歌山藩.md "wikilink")，以和歌山城為居所，進行了和歌山城的修繕。

[元和](../Page/元和.md "wikilink")5年（1619年），[德川家康的十男](../Page/德川家康.md "wikilink")[德川賴宣成為紀伊國和歌山藩](../Page/德川賴宣.md "wikilink")55萬石藩主，開始[紀伊德川家的時代](../Page/紀伊德川家.md "wikilink")。德川賴宣利用這些錢作為1621年和歌山城與[城下町擴建的基金](../Page/城下町.md "wikilink")。

1655年（[明暦元年](../Page/明暦.md "wikilink")）11月，與西之丸相接的家臣住所發生火災，西之丸與二之丸都遭到波及。1813年（[文化](../Page/文化.md "wikilink")10年），西之丸內部發生火災，西之丸御殿全部燒毀。1846年（[弘化](../Page/弘化.md "wikilink")3年）7月26日，因為發生雷雨，除了大小天守，御殿本丸主要建築物全部燒毀。

[明治](../Page/明治.md "wikilink")4年（1871年）廢藩置縣，和歌山城廢城，許多建築物遭到拆除移往其他地方。其中外城在1885年被移到[大阪城](../Page/大阪城.md "wikilink")，從[昭和](../Page/昭和.md "wikilink")6年（1931年）起被作為[大阪市的迎賓館](../Page/大阪市.md "wikilink")，但是後來在昭和22年（1947年）毀於火災。

1901年（明治34年），本丸・二之丸一帶開放遊客參觀、成為和歌山公園。和歌山城在1931年（昭和6年）被指定為國家[遺址](../Page/遺址.md "wikilink")，然後在昭和10年（1935年）和歌山城[天守等](../Page/天守.md "wikilink")11棟建築根據[國保保存法被指定為](../Page/國保保存法.md "wikilink")[國寶](../Page/國寶.md "wikilink")。

昭和20年（1945年）7月9日，和歌山城遭到美軍空襲轟炸（和歌山大空襲），天守等11棟建築全部燒毀。[財團法人](../Page/財團法人.md "wikilink")[日本城郭協會於](../Page/日本城郭協會.md "wikilink")2006年（[平成](../Page/平成.md "wikilink")18年）2月13日指定和歌山城為[日本100名城第](../Page/日本100名城.md "wikilink")62位。

## 結構

### 本丸

因為本丸御殿位於虎伏山山頂附近，所以藩主並未居住在二之丸御殿。紀州德川家初代藩主[徳川賴宣與正室瑤林院](../Page/徳川賴宣.md "wikilink")（[加藤清正的](../Page/加藤清正.md "wikilink")[女兒](../Page/女兒.md "wikilink")）、紀州德川家第14代藩主[徳川茂承與正室倫宮則子女王](../Page/徳川茂承.md "wikilink")（[伏見宮邦家親王的](../Page/伏見宮邦家親王.md "wikilink")[女兒](../Page/女兒.md "wikilink")）將都定居在邸宅中。因為倫宮則子女王定居於此，所以本丸又被稱為宮様御殿。

1621年建造的本丸庭園採用[寶船外貌來設計](../Page/寶船.md "wikilink")，就像是七福庭園。

### 天守曲輪

天守曲輪是[菱形狀的](../Page/菱形.md "wikilink")[平面地基](../Page/平面.md "wikilink")、基壇面積為2,640m<sup>2</sup>。

  - 大天守
  - 小天守
  - 乾櫓
  - 二之門櫓
  - 天守二之門（楠門）
  - 廚房

#### 天守

[天守是大天守與小天守連結式的建築](../Page/天守.md "wikilink")，被稱為連立式[城堡](../Page/城堡.md "wikilink")。和歌山城與[松山城](../Page/松山城.md "wikilink")、[姬路城合稱日本三大連立式平山城](../Page/姬路城.md "wikilink")。

原先被火燒燬的天守並不確定是何時興建完成的，目前存在淺野幸長創建説（1600年）\[2\]與徳川賴宣創建説（1619年）\[3\]等說法。大天守後來於1850年重建完成，當時是3重且3層建築、天守台平面為菱形。

[天守在昭和](../Page/天守.md "wikilink")10年（1935年）根據[國保保存法被指定為](../Page/國保保存法.md "wikilink")[國寶](../Page/國寶.md "wikilink")，但是後來在昭和20年（1945年）7月9日遭到美軍空襲轟炸（和歌山大空襲）而燒毀。現在的天守是1958年重建完成的，當時是根據1850年天守重建時的木工[水島平次郎後代子孫](../Page/水島平次郎.md "wikilink")[榮三郎所收蔵的天守圖](../Page/水島榮三郎.md "wikilink")，並參考「御天守御普請覚張」來完成的。這次重建工作由[東京工業大學](../Page/東京工業大學.md "wikilink")[名譽教授](../Page/名譽教授.md "wikilink")[藤岡通夫指導下完成](../Page/藤岡通夫.md "wikilink")，以[鋼筋](../Page/鋼筋.md "wikilink")[混泥土建築來重建](../Page/混泥土.md "wikilink")。

### 二之丸

初代藩主[德川賴宣與第](../Page/德川賴宣.md "wikilink")14代藩主[德川茂承以外的藩主都住在二之丸御殿](../Page/德川茂承.md "wikilink")。二之丸御殿與[江戸城本丸御殿結構類似](../Page/江戸城.md "wikilink")，區分為表向、中奥與[大奥](../Page/大奥.md "wikilink")。表向是將軍面見外臣的場所，中奧是將軍辦公起居的處所，大奧則是日本德川幕府將軍的[生母](../Page/生母.md "wikilink")、[正室](../Page/正室.md "wikilink")（御台所）、側室和各女官（稱為「奧女中」）的住處。

### 西之丸

[和歌山城西之丸庭園是江戸時代初期於西之丸御殿建造的](../Page/和歌山城西之丸庭園.md "wikilink")[日本庭園](../Page/日本庭園.md "wikilink")，是藩主的隠居住所，也被稱為紅葉渓庭園。當時根據和歌山城西北麓地形加以活用，設計出鳶魚閣與二段[瀑布](../Page/瀑布.md "wikilink")。後來和歌山城遭到美軍空襲轟炸（和歌山大空襲），鳶魚閣全部燒毀，直到1972年重建。但是鳶魚閣目前的位置與原先不同，原本鳶魚閣應該位在池塘中央附近。紅松庵則是[松下幸之助所捐贈的](../Page/松下幸之助.md "wikilink")[茶室](../Page/茶室.md "wikilink")。西之丸庭園是江戸時代初期遺留下來的[大名庭園](../Page/大名庭園.md "wikilink")，被指定為國家[名勝](../Page/名勝.md "wikilink")。

鶴之渓門遺蹟是淺野時代的鶴之渓庭園殘跡的一部分。西之丸庭園前的山吹谷則是徳川時代的水堀。

### 三之丸

紀州藩重臣[水野家與](../Page/水野氏.md "wikilink")[安藤家](../Page/安藤氏.md "wikilink")、三浦家等上流藩士居住的邸宅。廢藩置縣後，這裡成為公立機構與學校、商業建築、辦公建築等和歌山縣中樞機關的所在。

### 御蔵之丸

沿著東堀的石垣是櫓的遺蹟，城兵昇降時使用的巨大[雁木依然殘留下來](../Page/雁木造.md "wikilink")。

### 松之丸

松之丸塔樓是初代藩主[德川賴宣眺望](../Page/德川賴宣.md "wikilink")[紀州富士](../Page/鄉土富士.md "wikilink")（[龍門山](../Page/龍門山.md "wikilink")），思念故郷[駿河國的地方](../Page/駿河國.md "wikilink")。

## 戲劇

許多日本[時代劇都在和歌山城進行拍攝作業](../Page/時代劇.md "wikilink")，包括《[水戶黄門](../Page/水戶黄門.md "wikilink")》、《[暴坊將軍](../Page/暴坊將軍.md "wikilink")》、《[影武者德川家康](../Page/影武者德川家康.md "wikilink")》、《[德川風雲錄
八代將軍吉宗](../Page/德川風雲錄_八代將軍吉宗.md "wikilink")》、《[天下騷亂〜德川三代的陰謀](../Page/天下騷亂〜德川三代的陰謀.md "wikilink")》與《逃亡者おりん》等。

## 参考文献

  - 西ヶ谷恭弘/編 『定本 日本城郭事典』 秋田書店 2000年 279-280頁 ISBN 4-253-00375-3

  -
  -
## 參考資料

## 外部連結

  - [和歌山城介紹](https://web.archive.org/web/20070419161103/http://www.city.wakayama.wakayama.jp/menu_4/kankou/wakayama.html)

  - [和歌山城管理事務所](https://web.archive.org/web/20070911204129/http://www.city.wakayama.wakayama.jp/menu_1/gyousei/wakayama_siro/menu.html)

  - [和歌山城觀光](http://www.wakanoura.net/w_first1.htm)

  - [和歌山市的文化財](https://web.archive.org/web/20070928041937/http://www.city.wakayama.wakayama.jp/menu_4/bunka/bunka.htm)

  - [和歌山城介紹](https://web.archive.org/web/20070928041946/http://www.city.wakayama.wakayama.jp/english/sightseeing/places/castle.html)

  -
[Category:和歌山縣城堡](../Category/和歌山縣城堡.md "wikilink")
[Category:和歌山縣重要文化財](../Category/和歌山縣重要文化財.md "wikilink")
[Category:和歌山縣史跡](../Category/和歌山縣史跡.md "wikilink")
[Category:日本庭園](../Category/日本庭園.md "wikilink")
[Category:紀伊國](../Category/紀伊國.md "wikilink")
[Category:和歌山市](../Category/和歌山市.md "wikilink")
[Category:紀州藩](../Category/紀州藩.md "wikilink")
[Category:豐臣氏](../Category/豐臣氏.md "wikilink")
[Category:桑山氏](../Category/桑山氏.md "wikilink")
[Category:淺野氏](../Category/淺野氏.md "wikilink")
[Category:福島氏](../Category/福島氏.md "wikilink")
[Category:紀州德川家](../Category/紀州德川家.md "wikilink")

1.  加藤理文執筆『よみがえる日本の城1 大坂城』学習研究社 2004年

2.
3.  西ヶ谷恭弘監修『復原 名城天守』学習研究社 1996年