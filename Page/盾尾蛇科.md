**盾尾蛇科**（Uropeltidae）又名**針尾蛇科**，是[蛇亞目下的一個科](../Page/蛇亞目.md "wikilink")，這個科內的蛇類都具備洞棲性，主要分佈於[印度南部及](../Page/印度.md "wikilink")[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")。盾尾蛇的[學名](../Page/學名.md "wikilink")「Urapeltidae」語源來自[希臘語的](../Page/希臘語.md "wikilink")「ura」及「pelte」，意思分別是「[尾巴](../Page/尾巴.md "wikilink")」與「[盾牌](../Page/盾.md "wikilink")」，表示盾尾蛇的尾部尖端的大型角質盾狀物。目前，盾尾蛇科內有8個屬共47個品種已被確認。\[1\]''

## 特徵

盾尾蛇屬於小型蛇，成年的盾尾蛇大約有20至75[公分長](../Page/公分.md "wikilink")。盾尾蛇棲身於地下，牠們的骨骼構造比較原始，彈性亦不足，顎骨僵硬，骨塊呈四方形，下顎仍然保留喙狀骨，但卻眼窩骨卻已經消失。同時，盾尾蛇頭頂上側的骨骼組織亦呈退化跡象，雙眼細小，而且開始變質（蛇類[眼睛上應有一片薄膜](../Page/眼睛.md "wikilink")，但盾尾蛇的眼睛卻有一塊多邊形的甲塊），[盆骨亦已消失](../Page/盆骨.md "wikilink")。\[2\]另外，針尾蛇的[尾部是最具特色的地方](../Page/尾巴.md "wikilink")。

## 地理分佈

盾尾蛇主要分佈於[印度南部及](../Page/印度.md "wikilink")[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")。\[3\]在印度裡，盾尾蛇主要是沿[西高止山脈分佈](../Page/西高止山脈.md "wikilink")；在斯里蘭卡，盾尾蛇只聚集於氣候與西高止山脈接近的地區，例如斯里蘭卡的西南部。

## 飲食及繁殖

盾尾蛇主要進食[無脊椎動物](../Page/無脊椎動物.md "wikilink")，例如[蚯蚓](../Page/蚯蚓.md "wikilink")。不過相關研究仍相當貧乏，因此盾尾蛇的飲食習慣也許尚有商榷之處。另外，所有盾尾蛇都是[卵胎生動物](../Page/卵胎生.md "wikilink")。

## 品種

| 屬\[4\]                                                                    | 學名及命名者\[5\]                                   | 種數\[6\] | 異名 | 地理分佈\[7\]                                                                           |
| ------------------------------------------------------------------------- | --------------------------------------------- | ------- | -- | ----------------------------------------------------------------------------------- |
| **Brachyophidium**                                                        | Brachyophidium，<small>Wall，1921</small>       | 1       |    | [印度西南部](../Page/印度.md "wikilink")                                                   |
| **[頦溝蛇屬](../Page/頦溝蛇屬.md "wikilink")**                                    | Melanophidium，<small>Günther，1864</small>     | 3       |    | 印度南部[喀拉拉邦及](../Page/喀拉拉邦.md "wikilink")[泰米爾納德邦](../Page/泰米爾納德邦.md "wikilink")       |
| **[扁槌蛇屬](../Page/扁槌蛇屬.md "wikilink")**                                    | Platyplectrurus，<small>Günther，1868</small>   | 2       |    | 印度南部及[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")                                             |
| **[槌尾蛇屬](../Page/槌尾蛇屬.md "wikilink")**                                    | Plectrurus，<small>A.H.A. Duméril，1851</small> | 4       |    | 印度南部                                                                                |
| **[凸尾蛇屬](../Page/凸尾蛇屬.md "wikilink")**                                    | Pseudotyphlops，<small>Schlegel，1839</small>   | 1       |    | 斯里蘭卡中部及[烏沃省與](../Page/烏沃省.md "wikilink")[薩伯勒格穆沃省的南部](../Page/薩伯勒格穆沃省.md "wikilink") |
| **[銼尾蛇屬](../Page/銼尾蛇屬.md "wikilink")**                                    | Rhinophis，<small>Hemprich，1820</small>        | 12      |    | 印度南部及斯里蘭卡                                                                           |
| **[鑽尾蛇屬](../Page/鑽尾蛇屬.md "wikilink")**                                    | Teretrurus，<small>Beddome，1886</small>        | 1       |    | 印度南部                                                                                |
| **[針尾蛇屬](../Page/針尾蛇屬.md "wikilink")**<font size="-1"><sup>T</sup></font> | Uropeltis，<small>Cuvier，1829</small>          | 23      |    | 印度南部及斯里蘭卡                                                                           |
|                                                                           |                                               |         |    |                                                                                     |

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：盾尾蛇](http://www.jcvi.org/reptiles/families/uropeltidae.php)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:盾尾蛇科](../Category/盾尾蛇科.md "wikilink")

1.

2.  Parker HW、Grandison AGC：《Snakes -- a natural
    history》第二版，頁108，[大英博物館及](../Page/大英博物館.md "wikilink")[康乃爾大學出版社](../Page/康乃爾大學.md "wikilink")，1977年。ISBN
    0-8014-1095-9

3.
4.
5.
6.
7.