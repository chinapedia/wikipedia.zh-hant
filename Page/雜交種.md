[Ramphastos_(hybrid)_-Bird_Kingdom-8.jpg](https://zh.wikipedia.org/wiki/File:Ramphastos_\(hybrid\)_-Bird_Kingdom-8.jpg "fig:Ramphastos_(hybrid)_-Bird_Kingdom-8.jpg")的雜交種\]\]
[Netta_peposaca.jpg](https://zh.wikipedia.org/wiki/File:Netta_peposaca.jpg "fig:Netta_peposaca.jpg")（*Netta
peposaca*）與[美洲木鴨](../Page/美洲木鴨.md "wikilink")（*Aix sponsa*）的雜交種\]\]
**杂交种**（）又稱**[杂种](../Page/杂种.md "wikilink")**或**混种**，是指从两种不同的生物杂交产生的后代。

## 存活率和生殖力

依雙親的[生殖隔離程度](../Page/生殖隔離.md "wikilink")，雜交後代可能無法存活、可存活但無法生殖、可存活並生殖但適應力較低、或是完全可以生殖。

如果是同一[种内的不同](../Page/种.md "wikilink")[亚种](../Page/亚种.md "wikilink")、[品系或](../Page/品系.md "wikilink")[栽培種之间相互交配產生的後代](../Page/栽培種.md "wikilink")，通常後代可以正常存活，也被称为种内杂交。有時在同一[属下的不同](../Page/属.md "wikilink")[种之间的互相交配产生的后代也如此](../Page/种.md "wikilink")。種內雜交有時會產生存活率更高的後代，稱為[雜種優勢](../Page/雜種優勢.md "wikilink")(Hybrid
Vigor)。

如果後代完全無法存活或生殖，表示雙親是不同物種，此雜交称为种间杂交（interspecific
hybridization），一般只發生在同屬的生物。
種間雜交是非常困难的，失败率很高，而且通常杂交子代不具生育能力，但也有例外，甚至是更高層級間的雜交。跨屬間雜交的例子有[綿羊和](../Page/綿羊.md "wikilink")[山羊](../Page/山羊.md "wikilink")\[1\]，跨科間雜交的例子有雞和珠雞或雉，但很稀有\[2\]，跨目間雜交有在*[Strongylocentrotus
purpuratus](../Page/Strongylocentrotus_purpuratus.md "wikilink")*的雌海膽和*[Dendraster
excentricus](../Page/Dendraster_excentricus.md "wikilink")*的雄沙錢間找到\[3\]。一般而言动物產生雜交種（譬如[骡子和](../Page/骡子.md "wikilink")[獅虎](../Page/獅虎.md "wikilink")）是在被人類强行干预之下才會发生，因此刻意製造雜交種在倫理學上是有爭議的行為。

有時候雜交後代只有其中一個性別無法正常生殖或存活，通常這個性別是異配性別（性染色體為XY或WZ），此規則稱為[霍爾登氏法則](../Page/霍爾登氏法則.md "wikilink")。

## 雜交種化

有時會發生雜交後代可以自交或無性生殖，但無法與親代物種回交之情況，這種案例中雜交種即形成一新的物種。這種現象在植物中較常見，研究估計2\~4%的[被子植物和](../Page/被子植物.md "wikilink")7%的蕨類是雜交種化產生，在農作物中比例更高\[4\]。在動物中也有少數昆蟲和魚類的例子\[5\]。

## 參考資料

## 參見

  - [近親繁殖](../Page/近親繁殖.md "wikilink")
  - [混血](../Page/混血.md "wikilink")
  - [杂种优势](../Page/杂种优势.md "wikilink")

## 外部連結

  - [Hybrid
    Mammals](http://www.messybeast.com/genetics/hybrid-mammals.html)
  - [Hybrids of wildcats with domestic
    cats](http://www.messybeast.com/small-hybrids/hybrids.htm)
  - [Domestic Fowl
    Hybrids](http://www.feathersite.com/Poultry/BRKHybrids.html)
  - [Artificial
    Hybridisation](http://www.orchids.co.in/plant-facts/artificial-hybridisation.shtm)
    - Artificial Hybridisation in orchids
  - [Scientists Create Butterfly
    Hybrid](http://www.livescience.com/animalworld/060614_butterfly_hybrid.html)
    - Creation of new species through hybridization was thought to be
    common only in plants, and rare in animals.
  - [Hybridisation in
    animals](http://news.nationalgeographic.com/news/2005/07/0727_050727_evolution.html)
    Evolution Revolution: Two Species Become One, Study Says\]
    (nationalgeographic.com)

[雜交種](../Category/雜交種.md "wikilink")
[Category:自然保育](../Category/自然保育.md "wikilink")
[Category:物种形成](../Category/物种形成.md "wikilink")

1.
2.
3.
4.
5.