**杜伊斯堡**（[德語](../Page/德語.md "wikilink")：，发音：）位于[鲁尔区西部的](../Page/鲁尔区.md "wikilink")[下莱茵](../Page/莱茵河#下莱茵.md "wikilink")。它是[杜塞尔多夫行政区里的一个](../Page/杜塞尔多夫行政区.md "wikilink")[獨立市](../Page/獨立市.md "wikilink")，同时也是[北莱茵-威斯特法伦州的第五大城市](../Page/北莱茵-威斯特法伦.md "wikilink")。

[杜伊斯堡-鲁罗尔特港](../Page/杜伊斯堡-鲁罗尔特港.md "wikilink")，其市中心坐落在[鲁罗尔特区](../Page/鲁罗尔特.md "wikilink")，是欧洲最大的[内陆港](../Page/内陆港.md "wikilink")。杜伊斯堡市内一所[大学于](../Page/大学.md "wikilink")2003年和临近的[埃森大学重组为](../Page/埃森大学.md "wikilink")[杜伊斯堡-埃森大学](../Page/杜伊斯堡-埃森大学.md "wikilink")。

## 地名发音问题

事实上，德文“Duisburg”的发音应当是，字母组合ui在德国西北部地名中发音为\[yː\]，相当于发长音的字母“ü”。“杜伊斯堡”的译法或为不知其发音的前人根据“Duisburg”的拼写推测出来的误译，根据实际发音其本应译作“迪斯堡”，但“杜伊斯堡”之称呼已成惯例，故保留沿用至今。详情可参见该条目和德文条目。

## 地理情况

### 地理位置

杜伊斯堡位于丘陵地带的边上，[莱茵河的](../Page/莱茵河#下莱茵.md "wikilink")[鲁尔河的汇合口](../Page/鲁尔河.md "wikilink")。市区范围覆盖河流的两岸。城市的北部与旧Emsche和莱茵河上的小Emscher接壤。[联邦州规划时决定把杜伊斯堡发展成](../Page/联邦州规划.md "wikilink")[区域中心](../Page/区域中心.md "wikilink")。

### 毗邻城市

杜伊斯堡市西面和北面与[韦塞尔县的](../Page/韦塞尔县.md "wikilink")[默尔斯市](../Page/默尔斯.md "wikilink")、[莱茵贝格市和](../Page/莱茵贝格.md "wikilink")[丁斯拉肯市](../Page/丁斯拉肯.md "wikilink")，东面与[奥伯豪森市和](../Page/奥伯豪森.md "wikilink")[米尔海姆市](../Page/米尔海姆.md "wikilink")，南面与[梅特曼县的](../Page/梅特曼县.md "wikilink")[拉廷根市](../Page/拉廷根.md "wikilink")、[杜塞尔多夫市](../Page/杜塞尔多夫.md "wikilink")、[诺伊斯莱茵县的](../Page/诺伊斯莱茵县.md "wikilink")[梅尔布施市和](../Page/梅尔布施.md "wikilink")[克雷费尔德市相邻](../Page/克雷费尔德.md "wikilink")。

### 城市构成

杜伊斯堡市区由七个[城域组成](../Page/德国行政区划分#城域.md "wikilink")，共细分为46个[分区](../Page/德国行政区划分#分区.md "wikilink")。

## 历史

### 名字起源

杜伊斯堡的第一个音素可追溯到[日耳曼语的](../Page/日耳曼语.md "wikilink")“deus”，大概意为“潮湿的地区”或“浸水地”。“杜伊斯堡”因此意味着“浸水地中的城堡”。

名字的另外一个意义是从古德语“duis”，即小山（Hügel）演化过来的。因此“杜伊斯堡”可大概理解为“小山上的城堡”。
由此那个伫立在莱茵河边小丘上的，古时带着护城河、壁垒、栅栏的宫殿变成了如今的市政厅。

杜伊斯堡这个名字在欧洲并不唯一。[比利时](../Page/比利时.md "wikilink")[特尔菲伦东部的一片区域也叫做杜伊斯堡](../Page/特尔菲伦.md "wikilink")。
在[荷兰的](../Page/荷兰.md "wikilink")[海尔德兰省有一个城市叫](../Page/海尔德兰省.md "wikilink")[Doesburg](../Page/Doesburg.md "wikilink")。

### 近罗马时代

[De_merian_Westphaliae_111.jpg](https://zh.wikipedia.org/wiki/File:De_merian_Westphaliae_111.jpg "fig:De_merian_Westphaliae_111.jpg")
考古挖掘已经找到了证据，在公元前一世纪的时候，在杜伊斯堡曾有一个可防水淹的城堡区域。
其中“旧市场”这片区域自公元5世纪即为城市的中心市集，而杜伊斯堡所在的“Hellweg”（意：光明之路，为中世纪重要的商贸通道），有其得天独厚的地理优势，使该城在莱茵河畔显得独一无二。其时[罗马人常派兵驻守着这个莱茵的渡江口](../Page/罗马帝国.md "wikilink")。

  - 420年来自内日尔曼的[Franken人从罗马人手上纂夺了居住地和老城区](../Page/Franken.md "wikilink")。
  - 883年[Norman人和](../Page/Norman.md "wikilink")[维京人征服了杜伊斯堡并在此过冬](../Page/维京.md "wikilink")。最早有关杜伊斯堡的文字记录出自[Regino
    von
    Prüm年鉴](../Page/Regino_von_Prüm.md "wikilink")。[特里尔的城市图书馆藏有该文献](../Page/特里尔.md "wikilink")。

### [中世纪](../Page/中世纪.md "wikilink")

杜伊斯堡坐落在莱茵河和鲁尔河的汇流处的高地上。由于其天然优越的地理位置，大概在740年间，一座皇帝行宫在此修建。10世纪被扩建为[Königspfalz](../Page/Königspfalz.md "wikilink")。

### 现代和[工业化时代](../Page/工业化.md "wikilink")

[Bundesarchiv_Bild_183-R23152,_Mülheim,_Industriehafen.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-R23152,_Mülheim,_Industriehafen.jpg "fig:Bundesarchiv_Bild_183-R23152,_Mülheim,_Industriehafen.jpg")
[全球经济危机在](../Page/全球经济危机.md "wikilink")20世纪30年代初对该城冲击特别严重。那时杜伊斯堡的[失业率是整个帝国最高的](../Page/失业率.md "wikilink")。

### [第二次世界大战](../Page/第二次世界大战.md "wikilink")

作为鲁尔区的物资中转站和著名的化工和冶金中心，杜伊斯堡理所当然的成为盟军轰炸的首要目标。除了工业设施外，平民也成为轰炸目标。

1936年后，杜伊斯堡又开始有军队入驻。1936年5月7日，[纳粹政府公然无视](../Page/纳粹政府.md "wikilink")[凡尔赛和约和](../Page/凡尔赛和约.md "wikilink")[洛迦诺公约](../Page/洛迦诺公约.md "wikilink")，向[莱茵非军事区派驻军队](../Page/莱茵非军事区.md "wikilink")，第79步兵团的一个营进驻杜伊斯堡市，即所谓的“再军事化”。

1941年
仅在6月12日／6月13日，英国轰炸机就投放了445吨炸弹。虽然城市的北部在5月28日时战争已经结束，但是鲁尔河以南的战争直到1945年4月12日，随着美第9军第17空军师的进驻才结束。[二战中的至少](../Page/二战.md "wikilink")299次轰炸行动几乎将老城完全摧毁。80%的住房被完全摧毁或严重损坏。整个城市包括基础设施在战后几乎是完全新建的，大部分的历史遗迹都不复存在了。

### 当代

[Bundesarchiv_B_145_Bild-F015018-0005,_Walsum,_Kohlebergbau.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_B_145_Bild-F015018-0005,_Walsum,_Kohlebergbau.jpg "fig:Bundesarchiv_B_145_Bild-F015018-0005,_Walsum,_Kohlebergbau.jpg")
20世纪末杜伊斯堡面临着许多结构问题亟需解决。钢铁工业的没落使很多原本赖以为生的工人失去了工作。1986年位于Rheinhausen的Krupp-Hütte的员工试图阻止工厂的关闭，然而没有成功。

城市人口的急剧下降导致了购买力的流失。杜伊斯堡对消费者和企业的吸引力在渐渐消失，因此市政府想方设法改善这种状况。由此数年前在市中心的繁华地带建造大型购物中心的计划就被提上日程。然而市议会在2005年中做出了一个颇具争议的决定--它并没有批准建设计划，而是将投资者选中的地块划为特殊地区，所以这些工程的未来还充满了未知数。

## 宗教

### 基督教

杜伊斯堡在中世纪时隶属[列日主教区](../Page/列日.md "wikilink")，后来划归[科隆大主教区](../Page/科隆大主教区.md "wikilink")。

### 犹太教

杜伊斯堡-穆尔海姆／鲁尔-奥柏侯逊的[犹太教社团](../Page/犹太教.md "wikilink")|组织有2773位成员。

### 伊斯兰教

大概有8%的杜伊斯堡居民宣布自己为伊斯兰教徒。

## 政治和社会

杜伊斯堡自约1270年始就存在一个[地方议会并且从](../Page/地方议会.md "wikilink")1275年开始就有两位[市长](../Page/市长.md "wikilink")。以前该议会由10到22名成员组成。议会的选举制度也历经了多次的变动。原则上每年8月10日
更换成员一次。这一天，德语称作Laurentiustag，即Saint Lawrence's
Day，圣劳伦斯日，是基督教的节日。自1566年始Kleve公爵（Herzog
von
Kleve）可自行任命市长和两位参议员。然而该公爵几乎从未行使其特权。除地方议会外，15世纪开始还出现了一个由市民组成的委员会，也就是所谓的“16人委员会”（Sechzehner）。这16名委员将从杜伊斯堡的四个城区分选出来，每个城区四名代表。除此之外还有一个更大的委员会“24人委员会”。然而这并不能从当今的意义上被理解为“市民参政”。那时的委员会只有提意见的功能。随后，自由选举在1713年被临时取消了。在1807年的法占时期，建立市政宪法和市政议会。

[纳粹德國时代](../Page/纳粹德國.md "wikilink")，总市长由[纳粹党指定](../Page/纳粹党.md "wikilink")。[二战后](../Page/第二次世界大战.md "wikilink")[英军驻地的](../Page/英军驻地.md "wikilink")[军事政府指定了一个新的总市长](../Page/军事政府.md "wikilink")，并在1946年发表了英国模式的市政纲领。然后才有了一个由市民选举的*城市议会*（Rat
der
Stadt），其议员被称作“Stadtverordnete”。一开始，该城市议会从市中心选出一名*总市长*作为城市的主席和代表，义务地为人民服务。同样地，从1946年开始，议会选出一名专职的*城市总领事*作城市管理的领航人。然而从1997年开始，人们放弃了这种“一城双首”的城市管理模式。从此只有一位专职的*总市长*，她／他兼任议会主席和城市管理人。

在2004年才选举出第一位不属于[社民党的总市长](../Page/社民党.md "wikilink")。这之前有超过50年，该位置牢牢掌握于社民党之手。[Adolf
Sauerland在第二轮选举以](../Page/Adolf_Sauerland.md "wikilink")61.2%的得票率击败其前任社民党的[Bärbel
Zieling](../Page/Bärbel_Zieling.md "wikilink")。

每一个城区都有一个*区域代表*，每届*市政选举*都会重新选出这些代表。[德国基督教民主联盟在上届选举中赢得四名主管代表](../Page/德国基督教民主联盟.md "wikilink")，即区域代表主席，他们是Katharina
Gottschling（Rheinhausen），Hildegard Fischer（荷柏格／鲁罗尔特／Baerl），Heinrich
Klose（Stadtmitte），Arno
Müller（Süd）。另外三名主管代表隶属[社民党](../Page/社民党.md "wikilink")：Heinz
Plückelmann（Walsum），Uwe Heider（汉博恩），Wilhelm Jankowski（Meiderich/Beeck）.

### 城市议会

杜伊斯堡的城市议会有74名议员。2004年9月的选举中，其议员分布于以下政党：

选举后不久DAL就从基民盟中脱离出来，基民盟和绿党签订了合作条约，然而再加上总市长Sauerland（基民盟）在议会中还是不占优。

### 城市徽章

杜伊斯堡的[徽章是一个红黄分割的盾牌状图形](../Page/徽章.md "wikilink")。上半部分为一只黑色的展翅双头鹰，下半部分为三个相连的银白色城堡。市旗为红白色，并镶有城市徽章。1977年1月31日行政专区主席在[杜塞尔多夫授予杜伊斯堡该城市徽章](../Page/杜塞尔多夫.md "wikilink")。人们戏称之为“会说话”的徽章，因为该徽章里包含了城堡的图案，也正暗示着杜伊斯“堡”。*帝国之鹰*，即黑色的展翅双头鹰，表示隶属于德意志帝国，也同时暗示着帝国的自由。

杜伊斯堡的城市徽章受市政的徽章法保护。须经允许才能使用该徽章。

### 著名建筑

  - 市政厅：杜伊斯堡市政厅建成于1902年，它是一座早期[文艺复兴时期风格的建筑](../Page/文艺复兴.md "wikilink")。而在[中世纪时期](../Page/中世纪.md "wikilink")，这里曾是王室花园的一部分。
  - **教堂**
      - [Salvatorkirche](../Page/Salvatorkirche_（杜伊斯堡）.md "wikilink")：紧临市政厅的Salvatorkirche在中世纪时期也是王家花园的一部分。公元9世纪时这里就建起了第一座教堂，但于13世纪时毁于大火。1415年重建。1571年后这座座落在港区边上的Salvatorkirche成为[基督教新教的教堂](../Page/基督教新教.md "wikilink")。
      - Liebfrauenkirche：1960年，在战争中被摧毁的Stadtkirche的废墟上建成了Liebfrauenkirche，Stadtkirche原本是杜伊斯堡市历史最悠久的天主教堂。

## 基础设施和经济

多家德國或跨國大企業在杜伊斯堡設立總部，或者分支機構：

  - HAVI Logistics：食品物流公司
  - [德鐵信可鐵路](../Page/德鐵信可鐵路.md "wikilink")
  - [英飛凌](../Page/英飛凌.md "wikilink")：半導體公司
  - [西門子能源](../Page/西门子能源业务领域.md "wikilink")
  - [蒂林克虜伯鋼鐵](../Page/蒂森克虜伯.md "wikilink")
  - [安賽樂米塔爾](../Page/安賽樂米塔爾.md "wikilink")：鋼鐵公司
  - 三菱日立能源系統
  - [英特爾](../Page/英特爾.md "wikilink")

### 交通

杜伊斯堡是一个国际性的贸易和物流中心，拥有十分发达的公路，铁路和航运网。从杜伊斯堡火车总站出发，八分钟就能到达位于杜伊斯堡与[杜塞尔多夫交界处的杜塞尔多夫国际机场](../Page/杜塞尔多夫.md "wikilink")。

#### 港口

[0049_innenhafen_duisburg.JPG](https://zh.wikipedia.org/wiki/File:0049_innenhafen_duisburg.JPG "fig:0049_innenhafen_duisburg.JPG")
杜伊斯堡港“duisport”是欧洲最大的内河港。它还是一个海港，海船沿莱茵河入海驶往欧洲，非洲和近东地区。港区的中心位于鲁尔河的河口，它已有200年的历史。杜伊斯堡港的年吞吐量大约4000万吨。每年有超过20,000艘船只在这里停泊。港区占地740公顷，占地180余公顷的21个码头绵延40公里长的河岸。

此外杜伊斯堡港还有占地265公顷的物流仓库。如果加上许多大型公司在这里用自己的码头转运的货物，杜伊斯堡港的总吞吐量可以达到7000万吨。比较：
汉堡的货物吞吐量为1亿1500万吨。

#### 铁路

1846年杜伊斯堡就通过[Cöln-Mindener
Eisenbahn与铁路网相连](../Page/Cöln-Mindener_Eisenbahn.md "wikilink")。杜伊斯堡火车总站是一个长途火车站，这里每天都有有开往[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")，[柏林](../Page/柏林.md "wikilink")，[法兰克福和](../Page/法兰克福.md "wikilink")[汉堡方向的](../Page/汉堡.md "wikilink")[ICE](../Page/InterCityExpress.md "wikilink")（城际特快列车）。

[S-Bahn在杜伊斯堡的近距离交通中只充当次要的角色](../Page/S-Bahn.md "wikilink")，因为它只在城市南部拓展。有轨电车却同时也发展到杜伊斯堡的北部。

## 友好城市

杜伊斯堡和以下城市为[友好城市](../Page/友好城市.md "wikilink")：

  - [朴茨茅斯](../Page/朴茨茅斯.md "wikilink")，1950年起

  - [加来](../Page/加来.md "wikilink")，1964年起

  - [洛美](../Page/洛美.md "wikilink")，1973年起

  - [武汉](../Page/武汉.md "wikilink")，1982年起

  - [維爾紐斯](../Page/維爾紐斯.md "wikilink")，1985年起

  - [加济安泰普](../Page/加济安泰普.md "wikilink")，2005年起

[杜伊斯堡](../Category/杜伊斯堡.md "wikilink")
[D](../Category/北莱茵-威斯特法伦州市镇.md "wikilink")
[D](../Category/汉萨同盟.md "wikilink")
[Duisburg](../Category/世界運動會主辦城市.md "wikilink")