**曲棍球冠軍盃**（Hockey Champions
Trophy），是[國際曲棍球聯合會一年一度舉辦的大型世界級曲棍球比賽](../Page/國際曲棍球聯合會.md "wikilink")。

## 历届比赛

<table>
<thead>
<tr class="header">
<th><p>align ="center" rowspan=2 width=5%|年份</p></th>
<th><p>align ="center" rowspan=2 width=15%|主办地</p></th>
<th><p>align ="center" colspan=3|决赛</p></th>
<th><p>align ="center" colspan=3|三四名决赛</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>align ="center" width=15%|金牌</p></td>
<td><p>align ="center" width=5%|比分</p></td>
<td><p>align ="center" width=15%|银牌</p></td>
<td><p>align ="center" width=15%|铜牌</p></td>
</tr>
<tr class="even">
<td><p>1978年</p></td>
<td><p><a href="../Page/Lahore.md" title="wikilink">Lahore</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1980年</p></td>
<td><p><a href="../Page/Karachi.md" title="wikilink">Karachi</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td><p><a href="../Page/Karachi.md" title="wikilink">Karachi</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1982年</p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1983年</p></td>
<td><p><a href="../Page/Karachi.md" title="wikilink">Karachi</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td><p><a href="../Page/Karachi.md" title="wikilink">Karachi</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td><p><a href="../Page/Perth,_Western_Australia.md" title="wikilink">Perth</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1986年</p></td>
<td><p><a href="../Page/Karachi.md" title="wikilink">Karachi</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1987年</p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1988年</p></td>
<td><p><a href="../Page/Lahore.md" title="wikilink">Lahore</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1989年</p></td>
<td><p><a href="../Page/Berlin.md" title="wikilink">Berlin</a>, <a href="../Page/West_Germany.md" title="wikilink">West Germany</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990年</p></td>
<td><p><a href="../Page/Melbourne.md" title="wikilink">Melbourne</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><a href="../Page/Berlin.md" title="wikilink">Berlin</a>, <a href="../Page/Germany.md" title="wikilink">Germany</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p><a href="../Page/Karachi.md" title="wikilink">Karachi</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>4–0</strong></p></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p><a href="../Page/Kuala_Lumpur.md" title="wikilink">Kuala Lumpur</a>, <a href="../Page/Malaysia.md" title="wikilink">Malaysia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>4–0</strong></p></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p><a href="../Page/Lahore.md" title="wikilink">Lahore</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–2<br />
(7–6)</strong><br />
<a href="../Page/Penalty_shootout.md" title="wikilink">Penalty strokes</a></p></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p><a href="../Page/Berlin.md" title="wikilink">Berlin</a>, <a href="../Page/Germany.md" title="wikilink">Germany</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–2<br />
(4–2)</strong><br />
<a href="../Page/Penalty_shootout.md" title="wikilink">Penalty strokes</a></p></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p><a href="../Page/Chennai.md" title="wikilink">Madras</a>, <a href="../Page/India.md" title="wikilink">India</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–2</strong></p></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p><a href="../Page/Adelaide.md" title="wikilink">Adelaide</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–2</strong><br />
<a href="../Page/Extra_time.md" title="wikilink">after extra time</a></p></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/Lahore.md" title="wikilink">Lahore</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–1</strong></p></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/Brisbane.md" title="wikilink">Brisbane</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–1</strong></p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–1</strong><br />
<a href="../Page/Extra_time.md" title="wikilink">after extra time</a></p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/Rotterdam.md" title="wikilink">Rotterdam</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–1</strong></p></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/Cologne.md" title="wikilink">Cologne</a>, <a href="../Page/Germany.md" title="wikilink">Germany</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>0–0<br />
(3–2)</strong><br />
<a href="../Page/Penalty_shootout.md" title="wikilink">Penalty strokes</a></p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>4–2</strong></p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/Lahore.md" title="wikilink">Lahore</a>, <a href="../Page/Pakistan.md" title="wikilink">Pakistan</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>4–2</strong></p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/Chennai.md" title="wikilink">Chennai</a>, <a href="../Page/India.md" title="wikilink">India</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–1</strong></p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/Terrassa.md" title="wikilink">Terrassa</a>, <a href="../Page/Spain.md" title="wikilink">Spain</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–1</strong></p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/Kuala_Lumpur.md" title="wikilink">Kuala Lumpur</a>, <a href="../Page/Malaysia.md" title="wikilink">Malaysia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>1–0</strong></p></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/Rotterdam.md" title="wikilink">Rotterdam</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>4–1</strong></p></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/Melbourne.md" title="wikilink">Melbourne</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>5–3</strong></p></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/Mönchengladbach.md" title="wikilink">Mönchengladbach</a>, <a href="../Page/Germany.md" title="wikilink">Germany</a></p></td>
<td></td>
<td><p><strong>4–0</strong></p></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/New_Delhi.md" title="wikilink">New Delhi</a>, <a href="../Page/India.md" title="wikilink">India</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/Argentina.md" title="wikilink">Argentina</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 女子

<table>
<thead>
<tr class="header">
<th><p>align ="center" rowspan=2 width=5%|年份</p></th>
<th><p>align ="center" rowspan=2 width=15%|主办地</p></th>
<th><p>align ="center" colspan=3|决赛</p></th>
<th><p>align ="center" colspan=3|三四名决赛</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>align ="center" width=15%|金牌</p></td>
<td><p>align ="center" width=5%|比分</p></td>
<td><p>align ="center" width=15%|银牌</p></td>
<td><p>align ="center" width=15%|铜牌</p></td>
</tr>
<tr class="even">
<td><p>1987年</p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/Frankfurt.md" title="wikilink">Frankfurt</a>, <a href="../Page/West_Germany.md" title="wikilink">West Germany</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><a href="../Page/Berlin.md" title="wikilink">Berlin</a>, <a href="../Page/Germany.md" title="wikilink">Germany</a></p></td>
<td><p><strong></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>1–1</strong><br />
<strong>(4–2)</strong><br />
<a href="../Page/Penalty_shootout.md" title="wikilink">Penalty strokes</a></p></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p><a href="../Page/Mar_del_Plata.md" title="wikilink">Mar del Plata</a>, <a href="../Page/Argentina.md" title="wikilink">Argentina</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>1–1</strong><br />
<strong>(4–3)</strong><br />
<a href="../Page/Penalty_shootout.md" title="wikilink">Penalty strokes</a></p></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p><a href="../Page/Berlin.md" title="wikilink">Berlin</a>, <a href="../Page/Germany.md" title="wikilink">Germany</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–1</strong><br />
<a href="../Page/Extra_time.md" title="wikilink">after extra time</a></p></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/Brisbane.md" title="wikilink">Brisbane</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–2</strong></p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–2</strong></p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–2</strong></p></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p><a href="../Page/澳门.md" title="wikilink">澳门</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–2</strong><br />
<strong>(3–1)</strong><br />
<a href="../Page/Penalty_shootout.md" title="wikilink">Penalty strokes</a></p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/Sydney.md" title="wikilink">Sydney</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–2</strong></p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/Rosario.md" title="wikilink">Rosario</a>, <a href="../Page/Argentina.md" title="wikilink">Argentina</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>2–0</strong></p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/Canberra.md" title="wikilink">Canberra</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>0–0</strong><br />
<strong>(5–4)</strong><br />
<a href="../Page/Penalty_shootout.md" title="wikilink">Penalty strokes</a></p></td>
</tr>
<tr class="odd">
<td><p>2006年<br />
<em><a href="../Page/2006年世界女子曲棍球冠軍盃.md" title="wikilink">详细</a></em></p></td>
<td><p><a href="../Page/Amstelveen.md" title="wikilink">Amstelveen</a>, <a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>3–2</strong></p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/Quilmes.md" title="wikilink">Quilmes</a>, <a href="../Page/Argentina.md" title="wikilink">Argentina</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>1–0</strong></p></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/Mönchengladbach.md" title="wikilink">Mönchengladbach</a>, <a href="../Page/Germany.md" title="wikilink">Germany</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>6–2</strong></p></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/Sydney.md" title="wikilink">Sydney</a>, <a href="../Page/Australia.md" title="wikilink">Australia</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>0–0</strong><br />
<strong>(4–3)</strong><br />
<a href="../Page/Penalty_shootout.md" title="wikilink">Penalty strokes</a></p></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/Nottingham.md" title="wikilink">Nottingham</a>, <a href="../Page/England.md" title="wikilink">England</a></p></td>
<td><p><strong></strong></p></td>
<td><p><strong>4–2</strong></p></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/Netherlands.md" title="wikilink">Netherlands</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/Argentina.md" title="wikilink">Argentina</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/India.md" title="wikilink">India</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:曲棍球](../Category/曲棍球.md "wikilink")