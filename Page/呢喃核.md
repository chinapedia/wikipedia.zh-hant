**呢喃核**（）是[美国](../Page/美国.md "wikilink")[独立电影界在](../Page/独立电影.md "wikilink")2000年之后获得关注的一项运动。这个流派的电影特点是由超低预算制片（常常由手持的[数码摄影机完成](../Page/数码摄影机.md "wikilink")）、关注20岁左右年轻人的情感生活、[即兴创作的](../Page/即兴.md "wikilink")[剧本](../Page/剧本.md "wikilink")、非专业[演员出演](../Page/演员.md "wikilink")。可以被划分为这个流派的导演有：、、、
以及 。

呢喃核这个称谓最早是由的一位音响编辑提出来的。\[1\]平时也以“”的叫法出现。影评人也有把这个类型的电影叫做“”或者“”，后者用以向独立电影导演致敬。

2007年，[独立电影频道中心在纽约市开办了一个以](../Page/独立电影频道.md "wikilink")10部呢喃核电影短片为主题的影展：。

## 参考文献

## 相关条目

  - [法國新浪潮](../Page/法國新浪潮.md "wikilink")

[\*](../Category/美国独立電影.md "wikilink")
[Category:電影運動](../Category/電影運動.md "wikilink")
[Category:电影类型](../Category/电影类型.md "wikilink")

1.  [纽约时报 (2007年8月19日): "A Generation Finds Its Mumble" by Dennis
    Lim](http://www.nytimes.com/2007/08/19/movies/19lim.html)