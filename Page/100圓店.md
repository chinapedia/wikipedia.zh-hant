[Daiso_100Yen.jpg](https://zh.wikipedia.org/wiki/File:Daiso_100Yen.jpg "fig:Daiso_100Yen.jpg")
[100-Emon.jpg](https://zh.wikipedia.org/wiki/File:100-Emon.jpg "fig:100-Emon.jpg")

**100圓店**是[日本](../Page/日本.md "wikilink")20世紀末[泡沫經濟破滅後不景氣期間大為流行的](../Page/日本泡沫經濟.md "wikilink")[零售商店](../Page/零售商店.md "wikilink")。店內商品大部份劃一價錢為100[日圓](../Page/日圓.md "wikilink")，消費模式風行全國。其後，同類模式店鋪也出現於世界其他地方，但則因應商品收費稱為「1元店」、「10元店」等。廣義上，所謂的「百元商店」通常是指只要一枚[硬幣就可以消費的廉價店鋪](../Page/硬幣.md "wikilink")，而硬幣消費額則以各個地區最大面額的硬幣為售價上限。

日本一般的100圓店售賣的貨品有[成衣](../Page/成衣.md "wikilink")、[家庭用品](../Page/家庭用品.md "wikilink")、[裝飾品](../Page/裝飾品.md "wikilink")、[文具](../Page/文具.md "wikilink")、[盆栽以至](../Page/盆景.md "wikilink")[食品等日用品](../Page/食品.md "wikilink")。大部份貨品以100[日圓出售](../Page/日圓.md "wikilink")，加上8%[消费税後實收](../Page/消费税.md "wikilink")108日圓。在競爭激烈下，更出現99圓店甚至88圓店；顧名思義，貨品以更便宜的99日圓或88日圓出售。

現時日本最大規模的100圓店集團為[大創產業](../Page/大創百貨.md "wikilink")，首間商店於1991年開業，現在日本約有1,300間分店，每月以40間的速度增長。100圓店由於可以大量入貨，並主要售賣低成本地區如[中国生產的貨品以賺取較高](../Page/中国.md "wikilink")[利潤](../Page/利潤.md "wikilink")。

## 其他地區

：同類型的店舖稱為「生活廣場」，曾有類似店鋪「10蚊店」，但現在已經改變，所有貨品為12元。亦曾有**8蚊店**的出現。香港大部分生活廣場均為連鎖式經營，著名有[日本城](../Page/日本城.md "wikilink")、[吉之島](../Page/吉之島.md "wikilink")10元廣場（獨立10元店）及部分貨架等。[百佳則曾經在](../Page/百佳.md "wikilink")[超級市場內開設](../Page/超級市場.md "wikilink")「8蚊專區」。在[大埔墟](../Page/大埔墟.md "wikilink")、[太子等地更曾有](../Page/太子_\(香港\).md "wikilink")「兩蚊店」出現，所有貨品為2元，惟大埔墟的兩蚊店已結業。香港的生活廣場的貨品與日本的100圓店相若，但以匯率計香港的貨品比日本貴（12港元約等如150[日圓](../Page/日圓.md "wikilink")）。

：同類型的店舖稱為「十元商店」、「10元店」，大部分貨品以新台幣10元出售。另外台灣還有一些從日本引入的大創39元店。台灣當地的廉價商店售價通常不超過最大面額的台幣五十元銅板。

、：同類型的店舖稱為「1元店」（），大部分貨品以當地貨幣1元（[美元或](../Page/美元.md "wikilink")[加元](../Page/加拿大元.md "wikilink")）出售。也曾出現「99分店」（99￠
store）。

、：同類店鋪稱為「2元店」（$2
shop），大部分貨品以當地貨幣2元（[澳大利亚元或](../Page/澳大利亚元.md "wikilink")[新西蘭元](../Page/新西蘭元.md "wikilink")）出售。

：同類型的店舖稱為「1鎊店」（One Pound
Shop），最普遍的為連鎖店為「一鎊之地」（Poundland）大部分貨品以當地貨幣1[英鎊出售](../Page/英鎊.md "wikilink")。

：[飛虎](../Page/飛虎_\(雜貨\).md "wikilink")，發跡於[哥本哈根](../Page/哥本哈根.md "wikilink")，價格親民，在世界各地分店超過500間。

## 參考資料

  - [日本有趣的百元商店](https://web.archive.org/web/20060828190200/http://www.visit-japan.jp/supporter/02_06.html)
    Yokoso Japan

## 參見

  -
  -
## 外部連結

  - 日本－大型100圓店

<!-- end list -->

  - [Daiso](http://www.daiso-sangyo.co.jp/)
  - [Cando](http://www.cando-web.co.jp/)
  - [TOKUTOKUYA](http://www.tokutokuya.com/)
  - [Seria](http://www.seria-group.com/)
  - [Watts](http://www.watts-jp.com/)
  - [Kyukyu Plus (99 Yen Shop)](http://www.shop99.co.jp/)

<!-- end list -->

  - 香港

<!-- end list -->

  - [JUSCO 10元廣場](http://www.jusco.com.hk/chi/10_plaza/)

<!-- end list -->

  - 英國

<!-- end list -->

  - [Poundland](http://www.poundland.com/)
  - [TOKUTOKUYA](https://web.archive.org/web/20110523035634/http://www.tokutokuya.com/en/index.html)

[Category:日本零售商](../Category/日本零售商.md "wikilink")
[Category:零售](../Category/零售.md "wikilink")