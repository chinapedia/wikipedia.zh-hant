**保良局馬錦明中學**（，簡稱MKM或保馬），為[香港](../Page/香港.md "wikilink")[粉嶺男女子](../Page/粉嶺.md "wikilink")[中學](../Page/中學.md "wikilink")，於1995年創辦，為[香港慈善團體](../Page/香港慈善機構列表.md "wikilink")[保良局屬下第](../Page/保良局.md "wikilink")10所[政府資助男女文法](../Page/香港中學教育.md "wikilink")[中學](../Page/中學.md "wikilink")。該校校監為保良局之應屆主席，校長為[夏文亮先生](../Page/夏文亮.md "wikilink")。現時教職員共60人（已包括校長及外籍英語教師一名）。

## 架構

  - 中一至中三：各四班，每班約36人
  - 中四：五班，每班約36人
  - 中五至中六級：各六班，平均每班約38人

## 校舍

校舍舊翼於1996年啟用，於舊翼校舍未建成之前，曾借用田家炳中學的校舍。
2003年夏天，開始動工興建新翼（馬錦明博士紀念大樓），並於2005年落成。新翼在2006年1月開始遷入，並於2006年3月12日在保良局當年主席主持下正式啟用。
2008年夏天，開始興建綜藝舞台，並於同年落成，於2009年5月在保良局當年主席何志豪先生主持下正式啟用。
原本位於舊翼三樓的教員室改建為教室，而新翼設一個教員室、多用途室，大型電腦室和一個多媒體學習中心。

## 著名校友

  - [劉國勳](../Page/劉國勳.md "wikilink")：[立法會議員（區議會（一））](../Page/第六屆香港立法會.md "wikilink")、[北區區議會議員](../Page/北區區議會.md "wikilink")
  - 李明章：2005世界青少年野外定向錦標賽香港代表隊成員
  - 黎皓茌：2005世界青少年野外定向錦標賽香港代表隊成員
  - [張健峰](../Page/張健峰.md "wikilink")：香港足球代表隊成員
  - [簡采恩](../Page/簡采恩.md "wikilink")（Joyee）：有線娛樂新聞主播

## 参考资料

  - [保良局馬錦明中學](http://www.plkmkmc.edu.hk/)
  - [保良局馬錦明中學野外定向學會](https://web.archive.org/web/20080615022026/http://www.mkmoc.com/)
  - [香港中學概覽 2007-2008](http://ssp.proj.hkedcity.net/chi/detail_basic.php?sch_id=1302/)
  - [2017年十八區STEM學校巡禮](https://media.openschool.hk/images/temp/stem02_bookB.pdf)

[Category:粉嶺](../Category/粉嶺.md "wikilink")
[Category:香港北區中學](../Category/香港北區中學.md "wikilink")
[Category:1995年創建的教育機構](../Category/1995年創建的教育機構.md "wikilink")