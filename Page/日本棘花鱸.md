**日本棘花鱸**（[学名](../Page/学名.md "wikilink")：），又名**日本棘花鮨**、**花鱸**，为[鮨科](../Page/鮨科.md "wikilink")[棘花鮨屬下的一个种](../Page/棘花鮨屬.md "wikilink")。

## 分布

本魚分布於[印度西](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[印尼](../Page/印尼.md "wikilink")、[阿拉弗拉海](../Page/阿拉弗拉海.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣等海域](../Page/台灣.md "wikilink")。

## 深度

水深100至300公尺。

## 特徵

本魚體長橢圓形，側扁，體呈赤[紅色](../Page/紅色.md "wikilink")，腹部[白色](../Page/白色.md "wikilink")，體側有6至7條橙[黃色或黃色斑點](../Page/黃色.md "wikilink")。頭背部斜直；眶間區略凸。眼稍大，長於或等於吻長。口大；下頜前端不具小犬齒。頭部全部被鱗片，包含上下頷及頰部。側線完整，側線鱗孔數30至35枚，側線上鱗2列。背鰭具硬棘5枚，軟條15至16枚，第4棘最長；臀鰭硬棘3枚，軟條7枚；腹鰭腹位，末端延伸不及肛門開口；胸鰭延長，中央之鰭條長於上下方之鰭條，鰭條15至17枚，部份鰭條有分支；尾鰭截形或呈圓形。尾鰭後緣截平或近於圓形，無延長之鰭條。背鰭硬棘部與軟條部之間有深刻。體長可達15公分。

## 生態

本魚棲息在較深的沙礫質海底，為肉食性之魚類，以[甲殼類](../Page/甲殼類.md "wikilink")、[魚類為主要食物來源](../Page/魚類.md "wikilink")。

## 經濟利用

具經濟性的小型[食用魚](../Page/食用魚.md "wikilink")，適合[紅燒](../Page/紅燒.md "wikilink")。

## 参考文献

  -
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[japonicus](../Category/棘花鮨屬.md "wikilink")