**甲醇**（英語：**Methanol**，或**Methyl
alcohol**；[分子式](../Page/分子式.md "wikilink")：CH<sub>3</sub>OH或MeOH）又稱**羥基甲烷**、**木醇**（wood
alcohol）與**木精**（wood
spirits），是一种[有机化合物](../Page/有机化合物.md "wikilink")，為最簡單的[醇類](../Page/醇.md "wikilink")。甲醇有「木醇」與「木精」之名，源自於曾经其主要的生產方式是自[木醋液](../Page/木醋液.md "wikilink")（為木材[乾餾或](../Page/乾餾.md "wikilink")[裂解的產物之一](../Page/裂解.md "wikilink")）萃取。現代甲醇是直接從[一氧化碳](../Page/一氧化碳.md "wikilink")，[二氧化碳和](../Page/二氧化碳.md "wikilink")[氫的一個](../Page/氫.md "wikilink")[催化作用的工業過程中製備](../Page/催化.md "wikilink")。

甲醇很輕、[揮發度高](../Page/揮發性.md "wikilink")、無[色](../Page/顏色.md "wikilink")、易燃，并有與[乙醇](../Page/乙醇.md "wikilink")（飲用酒）非常相似的氣味。\[1\]但不同於乙醇，甲醇有[劇毒](../Page/毒物.md "wikilink")，不可以飲用。通常用作[溶劑](../Page/溶劑.md "wikilink")、[防冻剂](../Page/防冻剂.md "wikilink")、[燃料或](../Page/燃料.md "wikilink")[变性劑](../Page/变性乙醇.md "wikilink")，亦可用於經過[酯交換反應生產](../Page/酯交換反應.md "wikilink")[生物柴油](../Page/生物柴油.md "wikilink")。

甲醇可以在空氣中完全燃燒，並釋出[二氧化碳及](../Page/二氧化碳.md "wikilink")[水](../Page/水.md "wikilink")：

  -
    2 CH<sub>3</sub>OH + 3 [O<sub>2</sub>](../Page/氧.md "wikilink") → 2
    [CO<sub>2</sub>](../Page/二氧化碳.md "wikilink") + 4
    [H<sub>2</sub>O](../Page/水.md "wikilink")

甲醇的火焰近乎無色，所以點燃甲醇時要格外小心，以免被[燒傷](../Page/灼傷.md "wikilink")。

不少[細菌在進行](../Page/細菌.md "wikilink")[缺氧](../Page/厭氧生物.md "wikilink")[新陳代謝時會產生甲醇](../Page/新陳代謝.md "wikilink")。因此，空氣中存有少量的甲醇蒸氣，但幾日內就會在陽光照射之下被空氣中的氧氣[氧化](../Page/氧化.md "wikilink")，成為二氧化碳。

## 历史

[古埃及人在](../Page/古埃及人.md "wikilink")[遗体保存技术防腐的過程中](../Page/遗体保存技术.md "wikilink")，使用了包括甲醇的混合物質以保存屍體。然而，直到1661年，[罗伯特·波义耳才首次分离出纯的甲醇](../Page/罗伯特·波义耳.md "wikilink")，方法是蒸馏[黄杨](../Page/黄杨属.md "wikilink")（黄杨木）。\[2\]
它后来被称为“pyroxylic spirit”。
1834年，法国化学家[讓-巴蒂斯特·杜馬和](../Page/讓-巴蒂斯特·杜馬.md "wikilink")确定了它的元素组成。\[3\]

在2006年，天文學家利用[射電望遠鏡在](../Page/射电望远镜.md "wikilink")[焦德雷爾班克天文台的](../Page/卓瑞爾河岸天文台.md "wikilink")陣列發現了太空中一個2880億英里寬的巨大甲醇雲。\[4\]\[5\]

## 生產

合成甲醇可以用固體（如[煤](../Page/煤.md "wikilink")、[焦炭](../Page/焦炭.md "wikilink")），液體（如[原油](../Page/原油.md "wikilink")、[重油](../Page/重油.md "wikilink")、[輕油](../Page/輕油.md "wikilink")）或氣體（如[天然氣及其他可燃性氣體](../Page/天然氣.md "wikilink")）做為原料，經造氣凈化（脫硫）變換，除去二氧化碳，配制成一定的合成氣（一氧化碳和氫）。在不同的[催化劑存在下](../Page/催化劑.md "wikilink")，選用不同的工藝條件。單產甲醇（分高壓法低壓和中壓法），或與合成[氨聯產甲醇](../Page/氨.md "wikilink")（聯醇法）。將合成後的粗甲醇，經預精餾脫除[甲醚](../Page/甲醚.md "wikilink")，精餾而得成品甲醇。甲醇視為1級醇。

## 用途

甲醇是一种常见的溶剂。由于它的低的紫外線截止，使得甲醇很有用于[高效液相色谱法](../Page/高效液相色谱法.md "wikilink"),
[紫外－可见分光光度法及](../Page/紫外－可见分光光度法.md "wikilink")[液相色譜法-質譜聯用](../Page/液相色譜法-質譜聯用.md "wikilink")。

### 原料

甲醇最大的用途是制造其他化学品。大约40%的甲醇会被转化为[甲醛](../Page/甲醛.md "wikilink")，再用于生产[塑料](../Page/塑料.md "wikilink")，[胶合板](../Page/胶合板.md "wikilink")，[涂料](../Page/涂料.md "wikilink")，[炸药](../Page/炸药.md "wikilink"),
和免烫[纺织品](../Page/纺织品.md "wikilink")。

甲醇亦有時會用於更高的分子建構，例如：利用改性[高嶺土作催化劑](../Page/高嶺土.md "wikilink")，可以把甲醇脫水生成[二甲醚](../Page/二甲醚.md "wikilink")。科學家基於高嶺土在類似的環境所扮演的催化角色，以及地球創始之時的豐富[甲烷環境](../Page/甲烷.md "wikilink")，推論出地球生命的生成可能從甲醇開始。

### 燃料

[Fuel_cell_NASA_p48600ac.jpg](https://zh.wikipedia.org/wiki/File:Fuel_cell_NASA_p48600ac.jpg "fig:Fuel_cell_NASA_p48600ac.jpg")
甲醇可以在有限基础上用作为[内燃机的燃料](../Page/内燃机.md "wikilink")。按照规则，纯甲醇被要求使用于
[印第賽車](../Page/印第賽車.md "wikilink")，[怪兽卡车](../Page/怪兽卡车.md "wikilink")，（USAC）的冲刺汽车（以及侏儒，modifieds，等等），以及其他泥路賽車系列，如，和[沙地摩托车比賽](../Page/沙地摩托车.md "wikilink")。

### 其他应用

甲醇是一种传统的乙醇变性剂，混有甲醇之乙醇被称为“[變性乙醇](../Page/變性乙醇.md "wikilink")”或“甲基化酒精”（Methylated
spirit）。在[美国](../Page/美国.md "wikilink")1920年至1933年的[禁酒時期](../Page/禁酒時期.md "wikilink")，变性乙醇會被用來替代传统的乙醇，部分是因为甲醇比乙醇便宜。但其[致盲的毒性令甲醇酒精逐漸被取締及管制](../Page/失明.md "wikilink")。

甲醇也作为[溶剂使用](../Page/溶剂.md "wikilink")，并作为在[管道的](../Page/管道.md "wikilink")[防冻液和](../Page/防凍劑.md "wikilink")[挡风玻璃清洗液](../Page/风挡液.md "wikilink")。

在近年甲醇也成為[燃料電池的燃料之一](../Page/燃料電池.md "wikilink")，如
[直接甲醇燃料電池](../Page/直接甲醇燃料電池.md "wikilink")。

## 健康及安全

### 毒性

甲醇對人體有毒，因為甲醇在人體[新陳代謝中會氧化成比甲醇毒性更強的](../Page/代谢.md "wikilink")[甲醛和](../Page/甲醛.md "wikilink")[甲酸](../Page/甲酸.md "wikilink")（[蟻酸](../Page/甲酸.md "wikilink")），因此飲用含有甲醇的酒可引致[失明](../Page/失明.md "wikilink")、[肝病](../Page/肝.md "wikilink")、甚至死亡。誤飲4[毫升以上就會出現中毒症狀](../Page/毫升.md "wikilink")，超過10毫升即可因對[視神經的永久破壞而導致失明](../Page/視神經.md "wikilink")，30毫升已能導致死亡。\[6\]

初期中毒症狀包括[心跳加速](../Page/心跳.md "wikilink")、腹痛、嘔吐、下瀉、無胃口、[頭痛](../Page/頭痛.md "wikilink")、暈、全身無力。嚴重者會神智不清、[呼吸急速至衰竭](../Page/呼吸.md "wikilink")。失明是它最典型的症狀，甲醇進入[血液后](../Page/血液.md "wikilink")，會使組織[酸性變強產生](../Page/酸性.md "wikilink")[酸中毒](../Page/酸中毒.md "wikilink")，導致[腎衰竭](../Page/腎衰竭.md "wikilink")。最嚴重者是[死亡](../Page/死亡.md "wikilink")。

然而，仍然有不少不法商人不顧人命安全，用含有甲醇的工业[酒精勾兑假酒并出售](../Page/酒精.md "wikilink")。2011年12月13日，[印度](../Page/印度.md "wikilink")[西孟加拉邦的一座村莊多人因飲用含有甲醇的](../Page/西孟加拉邦.md "wikilink")[酒而出現中毒症狀](../Page/酒.md "wikilink")，造成至少171人死亡\[7\]。

但是，部分真酒中含有极微量的甲醇，是[宿醉的原因之一](../Page/宿醉.md "wikilink")。\[8\]

### 解毒

甲醇中毒可以用[乙醇](../Page/乙醇.md "wikilink")（真酒）與解毒。\[9\]\[10\]\[11\]因为甲醇在肝脏中被酒精脱氢酶氧化成[福尔马林](../Page/福尔马林.md "wikilink")（[甲醛水溶液](../Page/甲醛.md "wikilink")），然后是[甲酸](../Page/甲酸.md "wikilink")。乙醇可以和甲醇競爭[醇脱氢酶](../Page/醇脱氢酶.md "wikilink")，使人体有时间排除甲醇。\[12\]\[13\]\[14\]

## 标准

  - 车间空气卫生标准: 中国 MAC 50mg/m<sup>3</sup>；美国 OSHA PEL-TWA
    260mg/m<sup>3</sup>
  - 危规: GB 3.2 类 32058。
  - 原铁规: 一级易燃液体, 61069。UN NO.1230。
  - IMDG CODE 3087页，3类。副危险 6.1 类。

## 參看

  - [氘代甲醇](../Page/氘代甲醇.md "wikilink")
  - [變性乙醇](../Page/變性乙醇.md "wikilink")（[工業酒精](../Page/變性乙醇.md "wikilink")）

## 參考書目

  -
## 參考文獻

[Category:醇](../Category/醇.md "wikilink")
[Category:生物燃料](../Category/生物燃料.md "wikilink")
[Category:空气污染物](../Category/空气污染物.md "wikilink")
[Category:醇类溶剂](../Category/醇类溶剂.md "wikilink")
[Category:解剖保藏](../Category/解剖保藏.md "wikilink")
[Category:氧化](../Category/氧化.md "wikilink")
[Category:一碳有机物](../Category/一碳有机物.md "wikilink")

1.
2.  Boyle discusses the distillation of liquids from the wood of the box
    shrub in: Robert Boyle, *The Sceptical Chymist* (London, England: J.
    Cadwell, 1661),
    [pp. 192-195](https://archive.org/stream/scepticalchymis00BoylA#page/192/mode/2up/).
3.  A report on methanol to the French Academy of Sciences by J. Dumas
    and E. Péligot began during the Academy's meeting of October 27,
    1834 and finished during the meeting of November 3, 1834. See:
    *Procès-verbaux des séances de l'Académie*, **10** : 600-601.
    Available on:
    [Gallica](http://gallica.bnf.fr/ark:/12148/bpt6k33037/f606.image).
    The complete report appears in: J. Dumas and E. Péligot (1835)
    ["Mémoire sur l'espirit de bois et sur les divers composés ethérés
    qui en
    proviennent"](http://books.google.com/books?id=94c5AAAAcAAJ&pg=PA5#v=onepage&q&f=false)
    (Memoir on spirit of wood and on the various ethereal compounds that
    derive therefrom), *Annales de chimie et de physique*, **58** :
    5-74; from
    [page 9](http://books.google.com/books?id=94c5AAAAcAAJ&pg=PA9#v=onepage&q&f=false):
    ''Nous donnerons le nom de *méthylène* (1) à un radical … (1) Μεθυ,
    vin, et υλη, bois; c'est-à-dire vin ou liqueur spiritueuse du
    bois.'' (We will give the name "methylene" (1) to a radical … (1)
    *methy*, wine, and *hulē*, wood; that is, wine or spirit of wood.)
4.
5.
6.
7.  [印度假酒案死亡人數升至171人](http://news.sina.com.hk/news/12/1/1/2523311/1.html)
8.
9.
10.
11.
12.
13.
14.