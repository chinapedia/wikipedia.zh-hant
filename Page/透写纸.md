[Samples_of_TAKEYO_tracing_paper.jpg](https://zh.wikipedia.org/wiki/File:Samples_of_TAKEYO_tracing_paper.jpg "fig:Samples_of_TAKEYO_tracing_paper.jpg")
[GatewayTracingNewRoll.jpg](https://zh.wikipedia.org/wiki/File:GatewayTracingNewRoll.jpg "fig:GatewayTracingNewRoll.jpg")
**透写纸**（Tracing
paper）又稱**描圖紙**、**硫酸纸**，是一种半透明[纸](../Page/纸.md "wikilink")。它是将未裁剪和装订的纸张浸入优质[硫酸几秒时间制成的](../Page/硫酸.md "wikilink")。这种[酸将部分](../Page/酸.md "wikilink")[纤维素转成](../Page/纤维素.md "wikilink")[淀粉质而具有](../Page/淀粉质.md "wikilink")[凝胶状和不可渗透的特性](../Page/凝胶状.md "wikilink")。当这种处理过的纸张被彻底清洗和干燥过后，得到的产品比原来的纸张要[抗张耐用得多](../Page/抗张强度.md "wikilink")。透写纸耐[油脂并且很大程度上](../Page/油脂.md "wikilink")[水](../Page/水.md "wikilink")、[气密](../Page/气.md "wikilink")。

透写纸名字源于[艺术家能够](../Page/艺术家.md "wikilink")[映描](../Page/映描.md "wikilink")[图像于其上](../Page/图像.md "wikilink")。当透写纸被置放于一副[图画之上时](../Page/图画.md "wikilink")，很容易可以透过透写纸看到图画。因此，艺术家能够很方便的发现图画的边际线并[映描到透写纸上](../Page/映描.md "wikilink")。

此外，还有一種[縫紉用透写纸](../Page/縫紉.md "wikilink")，上面為有顏色的[蠟質](../Page/蠟.md "wikilink")，放於[紙樣和](../Page/紙樣.md "wikilink")[布料之間](../Page/布料.md "wikilink")，用[齒輪透写纸樣上的指示線條](../Page/齒輪.md "wikilink")，蠟質線用热水清洗即可去掉。

## 畫廊

<File:TRANSLUCENT> SHEET.jpg|透明的透写纸
[File:Gateway-Tracing-Paper.jpg|Gateway透写纸53A4和83A3](File:Gateway-Tracing-Paper.jpg%7CGateway透写纸53A4和83A3)
[File:YupoTracingPaper-13022009710.png|Yupo的合成透写纸](File:YupoTracingPaper-13022009710.png%7CYupo的合成透写纸)

## 参见

  - [透寫台](../Page/透寫台.md "wikilink")
  - [喷墨打印纸](../Page/喷墨打印纸.md "wikilink")

## 外部連結

  - [Tracing paper in
    Vietnam](https://web.archive.org/web/20070312103356/http://www.tmg.com.vn/product.asp?CatID=2)
  - [Gateway tracing paper in
    China](https://web.archive.org/web/20060112164022/http://www.gatewaypaper.com/en.htm)
  - [Schoellershammer glamma basic transparency
    paper](https://web.archive.org/web/20070928163529/http://www.schoellershammer.de/cms/front_content.php?changelang=2)
  - [Canson tracing
    paper](https://web.archive.org/web/20070225131750/http://www.canson.com/EN/)

[Category:紙](../Category/紙.md "wikilink")
[Category:艺术材料](../Category/艺术材料.md "wikilink")
[Category:Sewing equipment](../Category/Sewing_equipment.md "wikilink")
[Category:美術材料](../Category/美術材料.md "wikilink")