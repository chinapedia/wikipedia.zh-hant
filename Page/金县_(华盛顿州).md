**金郡**（）是[美国](../Page/美国.md "wikilink")[华盛顿州的一个郡](../Page/华盛顿州.md "wikilink")，郡治[西雅图](../Page/西雅图.md "wikilink")。根据[美国商务部](../Page/美国商务部.md "wikilink")2017年人口普查估算，郡人口為2,188,649人\[1\]，其中[高加索人占](../Page/高加索人种.md "wikilink")61.1%、[亚裔占](../Page/亚裔美国人.md "wikilink")17.4%、[拉丁裔占](../Page/拉丁裔.md "wikilink")9.5%、[非裔占](../Page/非裔美国人.md "wikilink")6.8%。按人口算，金郡是华盛顿州最大的郡，也是美国第13大郡。

金郡郡治[西雅圖是該郡最大都市](../Page/西雅圖.md "wikilink")**，**大約三分之二的人口居住在西雅圖的郊區。

金郡為[西雅圖](../Page/西雅圖.md "wikilink") -
[塔科馬](../Page/塔科马_\(华盛顿州\).md "wikilink") -
[貝爾維尤](../Page/貝爾維尤_\(華盛頓州\).md "wikilink")[都市區](../Page/都市圈.md "wikilink")，三個華盛頓州郡之中面積最大**，**（另外兩個分別為北部的[斯諾霍米甚縣和南部的](../Page/史諾霍米須郡.md "wikilink")[皮爾斯縣](../Page/皮爾斯縣_\(華盛頓州\).md "wikilink")。）

## 地理

[Kingcounty-wa.png](https://zh.wikipedia.org/wiki/File:Kingcounty-wa.png "fig:Kingcounty-wa.png")
根據[美国人口调查局](../Page/美国人口调查局.md "wikilink")，本郡的總面積為，其中陸地面積占、水域面積占 （8.3%）。

### 地理特色

#### 陸域

  - [喀斯喀特山脉](../Page/喀斯喀特山脉.md "wikilink")

  -
  - ，最高點

  -
  -
  -
  - [瑟马米什高原](../Page/瑟马米什_\(华盛顿州\).md "wikilink")

  - [默瑟島](../Page/默瑟島_\(華盛頓州\).md "wikilink")

  - [瓦雄島](../Page/瓦雄_\(華盛頓州\).md "wikilink")

#### 水域

  -
  -
  -
  -
  -
  -
  -
  - [华盛顿湖](../Page/华盛顿湖.md "wikilink")

  - [普拉特河](../Page/普拉特河.md "wikilink")

  - [普吉特海湾](../Page/普吉特海湾.md "wikilink")

  -
  -
  -
  -
### 主要公路

  - [I-5_(big).svg](https://zh.wikipedia.org/wiki/File:I-5_\(big\).svg "fig:I-5_(big).svg")
    [5號州際公路](../Page/5號州際公路.md "wikilink")
  - [I-90_(big).svg](https://zh.wikipedia.org/wiki/File:I-90_\(big\).svg "fig:I-90_(big).svg")
    [90號州際公路](../Page/90號州際公路.md "wikilink")
  - [I-405_(big).svg](https://zh.wikipedia.org/wiki/File:I-405_\(big\).svg "fig:I-405_(big).svg")
    [405號州際公路](../Page/405號州際公路_\(華盛頓州\).md "wikilink")
  - [US_2.svg](https://zh.wikipedia.org/wiki/File:US_2.svg "fig:US_2.svg")
    [美國國道2](../Page/美國國道2.md "wikilink")
  - [WA-18.svg](https://zh.wikipedia.org/wiki/File:WA-18.svg "fig:WA-18.svg")
    [18華盛頓州州道](../Page/18華盛頓州州道.md "wikilink")
  - [WA-99.svg](https://zh.wikipedia.org/wiki/File:WA-99.svg "fig:WA-99.svg")
    [99華盛頓州州道](../Page/99華盛頓州州道.md "wikilink")
  - [WA-520.svg](https://zh.wikipedia.org/wiki/File:WA-520.svg "fig:WA-520.svg")
    [520華盛頓州州道](../Page/520華盛頓州州道.md "wikilink")
  - [WA-167.svg](https://zh.wikipedia.org/wiki/File:WA-167.svg "fig:WA-167.svg")
    [167華盛頓州州道](../Page/167華盛頓州州道.md "wikilink")

### 毗鄰郡

  - 北－[斯诺霍米什县](../Page/斯诺霍米什县_\(华盛顿州\).md "wikilink")
  - 東北／東－[奇兰县](../Page/奇兰县_\(华盛顿州\).md "wikilink")
  - 東／東南－[基帝塔什縣](../Page/基帝塔什縣.md "wikilink")
  - 南－[皮尔斯县](../Page/皮尔斯县_\(华盛顿州\).md "wikilink")
  - 西－[基沙普縣](../Page/基沙普縣.md "wikilink")

### 國家保護區

  -
  - （部分，一部分在[史凯威](../Page/史凯威_\(阿拉斯加州\).md "wikilink")）

  - （部分）

## 資料來源

## 外部链接

  - [金县官方網站](https://kingcounty.gov/)

  - [King County Property Parcel
    Viewer](https:/kingcounty.gov/operations/gis/propresearch/parcelviewer.aspx)

  - [金县快照](https://content.lib.washington.edu/imls/kcsnapshots/index.html)

[King County](../Category/华盛顿州行政区划.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")

1.