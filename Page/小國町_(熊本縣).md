**小国町**（）是位于[日本](../Page/日本.md "wikilink")[熊本縣東北部](../Page/熊本縣.md "wikilink")，[阿蘇外輪山外側的一個](../Page/阿蘇外輪山.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[阿蘇郡](../Page/阿蘇郡.md "wikilink")。；轄區內有74%地區為林地
。\[1\]

為了與[山形縣](../Page/山形縣.md "wikilink")[西置賜郡](../Page/西置賜郡.md "wikilink")[小國町](../Page/小國町_\(山形縣\).md "wikilink")、[新潟縣](../Page/新潟縣.md "wikilink")[刈羽郡](../Page/刈羽郡.md "wikilink")[小國町](../Page/小國町_\(新潟縣\).md "wikilink")（已於2005年併入[長岡市](../Page/長岡市.md "wikilink")）區別，因此會以「肥後小國」來表示此町。

## 歷史

過去屬於「小國鄉」，小國鄉在1870年將原本的25個村合併為9個村，並在1889年進一步將其中六村合併為北小國村，也就是現在的小國町；另外三村合併為南小國村（現在的[南小國町](../Page/南小國町.md "wikilink")）。北小國村在改制為町時，同時改名為現在的「小國町」之名稱。\[2\]

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，設置**北小國村**。
  - 1935年4月1日：改制並改名為**小國町**。

## 交通

[MonumentOfHigoOguniStation-20070722.jpg](https://zh.wikipedia.org/wiki/File:MonumentOfHigoOguniStation-20070722.jpg "fig:MonumentOfHigoOguniStation-20070722.jpg")

### 鐵路

過去曾有[國鐵](../Page/國鐵.md "wikilink")[宮原線通過](../Page/宮原線.md "wikilink")，但已於1984年12月1日停駛。

  - 國鐵
      - 宮原線：[北里車站](../Page/北里車站.md "wikilink") -
        [肥後小國車站](../Page/肥後小國車站.md "wikilink")

## 觀光資源

### 景點

  - 宮原兩神社
  - 北里柴三郎記念館
  - 鍋瀑布

### 溫泉

  - [杖立溫泉](../Page/杖立溫泉.md "wikilink")
  - 奴留湯溫泉
  - 山川溫泉
  - 岳之湯溫泉
  - 峐之湯溫泉

## 教育

### 高等學校

  - [熊本縣立小國高等學校](../Page/熊本縣立小國高等學校.md "wikilink")

### 特殊學校

  - [熊本縣立小國養護學校](../Page/熊本縣立小國養護學校.md "wikilink")

## 本地出身之名人

  - [北里柴三郎](../Page/北里柴三郎.md "wikilink")：細菌學家
  - [勝野洋](../Page/勝野洋.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [大塚和征](../Page/大塚和征.md "wikilink")：[職業足球選手](../Page/職業足球.md "wikilink")

## 參考資料

## 外部連結

  - [小國町旅遊協會](http://www.tourism-oguni.com/)

<!-- end list -->

1.

2.