<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>中央政治大學法政系畢業<br />
<span style="color: blue;">（1946年）</span></li>
<li><a href="../Page/國立政治大學.md" title="wikilink">國立政治大學高等科畢業</a><br />
<span style="color: blue;">（1948年）</span></li>
<li>西德<a href="../Page/波昂大學.md" title="wikilink">波昂大學法學博士</a><br />
<span style="color: blue;">（1963年）</span></li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>國防部法制司司長<br />
<span style="color: blue;">（1969年－1972年）</span></li>
<li>國立政治大學法律研究所所長<br />
<span style="color: blue;">（1970年－1973年）</span></li>
<li>行政院法規委員會主任委員<br />
<span style="color: blue;">（1972年－1973年）</span></li>
<li>國立政治大學校長<br />
<span style="color: blue;">（1973年－1977年4月19日）</span></li>
<li>教育部部長<br />
<span style="color: blue;">（1977年4月19日－1978年5月29日）</span></li>
<li>行政院政務委員<br />
<span style="color: blue;">（1977年4月19日－1984年5月28日）</span></li>
<li>中國青年<a href="../Page/反共.md" title="wikilink">反共救國團</a>（第三任）主任<br />
<span style="color: blue;">（1978年1月1日－1978年6月30日）</span></li>
<li>司法行政部部長<br />
<span style="color: blue;">（1978年5月30日－1980年6月30日）</span></li>
<li>法務部部長<br />
<span style="color: blue;">（1980年6月30日－1984年5月28日）</span></li>
<li>總統府國策顧問<br />
<span style="color: blue;">（1984年5月28日－1988年10月17日）</span></li>
<li>總統府秘書長<br />
<span style="color: blue;">（1988年10月17日－1990年5月20日）</span></li>
<li><a href="../Page/中華民國副總統.md" title="wikilink">中華民國副總統</a>（第8任）<br />
<span style="color: blue;">（1990年5月20日－1996年5月20日）</span></li>
<li>中國國民黨副主席<br />
<span style="color: blue;">（1993年8月18日－2000年6月）</span></li>
<li>總統府資政<br />
<span style="color: blue;">(1996年5月20日－2008年5月20日)</span></li>
<li>國立故宮博物院指導委員會主任委員<br />
<span style="color: blue;">(1997年8月30日－2000年)</span></li>
</ul>
<p><strong>考試</strong></p>
<ul>
<li>高等考試司法官考試及格</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

**李元簇**（），字玉鰲，號肇東，[中華民國第](../Page/中華民國.md "wikilink")8任[副總統](../Page/副總統.md "wikilink")。出生於[中華民國](../Page/中華民國.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[平江縣梅仙東高村](../Page/平江縣.md "wikilink")，[中國國民黨籍](../Page/中國國民黨.md "wikilink")[法律學家及](../Page/法律學.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")。曾任[法官](../Page/法官.md "wikilink")、[國立政治大學校長](../Page/國立政治大學.md "wikilink")、[教育部長](../Page/中華民國教育部.md "wikilink")、[反共救國團主任](../Page/反共救國團.md "wikilink")、[法務部長](../Page/中華民國法務部.md "wikilink")、[總統府秘書長以及](../Page/中華民國總統府秘書長.md "wikilink")[中國國民黨副主席等職](../Page/中國國民黨副主席.md "wikilink")，並於1990年至1996年間出任[李登輝](../Page/李登輝.md "wikilink")[總統的副手](../Page/總統.md "wikilink")。

## 生平

1946年，自[中央政治學校法政系畢業](../Page/中央政治學校.md "wikilink")，並考取高等[司法官資格](../Page/司法官.md "wikilink")，前往[蘭州](../Page/蘭州.md "wikilink")、[迪化等地擔任當地](../Page/乌鲁木齐市.md "wikilink")[地方法院之](../Page/地方法院.md "wikilink")[推事](../Page/推官.md "wikilink")。後來，升任為當地[高等法院推事](../Page/高等法院.md "wikilink")，兼任《[掃蕩報](../Page/掃蕩報.md "wikilink")》與《青島日報》駐蘭州[記者](../Page/記者.md "wikilink")。

1949年，隨[中華民國政府前往](../Page/中華民國政府.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，歷任[新竹地方法院推事](../Page/新竹地方法院.md "wikilink")、[臺灣高等法院推事](../Page/臺灣高等法院.md "wikilink")、[臺灣省保安司令部及臺北衛戍司令部軍法處處長](../Page/臺灣省保安司令部.md "wikilink")。

1958年，前往[波昂大學進修](../Page/波昂大學.md "wikilink")，在1963年獲得[法學](../Page/法學.md "wikilink")[博士學位](../Page/博士.md "wikilink")。

1963年返回臺灣後，出任《[中央日報](../Page/中央日報.md "wikilink")》[社論主筆](../Page/社論.md "wikilink")。1969年，被聘任為[國防部法制司司長](../Page/中華民國國防部.md "wikilink")，開啟其[政壇生涯](../Page/政壇.md "wikilink")。後來，歷任[國防部顧問兼法規司司長](../Page/中華民國國防部.md "wikilink")、[行政院法規會及訴願委員會主任委員](../Page/行政院.md "wikilink")，並擔任[國立政治大學專任](../Page/國立政治大學.md "wikilink")[教授兼](../Page/教授.md "wikilink")[法律研究所所長](../Page/法律.md "wikilink")，以及校長，[教育部部長](../Page/中華民國教育部.md "wikilink")、[法務部部長等](../Page/中華民國法務部.md "wikilink")。

1990年[二月政爭中](../Page/二月政爭.md "wikilink")，李元簇於臨中全會前一天得到國民黨副秘書長[鄭心雄通知](../Page/鄭心雄.md "wikilink")「[非主流派](../Page/非主流派_\(中國國民黨\).md "wikilink")」[李煥與](../Page/李煥.md "wikilink")[郝柏村結盟的消息](../Page/郝柏村.md "wikilink")，李元簇以出身情治系統的敏感度緊急向[李登輝通報](../Page/李登輝.md "wikilink")，加上後來來自警政與心腹[蘇志誠的情資](../Page/蘇志誠.md "wikilink")，李登輝才一夕間心生警覺並反守為攻，使李登輝陣營成功反制「非主流派」。\[1\]

1990年3月21日，[經國民大會選舉成為中華民國第](../Page/1990年中華民國總統選舉.md "wikilink")8任[副總統](../Page/副總統.md "wikilink")，於同年5月20日與總統當選人就職。是迄今最後一位出生於中國大陸、以及經由國民大會選舉產生的副總統。任內行事低調，並被外界形容為「沒有聲音的副總統」。\[2\]。1996年5月20日，卸任後宣告[退休](../Page/退休.md "wikilink")。

退休後，長年隱居於[苗-{栗}-縣](../Page/苗栗縣.md "wikilink")[頭份市鄉間](../Page/頭份市.md "wikilink")\[3\]。1998年1月22日，夫人[徐曼雲女士先一步離世](../Page/徐曼雲.md "wikilink")，李元簇獨居當地。

2017年3月8日4時15分，因慢性[腎臟病放棄](../Page/腎臟病.md "wikilink")[急救過世](../Page/急救.md "wikilink")，享壽94歲。\[4\]\[5\]\[6\]\[7\]

2017年3月31日，在[臺北市立第一殯儀館景行廳舉行](../Page/臺北市立第一殯儀館.md "wikilink")[告別式](../Page/告別式.md "wikilink")，\[8\]獲總統[蔡英文親頒](../Page/蔡英文.md "wikilink")[褒揚令](../Page/褒揚令.md "wikilink")，並舉行覆蓋[中華民國國旗儀式](../Page/中華民國國旗.md "wikilink")，由副總統[陳建仁擔任主祭官](../Page/陳建仁.md "wikilink")，內政部部長[葉俊榮](../Page/葉俊榮.md "wikilink")、外交部部長[李大維](../Page/李大維.md "wikilink")、國防部部長[馮世寬和監察委員](../Page/馮世寬.md "wikilink")[劉德勳擔任覆旗官](../Page/劉德勳.md "wikilink")，隨後放追思影片，再由李登輝發表追思談話。\[9\][臺北市立第二殯儀館火化後](../Page/臺北市立第二殯儀館.md "wikilink")，在[新北市](../Page/新北市.md "wikilink")[金山區以佛教儀式樹葬植存於](../Page/金山區_\(台灣\).md "wikilink")[法鼓山金山環保生命園區](../Page/法鼓山.md "wikilink")。\[10\]
褒揚令原文如下：

> 　前副總統李元簇，沖默澹泊，清勤弘器。少歲失怙偃蹇，淬勉耽學，卒業中央政治學校；嗣負笈歐陸，獲德國波昂大學法學博士學位，沉潛濬瀹，宣通載籍。崢嶸歲月，四郊多壘，持秉書生報國襟懷，遠赴邊關北地服務，當艱彌奮，執志不遷。復通過司法官高等考試，自願來臺，開啟理冤摘伏職涯，秦庭朗鏡，法外仁昭。嗣迭膺重寄，支策據梧，於接掌國立政治大學暨教育部期間，著重人文社會科學，提升學術研究水準；進行校務學門評鑑，厚實技職體系發展；研擬教育政策方針，強化國際交流合作，樹教陶鎔，斯文振起；運帷謨慮，胸臆自出。旋履任司法行政部暨法務部部長，貫徹實施審檢分隸，初創國家賠償新法；確立緊急拘提機制，打擊貪瀆經濟犯罪；擴廣地方調解範疇，增益教化觀護成效，廉平裁規，民譽芳騰。尤以當選第八任副總統，銜命推動憲政改革，殫心終止萬年國會，克成總統暨中央民意代表直選，掊斗折衝，洞貫肯綮；股肱扶翼，襄贊廟廊，允為臺灣民主憲政之重要推手，爰獲頒一等卿雲勳章殊榮。綜其生平，朝野欽挹風猷，中外感慕聲采，遺緒遐祚，德隆望尊。遽聞殂落，震悼罔極，應予明令褒揚，用示政府崇禮元勳之至意。
>
> 總　　　統　[蔡英文](../Page/蔡英文.md "wikilink")　　　　
> 行政院院長　[林　全](../Page/林全.md "wikilink")　　　　

## 家庭

與妻子[徐曼雲結婚](../Page/徐曼雲.md "wikilink")，育有二子；長子為[李立心](../Page/李立心.md "wikilink")、次子為[李立天](../Page/李立天.md "wikilink")。

## 参考文獻

{{-}}   |- |colspan="3"
style="text-align:center;"|**[ROC_Office_of_the_President_Emblem.svg](https://zh.wikipedia.org/wiki/File:ROC_Office_of_the_President_Emblem.svg "fig:ROC_Office_of_the_President_Emblem.svg")[副總統](../Page/中華民國副總統.md "wikilink")**
|-       |- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")[行政院](../Page/行政院.md "wikilink")**
|-        |- |colspan="3" style="text-align:
center;"|**[國立政治大學](../Page/國立政治大學.md "wikilink")** |-

[Category:中華民國總統府資政](../Category/中華民國總統府資政.md "wikilink")
[Category:中華民國副總統](../Category/中華民國副總統.md "wikilink")
[Category:中華民國總統府秘書長](../Category/中華民國總統府秘書長.md "wikilink")
[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:中華民國教育部部長](../Category/中華民國教育部部長.md "wikilink")
[Category:中華民國法務部部長](../Category/中華民國法務部部長.md "wikilink")
[Category:國立政治大學校長](../Category/國立政治大學校長.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:波恩大學校友](../Category/波恩大學校友.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:台灣戰後湖南移民](../Category/台灣戰後湖南移民.md "wikilink")
[L李](../Category/平江县人.md "wikilink") [Y](../Category/李姓.md "wikilink")
[指導會首長](../Category/國立故宮博物院人物.md "wikilink")

1.

2.

3.

4.

5.
6.

7.

8.

9.

10.