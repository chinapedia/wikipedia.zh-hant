**一磷酸腺苷**（英文：**Adenosine
monophosphate**，簡稱**AMP**），又名**5'-腺嘌呤核苷酸**或**腺苷酸**，是一種在[核糖核酸](../Page/核糖核酸.md "wikilink")（RNA）中發現的[核苷酸](../Page/核苷酸.md "wikilink")。它是一種[磷酸及](../Page/磷酸.md "wikilink")[核苷](../Page/核苷.md "wikilink")[腺苷的](../Page/腺苷.md "wikilink")[酯](../Page/酯.md "wikilink")，並由[磷酸鹽](../Page/磷酸鹽.md "wikilink")[官能團](../Page/官能團.md "wikilink")、[戊糖](../Page/戊糖.md "wikilink")[核酸糖及](../Page/核酸糖.md "wikilink")[鹼基](../Page/鹼基.md "wikilink")[腺嘌呤所組成](../Page/腺嘌呤.md "wikilink")。\[1\]

## 生產及降解

一磷酸腺苷可以從[腺苷酸激酶催化兩個](../Page/腺苷酸激酶.md "wikilink")[二磷酸腺苷](../Page/二磷酸腺苷.md "wikilink")（ADP）分子合成[三磷酸腺苷](../Page/三磷酸腺苷.md "wikilink")（ATP）時生成：

\[2\mbox{ADP} \rightarrow \mbox{ATP} + \mbox{AMP}\]

或是經由[水解ADP的](../Page/水解.md "wikilink")[高能磷酸鍵生成](../Page/高能磷酸化合物.md "wikilink")：

\[\mbox{ADP} \rightarrow \mbox{AMP} + \mbox{P}_{\mbox{i}}\]

水解ATP亦可生成AMP及[焦磷酸鹽](../Page/焦磷酸鹽.md "wikilink")：

\[\mbox{ATP} \rightarrow \mbox{AMP} + \mbox{PP}_{\mbox{i}}\]

當[核糖核酸](../Page/核糖核酸.md "wikilink")（RNA）被分解後，一磷酸[核苷](../Page/核苷.md "wikilink")，包括一磷酸腺苷會被生成。

而AMP可以從以下的方式再生成ATP：

\[\mbox{AMP} + \mbox{ATP} \rightarrow 2\mbox{ADP}\]（腺苷酸激酶在另一方向）

\[2\mbox{ADP} + 2\mbox{P}_{\mbox{i}} \rightarrow 2\mbox{ATP}\]（此一步驟是經常被好氧性生物的[三磷酸腺苷合成酶在](../Page/三磷酸腺苷合成酶.md "wikilink")[氧化磷酸化採用](../Page/氧化磷酸化.md "wikilink")）

AMP亦可由[肌腺苷酸脫胺酶釋放一組](../Page/肌腺苷酸脫胺酶.md "wikilink")[氨基轉為](../Page/氨.md "wikilink")[一磷酸肌苷](../Page/一磷酸肌苷.md "wikilink")。

在分解代謝的過程中，AMP可以轉化成為[尿酸排出體外](../Page/尿酸.md "wikilink")。

## 環磷酸腺苷

  -

AMP亦可有一種環狀結構稱為[環磷酸腺苷](../Page/環磷酸腺苷.md "wikilink")（或稱為cAMP）。在某些[細胞由](../Page/細胞.md "wikilink")[腺苷環化酶催化ATP成為cAMP](../Page/腺苷環化酶.md "wikilink")，並一般由[腎上腺素或](../Page/腎上腺素.md "wikilink")[胰高血糖素所調節](../Page/胰高血糖素.md "wikilink")。cAMP在細胞內的訊息傳遞起著重要的角色。

## 商務應用

另外，AMP在對[人類測試後](../Page/人類.md "wikilink")，有著壓抑苦味的特性，使得食物仿佛甜些。這種發現可以使得低[卡路里食物更美味](../Page/卡路里.md "wikilink")，在對[肥胖症擔心的時間](../Page/肥胖症.md "wikilink")，成為改善食物質素的較有益方法。這種特性於2004年9月已經得到[美國食品藥物管理局的批准](../Page/美國食品藥物管理局.md "wikilink")，由[美國](../Page/美國.md "wikilink")[紐澤西州的](../Page/紐澤西州.md "wikilink")[靈瓜金以AMP作為食品的苦味遮罩的專利](../Page/靈瓜金.md "wikilink")。\[2\]

## 內部連結

  - [核苷](../Page/核苷.md "wikilink")
  - [核苷酸](../Page/核苷酸.md "wikilink")
  - [脫氧核糖核酸](../Page/脫氧核糖核酸.md "wikilink")
  - [核糖核酸](../Page/核糖核酸.md "wikilink")
  - [寡核苷酸](../Page/寡核苷酸.md "wikilink")

## 參考文獻

<references/>

## 外部連結

  - [Computational Chemistry
    Wiki](https://web.archive.org/web/20070310232615/http://www.compchemwiki.org/index.php?title=Adenosine_monophosphate)

[Category:核苷酸](../Category/核苷酸.md "wikilink")
[Category:有機磷酸鹽](../Category/有機磷酸鹽.md "wikilink")

1.  [Barnes-Jewish Hospital, 2005, Adenosine Monophosphate
    (於2006年9月27日查閱)](http://myhealth.barnesjewish.org/library/healthguide/en-us/Cam/topic.asp?hwid=hn-2796008)
2.   **靈瓜金**官方網頁