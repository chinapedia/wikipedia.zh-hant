**月兔**，也叫**玉兔**，在一些[神話傳說中是居住在](../Page/神話傳說.md "wikilink")[月球上的](../Page/月球.md "wikilink")[兔子](../Page/兔子.md "wikilink")。目前多認為是因[空想性错视而产生的](../Page/空想性错视.md "wikilink")。在许多文化中，特别是在[東亞](../Page/東亞.md "wikilink")（[中國](../Page/中國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[朝鮮](../Page/朝鮮半島.md "wikilink")、[台灣](../Page/台灣.md "wikilink")）的民间传说和[阿茲特克神話中](../Page/阿茲特克神話.md "wikilink")，常塑造成用研杵捣研钵的形象\[1\]\[2\]。在[中国神话中](../Page/中国神话.md "wikilink")，月兔在月宫陪伴[嫦娥并搗](../Page/嫦娥.md "wikilink")[藥](../Page/长生不老药.md "wikilink")，而在日本和韩国，月兔則是在上面搗[麻糬](../Page/麻糬.md "wikilink")，在台灣也有月兔搗麻糬之說。

## 历史

月兔的记载，首见于[屈原的](../Page/屈原.md "wikilink")《天问》：“夜光何德，死而又育？厥利维何，而顾菟在腹。”在西汉初期的[马王堆一号汉墓帛画中月上绘有蟾蜍和玉兔](../Page/马王堆一号汉墓帛画.md "wikilink")。[刘向](../Page/刘向.md "wikilink")《[五经通义](../Page/五经通义.md "wikilink")》：“月中有兔与蟾蜍何？月，阴也；蟾蜍，阳也，而与兔并，明阴系于阳也。”[古诗中往往以兔指代月](../Page/古诗.md "wikilink")，以下是[唐代前的例子](../Page/唐朝.md "wikilink")：

  - 《[古诗十九首](../Page/古诗十九首.md "wikilink")》之十七：三五明月满，四五蟾**兔**缺。
  - [庾信](../Page/庾信.md "wikilink")《宫调曲》：金波来**白兔**，弱木下苍乌。
  - [江总](../Page/江总.md "wikilink")《内殿赋新诗》**兔影**脉脉照金铺，虬水滴滴泻玉壸。
  - 江总《赋得三五明月满诗》：三五**兔辉**成，浮阴冷复轻。
  - 江总《箫史曲》：来时**兔月**满，去后凤楼空。

## 中國以外的版本

《[本生经](../Page/本生_\(佛教\).md "wikilink")》中记载：狐、獭、猴、兔每天到修道者处听道。某年天旱，修道者欲迁，四兽欲挽留各自寻找食。兔子自忖无能为力，于是自投火中，把自己作为食物。修道者显露[帝释天真身](../Page/帝释天.md "wikilink")，将兔绘于月上，作为纪念。在日本的《[今昔物语集](../Page/今昔物语集.md "wikilink")》中也有类似故事，兔子的伙伴变成了狐和猴。朝鮮童謠《[小白船](../Page/小白船.md "wikilink")》（原名《半月》，[諺文](../Page/諺文.md "wikilink")：）也提到月中有兔。

## 现代参考

  - 1969年阿波罗11号登陆月球前，舱内的太空员曾在谈话中提到嫦娥和月兔： \[3\]

> [林顿·约翰逊太空中心](../Page/林顿·约翰逊太空中心.md "wikilink"): Among the large
> headlines concerning Apollo this morning, there's one asking that you
> watch for a lovely girl with a big rabbit. An ancient legend says a
> beautiful Chinese girl called [Chang-o](../Page/嫦娥.md "wikilink") has
> been living there for 4000 years. It seems she was banished to the
> Moon because she stole the pill of immortality from her husband. You
> might also look for her companion, a large Chinese rabbit, who is easy
> to spot since he is always standing on his hind feet in the shade of a
> cinnamon tree. The name of the rabbit is not reported.

> [巴兹·奥尔德林](../Page/巴兹·奥尔德林.md "wikilink"): Okay. We'll keep a close eye
> out for the bunny girl.

  - 在1972年的电影即取材于此。

## 图集

## 参考文献

## 外部链接

  - [與月為伴
    愉閱中秋](https://web.archive.org/web/20130921054506/http://www.tpml.edu.tw/TaipeiPublicLibrary/index.php?page=chinese-hotnews-history_hotnews-9510z.php&subsite=chinese)

## 參見

  - [三足乌](../Page/三足乌.md "wikilink")
  - [中秋節](../Page/中秋節.md "wikilink")
  - [广寒宫](../Page/广寒宫.md "wikilink")
  - [长生不老](../Page/长生不老.md "wikilink")
  - [麻糬](../Page/麻糬.md "wikilink")
  - [月饼](../Page/月饼.md "wikilink")
  - [兔儿神](../Page/兔儿神.md "wikilink")
  - [玉兔号月球车](../Page/玉兔号月球车.md "wikilink")
  - [西遊記](../Page/西遊記.md "wikilink") - 玉兔精

{{-}}

[MR](../Category/月文化.md "wikilink")
[MR](../Category/中國傳說生物.md "wikilink")
[MR](../Category/日本傳說生物.md "wikilink")
[MR](../Category/朝鮮神話.md "wikilink")
[MR](../Category/神話傳說中的兔.md "wikilink")
[MR](../Category/中秋節.md "wikilink")

1.  [The Great
    Hare.](http://community-2.webtv.net/TheObsidianMask/GreatHare/index.html)
2.  [Windling, Terri. *The Symbolism of Rabbits and
    Hares*.](http://www.endicott-studio.com/rdrm/rrRabbits.html)
3.