**哈尔多尔·奥斯格里姆松**（，）。从1994年起，他便成为[冰岛进步党的主席](../Page/冰岛进步党.md "wikilink")，至2006年止。\[1\]
他在2004年9月15日至2006年6月15日擔任[冰岛总理](../Page/冰岛总理.md "wikilink")。

哈尔多尔·奥斯格里姆松曾分别于1974-1978和1979-2003在[冰岛人民大会](../Page/冰岛人民大会.md "wikilink")（Alþingi）上代表过冰岛东区。从2003起他代表着冰岛北区。他还曾担当过冰岛渔业部部长（1983-1991）；法律部部长與宗教部部长（1988-1989）；北欧交往部部长（1985-1987和1995-1999）；外交部部长（1995-2004）。

他于2015年5月18日因突发性心脏病病逝于[雷克雅未克](../Page/雷克雅未克.md "wikilink")。\[2\]

## 参考

## 外部链接

  - [冰岛总理](http://eng.forsaetisraduneyti.is/)

[A](../Category/冰岛总理.md "wikilink")
[A](../Category/哥本哈根大学校友.md "wikilink")
[Category:卑尔根大学校友](../Category/卑尔根大学校友.md "wikilink")
[Category:會計師](../Category/會計師.md "wikilink")

1.  [1](https://books.google.ca/books?id=uoIG6bbP32IC&pg=PA14&dq=Halld%C3%B3r+%C3%81sgr%C3%ADmsson+1947&hl=en&sa=X&ei=kGhbVbGCC-TjsASYtIHgCw&ved=0CBwQ6AEwAA#v=onepage&q=Halld%C3%B3r%20%C3%81sgr%C3%ADmsson%201947&f=false)
2.  [Former Prime Minister of Iceland Passes
    Away](http://icelandreview.com/news/2015/05/19/former-prime-minister-iceland-passes-away)