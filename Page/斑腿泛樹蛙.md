**斑腿泛樹蛙**（[學名](../Page/學名.md "wikilink")：**），又稱為**-{zh-cn:斑腿樹蛙;zh-tw:斑腿泛樹蛙;}-**、**大頭樹蛙**、**大頭泛樹蛙**等。模式標本來自[香港](../Page/香港.md "wikilink")。

## 特徵

中型蛙種，雄蛙約50毫米，體型略大的雌蛙可達70毫米。背腹扁平，呈淺褐色，體色可稍為改變以配合周圍環境，皮膚光滑，有零星小疣粒，背部或有交叉形花紋。大眼睛，鼓膜明顯，後肢有半蹼，趾端吸盤發達。

被捕捉或受驚時會即時排[尿](../Page/尿.md "wikilink")。

## 分佈

[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")，以及在[中國的](../Page/中國.md "wikilink")[廣東](../Page/廣東.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[海南](../Page/海南.md "wikilink")、[江西](../Page/江西.md "wikilink")、[貴州](../Page/貴州.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[雲南](../Page/雲南.md "wikilink")、[福建](../Page/福建.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[江蘇](../Page/江蘇.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[四川](../Page/四川.md "wikilink")、[甘肅及](../Page/甘肅.md "wikilink")[西藏](../Page/西藏.md "wikilink")。其生存的海拔范围为80至1600米。该物种的模式产地在香港。\[1\]

[印度北部亦有斑腿泛樹蛙的分佈](../Page/印度.md "wikilink")。[越南是否分布本種亦仍存疑](../Page/越南.md "wikilink")。斑腿泛樹蛙被引入至[日本和](../Page/日本.md "wikilink")[台灣](../Page/台灣.md "wikilink")，是[日本和](../Page/日本.md "wikilink")[台灣的外來種](../Page/台灣.md "wikilink")。在[台灣的長相與當地原生的](../Page/台灣.md "wikilink")[布氏樹蛙相近](../Page/布氏樹蛙.md "wikilink")，須加以分辨。

## 生活習性

[Brown_Tree_Frog_(Polypedates_megacephalus)_斑腿泛樹蛙_in_Amplexus2.jpg](https://zh.wikipedia.org/wiki/File:Brown_Tree_Frog_\(Polypedates_megacephalus\)_斑腿泛樹蛙_in_Amplexus2.jpg "fig:Brown_Tree_Frog_(Polypedates_megacephalus)_斑腿泛樹蛙_in_Amplexus2.jpg")
多栖息于丘陵地带的稻田或泥窝内、田梗石缝草丛、灌木枝上以及路边。

[蝌蚪為雜食性](../Page/蝌蚪.md "wikilink")，但以[水生植物等為主食](../Page/水生植物.md "wikilink")；成蛙以[昆蟲包括](../Page/昆蟲.md "wikilink")[蟋蟀](../Page/蟋蟀.md "wikilink")、[甲蟲及](../Page/甲蟲.md "wikilink")[蒼蠅為主食](../Page/蒼蠅.md "wikilink")。

## 威脅

斑腿泛樹蛙面臨水污染和生境退化等問題，但根據IUCN對斑腿泛樹蛙的描述，斑腿泛樹蛙的數量穩定，絕種威脅暫時不大。

## 保育現狀

2004年時[世界自然保護聯盟](../Page/世界自然保護聯盟.md "wikilink")（[IUCN](../Page/IUCN.md "wikilink")）對斑腿泛樹蛙的現況評估為[無危](../Page/無危.md "wikilink")。

## 註釋

## 參考文獻

  - 《蛙蛙世界：香港兩棲動物圖鑑》，陳堅峰、張家盛、賀貞意、林峰毅、鄧詠詩、劉惠寧及鮑嘉天，天地圖書，ISBN 988-211-312-5
  - 《香港動物原色圖鑑》

## 外部連結

  - [香港生物多樣性網頁 -
    斑腿泛樹蛙](http://www.afcd.gov.hk/tc_chi/conservation/hkbiodiversity/database/popup_record.asp?id=3102&lang=tc)
  - [香港自然網 - 生態日記](http://www.hknature.net/chi/ecodiary/diary_61.html)
  - [環境資源中心 － 白頷樹蛙](http://e-info.org.tw/node/13519)

[Category:泛树蛙属](../Category/泛树蛙属.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:中國兩棲動物](../Category/中國兩棲動物.md "wikilink")
[Category:印度兩棲動物](../Category/印度兩棲動物.md "wikilink")

1.