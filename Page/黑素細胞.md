**黑素細胞**（melanocyte），又叫**痣细胞**（nevus
cell），是一種動物[細胞](../Page/細胞.md "wikilink")，帶有[黑色素或是其他類似的色素](../Page/黑色素.md "wikilink")。通常位於[皮膚的](../Page/皮膚.md "wikilink")[表皮與眼睛的](../Page/表皮.md "wikilink")[葡萄膜](../Page/葡萄膜.md "wikilink")（[虹膜後面的色素層](../Page/虹膜.md "wikilink")）中。[恆溫動物的黑色素細胞又稱為黑素細胞](../Page/恆溫動物.md "wikilink")（melanocyte），除了黑色的色素以外，還能夠製造一些紅色或黃色的色素。[變溫動物的黑色素細胞則只能製造黑色的色素](../Page/變溫動物.md "wikilink")。

黑素細胞的代謝若是受到破壞或抑制，會產生一些[疾病](../Page/疾病.md "wikilink")，例如遺傳疾病[白化症](../Page/白化症.md "wikilink")，與一種稱為[黑色素細胞瘤](../Page/黑色素細胞瘤.md "wikilink")（melanoma）的癌症。此外皮膚、毛髮和眼睛的顏色，以及黑痣、雀斑等皮膚上的斑點，也都與黑色素細胞有關。

## 細胞的發育

黑素細胞是從[神經脊發展而來](../Page/神經脊.md "wikilink")。這種細胞能夠進行長距離的移動，因此許多區域都有黑素細胞的存在。[黑色素細胞瘤很容易在此時發生](../Page/黑色素細胞瘤.md "wikilink")。

## 参见

  - [色素細胞](../Page/色素細胞.md "wikilink")
  - [黑素细胞痣](../Page/黑素细胞痣.md "wikilink")
  - [黑色素癌](../Page/黑色素癌.md "wikilink")

## 延伸閱讀

  -
  -
## 外部連結

  - \- "Eye: fovea, RPE"

  - \- "Integument: pigmented skin"

[Category:人体细胞](../Category/人体细胞.md "wikilink")
[Category:色素细胞](../Category/色素细胞.md "wikilink")
[Category:皮肤解剖学](../Category/皮肤解剖学.md "wikilink")