马来西亚电视台[ntv7和新加坡](../Page/ntv7.md "wikilink")[新传媒联合制作的马来西亚剧集](../Page/新传媒.md "wikilink")《**天使的烙印**》，翻拍自[郑秀珍和](../Page/郑秀珍.md "wikilink")[李锦梅主演的](../Page/李锦梅.md "wikilink")[新传媒剧集](../Page/新传媒.md "wikilink")《[天使的诱惑](../Page/天使的诱惑_\(2005年电视剧\).md "wikilink")》。[梁丽芳因此剧获得签约](../Page/梁丽芳.md "wikilink")[新传媒两年](../Page/新传媒.md "wikilink")，不只会在[马来西亚拍剧](../Page/马来西亚.md "wikilink")，也会于[新加坡拍剧](../Page/新加坡.md "wikilink")。

## 演员

### 主要演员

<table>
<tbody>
<tr class="odd">
<td><p><strong>演员</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>关系</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陈泓宇.md" title="wikilink">陈泓宇</a></p></td>
<td><p>叶子扬</p></td>
<td><p>叶秋月儿子<br />
胡咏怡前男友<br />
张凯晴前男友<br />
胡咏怡和张凯晴表哥<br />
张凯元表弟</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡佩璇.md" title="wikilink">蔡佩璇</a></p></td>
<td><p>胡咏怡</p></td>
<td><p>陈学文女友<br />
林美凤女儿<br />
叶子扬前女友<br />
张凯元堂妹<br />
叶秋月表侄女<br />
张凯晴同父异母妹妹</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁丽芳.md" title="wikilink">梁丽芳</a></p></td>
<td><p>张凯晴</p></td>
<td><p>叶子扬前女友<br />
张凯元堂妹<br />
叶秋月表侄女<br />
胡咏怡同父异母姐姐</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/章缜翔.md" title="wikilink">章缜翔</a></p></td>
<td><p>陈学文</p></td>
<td><p>胡咏怡男友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘冠伸.md" title="wikilink">刘冠伸</a></p></td>
<td><p>张凯元</p></td>
<td><p>张凯晴和胡咏怡堂哥<br />
叶子扬表哥</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 其他演员

<table>
<tbody>
<tr class="odd">
<td><p><strong>演员</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>关系</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/叶丽仪.md" title="wikilink">叶丽仪</a></p></td>
<td><p>林美凤</p></td>
<td><p>胡咏怡母亲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吕爱琼.md" title="wikilink">吕爱琼</a></p></td>
<td><p>叶秋月</p></td>
<td><p>叶子扬母亲<br />
张凯晴，张凯元，胡咏怡表姑妈</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 音乐

  - 主题曲:《就现在》-[吕婉嘉](../Page/吕婉嘉.md "wikilink")
  - 片尾曲:《从此以后》-[徐燕芳](../Page/徐燕芳.md "wikilink")

## 海外播放

香港[亚视於](../Page/亚视.md "wikilink")2008年4月播映。

## 收视

在[马来西亚的收视率打破](../Page/马来西亚.md "wikilink")《[原点](../Page/原點_\(電視劇\).md "wikilink")》的纪录，拥有90万观众收看。

[T天](../Page/category:ntv7中文剧.md "wikilink")

[T天](../Category/新傳媒合拍劇.md "wikilink")
[T天](../Category/亞洲電視外購劇集.md "wikilink")
[T天](../Category/2007年電視劇集.md "wikilink")
[T天](../Category/翻拍新加坡电视剧的新马电视剧.md "wikilink")