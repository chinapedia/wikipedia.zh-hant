**鸭绿江**（[满语](../Page/满语.md "wikilink")：
；[汉语拼音](../Page/汉语拼音.md "wikilink")：，）是位于[中华人民共和国和](../Page/中华人民共和国.md "wikilink")[朝鲜民主主义人民共和国之间的一条](../Page/朝鲜民主主义人民共和国.md "wikilink")[界江](../Page/界河.md "wikilink")。[入海口地区](../Page/入海口.md "wikilink")，朝方岛屿——绸缎岛和[薪岛等与中方陆地接壤](../Page/薪岛.md "wikilink")，因此鸭绿江江面末段完全在朝方一侧。目前河口为双方共用。

## 名称

古称[浿水](../Page/浿水.md "wikilink")，[汉朝称为](../Page/汉朝.md "wikilink")**马訾水**，[唐朝始称鸭绿江](../Page/唐朝.md "wikilink")（隋唐时期[浿水为大同江](../Page/浿水.md "wikilink")），因其江水清澈，关于鸭绿江其名的来历主要流行着两种说法：一说因江水颜色似鸭头之色而得名（唐朝[杜佑](../Page/杜佑.md "wikilink")《[通典](../Page/通典.md "wikilink")》）；二说因上游地区有鸭江和绿江两条支流汇入，故合而为一，并称为“鸭绿江”。另有源自于[满族先民的语言](../Page/满族.md "wikilink")（满语称为Yalu
ula，意为“边界之江”）的说法。史书记载[建州卫女真常年生活在](../Page/建州卫.md "wikilink")[婆猪江畔](../Page/婆猪江.md "wikilink")（鸭绿江支流）。
[Dandong,_Liaoning_Province.jpg](https://zh.wikipedia.org/wiki/File:Dandong,_Liaoning_Province.jpg "fig:Dandong,_Liaoning_Province.jpg")（左侧）\]\]
[Rural_live_in_North_Korea.JPG](https://zh.wikipedia.org/wiki/File:Rural_live_in_North_Korea.JPG "fig:Rural_live_in_North_Korea.JPG")

## 水系

鸭绿江发源于中国[吉林省东南](../Page/吉林省.md "wikilink")，中朝边境[长白山主峰南麓海拔](../Page/长白山.md "wikilink")2300米处的[长白山天池](../Page/天池_\(长白山\).md "wikilink")，然后流向西南，流经吉林省的[长白朝鲜族自治县](../Page/长白朝鲜族自治县.md "wikilink")、[临江市](../Page/临江市.md "wikilink")、[集安市](../Page/集安市.md "wikilink")、[辽宁省的](../Page/辽宁省.md "wikilink")[丹东市](../Page/丹东市.md "wikilink")，在辽宁省丹东市[东港附近入](../Page/東港市.md "wikilink")[黄海北部的](../Page/黄海.md "wikilink")[西朝鲜湾](../Page/西朝鲜湾.md "wikilink")。鸭绿江入海口是中国大陆海岸线的最北端。全长795公里，其中流经吉林省境界长575公里，辽宁省220公里；[流域面积](../Page/流域面积.md "wikilink")6.19万[平方公里](../Page/平方公里.md "wikilink")，中国境内流域面积约为3.25万平方公里。

### 支流

  - 北岸（中国境内）
      - [八道沟河](../Page/八道沟河.md "wikilink")
      - [三道沟河](../Page/三道沟河.md "wikilink")
      - [浑江](../Page/浑江.md "wikilink")
      - [红土崖河](../Page/红土崖河.md "wikilink")
      - [大罗圈沟河](../Page/大罗圈沟河.md "wikilink")
      - [哈泥河](../Page/哈泥河.md "wikilink")
      - [喇蛄河](../Page/喇蛄河.md "wikilink")
      - [苇沙河](../Page/苇沙河.md "wikilink")
      - [小新开河](../Page/小新开河.md "wikilink")
      - [富尔河](../Page/富尔河.md "wikilink")
      - [大雅河](../Page/大雅河.md "wikilink")
      - [半砬江](../Page/半砬江.md "wikilink")
      - [蒲石河](../Page/蒲石河.md "wikilink")
      - [瑷河](../Page/瑷河.md "wikilink")
      - [八道河](../Page/八道河.md "wikilink")
      - [草河](../Page/草河.md "wikilink")
      - [柳林河](../Page/柳林河.md "wikilink")
  - 南岸（朝鲜境内）
      - [秃鲁江](../Page/秃鲁江.md "wikilink")
      - [长津江](../Page/长津江.md "wikilink")
      - [慈城江](../Page/慈城江.md "wikilink")
      - [虚川江](../Page/虚川江.md "wikilink")
      - [忠满江](../Page/忠满江.md "wikilink")

## 水文

鸭绿江上下落差较大，源头与河口落差达到2440米。鸭绿江年降水量约在870毫米，自上游向下游渐增，70％集中6～9月。平均流量1040立方米／秒，年径流量327.6亿立方米。7～8月形成夏汛，最大流量是年均流量的15～25倍

## 气候与生物

鸭绿江全流域气候凉湿，分布以[红松](../Page/红松.md "wikilink")、[枫桦为主的针阔叶混交林](../Page/枫桦.md "wikilink")，下游多[栎林](../Page/栎树.md "wikilink")。有多种野生动、植物，在入海口一带，盛产大[银鱼](../Page/银鱼.md "wikilink")。入海口处建有鸭绿江口滨海湿地自然保护区。

## 桥梁

[Sino-Korean_Friendship_Bridge_across_the_Yalu.jpg](https://zh.wikipedia.org/wiki/File:Sino-Korean_Friendship_Bridge_across_the_Yalu.jpg "fig:Sino-Korean_Friendship_Bridge_across_the_Yalu.jpg")

20世纪初，鸭绿江上始建铁桥，先后在丹东和朝鲜[新义州之间建了二座](../Page/新义州.md "wikilink")。第一座建于1909年，是座开闭式桥梁。1950年[朝鲜战争中被](../Page/朝鲜战争.md "wikilink")[美国空軍](../Page/美国.md "wikilink")
炸毁，桥墩至今犹存，现辟有“断桥游览区”。第二座桥建于1940年，为[铁路](../Page/铁路.md "wikilink")、[公路两用桥](../Page/公路.md "wikilink")，全长940米，属中朝两国共管。它是中朝两国的交通要道，也是游人观光览胜的景点。

## 水库与水电站

鸭绿江落差大，水利资源丰富，干流上建有[云峰水库](../Page/云峰水库.md "wikilink")、[渭源水库](../Page/渭源水库.md "wikilink")、[水丰水库](../Page/水丰水库.md "wikilink")、[太平湾水库](../Page/太平湾水库.md "wikilink")，支流上建有[桓仁水库](../Page/桓仁水库.md "wikilink")、[回龙山水库](../Page/回龙山水库.md "wikilink")、[太平哨水库等大中型综合性水利工程](../Page/太平哨水库.md "wikilink")。总装机容量188万[千瓦](../Page/千瓦.md "wikilink")，年发电量77.4亿千瓦时（包括水丰水库两侧的扩建电站）。

[水丰水电站是鸭绿江上的第一座大型电站](../Page/水丰水库.md "wikilink")，建于1937年，1941年发电，1955年成立中朝鸭绿江水力发电公司，双方合营水丰发电厂并进行了恢复改建。

## 参见

  - [图们江](../Page/图们江.md "wikilink") - 中朝边境的另外一条河，與鴨綠江的源頭幾乎相連。

## 注释

[\*](../Category/鴨綠江.md "wikilink")
[Category:中国国家重点风景名胜区](../Category/中国国家重点风景名胜区.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")
[Category:中国边界河流](../Category/中国边界河流.md "wikilink")
[Category:朝鮮河流](../Category/朝鮮河流.md "wikilink")
[Category:辽宁河流](../Category/辽宁河流.md "wikilink")
[Category:吉林河流](../Category/吉林河流.md "wikilink")
[Category:平安北道地理](../Category/平安北道地理.md "wikilink")
[Category:慈江道地理](../Category/慈江道地理.md "wikilink")
[Category:中朝邊界](../Category/中朝邊界.md "wikilink")