**前山街道**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[珠海市](../Page/珠海市.md "wikilink")[香洲区下辖的一个](../Page/香洲区.md "wikilink")[街道](../Page/街道_\(行政区划\).md "wikilink")\[1\]。前山街道辖区面积53平方公里，下辖21个[社区](../Page/社区.md "wikilink")，常住人口17.8万人，其中户籍人口4.8万人。2003年全区国内生产总值约68亿元。是一个以建材为特色、工业为主体，经济商贸繁荣、文化教育发达、人口集中的城郊结合街道。在今日珠海市前山街道有沈志亮的紀念墓。

## 行政区划

前山街道下辖以下地区：\[2\]

。

## 教育機構

  - [前山中学](../Page/前山中学.md "wikilink")：为珠海市最古老的中学，有250年历史。始建于1757年。为珠海市直属中学。
  - [珠海市第十中学](../Page/珠海市第十中学.md "wikilink"):始建于2008年。为香洲区直属中学。
  - [前山小学](../Page/前山小学.md "wikilink")
  - [启明学校](../Page/启明学校.md "wikilink")
  - [暨南大学珠海校区](../Page/暨南大学#學校環境.md "wikilink")

## 参考资料

  - [前山概况](https://web.archive.org/web/20070922024704/http://www.qianshan.gd.cn/gaikuang.asp)，前山街道办事处网站，2007年3月13日造访。

## 外部链接

  - [前山街道办事处网站](https://web.archive.org/web/20070316193426/http://www.qianshan.gd.cn/)

[街道](../Category/香洲区行政区划.md "wikilink")
[香洲区](../Category/珠海街道_\(行政区划\).md "wikilink")

1.
2.