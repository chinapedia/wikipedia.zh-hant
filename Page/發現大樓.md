[Eureka_Tower_GF_Plaza_2017.jpg](https://zh.wikipedia.org/wiki/File:Eureka_Tower_GF_Plaza_2017.jpg "fig:Eureka_Tower_GF_Plaza_2017.jpg")
[Eureka_Tower_Residence_Entrance_2017.jpg](https://zh.wikipedia.org/wiki/File:Eureka_Tower_Residence_Entrance_2017.jpg "fig:Eureka_Tower_Residence_Entrance_2017.jpg")
**尤里卡大樓**（[英文](../Page/英文.md "wikilink")：**Eureka
Tower**）是一幢樓高的[摩天大樓](../Page/摩天大樓.md "wikilink")，位於[澳洲](../Page/澳洲.md "wikilink")[墨爾本](../Page/墨爾本.md "wikilink")[南岸](../Page/南岸_\(維多利亞\).md "wikilink")。大樓於2002年8月開始動工，外部於2006年6月竣工，10月11日正式開幕。項目由墨爾本[建築公司Fender](../Page/建築.md "wikilink")
Katsalidis Architects設計，Grocon（格勞羅澳大利亞，Grollo
Australia）施工。發展商為尤里卡大樓集團有限公司，一家由Daniel
Grollo（Grocon）、投資者Tab Fried及大樓建築師之一Nonda
Katsalidis組成的[合資公司](../Page/合資公司.md "wikilink")。以最高樓層計算，大樓是澳洲最高住宅大樓。然而，若把螺旋尖頂（又稱天桿）納入總高度，則[昆士蘭州](../Page/昆士蘭州.md "wikilink")[黃金海岸的](../Page/黃金海岸_\(澳洲\).md "wikilink")[Q1大樓位列澳洲最高住宅大樓](../Page/Q1大廈.md "wikilink")。\[1\]

## 名稱

尤里卡大樓的名稱源於[尤里卡柵欄事件](../Page/尤里卡柵欄事件.md "wikilink")（Eureka
Stockade），一場發生於1854年[維多利亞淘金熱](../Page/維多利亞淘金熱.md "wikilink")（Victorian
gold
rush）期間的[叛亂](../Page/叛亂.md "wikilink")。大樓的設計亦結合了該事件，其金冠象徵淘金熱，紅條紋象徵叛亂時的喋血。大樓的藍玻璃外牆代表尤利卡柵欄事件旗幟的藍色背景，而白線則代表尤利卡柵欄事件旗幟。

## 高度

單以樓頂高度或最高可住樓層計算，尤里卡大樓是世界第二高住宅大樓，亦是世界第二多居住用房樓層的大樓。大樓高300米，地面有91層，地下一層。它是世界七幢有90層或以上的大樓之一、[世界第43高大樓](../Page/摩天大樓列表.md "wikilink")、澳洲第二高大樓，以及[墨爾本最高大樓](../Page/:en:List_of_tallest_buildings_in_Melbourne.md "wikilink")。地下層及頭九層設有[停車場](../Page/停車場.md "wikilink")。由於大樓接近[地下水位及](../Page/地下水位.md "wikilink")[亞拉河](../Page/亞拉河.md "wikilink")（Yarra
River），興建地下層停車場耗費甚多。大樓共有84層公寓房間（包括停車場和公寓房間共用的樓層），餘下樓層用作興建設施及[觀景-{台}-](../Page/觀景台.md "wikilink")。根據美國的[世界高層建築與都市人居學會訂立的排名制度](../Page/世界高層建築與都市人居學會.md "wikilink")，尤里卡大樓在四個類別中攞有兩項最高大樓資格。其中，分別計算到達最高使用樓層的高度，以及到達樓頂的高度。相比之下，昆士蘭州[黃金海岸的](../Page/黃金海岸_\(昆士蘭\).md "wikilink")[Q1公寓大樓擁有最高可住樓層觀景](../Page/Q1_\(大樓\).md "wikilink")-{台}-，高度達，略低於尤里卡大樓最高可住樓層。Q1的最高豪華公寓離地面，尤里卡大樓豪華公寓則是。然而，附於Q1上方的螺旋尖頂（又稱天桿）在其餘兩個類別中超越了尤里卡大樓，以「到達螺旋尖頂（天桿）頂端、小尖塔、天線
、塔狀天線杆或旗桿的高度」取勝。

## 建設

尤里卡大樓以[鋼筋混凝土及](../Page/鋼筋混凝土.md "wikilink")[滑模鑄型方法建造](../Page/滑模鑄型.md "wikilink")。2004年12月9日，大樓的升降機槽取代麗愛圖塔觀景-{台}-（Rialto
Towers）的高度排名。2006年5月23日，大樓頂部的起重機由較小型的起重機拆缷，經送貨電梯運走。

尤里卡大樓最高10層有24[卡](../Page/克拉.md "wikilink")100%黃金的鍍金玻璃窗。黃金玻璃的裝嵌於2006年3月完工。同年7月，公寓住戶和租戶入住地下至80樓。「最高級樓層」（Summit
Levels）位於82-87樓，每層只有一個公寓房間。每個公寓房間有原價700萬[澳元的價格標籤](../Page/澳元.md "wikilink")，但費用只用於購置公寓空間；裝備公寓房間會收取額外費用。2006年11月，大樓由當時的[維多利亞州州長布萊克斯](../Page/維多利亞州.md "wikilink")（Steve
Bracks）正式開幕。\[2\] {{-}}

## 觀景-{台}-（尤里卡摩天-{台}-88）

[Eureka_Skydeck_88_2017.jpg](https://zh.wikipedia.org/wiki/File:Eureka_Skydeck_88_2017.jpg "fig:Eureka_Skydeck_88_2017.jpg")
[Eureka_Skydeck_balcony_close-up_from_ground.jpg](https://zh.wikipedia.org/wiki/File:Eureka_Skydeck_balcony_close-up_from_ground.jpg "fig:Eureka_Skydeck_balcony_close-up_from_ground.jpg")

尤里卡摩天-{台}-88（Eureka Skydeck
88）佔用尤里卡大樓88樓全層，離地面（，是[南半球最高的建築物內公眾觀景](../Page/南半球.md "wikilink")-{台}-。而[紐西蘭](../Page/紐西蘭.md "wikilink")[奧克蘭的](../Page/奧克蘭.md "wikilink")[天空塔有較高的景觀](../Page/天空塔.md "wikilink")。觀景-{台}-在2007年5月15日對外開放，並設入場費。

[Eureka_on_Yarra_1.jpg](https://zh.wikipedia.org/wiki/File:Eureka_on_Yarra_1.jpg "fig:Eureka_on_Yarra_1.jpg")對面\]\]

摩天-{台}-特別設有30個觀景器，協助遊客準確觀察墨爾本各處的重要地標，並提供望遠鏡免費使用。這裏有一個叫「The
Terrace」（即陽-{台}-）的戶外小區域，強風時會關閉；另有一個叫「The
Edge」（即邊緣）的玻璃立方，伸出大樓邊緣，增添遊人的觀賞體驗。2005年1月10日，尤里卡大樓的建築公司Grocon建議增設一個的通信線桿或瞭望塔。該提案目前已提交當地的計劃委員會。這個塔狀天線杆會提供到達大樓頂尖的冒險攀登。2006年4月16日，一項新提案宣佈，建築公司和發展商考慮為大樓增設「空中玻璃走廊」（skywalk），將遊客帶到350米高。擬議的結構也可以容納通信塔。

### The Edge

摩天-{台}-88設有「The
Edge」，一個伸出大樓的玻璃立方，懸於離地面將近約。遊客進入時，玻璃是不透明的。玻璃立方完全移出大樓邊線後，玻璃會發出砸碎玻璃和機器故障的聲響。\[3\]除了摩天-{台}-的入場費外，參觀The
Edge亦收取額外費用。The
Edge容許輪椅進入，七歲或以下的兒童則須由家長或監護人陪同。該處禁止攝影（摩天-{台}-其他位置除外），顧客可購買置身The
Edge時的紀念照片。

## 參見

  - [Q1大廈](../Page/Q1大廈.md "wikilink")

## 參考文獻

## 外部連結

  - [尤里卡大樓官方網站](http://www.eurekatower.com.au)

  - [尤里卡大樓官方相片集](https://web.archive.org/web/20080507075153/http://www.eurekatower.com.au/photos.asp)


  - [尤里卡大樓觀景台網站](https://web.archive.org/web/20160717152358/http://www.eurekalookout.com.au/)


  - [尤里卡大樓公寓房間](http://www.apartmentreviews.com.au/buildings/00096.htm)

  - [尤里卡大樓 - Emporis Page](http://www.emporis.com/en/wm/bu/?id=131515)

  -
  - [摩天漢世界 - 圖騰 - 澳洲 - 墨爾本 - 尤里卡大樓（Eureka
    Tower）](http://skyscrapers.cn/diagram/australia_oceania/australia/melbourne/file_Eureka_Tower.htm)

[Category:墨爾本摩天大樓](../Category/墨爾本摩天大樓.md "wikilink")
[Category:墨爾本建築物](../Category/墨爾本建築物.md "wikilink")
[Category:大洋洲地標](../Category/大洋洲地標.md "wikilink")
[Category:2006年完工建築物](../Category/2006年完工建築物.md "wikilink")
[Category:250米至299米高的摩天大樓](../Category/250米至299米高的摩天大樓.md "wikilink")

1.
2.
3.  <http://www.news.com.au/sundayheraldsun/story/0,,21637111-2862,00.html>