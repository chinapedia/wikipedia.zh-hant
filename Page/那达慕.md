[Naadamceremony2006.jpg](https://zh.wikipedia.org/wiki/File:Naadamceremony2006.jpg "fig:Naadamceremony2006.jpg")
**那达慕**（），意为“[游戏](../Page/游戏.md "wikilink")”或“[娛樂](../Page/娛樂.md "wikilink")”，为[蒙古族一年一度的传统](../Page/蒙古族.md "wikilink")[體能運動竞技节日](../Page/體能運動.md "wikilink")\[1\]，从传统宗教仪式“敖包塔克勒根”（[敖包祭拜](../Page/敖包.md "wikilink")）发展而来，一般于每年7月至8月间举行。過去只会在[蒙古国和](../Page/蒙古国.md "wikilink")[内蒙古自治区等地舉行](../Page/内蒙古自治区.md "wikilink")，而[北京](../Page/北京.md "wikilink")、[哈爾濱等大城市亦會有蒙古人舉辦那达慕大會](../Page/哈爾濱.md "wikilink")。

## 历史

[成吉思汗于](../Page/成吉思汗.md "wikilink")1206年统一蒙古后，除舉行首領大會制定法律等事宜外，每年于7月至8月草原水草丰美之时举行[忽里勒台聚会](../Page/忽里勒台.md "wikilink")，在会上进行[搏克](../Page/搏克.md "wikilink")、[射箭或者](../Page/射箭.md "wikilink")[赛马的其中一種竞赛](../Page/赛马.md "wikilink")。最早記載那达慕活動的是銘刻在崖上的《成吉思汗石文》。据其记载，成吉思汗于1206年为纪念完成蒙古統一，在[布哈蘇齊举行了一次那达慕大会](../Page/布哈蘇齊.md "wikilink")，会上进行了射箭竞技。在這次比賽中，成吉思汗的侄子[叶松海洪霍都爾在](../Page/叶松海洪霍都爾.md "wikilink")335[庹外射中目標](../Page/庹.md "wikilink")。13世紀成書的《[蒙古秘史](../Page/蒙古秘史.md "wikilink")》亦多處提到射箭的比賽場面。后來[蒙古帝国将摔跤](../Page/蒙古帝国.md "wikilink")、射箭、赛马定为“男子三艺”，要求每名蒙古族男子都必须操习。

[清朝時期](../Page/清朝.md "wikilink")，那达慕已发展成为官方组织的定期活动，其規模、形式及內容均有所成長，當時各蒙古族王公的[苏木](../Page/苏木_\(行政区划\).md "wikilink")、[旗和](../Page/旗_\(行政区划\).md "wikilink")[盟均定期自行组织那达慕](../Page/盟.md "wikilink")。

那達慕大會一直是蒙古族王公、貴族牧主及富商的活動。1949年後正式開放予蒙古族平民，同時這個節日亦促進了物資交流及文化科學知識的融合\[2\]。

## 形式

早期各部落舉辦那达慕大會時只會挑選男兒三藝：摔跤、射箭、赛马的其中一種作競賽，同時還要在大會期間要進行大規模祭祀活動，[喇嘛們焚香點燈](../Page/喇嘛.md "wikilink")，唸經頌佛，祈求神靈保祐\[3\]。現時那达慕大會比賽已加入其他民族傳統項目，例如[賽布魯](../Page/賽布魯.md "wikilink")、[套馬及](../Page/套馬.md "wikilink")[蒙古象棋等](../Page/蒙古象棋.md "wikilink")，而部分地方更會引進現代的運動項目，例如[田徑](../Page/田徑.md "wikilink")、[排球及](../Page/排球.md "wikilink")[籃球等](../Page/籃球.md "wikilink")。

那达慕大會有各式各樣的文化活動。過去，[漢族](../Page/漢族.md "wikilink")、[回族及](../Page/回族.md "wikilink")[達斡爾族人都會身穿節日盛裝出席](../Page/達斡爾族.md "wikilink")。而近代，大會中有[蒙古長調](../Page/蒙古長調.md "wikilink")、[馬頭琴的文藝晚會](../Page/馬頭琴.md "wikilink")\[4\]，亦有、[歌舞](../Page/歌舞.md "wikilink")、影視放映、推廣科技、[帕日吉](../Page/帕日吉.md "wikilink")、[沙塔尔](../Page/蒙古象棋.md "wikilink")、[宝根·吉日格等](../Page/蒙古鹿棋.md "wikilink")[吉日格](../Page/吉日格.md "wikilink")，現時的那达慕大會被包裝成一項旅遊項目\[5\]。

## 现状

現行规模最大的那达慕是[蒙古国举行的](../Page/蒙古国.md "wikilink")[国庆伊赫那达慕大会](../Page/国庆.md "wikilink")，首届国庆伊赫那达慕大会于1921年在[阿拉坦布拉格Tesregtolgoi廣場舉行](../Page/买卖城.md "wikilink")，由[苏赫·巴托尔主持召开](../Page/苏赫·巴托尔.md "wikilink")。每年7月11日至7月13日在首都[乌兰巴托举行](../Page/乌兰巴托.md "wikilink")，由蒙古国国庆那达慕委员会统筹举办。每年7月11日，乌兰巴托会举行盛大开幕仪式，其后举行3项[竞技比赛](../Page/竞技.md "wikilink")。其中[赛马和](../Page/赛马.md "wikilink")[射箭也对女选手开放參加](../Page/射箭.md "wikilink")。摔跤比賽勝出者被人們公認为強壯、聰穎、鶴立雞群和衷心于傳統文化；射箭比賽勝出者被人們公認為堅毅、有耐性；賽馬比賽中勝出的騎手（通常是小孩兒），被讚賞為速度快和萬人之首。5匹在比賽中勝出的馬將會被灑[馬奶酒及以](../Page/馬奶酒.md "wikilink")[詩詞和](../Page/詩詞.md "wikilink")[音樂讚頌](../Page/音樂.md "wikilink")。

在[蒙古国及](../Page/蒙古国.md "wikilink")[内蒙古各地区](../Page/内蒙古.md "wikilink")，每年7月至8月间也会举行规模不等、各自独立的那达慕大会。[俄罗斯](../Page/俄罗斯.md "wikilink")[图瓦共和国也会在每年](../Page/图瓦共和国.md "wikilink")8月15日举行那达慕大会。在[内蒙古地区的那达慕大会上](../Page/内蒙古.md "wikilink")，女子也可以参加[摔跤比赛](../Page/摔跤.md "wikilink")。

## 参考文献

  - 香港文汇报 2007年7月30日 A28版 内蒙古成立60周年专辑三之三文化旅游篇
  - 蒙古国刊物[今日蒙古](../Page/今日蒙古.md "wikilink")（Mongolia Today）
  - 蒙古国报纸[乌兰巴托邮报](../Page/乌兰巴托邮报.md "wikilink")(Ulaanbaatar Post of
    Mongolia)
  - 柏宇等編, (2003), 中外節日紀念日大全, 北京:中國林業出版社

## 延伸阅读

  - [国庆“伊赫那达慕”
    蒙古国的狂欢节](http://news3.xinhuanet.com/sports/2007-07/11/content_6361918.htm)
  - [蒙古文化论坛有关那达慕的帖子](https://web.archive.org/web/20070927051100/http://www.qingis.com/sf/topic.asp?topic_id=1074)
  - [青城驿站--中国蒙古学个人网站](http://www.qingis.com)
  - [蒙古情--中国蒙古学网站](http://www.mgqing.com)

[Category:中国非物质文化遗产](../Category/中国非物质文化遗产.md "wikilink")
[Category:蒙古族節日](../Category/蒙古族節日.md "wikilink")

1.  [蒙古國的那达慕大會](http://www.chiculture.net/1302/html/1302festivals_i3.shtml)
2.  [草原盛會——那達慕](http://www.nmg.xinhuanet.com/nmgdcy/lvyou/tsly_ndm.htm)
3.  [那達慕大會](http://big5.huaxia.com/xj-tw/2005/00352459.html)
4.  [“江格爾故鄉”新疆和布克賽爾縣舉辦那達慕大會](http://big51.chinataiwan.org/xwzx/dlzl/200908/t20090807_967475.htm)
5.  [內蒙古第二十屆旅遊那達慕大會在烏蘭察布舉行](http://big51.chinataiwan.org/xwzx/dlzl/200907/t20090727_959413.htm)