《**黄山伐**》（，）是一部2003年的[韩国戰爭喜剧电影](../Page/韩国.md "wikilink")，导演[李濬益](../Page/李濬益.md "wikilink")，[-{朴}-重勳](../Page/朴重勋.md "wikilink")、[鄭進永主演](../Page/鄭進永.md "wikilink")。讲述7世纪时[唐朝出兵和](../Page/唐朝.md "wikilink")[新罗一起灭亡](../Page/新罗.md "wikilink")[百济的故事](../Page/百济.md "wikilink")。

本電影與其他韓國電影的不同之處是它的對白中含有韓國不同地域的[方言](../Page/方言.md "wikilink")（飾演百济国角色的說全罗道方言，新罗国角色的說庆尚道方言、忠清道方言，飾演高句丽国角色的說平安道方言）。

## 演员陣容

  - [-{朴}-重勳](../Page/朴重勳.md "wikilink") 飾
    [阶伯](../Page/阶伯.md "wikilink")
  - [鄭進永](../Page/鄭進永.md "wikilink") 飾 [金庾信](../Page/金庾信.md "wikilink")
  - [李文植](../Page/李文植.md "wikilink") 飾 百济士兵
  - [金炳書](../Page/金炳書.md "wikilink")
  - [金允泰](../Page/金允泰.md "wikilink")
  - [李浩城](../Page/李浩城.md "wikilink") 飾 [金春秋](../Page/金春秋.md "wikilink")
  - [安内相](../Page/安内相.md "wikilink") 飾 [金法敏](../Page/金法敏.md "wikilink")
  - [柳承秀](../Page/柳承秀.md "wikilink") 飾 [金仁问](../Page/金仁问.md "wikilink")
  - [申正根](../Page/申正根.md "wikilink") 飾 [金欽純](../Page/金欽純.md "wikilink")
  - [金陆龙](../Page/金陆龙.md "wikilink") 飾 [唐高宗](../Page/唐高宗.md "wikilink")
  - [高牧春](../Page/高牧春.md "wikilink")（中國演員） 飾
    [蘇定方](../Page/蘇定方.md "wikilink")

### 特別出演

  - [李原種](../Page/李原種.md "wikilink") 飾
    [渊盖苏文](../Page/渊盖苏文.md "wikilink")
  - [吴知明](../Page/吴知明.md "wikilink") 飾 [义慈王](../Page/义慈王.md "wikilink")
  - [金宣兒](../Page/金宣兒.md "wikilink") 飾 阶伯妻
  - [金勝友](../Page/金勝友.md "wikilink")
  - [申鉉濬](../Page/申鉉濬.md "wikilink")

## 外部链接

  - [黄山伐介绍](https://web.archive.org/web/20060614105537/http://et.21cn.com/movie/haibao/waiyu/2006/05/22/2585717.shtml)

[H](../Category/2003年电影.md "wikilink")
[H](../Category/韓語電影.md "wikilink")
[H](../Category/韓國喜劇片.md "wikilink")
[H](../Category/韓國戰爭片.md "wikilink")
[H](../Category/2000年代喜劇片.md "wikilink")
[H](../Category/2000年代戰爭片.md "wikilink")
[H](../Category/新羅背景作品.md "wikilink")
[H](../Category/百濟背景作品.md "wikilink")
[H](../Category/唐朝背景電影.md "wikilink")
[H](../Category/金庾信題材作品.md "wikilink")
[H](../Category/階伯題材作品.md "wikilink")
[H](../Category/李濬益執導電影.md "wikilink")