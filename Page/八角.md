**八角**（[学名](../Page/学名.md "wikilink")：**），又称**八角茴香**、**大料**和**[大茴香](../Page/大茴香.md "wikilink")**（在某些地方，大茴香指的不是八角），是木蘭藤目[八角属的一种植物](../Page/八角属.md "wikilink")。其同名的干燥果实是[中国菜和东南亚地区烹饪的](../Page/中国菜.md "wikilink")[调味料之一](../Page/调味料.md "wikilink")\[1\]。

## 形态

[缩略图](https://zh.wikipedia.org/wiki/File:Illicium_verum00.jpg "fig:缩略图")
[树皮灰色至红褐色](../Page/树皮.md "wikilink")。椭圆形全缘单叶，[披针形生长](../Page/披针形.md "wikilink")，叶为革质，短柄，长6－12厘米，宽2－5厘米，上表面可见透明油点。春秋季开花。花单生于叶腋部。有3片黄绿色萼片；6－9片花被，呈粉红至深红色。

秋季至第二年春季结果。[聚合果呈星状的八角形](../Page/聚合果.md "wikilink")，长1－2厘米，宽3－5厘米，高\<1厘米。成熟后由绿变黄。[蓇葖果顶端钝或钝尖](../Page/蓇葖果.md "wikilink")，每个内含6毫米长的种子一颗。

注意同属植物[野八角的果实与八角非常相似](../Page/野八角.md "wikilink")，但有毒，应避免混淆。

## 分布

为生长在湿润、温暖半阴环境中的常绿[乔木](../Page/乔木.md "wikilink")，高可至20米。主要分布于中国大陆南方。

## 用途

果实在秋冬季采摘，干燥后呈红棕色或黄棕色。气味芳香而甜。全果或磨粉使用。挥發油的重要成分包括[茴香醚](../Page/茴香醚.md "wikilink")（Anethole）、[黄樟醚](../Page/黄樟醚.md "wikilink")（Safrole）、[茴香醛](../Page/茴香醛.md "wikilink")（Anisaldehyde）和[茴香酮](../Page/茴香酮.md "wikilink")（Anisylacetone）。

### [中药用途](../Page/中药.md "wikilink")

药性温热散寒，理气止痛。治疗胃寒、[呕吐和腰痛](../Page/呕吐.md "wikilink")。

### 调味用途

在中国菜中，常少量加在鱼肉类材料中用来烹饪红烧、[卤制和五香等炖制的厚味菜肴](../Page/滷.md "wikilink")，也是[五香粉的成分之一](../Page/五香粉.md "wikilink")。

在[绍兴一带](../Page/绍兴.md "wikilink")，是[茴香豆的主要调味料](../Page/茴香豆.md "wikilink")，茴香豆因此而得名。

在中国南方和[越南是](../Page/越南.md "wikilink")[米粉汤的调味料](../Page/米粉.md "wikilink")。

### 其他用途

因为八角和[甘草的甜味接近](../Page/甘草.md "wikilink")，可以用来在食物中替代甘草。

所含的[莽草酸是制造抗](../Page/莽草酸.md "wikilink")[病毒药物](../Page/病毒.md "wikilink")[奥司他韦的合成原料之一](../Page/奥司他韦.md "wikilink")。该药物特别用于[流感的防治](../Page/流感.md "wikilink")\[2\]。但未經提煉的八角本身并无类似作用。

可以做為食慾抑制劑。促進腎臟、肝臟和脾臟功能，可以清肺，減輕腹部疼痛、結腸疾病、腸胃脹氣和腸胃道痙攣，可治療胃酸過多，對化學治療或放射線治療的病人有益。

## 生产与贸易

2000年全世界八角栽培面积约36.76万公顷，八角干果产量约4万吨。据不完全统计，中国八角种植面积约28.67万公顷，八角干果产量约3.2万吨，年出口八角干果约2000至3000吨，是八角的最主要生产国与出口国。其中广西种植面积24.67万公顷，八角干果产量3.097万吨。其它产量较大省份是云南（主要在[文山州](../Page/文山州.md "wikilink")）与广东。越南为世界第二生产国，其面积约5.6万公顷，八角干果产量约5000吨。

## 参考文献

## 外部連結

  - [八角茴香
    Bajiaohuixiang](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00387)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [八角茴香
    Bajiaohuixiang](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00155)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [八角茴香 Ba Jiao Hui
    Xiang](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00664)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [莽草酸 Shikimic
    acid](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00045)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

## 参见

  - [黄樟素](../Page/黄樟素.md "wikilink")

[樹](../Category/藥用植物.md "wikilink")
[verum](../Category/八角属.md "wikilink")
[Category:調味料](../Category/調味料.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")

1.
2.