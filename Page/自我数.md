**自我数**也叫哥伦比亚数，是在给定进制中，不能由任何一个整数加上这个整数的各位数字和生成的数，称之为自我数。例如：21不是自我数，因为21可以由整數15和15的各位數字1,5生成，即21＝15＋1＋5。20不能满足上述条件，所以它是自我数。1949年[印度](../Page/印度.md "wikilink")[数学家D](../Page/数学家.md "wikilink").R.
Kaprekar第一次描述这种数。

开始的几个十进制自我数是：

[1](../Page/1.md "wikilink"), [3](../Page/3.md "wikilink"),
[5](../Page/5.md "wikilink"), [7](../Page/7.md "wikilink"),
[9](../Page/9.md "wikilink"), [20](../Page/20.md "wikilink"),
[31](../Page/31.md "wikilink"), [42](../Page/42.md "wikilink"),
[53](../Page/53.md "wikilink"), [64](../Page/64.md "wikilink"),
[75](../Page/75.md "wikilink"), [86](../Page/86.md "wikilink"),
[97](../Page/97.md "wikilink"), [108](../Page/108.md "wikilink"),
[110](../Page/110.md "wikilink"), [121](../Page/121.md "wikilink"),
[132](../Page/132.md "wikilink"), [143](../Page/143.md "wikilink"),
[154](../Page/154.md "wikilink"), [165](../Page/165.md "wikilink"),
[176](../Page/176.md "wikilink"), [187](../Page/187.md "wikilink"),
[198](../Page/198.md "wikilink"), [209](../Page/209.md "wikilink"),
[211](../Page/211.md "wikilink"), [222](../Page/222.md "wikilink"),
[233](../Page/233.md "wikilink"), [244](../Page/244.md "wikilink"),
[255](../Page/255.md "wikilink"), [266](../Page/266.md "wikilink"),
[277](../Page/277.md "wikilink"), [288](../Page/288.md "wikilink"),
[299](../Page/299.md "wikilink"), [310](../Page/310.md "wikilink"),
[312](../Page/312.md "wikilink"), [323](../Page/323.md "wikilink"), 334,
345, 356, 367, 378, 389, [400](../Page/400.md "wikilink"), 411, 413,
424, 435, 446, 457, 468, 479, 490, 501,
[512](../Page/512.md "wikilink"), 514, 525

一般的，在偶数为底的[进制中](../Page/进制.md "wikilink")，所有小于这个偶数的奇数都是自我数，因为这个进制中所有的奇数加上1结果都是偶数。在奇数为底的进制中，所有的奇数都是自我数。

下面的线性[递推关系式生成](../Page/递推关系.md "wikilink")[十进制的自我数](../Page/十进制.md "wikilink")：

\(C_k = 8 \cdot 10^{k - 1} + C_{k - 1} + 8\)

其中*C*<sub>1</sub> = 9

在[二进制中](../Page/二进制.md "wikilink")

\(C_k = 2^j + C_{k - 1} + 1\)

*j*表示这个数的位数。我们可以生成一个在以*b*为底的进制中生成自我数的线性递推关系式。

\(C_k = (b - 2)b^{k - 1} + C_{k - 1} + (b - 2)\)

其中 *C*<sub>1</sub> = *b* - 1适用于偶数为底的进制中， *C*<sub>1</sub> = *b* -
2适用于奇数为底进制中。

这个线性递推关系式的存在说明在任意数为底的进制中自我数是无穷的。

[Category:數字相關的數列](../Category/數字相關的數列.md "wikilink")