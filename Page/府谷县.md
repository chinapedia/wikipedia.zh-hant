**府谷县**是[中華人民共和國](../Page/中華人民共和國.md "wikilink")[陕西省](../Page/陕西省.md "wikilink")[榆林市所辖的一个](../Page/榆林市.md "wikilink")[县](../Page/县.md "wikilink")。总[面积为](../Page/面积.md "wikilink")3212[平方公里](../Page/平方公里.md "wikilink")，2002年[人口为](../Page/人口.md "wikilink")22万人。

## 历史

1937年與[神木縣合併為](../Page/神木縣.md "wikilink")[神府縣](../Page/神府縣.md "wikilink")（取二縣首字）。初期縣治[賀家川](../Page/賀家川.md "wikilink")（今神木縣賀家川鎮），1939年遷[沙峁鎮](../Page/沙峁鎮.md "wikilink")（今屬神木縣），1944年徙[溫家川](../Page/溫家川.md "wikilink")（今屬神木縣），1948年6月又遷[高家堡](../Page/高家堡.md "wikilink")（今神木縣高家堡鎮），但縣治一直未離開神木縣範圍。1950年5月撤銷合併，恢復神木、府谷二縣。

2016年10月24日，该县新民镇新民医院附近的一栋建筑发生[爆炸](../Page/2016年10月24日陕西府谷爆炸事故.md "wikilink")，造成14人死亡，100多人受伤\[1\]。当地群众称爆炸与建筑物地下室由[煤矿企业私藏的](../Page/煤矿.md "wikilink")[炸药有关](../Page/炸药.md "wikilink")\[2\]。

## 地理交通

府谷县位于陕西省最北部，北纬38度42分至39度35分，东经110度22分至111度14分。
县内有府（谷）店（塔）一级公路通往神木，另有多条二级公路通往[太原](../Page/太原.md "wikilink")、[包头](../Page/包头.md "wikilink")、[鄂尔多斯](../Page/鄂尔多斯.md "wikilink")，府谷也是正在建设的“榆（林）商（州）高速公路”起点。县城至[榆林市](../Page/榆林市.md "wikilink")219公里；至省会[西安](../Page/西安.md "wikilink")595公里；至[山西省](../Page/山西省.md "wikilink")[省會](../Page/省會.md "wikilink")[太原](../Page/太原.md "wikilink")370公里。[铁路方面](../Page/铁路.md "wikilink")，有西煤东运大通道[神朔铁路通过县境](../Page/神朔铁路.md "wikilink")，神木北（店塔）—[大同往返旅客列车途径在府谷县城并设有客运站](../Page/大同市.md "wikilink")。

## 行政区划

下辖14个[镇](../Page/镇.md "wikilink")：

。

## 参考资料

## 外部链接

  - [府谷县政府](http://www.fg.gov.cn/)

[府谷县](../Category/府谷县.md "wikilink") [县](../Category/榆林区县.md "wikilink")
[榆林](../Category/陕西省县份.md "wikilink")

1.  [陕西爆炸增至14死
    “陕西爆炸”成敏感词](http://china.dwnews.com/news/2016-10-24/59777407.html)
2.