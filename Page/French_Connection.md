[Fcuk_French_Connection_Shop_Covent_Garden.jpg](https://zh.wikipedia.org/wiki/File:Fcuk_French_Connection_Shop_Covent_Garden.jpg "fig:Fcuk_French_Connection_Shop_Covent_Garden.jpg")
**FCUK**（**French Connection United
Kingdom**）是[英國](../Page/英國.md "wikilink")[服裝公司French](../Page/服裝.md "wikilink")
Connection所擁有的服飾品牌。成立於1972年，初期只以「French
Connection」作為品牌名的該公司，自1997年時開始大規模地推廣其新的品牌名稱縮寫「fcuk」（通常採全[小寫方式標示](../Page/小寫.md "wikilink")），由於名稱縮寫十分接近英文[粗口](../Page/粗口.md "wikilink")「[fuck](../Page/fuck.md "wikilink")」而容易被混淆，反而提高了知名度\[1\]。主要銷售地區為[歐洲](../Page/歐洲.md "wikilink")、[北美](../Page/北美.md "wikilink")、[亞洲與](../Page/亞洲.md "wikilink")[澳大利亞的French](../Page/澳大利亞.md "wikilink")
Connection，總部設於[倫敦](../Page/倫敦.md "wikilink")。

## 參考文獻

## 外部連結

  - [French Connection官方網站](http://www.frenchconnection.com/)

[Category:服裝品牌](../Category/服裝品牌.md "wikilink")
[Category:英國服裝公司](../Category/英國服裝公司.md "wikilink")
[Category:1972年成立的公司](../Category/1972年成立的公司.md "wikilink")
[Category:伦敦证券交易所上市公司](../Category/伦敦证券交易所上市公司.md "wikilink")

1.