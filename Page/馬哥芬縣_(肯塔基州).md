**馬哥芬县**（**Magoffin County,
Kentucky**）是位於[美國](../Page/美國.md "wikilink")[肯塔基州東部的一個縣](../Page/肯塔基州.md "wikilink")。面積801平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口13,332人。縣治[薩利爾斯維爾](../Page/薩利爾斯維爾_\(肯塔基州\).md "wikilink")
（Salyersville）。

成立於1860年2月22日。縣名紀念[州議員](../Page/肯塔基州州議會.md "wikilink")、第二十一任[州長](../Page/肯塔基州州長.md "wikilink")[比利亞·馬哥芬](../Page/比利亞·馬哥芬.md "wikilink")
（Beriah Magoffin）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[M](../Category/肯塔基州行政区划.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.