**Volari 8300**是一種[PCI-ELow](../Page/PCI-E.md "wikilink")
Profile[顯示卡](../Page/顯示卡.md "wikilink")，由[XGI設計](../Page/XGI.md "wikilink")。自前一代[Volari](../Page/Volari.md "wikilink")
V系列[AGP顯示卡慘淡收場後](../Page/AGP.md "wikilink")，XGI索性苦修一年，直接推出原生PCI-E顯示卡。本作不以效能出名，而是靠其優秀的視訊播放質素。

  - 核心：
      - [0.13微米製程](../Page/0.13微米製程.md "wikilink")
      - 9千萬個電晶體
      - 2x2條像素流水線
      - 4個頂點著色引擎
      - 時脈：300MHz
      - Direct X 9.0
          - Pixel Shader 2.0
          - Vertex Shader 2.0
      - TrueVideo
  - 記憶體：
      - 2顆GDDR記憶體BGA顆粒，時脈：600MHz
      - 總共32MB
      - 64Bit頻寬

最\*\*eXtreme Cache -- 與nVidia TurboCache及ATi
HyperMemory技術類似，透過PCI-E介面借用系統記憶體，作為顯視記憶體。這是動態借用的，。當執行2D程序時，便會釋放借用了的系統記憶體。

**Volair 8300 Mobile**是Volair 8300的流動版本。架構与Volair
8300没有分別，只是核心運作電壓較低，減少消耗電池。它亦會采用nVidia提倡的[MXM繪圖接口](../Page/MXM.md "wikilink")，手提電腦可以簡單地过渡使用
Volair 8300 Mobile，而不需重新設計基板，減少研發時間。

  - MPEG-2硬體解碼
  - 核心整合兩組350MHz RAMDAV
  - 核心整合NTSC/PAL編碼器
  - VGA介面最高解析度：2048x1536
  - DVI介面最高解析度：1600x1200
  - HDTV最高輸出：720p而不是1080i，比[S3
    Graphics](../Page/S3_Graphics.md "wikilink") 的[ChromeS20
    Series落後](../Page/ChromeS20_Series.md "wikilink")
  - 图元基础动作处理
  - 边缘自适应性逐行扫描功能
  - 逆转3:2 Pull Down功能
  - 动物和物件为主的自适应柔边
  - 针对超清晰TFT LCD影像画质的驱动技术
  - 采用立体插补技术进行边缘修复TrueVideo是XGI的提高視訊的播放質素技術，是Volari
    8300的賣点。与[nVidia的](../Page/nVidia.md "wikilink")[Pure
    Video](../Page/Pure_Video.md "wikilink")，[ATi的](../Page/ATi.md "wikilink")[AVIVO](../Page/AVIVO.md "wikilink")，[S3
    Graphics的](../Page/S3_Graphics.md "wikilink")[Chromotion 3.0 Video
    Engine](../Page/Chromotion_3.0_Video_Engine.md "wikilink")、[矽統科技的](../Page/SIS.md "wikilink")[Real
    Video相似](../Page/Real_Video.md "wikilink")。

[Category:顯示卡](../Category/顯示卡.md "wikilink")