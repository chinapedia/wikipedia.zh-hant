**米利都**（）是位于[安纳托利亚西海岸线上的一座](../Page/安纳托利亚.md "wikilink")[古希腊](../Page/古希腊.md "wikilink")[城邦](../Page/城邦.md "wikilink")，靠近米安得尔河口。它在[赫梯文献中被称为](../Page/赫梯.md "wikilink")*Millawanda*或者*Milawata*，在[荷马的](../Page/荷马.md "wikilink")《[伊利亚特](../Page/伊利亚特.md "wikilink")》中也有出现。公元前1500年左右，一些从[克里特岛来的移民定居于此](../Page/克里特岛.md "wikilink")，随后，这个城市就成为了[爱奥尼亚十二城邦之一](../Page/爱奥尼亚.md "wikilink")。在公元前6世纪它建立起了强大的海上力量，并建立了许多[殖民地](../Page/殖民地.md "wikilink")。在[希波战争前它处于](../Page/希波战争.md "wikilink")[波斯统治下](../Page/阿契美尼德王朝.md "wikilink")。米利都拥有一批著名的思想家，如[泰勒斯](../Page/泰勒斯.md "wikilink")、[阿那克西曼德](../Page/阿那克西曼德.md "wikilink")、[阿那克西美尼等](../Page/阿那克西美尼.md "wikilink")，世称[米利都学派](../Page/米利都学派.md "wikilink")。

米利都曾先後被[赫梯帝國](../Page/赫梯帝國.md "wikilink")、弗里吉亚人、[吕底亚](../Page/吕底亚.md "wikilink")、[马其顿帝国](../Page/马其顿帝国.md "wikilink")、[羅馬帝國](../Page/羅馬帝國.md "wikilink")、[拜占庭帝國和](../Page/拜占庭帝國.md "wikilink")[鄂圖曼帝國所統治](../Page/鄂圖曼帝國.md "wikilink")。在鄂圖曼帝國統治時期，米利都被用作一個與[威尼斯進行貿易的港口](../Page/威尼斯.md "wikilink")，後來港口淤塞，城市被廢棄。今日米利都的廢墟距離海面數十公里，在衛星地圖上
37°31.8'N 27°16.7'E 的位置，於土耳其[艾登省內](../Page/艾登省.md "wikilink")。
[Markttor_von_Milet.jpg](https://zh.wikipedia.org/wiki/File:Markttor_von_Milet.jpg "fig:Markttor_von_Milet.jpg")[柏林的](../Page/柏林.md "wikilink")[佩加蒙博物館](../Page/佩加蒙博物館.md "wikilink")\]\]

## 名人

  - [米利都的泰勒斯](../Page/泰勒斯.md "wikilink") (c. 624 BC – c. 546 BC), 哲学家

  - [阿那克西曼德](../Page/阿那克西曼德.md "wikilink") (c. 610 BC – c. 546 BC), 哲学家

  - (fl. c. 550 BC), writer

  - [阿那克西美尼](../Page/阿那克西美尼.md "wikilink") (c. 585 BC – c. 525 BC), 哲学家

  - [希波达莫斯](../Page/希波达莫斯.md "wikilink") (c. 498 — 408 BC), 城市规划

  - [阿斯帕齐娅](../Page/阿斯帕齐娅.md "wikilink") (c. 470 – 400 BC)
    ，[伯里克利情妇](../Page/伯里克利.md "wikilink")

  - [Aristides](../Page/Aristides_of_Miletus.md "wikilink"), writer

  - [米利都的赫卡塔埃乌斯](../Page/米利都的赫卡塔埃乌斯.md "wikilink"), 历史学家

  - (fl. 6th century), Greek chronicler and biographer

  - [米利都的伊西多尔](../Page/米利都的伊西多尔.md "wikilink") (fl. 4th–5th century),
    Greek architect

  - [阿里斯塔格拉斯](../Page/阿里斯塔格拉斯.md "wikilink") (fl. 5th–6th century),
    Tyrant of Miletus

  - [留基伯](../Page/留基伯.md "wikilink") (fl. first half of 5th century BC),
    philosopher and originator of Atomism (his association with Miletus
    is traditional, but disputed)

[Category:希腊城邦](../Category/希腊城邦.md "wikilink")