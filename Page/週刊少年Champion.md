《**週刊少年Champion**》（）是由[日本](../Page/日本.md "wikilink")[秋田書店發行的週刊](../Page/秋田書店.md "wikilink")[漫畫雜誌](../Page/漫画.md "wikilink")，以[少年漫畫為主](../Page/少年漫画.md "wikilink")。1969年7月15日創刊，創刊初期雜誌名為少年Champion(少年チャンピオン)。

2005年度每期發行量大約50萬冊左右。

與其他週刊少年漫畫雜誌相比較，《週刊少年Champion》對所刊登漫畫的風格及表現手法的限制較為寬鬆。

《週刊少年Champion》每週四發售，北海道、九州及其他部分地區是星期五，每本定價270[日圓](../Page/日圓.md "wikilink")。

## 連載過的作品

  - [怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")——[手塚治虫](../Page/手塚治虫.md "wikilink")
  - [甜心戰士](../Page/甜心戰士.md "wikilink")——[永井豪](../Page/永井豪.md "wikilink")
  - [乳旋風](../Page/乳旋風.md "wikilink")（[巨乳學園](../Page/巨乳學園.md "wikilink")）——[松山清治](../Page/松山清治.md "wikilink")
  - [烏爾](../Page/乌尔.md "wikilink")——[石川球太](../Page/石川球太.md "wikilink")
  - [赤影](../Page/赤影.md "wikilink")——[横山光輝](../Page/横山光輝.md "wikilink")
  - [大甲子園](../Page/大甲子園.md "wikilink")
    ——[水島新司](../Page/水島新司.md "wikilink")
  - [浦安鉄筋家族](../Page/浦安鉄筋家族.md "wikilink")——[浜岡賢次](../Page/浜岡賢次.md "wikilink")
  - [元祖！浦安鉄筋家族](../Page/元祖！抓狂一族.md "wikilink")——[浜岡賢次](../Page/浜岡賢次.md "wikilink")
  - [ORANGE](../Page/ORANGE.md "wikilink")——[能田達規](../Page/能田達規.md "wikilink")
  - [日本沉沒](../Page/日本沉沒.md "wikilink")——[小松左京](../Page/小松左京.md "wikilink")・[齊藤隆夫](../Page/齊藤隆夫.md "wikilink")
  - [刃牙](../Page/刃牙.md "wikilink")（Baki）——[板垣惠介](../Page/板垣惠介.md "wikilink")
  - [七小花](../Page/七小花.md "wikilink")——[今川泰宏](../Page/今川泰宏.md "wikilink")
  - [聖鬥士星矢 NEXT DIMENSION
    冥王神話](../Page/聖鬥士星矢_NEXT_DIMENSION_冥王神話.md "wikilink")——[車田正美](../Page/車田正美.md "wikilink")（連載中）
  - [聖鬥士星矢 THE LOST CANVAS
    冥王神話](../Page/聖鬥士星矢_THE_LOST_CANVAS_冥王神話.md "wikilink")——[車田正美](../Page/車田正美.md "wikilink")・[手代木史織](../Page/手代木史織.md "wikilink")（連載中）
  - [巴比倫二世](../Page/巴比倫二世.md "wikilink")（Babel
    II）——[横山光輝](../Page/横山光輝.md "wikilink")
  - [舞-HiME](../Page/舞-HiME.md "wikilink")——[佐藤健悦](../Page/佐藤健悦.md "wikilink")
  - [魔界学園](../Page/魔界学園.md "wikilink")——[菊地秀行](../Page/菊地秀行.md "wikilink")・[細馬信一](../Page/細馬信一.md "wikilink")
  - [七華6/17](../Page/七華6/17.md "wikilink")（Nanaka
    6/17）——[八神健](../Page/八神健.md "wikilink")
  - [超能奇兵](../Page/超能奇兵.md "wikilink")（s-CRY-ed）——[黑田洋介](../Page/黑田洋介.md "wikilink")・[戶田泰成](../Page/戶田泰成.md "wikilink")

## 外部链接

  - [秋田書店官方網站](https://web.archive.org/web/20120217230532/http://www.akitashoten.co.jp/index2.html)

[\*](../Category/週刊少年Champion.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:週刊漫畫雜誌](../Category/週刊漫畫雜誌.md "wikilink")