，是[美商艺电推出的一系列](../Page/美商艺电.md "wikilink")[赛车游戏](../Page/赛车游戏.md "wikilink")，包括[PC](../Page/PC.md "wikilink")、[GBA](../Page/GBA.md "wikilink")、[PSP](../Page/PSP.md "wikilink")、[NDS](../Page/NDS.md "wikilink")、[PSVita](../Page/PSVita.md "wikilink")、[PlayStation](../Page/PlayStation.md "wikilink")、[Xbox](../Page/Xbox.md "wikilink")、[Wii](../Page/Wii.md "wikilink")、[iPhone](../Page/iPhone.md "wikilink")、[iPad](../Page/iPad.md "wikilink")、Android及其它各[游戏机版本](../Page/游戏机.md "wikilink")。玩家可在不同赛道与不同车辆竞赛，甚至在某些版本里可以与[警车进行追逐](../Page/警车.md "wikilink")。是较为经典的一款游戏。

## 系列的起源

《极品飞车》系列最初是由[加拿大](../Page/加拿大.md "wikilink")[温哥华的游戏公司](../Page/温哥华.md "wikilink")所开发。在[美商艺电于](../Page/美商艺电.md "wikilink")1991年将其收购之前，该小组已经制作了和等受欢迎的赛车游戏。

公司被收购之后改名为[EA
Canada](../Page/EA加拿大.md "wikilink")。凭借着在该领域的经验，该公司於1992年开始开发《极品飞车》系列。2005年的大作《全民公敌》已成为大家所知的知名赛车游戏。而后，极品飞车也成為[电视游乐器线上玩家最喜欢玩的游戏](../Page/电视游乐器.md "wikilink")，并且玩家常在线上跟两名以上玩家竞速。

EA Canada和EA
Seattle多年来持续开发和扩展极品飞车系列。2002年，EA将新作《[極速快感：飆風再起2](../Page/極速快感：飆風再起2.md "wikilink")》交由另一家温哥华游戏公司[Black
Box Software接手开发](../Page/EA加拿大.md "wikilink")，并在2004年游戏发表前不久将Black
Box收购，将其改名为[EA Black Box](../Page/EA加拿大.md "wikilink")。此后，EA Black
Box成为《极品飞车》系列的主要开发者。

## 特色

EA曾经向几间名厂取得独佔授权，其车型只会在极品飞车中出现，包括[BMW
M](../Page/BMW_M.md "wikilink")、[雪佛蘭](../Page/雪佛蘭.md "wikilink")、[法拉利以及](../Page/法拉利.md "wikilink")[蓝宝坚尼](../Page/蓝宝坚尼.md "wikilink")，直到2000年代，这些车型开始在其他电玩游戏中出现。

於2016年前，[保时捷在电玩的授权由](../Page/保时捷.md "wikilink")[EA垄断](../Page/EA.md "wikilink")，[微软子公司Turn](../Page/微软.md "wikilink")
10开发的[极限竞速系列需要向EA取得保时捷授权](../Page/极限竞速系列.md "wikilink")。

2011年，EA拒绝在极限竞速4继续给予保时捷授权，Turn
10在过去18个月尽力与EA斡旋，并动用关系请好几个有影响力的业界人士向EA游说，直到[Xbox独佔赛车游戏](../Page/Xbox.md "wikilink")，极限竞速4开始获得独家授权，包括后期作品极限竞速：地平线2，极限竞速6都有[保时捷的车辆套件](../Page/保时捷.md "wikilink")。极品飞车和极限竞速也变成少数拥有保时捷授权的大型赛车游戏。

保时捷与EA的独佔授权於2016年完结，其他电玩游戏亦能名正言顺地取得保时捷授权。

## 游戏年表

本系列总共有22款，加上特别版（[《极品飞车：特别版](../Page/《极品飞车：特别版.md "wikilink")》、《[极品飞车II：特别版](../Page/极品飞车II：特别版.md "wikilink")》、《[极品飞车：全民公敌
黑名单特别版](../Page/极品飞车：全民公敌.md "wikilink")》、《[极品飞车：玩命山道
珍藏版](../Page/极品飞车：玩命山道.md "wikilink")》）与[线上游戏](../Page/线上游戏.md "wikilink")（《[极品飞车：经典城市-Online](../Page/极品飞车：经典城市-Online.md "wikilink")》、《[极品飞车Online](../Page/极品飞车Online.md "wikilink")》）以及[SONY](../Page/SONY.md "wikilink")
[PSP](../Page/PSP.md "wikilink")（《[极品飞车：地下宿敌](../Page/极品飞车：地下宿敌.md "wikilink")》、《[极品飞车
全民公敌5-1-0](../Page/极品飞车：全民公敌.md "wikilink")》、《[极品飞车：玩命山道之决战街头](../Page/极品飞车：玩命山道.md "wikilink")》）特殊版本，总共有30款游戏。

<table>
<thead>
<tr class="header">
<th><p>原文名称</p></th>
<th><p>中文名称</p></th>
<th><p>开发公司</p></th>
<th><p>发行公司</p></th>
<th><p><a href="../Page/游戏引擎.md" title="wikilink">引擎</a></p></th>
<th><p>平台</p></th>
<th><p>发行时间</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>The Need for Speed</p></td>
<td><p><a href="../Page/极品飞车_(1994年游戏).md" title="wikilink">极品飞车</a></p></td>
<td><p><a href="../Page/Pioneer_Productions.md" title="wikilink">Pioneer Productions</a> <a href="../Page/EA_Canada.md" title="wikilink">EA Canada</a></p></td>
<td><p><a href="../Page/EA.md" title="wikilink">EA</a></p></td>
<td></td>
<td><p><a href="../Page/DOS.md" title="wikilink">DOS</a> <a href="../Page/3DO.md" title="wikilink">3DO</a></p></td>
<td><p>1994年</p></td>
<td><p>第1代</p></td>
</tr>
<tr class="even">
<td><p>Need for Speed SE</p></td>
<td><p>极品飞车 特别版</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/Windows.md" title="wikilink">Windows</a></p></td>
<td><p>1996年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed II</p></td>
<td><p><a href="../Page/极品飞车II.md" title="wikilink">极品飞车 II</a></p></td>
<td><p><a href="../Page/EA_Seattle.md" title="wikilink">EA Seattle</a></p></td>
<td></td>
<td><p>1997年</p></td>
<td><p>第2代</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed II SE</p></td>
<td><p>极品飞车 II 特别版</p></td>
<td><p>1997年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: V-Rally</p></td>
<td><p><a href="../Page/V拉力赛车.md" title="wikilink">极品飞车：V拉力赛车</a></p></td>
<td><p><a href="../Page/Eden_Studios.md" title="wikilink">Eden Studios</a></p></td>
<td></td>
<td><p>1997年</p></td>
<td><p>衍生游戏</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed III: Hot Pursuit</p></td>
<td><p><a href="../Page/极品飞车III：闪电追踪.md" title="wikilink">极品飞车 III：热力追缉</a></p></td>
<td><p><a href="../Page/EA_Seattle.md" title="wikilink">EA Seattle</a></p></td>
<td></td>
<td><p>1998年</p></td>
<td><p>第3代</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: V-Rally 2</p></td>
<td><p>极品飞车：V赛事2</p></td>
<td><p><a href="../Page/Eden_Studios.md" title="wikilink">Eden Studios</a></p></td>
<td></td>
<td><p>1999年</p></td>
<td><p>衍生游戏</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: High Stakes（美版） Need for Speed: Road Challenge（南美及欧版）</p></td>
<td><p><a href="../Page/极品飞车：致命追击.md" title="wikilink">极品飞车：致命追击</a></p></td>
<td><p><a href="../Page/EA_Seattle.md" title="wikilink">EA Seattle</a></p></td>
<td></td>
<td><p>1999年</p></td>
<td><p>第4代</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Porsche Unleashed Need for Speed: Porsche 2000（欧版）</p>
<p>Need for Speed: Porsche（德版及美版）</p></td>
<td><p><a href="../Page/极品飞车：保时捷的荣耀.md" title="wikilink">极品飞车：保时捷的荣耀</a></p></td>
<td><p><a href="../Page/EA_Canada.md" title="wikilink">EA Canada</a></p></td>
<td></td>
<td><p>2000年</p></td>
<td><p>第5代</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Motor City Online</p></td>
<td><p><a href="../Page/汽车城_(游戏).md" title="wikilink">极品飞车：经典城市-Online</a></p></td>
<td><p><a href="../Page/EA.md" title="wikilink">EA</a></p></td>
<td></td>
<td><p>2001年</p></td>
<td><p>衍生线上游戏 已关闭</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Hot Pursuit 2</p></td>
<td><p><a href="../Page/极品飞车：闪电追踪2.md" title="wikilink">极品飞车：热力追缉2</a></p></td>
<td><p><a href="../Page/EA_Seattle.md" title="wikilink">EA Seattle</a></p></td>
<td></td>
<td><p>2002年</p></td>
<td><p>第6代</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: Underground</p></td>
<td><p><a href="../Page/极品飞车：地下狂飆.md" title="wikilink">极品飞车：地下狂飆</a></p></td>
<td><p>EA Black Box</p></td>
<td></td>
<td><p>2003年</p></td>
<td><p>第7代</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Underground 2</p></td>
<td><p><a href="../Page/极品飞车：飆风再起2.md" title="wikilink">极品飞车：飆风再起2</a></p></td>
<td></td>
<td><p>2004年</p></td>
<td><p>第8代</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: Underground Rivals</p></td>
<td><p>极品飞车：地下宿敌</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/PSP.md" title="wikilink">PSP</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Most Wanted</p></td>
<td><p><a href="../Page/极品飞车：全民公敌.md" title="wikilink">极品飞车：全民公敌</a></p></td>
<td><p>EA Black Box</p></td>
<td></td>
<td><p>Windows</p></td>
<td><p>2005年</p></td>
<td><p>第9代</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: Black Edition</p></td>
<td><p><a href="../Page/极品飞车：全民公敌.md" title="wikilink">极品飞车：全民公敌 黑名单特别版</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Most Wanted 5-1-0</p></td>
<td><p><a href="../Page/极品飞车：全民公敌.md" title="wikilink">极品飞车：全民公敌 5-1-0</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/PSP.md" title="wikilink">PSP</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: Carbon</p></td>
<td><p><a href="../Page/极品飞车：玩命山道.md" title="wikilink">极品飞车：玩命山道</a></p></td>
<td><p>EA Canada</p>
<p>EA Black Box</p></td>
<td></td>
<td><p>Windows <a href="../Page/OS_X.md" title="wikilink">OS X</a></p>
<p><a href="../Page/XBOX.md" title="wikilink">XBOX</a></p></td>
<td><p>2006年</p></td>
<td><p>第10代</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Carbon Collector's Edition SP</p></td>
<td><p><a href="../Page/极品飞车：玩命山道.md" title="wikilink">极品飞车：玩命山道 珍藏版</a></p></td>
<td><p>2006年</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need For Speed: Own The City</p></td>
<td><p><a href="../Page/极品飞车：玩命山道.md" title="wikilink">极品飞车：玩命山道之决战街头</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/PSP.md" title="wikilink">PSP</a></p></td>
<td><p>2006年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Pro Street</p></td>
<td><p><a href="../Page/极品飞车：职业街头.md" title="wikilink">极品飞车：职业街头</a></p></td>
<td><p>EA Black Box</p></td>
<td></td>
<td><p>Windows</p></td>
<td><p>2007年</p></td>
<td><p>第11代</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: Undercover</p></td>
<td><p><a href="../Page/极品飞车：卧底风云.md" title="wikilink">极品飞车：卧底风云</a></p></td>
<td></td>
<td><p>2008年</p></td>
<td><p>第12代</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Shift</p></td>
<td><p><a href="../Page/极品飞车：进化世代.md" title="wikilink">极品飞车：进化世代</a></p></td>
<td><p><a href="../Page/Slightly_Mad_Studios.md" title="wikilink">Slightly Mad Studios</a></p></td>
<td></td>
<td><p>2009年</p></td>
<td><p>第13代</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: Nitro</p></td>
<td><p>极品飞车：爆衝王</p></td>
<td><p><a href="../Page/Firebrand_Games.md" title="wikilink">Firebrand Games</a></p>
<p><a href="../Page/EA_Montreal.md" title="wikilink">EA Montreal</a></p></td>
<td></td>
<td><p>DS Wii</p></td>
<td><p>2009年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: World</p></td>
<td><p><a href="../Page/极品飞车：世界.md" title="wikilink">极品飞车：世界</a></p></td>
<td><p><a href="../Page/Quicklime_Games.md" title="wikilink">Quicklime Games</a></p>
<p><a href="../Page/EA_Singapore.md" title="wikilink">EA Singapore</a></p></td>
<td></td>
<td><p>Windows</p></td>
<td><p>2009年</p></td>
<td><p>线上游戏 已关闭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: Hot Pursuit</p></td>
<td><p><a href="../Page/极品飞车：超热力追缉.md" title="wikilink">极品飞车：超热力追缉</a></p></td>
<td><p><a href="../Page/Criterion_Games.md" title="wikilink">Criterion Games</a></p></td>
<td></td>
<td><p>2010年</p></td>
<td><p>第14代</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Shift 2: Unleashed</p></td>
<td><p><a href="../Page/变速2：释放.md" title="wikilink">-{zh-hans:变速2：释放; zh-hant:进化世代2：全面解放;}-</a></p></td>
<td><p><a href="../Page/Slightly_Mad_Studios.md" title="wikilink">Slightly Mad Studios</a></p></td>
<td></td>
<td><p>2011年</p></td>
<td><p>第15代</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: The Run</p></td>
<td><p><a href="../Page/极品飞车：亡命天涯.md" title="wikilink">极品飞车：亡命天涯</a></p></td>
<td><p><a href="../Page/EA_Black_Box.md" title="wikilink">EA Black Box</a></p></td>
<td><p><a href="../Page/寒霜引擎.md" title="wikilink">寒霜2</a></p></td>
<td><p>2011年</p></td>
<td><p>第16代</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: Most Wanted</p></td>
<td><p><a href="../Page/極速快感：新全民公敵.md" title="wikilink">极品飞车：新全民公敌</a></p></td>
<td><p><a href="../Page/Criterion_Games.md" title="wikilink">Criterion Games</a></p></td>
<td></td>
<td><p>2012年</p></td>
<td><p>第17代</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed: RIVALS</p></td>
<td><p><a href="../Page/极品飞车：生存竞速.md" title="wikilink">极品飞车：生存竞速</a></p></td>
<td><p><a href="../Page/Ghost_Games.md" title="wikilink">Ghost Games</a></p></td>
<td><p><a href="../Page/寒霜引擎.md" title="wikilink">寒霜3</a></p></td>
<td><p>2013年</p></td>
<td><p>第18代</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: No Limits</p></td>
<td><p><a href="../Page/极品飞车：零极限.md" title="wikilink">极品飞车：零极限</a></p></td>
<td><p>Firemonkeys Studios</p></td>
<td></td>
<td><p><a href="../Page/Android.md" title="wikilink">Android</a> <a href="../Page/iOS.md" title="wikilink">iOS</a></p></td>
<td><p>2015年</p></td>
<td><p>手机游戏</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed</p></td>
<td><p><a href="../Page/极品飞车_(2015年游戏).md" title="wikilink">极品飞车</a></p></td>
<td><p><a href="../Page/Ghost_Games.md" title="wikilink">Ghost Games</a></p></td>
<td><p><a href="../Page/寒霜引擎.md" title="wikilink">寒霜3</a></p></td>
<td><p>Windows</p></td>
<td><p>2015年</p></td>
<td><p>第19代</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Need for Speed: EDGE</p></td>
<td><p><a href="../Page/极品飞车：极限边缘.md" title="wikilink">极品飞车：极限边缘</a></p></td>
<td><p><a href="../Page/EA_Spearhead.md" title="wikilink">EA Spearhead</a></p></td>
<td><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a> <a href="../Page/腾讯.md" title="wikilink">腾讯</a></p></td>
<td><p>2016年</p></td>
<td><p>线上游戏</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Need for Speed Payback</p></td>
<td><p><a href="../Page/极品飞车：复仇.md" title="wikilink">极品飞车：复仇</a></p></td>
<td><p><a href="../Page/Ghost_Games.md" title="wikilink">Ghost Games</a></p></td>
<td><p><a href="../Page/EA.md" title="wikilink">EA</a></p></td>
<td><p><a href="../Page/寒霜引擎.md" title="wikilink">寒霜3</a></p></td>
<td><p><a href="../Page/Windows.md" title="wikilink">Windows</a>、<a href="../Page/PlayStation_4.md" title="wikilink">PlayStation 4</a>、<a href="../Page/Xbox_One.md" title="wikilink">Xbox One</a></p></td>
<td><p>2017年</p></td>
<td><p>第20代</p></td>
</tr>
</tbody>
</table>

## 電影

  - [極速快感 (電影)](../Page/極速快感_\(電影\).md "wikilink")

## 争议

2006年1月24日，均年仅18岁的Alexander Ryzanov与Wang-Piao Dumani
Ross各自驾驶一辆[梅赛德斯-奔驰於](../Page/梅赛德斯-奔驰.md "wikilink")[多伦多街头非法竞速](../Page/多伦多.md "wikilink")，其中一人撞上一台[出租车](../Page/出租车.md "wikilink")，并且肇事逃逸，导致46岁的出租车[司机Tahir](../Page/司机.md "wikilink")
Khan死亡\[1\]。鉴识人员事后於肇事车辆上发现一份《极品飞车》系列游戏之拷贝。有关该系列游戏过程中超速驾驶和逃避法律责任之问题遂开始引起社会关注。\[2\]

## 参考文献

## 外部连结

  - [极品飞车系列官方网站](http://www.needforspeed.com)

  - [以极品飞车為主题的wikia](http://nfs.wikia.com)

  - [《极品飞车：Online》美国官方网站](http://world.needforspeed.com/)

  - [《极品飞车：Online》秘密开发工坊部落格](http://eatw.pixnet.net/blog)

  - [《极品飞车：Online》相关资讯网站](http://nfswo.wordpress.com/)

  - [“极速禁地”台湾赛车游戏网站](http://www.nfspc.tw/)

  -
  -
[Category:竞速游戏](../Category/竞速游戏.md "wikilink")
[极品飞车系列](../Category/极品飞车系列.md "wikilink")
[Category:电子游戏系列](../Category/电子游戏系列.md "wikilink")
[Category:艺电电子游戏系列](../Category/艺电电子游戏系列.md "wikilink")

1.  [Memorial held for cab driver killed in alleged street
    race](http://www.cbc.ca/news/canada/memorial-held-for-cab-driver-killed-in-alleged-street-race-1.584852)
2.  <http://www.cbc.ca/news2/background/crime/street-racing.html>