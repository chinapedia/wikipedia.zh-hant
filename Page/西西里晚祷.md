[Francesco_Hayez_023.jpg](https://zh.wikipedia.org/wiki/File:Francesco_Hayez_023.jpg "fig:Francesco_Hayez_023.jpg")

**西西里晚祷**
()是一場發生在1282年的[西西里島](../Page/西西里島.md "wikilink")，反對[安茹王朝的西西里國王](../Page/安茹.md "wikilink")[查理一世對當地統治的](../Page/查理一世_\(那不勒斯\).md "wikilink")[起義](../Page/起義.md "wikilink")。這場起事的主因，是查理一世1266年在[羅馬教皇支持下攻占了西西里島](../Page/羅馬教皇.md "wikilink")。起事直接導致了[西西里晚禱戰爭的爆發](../Page/西西里晚禱戰爭.md "wikilink")。

## 誘因

這場起事可以說是統治[神聖羅馬帝國的](../Page/神聖羅馬帝國.md "wikilink")[霍亨斯陶芬王室和](../Page/霍亨斯陶芬王朝.md "wikilink")[羅馬教廷之間](../Page/羅馬教廷.md "wikilink")[敘任權之爭的延續](../Page/敘任權之爭.md "wikilink")。霍亨斯陶芬王室的[曼弗雷迪及](../Page/曼弗雷迪_\(西西里國王\).md "wikilink")[康拉丁叔侄被親教皇的法軍擊敗後](../Page/康拉丁.md "wikilink")，教皇[烏爾班四世將整個](../Page/烏爾班四世.md "wikilink")[西西里王國交給法王](../Page/西西里王國.md "wikilink")[路易九世之弟](../Page/路易九世_\(法兰西\).md "wikilink")—[安茹的查理管治](../Page/安茹的查理.md "wikilink")，是為西西里王查理一世。

查理視西西里王國領地為自己侵略[地中海沿岸地区的跳板](../Page/地中海.md "wikilink")，而當中推翻[拜占庭皇帝](../Page/拜占庭帝国.md "wikilink")[米海尔八世也是他的陰謀之一](../Page/米海尔八世.md "wikilink")。但在他麾下的法籍官吏，在西西里的管理不但乏善可陳，官員[搶劫](../Page/搶劫.md "wikilink")、[謀殺甚至](../Page/謀殺.md "wikilink")[強姦當地人的事件時有發生](../Page/強姦.md "wikilink")。

## 背後動機

對於起事背后是否有西西里島居民以外的國家勢力策動，至今一直無法釐清。一部分觀點認為，事件是前西西里国王曼弗雷德的女婿，[阿拉贡王国國王](../Page/阿拉贡王国.md "wikilink")[佩德罗三世和拜占庭皇帝米海尔八世合謀的](../Page/佩德罗三世_\(阿拉贡\).md "wikilink")：前者為了取回岳父的王位；後者便要阻止法國和羅馬教廷的勢力入侵地中海和拜占庭領地。

另一部分人則強調查理一世和其法籍部下不得人心的管治。這個說法在[意大利統一運動過後獲得更多人的認可](../Page/意大利統一.md "wikilink")，可能是由這一時期著名的愛國學者米開爾·阿馬利對此說法的宣揚，有莫大關係。

[Francesco_Hayez_022.jpg](https://zh.wikipedia.org/wiki/File:Francesco_Hayez_022.jpg "fig:Francesco_Hayez_022.jpg")

## 經過

這場起事得名於其爆發的時刻和地點，1282年3月30日，[復活節星期一](../Page/復活節.md "wikilink")[晚禱時分的西西里島](../Page/晚禱.md "wikilink")[巴勒莫聖神大教堂門外](../Page/巴勒莫.md "wikilink")。在此後的六個星期裡，西西里島上數以千計的法籍居民被屠殺。對於這次起事確切的導火索，卻是無法確定，眾說紛紜，但皆包含若干共同元素。

根據[英國歷史學家史蒂芬](../Page/英國.md "wikilink")·倫西曼爵士()的考究，當地人在參加復活節的慶祝活動，一眾法國軍人和官吏加入並開始飲酒。期間一位名叫德鲁埃(Drouet)的法國軍官將一名已婚年輕婦人從人群中拉出，並當眾侵犯了她。婦人的丈夫其後用刀襲擊並殺死了這名軍官。一眾法國軍士企圖為德魯埃復仇，但卻先被人群中的當地人全部殺害。正在此時，四週鐘聲大鳴——巴勒莫全市的教堂皆響起了晚禱的鐘聲。

但根據[文藝復興時期](../Page/文藝復興.md "wikilink")[佛羅倫薩學者](../Page/佛羅倫薩.md "wikilink")[李奧納度·布倫尼在](../Page/李奧納度·布倫尼.md "wikilink")1416年時的說法，巴勒莫居民在城郊慶祝復活節，突然一群法國軍士上前要求檢查人群中是否有人夾帶武器。一部分法軍士兵藉機撫摸人群中部分婦女的胸部。這行徑激起了在場人士的不滿，當地人首先以石塊襲擊法軍，接着紛紛以武器攻擊甚至殺死所見到的法國人。這則新聞傳遍了整個西西里島，導致全島各地皆有發生屠殺法國人的事件，特別是針對法國統治階級的襲擊。

巴勒莫當地的工匠，設立了臨時公社，作為起事期間管理市政的機構。很快[科尔莱奥内等鄰近城鎮紛紛成立類似的公社](../Page/科尔莱奥内.md "wikilink")。在起事期間，這些公社組成了城邦聯盟，暫時管治西西里島。但這些公社都是支持甚至鼓勵對法國人的屠殺，因此這個時期西西里島上可謂是一片腥風血雨。島上只有[墨西拿這個城鎮依然忠於查理一世](../Page/墨西拿.md "wikilink")。

## 結果

阿拉貢國王佩德罗三世乘機入侵了西西里島，阿拉貢軍隊在1282年9月2日確立了在西西里島的統治\[1\]。佩徳羅三世自此兼任西西里國王，是為**西西里王彼得罗一世**。

查理一世維持了對西西里王國[亞平寧半島部分](../Page/亞平寧半島.md "wikilink")，被後世稱為[那不勒斯王國的部分直至其駕崩](../Page/那不勒斯王國.md "wikilink")。而其後裔也一直固守這一部分，並和佩德羅三世的後裔展開了對西西里王國控制權的爭奪，也就是[西西里晚禱戰爭](../Page/西西里晚禱戰爭.md "wikilink")。雙方曾於1302年達成和解，但很快有戰端又起，整個西西里王國的領地直到1442年才再次被阿拉貢人統一。

## 文化影響

  - [朱塞佩·威尔第最受樂界好評的](../Page/朱塞佩·威尔第.md "wikilink")[歌劇作品](../Page/歌劇.md "wikilink")——[《西西里晚祷》](../Page/西西里晚禱_\(歌劇\).md "wikilink")()，便是以這場起事為故事背景。

## 其他提法

  - 1594年，法王[亨利四世在跟](../Page/亨利四世_\(法兰西\).md "wikilink")[西班牙駐法大使进行和谈时威胁说](../Page/西班牙帝国.md "wikilink")，如果西班牙不答应他提出的条件，法军将轻而易举地入侵西班牙王室在意大利的领地，并在提到法軍進軍将如何神速時炫耀地說到自己會“在[米蘭吃早餐](../Page/米蘭.md "wikilink")，在[羅馬吃晚餐](../Page/羅馬.md "wikilink")”。但西班牙大使半諷刺半恐嚇地回應：“現在，若果陛下所言非虛，陛下絕對趕得及在晚飯後準時到達西西里做晚禱。”

<!-- end list -->

  - 18世紀末，[拿破崙在意大利戰爭中和奧地利帝國軍對峙期間](../Page/拿破崙.md "wikilink")，因[威尼斯共和國表示中立但實際上偏向支持](../Page/威尼斯共和國.md "wikilink")[奧地利並暗中鼓動民間團體反法的行動](../Page/奧地利.md "wikilink")，批評威尼斯是企圖復興第二次的西西里晚禱。

<!-- end list -->

  - 在現代意義而言，1931年9月10日一場關於西西里[黑手黨的紛爭](../Page/黑手黨.md "wikilink")，也被稱為**西西里晚禱**。當晚著名幫派分子幸運的卢奇亚诺(Lucky
    Luciano)下令殺害了若干分別效忠於薩瓦托·馬然贊諾(Salvatore Maranzano)和約瑟夫·馬瑟利亞(Joseph
    Masseria)兩派的黑手黨成員，最終導致兩派在[紐約市爆發仇殺](../Page/紐約市.md "wikilink")。

<!-- end list -->

  - 一對在西西里島出生的兄弟，大衛和弗朗西斯·利佛吉亞托曾組建一對名為**西西里晚禱**的樂隊，不過很快淡出樂壇\[2\]。

## 備注

<div class="references-small">

<references />

</div>

## 参考文献

<div class="references-small">

  - Steven Runciman (1958),*The Sicilian Vespers*, ISBN 0-521-43774-1.
  - Leonardo Bruni (1416), *History of the Florentine People*, Harvard,
    2001, ISBN 0-674-00506-6.
  - Henri Bresc, Geneviève Bresc-Bautier (sous la direction de),
    *Palerme, 1070-1492. Mosaïque de peuples, nation rebelle: la
    naissance violente de l'identité sicilienne*, Autrement, 1993.
  - Isabelle Heullant-Donat, Jean-Pierre Delumeau, *L'Italie au Moyen
    Âge*, Paris, Hachette, Carré histoire, 2000.
  - ["Sicilean
    Vespers"](http://www.britannica.com/eb/article-9067619/Sicilian-Vespers).
    In *Encyclopedia Britannica* Online.

</div>

[Category:意大利歷史](../Category/意大利歷史.md "wikilink")

1.  [World
    Statesmen.org](http://www.worldstatesmen.org/Italy_states2.htm)資料
2.  [外部連結一](http://www.allmusic.com/artist/sicilian-vespers-p5424)、[外部連結二](http://cdbaby.com/cd/sicilianvespers)