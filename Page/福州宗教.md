[福州的](../Page/福州.md "wikilink")[宗教呈现多元化趋势](../Page/宗教.md "wikilink")，主要宗教有[基督新教](../Page/基督新教.md "wikilink")、[天主教](../Page/天主教.md "wikilink")、[佛教](../Page/佛教.md "wikilink")、[道教和](../Page/道教.md "wikilink")[伊斯兰教等](../Page/伊斯兰教.md "wikilink")；其中以佛教、道教信徒最多。受传统儒家思想影响，福州人祭祀祖先和地主非常普遍，许多家庭中设有祖先和地主牌位。

## [佛教](../Page/佛教.md "wikilink")

### 福州佛教歷史

佛教在[三国](../Page/三国.md "wikilink")[吴](../Page/吴.md "wikilink")、[晋之际传入福州](../Page/晋.md "wikilink")，[南朝时期大为发展](../Page/南朝.md "wikilink")，唐代福州则出现了[百丈怀海](../Page/百丈怀海.md "wikilink")、[黄蘗希运这样的高僧](../Page/黄蘗希运.md "wikilink")。[宋](../Page/宋.md "wikilink")[元之际](../Page/元.md "wikilink")，福州佛教更盛。福州佛教在明末一度衰落，但清代又复兴，1654年，[隐元禅师东渡](../Page/隐元禅师.md "wikilink")[日本](../Page/日本.md "wikilink")，开创日本佛教[黄檗宗](../Page/黄檗宗.md "wikilink")。福州重要的佛教寺庙包括[鼓山](../Page/鼓山.md "wikilink")[涌泉寺](../Page/涌泉寺.md "wikilink")、西郊[西禅寺](../Page/西禅寺.md "wikilink")、[金鸡山地藏寺](../Page/金鸡山地藏寺.md "wikilink")、瑞峰林阳寺、闽侯[雪峰崇圣寺](../Page/雪峰崇圣寺.md "wikilink")、福清[黄檗山](../Page/黄檗山.md "wikilink")[万福寺等](../Page/万福寺.md "wikilink")。\[1\]

### 重要佛教寺院

  - 市区：鼓山[涌泉寺](../Page/涌泉寺.md "wikilink")、怡山[西禅寺](../Page/西禅寺.md "wikilink")、金鸡山[地藏寺](../Page/地藏寺.md "wikilink")、瑞峰[林阳寺](../Page/林阳寺.md "wikilink")、崇福寺、车余寺、大堂寺、北禅寺、三界寺、海潮寺、荷泽寺、清凉寺、[瑞云寺](../Page/瑞云寺_\(福州\).md "wikilink")、开元寺、法海寺
  - 闽侯：雪峰[崇圣寺](../Page/崇圣寺.md "wikilink")、五仙寺、金仲阁、法云寺、三益寺
  - 福清：黄檗山[万福寺](../Page/万福寺.md "wikilink")、瑞岩寺、吉水寺、资福寺、香灯寺、柳圃寺、福山寺、灵石寺、西澗寺
  - 连江：莲峰寺，白云寺、玉华寺、下庵、上庵、静室、中岩、后岩、观音阁、一真堂、青芝寺、净光寺、保福寺、九龙寺、青峰寺、宝林寺
  - 长乐：龙泉寺、云山寺、文殊寺、银峰寺、屏山寺、西兴寺、竹林寺、晦翁寺、长者寺、清凉寺
  - 罗源：开善寺、龙华寺、瑞云寺、光化寺、凤山寺、金粟寺、圣水寺、碧云寺、仙茅寺，水陆寺、白马寺、枝南寺、万寿寺、翠峰寺
  - 永泰：方广寺、下际寺、重光寺、仙佛寺、姬岩寺、凤凰寺、暗亭寺、高盖山寺
  - 平潭：金峰禅寺、观音堂
  - 闽清：白岩寺

## 道教

### 福州道教歷史

福州的道教历史可追溯到公元前3世纪到公元前1世纪的[秦](../Page/秦.md "wikilink")[汉时代](../Page/汉.md "wikilink")。[汉武帝时](../Page/汉武帝.md "wikilink")[何氏九仙在福州于山修炼](../Page/何氏九仙.md "wikilink")，后从福清[石竹山到](../Page/石竹山.md "wikilink")[仙游](../Page/仙游.md "wikilink")[九鲤湖](../Page/九鲤湖.md "wikilink")。\[2\][东汉末年](../Page/东汉.md "wikilink")[侯官的](../Page/侯官.md "wikilink")[董奉既是](../Page/董奉.md "wikilink")[道人](../Page/道人.md "wikilink")，又是名医。[五代](../Page/五代.md "wikilink")[闽国时期](../Page/闽国.md "wikilink")，[闽王](../Page/闽王.md "wikilink")[王延钧](../Page/王延钧.md "wikilink")、[王昶崇信道教](../Page/王昶.md "wikilink")，福州道教大为兴盛，这一时期，闽王王延钧封[陈靖姑为](../Page/陈靖姑.md "wikilink")“[临水夫人](../Page/临水夫人.md "wikilink")”，赐[宫女](../Page/宫女.md "wikilink")36人为弟子，建[門第于临水](../Page/門第.md "wikilink")，后加封为“崇福临水夫人”，靖姑逝世后，又加封为“顺天圣母顺懿元君、惠忱慈量天尊”，陈靖姑成为[闽东一带最重要的道教女神之一](../Page/闽东.md "wikilink")，而福州则成为[闾山派的中心](../Page/闾山派.md "wikilink")，[闾山派的另外一支則尊奉](../Page/闾山派.md "wikilink")[法主真君為](../Page/法主真君.md "wikilink")[祖師](../Page/祖師.md "wikilink")。[明](../Page/明.md "wikilink")[清以来](../Page/清.md "wikilink")，新建道观渐少，[清](../Page/清.md "wikilink")[乾隆四年](../Page/乾隆.md "wikilink")(1739年)朝廷下令禁止道教之后，道教急剧衰落。但福州的道教通过渗透到民间宗教中去，仍在民间流行。福州市道教協會位于[于山九仙觀](../Page/于山.md "wikilink")。[妈祖](../Page/妈祖.md "wikilink")、[临水夫人是福州最重要的道教神祇](../Page/临水夫人.md "wikilink")，其餘如包括[懿德夫人](../Page/懿德夫人.md "wikilink")、[開閩聖王](../Page/開閩聖王.md "wikilink")、[威武尊王](../Page/威武尊王.md "wikilink")、[白馬尊王](../Page/白馬尊王.md "wikilink")（射鱔尊王）、[五福王爺](../Page/五福王爺.md "wikilink")、[水部尚書](../Page/水部尚書.md "wikilink")、[武夷真君](../Page/武夷君.md "wikilink")、[齐天大圣](../Page/齐天大圣.md "wikilink")、[丹霞大圣](../Page/丹霞大圣.md "wikilink")、[董真人](../Page/董奉.md "wikilink")、[裴真人](../Page/裴真人.md "wikilink")、[马天君](../Page/马天君.md "wikilink")、[照天君等等](../Page/照天君.md "wikilink")，等都是福州知名的神祇。\[3\]

### 重要道觀

  - 市区：福建都城隍庙、于山九仙观、道山观、照天宫、东郊东岳庙、上渡龙沄岭望北台真武殿、肃威路中段裴仙宫、帮洲广慧庵
  - 郊县：福清石竹山九仙阁、永泰高盖山名山室道院、闽侯青圃村洪恩灵济宫位。

## [基督新教](../Page/基督新教.md "wikilink")

[Puqian_church.JPG](https://zh.wikipedia.org/wiki/File:Puqian_church.JPG "fig:Puqian_church.JPG")

### 福州基督新教歷史

基督教新教在1847年由[美国基督教公理会](../Page/美国基督教公理会.md "wikilink")（美部会）和[美以美会传入福州](../Page/美以美会.md "wikilink")，[圣公会亦接踵而至](../Page/圣公会.md "wikilink")。1856年，[卫理公会在东亚地区的首座教堂](../Page/卫理公会.md "wikilink")[真神堂诞生于福州茶亭街](../Page/茶亭真神堂.md "wikilink")。从晚清至民国，新教教会在福州创办了许多新式学校和医院，对福州的现代化进程起到了推动作用。新教还发展了罗马化的福州话文字，即[平话字](../Page/平话字.md "wikilink")，并在教会学校中加以推广。新教在福州的传播过程中也产生了[黄乃裳](../Page/黄乃裳.md "wikilink")、[倪柝声这样颇有影响的教徒](../Page/倪柝声.md "wikilink")。中华人民共和国建立后，各新教教会也受到了[三自运动巨大的冲击](../Page/三自运动.md "wikilink")，有些持异议的教徒遭受到迫害，[家庭教会的活动形式则在后来兴盛起来](../Page/家庭教会.md "wikilink")。\[4\]\[5\]

### 重要教堂

  - 仓山：[中洲堂](../Page/中洲堂.md "wikilink")、[天安堂](../Page/天安堂.md "wikilink")、上渡堂、小岭堂、施埔堂、[马厂街堂](../Page/马厂街堂.md "wikilink")、浦下堂、胪厦堂、城门堂、义序堂、
  - 鼓楼：[花巷堂](../Page/花巷堂.md "wikilink")、[观巷堂](../Page/观巷堂.md "wikilink")、洪山堂、[大根堂](../Page/大根堂.md "wikilink")、[城守前堂](../Page/城守前堂.md "wikilink")
  - 台江：[苍霞堂](../Page/苍霞堂.md "wikilink")、[铺前堂](../Page/铺前堂.md "wikilink")
  - 晋安：鼓山堂、[鼓岭堂](../Page/鼓岭堂.md "wikilink")、东门堂、福兴堂、岭头堂
  - 马尾：马尾堂、闽安堂、和平堂、康庄堂、快安堂
  - 琅岐：琅岐堂、海屿堂
  - 福清：[福华堂](../Page/福华堂.md "wikilink")、[西大堂](../Page/西大堂.md "wikilink")
  - 长乐：城关堂
  - 闽侯：甘蔗堂、南通堂
  - 罗源：城关堂
  - 闽清：城关堂
  - 连江：城关堂
  - 平潭：城关堂

## [天主教](../Page/天主教.md "wikilink")

### 福州天主教歷史

1624年底，[艾儒略来福州传播天主教](../Page/艾儒略.md "wikilink")。1625年，福州历史上第一座天主教堂[三山堂建于宫巷](../Page/三山堂.md "wikilink")。早期，[耶稣会和](../Page/耶稣会.md "wikilink")[多明我会同时在福州地区传教](../Page/多明我会.md "wikilink")。[礼仪之争发生后](../Page/礼仪之争.md "wikilink")，[耶稣会退出福州](../Page/耶稣会.md "wikilink")，仅留[多明我会活动](../Page/多明我会.md "wikilink")。1720年清廷下令禁止天主教，福州的天主教活动转入地下。1842年[南京条约签订后](../Page/南京条约.md "wikilink")，天主教恢复活动，1848年，[西班牙多明我会在福州南门兜澳尾巷建堂](../Page/西班牙.md "wikilink")。大批[福州疍民也在清末皈依天主教](../Page/福州疍民.md "wikilink")，并借此得以上岸定居。1949年以前，教会神职人员以[西班牙人为主](../Page/西班牙人.md "wikilink")。中华人民共和国建立后，推行三自运动，福州的外籍神职人员被驱逐，有异议的本地神职人员遭到逮捕，教会遂分裂成地上教会和地下教会，这种局面维持至今。\[6\]\[7\]根據官方統計數字，1994年天主教友人数突破20万人，其中[长乐市约](../Page/长乐市.md "wikilink")8万人，[福州市区](../Page/福州市.md "wikilink")、福清市和[平潭县约](../Page/平潭县.md "wikilink")3-4万人。\[8\]

### 重要教堂

  - [仓山](../Page/仓山.md "wikilink")：[泛船浦天主堂](../Page/泛船浦天主堂.md "wikilink")、上渡天主堂、湾边天主堂
  - [鼓楼](../Page/鼓楼.md "wikilink")：西门天主堂、洪山桥天主堂
  - [台江](../Page/台江.md "wikilink")：苍霞洲天主堂
  - [马尾](../Page/马尾.md "wikilink")：营盘顶天主堂
  - 福清：城关利桥天主堂、龙田天主堂、江阴天主堂、渔溪天主堂、高山天主堂、江镜天主堂
  - 长乐：城关天主堂、金峰天主堂、鹤上天主堂、古槐龙田天主堂、金峰潭头天主堂
  - [连江](../Page/连江.md "wikilink")：城关天主堂
  - [罗源](../Page/罗源.md "wikilink")：凤山岐阳天主堂
  - [闽侯](../Page/闽侯.md "wikilink")：南屿南江天主堂、南屿南溪天主堂、上街百禄村天主堂、上街晓星天主堂、竹岐洋里天主堂、白沙天主堂
  - [永泰](../Page/永泰.md "wikilink")：城关天主堂、蒿口镇天主堂、塘前乡天主堂
  - [闽清](../Page/闽清.md "wikilink")：城关天主堂、坂东天主堂

## [伊斯兰教](../Page/伊斯兰教.md "wikilink")

### 福州伊斯蘭教歷史

历史上，福州的伊斯兰教活动一直很少。628年，福州出现了第一座，也是唯一一座[清真寺](../Page/清真寺.md "wikilink")。此后，福州伊斯兰教长期衰落，直到1474年即明朝成化十年，[市舶司由泉州迁至福州](../Page/市舶司.md "wikilink")，[穆斯林来福州者渐多](../Page/穆斯林.md "wikilink")，伊斯兰教才在福州稍微复苏，福州清真寺亦重建于此时。清代，福州伊斯兰教是靠外省来闽任职的穆斯林官员捐资修寺、修墓、办经堂教育来维持的。[中华人民共和国时期](../Page/中华人民共和国.md "wikilink")，福州穆斯林多是来自外省。\[9\]

### 清真寺

  - [福州清真寺](../Page/福州清真寺.md "wikilink")（八一七北路）

## 備註

## 参考文献

[\*](../Category/福州宗教.md "wikilink")

1.  福州市志，方志出版社

2.  何乔远，闽书

3.
4.  Dennis McCallum，Watchman Nee and the House Church Movement in China

5.
6.  [天主教徒和新教徒纷纷涌向玫瑰山庄圣母朝圣地朝圣](http://www.asianews.it/index.php?l=zh&art=835&size=)，亚洲新闻

7.  [A call for Catholic unity in
    China](http://www.theworld.org/2009/07/30/a-call-for-catholic-unity-in-china/)，PRI's
    the World

8.
9.