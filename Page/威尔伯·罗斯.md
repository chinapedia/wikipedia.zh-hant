**小威尔伯·路易斯·罗斯**（；），[美国](../Page/美国.md "wikilink")[投资家](../Page/投资家.md "wikilink")，擅长[重组不同](../Page/重组.md "wikilink")[行业的](../Page/行业.md "wikilink")[破产企业](../Page/破产企业.md "wikilink")，诸如，[钢铁](../Page/钢铁.md "wikilink")、[煤矿](../Page/煤矿.md "wikilink")、[电信](../Page/电信.md "wikilink")、跨国[投资公司和](../Page/投资公司.md "wikilink")[纺织等行业](../Page/纺织.md "wikilink")，尤其精通[杠杆收购](../Page/杠杆收购.md "wikilink")。他目前是[耶鲁大学](../Page/耶鲁大学.md "wikilink")[管理学院](../Page/管理学院.md "wikilink")[顾问](../Page/顾问.md "wikilink")，也是“[转机管理协会](../Page/转机管理协会.md "wikilink")”和“[美国破产协会](../Page/美国破产协会.md "wikilink")”的会员。经[克林顿总统任命](../Page/克林顿总统.md "wikilink")，Ross曾在“[美俄投资基金](../Page/美俄投资基金.md "wikilink")”的[董事会供职](../Page/董事会.md "wikilink")。之后，又成为[纽约市](../Page/纽约市.md "wikilink")[市长](../Page/市长.md "wikilink")[魯迪·朱利安尼的](../Page/魯迪·朱利安尼.md "wikilink")[私有化顾问](../Page/私有化.md "wikilink")。2005年，罗斯首次被《[福布斯杂志](../Page/福布斯杂志.md "wikilink")》选入[世界富豪榜](../Page/世界富豪榜.md "wikilink")。

2016年11月30日，[唐納·川普提名罗斯出任](../Page/唐納·川普.md "wikilink")[美国商务部长](../Page/美国商务部长.md "wikilink")\[1\]。2017年2月27日，[美国参议院以](../Page/美国参议院.md "wikilink")72票支持和27票反對，通過提名。\[2\]

## 学生时代

Ross出生在[新泽西郊外的一个](../Page/新泽西.md "wikilink")[富裕家庭](../Page/富裕家庭.md "wikilink")。父亲是[律师](../Page/律师.md "wikilink")，母亲是[教师](../Page/教师.md "wikilink")。年轻时，Ross
每天花两个小时从[新泽西去](../Page/新泽西.md "wikilink")[曼哈顿的Catholic](../Page/曼哈顿.md "wikilink")
Xavier[高中上课](../Page/高中.md "wikilink")。他热衷于[径赛](../Page/径赛.md "wikilink")，同时也是[步枪小组的组长](../Page/步枪.md "wikilink")。他毕业于[耶鲁学院](../Page/耶鲁学院.md "wikilink")（同样也是他父亲的[母校](../Page/母校.md "wikilink")），获得BA[学位](../Page/学位.md "wikilink")。在[耶鲁](../Page/耶鲁.md "wikilink")，他编辑一份[文学杂志](../Page/文学.md "wikilink")，并且在[电台工作](../Page/电台.md "wikilink")。刚开始，他希望能够成为一名[作家](../Page/作家.md "wikilink")，因而参加了一个[小说学习班](../Page/小说.md "wikilink")。但当他面对每天要写500字的任务时，觉得自己“[江郎才尽](../Page/江郎才尽.md "wikilink")”，遂放弃了作家梦。系里的一名[指导老师帮助他获得了第一份工作](../Page/指导老师.md "wikilink")——在[华尔街上的暑期工作](../Page/华尔街.md "wikilink")。之后，Ross
在[哈佛商学院获得了](../Page/哈佛商学院.md "wikilink")[MBA学位](../Page/MBA.md "wikilink")。

## 早期经历

70年代中期，Ross
成为了美国最出名的破产顾问，但到了1997年，他希望尝试不同的业务。他用3年时间在Rothschild之下创办了一家[私募股权公司](../Page/私募股权公司.md "wikilink")。但是，由于不能够投资那些自己已经开展咨询业务的公司，全美有1/3的破产企业与其投资业务无缘。

## 收购故事

2000年，Ross 以4亿4千万美元在[纽约成立了WL](../Page/纽约.md "wikilink") Ross & Co.
LLP。他找到了另外4人与其一同开展业务：David H Storper，主管销售；David L.
Wax，长期测试专家；Stephen J. Toy，亚洲专家；Pamela K
Wilson，[JP摩根的老雇员](../Page/JP摩根.md "wikilink")。

2004年，Ross将Burlington Industries 和 Cone
Mills合并，以组建[国际纺织集团](../Page/国际纺织集团.md "wikilink")（ITG）。ITG有5个不同的业务：Cone
Denim, Burlington Apparel Fabrics, Home Furnishings, Carlisle Finishing
and Nano-Tex
LLC。从[美国银行获得了一笔](../Page/美国银行.md "wikilink")5年期，数额为1亿5千万美元的[信贷](../Page/信贷.md "wikilink")[透支](../Page/透支.md "wikilink")。其他借款人包括[GE
Capital
Corporation和](../Page/:en:GE_Capital.md "wikilink")[花旗银行](../Page/花旗银行.md "wikilink")。

[2004年5月](../Page/2004年5月.md "wikilink")，Ross购买了Horizon Natural
Resources不少非[工会资产](../Page/工会.md "wikilink")，进而建立[国际煤矿集团](../Page/国际煤矿集团.md "wikilink")。之后，取消了Horizon的工会[合同](../Page/合同.md "wikilink")，包括原职工的[养老金协议](../Page/养老金.md "wikilink")。[美国矿工协会](../Page/美国矿工协会.md "wikilink")（UMWA）因而强烈抗议其利用[破产法案的行为](../Page/破产.md "wikilink")，认为其不顾工会诉求、更不考虑职工的健康安全及养老金问题。

2005年，Ross
协助[美国当地的](../Page/美国.md "wikilink")[钢铁工人协会](../Page/钢铁工人协会.md "wikilink")，帮助他们“拯救”[宾夕法尼亚州的](../Page/宾夕法尼亚州.md "wikilink")[钢铁工业](../Page/钢铁.md "wikilink")。同年4月，Ross以45亿[美元的价格](../Page/美元.md "wikilink")，将总部设在[怀俄明Richfield的](../Page/怀俄明.md "wikilink")[国际钢铁公司卖给了](../Page/国际钢铁公司.md "wikilink")[米塔尔钢铁公司](../Page/米塔尔钢铁公司.md "wikilink")（印度），但是Ross并没有出售自己在米塔尔钢铁公司的任何[股份](../Page/股份.md "wikilink")。

[2005年4月](../Page/2005年4月.md "wikilink")，在Oxford Automotive
进入[破产](../Page/破产.md "wikilink")[清算阶段之时](../Page/清算.md "wikilink")，Ross及其他[投资人为其提供](../Page/投资人.md "wikilink")1亿美元的[资本金支柱](../Page/资本金.md "wikilink")，以换取25%的股份。Oxford
Automotive是一家美方投资的[欧洲公司](../Page/欧洲.md "wikilink")。

2005年11月28日，Ross、Franklin Mutual Advisers LLC和Lear集团宣布收购 [Collins &
Aikman价值](../Page/:en:Collins_&_Aikman.md "wikilink")6亿美元的欧洲部分。此项[收购涉及到位于](../Page/收购.md "wikilink")[奥地利](../Page/奥地利.md "wikilink")、[比利时](../Page/比利时.md "wikilink")、[捷克](../Page/捷克.md "wikilink")、[德国](../Page/德国.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[斯洛伐克](../Page/斯洛伐克.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[瑞典和](../Page/瑞典.md "wikilink")[英国等国的工厂](../Page/英国.md "wikilink")。这些工厂为[福特](../Page/福特.md "wikilink")、[通用汽车](../Page/通用汽车.md "wikilink")、[Daimler
Chrysler](../Page/戴姆勒.md "wikilink")、[保时捷](../Page/保时捷.md "wikilink")、[萨博](../Page/萨博.md "wikilink")、[大众汽车](../Page/大众汽车.md "wikilink")、[沃尔沃和其他设备](../Page/沃尔沃.md "wikilink")[制造商提供](../Page/制造商.md "wikilink")[配件](../Page/配件.md "wikilink")。

2005年12月2日，Zapata集团的主席Avie Glazer宣布以5120万美元的价格，将Safety Components
International的77%的[股权出售给Ross](../Page/股权.md "wikilink")。Safety
Components
International主要从事于[安全气囊和窗帘的制造](../Page/安全气囊.md "wikilink")，总部在美国加利福尼亚的Greenville，其工厂分布在[欧洲](../Page/欧洲.md "wikilink")、[北美](../Page/北美.md "wikilink")、[中国和](../Page/中国.md "wikilink")[南非](../Page/南非.md "wikilink")。

2006年1月2日Sago煤矿发生矿难，12名[矿工遇难](../Page/矿工.md "wikilink")。“[Sago矿难](../Page/Sago矿难.md "wikilink")”之后，。

2006年2月，Ross宣布希望以Oxford
Automotive的25%的股权，作为反向收购的一部分，用以交换Wagon集团的股权，并且希望能够成为合并后新公司的[非执行董事](../Page/非执行董事.md "wikilink")。当月Wagon股价上涨，超过2英镑。

2006年4月份，Wagon通过向Oxford
Automotive公司的股东增发新股的方式，募集[资金](../Page/资金.md "wikilink")，进而再收购Oxford
Automotive。收购被定义为反向收购。投资成本最终为1亿6620万英镑。原Oxford
Automotive的3位董事成为集团的非执行董事，即Wilbur
Ross、Jens Hohnel、Rolf Zimmermann。Wagon的股票暴涨至2.6英镑每股。

2006年3月23日，面对美国劳动部、西弗吉尼亚州和国会的调查，Sage煤矿副总裁Sam
Kitts承认煤矿属于“狼奔矿业公司”，而“狼奔矿业公司”则属于“猎人山矿业公司”，而“猎人山”则属于“国际煤矿集团（ICG）”。ICG正是由Ross于2004年组建并所有。

2006年10月16日，作为[国际汽车配件集团](../Page/国际汽车配件集团.md "wikilink")（International
Automotive Components
Group）的[主席](../Page/主席.md "wikilink")，Ross宣布完成了对Lear
集团在欧洲的“内饰系统”部的收购工作。收购不涉及借款，而是采用换出国际汽车配件集团34%股权的方式完成的。收购使国际汽车配件集团获得了分布在欧洲9国的20家工厂，以及12亿美元的年销售额。

2007年上半年，Lear
集团将“内饰系统”部转移到了国际汽车配件集团，此项交易涉及26家制造商和2家在[中国的](../Page/中国.md "wikilink")[合资厂](../Page/合资厂.md "wikilink")。之后，Lear
出资2700万美元收购了[国际汽车配件集团](../Page/国际汽车配件集团.md "wikilink")25%的[股权](../Page/股权.md "wikilink")，并且保证还将追加7%的收购额。

## 家庭

第二任[妻子是前](../Page/妻子.md "wikilink")[纽约州](../Page/纽约州.md "wikilink")[副州长](../Page/副州长.md "wikilink")，[Betsy
McCaughey
Ross](../Page/:en:Betsy_McCaughey.md "wikilink")。[1998年1月他为第二任妻子McCaughey](../Page/1998年1月.md "wikilink")
Ross的竞选提供225万美元的资金，不过McCaughey却在[竞选当中惨败](../Page/竞选.md "wikilink")。2000年二人离婚，前妻宣称Wilbur
Ross以赞助其竞选为条件，强迫与其[结婚](../Page/结婚.md "wikilink")。2004年Wilbur Ross与Hilary
Geary结婚。

## 資料來源

[Category:美国企业家](../Category/美国企业家.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:哈佛商學院校友](../Category/哈佛商學院校友.md "wikilink")
[Category:前億萬富豪](../Category/前億萬富豪.md "wikilink")

1.
2.