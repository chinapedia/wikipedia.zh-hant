[-{湖里}-区](../Page/湖里区.md "wikilink"){{.w}}[集美区](../Page/集美区.md "wikilink"){{.w}}[同安区](../Page/同安区.md "wikilink"){{.w}}[翔安区](../Page/翔安区.md "wikilink")
}}

|group3style = text-align: center; |group3 =
[地级市](../Page/地级市.md "wikilink") |list3 =
[台江区](../Page/台江区.md "wikilink"){{.w}}[仓山区](../Page/仓山区.md "wikilink"){{.w}}[马尾区](../Page/马尾区.md "wikilink"){{.w}}[晋安区](../Page/晋安区.md "wikilink"){{.w}}[长乐区](../Page/长乐区.md "wikilink")<sup>2</sup>{{.w}}[福清市](../Page/福清市.md "wikilink"){{.w}}[闽侯县](../Page/闽侯县.md "wikilink"){{.w}}[连江县](../Page/连江县.md "wikilink")<sup>2</sup>{{.w}}[罗源县](../Page/罗源县.md "wikilink"){{.w}}[闽清县](../Page/闽清县.md "wikilink"){{.w}}[永泰县](../Page/永泰县.md "wikilink"){{.w}}[平潭县](../Page/平潭县.md "wikilink")<sup>1</sup>

|group4 = [莆田市](../Page/莆田市.md "wikilink") |list4 =
[城厢区](../Page/城厢区.md "wikilink"){{.w}}[涵江区](../Page/涵江区.md "wikilink"){{.w}}[荔城区](../Page/荔城区.md "wikilink"){{.w}}[秀屿区](../Page/秀屿区.md "wikilink")<sup>2</sup>{{.w}}[-{zh-hans:仙游;
zh-hant:仙遊}-县](../Page/仙游县.md "wikilink")

|group5 = [三明市](../Page/三明市.md "wikilink") |list5 =
[梅列区](../Page/梅列区.md "wikilink"){{.w}}[三元区](../Page/三元区.md "wikilink"){{.w}}[永安市](../Page/永安市.md "wikilink"){{.w}}[明溪县](../Page/明溪县.md "wikilink"){{.w}}[清流县](../Page/清流县.md "wikilink"){{.w}}[宁化县](../Page/宁化县.md "wikilink"){{.w}}[大田县](../Page/大田县.md "wikilink"){{.w}}[尤溪县](../Page/尤溪县.md "wikilink"){{.w}}[沙县](../Page/沙县.md "wikilink"){{.w}}[将乐县](../Page/将乐县.md "wikilink"){{.w}}[泰宁县](../Page/泰宁县.md "wikilink"){{.w}}[建宁县](../Page/建宁县.md "wikilink")

|group6 = [泉州市](../Page/泉州市.md "wikilink") |list6 =
[丰泽区](../Page/丰泽区.md "wikilink"){{.w}}[鲤城区](../Page/鲤城区.md "wikilink"){{.w}}[洛江区](../Page/洛江区.md "wikilink"){{.w}}[泉港区](../Page/泉港区.md "wikilink"){{.w}}[石狮市](../Page/石狮市.md "wikilink"){{.w}}[晋江市](../Page/晋江市.md "wikilink"){{.w}}[南安市](../Page/南安市.md "wikilink"){{.w}}[惠安县](../Page/惠安县.md "wikilink"){{.w}}[安溪县](../Page/安溪县.md "wikilink"){{.w}}[永春县](../Page/永春县.md "wikilink"){{.w}}[德化县](../Page/德化县.md "wikilink"){{.w}}*[金门县](../Page/金门县_\(中华人民共和国\).md "wikilink")*<sup>2</sup>

|group7 = [漳州市](../Page/漳州市.md "wikilink") |list7 =
[芗城区](../Page/芗城区.md "wikilink"){{.w}}[龙文区](../Page/龙文区.md "wikilink"){{.w}}[龙海市](../Page/龙海市.md "wikilink")<sup>2</sup>{{.w}}[云霄县](../Page/云霄县.md "wikilink"){{.w}}[漳浦县](../Page/漳浦县.md "wikilink"){{.w}}[诏安县](../Page/诏安县.md "wikilink"){{.w}}[长泰县](../Page/长泰县.md "wikilink"){{.w}}[东山县](../Page/东山县.md "wikilink"){{.w}}[南靖县](../Page/南靖县.md "wikilink"){{.w}}[平和县](../Page/平和县.md "wikilink"){{.w}}[华安县](../Page/华安县.md "wikilink")

|group8 = [南平市](../Page/南平市.md "wikilink") |list8 =
[建阳区](../Page/建阳区.md "wikilink"){{.w}}[延平区](../Page/延平区.md "wikilink"){{.w}}[邵武市](../Page/邵武市.md "wikilink"){{.w}}[武夷山市](../Page/武夷山市.md "wikilink"){{.w}}[建瓯市](../Page/建瓯市.md "wikilink"){{.w}}[顺昌县](../Page/顺昌县.md "wikilink"){{.w}}[浦城县](../Page/浦城县.md "wikilink"){{.w}}[光泽县](../Page/光泽县.md "wikilink"){{.w}}[松溪县](../Page/松溪县.md "wikilink"){{.w}}[政和县](../Page/政和县.md "wikilink")

|group9 = [龙岩市](../Page/龙岩市.md "wikilink") |list9 =
[新罗区](../Page/新罗区.md "wikilink"){{.w}}[永定区](../Page/永定区_\(龙岩市\).md "wikilink"){{.w}}[漳平市](../Page/漳平市.md "wikilink"){{.w}}[长汀县](../Page/长汀县.md "wikilink"){{.w}}[上杭县](../Page/上杭县.md "wikilink"){{.w}}[武平县](../Page/武平县.md "wikilink"){{.w}}[连城县](../Page/连城县.md "wikilink")

|group10 = [宁德市](../Page/宁德市.md "wikilink") |list10 =
[蕉城区](../Page/蕉城区.md "wikilink"){{.w}}[福安市](../Page/福安市.md "wikilink"){{.w}}[福鼎市](../Page/福鼎市.md "wikilink"){{.w}}[霞浦县](../Page/霞浦县.md "wikilink"){{.w}}[古田县](../Page/古田县.md "wikilink"){{.w}}[屏南县](../Page/屏南县.md "wikilink"){{.w}}[寿宁县](../Page/寿宁县.md "wikilink"){{.w}}[周宁县](../Page/周宁县.md "wikilink"){{.w}}[柘荣县](../Page/柘荣县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
注1：[平潭县实际上由正地厅级的](../Page/平潭县.md "wikilink")[福建省平潭综合实验区管辖](../Page/平潭综合实验区.md "wikilink")。
注2：[泉州市](../Page/泉州市.md "wikilink")[金门县](../Page/金门县_\(中华人民共和国\).md "wikilink")、
[福州市](../Page/福州市.md "wikilink")[连江县的](../Page/连江县.md "wikilink")[马祖乡](../Page/马祖乡.md "wikilink")、[长乐区的](../Page/长乐区.md "wikilink")[东犬岛](../Page/东犬岛.md "wikilink")、[西犬岛](../Page/西犬岛.md "wikilink")、[莆田市](../Page/莆田市.md "wikilink")[秀屿区](../Page/秀屿区.md "wikilink")[湄洲镇的](../Page/湄洲镇.md "wikilink")[烏坵嶼](../Page/烏坵鄉.md "wikilink")、[漳州市](../Page/漳州市.md "wikilink")[龙海市的](../Page/龙海市.md "wikilink")[东碇岛未实际统治](../Page/东碇岛.md "wikilink")，参见[臺海現狀](../Page/臺海現狀.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[福建省乡级以上行政区列表](../Page/福建省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[闽](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[福建行政区划模板](../Category/福建行政区划模板.md "wikilink")
[\*](../Category/福建行政区划.md "wikilink")