**李琳琳**（，），原名**李月玲**，前[香港演員](../Page/香港.md "wikilink")，籍贯[天津](../Page/天津.md "wikilink")，1960年代加入[電懋](../Page/電懋.md "wikilink")（後稱[國泰機構（香港）有限公司](../Page/國泰機構（香港）有限公司.md "wikilink")）成為演員，1970年代加入[無綫電視](../Page/無綫電視.md "wikilink")，主演多套經典電視劇，代表作是與[石修合演的](../Page/石修.md "wikilink")《[心有千千結](../Page/心有千千結.md "wikilink")》。李琳琳咬字清楚，外型爽朗，最合適演女俠，女強人等角色如1981年《[楊門女將](../Page/楊門女將_\(無綫電視劇\).md "wikilink")》的楊八妹。

李琳琳的夫婿是前邵氏武打巨星[姜大衛](../Page/姜大衛.md "wikilink")，1990年代初隨夫舉家而居[加拿大](../Page/加拿大.md "wikilink")[溫哥華](../Page/溫哥華.md "wikilink")，因姜大衛間中會回香港拍劇，因此間中隨夫回香港。最近一次電視露面是2006年與丈夫及孻子John
John接受《[娛樂直播](../Page/娛樂直播.md "wikilink")》訪問。2007年暑假舉家回流香港\[1\]。

2011年中，傳出李琳琳患大腸癌並擴散至淋巴\[2\]。

## 演出作品

### 電視劇

#### [無綫電視](../Page/無綫電視.md "wikilink")

  - [相見好](../Page/相見好.md "wikilink")（1975）
  - [心有千千結](../Page/心有千千結.md "wikilink")（1976）飾 江雨薇
  - [相見好II](../Page/相見好II.md "wikilink")（1976）
  - [民間傳奇之女秀才](../Page/民間傳奇.md "wikilink")（1976）飾　聞蜚娥／聞俊卿
  - [瑪麗關77](../Page/瑪麗關77.md "wikilink")（1977）
  - [民間傳奇之梁山伯與祝英台](../Page/民間傳奇.md "wikilink")（1977） 飾 祝英台
  - [民間傳奇之江山美人](../Page/民間傳奇.md "wikilink")（1977） 飾 李鳳姐
  - [民間傳奇之賭場煞星](../Page/民間傳奇.md "wikilink")（1977） 飾 寶姑娘
  - [小李飛刀](../Page/小李飛刀_\(1978年電視劇\).md "wikilink")（1978） 飾 林詩音
  - [女人三十](../Page/女人三十.md "wikilink")（1979）
  - [鹽梟](../Page/鹽梟.md "wikilink")（1980）
  - [仁者無敵](../Page/仁者無敵.md "wikilink")（1980）飾 李薇
  - [輪流傳](../Page/輪流傳.md "wikilink")（1980） 飾 翟粵生
  - [龍虎雙霸天](../Page/龍虎雙霸天.md "wikilink")（1981）
  - [無雙譜](../Page/無雙譜_\(1981年電視劇\).md "wikilink")（1981） 飾 段鳳三／段素四
  - [楊門女將](../Page/楊門女將_\(無綫電視劇\).md "wikilink")（1981） 飾 楊八妹
  - [天龍八部](../Page/天龍八部_\(1982年電視劇\).md "wikilink")（1982） 飾 王夫人
  - [十三太保](../Page/十三太保_\(電視劇\).md "wikilink")（1982） 飾 沈夢仙
  - [十三妹](../Page/十三妹.md "wikilink")（1983） 飾 九難師太
  - [故鄉情](../Page/故鄉情.md "wikilink")（1984）
  - [信是有緣](../Page/信是有緣.md "wikilink")（1984）
  - [武林聖火令](../Page/武林聖火令.md "wikilink")（1984） 飾 六絕師太
  - [楊家將](../Page/楊家將_\(無綫電視劇\).md "wikilink")（1985） 飾 蕭太后
  - [聖劍天嬌](../Page/聖劍天嬌.md "wikilink")（1986） 飾 石魔
  - [狄青](../Page/狄青.md "wikilink")（1986） 飾 楊夫人
  - [薛剛反唐](../Page/薛剛反唐.md "wikilink")（1986）飾 樊梨花
  - [我本善良](../Page/我本善良_\(無綫電視劇\).md "wikilink")（1990） 飾 簡慧心

#### 湖南衛視

  - [追魚傳奇](../Page/追魚傳奇.md "wikilink")（2013） 飾
    [觀音](../Page/觀音.md "wikilink")

### 節目主持（[無綫電視](../Page/無綫電視.md "wikilink")）

  - [動物奇趣錄](../Page/動物奇趣錄.md "wikilink") （1981）
  - [婦女新姿](../Page/婦女新姿.md "wikilink") （1986-1992）
  - [都市閒情](../Page/都市閒情.md "wikilink") （1992）

## 参考文獻

<div class="references-small">

<references />

</div>

[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Lam](../Category/李姓.md "wikilink")

1.
2.  [1](http://bka.mpweekly.com/focus/local/%E6%9D%8E%E7%90%B3%E7%90%B3%E8%AA%8D6%E5%B9%B4%E5%89%8D%E6%82%A3%E5%A4%A7%E8%85%B8%E7%99%8C-%E6%A8%82%E8%A7%80%E9%9D%A2%E5%B0%8D-%E6%8A%97%E7%97%85%E6%88%90%E5%8A%9F?cat=2)
    李琳琳認6年前患大腸癌 樂觀面對 抗病成功