**惡臭合唱團**（），於1991年成立，[美國著名](../Page/美國.md "wikilink")[朋克](../Page/朋克.md "wikilink")[樂隊](../Page/樂隊.md "wikilink")。它是由[提姆·阿姆斯壯](../Page/提姆·阿姆斯壯.md "wikilink")（Tim
Armstrong）與[麥特·費曼](../Page/麥特·費曼.md "wikilink")（Matt
Freeman）於[加州](../Page/加州.md "wikilink")[阿拉米達縣](../Page/阿拉米達縣.md "wikilink")[阿爾巴尼](../Page/阿爾巴尼.md "wikilink")（Albany）創立。目前的成員有提姆·阿姆斯壯（Tim
Armstrong，[吉他手與](../Page/吉他.md "wikilink")[歌手](../Page/歌手.md "wikilink")）、[拉斯·弗瑞德里克森](../Page/拉斯·弗瑞德里克森.md "wikilink")（Lars
Frederiksen，吉他手與歌手）、麥特·費曼（Matt
Freeman，[貝斯手與歌手](../Page/電貝斯.md "wikilink")）與[布蘭登·斯坦科特](../Page/布蘭登·斯坦科特.md "wikilink")（Branden
Steineckert，[鼓手](../Page/爵士鼓.md "wikilink")）。

## 注釋

[Category:美國搖滾樂團](../Category/美國搖滾樂團.md "wikilink")
[Category:龐克搖滾樂團](../Category/龐克搖滾樂團.md "wikilink")