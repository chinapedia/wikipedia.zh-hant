**传播史**是研究人类传播历史的[学科](../Page/学科.md "wikilink")，一般而言，传播史的起始时间是从[人类诞生的那一天算起](../Page/人类.md "wikilink")。傳播史是[文化史的一支](../Page/文化史.md "wikilink")，其研究的主題，包括傳播媒介的生產者、生產過程、及媒介本身內容，以及其與歷史文化思想等背景的關連等。

傳播的方式由口述傳播經過多個階段到目前的網絡傳播時代。

  - 口述傳播（Oral communication）
  - 文字傳播（Literal communication）
  - 印刷傳播（Press communication）
      - 1044年，[畢昇發明](../Page/畢昇.md "wikilink")[活字印刷術](../Page/活字印刷術.md "wikilink")。
      - 1450年，[日耳曼人](../Page/日耳曼.md "wikilink")[古騰堡發明](../Page/古騰堡.md "wikilink")[金屬活字印刷術](../Page/金屬活字印刷術.md "wikilink")。
  - 電子傳播（Electronic communication）
      - 1837年，[美國人](../Page/美國.md "wikilink")[-{zh-hans:莫尔斯;
        zh-hant:摩斯;}-發明](../Page/摩斯.md "wikilink")[電報機](../Page/電報.md "wikilink")。
      - 1857年，橫跨[大西洋海底電報](../Page/大西洋.md "wikilink")[電纜完成](../Page/電纜.md "wikilink")。
      - 1875年，[貝爾發明史上第一支](../Page/貝爾.md "wikilink")[電話](../Page/電話.md "wikilink")。
      - 1895年，[俄國人](../Page/俄國.md "wikilink")[波波夫和](../Page/波波夫.md "wikilink")[義大利人](../Page/義大利.md "wikilink")[馬可尼同時成功研製了](../Page/馬可尼.md "wikilink")[無線電接收機](../Page/無線電.md "wikilink")。
      - 1895年，[法國的](../Page/法國.md "wikilink")[-{zh-hans:卢米埃尔;
        zh-hant:盧米埃;}-兄弟](../Page/卢米埃尔.md "wikilink")，在[巴黎首映第一部](../Page/巴黎.md "wikilink")[電影](../Page/電影.md "wikilink")。
      - 1912年，[鐵達尼號沈船事件中](../Page/鐵達尼號.md "wikilink")，無線電救了700多條人命。
      - 1920年代，[收音機問世](../Page/收音機.md "wikilink")。
      - 1920年代，[英國人](../Page/英國.md "wikilink")[貝爾德成功進行了](../Page/貝爾德.md "wikilink")[電視畫面的傳送](../Page/電視.md "wikilink")，被譽為電視發明人。
      - [二次大戰爆發](../Page/二次大戰.md "wikilink")，[電視事業中斷](../Page/電視.md "wikilink")，戰火突顯[廣播發送成本低](../Page/廣播.md "wikilink")、接收容易的特性，聽眾再次增加。
      - 1962年，[美國發射第一顆](../Page/美國.md "wikilink")[人造衛星](../Page/人造衛星.md "wikilink")，開啟電視衛星傳送的時代。
  - 網絡傳播（Internet communication）
      - 1955年，美國為了大戰的需要，發行了第一部軍用[電子計算機](../Page/電子計算機.md "wikilink")。
      - 1969年，[美軍建立阿帕網](../Page/美軍.md "wikilink")（[ARPANET](../Page/ARPANET.md "wikilink")），目的是預防遭受攻擊時，通信中斷。
      - 1983年，美國國防部將阿帕網分為軍網和民網，漸漸擴大為今天的[網際網路](../Page/網際網路.md "wikilink")。
      - 1993年，美國宣布興建[信息高速通路計畫](../Page/信息高速通路.md "wikilink")，整合電腦、電話、電視媒體。

[category:各種主題的歷史](../Page/category:各種主題的歷史.md "wikilink")

[Category:传播学](../Category/传播学.md "wikilink")
[Category:文化史](../Category/文化史.md "wikilink")