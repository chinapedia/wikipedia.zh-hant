**谷關溫泉**位於[臺灣](../Page/臺灣.md "wikilink")[台中市](../Page/台中市.md "wikilink")[和平區博愛里的](../Page/和平區_\(臺中市\).md "wikilink")[大甲溪河谷](../Page/大甲溪.md "wikilink")。依地質分類屬於[雪山山脈帶的變質岩溫泉](../Page/雪山山脈.md "wikilink")。[日治時代稱](../Page/台灣日治時期.md "wikilink")**明治溫泉**，與[富士溫泉](../Page/廬山溫泉.md "wikilink")、[櫻溫泉並稱中部三大溫泉](../Page/春陽溫泉.md "wikilink")。

## 泉質

谷關溫泉的泉水清澈，可飲用，水溫約48℃-60℃，酸鹼pH值約7.9，含碳酸氫根離子242ppm，鈉離子約146ppm，屬於中性碳酸氫鈉泉，但也含硫化物，因此在溫泉區可聞到硫磺味。

## 歷史

1907年（明治40年）在此地發現溫泉，在[台灣日治時期已有澡堂建立](../Page/台灣日治時期.md "wikilink")，舊稱**明治溫泉**。後因[中部橫貫公路建設完工](../Page/中部橫貫公路.md "wikilink")，提升了谷關溫泉的觀光價值。曾遭到1999年[九二一大地震和](../Page/九二一大地震.md "wikilink")2004年[七二水災等天災破壞](../Page/七二水災.md "wikilink")，後又重建修復完成。谷關也因明治溫泉得名，舊名**明治**（Meiji，一說由泰雅族原住民語「美奇」音譯為日語漢字），附近有上明治（[上谷關](../Page/上谷關.md "wikilink")）、下明治（[下谷關](../Page/下谷關.md "wikilink")）。

## 交通

  - 大眾運輸：
    1.  在台中市搭乘往東勢、谷關的方向的客運。
    2.  搭乘豐原客運，到谷關的十文溪站下車，谷關溫泉離十文溪站約1公里。
  - 開車：
    1.  由[中山高速公路的](../Page/中山高速公路.md "wikilink")[台中系統交流道下](../Page/台中系統交流道.md "wikilink")，循[國道四號](../Page/國道四號.md "wikilink")[台中環線至](../Page/台中環線.md "wikilink")[豐原端接](../Page/豐原端.md "wikilink")[台3線到](../Page/台3線.md "wikilink")[東勢](../Page/東勢區.md "wikilink")，再轉接[台8線進入舊中部橫貫公路](../Page/台8線.md "wikilink")，途經[和平區](../Page/和平區_\(臺中市\).md "wikilink")、[裡冷部落](../Page/裡冷部落.md "wikilink")、[松鶴部落](../Page/松鶴部落.md "wikilink")、情人谷等地，至谷關。
    2.  亦可從[福爾摩沙高速公路的](../Page/福爾摩沙高速公路.md "wikilink")[中橫系統交流道下](../Page/中橫系統交流道.md "wikilink")，循[國道六號](../Page/國道六號.md "wikilink")[中橫高速公路至](../Page/中橫高速公路.md "wikilink")[埔里交流道往北走](../Page/埔里交流道.md "wikilink")[台21線至](../Page/台21線.md "wikilink")[和平區中心](../Page/和平區_\(臺中市\).md "wikilink")，再轉接[台8線進入舊中部橫貫公路](../Page/台8線.md "wikilink")，途經[裡冷部落](../Page/裡冷部落.md "wikilink")、[松鶴部落](../Page/松鶴部落.md "wikilink")、情人谷等地，至谷關。

## 外部連結

  - [谷關](https://web.archive.org/web/20130818222318/http://xg.tmi.tw/)
  - [谷關溫泉風景區](http://bluezz.tw/c.php?id=1480)
  - [裡冷溫泉](http://www.go-ya.com.tw)

[category:臺灣溫泉](../Page/category:臺灣溫泉.md "wikilink")
[category:台中市旅遊景點](../Page/category:台中市旅遊景點.md "wikilink")

[Category:梨山風景區](../Category/梨山風景區.md "wikilink") [Category:和平區
(臺灣)](../Category/和平區_\(臺灣\).md "wikilink")