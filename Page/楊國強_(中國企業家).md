**楊國強**（），生於[廣東](../Page/廣東.md "wikilink")[順德](../Page/順德.md "wikilink")[北滘鎮](../Page/北滘鎮.md "wikilink")，[中國大陸](../Page/中國大陸.md "wikilink")[企業家](../Page/企業家.md "wikilink")，[廣東省](../Page/廣東省.md "wikilink")[地產商](../Page/地產.md "wikilink")[碧桂園集團創辦人兼董事局主席](../Page/碧桂園.md "wikilink")。自2007年4月起，他的家族（主要由其二女儿[楊惠妍持有股份](../Page/楊惠妍.md "wikilink")），資產達455億元[人民幣](../Page/人民幣.md "wikilink")。\[1\]

## 生平

楊國強年少時家境十分貧窮，曾在鄉下[耕田放牛](../Page/耕田.md "wikilink")，其後轉職做[建築工人](../Page/建築工人.md "wikilink")，當時月入只有180元[人民幣](../Page/人民幣.md "wikilink")。20來歲左右，楊國強加入鎮政府屬下的北滘[建築工程公司](../Page/建築.md "wikilink")，從底層做起，至1990年代初，他已經晉升為[總經理](../Page/總經理.md "wikilink")。他首度創業，是在企業改制中與多名同鄉合資將北滘建築工程公司買下，並成為大股東。後來北滘建築更名為廣東騰越建築工程公司，成為目前碧桂園的附屬公司。不過他的房地產之路在起步即遭遇重大挫折。1992年，他趁內地房產熱，以低價買下順德[碧江及](../Page/碧江.md "wikilink")[桂山交界的大片荒地](../Page/桂山.md "wikilink")，並以“碧桂園”命名開始進軍房地產。不過由於內地樓市泡沫破裂，碧桂園4000戶只賣出了三戶，還差點變成[爛尾樓](../Page/爛尾樓.md "wikilink")。

不過，楊國強卻最後轉危為安，他到[廣州一間私營](../Page/廣州.md "wikilink")[貴族學校參觀](../Page/貴族學校.md "wikilink")，無意發現一間貴族學校，可以吸引有錢人願意到[窮鄉僻壤聚居](../Page/窮鄉僻壤.md "wikilink")，結果他照辦煮碗，在碧桂園興建國際學校，成功將樓盤賣光。碧桂園成立學校至初，成功連系[北京名校](../Page/北京.md "wikilink")[景山學校](../Page/景山學校.md "wikilink")，成為其廣東分校，並以「[鄧小平孫子都入讀景山](../Page/鄧小平.md "wikilink")」作招徠，結果一開校便吸引旅居順德的外地商人，及當地有錢人送子女入讀。他不僅通過貴族學校帶來樓盤銷售的客源，多達每人30萬元人民幣的學費，亦為當時陷入困境的碧桂園帶來一筆不菲的流動資金。碧桂園因而一炮走紅，其地產生意便擴展至廣東各地。\[2\]

碧桂園集團在2007年4月於[香港聯合交易所上市](../Page/香港聯合交易所.md "wikilink")，[股票獲同鄉](../Page/股票.md "wikilink")[李兆基和](../Page/李兆基.md "wikilink")[鄭裕彤各斥資](../Page/鄭裕彤.md "wikilink")10億港元率先認購，更曾引起全港市民爭相認購。\[3\]楊國強名義上雖是董事局主席，但個人沒有持有公司股份，而且交給第二女兒[楊惠妍持有](../Page/楊惠妍.md "wikilink")，其女兒因應公司股價上漲而成為中國大陸首富。不過，楊表明女兒只是代表楊氏家族持有碧桂園大部分股份，而他自己未來仍會參與公司的管理，暫時沒有退休的打算。\[4\]

## 慈善事业

曾10年內资助4000名学生。\[5\]
並於2017年獲得[傑出企業家社會責任獎](../Page/傑出企業家社會責任獎.md "wikilink")\[6\]。

## 收購邵氏傳聞

2008年5月，傳聞楊國強將以私人身分，出資至少100億港元現金，收購[邵逸夫持有的](../Page/邵逸夫.md "wikilink")[邵氏兄弟股權](../Page/邵氏兄弟.md "wikilink")；收購金額的其中30億港元是向[李兆基借來](../Page/李兆基.md "wikilink")。由於邵氏持有[電視廣播](../Page/電視廣播有限公司.md "wikilink")26%股權，所以如果傳聞屬實，楊氏也間接持有電視廣播，並成為其大股東，但由於未能實施而被迫擱置。\[7\]\[8\]

## 進軍香港傳媒

2008年初，楊國強在香港成立了一間私人公司名為[Century Home Entertainment
Limited以助他可以成功收購](../Page/Century_Home_Entertainment_Limited.md "wikilink")[邵氏進軍香港傳媒](../Page/邵氏.md "wikilink")。

## 家庭

妻：

大女儿：杨子莹

二女儿：杨惠妍

## 參考資料

  - [順德區](../Page/順德區.md "wikilink")
  - [碧桂園](../Page/碧桂園.md "wikilink")
  - [楊惠妍](../Page/楊惠妍.md "wikilink")
  - [李兆基](../Page/李兆基.md "wikilink")
  - [鄭裕彤](../Page/鄭裕彤.md "wikilink")

## 注釋

[Category:中华人民共和国房地产商](../Category/中华人民共和国房地产商.md "wikilink")
[Category:中華人民共和國慈善家](../Category/中華人民共和國慈善家.md "wikilink")
[Category:中華人民共和國億萬富豪](../Category/中華人民共和國億萬富豪.md "wikilink")
[Category:第十二屆全國政協委員](../Category/第十二屆全國政協委員.md "wikilink")
[Category:順德人](../Category/順德人.md "wikilink")
[G国强](../Category/杨姓.md "wikilink")

1.  [楊惠妍首獲富豪榜加冕](https://web.archive.org/web/20070501061306/http://hk.news.yahoo.com/070428/12/26g5s.html)
2.  [碧桂園創辦人楊國強傳奇:從農民踏上首富之路](http://www.p5w.net/stock/hkstock/gsxx/200704/t876613.htm)
3.  [碧桂園上市籌資達百億元，同鄉富豪李兆基鄭裕彤捧場](https://web.archive.org/web/20070319194927/http://hk.news.yahoo.com/070316/74/23rw9.html)
4.  [楊國強為女兒戴上首富桂冠](http://finance.sina.com.cn/stock/hkstock/ggIPO/20070410/07313486152.shtml)
5.  <http://news.sina.com.cn/s/2007-05-15/052812988242.shtml>
6.  \[[http://www.mirrorpost.com.hk/index.php/228-6th-award-ceremony"第六屆企業社會責任獎頒獎典禮](http://www.mirrorpost.com.hk/index.php/228-6th-award-ceremony%22第六屆企業社會責任獎頒獎典禮)"\],*鏡報月刊*,06/01/2017
7.  [楊國強籌得百億買無綫](https://web.archive.org/web/20080531003354/http://hk.news.yahoo.com/080526/12/2unpm.html)
8.  [楊國強百億元收購電視廣播](https://web.archive.org/web/20080529231534/http://hk.news.yahoo.com/080527/12/2uptm.html)