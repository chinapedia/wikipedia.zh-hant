**Odoo**（先前曾名為**OpenERP**，更早之前則為**TinyERP**），是一套[企業資源規劃](../Page/企業資源規劃.md "wikilink")（ERP）及[客戶關係管理](../Page/客戶關係管理.md "wikilink")（CRM）系统。以[Python语言开发](../Page/Python.md "wikilink")，数据库采用开源的[PostgreSQL](../Page/PostgreSQL.md "wikilink")，系统以[GNU](../Page/GNU.md "wikilink")
[GPL开源协议发布](../Page/GPL.md "wikilink")。

系统提供较灵活的模块架构，常用模块包括：[采购管理](../Page/采购管理.md "wikilink")、[销售管理](../Page/销售管理.md "wikilink")、[库存管理](../Page/库存管理.md "wikilink")、[财务管理](../Page/财务管理.md "wikilink")、货品管理、[营销管理](../Page/营销管理.md "wikilink")、客户关系管理、[生产管理](../Page/生产管理.md "wikilink")、[人事管理及服务支持等等](../Page/人事管理.md "wikilink")。用户可以直接从模块库中选择安装适用[模块](../Page/模块.md "wikilink")，或进行模块卸载、升级的管理操作。

客户端用户界面是基于[GTK的](../Page/GTK.md "wikilink")，同时支持[Linux和](../Page/Linux.md "wikilink")[Windows平台](../Page/Windows.md "wikilink")。目前还有开发中的基于[TurboGears的eTiny](../Page/TurboGears.md "wikilink")
Web客户端。

## 參見

  - [會計軟體](../Page/會計軟體.md "wikilink")
  - [會計軟體的比較](../Page/會計軟體的比較.md "wikilink")

## 外部連結

  - [Official Website after rebranding to Open ERP](http://openerp.com)
  - [社群網站](https://web.archive.org/web/20140317084507/http://openobject.com/)
  - [論壇](https://web.archive.org/web/20090315070843/http://www.openobject.com/forum/)
  - [Collaborative development
    platform](https://launchpad.net/openobject)
  - [OpenERP中文支持网站](http://www.shine-it.net)
  - [OpenERP中文论坛](http://www.openerpchina.org)
  - [OpenERP正式业务伙伴和培训中心](http://www.elico-corp.com/?lang=zh)
  - [文檔](http://www.infoacp.es/documentacion-openerp)

[Category:管理信息系統](../Category/管理信息系統.md "wikilink")
[Category:GTK](../Category/GTK.md "wikilink")
[Category:Python](../Category/Python.md "wikilink")
[Category:會計軟件](../Category/會計軟件.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")