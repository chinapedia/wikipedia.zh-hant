**李鍾原**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/演員.md "wikilink")，其他譯名**李鐘原**。曾擔任建築師的工作。

## 演出作品

### 電視劇

#### 1990年代

  - 1993年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[漢江布穀鳥](../Page/漢江布穀鳥.md "wikilink")》
  - 1994年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[最後的勝負](../Page/最後的勝負.md "wikilink")》飾演
    金善宰
  - 1994年：SBS《[愛藍色](../Page/愛藍色.md "wikilink")》
  - 1995年：KBS《[年輕人的陽地](../Page/年輕人的陽地.md "wikilink")》
  - 1998年：SBS《[洪吉童](../Page/洪吉童_\(1998年電視劇\).md "wikilink")》
  - 1999年：SBS《[青春陷阱](../Page/青春陷阱.md "wikilink")》

#### 2000年代

  - 2000年：KBS《》飾演 宋賢泰
  - 2000年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[警察特攻隊](../Page/警察特攻隊.md "wikilink")》
  - 2001年：SBS《[律師情人](../Page/律師情人.md "wikilink")》
  - 2001年：[KBS](../Page/韓國放送公社.md "wikilink")《[純情](../Page/純情.md "wikilink")》
  - 2004年：KBS《[愛情的條件](../Page/愛情的條件.md "wikilink")》飾演 陳廷翰
  - 2004年：SBS《[選擇](../Page/選擇_\(電視劇\).md "wikilink")》
  - 2005年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[悲傷戀歌](../Page/悲傷戀歌.md "wikilink")》飾演
    吳相真
  - 2005年：KBS《[悲傷啊，再見！](../Page/悲傷啊，再見！.md "wikilink")》飾演 韓成民
  - 2006年：SBS《[突然有一天](../Page/突然有一天.md "wikilink")》
  - 2006年：上海電視台《[迷路](../Page/迷路.md "wikilink")(中韓合拍)》飾演 林明正
  - 2007年：SBS《[我男人的女人](../Page/我男人的女人.md "wikilink")》飾演 朴碩俊
  - 2007年：SBS《[起飛](../Page/起飛_\(韓國連續劇\).md "wikilink")》飾演 姜仁成
  - 2007年：KBS《[媳婦的全盛時代](../Page/媳婦的全盛時代.md "wikilink")》飾演 金基河
  - 2008年：SBS《[幸福](../Page/幸福_\(電視劇\).md "wikilink")》飾演 朴尚旭
  - 2008年：MBC《[伊甸園之東](../Page/伊甸園之東.md "wikilink")》飾演 李琦喆
  - 2008年：MBC《[我人生的黃金期](../Page/我人生的黃金期.md "wikilink")》飾演 柳泰日
  - 2008年：KBS《[風之國](../Page/風之國.md "wikilink")》飾演 解明
  - 2008年：MBC《[綜合醫院2](../Page/綜合醫院2.md "wikilink")》飾演 韓基泰
  - 2009年：KBS《[一起恰恰恰](../Page/一起恰恰恰.md "wikilink")》飾演 李俊佑

#### 2010年代

  - 2010年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[金首露](../Page/金首露_\(電視劇\).md "wikilink")》飾演
    趙芳
  - 2011年：MBC《[Gloria](../Page/Gloria.md "wikilink")》飾演 李志碩
  - 2011年：[KBS](../Page/韓國放送公社.md "wikilink")《[近肖古王](../Page/近肖古王_\(電視劇\).md "wikilink")》飾演
    [故國原王](../Page/故國原王.md "wikilink")
  - 2011年：MBC《[光與影](../Page/光與影.md "wikilink")》飾演 趙明國
  - 2013年：MBC《[火之女神井兒](../Page/火之女神井兒.md "wikilink")》飾演 柳乙潭
  - 2013年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[主君的太陽](../Page/主君的太陽.md "wikilink")》飾演
    陶錫哲
  - 2013年：[JTBC](../Page/JTBC.md "wikilink")《[老大](../Page/老大_\(電視劇\).md "wikilink")》飾演
    孔昌來
  - 2014年：[MBN](../Page/MBN.md "wikilink")《[天國的眼淚](../Page/天國的眼淚.md "wikilink")》飾演
    李道燁
  - 2014年：MBC《[湔雪的魔女](../Page/湔雪的魔女.md "wikilink")》飾演 卓月翰
  - 2015年：MBC《[偉大的糟糠之妻](../Page/偉大的糟糠之妻.md "wikilink")》飾演 韓基哲
  - 2015年：tvN《[Oh 我的鬼神君](../Page/Oh_我的鬼神君.md "wikilink")》飾演 與Marco合照的演員
    (客串)
  - 2016年：KBS《[天上的約定](../Page/天上的約定.md "wikilink")》飾演 張敬莞
  - 2016年：MBC《[吹吧，美風啊](../Page/吹吧，美風啊.md "wikilink")》飾演 趙達浩
  - 2017年：KBS《[學校2017](../Page/學校2017.md "wikilink")》飾演 玄剛宇
  - 2018年：MBC《[捉迷藏](../Page/捉迷藏_\(韓國電視劇\).md "wikilink")》飾演 閔俊植

### 電影

  - 1991年：《[十七歲的coupdetat](../Page/十七歲的coupdetat.md "wikilink")》
  - 1991年：《[Green Sleeves](../Page/Green_Sleeves.md "wikilink")》
  - 1994年：《[Contract Couple](../Page/Contract_Couple.md "wikilink")》
  - 2002年：《[密愛](../Page/密愛.md "wikilink")》
  - 2003年：《[蝶](../Page/蝶_\(電影\).md "wikilink")》
  - 2003年：《[Choihui Mancheon](../Page/Choihui_Mancheon.md "wikilink")》
  - 2004年：《[Flying Boys](../Page/Flying_Boys.md "wikilink")》（客串）

## 外部連結

  - [EPG](https://web.archive.org/web/20070831011914/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=69)


  - [Yahoo
    Korea](http://kr.search.yahoo.com/search?p=%C5%C5%B7%B1%C6%AE+%C0%CC%C1%BE%BF%F8+&fr=fr_iy)


  -
[L](../Category/韓國電視演員.md "wikilink")
[L](../Category/韓國電影演員.md "wikilink")
[L](../Category/檀國大學校友.md "wikilink")
[L](../Category/全羅南道出身人物.md "wikilink")
[L](../Category/羅州市出身人物.md "wikilink")
[L](../Category/李姓.md "wikilink")