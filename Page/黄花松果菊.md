**黃花松果菊**（[學名](../Page/學名.md "wikilink")：**''Echinacea
paradoxa**''），或稱**黃花紫椎菊**，是[菊科](../Page/菊科.md "wikilink")[松果菊屬的一種](../Page/松果菊屬.md "wikilink")[多年生植物](../Page/多年生植物.md "wikilink")，也是該屬之下唯一不開紫色花的種類。植株高度約24至36公分。於夏季開花，花瓣黃色至橘黃色。葉子長約4到8[英呎](../Page/英呎.md "wikilink")，無毛，顏色深綠色。

## 参考文献

  - [美國植物資料庫中有關黃花松果菊的介紹](http://plants.usda.gov/java/profile?symbol=ECPA2)

  - [北卡羅來納州立大學網站中有關黃花松果菊的介紹](https://web.archive.org/web/20070617082340/http://www.ces.ncsu.edu/depts/hort/consumer/factsheets/wildflowers/echinacea_paradoxa.html)

  - [密蘇里植物園網頁中有關黃花松果菊的介紹](http://www.mobot.org/gardeninghelp/plantfinder/Plant.asp?code=K180)

[Category:紫錐花屬](../Category/紫錐花屬.md "wikilink")