[Containerbücken_für_Eurogate_008.jpg](https://zh.wikipedia.org/wiki/File:Containerbücken_für_Eurogate_008.jpg "fig:Containerbücken_für_Eurogate_008.jpg")
**上海振华重工（集团）股份有限公司**（），简称**振华重工**，原名**上海振华港口机械（集团）股份有限公司**，[英文商标](../Page/英文.md "wikilink")**ZPMC**（**Z**henhua
**P**ort **M**achinery
**C**ompany），是[中國交通建設集團的一間](../Page/中國交通建設.md "wikilink")[子公司](../Page/子公司.md "wikilink")，也是世界最大的[港口](../Page/港口.md "wikilink")[機械和大型鋼結構製造商](../Page/機械.md "wikilink")。

公司主要生產岸邊[集裝箱](../Page/集裝箱.md "wikilink")[起重機](../Page/起重機.md "wikilink")、輪胎式集裝箱龍門起重機、[散貨裝卸船機](../Page/散貨.md "wikilink")、鬥輪堆取料機、門座起重機、浮吊和工程船舶以及大型鋼橋構件等。公司具有[設計](../Page/設計.md "wikilink")、[製造](../Page/製造.md "wikilink")、[安裝](../Page/安裝.md "wikilink")、[調試](../Page/調試.md "wikilink")、整機[運輸](../Page/運輸.md "wikilink")、[售後服務和新](../Page/售後服務.md "wikilink")[產品開發等多個範疇](../Page/產品開發.md "wikilink")。

上海振华重工（集团）股份有限公司下有长兴分公司、常州分公司、长兴精密铸造有限公司、江阴分公司、上海港机重工有限公司、南通分公司、上海振华重工集团（南通）传动机械有限公司等多个生产基地。

上海振华已發行了[A股](../Page/A股.md "wikilink")、[B股](../Page/B股.md "wikilink")。公司淨[資產達](../Page/資產.md "wikilink")5億[美元](../Page/美元.md "wikilink")。

## 参考文献

## 外部链接

  - [上海振华重工(集团)股份有限公司](http://cn.zpmc.com/)

[Category:总部位于上海的中华人民共和国国有企业](../Category/总部位于上海的中华人民共和国国有企业.md "wikilink")
[Category:中华人民共和国机械制造企业](../Category/中华人民共和国机械制造企业.md "wikilink")
[Category:機械製造業](../Category/機械製造業.md "wikilink")
[Category:中國建築公司](../Category/中國建築公司.md "wikilink")
[Category:上海造船企业](../Category/上海造船企业.md "wikilink")
[Category:上海港](../Category/上海港.md "wikilink")
[Category:上海的世界之最](../Category/上海的世界之最.md "wikilink")
[Category:中华人民共和国的世界之最](../Category/中华人民共和国的世界之最.md "wikilink")
[Category:1992年成立的公司](../Category/1992年成立的公司.md "wikilink")