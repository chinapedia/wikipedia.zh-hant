[Central_Elevated_Walkway_2008.jpg](https://zh.wikipedia.org/wiki/File:Central_Elevated_Walkway_2008.jpg "fig:Central_Elevated_Walkway_2008.jpg")段於太子大廈結尾（通往交易廣場行人天橋起點）\]\]
[The_Central_Elevated_Walkway_near_Exchange_Square_2015.jpg](https://zh.wikipedia.org/wiki/File:The_Central_Elevated_Walkway_near_Exchange_Square_2015.jpg "fig:The_Central_Elevated_Walkway_near_Exchange_Square_2015.jpg")\]\]
[Alexander_House_Bridge_Access_2014.jpg](https://zh.wikipedia.org/wiki/File:Alexander_House_Bridge_Access_2014.jpg "fig:Alexander_House_Bridge_Access_2014.jpg")連接至[遮打大廈的行人天橋](../Page/遮打大廈.md "wikilink")\]\]
[HK_Hang_Seng_Bank_Headquarters.JPG](https://zh.wikipedia.org/wiki/File:HK_Hang_Seng_Bank_Headquarters.JPG "fig:HK_Hang_Seng_Bank_Headquarters.JPG")的一段路標\]\]
**中區行人天橋系統**
（），又稱**中環行人天橋網絡**，位於[香港島](../Page/香港島.md "wikilink")[中西區的大型](../Page/中西區_\(香港\).md "wikilink")[行人天橋網絡設施](../Page/行人天橋.md "wikilink")；部分路段靠近[維多利亞港](../Page/維多利亞港.md "wikilink")，可飽覽維港景色。

系統由[香港政府及](../Page/香港政府.md "wikilink")[中環各大地產發展商分期互聯建成](../Page/中環.md "wikilink")。發展商包括[香港置地](../Page/香港置地.md "wikilink")、[怡和](../Page/怡和.md "wikilink")、[信德集團等](../Page/信德集團.md "wikilink")（亦因此不同路段有不同設計；出現全球較罕見的多於一個發展商同時合作興建行人天橋），在部份地方設有[樓梯及](../Page/樓梯.md "wikilink")[電動扶梯方便行人](../Page/電動扶梯.md "wikilink")，而部份連接兩座大廈的天橋則設有[空氣調節](../Page/空氣調節.md "wikilink")。

在[金鐘一帶有另一個行人天橋系統](../Page/金鐘.md "wikilink")，經由[炮台里](../Page/炮台里.md "wikilink")、[皇后像廣場或](../Page/皇后像廣場.md "wikilink")[中西區海濱長廊](../Page/中西區海濱長廊.md "wikilink")（中環段），與中環的行人天橋系統連接。（中途經過[美國銀行中心](../Page/美國銀行中心.md "wikilink")、[金鐘廊](../Page/金鐘廊.md "wikilink")（穿過）等）

此系統以[國際金融中心商場作中心](../Page/國際金融中心商場.md "wikilink")，並分為[東](../Page/東.md "wikilink")[南](../Page/南.md "wikilink")[西](../Page/西.md "wikilink")[北四](../Page/北.md "wikilink")[方向的行人天橋段](../Page/方向.md "wikilink")。

## 歷史

1970年代，置地在當時的中環海旁興建康樂大廈（今[怡和大廈](../Page/怡和大廈.md "wikilink")），為方便行人來往，便興建了一條橫跨[干諾道中的行人天橋](../Page/干諾道中.md "wikilink")，連接太古大廈（今[遮打大廈](../Page/遮打大廈.md "wikilink")），康樂大廈及[香港郵政總局](../Page/香港郵政.md "wikilink")（須經過康樂大廈旁的樓梯前往地面前往）。置地亦在其發展的大廈之間興建多條行人天橋連接。1980年代初，[交易廣場落成後](../Page/交易廣場.md "wikilink")，政府興建了一條行人天橋連接置地天橋，並沿海旁向西擴展，連接當時的[中環碼頭及上環](../Page/中環碼頭.md "wikilink")[信德中心](../Page/信德中心.md "wikilink")。一些位於[皇后大道中的大廈](../Page/皇后大道中.md "wikilink")，如渣打銀行大廈及中匯大廈，亦興建了行人天橋連接置地的物業，成為網絡的一部份。

1993年，[中環至半山自動扶梯系統啟用](../Page/中環至半山自動扶梯系統.md "wikilink")，並經[恒生銀行總行連接至行人天橋系統](../Page/恒生銀行總行.md "wikilink")，以[皇后大道中為起點](../Page/皇后大道.md "wikilink")，貫穿中環多條狹窄的街道至[干德道止](../Page/干德道.md "wikilink")\[1\]。1998年，[香港國際金融中心及](../Page/香港國際金融中心.md "wikilink")[機場快綫](../Page/機場快綫.md "wikilink")[香港站落成](../Page/香港站.md "wikilink")，部份沿干諾道中興建的行人天橋拆卸，改為連接[國際金融中心](../Page/國際金融中心_\(香港\).md "wikilink")。2000年後，政府再興建一條平行的天橋連接[環球大廈與交易廣場](../Page/環球大廈.md "wikilink")。

1990年代初，一群[日本的學者以當時的中區行人天橋系統研究天橋系統對城市發展和規劃的重要性](../Page/日本.md "wikilink")，並繪製了有關地圖，稱之為「上の道」（Ue-no-michi）\[2\]。

## 運行時間

上午 06:00 至午夜（12:00）

  - 每天下行時間**：**上午 06:00 至上午 10:00
  - 每天上行時間**：**上午 10:00 至午夜（12:00）

## 功能

中區行人天橋系統是吸引人流客流的有利設施，實行人車分隔，一方面令行人有一個交通安全及舒適的環境，另一方面增加汽車流動速度。同時供觀賞城市景色的機會，吸引遊客及消費。

## 中環系統

[centre](https://zh.wikipedia.org/wiki/File:CentralElevatedWalkwaySystemDiagram_zh.png "fig:centre")

<File:Central> Elevated Walkway Sheung Wan Section view1
2015.jpg|中區行人天橋近[林士街多層停車場](../Page/林士街.md "wikilink")
<File:Central> Elevated Walkway Near ifc
2015.jpg|中區行人天橋向北擴展至[中環碼頭的部份](../Page/中環碼頭.md "wikilink")，可穿過[國際金融中心商場前往](../Page/國際金融中心商場.md "wikilink")
<File:Central> Elevated Walkway Sheung Wan Section
2015.jpg|在中區行人天橋上可欣賞[維多利亞港的海景](../Page/維多利亞港.md "wikilink")
<File:Connaught> Road Central Bridge 201705.jpg|中區行人天橋系統近華懋大廈

### 連接範圍

  - 東：[中環](../Page/中環.md "wikilink")[置地廣塲](../Page/置地廣塲.md "wikilink")、[太子大廈](../Page/太子大廈.md "wikilink")、[港鐵](../Page/港鐵.md "wikilink")[中環站](../Page/中環站.md "wikilink")、[怡和大廈](../Page/怡和大廈.md "wikilink")、[美國銀行中心](../Page/美國銀行中心.md "wikilink")
  - 南：[中環](../Page/中環.md "wikilink")[恒生銀行總行](../Page/恒生銀行.md "wikilink")（穿過）、前[中環街市](../Page/中環街市.md "wikilink")、[中環至半山自動扶梯系統](../Page/中環至半山自動扶梯系統.md "wikilink")：[中環蘇豪區](../Page/中環蘇豪區.md "wikilink")、半山[干德道](../Page/干德道.md "wikilink")（唯一一個不能直接通往港鐵站的路段）
  - 西：[上環](../Page/上環.md "wikilink")[信德中心](../Page/信德中心.md "wikilink")、[港澳碼頭](../Page/港澳碼頭_\(香港\).md "wikilink")、[無限極廣場](../Page/無限極廣場.md "wikilink")、[港鐵](../Page/港鐵.md "wikilink")[上環站](../Page/上環站.md "wikilink")（也是所有由國際金融中心商場開始之最長路段，但不計算[中環至半山自動扶梯系統路段](../Page/中環至半山自動扶梯系統.md "wikilink")）
  - 北：[香港國際金融中心](../Page/國際金融中心_\(香港\).md "wikilink")、[中環碼頭](../Page/中環碼頭.md "wikilink")、港鐵[香港站](../Page/香港站.md "wikilink")、[中西區海濱長廊](../Page/中西區海濱長廊.md "wikilink")（中環段）

### 連接建築物

  - [國際金融中心](../Page/國際金融中心_\(香港\).md "wikilink")（直接穿過其[商場內部](../Page/國際金融中心商場.md "wikilink")）
  - [香港大會堂](../Page/香港大會堂.md "wikilink")
  - [中西區海濱長廊](../Page/中西區海濱長廊.md "wikilink")（中環段）
  - [香港摩天輪](../Page/香港摩天輪.md "wikilink")
  - [怡和大廈](../Page/怡和大廈.md "wikilink")
  - [交易廣場](../Page/交易廣場.md "wikilink")
  - [香港郵政總局](../Page/香港郵政總局.md "wikilink")
  - [環球大廈](../Page/環球大廈.md "wikilink")
  - [遮打大廈](../Page/遮打大廈.md "wikilink")
  - [歷山大廈](../Page/歷山大廈.md "wikilink")

<!-- end list -->

  - [香港文華東方酒店](../Page/香港文華東方酒店.md "wikilink")
  - [太子大廈](../Page/太子大廈.md "wikilink")
  - [皇后大道中九號](../Page/皇后大道中九號.md "wikilink")
  - [渣打銀行大廈](../Page/渣打銀行大廈_\(香港\).md "wikilink")
  - [置地廣場](../Page/置地廣塲.md "wikilink")
  - [中建大廈](../Page/中建大廈.md "wikilink")
  - [中匯大廈](../Page/中匯大廈.md "wikilink")
  - [恒生銀行總行大廈](../Page/恒生銀行總行大廈.md "wikilink")
  - 前[中環街市](../Page/中環街市.md "wikilink")
  - [100QRC](../Page/100QRC.md "wikilink")

<!-- end list -->

  - [海港政府大樓](../Page/海港政府大樓.md "wikilink")
  - [無限極廣場](../Page/無限極廣場.md "wikilink")
  - [信德中心](../Page/信德中心.md "wikilink")

[缩略图](https://zh.wikipedia.org/wiki/File:Central_Market_\(1\).JPG "fig:缩略图")

## 金鐘系統

[AdmiraltyElevatedWalkwaySystemDiagram_zh.png](https://zh.wikipedia.org/wiki/File:AdmiraltyElevatedWalkwaySystemDiagram_zh.png "fig:AdmiraltyElevatedWalkwaySystemDiagram_zh.png")
離開中環核心區的天橋網後，透過[中環站到達](../Page/中環站.md "wikilink")[遮打花園後上樓梯進入](../Page/遮打花園.md "wikilink")**中區行人天橋系統**的金鐘系統的首站[美利道停車場大廈](../Page/美利道停車場大廈.md "wikilink")，向海的方向可以到和記大廈及友邦金融中心，或再接中環站，向山的方向可以到長江集團中心、花園道三號及香港公園（紅棉道入口），而直接穿過停車場大廈，到達東昌大廈，之後向海的方向可以到遠東金融中心、海富中心、香港政府總部，並擬建天橋至中信大廈。向山的方向可以到力寶中心、金鐘廊、太古廣場、金鐘道政府合署及香港公園（法院道入口），並透過太古廣場的地下通道到達[金鐘站或位於](../Page/金鐘站.md "wikilink")[灣仔的太古廣場三期](../Page/灣仔.md "wikilink")。

### 範圍

  - 東至：[中信大廈](../Page/中信大廈.md "wikilink")、[港鐵](../Page/港鐵.md "wikilink")[金鐘站](../Page/金鐘站.md "wikilink")
  - 南至：[香港公園](../Page/香港公園.md "wikilink")、[美利大廈](../Page/美利大廈.md "wikilink")
  - 西至：[遮打花園](../Page/遮打花園.md "wikilink")、[和記大廈](../Page/和記大廈.md "wikilink")、[友邦金融中心](../Page/友邦金融中心.md "wikilink")、[長江集團中心](../Page/長江集團中心.md "wikilink")
  - 北至：[香港大會堂](../Page/香港大會堂.md "wikilink")、[政府總部](../Page/政府總部_\(添馬艦\).md "wikilink")、[立法會綜合大樓](../Page/立法會綜合大樓.md "wikilink")、[行政長官辦公室](../Page/行政長官辦公室_\(添馬艦\).md "wikilink")、[中西區海濱長廊](../Page/中西區海濱長廊.md "wikilink")（中環段）

### 連接建築物

  - [太古廣場三期](../Page/太古廣場.md "wikilink")
  - [中信大廈](../Page/中信大廈.md "wikilink")
  - [太古廣場](../Page/太古廣場.md "wikilink")
  - [統一中心](../Page/統一中心.md "wikilink")
  - [海富中心](../Page/海富中心.md "wikilink")
  - [金鐘廊](../Page/金鐘廊.md "wikilink")
  - [遠東金融中心](../Page/遠東金融中心.md "wikilink")
  - [力寶中心](../Page/力寶中心.md "wikilink")
  - [金鐘道政府合署](../Page/金鐘道政府合署.md "wikilink")
  - [東昌大廈](../Page/東昌大廈.md "wikilink")
  - [美利道停車場大廈](../Page/美利道停車場大廈.md "wikilink") (已拆卸)
  - [美利大廈](../Page/美利大廈.md "wikilink")

<!-- end list -->

  - [聖約翰座堂](../Page/聖約翰座堂.md "wikilink")
  - [長江集團中心](../Page/長江集團中心.md "wikilink")、長江公園
  - [香港公園](../Page/香港公園.md "wikilink")
  - [花園道三號](../Page/花園道三號.md "wikilink")
  - [美國銀行中心](../Page/美國銀行中心.md "wikilink")
  - [和記大廈](../Page/和記大廈.md "wikilink") (即將拆卸)
  - [友邦金融中心](../Page/友邦金融中心.md "wikilink")
  - [政府總部](../Page/政府總部_\(添馬艦\).md "wikilink")
  - [立法會綜合大樓](../Page/立法會綜合大樓.md "wikilink")
  - [行政長官辦公室](../Page/行政長官辦公室_\(添馬艦\).md "wikilink")
  - [中西區海濱長廊](../Page/中西區海濱長廊.md "wikilink")（中環段）

[缩略图](https://zh.wikipedia.org/wiki/File:HK_Admiralty_紅棉道_Cotton_Tree_Drive_footbridge_Far_East_Finance_Centre_facade.JPG "fig:缩略图")\]\]

## 相關條目

  - [中環至半山自動扶手電梯系統](../Page/中環至半山自動扶手電梯系統.md "wikilink")
  - [灣仔北行人天橋系統](../Page/灣仔北行人天橋系統.md "wikilink")
  - [旺角行人天橋系統](../Page/旺角行人天橋系統.md "wikilink")
  - [荃灣行人天橋網絡](../Page/荃灣行人天橋網絡.md "wikilink")
  - [香港西九龍站行人天橋系統](../Page/香港西九龍站.md "wikilink")（連接[九龍站和](../Page/九龍站_\(港鐵\).md "wikilink")[柯士甸站](../Page/柯士甸站.md "wikilink")）
  - [九龍灣行人天橋系統](../Page/九龍灣.md "wikilink")（連接九龍灣商貿區至[牛頭角](../Page/牛頭角.md "wikilink")）
  - [翠屏道行人天橋](../Page/翠屏道.md "wikilink")
  - [慈雲山行人天橋系統](../Page/慈雲山.md "wikilink")（連接慈雲山至[荷里活廣場](../Page/荷里活廣場.md "wikilink")）
  - [寶馬山行人通道系統](../Page/寶馬山.md "wikilink")
  - [安達臣道發展計劃區域行人天橋系統](../Page/安達臣道發展計劃.md "wikilink")
  - [元朗行人天橋系統](../Page/元朗行人天橋系統.md "wikilink")
  - [深水埗行人天橋系統](../Page/深水埗行人天橋系統.md "wikilink")
  - [台北](../Page/台北.md "wikilink")[信義商圈空橋系統](../Page/信義商圈空橋系統.md "wikilink")
  - [吉隆坡](../Page/吉隆坡.md "wikilink")[城中城－武吉免登空橋系統](../Page/城中城－武吉免登空橋系統.md "wikilink")
  - [粉嶺站至](../Page/粉嶺站.md "wikilink")[聯和墟行人天橋](../Page/聯和墟.md "wikilink")

## 參考資料

<references/>

## 外部連結

  - [香港政府
    交通諮詢委員會文件 2001年5月](http://www.info.gov.hk/gia/general/200105/22/0522223.htm)
  - [香港臨時立法會 財務委員會 工務小組委員會
    討論文件 1997年11月](http://www.legco.gov.hk/yr97-98/chinese/fc/pwsc/papers/pw261185.htm)
  - [《香港天橋：空中走廊綿延別樣風情》](http://news.xinhuanet.com/newscenter/2004-11/07/content_2186989.htm)

{{-}}

[Category:上環](../Category/上環.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港行人天橋系統](../Category/香港行人天橋系統.md "wikilink")

1.
2.