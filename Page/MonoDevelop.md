**MonoDevelop**是個適用於[Linux](../Page/Linux.md "wikilink")、Mac OS
X\[1\]和Microsoft
Windows\[2\]的[開放原始碼](../Page/開放原始碼.md "wikilink")[整合開發環境](../Page/集成开发环境.md "wikilink")，主要用來開發[Mono與](../Page/Mono.md "wikilink")[.NET
Framework軟體](../Page/.NET_Framework.md "wikilink")。MonoDevelop整合了很多[Eclipse與](../Page/Eclipse.md "wikilink")[Microsoft
Visual
Studio的特性](../Page/Microsoft_Visual_Studio.md "wikilink")，像是Intellisense、[版本控制還有GUI與Web設計工具](../Page/版本控制.md "wikilink")。另外還整合了[GTK\#GUI設計工具](../Page/Gtk_Sharp.md "wikilink")（叫做Stetic）。\[3\]目前支援的語言有[Python](../Page/Python.md "wikilink")、[Vala](../Page/Vala.md "wikilink")、[C♯](../Page/C♯.md "wikilink")、[Java](../Page/Java.md "wikilink")、[BOO](../Page/BOO.md "wikilink")、[Nemerle](../Page/Nemerle.md "wikilink")、[Visual
Basic
.NET](../Page/Visual_Basic_.NET.md "wikilink")、[CIL](../Page/通用中間語言.md "wikilink")、[C與](../Page/C語言.md "wikilink")[C++](../Page/C++.md "wikilink")\[4\]\[5\]。

## 歷史

在2003年後期，部份[Mono社群的開發者開始移植](../Page/Mono.md "wikilink")[SharpDevelop到Linux上](../Page/SharpDevelop.md "wikilink")（[SharpDevelop是個成功的](../Page/SharpDevelop.md "wikilink").NET開放原始碼[整合開發環境](../Page/集成开发环境.md "wikilink")），將原本以System.Windows.Forms為基礎的代碼改為使用[GTK\#](../Page/Gtk_Sharp.md "wikilink")。也由於是由[SharpDevelop分支出來](../Page/SharpDevelop.md "wikilink")，所以MonoDevelop大致上的架構與[SharpDevelop相同](../Page/SharpDevelop.md "wikilink")，不過時至今日，其實已經完全脫勾了。

MonoDevelop幾乎都以[Mono專案為主](../Page/Mono.md "wikilink")，目前由Novell與[Mono社群維護](../Page/Mono.md "wikilink")。

## 在非Linux平台上

MonoDevelop也可以在Windows跟Mac OS
X平台上執行。但並不是一開始就可以的，而是到2.2之後才正式可以。\[6\]MonoDevelop的Mac
OS X版本裡包含了Mono的安裝程式，\[7\]但卻因為原生OS
X平台[GTK的拖拉問題而沒有包含Stetic視覺化設計工具](../Page/GTK.md "wikilink")。\[8\]Mono也提供了給執行在SPARC上的Solaris
8套件包，\[9\]給OpenSolaris用的套件包則只由OpenSolaris社群裡的群組提供。\[10\]在FreeBSD上，同樣地是由FreeBSD社群提供支援。\[11\]

## 參見

  - [Mono](../Page/Mono.md "wikilink")
  - [整合開發環境](../Page/集成开发环境.md "wikilink")
  - [Anjuta](../Page/Anjuta.md "wikilink")
  - [KDevelop](../Page/KDevelop.md "wikilink")
  - [SharpDevelop](../Page/SharpDevelop.md "wikilink")
  - [Eclipse](../Page/Eclipse.md "wikilink")

## 參考

## 外部連結

  -
[Category:集成开发环境](../Category/集成开发环境.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:自由整合開發環境](../Category/自由整合開發環境.md "wikilink")
[Category:Linux集成开发环境](../Category/Linux集成开发环境.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.