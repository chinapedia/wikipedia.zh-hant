**葉維廉**（），廣東中山人，香港孕育的台灣詩人。

## 生平

因戰亂來到香港，在香港接受教育及成長，詩創作亦由此開始，曾與崑南、王無邪辦《詩朵》詩刊。他的詩最早成形於香港，後去台灣，畢業於[台灣大學外文系](../Page/台灣大學.md "wikilink")、[台灣師範大學英語研究所](../Page/台灣師範大學.md "wikilink")，與[白先勇等人是同學](../Page/白先勇.md "wikilink")，擔任《現代文學》的撰稿者。1963年遠赴美國[愛荷華大學攻讀碩士](../Page/愛荷華大學.md "wikilink")，1967年獲[普林斯頓大學哲學博士](../Page/普林斯頓大學.md "wikilink")。任[加州大學聖地亞哥校區比較文學教授](../Page/加州大學.md "wikilink")。1970年返台灣大學擔任客座教授，協助建立比較文學博士班。1975年與[侯健](../Page/侯健.md "wikilink")、[楊牧](../Page/楊牧.md "wikilink")、[姚一葦等人創辦](../Page/姚一葦.md "wikilink")《文學評論》期刊。1980年出任香港中文大學英文系首席客座教授，建立比較文學研究所。1980年代多次回到大陸，在[北京大學](../Page/北京大學.md "wikilink")、[清華大學講授比較文學](../Page/清華大學.md "wikilink")。葉維廉早年曾名列“台灣十大傑出詩人”之一。後轉而從事比較文學、美學和文化哲學等的研究，著述等身，主要著作有《龐德的國泰集》、《中國現代小說的風貌》、《飲之太和》、《比較詩學》、《歷史、傳釋與美學》、《解讀現代與後現代》、《與當代藝術家的對話》、《尋求跨中西文化的共同文學規律》、《中國詩學》等。
葉維廉與葉灼出版的攝影詩集，在San Diego Museum of Art的展覽 “Paris: Dialogues and
Meditations”為底本，2012年精選了十三輯葉灼先生的黑白攝影及葉教授就影像裏的巴黎物事為靈感衍生的詩歌，在香港文學節專題展覽「異地情懷」的部分展出。可見他不時回港參與文學活動，心繫香港。

## 相关连接

  - [中国诗歌库](http://www.shigeku.com) <http://www.shigeku.com>
  - [中国诗歌史](http://poetrycn.com) <http://poetrycn.com>
  - [臺大近代名家手稿系列展之四 -
    葉維廉](https://web.archive.org/web/20090524172723/http://www.lib.ntu.edu.tw/CG/manuscript/yip/default.htm)
  - [臺灣大學圖書館專題書單 - 當代中文作家系列 -
    葉維廉](http://tulips.ntu.edu.tw/screens/c_yip.html)

## 参见

  - [现代诗歌](../Page/现代诗歌.md "wikilink")
  - [现代诗人](../Page/现代诗人.md "wikilink")
  - 诗人列表

[Y](../Category/台灣作家.md "wikilink") [Y](../Category/台灣詩人.md "wikilink")
[Category:中山人](../Category/中山人.md "wikilink")
[Category:國立臺灣大學校友](../Category/國立臺灣大學校友.md "wikilink")
[Category:國立臺灣師範大學校友](../Category/國立臺灣師範大學校友.md "wikilink")
[Y](../Category/艾奧瓦大學校友.md "wikilink")
[Y](../Category/普林斯頓大學校友.md "wikilink")