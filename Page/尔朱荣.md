**爾朱荣**（），字**天宝**，北秀容（今[山西省](../Page/山西省.md "wikilink")[忻州市](../Page/忻州市.md "wikilink")[忻府区](../Page/忻府区.md "wikilink")）人，先世為[契胡](../Page/契胡.md "wikilink")（或稱[羯胡](../Page/羯胡.md "wikilink")）部酋長，可能為[鮮卑化的](../Page/鮮卑.md "wikilink")[羯人](../Page/羯人.md "wikilink")。祖先居於[爾朱川](../Page/爾朱川.md "wikilink")（今山西西北部流經[神池](../Page/神池縣.md "wikilink")、[五寨](../Page/五寨縣.md "wikilink")、[保德縣之](../Page/保德縣.md "wikilink")[朱家川](../Page/朱家川.md "wikilink")），故以爾朱為姓氏，後為[北魏权臣](../Page/北魏.md "wikilink")。

## 生平

爾朱榮有潔白美麗的容貌，愛好射獵，頗曉兵法。娶南安王[拓跋桢女](../Page/拓跋桢.md "wikilink")[北乡公主](../Page/北乡公主.md "wikilink")。在北魏後期政權中，尔朱荣憑借镇压[六镇之乱](../Page/六镇之乱.md "wikilink")，迅速壮大了其军事力量，擢為[遊擊將軍](../Page/遊擊將軍.md "wikilink")、[冠軍將軍](../Page/冠軍將軍.md "wikilink")、[平北將軍](../Page/平北將軍.md "wikilink")、[北道都督](../Page/北道都督.md "wikilink")。[鲜于脩礼谋反后](../Page/鲜于脩礼.md "wikilink")，尔朱荣带兵向东讨伐，再度进号[征东将军](../Page/征东将军.md "wikilink")、[右卫将军](../Page/右卫将军.md "wikilink")、署理[车骑将军](../Page/车骑将军.md "wikilink")，都督[-{并}-](../Page/并州.md "wikilink")[肆](../Page/肆州.md "wikilink")[汾](../Page/汾州.md "wikilink")[雲](../Page/云州_\(北魏\).md "wikilink")[廣](../Page/广州_\(北魏\).md "wikilink")[恒六州诸军事](../Page/恒州_\(北魏\).md "wikilink")，升任大都督，加[金紫光禄大夫](../Page/金紫光禄大夫.md "wikilink")\[1\]。

[魏孝明帝死后](../Page/魏孝明帝.md "wikilink")，[建義元年](../Page/建義.md "wikilink")（528年），他扶植[魏孝莊帝元子攸即位](../Page/魏孝莊帝.md "wikilink")，女儿[大尔朱氏又成为孝莊帝的皇后](../Page/大尔朱氏.md "wikilink")。并随后发动了[河阴之变](../Page/河阴之变.md "wikilink")，「沉胡太后及幼主于河」，誘騙王公百官二千多人至[河陰](../Page/河陰.md "wikilink")（今[河南](../Page/河南.md "wikilink")[孟津縣](../Page/孟津縣.md "wikilink")），以鐵騎包圍，盡殺之。从而完全掌控朝政，此時“京邑士子，十無一存，率皆逃竄，無敢出者，直衛空虛，官守廢曠。”同年八月，尔朱榮鎮壓了[葛榮領導的](../Page/葛榮.md "wikilink")[河北起義](../Page/河北起義.md "wikilink")。

永安三年九月二十五日（530年11月1日），孝莊帝伏兵明光殿，聲稱皇后[大尔朱氏生下了太子](../Page/大尔朱氏.md "wikilink")，派[元徽向尔朱榮報喜](../Page/元徽_\(城阳王\).md "wikilink")。元子攸听说尔朱荣进宫臉色緊張，連忙喝酒以遮掩。尔朱荣见到光祿少卿[魯安](../Page/魯安.md "wikilink")、典御[李侃晞從東廂門执刀闖入](../Page/李侃晞.md "wikilink")，便撲向元子攸。元子攸用藏在膝下的刀砍到尔朱荣，魯安等揮刀亂砍，殺爾朱榮與[元天穆等人](../Page/元天穆.md "wikilink")，又杀同时进宫的尔朱荣子[尔朱菩提等](../Page/尔朱菩提.md "wikilink")。

尔朱榮死後，爾朱榮的侄子[尔朱兆由并州出兵](../Page/尔朱兆.md "wikilink")[洛陽](../Page/洛陽.md "wikilink")，殺死孝莊帝，立[元恭為節閔帝](../Page/元恭.md "wikilink")。[高歡收納了尔朱榮軍隊二十余萬人](../Page/高歡.md "wikilink")，進佔[冀州](../Page/冀州.md "wikilink")。

## 家庭

### 高祖

  - [尔朱羽健](../Page/尔朱羽健.md "wikilink")，北魏散骑常侍、领民酋长

### 曾祖

  - 尔朱郁德，北魏领民酋长

### 祖父

  - [尔朱代勤](../Page/尔朱代勤.md "wikilink")，北魏代理宁南将军、肆州刺史、领民酋长、梁郡莊公

### 父亲

  - [尔朱新兴](../Page/尔朱新兴.md "wikilink")，北魏北魏散骑常侍、平北将军、秀容第一领民酋长、西河简王

### 子女

  - [大尔朱氏](../Page/大尔朱氏.md "wikilink")
  - [尔朱菩提](../Page/尔朱菩提.md "wikilink")
  - [尔朱义罗](../Page/尔朱义罗.md "wikilink")
  - [尔朱文殊](../Page/尔朱文殊.md "wikilink")
  - [尔朱文畅](../Page/尔朱文畅.md "wikilink")
  - [尔朱文略](../Page/尔朱文略.md "wikilink")
  - 尔朱氏，嫁[于长孺](../Page/于长孺.md "wikilink")
  - 尔朱氏，嫁北魏侍中、抚军将军、陈留王[元宽](../Page/元宽.md "wikilink")

## 參考文獻

### 史籍相關記載

  - 《[魏書](../Page/:s:魏書.md "wikilink")·[卷七十四·列傳第六十二](../Page/:s:魏書/卷74.md "wikilink")》
  - 《[北史](../Page/:s:北史.md "wikilink")·[卷四十八·列傳第三十六](../Page/:s:北史/卷048.md "wikilink")》
  - 《[資治通鑒](../Page/:s:資治通鑒.md "wikilink")·[卷第一百五十二](../Page/:s:資治通鑒/卷152.md "wikilink")》

## 外部链接

[E](../Category/北魏官员.md "wikilink") [R](../Category/爾朱姓.md "wikilink")
[E](../Category/南北朝遇刺身亡者.md "wikilink")
[R](../Category/北魏郡公.md "wikilink")
[Category:谥武](../Category/谥武.md "wikilink")

1.  《魏书·卷七十四·列传第六十二》：鲜于脩礼之反也，荣表东讨，复进号征东将军、右卫将军、假车骑将军、都督并肆汾广恒云六州诸军事，进为大都督，加金紫光禄大夫。