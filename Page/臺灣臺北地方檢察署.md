|               |
| :-----------: |
| **臺灣臺北地方檢察署** |
|     管轄區域      |
|      檢察長      |
|    上級檢察機關     |
|     所在地區      |
|      地址       |

[Taiwan_Taipei_District_Court_20061105.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_Taipei_District_Court_20061105.jpg "fig:Taiwan_Taipei_District_Court_20061105.jpg")、[法務部同建築辦公](../Page/法務部.md "wikilink")）\]\]
**臺灣臺北地方檢察署**，是[中華民國](../Page/中華民國.md "wikilink")[臺灣臺北地方法院的起訴](../Page/臺灣臺北地方法院.md "wikilink")[檢察機關](../Page/檢察機關.md "wikilink")，一般被簡稱為**臺北地檢署**、或**北檢**，與同在[大臺北地區的](../Page/大臺北地區.md "wikilink")[新北地檢署](../Page/新北地檢署.md "wikilink")、[士林地檢署合稱](../Page/士林地檢署.md "wikilink")**北三檢**。

## 沿革

自[日治時期](../Page/台灣日治時期.md "wikilink")[日本人引進西方法制開始](../Page/日本人.md "wikilink")，台灣司法機關長期院檢不分，因此臺北地方法院檢察署於日治時期的前身「臺北地方法院檢察局」係隸屬於當時的臺北地方法院；1949年[國民政府接收台灣](../Page/國民政府.md "wikilink")，同年11月1日接收該局業務，並將該局更名為「臺灣臺北地方法院檢察處」時，仍然維持院檢不分的體制；迄1980年7月1日，院檢始分隸。

院檢分隸後，1989年12月24日《法院組織法》修正，改名「臺灣臺北地方法院檢察署」，機關首長「首席檢察官」亦同時改稱為「檢察長」。

2018年2月8日，全國檢察署正式去掉「法院」二字，直接命名地方檢察署。\[1\]

## 管轄和分局

[Taipei_Duhuiqu1.png](https://zh.wikipedia.org/wiki/File:Taipei_Duhuiqu1.png "fig:Taipei_Duhuiqu1.png")

  - 管轄
      - 臺北市－[中山區](../Page/中山區_\(臺北市\).md "wikilink")、[大安區](../Page/大安區_\(臺北市\).md "wikilink")、[松山區](../Page/松山區_\(臺北市\).md "wikilink")、[信義區](../Page/信義區_\(臺北市\).md "wikilink")、[中正區](../Page/中正區_\(臺北市\).md "wikilink")、[萬華區](../Page/萬華區.md "wikilink")、[文山區](../Page/文山區.md "wikilink")，共7個區。
      - 新北市－[新店區](../Page/新店區.md "wikilink")、[石碇區](../Page/石碇區.md "wikilink")、[深坑區](../Page/深坑區.md "wikilink")、[坪林區](../Page/坪林區.md "wikilink")、[烏來區](../Page/烏來區.md "wikilink")，共5個區。
  - 轄內分局
      - 臺北市－[臺北市政府警察局及中山分局](../Page/臺北市政府警察局.md "wikilink")、松山分局、大安分局、信義分局、萬華分局、[中正第一分局](../Page/臺北市政府警察局中正第一分局.md "wikilink")、中正第二分局、文山第一分局、文山第二分局，共9個分局。
      - 新北市－[新北市政府警察局新店分局](../Page/新北市政府警察局.md "wikilink")，共1個分局。

(含[內政部警政署刑事警察局](../Page/內政部警政署刑事警察局.md "wikilink")、[內政部警政署航空警察局臺北分局](../Page/內政部警政署航空警察局.md "wikilink")、[內政部警政署鐵路警察局臺北分局](../Page/內政部警政署鐵路警察局.md "wikilink")、[內政部警政署國道公路警察局第一大隊](../Page/內政部警政署國道公路警察局.md "wikilink")
第六大隊 第九大隊 )

## 歷任檢察長

  - 楊治宇(2010年～2015年5月7日)
  - 蔡碧玉(2015年5月7日～2016年7月18日)
  - 邢泰釗(2016年7月18日至今)

[缩略图](https://zh.wikipedia.org/wiki/File:2nd_Office_of_Judicial_Yuan_and_Taipei_District_Prosecutors_Office_light_box_20180616.jpg "fig:缩略图")

## 爭議

### 限制記者

2015年7月24日，在[反課綱運動學生與民眾闖入教育部事件中](../Page/反課綱運動.md "wikilink")，警方於凌晨4時20分起非法逮捕在現場採訪的記者以及沒收其器具，之後北檢不但未儘速釐清案情以釋放記者，反而遲至晚上才無保請回三位堅持不交保的記者，並且在沒有合理的理由下，限制這三位記者的住居。[自由時報晚間](../Page/自由時報.md "wikilink")22時47分針對該限制住居處分發出質疑與抗議聲明，表示其記者廖振輝在現場進行新聞採訪工作，報導事件真相並盡監督政府工作本份，沒有逾越任何法令，並表示認為該裁定已嚴重影響廖振輝的自由權與工作權。\[2\]

## 参考文献

### 引用

### 来源

  - 《司法院史實紀要》，司法院 編，1985年.
  - 《法務部史實紀要》，法務部 編，1990年.
  - [王泰升](../Page/王泰升.md "wikilink")，《台灣法律史的建立》，台灣大學法學叢書，1997年.
  - 王泰升，《台灣日治時期法律改革》，聯經出版公司，1999年.
  - 檢察官改革協會，《台灣檢察制度變遷史》.

## 外部链接

  - [臺灣臺北地方檢察署](http://www.tpc.moj.gov.tw/mp.asp?mp=009)
  - Google
    Map：[臺灣臺北地方檢察署](https://www.google.com/maps/place/%E5%8F%B0%E7%81%A3%E5%8F%B0%E5%8C%97%E5%9C%B0%E6%96%B9%E6%B3%95%E9%99%A2%E6%AA%A2%E5%AF%9F%E7%BD%B2/@25.036794,121.511122,1032m/data=!3m1!1e3!4m6!1m3!3m2!1s0x3442a9a0f90c04c9:0xdd4432cae9d1ce18!2z5Y-w54Gj5Y-w5YyX5Zyw5pa55rOV6Zmi5qqi5a-f572y!3m1!1s0x3442a9a0f90c04c9:0xdd4432cae9d1ce18)

[Category:1901年台灣建立](../Category/1901年台灣建立.md "wikilink")
[Category:中華民國檢察機關](../Category/中華民國檢察機關.md "wikilink")
[法](../Category/臺北市中正區的政府機關.md "wikilink")

1.
2.