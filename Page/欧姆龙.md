[Omron_Blood_Pressure_Monitor.jpg](https://zh.wikipedia.org/wiki/File:Omron_Blood_Pressure_Monitor.jpg "fig:Omron_Blood_Pressure_Monitor.jpg")\]\]
**欧姆龙**（；）是[日本的一家](../Page/日本.md "wikilink")[自動化控制及](../Page/自動化控制.md "wikilink")[電子設備製造商](../Page/電子設備.md "wikilink")，總部位於[京都](../Page/京都.md "wikilink")。歐姆龍掌握世界領先的傳感與控制技術，產品涉及工業自動化控制、電子元器件、汽車電子、社會系統、健康醫療設備等領域。較為大眾熟知的產品包括電子[血壓計](../Page/血壓計.md "wikilink")、[計步器](../Page/計步器.md "wikilink")、[磅和電動](../Page/秤.md "wikilink")[牙刷等健康產品](../Page/牙刷.md "wikilink")。

## 历史

前身是1930年[立石一真在](../Page/立石一真.md "wikilink")[京都創立的](../Page/京都.md "wikilink")「彩光社」。歐姆龍公司是於1933年（[昭和](../Page/昭和.md "wikilink")8年）5月10日正式創業，時稱「立石電機製作所」；1948年（昭和23年）5月19日法人化，成立「立石電機股份有限公司」（日文：）；1990年為使公司名稱與其知名商標名稱一致，更名為「歐姆龍股份有限公司」（日文：）。歐姆龍的全球網絡遍及日本、[大中華區](../Page/大中華區.md "wikilink")、[亞太地區](../Page/亞太地區.md "wikilink")、[美洲及](../Page/美洲.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")。台灣歐姆龍股份有限公司則成立於1987年，主要是以工業自動化商品為主,其在台關係企業有:台灣歐姆龍健康事業股份有限公司、香港商歐姆龍電子部品有限公司台灣分公司，員工數各為100多人上下。

公司創始人立石一真，於1953年所制定，企業理念「企業需為社會貢獻」。

## 相關條目

  - [IEEE里程碑列表](../Page/IEEE里程碑列表.md "wikilink")
  - [歐姆龍環](../Page/歐姆龍環.md "wikilink")

## 参考文献

## 外部連絡

  - [歐姆龍全球網站](http://www.omron.com/)
  - [歐姆龍大中華區](http://www.omron.com.cn/)
  - [歐姆龍台灣](http://www.omron.com.tw/)

[Category:日本電子公司](../Category/日本電子公司.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:大阪证券交易所上市公司](../Category/大阪证券交易所上市公司.md "wikilink")
[Category:京都府公司](../Category/京都府公司.md "wikilink")
[Category:工業自動化](../Category/工業自動化.md "wikilink")
[Category:1948年成立的公司](../Category/1948年成立的公司.md "wikilink")
[Category:1948年日本建立](../Category/1948年日本建立.md "wikilink")