**落叶松属**（[學名](../Page/學名.md "wikilink")：******）是[松科下的一个属](../Page/松科.md "wikilink")，主要分布在[北半球](../Page/北半球.md "wikilink")。

## 種類

落葉松屬約有10-14種。

  - [歐洲落葉松](../Page/歐洲落葉松.md "wikilink")（*Larix decidua*）
  - [落葉松](../Page/落葉松.md "wikilink")（*Larix gmelinii*）
  - [西藏紅杉](../Page/西藏紅杉.md "wikilink")（*Larix griffithii*）
  - [喜馬拉雅紅杉](../Page/喜馬拉雅紅杉.md "wikilink")（*Larix himalaica*）
  - [日本落葉松](../Page/日本落葉松.md "wikilink")（*Larix kaempferi*）
  - [美洲落葉松](../Page/美洲落葉松.md "wikilink")（*Larix laricina*）
  - [高山落葉松](../Page/高山落葉松.md "wikilink")（*Larix lyallii*）
  - [四川紅杉](../Page/四川紅杉.md "wikilink")（*Larix mastersiana*）
  - [西部落葉松](../Page/西部落葉松.md "wikilink")（*Larix occidentalis*）
  - [紅杉](../Page/紅杉.md "wikilink")（*Larix potaninii*）
  - [華北落葉松](../Page/華北落葉松.md "wikilink")（*Larix principis-rupprechtii*）
  - [西伯利亞落葉松](../Page/西伯利亞落葉松.md "wikilink")（*Larix sibirica*）
  - [怒江紅杉](../Page/怒江紅杉.md "wikilink")（*Larix speciosa*）

## 參考資料

  - [Flora of China:
    *Larix*](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=117628)
    （落葉松屬）

[Category:落叶松属](../Category/落叶松属.md "wikilink")
[Category:落葉植物](../Category/落葉植物.md "wikilink")