**島式疊式月台**為[島式月台的一種](../Page/島式月台.md "wikilink")，亦稱為**島疊式月台**、**疊落島式月台**，為兩座以上島式月台分布於上下樓層的[月台形式](../Page/車站月台.md "wikilink")，大部分用於[轉乘站](../Page/轉乘站.md "wikilink")（包括採用[跨月台轉乘設計者](../Page/跨月台轉車站.md "wikilink")），例如：

## 台灣

[HALL_02.JPG](https://zh.wikipedia.org/wiki/File:HALL_02.JPG "fig:HALL_02.JPG")[中正紀念堂站的上下樓設計](../Page/中正紀念堂站.md "wikilink")\]\]

  - [臺北捷運](../Page/臺北捷運.md "wikilink")：[西門站](../Page/西門站.md "wikilink")、[中正紀念堂站](../Page/中正紀念堂站.md "wikilink")、[古亭站](../Page/古亭站.md "wikilink")、[東門站](../Page/東門站_\(台北市\).md "wikilink")
  - [台灣高速鐵路](../Page/台灣高速鐵路.md "wikilink")：[板橋站](../Page/板橋車站_\(臺灣\).md "wikilink")

## 香港

[Yau_Tong_Station.jpg](https://zh.wikipedia.org/wiki/File:Yau_Tong_Station.jpg "fig:Yau_Tong_Station.jpg")[油塘站的上下樓設計](../Page/油塘站.md "wikilink")\]\]

  - [港鐵](../Page/港鐵.md "wikilink")：[金鐘站](../Page/金鐘站.md "wikilink")、[北角站](../Page/北角站.md "wikilink")\[1\]、[太子站](../Page/太子站.md "wikilink")、[旺角站](../Page/旺角站.md "wikilink")、[油麻地站](../Page/油麻地站.md "wikilink")、[荔景站](../Page/荔景站.md "wikilink")\[2\]、[油塘站](../Page/油塘站.md "wikilink")、[調景嶺站](../Page/調景嶺站.md "wikilink")、未來[屯馬綫和](../Page/屯馬綫.md "wikilink")[東鐵綫的](../Page/東鐵綫過海延綫.md "wikilink")[紅磡站](../Page/紅磡站.md "wikilink")。

## 中国大陆

  - [上海地铁](../Page/上海地铁.md "wikilink")：[罗山路站](../Page/罗山路站.md "wikilink")
  - [深圳地铁](../Page/深圳地铁.md "wikilink")：[老街站](../Page/老街站.md "wikilink")\[3\]、[太安站](../Page/太安站.md "wikilink")\[4\]
  - [重庆轨道交通](../Page/重庆轨道交通.md "wikilink")：[两路口站](../Page/两路口站.md "wikilink")、[大坪站](../Page/大坪站.md "wikilink")
  - [广州地铁](../Page/广州地铁.md "wikilink")：[沙园站](../Page/沙园站.md "wikilink")
  - [杭州地铁](../Page/杭州地铁.md "wikilink")：[武林广场站](../Page/武林广场站.md "wikilink")\[5\]、[西湖文化广场站](../Page/西湖文化广场站.md "wikilink")\[6\]
  - [南宁轨道交通](../Page/南宁轨道交通.md "wikilink")：[朝阳广场站](../Page/朝阳广场站.md "wikilink")
  - [长沙地铁](../Page/长沙地铁.md "wikilink")：[侯家塘站](../Page/侯家塘站.md "wikilink")
  - [武汉地铁](../Page/武汉地铁.md "wikilink")：[洪山广场站](../Page/洪山广场站.md "wikilink")、[钟家村站](../Page/钟家村站_\(武汉地铁\).md "wikilink")、[香港路站](../Page/香港路站.md "wikilink")（3、7号线）
  - [昆明轨道交通](../Page/昆明轨道交通.md "wikilink")：[环城南路站](../Page/环城南路站.md "wikilink")

## 日本

  - [東京地下鐵](../Page/東京地下鐵.md "wikilink")：[赤坂見附站](../Page/赤坂見附站.md "wikilink")、[要町站](../Page/要町站.md "wikilink")、[千川站](../Page/千川站.md "wikilink")、[東新宿站](../Page/東新宿站.md "wikilink")
  - [京王電鐵](../Page/京王電鐵.md "wikilink")：[調布站](../Page/調布站.md "wikilink")
  - [小田急電鐵](../Page/小田急電鐵.md "wikilink")：[下北澤站](../Page/下北澤站.md "wikilink")(2018年3月後)
  - [京成電鐵](../Page/京成電鐵.md "wikilink")：[青砥站](../Page/青砥站.md "wikilink")
  - [京濱急行電鐵](../Page/京濱急行電鐵.md "wikilink")：[京急蒲田站](../Page/京急蒲田站.md "wikilink")
  - [大阪市營地下鐵](../Page/大阪市營地下鐵.md "wikilink")：[宇宙廣場車站](../Page/宇宙廣場車站.md "wikilink")
  - [福岡市交通局](../Page/福岡市交通局.md "wikilink")：[中洲川端站](../Page/中洲川端站.md "wikilink")

## 马来西亚

  - [双溪毛糯－加影线](../Page/双溪毛糯－加影线.md "wikilink")：[桂莎白沙罗站](../Page/桂莎白沙罗站.md "wikilink")

## 美國

  - [市場街地鐵隧道](../Page/市場街地鐵隧道.md "wikilink")\[7\]：[內河碼頭站](../Page/內河碼頭站.md "wikilink")（）、[蒙哥馬利街站](../Page/蒙哥馬利街站.md "wikilink")（）、[跑華街站](../Page/跑華街站.md "wikilink")（）、[市政中心/聯合國廣場站](../Page/市政中心/聯合國廣場站.md "wikilink")（）
  - [紐約地鐵](../Page/紐約地鐵.md "wikilink")[蘇特芬林蔭路-射手大道-JFK機場車站
    (射手大道線)](../Page/蘇特芬林蔭路-射手大道-JFK機場車站_\(射手大道線\).md "wikilink")

## 加拿大

  - [多倫多地鐵](../Page/多倫多地鐵.md "wikilink")：[聖佐治站](../Page/聖佐治站.md "wikilink")（St.
    George Station）

## 註解

<div class="references-small">

<references />

</div>

## 相關條目

  - [島式月台](../Page/島式月台.md "wikilink")
  - [分离式月台](../Page/分离式月台.md "wikilink")
  - [側式月台](../Page/側式月台.md "wikilink")
  - [側式疊式月台](../Page/側式疊式月台.md "wikilink")
  - [同月台平行轉乘](../Page/同月台平行轉乘.md "wikilink")

[Category:鐵路車站](../Category/鐵路車站.md "wikilink")

1.  北角站興建時為側式疊式車站，隨[觀塘線延伸而將車站擴建成島式疊式車站](../Page/觀塘線.md "wikilink")

2.  荔景站興建時為島式車站，隨興建[東涌線而將車站擴建成島式疊式車站](../Page/東涌線.md "wikilink")

3.  老街站興建時為側式疊式月台，興建[3號綫時將月台擴建](../Page/深圳地鐵3號綫.md "wikilink")，形式島式疊式月台，而原來的側式疊式月台則被廢棄。

4.  於2016年10月28日隨下層[7號綫月台開通而成為島式疊式月台](../Page/深圳地鐵7號綫.md "wikilink")

5.  [3號綫月台尚未開通](../Page/杭州地鐵3號綫.md "wikilink")

6.
7.  [舊金山灣區捷運系統](../Page/舊金山灣區捷運系統.md "wikilink")、[舊金山輕軌共用](../Page/舊金山輕軌.md "wikilink")