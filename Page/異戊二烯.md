**異戊二烯**，[IUPAC名称](../Page/IUPAC名称.md "wikilink")**2-甲基-1,3-丁二烯**，是一種共軛[二烯烃](../Page/二烯烃.md "wikilink")，[分子式为C](../Page/分子式.md "wikilink")<sub>5</sub>H<sub>8</sub>。对于天然产物[萜类化合物](../Page/萜类化合物.md "wikilink")，就是以分子中含有的异戊二烯单元个数分类的。由一个异戊二烯单元组成的萜称为[半萜](../Page/半萜.md "wikilink")，两个单元组成的称为[单萜](../Page/单萜.md "wikilink")，依此类推。

## 性質

異戊二烯本身是為不溶於水的無色液體，但能與[乙醇](../Page/乙醇.md "wikilink")、[乙醚](../Page/乙醚.md "wikilink")、[丙酮和](../Page/丙酮.md "wikilink")[苯等溶劑混溶](../Page/苯.md "wikilink")。且易自身[聚合](../Page/聚合.md "wikilink")，也易與別的[不飽和化合物共聚合](../Page/不飽和.md "wikilink")。若在[齐格勒-納塔催化劑下聚合](../Page/齐格勒-納塔催化劑.md "wikilink")，則會產生[聚異戊二烯](../Page/聚異戊二烯.md "wikilink")，也就是我們說的[天然橡膠](../Page/天然橡膠.md "wikilink")。具有優秀的彈性、張力等。少量異戊二烯與[異丁烯共聚](../Page/異丁烯.md "wikilink")，會產生[丁基橡膠](../Page/丁基橡膠.md "wikilink")。

## 污染爭議

有一些[植物中的](../Page/植物.md "wikilink")[萜烯會產生成異戊二烯](../Page/萜烯.md "wikilink")。所謂的異戊二烯和單一[芬多精大多都是由樹木所產生](../Page/芬多精.md "wikilink")。屬於揮發性有機[碳氫化合物](../Page/碳氫化合物.md "wikilink")(VOC)，且具有一定的[抗氧化](../Page/抗氧化.md "wikilink")、殺菌並維護樹木本身的健康，一天當中，又以中午時段排放量較大。一般我們所知道的[甲醛](../Page/甲醛.md "wikilink")、[三氯乙烯](../Page/三氯乙烯.md "wikilink")、[甲苯](../Page/甲苯.md "wikilink")、[乙苯等都屬於](../Page/乙苯.md "wikilink")[揮發性有機物](../Page/揮發性有機物.md "wikilink")，而植物所排放的[芬多精其實也都是有機化合物](../Page/芬多精.md "wikilink")。前提是，異戊二烯必須要在[光合作用下產生](../Page/光合作用.md "wikilink")，當氣溫達35℃以上，樹木就會釋放出大量的異戊二烯和一種單一[芬多精](../Page/芬多精.md "wikilink")，同時也促進[臭氧的產生](../Page/臭氧.md "wikilink")。當環境中異戊二烯排放量增加到兩倍時，[臭氧平均濃度增加](../Page/臭氧.md "wikilink")4ppb。

然而，事實上已有證據顯示，相互作用後，產生[臭氧](../Page/臭氧.md "wikilink")，會形成[空氣污染](../Page/空氣污染.md "wikilink")。但對人體有益或有害尚且未得到[科學證實](../Page/科學.md "wikilink")。

## 工業用途

在常見的[工業上](../Page/工業.md "wikilink")，會經由異戊烷或者[甲基丁烯](../Page/甲基丁烯.md "wikilink")[催化去氫來生產](../Page/催化.md "wikilink")、也會從輕油裂化生產[乙烯所得的副產物中取出](../Page/乙烯.md "wikilink")、以及用[丙烯合成](../Page/丙烯.md "wikilink")。[丙烯在三](../Page/丙烯.md "wikilink")[烷基鋁下二聚](../Page/烷基鋁.md "wikilink")，所產生的2-甲基-1-戊烯在[酸性催化劑下](../Page/酸性.md "wikilink")[異構化為](../Page/異構化.md "wikilink")2-甲基-2-戊烯，分解成異戊二烯和[甲烷](../Page/甲烷.md "wikilink")。

## 参见

  - [天然橡膠](../Page/天然橡膠.md "wikilink")

## 參考來源

  - Reimann, S., Calanca, P. and Hofer, P., The anthropogenic
    contribution to isoprene concentrations in a rural atmosphere.
    Atmospheric Environment 34 109-115(2000).
  - Sharkey T. D., and Singsaas E. L., Why plants emit isoprene. Nature
    374:769(1995).
  - 吳金村，行政院農委會-林業研究專訊，森林釋出異戊二烯對大氣品質之影響，2004

[Category:二烯烃](../Category/二烯烃.md "wikilink")
[Category:单体](../Category/单体.md "wikilink")
[Category:IARC第2B类致癌物质](../Category/IARC第2B类致癌物质.md "wikilink")
[Category:半萜](../Category/半萜.md "wikilink")