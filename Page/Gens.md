**Gens**一套針對[電視遊戲機](../Page/電視遊戲機.md "wikilink")－[世嘉](../Page/世嘉.md "wikilink")[Mega
Drive](../Page/Mega_Drive.md "wikilink")（MD）而發展的[家用机模拟器](../Page/家用机模拟器.md "wikilink")[軟件](../Page/軟件.md "wikilink")，其功能是讓使用者配合遊戲資料檔（Rom）在個人電腦上亦能執行到Mega
Drive的遊戲，目前已有很高的開發完成度，支援大部份Mega
Drive遊戲。2006年后Gens虽不断更新，不过仅支持32位的Linux和BSD。

**Gens32**則是Gens的延伸版本，是以Gens作者開放的Gens[原始碼為基礎的](../Page/原始碼.md "wikilink")[模擬器](../Page/模擬器.md "wikilink")。Gens32支援了32[位元色彩模式](../Page/位元.md "wikilink")、[7z格式及](../Page/7z.md "wikilink")[LDU系統](../Page/LDU.md "wikilink")。Gens32作者為來自[中國大陸的Dark](../Page/中國大陸.md "wikilink")
Dancer，使得Gens32成為目前唯一致力提昇中文遊戲模擬度的模擬器。

## 參看

  - [模拟器](../Page/模拟器.md "wikilink")
  - [家用机模拟器](../Page/家用机模拟器.md "wikilink")
  - [MD](../Page/Mega_Drive.md "wikilink")
  - [Kega Fusion](../Page/Kega_Fusion.md "wikilink")

## 外部链接

  - [官方網站](http://www.gens.me/)
  - [开发项目页sourceforge](http://sourceforge.net/projects/gens/)

[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:遊戲機模擬器](../Category/遊戲機模擬器.md "wikilink")