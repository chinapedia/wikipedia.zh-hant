**PubChem**，即**有機小分子生物活性數據**，是一種化學模組的[資料庫](../Page/資料庫.md "wikilink")，目前由[美國國家生物技術信息中心維護](../Page/美國國家生物技術信息中心.md "wikilink")。PubChem可經由網站直接存取，數以百萬計的化學組成資料集可經由FTP免費下載。

## 外部連結

  - [PubChem homepage](http://pubchem.ncbi.nlm.nih.gov)
  - [PubChem searchpage](http://pubchem.ncbi.nlm.nih.gov/search/)
  - [PubChem FAQ](http://pubchem.ncbi.nlm.nih.gov/help.html#faq)
  - [PubChem Help](http://pubchem.ncbi.nlm.nih.gov/help.html)
  - [PubChemSR Search
    Aid](https://web.archive.org/web/20071227115807/http://cheminfo.informatics.indiana.edu/PubChemSR/)
  - [PubChem for
    Newbies](http://depth-first.com/articles/2007/09/26/pubchem-for-newbies/)

[Category:化學資料庫](../Category/化學資料庫.md "wikilink")