[HK_Central_120_Des_Voeux_Road_Public_Bank_Centre.JPG](https://zh.wikipedia.org/wiki/File:HK_Central_120_Des_Voeux_Road_Public_Bank_Centre.JPG "fig:HK_Central_120_Des_Voeux_Road_Public_Bank_Centre.JPG")
[Kuala_Lumpur_Malaysia_Menara-Public-Bank-01.jpg](https://zh.wikipedia.org/wiki/File:Kuala_Lumpur_Malaysia_Menara-Public-Bank-01.jpg "fig:Kuala_Lumpur_Malaysia_Menara-Public-Bank-01.jpg")
**大眾銀行有限公司**（，）依據[市值為](../Page/市值.md "wikilink")[馬來西亞第二大](../Page/馬來西亞.md "wikilink")[銀行](../Page/銀行.md "wikilink")，僅次於[馬來亞銀行](../Page/馬來亞銀行.md "wikilink")；總資產則為第三位，次於馬來亞銀行及[联昌国际银行](../Page/联昌国际银行.md "wikilink")\[1\]\[2\]。屬於一間[华人](../Page/马来西亚华人.md "wikilink")[資](../Page/資本.md "wikilink")[銀行](../Page/銀行.md "wikilink")，於1966年成立，創辦人為[鄭鴻標](../Page/鄭鴻標.md "wikilink")。1967年，該行在當時的[吉隆坡證券交易所上市](../Page/吉隆坡證券交易所.md "wikilink")，於1990年代开始向[香港](../Page/香港.md "wikilink")、[斯里兰卡](../Page/斯里兰卡.md "wikilink")、[越南](../Page/越南.md "wikilink")、[老挝及](../Page/老挝.md "wikilink")[柬埔寨扩展业务](../Page/柬埔寨.md "wikilink")。

大眾銀行目前擁有超過18,000名員工，國內外分行259間，其中[香港](../Page/香港.md "wikilink")83間、[柬埔寨](../Page/柬埔寨.md "wikilink")28間、[越南](../Page/越南.md "wikilink")7間、[寮國](../Page/寮國.md "wikilink")4間、[中國](../Page/中國.md "wikilink")3間及[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")3間。\[3\]

## 香港

以往大眾銀行在香港只有一間分行，為進一步擴展香港業務，該行透過旗下的[大眾金融控股收購](../Page/大眾金融控股.md "wikilink")[亞洲商業銀行的](../Page/亞洲商業銀行_\(香港\).md "wikilink")[股權](../Page/股權.md "wikilink")，並於2006年6月30日亞洲商業銀行改名為[大眾銀行（香港）有限公司](../Page/大眾銀行（香港）.md "wikilink")。

## 历史

大众银行的第一家分行于1966年8月6日在吉隆坡开业。这家银行之所以命名为「大众银行」是因为创办人郑鸿标想鼓励公众到他的银行储蓄。初期大众银行通过广告促销，鼓励民众储蓄，使马来西亚成为世界上储蓄率最高的国家之一。大众银行开业5个月后便在[马六甲市和](../Page/马六甲市.md "wikilink")[怡保设立分行](../Page/怡保.md "wikilink")，并在之后扩张至12家分行。1976年，大众银行正式在吉隆坡股票交易所上市。\[4\]

1978年，[马来西亚国家银行开放自由浮动利率](../Page/马来西亚国家银行.md "wikilink")，大众银行提供比其他银行高出2%存款利息的政策吸引了大量客户，使大众银行在1980年存款成长率达到其他马来西亚银行业成长率的4倍。大众银行在上世纪80年代逐步扩展其他的金融业务，传统商业银行之外，大众银行还相继进入融资、证券与期货经纪、单位信托管理、投资控股等多个领域，成为一站式的金融企业。\[5\]

1989年底，大众银行的资产总额达34.3亿美元，税后盈利达3,105万美元，在[马来西亚银行中居第三位](../Page/马来西亚银行列表.md "wikilink")。大众银行在马来西亚经济低潮的1987年和1997年延续每年获利，并在1998年，达到11亿8,380万[令吉的盈利](../Page/令吉.md "wikilink")。2001年，大众银行的净盈利是7.2亿[令吉](../Page/令吉.md "wikilink")，并在4年后增长至15亿[令吉](../Page/令吉.md "wikilink")，可谓马来西亚的“最佳银行”。2006年，大众银行获得香港《财资》杂志颁发“2006年大马最佳本地银行”，并在该年2月，以45亿港元并购[亚洲商业银行](../Page/亚洲商业银行.md "wikilink")，也是近年来最高的香港银行并购价格之一。\[6\]

2006年6月，亚洲商业银行正式易名为大众银行（香港），旗下12家分行也纷纷更改商标。亚洲商银在[深圳市设有分行](../Page/深圳市.md "wikilink")，在[上海市](../Page/上海市.md "wikilink")、[沈阳市和](../Page/沈阳市.md "wikilink")[台湾也都设有办事处](../Page/台湾.md "wikilink")。这使得大众银行在香港的分行增至逾50家，深圳则有2家。\[7\]

2007年，大众银行推出了首支中国基金，名为“PB中国太平洋证券基金”，该基金的主要向中国股市投资，并在[日本](../Page/日本.md "wikilink")、[韩国等地都有进行适当配置](../Page/韩国.md "wikilink")，1年后又发行了一支中国－[东盟股票基金](../Page/东盟.md "wikilink")，投资中国和东盟国家的股票。2009年，中国银行和大众银行宣布缔结人民币贸易结算服务合作协议以方便马来西亚以[人民币结算与中国的贸易](../Page/人民币.md "wikilink")。这项合作使大众银行成为中国银行（马来西亚）以外提供人民币贸易结算服务的首家马来西亚银行。
大众银行也陆续在东南亚和南亚国家如[柬埔寨](../Page/柬埔寨.md "wikilink")、[越南](../Page/越南.md "wikilink")、[寮国](../Page/寮国.md "wikilink")、[斯里兰卡等设立分行](../Page/斯里兰卡.md "wikilink")。现今拥有超过18,000名员工，2014年底其资产规模达345.72亿[令吉](../Page/令吉.md "wikilink")，表现卓越。\[8\]

2017年7月，大眾銀行創辦人兼集團主席[鄭鴻標宣布於](../Page/鄭鴻標.md "wikilink")2019年1月1日退下主席職務\[9\]，因其一儿三女無意接掌公司，引起市場關注。\[10\]

## 參見

  - [马来西亚银行列表](../Page/马来西亚银行列表.md "wikilink")
  - [大眾金融控股](../Page/大眾金融控股.md "wikilink")

## 参考

## 外部連結

  - [PBEBank.com](http://www.pbebank.com/)

[Category:1966年成立的公司](../Category/1966年成立的公司.md "wikilink")
[Category:馬來西亞銀行](../Category/馬來西亞銀行.md "wikilink")
[Category:華資銀行](../Category/華資銀行.md "wikilink")
[Category:柬埔寨銀行](../Category/柬埔寨銀行.md "wikilink")

1.

2.

3.
4.

5.
6.
7.
8.
9.

10.