**用戶創新**是指由[消費者和](../Page/消費者.md "wikilink")[最終用戶](../Page/最終用戶.md "wikilink")，而不是由[制造商發展出的](../Page/制造商.md "wikilink")[創新](../Page/創新.md "wikilink")。[麻省理工學院的](../Page/麻省理工學院.md "wikilink")[埃里克·馮·希貝爾](../Page/埃里克·馮·希貝爾.md "wikilink")（[:en:Eric
von
Hippel](../Page/:en:Eric_von_Hippel.md "wikilink")）教授發現，大多數產品和服務實際上是由用戶發展出來的

用戶將想法告訴制造商。這是因為[產品是為滿足最廣泛的需求而開發出來的](../Page/產品.md "wikilink")；當個別用戶遇到了大多數消費者沒有遇到的問題，這些用戶別無選擇，只有發展出現有產品的修改，或全新的產品，來解決這些問題。

通常用戶創新將會與制造商分享創意，期待制造商能夠生產出這些產品。用戶創新逐步得到重視，正是體現了[信息通訊技術發展下創新由生產範式向服務範式的轉變](../Page/信息通訊技術.md "wikilink")\[1\]。

信息通信技術（ICT）特別是[移動技術的融合與發展推動了社會形態的深刻變革](../Page/移動技術.md "wikilink")，推動了[知識社會的形成和](../Page/知識社會.md "wikilink")[創新形態的嬗變](../Page/創新.md "wikilink")，

生活、工作在社會中的用戶和大眾成為創新的主體更深切的參與創新進程，傳統意義的[實驗室的邊界以及創新活動的邊界也隨之消融](../Page/實驗室.md "wikilink")。『以生產者為中心的創新模式正在向以用戶為中心的創新模式轉變，創新正在經歷從生產範式向服務範式轉變的過程，正在經歷一個民主化的進程』\[2\]。以實驗室科技研發人員為中心的傳統科技創新活動面臨挑戰，以用戶為中心、社會為舞臺的面向知識社會、以人為本的下一代創新模式，即[創新2.0模式正逐步顯現其生命力和潛在價值](../Page/創新2.0.md "wikilink")，用戶創新成為科技創新活動的重要戰場。

面向知識社會的用戶創新是用戶參與的以用戶為中心的創新，特別關注[用戶體驗](../Page/用戶體驗.md "wikilink")，用戶體驗也因此被稱為創新2.0模式的精髓。

1986年，[埃里克·馮·希貝爾引入了](../Page/埃里克·馮·希貝爾.md "wikilink")[領先用戶方法](../Page/領先用戶.md "wikilink")，可以用於系統地研究用戶創新，以便在[新產品開發中應用](../Page/新產品開發.md "wikilink")。

## 相關書籍

  - 《客戶才是你的創新總監》（The Sources of Innovation）譯者：柳卸林、陳道斌等 出版社：博雅書屋
    出版日期：2011年7月22日 ISBN 9789866098192

## 參考文獻

## 參見

  - [創新2.0](../Page/創新2.0.md "wikilink")
  - [用戶體驗](../Page/用戶體驗.md "wikilink")
  - [眾包](../Page/眾包.md "wikilink")
  - [專業餘](../Page/專業餘.md "wikilink")

## 外部連結

  - [New York Times on User
    Innovation](http://dynamist.com/articles-speeches/nyt/innovation.html)
  - [Eric Von Hippel's books on user
    innovation](http://web.mit.edu/evhippel/www/books.htm)
  - [Innovation 2.0: Towards a service
    Paradigm](http://www.mgov.cn/innovation/)

[Category:創新](../Category/創新.md "wikilink")

1.  [Innovation 2.0 as a Paradigm Shift: Comparative Analysis of Three
    Innovation Modes](http://www.mgov.cn/MASS_2009_Paper.pdf)
2.  宋剛,張楠.[創新2.0：知識社會環境下的創新民主化](http://www.mgov.cn/complexity/complexity16.htm),
    *中國軟科學*, 10, 2009