[ankh.svg](https://zh.wikipedia.org/wiki/File:ankh.svg "fig:ankh.svg")

|                                         |
| --------------------------------------- |
| **聖書體寫法**                               |
| <hiero>S34-n:Aa1 \! or S34</hiero>\[1\] |

[Anch.png](https://zh.wikipedia.org/wiki/File:Anch.png "fig:Anch.png")
[Ankh-Royal_Ontario_Museum.jpg](https://zh.wikipedia.org/wiki/File:Ankh-Royal_Ontario_Museum.jpg "fig:Ankh-Royal_Ontario_Museum.jpg")在位时期
(前1508–前1458)的壁画，[皇家安大略博物馆](../Page/皇家安大略博物馆.md "wikilink")。\]\]

**生命之符**（英语：**Ankh**；符號：U+2625
<span style="font-size:200%"></span>），又稱**安卡**、**安可**，是埃及象形文字（又稱[聖書體](../Page/聖書體.md "wikilink")）的字母，代表詞語*ʿnḫ*，解作[生命](../Page/生命.md "wikilink")。部份[古埃及的神祇手持生命之符的圈](../Page/古埃及.md "wikilink")，或兩手各執生命之符，交叉雙手放於胸前。[拉丁文稱此符作](../Page/拉丁文.md "wikilink")*crux
ansata*，「有柄的[十字](../Page/十字.md "wikilink")」之意。

對[古埃及學者來說](../Page/古埃及學.md "wikilink")，其形象有何含意仍是個謎。部分專家認為它是[子宮形像](../Page/子宮.md "wikilink")，但此說法未被完全接受。英国的埃及古物学者[艾伦·加德纳](../Page/艾伦·加德纳.md "wikilink")（Alan
Gardiner）推測它是便鞋扣帶的形像，其圈讓腳踝套入，因為便鞋扣帶的寫法也是*ʿnḫ*（但可能讀音不同）。

生命之符在古埃及的墓地和藝術中常常出現。在畫像中，賦與[木乃伊生命的神祇的手指通常繪有生命之符](../Page/木乃伊.md "wikilink")。古埃及人以生命之符作為護身符，有時會加上另外兩個分別代表「力量」和「健康」的字母。鏡子也通常製作成生命之符的形狀。

在[Unicode中](../Page/Unicode.md "wikilink")，生命之符的編碼是U+2625（<span style="font-size:120%"></span>）。

在[流行文化中](../Page/流行文化.md "wikilink")，生命之符被廣泛應用，代表悠久的歷史、神秘的力量，也是古埃及的象徵符號。

在卡牌遊戲[遊戲王中](../Page/遊戲王.md "wikilink")，魔法卡\[死者蘇生\]上的圖案就是生命之符。

## 参考文献

## 外部連結

  - [生命之符的介紹](http://www.touregypt.net/featurestories/ankh.htm)

## 参见

  - [十字](../Page/十字.md "wikilink")
  - [聖書體](../Page/聖書體.md "wikilink")

{{-}}

[Category:古埃及宗教符号](../Category/古埃及宗教符号.md "wikilink")
[Category:十字](../Category/十字.md "wikilink")
[Category:煉金術](../Category/煉金術.md "wikilink")

1.  Collier, Mark and Manley, Bill. *How to Read Egyptian Hieroglyphs:
    Revised Edition* pg 23. Berkeley: University of California Press,
    1998.