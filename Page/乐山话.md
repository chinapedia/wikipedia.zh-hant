**樂山話**（[四川话](../Page/四川话.md "wikilink")[拼音](../Page/四川话拼音.md "wikilink")：<font face="Lucida Sans Unicode">No<sup>2</sup>san<sup>1</sup>hua<sup>4</sup></font>；本地发音：<font face="Lucida Sans Unicode">\[\]</font>）是指居住在[中国](../Page/中国.md "wikilink")[四川省](../Page/四川省.md "wikilink")[乐山市城区内的乐山本地人所使用的方言](../Page/乐山市.md "wikilink")，是[汉语](../Page/汉语.md "wikilink")[西南官话](../Page/西南官话.md "wikilink")[岷江小片的代表性方言](../Page/岷江小片.md "wikilink")，也是[四川話中極具代表性的方言之一](../Page/四川話.md "wikilink")。乐山话保留入声，共拥有5个声调，音韵、词汇等方面也独具特色，很多傳統的說法即使是成都人或者重慶人也未必可以完全聽懂。虽然广义的来说，乐山话也可以指乐山市所辖各区县所有乐山本地人所使用的方言，但由于乐山市境内的岷江话方言音韵多样性很高，同时境内还拥有大量四川话[仁富小片的使用者](../Page/仁富小片.md "wikilink")。因而学术上所指的乐山话，一般指乐山市内的岷江片。

## 音韵

### 声调

同其它的岷江话方言类似，乐山话的调类总共有[阴平](../Page/阴平.md "wikilink")、[阳平](../Page/阳平.md "wikilink")、[上声](../Page/上声.md "wikilink")、[去声](../Page/去声.md "wikilink")、[入声五类](../Page/入声.md "wikilink")，入声韵与舒声韵有明显区别，但无喉塞音。乐山话的四声调值依次为阴平55、阳平21、上声52、去声224、入声3\[1\]。

### 声母

乐山话的声母共有20个（包括零声母），乐山话的声母如下表所示\[2\]：

<table>
<thead>
<tr class="header">
<th></th>
<th><p><strong>双唇</strong></p></th>
<th><p><strong>唇齿</strong></p></th>
<th><p><strong>齿后</strong></p></th>
<th><p><strong>齿龈</strong></p></th>
<th><p><strong>硬腭</strong></p></th>
<th><p><strong>软腭</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>塞音</p></td>
<td><p>不送气</p></td>
<td><p><br />
贝</p></td>
<td></td>
<td></td>
<td><p><br />
得</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>送气</p></td>
<td><p><br />
配</p></td>
<td></td>
<td></td>
<td><p><br />
套</p></td>
<td></td>
<td><p><br />
可</p></td>
</tr>
<tr class="odd">
<td><p>塞擦音</p></td>
<td><p>不送气</p></td>
<td></td>
<td></td>
<td><p><br />
早</p></td>
<td></td>
<td><p><br />
价</p></td>
</tr>
<tr class="even">
<td><p>送气</p></td>
<td></td>
<td></td>
<td><p><br />
草</p></td>
<td></td>
<td><p><br />
巧</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鼻音</p></td>
<td><p><br />
没</p></td>
<td></td>
<td><p><br />
路</p></td>
<td></td>
<td></td>
<td><p><br />
我</p></td>
</tr>
<tr class="even">
<td><p>擦音</p></td>
<td><p>清</p></td>
<td></td>
<td><p><br />
发</p></td>
<td><p><br />
速</p></td>
<td></td>
<td><p><br />
小</p></td>
</tr>
<tr class="odd">
<td><p>浊</p></td>
<td></td>
<td><p><br />
五</p></td>
<td><p><br />
认</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>零声母</p></td>
<td><p><br />
衣</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 韵母

乐山话的韵母系统与其它岷江话方言类似，但与成渝话有很大差异。其拥有一套独立的仅用于入声的[紧元音韵母](../Page/元音松紧对立.md "wikilink")，可以利用主要元音的松紧对立来区分入声与舒声字。乐山话的韵母如下表所示，最上为拼音，加（q）者为相配入声韵，\[
\]中为国际音标，最下为例字\[3\]\[4\]\[5\]：

<table>
<thead>
<tr class="header">
<th></th>
<th><p><strong>开尾</strong></p></th>
<th><p><strong>元音尾</strong></p></th>
<th><p><strong>鼻音尾</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/开口呼.md" title="wikilink">开口呼</a></p></td>
<td><p><strong>-i</strong><sub>(q)</sub><br />
<br />
磁;尺</p></td>
<td><p><strong>a</strong><sub>(q)</sub><br />
<br />
茶;拍撤察</p></td>
<td><p><strong>e</strong><sub>q</sub><br />
<br />
各格</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/合口呼.md" title="wikilink">合口呼</a></p></td>
<td><p><strong>u</strong><sub></sub><br />
<br />
姑</p></td>
<td><p><strong>ua</strong><sub>(q)</sub><br />
[6]<br />
瓜;刮</p></td>
<td><p><strong>o</strong><sub>(q)</sub><br />
<br />
歌鍋;國郭骨</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/齐齿呼.md" title="wikilink">齐齿呼</a></p></td>
<td><p><strong>i</strong><sub>(q)</sub><br />
<br />
祭借;吉</p></td>
<td><p><strong>ia</strong><sub>(q)</sub><br />
<br />
架;甲</p></td>
<td><p><strong>io</strong><sub>q</sub><br />
<br />
卻屈缺</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/撮口呼.md" title="wikilink">撮口呼</a></p></td>
<td><p><strong>ü</strong><br />
<br />
渠瘸</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 區分

  - [苏杨片区口音](../Page/苏杨片区口音.md "wikilink")，“**苏稽话**”的正确叫法，是**乐山话**的一种。

## 注释

[Category:四川话](../Category/四川话.md "wikilink")
[Category:乐山文化](../Category/乐山文化.md "wikilink")

1.

2.
3.
4.  赖先刚（2004年第6期），《谈谈乐山方言语音的偏移》，天府新论

5.  甄尚灵（1983年第4期），《四川方言的鼻尾韵》，方言

6.  老派与相混，见：