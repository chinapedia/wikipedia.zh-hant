《**納蘭與康熙**》是[中國大陸導演](../Page/中國大陸.md "wikilink")[尤小剛的](../Page/尤小剛.md "wikilink")[清朝秘史四部曲中的最後一部曲](../Page/清朝.md "wikilink")，此劇圍繞着年少時的[康熙的功績](../Page/康熙.md "wikilink")、好友及情人的鬥爭。此劇集兩岸三地、老中青三代的演員、如：[夏雨](../Page/夏雨.md "wikilink")、[鍾漢良](../Page/鍾漢良.md "wikilink")、[胡靜](../Page/胡靜.md "wikilink")、[蔡琳等](../Page/蔡琳.md "wikilink")。2006年12月25日至2007年2月17日(共41集)在[香港亞洲電視](../Page/香港亞洲電視.md "wikilink")[本港台首次播映](../Page/本港台.md "wikilink")。

## 劇情概梗

講述少年時的[康熙的一生的故事](../Page/康熙.md "wikilink")。

## 主要演員

|                                        |                                          |                                                            |
| -------------------------------------- | ---------------------------------------- | ---------------------------------------------------------- |
| **演员**                                 | **角色**                                   | **关系**                                                     |
| [夏　雨](../Page/夏雨_\(大陆\).md "wikilink") | [愛新覺羅玄燁](../Page/康熙帝.md "wikilink")      | 大清皇帝，顺治帝第三子                                                |
| [蔡　琳](../Page/蔡琳.md "wikilink")        | [赫舍里氏](../Page/孝誠仁皇后.md "wikilink")      | 康熙帝的皇后                                                     |
| [丁志強](../Page/丁志強.md "wikilink")       | [愛新覺羅胤礽](../Page/愛新覺羅胤礽.md "wikilink")   | 大清皇太子，康熙帝和皇后之子                                             |
| [鍾漢良](../Page/鍾漢良.md "wikilink")       | [納蘭性德](../Page/納蘭性德.md "wikilink")       | 明珠之子，满洲第一才子，康熙帝伴读兼御前侍卫                                     |
| [胡　靜](../Page/胡靜.md "wikilink")        | 青格兒/[沈宛](../Page/沈宛.md "wikilink")       | 鳌拜之女，实为前清名将沈钧之女；平定三藩之前被迫嫁与尚之信；十年后，化名沈宛，与纳兰性德重逢，新婚之夜，被带进宫中。 |
| [鄔倩倩](../Page/鄔倩倩.md "wikilink")       | [孝莊太皇太后](../Page/孝莊文皇后.md "wikilink")    | 愛新覺羅玄燁的親祖母                                                 |
| [石小群](../Page/石小群.md "wikilink")       | [那拉惠兒](../Page/惠妃_\(康熙帝\).md "wikilink") | 纳兰性德表妹                                                     |
| [杜雨露](../Page/杜雨露.md "wikilink")       | [鰲　拜](../Page/鰲拜.md "wikilink")          | 輔政大臣之一                                                     |
| [李菲儿](../Page/李菲儿.md "wikilink")       | [端　敏](../Page/固倫端敏公主.md "wikilink")      |                                                            |
| [是　安](../Page/是安.md "wikilink")        | [曹　寅](../Page/曹寅.md "wikilink")          | 康熙帝伴读兼御前侍卫                                                 |
| [白慶琳](../Page/白慶琳.md "wikilink")       | [盧　蕊](../Page/纳兰性德.md "wikilink")        | 纳兰性德嫡福晋                                                    |
| [威　力](../Page/威力.md "wikilink")        | [福　全](../Page/愛新覺羅·福全.md "wikilink")     | 二阿哥，顺治帝第二子                                                 |
| [劉　燦](../Page/劉燦.md "wikilink")        | [班布爾善](../Page/班布爾善.md "wikilink")       |                                                            |
| [俞煒鋒](../Page/俞煒鋒.md "wikilink")       | [索額圖](../Page/索額圖.md "wikilink")         | 索尼之子，皇后之叔叔                                                 |
| [狄劍青](../Page/狄劍青.md "wikilink")       | [安親王](../Page/愛新覺羅岳樂.md "wikilink")      |                                                            |
| [薄貫君](../Page/薄貫君.md "wikilink")       | [明　珠](../Page/納蘭明珠.md "wikilink")        | 纳兰性德之父                                                     |
| [王旭峰](../Page/王旭峰.md "wikilink")       | [蘇克薩哈](../Page/蘇克薩哈.md "wikilink")       | 輔政大臣之一                                                     |
| [李国华](../Page/李国华.md "wikilink")       | [遏必隆](../Page/遏必隆.md "wikilink")         | 輔政大臣之一                                                     |
| [雷牧](../Page/雷牧.md "wikilink")         | [穆里玛](../Page/穆里玛.md "wikilink")         |                                                            |
| [庄瑾](../Page/庄瑾.md "wikilink")         | [苏墨儿](../Page/苏麻喇姑.md "wikilink")        | 孝莊皇太后贴身侍女                                                  |
| [胡中虎](../Page/胡中虎.md "wikilink")       | [梁九功](../Page/梁九功.md "wikilink")         | 康熙帝贴身太监                                                    |
| [孙秀晨](../Page/孙秀晨.md "wikilink")       | 宁嬷嬷                                      | 皇后贴身嬷嬷                                                     |
| [蔡文艳](../Page/蔡文艳.md "wikilink")       | 桂夫人                                      |                                                            |
| [赵连军](../Page/赵连军.md "wikilink")       | [康亲王](../Page/杰书.md "wikilink")          |                                                            |
| [陆恩华](../Page/陆恩华.md "wikilink")       | 肖 婉                                      | 青格儿的母亲，被迫从了鳌拜，后服毒自杀                                        |
| [任宇卿](../Page/任宇卿.md "wikilink")       | [尚之信](../Page/尚之信.md "wikilink")         | 三藩之一[尚可喜之子](../Page/尚可喜.md "wikilink")                     |
| [董子武](../Page/董子武.md "wikilink")       | [吴三桂](../Page/吴三桂.md "wikilink")         | 三藩之一                                                       |
|                                        |                                          |                                                            |

## 参加演出

蔡方云、陈曦、王珂、张亮、赵晨一、王端端、孙冰、马维福、王晓、杨春、王权、丁志强、沈长林、马文俊、李宁、董鹿、石峻奇、张永民、赵福愉、高兴彪、闫家慧、绳永琴、刘莉、张永强、耿俊、马强、石占营

## 電視劇歌曲

## 清朝秘史系列的電視劇

講述[清朝初年的幾位皇帝及太子年少時鮮為人知的事蹟](../Page/清朝.md "wikilink")。

  - 《[孝莊秘史](../Page/孝莊秘史.md "wikilink")》([愛新覺羅福臨](../Page/愛新覺羅福臨.md "wikilink")，不過劇情主要講述[愛新覺羅多爾袞與](../Page/愛新覺羅多爾袞.md "wikilink")[孝莊皇后](../Page/孝莊皇后.md "wikilink"))
  - 《[皇太子秘史](../Page/皇太子秘史.md "wikilink")》([愛新覺羅胤礽](../Page/愛新覺羅胤礽.md "wikilink")，康熙次子)
  - 《[太祖秘史](../Page/太祖秘史.md "wikilink")》([愛新覺羅努爾哈赤](../Page/愛新覺羅努爾哈赤.md "wikilink"))
  - 《納蘭與康熙》([愛新覺羅玄燁](../Page/愛新覺羅玄燁.md "wikilink"))

[K康](../Category/2006年中國電視劇集.md "wikilink")
[Category:亞洲電視外購劇集](../Category/亞洲電視外購劇集.md "wikilink")
[K康](../Category/康熙帝主角題材電視劇.md "wikilink")
[K康](../Category/美亞電視外購劇集.md "wikilink")