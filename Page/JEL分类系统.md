**JEL分类系统**，是美国经济学会《[经济文献杂志](../Page/经济文献杂志.md "wikilink")》（Journal of
Economic
Literature）所创立的对[经济学文献的主题分类系统](../Page/经济学.md "wikilink")，并被现代西方经济学界广泛采用。该分类方法主要采用开头的一个[英文](../Page/英文.md "wikilink")[字母与随后的两位](../Page/字母.md "wikilink")[阿拉伯数字一起对经济学各部类进行](../Page/阿拉伯数字.md "wikilink")“辞书式”编码分类。

例如，C71为“C：数理和数量方法”类中，“C7博弈论与讨价还价理论”中的有关“C71：合作博弈”的内容。

  - **A**: [经济学总论和教学](../Page/经济学总论和教学.md "wikilink") (General Economics
    and Teaching)
  - **B**: [经济学思想流派和方法论](../Page/经济学思想流派和方法论.md "wikilink") (Schools of
    Economic Thought and Methodology)
  - **C**: [数理和数量方法](../Page/数理和数量方法.md "wikilink") (Mathematical and
    Quantitative Methods)
  - **D**: [微观经济学](../Page/微观经济学.md "wikilink") (Microeconomics)
  - **E**: [宏观和货币经济学](../Page/宏观和货币经济学.md "wikilink") (Macroeconomics
    and Monetary Economics)
  - **F**: [国际经济学](../Page/国际经济学.md "wikilink") (International
    Economics)
  - **G**: [金融经济学](../Page/金融经济学.md "wikilink") (Financial Economics)
  - **H**: [公共经济学](../Page/公共经济学.md "wikilink") (Public Economics)
  - **I**: [卫生，教育和福利](../Page/卫生，教育和福利.md "wikilink") (Health, Education
    and Welfare)
  - **J**: [劳动和人口经济学](../Page/劳动和人口经济学.md "wikilink") (Labour and
    Demographic Economics)
  - **K**: [法律和经济学](../Page/法律和经济学.md "wikilink") (Law and Economics)
  - **L**: [产业组织](../Page/产业组织.md "wikilink") (Industrial Organization)
  - **M**:
    [企业管理和商务经济学](../Page/企业管理和商务经济学.md "wikilink")；[市场学](../Page/市场学.md "wikilink")；[会计学](../Page/会计学.md "wikilink")
    (Business Administration and Business Economics; Marketing;
    Accounting)
  - **N**: [经济史](../Page/经济史.md "wikilink")（Economic History）
  - **O**:
    [经济发展](../Page/经济发展.md "wikilink")，[技术变迁和增长](../Page/技术变迁和增长.md "wikilink")
    (Economic Development, Technological Change, and Growth)
  - **P**: [经济系统](../Page/经济系统.md "wikilink") (Economic Systems)
  - **Q**: [农业和自然资源经济学](../Page/农业和自然资源经济学.md "wikilink") (Agricultural
    and Natural Resource Economics)
  - **R**: [城市，农村和区域经济学](../Page/城市，农村和区域经济学.md "wikilink") (Urban,
    Rural and Regional Economics)
  - **Z**: [其他专题](../Page/其他专题.md "wikilink") (Other Special Topics)

以上所列为对开头字母的一级部门分类，具体的细致分类请参照**外部链接**中有关美国经济学会《经济文献杂志》分类系统的连接。

## 外部链接

  - [美国经济学会 (American Economic
    Association)](http://www.vanderbilt.edu/AEA/)（英文）
  - [《经济文献杂志》分类系统（*Journal of Economic Literature* Classification
    System）](http://www.aeaweb.org/journal/jel_class_system.html)（英文）

[Category:经济学](../Category/经济学.md "wikilink")
[Category:分类系统](../Category/分类系统.md "wikilink")