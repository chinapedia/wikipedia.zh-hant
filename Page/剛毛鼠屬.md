**剛毛鼠屬**（***Lophuromys***），[哺乳綱](../Page/哺乳綱.md "wikilink")、[囓齒目](../Page/囓齒目.md "wikilink")、[鼠科的一屬](../Page/鼠科.md "wikilink")，而與剛毛鼠屬（黃腹剛毛鼠）同科的動物尚有[新幾內亞跳鼠屬](../Page/新幾內亞跳鼠屬.md "wikilink")（新幾內亞跳鼠）、小齒鼠屬（雅小齒鼠）、塘鼠屬（塘鼠）、長足水鼠屬（長足水鼠）等之數種[哺乳動物](../Page/哺乳動物.md "wikilink")。

## 參考文獻

  - 中國科學院，《中國科學院動物研究所的世界動物名稱資料庫》，[1](https://web.archive.org/web/20050718232917/http://vzd.brim.ac.cn/division/fauna/index.asp)

[Category:剛毛鼠屬](../Category/剛毛鼠屬.md "wikilink")