**罗尔德·恩格爾布雷希特·格拉范林·阿蒙森**（，）是一位[挪威](../Page/挪威.md "wikilink")[极地](../Page/极地.md "wikilink")[探险家](../Page/探险家.md "wikilink")，於1911年至1912年，他领导的探险队為第一支到达[南极的探险队](../Page/南极.md "wikilink")。\[1\]\[2\]他们于1911年12月14日到达[南极点](../Page/南极点.md "wikilink")，1912年3月7日这个消息才传播出来。他们的探险队比較[英国人](../Page/英国人.md "wikilink")[罗伯特·费尔康·斯科特领导的早一个月到達](../Page/罗伯特·费尔康·斯科特.md "wikilink")。其探险队是毫无争议的最早到达[南极点的探险队](../Page/南极点.md "wikilink")。\[3\]\[4\]他还是第一个穿越[西北水道的](../Page/西北水道.md "wikilink")[探险家](../Page/探险家.md "wikilink")。

1928年一条“意大利号”[飞艇在北极遇难](../Page/飞艇.md "wikilink")，阿蒙森乘飞机前去营救时失踪。他的飞机至今没有被找到。

南极点上的[阿蒙森—斯科特站以他与他的竞争者斯科特命名](../Page/阿蒙森—斯科特站.md "wikilink")，[月球南极的一个比较大的](../Page/月球.md "wikilink")[环形山也以他命名为](../Page/环形山.md "wikilink")[阿蒙森环形山](../Page/阿蒙森环形山.md "wikilink")。

[Antarctic_expedition_map_(Amundsen_-_Scott)-en.svg](https://zh.wikipedia.org/wiki/File:Antarctic_expedition_map_\(Amundsen_-_Scott\)-en.svg "fig:Antarctic_expedition_map_(Amundsen_-_Scott)-en.svg")

## 早年

[Amundsenboy.jpg](https://zh.wikipedia.org/wiki/File:Amundsenboy.jpg "fig:Amundsenboy.jpg")
罗尔德出生于位于[腓特烈斯塔和](../Page/腓特烈斯塔.md "wikilink")[萨尔普斯堡之间的一个](../Page/萨尔普斯堡.md "wikilink")[挪威小镇](../Page/挪威.md "wikilink")。他父亲是个船主和船长，罗尔德是他的第四个儿子。罗尔德的母亲不希望他从事航海行业，热切希望他成为医生。虽然罗尔德在他母亲生前一直遵从她的愿望，但在其逝世后，21岁的罗尔德还是退学选择航海作为其一生的事业。\[5\]罗尔德对航海探险的兴趣是来自于[弗里乔夫·南森](../Page/弗里乔夫·南森.md "wikilink")1888年跨越[格陵兰的探险和](../Page/格陵兰.md "wikilink")[約翰·富蘭克林的](../Page/約翰·富蘭克林.md "wikilink")。最终，他选择了频繁探险的人生。\[6\]

## 極地探險

[Nlc_amundsen.jpg](https://zh.wikipedia.org/wiki/File:Nlc_amundsen.jpg "fig:Nlc_amundsen.jpg")

### 比利時號南極探險（1897－99）

[Amundsen2.jpg](https://zh.wikipedia.org/wiki/File:Amundsen2.jpg "fig:Amundsen2.jpg")

羅爾德·阿蒙森在1897年至1899年加入比利時南極探險，擔任大副。探險是由[亞得里安·傑拉許帶領](../Page/亞得里安·傑拉許.md "wikilink")，使用船隻是，是第一個在冬季前往南極探險的隊伍\[7\]
。不過船隻因為[海冰](../Page/海冰.md "wikilink")，一度卡在[亞歷山大一世島附近南緯](../Page/亞歷山大一世島.md "wikilink")70°30′的位置。在沒有足夠準備的情形下，船員在船上忍受了一整個冬季。依阿蒙森的估計，在缺乏柑橘類水果的情形下．探險隊的醫生用狩獵動物並讓船員食用生肉的方式，使船員直接從肉品中補充[維生素C](../Page/維生素C.md "wikilink")，免於[壞血病的威脅](../Page/壞血病.md "wikilink")，並且可以充飢。這對阿蒙森未來的探險是很重要的經驗。

### 西北水道（1903－1906）

罗尔德·阿蒙森在1903年乘小船從大西洋進入西北水道，3年後到達[阿拉斯加](../Page/阿拉斯加.md "wikilink")，成為第一個乘船通過整個西北水道的人。

### 南極探險（1910－1912）

[Aan_de_Zuidpool_-_p1913-160.jpg](https://zh.wikipedia.org/wiki/File:Aan_de_Zuidpool_-_p1913-160.jpg "fig:Aan_de_Zuidpool_-_p1913-160.jpg")

阿蒙森本來接下來要探索北極及[北极海盆](../Page/北极海盆.md "wikilink")，而他在1909年時聽到美國人[罗伯特·皮里和](../Page/罗伯特·皮里.md "wikilink")分別都宣稱已率領探險到達北極，阿蒙森知道他的北極之旅很難募集資金，因此改為探索南極大陸\[8\]。阿蒙森不太清楚他自己的意圖，而他在挪威的支持者及英國人[罗伯特·斯科特也很困惑](../Page/罗伯特·斯科特.md "wikilink")\[9\]。阿蒙森在那一年計劃要探索南極，使用的船隻是之前[弗里德乔夫·南森曾用過的](../Page/弗里德乔夫·南森.md "wikilink")[前進號](../Page/前進號.md "wikilink")。阿蒙森在1910年6月3日離開奧斯陸，往南前進\[10\]\[11\]。在[馬德拉時阿蒙森提醒他的船員他們會前往南極](../Page/馬德拉.md "wikilink")，並發了一封電報給斯科特，內文為"僅告知你前進號正前往南極——阿蒙森"\[12\]。

將近六個月後，他們在1911年的1月14日，到了[羅斯冰架的東角](../Page/羅斯冰架.md "wikilink")（當時稱為「大冰障」），在一個稱為[鯨灣的大入口處](../Page/鯨灣.md "wikilink")，探險隊建立了大本营，稱為「」。阿蒙森沒有選擇之前在南極探險時穿的厚重[羊毛衣物](../Page/羊毛.md "wikilink")，而選擇類似[因纽特人穿的皮衣](../Page/因纽特人.md "wikilink")\[13\]。
[Amundsen-in-ice.jpg](https://zh.wikipedia.org/wiki/File:Amundsen-in-ice.jpg "fig:Amundsen-in-ice.jpg")
利用滑雪及為運輸方式，阿蒙森他們在冰障的南方，南緯80°、81°及82°處，沿著一條直接通往南極的路上建立三補給站\[14\]。阿蒙森也決定在路上殺掉一些狗，作為生肉的來源。包括、及的小隊在1911年9月8日出發，但因為酷寒的溫度，不得不放棄他們旅程。艱苦的旅程也帶來了探險隊中的一次爭執，最後阿蒙森派和另外二人去探索[愛德華七世地](../Page/愛德華七世地.md "wikilink")。

第二次的小隊成員包括、、阿蒙森，在1911年10月19日離開大本营，帶了四隻雪橇和52隻狗。沿著當時已知的[阿塞爾海伯格冰川](../Page/阿塞爾海伯格冰川.md "wikilink")，經過四天的攀登後到達极地高原的边缘。在12月14日小隊中的五個人帶著16隻狗，到達了南極（90° 0′ S）。他們比斯科特的探險隊早了33到34天。阿蒙森將他們的南極營地命名為「」（極地之家），並將[南極高原重新命名為](../Page/南極高原.md "wikilink")「[哈康七世高原](../Page/哈康七世.md "wikilink")」（）。他們留下了一個小帳篷和信，若他們無法平安的回到大本营，可以以帳篷和信說明他們第一個到達南極的成就。

小隊在1912年1月25日回到大本营，只剩下11隻狗。他們離開南極大陸前往澳洲的[荷巴特](../Page/荷巴特.md "wikilink")，阿蒙森在1912年3月7日在荷巴特宣佈了他們是第一個到達南極的探險隊，並發出電報告訴支持者。

阿蒙森南極探險的成功是在於謹慎的規劃、良好的裝備、合適的服裝、簡單的前期任務（阿蒙森沒有去测量他往南極的路線，有的只有兩張照片）、了解狗及其乘載能力，以及有效的利用滑雪板。相較於斯科特探險隊的不幸，阿蒙森的探險比較平順。

阿蒙森如此評斷他的南極探險：

阿蒙森將南極探險的經過寫在《南極：記載在前進號上的挪威南極探險，1910－12》（1912）。

## 東北水道（1918－20）

## 到達北極

## 以阿蒙森為名的事物

許多事物及地點都起名為阿蒙森：

  - [阿蒙森-斯科特南極站是以阿蒙森及斯科特為名](../Page/阿蒙森-斯科特南極站.md "wikilink")。
  - [阿蒙森海是](../Page/阿蒙森海.md "wikilink")[南冰洋的一部分](../Page/南冰洋.md "wikilink")。
  - 南極洲上的[阿蒙森冰川](../Page/阿蒙森冰川.md "wikilink")。
  - 南極洲上的[阿蒙森灣](../Page/阿蒙森灣_\(南極洲\).md "wikilink")。
  - 南極洲上的[阿蒙森山](../Page/阿蒙森山.md "wikilink")。
  - [阿蒙森灣屬於](../Page/阿蒙森灣.md "wikilink")[北冰洋的一部分](../Page/北冰洋.md "wikilink")，在[加拿大](../Page/加拿大.md "wikilink")[西北地區的海岸外](../Page/西北地區.md "wikilink")，阿蒙森灣的另一側是[班克斯島及](../Page/班克斯島.md "wikilink")[维多利亚岛的西邊](../Page/维多利亚岛.md "wikilink")。
  - [月球南极的一个比较大的](../Page/月球.md "wikilink")[环形山也以他命名为](../Page/环形山.md "wikilink")[阿蒙森环形山](../Page/阿蒙森环形山.md "wikilink")。

## 著作

  - 《南极》(1912)
  - 《我作为探险家的一生》(1927)

## 注释

## 参考文献

  - 引用

## 外部連結

  - [The Fram Museum
    (Frammuseet)](https://web.archive.org/web/20040809203531/http://www.fram.museum.no/en/)
  - [Map of Amundsen's and Scott's South Pole
    journeys](https://web.archive.org/web/20100316063537/http://www.fram.museum.no/en/default.asp?page=158)
    at The [Fram Museum](../Page/Fram_Museum.md "wikilink") (Frammuseet)
  - [Arctic Passage](http://www.pbs.org/wgbh/nova/arctic/) at
    [PBS](../Page/Public_Broadcasting_Service.md "wikilink")'
    [Nova](../Page/Nova_\(TV_series\).md "wikilink") site has
    photographs, maps, [excerpts from Amundsen's
    autobiography](http://www.pbs.org/wgbh/nova/arctic/amundsen.html)
    and [an interview with Roland
    Huntford](http://www.pbs.org/wgbh/nova/arctic/huntford.html).
  - [Roald Amundsen article at
    south-pole.com](http://www.south-pole.com/p0000101.htm)
  - [70South – information on Roald
    Amundsen](https://web.archive.org/web/20131127044550/http://www.70south.com/resources/antarctic-history/explorers/roaldamundsen/)
  - [Short biography from Norwegian Foreign
    Ministry](http://odin.dep.no/odin/engelsk/norway/history/032005-990461/index-dok000-b-n-a.html)
  - [DIO vol.10 2000](http://www.dioi.org/vols/wa0.pdf) Amundsen's
    unparalleled record of polar firsts, including evidence that he was
    1st to each geographical pole.
  - [The Last Place On Earth](http://www.imdb.com/title/tt0088551/) 1985
    serial depicting the race between Amundsen (played by [Sverre Anker
    Ousdal](../Page/Sverre_Anker_Ousdal.md "wikilink")) and Scott.
  - [Roald Amundsen At Find A
    Grave](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=20956)
  - [The Red Tent](http://www.imdb.com/title/tt0067315/) [Sean
    Connery](../Page/Sean_Connery.md "wikilink") plays Amundsen
  - [Villemont](http://www.villemont.ch/index.php?id=16) Roald Amundsen
    Legacy at VILLEMONT
  - [1](http://www.searchforamundsen.com) Search for Amundsen's crashed
    plane

**罗尔德·阿蒙森的著作**

  - (plain text and HTML)

  - [Works by Roald
    Amundsen](http://www.archive.org/search.php?query=creator%3AAmundsen%20AND%20mediatype%3Atexts)
    at [Internet Archive](../Page/Internet_Archive.md "wikilink") and
    [Google Books](../Page/Google_Books.md "wikilink") (scanned books
    original versions color illustrated)

  - [Works by Roald
    Amundsen](http://www.archive.org/search.php?query=creator%3AAmundsen%20AND%20mediatype%3Aaudio)
    via [LibriVox](../Page/LibriVox.md "wikilink") (audiobooks)

  - [*The South
    Pole*](https://web.archive.org/web/20040301114205/http://etext.library.adelaide.edu.au/a/amundsen_r/southpole/)
    Arthur G. Chater's 1912 translation (HTML)

  -
## 参见

  - [南极历史](../Page/南极历史.md "wikilink")

{{-}}

[Category:挪威探险家](../Category/挪威探险家.md "wikilink")
[Category:南極探險家](../Category/南極探險家.md "wikilink")
[Category:地理学家](../Category/地理学家.md "wikilink")
[Category:歐洲下落不明者](../Category/歐洲下落不明者.md "wikilink")
[A](../Category/北極探險家.md "wikilink")
[Category:挪威空难身亡者](../Category/挪威空难身亡者.md "wikilink")

1.

2.

3.
4.
5.

6.

7.

8.

9.
10.
11.

12.
13.
14.