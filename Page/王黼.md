**王黼**（），[字](../Page/表字.md "wikilink")**將明**，[北宋](../Page/北宋.md "wikilink")[開封](../Page/開封.md "wikilink")[祥符](../Page/祥符.md "wikilink")（今[河南](../Page/河南.md "wikilink")[開封](../Page/開封.md "wikilink")）人，[北宋末期政治人物](../Page/北宋.md "wikilink")，風姿俊美，善於逢迎，被稱為[六賊之一](../Page/六賊.md "wikilink")。原名**王甫**，後因與[東漢](../Page/東漢.md "wikilink")[宦官](../Page/宦官.md "wikilink")[王甫同名](../Page/王甫_\(東漢\).md "wikilink")，而被賜名**黼**。

## 生平

王黼生於[神宗](../Page/宋神宗.md "wikilink")[元豐二年](../Page/元豐.md "wikilink")（1079年）。

[徽宗](../Page/宋徽宗.md "wikilink")[崇寧二年](../Page/崇寧.md "wikilink")（1103年）進士\[1\]，歷任[相州](../Page/相州.md "wikilink")[司理參軍](../Page/司理參軍.md "wikilink")、[校书郎](../Page/校书郎.md "wikilink")、[符宝郎](../Page/符宝郎.md "wikilink")、[左司谏](../Page/司谏.md "wikilink")。

[宣和元年春正月戊午](../Page/宣和.md "wikilink")（1119年），時任[通議大夫的王黼](../Page/通議大夫.md "wikilink")，連跳八級，被拔擢為特進、[少宰](../Page/少宰.md "wikilink")（[右宰相](../Page/右宰相.md "wikilink")）兼中書侍郎\[2\]，乃宋開國以來第一人\[3\]。

[宣和二年十一月庚戌](../Page/宣和.md "wikilink")，以王黼為[少保](../Page/少保.md "wikilink")、[太宰兼門下侍郎](../Page/太宰.md "wikilink")。[宣和三年九月丙寅](../Page/宣和.md "wikilink")，以王黼為[少傅](../Page/少傅.md "wikilink")，[鄭居中為](../Page/鄭居中.md "wikilink")[少師](../Page/少師.md "wikilink")。[宣和四年六月壬寅](../Page/宣和.md "wikilink")，以王黼為[少師](../Page/少師.md "wikilink")。[宣和四年秋七月](../Page/宣和.md "wikilink")，王黼以[耶律淳死](../Page/耶律淳.md "wikilink")，複命[童貫](../Page/童貫.md "wikilink")、[蔡攸治兵](../Page/蔡攸.md "wikilink")，以河陽三城節度使[劉延慶為都統制](../Page/劉延慶.md "wikilink")。[宣和四年九月戊午](../Page/宣和.md "wikilink")，朝散郎宋昭上書諫北伐，王黼大惡之，詔除名勒停、廣南編管\[4\]。

[宣和五年五月](../Page/宣和.md "wikilink")，宋廷以收復[燕雲十六州為目標](../Page/燕雲十六州.md "wikilink")，王黼總治三省事，於三省置經撫房，專治邊事，徵刮天下得錢六千二百萬[緡](../Page/緡.md "wikilink")，竟買了五六座空城而高奏凱旋-{回}-到汴京。然王黼又率百僚稱賀，[宋徽宗賞賜](../Page/宋徽宗.md "wikilink")[玉帶](../Page/玉帶.md "wikilink")，優進[太傅](../Page/太傅.md "wikilink")，封楚國公，許諾王黼可以穿紫花袍，坐騎與儀式物品幾乎可以與親王相等\[5\]。

[宣和五年秋七月庚午](../Page/宣和.md "wikilink")，太傅、楚國公王黼等人議上尊號曰繼天興道敷文成武睿明皇帝，[宋徽宗說](../Page/宋徽宗.md "wikilink")：「此神宗皇帝所不敢受者也。」不被允許。\[6\]\[7\]。

[宣和六年十一月丙子](../Page/宣和.md "wikilink")，王黼致仕\[8\]。

[钦宗受禪即位](../Page/宋钦宗.md "wikilink")，王黼驚恐倉惶入朝恭賀，但朝廷把門關上並稱皇上有旨說不能接納。金兵入汴後，帶著妻兒子女往東走。[宋欽宗下詔貶王黼為](../Page/宋欽宗.md "wikilink")[崇信軍](../Page/崇信軍.md "wikilink")[節度副使](../Page/節度副使.md "wikilink")，籍沒其家，流放[永州](../Page/永州.md "wikilink")（今[湖南](../Page/湖南.md "wikilink")[零陵](../Page/零陵.md "wikilink")）。[吳敏](../Page/吳敏.md "wikilink")、[李綱請殺王黼](../Page/李綱.md "wikilink")，朝廷交付[開封尹](../Page/開封尹.md "wikilink")[聶山](../Page/聶山.md "wikilink")，聶山與王黼有故隙、宿怨，遣[武士躡及至](../Page/武士.md "wikilink")[雍丘縣南的](../Page/雍丘縣.md "wikilink")[輔固村殺之](../Page/輔固村.md "wikilink")，有民家取其首級上交政府。

[钦宗以初即位](../Page/宋钦宗.md "wikilink")，對誅殺大臣有困難，於是托言說是被盜賊所殺。發出議論的人不以誅王黼為過，而以天討不正為失刑矣\[9\]。

## 民間謠言

“公然受賄賂，賣官鬻爵，至有定價”，京師謠言：“三百貫，曰通判；五百索，直秘閣。”\[10\]；又以巨款贖回[燕京](../Page/燕京.md "wikilink")（今[北京](../Page/北京.md "wikilink")），夸称大功，升至[少傅](../Page/少傅.md "wikilink")。[陳東稱之为](../Page/陳東.md "wikilink")[六贼之一](../Page/六贼.md "wikilink")。

## 参考文献

### 引用

### 来源

  -
  -
{{-}}

[W](../Category/宋朝宰相.md "wikilink")
[Category:崇寧二年癸未科進士](../Category/崇寧二年癸未科進士.md "wikilink")
[F](../Category/王姓.md "wikilink")
[Category:宋朝副相](../Category/宋朝副相.md "wikilink")
[Category:北宋尚书省官员](../Category/北宋尚书省官员.md "wikilink")

1.  《[會編](../Page/會編.md "wikilink")》卷31《[中興姓氏奸邪錄](../Page/中興姓氏奸邪錄.md "wikilink")》
2.  《[宋史](../Page/宋史.md "wikilink")》卷022
3.  《[宋史](../Page/宋史.md "wikilink")》卷470《王黼傳》
4.  《[宋史](../Page/宋史.md "wikilink")》卷022
5.  《[宋史](../Page/宋史.md "wikilink")》卷470
6.  《[宋史](../Page/宋史.md "wikilink")》卷022
7.  《[宋史](../Page/宋史.md "wikilink")》卷470
8.  《[宋史](../Page/宋史.md "wikilink")》卷022
9.  《[宋史](../Page/宋史.md "wikilink")》卷470
10. [朱弁](../Page/朱弁.md "wikilink")《[曲洧舊聞](../Page/曲洧舊聞.md "wikilink")》卷十