**苏拉威西岛**（）舊名**西里伯斯島**（）是[印度尼西亚东部的一个大](../Page/印度尼西亚.md "wikilink")[岛屿](../Page/岛屿.md "wikilink")。

## 地理

[Sulawesi_Topography.png](https://zh.wikipedia.org/wiki/File:Sulawesi_Topography.png "fig:Sulawesi_Topography.png")
苏拉威西岛按[面积是世界第十一大岛](../Page/面积.md "wikilink")，，位于印度尼西亚东部，[菲律宾南方](../Page/菲律宾.md "wikilink")，形状非常特别，类似一个大K字母，有四个[半岛](../Page/半岛.md "wikilink")，岛中部是险峻的山区，因此四个半岛之间很少来往，从海路连接比从陆路方便。

苏拉威西岛的行政区域分为6个省：[西蘇拉威西省](../Page/西蘇拉威西省.md "wikilink")、[北蘇拉威西省](../Page/北蘇拉威西省.md "wikilink")、[中蘇拉威西省](../Page/中蘇拉威西省.md "wikilink")、[南蘇拉威西省](../Page/南蘇拉威西省.md "wikilink")、[東南蘇拉威西省和](../Page/東南蘇拉威西省.md "wikilink")[哥伦打洛省](../Page/哥伦打洛省.md "wikilink")。最大的城市是西南部的[望加锡](../Page/望加锡.md "wikilink")，还有北部的[万鸦老也是较大的城市](../Page/万鸦老.md "wikilink")。

## 动植物

苏拉威西岛正位于[华莱士线上](../Page/华莱士线.md "wikilink")\[1\]，因此既具有[东洋界也具有](../Page/东洋界.md "wikilink")[澳新界的](../Page/澳新界.md "wikilink")[动](../Page/动物.md "wikilink")[植物](../Page/植物.md "wikilink")，其中大部分属于[澳新界的](../Page/澳新界.md "wikilink")，有2,290[平方千米的区划被划归罗尔](../Page/平方千米.md "wikilink")·林度国家公园。

苏拉威西岛目前已知有127种[哺乳动物](../Page/哺乳动物.md "wikilink")，其中62%（79[种](../Page/种.md "wikilink")）是本岛特有种，在世界其他地方不存在，其中最多的是一种矮水牛。[鸟类有](../Page/鸟.md "wikilink")34%是当地特有种。

## 文化艺术

苏拉威西人非常重视[艺术](../Page/艺术.md "wikilink")，包括制造[陶器](../Page/陶器.md "wikilink")、织物和[舞蹈](../Page/舞蹈.md "wikilink")，他们制造的陶器非常精制；织物非常细致，有重复的图案。男人的舞蹈雄武有力，女人柔美流畅，舞蹈经常能讲述一个故事情节。

## 宗教

大部分苏拉威西人信奉[伊斯兰教](../Page/伊斯兰教.md "wikilink")[逊尼派](../Page/逊尼派.md "wikilink")，但有19%的人信奉[基督宗教](../Page/基督宗教.md "wikilink")，其中2%信仰[罗马天主教](../Page/罗马天主教.md "wikilink")，17%的人信奉[基督新教](../Page/基督新教.md "wikilink")，主要集中在北部和中部，以及一些大城市，其中[北蘇拉威西省的基督徒人口比例更是佔絕大多數](../Page/北蘇拉威西省.md "wikilink")，超過70%。但大部分人不管是信奉基督宗教还是伊斯兰教，仍然相信当地的原始信仰和当地的神祇。

此外还存在一些外来人口的社区信仰[印度教或](../Page/印度教.md "wikilink")[佛教](../Page/佛教.md "wikilink")，主要在[印度人和华人之间](../Page/印度.md "wikilink")。

## 自然災害

2018年9月28日，蘇拉威西島遭逢規模7.5強震，隨後，中蘇拉威西島[帕盧市和](../Page/帕盧.md "wikilink")[棟加拉縣發生大規模](../Page/棟加拉縣.md "wikilink")[海嘯](../Page/海嘯.md "wikilink")，造成逾千人罹難\[2\]。5天後，2018年10月3日早上8點多，蘇拉威西島北方的[索普坦火山突然爆發](../Page/索普坦火山.md "wikilink")，火山灰衝上4000公尺高，災害損失難以估計。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  -
[S](../Category/印尼岛屿.md "wikilink") [S](../Category/太平洋島嶼.md "wikilink")
[S](../Category/大巽他群岛.md "wikilink")
[\*](../Category/苏拉威西岛.md "wikilink")

1.  丁楓峻(2010), 東南亞自助潛水趣. 華都文化. 頁30. ISBN 978-986-6860-93-5
2.  [才剛遭強震、海嘯重創
    印尼蘇拉威西島今早火山爆發。](http://news.ltn.com.tw/news/world/breakingnews/2569203)自由時報
    2018-10-03