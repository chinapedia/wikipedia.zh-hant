**TAKESHIS**''是[北野武于](../Page/北野武.md "wikilink")2005年[導演的](../Page/導演.md "wikilink")[日本电影](../Page/日本电影.md "wikilink")，是北野武的第十二部導演作品，也是一部北野武所擅長的[黑色喜劇電影](../Page/黑色喜劇.md "wikilink")。

北野武曾經發言表示這部電影是他最後一部黑色喜劇類型的電影。

## 出演

  - [北野武](../Page/北野武.md "wikilink")
  - [京野琴美](../Page/京野琴美.md "wikilink")
  - [大杉漣](../Page/大杉漣.md "wikilink")
  - [寺島進](../Page/寺島進.md "wikilink")
  - [岸本加世子](../Page/岸本加世子.md "wikilink")
  - [美輪明宏](../Page/美輪明宏.md "wikilink")
  - [松村邦洋](../Page/松村邦洋.md "wikilink")

## 外部链接

  - [電影官方网站（日语）](http://www.office-kitano.co.jp/takeshis/)

[Category:日本电影作品](../Category/日本电影作品.md "wikilink")
[Category:2005年电影](../Category/2005年电影.md "wikilink")
[Category:北野武電影](../Category/北野武電影.md "wikilink")
[Category:朝日電視台製作的電影](../Category/朝日電視台製作的電影.md "wikilink")