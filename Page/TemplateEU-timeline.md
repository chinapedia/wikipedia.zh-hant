<table style="width:10%;">
<colgroup>
<col style="width: -4%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 8%" />
<col style="width: 1%" />
<col style="width: -4%" />
<col style="width: -4%" />
<col style="width: -4%" />
<col style="width: -4%" />
<col style="width: -4%" />
<col style="width: -4%" />
<col style="width: -4%" />
<col style="width: -4%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><strong>簽署</strong><br />
<strong>生效</strong><br />
<strong><a href="歐洲聯盟基本條約.md" title="wikilink">條約</a></strong></p></td>
<td><p>1948<br />
1948<br />
<strong><a href="布魯塞爾條約_(1948年).md" title="wikilink">布魯塞爾條約</a></strong></p></td>
<td><p>1951<br />
1952<br />
<strong><a href="巴黎條約_(1951年).md" title="wikilink">巴黎條約</a></strong></p></td>
<td><p>1954<br />
1955<br />
<strong><a href="倫敦與巴黎會議.md" title="wikilink">布魯塞爾條約修正</a></strong></p></td>
<td><p>1957<br />
1958<br />
<strong><a href="羅馬條約.md" title="wikilink">羅馬條約</a></strong></p></td>
<td><p>1965<br />
1967<br />
<strong><a href="合併條約.md" title="wikilink">合併條約</a></strong></p></td>
<td><p>1975<br />
N/A<br />
<strong><a href="欧洲理事会.md" title="wikilink">欧洲理事会成立</a></strong></p></td>
<td><p>1986<br />
1987<br />
<strong><a href="單一歐洲法案.md" title="wikilink">單一歐洲法案</a></strong></p></td>
<td><p>1992<br />
1993<br />
<strong><a href="馬斯垂克條約.md" title="wikilink">馬斯垂克條約</a></strong></p></td>
<td><p>1997<br />
1999<br />
<strong><a href="阿姆斯特丹條約.md" title="wikilink">阿姆斯特丹條約</a></strong></p></td>
<td><p>2001<br />
2003<br />
<strong><a href="尼斯條約.md" title="wikilink">尼斯條約</a></strong></p></td>
<td><p>2007<br />
2009<br />
<strong><a href="里斯本條約.md" title="wikilink">-{里}-斯本條約</a></strong></p></td>
<td><figure>
<img src="Pix.gif" title="Pix.gif" alt="Pix.gif" width="1" /><figcaption>Pix.gif</figcaption>
</figure></td>
<td><figure>
<img src="Pix.gif" title="Pix.gif" alt="Pix.gif" width="1" /><figcaption>Pix.gif</figcaption>
</figure></td>
<td><figure>
<img src="Pix.gif" title="Pix.gif" alt="Pix.gif" width="1" /><figcaption>Pix.gif</figcaption>
</figure></td>
<td><figure>
<img src="Pix.gif" title="Pix.gif" alt="Pix.gif" width="1" /><figcaption>Pix.gif</figcaption>
</figure></td>
<td><figure>
<img src="Pix.gif" title="Pix.gif" alt="Pix.gif" width="1" /><figcaption>Pix.gif</figcaption>
</figure></td>
<td><figure>
<img src="Pix.gif" title="Pix.gif" alt="Pix.gif" width="1" /><figcaption>Pix.gif</figcaption>
</figure></td>
<td><figure>
<img src="Pix.gif" title="Pix.gif" alt="Pix.gif" width="1" /><figcaption>Pix.gif</figcaption>
</figure></td>
<td><figure>
<img src="Pix.gif" title="Pix.gif" alt="Pix.gif" width="1" /><figcaption>Pix.gif</figcaption>
</figure></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="歐盟三支柱.md" title="wikilink"><font color="white">歐盟三支柱</font></a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="歐洲各共同體.md" title="wikilink"><font color="white">歐洲各共同體</font></a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="歐洲原子能共同體.md" title="wikilink"><font color="black">歐洲原子能共同體</font></a><font color="black">(EURATOM)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="歐洲煤鋼共同體.md" title="wikilink"><font color="black">歐洲煤鋼共同體</font></a> <font color="black">(ECSC)</font></p></td>
<td><p><small>（條約於2002年終止）</small></p></td>
<td></td>
<td><p><a href="歐洲聯盟.md" title="wikilink"><font color="white">歐洲聯盟</font></a> <font color="white">(EU)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p><a href="歐洲經濟共同體.md" title="wikilink"><font color="black">歐洲經濟共同體</font></a> <font color="black">(EEC)</font></p></td>
<td></td>
<td><p><a href="歐洲共同體.md" title="wikilink"><font color="black">歐洲共同體</font></a> <font color="black">(EC)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="TREVI.md" title="wikilink"><font color="black">TREVI</font></a></p></td>
<td><p><a href="司法與內政合作.md" title="wikilink"><font color="black">司法與內政合作</font></a> <font color="black">(JHA)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="刑事案件的警察合作與司法合作（歐盟）.md" title="wikilink"><font color="black">刑事案件的警察與司法合作</font></a> <font color="black">(PJCC)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="歐洲政治合作.md" title="wikilink"><font color="black">歐洲政治合作</font></a> <font color="black">(EPC)</font></p></td>
<td><p><a href="共同外交與安全政策.md" title="wikilink"><font color="black">共同外交與安全政策</font></a> <font color="black">(CFSP)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><small>（鬆散的個體）</small></p></td>
<td><p><a href="西歐聯盟.md" title="wikilink"><font color="black">西歐聯盟</font></a> <font color="black">(WEU)</font></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><small>（條約於2010年失效）</small></p></td>
<td><p> </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<noinclude> </noinclude>

[Category:政治模板](../Category/政治模板.md "wikilink")
[Category:圖形時間線模板](../Category/圖形時間線模板.md "wikilink")