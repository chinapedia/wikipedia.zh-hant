《**媽媽咪呀！**》（[英語](../Page/英語.md "wikilink")：**Mamma
Mia\!**）是一部由[英国](../Page/英国.md "wikilink")[剧作家](../Page/剧作家.md "wikilink")（Catherin
Johnson）创作的[音乐剧](../Page/音乐剧.md "wikilink")，音乐剧的名稱来自于[ABBA樂團](../Page/ABBA.md "wikilink")1975年年度排行榜冠军的同名单曲。1999年由英国女导演[菲利达·劳埃德执导](../Page/菲利达·劳埃德.md "wikilink")，剧中包含了ABBA乐队多首成名曲目包括：《Super
Trouper》、《Dancing Queen》、《歌声让我飞翔》、《胜者为王》以及《S.O.S.》。全球累计已超过2千万人欣赏。"Mamma
Mia"为意大利语，本意是“我的妈妈”，用于惊叹，相当于汉语“我的妈呀！”，表示惊讶、悲伤、兴奋等情感。

故事围绕女主角苏菲（Sophie）和她的单身母亲唐娜（Donna）展開，苏菲希望在婚礼上由父亲引领走上红地毯，但是她不知道被自己偷偷邀请来的母亲三位旧情人中，究竟哪一位才是自己的親生父親。

## 历史

《媽媽咪呀！》于1999年4月6日在[伦敦](../Page/伦敦.md "wikilink")[爱德华王子剧院首演](../Page/爱德华王子剧院.md "wikilink")。2004年开始在[威尔士王子剧院演出](../Page/威尔士王子剧院.md "wikilink")。[北美的首场演出是](../Page/北美.md "wikilink")2000年5月23日在[多伦多的](../Page/多伦多.md "wikilink")[皇家亚历山大剧院](../Page/皇家亚历山大剧院.md "wikilink")。2001年的10月18日该剧在[百老汇的](../Page/百老汇.md "wikilink")[冬园剧院上演](../Page/冬园剧院.md "wikilink")。

目前多伦多的公演已经结束，其他演出包括澳大利亚国际巡迴演出（2001年-2005年）、[东京](../Page/东京.md "wikilink")（2002年-2004年）、[首尔演出](../Page/首尔.md "wikilink")（2004年初）和[乌得勒支的演出](../Page/乌得勒支\(城市\).md "wikilink")（2003年-2006年）。

2005年5月15日，《媽媽咪呀！》以百老汇初演一千五百场的记录超过了其他著名音乐剧如[真善美](../Page/真善美.md "wikilink")、[国王与我和](../Page/国王与我.md "wikilink")[红男绿女](../Page/红男绿女.md "wikilink")。2006年6月8日，它在[百老汇所有](../Page/百老汇剧场.md "wikilink")[音乐剧公演时间长度排名里名列第二十六位](../Page/音乐剧.md "wikilink")。

2006年4月19日，Playbill.com和Broadway.com网站公布了[《媽媽咪呀！》电影即将进入制作](../Page/媽媽咪呀！_\(電影\).md "wikilink")，2008年夏天上映。

目前尚在进行的演出有：[美国](../Page/美国.md "wikilink")（[百老汇](../Page/百老汇.md "wikilink")、[拉斯维加斯](../Page/拉斯维加斯.md "wikilink")，及全国巡回演出）、[欧洲](../Page/欧洲.md "wikilink")（国际巡回演出）、[日本](../Page/日本.md "wikilink")[大阪](../Page/大阪.md "wikilink")（于2007年结束）、[德国](../Page/德国.md "wikilink")[汉堡和](../Page/汉堡.md "wikilink")[斯图加特](../Page/斯图加特.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")[马德里](../Page/马德里.md "wikilink")，和[瑞典](../Page/瑞典.md "wikilink")[斯德哥尔摩](../Page/斯德哥尔摩.md "wikilink")。2006年，在[比利时的](../Page/比利时.md "wikilink")[安特卫普和](../Page/安特卫普.md "wikilink")[意大利的](../Page/意大利.md "wikilink")[米兰公演](../Page/米兰.md "wikilink")，分别用[佛兰芒语和](../Page/佛兰芒语.md "wikilink")[意大利语演出](../Page/意大利语.md "wikilink")，是该剧自[英语](../Page/英语.md "wikilink")、[德语](../Page/德语.md "wikilink")、[荷兰语](../Page/荷兰语.md "wikilink")、[日语](../Page/日语.md "wikilink")、[朝鲜语](../Page/朝鲜语.md "wikilink")、[西班牙语和](../Page/西班牙语.md "wikilink")[瑞典语演出以来的第八种和第九种语言](../Page/瑞典语.md "wikilink")。这部音乐剧于2006年重返[南非](../Page/南非.md "wikilink")（[开普敦和](../Page/开普敦.md "wikilink")[普利托里亚](../Page/普利托里亚.md "wikilink")）及韩国的首尔。此后，第十种语言[俄语版本已经于](../Page/俄语.md "wikilink")2006年10月在[俄罗斯](../Page/俄罗斯.md "wikilink")[莫斯科公演](../Page/莫斯科.md "wikilink")。第十四种语言[中文版本已于](../Page/中文.md "wikilink")2011年7月11日在中国大陆[上海大剧院正式首演](../Page/上海大剧院.md "wikilink")。2013年8月2日《妈妈咪呀！》中文版新一季演出开始，上演城市包括北京、上海、广州等。包括影子、沈小岑等首演演员再次领衔第三季的演出。

## 故事情节

故事发生在一个虚构的[希腊小岛上](../Page/希腊.md "wikilink")。苏菲一直想知道自己的父亲是谁，但她母亲唐娜却不愿提及过去。苏菲读了母亲的日记，发现唐娜提到曾经与三个男人有过亲密关系。苏菲认为这三个男人中肯定有一个是她的父亲，她给每个人都寄去了自己的婚礼请柬，但没有告诉母亲。这三个人是分别是哈瑞（Harry
"Head Banger" Bright），比尔（Bill Austin），萨姆（Sam
Carmichael），他们各有各的特点。哈瑞是个书生气十足的银行职员，笑起来声音古怪；比尔是个典型的澳大利亚人；而山姆是名建筑师，与唐娜恋爱时就是有妇之夫。除此之外，唐娜还曾和谭雅：一個曾经结过三次婚又离过三次婚的有钱人和罗茜：一个没结过婚、爱玩爱闹的老姑娘组成过合唱组，叫“唐娜炮弹”（Donna
and the Dynamos）。

最初，苏菲认为比尔是她的父亲，因为他有一个名叫苏菲娅的姑姑，曾经给家人留下一大笔遗产。唐娜常说苏菲的名字来自于“苏菲娅”，而她们用来开小饭店的钱据说是来自一笔遗产。她请比尔在婚礼上引领她入场，他也同意了。很快，哈利和山姆猜到了这件事的来龙去脉，他们找到苏菲，都觉得自己才有资格在婚礼上引领苏菲入场。

苏菲给三个人都写了信，告诉他们她不用他们引领她入场，因为唐娜也可以做这件事。而等到了婚礼，苏菲和她的未婚夫忽然发现他们其实并不愿意结婚。然而，他们没有取消婚礼，而是让山姆和唐娜举行了婚礼。与此同时，大家都发现原来罗茜和比尔也已经坠入了爱河。

## 歌曲

  - 第一幕

<!-- end list -->

  - 《序曲/序幕》
  - 《有一个梦》
  - 《Honey, Honey》
  - 《Money, Money, Money》
  - 《歌声让我飞翔》
  - 《妈妈咪呀》
  - 《Chiquitita》
  - 《Dancing Queen》
  - 《用爱把我包围》
  - 《Super Trouper》
  - 《给我！给我！给我！》
  - 《故事走向何方》
  - 《Voulez-Vous》

<!-- end list -->

  - 第二幕

<!-- end list -->

  - 《Entr'Act》
  - 《危险地带》
  - 《你我之间》
  - 《S.O.S.》
  - 《你妈妈知道吗》
  - 《没有对，没有错》
  - 《夏日回忆》
  - 《指尖时光流过》
  - 《胜者为王》
  - 《宝贝别等待》
  - 《说爱我》
  - 《有一个梦》

**安可**

  - 《妈妈咪呀》
  - 《Dancing Queen》
  - 《Waterloo》

预演的时候，在序幕后还有一首歌叫《夏夜里的城市》（"Summer Night
City"），在婚礼排练时唱起。此时，阿里、丽萨、谭雅和罗茜纷纷到达岛上。

## 外部链接

  - [《媽媽咪呀！》官方网站](http://www.mamma-mia.com/)
  - [ABBA樂團官方网站](http://www.abbasite.com/)

[Category:倫敦西區音樂劇](../Category/倫敦西區音樂劇.md "wikilink")
[Category:百老匯音樂劇](../Category/百老匯音樂劇.md "wikilink")
[Category:ABBA](../Category/ABBA.md "wikilink")
[Category:1999年音樂劇](../Category/1999年音樂劇.md "wikilink")