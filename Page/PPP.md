**PPP**，可能指：

## 商業法、經濟學

  - [購買力平價](../Page/購買力平價.md "wikilink")（），一种表示计算货币的相对价值的方法。

  - [污染者自付原則](../Page/污染者自付原則.md "wikilink")（英語：polluter-pays
    principle，略稱PPP）

  - PPP模式（英語：Public-private
    partnership），譯作[政府和社會資本合作](../Page/政府和社會資本合作.md "wikilink")、民間參與公共建設、公私合作夥伴關係、公用事業市場化、公用事業民營化等，或公用事業市場化，公共基础设施项目的一种资助模式。

  - ，通常指是Profit（Economic）、People（Social），和Planet（Environmental）。

## 醫學

  - [陰莖珍珠狀丘疹](../Page/陰莖珍珠狀丘疹.md "wikilink")，又稱為珍珠狀陰莖丘疹、陰莖珍珠樣丘疹、[珍珠樣陰莖丘疹](../Page/珍珠樣陰莖丘疹.md "wikilink")（Pearly
    penile papules）、陰莖多毛樣乳頭瘤、冠狀溝或龜頭丘疹等。

  - （platelet-poor plasma）

  - （Postpartum psychosis）

  - [包皮背切術](../Page/包皮背切術.md "wikilink")（Preputioplasty或prepuce plasty）

  -
  - （），又稱為進行性色素性紫斑或慢性色素性紫斑（Purpura pigmentosa
    progressiva），或色素性紫斑（Pigmented purpura）等等。

## 科學與科技

  - [點對點協議](../Page/點對點協議.md "wikilink")（），網際網路的数据链路层使用的一种协议。

  - [聚对亚苯](../Page/聚对亚苯.md "wikilink")（）

  - [戊糖磷酸途径](../Page/戊糖磷酸途径.md "wikilink")（）

  - （Pariser–Parr–Pople
    method），應用於[分子物理學](../Page/分子物理學.md "wikilink")、[計算化學](../Page/計算化學.md "wikilink")。

  - ，科學期刊。

## 政治

  - [巴基斯坦人民党](../Page/巴基斯坦人民党.md "wikilink")（；，缩写为PPP）
  - [人民進步黨](../Page/人民進步黨.md "wikilink")（），马来西亚的一个政党。
  - [公共政策民調基金會](../Page/公共政策民調基金會.md "wikilink")（），一間美國的民意調查公司。