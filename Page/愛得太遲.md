《**愛得太遲**》是[香港歌手](../Page/香港.md "wikilink")[古巨基在](../Page/古巨基.md "wikilink")2006年的[粵語歌曲](../Page/粤语流行歌曲.md "wikilink")，收录于《[Human我生](../Page/Human我生.md "wikilink")》，另有[古巨基及](../Page/古巨基.md "wikilink")[周慧敏合唱版本](../Page/周慧敏.md "wikilink")、国语版《爱得太晚》。此外，《愛得太遲》也是由古巨基、周慧敏及其他藝人參演的[音樂電影](../Page/音樂電影.md "wikilink")。

這首歌大受歡迎，為2000年代迄今為止，第二首（第一首為1994年[黎明的](../Page/黎明.md "wikilink")《那有一天不想你》）能夠全數奪取香港四大電子傳媒的年度最佳金曲獎，包括叱咤樂壇至尊歌曲大獎、勁歌金曲金曲金獎、新城勁爆年度歌曲大獎、港台全球華人至尊金曲等。此外帶摯古巨基獲得多個大獎\[1\]\[2\]\[3\]\[4\]\[5\]\[6\]\[7\]\[8\]。

## 歌曲

原曲為《不過是人》，為[宇宙大爆炸旗下創作人](../Page/宇宙大爆炸.md "wikilink")[楊鎮邦所創作](../Page/楊鎮邦.md "wikilink")，後來賣予[金牌娛樂](../Page/金牌娛樂.md "wikilink")，由[林夕填詞](../Page/林夕.md "wikilink")、[雷頌德編曲和監製](../Page/雷頌德.md "wikilink")、（[古巨基](../Page/古巨基.md "wikilink")）主唱。

歌詞背後的故事屬[古巨基一朋友之真實經歷](../Page/古巨基.md "wikilink")，[古巨基盼望藉](../Page/古巨基.md "wikilink")[林夕的歌詞勸導香港人別因工作過忙而愛得太遲](../Page/林夕.md "wikilink")。林夕指這首歌創作過程十分辛苦，還因這首歌第一次和雷頌德爭執，被雷掛斷電話。

## 各傳媒流行周榜之最高位置

|           |          |         |         |
| --------- | -------- | ------- | ------- |
| | **903** | **RTHK** | **997** | **TVB** |
| 最高位置      | **1**    | **(1)** | **(1)** |

**粗體** 表示 冠軍歌，**(1)**代表兩週冠軍

## 本曲獎項

  - 2006年度四台聯頒音樂大獎–傳媒大獎
  - 2006年度四台聯頒音樂大獎–歌曲獎（作曲人）
  - 叱咤樂壇頒獎典禮至尊金曲
  - 叱咤樂壇我最喜愛的歌曲
  - 新城勁爆卡拉OK歌曲 (合唱版)
  - 新城勁爆播放指數大獎
  - 新城勁爆年度歌曲大獎
  - TVB十大勁歌金曲最佳作曲
  - TVB十大勁歌金曲頒獎典禮金曲金獎
  - TVB十大勁歌金曲頒獎典禮最受歡迎廣告歌曲大獎（銀獎）
  - TVB十大勁歌金曲頒獎典禮最佳填詞
  - TVB十大勁歌金曲頒獎典禮最佳歌曲監製
  - 香港電台十大中文金曲頒獎典禮全球華人至尊金曲獎
  - RoadShow至尊歌曲
  - SINA Music樂壇民意指數頒獎禮全年最高收聽率歌曲
  - SINA Music 樂壇民意指數頒獎禮 我最喜愛至尊金曲
  - 雅虎香港Yahoo\! 搜尋人氣大獎頒獎禮「十大人氣歌曲和金曲」獎
  - 9+2音樂先鋒榜頒獎典禮：金曲金獎
  - 雪碧中國原創音樂流行榜頒獎禮全國至尊金曲大獎

## 音樂電影

  - 導演：古巨基 / Ricky Hayashi
  - 故事 / 編劇：鄧潔明
  - 監製：黃仲凱
  - 副導演：林偉匡
  - 攝影：張志偉
  - 配樂：李漢文
  - 美術：Ricky Hayashi
  - 化妝：Maggie Choy

<!-- end list -->

  - [古巨基](../Page/古巨基.md "wikilink") 飾演Key/Toro
  - [楊愛瑾飾演Keke](../Page/楊愛瑾.md "wikilink")/妹頭
  - [周汶錡飾演Kathy](../Page/周汶錡.md "wikilink")
  - [陳國坤飾演Otto](../Page/陳國坤.md "wikilink")/八爪魚
  - [Marian L飾演Z](../Page/Marian_L.md "wikilink")/Zita
  - [林海峰飾演](../Page/林海峰_\(香港\).md "wikilink") 九哥
  - [周慧敏飾演](../Page/周慧敏.md "wikilink") 花店東主
  - [胡楓飾演David](../Page/胡楓.md "wikilink")/Kathy的舅公
  - [紅耳龜飾演](../Page/紅耳龜.md "wikilink") 茫痴痴

## 参考文献

<div class="references-small">

<references />

</div>

## 外部參考

  - [Youtube:古巨基@《愛得太遲》音樂電影版](http://www.youtube.com/watch?v=joJTKAJiMzo&feature=related)
  - [愛得太遲@維基之點擊率](https://web.archive.org/web/20160411051020/http://stats.grok.se/zh/200805/%E6%84%9B%E5%BE%97%E5%A4%AA%E9%81%B2)

[Category:中文流行歌曲](../Category/中文流行歌曲.md "wikilink")
[Category:香港歌曲](../Category/香港歌曲.md "wikilink")
[Category:叱咤樂壇我最喜愛的歌曲](../Category/叱咤樂壇我最喜愛的歌曲.md "wikilink")
[Category:2006年單曲](../Category/2006年單曲.md "wikilink")
[Category:粤语歌曲](../Category/粤语歌曲.md "wikilink")

1.  古巨基於2006年取得香港樂壇最高榮譽大獎之一《四台聯頒傳媒大獎》
2.  [古巨基《爱得太迟》获全球华人至尊金曲（图）_影音娱乐_新浪网](http://ent.sina.com.cn/y/2007-01-27/22221428798.html)
3.  [古巨基获颁四台联颁传媒大奖（图）_影音娱乐_新浪网](http://ent.sina.com.cn/y/2007-01-27/22231428809.html)
4.  [第29届十大中文金曲颁奖礼完全获奖名单_影音娱乐_新浪网](http://ent.sina.com.cn/y/2007-01-27/19121428310.html)
5.  [2006年度TVB十大劲歌金曲颁奖礼完全获奖名单_影音娱乐_新浪网](http://ent.sina.com.cn/y/2007-01-13/19191408447.html)
6.  [2006香港新城劲爆颁奖礼完全获奖名单_影音娱乐_新浪网](http://ent.sina.com.cn/y/2006-12-26/19101387324.html)
7.  [2006叱咤乐坛流行榜颁奖礼完全获奖名单_影音娱乐_新浪网](http://ent.sina.com.cn/y/2007-01-01/20241395100.html)
8.  [2006年度香港TVB8金曲榜完全获奖名单_影音娱乐_新浪网](http://ent.sina.com.cn/y/2006-12-17/19131373424.html)