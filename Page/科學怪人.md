《**科學怪人**》（），又译作《弗兰肯斯坦》（*Frankenstein*），是西方文学中的第一部[科学幻想小说](../Page/科学幻想小说.md "wikilink")，诞生于[日内瓦湖畔](../Page/日内瓦湖.md "wikilink")，出自[玛丽·雪莱之手](../Page/玛丽·雪莱.md "wikilink")。最初出版於1818年，較為普及的版本是1831年印行的第三版，屬於受到[浪漫主義影響的](../Page/浪漫主義.md "wikilink")[哥特小说](../Page/哥特小说.md "wikilink")。後世有部份學者認為這部小說可視為[恐怖小說或](../Page/恐怖小說.md "wikilink")[科幻小說的始祖](../Page/科幻小說.md "wikilink")；不過，目前已知1764年出版的《[奧特蘭托堡](../Page/奧特蘭托堡.md "wikilink")》（The
Castle of
Otranto）方為第一部[哥特小說或](../Page/哥特小說.md "wikilink")[恐怖小說](../Page/恐怖小說.md "wikilink")。弗兰肯斯坦是故事中的瘋狂醫生，因為以科學的方式使死屍復活，所以中文版译作《科學怪人》。而那个[人造人稱為](../Page/人造生命.md "wikilink")「[弗兰肯斯坦的怪物](../Page/弗兰肯斯坦的怪物.md "wikilink")」。

## 劇情提要

故事主要描述一個科學家的瘋狂計劃，[維多·法蘭克斯坦計劃要靠自己的力量創造一個生命體](../Page/維多·法蘭克斯坦.md "wikilink")，说是想打造一个完美的人。於是，他從墳場精挑細選後挖出[屍塊](../Page/屍體.md "wikilink")，以專業知識判斷還能使用哪部分，再將之拼成人型，通过电击賦予他生命。不久，弗蘭肯斯坦便發現這是個嚴重的錯誤，他製造了一個[怪物](../Page/弗蘭肯斯坦的怪物.md "wikilink")。於是，他開始追殺這個怪物，怪物也本能地逃亡，雙方發生多次衝突。弗兰克（Frank
monster）杀死了法兰克斯坦的弟弟。

## 改編

[FrankensteinDraft.jpg](https://zh.wikipedia.org/wiki/File:FrankensteinDraft.jpg "fig:FrankensteinDraft.jpg")
後世有許多改編自「科學怪人」的作品，但大多與原作所描述的怪人一樣，頭上滿佈疤痕、強壯又高大（2.5米），脸色枯黄，眼窝深陷，两片嘴唇呈直溜溜的一条黑线，而怪物的脖子或太陽穴通常都有大螺絲，一般認為這是弗蘭肯斯坦為了增強導電使之復活而裝上的，上述的設定使得它更像個怪物；也有將科學怪人改編成面惡心善的作品，這種心地善良的怪物通常被稱為「Frankenstein」，但其實在原作中，這怪物是沒有名字的，這怪物只被稱為“怪物”（the
monster），而製造他的人是有名字的，叫Victor
Frankenstein。這類作品多半改編成卡通，但在卡通中，弗蘭肯有點像客串的角色。怪物的名字「弗蘭肯」（Franken）多被認為是取自弗蘭肯斯坦的原文名稱“Frankenstein”。

## 衍生作品

《科学怪人》发表后，出现大量重写或续写的[作品](../Category/弗兰肯斯坦题材的作品.md "wikilink")、[电影](../Page/科学怪人_\(电影\).md "wikilink")、戏剧、电视作品等。

## 参见

  - [弗兰肯斯坦的怪物](../Page/弗兰肯斯坦的怪物.md "wikilink")

  - [人造人](../Page/人造人.md "wikilink")

  - [隐形人](../Page/隐形人.md "wikilink")

  - [變蠅人](../Page/變蠅人.md "wikilink")

  -
  - [蓋拉](../Page/蓋拉.md "wikilink")

  - [山達](../Page/山達.md "wikilink")

  - [何蒙庫魯茲](../Page/何蒙庫魯茲.md "wikilink")

  -
  -
  - [弗兰肯斯坦题材的作品列表](../Page/弗兰肯斯坦题材的作品列表.md "wikilink")

## 参考资料

## 外部連結

  -
  - [雪莱年表及资源](http://www.rc.umd.edu/reference/chronologies/mschronology/mws.html)

  - [弗兰肯斯坦怪物](http://frankenstein.monstrous.com)

  - [玛丽雪莱的弗兰肯斯坦](http://www.maryshelley.nl)

[F](../Category/科幻小说.md "wikilink")
[Category:德國背景作品](../Category/德國背景作品.md "wikilink")
[Category:1818年長篇小說](../Category/1818年長篇小說.md "wikilink")
[\*](../Category/弗兰肯斯坦.md "wikilink")