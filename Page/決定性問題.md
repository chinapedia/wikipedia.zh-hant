在[可計算性理論與](../Page/可計算性理論.md "wikilink")[計算複雜性理論中](../Page/計算複雜性理論.md "wikilink")，所謂的**決定性問題**（）是一個在某些[形式系統回答](../Page/形式系統.md "wikilink")**是或否**的問題。例如：**「給兩個數字x與y，x是否可以整除y？」**便是決定性問題，此問題可回答是或否，且依據其x與y的值。

決定性問題與[功能性問題](../Page/功能性問題.md "wikilink")（，或[複雜型問題](../Page/複雜型問題.md "wikilink")）密切相關，功能性問題的答案內容，較簡單的是與非複雜許多。範例問題：「給予一個正整數x，則哪些數可整除x？」

另一個與上述兩類問題相關的是[最佳化問題](../Page/最佳化問題.md "wikilink")（），此問題關心的是尋找特定問題的最佳答案。

解決決定性問題的方法稱為**決策程式**或[演算法](../Page/演算法.md "wikilink")。一個針對決定性問題的演算法將說明給予參數x和y的情況下如何決定x是否整除y。若是某些決定性問題可以被一些演算法所解決，則稱此問題**可決定**。

計算複雜度的領域中，分類可決定問題的依據在於**此問題有多難被解決**。在此標準下，所謂的**難**是以解決某問題最有效率的演算法所花費的[計算資源為依據](../Page/計算資源.md "wikilink")。在[遞迴理論中](../Page/递归论.md "wikilink")，非決定性問題由[圖靈度決定](../Page/圖靈度.md "wikilink")，指的是一種在任何解答中隱含的不可計算性量詞。

計算性理論的研究集中在決定性問題上。在[與功能性問題的等值問題中](../Page/#與功能性問題的等值問題.md "wikilink")，並沒有失去其普遍性。

## 定義

**決定性問題**指的是在一個數量為無限大的輸入集合中，可產出任何是或非解答的問題之集合。因此傳統上定義決定性問題，乃依其解答為**是**的輸入之集合。在此情形下，一決定性問題亦等於一[形式語言](../Page/形式語言.md "wikilink")。

形式上，決定性問題是一自然數[子集](../Page/子集.md "wikilink")**A**。藉由使用[哥德尔数](../Page/哥德尔数.md "wikilink")，也可學習諸如形式語言的其他集合。非正規的定義決定性問題，就是判別一個給予的數字是否在此集合內。

一決定性問題若其**A**是一個[递归集合](../Page/递归集合.md "wikilink")，則稱做**可決定的**（decidable）或**有效可解**（effectively
solvable）。若其**A**是一[递归可枚举集合則稱為](../Page/递归可枚举集合.md "wikilink")**部分可決定的**（partially
decidable）、**半可決定的**（semidecidable）、**可解的**（solvable）或**可證明**（provable）。除此之外，此問題稱為**不可決定的**。

## 例子

一個經典可決定的決定性問題是質數問題。藉由測試每一個可能的因數，有可能**有效決定**一個自然數是否為質數。儘管存在很多效能更佳的[質數判定方法](../Page/素性測試.md "wikilink")，任何有效方法的存在就已足夠建立可決定性。

重要的不可決定的決定性問題包括[停機問題](../Page/停機問題.md "wikilink")，其他請見[不可決定的問題列表](../Page/不可決定的問題列表.md "wikilink")。在[計算複雜性理論中](../Page/計算複雜性理論.md "wikilink")，[完備的決定性問題通常用來判別其他決定性問題的複雜度類別](../Page/完備_\(複雜度\).md "wikilink")。重要的實例包括[SAT問題與其數變種](../Page/布爾可滿足性問題.md "wikilink")，還有[無向與](../Page/SL_\(複雜度\).md "wikilink")[有向圖可達性問題](../Page/有向圖可達性問題.md "wikilink")。

## 歷史

德语“*[Entscheidungsproblem](../Page/Entscheidungsproblem.md "wikilink")*”，亦即“判定性问题”（Decision-problem），最早出自于[大衞·希尔伯特的话](../Page/大衞·希尔伯特.md "wikilink")：“在1928年的会议上，[希尔伯特精确地描述了他的问题](../Page/希尔伯特.md "wikilink")。首先，数学是否具有[完备性](../Page/完备性.md "wikilink")？……其次，数学是否具有[相容性](../Page/相容性.md "wikilink")？……再次，数学是否具有[判定性](../Page/判定性.md "wikilink")？这些问题的意思是，是否存在这样一种确定的方法，在理论上可适用于任何假设，并且能够保证对无论是否正确的假设都能给出一个正确的结果”（Hodeges，p.
91）。[希尔伯特相信](../Page/希尔伯特.md "wikilink")“在数学上没有‘[ignorabimus](../Page/ignorabimus.md "wikilink")’”，亦即“我们将无从得之”。需要了解更多信息请参见[大衞·希尔伯特和](../Page/大衞·希尔伯特.md "wikilink")[停机问题](../Page/停机问题.md "wikilink")。

## 與函數問題的等價性

## 參考

  - [Hodges, A.](../Page/Andrew_Hodges.md "wikilink"), *Alan Turing: The
    Enigma*, Simon and Schuster, New York. Cf Chapter "The Spirit of
    Truth" for some more history that led to Turing's work.

<!-- end list -->

  -

      -
        Hodges references a biography of [David
        Hilbert](../Page/David_Hilbert.md "wikilink"): [Constance
        Reid](../Page/Constance_Reid.md "wikilink"), *Hilbert*（George
        Allen & Unwin; Springer-Verlag, 1970）. There are apparently more
        recent editions.

<!-- end list -->

  - Kozen, D.C.（1997）, *Automata and Computability*, Springer.
  - Hartley Rogers, Jr., *The Theory of Recursive Functions and
    Effective Computability*, MIT Press, ISBN 0-262-68052-1 (paperback),
    ISBN 0-07-053522-1
  - Sipser, M.（1996）, *Introduction to the Theory of Computation*, PWS
    Publishing Co.
  - Robert I. Soare (1987), *Recursively Enumerable Sets and Degrees*,
    Springer-Verlag, ISBN 0-387-15299-7

[J](../Category/數理邏輯.md "wikilink")
[Category:計算理論](../Category/計算理論.md "wikilink")
[Category:計算問題](../Category/計算問題.md "wikilink")
[Category:递归论](../Category/递归论.md "wikilink")
[Category:計算複雜性理論](../Category/計算複雜性理論.md "wikilink")