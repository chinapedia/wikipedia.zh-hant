[Pacific_place.svg](https://zh.wikipedia.org/wiki/File:Pacific_place.svg "fig:Pacific_place.svg")

**太古廣場**（[英文](../Page/英文.md "wikilink")：**Pacific
Place**，簡稱**PP**），位於[香港](../Page/香港.md "wikilink")[香港島金鐘](../Page/香港島.md "wikilink")[金鐘道](../Page/金鐘道.md "wikilink")88號，是香港一個綜合性商業物業。太古廣場共分3期，有3座辦公大樓、3間[五星級酒店](../Page/五星級酒店.md "wikilink")、一間精緻豪華酒店、[服務式住宅](../Page/服務式住宅.md "wikilink")、商場及停車場等。第1期於1985年動工興建，1988年落成，包括1座辦公大樓及1座酒店，第2期於1988年動工，1991年落成，包括兩座酒店，部份樓層則為辦公室。第3期於2004年落成，包括1座辦公大樓。位於[灣仔](../Page/灣仔.md "wikilink")[皇后大道東](../Page/皇后大道東.md "wikilink")1號的第3期，與第1、2期有地下通道連接。

## 歷史

太古廣場由香港英資地產商[太古地產興建](../Page/太古地產.md "wikilink")。太古廣場1、2期現址，從前為駐港英軍的[域多利兵房](../Page/域多利兵房.md "wikilink")
(Victoria
Camp)，兵房遷出後，地皮被香港政府於1985年4月18日公開拍賣，並由太古地產以7億港元投得。第三期地皮則由收購[灣仔](../Page/灣仔.md "wikilink")[星街一帶舊樓而得](../Page/星街.md "wikilink")，前後共耗時約10多年。而商場部份早年稱為The
Mall，到90年代後期才統一稱為太古廣場。

太古廣場平台的榕樹高度超過20米，據說有過百年歷史。地產商在80年代中建設第1期時，曾向政府建議將榕樹搬到別處，以免妨礙工程進行，但當時的政府強烈反對。最終王歐陽建築事務所的工程人員於是建造了個直徑18米、深10米的「巨型花盆」，保護樹根和泥土。\[1\]

## 结构

太古廣場有3座甲級辦公樓（太古廣場1、2及3期）、4家5星級酒店（香港港麗酒店、港島香格里拉大酒店、香港JW萬豪酒店及奕居）
，還有提供共270個單位的服務式住宅（太古廣場栢舍）。

### 第1期

[Pacific_Place_Tower_1_2014.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Place_Tower_1_2014.jpg "fig:Pacific_Place_Tower_1_2014.jpg")
[Pacific_Place_Tower_1_Office_Lobby_2015.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Place_Tower_1_Office_Lobby_2015.jpg "fig:Pacific_Place_Tower_1_Office_Lobby_2015.jpg")
太古廣場1座為寫字樓，於1988年6月落成，9月獲發入伙紙，樓高36層，總樓面面積逾863,000平方呎，每樓層均提供21,000至22,500平方呎可租用的樓面面積，太古集團香港總辦事處亦設於33樓，其他主要租戶包括[德勤‧關黃陳方會計師行](../Page/德勤.md "wikilink")（35-40樓）、[里昂證券](../Page/里昂證券.md "wikilink")（18樓）及大和資本市場（28樓）等。大廈設20部高速客用電梯，大堂於2013年至2014年進行翻新工程。

#### 主要租戶

  - [蘇富比藝術空間](../Page/蘇富比.md "wikilink")（5樓）
  - [遠洋地產](../Page/遠洋地產.md "wikilink")（香港）有限公司（601室）
  - 中楷證券有限公司（606室）
  - [徐嘉慎律師事務所](../Page/徐嘉慎.md "wikilink")（608室）
  - [米高蒲志國際](../Page/米高蒲志國際.md "wikilink")（香港）有限公司（611室）
  - [保利藝術空間](../Page/保利.md "wikilink")（701室至708室）
  - [億康先達國際有限公司](../Page/億康先達國際.md "wikilink")（801室）
  - [東建國際](../Page/東建國際.md "wikilink")（811室）
  - [東方匯理資產管理香港有限公司](../Page/東方匯理資產管理香港有限公司.md "wikilink")（901室至908室）
  - [Mega Expo](../Page/Mega_Expo.md "wikilink")（911室至912室）
  - [中民築友科技集團有限公司](../Page/中民築友科技.md "wikilink")（1001室至1004室）
  - [宮崎銀行](../Page/宮崎銀行.md "wikilink")（1005室）
  - [日本銀行](../Page/日本銀行.md "wikilink")（1012室）
  - [霍金路偉律師行](../Page/霍金路偉律師行.md "wikilink")（11樓）
  - [西盟斯律師行](../Page/西盟斯律師行.md "wikilink")（13樓）
  - [國際金融公司東亞及太平洋地區辦事處及](../Page/國際金融公司.md "wikilink")[世界銀行東亞及太平洋地區私營發展部辦事處](../Page/世界銀行.md "wikilink")（14樓）
  - [延長石油國際](../Page/延長石油國際.md "wikilink")（1512室）
  - [Temple Chambers](../Page/Temple_Chambers.md "wikilink")（16樓）
  - [Erste Group Bank
    AG](../Page/Erste_Group_Bank_AG.md "wikilink")（1710室至1712室）
  - [里昂證券](../Page/里昂證券.md "wikilink")（18樓）
  - [邦瀚斯藝術廊](../Page/邦瀚斯.md "wikilink")（20樓）
  - [旭輝集團](../Page/旭輝集團.md "wikilink")（2002室至2003室）
  - Jason Pow SC's Chambers（2006室）
  - [養和醫健專科中心](../Page/養和醫健專科中心.md "wikilink")（21至23樓共兩層半5.1萬平方呎樓面）
  - [貝克·麥堅時](../Page/貝克·麥堅時.md "wikilink")（23樓）
  - [穆迪](../Page/穆迪.md "wikilink")（24樓）
  - [千葉銀行](../Page/千葉銀行.md "wikilink")（2510室）
  - [大和証券](../Page/大和証券.md "wikilink")（26樓）
  - [澳洲國民銀行](../Page/澳洲國民銀行.md "wikilink")（27樓）
  - [大和資本市場](../Page/大和資本市場.md "wikilink")（28樓）
  - [華融國際金融](../Page/華融國際金融.md "wikilink")（29樓）
  - [中信資源控股有限公司](../Page/中信資源.md "wikilink")（3001-3006室）
  - [德匯律師事務所](../Page/德匯律師事務所.md "wikilink")（3008室）
  - [中國海外宏洋集團](../Page/中國海外宏洋集團.md "wikilink")（3012室）
  - [德勤‧關黃陳方會計師行](../Page/德勤.md "wikilink")（35-40樓）
  - [上海浦東發展銀行](../Page/上海浦東發展銀行.md "wikilink")（32樓）
  - [香港太古集團有限公司](../Page/香港太古集團有限公司.md "wikilink")（33樓）

至於1989年初開幕的[香港JW萬豪酒店](../Page/香港JW萬豪酒店.md "wikilink")，最高十層樓面（36至50樓）被改裝成酒店式住宅「曦暹軒」，提供136間酒店式住宅，面積由606平方呎至3,388平方呎。\[2\]

2009年10月，「曦暹軒」部份改建為精緻豪華酒店[奕居](../Page/奕居.md "wikilink")，由AFSO設計事務所的室內設計師傅厚民負責設計，現設有117間寬敞客房，包括21間套房及2間頂層套房，酒店49樓的Sky
Bridge 位於40米高的中庭之上，並設有天窗，引領賓客進入Sky Lounge及酒店餐廳及酒吧Café Gray
Deluxe。而6樓設「The Lawn」室外草坪供酒店住客享用。\[3\]

<File:Pacific> Place Tower 1 Office Tower Lobby
2010.jpg|太古廣場一座寫字樓大堂（翻新前） <File:One>
Pacific Place Office Tower Lobby 201405.jpg|太古廣場一座寫字樓大堂（翻新中） <File:JW>
Marriott Hotel Hong Kong
2014.jpg|[香港JW萬豪酒店](../Page/香港JW萬豪酒店.md "wikilink")
<File:Upper> House Hotel View
201212.jpg|[奕居車道入口](../Page/奕居.md "wikilink")

### 第2期

第2期設有2座高樓大廈及購物商場，於1991年落成，包括[香港港麗酒店和](../Page/香港港麗酒店.md "wikilink")[港島香格里拉酒店](../Page/港島香格里拉酒店.md "wikilink")。其中[港島香格里拉酒店位於](../Page/港島香格里拉酒店.md "wikilink")5樓至7樓及41樓至56樓，餘下的樓層為共27層的太古廣場二座寫字樓部份。2座主要租戶為建銀國際（34樓，已遷往[中國建設銀行大廈](../Page/中國建設銀行大廈.md "wikilink")）、凱雷投資集團（13樓及28樓）、施羅德投資管理（33樓）及FIL
Asia Holdings PTE. Limited等。

[香港港麗酒店餘下的樓層](../Page/香港港麗酒店.md "wikilink")，為服務式住宅「太古廣場栢舍」，位於11至37樓，設270個1房、2房及3房單位。

#### 主要租戶

  - [東方匯理資產管理香港有限公司](../Page/東方匯理.md "wikilink")（9樓）
  - [中國國際基金有限公司](../Page/中國國際基金有限公司.md "wikilink")（1003室至1006室）
  - [I.G. INVESTMENT MANAGEMENT (HONG KONG)
    LIMITED](../Page/I.G._INVESTMENT_MANAGEMENT_\(HONG_KONG\)_LIMITED.md "wikilink")（1011室至1012室）
  - [南南合作金融中心](../Page/南南合作金融中心.md "wikilink")（1102室至1106室）
  - [Visa Hong Kong
    Limited](../Page/Visa_Hong_Kong_Limited.md "wikilink")（1113室至1119室）
  - [NH Investment & Securities(H.K.)
    Ltd.](../Page/NH_Investment_&_Securities\(H.K.\)_Ltd..md "wikilink")（1201室）
  - [Moelis &
    Company](../Page/Moelis_&_Company.md "wikilink")（1203室至1210室）
  - [德美利證券香港](../Page/德美利證券.md "wikilink")（TD Ameritrade）（1211室至1213室）
  - [Lloyd's](../Page/Lloyd's.md "wikilink")（1220室）
  - [Asset Management One Hong Kong
    Limited](../Page/Asset_Management_One_Hong_Kong_Limited.md "wikilink")（1221室至1222室）
  - [中國寶豐（國際）有限公司](../Page/中國寶豐（國際）有限公司.md "wikilink")（1301室至1304室）
  - [凱雷投資集團](../Page/凱雷.md "wikilink")（13樓及28樓）
  - [友利銀行](../Page/友利銀行.md "wikilink")（1401室）
  - [Heidrick &
    Struggles](../Page/Heidrick_&_Struggles.md "wikilink")（1408室）
  - [沙伯基礎創新塑料香港有限公司](../Page/沙伯基礎創新塑料香港有限公司.md "wikilink")（1418室）
  - [Denis Chang's
    Chambers](../Page/Denis_Chang's_Chambers.md "wikilink")（1501室）
  - [Interactive
    Brokers](../Page/Interactive_Brokers.md "wikilink")（1512室）
  - [瑞穗實業銀行](../Page/瑞穗實業銀行.md "wikilink")（17樓，已遷出）
  - [北美信託香港有限公司](../Page/北美信託.md "wikilink")（1901室）
  - [MCP Asset
    Management](../Page/MCP_Asset_Management.md "wikilink")（1918室）
  - [Millennium Fund Services (Asia)
    Limited](../Page/Millennium_Fund_Services_\(Asia\)_Limited.md "wikilink")（1920室）
  - [富達基金](../Page/富達基金.md "wikilink")（香港）有限公司（21樓）
  - [Eight Roads Capital
    Advisors](../Page/Eight_Roads_Capital_Advisors.md "wikilink")（2201室）
  - [多倫多道明銀行](../Page/多倫多道明銀行.md "wikilink")（2210室）
  - [東方匯理銀行](../Page/東方匯理銀行.md "wikilink")（27樓）
  - [東方匯理私人銀行香港分行](../Page/東方匯理私人銀行.md "wikilink")（29樓）
  - [Parkside Chambers](../Page/Parkside_Chambers.md "wikilink")（3101室）
  - [施羅德投資管理](../Page/施羅德集團.md "wikilink")（香港）有限公司（33樓）
  - [太古股份有限公司](../Page/太古股份.md "wikilink")'B'（35樓）
  - [紅杉資本](../Page/紅杉資本.md "wikilink")（Sequoia Capital）（3613室）

<File:HK> Admiralty facades 港島香格里拉酒店 Isaland Shangri-la 金鐘道政府合署
Queensway Government Offices
Oct-2011.jpg|[港島香格里拉酒店及太古廣場二座](../Page/港島香格里拉酒店.md "wikilink")（圖左）
<File:Conrad> Hong Kong 2012.jpg|[香港港麗酒店](../Page/香港港麗酒店.md "wikilink")
<File:HK> Pacific Place Level 5 Corridor 2012.jpg|5樓通道 <File:HK> Pacific
Place Banyan Tree 2012.jpg|太古廣場平台大榕樹 <File:HK> Pacific Place no 2
garden.jpg|太古廣場平台 <File:Two> Pacific Place Lobby 201106.jpg|翻新前二座入口大堂

### 第3期

第3期位於灣仔星街小區，為2004年落成的太古廣場3座寫字樓，樓高34層。地庫設行人隧道直達太古廣場及港鐵[金鐘站F出口](../Page/金鐘站.md "wikilink")。

#### 主要租戶

  - [德事商務中心](../Page/德事商務中心.md "wikilink")（3樓）
  - [Allison
    Partners](../Page/Allison_Partners.md "wikilink")（310室-311室）
  - [Bird & Bird 鴻鵠律師事務所](../Page/Bird_&_Bird_鴻鵠律師事務所.md "wikilink")（4樓）
  - [飛利浦](../Page/飛利浦.md "wikilink")（5-7樓）
  - [仲量聯行企業評估及諮詢有限公司](../Page/仲量聯行.md "wikilink")（6樓）
  - [法國興業銀行](../Page/法國興業銀行.md "wikilink")（7、19、28樓及34-38樓）
  - [Goodman](../Page/Goodman.md "wikilink")（901室）
  - [中國海外發展有限公司](../Page/中國海外發展有限公司.md "wikilink")（10-11樓）
  - [紐約梅隆銀行](../Page/紐約梅隆銀行.md "wikilink")（12、14及24樓）
  - [富國銀行](../Page/富國銀行.md "wikilink")（13及27樓全層）
  - [未來資產環球投資](../Page/未來資產環球投資.md "wikilink")（香港）有限公司（Mirae Asset）（15樓）
  - [TOD'S](../Page/TOD'S.md "wikilink")（16樓）
  - [雅居樂集團控股有限公司](../Page/雅居樂.md "wikilink")（18樓）
  - [Lipman Karas](../Page/Lipman_Karas.md "wikilink")（23樓）
  - [伊泰煤炭](../Page/伊泰煤炭.md "wikilink")（28樓）
  - [騰訊控股有限公司](../Page/騰訊控股.md "wikilink")（29樓）
  - [金英証券](../Page/金英証券.md "wikilink")（香港）有限公司（30樓）
  - [柏瑞投資亞洲有限公司](../Page/柏瑞投資亞洲有限公司.md "wikilink")（31樓）
  - [荷蘭合作银行](../Page/荷蘭合作银行.md "wikilink") （32樓）

<File:Three> Pacific Place 200808.jpg|太古廣場3座 <File:Three> Pacific Place
Office Lobby View 2008.JPG|太古廣場3座寫字樓大堂 <File:Three> Pacific Place Public
Open Space 2008.jpg|太古廣場3座公共空間 <File:HK> Three Pacific Place MTR
Subway.jpg|連接太古廣場三期行人隧道

## 商場

[HK_Pacific_Place_Interior_1991.jpg](https://zh.wikipedia.org/wiki/File:HK_Pacific_Place_Interior_1991.jpg "fig:HK_Pacific_Place_Interior_1991.jpg")
[Pacific_Place_Phase_1_Void_201702.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Place_Phase_1_Void_201702.jpg "fig:Pacific_Place_Phase_1_Void_201702.jpg")
[Pacific_Place_LG1_Carpark_Lobby_2009.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Place_LG1_Carpark_Lobby_2009.jpg "fig:Pacific_Place_LG1_Carpark_Lobby_2009.jpg")
[The_Beauty_Gallery_20121228.jpg](https://zh.wikipedia.org/wiki/File:The_Beauty_Gallery_20121228.jpg "fig:The_Beauty_Gallery_20121228.jpg")
[Pacific_Place_Washroom_201108.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Place_Washroom_201108.jpg "fig:Pacific_Place_Washroom_201108.jpg")
[Pacific_Place_Level_3_Access_200912.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Place_Level_3_Access_200912.jpg "fig:Pacific_Place_Level_3_Access_200912.jpg")

太古廣場的商場部份共有5層，現時擁有約160家名店及設Harvey
Nichols百貨公司。商場裡多個大堂，是舉行各類展覽與時裝表演的場地。商場設有行人天橋連接[金鐘廊與](../Page/金鐘廊.md "wikilink")[統一中心](../Page/統一中心.md "wikilink")，亦有地下行人通道連接[香港鐵路](../Page/香港鐵路.md "wikilink")[金鐘站及太古廣場第](../Page/金鐘站.md "wikilink")3期。商場另設有行人電梯連接[香港公園](../Page/香港公園.md "wikilink")。

  - **地庫 （LG1）**

太古廣場第一期地庫在早年設有多間餐廳，當時包括高級中餐廳采蝶軒、[Oliver Super
Sandwiches快餐店](../Page/Oliver_Super_Sandwiches.md "wikilink")、[美心旗下的北京樓及錦江春中餐廳及兩間西餐廳](../Page/美心.md "wikilink")。到了1995年6月，Oliver
Super Sandwiches改為EAT美式快餐店，其中一間餐廳轉為La Fourchette
西餐廳。到了2004年，EAT結業，店舖變更為Café
MET（原名：Metropolitan Café）美式快餐店。

2008年起，地庫進行租戶重租工程，采蝶軒結業後變更為Thai Basil，采蝶軒搬遷至北京樓及錦江春位置。北京樓12月在Thai
Basil原址重開。La Fourchette
西餐廳於2009年8月結業，店舖變更為Zelo地中海菜餐廳，於2009年12月開業。采蝶軒於2013年8月結業，店舖變更為天一酒家，並於2013年12月開業。美心[m.a.x.
concepts旗下的泰式及日式主題餐廳Thai](../Page/m.a.x._concepts.md "wikilink")
Basil及Kokomi分別於2002及2015年開業。

另外，廣場第2期地庫於2000年5月前為西武「COO」美食廣場及超級市場。到了2001年1月變更為高級超級市場[great](../Page/Great.md "wikilink")，佔地34,000平方呎。第二期地庫其餘地方為停車場。

  - **L1**

太古廣場L1設有多間時裝品牌，包括[Agnès b.](../Page/Agnès_b..md "wikilink")、Ted
Baker及Zara等。其他店舖包括Jo Malone、[L'Occitane En
Provence](../Page/L'Occitane_En_Provence.md "wikilink")、[亮視點等](../Page/亮視點.md "wikilink")。

AMC Pacific Place
戲院於2006年12月9日開張，戲院採用科幻設計元素，如懸浮式的售票櫃位設計、像時光隧道的Ｕ形走廊等。戲院共有六間影院，共有超過600個座位，其中一間只有39座位的VIP
影院，更擁有獨立洗手間。

廣場曾設有佔地12,000平方呎馬莎百貨，於1991年開業，不過在2005年6月租約期滿後，卻未獲得續租。馬莎百貨於6月17日結業後，太古地產回收舖位，原址拆細成4個舖位分租。其中一半樓面（逾12,000呎樓面）租予西班牙時裝分銷商[Zara](../Page/Zara.md "wikilink")。其他店舖包括
Wise-Kids及Tak Baker等。

廣場第二期曾設有美食廣場，2001年年底，美食廣場Food
Fare重新裝修，由往日的快餐形象，搖身一變成為「高級餐廳」，裝修後的美食廣場以米黃色為主色，座位增加了20%，共有329個座位；原有的食肆[大家樂](../Page/大家樂.md "wikilink")、Super
BBQ、Patong Thai、Noodle
Place及[KFC等得以保留](../Page/KFC.md "wikilink")，並引入了及Momo日式食肆。但美食廣場於2006年結業，原址變為[連卡佛傢具店](../Page/連卡佛.md "wikilink")。

[香港西武曾經是太古廣場主要租戶之一](../Page/香港西武.md "wikilink")，於1989年第3季開業。不過到了2010年9月，太古地產和[迪生創建宣佈簽訂租務協議](../Page/迪生創建.md "wikilink")，於太古廣場開設Harvey
Nichols旗艦店。隨著香港西武的22年租約於2011年6月屆滿，Harvey Nichols進駐該店位置，並於2011年10月開業。

連卡佛於2012年1月結業後，現時該位置已出租予名店[Burberry](../Page/Burberry.md "wikilink")，餘下部分則分拆出租。\[4\]
而於L1的The Beauty
Gallery亦於2012年8月正式開幕，9個國際美妝及護膚品牌分別進駐，包括Chanel、BEYORG、Giorgio
Armani、Joyce Beauty、La Mer、La Prairie、Natura Bissé、Shu Uemura及Yves Saint
Laurent。

2015年11月12日太古集團已與Burberry就太古廣場重組計劃達成協議，該店面積2.15萬呎，共2層，是Burberry於香港首間、也是其在全球面積最大的旗艦店，土地註冊處資料顯示Burberry於2012年5月開始租用太古廣場舖位，合約期至2018年5月，本年度每月租金為365萬元及店舖收入的15%。按租約計，2017年租金約200萬元。

到2016年2月，開業27年的美式餐廳Dan Ryan's
因不獲續約，將於4月結業，舖位將由一家意大利餐廳進駐。1990年開業的Grappa's
Ristorante亦將於7月搬遷，將由另一香港知名餐飲集團Dining
Concepts承租。同年12月，Burberry決定不續地下層位置，原址改為Pure
Yoga，在2017年9月1日開業。

  - **L2**

太古廣場2樓為高尚時裝品牌集中地，主要包括[Bally](../Page/Bally.md "wikilink")、[I.T](../Page/I.T.md "wikilink")、[Joyce](../Page/Joyce.md "wikilink")、Lanvin、[Montblanc及Sandro等](../Page/Montblanc.md "wikilink")。而咖啡室C'est
La B、Kelly & Walsh及太古廣場首間理髮及美甲店 Il Colpo則於2012年8月正式開幕。同層其他店舖包括King &
Country及中藝。Smythson位於香港的首間專門店於2013年6月進駐太古廣場。本層的北面更設有行人天橋通道，連接[金鐘廊](../Page/金鐘廊.md "wikilink")。

  - **L3**

太古廣場L3為高級名店集中地，包括[Louis
Vuitton](../Page/Louis_Vuitton.md "wikilink")、[Dior](../Page/Dior.md "wikilink")、[Hermès](../Page/Hermès.md "wikilink")、[Gucci](../Page/Gucci.md "wikilink")、[Cartier](../Page/Cartier.md "wikilink")、Goyard及Loewe等。三樓曾經為[連卡佛男裝部](../Page/連卡佛.md "wikilink")，其中6,000平方呎在1999年1月被夜上海中式餐廳取代。其餘租戶包括佔地3,500平方呎意大利名牌服飾零售商[PRADA](../Page/PRADA.md "wikilink")、Chanel.、Loro
Piana等，商戶於1999年1月開業。到了2007年的租戶重租工程後，該處變更為首飾品名店，包括Jaeger-LeCoultre、Piaget、[Tiffany
& Co.及Bulgari](../Page/Tiffany_&_Co..md "wikilink")。

2002年，商場開設[Chanel首間以配飾為主的專門店和亞洲區首間](../Page/Chanel.md "wikilink")
Burberry 概念店，佔地3,200呎。此外，Gucci也把原有店鋪重新裝修擴充至4,000多呎，並新增一個入口。

廣場第1期3樓在商場落成初期曾設有一個室內園林，為顧客提供一個休憩場所。不過該處在1999年6月被高級餐廳Cova取代。

[File:HKPacificPlacePlaseIvoid.jpg|太古廣場第一期中庭Garden](File:HKPacificPlacePlaseIvoid.jpg%7C太古廣場第一期中庭Garden)
Court（2007年）
[File:HKPacificPlacePlase2void.jpg|翻新前的太古廣場Park](File:HKPacificPlacePlase2void.jpg%7C翻新前的太古廣場Park)
Court <File:Pacific> Place Main Void 201405.jpg|翻新後的太古廣場Park Court
<File:Pacific> Place Domani Interior 201405.jpg|Domani高級餐廳（已於2014年5月中結業）
<File:Footbridge> to Pacific Place 2014.jpg|步向太古廣場的行人天橋

## 翻新工程

[Pacific_Place_Garden_Court_200908.jpg](https://zh.wikipedia.org/wiki/File:Pacific_Place_Garden_Court_200908.jpg "fig:Pacific_Place_Garden_Court_200908.jpg")
1998年，太古地產誠邀美籍著名設計師James
Carpenter主理太古廣場翻新工程。工程包括翻新各層洗手間及2至3樓增設了一道全新的玻璃樓梯，方便人流來往。\[5\]

2008年7月，太古地產宣佈耗資15億元為太古廣場進行改建計畫及翻新工程。其中酒店式住宅曦暹軒，改建為酒店「奕居」，提供117個房間，於2009年第4季落成。商場第1階段翻新工程包括改裝地下至2樓洗手間，以流線形木材創造波浪效果、拆卸第一期原有的升降機，並改建為貫穿地庫停車場至四樓的透明升降機，以增加設施的互通性。此外，1期及2期地庫各新建了一個停車場大堂，並有扶手電梯連接，令購物人士更加便利。第1期中庭Garden
Court亦進行翻新，加添了大型吊燈，並重鋪石地台；4樓加建Domani高級餐廳，佔地4,843方呎。第2階段翻新工程於2011年底完成。\[6\]
整個翻新工程由[英國著名設計師](../Page/英國.md "wikilink")[托馬斯·赫斯維克主理](../Page/托馬斯·赫斯維克.md "wikilink")，最終耗資港幣21億元。\[7\]

為配合商場在外觀及內部的全新格調，太古廣場於2009年11月換上新標誌，以取代80年代沿用至今的一款，新標誌以伸延流動的綫條刻劃出太古廣場的名字，圖案包括兩個相連的「P」字母構成的「PP」徽號。

2016年年中，太古廣場重整商鋪組合，1989年開業的Dan Ryan's Chicago Grill、1990年開業的Grappa's
Ristorante被迫搬遷。加強餐飲及消閒娛樂元素，引入逾10間新餐廳和大型連鎖健身中心Pure
Yoga；另外亦針對不同節日引入期間限定商店，期望於零售市道持續轉差下吸引更多人流。\[8\]2018年年中，公布有10間新食肆和零售商舖開幕，包括[無印良品](../Page/無印良品.md "wikilink")、台式珍珠奶茶飲品KIKI茶、美國漢堡店Shake
Shack及旅行箱品牌RIMOWA。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")：

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")、<font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")、<font color="{{南港島綫色彩}}">█</font>[南港島綫](../Page/南港島綫.md "wikilink")：[金鐘站C](../Page/金鐘站.md "wikilink")1、F出口

<!-- end list -->

  - [電車](../Page/香港電車.md "wikilink")

<!-- end list -->

  - 金鐘道（金鐘站對面）

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 相關

  - [中區行人天橋系統](../Page/中區行人天橋系統.md "wikilink")

## [都市傳說](../Page/都市傳說.md "wikilink")

  - **防空洞傳聞**

1941年12月8日上午9時40分，[日本皇軍飛機空襲香港](../Page/日本.md "wikilink")，第1個炸彈是投在[金鐘兵房](../Page/金鐘.md "wikilink")，金鐘兵房就是太古廣場的前身。所以炸死了許多[英國士兵及市民](../Page/英國.md "wikilink")，也有許多日本軍機(资料纪录当时有[九六式陸上攻擊機](../Page/九六式陸上攻擊機.md "wikilink")、[一式陸上攻擊機](../Page/一式陸上攻擊機.md "wikilink")、[一式戰鬥機](../Page/一式戰鬥機.md "wikilink")、[二式戰鬥機](../Page/二式戰鬥機.md "wikilink")、[二式複座戰鬥機](../Page/二式複座戰鬥機.md "wikilink")、[三式戰鬥機](../Page/三式戰鬥機.md "wikilink")、[四式戰鬥機](../Page/四式戰鬥機.md "wikilink"))在附近地區墜機。

**西武防盜系統**

據說西武百貨當年開業時內部的防盜系統是採用紅外線探測物體移動的，但啟用後卻經常誤嗚，保安人員到場時又沒有發現任何物體。由於紅外線防盜系統是必須要有物體移動才會觸動系統，所以西武公司要求保安公司晚上特別進行測試，當晚測試時系統又再誤嗚，保安公司人員在檢查時才發現原來是場內的時裝模特兒公仔觸動系統，自始之後西武決定不再採用紅外線系統。

  - **鬼日軍步操**

據說1990年某天中午，日資百貨公司[西武在太古廣場進行開幕儀式](../Page/西武.md "wikilink")，許多市民、[日本人百貨公司經理及高層店員等人](../Page/日本人.md "wikilink")，看見一隊40多人的日本皇軍在步操更進入了牆壁內，傳聞公司因此做了3天的[日式法事](../Page/日式法事.md "wikilink")。

據說1995年某天深夜有2名保安在百貨公司地庫美食廣場廚房的位置，看見一隊日本皇軍在步操，最恐怖的是有市民在美食廣場廚房看見有一面日本軍旗([日之丸](../Page/日之丸.md "wikilink"))及地上寫有[日文的腳印](../Page/日文.md "wikilink")。

## 參考

## 外部链接

  - [太古廣場](http://www.pacificplace.com.hk/)
  - [萬豪酒店（英語）](http://marriott.com/property/propertypage/HKGDT)
  - [港麗酒店（英語）](https://web.archive.org/web/20030205111124/http://www.conradhotels.com/en/ch/hotels/index.jhtml?ctyhocn=HKGHCCI)
  - [港島香格里拉酒店（英語）](https://web.archive.org/web/20051129092217/http://www.shangri-la.com/hongkong/island/en/index.aspx)
  - [奕居](http://www.upperhouse.com/)

[Category:中西區商場 (香港)](../Category/中西區商場_\(香港\).md "wikilink")
[Category:香港摩天大樓](../Category/香港摩天大樓.md "wikilink")
[Category:金鐘](../Category/金鐘.md "wikilink")
[Category:灣仔](../Category/灣仔.md "wikilink")
[Category:太古地產物業](../Category/太古地產物業.md "wikilink")

1.  [街知巷聞﹕金鐘從前禁止踏入《明報》(2014年5月25日)](http://news.mingpao.com/20140525/uaf1.htm)
2.  [太古地產展示太古廣場酒店式住宅─柏舍─新面貌
    (2004年10月19日)](http://www.swireproperties.com/tc/media-resources/press-releases/2004/pages/swire-properties-relaunches-park-side-of-pacific-place-apartments.aspx)
3.  [從繁囂鬧市登上恬靜舒適的奕居(2009年6月25日)](http://www.upperhouse.com/tc//~/media/Files/TheUpperHouse/pdf/the_press/press_releases/2009-6-25%20A%20Journey%20of%20Upward%20Discovery%20at%20The%20Upper%20House_e.pdf)
4.  星島日報：太古廣場巨鋪分拆出租 2012年1月31日
5.  文匯報：太古廣場租出萬呎樓面 (1998年9月13日)
6.  星島日報：太古廣場優化涉資15億 (2009年11月6日)
7.  [太古地產慶祝太古廣場開業25周年
    (2014年5月21日)](http://www.swireproperties.com/tc/media-resources/press-releases/2014/Pages/SwireProperties_Celebrates_25thAnniversary_of_PacificPlace.aspx)
8.  [有線電視《八點有線財經》，20:56開始
    (2016年12月12日)](http://cablenews.i-cable.com/ci/videopage/program/12257353/)