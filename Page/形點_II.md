[SunYuenLongCentreLOGO.jpg](https://zh.wikipedia.org/wiki/File:SunYuenLongCentreLOGO.jpg "fig:SunYuenLongCentreLOGO.jpg")
[YOHO_Mall_II_Entrance_201509.jpg](https://zh.wikipedia.org/wiki/File:YOHO_Mall_II_Entrance_201509.jpg "fig:YOHO_Mall_II_Entrance_201509.jpg")
[YOHO_Mall_II_Void1_201901.jpg](https://zh.wikipedia.org/wiki/File:YOHO_Mall_II_Void1_201901.jpg "fig:YOHO_Mall_II_Void1_201901.jpg")
[Sun_Yuen_Long_CentreVOID_20070901.jpg](https://zh.wikipedia.org/wiki/File:Sun_Yuen_Long_CentreVOID_20070901.jpg "fig:Sun_Yuen_Long_CentreVOID_20070901.jpg")

**形點
II**（），2015年9月前稱新元朗中心商場，簡稱**新元中**或**SYLC**，是由香港[新鴻基地產發展的一項地產項目](../Page/新鴻基地產.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗朗日路](../Page/元朗.md "wikilink")8號，於1994年2月落成。[YOHO
MALL
形點之上建有住宅](../Page/YOHO_MALL_形點.md "wikilink")[新元朗中心](../Page/新元朗中心.md "wikilink")，此地產項目現時也是[YOHO
MALL 形點項目之一](../Page/YOHO_MALL_形點.md "wikilink")。

## 特色

商場總面積280,000平方呎，樓高4層。地下為入口大堂及[輕鐵](../Page/香港輕鐵.md "wikilink")[元朗總站](../Page/元朗總站.md "wikilink")；二至四樓設有各式商舖及[食肆](../Page/食肆.md "wikilink")，以及一個備有4間影院的百老匯電影院。早期四樓曾設溜冰場，後期改為[家樂福大型超級市場](../Page/家樂福.md "wikilink")，家樂福結業後被[百佳超級廣場取代](../Page/百佳超級廣場.md "wikilink")。在2003年12月20日[西鐵正式啟用前](../Page/西鐵.md "wikilink")，場內人流稀少，商舖大多空置。到了[新時代廣場在](../Page/新時代廣場_\(元朗\).md "wikilink")2004年入伙及西鐵啟用後，人流才顯著上升。

2011年下半年起，為配合[YOHO MALL
形點商場](../Page/YOHO_MALL_形點.md "wikilink")，商場部份範圍正進行翻新工程，工程斥資約3.6億元。翻新將主攻熱門運動零售店。首期翻新工程於2013年11月竣工，包括主中庭進行改建、分層進行翻新、拆除其中一個地下入口大堂及扶手電梯、拆除其中兩部升降機並改為一部大升降機、增建兩條走火樓梯和重置洗手間。翻新後大部份範圍以白色為主調，洗手間外設商場唯一的軟座閒坐區，全新洗手間則採用豪華設計。

整個翻新工程於2016年中完成。

<File:Sun> Yuen Long Centre GF Entrance Lobby
201101.jpg|地下入口大堂，已於2013年拆除 (2007年) <File:Sun>
Yuen Long Centre Mall Liftlobby 2007.jpg|升降機大堂 (2007年) <File:Sun> Yuen
Long Centrecorridor 20070901.jpg|三樓商場走廊 (2007年) <File:Sun> Yuen Long
Centre Mall L3 View 201310.jpg|三樓商場走廊 (2013年)

## 樓層

2013年10月起，商場更改樓層編號，由以前的 L2至L4 改為 L1至L3。

一樓開設Levi's、Mirabell、ans、Bauhaus、[G2000](../Page/G2000.md "wikilink")、egg
optical、珠寶店My
Jewellery、[MaBelle](../Page/MaBelle.md "wikilink")；日本女性服裝Oriental
Traffic及Tonymoly化妝品店。食肆包括[Break
Talk及](../Page/Break_Talk.md "wikilink")[Toast
Box](../Page/Toast_Box.md "wikilink")；二樓開設多間運動零售店，包括adidas、Bigpack、Nike、New
Balance、Gigasports及多間旅行社、[萬寧](../Page/萬寧_\(零售商\).md "wikilink")、fusion
by
PARKnSHOP超級廣場、[太興燒味](../Page/太興燒味.md "wikilink")、[奇華餅家和服裝店bossini及BSX](../Page/奇華餅家.md "wikilink")；三樓開設連鎖電器店、電訊公司、多間潮流配飾店、茶木‧台式休閒餐廳、[鴻星海鮮酒家及](../Page/鴻星海鮮酒家.md "wikilink")[Starbucks](../Page/Starbucks.md "wikilink")。

商場第一期翻新部份於2013年11月正式開業。第二期翻新部份於2014年12月完成，商店於2015年1月中至2月陸續開業。第三期翻新部份於2016年2月完成，商店於同年3月中至4月陸續開業。主要以食肆及電訊公司為主，包括[利小館](../Page/利小館.md "wikilink")、[譚仔三哥](../Page/譚仔三哥.md "wikilink")、快餐店[麥當勞](../Page/麥當勞.md "wikilink")、[滿記甜品](../Page/滿記甜品.md "wikilink")、百樂潮州酒樓及[Pepper
Lunch](../Page/Pepper_Lunch.md "wikilink")。

<File:Yoho> SYLC Level 1 Shops 201404.jpg|翻新後一樓商場走廊 (2014年) <File:Yoho>
SYLC Level 2 Shops 20131101.jpg|翻新後二樓商場走廊 (2013年) <File:YOHO> Mall II L2
Carpark entrance 2014.jpg|翻新後二樓停車場入口 (2014年) <File:Yoho> SYLC Level 2
Seats area 201311.jpg|洗手間外設軟座閒坐區 <File:YOHO> Mall II Level 3
201901.jpg|翻新後三樓商場 (2019年)

## 交通

[SunYuenLongCentreoverview1_20070901.jpg](https://zh.wikipedia.org/wiki/File:SunYuenLongCentreoverview1_20070901.jpg "fig:SunYuenLongCentreoverview1_20070901.jpg")

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[元朗站](../Page/元朗站_\(西鐵綫\).md "wikilink")
  - [輕鐵](../Page/輕鐵.md "wikilink")：[元朗站](../Page/元朗站_\(輕鐵\).md "wikilink")

<!-- end list -->

  - 新元朗中心居民巴士總站

<!-- end list -->

  - [元朗站公共運輸交匯處](../Page/元朗站公共運輸交匯處.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 元朗至上水線\[1\]
  - [爾巒至元朗線](../Page/爾巒.md "wikilink")

<!-- end list -->

  - [元朗（東）巴士總站](../Page/元朗（東）巴士總站.md "wikilink")

<!-- end list -->

  - [朗日路](../Page/朗日路.md "wikilink")

<!-- end list -->

  - [青山公路](../Page/青山公路.md "wikilink") (元朗段)

</div>

</div>

## 參見

  - [YOHO系列](../Page/YOHO系列.md "wikilink")
  - [YOHO MALL 形點](../Page/YOHO_MALL_形點.md "wikilink")
  - [新元朗中心](../Page/新元朗中心.md "wikilink")
  - [元朗市中心](../Page/元朗市中心.md "wikilink")
  - [元朗廣場](../Page/元朗廣場.md "wikilink")
  - [開心廣場](../Page/開心廣場.md "wikilink")
  - [元朗千色匯](../Page/元朗千色匯.md "wikilink")

## 外部連結

  - [新元朗中心](https://web.archive.org/web/20131212114139/http://www.sunyuenlongcentre.com.hk/)
  - [形點](http://www.yohomall.hk/)

[Category:元朗區商場](../Category/元朗區商場.md "wikilink")
[Category:新鴻基地產物業](../Category/新鴻基地產物業.md "wikilink")
[Category:元朗市中心](../Category/元朗市中心.md "wikilink")

1.  [元朗水車館街　—　上水新發街](http://www.16seats.net/chi/rmb/r_n17.html)