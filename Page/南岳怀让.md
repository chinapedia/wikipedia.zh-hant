**南嶽懷讓**禪師（），俗姓[杜](../Page/杜.md "wikilink")，唐代金州安康（今[陕西](../Page/陕西.md "wikilink")[安康](../Page/安康.md "wikilink")）人，[禅宗高僧](../Page/禅宗.md "wikilink")，為[六祖惠能門下](../Page/六祖惠能.md "wikilink")，與[青原行思形成兩大支派](../Page/青原行思.md "wikilink")。[唐敬宗时](../Page/唐敬宗.md "wikilink")，追谥「大慧禅师」。

## 生平

懷讓禪師生於唐[儀鳳二年](../Page/儀鳳.md "wikilink")〈677年〉。唐[武則天](../Page/武則天.md "wikilink")[天授二年](../Page/天授_\(武則天\).md "wikilink")（691年），在[荆州](../Page/荆州.md "wikilink")[玉泉寺](../Page/玉泉寺_\(当阳市\).md "wikilink")[弘景律师座下出家](../Page/玉泉弘景.md "wikilink")。經坦然禪師介紹，怀让禅师到嵩山拜謁慧安大师為師。經慧安大師的指導，懷讓禪師抵[曹溪宝林寺晋谒禅宗六祖](../Page/曹溪.md "wikilink")[惠能大师](../Page/惠能.md "wikilink")，留在六祖身边侍奉十五年之久，後移居南嶽[衡山觀音台](../Page/衡山.md "wikilink")。

[唐玄宗](../Page/唐玄宗.md "wikilink")[天宝三年](../Page/天宝.md "wikilink")（744年）八月[圆寂](../Page/圆寂.md "wikilink")，世壽六十八，僧臘四十八。

## 弟子

怀让弟子有道峻（又作嚴峻）、神照（又作慧照）、道一等人。[藥山惟儼](../Page/藥山惟儼.md "wikilink")，[潮州大顛](../Page/潮州大顛.md "wikilink")，[百丈懷海等大師](../Page/百丈懷海.md "wikilink")，都是從神照出家的，但真正光大懷讓門下的，則是[馬祖道一](../Page/馬祖道一.md "wikilink")，道一禪師建立了[洪州宗](../Page/洪州宗.md "wikilink")。其後代弟子又創[臨濟宗](../Page/臨濟宗.md "wikilink")、[溈仰宗兩大宗派](../Page/溈仰宗.md "wikilink")。

[H](../Category/唐朝僧人.md "wikilink") [H](../Category/安康人.md "wikilink")
[H](../Category/禪僧.md "wikilink")
[Category:杜姓](../Category/杜姓.md "wikilink")