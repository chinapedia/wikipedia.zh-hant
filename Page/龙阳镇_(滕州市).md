**龙阳镇**位于[山东省](../Page/山东省.md "wikilink")[滕州市](../Page/滕州市.md "wikilink")，距城区北5公里，[龙山之阳](../Page/龙山.md "wikilink")，[龙河之滨](../Page/龙河.md "wikilink")，北依[孔孟故里](../Page/孔孟.md "wikilink")，南邻[苏淮平原](../Page/苏淮平原.md "wikilink")，西濒[微山湖畔](../Page/微山湖.md "wikilink")，总面积79[平方千米](../Page/平方千米.md "wikilink")，耕地面积5.4万[亩](../Page/亩.md "wikilink")，辖56个行政[村](../Page/村.md "wikilink")，7.1万人。\[1\]

## 简介

龙阳山水相间，风光优美，资源丰富，交通便利。境内的龙山主峰海拔415米，山体蜿蜒若游龙，绵延18公里，“[龙岭晴云](../Page/龙岭睛云.md "wikilink")”为“[古滕八景](../Page/古滕八景.md "wikilink")”之一。千龙碑林汇聚了[迟浩田](../Page/迟浩田.md "wikilink")、[杨斯德](../Page/杨斯德.md "wikilink")、[李景上将及各界名人](../Page/李景.md "wikilink")[书法瑰宝](../Page/书法.md "wikilink")。

## 行政区划

龙阳镇下辖以下地区：\[2\]

。

## 交通

[104国道](../Page/104国道.md "wikilink")、[京台高速公路](../Page/京福高速公路.md "wikilink")、[津浦铁路和](../Page/津浦铁路.md "wikilink")[京沪高速铁路穿境而过](../Page/京沪高速铁路.md "wikilink")。

## 农业

全年[马铃薯](../Page/马铃薯.md "wikilink")、[绿萝卜](../Page/绿萝卜.md "wikilink")、[葱](../Page/葱.md "wikilink")、[姜](../Page/姜.md "wikilink")、[蒜等特色蔬菜播种面积达到](../Page/蒜.md "wikilink")12.6万亩，被农业部命名为“[中国马铃薯之乡](../Page/中国马铃薯之乡.md "wikilink")”。“龙阳绿萝卜”为国家地理标志登记保护产品。

## 旅游

境内有水域面积1.5万亩的[滕龙湖](../Page/滕龙湖.md "wikilink")，水体清澈，波光潋滟。[龙山由湖西北岸而起](../Page/龙山.md "wikilink")，向西南蜿蜒9公里，被誉为“天然氧吧”。[龙河纵贯全镜](../Page/龙河.md "wikilink")，龙河大桥和橡胶坝可蓄水500米，形成自然的月亮湾风景区。

## 教育

小镇比较重视教育，。

## 小镇特产

## 小镇名人

  - [张知寒](../Page/张知寒.md "wikilink")
    著名[墨学研究专家](../Page/墨学.md "wikilink")，[山东大学教授](../Page/山东大学.md "wikilink")
  - [戴广坤](../Page/戴广坤.md "wikilink")
    [中国人民解放军](../Page/中国人民解放军.md "wikilink")[少将](../Page/少将.md "wikilink")
  - [冯贵州](../Page/冯贵州.md "wikilink") 青年小说家，著有长篇小说《夭折》、《部落风语》

## 参考资料

[Category:枣庄市乡镇](../Category/枣庄市乡镇.md "wikilink")
[Category:滕州市行政区划](../Category/滕州市行政区划.md "wikilink")

1.
2.