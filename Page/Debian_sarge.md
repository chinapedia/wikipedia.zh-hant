**Debian 3.1**又称为Debian
sarge，是Debian的稳定版，发行于2005年6月6日。里面的软件包基本不再变化，只做安全更新。

## 新特性\[1\]

[机器架构](../Page/机器架构.md "wikilink")：

  - [Alpha](../Page/Alpha.md "wikilink")
  - [ARM](../Page/ARM.md "wikilink")
  - [HP PA-RISC](../Page/HP_PA-RISC.md "wikilink")
  - [Intel x86](../Page/Intel_x86.md "wikilink")
  - [Intel IA-64](../Page/Intel_IA-64.md "wikilink")
  - [Motorola 680x0](../Page/Motorola_680x0.md "wikilink")
  - [MIPS](../Page/MIPS.md "wikilink")
  - [MIPS (DEC)](../Page/MIPS_\(DEC\).md "wikilink")
  - [PowerPC](../Page/PowerPC.md "wikilink")
  - [IBM S/390](../Page/IBM_S/390.md "wikilink")
  - [SPARC](../Page/SPARC.md "wikilink")

[安装程序](../Page/安装程序.md "wikilink")：

  - 新的模块化的精致安装程序[debian-installer](../Page/debian-installer.md "wikilink")，包括整合在内的[硬件识别](../Page/硬件识别.md "wikilink")、[无人值守安装](../Page/无人值守安装.md "wikilink")
  - 支持30种语言
  - 包括多种显卡的图形界面配置
  - 改良的灵活任务选择系统，[debconf工具整合进许多软件包以增强易用性与友好性](../Page/debconf.md "wikilink")
  - 可从DVD、CD、USB盘、软盘或网络安装
  - 从低版本升级可使用[aptitude或者](../Page/aptitude.md "wikilink")[apt-get包管理工具](../Page/apt-get.md "wikilink")，无须停机

[软件包](../Page/软件包.md "wikilink")：

  - [KDE](../Page/KDE.md "wikilink") 3.3
  - [GNOME](../Page/GNOME.md "wikilink") 2.8
  - [GNUstep](../Page/GNUstep.md "wikilink")
  - [XFree86](../Page/XFree86.md "wikilink") 4.3.0
  - [GIMP](../Page/GIMP.md "wikilink") 2.2.6
  - [Mozilla](../Page/Mozilla.md "wikilink") 1.7.8
  - [Galeon](../Page/Galeon.md "wikilink") 1.3.20
  - [Mozilla Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink")
    1.0.2
  - [Firefox](../Page/Firefox.md "wikilink") 1.0.4
  - [PostgreSQL](../Page/PostgreSQL.md "wikilink") 7.4.7
  - [MySQL](../Page/MySQL.md "wikilink") 4.0.24和4.1.11a
  - [GCC](../Page/GCC.md "wikilink") 3.3.5
  - Linux[内核版本](../Page/内核.md "wikilink")2.4.27和2.6.8
  - [Apache](../Page/Apache.md "wikilink") 1.3.33和2.0.54
  - [Samba](../Page/Samba.md "wikilink") 3.0.14
  - [Python](../Page/Python.md "wikilink") 2.3.5和2.4.1
  - [Perl](../Page/Perl.md "wikilink") 5.8.4
  - [OpenOffice.org](../Page/OpenOffice.org.md "wikilink") 1.1.3
  - 其它

[加密软件](../Page/加密软件.md "wikilink")：

  - [OpenSSH](../Page/OpenSSH.md "wikilink")
  - [GNU Privacy Guard](../Page/GNU_Privacy_Guard.md "wikilink")
  - [网页浏览器](../Page/网页浏览器.md "wikilink")、[网页服务器](../Page/网页服务器.md "wikilink")、[数据库及其它软件均拥有强加密手段](../Page/数据库.md "wikilink")

[子项目](../Page/子项目.md "wikilink")：

  - Debian-Edu/Skolelinux，用于教育的软件包
  - Debian-Med，用于医学联盟的软件包
  - Debian-Accessibility，适用于残障人士的软件包

## 參考文獻

[Category:Debian](../Category/Debian.md "wikilink")

1.