**时钟花科**（學名：；），也叫[有叶花科](../Page/有叶花科.md "wikilink")、[榆叶葵科](../Page/榆叶葵科.md "wikilink")、[穗柱榆科](../Page/穗柱榆科.md "wikilink")，甚至音译为[特纳草科](../Page/特纳草科.md "wikilink")，是一個[被子植物的](../Page/被子植物.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，共包括10[属约](../Page/属.md "wikilink")205[种](../Page/种.md "wikilink")。

## 型態描述

本科[植物主要为](../Page/植物.md "wikilink")[熱帶及](../Page/熱帶.md "wikilink")[亞熱帶的](../Page/亞熱帶.md "wikilink")，也有少数[草本和](../Page/草本.md "wikilink")[乔木品种](../Page/乔木.md "wikilink")。這些物種近半屬於[時鐘花屬](../Page/時鐘花屬.md "wikilink")。[叶有气味](../Page/叶.md "wikilink")，有毛，无托叶，被用做药材；[花萼和](../Page/花萼.md "wikilink")[花冠结合成筒状](../Page/花冠.md "wikilink")；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，[种子有网状纹](../Page/种子.md "wikilink")。

## 分佈

分布在[非洲南部和](../Page/非洲.md "wikilink")[南美洲的](../Page/南美洲.md "wikilink")[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，大部分种类生长在[美洲](../Page/美洲.md "wikilink")。其中只[时钟花属就有](../Page/时钟花属.md "wikilink")122种，[Piriqueta属有](../Page/Piriqueta.md "wikilink")44种，[中国只有引进种](../Page/中国.md "wikilink")。

## 分類

1981年的[克朗奎斯特分类法将其裂入](../Page/克朗奎斯特分类法.md "wikilink")[堇菜目](../Page/堇菜目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其分到新设立的](../Page/APG_分类法.md "wikilink")[金虎尾目](../Page/金虎尾目.md "wikilink")，2003年经过修订的[APG
II
分类法认为可以选择性地与](../Page/APG_II_分类法.md "wikilink")[西番莲科合并](../Page/西番莲科.md "wikilink")。在2009年的[APG
III分类法](../Page/被子植物APG_III分类法.md "wikilink")，本科正式被併入西番莲科\[1\]。

### 屬

下列本科常見的屬：

  - *[Adenoa](../Page/Adenoa.md "wikilink")*
  - *[Erblichia](../Page/Erblichia.md "wikilink")*
  - *[Hyalocalyx](../Page/Hyalocalyx.md "wikilink")*
  - *[Loewia](../Page/Loewia_\(plant\).md "wikilink")*
  - *[Mathurina](../Page/Mathurina.md "wikilink")*
  - *[Piriqueta](../Page/Piriqueta.md "wikilink")*
  - *[Stapfiella](../Page/Stapfiella.md "wikilink")*
  - *[Streptopetalum](../Page/Streptopetalum.md "wikilink")*
  - *[Tricliceras](../Page/Tricliceras.md "wikilink")* (*Wormskioldia*)
  - [时钟花属](../Page/时钟花属.md "wikilink")
    *[Turnera](../Page/Turnera.md "wikilink")*

## 參考文獻

## 外部連結

  -
  -
  - [APG II
    网站中的时钟花科](http://www.mobot.org/MOBOT/Research/APweb/orders/malpighialesweb.htm#%20Turneraceae)

[时钟花科](../Category/时钟花科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.