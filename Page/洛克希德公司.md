[Lockheed_SR-71_in_Smithonian.JPG](https://zh.wikipedia.org/wiki/File:Lockheed_SR-71_in_Smithonian.JPG "fig:Lockheed_SR-71_in_Smithonian.JPG")是公司的知名產品\]\]
[Profile_of_Agena_Docking_Target_-_GPN-2000-001345.jpg](https://zh.wikipedia.org/wiki/File:Profile_of_Agena_Docking_Target_-_GPN-2000-001345.jpg "fig:Profile_of_Agena_Docking_Target_-_GPN-2000-001345.jpg")
**洛克希德公司**（Lockheed
Corporation）創立於1912年，是美国一家主要航太工業公司，1995年與[马丁·玛丽埃塔共同合并为](../Page/马丁·玛丽埃塔.md "wikilink")[洛克希德·马丁](../Page/洛克希德·马丁.md "wikilink")。

## 历史

### 起始

1912年[阿伦·洛克希德和](../Page/阿伦·洛克希德.md "wikilink")兄弟在[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[圣巴巴拉
(加利福尼亚州)創辦了](../Page/圣巴巴拉_\(加利福尼亚州\).md "wikilink")**Alco[水上飞机公司](../Page/水上飞机.md "wikilink")**，后该公司更名为**洛克希德（Loughhead）飞行器制造公司**。

1926年原洛克希德公司倒闭，阿伦·洛克希德在加利福尼亚州[好莱坞重新开办了](../Page/好莱坞.md "wikilink")**洛克希德飞行器公司（Lockheed
Aircraft Company）**。1929年洛克希德公司卖给了底特律飞行器公司。

大萧条期间，底特律飞行器公司倒闭。罗伯特·格罗斯（Robert Gross）和科特兰·格罗斯（Courtland
Gross）兄弟以四万[美元的价格收购了原洛克希德公司部分](../Page/美元.md "wikilink")。阿伦·洛克希德当时筹得了五万美元，但是认为资金太少而没有参加拍卖。

1934年罗伯特·格罗斯成为新公司的主席并把公司改名为洛克希德飛機集團（Lockheed Aircraft Corporation）。

1930年代，洛克希德投资139,400美元开发了洛歇10伊莱克特拉型（Lochkeed Model 10
Electra）雙發動機運動機，生產第一年就销售了40架。著名的女飞行员[阿梅莉亞·埃爾哈特的最后一次飞行就是驾驶的这种飞机](../Page/阿梅莉亞·埃爾哈特.md "wikilink")。这种飞机还是在第二次世界大战期间[英国皇家空军和美国军队使用的](../Page/英国皇家空军.md "wikilink")[哈德遜型](../Page/哈德遜式轟炸機.md "wikilink")[轰炸机的原型](../Page/轰炸机.md "wikilink")。

### 第二次世界大战

在[第二次世界大戰爆發初期洛克希德成功设计了](../Page/第二次世界大戰.md "wikilink")[P-38闪电型](../Page/P-38.md "wikilink")[战斗机](../Page/战斗机.md "wikilink")，這是一款雙發動機加上雙尾椼機身結構的高速攔截機，在戰場上的用途包括對地攻擊、轟炸機護航以及奪取空中優勢等。最有名的就是擊落[日本海軍](../Page/日本海軍.md "wikilink")[聯合艦隊司令長官](../Page/聯合艦隊.md "wikilink")[山本五十六的任務](../Page/山本五十六.md "wikilink")。

整个第二次世界大战期间洛克希德公司共生产了19,278架飞机，占二战期间美国飞机制造总量的6%。包括2,600架文图拉型（Ventura）雙發動機轟炸機、2,700架[B-17飞行堡垒](../Page/B-17轟炸機.md "wikilink")（[波音授权制造](../Page/波音.md "wikilink")）、2,900架哈德遜型轟炸機和9,000架P-38闪电型戰鬥機。

### 战后生产

第二次世界大战期间，洛克希德和**跨世界航空公司**共同开发了L-049星座型（Constellation）客机。这种飞机可运载43名乘客以每小时300[英里的速度用](../Page/英里.md "wikilink")13个小时从[纽约飞到](../Page/纽约.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")。但是在战争期间所有的产品都被军方购买，战争结束后民航才得到其订货。

### 臭鼬工廠

[Skunk-works.jpg](https://zh.wikipedia.org/wiki/File:Skunk-works.jpg "fig:Skunk-works.jpg")
1943年，洛克希德設計美國空軍第一種服役的噴射戰鬥機[F-80流星](../Page/F-80流星戰鬥機.md "wikilink")。这种飞机由其被戏称为「[臭鼬工廠](../Page/臭鼬工廠.md "wikilink")」的高级开发部开发。后来臭鼬工廠还設計了[U-2偵察機](../Page/U-2偵察機.md "wikilink")、[SR-71黑鳥式偵察機](../Page/SR-71黑鳥式偵察機.md "wikilink")、[F-117夜鷹戰鬥機](../Page/F-117夜鷹戰鬥機.md "wikilink")、[F-35閃電II戰鬥機和](../Page/F-35閃電II戰鬥機.md "wikilink")[F-22猛禽戰鬥機等](../Page/F-22猛禽戰鬥機.md "wikilink")。臭鼬工廠通常能够在有限的时间和有限的资源下，突破技術上的限制，推出極為令人驚訝的飛機。

### 冷战期间

1953年，洛克希德开发了[C-130大力神运输机](../Page/C-130運輸機.md "wikilink")，这种飞机至今仍在大规模使用。

1956年，洛克希德开发了[北极星](../Page/北极星.md "wikilink")[潜射弹道导弹](../Page/潜射弹道导弹.md "wikilink")，后来又开发了[海神和](../Page/海神_\(导弹\).md "wikilink")[三叉戟导弹](../Page/三叉戟_\(导弹\).md "wikilink")。

1976年，臭鼬工作室开始展開第一代[低可偵測性飛機的研究計畫](../Page/低可偵測性.md "wikilink")：“擁藍”（Have
Blue），隨後以擁藍測試資料為基礎製造出史上第一架[隐形軍用機](../Page/低可偵測性.md "wikilink")－[F-117夜鷹戰鬥攻擊機](../Page/F-117夜鷹戰鬥攻擊機.md "wikilink")。

其他洛克希德产品还包括兩倍[音速戰鬥機](../Page/音速.md "wikilink")[F-104](../Page/F-104.md "wikilink")、[L-1011三星宽体噴射客機](../Page/L-1011.md "wikilink")，美國空軍現役最大的[C-5银河噴射運輸機](../Page/C-5.md "wikilink")。

1980年代，洛克希德參與了[先進戰術戰鬥機](../Page/先進戰術戰鬥機.md "wikilink")（Advanced Tactical
Fighter，簡稱ATF）計劃競標，創造出號稱20世紀以來最優秀之空優戰機[F-22](../Page/F-22.md "wikilink")。

### 时间线

  - 1912年，Alco水上飞机公司成立。
  - 1916年，公司更名为洛克希德（Loughead）飞行器制造公司。
  - 1926年，洛克希德公司（Lockheed Aircraft Company）成立。
  - 1929年，洛克希德成为底特律飞行器的子公司。
  - 1932年，格罗斯兄弟在底特律飞行器破产后收购了洛克希德公司。
  - 1932年，更名为洛克希德公司洛克希德飛機集團（Lockheed Aircraft Corporation），大范围重组。
  - 1943年，臭鼬工廠成立。
  - 1954年，[C-130大力神首飞](../Page/C-130.md "wikilink")。[U-2首飞](../Page/U-2.md "wikilink")。
  - 1976年，因為賄賂[日本首相](../Page/日本首相.md "wikilink")[田中角榮採購](../Page/田中角榮.md "wikilink")[P-3](../Page/P-3.md "wikilink")[反潛機引發](../Page/反潛機.md "wikilink")[政治醜聞](../Page/洛克希德事件.md "wikilink")。
  - 1986年，收购桑得斯联盟的电子部门。
  - 1991年，與[通用动力和](../Page/通用动力.md "wikilink")[波音合作設計](../Page/波音.md "wikilink")[YF-22](../Page/F-22.md "wikilink")，參與先進戰術戰鬥機（ATF）的競標計畫。
  - 1993年，收购[通用动力的](../Page/通用动力.md "wikilink")[F-16生产部门](../Page/F-16戰隼式戰鬥機.md "wikilink")。
  - 1995年，與[馬丁·馬瑞塔合并为](../Page/馬丁·馬瑞塔.md "wikilink")[洛克希德·马丁](../Page/洛克希德·马丁.md "wikilink")。

## 产品

### 民用产品

[Lockheed_P-38_Lightnings_on_CVE.jpg](https://zh.wikipedia.org/wiki/File:Lockheed_P-38_Lightnings_on_CVE.jpg "fig:Lockheed_P-38_Lightnings_on_CVE.jpg")生產線\]\]

  - [织女星](../Page/洛克希德织女星.md "wikilink")（Vega）
  - [10型伊莱克特拉](../Page/洛克希德10型伊莱克特拉.md "wikilink")（Model 10 Electra）
  - [12型小伊莱克特拉](../Page/洛克希德12型小伊莱克特拉.md "wikilink")（Model 12 Electra
    Junior）
  - [14型超级伊莱克特拉](../Page/洛克希德14型超级伊莱克特拉.md "wikilink")（Model 14 Super
    Electra）
  - [18型北极星](../Page/洛克希德18型北极星.md "wikilink")（Model 18 Lodestar）
  - [星座](../Page/洛克希德星座.md "wikilink")
  - [L-1049超級星座](../Page/洛歇L-1049超級星座式.md "wikilink")
  - [土星](../Page/洛克希德土星.md "wikilink")（Saturn）
  - [喷气星](../Page/洛克希德喷气星.md "wikilink")（JetStar）商務噴射機
  - [L-1011三星](../Page/洛克希德L-1011.md "wikilink")

### 军用运输机

  - C-69/121星座军用运输机
  - 洛克希德R6V憲章大型运输机
  - [C-130大力神中型运输机系列](../Page/C-130運輸機.md "wikilink")
  - [C-141运输星远程喷气运输机](https://zh.m.wikipedia.org/zh-tw/洛克希德C-141%22星%22式运输机)
  - [C-5银河重型运输机](../Page/C-5運輸機.md "wikilink")

### 战斗机

  - [P-38闪电](../Page/P-38閃電式戰鬥機.md "wikilink")
  - [P-80流星](../Page/F-80戰鬥機.md "wikilink")
  - [F-94星火](../Page/F-94戰鬥機.md "wikilink")（F-94 Starfire）
  - [F-104星战士](../Page/F-104戰鬥機.md "wikilink")
  - [F-117夜鹰](../Page/F-117戰鬥機.md "wikilink")
  - [F-16戰隼](../Page/F-16戰鬥機.md "wikilink")
  - [F-22猛禽](../Page/F-22戰鬥機.md "wikilink")
  - [F-35闪电II](../Page/F-35戰鬥機.md "wikilink")

### 巡逻与侦察机

[Hercules.c-130h.royaljordaf.346.arp.jpg](https://zh.wikipedia.org/wiki/File:Hercules.c-130h.royaljordaf.346.arp.jpg "fig:Hercules.c-130h.royaljordaf.346.arp.jpg")
[Lockheed_Hudson_ExCC.jpg](https://zh.wikipedia.org/wiki/File:Lockheed_Hudson_ExCC.jpg "fig:Lockheed_Hudson_ExCC.jpg")

  - [哈德遜式轟炸機](../Page/哈德遜式轟炸機.md "wikilink")
  - PV-1文图拉（Ventura）、PV-2捕鲸叉（Harpoon）
  - P-2海王星（Neptune）
  - [P-3獵戶座海上巡邏機](../Page/P-3獵戶座海上巡邏機.md "wikilink")
  - [U-2偵察機](../Page/U-2偵察機.md "wikilink")
  - [SR-71黑鸟](../Page/SR-71.md "wikilink")
  - [S-3海盗](../Page/S-3反潛機.md "wikilink")

### 直升机

  - [XH-51A](../Page/X-51乘波者試驗機.md "wikilink")
  - [AH-56A](https://en.m.wikipedia.org/wiki/Lockheed_AH-56_Cheyenne)

### 导弹

  - 北极星
  - 海神
  - 三叉戟

### 太空飛行器

  - X-7
  - X-17
  - 光环
  - [哈勃太空望远镜](../Page/哈勃太空望远镜.md "wikilink")

### 船只

  - [海影](../Page/海影.md "wikilink")

## 外部链接

  - [Lockheed Martin Official Web Site](http://www.lockheedmartin.com/)

[\*](../Category/洛克希德.md "wikilink")
[Category:美国飞机公司](../Category/美国飞机公司.md "wikilink")
[Category:1912年成立的公司](../Category/1912年成立的公司.md "wikilink")