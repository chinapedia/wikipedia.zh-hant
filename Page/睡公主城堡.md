[Sleeping_Beauty_Castle_DLR.jpg](https://zh.wikipedia.org/wiki/File:Sleeping_Beauty_Castle_DLR.jpg "fig:Sleeping_Beauty_Castle_DLR.jpg")
[Sleeping_Beauty_Castle_at_Hong_Kong_Disneyland_200705.jpg](https://zh.wikipedia.org/wiki/File:Sleeping_Beauty_Castle_at_Hong_Kong_Disneyland_200705.jpg "fig:Sleeping_Beauty_Castle_at_Hong_Kong_Disneyland_200705.jpg")的睡美人城堡\]\]
[SchlossNeuschwanstein2011.JPG](https://zh.wikipedia.org/wiki/File:SchlossNeuschwanstein2011.JPG "fig:SchlossNeuschwanstein2011.JPG")，可以看得出前三者風的格極度相近\]\]

**睡美人城堡（Sleeping Beauty
Castle）**乃置於[美國](../Page/美國.md "wikilink")[加州迪士尼樂園](../Page/加州迪士尼樂園.md "wikilink")、[香港迪士尼樂園的小巧童話式城堡](../Page/香港迪士尼樂園.md "wikilink")，另外[巴黎迪士尼樂園也有一座睡美人城堡](../Page/巴黎迪士尼樂園.md "wikilink")、但和前兩者有著本質的不同。其中加州迪士尼的睡美人城堡是歷史最悠久，也是香港和巴黎迪士尼樂園的睡美人城堡、奧蘭多和東京的[灰姑娘城堡](../Page/灰姑娘城堡.md "wikilink")、上海的奇幻童話城堡的原型。

城堡取材自十九世紀末於[德國](../Page/德國.md "wikilink")[巴伐利亞落成的](../Page/巴伐利亞.md "wikilink")[新天鵝堡](../Page/新天鵝堡.md "wikilink")，以德國風格為絕對的主體。原本是為了德國童話故事——[白雪公主而設計的城堡](../Page/白雪公主.md "wikilink")，但是因為在[1955年配合當時的迪士尼電影](../Page/1955年.md "wikilink")《[睡美人](../Page/睡美人.md "wikilink")》的上映，而改變了名稱。

法國巴黎也有一座高大的、粉紅色的睡美人城堡，但拼寫成法文的方式**Le Château de la Belle au Bois
Dormant**，並加入一些[法國色彩](../Page/法國.md "wikilink")（參考了[巴黎聖母院](../Page/巴黎聖母院.md "wikilink")、[的波那收容所的建築細節](../Page/的波那收容所.md "wikilink")），並且配合1992年的《[美女與野獸](../Page/美女與野獸.md "wikilink")》的上映，也叫作**Belle城堡**，一是因為法文中的Belle既是睡美人中的“美人（Beautiful）”的法文拼寫方式、二是因為美女與野獸中的貝兒（Belle）的名字就是這麼拼的，而且無論睡美人還是美女與野獸都是源自於法國的童話故事。

## 相關

  - [睡美人](../Page/睡美人.md "wikilink")
  - [護城河](../Page/護城河.md "wikilink")
  - [吊橋](../Page/吊橋.md "wikilink")
  - [灰姑娘城堡](../Page/灰姑娘城堡.md "wikilink")
  - [于塞城堡](../Page/于塞城堡.md "wikilink")

## 外部参考

  - [迪士尼樂園:
    睡美人城堡](http://news.sina.com.cn/c/p/2005-09-12/14347745944.shtml)

[category:美国城堡](../Page/category:美国城堡.md "wikilink")

[Category:香港迪士尼樂園遊樂設施](../Category/香港迪士尼樂園遊樂設施.md "wikilink")
[Category:迪士尼乐园](../Category/迪士尼乐园.md "wikilink")
[Category:巴黎迪士尼樂園](../Category/巴黎迪士尼樂園.md "wikilink")
[Category:睡美人](../Category/睡美人.md "wikilink")
[Category:1955年加利福尼亞州建立](../Category/1955年加利福尼亞州建立.md "wikilink")
[Category:2005年香港建立](../Category/2005年香港建立.md "wikilink")