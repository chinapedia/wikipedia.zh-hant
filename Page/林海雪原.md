《**林海雪原**》是一部描写[第二次國共內戰時期解放軍在](../Page/第二次國共內戰.md "wikilink")[中国东北地区](../Page/中国东北地区.md "wikilink")「[剿匪](../Page/东北剿匪.md "wikilink")」的[小说](../Page/小说.md "wikilink")。作者是[曲波](../Page/曲波_\(作家\).md "wikilink")。原名为《林海雪原荡匪记》，曲波于1954年开始创作，1955年完成。投稿至[人民文学出版社](../Page/人民文学出版社.md "wikilink")，后经编辑[龙世辉修改](../Page/龙世辉.md "wikilink")，改名为《林海雪原》，于1957年正式出版\[1\]。曾被翻譯成[英文](../Page/英文.md "wikilink")、[日文等多種文字](../Page/日文.md "wikilink")。本书中《[智取威虎山](../Page/智取威虎山.md "wikilink")》的情节十分著名，曾被改编成[京剧](../Page/京剧.md "wikilink")，成為八部[樣板戲之一](../Page/樣板戲.md "wikilink")。

林海雪原是根據曲波在1946年冬天親率解放軍小分隊深入[牡丹江地區的深山老林](../Page/牡丹江.md "wikilink")，以半年時間消滅親[国民政府地方武裝的真實經歷作為背景而著成的](../Page/国民政府.md "wikilink")，可謂其代表著作。

## 主要人物

### 正面角色

  - [少剑波](../Page/少剑波.md "wikilink")-剿匪小分队队长
  - [白茹](../Page/白茹.md "wikilink")-剿匪小分队医疗员
  - [杨子荣](../Page/杨子荣.md "wikilink")-偵查隊排長
  - [高波](../Page/高波.md "wikilink")-警卫员
  - [李勇奇](../Page/李勇奇.md "wikilink")-民兵队长

### 反面角色

  - [座山雕](../Page/座山雕.md "wikilink")
  - [许大马棒](../Page/许大马棒.md "wikilink")
  - [蝴蝶迷](../Page/蝴蝶迷.md "wikilink")
  - [馬希方](../Page/馬希方.md "wikilink")
  - [侯殿坤](../Page/侯殿坤.md "wikilink")
  - [谢文东](../Page/谢文东.md "wikilink")
  - [郑三炮](../Page/郑三炮.md "wikilink")
  - [栾平](../Page/栾平.md "wikilink")

## 经典场景

  - 夜袭奶头山
  - [智取威虎山](../Page/智取威虎山.md "wikilink")
  - 剿匪决战

## 注释

[Category:中共剿匪题材作品](../Category/中共剿匪题材作品.md "wikilink")
[Category:第二次国共内战题材小说](../Category/第二次国共内战题材小说.md "wikilink")
[Category:1957年中国小说](../Category/1957年中国小说.md "wikilink")
[Category:1957年長篇小說](../Category/1957年長篇小說.md "wikilink")
[Category:中文長篇小說](../Category/中文長篇小說.md "wikilink")
[Category:改编成电影的中华人民共和国小说](../Category/改编成电影的中华人民共和国小说.md "wikilink")
[Category:電視劇原著小說](../Category/電視劇原著小說.md "wikilink")

1.