**中天電視海外頻道 - 亞洲 /
北美**是由[中天電視擁有的一條在](../Page/中天電視.md "wikilink")[台灣以外地區所播放的](../Page/台灣.md "wikilink")[電視頻道](../Page/電視頻道.md "wikilink")，面向[亞太和](../Page/亞太.md "wikilink")[美洲地區](../Page/美洲.md "wikilink")，其电视呼号名称分别为**中天亞洲台**、**中天北美台**，兩個频道所播放的節目內容並不相同。

## 频道列表及覆盖情况

### 亞太

[Ctitv_Aisa_Channel_Logo_by_aTV.jpg](https://zh.wikipedia.org/wiki/File:Ctitv_Aisa_Channel_Logo_by_aTV.jpg "fig:Ctitv_Aisa_Channel_Logo_by_aTV.jpg")轉播中天亞洲台節目訊號\]\]
**[中天亞洲台](../Page/中天亞洲台.md "wikilink")**（或稱「中天海外頻道 -
亞洲」）透過PAS-8衛星向[亞太](../Page/亞太.md "wikilink")、[歐陸](../Page/歐陸.md "wikilink")、和[印度洋沿岸等地區開路播出](../Page/印度洋.md "wikilink")，其收視範圍包括：[日本](../Page/日本.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[歐洲](../Page/歐洲.md "wikilink")、[南非](../Page/南非.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[關島](../Page/關島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[夏威夷](../Page/夏威夷.md "wikilink")、以及[香港](../Page/香港.md "wikilink")/[澳門等等](../Page/澳門.md "wikilink")。

#### 香港

2009年1月，[旺旺集團董事長](../Page/旺旺集團.md "wikilink")[蔡衍明入股香港](../Page/蔡衍明.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")。同年4月1日開始轉播中天亞洲台，此為首次有整條台灣電視頻道於香港免費電視播映；使得香港觀眾除了可透過[衛星接收傳送收看](../Page/衛星電視.md "wikilink")（但在當地並無透過[有線電視或](../Page/有線電視.md "wikilink")[IPTV等平台落地](../Page/IPTV.md "wikilink")）外，也能透過[香港數位地面電視廣播第](../Page/香港數位地面電視廣播.md "wikilink")14頻道來收看中天亞洲台，因为中天亞洲台以[4:3播出](../Page/4:3.md "wikilink")，故亞洲電視轉播的版本中，左右兩邊以[黑邊填補](../Page/黑邊.md "wikilink")，同時左上角加入亞洲電視台标（台标的颜色和中天台台标的[淺藍色接近](../Page/淺藍色.md "wikilink")）。

2011年1月1日凌晨零時起，亞視所有自製及轉播頻道的台标颜色統一為橙色。

由於2010年[查懋聲與蔡衍明爆出](../Page/查懋聲.md "wikilink")[亞洲電視股權風波](../Page/亞洲電視股權風波_\(2010年\).md "wikilink")，最終中天與亞視雙方都無意繼續合作，中天終止與亞視的合作、並改與[now寬頻電視合作](../Page/now寬頻電視.md "wikilink")。亞視於2011年4月1日停止轉播中天亞洲台\[1\]。now寬頻電視于同日開始轉播中天亞洲台，用戶可免費收看。

### 美洲

**中天北美台**（或稱「中天海外頻道 -
北美」）通過[精宇直播向](../Page/精宇直播.md "wikilink")[北美洲和](../Page/北美洲.md "wikilink")[南美洲收費播出](../Page/南美洲.md "wikilink")，其收視範圍包括[美國](../Page/美國.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")、以及[拉美等地](../Page/拉美.md "wikilink")。

## 海外媒體傳播據點

中天電視是最早實現全球廣播的台灣電視媒體。中天頻道曾是在[歐洲](../Page/歐洲.md "wikilink")、[南非](../Page/南非.md "wikilink")、[中南美洲等地唯一可以接收的台灣電視頻道](../Page/中南美洲.md "wikilink")。

中天頻道於北美的相關服務——「[中旺電視](../Page/中旺電視.md "wikilink")57.9」，已於2010年2月13日（星期六）正式開始服務。無線收視的頻道為57.9台。

## 節目

註明：以首播（該台）的節目為主

**亞洲台**

  - 魅力東方（与[东方卫视合作](../Page/东方卫视.md "wikilink")）
  - 雙城記（与[东方卫视合作](../Page/东方卫视.md "wikilink")）
  - 兩岸祕密檔案（与[厦门卫视合作](../Page/厦门卫视.md "wikilink")）

**北美台**

  - 生活的藝術
  - 雙城記
  - 美洲地方新聞
  - 生活情報

其他未列出的節目，就是重播[中天綜合台](../Page/中天綜合台.md "wikilink")、[中天娛樂台](../Page/中天娛樂台.md "wikilink")、[中天新聞台已播畢或當日播出的節目](../Page/中天新聞台.md "wikilink")，或者聯播[中天新聞台節目](../Page/中天新聞台.md "wikilink")，以及購自其他電視台的節目，當中包括：
亞洲台

  - [新聞龍捲風](../Page/新聞龍捲風.md "wikilink")^
  - [夜問打權](../Page/夜問打權.md "wikilink")^
  - [新聞深喉嚨](../Page/新聞深喉嚨.md "wikilink")^
  - [樂活有方](../Page/樂活有方.md "wikilink")
  - [聚焦新亞洲](../Page/聚焦新亞洲.md "wikilink")^
  - [小明星大跟班](../Page/小明星大跟班.md "wikilink")^
  - [中天新聞](../Page/中天新聞.md "wikilink") ^
  - [大政治大爆卦](../Page/大政治大爆卦.md "wikilink")^
  - [麻辣天后傳](../Page/麻辣天后傳.md "wikilink")^
  - [學習逆轉勝](../Page/學習逆轉勝.md "wikilink")
  - [好食記](../Page/好食記.md "wikilink")
  - [文茜世界周報](../Page/文茜世界周報.md "wikilink")^
  - [中天夢想驛站](../Page/中天夢想驛站.md "wikilink")^
  - [開放新中國](../Page/開放新中國.md "wikilink")
  - [中國正在說](../Page/中國正在說.md "wikilink")
  - [MIT 台灣誌](../Page/MIT_台灣誌.md "wikilink")
  - [前進東協](../Page/前進東協.md "wikilink")
  - [大國武器大觀](../Page/大國武器大觀.md "wikilink")^
  - [文茜的世界財經周報](../Page/文茜的世界財經周報.md "wikilink")^
  - [中天調查報告](../Page/中天調查報告.md "wikilink")

^美洲台

  - [中天全球新聞](../Page/中天全球新聞.md "wikilink")
  - [真的？假的](../Page/真的？假的.md "wikilink")
  - [金牌大健諜](../Page/金牌大健諜.md "wikilink")
  - [文茜的世界週報](../Page/文茜的世界周報.md "wikilink")
  - [文茜的世界財經週報](../Page/文茜的世界財經周報.md "wikilink")
  - [新聞深喉嚨](../Page/新聞深喉嚨.md "wikilink")
  - [夜問打權](../Page/夜問打權.md "wikilink")
  - [健康總動員](../Page/健康總動員.md "wikilink")
  - [飢餓遊戲](../Page/飢餓遊戲_\(電視節目\).md "wikilink")
  - [冰冰好料理](../Page/冰冰好料理.md "wikilink")
  - [大陸尋奇](../Page/大陸尋奇.md "wikilink")
  - [健康加油站](../Page/健康加油站.md "wikilink")
  - [人體實驗室](../Page/人體實驗室.md "wikilink")
  - [型男大主廚](../Page/型男大主廚.md "wikilink")
  - [改變的起點](../Page/改變的起點.md "wikilink")
  - [驚奇四超人](../Page/驚奇四超人.md "wikilink")
  - [天天樂財神](../Page/天天樂財神.md "wikilink")
  - [小明星大跟班](../Page/小明星大跟班.md "wikilink")
  - [中天午間新聞](../Page/中天午間新聞.md "wikilink")
  - [中天晨報](../Page/中天晨報.md "wikilink")
  - [電視晨報](../Page/電視晨報.md "wikilink")
  - [電視早餐](../Page/電視早餐.md "wikilink")
  - [中天夜線新聞](../Page/中天夜線新聞.md "wikilink")

^亞洲台和美洲台同步首播，節目播出時間因應地區和時間不同。

## 參考資料

## 外部連結

  - [中天電視](http://www.ctitv.com.tw/)

  - [中天北美台電視節目表](https://web.archive.org/web/20100612081144/http://www.ctitv.com.tw/schedule_A1.html)

  - [中天亞洲台電視節目表](https://web.archive.org/web/20100819173412/http://www.ctitv.com.tw/schedule_A2.html)

  -
  -
[國際台](../Category/中天電視.md "wikilink")
[Category:新加坡电视播放频道](../Category/新加坡电视播放频道.md "wikilink")
[Category:日本衛星電視頻道](../Category/日本衛星電視頻道.md "wikilink")
[Category:2009年成立的電視台或電視頻道](../Category/2009年成立的電視台或電視頻道.md "wikilink")

1.  [擷取畫面](http://ww3.sinaimg.cn/large/684ba82bjw1dfsaimb73hj.jpg)