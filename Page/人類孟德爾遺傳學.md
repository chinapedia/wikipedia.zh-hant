**人類孟德爾遺傳學**（，[缩写MIM](../Page/缩写.md "wikilink")）是一個[數據庫將現時所知的](../Page/數據庫.md "wikilink")[遺傳病分類](../Page/遺傳病.md "wikilink")，並且連接相關的[人類基因組中的](../Page/人類基因組.md "wikilink")[基因](../Page/基因.md "wikilink")。這個數據庫出版了名為《孟德爾遺傳定律說明》，最新的版本是第12版。它亦有網上版本，稱為**在線人類孟德爾遺傳**（英文：Online
Mendelian Inheritance in Man，簡稱OMIM）。

OMIM™及Online Mendelian Inheritance in
Man™是[約翰·霍普金斯大學的商標](../Page/約翰·霍普金斯大學.md "wikilink")。

## 收集過程

這個[數據庫的資料是由](../Page/數據庫.md "wikilink")[維克托·麥庫西克醫生的帶領下](../Page/維克托·麥庫西克.md "wikilink")，在[約翰·霍普金斯大學所收集及處理](../Page/約翰·霍普金斯大學.md "wikilink")，並由一組科學作者及編輯協助。相關的文章經過鑑定、討論及編寫在數據庫內成為相關的條目。

## MIM編號

每一種疾病及[基因會有一個獨特的六個位編號](../Page/基因.md "wikilink")，編號的頭一個號碼是遺傳方式的分類：如頭一個號碼是1，即是指[染色體顯性遺傳](../Page/染色體.md "wikilink")；如果是2，就是指染色體隱性遺傳；若是3，則是與X連鎖。無論如何編號，在第12版的MIM中，所有編號皆會加上方括號，某些當中會有\*標（\*標代表已知的遺傳模式；若在編號前加上一個符號（\#），則代表病徵是因2個或以上的基因[突變而成](../Page/突變.md "wikilink")）。舉例來說，[佩利措伊斯—梅茨巴赫病](../Page/佩利措伊斯—梅茨巴赫病.md "wikilink")（MIM\*169500）就是指已知的、染色體的、顯性的、孟德爾疾病。

|         |               |                                         |
| ------- | ------------- | --------------------------------------- |
| **首號碼** | **MIM編號範圍**   | **遺傳方式**                                |
| 1       | 100000-199999 | 染色體顯性位點或外顯特質（於1994年5月15日創建）             |
| 2       | 200000-299999 | 染色體隱性位點或外顯特質（於1994年5月15日創建）             |
| 3       | 300000-399999 | X連鎖位點或外顯特質                              |
| 4       | 400000-499999 | Y連鎖位點或外顯特質                              |
| 5       | 500000-599999 | [線粒體位點或外顯特質](../Page/線粒體.md "wikilink") |
| 6       | 600000-       | 染色體位點或外顯特質（於1994年5月15日創建）               |

## 參考文献

  - [OMIM常見問題](http://www.ncbi.nlm.nih.gov/Omim/omimfaq.html)
  - McKusick VA. Mendelian Inheritance in Man; A Catalog of Human Genes
    and Genetic Disorders. Baltimore, MD: The Johns Hopkins University
    Press, 1998. ISBN 0-8018-5742-2.

## 外部連結

  - [在線人類孟德爾遺傳](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=OMIM)

## 参见

  - [醫學分類](../Page/醫學分類.md "wikilink")
  - [疾病資料庫](../Page/疾病資料庫.md "wikilink")

[Category:生物資訊資料庫](../Category/生物資訊資料庫.md "wikilink")
[Category:診斷分類](../Category/診斷分類.md "wikilink")