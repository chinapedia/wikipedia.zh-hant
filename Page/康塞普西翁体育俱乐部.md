**康塞普西翁体育俱乐部**（**Club de Deportes
Concepción**）是[智利足球俱乐部](../Page/智利.md "wikilink")，位于[康塞普西翁市](../Page/康塞普西翁.md "wikilink")。

俱乐部创建于1966年2月29日，是由当地的加尔瓦里诺(Galvarino),利物浦(Liverpool),尤文尔联(Juvenil
Unido),圣塔菲(Santa Fe)和科克伦勋爵俱乐部（Club Lord
Cochrane）多个业余足球俱乐部所组成的。俱乐部是智利第8区最主要的球队之一，球队的最大竞争对手是[费尔南德斯·维亚尔](../Page/阿尔图罗·费尔南德斯·维亚尔海军上将铁路体育俱乐部.md "wikilink")。

球队在1966年加入了[智利足球乙级联赛并且在一年之后晋级为](../Page/智利足球乙级联赛.md "wikilink")[智利足球甲级联赛球队](../Page/智利足球甲级联赛.md "wikilink")。1991年球队代表智利出战[南美解放者杯球队在](../Page/南美解放者杯.md "wikilink")16强中败给了[卡利美洲](../Page/卡利美洲.md "wikilink")。1993年俱乐部取得16支球队中的第15位从而降级，但球队第二年就取得乙级联赛冠军重回甲级行列。

康塞普西翁体育在1999年再次出现在国际舞台上，球队参加[南美足协杯但是在半决赛中被](../Page/南美足协杯.md "wikilink")[阿根廷球队](../Page/阿根廷.md "wikilink")[科尔多巴塔勒雷斯所击败](../Page/科尔多巴塔勒雷斯竞技俱乐部.md "wikilink")，球队对此非常失望。

2001年又一届的南美解放者杯中，球队相继击败了[阿根廷的](../Page/阿根廷.md "wikilink")[圣洛伦佐和](../Page/阿尔马格罗圣洛伦佐竞技俱乐部.md "wikilink")[玻利维亚的](../Page/玻利维亚.md "wikilink")[约格·威斯特曼](../Page/约格·威斯特曼俱乐部.md "wikilink")，但在16强中被[罗马里奥领衔的](../Page/罗马里奥.md "wikilink")[瓦斯科·达伽马所淘汰](../Page/瓦斯科·达伽马体育俱乐部.md "wikilink")。

2002年球队联赛排16支球队倒数第二再次降入乙级联赛，直到2004年再次升回甲级行列。

2006年球队由于债务和拖欠球员工资可能不法参加整年度的职业联赛。俱乐部将被勒令降级，但在球队最后被重新接纳为甲级联赛球队，允许参加2007年赛事，与其相关联的球队康塞普西翁体育二队则最终参加丙级联赛。

## 知名球员

  - [Osvaldo "Pata Bendita"
    Castro](../Page/Osvaldo_Castro.md "wikilink")

  - [文森特·康塔多莱](../Page/文森特·康塔多莱.md "wikilink")

  - [Fernando "Palito"
    Cavaleri](../Page/Fernando_Cavaleri.md "wikilink")

  - [Carlos Fernando Navarro
    Montoya](../Page/Carlos_Fernando_Navarro_Montoya.md "wikilink")

  - [Daniel Morón](../Page/Daniel_Morón.md "wikilink")

  - [Mario Osbén](../Page/Mario_Osbén.md "wikilink")

  -  [Mario Vener](../Page/Mario_Vener.md "wikilink")

  - [Nicolas "El Loco"
    Villamil](../Page/Servando_Nicolas_Villamil.md "wikilink")

  - [克里斯蒂安·蒙特奇诺斯](../Page/克里斯蒂安·蒙特奇诺斯.md "wikilink")

## 外部链接

  - [Sitio web oficial](http://www.cdconcepcion.cl/)

[Category:智利足球俱樂部](../Category/智利足球俱樂部.md "wikilink")