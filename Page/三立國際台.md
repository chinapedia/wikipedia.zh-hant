**三立國際台**（SET
International），簡稱**[SETI](../Page/SETI.md "wikilink")**，是[三立電視旗下的海外](../Page/三立電視.md "wikilink")[電視頻道](../Page/電視頻道.md "wikilink")，2000年3月開播。

三立國際台節目内容包括[三立台灣台的](../Page/三立台灣台.md "wikilink")[八點檔台語劇](../Page/八點檔.md "wikilink")、[綜藝節目](../Page/綜藝節目.md "wikilink")，以及[三立都會台的部分](../Page/三立都會台.md "wikilink")[綜藝節目](../Page/綜藝節目.md "wikilink")。[三立華劇以及](../Page/三立華劇.md "wikilink")《[愛台客](../Page/愛台客.md "wikilink")》系列等節目，因三立電視已經向其他單位（諸如東森美洲台、[天下衛視等](../Page/天下衛視.md "wikilink")）出售海外播映權，故未能在三立國際台播放。

## 現今節目列表

戲劇節目

| 日期            | 播出時间                                     | 现时节目                                 | 备注 |
| ------------- | ---------------------------------------- | ------------------------------------ | -- |
| 星期一至五         | 18:00 - 18:30                            | [新戲說台灣](../Page/戲說台灣.md "wikilink")  |    |
| 19:00 - 21:00 | [天下父母心](../Page/天下父母心.md "wikilink")     |                                      |    |
| 星期六           | 22:00 - 24:00                            | [真愛黑白配](../Page/真愛黑白配.md "wikilink") |    |
| 星期日           | [22K夢想高飛](../Page/22K夢想高飛.md "wikilink") |                                      |    |
|               |                                          |                                      |    |

旅遊節目

| 日期    | 播出時间          | 现时节目                             | 备注 |
| ----- | ------------- | -------------------------------- | -- |
| 星期一至四 | 22:00 - 23:00 | [愛玩客](../Page/愛玩客.md "wikilink") |    |
|       |               |                                  |    |

綜藝節目

| 日期            | 播出時间                                 | 现时节目                                 | 备注 |
| ------------- | ------------------------------------ | ------------------------------------ | -- |
| 星期一至五         | 23:00-00:00                          | [國光幫幫忙](../Page/國光幫幫忙.md "wikilink") |    |
| 21:00 - 22:00 | [綜藝大熱門](../Page/綜藝大熱門.md "wikilink") |                                      |    |
| 星期五           | 22:00 -23:00                         | [超愛美小姐](../Page/超愛美小姐.md "wikilink") |    |
| 星期六、日         | 18:00 - 19:00                        | [新完全娛樂](../Page/完全娛樂.md "wikilink")  |    |
| 20:00-22:00   | [綜藝玩很大](../Page/綜藝玩很大.md "wikilink") |                                      |    |
|               |                                      |                                      |    |

資訊節目

| 日期    | 播出時间                               | 现时节目                                 | 备注 |
| ----- | ---------------------------------- | ------------------------------------ | -- |
| 星期一至二 | 17:00=18:00                        | [婆媳當家](../Page/婆媳當家.md "wikilink")   |    |
| 星期三至五 | [姐姐当家](../Page/姐姐当家.md "wikilink") |                                      |    |
| 星期六   | 17:00 - 18:00                      | [寶島神很大](../Page/寶島神很大.md "wikilink") |    |
| 星期日   | [草地狀元](../Page/草地狀元.md "wikilink") |                                      |    |
|       |                                    |                                      |    |

## 已下檔節目

平日六點半檔

  - [天下女人心](../Page/天下女人心.md "wikilink")

週六十點檔

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/鍾無艷.md" title="wikilink">鍾無艷</a></li>
<li><a href="../Page/愛情魔髮師.md" title="wikilink">愛情魔髮師</a></li>
<li><a href="../Page/倪亞達.md" title="wikilink">倪亞達</a></li>
<li><a href="../Page/國民英雄.md" title="wikilink">國民英雄</a></li>
<li><a href="../Page/偷心大聖PS男.md" title="wikilink">偷心大聖PS男</a></li>
<li><a href="../Page/鬥牛要不要.md" title="wikilink">鬥牛要不要</a></li>
<li><a href="../Page/王子變青蛙.md" title="wikilink">王子變青蛙</a></li>
<li><a href="../Page/微笑pasta.md" title="wikilink">微笑pasta</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/櫻野三加一.md" title="wikilink">櫻野三加一</a></li>
<li><a href="../Page/命中注定我愛你.md" title="wikilink">命中注定我愛你</a></li>
<li><a href="../Page/小資女孩向前衝.md" title="wikilink">小資女孩向前衝</a></li>
<li><a href="../Page/向前走向愛走.md" title="wikilink">向前走向愛走</a></li>
<li><a href="../Page/回到愛以前.md" title="wikilink">回到愛以前</a></li>
<li><a href="../Page/就是要你爱上我.md" title="wikilink">就是要你爱上我</a></li>
<li><a href="../Page/再說一次我願意_(電視劇).md" title="wikilink">再說一次我願意</a></li>
<li><a href="../Page/愛上兩個我.md" title="wikilink">愛上兩個我</a></li>
</ul></td>
</tr>
</tbody>
</table>

週日十點檔

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/波麗士大人.md" title="wikilink">波麗士大人</a></li>
<li><a href="../Page/千金百分百.md" title="wikilink">千金百分百</a></li>
<li><a href="../Page/比賽開始.md" title="wikilink">比賽開始</a></li>
<li><a href="../Page/那一年的幸福時光.md" title="wikilink">那一年的幸福時光</a></li>
<li><a href="../Page/第2回合我愛你.md" title="wikilink">第2回合我愛你</a></li>
<li><a href="../Page/天國的嫁衣.md" title="wikilink">天國的嫁衣</a></li>
<li><a href="../Page/綠光森林.md" title="wikilink">綠光森林</a></li>
<li><a href="../Page/放羊的星星.md" title="wikilink">放羊的星星</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/敗犬女王.md" title="wikilink">敗犬女王</a></li>
<li><a href="../Page/下一站幸福.md" title="wikilink">下一站幸福</a></li>
<li><a href="../Page/金大花的華麗冒險.md" title="wikilink">金大花的華麗冒險</a></li>
<li><a href="../Page/螺絲小姐要出嫁.md" title="wikilink">螺絲小姐要出嫁</a></li>
<li><a href="../Page/我租了一個情人.md" title="wikilink">我租了一個情人</a></li>
<li><a href="../Page/莫非，這就是愛情.md" title="wikilink">莫非，這就是愛情</a></li>
<li><a href="../Page/喜歡·一個人.md" title="wikilink">喜歡·一個人</a></li>
<li><a href="../Page/我的自由年代.md" title="wikilink">我的自由年代</a></li>
</ul></td>
</tr>
</tbody>
</table>

特別節目

  - 《2018花蓮夏戀嘉年華》(2018/7/7-2018/7/14)

## 外部連結

  - [三立電視](http://www.iset.com.tw/portal/taiwan.php)

  -
  -
  -
[Category:三立國際台](../Category/三立國際台.md "wikilink")
[Category:三立電視旗下頻道](../Category/三立電視旗下頻道.md "wikilink")
[Category:日本衛星電視頻道](../Category/日本衛星電視頻道.md "wikilink")