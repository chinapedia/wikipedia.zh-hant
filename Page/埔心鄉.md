**埔心鄉**位於[臺灣](../Page/臺灣.md "wikilink")[彰化縣中部](../Page/彰化縣.md "wikilink")，地勢平坦，屬[彰化平原的一部份](../Page/彰化平原.md "wikilink")。

## 歷史沿革

  - [明鄭時期](../Page/明鄭時期.md "wikilink")（南明永曆十五年、1661年）\[1\]隸屬於一府二縣之東都承天府天興縣（1664年更改為天興州）。
  - [清](../Page/清.md "wikilink")[康熙二十三年](../Page/康熙.md "wikilink")（1684年）改隸[福建省](../Page/福建省.md "wikilink")[台灣府](../Page/台灣府.md "wikilink")[諸羅縣](../Page/諸羅縣.md "wikilink")。
  - [雍正元年](../Page/雍正.md "wikilink")（1723年）由諸羅縣劃出虎尾溪以北、大甲溪以南之地，設彰化縣治，取「彰聖天子丕昌海隅之化」前後兩字，而命名為彰化。彰化縣轄十六堡，現在的埔心鄉原屬武西堡，以「大埔心」為最大村莊。
  - [光緒十三年](../Page/光緒.md "wikilink")（1887年）台灣建省，彰化縣轄區縮小為[濁水溪以北](../Page/濁水溪.md "wikilink")、[大肚溪以南之地](../Page/大肚溪.md "wikilink")。光緒末期，彰化縣境置十三堡區，本鄉全名為「台灣省彰化縣武西堡坡心區」\[2\]。
  - 日治五十一年間（1895年至1945年），台灣地區行政建制有9次變革，本鄉前後隸屬於台灣縣、台灣民政支部彰化出張所、台中縣鹿港支廳、台中縣、彰化廳、台中廳及台中州等機構\[3\]。
  - 民國前十五年（明治三十年、1897年）劃歸台中縣員林辦務署，旋於民國前十三年改隸北斗辦務署，民國前十一年改制為彰化廳坡心區，民國前三年改為台中廳武西堡坡心區。
  - 埔心鄉位在[彰化縣之中心](../Page/彰化縣.md "wikilink")，昔日稱為「大埔心」（另有「小埔心」在今之埤頭鄉合興村），民國九年改為閩南語近似音之「坡心」，民國34年底，再正名為今日之「埔心」。
  - 本鄉的開發比附近鄉鎮稍晚，我們的祖先們蓽路藍縷，入墾之時這一帶都是荒蕪之地，由於中心之地適於居住，故稱為「埔心」\[4\]。現在面積20.9526平方公里，人口3萬5千多人。
  - 現今台灣包括福佬、客家、外省及原住民四大族群。除台灣北部及南部是客家人的大本營外，彰化也有許多客家人，埔心鄉便就是典型客家-{庄}-。鄉人中七、八成都是客家人，只不過經歷時間的推移，被周圍強勢的福佬人同化，變成了不會客語的福佬客了。彰化平原的客家人，大部分是在1696年時[施琅死後才大量自鹿港而登陸抵台來墾荒](../Page/施琅.md "wikilink")\[5\]。

## 公所組織

  - 埔心鄉公所是彰化縣埔心鄉最高層級的地方[行政機關](../Page/行政機關.md "wikilink")，在中華民國政府架構中為自治的行政機關，同時負責執行縣政府及中央機關委辦事項，埔心鄉的自治監督機關為[彰化縣政府](../Page/彰化縣政府.md "wikilink")。鄉長由全體鄉民直接選舉產生，任期為四年，可連選連任一次。埔心鄉公所並置鄉政會議，為鄉政最高決策機構，在鄉之下，設有6課3室等9個內部單位及4個附屬機關。

<!-- end list -->

  - [彰化縣埔心鄉民代表會是埔心鄉的最高](../Page/彰化縣埔心鄉民代表會.md "wikilink")[民意機關](../Page/民意機關.md "wikilink")，代表埔心鄉全體鄉民立法和監察鄉政。鄉民代表由公民直選選出，任期為四年，可連選連任。共有11位鄉民代表，第一選區7席鄉民代表、第二選區3席鄉民代表、第三選區代表1席鄉民代表，主席、副主席由11位鄉代表互選產生。

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>所內單位</strong></p>
<ul>
<li>民政課</li>
<li>財政課</li>
<li>社會課</li>
<li>建設課</li>
<li>農業課</li>
<li>行政課</li>
<li>人事室</li>
<li>主計室</li>
<li>政風室</li>
</ul></td>
<td><p><strong>附屬機關</strong></p>
<ul>
<li>彰化縣埔心鄉清潔隊</li>
<li>彰化縣埔心鄉立圖書館</li>
<li>彰化縣埔心鄉立幼兒園</li>
<li>彰化縣埔心鄉調解委員會</li>
</ul></td>
</tr>
</tbody>
</table>

## 歷任鄉長

  - 官派

|                                 |                                 |                                  |                                    |                                    |                                                                     |                     |                       |                   |                               |                               |                    |
| ------------------------------- | ------------------------------- | -------------------------------- | ---------------------------------- | ---------------------------------- | ------------------------------------------------------------------- | ------------------- | --------------------- | ----------------- | ----------------------------- | ----------------------------- | ------------------ |
| align ="center" width="3%" | 屆次 | align ="center" width="5%" | 姓名 | align ="center" width="12%" | 黨籍 | align ="center" width="10%" | 就任時間 | align ="center" width="10%" | 卸任時間 | align ="center" width="10%" | 備註 |----- style="background: \#FFFFFF | align ="center" | 1 | align ="center" | 張坤成 | align ="center" | | align ="center" | 1945年11月15日 | align ="center" | 1946年12月31日 | align ="left" | 官派 |

  - 代表會產生

|                                 |                                  |                                 |                                   |                                   |                                  |
| ------------------------------- | -------------------------------- | ------------------------------- | --------------------------------- | --------------------------------- | -------------------------------- |
| align ="center" width="10%"| 屆次 | align ="center" width="15%" | 姓名 | align ="center" width="10%"| 黨籍 | align ="center" width="20%"| 就任時間 | align ="center" width="20%"| 卸任時間 | align ="center" width="25%" | 備註 |
| align ="center" | 1             | align ="center" |張坤成             | align ="center" |               | align ="center" |1947年1月1日        | align ="center" |1948年12月16日      | align ="center" |代表會產生           |
| align ="center" | 2             | align ="center" |江慍木             | align ="center" |               | align ="center" |1948年12月17日      | align ="center" |1951年6月30日       | align ="center" |代表會產生           |

  - 民選鄉長

|                                 |                                  |                                 |                                   |                                   |                                                     |
| ------------------------------- | -------------------------------- | ------------------------------- | --------------------------------- | --------------------------------- | --------------------------------------------------- |
| align ="center" width="10%"| 屆次 | align ="center" width="15%" | 姓名 | align ="center" width="20%"| 黨籍 | align ="center" width="20%"| 就任時間 | align ="center" width="20%"| 卸任時間 | align ="center" width="15%" | 備註                    |
| align ="center" | 1             | align ="center" |黃耀坤             | align ="center" |               | align ="center" |1951年7月1日        | align ="center" |1953年6月30日       | align ="center" |民選首任                               |
| align ="center" | 2             | align ="center" |江慍木             | align ="center" |               | align ="center" |1953年7月1日        | align ="center" |1956年6月30日       | align ="center" |                                   |
| align ="center" | 3             | align ="center" |黃獅保             | align ="center" |               | align ="center" |1956年7月1日        | align ="center" |1960年1月15日       | align ="center" | |----style="background:\#73C2FB   |
| align ="center" | 15            | align ="center" |徐平圳             | align ="center" |               | align ="center" |2006年3月1日        | align ="center" |2010年2月28日       | align ="center" |                                   |
| align ="center" | 16            | align ="center" |徐平圳             | align ="center" |               | align ="center" |2010年3月1日        | align ="center" |2014年12月24日      | align ="center" | |----- style="background:\#99FF66 |

## 行政區域

  - **[坡心](../Page/坡心.md "wikilink")**：埔心村、義民村、油車村、東門村
  - **[太平](../Page/太平.md "wikilink")**：太平村、經口村
  - **[羅厝](../Page/羅厝.md "wikilink")**：羅厝村、芎蕉村
  - **[舊館](../Page/舊館.md "wikilink")**：舊-{館}-村、新-{館}-村、南-{館}-村
  - **[梧鳳](../Page/梧鳳.md "wikilink")**：梧鳳村、二重村
  - **[埤霞](../Page/埤霞.md "wikilink")**：埤霞村、埤腳村
  - **[瓦磘厝](../Page/瓦磘厝.md "wikilink")**：瓦北村、瓦中村、瓦南村
  - **[大溝尾](../Page/大溝尾.md "wikilink")**：大華村、仁里村

## 人口

## 金融

  - 彰化縣埔心鄉農會：忠義北路100號
  - [台中商業銀行埔心分行](../Page/台中商業銀行.md "wikilink")：中正路一段217號
  - 彰化第五信用合作社埔心分社：員鹿路一段239號
  - 彰化第六信用合作社埔心分社：員鹿路一段162號
  - [中華郵政股份有限公司埔心郵局](../Page/中華郵政.md "wikilink")：忠義北路70號
  - [中華郵政股份有限公司太平郵局](../Page/中華郵政.md "wikilink")：中山路66、68號

## 經濟

### 購物中心

  - [大潤發](../Page/大潤發.md "wikilink")
      - 員林店：彰化縣埔心鄉瓦南村中山路319號　(04)831-5058
  - [全聯福利中心](../Page/全聯福利中心.md "wikilink")
      - 埔心店：彰化縣埔心鄉員鹿路二段468號1樓　(04)829-7495
  - 全方位食品五金百貨大賣場
      - 埔心直營店：彰化縣埔心鄉員鹿路二段237號 (04)8285656
  - 好利多五金百貨大賣場
      - 埔心店：彰化縣埔心鄉永坡路三段354號

### 藥妝店

  - [丁丁藥妝](http://www.norbelbaby.com.tw/TinTin/)
      - 埔心店：彰化縣埔心鄉正義路100號　(04)829-7252

### 咖啡連鎖店

  - 85度C
      - 埔心店：彰化縣埔心鄉中正路一段296號

### 超商門市

  - <font color=green>**[7-eleven_logo.svg](https://zh.wikipedia.org/wiki/File:7-eleven_logo.svg "fig:7-eleven_logo.svg")7-Eleven**</font>
      - 員埔店：彰化縣埔心鄉員鹿路二段248號(04)8294556
      - 埔成店：彰化縣埔心鄉中正路一段137號139號1樓(04)8281820
      - 埔新店：彰化縣埔心鄉員鹿路一段145號(04)8284109
      - 順心店：彰化縣埔心鄉中山路145號147號149號(04)8399902
      - 彰醫店：彰化縣埔心鄉舊館村中興路1號(04)8280380
  - <font color=blue>**[FamilyMart_Logo_(2016-).svg](https://zh.wikipedia.org/wiki/File:FamilyMart_Logo_\(2016-\).svg "fig:FamilyMart_Logo_(2016-).svg")全家便利商店**</font>
      - 署醫店：彰化縣埔心鄉舊館村中興路35號(04)8284693、(04)7061012|}
  - <font color=red>**OK超商**</font>
      - 忠義店：彰化縣埔心鄉忠義北路1號(04)7003902
      - 員鹿店：彰化縣埔心鄉員鹿路一段123.125號(04)04-7003906
      - 新館店：彰化縣埔心鄉新館路726號1樓(04)04-7003913
      - 彰醫店：彰化縣埔心鄉舊館村中興路1號(04)8280380

## 警消

### 警察機關

  - [Republic_of_China_Police_Logo.svg](https://zh.wikipedia.org/wiki/File:Republic_of_China_Police_Logo.svg "fig:Republic_of_China_Police_Logo.svg")彰化縣警察局
      - 埔心分駐所：彰化縣埔心鄉員鹿路二段426號
      - 舊館派出所：彰化縣埔心鄉員鹿路四段182號

### 消防機關

  - [Republic_of_China_Fire_Services_Logo.svg](https://zh.wikipedia.org/wiki/File:Republic_of_China_Fire_Services_Logo.svg "fig:Republic_of_China_Fire_Services_Logo.svg")彰化縣消防局
      - 埔心分隊：彰化縣埔心鄉中正路二段155號

## 交通

### 公路

  - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號（中山高速公路）](../Page/中山高速公路.md "wikilink")
      - [埔鹽系統交流道](../Page/埔鹽系統交流道.md "wikilink")（207k，連接[TW_PHW76.svg](https://zh.wikipedia.org/wiki/File:TW_PHW76.svg "fig:TW_PHW76.svg")[台76線](../Page/台76線.md "wikilink")）位於邊界
  - [省道](https://zh.wikipedia.org/wiki/File:TW_PHW1.svg "fig:省道")[台1線](../Page/台1線.md "wikilink")：<font size= -1>員林市－埔心鄉－永靖鄉</font>
  - [省道](https://zh.wikipedia.org/wiki/File:TW_PHW76.svg "fig:省道")[台76線](../Page/台76線.md "wikilink")（東西向快速公路
    漢寶－草屯線）
      - [埔鹽系統交流道](../Page/埔鹽系統交流道.md "wikilink")（15k，連接[TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號](../Page/中山高速公路.md "wikilink")）位於邊界
      - 埔心交流道（19）
      - 員林交流道（22）
  - [縣道](https://zh.wikipedia.org/wiki/File:TW_CHW141.svg "fig:縣道")[縣道141號](../Page/縣道141號.md "wikilink")：<font size= -1>埔心鄉－員林市</font>
  - [縣道](https://zh.wikipedia.org/wiki/File:TW_CHW144.svg "fig:縣道")[縣道144號](../Page/縣道144號.md "wikilink")（台76線員林大排橋下側車道）：<font size= -1>大村鄉－埔心鄉－員林市</font>
  - [縣道](https://zh.wikipedia.org/wiki/File:TW_CHW146.svg "fig:縣道")[縣道146號](../Page/縣道146號.md "wikilink")：<font size= -1>大村鄉－埔心鄉－溪湖鎮</font>
  - [縣道](https://zh.wikipedia.org/wiki/File:TW_CHW148.svg "fig:縣道")[縣道148號](../Page/縣道148號.md "wikilink")：<font size= -1>溪湖鎮－埔心鄉－員林市</font>

## 教育

### 國民中學

  - [彰化縣立埔心國民中學](http://163.23.69.202/)

### 國民小學

  - [彰化縣埔心鄉埔心國民小學](http://www.pses.chc.edu.tw)
  - [彰化縣埔心鄉太平國民小學](http://www.tpes.chc.edu.tw)
  - [彰化縣埔心鄉明聖國民小學](http://www.msps.chc.edu.tw)
  - [彰化縣埔心鄉梧鳳國民小學](http://www.wfes.chc.edu.tw)
  - [彰化縣埔心鄉舊-{舘}-國民小學](http://163.23.98.129)
  - [彰化縣埔心鄉羅厝國民小學](http://www.rtes.chc.edu.tw)
  - [彰化縣埔心鄉鳳霞國民小學](http://163.23.99.1)

## 旅遊

### 宗教

  - [太平茄苳公](../Page/太平茄苳公.md "wikilink")
  - [羅厝天主堂](../Page/羅厝天主堂.md "wikilink")（[彰化縣歷史建築](../Page/彰化縣歷史建築.md "wikilink")）
  - [埔心忠義廟](../Page/埔心忠義廟.md "wikilink")
  - [埔心五通宮](../Page/埔心五通宮.md "wikilink")
  - [武聖宮](../Page/武聖宮.md "wikilink")
  - [五湖宮](../Page/五湖宮.md "wikilink")

### 設施

  - [埔心鄉運動公園](../Page/埔心鄉運動公園.md "wikilink")
  - [舊館派出所舊廳舍](../Page/舊館派出所舊廳舍.md "wikilink")（[員鹿路四段](../Page/員鹿路.md "wikilink")182號）
  - [舊館國小日式建築](../Page/舊館國小.md "wikilink")（員鹿路四段201號）

### 民居

  - [黃耀南武舉人宅](../Page/黃耀南武舉人宅.md "wikilink")（[武英北路](../Page/武英北路.md "wikilink")272號）
  - [黃三元故居](../Page/黃三元故居.md "wikilink")（[瑤鳳路三段](../Page/瑤鳳路.md "wikilink")446號）
  - [張氏家廟長源堂](../Page/張氏家廟長源堂.md "wikilink")（[明聖路二段](../Page/明聖路.md "wikilink")365巷41號）
  - [涂氏宗祠南昌堂](../Page/涂氏宗祠南昌堂.md "wikilink")（[南昌西路](../Page/南昌西路.md "wikilink")152號）
  - [張氏古厝](../Page/張氏古厝.md "wikilink")（[仁安路](../Page/仁安路.md "wikilink")324號）

## 醫療

  - [衛生福利部彰化醫院](../Page/衛生福利部.md "wikilink")

## 名人

  - [魏明谷](../Page/魏明谷.md "wikilink")：前[立法委員](../Page/立法委員.md "wikilink")、[彰化縣縣長](../Page/彰化縣縣長.md "wikilink")。

## 參考文獻

## 外部連結

  - [埔心鄉公所](http://www.puxin.gov.tw/)

[Category:彰化縣行政區劃](../Category/彰化縣行政區劃.md "wikilink")
[埔心鄉](../Category/埔心鄉.md "wikilink")

1.  臺灣省彰化縣《埔心鄉志》張榮昌撰寫

2.
3.
4.
5.