**-{zh-cn:接口描述语言;zh-tw:介面描述語言}-**（Interface description
language，缩写**IDL**），是用来描述[软件组件](../Page/软件组件.md "wikilink")[介面的一种](../Page/介面.md "wikilink")[计算机语言](../Page/计算机语言.md "wikilink")。IDL通过一种中立的方式来描述接口，使得在不同平台上运行的对象和用不同语言编写的程序可以相互通信交流；比如，一个组件用[C++写成](../Page/C++.md "wikilink")，另一个组件用[Java写成](../Page/Java.md "wikilink")。

IDL通常用于[远程调用软件](../Page/远程调用.md "wikilink")。在这种情况下，一般是由远程客户终端调用不同操作系统上的对象组件，并且这些对象组件可能是由不同计算机语言编写的。IDL建立起了两个不同操作系统间通信的桥梁。

在IDL基础上开发出来的软件系统有[Sun的](../Page/Sun_Microsystems.md "wikilink")[ONC
RPC](../Page/ONC_RPC.md "wikilink")，[The Open
Group的](../Page/The_Open_Group.md "wikilink")[Distributed Computing
Environment](../Page/Distributed_Computing_Environment.md "wikilink")，[IBM的](../Page/IBM.md "wikilink")[System
Object Model](../Page/System_Object_Model.md "wikilink")，[Object
Management
Group的](../Page/Object_Management_Group.md "wikilink")[CORBA](../Page/CORBA.md "wikilink")，和[SOAP](../Page/Simple_Object_Access_Protocol.md "wikilink")（用于[Web
service](../Page/Web_service.md "wikilink")）。

## Interface definition languages

  - [IDL specification
    language](../Page/IDL_specification_language.md "wikilink")，the
    original Interface Description Language.
  - [Microsoft Interface Definition
    Language](../Page/Microsoft_Interface_Definition_Language.md "wikilink")
  - [Open Service Interface
    Definitions](../Page/Open_Service_Interface_Definitions.md "wikilink")
  - [Platform-Independent Component Modeling
    Language](../Page/Platform-Independent_Component_Modeling_Language.md "wikilink")
  - [Simple Object Access
    Protocol](../Page/Simple_Object_Access_Protocol.md "wikilink")（SOAP）
  - [WDDX](../Page/WDDX.md "wikilink")
  - [XML-RPC](../Page/XML-RPC.md "wikilink")，the predecessor of SOAP

## 项目

  - JavaSE
    Corba[1](http://docs.oracle.com/javase/6/docs/technotes/guides/idl/index.html)
  - Thrift [2](http://thrift.apache.org)
  - ICE [3](https://www.zeroc.com)

## 外部链接

  - [OMG Tutorial on OMG
    IDL](http://www.omg.org/gettingstarted/omg_idl.htm)
  - [OMG Specification of OMG
    IDL](http://www.omg.org/cgi-bin/doc?formal/02-06-39)

[Category:規範語言](../Category/規範語言.md "wikilink")
[Category:程序设计语言](../Category/程序设计语言.md "wikilink")