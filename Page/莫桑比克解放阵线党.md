**莫桑比克解放陣線党**（）是[莫桑比克的一個政黨](../Page/莫桑比克.md "wikilink")。该党成立于1962年，初名**莫桑比克解放阵线**（Frente
de Libertação de Moçambique，缩写为Frelimo）。

该党成立后便开始领导反对[葡萄牙殖民统治的](../Page/葡萄牙.md "wikilink")[莫桑比克独立战争](../Page/莫桑比克独立战争.md "wikilink")。1975年，该党建立了[莫桑比克人民共和国](../Page/莫桑比克人民共和国.md "wikilink")。1977年，在该党“三大”上，更改党名为现名，并正式决定以[马列主义作为党的指导思想](../Page/马列主义.md "wikilink")。1989年，受[东欧剧变影响](../Page/东欧剧变.md "wikilink")，该党放弃了马列主义意识形态，又放弃了社会主义制度。1994年，该党赢得了莫桑比克首次多党大选。此后多次大选，该党均获胜，使该党一直保持着执政党地位。

[2014年莫三比克大選後](../Page/2014年莫三比克大選.md "wikilink")，莫桑比克解放陣線在[共和国议会](../Page/共和国议会_\(莫桑比克\).md "wikilink")250席中佔有144席。

[分類:1962年建立的政黨](../Page/分類:1962年建立的政黨.md "wikilink")

[Category:莫桑比克政黨](../Category/莫桑比克政黨.md "wikilink")
[Category:社會黨國際](../Category/社會黨國際.md "wikilink")
[Category:民族解放運動](../Category/民族解放運動.md "wikilink")