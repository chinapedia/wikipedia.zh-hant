**韋斯卡省**（；,；，）\[1\]
為[西班牙北部的一個](../Page/西班牙.md "wikilink")[省份](../Page/西班牙行政區劃.md "wikilink")，位於[阿拉貢自治區的北部](../Page/阿拉貢自治區.md "wikilink")。它的四周有[萊里達省](../Page/萊里達省.md "wikilink")、[薩拉戈薩省](../Page/薩拉戈薩省.md "wikilink")、[納瓦拉自治區以及](../Page/納瓦拉自治區.md "wikilink")[法國](../Page/法國.md "wikilink")。

韋斯卡省土地面積為15,626
km²，人口有228,566（2010年），大約有四分之一的人口居住在首府[韋斯卡](../Page/韋斯卡.md "wikilink")。總共有202個自治市。

## 地理

韋斯卡省境內內有最高峰[阿內托峰](../Page/阿內托峰.md "wikilink")，海拔3,404米。

## 人口

[Huesca-municipiosgrandesl22.png](https://zh.wikipedia.org/wiki/File:Huesca-municipiosgrandesl22.png "fig:Huesca-municipiosgrandesl22.png")

<table>
<thead>
<tr class="header">
<th><p>市镇</p></th>
<th><p>面积<br />
Km2</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/萨维尼亚尼戈.md" title="wikilink">萨维尼亚尼戈</a></p></td>
<td><p>586,8</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/弗拉加.md" title="wikilink">弗拉加</a></p></td>
<td><p>437,6</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈卡_(西班牙).md" title="wikilink">哈卡</a></p></td>
<td><p>406,3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/格劳斯.md" title="wikilink">格劳斯</a></p></td>
<td><p>299,8</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿因萨-索夫拉尔韦.md" title="wikilink">阿因萨-索夫拉尔韦</a></p></td>
<td><p>284,8</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/萨里涅纳.md" title="wikilink">萨里涅纳</a></p></td>
<td><p>275,6</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴列德埃乔.md" title="wikilink">巴列德埃乔</a></p></td>
<td><p>234,4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/贝纳斯克.md" title="wikilink">贝纳斯克</a></p></td>
<td><p>233,6</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安索.md" title="wikilink">安索</a></p></td>
<td><p>223,1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉富埃瓦.md" title="wikilink">拉富埃瓦</a></p></td>
<td><p>218,8</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<caption>align=center style="font-weight:bold" | 市镇人口一览表</caption>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:Huesca_desde_la_Catedral_I.JPG" title="fig:Huesca_desde_la_Catedral_I.JPG">Huesca_desde_la_Catedral_I.JPG</a><br />
<a href="../Page/韦斯卡.md" title="wikilink">韦斯卡</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Monzón_(13730896693)_(cropped).jpg" title="fig:Monzón_(13730896693)_(cropped).jpg">Monzón_(13730896693)_(cropped).jpg</a><br />
<a href="../Page/蒙宗.md" title="wikilink">蒙宗</a><br />
<a href="https://zh.wikipedia.org/wiki/File:Vista_Barbastro_Catedral_(Huesca).JPG" title="fig:Vista_Barbastro_Catedral_(Huesca).JPG">Vista_Barbastro_Catedral_(Huesca).JPG</a><br />
<a href="../Page/巴尔瓦斯特罗.md" title="wikilink">巴尔瓦斯特罗</a><br />
</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>排名</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p><small>来源: {{tsl|es|Instituto Nacional de Estadística (España)</p></td>
</tr>
</tbody>
</table>

## 语言

韋斯卡省是[庇里牛斯山河谷地區僅存的說](../Page/庇里牛斯山.md "wikilink")[阿拉貢語的地方](../Page/阿拉貢語.md "wikilink")，在其東部也有幾個說[加泰羅尼亞語的地方](../Page/加泰羅尼亞語.md "wikilink")。

## 注释

[\*](../Category/韦斯卡省.md "wikilink")
[Huesca](../Category/西班牙省份.md "wikilink")
[Category:阿拉貢省份](../Category/阿拉貢省份.md "wikilink")

1.  [Ley 27/2002 de creación de la Comarca de Hoya de Huesca/Plana de
    Uesca. (BOA 27/2002, 26 de
    noviembre](http://www.comarcas.es/index.php/mod.normativas/mem.detalle/idnormativa.41/relcategoria.201/chk.69a99ff9a8f6383f39984e915502402b.html)**Huesca/Uesca**均为官方名称.