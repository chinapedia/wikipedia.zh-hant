**倒棘鯒**，又稱**松葉倒棘牛尾魚**，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮋形目](../Page/鮋形目.md "wikilink")[牛尾魚科的一](../Page/牛尾魚科.md "wikilink")[種](../Page/種.md "wikilink")。分布于日本本州的中部以南及菲律宾各近海内以及中国[南海](../Page/南海.md "wikilink")、[台湾海峡等海域](../Page/台湾海峡.md "wikilink")，属于热带底层海鱼。其常栖息于沙底地区。该物种的模式产地在日本。\[1\]

## 深度

水深5至50公尺。

## 特徵

頭部平扁，有強突起之稜線及銳棘，前鰓蓋骨後緣有1枚肥大之棘；下緣有1向前逆生之棘；鰓蓋主骨邊緣無皮質突起，眼眶上緣無觸角突起。體側背[黃褐色](../Page/黃褐色.md "wikilink")，具6個[褐色斑點](../Page/褐色.md "wikilink")。背鰭據8行點列橫紋。體長可達20公分。

## 生態

本魚棲息於沙泥質海底，常潛於沙中，露出兩眼，伺機捕食魚[蝦等](../Page/蝦.md "wikilink")。

## 經濟利用

[食用魚](../Page/食用魚.md "wikilink")，可用油煎食。

## 参考文献

  -
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[asper](../Category/倒棘鯒屬.md "wikilink")

1.