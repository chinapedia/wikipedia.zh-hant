**雲仙天草國立公園**（）是一位于[長崎縣](../Page/長崎縣.md "wikilink")[島原半島與](../Page/島原半島.md "wikilink")[熊本縣](../Page/熊本縣.md "wikilink")[天草群島的](../Page/天草群島.md "wikilink")[日本國立公園](../Page/日本國立公園.md "wikilink")（另含有小部份[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")）。[雲仙地区于](../Page/雲仙.md "wikilink")1934年3月16日成為**雲仙国立公園**，與[瀨戶内海國立公園](../Page/瀨戶内海國立公園.md "wikilink")，霧島國立公園（現・[霧島屋久國立公園](../Page/霧島屋久國立公園.md "wikilink")）并為日本最早的國立公園。天草地区于1956年7月20日編入，現更名為雲仙天草國立公園。

## 主要景點

### 雲仙地域

  - [雲仙岳](../Page/雲仙岳.md "wikilink")
  - [雲仙溫泉](../Page/雲仙溫泉.md "wikilink")

### 天草地域

  - [天草松島](../Page/天草松島.md "wikilink")
  - [富岡海岸](../Page/富岡海岸.md "wikilink")

## 面積

  - 面積 - 28,279ha （陸域部分）
      - 長崎縣 - 12,858ha
      - 熊本縣 - 13,974ha
      - 鹿兒島縣 - 1,447ha

## 關連市町村

  - 長崎縣 -
    [島原市](../Page/島原市.md "wikilink")、[雲仙市](../Page/雲仙市.md "wikilink")、[南島原市](../Page/南島原市.md "wikilink")
  - 熊本縣 -
    [天草市](../Page/天草市.md "wikilink")、[上天草市](../Page/上天草市.md "wikilink")、[苓北町](../Page/苓北町.md "wikilink")
  - 鹿兒島縣 - [長島町](../Page/長島町.md "wikilink")

## 關連項目

  - [日本國立公園](../Page/日本國立公園.md "wikilink")

## 参考资料

## 外部連結

  - [雲仙天草国立公園（財団法人自然公園財団）](http://www.bes.or.jp/unzen/)

  - [雲仙天草国立公園（環境省）](https://www.env.go.jp/park/unzen/)

[Category:日本國立公園](../Category/日本國立公園.md "wikilink")
[Category:長崎縣地理](../Category/長崎縣地理.md "wikilink")
[Category:熊本縣地理](../Category/熊本縣地理.md "wikilink")
[Category:鹿兒島縣地理](../Category/鹿兒島縣地理.md "wikilink")
[Category:島原市](../Category/島原市.md "wikilink")
[Category:雲仙市](../Category/雲仙市.md "wikilink")
[Category:南島原市](../Category/南島原市.md "wikilink")
[Category:天草市](../Category/天草市.md "wikilink")
[Category:上天草市](../Category/上天草市.md "wikilink")
[Category:苓北町](../Category/苓北町.md "wikilink")
[Category:長島町](../Category/長島町.md "wikilink")
[Category:1934年設立的保護區](../Category/1934年設立的保護區.md "wikilink")
[Category:1934年日本建立](../Category/1934年日本建立.md "wikilink")