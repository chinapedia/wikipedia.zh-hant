**西北區**（）是[博茨瓦納的一個區](../Page/博茨瓦納.md "wikilink")，位於該國北部，西鄰[納米比亞](../Page/納米比亞.md "wikilink")，東鄰[津巴布韋](../Page/津巴布韋.md "wikilink")，北部與[贊比亞分享一小段](../Page/贊比亞.md "wikilink")[尚比西河](../Page/尚比西河.md "wikilink")。2001年由恩加米蘭區和喬貝區合併而成。面積129,930平方公里，2001年人口142,970人。首府[馬翁](../Page/馬翁.md "wikilink")。下分4次區。

著名的[奧卡萬戈沼澤位於本區](../Page/奧卡萬戈沼澤.md "wikilink")。

[N](../Category/博茨瓦納行政區劃.md "wikilink") [Category:西北區
(波札那)](../Category/西北區_\(波札那\).md "wikilink")