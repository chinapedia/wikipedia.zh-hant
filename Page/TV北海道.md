**株式會社TV北海道**（，正式英文名稱為Television Hokkaido Broadcasting Co.,
Ltd.）是[TXN的加盟](../Page/東京電視網.md "wikilink")[電視台](../Page/電視台.md "wikilink")，成立於1988年6月，開播于1989年10月1日且同時加盟TXN，[數位電視呼號為JOHI](../Page/數位電視.md "wikilink")-DTV。放送范圍為[北海道](../Page/北海道.md "wikilink")。簡稱TVh。是[北海道新聞社的](../Page/北海道新聞.md "wikilink")[關係企業](../Page/關係企業.md "wikilink")。
[TVH_Sapporo_HQ_20070601-001.jpg](https://zh.wikipedia.org/wiki/File:TVH_Sapporo_HQ_20070601-001.jpg "fig:TVH_Sapporo_HQ_20070601-001.jpg")

## 外部連結

  - [TV北海道](http://www.tv-hokkaido.co.jp/index.html)
  - [TV北海道放送區域](https://web.archive.org/web/20070928000416/http://www.c-direct.ne.jp/tvtokyo/jpn/media/img/tvh.jpg)

[Category:日本電視台](../Category/日本電視台.md "wikilink")
[Category:札幌市公司](../Category/札幌市公司.md "wikilink")
[Category:1988年日本建立](../Category/1988年日本建立.md "wikilink")
[Category:東京電視網](../Category/東京電視網.md "wikilink")