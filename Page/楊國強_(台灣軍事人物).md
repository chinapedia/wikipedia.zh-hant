**楊國強**（），[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[中將退役](../Page/中將.md "wikilink")，生於[臺灣省](../Page/臺灣省.md "wikilink")[新竹市](../Page/新竹市.md "wikilink")[眷村](../Page/眷村.md "wikilink")，籍貫[河南省](../Page/河南省.md "wikilink")[新野縣](../Page/新野縣.md "wikilink")，畢業於[陸軍官校](../Page/陸軍官校.md "wikilink")41期裝甲兵科、[南非共和國](../Page/南非共和國.md "wikilink")[陸軍學院](../Page/陸軍.md "wikilink")、[美國陸軍](../Page/美國陸軍.md "wikilink")[戰爭學院](../Page/戰爭學院.md "wikilink")，在[國安會](../Page/中華民國國家安全會議.md "wikilink")[高華柱秘書長時期出任](../Page/高華柱.md "wikilink")[國安局局長](../Page/中華民國國家安全局.md "wikilink")。

曾任[國防部](../Page/中華民國國防部.md "wikilink")[總政治作戰部軍紀監察處處長](../Page/總政治作戰部.md "wikilink")、財產申報處處長、[陸軍裝甲兵學校校長](../Page/陸軍裝甲兵訓練指揮部.md "wikilink")、[陸軍軍官學校校長](../Page/陸軍軍官學校.md "wikilink")、國家安全局副局長、駐[美國](../Page/美國.md "wikilink")[華府中將特派員](../Page/華府.md "wikilink")、[欣欣客運](../Page/欣欣客運.md "wikilink")[董事長](../Page/董事長.md "wikilink")\[1\]。

2017年3月24日，[蔡英文總統任命楊國強中將為](../Page/中華民國總統.md "wikilink")[中華民國駐](../Page/中華民國.md "wikilink")[泰國](../Page/泰國.md "wikilink")[大使](../Page/大使.md "wikilink")\[2\]。4月19日，已向總統[蔡英文請辭](../Page/蔡英文.md "wikilink")，原因是家人的健康因素\[3\]。

## 引用

[Category:台灣軍事人物](../Category/台灣軍事人物.md "wikilink")
[Category:新野人](../Category/新野人.md "wikilink")
[Guo國強](../Category/楊姓.md "wikilink")
[Category:中華民國陸軍軍官學校校友](../Category/中華民國陸軍軍官學校校友.md "wikilink")
[Category:中華民國陸軍中將](../Category/中華民國陸軍中將.md "wikilink")
[Category:美國陸軍戰爭學院校友](../Category/美國陸軍戰爭學院校友.md "wikilink")
[Category:河南裔台灣人](../Category/河南裔台灣人.md "wikilink")
[Category:中華民國國家安全局局長](../Category/中華民國國家安全局局長.md "wikilink")

1.
2.  [國安前局長楊國強
    出任駐泰國大使](http://www.appledaily.com.tw/appledaily/article/headline/20170325/37595598/)
3.