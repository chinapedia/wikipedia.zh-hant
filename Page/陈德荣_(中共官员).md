**陈德荣**，[浙江](../Page/浙江.md "wikilink")[永嘉人](../Page/永嘉.md "wikilink")；[武汉钢铁学院冶金系钢铁冶金专业毕业](../Page/武汉钢铁学院.md "wikilink")，工学硕士学位、高级工程师。1992年3月加入[中国共产党](../Page/中国共产党.md "wikilink")。曾任[中共浙江省委常委](../Page/中共浙江省委.md "wikilink")、[温州](../Page/温州.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")，[浙江省人民政府副省长等职](../Page/浙江省人民政府.md "wikilink")。现任[中国宝武钢铁集团有限公司董事长](../Page/中国宝武钢铁集团有限公司.md "wikilink")、党委书记。

## 生平

大学毕业后，陈德荣被分配到杭州钢铁厂工作；历任厂技术开发处处长助理、副处长，转炉炼钢分厂副厂长、厂长；后担任浙江冶金集团([杭州钢铁集团公司](../Page/杭州钢铁集团.md "wikilink"))副总经理。

1998年4月，陈德荣调任[嘉兴市副市长](../Page/嘉兴市.md "wikilink")，开启从政之路；后历任市委常委、常务副市长，市委副书记、嘉兴市代市长、市长；2007年8月，出任[中共](../Page/中共.md "wikilink")[嘉兴](../Page/嘉兴.md "wikilink")[市委书记](../Page/市委书记.md "wikilink")；2010年7月，升任[浙江省人民政府副省长](../Page/浙江省人民政府.md "wikilink")、兼中共[温州市委书记](../Page/温州.md "wikilink")。在主政温州期间，正值[全球金融危机](../Page/全球金融危机.md "wikilink")，当地经济受民间借贷危机、中小企业资金链断裂、房地产泡沫破裂等严重影响，陈德荣推进改革，以勤政出名，曾经因为劳累三度晕倒。\[1\]
2012年6月，在[中共浙江省第十三届委员会第一次上](../Page/中共浙江省委.md "wikilink")，当选省委常委\[2\]。2013年6月起，任中共浙江省委常委、浙江生态省建设工作领导小组副组长，省新农村建设领导小组副组长。

2014年7月，调任[宝钢集团有限公司](../Page/宝钢集团有限公司.md "wikilink")，任集团总经理，并兼任宝钢集团旗下的宝钢股份董事长\[3\]。2016年10月31日，任新重组的[中国宝武钢铁集团有限公司总经理](../Page/中国宝武钢铁集团有限公司.md "wikilink")、董事、党委副书记\[4\]，2018年6月28日升任董事长、党委书记\[5\]。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

{{-}}

[category:第十届全国人大代表](../Page/category:第十届全国人大代表.md "wikilink")
[category:武汉科技大学校友](../Page/category:武汉科技大学校友.md "wikilink")
[category:永嘉人](../Page/category:永嘉人.md "wikilink")
[De德](../Page/category:陈姓.md "wikilink")

[Category:中共浙江省委常委](../Category/中共浙江省委常委.md "wikilink")
[Category:中华人民共和国浙江省副省长](../Category/中华人民共和国浙江省副省长.md "wikilink")
[Category:中共温州市委书记](../Category/中共温州市委书记.md "wikilink")
[Category:中共嘉兴市委书记](../Category/中共嘉兴市委书记.md "wikilink")
[Category:中华人民共和国嘉兴市市长](../Category/中华人民共和国嘉兴市市长.md "wikilink")
[Category:宝武集团人物](../Category/宝武集团人物.md "wikilink")

1.  [温州市委书记陈德荣去职引猜测](http://news.takungpao.com/mainland/focus/2013-07/1744027.html)
2.  [赵洪祝当选中共浙江省委书记](http://renshi.people.com.cn/GB/18131426.html)
3.  [陈德荣任宝钢总经理
    曾任浙江省副省长](http://www.ce.cn/xwzx/gnsz/gdxw/201407/31/t20140731_3264633.shtml)，中国经济网，2014年7月31日
4.
5.