**美國銀行廣場**（**Bank of America
Plaza**）是位於[美國](../Page/美國.md "wikilink")[佐治亞州](../Page/佐治亞州.md "wikilink")[亞特蘭大的](../Page/亞特蘭大.md "wikilink")[摩天大樓](../Page/摩天大樓.md "wikilink")，名稱來源自其最大業主[美國銀行](../Page/美國銀行.md "wikilink")，建於1992年。大廈樓高55層，高311.8米（1,023英尺）。是[北美洲](../Page/北美洲.md "wikilink")（[紐約市及](../Page/紐約市.md "wikilink")[芝加哥以外](../Page/芝加哥.md "wikilink")）的第一高樓。

## 參考

## 參見

  - [美國摩天大樓](../Page/美國摩天大樓.md "wikilink")
  - [美國銀行大廈](../Page/美國銀行大廈.md "wikilink")
  - [美國銀行中心](../Page/美國銀行中心.md "wikilink")

## 外部連結

  - [美國銀行廣場 - Emporis](http://www.emporis.com/en/wm/bu/?id=121137)
  - [美國銀行廣場 -
    SkyscraperPage](http://www.skyscraperpage.com/cities/?buildingID=57)

[Category:美國摩天大樓](../Category/美國摩天大樓.md "wikilink")
[Category:1992年完工建築物](../Category/1992年完工建築物.md "wikilink")
[Category:300米至349米高的摩天大樓](../Category/300米至349米高的摩天大樓.md "wikilink")