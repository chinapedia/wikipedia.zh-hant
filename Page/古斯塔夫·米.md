**古斯塔夫·米**（[德语](../Page/德语.md "wikilink")：****，），[德国](../Page/德国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")。

## 传记

米生于[罗斯托克](../Page/罗斯托克.md "wikilink")。1886年，他進入[罗斯托克大学攻讀](../Page/罗斯托克大学.md "wikilink")[数学和](../Page/数学.md "wikilink")[物理学](../Page/物理学.md "wikilink")。
除了他的主修科目，他也學[生化学](../Page/生化学.md "wikilink")、[动物学](../Page/动物学.md "wikilink")、[地质学](../Page/地质学.md "wikilink")、[矿物学](../Page/矿物学.md "wikilink")、[天文学](../Page/天文学.md "wikilink")、[逻辑和](../Page/逻辑.md "wikilink")[形上学的课程](../Page/形上学.md "wikilink")。在1889年，年仅22岁的他在[海德堡大学取得了数学博士学位](../Page/海德堡大学.md "wikilink")。

1897年，他获得了[哥廷根大学](../Page/哥廷根大学.md "wikilink")[理论物理学的授课资格](../Page/理论物理学.md "wikilink")。在1902年，他成为格赖夫斯瓦尔德大学的理论物理学特别教授。1917年，他成为[哈雷-维滕贝格大学实验物理学的全职教授](../Page/哈雷-维滕贝格大学.md "wikilink")。1924年，前往[弗莱堡大学任教直至退休](../Page/弗莱堡大学.md "wikilink")。

他在1957年于[弗莱堡辞世](../Page/弗莱堡.md "wikilink")。

## 相关条目

  - [米氏散射](../Page/米氏散射.md "wikilink")

[Category:德国物理学家](../Category/德国物理学家.md "wikilink")
[Category:弗賴堡大學教師](../Category/弗賴堡大學教師.md "wikilink")
[Category:哈雷-維滕貝格大學教師](../Category/哈雷-維滕貝格大學教師.md "wikilink")
[Category:格賴夫斯瓦爾德大學教師](../Category/格賴夫斯瓦爾德大學教師.md "wikilink")
[Category:哥廷根大學教師](../Category/哥廷根大學教師.md "wikilink")
[Category:海德堡大學校友](../Category/海德堡大學校友.md "wikilink")
[Category:羅斯托克大學校友](../Category/羅斯托克大學校友.md "wikilink")
[Category:梅克倫堡-前波門人](../Category/梅克倫堡-前波門人.md "wikilink")