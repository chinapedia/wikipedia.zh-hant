**何志浩**（），[中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[中將](../Page/中將.md "wikilink")，[浙江](../Page/浙江.md "wikilink")[宁波](../Page/宁波.md "wikilink")[象山人](../Page/象山.md "wikilink")。

[国立中央大学經濟系肄業](../Page/国立中央大学_\(南京\).md "wikilink")。[黃埔軍校第四期畢業](../Page/黃埔軍校.md "wikilink")。曾在[陸軍步兵學校](../Page/陸軍步兵學校.md "wikilink")、[陸軍大學](../Page/陸軍大學.md "wikilink")、[陸軍參謀大學](../Page/陸軍參謀大學.md "wikilink")、[國防大學學習](../Page/國防大學.md "wikilink")。抗戰期間擔任國民軍事訓練委員會主席等職。曾任浙江綏靖總部中將副司令等職。1949年到[臺灣](../Page/臺灣.md "wikilink")。曾任國民革命軍第二十五師師長、總政治部政戰計畫委員會主任委員、總統府參軍等職。退役後受聘為[中國文化大學教授](../Page/中國文化大學.md "wikilink")。
[一江新城一江山烈士歌.jpg](https://zh.wikipedia.org/wiki/File:一江新城一江山烈士歌.jpg "fig:一江新城一江山烈士歌.jpg")[中和區一江新城](../Page/中和區.md "wikilink")，刻有何志浩創作、[賈景德書寫的](../Page/賈景德.md "wikilink")〈一江山烈士歌〉。\]\]
何志浩將軍也是一位詞人。軍紀歌、[中國抗日軍歌](../Page/中國抗日軍歌.md "wikilink")、[國軍陸軍軍歌等都由他作詞](../Page/國軍陸軍軍歌.md "wikilink")。他也曾擔任中華民族舞蹈協會理事長。曾加入[中國文藝協會](../Page/中國文藝協會.md "wikilink")。

1950年何志浩將軍到「大嶼」[七美人塚憑弔](../Page/七美人塚.md "wikilink")，感念她們英雌不讓鬚眉，遂立碑改「大嶼」為「[七美嶼](../Page/七美嶼.md "wikilink")」。碑文如下：

> 明嘉靖間，倭寇侵澎湖掠大嶼，有七美女不受寇辱投井全貞，鄉人掩井葬之，塚上長香花樹七株，終年不凋，令改大嶼為七美嶼，以紀念節烈。民國三十九年秋，余與李縣長玉林至島上弔七美人塚，感其氣節，逐為之歌：
>
> 「七美人兮白壁姿，抱貞拒賊兮死隨之，英魂永寄孤芳樹，井上長春兮開滿枝。」

## 詞作

中華民國陸軍軍歌
風雲起，山河動，黃埔建軍聲勢雄，革命壯士矢精忠。
金戈鐵馬，百戰沙場，安內攘外作先鋒。縱橫掃蕩，復興中華，所向無敵，立大功。
旌旗耀，金鼓響，龍騰虎躍軍威壯，忠誠精實風紀揚。
機動攻勢，勇敢沈著，奇襲主動智謀廣。肝膽相照，團結自強，殲滅敵寇，凱歌唱。

## 外部連結

[「陸軍軍歌」作者何志浩將軍過世](http://www.ptt.cc/man/Militarylife/DA82/M.1187699066.A.872.html)

[H何](../Category/中華民國大陸時期軍事人物.md "wikilink")
[H何](../Category/中華民國陸軍中將.md "wikilink")
[H何](../Category/中華民國總統府相關官員及顧問.md "wikilink")
[法H何](../Category/中央大学校友.md "wikilink")
[H何](../Category/南京大学校友.md "wikilink")
[H何](../Category/中國文化大學教授.md "wikilink")
[H何](../Category/象山人.md "wikilink") [Z](../Category/何姓.md "wikilink")
[H何](../Category/中华民国人瑞.md "wikilink")
[Category:國防大學校友](../Category/國防大學校友.md "wikilink")
[Category:台灣戰後寧波移民](../Category/台灣戰後寧波移民.md "wikilink")