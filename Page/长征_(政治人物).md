[缩略图](https://zh.wikipedia.org/wiki/File:Truong_Chinh,_Le_Duan,_Nicolae_og_Elena_Ceausescu.jpeg "fig:缩略图")夫妇合影\]\]
**长征**（，）是[越南政治人物](../Page/越南.md "wikilink")，[越南共产党](../Page/越南共产党.md "wikilink")、[越南民主共和国和](../Page/越南民主共和国.md "wikilink")[越南社会主义共和国的主要缔造者和领导人之一](../Page/越南社会主义共和国.md "wikilink")。曾任[越共中央总书记和](../Page/越共中央总书记.md "wikilink")[国务委员会主席](../Page/越南国家主席.md "wikilink")。原名**邓春区**（），因仰慕[中国共产党领导的](../Page/中国共产党.md "wikilink")[红军](../Page/中國工農紅軍.md "wikilink")[长征](../Page/长征.md "wikilink")，因而改名为「长征」。

## 生平

长征1927年参加[越南青年革命同志会](../Page/越南共产党.md "wikilink")。1928年因参加反法示威游行被捕。后参加[印度支那共产党](../Page/印度支那共产党.md "wikilink")，在[河内主编党报](../Page/河内.md "wikilink")。1932年被[法国人逮捕](../Page/法国.md "wikilink")，坐牢四年。1941年当选为印度支那共产党第一书记。1951年任[越南劳动党中央第一书记](../Page/越南共产党.md "wikilink")。

长征是越共亲华派人物，1956年因在[土改中的](../Page/土改.md "wikilink")[極左派政策](../Page/極左派.md "wikilink")，被以[黎笋为首的南方派](../Page/黎笋.md "wikilink")（以后发展为亲[苏派](../Page/苏联.md "wikilink")）免去第一书记的职务。1958年任政府副总理和国家科学委员会主任。1976年任国会常务委员会主席、新宪法起草委员会主席。1981年担任[国家主席](../Page/越南社会主义共和国主席.md "wikilink")（国家元首）。

1986年黎笋去世后出任[越共中央总书记](../Page/越共中央总书记.md "wikilink")，并且很快就开始改变黎笋的路线，出任总书记不足半年，就讓另一亲华派大将[阮文灵接替越共一把手地位](../Page/阮文灵.md "wikilink")，使越南逐渐摆脱[苏联控制](../Page/苏联.md "wikilink")，走上[革新开放之路](../Page/革新开放.md "wikilink")，同時改善因[1979年中越戰爭而中斷的](../Page/1979年中越戰爭.md "wikilink")[中越關係](../Page/中越關係.md "wikilink")。

长征是作家、诗人和理论家。著有《抗战一定胜利》、《论八月革命》、《论越南革命》等。

[Category:越南共产党中央委员会总书记](../Category/越南共产党中央委员会总书记.md "wikilink")
[Category:越南国家主席](../Category/越南国家主席.md "wikilink")
[Category:廣義省人](../Category/廣義省人.md "wikilink")
[\~](../Category/邓姓.md "wikilink")
[Category:越南國會主席](../Category/越南國會主席.md "wikilink")