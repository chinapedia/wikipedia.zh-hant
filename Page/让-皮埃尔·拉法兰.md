**让-皮埃尔·拉法兰**（[法语](../Page/法语.md "wikilink")：****，，），[法国](../Page/法国.md "wikilink")[保守派](../Page/保守派.md "wikilink")[政治家和](../Page/政治家.md "wikilink")[维埃纳省](../Page/维埃纳省.md "wikilink")[参议员](../Page/参议员.md "wikilink")。

拉法兰夫人安尼·玛丽·皮埃尔（Anne-Marie Perrier，生于1952年 in Chamalières)，育有一个女儿Fleur.

## 生涯

拉法兰生于[普瓦捷](../Page/普瓦捷.md "wikilink")（Poitiers）.
就读于[巴黎大学阿萨斯](../Page/巴黎大学.md "wikilink")(Assas)法学院和[巴黎高等商业学院](../Page/巴黎高等商业学院.md "wikilink")，学习法律，后毕业于[巴黎高等商业学院-欧洲管理学院](../Page/巴黎高等商业学院-欧洲管理学院.md "wikilink")（ESCP-EAP,École
Supérieure de Commerce de
Paris）。他当过[巴黎政治学院讲师](../Page/巴黎政治学院.md "wikilink")、贝尔纳·克里耶夫通信公司总经理。
1977年他当选普瓦捷市议员，1986年当选普瓦图-夏朗特大区议员，1988年至2002年任该大区议会主席。

### 国际政治

## 第一届拉法兰内阁

*2002年5月7日 - 2004年3月31日 (called Raffarin I until [17
June](../Page/17_June.md "wikilink"), and became Raffarin II)*

  - 让－皮埃尔·拉法兰 - 总理
  - [多米尼克·德维尔潘](../Page/多米尼克·德维尔潘.md "wikilink") - 外交、合作和法语国家部长
  - [米谢勒·阿利奥-马里](../Page/米谢勒·阿利奥-马里.md "wikilink") - 国防和退伍军人部长
  - [尼古拉·萨科齐](../Page/尼古拉·萨科齐.md "wikilink") - 内政、内部安全与地方自由部长Minister of
    the Interior, Interior Security, and Local Liberties
  - [弗朗西斯·梅尔](../Page/弗朗西斯·梅尔.md "wikilink") - 经济、财政和工业部长Minister of
    Economy, Finance, and Industry
  - [弗朗索瓦·菲永](../Page/弗朗索瓦·菲永.md "wikilink") - 劳动、社会团结部长Minister of
    Labour, Social Affairs, and Solidarity
  - [多米尼克·佩尔邦](../Page/多米尼克·佩尔邦.md "wikilink") - 司法部长Minister of Justice
  - [吕克·费里](../Page/吕克·费里.md "wikilink") - 国民教育、高等教育与科研部长Minister of
    National Education, Youth, Higher Education, and Research
  - [让-雅克·阿亚贡](../Page/让-雅克·阿亚贡.md "wikilink") - 文化和联络部长Minister of
    Culture and Communication
  - [埃尔韦·盖马尔](../Page/埃尔韦·盖马尔.md "wikilink") - 农业、食品和农村部长Minister of
    Agriculture, Food, and Rural Affairs
  - [罗塞利娜·巴舍洛-纳尔坎](../Page/罗塞利娜·巴舍洛-纳尔坎.md "wikilink") -
    生态及永续发展部长Minister of Ecology and Sustainable
    Development
  - [托基亚·萨菲](../Page/托基亚·萨菲.md "wikilink") - 可持续发展部长Minister Delegate of
    Sustainable Development
  - [让-弗朗索瓦·拉默尔](../Page/让-弗朗索瓦·拉默尔.md "wikilink") - 体育部长Minister of
    Sport
  - [布里吉特·吉拉尔丹](../Page/布里吉特·吉拉尔丹.md "wikilink") - 海外部长Minister of
    Overseas
  - [吉勒·德罗宾](../Page/吉勒·德罗宾.md "wikilink") - 交通、住房、旅游和海洋部长Minister of
    Transport, Housing, Tourism, Sea, and Equipment
  - [让-弗朗索瓦·马太](../Page/让-弗朗索瓦·马太.md "wikilink") - 卫生、家庭和残障人士部长Minister
    of Health, Family, and Handicapped People
  - [让-保罗·德勒瓦](../Page/让-保罗·德勒瓦.md "wikilink") -
    行政事务、国家改革和区域规划部长Minister of Civil
    Service, Reform of the State, and Regional Planning

### 期间更换

2002年6月17日

  - [米谢勒·阿利奥-马里](../Page/米谢勒·阿利奥-马里.md "wikilink") 停止任退伍军人部长，专任国防部长.
  - [多米尼克·德维尔潘](../Page/多米尼克·德维尔潘.md "wikilink") 停止任合作和法语国家部长，只任外交部长.
  - [雷诺·多纳迪约·德瓦布尔](../Page/雷诺·多纳迪约·德瓦布尔.md "wikilink") 停止任欧洲事务部长Minister
    of European affairs
    ，由[诺埃勒·勒努瓦](../Page/诺埃勒·勒努瓦.md "wikilink")(Noëlle
    Lenoir)代替.

## 第二届拉法兰内阁

*2004年3月31日 - 2004年11月29日 (called Raffarin III)*

  - 让－皮埃尔·拉法兰 - 总理
  - [米歇尔·巴尼耶](../Page/米歇尔·巴尼耶.md "wikilink") - 外交部长Minister of Foreign
    Affairs
  - [米谢勒·阿利奥-马里](../Page/米谢勒·阿利奥-马里.md "wikilink") - 国防部长Minister of
    Defense
  - [多米尼克·德维尔潘](../Page/多米尼克·德维尔潘.md "wikilink") -
    内政、内部安全与地方自由部长Minister of the Interior,
    Interior Security, and Local Liberties
  - [尼古拉·萨科齐](../Page/尼古拉·萨科齐.md "wikilink") - 经济、财政和工业部长Minister of
    Economy, Finance, and Industry
  - [让-路易·博洛](../Page/让-路易·博洛.md "wikilink") - 劳工兼社会凝聚力部长Minister of
    Labour, Employment, and Social Cohesion
  - [多米尼克·佩尔邦](../Page/多米尼克·佩尔邦.md "wikilink") - 司法部长Minister of Justice
  - [弗朗索瓦·菲永](../Page/弗朗索瓦·菲永.md "wikilink") - 国民教育、高等教育和科研部长Minister of
    National Education, Higher Education, and Research
  - [弗朗索瓦·多贝尔](../Page/弗朗索瓦·多贝尔.md "wikilink") - 研究部长Minister delegate
    of Research
  - [雷诺·多纳迪约·德瓦布尔](../Page/雷诺·多纳迪约·德瓦布尔.md "wikilink") - 文化和联络部长Minister
    of Culture and Communication
  - [埃尔韦·盖马尔](../Page/埃尔韦·盖马尔.md "wikilink") - 农业、食品、渔业和乡村部长Minister of
    Agriculture, Food, Fish, and Rural Affairs
  - [塞吉·Lepeltier](../Page/塞吉·Lepeltier.md "wikilink") -
    生态和可持续发展部长Minister of Ecology and Sustainable
    Development
  - [让-弗朗索瓦·拉默尔](../Page/让-弗朗索瓦·拉默尔.md "wikilink") -
    青年、体育和公共生活部长Minister of Youth, Sport, and
    Community Life
  - [布里吉特·吉拉尔丹](../Page/布里吉特·吉拉尔丹.md "wikilink") - 海外部长Minister of
    Overseas
  - [吉尔·德罗宾](../Page/吉尔·德罗宾.md "wikilink") - 交通、旅游、区域规划、海洋和装备部长Minister
    of Transport, Tourism, Regional Planning, Sea, and Equipment
  - [菲利普·杜斯特-布拉齐](../Page/菲利普·杜斯特-布拉齐.md "wikilink") - 卫生与社会保护部长Minister
    of Health and Social Protection
  - [玛丽-若赛·卢瓦尔](../Page/玛丽-若赛·卢瓦尔.md "wikilink") - 家庭和幼年部长Minister of
    Family and Childhood
  - [雷诺·迪特雷伊](../Page/雷诺·迪特雷伊.md "wikilink") - Minister of Civil Service
    and Reform of the State
  - [尼可尔·阿梅利娜](../Page/尼可尔·阿梅利娜.md "wikilink") - Minister of Parity and
    Professional Equality

### 期间更换

2004年11月29日 - [尼古拉·萨科齐](../Page/尼古拉·萨科齐.md "wikilink") left to be the
president of the
[UMP](../Page/Union_for_a_Popular_Movement.md "wikilink"). Thus there
was a reshuffle.

  - [埃尔韦·盖马尔](../Page/埃尔韦·盖马尔.md "wikilink") - 经济、财政和工业部长Minister of
    Economy, Finance, and Industry
    (代替[尼古拉·萨科齐](../Page/尼古拉·萨科齐.md "wikilink"))
  - [多米尼克·布斯洛](../Page/多米尼克·布斯洛.md "wikilink") - 农业、食品、渔业和乡村部长Minister
    of Agriculture, Food, Fish, and Rural Affairs
    (代替[埃尔韦·盖马尔](../Page/埃尔韦·盖马尔.md "wikilink"))

2005年2月25日 - 在丑闻压力下[埃尔韦·盖马尔辞职](../Page/埃尔韦·盖马尔.md "wikilink").

  - [蒂埃里·布雷东](../Page/蒂埃里·布雷东.md "wikilink") - 经济、财政和工业部长Minister of
    Economy, Finance, and Industry,

## 对外联系

  - [Official biography (in
    French)](https://web.archive.org/web/20050608005408/http://www.premier-ministre.gouv.fr/acteurs/premier_ministre/histoire_chefs_gouvernement_28/jean_pierre_raffarin_295/)
  - [BBC Profile (in
    English)](http://news.bbc.co.uk/1/hi/world/europe/1970512.stm)

## 参考文献

  -
## 参见

  - [法国总理](../Page/法国总理.md "wikilink")
  - [法国政治人物](../Page/法国政治人物.md "wikilink")

{{-}}

[R](../Category/法國總理.md "wikilink")
[R](../Category/法國政治人物.md "wikilink")
[Category:中国人民的老朋友](../Category/中国人民的老朋友.md "wikilink")
[R](../Category/巴黎第二大學校友.md "wikilink")