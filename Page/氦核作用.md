**氦核作用**
（或**α作用、α反應**）是兩種[核融合的類型之一](../Page/核融合.md "wikilink")，能將恆星的氦轉換成重元素，另一種即是[3氦過程](../Page/3氦過程.md "wikilink")（3α反應）。
當3氦反應進行時只需要氦的參與，而一旦有一些[碳產生](../Page/碳.md "wikilink")，能消耗氦的其他反應也會發生：

\[\mathrm{_6^{12}C} + \mathrm{_2^4He}  \rightarrow \mathrm{_{8}^{16}O} + \gamma + Q\],
Q = 7.16 MeV

\[\mathrm{_8^{16}O} + \mathrm{_2^4He}  \rightarrow \mathrm{_{10}^{20}Ne} + \gamma + Q\],
Q = 4.73 MeV

\[\mathrm{_{10}^{20}Ne} + \mathrm{_2^4He}  \rightarrow \mathrm{_{12}^{24}Mg} + \gamma + Q\],
Q = 9.31 MeV

\[\mathrm{_{12}^{24}Mg} + \mathrm{_2^4He}  \rightarrow \mathrm{_{14}^{28}Si} + \gamma + Q\],
Q = 9.98 MeV

\[\mathrm{_{14}^{28}Si} + \mathrm{_2^4He}  \rightarrow \mathrm{_{16}^{32}S} + \gamma + Q\],
Q = 6.95 MeV

\[\mathrm{_{16}^{32}S} + \mathrm{_2^4He}  \rightarrow \mathrm{_{18}^{36}Ar} + \gamma\]

\[\mathrm{_{18}^{36}Ar} + \mathrm{_2^4He}  \rightarrow \mathrm{_{20}^{40}Ca} + \gamma\]

\[\mathrm{_{20}^{40}Ca} + \mathrm{_2^4He}  \rightarrow \mathrm{_{22}^{44}Ti} + \gamma\]

\[\mathrm{_{22}^{44}Ti} + \mathrm{_2^4He}  \rightarrow \mathrm{_{24}^{48}Cr} + \gamma\]

\[\mathrm{_{24}^{48}Cr} + \mathrm{_2^4He}  \rightarrow \mathrm{_{26}^{52}Fe} + \gamma\]

\[\mathrm{_{26}^{52}Fe} + \mathrm{_2^4He}  \rightarrow \mathrm{_{28}^{56}Ni} + \gamma\]

反应的过程是：

氦-4 → 铍-8 → 碳-12 → 氧-16 → 氖-20 → 镁-24 → 矽–28 → 硫–32 → 氩–36 → 钙–40 → 钛–44
→ 铬–48 → 铁–52 → 镍–56

其中从氦-4开始到矽-28的反应过程叫氦聚变，从矽-28开始至镍-56的反应过程叫[矽燃烧过程](../Page/矽燃烧过程.md "wikilink")。所有這些反應在恆星內部發生的比率都不高，因此對於能量的貢獻並不大；比[氖](../Page/氖.md "wikilink")（[原子量](../Page/原子量.md "wikilink")
\> 10）重的元素，由於[庫侖障壁的增加](../Page/庫侖障壁.md "wikilink")，因此不太容易產生。

所謂的**α作用元素**
（或**α元素**）是質量為[氦核](../Page/氦核.md "wikilink")（α粒子）整數倍的同位素，它們的豐度是最高的。

α元素的[原子序数](../Page/原子序数.md "wikilink")≤
28：[He](../Page/氦.md "wikilink")、[Be](../Page/铍.md "wikilink")、[C](../Page/碳.md "wikilink")、[O](../Page/氧.md "wikilink")、[Ne](../Page/氖.md "wikilink")、[Mg](../Page/鎂.md "wikilink")、[Si](../Page/矽.md "wikilink")、[S](../Page/硫.md "wikilink")、[Ar](../Page/氬.md "wikilink")、[Ca](../Page/鈣.md "wikilink")、[Ti](../Page/鈦.md "wikilink")、[Cr](../Page/铬.md "wikilink")、[Fe](../Page/铁.md "wikilink")、[Ni](../Page/镍.md "wikilink")。它们在[II型超新星的](../Page/II型超新星.md "wikilink")[矽融合過程中經由α捕獲而形成](../Page/矽燃燒過程.md "wikilink")，镍-56是大质量恒星以核融合能产生的最后一种元素。

矽和鈣是純粹的α作用元素，鎂可以由氫核捕獲的燃燒過程中產生。至於氧，有些人認為是α作用元素，但也有人認為不是，在[金屬量低的](../Page/金屬量.md "wikilink")[第二星族星中](../Page/星族.md "wikilink")，氧確實是α作用元素；其他的在第二型超新星中產生的α作用元素，它們增加的質量都和氦的質量有很好的關聯性。有時候碳和氮也會被視為α作用元素，因為它們是經由α捕獲所形成的元素。

在恆星內的α作用元素[豐度通常都以對數的形式來表達](../Page/豐度.md "wikilink")：

\[[\alpha/Fe] = \log_{10}{\left(\frac{N_{\alpha}}{N_{Fe}}\right)_{Star}} - \log_{10}{\left(\frac{N_{\alpha}}{N_{Fe}}\right)_{Sun}}\],
此處\(N_{\alpha}\)和\(N_{Fe}\)分別是每單位體積內α作用元素和鐵原子的數量。理論的[星系演化模型預測在宇宙的早期](../Page/星系形成和演化.md "wikilink")，相對於鐵有更多的α作用元素。第二型超新星主要合成的元素是氧和α作用元素（氖、鎂、矽、硫、氬、鈣和鈦），而[Ia超新星在](../Page/Ia超新星.md "wikilink")[鐵峰頂產生元素](../Page/鐵峰頂.md "wikilink")（[V](../Page/釩.md "wikilink")、[Cr](../Page/鉻.md "wikilink")、[Mn](../Page/錳.md "wikilink")、[Fe](../Page/鐵.md "wikilink")、[Co和](../Page/鈷.md "wikilink")[Ni](../Page/鎳.md "wikilink")）。

## 外部連結

[The Age, Metallicity and Alpha-Element Abundance of Galactic Globular
Clusters from Single Stellar Population
Models](http://www.citebase.org/abstract?identifier=oai:arXiv.org:0705.4511)

[H](../Category/核合成.md "wikilink") [H](../Category/核聚变.md "wikilink")