**苏提·苏克索姆科特**（；）生于[泰国](../Page/泰国.md "wikilink")，泰国[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，效力于[新加坡职业足球联赛球队](../Page/新加坡职业足球联赛.md "wikilink")[淡滨尼流浪](../Page/淡滨尼流浪.md "wikilink")，[泰国国家足球队成员](../Page/泰国国家足球队.md "wikilink")，司职左边锋和前锋。

在俱乐部中素崇吉先后效力过泰国国内联赛强队[泰国农民银行](../Page/泰国农民银行足球俱乐部.md "wikilink")，曾短暫效力[英國的](../Page/英國.md "wikilink")[切尔西足球俱樂部](../Page/切尔西足球俱樂部.md "wikilink")，以及[丹戎巴葛联](../Page/丹戎巴葛联.md "wikilink")、[内政联和](../Page/内政联.md "wikilink")[淡滨尼流浪三支新加坡职业联赛球队](../Page/淡滨尼流浪.md "wikilink")。現在為[新加坡](../Page/新加坡.md "wikilink")[淡濱尼流浪隊踢球](../Page/淡濱尼流浪隊.md "wikilink")，球衣編號為17。

素崇吉最初是边路球员同时也可以胜任前锋，在[2004年亚洲杯足球赛中](../Page/2004年亚洲杯足球赛.md "wikilink")，他在对阵[中国对的比赛中攻入一球](../Page/中国国家足球队.md "wikilink")，这也是泰国在这届赛事中的唯一一个进球。同年，他获得了一份加盟韩国[K联赛](../Page/K联赛.md "wikilink")[釜山偶像的合约](../Page/釜山偶像.md "wikilink")，但在最后时刻双方未能正式签约。素崇吉的左脚能力十分了得，正是由于他的出色发挥，2003年，曾和內政聯的新加坡籍队友（Indra
Sahdan
Daud）一起赴[英格兰足球超级联赛豪门](../Page/英格兰足球超级联赛.md "wikilink")[切尔西受训](../Page/切尔西.md "wikilink")。

近年素崇吉多次代表[泰國國家隊出場](../Page/泰王國國家足球隊.md "wikilink")，球衣編號為17，在[2007年亚洲杯足球赛中素崇吉在揭幕战中射入一粒](../Page/2007年亚洲杯足球赛.md "wikilink")[-{zh-hans:点球;zh-hk:十二碼;zh-tw:點球;}-帮助球队](../Page/点球.md "wikilink")1-1战平了[伊拉克](../Page/伊拉克足球隊.md "wikilink")。

## 外部链接

  - [泰王國球星資訊](http://www.thaifootball.com/player.html)
  - [多哈亞運會足球運動員資訊](http://www.doha-2006.com/gis/SPORTS/CO/IGCOParticipantInfo.aspx?Register=5813970)
  - [泰王國國家隊球員資訊](http://www.national-football-teams.com/php/spieler.php?id=7024)

[Suksomkit](../Category/在世人物.md "wikilink")
[Suksomkit](../Category/新加坡联赛球员.md "wikilink")
[Suksomkit](../Category/泰国足球运动员.md "wikilink")