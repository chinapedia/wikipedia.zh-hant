**石友三**（）\[1\]，[字](../Page/表字.md "wikilink")**漢章**，[吉林将軍轄区](../Page/吉林省.md "wikilink")[副都統轄区長春厅九台](../Page/長春市.md "wikilink")（今[九台市](../Page/九台市.md "wikilink")）人，[國民革命軍陸軍中將](../Page/國民革命軍.md "wikilink")。[馮玉祥手下](../Page/馮玉祥.md "wikilink")「十三太保」之一。一生時常依附各大勢力，叛服无常，後遭到部下[高樹勋](../Page/高樹勋.md "wikilink")[兵变](../Page/兵变.md "wikilink")，[活埋而死](../Page/活埋.md "wikilink")。

## 生平

### 早年

石友三早年有志于学，苦讀[儒家](../Page/儒家.md "wikilink")[五經](../Page/五經.md "wikilink")、[四史](../Page/四史.md "wikilink")，修練[制藝](../Page/制藝.md "wikilink")。于1908年從軍，入[清朝](../Page/清朝.md "wikilink")[新军第三镇](../Page/陆军第三镇.md "wikilink")[吴佩孚部下](../Page/吴佩孚.md "wikilink")，驻[長春](../Page/長春.md "wikilink")。

### 国民军的活动

1912年（[民国元年](../Page/民国紀元.md "wikilink")），改投[馮玉祥手下](../Page/馮玉祥.md "wikilink")，先后在第16混成旅、第11師逐步升职。

1924年（民国13年）10月，[馮玉祥发动](../Page/馮玉祥.md "wikilink")[北京政变](../Page/北京政变.md "wikilink")，成立[国民军](../Page/国民军.md "wikilink")，石友三任第1軍第8混成旅旅長。民国14年（1925年）春，升任第6師師長。1926年（民国15年），率部在[南口](../Page/昌平区.md "wikilink")[作战](../Page/南口战斗_\(北洋政府时期\).md "wikilink")，[山西省的](../Page/山西省_\(中華民国\).md "wikilink")[閻錫山袭击国民軍背后](../Page/閻錫山.md "wikilink")，石友三和[韓復榘迎擊](../Page/韓復榘.md "wikilink")，作战十分勇猛。然而，由于国民軍处于劣勢，同年8月放弃南口。[張之江率本队撤往](../Page/張之江.md "wikilink")[綏遠省](../Page/綏遠省.md "wikilink")，石友三和[韓復榘投降](../Page/韓復榘.md "wikilink")[閻錫山](../Page/閻錫山.md "wikilink")。

同年9月，馮玉祥归国举行[五原誓師](../Page/五原誓師.md "wikilink")，石友三和[韓復榘复归馮玉祥部](../Page/韓復榘.md "wikilink")，石友三被任命为[国民聯軍援陝第](../Page/国民聯軍.md "wikilink")5路总指揮。翌年6月，国民聯軍改组为[国民革命軍第二集团軍](../Page/国民革命軍第二集团軍.md "wikilink")，石友三任第1方面軍副总指揮兼第5軍軍長。石友三参加了[中国国民党的](../Page/中国国民党.md "wikilink")[北伐](../Page/北伐.md "wikilink")，在[山東省击败了北京政府方面的](../Page/山東省_\(中華民国\).md "wikilink")[孫传芳軍](../Page/孫传芳.md "wikilink")，立下軍功。北伐结束後的1928年（民国17年），伴随着裁軍，石友三改任第24師師長，驻扎[河南省](../Page/河南省_\(中華民国\).md "wikilink")[南陽市](../Page/南陽市.md "wikilink")。

### 叛服无常

[Shi_Yousan.jpg](https://zh.wikipedia.org/wiki/File:Shi_Yousan.jpg "fig:Shi_Yousan.jpg")》，1941年）|180px\]\]
1929年（民国18年），馮玉祥与[蒋介石的矛盾激化](../Page/蒋介石.md "wikilink")，冲突不可避免。5月22日，石友三和韓復榘放弃追随馮玉祥，改投蒋介石。这也是蒋介石方面金錢攏絡的效果。10月，石友三被任命为[安徽省政府主席](../Page/安徽省_\(中華民国\).md "wikilink")。

此後，[湖南省的](../Page/湖南省_\(中華民国\).md "wikilink")[唐生智暗中联络石友三叛离蒋介石](../Page/唐生智.md "wikilink")。12月，石友三突然叛离蒋介石，炮轰[南京](../Page/南京市.md "wikilink")。这出乎蒋介石意料之外，一時南京方面发生大混乱。随后，石友三发觉无法对抗蒋介石，乃逃走，赴[河南省仰赖韓復榘](../Page/河南省.md "wikilink")。

翌年，[中原大战爆发](../Page/中原大战.md "wikilink")，石友三再度投向过去的上司[馮玉祥](../Page/馮玉祥.md "wikilink")、[閻錫山](../Page/閻錫山.md "wikilink")。石友三被任命为第4方面軍总司令，同蔣的中央軍作战。馮玉祥、閻錫山敗局已定后，石友三经[張学良](../Page/張学良.md "wikilink")、韓復榘斡旋，還是[歸順](../Page/歸順.md "wikilink")[蒋介石](../Page/蒋介石.md "wikilink")，被任命为第13路总指揮。

1931年（民国20年），反蒋派在[广州成立](../Page/广州市.md "wikilink")[国民政府](../Page/广州国民政府_\(1931年_-_1932年\).md "wikilink")，石友三参加，出任广州国民政府第5集团軍总司令。結果同年8月，遭到張学良敗北，石友三逃往山東省再度仰赖韓復榘。1932年（民国21年），石友三到華北[宋哲元手下活動](../Page/宋哲元.md "wikilink")。另外从此时开始，石友三同[土肥原賢二等日本方面人士进行交涉](../Page/土肥原賢二.md "wikilink")。

### 抗日投日

1937年（民国26年）[抗日战争爆发初期](../Page/抗日战争.md "wikilink")，石友三从事抗战。石友三为保存实力，遂转而与[中国共产党合作](../Page/中国共产党.md "wikilink")。1938年2月，出任第69軍軍長。同年5月，任第10軍团軍团長。12月，任冀察战区副司令兼[察哈爾省政府主席](../Page/察哈爾省.md "wikilink")。

此后，石友三的反共姿态鮮明，对[八路軍不断进攻](../Page/八路軍.md "wikilink")。1940年（民国29年）2月，石友三遭到八路軍反擊，逃往山東省[曹县](../Page/曹县.md "wikilink")。其後，日本方面诱以[河北省省長等职位](../Page/河北省_\(汪精卫政权\).md "wikilink")，石友三乃策划投降日本。

1940年12月1日，遵照蒋介石的秘令并经过军统[陳恭澍的组织](../Page/陳恭澍.md "wikilink")\[2\]，石友三部下的新8軍軍長[高樹勋在](../Page/高樹勋.md "wikilink")[濮陽对石友三发动兵变](../Page/濮陽.md "wikilink")，逮捕了石友三。当天即将石友三处决，[活埋而死](../Page/活埋.md "wikilink")。石友三时年50岁。

### 逸事

石一生中曾先后投靠[馮玉祥](../Page/馮玉祥.md "wikilink")、[閻錫山](../Page/閻錫山.md "wikilink")、[蒋介石](../Page/蒋介石.md "wikilink")、[汪精卫](../Page/汪精卫.md "wikilink")、[张学良等政治人物](../Page/张学良.md "wikilink")，甚至歸順[日軍](../Page/日軍.md "wikilink")，而又先后背叛之。石友三由于叛服无常，故有「倒戈将軍」的称号。又因喜欢采用[活埋等残酷的刑罚](../Page/活埋.md "wikilink")（自己也被活埋），故人称「石閻王」。

## 历史影响

###### 火烧少林寺

1928年春，亲[孙文势力](../Page/孙文.md "wikilink")[樊钟秀夺得](../Page/樊钟秀.md "wikilink")[巩县](../Page/巩县.md "wikilink")、[偃师](../Page/偃师.md "wikilink")，进围[登封](../Page/登封.md "wikilink")，以[少林寺为司令部](../Page/少林寺.md "wikilink")。[冯玉祥部下石友三于](../Page/冯玉祥.md "wikilink")3月15日纵火焚烧法堂，次日，又令军士焚烧天王殿、大雄宝殿、钟楼、鼓楼、六祖堂、龙王殿、紧那罗殿、阎王殿、库房、禅堂\[3\]，200多僧众几乎被杀尽\[4\]，一大批珍贵文物及5480卷藏经化为灰烬。

## 注释

## 参考文献

  - <span style="font-size:90%;">沈慶生「石友三」</span>
  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>

| （[国民政府](../Page/国民政府.md "wikilink")） |
| ------------------------------------ |

[Category:陆军第三镇将领](../Category/陆军第三镇将领.md "wikilink")
[Category:直军将领](../Category/直军将领.md "wikilink")
[Category:西北军将领](../Category/西北军将领.md "wikilink")
[Category:国民革命军将领](../Category/国民革命军将领.md "wikilink")
[Category:中華民國中將](../Category/中華民國中將.md "wikilink")
[Category:中華民國陸軍將領](../Category/中華民國陸軍將領.md "wikilink")
[Category:與大日本帝國合作的中國人](../Category/與大日本帝國合作的中國人.md "wikilink")
[Category:中原大戰人物](../Category/中原大戰人物.md "wikilink")
[Category:北伐人物](../Category/北伐人物.md "wikilink")
[Category:中华民国大陆时期遇刺身亡者](../Category/中华民国大陆时期遇刺身亡者.md "wikilink")
[Category:九台人](../Category/九台人.md "wikilink")
[You友三](../Category/石姓.md "wikilink")

1.  徐友春主編『民国人物大辞典 增訂版』256頁作生于1891年。Who's Who in China 3rd
    ed.,p.208作生于1892年。
2.
3.
4.