**劉婴**（），[西汉末代皇太子](../Page/西汉.md "wikilink")（6年4月17日—9年1月10日為皇太子，未正式即位），号**孺子**，是[汉宣帝的玄孙](../Page/汉宣帝.md "wikilink")、楚孝王[劉囂曾孙](../Page/劉囂.md "wikilink")、广戚煬侯[劉勳孫](../Page/劉勳_\(廣戚侯\).md "wikilink")、广戚侯[刘显子](../Page/劉顯_\(廣戚侯\).md "wikilink")。

## 生平

[元始五年](../Page/元始_\(西汉\).md "wikilink")（5年），[汉朝外戚](../Page/汉朝.md "wikilink")[王莽毒死](../Page/王莽.md "wikilink")[汉平帝](../Page/汉平帝.md "wikilink")，次年从汉朝宗室中挑选时年2岁的刘婴。但是因為年齡太小，未正式即位，僅為「皇太子」。王莽自称“摄皇帝”，任何排場實與皇帝無異，僅在見孺子及太后時需自稱臣。為了實質控制朝政大权，完全摄政，以[周公](../Page/周公旦.md "wikilink")、[伊尹自居](../Page/伊尹.md "wikilink")，改元“[居摄](../Page/居摄.md "wikilink")”。而刘婴這個皇太子則其實只是傀儡。

[初始元年十一月甲子](../Page/初始.md "wikilink")（9年1月6日），王莽將「攝皇帝」稱號改稱「假皇帝」；十一月戊辰（1月10日），王莽自称汉太祖[刘邦要他做皇帝](../Page/刘邦.md "wikilink")，便称帝，建国号“[新](../Page/新朝.md "wikilink")”，尊太皇太后[王政君为皇太后](../Page/王政君.md "wikilink")，刘婴为**定安公**。至此，立国二百一十一年的西汉王朝结束。

王莽将年幼的刘婴养在高墙府第之中，与外界隔绝任何联系，甚至乳母也不被允许和他讲话，导致刘婴成人后不识六畜，知识面与幼儿无异。王莽将自己的孙女、[王宇的女儿](../Page/王宇_\(汉朝\).md "wikilink")[王氏嫁给他做妻子](../Page/王夫人_\(孺子婴\).md "wikilink")。

更始二年（24年），王莽为更始帝[刘玄所败](../Page/更始帝.md "wikilink")，刘婴当时身在长安。[平陵人](../Page/平陵县_\(陕西\).md "wikilink")[方望等人依据天象认为更始帝必败](../Page/方望.md "wikilink")，而刘婴才是天子正统，便起兵将刘婴迎至[临泾](../Page/临泾镇.md "wikilink")，拥立为天子。更始三年（25年），更始帝派遣丞相[李松進攻临泾](../Page/李松_\(漢朝\).md "wikilink")，刘婴被杀\[1\]。

## 家世

<center>

</center>

## 参考文献

|-   |-

[Category:西漢皇帝](../Category/西漢皇帝.md "wikilink")
[Category:中国末代皇帝](../Category/中国末代皇帝.md "wikilink")
[Category:中國被殺帝王](../Category/中國被殺帝王.md "wikilink")
[Category:刘姓](../Category/刘姓.md "wikilink")

1.  《[汉书·卷八十 宣元六王传](../Page/s:汉书/卷080.md "wikilink")》