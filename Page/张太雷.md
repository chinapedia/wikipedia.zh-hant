[thumb](../Page/文件:Zhang_Tailei.jpg.md "wikilink")
**张太雷**（），原名**张曾让**，后曾改名**张椿年**，[江苏](../Page/江苏.md "wikilink")[常州人](../Page/常州.md "wikilink")，[中国共产党早期领导人之一](../Page/中国共产党.md "wikilink")，[共产国际代表](../Page/共产国际.md "wikilink")[鲍罗廷的翻译](../Page/鲍罗廷.md "wikilink")，[廣州暴動的主要策划者和领导人](../Page/廣州暴動.md "wikilink")，在廣州暴動中被杀。

## 早年生平

自小家境貧寒，8歲丧父，靠母親帮佣的微薄收入为生。早年就讀於[常州府中學堂](../Page/常州府.md "wikilink")，1915年考入天津[北洋大学法科](../Page/北洋大学.md "wikilink")\[1\]。1919年积极参加五四运动，发起组织「社会建设会」，后被大学开除。

## 參與籌備中國共產黨，出席第一次全國代表大會

1920年8月赴[北京](../Page/北京.md "wikilink")，参加[李大钊](../Page/李大钊.md "wikilink")、[张申府组织的共产主义小组](../Page/张申府.md "wikilink")。1920年10月，张太雷和[鄧中夏等一起加入李大钊发起成立的北京的中国共产党早期组织](../Page/鄧中夏.md "wikilink")，成为中国共产党最早的党员之一。此後，他與鄧中夏開展工人運動，到[長辛店组建](../Page/長辛店.md "wikilink")[勞動補習學校](../Page/勞動補習學校舊址.md "wikilink")，培养了中國北方鐵路工人運動的第一批幹部\[2\]。

之後返回[天津](../Page/天津.md "wikilink")，组织[天津社会主义青年团](../Page/天津社会主义青年团.md "wikilink")。1921年春被派往苏俄[伊尔库茨克](../Page/伊尔库茨克.md "wikilink")，任[共产国际东方局中国科书记](../Page/共产国际.md "wikilink")。6月陪同共产国际代表[马林和](../Page/马林.md "wikilink")[赤色职工国际代表](../Page/赤色职工国际.md "wikilink")[尼克爾斯基来中国](../Page/尼克爾斯基.md "wikilink")，筹备召开[中共一大](../Page/中共一大.md "wikilink")。6月23日，再度被派往俄国，代表中共出席莫斯科共产国际第三次代表大会。并介绍中学老同学、《晨报》驻莫斯科通讯员、东方大学俄文翻译[瞿秋白](../Page/瞿秋白.md "wikilink")，加入中共。1922年春回国。

1924年任[中国社会主义青年团中央书记](../Page/中国社会主义青年团.md "wikilink")；1925年春调至广州工作；[中共四大](../Page/中共四大.md "wikilink")、[五大当选为中央委员](../Page/中共五大.md "wikilink")，任湖北省委书记；1927年在[八七会议上同](../Page/八七会议.md "wikilink")[陈独秀路线进行斗争](../Page/陈独秀.md "wikilink")，被选为中央临时政治局候补委员，任广东省委书记。

## 發動廣州暴動與犧牲

1927年12月11日领导廣州暴動，第二天下午两点多，张太雷与共产国际代表乘坐一辆敞篷汽车，在大北门附近遭遇「廣州機器工會」反共份子襲擊\[3\]\[4\]，身中三弹，倒在车内身亡。

## 家庭

夫人[王一知](../Page/王一知.md "wikilink")（1901年-1991年）原为[施存统的夫人](../Page/施存统.md "wikilink")，后与施离婚，改嫁张太雷。

## 参考文献

[Z张](../Category/中国共产党第四届中央执行委员会候补委员.md "wikilink")
[Z张](../Category/中国共产党第五届中央委员会委员.md "wikilink")
[Category:中华民国大陆时期被杀害人物](../Category/中华民国大陆时期被杀害人物.md "wikilink")
[Category:共产国际人物](../Category/共产国际人物.md "wikilink")
[Z张](../Category/常州人.md "wikilink")
[T](../Category/張姓.md "wikilink")
[Category:北洋大學校友](../Category/北洋大學校友.md "wikilink")
[Category:中共四大代表](../Category/中共四大代表.md "wikilink")
[Category:中共五大代表](../Category/中共五大代表.md "wikilink")

1.  [张太雷——
    真正的国际主义者--党史频道--人民网](http://dangshi.people.com.cn/GB/144964/145594/8827596.html)
2.  [张太雷：愿化作震碎旧世界惊雷-新华网](http://www.xinhuanet.com/politics/2018-05/12/c_1122822565.htm)
3.  [二、羊城上空响起一声惊雷
    (3)--党史频道-人民网](http://dangshi.people.com.cn/GB/17268361.html)
4.  [血雨腥风之广东机器工会 - 广州党史 - Powered by
    JEECMS](http://www.zggzds.org.cn/dsyjyjwz/111.jhtml)