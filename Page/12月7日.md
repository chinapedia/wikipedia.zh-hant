**12月7日**是[公历一年中的第](../Page/公历.md "wikilink")341天（[闰年第](../Page/闰年.md "wikilink")342天），离全年结束还有24天。

## 大事记

### 前1世紀

  - [前43年](../Page/前43年.md "wikilink")：[古罗马政治家](../Page/古罗马.md "wikilink")、哲学家[西塞罗因坚持](../Page/西塞罗.md "wikilink")[共和制](../Page/共和制.md "wikilink")，发表反[马克·安东尼的演说而遭杀死](../Page/马克·安东尼.md "wikilink")。

### 3世紀

  - [208年](../Page/208年.md "wikilink")：[黃蓋在](../Page/黃蓋.md "wikilink")[乌林以火攻突襲](../Page/乌林.md "wikilink")[曹操水军](../Page/曹操.md "wikilink")，为[孙权](../Page/孙权.md "wikilink")、[刘备联盟军队在](../Page/刘备.md "wikilink")[赤壁之戰取胜](../Page/赤壁之戰.md "wikilink")。

### 18世紀

  - [1732年](../Page/1732年.md "wikilink")：[英国皇家大剧院](../Page/皇家歌劇院.md "wikilink")（Royal
    Opera House）在[伦敦开幕](../Page/伦敦.md "wikilink")。

### 19世紀

  - [1815年](../Page/1815年.md "wikilink")：[法国元帅](../Page/法国元帅.md "wikilink")[内伊因支持](../Page/米歇尔·内伊.md "wikilink")[拿破仑而被](../Page/拿破仑·波拿巴.md "wikilink")[波旁王朝在](../Page/波旁王朝.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[卢森堡公园附近以](../Page/卢森堡公园.md "wikilink")[叛国罪](../Page/叛国罪.md "wikilink")[枪决](../Page/枪决.md "wikilink")。
  - [1889年](../Page/1889年.md "wikilink")：第一个充气[轮胎受](../Page/轮胎.md "wikilink")[专利保护](../Page/专利.md "wikilink")。

### 20世紀

  - [1926年](../Page/1926年.md "wikilink")：第一台[电冰箱受美国专利保护](../Page/电冰箱.md "wikilink")。
  - [1941年](../Page/1941年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：[大日本帝國](../Page/大日本帝國.md "wikilink")[联合舰队偷袭](../Page/联合舰队.md "wikilink")[美国太平洋舰队位於](../Page/美国太平洋舰队.md "wikilink")[夏威夷的基地](../Page/夏威夷.md "wikilink")[珍珠港](../Page/珍珠港.md "wikilink")，致使太平洋舰队重创，[珍珠港事件发生](../Page/珍珠港事件.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[第二次国共内战](../Page/第二次国共内战.md "wikilink")：[中华民国政府从](../Page/中华民国政府.md "wikilink")[成都迁往](../Page/成都.md "wikilink")[臺北](../Page/臺北市.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[英国首相](../Page/英国首相.md "wikilink")[艾德禮退出政壇](../Page/克莱门特·艾德礼.md "wikilink")。
  - [1970年](../Page/1970年.md "wikilink")：[联邦德国总理](../Page/西德總理.md "wikilink")[維利·勃蘭特在](../Page/維利·勃蘭特.md "wikilink")[华沙犹太人殉难者纪念碑前](../Page/华沙犹太人殉难者纪念碑.md "wikilink")[下跪](../Page/华沙之跪.md "wikilink")，对[納粹德國在二战期间的暴行表示忏悔](../Page/納粹德國.md "wikilink")。
  - [1972年](../Page/1972年.md "wikilink")：[美国](../Page/美国.md "wikilink")[阿波罗17号](../Page/阿波罗17号.md "wikilink")[宇宙飞船發射](../Page/宇宙飞船.md "wikilink")，為[阿波羅計劃中最後一次登月任務](../Page/阿波羅計劃.md "wikilink")。本日在飞向[月球的过程中](../Page/月球.md "wikilink")，其[宇航员拍下](../Page/宇航员.md "wikilink")[蓝色弹珠](../Page/蓝色弹珠.md "wikilink")[地球照片](../Page/地球.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[印度尼西亚侵略](../Page/印度尼西亚.md "wikilink")[东帝汶](../Page/东帝汶.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[以色列开凿](../Page/以色列.md "wikilink")[地中海死海运河计划遭](../Page/地中海死海运河计划.md "wikilink")[联大反对](../Page/联大.md "wikilink")。
  - [1983年](../Page/1983年.md "wikilink")：伦敦著名的[哈洛德百货公司](../Page/哈洛德百货公司.md "wikilink")（Harrods）遭到恐怖分子炸弹袭击，造成5人死亡，91人受伤，[爱尔兰共和军宣布对事件负责](../Page/爱尔兰共和军.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：[前蘇聯](../Page/苏联.md "wikilink")[亞美尼亞北部发生](../Page/亞美尼亞.md "wikilink")[强烈地震](../Page/亚美尼亚大地震.md "wikilink")，2万5千人死亡。
  - [1991年](../Page/1991年.md "wikilink")：美国研制出[全数字电视系统](../Page/全数字电视系统.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：由于连夜暴风雨，[玻利維亞的](../Page/玻利維亞.md "wikilink")[伊皮山发生了山体滑坡](../Page/伊皮山.md "wikilink")，造成上千人死亡。
  - [1995年](../Page/1995年.md "wikilink")：[美国](../Page/美国.md "wikilink")[伽利略号](../Page/伽利略号.md "wikilink")[太空探测器在由](../Page/太空探测器.md "wikilink")[阿特兰蒂斯号](../Page/阿特兰蒂斯号航天飞机.md "wikilink")[航天飞机送上太空六年后](../Page/航天飞机.md "wikilink")，进入绕[木星飞行的](../Page/木星.md "wikilink")[轨道](../Page/轨道.md "wikilink")。
  - [1996年](../Page/1996年.md "wikilink")：[歐洲核子研究組織的科研人员发现](../Page/歐洲核子研究組織.md "wikilink")[正负K介子在转换过程中存在](../Page/K介子.md "wikilink")[时间上的不对称性](../Page/时间.md "wikilink")。
  - [1997年](../Page/1997年.md "wikilink")：纪念[南京大屠杀的](../Page/南京大屠殺.md "wikilink")“[为和平而歌](../Page/为和平而歌.md "wikilink")”音乐会在美国纽约[卡内基音乐厅举行](../Page/卡内基大厅.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：[美国唱片业协会指控以](../Page/美国唱片业协会.md "wikilink")[点对点技术进行档案分享服务的](../Page/点对点技术.md "wikilink")[Napster严重侵犯版权并且提出控诉](../Page/Napster.md "wikilink")。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：[塔利班向](../Page/塔利班.md "wikilink")[坎大哈新政权](../Page/坎大哈.md "wikilink")[缴械](../Page/缴械.md "wikilink")。
  - [2002年](../Page/2002年.md "wikilink")：[伊拉克否认拥有](../Page/伊拉克.md "wikilink")[大规模杀伤性武器](../Page/大规模杀伤性武器.md "wikilink")，总统[萨达姆向](../Page/萨达姆·侯赛因.md "wikilink")[科威特人民道歉](../Page/科威特.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")：韩国河北精神号[油轮与](../Page/油轮.md "wikilink")[驳船碰撞](../Page/驳船.md "wikilink")，引起[河北精神号漏油事故](../Page/河北精神号漏油事故.md "wikilink")，成为韩国史上最严重的漏油事件。
  - 2007年：響應當時[陳水扁政府](../Page/陳水扁政府.md "wikilink")[去蔣化之](../Page/去蔣化.md "wikilink")[轉型正義政策](../Page/轉型正義.md "wikilink")，[中正紀念堂](../Page/中正紀念堂.md "wikilink")[大中至正之牌匾由](../Page/大中至正.md "wikilink")[中華民國教育部強制拆除](../Page/中華民國教育部.md "wikilink")。

## 出生

  - [521年](../Page/521年.md "wikilink")：[聖高隆](../Page/聖高隆.md "wikilink")，[愛爾蘭僧侶](../Page/愛爾蘭.md "wikilink")，將天主教傳入蘇格蘭及愛爾蘭的先驅（逝於[597年](../Page/597年.md "wikilink")）
  - [903年](../Page/903年.md "wikilink")：[阿卜杜勒－拉赫曼·苏菲](../Page/阿卜杜勒－拉赫曼·苏菲.md "wikilink")，[波斯天文學家](../Page/波斯.md "wikilink")（逝於[986年](../Page/986年.md "wikilink")）
  - [1561年](../Page/1561年.md "wikilink")：[吉川廣家](../Page/吉川廣家.md "wikilink")，[日本戰國時代武將](../Page/日本.md "wikilink")，初代岩國藩當主（逝於[1625年](../Page/1625年.md "wikilink")）
  - [1598年](../Page/1598年.md "wikilink")：[济安·贝尼尼](../Page/济安·贝尼尼.md "wikilink")，[義大利巴洛克藝術家](../Page/義大利.md "wikilink")（逝於[1680年](../Page/1680年.md "wikilink")）
  - [1764年](../Page/1764年.md "wikilink")：[克勞德·皮爾林·維克托](../Page/克勞德·皮爾林·維克托.md "wikilink")，[法國元帥](../Page/法國元帥.md "wikilink")（逝於[1841年](../Page/1841年.md "wikilink")）
  - [1810年](../Page/1810年.md "wikilink")：[泰奥多尔·施旺](../Page/泰奥多尔·施旺.md "wikilink")，[德國動物學](../Page/德國.md "wikilink")、細胞學家（逝於[1882年](../Page/1882年.md "wikilink")）
  - [1823年](../Page/1823年.md "wikilink")：[利奥波德·克罗内克](../Page/利奥波德·克罗内克.md "wikilink")，德國數學與邏輯學家（逝於[1891年](../Page/1891年.md "wikilink")）
  - [1847年](../Page/1847年.md "wikilink")：[璧利南](../Page/璧利南.md "wikilink")，英國外交官（逝於[1927年](../Page/1927年.md "wikilink")）
  - [1860年](../Page/1860年.md "wikilink")：[約瑟夫·庫克](../Page/約瑟夫·庫克.md "wikilink")，[澳洲第](../Page/澳洲.md "wikilink")6任總理（逝於[1947年](../Page/1947年.md "wikilink")）
  - [1863年](../Page/1863年.md "wikilink")：[皮埃特罗·马斯卡尼](../Page/皮埃特罗·马斯卡尼.md "wikilink")，義大利作曲家（逝於[1945年](../Page/1945年.md "wikilink")）
  - [1872年](../Page/1872年.md "wikilink")：[约翰·赫伊津哈](../Page/约翰·赫伊津哈.md "wikilink")，[荷兰語言學家](../Page/荷兰.md "wikilink")、[历史学家](../Page/历史学家.md "wikilink")（逝於[1945年](../Page/1945年.md "wikilink")）
  - [1873年](../Page/1873年.md "wikilink")：[薇拉·凯瑟](../Page/薇拉·凯瑟.md "wikilink")，美國作家，1923年獲普立茲獎（逝於[1947年](../Page/1947年.md "wikilink")）
  - [1887年](../Page/1887年.md "wikilink")：[恩斯特·托赫](../Page/恩斯特·托赫.md "wikilink")，[奧地利裔美國作曲家](../Page/奧地利.md "wikilink")，發明「語音音樂」技術（逝於[1964年](../Page/1964年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[郁達夫](../Page/郁達夫.md "wikilink")，[中国作家](../Page/中国.md "wikilink")、小說家（逝於[1945年](../Page/1945年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[杰拉德·柯伊伯](../Page/杰拉德·柯伊伯.md "wikilink")，荷裔美籍[天文学家](../Page/天文学家.md "wikilink")（逝於[1973年](../Page/1973年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[袁珂](../Page/袁珂.md "wikilink")，中國[神話學家](../Page/神話學.md "wikilink")（逝於[2001年](../Page/2001年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[沃爾特·諾沃特尼](../Page/沃爾特·諾沃特尼.md "wikilink")，二次大戰德國空軍第5號王牌飛行員，442次任務擊落258架敵機（逝於[1944年](../Page/1944年.md "wikilink")）
  - [1924年](../Page/1924年.md "wikilink")：[马里奥·苏亚雷斯](../Page/马里奥·苏亚雷斯.md "wikilink")，第17任[葡萄牙總統](../Page/葡萄牙總統.md "wikilink")（逝於[2017年](../Page/2017年.md "wikilink")）
  - [1925年](../Page/1925年.md "wikilink")：[許瑜](../Page/許瑜.md "wikilink")，前香港[教育署署長](../Page/教育局_\(香港\).md "wikilink")。（逝於[2015年](../Page/2015年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[诺姆·乔姆斯基](../Page/诺姆·乔姆斯基.md "wikilink")，美國[語言學家](../Page/語言學.md "wikilink")
  - [1931年](../Page/1931年.md "wikilink")：[艾倫·鮑絲汀](../Page/艾倫·鮑絲汀.md "wikilink")，美國電影、舞台劇和電視劇演員
  - [1949年](../Page/1949年.md "wikilink")：[汤姆·威茨](../Page/汤姆·威茨.md "wikilink")，美國音樂人、演員
  - [1956年](../Page/1956年.md "wikilink")：[拉里·伯德](../Page/拉里·伯德.md "wikilink")，美國[NBA球員](../Page/NBA.md "wikilink")、教練
  - [1957年](../Page/1957年.md "wikilink")：[森博嗣](../Page/森博嗣.md "wikilink")，日本[推理小說家](../Page/推理小說.md "wikilink")，代表作《[全部成為F](../Page/全部成為F.md "wikilink")》
  - [1961年](../Page/1961年.md "wikilink")：[林夕](../Page/林夕.md "wikilink")，[香港](../Page/香港.md "wikilink")[填詞人](../Page/填詞人.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[C·托马斯·豪威尔](../Page/C·托马斯·豪威尔.md "wikilink")，美國演員和電影監製
  - [1973年](../Page/1973年.md "wikilink")：[戴米恩·莱斯](../Page/戴米恩·莱斯.md "wikilink")，愛爾蘭創作歌手、音樂家
  - [1977年](../Page/1977年.md "wikilink")：[艾瑞克·夏維茲](../Page/艾瑞克·夏維茲.md "wikilink")，美國職棒選手
  - [1978年](../Page/1978年.md "wikilink")：[蔡妍](../Page/蔡妍.md "wikilink")，[韓國歌手](../Page/韓國.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[莎拉·芭瑞黎絲](../Page/莎拉·芭瑞黎絲.md "wikilink")，美國創作歌手及鋼琴家
  - [1980年](../Page/1980年.md "wikilink")：[-{zh-hans:约翰·特里;zh-hk:約翰·泰利;zh-tw:約翰·泰瑞;}-](../Page/約翰·泰利.md "wikilink")，[英格兰足球運動員](../Page/英格兰.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[文倉十](../Page/文倉十.md "wikilink")，[日本插畫家](../Page/日本.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[陳國峰](../Page/陳國峰.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[佛斯托·卡蒙納](../Page/佛斯托·卡蒙納.md "wikilink")，美國職棒選手
  - [1984年](../Page/1984年.md "wikilink")：[-{zh-hans:罗伯特·库比卡;zh-hk:羅伯特·古碧沙;zh-tw:羅伯特·庫畢薩;}-](../Page/罗伯特·库比查.md "wikilink")，[波兰史上首位](../Page/波兰.md "wikilink")[一级方程式赛车](../Page/一级方程式赛车.md "wikilink")
  - 1984年：[亚伦·格雷](../Page/亚伦·格雷.md "wikilink")，美國職業籃球運動員
  - [1984年](../Page/1984年.md "wikilink")：[方宥心](../Page/方宥心.md "wikilink")，台灣演員
  - [1987年](../Page/1987年.md "wikilink")：[張名雅](../Page/張名雅.md "wikilink")，香港模特兒、2012年度香港小姐競選冠軍
  - 1987年：[亞倫·卡特](../Page/亞倫·卡特.md "wikilink")，美國歌手、演員
  - [1988年](../Page/1988年.md "wikilink")：[-{zh-cn:艾米莉; zh-tw:艾蜜莉;
    zh-hk:艾美莉;}-·布朗寧](../Page/艾米莉·布朗寧.md "wikilink")，[澳洲女演員及模特兒](../Page/澳洲.md "wikilink")
  - 1988年：[内森·阿德里安](../Page/内森·阿德里安.md "wikilink")，美國游泳運動員
  - [1989年](../Page/1989年.md "wikilink")：[尼古拉斯·霍尔特](../Page/尼古拉斯·霍尔特.md "wikilink")，英國電影和電視劇演員
  - 1989年：[林筠翔](../Page/林筠翔.md "wikilink")，香港配音員
  - [1990年](../Page/1990年.md "wikilink")：[蔣夢婕](../Page/蔣夢婕.md "wikilink")，[中國](../Page/中國.md "wikilink")[女演員](../Page/女演員.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[櫻田通](../Page/櫻田通.md "wikilink")，日本男演員
  - [1994年](../Page/1994年.md "wikilink")：[羽生結弦](../Page/羽生結弦.md "wikilink")，日本男子花式滑冰選手
  - [1995年](../Page/1995年.md "wikilink")：[Sowon](../Page/Sowon.md "wikilink")，[南韓女子團體](../Page/南韓.md "wikilink")[GFRIEND隊長](../Page/GFRIEND.md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[安藤聖峻](../Page/安藤聖峻.md "wikilink")，[日本演員](../Page/日本.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")：
    [余昊台灣](../Page/余昊.md "wikilink") 台東 最強幹話王
  - [2003年](../Page/2003年.md "wikilink")：[凱薩琳娜-艾瑪莉亞](../Page/凱薩琳娜-艾瑪莉亞_\(荷蘭\).md "wikilink")，[荷蘭公主](../Page/荷蘭.md "wikilink")
  - [葉怡彤](../Page/葉怡彤.md "wikilink")，信義性感卡翹姊妹 中分熱愛者

## 逝世

  - [前43年](../Page/前43年.md "wikilink")：[西塞罗](../Page/西塞罗.md "wikilink")，[罗马共和国晚期的](../Page/罗马共和国.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")、[政治家](../Page/政治家.md "wikilink")、[律师](../Page/律师.md "wikilink")、[作家](../Page/作家.md "wikilink")、[雄辩家](../Page/雄辩家.md "wikilink")（[前106年出生](../Page/前106年.md "wikilink")）

  -
  -
  - [1912年](../Page/1912年.md "wikilink")：[乔治·达尔文](../Page/乔治·达尔文.md "wikilink")，爵士，[英国天文学家](../Page/英国.md "wikilink")，[查尔斯·达尔文的第二个儿子](../Page/查尔斯·达尔文.md "wikilink")（[1845年出生](../Page/1845年.md "wikilink")）

  -
  - [1970年](../Page/1970年.md "wikilink")：[魯布·戈德堡](../Page/魯布·戈德堡.md "wikilink")，美国漫画家（[1883年出生](../Page/1883年.md "wikilink")）

  - [1976年](../Page/1976年.md "wikilink")：[郑律成](../Page/郑律成.md "wikilink")，[中国作曲家](../Page/中国.md "wikilink")（[1917年出生](../Page/1917年.md "wikilink")）

  - [1977年](../Page/1977年.md "wikilink")：[彼得·古德马克](../Page/彼得·古德马克.md "wikilink")（Peter
    Carl
    Goldmark），美国工程师，第一张唱片的发明人（[1906年出生](../Page/1906年.md "wikilink")）

  - [1979年](../Page/1979年.md "wikilink")：[松冈洋子](../Page/松冈洋子.md "wikilink")，[日本女权运动家](../Page/日本.md "wikilink")（[1954年出生](../Page/1954年.md "wikilink")）

  - [1993年](../Page/1993年.md "wikilink")：[沃爾夫岡·保羅](../Page/沃爾夫岡·保羅.md "wikilink")，[德國](../Page/德國.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")，[離子阱的開發人之一](../Page/離子阱.md "wikilink")，1989年獲頒[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")（[1913年出生](../Page/1913年.md "wikilink")）

  - [1998年](../Page/1998年.md "wikilink")：[马丁·罗德贝尔](../Page/马丁·罗德贝尔.md "wikilink")，美国生化学家（[1925年出生](../Page/1925年.md "wikilink")）

  - [2003年](../Page/2003年.md "wikilink")：[溫世仁](../Page/溫世仁.md "wikilink")，[台灣企業家](../Page/台灣.md "wikilink")（[1948年出生](../Page/1948年.md "wikilink")）

  - [2004年](../Page/2004年.md "wikilink")：[費德利克·芬奈爾](../Page/費德利克·芬奈爾.md "wikilink")，[美國指揮家](../Page/美國.md "wikilink")（[1914年出生](../Page/1914年.md "wikilink")）

  - [2018年](../Page/2018年.md "wikilink")：[劉駿耀](../Page/劉駿耀.md "wikilink")，台灣名嘴（生於1966年）

## 节日、风俗习惯

  - 12月7日是[世界民航日](../Page/世界民航日.md "wikilink")。
  - [1974年起每年的](../Page/1974年.md "wikilink")12月7日是[科特迪瓦的国庆日](../Page/科特迪瓦.md "wikilink")。
  - 12月7日是[聖安波羅修日](../Page/安波羅修.md "wikilink")