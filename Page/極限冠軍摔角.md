**極限冠軍摔角**（，簡稱**ECW**）是[美國的](../Page/美國.md "wikilink")[摔角](../Page/摔角.md "wikilink")[聯盟之一](../Page/聯盟.md "wikilink")，成立於1992年，現已被[世界摔角娛樂](../Page/世界摔角娛樂.md "wikilink")（WWE）收購並納入旗下，[創辦人為](../Page/創辦人.md "wikilink")（Tod
Gordon），解散前，[負責人為](../Page/負責人.md "wikilink")[保羅·黑門](../Page/保羅·黑門.md "wikilink")（Paul
Heyman）。

## 簡介

初創立時稱為**東部冠軍摔角**（），成立一年後便改名，但簡稱不變。

極限冠軍摔角主要特色是以呈現真實的摔角比賽過程，允許摔角選手使用各式各樣的招式與武器攻擊，在無規則的情況下盡情展現，比賽過程常有搏命演出的舉動，或者想盡各種辦法使對手投降，使比賽情緒沸騰，達到感官上的刺激，這類的比賽內容被認為是（Hardcore
wrestling），在美國擁有一定的收視群與支持者。

然而極限冠軍摔角的比賽方式畢竟無法得到大眾所接受，加上到後期由於經營上的錯誤，開始出現財務危機，2001年4月便正式解散，並與[世界冠軍摔角](../Page/世界冠軍摔角.md "wikilink")（**World
Championship
Wrestling**，簡稱**WCW**）一起被世界摔角娛樂所收購，2003年起，世界摔角娛樂將極限冠軍摔角重新組織成為旗下品牌[WWE
ECW](../Page/WWE_ECW.md "wikilink")，並與[WWE
Raw和](../Page/WWE_Raw.md "wikilink")[SmackDown一同列入每週例行節目當中](../Page/WWE_Friday_Night_SmackDown!.md "wikilink")，除原極限冠軍摔角為班底的選手以外，不時穿插世界摔角娛樂的選手跨台演出，目前極限冠軍摔角在世界摔角娛樂的主導下，不再有當年過於殘酷的比賽景象，終於2010年底結束此節目，改成了[WWE
NXT](../Page/WWE_NXT.md "wikilink")，節目類型也轉變成了訓練新人為主。

## 冠軍頭銜

### 極限冠軍摔角時代

  - [ECW世界重量級冠軍](../Page/ECW世界重量級冠軍.md "wikilink")（ECW World Heavyweight
    Championship）
  - [ECW世界雙打冠軍](../Page/ECW世界雙打冠軍.md "wikilink")（ECW World Tag Team
    Championship）
  - [ECW世界電視冠軍](../Page/ECW世界電視冠軍.md "wikilink")（ECW World Television
    Championship）

### [世界摔角娛樂時代](../Page/世界摔角娛樂.md "wikilink")

  - [ECW冠軍](../Page/ECW世界重量級冠軍.md "wikilink")（）

## 相關條目

  - [世界摔角娛樂](../Page/世界摔角娛樂.md "wikilink")

[Category:世界摔角娛樂](../Category/世界摔角娛樂.md "wikilink")