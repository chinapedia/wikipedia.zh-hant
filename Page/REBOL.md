**REBOL**（發音**reb-ol** \['reb-ol\]，英文**R**elative **E**xpression
**B**ased **O**bject
**L**anguage的缩写），[程序设计语言](../Page/程序设计语言.md "wikilink")，结合了[编程语言和](../Page/编程语言.md "wikilink")[元数据语言的特点](../Page/元数据语言.md "wikilink")，具有方言化的功能。针对[分布式计算而设计](../Page/分布式计算.md "wikilink")。

REBOL由[Carl
Sassenrath设计](../Page/:en:Carl_Sassenrath.md "wikilink")，他是[AmigaOS的系统架构师](../Page/AmigaOS.md "wikilink")。

## [Hello, world\!](../Page/Hello,_world!.md "wikilink")

``` text
REBOL []
alert "Hello World"
```

或

``` text
REBOL [] alert "Hello World"
```

## 外部連結

  - [官方主页](http://www.REBOL.com)
  - [開發网络](http://www.REBOL.net)
  - [REBOL之父--Carl's REBOL Blog](http://www.rebol.com/cgi-bin/blog.r)
  - ["Rebol This Rebol That"
    Blog](http://REBOLLovesJerry.BlogSpot.com/)（REBOL alpha團隊CJK區團員）
  - [RIX--the Rebol IndeXer](http://babelserver.org/rix)
  - [REBOL中文文档](http://shiningray.cnblogs.com/category/25334.html)