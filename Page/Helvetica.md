**Helvetica**是一種廣泛使用於[拉丁字母的](../Page/拉丁字母.md "wikilink")[无衬线字体](../Page/无衬线体.md "wikilink")，是[瑞士设计师](../Page/瑞士.md "wikilink")[马克斯·米丁格（Max
Miedinger）和爱德华](../Page/马克斯·米丁格.md "wikilink")·霍夫曼（Eduard
Hoffmann）于1957年设计的。

## 历史

Helvetica是由米丁格和霍夫曼在瑞士哈斯铸造所（Haas’sche
Schriftgiesserei）所製做。作为排版铅字制作的。当时人们熟悉的是「国际字体风格」（也称为「瑞士风格」），比如Josef
Muller-Brockmann在1896年推广Akzidenz
Grotesk這類在50多年前制作的无衬线字体，并由德国铸造所Berthold持續進行市场推广。因此哈斯希望能设计一套新的无衬线字体能够与Akzidenz
Grotesk在[瑞士市场上竞争](../Page/瑞士.md "wikilink")。最初的名称是「Neue Haas
Grotesk」，意为「哈斯的新无衬线铅字」，后来哈斯的德国母公司斯滕佩尔（Stempel）在1960年曾考虑将名字改为
Helvetia（源自拉丁文的“[瑞士](../Page/瑞士.md "wikilink")”一词），不过最终改为Helvetica，在拉丁文中意味“瑞士的”，以使其更具有国际市场竞争力。现在，Helvetica
由[Linotype公司所拥有](../Page/Linotype.md "wikilink")，Stempel 是它的子公司。

纪录片电影导演盖瑞·胡斯崔特（Gary
Hustwit）制作了一部关于Helvetica的同名纪录片。\[1\]\[2\]该片在2007年上映，纪念字体开发50周年。

## 特点

Helvetica被视作[现代主义在字体设计界的典型代表](../Page/现代主义.md "wikilink")。按照现代主义的观点，字体应该“像一个透明的容器一样”，使得读者在阅读的时候专注于文字所表达的内容，而不会关注文字本身所使用的字体。由于这种特点的存在，使得Helvetica适合用于表达各种各样的信息，并且在[平面设计界获得了广泛的应用](../Page/平面设计.md "wikilink")。

## 其他类似字体

[Helvarial.png](https://zh.wikipedia.org/wiki/File:Helvarial.png "fig:Helvarial.png")
Helvetica最常見的派生字體，应该是Monotype公司于1982年出品的「[Arial](../Page/Arial.md "wikilink")」字体。虽然在很多细节上與Helvetica有所不同，但許多非專業人士很容易將兩者混淆。Arial字体随着[Microsoft
Windows作業系統的附贈而迅速普及](../Page/Microsoft_Windows.md "wikilink")。

斯滕佩尔公司于1983年发布改进版本的**Neue
Helvetica**，增加了更多不同的粗細與寬度的選擇。原来的Helvetica字体家族只有31种，新版本的Neue
Helvetica有51种，而且以Univers为参照进行编号，以Neue Helvetica 55
Roman为基准，数字第一位为粗细，第二位为风格。现在Helvetica以及Neue
Helvetica由于企业收购合并的原因，成为Linotype公司的商品和商标。

## 用途

Helvetica被广泛用于全世界使用[拉丁字母和](../Page/拉丁字母.md "wikilink")[西里尔字母的国家](../Page/西里尔字母.md "wikilink")。同样的风格也被移植到[希腊字母](../Page/希腊字母.md "wikilink")、[阿拉伯字母](../Page/阿拉伯字母.md "wikilink")、[希伯来字母和](../Page/希伯来字母.md "wikilink")[汉字](../Page/汉字.md "wikilink")。[加拿大政府的联邦认证制度](../Page/加拿大.md "wikilink")（corporate
identity program）也使用了这个字体及其各種版本。

Helvetica被大量使用在标志、电视、新闻标题以及无数的商标。如[3M](../Page/3M.md "wikilink")、[愛克發](../Page/愛克發.md "wikilink")、[BASF](../Page/巴斯夫.md "wikilink")、[American
Apparel](../Page/American_Apparel.md "wikilink")、[BMW](../Page/BMW.md "wikilink")、[Crate
& Barrel](../Page/Crate_&_Barrel.md "wikilink")
箱桶之家、[Epson](../Page/Epson.md "wikilink")、[德国汉莎航空公司](../Page/德国汉莎航空公司.md "wikilink")、[Fendi](../Page/Fendi.md "wikilink")、[J.
C.
Penney](../Page/J._C._Penney.md "wikilink")、[Jeep](../Page/吉普汽車.md "wikilink")、[川崎重工業](../Page/川崎重工業.md "wikilink")、[Knoll](../Page/Knoll.md "wikilink")、[英特尔](../Page/英特尔.md "wikilink")、[無印良品](../Page/無印良品.md "wikilink")、[雀巢](../Page/雀巢.md "wikilink")、[松下](../Page/松下.md "wikilink")、[微软](../Page/微软.md "wikilink")、[三菱電機](../Page/三菱電機.md "wikilink")、[摩托羅拉](../Page/摩托羅拉.md "wikilink")、[豐田](../Page/豐田汽車公司.md "wikilink")、[Parmalat](../Page/Parmalat.md "wikilink")、[SAAB](../Page/SAAB.md "wikilink")（Helvetica
83粗體）、[三星](../Page/三星電子.md "wikilink")、[渣打銀行](../Page/渣打銀行.md "wikilink")、[Staples](../Page/Staples_Inc..md "wikilink")、[Target](../Page/Target_Corporation.md "wikilink")、[Texaco等數百間主要企業的標誌都是使用Helvetica字體](../Page/Texaco.md "wikilink")。

Helvetica是[iOS 7與](../Page/iOS_7.md "wikilink")[iOS
8系统的預設字体](../Page/iOS_8.md "wikilink")。系统中也包含Helvetica和[Helvetica
Neue字体](../Page/Helvetica_Neue.md "wikilink")。[Nimbus
Sans字体](../Page/Nimbus_Sans.md "wikilink")（[GNU/Linux系统中的預設字体](../Page/Linux.md "wikilink")）也是基于Helvetica的，Bitstream公司的Swiss
721 BT字体也是一样（从这个字体名字就可以看出）。

Helvetica在政府部门和公共机构中也获得广泛使用，例如[美国](../Page/美国.md "wikilink")[华盛顿和](../Page/华盛顿.md "wikilink")[波士顿的](../Page/波士顿.md "wikilink")[地铁和大众交通系统采用了该字体](../Page/地铁.md "wikilink")。新的[纽约地铁也将标志字体从](../Page/纽约地铁.md "wikilink")[Akzidenz
Grotesk体转为Helvetica字体](../Page/Akzidenz_Grotesk.md "wikilink")。

### 相關例子

  - [九廣東鐵](../Page/九廣東鐵.md "wikilink")（現為[港鐵](../Page/港鐵.md "wikilink")[東鐵綫](../Page/東鐵綫.md "wikilink")）所有車站（翻新後的[大圍站及](../Page/大圍站.md "wikilink")[落馬洲站除外](../Page/落馬洲站.md "wikilink")）的站牌和指示牌均使用Helvetica，但於2008年已被[港鐵換掉而消失](../Page/港鐵.md "wikilink")。而[港鐵公司於](../Page/港鐵公司.md "wikilink")1998年前興建之車站（[荔景](../Page/荔景站.md "wikilink")、[油麻地](../Page/油麻地站.md "wikilink")、[九龍灣](../Page/九龍灣站.md "wikilink")、[牛頭角](../Page/牛頭角站.md "wikilink")、[觀塘站和](../Page/觀塘站.md "wikilink")[北角站](../Page/北角站.md "wikilink")[將軍澳綫月台除外](../Page/將軍澳綫.md "wikilink")）均使用Helvetica。[九龍站月台是最後一個使用Helvetica的車站](../Page/九龍站_\(港鐵\).md "wikilink")。
  - 香港[無綫電視](../Page/無綫電視.md "wikilink")[明珠台](../Page/明珠台.md "wikilink")[新聞報道字幕系統於](../Page/無綫新聞.md "wikilink")1986年12月至1994年5月期間使用Helvetica字體。
  - [方正黑体](../Page/方正集團有限公司.md "wikilink")、方正超粗黑、方正中等线的字体英文部分使用的是Helvetica。
  - [台北捷運的站內英文標示](../Page/台北捷運.md "wikilink")（不含站名）均使用Helvetica字體，無論是初期路網或是後面新建的車站皆同。
  - 香港交通路牌在1996年至2010年代期間曾使用Helvetica字體，其後恢復使用Transport字體。
  - 中国大陆的央视曾在四套（1995.2.6-1995.7.26）、五套（1994.12.1-1995.7.26）的报时器字体使用类似于Helvetica的字体，1996年4月1日-1998年6月1日，三套/五套到八套的报时器字体采用真•Helvetica。

## 参见

  - [Arial](../Page/Arial.md "wikilink")
  - [Frutiger](../Page/Frutiger.md "wikilink")
  - [Univers](../Page/Univers.md "wikilink")

## 參考文獻

## 外部链接

  - [Typowiki:
    Helvetica](https://web.archive.org/web/20070228170819/http://typophile.com/wiki/Helvetica)
    - 一個關於字體的維基網站，其中的Helvetica條目
  - [linotype.com:
    Helvetica™字體家族總覽和相關訊息](http://www.linotype.com/526/helvetica-family.html)
  - [Helvetica記錄片電影的官方網站](https://web.archive.org/web/20060812192727/http://www.helveticafilm.com/)

### 深入阅读

  - [The Scourge of Arial](http://www.ms-studio.com/articles.html) -
    关于Arial字体的制作以及与Helvetica字体的渊源
  - [区别Helvetica和Arial](http://www.ms-studio.com/articlesarialsid.html)
  - [Arial字體與Helvetica字體之間的標誌](http://www.ironicsans.com/helvarialquiz/index.php)

[Category:无衬线字体](../Category/无衬线字体.md "wikilink")
[Category:Linotype字体](../Category/Linotype字体.md "wikilink")

1.  [*Helvetica*紀錄片電影官方網站](http://www.helveticafilm.com)
2.  [IMDb資料](http://www.imdb.com/title/tt0847817/)