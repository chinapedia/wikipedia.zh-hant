**NEEKO**（、），本名**西萩五十鈴**，[日本](../Page/日本.md "wikilink")[模特兒及](../Page/模特兒.md "wikilink")[聲優](../Page/聲優.md "wikilink")，[Smile
Maker所屬](../Page/Smile_Maker.md "wikilink")，[兵庫縣出身](../Page/兵庫縣.md "wikilink")。於2015年2月在自己的部落格上發表已經結婚並懷孕的訊息，同年的8月2日長男出生。

## 演出作品

### 電視動畫

**2006年**

  - [家庭教師HITMAN
    REBORN\!](../Page/家庭教師HITMAN_REBORN!_\(動畫\).md "wikilink")（**里包恩**）

<!-- end list -->

  -
    聲優出道作，[週刊少年Jump連載中REBORN的主角](../Page/週刊少年Jump.md "wikilink")。

**2007年**

  - [Ghost Hunt](../Page/惡靈系列.md "wikilink") \#23（克己的附身靈）

**2013年**

  - [超次元戰記 戰機少女](../Page/超次元戰記_戰機少女.md "wikilink")（瓦雷邱）

**2016年**

  - [魔法使 光之美少女！](../Page/魔法使_光之美少女！.md "wikilink")（奇庫倫）

**2017年**

  - [élDLIVE宇宙警探](../Page/élDLIVE宇宙警探.md "wikilink")（哈里）
  - [數碼暴龍宇宙-應用怪獸](../Page/數碼暴龍宇宙-應用怪獸.md "wikilink")（舔舔瑪麗）

### 電視劇

  - （（本人））

### 遊戲

  - [家庭教師HITMAN
    REBORN\!シリーズ](../Page/家庭教師HITMAN_REBORN!.md "wikilink")（**里包恩**）
      - 家庭教師ヒットマンREBORN\!DS 死ぬ気MAX\!ボンゴレカーニバル\!\!
      - 家庭教師ヒットマンREBORN\!DS フレイムランブル骸強襲\!
      - 家庭教師ヒットマンREBORN\! ドリームハイパーバトル\!
      - 家庭教師ヒットマンREBORN\!DS フレイムランブル 開炎 リング争奪戦\!
      - 家庭教師ヒットマンREBORN\! Let's暗殺\!? 狙われた10代目\!
      - 家庭教師ヒットマンREBORN\!DS ボンゴレ式 対戦バトルすごろく
      - 家庭教師ヒットマンREBORN\!DS フェイトオブヒート 炎の運命
      - 家庭教師ヒットマンREBORN\!DS フレイムランブル超 燃えよ未来
      - 家庭教師ヒットマンREBORN\! バトルアリーナ
      - 家庭教師ヒットマンREBORN\! 狙え\!? リング×ボンゴレトレーナーズ
      - 家庭教師ヒットマンREBORN\! 禁断の闇のデルタ
      - 家庭教師ヒットマンREBORN\!DS マフィア大集合\!ボンゴレフェスティバル\!\!

## 外部連結

  - [](https://web.archive.org/web/20070402130854/http://www.neeko21.com/)
  - [Smile
    Maker官方網](https://web.archive.org/web/20070928071452/http://www.smilemaker.co.jp/sub/neeko.html)

[Category:日本女性模特兒](../Category/日本女性模特兒.md "wikilink")
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:兵庫縣出身人物](../Category/兵庫縣出身人物.md "wikilink")