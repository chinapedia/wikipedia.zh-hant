[the_robots_of_dawn.jpg](https://zh.wikipedia.org/wiki/File:the_robots_of_dawn.jpg "fig:the_robots_of_dawn.jpg")
《**曙光中的機器人**》（***The Robots of
Dawn***），是[美國作家](../Page/美國作家.md "wikilink")[以撒·艾西莫夫](../Page/以撒·艾西莫夫.md "wikilink")（Isaac
Asimov）[出版於](../Page/出版.md "wikilink")1983年的[科幻小說](../Page/科幻小說.md "wikilink")，第三部以[以利亞·貝萊](../Page/以利亞·貝萊.md "wikilink")（Elijah
Baley）為主角的[科幻](../Page/科幻.md "wikilink")[推理小說](../Page/推理小說.md "wikilink")，艾西莫夫以實例說明[科幻小說有多種可能發展性](../Page/科幻小說.md "wikilink")，不必自我設限。這部小說在1984年榮獲[雨果獎](../Page/雨果獎.md "wikilink")（Hugo
Award）最佳長篇小說提名。

## 故事情節

地球紀元3424年\[1\]。

## 參考資料與外部連結

### 參考資料

<references/>

### 外部連結

  - [艾西莫夫官方網頁](http://www.asimovonline.com/)
  - [葉李華個人網站](http://yehleehwa.net/)，被譽為「艾西莫夫中文世界代言人」
  - [科幻國協在臺辦事處](http://www.wretch.cc/blog/danjalin)
  - [卡蘭坦斯蓋普恩基地](http://blog.yam.com/krantas)

### 延伸閱讀

  - [艾西莫夫：機器人與基地宇宙作品年表](http://blog.yam.com/krantas/article/1911265)，卡蘭坦斯蓋普恩基地，卡蘭坦斯
  - [艾西莫夫宇宙：銀河地圖／星球分區對照](http://blog.yam.com/krantas/article/3197914)，卡蘭坦斯蓋普恩基地，卡蘭坦斯

[Category:美國小說](../Category/美國小說.md "wikilink")
[Category:科幻小说](../Category/科幻小说.md "wikilink")
[Category:機器人小說](../Category/機器人小說.md "wikilink")
[Category:1983年美國小說](../Category/1983年美國小說.md "wikilink")

1.  [艾氏機器人與基地系列年代表](http://www.asimovonline.com/oldsite/insane_list.html)（英文）