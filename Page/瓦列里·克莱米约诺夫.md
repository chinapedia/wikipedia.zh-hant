**瓦列里·克莱米约诺夫**，是一位已退役的俄罗斯职业足球守门员，曾短暂入选过[独联体国家队](../Page/独联体国家足球队.md "wikilink")。

1997年，瓦列里加盟中国[甲A联赛](../Page/甲A联赛.md "wikilink")[上海申花队](../Page/上海申花.md "wikilink")，尽管经历了申花队史上最惨的失利，以1：9负于[北京国安](../Page/北京国安.md "wikilink")，但是他的能力仍然得到充分肯定。在当年最后一轮联赛中，正是他的出色发挥，才使得球队在队长[范志毅被罚下的情况下](../Page/范志毅.md "wikilink")4：2力克[大连万达](../Page/大连万达.md "wikilink")，终结了后者联赛56场不败的纪录。

瓦列里退役后出任了多支球队的助理教练，目前是[莫斯科斯巴达的守门员教练](../Page/莫斯科斯巴达.md "wikilink")。

[K](../Category/俄罗斯足球运动员.md "wikilink")
[V瓦](../Category/上海申花球員.md "wikilink")
[K](../Category/俄罗斯足球教练.md "wikilink")
[Category:在中华人民共和国的俄罗斯人](../Category/在中华人民共和国的俄罗斯人.md "wikilink")