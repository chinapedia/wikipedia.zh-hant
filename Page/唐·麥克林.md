**唐納德·理查·麥克林**（**Don McLean**，本名**Donald Richard
McLean**，）是美國籍的創作歌手，他的代表作包括長達八分鐘的《[American
Pie](../Page/American_Pie_\(歌曲\).md "wikilink")》美國派以及膾炙人口的[梵谷之歌](../Page/梵谷之歌.md "wikilink")。

## 個人生活

唐·麥克林受到[巴迪·霍利與](../Page/巴迪·霍利.md "wikilink")[約翰·甘迺迪總統被暗殺相當大的影響](../Page/約翰·甘迺迪.md "wikilink")。在他私人的生活上，他忍受著父親在1961年過世後所帶來的痛苦。麥克林從小愛好音樂，受過[歌劇課程的發聲訓練](../Page/歌劇.md "wikilink")，中學時期便積極想往音樂界發展，1963年從[Iona
Preparatory
School畢業](../Page/Iona_Preparatory_School.md "wikilink")，但是進入位於[賓夕法尼亞州的](../Page/賓夕法尼亞州.md "wikilink")[維拉諾瓦大學只不過四個月後就休學了](../Page/維拉諾瓦大學.md "wikilink")。稍後他進入了位於[紐約的](../Page/紐約.md "wikilink")[Iona
College就讀夜校](../Page/Iona_College_\(New_York\).md "wikilink")，並在1968年於此校獲得"工商管理"的學士學位，畢業後爲了到紐約當歌手放棄了[哥倫比亞大學的研究所獎學金](../Page/哥倫比亞大學.md "wikilink")。他在學校的活動中已經是一位很受歡迎的民謠歌手，在[紐約藝術廳](../Page/紐約藝術廳.md "wikilink")（New
York State Council on the
Arts）的獎金幫助之下，他開始接觸更多的大眾活動，在[哈德遜河附近的城鎮來回穿梭著](../Page/哈德遜河.md "wikilink")。他從友人兼精神導師[Pete
Seeger那裡學習到了表演的藝術](../Page/Pete_Seeger.md "wikilink")，麥克林在1969年隨著Seeger駕著他的船順著哈德遜河而上，目的是伸張對此河流環保問題的主張。這個清水活動對於改善此河流的水質立下很大的功勞。\[1\]

## 歌曲

### [American Pie](../Page/American_Pie_\(歌曲\).md "wikilink")

唐·麥克林最著名的創作[American
Pie常被用來詮釋與形容](../Page/American_Pie_\(歌曲\).md "wikilink")[巴迪·霍利](../Page/巴迪·霍利.md "wikilink")、與在1959年2月3日飛機失事而死亡的事件。尤其是“[音樂死去的那一天](../Page/音樂死去的那一天.md "wikilink")”（The
Day the Music Died）這句歌詞最常被廣泛提及\[2\]
。麥克林也曾指出歌曲裡的詞句是他個人自傳，也抽象呈現他在50年代中期到60年代撰寫歌曲時的人生故事\[3\]。根據家鄉傳說
"the levy" 是他家鄉的酒吧－靠近"Iona College" 的"Beechmont Tavern"。"American Pie"
這首歌的涵意象徵了在那時期不斷激化與激烈改變的流行音樂，導致之前在1950年代發展出以輕快愉悅為特色的搖滾樂，轉變成1960年代晚期以黑暗、省思、憤世嫉俗與增加社會關注為主的類型，這樣的轉變是由社會劇烈的轉變與浮動的政治氣氛驅使而成，音樂在此其中當然也被捲入其中，且在60年代末期要為變化中的美國下個定義。\[4\]

唐·麥克林的 "American Pie"
也存在對於音樂三十多年來歷史的仔細觀察與哲學詮釋的議題，學者、現代美國文學的教授與他的歌迷仍繼續找尋著歌曲中更為深邃的意義。在訪問中麥克林逗趣的說到，有許多關於這首歌的詮釋其實是假設的，很多關於這首歌的說法與涵意，其實他根本沒有提過也從未證實過。

2001年，“美國派”經過[美國唱片業協會和](../Page/美國唱片業協會.md "wikilink")[美國國家藝術基金會所舉辦投票方式進行表決的](../Page/美國國家藝術基金會.md "wikilink")[世紀之歌的第](../Page/世紀之歌.md "wikilink")5名。

2015年4月7日，麥克林原創的“美國派”工作手稿在[佳士得紐約拍賣行以](../Page/佳士得.md "wikilink")1,205,000美元（當時相當809,524英鎊或1,109,182歐元）拍出，成為美國文學手稿拍賣價的第三高。\[5\]

### 其他歌曲

麥克林其他為人所熟知的歌曲包括了：

  - "And I Love You So"，[-{zh-tw:艾維斯·普里斯萊;zh-hk:埃爾維斯·皮禮士利;
    zh-cn:埃尔维斯·普雷斯利;}-演唱版本](../Page/艾維斯·普里斯萊.md "wikilink")。
  - "[梵谷之歌](../Page/梵谷之歌.md "wikilink")"，一首獻給19世紀的荷蘭畫家[文生·梵谷的歌曲](../Page/文生·梵谷.md "wikilink")。
  - "Castles in the Air",，這首歌曲麥克林錄製過兩次，在1981年的重新錄製版本還登上暢銷排行榜的前四十名。
  - "Winterwood"
  - "Wonderful Baby"，一首獻給Fred Astaire由他本人錄音的歌曲
  - "Superman's Ghost"， 一首獻給George
    Reeves這位在1950年代扮演電視版[超人的男藝人之歌](../Page/超人.md "wikilink")。

[American
Pie專輯收錄了一首歌曲](../Page/American_Pie_\(歌曲\).md "wikilink")*Babylon*是由麥克林與The
Weavers的"Lee Hays"一同製作。Boney M樂團則在1978年將這首歌曲重新錄製為歌名Rivers of
Babylon的版本，也在英國獲得了暢銷榜第一名的成績，不過這兩首歌的編曲與表現方式十分不同，以致於在聽到時不會隨即聯想這是同樣的一首歌。

在1980年，麥克林創作了一首由[Roy
Orbison演唱獲得全球第一名的暢銷經典歌曲](../Page/Roy_Orbison.md "wikilink")
"[Crying](../Page/Crying_\(song\).md "wikilink")"，這首歌曲是先在海外發行取得成功後才在美國出版，成為了1981年前十名的暢銷作品。Orbison
本人曾說過麥克林是："本世紀的聲音。" ，並在之後再度錄製本歌曲且混合了麥克林的版本。

### 近期作品

在1991年，麥克林的"[American
Pie](../Page/American_Pie_\(歌曲\).md "wikilink")"因為重新成為話題而再度回到[英國金榜前二十名的排行上](../Page/英國.md "wikilink")，這首歌在2000年又因為[瑪丹娜重新編曲演唱而再度成為全球大熱門歌曲](../Page/瑪丹娜.md "wikilink")。

在2006年，他繼續著自己從美國延伸到歐洲的巡迴演唱活动，新專輯*Addicted to Black*在這之後就會搭配他的自傳*Killing
Us Softly: The Don McLean Story*一起發行。一位同樣也是歌手兼創作人的藝人"Lori
Lieberman"，曾在演唱會看了麥克林演唱著麥克林自己的創作 "Empty Chairs"
，之後就有感而發的寫下了一首名為 "Killing me softly with
his blue"。藉此詩為靈感，之後兩位創作人Norman Gimbel/Charles Fox 也才寫出經典歌曲"Killing Me
Softly with His Song。"

## 英語專輯

| 年度   | 專輯                                                                                                                 |
| :--- | :----------------------------------------------------------------------------------------------------------------- |
| 1970 | [Tapestry](../Page/Tapestry_\(Don_McLean\).md "wikilink")                                                          |
| 1971 | [American Pie](../Page/American_Pie_\(歌曲\).md "wikilink")                                                          |
| 1972 | [Don McLean](../Page/Don_McLean_\(同名專輯\).md "wikilink")                                                            |
| 1973 | [Playin' Favorites](../Page/Playin'_Favorites.md "wikilink")                                                       |
| 1974 | [Homeless Brother](../Page/Homeless_Brother.md "wikilink")                                                         |
| 1976 | Solo (LIVE)                                                                                                        |
| 1977 | [Prime Time](../Page/Prime_Time.md "wikilink")                                                                     |
| 1978 | [Chain Lightning](../Page/Chain_Lightning.md "wikilink")                                                           |
| 1981 | [Believers](../Page/Believers.md "wikilink")                                                                       |
| 1982 | Dominion (LIVE)                                                                                                    |
| 1987 | [Love Tracks](../Page/Love_Tracks.md "wikilink")                                                                   |
| 1989 | [For the Memories Vols I & II](../Page/For_the_Memories_Vols_I_&_II.md "wikilink")                                 |
| 1989 | [And I Love You So](../Page/And_I_Love_You_So.md "wikilink")（英國釋出）                                                 |
| 1990 | [Headroom](../Page/Headroom.md "wikilink")                                                                         |
| 1991 | [Christmas](../Page/Christmas_\(Don_McLean\).md "wikilink")                                                        |
| 1995 | [The River of Love](../Page/The_River_of_Love.md "wikilink")                                                       |
| 1997 | [Christmas Dreams](../Page/Christmas_Dreams.md "wikilink")                                                         |
| 2001 | [Sings Marty Robbins](../Page/Sings_Marty_Robbins.md "wikilink")                                                   |
| 2001 | Starry Starry Night (LIVE)                                                                                         |
| 2003 | [You've Got to Share: Songs for Children](../Page/You've_Got_to_Share:_Songs_for_Children.md "wikilink")           |
| 2003 | [The Western Album](../Page/The_Western_Album.md "wikilink")                                                       |
| 2004 | [Christmas Time\!](../Page/Christmas_Time!.md "wikilink")                                                          |
| 2005 | [Rearview Mirror: An American Musical Journey](../Page/Rearview_Mirror:_An_American_Musical_Journey.md "wikilink") |

### 合輯

| 年度   | 專輯                                      |
| :--- | :-------------------------------------- |
| 1977 | {{〈}}The Very Best of Don McLean{{〉}}   |
| 1993 | {{〈}}Favorites and Rarities{{〉}}        |
| 2003 | {{〈}}Legendary Songs of Don McLean{{〉}} |

### Rarities

| Year | Title                   | Additional information                                                                                                         |
| :--- | :---------------------- | :----------------------------------------------------------------------------------------------------------------------------- |
| 1982 | "The Flight of Dragons" | This song was recorded for the film *[The Flight of Dragons](../Page/The_Flight_of_Dragons.md "wikilink")* in the early 1980s. |

## 參見

<references/>

## 連結

  - [唐麥克林官方網站](http://www.don-mclean.com/)
  - [Allmusic](http://www.allmusic.com/artist/don-mclean-p4876)
  - [YouTube－對於美國派的解釋](http://www.youtube.com/watch?v=VsZFiMo8TIc)

[Category:美國歌手](../Category/美國歌手.md "wikilink")
[Category:創作歌手](../Category/創作歌手.md "wikilink")

1.
2.
3.  However, [Casey Kasem](../Page/Casey_Kasem.md "wikilink") confirmed
    the main outline of what Dearborn had said, and seemed to indicate
    that McLean agreed with that outline, on the January 15, 1972
    edition of *[American Top
    40](../Page/American_Top_40.md "wikilink")*, when "American Pie" had
    just ascended to \#1 on the Hot 100.
4.
5.