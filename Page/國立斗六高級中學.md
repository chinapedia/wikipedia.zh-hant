**國立斗六高級中學**，簡稱**斗六高中**、**斗高**，舊稱**省斗中**，別稱**綠城**，創立於1946年，1954年8月奉令改為省立斗六中學。1999年2月1日，更名為國立斗六高級中學。

## 校史

  - 1946年4月15日，由[台南縣政府及地方熱心教育人士就](../Page/台南縣政府.md "wikilink")[斗六神社及](../Page/斗六神社.md "wikilink")[斗六尋常高等小學舊址](../Page/斗六尋常高等小學.md "wikilink")，創設[台南縣立斗六初級中學](../Page/台南縣立斗六初級中學.md "wikilink")。
  - 1949年8月增設高中部，更名為[台南縣立斗六中學](../Page/台南縣立斗六中學.md "wikilink")。
  - 1950年8月因應縣治劃分，改為[雲林縣立斗六中學](../Page/雲林縣立斗六中學.md "wikilink")。
  - 1954年8月奉令改為[台灣省立斗六中學](../Page/台灣省立斗六中學.md "wikilink")，簡稱**省斗中**。
  - 1970年8月初中部結束，奉令改為[台灣省立斗六高級中學](../Page/台灣省立斗六高級中學.md "wikilink")。
  - 1995年8月奉[臺灣省政府教育廳令籌辦斗南分班](../Page/臺灣省政府教育廳.md "wikilink")，第一年招生兩班，第二年招收三班。
  - 1999年，斗南分班停止招生。
  - 1999年2月1日，更名為[國立斗六高級中學](../Page/國立斗六高級中學.md "wikilink")。

## 歷屆校長

| 任別 | 就職時間   | 姓名  |
| -- | ------ | --- |
| 一  | 1946.2 | 吳景箕 |
| 二  | 1948.2 | 林明海 |
| 三  | 1948.8 | 魏景嶷 |
| 四  | 1951.8 | 趙蘭庭 |
| 五  | 1952.8 | 李樹梓 |
| 六  | 1962.8 | 關 中 |
| 七  | 1967.9 | 許俊哲 |
| 八  | 1971.9 | 曹書勤 |
| 九  | 1978.8 | 陳　震 |
| 十  | 1983.2 | 周慶星 |
| 十一 | 1985.2 | 何瑞仁 |
| 十二 | 1989.2 | 蔡　勇 |
| 十三 | 1993.2 | 廖福榮 |
| 十四 | 1999.8 | 廖萬成 |
| 十五 | 2003.8 | 林明和 |
| 十六 | 2007.8 | 李世峰 |
| 十七 | 2011.8 | 劉永堂 |
| 十八 | 2017.8 | 羅聰欽 |

## 著名校友

### 教育界

  - [林見昌](../Page/林見昌.md "wikilink")：前[國立虎尾科技大學校長](../Page/國立虎尾科技大學.md "wikilink")。
  - [賴鼎銘](../Page/賴鼎銘.md "wikilink")：前[世新大學校長](../Page/世新大學.md "wikilink")。
  - [戴嘉南](../Page/戴嘉南.md "wikilink")：前[國立高雄師範大學校長](../Page/國立高雄師範大學.md "wikilink")。
  - [陳雅鴻](../Page/陳雅鴻.md "wikilink")：前[淡江大學校長](../Page/淡江大學.md "wikilink")。

### 醫學界

  - [游憲章](../Page/游憲章.md "wikilink")：台北[慈濟醫院副院長](../Page/慈濟醫院.md "wikilink")，台灣內視鏡外科醫學會名譽理事長、[國立台大醫學院副教授](../Page/國立臺灣大學醫學院.md "wikilink")。
  - [陳漢湘](../Page/陳漢湘.md "wikilink")：[馬偕護專校長](../Page/馬偕護專.md "wikilink")，曾任馬偕紀念醫院內科部主任。

### 政治界及政府官員

  - [李錫津](../Page/李錫津.md "wikilink")：前[台北市](../Page/台北市.md "wikilink")[教育局局長](../Page/教育局.md "wikilink")、前[台北市](../Page/台北市.md "wikilink")[建國中學校長](../Page/建國中學.md "wikilink")、[台北市公務人員訓練中心主任](../Page/台北市.md "wikilink")、[嘉義市副市長](../Page/嘉義市.md "wikilink")。
  - [高孟定](../Page/高孟定.md "wikilink")：前[逢甲大學教授](../Page/逢甲大學.md "wikilink")、前[雲林縣副縣長](../Page/雲林縣.md "wikilink")、前[立法委員](../Page/立法委員.md "wikilink")。
  - [張有惠](../Page/張有惠.md "wikilink")：曾任[台糖董事長](../Page/台糖.md "wikilink")、[行政院政務委員](../Page/行政院.md "wikilink")、行政院秘書長。
  - [李錫津](../Page/李錫津.md "wikilink")：前[台北市](../Page/台北市.md "wikilink")[教育局局長](../Page/教育局.md "wikilink")、前[台北市](../Page/台北市.md "wikilink")[建國中學校長](../Page/建國中學.md "wikilink")、[台北市公務人員訓練中心主任](../Page/台北市.md "wikilink")、[嘉義市副市長](../Page/嘉義市.md "wikilink")。
  - [陳連禎](../Page/陳連禎.md "wikilink")：曾任台灣[警察專科學校校長](../Page/警察專科學校.md "wikilink")、前[內政部](../Page/內政部.md "wikilink")[警政署保防室主任](../Page/警政署.md "wikilink")、彰化縣警察局局長。
  - [高坤輝](../Page/高坤輝.md "wikilink")：[台南市警察局局長](../Page/台南市.md "wikilink")。
  - [胡水旺](../Page/胡水旺.md "wikilink")：曾任[南投縣消防局副局長](../Page/南投縣.md "wikilink")。
  - [陳合發](../Page/陳合發.md "wikilink")：[財政部中區國稅局雲林分局局長](../Page/財政部.md "wikilink")。
  - [林進忠](../Page/林進忠.md "wikilink")：[外交部簡任](../Page/外交部.md "wikilink")14職等大使退休，曾任外交部亞西司司長、駐[約旦代表處代表](../Page/約旦.md "wikilink")、[沙烏地阿拉伯代表處代表等職](../Page/沙烏地阿拉伯.md "wikilink")。

### 企業界

  - [王俊傑](../Page/王俊傑.md "wikilink")：弘晟科技股份有限公司董事長。
  - [劉文正](../Page/劉文正.md "wikilink")：前台灣必治妥施貴寶公司總經理、現任創意電子股份有限公司獨立董事。
  - [張有惠](../Page/張有惠.md "wikilink")：曾任[台糖董事長](../Page/台糖.md "wikilink")、行政院政務委員、行政院秘書長。
  - [陳剛信](../Page/陳剛信.md "wikilink")：[民視總經理](../Page/民視.md "wikilink")。

### 法律界

  - [林清祥](../Page/林清祥.md "wikilink")：[行政法院臺中分院院長](../Page/行政法院.md "wikilink")。
  - [劉清景](../Page/劉清景.md "wikilink")：[臺灣高等法院法官](../Page/臺灣高等法院.md "wikilink")。
  - [戴勝利](../Page/戴勝利.md "wikilink")：[臺灣高等法院臺南分院法官](../Page/臺灣高等法院.md "wikilink")。
  - [林永茂](../Page/林永茂.md "wikilink")：[臺灣高等法院臺南分院法官退休](../Page/臺灣高等法院.md "wikilink")。

### 其他

  - [沈芯菱](../Page/沈芯菱.md "wikilink")：公益慈善青年。

## 外部連結

  - [國立斗六高級中學網站](http://www.tlsh.ylc.edu.tw/)

[Category:臺灣國立普通型高級中等學校](../Category/臺灣國立普通型高級中等學校.md "wikilink")
[Category:雲林縣中學](../Category/雲林縣中學.md "wikilink")
[Category:1946年創建的教育機構](../Category/1946年創建的教育機構.md "wikilink")
[Category:斗六市](../Category/斗六市.md "wikilink")