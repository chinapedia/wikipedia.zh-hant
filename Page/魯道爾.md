在[托爾金](../Page/托爾金.md "wikilink")（J. R. R.
Tolkien）的小說裡，**魯道爾**（Rhudaur）是[中土大陸西北部的一個虛構領地](../Page/中土大陸.md "wikilink")，也是三個[亞爾諾](../Page/亞爾諾.md "wikilink")（Arnor）崩潰後繼承國的其中一個，另外兩個是[雅西頓](../Page/雅西頓.md "wikilink")（Arthedain）和[卡多蘭](../Page/卡多蘭.md "wikilink")（Cardolan）。

魯道爾這名字在[辛達林語中解作](../Page/辛達林語.md "wikilink")「東方森林」，魯道爾的確是[伊利雅德](../Page/伊利雅德.md "wikilink")（Eriador）三國中位處最東的，由[風雲頂](../Page/風雲頂.md "wikilink")（Weathertop）延伸至[布魯南河](../Page/布魯南河.md "wikilink")（Bruinen），[東方大道](../Page/東方大道.md "wikilink")（Great
East Road）是魯道爾和卡多蘭的邊界，風雲丘（Weather Hills）則是魯道爾和雅西頓的邊界。

[狂吼河](../Page/狂吼河.md "wikilink")（Hoarwel）及[喧水河](../Page/喧水河.md "wikilink")（Loudwater）之間的土地應該也是魯道爾的領土。這裡是第三紀元1150年首個[史圖爾](../Page/史圖爾.md "wikilink")（Stoor）[哈比人踏足伊利雅德之地](../Page/哈比人.md "wikilink")。不過，[安格馬的敵意使史都爾哈比人逃到西面的](../Page/安格馬.md "wikilink")[夏爾](../Page/夏爾.md "wikilink")，有部分則前往[羅馬尼安](../Page/羅馬尼安.md "wikilink")（Rhovanion）。

魯道爾的[登丹人](../Page/登丹人.md "wikilink")（Dúnedain）人口甚少，佔魯道爾人口很少的比率。這事實使魯道爾對雅西頓和卡多蘭存有敵意，並與雅西頓爭奪風雲頂堡壘及[真知晶球](../Page/真知晶球.md "wikilink")（palantír）。

魯道爾的山地人在國內佔主要地位，並支配登丹人，後來甚至趕殺登丹人。第三紀元1356年魯道爾的邪惡人類刺殺雅西頓的國王[亞瑞吉來布一世](../Page/亞瑞吉來布一世.md "wikilink")（Argeleb
I）。史都爾哈比人開始逃往[登蘭德](../Page/登蘭德.md "wikilink")（Dunland）或穿越東面的山脈到達安都因區。第三紀元1409年，[安格馬開始支配魯道爾](../Page/安格馬.md "wikilink")。

第三紀元1636年，[大瘟疫肆虐於伊利雅德](../Page/大瘟疫.md "wikilink")，花費了約三百年才平息，這是因為安格馬和魯道爾並沒有進行救助。但兩國在第三紀元1975年遭到最沉重的打擊，雅西頓和卡多蘭被安格馬和魯道爾聯軍擊潰後，[剛鐸](../Page/剛鐸.md "wikilink")（Gondor）和[林頓](../Page/林頓.md "wikilink")（Lindon）的援軍抵達，並擊敗安格馬和魯道爾聯軍。安格馬巫王向北逃遁，山地人自此消此在中土大陸的歷史中。東方大道以南的地區成為[食人妖](../Page/食人妖.md "wikilink")（Troll）的國度，旅行者都匆匆走過以躲避食人妖。

安格馬在[佛諾斯特戰役](../Page/佛諾斯特戰役.md "wikilink")（Battle of
Fornost）中滅亡後，狂吼河及喧水河之間的土地成為登丹人的聚居地，[北方遊俠](../Page/北方遊俠.md "wikilink")（Rangers
of the
North）在這裡建立了多個村莊。但北魯道爾在第三紀元仍十分危險：第三紀元2930年，[亞拉德](../Page/亞拉德.md "wikilink")（Arador）在這裡被山地食人妖所殺，其子[亞拉松二世](../Page/亞拉松二世.md "wikilink")（Arathorn
II）在第三紀元2933年與半獸人在這裡爆發戰爭。

## 相關條目

  - [亞爾諾](../Page/亞爾諾.md "wikilink")
  - [安格馬](../Page/安格馬.md "wikilink")

[en:Arnor\#Rhudaur](../Page/en:Arnor#Rhudaur.md "wikilink")
[fr:Arnor\#Division et
ruine](../Page/fr:Arnor#Division_et_ruine.md "wikilink")
[nl:Arnor\#Rhudaur](../Page/nl:Arnor#Rhudaur.md "wikilink")
[sv:Arnor\#Rhudaur](../Page/sv:Arnor#Rhudaur.md "wikilink")

[R](../Category/中土大陸的地理.md "wikilink")