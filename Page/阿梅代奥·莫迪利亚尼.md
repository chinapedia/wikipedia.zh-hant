**亞美迪歐·莫迪里安尼**（****，），[義大利藝術家](../Page/義大利.md "wikilink")、畫家和雕塑家，為[表現主義畫派的代表藝術家之一](../Page/表現主義.md "wikilink")。

莫迪里安尼的特色是大膽創作裸女畫，曾受到當代保守風氣嚴厲批評，時至後世才獲得認可。莫迪里安尼自幼即受到古代和[文藝復興美術的薰陶](../Page/文藝復興美術.md "wikilink")，且與[巴伯羅·畢卡索](../Page/巴伯羅·畢卡索.md "wikilink")、[康斯坦丁·布朗庫西等著名藝術家交情菲淺](../Page/康斯坦丁·布朗庫西.md "wikilink")，進而受到19世紀末期[新印象派影響](../Page/新印象派.md "wikilink")，以及同時期的[非洲藝術](../Page/非洲.md "wikilink")、[立體主義等藝術流派刺激](../Page/立體主義.md "wikilink")，創作出深具個人風格，以優美弧形為特色的人物肖像畫，而成為[表現主義畫派的代表藝術家之一](../Page/表現主義.md "wikilink")。

## 生平

亞美迪歐·莫迪里安尼生於1884年[義大利](../Page/義大利.md "wikilink")[托斯卡尼](../Page/托斯卡尼.md "wikilink")（Toscana）區的[利佛諾](../Page/利佛諾.md "wikilink")（Livorno），雙親家族都是[瑟法底猶太人](../Page/瑟法底猶太人.md "wikilink")（Sephardi
Jews），分別居住在[羅馬](../Page/羅馬.md "wikilink")（Rome）與[法國](../Page/法國.md "wikilink")。

莫迪里安尼出生前，父親弗拉米諾·莫迪里安尼（Flaminio
Modigliani）是富有的企業家，但他出生之年事業失敗，宣告破產，全家由高級住宅搬到破舊的廉價公寓裡，家中財產則為了抵押債務，需要全部搬空，但当时法律规定当家中有女人怀孕时所放在孕妇床上的物品不得被搬走，当时莫迪里安尼的母亲已经怀孕，所以其床鋪和放在床上的物品因法律規定而未被搬走。

雖然家道中落，莫迪里安尼仍接受相當好的教育，尤其母親耶婕米耶·加爾辛（Eugénie
Garsin）出身法國加爾辛（Garsin）家族，精通[英語](../Page/英語.md "wikilink")、[法語和](../Page/法語.md "wikilink")[義大利語](../Page/義大利語.md "wikilink")，對[哲學和](../Page/哲學.md "wikilink")[文學有高深的造詣](../Page/文學.md "wikilink")，且據推測曾擔任為人捉刀，寫作英語小說。

莫迪里安尼身為长子，母親對他的教育最花心思，在[義務教育外](../Page/義務教育.md "wikilink")，又讓他接受四年的[古典主義教育](../Page/古典主義.md "wikilink")，培養出日後優雅的氣質。

而母親加爾辛似乎也注意到兒子藝術上的性向，1895年莫迪里安尼十一歲時，她在日記裡寫道：

  -
    *這孩子的特徵一直沒有定型，所以實在無法描述他會是一個什麼樣的人物。他的舉止就像一個被寵愛的小孩，但他從未缺乏智慧，我們必須等待及觀察這繭裡是什麼：也許他會是一名藝術家。*

### 少年時期

莫迪里安尼自小便體弱多病，1898年罹患[傷寒後更不得不休學回家修養](../Page/傷寒.md "wikilink")。恢復健康後，母親送他到當地著名風景畫畫家[古里耶摩·米克里的畫室學習](../Page/:en:Guglielmo_Micheli.md "wikilink")。

米克里是[斑痕畫派創始人](../Page/:en:Macchiaioli.md "wikilink")[喬凡尼·法托里的學生](../Page/:en:Giovanni_Fattori.md "wikilink")，擅長以抖動的線條表達光線動感。在他指導下，莫迪里安尼雖未承襲其畫風與取材興趣（事實上，他畢生作品中只有四張是風景畫），但顯露出繪畫上的才華，僅僅三、四個月便能畫出相當水準的素描。

莫迪里安尼因長期生活在窮困與疾病之中，留下的文字資料有限，因此關於他本人的軼事傳說十分多。其中，他與[巴伯羅·畢卡索的友誼](../Page/巴伯羅·畢卡索.md "wikilink")、未完成的[雕塑夢想](../Page/雕塑家.md "wikilink")，以及與[珍妮·赫布特尼傳奇性的戀情](../Page/珍妮·赫布特尼.md "wikilink")，仍是[巴黎](../Page/巴黎.md "wikilink")[蒙馬特與](../Page/蒙馬特.md "wikilink")[繪畫界膾炙人口的故事與話題](../Page/繪畫.md "wikilink")。

[Amedeo_Modigliani_-_Le_Grand_Nu.jpg](https://zh.wikipedia.org/wiki/File:Amedeo_Modigliani_-_Le_Grand_Nu.jpg "fig:Amedeo_Modigliani_-_Le_Grand_Nu.jpg")藏。\]\]
[HebuterneModigliani.jpg](https://zh.wikipedia.org/wiki/File:HebuterneModigliani.jpg "fig:HebuterneModigliani.jpg")畫像，1919年\]\]

## 外部链接

  - [amedeo modigliani full works
    list](https://web.archive.org/web/20140621131722/http://www.e-amedeo-modigliani.com/)

[M](../Category/托斯卡纳大区人.md "wikilink")
[M](../Category/意大利画家.md "wikilink")
[M](../Category/義大利猶太人.md "wikilink")
[Category:安葬于拉雪兹神父公墓者](../Category/安葬于拉雪兹神父公墓者.md "wikilink")