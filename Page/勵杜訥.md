**勵杜訥**（），[字](../Page/表字.md "wikilink")**近公**，又字**澹園**，室名**松喬堂**。[北直隸](../Page/北直隸.md "wikilink")[河間府](../Page/河間府.md "wikilink")[靜海縣](../Page/靜海縣.md "wikilink")（今屬[天津市](../Page/天津市.md "wikilink")）人。祖籍[浙江](../Page/浙江.md "wikilink")[紹興](../Page/紹興.md "wikilink")。[中國](../Page/中國.md "wikilink")[清代官员](../Page/清代.md "wikilink")、学者、书法家。

## 經歷

勵杜訥出生於[浙江](../Page/浙江.md "wikilink")，早年失去父母，由杜姓養父收養，故改名為杜訥。他精於[書法](../Page/書法.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[順治六年](../Page/順治.md "wikilink")（1649年）為[諸生](../Page/諸生.md "wikilink")。[康熙二年召求善於書寫之文士](../Page/康熙.md "wikilink")，訥以諸生試第一，參纂世祖實錄。康熙十九年（1680年）特授[翰林院](../Page/翰林院.md "wikilink")[编修](../Page/编修.md "wikilink")，充[日讲起居注官](../Page/日讲起居注官.md "wikilink")。康熙二十一年（1682年）復原姓，名勵杜訥。歷官[右贊善](../Page/右贊善.md "wikilink")、[左贊善](../Page/左贊善.md "wikilink")、翰林院[侍講](../Page/侍講.md "wikilink")、[光祿寺](../Page/光祿寺.md "wikilink")[少卿](../Page/少卿.md "wikilink")、[通政使司](../Page/通政使司.md "wikilink")[參議](../Page/參議.md "wikilink")、[右通政](../Page/右通政.md "wikilink")、[太僕寺卿](../Page/太僕寺卿.md "wikilink")、[宗人府](../Page/宗人府.md "wikilink")[府丞](../Page/府丞.md "wikilink")、[都察院](../Page/都察院.md "wikilink")[左副都御史](../Page/左副都御史.md "wikilink")。官至[刑部右侍郎](../Page/刑部右侍郎.md "wikilink")，四代皆通書畫。諡**文恪**\[1\]。雍正元年（1723年）追赠[礼部尚书](../Page/礼部尚书.md "wikilink")、[太子太傅](../Page/太子太傅.md "wikilink")\[2\]。有子[励廷仪为进士](../Page/励廷仪.md "wikilink")。

## 參考文獻

[Category:清朝通政使](../Category/清朝通政使.md "wikilink")
[Category:清朝光祿寺少卿](../Category/清朝光祿寺少卿.md "wikilink")
[Category:清朝太僕寺卿](../Category/清朝太僕寺卿.md "wikilink")
[Category:清朝宗人府丞](../Category/清朝宗人府丞.md "wikilink")
[Category:清朝左副都御史](../Category/清朝左副都御史.md "wikilink")
[Category:清朝畫家](../Category/清朝畫家.md "wikilink")
[Category:清朝書法家](../Category/清朝書法家.md "wikilink")
[Category:靜海人](../Category/靜海人.md "wikilink")
[D](../Category/勵姓.md "wikilink") [N](../Category/杜姓.md "wikilink")
[Category:諡文恪](../Category/諡文恪.md "wikilink")

1.  《藤陰雜記》（卷1）：勵近公杜訥，靜海歲貢。年六十，選授州同。會三殿三門禁扁書未工整，人薦書扁，合式，遂授編修。七十二歲，開坊升侍郎，贈尚書。亦諡文恪。二公不由科目，遭際亦同。
2.