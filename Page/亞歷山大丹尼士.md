**亞歷山大丹尼士**（；簡稱**丹尼士**；前稱**TransBus
International**）總部設於[英國](../Page/英國.md "wikilink")[蘇格蘭](../Page/蘇格蘭.md "wikilink")，為英國[巴士底盤製造商兼備製造巴士車身的公司](../Page/巴士.md "wikilink")，亦為西歐最大巴士生產商。

## 歷史

Alexander Dennis前身TransBus International於2001年1月1日開始運作，由Mayflower
Corporation同Henlys集團合作成立，包含原本由Mayflower Corporation擁有的Walter
Alexander和[Dennis Specialist
Vehicles](../Page/丹尼士車廠.md "wikilink")，以及原本由Henlys集團擁有的Plaxton業務。其中Mayflower
Corporation佔TransBus七成，而Henlys集團就佔其餘三成。

當時公司設有幾間廠房，分佈蘇格蘭[福爾柯克和北愛爾蘭貝爾法斯特Mallusk](../Page/福爾柯克_\(城市\).md "wikilink")（原Walter
Alexander）、英格蘭Anston和Scarborough（原Plaxton）、英格蘭[威根](../Page/威根.md "wikilink")（原Northern
Counties）以及英格蘭[僑福](../Page/吉爾福德.md "wikilink")（原Dennis Specialist
Vehicles）。

TransBus運作期間，巴士上的Alexander、Plaxton以及Dennis三個品牌陸續以TransBus取代。2004年3月31日，TransBus連同其大股東Mayflower
Corporation一齊被接管。同年5月17日，TransBus客車及小巴車身生產業務（包括Anston和Scarborough廠房）被管理層收購，並用回Plaxton。四日後，TransBus餘下的業務亦被商人收購，並易名為Alexander
Dennis。而TransBus的貝爾法斯特廠房因為無人問津而關閉。

其後Alexander Dennis進行公司重整。2005年1月26日，Alexander
Dennis的Wigan廠房關閉。2007年5月，Alexander
Dennis收購Plaxton，令原本TransBus分開的業務再度復合。

2012年6月7日，Alexander Dennis收購澳洲的車身建造商Custom Coaches，但是在2014年5月出售。

## 產品

### 巴士底盤

  - [飛鏢SLF（Dart SLF）](../Page/丹尼士飛鏢巴士.md "wikilink")
  - [三叉戟2型（Trident 2）](../Page/丹尼士三叉戟二型.md "wikilink")
  - [三叉戟3型（Trident 3）](../Page/丹尼士三叉戟三型.md "wikilink")
  - 標槍（Javelin，客車）
  - R 系列（R-Series，客車）
  - [Enviro 350H](../Page/亞歷山大丹尼士Enviro_350H.md "wikilink")

### 巴士車身

  - [DM5000](../Page/都普#都普DM5000.md "wikilink")
  - [ALX200](../Page/亞歷山大ALX200.md "wikilink")
  - [ALX300](../Page/亞歷山大ALX300.md "wikilink")
  - [ALX400](../Page/亞歷山大ALX400.md "wikilink")
  - [ALX500](../Page/亞歷山大ALX500.md "wikilink")
  - [Pointer](../Page/Plaxton_Pointer.md "wikilink")
  - [President](../Page/Plaxton_President.md "wikilink")

### 一體化設計的巴士（或底盤／車身）

  - [Enviro 200](../Page/亞歷山大丹尼士Enviro_200.md "wikilink")（未投產）
  - [Enviro 200
    Dart](../Page/亞歷山大丹尼士Enviro_200_Dart.md "wikilink")（取代飛鏢SLF底盤／Pointer車身）
  - [Enviro
    300](../Page/亞歷山大丹尼士Enviro_300.md "wikilink")（2007年起正式取代ALX300車身，現已停產）
  - [Enviro 400](../Page/亞歷山大丹尼士Enviro_400.md "wikilink")（取代ALX400車身）
  - [Enviro
    500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")（已停產，取代三叉戟3型底盤／ALX500車身）
  - [Enviro 500 MMC/Enviro 500 MMC
    Facelift](../Page/亞歷山大丹尼士Enviro_500_MMC.md "wikilink")（取代舊版Enviro
    500底盤／車身）
  - [Enviro 400
    MMC](../Page/亞歷山大丹尼士Enviro_400_MMC.md "wikilink")（取代舊版Enviro
    400底盤／車身）

亞歷山大丹尼士同時亦有生產[混燃車系](../Page/混合動力車輛.md "wikilink")。

### 消防車

Alexander Dennis初時繼續生產丹尼士消防車，直到2007年宣佈不再生產消防車為止
[DennisSabre.JPG](https://zh.wikipedia.org/wiki/File:DennisSabre.JPG "fig:DennisSabre.JPG")

  - Sabre
  - Rapier
  - Dagger

## 外部連結

  -
[Category:英國汽车公司](../Category/英國汽车公司.md "wikilink")
[Category:巴士生產商](../Category/巴士生產商.md "wikilink")
[丹尼士巴士](../Category/丹尼士巴士.md "wikilink")