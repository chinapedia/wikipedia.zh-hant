**人體冷凍技術**（或**人體冷藏學**或**人體冰凍法**；）是一種[試驗中的](../Page/試驗.md "wikilink")[醫學技術](../Page/醫學技術.md "wikilink")，把[人體或](../Page/人體.md "wikilink")[動物在極低溫](../Page/動物.md "wikilink")（一般在[攝氏零下](../Page/攝氏.md "wikilink")196度以下
/
[華氏零下](../Page/華氏.md "wikilink")320以下）的情況下[深低溫保存](../Page/深低溫保存.md "wikilink")，並希望可以在[未來通過先進的醫療](../Page/未來.md "wikilink")[科技使他們解凍後](../Page/科技.md "wikilink")[復活及](../Page/復活.md "wikilink")[治療](../Page/治療.md "wikilink")。1967年，[詹姆斯·貝德福德的遺體成為首位接受人體冷凍技術的例子](../Page/詹姆斯·貝德福德.md "wikilink")。

人體冷凍技術被[美國的](../Page/美國.md "wikilink")《生活科學》雜誌（*Live
Science*）列為十大[人腦未解之](../Page/人腦.md "wikilink")[謎之一](../Page/謎.md "wikilink")\[1\]\[2\]，及十大超越人类极限的未来[科学](../Page/科学.md "wikilink")[技术](../Page/技术.md "wikilink")\[3\]。

現時人體冷凍技術不被主流科學界認可及接受。國際低溫生物學學會認為，人體冷凍技術不是科學，也不屬[低溫生物學的範疇](../Page/低溫生物學.md "wikilink")。\[4\]美國低溫物理學認為，人體冷凍也不屬[低溫物理學範疇](../Page/低溫物理學.md "wikilink")，並認為人體冷凍技術的理論是站不住腳。\[5\]

目前，最大型的人體冷藏公司為[美國的](../Page/美國.md "wikilink")[阿爾科生命延續基金](../Page/阿爾科生命延續基金.md "wikilink")（Alcor
Life Extension
Foundation）和[人體冷凍機構](../Page/人體冷凍機構.md "wikilink")（Cryonics
Institute）。

## 技術前設

人體冷凍技術主要前設是，人類所有[記憶](../Page/記憶.md "wikilink")、[性格](../Page/性格.md "wikilink")、[身份](../Page/身份.md "wikilink")、[意識都是以](../Page/意識.md "wikilink")[細胞結構及](../Page/細胞.md "wikilink")[化學形式](../Page/化學.md "wikilink")（主要在[腦部](../Page/腦部.md "wikilink")）儲存。人體冷凍技術希望透過冷凍科技，防止腦部損害，以達至[暫停生命的效果](../Page/暫停生命.md "wikilink")，及希望未來可以透過科技把冷凍者[復生及醫治](../Page/復活.md "wikilink")。

## 哲學道德倫理考慮的問題

### 宗教考慮的問題

不少[宗教人士認為](../Page/宗教.md "wikilink")，人體冷凍技術與宗教發生[衝突](../Page/衝突.md "wikilink")，主要原因是因為人的[靈魂已離去](../Page/靈魂.md "wikilink")，該技術不可能冷凍人的靈魂。但人體冷凍公司則認為，人體冷凍者就如[人長期](../Page/人.md "wikilink")[睡覺或](../Page/睡覺.md "wikilink")[昏迷一樣](../Page/昏迷.md "wikilink")，其靈魂不會離開[肉體](../Page/身體.md "wikilink")，而人體冷凍技術亦跟其他醫療技術（如[心臟](../Page/心臟.md "wikilink")[移植](../Page/器官移植.md "wikilink")[手術](../Page/手術.md "wikilink")）一樣，只是設法把冷藏者醫治和[延續他們的生命](../Page/生命延續.md "wikilink")。人體冷凍公司更認為冷藏者只是好像患了病的[病人一樣](../Page/病人.md "wikilink")。因此，人體冷凍公司認為人體冷凍技術與宗教並沒有任何衝突。\[6\]\[7\]

## 費用

費用方面，不同公司的收費均不同。人體冷凍的收費由10,000[美元](../Page/美元.md "wikilink")（[俄羅斯](../Page/俄羅斯.md "wikilink")[KrioRus公司](../Page/KrioRus公司.md "wikilink")（）的[腦神經冷藏](../Page/腦神經.md "wikilink")），到28,000美元（人體冷凍機構的全身冷藏），至155,000美元（[美國人體冷凍學會](../Page/美國人體冷凍學會.md "wikilink")〔American
Cryonics
Society〕的全身冷凍），至200,000美元（阿爾科生命延續基金的全身冷藏）均有，收費視乎不同的[公司](../Page/公司.md "wikilink")、[服務](../Page/服務.md "wikilink")、[國家或](../Page/國家.md "wikilink")[地區而定](../Page/地區.md "wikilink")。\[8\]

此外，人體冷藏者也可以考慮使用以[人壽保險的形式付款](../Page/人壽保險.md "wikilink")，或[分期付款繳交](../Page/分期付款.md "wikilink")。\[9\]\[10\]\[11\]

不過，一般而言，人體冷凍機構不會保證所收取的收費可長期為人體冷凍技術服務提供經濟支持。人體冷凍服務商會為冷凍者成立一個類似基金來營運，如果冷凍者提供的資金用盡，可能會再須收取額外費用，或需停止人體冷凍的保存服務。因此，人體冷凍機構的收費，一般指接受冷凍的最低營運基金的收費（minimum
funding）。人體冷凍機構一般的收費相等於50年至100年的營運費用，這是基於2%的[投資回報率](../Page/投資.md "wikilink")（在扣除通脹及物價上升後）的假設。然而,
這會受到其他因素，如[投資失利或](../Page/投資.md "wikilink")[物價升幅過高等影響](../Page/通脹.md "wikilink")，一旦營運基金用盡，就會停止冷凍服務。

## 技術可行性

現時並沒有任何人能在冷藏後[復生](../Page/復生.md "wikilink")，但是，人體冷凍者卻基於以下的[信念](../Page/信念.md "wikilink")，相信其[技術的可行性](../Page/技術.md "wikilink")，包括：

  - 假如[人體或](../Page/人體.md "wikilink")[動物的結構能夠完好地保存](../Page/動物.md "wikilink")，那麼[生命可以被停止](../Page/生命.md "wikilink")，也可以被重新啟動。
  - 玻璃化冷凍方法能有效地保存人體或生命。
  - 相信未來的[分子修復技術可以有效地修復受損](../Page/分子.md "wikilink")（甚至已死去）的生理結構。\[12\]

此外，有些人相信以下例子能[推斷其技術的可行性](../Page/推論.md "wikilink")，包括：

  - 有實驗成功將長年冷凍下的[線蟲解凍後復活](../Page/線蟲.md "wikilink")。\[13\]\[14\]<ref>

[科幻片成真！4.2萬年前「冰封線蟲」成功解凍...能動還能吃](https://www.ettoday.net/news/20180727/1221794.htm)</ref>\[15\]\[16\]

  - 曾有女嬰在加拿大凍死數小時後解凍生還。\[17\]\[18\]
  - 有實驗成功冷凍一[猴子數小時後復原](../Page/猴子.md "wikilink")。\[19\]
  - 玻璃水的冷凍方法解決了身體冷凍後易[受損的問題](../Page/損害.md "wikilink")。\[20\]\[21\]
  - 在攝氏零下196度的低溫下，其反應速率僅為攝氏37度體溫的9。\[22\]
  - 有研究指體溫較低更易使人[長壽](../Page/壽命.md "wikilink")。\[23\]\[24\]
  - 有[實驗成功用低溫使](../Page/實驗.md "wikilink")[狗隻](../Page/狗隻.md "wikilink")、[豬隻和](../Page/豬.md "wikilink")[老鼠冷卻後](../Page/老鼠.md "wikilink")（冷卻後只有極微弱心跳）數小時後復原。\[25\]\[26\]\[27\]\[28\]
  - 有實驗成功把冷凍了16年的[老鼠](../Page/老鼠.md "wikilink")[基因](../Page/基因.md "wikilink")，[複製出新的老鼠](../Page/克隆.md "wikilink")，並可[健康地成長](../Page/健康.md "wikilink")。\[29\]
  - 部分人體[器官可在低溫下長期保存](../Page/器官.md "wikilink")。\[30\]
  - 科技[樂觀者認為](../Page/樂觀.md "wikilink")，待[納米技術在將來發展成熟後](../Page/納米技術.md "wikilink")，可以修復冷凍過程帶來的細胞損傷，還可以修復由於[衰老或者](../Page/衰老.md "wikilink")[疾病引起的損傷](../Page/疾病.md "wikilink")。\[31\]

## 批評及爭議

  - 在原理上人體冷凍不是現實動物的[冬眠或在科幻的](../Page/冬眠.md "wikilink")[人工冬眠](../Page/人工冬眠.md "wikilink")，這兩者仍然保持著生命的表徵如呼吸和心跳等，人體冷凍則否。
  - 就目前技術而言，能低温保存的只有[血液](../Page/血液.md "wikilink")、[细胞和](../Page/细胞.md "wikilink")[人体](../Page/人体.md "wikilink")[器官](../Page/器官.md "wikilink")。但目前要保存單個人体器官仍然非常困难。而主流[科学界还是在研究细胞和组织器官的保存](../Page/科学界.md "wikilink")。\[32\]
  - 現時還沒有冷凍成功的例子。\[33\]
  - 現時沒有[實驗能證明](../Page/實驗.md "wikilink")，在冷凍一段長時間後，[細胞仍可以保存完好](../Page/細胞.md "wikilink")。\[34\]
  - 人體冷凍技術目前被部分[醫學](../Page/醫學.md "wikilink")[專家認為不具任何](../Page/專家.md "wikilink")[醫學價值](../Page/醫學.md "wikilink")。\[35\]
  - 一些學者更稱人體冷凍技術為「製作現代[木乃伊](../Page/木乃伊.md "wikilink")（Modern
    mummies）的技術」。\[36\]\[37\]\[38\]

## 法律議題及法律保障

在[法律上](../Page/法律.md "wikilink")，人體冷凍的冷藏者會被視為已[死亡的人](../Page/死亡.md "wikilink")。而人體冷凍的公司則視為一間提供殯葬的機構及一間[醫學研究的機構](../Page/醫學.md "wikilink")。人體冷凍者亦可被視為一種[殯葬的方式](../Page/殯葬.md "wikilink")，或視為將遺體或[器官捐贈給醫學機構作](../Page/器官捐贈.md "wikilink")[醫療](../Page/醫療.md "wikilink")[科學研究用途](../Page/科學研究.md "wikilink")。人體冷凍公司必須在病人在法律上宣告死亡後才能進行人體冷凍。人體冷凍保存的機構，對冷凍者遺體的完整性不會作出任何的法律保證，也不會保證遺體可長久保存。\[39\]\[40\]

### 人體冷凍保存服務的可持續性

大部分的冷凍公司的[協議中均聲明](../Page/協議.md "wikilink")，不會為人體冷凍者提供可永久或長期保存的法律保證。人體冷凍提供者也不能保證人體冷凍服務的長期[合法性](../Page/合法性.md "wikilink")。如提供人體冷凍服務機構的[資金用盡或宣佈](../Page/資金.md "wikilink")[倒閉](../Page/倒閉.md "wikilink")，人體冷凍機構可在此情況下停止人體冷凍服務。如未來法律禁止人體冷凍繼續進行，有關人體冷凍服務的機構也須停止人體冷凍保存服務。

如人體冷凍者所提供的資金及[費用未能足以維持將來的人體冷凍服務](../Page/費用.md "wikilink")，以及該冷凍者及其親友或支持者提供不到足夠的資金繼續維持，人體冷凍機構也會在此情況下停止人體冷凍服務。

如果人體冷凍保存服務機構所在的地方，當地政府通過法律或政策，限制人體冷凍服務進行，人體冷凍服務機構也需終止人體冷凍服務。

一般而言，如果人體冷凍服務企業倒閉，人體冷凍企業會盡量找尋市場上有沒有其他機構或志願團體，可承繼該冷凍服務企業的服務。如果無法找到合適的承繼者，則會終止人體冷凍的服務。

當人體冷凍機構停止人體冷凍服務後，在可行及經濟許可的情況行，可能改以其他冷凍方式保存，例如由全身冷凍改為只冷凍腦部或神經系統，或改以較高溫的方式繼續進行冷凍保存。也可能會停止冷凍，但改為以[化學保存的方式繼續保留](../Page/人體塑化技術.md "wikilink")，或以其他[遺體保存技術繼續保存遺體](../Page/遺體保存技術.md "wikilink")。

如果經濟情況或法律政策或因技術問題不允許繼續保存，人體冷凍公司會在服務停止後，為冷凍者以[土葬的方式埋葬](../Page/土葬.md "wikilink")，或直接為冷凍者進行[火化](../Page/火化.md "wikilink")，或以其他的[殯葬形成來處理遺體](../Page/殯葬.md "wikilink")，視冷凍服務計劃、冷凍者意向、冷凍者的經濟和財務情況及當時的法規而定。
\[41\]\[42\]\[43\]

## 其他

### 自然冷凍法

自然冷凍法（Natural
Cryonics）是一種[經濟型人體冷凍的構思](../Page/經濟.md "wikilink")，把人體冷凍在[地球](../Page/地球.md "wikilink")[極地氣候地區](../Page/寒帶.md "wikilink")（多年[溫度均在](../Page/溫度.md "wikilink")[攝氏零度以下的地方](../Page/攝氏.md "wikilink")）的[永久凍土層下保存](../Page/永久凍土.md "wikilink")，期望未來有人使用醫療技術使病人[復生](../Page/復生.md "wikilink")\[44\]\[45\]\[46\]。因此，這類在「大自然的冰箱」下長久保存的方法，有時候又稱為冰葬或雪葬。

### 某些作品以此作為故事內容

1.  [怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")：漫畫版第十七集，第四章《前往未來的禮物》
2.  [三体](../Page/三体.md "wikilink")
3.  [獵殺星期一](../Page/獵殺星期一.md "wikilink")
4.  [逆緣](../Page/逆緣.md "wikilink")
5.  [飛出個未來](../Page/飛出個未來.md "wikilink")

## 注释

## 参考文献

## 外部連結

  - [人體冷凍技術常見問題](http://www.benbest.com/cryonics/CryoFAQ.html)
  - [美國阿爾科生命延續基金](http://www.alcor.org/)
  - [美國人體冷凍機構](http://www.cryonics.org/)
  - [俄羅斯 Kriorus 機構](http://www.kriorus.ru/en)
  - [20年实现人体冷冻复活](http://www.people.com.cn/GB/paper447/16692/1469113.html)
  - [人體冷凍：通向未來之路](http://tech.qq.com/zt/2006/ldfhjh/index.htm)
  - [中国狂人欲启冷冻复活计划](http://tech.sina.com.cn/d/focus/ldfhjh/index.shtml)
  - [十大争议科学话题](http://tech.sina.com.cn/d/focus/zyht/index.shtml)
  - [郑奎飞：“我要让死人复活”](http://paper.people.com.cn/hqrw/html/2006-11/16/content_12065967.htm)

## 參見

  - [低溫物理學](../Page/低溫物理學.md "wikilink")
  - [低溫生物學](../Page/低溫生物學.md "wikilink")
  - [超低溫保存](../Page/超低溫保存.md "wikilink")
  - [生命延續](../Page/生命延續.md "wikilink")
  - [意识上传](../Page/意识上传.md "wikilink")
  - [遺體保存技術](../Page/遺體保存技術.md "wikilink")
  - [阿爾科生命延續基金](../Page/阿爾科生命延續基金.md "wikilink")
  - [人體冷凍機構](../Page/人體冷凍機構.md "wikilink")
  - [長生不老](../Page/長生不老.md "wikilink")
  - [疾病](../Page/疾病.md "wikilink")
  - [復活](../Page/復活.md "wikilink")
  - [暫停生命](../Page/暫停生命.md "wikilink")
  - [未來學](../Page/未來學.md "wikilink")
  - [化學腦保存](../Page/化學腦保存.md "wikilink")

{{-}}

[人体冷冻技术](../Category/人体冷冻技术.md "wikilink")
[Category:丧葬](../Category/丧葬.md "wikilink")
[Category:科幻主题](../Category/科幻主题.md "wikilink")
[Category:伪科学](../Category/伪科学.md "wikilink")
[Category:邊緣理論](../Category/邊緣理論.md "wikilink")
[Category:虛假學術](../Category/虛假學術.md "wikilink")

1.  [Top 10 Mysteries of the
    Mind](http://www.livescience.com/health/top10_mysteriesofthemind.html),
    Live Science
2.  [十大人腦未解之迷](http://scitech.people.com.cn/BIG5/4650678.html),人民網
3.  [十大超越人类极限的未来科学技术](http://www.pcworld.com.cn/news/1/2007/0718/140854.shtml)
    , PCHOME 中国网
4.  [Society for Cryobiology Position Statement on
    Cryonics](https://www.societyforcryobiology.org/assets/documents/Position_Statement_Cryonics_Nov_18.pdf),
    Society for Cryobiology, Nov 2018
5.  [Cryonics is NOT the Same as
    Cryogenics](https://cryogenicsociety.org/cryonics/), Cryogenic
    Society of America
6.  [Cryonics FAQ - Religion and
    Philosophy](http://www.benbest.com/cryonics/CryoFAQ.html#_IVA_)
7.  [Alcor - Christianity and Cryonics: Questions and
    Answers](http://www.alcor.org/Library/html/christianityandcryonics.html),
    Alcor Life Extension Foundation
8.  [Alcor Membership Info:
    Costs](http://www.alcor.org/BecomeMember/scheduleA.html)
9.  [Cryonics FAQ - Financial
    Questions](http://www.benbest.com/cryonics/CryoFAQ.html#_IIID_)
10. [英国人每周缴10镑做“人体冷冻”
    期待未来復活](http://news.163.com/09/0329/04/55HVBK7K0001121M.html),
    網易新聞, 2009年6月1日查閱
11. [冷冻遗体等待重生 英国人争做“復活梦”](http://tech.qq.com/a/20090330/000044.htm),
    腾讯科技頻道, 2009年6月1日查閱
12. [Alcor: About
    Cryonics](http://www.alcor.org/AboutCryonics/index.html)
13. [Worms Frozen for 42,000 Years in Siberian Permafrost Wriggle to
    Life](https://www.livescience.com/63187-siberian-permafrost-worms-revive.html),
    Live Science
14. [4.2万年冰冻线虫竟起死回生:人体冷冻术何时能成](http://tech.163.com/18/0807/00/DOIJVUBU00097U81.html),
    网易科学人
15. [線蟲冰封4萬年復活！　科學家警告恐有「古代病毒」](https://news.tvbs.com.tw/world/966831)
16. [科幻片成真！4万年前冰封线虫被复活](http://tech.ifeng.com/a/20180727/45088077_0.shtml?tp=1532620800000)
17. [“凍死”女嬰復活
    速凍大腦是原因？](http://www.people.com.cn/BIG5/kejiao/42/154/20010320/420789.html)，人民網
18. [人體冷藏復活將不再隻是夢想](http://news.eastday.com/epublish/big5/paper148/20010320/class014800009/hwz341301.htm)
19. [打破砂鍋:“人體冷凍術”能否讓人起死回生？](http://www.stdaily.com/big5/dengxiaoping/2006-02/21/content_490468.htm)(文中最後第二段提到江基堯教授曾讓一猴子死而復生)，科技日報電子版
20. [玻璃水能否让人冻后复活](http://learning.sohu.com/20060703/n244026417.shtml)，搜弧
21. [芬兰科学家造出玻璃水，可让人体冷冻后复活](http://www.bioon.com/biology/news/184405.shtml)
    ，生物谷生物频道
22. [Scientific Justification of Cryonics
    Practice](http://www.cryonics.org/reports/Scientific_Justification.pdf)
    , Page 4, REJUVENATION RESEARCH, Volume 11, Number 2, 2008, Cryonics
    Institute ．於2008年12月29日查閱．
23. [俄罗斯科学家称人体温降低两度可多活120到150年](http://news3.xinhuanet.com/st/2003-07/30/content_1000590.htm)
    ，新華網
24. [冰凍長生夢——我們20年後再見](http://big5.china.com.cn/news/zhuanti/xzk/txt/2006-11/16/content_7367944_4.htm)，中國網
25.
26.
27.
28. [冷冻技术让“死”猪复活 可使生命暂停](http://tech.qq.com/a/20060213/000272.htm), 腾讯科技
29. [冷凍16年老鼠，遭成功克隆「復活」](http://news.wenweipo.com/2008/11/05/IN0811050074.htm)，文匯報
30. [黃金书屋 -
    美国的冷藏人体公司](http://210.44.16.6/gold/gold-digest/abroad/179.htm)

31. [有人使用人体冷冻法保存过吗？](http://science.bowenwang.com.cn/cryonics3.htm) ,博聞網
32. [幻想还是欺骗？复活冷冻人狂想曲](http://tech.icxo.com/htmlnews/2006/01/18/757286_2.htm),
    世界科技报道
33. [不是蛮干就能实现](http://tech.qq.com/a/20060213/000278.htm), 腾讯科技
34. [“人體冷凍術”能否讓人起死回生？](http://popul.jqcq.com/focus/2006-02/1140746482.html)
    , 金桥科普
35. [人体冷冻目前无任何医学价值](http://www.sh-yaoxue.com/article_list.asp?id=652) ,
    上海市藥學會
36.
37.
38. [The Modern Mummification Cryonics: Search for Immortality
    Continues](http://www.fountainmagazine.com/article.php?ARTICLEID=718)
    , The Fountain Magazine
39. [Legal Protection of Cryonics Patients,
    Part 1](http://www.evidencebasedcryonics.org/2012/02/23/legal-protection-of-cryonics-patients-part-1/),INSTITUTE
    FOR EVIDENCE BASED CRYONICS
40. [Legal Protection of Cryonics Patients,
    Part 2](http://www.evidencebasedcryonics.org/2012/05/05/legal-protection-of-cryonics-patients-part-2/),INSTITUTE
    FOR EVIDENCE BASED CRYONICS
41. [The Legal Status of Cryonics
    Patients](http://www.alcor.org/Library/html/legalstatus.html),阿爾科生命延續基金會
42. [CRYONIC SUSPENSION
    AGREEMENT](http://www.cryonics.org/documents/CS_Agreement.html)
    ,Cryonics Institute
43. [Cryopreservation
    Agreement](http://www.alcor.org/Library/pdfs/signup-CryopreservationAgreement.pdf),
    Alcor Life Extension Foundation
44. [Greenland Burial
    Plots](http://www.cryonet.org/cgi-bin/dsp.cgi?msg=26166),CryoNet
45. [Nature's cryonics
    (IV)](http://www.cryonet.org/cgi-bin/dsp.cgi?msg=4752), CryoNet
46. [Is Permafrost Burial a Viable
    Option?](http://www.cryonet.org/cgi-bin/dsp.cgi?msg=7733), CryoNet