**位元堂藥廠有限公司**是[香港一家](../Page/香港.md "wikilink")[中藥廠](../Page/中藥.md "wikilink")，生產、銷售中成藥及保健產品。在市區上有商場及啇鋪買入。位元堂藥廠有限公司的母公司是「位元堂藥業控股有限公司」
（），它於1997年10月31日在[香港交易所](../Page/香港交易所.md "wikilink")（以[得利集團名義](../Page/得利集團.md "wikilink")）上市。

## 歷史

[WaiYuenTong_ShamShuiPo.jpg](https://zh.wikipedia.org/wiki/File:WaiYuenTong_ShamShuiPo.jpg "fig:WaiYuenTong_ShamShuiPo.jpg")
[Wai_Yuen_Tong_at_Olympian_City_2,_Hong_Kong.JPG](https://zh.wikipedia.org/wiki/File:Wai_Yuen_Tong_at_Olympian_City_2,_Hong_Kong.JPG "fig:Wai_Yuen_Tong_at_Olympian_City_2,_Hong_Kong.JPG")2期的分店\]\]
位元堂創立於[清](../Page/清.md "wikilink")[光緒](../Page/光緒.md "wikilink")23年（1897年），由黎昌厚及多位官宦家族合夥人創辦，老舖位於[廣州](../Page/廣州.md "wikilink")[西關太平南路](../Page/西關.md "wikilink")（今[人民南路](../Page/人民路_\(廣州\).md "wikilink")）旁的漿欄街（現漿欄路）44號。1930年在香港[旺角](../Page/旺角.md "wikilink")[荔枝角道開設分店](../Page/荔枝角道.md "wikilink")。中共建政後的1952年，位元堂把總部從廣州遷至香港，避開了令眾多廣州藥號被當局充公的「[公私合營](../Page/公私合營.md "wikilink")」。1979年正式在香港註冊[商標](../Page/商標.md "wikilink")。2001年被[宏安以](../Page/宏安集團有限公司.md "wikilink")1億2,600萬港元收購。2002年8月，獲正式注入「位元堂藥業控股有限公司」。

## 發展

位元堂位於[九龍灣宏光道的廠房](../Page/九龍灣.md "wikilink")，佔地逾12萬[平方呎](../Page/平方呎.md "wikilink")，集自動化製藥工場、藥材加工中心、產品及藥材物流中心、低溫藥物倉庫房於一體，確保產品的安全。由2001年開始，位元堂以[特許經營方式將其零售分店數目增加](../Page/特許經營.md "wikilink")，擴闊服務覆蓋地域及加速滲透市場。除了分別位於[佐敦](../Page/佐敦.md "wikilink")、[油麻地及](../Page/油麻地.md "wikilink")[旺角](../Page/旺角.md "wikilink")[荔枝角道的](../Page/荔枝角道.md "wikilink")
3間旗艦店外，門市地點分佈在[銅鑼灣](../Page/銅鑼灣.md "wikilink")、[元朗](../Page/元朗.md "wikilink")、[上水](../Page/上水.md "wikilink")、[荃灣](../Page/荃灣.md "wikilink")、[觀塘](../Page/觀塘.md "wikilink")、[沙田及](../Page/沙田.md "wikilink")[黃大仙各區](../Page/黃大仙.md "wikilink")，遍及[香港](../Page/香港.md "wikilink")、[九龍和](../Page/九龍.md "wikilink")[新界](../Page/新界.md "wikilink")。
時至今日位元堂已成現代化的醫療保健集團，除了在香港經營50多家中醫中藥店，更在中國內地、美加、澳洲、新加坡、馬來西亞及其他東南亞地區銷售藥物及保健產品，發展多元化國際業務。

2013年12月2日美麗寶時裝老闆麥志光剛斥資約6,000萬元向位元堂買入九龍彌敦道510號地舖，此舖位面積約600方呎，一直由位元堂自用

## 推廣

位元堂最為人熟悉的產品是「位元堂扶正養陰丸」，1980年代以[鄭少秋演出之](../Page/鄭少秋.md "wikilink")[電視](../Page/電視.md "wikilink")[廣告](../Page/廣告.md "wikilink")，以歌舞配以廣告歌「太陽出來了」；2001年以[李克勤重新拍攝此電視廣告](../Page/李克勤.md "wikilink")。2005年仙草靈芝孢子則找來[古巨基擔任產品代言人](../Page/古巨基.md "wikilink")。2015年[鄭欣宜重新拍攝此電視廣告](../Page/鄭欣宜.md "wikilink")。

2009年與[寶潔旗下品牌](../Page/寶潔.md "wikilink")[飄柔](../Page/飄柔.md "wikilink")（Rejoice）夥拍推出漢方防掉髮系列洗頭水。

## 產品

  - 扶正養陰丸
  - 燕窩白鳳丸
  - 珠珀保嬰丹
  - 猴棗除痰散
  - 靈芝孢子
  - 至尊燕窩系列
  - 冬蟲草系列

## 榮譽

  - 2001年位元堂獲選為[香港名牌](../Page/香港名牌.md "wikilink")
  - 2002年則取得[香港超級品牌獎項](../Page/香港超級品牌.md "wikilink")
  - 2004年獲[廣州日報頒發第一屆](../Page/廣州日報.md "wikilink")「香港優質誠信商號」獎項
  - 2006年同時獲得[香港中醫藥管理委員會及](../Page/香港中醫藥管理委員會.md "wikilink")[澳洲藥品管理局](../Page/澳洲藥品管理局.md "wikilink")（TGA）頒發[GMP](../Page/GMP.md "wikilink")（中成藥生產質量管理規範）國際權威的認證
  - 2006年獲廣州日報主辦明報協辦第三屆港澳優質誠信商號
  - 2007年獲廣州日報主辦明報協辦第四屆港澳優質誠信商號
  - 2007年獲得資本雜誌香港傑出連鎖店
  - 2007年同時獲得第十三屆十大電視廣告頒獎典禮最受歡迎電視廣告男演員大獎
  - 2007年由[明報及](../Page/明報.md "wikilink")[香港中文大學主辦第一屆香港驕傲企業品牌選舉](../Page/香港中文大學.md "wikilink")
  - 2007年Design for Asia Award
  - 2008年[第十四屆十大電視廣告頒獎典禮最佳電視廣告動作場面大獎](../Page/第十四屆十大電視廣告頒獎典禮.md "wikilink")
  - 2008年廣州日報主辦明報協辦第五屆港澳優質誠信商號
  - 2008年由資本企業家主辦的首屆「香港品牌大賞2008」
  - 2008年由[經濟日報集團](../Page/經濟日報.md "wikilink")《Take me
    Home‧生活區報》首度舉辦之「香港家庭最愛品牌大賞08-09」，位元堂榮獲香港家庭最愛中式保健湯包大獎
  - 2009年由資本雜誌主辦第九屆資本傑出企業成就獎最佳生物科技公司
  - 2009年由[讀者文摘舉辦信譽品牌](../Page/讀者文摘.md "wikilink")2009中藥保健產品金獎
  - 2009年[TVB周刊頒發傑出企業形象大獎](../Page/TVB周刊.md "wikilink")2009
  - 2010年香港中藥業協會。[TVB周刊合辦](../Page/TVB周刊.md "wikilink")2010至愛優質中藥品牌大獎
  - 2010年由[經濟日報集團](../Page/經濟日報.md "wikilink")《Take me
    Home‧生活區報》舉辦之「香港家庭最愛品牌大賞09-10，位元堂榮獲香港家庭最愛中藥材及保健產品專門店
  - 2011年由[明報及](../Page/明報.md "wikilink")[香港中文大學主辦香港驕傲企業品牌](../Page/香港中文大學.md "wikilink")
  - 2011年卓越品牌大獎
  - 2011年香港中藥業協會。[TVB周刊合辦](../Page/TVB周刊.md "wikilink")2011優質品牌大獎
  - 2011年[東周刊主辦香港服務大獎](../Page/東周刊.md "wikilink")2011

## 參考資料

## 外部連結

  - [位元堂網站](http://www.wyt.com.hk/)
  - [位元堂藥業控股網站](http://www.wyth.net/)
  - [位元堂鄧梅芬從街市到商場](http://www.xpweekly.com/share/xpweekly/0365/finance/20050818finance0104/content.htm)
    [快周刊](../Page/快周刊.md "wikilink")

[Category:香港製藥公司](../Category/香港製藥公司.md "wikilink")
[Category:中成藥生產商](../Category/中成藥生產商.md "wikilink")
[Category:香港名牌](../Category/香港名牌.md "wikilink")
[Category:再次上市公司企業](../Category/再次上市公司企業.md "wikilink")
[Category:1897年建立](../Category/1897年建立.md "wikilink")
[Category:家族式企業](../Category/家族式企業.md "wikilink")
[Category:广州历史](../Category/广州历史.md "wikilink")
[Category:宏安集團](../Category/宏安集團.md "wikilink")