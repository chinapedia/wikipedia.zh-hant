**西拉法葉**（[英语](../Page/英语.md "wikilink")：****）是位於[美國](../Page/美國.md "wikilink")[印第安那州的一個城市](../Page/印第安那.md "wikilink")，在[印第安納波利斯西北方約](../Page/印第安納波利斯.md "wikilink")65英里（105公里）處，在[芝加哥东南方約](../Page/芝加哥.md "wikilink")106英里（171公里）處。2010年普查時人口為29,596人，隔[沃巴什河](../Page/沃巴什河.md "wikilink")\[1\]與[拉法葉相望](../Page/拉斐特_\(印第安纳州\).md "wikilink")，是[普渡大學主校區所在地](../Page/普渡大學.md "wikilink")。

## 歷史

[Westlafayette_indiana.jpg](https://zh.wikipedia.org/wiki/File:Westlafayette_indiana.jpg "fig:Westlafayette_indiana.jpg")
西拉法葉最先在1836年時由奧古斯特斯·懷里（Augustus
Wylie）於[沃巴什河畔洪水區南麓的的李維區](../Page/沃巴什河.md "wikilink")（Leeve）開始發跡。由於經常性淹水的影響，懷里的鎮從未真正的設立過。目前西拉法葉城鎮的規模是由鄰近的幾個地區，包括瓊西區（Chauncey）、橡樹區（Oakwood）及金士頓區（Kingston，在沃巴什河另一側的拉法葉市旁）所合併\[2\]。這三個鎮原本是三個相鄰的小型村落，金士頓區是1855年由傑西·拉茲（Jesse
B.
Lutz）所設立，瓊西區則是由來自[費城從事土地投資的富豪瓊西家族在](../Page/費城.md "wikilink")1860年時設立。瓊西區和金士頓區在1866年成立市政府並選定瓊西為新的城鎮名\[3\]

直到1869年普渡大學的成立之前，這個新市鎮初期依舊只是個郊區的小村落。1871年時，由於無法提供完善的城市公共設施，諸如良好的[街道](../Page/街道.md "wikilink")、供[水](../Page/水.md "wikilink")、[警察及](../Page/警察.md "wikilink")[消防系統](../Page/消防.md "wikilink")，於是瓊西鎮投票表決併入拉法葉市。但由於改善瓊西鎮缺少的公共設施所需的花費太過高昂，拉法葉市隨即表決反對併入瓊西鎮
\[4\]。1888年西拉法葉市正式成立時，普渡大學的成長已足以帶動這個小鎮的成長。普渡大學的地址也在20世紀之後由拉法葉轉成西拉法葉。西拉法葉從來沒有設立過鐵路運輸的車站，並且在建設及公共設施上落後拉法葉數年，但在1940年時，西拉法葉已經成為一個獨立的城市。

## 地理

西拉法葉座落於 (40.441935,
-86.912409)的沃巴什河畔，境內高度變化由500呎到北部靠近52號公路的720呎不等。根據[美國人口調查局的資料](../Page/美國人口調查局.md "wikilink")，西拉法葉的面積為14.3平方公里（5.5平方英里）。其中陸地幾乎佔有所有的14.3平方公里，水域僅有0.18%。

### 氣候

西拉法葉所屬的[印第安那州中部](../Page/印第安那州.md "wikilink")，受[大湖效應影響](../Page/大湖效應.md "wikilink")，冬季嚴寒常有降雪，夏季則普遍熾熱，春季與秋季為最舒適的天氣。由於[美國中西部地形及氣候關係](../Page/美國中西部.md "wikilink")，較常見的天然災害有[龍捲風](../Page/龍捲風.md "wikilink")、[暴風雪](../Page/暴風雪.md "wikilink")、[雷陣雨等](../Page/雷陣雨.md "wikilink")。

## 社群

根據2000年的普查，西拉法葉人口共有28,778人及10,462戶，人口密度為每平方公里2,016.6人（每平方哩5,219.6人）。而人口中有83.4%的[白人](../Page/白人.md "wikilink")，11.34%的[亞洲人](../Page/亞洲.md "wikilink")，2.38%的黑人，0.16%的美國[原住民](../Page/原住民.md "wikilink")，0.03%的[太平洋島民](../Page/太平洋.md "wikilink")，3.2%的[拉丁美洲裔](../Page/拉丁美洲.md "wikilink")，1.17%的其他種族，以及1.58%的多族裔\[5\]。

10,462戶中有14.9%為有18歳以下小孩同住的家庭，27.6%是只有夫妻同住的家庭，4.4%是妻子獨居的家庭，65.7%為非家庭戶。其中10,462戶中有32.7%為獨居戶而7.3%為65歳以上的老人獨居戶。平均每戶成員為2.26人，每個家庭成員平均為2.89人。

居民年齡分布為10.4%為18歳以下，54.6%的居民為18歳至24歳，16.9%為25歳至44歳，10.3%為45歳至64歳，7.7%為65歲以上。年齡中位數為22歳。居民年齡分布充分反應普渡大學學生在這個城市中的活動情形。另外男女比為1.335:1，而18歳以上的男女比為1.372:1，這種不尋常的男女比也可以由普渡大學所提供的科系較多為由男性學生為主的理工科系來解釋。

### 居民收入

西拉法葉每戶年收入的[中位數為](../Page/中位數.md "wikilink")24,869[美元](../Page/美元.md "wikilink")，而家庭年收入的中位數則為71,510美元。男性收入中位數為46,787美元，女性收入中位數為30,218美元，其中大約有9.5%的家庭及38.3%的人口收入低於[貧窮線](../Page/貧窮線.md "wikilink")，但是貧窮線的數據並不能準確反映西拉法葉這種以學生為主的居民貧富。

## 教育

[Purdue_EngineeringMall.jpg](https://zh.wikipedia.org/wiki/File:Purdue_EngineeringMall.jpg "fig:Purdue_EngineeringMall.jpg")
西拉法葉是普渡大學主校區的所在，目前普渡大學的學生約有40,000人。

而西拉法葉的基礎教育包括三個公立學校，分別是坎柏蘭[小學](../Page/小學.md "wikilink")（Cumberland
Elementary School，一到三年級）、快樂山谷小學（Happy Hollow Elementary
School，四到六年級）以及西拉法葉[中學](../Page/中學.md "wikilink")（West
Lafayette Junior-Senior High School，七年級以上）\[6\]。

另外，一些教堂或一些私人機構也有成立其他學校，像是Seventh-day Adventist教堂的Pleasantview
SDA學校提供八年級的課程，大拉法葉地區的[蒙特梭利](../Page/蒙特梭利.md "wikilink")（Montessori）學校有六年級的課程。除此之外，還有一些公立和私人的學校雖然位在西拉法葉地區之外但仍以西拉法葉的地址提供教育課程。

## 政府組織

西拉法葉的政府包括[市長](../Page/市長.md "wikilink")、財政官及一個七個成員所組成的[議會](../Page/議會.md "wikilink")。而市長是最高行政長官議會及公共建設及安全委員會的主席，財政官則是掌管全市及法院的財政。

西拉法葉主要分為五個行政區，每個行政區中選出一名[議員](../Page/議員.md "wikilink")，另外兩名則是不分區議員。議會主要工作則是制定法律、命令及發展方向，議會議長也是市長在無法完成任期遞補的第一順位，並在市長無法出席議會時代理市長。

每一個經由[選舉產生的公職人員均為](../Page/選舉.md "wikilink")4年一任，但連選得連任。所有公職人員的選舉都是在[總統大選後的](../Page/總統.md "wikilink")[奇數年時一起舉行](../Page/奇數.md "wikilink")\[7\]。

## 經濟

[Vistech_1_Building_2005.jpg](https://zh.wikipedia.org/wiki/File:Vistech_1_Building_2005.jpg "fig:Vistech_1_Building_2005.jpg")
西拉法葉的經濟受普渡大學的影響甚大，主要原因是普渡大學學生人數將近40000人，比西拉法葉的人口數還多，且普渡大學提供了拉法葉與西拉法葉地區約12000個工作機會。2006年6月，統計資料顯示拉法葉都會區（包括拉法葉及西拉法葉）的失業率為4.5%\[8\]。

而1961年成立，佔地591公頃的[普渡研究園區中有超過](../Page/普渡研究園區.md "wikilink")140個高科技公司，並約有2500個員工。主要工作為研發通訊設備以及其他世界級的研究，園區內並有許多廠房及辦公室提供新設立的公司創業的協助。普渡研究園區也是全美國以大學名義成立的最大型研究園區之一，是由[普渡研究基金會管理](../Page/普渡研究基金會.md "wikilink")。位於普渡研究園區內的公司包括有：

  - [Arxan科技公司](http://www.arxan.com/)
  - [C-SPAN](../Page/C-SPAN.md "wikilink") archives
  - [Cook生物科技](https://web.archive.org/web/20071208111532/http://www.cookgroup.com/profile/med-mfg/biotech.html)
  - [Endocyte生物科技](http://www.endocyte.com/)

商業活動主要是在拉法葉市中。由於緊鄰通往拉法葉地區的[橋樑](../Page/橋.md "wikilink")，佔地約90公頃的李維區（Levee）也成為西拉法葉最大的商業區。區內最主要的購物區[Wabash
Landing](http://www.city.west-lafayette.in.us/wabashlanding/index.htm)有各種店舖、[餐廳](../Page/餐廳.md "wikilink")、[咖啡店](../Page/咖啡.md "wikilink")、[電影院](../Page/電影院.md "wikilink")、[旅館及](../Page/旅館.md "wikilink")[溜冰場](../Page/溜冰.md "wikilink")。另外一個商業區則是位於[市政府周圍靠近Sagamore](../Page/市政府.md "wikilink")
Parkway的商圈，主要的雜貨、食品、[藥房以及連鎖餐廳都可以在這裡找到](../Page/藥房.md "wikilink")。

## 交通

### 機場

  - [普度大學機場](../Page/普度大學.md "wikilink")：目前沒有商業航班。

### 公路

  - [I-65.svg](https://zh.wikipedia.org/wiki/File:I-65.svg "fig:I-65.svg")[州際公路I](../Page/州際公路.md "wikilink")-65，連接[芝加哥近郊與](../Page/芝加哥.md "wikilink")[印第安納波利斯的主要道路](../Page/印第安納波利斯.md "wikilink")。
  - [US_52.svg](https://zh.wikipedia.org/wiki/File:US_52.svg "fig:US_52.svg")[美國國道US](../Page/美國國道.md "wikilink")
    52，連接[芝加哥近郊與](../Page/芝加哥.md "wikilink")[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")。
  - [US_231.svg](https://zh.wikipedia.org/wiki/File:US_231.svg "fig:US_231.svg")[美國國道US](../Page/美國國道.md "wikilink")
    231，連接[芝加哥近郊與](../Page/芝加哥.md "wikilink")[肯塔基州](../Page/肯塔基州.md "wikilink")，亦為市區西側南北向重要連外道路。
  - [Indiana_26.svg](https://zh.wikipedia.org/wiki/File:Indiana_26.svg "fig:Indiana_26.svg")印第安納州州道SR
    26，連接[普度大學與](../Page/普度大學.md "wikilink")[科科莫的主要道路](../Page/科科莫_\(印第安納州\).md "wikilink")。往西可連接[Illinois_9.svg](https://zh.wikipedia.org/wiki/File:Illinois_9.svg "fig:Illinois_9.svg")伊利諾州9號州道，往東可連接[OH-119.svg](https://zh.wikipedia.org/wiki/File:OH-119.svg "fig:OH-119.svg")俄亥俄州119號州道。在[西拉法葉](../Page/西拉法葉.md "wikilink")，這條路命名為State
    St.，亦為市中心及[普度大學東西向重要連外道路](../Page/普度大學.md "wikilink")。
  - [Indiana_43.svg](https://zh.wikipedia.org/wiki/File:Indiana_43.svg "fig:Indiana_43.svg")印第安納州州道SR
    43，往北可銜接[US_421.svg](https://zh.wikipedia.org/wiki/File:US_421.svg "fig:US_421.svg")[美國國道US](../Page/美國國道.md "wikilink")
    421，可續行前往[密西根市](../Page/密西根市.md "wikilink")。在[西拉法葉](../Page/西拉法葉.md "wikilink")，這條路命名為River
    Rd.，亦為市區東側南北向重要連外道路，[I-65.svg](https://zh.wikipedia.org/wiki/File:I-65.svg "fig:I-65.svg")[州際公路I](../Page/州際公路.md "wikilink")-65於西拉法葉的交流道與此路交會。
  - Sagamore
    Parkway，為西拉法葉北部主要道路，原隸屬[US_52.svg](https://zh.wikipedia.org/wiki/File:US_52.svg "fig:US_52.svg")[美國國道US](../Page/美國國道.md "wikilink")
    52。
  - Northwestern Avenue ，為西拉法葉南北向主要道路，連接Sagamore
    Parkway與[普度大學](../Page/普度大學.md "wikilink")。原隸屬[US_231.svg](https://zh.wikipedia.org/wiki/File:US_231.svg "fig:US_231.svg")[美國國道US](../Page/美國國道.md "wikilink")
    231。

### 鐵路

  - Amtrak：位於僅一橋之隔的[拉法葉](../Page/拉斐特_\(印第安納州\).md "wikilink")。
      - [印第安那州號](../Page/印第安那州號.md "wikilink")：提供來回[芝加哥與](../Page/芝加哥.md "wikilink")[印第安納波利斯的每日班次](../Page/印第安納波利斯.md "wikilink")。
      - [紅雀號](../Page/紅雀號.md "wikilink")：來回[芝加哥與](../Page/芝加哥.md "wikilink")[紐約之間的每週固定班次](../Page/紐約.md "wikilink")。透過鐵路可由拉法葉直達[紐約](../Page/紐約.md "wikilink")、[費城](../Page/費城.md "wikilink")、[巴爾的摩](../Page/巴爾的摩.md "wikilink")、[華盛頓特區](../Page/華盛頓特區.md "wikilink")、[辛辛那提](../Page/辛辛那提.md "wikilink")、[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")、[芝加哥等大城市](../Page/芝加哥.md "wikilink")，或於[芝加哥轉乘至其他各州](../Page/芝加哥.md "wikilink")。

### 市內大眾運輸與聯外客運

  - CityBus：提供[拉法葉與](../Page/拉法葉.md "wikilink")[西拉法葉地區市內公共運輸服務](../Page/西拉法葉.md "wikilink")。可利用此大眾運輸服務至[拉法葉轉乘](../Page/拉斐特_\(印第安纳州\).md "wikilink")[Amtrak火車或長程客運](../Page/Amtrak.md "wikilink")[灰狗巴士](../Page/灰狗巴士.md "wikilink")，前往[芝加哥](../Page/芝加哥.md "wikilink")、[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")、[辛辛那提](../Page/辛辛那提.md "wikilink")、[亞特蘭大](../Page/亞特蘭大.md "wikilink")、[紐約等大城市](../Page/紐約.md "wikilink")。\[9\]
  - Express Air
    Coach：提供前往[芝加哥歐哈爾國際機場的機場快捷巴士](../Page/歐海爾國際機場.md "wikilink")，並提供周末來回[伊利諾伊大學厄巴納-香檳分校的快捷巴士](../Page/伊利諾伊大學厄巴納-香檳分校.md "wikilink")。\[10\]
  - Reindeer
    Shuttle：提供前往[印第安納波利斯國際機場與](../Page/印第安納波利斯國際機場.md "wikilink")[芝加哥歐哈爾國際機場的機場快捷巴士](../Page/歐海爾國際機場.md "wikilink")。\[11\]
  - Lafayette Limo：
    提供前往[印第安納波利斯國際機場與](../Page/印第安納波利斯國際機場.md "wikilink")[芝加哥歐哈爾國際機場的機場快捷巴士](../Page/歐海爾國際機場.md "wikilink")。\[12\]

## 文化活動

由於西拉法葉地區國際學生及教授眾多，因此國際文化活動也相當豐富。除以[普渡大學為主的校園活動之外](../Page/普渡大學.md "wikilink")，亦有不少地區性文化活動。另有眾多活動與[拉法葉合辦](../Page/拉斐特_\(印第安纳州\).md "wikilink")，例如Taste
of Tippecanoe、國慶煙火等。

  - Spring
    Fest：每年四月於[普渡大學校園舉行的春季展示](../Page/普渡大學.md "wikilink")，以農業相關科系為主，為一寓教於樂的活動。\[13\]
  - Global Fest：每年[勞動節前後的Global](../Page/勞動節.md "wikilink")
    Fest是夏天結束前市政府會舉辦的一個慶典\[14\]，當地來自不同國家的國際學生及居民均會參與。
  - Starry Night Festival：每年九月或十月舉行，為一充滿音樂與藝術的文化盛會。\[15\]
  - The Feast of the Hunters' Moon：紀念早年印地安人與法國人交流與土地開墾的歷史性活動，每年十月初於Fort
    Ouiatenon舉行。\[16\]

## 參考資料

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[West Lafayette](../Category/印第安纳州城市.md "wikilink") [West
Lafayette](../Category/蒂珀卡努縣城市_\(印第安納州\).md "wikilink")

1.  沃巴什河。[大英百科全書線上繁體中文版](http://tw.britannica.com/MiniSite/Article/id00064373.html)
2.
3.
4.
5.
6.
7.
8.
9.  [CityBus官方網站](http://www.gocitybus.com/)
10. [Express Air Coach官方網站](http://expressaircoach.com/)
11. [Reindeer Shuttle官方網站](http://reindeershuttle.com/)
12. [Lafayette Limo官方網站](http://www.lafayettelimo.com/)
13. [Spring
    Fest官方網站](https://ag.purdue.edu/springfest/Pages/default.aspx)
14. [Global
    Fest介紹](http://www.city.west-lafayette.in.us/community/globalfest/index.html)
15. [Starry Night Festival官方網站](http://starrynightfestival.com/)
16. [The Feast of the Hunters'
    Moon官方網站](http://feastofthehuntersmoon.org/)