**永享**是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")。在[正長之後](../Page/正長.md "wikilink")，[嘉吉之前](../Page/嘉吉.md "wikilink")。指1429年到1441年的期間。這個時代的[天皇是](../Page/天皇.md "wikilink")[後花園天皇](../Page/後花園天皇.md "wikilink")。[室町幕府的將軍是](../Page/室町幕府.md "wikilink")[足利義教](../Page/足利義教.md "wikilink")。

## 改元

  - 正長2年[9月5日](../Page/9月5日_\(陰曆\).md "wikilink")（[陽曆](../Page/陽曆.md "wikilink")1429年10月3日）
    因後花園天皇即位而改元
  - 永享13年[2月17日](../Page/2月17日_\(陰曆\).md "wikilink")（陽曆1441年3月10日） 改元為嘉吉

## 出典

出自『[後漢書](../Page/後漢書.md "wikilink")』的「能立魏魏之功、傳于子孫、**永享**無窮之祚」。

## 永享年間要事

  - 3年[大内盛見在](../Page/大内盛見.md "wikilink")[筑前國萩原受](../Page/筑前國.md "wikilink")[大友氏](../Page/大友氏.md "wikilink")、[少弐氏攻擊而戰死](../Page/少弐氏.md "wikilink")。
  - 4年
    幕府將軍[足利義教恢復](../Page/足利義教.md "wikilink")[日明貿易](../Page/日明貿易.md "wikilink")（勘合貿易）。
  - 4年10月[細川持之就任](../Page/細川持之.md "wikilink")[管領](../Page/管領.md "wikilink")。
  - 5年[永享山門騷動](../Page/永享山門騷動.md "wikilink")
  - 10年[8月16日](../Page/8月16日_\(陰曆\).md "wikilink")（1438年） -
    [永享之亂](../Page/永享之亂.md "wikilink")。鎌倉公方・足利持氏為攻擊關東管領・上杉憲實而出陣。
  - 11年[6月27日](../Page/6月27日_\(陰曆\).md "wikilink")（1439年） -
    [飛鳥井雅世撰寫](../Page/飛鳥井雅世.md "wikilink")『[新續古今和歌集](../Page/新續古今和歌集.md "wikilink")』，並上呈給天皇。
  - 12年[結城合戰](../Page/結城合戰.md "wikilink")
  - [土浦城築城](../Page/土浦城.md "wikilink")。

### 出生

  - 元年（1429年） -
    [日野勝光](../Page/日野勝光.md "wikilink")、貴族・日野富子之兄（[文明](../Page/文明_\(年號\).md "wikilink")8年逝世）
  - 2年（1430年） -
    [細川勝元](../Page/細川勝元.md "wikilink")、[武將](../Page/武將.md "wikilink")（文明5年逝世）
  - 4年（1432年） -
    [北條早雲](../Page/北條早雲.md "wikilink")、武將（[永正](../Page/永正.md "wikilink")16年逝世）
  - 6年[2月9日](../Page/2月9日_\(陰曆\).md "wikilink")（1434年3月19日） -
    [足利義勝](../Page/足利義勝.md "wikilink")、[室町幕府](../Page/室町幕府.md "wikilink")7代[將軍](../Page/征夷大將軍.md "wikilink")（[嘉吉](../Page/嘉吉.md "wikilink")3年逝世）
  - 6年（1434年） -
    [狩野正信](../Page/狩野正信.md "wikilink")、畫師（[享祿](../Page/享祿.md "wikilink")3年逝世）
  - 8年[1月2日](../Page/1月2日_\(陰曆\).md "wikilink")（1436年1月20日） -
    [足利義政](../Page/足利義政.md "wikilink")、室町幕府8代將軍（[延德](../Page/延德.md "wikilink")2年逝世）
  - 9年（1437年） - [畠山義就](../Page/畠山義就.md "wikilink")、武將（延德2年逝世）
  - 11年[閏1月18日](../Page/1月18日_\(陰曆\).md "wikilink")（1439年3月3日） -
    [足利義視](../Page/足利義視.md "wikilink")、足利義政之弟（延德3年逝世）
  - 12年（1440年） -
    [日野富子](../Page/日野富子.md "wikilink")、足利義政的正室・[足利義尚之母](../Page/足利義尚.md "wikilink")（[明應](../Page/明應.md "wikilink")5年逝世）

### 逝世

  - 3年 大内盛見（享年55）
  - 5年[10月20日](../Page/10月20日_\(陰曆\).md "wikilink")（1433年） -
    [後小松天皇](../Page/後小松天皇.md "wikilink")、100代・北朝6代[天皇](../Page/天皇.md "wikilink")（南朝[天授](../Page/天授_\(日本\).md "wikilink")3年/北朝[永和](../Page/永和_\(日本\).md "wikilink")3年出生）
  - 5年[大内持盛](../Page/大内持盛.md "wikilink")（享年37）、[畠山滿家](../Page/畠山滿家.md "wikilink")（享壽62）、[斯波義淳](../Page/斯波義淳.md "wikilink")（享年37）
  - 6年[日野義資](../Page/日野義資.md "wikilink")（享年38）
  - 7年[6月13日](../Page/6月13日_\(陰曆\).md "wikilink")（1435年） -
    [滿濟](../Page/滿濟.md "wikilink")、[真言宗的僧侶](../Page/真言宗.md "wikilink")「黑衣宰相」（南朝天授4年/北朝永和4年出生）
  - 7年[山名時熙](../Page/山名時熙.md "wikilink")（享壽69）
  - 9年[惟肖得巖](../Page/惟肖得巖.md "wikilink")（享壽78）
  - 11年 足利持氏（享年42）

## 西元對照表

| 永享                             | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             | 6年                             | 7年                             | 8年                             | 9年                             | 10年                            |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西曆                             | 1429年                          | 1430年                          | 1431年                          | 1432年                          | 1433年                          | 1434年                          | 1435年                          | 1436年                          | 1437年                          | 1438年                          |
| [干支](../Page/干支.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") |
| 永享                             | 11年                            | 12年                            | 13年                            |                                |                                |                                |                                |                                |                                |                                |
| 西曆                             | 1439年                          | 1440年                          | 1441年                          |                                |                                |                                |                                |                                |                                |                                |
| 干支                             | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") |                                |                                |                                |                                |                                |                                |                                |

## 相關項目

[Category:15世纪日本年号](../Category/15世纪日本年号.md "wikilink")
[Category:1420年代日本](../Category/1420年代日本.md "wikilink")
[Category:1430年代日本](../Category/1430年代日本.md "wikilink")
[Category:1440年代日本](../Category/1440年代日本.md "wikilink")