**池田恒興**（）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[安土桃山時代武將](../Page/安土桃山時代.md "wikilink")。父親是[池田恆利](../Page/池田恆利.md "wikilink")。母親[養德院是](../Page/養德院.md "wikilink")[織田信長的](../Page/織田信長.md "wikilink")[乳母](../Page/乳母.md "wikilink")。[尾張](../Page/尾張.md "wikilink")[犬山城城主](../Page/犬山城.md "wikilink")、[攝津](../Page/攝津.md "wikilink")[兵庫城城主](../Page/兵庫城.md "wikilink")、[美濃](../Page/美濃國.md "wikilink")[大垣城城主](../Page/大垣城.md "wikilink")。

[清洲会議中的四](../Page/清洲会議.md "wikilink")[宿老之一](../Page/宿老.md "wikilink")。有軍記物記載名字為**信輝**，但是沒有可信性高的同時代史料支持。

## 生平

出身地有尾張、美濃、攝津、[近江等諸多説法](../Page/近江.md "wikilink")。

父親[恆利很早就過世](../Page/池田恆利.md "wikilink")，母親則成為[織田信長的乳母](../Page/織田信長.md "wikilink")，後來又成為[織田信秀的側室](../Page/織田信秀.md "wikilink")，因此恆興跟信長自幼一起長大，日後成為信長非常信賴的家臣。年幼時就以[小姓身份仕於](../Page/小姓.md "wikilink")[織田家](../Page/織田家.md "wikilink")，參與[桶狹間之戰](../Page/桶狹間之戰.md "wikilink")、攻略美濃等戰鬥。在[元龜元年](../Page/元龜.md "wikilink")（1570年）的[姉川之戰中非常活躍](../Page/姉川之戰.md "wikilink")，因為立下戰功而被賜予[犬山城](../Page/犬山城.md "wikilink")1萬[貫](../Page/貫.md "wikilink")。以後亦參與火攻[比叡山](../Page/比叡山.md "wikilink")、[長島一向一揆](../Page/長島一向一揆.md "wikilink")、[長篠之戰等戰鬥](../Page/長篠之戰.md "wikilink")，在天正8年（1580年）擊破在攝津[花隈城籠城的](../Page/花隈城.md "wikilink")[荒木村重後](../Page/荒木村重.md "wikilink")，拜領村重原本的領地（[花隈城之戰](../Page/花隈城之戰.md "wikilink")）。

[天正](../Page/天正_\(日本\).md "wikilink")10年（1582年），[甲州征伐後](../Page/甲州征伐.md "wikilink")[武田氏滅亡](../Page/武田氏.md "wikilink")，秘密匿藏並保護[武田勝賴的三男](../Page/武田勝賴.md "wikilink")[勝親](../Page/武田勝親.md "wikilink")。同年，信長在[本能寺之變中被家臣](../Page/本能寺之變.md "wikilink")[明智光秀殺死](../Page/明智光秀.md "wikilink")，於是與自中國返回的[羽柴秀吉會師](../Page/羽柴秀吉.md "wikilink")，於[山崎之戰中擔任右翼先鋒並擊破光秀](../Page/山崎之戰.md "wikilink")，被列為織田家的[宿老](../Page/宿老.md "wikilink")。

在[清洲會議中與](../Page/清洲會議.md "wikilink")[柴田勝家等人對抗](../Page/柴田勝家.md "wikilink")，與秀吉、[丹羽長秀一同擁立信長的嫡孫三法師](../Page/丹羽長秀.md "wikilink")（[織田秀信](../Page/織田秀信.md "wikilink")），在領地再分配後領有攝津的內大坂、尼崎、兵庫12萬石。參與翌年（1583年）的[賤岳之戰](../Page/賤岳之戰.md "wikilink")，拜領美濃13萬石並成為[大垣城城主](../Page/大垣城.md "wikilink")。

[thumb](../Page/file:Shōnyū-zuka.jpg.md "wikilink")[長久手市](../Page/長久手市.md "wikilink")）\]\]
在天正12年（1584年）爆發的[小牧長久手之戰中代表秀吉方參戰](../Page/小牧長久手之戰.md "wikilink")。在前哨戰中攻略犬山城後，於途中進入[上條城](../Page/上條城.md "wikilink")，與[三好信吉](../Page/三好信吉.md "wikilink")、女婿[森長可](../Page/森長可.md "wikilink")、[堀秀政一同往](../Page/堀秀政.md "wikilink")[三河方向攻擊](../Page/三河.md "wikilink")，但是在合戰的前半段時，鞍被銃彈打中而落馬受傷，但還是撐著受傷的身體繼續行軍，在長久手與森長可和自己的長男[池田元助一同戰死](../Page/池田元助.md "wikilink")，享年49歲。因為與嫡男[元助一同被討死](../Page/池田元助.md "wikilink")，於是池田家的[家督由次男](../Page/家督.md "wikilink")[輝政繼承](../Page/池田輝政.md "wikilink")。

[thumb](../Page/file:Grave_of_Ikeda_Tsuneoki_in_Ikeda.jpg.md "wikilink")[揖斐郡](../Page/揖斐郡.md "wikilink")[池田町本鄉](../Page/池田町_\(岐阜縣\).md "wikilink")）\]\]
遺體一時間被葬在[遠江新居](../Page/遠江.md "wikilink")，後來改葬至[京都](../Page/京都.md "wikilink")[妙心寺的](../Page/妙心寺.md "wikilink")[護國院](../Page/護國院.md "wikilink")。

## 相關條目

  - [池田氏](../Page/池田氏.md "wikilink")

[Category:美濃池田氏](../Category/美濃池田氏.md "wikilink")
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:1536年出生](../Category/1536年出生.md "wikilink")
[Category:1584年逝世](../Category/1584年逝世.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")