**野島裕史**，是出身於日本[東京都的男性](../Page/東京都.md "wikilink")[聲優](../Page/聲優.md "wikilink")，所屬事務所為[青二Production](../Page/青二Production.md "wikilink")。

## 人物簡介

  - 出身於[聲優世家](../Page/聲優.md "wikilink")。父親是[野島昭生](../Page/野島昭生.md "wikilink")、弟弟是[野島健兒](../Page/野島健兒.md "wikilink")。
  - 坦言小时候经常和二弟野岛健儿打架、吵架，長大之後就好多了。
  - 兴趣爱好，自行车，在事务所的官网sample里说道“喝着啤酒，远远地欣赏自己的自行车是人生幸福的瞬间。”
  - 海外配音作品中，以亞洲地區的電視劇配音比較多，同時也是韓國演員宋仲基的常任配音員。

## 配音作品

  - **粗體字**為主要角色

### 電視動畫

  - 2002年

<!-- end list -->

  - [翼神世音](../Page/翼神世音.md "wikilink")（鳥飼守）※**出道作**
  - [戰鬥陀螺](../Page/戰鬥陀螺.md "wikilink")（金）
  - [推理之絆](../Page/推理之絆.md "wikilink")（今里）
  - [天使特警](../Page/天使特警.md "wikilink")（路諾(中學生)）
  - [帝皇戰紀](../Page/帝皇戰紀.md "wikilink")（**基拿·辛格**）
  - [陸上防衛隊](../Page/陸上防衛隊.md "wikilink")（素敵一郎）
  - [十二國記](../Page/十二國記.md "wikilink")（塙王的太子）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [灌籃少年](../Page/灌籃少年.md "wikilink")（澤登聖人）

  - [魔偵探洛基RAGNAROK](../Page/魔偵探洛基.md "wikilink")（芬里爾）

  - [洛克人EXE AXESS](../Page/洛克人.md "wikilink")（、、其他）

  - Nitro（**阿久澤由宇**）

  - SUBMARINE SUPER 99（水兵、通信長）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [美鳥伴身邊](../Page/美鳥伴身邊.md "wikilink")（宮原修）

  - [犬夜叉](../Page/犬夜叉.md "wikilink")（弟子）

  - [詩∽片](../Page/詩∽片.md "wikilink")（足立）

  - [SD 鋼彈 FORCE](../Page/SD鋼彈.md "wikilink")（機獸丸、、、其他）

  - [陰陽大戰記](../Page/陰陽大戰記.md "wikilink")（）

  - [學園愛麗絲](../Page/學園愛麗絲.md "wikilink")（副班主任老師）

  - [現視研](../Page/現視研.md "wikilink")（御宅3、原男友）

  - [攻殼機動隊 S.A.C. 2nd
    GIG](../Page/攻殼機動隊_S.A.C._2nd_GIG.md "wikilink")（駕駛員）

  - [老師的時間](../Page/老師的時間.md "wikilink")（紳士）

  - （長井老師、其他）

  - [ToHeart Remember my memories](../Page/To_Heart.md "wikilink")（長瀨祐介）

  - （忍者4）

  - [馳風！競艇王](../Page/馳風！競艇王.md "wikilink")（松永秀明）

  - [洛克人EXE Stream](../Page/洛克人.md "wikilink")（配送員、、職員A、社員B、警員B、其他）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [高機動交響曲](../Page/高機動交響曲.md "wikilink")（**小島航**）
  - [ARIA The ANIMATION](../Page/ARIA_The_ANIMATION.md "wikilink")（出雲曉）
  - [植木的法則](../Page/植木的法則.md "wikilink")（黒岩）
  - [艾瑪 第一幕](../Page/艾瑪.md "wikilink")（羅伯特·哈爾福特）
  - [天使心](../Page/天使心.md "wikilink")（李堅強(青年時代)）
  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（**池速人**）
  - [增血鬼果林](../Page/增血鬼果林.md "wikilink")（布奇、茱莉安店長）
  - [聖魔之血](../Page/聖魔之血.md "wikilink")（\#24 該隱・奈特羅德(少年時期)）
  - [蜂蜜幸運草](../Page/蜂蜜幸運草.md "wikilink")（長谷川一彥）
  - [雙戀II](../Page/雙戀.md "wikilink")（三木公彥）
  - [棒球大聯盟2nd](../Page/棒球大聯盟.md "wikilink")（澤村涼太）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [數碼寶貝拯救隊](../Page/數碼寶貝拯救者.md "wikilink")（**托瑪·Ｈ·諾史丁**）

  - [極上生徒會](../Page/極上生徒會.md "wikilink")（金城史郎）

  - [ARIA The NATURAL](../Page/ARIA_The_NATURAL.md "wikilink")（出雲曉）

  - [犬神](../Page/犬神.md "wikilink")（川平宗太郎）

  - [傳頌之物](../Page/傳頌之物.md "wikilink")（希恩）

  - [地獄少女 二籠](../Page/地獄少女.md "wikilink")（翔貴(黑部一郎)）

  - （遠射）

  - [不平衡抽籤](../Page/不平衡抽籤.md "wikilink")（織部亞彥）

  - [死亡代理人](../Page/死亡代理人.md "wikilink")（研究員）

  - [完美小姐進化論](../Page/完美小姐進化論.md "wikilink")（**森井蘭丸**）

  - [天保異聞 妖奇士](../Page/天保異聞_妖奇士.md "wikilink")（嚴見）

  - [學園天堂BOY'S LOVE HYPER\!](../Page/學園天堂.md "wikilink")（岩井卓人）

  - [妖怪人間貝姆](../Page/妖怪人間貝姆.md "wikilink")（田無幻狼齋）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [羅密歐與茱麗葉](../Page/羅密歐與茱麗葉_\(電視動畫\).md "wikilink")（**弗朗西斯科**）
  - [彩雲國物語 第二系列](../Page/彩雲國物語.md "wikilink")（歐陽純）
  - [死亡筆記本](../Page/死亡筆記本.md "wikilink")（奈南川零司）
  - [艾瑪 第二幕](../Page/艾瑪.md "wikilink")（羅伯特·哈爾福特）
  - [精靈守護者](../Page/精靈守護者.md "wikilink")（修伽）
  - [七色★Drops](../Page/七色★Drops.md "wikilink")（**石蕗正晴**）
  - [BLUE DRAGON](../Page/藍龍.md "wikilink")（）
  - [惡魔獵人系列](../Page/惡魔獵人系列.md "wikilink")（）
  - [灼眼的夏娜II](../Page/灼眼的夏娜.md "wikilink")（**池速人**）
  - [蟲之歌](../Page/蟲之歌.md "wikilink")（白井）
  - [REIDEEN](../Page/REIDEEN.md "wikilink")（）
  - [Deltora Quest](../Page/Deltora_Quest.md "wikilink")（）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [ARIA The ORIGINATION](../Page/水星領航員.md "wikilink")（出雲曉）

  - [閃電十一人](../Page/閃電十一人.md "wikilink")（**豪炎寺修也**）

  - [吸血鬼騎士 Guilty](../Page/吸血鬼騎士.md "wikilink")（玖蘭悠）

  - （****）

  - [武裝機甲](../Page/武裝機甲.md "wikilink")（中島宗美）

  - [SKIP BEAT！華麗的挑戰](../Page/SKIP_BEAT！華麗的挑戰.md "wikilink")（新開誠士）

  - [楚楚可憐超能少女組](../Page/楚楚可憐超能少女組.md "wikilink")（犯人）

  - [Keroro軍曹](../Page/Keroro軍曹.md "wikilink")（(※第210話)）

  - [TIGER×DRAGON！](../Page/TIGER×DRAGON！.md "wikilink")（**北村祐作**）

  - [逮捕令](../Page/逮捕令.md "wikilink")（男）

  - [BAMBOO BLADE](../Page/BAMBOO_BLADE.md "wikilink")（杉小路隆千穗）

  - [非常辣妹](../Page/非常辣妹.md "wikilink")（潮之兄）

  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")（蓋特、電磁）

  - [超時空要塞 Frontier](../Page/超時空要塞_Frontier.md "wikilink")（、早乙女矢三郎）

  - [無限之住人](../Page/無限之住人.md "wikilink")（**天津影久**）

  - [藥師寺涼子怪奇事件簿](../Page/藥師寺涼子怪奇事件簿.md "wikilink")（吾妻克彥）

  - [世界毀滅 毀滅世界的六人](../Page/世界毀滅.md "wikilink")（）

  - [ONE OUTS](../Page/ONE_OUTS.md "wikilink")--（手塚、水橋、美軍I、美軍B）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [SOUL EATER](../Page/SOUL_EATER.md "wikilink")（小丑）
  - [黑神](../Page/黑神.md "wikilink")（揶雲）
  - [BASQUASH\!](../Page/BASQUASH!.md "wikilink")（**楊·哈里斯**）
  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink")（介旅初矢）
  - [潘朵拉之心](../Page/潘朵拉之心.md "wikilink")（艾略特·奈特雷伊）
  - [戰鬥司書系列](../Page/戰鬥司書系列.md "wikilink")（艾恩立凱＝畢斯海爾）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [大神與七個夥伴](../Page/大神與…系列.md "wikilink")（**桐木 李斯特**）
  - [HEROMAN](../Page/HEROMAN.md "wikilink")（艾爾曼‧哈瓦多）
  - [爆漫王](../Page/爆漫王.md "wikilink")（服部雄二郎）
  - [寵物小精靈鑽石&珍珠](../Page/寵物小精靈鑽石&珍珠.md "wikilink")（電磁）
  - [Heartcatch
    光之美少女！](../Page/Heartcatch_光之美少女！.md "wikilink")（寇布拉查（コブラージャ，Cobraja））

<!-- end list -->

  - 2011年

<!-- end list -->

  - [爆丸3狩獵保衛者](../Page/爆丸3狩獵保衛者.md "wikilink")（仁 克昂勒）
  - [閃電十一人GO](../Page/閃電十一人GO.md "wikilink")（車田剛一、石戶修二）
  - [Sacred Seven](../Page/Sacred_Seven.md "wikilink")（零号）
  - [妖怪少爺](../Page/妖怪少爺.md "wikilink")（花開院雅次）
  - [魔乳秘劍帖](../Page/魔乳秘劍帖.md "wikilink")（板胸佐小路）
  - [灼眼的夏娜III](../Page/灼眼的夏娜.md "wikilink")（**池速人**）
  - [爆漫王。2](../Page/爆漫王。.md "wikilink")（服部雄二郎）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [黑子的籃球](../Page/黑子的籃球.md "wikilink")（**伊月俊**、哲也二號）
  - [閃電十一人GO 時空之石](../Page/閃電十一人GO.md "wikilink")（豪炎寺修也、車田剛一）
  - [CØDE:BREAKER 法外制裁者](../Page/CØDE:BREAKER_法外制裁者.md "wikilink")（津野）
  - [絕園的暴風雨](../Page/絕園的暴風雨.md "wikilink")（星村潤一郎）
  - [爆漫王。3](../Page/爆漫王。.md "wikilink")（服部雄二郎）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（阿布馬德）
  - [星光少女 Dear My Future](../Page/星光少女_Dear_My_Future.md "wikilink")（塩谷）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [RDG 瀕危物種少女](../Page/RDG_瀕危物種少女.md "wikilink")（高柳一條）
  - [變態王子與不笑貓](../Page/變態王子與不笑貓.md "wikilink")（戳太）
  - [王者天下2](../Page/王者天下_\(漫畫\).md "wikilink")（**[蒙恬](../Page/蒙恬.md "wikilink")**）
  - [Hunter × Hunter](../Page/Hunter_×_Hunter.md "wikilink")（寇魯多）
  - [閃電十一人GO 銀河](../Page/閃電十一人GO.md "wikilink")（**真名部陣一郎**）
  - [神不在的星期天](../Page/神不在的星期天.md "wikilink")（耀基）
  - [有頂天家族](../Page/有頂天家族.md "wikilink")（南禪寺）
  - [黑子的籃球 第2季](../Page/黑子的籃球.md "wikilink")（**伊月俊**、哲也二號）
  - [銀狐](../Page/銀狐_\(漫畫\).md "wikilink")（吉住真一）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [飆速宅男](../Page/飆速宅男.md "wikilink")（石垣光太郎）
  - [名偵探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")（左門治）※第726話「幸福簡訊會招來不幸」
  - [諸神的惡作劇](../Page/諸神的惡作劇.md "wikilink")（戴歐尼修斯・菲利索斯）
  - [神奇寶貝XY](../Page/神奇寶貝XY.md "wikilink")（石榴）
  - [境界觸發者](../Page/境界觸發者.md "wikilink")（雨取麟兒）
  - [七大罪](../Page/七大罪_\(漫畫\).md "wikilink")（艾利歐尼）
  - 飆速宅男 GRANDE ROAD（石垣光太郎）
  - [Happiness Charge
    光之美少女！](../Page/Happiness_Charge_光之美少女！.md "wikilink")（范統）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [黑子的籃球 第3季](../Page/黑子的籃球.md "wikilink")（**伊月俊**、哲也二號）
  - 境界觸發者（村上鋼）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [Active
    Raid－機動強襲室第八課－](../Page/Active_Raid－機動強襲室第八係－.md "wikilink")（深村昇太）
  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（川越優）
  - [文豪Stray Dogs](../Page/文豪Stray_Dogs.md "wikilink")（金髮男子）
  - [齊木楠雄的災難](../Page/齊木楠雄的災難.md "wikilink")（中丸工作）
  - [漂流武士](../Page/漂流武士.md "wikilink") (米爾茲)

<!-- end list -->

  - 2017年

<!-- end list -->

  - 飆速宅男 NEW GENERATION（石垣光太郎）
  - 有頂天家族2（南禪寺正二郎）
  - [潔癖男子青山！](../Page/潔癖男子青山！.md "wikilink")（成田紫苑／Smart）
  - 名侦探柯南（雨宫秀人 ）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [刻刻](../Page/刻刻.md "wikilink")（佑河翼）
  - [霸穹 封神演義](../Page/封神演義_\(漫畫\).md "wikilink")（雲中子）
  - [龍族拼圖](../Page/龍族拼圖.md "wikilink")（松原）
  - [銀河英雄傳說 Die Neue These 邂逅](../Page/銀河英雄傳說.md "wikilink")（卡內連·魯茲）
  - [後街女孩](../Page/後街女孩.md "wikilink")（廣）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [百慕達三角～多彩田園曲～](../Page/百慕達三角～多彩田園曲～.md "wikilink")（海豹郵遞員）

### OVA

  - （衛兵）

  - [HUNTER×HUNTER OVA](../Page/HUNTER×HUNTER.md "wikilink")（李四特）

  - [ARIA The OVA 〜ARIETTA〜](../Page/水星領航員.md "wikilink")（出雲曉）

  - [灼眼的夏娜SP 「戀愛&溫泉的校外教學！」](../Page/灼眼的夏娜.md "wikilink")（池速人）

  - [野性類戀人](../Page/野性類戀人.md "wikilink")（青桐王將）

  - [聖鬥士星矢 THE LOST CANVAS
    冥王神話](../Page/聖鬥士星矢_THE_LOST_CANVAS_冥王神話.md "wikilink")（射手座
    希緒弗斯）

### 動畫電影

  - 2002年

<!-- end list -->

  - [千年女優](../Page/千年女優.md "wikilink")

<!-- end list -->

  - 2007年

<!-- end list -->

  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（池速人）
  - [異邦人 無皇刃譚](../Page/異邦人_無皇刃譚.md "wikilink")（風午）

**2012年**

  - （**[家定](../Page/德川家定.md "wikilink")**）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [心靈想要大聲呼喊。](../Page/心靈想要大聲呼喊。.md "wikilink")（順的父親）

### 遊戲

  - [學園天堂系列](../Page/學園天堂.md "wikilink")（岩井卓人）
  - [夢幻遊戲](../Page/夢幻遊戲.md "wikilink") 玄武開傳 外傳～鏡之巫女～（望月匠）
  - [ARIA The NATURAL～遙遠記憶之幻象～](../Page/水星領航員.md "wikilink") （出雲曉）
  - [星空幻想Online](../Page/星空幻想Online.md "wikilink")（**艾洛特**）
  - [刀劍亂舞](../Page/刀劍亂舞.md "wikilink")（巴形薙刀）

### 特摄

  - [海賊戰隊豪快者](../Page/海賊戰隊豪快者.md "wikilink")（**司令官ワルズ・ギル**）

### 吹替

**電視／電影**

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>作品</p></th>
<th><p>配演角色</p></th>
<th><p>演員</p></th>
<th><p>媒介備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2009</strong></p></td>
<td><p><a href="../Page/耶誕節會下雪嗎.md" title="wikilink">耶誕節會下雪嗎</a></p></td>
<td><p>韓智勇</p></td>
<td><p><a href="../Page/宋仲基.md" title="wikilink">宋仲基</a></p></td>
<td><p>衛星頻道/DVD版本</p></td>
</tr>
<tr class="even">
<td><p><strong>2010</strong></p></td>
<td><p><a href="../Page/成均館緋聞.md" title="wikilink">成均館緋聞</a></p></td>
<td><p><strong>女林/具龍河</strong></p></td>
<td><p>TBS頻道/DVD版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong>東宇</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2011</strong></p></td>
<td></td>
<td><p><strong>千智雄</strong></p></td>
<td><p>DVD版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/樹大根深.md" title="wikilink">樹大根深</a></p></td>
<td><p><a href="../Page/李祹.md" title="wikilink">世宗李祹</a></p></td>
<td><p>朝日電視台/DVD版本</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2012</strong></p></td>
<td><p><a href="../Page/世上哪裡都找不到的善良男人.md" title="wikilink">世上哪裏都找不到的善良男人</a></p></td>
<td><p><strong>姜馬陸</strong></p></td>
<td><p>DVD版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狼少年：不朽的愛.md" title="wikilink">狼少年：不朽的愛</a></p></td>
<td><p><strong>狼少年/金哲秀</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2016</strong></p></td>
<td><p><a href="../Page/太陽的後裔.md" title="wikilink">太陽的後裔</a></p></td>
<td><p><strong>劉時鎮</strong></p></td>
<td><p>影碟版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2014</strong></p></td>
<td><p><a href="../Page/非凡生命歷.md" title="wikilink">非凡生命歷</a></p></td>
<td><p><em>' Russell "Phil" Phillips</em>'</p></td>
<td><p><a href="../Page/當勞·格利遜.md" title="wikilink">當勞·格利遜</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2015</strong></p></td>
<td><p><a href="../Page/智能叛侶.md" title="wikilink">Ex Machina</a></p></td>
<td><p><strong>Caleb Smith</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2001</strong></p></td>
<td><p><a href="../Page/流星花園_(台灣電視劇).md" title="wikilink">流星花園</a></p></td>
<td><p><strong>西門</strong></p></td>
<td><p><a href="../Page/朱孝天.md" title="wikilink">朱孝天</a></p></td>
<td><p>衛星頻道/DVD版本</p></td>
</tr>
<tr class="even">
<td><p><strong>2004</strong></p></td>
<td><p><a href="../Page/色慾荷里活.md" title="wikilink">色慾荷里活</a></p></td>
<td><p><strong>文森·「文斯」·錢司</strong></p></td>
<td></td>
<td><p>Fox Japan/DVD版本</p></td>
</tr>
<tr class="odd">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/花樣少年少女_(2006年電視劇).md" title="wikilink">花樣少年少女</a></p></td>
<td><p><strong>難波南</strong></p></td>
<td><p><a href="../Page/唐禹哲.md" title="wikilink">唐禹哲</a></p></td>
<td><p>免費頻道/DVD版本</p></td>
</tr>
<tr class="even">
<td><p><strong>2011</strong></p></td>
<td><p>孔子春秋</p></td>
<td><p><strong><a href="../Page/孔丘.md" title="wikilink">青年孔丘</a></strong></p></td>
<td><p>朱剛日尧</p></td>
<td><p>WOWOW頻道</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無聲吶喊.md" title="wikilink">無聲吶喊</a></p></td>
<td><p><strong>姜仁浩</strong></p></td>
<td><p><a href="../Page/孔劉.md" title="wikilink">孔劉</a></p></td>
<td><p>DVD版本</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2012</strong></p></td>
<td><p><a href="../Page/亡命快遞.md" title="wikilink">亡命快遞</a></p></td>
<td><p><strong>威利</strong></p></td>
<td><p><a href="../Page/祖瑟夫·哥頓-利域.md" title="wikilink">祖瑟夫·哥頓-利域</a></p></td>
<td><p>影碟版本</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/孔子.md" title="wikilink">孔子</a></p></td>
<td><p><a href="../Page/孔丘.md" title="wikilink">青年孔丘</a></p></td>
<td><p>夏德俊</p></td>
<td><p>衛星頻道</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星級殺人犯.md" title="wikilink">星級殺人犯</a></p></td>
<td><p><strong>李鬥石</strong></p></td>
<td><p><a href="../Page/朴施厚.md" title="wikilink">朴施厚</a></p></td>
<td><p>DVD版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2014</strong></p></td>
<td></td>
<td><p><strong>Nevada/Nick Chambers</strong></p></td>
<td><p><a href="../Page/伊力查·活.md" title="wikilink">伊力查·活</a></p></td>
<td><p>影碟版本</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/下女們.md" title="wikilink">下女們</a></p></td>
<td><p><strong>金殷基</strong></p></td>
<td><p><a href="../Page/金東昱.md" title="wikilink">金東昱</a></p></td>
<td><p>免費頻道/DVD版本</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2016</strong></p></td>
<td><p><a href="../Page/殺破狼2.md" title="wikilink">殺破狼2</a></p></td>
<td><p><strong>陳志杰</strong></p></td>
<td><p><a href="../Page/吳京.md" title="wikilink">吳京</a></p></td>
<td><p>影碟版本</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### CD

**BLCD** 原作名／台版譯名

  - イベリコ豚と恋と椿。／黑毛豬與椿之戀（**入江**）
  - 嘘みたいな話ですが／如夢似真的愛告白（**北川**）
  - 居酒屋 明樂 （**鳥原泰行**）
  - 草之冠 星之冠（小説家）
  - グッドモーニング／醉後愛上你（**篠原**）
  - 緋色誘惑2（**哈爾雷因**）
  - COLD系列（**藤島啓志**）
      - COLD SLEEP
      - COLD LIGHT
      - COLD FEVER
  - 下がってお待ち下さい／退後一步的愛表白（**田中智宏**）
  - 3軒隣の遠い人／隔鄰三間房的戀人（**高科昇**）
  - 先輩の焦れったい秘密（**森**）
  - タッチ・ミー・アゲイン／再愛我一次（**遠田**）
  - ダブルミンツ／Double Mints（**市川光央**）
  - 散る散る、満ちる／最遙遠的距離（**如月春水**）
  - どうしても触れたくない／無法觸碰的愛（**出口**）
  - それでも、やさしい恋をする／即使如此，依然溫柔的相戀（**出口**）
  - ≠ ノットイコール／≠不等於（**末續果**）
  - 花ムコさん／續．花嫁君（**艸田節**）
  - 四號×警備-一心一意-（**日下大和**）
  - ばらのはなびら／訴情的玫瑰花瓣（**嵐谷真次**）
  - ブーランジェの恋人／麵包店的戀人（**藤川聖**）
  - マッチ売り／賣火柴的男人（花城青司）
  - 花とうさぎ ／ 花與兔 ( **相澤守** )
  - 花のみやこで ／ 於花都之中 ( **蓮見晶** )
  - Nights（**穗積**）
  - ショートケーキの苺にはさわらないで（**南里輝**）

## 外部連結

  - [事務所公開簡歷](https://web.archive.org/web/20150924101730/http://www.sigma7.co.jp/profile/m_27.html)

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:Sigma Seven所属声优](../Category/Sigma_Seven所属声优.md "wikilink")