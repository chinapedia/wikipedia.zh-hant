[AltaVista.svg](https://zh.wikipedia.org/wiki/File:AltaVista.svg "fig:AltaVista.svg")
**Altavista**是全球較知名的網上[搜尋引擎公司之一](../Page/搜尋引擎.md "wikilink")，同時提供搜尋引擎后台技术支持等相关產品。

“AltaVista”這名稱代表「從高處望下」，它於1995年由[迪吉多公司](../Page/迪吉多.md "wikilink")（Digital
Equipment Corporation）創立，因此起初其網址用了*altavista.digital.com*\[1\]\[2\]
。網站使用了[DEC Alpha伺服器](../Page/DEC_Alpha.md "wikilink")。
在該搜尋器出現以前，已有一位名為Jack
Marshall的人開設了名為AltaVista的公司，並使用了altavista.com此域名。結果，該搜尋器被迫使用較長的網址。至1998年，迪吉多公司被[康柏電腦收購](../Page/康柏電腦.md "wikilink")，並出價335萬美元購買altavista.com網址。最後，賣家願意賣出網址，並改名為PhotoLoft。AltaVista也需要把PhotoLoft公司的連結放在首頁一段時間。

Altavista在2003年被[Yahoo收购](../Page/Yahoo.md "wikilink")，成為Yahoo的子公司，2013年7月8日起關閉網站，網址会自动跳转到Yahoo搜索\[3\]。

## 參考資料

[Category:1995年建立的网站](../Category/1995年建立的网站.md "wikilink")
[Category:2013年关闭的网站](../Category/2013年关闭的网站.md "wikilink")
[Category:網路搜尋引擎](../Category/網路搜尋引擎.md "wikilink")
[Category:帕羅奧圖公司](../Category/帕羅奧圖公司.md "wikilink")
[Category:1995年加利福尼亞州建立](../Category/1995年加利福尼亞州建立.md "wikilink")

1.
2.
3.