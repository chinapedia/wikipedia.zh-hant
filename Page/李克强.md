**李克强**（），[安徽](../Page/安徽.md "wikilink")[定远](../Page/定远县.md "wikilink")[吳圩鎮高埂村人](../Page/吳圩鎮_\(定遠縣\).md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[江西](../Page/江西.md "wikilink")[武寧](../Page/武寧.md "wikilink")\[1\]，[中国共产党和](../Page/中国共产党.md "wikilink")[中华人民共和国主要](../Page/中华人民共和国.md "wikilink")[领导人之一](../Page/党和国家领导人.md "wikilink")。[北京大学法学学士](../Page/北京大学.md "wikilink")、经济学博士，[在职研究生学历](../Page/在职研究生.md "wikilink")。1976年5月加入[中国共产党](../Page/中国共产党.md "wikilink")，现任[中华人民共和国国务院总理](../Page/中华人民共和国国务院总理.md "wikilink")、[党组书记](../Page/中国共产党国务院党组.md "wikilink")。中共第十五届、十六届、十七届、十八届、十九届中央委员，十七届、十八届、十九届中央政治局委员、常委。第八届全国人大常委会委员。

李克强负责分管[经济](../Page/中央财经委员会.md "wikilink")、[外交](../Page/中央外事工作委员会.md "wikilink")、[国家安全](../Page/中央国家安全委员会.md "wikilink")、[深化改革](../Page/中央全面深化改革委员会.md "wikilink")、[军民发展](../Page/中央军民融合发展委员会.md "wikilink")、[网络信息](../Page/中央网络安全和信息化委员会.md "wikilink")、[法治](../Page/中央全面依法治国委员会.md "wikilink")、[教育和](../Page/中央教育工作领导小组.md "wikilink")[审计等工作](../Page/中央审计委员会.md "wikilink")。此外，他还担任[中央机构编制委员会主任](../Page/中央机构编制委员会.md "wikilink")、[国家国防动员委员会主任](../Page/国家国防动员委员会.md "wikilink")、[国家能源委员会主任等职务](../Page/国家能源委员会.md "wikilink")。

李克强在2017年10月召开的[中共十九大上](../Page/中国共产党第十九次全国代表大会.md "wikilink")，与总书记[习近平一起以全票当选](../Page/习近平.md "wikilink")[第十九届中央委员](../Page/中国共产党第十九届中央委员会委员列表.md "wikilink")\[2\]，并于10月25日召开的[中共十九届一中全会上连任中央政治局常委](../Page/中国共产党第十九届中央委员会第一次全体会议.md "wikilink")，开始其第三届政治局常委任期，排名继续维持在党内第二位。

## 早年经历

1955年7月1日生于[安徽](../Page/安徽.md "wikilink")[合肥](../Page/合肥.md "wikilink")。1972年至1974年在[合肥八中高中部学习](../Page/合肥八中.md "wikilink")。1974年至1976年为安徽省[凤阳县大庙公社东陵大队](../Page/凤阳县.md "wikilink")[知青](../Page/知青.md "wikilink")；1976年至1978年担任安徽省凤阳县大庙公社大庙大队党支部书记。

李克强是1977年中国恢复[高考后第一批](../Page/普通高等学校招生全国统一考试.md "wikilink")[北京大學](../Page/北京大學.md "wikilink")[法律系的学生](../Page/北京大学法学院.md "wikilink")，在北大上学期间，師從[厲以寧等名家](../Page/厉以宁.md "wikilink")，曾任北大学生会常代会会长，毕业后在职获得北大[经济学](../Page/经济学.md "wikilink")[博士學位](../Page/博士.md "wikilink")。\[3\]李克强的[外语水平高](../Page/外语.md "wikilink")，\[4\]能使用流利[英语与](../Page/英语.md "wikilink")[外国人交流](../Page/外国人.md "wikilink")。\[5\]李克强的夫人[程虹是一名英语教授](../Page/程虹.md "wikilink")，据悉夫妇两人在家中会使用英语交谈。\[6\]

## 步入政界

李克強1976年5月加入[中国共产党](../Page/中国共产党.md "wikilink")；1982年起先后担任北京大學團委書記、[中國共青團中央書記處書記](../Page/中國共青團.md "wikilink")、[全國青聯副主席](../Page/全國青聯.md "wikilink")。1993年至1998年任共青團中央書記處[第一書記](../Page/第一书记_\(中国\).md "wikilink")。1998年6月，任[中共河南省委副书记](../Page/中共河南省委.md "wikilink")；1998年7月，任河南省副省长、代省长；1999年2月，任河南省省长；2002年12月，任中共河南省委书记、省长；2003年1月，任河南省委书记、河南省人大常委会的主任；2004年12月至2007年11月，任[中共辽宁省委书记](../Page/中共辽宁省委.md "wikilink")、省人大常委会主任。

### 主政河南

1998年6月，李克强离开北京转任[河南省省长](../Page/河南行政长官列表.md "wikilink")，开始了在河南长达7年的工作，期间河南省的经济总量跃居至全国第5位，居中西部省份之首。基于河南省城镇化落后，导致产业结构升级缓慢，现代农业发展困难重重的现实，2002年12月24日，李克强在河南提出加快工业化、城镇化、推进农业现代化，从而把更多的富余劳动力从土地上解放出来。2003年3月李克强提出“中原崛起”概念，在他的领导和推动下，协调发展大中小城市、规划建设[郑东新区](../Page/郑东新区.md "wikilink")、谋划[中原城市群](../Page/中原城市群.md "wikilink")、振兴小城镇等一系列举措相继展开。\[7\]

[李长春任职河南期间时](../Page/李长春.md "wikilink")“[血浆经济](../Page/河南血祸.md "wikilink")”对部分河南人民造成重大伤害，致使[艾滋病疫情泛滥](../Page/艾滋病.md "wikilink")，\[8\]后來李克强担任[中共河南省委书记兼](../Page/中共河南省委.md "wikilink")[河南省人大常委会主任](../Page/河南省人民代表大会.md "wikilink")，组织力量开展了中国首次省级艾滋病情普查，组织省直部门对口帮扶重灾村，并确立了患者“四有一不”的权益机制，使疫情蔓延得以遏制。\[9\]

### 转任辽宁

2004年李克强出任[中共辽宁省委书记兼](../Page/中共辽宁省委.md "wikilink")[辽宁省人大常委会主任](../Page/辽宁省人民代表大会.md "wikilink")，解决了辽宁国有企业改革和大量人员“下岗”的难题。李克强鼓励非国营经济的发展，主导棚户区改造\[10\]。

## 政治局常委

2007年10月在[中共十七届一中全会上从中央委员直接晋升为](../Page/中共十七届一中全会.md "wikilink")[中共中央政治局委员](../Page/中共中央政治局.md "wikilink")、常委，進入中國最高決策層。

2007年11月被任命为[国务院党组副书记](../Page/中华人民共和国国务院.md "wikilink")，[中共中央财经领导小组成员](../Page/中共中央财经领导小组.md "wikilink")，为2008年[第十一届全国人大第一次会议中接任](../Page/第十一届全国人大第一次会议.md "wikilink")[国务院副总理做准备](../Page/中华人民共和国国务院副总理.md "wikilink")。

2008年3月17日，李克强在十一屆全國人大一次會議第七次全體會議當選為[国务院副总理](../Page/中华人民共和国国务院副总理.md "wikilink")（排名第一）。

李克强担任过中共第15屆至19屆中央委員，17屆至19屆中央政治局委員、常委。第8屆[全國人大常委會委員](../Page/全國人大常委會.md "wikilink")。

2012年11月15日[中共十八届一中全会上](../Page/中共十八届一中全会.md "wikilink")，李克强连任[中国共产党第十八届](../Page/中国共产党.md "wikilink")[中央政治局常务委员会委员](../Page/中央政治局常务委员会.md "wikilink")（排名第二，仅次于总书记[习近平](../Page/习近平.md "wikilink")）。同年，美国《[福布斯](../Page/福布斯.md "wikilink")》杂志[2012年全球最具权力人物排行榜上](../Page/2012年全球最具权力人物排行榜.md "wikilink")，李克强名列13位，此后均入选“[全球最具权力人物排行榜](../Page/福布斯杂志全球最具影响力人物列表.md "wikilink")”头20位。

2017年10月24日，李克强以全票当选[中共十九届中央委员](../Page/中国共产党第十九届中央委员会委员列表.md "wikilink")。\[11\]10月25日的[中共十九届一中全会上](../Page/中共十九届一中全会.md "wikilink")，李克强再次连任中共第十九届中央政治局常委。

<File:Foreign> Secretary meeting Vice Premier of the State Council of
the People’s Republic of China
(5347479746).jpg|2011年1月，到访英国并会见外交大臣[威廉·黑格](../Page/威廉·黑格.md "wikilink")
<File:Vice> Premier of the State Council of the People’s Republic of
China (5347479772).jpg|2011年1月，在英国出席英中贸易协会晚宴并发表演讲 <File:Dmitry> Medvedev
and Li
Keqiang-2.jpeg|2012年4月，到访俄罗斯并会见时任总统的[德米特里·梅德韦杰夫](../Page/德米特里·梅德韦杰夫.md "wikilink")

## 担任总理

[李克强宣誓.jpg](https://zh.wikipedia.org/wiki/File:李克强宣誓.jpg "fig:李克强宣誓.jpg")
2013年3月15日，[第十二届全国人大第一次会议上](../Page/第十二届全国人大第一次会议.md "wikilink")，以2940票（得票率99.7%）当选[国务院总理](../Page/中华人民共和国国务院总理.md "wikilink")，是[中华人民共和国成立以来第七位总理](../Page/中华人民共和国成立.md "wikilink")。3月25日，李克强出任[中央机构编制委员会主任](../Page/中央机构编制委员会.md "wikilink")。\[12\]8月8日，李克强出任[国家国防动员委员会主任](../Page/国家国防动员委员会.md "wikilink")。\[13\]

李克强分别在2013年12月和2014年1月的[中共中央政治局会议上](../Page/中共中央政治局.md "wikilink")，出任新成立的[中共中央全面深化改革领导小组副组长和](../Page/中共中央全面深化改革领导小组.md "wikilink")[中共中央国家安全委员会副主席的职务](../Page/中共中央国家安全委员会.md "wikilink")。

2018年3月的[第十三届全国人民代表大会第一次全体会议上](../Page/第十三届全国人民代表大会.md "wikilink")，李克强以2964票连任国务院总理。

### 接手困境

[Li_Keqiang,_Chinese_and_foreign_press_conference.jpg](https://zh.wikipedia.org/wiki/File:Li_Keqiang,_Chinese_and_foreign_press_conference.jpg "fig:Li_Keqiang,_Chinese_and_foreign_press_conference.jpg")后的[总理中外记者会](../Page/国务院总理记者会.md "wikilink")\]\]
李克强从[温家宝手中接掌](../Page/温家宝.md "wikilink")[国务院总理一职时](../Page/国务院总理.md "wikilink")，他所承接的与温家宝主政十年已不可同日而语，生态问题、资源枯竭、内需不振、产业结构畸形、[房地产泡沫超大](../Page/房地产.md "wikilink")、金融问题不断都是摆在李克强面前的难题\[14\]。另一方面，[朱镕基时代三大改革的财富被彻底耗尽](../Page/朱镕基.md "wikilink")，使得李克强的改革之路将充满坎坷\[15\]。

李克强任内提出[中国制造2025](../Page/中国制造2025.md "wikilink")、[互联网+](../Page/互联网+.md "wikilink")、[大众创业、万众创新](../Page/大众创业、万众创新.md "wikilink")、供给侧改革等政策，带动[中国经济转型](../Page/中华人民共和国经济.md "wikilink")。

2018年7月6日，美国总统[特朗普正式对价值](../Page/唐納·川普.md "wikilink")340亿美元的[中国商品加征](../Page/中國製造.md "wikilink")25%[关税](../Page/关税.md "wikilink")。李克强回应[中美贸易战时公开表示中国不会主动打](../Page/2018年中美贸易争端.md "wikilink")[贸易战](../Page/贸易战.md "wikilink")，但如果对方挑起贸易战，中方必将采取相应反制措施。李克强又强调贸易战不会有赢家，只能是损人害己，中国将坚定不移[深化改革](../Page/深化改革.md "wikilink")、扩大开放\[16\]。

### 外交关系

李克强就任总理后，外访的第一站为[印度](../Page/印度.md "wikilink")，他表示，两国人民都期望未来[中印合作能在各领域取得更多进展](../Page/中華人民共和國－印度關係.md "wikilink")。\[17\]他还说，中印是山水相连的友好邻邦。\[18\]他也对[中印边境问题进行了会谈](../Page/中印边境.md "wikilink")。

除印度外，李克强亦同时访问[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[瑞士和](../Page/瑞士.md "wikilink")[德国](../Page/德国.md "wikilink")，并同时会见三国领导人。\[19\]

2014年6月，李克强偕同夫人[程虹出访](../Page/程虹.md "wikilink")[英国](../Page/英国.md "wikilink")，会见了英女王[伊丽莎白二世](../Page/伊丽莎白二世.md "wikilink")，与首相[卡梅伦进行了会谈](../Page/卡梅伦.md "wikilink")。\[20\]2015年11月1日，李克强出席在[韩国](../Page/韩国.md "wikilink")[首尔举行已经中断三年的](../Page/首尔.md "wikilink")[中日韩领导人会议](../Page/中日韩领导人会议.md "wikilink")，与韩国总统[朴槿惠和日本首相](../Page/朴槿惠.md "wikilink")[安倍晋三进行会谈](../Page/安倍晋三.md "wikilink")。\[21\]

2016年，李克强先后[访问蒙古暨出席第十一届亚欧首脑会议](../Page/李克强访问蒙古暨出席亚欧会议.md "wikilink")、[出席联合国大会暨访问加拿大和古巴](../Page/李克强出席联合国大会暨访问加拿大和古巴.md "wikilink")、[到访澳门并出席中国-葡语国家经贸合作论坛等多场重要外交活动](../Page/2016年李克強訪問澳門.md "wikilink")。

2018年3月[國務院總理記者會上](../Page/國務院總理記者會.md "wikilink")，李克強喊話：不能允許外國勢力打台灣牌。同年5月[李克強訪日](../Page/2018年李克強訪日.md "wikilink")，出席[中日韓領導人會議](../Page/中日韓領導人會議.md "wikilink")，與日本首相[安倍晉三和韓國總統](../Page/安倍晉三.md "wikilink")[文在寅會談](../Page/文在寅.md "wikilink")，又出席《[中日和平友好條約](../Page/中日和平友好條約.md "wikilink")》締結40周年紀念活動等並受到日本天皇[明仁高規格接待](../Page/明仁.md "wikilink")。

[File:Korea_President_Park_PrimeMinister_LiKeqiang_20130628_01.jpg|2013年6月李克强会见到访的韩国总统](File:Korea_President_Park_PrimeMinister_LiKeqiang_20130628_01.jpg%7C2013年6月李克强会见到访的韩国总统)[朴槿惠](../Page/朴槿惠.md "wikilink")
<File:Chinese> Premiere Li Greets Secretary Kerry in Beijing
(12517506424).jpg|2014年2月李克强会见美国国务卿[约翰·克里](../Page/约翰·克里.md "wikilink")
|2014年11月李克强会见印度总理[纳伦德拉·莫迪](../Page/纳伦德拉·莫迪.md "wikilink") <File:Dilma>
Rousseff e o primeiro-ministro chinês Li
Keqiang.jpg|2015年5月李克强与巴西总统[迪爾瑪·羅塞夫共同出席会议](../Page/迪爾瑪·羅塞夫.md "wikilink")
<File:Sommet> éco
franco-chinois-2562.jpg|2015年7月李克强在法国出席工商峰会与总理[曼纽尔·瓦尔斯会面](../Page/曼纽尔·瓦尔斯.md "wikilink")
[File:Vladimir_Putin_and_Li_Keqiang_(2016-11-08)_01.jpg|2016年11月李克强在俄罗斯克里姆林宫与总统](File:Vladimir_Putin_and_Li_Keqiang_\(2016-11-08\)_01.jpg%7C2016年11月李克强在俄罗斯克里姆林宫与总统)[弗拉基米尔·普京会面](../Page/弗拉基米尔·普京.md "wikilink")
<File:Encontro> com o Senhor Li Keqiang, Primeiro-Ministro da República
Popular da China
2.jpg|2017年9月李克强于巴西总统[米歇爾·特梅爾会面](../Page/米歇爾·特梅爾.md "wikilink")

## 家庭及私人生活

  - 父亲[李奉三曾任安徽省](../Page/李奉三.md "wikilink")[凤阳县](../Page/凤阳县.md "wikilink")[县长](../Page/县长.md "wikilink")，安徽省[蚌埠市](../Page/蚌埠市.md "wikilink")[中级人民法院院长](../Page/中级人民法院.md "wikilink")，安徽省[地方志办公室副主任](../Page/地方志.md "wikilink")\[22\]。
  - 妻子[程虹是英语教授](../Page/程虹.md "wikilink")，任职于[首都经济贸易大学](../Page/首都经济贸易大学.md "wikilink")。两人育有一女，姓名不详，北京大学毕业后到美国[哈佛大学留学](../Page/哈佛大学.md "wikilink")\[23\]。
  - 弟弟[李克明现任国有重点大型企业监事会主席](../Page/李克明.md "wikilink")
  - 岳父[程金瑞曾任共青團](../Page/程金瑞.md "wikilink")[河南省委副書記](../Page/河南省.md "wikilink")，後調北京，担任[國務院扶貧開發領導小組辦公室顧問](../Page/國務院扶貧開發領導小組.md "wikilink")，為副部級幹部。
  - 岳母劉益清曾是[新華社記者](../Page/新華社.md "wikilink")\[24\]。

## 参考文献

## 外部链接

  - [李克强：中国党政领导干部资料库](http://cpc.people.com.cn/gbzl/html/121000288.html)
  - [新华网：李克强简历](http://news.xinhuanet.com/ziliao/2002-02/25/content_289095.htm)
  - [何清涟:本届中国总理不好做](http://www.voachinese.com/content/heqinglian-blog-china-politics-20160514/3330462.html).2016.05.15

## 参见

  - [习李体制](../Page/习李体制.md "wikilink")、[李克强政府](../Page/李克强政府.md "wikilink")
  - [李克强办公室](../Page/李克强办公室.md "wikilink")
  - [李克强经济学](../Page/李克强经济学.md "wikilink")、[克强指数](../Page/克强指数.md "wikilink")、[簡政放權](../Page/簡政放權.md "wikilink")
  - [大众创业、万众创新](../Page/大众创业、万众创新.md "wikilink")、[中国制造2025](../Page/中国制造2025.md "wikilink")、[互联网+](../Page/互联网+.md "wikilink")
  - [中国（上海）自由贸易试验区](../Page/中国（上海）自由贸易试验区.md "wikilink")
  - [粵港澳大灣區](../Page/粵港澳大灣區.md "wikilink")
  - [李克強訪問香港期間保安爭議](../Page/李克強訪問香港期間保安爭議.md "wikilink")
  - [团派](../Page/团派.md "wikilink")

{{-}}       |-  |-   |-    |-

[Category:李克强](../Category/李克强.md "wikilink")
[Category:李克强家族](../Category/李克强家族.md "wikilink")
[7](../Category/中华人民共和国国务院总理.md "wikilink")
[Category:中共河南省委书记](../Category/中共河南省委书记.md "wikilink")
[Category:中共辽宁省委书记](../Category/中共辽宁省委书记.md "wikilink")
[Category:第11届国务院副总理](../Category/第11届国务院副总理.md "wikilink")
[Category:中国共产党党员
(1976年入党)](../Category/中国共产党党员_\(1976年入党\).md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:合肥人](../Category/合肥人.md "wikilink")
[Category:定遠人](../Category/定遠人.md "wikilink")
[Keqiang](../Category/李姓.md "wikilink")
[Category:辽宁省人大常委会主任](../Category/辽宁省人大常委会主任.md "wikilink")
[Category:河南省人大常委会主任](../Category/河南省人大常委会主任.md "wikilink")
[Category:中国共产党第十九届中央委员会委员](../Category/中国共产党第十九届中央委员会委员.md "wikilink")
[Category:中国共产党第十九届中央政治局委员](../Category/中国共产党第十九届中央政治局委员.md "wikilink")
[Category:中国共产党第十八届中央政治局常务委员会委员](../Category/中国共产党第十八届中央政治局常务委员会委员.md "wikilink")
[Category:中国共产党第十八届中央政治局委员](../Category/中国共产党第十八届中央政治局委员.md "wikilink")
[Category:中国共产党第十九届中央政治局常务委员会委员](../Category/中国共产党第十九届中央政治局常务委员会委员.md "wikilink")
[Category:第13届国务院常务会议组成人员](../Category/第13届国务院常务会议组成人员.md "wikilink")
[Category:中国共产党第十五届中央委员会委员](../Category/中国共产党第十五届中央委员会委员.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:粤港澳大湾区](../Category/粤港澳大湾区.md "wikilink")
[Category:全国青联副主席](../Category/全国青联副主席.md "wikilink")

1.  [安徽定远九梓乡宗亲联谊记_中华磨刀李--永修县磨刀李文化研究会](http://www.zhmodaoli.com/zh/lishiwenhua/muzonglianyi/wenhua_114.html)
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22. [父为基层官员性格相似](http://specials.on.cc/17thcpc/news02b.html)
    [東方日報](../Page/東方日報_\(香港\).md "wikilink")
23.
24. [大學邂逅成婚，妻在大學做研究](http://hk.apple.nextmedia.com/international/art/20121031/18053118)[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")