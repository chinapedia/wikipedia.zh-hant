**邢佳棟**（），[中國](../Page/中國.md "wikilink")[演員](../Page/演員.md "wikilink")，以飾演[軍人聞名](../Page/軍人.md "wikilink")。

## 生平

1995年第二次考入[北京电影学院](../Page/北京电影学院.md "wikilink")。毕业后，进入[中国国家话剧院](../Page/中国国家话剧院.md "wikilink")。

## 代表作品

### 话剧

  - 《[唐璜](../Page/唐璜.md "wikilink")》
  - 《[纪念碑](../Page/纪念碑.md "wikilink")》
  - 《[赵氏孤儿](../Page/赵氏孤儿.md "wikilink")》（赵樱）
  - 《[四世同堂](../Page/四世同堂.md "wikilink")》

### 电影

| 首播时间 | 剧名                                      | 角色     | 备注 |
| ---- | --------------------------------------- | ------ | -- |
|      | 《[8点35分](../Page/8点35分.md "wikilink")》  | 顾聪     |    |
| 2005 | 《[情人结](../Page/情人结.md "wikilink")》      | 屈哥     |    |
| 2015 | 《[剩者为王](../Page/剩者为王.md "wikilink")》    | 白醫生    |    |
| 2016 | 《[谎言西西里](../Page/谎言西西里.md "wikilink")》  | 馬帥     |    |
| 2017 | 《[非凡任务](../Page/非凡任务.md "wikilink")》    | 张海涛    |    |
| 2018 | 《[我是检察官](../Page/我是检察官.md "wikilink")》  |        |    |
| 2018 | 《[無雙](../Page/無雙_\(電影\).md "wikilink")》 | 泰國將軍手下 |    |
| 待映   | 《[三体](../Page/三体_\(电影\).md "wikilink")》 | 杨卫宁    |    |
|      |                                         |        |    |

### 电视

| 首播时间  | 剧名                                             | 角色                             | 备注                                    |
| ----- | ---------------------------------------------- | ------------------------------ | ------------------------------------- |
| 1996年 | 《[风墙](../Page/风墙.md "wikilink")》               | 钱天玄                            | 获奖作品                                  |
| 1998年 | 《[水命](../Page/水命_\(电视剧\).md "wikilink")》       | 猛子                             |                                       |
| 1998年 | 《[嫂子](../Page/嫂子_\(电视剧\).md "wikilink")》       | 焦满堂（成年）                        |                                       |
| 2001年 | 《[城市的星空](../Page/城市的星空.md "wikilink")》         |                                |                                       |
| 2002年 | 《[缉私要案组](../Page/缉私要案组.md "wikilink")》         |                                |                                       |
| 2003年 | 《[军歌嘹亮](../Page/军歌嘹亮.md "wikilink")》           | 高权（成年）                         |                                       |
| 2003年 | 《[神探谷梁](../Page/神探谷梁.md "wikilink")》           | 大龙                             |                                       |
| 2003年 | 《[天下第一楼](../Page/天下第一楼_\(电视剧\).md "wikilink")》 | 唐茂盛                            |                                       |
| 2003年 | 《[都市第五季](../Page/都市第五季.md "wikilink")》         | 恐龙                             | [情景喜剧](../Page/情景喜剧.md "wikilink")    |
| 2004年 | 《[热带风暴](../Page/热带风暴_\(电视剧\).md "wikilink")》   | 韩非                             |                                       |
| 2005年 | 《[吕梁英雄传](../Page/吕梁英雄传_\(电视剧\).md "wikilink")》 | 孟二楞                            |                                       |
| 2006年 | 《[士兵突击](../Page/士兵突击.md "wikilink")》           | 伍六一                            |                                       |
| 2007年 | 《[爸爸,别哭](../Page/爸爸,别哭.md "wikilink")》         | 王大年                            |                                       |
| 2007年 | 《[秋海棠](../Page/秋海棠_\(2007年\).md "wikilink")》   | 刘玉华                            |                                       |
| 2007年 | 《[婚姻背后](../Page/婚姻背后.md "wikilink")》           | 吴双                             | 又名：[合同婚姻](../Page/合同婚姻.md "wikilink") |
| 2007年 | 《[雪琉璃](../Page/雪琉璃.md "wikilink")》             | 向菩明                            | 未播出                                   |
| 2009年 | 《[我的团长我的团](../Page/我的团长我的团.md "wikilink")》     | 虞啸卿                            |                                       |
| 2009年 | 《[化剑](../Page/化剑.md "wikilink")》               | 刘铁                             |                                       |
| 2008年 | 《[进城](../Page/进城.md "wikilink")》               | 铁良                             |                                       |
| 2009年 | 《[雾里看花](../Page/雾里看花.md "wikilink")》           | 郑岩                             |                                       |
| 2009年 | 《[烈火红岩](../Page/烈火红岩.md "wikilink")》           | 杜荫山                            |                                       |
| 2012年 | 《[风云之十里洋场](../Page/风云之十里洋场.md "wikilink")》     |                                |                                       |
| 2012年 | 《我的非常闺密》                                       |                                | 客串                                    |
| 2012年 | 《[缉毒精英](../Page/缉毒精英.md "wikilink")》           | 李涤凡                            |                                       |
| 2012年 | 《[狂飙支队](../Page/狂飙支队.md "wikilink")》           | 桥隆飙                            |                                       |
| 2013年 | 《[零炮楼](../Page/零炮楼.md "wikilink")》             |                                |                                       |
| 2013年 | 《[战雷](../Page/战雷.md "wikilink")》               |                                |                                       |
| 2014年 | 《[结婚的秘密](../Page/结婚的秘密.md "wikilink")》         | 林旭阳                            |                                       |
| 2014年 | 《[婚战](../Page/婚战.md "wikilink")》               | 游弋                             |                                       |
| 2014年 | 《[麻雀春天](../Page/麻雀春天.md "wikilink")》           | 赵包子                            |                                       |
| 2014年 | 《[古城往事](../Page/古城往事.md "wikilink")》           | 慕天恩                            | 2010年拍攝                               |
| 2014年 | 《[养父的花样年华](../Page/养父的花样年华.md "wikilink")》     | 郎德贵                            |                                       |
| 2017年 | 《[大秦帝国之崛起](../Page/大秦帝国之崛起.md "wikilink")》     | [白起](../Page/白起.md "wikilink") |                                       |
| 待播    | 《[烈马长风](../Page/烈马长风.md "wikilink")》           | 乔伦                             | 又名: 金戈铁马                              |
| 待播    | 《[代号·山豹](../Page/代号·山豹.md "wikilink")》         |                                |                                       |
| 待播    | 《[平遥人](../Page/平遥人.md "wikilink")》             |                                |                                       |
| 待播    | 《锅盖头》                                          |                                |                                       |
|       |                                                |                                |                                       |

## 外部链接

  - [君为传播·邢佳栋](https://web.archive.org/web/20090326081535/http://www.chinakingway.com/artist/star.asp?id=82)
  - [邢佳栋新浪博客](http://blog.sina.com.cn/xingjiadong)
  - [新浪明星全接触-邢佳栋](http://ent.sina.com.cn/s/m/f/xingjiadong)

[J佳](../Category/邢姓.md "wikilink") [X邢](../Category/太原人.md "wikilink")
[Category:中国电影男演员](../Category/中国电影男演员.md "wikilink")
[Category:中国电视男演员](../Category/中国电视男演员.md "wikilink")
[X邢](../Category/北京电影学院校友.md "wikilink")
[X邢](../Category/中国国家话剧院演员.md "wikilink")