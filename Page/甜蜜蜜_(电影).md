是1996年[嘉禾电影出品的一部香港电影](../Page/嘉禾电影.md "wikilink")，[陈可辛执导](../Page/陈可辛.md "wikilink")，[张曼玉和](../Page/张曼玉.md "wikilink")[黎明主演](../Page/黎明.md "wikilink")。荣获[第34屆金馬獎最佳剧情片和最佳女主角奖](../Page/第34屆金馬獎.md "wikilink")，及[第16届香港电影金像奖最佳电影](../Page/第16届香港电影金像奖.md "wikilink")、最佳导演等9项大奖。被美国《[时代周刊](../Page/时代周刊.md "wikilink")》评为1997年度十大佳片第二名。\[1\]

## 故事概要

1986年3月，黎小軍告別女友小婷，從[天津來到](../Page/天津.md "wikilink")[香港討生活](../Page/香港.md "wikilink")，期望有天把小婷接來風光地成婚。黎小軍在[速食店與同樣從大陸到香港討生活的李翹結識](../Page/速食店.md "wikilink")，因為共同喜愛[鄧麗君](../Page/鄧麗君.md "wikilink")，兩人在交往的過程中產生了真愛。但之後黎小軍依然娶了小婷，而李翹跟著黑社會老大豹哥離開香港。

黎小軍之後轉往美國[紐約工作](../Page/紐約.md "wikilink")，李翹在漂泊各地之後也移居紐約，兩人於1995年重逢。\[2\]

## 演员表

|                                              |                          |                                               |
| -------------------------------------------- | ------------------------ | --------------------------------------------- |
| **演員**                                       | **角色**                   | **介紹**                                        |
| [黎明](../Page/黎明.md "wikilink")               | 黎小军                      |                                               |
| [张曼玉](../Page/张曼玉.md "wikilink")             | 李翘                       | [广州人](../Page/广州.md "wikilink")               |
| [曾志伟](../Page/曾志伟.md "wikilink")             | 欧阳豹                      | 黑道大佬，最后在纽约街头被黑人小混混打劫射杀                        |
| [张同祖](../Page/张同祖.md "wikilink")             |                          |                                               |
| [杜可风](../Page/杜可风.md "wikilink")             | 英文老师                     |                                               |
| [杨恭如](../Page/杨恭如.md "wikilink")             | 方小婷                      | 小军的女友，後成為其妻                                   |
| Len Berdick                                  | U.S. Immigration Officer |                                               |
| Michelle Gabriel                             |                          |                                               |
| [Irene Tsu](../Page/Irene_Tsu.md "wikilink") | Aunt Rosie               | 小军的姑母，偶像为[威廉·荷顿](../Page/威廉·荷顿.md "wikilink") |
| [丁羽](../Page/丁羽.md "wikilink")               |                          |                                               |

## 其他主创

  - 出品人：[曾志偉](../Page/曾志偉.md "wikilink")、[鄒文懷](../Page/鄒文懷.md "wikilink")
  - 副導演 /
    助理导演：[邓汉强](../Page/邓汉强.md "wikilink")、[關信輝](../Page/關信輝.md "wikilink")、[趙良駿](../Page/趙良駿.md "wikilink")
  - 製片：張志光、杜玉貞、Gine Lui
  - 執行監製：鍾珍
  - 艺术指导：[奚仲文](../Page/奚仲文.md "wikilink")
  - 服装设计：[吴里璐](../Page/吴里璐.md "wikilink")、陳秀明

## 关于主题曲

本片的主題曲〈[甜蜜蜜](../Page/甜蜜蜜_\(鄧麗君歌曲\).md "wikilink")〉是[鄧麗君在](../Page/鄧麗君.md "wikilink")[中國大陸最为人耳熟能详的代表作之一](../Page/中國大陸.md "wikilink")。本片为纪念邓丽君逝世而作，故事发生的年代正是邓丽君在日本乃至整个东亚最为活跃的巅峰时期，最后以她不幸因病去世这一事件为全片故事结尾的契机。

## 奖项

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1997年香港電影金像獎.md" title="wikilink">第16届香港电影金像奖</a></p></td>
<td><p>最佳影片</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳導演</p></td>
<td><p><a href="../Page/陳可辛.md" title="wikilink">陳可辛</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳編劇</p></td>
<td><p><a href="../Page/岸西.md" title="wikilink">岸西</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳女主角</p></td>
<td><p><a href="../Page/張曼玉.md" title="wikilink">張曼玉</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳男配角</p></td>
<td><p><a href="../Page/曾志偉.md" title="wikilink">曾志偉</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳攝影</p></td>
<td><p>馬楚成</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳美術指導</p></td>
<td><p>奚仲文</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳服裝造型設計</p></td>
<td><p>吳里璐</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳原創電影音樂</p></td>
<td><p><a href="../Page/趙增熹.md" title="wikilink">趙增熹</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳男主角</p></td>
<td><p>黎明</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳新演员</p></td>
<td><p>杨恭如</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第34屆<a href="../Page/金马奖.md" title="wikilink">金马奖</a></p></td>
<td><p><strong>最佳剧情片</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>最佳女主角</strong></p></td>
<td><p>張曼玉</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳原著剧本</p></td>
<td><p>岸西</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳男配角</p></td>
<td><p>曾志偉</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳摄影</p></td>
<td><p>馬楚成</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳造型设计</p></td>
<td><p>吳里璐</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第3屆<a href="../Page/香港電影評論學會大獎.md" title="wikilink">香港電影評論學會大獎</a></p></td>
<td><p>最佳電影</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳導演</p></td>
<td><p>陳可辛</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳女演員</p></td>
<td><p>張曼玉</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第42届亚太影展.md" title="wikilink">第42届亚太影展</a></p></td>
<td><p>最佳女主角奖</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳剧本奖</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## DVD

| 發行日期       | 規格                                  | 發行公司                                      | 發行地   | 產品編號    |
| ---------- | ----------------------------------- | ----------------------------------------- | ----- | ------- |
| 2017年3月3日  | 修復版[DVD](../Page/DVD.md "wikilink") | [華納兄弟影業](../Page/華納兄弟.md "wikilink")、得利影视 | 台灣、香港 | WBD3171 |
| 2017年3月10日 | 修復版[藍光光碟](../Page/BD.md "wikilink") | [華納兄弟影業](../Page/華納兄弟.md "wikilink")、得利影视 | 台灣、香港 | WBB2596 |

## 參考來源

## 外部連結

  - 《[甜蜜蜜](http://hk.movies.yahoo.net/movie/details/11740?movie_id=20342&cinema_id=2)》在香港[雅虎的電影頁面](../Page/雅虎.md "wikilink")

  - {{@movies|fWatm0853019|甜蜜蜜}}

  -
  -
  -
  -
  -
  -
[Category:陳可辛電影](../Category/陳可辛電影.md "wikilink")
[Category:香港爱情片](../Category/香港爱情片.md "wikilink")
[Category:1990年代爱情片](../Category/1990年代爱情片.md "wikilink")
[Category:粤语电影](../Category/粤语电影.md "wikilink")
[6](../Category/1990年代香港電影作品.md "wikilink")
[Category:1986年背景电影](../Category/1986年背景电影.md "wikilink")
[Category:1987年背景电影](../Category/1987年背景电影.md "wikilink")
[Category:1990年背景电影](../Category/1990年背景电影.md "wikilink")
[Category:1993年背景电影](../Category/1993年背景电影.md "wikilink")
[Category:1995年背景电影](../Category/1995年背景电影.md "wikilink")
[Category:香港背景电影](../Category/香港背景电影.md "wikilink")
[Category:纽约市背景电影](../Category/纽约市背景电影.md "wikilink")
[Category:嘉禾电影](../Category/嘉禾电影.md "wikilink")
[Category:邓丽君](../Category/邓丽君.md "wikilink")
[Category:金馬獎最佳劇情片](../Category/金馬獎最佳劇情片.md "wikilink")
[Category:金馬獎最佳女主角獲獎電影](../Category/金馬獎最佳女主角獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳電影](../Category/香港電影金像獎最佳電影.md "wikilink")
[Category:香港電影金像獎最佳導演獲獎電影](../Category/香港電影金像獎最佳導演獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳女主角獲獎電影](../Category/香港電影金像獎最佳女主角獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳編劇獲獎電影](../Category/香港電影金像獎最佳編劇獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳男配角獲獎電影](../Category/香港電影金像獎最佳男配角獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳攝影獲獎電影](../Category/香港電影金像獎最佳攝影獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳原創電影音樂獲獎電影](../Category/香港電影金像獎最佳原創電影音樂獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳美術指導獲獎電影](../Category/香港電影金像獎最佳美術指導獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳服裝造型設計獲獎電影](../Category/香港電影金像獎最佳服裝造型設計獲獎電影.md "wikilink")
[Category:香港電影評論學會大獎最佳電影](../Category/香港電影評論學會大獎最佳電影.md "wikilink")

1.
2.