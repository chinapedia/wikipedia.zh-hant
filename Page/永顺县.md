**永顺县**是[湖南省](../Page/湖南省.md "wikilink")[湘西土家族苗族自治州下辖的](../Page/湘西土家族苗族自治州.md "wikilink")[县](../Page/县.md "wikilink")。[国产值](../Page/国内生产总值.md "wikilink")13.17亿元（公元2004年）。县人民政府驻：灵溪镇。

永顺总面积3809平方千米。公元2004年，全县总人口491,640人，其中非农业人口62,722人，占总人口的12.76%；少数民族人口421,674人，占总人口的85.77%，其中土家族373,455人，占少数民族人口总数的88.56%；苗族45656人，占少数民族人口总数的10.83%。

## 历史

  - [汉代为](../Page/汉朝.md "wikilink")“酉阳县”地
  - [三国时](../Page/三国.md "wikilink")[蜀设](../Page/蜀汉.md "wikilink")“黚阳县”
  - [南北朝](../Page/南北朝.md "wikilink")[梁](../Page/南朝梁.md "wikilink")（萧梁）时为“大乡县”
  - [唐朝置](../Page/唐朝.md "wikilink")“溪州”，[宋朝为](../Page/宋朝.md "wikilink")“永顺州”
  - [元朝为](../Page/元朝.md "wikilink")“永顺宣抚司”治所
  - [明为以](../Page/明朝.md "wikilink")“永顺宣慰使司”治所
  - [清雍正七年](../Page/清朝.md "wikilink")（公元1729年）设“永顺县”至今

## 地理

永顺地处[武陵山脉地带](../Page/武陵山脉.md "wikilink")，主要有羊峰山、大青山、人头山等。辖境至高点为羊峰山主峰，海拔1438米，最低处位于[酉水河面](../Page/酉水_\(沅水支流\).md "wikilink")，海拔119米。全县有河流、溪水71条，[酉水位于县南](../Page/酉水_\(沅水支流\).md "wikilink")，属[凤滩水库库区](../Page/凤滩水库.md "wikilink")，[猛洞河位于辖境自北向南贯穿](../Page/猛洞河.md "wikilink")。

相邻[县级行政区有](../Page/县级行政区.md "wikilink")，北邻[桑植县](../Page/桑植县.md "wikilink")，东连[张家界市](../Page/张家界市.md "wikilink")[永定区和](../Page/永定区_\(张家界市\).md "wikilink")[沅陵县](../Page/沅陵县.md "wikilink")，南部与[古丈县](../Page/古丈县.md "wikilink")、[保靖县接壤](../Page/保靖县.md "wikilink")，西与[龙山县为邻](../Page/龙山县.md "wikilink")。

## 行政区划

永顺县现辖有12个镇、11个乡：

  - 镇：灵溪镇、芙蓉镇、小溪镇、泽家镇、首车镇、石堤镇、永茂镇、青坪镇、砂坝镇、塔卧镇、松柏镇、万坪镇
  - 乡：两岔乡、西歧乡、对山乡、润雅乡、朗溪乡、车坪乡、毛坝乡、万民乡、盐井乡、高坪乡、颗砂乡

## 参考文献

## 官方网站

  - [永顺县人民政府](http://www.ysx.gov.cn/)

[湘](../Page/分类:国家级贫困县.md "wikilink")

[永顺县](../Category/永顺县.md "wikilink") [县](../Category/湘西县市.md "wikilink")
[湘西](../Category/湖南省县份.md "wikilink")
[Category:土家族](../Category/土家族.md "wikilink")
[Category:苗族](../Category/苗族.md "wikilink")