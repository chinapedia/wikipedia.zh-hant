**三井物產株式會社舊廈**是[臺灣](../Page/臺灣.md "wikilink")[臺北市市定](../Page/臺北市.md "wikilink")[古蹟](../Page/古蹟.md "wikilink")，位於臺北市[中正區館前路](../Page/中正區_\(臺北市\).md "wikilink")54號，[館前路與](../Page/館前路.md "wikilink")[襄陽路的交口路口上](../Page/襄陽路.md "wikilink")。該地段擁有許多[日治時期遺留下來的金融機構舊址](../Page/台灣日治時期.md "wikilink")，而該建築為最古樸的一棟。

## 背景

[三井物產株式會社舊廈騎樓天花板.jpg](https://zh.wikipedia.org/wiki/File:三井物產株式會社舊廈騎樓天花板.jpg "fig:三井物產株式會社舊廈騎樓天花板.jpg")

[三井物產是](../Page/三井物產.md "wikilink")[日本四大](../Page/日本.md "wikilink")[財閥之一的](../Page/財閥.md "wikilink")[三井財閥母公司](../Page/三井財閥.md "wikilink")，也是[台灣日治時期](../Page/台灣日治時期.md "wikilink")[統治當局刻意從日本引進及培植的](../Page/台灣總督府.md "wikilink")[財團之一](../Page/財閥.md "wikilink")；到了20世紀初，該集團不但已成為當時北台灣的最大[株式會社](../Page/株式會社.md "wikilink")（即[股份公司](../Page/股份公司.md "wikilink")），也是全台灣數一數二的企業體。三井物產不但與日本[三菱商事及](../Page/三菱商事.md "wikilink")[藤山集團三大資本鼎足而立](../Page/藤山集團.md "wikilink")，獨佔了台灣的糖業，最重要的是三井與[台灣總督府專賣局壟斷了台灣的](../Page/台灣總督府專賣局.md "wikilink")[樟腦](../Page/樟腦.md "wikilink")、[茶葉](../Page/茶葉.md "wikilink")、[煤](../Page/煤.md "wikilink")，甚至[鴉片的進出口](../Page/鴉片.md "wikilink")。

1899年，三井在得到台灣樟腦與鴉片專賣權（另還有一家洋商參與專賣），1912年更是獨占台灣所有鴉片市場進口專賣權之後，該財團於1920年台北新設立總部。該總部就選定在當時設立許多金融機構的[表町通](../Page/表町.md "wikilink")[三角窗上](../Page/三角窗.md "wikilink")。（今館前路與襄陽路的交叉口上）。這地點距離當時[台北驛](../Page/台北驛.md "wikilink")（即[台北車站](../Page/台北車站.md "wikilink")）及[大稻埕茶商都相當近](../Page/大稻埕.md "wikilink")，商業機能優越，另外也緊鄰[新公園](../Page/新公園.md "wikilink")（今[二二八和平紀念公園](../Page/二二八和平紀念公園.md "wikilink")）與[總督府博物館](../Page/台灣總督府博物館.md "wikilink")（今[國立台灣博物館](../Page/國立台灣博物館.md "wikilink")）等城內重要日人活動區域。

## 簡介

三井舊廈始建於1920年，為[野村一郎設計](../Page/野村一郎.md "wikilink")，樓高三層，轉角有一塔樓，屬於當時極為盛行的[文藝復興建築樣式](../Page/文藝復興.md "wikilink")。戰後1940年代晚期整修，外推陽臺，改為[裝飾風藝術風格](../Page/裝飾風藝術.md "wikilink")，顯得較為簡潔。

整修後該舊廈比起緊鄰的勸業株式會社舊廈或者當時其他金融建築，造型顯得較為簡潔。不過仍有特色。如牆壁立面有水平四角窗框，轉角與建築兩端的的兩側山牆較高，有突出的四直線小柱做為浮雕裝飾。如同[臺灣鐵道飯店和](../Page/臺灣鐵道飯店.md "wikilink")[菊元百貨皆設有電梯](../Page/菊元百貨.md "wikilink")，是當時相當現代化的建築。

現今該舊廈為[臺灣土地銀行所有](../Page/臺灣土地銀行.md "wikilink")，為台北市公佈的市定古蹟之一。

[最高法院檢察署特別偵查組在](../Page/最高法院檢察署特別偵查組.md "wikilink")2007年4月2日成立後，曾租用該建築的部分樓層作為辦公廳舍；2010年，特偵組遷至臺北市[信義路一段](../Page/信義路_\(台北市\).md "wikilink")[國防部文化營區](../Page/國防部文化營區.md "wikilink")。

## 參見

  - [三井物產株式會社舊倉庫](../Page/三井物產株式會社舊倉庫.md "wikilink")

[M](../Category/國立臺灣博物館.md "wikilink")
[M](../Category/台灣西式建築.md "wikilink")
[M](../Category/台灣日治時期產業建築.md "wikilink")
[M](../Category/台北市古蹟.md "wikilink")
[M](../Category/1920年建造.md "wikilink")
[M](../Category/1920年完工建築物.md "wikilink")
[M](../Category/1920年台灣建立.md "wikilink")
[M](../Category/中正區_\(臺北市\).md "wikilink")
[Category:三井集團](../Category/三井集團.md "wikilink")