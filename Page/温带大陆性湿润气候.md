[ClimateMapWorld.png](https://zh.wikipedia.org/wiki/File:ClimateMapWorld.png "fig:ClimateMapWorld.png")地区也称[温带季风气候](../Page/温带季风气候.md "wikilink")}}\]\]
**温带大陆性湿润气候**，又称**湿润大陆性气候**，是一种[气候类型](../Page/气候.md "wikilink")，温度变化较大。夏季炎热、冬季寒冷。本气候的[降水分布较为均匀](../Page/降水.md "wikilink")。

## 成因与特征

1.  冬季寒冷少雨：从海洋来的[西风经过大陆](../Page/西风带.md "wikilink")[变性作用](../Page/气团变性.md "wikilink")，气温较低且湿度较低。但常有[锋面气旋经过](../Page/锋面气旋.md "wikilink")。冬季降水比[温带海洋性气候要少但比](../Page/温带海洋性气候.md "wikilink")[温带季风气候要多](../Page/温带季风气候.md "wikilink")。
2.  夏季多[对流雨](../Page/对流雨.md "wikilink")，但不如温带季风气候集中。

## 自然带

[温带阔叶林](../Page/温带阔叶林.md "wikilink")

## 区内农业

[大牧场放牧业](../Page/大牧场放牧业.md "wikilink")、[商品谷物农业等](../Page/商品谷物农业.md "wikilink")

## 代表城市

  - 列表

<!-- end list -->

  - [莫斯科](../Page/莫斯科.md "wikilink")

  - [圣彼得堡](../Page/圣彼得堡.md "wikilink")

  - [加里宁格勒](../Page/加里宁格勒.md "wikilink")

  - [斯大林格勒](../Page/斯大林格勒.md "wikilink")

  - [基辅](../Page/基辅.md "wikilink")

  - [敖德萨](../Page/敖德萨.md "wikilink")

  - [明斯克](../Page/明斯克.md "wikilink")

  - [布列斯特](../Page/布列斯特.md "wikilink")

  - [塔林](../Page/塔林.md "wikilink")

  - [华沙](../Page/华沙.md "wikilink")

  - [格但斯克](../Page/格但斯克.md "wikilink")

  - [克拉科夫](../Page/克拉科夫.md "wikilink")

  - [維也納](../Page/維也納.md "wikilink")

  - [布拉提斯拉瓦](../Page/布拉提斯拉瓦.md "wikilink")

  - [布達佩斯](../Page/布達佩斯.md "wikilink")

  - [布拉格](../Page/布拉格.md "wikilink")

  - [柏林](../Page/柏林.md "wikilink")

  - [法蘭克福](../Page/法蘭克福.md "wikilink")

  - [慕尼黑](../Page/慕尼黑.md "wikilink")

  - [赫尔辛基](../Page/赫尔辛基.md "wikilink")

  - [芝加哥](../Page/芝加哥.md "wikilink")

  - [紐約](../Page/紐約.md "wikilink")

  - [華盛頓特區](../Page/華盛頓特區.md "wikilink")

  - [波士頓](../Page/波士頓.md "wikilink")

  - [费城](../Page/费城.md "wikilink")

  - [底特律](../Page/底特律.md "wikilink")

  - [渥太华](../Page/渥太华.md "wikilink")

  - [多伦多](../Page/多伦多.md "wikilink")

  - [蒙特利尔](../Page/蒙特利尔.md "wikilink")

  - [温尼伯](../Page/温尼伯.md "wikilink")

  - [卡尔加里](../Page/卡尔加里.md "wikilink")

  - [埃德蒙顿](../Page/埃德蒙顿.md "wikilink")

<!-- end list -->

  - 气候图表

[category:欧洲气候](../Page/category:欧洲气候.md "wikilink")

[Category:美洲气候](../Category/美洲气候.md "wikilink")
[Category:亚洲气候](../Category/亚洲气候.md "wikilink")