[Paolo_Uccello_031.jpg](https://zh.wikipedia.org/wiki/File:Paolo_Uccello_031.jpg "fig:Paolo_Uccello_031.jpg")

**保罗·乌切洛**（
，），原名**保罗·迪·多诺**（****），[意大利](../Page/意大利.md "wikilink")[画家](../Page/画家.md "wikilink")。由於烏切洛生活于[中世紀末期和](../Page/中世纪末期.md "wikilink")[文藝復興初期](../Page/文艺复兴.md "wikilink")，因此他的作品相應地也呈現出跨時代的特征：他將晚期哥特式和[透視法這兩種不同的藝術潮流融合在了一起](../Page/透视法.md "wikilink")。\[1\]他最著名作品是描繪[聖羅馬諾之戰的](../Page/聖羅馬諾之戰_\(烏切洛\).md "wikilink")[三聯畫](../Page/三聯畫.md "wikilink")。

[瓦薩里在](../Page/瓦薩里.md "wikilink")《[藝苑名人傳](../Page/藝苑名人傳.md "wikilink")》中記載，烏切洛癡迷于[透視法](../Page/透视法.md "wikilink")，常常爲了找到精確的[消失點而徹夜不眠](../Page/消失点.md "wikilink")。他最著名作品是描繪[聖羅馬諾之戰的](../Page/聖羅馬諾之戰_\(烏切洛\).md "wikilink")[三聯畫](../Page/三聯畫.md "wikilink")。

## 生平

記載烏切洛生平的文獻非常少，僅有一些他同時代的官方文件和在他死後75年[瓦薩里寫的](../Page/瓦薩里.md "wikilink")《[藝苑名人傳](../Page/藝苑名人傳.md "wikilink")》。由於文獻的缺少，我們甚至不能確切的知道他的出生日期。一般認為他1397年在[普拉托韦基奥出生](../Page/普拉托韦基奥.md "wikilink")。\[2\]他在稅務申報表上也表明他是1397年出生的，但1446年后他又說他是1396年出生。\[3\]他爸爸多纳·迪·保罗是當地的理发师兼外科医生，他媽媽安东尼娅是弗洛倫撒的大戶人家。他被稱為“烏切洛”，是因為他喜歡畫鳥，“乌切洛”在意大利语就是鸟的意思。

## 参考文献

[Category:佛羅倫薩畫家](../Category/佛羅倫薩畫家.md "wikilink")
[Category:意大利文藝復興畫家](../Category/意大利文藝復興畫家.md "wikilink")

1.

2.
3.  Borsi, Franco & Stefano. *Paolo Uccello*. pp. 15, 34. London: Thames
    & Hudson, 1994.