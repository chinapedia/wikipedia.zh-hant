**神龙汽车有限公司**（Dongfeng Peugeot-Citroën Automobile Company
LTD）是由[东风汽车公司和](../Page/东风汽车公司.md "wikilink")[标致雪铁龙集团共同出资于](../Page/标致雪铁龙集团.md "wikilink")1992年组建的中國區總公司，总部位于[湖北省](../Page/湖北省.md "wikilink")[武汉市拥有武汉](../Page/武汉市.md "wikilink")、襄阳两大生产基地2011年5月已启动第三工厂建设。

根据中国汽车工业协会统计，神龙汽车2005年轿车销量为14.04万辆，列全国第十位，获得2006年度湖北省科技进步奖一等奖、武汉市优秀企业。

## 产品

### 国产车型

  - [神龙富康](../Page/神龙富康.md "wikilink")（Citroën ZX/998，已停产）
  - [东风雪铁龙爱丽舍](../Page/东风雪铁龙爱丽舍.md "wikilink")（Citroën
    Elysée/C-Elysée，第一代及中期改款已停产，第二代正在生产）
  - [东风雪铁龙赛纳](../Page/东风雪铁龙赛纳.md "wikilink")（Citroën Xsara，已停产）
  - [东风雪铁龙萨拉·毕加索](../Page/东风雪铁龙萨拉·毕加索.md "wikilink")（Citroën Xsara
    Picasso，已停产）
  - [东风雪铁龙C2](../Page/东风雪铁龙C2.md "wikilink")（Citroën C2）
  - [东风雪铁龙凯旋](../Page/东风雪铁龙凯旋.md "wikilink")（Citroën C-Triomphe，已停产）
  - [东风雪铁龙世嘉](../Page/东风雪铁龙世嘉.md "wikilink")（Citroën C-Quatre）
  - [东风雪铁龙C5](../Page/东风雪铁龙C5.md "wikilink")（Citroën C5）
  - [东风标致307](../Page/东风标致307.md "wikilink")（Peugeot 307）
  - [东风标致206](../Page/标致206.md "wikilink")（Peugeot 206，已停产）
  - [东风标致207](../Page/标致206#206+.md "wikilink")（Peugeot 207）
  - [东风标致308](../Page/标致308.md "wikilink")（Peugeot 308）
  - [东风标致408](../Page/标致408.md "wikilink")（Peugeot 408）
  - [东风标致508](../Page/标致508.md "wikilink")（Peugeot 508）

<File:Citroën> C3-XR 01
2014-10-14.jpg|[雪铁龙C3-XR](../Page/雪铁龙C3-XR.md "wikilink")\[1\]
<File:Peugeot> 2008 82 VTi Access – Frontansicht, 28. Mai 2014,
Düsseldorf.jpg|[标致2008](../Page/标致2008.md "wikilink") <File:Peugeot>
3008 CN China 2013-03-04.jpg|[标致3008](../Page/标致3008.md "wikilink")
<File:Citroën> C5 II front-1.JPG|[雪铁龙C5](../Page/雪铁龙C5.md "wikilink") II
<File:Peugeot> 508 2.0 HDi Allure 2014
(13921020187).jpg|[标致508](../Page/标致508.md "wikilink")
<File:Citroën> C4L 02 China
2013-03-02.jpg|[雪铁龙C4L](../Page/雪铁龙C4L.md "wikilink")
<File:Citroën> C4L 01 China
2013-03-02.jpg|[雪铁龙C4L](../Page/雪铁龙C4.md "wikilink")
<File:Peugeot> 408 II China
2015-04-10.jpg|[标致408](../Page/标致408.md "wikilink")
<File:Citroën> C-Quatre sedan facelift China
2012-06-16.JPG|[雪铁龙C-Quatre](../Page/雪铁龙C-Quatre.md "wikilink")
<File:Peugeot> 308 sedan China
2013-02-28.jpg|[标致308](../Page/标致308.md "wikilink")
<File:Citroen> C-Elysee 1.6i
2013.jpg|[雪铁龙C-爱丽舍于](../Page/雪铁龙C-爱丽舍于#第二代（2013-至今）.md "wikilink")
<File:Peugeot> - 301 - Mondial de l'Automobile de Paris 2012 -
201.jpg|[标致301](../Page/标致301.md "wikilink") <File:Festival>
automobile international 2014 - Citroën C-Elysée - 005.jpg|[雪铁龙C-爱丽舍于
WTCC](../Page/雪铁龙C-爱丽舍于#第二代（2013-至今）.md "wikilink")

[[雪铁龙C4毕加索](../Page/雪铁龙C4毕加索.md "wikilink")|thumb|right](https://zh.wikipedia.org/wiki/File:Citroën_C4-Picasso_Mk2-7_places_Front.JPG "fig:雪铁龙C4毕加索|thumb|right")

### 进口车型

雪铁龙

  - [雪铁龙C4毕加索](../Page/雪铁龙C4毕加索.md "wikilink")（Citroën C4 Picasso）
  - [雪铁龙C4轿跑](../Page/雪铁龙C4轿跑.md "wikilink")（Citroën C4 Coupé）
  - [雪铁龙C6](../Page/雪铁龙C6.md "wikilink")（Citroën C6）

标致

  - [标致RCZ](../Page/标致RCZ.md "wikilink")（Peugeot RCZ / RCZ R 275）
  - [标致206轿跑](../Page/标致206.md "wikilink")（Peugeot 206 CC）
  - [标致207轿跑](../Page/标致207.md "wikilink")（Peugeot 207 CC）
  - [标致307轿跑](../Page/标致307.md "wikilink")（Peugeot 307 CC）
  - [标致308](../Page/标致308.md "wikilink")（Peugeot 308 CC/SW）
  - [标致407](../Page/标致407.md "wikilink")（Peugeot 407/SW/Coupé）
  - [标致607](../Page/标致607.md "wikilink")（Peugeot 607）
  - [标致5008](../Page/标致5008.md "wikilink")（Peugeot 5008）

## 銷售量

  - 1996 -
  - 1997 -
  - 1998 -
  - 1999 -
  - 2000 -
  - 2001 -
  - 2002 - \[2\]
  - 2003 -
  - 2004 -
  - 2005 -
  - 2006 -
  - 2007 -
  - 2008 -
  - 2009 -
  - 2010 -
  - 2011 -
  - 2012 -
  - 2013 -

\[3\] Sources PSA\[4\]

## 外部链接

  - [东风汽车公司](http://www.dfmc.com.cn)
  - [神龙汽车有限公司](http://www.dpca.com.cn)
  - [东风标致](http://www.peugeot.com.cn)
  - [东风雪铁龙](https://web.archive.org/web/20090301044539/http://dcad.com.cn/)
  - [雪铁龙中国](https://web.archive.org/web/20120920023304/http://www.citroen.com.cn/)

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[category:1992年成立的公司](../Page/category:1992年成立的公司.md "wikilink")

[Category:湖北公司](../Category/湖北公司.md "wikilink")
[Category:东风汽车](../Category/东风汽车.md "wikilink")
[Category:标致雪铁龙集团](../Category/标致雪铁龙集团.md "wikilink")

1.  <http://www.dpca.com.cn/dpca/publish/news/W149C578BC97CA3FB840E068content.html>
    　　洞悉消费者 C3-XR进军SUV市场占先机
2.  [Dossier de presse PSA
    (1996-2005)](http://www.psa-peugeot-citroen.com/document/presse_dossier/DP_Chine_fr1143818564.pdf)

3.  [Dossier de presse PSA
    (1996-2005)](http://www.psa-peugeot-citroen.com/document/presse_dossier/DP_Chine_fr1143818564.pdf)

4.