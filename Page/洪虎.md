**洪虎**（），[中国](../Page/中国.md "wikilink")[安徽](../Page/安徽.md "wikilink")[金寨人](../Page/金寨.md "wikilink")，第十届、十一屆[全国人大法律委员会副主任委员](../Page/全国人大.md "wikilink")，1999年2月在[吉林省九届人大二次会议上当选为省长](../Page/吉林省.md "wikilink")。2004年10月，洪虎卸去中共吉林省委副书记、常委、委员职务，不再担任吉林省省长，退居二线。

洪虎是解放軍上將[洪学智之子](../Page/洪学智.md "wikilink")。

## 生平

1963年[北京工业学院化工系毕业](../Page/北京工业学院.md "wikilink")，高级工程师，9月参加工作。1965年6月加入[中国共产党](../Page/中国共产党.md "wikilink")。1991年至1998年3月任[国家体改委副主任](../Page/国家体改委.md "wikilink")、党组成员、机关党委书记。1998年4月至9月任[国务院经济体制改革办公室副主任](../Page/国务院经济体制改革办公室.md "wikilink")。1998年9月起任中共[吉林省委委員](../Page/吉林省.md "wikilink")、常委、副书记，吉林省人民政府副省长、代省长。1999年2月至2004年10月任吉林省省长。

2005年2月被任命为第十届[全国人大法律委员会副主任委员](../Page/全国人大.md "wikilink")。2008年，担任全国人大法律委员会副主任委员\[1\]，他还是中共第十五屆、十六屆中央委員。

## 參考文獻

[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:第十屆全國人大代表](../Category/第十屆全國人大代表.md "wikilink")
[Category:吉林省全國人民代表大會代表](../Category/吉林省全國人民代表大會代表.md "wikilink")
[Category:中華人民共和國吉林省副省長](../Category/中華人民共和國吉林省副省長.md "wikilink")
[Category:國務院經濟體制改革辦公室副主任](../Category/國務院經濟體制改革辦公室副主任.md "wikilink")
[Category:國家經濟體制改革委員會副主任](../Category/國家經濟體制改革委員會副主任.md "wikilink")
[Category:中國共產黨第十六屆中央委員會委員](../Category/中國共產黨第十六屆中央委員會委員.md "wikilink")
[Category:中國共產黨第十五屆中央委員會委員](../Category/中國共產黨第十五屆中央委員會委員.md "wikilink")
[Category:中共吉林省委副書記](../Category/中共吉林省委副書記.md "wikilink")
[Category:中共吉林省委常委](../Category/中共吉林省委常委.md "wikilink")
[Category:中国共产党党员](../Category/中国共产党党员.md "wikilink")
[Category:北京理工大学校友](../Category/北京理工大学校友.md "wikilink")
[Category:金寨人](../Category/金寨人.md "wikilink")
[H](../Category/洪姓.md "wikilink")

1.