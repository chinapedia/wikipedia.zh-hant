**陳演**（），[字](../Page/表字.md "wikilink")**發聖**，[號](../Page/號.md "wikilink")**讚皇**，[四川](../Page/四川.md "wikilink")[井研人](../Page/井研.md "wikilink")，[崇禎時期的](../Page/崇禎.md "wikilink")[內閣首輔](../Page/內閣首輔.md "wikilink")。

## 生平

陳演是[天啟二年](../Page/天啟_\(明熹宗\).md "wikilink")（1622年）壬戌進士。[崇禎十五年](../Page/崇禎.md "wikilink")（1642年），陳演於[山東平定盜賊有功](../Page/山東.md "wikilink")，升任[戶部尚書](../Page/戶部尚書.md "wikilink")，加[太子少保銜](../Page/太子少保.md "wikilink")。[崇禎十六年五月](../Page/崇禎.md "wikilink")，[周延儒被罷免](../Page/周延儒.md "wikilink")，陳演代為[首輔](../Page/首輔.md "wikilink")，後因謊報戰功而被罷免。《[明史](../Page/明史.md "wikilink")》稱陳演為人“既庸且刻”，[崇禎十七年](../Page/崇禎.md "wikilink")（1644年）正月初三，[李明睿勸崇禎放棄](../Page/李明睿.md "wikilink")[北京](../Page/北京.md "wikilink")，儘快南遷。崇禎有意遷都，陈演反对“南迁”，並示意[兵科](../Page/兵科.md "wikilink")[给事中](../Page/给事中.md "wikilink")[光時亨](../Page/光時亨.md "wikilink")，严厉谴责李明睿，扬言：“不杀李明睿，不足以安定民心”。二月初八，[李自成陷](../Page/李自成.md "wikilink")[太原](../Page/太原.md "wikilink")，北京震動。[薊遼總督](../Page/薊遼總督.md "wikilink")[王永吉](../Page/王永吉.md "wikilink")、[順天巡撫](../Page/順天巡撫.md "wikilink")**楊鶚**建議提調[吳三桂保衛](../Page/吳三桂.md "wikilink")[京師](../Page/京師.md "wikilink")，二月二十七日，崇禎帝在[文華殿召開緊急會議](../Page/文華殿.md "wikilink")，徵調吳三桂“勤王”，陳演、[魏藻德兩人不敢同意](../Page/魏藻德.md "wikilink")，以致此事一延再延。最後崇禎罵他：「汝一死不足蔽辜！」。三月初四，[崇禎帝終於決定](../Page/崇禎帝.md "wikilink")，放棄[寧遠](../Page/寧遠.md "wikilink")，徵調吳三桂、王永吉、[唐通](../Page/唐通.md "wikilink")、[劉澤清護衛京師](../Page/劉澤清.md "wikilink")。吳三桂接到命令後，三月上旬啟程，十六日入[山海關](../Page/山海關.md "wikilink")，二十日至[豐潤時](../Page/豐潤.md "wikilink")，李自成已攻破北京。

[李自成入北京](../Page/李自成.md "wikilink")，陳演想逃離北京，但因家產太多而未果。他主動向[劉宗敏獻白銀四萬兩](../Page/劉宗敏.md "wikilink")。稍後，其家僕告發，說他家中地下藏銀數萬。農民軍掘之，果見地下全是白銀。刘宗敏大怒，大刑伺侯，刑求得黄金数百两，珠珍成斛，四月八日，得釋。十二日，李自成敗於[多爾袞](../Page/多爾袞.md "wikilink")、吳三桂之聯軍，临走前，將陈演等人斩首。

## 參考書目

  - 《[明季北略](../Page/明季北略.md "wikilink")》卷二十二

[Category:明朝戶部尚書](../Category/明朝戶部尚書.md "wikilink")
[Category:明朝大學士](../Category/明朝大學士.md "wikilink")
[Category:明朝被處決者](../Category/明朝被處決者.md "wikilink")
[Category:成都人](../Category/成都人.md "wikilink")
[Y](../Category/陳姓.md "wikilink")
[Category:明朝内阁首辅](../Category/明朝内阁首辅.md "wikilink")