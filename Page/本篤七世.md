[教宗](../Page/教宗.md "wikilink")**本篤七世**（，），原名不详，於974年至983年7月10日岀任教宗。原為蘇得利教區主教，獲得神聖羅馬帝國皇帝[奧托二世](../Page/奧托二世.md "wikilink")，以及反對偽教宗博義七世的羅馬神職團以及居民們的支持下成為教宗，並驅除了博義七世。在其任內他積極推動隱修改革，並曾於981年三月在聖伯多祿堂舉行教務會議譴責[買賣聖職](../Page/買賣聖職.md "wikilink")。\[1\]

## 参考文献

## 譯名列表

  - 本篤七世：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)、[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)、[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=benedict&mode=3)作本篤。
  - 本尼狄克七世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作本尼狄克。

[B](../Category/教宗.md "wikilink")
[Category:羅馬人](../Category/羅馬人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")

1.  《歷代教宗簡史》，鄒保祿，碧岳學舍出版