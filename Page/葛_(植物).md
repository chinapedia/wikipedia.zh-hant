**葛**（[学名](../Page/学名.md "wikilink")：）是[葛屬](../Page/葛屬.md "wikilink")[山葛的](../Page/山葛.md "wikilink")[变种](../Page/变种.md "wikilink")。其[根部為](../Page/根部.md "wikilink")[中草藥](../Page/中草藥.md "wikilink")**葛根**（中药拉丁名*Puerariae
Radix*），又名**鹿藿**、**黃斤**、**雞齊根**，[主治](../Page/主治.md "wikilink")[傷寒溫熱](../Page/傷寒.md "wikilink")、[頭痛項強](../Page/頭痛.md "wikilink")（頸僵）、煩熱消渴、[洩瀉](../Page/洩瀉.md "wikilink")、[痢疾](../Page/痢疾.md "wikilink")、[麻疹不透](../Page/麻疹.md "wikilink")、[高血壓](../Page/高血壓.md "wikilink")、心紋痛、[耳聾等証](../Page/耳聾.md "wikilink")。

### 性味歸經

甘辛平,入脾、胃經。

## 性味

  - 《[神農本草經](../Page/神農本草經.md "wikilink")》：葛根味甘，性平。主消渴，身大熱，[嘔吐](../Page/嘔吐.md "wikilink")，諸痺。起陰氣，解諸毒。葛穀，主下利十歲以上。
  - 《[吴普本草](../Page/吴普本草.md "wikilink")》：葛根，神農：甘。生太山（《御覽》）。

## 藥用

  - 感冒發熱,頭痛項背強硬。
  - 熱痛口渴或消渴症。
  - 麻疹透發不暢,或發熱口渴伴有腹瀉。
  - 本品可緩解高血壓頸項硬疼痛。
  - 临床发现对心脑血管疾病有一定的疗效

## 禁忌

陰虛火旺上盛下虛忌用

## 常用成方

葛根湯：葛根四兩、[麻黃三兩去節](../Page/麻黃.md "wikilink")、[桂枝二兩](../Page/桂枝.md "wikilink")、[芍藥二兩](../Page/芍藥.md "wikilink")、[甘草二兩灸](../Page/甘草.md "wikilink")、[生薑三兩切](../Page/生薑.md "wikilink")、[大棗十二枚擘](../Page/大棗.md "wikilink")。

解酒：葛根一錢，在喝酒前泡一杯開水喝下再喝酒，酒精可解，所以人不會醉。

## 用途

可抑制對酒的渴望，降低血壓和減輕頭痛、頸部僵硬、眩暈、耳鳴。有助於治療酒精性中毒、感冒、流行性感冒和腸胃疾病。

## 注释

## 參考文獻

## 外部链接

  - [野葛
    Yege](http://libproject.hkbu.edu.hk/was40/detail?channelid=1288&searchword=herb_id=D00354)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

  - [葛根
    Gegen](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00063)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)

  - [葛根 Ge
    Gen](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00396)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

  - [黃豆苷
    Daidzin](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00461)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [黃豆苷元
    Daidzein](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00270)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [黃豆黃苷
    Glycitin](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00462)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  - [鳶尾黃酮苷
    Tectoridin](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00049)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

  -
[葛](../Category/葛.md "wikilink") [其](../Category/藥用植物.md "wikilink")
[Category:中草藥](../Category/中草藥.md "wikilink")
[Category:入侵植物種](../Category/入侵植物種.md "wikilink")