**鴻海科技集團**（英語譯名：），是由[臺灣企業家](../Page/臺灣.md "wikilink")[郭台銘創立並擔任](../Page/郭台銘.md "wikilink")[董事長的](../Page/董事長.md "wikilink")[跨國企業集團](../Page/跨國公司.md "wikilink")，總部位於[新北市](../Page/新北市.md "wikilink")[土城區](../Page/土城區.md "wikilink")，以**富士康**（）做為[商標名稱](../Page/商標名稱.md "wikilink")。其專注於[電子產品的](../Page/電子工業.md "wikilink")[代工服務](../Page/代工.md "wikilink")（CM），研發生產精密電氣元件、機殼、準系統、系統組裝、光通訊元件、液晶顯示件等3C產品上、下游產品及服務。旗下多家企業在[臺灣證券交易所](../Page/臺灣證券交易所.md "wikilink")、[香港交易所](../Page/香港交易所.md "wikilink")、[東京證券交易所掛牌上市](../Page/東京證券交易所.md "wikilink")，包括做為集團核心的[鴻海精密](../Page/鴻海精密.md "wikilink")，在世界多國都設有廠房，員工總數超過百萬人。

鴻海科技集團是在創辦人郭台銘發想的「eCMMS」模式進行產業上下游，來建立[經濟規模](../Page/經濟規模.md "wikilink")。旗下各關係企業的研發、設計、製造、銷售、售後服務等領域包括精密零組件、外觀、結構件、系統組裝、光通訊元件、[液晶顯示](../Page/液晶顯示.md "wikilink")、[半導體設備等](../Page/半導體.md "wikilink")。2010年其集團主要企業鴻海精密[合併營收超過千億](../Page/合併營收.md "wikilink")[美元](../Page/美元.md "wikilink")，並列入[-{zh-hans:福布斯;
zh-hant:福布斯; zh-cn:福布斯; zh-tw:富比士; zh-hk:福布斯;
zh-sg:资本家杂志}-全球前五十大排行榜及世界第三大資訊科技公司](../Page/福布斯.md "wikilink")。鴻海在美国《[财富](../Page/财富.md "wikilink")》杂志2018年[全球500大公司排行榜中位列第](../Page/财富世界500强.md "wikilink")24名\[1\]。

## 歷史

1974年，[郭台銘利用新台幣](../Page/郭台銘.md "wikilink")10萬元成立「鴻海塑膠企業有限公司」。

鴻海塑膠成立之初員工僅有10人，主要業務為製造[黑白電視機的旋鈕](../Page/黑白電視機.md "wikilink")。1981年，鴻海塑膠開發[個人電腦](../Page/個人電腦.md "wikilink")[連接器產品](../Page/連接器.md "wikilink")，由此轉型生產個人電腦連接器。1982年，鴻海塑膠改名「[鴻海精密工業股份有限公司](../Page/鴻海精密.md "wikilink")」\[2\]，註冊資本額達到1600萬元。1985年公司成立[美國分公司](../Page/美國.md "wikilink")。

2000年，鴻海精密工業市值突破[新台幣](../Page/新台幣.md "wikilink")1000億元，2005年鴻海集團總營業額突破新台幣兆元，超越[新加坡的](../Page/新加坡.md "wikilink")[偉創力成為世界上最大的科技代工廠](../Page/偉創力.md "wikilink")。爾後持續倍增，2010年底僅鴻海集團營業額達[美元千億](../Page/美元.md "wikilink")，名列全球前五十大企業之列。

2015年，富士康在[中国大陆推出电商平台](../Page/中国大陆.md "wikilink")“富连网”，宣称其目標要在三年内超越[京东](../Page/京东.md "wikilink")，成为中国第二大电商平台\[3\]。

### 投資與併購

<table>
<colgroup>
<col style="width: 17%" />
<col style="width: 12%" />
<col style="width: 15%" />
<col style="width: 20%" />
<col style="width: 36%" />
</colgroup>
<thead>
<tr class="header">
<th><p>投資或併購企業</p></th>
<th><p>註冊地</p></th>
<th><p>金額</p></th>
<th><p>時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>立衛科技</p></td>
<td></td>
<td></td>
<td><p>1994年</p></td>
<td><p>轉投資。[4]</p></td>
</tr>
<tr class="even">
<td><p>隴華電子</p></td>
<td></td>
<td></td>
<td><p>1994年</p></td>
<td><p>轉投資。[5]</p></td>
</tr>
<tr class="odd">
<td><p>欣興電子</p></td>
<td></td>
<td></td>
<td><p>1995年</p></td>
<td><p>轉投資。[6]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/聯華電子.md" title="wikilink">聯華電子</a></p></td>
<td></td>
<td></td>
<td><p>1995年</p></td>
<td><p>轉投資。[7]</p></td>
</tr>
<tr class="odd">
<td><p>鴻揚創業</p></td>
<td></td>
<td></td>
<td><p>1996年</p></td>
<td><p>入股。[8]</p></td>
</tr>
<tr class="even">
<td><p>矽豐</p></td>
<td></td>
<td></td>
<td><p>1996年</p></td>
<td><p>入股。[9]</p></td>
</tr>
<tr class="odd">
<td><p>廣宇科技</p></td>
<td></td>
<td></td>
<td><p>1999年4月</p></td>
<td><p>鴻海集團以億取得廣宇科技16%股權，成為最大單一股東。[10]</p></td>
</tr>
<tr class="even">
<td><p>華升</p></td>
<td></td>
<td></td>
<td><p>1999年7月</p></td>
<td><p>2004年改名鴻準精密。</p></td>
</tr>
<tr class="odd">
<td><p>HTT-Telsa</p></td>
<td></td>
<td></td>
<td><p>2000年5月</p></td>
<td><p>[11]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/群創光電.md" title="wikilink">群創光電</a></p></td>
<td></td>
<td></td>
<td><p>2002年</p></td>
<td><p>鴻海集團投資億元。</p></td>
</tr>
<tr class="odd">
<td><p>撼訊</p></td>
<td></td>
<td></td>
<td><p>2003年</p></td>
<td><p>[12]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/摩托羅拉.md" title="wikilink">摩托羅拉墨西哥廠</a></p></td>
<td></td>
<td><p>1800萬美元</p></td>
<td><p>2003年</p></td>
<td><p>[13]</p></td>
</tr>
<tr class="odd">
<td><p>藝模（Eimo Oyj）集團</p></td>
<td></td>
<td><p>24.55億新台幣</p></td>
<td><p>2003年10月16日</p></td>
<td><p>透過子公司富士康芬蘭投資公司投資24.55億取得藝模集團93.4%，收購藝模集團。[14]</p></td>
</tr>
<tr class="even">
<td><p>國碁電子</p></td>
<td></td>
<td><p>367億新台幣</p></td>
<td><p>2003年10月16日</p></td>
<td><p>國碁電子當時為全球最大的網通產品代工廠。[15]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/湯姆笙.md" title="wikilink">湯姆笙深圳廠</a></p></td>
<td></td>
<td><p>5000萬美元</p></td>
<td><p>2004年</p></td>
<td><p>[16]</p></td>
</tr>
<tr class="even">
<td><p>麗卡</p></td>
<td></td>
<td></td>
<td><p>2004年</p></td>
<td><p>[17]</p></td>
</tr>
<tr class="odd">
<td><p>麗臺</p></td>
<td></td>
<td></td>
<td><p>2004年</p></td>
<td><p>[18]</p></td>
</tr>
<tr class="even">
<td><p>安泰電業</p></td>
<td></td>
<td><p>3.7億新台幣</p></td>
<td><p>2004年2月3日</p></td>
<td><p>安泰電業當時為全球第四大汽車零組件廠。[19]</p></td>
</tr>
<tr class="odd">
<td><p>英群</p></td>
<td></td>
<td></td>
<td><p>2005年4月28日</p></td>
<td><p>入股。[20]</p></td>
</tr>
<tr class="even">
<td><p>奇美通訊</p></td>
<td></td>
<td><p>24.99億新台幣</p></td>
<td><p>2005年5月13日</p></td>
<td><p>透過子公司Transworld Holdings Ltd.轉投資約25億取得<a href="../Page/奇美集團.md" title="wikilink">奇美集團旗下奇美通訊</a>56.48%股權，收購奇美通訊。[21][22][23]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/摩托羅拉.md" title="wikilink">摩托羅拉天津電池分公司</a></p></td>
<td></td>
<td><p>24.99億新台幣</p></td>
<td><p>2005年9月16日</p></td>
<td><p>[24]</p></td>
</tr>
<tr class="even">
<td><p>建漢科技</p></td>
<td></td>
<td><p>3.57億新台幣</p></td>
<td><p>2005年11月</p></td>
<td><p>鴻海以3.57億取得建漢11.46%股權。[25]</p></td>
</tr>
<tr class="odd">
<td><p>普立爾</p></td>
<td></td>
<td><p>282億新台幣</p></td>
<td><p>2006年6月</p></td>
<td><p>鴻海以282億收購並合併相機製造大廠普立爾。[26][27]</p></td>
</tr>
<tr class="even">
<td><p>奇美電子</p></td>
<td></td>
<td><p>1650億新台幣</p></td>
<td><p>2009年11月14日</p></td>
<td><p>鴻海集團旗下<a href="../Page/群創光電.md" title="wikilink">群創光電以</a>1650億併購奇美電子。[28]</p></td>
</tr>
<tr class="odd">
<td><p>台揚科技</p></td>
<td></td>
<td><p>18億新台幣</p></td>
<td><p>2012年9月22日</p></td>
<td><p>鴻海認購2億元、鴻海旗下建漢科技認購16億元，共以18億元取得台揚科技32.62%股權。[29]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亞太電信.md" title="wikilink">亞太電信</a></p></td>
<td></td>
<td><p>116億新台幣</p></td>
<td><p>2014年5月28日</p></td>
<td><p>鴻海集團旗下國碁電子以116億取得<a href="../Page/亞太電信.md" title="wikilink">亞太電信</a>14%股權，成為最大單一股東。[30]</p></td>
</tr>
<tr class="odd">
<td><p>訊智海國際</p></td>
<td></td>
<td><p>2.4億港元</p></td>
<td><p>2016年4月13日</p></td>
<td><p>鴻海集團以2.4億港元取得香港千里眼公司50.07%股權，併購千里眼公司。[31]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏普.md" title="wikilink">夏普株式會社 SHARP CORP.</a></p></td>
<td></td>
<td><p>3888億日圓</p></td>
<td><p>2016年4月2日</p></td>
<td><p>鴻海集團、鴻海旗下鴻準公司、董事長郭台銘個人投資公司SIO合計以3888億日圓取得日本夏普株式會社66%股權，收購夏普[32]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/堺顯示產品公司.md" title="wikilink">堺顯示器株式會社</a> Sakai Display Products Corp.</p></td>
<td></td>
<td><p>660億日圓</p></td>
<td><p>2012年7月12日</p></td>
<td><p>2012年董事長郭台銘以個人投資公司SIO名義注資660億日圓取得夏普顯示器株式會社37.61%股份，之後並更名為堺顯示器株式會社。[33][34]2016年郭台銘再以個人投資公司SIO名義加碼171.7億日圓取得53.05%過半股份，另以349.7億日圓增資SDP，郭台銘成為SDP最大股東，夏普持有股份減至26.71%。</p></td>
</tr>
<tr class="even">
<td><p>SHARP JUSDA Logistics Corporation</p></td>
<td></td>
<td><p>1.632億日圓</p></td>
<td><p>2016年12月1日</p></td>
<td><p>鴻海集團透過子公司準時達以1.632億日圓取得夏普物流公司51%股權。[35]</p></td>
</tr>
<tr class="odd">
<td><p>ScienBiziP Japan</p></td>
<td></td>
<td><p>100萬日元</p></td>
<td><p>2017年2月25日</p></td>
<td><p>鴻海集團透過子公司ScienBiziP Consulting Inc.以100萬日元與<a href="../Page/夏普集團.md" title="wikilink">夏普集團共組合資公司ScienBiziP</a> Japan，取得20%股權。[36]</p></td>
</tr>
<tr class="even">
<td><p>Softbank Group Capital APAC Pte. Ltd.</p></td>
<td></td>
<td><p>6億美元</p></td>
<td><p>2017年2月25日</p></td>
<td><p>鴻海集團透過子公司Foxconn (Far East) Limited以6億美元與<a href="../Page/軟體銀行.md" title="wikilink">軟體銀行共組合資公司</a>，並以54.5%的持股比例取得主導權。[37]</p></td>
</tr>
<tr class="odd">
<td><p>SHARP Healthcare and Medical Company KY</p></td>
<td></td>
<td><p>2500萬美元</p></td>
<td><p>2017年2月25日</p></td>
<td><p>鴻海集團透過子公司Fabrigene Limited以2500萬美元取得夏普醫療保健公司51.2%股權。[38]</p></td>
</tr>
</tbody>
</table>

## 業務發展

### 中国大陸

[Electronics_factory_in_Shenzhen.jpg](https://zh.wikipedia.org/wiki/File:Electronics_factory_in_Shenzhen.jpg "fig:Electronics_factory_in_Shenzhen.jpg")的廠房\]\]
主要分部有：

1.  龙华科技园　　
2.  鸿观科技园
3.  [江苏](../Page/江苏.md "wikilink")[阜宁光电光伏产业基地](../Page/阜宁.md "wikilink")　
4.  [上海](../Page/上海.md "wikilink")[松江科技园](../Page/松江.md "wikilink")
5.  [南京软件科技园](../Page/南京.md "wikilink")　
6.  [杭州钱塘科技园](../Page/杭州.md "wikilink")　　
7.  富士康科技集团（[瀋陽](../Page/瀋陽.md "wikilink")）工業園
8.  富士康科技集团（[营口](../Page/营口.md "wikilink")）工業園，建于2007年，厂区占地3平方公里。
9.  宏启胜精密电子有限公司，富士康（[秦皇岛](../Page/秦皇岛.md "wikilink")）工业园，建于2007年，厂区占地1000余亩。
10. 富士康精密组件（[北京](../Page/北京.md "wikilink")）有限公司 - 北京经济技术开发区　　
11. 鸿富锦精密电子（[天津](../Page/天津.md "wikilink")）有限公司
12. 富士康精密电子（[廊坊](../Page/廊坊.md "wikilink")）有限公司 - 富士康廊坊科技工業園　　　
13. 鸿富锦精密电子（[烟台](../Page/烟台.md "wikilink")）有限公司
14. 富晉精密工業（[晉城](../Page/晉城.md "wikilink")）有限公司 - 鴻海科技集團-大陸投資事業　
15. 鸿富锦精密电子（[郑州](../Page/郑州.md "wikilink")）有限公司　
16. 南阳富士康LED，位于[南阳市](../Page/南阳市.md "wikilink")[卧龙区王村龙升工业园](../Page/卧龙区.md "wikilink")
17. [淮安　](../Page/淮安.md "wikilink")
18. [常熟厂区](../Page/常熟.md "wikilink")　　
19. 鸿富锦精密电子（[武汉](../Page/武汉.md "wikilink")）有限公司　　
20. 富士迈半导体精密工业（上海）有限公司　　
21. [昆山](../Page/昆山.md "wikilink")　　
22. [嘉善](../Page/嘉善.md "wikilink")　　
23. 鸿富锦精密电子（[重庆](../Page/重庆.md "wikilink")）有限公司 - 重庆西永综合保税区　　
24. [成都](../Page/成都.md "wikilink")　　
25. [衡阳](../Page/衡阳.md "wikilink")　
26. [惠州](../Page/惠州.md "wikilink")[龙溪](../Page/龙溪.md "wikilink")　
27. 鸿富锦精密工业（[深圳](../Page/深圳.md "wikilink")）有限公司 - 富士康科技集团
28. 佛山全亿大科技（[佛山](../Page/佛山.md "wikilink")）有限公司 - 富士康科技集团佛山园区
29. 中山国碁电子（[中山](../Page/中山.md "wikilink")）有限公司 - 富士康科技集团中山园区　　
30. [南寧富桂精密工業有限公司](../Page/南寧.md "wikilink") - 鴻海集團富士康南寧科技園研發檢測認證中心
31. 南京鴻富夏精密電子有限公司
32. 富士康（[贵州](../Page/贵州.md "wikilink")）绿色产业园

此外，集团在[深圳地区还有黄田](../Page/深圳.md "wikilink")、松岗、大浪、石岩厂区、精密机构快速制造中心等基地。　

### 澳大利亞

富士康於[雪梨西部設有工廠和倉庫](../Page/雪梨.md "wikilink")。\[39\]

### 巴西

富士康到[巴西設廠已經有多年](../Page/巴西.md "wikilink")。至今已經建五個廠，員工6000名，在[瑪瑙斯專門製造手機](../Page/瑪瑙斯.md "wikilink")，在[均佳宜則生產電腦](../Page/均佳宜.md "wikilink")。\[40\]

### 歐洲

[Foxconn_Pardubice.JPG](https://zh.wikipedia.org/wiki/File:Foxconn_Pardubice.JPG "fig:Foxconn_Pardubice.JPG")
鴻海科技的[歐洲營運總部暨製造中心位於](../Page/歐洲.md "wikilink")[捷克](../Page/捷克.md "wikilink")[帕爾杜比采](../Page/帕爾杜比采.md "wikilink")。
富士康在[匈牙利](../Page/匈牙利.md "wikilink")\[41\]
，[斯洛伐克](../Page/斯洛伐克.md "wikilink")，[土耳其](../Page/土耳其.md "wikilink")\[42\]、[芬蘭](../Page/芬蘭.md "wikilink")\[43\]和[捷克設有廠房](../Page/捷克.md "wikilink")。

[惠普與富士康曾於](../Page/惠普.md "wikilink")2008年5月投資5,000萬美元興建的工廠，但因[俄羅斯的電腦組裝成本較高](../Page/俄羅斯.md "wikilink")，加上俄羅斯加入[世界貿易組織](../Page/世界貿易組織.md "wikilink")(WTO)後，電腦進口關稅降低的緣故，導致在俄罗斯設廠組裝個人電腦變得無利可圖，因此決定關閉。\[44\]

### 印度

在2015年，富士康宣布將在[印度設立](../Page/印度.md "wikilink")12工廠，將創造約一萬個就業機會。

路透社與印度媒體The Hans
India等報導指出，富士康在印度安德拉邦的工廠在9月開始量產[InFocus手機](../Page/InFocus.md "wikilink")，7至8月分批量產小米、華碩等手機，華碩更因需求強勁，先包下兩條生產線，確保供給順暢。\[45\]

### 馬來西亞

截至2011年，富士康至少有七家工廠於[馬來西亞](../Page/馬來西亞.md "wikilink")。

### 美國

鴻海目前於[美國](../Page/美國.md "wikilink")[威斯康辛州興建面板廠](../Page/威斯康辛州.md "wikilink")。

### 墨西哥

2004年，[墨西哥華瑞茲廠等基地開幕](../Page/墨西哥.md "wikilink")。2011年7月，[思科出售墨西哥工廠給富士康](../Page/思科.md "wikilink")，思科還宣佈把位於墨西哥的[機頂盒生產設施售予富士康](../Page/機頂盒.md "wikilink")，該業務相關的5000名員工也將在8月份隨產品線一同轉移；2011年，富士康在巴西建廠用來組裝iPad和iPhone，預計巴西工廠的年產量在5年達到4億台。\[46\]

### 韓國分公司

鴻海於[韓國設有分公司](../Page/韓國.md "wikilink")，目前約有40多名職員。

### 日本分公司

以[夏普](../Page/夏普.md "wikilink")(SHARP)為[日本核心重要企業公司](../Page/日本.md "wikilink")。

## 争议

  - 白手起家的郭台銘，一向作風強硬，2006年即有[富士康工廠員工揭露](../Page/富士康.md "wikilink")[血汗工廠的黑幕](../Page/血汗工廠.md "wikilink")\[47\]，包含軍事化高壓管理\[48\]、超時加班\[49\]等。對於媒體報導，鴻海控告新聞編採人員，引起中國媒體群起激憤，在鴻海的大客戶[蘋果公司介入後](../Page/蘋果公司.md "wikilink")，戲劇化和解，僅索賠1元。\[50\]
  - 2010年，富士康接連發生員工連環跳的「墜樓事件」（[深圳富士康員工墜樓事件](../Page/深圳富士康員工墜樓事件.md "wikilink")），引起軒然大波，國際科技大廠關注，甚至驚動[國台辦](../Page/国务院台湾事务办公室.md "wikilink")，富士康為「血汗工廠」之名不脛而走\[51\]。在連11跳之後，郭台銘及富士康集團特別向員工發出兩封公開信，郭台銘除了慰問員工、對廠內軟硬體做改善外，另一封信則要求員工簽協定，大意為若再發生自殺事件，員工及家屬不再向富士康要求法律以外過當訴求。\[52\]
  - 2010年5月27日，郭台銘親上火線，眼眶泛紅四度鞠躬道歉，並開放廠房讓媒體參觀採訪，盼以此破除血汗工廠之名，也表示撤回先前之公開信。\[53\]郭台銘返台後，隨即傳出第12跳的消息。
  - 2012年4月29日，郭台銘反擊表示「人家叫我們是血汗工廠，我說有什麼不好？」，他認為流血流汗，只要符合法令。\[54\]

### 2004年鸿海集团与台湾《工商时报》法律纠纷

2004年，鸿海集团因台湾《工商时报》的一系列不實报道，向台北地方法院申请“[假扣押](../Page/假扣押.md "wikilink")”该报记者旷文琪的个人财产并向其个人索赔3000万元新台币。

2004年6月起旷文琪每月薪水被扣1/3，名下财产亦无法处分。12月18日，鸿海集團董事長[郭台铭亲自拜会工商时报并与之沟通](../Page/郭台铭.md "wikilink")，在旷文琪誠意悔過親書悔過書並表示不再做惡意不實的新聞操作后，在工商时报主管同表歉意後，决定收回“假扣押”。並於2004年12月20日，鸿海与工商时报发表共同声明，正式撤回对旷文琪“假扣押”的诉讼请求。

### 2010年深圳富士康跳樓事件

详情：[2010年深圳富士康跳樓事件](../Page/2010年深圳富士康跳樓事件.md "wikilink")

2009年底，富士康[深圳廠區發生首起大陸員工](../Page/深圳.md "wikilink")[跳樓事件](../Page/跳樓.md "wikilink")，跳樓者為一名大陸百大的優秀學生孫丹勇，此為富士康第一起跳樓個案。其後2010年1月\~6月，接二連三發生跳樓案例，總計為媒體揭露報導者總計有[13跳](../Page/深圳富士康員工墜樓事件.md "wikilink")。

2010年5月，集團面對國際勞工組織、外界排山倒海而來的遣責聲浪及輿論壓力紛沓等況，即全體進入戒備狀態，並由郭總裁帶領宣示下啟動集團「愛心平安工程計畫」，包括：大量舖設大樓安全網、引介外部資源（如[台大](../Page/台大.md "wikilink")、[彰化基督教醫院](../Page/彰化基督教醫院.md "wikilink")、[馬偕醫院及大陸四川等醫療團隊及各省家鄉關懷團](../Page/馬偕醫院.md "wikilink")）進行人員[心理輔導及推動](../Page/心理.md "wikilink")[諮商輔導計畫](../Page/諮商.md "wikilink")、培訓心理諮商師及成立78585員工關愛小組等工作，以徹底防範跳樓個案再度發生及其可能產生之連鎖效應。

歷經全面動員後，終於在該年7月即平息危機。此後，富士康大門敞開，不復以往神秘，並促使內地員工加薪，同時也開啟了富士康向內地其他省份建廠、設廠的新里程碑。

### 2011年成都富士康爆炸事件

2011年5月21日19时左右，富士康集团鸿富锦成都公司抛光车间在生产中发生爆炸，造成3人死亡，16人受伤，其中重伤3人。\[55\]

### 2012年鄭州罷工事件

2012年10月5日，[蘋果公司最大代工廠富士康鄭州廠區有](../Page/蘋果公司.md "wikilink")3000至4000名工人罷工，原因是[iPhone
5被客戶投訴品質出現問題](../Page/iPhone_5.md "wikilink")，富士康生產線工人因而壓力倍增，並遷怒於質檢人員，雙方更發生衝突。最後質檢人員選擇罷工，導致多條生產線停產。\[56\]

### 2013年PlayStation 4「質量門」事件

[索尼的PlayStation](../Page/索尼.md "wikilink")
4的生產由[中國大陸富士康所負責](../Page/中國大陸.md "wikilink")，然而在[PlayStation
4發售後](../Page/PlayStation_4.md "wikilink")，部分產品出現品質缺陷。國外遊戲論壇NeoGaf調查問題產品批次及生產地，以及了解到此前[中國央視報導](../Page/中國中央電視台.md "wikilink")[富士康用工不當問題](../Page/富士康.md "wikilink")，相信是富士康的產品製造出現了問題。此前報導的富士康用工不當問題，當中涉及富士康強迫前來富士康實習的大學畢業生加班加點，長期的高壓工作以及缺乏人性關懷導致實習學生以及工人健康問題百出，受害學生以及工人則以所組裝的PS4作為洩憤工具。此次不僅成為了PS4品質問題的輿論焦點，更再次暴露富士康的用工不當、[血汗工廠等問題](../Page/血汗工廠.md "wikilink")。\[57\]
\[58\]

### 2015年[中华全国总工会事件](../Page/中华全国总工会.md "wikilink")

2015年2月2日，[中华全国总工会通报了](../Page/中华全国总工会.md "wikilink")2014年度中国劳动关系领域十起具有典型意义的违法案件和劳动事件。全国总工会书记处书记、法律工作部部长[郭军点名批评了富士康长时间违法加班](../Page/郭军.md "wikilink")\[59\]。随后富士康对此事作出了回应\[60\]。

## 其它

  - 2014年热播的纪录片《[舌尖上的中国](../Page/舌尖上的中国.md "wikilink")》第二季第七集，其部分镜头是在富士康深圳工厂取景的\[61\]。
  - 2017年5月由於鴻海瞄準東芝出售半導體部門案在日本政經界引發的議論，擔憂繼[夏普後日本大型企業在財務困難時期被外商連連收購](../Page/夏普.md "wikilink")，導致技術外流最終日本喪失一切競爭力，日參院通過《外匯修正法》實質禁止了許多收購活動，被戲稱鴻海條款。\[62\]

## 關係企業

  - [鴻海精密](../Page/鴻海精密.md "wikilink")（）
  - [鴻準精密工業](../Page/鴻準精密工業.md "wikilink")（）
  - [富士康工業互聯網股份有限公司](../Page/富士康工業互聯網.md "wikilink")（）
  - [鴻揚創投](../Page/鴻揚創投.md "wikilink")
  - [樺漢](../Page/樺漢.md "wikilink")（）
  - [瑞祺電通](../Page/瑞祺電通.md "wikilink")（）
  - [正達國際光電](../Page/正達國際光電.md "wikilink")（）
  - [廣宇科技](../Page/廣宇科技.md "wikilink")（）
  - [建漢科技](../Page/建漢科技.md "wikilink")（）
  - [台揚科技](../Page/台揚科技.md "wikilink")（）
  - [賜福科技](../Page/賜福科技.md "wikilink")
  - [京鼎精密機械有限公司](../Page/京鼎精密機械有限公司.md "wikilink")（）
  - [群創光電](../Page/群創光電.md "wikilink") （）
  - [榮創能源科技](../Page/榮創能源科技.md "wikilink") （）
  - [富智康集團](../Page/富智康集團.md "wikilink")（）
  - [鸿富锦精密电子(成都)有限公司](../Page/鸿富锦精密电子\(成都\)有限公司.md "wikilink")
  - [富泰華工業(深圳)有限公司](../Page/富泰華工業\(深圳\)有限公司.md "wikilink")
  - [深圳富泰宏精密工業有限公司](../Page/深圳富泰宏精密工業有限公司.md "wikilink")
  - [F_臻鼎](../Page/F_臻鼎.md "wikilink")（）

<!-- end list -->

  - [F_訊芯](../Page/F_訊芯.md "wikilink")（）
  - [F_乙盛](../Page/F_乙盛.md "wikilink")（）
  - [F_GIS](../Page/F_GIS.md "wikilink")（）
  - [正一特殊材料股份有限公司](../Page/正一特殊材料股份有限公司.md "wikilink")
  - [三創數位股份有限公司](../Page/三創數位股份有限公司.md "wikilink")
  - [安泰電業股份有限公司](../Page/安泰電業股份有限公司.md "wikilink")
  - [合惠集團](../Page/合惠集團.md "wikilink")
  - [亞太電信](../Page/亞太電信.md "wikilink")（）
  - [富士康連接器科技公司](../Page/富士康連接器科技公司.md "wikilink")
  - [SK C\&C](../Page/SK_C&C.md "wikilink") 4.9%
  - [雲智匯科技](../Page/雲智匯科技.md "wikilink")（）
  - [夏普](../Page/夏普.md "wikilink")（）
  - [芳舟生活科技](../Page/芳舟生活科技.md "wikilink")
  - 台灣寬頻通訊
  - [訊智海國際](../Page/訊智海國際.md "wikilink")
  - [鴻騰六零八八精密科技](../Page/鴻騰六零八八精密科技.md "wikilink")（）
  - [鴻圖股份有限公司](../Page/鴻圖股份有限公司.md "wikilink")
  - [業成科技(深圳)有限公司](../Page/業成科技\(深圳\)有限公司.md "wikilink")

## 注釋

<div class="references-2column">

</div>

## 參考資料

<div class="references-small" style="height: 220px; overflow: auto; padding: 3px">

</div>

## 外部連結

  - [Foxconn Technology Group](http://www.foxconn.com/)
  - [鴻海科技集團](http://www.foxconn.com.tw/)
  - [富士康科技集团](http://www.foxconn.com.cn/)
  - [富连网](http://www.flnet.com/)
  - [鴻圖股份有限公司](http://omniguider.pano3d.tw/)

[鴻海集團](../Category/鴻海集團.md "wikilink")
[Category:台灣電子公司](../Category/台灣電子公司.md "wikilink")
[Category:總部在台灣的跨國公司](../Category/總部在台灣的跨國公司.md "wikilink")
[Category:主板製造商](../Category/主板製造商.md "wikilink")
[Category:1974年成立的公司](../Category/1974年成立的公司.md "wikilink")

1.

2.  Terry Gou founded Hon Hai Precision Industry Company Ltd, the anchor
    company of Foxconn Technology Group in 1974 with US$7,500

3.

4.

5.
6.
7.
8.
9.
10.

11.

12. ,ISBN 9789571142265

13.
14.

15.

16.
17.
18.
19.
20.
21. [手機代工版圖，鴻海藉由併購奇美通訊再下一城](http://cdnet.stpi.narl.org.tw/techroom/analysis/pat041.htm)

22.
23.
24.
25.
26.
27. , ISBN 9789571145136

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.
38.

39. 梁燕蕙.[富士康澳洲外包工廠被踢爆為血汗工廠](http://www.digitimes.com.tw/tw/dt/n/shwnws.asp?id=0000208073_0KQL6UU08CK34R28M4622).DIGITIMES.2010-11-15

40. 彭漣漪.[直擊富士康新廠， 5年投資120億美元？](http://www.gvm.com.tw/Boardcontent_18931.html)遠見雜誌2011年10月號.2011-09-29

41.

42. ["Foxconn Turkey"](http://www.foxconn.com/tr/en/hakkimizda.html).
    foxconn.com. 2015-01-13.

43. 李宜儒.[富士康芬蘭手機廠裁員](http://www.appledaily.com.tw/appledaily/article/finance/20120603/34273141/).蘋果日報.2012-06-03

44. [銷售不佳　惠普關閉俄國工廠](http://www.appledaily.com.tw/realtimenews/article/new/20150803/660903/).蘋果日報2015-08-03

45. [鴻海印度製造
    再下一城](http://udn.com/news/story/7240/1247713-%E9%B4%BB%E6%B5%B7%E5%8D%B0%E5%BA%A6%E8%A3%BD%E9%80%A0-%E5%86%8D%E4%B8%8B%E4%B8%80%E5%9F%8E)

46. [除了中國大陸
    富士康還在全球哪些地區設有工廠？](http://big5.huaxia.com/tslj/qycf/2012/07/2938696.html).華夏經緯網.2012-07-26

47.

48.

49.

50.

51.

52.

53.

54.

55. [富士康成都公司车间爆炸造成2死16伤](http://news.sina.com.cn/c/2011-05-21/083622504454.shtml).新浪网.2011-05-21

56. [iPhone5掉漆引爆富士康工潮 蘋果逼加強質檢
    鄭州廠工人內訌](https://archive.is/20130105124940/hk.news.yahoo.com/iphone5%E6%8E%89%E6%BC%86%E5%BC%95%E7%88%86%E5%AF%8C%E5%A3%AB%E5%BA%B7%E5%B7%A5%E6%BD%AE-%E8%98%8B%E6%9E%9C%E9%80%BC%E5%8A%A0%E5%BC%B7%E8%B3%AA%E6%AA%A2-%E9%84%AD%E5%B7%9E%E5%BB%A0%E5%B7%A5%E4%BA%BA%E5%85%A7%E8%A8%8C-213329642.html).明報.2012-10-07

57.

58.

59.

60.

61.

62. 黃菁菁.[日參院通過《外匯修正法》阻外資併購日商取得高端技術](https://ctee.com.tw/mobile/NowNews.aspx?nid=20170517005525-260408&ch=gj).工商時報.2017-05-17