[Hong_Kong_Daily_News_2015-07-11.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Daily_News_2015-07-11.jpg "fig:Hong_Kong_Daily_News_2015-07-11.jpg")
《**新報**》（）是[香港一份已停刊的](../Page/香港.md "wikilink")[繁體中文](../Page/繁體中文.md "wikilink")[報紙](../Page/報紙.md "wikilink")，創刊於1959年10月5日，創辦人為環球圖書雜誌出版社老闆[羅斌](../Page/羅斌_\(出版界\).md "wikilink")。停刊前由[英皇集團](../Page/英皇集團.md "wikilink")[董事局主席](../Page/董事局主席.md "wikilink")[楊受成擁有](../Page/楊受成.md "wikilink")，並為英皇集團旗下印刷媒介業務，[何鴻燊亦是股東之一](../Page/何鴻燊.md "wikilink")。雖然報紙已停刊，但仍以新報馬簿及新報馬經名義發行刊物。

## 歷史

《新報》於1959年由環球圖書雜誌出版社老闆羅斌創辦\[1\]，後來在港上市後，被由姓郭[台灣商人買下](../Page/台灣.md "wikilink")，1992年由英皇集團楊受成收購，[何鴻燊亦曾入股](../Page/何鴻燊.md "wikilink")，而新報曾於2007年-2010年間取消風月版。

1988年6月6日，曾以**新系集團有限公司**上市，之後出售予林太珏，同時購回業務，更名為[鷹馳實業](../Page/鷹馳實業.md "wikilink")，再更名為[超越集團至今](../Page/超越集團.md "wikilink")。

1999年3月，《[太陽報](../Page/太陽報_\(香港\).md "wikilink")》創刊所引發的報章減價戰期間，《新報》每份零售價只是1元，該零售價與1980年代初的零售價相去不遠。

報社於2008年由[石塘咀](../Page/石塘咀.md "wikilink")[德輔道西](../Page/德輔道西.md "wikilink")444號香港工業大廈，遷至[筲箕灣亞公岩村道](../Page/筲箕灣.md "wikilink")3號川匯集團大廈5及6樓，及後再遷往[柴灣安全貨倉工業大廈](../Page/柴灣.md "wikilink")11樓。

2009年7月17日，《新報》社長[胡雪麗向全體員工發出內部通告](../Page/胡雪麗.md "wikilink")，宣佈[何鴻燊最近正式入股該報](../Page/何鴻燊.md "wikilink")。通告強調入股《新報》純屬支持報社，何鴻燊不會過問任何編採運作事宜，《新報》編採獨立不變，一切如常。惟並無透露入股金額及所佔股權比例，亦未知是以個人名義入股抑或上市公司投資\[2\]。

2009年11月中，總編輯[郭一鳴離職後](../Page/郭一鳴.md "wikilink")，由社長[胡雪麗兼任總編輯一職](../Page/胡雪麗.md "wikilink")。

2010年9月，[胡雪麗離職](../Page/胡雪麗.md "wikilink")，由資深傳媒人[馮兆榮接任](../Page/馮兆榮.md "wikilink")\[3\]，內容改為主攻波經、馬經，但[馮兆榮接任僅幾個月](../Page/馮兆榮.md "wikilink")，《新報》的銷量卻急跌。

2015年7月11日下午，新報於網站發表聲明，表示隨著[免費報章出現](../Page/免費報紙.md "wikilink")，傳統報章近年銷量大幅下降，加上讀者讀報習慣有所改變，該報章長期處於虧蝕狀態，決定7月12日起停刊，所有受影響員工將獲合理賠償及妥善安排。\[4\]\[5\]。

但於2015年9月3日，繼續發行《新報馬簿》，另外新報馬經版部份以《新報馬經》發行，並保留馬經版原班人馬。新報馬簿在2016年7-8月由經營新加坡賽馬網站iRace
Media Pte Ltd全資收購，正式脫離英皇集團。

## 海外分支

早年也曾在[美國及](../Page/美國.md "wikilink")[澳洲開辦](../Page/澳洲.md "wikilink")《新報》，近年則在[澳門創辦](../Page/澳門.md "wikilink")《新報今日澳門》，逢周一至周五於澳門免費派發。

## 其他出版刊物

  -
    **新報馬簿**，逢[香港賽馬排位日出版](../Page/香港賽馬.md "wikilink")，於新報停止發行後於2015年9月繼續發售，每本30港元。
    **新報馬經**，由新報馬經版分拆，逢賽前一天下午出版。由2015年9月4日發行，每份7港元。
    **iRace**，香港賽馬會授權於馬場內發行的賽馬刊物，分中英文版本。2015年9月起由[蘋果日報接手](../Page/蘋果日報_\(香港\).md "wikilink")。

## 參考資料

## 外部連結

  - [新報網站](https://web.archive.org/web/20150711095122/http://www.hkdailynews.com.hk/)（2015年7月）

  - [新報馬簿網站](http://www.hkdnracing.com/)

[Category:香港親共報紙](../Category/香港親共報紙.md "wikilink")
[Category:香港已停刊報紙](../Category/香港已停刊報紙.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:1959年建立的出版物](../Category/1959年建立的出版物.md "wikilink")
[Category:英皇集團](../Category/英皇集團.md "wikilink")

1.  [1959年創刊 07年曾重組裁員](http://hk.apple.nextmedia.com/realtime/news/20150711/53959828)《[蘋果日報](../Page/蘋果日報.md "wikilink")》
2.  [賭王入股新報](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20090719&sec_id=4104&subsec_id=15333&art_id=13005072&cat_id=45&coln_id=20)《[蘋果日報](../Page/蘋果日報.md "wikilink")》
3.  [隔牆有耳：《新報》《成報》又換主帥](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20100911&sec_id=4104&subsec_id=15333&art_id=14439487&cat_id=45&coln_id=20)
    [蘋果日報](../Page/蘋果日報.md "wikilink")
4.
5.