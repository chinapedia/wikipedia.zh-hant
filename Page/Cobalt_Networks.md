**Cobalt Networks**公司於1996年在[加州的](../Page/加州.md "wikilink")[Mountain
View成立](../Page/Mountain_View.md "wikilink")，是一家專門研發產製低價[Linux伺服器](../Page/Linux.md "wikilink")（也稱：伺服器）的業者，當時稱為Cobalt
Microserver Inc.。該公司也是易用型伺服應用機（[Server
Appliance](../Page/Server_Appliance.md "wikilink")，也稱：精簡型伺服器）的先鋒業者，而產品的訴求用戶主要為[網際服務供應商](../Page/網際服務供應商.md "wikilink")（[Internet
Service
Provider](../Page/因特网服务提供商.md "wikilink")；[ISP](../Page/ISP.md "wikilink")）及[中小企業](../Page/中小企業.md "wikilink")（[Small
& Medium
Business](../Page/Small_&_Medium_Business.md "wikilink")；[SMB](../Page/SMB.md "wikilink")），用戶可以透過具安全防護性的遠端遙控、傳輸來操作管理伺服應用機，該機自身已具多種智慧型（也稱：智能型）的網路環境與組態（也稱：配置）偵測能力，並自行完成多數的合適設定，使用者只要就數個簡單項目進行檢視、調整，就可立即享用網路服務，往後在維護管理上也相當輕鬆省力。

之後到了1999年11月5日，Cobalt公司成功的完成[首次公开募股](../Page/首次公开募股.md "wikilink")（Initial
Public
Offerings；[IPO](../Page/IPO.md "wikilink")），其[NASDAQ股票代號為](../Page/NASDAQ.md "wikilink")**COBT**，股票首次掛牌公開發行後，從最初的22美元一路飆漲到128.13美元坐收。一年後，[昇陽電腦](../Page/昇陽電腦.md "wikilink")（Sun
Microsystems）以換股方式併購Cobalt
Networks公司，[昇陽電腦期望透過此一併購能夠與其他](../Page/昇陽電腦.md "wikilink")[Linux伺服器的業者競爭](../Page/Linux.md "wikilink")。

不過，Cobalt成為[昇陽電腦的新系列產品線後](../Page/昇陽電腦.md "wikilink")（Cobalt一稱從公司名稱轉變成產品的系列名稱），除了有些初期的成功外，之後就逐漸不斷地走弱，到了2003年已停產多款Cobalt系列的產品，到了2004年2月19日則完全停產Cobalt系列的各項產品，但[昇陽電腦仍承諾持續提供三年的後續支援服務](../Page/昇陽電腦.md "wikilink")，自此Cobalt完全走入歷史。最後[昇陽電腦將Cobalt產品的](../Page/昇陽電腦.md "wikilink")[軟體](../Page/軟體.md "wikilink")、[韌體程式釋出成](../Page/韌體.md "wikilink")[開放原碼](../Page/開放原碼.md "wikilink")（也稱：源代碼）。

## 附註

1.  \-
    [昇陽電腦當時提供的股數再對應當時](../Page/昇陽電腦.md "wikilink")[昇陽電腦在NASDAQ的股價](../Page/昇陽電腦.md "wikilink")（每股約65美元），如此換股併購的金額約在20億美元左右。

2.  \-
    許多市場分析觀察家認為，[昇陽電腦成立以來最大且最失敗的併購案即是收併Cobalt](../Page/昇陽電腦.md "wikilink")
    Networks，不過觀察家也認為昇陽電腦之後所收購的[SeeBeyond](../Page/SeeBeyond.md "wikilink")、[StorageTek等公司也可能成為一場大賭注](../Page/StorageTek.md "wikilink")。

## 相關參考

  - [Cobalt開放原碼軟體的貯庫：BlueQuartz](http://bluequartz.org/) -**（英文）**
  - [發酸的市場致使Linux概念股跌破首次公開掛牌時的股價（CNET）](https://archive.is/20130119190431/http://news.com.com/2100-1001-239298.html?legacy=cnet)
    -**（英文）**
  - [昇陽電腦的Cobalt伺服器軟體以開放原碼方式重生（eWeek.com）](http://www.eweek.com/article2/0,1759,1426299,00.asp)
    -**（英文）**
  - [昇陽停產Cobalt精簡型伺服器](http://taiwan.cnet.com/news/hardware/0,2000064553,20086766,00.htm)
    -**（中文）**

[Category:昇陽電腦](../Category/昇陽電腦.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")