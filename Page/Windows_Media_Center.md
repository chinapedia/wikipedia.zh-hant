**Windows Media Center**是一個媒體中心應用程式，內置在[Windows
XP](../Page/Windows_XP.md "wikilink") Media、[Windows
Vista家用進階版及旗艦版和](../Page/Windows_Vista.md "wikilink")[Windows
7家用進階版](../Page/Windows_7.md "wikilink")、專業版、企業版及旗艦版。[Windows
8Pro和](../Page/Windows_8.md "wikilink")[Windows
8.1Pro用户则需要单独购买升级密钥将Windows](../Page/Windows_8.1.md "wikilink")
Media Center加入到Windows 8/8.1。（[Windows 8
Enterprise不支持Windows](../Page/Windows_8_Enterprise.md "wikilink")
Media Center。）[Windows
10將不支援](../Page/Windows_10.md "wikilink")（可通过第三方安装在Windows
10），包括以電影投影片形式觀看圖片、瀏覽並播放[音樂](../Page/音樂.md "wikilink")、播放[DVR](../Page/DVR.md "wikilink")、收看並錄製[電視節目](../Page/電視節目.md "wikilink")、下載[電影](../Page/電影.md "wikilink")、放映家庭影片等。此外，Windows
Media
[DVR和](../Page/DVR.md "wikilink")[DVR燒錄功能](../Page/DVR.md "wikilink")。

## 程式開發

WM是一種可程式化平台，目前有API

## Windows Media

微軟提供\[WM\]

## 外部链接

  - [Windows Media Center主页](http://www.windowsmediacenter.com/)
  - [Windows Vista： 功能介紹：Windows Media
    Center](http://www.microsoft.com/us/windows/chinese/products/windowsvista/features/details/mediacenter.mspx)
  - [Windows XP Media Center
    Edition 2005主頁](http://www.microsoft.com/windowsxp/mediacenter/default.mspx)

[Category:Windows组件](../Category/Windows组件.md "wikilink")
[Category:媒体播放器](../Category/媒体播放器.md "wikilink")
[Category:隨選視訊服務](../Category/隨選視訊服務.md "wikilink")