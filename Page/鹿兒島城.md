**鹿兒島城**是位於日本[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[鹿兒島市](../Page/鹿兒島市.md "wikilink")[城山山麓的城堡](../Page/城山.md "wikilink")。是一座平山城，別名是「鶴丸城」，被鹿兒島縣指定為鹿兒島縣定古蹟，[江戶時代是](../Page/江戶時代.md "wikilink")[島津氏的居城](../Page/島津氏.md "wikilink")。

## 歷史

[Kagoshima_Castle_air.jpg](https://zh.wikipedia.org/wiki/File:Kagoshima_Castle_air.jpg "fig:Kagoshima_Castle_air.jpg")

1601年由[島津忠恒](../Page/島津忠恒.md "wikilink")（家久）開始進行築城計劃。在此之前島津家在[關原之戰中敗陣](../Page/關原之戰.md "wikilink")，父親[島津義弘負上全責](../Page/島津義弘.md "wikilink")。由忠恒成為新的當主。開始建造取代[內城的鶴丸城](../Page/內城.md "wikilink")，1604年築城完畢。

忠恒的父親義弘認為海岸附近築城是有問題，在建成前一直反對築城計劃。鹿兒島城在1693年被燒毀。1874年再被燒毀，自此再無重建。現在建築為大手門及石橋。

## 關連項目

  - [西南戰爭](../Page/西南戰爭.md "wikilink")

## 外部連結

  - [鹿兒島城挖掘調查](http://www.jomon-no-mori.jp/no26.htm)

  - [鹿兒島城](http://www.geocities.jp/shanehashi/Travel/Japan/Oshiromeguri/KagoshimaJyo.html)

  -
[Category:鹿兒島縣城堡](../Category/鹿兒島縣城堡.md "wikilink")
[Category:鹿兒島市建築物](../Category/鹿兒島市建築物.md "wikilink")
[Category:薩摩藩](../Category/薩摩藩.md "wikilink")
[Category:鹿兒島縣重要文化財](../Category/鹿兒島縣重要文化財.md "wikilink")
[Category:安土桃山時代建築](../Category/安土桃山時代建築.md "wikilink")
[Category:奧州島津氏](../Category/奧州島津氏.md "wikilink")
[Category:日本史跡](../Category/日本史跡.md "wikilink")