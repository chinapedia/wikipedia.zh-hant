**凯蒂·斯佳丽·奥哈拉**（[英文](../Page/英文.md "wikilink")：），又译**郝思嘉**，是[玛格丽特·米切尔](../Page/玛格丽特·米切尔.md "wikilink")1936年[小说](../Page/小说.md "wikilink")《[飘](../Page/飘.md "wikilink")》和1939年改编电影《[乱世佳人](../Page/乱世佳人_\(电影\).md "wikilink")》的女主角。她还是1970年音乐剧《[斯佳丽](../Page/斯佳丽_\(音乐剧\).md "wikilink")》和1991年小说续集《[斯佳丽](../Page/斯佳丽_\(小说\).md "wikilink")》的女主角，1994年被改编成电视系列剧。小说的初稿中，米切尔提到女主角名字为潘西（Pansy），直到出版时才改为斯佳丽（Scarlett）。

[Category:文学角色](../Category/文学角色.md "wikilink")
[Category:虛構愛爾蘭裔美國人](../Category/虛構愛爾蘭裔美國人.md "wikilink")
[Category:虛構法國裔美國人](../Category/虛構法國裔美國人.md "wikilink")
[Category:虚构商人](../Category/虚构商人.md "wikilink")
[Category:来自佐治亚州的虚构角色](../Category/来自佐治亚州的虚构角色.md "wikilink")
[Category:虛構謀殺犯](../Category/虛構謀殺犯.md "wikilink")
[Category:虚构社会名流](../Category/虚构社会名流.md "wikilink")