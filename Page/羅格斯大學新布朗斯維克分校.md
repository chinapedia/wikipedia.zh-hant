**罗格斯大學-新布朗斯維克分校**（Rutgers University，New Brunswick
Campus）是[罗格斯大学的主要分校](../Page/罗格斯大学.md "wikilink")，也是[罗格斯大学三个分校中面积最大](../Page/罗格斯大学.md "wikilink")，排名最高的校區。位于[新布朗斯維克和](../Page/新布朗斯維克.md "wikilink")[皮斯卡特維](../Page/皮斯卡特維.md "wikilink")，[新泽西州](../Page/新泽西.md "wikilink")。

[Old_Queens,_New_Brunswick,_NJ_-_looking_north,_2014.jpg](https://zh.wikipedia.org/wiki/File:Old_Queens,_New_Brunswick,_NJ_-_looking_north,_2014.jpg "fig:Old_Queens,_New_Brunswick,_NJ_-_looking_north,_2014.jpg")

## 學院組成

新布朗斯威克校园包括以下校園和学院：

  - Cook
    College，库克學院。课程侧重于[生命科学](../Page/生命科学.md "wikilink")，[农学及](../Page/农学.md "wikilink")[海洋科学](../Page/海洋科学.md "wikilink")。
  - Douglass College，道格拉斯學院。只收女性學生，其课程综合性较强。
  - Livingston
    College，李文斯頓學院。其學院主旨為增進多樣性。课程侧重于[社会科学](../Page/社会.md "wikilink")。
  - Rutgers
    College，羅格斯學院。為羅格斯大學最初的學院。课程侧重于[经济和](../Page/经济.md "wikilink")[语言学科](../Page/语言.md "wikilink")。
  - College of Nursing，[護理学院](../Page/護理.md "wikilink")。
  - Ernest Mario School of Pharmacy，[藥學院](../Page/藥學.md "wikilink")。
  - Graduate School of Applied and Professional
    Psychology，[心理学院](../Page/心理学.md "wikilink")。
  - Graduate School of Education，[教育学院](../Page/教育学.md "wikilink")。
  - Mason Gross School of the
    Arts，梅森·格羅斯[藝術学院](../Page/藝術.md "wikilink")。
  - Rutgers Business School-New
    Brunswick，罗格斯大学商学院-[新布朗斯維克分校](../Page/新布朗斯維克.md "wikilink")
  - School of Communication, Information and Library
    Studies，[通讯](../Page/通讯.md "wikilink")，[信息暨](../Page/信息.md "wikilink")[图书馆学院](../Page/图书馆.md "wikilink")。
  - School of Engineering，[工程学院](../Page/工程.md "wikilink")。
  - School of Management and Labor
    Relations，[管理暨](../Page/管理.md "wikilink")[勞工與人力資源学院](../Page/勞工與人力資源.md "wikilink")。
  - School of Social Work，[社会工作学院](../Page/社会.md "wikilink")。

[Category:羅格斯大學](../Category/羅格斯大學.md "wikilink")