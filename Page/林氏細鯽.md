**林氏細鯽**（學名：**），[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鯉形目](../Page/鯉形目.md "wikilink")[鯉科魚類](../Page/鯉科.md "wikilink")，見於[廣東](../Page/廣東.md "wikilink")、[海南](../Page/海南.md "wikilink")、[广西零星地區](../Page/广西.md "wikilink")，。1936年由Herre,
Albert
W.採集於[香港](../Page/香港.md "wikilink")[粉嶺](../Page/粉嶺.md "wikilink")，1966年由Weitzman
&
Chan辨別為新種。多棲息於在淡水[溪流](../Page/溪流.md "wikilink")。1992年於[大帽山发现过一次](../Page/大帽山.md "wikilink")，此後再無記錄，相信在香港已野外滅絕。灭绝原因估計是生境退化和外來種競爭（主要是[食蚊魚](../Page/食蚊魚.md "wikilink")*Gambusia
affinis*）的共同影響。

本魚身體土黃色的顏色，藍色的背面與在腹面上的白色；身體有的中動脈軸淡紅黃色的狹窄區域，在它下面的區域與光亮的藍綠色線上面的藍黑色的線；尾鰭基底具有一個大的藍色斑點而且斑點具有一個金的邊緣；鰭灰白的白色，體長可達5公分，可做為觀賞魚。

## 参考文献

  -
  - [香港生物數據庫](http://www.afcd.gov.hk/tc_chi/conservation/hkbiodiversity/database/resultlist.asp?lang=tc)

  - [環境保護署 -
    策略性環境評估](http://www.epd.gov.hk/epd/tc_chi/environmentinhk/eia_planning/sea2005/baseline_4_13.html)

  - [中國科學院動物研究所《中國動物物種編目數據庫》](https://web.archive.org/web/20131203122335/http://www.bioinfo.cn/db05/BjdwSpecies.php?action=view)

  - [中國水產科學院](http://www.fishinfo.cn/page/mulresultbrw-info.cbs?ResultFile=c%3A%5Ctemp%5Ctbs%5CD29F26F%2Etmp&order=1&begin=1&RecordCount=1)

  - [大公網-林氏細鯽](http://www.takungpao.com.hk/news/09/08/09/KJXZ-1124256.htm)

[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:中國魚類](../Category/中國魚類.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[lini](../Category/細鯽屬.md "wikilink")