[Ma_Tau_Wai_Road.jpg](https://zh.wikipedia.org/wiki/File:Ma_Tau_Wai_Road.jpg "fig:Ma_Tau_Wai_Road.jpg")
[Tokwawan_Market_and_Government_Offices.jpg](https://zh.wikipedia.org/wiki/File:Tokwawan_Market_and_Government_Offices.jpg "fig:Tokwawan_Market_and_Government_Offices.jpg")\]\]
[MaTauWaiRoad_FarmRoad_2017.jpg](https://zh.wikipedia.org/wiki/File:MaTauWaiRoad_FarmRoad_2017.jpg "fig:MaTauWaiRoad_FarmRoad_2017.jpg")
[Ma_Tau_Wai_Road_45J_Building_200811.jpg](https://zh.wikipedia.org/wiki/File:Ma_Tau_Wai_Road_45J_Building_200811.jpg "fig:Ma_Tau_Wai_Road_45J_Building_200811.jpg")
**馬頭圍道**（），昔日命名為**碼頭圍道**，是香港[九龍的一條道路](../Page/九龍.md "wikilink")，橫跨[馬頭圍](../Page/馬頭圍.md "wikilink")、[土瓜灣及](../Page/土瓜灣.md "wikilink")[紅磡](../Page/紅磡.md "wikilink")3地，北至[馬頭涌道](../Page/馬頭涌道.md "wikilink")，南至[蕪湖街](../Page/蕪湖街.md "wikilink")。在1965年10月17日前馬頭圍道及[土瓜灣道的交匯處為八字型的大型迴旋處](../Page/土瓜灣道.md "wikilink")，為改善該處的交通情況把馬頭圍道在[東九龍走廊分拆](../Page/東九龍走廊.md "wikilink")，馬頭圍道北部的車輛須要轉入[漆咸道北](../Page/漆咸道.md "wikilink")，土瓜灣道的車輛須要轉入馬頭圍道南部。現馬頭圍道中間可以看到有行人路將兩段分隔，在該段行人路中有巴士站。\[1\]

馬頭圍道是紅磡區的交通樞紐，車輛多由[九龍城前往](../Page/九龍城.md "wikilink")[尖沙咀及](../Page/尖沙咀.md "wikilink")[紅磡](../Page/紅磡.md "wikilink")。昔日[啟德機場還在使用時是車輛前往機場的主要道路](../Page/啟德機場.md "wikilink")，使馬頭圍道十分擠迫。

## 沿線街道

馬頭圍道貫穿九龍城區[紅磡](../Page/紅磡.md "wikilink")、[大環](../Page/大環_\(九龍\).md "wikilink")、[鶴園](../Page/鶴園.md "wikilink")、[土瓜灣和](../Page/土瓜灣.md "wikilink")[馬頭圍](../Page/馬頭圍.md "wikilink")，與之相交的街道多達30條(由南至北排序)：

  - [蕪湖街](../Page/蕪湖街.md "wikilink")
  - [差館里](../Page/差館里.md "wikilink")
  - [大環道](../Page/大環道.md "wikilink")
  - [佛光街](../Page/佛光街.md "wikilink")/[民裕街](../Page/民裕街.md "wikilink")(另有佛光街天橋在上方駛過)
  - [青州街](../Page/青州街.md "wikilink")
  - [鶴園街](../Page/鶴園街.md "wikilink")
  - [北拱街](../Page/北拱街.md "wikilink")
  - [新柳街](../Page/新柳街.md "wikilink")
  - [機利士北路](../Page/機利士路.md "wikilink")
  - [庇利街](../Page/庇利街.md "wikilink")
  - [石塘街](../Page/石塘街.md "wikilink")
  - [榮光街](../Page/榮光街.md "wikilink")
  - [啟明街](../Page/啟明街.md "wikilink")(南段終點，連接[土瓜灣道](../Page/土瓜灣道.md "wikilink"))
  - (北段起點，連接[漆咸道北](../Page/漆咸道北.md "wikilink"))
  - [江西街](../Page/江西街.md "wikilink")
  - [安徽街](../Page/安徽街.md "wikilink")
  - [浙江街](../Page/浙江街.md "wikilink")
  - [江蘇街](../Page/江蘇街.md "wikilink")
  - [落山道](../Page/落山道.md "wikilink")
  - [上鄉道](../Page/上鄉道.md "wikilink")
  - [永耀街](../Page/永耀街.md "wikilink")
  - [鴻光街](../Page/鴻光街.md "wikilink")
  - [天光道](../Page/天光道.md "wikilink")/[北帝街](../Page/北帝街.md "wikilink")
  - [農圃道](../Page/農圃道.md "wikilink")
  - [譚公道](../Page/譚公道.md "wikilink")
  - [新山道](../Page/新山道.md "wikilink")
  - [馬頭涌道](../Page/馬頭涌道.md "wikilink")

## 途經的公共交通服務

<div class="NavFrame collapsed" style="background:yellow; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background:yellow; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background:yellow;margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[何文田站](../Page/何文田站.md "wikilink")、[黃埔站](../Page/黃埔站.md "wikilink")
  - <font color="{{屯馬綫色彩}}">█</font>[屯馬綫](../Page/屯馬綫.md "wikilink")：[土瓜灣站](../Page/土瓜灣站.md "wikilink")（建造中，2019年啟用）

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 油塘至佐敦道線\[2\]
  - 藍田至佐敦道線\[3\]
  - 秀茂坪至佐敦道線\[4\]
  - 觀塘至佐敦道線\[5\]
  - 牛頭角至佐敦道線\[6\]
  - 慈雲山至灣仔線\[7\]
  - 慈雲山至銅鑼灣線\[8\]
  - 銅鑼灣至觀塘線\[9\]
  - 中環/灣仔至觀塘線\[10\]
  - 西貢至銅鑼灣線\[11\]
  - 觀塘至西環線\[12\]
  - 觀塘至香港仔線\[13\]
  - 紅磡至觀塘線\[14\]
  - 紅磡至旺角線\[15\]
  - 美孚至土瓜灣線\[16\]
  - 土瓜灣至旺角線\[17\]
  - 荃灣至土瓜灣線\[18\]
  - 土瓜灣至青山道線\[19\]
  - 何文田邨至紅磡線\[20\]

</div>

</div>

## 沿線事故

### 唐樓倒塌

2010年1月29日，[鶴園馬頭圍道](../Page/鶴園.md "wikilink")45號J一幢樓齡達55年的六層高唐樓全幢倒塌，造成4人死亡，2人受傷。

### 唐樓火災

2011年6月15日，[土瓜灣馬頭圍道](../Page/土瓜灣.md "wikilink")111號一幢八層高[唐樓於凌晨](../Page/唐樓.md "wikilink")3時左右起火，火勢於3時47分升為三級，大火在黎明時份才撲熄。事件共造成4人死亡，19人受傷。該幢唐樓部份單位被人間成多個分租房間，俗稱[劏房](../Page/劏房.md "wikilink")，令市民關注該類屋宇的消防安全。

## 参考文献

## 相關條目

  - [馬頭涌道](../Page/馬頭涌道.md "wikilink")
  - [馬頭角道](../Page/馬頭角道.md "wikilink")
  - [土瓜灣道](../Page/土瓜灣道.md "wikilink")
  - [東九龍走廊](../Page/東九龍走廊.md "wikilink")

[Category:紅磡街道](../Category/紅磡街道.md "wikilink")
[Category:土瓜灣街道](../Category/土瓜灣街道.md "wikilink")
[Category:馬頭圍街道](../Category/馬頭圍街道.md "wikilink")

1.  香港地方．被分開的道路 [1](http://www.hk-place.com/view.php?id=304#tkwr)
2.  [佐敦道吳松街　—　油塘及鯉魚門](http://www.16seats.net/chi/rmb/r_k19.html)
3.  [佐敦道寧波街　—　藍田](http://www.16seats.net/chi/rmb/r_k55.html)
4.  [秀茂坪　—　佐敦道北海街](http://www.16seats.net/chi/rmb/r_k95.html)
5.  [觀塘同仁街　—　佐敦道上海街](http://www.16seats.net/chi/rmb/r_k12.html)
6.  [牛頭角站　—　佐敦道吳松街](http://www.16seats.net/chi/rmb/r_k56.html)
7.  [慈雲山　＞　銅鑼灣及灣仔](http://www.16seats.net/chi/rmb/r_kh32.html)
8.  [慈雲山　—　銅鑼灣鵝頸橋](http://www.16seats.net/chi/rmb/r_kh16.html)
9.  [銅鑼灣鵝頸橋　—　牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh01.html)
10. [中環／灣仔　＞　牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_kh41.html)
11. [西貢市中心　—　銅鑼灣登龍街](http://www.16seats.net/chi/rmb/r_hn98.html)
12. [觀塘宜安街　＞　西環卑路乍街,
    西環修打蘭街　＞　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh18.html)
13. [香港仔湖北街　—　觀塘宜安街](http://www.16seats.net/chi/rmb/r_kh71.html)
14. [紅磡差館里　＞　牛頭角及觀塘](http://www.16seats.net/chi/rmb/r_k15.html)
15. [紅磡寶其利街　—　旺角始創中心](http://www.16seats.net/chi/rmb/r_k71.html)
16. [土瓜灣欣榮花園　—　美孚](http://www.16seats.net/chi/rmb/r_k66.html)
17. [土瓜灣欣榮花園　—　旺角先達廣場](http://www.16seats.net/chi/rmb/r_k51.html)
18. [荃灣川龍街　—　土瓜灣欣榮花園](http://www.16seats.net/chi/rmb/r_kn30.html)
19. [土瓜灣及紅磡　—　青山道](http://www.16seats.net/chi/rmb/r_k61.html)
20. [何文田邨　＞　紅磡寶其利街](http://www.16seats.net/chi/rmb/r_k74.html)