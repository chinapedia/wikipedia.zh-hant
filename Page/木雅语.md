**木雅语**（，藏语：*mi nyag
skad*），是[汉藏语系](../Page/汉藏语系.md "wikilink")[羌语支的一种语言](../Page/羌语支.md "wikilink")，分布在[四川省](../Page/四川省.md "wikilink")[甘孜藏族自治州](../Page/甘孜藏族自治州.md "wikilink")[康定县的朋布](../Page/康定县.md "wikilink")（phur
bu）、沙德（gsar sde）、宜代（vjig rten）、六巴（klu
pa）四个乡和[九龙县的唐古乡](../Page/九龙县.md "wikilink")（thang
gu），使用人数超过有一万人。操木雅语的人被识别为[藏族](../Page/藏族.md "wikilink")。

由于木雅语的藏文名与[西夏的藏文名一样](../Page/西夏.md "wikilink")，都是（），漢文曾把藏文音譯為“弭藥”，所以很多学者怀疑讲木雅的藏族人可能是西夏人的后代，但这一看法颇有争议，因为虽然木雅语和西夏文都属于羌语支，但两种语言区别很大，可能羌语支的其他语言（如[羌语](../Page/羌语.md "wikilink")）与[西夏文之间的关系更为密切](../Page/西夏文.md "wikilink")。

不同于[嘉绒语](../Page/嘉绒语.md "wikilink")，木雅语已经脱落了所有的[韵尾](../Page/韵尾.md "wikilink")，简化了所有的复辅音，但是有非常丰富的元音系统，有[鼻化元音和](../Page/鼻化元音.md "wikilink")[松紧元音的对立](../Page/鬆緊元音對立.md "wikilink")。

## 參考文獻

  - Ikeda Takumi (1998-09) "Some Phonological Features of Modern Munya
    (Minyak) Language." 内陸アジア言語の研究 *Nairiku Ajia Gengo no Kenkyuu* 13:
    83-91.
  - Ikeda, T. 2002, "On pitch accent in the Mu-nya language", in
    Linguistics of the Tibeto-Burman Area, vol. 25, no. 2, pp. 27–45.
  - Sun Hongkai et al. 1991. *Zangmianyu yuyin he cihui 藏缅语音和词汇
    \[Tibeto-Burman phonology and lexicon\]*. Chinese Social Sciences
    Press.
  - [Minyak language elementary
    textbook](http://www.khamaid.org/programs/culture/minyaklanguage/minyak_language.pdf),
    a project of the Kham Aid Foundation, 2009.

## 外部連結

  - [木雅語ISO代碼](http://www.ethnologue.com/show_language.asp?code=mvm)
  - [木雅语的语音资料](https://web.archive.org/web/20061201095105/http://xiang.free.fr/minyag.htm)

[Category:羌语支](../Category/羌语支.md "wikilink")
[Category:中国语言](../Category/中国语言.md "wikilink")