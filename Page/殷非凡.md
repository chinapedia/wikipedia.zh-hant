**殷非凡**（**Crystal
Huang**），[臺灣著名](../Page/臺灣.md "wikilink")[補教名師](../Page/補教名師.md "wikilink")。[臺北市立中山女子高級中學](../Page/臺北市立中山女子高級中學.md "wikilink")、[國立臺灣大學外文系](../Page/國立臺灣大學.md "wikilink")[畢業](../Page/畢業.md "wikilink")，曾任[育達商職](../Page/育達商職.md "wikilink")、[基隆女中的](../Page/基隆女中.md "wikilink")[教師](../Page/教師.md "wikilink")，後與夫婿[黃健華](../Page/黃健華.md "wikilink")（現為[誠研科技董事長](../Page/誠研科技.md "wikilink")）共同經營「殷非凡文理補習班」，以「殷非凡英文家教班」為品牌專攻升[大學的](../Page/大學.md "wikilink")[英文補教業務](../Page/英文.md "wikilink")，[補習班學生逾萬人](../Page/補習班.md "wikilink")，在1990年代末期堪稱[台北市最大的升大學英語補習班](../Page/台北市.md "wikilink")，一年營業額超過兩億元[新台幣](../Page/新台幣.md "wikilink")，[淨利超過一億元新台幣](../Page/淨利.md "wikilink")。\[1\]

殷非凡補習班原先位在忠孝東路一段，華山市場一帶的商辦大樓十樓的教室；在1995年左右遷移至在台北市[紹興北街與](../Page/紹興北街.md "wikilink")[北平東路口的](../Page/北平東路.md "wikilink")[華山商業大樓](../Page/華山商業大樓.md "wikilink")，據有一至五層之龐大空間。該建築之八至十一樓為[民主進步黨中央黨部所在地](../Page/民主進步黨.md "wikilink")。

殷非凡也主編了多套的[高中英文](../Page/高中.md "wikilink")[參考書](../Page/參考書.md "wikilink")，而這些參考書是臺灣的[高中最普遍採用的](../Page/高中.md "wikilink")[遠東圖書公司版](../Page/遠東圖書公司.md "wikilink")《高中英文》[教科書相應的參考書](../Page/教科書.md "wikilink")。\[2\]

2004年，殷非凡宣佈[退休](../Page/退休.md "wikilink")，並將殷非凡文理補習班經營權移交給台北市補教界著名[數學教師](../Page/數學.md "wikilink")[沈赫哲](../Page/沈赫哲.md "wikilink")\[3\]。2004年7月1日，殷非凡文理補習班註銷登記。\[4\]殷非凡現定居於[澳洲](../Page/澳洲.md "wikilink")。

## 著作

  - 《新解析遠東高中英文第1冊》（分A、B、C三冊）
  - 《新解析遠東高中英文第2冊》（分A、B、C三冊）
  - 《新解析遠東高中英文第3冊》（分A、B、C三冊），遠東圖書公司2000年7月初版1刷，ISBN 957-612-464-6
  - 《新解析遠東高中英文第4冊》（分A、B、C三冊）
  - 《新解析遠東高中英文第5冊》（分A、B、C三冊），遠東圖書公司2001年8月初版1刷，ISBN 957-612-495-6
  - 《新解析遠東高中英文第6冊》（分A、B、C三冊），遠東圖書公司2002年初版，ISBN 957-612-506-5
  - 《遠東新高中英文參考書第6冊》（分A、B、C三冊），遠東圖書公司
  - 《遠東英文克漏字測驗》（Ｉ），遠東圖書公司，ISBN 957-612-458-1
  - 《遠東英文克漏字測驗》（Ⅱ），遠東圖書公司，ISBN 957-612-480-8
  - 《遠東英文閱讀測驗》，遠東圖書公司，ISBN 957-612-477-8
  - 《遠東英文句型解析》，遠東圖書公司，ISBN 957-612-478-6
  - 《遠東最新美式慣用語》，遠東圖書公司，ISBN 957-612-486-7

## 參考資料

<div class="references-small">

<references />

</div>

  - [補習天王天后法庭火併](http://www.appledaily.com.tw/appledaily/article/headline/20030930/390499/)

## 参见

  - [補教名師](../Page/補教名師.md "wikilink")

## 外部連結

  - [殷非凡的部落格](https://web.archive.org/web/20070812102223/http://tw.myblog.yahoo.com/fei-fan)（主站，設於[Yahoo\!奇摩](../Page/Yahoo!奇摩.md "wikilink")）
  - [殷非凡的部落格](https://web.archive.org/web/20090820054624/http://blog.nownews.com/feifan/)（分站，設於[NOWnews](../Page/NOWnews.md "wikilink")）

[Y](../Category/國立臺灣大學文學院校友.md "wikilink")
[Y](../Category/臺北市立中山女子高級中學校友.md "wikilink")
[Y](../Category/台灣補習教育工作者.md "wikilink")
[Y](../Category/英語教師.md "wikilink")
[Y](../Category/澳大利亚华人.md "wikilink")
[F](../Category/殷姓.md "wikilink")

1.  <http://www.hi-ti.com.tw/info/NewsDetail.asp?lid=&nid=157>
2.  <http://www.hsenglish.com.tw/2007/index.asp?choice=501&type=S&kind=參考書>
3.  [華一文教機構](http://www.her-jer.com.tw/her-jer/history-5.asp)
4.  [直轄市及各縣市短期補習班資訊管理系統
    撤銷名冊及註銷名冊查詢](http://bsb.edu.tw/afterschool/register/cancel_list.jsp?pageno=45&citylink=20&unit=&c_type=&area=&road=&sn_year=&sn_month=&end_year=&end_month=)