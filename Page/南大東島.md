**南大東島**是位於[菲律賓海東南部](../Page/菲律賓海.md "wikilink")[大東群島中的一座](../Page/大東群島.md "wikilink")[珊瑚島](../Page/珊瑚島.md "wikilink")，也是該群島中最大的一個島嶼。位於[北大東島以南約](../Page/北大東島.md "wikilink")9公里處，距離[那霸有](../Page/那霸.md "wikilink")360公里遠。今南大東島在行政區劃上屬於[日本](../Page/日本.md "wikilink")[沖繩縣](../Page/沖繩縣.md "wikilink")[島尻郡](../Page/島尻郡.md "wikilink")[南大東村管轄](../Page/南大東村.md "wikilink")。島上居民完全以農業種植為生計，雖然該島缺乏新鮮淡水。此島既無海灘也無港口，海運時貨物進出必須通過起重機，但建有[南大東機場](../Page/南大東機場.md "wikilink")。

## 地理

南大東島周圍隆起，中央凹陷，為典型的隆起[環礁](../Page/環礁.md "wikilink")，2007年入選[日本地質百選](../Page/日本地質百選.md "wikilink")。島內有許多，其中為[琉球群島最大的天然湖](../Page/琉球群島.md "wikilink")。

在日本尚未使用氣象衛星之前為觀測颱風的重要地點。

## 歷史

大東群島自古為[琉球人所知](../Page/琉球人.md "wikilink")，稱為大東島（ウフアガリジマ）。1885年日本派員調查並宣告為日本領土，1900年以[玉置半右衛門為首](../Page/玉置半右衛門.md "wikilink")，來自[八丈島的開拓團開始對南大東島進行開拓](../Page/八丈島.md "wikilink")。

## 生態

南大東島原有許多大東群島[特有](../Page/特有種.md "wikilink")[亞種生物](../Page/亞種.md "wikilink")，但經人類開發後已有數種[絕種或](../Page/絕種.md "wikilink")[瀕危](../Page/瀕危物種.md "wikilink")，並有[外來種入侵](../Page/外來種.md "wikilink")。大池具有[木欖](../Page/木欖.md "wikilink")[群落](../Page/群落.md "wikilink")，被日本指定為國家[自然紀念物](../Page/自然紀念物.md "wikilink")。

[N南](../Category/大東群島.md "wikilink")