[Tunguska_event_fallen_trees.jpg](https://zh.wikipedia.org/wiki/File:Tunguska_event_fallen_trees.jpg "fig:Tunguska_event_fallen_trees.jpg")
[Ru200007310039.jpg](https://zh.wikipedia.org/wiki/File:Ru200007310039.jpg "fig:Ru200007310039.jpg")

**通古斯大爆炸**（）是1908年6月30日上午7時17分（[UTC](../Page/协调世界时.md "wikilink")
零時17分）發生在現今[俄羅斯](../Page/俄羅斯.md "wikilink")[西伯利亞](../Page/西伯利亞.md "wikilink")[埃文基自治區上空的](../Page/埃文基自治區.md "wikilink")[爆炸事件](../Page/爆炸.md "wikilink")。爆炸發生於[通古斯河附近](../Page/通古斯河.md "wikilink")、[貝加爾湖西北方](../Page/貝加爾湖.md "wikilink")800公里處，[北緯](../Page/北緯.md "wikilink")60.55度，[東經](../Page/東經.md "wikilink")101.57度，當時估計爆炸威力相當於2千萬噸[TNT炸药](../Page/TNT炸药.md "wikilink")，超過2,150[平方公里內的](../Page/平方公里.md "wikilink")8千萬棵樹焚毀倒下。

據報導，當天早上在貝加爾湖西北方的當地人觀察到一個巨大的火球劃過天空，其亮度和[太陽相當](../Page/太陽.md "wikilink")，幾分鐘後，一道強光照亮了整個天空，稍後爆炸產生的[衝擊波將附近](../Page/衝擊波.md "wikilink")650公里內的窗戶玻璃震碎，並且觀察到了[蕈狀雲的現象](../Page/蕈狀雲.md "wikilink")，這個爆炸被橫跨歐亞大陸的[地震监测点所記錄](../Page/地震监测点.md "wikilink")，其所造成的[氣壓不穩定甚至由在當時](../Page/氣壓.md "wikilink")[英國剛被發明的](../Page/英國.md "wikilink")[氣壓自動記錄儀所偵測](../Page/氣壓自動記錄儀.md "wikilink")。在事发後数天内，[亚洲与](../Page/亚洲.md "wikilink")[欧洲的夜空呈现出暗红色](../Page/欧洲.md "wikilink")\[1\]；有假说认为这是由於光线穿过在高纬度地区的极度低温中形成的[冰晶颗粒造成的](../Page/冰.md "wikilink")，这种现象常在航天飞机返回地球大气时出现\[2\]\[3\]。在[美國的](../Page/美國.md "wikilink")[史密松天体物理台和](../Page/史密松天体物理台.md "wikilink")[威爾遜山天文台也觀察到](../Page/威爾遜山天文台.md "wikilink")[大氣的透明度至少數個月有降低](../Page/大氣.md "wikilink")。

而如果這個物體再遲4小時37分撞擊地球，那麼這場爆炸將摧毁当时[俄罗斯帝国的首都](../Page/俄罗斯帝国.md "wikilink")[圣彼得堡](../Page/圣彼得堡.md "wikilink")，而不是人口稀少的[通古斯地區](../Page/通古斯.md "wikilink")，造成更大的人員傷亡。

## 歷史

由於通古斯地區過於偏遠，當時只有少數科學家對這個爆炸事件感興趣。就算當時有任何對這地區的調查，那些記錄也很可能在接下來的混亂時代——[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")、[俄國革命和](../Page/1917年俄国革命.md "wikilink")[俄國內戰中遺失](../Page/俄國內戰.md "wikilink")。

現存對此地區最早的調查是於事件發生幾乎20年後進行的。1927年，[苏联科學院的](../Page/苏联科學院.md "wikilink")[礦物學家](../Page/礦物學家.md "wikilink")[列昂尼德·庫利克到達](../Page/列昂尼德·庫利克.md "wikilink")[通古斯河地區](../Page/通古斯河.md "wikilink")，並在這個地區調查當時隕石撞擊的確切地點。他用隕石上的鐵可能有助于蘇聯发展工業的理由，說服[蘇聯政府對科學調查隊給予資金](../Page/蘇聯.md "wikilink")。

[Tunguska.png](https://zh.wikipedia.org/wiki/File:Tunguska.png "fig:Tunguska.png")

庫利克的調查隊在1927年終於找到爆炸地點。讓他們驚訝的是，沒有發現任何[隕石坑](../Page/隕石坑.md "wikilink")。燒焦枯死的樹橫跨了大約50公里。少數靠近爆炸中心的樹沒有傾倒，它們的樹枝和樹皮則被脫去。傾倒的樹則是向爆炸中心相反的方向傾倒。

接下來10年，有另外3支隊伍被派到這一地區。庫利克發現有一個小沼澤可能是隕石坑，但在排光其中的水後，他在底部發現一些樹木殘枝，所以確定那不是隕石坑。1938年，庫利克又找人來航拍整個區域，顯示樹是以一個像蝴蝶的巨大形狀傾倒，然而他仍然沒有發現任何隕石坑。

1950和1960年代的調查隊在這個地區發現了極小的[玻璃球灑在土地上](../Page/玻璃球.md "wikilink")。化學分析顯示球內含有大量在[隕石中常見的金屬](../Page/隕石.md "wikilink")-[鎳和](../Page/鎳.md "wikilink")[銥](../Page/銥.md "wikilink")，而且也確定它們是來自地球以外的。另外由根纳季·普列汉诺夫（Геннадий
Плеханов）所領導的研究隊發現其中沒有[輻射異常的跡象](../Page/輻射.md "wikilink")，這表示這並不是自然的核自爆現象。

1975年，[以色列的](../Page/以色列.md "wikilink")[魏茨曼科學研究所的地震學家](../Page/魏茨曼科學研究學院.md "wikilink")分析了通古斯地震波的資料，認為爆炸的能量相當於1,000枚投在廣島的[原子彈](../Page/原子彈.md "wikilink")，引起了天體物理學家的注意，進一步的模擬顯示小行星可能在10公里的高度就蒸發了，但不排除有1公尺直徑的碎片落下。1991年，意大利的探查隊前往通古斯勘查，找到了兩篇1960年代有關於距離爆炸地點8公里的[切科湖的論文](../Page/切科湖.md "wikilink")，為這百年懸案帶來了一線新的曙光\[4\]，答案可能就在契科湖底。

## 天体撞击說

### 陨星空中爆炸

目前比較被科學界所接受的說法是一个[隕星在大約離地](../Page/隕星.md "wikilink")6至10公里上空爆炸。

隕星通常是從[外太空進入地球](../Page/外太空.md "wikilink")，速度通常可達每秒10公里。其在通過大氣層時[摩擦所產生的](../Page/摩擦.md "wikilink")[熱十分巨大](../Page/熱能.md "wikilink")，大部分的隕星在到達地面時便已燃燒殆盡或爆炸。一個直徑10公尺的隕石可以產生約2萬噸左右的爆炸，相當於投在[廣島的](../Page/廣島市.md "wikilink")[原子彈威力](../Page/原子彈.md "wikilink")。

從20世紀後半開始，對地球大氣層的監測讓人類開始注意千噸級隕石的空中爆炸。根據[美國空軍](../Page/美國空軍.md "wikilink")的資料顯示，這種爆炸大約一年會發生一次。而類似通古斯那種大小的一千萬噸級的是非常罕見的；[尤金·舒梅克博士估計這種大小的隕石大約](../Page/尤金·舒梅克.md "wikilink")300年才會發生一次。

[2013年俄罗斯乌拉尔陨石雨事件为陨石爆炸说提供了强力佐证](../Page/2013年俄罗斯乌拉尔陨石雨.md "wikilink")。比太阳还要明亮的火球，巨大的音爆，和在天空中留下长长的烟雾轨迹无不与通古斯卡巴大爆炸当时的记录相符，只是在通古斯卡上空燃烧爆炸的陨石显然要比2013年的大得多。

### 爆炸類型

在通古斯爆炸時，在爆炸中心正下方的樹被脫去樹枝樹皮，而稍遠的樹則因為爆炸波而傾倒，這個現象也被在[核試驗中發現](../Page/核試驗.md "wikilink")。

1960年中期，蘇聯使用模型樹跟小型炸藥作實驗，尋找哪種爆炸方式可以產生像通古斯爆炸相似的蝴蝶型爆炸。實驗顯示這個物體是以大約與地面夾角30度、與北方夾角約115度接近地面，然後在空中爆炸。

### 彗星撞击说

在天体撞击说的基础上，由于至今未在地上发现陨石，所以通古斯天体的物质成份一直是个謎。在1930年，英国天文学者指通古斯天体很可能是一个小[彗星](../Page/彗星.md "wikilink")。由于彗星主要由冰与尘埃所组成，在撞击爆炸后就已经蒸发殆尽，所以没有留下一般石质天體会留下的陨石。而且，彗星说也解释了为何通古斯大爆炸后周边地区夜如白昼的现象，这可能是[彗尾残留在高空的冰](../Page/彗尾.md "wikilink")、尘颗粒反射阳光照亮夜空的结果\[5\]。这一理论在1960年代也普遍获得[苏联的通古斯调查员所接受](../Page/苏联.md "wikilink")\[6\]。

1978年，[斯洛伐克天文学者](../Page/斯洛伐克.md "wikilink")提出，通古斯天体可能是来自[短周期彗星](../Page/短周期彗星.md "wikilink")[恩克彗星的碎片](../Page/恩克彗星.md "wikilink")。[恩克彗星是](../Page/恩克彗星.md "wikilink")6月至7月[金牛座β流星雨的来源](../Page/金牛座β流星群.md "wikilink")，而通古斯大爆炸的时间点正好处于该流星雨的高峰期\[7\]，且该撞击体的模拟撞击方向与该[流星群的轨道相吻合](../Page/流星群.md "wikilink")\[8\]。已知这类流星通常会在离地表十至数百公里的高空爆炸，这现象一直被军事卫星所观测\[9\]。

## 其他假說

作為20世紀最大的不解之謎之一的通古斯大爆炸，吸引了無數科學家的研究。因此便有了上百種的假說和解釋，但所有的這些假說都不能給所有的怪異現象作出完滿的解答。
由於現代科技的發達，像是2013年發生的烏拉爾隕石雨墜落事件被包括俄國、美國、法國等多國的天文機構、甚至是一般民眾的[行車紀錄器完整地紀錄下來](../Page/行車紀錄器.md "wikilink")，使得通古斯大爆炸的成因逐漸明朗（事實上大陸型國家例如加拿大，美國，澳洲等也經常有隕石衝入大氣層的目擊報告）。

其中較為著名的有：

  - [尼古拉·特斯拉远距离输电实验](../Page/沃登克里弗塔.md "wikilink")
  - 上世紀20年代的隕石撞地說和隕石爆炸氣化
  - 1930年的[彗星](../Page/彗星.md "wikilink")[彗核爆炸](../Page/彗核.md "wikilink")
  - 1932年的[宇宙塵埃與地球相撞](../Page/宇宙塵埃.md "wikilink")
  - 1934年的彗尾撞擊地球
  - 1945年的外星飛船發生核爆炸
  - 1946年的火星人飛船爆炸
  - 1947年的[反物質隕石湮滅](../Page/反物質.md "wikilink")
  - 1959年的[行星內核碎片撞地](../Page/行星.md "wikilink")
  - 1961年的外星[飛碟解體](../Page/不明飛行物.md "wikilink")
  - 1962年的[流星引起地球](../Page/流星.md "wikilink")[電離層破壞](../Page/電離層.md "wikilink")
  - 1964年的外星智能文明利用宇宙激光探測地球生命
  - 1965年的外星雪人飛船入侵地球
  - 1966年[白矮星的恆星超密度碎片隕落撞地](../Page/白矮星.md "wikilink")
  - 1967年的自然閃電引起[甲烷氣體爆炸](../Page/甲烷.md "wikilink")
  - 1968年易燃氣體爆炸及水分解
  - 1969年宇宙間反物質[彗星墜落](../Page/彗星.md "wikilink")
  - 1993年再次出現了[冰隕石墜落](../Page/冰隕石.md "wikilink")
  - 1995年出現了反物質進入地球大氣引起爆炸
  - 1995年的碳球隕石撞地爆炸
  - 1996年的小行星墜落
  - 1996年的[銥含量極高行星撞地](../Page/銥.md "wikilink")

### 黑洞

1973年，美國[德克薩斯大學的兩位科學家傑克遜](../Page/德克薩斯大學.md "wikilink")（Jackson）和賴安（Ryan）提出，通古斯大爆炸是由於一個小型的[黑洞](../Page/黑洞.md "wikilink")（約10^23
kg）通過地球所引起的。並且斷定小型黑洞是在[冰島和](../Page/冰島.md "wikilink")[紐芬蘭島之間大西洋某地穿過地球的](../Page/紐芬蘭島.md "wikilink")。但由於沒有證據顯示在黑洞穿出地球時發生的第二次爆炸，所以此假說沒有被廣泛接受。而後來人們發現黑洞其實一直在向外發射出大量的[X射線和](../Page/X射線.md "wikilink")[γ射線](../Page/γ射線.md "wikilink")。根據[史提芬·威廉·霍金所提出的理論](../Page/史提芬·威廉·霍金.md "wikilink")，黑洞質量越小，「蒸發」越快。這樣小質量的黑洞早在到達地球以前就「蒸發」殆盡了。

### 反物質

1965年，三位科學家（Cowan、Atluri、和Libby）提出通古斯大爆炸是由一個從太空墜落的[反物質所造成的](../Page/反物質.md "wikilink")。他們還認為，半克鐵和半克「反鐵」相撞，便足以產生相當於廣島原子彈的威力。但一如其他很多解釋，這種解釋同樣未能解釋在當地找到的隕石和當地多種元素含量異常的現象。如果反物質存在的話，它們應當會在與物質相遇爆炸時，放出大量的γ射線。但一直沒有找到因此發出的γ射線。

### 飛碟

最早在1945年，由當時蘇聯的軍事工程專家提出。他的靈感來源於二戰中在日本爆炸的[原子彈](../Page/原子彈.md "wikilink")。他覺得通古斯大爆炸中那雷鳴般的爆炸聲，衝天的火柱，劇烈的地震，強大的衝擊波和光輻射都和核爆非常相像。於是他在1945年8月提出了核爆說。主張通古斯大爆炸是一場核爆。進一步地，他在1946年大膽地提出這場核爆是由一艘訪問地球的外星飛船所引起的。

此解說引起了很多[不明飛行物](../Page/不明飛行物.md "wikilink")（UFO）迷的極大興趣。從此通古斯大爆炸就和地外智慧生命有了緊密的聯繫。有人認為爆炸是因為外星飛船核爆引起的，也有認為是飛船解體，也有人認為是外星人通過通古斯大爆炸為地球解決了一次不明的災難……凡此種種，不勝枚舉。由於這種解釋非常能吸引人的好奇心，所以一直比較流行。甚至有人稱通古斯是俄羅斯的[羅茲威爾](../Page/羅斯威爾飛碟墜毀事件.md "wikilink")。

## 參見

  - [東地中海事件](../Page/東地中海事件.md "wikilink")
  - [2008 TC3](../Page/2008_TC3.md "wikilink")
  - [2009 DD<sub>45</sub>](../Page/2009_DD45.md "wikilink")
  - [味地謨事件](../Page/味地謨事件.md "wikilink")
  - [坎多事件](../Page/坎多事件.md "wikilink")
  - [潛在威脅小行星](../Page/潛在威脅小行星.md "wikilink")
  - [近地天體](../Page/近地天體.md "wikilink")
  - [撞擊事件](../Page/撞擊事件.md "wikilink")
  - [白日大火球](../Page/1972年白日大火球.md "wikilink")
  - [俄罗斯陨石事件](../Page/2013年車里雅賓斯克小行星撞擊事件.md "wikilink")
  - [王恭廠大爆炸](../Page/王恭廠大爆炸.md "wikilink")
  - [尼古拉·特斯拉](../Page/尼古拉·特斯拉.md "wikilink")
  - [沃登克里弗塔](../Page/沃登克里弗塔.md "wikilink")

## 參考資料

氫隕石通過大氣層磨擦氣化產生大爆炸，氫是一個很不穩定元素。

## 外部連結

  - [Tunguska similar impacts and origin of life, Dr. Andrei E. Zlobin,
    article in electronic journal "Modern scientific researches and
    innovations." – December 2013. -
    № 12.](http://web.snauka.ru/en/issues/2013/12/30018)
  - [Discovery of probably Tunguska meteorites at the bottom of Khushmo
    river's shoal, Andrei E. Zlobin, Paper 1304.8070 in arXiv.org
    (PDF)](http://arxiv.org/abs/1304.8070)
  - [Quasi Three-dimensional Modeling of Tungguska Comet Impact(1908),
    Andrei E.
    Zlobin](https://web.archive.org/web/20080407110721/http://www.aero.org/conferences/planetarydefense/2007papers/P4-1--Zlobin_Paper.pdf)

  - [Tunguska Comet Impact
    - 1908](https://web.archive.org/web/20080725011708/http://www.orc.ru/~azorcord/)

  - [The Tunguska Event in 1908: Evidence from Tree-Ring
    Anatomy](http://forest.akadem.ru/Articles/04/vaganov_en_1.pdf)
  - [Tunguska.ru](http://www.tunguska.ru/)包含一些當時的照片

[Category:神秘事件](../Category/神秘事件.md "wikilink")
[T](../Category/撞擊事件.md "wikilink")
[Category:俄罗斯历史](../Category/俄罗斯历史.md "wikilink")
[Category:俄羅斯災難](../Category/俄羅斯災難.md "wikilink")
[Category:1908年6月](../Category/1908年6月.md "wikilink")
[Category:1908年爆炸案](../Category/1908年爆炸案.md "wikilink")
[Category:俄罗斯爆炸案](../Category/俄罗斯爆炸案.md "wikilink")

1.  Watson, Nigel. ''The Tunguska Event”. History Today 58.1 (July
    2008): 7. MAS Ultra-School Edition. EBSCO. February 10, 2009
    \<<http://search.ebscohost.com>\>

2.  Cornell University (2009, June 25). [Space Shuttle Science Shows
    How 1908 Tunguska Explosion Was Caused By A
    Comet.](http://www.sciencedaily.com/releases/2009/06/090624152941.htm)

3.  Kelley, M. C., C. E. Seyler, and M. F. Larsen. (2009),
    Two-dimensional Turbulence, Space Shuttle Plume Transport in the
    Thermosphere, and a Possible Relation to the Great Siberian Impact
    Event. Geophys. Res. Lett, (in press) DOI: 10.1029/2009GL038362

4.  西伯利亞的隕石懸案：科學人77期 PP102-107，遠流出版社。

5.

6.
7.

8.
9.