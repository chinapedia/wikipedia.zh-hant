**伯灵顿**（）是[美國](../Page/美國.md "wikilink")[佛蒙特州最大的城市](../Page/佛蒙特州.md "wikilink")，也是[奇滕登县的县治所在](../Page/奇滕登县_\(佛蒙特州\).md "wikilink")。該市位於[尚普蘭湖湖畔](../Page/尚普蘭湖.md "wikilink")，遙望[紐約州](../Page/紐約州.md "wikilink")，往西可達[綠山山脈](../Page/綠山山脈.md "wikilink")。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，[人口為](../Page/人口.md "wikilink")38,889人。

## 历史

## 地理

## 气候

伯灵顿属于新英格兰典型的[大陆性气候](../Page/大陆性气候.md "wikilink")。\[1\]冬季寒冷而漫长，潮湿，日照少，日最高气温低于的平均日数为61天，日最低气温低于的平均日数为29天，低于的有12天；夏季相对潮湿，日最高气温超过的日数年均只有20天，以上的气温罕见。\[2\]最冷月（1月）均温，极端最低气温（1957年1月15日，1979年2月12日）。\[3\]最热月（7月）均温，极端最高气温（1944年8月11日）。\[4\]无霜期平均为152天（5月8日至10月6日）。\[5\]年均降水量约，年均降雪量为；1912–13年的降雪量最少，积累降雪量只有，1899–00年的降雪量最多，积累降雪量为。\[6\]

## 人口

根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，[伯靈頓市的總](../Page/伯靈頓.md "wikilink")[人口為](../Page/人口.md "wikilink")
38,889 人。該市的[人口密度為每平方公里](../Page/人口密度.md "wikilink") 1421.9 人
(1,421.9/km²)。[種族分類為](../Page/種族.md "wikilink")：[白人](../Page/白人.md "wikilink")
92.27%、[非裔](../Page/非裔美國人.md "wikilink")
1.78%、[美國原住民](../Page/美國原住民.md "wikilink")
0.47%、[亚裔](../Page/亚裔美国人.md "wikilink")
2.56%、[猶太裔](../Page/猶太裔美國人.md "wikilink")
0.02%、多種族人士 2.27% 和其他種族人士
2.27%。此外，[拉美裔及](../Page/拉美裔.md "wikilink")[拉丁美洲人佔總人口的](../Page/拉丁美洲人.md "wikilink")
1.40%。

值得一提的是，18歲以下的人士佔總人口的 16.3%，18-24歲佔25.4%，25-44歲佔31.0%，45-64歲佔16.8%,
而65歲或以上則佔10.5%。該市的[中位數年齡為](../Page/中位數.md "wikilink")29歲。

[男女比例方面](../Page/男女比例.md "wikilink")，每100名女性便有93.2名男性，而每100名18歲或以上的女性就有90.8名男性。

## 法律与政府

## 经济

## 教育

  - University of Vermont (UVM)
  - Burlington College
  - Champlain College
  - Community College of Vermont
  - Saint Michael's College

## 文化

## 媒体

  - Burlington Free Press
  - Seven Days
  - Vermont Business Magazine

## 体育

## 医疗

## 交通

  - [89號州際公路](../Page/89號州際公路.md "wikilink")（I-89）

## 姐妹城市

  - [雅罗斯拉夫尔](../Page/雅罗斯拉夫尔.md "wikilink")

  - [阿拉德](../Page/阿拉德_\(以色列\).md "wikilink")

  - [卡貝薩斯港](../Page/卡貝薩斯港.md "wikilink")

  - [伯利恆](../Page/伯利恆.md "wikilink")

  - [Moss Point,
    Mississippi](../Page/Moss_Point,_Mississippi.md "wikilink")

## 參考

## 外部連結

[B](../Category/佛蒙特州城市.md "wikilink")

1.  Peel, M. C., Finlayson, B. L., and McMahon, T. A.: [Updated world
    map of the Köppen-Geiger climate
    classification](../Page/Media:Americas_Köppen_Map_original_colors.png.md "wikilink"),
    Hydrol. Earth Syst. Sci., 11, 1633–1644, 2007.

2.
3.
4.
5.
6.