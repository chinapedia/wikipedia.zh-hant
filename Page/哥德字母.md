**哥特字母**，[哥特語所使用的文字](../Page/哥特語.md "wikilink")。哥特字母現在在[訓詁上的研究較多](../Page/訓詁.md "wikilink")。

## 字母表

<table>
<thead>
<tr class="header">
<th><p>字母</p></th>
<th><p>轉寫</p></th>
<th><p>比照</p></th>
<th><p>名字</p></th>
<th><p><a href="../Page/IPA.md" title="wikilink">IPA</a></p></th>
<th><p>№</p></th>
<th><p>XML實體</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_ahsa.svg" title="fig:Gothic_letter_ahsa.svg">Gothic_letter_ahsa.svg</a></p></td>
<td></td>
<td><p>a</p></td>
<td><p><a href="../Page/Α.md" title="wikilink">Α</a></p></td>
<td><p><a href="../Page/ansuz_rune.md" title="wikilink">ahsa</a> / aza</p></td>
<td></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_bairkan.svg" title="fig:Gothic_letter_bairkan.svg">Gothic_letter_bairkan.svg</a></p></td>
<td></td>
<td><p>b</p></td>
<td><p><a href="../Page/Β.md" title="wikilink">Β</a></p></td>
<td><p><a href="../Page/bairkan.md" title="wikilink">bairkan</a> / bercna</p></td>
<td></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_giba.svg" title="fig:Gothic_letter_giba.svg">Gothic_letter_giba.svg</a></p></td>
<td></td>
<td><p>g</p></td>
<td><p><a href="../Page/Γ.md" title="wikilink">Γ</a></p></td>
<td><p><a href="../Page/giba_(rune).md" title="wikilink">giba</a> / geuua</p></td>
<td></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_dags.svg" title="fig:Gothic_letter_dags.svg">Gothic_letter_dags.svg</a></p></td>
<td></td>
<td><p>d</p></td>
<td><p><a href="../Page/Δ.md" title="wikilink">Δ</a></p></td>
<td><p><a href="../Page/dagaz.md" title="wikilink">dags</a> / daaz</p></td>
<td></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_aihvus.svg" title="fig:Gothic_letter_aihvus.svg">Gothic_letter_aihvus.svg</a></p></td>
<td></td>
<td><p>e</p></td>
<td><p><a href="../Page/Ε.md" title="wikilink">Ε</a></p></td>
<td><p><a href="../Page/aiƕus.md" title="wikilink">aiƕus</a> / eyz</p></td>
<td></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_qairthra.svg" title="fig:Gothic_letter_qairthra.svg">Gothic_letter_qairthra.svg</a></p></td>
<td></td>
<td><p>q</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Greek_Digamma_cursive_06.svg" title="fig:链接=https://en.wikipedia.org/wiki/File:Greek_Digamma_cursive_06.svg">链接=<a href="https://en.wikipedia.org/wiki/File:Greek_Digamma_cursive_06.svg">https://en.wikipedia.org/wiki/File:Greek_Digamma_cursive_06.svg</a></a> <a href="../Page/Ϛ.md" title="wikilink">(Ϛ), ϰ</a></p></td>
<td><p><a href="../Page/qairþra.md" title="wikilink">qairþra</a> (qairthra) / qertra</p></td>
<td></td>
<td><p>6</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_iuja.svg" title="fig:Gothic_letter_iuja.svg">Gothic_letter_iuja.svg</a></p></td>
<td></td>
<td><p>z</p></td>
<td><p><a href="../Page/Ζ.md" title="wikilink">Ζ</a></p></td>
<td><p><a href="../Page/ezec.md" title="wikilink">ezec</a></p></td>
<td></td>
<td><p>7</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_hagl.svg" title="fig:Gothic_letter_hagl.svg">Gothic_letter_hagl.svg</a></p></td>
<td></td>
<td><p>h</p></td>
<td><p><a href="../Page/H.md" title="wikilink">H</a></p></td>
<td><p><a href="../Page/hagl.md" title="wikilink">hagl</a> / haal</p></td>
<td></td>
<td><p>8</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_thiuth.svg" title="fig:Gothic_letter_thiuth.svg">Gothic_letter_thiuth.svg</a></p></td>
<td></td>
<td><p>þ, th</p></td>
<td><p><a href="../Page/Θ.md" title="wikilink">Θ</a></p></td>
<td><p><a href="../Page/þiuþ.md" title="wikilink">þiuþ</a> (thiuth) / thyth</p></td>
<td></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_eis.svg" title="fig:Gothic_letter_eis.svg">Gothic_letter_eis.svg</a></p></td>
<td></td>
<td><p>i</p></td>
<td><p><a href="../Page/Ι.md" title="wikilink">Ι</a></p></td>
<td><p><a href="../Page/eis.md" title="wikilink">eis</a> / iiz</p></td>
<td></td>
<td><p>10</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_kusma.svg" title="fig:Gothic_letter_kusma.svg">Gothic_letter_kusma.svg</a></p></td>
<td></td>
<td><p>k</p></td>
<td><p><a href="../Page/Κ.md" title="wikilink">Κ</a></p></td>
<td><p><a href="../Page/kaunan.md" title="wikilink">kusma</a> / chozma</p></td>
<td></td>
<td><p>20</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_lagus.svg" title="fig:Gothic_letter_lagus.svg">Gothic_letter_lagus.svg</a></p></td>
<td></td>
<td><p>l</p></td>
<td><p><a href="../Page/Λ.md" title="wikilink">Λ</a></p></td>
<td><p><a href="../Page/laukaz.md" title="wikilink">lagus</a> / laaz</p></td>
<td></td>
<td><p>30</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_manna.svg" title="fig:Gothic_letter_manna.svg">Gothic_letter_manna.svg</a></p></td>
<td></td>
<td><p>m</p></td>
<td><p><a href="../Page/Μ.md" title="wikilink">Μ</a></p></td>
<td><p><a href="../Page/mannaz.md" title="wikilink">manna</a></p></td>
<td></td>
<td><p>40</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_nauthus.svg" title="fig:Gothic_letter_nauthus.svg">Gothic_letter_nauthus.svg</a></p></td>
<td></td>
<td><p>n</p></td>
<td><p><a href="../Page/Ν.md" title="wikilink">Ν</a></p></td>
<td><p><a href="../Page/nauþs.md" title="wikilink">nauþs</a> (nauths) / noicz</p></td>
<td></td>
<td><p>50</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_jer.svg" title="fig:Gothic_letter_jer.svg">Gothic_letter_jer.svg</a></p></td>
<td></td>
<td><p>j</p></td>
<td><p><a href="../Page/ᛃ.md" title="wikilink">ᛃ</a></p></td>
<td><p><a href="../Page/jer.md" title="wikilink">jer</a> / gaar</p></td>
<td></td>
<td><p>60</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_urus.svg" title="fig:Gothic_letter_urus.svg">Gothic_letter_urus.svg</a></p></td>
<td></td>
<td><p>u</p></td>
<td><p><a href="../Page/ᚢ.md" title="wikilink">ᚢ</a></p></td>
<td><p><a href="../Page/ur_(rune).md" title="wikilink">urus</a> / uraz</p></td>
<td></td>
<td><p>70</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_pairthra.svg" title="fig:Gothic_letter_pairthra.svg">Gothic_letter_pairthra.svg</a></p></td>
<td></td>
<td><p>p</p></td>
<td><p><a href="../Page/Π.md" title="wikilink">Π</a></p></td>
<td><p><a href="../Page/pairþra.md" title="wikilink">pairþra</a> (pairthra) / pertra</p></td>
<td></td>
<td><p>80</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_numeral_ninety.svg" title="fig:Gothic_numeral_ninety.svg">Gothic_numeral_ninety.svg</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/Ϟ.md" title="wikilink">Ϟ</a></p></td>
<td></td>
<td></td>
<td><p>90</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_raida.svg" title="fig:Gothic_letter_raida.svg">Gothic_letter_raida.svg</a></p></td>
<td></td>
<td><p>r</p></td>
<td><p><a href="../Page/R.md" title="wikilink">R</a></p></td>
<td><p><a href="../Page/raida.md" title="wikilink">raida</a> / reda</p></td>
<td></td>
<td><p>100</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_sauil.svg" title="fig:Gothic_letter_sauil.svg">Gothic_letter_sauil.svg</a></p></td>
<td></td>
<td><p>s</p></td>
<td><p><a href="../Page/S.md" title="wikilink">S</a></p></td>
<td><p><a href="../Page/sowilo.md" title="wikilink">sauil</a> / sugil</p></td>
<td></td>
<td><p>200</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_teiws.svg" title="fig:Gothic_letter_teiws.svg">Gothic_letter_teiws.svg</a></p></td>
<td></td>
<td><p>t</p></td>
<td><p><a href="../Page/Τ.md" title="wikilink">Τ</a></p></td>
<td><p><a href="../Page/teiws.md" title="wikilink">teiws</a> / tyz</p></td>
<td></td>
<td><p>300</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_winja.svg" title="fig:Gothic_letter_winja.svg">Gothic_letter_winja.svg</a></p></td>
<td></td>
<td><p>w</p></td>
<td><p><a href="../Page/Υ.md" title="wikilink">Υ</a></p></td>
<td><p><a href="../Page/winja.md" title="wikilink">winja</a> / uuinne</p></td>
<td></td>
<td><p>400</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_faihu.svg" title="fig:Gothic_letter_faihu.svg">Gothic_letter_faihu.svg</a></p></td>
<td></td>
<td><p>f</p></td>
<td><p><a href="../Page/F.md" title="wikilink">F</a></p></td>
<td><p><a href="../Page/faihu.md" title="wikilink">faihu</a> / fe</p></td>
<td></td>
<td><p>500</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_iggws.svg" title="fig:Gothic_letter_iggws.svg">Gothic_letter_iggws.svg</a></p></td>
<td></td>
<td><p>x</p></td>
<td><p><a href="../Page/Χ.md" title="wikilink">Χ</a></p></td>
<td><p>iggws / enguz</p></td>
<td></td>
<td><p>600</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_hwair.svg" title="fig:Gothic_letter_hwair.svg">Gothic_letter_hwair.svg</a></p></td>
<td></td>
<td><p>ƕ, hw</p></td>
<td></td>
<td><p><a href="../Page/ƕair.md" title="wikilink">ƕair</a> / uuaer</p></td>
<td></td>
<td><p>700</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_letter_othal.svg" title="fig:Gothic_letter_othal.svg">Gothic_letter_othal.svg</a></p></td>
<td></td>
<td><p>o</p></td>
<td><p><a href="../Page/Ω.md" title="wikilink">Ω</a></p></td>
<td><p><a href="../Page/oþal.md" title="wikilink">oþal</a> (othal) / utal</p></td>
<td></td>
<td><p>800</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gothic_numeral_nine_hundred.svg" title="fig:Gothic_numeral_nine_hundred.svg">Gothic_numeral_nine_hundred.svg</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/Ϡ.md" title="wikilink">Ϡ</a></p></td>
<td></td>
<td></td>
<td><p>900</p></td>
</tr>
</tbody>
</table>

## Unicode 編碼

哥特字母的 [Unicode](../Page/Unicode.md "wikilink") 編碼范圍是 U+10330–U+1034F。

## 外部連結

  - [Omniglot's Gothic writing
    page](http://www.omniglot.com/writing/gothic.htm)
  - [Pater Noster in
    Gothic](https://web.archive.org/web/20130505053530/http://www.christusrex.org/www1/pater/JPN-gothic.html)
  - [JavaScript Gothic
    transliterator](https://web.archive.org/web/20051029233446/http://marnanel.org/gothic)
  - [Unicode code chart for
    Gothic](http://www.unicode.org/charts/PDF/U10330.pdf)
  - [WAZU JAPAN's Gallery of Gothic Unicode
    Fonts](http://www.wazu.jp/gallery/Fonts_Gothic.html)

[Category:东日耳曼语支](../Category/东日耳曼语支.md "wikilink")
[Category:字母系统](../Category/字母系统.md "wikilink")
[Category:欧洲文字](../Category/欧洲文字.md "wikilink")