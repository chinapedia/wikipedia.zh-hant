**坦桑尼亞國旗**是一面由藍、綠兩個直角三角形和中間金黄、黑、金黄斜條組成的旗幟。其含意為

  - 黑色代表人民
  - 綠色代表土地
  - 藍色代表海洋
  - 金色代表礦產\[1\]

本旗採用於1964年4月21日，由[坦噶尼喀和](../Page/坦噶尼喀.md "wikilink")[桑給巴爾的旗幟組合而成](../Page/桑給巴爾.md "wikilink")。

## 历史国旗

### 坦噶尼喀

### 桑給巴爾

#### 桑給巴爾革命政府旗

### 政党旗帜

## 参考文献

<div class="references-small">

<references />

</div>

[Category:坦桑尼亞](../Category/坦桑尼亞.md "wikilink")
[T](../Category/國旗.md "wikilink")
[Category:1964年面世的旗幟](../Category/1964年面世的旗幟.md "wikilink")

1.  [The Republic of Tanzania: Country
    Profile](http://www.tanzania.go.tz/profilef.html)