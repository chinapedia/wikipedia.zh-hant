**王鼎昌**（****，），是[新加坡首任民选](../Page/新加坡.md "wikilink")[总统](../Page/新加坡总统列表.md "wikilink")，也是[華人世界史上首位民選總統](../Page/華人世界.md "wikilink")。

1936年1月出生于[新加坡](../Page/新加坡.md "wikilink")，1955年毕业于[华侨中学之后曾留學于](../Page/华侨中学_\(新加坡\).md "wikilink")[南澳州的](../Page/南澳州.md "wikilink")[阿德萊德大學](../Page/阿德萊德.md "wikilink")。1961年考获[阿德萊德大学建筑绘测学士学位](../Page/阿德萊德大学.md "wikilink")，接著獲得[利物浦大学城市设计碩士學位](../Page/利物浦大学.md "wikilink")。1972年，当选国会议员。1975年，王鼎昌出任交通部高级政务部长，兼代文化部长。1981年，王鼎昌由[人民行动党第二副主席升任党主席](../Page/人民行动党.md "wikilink")。他又出任交通部长兼劳工部长，后升任新加坡第二副总理。1993年8月28日，新加坡举行第一次总统選舉，王鼎昌获胜，于9月1日正式宣誓就任新加坡首位民选总统。

1963年4月13日與林秀梅（1936-1999）結束十年愛情長跑，在澳洲結婚，1966年在英國生下長子王子元，兩年後在新加坡生下次子王子文。妻子林秀梅於1999年7月30日因患上結腸癌而逝世，享年63歲。\[1\]王鼎昌在当选总统前发现患上轻度恶性[淋巴肿瘤](../Page/淋巴肿瘤.md "wikilink")，之后经常要到[美国接受治疗](../Page/美国.md "wikilink")。2002年2月8日在寓所内病逝，享年67岁。

2017年6月28日，哈萨克斯坦登山总会批准以王鼎昌的名字命名[天山山脉其中一座海拔](../Page/天山山脉.md "wikilink")4743公尺的山峰，为全球首个以新加坡人命名的山峰。\[2\]

## 參考

|width=40% align=center|**前任：**
[黃金輝](../Page/黃金輝.md "wikilink")
1985年—1993年 |width=20% align=center
colspan="2"|**[新加坡總統](../Page/新加坡總統.md "wikilink")**
1993年—1999年 |width=40% align=center|**繼任：**
[塞拉潘·纳丹](../Page/塞拉潘·纳丹.md "wikilink")
1999年—2011年

[Category:新加坡總統](../Category/新加坡總統.md "wikilink")
[Category:新加坡副總理](../Category/新加坡副總理.md "wikilink")
[Category:新加坡交通部長](../Category/新加坡交通部長.md "wikilink")
[Category:新加坡勞工部長](../Category/新加坡勞工部長.md "wikilink")
[Category:新加坡文化部長](../Category/新加坡文化部長.md "wikilink")
[Category:前新加坡國會議員](../Category/前新加坡國會議員.md "wikilink")
[Category:人民行動黨黨員](../Category/人民行動黨黨員.md "wikilink")
[Category:新加坡建築師](../Category/新加坡建築師.md "wikilink")
[Category:新加坡佛教徒](../Category/新加坡佛教徒.md "wikilink")
[O](../Category/利物浦大學校友.md "wikilink")
[O](../Category/阿德雷得大學校友.md "wikilink")
[Category:新加坡華僑中學校友](../Category/新加坡華僑中學校友.md "wikilink")
[Category:閩南裔新加坡人](../Category/閩南裔新加坡人.md "wikilink")
[Category:新加坡華人](../Category/新加坡華人.md "wikilink")
[Category:担任国家元首或政府首脑的海外华人](../Category/担任国家元首或政府首脑的海外华人.md "wikilink")
[Category:王姓](../Category/王姓.md "wikilink")

1.  [胡姬花：记新加坡原总统王鼎昌夫人－－林秀梅 - 桑叶文集 - 文协专辑
    SGCLS.org](http://sgcls.hi2net.com/blog_read.asp?id=12&blogid=2976)
2.  [12年前就命名
    ‘王鼎昌峰’获批准](http://www.zaobao.com.sg/znews/singapore/story20170824-789741)