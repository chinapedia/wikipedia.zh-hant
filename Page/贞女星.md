****（Virginia）是第50颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1857年10月4日由[德國天文學家](../Page/德國.md "wikilink")[卡尔·特奥多尔·罗伯特·路德发现](../Page/卡尔·特奥多尔·罗伯特·路德.md "wikilink")。的[直径为](../Page/直径.md "wikilink")99.8千米，[质量为](../Page/质量.md "wikilink")1.0×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1576.682天。

## 參考資料

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1857年发现的小行星](../Category/1857年发现的小行星.md "wikilink")