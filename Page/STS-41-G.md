****是历史上第十三次航天飞机任务，也是[挑战者号航天飞机的第六次太空飞行](../Page/挑戰者號太空梭.md "wikilink")。

## 任务成员

  - **[罗伯特·克里彭](../Page/罗伯特·克里彭.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[乔恩·麦克布莱德](../Page/乔恩·麦克布莱德.md "wikilink")**（，曾执行任务），飞行员
  - **[凯瑟琳·萨利文](../Page/凯瑟琳·萨利文.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[萨莉·莱德](../Page/萨莉·莱德.md "wikilink")**（，曾执行以及任务），任务专家
  - **[大卫·里茨马](../Page/大卫·里茨马.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[马克·加诺](../Page/马克·加诺.md "wikilink")**（，[加拿大宇航员](../Page/加拿大.md "wikilink")，曾执行、以及任务），有效载荷专家
  - **[保罗·斯库里·巴瓦](../Page/保罗·斯库里·巴瓦.md "wikilink")**（，曾执行任务），有效载荷专家

## 替补有效载荷专家

  - **[罗伯特·瑟斯克](../Page/罗伯特·瑟斯克.md "wikilink")**（，曾执行任务）

[Category:1984年佛罗里达州](../Category/1984年佛罗里达州.md "wikilink")
[Category:1984年科學](../Category/1984年科學.md "wikilink")
[Category:挑战者号航天飞机任务](../Category/挑战者号航天飞机任务.md "wikilink")
[Category:1984年10月](../Category/1984年10月.md "wikilink")