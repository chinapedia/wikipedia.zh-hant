**STARCAMP
EP**（）是[水樹奈奈第](../Page/水樹奈奈.md "wikilink")17首的[單曲](../Page/單曲.md "wikilink")。2008年2月6日發售。

## 解說

  - 在「[still in the
    groove](../Page/still_in_the_groove.md "wikilink")」後4年半以來，第一次有單曲的第一首歌不是本人作詞。
  - 第一张没有使用任何收录歌曲名字的单曲。
  - 全碟4首歌皆為主打歌。
  - 4曲皆以「[天空](../Page/天空.md "wikilink")」為主題
  - 單曲名稱「**STARCAMP
    EP**」的由來為：因為是第17張單曲，取[塔羅牌中第](../Page/塔羅牌.md "wikilink")17張牌「星
    (**STAR**)」，以及同樣思想的人所組成的「陣營
    (**CAMP**)」。「[EP](../Page/EP.md "wikilink")」則是因為總共四首歌。

## 收錄曲

1.  Astrogation
      - 作詞：[Hibiki](../Page/Hibiki.md "wikilink")、作曲・編曲：[陶山隼](../Page/陶山隼.md "wikilink")
      - [日本電視台系列](../Page/日本電視台.md "wikilink")『[音楽戦士 MUSIC
        FIGHTER](../Page/音楽戦士_MUSIC_FIGHTER.md "wikilink")』2008年2月[主題曲](../Page/主題曲.md "wikilink")
      - 「Astrogation」有著「宇宙飛行」「宇宙航空學」「天體地質學」「有關於宇宙研究、開發的技術與知識」等涵義。水樹本人對此歌名亦曾表示：「期待大家聽了之後會有什麼樣的解釋。」(「」)
2.  COSMIC LOVE
      - 作詞：[園田凌士](../Page/園田凌士.md "wikilink")、作曲・編曲：[藤田淳平](../Page/藤田淳平.md "wikilink")（[Elements
        Garden](../Page/Elements_Garden.md "wikilink")）
      - 動畫『[十字架與吸血鬼](../Page/十字架與吸血鬼.md "wikilink")』主題曲（[OP](../Page/OP.md "wikilink")）
      - [DS版](../Page/NDS.md "wikilink")『十字架與吸血鬼 七夕的陽海学園小姐』主題曲
3.  Dancing in the velvet moon
      - 作詞：水樹奈々、作曲：[上松範康](../Page/上松範康.md "wikilink")、編曲：上松範康・[中山真斗](../Page/中山真斗.md "wikilink")（[Elements
        Garden](../Page/Elements_Garden.md "wikilink")）
      - 動畫『十字架與吸血鬼』片尾曲（[ED](../Page/ED.md "wikilink")）
4.  空時計
      - 作詞：[SAYURI](../Page/片山さゆり.md "wikilink")、作曲・編曲：[大平勉](../Page/大平勉.md "wikilink")

[Category:2008年單曲](../Category/2008年單曲.md "wikilink")
[Category:水樹奈奈單曲](../Category/水樹奈奈單曲.md "wikilink")
[Category:日本電視台節目主題曲](../Category/日本電視台節目主題曲.md "wikilink")
[Category:UHF動畫主題曲](../Category/UHF動畫主題曲.md "wikilink")
[Category:遊戲主題曲](../Category/遊戲主題曲.md "wikilink")