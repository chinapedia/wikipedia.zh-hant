**寳曆**（825年正月－827年二月）是[唐敬宗的年號](../Page/唐敬宗.md "wikilink")，共计3年。

寳曆二年十二月[唐文宗李昂即位沿用](../Page/唐文宗.md "wikilink")。

## 大事記

## 出生

## 逝世

## 紀年

| 寳曆                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 825年                           | 826年                           | 827年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") |

## 參看

  - [渤海国文王](../Page/渤海国.md "wikilink")[大欽茂在](../Page/大欽茂.md "wikilink")[大兴时期曾用过一段年号](../Page/大興_\(大欽茂\).md "wikilink")，名称也是寳曆
  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 同期存在的其他政权年号
      - [天長](../Page/天长_\(淳和天皇\).md "wikilink")（824年正月五日至834年正月三日）：[平安時代](../Page/平安時代.md "wikilink")[淳和天皇之年號](../Page/淳和天皇.md "wikilink")
      - [建興](../Page/建興_\(大仁秀\).md "wikilink")（819年至830年）：[渤海宣王](../Page/渤海國.md "wikilink")[大仁秀之年號](../Page/大仁秀.md "wikilink")
      - [保和](../Page/保和.md "wikilink")（824年至839年）：[南詔領袖](../Page/南詔.md "wikilink")[勸豐祐之年號](../Page/勸豐祐.md "wikilink")
      - [彝泰](../Page/彝泰.md "wikilink")（815年至838年）：[吐蕃可黎可足](../Page/吐蕃.md "wikilink")[贊普](../Page/贊普.md "wikilink")[赤祖德贊之年號](../Page/赤祖德贊.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129
  - 所功，《年号の歴史―元号制度の史的研究》，雄山閣，1998年3月，ISBN 4639007116

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:9世纪中国年号](../Category/9世纪中国年号.md "wikilink")
[Category:820年代中国政治](../Category/820年代中国政治.md "wikilink")