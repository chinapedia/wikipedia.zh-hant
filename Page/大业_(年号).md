**大业**（605年正月—618年三月）是[隋朝政权第二代皇帝](../Page/隋朝.md "wikilink")[隋炀帝杨广的](../Page/隋炀帝.md "wikilink")[年号](../Page/年号.md "wikilink")，歷時13年餘。其名称来自《[易经](../Page/易经.md "wikilink")·系辞上》：「盛德**大业**至矣哉，富有之谓**大业**，日新之谓盛德。」

《[隋书](../Page/隋书.md "wikilink")》、《[北史](../Page/北史.md "wikilink")》等记载到大业十三年，《[资治通鉴](../Page/资治通鉴.md "wikilink")》只记载到十二年。清朝学者赵翼则认为[李渊虽然拥立](../Page/李渊.md "wikilink")[杨侑为帝](../Page/杨侑.md "wikilink")，改元[义宁](../Page/义宁.md "wikilink")，但是隋炀帝仍在，大业年号并没有废。事实上隋炀帝当时在江都，未必知道李渊已另立皇帝，故仍以皇帝自居并继续使用大业年号，直至被[宇文化及所殺](../Page/宇文化及.md "wikilink")。南京图书馆研究部主任徐忆农指出，《隋史》是唐太宗在位时修撰的，的确刻意回避“大业十四年”，但并非绝对禁止，如《隋史·许善心》传中写道：“十四年化及弑逆之日，隋官尽诣朝堂谒贺，善心独不至”，南京大学历史系教授[张学锋则指出有多种书写](../Page/张学锋.md "wikilink")“大业十四年”的墓志，《卢文构夫人月相墓志铭》中，有“大业十四年遇疾”；而洛阳出土的王德备墓志上书写“于时大业十四年正月十五日……卒”，该墓志撰于贞观十八年。张学锋认为唐初[朝廷对于](../Page/唐朝政府.md "wikilink")“大业十四年”的使用比较宽容\[1\]。

## 大事记

  - [大业元年](../Page/大业元年.md "wikilink")——隋击[契丹之战](../Page/契丹.md "wikilink")
  - [大业四年至](../Page/大业四年.md "wikilink")[五年](../Page/大业五年.md "wikilink")——隋击[吐谷浑之战](../Page/吐谷浑.md "wikilink")
  - [大业七年至隋亡](../Page/大业七年.md "wikilink")——[隋末农民起义](../Page/隋末农民起义.md "wikilink")
  - [大业七年至隋亡](../Page/大业七年.md "wikilink")——[瓦岗农民起义](../Page/瓦岗.md "wikilink")
  - [大业七年至唐武德四年](../Page/大业七年.md "wikilink")——[窦建德起义](../Page/窦建德.md "wikilink")
  - [大业八年](../Page/大业八年.md "wikilink")——隋炀帝第一次攻[高句丽之战](../Page/隋高句丽之战.md "wikilink")
  - [大业九年](../Page/大业九年.md "wikilink")——隋炀帝第二次攻[高句丽之战](../Page/高句丽.md "wikilink")
  - [大业九年六月至八月](../Page/大业九年.md "wikilink")——隋平杨玄感之战
  - [大业十年](../Page/大业十年.md "wikilink")——隋炀帝第三次攻高句丽之战
  - [大业十一年](../Page/大业十一年.md "wikilink")——[雁门之战](../Page/雁门.md "wikilink")
  - [大业十二年](../Page/大业十二年.md "wikilink")——[李渊击突厥之战](../Page/李渊.md "wikilink")
  - [大业十二年十二月](../Page/大业十二年.md "wikilink")——[雀鼠谷之战](../Page/雀鼠谷.md "wikilink")
  - [大业十三年二月](../Page/大业十三年.md "wikilink")——隋石子河之战
  - [大业十三年七月](../Page/大业十三年.md "wikilink")——隋河间之战

## 出生

## 逝世

## 纪年

| 大业                               | 元年                                 | 二年                                 | 三年                                 | 四年                                 | 五年                                 | 六年                                 | 七年                                 | 八年                                 | 九年                                 | 十年                                 |
| -------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [605年](../Page/605年.md "wikilink") | [606年](../Page/606年.md "wikilink") | [607年](../Page/607年.md "wikilink") | [608年](../Page/608年.md "wikilink") | [609年](../Page/609年.md "wikilink") | [610年](../Page/610年.md "wikilink") | [611年](../Page/611年.md "wikilink") | [612年](../Page/612年.md "wikilink") | [613年](../Page/613年.md "wikilink") | [614年](../Page/614年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink")     | [丙寅](../Page/丙寅.md "wikilink")     | [丁卯](../Page/丁卯.md "wikilink")     | [戊辰](../Page/戊辰.md "wikilink")     | [己巳](../Page/己巳.md "wikilink")     | [庚午](../Page/庚午.md "wikilink")     | [辛未](../Page/辛未.md "wikilink")     | [壬申](../Page/壬申.md "wikilink")     | [癸酉](../Page/癸酉.md "wikilink")     | [甲戌](../Page/甲戌.md "wikilink")     |
| 大业                               | 十一年                                | 十二年                                | 十三年                                | 十四年                                |                                    |                                    |                                    |                                    |                                    |                                    |
| [公元](../Page/公元纪年.md "wikilink") | [615年](../Page/615年.md "wikilink") | [616年](../Page/616年.md "wikilink") | [617年](../Page/617年.md "wikilink") | [618年](../Page/618年.md "wikilink") |                                    |                                    |                                    |                                    |                                    |                                    |
| [干支](../Page/干支纪年.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink")     | [丙子](../Page/丙子.md "wikilink")     | [丁丑](../Page/丁丑.md "wikilink")     | [戊寅](../Page/戊寅.md "wikilink")     |                                    |                                    |                                    |                                    |                                    |                                    |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [白鳥](../Page/白鳥_\(年號\).md "wikilink")（[613年十二月](../Page/613年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[向海明年号](../Page/向海明.md "wikilink")
      - [大世](../Page/大世.md "wikilink")（[614年五月](../Page/614年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[劉迦論年号](../Page/劉迦論.md "wikilink")
      - [昌達](../Page/昌達.md "wikilink")（[615年十二月](../Page/615年.md "wikilink")—[619年閏二月](../Page/619年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[楚政權](../Page/楚.md "wikilink")[朱粲年号](../Page/朱粲.md "wikilink")
      - [始兴](../Page/始兴_\(操师乞\).md "wikilink")（[616年十二月](../Page/616年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[操师乞年号](../Page/操师乞.md "wikilink")
      - [太平](../Page/太平_\(林士弘\).md "wikilink")（[616年十二月](../Page/616年.md "wikilink")—[622年十月](../Page/622年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[楚政權](../Page/楚.md "wikilink")[林士弘年号](../Page/林士弘.md "wikilink")
      - [丁丑](../Page/丁丑_\(窦建德\).md "wikilink")（[617年正月](../Page/617年.md "wikilink")—[618年十一月](../Page/618年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[夏政權](../Page/夏.md "wikilink")[窦建德年号](../Page/窦建德.md "wikilink")
      - [永平](../Page/永平_\(李密\).md "wikilink")（[617年二月](../Page/617年.md "wikilink")—[618年十二月](../Page/618年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[魏政權](../Page/魏.md "wikilink")[李密年号](../Page/李密.md "wikilink")
      - [天兴](../Page/天兴_\(刘武周\).md "wikilink")（[617年三月](../Page/617年.md "wikilink")—[620年四月](../Page/620年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[刘武周年号](../Page/刘武周.md "wikilink")
      - [永隆](../Page/永隆_\(梁師都\).md "wikilink")（[618年](../Page/618年.md "wikilink")—[628年四月](../Page/628年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[梁政權](../Page/梁.md "wikilink")[梁師都年号](../Page/梁師都.md "wikilink")
      - [正平](../Page/正平_\(郭子和\).md "wikilink")（[617年三月](../Page/617年.md "wikilink")—[618年七月](../Page/618年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[永樂王](../Page/永樂.md "wikilink")[郭子和年号](../Page/郭子和.md "wikilink")
      - [秦兴](../Page/秦兴.md "wikilink")（[617年四月](../Page/617年.md "wikilink")—[618年十一月](../Page/618年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[秦政權](../Page/秦.md "wikilink")[薛擧年号](../Page/薛擧.md "wikilink")
      - [鸣凤](../Page/鸣凤.md "wikilink")（[617年四月](../Page/617年.md "wikilink")—[618年十一月](../Page/618年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[梁政權](../Page/梁.md "wikilink")[蕭銑年号](../Page/蕭銑.md "wikilink")
      - [通聖](../Page/通聖.md "wikilink")（[617年十二月](../Page/617年.md "wikilink")）：[隋朝時期](../Page/隋朝.md "wikilink")[曹武徹年号](../Page/曹武徹.md "wikilink")
      - [延和](../Page/延和_\(麴伯雅\).md "wikilink")（[602年](../Page/602年.md "wikilink")—[613年](../Page/613年.md "wikilink")）：[高昌政权](../Page/高昌.md "wikilink")[麴伯雅年号](../Page/麴伯雅.md "wikilink")
      - [义和](../Page/义和_\(高昌\).md "wikilink")（[614年](../Page/614年.md "wikilink")—[619年](../Page/619年.md "wikilink")）：[高昌政权年号](../Page/高昌.md "wikilink")
      - [建福](../Page/建福_\(新羅真平王\).md "wikilink")（[584年](../Page/584年.md "wikilink")—634年）：[新羅](../Page/新羅.md "wikilink")[真平王的年號](../Page/新羅真平王.md "wikilink")

## 注释

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 松橋達良，《》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:隋朝年号](../Category/隋朝年号.md "wikilink")
[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:600年代中国政治](../Category/600年代中国政治.md "wikilink")
[Category:610年代中国政治](../Category/610年代中国政治.md "wikilink")

1.