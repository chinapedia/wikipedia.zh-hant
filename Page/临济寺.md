[Linji_monastery02.JPG](https://zh.wikipedia.org/wiki/File:Linji_monastery02.JPG "fig:Linji_monastery02.JPG")

**临济寺**，位于[河北省](../Page/河北省.md "wikilink")[正定县](../Page/正定县.md "wikilink")[老城内](../Page/正定城.md "wikilink")，为[佛教](../Page/佛教.md "wikilink")[禅宗](../Page/禅宗.md "wikilink")[临济宗祖庭](../Page/临济宗.md "wikilink")。

## 历史

临济寺始建于[北朝](../Page/北朝.md "wikilink")[东魏兴和二年](../Page/东魏.md "wikilink")（540年）。《正定县志》载：“临济寺，东魏兴和二年建，在城东南二里许临济村。”概因濒临[滹沱河渡口得名](../Page/滹沱河.md "wikilink")。今在其原址立有临济院旧址纪念碑。\[1\]

### 唐

[A_statue_of_Linji_Yixuan.JPG](https://zh.wikipedia.org/wiki/File:A_statue_of_Linji_Yixuan.JPG "fig:A_statue_of_Linji_Yixuan.JPG")
[唐](../Page/唐.md "wikilink")[大中八年](../Page/大中.md "wikilink")（854年），佛教临济宗创始人[义玄入驻此寺](../Page/义玄.md "wikilink")。唐咸通元年（860年）镇州（正定）战争不断，檀越墨君和将临济寺迁入城内（即临济寺今址）。

唐[咸通八年](../Page/咸通.md "wikilink")（867年）四月十日，义玄禅师[圆寂](../Page/圆寂.md "wikilink")。遗体火化后，其弟子将舍利分建二塔藏之，一塔建于河北[大名](../Page/大名县.md "wikilink")（已毁），一塔建于河北正定城内临济寺。[唐懿宗赐谥](../Page/唐懿宗.md "wikilink")“惠照禅师”，赐正定塔为“澄灵塔”。

### 宋金

临济寺在[宋金战争中毁于战火](../Page/宋金战争.md "wikilink")，仅存残塔。[金大定二十三年](../Page/金.md "wikilink")（1183年），[金世宗下旨修复澄灵塔及临济寺各建筑](../Page/金世宗.md "wikilink")。今之澄灵塔即为辽金典型样式。\[2\]

### 元

[元朝时](../Page/元朝.md "wikilink")，住持[海雲印簡禪師主持了重修和扩建](../Page/海雲印簡.md "wikilink")。[元](../Page/元朝.md "wikilink")[葛罗禄迺贤所著](../Page/葛罗禄迺贤.md "wikilink")《河朔访古记》载：“临济寺在真定府城中，定远门街，飞云楼之东。其三门下有唐[吴道子所画](../Page/吴道子.md "wikilink")[布袋和尚像及摇铃普化真赞](../Page/布袋和尚.md "wikilink")、东坡墨竹、绿筠轩诗等石刻，极为精细”。元至大二年（1309年），[赵孟頫奉敕撰](../Page/赵孟頫.md "wikilink")《临济正宗碑》文，立于寺中。

### 明

[明朝正德十六年](../Page/明朝.md "wikilink")（1521年），临济寺又进行了重修，当时，寺内的主要建筑有山门、大雄宝殿、澄灵塔和祖堂及僧房。并新立石碑，由王饼撰记，郭希愈书。至明末清初，寺院再度荒废。

### 清

[清朝](../Page/清朝.md "wikilink")[雍正帝封临济寺僧义玄为](../Page/雍正帝.md "wikilink")“真常惠幽禅师”，并拨银重修临济寺。[道光十年](../Page/道光.md "wikilink")（1830年），总兵[舒通阿出资再度重修](../Page/舒通阿.md "wikilink")，并将祖师殿移至大雄宝殿两侧。

### 民国

[抗日战争时期](../Page/中国抗日战争.md "wikilink")，临济寺除澄灵塔外，尚有祖堂三间，东配殿三间。1947年底，这些殿堂都被拆毁，仅存澄灵塔。

### 1949年后

[中国共产党夺取中国政权后推行](../Page/中国共产党.md "wikilink")[无神论](../Page/无神论.md "wikilink")，其控制区域内宗教活动受到抑制。直到1983年该寺被定为[汉族地区佛教全国重点寺院](../Page/汉族地区佛教全国重点寺院.md "wikilink")。而后于1984年正式交由佛教界管理，作为佛教活动的场所开放。

中日两国临济宗与[黄檗宗](../Page/黄檗宗.md "wikilink")（临济宗分支，明末清初由[福建](../Page/福建.md "wikilink")[福清县](../Page/福清县.md "wikilink")[黄檗山](../Page/黄檗山.md "wikilink")[万福寺临济宗](../Page/万福寺.md "wikilink")[隐武禅师赴日创立](../Page/释隐武.md "wikilink")）共奉临济寺为祖庭。1979年5月，日本临济宗、黄檗宗联合成立“日中友好临黄协会”。自1980年起，每年派遣代表团访华，朝拜临济禅师塔，并捐款维修。1985年由日本临济宗、黄檗宗法侣资助及当地政府出资，修复“澄灵塔”，\[3\]其后又逐步重建大雄宝殿、祖堂、寺舍等。

1986年5月19日，寺内举行了祖塔修复落成剪彩仪式佛像开光典礼和诵经法会，有日中友好临黄协会访华团一百人及当地佛教人士共同参加。开光法会由[中国佛教协会常务理事](../Page/中国佛教协会.md "wikilink")、[广东省佛教协会副会长](../Page/广东省佛教协会.md "wikilink")、[光孝寺住持](../Page/光孝寺.md "wikilink")、临济宗第四十四代传人[本焕法师和日本](../Page/释本焕.md "wikilink")[临济宗永源寺派管长](../Page/临济宗永源寺派.md "wikilink")[筱原大雄长老共同主持](../Page/筱原大雄.md "wikilink")。

1988年5月15日，[河北省佛教协会在寺内成立](../Page/河北省佛教协会.md "wikilink")，[净慧法师任会长](../Page/释净慧.md "wikilink")。

1990年10月14日，[有明法师接任临济寺方丈](../Page/释有明.md "wikilink")，为临济宗第四十五代传人。有[青岛](../Page/青岛.md "wikilink")[湛山寺方丈](../Page/湛山寺.md "wikilink")[明哲](../Page/释明哲.md "wikilink")、[洛阳](../Page/洛阳.md "wikilink")[白马寺方丈](../Page/白马寺.md "wikilink")[海法](../Page/释海法.md "wikilink")、[五台山](../Page/五台山.md "wikilink")[善财洞监院](../Page/善财洞.md "wikilink")[能修法师等诸山长老及各地教徒](../Page/释能修.md "wikilink")300余人参加了有明法师的升座仪式。

## 建筑

### 山门殿

山门殿为[歇山灰筒瓦顶](../Page/歇山.md "wikilink")，面宽呈明五暗七形式，殿中央立“义玄禅师大型石刻画像碑”。此碑选用青田石料，由碑身和须弥座两部分组成，通高2.9米。碑身正面镌义玄禅师半身画像。像的左上方刻着[中国佛教协会会长](../Page/中国佛教协会.md "wikilink")[赵朴初题写的](../Page/赵朴初.md "wikilink")《临济义玄禅师像赞》：

石碑的背面，刻《临济义玄禅师传赂》。

### 澄灵塔

澄灵塔为八角九级[密檐式实心砖塔](../Page/密檐式塔.md "wikilink")，高30.47米。塔身嵌有清雍正十二年（1734年）谕旨石刻。“卍”字图案和花卉图案及“唐临济慧照澄灵塔”石匾。塔身各檐角梁为木制，檐瓦、脊兽和套兽均为绿琉璃制作。塔刹由仰莲、宝瓶、相轮、圆光、宝盖、仰月、宝珠等组成。\[4\]
[The_Mahavira_Palace_of_Linji_Monastery_01.JPG](https://zh.wikipedia.org/wiki/File:The_Mahavira_Palace_of_Linji_Monastery_01.JPG "fig:The_Mahavira_Palace_of_Linji_Monastery_01.JPG")

### 大雄宝殿

临济寺大雄宝殿于1987年落成，坐北朝南，位于澄灵塔后，高11米，宽20米，面阔五间，[硬山调大脊灰筒瓦顶](../Page/硬山.md "wikilink")，额枋绘旋子彩画。殿中供奉释迦牟尼佛和迦叶、阿难二尊者像以及文殊、普贤、观音像，两侧是樟木雕刻的十八罗汉像。
[Lin_Ji_Si.JPG](https://zh.wikipedia.org/wiki/File:Lin_Ji_Si.JPG "fig:Lin_Ji_Si.JPG")

### 其他

法乳堂，位于大雄宝殿东侧，殿内供奉菩提达摩大师、六祖[惠能禅师](../Page/惠能.md "wikilink")、义玄禅师三位祖师像。传灯堂在大雄宝殿西侧，供奉日本临济宗荣西禅师、南浦绍明禅师、日本黄檗宗初祖隐元禅师三位祖师像。

在大雄宝殿两侧建两厢配殿，东为客堂、斋堂，西为讲堂、禅堂，大殿后有[藏经楼](http://commons.wikimedia.org/wiki/Image:Linji_monastery.JPG)、方丈室等。

## 參考文献

<div class="references-small">

<references/>

</div>

## 外部链接

  - [临济在线](http://www.linjichansi.com/)與主題不合

[L](../Category/石家庄佛寺.md "wikilink") [L](../Category/河北的塔.md "wikilink")
[L](../Category/金代的塔.md "wikilink") [L](../Category/密檐式塔.md "wikilink")
[L](../Category/正定县.md "wikilink")
[L临](../Category/汉族地区佛教全国重点寺院.md "wikilink")

1.  凤鸣：《日本临济宗岭长老来临济寺朝拜祖庭》。《临济在线》報導，2006年9月11日，[新聞](../Page/新聞.md "wikilink")。http://www.linjichansi.com/Article/ShowArticle.asp?ArticleID=221,

2.  梁思成，《中国建筑史》，百花文艺出版社，ISBN 7530626051，2003年，215页
3.  刘友恒 聂连顺 樊子林，《正定古塔》，河北省正定县文物保管所编印，1986.10，p.7
4.  刘友恒 聂连顺 樊子林，《正定古塔》，河北省正定县文物保管所编印，1986.10，p.5