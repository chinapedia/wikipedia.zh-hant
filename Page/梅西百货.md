[MacysDepartmentStoreNewyork.jpg](https://zh.wikipedia.org/wiki/File:MacysDepartmentStoreNewyork.jpg "fig:MacysDepartmentStoreNewyork.jpg")
**梅西百货**（**Macy's**）是[美国的一个连锁](../Page/美国.md "wikilink")[百货公司](../Page/百货公司.md "wikilink")，其[旗舰店位于](../Page/旗舰.md "wikilink")[纽约市](../Page/纽约市.md "wikilink")[先驱广场](../Page/先驱广场.md "wikilink")\[1\]，1924年在第7大道开张时曾被宣传为“世界最大商店”。该公司还有2个全国性旗舰店，分别设在[旧金山的](../Page/旧金山.md "wikilink")[联合广场和](../Page/联合广场_\(旧金山\).md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")[州街](../Page/州街_\(芝加哥\).md "wikilink")。

该公司是以辛辛那提为基地的[联邦百货公司](../Page/联邦百货公司.md "wikilink")（2007年更名梅西公司\<Macy's
Inc\>）的一部分。该公司还有几座地区性的旗舰店，分布在[亚特兰大](../Page/亚特兰大.md "wikilink")、[迈阿密](../Page/迈阿密.md "wikilink")、[圣路易斯和](../Page/圣路易斯_\(密苏里州\).md "wikilink")[西雅图](../Page/西雅图.md "wikilink")。

梅西百货从1924年开始，每年[感恩节都会在纽约市举行盛大的](../Page/感恩节.md "wikilink")[感恩节大游行](../Page/梅西感恩节大游行.md "wikilink")。而且每年圣诞节都会有别出风格的主题橱窗秀。

近年不敵電子商務的趨勢，2017年梅西百貨總共關閉了68家門店，2018年年初再關閉11家門店。\[2\]

## 参考文献

## 外部链接

  - [官方网站](https://web.archive.org/web/20090210202041/http://www.macys.com/)
      - [Macy's Wedding & Gift
        Registry](https://web.archive.org/web/20070529093632/http://macys-catalog.weddingchannel.com/catalog/fds/macysDivisionalHome.action)
  - [The Longest Running Show on
    Broadway](https://web.archive.org/web/20080527001307/http://www.wm.edu/amst/370/2005F/sp4/)
  - ["New Macy's is cream of the East
    Coast"](http://www.philly.com/mld/inquirer/news/magazine/daily/15184159.htm)
    by Elizabeth Wellington, *Philadelphia Inquirer*, August 3, 2006
  - ["Name Change Hurt Macy's: Decision to drop ‘Lazarus’ not a hit
    here"](http://www.dispatch.com/business-story.php?story=dispatch/2006/06/06/20060606-F1-01.html)
    by Jeffrey Sheban, *The Columbus Dispatch*, Tuesday, June 06, 2006

[M梅](../Category/美國百貨公司.md "wikilink")
[Category:1858年成立的公司](../Category/1858年成立的公司.md "wikilink")
[M](../Category/俄亥俄州公司.md "wikilink")
[M](../Category/美國服裝零售商.md "wikilink")

1.
2.