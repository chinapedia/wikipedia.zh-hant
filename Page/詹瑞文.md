**詹瑞文**（**Jim Chim Shui
Man**；），香港[舞臺劇男演員](../Page/舞臺劇.md "wikilink")、導演及編劇。自1993年留學回港後，創辦[劇場組合劇團及](../Page/劇場組合.md "wikilink")[PIP藝術學校](../Page/PIP藝術學校.md "wikilink")，並演出多套劇目及電影，獲得多個獎項，其劇作《[男人之虎](../Page/男人之虎.md "wikilink")》曾演出128場，吸引逾90,000觀眾，創下香港劇場紀錄，被視為香港重要的舞台藝術家。2008年改組[劇場組合](../Page/劇場組合.md "wikilink")，脫離香港特區政府資助架構，成立[PIP文化產業](../Page/PIP文化產業.md "wikilink")。2013年10月受[鄭經翰之邀在其網台](../Page/鄭經翰.md "wikilink")[D100與](../Page/D100.md "wikilink")[李麗蕊主持全新節目](../Page/李麗蕊.md "wikilink")《[喜劇之王](../Page/喜劇之王.md "wikilink")》，節目於11月3日首播，逢星期日早上十時至下午一時在自己友台播岀。

於2012-2013年度微笑企業大獎頒獎典禮擔任微笑大使，頒獎予港澳地區多個於微笑服務表現優異的企業。\[1\]

## 生平

詹瑞文童年時居於[大坑東的七層大廈](../Page/大坑東.md "wikilink")[徙置區](../Page/徙置區.md "wikilink")，一家七口擠在百多呎小單位，小一到小三都在[天台學校唸書](../Page/天台學校.md "wikilink")，小四時才轉到[寶安商會學校就讀](../Page/寶安商會學校.md "wikilink")，但1984年[會考時](../Page/會考.md "wikilink")，因成績不理想而要留班，轉到私校唸書，此後報讀[香港演藝學院戲劇學院](../Page/香港演藝學院.md "wikilink")，最初不獲取錄，第二次報考時才能入讀該院校。

1990年畢業於香港演藝學院戲劇學院，隨後獲著名[英國形體劇場導演](../Page/英國.md "wikilink")[David
Glass賞識](../Page/David_Glass.md "wikilink")，參與英國巡迴演出及研習現代默劇。翌年跟隨大師級表演導師[Philippe
Gaulier及](../Page/Philippe_Gaulier.md "wikilink")[Monika
Pagneux](../Page/Monika_Pagneux.md "wikilink")
研習演技及學習形體、聲線及物件劇場等技巧訓練，其「PIP
理念 (Pleasure．Imagination．Play---快樂．想像．遊戲)」亦告萌芽。這是他的人生哲學，他生活與創作的本源。

他亦從事演員培訓及劇場教育，曾出任[香港演藝學院客席戲劇導師](../Page/香港演藝學院.md "wikilink")、[無綫電視藝員訓練班表演及默劇導師](../Page/無綫電視藝員訓練班.md "wikilink")，應邀擔任多位影星，包括[梁洛施](../Page/梁洛施.md "wikilink")、[鍾欣桐](../Page/鍾欣桐.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")、[林嘉欣](../Page/林嘉欣.md "wikilink")、名模[蔣怡](../Page/蔣怡.md "wikilink")、電視明星[鄧萃雯](../Page/鄧萃雯.md "wikilink")、[徐子珊](../Page/徐子珊.md "wikilink")、[鄭嘉穎](../Page/鄭嘉穎.md "wikilink")、[吳佩慈和國內影星](../Page/吳佩慈.md "wikilink")[赫蕾等的表演導師](../Page/赫蕾.md "wikilink")。他亦經常應邀往海外如[北京](../Page/北京.md "wikilink")、[英國](../Page/英國.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣及](../Page/台灣.md "wikilink")[上海等地](../Page/上海.md "wikilink")，為專業劇團及大學主持工作坊。2003年創立
[PIP藝術學校](../Page/PIP藝術學校.md "wikilink")，開設各式課程，由小孩子的藝術課程，成人的專業表演及個人成長課程，到企業管理培訓課程等，均由「玩」開始，透過各種「遊戲式」的練習，讓參加者體驗快樂，激發創意，將PIP(Pleasure
Imagination Play)理念帶進生活。

### 演藝事業

1993年詹瑞文回港，與[甄詠蓓創辦](../Page/甄詠蓓.md "wikilink")[劇場組合劇團](../Page/劇場組合.md "wikilink")，同時擔任編劇、導演及演員，演出作品包括經典劇及自創劇目，部份屬個人演出。2004年的《兩條老柴玩遊戲》曾到[日本及](../Page/日本.md "wikilink")[台灣作巡迴表演](../Page/台灣.md "wikilink")。詹瑞文創作「形體棟篤笑」表演方式，擅長以[身體語言及扮演不同角色營造惹笑效果](../Page/身體語言.md "wikilink")。2003年創立PIP藝術學校。

詹瑞文曾演出多部電影、[電視](../Page/電視.md "wikilink")[廣告及舞台劇](../Page/廣告.md "wikilink")，亦曾擔任戲劇導師、表演及默劇導師，及予演員私人授課，曾教導韓國[忠武路影后](../Page/忠武路.md "wikilink")、金像金馬女配角[林嘉欣及葡萄牙波圖影后](../Page/林嘉欣.md "wikilink")[梁洛施](../Page/梁洛施.md "wikilink")。他多年來的成績獲得業界肯定，曾獲香港多個獎項，1999年獲[香港藝術發展局頒發第一屆戲劇發展獎](../Page/香港藝術發展局.md "wikilink")，並分別於第7屆及第9屆香港舞台劇獎，獲頒最佳男配角（悲／正劇）獎及最佳男主角（喜／鬧劇）獎等。1999年獲得荷蘭當地之獎學金，跟隨名師Claire
Heggen及Vicente Fuentes，深造形體、聲線及物件劇場。2000年獲香港藝術發展局戲劇小組委員會頒發第一屆戲劇發展獎。

詹瑞文曾於2004及2005年與[香港小交響樂團合作](../Page/香港小交響樂團.md "wikilink")，和樂團音樂總監[葉詠詩主持音樂](../Page/葉詠詩.md "wikilink")[棟篤笑音樂會](../Page/棟篤笑.md "wikilink")「音樂詞彙笑療法」。2006年11月，他們再次合作，於[紅磡體育館舉行](../Page/紅磡體育館.md "wikilink")「詹瑞文&葉詠詩棟篤交響Show」。

2007年，詹瑞文的成名作《[男人之虎](../Page/男人之虎.md "wikilink")》曾7度公演，演出超過128場，吸引超過90,000名觀眾，為香港劇場的紀錄，亦奠定他舞台藝術家的地位。2008年3月12日，「劇場組合」聯合藝術總監詹瑞文召開記者招待會，向傳媒宣佈
—「劇場組合」為配合劇團文化藝術發展的願景與步伐，決定於2008年3月31日的資助期屆滿後，主動放棄政府的資助，成立[文化產業機構](../Page/文化創意產業.md "wikilink")「[PIP文化產業](../Page/PIP文化產業.md "wikilink")」（簡稱PIP），把創作領域從[舞台擴展至不同範疇](../Page/舞台.md "wikilink")。在[文化產業的營運模式下](../Page/文化創意產業.md "wikilink")，PIP將以長遠及有系統的商業化管理，以更主動、更靈活的方式，推動藝術行業的建立及提昇藝術的[經濟價值](../Page/經濟.md "wikilink")，從而帶動優化香港的[藝術文化生態](../Page/藝術.md "wikilink")。

2008年6月，《男人之虎》將第8度公演，取名為「超級防火版」，並以2008年[北京奧運為題](../Page/2008年夏季奧林匹克運動會.md "wikilink")，由詹瑞文以火炬手造型飾演的「凸首詹」（即詹特首的諧音，實為扮演「[煲呔曾](../Page/煲呔.md "wikilink")」，[借代當時](../Page/借代.md "wikilink")[香港特別行政區行政長官](../Page/香港特別行政區行政長官.md "wikilink")[曾蔭權](../Page/曾蔭權.md "wikilink")）作為主體。劇組在2008年5月6日在[旺角鬧市舉行](../Page/旺角.md "wikilink")「凸首補跑迎奧運」活動，詹瑞文扮演火炬手「凸首詹」，在兩名扮演[聖火護衛隊的工作人員護跑下](../Page/聖火護衛隊.md "wikilink")，從[新寶戲院跑往](../Page/新寶戲院.md "wikilink")[豉油街](../Page/豉油街.md "wikilink")，吸引不少途人圍觀\[2\]\[3\]。此外，在其宣傳海報上尤其放大傳送[香港區火炬接力的情節](../Page/2008年夏季奧林匹克運動會香港區火炬接力.md "wikilink")。

## 演出作品

### 舞台剧

  - 《紅日落‧紅日出》
  - 《驚驗補習》
  - 《再見紅日出》
  - 《甩翼天使》
  - 《大小不良》
  - 《無人地帶》
  - 《射日》
    <sup>（第八屆香港舞台劇獎「十大最受歡迎製作獎」）</sup>
  - 《兩條老柴玩遊戲》
    <sup>（第九屆香港舞台劇獎「最佳男主角（喜/鬧劇）」及「十大最受歡迎製作獎」）</sup>
  - 《動物農莊攪攪震》
    <sup>（第十屆香港舞台劇獎「十大最受歡迎製作獎」）</sup>
  - 《無好死》
  - 《大食騷》
  - 《月光光》
  - 《麻甩騷》
  - 《香港煙花燦爛 also know as 港燦》
    <sup>（第十二屆香港舞台劇獎「十大最受歡迎製作獎」）</sup>
  - 《跟住個口靚妹氹氹轉》、《男人·張生·Romeo》- [毛俊輝實驗製作](../Page/毛俊輝.md "wikilink")
  - 《棺材太大洞太小》 - 新加坡實踐劇場
  - 《迷宮》 - 北京兒童藝術劇院
  - 《大娛樂家》 - [進念二十面體](../Page/進念二十面體.md "wikilink")
  - 《畸人說夢》- [城市當代舞蹈團](../Page/城市當代舞蹈團.md "wikilink")
  - 《單人匹馬詹瑞文》
  - 《虎鶴雙形》
  - 《烏哩單刀》
  - 《玩得喜》
  - 《棟篤文響SHOW》- [香港小交響樂團](../Page/香港小交響樂團.md "wikilink")
  - 《音樂詞彙笑療法》、《古典音樂速成 - 音樂詞彙笑療法「易」》-
    [香港小交響樂團](../Page/香港小交響樂團.md "wikilink")
  - 《[男人之虎](../Page/男人之虎.md "wikilink")》
  - 《[萬世歌王](../Page/萬世歌王.md "wikilink")》
  - 《月亮7個半》
  - 《廁客浮士德》
  - 《蛇鼠一窩》
  - 《[萬千師奶賀台慶](../Page/萬千師奶賀台慶.md "wikilink")》
  - 《仲夏夜の夢》
  - 《港女發狂之港男發瘟》
  - 《笑林十八窮人》
  - 《0靚模襲地球》
  - 《真的等不了》
  - 《笑帝》
  - 《[柔软](../Page/柔软（戏剧）.md "wikilink")》
  - 《踢館》
  - 《女人之虎》
  - 《女人之虎之挑剔女皇》
  - 《硬膠先生勇救核心價值》
  - 《帝女詹-歡樂有渣拿》

### 电影

  - 《[買兇拍人](../Page/買兇拍人.md "wikilink")》 飾 雙鎗雄
  - 《[大丈夫](../Page/大丈夫.md "wikilink")》 飾 網吧經理巴治奧
  - 《[絕世好B](../Page/絕世好B.md "wikilink")》 飾 醫院高級副院長、催眠師金雷蒙醫生
  - 《[龍咁威](../Page/龍咁威.md "wikilink")》 飾 貴利公司大佬
  - 《[絕種鐵金剛](../Page/絕種鐵金剛.md "wikilink")》飾 李先生、李太太
  - 《[婚前殺行為](../Page/婚前殺行為.md "wikilink")》
  - 《[戀上你的床](../Page/戀上你的床.md "wikilink")》飾 顧全家醫生
  - 《[重案黐孖Gun](../Page/重案黐孖Gun.md "wikilink")》飾 朱因
  - 《[絕世好賓](../Page/絕世好賓.md "wikilink")》 飾
    深水埗區議員詹鈺成、嫖客、Peter仔、表弟、飛哥、四姨婆、食環署衛生督察、A貨推銷員
    (提名2005年[香港電影金像獎最佳男配角](../Page/香港電影金像獎.md "wikilink"))
  - 《[我要做Model](../Page/我要做Model.md "wikilink")》飾 Lawrence
  - 《[墨斗先生](../Page/墨斗先生.md "wikilink")》 飾 銀行何經理
  - 《[公主復仇記](../Page/公主復仇記.md "wikilink")》 飾 偷情父親
  - 《[AV](../Page/AV.md "wikilink")》 飾 舅父
  - 《[出埃及記](../Page/出埃及記.md "wikilink")》
  - 《[春田花花同學會](../Page/春田花花同學會.md "wikilink")》飾 會議主講經理
  - 《[伊莎貝拉](../Page/伊莎贝拉_\(电影\).md "wikilink")》 飾 包租公
  - 《[大丈夫2](../Page/大丈夫2.md "wikilink")》飾 爸爸生、雄哥
  - 《[戲王之王](../Page/戲王之王.md "wikilink")》 飾 陳文龍
  - 《[美麗密令](../Page/美麗密令.md "wikilink")》 飾 岑志文
  - 《[我愛HK開心萬歲](../Page/我愛HK開心萬歲.md "wikilink")》
  - 《[猛男滾死隊](../Page/猛男滾死隊.md "wikilink")》飾 張秋雲
  - 《[潮性辦公室](../Page/潮性辦公室.md "wikilink")》飾 Ben Chow
  - 2011年《[桃姐](../Page/桃姐.md "wikilink")》飾 Roger 舊同學
  - 2012年《[春嬌與志明](../Page/春嬌與志明.md "wikilink")》飾 Paul
  - 2012年《[低俗喜劇](../Page/低俗喜劇.md "wikilink")》飾 軍火劉
  - 2013年《[臨終囧事](../Page/臨終囧事.md "wikilink")》
  - 2013年《[飛虎出征](../Page/飛虎出征.md "wikilink")》飾 媽媽生
  - 2014年《[黑色喜劇](../Page/黑色喜劇.md "wikilink")》飾 天使詹
  - 2014年 [唐伯虎衝上雲霄](../Page/唐伯虎衝上雲霄.md "wikilink") 客串
  - 2015年 [臨時演員](../Page/临时演员_\(电影\).md "wikilink")
  - 2015年 《[港囧](../Page/港囧.md "wikilink")》飾 陳大夫
  - 2018年 《[我的情敵女婿](../Page/我的情敵女婿.md "wikilink")》飾

### 配音

  - 1993年 《[阿拉丁](../Page/阿拉丁_\(電影\).md "wikilink")》聲演 小販（旁白）
  - 1994年 《[獅子王](../Page/獅子王.md "wikilink")》聲演 成年辛巴
  - 1996年 《[鐘樓駝俠](../Page/鐘樓怪人_\(1996年電影\).md "wikilink")》聲演 加西莫多
  - 1996年 《》聲演 烏龜
  - 2005年 《[荒失失奇兵](../Page/馬達加斯加_\(2005年電影\).md "wikilink")》聲演 邁文
  - 2005年 《[露寶治的世界](../Page/露寶治的世界.md "wikilink")》聲演 發明家大焊
  - 2005年 《[龍刀奇緣](../Page/龍刀奇緣.md "wikilink")》聲演 胡老師
  - 2006年 《》聲演 戴偉德
  - 2009年 《[麥兜响噹噹](../Page/麥兜响噹噹.md "wikilink")》聲演 旁白
  - 2011年 《[神奇俠侶](../Page/神奇俠侶.md "wikilink")》聲演 皇帝、百曉生

### 导演作品

  - 《[潮性辦公室](../Page/潮性办公室_\(戏剧\).md "wikilink")》（2009-2012年）
  - 《[桃色办公室](../Page/桃色办公室_\(戏剧\).md "wikilink")》（2011年）《潮性办公室》普通话版，[孟京辉监制](../Page/孟京辉.md "wikilink")\[4\]
  - 《[微男博女](../Page/微男博女.md "wikilink")》（2011年）
  - 《[失戀單位](../Page/失戀單位.md "wikilink")》（2012年）

### 節目

  - 《碌卡大導》
  - 2018年：[VR驅魔人](../Page/VR驅魔人.md "wikilink")

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/萬世歌王.md" title="wikilink">萬世歌王</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/東亞唱片.md" title="wikilink">東亞唱片</a></p></td>
<td style="text-align: left;"><p>2006年10月9日</p></td>
<td style="text-align: left;"><p>AVCD</p>
<ol>
<li>Computer Data</li>
<li>單人匹訪 Interviewing</li>
<li>奮鬥歌之上海灘 MV</li>
<li>卡門 (沖凍水涼版)</li>
<li>新香港地 (OT: 香港地)</li>
<li>奮鬥歌之上海灘</li>
<li>Be Yourself (OT: 我找到自己)</li>
<li>Seven Daffodils</li>
<li>難為正邪定分界</li>
<li>“萬世歌王” 主題曲</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/Heageology.md" title="wikilink">Heageology</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/英皇娛樂.md" title="wikilink">英皇娛樂</a><br />
<a href="../Page/音樂家唱片.md" title="wikilink">音樂家唱片</a></p></td>
<td style="text-align: left;"><p>2015年2月10日</p></td>
<td style="text-align: left;"><p>CD</p>
<ol>
<li>慶合合</li>
<li>十五二十</li>
<li>陰公豬</li>
<li>舞大舞細</li>
</ol>
<p>DVD</p>
<ol>
<li>慶合合 MV</li>
<li>十五二十 MV</li>
<li>陰公豬 NV</li>
</ol></td>
</tr>
</tbody>
</table>

## 廣告

  - 2013年 [特強喜療妥](../Page/特強喜療妥.md "wikilink") 代言人
  - 2012年 [新鴻基金融](../Page/新鴻基金融.md "wikilink") 代言人
  - 2012年 [Sharp](../Page/夏普.md "wikilink") AQUOS 3D LX640系列 代言人
  - 2012年 [獅王](../Page/獅王.md "wikilink") 納米樂洗衣液 代言人

## 獎項

  - 1998年 香港舞台劇獎：最佳男配角
  - 2000年 香港舞台劇獎：最佳男主角（喜／鬧劇）
  - 2000年 [香港藝術發展局戲劇小組委員會](../Page/香港藝術發展局.md "wikilink")：第一屆戲劇發展獎
  - 2004年 [皇家禮炮](../Page/皇家禮炮.md "wikilink")（Royal Salute）：創見人士2004（Men
    of Vision 2004）
  - 2005年 獲民政事務處頒授十大傑出服務（文化）獎
  - 2005年 [新城勁爆兒歌頒獎禮](../Page/新城勁爆兒歌頒獎禮.md "wikilink")：新城勁爆兒歌
  - 2006年 [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink")：新城勁爆創意表現成就大獎
  - 2006年 演藝家年獎「我最喜愛舞台劇演員銀獎」
  - 2007年 香港特區10週年電影選舉「最具潛質男演員優異獎」

## 職務

  - 香港藝術發展局評審
  - 香港戲劇協會評審
  - [劇場組合聯合藝術總監](../Page/劇場組合.md "wikilink")
  - [PIP文化產業執行董事](../Page/PIP文化產業.md "wikilink")
  - PIP藝術學校校長

## 外部連結

  - [官方個人網頁](http://www.jimchim.com)
  - [官方公司網頁](http://www.pip-group.org)
  - [詹瑞文 Facebook](http://www.facebook.com/pages/Jim-Chim-/26767227562)
  - [詹瑞文 新浪微博](http://www.weibo.com/jimcpip)
  - [詹瑞文 騰訊微博](http://t.qq.com/jimcpip)
  - [詹瑞文 Twitter](http://twitter.com/jimcpip)

## 參考文獻

[S](../Category/詹姓.md "wikilink")
[Category:20世紀男演員](../Category/20世紀男演員.md "wikilink")
[Category:21世紀男演員](../Category/21世紀男演員.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:香港舞臺劇演員](../Category/香港舞臺劇演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:香港棟篤笑演員](../Category/香港棟篤笑演員.md "wikilink")
[Category:香港戲劇導演](../Category/香港戲劇導演.md "wikilink")
[Category:香港電影導演](../Category/香港電影導演.md "wikilink")
[Category:電影演員兼導演](../Category/電影演員兼導演.md "wikilink")
[Category:香港編劇](../Category/香港編劇.md "wikilink")
[Category:香港演藝學院校友](../Category/香港演藝學院校友.md "wikilink")
[Category:宣道中學校友](../Category/宣道中學校友.md "wikilink")

1.  [mystery_shopper_hk\[\#回憶](https://www.instagram.com/p/-N4NuWSmq8/)
    \#微笑企業 \#微笑大使\#盧海鵬 先生,\#詹瑞文 先生,\#張堅庭 先生,\#李力持 先生\]
2.
3.  [詹瑞文扮煲呔傳聖火](http://hk.youtube.com/watch?v=HlQGV_YhtT4)，蘋果動新聞，2008年5月6日
4.  [[孟京辉监制](../Page/孟京辉.md "wikilink")
    詹瑞文作品《桃色办公室》](http://data.ent.sina.com.cn/show/sp31036.shtml)