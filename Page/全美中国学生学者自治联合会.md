**全美中国学生学者自治联合会**（\[1\]），简称为**全美学自联**（），由1989年在[美国的](../Page/美国.md "wikilink")4万多位[中国大陆](../Page/中国大陆.md "wikilink")[留学生通过各校](../Page/留学生.md "wikilink")[中国学生学者联合会推选代表](../Page/中国学生学者联合会.md "wikilink")，于1989年7月底在芝加哥召开的全美中国学生学者第一届代表大会上成立,
以推进[中国民主化和维护留学生利益为目的](../Page/中国民主化.md "wikilink")\[2\]\[3\]。

全美中国学生学者自治联合会是在上世纪90年代存在的代表中国留学生的学生学者团体，为留学生谋求利益\[4\]\[5\]，在当时很有影响力。2000年之后的影响及协调能力逐渐消失，各校[中国学生学者联合会也与全美学自联脱离关系](../Page/中国学生学者联合会.md "wikilink")。

## 历史

全美学自联是第一个民主选举领导人的全国性中国人组织，全美学自联是在[美国国会游会上创造了历史记录的中国人利益集团](../Page/美国国会.md "wikilink")，全美学自联是在[美国首都组织了最大型中国人集会游行的团体](../Page/美国首都.md "wikilink")，全美学自联是最早运用[网际网络于社会动员的非盈利集团之一](../Page/网际网络.md "wikilink")\[6\]。

## 活动

2018年3月13日，[山西大學校園內的](../Page/山西大學.md "wikilink")[「十九大」宣傳欄上出現了反對](../Page/中国共产党第十九次全国代表大会.md "wikilink")[中共總書記](../Page/中国共产党中央委员会总书记.md "wikilink")[習近平](../Page/習近平.md "wikilink")、[修憲](../Page/中华人民共和国宪法修正案_\(2018年\).md "wikilink")[恢復終身制及](../Page/废除干部领导职务终身制#后续.md "wikilink")[中國共產黨的海報](../Page/中國共產黨.md "wikilink")，海報落款爲「全美学自联」及「[中国民主党](../Page/中国民主党.md "wikilink")」。\[7\]此事件被多家海外中文媒體報道，但目前尚未有消息顯示此事確系全美學自聯所爲。

## 參考鏈接

[Category:美国学生组织](../Category/美国学生组织.md "wikilink")
[Category:族群学生组织](../Category/族群学生组织.md "wikilink")
[Category:美国华人组织](../Category/美国华人组织.md "wikilink")
[Category:1989年建立的組織](../Category/1989年建立的組織.md "wikilink")

1.
2.
3.
4.
5.
6.
7.