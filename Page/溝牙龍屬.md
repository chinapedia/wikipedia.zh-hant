**溝牙龍屬**（[學名](../Page/學名.md "wikilink")：*Alocodon*）是[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[中侏羅紀的](../Page/侏羅紀.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")。[正模標本](../Page/正模標本.md "wikilink")（編號LPFU P X 2）只有牙齒化石。有關溝牙龍的研究全是根據牙齒化石而來，所以溝牙龍通常被認為是[疑名](../Page/疑名.md "wikilink")。

溝牙龍的[模式種是](../Page/模式種.md "wikilink")「*Alocodon
kuehnei*」，是在1973年由Richard
Thulborn所敘述、命名的。屬名是由[古希臘文的](../Page/古希臘文.md "wikilink")「*alox*」及「*odon*」組合而成，分別指「溝」及「牙」的意思，故[中文譯名為溝牙龍](../Page/中文.md "wikilink")；種名是以[德國](../Page/德國.md "wikilink")[古生物學家Kühne為名](../Page/古生物學家.md "wikilink")。

溝牙龍有類似[鳥臀目恐龍的](../Page/鳥臀目.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")，牙齒上有垂直的溝。溝牙龍最初被認為是[法布爾龍科](../Page/法布爾龍科.md "wikilink")\[1\]，之後被[彼得·加爾東](../Page/彼得·加爾東.md "wikilink")（Peter
Galton）歸類於[稜齒龍科](../Page/稜齒龍科.md "wikilink")，[保羅·賽里諾](../Page/保羅·賽里諾.md "wikilink")（Paul
Sereno）認為牠們是[鳥臀目中的分類未定屬](../Page/鳥臀目.md "wikilink")。有近年研究認為牠們有可能是屬於[裝甲亞目](../Page/裝甲亞目.md "wikilink")。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [恐龍博物館有關溝牙龍的介紹](https://web.archive.org/web/20070405170955/http://www.dinosaur.net.cn/museum/Alocodon.htm)
  - [溝牙龍](http://www.dinodata.org/index.php?option=com_content&task=view&id=6017&Itemid=67)
    DinoData

[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:鳥臀目](../Category/鳥臀目.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.  Thulborn, R.A. (1975). "Teeth of ornithischian dinosaurs from the
    Upper Jurassic of Portugal, with description of a hypsilophodontid
    (*Phyllodon henkeli* gen. et sp. nov.) from the Guimarota lignite",
    *Contribuição para o conhecimento da Fauna do Kimerridgiano da Mina
    de Lignito Guimarota (Leiria, Portugal)*, Serviços Geológicos de
    Portugal, Memória (Nova Série) **22**: 89-134