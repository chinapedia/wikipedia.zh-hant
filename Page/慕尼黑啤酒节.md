[Oktoberfest_at_night.jpg](https://zh.wikipedia.org/wiki/File:Oktoberfest_at_night.jpg "fig:Oktoberfest_at_night.jpg")
**慕尼黑啤酒節**（又稱「十月節」，）每年九月末到十月初在[德國的](../Page/德國.md "wikilink")[慕尼黑舉行](../Page/慕尼黑.md "wikilink")，持續兩周（大概16天），是慕尼黑一年中最盛大的活動。2002年大約有六百萬人參加了啤酒節，許多其他城市也效法慕尼黑舉辦「十月節」。

啤酒節最初起源於1810年，為慶祝[路德维希王子的婚禮所舉辦](../Page/路德维希一世_\(巴伐利亚\).md "wikilink")，不過現今已經轉為嘉年華了。

## 活動日期

  - 2005年：9月17日--10月3日
  - 2006年：9月16日--10月1日
  - 2007年：9月22日--10月6日
  - 2008年：9月20日--10月5日
  - 2009年：9月19日--10月4日
  - 2010年：9月18日--10月3日（慕尼黑啤酒节200年）
  - 2011年：9月17日--10月3日
  - 2012年：9月22日--10月7日
  - 2013年：9月21日--10月6日\[1\]
  - 2014年：9月20日--10月5日
  - 2015年：9月19日--10月4日
  - 2016年：9月17日--10月3日
  - 2017年：9月16日--10月3日
  - 2018年：9月22日--10月7日

## 其他城市的十月節

  - 德國之外最大的十月節是[加拿大](../Page/加拿大.md "wikilink")[安大略省的Kitchener](../Page/安大略.md "wikilink")-Waterloo，時間是在加拿大的[感恩節左右](../Page/感恩節.md "wikilink")，這個雙子城有不少德裔居民。
  - 另一個比較大的活動在[辛辛那提](../Page/辛辛那提.md "wikilink")，超過五十萬人參加了2002年的辛辛那提十月節。
  - 美國[華盛頓州有一個讓人感覺像到了](../Page/華盛頓州.md "wikilink")[巴伐利亞的小城Leavenworth](../Page/巴伐利亞.md "wikilink"),
    每年十月的頭兩個星期舉行十月節的活動
  - 美國[德克薩斯的New](../Page/德克薩斯.md "wikilink")
    Braunfels也舉行十月節的活動，除此之外還有Mount
    Angel, Oregon、La Crosse, Wisconsin、Lancaster County,
    Pennsylvania和Panama City, Florida、Hays, Kansas以及巴伐利亞小城的複製品Helen,
    Georgia等城市。
  - 在[巴西的Blumenau也舉行盛大的十月節活動](../Page/巴西.md "wikilink")，巴西還有許多德國人建立的城市也有他們自己的十月節，比如Santa
    Cruz do Sul, Rolandia, Sao Jose do Cedro, Seara 和 Itapiranga.
  - 在[阿根廷](../Page/阿根廷.md "wikilink")，一個叫做Villa General Belgrano in the
    Cordoba Province 舉辦全國最大和最著名的十月節活動。
  - [青岛国际啤酒节](../Page/青岛国际啤酒节.md "wikilink")，是亚洲最大的啤酒节，与捷克啤酒节、德国慕尼黑啤酒节、日本札幌啤酒节一同并列为全球四大啤酒节，始于1991年。与德国不同，青岛啤酒节通常在每年8月盛夏的时候举行。
  - [香港從](../Page/香港.md "wikilink")1991年起慶祝十月節，由馬可波羅酒店舉辦。
  - 在[澳門](../Page/澳門.md "wikilink")，由[澳門美高梅舉辦慶祝十月節](../Page/澳門美高梅.md "wikilink")。
  - 越南[胡志明市自](../Page/胡志明市.md "wikilink")1992年起慶祝十月節，地點在赤道飯店（Hotel
    Equatorial）。
  - [中國](../Page/中國.md "wikilink")[北京](../Page/北京.md "wikilink")2013年9月6日至21日在北京奧林匹克公園觀光塔西側舉行首屆「慕尼黑啤酒節—北京之旅」
  - 中國[成都](../Page/成都.md "wikilink")2014年5月22日至6月7日在在成都國際非物質文化遺產博覽園首屆「慕尼黑啤酒節—成都之旅」
  - 馬來西亞[雪蘭莪州](../Page/雪蘭莪州.md "wikilink")[万达广场在](../Page/万达广场_\(雪兰莪\).md "wikilink")2014年舉辦慕尼黑啤酒節時，被[伊斯蘭黨人士投訴宣傳活動違反](../Page/伊党.md "wikilink")[伊斯蘭教的宗教道德](../Page/伊斯蘭教.md "wikilink")。\[2\]随后在2017年啤酒节课题又再马来西亚挑起，引起轩然大波，并导致在[吉隆坡的](../Page/吉隆坡.md "wikilink")2017年绝佳啤酒节\[3\]、雪兰莪州[灵市万达啤酒节](../Page/灵市.md "wikilink")\[4\]和巴生先都广场德国美食派对\[5\]先后被取消。换言之只有槟城和[砂拉越不禁止举办啤酒节](../Page/砂拉越.md "wikilink")\[6\]。

世界許多其他地方也舉辦啤酒節，不過**十月節**這個名字一直屬於慕尼黑的這一活動。

## 參考資料

## 外部連結

  - [慕尼黑啤酒節](http://www.oktoberfest.de/en/index.php)
  - [Hofbrau啤酒節帳篷](http://www.hb-festzelt.de/en/festzelt.html)
  - [照片](https://web.archive.org/web/20050826083425/http://www.travel-impressions.de/oktoberfest/oktoberfest.htm)
  - [加拿大的Kitchener-Waterloo十月節](http://www.oktoberfest.ca/)
  - [巴西十月節](https://web.archive.org/web/20140328184224/http://www.oktoberfest.com.br/)
  - [Oktoberfest in
    Helen](https://web.archive.org/web/20050708092614/http://www.helenga.org/visitorinformation/oktoberfest.asp)
  - [Lancaster
    Liederkranz](http://members.tripod.com/lancasterliederkranz/)
  - [Oktoberfest in Leavenworth
    Washington](http://www.oktoberfestleavenworth.com/)
  - [辛辛那提十月節](https://web.archive.org/web/20050713084949/http://www.oktoberfest-zinzinnati.com/)
  - [從圖片
    编辑“慕尼黑啤酒节](https://web.archive.org/web/20150925075428/http://bilder-hamburg.info/index.php/Sportveranstaltungen/Oktoberfest-Muenchen)

[category:啤酒节](../Page/category:啤酒节.md "wikilink")

[Category:主題日](../Category/主題日.md "wikilink")
[Category:慕尼黑文化](../Category/慕尼黑文化.md "wikilink")
[Category:9月節日](../Category/9月節日.md "wikilink")
[Category:10月份的活動](../Category/10月份的活動.md "wikilink")
[Category:德国文化节](../Category/德国文化节.md "wikilink")
[Category:德国啤酒](../Category/德国啤酒.md "wikilink")
[Category:1810年建立](../Category/1810年建立.md "wikilink")

1.  [今秋在北京过个慕尼黑啤酒节](http://www.apdnews.com/news/30763.html)
    ，亚太日报，2013年7月30日
2.  [雪伊黨：廣泛宣傳違宗教道德‧辦啤酒節欠敏感](http://www.guangming.com.my/node/224740?tid=3)，馬來西亞《光明日報》，2014-10-08。
3.
4.
5.
6.