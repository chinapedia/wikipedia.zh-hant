**慾海肥花**是[香港](../Page/香港.md "wikilink")[90年代初期一間媒體事件](../Page/香港1990年代.md "wikilink")。

## 經過

事件大約發生於[1993至](../Page/1993年.md "wikilink")[1994年間](../Page/1994年.md "wikilink")。當時，[新界有一名被稱為](../Page/新界.md "wikilink")「肥花」的胖婦人**梁秀英**乘搭[的士時故意不付款](../Page/計程車.md "wikilink")，寧願「車債肉償」，與的士司機行房代替繳付車費\[1\]，完事後又訛稱被司機強暴。\[2\]後來梁秀英被控浪費警力並被罰2,000元。\[3\]

1994年11月14日，肥花受[TVB及](../Page/TVB.md "wikilink")[亞洲電視訪問](../Page/亞洲電視.md "wikilink")，向記者表明自己挑逗計程車司機的經歷，又訪問她的未婚夫。後來肥花表示在訪問時有從電視台獲得金錢。

值得一提的是，當肥花接受記者採訪時，曾當眾向TVB娛樂資訊節目[城市追擊主持人](../Page/城市追擊.md "wikilink")[安德尊求婚](../Page/安德尊.md "wikilink")，和在安德尊某影迷聚會時強吻[古天樂](../Page/古天樂.md "wikilink")，一度轟動全港。\[4\]

## 資料來源

[Category:香港傳媒爭議](../Category/香港傳媒爭議.md "wikilink")
[Category:1993年香港](../Category/1993年香港.md "wikilink")
[Category:1994年香港](../Category/1994年香港.md "wikilink")

1.

2.

3.

4.