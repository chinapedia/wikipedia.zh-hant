**-{zh-hans:吕西安·洛朗;
zh-hant:盧遜·羅倫;}-**（****，），[法国](../Page/法国.md "wikilink")[足球运动员](../Page/足球.md "wikilink")，因攻进[世界杯第一個进球而留名历史](../Page/世界杯.md "wikilink")。

洛朗1907年12月10日出生于法国[马恩省](../Page/马恩省.md "wikilink")，1921年至1930年期间，与兄长让·洛朗一同效力于[巴黎的半职业球队](../Page/巴黎.md "wikilink")[CA巴黎俱乐部](../Page/CA巴黎俱乐部.md "wikilink")。1930年，他所工作的[标致汽车公司组建的法国第一支职业球队](../Page/标致.md "wikilink")，[-{zh-hans:索肖;
zh-hant:索察;}-而在整个欧洲范围内四处寻觅优秀的球员加盟](../Page/索肖足球俱乐部.md "wikilink")。时年22岁的洛朗旋即与他们签定了月薪2200[法郎的合同](../Page/法郎.md "wikilink")。

1930年，首届[世界杯在](../Page/1930年世界杯.md "wikilink")[乌拉圭举行](../Page/乌拉圭.md "wikilink")。在7月13日法国队分组赛首战[墨西哥队进行到第](../Page/墨西哥.md "wikilink")13分钟时，洛朗为法国队首开记录，打进了世界杯历史上的第一个进球。这场比赛法国队4比1获胜，洛朗也一战成名。而这仅仅是洛朗代表法国队的第二场比赛，而此时还是业余球员的洛朗，只能从[法国足协获取少的可怜的津贴](../Page/法國足球總會.md "wikilink")。不过，在接下来对[阿根廷和](../Page/阿根廷.md "wikilink")[智利的比赛中](../Page/智利.md "wikilink")，法国队均落败而惨遭淘汰，洛朗本人也在比赛中受伤。

因为伤病的原因，洛朗没有参加[1934年世界杯](../Page/1934年世界杯.md "wikilink")。在这些年中，他曾效力于多家俱乐部，直到1939年，[第二次世界大战爆发](../Page/第二次世界大战.md "wikilink")。

战争爆发后，洛朗被征召入伍并在战争中被[纳粹德国俘虏](../Page/纳粹德国.md "wikilink")。在当了3年的[战俘之后](../Page/战俘.md "wikilink")，洛朗于1943年被释放，并前往[貝桑松競技足球會效力](../Page/貝桑松競技足球會.md "wikilink")。1946年洛朗在贝桑松退役，开始了教练生涯。

2005年4月11日，洛朗与世长辞，享耆壽97岁。洛朗是1930年法国世界杯参赛队员中唯一一个亲眼看到法国队在本土举行的[世界杯折桂的人](../Page/1998年世界杯.md "wikilink")。他一生中共代表法国队出场10次，攻入2球。

## 曾效力俱乐部

  - 1921-1930 : [CA巴黎俱乐部](../Page/CA巴黎俱乐部.md "wikilink")（CA Paris）
  - 1930-1932 : [-{zh-hans:索肖;
    zh-hant:索察;}-](../Page/索肖足球俱乐部.md "wikilink")（FC
    Sochaux）
  - 1932-1933 : [法兰西俱乐部](../Page/法兰西俱乐部.md "wikilink")（Club Français）
  - 1933-1934 : [CA巴黎俱乐部](../Page/CA巴黎俱乐部.md "wikilink")（CA Paris）
  - 1934-1935 : [FC米卢斯俱乐部](../Page/FC米卢斯俱乐部.md "wikilink")（FC Mulhouse）
  - 1935-1936 : [-{zh-hans:索肖;
    zh-hant:索察;}-](../Page/索肖足球俱乐部.md "wikilink")（FC
    Sochaux）
  - 1936-1937 : [雷恩](../Page/雷恩足球俱乐部.md "wikilink")（Stade Rennais）
  - 1937-1939 : [斯特拉斯堡](../Page/斯特拉斯堡足球俱乐部.md "wikilink")（RC Strasbourg）
  - 1939-1943 : [圖盧茲](../Page/图卢兹足球俱乐部.md "wikilink")（Toulouse FC）
  - 1943-1946 : [貝桑松](../Page/貝桑松競技足球會.md "wikilink")（Besançon RC）

## 荣誉

  - 1928年[法國盃](../Page/法國盃.md "wikilink")[亚军](../Page/亚军.md "wikilink")

[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:索察球員](../Category/索察球員.md "wikilink")
[Category:雷恩球員](../Category/雷恩球員.md "wikilink")
[Category:斯特拉斯堡球員](../Category/斯特拉斯堡球員.md "wikilink")
[Category:圖盧茲球員](../Category/圖盧茲球員.md "wikilink")
[Category:瓦勒德馬恩省人](../Category/瓦勒德馬恩省人.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:法國國家足球隊球員](../Category/法國國家足球隊球員.md "wikilink")
[Category:1930年世界盃足球賽球員](../Category/1930年世界盃足球賽球員.md "wikilink")
[Category:1934年世界盃足球賽球員](../Category/1934年世界盃足球賽球員.md "wikilink")