[西固区](../Page/西固区.md "wikilink"){{.w}}[安宁区](../Page/安宁区.md "wikilink"){{.w}}[红古区](../Page/红古区.md "wikilink"){{.w}}[永登县](../Page/永登县.md "wikilink"){{.w}}[皋兰县](../Page/皋兰县.md "wikilink"){{.w}}[榆中县](../Page/榆中县.md "wikilink")

|group3 = [嘉峪关市](../Page/嘉峪关市.md "wikilink") |list3 =
[*无县级行政区*](../Page/:T:嘉峪关市行政区划.md "wikilink")

|group4 = [金昌市](../Page/金昌市.md "wikilink") |list4 =
[金川区](../Page/金川区.md "wikilink"){{.w}}[永昌县](../Page/永昌县.md "wikilink")

|group5 = [白银市](../Page/白银市.md "wikilink") |list5 =
[白银区](../Page/白银区.md "wikilink"){{.w}}[平川区](../Page/平川区.md "wikilink"){{.w}}[靖远县](../Page/靖远县.md "wikilink"){{.w}}[会宁县](../Page/会宁县.md "wikilink"){{.w}}[景泰县](../Page/景泰县.md "wikilink")

|group6 = [天水市](../Page/天水市.md "wikilink") |list6 =
[秦州区](../Page/秦州区.md "wikilink"){{.w}}[麦积区](../Page/麦积区.md "wikilink"){{.w}}[清水县](../Page/清水县.md "wikilink"){{.w}}[秦安县](../Page/秦安县.md "wikilink"){{.w}}[甘谷县](../Page/甘谷县.md "wikilink"){{.w}}[武山县](../Page/武山县.md "wikilink"){{.w}}[张家川回族自治县](../Page/张家川回族自治县.md "wikilink")

|group7 = [武威市](../Page/武威市.md "wikilink") |list7 =
[凉州区](../Page/凉州区.md "wikilink"){{.w}}[民勤县](../Page/民勤县.md "wikilink"){{.w}}[古浪县](../Page/古浪县.md "wikilink"){{.w}}[天祝藏族自治县](../Page/天祝藏族自治县.md "wikilink")

|group8 = [张掖市](../Page/张掖市.md "wikilink") |list8 =
[甘州区](../Page/甘州区.md "wikilink"){{.w}}[民乐县](../Page/民乐县.md "wikilink"){{.w}}[临泽县](../Page/临泽县.md "wikilink"){{.w}}[高台县](../Page/高台县.md "wikilink"){{.w}}[山丹县](../Page/山丹县.md "wikilink"){{.w}}[肃南裕固族自治县](../Page/肃南裕固族自治县.md "wikilink")

|group9 = [平凉市](../Page/平凉市.md "wikilink") |list9 =
[崆峒区](../Page/崆峒区.md "wikilink"){{.w}}[华亭市](../Page/华亭市.md "wikilink"){{.w}}[泾川县](../Page/泾川县.md "wikilink"){{.w}}[灵台县](../Page/灵台县.md "wikilink"){{.w}}[崇信县](../Page/崇信县.md "wikilink"){{.w}}[庄浪县](../Page/庄浪县.md "wikilink"){{.w}}[静宁县](../Page/静宁县.md "wikilink")

|group10 = [酒泉市](../Page/酒泉市.md "wikilink") |list10 =
[肃州区](../Page/肃州区.md "wikilink"){{.w}}[玉门市](../Page/玉门市.md "wikilink"){{.w}}[敦煌市](../Page/敦煌市.md "wikilink"){{.w}}[金塔县](../Page/金塔县.md "wikilink"){{.w}}[瓜州县](../Page/瓜州县.md "wikilink"){{.w}}[肃北蒙古族自治县](../Page/肃北蒙古族自治县.md "wikilink"){{.w}}[阿克塞哈萨克族自治县](../Page/阿克塞哈萨克族自治县.md "wikilink")

|group11 = [庆阳市](../Page/庆阳市.md "wikilink") |list11 =
[西峰区](../Page/西峰区.md "wikilink"){{.w}}[庆城县](../Page/庆城县.md "wikilink"){{.w}}[环县](../Page/环县.md "wikilink"){{.w}}[华池县](../Page/华池县.md "wikilink"){{.w}}[合水县](../Page/合水县.md "wikilink"){{.w}}[正宁县](../Page/正宁县.md "wikilink"){{.w}}[宁县](../Page/宁县.md "wikilink"){{.w}}[镇原县](../Page/镇原县.md "wikilink")

|group12 = [定西市](../Page/定西市.md "wikilink") |list12 =
[安定区](../Page/安定区_\(定西市\).md "wikilink"){{.w}}[通渭县](../Page/通渭县.md "wikilink"){{.w}}[陇西县](../Page/陇西县.md "wikilink"){{.w}}[渭源县](../Page/渭源县.md "wikilink"){{.w}}[临洮县](../Page/临洮县.md "wikilink"){{.w}}[漳县](../Page/漳县.md "wikilink"){{.w}}[岷县](../Page/岷县.md "wikilink")

|group13 = [陇南市](../Page/陇南市.md "wikilink") |list13 =
[武都区](../Page/武都区.md "wikilink"){{.w}}[成县](../Page/成县.md "wikilink"){{.w}}[文县](../Page/文县.md "wikilink"){{.w}}[宕昌县](../Page/宕昌县.md "wikilink"){{.w}}[康县](../Page/康县.md "wikilink"){{.w}}[西和县](../Page/西和县.md "wikilink"){{.w}}[礼县](../Page/礼县.md "wikilink"){{.w}}[两当县](../Page/两当县.md "wikilink"){{.w}}[徽县](../Page/徽县.md "wikilink")
}}

|group3style =text-align: center; |group3 =
[自治州](../Page/自治州.md "wikilink") |list3 =
[临夏县](../Page/临夏县.md "wikilink"){{.w}}[康乐县](../Page/康乐县.md "wikilink"){{.w}}[永靖县](../Page/永靖县.md "wikilink"){{.w}}[广河县](../Page/广河县.md "wikilink"){{.w}}[和政县](../Page/和政县.md "wikilink"){{.w}}[东乡族自治县](../Page/东乡族自治县.md "wikilink"){{.w}}[积石山保安族东乡族撒拉族自治县](../Page/积石山保安族东乡族撒拉族自治县.md "wikilink")

|group15 = [甘南藏族自治州](../Page/甘南藏族自治州.md "wikilink") |list15 =
[合作市](../Page/合作市.md "wikilink"){{.w}}[临潭县](../Page/临潭县.md "wikilink"){{.w}}[卓尼县](../Page/卓尼县.md "wikilink"){{.w}}[舟曲县](../Page/舟曲县.md "wikilink"){{.w}}[迭部县](../Page/迭部县.md "wikilink"){{.w}}[玛曲县](../Page/玛曲县.md "wikilink"){{.w}}[碌曲县](../Page/碌曲县.md "wikilink"){{.w}}[夏河县](../Page/夏河县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below=
注：[嘉峪关市的行政区划是地级市](../Page/嘉峪关市.md "wikilink")、镇（街道）两级编制，没有县级编制。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[甘肃省乡级以上行政区列表](../Page/甘肃省乡级以上行政区列表.md "wikilink")。
}}<noinclude> </noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/甘肃行政区划.md "wikilink")
[甘肃行政区划模板](../Category/甘肃行政区划模板.md "wikilink")