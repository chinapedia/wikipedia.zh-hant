**王國進**（)為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾效力於[中信鯨和](../Page/中信鯨.md "wikilink")[興農牛](../Page/興農牛.md "wikilink")，目前效力於[台北市成棒隊](../Page/台北市成棒隊.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")。

## 經歷

  - [台東縣武陵國小少棒隊](../Page/台東縣.md "wikilink")
  - [台東縣卑南國中青少棒隊](../Page/台東縣.md "wikilink")
  - [台東縣](../Page/台東縣.md "wikilink")[臺東體中青棒隊](../Page/國立臺東大學附屬體育高級中學.md "wikilink")
  - [合作金庫棒球隊](../Page/合作金庫.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[中信鯨隊](../Page/中信鯨.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛隊](../Page/興農牛.md "wikilink")
  - [台北市成棒隊](../Page/台北市成棒隊.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                               | 出賽 | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死 | 三振  | 責失 | 投球局數  | 防禦率   |
| ----- | -------------------------------- | -- | -- | -- | -- | -- | -- | -- | -- | --- | -- | ----- | ----- |
| 2004年 | [中信鯨](../Page/中信鯨.md "wikilink") | 53 | 6  | 6  | 0  | 4  | 0  | 0  | 36 | 125 | 38 | 128.2 | 2.66  |
| 2005年 | [中信鯨](../Page/中信鯨.md "wikilink") | 14 | 1  | 2  | 0  | 0  | 0  | 0  | 11 | 23  | 21 | 53.0  | 3.57  |
| 2006年 | [中信鯨](../Page/中信鯨.md "wikilink") | 4  | 0  | 0  | 0  | 0  | 0  | 0  | 3  | 2   | 6  | 4.2   | 11.57 |
| 2008年 | [興農牛](../Page/興農牛.md "wikilink") | 15 | 0  | 1  | 1  | 0  | 0  | 0  | 6  | 9   | 8  | 19.1  | 3.72  |
| 合計    | 4年                               | 86 | 7  | 9  | 1  | 4  | 0  | 0  | 56 | 159 | 73 | 205.2 | 3.19  |

## 特殊事蹟

## 外部連結

[G](../Category/王姓.md "wikilink") [W](../Category/中信鯨隊球員.md "wikilink")
[W](../Category/興農牛隊球員.md "wikilink")
[W](../Category/台灣棒球選手.md "wikilink")
[Category:國立臺東體育高中校友](../Category/國立臺東體育高中校友.md "wikilink")
[Category:臺東人](../Category/臺東人.md "wikilink")