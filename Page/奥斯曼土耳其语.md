**奥斯曼土耳其语**（），属于[阿尔泰语系](../Page/阿尔泰语系.md "wikilink")[突厥语族](../Page/突厥语族.md "wikilink")，是[奥斯曼帝国的官方语言](../Page/奥斯曼帝国.md "wikilink")。奥斯曼土耳其语中存在大量的[阿拉伯语和](../Page/阿拉伯语.md "wikilink")[波斯语借词](../Page/波斯语.md "wikilink")，流行於[奥斯曼帝国的贵族及上层社会中](../Page/奥斯曼帝国.md "wikilink")。奥斯曼土耳其语的书写系统为是经修改过的[阿拉伯字母](../Page/阿拉伯字母.md "wikilink")，称[奥斯曼土耳其语字母](../Page/奥斯曼土耳其语字母.md "wikilink")。

奥斯曼土耳其语有时也会使用[亚美尼亚字母书写](../Page/亚美尼亚字母.md "wikilink")。

[土耳其共和国建立后](../Page/土耳其共和国.md "wikilink")，1928年，[穆斯塔法·凯末尔·阿塔土克进行了土耳其语言及文字改革](../Page/穆斯塔法·凯末尔·阿塔土克.md "wikilink")。含有大量外语借词的奥斯曼土耳其语被强行废除。[拉丁字母书写的民间白话被使用一直至今](../Page/拉丁字母.md "wikilink")，演变成为如今土耳其人使用的[现代土耳其语](../Page/土耳其语.md "wikilink")。

## 外部链接

  - [密歇根大学的奥斯曼土耳其语资源](http://www.umich.edu/~turkish/langres_ott.html)
  - <http://www.osmanlimedeniyeti.com>
  - [输入奥斯曼字母的网页工具](http://fatihgenc.com/upload/ottomanischklavye.html)

[Category:奥斯曼帝国](../Category/奥斯曼帝国.md "wikilink")
[Category:土耳其语](../Category/土耳其语.md "wikilink")