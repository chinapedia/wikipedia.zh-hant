**歐米加星雲**，也稱為**天鵝星雲**、**核對符號星雲**（Checkmark
nebula）、和**馬蹄星雲**\[1\]\[2\]（在星表上是**梅西爾17**或**M17**），是位於[人馬座的一個](../Page/人馬座.md "wikilink")[電離氫區](../Page/電離氫區.md "wikilink")。它是[Philippe
Loys de
Chéseaux在](../Page/Philippe_Loys_de_Chéseaux.md "wikilink")1745年發現的，[查爾斯·梅西耶在](../Page/查爾斯·梅西耶.md "wikilink")1746年將它收入於目錄中。它坐落在[銀河系內的人馬座](../Page/銀河系.md "wikilink")。

## 特徵

## 相關條目

  - [梅西耶天体](../Page/梅西耶天体.md "wikilink")
  - [梅西耶天体列表](../Page/梅西耶天体列表.md "wikilink")
  - [深空天体](../Page/深空天体.md "wikilink")
  - [人马座](../Page/人马座.md "wikilink")
  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")
  - [NGC天體](../Page/NGC天體.md "wikilink")
  - [科幻小說中的奧米加星雲](../Page/科幻小說中的星雲#奧米加星雲.md "wikilink")

## 參考資料

## 外部連結

  - [Messier 17, SEDS Messier
    pages](http://messier.seds.org/m/m017.html)

  - [Omega Nebula at
    ESA/Hubble](https://web.archive.org/web/20070930030807/http://www.spacetelescope.org/bin/images.pl?embargo=0&viewtype=standard&searchtype=freesearch&lang=en&string=M17)

  -
  - [Omega Nebula (Messier 17) at Constellation
    Guide](http://www.constellation-guide.com/omega-nebula-messier-17/)

[Category:電離氫區](../Category/電離氫區.md "wikilink")
[Category:梅西耶天體](../Category/梅西耶天體.md "wikilink")
[Category:NGC天體](../Category/NGC天體.md "wikilink")
[Category:人馬座](../Category/人馬座.md "wikilink")
[045](../Category/沙普利斯天體.md "wikilink")
[17450615](../Category/1745年發現的天體.md "wikilink")

1.
2.