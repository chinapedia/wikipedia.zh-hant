|        |            |
| ------ | ---------- |
| 口徑     | 105公釐      |
| 砲管長    | 2.364公尺    |
| 砲管重    | 320kg      |
| 砲全長    | 6公尺        |
| 砲全重    | 2260公斤     |
| 砲寬     | 2.146公尺    |
| 砲高     | 1.575公尺    |
| 射角     | \-5度至65度   |
| 射界     | 左右23度      |
| 平衡機    | 彈簧式        |
| 制退復進機  | 液體氣壓式      |
| 正常射擊速度 | 2發／分鐘      |
| 最大射擊速度 | 4發／分鐘      |
| 額定最大膛壓 | 2285公斤／公釐² |
| 最大射程   | 11270公尺    |
| 編制人員   | 8員         |

**63甲式105公釐榴彈砲性能諸元**

**63甲式105公釐榴彈砲**是目前[中華民國陸軍現役火砲](../Page/中華民國陸軍.md "wikilink")，63甲式是自63式改良而來，而63式則是仿美制[M101
105公釐榴彈砲而來](../Page/M101榴彈炮.md "wikilink")。

## 歷史

[M101榴彈炮來自](../Page/M101榴彈炮.md "wikilink")[美國於](../Page/美國.md "wikilink")1919年開始發展，1928年定型「M1
105公釐榴彈砲」。1934年改良為M2，1940年再度進行改良為M2A1
105公釐榴彈砲並且開始量產。[第二次世界大戰結束後](../Page/第二次世界大戰.md "wikilink")，M2A1重新命名為M101。

[中華民國於](../Page/中華民國.md "wikilink")1973年（民國62年）仿製M2A1，也就是M101成功，命名為63式，63式完全參照M2A1砲身、砲架與制退複進系統，可能軍方對其老舊性能不甚滿意，因此沒有量產。

次年8月，M101A1榴彈炮的[藍圖翻譯完成](../Page/藍圖.md "wikilink")，9月進入仿製工作。1974年10月18日完成第一門，19日牽引至大福靶場並且在20日試射12發成功。63甲式則以M2A2砲身、砲架與M2A2A5的制退複進系統，結合改良砲身護盾，以及將機械[千斤頂改成](../Page/千斤頂.md "wikilink")[油壓千斤頂](../Page/油壓.md "wikilink")，全部零組件皆為[中華民國自行生產](../Page/中華民國.md "wikilink")。

63甲式於1977年（民國66年）正式量產，配屬活躍於[步兵師的](../Page/步兵師.md "wikilink")[營級](../Page/營.md "wikilink")[炮兵之中](../Page/炮兵.md "wikilink")。但畢竟是老舊的設計，在[精實案](../Page/精實案.md "wikilink")、[精進案以及砲兵自走化理念的影響下](../Page/精進案.md "wikilink")，63甲式很可能是[中華民國陸軍最後一款](../Page/中華民國陸軍.md "wikilink")105公釐榴彈砲，目前無任何更新計劃。

## 参考文献

### 引用

### 书籍

  - [陳笏](../Page/陳笏.md "wikilink")：《霹靂神火——中華民國火炮傳奇》，[雲皓出版社](../Page/雲皓出版社.md "wikilink")。书号：0258-2341

[Category:臺灣武器](../Category/臺灣武器.md "wikilink")
[Category:榴彈炮](../Category/榴彈炮.md "wikilink")
[Category:中華民國火砲](../Category/中華民國火砲.md "wikilink")
[Category:中華民國陸軍炮兵裝備](../Category/中華民國陸軍炮兵裝備.md "wikilink")