**阿龙·迈克尔·格雷**（，），生于美国[加利福尼亚](../Page/加利福尼亚.md "wikilink")[洛杉矶](../Page/洛杉矶.md "wikilink")，美国职业篮球运动员，司职[中锋](../Page/中锋.md "wikilink")，2007年毕业于[匹兹堡大学](../Page/匹兹堡大学.md "wikilink")。

在2007年3月，格雷当选为[美联社全美大学最佳球员](../Page/美联社.md "wikilink")，之后领衔匹兹堡大学打入了2007年NCAA一级篮球锦标赛的16强（Sweet
16）。

高中时期格雷就读于[宾夕法尼亚的Emmaus高中](../Page/宾夕法尼亚.md "wikilink")，高中属于篮球Lehigh
ValleyGray联盟的球队。

在2007年选秀前训练营中，格雷是唯一一位脱鞋后身高不足7英尺（213公分）的营员。他身高213公分，体重271磅。在[2007年NBA选秀中格雷在第二轮第](../Page/2007年NBA选秀.md "wikilink")49顺位被[芝加哥公牛摘下](../Page/芝加哥公牛.md "wikilink")。

## NCAA时期数据统计

| 年份        | 球队    | 场次  | 命中数 | 出手次数 | 篮板  | 盖帽  | 抢断 |
| --------- | ----- | --- | --- | ---- | --- | --- | -- |
| 2003-2004 | 匹兹堡大学 | 15  | 11  | 20   | 22  | 3   | 0  |
| 2004-2005 | 匹兹堡大学 | 29  | 49  | 85   | 82  | 16  | 5  |
| 2005-2006 | 匹兹堡大学 | 33  | 170 | 323  | 345 | 49  | 21 |
| 2006-2007 | 匹兹堡大学 | 27  | 163 | 281  | 271 | 45  | 14 |
| 总计        |       | 104 | 393 | 724  | 705 | 113 | 40 |

## 参考资料

## 外部链接

  - [Aaron Gray at
    ESPN.com](http://sports.espn.go.com/ncb/player/profile?playerId=15476).
  - [Aaron Gray at NBA
    Draft.net](https://archive.is/20130112043917/http://nbadraft.net/admincp/profiles/aarongray.html).
  - [Aaron Gray at University of Pittsburgh Athletics Web
    Site](http://pittsburghpanthers.cstv.com/sports/m-baskbl/mtt/gray_aaron00.html).
  - [Who Is Pitt's Aaron Gray? at
    Hoopsworld.com](http://www.hoopsworld.com/article_21596.shtml).

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:芝加哥公牛队球员](../Category/芝加哥公牛队球员.md "wikilink")
[Category:新奥尔良黄蜂队球员](../Category/新奥尔良黄蜂队球员.md "wikilink")
[Category:多伦多猛龙队球员](../Category/多伦多猛龙队球员.md "wikilink")
[Category:萨克拉门托国王队球员](../Category/萨克拉门托国王队球员.md "wikilink")