**園藝治療**，或稱園藝療法，是一種輔助治療專業，旨在通過園藝活動，發揮植物的療癒力量，為各類有需要人士帶來身心效益。在園藝治療師的引導之下，服務對象藉由實際接觸和運用[園藝材料](../Page/園藝.md "wikilink")，維護美化[植物或](../Page/植物.md "wikilink")[盆栽和](../Page/盆栽.md "wikilink")[庭園](../Page/庭園.md "wikilink")，從而達至預定的治療目標，例如紓解壓力、復健心靈、促進社交、情緒、小肌肉訓練、認知訓練、專注力恢復、精神健康等等好處。\[1\]

目前園藝療法運用在一般療育和[復健醫學方面](../Page/復健醫學.md "wikilink")，例如精神病院、教養機構、老人和兒童中心、勒戒中心、醫療院所或社區。一般大眾皆可學習此種療法，但必須完成相關課程和累積指定的實習時數，才能取得正式園藝治療師認證執照\[2\]。

## 起源和發展

很多國家和民族自古以來都認為特定的自然環境有益於身心健康\[3\]。[古埃及](../Page/古埃及.md "wikilink")、[古希臘和](../Page/古希臘.md "wikilink")[古羅馬時期](../Page/古羅馬.md "wikilink")，都有[醫生將在](../Page/醫生.md "wikilink")[花園](../Page/花園.md "wikilink")[散步](../Page/散步.md "wikilink")、接觸陽光、新鮮空氣和植物等治療某些心理疾病\[4\]。

18世紀初，[蘇格蘭的](../Page/蘇格蘭.md "wikilink")[戈雷卡迪博士首先對精神病患者施以園藝栽培訓練](../Page/戈雷卡迪.md "wikilink")\[5\]。1792年，[約克收容所讓](../Page/約克收容所.md "wikilink")[精神病患者與](../Page/精神病.md "wikilink")[兔子](../Page/兔子.md "wikilink")、[雞等進行玩耍並從事園藝工作](../Page/雞.md "wikilink")，作為治療的一種方式\[6\]。隨後法國、西班牙的[醫院也相繼採用園藝療法幫助精神病患者康復](../Page/醫院.md "wikilink")\[7\]。
1812年美國[賓州大學精神病學專家](../Page/賓州大學.md "wikilink")[本傑明·拉什發現園藝勞作對](../Page/本傑明·拉什.md "wikilink")[燥症病人有治療效果](../Page/燥症病.md "wikilink")\[8\]。1817年，費城Friend醫院将園藝活動導入治療中\[9\]，在1879年建立了大型[温室](../Page/温室.md "wikilink")，并在其中进行各种園藝療法\[10\]。1940年代－1950年代，美國引進這種療法輔助退役軍人。1973年，美國園藝治療協會成立。

## 操作方法

  - 感知和感受：[觸覺](../Page/觸覺.md "wikilink")、[味覺](../Page/味覺.md "wikilink")、[聽覺](../Page/聽覺.md "wikilink")、[視覺](../Page/視覺.md "wikilink")、[嗅覺](../Page/嗅覺.md "wikilink")。
  - 運動和活動：實際動手照顧植物或做園藝設計的過程。\[11\]

## 註釋

## 參考資料

### 書目

  - 《「綠色療癒力」》，作者：沈瑞琳，2010麥浩斯出版，ISBN 978-986-6322-73-0
  - 《「綠色療癒力」》，作者：沈瑞琳，2013麥浩斯出版，ISBN 978-986-5802-01-1
  - 《園藝治療 -
    種出身心好健康》，作者：[馮婉儀](../Page/馮婉儀.md "wikilink")，2014年明窗出版社出版，ISBN
    978-988-8287-19-2

## 外部链接

  - [American Horticultural Therapy Association
    美國園藝治療學會(AHTA)](http://www.ahta.org/)
  - [Canadian Horticultural Therapy Association
    加拿大園藝治療學會](http://www.chta.ca/)
  - [日本園藝療法研究會](http://www.bekkoame.ne.jp/~takasuna/)
  - [園藝治療專業發展協會htpda](https://www.facebook.com/htpda)
  - [園藝治療專業發展協會Horticultural Therapy Professional Development
    Association](https://www.htpda.com/)
  - [香港園藝治療協會](http://www.hkath.org/)
  - [<心靈綠洲>園藝治療花園](http://www.sereneoasis.org.hk/)
  - [園藝治療證書課程](http://www.sjsmile.com/course/4/)

[Category:特殊教育](../Category/特殊教育.md "wikilink")
[Category:園藝](../Category/園藝.md "wikilink")
[Category:職能治療](../Category/職能治療.md "wikilink")

1.

2.

3.

4.
5.

6.

7.
8.

9.  《治療景觀與園藝療法》作者：郭毓仁，詹氏書局出版，2005年，ISBN 978-957-705-307-7

10.

11. 《走進園藝療法的世界》作者：黃盛璘，心靈工坊文化出版，2007年，ISBN 978-986-7574-98-5