**球蟒**（[學名](../Page/學名.md "wikilink")：**）是[蛇亞目](../Page/蛇亞目.md "wikilink")[蟒科](../Page/蟒科.md "wikilink")[蟒屬下一種分布於](../Page/蟒屬.md "wikilink")[非洲的無毒蟒蛇](../Page/非洲.md "wikilink")。

## 特徵

成\[1\]\[2\]球蟒的表鱗平滑，有兩片肛鱗，雄蛇的肛鱗較大。\[3\]\[4\]

球蟒的體色以[黑色為基調](../Page/黑色.md "wikilink")，背部有許多圓形斑紋。腹部呈[白色或奶油色](../Page/白色.md "wikilink")，有時會出現細碎的黑紋。\[5\]不過一些特殊表現[基因經過寵物商人的](../Page/基因.md "wikilink")[選育](../Page/選育.md "wikilink")，使球蟒產生同種多形的情況，令球蟒的品系（斑紋或體色出現各種變異）數量大增。\[6\]

## 地理分布

球蟒主要分布於[非洲](../Page/非洲.md "wikilink")，包括[塞內加爾](../Page/塞內加爾.md "wikilink")、[馬里共和國](../Page/馬里共和國.md "wikilink")、[幾內亞比紹](../Page/幾內亞比紹.md "wikilink")、[畿內亞](../Page/畿內亞.md "wikilink")、[塞拉利昂](../Page/塞拉利昂.md "wikilink")、[利比里亞](../Page/利比里亞.md "wikilink")、[科特迪瓦](../Page/科特迪瓦.md "wikilink")、[加納](../Page/加納.md "wikilink")、[貝寧](../Page/貝寧.md "wikilink")、[尼日爾](../Page/尼日爾.md "wikilink")、[奈及利亞](../Page/奈及利亞.md "wikilink")、[喀麥隆](../Page/喀麥隆.md "wikilink")、[乍得](../Page/乍得.md "wikilink")、[烏干達](../Page/烏干達.md "wikilink")、[中非共和國和](../Page/中非共和國.md "wikilink")[蘇丹共和國](../Page/蘇丹共和國.md "wikilink")。\[7\]

## 棲息及生態習慣

球蟒多出沒於草地、[熱帶草原及疏林地帶](../Page/熱帶乾濕季氣候.md "wikilink")。\[8\]以陸行為主，當遇到威脅的時候球蟒會將身體緊縮成球體，並將頭頸要害藏在球體中心，在這種狀況下牠們的確能把身體擠成一個完整的圓球。喜歡躲在其它動物所挖掘的洞穴裡，而且有[夏眠的傾向](../Page/夏眠.md "wikilink")。\[9\]

## 進食習慣

野生球蟒會捕食小型的[哺乳動物](../Page/哺乳動物.md "wikilink")，幼蛇則會捕食[鳥類](../Page/鳥類.md "wikilink")。被飼養的球蟒更能接受各種形式的食物（包括活生生的、已殺死的，與及經過雪藏處理的），\[10\]食物的體積應與球蟒體型相等或稍大。球蟒以揀飲擇食著名，若遇不上理想的食物，甚至會絕食數月之久，因此若要飼養球蟒，必須經常留意其體重有否出現銳減的情況，避免讓其絕食以致危害其生命。如果有換棲息地盡量不要一直放它出來因為不適應也會面對絕食的危險\[11\]

## 繁殖

球蟒屬於[卵生動物](../Page/卵生.md "wikilink")，雌蛇每次約能誕下3至11枚蛇卵。\[12\]母蛇會負責保護及孵育蛇卵，孵育期約為90日。\[13\]雄性幼蛇的發育期約為12至18個月，雌蛇則需要24至36個月。幼蛇的年齡考慮及體重考慮，都是決定開始餵食期與及選擇食物種類的重要參考。\[14\]

## 飼養情況

與其它[蟒蛇比較](../Page/蟒屬.md "wikilink")，球蟒的體型較為細小，性情亦較溫和，是相當受歡迎的寵物蛇類。\[15\]初生幼蛇可能較具侵略性，但只要自小讓牠們常與人類接觸便可以有效地減低其戾性。從野外捕獲的球蟒較難照料，由於生活氣候差異等因素的影響，球蟒容易被[寄生生物影響](../Page/寄生.md "wikilink")，危害健康。球蟒壽命大約有20至40歲，目前紀錄中最長壽的球蟒有48年的壽命。\[16\]\[17\]

## 与人类的关系

球蟒與[西非](../Page/西非.md "wikilink")[奈及利亞地區](../Page/奈及利亞.md "wikilink")[伊博族的傳統神話有一定關係](../Page/伊博族.md "wikilink")。當地人認為球蟒近貼大地行動的特性，是[地球的象徵](../Page/地球.md "wikilink")。儘管伊博族中有很多[基督教信徒](../Page/基督教.md "wikilink")，一些誤闖民居的球蟒仍會得到當地人的重視及悉心照顧。球蟒能在伊博族間肆意遊走，若果有球蟒意外死去，伊博族群更會為牠們設置棺木，立墳墓，甚至為其舉行小型喪禮。\[18\]

[PR](../Category/IUCN數據缺乏物種.md "wikilink")
[R](../Category/蟒科.md "wikilink")
[PR](../Category/蛇亞目.md "wikilink")

1.  <http://www.peteducation.com/article.cfm?c=17+1831&aid=2422>

2.  Mehrtens JM. 1987. Living Snakes of the World in Color. New York:
    Sterling Publishers. 480 pp. ISBN 0-8069-6460-X.

3.  Barker DG, Barker TM. 2006. Ball Pythons: The History, Natural
    History, Care and Breeding (Pythons of the World, Volume 2). VPI
    Library. 320 pp. ISBN 0-9785411-0-3.

4.  [Ball
    python](http://www.peteducation.com/article.cfm?cls=17&cat=1831&articleid=2422)
    at [Pet Education](http://www.peteducation.com/).

5.
6.  [(*P. regius*) Base
    Mutations](http://www.grazianireptiles.com/mutations.htm) at
    [Graziani Reptiles](http://www.grazianireptiles.com/)

7.
8.
9.
10.
11.

12.
13.
14.
15. [Ball Pythons, Selection and
    Maintenance](http://groups.msn.com/BallPythons/ballpythons2.msnw) at
    [MSN Groups](http://groups.msn.com/)

16. [Ball python](http://www.newenglandreptile.com/CareBall.html)  at
    [NERD Herpetocultural Library](http://www.newenglandreptile.com/)

17. [Making Responsible Choices When Considering a Reptile as a
    Pet](http://www.alaskazoo.org/reptilehome.htm) at [The Alaska
    Zoo](http://www.alaskazoo.org/)

18. Hambly, Wilfrid Dyson; Laufer, Berthold (1931).["Serpent
    worship"](http://www.archive.org/details/serpentworshipin211hamb).
    Fieldiana Anthropology 21 (1).