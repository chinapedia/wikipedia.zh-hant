**郵政局**是[郵政系統中的基本設施](../Page/郵政.md "wikilink")。

[Post_Office_Oxford_20040124.jpg](https://zh.wikipedia.org/wiki/File:Post_Office_Oxford_20040124.jpg "fig:Post_Office_Oxford_20040124.jpg")[牛津的主要郵政局](../Page/牛津.md "wikilink")\]\]

[Mail_sorting_assembly_line.jpg](https://zh.wikipedia.org/wiki/File:Mail_sorting_assembly_line.jpg "fig:Mail_sorting_assembly_line.jpg")

[眺望上海市邮政局.jpg](https://zh.wikipedia.org/wiki/File:眺望上海市邮政局.jpg "fig:眺望上海市邮政局.jpg")，[上海](../Page/上海.md "wikilink")，中國\]\]

[China_Post_office_in_Shanghai.JPG](https://zh.wikipedia.org/wiki/File:China_Post_office_in_Shanghai.JPG "fig:China_Post_office_in_Shanghai.JPG")

[Environmental_Resources_Centre,_Wanchai,_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Environmental_Resources_Centre,_Wanchai,_Hong_Kong.jpg "fig:Environmental_Resources_Centre,_Wanchai,_Hong_Kong.jpg")\]\]

[Taipei_Post_Office_front_view_20060719.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Post_Office_front_view_20060719.jpg "fig:Taipei_Post_Office_front_view_20060719.jpg")\]\]

## 功能

郵局負責[郵件的投寄](../Page/郵件.md "wikilink")、收件、分類、處理、傳送及交付。郵政局亦會提供其他郵政相關的服務，例如[郵政信箱](../Page/郵政信箱.md "wikilink")、[郵資及包裝供應](../Page/邮政资费.md "wikilink")。另外，一些郵政局亦會提供非郵政服務，如處理[護照及其他政府表格的申請](../Page/護照.md "wikilink")、[匯票](../Page/匯票.md "wikilink")，以及[銀行](../Page/銀行.md "wikilink")、[保險等](../Page/保險.md "wikilink")[金融服務](../Page/金融.md "wikilink")。

對於不想在[家中或](../Page/家.md "wikilink")[辦公室收件的人](../Page/辦公室.md "wikilink")，或居住地點偏僻而無法使用郵政服務的人，郵政局會對他們借出[郵政信箱](../Page/郵政信箱.md "wikilink")（設於郵政局內），令他們可在郵政局中收件。

郵政局的內房會用作處理郵件的分類及交付。郵件亦會被一些不對公眾開放的郵政局（沒有服務櫃檯的郵件處理中心）去處理。

## 郵政概念

  - [郵票](../Page/郵票.md "wikilink")
  - [郵件](../Page/郵件.md "wikilink")
  - [郵差](../Page/郵差.md "wikilink")
  - [郵區編號](../Page/邮政编码.md "wikilink")
  - [免費郵遞](../Page/免費郵遞.md "wikilink")（商業回郵）

## 邮政机构

  - [萬國郵政聯盟](../Page/萬國郵政聯盟.md "wikilink")
  - [英國皇家郵政](../Page/英國皇家郵政.md "wikilink")
  - [法國郵政](../Page/法國郵政.md "wikilink")
  - [美國郵政](../Page/美國郵政.md "wikilink")
  - [德國郵政](../Page/德國郵政.md "wikilink")
  - [韓國郵政](../Page/韓國郵政.md "wikilink")
  - [中華郵政](../Page/中華郵政.md "wikilink")（[臺灣](../Page/臺灣.md "wikilink")）
  - [中国邮政](../Page/中国邮政.md "wikilink")（[中国大陸](../Page/中国大陸.md "wikilink")）
  - [香港郵政](../Page/香港郵政.md "wikilink")（[香港](../Page/香港.md "wikilink")）
  - [澳門郵政](../Page/澳門郵政.md "wikilink")（[澳門](../Page/澳門.md "wikilink")）
  - [日本郵政](../Page/日本郵政.md "wikilink")

## 外部連結

  - [萬國郵政聯盟](http://www.upu.int/)

  - [GRC Database Information](http://www.grcdi.nl/)

  - [世界各地的郵政局的照片](http://www.postmarks.org/photos/)

  - [中華郵政全球資訊網](http://www.post.gov.tw/post/index.jsp)

[地址](../Category/邮政.md "wikilink")