**木衛五十三**（Dia）即S/2000 J
11，是环绕[木星运行的一颗](../Page/木星.md "wikilink")[卫星](../Page/卫星.md "wikilink")。它在2000年被由[斯科特·谢泼德領導的](../Page/斯科特·谢泼德.md "wikilink")[夏威夷大學一個天文小組發現](../Page/夏威夷大學.md "wikilink")。\[1\]\[2\]

## 概述

木衛五十三的直徑約4公里，公轉軌道的平均半徑為12.555
Gm，公轉周期287日，[軌道傾角](../Page/軌道傾角.md "wikilink")28°，[軌道離心率為](../Page/軌道離心率.md "wikilink")0.248。\[3\]

## 參考資料

<div class="references-small">

<references />

</div>

[Category:木星的卫星](../Category/木星的卫星.md "wikilink")

1.  [IAUC 7555: *Satellites of
    Jupiter*](http://cfa-www.harvard.edu/iauc/07500/07555.html)
    2001-01-05 (discovery)
2.  [MPEC 2001-A29: *S/2000 J 7, S/2000 J 8, S/2000 J 9, S/2000 J 10,
    S/2000 J 11*](http://cfa-www.harvard.edu/mpec/K01/K01A29.html)
    2001-01-15 (discovery and ephemeris)
3.  Sheppard, S. S.; Jewitt, D. C.; [Porco,
    C.](../Page/Carolyn_Porco.md "wikilink"); [*Jupiter's outer
    satellites and
    Trojans*](http://www.ifa.hawaii.edu/~jewitt/papers/JUPITER/JSP.2003.pdf)
    , in *Jupiter: The planet, satellites and magnetosphere,* edited by
    Fran Bagenal, Timothy E. Dowling, William B. McKinnon, Cambridge
    Planetary Science, Vol. 1, Cambridge, UK: Cambridge University
    Press, ISBN 0-521-81808-7, 2004, pp. 263-280