[Tsuen_Wan_Magistracy.JPG](https://zh.wikipedia.org/wiki/File:Tsuen_Wan_Magistracy.JPG "fig:Tsuen_Wan_Magistracy.JPG")
**荃灣裁判法院**（）是[香港的一座已關閉的](../Page/香港.md "wikilink")[裁判法院](../Page/裁判法院.md "wikilink")，位處[新界](../Page/新界.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[大河道](../Page/大河道.md "wikilink")70號荃灣法院大樓，樓高3層，鄰近[荃灣大會堂](../Page/荃灣大會堂.md "wikilink")，曾是[荃灣區](../Page/荃灣區.md "wikilink")、[葵青區及](../Page/葵青區.md "wikilink")[離島區的最初級刑事法院](../Page/離島區.md "wikilink")，於1971年啟用。2016年12月遷至[西九龍法院大樓並改名為](../Page/西九龍法院大樓.md "wikilink")[西九龍裁判法院](../Page/西九龍裁判法院.md "wikilink")。

大樓的外圍有[混凝土欄柵](../Page/混凝土.md "wikilink")，正門入口設有鋁閘，而大樓的高層設有和已經被拆卸的[新蒲崗裁判法院相同的窄小的玻璃幕牆](../Page/新蒲崗裁判法院.md "wikilink")。該法院關閉前為[香港唯一沒有電梯的法庭](../Page/香港.md "wikilink")。\[1\]

## 歷史

[香港司法機構曾經打算於](../Page/香港司法機構.md "wikilink")2006年初關閉荃灣裁判法院，與[屯門裁判法院合併](../Page/屯門裁判法院.md "wikilink")，以節省開支，估計每年可以節省500萬港元，而原本由荃灣裁判法院處理的案件，則改由[屯門裁判法院和](../Page/屯門裁判法院.md "wikilink")[沙田裁判法院兩個法院負責處理](../Page/沙田裁判法院.md "wikilink")；但是關閉荃灣裁判法院的計劃最終沒有成事，結果法院仍然運作，其後司法機構最終決定在[西九龍法院大樓竣工後](../Page/西九龍法院大樓.md "wikilink")，荃灣裁判法院、[小額錢債審裁處](../Page/小額錢債審裁處.md "wikilink")、[死因裁判法庭及](../Page/死因裁判法庭.md "wikilink")[淫褻物品審裁處於](../Page/淫褻物品審裁處.md "wikilink")2016年重新設置於西九龍法院大樓之內，西九龍法院大樓及東區法院則處理原荃灣裁判法院的案件\[2\]。法院將於同年12月27日正式關閉，完成45年歷史任務。\[3\]目前，大樓暫用作[沙中線紅磡站偷工減料事件調查委員會的特別法庭](../Page/沙中綫偷工減料醜聞.md "wikilink")，之後將改作社會福利設施。

## 公共交通

<div class="NavFrame collapsed" style="color: black; background: yellow; margin: 0 auto; padding: 0 10px">

<div class="NavHead" style="background: yellow; margin: 0 auto; padding: 0 10px">

**交通路線列表**

</div>

<div class="NavContent" style="background: yellow; margin: 0 auto; padding: 0 10px">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[荃灣西站D出口](../Page/荃灣西站.md "wikilink")
  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[荃灣站A](../Page/荃灣站.md "wikilink")1出口

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 圖集

## 參考來源

<div style="font-size: 85%">

<references />

</div>

## 外部連結

  - [荃灣裁判法院關閉計劃押後](http://archive.news.gov.hk/isd/ebulletin/tc/category/lawandorder/050425/html/050425tc08005.htm)

[Category:香港已停用裁判法院](../Category/香港已停用裁判法院.md "wikilink")
[Category:荃灣](../Category/荃灣.md "wikilink")

1.
2.  [政府倡建西九法院大樓/梁美芬提議選址移海旁](http://www.takungpao.com.hk/news/10/04/21/GW-1246629.htm)
    《大公報》 2010年4月21日
3.