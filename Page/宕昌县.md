**昌县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省](../Page/甘肃省.md "wikilink")[陇南市下属的一个](../Page/陇南市.md "wikilink")[县](../Page/县.md "wikilink")。面积3331平方公里，2004年人口29万。[邮政编码](../Page/邮政编码.md "wikilink")
748500，县政府驻城关镇。

## 历史

宕昌以宕昌山而得名。[晋朝至](../Page/晋朝.md "wikilink")[南北朝時期](../Page/南北朝.md "wikilink")，[羌人在此建](../Page/羌.md "wikilink")“[宕昌国](../Page/宕昌国.md "wikilink")”。[民国二年](../Page/民国.md "wikilink")（1913年），阶州西固分州改为西固县，隶属陇南道（旋改渭川道）。1954年6月，西固县治迁宕昌，11月13日更名为宕昌县。

1935-1936年，[中国工农红军一](../Page/中国工农红军.md "wikilink")、二、四方面军两次经过境内，[毛泽东在此地](../Page/毛泽东.md "wikilink")[哈达铺的会议上正式把此次战略转移称作](../Page/哈达铺会议.md "wikilink")[长征](../Page/长征.md "wikilink")。有哈达铺长征纪念馆。

## 行政区划

下辖11个[镇](../Page/镇.md "wikilink")、13个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")：

。

## 外在链接

[宕昌县政府网站](http://www.tanchang.gov.cn/)

[甘/甘肃](../Page/category:国家级贫困县.md "wikilink")

[宕昌县](../Category/宕昌县.md "wikilink") [县](../Category/陇南区县.md "wikilink")
[陇南](../Category/甘肃省县份.md "wikilink")