**三稜龍目**（Trilophosauria）是一群[雙孔類](../Page/雙孔類.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")，生存在[三疊紀晚期](../Page/三疊紀.md "wikilink")，外表類似[蜥蜴](../Page/蜥蜴.md "wikilink")，跟[主龍類有接近親緣關係](../Page/主龍類.md "wikilink")。最著名的屬是[三稜龍](../Page/三稜龍.md "wikilink")，[草食性](../Page/草食性.md "wikilink")，身長可達2.5公尺，頭骨短而重，上有寬廣平坦的[頰齒](../Page/頰齒.md "wikilink")，表面銳利，可切斷堅硬的植物。[前上頜骨與下頜前部缺乏牙齒](../Page/前上頜骨.md "wikilink")，牠們生前可能有角質覆蓋的喙狀嘴。
[Trilophosaurus_Buettneri.jpg](https://zh.wikipedia.org/wiki/File:Trilophosaurus_Buettneri.jpg "fig:Trilophosaurus_Buettneri.jpg")
三稜龍類頭骨也很不尋常，因為牠們的下[顳顬孔消失了](../Page/顳顬孔.md "wikilink")，使牠們看起來像是[闊孔類的頭骨](../Page/闊孔類.md "wikilink")，所以三稜龍類最初跟[楯齒龍目](../Page/楯齒龍目.md "wikilink")、[鰭龍超目一起被歸類於](../Page/鰭龍超目.md "wikilink")[闊孔亞綱](../Page/闊孔亞綱.md "wikilink")。在1988年，Robert
L. Carroll提出下[顳顬孔消失是為了使頭骨強化](../Page/顳顬孔.md "wikilink")。

到目前為止，三稜龍類的化石發現於[北美與](../Page/北美.md "wikilink")[歐洲的晚](../Page/歐洲.md "wikilink")[三疊紀地層](../Page/三疊紀.md "wikilink")。

## 參考資料

  - Michael J. Benton (2000), *Vertebrate Paleontology*, 2nd ed.
    Blackwell Science Ltd, p.144
  - Robert L. Carroll (1988), *Vertebrate Paleontology and Evolution*,
    W.H. Freeman & Co. p.266

## 外部連結

  - [Archosauromorpha: Rhynchosaurs and
    Trilophosaurus](https://web.archive.org/web/20060313165417/http://www.palaeos.com/Vertebrates/Units/270Archosauromorpha/270.200.html)
    at Palaeos

[\*](../Category/主龍形下綱.md "wikilink")