本列表是[彈幕射擊遊戲](../Page/彈幕射擊遊戲.md "wikilink")[東方Project中登場的角色介紹](../Page/東方Project.md "wikilink")。東方Project角色的特徵是登場角色幾乎都是女性，男性角色的數量十分之少见。

## 本條目的作品名稱

**PC-98平台游戏**

  - 《[東方靈異傳 ～ Highly Responsive to
    Prayers.](../Page/東方靈異傳_～_Highly_Responsive_to_Prayers..md "wikilink")》⇒《靈異傳》
  - 《[東方封魔錄 ～ the Story of Eastern
    Wonderland.](../Page/東方封魔錄_～_the_Story_of_Eastern_Wonderland..md "wikilink")》⇒《封魔錄》
  - 《[東方夢時空 ～ Phantasmagoria of
    Dim.Dream.](../Page/東方夢時空_～_Phantasmagoria_of_Dim.Dream..md "wikilink")》⇒《夢時空》
  - 《[東方幻想鄉 ～ Lotus Land
    Story.](../Page/東方幻想鄉_～_Lotus_Land_Story..md "wikilink")》⇒《幻想鄉》
  - 《[東方怪綺談 ～ Mystic
    Square.](../Page/東方怪綺談_～_Mystic_Square..md "wikilink")》⇒《怪綺談》

**Windows平台游戏**

  - 《[東方紅魔鄉 ～ the Embodiment of Scarlet
    Devil.](../Page/東方紅魔鄉_～_the_Embodiment_of_Scarlet_Devil..md "wikilink")》⇒《紅魔鄉》
  - 《[東方妖妖夢 ～ Perfect Cherry
    Blossom.](../Page/東方妖妖夢_～_Perfect_Cherry_Blossom..md "wikilink")》⇒《妖妖夢》
  - 《[東方萃夢想 ～ Immaterial and Missing
    Power.](../Page/東方萃夢想_～_Immaterial_and_Missing_Power..md "wikilink")》⇒《萃夢想》
  - 《[東方永夜抄 ～ Imperishable
    Night.](../Page/東方永夜抄_～_Imperishable_Night..md "wikilink")》⇒《永夜抄》
  - 《[東方花映塚 ～ Phantasmagoria of Flower
    View.](../Page/東方花映塚_～_Phantasmagoria_of_Flower_View..md "wikilink")》⇒《花映塚》
  - 《[東方文花帖 ～ Shoot the
    Bullet.](../Page/東方文花帖_～_Shoot_the_Bullet..md "wikilink")》⇒《文花帖》
  - 《[東方緋想天 ～ Scarlet Weather
    Rhapsody.](../Page/東方緋想天_～_Scarlet_Weather_Rhapsody..md "wikilink")》⇒《緋想天》
  - 《[東方風神錄 ～ Mountain of
    Faith.](../Page/東方風神錄_～_Mountain_of_Faith..md "wikilink")》⇒《風神錄》
  - 《[東方地靈殿 ～ Subterranean
    Animism.](../Page/東方地靈殿_～_Subterranean_Animism..md "wikilink")》⇒《地靈殿》
  - 《[東方星蓮船 ～ Undefined Fantastic
    Object.](../Page/東方星蓮船_～_Undefined_Fantastic_Object..md "wikilink")》⇒《星蓮船》
  - 《[東方非想天則 ～
    追尋特大型人偶之謎](../Page/東方非想天則_～_追尋特大型人偶之謎.md "wikilink")》⇒《非想天則》
  - 《[Double Spoiler ～
    東方文花帖](../Page/Double_Spoiler_～_東方文花帖.md "wikilink")》⇒《DS文花帖》
  - 《[妖精大戰爭 ～ 東方三月精](../Page/妖精大戰爭_～_東方三月精.md "wikilink")》⇒《妖精大戰爭》
  - 《[東方神靈廟 ～ Ten
    Desires.](../Page/東方神靈廟_～_Ten_Desires..md "wikilink")》⇒《神靈廟》
  - 《[東方心綺樓 ～
    Hopeless_Masquerade.](../Page/東方心綺樓_～_Hopeless_Masquerade..md "wikilink")》⇒《心綺樓》
  - 《[東方輝針城 ～
    Double_Dealing_Character.](../Page/東方輝針城_～_Double_Dealing_Character..md "wikilink")》⇒《輝針城》
  - 《[弹幕天邪鬼 ～ Impossible Spell
    Card](../Page/弹幕天邪鬼_～_Impossible_Spell_Card.md "wikilink")》⇒《天邪鬼》
  - 《[東方深秘錄 ～ Urban Legend in
    Limbo.](../Page/東方深秘錄_～_Urban_Legend_in_Limbo..md "wikilink")》⇒《深秘錄》
  - 《[东方绀珠传 ～ Legacy of Lunatic
    Kingdom.](../Page/东方绀珠传_～_Legacy_of_Lunatic_Kingdom..md "wikilink")》⇒《绀珠传》
  - 《[东方天空璋 ～ Hidden Star in Four
    Seasons.](../Page/东方天空璋_～_Hidden_Star_in_Four_Seasons..md "wikilink")》⇒《天空璋》

**出版物**

  - 《[東方香霖堂 ～ Curiosities of Lotus
    Asia.](../Page/東方香霖堂_～_Curiosities_of_Lotus_Asia..md "wikilink")》⇒《香霖堂》
  - 《[東方文花帖 ～ Bohemian Archive in Japanese
    Red.](../Page/東方文花帖_～_Bohemian_Archive_in_Japanese_Red..md "wikilink")》⇒《文花帖》（書籍）
  - 《[東方紫香花 ～ Seasonal Dream
    Vision.](../Page/東方紫香花_～_Seasonal_Dream_Vision..md "wikilink")》⇒《紫香花》
  - 《[東方求聞史紀 〜 Perfect Memento in Strict
    Sense.](../Page/東方求聞史紀_〜_Perfect_Memento_in_Strict_Sense..md "wikilink")》⇒《求聞史紀》
  - 《[東方三月精 ～ Eastern and Little Nature
    Deity.](../Page/東方三月精.md "wikilink")》⇒《舊三月精》
  - 《[東方三月精 ～ Strange and Bright Nature
    Deity.](../Page/東方三月精.md "wikilink")》⇒《新三月精》
  - 《[東方儚月抄 ～ Silent Sinner in
    Blue.](../Page/東方儚月抄#东方儚月抄_～_Silent_Sinner_in_Blue..md "wikilink")》⇒《儚月抄》（漫畫）
  - 《[東方儚月抄\#東方儚月抄 ～
    月宮的兔子與地上的兔子](../Page/東方儚月抄#東方儚月抄_～_月宮的兔子與地上的兔子.md "wikilink")》⇒《儚月抄》（四格漫畫）
  - 《[東方儚月抄 ～ Cage in Lunatic
    Runagate.](../Page/東方儚月抄#东方儚月抄_～_Cage_in_Lunatic_Runagate..md "wikilink")》⇒《儚月抄》（小說）
  - 《[東方茨歌仙 ～ Wild and Horned
    Hermit.](../Page/東方茨歌仙_～_Wild_and_Horned_Hermit..md "wikilink")》⇒《茨歌仙》
  - 《[東方鈴奈庵 ～ Forbidden
    Scrollery.](../Page/東方鈴奈庵_～_Forbidden_Scrollery..md "wikilink")
    》 ⇒ 《鈴奈庵》

## 主角

### 博麗 靈夢

### 霧雨 魔理沙

## 《靈異傳》中首次登場

  - [神玉](../Page/東方靈異傳_～_Highly_Responsive_to_Prayers.#神玉.md "wikilink")
    第5关头目，为了阻止博丽灵梦通过其守驻的入口，又或者是测试她是否有没有资格解决。起初为一个[太极图的图案](../Page/太极图.md "wikilink")，接着变成一名身着红衣服的女性，最后变成一名端坐的戴着蓝色乌帽的男子。
    官方资料没有其详细的设定介绍，可能其与博丽神社的过去有关联。

<!-- end list -->

  - [幽玄魔眼](../Page/東方靈異傳_～_Highly_Responsive_to_Prayers.#幽玄魔眼.md "wikilink")
    魔界路线第10关头目，由五只巨大浮空的眼睛和中间虚无的人影组成。
    基于幽玄魔眼没有官方设定以及没有和灵梦有对话记录，它可能被视为是普通、没有实际目的的头目。

<!-- end list -->

  - [伊莉斯](../Page/東方靈異傳_～_Highly_Responsive_to_Prayers.#伊莉斯.md "wikilink")
    魔界路线第15关头目，拥有紫色的瞳孔和金色的头发，头上绑着红色的大发带。左手持有带有白色星形顶端的法杖。在白色衬衣外穿着的是带有大红丝带的蓝色背心和镶有紫色边缘的红长裙，拥有紫色的翅膀。能够变身为红眼紫蝙蝠，可能是吸血魔。
    由于伊莉斯没有官方设定并且与灵梦没有发生过任何对话，她可能仅仅是作为敌人的一个普通头目。

<!-- end list -->

  - [萨丽艾尔](../Page/東方靈異傳_～_Highly_Responsive_to_Prayers.#萨丽艾尔.md "wikilink")
    魔界路线第20关头目，如鬼魂一般的天使，有着蓝色及膝长发以及看起来似乎是蓝色的双眼。身着带有白色长袖的简易连衣长裙，身后有三对天使之翼并手持着奇怪的法杖。
    由于萨丽艾尔没有官方设定并且与灵梦没有发生过任何对话，她可能仅仅是作为敌人的一个普通头目。

<!-- end list -->

  - [魅魔](../Page/東方靈異傳_～_Highly_Responsive_to_Prayers.#魅魔.md "wikilink")
    地狱路线第10关头目，也是多次在[PC-98系列作品多次出现的角色](../Page/PC-98.md "wikilink")，是个环绕在博丽神社四周的恶灵。旧作反派头目之一，虽是敌人，但让人恨不起来。个性开朗，有人情味，对自己的能力非常有信心，甚至有些自负，在后作中曾试图抢夺[博丽灵梦的阴阳玉但失败告终](../Page/博丽灵梦.md "wikilink")。
    本作为黑色眼睛，绿色长髮，戴白色帽子。身穿蓝白相间、裙边有红色与绿色装饰的裙子。身披中长碎边蓝色披风，并以红色丝带系于颈部。作为亡灵，下体拥有一条淡蓝色的尾巴而不是脚。拥有两条以上的胳膊，其中一只手中握有滴血的小刀。

<!-- end list -->

  - [菊理](../Page/東方靈異傳_～_Highly_Responsive_to_Prayers.#菊理.md "wikilink")
    地狱路线第15关头目，一个被苍白紫色灵气围绕着的大铜玉，表面上有个长发的年轻女孩，闭着眼睛，脸上有表达着内容的样子，以及一个闪闪发亮的蓝色玉石在她胸前的两个手之间。
    基于菊理没有官方设定并且没有与灵梦有对话记录，她可能被视为是普通，没有实际目的的头目。

<!-- end list -->

  - [矜羯罗](../Page/東方靈異傳_～_Highly_Responsive_to_Prayers.#矜羯罗.md "wikilink")
    地狱路线第20关头目，红眼黑发，在头上有绑着红色缎带并拿着一把大剑。穿着有白色外边的红色法衣和灰色袖子。下半身呈雾状，没有实体。在她头上看似有尖锐的东西嵌入她的额头。
    由于矜羯罗没有设定以及没和灵梦有交谈记录，她可能被认为是一个普通、没有什么目的的敌方头目。

## 《封魔錄》中首次登場

  - [玄爷](../Page/東方封魔錄_～_the_Story_of_Eastern_Wonderland.#玄爺.md "wikilink")
    被博丽灵梦收养的坐骑，长期生活在各种各样的神力之中的龟，拥有在空中自由飞翔的能力。博丽灵梦称呼其为“老爷爷”，而其称灵梦为“主人”。

<!-- end list -->

  - [里香](../Page/東方封魔錄_～_the_Story_of_Eastern_Wonderland.#里香.md "wikilink")
    天才的坦克技师，在第一关头目以驾驶「ふらわ〜戦車」和Extra关卡头目以驾驶「飛行型戦車、イビルアイΣ」出现。

<!-- end list -->

  - [明羅](../Page/東方封魔錄_～_the_Story_of_Eastern_Wonderland.#明罗.md "wikilink")
    女战士，第二关头目，要求挑战灵梦交出博丽之力归其，但输掉后就立即逃离神社。

## 《夢時空》中首次登場

  - [爱莲](../Page/東方夢時空_～_Phantasmagoria_of_Dim.Dream.#爱莲.md "wikilink")
    可选择的自机角色，长不大的魔法使。

<!-- end list -->

  - [小兔姬](../Page/東方夢時空_～_Phantasmagoria_of_Dim.Dream.#小兎姫.md "wikilink")
    可选择的自机角色，一个乔装打扮的警察。

<!-- end list -->

  - [卡娜・安娜贝拉尔](../Page/東方夢時空_～_Phantasmagoria_of_Dim.Dream.#卡娜・安娜贝拉尔.md "wikilink")
    可选择的自机角色，一名骚灵，忧郁，精神不太安定。

<!-- end list -->

  - [朝倉理香子](../Page/東方夢時空_～_Phantasmagoria_of_Dim.Dream.#朝仓_理香子.md "wikilink")
    可选择的自机角色，人类科学家，在以魔法为主的幻想乡中难得一见的科学家。她其实也是魔力很强的魔法师，但讨厌使用魔法，对科学很坚持而认为魔法只是一些魔术把戏。

<!-- end list -->

  - [冈崎梦美](../Page/東方夢時空_～_Phantasmagoria_of_Dim.Dream.#冈崎_梦美.md "wikilink")
    第九关头目，人类大学比较物理学教授，18岁就成为教授的疯狂科学家，为了证明魔法的存在而与助手北白河千百合乘“可能性空间移动船”的时空飞船来到幻想乡收集有关魔法的资料，但得到的成果仍被大学的同僚不认同，最终抛弃了人类大学而再来到幻想乡。在某些情况下，可以变成可选择的自机角色

<!-- end list -->

  - [北白河千百合](../Page/東方夢時空_～_Phantasmagoria_of_Dim.Dream.#北白河_千百合.md "wikilink")
    第八关头目，人类，冈崎梦美的助手，和冈崎梦美一样，15岁就大学毕业，专长物理学，有散落奇怪的东西来引诱有巨大魔法的人来到飞船上。

## 《幻想鄉》中首次登場

  - [橘子](../Page/東方幻想鄉_～_Lotus_Land_Story.#橘子.md "wikilink")
    第1关卡头目，妖怪，因此要被灵梦消灭，而魔理沙消灭其仅因为其挡路……

<!-- end list -->

  - [胡桃](../Page/東方幻想鄉_～_Lotus_Land_Story.#胡桃.md "wikilink")
    第2关卡头目，妖怪，试图守住通往湖中心岛的道路。

<!-- end list -->

  - [艾丽](../Page/東方幻想鄉_～_Lotus_Land_Story.#艾丽.md "wikilink")
    第3关卡头目，妖怪，是幽香住宅「夢幻館」的守門者，看守著夢幻世界和現實世界的邊界，使用一把大镰刀。由于很少人来，所以很惊讶玩家的前来，在败于玩家时说是由于久未战斗的原因。

<!-- end list -->

  - [幽香](../Page/東方幻想鄉_～_Lotus_Land_Story.#幽香.md "wikilink")
    第5，6关头目，妖怪，连接夢幻世界和現實世界的「夢幻館」的主人，擅长使用花和伞子进行攻击。最早栖身于幻想乡的妖怪之一。
    平常生活在花丛中，总是穿着明亮的衣服，外表与人类无异。
    看上去很和蔼，但仍有发怒的时候，尤其擅闯花田者。

<!-- end list -->

  - [夢月](../Page/東方幻想鄉_～_Lotus_Land_Story.#夢月.md "wikilink")
    EXTRA关卡第一名头目，玩家偶然到达某个奇怪世界后遇见的恶魔。

<!-- end list -->

  - [幻月](../Page/東方幻想鄉_～_Lotus_Land_Story.#幻月.md "wikilink")
    EXTRA关卡第二名头目，夢月的姐姐，在夢月被玩家击败后支援妹妹的。

## 《怪綺談》中首次登場

  - [沙罗](../Page/東方怪綺談_～_Mystic_Square.#沙罗.md "wikilink")
    第1关卡头目，魔界的守门者。工作就是阻止主角们进入魔界。

<!-- end list -->

  - [露伊兹](../Page/東方怪綺談_～_Mystic_Square.#露伊兹.md "wikilink")
    第2关卡头目，想去人类世界的恶魔之一，虽然口里说的不是……

<!-- end list -->

  - [爱莉丝](../Page/東方怪綺談_～_Mystic_Square.#爱莉丝.md "wikilink")
    第3关卡和EXTRA关卡头目，擅长使用人偶的魔法使，玩家在魔界的黑暗城市遇见其并与其战斗起来。

<!-- end list -->

  - [雪，舞](../Page/東方怪綺談_～_Mystic_Square.#雪，舞.md "wikilink")
    第4关卡头目，魔法使，等待着玩家的到来，雪比较活泼多话，舞比较沉默。雪的弹幕比舞较为复杂。

<!-- end list -->

  - [梦子](../Page/東方怪綺談_～_Mystic_Square.#梦子.md "wikilink")
    第5关卡头目，神绮的手下，神绮提过其是其手下最强之一。

<!-- end list -->

  - [神绮](../Page/東方怪綺談_～_Mystic_Square.#神綺.md "wikilink")
    第6关卡头目，魔界创造者，也是本次异变的原因——因为允许魔界的恶魔旅行团到人间旅行……

## 《紅魔鄉》中首次登場

  - [露米婭](../Page/露米婭.md "wikilink")（，Rumia）
    第一關中頭目及頭目。不明白自己為何生存著的妖怪。平時身邊會圍繞著一團黑暗，是人之火無法照耀的黑暗（不過連帶自己都看不見身邊的事物了）。頭上的緞帶是一道[符咒](../Page/符咒.md "wikilink")，自己無法解下。
    種族：[妖怪](../Page/妖怪.md "wikilink")

<!-- end list -->

  - [大妖精](../Page/東方紅魔鄉_～_the_Embodiment_of_Scarlet_Devil.#大妖精.md "wikilink")（，Daiyousei）
    第二关中头目，妖精中實力較強的一分子。
    种族：妖精

<!-- end list -->

  - [琪露諾](../Page/琪露諾.md "wikilink")（，Cirno）
    第二關頭目。脾氣像小孩的她輕視著靈夢和魔理沙，並向她們作出挑釁。[ZUN在](../Page/ZUN.md "wikilink")《花映塚》附帶的遊戲說明書中將她稱為⑨（笨蛋），並因此產生了很多基於此的同人創作。
    種族：[雪女](../Page/雪女.md "wikilink")（），但往後的作品将琪露諾的种族设定改为[妖精](../Page/妖精.md "wikilink")。后期的二次创作作品中，也普遍使用琪露諾为妖精的设定。

<!-- end list -->

  - [紅美鈴](../Page/紅美鈴.md "wikilink")（，Hong Meiling）
    第三關中頭目及頭目。是紅魔館的看門人，專門對付紅魔館的入侵者。
    種族：妖怪

<!-- end list -->

  - [小惡魔](../Page/東方紅魔鄉_～_the_Embodiment_of_Scarlet_Devil.#小恶魔.md "wikilink")（，Koakuma）
    第四關中頭目。雖然是魅魔但實力不算強。
    种族：恶魔

<!-- end list -->

  - [帕秋莉·諾雷姬](../Page/帕秋莉·諾雷姬.md "wikilink")／帕秋莉·诺蕾姬（，Patchouli
    Knowledge）
    第四關頭目、Extra中頭目。已有100歲，一直都生活在紅魔館的[圖書館](../Page/圖書館.md "wikilink")。有哮喘的毛病。
    她在本作所使用的符卡會因自機角色及武器種類的不同而不同。
    種族：魔法使

<!-- end list -->

  - [十六夜咲夜](../Page/十六夜咲夜.md "wikilink")（，Izayoi Sakuya）
    第五關中頭目及頭目、以及第六關中頭目。紅魔館的[女僕長](../Page/女僕.md "wikilink")，擅長打掃。能夠無視時間的不可逆性停止時間。專門用能夠對付妖怪或吸血鬼的銀製小刀攻擊，除此以外似乎也會用樸克牌。自稱是16或17歲的少女。
    種族：人類

<!-- end list -->

  - [蕾米莉亞·斯卡蕾特](../Page/蕾米莉亞·斯卡蕾特.md "wikilink")（，Remilia Scarlet）
    第六關頭目。是今次自[符卡規則訂立以來第一次異變的主犯](../Page/符卡.md "wikilink")。生活了500年以上。懼怕陽光，因此放出濃霧遮擋陽光的照射。能夠操縱一切的命運，Spellcard
    神槍「Spear．the．Gungnir」(剛爾瓦尼)，出自[奧丁主神的槍](../Page/奧丁主神.md "wikilink")，擲出後必定命中。
    種族：[吸血鬼](../Page/吸血鬼.md "wikilink")

<!-- end list -->

  - [芙蘭朵露·斯卡蕾特](../Page/芙蘭朵露·斯卡蕾特.md "wikilink")（，Flandre Scarlet）
    Extra頭目。蕾米莉亞的妹妹。生存了495年以上。破壞力極大，因此平時會被禁止出外與人接觸。能夠無視一切，將任何生物的弱點「目」任意操縱、破壞。一個「目」被破壞，意味一個生命消亡。所以蕾米莉亞才把她鎖在紅魔館的地下室，直至紅霧異變被解決，才有一點點有限度的自由和靈夢等人戰鬥。
    種族：吸血鬼

## 《妖妖夢》中首次登場

  - [蕾迪·霍瓦特蘿克](../Page/東方妖妖夢_～_Perfect_Cherry_Blossom.#蕾迪·霍瓦特蘿克.md "wikilink")（，Letty
    Whiterock）
    一面頭目。因為冬天來臨而變得活躍的妖怪。為了阻止靈夢等人打破她喜愛的冬天，因此對她們展開攻擊。
    種族：妖怪

<!-- end list -->

  - [橙](../Page/東方妖妖夢_～_Perfect_Cherry_Blossom.#橙.md "wikilink")（，Chen）
    二面中頭目及頭目、Extra中頭目。憑依在山中居住的[怪貓身上](../Page/怪貓.md "wikilink")。懼怕[水](../Page/水.md "wikilink")，而其力量也是來自憑依的[式神](../Page/式神.md "wikilink")，遇水就會分離，所以水是她的大敵。
    種族：憑依在怪貓身上的式神

<!-- end list -->

  - [爱丽丝·玛格特罗依德](../Page/爱丽丝·玛格特罗依德.md "wikilink")（，Alice Margatroid）
    三面中頭目及頭目。與靈夢一行戰鬥只是因為偶然遇到而想以自己的魔法與她們一決勝負。戰鬥時不想希望壓倒對手，只是盡量以比對手強少許的能力戰鬥。普遍认为，爱丽丝·玛格特罗依德是PC-9800系列作品中登场的人物“爱丽丝”在新作中的接续。另外此角色在本作的Music
    Room和Result的背景中會出現。
    種族：魔法使

<!-- end list -->

  - [莉莉白](../Page/莉莉白.md "wikilink")／莉莉怀特（，Lily White）
    四面中頭目。傳達春天來臨的消息的妖精。當春天來臨時她會表現得極為興奮。而她的興奮是以彈幕表現出來的。今次的出現是因為雲上的範圍已進入春天，所以才會在靈夢等人面前現身。

<!-- end list -->

  - [普利森瑞柏三姐妹](../Page/東方妖妖夢_～_Perfect_Cherry_Blossom.#普利森瑞柏三姊妹.md "wikilink")／普莉兹姆利巴三姊妹（，Prismriver
    Sisters）
    四面頭目。她們是[騷靈](../Page/騷靈.md "wikilink")，所謂引發[騷靈現象的](../Page/騷靈現象.md "wikilink")[東西屋](../Page/東西屋.md "wikilink")。屢次獲招待到西行寺家作表演。在這次也因為要舉行賞花大會，因此亦被邀請到來。
    種族：騷靈
      - [露娜薩·普利森瑞柏](../Page/東方妖妖夢_～_Perfect_Cherry_Blossom.#露娜薩·普利森瑞柏.md "wikilink")／露娜萨·普莉兹姆利巴（，Lunasa
        Prismriver）
        三姐妹的長女。性格陰暗。擅長[弦樂器](../Page/弦樂器.md "wikilink")，特別是[小提琴](../Page/小提琴.md "wikilink")。

      - [梅露蘭·普利森瑞柏](../Page/東方妖妖夢_～_Perfect_Cherry_Blossom.#梅露蘭·普利森瑞柏.md "wikilink")／梅露兰·普莉兹姆利巴（，Merlin
        Prismriver）
        三姐妹的次女。性格開朗。擅長[管樂器](../Page/管樂器.md "wikilink")，特別是[小號](../Page/小號.md "wikilink")。

      - [莉莉卡·普利森瑞柏](../Page/東方妖妖夢_～_Perfect_Cherry_Blossom.#莉莉卡·普利森瑞柏.md "wikilink")／莉莉卡·普莉兹姆利巴（，Lyrica
        Prismriver）
        三姐妹的三女。性格狡猾。擅長任何樂器，常用[鍵盤及敲擊樂器](../Page/鍵盤樂器.md "wikilink")。

      - 除在游戏中登场的三姊妹外，还有一名只在设定文档中提到的角色，名为蕾拉·普莉兹姆利巴。

<!-- end list -->

  - [魂魄妖夢](../Page/魂魄妖夢.md "wikilink")（，Konpaku Youmu）
    五面中頭目及頭目、六面中頭目。西行寺家專屬的[庭師兼劍術指導](../Page/庭師.md "wikilink")。受幽幽子之命收集幻想鄉的春。
    種族：半人半靈

<!-- end list -->

  - [西行寺幽幽子](../Page/西行寺幽幽子.md "wikilink")（，Saigyouji Yuyuko）
    六面頭目。西行寺家的大小姐。在聽說解開妖櫻樹「西行妖」的封印後可使樹下死人復活的傳說後感到興趣，命令妖夢收集幻想鄉的春。但身為亡靈的自己不知道，樹下的屍體就是自己，一旦解開「西行妖」的封印，自己會再度進入輪迴。所以，她看見「西行妖」盛開之日，絕不會到來。
    種族：[亡靈](../Page/亡靈.md "wikilink")

<!-- end list -->

  - [八雲藍](../Page/八雲藍.md "wikilink")（，Yakumo Ran）
    Extra頭目、Phantasm中頭目。憑依在[九尾狐身上](../Page/九尾狐.md "wikilink")。代替著睡眠的主人出外行動。雖然自己是式神，但也擁有屬於自己的式神。本身式神來自數式的結合，所以自身對數學持有非常的見解。是傳說中的狐姬。
    種族：憑依在九尾狐的式神

<!-- end list -->

  - [八雲紫](../Page/八雲紫.md "wikilink")（，Yakumo Yukari）
    Phantasm頭目。可操控境界的隙間妖怪。就寢的時候會把事務交給藍去做。這次也受到了友人幽幽子的委託修復幽冥結界。
    種族：妖怪

## 《萃夢想》中首次登場

  - [伊吹萃香](../Page/伊吹萃香.md "wikilink")（，Ibuki Suika）
    新增角色，本作最终BOSS，也是本次异变源头，由于每次赏花宴会都只持续很短的时间，于是她让人们不停地举行宴会，本人则利用自己的能力化为雾状，享受宴会时的气氛。
    种族：鬼

## 《永夜抄》中首次登場

  - [莉格露·奈特巴格](../Page/東方永夜抄_～_Imperishable_Night.#莉格露·奈特巴格.md "wikilink")（，Wriggle
    Nightbug）
    第一關中頭目及頭目。[螢火蟲妖怪](../Page/螢火蟲.md "wikilink")。看上去很像男生，其實是女孩。害怕[殺蟲劑和寒冷](../Page/殺蟲劑.md "wikilink")。一般她的出現，通常會伴隨著一大堆有毒或組織性強的昆蟲（反而令[殺蟲劑這個弱點更明顯了](../Page/殺蟲劑.md "wikilink")）。
    種族：妖蟲

<!-- end list -->

  - [蜜斯蒂亞·蘿蕾拉](../Page/東方永夜抄_～_Imperishable_Night.#蜜斯蒂亞·蘿蕾萊.md "wikilink")（，Mystia
    Lorelei）
    第二關中頭目及頭目。會以歌聲令夜間走路的人迷路，還會使人得到鳥目（在一定時間內視力減退，而且通常在夜間才會遇上她，說成夜盲也亦無不可）。
    種族：夜雀

<!-- end list -->

  - [上白澤慧音](../Page/上白澤慧音.md "wikilink")（，Kamishirasawa Keine）
    第三關中頭目及頭目、Extra中頭目。平時與人類沒有分別，魔理沙的老師，但一到滿月之夜時就會化為聖獸[白澤](../Page/白澤.md "wikilink")。在人類形態時，可以將歷史消滅;在白澤形態時，則可以創造歷史。第三面時衣服是藍色、戴秀才帽；Extra時衣服是綠色、長出角。
    種族：半獸

<!-- end list -->

  - [因幡帝](../Page/因幡帝.md "wikilink")（， Inaba Tewi（Inaba Tei））
    第五關中頭目。因健康成長而化為妖怪的[兔](../Page/兔.md "wikilink")。在本作中無對話及符卡，只會在使用終句（Last
    Spell）時出現立繪。極端地狡猾，其詐騙技巧在幻想鄉無人能及，再聰明的人或妖怪也會被她那小孩子的面孔而受騙。
    種族：兔精

<!-- end list -->

  - [鈴仙·優曇華院·稻葉](../Page/鈴仙·優曇華院·稻葉.md "wikilink")（或譯作鈴仙·優曇華院·因幡）（，Reisen
    Udongein Inaba）
    第五關頭目。住在月上的兔，眼神可以使人發狂。「優曇華院」是永琳給的稱呼，而「イナバ」則是輝夜對兔的統稱，同時亦是為了避免永遠亭的妖怪兔起疑而使用。
    種族：月兔

<!-- end list -->

  - [八意永琳](../Page/八意永琳.md "wikilink")（，Yagokoro Eirin）
    最終關（Ａ）道中頭目及最終頭目、最終關（Ｂ）道中頭目。制作蓬萊仙藥的天才藥師（其聰明程度被月球上的同伴稱為「月之頭腦」）。雖然實力比輝夜強，但平時都是把自己的實力壓在輝夜之下的。因為不死藥是自己制作的而自己卻無罪，所以對因吃下不死藥而被流放的輝夜感到內疚。
    種族：月球人

<!-- end list -->

  - [蓬萊山輝夜](../Page/蓬萊山輝夜.md "wikilink")（，Houraisan Kaguya）
    最終關（Ｂ）頭目。由於吃下了不死禁藥，被從月球放逐到[地球上](../Page/地球.md "wikilink")，不老不死。她也是知名的輝夜姬（[竹取物語中出謎題考驗他人的公主](../Page/竹取物語.md "wikilink")）。
    種族：蓬萊人

<!-- end list -->

  - [藤原妹紅](../Page/藤原妹紅.md "wikilink")（，Fujiwara no Mokou）
    Extra頭目。過去其父親因向輝夜求婚，被輝夜的謎題羞辱了一番，因此妹紅怨恨輝夜起來，兩人一直互相仇視對方。有不老不死的能力，就算被消滅至剩下一條頭髮也可再生，不過會因其痛楚昏迷幾日。其不老不死的能力，外界猜測可能她服用了蓬萊之藥，才能做到和天人有同等的不老不死能力。每擊破她一張符卡她就會被「消滅」，然後又會回復原狀（大約1到2秒）。
    以為靈夢、魔理沙、咲夜（組）是輝夜派來找碴的而開戰，妖夢（組）則是因為幽幽子認為蓬萊人是被詛咒的身體而開戰（或者只是想嘗試看看不死的亡靈吃下蓬萊人的肝臟會發生什麼事）。
    種族：人類 → 蓬萊人

## 《花映塚》中首次登場

以下5名為《[東方花映塚 ～ Phantasmagoria of Flower
View.](../Page/東方花映塚_～_Phantasmagoria_of_Flower_View..md "wikilink")》中登場的角色，雖然本作中首次登場的角色均是在第7關以後擔當頭目的角色，但只要達到特定條件便能夠使用。再者，射命丸文初登場的作品其實是《[東方文花帖
～ Bohemian Archive in Japanese
Red.](../Page/東方文花帖_～_Bohemian_Archive_in_Japanese_Red..md "wikilink")》，在遊戲中首次登場的則是《花映塚》。

  - [射命丸文](../Page/射命丸文.md "wikilink")（，Shameimaru Aya）
    [鴉天狗](../Page/鴉天狗.md "wikilink")。她是[報章](../Page/報章.md "wikilink")《文文。新聞》的編輯。當百花齊放的景象出現時，她立即就注意到此事的新聞價值。不過更加吸引她注意的卻是幽靈的出現，所以她就集中將新聞焦點放在有出現幽靈附著的花。
    种族：鸦天狗

<!-- end list -->

  - [梅蒂森·梅蘭可莉](../Page/東方花映塚_～_Phantasmagoria_of_Flower_View.#梅蒂森·梅蘭可莉.md "wikilink")（，Medicine
    Melancholy）
    被遺棄在[鈴蘭田](../Page/鈴蘭.md "wikilink")，後來變成妖怪的人偶。雖然她對異變沒有興趣，但如果有甚麼人經過鈴蘭田的話，她會非常樂意地與其一戰的。
    种族：人偶

<!-- end list -->

  - [風見幽香](../Page/風見幽香.md "wikilink")（，Kazami Yuuka）
    棲息在幻想鄉的強大妖怪。她已經作為一名妖怪生存了一段非常長的時間，對此異變的成因瞭如指掌，可是因為她只顧觀賞花，所以成為大家的攻擊對象。
    种族：妖怪

<!-- end list -->

  - [小野塚小町](../Page/小野塚小町.md "wikilink")（，Onozuka Komachi）
    [死神](../Page/死神.md "wikilink")。雖然這次異變不是一個人的責任，但以程度來說，小町的責任是最大的。作為運送幽靈從幻想鄉往彼岸的引路人的她，因為偷懶而沒能及時處理大量湧現的幽靈，引致幽靈附在花朵上，使所有的花都盛開起來。
    种族：死神

<!-- end list -->

  - [四季映姬·亞瑪撒那度](../Page/東方花映塚_～_Phantasmagoria_of_Flower_View.#四季映姬·亞瑪撒那度.md "wikilink")／四季映姬·亚玛萨那度（，Shikieiki
    Yamaxanadu）
    審判死者的[閻魔](../Page/閻魔.md "wikilink")。知道小町無法運送死者來到她面前，因此來到幻想鄉處視察。看見幻想鄉四處開花，並受到人類和妖怪的挑戰後，終於了解到這次異變的事態。每當有人類或妖怪來到她面前，她都會對對方訓話一番。
    种族：阎罗王

## 《風神錄》中首次登場

以下8名為《[東方風神錄 ～ Mountain of
Faith.](../Page/東方風神錄_～_Mountain_of_Faith..md "wikilink")》中首次登場的角色。本作的特徵是所有角色的名字一概不用[片假名](../Page/片假名.md "wikilink")，全部以[平假名和漢字書寫的](../Page/平假名.md "wikilink")（原因是和風故事）。

  - [秋靜葉](../Page/東方風神錄_～_Mountain_of_Faith.#秋_靜葉.md "wikilink")（，Aki
    Shizuha）
    第一關中頭目。穰子之姐，紅葉之神。
    种族：红叶之神

<!-- end list -->

  - [秋穰子](../Page/東方風神錄_～_Mountain_of_Faith.#秋_穰子.md "wikilink")（，Aki
    Minoriko）
    第一關頭目。靜葉之妹，豐穰之神。雖然遊戲中其名稱是寫成「」，但在作品付錄的中卻寫為「」，兩者讀音都相同。和靜葉同為幻想鄉八百萬之神的其中兩神。
    种族：丰穰之神

<!-- end list -->

  - [鍵山雛](../Page/東方風神錄_～_Mountain_of_Faith.#鍵山_雛.md "wikilink")（，Kagiyama
    Hina）
    第二關中頭目及頭目。災厄之神。為了阻止靈夢及魔理沙進入妖怪之山而對兩人展開攻擊。人類觸及她一定會沾染上惡運（但她卻不會有厄運，因為她只是儲存他人的厄運，而非本身的厄運）。
    种族：厄神

<!-- end list -->

  - [河城 似鳥/河城荷取](../Page/河城似鳥.md "wikilink")（，Kawashiro Nitori）
    第三關中頭目及頭目。[河童](../Page/河童.md "wikilink")。警告靈夢一夥不要入侵妖怪之山無效後，對她們展開攻擊，順便測試自己新製成的「[光學迷彩服](../Page/光學迷彩.md "wikilink")」（但只能在躲藏時發動光學迷彩，經測試後，重新考慮其可用性）。在妖怪之山所棲息的河童，基本上其形象都是一樣的，主要是頭戴盤子（帽子），背上有龜殼（背包）。其科技在幻想鄉可謂數一數二，對外界的產物有極重的好奇心，通常會拆開再重組了解其結構。不過對陌生人非常怕生，一般看到一眼都會很快地逃走了。
    种族：[河童](../Page/河童.md "wikilink")

<!-- end list -->

  - [犬走椛](../Page/東方風神錄_～_Mountain_of_Faith.#犬走_椛.md "wikilink")（，Inubashiri
    Momiji）
    第四關中頭目。白狼天狗，為[射命丸文的後輩](../Page/射命丸文.md "wikilink")。向入侵妖怪之山的靈夢一行展開威嚇攻擊。其後聽從大天狗的命令，暗中監視兩人。
    种族：白狼天狗

<!-- end list -->

  - [東風谷早苗](../Page/東風谷早苗.md "wikilink")（，Kochiya Sanae）
    第五關中頭目及頭目。守矢神社的風祝（），向博麗神社提出停業警告，但後來被靈夢打敗了。其實她是作為現人神、EXTRA頭目洩矢
    諏訪子的子孫的存在。其實其喚來神風之力是來自諏訪子的後代的力量，只是她本人並沒知情罷了。
    种族：人类

<!-- end list -->

  - [八坂神奈子](../Page/八坂神奈子.md "wikilink")（，Yasaka Kanako）
    第六關（最終關）頭目、Extra中頭目。早苗所祭祀的山神。當初私自把神社移到妖怪之山，引起山上天狗們的警戒，後來和解。形象為蛇，守矢神社中兩柱神的其中一柱神。
    种族：神明
    其收納博麗神社的念頭是因為她自覺只靠妖怪間的信仰並不足夠，于是就想到利用山下的神社－博麗神社作為分社，收集人類對她和諏訪子的信仰。

<!-- end list -->

  - [洩矢諏訪子](../Page/洩矢諏訪子.md "wikilink")（，Moriya Suwako）
    Extra頭目。守矢神社所祭祝的以青蛙為形象、兩柱神的其中一柱神。與到訪神社本殿的靈夢和魔理沙進行了名為「神的遊戲」的彈幕戰。其實要追溯到守矢神社的源頭的話，諏訪子才是真正在守矢神社所供奉的神明。
    种族：神明

## 《緋想天》中首次登場

  - [永江衣玖](../Page/永江衣玖.md "wikilink")（，Nagae Iku）
    龍宮的使者，引領登上山上的人前往天界。在衣玖的故事模式中，衣玖因為感受到地震的發生，而出發把這消息告知幻想鄉的所有人。
    種族：妖怪（龍宮的使者）

<!-- end list -->

  - [比那名居天子](../Page/東方緋想天_～_Scarlet_Weather_Rhapsody.#比那名居_天子.md "wikilink")（，Hinanawi
    Tenshi）
    天人，在沉悶的時候想著「引起一個異變讓人類來戰鬥吧」，於是收集氣質，在博麗神社處引起地震。
    種族：後天的天人

## 《地靈殿》中首次登場

  - [琪斯美](../Page/東方地靈殿_～_Subterranean_Animism.#琪斯美.md "wikilink")（，Kisume）
    第一關中頭目。
    种族：[钓瓶妖](../Page/钓瓶妖.md "wikilink")（）

<!-- end list -->

  - [黑谷山女](../Page/東方地靈殿_～_Subterranean_Animism.#黑谷_山女.md "wikilink")（，Kurodani
    Yamame）
    第一關頭目。能操縱疾病程度的能力。
    种族：[土蜘蛛](../Page/土蜘蛛.md "wikilink")

<!-- end list -->

  - [水橋帕露希](../Page/東方地靈殿_～_Subterranean_Animism.#水橋_帕露希.md "wikilink")（，Mizuhashi
    Parsee）
    第二關中頭目及頭目。種族為[橋姬](../Page/橋姬.md "wikilink")。因嫉妒心而對前來地底的靈夢們攻擊。
    种族：桥姬

<!-- end list -->

  - [星熊勇儀](../Page/東方地靈殿_～_Subterranean_Animism.#星熊_勇儀.md "wikilink")（，Hoshiguma
    Yuugi）
    第三關中頭目及頭目。與萃香一樣被稱為鬼族四天王的其中一人。鬼族四天王「怪」、「力」、「乱」、「神」中「力」的代表。為了測試到來的人類的實力，與靈夢等人展開了戰鬥。
    种族：鬼

<!-- end list -->

  - [古明地覺](../Page/古明地覺.md "wikilink")（，Komeiji Satori）
    第四關頭目。是灼熱地獄遺址上的地靈殿的主人。種族為[覺](../Page/覺.md "wikilink")。對靈夢等人談到的「怨靈」和「噴泉」感到疑問和奇怪，於是與她們對決。戰敗後就引領了她們進入灼熱地獄遺址（地靈殿中庭）。比較特別的是，因為她會讀心術，因此面對不同選擇的角色和妖怪會使出不同的符卡攻擊。
    种族：觉

<!-- end list -->

  - [火焰貓燐](../Page/東方地靈殿_～_Subterranean_Animism.#火焰貓_燐.md "wikilink")（）別名：阿燐（，Kaenbyou
    Rin）
    第五關頭目、第六關中頭目。種族為火車、[猫又](../Page/猫又.md "wikilink")。在第四關和第五關以貓的形態作為中頭目出現，並以人形作為第五關頭目及第六關中頭目。作為覺的寵物，同時被主人委任管理怨靈。為了解決由親友空引發的異變，於是放出怨靈到地上，向妖怪們求援。（東方系列歷代以來，在同一款遊戲中登場次數最多的BOSS。）
    种族：火车、[猫又](../Page/猫又.md "wikilink")

<!-- end list -->

  - [靈烏路空](../Page/靈烏路空.md "wikilink")（）別名：阿空（，Reiuji Utsuho）
    第六關頭目。她是覺的寵物，也被主人委任調節灼熱地獄遺址的火力。原本只是一只地獄鴉，後來從山神那裡獲得了八咫烏的力量，姿態改變，力量也大為增強。由於使用了增長的力量，使得灼熱地獄的熱力上升，過多的力量變為間歇泉從地上噴出。
    种族：地狱鸦→八咫乌

<!-- end list -->

  - [古明地戀](../Page/古明地戀.md "wikilink")（，Komeiji Koishi）
    Extra頭目。覺的妹妹。由於空的力量激增，戀也想使自己的寵物的力量上升，於是來到了守矢神社。從靈夢那裡聽說覺的事後，就與她們展開了戰鬥。有著操縱潛意識的能力。
    种族：觉

## 《星蓮船》中首次登場

  - [娜茲玲](../Page/東方星蓮船_～_Undefined_Fantastic_Object.#娜茲琳.md "wikilink")／娜茲琳（，Nazrin）
    一面中頭目，頭目及五面中頭目，能力是要搜尋東西就能搜尋到的能力。操縱著無數的[老鼠](../Page/老鼠.md "wikilink")，使喚牠們找尋自己要找的東西。雖是五面頭目寅丸
    星最信任的心腹，實際上是毘沙門天派來暗中監視星的妖怪。
    種族：妖怪（妖怪鼠）

<!-- end list -->

  - [多多良小傘](../Page/多多良小傘.md "wikilink")（，，Tatara Kogasa）
    二面中頭目及頭目，能力是令人類驚慌的能力（但從來沒有人類被嚇到），是由長年不被使用的[傘化成的妖怪](../Page/傘.md "wikilink")。
    種族：[附喪神](../Page/附喪神.md "wikilink")（[唐傘妖怪](../Page/唐傘小僧.md "wikilink")）

<!-- end list -->

  - [雲居一輪](../Page/雲居一輪.md "wikilink")（，Kumoi Ichirin）
    三面中頭目及頭目，一輪的能力是使喚入道的能力，好像有著甚麼秘密而守著寶船不讓人進去似的。實際上，她和**雲山**是為了誘使得到飛船的碎片的靈夢等人們進入寶船，從而令寶船能夠前向魔界。
    種族：妖怪

<!-- end list -->

  - [雲山](../Page/東方星蓮船_～_Undefined_Fantastic_Object.#雲山.md "wikilink")（，Unzan）
    雖是性情頑固的入道，但它卻對一輪忠心不二。絕不會說謊，雲山的能力則是能自在地改變形態和大小的能力。和雲居 一輪组合为三面中頭目及頭目。
    種族：[入道](../Page/入道.md "wikilink")

<!-- end list -->

  - [村紗水蜜](../Page/東方星蓮船_～_Undefined_Fantastic_Object.#村紗_水蜜.md "wikilink")（，Murasa
    Minamitsu）
    四面頭目，能力是引起水難事故的能力，是很久以前在海難事故中死了的人的靈魂化成的念縛靈。水蜜是原名，而村紗是代表該海域引發水難事故的妖怪，在水蜜死後成為念縛靈後，因其怨念過深，所以水蜜取代了其地位，成為現今的村紗水蜜。
    種族：舟幽霊

<!-- end list -->

  - [寅丸星](../Page/東方星蓮船_～_Undefined_Fantastic_Object.#寅丸_星.md "wikilink")（，Toramaru
    Shou）
    五面頭目，能力是聚集財寶的能力，吉利的妖怪，是[毗沙門天王](../Page/毗沙門天王.md "wikilink")（）半默認的弟子。可是毗沙門天王並不太放心，所以派出娜茲玲監視其一舉一動。
    種族：妖怪

<!-- end list -->

  - [聖白蓮](../Page/東方星蓮船_～_Undefined_Fantastic_Object.#聖_白蓮.md "wikilink")（，Hijiri
    Byakuren）
    六面頭目，能力是使用魔法的能力，擅長提升體能的魔法。在很久以前曾接受委托去退治妖怪，暗地裡卻在幫助妖怪，因此與人們的期待之間產生了間隙，而被封印在魔界。後來希望幫助白蓮的妖怪計畫成功，利用靈夢等人的飛船碎片促使白蓮的在法界的封印被破解。
    種族：魔法使

<!-- end list -->

  - [封獸鵺](../Page/東方星蓮船_～_Undefined_Fantastic_Object.#封獸_鵺.md "wikilink")（，Houjuu
    Nue）
    Extra頭目，四面與六面道中時出現的光球真面目，擁有讓人無法判別事物真相的能力，由於她的把戲讓靈夢等人將飛船的碎片誤認為是飛碟。曾暗中搗亂妖怪們復活白蓮，不過事後自己卻也感到後悔。原作者對鵺的創作意念來自日本民間傳說「平家物語」中的妖獸「鵺」，其符卡「源三位賴政之弓」來自鵺的傳說的結局，鵺被[源賴政用弓所殺](../Page/源賴政.md "wikilink")，因而得名。
    種族：[鵺](../Page/鵺.md "wikilink")

## 《DS文花帖》中首次登場

  - [姬海棠果](../Page/姬海棠果.md "wikilink")／姬海棠羽立／姬海棠極（，Himekaidou Hatate）
    本作新角色，新特定条件可用自机，同文一样为天狗的摄影记者，原有自己的报章《花果子念报》缺少读众，于是跟踪文的采访，并发现文的采访秘密。其使用的相机没远程拍摄，但胶片装填速度较文的快，而且摄像框更宽。
    種族：鴉天狗

## 《神靈廟》中首次登場

  - [幽谷響子](../Page/東方神靈廟_～_Ten_Desires.#幽谷響子.md "wikilink")（，Kasodani
    Kyouko）
    第二關頭目，山彥妖怪。能夠製造回音。命蓮寺的妖怪僧侶，反對殺生。對靈夢攻擊的理由是因為襲擊人類算是妖怪僧的修行之一；妖夢的理由則是她以為她的樓觀劍是拿來殺生的。
    種族：山彥

<!-- end list -->

  - [宮古芳香](../Page/東方神靈廟_～_Ten_Desires.#宮古芳香.md "wikilink")（，Miyako
    Yoshika）
    第三關頭目、第四關輔助頭目，僵屍（道教）。守護神靈廟不受命蓮寺的僧人攻擊，施術者是青娥，用頭上的符咒來操控。會食去四周的靈體回復體力，被它的毒爪或毒牙咬到會變成僵屍，力氣也是數一數二的大。就算打死也會復活的不死妖怪。
    種族：僵屍

<!-- end list -->

  - [霍青娥](../Page/東方神靈廟_～_Ten_Desires.#霍青娥.md "wikilink")（，Kaku Seiga）
    第四關頭目。仙人，穿牆邪仙。阻止靈夢進入大祀廟，喜歡操縱屍體。從遠東來到日本傳播道教的仙人，但因為祂的成仙過程不合乎倫道，加上為追求力量做了許多偷雞摸狗、見不得光的事，因此被眾仙貶為邪仙。
    名字來自[聊齋誌異青娥裡的男主角](../Page/聊齋誌異.md "wikilink")**霍桓**和女主角**青娥**，穿牆能力則是出自同本的勞山道士中道士教給王生的穿牆仙術。
    種族：邪仙

<!-- end list -->

  - [蘇我屠自古](../Page/東方神靈廟_～_Ten_Desires.#蘇我屠自古.md "wikilink")（，Soga no
    Tojiko）
    第五關中頭目。豪族亡靈，沒有腳。以歷史的角度來說，蘇我氏和物部氏是勢不兩立的冤家，但在這裡她們的關係沒那麼糟，她們兩個還一起愉快地出現在神子的Spell
    Card中（召喚「豪族亂舞」）。比起人類祂更喜歡做為一個亡靈，因為祂不喜歡會隨時間凋零的肉體。蘇我一族的祂現在卻和布都一樣排斥佛教。
    種族：亡霊

<!-- end list -->

  - [物部布都](../Page/東方神靈廟_～_Ten_Desires.#物部布都.md "wikilink")（，Mononobe no
    Futo）
    第五關頭目。種族不明，似乎是「人」，但已經死過一次了。大祀廟的道士，被神子作為屍解術的實驗品，相信神子的屍解術會成功，自願為神子「死亡」一次，最後如期復活。個性自我加天然，誤以為自機們的到來是為了祝賀自己的復活，而得知神子復活後又放著靈夢等自機不管自行跑掉，結果把麻煩引進來了。
    種族：（不明）

<!-- end list -->

  - [豐聰耳神子](../Page/東方神靈廟_～_Ten_Desires.#豐聰耳神子.md "wikilink")（，Toyosatomimi
    no Miko）
    第六（最終）關頭目。[聖人](../Page/聖德太子.md "wikilink")，每天都得聽一堆願望，但能夠解讀出人類的「十欲」（像是妖夢只有八欲，少了生欲和死欲），同時也是一位死而復生的屍解仙。神子是位德高望重、為人指點迷津的「聖人」。非常不满命蓮寺直接壓在大祀廟正上方，但大量神靈的出沒也是因為祂即將復活的徵兆。
    種族：聖人

<!-- end list -->

  - [二岩貒藏](../Page/東方神靈廟_～_Ten_Desires.#二岩貒藏.md "wikilink")（，Futatsuiwa
    Mamizou ）
    Extra頭目。貍貓妖怪，擅長變身術、分身術、以及操弄動物造型的式神犬。東方系列win作第一位眼鏡娘。腰上繫著一個酒甕，似乎也是個酒鬼。混在人群當中與人類和平相處生活，為什麼能進出幻想鄉不得而知。本來是因為聖人復活會對妖怪造成極大影響，從幻想鄉外要來一起幫忙阻止，卻碰上靈夢一行人，以為靈夢來阻撓她，對靈夢一行人發動了攻擊。
    種族：妖獸（狸貓）

## 《心綺樓》中首次登場

  - [秦心](../Page/東方心綺樓_～_Hopeless_Masquerade..md "wikilink")（，Hata no
    Kokoro）
    由面具所转化而成的付丧神，持有66个代表不同情绪的面具，通过这些面具可以表达出不同的感情，最終頭目，同時也是可使用的隱藏角色。
    種族：面靈氣

## 《輝針城》中首次登場

  - [若鷺姬](../Page/東方輝針城_～_Double_Dealing_Character.#わかさぎ姫.md "wikilink")（，Wakasagihime）
    第一關頭目，棲息於淡水的人魚。
    種族：[人魚](../Page/人魚.md "wikilink")

<!-- end list -->

  - [赤蠻奇](../Page/東方輝針城_～_Double_Dealing_Character.#赤蠻奇.md "wikilink")（，Sekibanki）
    第二關頭目兼中頭目，轆轤首的怪奇，即使破壞頭的部份仍會再生，身體仍舊才是本體。
    種族：[轆轤首](../Page/轆轤首.md "wikilink")

<!-- end list -->

  - [今泉影狼](../Page/東方輝針城_～_Double_Dealing_Character.#今泉影狼.md "wikilink")（，Imaizumi
    Kagerou）
    第三關頭目兼中頭目，出沒於竹林中的狼女。
    種族：[狼女](../Page/狼人.md "wikilink")

<!-- end list -->

  - [九十九弁弁](../Page/東方輝針城_～_Double_Dealing_Character.#九十九弁弁.md "wikilink")（，Tsukumo
    Benben）
    第四關頭目（使用妖器）兼中頭目（不使用妖器），Extra中頭目，古琵琶的付喪神。
    種族：[付喪神](../Page/付喪神.md "wikilink")

<!-- end list -->

  - [九十九八橋](../Page/東方輝針城_～_Double_Dealing_Character.#九十九八橋.md "wikilink")（，Tsukumo
    Yatsuhashi）
    第四關頭目（不使用妖器）兼中頭目（使用妖器），Extra中頭目，古琴的付喪神。
    種族：[付喪神](../Page/付喪神.md "wikilink")

<!-- end list -->

  - [鬼人正邪](../Page/東方輝針城_～_Double_Dealing_Character.#鬼人正邪.md "wikilink")（，Kijin
    Seija）
    第五關頭目兼中頭目，第六（最終）關中頭目，逆襲的天邪鬼，是《輝針城》中異變的幕後黑手。
    種族：[天邪鬼](../Page/天邪鬼.md "wikilink")

<!-- end list -->

  - [少名針妙丸](../Page/東方輝針城_～_Double_Dealing_Character.#少名針妙丸.md "wikilink")（，
    Sukuna Shinmyoumaru）
    第六（最終）關頭目，小人的末裔，拿著針與槌子，原型為[一寸法師](../Page/一寸法師.md "wikilink")。
    種族：[一寸法師](../Page/一寸法師.md "wikilink")

<!-- end list -->

  - [崛川雷鼓](../Page/東方輝針城_～_Double_Dealing_Character.#崛川雷鼓.md "wikilink")（，Horikawa
    Raiko）
    Extra頭目，夢幻的打擊樂手，太鼓付喪神。
    種族：[付喪神](../Page/付喪神.md "wikilink")

## 《深秘錄》中首次登場

  - [宇佐见堇子](../Page/东方深秘录_～_Urban_Legend_in_Limbo.#宇佐见堇子.md "wikilink")（，Usami
    Sumireko）
    外界相信拥有超能力的女高中生，由于相信超能力等灵异事件而想借都市传说来进入幻想乡并试图将其与外界连接起来，最后被阻止。并且觉醒了通过梦境进入幻想乡的能力。
    种族：女高中生

## 《紺珠傳》中首次登場

  - [清蘭](../Page/東方紺珠傳_～_Legacy_of_Lunatic_Kingdom.#清蘭.md "wikilink")（，Seiran）
    第一關頭目兼中頭目，地面調查部隊「伊格爾拉比」滲透組成員。
    種族：月兔

<!-- end list -->

  - [鈴瑚](../Page/東方紺珠傳_～_Legacy_of_Lunatic_Kingdom.#鈴瑚.md "wikilink")（，Ringo）
    第二關頭目兼中頭目，地面調查部隊「伊格爾拉比」情報組成員。
    種族：月兔

<!-- end list -->

  - [哆来咪·苏伊特](../Page/東方紺珠傳_～_Legacy_of_Lunatic_Kingdom.#哆来咪·苏伊特.md "wikilink")（，Doremy
    Sweet）
    第三關頭目兼中頭目，Extra中頭目，吞食夢境的夢貘。
    種族：貘

<!-- end list -->

  - [稀神探女](../Page/東方紺珠傳_～_Legacy_of_Lunatic_Kingdom.#稀神_探女.md "wikilink")（，Kishin
    Sagume）
    第四關頭目兼中頭目，月之賢者，起始「[深秘錄](../Page/東方深秘錄_～_Urban_Legend_in_Limbo..md "wikilink")」都市传说异变的元兇，因爲自己的能力而不太愛說話。
    種族：月人

<!-- end list -->

  - [克勞恩皮絲](../Page/東方紺珠傳_～_Legacy_of_Lunatic_Kingdom.#克勞恩皮絲.md "wikilink")（，Clownpiece）
    第五關頭目，住在地獄的妖精。紺珠異變結束後，「在某位朋友的幫助下」入住幻想鄉，並與「[三月精](../Page/東方三月精.md "wikilink")」結識。
    種族：妖精

<!-- end list -->

  - [純狐](../Page/東方紺珠傳_～_Legacy_of_Lunatic_Kingdom.#純狐.md "wikilink")（，Junko）
    第六（最終）關頭目，无名的存在，對嫦娥懷有強烈恨意的神靈。
    種族：神靈

<!-- end list -->

  - [赫卡提亚·拉碧斯拉祖利](../Page/東方紺珠傳_～_Legacy_of_Lunatic_Kingdom.#赫卡提亚·拉碧斯拉祖利.md "wikilink")（，Hecatia
    Lapislazuli）
    Extra頭目，掌管月球、地球、異界三處地獄的女神，與純狐同樣對嫦娥懷有恨意。
    種族：神

## 《憑依華》中首次登場

  - [依神女苑](../Page/東方憑依華_～_Antinomy_of_Common_Flowers.#依神_女苑.md "wikilink")（，Yorigami
    Jyoon）
    这次乘着都市传说异变的余波，发起了完全凭依异变，从参加堀立兹姆利巴乐团的客人身上大量夺取财物。最凶最恶的双子之妹。
    种族：疫病神

<!-- end list -->

  - [依神紫苑](../Page/東方憑依華_～_Antinomy_of_Common_Flowers.#依神_紫苑.md "wikilink")（，Yorigami
    Shion）
    配合着妹妹依神女苑的计画行动，靠着完全凭依，从参加堀立兹姆利巴乐团的客人身上大量夺取财物。最凶最恶的双子之姐。
    种族：贫穷神

## 《天空璋》中首次登場

  - [爱塔妮缇·拉尔瓦](../Page/東方天空璋_～_Hidden_Star_in_Four_Seasons.#爱塔妮缇·拉尔瓦.md "wikilink")（，Etarnity
    Larva）
    第一關頭目兼中頭目，接近神的蝴蝶妖精，因爲四季異變而獲得力量。
    種族：[妖精](../Page/妖精.md "wikilink")

<!-- end list -->

  - [坂田合欢乃](../Page/東方天空璋_～_Hidden_Star_in_Four_Seasons.#坂田合欢乃.md "wikilink")（，Sakata
    Nemuno）
    第二關頭目兼中頭目，跨越浮世门关的山姥。
    種族：[山姥](../Page/山姥.md "wikilink")

<!-- end list -->

  - [高丽野阿吽](../Page/東方天空璋_～_Hidden_Star_in_Four_Seasons.#高丽野阿吽.md "wikilink")（，Komano
    Aunn）
    第三關頭目，在守護博麗神社時被靈夢誤會而與之戰鬥。
    種族：[狛犬](../Page/狛犬.md "wikilink")

<!-- end list -->

  - [矢田寺成美](../Page/東方天空璋_～_Hidden_Star_in_Four_Seasons.#矢田寺_成美.md "wikilink")（，Yatadera
    Narumi）
    第四關頭目，垂迹于森林的魔法地藏。
    種族：魔法使（地藏）

<!-- end list -->

  - [丁禮田舞](../Page/東方天空璋_～_Hidden_Star_in_Four_Seasons.#丁禮田_舞.md "wikilink")（，Teireida
    Mai）
    第四關中頭目，第五關頭目，Extra關中頭目，过于危险的背景舞者。
    種族：人类（？）

<!-- end list -->

  - [爾子田里乃](../Page/東方天空璋_～_Hidden_Star_in_Four_Seasons.#爾子田_里乃.md "wikilink")（，Nishida
    Satono）
    第五關頭目兼中頭目，Extra關中頭目，过于危险的背景舞者。
    種族：人类（？）

<!-- end list -->

  - [摩多羅隱岐奈](../Page/東方天空璋_～_Hidden_Star_in_Four_Seasons.#摩多羅_隱岐奈.md "wikilink")（，Matara
    Okina）
    第六（最終）關頭目、Extra關頭目，是這次異變的元兇，有着諸多個神的名號，在主人公的背後打開門，令主人公不得不在處於劣勢的情況下與之對抗。
    種族：秘神

## 《香霖堂》中首次登場

  - [森近霖之助](../Page/森近霖之助.md "wikilink")（，Morichika Rinnosuke）
    香霖堂的主人，角色中为数不多的男性之一，有识别物品的名称和用途的能力，和魔理沙十分相熟。
    種族：半人半妖

<!-- end list -->

  - [朱鷺子](../Page/東方香霖堂_～_Curiosities_of_Lotus_Asia.#朱鷺子.md "wikilink")（，Tokiko）
    在小說《東方香霖堂》第二話裡登場的讀書妖怪，只出现两话。名字是粉丝们命名的。
    種族：妖怪

## 《三月精》中首次登場

  - [桑妮蜜兒可](../Page/桑妮蜜兒可.md "wikilink")／桑妮·米爾克／桑尼·米爾克（，Sunny Milk）
    光之三妖精之首，代表太阳，十分活泼，拥有改变光线折射的能力。
    種族：妖精（日光之妖精）

<!-- end list -->

  - [露娜柴爾德](../Page/露娜柴爾德.md "wikilink")／露娜·切雲德／露娜·洽尔德／露娜·切露徳（，Luna
    Child）
    光之三妖精之一，代表月亮，比较沉寂，拥有消除周围声音的能力。
    種族：妖精（月光之妖精）

<!-- end list -->

  - [絲妲莎菲雅](../Page/絲妲莎菲雅.md "wikilink")／斯塔·萨菲娅／斯塔·塞菲爾（，Star Sapphire）
    光之三妖精之一，代表星辰，性格友善但不稳定，拥有察觉周围活动生物的能力
    種族：妖精（星光之妖精）

## 《求聞史紀》中首次登場

  - [稗田阿求](../Page/稗田阿求.md "wikilink")（，Hieda no Akyuu）
    稗田家御阿禮之子的第九代轉生，負責撰寫《幻想鄉緣起》。
    種族：人類

<!-- end list -->

  - [御阿禮之子](../Page/東方求聞史紀_〜_Perfect_Memento_in_Strict_Sense.#御阿禮之子.md "wikilink")（）
    家族代代受紫的許可進行求聞史紀的撰寫，名字來源於日本著名史料《[古事記](../Page/古事記.md "wikilink")》的編纂者[稗田阿禮](../Page/稗田阿禮.md "wikilink")。
    種族：人類

## 《儚月抄》中首次登場

  - [綿月豐姬](../Page/東方儚月抄#綿月_豐姬.md "wikilink")（，Watatsuki no Toyohime）
    月的公主，也是月之使者的領導者之一，月都中少數能夠連繫月都與地上、表之月與裹之月的能力者。
    種族：月人

<!-- end list -->

  - [綿月依姬](../Page/東方儚月抄#綿月_依姬.md "wikilink")（，Watatsuki no Yorihime）
    月的公主，也是月之使者的領導者之一，豐姬的妹妹，能夠將神明憑依在自己身上並自在的使用它們的力量。
    種族：月人

<!-- end list -->

  - [泠仙](../Page/泠仙.md "wikilink")（，Reisen）
    从月球逃到地面的月兔，后来与永琳会面后，带着永琳交待的信件回到月球，并作为豐姬的宠物。
    種族：月兔

## 《茨歌仙》中首次登場

  - [茨木華扇](../Page/东方茨歌仙_～_Wild_and_Horned_Hermit.#茨木_華扇.md "wikilink")（，Ibaraki
    Kasen）
    突然出現在博麗神社，並自稱為仙人的少女。操弄著無數的動物，有著仙人般的談吐舉止，而且相當愛說教。住在妖怪深山裡的修道場。名字來自於[酒吞童子的手下](../Page/酒吞童子.md "wikilink")[茨木童子](../Page/茨木童子.md "wikilink")。
    種族：不明（据推测可能是鬼）
    职业：仙人

## 《鈴奈庵》中首次登場

  - [本居小铃](../Page/本居小铃.md "wikilink")（，Motoori Kosuzu）
    居住在人间之里租书屋铃奈庵的主人之女， 東方鈴奈庵的主角，拥有读懂他人无法理解的书籍和可能利用其上面力量的能力。
    種族：人類

## 音乐集中登場

以下2名角色是 [ZUN's Music Collection系列的第二作《蓮台野夜行 ～ Ghostly Field
Club.》初次登場的角色](../Page/上海爱丽丝幻乐团#ZUN's_Music_Collection系列.md "wikilink")。

### 瑪艾瑞貝莉·赫恩

  - ，Maribel Han
    **種族**：人類
    **能力**：看見結界的能力
    **主題曲**
      - 　少女秘封俱樂部

      - 　魔術師梅莉

      - 　月之妖鳥，化貓之幻
    **登場**
      - 《蓮台野夜行 ～ Ghostly Field Club.》
      - 《夢違科學世紀 ～ Changeability of Strange Dream.》
      - 《卯酉東海道 ～ Retrospective 53 minutes.》
      - 《大空魔術 ～ Magical Astronomy.》
      - 《求聞史紀》
      - 《》
      - 《鳥船遺跡　～ Trojan Green Asteroid.》
      - 《伊弉諾物質　～ Neo-traditionalism of Japan.》

幻想鄉外的一個近未來的大學生，是專門探索境界的靈異組織「秘封俱樂部」的社員，專攻相對性精神學。她的住所是在音樂CD的舞台中成為了日本首都的京都。時常都很討厭蓮子的能力。蓮子叫她作「梅莉」（），而她的本名則初次出在《夢違科學世紀》中。她好像曾經夢見自己飛到幻想鄉，並與八雲紫等人見面，蓮子得悉後對梅莉說：「妳的能力不是正在變成『操縱境界的能力』嗎？」，這也使得她為此煩惱。

其姓氏與日本妖怪小說作家[小泉八雲的原來的姓相同](../Page/小泉八雲.md "wikilink")，又因為形象與八雲紫相似，同人有認為兩人有關係。

### 宇佐見蓮子

**，Usami Renko**

  -
    **種族**：人類
    **能力**：只看星星能知現在的時間、只看月亮能知目前的所在地的能力
    **主題曲**
      - 　少女秘封俱樂部

      - 　月之妖鳥，化貓之幻
    **登場**
      - 《蓮台野夜行 ～ Ghostly Field Club.》
      - 《夢違科學世紀 ～ Changeability of Strange Dream.》
      - 《卯酉東海道 ～ Retrospective 53 minutes.》
      - 《大空魔術 ～ Magical Astronomy.》
      - 《》
      - 《鳥船遺跡　～ Trojan Green Asteroid.》
      - 《伊弉諾物質　～ Neo-traditionalism of Japan.》

幻想鄉外的一個近未來的日本大學生，與梅莉一樣也是「秘封俱樂部」的成員，專攻超統一物理學，亦研究[弦理論](../Page/弦理論.md "wikilink")。雖然與梅莉一同住在京都，但她的父母家似乎在[東京](../Page/東京.md "wikilink")。雖然只看天便能知到現在時間和地點，但卻常常在梅莉等候時遲到。

## 只在設定上存在的角色

### 冴月麟

  - ，Satsuki Rin
    **登場**：《紅魔鄉》（没正式登场，名字出现在程序中）
    在[Shift
    JIS編碼下使用](../Page/Shift_JIS.md "wikilink")[文字編輯器打開東方紅魔鄉主程序後能看到名字的相应字符串和](../Page/文字編輯器.md "wikilink")「花符」、「風符」的字樣，解包遊戲文件也能發現缺失的立繪文件名。據愛好者推測可能是製作遊戲時預定登場但最後沒有登場的角色。在游戏发售前的相应海报封面上也有一張黑白色的彈着二胡的未知名稱的少女立繪，被推測爲其的立繪，後來的二次創作也多以這張立繪作爲基礎。名稱讀音不明，但直接讀的話應為「」。

### 麗菈·普利森瑞柏／蕾拉·普莉兹姆利巴

  - ，Layla Prismriver
    **登場**：《妖妖夢》（）
    **種族**：人類
    創造出「普利森瑞柏三姊妹」的少女，也是人類的「普利森瑞柏姊妹」的四女。偶然由身為貴族貿易商的父親拿到了幻想鄉的魔法道具。之後普利森瑞柏崩壞，三個姐姐分別被別人收養，只有麗菈一個人留在普利森瑞柏家。麗菈使用她最大的力量創造了和姐姐擁有一樣外表的騷靈，之後與騷靈和房屋一起从现世消失到了幻想鄉。在騷靈的陪伴下結束了其天壽。

### 魂魄 妖忌

  - ，Konpaku Youki
    **登場**：《妖妖夢》（）
    **種族**：半人半靈
    妖夢的爺爺，一名精熟劍術的師傅，在西行寺家擔任了300年的庭師。某一天突然頓悟，把庭師的身份繼讓給妖夢後就隱居起來。知道1000年前西行妖和幽幽子的關係。

### 名居家族、名居守

  -
    **登場**：《緋想天》
    **種族**：天人
    神官，管理幻想鄉一帶的地震。比那名居家族為其部下。

### 比那名居的首領

  -
    **登場**：《緋想天》
    **種族**：後天的天人
    比那名居家族的人。在衣玖的故事路線裡看起來似乎是她的上司。

### 聖命蓮

  -
    **登場**：《星蓮船》
    **種族**：人類
    白蓮的弟弟。為佛法修行高深的僧人，不過卻比白蓮還快老死。對於白蓮有很大的影響。「命蓮寺」就是以他的名字命名的。

### 魔理沙的父親

### 嫦娥

### 月夜見

### 天照大神

  -
    **登場**：《三月精》
    **種族**：
    月夜見的姐姐，太陽之神。其使者是[八咫烏](../Page/八咫烏.md "wikilink")。
    每年跨年的晚上會與天香香背男命展開「日與昼的世界」和「月與星的夜的世界」的戰鬥，如果沒有辦法擊敗明星的話，當年就會成為妖怪力量大漲的妖怪之年。
    住在太陽上的賢者，太陽內一樣擁有像月之都的結界。

### 天香香背男命

  -
    **登場**：東方三月精，《儚月抄》 （漫畫）
    **種族**：神明
    。天津甕星。抵抗太陽到最後的惡魔之星的神。日本神话中对应的神明为[天香香背男](../Page/天香香背男.md "wikilink")。

### 先代博麗巫女

  -
    **登場**：[C65](../Page/Comic_Market.md "wikilink")─「上海愛麗絲通訊 ver 2.7」傳單
    **種族**：人類
    博麗靈夢之前、先代博麗神社的巫女。只作為霖之助回憶的名字出現。先代「博麗巫女」通常只是以「巫女」來稱呼（對靈夢直接稱呼巫女的很少，因為她不太認真工作），所以霖之助也忘了她的名字，不過大家認同她創造了一個安定的生活。

## 其他特定的生物角色

### 大蟾蜍

  -
    **登場**：《花映塚》、《文花帖》（書籍）
    **種族**：妖怪蛙（[大蝦蟇](../Page/大蝦蟇.md "wikilink")）
    或稱「大蛤蟆」，妖怪山中「大蟾蜍之池」的主人。為了懲罰把青蛙冰起來玩的琪露諾而曾經把她吞掉。

### 大鯰魚

  -
    **登場**：《緋想天》、《非想天則》、《憑依華》
    **種族**：[大鯰](../Page/大鯰.md "wikilink")
    巨大的鯰魚。住在地下，身體搖動時會引起地震。《緋想天》中以普通的大鯰登場，被咲夜烹調成為了宴會的下酒菜；《非想天則》裡則在對戰中登場。

### 小槌蛇

  -
    **登場**：《三月精》
    **種族**：[野槌](../Page/野槌.md "wikilink")、[槌子蛇](../Page/槌子蛇.md "wikilink")
    草之精靈的使者、出沒於藤蔓間的妖怪，力量雖然弱但一樣擁有操縱草的魔法。魔理沙覺得很可愛而把牠當寵物養了起來。

## 附註

[\*](../Category/東方Project角色.md "wikilink")
[Category:电子游戏角色列表](../Category/电子游戏角色列表.md "wikilink")