**高霧站**（）是[雪梨](../Page/雪梨_\(城市\).md "wikilink")[城市鐵路的](../Page/城市鐵路.md "wikilink")[東郊及伊拉瓦拉線的一個沿線車站](../Page/東郊及伊拉瓦拉線.md "wikilink")，它服務悉尼的的一個住宅區—高霧。它有一個[島式月台](../Page/島式月台.md "wikilink")。

## 歷史

舊高霧站最初在1885年12月開幕，使高霧成為一個有名的度假區。當新高霧橋在1972年建成後，新站亦建成。現今，我們仍可在電壓站附近見到舊站。

## 月台服務

大部份時間每小時會在2班列車，星期一至五的繁忙時間會加設班次。有時會有[南海岸線的列車停在此站](../Page/南海岸線.md "wikilink")。

[Como_trackplan.png](https://zh.wikipedia.org/wiki/File:Como_trackplan.png "fig:Como_trackplan.png")
 **1號月台**

  - 東郊及伊拉瓦拉線 — 往邦迪聯運。

**2號月台**

  - 東郊及伊拉瓦拉線 — 往瀑布。

### 傷殘人士設施

該站以行人隧道通往街外，但不設升降機等傷殘人士設施。車站在開放時間會在職員當值。\[1\]

## 外部連結

|                                        |                                  |                                                 |
| :------------------------------------: | :------------------------------: | :---------------------------------------------: |
|                **往都會區**                |              **路線**              |                     **往外城**                     |
| <span style="color: White;">奧特利</span> | [←](../Page/奧特利車站.md "wikilink") | <span style="color: White;">**東郊及伊拉瓦拉線**</span> |

[Category:悉尼鐵路車站](../Category/悉尼鐵路車站.md "wikilink")

1.