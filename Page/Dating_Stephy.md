《**Dating
Stephy**》是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[鄧麗欣的第](../Page/鄧麗欣.md "wikilink")3張個人音樂專輯，於2007年2月14日推出。該日剛剛好是西方[情人節](../Page/情人節.md "wikilink")，所以中文譯名亦叫“[約會](../Page/約會.md "wikilink")（Dating）鄧麗欣（Stephy）”。這張大碟比上一張大碟[Fantasy發行相差](../Page/Fantasy.md "wikilink")11個月，同時亦跟隨Fantasy的風格，推出三個不同版本。主打歌包括電燈膽、日久生情和七夕。這張大碟入選《IFPI香港唱片銷量大獎2007》的十大銷量粵語唱片。

## 大碟特色

  - 鄧麗欣在此大碟亦和上一張大碟一樣也有作詞作品，雖然不及上一隻大碟所有歌曲的作詞包辦，只有兩首歌是她的作詞作品，但也一樣肯定了鄧麗欣的作詞能力。
  - 電燈膽不但是她在2007年的第一首派台歌外，還是唱出她一直的心聲，就是入行以來一直都沒有拍拖。
  - 碟中所載歌曲《七夕》是以[中國](../Page/中國.md "wikilink")[情人節](../Page/情人節.md "wikilink")——[七夕為背景](../Page/七夕.md "wikilink")，其中有幾句歌詞也是以[牛郎](../Page/牛郎.md "wikilink")[織女的故事為題去填的](../Page/織女.md "wikilink")。

## 曲目列表

## 專輯派台歌曲成績

1.  電燈膽
2.  日久生情
3.  初見
4.  Fantasy Tale

## 外部連結

[Yahoo\!Music鄧麗欣《Dating
Stephy主頁》](https://web.archive.org/web/20110718133829/http://hk.music.yahoo.com/album-album.html?albumid=24556)

[category:鄧麗欣音樂專輯](../Page/category:鄧麗欣音樂專輯.md "wikilink")
[category:香港音樂專輯](../Page/category:香港音樂專輯.md "wikilink")
[category:流行音樂專輯](../Page/category:流行音樂專輯.md "wikilink")

[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")