**廣佛高速公路**是[广东省的](../Page/广东省.md "wikilink")[高速公路之一](../Page/高速公路.md "wikilink")。

東起於[廣州市](../Page/廣州市.md "wikilink")[白云区](../Page/白云区_\(广州市\).md "wikilink")[金沙洲的](../Page/金沙洲.md "wikilink")[橫沙](../Page/橫沙.md "wikilink")，連接[廣州環城高速公路北环段](../Page/廣州環城高速公路.md "wikilink")，經[南海](../Page/南海.md "wikilink")[里水鎮](../Page/里水鎮.md "wikilink")、[大瀝鎮](../Page/大瀝鎮.md "wikilink")，西止於[佛山](../Page/佛山.md "wikilink")[謝邊](../Page/謝邊.md "wikilink")，连接[佛開高速公路](../Page/佛開高速公路.md "wikilink")，全長15.7[公里](../Page/公里.md "wikilink")。

初期只有雙向四車道，但由於廣佛兩地交通極為繁忙，現在已擴闊至雙向八車道，兩側並设有緊急停車帶，配合交通流量。全線設有沙贝、沙涌、雅瑶、[大瀝及謝邊等互通式](../Page/大瀝.md "wikilink")[立交橋](../Page/立交橋.md "wikilink")。

工程于1986年动工，1989年8月8日通车，为[广东省第一条通车的](../Page/广东省.md "wikilink")[高速公路](../Page/高速公路.md "wikilink")。

## 互通立交及服务设施

## 注释

## 参见

  - [广佛新干线](../Page/广佛新干线.md "wikilink")

[Category:广州市高速公路](../Category/广州市高速公路.md "wikilink")
[Category:佛山市高速公路](../Category/佛山市高速公路.md "wikilink")