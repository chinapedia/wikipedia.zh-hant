電視劇《痞子英雄》裡的海港城}}
[Harbour_City_flags.jpg](https://zh.wikipedia.org/wiki/File:Harbour_City_flags.jpg "fig:Harbour_City_flags.jpg")\]\]
[Harbour_City,_Ocean_Terminal_Forecourt_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Harbour_City,_Ocean_Terminal_Forecourt_\(Hong_Kong\).jpg "fig:Harbour_City,_Ocean_Terminal_Forecourt_(Hong_Kong).jpg")誕前百年展」，右方為[尖沙咀天星碼頭](../Page/尖沙咀天星碼頭.md "wikilink")\]\]
**海港城**（英文：**Harbour
City**）位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")，是香港最大面積的購物中心及名牌集中地，屬[九龍倉置業旗下的物業](../Page/九龍倉置業.md "wikilink")。九倉於1987年，將海洋中心、海運大廈、馬哥孛羅香港酒店商場和港威商場所有廣東道物業統稱為海港城購物區。[商場部份佔地](../Page/商場.md "wikilink")200萬平方呎，包括六十多家食肆、1間大型[電影院](../Page/電影院.md "wikilink")、3間酒店以及約450間零售商店。而寫字樓部份佔地440萬平方呎。其他部份則佔地600萬平方呎，包括3間高級[酒店](../Page/酒店.md "wikilink")、有500個房間的服務式[住宅](../Page/住宅.md "wikilink")、一個私人[會所](../Page/會所.md "wikilink")（[九龍太平洋會](../Page/九龍太平洋會.md "wikilink")）及2000多個泊車位。海港城是由4個部份組成的建築群，分別為[海運大廈](../Page/海運大廈.md "wikilink")、[馬哥孛羅香港酒店](../Page/馬哥孛羅香港酒店.md "wikilink")、[海洋中心和](../Page/海洋中心.md "wikilink")[港威大廈](../Page/港威大廈.md "wikilink")。海港城由[九倉發展](../Page/九倉.md "wikilink")，由[九龍倉碼頭分階段重建而成](../Page/九龍倉碼頭.md "wikilink")。最早的部份[海運大廈於](../Page/海運大廈.md "wikilink")1966年落成，附設[郵輪碼頭](../Page/郵輪碼頭.md "wikilink")。整個建築群則於1980年代成形。

海港城（包括酒店部分）在2018年營業額為118.71億港元，按年升10%，營業盈利則升12%至96.78億元。租戶零售銷售總額370億元，整個物業項目在2018年底的估值為1843.89億元，酒店及會所營業額增加10%至14.76億元，服務式住宅營業額減少29%至至2.32億元，寫字樓營業額則升6%至26.34億元。商場營業額增長14%至75.29億元

## 歷史

  - 1987年：九倉將海洋中心、海運大廈、馬哥孛羅香港酒店商場和港威商場所有廣東道物業統稱為海港城購物區。
  - 2005年：九倉建議擴建九龍太平洋會與海運大廈之間的九龍永久碼頭第7號作新郵輪泊位，但不獲政府批准。\[1\]
  - 2008年：九龍太平洋會地契續約15年（舊契已於2006年底到期，故新契由2007年開始計算）\[2\]，地契由九龍永久碼頭第6號改稱為九龍內地段第11179號。\[3\]
  - 2012年：海運大廈地契續約21年，地契由九龍永久碼頭第83號改稱為九龍內地段第11178號。
  - 2013年：將港威商場上蓋的寫字樓10萬方呎樓面，改為商場。
  - 2018年：為滿足市場需求，[九龍倉置業將港威豪庭其中一座服務式住宅聽濤閣改建為寫字樓及商場](../Page/九龍倉置業.md "wikilink")，有關工程將於2019年年中前完成，改建後将提供36萬平方呎的寫字樓及商場空間，其中逾30萬方呎樓面寫字樓

## 特色

[The_Gateway_Harbour_City_night_view_201710.jpg](https://zh.wikipedia.org/wiki/File:The_Gateway_Harbour_City_night_view_201710.jpg "fig:The_Gateway_Harbour_City_night_view_201710.jpg")夜景\]\]
[Canton_Road_201409.jpg](https://zh.wikipedia.org/wiki/File:Canton_Road_201409.jpg "fig:Canton_Road_201409.jpg")
海港城是香港一個大型建築群，南至尖沙咀[天星碼頭](../Page/天星小輪.md "wikilink")，北至[中港城](../Page/中港城.md "wikilink")。由於它是香港最大面積的購物中心，因此也成為了遊客的觀光購物景點之一。每逢星期六及星期日，都會有逾15萬人次進入商場，當中包括當地居民、外地遊客，及中國大陸的[自由行人士](../Page/港澳個人遊.md "wikilink")。而平日亦有超過60,000人於上蓋的辦公室大樓上班。該商場擁有表演及展覽小場地，商場亦舉辦“Music
in the City”，這是一個逢星期六、日邀請樂隊或在商場內演奏樂曲。

海港城在聖誕節和農曆新年等節日，都會把廣場佈置具有濃厚的節日氣氛，而佈置重點都放在近尖沙咀天星碼頭的入口。

海港城南面入口前的[五支旗桿](../Page/:zh-yue:五支旗桿.md "wikilink")，是市民集合及民間活動的熱點。昔日，五支旗桿所懸掛的旗幟由高至矮分別是：[英國國旗](../Page/英國國旗.md "wikilink")、[香港旗](../Page/香港旗.md "wikilink")、[九龍倉](../Page/九龍倉.md "wikilink")、**海港城**、[天星小輪](../Page/天星小輪.md "wikilink")。\[4\][九七回歸後](../Page/九七回歸.md "wikilink")，五支旗桿所懸掛的旗幟已換成：[九龍倉](../Page/九龍倉.md "wikilink")、**海港城**、[九倉電訊](../Page/九倉電訊.md "wikilink")、[香港有線電視](../Page/香港有線電視.md "wikilink")、[天星小輪](../Page/天星小輪.md "wikilink")。近年，再次換成：[中華人民共和國國旗](../Page/中華人民共和國國旗.md "wikilink")、[香港特別行政區區旗](../Page/香港特別行政區區旗.md "wikilink")、[九龍倉](../Page/九龍倉.md "wikilink")、**海港城**、[天星小輪](../Page/天星小輪.md "wikilink")。

<File:Harbour> City Lunar New Year 2006 decoration.jpg|海港城農曆新年佈置（2006年）
<File:Harbour> City in Christmas decoration.jpg|海港城聖誕節佈置（2006年）
<File:Harbour> City 2013 Christmas.jpg|海港城聖誕節佈置（2013年） <File:Harbour>
City Spider Man decoration 201404.jpg|海港城2014年The Amazing Power @Harbour
City電影場景裝置 <File:Hong> Kong (2017) -
1,242.jpg|海港城的[五支旗桿](../Page/:zh-yue:五支旗桿.md "wikilink")

## 購物區

海港城可分為以下的五個購物區：

**購物區OT** - 海港城·[海運大廈](../Page/海運大廈.md "wikilink")

  -
    在2002至2004年，2012年至2015年時曾進行大型翻新，現分為地下的**kid-x**、二樓的**sport-X**和三樓的**LCX**，分別設有兒童用品、運動用品和年青人服裝。
    主要商戶<small>（根據海港城地圖顯示）</small>:
    [玩具反斗城](../Page/玩具反斗城.md "wikilink")、[facess](../Page/facess.md "wikilink")、[GigaSports](../Page/GigaSports.md "wikilink")、旅客候船大堂

**購物區HH** - 海港城·馬哥孛羅香港酒店商場

  -
    酒店商場設多層商店及海運戲院，到1993年進行改建，部份戲院範圍於1994年改為[Planet
    Hollywood餐廳](../Page/Planet_Hollywood.md "wikilink")，1999年結業。於2003年時曾進行大型翻新，主要商戶[連卡佛亦由海運大廈購物區的舖位搬遷入本購物區](../Page/連卡佛.md "wikilink")。
    主要商戶<small>（根據海港城地圖顯示）</small>:
    [連卡佛](../Page/連卡佛.md "wikilink")、[海運戲院](../Page/橙天嘉禾.md "wikilink")

**購物區OC** - 海港城·[海洋中心](../Page/海洋中心.md "wikilink")

  -
    面向廣東道的海港城擁有全球名氣數一數二的名店，由海洋中心延伸至港威商場，形成一條名店街。
    主要商戶<small>（根據海港城地圖顯示）</small>:
    [Hermes](../Page/Hermes.md "wikilink")、[Salvatore
    Ferragamo](../Page/Salvatore_Ferragamo.md "wikilink")、[Chanel](../Page/香奈兒.md "wikilink")、[Louis
    Vuitton](../Page/Louis_Vuitton.md "wikilink")、[馬莎百貨](../Page/馬莎百貨.md "wikilink")、[太子珠寶鐘錶](../Page/太子珠寶鐘錶.md "wikilink")、[Gallery
    @ Harbour
    City](../Page/Gallery_@_Harbour_City.md "wikilink")、[香港上海匯豐銀行](../Page/香港上海匯豐銀行.md "wikilink")、[MUJI](../Page/MUJI.md "wikilink")

**購物區GW** - 海港城·[港威商場](../Page/港威商場.md "wikilink")

  -
    商場前身是海洋廊購物商場，建築物於1991年12月分階段拆卸。其中沿廣東道延綿530米的街舖於2005年開始成為頂級品牌的集中地。2011年起，港威大廈開始分階段進行翻新及改建工程，並將寫字樓改為商場。新租戶包括[GAP](../Page/GAP.md "wikilink")、[Uniqlo](../Page/Uniqlo.md "wikilink")、[PageOne旗艦店及Nike](../Page/葉壹堂.md "wikilink")
    Kicks Lounge。
    主要商戶<small>（根據海港城地圖顯示）</small>:
    [BVLGARI](../Page/BVLGARI.md "wikilink")、[Gucci](../Page/Gucci.md "wikilink")、[Moncler](../Page/Moncler.md "wikilink")、[Prada](../Page/Prada.md "wikilink")、[Miu
    Miu](../Page/Miu_Miu.md "wikilink")、[Valentino](../Page/Valentino.md "wikilink")、[FENDI](../Page/FENDI.md "wikilink")、[Coach](../Page/Coach.md "wikilink")、[Dolce
    & Gabbana](../Page/Dolce_&_Gabbana.md "wikilink")、[Paul &
    Shark](../Page/Paul_&_Shark.md "wikilink")、[JOYCE](../Page/JOYCE.md "wikilink")、[Emporio
    Armani](../Page/Emporio_Armani.md "wikilink")、[Giorgio
    Armani](../Page/Giorgio_Armani.md "wikilink")、[嘉禾港威電影城](../Page/橙天嘉禾.md "wikilink")2016年2月15日結業、[ZARA](../Page/ZARA.md "wikilink")、[agnes
    b cafe
    lpg](../Page/agnes_b_cafe_lpg.md "wikilink")、[UNIQLO](../Page/UNIQLO.md "wikilink")、[c\!ty'super](../Page/c!ty'super.md "wikilink")、[Cooked
    Deli by
    c\!tysuper](../Page/c!ty'super.md "wikilink")、[LOGON](../Page/LOGON.md "wikilink")、[PageOne已結業](../Page/葉壹堂.md "wikilink")、[HSBC
    Premier](../Page/香港上海匯豐銀行.md "wikilink")、[Harbour City
    Bazaar](../Page/Harbour_City_Bazaar.md "wikilink")、[芝樂坊餐廳](../Page/芝樂坊餐廳.md "wikilink")

**購物區ST** - STAR ANNEX@[星光行](../Page/星光行.md "wikilink")

  -
    在2005年九龍倉購入[星光行](../Page/星光行.md "wikilink")2樓及3樓樓層，並於2008年交由海港城管理，並易名為星光城，成為海港城的一部分。這裡主要售賣電腦配件，三樓有天橋接駁馬哥孛羅香港酒店商場。到2015年改為共3.9萬方呎的[誠品生活](../Page/誠品生活.md "wikilink")。
    主要商戶<small>（根據海港城地圖顯示）</small>:[誠品生活](../Page/誠品生活.md "wikilink")

## 圖集

<File:The> Gateway 2008.jpg|[港威大廈](../Page/港威大廈.md "wikilink") <File:HK>
Ocean Terminal Dusk View 201205.jpg|[海運大廈](../Page/海運大廈.md "wikilink")
<File:Ocean> Centre.jpg|[海洋中心](../Page/海洋中心.md "wikilink")
[File:Harbour_city.svg|海港城各大廈分佈簡圖](File:Harbour_city.svg%7C海港城各大廈分佈簡圖)

## 口號

海港城有一廣告，口號為：「一個海港，只有一個**海港城**」，暗示其獨一無二的大。

## 途經之公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[尖沙咀站](../Page/尖沙咀站.md "wikilink")
  - <font color="{{西鐵綫色彩}}">█</font>[西鐵綫](../Page/西鐵綫.md "wikilink")：[尖東站](../Page/尖東站.md "wikilink")、[柯士甸站](../Page/柯士甸站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參見

  - [海運大廈](../Page/海運大廈.md "wikilink")
  - [馬哥孛羅香港酒店](../Page/馬哥孛羅香港酒店.md "wikilink")
  - [海洋中心](../Page/海洋中心.md "wikilink")
  - [港威大廈](../Page/港威大廈.md "wikilink")
  - [中港城](../Page/中港城.md "wikilink")

## 資料來源

## 外部連結

  - [海港城的官方網站](http://www.harbourcity.com.hk/)

[Category:九龍倉置業物業](../Category/九龍倉置業物業.md "wikilink")
[Category:油尖旺區寫字樓](../Category/油尖旺區寫字樓.md "wikilink")
[Category:油尖旺區商場](../Category/油尖旺區商場.md "wikilink")
[Category:尖沙咀](../Category/尖沙咀.md "wikilink")
[Category:幻彩詠香江](../Category/幻彩詠香江.md "wikilink")
[Category:維多利亞港](../Category/維多利亞港.md "wikilink")
[Category:香港建築之最](../Category/香港建築之最.md "wikilink")

1.  <http://www.wharfholdings.com/file/051231%20Executive_Summary_c.doc>
2.  <http://www.mpfinance.com/htm/Finance/20080411/News/la_lam1.htm>
3.  <http://www.wharfholdings.com/download_chi/ar2008/13%20Wharf%20AR08_Finan%20Information_c.pdf>
4.  <http://i84.photobucket.com/albums/k32/netwwr/ybhk08.jpg>