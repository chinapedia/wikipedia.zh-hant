**阿利坎特省**（，[瓦倫西亞語](../Page/瓦倫西亞語.md "wikilink")：Alacant）是[西班牙](../Page/西班牙.md "wikilink")[省份之一](../Page/西班牙行政區劃.md "wikilink")，在西班牙東部、[巴倫西亞自治區的南部](../Page/巴倫西亞自治區.md "wikilink")。[東有](../Page/東.md "wikilink")[地中海](../Page/地中海.md "wikilink")、[西有](../Page/西.md "wikilink")[卡斯特利翁](../Page/卡斯特利翁.md "wikilink")、[北有](../Page/北.md "wikilink")[巴倫西亞省](../Page/巴倫西亞省.md "wikilink")、西南有[穆爾西亞自治區](../Page/穆爾西亞自治區.md "wikilink")。首府為[阿利坎特](../Page/阿利坎特.md "wikilink")。

阿利坎特省總面積為5817平方公里\[1\]。根據2017年的人口普查，阿利坎特省的人口是西班牙各省的第五高，有1 825
332名居民\[2\]。

## 地理

### 行政

<table>
<thead>
<tr class="header">
<th><p>市镇人口排名<br />
(2017年)[3]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>排名</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

阿利坎特省多於五萬人的城市有阿利坎特、[埃爾切](../Page/埃爾切.md "wikilink")、[托雷维耶哈](../Page/托雷维耶哈.md "wikilink")、[奥里韦拉](../Page/奥里韦拉.md "wikilink")、[贝尼多尔姆](../Page/贝尼多尔姆.md "wikilink")、[阿尔科伊](../Page/阿尔科伊.md "wikilink")、[圣比森特德尔拉斯佩格和](../Page/圣比森特德尔拉斯佩格.md "wikilink")[埃尔达](../Page/埃尔达.md "wikilink")。该省有18%的人口居住在省会阿利坎特。

### 自然地理

#### 地形

北面和西面的地較多山，南面較平；省內的最高點有：

| 汉语译名                                     | 当地名称                     | [平均海拔](../Page/平均海拔.md "wikilink") |
| ---------------------------------------- | ------------------------ | ---------------------------------- |
| [艾塔納山](../Page/艾塔納山.md "wikilink")       | Sierra de Aitana         | 1558米                              |
| [坎帕納峰](../Page/坎帕納峰.md "wikilink")       | Puig Campana             | 1401米                              |
| [馬里奧拉山脈](../Page/馬里奧拉山脈.md "wikilink")   | Sierra de Mariola        | 1400米                              |
| [邁格莫山脈](../Page/邁格莫山脈.md "wikilink")     | Sierra del Maigmó        | 1226米                              |
| [貝爾尼亞山脈](../Page/貝爾尼亞山脈.md "wikilink")   | Sierra de Bernia         | 1128米                              |
| [錫德山脈](../Page/錫德山脈.md "wikilink")       | Sierra del Cid           | 1127米                              |
| [卡拉斯克塔山脈](../Page/卡拉斯克塔山脈.md "wikilink") | Sierra de la Carrasqueta | 1240米                              |
| [克雷維連特山脈](../Page/克雷維連特山脈.md "wikilink") | Sierra de Crevillente    | 835米                               |

海岸從北方的 Cabo de la Nao 延伸到南方的 Mar Menor。

#### 水文

该省有許多乾枯的河流，在下雨時能存水。 主要[河流有三条](../Page/河流.md "wikilink")：

| 河流                                   | 当地名称         | 流经该省长度（总长度） | 流经的主要城市                                                                                              | 入海口                                            |
| ------------------------------------ | ------------ | ----------- | ---------------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| [塞古拉河](../Page/塞古拉河.md "wikilink")   | Río Segura   | 40（325公里）   | [奥里韦拉和](../Page/奥里韦拉.md "wikilink")[罗哈莱斯](../Page/罗哈莱斯.md "wikilink")                                | [瓜尔达马尔德尔塞古拉](../Page/瓜尔达马尔德尔塞古拉.md "wikilink") |
| [比納洛波河](../Page/比納洛波河.md "wikilink") | Río Vinalopó | 81.2公里      | [埃尔达](../Page/埃尔达.md "wikilink")、[诺韦尔达和](../Page/诺韦尔达.md "wikilink")[埃爾切](../Page/埃爾切.md "wikilink") | [圣波拉](../Page/圣波拉.md "wikilink")               |
| 色比斯河                                 | Río Serpis   | 75公里        | [阿尔科伊](../Page/阿尔科伊.md "wikilink")                                                                   | [甘迪亚](../Page/甘迪亚.md "wikilink")               |

#### 气候

阿利坎特省的大部分地区在[柯本气候分类法中属于](../Page/柯本气候分类法.md "wikilink")[地中海氣候](../Page/地中海氣候.md "wikilink")（Csa）和[半干旱性气候](../Page/半干旱气候.md "wikilink")（Bsh）。\[4\]有溫和的各季和又熱又乾的夏季。溫度中位數是17.5℃，且有較低的雨量——每年355立方毫米。在沿岸地區，雪和霜都甚為少見。

代表城市[阿利坎特属于半干旱性气候](../Page/阿利坎特.md "wikilink")，冬季温和，夏季炎热，全年降雨稀少。\[5\]\[6\]

主要的植被类型是叢木，例如[百里香](../Page/百里香.md "wikilink")、[杜松和](../Page/杜松.md "wikilink")等，不過有些特別的地區有[橄欖樹](../Page/橄欖樹.md "wikilink")、[橙樹](../Page/橙樹.md "wikilink")、[棕櫚科](../Page/棕櫚科.md "wikilink")、[松等等](../Page/松.md "wikilink")。

## 经济

阿利坎特省的經濟主要有：[農業](../Page/農業.md "wikilink")（蔬果、酒）、[漁業](../Page/漁業.md "wikilink")（[沙丁魚](../Page/沙丁魚.md "wikilink")、鯡鯢鰹）、[工業](../Page/工業.md "wikilink")（[紡織](../Page/紡織.md "wikilink")、造鞋業、[玩具](../Page/玩具.md "wikilink")）、[旅遊和](../Page/旅遊.md "wikilink")[貿易](../Page/貿易.md "wikilink")（[港口](../Page/港口.md "wikilink")）。除了首府阿利坎特，其他重要商業中心有：[阿尔科伊](../Page/阿尔科伊.md "wikilink")（紡織、紙）；[德尼亚](../Page/德尼亚.md "wikilink")
和
[贝尼多尔姆](../Page/贝尼多尔姆.md "wikilink")（海濱度假勝地）；[埃爾切和](../Page/埃爾切.md "wikilink")[埃尔达](../Page/埃尔达.md "wikilink")（製鞋）；[克雷维连特](../Page/克雷维连特.md "wikilink")（地氈）；[希霍纳](../Page/希霍纳.md "wikilink")（牛軋糖）；[奥里韦拉](../Page/奥里韦拉.md "wikilink")（廚房花園）；[圣比森特德尔拉斯佩格](../Page/圣比森特德尔拉斯佩格.md "wikilink")（西班牙第四大的[大学城](../Page/大学.md "wikilink")\[7\]）；[托雷维耶哈](../Page/托雷维耶哈.md "wikilink")（旅遊、製鹽）；[阿尔特亚](../Page/阿尔特亚.md "wikilink")、[卡尔佩](../Page/卡尔佩.md "wikilink")、[比利亚霍约萨](../Page/比利亚霍约萨.md "wikilink")
和 [比列纳](../Page/比列纳.md "wikilink")。

## 著名人物

[David_Ferrer_-_Roland_Garros_2013_-_001.jpg](https://zh.wikipedia.org/wiki/File:David_Ferrer_-_Roland_Garros_2013_-_001.jpg "fig:David_Ferrer_-_Roland_Garros_2013_-_001.jpg")\]\]

  - [恩里克·巴洛爾](../Page/恩里克·巴洛爾.md "wikilink")（1911年－2000年）：[作家](../Page/作家.md "wikilink")、[语言学家](../Page/语言学家.md "wikilink")，为[巴伦西亚语的推广做出了重要贡献](../Page/巴伦西亚语.md "wikilink")\[8\]
  - [安東尼奧·加德斯](../Page/安東尼奧·加德斯.md "wikilink")（1936年－2004年）：[佛朗明哥舞者](../Page/佛朗明哥.md "wikilink")、[编舞家](../Page/编舞家.md "wikilink")，前西班牙舞团艺术总监\[9\]
  - [吉列尔莫·阿莫尔](../Page/吉列尔莫·阿莫尔.md "wikilink")：男子[足球运动员](../Page/足球.md "wikilink")，教练，现已退役，\[10\]出生于[贝尼多尔姆](../Page/贝尼多尔姆.md "wikilink")
  - [胡安·卡洛斯·费雷罗](../Page/胡安·卡洛斯·费雷罗.md "wikilink")：职业男子[网球运动员](../Page/网球.md "wikilink")，是第21位登上[ATP男子单打排名第](../Page/职业网球联合会.md "wikilink")1的运动员\[11\]
  - [瑪麗亞·特雷莎·陶洛·弗洛](../Page/瑪麗亞·特雷莎·陶洛·弗洛.md "wikilink")：职业网球女运动员\[12\]，出生于[比列纳](../Page/比列纳.md "wikilink")
  - [大卫·费雷尔](../Page/大卫·费雷尔.md "wikilink")：职业男子网球运动员，个人最好成绩是2013年7月的[ATP世界男单第](../Page/职业网球联合会.md "wikilink")3名\[13\]，出生于[哈韦亚](../Page/哈韦亚.md "wikilink")\[14\]

## 图集

<div class="center">

<File:Altea3.jpg>|[阿尔特亚的La](../Page/阿尔特亚.md "wikilink") Mare de Déu del
Consol建筑群
<File:Montgó.jpg>|[哈韦亚的](../Page/哈韦亚.md "wikilink")[蒙特戈山](../Page/蒙特戈山.md "wikilink")
<File:Castillo> de Denia.jpg|[德尼亚城堡](../Page/德尼亚.md "wikilink")
<File:La> vila joiosa.jpg| [比利亚霍约萨海滩](../Page/比利亚霍约萨.md "wikilink")
<File:Sonnenaufgang> penon de ifach.jpg|[卡尔佩](../Page/卡尔佩.md "wikilink")
<File:Elche> el palmeral.JPG|[埃尔切的棕榈林](../Page/埃尔切的棕榈林.md "wikilink")
<File:Explanada> de España.jpg|[阿利坎特散步道](../Page/阿利坎特.md "wikilink")
<File:Claustro> catedral Orihuela.JPG|
[奥里韦拉教堂](../Page/奥里韦拉.md "wikilink")
<File:Pantano_de_Guadalest.jpg>| [瓜达莱斯特水库](../Page/瓜达莱斯特.md "wikilink")
<File:Aitana02.jpg>|
[普雷貝蒂科山脈最高峰](../Page/普雷貝蒂科山脈.md "wikilink")[艾塔納山](../Page/艾塔納山.md "wikilink")
<File:Vista> de Penáguila.jpg| [佩纳吉拉](../Page/佩纳吉拉.md "wikilink")
<File:Guadalest> (Alicante).jpg|
旅游小镇[瓜达莱斯特](../Page/瓜达莱斯特.md "wikilink")

</div>

## 注释

## 外部链接

  - [Excma. Diputación Provincial de
    Alicante](http://www.dip-alicante.es/)

[Alicante](../Category/西班牙省份.md "wikilink")
[Category:巴倫西亞自治區](../Category/巴倫西亞自治區.md "wikilink")

1.  [Superficie y
    altimetría](http://www.ine.es/inebaseweb/pdfDispacher.do?td=154090&L=0).INE.\[2018-09-05\]
2.  [Población por
    provincias](http://www.ine.es/jaxiT3/Datos.htm?t=2852).INE.\[2018-09-05\]
3.  Padrón a 01-01-2017 del INE, [Cifras oficiales de población
    resultantes de la revisión del Padrón municipal a 1 de enero
    de 2017,](http://www,ine,es/jaxiT3/Datos,htm?t=2856)
4.
5.
6.
7.
8.  [Rondalles Valencianes d'Enric
    Valor](http://www.bullent.net/llibres/llibres.asp?Col=Rondalles%20Valencianes)
9.
10.
11.
12.
13.
14.