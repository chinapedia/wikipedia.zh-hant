**拉萨·奥本海**\[1\]（Lassa Francis Lawrence Oppenheim，），德国法学家，被誉为“现代国际法之父”。

## 生平

奥本海生于[德国](../Page/德国.md "wikilink")[法兰克福附近的](../Page/法兰克福.md "wikilink")[温德肯](../Page/温德肯.md "wikilink")，求学于[柏林洪堡大学](../Page/柏林洪堡大学.md "wikilink")、[哥廷根大学](../Page/哥廷根大学.md "wikilink")、[海德堡大学和](../Page/海德堡大学.md "wikilink")[莱比锡大学](../Page/莱比锡大学.md "wikilink")，并于1881年获得[哥廷根大学法学博士](../Page/哥廷根大学.md "wikilink")。后于[弗莱堡大学完成教授资格论文](../Page/弗莱堡大学.md "wikilink")。1895年赴[伦敦任教直至逝世](../Page/伦敦.md "wikilink")。
其初任教于[巴塞尔大学](../Page/巴塞尔大学.md "wikilink")，后赴[伦敦政经学院并于](../Page/伦敦政经学院.md "wikilink")1908年成为[剑桥大学国际法教授](../Page/剑桥大学.md "wikilink")。其代表作为*International
Law: A Treatise*，于1905-1906出版。该书迄今被认为是国际法的经典教科书。

## 主要著作

  - 《盗窃罪的客体》 (1894)
  - 《良心论》 (1898)
  - 《国际法》 第一卷 *和平论* (1905; 1911年第二版)，第二卷 *战争论*, (1906; 1912年第二版)
  - 《国际法律科学》 (1908)
  - 《国际事件》 (1909; 1911第二版)
  - 《国际法的未来》 (1912年德文版; 1914年英文版)
  - 《巴拿马运河之争》 (1913)

## 參考資料

## 外部链接

  - [国际法](http://gallica.bnf.fr/ark:/12148/bpt6k93562g). 第一卷: 和平论
    1905年[Gallica版](../Page/Gallica.md "wikilink")
  - [国际法](http://gallica.bnf.fr/ark:/12148/bpt6k93563t). 第二卷: 战争与中立论
    1905年[Gallica版](../Page/Gallica.md "wikilink")
  - [1](http://ejil.oxfordjournals.org/cgi/reprint/11/3/699.pdf).
    作为法学家与先驱的国际法主义者：拉萨·奥本海
  - [2](http://ejil.oxfordjournals.org/cgi/reprint/13/2/401.pdf). Legal
    Positivism as Normative Politics: International Society, Balance of
    Power and Lassa Oppenheim's Positive International Law

[Category:德国法学家](../Category/德国法学家.md "wikilink")
[Category:国际法](../Category/国际法.md "wikilink")
[Category:柏林洪堡大学校友](../Category/柏林洪堡大学校友.md "wikilink")
[Category:哥廷根大学校友](../Category/哥廷根大学校友.md "wikilink")
[Category:海德堡大学校友](../Category/海德堡大学校友.md "wikilink")
[Category:莱比锡大学校友](../Category/莱比锡大学校友.md "wikilink")

1.  《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》，2056頁，「Oppenheim」條。