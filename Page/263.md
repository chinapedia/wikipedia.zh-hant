**263**是[262與](../Page/262.md "wikilink")[264之間的](../Page/264.md "wikilink")[自然數](../Page/自然數.md "wikilink")。

## 在數學中

  - [快樂數](../Page/快樂數.md "wikilink")
  - 5個連續[質數和](../Page/質數.md "wikilink")（43+47+53+59+61）

## 在人類文化中

  - [宜蘭縣](../Page/宜蘭縣.md "wikilink")[壯圍鄉的](../Page/壯圍鄉.md "wikilink")[郵遞區號為](../Page/郵遞區號.md "wikilink")263
  - [辛巴威的](../Page/辛巴威.md "wikilink")[國際電話區號為](../Page/國際電話區號.md "wikilink")263
  - [大和級戰艦是](../Page/大和級戰艦.md "wikilink")[大日本帝國海軍所建造](../Page/大日本帝國海軍.md "wikilink")[戰艦的艦型](../Page/戰艦.md "wikilink")，全長263[公尺](../Page/公尺.md "wikilink")
  - [九七式艦上攻擊機為大日本帝國海軍於](../Page/九七式艦上攻擊機.md "wikilink")[1930年代後期開發的艦上攻擊機](../Page/1930年代.md "wikilink")，巡航速度為263km/h
  - [2008年](../Page/2008年.md "wikilink")[10月3日](../Page/10月3日.md "wikilink")，[美國國會](../Page/美國國會.md "wikilink")[眾議院以](../Page/眾議院.md "wikilink")263票贊成[171票反對投票通過經過修改的總額達](../Page/171.md "wikilink")7000億[美元的](../Page/美元.md "wikilink")[2008年經濟穩定緊急法案](../Page/2008年經濟穩定緊急法案.md "wikilink")
  - [維克斯VC.1維京的](../Page/維克斯VC.1維京.md "wikilink")[最大速度為](../Page/最大速度.md "wikilink")263[英里](../Page/英里.md "wikilink")
  - [玉郎漫畫出版至](../Page/玉郎漫畫.md "wikilink")[1992年](../Page/1992年.md "wikilink")[9月19日第](../Page/9月19日.md "wikilink")263期停刊

## 在科學中

  - [鑪](../Page/鑪.md "wikilink")-263是鑪中最穩定的[同位素](../Page/同位素.md "wikilink")，它的[半衰期有](../Page/半衰期.md "wikilink")10分鐘
  - [1983年的美國針對](../Page/1983年.md "wikilink")[三聚氰胺進行](../Page/三聚氰胺.md "wikilink")[動物實驗研究](../Page/動物實驗.md "wikilink")，49隻雄鼠和50隻雌鼠每天被餵食263[毫克的三聚氰胺](../Page/毫克.md "wikilink")20週之後，約1/6的雄鼠身上被驗出有[癌細胞](../Page/癌細胞.md "wikilink")，而經過103週相當於2年多以後，49隻雄鼠全部都帶有癌細胞

## 在地理中

  - [香港一共有](../Page/香港.md "wikilink")263個[島嶼](../Page/島嶼.md "wikilink")
  - [亞歷山大縣
    (北卡羅萊納州)的面積為](../Page/亞歷山大縣_\(北卡羅萊納州\).md "wikilink")263[平方英里](../Page/平方英里.md "wikilink")
  - [伊塔斯加縣的水域面積為](../Page/伊塔斯加縣.md "wikilink")263平方英里

## 在天文中

  - [NGC 263](../Page/NGC_263.md "wikilink")
    是[鯨魚座的一個](../Page/鯨魚座.md "wikilink")[星系](../Page/星系.md "wikilink")

## 在其他領域中

  - 公元[263年和](../Page/263年.md "wikilink")[前263年](../Page/前263年.md "wikilink")
  - [H.263是由](../Page/H.263.md "wikilink")[ITU-T制定低碼率視訊編碼標準](../Page/ITU-T.md "wikilink")，屬於[視訊編解碼器](../Page/視訊編解碼器.md "wikilink")
  - [香港](../Page/香港.md "wikilink")[九龍巴士263線](../Page/九龍巴士263線.md "wikilink")，來往[屯門鐵路站與](../Page/屯門站公共運輸交匯處.md "wikilink")[沙田鐵路站](../Page/沙田站公共運輸交匯處.md "wikilink")
  - [猛獅NL263](../Page/猛獅NL263.md "wikilink")，[德國](../Page/德國.md "wikilink")[猛獅出產的巴士車款](../Page/猛獅.md "wikilink")

[Category:整數](../Category/整數.md "wikilink")
[Category:整數素數](../Category/整數素數.md "wikilink")