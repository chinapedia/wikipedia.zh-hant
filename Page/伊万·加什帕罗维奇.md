**伊万·加什帕罗维奇**（[斯洛伐克语](../Page/斯洛伐克语.md "wikilink")：****，），[斯洛伐克政治家](../Page/斯洛伐克.md "wikilink")、律师，2004年6月15日起任该国[总统至](../Page/斯洛伐克总统.md "wikilink")2014年。

加什帕罗维奇在[布拉迪斯拉發-{夸}-美紐斯大學法学院毕业](../Page/布拉迪斯拉發夸美紐斯大學.md "wikilink")，曾于[布拉迪斯拉发任检察官](../Page/布拉迪斯拉发.md "wikilink")。1968年，他加入[斯洛伐克共产党](../Page/斯洛伐克共产党_\(1939年\).md "wikilink")，支持[亚历山大·杜布切克的改革](../Page/亚历山大·杜布切克.md "wikilink")，但同年8月[华沙条约组织](../Page/华沙条约组织.md "wikilink")[入侵捷克斯洛伐克后](../Page/蘇聯及華沙條約成員國入侵捷克斯洛伐克.md "wikilink")，他被驱逐出党。

1968年至1990年7月，加什帕罗维奇在考门斯基大学法学院任教。[天鹅绒革命后共产政权被推翻](../Page/天鹅绒革命.md "wikilink")，加什帕罗维奇被[总统](../Page/捷克斯洛伐克总统.md "wikilink")[瓦茨拉夫·哈维尔委任为总检察长](../Page/瓦茨拉夫·哈维尔.md "wikilink")，1992年当选为捷克斯洛伐克的斯洛伐克国民议会议员，并担任副议长。

1992年，加什帕罗维奇加入[弗拉迪米尔·梅恰尔领导的](../Page/弗拉迪米尔·梅恰尔.md "wikilink")[争取民主斯洛伐克运动](../Page/争取民主斯洛伐克运动.md "wikilink")，翌年担任独立后的斯洛伐克国民议会议长。2002年，因为认为梅恰尔领导的争取民主斯洛伐克运动不民主，加什帕罗维奇退出该党，成立[争取民主运动](../Page/争取民主运动_\(斯洛伐克\).md "wikilink")，担任主席。\[1\]2004年4月17日，他在选举中击败梅恰尔，当选总统。[2009年成功連任](../Page/2009年斯洛伐克总统选举.md "wikilink")。

## 参考资料

## 外部链接

  - [竞选网站](https://web.archive.org/web/20040405184607/http://www.gasparovic.sk/cv.html)
  - [官方网站](http://www.prezident.sk/?introduction)

[Category:斯洛伐克天主教徒](../Category/斯洛伐克天主教徒.md "wikilink")
[Category:斯洛伐克律师](../Category/斯洛伐克律师.md "wikilink")
[Category:斯洛伐克总统](../Category/斯洛伐克总统.md "wikilink")
[Category:斯洛伐克共產主義者](../Category/斯洛伐克共產主義者.md "wikilink")
[Category:夸美紐斯大學校友](../Category/夸美紐斯大學校友.md "wikilink")

1.