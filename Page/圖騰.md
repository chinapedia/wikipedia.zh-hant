[Wawadit'la(Mungo_Martin_House)_a_Kwakwaka'wakw_big_house.jpg](https://zh.wikipedia.org/wiki/File:Wawadit'la\(Mungo_Martin_House\)_a_Kwakwaka'wakw_big_house.jpg "fig:Wawadit'la(Mungo_Martin_House)_a_Kwakwaka'wakw_big_house.jpg")

**图腾崇拜**是将某种特定物体视作与本氏族有[亲属或其他特殊关系的崇拜行为](../Page/亲属.md "wikilink")，是[原始宗教的最初形式](../Page/原始宗教.md "wikilink")，大约出现在[旧石器时代晚期](../Page/旧石器时代.md "wikilink")。「图腾」为[印第安语](../Page/印第安语.md "wikilink")*totem*的音译，源自[北美](../Page/北美.md "wikilink")[阿爾岡昆人](../Page/阿爾岡昆人.md "wikilink")[歐及布威族](../Page/歐及布威族.md "wikilink")[奧傑布瓦語](../Page/語言.md "wikilink")*ototeman*，意为“他的亲族”或“他的氏族”，相当于是整个部族的[标记](../Page/标记.md "wikilink")。许多[氏族往往以它命名](../Page/氏族.md "wikilink")。

所谓图腾，就是原始时代的人们把某种[动物](../Page/动物.md "wikilink")、[植物或](../Page/植物.md "wikilink")[非生物当做自己的](../Page/非生物.md "wikilink")[亲属](../Page/亲属.md "wikilink")、[祖先或](../Page/祖先.md "wikilink")[保护神](../Page/保护神.md "wikilink")。相信他们有一种[超自然力](../Page/超自然力.md "wikilink")，会保护自己和自己的[族群及](../Page/族群.md "wikilink")[部落](../Page/部落.md "wikilink")，并且还可以获得他们的[力量和](../Page/力量.md "wikilink")[技能](../Page/技能.md "wikilink")。在原始人的眼里，图腾是一个被人格化的[崇拜](../Page/崇拜.md "wikilink")[对象](../Page/对象.md "wikilink")。

它具有增加群体的[团结精神](../Page/团结精神.md "wikilink")、加強[血缘关系](../Page/血缘关系.md "wikilink")、令社会组织正常運作和互相区别的能力。同时通过图腾[标志](../Page/标志.md "wikilink")，得到對自己和族群的认同，受到同個族群裏大眾的保护。图腾标志最典型的就是[图腾柱](../Page/图腾柱.md "wikilink")，在[印第安的村落中](../Page/印第安.md "wikilink")，多立有图腾柱。在[中国东南沿海考古中](../Page/中国.md "wikilink")，也有人发现有鸟的图腾柱。[浙江](../Page/浙江.md "wikilink")[绍兴出土的](../Page/绍兴.md "wikilink")，战国时古越人铜质房屋的模型，屋顶竟然頂立了图腾柱，柱顶部有尾鸠。

图腾崇拜的[动物是不能](../Page/动物.md "wikilink")[捕杀的](../Page/捕杀.md "wikilink")，只能在特殊的场合举行[祭祀时才能杀死它](../Page/祭祀.md "wikilink")。

以[澳洲原住民為例](../Page/澳大利亞原住民.md "wikilink")，他們認為[靈魂會離開身體](../Page/靈魂.md "wikilink")，並停留在圖騰（即生物的替身）上。

[商族的圖騰是玄鳥](../Page/商朝.md "wikilink")\[1\]。因此，圖騰崇拜也可以說是對祖先的[崇拜](../Page/崇拜.md "wikilink")。圖騰與[親緣的關係有很多](../Page/親緣.md "wikilink")，如：[鄂倫春族族人把公熊稱為](../Page/鄂倫春族.md "wikilink")「雅亞」，意思為[祖父](../Page/祖父.md "wikilink")，又稱母熊為「太帖」，有[祖母的意思](../Page/祖母.md "wikilink")。

## 人與圖騰的關係

1.  圖騰是自己[血緣](../Page/血緣.md "wikilink")[親屬的](../Page/親屬.md "wikilink")[祖先](../Page/祖先.md "wikilink")，他們會用[父親](../Page/父親.md "wikilink")、[祖父](../Page/祖父.md "wikilink")、[祖母等等](../Page/祖母.md "wikilink")，[親屬的稱呼來稱呼圖騰](../Page/親屬.md "wikilink")，並以圖騰名稱作為自己整個族群的名稱。
2.  圖騰是自己族群的[祖先](../Page/祖先.md "wikilink")，認為自己整個族群的成員都是由圖騰[繁衍而來](../Page/繁衍.md "wikilink")。
3.  圖騰是群體的[保護神](../Page/保護神.md "wikilink")。但在[人類的](../Page/人類.md "wikilink")[思維有了一定](../Page/思維.md "wikilink")[發展後](../Page/發展.md "wikilink")，[人類便會了解到](../Page/人類.md "wikilink")[人類與](../Page/人類.md "wikilink")[動物之間有很大](../Page/動物.md "wikilink")[差異後](../Page/差異.md "wikilink")，他們不再認為圖騰可以[繁殖](../Page/繁殖.md "wikilink")[人類](../Page/人類.md "wikilink")。但圖騰祖先的[觀念根深蒂固](../Page/觀念.md "wikilink")，於是產生了圖騰是保護神的[觀念](../Page/觀念.md "wikilink")。

## 參考資料

  - 曲風：〈[图腾：古代神话还是现代神话？](http://www.nssd.org/articles/article_read.aspx?id=11495353)〉。

## 关连条目

  - [灵魂](../Page/灵魂.md "wikilink")
  - [神秘主义](../Page/神秘主义.md "wikilink")

[Category:原始宗教](../Category/原始宗教.md "wikilink")
[Category:文化人类学](../Category/文化人类学.md "wikilink")

1.  《史記》：天命玄鳥，降而生商。