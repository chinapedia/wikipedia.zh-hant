**印度-伊朗語族**（，或），又稱**雅利安語族**（）是[印歐語系今日仍在使用的](../Page/印歐語系.md "wikilink")[語言中最東方的一族](../Page/語言.md "wikilink")，下分為[印度-雅利安語支](../Page/印度-雅利安語支.md "wikilink")、[伊朗語支和](../Page/伊朗語支.md "wikilink")[奴利斯塔尼語支](../Page/奴利斯塔尼語支.md "wikilink")。

## 概述

關於印歐語言最古老的文獻中能夠看到大量此語族的記載。此族語言起源於上古[歐洲的](../Page/歐洲.md "wikilink")[希臘地區](../Page/希臘.md "wikilink")，在遠古時期就向東南方的[印度前進](../Page/印度.md "wikilink")，途經上古的[伊朗](../Page/伊朗.md "wikilink")、[阿富汗](../Page/阿富汗.md "wikilink")、和[巴基斯坦地區](../Page/巴基斯坦.md "wikilink")。

## 分類

  - [印度-雅利安語支舉例](../Page/印度-雅利安語支.md "wikilink")：
      - [梵语](../Page/梵语.md "wikilink") ()
      - [阿萨姆语](../Page/阿萨姆语.md "wikilink") ()
      - [孟加拉语](../Page/孟加拉语.md "wikilink") ()
      - [比哈尔语](../Page/比哈尔语.md "wikilink")
          - [安吉卡语](../Page/安吉卡语.md "wikilink") ()
          - [博杰普尔语](../Page/博杰普尔语.md "wikilink") ()
          - [摩揭陀語](../Page/摩揭陀語.md "wikilink") ()
          - [迈蒂利语](../Page/迈蒂利语.md "wikilink") ()
      - [迪维希语](../Page/迪维希语.md "wikilink") ()
      - [古吉拉特语](../Page/古吉拉特语.md "wikilink") ()
      - [印地语](../Page/印地语.md "wikilink") ()
      - [马拉地语](../Page/马拉地语.md "wikilink") ()
      - [尼泊尔语](../Page/尼泊尔语.md "wikilink") ()
      - [奥利亚语](../Page/奥利亚语.md "wikilink") ()
      - [巴利语](../Page/巴利语.md "wikilink") ()
      - [旁遮普语](../Page/旁遮普语.md "wikilink")（东旁遮普语） ()
      - [西旁遮普语](../Page/西旁遮普语.md "wikilink") ()
      - [罗姆语](../Page/罗姆语.md "wikilink")（又称[吉卜赛语](../Page/吉卜赛语.md "wikilink")）
        ()
      - [僧伽罗语](../Page/僧伽罗语.md "wikilink") ()
      - [乌尔都语](../Page/乌尔都语.md "wikilink") ()
      - [羅興亞語](../Page/羅興亞語.md "wikilink")

<!-- end list -->

  - [努力斯坦语支](../Page/努力斯坦语支.md "wikilink") Nuristani
      - Ashkun () (阿富汗)
      - Kati (Bashgali) () (阿富汗)
      - Prasuni (Wasi-Weri) (阿富汗)
      - Tregami () (阿富汗)
      - Waigali (Kalasha-Ala) () (阿富汗)
      - Kamviri () (阿富汗)

<!-- end list -->

  - [伊朗語支舉例](../Page/伊朗語支.md "wikilink")：
      - 東部
          - 東北部
              - [西徐亚语](../Page/西徐亚语.md "wikilink") (Scythian)（滅亡） ()
              - [阿維斯陀語](../Page/阿維斯陀語.md "wikilink")（滅亡） ()
              - [粟特語](../Page/粟特語.md "wikilink") （滅亡）()
              - [奧塞梯語](../Page/奧塞梯語.md "wikilink") ()
              - [雅格諾比語](../Page/雅格諾比語.md "wikilink") ()
          - 東南部
              - [普什圖語](../Page/普什圖語.md "wikilink") ()
      - 西部
          - 西北部
              - [俾路支語](../Page/俾路支語.md "wikilink") ()
              - [達利語](../Page/達利語.md "wikilink") ()
              - [庫爾德語](../Page/庫爾德語.md "wikilink") ()
              - [塔里什語](../Page/塔里什語.md "wikilink") ()
              - [扎扎其語](../Page/扎扎其語.md "wikilink") ()
                  - [南扎扎其語](../Page/南扎扎其語.md "wikilink") ()
                  - [北扎扎其語](../Page/北扎扎其語.md "wikilink") ()
          - 西南部
              - [盧爾語](../Page/盧爾語.md "wikilink")（Luri）
              - [波斯語](../Page/波斯語.md "wikilink") ()
                  - [東波斯語](../Page/東波斯語.md "wikilink") ()
                  - [西波斯語](../Page/西波斯語.md "wikilink") ()
                  - [塔吉克語](../Page/塔吉克語.md "wikilink") ()
                  - [回回语](../Page/回回语.md "wikilink") (khwari)
              - [塔特語](../Page/塔特語.md "wikilink") ()

<!-- end list -->

  - 語支未明：
      - Badeshi () (巴基斯坦)
      - Luwati () (阿曼)

## 参见

  - [印欧语系](../Page/印欧语系.md "wikilink")
  - [语言系属分类](../Page/语言系属分类.md "wikilink")

## 外部链接

  - <http://www.ethnologue.com/show_family.asp?subid=90018>

[Category:印欧语系](../Category/印欧语系.md "wikilink")
[印度-伊朗語族](../Category/印度-伊朗語族.md "wikilink")