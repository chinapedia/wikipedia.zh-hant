**格利揚**(Kalyan)为[印度](../Page/印度.md "wikilink")[马哈拉施特拉邦](../Page/马哈拉施特拉邦.md "wikilink")[康坎专区所属](../Page/康坎.md "wikilink")[城市](../Page/城市.md "wikilink")，是邻近[孟买的](../Page/孟买.md "wikilink")[铁路枢纽中心](../Page/铁路.md "wikilink")。卡延和[东比夫里](../Page/东比夫里.md "wikilink")([Dombivli](../Page/:en:Dombivli.md "wikilink"))市镇构成城市联合体。从区域上，卡延属于孟买都会区的一部分，和[新孟买以及](../Page/新孟买.md "wikilink")[比宛迪](../Page/比宛迪.md "wikilink")、[塔那](../Page/塔那.md "wikilink")、[烏爾哈斯訥格爾和](../Page/烏爾哈斯訥格爾.md "wikilink")[瓦塞维拉尔构成城市圈](../Page/瓦塞维拉尔.md "wikilink")。

[bpy:কল্যান-দোম্বিৱালি](../Page/bpy:কল্যান-দোম্বিৱালি.md "wikilink")
[de:Kalyan-Dombivli](../Page/de:Kalyan-Dombivli.md "wikilink")
[eo:Kaljan-Dombivali](../Page/eo:Kaljan-Dombivali.md "wikilink")
[ko:칼리얀](../Page/ko:칼리얀.md "wikilink")
[nl:Kalyan-Dombivli](../Page/nl:Kalyan-Dombivli.md "wikilink")
[no:Kalyan-Dombivali](../Page/no:Kalyan-Dombivali.md "wikilink")
[pam:Kalyan-Dombivali](../Page/pam:Kalyan-Dombivali.md "wikilink")
[pt:Kalyan-Dombivli](../Page/pt:Kalyan-Dombivli.md "wikilink")
[sv:Kalyan-Dombivli](../Page/sv:Kalyan-Dombivli.md "wikilink")

[Category:马哈拉施特拉邦城镇](../Category/马哈拉施特拉邦城镇.md "wikilink")
[Category:印度人口过百万的城市](../Category/印度人口过百万的城市.md "wikilink")