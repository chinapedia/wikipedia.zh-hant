[Copsychus_malabaricus_male_-_Khao_Yai.jpg](https://zh.wikipedia.org/wiki/File:Copsychus_malabaricus_male_-_Khao_Yai.jpg "fig:Copsychus_malabaricus_male_-_Khao_Yai.jpg")
[Copsychus_malabaricus_-_Khao_Yai.jpg](https://zh.wikipedia.org/wiki/File:Copsychus_malabaricus_-_Khao_Yai.jpg "fig:Copsychus_malabaricus_-_Khao_Yai.jpg")
**白腰鵲鴝**（[學名](../Page/學名.md "wikilink")：''
''），俗稱**長尾知渣**。曾被列入[鶇科](../Page/鶇科.md "wikilink")，故曾有**白腰鵲鶇**或稱**鵲鶇**等舊稱。

## 生态环境

普遍分佈於山谷樹林和[Ko'olaus南部的山嵴](../Page/Ko'olaus.md "wikilink")。在樹木稀疏的[低地](../Page/低地.md "wikilink")[闊葉林築巢](../Page/闊葉林.md "wikilink")。

## 分布地域

白腰鵲鴝分布于印度、尼泊尔、不丹、斯里兰卡、孟加拉、缅甸、泰国、老挝、越南、马来半岛、印度尼西亚诸岛以及[中国大陆的](../Page/中国大陆.md "wikilink")[云南](../Page/云南.md "wikilink")、[海南等地](../Page/海南.md "wikilink")，主要生活于热带林及竹林内。该物种的模式产地在印度西南部Malabar。\[1\]。但在1931年初從[馬來西亞引入](../Page/馬來西亞.md "wikilink")[夏威夷](../Page/夏威夷.md "wikilink")，1940年被引進到夏威夷[歐胡島](../Page/歐胡島.md "wikilink")。

## 亚种

  - **白腰鹊鸲云南亚种**（[学名](../Page/学名.md "wikilink")：）。分布于缅甸、泰国、老挝、越南以及[中国大陆的](../Page/中国大陆.md "wikilink")[云南等地](../Page/云南.md "wikilink")。该物种的模式产地在越南南部Daban。\[2\]
  - **白腰鹊鸲海南亚种**（[学名](../Page/学名.md "wikilink")：），是中国的特有物种。分布于[海南等地](../Page/海南.md "wikilink")。该物种的模式产地在海南岛。\[3\]

## 参考文献

[Category:鵲鴝屬](../Category/鵲鴝屬.md "wikilink")

1.
2.

3.