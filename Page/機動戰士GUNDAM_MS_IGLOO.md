**《机动战士GUNDAM MS
IGLOO》**是2004年起，在[日本松戶市](../Page/日本.md "wikilink")[万代](../Page/万代.md "wikilink")（[Bandai](../Page/Bandai.md "wikilink")）美術館所放映的三部《[机动战士高达](../Page/机动战士高达.md "wikilink")》系列[宇宙世纪背景的](../Page/宇宙世纪.md "wikilink")3DCG影片，以故事／纪录片的形式，描述[一年戰爭中](../Page/一年戰爭.md "wikilink")[吉恩軍兵器開發部門與](../Page/吉恩公国.md "wikilink")“第603技术试验部隊”所测试的一些“失敗品”的故事。第一部為**《一年战争秘录》**。2005年底發售三話DVD，並且再追加企畫三話OVA，名為**《默示录0079》**，描寫主角們在大戰末期最終的戰鬥。

目前，此系列的第二部**《MS IGLOO 2
重力战线》**的第一话《向那死神射击！》已于2008年10月24日发布，第二话《陆地王者，前进！》于2009年1月23日面世，第三话《敖德萨，钢铁风暴！》于2009年4月24日销售。《重力战线》将站在[联邦军的视角描述故事](../Page/地球联邦.md "wikilink")。

## 故事

一年戰爭時期，[吉翁公國以MS為首的各種新兵器向](../Page/吉翁公國.md "wikilink")[地球聯邦挑起獨立戰爭](../Page/地球聯邦.md "wikilink")，雖然相繼投入的各種新型機看來百花齊放，但是其背後卻是不為人知的殘酷現實。而公國軍兵器開發局所屬「第603技術實驗中隊」在戰場中進行了各項新兵器的測試，並且看著吉翁從勢如破竹到最後的敗北，他們最後的戰鬥命運將會是如何……

## 作品解說

本作品是以第603技术试验部队所屬的**奧利弗·麥伊**技術中尉的觀點來看吉翁軍試作兵器以及其開發者的悲喜小故事，作品的視點與描寫都是完全的吉翁觀點。另外本作也是繼[GUNDAM
EVOLVE系列之後第二部完全以](../Page/GUNDAM_EVOLVE.md "wikilink")3D電腦繪圖來表現的鋼彈系列映像作品。

## 主要工作人員

  - 總監督：[今西隆志](../Page/今西隆志.md "wikilink")
  - 腳本：大熊朝秀，大野木 宽
  - 音樂：大橋　惠
  - 總合設定：[出渕裕](../Page/出渕裕.md "wikilink")、[カトキハジメ](../Page/角木肇.md "wikilink")、[荒牧伸志](../Page/荒牧伸志.md "wikilink")、[山根公利](../Page/山根公利.md "wikilink")、[藤岡建機](../Page/藤岡建機.md "wikilink")
  - 設定考證：永瀬唯
  - CG監督：小畑正好
  - 製作：[Sunrise](../Page/Sunrise.md "wikilink")

## 主題曲

  - 『』
    作詞 - 菜穂／作曲、編曲、主唱 - Taja
    《1年戦争秘録》主題歌。
  - 『』
    作詞 - 菜穂／作曲、編曲、主唱 - Taja
    《黙示録0079》主題歌。
  - 『』
    編曲 - 西田マサラ／作曲、作詞、主唱 - 横田はるな
    《重力戦線》第1话《あの死神を撃て！》主题歌
  - 『』
    主唱 - 柿岛伸次
    《重力戦線》第2话《陸の王者、前へ！》主题歌
  - 『』
    作詞 - 菜穂／作曲、編曲、主唱 - Taja
    《重力戦線》第3话《オデッサ、鉄の嵐！》主题歌

## 各集標題（下附各集出现的测试兵器及战死机师姓名）

### MS IGLOO 一年戰爭秘錄

1.    -   - （声優：[宝亀克寿](../Page/宝亀克寿.md "wikilink")）

2.    -   - （声優：[天田益男](../Page/天田益男.md "wikilink")）

          - [地球聯邦中佐](../Page/地球聯邦.md "wikilink")（声優：[中田讓治](../Page/中田讓治.md "wikilink")）

3.    -   - 让·吕克·杜瓦尔少校（）（声優：[土師孝也](../Page/土師孝也.md "wikilink")）
          - Otchinan Shell（）（死于兹达3号机发动机失控引起的机体解体）

### MS IGLOO 默示錄0079

1.  （2006/04/26）

      -   - 威路拿·荷爾拜因（）少尉（声優：[堀内賢雄](../Page/堀内賢雄.md "wikilink")）

2.  （2006/06/23）

      -   - 欧文·凯迪拉克（）（声優：[相田さやか](../Page/相田さやか.md "wikilink")）

3.  （2006/08/25）

      - （非测试兵器，于《越過光芒之巔》登场）

          - 赫伯特·冯·卡斯本大校（）（声優：[澤木郁也](../Page/澤木郁也.md "wikilink")）（非测试驾驶员，于《越過光芒之巔》登场）

      -   - 奥利弗·梅技术中尉（）（声優：[石川英郎](../Page/石川英郎.md "wikilink")）（未战死）

### MS IGLOO 2 重力戰線

（声優：[东地宏树](../Page/东地宏树.md "wikilink")）

1.    -   - （声優：[寺杣昌纪](../Page/寺杣昌纪.md "wikilink")）

          - （声優：[桧山修之](../Page/桧山修之.md "wikilink")）

2.    - （非测试兵器）

          - （声优：[矶部勉](../Page/矶部勉.md "wikilink")）

          - （声优：[小西克幸](../Page/小西克幸.md "wikilink")）（未战死）

3.    -   - （声優：[井上喜久子](../Page/井上喜久子.md "wikilink")） （死于己方炮火）

          - （声優：[楠大典](../Page/楠大典.md "wikilink")）

          - （声優：[伊藤健太郎](../Page/伊藤健太郎.md "wikilink")）

## 漫畫

由[MEIMU作畫](../Page/MEIMU.md "wikilink"),"1年戰爭祕錄"的漫畫版:《机动战士高达 MS IGLOO
603》於[GUNDAM
ACE上連載](../Page/GUNDAM_ACE.md "wikilink").漫畫中追加了动画版所沒有的一些兵器的故事(但是OVA第二話則沒有在漫畫中出現).第二期的OVA發售同時，漫畫也重開連載，名字與OVA版一樣為《機動戰士鋼彈
MS IGLOO 默示錄0079》。第二部的漫画版名称与OVA版一样为《机动战士高达 MS IGLOO 2 重力战线》。

沒被OVA化的故事，機體及人物有:

1.  在所罗门飞翔的蝙蝠（）（收錄於單行本第一卷第3話-第5話）

      -   - 登場人物:

2.  ……（收錄於單行本第一卷第7話-第二卷第8話）

      - QEX-04M 艾吉爾（）

      -
      -   - 登場人物:

3.  視線貫穿的前方（）（收錄於單行本第二卷第11話-第12話）

      - YOP-04 [巴罗尔](../Page/巴羅爾.md "wikilink")
          - 登場人物:

4.  （收錄於單行本第二卷第13話）

      -   -
          -
          -
## 小說

《一年戰爭秘錄》與《默示錄0079》的小說由[林讓治執筆](../Page/林讓治.md "wikilink")。

## 外部連結

  - [MS IGLOO官方網站](http://www.msigloo.net/)

  - [MS IGLOO2官方网站](http://www.msigloo2.net/)

[Ms Igloo](../Category/宇宙世紀.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:2004年日本劇場動畫](../Category/2004年日本劇場動畫.md "wikilink")
[Category:2006年日本OVA動畫](../Category/2006年日本OVA動畫.md "wikilink")
[Category:2008年日本OVA動畫](../Category/2008年日本OVA動畫.md "wikilink")
[Category:2005年日本小說](../Category/2005年日本小說.md "wikilink")
[Category:2006年日本小說](../Category/2006年日本小說.md "wikilink")
[Category:角川Sneaker文庫](../Category/角川Sneaker文庫.md "wikilink") [Ms
Igloo](../Category/GUNDAM_ACE.md "wikilink") [Ms
Igloo](../Category/GUNDAM系列.md "wikilink")