[Tsz_Shan_Monastery.jpg](https://zh.wikipedia.org/wiki/File:Tsz_Shan_Monastery.jpg "fig:Tsz_Shan_Monastery.jpg")

**慈山寺**是[香港一座大型](../Page/香港.md "wikilink")[佛寺](../Page/佛寺.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[大埔區](../Page/大埔區.md "wikilink")[汀角及](../Page/汀角.md "wikilink")[船灣附近的](../Page/船灣淡水湖.md "wikilink")[洞梓](../Page/洞梓.md "wikilink")，佔地46,764平方米，建築樓面面積達5,100平方米，[李嘉诚捐资](../Page/李嘉诚.md "wikilink")15億港元興建，於2014年竣工，2015年4月啟用。\[1\]

## 設施

慈山寺分三個區域，中間是寺殿、僧寮和觀音像則分別座落在兩旁。慈山寺採取了低調建築、顏色、材料及比例都配合自然環境，以求營造修行氣氛；當中大雄寶殿的建築比例乃依從[唐朝佛寺](../Page/唐朝.md "wikilink")，屋頂不高，尺度配合[八仙嶺的高度](../Page/八仙嶺.md "wikilink")。在大雄寶殿前，大庭院的青銅燈籠則參照了位於[日本](../Page/日本.md "wikilink")[奈良的](../Page/奈良市.md "wikilink")[東大寺的作品](../Page/東大寺.md "wikilink")。
慈山寺的設施包括：

  - 一座76米高（相當於約25層樓）的戶外[青銅](../Page/青銅.md "wikilink")、[合金](../Page/合金.md "wikilink")[觀音像](../Page/觀音.md "wikilink")：為全球第二高，僅次於108米高的[海南](../Page/海南.md "wikilink")[南山海上觀音聖像](../Page/南山海上觀音聖像.md "wikilink")，坐北向南，與位於[大嶼山的](../Page/大嶼山.md "wikilink")[天壇大佛遙遙相對](../Page/天壇大佛.md "wikilink")。
  - 一座佔地約1,027平方米樓高18米的大雄寶殿：供奉有釋迦牟尼佛、藥師佛及阿彌陀佛，佛頂有金蓮花華蓋。佛像後的空間有大幅壁畫，取材自[敦煌石窟](../Page/敦煌石窟.md "wikilink")，又擺放了國學大師[饒宗頤手筆的](../Page/饒宗頤.md "wikilink")《[心經](../Page/心經.md "wikilink")》。
  - 一座佔地約333平方米的觀音殿
  - 三座佔地共約1,350平方米的僧寮，樓高兩層，一樓有5間房，全部均設有獨立洗手間\[2\]。
  - 110個私家車停泊位和13個[旅遊巴士停泊位](../Page/旅遊巴士.md "wikilink")

## 歷史

[Tsz_Shan_Monastery_under_construction_201210.JPG](https://zh.wikipedia.org/wiki/File:Tsz_Shan_Monastery_under_construction_201210.JPG "fig:Tsz_Shan_Monastery_under_construction_201210.JPG")

2003年12月，由[香港佛教聯合會會長](../Page/香港佛教聯合會.md "wikilink")[覺光法師擔任社長](../Page/覺光法師.md "wikilink")、[長江實業主席](../Page/長江實業.md "wikilink")[李嘉誠擔任名譽顧問的](../Page/李嘉誠.md "wikilink")[香海正覺蓮社提出了發展慈山寺的計劃](../Page/香海正覺蓮社.md "wikilink")。有關項目主要為了傳揚[佛教](../Page/佛教.md "wikilink")，提供靜修環境為目的\[3\]\[4\]，並且保證不會作為[骨灰龕或者發展成為旅遊發展項目](../Page/骨灰龕.md "wikilink")。計劃曾經引起[環境保護團體以及部份區內居民的關注](../Page/環境保護.md "wikilink")，例如需要砍伐山林，並且擔心區內唯一出入的道路[汀角路不能夠應付其落成後所增加的交通流量](../Page/汀角路.md "wikilink")，惟經過覺光法師及工程設計人員在[區議會及](../Page/區議會.md "wikilink")[城市規劃委員會解說後](../Page/城市規劃委員會.md "wikilink")，此計劃獲[民政事務局及](../Page/民政事務局.md "wikilink")[規劃署同意](../Page/規劃署.md "wikilink")，於2006年6月獲得[城市規劃委員會批准](../Page/城市規劃委員會.md "wikilink")\[5\]。2008年7月，[香港政府以](../Page/香港政府.md "wikilink")7,121萬港元撥地予正覺蓮社修築慈山寺及觀音像，土地有效期達50年。

2015年4月15日起，慈山寺接待首批400名透過網站預約的參觀者\[6\]。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - 居民巴士

</div>

</div>

## 參見

  - [寶蓮寺](../Page/寶蓮寺.md "wikilink")
  - [天壇大佛](../Page/天壇大佛.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [慈山寺官方網址](http://www.tszshan.org/home/)

[分類:香港建築之最](../Page/分類:香港建築之最.md "wikilink")

[Category:香港佛寺](../Category/香港佛寺.md "wikilink") [Category:大埔
(香港)](../Category/大埔_\(香港\).md "wikilink")
[Category:李嘉诚](../Category/李嘉诚.md "wikilink")

1.
2.  [15億慈山寺 4‧15起開放](http://orientaldaily.on.cc/cnt/news/20150330/00176_040.html)
    《東方日報》 2015年3月30日
3.  [慈山寺主為市民
    提供淨心清幽地](https://hk.news.yahoo.com/%E6%85%88%E5%B1%B1%E5%AF%BA%E4%B8%BB%E7%82%BA%E5%B8%82%E6%B0%91-%E6%8F%90%E4%BE%9B%E6%B7%A8%E5%BF%83%E6%B8%85%E5%B9%BD%E5%9C%B0-214700516.html)
    《星島日報》 2013年9月18日
4.  [15億建慈山寺：百分百為港人](http://orientaldaily.on.cc/cnt/news/20130918/00176_013.html)
    《東方日報》 2013年9月18日
5.  。〈城規-{准}-建大埔觀音 維持逾百車位捱轟〉，《明報》，2006年6月3日
6.  [慈山寺昨起接待預約參觀者](http://orientaldaily.on.cc/cnt/news/20150416/00176_061.html)
    《東方日報》 2015年4月16日