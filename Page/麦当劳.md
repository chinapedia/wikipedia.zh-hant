<div style="display:none">

\-{A|zh-hans:意式; zh-hk:意式; zh-tw:義式;}- -{A|zh-hans:小飞飞; zh-hk:小飛飛;
zh-tw:大鳥姐姐;}- -{A|zh-hans:滑溜溜; zh-hk:滑嘟嘟; zh-tw:奶昔大哥;}- -{A|zh-cn:沙拉;
zh-hk:沙律; zh-tw:沙拉;}- -{A|zh-hans:经典大早餐; zh-hk:精選早晨套餐; zh-tw:經典大早餐;}-
-{A|zh:鷄; zh-hans:鸡; zh-hant:雞;}-

</div>

[McDonaldsHQIL.jpg](https://zh.wikipedia.org/wiki/File:McDonaldsHQIL.jpg "fig:McDonaldsHQIL.jpg")

**麥當勞**（）是源自[美國](../Page/美國.md "wikilink")[南加州的](../Page/南加州.md "wikilink")[跨國](../Page/跨國公司.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[快餐店](../Page/快餐店.md "wikilink")，也是全球最大的快餐連鎖店，主要販售[漢堡包及](../Page/漢堡包.md "wikilink")[薯條](../Page/薯條.md "wikilink")、[炸雞](../Page/炸雞.md "wikilink")、[汽水](../Page/汽水.md "wikilink")、[冰品](../Page/冰品.md "wikilink")、[沙拉](../Page/沙拉.md "wikilink")、[水果](../Page/水果.md "wikilink")、[咖啡等](../Page/咖啡.md "wikilink")[快餐食品](../Page/快餐.md "wikilink")，目前總部位於美國[芝加哥近郊的](../Page/芝加哥.md "wikilink")。

麦当劳之企業版圖遍布全球六[大洲](../Page/大洲.md "wikilink")119個[國家](../Page/國家.md "wikilink")，截至2017年4月，麥當勞在全球擁有約3萬6千間[分店](../Page/商店.md "wikilink")\[1\]\[2\]，是全球餐飲業中知名度最高的[品牌](../Page/品牌.md "wikilink")，在很多国家代表着[美国文化](../Page/美国文化.md "wikilink")。其購買[開心樂園餐免费赠送](../Page/開心樂園餐.md "wikilink")[玩具等](../Page/玩具.md "wikilink")[銷售策略对](../Page/市场营销.md "wikilink")[儿童族群更是具有吸引力](../Page/儿童.md "wikilink")。由於其規模與影響力跨越國界，麦当劳已是公眾討論關於食物導致肥胖、[企業倫理和消費](../Page/企業倫理.md "wikilink")[責任焦點等議題之焦點](../Page/責任.md "wikilink")；其商品被指影响公众[健康](../Page/健康.md "wikilink")，例如高热量导致[肥胖以及缺乏足够均衡营养等](../Page/肥胖.md "wikilink")，因而被抨击为[垃圾食品](../Page/垃圾食品.md "wikilink")。部分國家甚至视其为美国生活方式入侵之象徵。

**麥當勞**在[中國大陸譯名原本是](../Page/中國大陸.md "wikilink")“麥克唐納快餐”\[3\]，直到該品牌被引入中國大陸之後才統一採用現今的[粵語譯名](../Page/粵語.md "wikilink")。值得一提的是，“McDonald's”一詞直譯成粵語本為“麥當奴”，因“奴”字不雅，改成其粵語同音字“勞”。引入新資本的麥當勞（中國）公司2017年以其招牌標誌“**金拱门**”
（）為公司新名稱\[4\]，但保留麥當勞品牌於旗下餐廳使用。而[港澳地區和](../Page/港澳地區.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[粵語區又常將其稱為](../Page/粵語區.md "wikilink")**M記**、**麥記**或**老麥**。[台灣則是隨著麥當勞出售台灣麥當勞所有股權後](../Page/台灣.md "wikilink")，更名為「**和德昌**」公司。\[5\]

## 吉祥物

  - [麥當勞叔叔](../Page/麥當勞叔叔.md "wikilink")（Ronald
    McDonald）：一個穿著黃色衣服的「小丑」人物形象。
  - 漢堡神偷（Hamburglar）：穿著黑白間條衣服、眼綁黑帶的人物形象。
  - \-{zh:大鳥姊姊;zh-hans:大鳥姊姊;zh-hk:小飛飛;zh-tw:大鳥姊姊}-（Birdie the Early
    Bird）：一隻會飛的黃色母雞之卡通形象 。
  - \-{zh:奶昔大哥;zh-hans:奶昔大哥;zh-hk:滑嘟嘟;zh-tw:奶昔大哥}-（The
    Grimace）：蕃薯形狀、全身紫色的人物形象。

## 歷史

1940年，理查·麥當勞（Richard -{“}-Dick-{”}- J.
McDonald，生卒：1909年2月16日－1998年7月14日）與莫里斯·麦当劳（Maurice
-{“}-Mac-{”}-
McDonald，生卒：1902年11月26日－1971年12月11日）兄弟在美國加州的[聖貝納迪諾創立](../Page/聖貝納迪諾_\(加利福尼亞州\).md "wikilink")「麥當勞燒烤」餐廳（McDonald's
Bar-B-Que），開始了今日麥當勞之歷史。

## 公司概况

[McDonalds_in_Moncton.jpg](https://zh.wikipedia.org/wiki/File:McDonalds_in_Moncton.jpg "fig:McDonalds_in_Moncton.jpg")分店建築\]\]
麦当劳快餐厅大多提供柜台式和[得来速式](../Page/得来速.md "wikilink")（drive-through的音译，即指不下车便能够用餐店快餐服务。顾客可驾车在门口点菜，然后绕过餐厅，在出口处取餐）两种服务，同时提供室内就餐，有时也提供室外座位。

“Drive-Thru”、“Auto-Mac”、“Pay and
Drive”和“McDrive”在很多国家都很出名，这些服务一般会分开点餐处、付款处、取餐处，虽然后两者多数是一起的。有些地区，公路干道两旁会设有**麦大道***（McDrive）*是一种无柜台无座位，专为驾车人士开设的大道，这种大道往往以得来速餐厅简化方式，出现在闹区等人口密集地带。反之，人口较松散地方就没有[得來速](../Page/得來速.md "wikilink")（drive-through）服务。亦有部份位于闹市麦当劳，会“Walk-Thru”取代“Drive-Thru”。

（有消息称，由于美国[北达科他州员工最低工资明显低于](../Page/北达科他州.md "wikilink")[俄勒冈州和](../Page/俄勒冈州.md "wikilink")[华盛顿州](../Page/华盛顿州.md "wikilink")，麦当劳公司计划在2005年初尝试着在北达科他州法戈设立电话服务中心，用来接受处理来自俄勒冈州和华盛顿州的得来速电话订单，以缓解这种现象。）

麦当劳也有专门主题餐厅，例如1950年[摇滚主题餐厅](../Page/摇滚.md "wikilink")、2007年[英格兰超级联赛主题餐厅](../Page/英格兰超级联赛.md "wikilink")、一些坐落在近郊及某些城市大型室内或室外游乐场的餐厅。它们被称作[麦当劳乐园](../Page/麦当劳乐园.md "wikilink")（McDonald's
PlayPlace，旧称PlayLand），最早于1970年代和1980年代在美国本土出现，加拿大大部分地区直到1990年代中期才陆续出现。在国际上較遲出現。有些[郊区和部份城市麦当劳有设立内部或外部](../Page/郊区.md "wikilink")[游乐场](../Page/游乐场.md "wikilink")，称为“麦当劳乐园”。第一个麦当劳乐园于1987年问世，设计爬行管内有球和溜滑梯，而且越来越多。

麦当劳每年会将部分营业额用于[慈善事业](../Page/慈善.md "wikilink")。创始人雷·克洛克去世时，以他全部财产成立麦当劳叔叔慈善基金，在部份医院附近设立“[麦当劳之家](../Page/麦当劳之家.md "wikilink")”，以提供重症病童及家属就医期间暂住。

麦当劳和[可口可乐结成战略伙伴](../Page/可口可乐.md "wikilink")，固定销售[可口可乐公司](../Page/可口可乐公司.md "wikilink")[汽水](../Page/汽水.md "wikilink")。

### 形象宣傳

[Batala_Colony_Faisalabad_McDonalds.jpg](https://zh.wikipedia.org/wiki/File:Batala_Colony_Faisalabad_McDonalds.jpg "fig:Batala_Colony_Faisalabad_McDonalds.jpg")的[費薩拉巴德分店](../Page/費薩拉巴德.md "wikilink")\]\]
在约1965年，麦当劳推出第一个麦当劳[电视广告](../Page/电视广告.md "wikilink")，自当时起，麦当劳公司推出了不同类型的电视广告结构，唯一一样的是年份，麦当劳所有电视广告都有一行字标示著年份，结构如下：

  - 1965年－1975年──（只在最后标示年份）
  - 1975年－1979年──上面一个影像框，中间一个麦当劳标志，底下一句口号（如：We do it all for you）
  - 1979年－1984年（中）──一个大双线黄框，黄框最底标示著口号（某些国家地区例外，如：Nobody can do it like
    McDonald's can），右下角一个麦当劳标志
  - 1984年－1985年──右下角一个麦当劳标志和口号
  - 1984年－2000年年──右下角一个以红色长方形框的麦当劳标志和口号
  - 1980年代末－1995年──左/右下角一个麦当劳标志和口号，犹如在街上看到的大麦当劳标志甚似（只局限在某些国家和地区，例：美国，台湾）
  - 2000年－2003年9月──一个微笑着的长方形M字和口号（亚洲某地区地区例外，如香港和台湾，则以一个微笑着的圆M字代表，Every
    time a good time）
  - 2003年9月至今──一个大M字和口号（通常在最后才出现）

直到现在，麦当劳已在美国的广告用了23个不同的口号，有些更是其国家地区的精选口号。但同时它某些广告活动也带来不少问题。

1996年，英国成人漫画杂志控告麦当劳抄袭其存在已久的Top Tips特征的名字和形式，读者也发起其讽刺重点。麦当劳用相同名字Top
Tips来作为宣传活动的名（意思是节省钱的话，就来麦当劳）。类似的部份的几乎逐字翻译的：

  -
    “Save a fortune on laundry bills. Give your dirty shirts to Oxfam.
    They will wash and iron them, and then you can buy them back for
    50p.”──Viz Top Tip出版于1989年5月
    “Save a fortune on laundry bills. Give your dirty shirts to a
    second-hand shop. They will wash and iron them, and then you can buy
    them back for 50p.”──麦当劳1996年的广告。

香港/澳門麥當勞早期广告以[麦当劳叔叔](../Page/麦当劳叔叔.md "wikilink")、[汉堡神偷](../Page/汉堡神偷.md "wikilink")、[小飞飞](../Page/小飞飞.md "wikilink")、[滑嘟嘟四个](../Page/滑嘟嘟.md "wikilink")[公仔为主角](../Page/公仔.md "wikilink")，曾拍摄不少的故事性广告，吸引不少小朋友，例如引用美国麥當勞广告改成粤语版本，或者引用美国麥當勞广告，找一些演员来當麥當勞广告人物（以代替外国人），1985年曾推出“好梦成真”，1986年推出“二人行”等。麥當勞把很多相关玩具加入[开心乐园餐](../Page/开心乐园餐.md "wikilink")，故这四个角色可謂与那個年代小朋友一起成长。惟在[香港回歸后](../Page/香港回歸.md "wikilink")，除了麥當勞叔叔外，其余角色都很少出现。

### 麦当劳标志

[Mcdonald's_logo.svg](https://zh.wikipedia.org/wiki/File:Mcdonald's_logo.svg "fig:Mcdonald's_logo.svg")

  - 黄色M字。（但美國亞利桑拿州塞多納市使用藍綠色M字）
  - [麦当劳叔叔](../Page/麦当劳叔叔.md "wikilink")：穿著黃色衣服，袖子有红白相间条纹的「小丑」打扮。
  - 汉堡神偷：穿著黑白間條衣服，眼綁黑帶的囚犯造型。
  - 大鳥姐姐-{zh-hans:：; zh-tw:（香港稱小-{}-飛飛）：; zh-hk:：;}-一隻帶著飛行帽會飛的鴨女，大鳥姐姐。
  - 奶昔大哥-{zh-hans:：; zh-tw:（香港稱滑-{}-嘟嘟）：; zh-hk:：;}-紫色番薯形象的人物，奶昔大哥。
  - [我就喜欢](../Page/我就喜欢.md "wikilink")(i'm lovin'
    it)：近期沿用口號，英文版為其一**McDonald's**註冊商標。
  - 脆卜卜：好像啦啦球樣子，主色是粉紅色。

麦当劳公司[-{zh-cn:商业模式;zh-tw:商業模型;zh-hk:商業模式}-与大部分速食连锁店有点不同](../Page/商業模式.md "wikilink")。除了普通[特許費外](../Page/加盟連鎖.md "wikilink")，也提供租借。
[A_McDonald's_logo_in_Taiwan.jpg](https://zh.wikipedia.org/wiki/File:A_McDonald's_logo_in_Taiwan.jpg "fig:A_McDonald's_logo_in_Taiwan.jpg")
麥當勞的「M」字以象徵明亮、活潑、希望的黃色作顏色，可說是家喻戶曉。行銷心理專家路易斯·契斯金 (Louis
Cheskin)，他認為這對「金拱門」()可以引起消費者「母親雙乳」的潛意識聯想。\[6\]

## 業務拓展全球

麦当劳已成为[全球化标记](../Page/全球化.md "wikilink")，也被指是社会“[麦当劳化](../Page/麦当劳化.md "wikilink")”一個國家麥當勞價位也反映著這個國基經濟與消費能力。\[7\]英国《[经济学人](../Page/经济学人.md "wikilink")》[杂志更用](../Page/杂志.md "wikilink")“[巨无霸指数](../Page/巨无霸指数.md "wikilink")”──一个[巨无霸在不同地方](../Page/巨无霸.md "wikilink")[货币价格比较](../Page/货币.md "wikilink")──来非正式评估这些货币[购买力平价](../Page/购买力平价.md "wikilink")。因为麦当劳几乎等于美国文化和生活方式，其国际商业扩张已被称为[美国化部份和美国](../Page/美国化.md "wikilink")[文化帝国主义](../Page/文化帝国主义.md "wikilink")。因此麦当劳是世界各地[反全球化人士抗议目标之一](../Page/反全球化.md "wikilink")。

### 香港

[McDonald's_Next_in_Hong_Kong_Admiralty_Centre_201512.jpg](https://zh.wikipedia.org/wiki/File:McDonald's_Next_in_Hong_Kong_Admiralty_Centre_201512.jpg "fig:McDonald's_Next_in_Hong_Kong_Admiralty_Centre_201512.jpg")[金鐘](../Page/金鐘.md "wikilink")[海富中心麥當勞分店翻新而成](../Page/海富中心.md "wikilink")，特點為提供59[港元起的自選漢堡](../Page/港元.md "wikilink")、40元起的自選[沙律和晚上送餐服務](../Page/沙律.md "wikilink")（晚上6點前則限送漢堡），兼免費電話充電供顧客使用。製作食物的地方改用開放式設計，顧客可透過櫥窗玻璃揀選漢堡或沙律配料，製作沙律員工均經過720小時特訓以確保水準，全店裝修費不菲。\[8\]\]\]

  - 1975年1月8日，[伍日照博士在香港引入首間麥當勞餐廳於](../Page/伍日照.md "wikilink")[香港島](../Page/香港島.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[百德新街](../Page/百德新街.md "wikilink")[恒隆中心開設首家分店](../Page/恒隆中心.md "wikilink")。
  - 1976年8月15日，[九龍第一間麥當勞在](../Page/九龍.md "wikilink")[油麻地](../Page/油麻地.md "wikilink")[彌敦道](../Page/彌敦道.md "wikilink")363-373號恆成大廈地庫開設分店。該分店在2018年4月1日關閉\[9\]，同年第四季翻新後在原址地面重開。
  - 1977年，香港麥當勞售出第100萬個漢堡包。\[10\]
  - 1978年11月18日，[新界第一間麥當勞在](../Page/新界.md "wikilink")[荃灣街市街開設分店](../Page/荃灣街市街.md "wikilink")。
  - 1981年10月18日，[觀塘](../Page/觀塘.md "wikilink")[裕民坊麥當勞打破全球麥當勞單日最多惠顧人次紀錄](../Page/裕民坊.md "wikilink")。\[11\]\[12\]
  - 1984年香港麥當勞成為全球首個供應全線早餐食品（早晨全餐、熱香餅、薯餅）的市場。\[13\]
  - 1987年，[尖沙咀](../Page/尖沙咀.md "wikilink")[北京道分店地庫及地下部分食區正式通宵營業](../Page/北京道.md "wikilink")，成为全港首間24小时通宵營業麥當勞分店。\[14\]
  - 1989年4月23日，全球第11,000間麥當勞分店於[杏花邨開業](../Page/杏花邨.md "wikilink")。
  - 1992年，全球最繁忙的十大麥當勞，香港佔七間，包括[星光行](../Page/星光行.md "wikilink")、[德福廣場](../Page/德福廣場.md "wikilink")、[沙田](../Page/沙田.md "wikilink")[新城市廣場及](../Page/新城市廣場.md "wikilink")[裕民坊等](../Page/裕民坊.md "wikilink")。\[15\]
  - 1993年12月20日，於[大嶼山](../Page/大嶼山.md "wikilink")[愉景灣開設分店](../Page/愉景灣.md "wikilink")，是[離島區和](../Page/離島區.md "wikilink")[大嶼山第一間麥當勞分店](../Page/大嶼山.md "wikilink")。
  - 1995年12月，推出「[開心樂園餐](../Page/開心樂園餐.md "wikilink")」。
  - 1996年3月，香港第100間麥當勞分店於[小西灣開業](../Page/小西灣.md "wikilink")。
  - 1996年9月，全亞洲首間[麥當勞叔叔之家在香港成立](../Page/麥當勞叔叔之家.md "wikilink")。
  - 1999年，首間[McCafé於](../Page/McCafé.md "wikilink")[上環麥當勞分店開業](../Page/上環.md "wikilink")。
  - 2002年，成立香港[漢堡大學](../Page/漢堡大學_\(麥當勞\).md "wikilink")\[16\]\[17\]。
  - 2004年，推出「清新滋選」食品。
  - 2005年，成立麥樂會和推出板燒雞腿包。
  - 2006年7月15日，[金鐘](../Page/金鐘.md "wikilink")[海富中心麥當勞分店成為首間有無線上網的麥當勞餐廳](../Page/海富中心.md "wikilink")，也成為全港最大24小時餐廳，是第2間在香港開幕時已有McCafe和24小時營業餐廳。
  - 2007年，成立麥當勞媽咪會。
  - 2008年，McCafé優質即磨咖啡於麥當勞全線推出。
  - 2009年，推出[麥麥送餐點外送速遞服務](../Page/麥麥送.md "wikilink")。同時引進「即時點‧即時製」模式。
  - 2010年8月19日，麥當勞在香港發行2億元[人民幣債券](../Page/人民幣債券.md "wikilink")，成為首家在香港發行人民幣債券的外資跨國企業。
  - 2011年，全球首創麥當勞婚禮派對服務正式推出，吸引世界各地的傳媒報導。
  - 2013年4月推出「麥飯卷」及「麥趣飯」兩款新食品，但已經在福喜黑心肉事件後停止供應。
  - 2015年12月9日，[金鐘](../Page/金鐘.md "wikilink")[海富中心麥當勞分店經過裝修後](../Page/海富中心.md "wikilink")，成為全球首間McDonald's
    Next餐廳\[18\]，它採用了類似酒店餐廳的全新裝潢，餐廳廚房採用半開放設計，顧客可透過玻璃櫥窗觀看廚房運作情況，該餐廳同時提供一般餐點、McCafé及自選漢堡或沙律。
  - 2017年1月9日，中信股份宣布與中信資本、美國凱雷投資集團和麥當勞成立新公司，成為麥當勞未來20年在中國大陸和香港的主要特許經營商。
  - 2017年3月9日，全線推出「The Signature
    Collection」系列共三款漢堡並長期供應，同時全線停售[至尊漢堡](../Page/至尊漢堡.md "wikilink")、[足三兩及板燒雞腿包](../Page/足三兩.md "wikilink")（板燒雞腿包在2018年重新推出）。另外，於同日在60間分店推出「My
    Signature」自選漢堡服務及「Experience of the Future」新一代餐飲服務，包括晚上六點後提供送餐服務等服務。
  - 2017年8月8日，麦当劳完成对麦当劳在中国大陆以及香港的运营改革。麦当劳在中国大陆以及香港的业务将交由新成立的[麦当劳中国负责](../Page/麦当劳中国.md "wikilink")，而新公司由[中信股份和中信资本](../Page/中信股份.md "wikilink")（共52%）以及[凯雷投资](../Page/凯雷集团.md "wikilink")（28%）和麦当劳（20%）共同持有。\[19\]
  - 2017年8月，香港麥當勞的公司名稱由「香港麥當勞有限公司」或「McDonald's Restaurants（Hong
    Kong）」更名為「MHK Restaurants Limited」或「MHK Restaurants
    Limited（a McDonald's Franchised Business）」

### 台灣

[McDonalds_red_packet.JPG](https://zh.wikipedia.org/wiki/File:McDonalds_red_packet.JPG "fig:McDonalds_red_packet.JPG")
[臺灣麥當勞餐廳內的顧客.jpg](https://zh.wikipedia.org/wiki/File:臺灣麥當勞餐廳內的顧客.jpg "fig:臺灣麥當勞餐廳內的顧客.jpg")

  - 1984年1月28日，臺灣第一間麥當勞開設在[台北市松山區](../Page/台北市松山區.md "wikilink")[民生東路三段](../Page/民生東路.md "wikilink")，由[孫大強家族引進](../Page/孫大強.md "wikilink")，與麥當勞總公司合資設立「台灣麥當勞餐廳股份有限公司」，並由[孫大偉擔任首任](../Page/孫大偉_\(企業家\).md "wikilink")[董事長](../Page/董事長.md "wikilink")，雙方各持股一半，當時造成極大轟動，曾創下麥當勞單週營業的世界紀錄，間接讓台灣餐飲業邁入新紀元。麥當勞將其理念Q（Quality，質量、品質）、S（Service，服務）、C（Cleanliness，衛生、清潔）、V（Value，價值）帶入台灣，為當時台灣餐飲業投下一顆震撼彈。而同年第二間位於[台北市萬華區昆明街](../Page/台北市萬華區.md "wikilink")（[西門町](../Page/西門町.md "wikilink")）開幕。
  - 1986年，台灣麥當勞推出有氧早餐系列，在西式速食餐飲界開設早餐時段餐點。
  - 1988年，台灣麥當勞推出快樂兒童餐，購買兒童餐加贈玩具。
  - 1993年，麥當勞正式決定接手孫大偉持股，全面介入臺灣麥當勞管理層面，形成7人集體領導模式，改派美籍人士羅威廉擔任總裁直接經營。
  - 2003年，引進全台第一間麥當勞McCafe'坐落在台北市天母東路與忠誠路交叉路口，正式跨足咖啡飲品產業。
  - 2006年，改變出餐模式，採用現點現做(_Make for you系統)，讓消費者能享用熱騰騰且新鮮的餐點。
  - 2006年6月1日起，在全台99家麥當勞得來速施行24小時營業，提供消費者一個享用消夜的好去處。
  - 2010年1月，台灣麥當勞店內電視媒體由[前線媒體取得營運代理權並定名為](../Page/前線媒體.md "wikilink")「MyTV」。
  - 2013年1月21日起，臺灣麥當勞的24小時「麥當勞歡樂送」外賣專線「40-666-888」，交由香港[電訊盈科負責](../Page/電訊盈科.md "wikilink")，由電盈在[上海的客服中心接聽來電](../Page/上海.md "wikilink")。
  - 2015年麥當勞在台灣已有388家分店，並計劃兩年內擴展至500家。\[20\]
  - 2015年6月12日，麥當勞大中華區總裁曾啟山在台灣麥當勞公司的內部會議上，用英文表示母公司將轉手台灣麥當勞經營權，麥當勞在台灣將改採「發展式[特許經營](../Page/特許經營.md "wikilink")」模式，由新的投資方主導本地市場經營。\[21\]\[22\]\[23\]\[24\]

[McDonald's_Linsen_2nd_Restaurant_20161031.jpg](https://zh.wikipedia.org/wiki/File:McDonald's_Linsen_2nd_Restaurant_20161031.jpg "fig:McDonald's_Linsen_2nd_Restaurant_20161031.jpg")

  - 臺灣麥當勞總部位於臺北市中正區林森南路1號，與麥當勞林森二店相同。
  - 2016年12月21日，仰德集團二少東、[國賓大飯店總經理李昌霖以個人及家族力量投入約三億美元的資金](../Page/國賓大飯店.md "wikilink")，決定購入臺灣麥當勞的經營權\[25\]。
  - 2017年3月29日，台灣麥當勞經營權以新臺幣51億元賣給李昌霖\[26\]\[27\]，並於4月26日獲得[中華民國公平交易委員會核准](../Page/中華民國公平交易委員會.md "wikilink")\[28\]\[29\]。
  - 2017年5月31日，台灣麥當勞推出首張[儲值卡](../Page/儲值卡.md "wikilink")「麥當勞點點卡」，空卡售價新臺幣100元，但不適用於麥當勞歡樂送及部分門市。
  - 2017年6月1日，台灣麥當勞宣布，由李昌霖成立的「[德昱股份有限公司](../Page/德昱.md "wikilink")」正式接手台灣麥當勞經營權，成為麥當勞台灣市場的授權發展商\[30\]\[31\]。
  - 2017年8月，台灣麥當勞的公司名稱由「台灣麥當勞餐廳股份有限公司」更名為「和德昌股份有限公司」。\[32\]
  - 2018年8月31日，為慶祝台灣羽球國手戴姿穎在亞運會上奪得金牌，麥當勞推出限時優惠，單點大麥克買一送一，引起全台得排隊風潮，當日大麥克售罄後，彈性開放2號到19號主餐單點仍享有買一送一的優惠，而在優惠結束的隔天(2018年9月1日)麥當勞更是宣布10:30暫停營業一天，以慰勞前天大量人潮帶來員工忙碌抑或是加班的辛勞，不論在商品行銷或者是形象宣傳方面都達到非常好的效果。\[33\]

### 澳門

1987年4月，[澳門在香港麥當勞資金投資下於](../Page/澳門.md "wikilink")[水坑尾街開設了首家麥當勞分店](../Page/水坑尾街.md "wikilink")，也是當年葡萄牙國內首家麥當勞（至1991年，葡萄牙本土才出現首家麥當勞）。現時澳門有36間分店，其中十數間分店24小時營業，7間設有McCafe。澳門所有[堂區都已有麥當勞分店](../Page/堂區_\(行政區劃\).md "wikilink")。澳門首間[McCafé於](../Page/McCafé.md "wikilink")2006年於[新馬路開業](../Page/新馬路.md "wikilink")，但目前已經結業。

值得一提是，澳門麥當勞位於[澳門威尼斯人度假村酒店](../Page/澳門威尼斯人度假村酒店.md "wikilink")[金光綜藝館之分店只在有演唱會舉行時才營業](../Page/金光綜藝館.md "wikilink")，澳門威尼斯人度假村酒店員工餐廳分店只向威尼斯人屬下員工提供餐飲服務。

澳門麥當勞提供的食品及飲品大部分與香港麥當勞相同，但兩地的售價有別，雖然用[澳門元標價](../Page/澳門元.md "wikilink")，但接受[港元付款](../Page/港元.md "wikilink")；不過，在香港推出的麥當勞優惠券，澳門麥當勞均不適用。

### 中国大陸

[ShenZjen_Dongmen_cut.jpg](https://zh.wikipedia.org/wiki/File:ShenZjen_Dongmen_cut.jpg "fig:ShenZjen_Dongmen_cut.jpg")[東門商業區](../Page/東門商業區.md "wikilink")（圖中間）\]\]
1990年10月8日，麥當勞在[深圳市](../Page/深圳市.md "wikilink")[解放路寶华楼西华宫開設大陸首家分店](../Page/解放路.md "wikilink")，略晚於蘇聯同年1月於[莫斯科開出的首個社會主義地區分店](../Page/莫斯科.md "wikilink")。該餐廳有500個座位，由麥當勞（香港）有限公司的全資附屬公司投資4,000萬港元開設。在開幕儀式上，麥當勞（深圳）有限公司向深圳社会福利中心捐款15萬元。截至2011年12月，麦当劳（中國）有限公司营业额为81亿[人民币](../Page/人民币.md "wikilink")，拥有2,157家餐厅。

2014年2月，麥當勞暫時開放內地5個城市的加盟，分別為上海、深圳、[惠州](../Page/惠州.md "wikilink")、[四川省](../Page/四川省.md "wikilink")[成都和](../Page/成都.md "wikilink")[瀘州](../Page/瀘州.md "wikilink")。入場門檻為200萬元人民幣，須在簽約時一次付清；提交申請表時，必須提供擁有價值相當於200萬元現金的可變現資產證明。

2017年8月8日，麦当劳完成对麦当劳中国的运营改革。麦当劳在中国大陆以及香港的业务将交由新成立的麦当劳中国负责，新公司由[中信股份和中信资本合組公司](../Page/中信股份.md "wikilink")61.54%和38.46%股權的公司持有（共52%）以及[凯雷投资](../Page/凯雷集团.md "wikilink")（28%）和麦当劳（20%）共同持有。\[34\]同年10月12日，麦当劳（中国）确认，因业务发展需要，公司名称自即日起变更为“金拱门（中国）有限公司”，新公司同時營運中國大陸及香港業務，但餐厅名称、安全標準和營運流程等保持不变\[35\]，同時提出「願景2022」的加速發展計劃，對未來5年業務作極速擴張發展宏圖\[36\]。

### 各地麥當勞圖片

<File:William> House on Minsheng East Road
20150912.jpg|台灣第一家麦当劳分店，位于[台北民生东路](../Page/台北.md "wikilink")
[File:麥當勞台灣苗栗三義店.JPG|現今台灣部分麥當勞門市附設McCafe，並導入](File:麥當勞台灣苗栗三義店.JPG%7C現今台灣部分麥當勞門市附設McCafe，並導入)[24小時營業](../Page/24小時營業.md "wikilink")
[File:McChina.jpg|麦当劳"Paris](File:McChina.jpg%7C麦当劳%22Paris) - Choisy" -
位於[巴黎十三区華人街麥當勞分店](../Page/巴黎十三区.md "wikilink") <File:McDonald's・Tokyo>,
Japan 01.jpg|[日本](../Page/日本.md "wikilink")24小时麦当劳
[File:Mac_Japan.jpg|位於日本](File:Mac_Japan.jpg%7C位於日本)[大阪市麦当劳分店](../Page/大阪市.md "wikilink")
<File:McDonald's>,
Karlshamn.jpg|位于[瑞典](../Page/瑞典.md "wikilink")[卡尔斯港麦当劳分店](../Page/卡尔斯港.md "wikilink")
<File:The> McDonalds at
Guantanamo.jpg|[关塔那摩湾美军基地麦当劳分店](../Page/关塔那摩湾.md "wikilink")
<File:Mcdonold> hk
taiwo.jpg|位于香港[大埔](../Page/大埔_\(香港\).md "wikilink")[太和邨麦当劳分店](../Page/太和邨.md "wikilink")，不少香港麦当劳的[电视广告都于此取景](../Page/电视广告.md "wikilink")
<File:Choi> Ming Shopping Centre 2013 09
part5.JPG|香港[將軍澳](../Page/將軍澳新市鎮.md "wikilink")[彩明商場內的麦当劳分店](../Page/彩明商場.md "wikilink")
[File:McDonalds_in_St_Petersburg_2004.JPG|位于](File:McDonalds_in_St_Petersburg_2004.JPG%7C位于)[俄罗斯麦当劳分店](../Page/俄罗斯.md "wikilink")
<File:Macau> Venetian Macao
McDonalds.JPG|澳门威尼斯人度假村酒店[大运河购物中心内麦当劳分店](../Page/大运河购物中心.md "wikilink")
<File:McDonalds>
Thailand.jpg|位于[泰国分店麥當勞叔叔](../Page/泰国.md "wikilink")，都以雙手合十作為造型
<File:Mcdonalds>
seoul.JPG|[韓國](../Page/韓國.md "wikilink")[首爾麥當勞](../Page/首爾.md "wikilink")
<File:SGMcD.JPG>|[新加坡麥當勞](../Page/新加坡.md "wikilink") <File:Venezia-mc>
donalds.jpg|義大利[威尼斯在船上开设的麦当劳分店](../Page/威尼斯.md "wikilink")
<File:McDonalds> Sudurlandsbraut
North.jpg|[冰岛麥當勞分店](../Page/冰岛.md "wikilink")，已於2009年關閉

## 麦当劳產品

麥當勞在美國及全球各地主要販售[漢堡包](../Page/漢堡包.md "wikilink")，以及[薯條](../Page/薯條.md "wikilink")、[炸雞](../Page/炸雞.md "wikilink")、[汽水](../Page/汽水.md "wikilink")、[冰品](../Page/冰品.md "wikilink")、[沙拉](../Page/沙拉.md "wikilink")、[水果](../Page/水果.md "wikilink")、[咖啡等](../Page/咖啡.md "wikilink")[快餐食品](../Page/快餐.md "wikilink")。

為了迎合在世界各地民眾的口味，麥當勞也衍生許多不同特色的產品，如：蝦漢堡、[椰漿飯漢堡](../Page/椰漿飯.md "wikilink")(nasi
lemak burger)等。

## 特色

### 通宵营业

香港首間24小時麥當勞為[尖沙嘴](../Page/尖沙嘴.md "wikilink")[北京道分店](../Page/北京道.md "wikilink")，始於1987年。\[37\]

2003年7月，香港[湾仔](../Page/湾仔.md "wikilink")[莊士敦道麦当劳分店开始了](../Page/莊士敦道.md "wikilink")24小时营业。2006年6月21日後，[銅鑼灣廣場](../Page/銅鑼灣廣場.md "wikilink")2期分店是香港麥當勞史上首間一開幕就是24小時營業而且有咖啡館特色的分店，後來該種分店愈來愈多。至今香港已经有103间麦当劳分店实行24小时通宵营业。

而[澳门方面](../Page/澳门.md "wikilink")，[黑沙環](../Page/黑沙環.md "wikilink")[龍園分店的麦当劳于](../Page/龍園.md "wikilink")2007年开始设有24小时麦当劳，現時澳門有7間麦当劳实行24小时通宵营业。

在中国內地已经有过半数的麦当劳分店陆续实行24小时营业。

在臺灣已經有超過200間麥當勞分店实行24小时营业。

### 外賣速遞服務

麥當勞於2009年大力推廣其24小時外賣速遞服務，香港名為「[麥麥送](../Page/麥麥送.md "wikilink")」（取[粵語](../Page/粵語.md "wikilink")「密密送」諧音），台灣名為「[歡樂送](../Page/歡樂送.md "wikilink")」，中國大陸名為「[麥樂送](../Page/麥樂送.md "wikilink")」，並起用當地著名藝人為代言人。

香港推出該服務不久後即傳出「麥麥送」員工為[外判速遞公司](../Page/外判.md "wikilink")「域迅快遞有限公司」，以[自僱形式聘用](../Page/自僱.md "wikilink")，不受香港《[僱傭條例](../Page/僱傭條例.md "wikilink")》規管。員工無法享有勞工法例保障，包括[工傷](../Page/工傷.md "wikilink")、各項[假期及](../Page/假期.md "wikilink")[強積金等補償](../Page/強積金.md "wikilink")。直至有速遞員因工重傷危殆，為傳媒廣泛報道後，才引來[香港社會及](../Page/香港社會.md "wikilink")[勞工及福利局局長](../Page/勞工及福利局.md "wikilink")[張建宗關注](../Page/張建宗.md "wikilink")。\[38\]

### 帶動贈品換購潮流

麥當勞在1998年至2000年期間，曾經推出多款[史努比公仔系列贈品換購](../Page/史努比.md "wikilink")，曾經成為[潮流](../Page/潮流.md "wikilink")。至今，香港麥當勞仍不時推出禮物換購優惠。

<File:HK> McDonalds Souvenir Coca-Cola Glass Cup
2.JPG|麥當勞[可口可樂](../Page/可口可樂.md "wikilink")[玻璃杯](../Page/玻璃.md "wikilink")[贈品](../Page/贈品.md "wikilink")
<File:McDonalds> red
packet.JPG|香港麥當勞的[-{zh-hans:红封包;zh-hk:利市封;zh-tw:紅封包;}-贈品](../Page/紅包袋.md "wikilink")

## 各地爭議

麦当劳是世界上最大的快餐食物连锁店集團，亦是面對最多批評的國際速食集團之一。

## 相關經濟指標

由於麥當勞為全球最大的速食連鎖餐廳系統，有一些經濟學家從該公司的產品與服務進行相關研究，例如[大麥克指數](../Page/巨無霸指數.md "wikilink")。

## 相關影視作品

  - 2003年，美国[导演](../Page/导演.md "wikilink")[摩根·史柏路克拍摄一部关于麦当劳纪录片](../Page/摩根·史柏路克.md "wikilink")。片名《[不瘦降之謎](../Page/不瘦降之謎.md "wikilink")》。史柏路克在片内暗示麦当劳食品是导致美国民众超重祸首之一。麦当劳后来更改餐单，取消特大号食品，以较健康食物代替。更换餐单刚好在影展发表此片之后，在[戏院正式上映之前](../Page/戏院.md "wikilink")。
  - 2016年，[美國](../Page/美國.md "wikilink")[導演](../Page/導演.md "wikilink")[約翰·李·漢考克拍攝了一部有關麥當勞的電影](../Page/約翰·李·漢考克.md "wikilink")《[速食遊戲](../Page/速食遊戲.md "wikilink")》，劇情描述[雷·克洛克收購麥當勞連鎖速食店的故事](../Page/雷·克洛克.md "wikilink")。

## 同類型西式快餐連鎖店

  - [肯德基](../Page/肯德基.md "wikilink")
  - [溫蒂漢堡](../Page/溫蒂漢堡.md "wikilink")
  - [漢堡王](../Page/漢堡王.md "wikilink")
  - [摩斯漢堡](../Page/摩斯漢堡.md "wikilink")
  - [Subway](../Page/賽百味.md "wikilink")
  - [儂特利](../Page/儂特利.md "wikilink")
  - [祖樂比](../Page/祖樂比.md "wikilink")
  - [德克士](../Page/德克士.md "wikilink")
  - [华莱士](../Page/华莱士_\(连锁\).md "wikilink")
  - [頂呱呱](../Page/頂呱呱.md "wikilink")
  - [-{zh-hant:哈迪斯;zh-hans:哈迪斯;zh-tw:哈帝漢堡}-](../Page/哈迪斯_\(公司\).md "wikilink")

## 參見

  - [山東招遠麥當勞殺人事件](../Page/山東招遠麥當勞殺人事件.md "wikilink")
  - [1992年台北麥當勞爆炸案](../Page/1992年台北麥當勞爆炸案.md "wikilink")
  - [快餐店](../Page/快餐店.md "wikilink")
  - [快餐](../Page/快餐.md "wikilink")
  - [反式脂肪](../Page/反式脂肪.md "wikilink")
  - [麦当劳咖啡（McCafé）](../Page/McCafé.md "wikilink")
  - [蓝蓝路](../Page/蓝蓝路.md "wikilink")
  - [麥胖報告](../Page/麥胖報告.md "wikilink")
  - [台灣速食店列表](../Page/台灣速食店列表.md "wikilink")
  - [速食遊戲](../Page/速食遊戲.md "wikilink")

## 参考文献

## 外部链接

{{ external media | align = right | width = 200px | image1 =
[位於香港皇后大道西的麥當勞分店](http://i.imgur.com/miweuY3.png)（[Google地圖](../Page/Google地圖.md "wikilink")）
| image2 = [位於德國的麥當勞分店](http://i.imgur.com/RGof7iZ.jpg) | image3 =
[義大利威尼斯的麥當勞分店](http://media-cdn.tripadvisor.com/media/photo-s/03/fc/82/68/mc-do-venise.jpg)
| audio1 = | audio2 = }}

  - [麥當勞-美國](http://www.mcdonalds.com)（可转至该餐厅60多个国家／地区）

  - [麥當勞-英國](http://www.mcdonalds.co.uk/)

  - [麥當勞-中国](http://www.mcdonalds.com.cn)

  - [麥當勞-台灣](http://www.mcdonalds.com.tw)

  - [麥當勞-香港](http://www.mcdonalds.com.hk)

  - [麥當勞-新加坡](https://www.mcdonalds.com.sg)

  - [麥當勞-馬來西亞](http://www.mcdonalds.com.my)

  - [麥當勞-泰國](http://www.mcthai.co.th)

  - [麥當勞-越南](http://www.mcdonalds.vn)

  - [麥當勞-日本](http://www.mcdonalds.co.jp/)

  - [麥當勞-韓國](http://www.mcdonalds.co.kr/www/kor/main/main.do#/)

  -
  -
  -
  -
  -
  - [麥當勞叔叔之家慈善基金會](http://www.rmhc.org.tw/)

  -
  -
{{-}}

[麥當勞](../Category/麥當勞.md "wikilink")
[M](../Category/1940年成立的公司.md "wikilink")
[M](../Category/1940年美國建立.md "wikilink")
[M](../Category/1940年加利福尼亞州建立.md "wikilink")
[M](../Category/美國連鎖速食店.md "wikilink")
[M](../Category/美國品牌.md "wikilink")
[M](../Category/總部在美國的跨國公司.md "wikilink")
[M](../Category/標準普爾500指數成分股.md "wikilink")
[M](../Category/道瓊斯工業平均指數成份股.md "wikilink")
[M](../Category/東京證券交易所已除牌公司.md "wikilink")

1.

2.

3.

4.

5.

6.

7.  \-{zh-hans:; zh-hant:;}-

8.

9.

10.
11.

12.

13.
14.
15.

16.

17.

18.

19.
20. [麥當勞開千坪旗艦店](http://appledaily.com.tw/appledaily/article/headline/20120901/34479670/applesearch/)，[蘋果日報](../Page/蘋果日報.md "wikilink")，2012年9月1日

21. [獨家！美國總部下令，台灣近350家直營店喊賣　麥當勞子公司
    撤出台灣！](http://www.businessweekly.com.tw/KIndepArticle.aspx?ID=25624)

22. [震驚！麥當勞傳退出台灣350間直營店喊賣](http://www.appledaily.com.tw/realtimenews/article/new/20150624/634842/)

23. [大勢已去？全球麥當勞要改革](http://world.yam.com/post.php?id=4181)

24. [養生風潮當道　速食天王裁員求翻身](http://world.yam.com/post.php?id=3142)

25. 嚴雅芳、楊美玲，[國賓證實
    仰德二少李昌霖買台麥當勞](https://udn.com/news/story/7241/2182998)，聯合晚報，2016-12-21

26.

27.

28. [臺灣麥當勞賣給國賓二少
    公平會准了](https://tw.news.yahoo.com/%E5%8F%B0%E7%81%A3%E9%BA%A5%E7%95%B6%E5%8B%9E%E8%B3%A3%E7%B5%A6%E5%9C%8B%E8%B3%93%E4%BA%8C%E5%B0%91-%E5%85%AC%E5%B9%B3%E6%9C%83%E5%87%86%E4%BA%86-062244774.html)，雅虎奇摩新聞，2017-04-26

29. 林俊宏，[【麥叔叔換老闆】本刊年初獨家揭麥當勞易主
    公平會裁准了](https://www.mirrormedia.mg/story/20170426inv007/)，鏡傳媒，2017-04-26

30. [終於宣布了　國賓李昌霖正式接手台灣麥當勞](https://www.nownews.com/news/20170601/2545952/)，NOWnews
    今日新聞，2017-06-01

31. [台灣麥當勞老闆換人
    國賓飯店總座接手](http://www.cna.com.tw/news/afe/201706015019-1.aspx)，中央社，2017-06-01

32.

33. [從「大麥克之亂」中可以學到的4個商業行銷手法](https://www.thenewslens.com/article/105039)《The
    News Lens》2018-09-30,商業

34. [中信正式入主麦当劳中国：5年增开两千家店，主攻三四线城市](http://www.thepaper.cn/newsDetail_forward_1755669).澎湃新闻.2017-08-08.\[2017-08-12\].

35. [麦当劳中国回应更名金拱门：确实是“M”Logo像拱门](http://www.nbd.com.cn/articles/2017-10-25/1156404.html)，每日经济新闻-每经网

36.

37.
38.