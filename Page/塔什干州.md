<table>
<thead>
<tr class="header">
<th><p>塔什干州<br />
</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Toshkent_Viloyati_in_Uzbekistan.svg" title="fig:Toshkent_Viloyati_in_Uzbekistan.svg">Toshkent_Viloyati_in_Uzbekistan.svg</a></p></td>
</tr>
<tr class="even">
<td><p>基本統計資料</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/首府.md" title="wikilink">首府</a>:</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/面積.md" title="wikilink">面積</a> :</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人口.md" title="wikilink">人口</a>(2005):<br />
 · <a href="../Page/人口密度.md" title="wikilink">密度</a> :</p></td>
</tr>
<tr class="even">
<td><p>主要<a href="../Page/民族.md" title="wikilink">民族</a>:</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ISO_3166-2.md" title="wikilink">行政區代碼</a>:</p></td>
</tr>
</tbody>
</table>

**塔什干州**（[烏茲別克語](../Page/烏茲別克語.md "wikilink")：）是[烏茲別克十二個州份之一](../Page/烏茲別克.md "wikilink")。它涵蓋15,300平方公里，有人口4,450,000。塔什干州下轄15個縣，[首府設於](../Page/首府.md "wikilink")[塔什干直辖市](../Page/塔什干.md "wikilink")。虽然塔什干州首府设在塔什干市，但塔什干市作为直辖市并不由塔什干州管理。

## 地理位置

塔什干州位於烏茲別克東部，在[天山山脉和](../Page/天山山脉.md "wikilink")[锡尔河之间](../Page/锡尔河.md "wikilink")。它與以下州份或國家相連（從北開始逆時針）：[哈薩克](../Page/哈薩克.md "wikilink")、[錫爾河州](../Page/錫爾河州.md "wikilink")、[塔吉克](../Page/塔吉克.md "wikilink")、[納曼干州](../Page/納曼干州.md "wikilink")、[吉爾吉斯](../Page/吉爾吉斯.md "wikilink")。公路全長3,771公里，另外[鐵路](../Page/鐵路.md "wikilink")360公里。

## 行政区划

塔什干州分为13个[区](../Page/行政区.md "wikilink")，如下：

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
## 重要城镇

  - [塔什干市](../Page/塔什干.md "wikilink")（）
  - [安格連](../Page/安格連.md "wikilink")（）
  - [阿爾馬雷克](../Page/阿爾馬雷克.md "wikilink")（）
  - [阿汉加兰](../Page/阿汉加兰.md "wikilink")（）
  - [别卡巴德](../Page/别卡巴德.md "wikilink")（）
  - [奇爾奇克](../Page/奇爾奇克.md "wikilink")（）
  - [帕尔肯特](../Page/帕尔肯特.md "wikilink")（）
  - [托伊铁帕](../Page/托伊铁帕.md "wikilink")（）
  - [扬吉阿巴德](../Page/扬吉阿巴德.md "wikilink")（）
  - [扬吉尤利](../Page/扬吉尤利.md "wikilink")（）

## 經濟

塔什干州是乌兹别克经济最发达的州份，全州一共有9081家企业，其中128家已注册受到外资。1997年外贸额达到6.85亿美元。

## 外部链接

  - [塔什干州官方网站](https://web.archive.org/web/20070331070444/http://www.tashvil.gov.uz/uz/indexuz.htm)
    （乌兹别克语）

[Category:烏茲別克行政區劃](../Category/烏茲別克行政區劃.md "wikilink")