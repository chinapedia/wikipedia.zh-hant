**詔安客家人**指出生或[祖籍來自](../Page/祖籍.md "wikilink")[福建省](../Page/福建省.md "wikilink")[漳州府](../Page/漳州府.md "wikilink")[詔安縣](../Page/詔安縣.md "wikilink")[客家族群者](../Page/客家.md "wikilink")\[1\]\[2\]，是臺灣目前急速減少的族群。

在台灣，於雲林縣的[二崙鄉](../Page/二崙鄉.md "wikilink")、[崙背鄉](../Page/崙背鄉.md "wikilink")、[西螺鎮](../Page/西螺鎮.md "wikilink")、[口湖鄉](../Page/口湖鄉.md "wikilink")[椬梧村等四鄉鎮為最主要分佈地點](../Page/椬梧村.md "wikilink")，另外[新北市的](../Page/新北市.md "wikilink")[新店區境內的安坑庄](../Page/新店區.md "wikilink")，[桃園市的](../Page/桃園市.md "wikilink")[八德區部份地區](../Page/八德區.md "wikilink")、[大溪區境內的南興](../Page/大溪區.md "wikilink")，台中市的[西屯區](../Page/西屯區.md "wikilink")、[北屯區](../Page/北屯區.md "wikilink")、[港尾里部分地區](../Page/港尾里_\(臺中市\).md "wikilink")、[南投縣的](../Page/南投縣.md "wikilink")[中寮鄉部分地區](../Page/中寮鄉.md "wikilink")、[嘉義縣的](../Page/嘉義縣.md "wikilink")[中埔鄉境內的詔安厝](../Page/中埔鄉.md "wikilink")、[台南市的](../Page/台南市.md "wikilink")[白河區境內的詔安厝](../Page/白河區.md "wikilink")、[楠西區境內的鹿陶洋江家聚落](../Page/楠西區.md "wikilink")(亦是全台最大的單姓詔安古厝群及現存住人聚落)，[宜蘭縣部份地區](../Page/宜蘭縣.md "wikilink")，前述等地皆有聚落。而大多地區散居的詔安客家人因為被闽南人同化，已消失的差不多了，幾乎都[福佬化](../Page/福佬客.md "wikilink")。

目前以崙背鄉的港尾、羅厝、崙前、鹽園以及二崙鄉的復興村、三和村、來惠村為大本營。如今會講[詔安客語的人口大概只剩下八千人左右](../Page/詔安客語.md "wikilink")。目前有鄉土語言教育，有[崙背國中](../Page/崙背國中.md "wikilink")、[二崙國中以及東興國小](../Page/二崙國中.md "wikilink")、來惠國小、三和國小、二崙國小等學校積極推廣中，[行政院](../Page/行政院.md "wikilink")[客家委員會更提供相關經費協助雲林縣政府於](../Page/客家委員會.md "wikilink")[崙背鄉建設](../Page/崙背鄉.md "wikilink")[詔安客家文化館](../Page/詔安客家文化館.md "wikilink")\[3\]，並且在東興國小設立詔安客語教學資源中心。另外有也一些自發性民間組職，定期性的開課教授[詔安客語](../Page/詔安客語.md "wikilink")，如[新北市詔安客家文化促進會](../Page/新北市詔安客家文化促進會.md "wikilink")\[4\]等。

[客家電視臺在](../Page/客家電視臺.md "wikilink")2007年7月到9月中舉辦了[大埔腔](../Page/臺灣客家語.md "wikilink")、[饒平腔以及詔安腔的配音人才培訓班](../Page/臺灣客家語.md "wikilink")，目前在客家電視臺已播過〈大嬸婆與小聰明〉的卡通，陸續也會有其他卡通上映。同時在2007年也由廖文和布袋戲團演出《金黑巾英雄傳》對詔安語言傳承貢獻良多。接著2008年又製播了〈[西螺七崁阿善師](../Page/西螺七崁.md "wikilink")〉布袋戲，使得詔安客更具特色，8月31日更將黃俊雄布袋戲《包公俠義傳》加以配音，使得詔安客語能有一絲復興的機會。

## 名人

  - [廖朝孔](../Page/廖朝孔.md "wikilink")
  - [李萬居](../Page/李萬居.md "wikilink")
  - [呂泉生](../Page/呂泉生.md "wikilink")
  - [張小燕](../Page/張小燕.md "wikilink")
  - [宋澤萊](../Page/宋澤萊.md "wikilink")（本名廖偉竣）
  - [李應元](../Page/李應元.md "wikilink")
  - [廖本興](../Page/廖本興.md "wikilink")
  - [廖昭堂](../Page/廖昭堂.md "wikilink")
  - [李壬奎](../Page/李壬奎.md "wikilink")
  - [李茂盛](../Page/李茂盛.md "wikilink")
  - [游錫堃](../Page/游錫堃.md "wikilink")

## 參見

  - [詔安縣](../Page/詔安縣.md "wikilink")
  - [西螺七嵌](../Page/西螺七嵌.md "wikilink")
  - [張廖姓](../Page/張廖姓.md "wikilink")
      - [張廖家廟](../Page/張廖家廟.md "wikilink")
      - [清武家廟垂裕堂](../Page/清武家廟垂裕堂.md "wikilink")

## 參考文獻

## 外部連結

  - [雲林詔安客家文化節](http://blog.yunlin.me/1-20/)
  - [詔安客家文化館](http://www.zhaoanka.org.tw/)
  - [黃帝祠-百家姓](https://web.archive.org/web/20100122161522/http://www.chinaemperorhall.com/index.asp)
  - [雲林二崙鄉土傳奇](http://www.lhps.ylc.edu.tw/country-material/Erthlen/tea4.htm)
  - [南投廖姓宗親會網站](https://web.archive.org/web/20100627010552/http://www.liao.url.tw/)
  - [張廖族親各派淵源](http://familyliao.myweb.hinet.net/f06.htm)
  - [崇遠堂官方網站](http://www.chongyuantang.com/)
  - [漳臺客家淵源](https://web.archive.org/web/20120111072154/http://big5.chinataiwan.org/zppd/zpgc/200809/t20080918_748604.htm)
  - [世界廖氏宗親總會網站](https://web.archive.org/web/20120416064652/http://liaos.no49.cuttle.com.cn/view.asp?id=128)

[\*](../Page/category:福建客家人.md "wikilink")
[category:诏安人](../Page/category:诏安人.md "wikilink")

[\*](../Category/客家裔臺灣人.md "wikilink")
[Category:雲林縣文化](../Category/雲林縣文化.md "wikilink")

1.  [詔安好客 七崁精神相傳三百年](http://www.youtube.com/watch?v=w6v3zhb3-j4)
2.  [張廖源流之詔安客家文化及詔安客語](http://familyliao.myweb.hinet.net/b06.htm)
3.  [詔安客家文化館](https://www.facebook.com/pages/%E8%A9%94%E5%AE%89%E5%AE%A2%E5%AE%B6%E6%96%87%E5%8C%96%E9%A4%A8/724911884200370)
4.  [新北市詔安客家文化促進會](https://www.facebook.com/pages/%E6%96%B0%E5%8C%97%E5%B8%82%E8%A9%94%E5%AE%89%E5%AE%A2%E5%AE%B6%E6%96%87%E5%8C%96%E4%BF%83%E9%80%B2%E6%9C%83/128553433919692)