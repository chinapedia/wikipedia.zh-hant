**吉林**（），[上海市人](../Page/上海市.md "wikilink")；[中国人民大学法律系刑法专业毕业](../Page/中国人民大学.md "wikilink")，法学硕士，副教授。1984年5月加入[中国共产党](../Page/中国共产党.md "wikilink")，1987年7月参加工作。他是[中共第十七屆](../Page/中国共产党第十八届中央委员会候补委员列表.md "wikilink")、[十八届候補中央委員](../Page/中国共产党第十八届中央委员会候补委员列表.md "wikilink")。现任[北京市政协主席](../Page/中国人民政治协商会议北京市委员会.md "wikilink")、党组书记。

## 从政经历

1987年大学毕业后，吉林留校从事[共青团](../Page/共青团.md "wikilink")、及党务工作；曾历任校共青团委干事、副书记、书记，校党委常委、法律系党总支书记。1994年8月，出任共青团北京市委副书记；一年后，晋升共青团北京市委书记。1998年10月调任[密云县委书记](../Page/密云县.md "wikilink")。2002年6月升任北京市委常委、政法委书记；2004年调任北京市副市长；2007年升任中共北京市委常委、常务副市长；2012年6月，当选北京市委副书记。\[1\]2012年7月25日上午，在北京市第十三届人民代表大会常务委员会第三十四次会议中，吉林辞去北京市副市长职务\[2\]。2013年1月25日，吉林当选为北京市政协主席。2013年4月27日，吉林不再兼任北京市委副书记。

## 参考资料

{{-}}

[Category:第十一屆全國人大代表](../Category/第十一屆全國人大代表.md "wikilink")
[Category:第十二届全国政协委员](../Category/第十二届全国政协委员.md "wikilink")
[Category:北京市政協主席](../Category/北京市政協主席.md "wikilink")
[Category:中华人民共和国北京市副市长](../Category/中华人民共和国北京市副市长.md "wikilink")
[Category:中国共产党第十七届中央委员会候补委员](../Category/中国共产党第十七届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十八届中央委员会候补委员](../Category/中国共产党第十八届中央委员会候补委员.md "wikilink")
[Category:中共北京市委副書記](../Category/中共北京市委副書記.md "wikilink")
[Category:中共北京市委常委](../Category/中共北京市委常委.md "wikilink")
[Category:中共密雲縣委書記](../Category/中共密雲縣委書記.md "wikilink")
[Category:中國共產主義青年團北京市委員會書記](../Category/中國共產主義青年團北京市委員會書記.md "wikilink")
[Category:中国人民大学校友](../Category/中国人民大学校友.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[L](../Category/吉姓.md "wikilink")
[Category:中共北京市委政法委书记](../Category/中共北京市委政法委书记.md "wikilink")
[Category:中华人民共和国北京市常务副市长](../Category/中华人民共和国北京市常务副市长.md "wikilink")

1.  [王安顺、吉林当选北京市委副书记](http://news.ifeng.com/mainland/special/beijingdangdaihui11/content-3/detail_2012_07/03/15749636_0.shtml)
2.