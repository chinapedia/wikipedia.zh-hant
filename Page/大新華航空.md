**大新華航空**，是由[海南航空](../Page/海南航空.md "wikilink")、[中國新華航空](../Page/中國新華航空.md "wikilink")、[長安航空和](../Page/長安航空.md "wikilink")[山西航空合併而成](../Page/山西航空.md "wikilink")，和[海南航空同样是](../Page/海南航空.md "wikilink")**海航集团**旗下的骨干航空运输企业。2012年8月海航集团正式确认放弃以大新华航空作为平台整合资源，转而使用海南航空平台。同年底，在“海航创业20周年”系列活动正式启动仪式上，海航集团新的标识也正式向社会公众呈现。
\[1\]

## 歷史及未來發展

大新華航空在2007年11月27日頒發了運營合格証，2007年11月29日合併成為全新的航空公司，並於同日在北京舉行掛牌儀式。合併完成後，大新華航空預計會有100架飛機，並計劃於2008年以紅籌形式到[香港上市](../Page/香港.md "wikilink")\[2\]。
[海南航空在](../Page/海南航空.md "wikilink")2008年6月9日開辦來往[北京及](../Page/北京.md "wikilink")[西雅圖的直航路線會以大新華航空的名義飛航](../Page/西雅圖.md "wikilink")，每周一、三、五、六由一部[空中巴士A330飛行](../Page/空中巴士A330.md "wikilink")，提供商務艙和經濟艙。\[3\]

## 机队

  -
    <span style="font-size:smaller;">截止2012年7月</span>

<!-- end list -->

  - [波音737-800](../Page/波音737.md "wikilink")：3架（B-2637、B-2652、B-5089）

[缩略图](https://zh.wikipedia.org/wiki/File:XW_B737-800_B-5138.jpg "fig:缩略图")

## 相關條目

  - [海南航空](../Page/海南航空.md "wikilink")
  - [中國新華航空](../Page/中國新華航空.md "wikilink")
  - [長安航空](../Page/長安航空.md "wikilink")
  - [山西航空](../Page/山西航空.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [大新華航空](https://web.archive.org/web/20090830055101/http://www.grandchinaair.com/)
  - [大新华航空有限公司首航记录—民航论坛](http://pic.feeyo.com/posts/280/2805093.html)

[分類:2007年成立的航空公司](../Page/分類:2007年成立的航空公司.md "wikilink")

[Category:海南航空](../Category/海南航空.md "wikilink")
[Category:2012年結業航空公司](../Category/2012年結業航空公司.md "wikilink")

1.  [1](http://www.rologo.com/hna-group-logo-evolution.html)
2.  [大新華航空 周四京成立](http://hk.news.yahoo.com/071119/12/2jrv2.html)
3.  [海南航空6月開通西雅圖至北京直航
    每周往返四班](http://www.chinapressusa.com/big5/newscenter/2008-01/29/content_69089.htm)