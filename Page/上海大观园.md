[Shanghaidaguanyuan.jpg](https://zh.wikipedia.org/wiki/File:Shanghaidaguanyuan.jpg "fig:Shanghaidaguanyuan.jpg")
**上海大观园**是根据[中国](../Page/中国.md "wikilink")[清代名著](../Page/清代.md "wikilink")《[红楼梦](../Page/红楼梦.md "wikilink")》中[大观园的描写设计而成的大型仿古园林建筑群](../Page/大观园.md "wikilink")，是[国家4A级旅游景区](../Page/国家4A级旅游景区.md "wikilink")，[上海五星级公园](../Page/上海五星级公园.md "wikilink")，位于[上海](../Page/上海.md "wikilink")[青浦区](../Page/青浦区.md "wikilink")[青商公路](../Page/青商公路.md "wikilink")701号，[淀山湖西侧](../Page/淀山湖.md "wikilink")，距离[上海市区](../Page/上海.md "wikilink")65公里，占地135亩，建筑面积约8000平方米。原称-{淀山湖}-风景游览区，1991年改称上海大观园，占地也扩大到1500亩。

## 历史

上海大观园由[上海园林院](../Page/上海园林院.md "wikilink")[梁友松主持规划设计](../Page/梁友松.md "wikilink")，1979年秋动土起造，1980年局部开放，1988年基本建成开放。总体布局以[大观楼为主体](../Page/大观楼.md "wikilink")，由“省亲别墅”石[牌坊](../Page/牌坊.md "wikilink")、石[灯笼](../Page/灯笼.md "wikilink")、沁芳湖、体仁沐德、曲径通幽、宫门、“太虚幻境”[浮雕](../Page/浮雕.md "wikilink")[照壁](../Page/照壁.md "wikilink")、木牌坊等形成全园[中轴线](../Page/中轴线.md "wikilink")。西侧设置[怡红院](../Page/怡红院.md "wikilink")、[拢翠庵](../Page/拢翠庵.md "wikilink")、梨香院、石舫。东侧设置[潇湘馆](../Page/潇湘馆.md "wikilink")、[蘅芜苑](../Page/蘅芜苑.md "wikilink")、[蓼风轩](../Page/蓼风轩.md "wikilink")、[稻香村等](../Page/稻香村_\(红楼梦\).md "wikilink")20多组建筑景点。

上海大观园于1980年工程未完成时已接受访问，从1988年10月正式对外开放以来，共接待了国内外来宾近2000万人次。上海大观园曾荣获国家建筑[鲁班奖](../Page/鲁班奖.md "wikilink")，上海40周年十佳建筑，十佳休闲新景点，新中国50周年上海优秀建筑，[上海十大旅游特色园林](../Page/上海十大旅游特色园林.md "wikilink")，上海市七大文明公园，[国家建设部全国风景名胜区先进集体等荣誉称号](../Page/国家建设部.md "wikilink")。

## 建筑特色

大观园在设计上颇费心机，与[北京大观园明显不同的是](../Page/北京大观园.md "wikilink")，上海大观园利用[江南水乡的特点在园中布置了大面积人工](../Page/江南.md "wikilink")[湖泊](../Page/湖泊.md "wikilink")。设计曲径通幽大[假山作入口屏障](../Page/假山.md "wikilink")，以挖湖取土堆掇1座高16米的小山为大观楼的背景，构成大观楼背山面水的壮丽气势。全园以大湖为中心，以[池塘](../Page/池塘.md "wikilink")、沁芳溪沟通各景点，构成有主有支、有动有静的水系，湖边设[亭](../Page/亭.md "wikilink")、[榭](../Page/榭.md "wikilink")，湖中设曲桥、石舫、石灯，溪上设桥亭，形成山重水复、流水人家的江南园林风光。

大观园巧妙运用园必封、必隔，在封隔中求得气势流动和内聚中心的[中国传统建筑观念](../Page/中国传统建筑.md "wikilink")，建造1个封闭、向心的内涵丰富的小天地，在有限空间中安置无限空间，增加景物层次，使建筑与环境融合为一，借环境气氛表达人物品格，使《红楼梦》中的大观园景观再现人间。

  -
    大观楼：为三进宫式建筑群，主楼为7间2层琉璃建筑，雕梁画栋，飞金彩绘，是元妃省亲，族人朝觐的庆典用殿堂。两侧有配殿、楼阁，其后为寝宫和刻有清水砖雕的北宫门。

<!-- end list -->

  -
    [怡红院](../Page/怡红院.md "wikilink")：[贾宝玉的居所](../Page/贾宝玉.md "wikilink")。布局分东西两路：东路以供起居的绛芸轩为主，由[曲廊](../Page/曲廊.md "wikilink")、洞门分成3个小院，以东楼为对景，山亭为中心，形成疏密相间的三进庭院。西路为宝玉读书、会客、奕棋的三进院落。建筑内部精雕细刻，设置有[红木](../Page/红木.md "wikilink")[家具](../Page/家具.md "wikilink")，珠光宝器，配植[芭蕉](../Page/芭蕉.md "wikilink")、[海棠](../Page/海棠.md "wikilink")，显示雍容华贵，带浓重脂粉的人物性格。

<!-- end list -->

  -
    [潇湘馆](../Page/潇湘馆.md "wikilink")：“潇湘妃子”[林黛玉的居所](../Page/林黛玉.md "wikilink")，设于竹林旁，以“有凤来仪”为主体，由书房、厅堂与画廊构成三进院落。设小桥流水，配植翠竹、梨树、梅花，有精美的花阶铺地，形成清秀、疏朗、高雅的园林风貌，以表现黛玉清逸孤傲，脱尘去俗的性格。

<!-- end list -->

  -
    [蘅芜苑](../Page/蘅芜苑.md "wikilink")：[薛宝钗的居所](../Page/薛宝钗.md "wikilink")，具有[歇山加](../Page/歇山.md "wikilink")[撵尖屋顶独特建筑形式](../Page/撵尖.md "wikilink")。以大假山、二层轩廊及多植藤萝为特色，形成闭塞、庄重的园林空间，表现宝钗清幽深邃中微露几分骄情和“冷美人”的品格。

<!-- end list -->

  -
    [拢翠庵](../Page/拢翠庵.md "wikilink")：[妙玉清修之地](../Page/妙玉.md "wikilink")，以[观音壁泉和茶室庭园相组合](../Page/观音.md "wikilink")，环植[红梅](../Page/红梅.md "wikilink")、[绿萼](../Page/绿萼.md "wikilink")、苍[松](../Page/松.md "wikilink")、翠[竹](../Page/竹.md "wikilink")，形成妙玉看破红尘，清雅绝俗的超然环境。

<!-- end list -->

  -
    [稻香村](../Page/稻香村_\(红楼梦\).md "wikilink")：[李纨的住所](../Page/李纨.md "wikilink")，不施丹朱、除尽雕镂，用素墙瓦房，白木门窗，竹笆护墙，种植瓜藤果蔬，依然本色，朴实无华，以一派乡村气息的园景，恰当地表现了李纨朴实无华的性格。

大观园内装修多用[木雕](../Page/木雕.md "wikilink")、[石雕和](../Page/石雕.md "wikilink")[砖雕](../Page/砖雕.md "wikilink")，广泛应用砖细雕缕花格漏窗，其中北宫门的[牡丹浮雕尤为精美](../Page/牡丹.md "wikilink")。大门广场的石雕照壁─[女娲补天](../Page/女娲.md "wikilink")、宝玉下凡大幅[花岗石嵌玉浮雕](../Page/花岗石.md "wikilink")，以及背面[警幻仙子与](../Page/警幻仙子.md "wikilink")[金陵十二金钗](../Page/金陵十二金钗.md "wikilink")[汉白玉浮雕](../Page/汉白玉.md "wikilink")，亦甚精致，正面朴素粗犷，背面纤细华美，耐人寻味。

大观园的左前侧设置一座7层楼阁式宝塔青云[塔](../Page/塔.md "wikilink")，造型秀丽，[钢筋](../Page/钢筋.md "wikilink")[混凝土结构](../Page/混凝土.md "wikilink")。塔高42.5米，顶部2层为水塔，下部5层为观光层，是淀山湖畔的标志建筑。

大观园以建筑为主，建筑与绿化相结合的手法造景；大观园外围景区则以大面积绿化造景为主。

  -
    梅林春深，在风景区东北部挖池筑坡，占地190亩。遍植红梅、绿萼4000余株，[梅花形成早春香雪海](../Page/梅花.md "wikilink")，气势非凡。
    柳堤春晓，东部湖中筑200米长堤，围成200余亩的湖中湖，堤上筑拱桥、植[垂柳和花木](../Page/垂柳.md "wikilink")。
    群芳争艳，在东部湖滨改造地形，铺设占地200亩的大草坪，片植竹林，丛植[樱花](../Page/樱花.md "wikilink")、海棠等，形成碧波荡漾，绿茵浩翰，百花争艳的壮丽景观。
    金雪飘香，在东部广植[金桂](../Page/金桂.md "wikilink")、[银桂](../Page/银桂.md "wikilink")、[丹桂](../Page/丹桂.md "wikilink")、[四季桂](../Page/四季桂.md "wikilink")2500余株，在郁密的[桂花](../Page/桂花.md "wikilink")、[菊花丛中](../Page/菊花.md "wikilink")，建造水上餐厅，形成蟹肥菊黄，金雪飘香的金秋风光。
    女娲遗石，种植占地100亩的大片密林，林中构筑“女娲补天”、“女娲遗石”等山石景点，构筑石头城，作为大观园的起点。

## 外部链接

  - [上海大观园官方主页](http://www.sh-daguanyuan.com/)

[沪](../Category/国家4A级旅游景区.md "wikilink")
[Category:紅樓夢](../Category/紅樓夢.md "wikilink")
[Category:上海公园](../Category/上海公园.md "wikilink")
[Category:上海園林](../Category/上海園林.md "wikilink")