**顺平县**位于中国[河北省中部偏西](../Page/河北省.md "wikilink")、[太行山东麓](../Page/太行山.md "wikilink")，是[保定市下辖的一个县](../Page/保定市.md "wikilink")。

## 历史沿革

### 古代

[秦为](../Page/秦朝.md "wikilink")**曲逆县**；[西汉封](../Page/西汉.md "wikilink")[陈平为曲逆侯](../Page/陈平_\(汉朝\).md "wikilink")，遂为曲逆侯国，属[中山国](../Page/中山郡.md "wikilink")；王莽改为**顺平县**；东汉初复原名，章帝元和三年（86年）改为**蒲阴县**；[北魏属](../Page/北魏.md "wikilink")[定州](../Page/定州.md "wikilink")[北平郡](../Page/北平郡.md "wikilink")；[北齐省蒲阴](../Page/北齐.md "wikilink")、[望都入](../Page/望都.md "wikilink")[北平县](../Page/北平县_\(西汉\).md "wikilink")，治原蒲阴县城；[隋遂为北平县](../Page/隋朝.md "wikilink")；[唐](../Page/唐朝.md "wikilink")[宋因之](../Page/宋朝.md "wikilink")，属[定州](../Page/定州_\(北魏\).md "wikilink")；[金改](../Page/金朝.md "wikilink")**永平县**，后升**完州**；[元仍为完州](../Page/元朝.md "wikilink")；[明](../Page/明朝.md "wikilink")[洪武二年](../Page/洪武.md "wikilink")（1369年）降为**完县**。

### 現代

1958年6月，完县、[满城县合并](../Page/满城区.md "wikilink")，称完满县，属保定专区。同年11月又分立，完县改为保定市完县区。

1960年3月，清苑、完县、满城三区合并，称清苑县，县治满城县，仍属保定市。

1961年12月，恢复完县建制，改属保定专区。

1993年8月，更名顺平县。

1995年，保定地市合并，顺平县属保定市。

## 地理

地理坐标北纬38度45秒—39度09秒，东经114度50秒—115度20秒。东临[满城](../Page/满城区.md "wikilink")，西接[唐县](../Page/唐县.md "wikilink")，南毗[望都](../Page/望都县.md "wikilink")，北连[易县](../Page/易县.md "wikilink")，东南与[清苑接壤](../Page/清苑区.md "wikilink")，西北与[涞源搭界](../Page/涞源县.md "wikilink")。距北京162公里，天津210公里，石家庄102公里，保定32公里。

## 交通

京广西线公路从中部穿过，保源公路从县境北部通过，[107国道和](../Page/107国道.md "wikilink")[京广铁路从县境南部边缘通过](../Page/京广铁路.md "wikilink")（距县城14公里）。[保阜高速公路](../Page/保阜高速公路.md "wikilink")、[张石高速公路在境内交汇](../Page/张石高速公路.md "wikilink")，为保定西部重要的交通枢纽。

## 行政区划

下辖5个[镇](../Page/行政建制镇.md "wikilink")、5个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [顺平县人民政府](http://www.shunping.gov.cn/)

## 参考文献

[顺平县](../Page/category:顺平县.md "wikilink")
[县](../Page/category:保定区县市.md "wikilink")
[保定](../Page/category:河北省县份.md "wikilink")
[冀](../Page/分类:国家级贫困县.md "wikilink")