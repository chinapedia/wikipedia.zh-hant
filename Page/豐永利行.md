**豊永利行**（，）是日本的男[演員](../Page/演員.md "wikilink")、[聲優](../Page/聲優.md "wikilink")。[Super
Eccentric
Theater所屬](../Page/Super_Eccentric_Theater.md "wikilink")。[東京都出身](../Page/東京都.md "wikilink")。血型B型。身高162cm。

## 概要

  - 暱稱トッシー。
  - [左撇子](../Page/左撇子.md "wikilink")。
  - 愛好：作曲、吉他、讀書、觀劇。特技：鼓、歌、跳舞。
  - 有絕對音感。
  - 喜歡的歌手為[槇原敬之](../Page/槇原敬之.md "wikilink")。喜歡的電影是[成龍主演的電影](../Page/成龍.md "wikilink")。
  - 在電視劇、動畫、舞台劇廣泛活躍。
  - 近幾年廣播劇的工作變多，主役的主要角色也增多。
  - 聲質與[水島大宙](../Page/水島大宙.md "wikilink")、[入野自由相似](../Page/入野自由.md "wikilink")。
  - 和演員[松山研一是高中同學](../Page/松山研一.md "wikilink")。
  - 與同為聲優的[木村良平是很好的朋友](../Page/木村良平.md "wikilink")。
  - 給好友[花澤香菜寫過](../Page/花澤香菜.md "wikilink")《花》，給[內山昂輝寫過](../Page/內山昂輝.md "wikilink")《bonds》，給木村良平寫過《encounter》。
  - 目標演員為[真田廣之與](../Page/真田廣之.md "wikilink")[春田純一](../Page/春田純一.md "wikilink")。
  - 飼養兩隻貓。
  - 2015年10月25日公布已結婚（入籍）。
  - 2018年贏得主演男聲優獎, 同年5月7日在博客發表自己女兒於去年出生的消息。

## 出演作品

### 電視動畫

**2003年**

  - [網球王子](../Page/網球王子_\(動畫\).md "wikilink")（葵劍太郎）
  - [我要上太空](../Page/我要上太空.md "wikilink")（**府中野新之介**）

**2004年**

  - [御行者](../Page/御行者.md "wikilink")（**ジョイ・レオン**）

**2005年**

  - [絕對少年](../Page/絕對少年.md "wikilink")（**逢澤步**）
  - [魔界女王候補生](../Page/魔界女王候補生.md "wikilink")（星細工師）
  - [地獄少女](../Page/地獄少女.md "wikilink")（仙太郎）

**2006年**

  - [鍵姬物語 永久愛莉絲迴旋曲](../Page/鍵姬物語_永久愛莉絲迴旋曲.md "wikilink")（**桐原有人**）
  - [極速方程式](../Page/極速方程式.md "wikilink")（**平勝平太**（中学時代））
  - [地獄少女 二籠](../Page/地獄少女.md "wikilink")（仙太郎）
  - [死神的歌謠](../Page/死神的歌謠.md "wikilink")（市原カンタロウ）
  - [人造昆虫カブトボーグ V×V](../Page/人造昆虫カブトボーグ_V×V.md "wikilink")（ジョニー/山田一郎）
  - [嬌蠻之吻](../Page/嬌蠻之吻.md "wikilink")（**對馬雷奧**）
  - [我們的存在](../Page/我們的存在.md "wikilink")（田町）
  - [妖怪人間貝姆](../Page/妖怪人間貝姆.md "wikilink")（2006年版）（三上裕也）

**2007年**

  - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink")（細川一休）
  - [家庭教師HITMAN
    REBORN\!](../Page/家庭教師HITMAN_REBORN!_\(動畫\).md "wikilink")（柿本千種）
  - [GO！純情水泳社！](../Page/GO！純情水泳社！.md "wikilink")（**沖浦要**）
  - [Saint October](../Page/Saint_October.md "wikilink")（帝獵兵）
  - [Myself ;
    Yourself](../Page/Myself_;_Yourself.md "wikilink")（藝人、アニメグリーン）
  - [Master of Epic The Animation
    Age](../Page/奇幻冒險.md "wikilink")（紐特・男、他）

**2008年**

  - [家庭教師HITMAN
    REBORN\!](../Page/家庭教師HITMAN_REBORN!_\(動畫\).md "wikilink")（入江正一）
  - [我的狐仙女友](../Page/我的狐仙女友.md "wikilink")（**源多由良**）
  - [波菲的漫長旅程](../Page/波菲的漫長旅程.md "wikilink")（薩伊米斯）
  - [棒球大聯盟](../Page/棒球大聯盟.md "wikilink") 4th season（涉谷）
  - [遊戲王GX](../Page/遊戲王GX.md "wikilink")（空野大悟）
  - [World Destruction 世界撲滅的六人](../Page/世界毀滅.md "wikilink")（シン）

**2009年**

  - [料理偶像](../Page/料理偶像.md "wikilink")（AD小林）
  - [FRESH光之美少女\!](../Page/FRESH光之美少女!.md "wikilink")（裕子柴健人）
  - [卡片鬥士翔](../Page/卡片鬥士翔.md "wikilink")（大場ヒイト）

**2010年**

  - [無頭騎士異聞錄
    DuRaRaRa\!\!](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**龍之峰帝人**）
  - [棒球大聯盟](../Page/棒球大聯盟.md "wikilink") 6th season（涉谷）

**2011年**

  - [人家一點都不喜歡啦！](../Page/人家一點都不喜歡啦！.md "wikilink")（**高梨修輔**）
  - [少年同盟](../Page/少年同盟.md "wikilink")（**松岡春**）
  - [機動戰士GUNDAM AGE](../Page/機動戰士GUNDAM_AGE.md "wikilink")（**菲力特·亞斯諾**）
  - [戰鬥陀螺 鋼鐵奇兵](../Page/戰鬥陀螺_鋼鐵奇兵.md "wikilink")（布魯圖）

**2012年**

  - [大尾小岡](../Page/大尾小岡.md "wikilink")（ガイゼル（小羚羊））
  - [少年同盟2](../Page/少年同盟.md "wikilink")（**松岡春**）
  - [超譯百人一首戀歌](../Page/超譯百人一首戀歌.md "wikilink")（清原致信）
  - [刀劍神域](../Page/刀劍神域.md "wikilink")（啟太）
  - [心連·情結](../Page/心連·情結.md "wikilink")（宇和千尋）
  - [境界上的地平線2](../Page/境界上的地平線.md "wikilink")（清原致信）
  - [絕園的暴風雨](../Page/絕園的暴風雨.md "wikilink")（**不破真廣**）
  - [CØDE:BREAKER](../Page/CØDE:BREAKER.md "wikilink")（**天宝院遊騎**）
  - [偶像學園](../Page/偶像學園_\(動畫\).md "wikilink")（涼川直人）

**2013年**

  - [黑色嘉年華](../Page/黑色嘉年華.md "wikilink")（夏切）
  - [召喚惡魔Z](../Page/召喚惡魔.md "wikilink")（印裘巴斯）
  - [眼鏡部！](../Page/眼鏡部！.md "wikilink")（望月紘一）
  - [我的腦內戀礙選項](../Page/我的腦內戀礙選項.md "wikilink")（**甘草奏**）
  - [武士弗拉明戈](../Page/武士弗拉明戈.md "wikilink")（綠川碧）
  - [偶像學園2](../Page/偶像學園_\(動畫\).md "wikilink")（涼川直人）

**2014年**

  - [諸神的惡作劇](../Page/諸神的惡作劇.md "wikilink")（**戶塚尊**）
  - [東京喰種](../Page/東京喰種.md "wikilink")（**永近英良**）
  - [地球隊長](../Page/地球隊長.md "wikilink")（莫）
  - [幕末Rock](../Page/幕末Rock.md "wikilink")（藤堂平助）
  - [偶像學園3](../Page/偶像學園_\(動畫\).md "wikilink")（涼川直人）

**2015年**

  - [東京喰種√A](../Page/東京喰種.md "wikilink")（**永近英良**）
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2
    承](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**龍之峰帝人**）
  - [ALDNOAH.ZERO 第2季](../Page/ALDNOAH.ZERO.md "wikilink")（馬祖魯卡）
  - [鑽石王牌－SECOND SEASON－](../Page/鑽石王牌.md "wikilink")（近藤大樹）\[1\]
  - [Classroom☆Crisis](../Page/Classroom☆Crisis.md "wikilink")（**北原小次郎**）
  - 無頭騎士異聞錄 DuRaRaRa\!\!×2 轉（**龍之峰帝人**）
  - [赤髮白雪姬](../Page/赤髮白雪姬.md "wikilink")（巳早）\[2\]
  - [純情羅曼史3](../Page/純情羅曼史.md "wikilink")（雫石涼）

**2016年**

  - [疾走王子Alternative](../Page/疾走王子.md "wikilink")（奧村楓）
  - [少女們向荒野進發](../Page/少女們向荒野進發.md "wikilink")（**甲斐亞登夢**）
  - 無頭騎士異聞錄 DuRaRaRa\!\!×2 結（**龍之峰帝人**）
  - [赤髮白雪姬](../Page/赤髮白雪姬.md "wikilink") 第2期（巳早）
  - [線上遊戲的老婆不可能是女生？](../Page/線上遊戲的老婆不可能是女生？.md "wikilink")（**西村英騎／盧西安**、皮洛斯基）
  - [文豪Stray Dogs](../Page/文豪Stray_Dogs.md "wikilink")（**谷崎潤一郎**）
  - [B-PROJECT〜鼓動＊Ambitious〜](../Page/B-PROJECT.md "wikilink")（**金城剛士**）
  - [DAYS](../Page/DAYS_\(漫畫\).md "wikilink")（**今歸仁翔**）
  - [數碼暴龍宇宙 應用怪獸](../Page/數碼暴龍宇宙_應用怪獸.md "wikilink")（**桂零**）
  - [侍靈演武：將星亂](../Page/侍靈演武.md "wikilink")（周倉）
  - [黑白來看守所](../Page/黑白來看守所.md "wikilink")（九十九）
  - [勇利\!\!\! on ICE](../Page/勇利!!!_on_ICE.md "wikilink")（**勝生勇利**）
  - [排球少年！！烏野高中VS白鳥澤學園高中](../Page/排球少年！！.md "wikilink")（白布賢二郎）

**2017年**

  - [鎖鏈戰記 ～赫克瑟塔斯之光～](../Page/鎖鏈戰記.md "wikilink")（**凱恩**）
  - [黑白來看守所 第2期](../Page/黑白來看守所.md "wikilink")（九十九）
  - [MARGINAL\#4 由KISS開始創造的Big
    Bang](../Page/MARGINAL＃4.md "wikilink")（**牧島沙伊**）
  - [將國戡亂記](../Page/將國戡亂記.md "wikilink")（約翰·弗倫岑）
  - [辣妹與我的第一次](../Page/辣妹與我的第一次.md "wikilink")（坂本慎平、消極純一）
  - [Gamers 電玩咖！](../Page/Gamers_電玩咖！.md "wikilink")（**上原祐**）
  - [TSUKIPRO THE
    ANIMATION](../Page/TSUKIPRO_THE_ANIMATION.md "wikilink")（**大原空**）

**2018年**

  - [BASILISK～櫻花忍法帖～](../Page/BASILISK～櫻花忍法帖～.md "wikilink")（孔雀啄）
  - [Butlers～千年百年物語～](../Page/Butlers～千年百年物語～.md "wikilink")（**緋櫻春人**）
  - [魔法少女 我](../Page/魔法少女_我.md "wikilink")（御翔桃拾）
  - [琴之森](../Page/琴之森.md "wikilink")（**平田光生**）
  - [東京喰種：re](../Page/東京喰種：re.md "wikilink")（**永近英良**）
  - [美男高校地球防衛部HAPPY
    KISS！](../Page/美男高校地球防衛部HAPPY_KISS！.md "wikilink")（臼井一）
  - [千銃士](../Page/千銃士.md "wikilink")（ベルガー）
  - [Free\!-Dive to the
    Future-](../Page/Free!.md "wikilink")（**椎名旭**\[3\]）
  - [強風吹拂](../Page/強風吹拂.md "wikilink")（**清瀨灰二**\[4\]）
  - [我喜歡的妹妹不是妹妹](../Page/我喜歡的妹妹不是妹妹.md "wikilink")（店員）

**2019年**

  - B-PROJECT ～絕頂＊Emotion～（**金城剛士**）

### OVA

**2002年**

  - [橫濱購物紀行－Quiet Country Cafe－](../Page/橫濱購物紀行.md "wikilink")（高廣）※聲優出道作

**2006年**

  - [網球王子 OVA 全国大会篇](../Page/網球王子_\(動畫\).md "wikilink")（葵劍太郎）

**2008年**

  - （**久遠寺睦**（16歲））

**2009年**

  - [我的狐仙女友 ～真夏的大謝肉祭～](../Page/我的狐仙女友.md "wikilink")（**源多由良**）

**2011年**

  - [古拉爾騎士團 Evoked THE Beginning
    white](../Page/古拉爾騎士團_Evoked_THE_Beginning_white.md "wikilink")
    （**ケンセイ**）
  - [這個男子，能與宇宙人作戰](../Page/這個男子，能與宇宙人作戰.md "wikilink") （**有川**）

**2012年**

  - [CØDE:BREAKER](../Page/CØDE:BREAKER.md "wikilink")（**天宝院遊騎**）

**2013年**

  - [最遊記外傳 特別篇 香花之章](../Page/最遊記#OVA「最遊記外傳」.md "wikilink")（鯉昇）

**2014年**

  - [我的腦內戀礙選項](../Page/我的腦內戀礙選項.md "wikilink")（**甘草奏**）
  - [BLB](../Page/BLB.md "wikilink")（**江角司**）\[5\]

**2015年**

  - [無頭騎士異聞錄 DuRaRaRa\!\!×2 承 外傳！？ 第4.5話
    我的心是火鍋的形狀](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**龍之峰帝人**）※劇場先行上映\[6\]

**2016年**

  - [少女們向荒野進發](../Page/少女們向荒野進發.md "wikilink")（**甲斐亞登夢**）

**2018年**

  - [Free\!－Dive to the
    Future－第](../Page/Free!.md "wikilink")0話（**椎名旭**）

### 動畫電影

**2005年**

  - [網球王子 OVA 全国大会篇](../Page/網球王子_\(動畫\).md "wikilink")（葵劍太郎）

**2008年**

  - [地下巧克力](../Page/地下巧克力.md "wikilink")（**漢托利·漢特**）\[7\]

**2014年**

  - [偶像學園](../Page/偶像學園_\(動畫\).md "wikilink")（涼川直人）

**2015年**

  - [High Speed\! -Free\! Starting
    Days-](../Page/Free!.md "wikilink")（**椎名旭**）

**2016年**

  - [電影版 聲之形](../Page/電影版_聲之形.md "wikilink")（**真柴智**）

**2017年**

  - [煙花](../Page/煙花_\(劇場版\).md "wikilink")（和弘）
  - [劇場版 Free\!-Timeless Medley-](../Page/Free!.md "wikilink")（椎名旭）
  - 特別版 Free\!-Take Your Marks-（椎名旭）

### 網路動畫

**2007年**

  - （**久遠寺睦**）

**2018年**

  - [B：The
    Beginning](../Page/B：The_Beginning.md "wikilink")（**布萊恩·布蘭登**）

### 電視劇

**1997年**

  - [超人力霸王帝納](../Page/超人帝拿.md "wikilink")（每日放送） - たっちゃん（辰雄）

**2005年**

  - [電車男](../Page/電車男_\(電視劇\).md "wikilink")（-{骨川洒落男}-）
  - [彼氏宣誓\!\!](../Page/彼氏宣誓!!.md "wikilink")（淳治）

**2006年**

  - [折翼的天使們](../Page/折翼的天使們.md "wikilink") 第三夜「アクトレス」
  - [交響情人夢](../Page/交響情人夢_\(電視劇\).md "wikilink")（小林）

**2007年**

  - [ハッピィ★ボーイズ](../Page/ハッピィ★ボーイズ.md "wikilink") \#9,10（高橋）

**2008年**

  - [今日は渋谷で6時](../Page/今日は渋谷で6時.md "wikilink") - 利行
  - [33分偵探](../Page/33分偵探.md "wikilink") \#9 - 進藤
  - [Room Of King](../Page/Room_Of_King.md "wikilink") - 看護師

### 電影

**1997年**

  - [學校怪談3](../Page/學校怪談_\(電影\)#「學校怪談3」（1997年）.md "wikilink")（木村悟）

**2002年**

  - [夢、おいかけて](../Page/夢、おいかけて.md "wikilink")（圭司）

**2003年**

  - 1980（中文譯名：羽柴家的1980年）（物集）
  - [大逃殺2](../Page/大逃殺2.md "wikilink")（日笠将太）

**2004年**

  - [この世の外へ クラブ進駐軍](../Page/この世の外へ_クラブ進駐軍.md "wikilink")
    （中文譯名：向世外的俱樂部進軍）

**2005年**

  - [絶対恐怖 Pray プレイ](../Page/絶対恐怖_Pray_プレイ.md "wikilink")（シマ）
  - [琳達！琳達！](../Page/琳達！琳達！.md "wikilink")（池沢）

**2006年**

  - [ザ・コテージ](../Page/ザ・コテージ.md "wikilink")（甲斐智則）

**2012年**

  - [死ガ二人ヲワカツマデ...第二章「南瓜花-nananka-」](../Page/死ガ二人ヲワカツマデ...第二章「南瓜花-nananka-」.md "wikilink")
    - **中里貴春**

### 舞台劇

  - [アルゴミュージカル](../Page/アルゴミュージカル.md "wikilink")「Sing for You, Sing for
    Me.」（1995年）
  - [四季劇團](../Page/四季劇團.md "wikilink")「[美女與野獸](../Page/美女與野獸.md "wikilink")」
  - [網球王子舞台劇](../Page/網球王子舞台劇.md "wikilink")
  - [理由なき反抗](../Page/理由なき反抗.md "wikilink")

### 遊戲

  - [少女們向荒野進發](../Page/少女們向荒野進發.md "wikilink")（**甲斐亞登夢**）
  - 在茜色的世界中與君詠唱（沖田總司）

**2015年**

  - 夢色キャスト（**橘蒼星**\[8\]）
  - アイ★チュウ（**御劍晃**）
  - 夢王國與沉睡中的100位王子殿下（亞迪艾爾）

**2017年**

  - [A3！](../Page/A3！.md "wikilink")（**有栖川誉**）
  - B-PROJECT 無敵危險 (金城剛士)

**2018年**

  - DREAM\!ing (針宮藤次)
  - オンエア\! (輝崎螢)
  - ピオフィオーレの晩鐘（ダンテ）

## 注釋

<references />

## 外部連結

  - [官方網站](https://web.archive.org/web/20070112022105/http://www.set1979.com/main/eihoubu/profile_e05toyonaga.htm)

  - [豊永利行的日記](http://ameblo.jp/tossy-t/)

  - [豊永利行的日記(舊)](http://set.cocolog-nifty.com/toshi/)

  -
  - [BAKAIWA Official Blog](http://bakaiwa.cocolog-nifty.com/about.html)

[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:聲優獎主演男優獎得主](../Category/聲優獎主演男優獎得主.md "wikilink")

1.
2.
3.
4.
5.  <http://www.st-noix.jp/blb/chara.html> BLB官方網站
6.  第4.5話 劇場上映{{\!}}電視動畫「無頭騎士異聞錄
    DuRaRaRa\!\!×2」官方網站|accessdate=2015年7月29日|author=
    |date= |publisher= |language=ja}}
7.   地下巧克力官方網站
8.