**新丰特**（），是[意大利](../Page/意大利.md "wikilink")[罗马省的一个](../Page/罗马省.md "wikilink")[市镇](../Page/市镇_\(意大利\).md "wikilink")。总面积20.15平方公里，人口28210人，人口密度1400.0人/平方公里（2009年）。ISTAT代码为058122。

## 外部链接

  - [Official
    website](https://web.archive.org/web/20080705114157/http://www.comune.fonte-nuova.rm.it/)

[F](../Category/罗马省市镇.md "wikilink")
[Category:罗马省市镇](../Category/罗马省市镇.md "wikilink")