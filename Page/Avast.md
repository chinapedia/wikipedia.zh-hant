****是位於[捷克](../Page/捷克.md "wikilink")[布拉格的](../Page/布拉格.md "wikilink")（舊稱ALWIL
Software
a.s.\[1\]）於1988年首次發行的[防毒](../Page/防毒軟體.md "wikilink")[軟體](../Page/電腦程式.md "wikilink")，軟體名取自「Anti-Virus-Advanced-Set」，即“高級防毒軟體”。\[2\]其中央掃描引擎成功通過了[ICSA實驗室](../Page/ICSA實驗室.md "wikilink")[認證以及西海岸實驗室的測試過程](../Page/認證.md "wikilink")，\[3\]軟體亦結合有反[間諜軟體功能](../Page/間諜軟體.md "wikilink")、反[Rootkit功能以及自我保護能力](../Page/Rootkit.md "wikilink")。截至2009年12月10日，已有一億用户註冊使用；至2010年9月，全球注册人数超过一億三千萬人次。\[4\]软件中文名有「艾維斯特」\[5\]、“愛維士”\[6\]及[昵称](../Page/昵称.md "wikilink")“小a”（“大A”对应[AVG
Anti-Virus](../Page/AVG_Anti-Virus.md "wikilink")，已于2016年9月被avast公司[并购](../Page/并购.md "wikilink")\[7\]）。

avast\!分成家用用途的[免費版本以及企業和專業用戶的付費版本](../Page/免費軟體.md "wikilink")。其中免費版本的avast\!可安裝於[Microsoft
Windows和](../Page/Microsoft_Windows.md "wikilink")[Linux用戶](../Page/Linux.md "wikilink")，\[8\]
因此許多使用者經常將此與[AVG杀毒软件和](../Page/AVG杀毒软件.md "wikilink")[Avira
AntiVir的免費版本作比較](../Page/Avira_AntiVir.md "wikilink")。付費版本的avast\!主要提供給企業和專業用戶，擁有其他免費版所沒有的附加功能，包括能較早更新程式檔、以命令來執行掃描等，但現在免費版也提供[腳本攔截器](../Page/腳本語言.md "wikilink")。\[9\]

自2010年起，Avast在台灣、香港及部分中國大陸地區交由棋勝科技代理，官方網址為 <http://avast.it4win.com/>
2017年10月，Avast收購AVG並完成整合，正式推出新產品線。

## 特色

### 核心和檢測

  - avast\! Community
    IQ：透過用戶群中的數據蒐集，經過一系列分析後相互分享Anti-rootkit的模組資料，進而防範之。
  - 反[rootkit功能](../Page/rootkit.md "wikilink")（Anti-rootkit
    built-in）：藉由GMER自動檢查技術，可檢查並標記Rootkit底下加載的驅動程式，如此一來即使是檢測到未知種類的Rootkit，也能在執行惡意運作前加以阻止，並安全地刪除Rootkit。
  - 反[間諜軟體功能](../Page/間諜軟體.md "wikilink")（Anti-spyware built-in）
  - 代碼模擬器：自Avast\!
    5.0開始安裝，在手動掃描和自動掃描時遇到可疑的執行程序時，avast\!能夠在[沙盒中模擬程序的代碼運作](../Page/沙盒_\(電腦安全\).md "wikilink")，運作時並不會傷害[系統](../Page/系統.md "wikilink")。而在技術上不同於傳統的模擬技術，其模擬程序代碼式採較快的動態轉換完成。
  - 檢測不必要程序：為avast\!
    5.0版本中新增的功能，可以檢測如[遠端控制軟體和商業](../Page/遠端控制軟體.md "wikilink")[密鑰記錄等不必要的程序](../Page/密鑰.md "wikilink")，並自定程序來處理這些類型的程式。
  - 啟發式引擎：從avast\!
    5.0版本開始配備一個新型啟發式引擎，可主動檢測具有常見病毒定義碼、但無法實際驗證的[惡意軟件](../Page/惡意軟件.md "wikilink")，亦可檢測[二進制檔案](../Page/二進制檔案.md "wikilink")（執行文件）和惡意腳本軟件。
  - 掃描引擎：已經ICSA等認證過的掃描引擎，能有效防禦[電腦病毒](../Page/電腦病毒.md "wikilink")、[間諜軟體和其他形式](../Page/間諜軟體.md "wikilink")[惡意軟體](../Page/惡意軟體.md "wikilink")。除了一般執行檔外，還可掃瞄[ARJ](../Page/ARJ.md "wikilink")、[ZIP](../Page/ZIP_\(檔案格式\).md "wikilink")、[MIME](../Page/MIME.md "wikilink")、[RAR](../Page/RAR.md "wikilink")、[TAR](../Page/tar_\(计算机科学\).md "wikilink")、[gzip](../Page/gzip.md "wikilink")、[CAB](../Page/CAB.md "wikilink")、[bzip2](../Page/bzip2.md "wikilink")、[LHA](../Page/LHA.md "wikilink")、[Cpio](../Page/Cpio.md "wikilink")、[CHM](../Page/CHM.md "wikilink")、[7-Zip和](../Page/7-Zip.md "wikilink")[SIS等壓縮檔內容](../Page/SIS_\(文件格式\).md "wikilink")，並支援如[UPX等檔案壓縮器](../Page/UPX.md "wikilink")，以及隱藏在[NTFS卷宗交換資料流的病毒](../Page/NTFS.md "wikilink")。

### 掃描和殺毒

  - avast\! Intelligent
    Scanner：根據已證實安全可靠的[應用程式名單](../Page/應用程式.md "wikilink")，將需要掃描檔案的數量減少80%。除非該檔案另有更改，否則系統會直接視為安全狀態。
  - avast\! Virus
    Cleaner：從版本4.1開始安裝，這也使得avast\!開始能從感染電腦上完全移除電腦病毒檔案。而在某些緊急情況下，avast\!
    Virus Cleaner可當作獨立產品使用。
  - 快速掃描（Quick Scan）：只掃描系統重要磁區與記憶體，掃瞄進度以百分比顯示。
  - 開機掃描（Boot-time Scan，只有在[32位的](../Page/32位.md "wikilink")[Microsoft
    Windows](../Page/Microsoft_Windows.md "wikilink")[2000](../Page/Windows_2000.md "wikilink")/[XP](../Page/Windows_XP.md "wikilink")/[Vista](../Page/Windows_Vista.md "wikilink")/[7](../Page/Windows_7.md "wikilink")）：使用戶可以自行安排開機掃描，以消除藏在[硬碟中](../Page/硬碟.md "wikilink")、透過Windows[作業系統啟動過程來激活的電腦病毒](../Page/作業系統.md "wikilink")，自4.8版本開始以百分比來顯示進度。
  - 喚醒掃描：當Windows進入休眠模式時執行計劃掃描，結束後再返回到休眠模式。
  - 計劃掃描：允許用戶自行定義掃描時間和要掃描的檔案。
  - 全系統掃描（Full System Scan）：深層掃描整個系統。
  - 自訂資料夾掃描（Select folder to scan）：讓使用戶選擇要掃描的資料夾或硬碟。
  - 移除式媒體掃描（Removable media
    scan）：掃描所有目前連接在電腦上的可移除[磁碟](../Page/磁碟.md "wikilink")。
  - 螢幕保護程式：在執行病毒掃瞄時，avast\!在預設上可與[螢幕保護裝置共同工作](../Page/螢幕保護裝置.md "wikilink")。

### 常駐系統防護

每一個功能皆可以單獨管理或禁用：

  - Behavioral Honeypot：可識別和監控所選電腦上的可疑檔案活動，並自動提交到病毒實驗室以供進一步分析。
  - P2P防護（P2P
    Shield）：掃描自[點對點技術藉由](../Page/點對點技術.md "wikilink")[檔案分享下載而來的檔案](../Page/檔案分享.md "wikilink")，可支援[Vuze](../Page/Vuze_\(軟體\).md "wikilink")、[BitTorrent](../Page/BitTorrent_\(軟體\).md "wikilink")、[BitComet](../Page/BitComet.md "wikilink")、[eDonkey2000](../Page/eDonkey2000.md "wikilink")、[eMule](../Page/eMule.md "wikilink")、[LimeWire](../Page/LimeWire.md "wikilink")、[Shareaza](../Page/Shareaza.md "wikilink")、[μTorrent](../Page/μTorrent.md "wikilink")、[WinMX等軟體](../Page/WinMX.md "wikilink")。
  - 即時信息防護（IM
    Shield）：掃描藉由[即時通訊所傳輸的檔案和應用程式](../Page/即時通訊.md "wikilink")，支援有[AOL即時通訊](../Page/AOL即時通訊.md "wikilink")、[Pidgin](../Page/Pidgin.md "wikilink")、[Google
    Talk](../Page/Google_Talk.md "wikilink")、[ICQ](../Page/ICQ.md "wikilink")、[Miranda
    IM](../Page/Miranda_IM.md "wikilink")、[mIRC](../Page/mIRC.md "wikilink")、[Windows
    Live Messenger](../Page/Windows_Live_Messenger.md "wikilink")
    、[Psi](../Page/Psi_\(即時通訊軟體\).md "wikilink")、[騰訊QQ](../Page/騰訊QQ.md "wikilink")、[Skype](../Page/Skype.md "wikilink")、[Trillian](../Page/Trillian.md "wikilink")、[Yahoo\!奇摩即時通等軟體](../Page/Yahoo!奇摩即時通.md "wikilink")。
  - 行為防護（Behavior Shield）：自Avast\! 5.0開始安裝，能對可疑行為分析，進而提出行為報告方案。
  - 自我保護（Self-protection）：防止惡意軟體終止avast\!運作，或者破壞avast\!的程式檔案。早期是以[資料庫來儲存原始資料](../Page/資料庫.md "wikilink")，而在avast\!
    5.0中改已別種方式保存。
  - 郵件防護（Mail Shield）：掃描在[電子郵件如](../Page/電子郵件.md "wikilink")[Microsoft
    Outlook](../Page/Microsoft_Outlook.md "wikilink")/[Outlook
    Express](../Page/Outlook_Express.md "wikilink")、[Microsoft Exchange
    Server等的郵件和附件](../Page/Microsoft_Exchange_Server.md "wikilink")，藉由[啟發式方法除去內容中可能潛藏的病毒](../Page/啟發法.md "wikilink")。其中對於Microsoft
    Outlook使用特殊插件防範；其他電子郵件客戶則由常規[POP3](../Page/POP3.md "wikilink")/[IMAP4](../Page/IMAP4.md "wikilink")/[SMTP代理保護](../Page/SMTP.md "wikilink")。在Avast\!
    5.0中，還新增了支援掃描[SSL通信](../Page/SSL.md "wikilink")（包括[gmail等軟體運用](../Page/gmail.md "wikilink")）的功能。
  - [檔案系统防護](../Page/檔案系统.md "wikilink")（File System
    Shield）：主要的防護，藉由掃描檔案來避免有毒檔案於電腦中運作，來防止病毒和其他惡意軟體的威脅；此外亦對已執行檔案進行實時掃描。
  - 網絡防護（Network
    Shield）：分為以專門用於阻止惡意鏈接的[URL阻止程序和](../Page/URL.md "wikilink")[入侵檢測系統兩部分](../Page/入侵檢測系統.md "wikilink")，來防止[電腦病毒或](../Page/電腦病毒.md "wikilink")[電腦蠕蟲滲入](../Page/電腦蠕蟲.md "wikilink")[電腦](../Page/電腦.md "wikilink")。
  - [網頁防護](../Page/網頁.md "wikilink")（Web
    Shield）：掃描所瀏覽的的網頁，並檢查所有從網站下載的檔案、頁面和[java腳本](../Page/java.md "wikilink")。由於有了智能數據流掃描功能，網頁防護理論上並不會降低網頁瀏覽速度。

### 更新

  - 智能病毒定義碼更新：更新時系統會盡可能減少常規更新檔案的大小。
  - 自動更新：藉由更新程式和病毒[數據庫](../Page/數據庫.md "wikilink")，用於識別更多的潛在的威脅。avast\!會定期更新病毒數據庫（有時一日會有很多次），而這個過程是自動默認的，在預設值上是設定成每四小時自動檢查一次病毒碼更新，順利更新後會有彈出視窗和語音提示。
  - 快速應用更新：病毒定義碼的文件新格式可加速更新，並且減少對[CPU內存的需求](../Page/CPU.md "wikilink")，從而不會干擾電腦的使用。
  - 靜默/遊戲模式：將自動檢測全屏應用程序，並禁用彈出窗口和其他屏幕提示。在「Off」狀態時，表示會有彈出視窗的通知提醒，但是如果進入全螢幕操作時，就會自動關閉彈出視窗功能；而在「On」的狀態，表示強制任何時候都關閉各種彈出視窗提醒。
  - 有聲提示：會藉由發出聲音警告和字幕來提示使用者。

### CPU優化

  - avast\!掃描引擎優化：在代碼上進行了優化，自avast\!5.0開始不再像過去一樣，同時開啟很多執行程序（舊版中使用七種防禦模式要打開七道處理程序），新版中把常駐程序精簡（只剩兩道處理程序），總體記憶體佔用也有大幅降低：
  - 多行程掃描優化：可有效運用[多核心CPU運行速度更快的優勢](../Page/多核心.md "wikilink")，允許在兩內核之間分配工作，從而加速掃描進程。[Intel地區理事Wolfgang](../Page/Intel.md "wikilink")
    Petersen便曾說：「Intel為不斷求進的客戶們提供高效能的處理器。我們很高興看到avast\!能夠優化其安全解決方案，來迎合我們最近技術帶來的優勢，為用戶們帶來更多效益。」\[10\]
  - 綠色計算：降低對硬碟的需求，從而減少資源消耗。

### 沙盒

  - 這是 avast\! 全功能防毒軟體以及 avast\!
    網絡安全軟體的全新功能，可以讓用戶在一個完全安全的環境內瀏覽網頁或是執行應用程式。可以在沙箱中執行任何用戶認為可疑的程式，看它有沒有出現任何惡意的行為來決定此程式安不安全。

### 其他

  - avast\! iTrack：實時圖形掃描報告。
  - 病毒隔離區：使受感染的文件仍可以被保存。該病毒主體將被儲存於另外的隔離區中，以避免傷害其他軟體，用戶亦可以自行添加檔案於病毒隔離處。
  - 多國語言界面：包括了[简体中文](../Page/简体中文.md "wikilink")、[繁体中文](../Page/繁体中文.md "wikilink")、[保加利亞語](../Page/保加利亞語.md "wikilink")、[捷克語](../Page/捷克語.md "wikilink")、[荷蘭語](../Page/荷蘭語.md "wikilink")、[英語](../Page/英語.md "wikilink")、[愛沙尼亞語](../Page/愛沙尼亞語.md "wikilink")、[芬蘭語](../Page/芬蘭語.md "wikilink")、[法語](../Page/法語.md "wikilink")、[德語](../Page/德語.md "wikilink")、[希臘語](../Page/希臘語.md "wikilink")、[匈牙利語](../Page/匈牙利語.md "wikilink")、[意大利語](../Page/意大利語.md "wikilink")、[日語](../Page/日語.md "wikilink")、[韓語](../Page/韓語.md "wikilink")、[挪威語](../Page/挪威語.md "wikilink")、[波蘭語](../Page/波蘭語.md "wikilink")、[巴西葡萄牙語](../Page/巴西葡萄牙語.md "wikilink")、[葡萄牙語](../Page/葡萄牙語.md "wikilink")、[俄語](../Page/俄語.md "wikilink")、[斯洛伐克語](../Page/斯洛伐克語.md "wikilink")、[斯洛文尼亞語](../Page/斯洛文尼亞語.md "wikilink")、[西班牙語](../Page/西班牙語.md "wikilink")、[瑞典語](../Page/瑞典語.md "wikilink")、[土耳其語](../Page/土耳其語.md "wikilink")、[烏克蘭語](../Page/烏克蘭語.md "wikilink")、[越南語等版本](../Page/越南語.md "wikilink")。「當人們可以用自己的母語讀出『病毒數據庫已經更新』，他們會有更強的安全感。」AVAST
    Software a.s.軟件公司的首席技術官Ondrej
    Vlcek說道：「avast\!的用戶遍及世界上238個國家和地區，如果軟件語言是用戶當地的語言，那麼軟件在不同地區推廣的過程中影響力會更大。」\[11\]
  - [圖形使用者介面](../Page/圖形使用者介面.md "wikilink")：自Avast\!
    5.0開始將舊版中控制台和掃毒器分離的操作方式取消，合併成單一介面方便使用者使用。用戶可直接在介面上啟動手動掃瞄、處理掃瞄結果和變更設定等，而其他基本的常註防護設定亦在此更改，此外，介面的外觀是可更換的，除了原本預設的三款外，亦可藉由官方的[ZIP壓縮檔案自由更換介面](../Page/ZIP_\(檔案格式\).md "wikilink")。
  - 自動處理：無需用戶指令便能自動處理受感染的文件。

### 付費版本

  - avast\!
    Sandbox：使潛在可利用程序（如[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")）或可疑的應用程式在安全的虛擬環境下運行。
  - 反垃圾郵件：自Avast\!
    5.0開始安裝，主體有反[垃圾郵件程式和欺詐信息過濾器](../Page/垃圾郵件.md "wikilink")，可作為MS
    Outlook插件以及其他電子郵件客戶端的常規POP3/IMAP4運行。
  - 命令執行：直接以命令要求程式進行掃描。
  - 腳本攔截：掃描[系統](../Page/系統.md "wikilink")、[網頁中的惡意](../Page/網頁.md "wikilink")[腳本](../Page/腳本語言.md "wikilink")，並阻止這些惡意腳本運行來防止其感染電腦，但在[Internet
    Explorer](../Page/Internet_Explorer.md "wikilink")、[網景領航員](../Page/網景領航員.md "wikilink")、[Mozilla
    Firefox等瀏覽器上時](../Page/Mozilla_Firefox.md "wikilink")，網頁仍可以使用（如：點選按鈕等）。
  - 靜默[防火牆](../Page/防火牆_\(計算機\).md "wikilink")：自Avast\!
    5.0開始安裝，透過防火牆可以控制進出電腦的流量，其防護結合了啟發式、行為分析以及已知安全的白名單執行，根據連接類型有三種網絡設置可供選擇。

## 2011年推出的版本 (6.0介面/新核心)

6.0免費版加入sandbox、腳本防護及網站評價系統，avast\!主要產品系列包括：\[12\]\[13\]\[14\]

  - avast\! Free Antivirus —
    為[免費軟體](../Page/免費軟體.md "wikilink")，並不能用於商業用途。\[15\]
  - avast\! Pro Antivirus —
    為[共享軟體](../Page/共享軟體.md "wikilink")，主要提供給個人和商業用途使用，軟體中並不包含avast\!網絡安全軟體的防火牆和反垃圾郵件兩項功能。\[16\]\[17\]
  - avast\! Internet Security —
    為[共享軟體](../Page/共享軟體.md "wikilink")，主要提供給個人和商業用途使用。\[18\]\[19\]
  - avast\! Business Protection (2011年新版|6.0介面) —
    具中控台介面，含ProAntivirus完整功能，適用於Windows
    XP/Vista/7/2003/2008/2011(SBS)等。\[20\]
  - avast\! Business Protection Plus (2011年新版|6.0介面) — 具中控台介面，含Interner
    Security完整功能，適用於Windows XP/Vista/7/2003/2008/2011(SBS)等。\[21\]

### 其他版本(4.x介面/舊核心)

  - avast\! [PDA](../Page/PDA.md "wikilink") Edition — 為共享軟體，主要於[Windows
    Mobile](../Page/Windows_Mobile.md "wikilink")、[Windows Embedded
    Compact和](../Page/Windows_Embedded_Compact.md "wikilink")[Palm
    OS運作](../Page/Palm_OS.md "wikilink")。
  - avast\! Professional Family Pack —
    為共享軟體，提供給在擁有多台電腦的家庭。內包括10個已認證的avast\!Professional
    Edition軟體和avast\! Windows Home Server Edition。
  - avast\! Small Business Server Edition —
    為共享軟體，主要用於Windows系列的小型企業伺服器，包括[Microsoft
    Exchange
    Server或](../Page/Microsoft_Exchange_Server.md "wikilink")[ISA伺服器](../Page/ISA.md "wikilink")。
  - avast\! Server Edition — 為企業共享軟體，主要運行在[Windows
    Server](../Page/Windows_Server.md "wikilink")，包括[Windows 2000
    Server](../Page/Windows_2000_Server.md "wikilink")、[Windows Server
    2003等](../Page/Windows_Server_2003.md "wikilink")。\[22\]
  - avast\! Windows Home Server Edition —
    為共享軟體，使用於Windows[家用伺服器上](../Page/家用伺服器.md "wikilink")。
  - avast\! BART CD — 為共享軟體，可當作可開機防毒和恢復工具使用，以拿來檢測和清除感染病毒。
  - avast\! Distributed Network Manager —
    允許[網路管理員管理的avast](../Page/網路管理員.md "wikilink")\!產品，可一次讓防護範圍擴及整個公司。
  - avast\! Free Online Scanner（免费）
  - avas\!t Free Virus Cleaner（免费）
  - avast\! Kerio Edition — 為共享軟體，只能安裝於產品，如其安全郵件伺服器。
  - avast\! Linux Home Edition —
    為免费軟體，提供給個人而非商業用途，主要用於在[Linux系統上](../Page/Linux系統.md "wikilink")。
  - avast\! Linux/Unix Edition —
    共享軟體，主要使用在[Linux和以](../Page/Linux.md "wikilink")[x86](../Page/x86.md "wikilink")[核心運行的BSD伺服器上](../Page/核心.md "wikilink")。軟體亦可於[Debian或](../Page/Debian.md "wikilink")[tar上運作](../Page/tar_\(计算机科学\).md "wikilink")。\[23\]
  - avast\! Mac Edition — 為共享軟件，適用於[Mac平台上](../Page/Mac.md "wikilink")。
  - avast\! U3 Edition — 為共享軟體，用於保護免於病毒和其他惡意軟體的攻擊。
  - avast\! WHS Edition

### 企業套裝 (4.x介面/舊核心)

  - avast\! Advanced Suite(舊版|4.x介面) — 包含avast\! Professional Edition
    、avast\! Server Edition、avast\! Exchange Server Edition、avast\!
    Linux/Unix Edition 和 avast\! Distributed Network Manager。\[24\]
  - avast\! Enterprise Suite(舊版|4.x介面)— 包含avast\! Professional Edition
    、avast\! Server Edition、avast\! Linux/Unix Edition、avast\! Kerio
    Edition、avast\! PDA Edition 和 avast\! Distributed Network
    Manager。\[25\]
  - avast\! Small Business Server Suite(舊版|4.x介面) — 包含avast\!
    Professional Edition 4.8、avast\! Small Business Server Edition 和
    avast\! Distributed Network Manager。
  - avast\! Standard Suite(舊版|4.x介面) — 包含avast\! Professional Edition
    、avast\! Server Edition和avast\! Distributed Network Manager。\[26\]

### 免費家用版

avast\!防毒軟體可以透過網路下載免費使用30天。而30天的測試版必須免費註冊，以得到[金鑰讓程式可以繼續運作](../Page/公開金鑰基礎建設.md "wikilink")，註冊後Avast\!將可以持續免費使用12個月。\[27\]
12個月後，如果要再繼續免費使用則必須重新註冊。但如果用戶版本為4.8、且已升級到最新版本時，則可以自動使用14個月。在使用期限內，並不會有任何要求升級成商業版的彈出視窗，病毒碼亦會自動更新，更新和排程掃描功能也沒有限制。而自avast\!
5.0版開始不需要另外收取認證信、認證碼，而是直接在軟體介面中即可完成免費註冊，同樣獲得一年的免費使用期（4.8版本則仍為14個月）。\[28\]

2010年4月12日，avast\! Free Antivirus 5.0推出後，便是以市面上的防毒產品作為性能基準。在2010年9月的AV
Comparatives測試中，avast\!擁有最快的掃描速度，相對於[諾頓防毒或者](../Page/諾頓防毒.md "wikilink")[AVG快約](../Page/AVG.md "wikilink")30%，甚至比[Microsoft
Security
Essentials快近三倍左右](../Page/Microsoft_Security_Essentials.md "wikilink")，而檢測率達到99.3%的成功率。\[29\][英國](../Page/英國.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")《[Virus
Bulletin](../Page/Virus_Bulletin.md "wikilink")》的技術顧問兼測試工作隊理事John
Hawes在2010年1月的雜誌中寫道「這個產品比許多同行的產品都要優勝，在產品界面也做得非常出色；新的套裝版本添加了一些有趣且實用的功能，而免費版亦完全免費供家庭用戶使用，這簡直是一個奇蹟。」\[30\]

## 與Google合作

2009年時，與[Google合作計劃](../Page/Google.md "wikilink")，當使用者正在執行avast\!防毒軟體的安裝程式，而電腦上尚未安裝[Google
Chrome時](../Page/Google_Chrome.md "wikilink")，便可以選擇同時安裝Google
Chrome。\[31\]
儘管許多程式在安裝時，都會提供選項讓使用者選擇要安裝其他程式，因此這也並不能說是一項創舉。但avast\!安裝程式詢問是否安裝Google
Chrome時，只有當使用者電腦尚未安裝Google
Chrome的情況下，提示視窗才會彈出；且讓使用者自行主動選擇安裝，而在預設狀態中，並沒有事先在選項「是，安裝」或「不，不安裝」上勾選，完全由使用者自行決定。Google這次行銷計劃是看中avast\!的使用者人數比Google
Chrome多將近一倍以上。Google發言人便表示：「我們會持續探尋各種方式，把Google
Chrome帶給更多的民眾。這可能包括透過眾多管道發布，我們與Avast合作的方式即為其中一例。」\[32\]

此外，雖然AVAST防毒軟體並不支援[Windows
95](../Page/Windows_95.md "wikilink")、[Windows
98](../Page/Windows_98.md "wikilink")、[Windows
Me](../Page/Windows_Me.md "wikilink")、[Windows
NT等作業軟體](../Page/Windows_NT.md "wikilink")，但無論在[法語](../Page/法語.md "wikilink")、[德語](../Page/德語.md "wikilink")、[義大利語](../Page/義大利語.md "wikilink")、[捷克語](../Page/捷克語.md "wikilink")、[西班牙語](../Page/西班牙語.md "wikilink")、[葡萄牙語](../Page/葡萄牙語.md "wikilink")、[俄語](../Page/俄語.md "wikilink")、[波蘭語等語言版本的](../Page/波蘭語.md "wikilink")[Google軟體集上](../Page/Google軟體集.md "wikilink")，便以avast\!
Free Antivirus 5.0取代[Spyware
Doctor的下載](../Page/Spyware_Doctor.md "wikilink")。\[33\]\[34\]

## 併購AVG Technologies

Avast 於 2016 年 9 月併購 AVG Technologies。

## 獎項和測試結果

[Avast\!_icon.png](https://zh.wikipedia.org/wiki/File:Avast!_icon.png "fig:Avast!_icon.png")

  - avast\! 4 Pro伺服器版本曾獲得SoftChecker最佳獎助。
  - avast\! Virus Cleaner 1.0.207曾獲得FiveSign伺服器類四星獎助。
  - 2006年：於Secure Computing SC獎中贏得讀者最佳信賴殺毒軟體獎。\[35\]
  - 2007年：於Secure Computing SC獎中贏得讀者最佳信賴殺毒軟體獎。
  - 2007年2月：[Virus Bulletin](../Page/Virus_Bulletin.md "wikilink") 100獎。
  - 2007年4月：Virus Bulletin 100獎。
  - 2008年2月：Virus Bulletin 100獎。
  - 2008年6月28日：Virus
    Bulletin於[2008年6月防毒軟體評測第](../Page/2008年6月.md "wikilink")13名。\[36\]
  - 2008年8月：Virus Bulletin 100獎。
  - 2008年8月：於AV Comparatives的測試中獲得「Advanced+」等級。\[37\]
  - 2008年10月：Virus Bulletin 100獎。
  - 2009年2月：Virus Bulletin 100獎。
  - 2009年2月：於AV Comparatives的測試中獲得「Advanced」等級。\[38\]
  - 2009年8月：於AV Comparatives的測試中獲得「Advanced+」等級。\[39\]\[40\]\[41\]
  - 2009年9月17日：Virus
    Bulletin於[2009年9月防毒軟體評測第](../Page/2009年9月.md "wikilink")18名。\[42\]
  - 2010年：於CNET安全初学者工具包（the CNET Security Starter Kit
    2010）中赢得最高防病毒选择。\[43\]
  - 2010年8月：於AV Comparatives的測試中獲得「Advanced+」等級。\[44\]

## 市場

2010年6月由公司的報告指出，avast\!免費[防毒軟體實際安裝用戶人數的世界排名中為第五名](../Page/防毒軟體.md "wikilink")，這項報告是依其端點裝置而非[營業額所整理出](../Page/營業額.md "wikilink")。\[45\]\[46\]
而在使用者人數上，有三分之二是經由朋友介紹才使用的，也因此大力推廣免費產品作為市場戰略。\[47\]

2010年8月23日，美國私募股權公司頂峰投資（）投資近1億美元於avast\!軟體上，AVAST公司的[首席執行官Vince](../Page/首席執行官.md "wikilink")
Steckler說：「頂峰投資的注資無疑是對我們『免費增值』經營模式的肯定，透過全功能免費產品與收費產品相混合的市場模式，這種經營手段已經打破了目前防毒軟體市場的傳統經營模式。我們無需採用[廣告模式或預裝avast](../Page/廣告.md "wikilink")\!方式，僅透過用戶口口相傳這途徑，同樣也能使avast\!業務規模獲得了持續穩定的增長。」並補充說道：「『免費增值』將是未來的主流，和頂峰投資一起，我們將能繼續我們的戰略，提供優質且免費的防病毒解決方案給所有的用戶；我們並沒有計劃順應傳統的零售模式去改變我們的做法。」\[48\]
而在這之前，Vince Steckler便已明確指出AVAST Software a.s.的銷售策略：

## 中國大陸封锁

2015年3月1日起，avast\!遭到中国大陆[防火長城封锁](../Page/防火長城.md "wikilink")，用户程序及病毒库无法获得更新。有网民猜测可能與avast\!早前推出的[VPN服务](../Page/VPN.md "wikilink")“安全线”（Avast
SecureLine
VPN）有关。\[49\]7月21日后，封锁被解除。但同时原先内置的“安全线”服务被防火長城定點屏蔽，無法使用。而avast\!官方也将网站上“安全线”中文版本介绍页面跳转至avast\!首页，英文版本網頁則顯示正常。

## 參見

  - [防毒軟體](../Page/防毒軟體.md "wikilink")
  - [防毒軟體列表](../Page/防毒軟體列表.md "wikilink")
  - [電腦病毒](../Page/電腦病毒.md "wikilink")

## 參考資料

## 外部链接

  - [Avast\!原廠指定之台灣香港中國代理商官方主页](http://www.it4win.com/)
  - [Avast\!中文版官方主页](http://www.avast.com/zh-cn/index)
  - [Avast\!中文版官方首頁](http://www.avast.com/zh-tw/index)
  - [杀毒软件排行榜](http://anti-virus-software-review.toptenreviews.com/)

[Category:Windows軟體](../Category/Windows軟體.md "wikilink")
[Category:免費軟體](../Category/免費軟體.md "wikilink")
[Category:安全軟體](../Category/安全軟體.md "wikilink")
[Category:殺毒软件](../Category/殺毒软件.md "wikilink")

1.  2010年6月1日變更其公司名

2.

3.

4.

5.  avast\! 艾維斯特- PChome線上購物

6.  香港商愛維士有限公司

7.

8.

9.

10.

11.

12.

13.

14.

15.

16.
17.

18.
19.

20.

21.

22.
23.
24.

25.

26.

27.

28.

29.

30.
31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.
48.

49. [中国封锁免费杀毒软件Avast](http://www.solidot.org/story?sid=43154)