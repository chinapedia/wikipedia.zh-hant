**詹姆斯·門羅**（**James
Monroe**，）[美國政治家](../Page/美國.md "wikilink")，1817–1825年担任第五任[美國總統](../Page/美國總統.md "wikilink")。门罗是美国独立革命时期最后一任总统，也是出生于弗吉尼亚州并连任的最后一位总统。他出生于弗吉尼亚州威斯特摩兰郡富裕的农场主之家，参加过美国独立战争，战后在弗吉尼亚州按照杰斐逊的指导学习法律，并在大陆会议上当了代表。

虽然以他命名的“[门罗主义](../Page/门罗主义.md "wikilink")”正是在他任上发展的，但是大部分内容来自他的[国务卿](../Page/美國国务卿.md "wikilink")[约翰·昆西·亚当斯](../Page/约翰·昆西·亚当斯.md "wikilink")。

## 早年生涯

出身於[蘇格蘭移民的小農場主家庭](../Page/蘇格蘭.md "wikilink")，家境並不富裕。

## 晚年生涯

他歸宿的橡樹莊園，是[杰斐逊親自幫他設計的](../Page/托马斯·杰斐逊.md "wikilink")。與當時其他總統一樣，由於總統薪俸根本不足以支付開支，離任時審計發現，他原有的莊園由於賣地還債，已經剩餘不多了。於是他致信當時總統[约翰·昆西·亚当斯](../Page/约翰·昆西·亚当斯.md "wikilink")，要求美國政府補償對他的拖欠並請求[國會援助](../Page/美國國會.md "wikilink")，否則他將難以應付退休後的生活。但無結果。這樣他不得不賣掉[阿爾伯馬爾和](../Page/阿爾伯馬爾縣_\(維吉尼亞州\).md "wikilink")[密爾頓附近的土地](../Page/密爾頓.md "wikilink")，僅能償還部分債務。在有的[債權人開始追索的情況下](../Page/債權人.md "wikilink")，他企圖向傑弗遜請求援助。卻不料這才知道傑弗遜比他更貧困，於是門羅聯合其他人，聯名向[弗吉尼亚議會要求接濟傑弗遜](../Page/弗吉尼亚.md "wikilink")。門羅對於美國政府的補償要求被一拖再拖，門羅只好作放棄的打算。

1830年9月23日，門羅夫人因[中風逝世](../Page/中風.md "wikilink")，給門羅以極大打擊。安葬完夫人之後，門羅搬到二女兒在[紐約的家中](../Page/紐約.md "wikilink")。當時的總統[約翰·昆西·亞當斯任命他的二女婿為紐約](../Page/約翰·昆西·亞當斯.md "wikilink")[郵政局局長](../Page/郵政局.md "wikilink")，這份薪俸保證了門羅一家的生活。

1831年7月4日，門羅[總統在女兒家因](../Page/美國總統.md "wikilink")[心臟衰竭離開了人世](../Page/心臟衰竭.md "wikilink")，享壽七十三歲，成為死於[國慶日的第三位前總統](../Page/國慶日.md "wikilink")。門羅死後爲子女留下了大量遺產，除一些散佈在[弗吉尼亚](../Page/弗吉尼亚.md "wikilink")，[肯塔基及](../Page/肯塔基.md "wikilink")[紐約州等處的土地之外](../Page/紐約州.md "wikilink")，還有門羅生前居住的橡樹山莊與在該莊園工作的五十名奴隸以及莊園宅邸中的豪華傢俱。\[1\]

## 參考資料

{{-}}

{{-}}

[Category:美国总统](../Category/美国总统.md "wikilink")
[Category:美國總統候選人](../Category/美國總統候選人.md "wikilink")
[Category:1820年美國總統選舉](../Category/1820年美國總統選舉.md "wikilink")
[Category:1816年美國總統選舉](../Category/1816年美國總統選舉.md "wikilink")
[Category:弗吉尼亞州州長](../Category/弗吉尼亞州州長.md "wikilink")
[Category:美國駐法國大使](../Category/美國駐法國大使.md "wikilink")
[Category:美國駐英國大使](../Category/美國駐英國大使.md "wikilink")
[Category:1812年戰爭人物](../Category/1812年戰爭人物.md "wikilink")
[Category:美國獨立戰爭人物](../Category/美國獨立戰爭人物.md "wikilink")
[Category:大陸會議代表](../Category/大陸會議代表.md "wikilink")
[Category:威廉与玛丽学院校友](../Category/威廉与玛丽学院校友.md "wikilink")
[Category:美國聖公宗教徒](../Category/美國聖公宗教徒.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:維吉尼亞州聯邦參議員](../Category/維吉尼亞州聯邦參議員.md "wikilink")
[Category:死於心臟衰竭的人](../Category/死於心臟衰竭的人.md "wikilink")
[Category:19世纪美国政治家](../Category/19世纪美国政治家.md "wikilink")
[Category:美國民主共和黨聯邦參議員](../Category/美國民主共和黨聯邦參議員.md "wikilink")
[Category:美國1812年戰爭人物](../Category/美國1812年戰爭人物.md "wikilink")
[Category:美国民主党共和党州长](../Category/美国民主党共和党州长.md "wikilink")
[Category:18世纪美国政治家](../Category/18世纪美国政治家.md "wikilink")

1.