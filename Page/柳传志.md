**柳传志**（），生於[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[镇江市](../Page/镇江市.md "wikilink")，中国著名[企业家](../Page/企业家.md "wikilink")，[联想集团创始人](../Page/联想集团.md "wikilink")，中国共产党党员。

柳传志创办联想集团之后，曾任[联想控股有限公司总裁](../Page/联想控股有限公司.md "wikilink")、联想集团有限公司董事局主席，2011年11月2日卸任，现任联想集团有限公司董事局名誉主席，联想集团高级顾问。

## 生平

其父[柳谷書](../Page/柳谷書.md "wikilink")，是中國共產黨黨員。1984年3月創辦中國專利代理(香港)有限公司，隨後又創辦柳沈專利事務所，是中國最早從事於[專利事務的律師之一](../Page/專利.md "wikilink")。

1966年，柳传志毕业于西安军事电讯工程学院（现[西安电子科技大学前身](../Page/西安电子科技大学.md "wikilink")），后曾在国防科工委十院四所和中科院计算所从事科学研究工作。

1984年，作为公司创始人之一创办北京计算机新技术发展公司(联想集团前身)，后曾任总经理、总裁。

1988年，创建香港联想并出任主席。

1997年，北京联想与香港联想合并，柳传志出任联想集团主席。

2000年1月，被《[财富](../Page/财富_\(杂志\).md "wikilink")》杂志评选为“亚洲最佳商业人士”。

2000年6月，被《[商业周刊](../Page/商業周刊_\(臺灣\).md "wikilink")》评选为“亚洲之星”

2013年10月，全国工商联60华诞之际，《中国工商》杂志、华商韬略编辑委员会、中华工商联合出版社联合发起《民营力量璀璨中国梦想——100位对民族产业贡献卓著的民营功勋企业家》荣誉表彰报道活动，彰显民营经济及民营企业家的民族成就与国家贡献，柳传志获“对民族产业贡献卓著的民营功勋企业家”荣誉肯定。

2018年12月18日，在庆祝改革开放40周年大会上，柳传志获得改革先锋称号，被誉为“科技产业化的先行者”。\[1\]

## 参考资料

<references />

## 参见

  - [倪光南](../Page/倪光南.md "wikilink")
  - [李勤](../Page/李勤.md "wikilink")
  - [郭为](../Page/郭为.md "wikilink")
  - [杨元庆](../Page/杨元庆.md "wikilink")
  - [柳青](../Page/柳青_\(企业家\).md "wikilink")（柳传志之女）
  - [柳甄](../Page/柳甄.md "wikilink")（柳传志之侄女）

## 外部链接

  -
[category:中国企业家](../Page/category:中国企业家.md "wikilink")

[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:联想人物](../Category/联想人物.md "wikilink")
[Category:中華人民共和國企業家](../Category/中華人民共和國企業家.md "wikilink")
[Category:西安電子科技大學校友](../Category/西安電子科技大學校友.md "wikilink")
[Category:香港科技大學榮譽博士](../Category/香港科技大學榮譽博士.md "wikilink")
[Category:军人出身的商人](../Category/军人出身的商人.md "wikilink")
[Chuanzhi](../Category/柳姓.md "wikilink")
[Category:改革开放40年百名杰出民营企业家](../Category/改革开放40年百名杰出民营企业家.md "wikilink")
[Category:中国共产党党员](../Category/中国共产党党员.md "wikilink")
[Category:改革先锋称号获得者](../Category/改革先锋称号获得者.md "wikilink")

1.