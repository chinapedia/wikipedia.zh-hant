**重庆道**是[中国](../Page/中国.md "wikilink")[天津市](../Page/天津市.md "wikilink")[和平区](../Page/和平区_\(天津市\).md "wikilink")[五大道地区的一条街道](../Page/五大道.md "wikilink")，修筑于1922年，原属[天津英租界](../Page/天津英租界.md "wikilink")。重庆道东北到[马场道](../Page/马场道.md "wikilink")，西南到昆明路，全长1919米，宽10米，南北两侧分别与[常德道和](../Page/常德道.md "wikilink")[成都道平行](../Page/成都道.md "wikilink")。目前，重庆道地区是[天津市历史风貌建筑的聚集地之一](../Page/天津市历史风貌建筑.md "wikilink")，大量的名人故居坐落于此。\[1\]

## 历史

重庆道所在地域原是[天津老城东南的一片沼泽洼地](../Page/天津老城.md "wikilink")，散落着一些窝棚式的简陋民居。1903年，天津英租界工部局在获得重庆道所在区域。1922年至1932年期间，天津英租界工部局结合海河清淤工程，对重庆道所在区域进行填筑使之成为适合天津城市建设的用地。1929年至1932年，天津英租界工部局修筑此路并以[河北路](../Page/河北路_\(天津\).md "wikilink")（原威灵顿路）为界，东段原为剑桥道（Cambridge
Road），又称“39号路”；西段原为爱丁堡道（Edinburgh
Road），又称“37号路”。此后，两侧迅速形成大片高级住宅区，绿化良好，建筑风格属于各种不同的欧洲式样。1943年，[日本占领天津后](../Page/日本.md "wikilink")，剑桥道改称“兴亚二区39号路”，爱丁堡道改称“兴亚二区37号路”。1946年，[天津市政府光复后以](../Page/天津市政府_\(中华民国\).md "wikilink")[重庆市将其统一命名为重庆道至今](../Page/重庆市.md "wikilink")。历史上，[载振](../Page/载振.md "wikilink")、[严修](../Page/严修.md "wikilink")、[张作相](../Page/张作相.md "wikilink")、[李爱锐](../Page/李爱锐.md "wikilink")、[龚心湛](../Page/龚心湛.md "wikilink")、[金邦平](../Page/金邦平.md "wikilink")、孙桐萱、王雨生、张泽湘等名人均居住过重庆道。\[2\]

## 现况与发展

重庆道目前东北到[马场道](../Page/马场道.md "wikilink")，西南到昆明路，全长1919米，车行道平均度10米，人行道宽1.5米至13米，由于道路比较狭窄，重庆道一直作为单行道使用。南北两侧分别与[常德道和](../Page/常德道.md "wikilink")[成都道平行](../Page/成都道.md "wikilink")。由于重庆道街区被纳入[天津历史文化街区的保护规划](../Page/天津历史文化街区列表.md "wikilink")，因此未来将保持现有风貌不会拓宽。\[3\]

## 沿街著名建筑

[张作相旧居.jpg](https://zh.wikipedia.org/wiki/File:张作相旧居.jpg "fig:张作相旧居.jpg")|200px\]\]
重庆道现存比较著名的建筑有：

  - [天津市文物保护单位](../Page/天津市文物保护单位.md "wikilink")：[天津庆王府](../Page/天津庆王府.md "wikilink")，重庆道55号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[张作相旧居](../Page/张作相旧居.md "wikilink")，重庆道4号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[李爱锐故居](../Page/李爱锐故居.md "wikilink")，重庆道38号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[金梁故居](../Page/金梁故居.md "wikilink")，重庆道52号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[龚心湛旧宅](../Page/龚心湛旧宅.md "wikilink")，重庆道64号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[民园大楼](../Page/民园大楼.md "wikilink")，重庆道66号至68号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[孙桐萱旧宅](../Page/孙桐萱旧宅.md "wikilink")，重庆道68号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[王雨生旧居](../Page/王雨生旧居.md "wikilink")，重庆道89号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[张泽湘旧居](../Page/张泽湘旧居.md "wikilink")，重庆道106号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[金邦平旧居](../Page/金邦平旧居.md "wikilink")，重庆道114号
  - [天津市历史风貌建筑](../Page/天津市历史风貌建筑.md "wikilink")：[严修旧居](../Page/严修旧居.md "wikilink")，重庆道144号至146号\[4\]

## 交汇道路

[chongqingroad.jpg](https://zh.wikipedia.org/wiki/File:chongqingroad.jpg "fig:chongqingroad.jpg")
目前，与重庆道相交汇的主要道路有：

  - [马场道](../Page/马场道.md "wikilink") 原马厂道（Race Course Road）
  - 湖北路 原怡丰道（Avon Road ）
  - 香港路 原泰安道（Tyne Road ）
  - 澳门路 原西德尼道（Sydney Road ）
  - [新华路](../Page/新华路_\(天津\).md "wikilink") 原牛津道（Oxford Road ）
  - 南海路 原摩西道（Mersey Road ）
  - [河北路](../Page/河北路_\(天津\).md "wikilink") 原威灵顿道（Wellington Road ）
  - 湖南路 原西芬道（Severn Road ）
  - [长沙路](../Page/长沙路_\(天津\).md "wikilink") 原加的夫道（Cardiff Road ）
  - 衡阳路 原马耳他道（Malta Road ）
  - 桂林路 原格拉斯哥道（Glasgow Road ）
  - 云南路 原登百敦道（Dumbarton Road ）
  - 昆明路 原康伯兰道（Cumberland Road ）

## 参考文献

[Category:天津歷史](../Category/天津歷史.md "wikilink")
[Category:天津道路](../Category/天津道路.md "wikilink")
[Category:天津英租界](../Category/天津英租界.md "wikilink")
[Category:天津市和平区道路](../Category/天津市和平区道路.md "wikilink")

1.  [重庆道上的里弄住宅](http://news.163.com/07/0110/10/34FG9MJM000120GU.html)
2.  [洋中之洋重庆道](http://www.tianjinwe.com/rollnews/dzb/cskb/201005/t20100510_817259.html)
3.  [和平区志](http://www.tjhp.gov.cn/servlet/ContentServer?cid=1105523261039&pagename=tjhp%2FPage%2Fsub-t46&lv=2&lv2=1100820937330&lv1=1087380370033&c=Page)
4.  [天津市历史风貌建筑网](http://www.fmjz.cn/Pages/default.aspx)