**HJ文庫**是Hobby
Japan在2006年7月1日創刊的[輕小說文庫](../Page/輕小說.md "wikilink")。台灣由[東立出版社代理](../Page/東立出版社.md "wikilink")。

## 簡介

  - 有舉辦發掘新人的Novel Japan大獎（現為[HJ文庫大賞](../Page/HJ文庫大賞.md "wikilink")）。
  - 創刊初級封面設計和[角川Sneaker文庫很難區別而受到批評](../Page/角川Sneaker文庫.md "wikilink")，不到半年後的2007年1月新出版作品設計便大幅變更。
  - 儘管是輕小說後進文庫系列，代理到國外卻進行的相當迅速。在台灣和[東立出版社簽訂獨佔條約](../Page/東立出版社.md "wikilink")，幾乎所有作品都翻譯成繁體中文。
  - 2007年10月開始，成立以出版[TRPG的Reply](../Page/TRPG.md "wikilink")（遊戲歷程）為主的姊妹系列「HJ文庫G」。

## 作品列表

  -
    由於未出版書籍眾多，完整列表請見日語版條目。

### 東立出版社

  - [明天好天氣](../Page/明天好天氣.md "wikilink")（[雨森麻社](../Page/雨森麻社.md "wikilink")／[下北澤鈴成](../Page/下北澤鈴成.md "wikilink")）

  - [亞雷克森傳奇](../Page/亞雷克森傳奇.md "wikilink")（[五代ゆう](../Page/五代ゆう.md "wikilink")／[いのまたむつみ](../Page/いのまたむつみ.md "wikilink")）

  - [戰姬](../Page/戰姬.md "wikilink")（[夏綠](../Page/夏綠.md "wikilink")／[シオミヤイルカ](../Page/シオミヤイルカ.md "wikilink")）

  - [退魔少女隊](../Page/退魔少女隊.md "wikilink")（[すずきあきら](../Page/すずきあきら.md "wikilink")／[明貴美加](../Page/明貴美加.md "wikilink")）

  - [串刺少女](../Page/串刺少女.md "wikilink")（[木村航](../Page/木村航.md "wikilink")／[中村哲也](../Page/中村哲也.md "wikilink")）

  - （[清水文化](../Page/清水文化.md "wikilink")／[牛木義隆](../Page/牛木義隆.md "wikilink")）

  - [壯麗的黎明](../Page/壯麗的黎明.md "wikilink")（[庄司卓](../Page/庄司卓.md "wikilink")／[四季童子](../Page/四季童子.md "wikilink")）

  - [郭德堡變奏曲](../Page/郭德堡變奏曲_\(小說\).md "wikilink")（五代ゆう／[鈴木理華](../Page/鈴木理華.md "wikilink")）

  - [高中女神](../Page/高中女神.md "wikilink")（[ひかわ玲子](../Page/ひかわ玲子.md "wikilink")／[近衛乙嗣](../Page/近衛乙嗣.md "wikilink")）

  - [佐藤家的選擇](../Page/佐藤家的選擇.md "wikilink")（[貝花大介](../Page/貝花大介.md "wikilink")／[今泉昭彦](../Page/今泉昭彦.md "wikilink")）

  - [人體戰艦](../Page/人體戰艦.md "wikilink")（[大迫純一](../Page/大迫純一.md "wikilink")／[hippo](../Page/hippo.md "wikilink")）

  - [潛水艇](../Page/潛水艇_\(小說\).md "wikilink")（[水城正太郎](../Page/水城正太郎.md "wikilink")／[あぼしまこ](../Page/あぼしまこ.md "wikilink")）

  - [秋吉同學幫幫忙](../Page/秋吉同學幫幫忙.md "wikilink")（[不破悠](../Page/不破悠.md "wikilink")／[超肉](../Page/超肉.md "wikilink")）

  - [礦物質超女](../Page/礦物質超女.md "wikilink")（[冬樹忍](../Page/冬樹忍.md "wikilink")／[魚](../Page/魚_\(插畫家\).md "wikilink")）

  - [超鋼女雪拉](../Page/超鋼女雪拉.md "wikilink")（[寺田とものり](../Page/寺田とものり.md "wikilink")／[Ein](../Page/Ein.md "wikilink")）

  - [月之少女](../Page/月之少女.md "wikilink")（[渡辺まさき](../Page/渡辺まさき.md "wikilink")／[山田秀樹](../Page/山田秀樹.md "wikilink")）

  - [緋華Sparkling](../Page/緋華Sparkling.md "wikilink")（[藤原征矢](../Page/藤原征矢.md "wikilink")／[松本規之](../Page/松本規之.md "wikilink")）

  - [叛紅獵殺](../Page/叛紅獵殺.md "wikilink")（[在原竹広](../Page/在原竹広.md "wikilink")／[弘司](../Page/弘司.md "wikilink")）

  - [半熟公主](../Page/半熟公主.md "wikilink")（[榊一郎](../Page/榊一郎.md "wikilink")／[深山和香](../Page/深山和香.md "wikilink")）

  - [屍人狩獵者](../Page/屍人狩獵者.md "wikilink")（[葛西伸哉](../Page/葛西伸哉.md "wikilink")／[金田榮路](../Page/金田榮路.md "wikilink")）

  - [模造王女騷動記](../Page/模造王女騷動記.md "wikilink")（榊一郎／[藤田香](../Page/藤田香.md "wikilink")）

  - [女王之刃系列](../Page/女王之刃.md "wikilink")（[沖田榮次](../Page/沖田榮次.md "wikilink")／[えぃわ](../Page/えぃわ.md "wikilink")）

  - [惡魔少女系列](../Page/惡魔少女.md "wikilink")（[若月光](../Page/若月光.md "wikilink")／[高階@聖人](../Page/高階@聖人.md "wikilink")）

  - [CUTTING
    傷痕](../Page/CUTTING_傷痕.md "wikilink")（[翅田大介](../Page/翅田大介.md "wikilink")／[も](../Page/も.md "wikilink")）

  - [重回那天](../Page/重回那天.md "wikilink")（[健速](../Page/健速.md "wikilink")／[雙](../Page/雙.md "wikilink")）

  - [TOY JOY POP
    狂熱青春](../Page/TOY_JOY_POP_狂熱青春.md "wikilink")（[浅井ラボ](../Page/浅井ラボ.md "wikilink")／[柴倉乃杏](../Page/柴倉乃杏.md "wikilink")）

  - [特務魔法使](../Page/特務魔法使.md "wikilink")（[すえばしけん](../Page/すえばしけん.md "wikilink")／[かぼちゃ](../Page/かぼちゃ.md "wikilink")）

  - [眼鏡少女HOLIC](../Page/眼鏡少女HOLIC.md "wikilink")（[上栖綴人](../Page/上栖綴人.md "wikilink")／[トモセシュンサク](../Page/トモセシュンサク.md "wikilink")）

  - [無賴勇者的鬼畜美學](../Page/無賴勇者的鬼畜美學.md "wikilink")（上栖綴人／[卵の黄身](../Page/卵の黄身.md "wikilink")）

  - [最後大魔王](../Page/最後大魔王.md "wikilink")（[水城正太郎](../Page/水城正太郎.md "wikilink")／[伊藤宗一](../Page/伊藤宗一.md "wikilink")）

  - [歡迎光臨日本上空](../Page/歡迎光臨日本上空.md "wikilink")（[佐々原史緒](../Page/佐々原史緒.md "wikilink")／[タスクオーナ](../Page/タスクオーナ.md "wikilink")）

  - [神祕博物館](../Page/神祕博物館.md "wikilink")（[藤春都](../Page/藤春都.md "wikilink")／[森井しづき](../Page/森井しづき.md "wikilink")）

  - [阿妮絲與愛擺臭臉的魔法使](../Page/阿妮絲與愛擺臭臉的魔法使.md "wikilink")（[花房牧生](../Page/花房牧生.md "wikilink")／[植田亮](../Page/植田亮.md "wikilink")）

  - [戀上不死之男的少女](../Page/戀上不死之男的少女.md "wikilink")（[空埜一樹](../Page/空埜一樹.md "wikilink")／[ぷよ](../Page/ぷよ.md "wikilink")）

  - [BLACK SHEEP
    聖夜迷途的黑羊](../Page/BLACK_SHEEP_聖夜迷途的黑羊.md "wikilink")（[富永浩史](../Page/富永浩史.md "wikilink")／[珊琶挿](../Page/珊琶挿.md "wikilink")）

  - [三坪房間的侵略者\!?](../Page/三坪房間的侵略者!?.md "wikilink")（健速／[ポコ](../Page/ポコ.md "wikilink")）

  - [放學後的征服世界](../Page/放學後的征服世界.md "wikilink")（[若月光](../Page/若月光.md "wikilink")／[步鳥](../Page/步鳥.md "wikilink")）

  - [新感覺奇幻冒險
    傲蕉魔法使](../Page/新感覺奇幻冒險_傲蕉魔法使.md "wikilink")（[谷口シュンスケ](../Page/谷口シュンスケ.md "wikilink")／[京作](../Page/京作.md "wikilink")）

  - [前略。我與貓和天使同居](../Page/前略。我與貓和天使同居.md "wikilink")（[緋月薙](../Page/緋月薙.md "wikilink")／[明星かがよ](../Page/明星かがよ.md "wikilink")）

  - [突然成為騎士](../Page/突然成為騎士.md "wikilink")（[系緒思惟](../Page/系緒思惟.md "wikilink")／[三色網戶。](../Page/三色網戶。.md "wikilink")）

  - [鋼音之劍](../Page/鋼音之劍.md "wikilink")（[無嶋樹了](../Page/無嶋樹了.md "wikilink")／[rei](../Page/rei.md "wikilink")）

  - [STITCH裁縫少女\!](../Page/STITCH裁縫少女!.md "wikilink")（[相内円](../Page/相内円.md "wikilink")／[ほっぺげ](../Page/ほっぺげ.md "wikilink")）

  - [我與她的絕對領域](../Page/我與她的絕對領域.md "wikilink")（[鷹山誠一](../Page/鷹山誠一.md "wikilink")／[伍長](../Page/伍长_\(插畫家\).md "wikilink")）

  - [我的妹妹會讀漢字](../Page/我的妹妹會讀漢字.md "wikilink")（[かじいたかし](../Page/かじいたかし.md "wikilink")／[皆村春樹](../Page/皆村春樹.md "wikilink")）

  - [魔王學校裡只有我是勇者\!?](../Page/魔王學校裡只有我是勇者!?.md "wikilink")（夏綠／[朱シオ](../Page/朱シオ.md "wikilink")）

  - [魔龍妹與禿頭兄](../Page/魔龍妹與禿頭兄.md "wikilink")（谷口シュンスケ／[あき](../Page/あき.md "wikilink")）

  - [旅行少女與灼熱之國](../Page/旅行少女與灼熱之國.md "wikilink")（[藤原征矢](../Page/藤原征矢.md "wikilink")／[田上俊介](../Page/田上俊介.md "wikilink")）

  - [我的宅女神](../Page/我的宅女神.md "wikilink")（すえばしけん／[みえはる](../Page/みえはる.md "wikilink")）

  - [我拯救太多女主角引發了世界末日\!?](../Page/我拯救太多女主角引發了世界末日!?.md "wikilink")（[なめこ印](../Page/なめこ印.md "wikilink")／[和狸ナオ](../Page/和狸ナオ.md "wikilink")）

  - [我果然還是渾然不覺](../Page/我果然還是渾然不覺.md "wikilink")（[望公太](../Page/望公太.md "wikilink")／[タカツキイチ](../Page/タカツキイチ.md "wikilink")）

  - [白銀龍王的搖籃](../Page/白銀龍王的搖籃.md "wikilink")（[ツガワトモタカ](../Page/ツガワトモタカ.md "wikilink")／[ぽんじりつ](../Page/ぽんじりつ.md "wikilink")）

  - [我家會長是個壞壞虎斑貓。](../Page/我家會長是個壞壞虎斑貓。.md "wikilink")（[空埜一樹](../Page/空埜一樹.md "wikilink")／[ろんど](../Page/ろんど.md "wikilink")）

  - [我的現實與網遊被戀愛喜劇侵蝕了](../Page/我的現實與網遊被戀愛喜劇侵蝕了.md "wikilink")（[藤谷ある](../Page/藤谷ある.md "wikilink")／[三嶋くろね](../Page/三嶋くろね.md "wikilink")）

  - [魔王的我與不死公主的戒指](../Page/魔王的我與不死公主的戒指.md "wikilink")（[柑橘ゆすら](../Page/柑橘ゆすら.md "wikilink")／[しゅがすく](../Page/しゅがすく.md "wikilink")）

  - [SAMURAI
    BLOOD武士之血](../Page/SAMURAI_BLOOD武士之血.md "wikilink")（[松時ノ介](../Page/松時ノ介.md "wikilink")／[津雪](../Page/津雪.md "wikilink")）

  - [我對惡魔非揉不可的理由](../Page/我對惡魔非揉不可的理由.md "wikilink")（[鏡裕之](../Page/鏡裕之.md "wikilink")/[黒川いづみ](../Page/黒川いづみ.md "wikilink")）

  - [月花歌姬與魔技之王](../Page/月花歌姬與魔技之王.md "wikilink")（翅田大介/[大場陽炎](../Page/大場陽炎.md "wikilink")）

  - [神話戰域](../Page/神話戰域.md "wikilink")（無嶋樹了／[すみ兵](../Page/すみ兵.md "wikilink")）

  - [火界王劍的神滅者](../Page/火界王劍的神滅者.md "wikilink")（ツガワトモタカ／[Riv](../Page/Riv.md "wikilink")）

  - [百鍊霸主與聖約女武神](../Page/百鍊霸主與聖約女武神.md "wikilink")（鷹山誠一／[ゆきさん](../Page/ゆきさん.md "wikilink")）

  - [時之惡魔與三篇物語](../Page/時之惡魔與三篇物語.md "wikilink")（[ころみごや](../Page/ころみごや.md "wikilink")／かぼちゃ）

  - [我的姊姊有中二病](../Page/我的姊姊有中二病.md "wikilink")（[藤孝剛志](../Page/藤孝剛志.md "wikilink")／[An2A](../Page/An2A.md "wikilink")）

  - [格蘭斯坦迪亞皇國物語](../Page/格蘭斯坦迪亞皇國物語.md "wikilink")（[内堀優一](../Page/内堀優一.md "wikilink")／[鵜飼沙樹](../Page/鵜飼沙樹.md "wikilink")）

  - [勇者(我)和魔王(她)的客廳之戰](../Page/勇者\(我\)和魔王\(她\)的客廳之戰.md "wikilink")（緋月薙／三嶋くろね）

  - [漆黑英雄的一擊無雙](../Page/漆黑英雄的一擊無雙.md "wikilink")（望公太／[夕薙](../Page/夕薙.md "wikilink")）

### 青文出版社

  - [水乳交揉](../Page/水乳交揉.md "wikilink")（[鏡裕之](../Page/鏡裕之.md "wikilink")／[くりから](../Page/くりから.md "wikilink")）
  - [GIVE
    UP\!](../Page/GIVE_UP!.md "wikilink")（[上栖綴人](../Page/上栖綴人.md "wikilink")／[會田孝信](../Page/會田孝信.md "wikilink")）

### 四季國際

  - [現充王！](../Page/現充王！.md "wikilink")（[若櫻拓海](../Page/若櫻拓海.md "wikilink")／[モノリノ](../Page/モノリノ.md "wikilink")）
  - [墮落邪惡組織，擁抱美少女大勝利\!\!](../Page/墮落邪惡組織，擁抱美少女大勝利!!.md "wikilink")（[岡沢六十四](../Page/岡沢六十四.md "wikilink")／[むつみまさと](../Page/むつみまさと.md "wikilink")）

### 吉美文化

  - 我与她的绝对领域（鹰山诚一／伍长）
  - [三坪房間的侵略者\!?](../Page/三坪房間的侵略者!?.md "wikilink")（[健速](../Page/健速.md "wikilink")／Poco）
  - [我的妹妹会认汉字](../Page/我的妹妹会认汉字.md "wikilink")（Takashi Kajii／皆村春树）

### 未代理

  - [舞-HiME★DESTINY
    龍の巫女](../Page/舞-HiME★DESTINY_龍の巫女.md "wikilink")（[伊吹秀明](../Page/伊吹秀明.md "wikilink")
    原作：[矢立肇](../Page/矢立肇.md "wikilink")・[舞-HiMEプロジェクト](../Page/舞-HiMEプロジェクト.md "wikilink")／[目黑三吉](../Page/目黑三吉.md "wikilink")）

## 外部連結

  - [HJ文庫](http://www.hobbyjapan.co.jp/hjbunko/)

[\*](../Category/HJ文庫.md "wikilink")