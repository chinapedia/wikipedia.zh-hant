**路易斯·西摩爾·巴澤特·李奇**（[英语](../Page/英语.md "wikilink")：**Louis Seymour Bazett
Leakey**，）是一位[考古学家和](../Page/考古学家.md "wikilink")[人类学家](../Page/人类学家.md "wikilink")，他对其妻子[瑪麗·李奇在](../Page/瑪麗·李奇.md "wikilink")[坦桑尼亚发现的](../Page/坦桑尼亚.md "wikilink")175万年前的[东非人头盖骨的叙述和分析](../Page/东非.md "wikilink")，影响了[人类演化理论](../Page/人类演化.md "wikilink")。

## 生平

路易斯出生在[肯尼亚](../Page/肯尼亚.md "wikilink")，他的父母是[英国的](../Page/英国.md "wikilink")[基督教](../Page/基督教.md "wikilink")[传教士](../Page/传教士.md "wikilink")，从小与[基库尤人生活在一起](../Page/基库尤族.md "wikilink")，后被[剑桥大学录取](../Page/剑桥大学.md "wikilink")，学习[人类学](../Page/人类学.md "wikilink")。

## 參見

  - [加州閃亮貓](../Page/加州閃亮貓.md "wikilink")（路易斯·李奇曾執行該種家貓的混種計畫）

## 参考文献

  -
[Category:英国考古学家](../Category/英国考古学家.md "wikilink")
[Category:英国人类学家](../Category/英国人类学家.md "wikilink")
[Category:肯尼亚人](../Category/肯尼亚人.md "wikilink")
[Category:剑桥大学圣约翰学院校友](../Category/剑桥大学圣约翰学院校友.md "wikilink")
[Category:英國基督徒](../Category/英國基督徒.md "wikilink")
[Category:英国古人类学家](../Category/英国古人类学家.md "wikilink")