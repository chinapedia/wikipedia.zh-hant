**巴黎酒店**（**Paris Las
Vegas**）是位於[美國](../Page/美國.md "wikilink")[內華達州](../Page/內華達州.md "wikilink")[拉斯維加斯](../Page/拉斯維加斯.md "wikilink")[賭城大道上的一間賭場酒店](../Page/賭城大道.md "wikilink")，由[哈拉斯娛樂所持有及經營](../Page/哈拉斯娛樂.md "wikilink")。顧名思義，酒店的主題為[法國的](../Page/法國.md "wikilink")[首都](../Page/首都.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")，並建有模仿原[比例一半](../Page/比例.md "wikilink")(165米高)的[艾菲爾鐵塔](../Page/艾菲爾鐵塔.md "wikilink")、[凱旋門等建築](../Page/凱旋門.md "wikilink")，有如一個縮小的巴黎。建築群的前部則代表[巴黎歌劇院](../Page/巴黎歌劇院.md "wikilink")。

[拉斯維加斯單軌鐵路的站點位於巴黎酒店側的](../Page/拉斯維加斯單軌鐵路.md "wikilink")[貝利酒店內](../Page/貝利酒店.md "wikilink")。

## 歷史

[Parishotel2.jpg](https://zh.wikipedia.org/wiki/File:Parishotel2.jpg "fig:Parishotel2.jpg")
酒店於1999年9月1日開幕，原先是由貝利酒店的持有集團[貝利娛樂所倡建的](../Page/貝利娛樂.md "wikilink")。

及後，貝利娛樂於1996年被[希爾頓酒店集團所收購](../Page/希爾頓酒店.md "wikilink")，興建初期原打算命名為《芭黎絲·希爾頓酒店》，名字來源於集團主席希爾頓先生的女兒[芭黎絲·希爾頓](../Page/芭黎絲·希爾頓.md "wikilink")（Paris
Hilton）。但後期經過一輪合併收購後，酒店落到[凱撒娛樂的手中](../Page/凱撒娛樂.md "wikilink")，遂把酒店名稱改為巴黎酒店（Paris
Hotel & Casino Las Vegas）。

在開幕初期，酒店在美國各大[傳媒刊登廣告](../Page/傳媒.md "wikilink")，標榜其建築有如將[巴黎複製](../Page/巴黎.md "wikilink")，有如一個真正的[城市一樣](../Page/城市.md "wikilink")。

而建築[仿製的艾菲爾鐵塔時](../Page/仿製.md "wikilink")，原先是打算以原本[尺寸興建的](../Page/尺寸.md "wikilink")。但由於[機場的位置太過接近](../Page/機場.md "wikilink")，故這項打算亦無奈告吹。

2005年，[哈拉斯娛樂收購了](../Page/哈拉斯娛樂.md "wikilink")[凱撒娛樂](../Page/凱撒娛樂.md "wikilink")，巴黎酒店成了哈拉斯娛樂旗下的一所賭場酒店。

## 外部連結

  - [巴黎酒店](http://www.caesars.com/Paris/LasVegas/)
  - [哈拉斯娛樂](http://www.harrahs.com/)

[Category:拉斯維加斯賭場](../Category/拉斯維加斯賭場.md "wikilink")
[Category:1999年完工建築物](../Category/1999年完工建築物.md "wikilink")
[P](../Category/賭城大道.md "wikilink")
[P](../Category/拉斯維加斯度假村.md "wikilink")
[P](../Category/天堂市建築物.md "wikilink")