**Blender**是一套专业的，[自由及开放源代码的](../Page/自由及开放源代码软件.md "wikilink")[三维计算机图形軟體](../Page/三维计算机图形.md "wikilink")。

## 歷史

最初，這個程式是被荷蘭的一個动画工作室[NeoGeo設計為内部使用的程式](../Page/NeoGeo.md "wikilink")。但後來NeoGeo被收購，其主要程式設計者[彤·羅森達爾於](../Page/彤·羅森達爾.md "wikilink")1998年6月成立[Not
a Number
Technologies](../Page/Not_a_Number_Technologies.md "wikilink")（NaN）公司，將其進一步發展，並以[共享軟體的形式對外發佈這個程式](../Page/共享軟體.md "wikilink")，直到NaN公司於2002年宣告破產。

在經過債權人同意後，Blender繳付一次性報酬十萬歐元後變為自由軟體，並以[GNU通用公共许可证發佈](../Page/GNU通用公共许可证.md "wikilink")。在2002年7月18日，彤·羅森達爾開始為Blender籌集資金；同年9月7日，Blender宣佈籌集足夠資金，並將其源碼對外公佈。因此，Blender現在是[自由軟體](../Page/自由軟體.md "wikilink")，並由[Blender基金會維護與更新](../Page/Blender_Foundation.md "wikilink")。\[1\]

## 功能

Blender可以運行於不同的平台，而且安裝後佔很少空間。雖然它經常不支援說明文檔或範例發佈，但其擁有極豐富的功能，而且絕大部份是高端模組塑造軟體。其特性有：

  - 支持不同的幾何圖元，包括多邊形網紋，快速表層塑模，曲線及向量字元。
  - 多用途的內部渲染及整合[YafRay這個開源的射線追蹤套件](../Page/YafRay.md "wikilink")。
  - 動畫工具，包括了[反向動作組件](../Page/反向動作.md "wikilink")，可設定骨幹，結構變形，關鍵影格，時間線，非線性動畫，系統規定參數，頂點量重及柔化動量組件，包括網孔碰撞偵察和一個具有偵察碰撞的粒子系統。
  - 使用[Python語言來創作及製作遊戲及工作自動化腳本](../Page/Python.md "wikilink")。\[2\]
  - 基本的非線性影像編輯及製作功能。
  - [Game
    Blender](../Page/Game_Blender.md "wikilink")，一個子計劃，用以製作實時的[電腦遊戲](../Page/電腦遊戲.md "wikilink")。

[Engine_movingparts.jpg](https://zh.wikipedia.org/wiki/File:Engine_movingparts.jpg "fig:Engine_movingparts.jpg")渲染完成的[光线跟踪和环境光吸收特效的图象](../Page/光线跟踪.md "wikilink")\]\]
[Physics-Fluid-Simulation-Blender.gif](https://zh.wikipedia.org/wiki/File:Physics-Fluid-Simulation-Blender.gif "fig:Physics-Fluid-Simulation-Blender.gif")

### 先進功能

  - 在Blender中，物體與數據是分離的，這使其可以快速塑模。
  - 可以將多個[場景合併至單一檔案](../Page/場景.md "wikilink")（稱為".blend"的檔案）。
  - 所有blender所生成的".blend"檔案均有完善的向前，向後版本的兼容性，同時亦具有完整的跨平台支援。
  - 自動定時儲存".blend"免得因當機而出現資料遺失。
  - 所有場景，[物件](../Page/物件.md "wikilink")，[材料](../Page/材料.md "wikilink")，[材質](../Page/材質.md "wikilink")，[聲音](../Page/聲音.md "wikilink")，[圖片](../Page/圖片.md "wikilink")，[後期製作](../Page/後期製作.md "wikilink")[特效均可整合至最後生成的](../Page/特效.md "wikilink")".blend"動畫檔裡。
  - 可透過".blend"檔案來自訂使用者介面。

### 用戶介面

Blender提供了非常多的工具，因此其使用者介面對初學者來說是複雜的，但通常要花點時間看說明文件便能很快上手。一般來說，可大致分為以下各種介面模式和技巧。

  - 編輯模式。包含了兩種基本模式，分別為*物件模式*及*編輯模式*。其他模式還有*雕刻模式*、*權重繪製*、*紋理繪製*、*頂點繪製*、*姿勢模式*...等。
  - 大量運用熱鍵，並和選單相配合，增加使用效率。
  - 可自行調節工作空間。

## 發展

2.6x版本改良特點：

  - 增加Cycles渲染器，Cycles渲染器是使用光線追蹤演算法，可以得到接近相片級的品質，也可渲染動畫。Cycles渲染器可使用GPU渲染，使渲染速度比CPU快好幾倍。
  - 增加卡通渲染（2.67版本），使3D模型可以得到像2D繪圖一樣的筆觸，多半使用者來建構2D動畫。
  - 幾何物體的管理介面
  - 全新多边形建模工具
  - 使用者自訂鍵盤熱鍵
  - 新的幾何物體在2.53b的版本中被加入，而你可以從Add下拉指令使用它們
  - xyz的座標軸數據值
  - 工具系統
  - 64位元Windows, Linux以及OS X版本
  - 讓您能以雕塑的方式塑模
  - 改良的動畫系統
  - 煙霧模擬
  - 粒子效果
  - 射線追蹤技術
  - 色彩管理器
  - 新的Python API

## 支持的格式

支持自身所用.blend之外的多种通用3D格式。

**导入**\[3\]

  - Collada（.dae）
  - Motion Capture（.bvh）
  - Scalable Vector Graphics（.svg）
  - Stanford（.ply）
  - [Stl](../Page/STL_\(檔案格式\).md "wikilink")（.stl）
  - Autodesk [3ds Max](../Page/3ds_Max.md "wikilink")（.3ds）
  - Autodesk FBX（.fbx）
  - [Wavefront](../Page/Wavefront_.obj文件.md "wikilink")（.obj）
  - X3D Extensible 3D（.x3d）

**导出**\[4\]

  - Collada（.dae）
  - Stanford（.ply）
  - Stl（.stl）
  - Autodesk 3ds Max（.3ds）
  - Autodesk FBX（.fbx）
  - Wavefront（.obj）
  - X3D Extensible 3D（.x3d）

## 支援

在世界各地有大約25萬Blender使用者，並有很多熱心人士自行寫了不少教學和架設使用論壇，其中最為著名的論壇是[Elysiun](http://www.elysiun.com)现在已经改名为[Blender艺术家](http://www.blenderartists.org)，服务器由美国迁到了荷兰。簡體中文的[Blender中国官方授权网站](http://www.blendercn.org)于2005年6月成立，翻译完成整部手册，成功解决了簡體中文显示难题，完成全面中文汉化工作，得到了官方的认可，筹建中的Blender中国支持团队致力于Blender的功能提升和动画制作以及中小企业支援。

正體中文的[Blender繁體中文社群](http://blender.tw)則大約於2008年建立，為正體中文使用者提供了良好的資訊平台和社群支援。

## 使用

[thumb](../Page/文件:Experience_curiosity1.png.md "wikilink")

国家航空航天局
（[NASA](../Page/美国国家航空航天局.md "wikilink")）为[火星探测车“好奇”着陆三周年纪念日作出了交互web](../Page/火星科学实验室.md "wikilink")-软件\[5\]。探测车的动作、摄影机和机械手的操纵是基于
Blender 和 [Blend4Web](../Page/Blend4Web.md "wikilink")
创造的软件\[6\]实现的，也打了一些著名的使命事件\[7\]\[8\]。软件是在[SIGGRAPH](../Page/SIGGRAPH.md "wikilink")2015大会WebGL部分的开始在年提出的\[9\]。

## 使用Blender的藝術家

以下的著名藝術家使用Blender為其主要或唯一的製片工具

  - [Andreas Goralczyk（@ndy）](http://www.artificial3d.com)，winner of two
    subsequent Suzanne Blender Awards（2003 - Best Animation，2004 - Best
    Still）
  - [Stefano Selleri
    (S68)](https://web.archive.org/web/20051024010229/http://www.selleri.org/Blender/)（Suzanne
    Blender Awards 2003 - Best Still）
  - Bassam Kurdali (slikdigit)（Suzanne Blender Awards 2004 - Best
    Animation）
  - [Bastian Salmela
    (basse)](https://web.archive.org/web/20050310080224/http://www.kimppu.org/basse/)
  - Endre Barath (endi)
  - Jean-Sébastien Guillemette（Ecks，formerly X-WARRIOR）
  - [Robert Tiess (RobertT)](http://www.artofinterpretation.com)
  - Huanghai（DeathBlood）2006 Blender F1艺术大赛第二名
  - [congcong009 2009 BWC艺术大赛第一名](http://blog.sina.com.cn/u/1273273101)
  - Blender Animation

## 在製片工業的使用

第一套使用Blender專業影片是-{zh-hans:[蜘蛛侠2](../Page/蜘蛛侠2.md "wikilink");zh-hant:[蜘蛛人2](../Page/蜘蛛人2.md "wikilink");}-，在該片製作中用來繪製分鏡腳本。

### 大象之梦（開源影片計劃：橙）

在2005年9月，有部份使用Blender的藝術家打算使用自由軟體製作一套短片，並將此計劃命名為「橙」。此計劃同時亦回饋了一些功能至Blender，詳情可參看[hair
rendering](http://orange.blender.org/blog/hairy-issues)及[橙色影片計劃在Blender的介紹網站](http://orange.blender.org/)

### 大雄兔（開源影片計劃：桃）

2007年10月1日「桃」開始運作，片名為[大雄兔](../Page/Big_Buck_Bunny.md "wikilink")，於2008年5月30日上映，是Blender基金會第2部開放版權、[創作共用授權的動畫電影](../Page/創作共用.md "wikilink")。不同於上一個影片[大象之梦](../Page/大象之梦.md "wikilink")，本篇全程無語音。

### 辛特尔（開源影片計劃：榴莲）

[辛特尔是Blender基金会第三部基于开源代码创作工具集](../Page/辛特尔.md "wikilink")、[創作共用授權动画短片](../Page/創作共用.md "wikilink")，片长15分钟，本次电影目标是创作真实感效果的动画短片，并进一步提升Blender在电影应用中的实践水平。

### Tears of Steel（開源影片計劃：芒果）

[Tears of
Steel是Blender基金会下属Blender研究所的第四部开源电影](../Page/Tears_of_Steel.md "wikilink")，於2012年9月26日上映。Blender研究所以非常开放态度来对待所有电影投资人，让电影制作的开放程度达到了空前的高度，该影片是实拍和大量后期结合的电影，加入了真人演員的表演。持续的开发使得Blender的功能提升到新的地步。

## 参考资料

## 外部連結

  - [Blender官方網站](http://www.blender.org/)
  - [Blender開發者維基](http://wiki.blender.org/)
  - [Blender官方授权中国網站](http://www.blendercn.org/)
  - [臺灣Blender使用者俱樂部](http://mis.lit.edu.tw/blgarage/)
  - [FetchCFD](http://fetchcfd.com/) 展示和探索工程模拟

[Category:三维图像软件](../Category/三维图像软件.md "wikilink")
[Category:自由三维图形软件](../Category/自由三维图形软件.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")
[Category:用C++編程的自由軟體](../Category/用C++編程的自由軟體.md "wikilink")
[Category:用Python編程的自由軟體](../Category/用Python編程的自由軟體.md "wikilink")
[Category:動畫軟件](../Category/動畫軟件.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:荷兰发明](../Category/荷兰发明.md "wikilink")

1.  <http://www.blender.org/blenderorg/blender-foundation/history/>
2.
3.
4.
5.
6.
7.
8.
9.