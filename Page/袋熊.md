**袋熊**，即**袋熊科**（，学名：*Vombatidae*），是[澳洲的](../Page/澳洲.md "wikilink")[有袋類动物](../Page/有袋類.md "wikilink")。牠們的腳短及[肌肉發達](../Page/肌肉.md "wikilink")，身長約有1米，尾巴亦很短。牠們生活於澳洲東南部及[塔斯馬尼亞州的](../Page/塔斯馬尼亞州.md "wikilink")[森林](../Page/森林.md "wikilink")、山地及[石楠地](../Page/石楠地.md "wikilink")。

## 特徵

袋熊會以其类似[齧齒目动物的](../Page/齧齒目.md "wikilink")[牙齒及強壯的爪來挖巢穴](../Page/牙齒.md "wikilink")。雖然牠們主要是黎明、白天及黃昏活動的，但仍會在夜間或寒冷的日子出來攝食。牠們並非容易看見的[動物](../Page/動物.md "wikilink")，但所到之處都可以看到牠們留下的證據。

袋熊是[草食性的](../Page/草食性.md "wikilink")，主要以[草](../Page/草.md "wikilink")、[莎草科](../Page/莎草科.md "wikilink")、[香草](../Page/香草.md "wikilink")、樹皮及樹根為食物。牠們的[門齒有點像](../Page/門齒.md "wikilink")[齧齒目的](../Page/齧齒目.md "wikilink")，可以用來咬粗糙的樹，或挖洞穴。就像其他的草食性動物一樣，牠們的門齒與頰齒之間有很大的縱裂。另外，牠們的糞便是方形的
，但生物學家還不是很清楚方形的排泄物的形成原因，一般推測是跟領地有關。

[澳洲野犬及](../Page/澳洲野犬.md "wikilink")[袋獾會獵食袋熊](../Page/袋獾.md "wikilink")。袋熊的毛皮顏色各有不同，一般由沙色至褐色，或灰色至黑色不等。所有其下的[物種都約只有](../Page/物種.md "wikilink")1米長，重20-35公斤。

雌性袋熊經過26-28日的[妊娠期後](../Page/妊娠期.md "wikilink")，會在[春天會產下一頭幼袋熊](../Page/春天.md "wikilink")。牠們有良好發育的[育幼袋](../Page/育幼袋.md "wikilink")，可保護幼袋熊6-7個月。袋熊在出生後15個月會斷奶，在18個月會達至性成熟。\[1\]

### 生態及行為

袋熊的[新陳代謝非常的慢](../Page/新陳代謝.md "wikilink")，差不多要用14天的時間來完成[消化](../Page/消化.md "wikilink")，這有助於生活在乾燥的環境。\[2\]牠們一般行動得很慢，但當遇上危害時，逃走速度可以達40km/h，並維持達90秒。袋熊會保護由其巢穴為中心的疆界，對入侵者存有攻擊性。[塔斯馬尼亞袋熊的疆界達](../Page/塔斯馬尼亞袋熊.md "wikilink")23公頃，而[毛鼻袋熊屬的則不多於](../Page/毛鼻袋熊屬.md "wikilink")4公頃。\[3\]

當受到攻擊時，袋熊會發揮巨大的抵抗力。例如當牠受到地底下的掠食者攻擊時，牠會破壞地底隧道，令掠食者窒息。牠主要的防禦是靠身體後部以[軟骨組成的結構](../Page/軟骨.md "wikilink")。連同不怎麼長的尾巴，可以避免在逃走入隧道時被掠食者攻擊尾部。

另外，袋熊是有袋動物中少數袋子開口朝後的，可避免在挖地洞時泥土跑進袋中影響小袋熊。

## 演化

袋熊是屬於[雙門齒目](../Page/雙門齒目.md "wikilink")。牠們的祖先於5500-2600萬年前開始[演化](../Page/演化.md "wikilink")。在進入[冰河時期前共有](../Page/冰河時期.md "wikilink")11個[物種生成](../Page/物種.md "wikilink")，當中幾類的體型像[犀牛](../Page/犀牛.md "wikilink")（如[雙門齒獸](../Page/雙門齒獸.md "wikilink")），是[有袋類中最大的](../Page/有袋類.md "wikilink")。最早的[澳洲原住民到達時正值雙門齒獸最為普遍的時期](../Page/澳洲原住民.md "wikilink")，這類動物的[滅絕可能是被獵殺](../Page/滅絕.md "wikilink")、失去棲息地等原因所造成。

## 物種

袋熊現存的只有三個[物種](../Page/物種.md "wikilink")：\[4\]

  - [塔斯馬尼亞袋熊](../Page/塔斯馬尼亞袋熊.md "wikilink")（*Vombatus ursinus*）
  - [毛鼻袋熊](../Page/毛鼻袋熊.md "wikilink")（*Lasiorhinus latifrons*）
  - [澳洲毛鼻袋熊](../Page/澳洲毛鼻袋熊.md "wikilink")（*Lasiorhinus krefftii*）

## 與人類的關係

被飼養的袋熊可以很溫馴。很多[澳洲的公園](../Page/澳洲.md "wikilink")、[動物園及其他旅遊設施都有袋熊展覽](../Page/動物園.md "wikilink")。

但是由於袋熊外型可愛溫馴，人們容易缺乏足夠警戒心，袋熊可能在憤怒或鬧情緒時帶有攻擊性。袋熊的體重可以將一般體型的成人撞倒，利齒及爪可以造成損傷。袋熊一咬可以咬破褲子、厚羊毛襪及膠靴，並造成2厘米深的傷口。至2013年世界最老的袋熊為[澳洲](../Page/澳洲.md "wikilink")[巴拉瑞特野生動物園](../Page/巴拉瑞特野生動物園.md "wikilink")([Ballarat
Wildlife
Park](../Page/:en:Ballarat_Wildlife_Park.md "wikilink"))27歲的派翠克(Patrick)。\[5\]\[6\]

## 參考

## 外部連結

  - [South Australian Government Faunal
    Emblem](https://web.archive.org/web/20071009172504/http://www.premcab.sa.gov.au/emblems/wombat1.htm)
    (official website)
  - [Wombania's Wombat Information
    Index](http://www.wombania.com/wombats/index.htm)
  - [Russell The Wombat's
    Burrow](https://web.archive.org/web/20080511183135/http://www.wombadilliac.com.au/)
  - [Fauna of Australia, Vombatidae by R. T.
    Wells](https://web.archive.org/web/20080307161840/http://www.environment.gov.au/biodiversity/abrs/publications/fauna-of-australia/pubs/volume1b/32-ind.pdf)

[Category:袋熊亞目](../Category/袋熊亞目.md "wikilink")
[Category:袋熊科](../Category/袋熊科.md "wikilink")

1.

2.
3.
4.
5.  國際中心／綜合報導,"有育幼袋外型像熊　澳洲派翠克是世界最老的「袋熊」"[1](http://www.ettoday.net/news/20130820/259180.htm),ETtoday寵物動物新聞,2013年08月20日
    10:41.

6.  Heather Lighton,"World's oldest
    wombat"[2](http://thethousands.com.au/melbourne/stray/world-s-oldest-wombat),STRAY/Three
    Thousand,retrieved Aug 20, 2013.