**西日本JR巴士**（）是由[JR西日本全資持有負責經營](../Page/西日本旅客鐵道.md "wikilink")[巴士業務的公司](../Page/巴士.md "wikilink")。總社在[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[此花區北港一丁目](../Page/此花區.md "wikilink")3番23號。營業區域為[島根縣](../Page/島根縣.md "wikilink")、[岡山縣](../Page/岡山縣.md "wikilink")、[廣島縣](../Page/廣島縣.md "wikilink")、[山口縣以外的JR西日本轄區](../Page/山口縣.md "wikilink")。

## 历史

  - 1986年12月4日　[日本國有鐵道改革法施行](../Page/日本國有鐵道改革法.md "wikilink")。旅客自動車運送事業由各旅客會社繼承、原則上是獨立經營。
  - 1987年4月1日　國鐵分割民營化、JR西日本成立。
  - 1988年3月1日　**西日本JR巴士株式會社**成立。
  - 1988年4月1日 營業開始。
  - 1996年2月　神戶貸切中心開設。
  - 1998年2月1日　大阪營業所貸切部門分裂、大阪貸切中心與統合京橋營業所開設(舊[片町站位置](../Page/片町站.md "wikilink"))。
  - 1998年3月1日　神戶營業所開設。
  - 2001年11月30日　廢除龜山貸切巴士中心。
  - 2002年2月4日　廢除京橋營業所。轉移至本社・大阪高速管理所、舊大阪高速管理所成為大阪高速管理所豊崎支所。
  - 2002年3月31日　廢除金澤營業所穴水支所・金澤營業所能登飯田・福光派出所・近江今津營業所敦賀・小濱支所・紀伊田邊營業所新宮派出所・木之本貸切巴士中心。
  - 2002年9月30日　廢除五條營業所・信樂派出所。
  - 2003年2月5日 大阪～高松・丸龜線(高松Express大阪號)Point Card Service開始。
  - 2003年3月28日　廢除加茂營業所。
  - 2003年12月1日　大阪高速管理所豊崎支所改名大阪北營業所。
  - 2005年3月31日　廢除水口營業所。
  - 2005年6月13日　開設金澤營業所美山派出所。
  - 2006年9月30日 大阪～高松・丸龜線(高松Express大阪號)Point Card Service結束。

## 車輛

### 車輛稱號

[644-9910-Nishinihon-JR-Bus-P-MS729SA.jpg](https://zh.wikipedia.org/wiki/File:644-9910-Nishinihon-JR-Bus-P-MS729SA.jpg "fig:644-9910-Nishinihon-JR-Bus-P-MS729SA.jpg")
[JRbus_372.JPG](https://zh.wikipedia.org/wiki/File:JRbus_372.JPG "fig:JRbus_372.JPG")
[JRbus_3175.JPG](https://zh.wikipedia.org/wiki/File:JRbus_3175.JPG "fig:JRbus_3175.JPG")
[West_JR_Bus_Seishun_Mega_Dream.jpg](https://zh.wikipedia.org/wiki/File:West_JR_Bus_Seishun_Mega_Dream.jpg "fig:West_JR_Bus_Seishun_Mega_Dream.jpg")
基本的上依據[國鐵巴士\#車輛稱號來編排](../Page/國鐵巴士#車輛稱號.md "wikilink")。

| 7  | 4  | 8   | \- | 9  | 9  | 85   |
| -- | -- | --- | -- | -- | -- | ---- |
| 車種 | 形狀 | 製造廠 |    | 年式 | 装備 | 固有編號 |

  - 車種
      -
        1…車體宽2300mm以下 全長未滿7000mm(Micro巴士)
        2…車體宽2300mm以下 全長7000mm以上8400mm未滿(中型巴士)
        3…車體宽2300mm以下 全長8400mm以上9000mm未滿(中型巴士)
        4…車體宽超過2300mm 全長9000mm以上10000mm未滿(大型系)
        5…全長10000mm以上(大型巴士)
        6…觀光・高速
        7…高速(雙層)
  - 形狀
      -
        1…横向座席
        2…混合(半數以上前向)
        3…前向座席
        4…躺椅
  - 製造廠
      -
        1…[五十鈴](../Page/五十鈴汽車.md "wikilink")
        4…[三菱扶桑](../Page/三菱扶桑.md "wikilink")
        7…[日野](../Page/日野汽車.md "wikilink")
        8…[日產柴油](../Page/日產柴油.md "wikilink")
        9…外車
  - 年式
      - 西暦下1桁
  - 裝備
      -
        4…冷氣・板式
        9…冷氣・氣墊
  - 固有編號
      - 前5桁連號
      - 01～49與71～99為主要路線車・51～69為貸切車

依據上記的法則、「748-9985」是「有躺椅装備的雙層高速車，由日産DIESEL製、製造年為xxx9年、有冷氣・氣墊設備的車輛」，為第85號車。

## 關連會社

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
  - [中國JR巴士](../Page/中國JR巴士.md "wikilink")

## 外部連結

  - [西日本JR巴士](http://www.nishinihonjrbus.co.jp/)

[Category:日本巴士公司](../Category/日本巴士公司.md "wikilink")
[Category:西日本旅客鐵道](../Category/西日本旅客鐵道.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")