[SOP_Logo.gif](https://zh.wikipedia.org/wiki/File:SOP_Logo.gif "fig:SOP_Logo.gif")
**倫敦大學學院藥學院**（****）\[1\]，是由[英國皇家藥物協會](../Page/英國皇家藥物協會.md "wikilink")（The
Royal Pharmaceutical Society of Great
Britain，RPSGB）於1842年所創立之皇家藥物協會學院(College
of the Pharmaceutical Society)，創校校址為No 17, Bloomsbury Square,
London，自1925年起開始授予[倫敦大學學歷](../Page/倫敦大學.md "wikilink")。
自1935年起，於[倫敦](../Page/倫敦.md "wikilink")29/39 Brunswick
Square啟動新校區建築工程，該工程於1939因[第二次世界大戰而停工](../Page/第二次世界大戰.md "wikilink")，該校也在二戰期間遷校自卡爾地夫(Cardiff)直到1943年9月始遷回[倫敦](../Page/倫敦.md "wikilink")，並於1949年正式脫離皇家藥物協會而成為[倫敦大學轄下其中的一所專門學院](../Page/倫敦大學.md "wikilink")，為[英國第一所頒授](../Page/英國.md "wikilink")[藥學學位之教育機構](../Page/藥學.md "wikilink")，同年更名為更名為[倫敦大學藥學院](../Page/倫敦大學藥學院.md "wikilink")(The
School of Pharmacy, University of London
或縮寫為ULSOP或SOP)，該校[紋章於](../Page/紋章.md "wikilink")1950年誕生，其上註有拉丁文Salutifer
Orbi，英文翻譯為 Bringing health to the
world，也就是將健康帶來這個世界之[校訓](../Page/校訓.md "wikilink")(motto)。[皇家特許狀](../Page/皇家特許狀.md "wikilink")([Royal
Charter](../Page/Royal_Charter.md "wikilink"))於1952年獲頒。位於29/39 Brunswick
Square的新校建築終於在1960年完工啟用，並由[伊莉莎白王太后](../Page/伊莉莎白王太后.md "wikilink")(Queen
Elizabeth，The Queen Mother
時任倫敦大學校長)舉行落成啟用典禮，[伊莉莎白王太后在落成典禮上稱之為倫敦當年最古老的新建築](../Page/伊莉莎白王太后.md "wikilink")(因其始造於1938年)，該建築亦成為英國史上第一間專門為[藥學院而修建的](../Page/藥學院.md "wikilink")[建物](../Page/建物.md "wikilink")。由於該校校址皆位於倫敦的Squares旁，該校師生與校友亦暱稱該校為"The
Square"，該校之正方形校徽也因此而來。\[2\]
經過長時間和廣泛的咨詢，在該校師生抗議未果之下，倫敦大學藥學院在2011年5月13日正式公布與[倫敦大學學院的合併計劃](../Page/倫敦大學學院.md "wikilink")，最後一屆[倫敦大學藥學院畢業證書於](../Page/倫敦大學藥學院.md "wikilink")2011年9月頒發。\[3\]有關合併計劃於2012年1月起生效，該校改隸[倫敦大學學院](../Page/倫敦大學學院.md "wikilink")，並更為現名([倫敦大學學院](../Page/倫敦大學學院.md "wikilink")
藥學院 [UCL School of
Pharmacy](../Page/UCL_School_of_Pharmacy.md "wikilink"))，自2012年起改與頒發[倫敦大學學院之畢業證書](../Page/倫敦大學學院.md "wikilink")，校友亦併入[倫敦大學學院管理](../Page/倫敦大學學院.md "wikilink")。

## [紋章](../Page/紋章.md "wikilink")

The shield has a blue band across the top and a blue diagonal band
running from the top right-hand corner\* on either side of which stands
a mortar and pestle in gold on a silvery white ground.

On the diagonal band is the Staff of Asclepius, entwined by a serpent in
gold. Upon the transverse blue band is in the centre a Tudor rose
surrounded by golden rays and on either side of it an open book, with
gilt edges and clasps, these being part of the arms of the University of
London.

The helm is the plain-helm of an esquire and on top of the helm is the
crest consisting of two poppy heads with stalks crossed in the form of a
St Andrew's cross, arranged in front of a foxglove. Covering the helm
behind is the mantle. This consists of stylized foliage ending in two
tassels on either side below the motto 'Salutifer Orbi' and two more
tassels on either side of the helm.

  - In heraldic decriptions, the right- and left-hand sides of a shield
    are indicated in the same way as the right and left of the person
    carrying the shield and not from the point of view of the observer.

## 大學排名

[QS世界大學科目排名](../Page/QS世界大學科目排名.md "wikilink")(QS World University
Rankings by
Subject)之[藥學與](../Page/藥學.md "wikilink")[藥理學](../Page/藥理學.md "wikilink")(Pharmacy
& Pharmacology)科目:
2015年排名世界第五位，歐洲排名第三位，僅次於[英國](../Page/英國.md "wikilink")[牛津大學與](../Page/牛津大學.md "wikilink")[英國](../Page/英國.md "wikilink")[劍橋大學](../Page/劍橋大學.md "wikilink")，但牛津大學與劍橋大學僅於[醫學院設立](../Page/醫學院.md "wikilink")[藥理學研究所](../Page/藥理學.md "wikilink")，並無設立獨立[藥學院亦無授予英國](../Page/藥學院.md "wikilink")[藥師必須具備之藥學士](../Page/藥師.md "wikilink")[Master
of Pharmacy](../Page/Master_of_Pharmacy.md "wikilink")
[MPharm文憑亦無教授其他藥學專業科目如](../Page/MPharm.md "wikilink")[藥劑學](../Page/藥劑學.md "wikilink"))。
2016年排名世界第八位。
2017年排名世界第八位。
2018年排名世界第七位。\[4\]

## 學科

[School_of_pharmacy_mckay.jpg](https://zh.wikipedia.org/wiki/File:School_of_pharmacy_mckay.jpg "fig:School_of_pharmacy_mckay.jpg")
The School is organised into four academic departments, each with one or
more associated specialist research centres.

### 藥物化學與生物化學

The Department of Pharmaceutical and Biological Chemistry is the largest
of the School's departments. Its research is focused on cancer, natural
products and phytomedicines, molecular neurosciences and
biopharmaceutical analysis.

The department's staff help teach the undergraduate MPharm degree in the
areas of drug discovery, medicinal chemistry, pharmaceutical chemistry
and pharmacognosy/medicinal plants, and an MSc in Pharmacognosy is
offered.

### 藥理學

The Wellcome Department of Pharmacology is one of the oldest departments
of pharmacology in the UK. The department has played a major role in the
development of Pharmacology in the UK and many pharmacologists who
trained here are to be found in academies and in industries all over the
world.

The department's research focuses on the nervous system, and a wide
range of approaches are used to study normal brain function and the
causes of many neurological and psychiatric diseases.

### 藥劑學

The Department of Pharmaceutics is home to a wide range of research
activities, such as in Materials Science and Processing and Clinical
Pharmaceutical Science.

The department's research in Materials Science and Processing is centred
on the fundamental properties of materials and their adaptation to
optimise processing and enhance drug delivery. It operates a number of
joint ventures, including the Centre for Paediatric Pharmacy Research, a
joint venture with Great Ormond Street Hospital and the Institute of
Child Health, and in the Clinical Pharmaceutics with University College
Hospitals and Camden and Islington's NHS Trust.

The Microbiology Research Group is also well-established, with work
focusing mainly in overcoming antibiotic resistance and obtaining new
actives from natural sources. The Group has been particularly successful
in investigating new approaches to the treatment of the ‘superbug’
[MRSA](../Page/Methicillin-resistant_Staphylococcus_aureus.md "wikilink").

### Practice and Policy

The Department of Practice and Policy focuses upon making the use of
medicines safer and more effective through teaching, service and
research.

## 外部連結

  - [倫敦大學學院藥學院官方網站](http://www.ucl.ac.uk/pharmacy/)

## 参考资料

[Category:伦敦大学学院](../Category/伦敦大学学院.md "wikilink")
[Category:1842年創建的教育機構](../Category/1842年創建的教育機構.md "wikilink")
[Category:1842年英格蘭建立](../Category/1842年英格蘭建立.md "wikilink")

1.
2.
3.
4.