[大渡口区](../Page/大渡口区.md "wikilink"){{.w}}[江北区](../Page/江北区_\(重庆市\).md "wikilink"){{.w}}[南岸区](../Page/南岸区.md "wikilink"){{.w}}[北碚区](../Page/北碚区.md "wikilink"){{.w}}[渝北区](../Page/渝北区.md "wikilink"){{.w}}[巴南区](../Page/巴南区.md "wikilink"){{.w}}[长寿区](../Page/长寿区.md "wikilink"){{.w}}[沙坪坝区](../Page/沙坪坝区.md "wikilink"){{.w}}[万州区](../Page/万州区.md "wikilink"){{.w}}[涪陵区](../Page/涪陵区.md "wikilink"){{.w}}[黔江区](../Page/黔江区.md "wikilink"){{.w}}[永川区](../Page/永川区.md "wikilink"){{.w}}[合川区](../Page/合川区.md "wikilink"){{.w}}[江津区](../Page/江津区.md "wikilink"){{.w}}[九龙坡区](../Page/九龙坡区.md "wikilink"){{.w}}[南川区](../Page/南川区.md "wikilink"){{.w}}[綦江区](../Page/綦江区.md "wikilink"){{.w}}[大足区](../Page/大足区.md "wikilink"){{.w}}[璧山区](../Page/璧山区.md "wikilink"){{.w}}[铜梁区](../Page/铜梁区.md "wikilink"){{.w}}[荣昌区](../Page/荣昌区.md "wikilink"){{.w}}[潼南区](../Page/潼南区.md "wikilink"){{.w}}[开州区](../Page/开州区.md "wikilink"){{.w}}[梁平区](../Page/梁平区.md "wikilink"){{.w}}[武隆区](../Page/武隆区.md "wikilink")

|group3 = [县](../Page/县.md "wikilink") |list3 =
[忠县](../Page/忠县.md "wikilink"){{.w}}[城口县](../Page/城口县.md "wikilink"){{.w}}[垫江县](../Page/垫江县.md "wikilink"){{.w}}[丰都县](../Page/丰都县.md "wikilink"){{.w}}[奉节县](../Page/奉节县.md "wikilink"){{.w}}[云阳县](../Page/云阳县.md "wikilink"){{.w}}[巫溪县](../Page/巫溪县.md "wikilink"){{.w}}[巫山县](../Page/巫山县.md "wikilink")

|group4 = [自治县](../Page/自治县.md "wikilink") |list4 =
[石柱土家族自治县](../Page/石柱土家族自治县.md "wikilink"){{.w}}[秀山土家族苗族自治县](../Page/秀山土家族苗族自治县.md "wikilink"){{.w}}[酉阳土家族苗族自治县](../Page/酉阳土家族苗族自治县.md "wikilink"){{.w}}[彭水苗族土家族自治县](../Page/彭水苗族土家族自治县.md "wikilink")

|belowstyle = text-align:left; font-size:80%; |below =
参：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[重庆市乡级以上行政区列表](../Page/重庆市乡级以上行政区列表.md "wikilink")。
}}<noinclude> </noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[重庆行政区划模板](../Category/重庆行政区划模板.md "wikilink")