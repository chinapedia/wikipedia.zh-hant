**毛腿吸血蝠**为**毛腿吸血蝠屬**下唯一一种，[哺乳綱](../Page/哺乳綱.md "wikilink")、[翼手目](../Page/翼手目.md "wikilink")、[葉口蝠科的一屬](../Page/葉口蝠科.md "wikilink")，而與毛腿吸血蝠屬同科的動物有[長腿蝠屬](../Page/長腿蝠屬.md "wikilink")（麗長腿蝠）、白翼吸血蝠屬、吸血蝠屬等數種[哺乳動物](../Page/哺乳動物.md "wikilink")。

## 参考文献

[Category:葉口蝠科](../Category/葉口蝠科.md "wikilink")
[Category:哥伦比亚哺乳动物](../Category/哥伦比亚哺乳动物.md "wikilink")
[Category:哥斯达黎加哺乳动物](../Category/哥斯达黎加哺乳动物.md "wikilink")
[Category:危地马拉哺乳动物](../Category/危地马拉哺乳动物.md "wikilink")
[Category:巴拿马哺乳动物](../Category/巴拿马哺乳动物.md "wikilink")
[Category:秘鲁哺乳动物](../Category/秘鲁哺乳动物.md "wikilink")