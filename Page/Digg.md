**digg**是一个以[科技为主的新闻站点](../Page/科技.md "wikilink")，用户可提交新闻予digg，通过digg机制显示於digg首页上。

## 网站

与一般新闻网站不同的是，在digg中用户可以提交新闻并订阅新闻，当订阅数达到一定数量后，digg算法将自动把新闻加入首页。文章分三种（首页发布的文章，列队等待投票文章和全部文章）保存在不同分类中，读者可以浏览所有的提交的文章，当一个文章得到足够多的票数，它将会显示到首页。文章如果没有得到足够的票数或者一定数量的读者报告这个提交有问题，它留在“全部文章里”，在设定的天数后会被系统删除。digg把文章筛选的权利交给了网民，筛选最受关注和有价值的文章。

## 類似網站

在中国曾经出现了有和digg形式类似的许多网站，例如以翻译Digg内容为主的煎蛋网（jandan.net)，如今都已经改版或关闭。

## 页面分类

在目前版本中，文章可以分为6个大主题：[科技](../Page/科技.md "wikilink")、[科学](../Page/科学.md "wikilink")、[世界新闻](../Page/世界新闻.md "wikilink")、[影像](../Page/影像.md "wikilink")、[娱乐](../Page/娱乐.md "wikilink")、[游戏和](../Page/游戏.md "wikilink")[体育](../Page/体育.md "wikilink")。每个主题中也有小主题，例如科技可以分为[苹果公司](../Page/苹果公司.md "wikilink")、[软件](../Page/软件.md "wikilink")、[硬件](../Page/硬件.md "wikilink")、[unix](../Page/unix.md "wikilink")、[编程和安全等](../Page/编程.md "wikilink")。

## 参见

  - [Delicious](../Page/Delicious.md "wikilink")
  - [社会化书签](../Page/社会化书签.md "wikilink")
  - [Voat](../Page/Voat.md "wikilink")
  - [Reddit](../Page/Reddit.md "wikilink")

## 參考資料

## 外部链接

[Category:社群網站](../Category/社群網站.md "wikilink")
[Category:2004年建立的网站](../Category/2004年建立的网站.md "wikilink")
[Category:Web 2.0](../Category/Web_2.0.md "wikilink")
[Category:网站](../Category/网站.md "wikilink")
[Category:新聞網站](../Category/新聞網站.md "wikilink")
[Category:社会信息处理](../Category/社会信息处理.md "wikilink")
[Category:总部位于纽约市的公司](../Category/总部位于纽约市的公司.md "wikilink")