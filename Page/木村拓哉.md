**木村拓哉**（，）是[日本一名](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")、[配音員](../Page/配音員.md "wikilink")，已解散的日本偶像組合[SMAP的前成員](../Page/SMAP.md "wikilink")。東京都出身，經紀公司為[傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")。2000年的時候與[工藤靜香完婚](../Page/工藤靜香.md "wikilink")，\[1\]
並分別於2001年和2003年誕下木村心美與[木村光希兩個女兒](../Page/木村光希.md "wikilink")。\[2\]

## 簡歷

1987年11月，加入[傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")。1991年9月9日，與[中居正廣](../Page/中居正廣.md "wikilink")、[稲垣吾郎](../Page/稲垣吾郎.md "wikilink")、[森且行](../Page/森且行.md "wikilink")、[草彅剛](../Page/草彅剛.md "wikilink")、[香取慎吾組成男子偶像團體](../Page/香取慎吾.md "wikilink")[SMAP](../Page/SMAP.md "wikilink")，\[3\]
並以單曲《[Can't Stop\!\!
-LOVING-](../Page/Can't_Stop!!_-LOVING-.md "wikilink")》正式出道。\[4\]

1994年因出演[電影](../Page/電影.md "wikilink")《[足球風雲](../Page/足球風雲#.E9.9B.BB.E5.BD.B1.E7.89.88.md "wikilink")》而獲得[石原裕次郎新人獎](../Page/日刊體育電影大獎.md "wikilink")。\[5\]
隨後，數度出演電視劇、電影。在[富士電視台主打電視劇時段](../Page/富士電視台.md "wikilink")“[月九](../Page/月九.md "wikilink")”獲得10次主演資格，成為該時段最多主演次數記錄（截止2017年）。\[6\]

2000年，與歌手[工藤靜香結婚](../Page/工藤靜香.md "wikilink")。\[7\]

2008年，連續15年獲得《》“最喜歡的男性排行榜”冠軍。\[8\]

2016年12月31日，SMAP解散，成為獨立藝人。\[9\]

## 獲獎歷史

  - 第11－15屆日本獎得主（1994年－1998年），名人堂入選。\[10\]

<!-- end list -->

  - [石原裕次郎獎](../Page/日刊體育電影大獎.md "wikilink")
      - 第7屆新人獎：《[足球風雲](../Page/足球風雲#.E9.9B.BB.E5.BD.B1.E7.89.88.md "wikilink")》（1994年）\[11\]
      - 第20屆最佳男主角：《》（2007年）\[12\]

<!-- end list -->

  - ·新人獎（1995年）\[13\]

<!-- end list -->

  - 第16屆最佳男主角：《武士的一分》（2006年）\[14\]

<!-- end list -->

  - 第32屆[銀河賞個人獎](../Page/銀河賞.md "wikilink")：《[青春無悔](../Page/青春無悔.md "wikilink")》（1994年）\[15\]

<!-- end list -->

  - [日劇學院賞最佳男主角](../Page/日劇學院賞.md "wikilink")
      - 第9屆：《[悠長假期](../Page/悠長假期.md "wikilink")》（1996年）
      - 第15屆：《[戀愛世紀](../Page/戀愛世紀.md "wikilink")》（1997年）
      - 第19屆：《[沉睡的森林](../Page/沉睡的森林.md "wikilink")》（1998年）
      - 第24屆：《[美麗人生](../Page/美麗人生_\(日本電視劇\).md "wikilink")》（1999年）
      - 第28屆：《[HERO](../Page/HERO_\(日本電視劇\).md "wikilink")》（2001年）
      - 第33屆：《[從天而降億萬顆星星](../Page/從天而降億萬顆星星.md "wikilink")》（2002年）
      - 第52屆：《[華麗一族](../Page/華麗一族.md "wikilink")》（2007年）
      - 第57屆：《[CHANGE](../Page/CHANGE.md "wikilink")》（2008年）
      - 第75屆：《[PRICELESS～無價人生～](../Page/PRICELESS人生無價.md "wikilink")》（2012年）
      - 第82屆：《[HERO
        第二季](../Page/HERO_\(日本電視劇\).md "wikilink")》（2014年）\[16\]

<!-- end list -->

  - [TV LIFE年度日劇大賞最佳男主角](../Page/TV_LIFE年度日劇大賞.md "wikilink")\[17\]
      - 第6屆：《悠長假期》（1996年）
      - 第7屆：《》、《戀愛世紀》（1997年）
      - 第10屆：《美麗人生》（2000年）
      - 第11屆：《HERO》（2001年）

<!-- end list -->

  - [日刊體育日劇大賞最佳男主角](../Page/日刊體育日劇大賞.md "wikilink")\[18\]
      - 第1屆：《戀愛世紀》（1998年）
      - 第3屆：《美麗人生》（1999年）
      - 第4屆：《HERO》（2001年）
      - 第6屆：《[夢想飛行](../Page/夢想飛行.md "wikilink")》（2003年）
      - 第17屆秋：《[安堂機器人](../Page/安堂機器人.md "wikilink")》（2013年）\[19\]
      - 第18屆夏：《HERO 第二季》（2014年）\[20\]

<!-- end list -->

  - [TVnavi日劇年度大賞最佳男主角](../Page/TVnavi日劇年度大賞.md "wikilink")\[21\]
      - 第4屆：《華麗一族》（2007年）
      - 第9屆：《PRICELESS～無價人生～》（2012年）
      - 第10屆：《安堂機器人》（2013年）

<!-- end list -->

  - 第26屆男演員演技獎：《美麗人生》（2000年）\[22\]

<!-- end list -->

  - 日本放送映画藝術大賞優秀男主角獎
      - 第61屆：《武士的一分》（2007年）
      - 第66屆：《[南極大陸](../Page/南極大陸_\(電視劇\).md "wikilink")》（2011年）

<!-- end list -->

  - 首爾戲劇獎最佳男主角：《華麗一族》（2007年）

<!-- end list -->

  - 第52屆[全日本CM放送聯盟CM節演員獎](../Page/全日本CM放送聯盟.md "wikilink")：[豐田汽車](../Page/豐田汽車.md "wikilink")（2012年）\[23\]

## 演出作品

<small>*本章節列舉的為木村拓哉以個人身份出演的作品，其以團隊成員身份出演的作品請參看[SMAP](../Page/SMAP.md "wikilink")。**粗體字**表示為主演。*</small>

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>電視台</strong></p></td>
<td><p><strong>備注／獲獎</strong></p></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p><a href="../Page/:ja:あぶない少年III.md" title="wikilink">危險少年3</a></p></td>
<td><p>木村拓哉</p></td>
<td><p><a href="../Page/東京電視台.md" title="wikilink">東京電視台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p><a href="../Page/是誰偷走我的心.md" title="wikilink">是誰偷走我的心</a></p></td>
<td><p>片瀨雅人</p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p><a href="../Page/愛情白皮書.md" title="wikilink">愛情白皮書</a></p></td>
<td><p>取手治</p></td>
<td><p>富士電視台</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p><a href="../Page/青春無悔.md" title="wikilink">青春無悔</a></p></td>
<td><p>上田武志</p></td>
<td><p>富士電視台</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p><a href="../Page/:ja:人生は上々だ.md" title="wikilink">人往高處爬</a></p></td>
<td><p>大上一馬</p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p>古畑任三郎（紅線、藍線）</p></td>
<td><p>林功夫</p></td>
<td></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p>古畑任三郎（消失的古畑任三郎）</p></td>
<td><p>林功夫</p></td>
<td></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/悠長假期.md" title="wikilink">悠長假期</a></strong></p></td>
<td><p><strong>瀨名秀俊</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>首次主演的作品／首次主演富士電視台<a href="../Page/月九.md" title="wikilink">月九</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/:ja:協奏曲_(テレビドラマ).md" title="wikilink">協奏曲</a></p></td>
<td><p>貴倉翔</p></td>
<td><p>TBS電視台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p>理想與友情</p></td>
<td><p>黒澤力</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>一個好人</p></td>
<td><p>早阪由紀夫</p></td>
<td></td>
<td><p>特別出演</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/:ja:ギフト_(テレビドラマ).md" title="wikilink">快遞高手</a></strong></p></td>
<td><p><strong>早坂由紀夫</strong></p></td>
<td><p>富士電視台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/戀愛世紀.md" title="wikilink">戀愛世紀</a></strong></p></td>
<td><p><strong>片桐哲平</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第2度主演富士電視台月九</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p>織田信長 奪取天下的傻子</p></td>
<td><p><a href="../Page/織田信長.md" title="wikilink">織田信長</a></p></td>
<td></td>
<td><p>單元劇</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/沉睡森林.md" title="wikilink">沉睡森林</a></strong></p></td>
<td><p><strong>伊藤直季</strong></p></td>
<td><p>富士電視台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/世界奇妙物語.md" title="wikilink">世界奇妙物語</a>99春SP-狗仔隊</p></td>
<td><p>喜美男</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>古畑任三郎 VS. SMAP</p></td>
<td><p>木村拓哉</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>今夜營業中</p></td>
<td><p>村木拓哉、木村拓哉</p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p>傳說中的教師</p></td>
<td><p>水谷庸司</p></td>
<td></td>
<td><p>友情出演</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/美麗人生_(日劇).md" title="wikilink">美麗人生</a></strong></p></td>
<td><p><strong>沖島柊二</strong></p></td>
<td><p>TBS電視台</p></td>
<td><p>首次主演TBS電視台<a href="../Page/周日劇場.md" title="wikilink">周日劇場</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>冠軍大胃王</p></td>
<td><p>九官鳥九太郎（配音）</p></td>
<td><p>日本電視台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><strong><a href="../Page/律政英雄.md" title="wikilink">律政英雄</a></strong></p></td>
<td><p><strong>久利生公平</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第3度主演富士電視台月九</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/忠臣蔵_1/47.md" title="wikilink">忠臣蔵 1/47</a></p></td>
<td><p><a href="../Page/堀部武庸.md" title="wikilink">堀部武庸</a></p></td>
<td></td>
<td><p>單元劇</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>|<strong><a href="../Page/從天而降億萬顆星星.md" title="wikilink">從天而降億萬顆星星</a></strong></p></td>
<td><p><strong>片瀨涼</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第4度主演富士電視台月九</p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>|<strong><a href="../Page/夢想飛行Good_Luck.md" title="wikilink">夢想飛行Good Luck</a></strong></p></td>
<td><p><strong>新海元</strong></p></td>
<td><p>TBS電視台</p></td>
<td><p>第2度主演TBS電視台周日劇場</p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>|<strong><a href="../Page/冰之驕子Pride.md" title="wikilink">冰之驕子Pride</a></strong></p></td>
<td><p><strong>里中晴</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第5度主演富士電視台月九</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><strong><a href="../Page/飆風引擎.md" title="wikilink">飆風引擎</a></strong></p></td>
<td><p><strong>神崎次郎</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第6度主演富士電視台月九</p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/西遊記_(2006年電視劇).md" title="wikilink">西遊記</a></p></td>
<td><p>幻翼大王</p></td>
<td></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/律政英雄之小鎮風雲.md" title="wikilink">律政英雄之小鎮風雲</a></strong></p></td>
<td><p><strong>久利生公平</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>單元劇</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><strong><a href="../Page/華麗一族.md" title="wikilink">華麗一族</a></strong></p></td>
<td><p><strong>萬俵鐵平</strong></p></td>
<td><p>TBS電視台</p></td>
<td><p>第3度主演TBS電視台周日劇場、TBS電視台55週年台慶紀念電視劇</p></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p>|<strong><a href="../Page/CHANGE.md" title="wikilink">CHANGE</a></strong></p></td>
<td><p><strong>朝倉啟太</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第7度主演富士電視台月九</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><strong><a href="../Page/MR.BRAIN.md" title="wikilink">MR.BRAIN</a></strong></p></td>
<td><p><strong>九十九龍介</strong></p></td>
<td><p>TBS電視台</p></td>
<td><p>第4度主演TBS電視台</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/烏龍派出所#真人版.md" title="wikilink">烏龍派出所</a></p></td>
<td><p>拓仔</p></td>
<td></td>
<td><p>特別出演</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><strong><a href="../Page/月之戀人.md" title="wikilink">月之戀人</a></strong></p></td>
<td><p><strong>葉月蓮介</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第8度主演富士電視台月九</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SMAP毒蕃茄殺人事件.md" title="wikilink">SMAP毒蕃茄殺人事件特別篇</a></p></td>
<td><p>木村拓哉</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>|<strong><a href="../Page/南極大陸_(電視劇).md" title="wikilink">南極大陸</a></strong></p></td>
<td><p><strong>倉持岳志</strong></p></td>
<td><p>TBS電視台</p></td>
<td><p>第5度主演TBS電視台周日劇場</p></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><strong><a href="../Page/Priceless無價人生.md" title="wikilink">Priceless無價人生</a></strong></p></td>
<td><p><strong>金田一二三男</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第9度主演富士電視台月九</p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><strong><a href="../Page/安堂機器人.md" title="wikilink">安堂機器人</a></strong></p></td>
<td><p><strong>安堂萊特／沫嶋黎士</strong></p></td>
<td><p>TBS電視台</p></td>
<td><p>一人分飾二角<br />
第6度主演TBS電視台周日劇場</p></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><strong><a href="../Page/宮本武藏_(2014年電視劇).md" title="wikilink">宮本武藏 (2014年電視劇)</a></strong></p></td>
<td><p><strong><a href="../Page/宮本武藏.md" title="wikilink">宮本武藏</a></strong></p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台</a></p></td>
<td><p>單元劇、朝日電視台開業55週年紀念特別電視劇</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/極惡頑暴.md" title="wikilink">極惡頑暴</a></p></td>
<td><p>久利生公平</p></td>
<td><p>富士電視台</p></td>
<td><p>特別客串（完結篇）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/律政英雄.md" title="wikilink">律政英雄2</a></strong></p></td>
<td><p><strong>久利生公平</strong></p></td>
<td><p>富士電視台</p></td>
<td><p>第10度主演富士電視台月九</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><strong><a href="../Page/家的記憶.md" title="wikilink">家的記憶</a></strong></p></td>
<td><p><strong>家路久</strong></p></td>
<td><p>朝日電視台</p></td>
<td><p>首度主演朝日電視台連續劇</p></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p>|<strong><a href="../Page/A_LIFE〜深愛的人〜.md" title="wikilink">A LIFE〜深愛的人〜</a></strong></p></td>
<td><p><strong>沖田一光</strong></p></td>
<td><p>TBS電視台</p></td>
<td><p>第7度主演TBS電視台周日劇場</p></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><strong><a href="../Page/BG～身邊警護人～.md" title="wikilink">BG〜身邊警護人〜</a></strong></p></td>
<td><p><strong>島崎章</strong></p></td>
<td><p>朝日電視台</p></td>
<td><p>第2度主演朝日電視台連續劇</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

  - [足球風雲](../Page/足球風雲#.E9.9B.BB.E5.BD.B1.E7.89.88.md "wikilink")（1994年3月12日公映，[松竹](../Page/松竹.md "wikilink")）－飾：久保嘉晴

<!-- end list -->

  - （1995年9月23日公映，[日本先驅電影](../Page/角川映畫.md "wikilink")）－飾：上田淳一郎

<!-- end list -->

  - [2046](../Page/2046_\(電影\).md "wikilink")（2004年10月23日公映，[日本迪士尼影業](../Page/华特迪士尼影片.md "wikilink")）－飾：Taku

<!-- end list -->

  - （2006年12月1日公映，松竹）－飾：**三村新之丞**

<!-- end list -->

  - [HERO](../Page/HERO_\(2007年電影\).md "wikilink")（2007年9月8日公映，[東寶](../Page/東寶.md "wikilink")）－飾：**久利生公平**

<!-- end list -->

  - [伴雨行](../Page/幻雨追緝.md "wikilink")（2009年6月6日公映，）－飾：師濤

<!-- end list -->

  - [宇宙戰艦大和號](../Page/宇宙戰艦大和號.md "wikilink")（2010年12月1日公映，東寶）－飾：\[24\]

<!-- end list -->

  - [HERO](../Page/HERO_\(2015年電影\).md "wikilink")（2015年7月18日公映，東寶）－飾：**久利生公平**

<!-- end list -->

  - [無限之住人](../Page/無限之住人#.E9.9B.BB.E5.BD.B1.md "wikilink")（2017年4月29日公映，[華納兄弟](../Page/華納兄弟.md "wikilink")）－飾：**万次**\[25\]\[26\]

<!-- end list -->

  - [检察方的罪人](../Page/检察方的罪人#.E9.9B.BB.E5.BD.B1.E7.89.88.md "wikilink")（預計2018年8月24日公映，東寶）－飾：**最上毅**

<!-- end list -->

  - [假面飯店](../Page/假面飯店#.E9.9B.BB.E5.BD.B1.md "wikilink")（預計2019年公映）－飾：**新田浩介**

### 舞台劇

  - 導盲犬（）（1989年，日生劇場）

<!-- end list -->

  - 勇者鬥惡龍（）（1992年，京都南座）

<!-- end list -->

  - 花影之花～大石內蔵助之妻（）（1992年，日生劇場）

<!-- end list -->

  - ANOTHER～沉默之島篇·少年之島篇（ ）（1993年，天王洲Art Sphere、京都南座）

<!-- end list -->

  - 時尚男士～Modern Boys～（）（1994年）

### 配音

  - [花樣男子 CD書](../Page/花樣男子#CD_BOOK.md "wikilink")－配：花澤類
      - 花樣男子（1993年7月）
      - 花樣男子2（1993年12月）
      - 花樣男子3（1994年7月）

<!-- end list -->

  - [哈爾的移動城堡](../Page/哈爾的移動城堡.md "wikilink")（2004年11月20日公映）－配：霍爾

<!-- end list -->

  - [伴雨行](../Page/幻雨追緝.md "wikilink")（2009年6月6日公映）－師濤（日語配音）\[27\]

<!-- end list -->

  - [超時空甩尾](../Page/超時空甩尾.md "wikilink")（2010年10月9日公映）－配：JP\[28\]

<!-- end list -->

  - [海螺小姐](../Page/海螺小姐.md "wikilink")（2014年7月27日，）－配：拓哉

<!-- end list -->

  - [蠟筆小新](../Page/蜡笔小新#.E9.9B.BB.E8.A6.96.E5.8B.95.E7.95.AB.md "wikilink")（2015年5月29日、6月5日，[朝日電視台](../Page/朝日電視台.md "wikilink")）－配：野原家家族\[29\]

### 綜藝節目

  - COMING
    OUT\!（）（1994年10月29日－1995年3月25日，[TBS電視台](../Page/TBS電視台.md "wikilink")）

<!-- end list -->

  - 伊藤公式（）（1995年4月29日－9月30日，[日本電視台](../Page/日本電視台.md "wikilink")）－*助理主持*

<!-- end list -->

  - （1999年7月31日－1999年9月4日，[富士電視台](../Page/富士電視台.md "wikilink")）－*助理主持*

<!-- end list -->

  - （2000年10月13日－2001年3月23日，富士電視台）

<!-- end list -->

  - （2001年10月13日－2017年9月23日，[朝日電視台](../Page/朝日電視台.md "wikilink")）－*助理主持*

<!-- end list -->

  - (2003年1月3日－迄今，富士電視台）

### 單獨節目

  - [24小時電視28
    「愛心救地球」～直播～](../Page/24小時電視_「愛心救地球」.md "wikilink")（2005年8月27日－8月28日，[日本电视台](../Page/日本电视台.md "wikilink")）

### 特別節目

  - [野口勇雕刻地球的男人](../Page/野口勇.md "wikilink")（1996年2月12日，[札幌電視台](../Page/札幌電視台.md "wikilink")）－導讀

<!-- end list -->

  - 愛與勇氣的檔案 兒童醫院24小時（1996年10月12日，[富士電視台](../Page/富士電視台.md "wikilink")）

<!-- end list -->

  - （1999年9月18日，[日本電視台](../Page/日本電視台.md "wikilink")）－主持及表演

<!-- end list -->

  - [NHK特集足球地球的熱情](../Page/NHK特集.md "wikilink") 第1集
    齊達內之路（2002年5月3日，[NHK綜合頻道](../Page/NHK綜合頻道.md "wikilink")）－解說

### 廣播節目

  - （1994年10月1日－2016年12月30日，[TOKYO FM](../Page/TOKYO_FM.md "wikilink")）

<!-- end list -->

  - （1995年1月－迄今，TOKYO FM）

### 電玩遊戲

  - [審判之眼：死神的遺言](../Page/審判之眼：死神的遺言.md "wikilink")（2018年12月，[SEGA](../Page/SEGA.md "wikilink")，PlayStation
    4）主角：八神隆之\[30\]

### 音樂錄影帶

  - [ALEXANDROS](../Page/ALEXANDROS.md "wikilink")
    -「アルペジオ」(PlayStation®4 用ソフト「[JUDGE
    EYES:死神の遺言](../Page/JUDGE_EYES:死神の遺言.md "wikilink")」主題歌)
    (客串）

### 廣告

  - [大塚製藥](../Page/大塚製藥.md "wikilink")“”（1993年－1997年）

<!-- end list -->

  - [豐田汽車](../Page/豐田汽車.md "wikilink")“[卡羅拉](../Page/丰田卡罗拉.md "wikilink")”（1994年－2017年）

<!-- end list -->

  - [任天堂](../Page/任天堂.md "wikilink")（1994年－1995年）

<!-- end list -->

  - [樂天](../Page/樂天_\(Lotte\).md "wikilink")（1995年－1997年）

<!-- end list -->

  - （1996年）

<!-- end list -->

  - （1996年）

<!-- end list -->

  - [佳麗寶](../Page/花王.md "wikilink")（1996年）

<!-- end list -->

  - [JCB](../Page/JCB.md "wikilink")（1997年－）

<!-- end list -->

  - [NTT東日本](../Page/日本電信電話.md "wikilink")（1997年－）

<!-- end list -->

  - （1997年－）

<!-- end list -->

  - [麒麟啤酒](../Page/麒麟啤酒.md "wikilink")（1997年）

<!-- end list -->

  - [日本中央賽馬會](../Page/日本中央競馬會.md "wikilink")（1998年－1999年）

<!-- end list -->

  - [三得利](../Page/三得利.md "wikilink")（1998年－1999年）

<!-- end list -->

  - [森永糖果](../Page/森永製菓.md "wikilink")（1999年－2005年）

<!-- end list -->

  - [富士通](../Page/富士通.md "wikilink")（2000年－2010年）

<!-- end list -->

  - [李維斯](../Page/Levi's.md "wikilink")（2000年－2008年）

<!-- end list -->

  - （2001年－）

<!-- end list -->

  - （2004年）

<!-- end list -->

  - [產業經濟新聞社](../Page/產業經濟新聞社.md "wikilink")（2006年－）

<!-- end list -->

  - [尼康](../Page/尼康.md "wikilink")（2006年－2014年）

<!-- end list -->

  - [日本可口可樂](../Page/可口可乐公司.md "wikilink")（2006年－2007年）

<!-- end list -->

  - （2006年－2011年）

<!-- end list -->

  - 大塚製藥 [寶礦力水特](../Page/寶礦力水特.md "wikilink")（2007年－）

<!-- end list -->

  - [明治糖果](../Page/明治製菓.md "wikilink")（2007年－2010年）

<!-- end list -->

  - [WOWOW](../Page/WOWOW.md "wikilink")（2007年）

<!-- end list -->

  - [西科姆](../Page/SECOM.md "wikilink")（2008年）

<!-- end list -->

  - [日清食品](../Page/日清食品.md "wikilink")（2008年－2010年）

<!-- end list -->

  - （2009年）

<!-- end list -->

  - （2009年－）

<!-- end list -->

  - （2010年）

<!-- end list -->

  - [好侍食品](../Page/好侍食品.md "wikilink")（2010年－2014年）

<!-- end list -->

  - 三得利 (2012年－2013年)

<!-- end list -->

  - 蜜丝芭莉 （2012年－2017年）

<!-- end list -->

  - （2012年－2014年）

<!-- end list -->

  - [台灣觀光協會](../Page/台灣觀光協會.md "wikilink")（2015年－2016年）\[31\]

<!-- end list -->

  - “男裝”（2015年－）

<!-- end list -->

  - （2016年1月18日－2016年12月31日）\[32\]

## 歌詞創作

  - 《Waiting Waiting
    Waiting》——收錄于SMAP的[錄音室專輯](../Page/錄音室專輯.md "wikilink")《[SMAP
    002](../Page/SMAP_002.md "wikilink")》

<!-- end list -->

  - 《HA》——收錄于SMAP的[迷你專輯](../Page/迷你專輯.md "wikilink")《》

<!-- end list -->

  - 《番茄汁啊》（）——[明石家秋刀魚與](../Page/明石家秋刀魚.md "wikilink")聯合作曲。於2014年1月1日在《SANTAKU》公開，成為該節目中的第一首原創歌曲。

## 音樂製作

  - [竹內瑪莉亞](../Page/竹內瑪莉亞.md "wikilink")《》——竹內瑪麗亞於1995年發行的[單曲](../Page/單曲.md "wikilink")。木村拓哉參與了部分歌詞的創作，比如歌詞中有“木拓”（）這樣的短語。同時，木村還負責歌曲開始部分“”的演唱，以及歌曲中的和聲部分。\[33\]\[34\]

## 書籍出版

  - 《開放區》（2003年4月24日，[集英社](../Page/集英社.md "wikilink")）ISBN
    978-4-08-780377-8

<!-- end list -->

  - 《開放區2》（2011年9月30日，集英社）ISBN 978-4-08-780613-7

### 寫真集

  - 《木村拓哉》（1996年，川島國際，攝影：）ISBN 978-4-533-02643-0

<!-- end list -->

  - 《》（2006年11月11日，[Magazine
    House](../Page/Magazine_House.md "wikilink")）ISBN 978-4-8387-1726-2

<!-- end list -->

  - 《》（2011年9月30日，集英社）ISBN 978-4-08-780614-4

### 雜誌連載

  - 《》“看看木村的開放區！（）”（1995年6月号－2012年）

## 音像作品

  - DVD《一分TAKUYA KIMURA》（2006年，[NBC環球](../Page/NBC環球.md "wikilink")）EAN
    4988102307332

## 參考資料

### 腳註

### 出典

## 外部連接

  - [傑尼斯事務所\>木村拓哉公式站](http://www.johnnys-net.jp/page?id=artistTop&artist=35)

  -
  -
  -
  -
  -
  -
[K](../Category/SMAP的成员.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:傑尼斯事務所所屬藝人](../Category/傑尼斯事務所所屬藝人.md "wikilink")
[Category:日本男性偶像](../Category/日本男性偶像.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")
[Category:日劇學院賞最佳男主角得主](../Category/日劇學院賞最佳男主角得主.md "wikilink")
[Category:TVnavi日劇年度大賞最佳男主角獎](../Category/TVnavi日劇年度大賞最佳男主角獎.md "wikilink")
[Category:日本廣播主持人](../Category/日本廣播主持人.md "wikilink")
[Category:日刊體育電影大獎最佳男主角得主](../Category/日刊體育電影大獎最佳男主角得主.md "wikilink")
[Category:石原裕次郎新人獎得主](../Category/石原裕次郎新人獎得主.md "wikilink")
[Category:勝利娛樂旗下藝人](../Category/勝利娛樂旗下藝人.md "wikilink")

1.

2.  根據2017年[娛樂媒體的報道](http://ent.ifeng.com/a/20170821/42969029_0.shtml)中所提及的年紀，推斷出兩個女兒的出生年份。

3.
4.  「[木村拓哉のプロフィール](http://www.oricon.co.jp/prof/251846/profile/)」 ORICON
    STYLE 2014年11月16日閲覧。

5.  「[木村拓哉-映画ならKINENOTE](http://www.kinenote.com/main/public/cinema/person.aspx?person_id=127310)」
    KINENOTE 2016年5月20日閲覧。

6.

7.
8.

9.

10.

11.
12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25. 「[三池監督
    キクタクの起用理由を語る](http://www.daily.co.jp/gossip/2015/10/05/0008457278.shtml)」デイリースポーツ
    2015年10月5日閲覧。

26.

27. 該角色由木村拓哉飾演，但原片為英語發音。日語版則依舊由木村拓哉為自己所飾演的角色配音。

28.

29.

30.

31.

32.

33. [「木村拓哉、山下達郎から「愛してるよと言って」と頼まれ…」](http://www.cyzowoman.com/2008/11/post_113.html)cyzo
    woman（2008年11月6日）2017年4月23日閲覧。

34. 竹内まりやベスト・アルバム『[expressions](../Page/expressions.md "wikilink")』ライナーノーツより