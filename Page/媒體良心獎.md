**傳媒良心獎**（[英文](../Page/英文.md "wikilink")：****）是由[美國新聞工作者與作家協會](../Page/美國新聞工作者與作家協會.md "wikilink")（American
Society of Journalists and
Authors，ASJA）所頒發，受獎的條件是該協會所認定值得為他們獨特的貢獻所肯定的[記者](../Page/記者.md "wikilink")。

## 参考文献

[Category:美国奖项](../Category/美国奖项.md "wikilink")
[Category:美國媒體](../Category/美國媒體.md "wikilink")
[Category:新聞獎項](../Category/新聞獎項.md "wikilink")
[Category:1975年美国建立](../Category/1975年美国建立.md "wikilink")
[Category:1975年建立的奖项](../Category/1975年建立的奖项.md "wikilink")