<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><big><strong>马里博尔<br />
Maribor</strong></big></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Karte_Maribor_si.png" title="fig:Karte_Maribor_si.png">Karte_Maribor_si.png</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>基本资料</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>面积</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>人口</strong><br />
- 总人口<br />
- <a href="../Page/人口密度.md" title="wikilink">人口密度</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>坐标</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>网站</strong></p></td>
</tr>
</tbody>
</table>

**马里博尔**（[斯洛文尼亚语](../Page/斯洛文尼亚语.md "wikilink")：**Maribor**，，**德拉河畔的马尔堡**）位于[斯洛文尼亚](../Page/斯洛文尼亚.md "wikilink")[德拉瓦河畔](../Page/德拉瓦河_\(多瑙河\).md "wikilink")，是该国第二大城市。

该市属于南施泰尔马克，原是奥地利哈布斯堡王室领地。该区与奥地利接壤
一战之前，该城市中80%人口为德意志人，20%为斯洛文尼亚人，整个城市由德意志人控制；德意志人主要聚居在该城中和附近的小城镇内，斯拉夫人聚居在周边的乡村里。一战之后未经全民公投随着南施泰尔马克被新成立的南斯拉夫王国占领；期间还爆发了斯洛文尼亚民兵对德意志居民和平示威的枪杀，称为“马尔堡的血腥周日”。之后，该地德意志人被被强制进行斯拉夫化，德意志人的学校、俱乐部、各种组织被南斯拉夫当局强制关闭；大批德意志人搬离该市撤到新的奥地利境内。截止到1930年代，该市德意志人尚占25%。1941年德国入侵南斯拉夫之后，南[施泰尔马克](../Page/施泰尔马克.md "wikilink")（包括马尔堡）、[克雷恩](../Page/克雷恩.md "wikilink")，南[克恩滕三个地区重新回归德国](../Page/克恩滕.md "wikilink")；期间[希特勒在马尔堡市政厅发表演说](../Page/希特勒.md "wikilink")，宣布三个地区回归德国，受到当地居民热烈欢迎。二战之后，该地重新划归南斯拉夫，大部分德意志人遭到驱逐；未来得及逃走的老弱妇孺和儿童则被送入劳改营。

马尔堡在战后发展为斯洛文尼亚北部重要的工业和文化城市，2012年还被定为[欧洲文化之都](../Page/欧洲文化之都.md "wikilink")。

## 友好城市

  - [格拉茨](../Page/格拉茨.md "wikilink")

## 外部链接

  - [马里博尔旅游网站](http://www.maribor-tourism.si/)
  - [马里博尔官方网站](http://www.maribor.si/)
  - [马里博尔机场](http://www.maribor-airport.si)

[Category:斯洛文尼亚城市](../Category/斯洛文尼亚城市.md "wikilink")