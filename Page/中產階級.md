[House.jpg](https://zh.wikipedia.org/wiki/File:House.jpg "fig:House.jpg")
**中產階級**（）是资产阶级中的一个阶层。在現代社會中，指擁有一定程度的經濟獨立，例如有穩定、較高[薪酬的工作](../Page/薪酬.md "wikilink")，在现代社会之中对社会的发展和稳定起很大的作用，也可以被稱作小康階級，但有時貧富差距大時可能位於社會中間收入卻不高。此詞常用於[專業人士](../Page/專業人士.md "wikilink")、[学者](../Page/学者.md "wikilink")、[知識分子](../Page/知識分子.md "wikilink")，或[公營機構](../Page/公營機構.md "wikilink")、[政府部門](../Page/政府部門.md "wikilink")、大型[企業的中級](../Page/企業.md "wikilink")[管理層](../Page/管理層.md "wikilink")，或[中小型企業](../Page/中小型企業.md "wikilink")[-{zh-hans:老板;
zh-hant:老闆;
zh-hk:東主;}-](../Page/老板.md "wikilink")，[中學](../Page/中學.md "wikilink")、[小學](../Page/小學.md "wikilink")、[-{zh-hans:幼儿园;
zh-hant:幼稚園;
zh-sg:幼稚园;}-](../Page/幼稚園.md "wikilink")[校長](../Page/校長.md "wikilink")、[教師](../Page/教師.md "wikilink")、[社工和](../Page/社工.md "wikilink")[護士等](../Page/護士.md "wikilink")。

[马克思主义将这一](../Page/马克思主义.md "wikilink")[阶层称为](../Page/阶层.md "wikilink")“[小资产阶级](../Page/小资产阶级.md "wikilink")”。中產階級有時——特別在[香港](../Page/香港.md "wikilink")——也被稱作「**夾心階層**」，意即他們夾在社會中層，未及[上流社會人士般擁有巨大財富](../Page/上流社會.md "wikilink")，亦不像[低收入人士般能領取](../Page/弱勢群體.md "wikilink")[社會福利保障](../Page/社會福利.md "wikilink")。

中產階級的界定因素隨各國有所分別，但基本上多以收入及擁有資產作界定，其他因素則包括[教育](../Page/教育.md "wikilink")、專業地位、擁有住屋或[文化等](../Page/文化.md "wikilink")。例如擁有[大學](../Page/大學.md "wikilink")[学位的人](../Page/学位.md "wikilink")，多數可躋身中產階級的行列。而擁有自置[物業及](../Page/物業.md "wikilink")[私家車](../Page/轿车.md "wikilink")，亦是中產階級的象徵。

## 定义

[缩略图](https://zh.wikipedia.org/wiki/File:Indian_middle_class.JPG "fig:缩略图")中產階級的定義法，僅供參考，並未受廣泛認可。\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:MOS_TaiShuiHangVillage_03.jpg "fig:缩略图")
对中产阶级还没有一个能够被广泛认可的定义，最常用的方法是测定其收入增长。

[世界银行将经过](../Page/世界银行.md "wikilink")[购买力平价调整后的日收入超过](../Page/购买力平价.md "wikilink")2[美元的人定义为中产阶级](../Page/美元.md "wikilink")。根据这种划分方法，全世界有超过40亿的人达到中产阶级水准。狭义的中产阶级定义则是收入接近或超过[发达国家中等收入者的人](../Page/发达国家.md "wikilink")，其收入大约为人均31000美元或每天85美元。按照这种定义，全世界有约12%的人属于中产阶级。\[1\]

## 現況

### 中國大陸

在[中国大陸](../Page/中国大陸.md "wikilink")，2005年[国家统计局根據人均](../Page/国家统计局.md "wikilink")[GDP和](../Page/国内生产总值.md "wikilink")[购买力平价給出中產階級的家庭年收入在](../Page/购买力平价.md "wikilink")6萬至50萬[人民幣之間的結論](../Page/人民幣.md "wikilink")。\[2\]由于GDP和购买力平价这两个数据都只反映国家经济总量而不反映分布[收入分配](../Page/收入分配.md "wikilink")，这个中产阶级的标准很快遭到了包括中国大陆官方媒体《[北京晨报](../Page/北京晨报.md "wikilink")》在内的质疑\[3\]。
2013年3月28日，《[福布斯](../Page/福布斯.md "wikilink")》中文版联合宜信财富发布《中国大众富裕阶层财富白皮书》称，中国大陸的大众富裕阶层近年来迅速壮大，2012年已超过千万人，达到1026万人，人均可投资资产达133万元[人民币](../Page/人民币.md "wikilink")。参考国际通行标准，白皮书所定义的大众富裕阶层是指个人可投资资产在10万美元至100万美元之间的中国大陸中产群体，即所谓的“高级白领”。\[4\]此人群中，从事金融和贸易的占19.2%，从事制造业的占据了11.5%的份额，从事房地产行业的有9.0%。

### 香港

在[香港](../Page/香港.md "wikilink")，根據[香港中文大学社会学系](../Page/香港中文大学.md "wikilink")[教授](../Page/教授.md "wikilink")[呂大樂的界定](../Page/呂大樂.md "wikilink")，收入并不能够成为被强调的起点。吕大乐说“这（收入）不是一个太有用的分类”。“就香港而言，[月薪](../Page/月薪.md "wikilink")2万到5万[港币完全可以排到中等收入](../Page/港币.md "wikilink")[群体了](../Page/群体.md "wikilink")，但是，这并不是等同于你就成了中产阶级，还要看你住的[房子的房价](../Page/房子.md "wikilink")，你的[消费方式](../Page/消费.md "wikilink")，是否住在体面的[樓盤](../Page/樓盤.md "wikilink")，是否有定期的[渡假等等](../Page/旅行.md "wikilink")”。吕大乐界定中的香港中产阶级更重要的是按[职业](../Page/职业.md "wikilink")[群体划分](../Page/群体.md "wikilink")，同时强调“他们是成功透过教育渠道和凭着学历文凭而晋身。”按照这样标准，香港的中产阶级最多占到人口比例的20%到30%。

根据[香港](../Page/香港.md "wikilink")[政府統計處的定义](../Page/政府統計處.md "wikilink")，每个月收入1万至4万港元的住户列为中产住户，这类住户占了整体住户数目的55%。\[5\]

2013年2月，月薪37萬港元的時任香港[財政司司長](../Page/財政司司長.md "wikilink")[曾俊華就年度](../Page/曾俊華.md "wikilink")[財政預算案發言時](../Page/財政預算案.md "wikilink")，指自己也屬於中產。\[6\]又指中產和生活習慣等有關，例如喝[咖啡和看](../Page/咖啡.md "wikilink")[法国电影的人也算中產](../Page/法国电影.md "wikilink")。\[7\]

但理論上喝咖啡與看法國電影只是個人喜好和興趣，與資產階級並無關係。

### 美國

就收入而言，[美国的中产家庭年收入在](../Page/美国.md "wikilink")3至20萬[美元的即可認爲屬于中產階級](../Page/美元.md "wikilink")，據估計大約80%的[美国人屬于中產階級](../Page/美国人.md "wikilink")。

## 参考文献

## 參見

  - [小资产阶级](../Page/小资产阶级.md "wikilink")
  - [四仔主義](../Page/四仔主義.md "wikilink")（[香港用語](../Page/香港.md "wikilink")）
  - [M型社會](../Page/M型社會.md "wikilink")
  - [新貧階級](../Page/新貧階級.md "wikilink")

## 外部链接

  - [美国中产阶级全面情况报告](http://lady.163.com/05/1008/10/1VHKMJE900261IFS.html)
  - [全球中产阶级报告](https://web.archive.org/web/20111104005515/http://book.sina.com.cn/nzt/fin/quanqiuzhongchanjieji/index.shtml)
  - [中产阶级的意义](http://cn.wsj.com/gb/middleclasslife.asp) -
    [华尔街日报中文网](../Page/华尔街日报.md "wikilink")
  - [我国中产阶级已超千万 60后70后占主体](http://finance.ce.cn/rolling/201303/29/t20130329_17085975.shtml)
    中国经济网 2014-05-30

[Category:社会阶层](../Category/社会阶层.md "wikilink")

1.
2.
3.
4.
5.  <https://web.archive.org/web/20070224120232/http://hk.news.yahoo.com/070222/12/226ho.html>
6.
7.