**新村號**
（、）是[韓國高速鐵道通車以前](../Page/韓國高速鐵道.md "wikilink")，[韓國最高級的鐵路客車](../Page/韓國.md "wikilink")。

## 概况

新村号的一般由5\~12節車廂組成。與次一級的[無窮花號比較](../Page/無窮花號.md "wikilink")，新村號比較寬敞，而且沒有站位。顏色方面，新村號以綠、白、黃為主。另一方面，新村號集中於幹線上行駛，例如行駛首爾—釜山區間會採用[京釜線而非](../Page/京釜線.md "wikilink")[中央線](../Page/中央線.md "wikilink")
（2006年11月起）。

### 名稱的由來

其列車的名稱來自於1970年代[朴正熙推行的](../Page/朴正熙.md "wikilink")[新村運動](../Page/新村運動.md "wikilink")。

## 历史

### 初期运用

1969年2月8日，以[超特急列车](../Page/超特急列车.md "wikilink")**观光号**的名义，运行于[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")，最高时速可达到120km/h\[1\]，仅需时间4小时50分。在1974年8月15日改为现在的名称。之后开始固定运行于[首尔](../Page/首尔站.md "wikilink")-[大田](../Page/大田站_\(韩国\).md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")。在1970年代后期，还开通了[首尔](../Page/首尔站.md "wikilink")-[庆州](../Page/庆州站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[马山的运行区间](../Page/马山站.md "wikilink")。

### 中期运用

1985年11月，新村号行驶的路线全部替换为水泥混凝土轨枕，使得最高运行速度提升至120km/h，使得运行时间缩短至4小时10分\[2\]。之后，服务范围扩展至[清凉里](../Page/清凉里站.md "wikilink")-[安东及](../Page/安东站_\(庆尚南道\).md "wikilink")[首尔](../Page/首尔站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")。
进入1990年之后，服务范围扩展至[首尔](../Page/首尔站.md "wikilink")-[浦项及](../Page/浦项站.md "wikilink")[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")/[清凉里](../Page/清凉里站.md "wikilink")-[东海](../Page/东海站_\(江原道\).md "wikilink")。在1993年，运行于[首尔](../Page/首尔站.md "wikilink")-[釜山暂时停止服务](../Page/釜山站.md "wikilink")。不过在1994年3月31日之后，再次恢复[首尔](../Page/首尔站.md "wikilink")-[釜山的服务](../Page/釜山站.md "wikilink")，不过运行时间变为4小时16分-4小时21分。在1994年7月，开始了[首尔](../Page/首尔站.md "wikilink")-[海云台的临时服务](../Page/海云台站_\(韩国铁道公社\).md "wikilink")。1999年6月1日，在[大田站](../Page/大田站_\(韩国\).md "wikilink")、[东大邱站的停靠基础上增加停靠](../Page/东大邱站.md "wikilink")[新滩津站](../Page/新滩津站.md "wikilink")、[永同站](../Page/永同站.md "wikilink")、[倭馆站](../Page/倭馆站.md "wikilink")、[庆山站](../Page/庆山站.md "wikilink")，使得行车时间延长7-8分钟\[3\]。

### 后期运用

2004年4月1日，高速鐵路[KTX开通](../Page/KTX.md "wikilink")，导致运行范围开始进一步调整。随着[中央高速公路和](../Page/中央高速公路_\(韩国\).md "wikilink")[岭东高速公路的车流持续增长](../Page/岭东高速公路.md "wikilink")，导致2006年11月1日开始停止[清凉里](../Page/清凉里站.md "wikilink")-[江陵的服务](../Page/江陵站.md "wikilink")。
由于2006年11月1日[KTX开始进一步扩大服务区间](../Page/KTX.md "wikilink")，使得[大邱](../Page/大邱站.md "wikilink")-[镇海的班次减少](../Page/镇海站_\(庆尚南道\).md "wikilink")，最终在2012年11月1日，将运行范围缩短至[马山](../Page/马山站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")。
随着[中部高速公路的车流增长](../Page/中部高速公路.md "wikilink")，导致在2007年6月1日[首尔](../Page/首尔站.md "wikilink")-[晋州的服务停止](../Page/晋州站_\(庆尚南道\).md "wikilink")，缩减为[马山](../Page/马山站.md "wikilink")-[晋州区间](../Page/晋州站_\(庆尚南道\).md "wikilink")。在2012年12月4日，因运行线路开始复线化，使得运营暂时停止。
在[京义线成为](../Page/京义线.md "wikilink")[首都圈电铁的路线之前](../Page/首都圈电铁.md "wikilink")，本列车曾在2007年6月1日至2009年6月30日于此线路进行过服务。
2010年11月1日，因[京釜高速线的开通](../Page/京釜高速线.md "wikilink")，使得在[东海南部线的运行区间由](../Page/东海南部线.md "wikilink")[首尔](../Page/首尔站.md "wikilink")-[釜田缩减为](../Page/釜田站_\(韩国铁道公社\).md "wikilink")[首尔](../Page/首尔站.md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")。在2012年11月1日，[东海南部线的运行车种正式换为了](../Page/东海南部线.md "wikilink")[无穷花号](../Page/无穷花号.md "wikilink")。
2010年12月15日，新村号在[京釜线的运行区域变为](../Page/京釜线.md "wikilink")[首尔](../Page/首尔站.md "wikilink")-[马山和](../Page/马山站.md "wikilink")[首尔](../Page/首尔站.md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")，同时停止了[清凉里](../Page/清凉里站.md "wikilink")-[安东的服务](../Page/安东站_\(庆尚南道\).md "wikilink")。在2011年8月16日，由[釜山开往](../Page/釜山.md "wikilink")[首尔的列车终点变为了](../Page/首尔.md "wikilink")[龙山站](../Page/龙山站_\(首尔\).md "wikilink")。在2011年11月1日，取消了在[倭馆站和](../Page/倭馆站.md "wikilink")[清道站的停靠](../Page/清道站.md "wikilink")。由于[KTX开始在](../Page/KTX.md "wikilink")2011年11月5日于[全罗线开始服务](../Page/全罗线.md "wikilink")，因此在2012年3月20日，[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO改为用](../Page/丽水EXPO站.md "wikilink")[电力机车牵引](../Page/电力机车.md "wikilink")\[4\]。

目前，此种类列车在2014年12月31日计划停止使用，全部以**ITX-新村**電聯車代之\[5\]\[6\]。

## 使用车型

新村号车型大致分为2个时期：第1代为客车，自1969年-1982年制造；第2代多为[柴油动车组](../Page/柴油动车组.md "wikilink")，自1986年-1999年制造。

### 客车

1969年2月8日，以**观光号**的名义开始运营，列车在初期由[日本](../Page/日本.md "wikilink")[日立制作所和](../Page/日立制作所.md "wikilink")[日本车辆制造负责制造](../Page/日本车辆制造.md "wikilink")。在1975年-1982年，开始由[大宇重工业负责制造](../Page/大宇重工业.md "wikilink")。列车的座位后端下方拥有踏板，洗手间安装有集便器。列车拥有餐车和观光车厢，但已于2007年12月31日全部停用。

### 柴油动车组

自1980年开始，新村号开始投入使用[9201系柴油动车组](../Page/韩国铁道9201系柴油动车组.md "wikilink")，运行区间为[首尔](../Page/首尔站.md "wikilink")-[南原](../Page/南原站.md "wikilink")。为迎接[1986年亚洲运动会及](../Page/1986年亚洲运动会.md "wikilink")[1988年夏季奥林匹克运动会](../Page/1988年夏季奥林匹克运动会.md "wikilink")，决定停用[9201系柴油动车组](../Page/韩国铁道9201系柴油动车组.md "wikilink")，改为编入至[无穷花号中](../Page/无穷花号.md "wikilink")。
在1986年7月12日改为使用由[现代Rotem](../Page/现代Rotem.md "wikilink")、[大宇重工业](../Page/大宇重工业.md "wikilink")、[韩进重工业和](../Page/韩进重工业.md "wikilink")[大韩造船公社制造的](../Page/大韩造船公社.md "wikilink")[101系柴油动车组](../Page/韩国铁道101系柴油动车组.md "wikilink")，此列车为流线型车体\[7\]。运行区间为[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")，并在1987年-1988年开始批量生产，并开始在[中央线和](../Page/中央线_\(韩国\).md "wikilink")[全罗线服役](../Page/全罗线.md "wikilink")，替代了[无穷花号的部分使用区间](../Page/无穷花号.md "wikilink")。较第1代客车相比，每节车厢内部座位为64个，每排4个，此外还拥有咖啡厅和舞厅。目前本车型在2013年1月5日全部停用。

## 运行区间

由于列车的可运行范围不断的变化，所以行走区间也会发生相应的更改。

### 当前运行区间（自2012年12月5日开始）

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")、[釜山](../Page/釜山站.md "wikilink")→[龙山](../Page/龙山站_\(首尔\).md "wikilink")\[8\]
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")
  - [海列车](../Page/海列车.md "wikilink")：[三陟](../Page/三陟站.md "wikilink")-[江陵](../Page/江陵站.md "wikilink")\[9\]
  - 其他旅游专列：[嗵嗵嗵音乐茶座列车](../Page/嗵嗵嗵音乐茶座列车.md "wikilink")、[酒吧影院列车](../Page/酒吧影院列车.md "wikilink")、[海浪号](../Page/海浪号.md "wikilink")

### 过去运行区间

部分时期运营区间记录：

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[长项](../Page/长项站.md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")、[岭东线](../Page/岭东线.md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[江陵](../Page/江陵站.md "wikilink")
  - 非定期运行：[京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")/[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")/[浦项](../Page/浦项站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")/[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")/[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")\[10\]
  - [京釜线](../Page/京釜线.md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")、[东大邱](../Page/东大邱站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")、[东大邱](../Page/东大邱站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")、[大田](../Page/大田站_\(韩国\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")、[岭东线](../Page/岭东线.md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[江陵](../Page/江陵站.md "wikilink")
  - 非定期运行：[京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")/[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")/[浦项](../Page/浦项站.md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[长项](../Page/长项站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")、[岭东线](../Page/岭东线.md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[江陵](../Page/江陵站.md "wikilink")
  - 非定期运行：[京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")/[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")/[浦项](../Page/浦项站.md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京义线](../Page/京义线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[都罗山](../Page/都罗山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[长项](../Page/长项站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")、[岭东线](../Page/岭东线.md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[江陵](../Page/江陵站.md "wikilink")
  - 非定期运行：[京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")/[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")/[浦项](../Page/浦项站.md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京义线](../Page/京义线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[都罗山](../Page/都罗山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[晋州](../Page/晋州站_\(庆尚南道\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[长项](../Page/长项站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")
  - 非定期运行：[京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")/[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[太和江](../Page/太和江站.md "wikilink")/[浦项](../Page/浦项站.md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京义线](../Page/京义线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[都罗山](../Page/都罗山站.md "wikilink")、[临津江](../Page/临津江站.md "wikilink")-[都罗山](../Page/都罗山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[釜田](../Page/釜田站_\(韩国铁道公社\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）、[舒川](../Page/舒川站.md "wikilink")-[益山](../Page/益山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")
  - 非定期运行：[京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")/[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")/[光州](../Page/光州站.md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[釜田](../Page/釜田站_\(韩国铁道公社\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")
  - 非定期运行：[京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")/[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")/[光州](../Page/光州站.md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[釜田](../Page/釜田站_\(韩国铁道公社\).md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[海云台](../Page/海云台站_\(韩国铁道公社\).md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[龟浦](../Page/龟浦站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[东大邱](../Page/东大邱站.md "wikilink")-[釜田](../Page/釜田站_\(韩国铁道公社\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[东大邱](../Page/东大邱站.md "wikilink")-[釜田](../Page/釜田站_\(韩国铁道公社\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")、[釜山](../Page/釜山站.md "wikilink")→[龙山](../Page/龙山站_\(首尔\).md "wikilink")\[11\]
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[东大邱](../Page/东大邱站.md "wikilink")-[釜田](../Page/釜田站_\(韩国铁道公社\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[大邱](../Page/大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")、[釜山](../Page/釜山站.md "wikilink")→[龙山](../Page/龙山站_\(首尔\).md "wikilink")\[12\]
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[东大邱](../Page/东大邱站.md "wikilink")-[釜田](../Page/釜田站_\(韩国铁道公社\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[东大邱](../Page/东大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")\[13\]
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")
  - [海列车](../Page/海列车.md "wikilink")：[三陟](../Page/三陟站.md "wikilink")-[江陵](../Page/江陵站.md "wikilink")

<!-- end list -->

  - [京釜线](../Page/京釜线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[釜山](../Page/釜山站.md "wikilink")、[首尔](../Page/首尔站.md "wikilink")-[东大邱](../Page/东大邱站.md "wikilink")、[釜山](../Page/釜山站.md "wikilink")→[龙山](../Page/龙山站_\(首尔\).md "wikilink")\[14\]
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[马山](../Page/马山站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[大邱线](../Page/大邱线.md "wikilink")、[中央线](../Page/中央线_\(韩国\).md "wikilink")、[东海南部线](../Page/东海南部线.md "wikilink")：[首尔](../Page/首尔站.md "wikilink")-[浦项](../Page/浦项站.md "wikilink")、[东大邱](../Page/东大邱站.md "wikilink")-[釜田](../Page/釜田站_\(韩国铁道公社\).md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[长项线](../Page/长项线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[益山](../Page/益山站.md "wikilink")（[101系经停](../Page/韩国铁道101系柴油动车组.md "wikilink")[长项](../Page/长项站.md "wikilink")、[群山](../Page/群山站.md "wikilink")）
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[光州线](../Page/光州线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[木浦](../Page/木浦站.md "wikilink")、[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[光州](../Page/光州站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[湖南线](../Page/湖南线.md "wikilink")、[全罗线](../Page/全罗线.md "wikilink")：[龙山](../Page/龙山站_\(首尔\).md "wikilink")-[丽水EXPO](../Page/丽水EXPO站.md "wikilink")
  - [京釜线](../Page/京釜线.md "wikilink")、[庆全线](../Page/庆全线.md "wikilink")、[镇海线](../Page/镇海线.md "wikilink")：[东大邱](../Page/东大邱站.md "wikilink")-[镇海](../Page/镇海站_\(庆尚南道\).md "wikilink")\[15\]
  - [中央线](../Page/中央线_\(韩国\).md "wikilink")：[清凉里](../Page/清凉里站.md "wikilink")-[安东](../Page/安东站_\(庆尚北道\).md "wikilink")

## 图片

Image:Saemaeul Exp passenger car of Korean National Railroad.jpg|新村号钢制客车
Image:88 peace train of Korean National
Railroad.jpg|1988年的[111系](../Page/韩国铁道101系柴油动车组.md "wikilink")
Image:Korail saemaeul train old CI color.jpg|新村号柴油动车组旧涂装
Image:PP-DHC.jpg|新村号柴油动车组新涂装 Image:Saemaeul Exp. Normal Seat.jpg|列车内部座席1
Image:Saemaeul-seat-pitch-interval.jpg|列车内部座席2 Image:DHC Saemaeul Bound
for Yongsan.jpg|新村号柴油动车组在[益山站](../Page/益山站.md "wikilink")

## 静态保存车辆

### 第1代

  - [蟾津江汽車鎮](../Page/蟾津江汽車鎮.md "wikilink")：3辆\[16\]
  - [九切里站](../Page/九切里站.md "wikilink")：1辆\[17\]

### 第2代

  - [韩国国立果川科学馆](../Page/韩国国立果川科学馆.md "wikilink")：1辆\[18\]
  - 华川列车存放处：10辆\[19\]
  - [清道站](../Page/清道站.md "wikilink")：1辆\[20\]
  - [蟾津江汽車鎮](../Page/蟾津江汽車鎮.md "wikilink")：[101系](../Page/韩国铁道101系柴油动车组.md "wikilink")12辆\[21\]
  - [店村站](../Page/店村站.md "wikilink")：2辆
  - [直指寺站](../Page/直指寺站.md "wikilink")：1辆

## 车辆编号

<table>
<thead>
<tr class="header">
<th><p>编号</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>133~262</p></td>
<td><p><a href="../Page/韩国铁道101系柴油动车组.md" title="wikilink">101系的动力车厢</a>，已于2013年1月5日停用。</p></td>
</tr>
<tr class="even">
<td><p>348~363, 365~373, 375~408, 410~412,<br />
414~444, 522~523, 525~527, 529~552,<br />
554~557, 581~585</p></td>
<td><p>普通座席车厢，由<a href="../Page/韩国铁道8200型电力机车.md" title="wikilink">8200型电力机车牵引</a>。目前522~523、525~527、529~552、554~557、581~585还在使用中。</p></td>
</tr>
<tr class="odd">
<td><p>621~625, 627~637, 639~649</p></td>
<td><p>头等座席车厢，由<a href="../Page/韩国铁道8200型电力机车.md" title="wikilink">8200型电力机车牵引</a>。现在运行于<a href="../Page/长项线.md" title="wikilink">长项线</a>。</p></td>
</tr>
<tr class="even">
<td><p>701~715</p></td>
<td><p>头等座席车厢，2012年11月1日停用。</p></td>
</tr>
<tr class="odd">
<td><p>751~755</p></td>
<td><p>头等座席车厢，2012年11月1日停用。</p></td>
</tr>
<tr class="even">
<td><p>781~784, 786~787</p></td>
<td><p>头等座席车厢，并拥有会议室，曾运行于<a href="../Page/长项线.md" title="wikilink">长项线</a>，在2012年11月1日停用。</p></td>
</tr>
<tr class="odd">
<td><p>819</p></td>
<td><p>餐车。</p></td>
</tr>
<tr class="even">
<td><p>902~930, 932~933, 938~939</p></td>
<td><p>拥有咖啡厅的车厢，现在已经改为餐车。</p></td>
</tr>
<tr class="odd">
<td><p>9564, 9565, 9666</p></td>
<td><p><a href="../Page/韩国铁道9501系柴油动车组.md" title="wikilink">9501系</a>，现已改为<a href="../Page/无穷花号.md" title="wikilink">无穷花号的</a><a href="../Page/海列车.md" title="wikilink">海列车</a>。</p></td>
</tr>
<tr class="even">
<td><p>10041~10043</p></td>
<td><p>头等座席车厢，其中10041、10042在<a href="../Page/2012年世界博览会.md" title="wikilink">2012年世界博览会期间使用</a>，目前均已停用。</p></td>
</tr>
<tr class="odd">
<td><p>10901~10908</p></td>
<td><p>拥有咖啡厅的车厢，现在已经改为餐车。</p></td>
</tr>
<tr class="even">
<td><p>11101~11119, 11121~11130,<br />
11132~11144, 11146~11147, 11149~11161,<br />
11163~11166, 11168~11170, 11174~11175,<br />
11186</p></td>
<td><p>普通座席车厢</p></td>
</tr>
<tr class="odd">
<td><p>11167, 11171, 41, 45, 10044, 10046, 10047</p></td>
<td><p>头等座席车厢、普通座席车厢和餐车，现已改为<a href="../Page/无穷花号.md" title="wikilink">无穷花号的</a><a href="../Page/酒吧影院列车.md" title="wikilink">酒吧影院列车</a>。</p></td>
</tr>
<tr class="even">
<td><p>11301~11309</p></td>
<td><p>头等座席车厢，现已改为<a href="../Page/无穷花号.md" title="wikilink">无穷花号的</a><a href="../Page/嗵嗵嗵音乐茶座列车.md" title="wikilink">嗵嗵嗵音乐茶座列车</a>。</p></td>
</tr>
<tr class="odd">
<td><p>11311~11329</p></td>
<td><p>观光用车厢，现在以改装为<a href="../Page/海浪号.md" title="wikilink">海浪号</a>。</p></td>
</tr>
</tbody>
</table>

## 参考资料

## 参见

  - [无穷花号](../Page/无穷花号.md "wikilink")
  - [统一号](../Page/统一号.md "wikilink")

[Category:韓國鐵路車輛](../Category/韓國鐵路車輛.md "wikilink")
[Category:鐵路列車](../Category/鐵路列車.md "wikilink")

1.  객차의 대차설계속도는 150km/h였으나 선로조건때문에 120km/h로 운전하였다.
2.  당시에는 모든 새마을호가 1세대 직각형차량이었고 유선형 차량은 8개월 뒤인 1986년 7월에야 운행을 개시한다.
3.  이는 당시 [고속철도](../Page/高速铁路.md "wikilink") 건설로 인한 작업 관계로 열차 서행구간이 많아
    종전의 시각표로는 정시 운전이 불가능했기 때문이다.
4.  运行于[全罗线和](../Page/全罗线.md "wikilink")[湖南线的新村号改为使用](../Page/湖南线.md "wikilink")[电力机车牵引](../Page/电力机车.md "wikilink")
5.  [새마을호 2015년 '역사 속으로'.. '누리로'가
    대체](http://view.asiae.co.kr/news/view.htm?idxno=2012032008384203934&nvr=Y)《아시아경제》2012.3.20
6.  [신형 새마을호 이름 'ITX-새마을'로
    불러주세요](http://www.newsis.com/ar_detail/view.html?ar_id=NISX20130221_0011859199&cID=10201&pID=10200)《뉴시스》2013.02.21
7.  통상적인 좁은 의미로는 동차형 새마을호와 장대형 새마을호를 제외한 1986\~1987년에 도입한 유선형 새마을호 객차만을
    의미한다.
8.  1010열차로 [서울역](../Page/首尔站.md "wikilink") 환승통로 설치공사 완료 시 까지
    [용산역](../Page/龙山站_\(首尔\).md "wikilink") 종착으로 운행한다.
9.  이 열차는 [통근형 디젤 동차차량을](../Page/韩国铁道9501系柴油动车组.md "wikilink") 이용하며,
    [코레일관광개발를](../Page/韩国铁路观光发展公司.md "wikilink") 통해 예약이 가능하다.
10. 개정 초기에는 없었으나, 동년 4월 12일 수정 개정에 의하여 운행이 재개되었다.
11. 1010열차로 [서울역](../Page/首尔站.md "wikilink") 환승통로 설치공사 완료 시 까지
    [용산역](../Page/龙山站_\(首尔\).md "wikilink") 종착으로 운행한다.
12. 1010열차로 [서울역](../Page/首尔站.md "wikilink") 환승통로 설치공사 완료 시 까지
    [용산역](../Page/龙山站_\(首尔\).md "wikilink") 종착으로 운행한다.
13. [경부고속철도](../Page/京釜高速铁道.md "wikilink")
    [대구](../Page/大邱.md "wikilink") 도심구간 공사로
    [동대구역](../Page/东大邱站.md "wikilink") 종착으로 변경된다.
14. 1010열차로 [서울역](../Page/首尔站.md "wikilink") 환승통로 설치공사 완료 시 까지
    [용산역](../Page/龙山站_\(首尔\).md "wikilink") 종착으로 운행한다.
15. [경부고속철도](../Page/京釜高速铁道.md "wikilink")
    [대구](../Page/大邱.md "wikilink") 도심구간 공사로
    [동대구역](../Page/东大邱站.md "wikilink") 종착으로 변경된다.
16. 1량은 자동문과 시트 교체 상태를 제외한 대체적으로 온전한 상태로, 남은 2량은 식당 및 편의점으로 개조된 채 전시되어
    있다.
17. 11035호 새마을호 객차가 특산품 매장 및 편의점으로 개조되어 전시되고 있다.
18. [101系](../Page/韩国铁道101系柴油动车组.md "wikilink")304号静态保存
19. [7100型柴油机车](../Page/韩国铁道7100型柴油机车.md "wikilink")7134号静态保存
20. [101系](../Page/韩国铁道101系柴油动车组.md "wikilink")336号静态保存
21. 현재 [레일펜션](http://www.gsrailpension.co.kr/)으로 사용되고 있다.