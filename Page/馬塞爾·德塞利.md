**馬塞爾·德塞利**（，），已退役的[法國足球員](../Page/法國.md "wikilink")，世界足壇最佳法國巨星之一。司職後衛，乃法國[1998年世界杯冠軍隊成員](../Page/1998年世界杯.md "wikilink")。

## 生平

迪西里出身於法甲球會[南特](../Page/南特足球俱乐部.md "wikilink")，1992年轉會[馬賽](../Page/马赛足球俱乐部.md "wikilink")，曾協助球會贏得1993年[歐冠杯冠軍](../Page/歐冠杯.md "wikilink")。其後馬賽被揭發賽前曾向球員注射禁藥，結果馬賽的法甲联赛冠軍資格被撤消，而馬賽亦因財政問題降级。迪西里於是在1993年轉會[意甲的](../Page/意甲.md "wikilink")[AC米蘭](../Page/AC米蘭.md "wikilink")。

轉投AC米蘭的迪西里最初一度是後防重心，並為AC米蘭贏得兩屆聯賽冠軍。但及後AC米蘭成績大倒退，聯賽排名更一度跌至第十名左右，而迪西里的表現亦日見低沉。1998年世界杯迪西里雖然入選了法國國家隊，成為冠軍隊成員，但在決賽時卻領紅而被罰離場。

1998年世界杯後迪西里轉投英超球會[切尔西](../Page/切尔西.md "wikilink")，當時迪西里表現依舊與在AC米蘭後期時一樣，一季後逐漸恢復過來。當時車路士財力及實力皆遜於[曼聯](../Page/曼聯.md "wikilink")，雖然迪西里在車路士沒有贏得英超聯賽冠軍，但曾獲得足總杯冠軍。

在2004年歐洲國家杯後，迪西里宣告退出國家隊，表示著他代表國家隊次數永遠維持在116場，乃法國國腳中數一數二。之後他亦離開車路士轉投[卡塔爾聯賽](../Page/卡塔爾.md "wikilink")，之後宣告退役。

2003年迪西里獲國際足協選為[FIFA
100成員](../Page/FIFA_100.md "wikilink")，這名單中法國球員有多達14人入選，僅次於巴西。

## 效力球會

  - 1986-1992 南特
  - 1992-1993 馬賽
  - 1993-1998 AC米蘭
  - 1998-2004 車路士

## 榮譽

  - 世界杯冠軍：1998
  - 意甲聯賽冠軍：1994、1996
  - 歐洲聯賽冠軍杯：1993、1994
  - 英格蘭足總杯：2000

[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:南特球員](../Category/南特球員.md "wikilink")
[Category:馬賽球員](../Category/馬賽球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:艾加拉法球員](../Category/艾加拉法球員.md "wikilink")
[Category:FIFA 100](../Category/FIFA_100.md "wikilink")
[Category:FIFA世纪俱乐部](../Category/FIFA世纪俱乐部.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")
[Category:洲際國家盃冠軍隊球員](../Category/洲際國家盃冠軍隊球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:1996年歐洲國家盃球員](../Category/1996年歐洲國家盃球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:2000年歐洲國家盃球員](../Category/2000年歐洲國家盃球員.md "wikilink")
[Category:2001年洲際國家盃球員](../Category/2001年洲際國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2003年洲際國家盃球員](../Category/2003年洲際國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")