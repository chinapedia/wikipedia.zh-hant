**应城市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[湖北省的一个](../Page/湖北省.md "wikilink")[县级市](../Page/县级市.md "wikilink")，由地级[孝感市代管](../Page/孝感市.md "wikilink")。应城市位于湖北省东方，孝感市西南方向，介于东经113°19′-113°45′，北纬30°43′-31°08′之间。[1](http://www.yingcheng.gov.cn)

应城市为古蒲骚之地。是[南朝](../Page/南朝.md "wikilink")[宋孝武帝孝建元年](../Page/宋孝武帝.md "wikilink")（454年）析安陆县南境置应城县，北朝[西魏大统十六年](../Page/西魏.md "wikilink")（550年）以应城为城阳郡治，在隋时曾改应城为应阳，在[唐武德四年](../Page/唐德剛.md "wikilink")（621年）复应阳为应城。在1944年8月，应城县还只分为应城、应西两县，但在第二年年3月，应城、应西两县复合并为应城县。1986年5月，经国务院批准，应城撤县设市。[2](http://www.yingcheng.gov.cn)

应城市境内有大量的[石膏矿](../Page/石膏.md "wikilink")、盐矿，有『膏都盐海』之称。

## 行政划分

### 街道

[城中街道](../Page/城中街道_\(应城市\).md "wikilink") -
[城北街道](../Page/城北街道_\(应城市\).md "wikilink") -
[四里棚街道](../Page/四里棚街道.md "wikilink") -
[东马坊街道](../Page/东马坊街道.md "wikilink") -
[长江埠街道](../Page/长江埠街道.md "wikilink")

### 镇

[田店镇](../Page/田店镇.md "wikilink") -
[杨河镇](../Page/杨河镇_\(应城市\).md "wikilink") -
[三合镇](../Page/三合镇.md "wikilink") -
[郎君镇](../Page/郎君镇.md "wikilink") -
[黄滩镇](../Page/黄滩镇.md "wikilink") -
[天鹅镇](../Page/天鹅镇.md "wikilink") -
[义和镇](../Page/义和镇.md "wikilink") -
[陈河镇](../Page/陈河镇.md "wikilink") -
[杨岭镇](../Page/杨岭镇.md "wikilink") -
[汤池镇](../Page/汤池镇_\(应城市\).md "wikilink")

### 其它

[南垸良种场](../Page/南垸良种场.md "wikilink") -
[经济技术开发区](../Page/经济技术开发区_\(应城市\).md "wikilink")

## 人口及民族

### 人口

2016年末，户籍总人口66.59万人，当年出生人口6620人，死亡人口4001人，人口自然增长率3.96‰。出生人口性别比为100:111.57。[3](http://yingcheng.gov.cn/government/publicinfoshow.aspx?id=5531)

### 民族

截至2011年末，应城市境内主体民族为汉族，另有土家族、回族、苗族、侗族、壮族、维吾尔族、满族、蒙古族、土族、朝鲜族、藏族、纳西族、瑶族、达斡尔族、水族、锡伯族、仡佬族等17个少数民族[。](http://www.yingcheng.gov.cn)

## 著名人物

### 政治军事人物

[刘仁静](../Page/刘仁静.md "wikilink") [杨德清](../Page/杨德清.md "wikilink")
[喻林祥](../Page/喻林祥.md "wikilink") [蒋作宾](../Page/蒋作宾.md "wikilink")
[张孝忠](../Page/张孝忠.md "wikilink") [程明群](../Page/程明群.md "wikilink")
[汪德源](../Page/汪德源.md "wikilink") [陈进平](../Page/陈进平.md "wikilink")
[杨业功](../Page/杨业功.md "wikilink")

### 体育人物

[彭丽萍](../Page/彭丽萍.md "wikilink") [李青](../Page/李青.md "wikilink")

[应城市](../Category/应城市.md "wikilink")
[市](../Category/孝感区县市.md "wikilink")
[孝感市](../Category/湖北县级市.md "wikilink")
[鄂](../Category/中国小城市.md "wikilink")