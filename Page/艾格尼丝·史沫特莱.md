**艾格尼丝·史沫特莱**（；）[美国著名](../Page/美国.md "wikilink")[左派](../Page/左派.md "wikilink")[记者](../Page/记者.md "wikilink")，以对[中共革命的报道著称](../Page/中共.md "wikilink")。她支持[女权](../Page/女权.md "wikilink")、支持[印度独立](../Page/印度独立.md "wikilink")、亦支持中国共产党，她著有8本书。

## 生平

### 早年

1892年，史沫特莱出生于美国[密苏里州的](../Page/密苏里州.md "wikilink")[奥斯古德](../Page/奥斯古德.md "wikilink")，幼年家庭困难，从事过许多不同的工作，包括侍女、烟厂工人。1901年时随父母移居科罗拉多州的[特立尼达](../Page/特立尼达_\(科罗拉多州\).md "wikilink")。17岁时，史沫特莱参加县教师资格考试并在[新墨西哥州的乡村学校任教一学期](../Page/新墨西哥州.md "wikilink")。

1910年，史沫特莱在加利福尼亚积极从事印度反抗英国统治的斗争。1912年8月，史沫特莱与欧内斯特（）结婚，1916年两人离婚。次年史沫特莱到达[纽约](../Page/纽约.md "wikilink")，为《号角》撰稿同时为女权主义刊物《节育评论》撰稿。1918年3月则因声援[印度独立运动而被当局以煽动反抗英国统治的叛乱而捕入狱](../Page/印度独立运动.md "wikilink")6个月。1919年起侨居[柏林](../Page/柏林.md "wikilink")8年，继续从事印度解放运动，还曾在柏林与[尼赫鲁会面](../Page/尼赫鲁.md "wikilink")。1928年完成[自传](../Page/自传.md "wikilink")《大地的女儿》（）。

### 前往中国

1929年初，史沫特莱通过一位德国共产党人的介绍，以《[法兰克福日报](../Page/法兰克福日报.md "wikilink")》记者身份途经[苏联来到中国](../Page/苏联.md "wikilink")，在[上海](../Page/上海.md "wikilink")，她认识了[鲁迅](../Page/鲁迅.md "wikilink")、[郭沫若等文化名人](../Page/郭沫若.md "wikilink")，并协助[宋庆龄处理了一些文件](../Page/宋庆龄.md "wikilink")。[希特勒上台后](../Page/希特勒.md "wikilink")，她改为担任英国《曼彻斯特卫报》驻中国记者，每周发回两篇报道。撰文报道[江西的革命斗争](../Page/江西.md "wikilink")，写出了《[国民党反动的五年](../Page/国民党.md "wikilink")》、《中国人的命运》、《[中国红军在前进](../Page/中国红军.md "wikilink")》等文章。

1934年她再次访华。1936年冬她来到中共地下党十分活跃的[西安](../Page/西安.md "wikilink")，试探去延安的可能。恰在此时[西安事变爆发](../Page/西安事变.md "wikilink")，[国民政府封锁消息](../Page/国民政府.md "wikilink")，史沫特莱在西安电厂将事变情况及时向上海的西方新闻媒体通报，成了相当一段时间里唯一的英文新闻来源，一时声名大噪。

西安事变後，1937年1月，她应邀来到延安。一路上受到[左权](../Page/左权.md "wikilink")、[彭德怀和](../Page/彭德怀.md "wikilink")[贺龙的接待](../Page/贺龙.md "wikilink")，进延安前左派女作家丁玲受党委派前来迎接。史沫特莱一到延安就受到[毛泽东和](../Page/毛泽东.md "wikilink")[朱德的接见](../Page/朱德.md "wikilink")。到达延安的第二天，延安党政机关举行欢迎大会，史沫特莱在会上畅谈了一个多小时自己的反帝斗争经历。

史沫特莱去延安的最初目的是采访，写一部象《西行漫记》那样的作品。但史沫特莱并不把自己看成职业记者，而是一个政治活动家。她积极地利用自己的国际关系为延安争取援助，后来加拿大医生[白求恩来中国的部分原因就是源自她的推荐](../Page/白求恩.md "wikilink")。她邀请自己的许多记者朋友来延安，打破国民党对边区的新闻封锁。她参与并主持了延安[鲁迅艺术学院外语部的工作](../Page/鲁迅艺术学院.md "wikilink")。她甚至还发起了一场节制生育运动。接著她又发起了灭鼠运动，一开始被嘲笑为是西方人不切实际的讲究，但後来受到了毛泽东的支持。

1937年7月史沫特莱提出加入中共的申请，但被中共拒绝了。她嚎啕大哭，把前来通知她的[陆定一惊得不知所措](../Page/陆定一.md "wikilink")，只能用“当一个党外记者作用更大”来安慰她。于是她开始担任八路军总部随军外国记者一职。

延安期间，毛泽东经常与史沫特莱以及女翻译[吴莉莉跳舞](../Page/吴莉莉.md "wikilink")、聊天。因此贺子珍与毛泽东的感情出现裂痕，关系紧张。最终贺子珍决定出走苏联。\[1\]

1938年1月，她从山西来到武汉和美国大使、[南斯拉夫卫生专家等人商谈筹办中国红十字救助总队](../Page/南斯拉夫.md "wikilink")。此后，她便在中国红十字会军医部展开工作。她还说服[日内瓦](../Page/日内瓦.md "wikilink")[红十字国际委员会供应中国军队部分急需药品](../Page/红十字国际委员会.md "wikilink")。

为解决中国军医缺乏问题，史沫特莱积极号召外国医务志愿者来中国，著名加拿大医生[诺尔曼·白求恩与](../Page/诺尔曼·白求恩.md "wikilink")[理查德·布朗](../Page/理查德·布朗.md "wikilink")、印度著名外科医生[柯棣华等受到她的影响来中国参与支援](../Page/柯棣华.md "wikilink")。在武汉的10个月，通过在前线的经历完成了《中国在反击》一书。1938年10月中旬，随[中国红十字会医疗救护队退至长沙](../Page/中国红十字会.md "wikilink")。将[皖南事变的消息发表在](../Page/皖南事变.md "wikilink")《[纽约时报](../Page/纽约时报.md "wikilink")》上。1940年9月去香港养病。

1941年5月回到美国，回国后，为中国募集救济战争灾难的捐款，并為[朱德作傳](../Page/朱德.md "wikilink")《偉大的道路：朱德的生平和時代》，该书於1956年作者逝世後出版，1979年[三聯書店出版中譯本](../Page/三聯書店.md "wikilink")。這本著作和[埃德加·斯诺的](../Page/埃德加·斯诺.md "wikilink")《[西行漫記](../Page/西行漫記.md "wikilink")》並列為西方人向本國介紹[中共革命的經典著作](../Page/中共.md "wikilink")。

### 战后

1940年代中期，史沫特莱移居纽约，1949年在[麦卡锡主义的反共潮流中被称作](../Page/麦卡锡主义.md "wikilink")“苏联[间谍](../Page/间谍.md "wikilink")”，被迫流亡英国。

1950年5月6日，史沫特莱因手术不治在[伦敦逝世](../Page/伦敦.md "wikilink")。1951年5月6日，在她逝世一周年時，骨灰安葬於[北京市八寶山革命公墓](../Page/北京市八寶山革命公墓.md "wikilink")，朱德親題：「[中國人民之友美國革命作家艾格妮絲](../Page/中国人民的老朋友.md "wikilink")·史沫特萊女士之墓」。

史沫特莱与[埃德加·斯诺](../Page/埃德加·斯诺.md "wikilink")、[安娜·路易絲·斯特朗被并称为](../Page/安娜·路易絲·斯特朗.md "wikilink")“[3S](../Page/3S.md "wikilink")”。1984年，中国曾专门成立“中国三S研究会”（后改名为“[中国国际友人研究会](../Page/中国国际友人研究会.md "wikilink")”），[邓颖超任名誉会长](../Page/邓颖超.md "wikilink")，[黄华任会长](../Page/黄华.md "wikilink")。1985年，[中国人民邮政曾发行](../Page/中国人民邮政.md "wikilink")“[中国人民之友](../Page/中国人民的老朋友.md "wikilink")”系列纪念邮票，包含“3S”\[2\]。

## 身份曝光

美国陆军将军威洛比曾声称她是曾同居过的间谍[佐尔格的情报网中的一名间谍](../Page/佐尔格.md "wikilink")，为此她曾经威胁说要控告他。史沫特莱的传记記載，从苏联档案中能发现有力的证据，表明她是[共产国际的一名间谍](../Page/共产国际.md "wikilink")。\[3\]据[公共電視網](../Page/公共電視網.md "wikilink")，史沫特莱作為三重间谍，替[中国共产党](../Page/中国共产党.md "wikilink")、[印度共产党和](../Page/印度共产党.md "wikilink")[苏联共产党工作](../Page/苏联共产党.md "wikilink")，“是20世纪最多產的女间谍之一”\[4\]

## 图集

<File:1H-ACC> 93-1116-teenager.jpg|1914年的史沫特莱 <File:Dubail> Apartments
Shanghai.JPG|thumb|250px|史沫特莱在中国的第一个住处——上海[吕班公寓](../Page/吕班公寓.md "wikilink")
<File:Soong>, Lin, Lu and
Smedley.jpg|1930年代和[宋庆龄](../Page/宋庆龄.md "wikilink")、[鲁迅一行人](../Page/鲁迅.md "wikilink")
<File:Smedley> headstone babaoshan 2012
01.jpg|[北京八宝山革命公墓史沫特莱之墓](../Page/北京八宝山革命公墓.md "wikilink")

## 参考文献

### 引用

### 来源

  - 网页

<!-- end list -->

  - [史沫特莱创作朱德传记始末](http://cpc.people.com.cn/GB/64093/67507/5248834.html)
  - [美国革命作家——艾格尼丝·史沫特莱](http://politics.people.com.cn/GB/1026/3713062.html)
  - [史沫特莱，为谁的事业而奋斗？](http://www.xschina.org/show.php?id=4511)
  - [史沫特莱，一颗昂然而逝的彗星](https://web.archive.org/web/20160305025735/http://blog.sina.com.cn/s/blog_46830b41010000fw.html)

<!-- end list -->

  - 书籍

<!-- end list -->

  - 朱华丽：《史沫特莱》，《布老虎传记文库·巨人百传丛书：英雄探险家卷》 辽海出版社 1998-10-1 ISBN
    978-7-80638-926-3

## 参见

  - [中国人民的老朋友](../Page/中国人民的老朋友.md "wikilink")
      - “[3S](../Page/3S.md "wikilink")”
          - [安娜·路易斯·斯特朗](../Page/安娜·路易斯·斯特朗.md "wikilink")
          - [埃德加·斯诺](../Page/埃德加·斯诺.md "wikilink")
  - [共产国际](../Page/共产国际.md "wikilink")、[间谍](../Page/间谍.md "wikilink")
  - [麥卡錫主義](../Page/麦卡锡主义.md "wikilink")

{{-}}

[Category:美国记者](../Category/美国记者.md "wikilink")
[Category:美国作家](../Category/美国作家.md "wikilink")
[Category:共产国际女特工](../Category/共产国际女特工.md "wikilink")
[美国](../Category/中国人民的老朋友.md "wikilink")
[Category:在中华民国的美国人](../Category/在中华民国的美国人.md "wikilink")
[Category:在德国的美国人](../Category/在德国的美国人.md "wikilink")
[Category:在英国的美国人](../Category/在英国的美国人.md "wikilink")
[Category:麥卡錫主義受害者](../Category/麥卡錫主義受害者.md "wikilink")
[Category:葬于北京市八宝山革命公墓](../Category/葬于北京市八宝山革命公墓.md "wikilink")

1.
2.
3.
4.  [Agnes
    Smedley](http://www.pbs.org/wgbh/nova/venona/dece_smedley.html), PBS
    Nova