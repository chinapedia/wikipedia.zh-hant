**氯化錫（II）**，化學式[Sn](../Page/錫.md "wikilink")[Cl](../Page/氯.md "wikilink")，為白色[固體](../Page/固體.md "wikilink")。

## 製備

將[錫溶於濃](../Page/錫.md "wikilink")[鹽酸中](../Page/鹽酸.md "wikilink")，再將溶液蒸發，製得無色針狀結晶SnCl‧2HO。反應式：

\[\ Sn+2HCl\to SnCl_2+H_2\uparrow\]

## 性質

溶於水中則水解生成[鹼式氯化亞錫](../Page/鹼式.md "wikilink")\[Sn(OH)Cl\]的白色沉澱。反應式：

\[\ SnCl_2+H_2O\to Sn(OH)Cl+HCl\]

但可完全溶解於酸性溶液之中，並能成功進行[金屬置換反應](../Page/金屬置換反應.md "wikilink") (displacement
reaction)

\[\ SnCl_2 (aq) + Zn\to Sn + ZnCl_2 (aq)\]

在酸性環境下易[氧化成](../Page/氧化.md "wikilink")[氯化錫](../Page/氯化錫.md "wikilink")(IV)，為強[還原劑](../Page/還原劑.md "wikilink")。主要用作化学试剂、有机锡合成原料、电镀塑料电镀的敏化剂、香料的稳定剂、食品添加剂印染助剂、农药中间体、有机合成催化剂等。

[Category:金属卤化物](../Category/金属卤化物.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:锡化合物](../Category/锡化合物.md "wikilink")
[Category:配位化合物](../Category/配位化合物.md "wikilink")
[Category:潮解物质](../Category/潮解物质.md "wikilink")
[Category:还原剂](../Category/还原剂.md "wikilink")