**孙宝琦**（），字**慕韩**，[浙江省](../Page/浙江省.md "wikilink")[杭州府](../Page/杭州府.md "wikilink")[钱塘县](../Page/钱塘县.md "wikilink")（今[杭州市](../Page/杭州市.md "wikilink")）人。[清末](../Page/清末.md "wikilink")[民初政治家](../Page/中華民國.md "wikilink")、外交官，曾任中华民国外交总长、[国务总理](../Page/中华民国国务总理.md "wikilink")。

## 生平

清末大臣[孫詒經之子](../Page/孫詒經.md "wikilink")。成[廕生](../Page/廕生.md "wikilink")。曾任[直隶](../Page/直隶省.md "wikilink")[道员](../Page/道员.md "wikilink")。1900年[八国联军进京](../Page/八国联军.md "wikilink")，随[光绪帝至](../Page/光绪帝.md "wikilink")[西安](../Page/西安.md "wikilink")。\[1\]1901年（[光緒二十七年](../Page/光緒.md "wikilink")）起，历任驻[德国](../Page/德国.md "wikilink")、[日斯巴尼亚](../Page/日斯巴尼亚.md "wikilink")（今称[西班牙](../Page/西班牙.md "wikilink")）、[葡萄牙](../Page/葡萄牙.md "wikilink")、[法国等国使馆随员](../Page/法国.md "wikilink")。1902年7月，升任[出使法国大臣](../Page/中国驻法国大使列表.md "wikilink")。1905年（光緒31年）归国，署[顺天](../Page/順天府.md "wikilink")[府尹](../Page/府尹.md "wikilink")。1907年4月，出任[出使德国大臣](../Page/中国驻德国大使列表.md "wikilink")，筹措讨还[青岛事宜](../Page/青岛.md "wikilink")。1908年（光緒三十四年）回国，充帮办[津浦铁路大臣](../Page/津浦铁路.md "wikilink")。1911年（[宣統三年](../Page/宣統.md "wikilink")），转任[山东巡抚](../Page/山东巡抚.md "wikilink")。任内阻止德国取得山东矿权。\[2\]同年，[辛亥革命爆发](../Page/辛亥革命.md "wikilink")，孫寶琦宣布[山东独立](../Page/山东.md "wikilink")，随后又取消独立。\[3\]

1913年（[民国二年](../Page/民国紀元.md "wikilink")）9月，孫寶琦任[北京政府](../Page/北洋政府.md "wikilink")[熊希龄内阁外交总长](../Page/熊希龄内阁.md "wikilink")。1914年2月12日，兼代国务总理。1914年5月1日辞去国务总理，专任外交总长（外交次长为[曹汝霖](../Page/曹汝霖.md "wikilink")）。同年7月[第一次世界大战爆发](../Page/第一次世界大战.md "wikilink")，孫寶琦支持[袁世凯的中立政策](../Page/袁世凯.md "wikilink")。1915年1月，因日本提出[二十一条](../Page/二十一条.md "wikilink")，辞去外交总长（[陆征祥接任](../Page/陆征祥.md "wikilink")），改任[审计院院长](../Page/中華民國审计院.md "wikilink")。1916年（民国5年）4月，任[段祺瑞内阁财政总长](../Page/第一次段祺瑞內閣.md "wikilink")。同年6月兼[汉冶萍公司董事长](../Page/汉冶萍公司.md "wikilink")。1919年任[招商局董事会会长](../Page/招商局.md "wikilink")。1920年（民国9年）春，任经济调查局总裁（税务督办）。1922年（民国十一年）1月，任[扬子江水道讨论委员会会长](../Page/扬子江.md "wikilink")；同年4月任外交部[太平洋会议善后委员会副会长](../Page/太平洋会议.md "wikilink")。\[4\]\[5\]

经[大总统](../Page/中华民国大总统.md "wikilink")[曹锟提名](../Page/曹锟.md "wikilink")，[国会通过](../Page/中華民國第一届国会.md "wikilink")，1924年（民国十三年）1月12日，孫寶琦被任命为国务总理，1月15日[孙宝琦内阁就职](../Page/孙宝琦内阁.md "wikilink")。1924年，孫寶琦还兼任外交委员会委员长。任内，孫寶琦提出「奉行宪法」、「和平统一」的施政方针，同[苏联建立了外交关系](../Page/苏联.md "wikilink")，向[德国索赔成功](../Page/德国.md "wikilink")。同年7月2日，因与[曹锟的嫡系](../Page/曹锟.md "wikilink")、财政总长[王克敏不和](../Page/王克敏.md "wikilink")，提出辞职。由外交总长[顾维钧代理国务总理](../Page/顾维钧.md "wikilink")。\[6\]\[7\]

1925年（民国十四年）2月，孫寶琦被[段祺瑞任命为淞滬商埠督办](../Page/段祺瑞.md "wikilink")。同年8月，被任命为[驻苏联大使](../Page/中国驻俄国大使列表.md "wikilink")，但未就任。1926年（民国15年），任[中法大学董事長](../Page/中法大学.md "wikilink")。\[8\]\[9\]

1931年（民国二十年）2月3日，孫寶琦在上海逝世，享年65岁。\[10\]

孙宝琦在北京的故居位于[北京市](../Page/北京市.md "wikilink")[东城区](../Page/东城区_\(北京市\).md "wikilink")[东四六条](../Page/东四六条.md "wikilink")。

## 圖集

<File:Sun> Baoqi4.jpg|孙宝琦 <File:Sun> Baoqi5.jpg|孙宝琦在法国 <File:Sun>
Baoqi14.jpg|孙宝琦

## 家庭

[孫寶琦總長之女公子.jpg](https://zh.wikipedia.org/wiki/File:孫寶琦總長之女公子.jpg "fig:孫寶琦總長之女公子.jpg")。\]\]
[Sun_Baoqi_and_his_son.jpg](https://zh.wikipedia.org/wiki/File:Sun_Baoqi_and_his_son.jpg "fig:Sun_Baoqi_and_his_son.jpg")
孙宝琦有一妻四妾，子女24人（8个儿子，16个女儿）：\[11\]\[12\]\[13\]

  - 长女：孙用慧，嫁[盛恩颐](../Page/盛恩颐.md "wikilink")（[盛宣怀的第四子](../Page/盛宣怀.md "wikilink")）
  - 次女：孙用智，嫁[载伦](../Page/载伦.md "wikilink")（[庆亲王](../Page/庆亲王.md "wikilink")[奕劻的第五子](../Page/奕劻.md "wikilink")）
  - 三女：嫁大学士[王文韶之孙](../Page/王文韶.md "wikilink")
  - 四女：孙用履，嫁入大臣[宝熙家](../Page/宝熙.md "wikilink")
  - 五女：嫁[袁克齐](../Page/袁克齐.md "wikilink")（[袁世凯的第七子](../Page/袁世凯.md "wikilink")）
  - 六女：
  - 七女：孙用藩，嫁[张廷重](../Page/张廷重.md "wikilink")（[张佩纶之子](../Page/张佩纶.md "wikilink")，[李鸿章的外孙](../Page/李鸿章.md "wikilink"))，成为[张爱玲的继母](../Page/张爱玲.md "wikilink")
  - 八女：嫁[天津](../Page/天津.md "wikilink")[国华银行经理崔氏](../Page/国华银行.md "wikilink")
  - 九女：
  - 十女：
  - 十一女：
  - 十二女：
  - 十三女：
  - 十四女：

[Sun_Baoqi9.jpg](https://zh.wikipedia.org/wiki/File:Sun_Baoqi9.jpg "fig:Sun_Baoqi9.jpg")

  - 十五女：
  - 十六女：
  - 长子：孙用时（景阳）
  - 次子：孙用震
  - 三子：孙雷生，娶冯家贤（[冯国璋之女](../Page/冯国璋.md "wikilink")）
  - 四子：孙用岱（蔚青），娶[盛范颐](../Page/盛范颐.md "wikilink")（[盛宣怀的亲侄女](../Page/盛宣怀.md "wikilink")）
  - 五子：
  - 六子：
  - 七子：
  - 八子：
  - 侄子：姓名未详，娶袁籙祯（袁世凯的第六女）

## 逸事

1904年冬，[孙中山携](../Page/孙中山.md "wikilink")[朱和中赴](../Page/朱和中.md "wikilink")[德国](../Page/德国.md "wikilink")[柏林](../Page/柏林.md "wikilink")，访问中国留德学生。在[宾步程的带动下](../Page/宾步程.md "wikilink")，[王发科](../Page/王发科.md "wikilink")、[王相楚等留学生加入了](../Page/王相楚.md "wikilink")[兴中会](../Page/兴中会.md "wikilink")。在柏林逗留数日后，孙中山与[朱和中赴](../Page/朱和中.md "wikilink")[法国](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")，留法学生[唐豸](../Page/唐豸.md "wikilink")、[胡瑛](../Page/胡瑛.md "wikilink")、[汤芗铭](../Page/汤芗铭.md "wikilink")、[向国华等十多人应邀入](../Page/向国华.md "wikilink")[兴中会](../Page/兴中会.md "wikilink")。[孙中山在](../Page/孙中山.md "wikilink")[巴黎住在](../Page/巴黎.md "wikilink")[利倭尼街的瓦克拉旅馆](../Page/利倭尼街.md "wikilink")。一天，[汤芗铭](../Page/汤芗铭.md "wikilink")、[向国华](../Page/向国华.md "wikilink")、[王发科](../Page/王发科.md "wikilink")、[王相楚四人结伴来找孙中山去附近的咖啡馆喝咖啡](../Page/王相楚.md "wikilink")，王发科、王相楚二人中途离席，到瓦克拉旅馆盗走了孙中山的行箧中的党人名单及入盟书、法国殖民大臣致安南总督的信函。此为王发科、王相楚、汤芗铭、向国华四人合伙设计。得手后，四人到清国驻法国公使馆告密，公使孙宝琦训斥四人称：“你们加入革命党，是叛清朝；今来自首，又叛革命党。且陷害同学，人格何在！”孙宝琦收阅了四人盗取的文件后称，“今后你们要好好念书，安分守己，不要胡闹。”留法学生[夏坚仲是孙宝琦的亲戚](../Page/夏坚仲.md "wikilink")，得知消息后赶到使馆向孙宝琦疏通。孙宝琦乃将入盟者的宣誓书烧掉，将其余文件交给夏坚仲，夏坚仲通过邮局将文件寄还孙中山。但是，孙宝琦从法国殖民大臣致安南总督的信函中得知孙中山的安南起义计划后，即赴法国外交部交涉，导致孙中山的起义计划未能实现。\[14\]

## 参考文献

## 相关条目

  - [孙宝琦内阁](../Page/孙宝琦内阁.md "wikilink")

|                                      |
| ------------------------------------ |
| （[北京政府](../Page/北京政府.md "wikilink")） |

[Category:清朝山東巡撫](../Category/清朝山東巡撫.md "wikilink")
[Category:出使德国钦差大臣](../Category/出使德国钦差大臣.md "wikilink")
[Category:出使法国钦差大臣](../Category/出使法国钦差大臣.md "wikilink")
[Category:清朝外交官](../Category/清朝外交官.md "wikilink")
[Category:清朝順天府府尹](../Category/清朝順天府府尹.md "wikilink")
[Category:中華民國國務總理](../Category/中華民國國務總理.md "wikilink")
[Category:中華民國外交總長](../Category/中華民國外交總長.md "wikilink")
[Category:中華民國财政總長](../Category/中華民國财政總長.md "wikilink")
[Category:中華民國駐蘇聯大使](../Category/中華民國駐蘇聯大使.md "wikilink")
[Category:余杭人](../Category/余杭人.md "wikilink")
[B寶](../Category/孫姓.md "wikilink")

1.  光绪朝实录 卷之四百九十二

2.

3.

4.
5.

6.
7.
8.
9.
10.
11.

12.
13. 宋路霞，盛宣怀家族，上海科学文献出版社，2009年

14. [胡为雄，帝制的终结——辛亥天变，当代中国出版社，2011年](http://history.people.com.cn/GB/205478/15499794.html)