**Xcin**是一套廣泛使用的[X
Window](../Page/X_Window.md "wikilink")[中文輸入軟體](../Page/中文輸入法.md "wikilink")，現已停止開發，最後版本是2.5.3pre2。

## XIM時代之前的Xcin

最早期的版本於1994年10月發表，由名為劉德華（Edward Der-Hua
Liu）的作者開發，使用專屬的[API](../Page/API.md "wikilink")，各種軟體只要經過簡單的改寫就能夠增加中文輸入的功能。這個軟體可以輕易的加入各種不同的輸入法。作者並且搭改寫了一套[crxvt的](../Page/crxvt.md "wikilink")[終端機](../Page/終端機.md "wikilink")[模擬程式來配合xcin的使用](../Page/模擬.md "wikilink")，讓使用者可以在[X視窗下使用各類文字](../Page/X視窗.md "wikilink")[軟體](../Page/軟體.md "wikilink")。crxvt加上xcin可以取代xterm的功能。由於輸入法是分開的模組，所以原則上早期的xcin可以靈活搭配各式軟體使用，作者也改寫了一個文字編輯器，使它可以輸入中文，做為範例。可惜實際上除了上述的兩個軟體外，幾乎沒有程式被改寫來搭配xcin的專屬API。直到[Xcin
Anywhere的出現](../Page/Xcin_Anywhere.md "wikilink")，才使得Xcin成為[X視窗泛用輸入法](../Page/X視窗.md "wikilink")。

## XIM時代後的Xcin

後來由[謝東翰等人改寫了xcin](../Page/謝東翰.md "wikilink")，將專屬的API改成了標準的[XIM](../Page/XIM.md "wikilink")，開啟了xcin的新時代。在多人的努力下，xcin成了高度模組化的架構，成為X視窗下最重要的中文輸入程式之一。許多的[Linux套件都包含了xcin](../Page/Linux.md "wikilink")。例如[CLE自v](../Page/Linux中文延伸套件計劃.md "wikilink")0.9版起，即改用Xcin
2.5做為系統預設的輸入法。改寫後的版本由2.5開始。最後的版本是2.5.3 beta。

## 螢火飛新版

著名自由軟體開發者[Firefly](../Page/Firefly.md "wikilink")（[螢火飛](../Page/螢火飛.md "wikilink")）曾開發及發表3.0及3.1版的Xcin，新版本已改名為[OXIM發表](../Page/OXIM.md "wikilink")。他的版本有以下改良：

  - 全新介面
  - 選字視窗垂直排列
  - 打繁出簡、打簡出繁
  - 支援[GTK+](../Page/GTK+.md "wikilink") On The Spot

## 參見

  - [Linux中文延伸套件計劃](../Page/Linux中文延伸套件計劃.md "wikilink")
  - [謝東翰](../Page/謝東翰.md "wikilink")

## 外部連結

  - [xcin的官方網站](https://web.archive.org/web/20061009224503/http://cle.linux.org.tw/xcin/)
  - [OSS桌面應用增進計畫](http://opendesktop.org.tw/)
  - [新XCIN特色簡介](https://web.archive.org/web/20060512192857/http://opendesktop.org.tw/demopage/new_xcin/)所謂的新XCIN即OXIM的前身
  - [OXIM特色簡介](https://web.archive.org/web/20071213212222/http://opendesktop.org.tw/demopage/oxim/)

[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:中文輸入法平台](../Category/中文輸入法平台.md "wikilink")