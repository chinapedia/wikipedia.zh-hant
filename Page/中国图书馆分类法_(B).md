## [哲学](../Page/哲学.md "wikilink")、[宗教](../Page/宗教.md "wikilink")

  - B-4 哲学教育与普及

:\*B-49 哲学学习与普及

  - B0 哲学理论

:\*B0-0 [马克思主义哲学](../Page/马克思主义哲学.md "wikilink")

:\*B01 [哲学基本问题](../Page/哲学基本问题.md "wikilink")

::\*B013 哲学的阶级性和实践性

::\*B014 哲学的对象、目的与方法

::\*B015
[唯物主义和](../Page/唯物主义.md "wikilink")[唯心主义](../Page/唯心主义.md "wikilink")

::\*B016 [本体论](../Page/本体论_\(哲学\).md "wikilink")

:::\*B016.8 [宇宙论](../Page/宇宙論.md "wikilink")

:::\*B016.9 [时空论](../Page/时空论.md "wikilink")

::\*B017 [认识论](../Page/认识论.md "wikilink")

:::\*B017.8
[决定论与](../Page/決定論.md "wikilink")[非决定论](../Page/非决定论.md "wikilink")

:::\*B017.9 [自我论](../Page/自我论.md "wikilink")

::\*B018 [价值论](../Page/價值論.md "wikilink")

:::\*B019.1 [唯物主义](../Page/唯物主义.md "wikilink")

::::\*B019.11 [朴素唯物主义](../Page/朴素唯物主义.md "wikilink")

::::\*B019.12 [形而上学唯物主义](../Page/形而上学唯物主义.md "wikilink")

::::\*B019.13 [辩证唯物主义](../Page/辩证唯物主义.md "wikilink")

:::\*B019.2 [唯心主义](../Page/唯心主义.md "wikilink")

:\*B02 [辩证唯物主义](../Page/辩证唯物主义.md "wikilink")

::\*B021 [物质论](../Page/物质论.md "wikilink")

:::\*B021.2 [运动论](../Page/运动论.md "wikilink")

:::\*B021.3 [时空论](../Page/时空论.md "wikilink")

:::\*B021.4 [物质运动的规律性](../Page/物质运动的规律性.md "wikilink")

::\*B022 [意识论](../Page/意识论.md "wikilink")

:::\*B022.2
[客观规律性与](../Page/客观规律性.md "wikilink")[主观能动性](../Page/主观能动性.md "wikilink")

::\*B023
[认识论](../Page/认识论.md "wikilink")、[反映论](../Page/反映论.md "wikilink")

:::\*B023.2 认识的辩证过程

:::\*B023.3 [真理论](../Page/真理论.md "wikilink")

::\*B024 [唯物辩证法](../Page/唯物辩证法.md "wikilink")

:::\*B024.1 [内因与外因](../Page/内因与外因.md "wikilink")

:::\*B024.2 [矛盾的普遍性和特殊性](../Page/矛盾.md "wikilink")

:::\*B024.3 矛盾发展的不平衡性

:::\*B024.4 矛盾的[同一性和斗争性](../Page/同一性.md "wikilink")

:::\*B024.5
[对抗性矛盾与](../Page/对抗性矛盾.md "wikilink")[非对抗性矛盾](../Page/非对抗性矛盾.md "wikilink")

:::\*B024.7
[质变与](../Page/质变.md "wikilink")[量变](../Page/量变.md "wikilink")

:::\*B024.8 [否定之否定](../Page/否定之否定.md "wikilink")

::\*B025 唯物辩证法诸范畴

:::\*B025.1
[现象与](../Page/现象.md "wikilink")[本质](../Page/本质.md "wikilink")

:::\*B025.2
[形式与](../Page/形式_\(哲學\).md "wikilink")[内容](../Page/内容.md "wikilink")

:::\*B025.3
[全局与](../Page/全局.md "wikilink")[局部](../Page/局部.md "wikilink")

:::\*B025.4 分析与综合

:::\*B025.5 原因与结果（[因果论](../Page/因果论.md "wikilink")）

:::\*B025.6 必然性与偶然性

:::\*B025.7 可能性与现实性

:::\*B025.9 其他哲学范畴

::\*B026 [方法论](../Page/方法论.md "wikilink")

::\*B027 辩证唯物主义的应用

::\*B028 [自然哲学](../Page/自然哲学.md "wikilink")

::\*B029 [自然辩证法](../Page/自然辩证法.md "wikilink")

:\*B03 [历史唯物主义](../Page/历史唯物主义.md "wikilink")

::\*B031 社会物质生活条件

::\*B032 [社会基本矛盾](../Page/社会基本矛盾.md "wikilink")

:::\*B032.1
[生产力和](../Page/生产力理论.md "wikilink")[生产关系](../Page/生产关系.md "wikilink")

:::\*B032.2
[经济基础和](../Page/经济基础.md "wikilink")[上层建筑](../Page/上层建筑.md "wikilink")

::\*B033 阶级理论

::\*B034 革命理论

::\*B035 国家理论

::\*B036
[社会存在与](../Page/社会存在.md "wikilink")[社会意识](../Page/社会意识.md "wikilink")

::\*B037 [人民内部矛盾](../Page/人民内部矛盾.md "wikilink")

::\*B038 人民在历史发展中的作用

:\*B08 哲学流派及其研究

::\*B081 [唯心主义](../Page/唯心主义.md "wikilink")

:::\*B081.1 [形而上学](../Page/形而上学.md "wikilink")

:::\*B081.2 唯心主义认识论、[先验论](../Page/先验论.md "wikilink")

::\*B082
[实证论](../Page/实证论.md "wikilink")、[经验批判主义](../Page/经验批判主义.md "wikilink")（[马赫主义](../Page/马赫主义.md "wikilink")）

::\*B083
[唯意志论](../Page/唯意志论.md "wikilink")、[生命哲学](../Page/生命哲学.md "wikilink")

::\*B084
[新康德主义](../Page/新康德主义.md "wikilink")、[新黑格尔主义](../Page/新黑格尔主义.md "wikilink")

::\*B085
[新实在论](../Page/新实在论.md "wikilink")、[逻辑实证论](../Page/逻辑实证论.md "wikilink")（[新实证论](../Page/新实证论.md "wikilink")、[逻辑经验论](../Page/逻辑经验论.md "wikilink")）

::\*B086
[存在主义](../Page/存在主义.md "wikilink")（[生存主义](../Page/生存主义.md "wikilink")）

::\*B087 [实用主义](../Page/实用主义.md "wikilink")

::\*B088
[新托马斯主义](../Page/新托马斯主义.md "wikilink")（[新经院哲学](../Page/新经院哲学.md "wikilink")）

::\*B089 其他哲学流派

:::\*B089.1 [西方马克思主义](../Page/西方马克思主义.md "wikilink")

:::\*B089.2 [哲学解释学](../Page/哲学解释学.md "wikilink")

:::\*B089.3 [哲学人类学](../Page/哲学人类学.md "wikilink")

  - B1　世界哲学

:\*B12 [古代哲学](../Page/古代哲学.md "wikilink")

:\*B13 [中世纪哲学](../Page/中世纪哲学.md "wikilink")

:\*B14 [近代哲学](../Page/近代哲学.md "wikilink")

::\*B141 十七世纪哲学

::\*B142 十八世纪哲学

::\*B143 十九世纪哲学

:\*B15 [现代哲学](../Page/近代哲學.md "wikilink")

::\*B151 二十世纪哲学

::\*B152 二十一世纪哲学

:\*B17 马克思主义哲学的传播和发展

  - B2　[中国哲学](../Page/中国哲学.md "wikilink")

:\*B20
[唯物主义和](../Page/唯物主义.md "wikilink")[唯心主义](../Page/唯心主义.md "wikilink")（总论）

::\*B201 唯物主义

::\*B202 唯心主义

:\*B21 [古代哲学](../Page/古代哲学.md "wikilink")

:\*B22 [先秦哲学](../Page/先秦哲学.md "wikilink")（～公元前221年）

::\*B221 诸子前哲学

::\*B222 [儒家](../Page/儒家.md "wikilink")

:::\*B222.1 [四书](../Page/四书.md "wikilink")

:::\*B222.2 [孔子](../Page/孔子.md "wikilink")（孔丘，公元前551\~前479年）

:::\*B222.3 [孔子弟子](../Page/孔子弟子.md "wikilink")

:::\*B222.4 [子思](../Page/子思.md "wikilink")（孔伋，公元前483\~前402年）

:::\*B222.5 [孟子](../Page/孟子.md "wikilink")（孟轲，公元前390\~前305年）

:::\*B222.6 [荀子](../Page/荀子.md "wikilink")（荀况，公元前313\~前238年）

:::\*B222.9 其他

::\*B223 [道家](../Page/道家.md "wikilink")

:::\*B223.1 [老子](../Page/老子.md "wikilink")（李耳）

:::\*B223.2 [列子](../Page/列子.md "wikilink")（列御寇）

:::\*B223.3 [杨朱](../Page/杨朱.md "wikilink")（公元前395\~前335年）

:::\*B223.4 [关尹子](../Page/关尹子.md "wikilink")

:::\*B223.5 [庄子](../Page/庄子.md "wikilink")（庄周，公元前369\~前286年）

:::\*B223.9 其他

::\*B224 [墨家](../Page/墨家.md "wikilink")

::\*B225 [名家](../Page/名家.md "wikilink")

:::\*B225.1 [邓析](../Page/邓析.md "wikilink")（公元前？\~前501年）

:::\*B225.2
[宋尹学派](../Page/宋尹学派.md "wikilink")（[宋钘](../Page/宋钘.md "wikilink")、[尹文](../Page/尹文.md "wikilink")）

:::\*B225.3 [惠施](../Page/惠施.md "wikilink")（公元前370\~前310年）

:::\*B225.4 [公孙龙](../Page/公孙龙.md "wikilink")（公元前320\~前250年）

:::\*B225.9 其他

::\*B226 [法家](../Page/法家.md "wikilink")

:::\*B226.1 [管子](../Page/管仲.md "wikilink")（管仲，公元前？\~前645年）

:::\*B226.2 [商鞅](../Page/商鞅.md "wikilink")（公孙鞅，公元前？\~前338年）

:::\*B226.3 [慎到](../Page/慎到.md "wikilink")（公元前395\~前315年）

:::\*B226.4 [申不害](../Page/申不害.md "wikilink")（公元前385\~前337年）

:::\*B226.5 [韩非](../Page/韩非.md "wikilink")（公元前280\~前233年）

:::\*B226.6 [李斯](../Page/李斯.md "wikilink")（公元前？\~前208年）

:::\*B226.9 其他

::\*B227 [阴阳家](../Page/阴阳家.md "wikilink")

::\*B228 [纵横家](../Page/纵横家.md "wikilink")

:::\*B228.1 [苏秦](../Page/苏秦.md "wikilink")

:::\*B228.2 [张仪](../Page/张仪.md "wikilink")

::\*B229 [杂家](../Page/杂家.md "wikilink")

:::\*B229.1 [尸子](../Page/尸子.md "wikilink")（尸佼，公元前390\~前330年）

:::\*B229.2 [吕不韦](../Page/吕不韦.md "wikilink")（公元前？\~前235年）

:::\*B229.3 [孔鲋](../Page/孔鲋.md "wikilink")（孔甲，公元前264\~前208年）

:::\*B229.9 其他

:\*B23 秦汉、三国、晋、南北朝哲学（公元前221年－公元589年）

::\*B232 [秦汉哲学](../Page/秦汉哲学.md "wikilink")（总论）（公元前221\~公元220年）

::\*B233 秦代哲学（公元前221\~前207年）

::\*B234 汉代哲学（公元前206\~公元220年）

:::\*B234.1 [陆贾](../Page/陆贾.md "wikilink")

:::\*B234.2 [贾谊](../Page/贾谊.md "wikilink")（公元前200\~前168年）

:::\*B234.4 [淮南子](../Page/淮南子.md "wikilink")（刘安，公元前179\~前122年）

:::\*B234.5 [董仲舒](../Page/董仲舒.md "wikilink")（公元前179\~前104年）

:::\*B234.7 [桓谭](../Page/桓谭.md "wikilink")（？－56年）

:::\*B234.8 [王充](../Page/王充.md "wikilink")（27－97年）

:::\*B234.93 [王符](../Page/王符.md "wikilink")

:::\*B234.94 [荀悦](../Page/荀悦.md "wikilink")（148－209年）

:::\*B234.99 其他

::\*B235 三国、晋、南北朝哲学（220－589年）

:::\*B235.1 [何晏](../Page/何晏.md "wikilink")（190－249年）

:::\*B235.2 [王弼](../Page/王弼_\(三國\).md "wikilink")（226－249年）

:::\*B235.3 [嵇康](../Page/嵇康.md "wikilink")（224－263年）

:::\*B235.4 [杨泉](../Page/杨泉.md "wikilink")

:::\*B235.5 [裴頠](../Page/裴頠.md "wikilink")（267－300年）

:::\*B235.6 [郭象](../Page/郭象.md "wikilink")（252－312年）

:::\*B235.7
[抱朴子](../Page/抱朴子.md "wikilink")（[葛洪](../Page/葛洪.md "wikilink")，约284－364年）

:::\*B235.8 [范缜](../Page/范缜.md "wikilink")（约450－510年）

:::\*B235.9 其他

:\*B24 隋、唐、宋、元、明、清哲学（589年－1840年）

::\*B241 隋、唐、五代哲学（581－960年）

:::\*B241.1 [文中子](../Page/文中子.md "wikilink")（王通，584－617年）

:::\*B241.2 [吕才](../Page/呂才.md "wikilink")（600－665年）

:::\*B241.6 [李翱](../Page/李翱.md "wikilink")（772－841年）

:::\*B241.7 [刘禹锡](../Page/刘禹锡.md "wikilink")（772－842年）

:::\*B241.8 [谭峭](../Page/谭峭.md "wikilink")

:::\*B241.9 其他

::\*B244 宋、元哲学（960－1368年）

:::\*B244.1 [李觏](../Page/李覯.md "wikilink")（1009－1059年）

:::\*B244.2 [周敦颐](../Page/周敦颐.md "wikilink")（濂溪，1017－1073年）

:::\*B244.3 [邵雍](../Page/邵雍.md "wikilink")（康节，1011－1077年）

:::\*B244.4 [张载](../Page/张载.md "wikilink")（横渠，1020－1077年）

:::\*B244.5 [王安石](../Page/王安石.md "wikilink")（1021－1086年）

:::\*B244.6
[程颢](../Page/程颢.md "wikilink")（明道，1032－1085年）、[程颐](../Page/程颐.md "wikilink")（伊川，1033－1107年）及[程朱理学](../Page/程朱理学.md "wikilink")

:::\*B244.7
[朱熹](../Page/朱熹.md "wikilink")（1130－1200年）及[考亭学派](../Page/考亭学派.md "wikilink")

:::\*B244.8 [陆九渊](../Page/陆九渊.md "wikilink")（象山，1139－1193年）及其学派

:::\*B244.91
[陈亮](../Page/陈亮.md "wikilink")（1143－1194年）及[永康学派](../Page/永康学派.md "wikilink")

:::\*B244.92
[叶适](../Page/叶适.md "wikilink")（1150－1223年）及[永嘉学派](../Page/永嘉学派.md "wikilink")

:::\*B244.93 [邓牧](../Page/邓牧.md "wikilink")（1247－1306年）B244.99 其他

::\*B248 明代哲学（1368－1644年）

:::\*B248.1 [陈献章](../Page/陈献章.md "wikilink")（1428－1500年）

:::\*B248.2
[王守仁](../Page/王守仁.md "wikilink")（王阳明，1472－1528年）及[陆王学派](../Page/陆王学派.md "wikilink")

:::\*B248.3
[王艮](../Page/王艮_\(泰州\).md "wikilink")（1483－1541年）及[泰州学派](../Page/泰州学派.md "wikilink")

:::\*B248.4 [王廷相](../Page/王廷相.md "wikilink")（1474－1544年）

:::\*B248.5 [罗钦顺](../Page/罗钦顺.md "wikilink")（1465－1547年）

:::\*B248.6 [黄绾](../Page/黃綰.md "wikilink")（1477－1551年）

:::\*B248.7 [何心隐](../Page/何心隱.md "wikilink")（1517－1579年）

:::\*B248.91 [李贽](../Page/李贽.md "wikilink")（1527－1602年）

:::\*B248.92 [吕坤](../Page/吕坤.md "wikilink")（1536－1618年）

:::\*B248.93 [方以智](../Page/方以智.md "wikilink")（1611－1671年）

:::\*B248.99 其他

::\*B249 清代哲学(1644\~1840年)

:::\*B249.1 [顾炎武](../Page/顾炎武.md "wikilink")（1613－1682年）

:::\*B249.2 [王夫之](../Page/王夫之.md "wikilink")（船山，1619－1692年）

:::\*B249.3 [黄宗羲](../Page/黄宗羲.md "wikilink")（梨州，1610－1695年）

:::\*B249.4 [唐甄](../Page/唐甄.md "wikilink")（1630－1704年）

:::\*B249.5
[颜元](../Page/颜元.md "wikilink")（习斋，1635－1704年）、[李塨](../Page/李塨.md "wikilink")（恕谷，1659－1733年）及[颜李学派](../Page/颜李学派.md "wikilink")

:::\*B249.6 [戴震](../Page/戴震.md "wikilink")（东原，1723－1777年）

:::\*B249.7 [章学诚](../Page/章学诚.md "wikilink")（实斋，1738－1801年）

:::\*B249.8 [焦循](../Page/焦循.md "wikilink")（理堂，1763－1820年）

:\*B25 近代哲学（1840年－1916年）

::\*B251 [龚自珍](../Page/龚自珍.md "wikilink")（1792－1841年）

::\*B252 [魏源](../Page/魏源_\(清朝\).md "wikilink")（1794－1857年）

::\*B254 [谭嗣同](../Page/谭嗣同.md "wikilink")（1865－1898年）

::\*B256 [严复](../Page/严复.md "wikilink")（1853－1921年）

::\*B258 [康有为](../Page/康有为.md "wikilink")（1858－1927年）

::\*B259.1 [梁启超](../Page/梁启超.md "wikilink")（1873－1929年）

::\*B259.2 [章炳麟](../Page/章炳麟.md "wikilink")（1869－1936年）

:\*B26 [现代哲学](../Page/现代哲学.md "wikilink")（1919年－）

::\*B261 二十世纪哲学

::\*B262 二十一世纪哲学

:\*B27 [马克思主义哲学在中国的传播与发展](../Page/马克思主义哲学.md "wikilink")

  - B3　[亚洲哲学](../Page/亚洲哲学.md "wikilink")

::\*B302 古代哲学

::\*B303 中世纪哲学

::\*B304 近代哲学

::\*B305 现代哲学

::\*B307 马克思主义哲学在亚洲的传播与发展

::\*B312 [朝鲜哲学](../Page/朝鲜哲学.md "wikilink")

::\*B313 [日本哲学](../Page/日本哲学.md "wikilink")

:::\*B313.2 古代哲学

:::\*B313.3 封建时代哲学

:::\*B313.4 明治时代哲学

:::\*B313.5 现代哲学

:::\*B313.7 马克思主义哲学在日本的传播与发展

::\*B333 [越南哲学](../Page/越南哲学.md "wikilink")

::\*B351 [印度哲学](../Page/印度哲学.md "wikilink")

:::\*B351.2 古代哲学

:::\*B351.3 中古时期哲学

:::\*B351.4 近代哲学（1757－1947年）

:::\*B351.5 现代哲学

:::\*B351.7 马克思主义哲学在印度的传播与发展

::\*B371 [阿拉伯哲学](../Page/阿拉伯哲学.md "wikilink")（总论）

::\*B373
[伊朗哲学](../Page/伊朗哲学.md "wikilink")（[波斯哲学](../Page/波斯哲学.md "wikilink")）

::\*B382 [以色列哲学](../Page/以色列哲学.md "wikilink")

  - B4　[非洲哲学](../Page/非洲哲学.md "wikilink")

:\*B41 [北非哲学](../Page/北非哲学.md "wikilink")

  - B5　[欧洲哲学](../Page/欧洲哲学.md "wikilink")

::\*B502 古代哲学

:::\*B502.1 希腊奴隶制形成时期（公元前7\~前6世纪）

::::\*B502.11 [爱奥尼亚学派](../Page/爱奥尼亚学派.md "wikilink")

::::\*B502.12 [米利都学派](../Page/米利都学派.md "wikilink")

:::::\*B502.121 [泰勒斯](../Page/泰勒斯.md "wikilink")（Thales，约公元前624\~前547年）

:::::\*B502.122
[阿那克西曼德](../Page/阿那克西曼德.md "wikilink")（Anaxi-mandros，约公元前610\~前546年）

:::::\*B502.123
[阿那克西美尼](../Page/阿那克西美尼.md "wikilink")（Anaxime-nes，公元前588\~前525年）

::::\*B502.13 [爱非斯学派](../Page/爱非斯学派.md "wikilink")

::::\*B502.14 [毕达哥拉斯学派](../Page/毕达哥拉斯学派.md "wikilink")

::::\*B502.15 [埃利亚学派](../Page/埃利亚学派.md "wikilink")

:::::\*B502.151
[色诺芬尼](../Page/色诺芬尼.md "wikilink")（Xenophanes，公元前565\~前473年）

:::::\*B502.152 [巴门尼德](../Page/巴门尼德.md "wikilink")（Parmenides，公元前6世纪末）

:::::\*B502.153 [芝诺](../Page/埃利亚的芝诺.md "wikilink")（Zenon，公元前490\~前436年）

::::\*B502.19 其他

:::\*B502.2 希腊奴隶主民主制繁荣和衰落时期（公元前5\~前4世纪）

::::\*B502.21 古希腊唯物论哲学学派

:::::\*B502.211
[阿那克萨哥拉](../Page/阿那克萨哥拉.md "wikilink")（Anaxagoras，公元前500\~前428年）

:::::\*B502.212
[恩培多克勒](../Page/恩培多克勒.md "wikilink")（Empedok-les，公元前490\~前430年）

:::::\*B502.213
[德谟克利特](../Page/德谟克利特.md "wikilink")（Demokritos，约公元前460\~前370年）

::::\*B502.22
[智者派](../Page/智者派.md "wikilink")（[诡辩派](../Page/诡辩派.md "wikilink")）

::::\*B502.23 唯心论哲学学派

:::::\*B502.231
[苏格拉底](../Page/苏格拉底.md "wikilink")（Sokrates，公元前469\~前399年）

:::::\*B502.232 [柏拉图](../Page/柏拉图.md "wikilink")（Platon，公元前427\~前347年）

:::::\*B502.233
[亚里士多德](../Page/亚里士多德.md "wikilink")（Aristoteles，公元前384\~前322年）

::::\*B502.24 [麦加拉学派](../Page/麦加拉学派.md "wikilink")

::::\*B502.25 [犬儒学派](../Page/犬儒学派.md "wikilink")

::::\*B502.26
[昔勒尼学派](../Page/昔勒尼学派.md "wikilink")（[克兰尼学派](../Page/克兰尼学派.md "wikilink")）

::::\*B502.29 其他

:::\*B502.3 希腊奴隶制危机和衰落时期（公元前336\~前30年）

::::\*B502.31 [伊壁鸠鲁及其学派](../Page/伊壁鸠鲁.md "wikilink")

::::\*B502.32
[斯多亚派](../Page/斯多亚派.md "wikilink")（[画廊派](../Page/画廊派.md "wikilink")）

::::\*B502.33 [怀疑论派](../Page/怀疑论派.md "wikilink")

::::\*B502.39 其他

:::\*B502.4 [古罗马哲学](../Page/古罗马哲学.md "wikilink")

::::\*B502.41 唯物论

::::\*B502.42 [折衷主义](../Page/折衷主义.md "wikilink")

::::\*B502.43 [新斯多亚派](../Page/新斯多亚派.md "wikilink")

::::\*B502.44 [新柏拉图主义](../Page/新柏拉图主义.md "wikilink")

::::\*B502.49 其他

::\*B503 中世纪哲学

:::\*B503.1 [教父哲学](../Page/教父哲学.md "wikilink")

:::\*B503.2 [经院哲学](../Page/经院哲学.md "wikilink")

::::\*B503.21
[托马斯·阿奎那](../Page/托马斯·阿奎那.md "wikilink")（Thomas,Aquinas,1225－1274年）

::::\*B503.22 [安瑟伦](../Page/安瑟伦.md "wikilink")（Anselm,1033－1109年）

::::\*B503.23
[邓斯·司各脱](../Page/邓斯·司各脱.md "wikilink")（DunsScotus,Jahahnes,1265－1308年）

::::\*B503.24
[奥卡姆](../Page/奥卡姆.md "wikilink")（Occam,Williamof,1300－1350年）

:::\*B503.3 [神秘主义](../Page/神秘主义.md "wikilink")

::::\*B503.31
[爱克哈特](../Page/爱克哈特.md "wikilink")（Eckhart,M.J.约1260－1327年）

::::\*B503.32 [亚克利巴](../Page/亚克利巴.md "wikilink")（Agrippa,1486－1535年）

::::\*B503.33 [魏格尔](../Page/魏格尔.md "wikilink")（Weigel,V.1533－1588年）

:::\*B503.9 资本主义产生时期（文艺复兴时期，14－16世纪）哲学

::::\*B503.91 [人文主义](../Page/人文主义.md "wikilink")

:::::\*B503.911
[佩脱拉克](../Page/佩脱拉克.md "wikilink")（Petrurca,F.1304－1374年）

:::::\*B503.912
[薄伽丘](../Page/薄伽丘.md "wikilink")（Boccaccio,Gio-vanni,1313－1375年）

:::::\*B503.913
[彭波那齐](../Page/彭波那齐.md "wikilink")（Pomponnazzi,Pietro,1462－1525年）

:::::\*B503.914
[爱拉斯谟](../Page/爱拉斯谟.md "wikilink")（Erasmus,Desi-derius,1465－1536年）

:::::\*B503.915
[蒙台涅](../Page/蒙台涅.md "wikilink")（Montaigne,M.E.de,1533－1592年）

:::::\*B503.916 [斐未斯](../Page/斐未斯.md "wikilink")（Vives,Louis,1492－1540年）

::::\*B503.92 科学和自然哲学

:::::\*B503.921
[尼古拉](../Page/尼古拉.md "wikilink")（库萨的）（Nicolaus,Cusanus,1401－1464年）

:::::\*B503.922
[伽利略](../Page/伽利略.md "wikilink")（Galileo,Galilei,1564－1642年）

:::::\*B503.923
[布鲁诺](../Page/布鲁诺.md "wikilink")（Bruno,Giordano,1548－1600年）

::::\*B503.99 其他

::\*B504 十七\~十九世纪前期哲学

::\*B505 十九世纪后期\~二十世纪哲学

::\*B506 二十一世纪哲学

::\*B507 马克思主义哲学在欧洲的传播与发展

:::\*B511.2 [俄罗斯哲学](../Page/俄罗斯哲学.md "wikilink")

::\*B512 俄国及苏联（1917－1991年）哲学

:::\*B512.3 十八世纪及其以前哲学

::::\*B512.31
[罗蒙诺索夫](../Page/罗蒙诺索夫.md "wikilink")（Ломоносов,М.В.1711－1765年）

::::\*B512.32 [拉吉舍夫](../Page/拉吉舍夫.md "wikilink")（Раднщев,А.Н.1749－1802年）

::::\*B512.39 其他

:::\*B512.4 十九世纪哲学

::::\*B512.41
[别林斯基](../Page/别林斯基.md "wikilink")（Белинcкий,В.Г.1811－1848年）

::::\*B512.42 [赫尔岑](../Page/赫尔岑.md "wikilink")（Герцен,А.И.1812－1870年）

::::\*B512.43 [奥格辽夫](../Page/奥格辽夫.md "wikilink")（Огарёв,Н.П.1813－1877年）

::::\*B512.44
[车尔尼雪夫斯基](../Page/车尔尼雪夫斯基.md "wikilink")（Черныщевский,Н.Г.1828－1889年）

::::\*B512.45
[杜勃罗留勃夫](../Page/杜勃罗留勃夫.md "wikilink")（Доброл-юбов,Н.А.1836－1861年）

::::\*B512.46 [皮萨列夫](../Page/皮萨列夫.md "wikilink")（Писарев,Д.И.1840－1868年）

::::\*B512.49 其他

:::\*B512.5 十九世纪后期至二十世纪哲学

::::\*B512.51 [巴枯宁](../Page/巴枯宁.md "wikilink")（Бакунин,М.А.1814－1876年）

::::\*B512.52 [拉甫罗夫](../Page/拉甫罗夫.md "wikilink")（Лавров,П.Л.1823－1900年）

::::\*B512.53 [特卡切夫](../Page/特卡切夫.md "wikilink")（Ткачев,П.Н.1844－1885年）

::::\*B512.54
[普列汉诺夫](../Page/普列汉诺夫.md "wikilink")（Плеханов,Г.В.1856－1918年）

::::\*B512.55
[波格丹诺夫](../Page/波格丹诺夫.md "wikilink")（Богданов,А.А.1873－1928年）

::::\*B512.59 其他

:::\*B512.6 二十一世纪哲学

:::\*B512.7 马克思主义哲学的传播与发展

::\*B516 [德国哲学](../Page/德国哲学.md "wikilink")

:::\*B516.2 十七世纪哲学

::::\*B516.21 [伯麦](../Page/伯麦.md "wikilink")（Bohme,Jakob.1575－1624年）

::::\*B516.22 [莱布尼兹](../Page/莱布尼兹.md "wikilink")（Leibniz,G.W.1646－1716年）

::::\*B516.29 其他

:::\*B516.3 十八世纪\~十九世纪前期哲学

::::\*B516.31 [康德](../Page/伊曼努尔·康德.md "wikilink")（Kant,I.1724－1804年）

::::\*B516.32 [福尔斯特](../Page/福尔斯特.md "wikilink")（Forster,G.1754－1794年）

::::\*B516.33 [费希特](../Page/费希特.md "wikilink")（Fichte,J.G.1762－1814年）

::::\*B516.34 [谢林](../Page/谢林.md "wikilink")（Schelling,F.W.J.1775－1854年）

::::\*B516.35 [黑格尔](../Page/黑格尔.md "wikilink")（Hegel,G.W.F.1770－1831年）

::::\*B516.36
[费尔巴哈](../Page/费尔巴哈.md "wikilink")（Feuerbach,L.A.1804－1872年）

::::\*B516.39 其他

:::\*B516.4 十九世纪后期哲学

::::\*B516.41
[叔本华](../Page/叔本华.md "wikilink")（Schopenhauer,A.1788－1860年）

::::\*B516.42 [赫尔巴特](../Page/赫尔巴特.md "wikilink")（Herbart,J.F.1776－1841年）

::::\*B516.43 [洛兹](../Page/洛兹.md "wikilink")（Lotze,R.H.1817－1881年）

::::\*B516.44 [海克尔](../Page/海克尔.md "wikilink")（Haeckel,E.H.1834－1919年）

::::\*B516.45 [冯德](../Page/冯德.md "wikilink")（Wundt,W.1832－1920年）

::::\*B516.46 [哈特曼](../Page/哈特曼.md "wikilink")（Hartmann,E.1842－1906年）

::::\*B516.47 [尼采](../Page/尼采.md "wikilink")（Nietzsche,F.1844－1900年）

::::\*B516.49 其他

:::\*B516.5 二十世纪哲学

::::\*B516.51 [倭铿](../Page/倭铿.md "wikilink")（Eucken,R.C.1846－1926年）

::::\*B516.52 [胡塞尔](../Page/胡塞尔.md "wikilink")（Husserl,E.1859－1938年）

::::\*B516.53 [雅斯贝尔斯](../Page/雅斯贝尔斯.md "wikilink")（Jaspers,K.1883－1969年）

::::\*B516.54 [海德格尔](../Page/海德格尔.md "wikilink")（Heidegger,M.1889－1976年）

::::\*B516.59 其他

:::\*B516.6 二十一世纪哲学

:::\*B516.7 马克思主义哲学在德国的产生和发展

::\*B517 [德意志民主共和国哲学](../Page/德意志民主共和国哲学.md "wikilink")（1945－1990年）

::\*B518 [德意志联邦共和国哲学](../Page/德意志联邦共和国哲学.md "wikilink")（1945－1990年）

::\*B521 [奥地利哲学](../Page/奥地利哲学.md "wikilink")

::\*B534 [丹麦哲学](../Page/丹麦哲学.md "wikilink")

::\*B541 [阿尔巴尼亚哲学](../Page/阿尔巴尼亚哲学.md "wikilink")

::\*B546 [意大利哲学](../Page/意大利哲学.md "wikilink")

::\*B551 [西班牙哲学](../Page/西班牙哲学.md "wikilink")

::\*B561 [英国哲学](../Page/英国哲学.md "wikilink")

:::\*B561.2 十七、十八世纪哲学

::::\*B561.21 [培根](../Page/培根.md "wikilink")（Bacon,F.1561－1626年）

::::\*B561.22 [霍布斯](../Page/霍布斯.md "wikilink")（Hobbes,T.1588－1679年）

::::\*B561.23 [克德沃斯](../Page/克德沃斯.md "wikilink")（Cudworth,R.1617－1688年）

::::\*B561.24 [洛克](../Page/洛克.md "wikilink")（Locke,J.1632－1704年）

::::\*B561.25 [托兰德](../Page/托兰德.md "wikilink")（Toland,John,1670－1722年）

::::\*B561.26
[柯林斯](../Page/柯林斯.md "wikilink")（Collins,Anthony,1676－1729年）

::::\*B561.27 [贝克莱](../Page/贝克莱.md "wikilink")（Berkeley,G.1684－1753年）

::::\*B561.28 [李德](../Page/李德.md "wikilink")（Reid,T.1710－1796年）

::::\*B561.291 [休谟](../Page/休谟.md "wikilink")（Hume,D.1711－1776年）

::::\*B561.299 其他

:::\*B561.4 十九世纪哲学

::::\*B561.41 [边沁](../Page/边沁.md "wikilink")（Bentham,J.1748－1832年）

::::\*B561.42
[约翰·斯图尔特·密尔](../Page/约翰·斯图尔特·密尔.md "wikilink")（Mill,J.S.1806－1873年）

::::\*B561.43
[赫胥黎](../Page/托马斯·亨利·赫胥黎.md "wikilink")（Huxley,T.H.1825－1895年）

::::\*B561.44 [格林](../Page/格林.md "wikilink")（Green,T.H.1836－1882年）

::::\*B561.45
[赫伯特·斯宾塞](../Page/赫伯特·斯宾塞.md "wikilink")（Spencer,H.1820－1903年）

::::\*B561.46 [布拉德莱](../Page/布拉德莱.md "wikilink")（Bradley,F.H.1846－1924年）

::::\*B561.47 [鲍桑葵](../Page/鲍桑葵.md "wikilink")（Bosanguet,B.1848－1923年）

::::\*B561.49 其他

:::\*B561.5 二十世纪哲学

::::\*B561.51 [亚历山大](../Page/亚历山大.md "wikilink")（Alexander,S.1859－1938年）

::::\*B561.52 [怀特海](../Page/怀特海.md "wikilink")（Whitehead,A.N.1861－1947年）

::::\*B561.53
[麦克特](../Page/麦克特.md "wikilink")（Mctaggart,J.M.E.1866－1925年）

::::\*B561.54
[伯特兰·罗素](../Page/伯特兰·罗素.md "wikilink")（Russell,B.1872－1970年）

::::\*B561.55 [穆尔](../Page/穆尔.md "wikilink")（Moore,G.E.1873－1958年）

::::\*B561.59 其他

:::\*B561.6 二十一世纪哲学

:::\*B561.7 马克思主义哲学在英国的传播与发展

::\*B563 [荷兰哲学](../Page/荷兰哲学.md "wikilink")

:::\*B563.1 [斯宾诺莎](../Page/斯宾诺莎.md "wikilink")（Spinoza,B.1632－1677年）

:::\*B563.2 [古林克斯](../Page/古林克斯.md "wikilink")（Geulincx,A.1625－1669年）

:::\*B563.9 其他

::\*B565 [法国哲学](../Page/法国哲学.md "wikilink")

:::\*B565.2 十七、十八世纪哲学

::::\*B565.21 [笛卡儿](../Page/笛卡儿.md "wikilink")（Descartes,R.1596－1650年）

::::\*B565.22 [伽桑狄](../Page/伽桑狄.md "wikilink")（Gassendi,P.1592－1655年）

::::\*B565.23 [帕斯卡](../Page/帕斯卡.md "wikilink")（Pascal,B.1623－1662年）

::::\*B565.24 [孟德斯鸠](../Page/孟德斯鸠.md "wikilink")（Montesquieu,C.L.de
S.1689－1755年）

::::\*B565.25 [伏尔泰](../Page/伏尔泰.md "wikilink")（Voltaire,1694－1778年）

::::\*B565.26
[卢梭](../Page/让-雅克·卢梭.md "wikilink")（Rousseau,J.J.1712－1778年）

::::\*B565.27 [拉·美特利](../Page/朱利安·奥弗雷·拉·美特利.md "wikilink")（La
Mettrie,J.O.de1709－1751年）

::::\*B565.28 [狄德罗](../Page/狄德罗.md "wikilink")（Diderot,D.1713－1784年）

::::\*B565.291
[爱尔维修](../Page/爱尔维修.md "wikilink")（Helvétius,C.A.1715－1771年）

::::\*B565.292
[孔狄亚克](../Page/埃蒂耶纳·博诺·德·孔狄亚克.md "wikilink")（Condillac,E.B.de1715－1780年）

::::\*B565.293
[达兰贝尔](../Page/达朗贝尔.md "wikilink")（D'Alembert,J.LeR.1717－1783年）

::::\*B565.294
[霍尔巴赫](../Page/霍尔巴赫.md "wikilink")（Holbach,P.H.D.1723－1789年）

::::\*B565.299 其他

:::\*B565.4 十九世纪哲学

::::\*B565.41 [库然](../Page/库然.md "wikilink")（Cousin,V.1792－1867年）

::::\*B565.42
[奥古斯特·孔德](../Page/奥古斯特·孔德.md "wikilink")（Comte,A.1798－1857年）

::::\*B565.49 其他

:::\*B565.5 二十世纪哲学

::::\*B565.51 [柏格森](../Page/柏格森.md "wikilink")（Bergson,H.1859－1941年）

::::\*B565.52 [马利丹](../Page/马利丹.md "wikilink")（Maritain,J.1882－1973年）

::::\*B565.53
[萨特尔](../Page/让-保罗·萨特.md "wikilink")（Sartre,J.P.1905－1980年）

::::\*B565.59 其他

:::\*B565.6 二十一世纪哲学

:::\*B565.7 马克思主义哲学在法国的传播与发展

  - B6　[大洋洲哲学](../Page/大洋洲哲学.md "wikilink")
  - B7　[美洲哲学](../Page/美洲哲学.md "wikilink")

:\*B71 [北美洲哲学](../Page/北美洲哲学.md "wikilink")

::\*B712 [美国哲学](../Page/美国哲学.md "wikilink")

:::\*B712.3 十八世纪\~十九世纪前期哲学

::::\*B712.31
[富兰克林](../Page/本傑明·富蘭克林.md "wikilink")（Franklin,B.1706－1790年）

::::\*B712.32
[普利斯特莱](../Page/约瑟夫·普利斯特里.md "wikilink")（Priestley,J.1733－1804年）

::::\*B712.33 [库柏](../Page/库柏.md "wikilink")（Cooper,T.1759－1840年）

::::\*B712.39 其他

:::\*B712.4 十九世纪后期哲学

::::\*B712.41 [爱默生](../Page/爱默生.md "wikilink")（Emerson,R.W.1803－1882年）

::::\*B712.42 [哈利斯](../Page/哈利斯.md "wikilink")（Harris,W.T.1835－1909年）

::::\*B712.43 [皮尔斯](../Page/皮尔斯.md "wikilink")（Peirce,C.S.1839－1914年）

::::\*B712.44 [詹姆斯](../Page/詹姆斯.md "wikilink")（James,W.1842－1910年）

::::\*B712.45 [波温](../Page/波温.md "wikilink")（Bowne,B.P.1847－1910年）

::::\*B712.46 [罗伊斯](../Page/罗伊斯.md "wikilink")（Royce,J.1855－1916年）

::::\*B712.49 其他

:::\*B712.5 二十世纪哲学

::::\*B712.51 [杜威](../Page/约翰·杜威.md "wikilink")（Dewey,J.1859－1952年）

::::\*B712.52 [桑塔亚那](../Page/桑塔亚那.md "wikilink")（Santayana,G.1863－1952年）

::::\*B712.59 其他

:::\*B712.6 二十一世纪哲学

:::\*B712.7 马克思主义哲学在美国的传播与发展

:\*B80　[思维科学](../Page/思维科学.md "wikilink")（总论）

::\*B80-0 思维科学理论与方法论

:::\*B80-05 思维科学与其他科学的关系

::\*B802 [思维规律](../Page/思维规律.md "wikilink")

::\*B804 [思维方式](../Page/思维方式.md "wikilink")

:::\*B804.1 [抽象思维](../Page/抽象思维.md "wikilink")

:::\*B804.2 [形象思维](../Page/形象思维.md "wikilink")

:::\*B804.3 [灵感思维](../Page/灵感思维.md "wikilink")

:::\*B804.4 [创造性思维](../Page/创造性思维.md "wikilink")

:\*B81　[逻辑学](../Page/逻辑学.md "wikilink")（[论理学](../Page/论理学.md "wikilink")）

::\*B81-0 逻辑学理论与方法论

:::\*B81-05 逻辑学与其他学科的关系

:::\*B81-06 逻辑学流派及其研究

:::\*B81-09
[逻辑学史](../Page/逻辑学史.md "wikilink")、[逻辑思想史](../Page/逻辑思想史.md "wikilink")

::::\*B81-092 [中国逻辑学史](../Page/中国逻辑学史.md "wikilink")

::::::\*B81-093.51 [印度因明学史](../Page/印度因明学史.md "wikilink")

::::::\*B81-093.71 [阿拉伯逻辑学史](../Page/阿拉伯逻辑学史.md "wikilink")

::::\*B81-095 [西方逻辑学史](../Page/西方逻辑学史.md "wikilink")

::\*B811 [辩证逻辑](../Page/辩证逻辑.md "wikilink")

:::\*B811.0 基础理论

::::\*B811.01 黑格尔的逻辑

::::\*B811.02 马克思主义的辩证逻辑

::::\*B811.04 [客观与主观逻辑](../Page/客观与主观逻辑.md "wikilink")

::::\*B811.05 [知性与理性逻辑](../Page/知性与理性逻辑.md "wikilink")

::::\*B811.07 [辩证思维](../Page/辩证思维.md "wikilink")

:::\*B811.1 基本规律

:::\*B811.2 思维形式与方法

::::\*B811.21
[概念](../Page/概念.md "wikilink")、[范畴](../Page/范畴_\(哲学\).md "wikilink")

::::\*B811.22 [辩证判断](../Page/辩证判断.md "wikilink")

::::\*B811.23 [辩证推理](../Page/辩证推理.md "wikilink")

::::\*B811.24 [辩证思维方法](../Page/辩证思维方法.md "wikilink")

::\*B812
[形式逻辑](../Page/形式逻辑.md "wikilink")（[名学](../Page/名学.md "wikilink")、[辩学](../Page/辩学.md "wikilink")）

:::\*B812.1 基本规律

:::\*B812.2 思维形式和方法

::::\*B812.21 概念、范畴

::::\*B812.22
[判断](../Page/判断.md "wikilink")、[命题](../Page/命题.md "wikilink")

::::\*B812.23
[演绎推理](../Page/演绎推理.md "wikilink")、[三段论推理](../Page/三段论推理.md "wikilink")

:::\*B812.3
[归纳推理](../Page/归纳推理.md "wikilink")（[归纳法](../Page/归纳法.md "wikilink")）

:::\*B812.4
[论证与](../Page/论证.md "wikilink")[反驳](../Page/反驳.md "wikilink")

:::\*B812.5
[谬误与](../Page/谬误.md "wikilink")[诡辩](../Page/诡辩.md "wikilink")

::\*B813
[数理逻辑](../Page/数理逻辑.md "wikilink")（[符号逻辑](../Page/符号逻辑.md "wikilink")）

::\*B814 [概率逻辑](../Page/概率逻辑.md "wikilink")

::\*B815
[哲理逻辑](../Page/哲理逻辑.md "wikilink")（[非经典逻辑](../Page/非经典逻辑.md "wikilink")）

:::\*B815.1 [模态逻辑](../Page/模态逻辑.md "wikilink")

:::\*B815.2 [多值逻辑](../Page/多值逻辑.md "wikilink")

:::\*B815.3 [认知逻辑](../Page/认知逻辑.md "wikilink")

:::\*B815.4 [价值逻辑](../Page/价值逻辑.md "wikilink")

:::\*B815.5 [时态逻辑](../Page/时态逻辑.md "wikilink")

:::\*B815.6 [模糊逻辑](../Page/模糊逻辑.md "wikilink")

:::\*B815.7
[相干与](../Page/相干.md "wikilink")[衍推逻辑](../Page/衍推逻辑.md "wikilink")

:::\*B815.8 [自由逻辑](../Page/自由逻辑.md "wikilink")

:::\*B815.9 其他

::\*B819 [应用逻辑](../Page/应用逻辑.md "wikilink")

:\*B82　[伦理学](../Page/伦理学.md "wikilink")

::\*B82-0 伦理学理论与方法论

:::\*B82-02 伦理学的哲学基础

:::\*B82-05 伦理学与其他科学的关系

::::\*B82-051 [道德与政治](../Page/道德.md "wikilink")、道德与法制

::::\*B82-052 道德与社会

::::\*B82-053 道德与经济

::::\*B82-054 道德与心理

:::::\*B82-054.9 道德与美学

::::\*B82-055 道德与宗教

:::::\*B82-055.9 道德与语言

::::\*B82-056 道德与文艺

::::\*B82-057 道德与科学技术

::::\*B82-058 道德与环境

::::\*B82-059 其他

:::\*B82-06 伦理学流派及其研究

::::\*B82-061 [人道主义](../Page/人道主义.md "wikilink")

::::\*B82-062
[享乐主义](../Page/享乐主义.md "wikilink")、[利己主义](../Page/利己主义.md "wikilink")

::::\*B82-063 [人格主义](../Page/人格主义.md "wikilink")

::::\*B82-064
[功利主义](../Page/功利主义.md "wikilink")、[实用主义](../Page/实用主义.md "wikilink")

::::\*B82-065
[直观主义](../Page/直观主义.md "wikilink")（[直觉主义](../Page/直觉主义.md "wikilink")）

::::\*B82-066
[元伦理学](../Page/元伦理学.md "wikilink")（[分析伦理学](../Page/分析伦理学.md "wikilink")）

::::\*B82-067 [规范伦理学](../Page/规范伦理学.md "wikilink")

::::\*B82-069 其他

:::\*B82-09 [伦理学史](../Page/伦理学史.md "wikilink")

::\*B821
[人生观](../Page/人生观.md "wikilink")、[人生哲学](../Page/人生哲学.md "wikilink")

:::\*B821.1 [共产主义人生观](../Page/共产主义人生观.md "wikilink")

:::\*B821.2 非共产主义人生观

::\*B822 [国家道德](../Page/国家道德.md "wikilink")

:::\*B822.1 [国民公德](../Page/国民公德.md "wikilink")

:::\*B822.2 [集体公德](../Page/集体公德.md "wikilink")

:::\*B822.9
[职业道德](../Page/职业道德.md "wikilink")（[工作道德](../Page/工作道德.md "wikilink")）

::::\*B822.98 [专业工作道德](../Page/专业工作道德.md "wikilink")

::\*B823
[家庭](../Page/家庭.md "wikilink")、[婚姻道德](../Page/婚姻道德.md "wikilink")

:::\*B823.1 [家庭道德](../Page/家庭道德.md "wikilink")

:::\*B823.2 [婚姻道德](../Page/婚姻道德.md "wikilink")

:::\*B823.3 [恋爱道德](../Page/恋爱道德.md "wikilink")

:::\*B823.4
[性道德](../Page/性道德.md "wikilink")、[生育道德](../Page/生育道德.md "wikilink")

::\*B824 [社会公德](../Page/社会公德.md "wikilink")

:::\*B824.2
[友谊与](../Page/友谊.md "wikilink")[同志关系](../Page/革命同志.md "wikilink")

:::\*B824.3
[公共秩序及](../Page/公共秩序.md "wikilink")[纪律](../Page/纪律.md "wikilink")

:::\*B824.5 [社会风尚](../Page/社会风尚.md "wikilink")

::\*B825 [个人修养](../Page/个人修养.md "wikilink")

::\*B829 其他[伦理规范](../Page/伦理.md "wikilink")

:\*B83　[美学](../Page/美学.md "wikilink")

::\*B83-0 美学理论

:::\*B83-02 [美学哲学基础](../Page/美学哲学.md "wikilink")

:::\*B83-05 美学与其他学科的关系

:::\*B83-06 美学流派及其研究

::::\*B83-061
[抽象主义美学](../Page/抽象主义美学.md "wikilink")、[印象主义美学](../Page/印象主义美学.md "wikilink")、[形式主义美学](../Page/形式主义美学.md "wikilink")

::::\*B83-062
[唯美主义美学](../Page/唯美主义美学.md "wikilink")、[相对主义美学](../Page/相对主义美学.md "wikilink")

::::\*B83-064 [结构主义美学](../Page/结构主义美学.md "wikilink")

::::\*B83-066 [自然主义美学](../Page/自然主义美学.md "wikilink")

::::\*B83-069 其他

:::\*B83-09 [美学史](../Page/美学史.md "wikilink")

::\*B832 美学与社会生产

:::\*B832.1 [技术美学](../Page/技术美学.md "wikilink")

:::\*B832.2 [生产场所美学](../Page/生产场所美学.md "wikilink")

:::\*B832.3 [产品美学](../Page/产品美学.md "wikilink")

:::\*B832.5 [劳动美学](../Page/劳动美学.md "wikilink")

::\*B834 美学与现实社会生活

:::\*B834.2 [城市美学](../Page/城市美学.md "wikilink")

:::\*B834.3 [生活美学](../Page/生活美学.md "wikilink")

:::\*B834.4 [社会美学](../Page/社会美学.md "wikilink")

::\*B835 [艺术美学](../Page/艺术美学.md "wikilink")

:\*B84　[心理学](../Page/心理学.md "wikilink")

::\*B84-0 心理学理论

:::\*B84-05 心理学与其他学科的关系

:::\*B84-06 心理学派别及其研究

::::\*B84-061 [构造学派](../Page/构造学派.md "wikilink")

::::\*B84-062 [机能主义派](../Page/机能主义派.md "wikilink")

::::\*B84-063 [行为主义派](../Page/行为主义派.md "wikilink")

::::\*B84-064
[格式塔心理学派](../Page/格式塔心理学派.md "wikilink")（[完形派](../Page/完形派.md "wikilink")）

::::\*B84-065 [精神分析学派](../Page/精神分析学派.md "wikilink")

::::\*B84-066 [存在主义心理学派](../Page/存在主义心理学派.md "wikilink")

::::\*B84-067 [人本主义学派](../Page/人本主义学派.md "wikilink")

::::\*B84-069 其他

:::\*B84-09 [心理学史](../Page/心理学史.md "wikilink")

::\*B841 [心理研究方法](../Page/心理.md "wikilink")

:::\*B841.1 [电生理技术](../Page/电生理技术.md "wikilink")

:::\*B841.2
[数理心理学](../Page/数理心理学.md "wikilink")、[心理统计法](../Page/心理统计法.md "wikilink")

:::\*B841.3 [条件反射研究法](../Page/条件反射.md "wikilink")

:::\*B841.4 [实验法](../Page/实验法.md "wikilink")

:::\*B841.5 [观察法](../Page/观察法.md "wikilink")

:::\*B841.7 [心理测验](../Page/心理测验.md "wikilink")

::\*B842
[心理过程与](../Page/心理过程.md "wikilink")[心理状态](../Page/心理状态.md "wikilink")

:::\*B842.1 [认知](../Page/认知.md "wikilink")

:::\*B842.2
[感觉与](../Page/感觉.md "wikilink")[知觉](../Page/知觉.md "wikilink")

:::\*B842.3
[学习与](../Page/学习.md "wikilink")[记忆](../Page/记忆.md "wikilink")

:::\*B842.4
[表象与](../Page/表象.md "wikilink")[想象](../Page/想象.md "wikilink")

:::\*B842.5
[言语与](../Page/言语.md "wikilink")[思维](../Page/思维.md "wikilink")

:::\*B842.6
[情绪与](../Page/情绪.md "wikilink")[情感](../Page/情感.md "wikilink")

:::\*B842.7
[意识与](../Page/意识.md "wikilink")[潜意识](../Page/潜意识.md "wikilink")

::\*B843 [发生心理学](../Page/发生心理学.md "wikilink")

:::\*B843.1 [比较心理学](../Page/比较心理学.md "wikilink")

:::\*B843.2 [动物心理学](../Page/动物心理学.md "wikilink")

:::\*B843.3 [原始人类心理学](../Page/原始人类心理学.md "wikilink")

:::\*B843.5 心理与[遗传](../Page/遗传.md "wikilink")

:::\*B843.9 其他

::\*B844
[发展心理学](../Page/发展心理学.md "wikilink")（[人类心理学](../Page/人类心理学.md "wikilink")）

:::\*B844.1 [儿童心理学](../Page/儿童心理学.md "wikilink")

::::\*B844.11 胎儿、[新生儿心理学](../Page/新生儿心理学.md "wikilink")

::::\*B844.12 [幼儿心理学](../Page/幼儿心理学.md "wikilink")

::::\*B844.13 [智力超常儿童心理学](../Page/智力超常儿童心理学.md "wikilink")

::::\*B844.14 [变态儿童心理学](../Page/变态儿童心理学.md "wikilink")

:::\*B844.2 [青少年心理学](../Page/青少年心理学.md "wikilink")

:::\*B844.3 [成年人心理学](../Page/成年人心理学.md "wikilink")

:::\*B844.4 [老年人心理学](../Page/老年人心理学.md "wikilink")

:::\*B844.5 [女性心理学](../Page/女性心理学.md "wikilink")

:::\*B844.6 [男性心理学](../Page/男性心理学.md "wikilink")

:::\*B844.7 [种族心理学](../Page/种族心理学.md "wikilink")

::\*B845 [生理心理学](../Page/生理心理学.md "wikilink")

:::\*B845.1 [神经心理](../Page/神经心理.md "wikilink")

:::\*B845.2 [感官生理心理](../Page/感官生理心理.md "wikilink")

:::\*B845.3 [内分泌与心理](../Page/内分泌.md "wikilink")

:::\*B845.4 [精神药物与心理](../Page/精神药物.md "wikilink")

:::\*B845.5 [神经化学与心理](../Page/神经化学.md "wikilink")

:::\*B845.6 环境与生理心理

::::\*B845.61 [居住环境与心理](../Page/居住环境.md "wikilink")

::::\*B845.63
[建筑](../Page/建筑.md "wikilink")、[音响](../Page/音响.md "wikilink")、[照明与心理](../Page/照明.md "wikilink")

::::\*B845.65 [生态环境与心理](../Page/生态环境.md "wikilink")

::::\*B845.66 特殊环境与心理

::::\*B845.67 灾害、事故、伤害与心理

:::\*B845.9 其他

::\*B846
[变态](../Page/变态.md "wikilink")、[病态](../Page/病态.md "wikilink")、[超意识心理学](../Page/超意识心理学.md "wikilink")

::\*B848
[个性心理](../Page/个性心理.md "wikilink")（[人格心理学](../Page/人格心理学.md "wikilink")）

:::\*B848.1
[神经类型与](../Page/神经类型.md "wikilink")[气质](../Page/气质.md "wikilink")

:::\*B848.2
[能力与](../Page/能力.md "wikilink")[才能](../Page/才能.md "wikilink")

:::\*B848.3
[兴趣](../Page/兴趣.md "wikilink")、[态度](../Page/态度.md "wikilink")

:::\*B848.4
[信念](../Page/信念.md "wikilink")、[意志](../Page/意志.md "wikilink")、[行为](../Page/行为.md "wikilink")

:::\*B848.5
[智力](../Page/智力.md "wikilink")、[智慧](../Page/智慧.md "wikilink")

:::\*B848.6 [性格](../Page/性格.md "wikilink")

:::\*B848.8 [个别差异](../Page/个别差异.md "wikilink")

:::\*B848.9 其他

::\*B849 [应用心理学](../Page/应用心理学.md "wikilink")

  - B9　[无神论](../Page/无神论.md "wikilink")、[宗教](../Page/宗教.md "wikilink")

:\*B91 对[宗教的分析和研究](../Page/宗教.md "wikilink")

::\*B911 宗教与社会政治

::\*B913 宗教与科学

::\*B917 破除[迷信](../Page/迷信.md "wikilink")

:\*B92 [宗教理论与概况](../Page/宗教理论.md "wikilink")

::\*B920 宗教理论、宗教思想

::\*B921
[自然神学](../Page/自然神学.md "wikilink")、[宗教神学](../Page/宗教神学.md "wikilink")

::\*B922
[宗教组织和](../Page/宗教组织.md "wikilink")[宗教教育](../Page/宗教教育.md "wikilink")

::\*B925 [宗教文学与艺术](../Page/宗教文学.md "wikilink")

::\*B928 世界各国宗教概况

::\*B929
[宗教史](../Page/宗教史.md "wikilink")、[宗教地理](../Page/宗教地理.md "wikilink")

::\*B929.9 [宗教家传记](../Page/宗教家.md "wikilink")

:\*B93 神话与原始宗教

::\*B932 [神话](../Page/神话.md "wikilink")

::\*B933 [原始宗教](../Page/原始宗教.md "wikilink")

:\*B94 [佛教](../Page/佛教.md "wikilink")

::\*B941 [大藏经](../Page/大藏经.md "wikilink")

::\*B942 [经及](../Page/经.md "wikilink")[经疏](../Page/经疏.md "wikilink")

:::\*B942.1 [大乘](../Page/大乘.md "wikilink")

:::\*B942.2 [小乘](../Page/小乘.md "wikilink")

:::\*B942.3 [秘密部](../Page/秘密部.md "wikilink")

::\*B943 [律及](../Page/律.md "wikilink")[律疏](../Page/律疏.md "wikilink")

:::\*B943.1 [大乘律](../Page/大乘律.md "wikilink")

:::\*B943.2 [有部律](../Page/有部律.md "wikilink")

:::\*B943.3
[四分律](../Page/四分律.md "wikilink")（[戒本](../Page/戒本.md "wikilink")、[羯磨](../Page/羯磨.md "wikilink")）

:::\*B943.4 [五分律](../Page/五分律.md "wikilink")

:::\*B943.9 其他

::\*B944 [论及](../Page/论.md "wikilink")[论疏](../Page/论疏.md "wikilink")

:::\*B944.1 [大乘宗经论](../Page/大乘宗经论.md "wikilink")

:::\*B944.2 [大乘释经论](../Page/大乘释经论.md "wikilink")

:::\*B944.3 [大乘诸论释](../Page/大乘诸论释.md "wikilink")

:::\*B944.4 [小乘论](../Page/小乘论.md "wikilink")

:::\*B944.5 [秘密部论](../Page/秘密部论.md "wikilink")

:::\*B944.6 [杂藏](../Page/杂藏.md "wikilink")

::\*B945 布教、[仪注](../Page/仪注.md "wikilink")

::\*B946 [宗派](../Page/派别.md "wikilink")

:::\*B946.1
[天台宗](../Page/天台宗.md "wikilink")（[日莲宗](../Page/日莲宗.md "wikilink")、[法华宗](../Page/法华宗.md "wikilink")）

:::\*B946.2
[三论宗](../Page/三论宗.md "wikilink")（[法性宗](../Page/法性宗.md "wikilink")）

:::\*B946.3
[法相宗](../Page/法相宗.md "wikilink")（[唯识宗](../Page/唯识宗.md "wikilink")、[慈恩宗](../Page/慈恩宗.md "wikilink")）

:::\*B946.4
[华严宗](../Page/华严宗.md "wikilink")（[贤首宗](../Page/贤首宗.md "wikilink")）

:::\*B946.5
[禅宗](../Page/禅宗.md "wikilink")（[佛心宗](../Page/佛心宗.md "wikilink")）

:::\*B946.6
[密宗](../Page/密宗.md "wikilink")（[秘宗教](../Page/秘宗教.md "wikilink")、[真言乘](../Page/真言乘.md "wikilink")、[金刚乘](../Page/金刚乘.md "wikilink")）

:::\*B946.7
[律宗](../Page/律宗.md "wikilink")（[南山律宗](../Page/南山律宗.md "wikilink")）

:::\*B946.8
[净土宗](../Page/净土宗.md "wikilink")（[莲宗](../Page/莲宗.md "wikilink")）

:::\*B946.9 其他

::\*B947 佛教组织及[寺院](../Page/寺院.md "wikilink")

::\*B948 对佛教的分析和研究

::\*B949 [佛教史](../Page/佛教史.md "wikilink")

::\*B949.9 传记

:\*B95 [道教](../Page/道教.md "wikilink")

::\*B951 [道藏](../Page/道藏.md "wikilink")

::\*B952 经文

::\*B953 [戒律](../Page/戒律.md "wikilink")

::\*B955 布教、仪注

::\*B956 宗派

:::\*B956.1 [太平道](../Page/太平道_\(宗教\).md "wikilink")

:::\*B956.2
[天师道](../Page/天师道.md "wikilink")（[五斗米道](../Page/五斗米道.md "wikilink")）

:::\*B956.3 [全真道](../Page/全真道.md "wikilink")

:::\*B956.4
[真大道教](../Page/真大道教.md "wikilink")（[大道教](../Page/大道教.md "wikilink")）

:::\*B956.5 [太一道](../Page/太一道.md "wikilink")

:::\*B956.9 其他

::\*B957 道教组织及[道观](../Page/道观.md "wikilink")

::\*B958 对道教的分析与研究

::\*B959 [道教史](../Page/道教史.md "wikilink")

::\*B959.9 传记

:\*B96
[伊斯兰教](../Page/伊斯兰教.md "wikilink")（[回教](../Page/回教.md "wikilink")）

::\*B961
[古兰经](../Page/古兰经.md "wikilink")（[可兰经](../Page/可兰经.md "wikilink")）

::\*B963 教义、规律

::\*B964 圣训及注释

::\*B965 布教、仪注

::\*B966 宗派

:::\*B966.1
[逊尼派](../Page/逊尼派.md "wikilink")（[圣训派](../Page/圣训派.md "wikilink")）

:::\*B966.2
[十叶派](../Page/十叶派.md "wikilink")（[什叶派](../Page/什叶派.md "wikilink")）

:::\*B966.3 近现代新教派

::\*B967 教会组织及[清真寺](../Page/清真寺.md "wikilink")

::\*B968 对伊斯兰教的分析与研究

::\*B969 [伊斯兰教史](../Page/伊斯兰教史.md "wikilink")

::\*B969.9 传记

:\*B97 [基督教](../Page/基督教.md "wikilink")

::\*B971 [圣经](../Page/圣经.md "wikilink")

:::\*B971.1 [旧约](../Page/旧约.md "wikilink")

:::\*B971.2 [新约](../Page/新约.md "wikilink")

::\*B972 教义、[神学](../Page/神学.md "wikilink")

::\*B975 布教、传道、仪注

::\*B976 宗派

:::\*B976.1 [天主教](../Page/天主教.md "wikilink")（旧教、公教、罗马公教）

:::\*B976.2
[正教](../Page/正教.md "wikilink")（[东正教](../Page/东正教.md "wikilink")）

:::\*B976.3 [新教](../Page/新教.md "wikilink")（耶稣教、更正教）

::\*B977 教会组织及[教堂](../Page/教堂.md "wikilink")

::\*B978 对基督教的分析与研究

::\*B979 [基督教史](../Page/基督教史.md "wikilink")

::\*B979.9 传记

:\*B98 其他宗教

::\*B981 [神道教](../Page/神道教.md "wikilink")

::\*B982
[婆罗门教](../Page/婆罗门教.md "wikilink")、[印度教](../Page/印度教.md "wikilink")、[耆那教](../Page/耆那教.md "wikilink")、[锡克教](../Page/锡克教.md "wikilink")

::\*B983
[琐罗亚斯德教](../Page/琐罗亚斯德教.md "wikilink")（[祆教](../Page/祆教.md "wikilink")）、[波斯教](../Page/波斯教.md "wikilink")

::\*B985
[犹太教](../Page/犹太教.md "wikilink")（[希伯来教](../Page/希伯来教.md "wikilink")）

::\*B986
[希腊教](../Page/希腊教.md "wikilink")、[罗马教](../Page/罗马教.md "wikilink")

::\*B987 呼都（[伏都](../Page/伏都.md "wikilink")）教（非洲）

::\*B989.1 其他古代宗教

::\*B989.2 地方宗教

::\*B989.3 新兴宗教

:\*B99 B99 [术数](../Page/术数.md "wikilink")、[迷信](../Page/迷信.md "wikilink")

::\*B991 世界

::\*B992 中国

:::\*B992.1
[阴阳](../Page/阴阳.md "wikilink")[五行说](../Page/五行.md "wikilink")

:::\*B992.2 [占卜](../Page/占卜.md "wikilink")

:::\*B992.3 [命相](../Page/命相.md "wikilink")

:::\*B992.4
[堪舆](../Page/堪舆.md "wikilink")（[风水](../Page/风水.md "wikilink")）

:::\*B992.5
[巫医](../Page/巫医.md "wikilink")、[巫术](../Page/巫术.md "wikilink")

::\*B993/997 各国

-----

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")