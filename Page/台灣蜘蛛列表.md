[台灣約有近](../Page/台灣.md "wikilink")300種[蜘蛛](../Page/蜘蛛.md "wikilink")。

## 原蛛亞目 Mygalomorphae

### 六疣蛛總科 Hexatheloidea

  - [六疣蛛科](../Page/六疣蛛科.md "wikilink") Hexathelidae

<!-- end list -->

1.  [赫爾斯特上戶蛛](../Page/赫爾斯特上戶蛛.md "wikilink") - *Macrothele holsti*
    <small>Pocock</small>
2.  [黑毛上戶蛛](../Page/黑毛上戶蛛.md "wikilink") - *Macrothele simplicata*
    <small>(Saito)</small>

### 上戶蛛總科 Dipluroidea

  - [上戶蛛科](../Page/上戶蛛科.md "wikilink") Dipluridae

<!-- end list -->

1.  [台灣上戶蛛](../Page/台灣上戶蛛.md "wikilink") - *Euagrus formosanus*
    <small>Saito</small>

### 捕鳥蛛總科 Theraphosoidea

  - [捕鳥蛛科](../Page/捕鳥蛛科.md "wikilink") Theraphosidae

<!-- end list -->

1.  [雅美蛛](../Page/雅美蛛.md "wikilink") - *Yamia watase*
    <small>Kishida</small>

### 地蛛總科 Atypoidea

  - [地蛛科](../Page/地蛛科.md "wikilink") Atypidae

[Atypus_karschi.jpg](https://zh.wikipedia.org/wiki/File:Atypus_karschi.jpg "fig:Atypus_karschi.jpg")

1.  [台灣地蛛](../Page/台灣地蛛.md "wikilink") - *Atypus formosensis*
    <small>Kishida</small>
2.  [卡氏地蛛](../Page/卡氏地蛛.md "wikilink") - *Atypus karschi*
    <small>Doenitz</small>

### 螲蟷總科 Ctenizoidea

  - [螲蟷科](../Page/螲蟷科.md "wikilink") Ctenizidae

<!-- end list -->

1.  [大螲蟷](../Page/大螲蟷.md "wikilink") - *Bothriocyrtum tractabile*
    <small>Saito</small>
2.  [台灣螲蟷](../Page/台灣拉土蛛.md "wikilink") - *Latouchia formosensis*
    <small>Kishida</small>

## 新蛛亞目 Araneomorphae

### 縮網蛛總科 Filistatoidea

  - [縮網蛛科](../Page/縮網蛛科.md "wikilink") Filistatidae

<!-- end list -->

1.  [縮網蛛](../Page/縮網蛛.md "wikilink") - *Filistata marginata*
    <small>Komatsu</small>

### 花皮蛛總科 Scytodoidea

  - [山城蛛科](../Page/山城蛛科.md "wikilink") Scytodidae

<!-- end list -->

1.  [黑條山城蛛](../Page/黑條山城蛛.md "wikilink") - *Scytodes nigrolineata*
    <small>(Simon)</small>
2.  [花斑山城蛛](../Page/花斑山城蛛.md "wikilink") - *Scytodes thoracica*
    <small>(Latreille)</small>

<!-- end list -->

  - [絲蛛科](../Page/絲蛛科.md "wikilink") Sicariidae

<!-- end list -->

1.  [絲蛛](../Page/絲蛛.md "wikilink") - *Loxosceles rufescens*
    <small>(Dufour)</small>

### 幽靈蛛總科 Pholcoidea

  - [幽靈蛛科](../Page/幽靈蛛科.md "wikilink") Pholcidae

<!-- end list -->

1.  [熱帶幽靈蛛](../Page/熱帶幽靈蛛.md "wikilink") - *Artema atlanta*
    <small>Walckenaer</small>
2.  [隱居幽靈蛛](../Page/隱居幽靈蛛.md "wikilink") - *Pholcus crypticolens*
    <small>Boes. et Str.</small>
3.  [無毛幽靈蛛](../Page/無毛幽靈蛛.md "wikilink") - *Pholcus inermis*
    <small>Kishida</small>
4.  [室內幽靈蛛](../Page/室內幽靈蛛.md "wikilink") - *Pholcus phalangioides*
    <small>(Fuesslin)</small>
5.  [擬幽靈蛛](../Page/擬幽靈蛛.md "wikilink") - *Smeringopus pallidus*
    <small>(Blackwall)</small>
6.  [廣六眼幽靈蛛](../Page/廣六眼幽靈蛛.md "wikilink") - *Spermophora senoculata*
    <small>(Duges)</small>

### 石蛛總科 Dysderoidea

  - [野豬蛛科](../Page/野豬蛛科.md "wikilink") Dysderidae

<!-- end list -->

1.  [野豬蛛](../Page/野豬蛛.md "wikilink") - *Dysdera crocota* <small>L.
    Koch</small>

<!-- end list -->

  - [卵蛛科](../Page/卵蛛科.md "wikilink") Oonopidae

<!-- end list -->

1.  [螨蛛](../Page/螨蛛.md "wikilink") - *Gamasomorpha cataphracta*
    <small>Karsch</small>
2.  [鎧蛛](../Page/鎧蛛.md "wikilink") - *Ischnothyreus narutomii*
    <small>(Nakatsudi)</small>
3.  [梭德氏蜱蛛](../Page/梭德氏蜱蛛.md "wikilink") - *Opopaea sauteri*
    <small>Brignoli</small>

<!-- end list -->

  - [閻魔蛛科](../Page/閻魔蛛科.md "wikilink") Segestriidae

<!-- end list -->

1.  [宮蛛](../Page/宮蛛.md "wikilink") - *Ariadna lateralis*
    <small>(Karsch)</small>

### 隆頭蛛總科 Eresoidea

  - [長疣蛛科](../Page/長疣蛛科.md "wikilink") Hersiliidae

<!-- end list -->

1.  [亞洲長疣蛛](../Page/亞洲長疣蛛.md "wikilink") - *Hersilia asiatica*
    <small>Song and Zheng</small>
2.  [薩氏長疣蛛](../Page/薩氏長疣蛛.md "wikilink") - *Hersilia savignyi*
    <small>Lucas</small>

<!-- end list -->

  - [埃蛛科](../Page/埃蛛科.md "wikilink") Oecobiidae

<!-- end list -->

1.  [環腳埃蛛](../Page/環腳埃蛛.md "wikilink") - *Oecobius annulipes*
    <small>(Lucas)</small>
2.  [台灣埃蛛](../Page/台灣埃蛛.md "wikilink") - *Oecobius formosensis*
    <small>(Kishida)</small>

### 渦蛛總科 Uloboroidea

  - [渦蛛科](../Page/渦蛛科.md "wikilink") Uloboridae

<!-- end list -->

1.  [扇網蛛](../Page/扇網蛛.md "wikilink") - *Hyptiotes affinis* <small>Boes.
    et Str.</small>
2.  [長夜蛛](../Page/長夜蛛.md "wikilink") - *Miagrammopes oblongus*
    <small>Yoshida</small>
3.  [東亞夜蛛](../Page/東亞夜蛛.md "wikilink") - *Miagrammopes orientalis*
    <small>Boes. et Str.</small>
4.  [棘渦蛛](../Page/棘渦蛛.md "wikilink") - *Octonoba spinosa*
    <small>Yoshida</small>
5.  [台灣渦蛛](../Page/台灣渦蛛.md "wikilink") - *Octonoba taiwanica*
    <small>Yoshida</small>
6.  [變異渦蛛](../Page/變異渦蛛.md "wikilink") - *Octonoba varians*
    <small>(Boes. et Str.)</small>
7.  [黑斑渦蛛](../Page/黑斑渦蛛.md "wikilink") - *Philoponella nigromaculata*
    <small>Yoshida</small>
8.  [隆背渦蛛](../Page/隆背渦蛛.md "wikilink") - *Philoponella prominens*
    <small>(Boes. et Str.)</small>
9.  [膝形渦蛛](../Page/膝形渦蛛.md "wikilink") - *Zosis geniculatus*
    <small>(Olivier)</small>

### 金蛛總科 Araneoidea

  - [圓蛛科（或稱金蛛科）](../Page/圓蛛科.md "wikilink") Araneidae

<!-- end list -->

1.  [吊葉蛛](../Page/吊葉蛛.md "wikilink") - *Acusilas coccineus*
    <small>Simon</small>
2.  [蛇紋蛛](../Page/蛇紋蛛.md "wikilink") - *Anepsion roeweri*
    <small>Chrysanthus</small>
3.  [雙峰曳尾蛛](../Page/雙峰曳尾蛛.md "wikilink") - *Arachnura logio*
    <small>Yaginuma</small>
4.  [黑尾曳尾蛛](../Page/黑尾曳尾蛛.md "wikilink") - *Arachnura melanura*
    <small>Simon</small>
5.  [褐斑鬼蛛](../Page/褐斑鬼蛛.md "wikilink") - *Araneus corporosus*
    <small>(Keyserling)</small>
6.  [三角鬼蛛](../Page/三角鬼蛛.md "wikilink") - *Araneus dehaani*
    <small>(Doleschall)</small>
7.  [針毛鬼蛛](../Page/針毛鬼蛛.md "wikilink") - *Araneus doenitzella*
    <small>Strand</small>
8.  [黃斑鬼蛛](../Page/黃斑鬼蛛.md "wikilink") - *Araneus ejusmode* <small>Boes.
    et Str.</small>
9.  [卵形鬼蛛](../Page/卵形鬼蛛.md "wikilink") - *Araneus inustus* <small>(L.
    Koch)</small>
10. [拉克氏鬼蛛](../Page/拉克氏鬼蛛.md "wikilink") - *Araneus laglaizei*
    <small>(Simon)</small>
11. [黑綠鬼蛛](../Page/黑綠鬼蛛.md "wikilink") - *Araneus mitificus*
    <small>(Simon)</small>
12. [五紋鬼蛛](../Page/五紋鬼蛛.md "wikilink") - *Araneus pentagrammicus*
    <small>(Karsch)</small>
13. [尖鬼蛛](../Page/尖鬼蛛.md "wikilink") - *Araneus pseudocentrodes*
    <small>Boes. et Str.</small>
14. [茶色鬼蛛](../Page/茶色鬼蛛.md "wikilink") - *Araneus punctigera*
    <small>(Doleschall)</small>
15. [紅斑鬼蛛](../Page/紅斑鬼蛛.md "wikilink") - *Araneus roseomaculatus*
    <small>Ono</small>
16. [赤腳鬼蛛](../Page/赤腳鬼蛛.md "wikilink") - *Araneus rufofemoratus*
    <small>Simon</small>
17. [大腹鬼蛛](../Page/大腹鬼蛛.md "wikilink") - *Araneus ventricosus*
    <small>(L. Koch)</small>
18. [長圓金蛛](../Page/好勝金蛛.md "wikilink") - *Argiope aemula*
    <small>(Walckenaer)</small>
19. [中形金蛛](../Page/中形金蛛.md "wikilink") - *Argiope aetheroides*
    <small>Yin et al.</small>
20. [悅目金蛛](../Page/悅目金蛛.md "wikilink") - *Argiope amoena* <small>L.
    Koch</small>
21. [夏威夷花園蛛](../Page/夏威夷花園蛛.md "wikilink") - *Argiope appensa*
    <small>Walckenaer</small>
22. [小形金蛛](../Page/小形金蛛.md "wikilink") - *Argiope minuta*
    <small>(Karsch)</small>
23. [眼點金蛛](../Page/眼點金蛛.md "wikilink") - *Argiope ocula*
    <small>Fox</small>
24. [高頭蛛](../Page/高頭蛛.md "wikilink") - *Caerostris paradoxa*
    <small>(Doleschall)</small>
25. [銀塵蛛](../Page/銀塵蛛.md "wikilink") - *Cyclosa argentata*
    <small>Tanikawa</small>
26. [假銀塵蛛](../Page/假銀塵蛛.md "wikilink") - *Cyclosa argenteoalba*
    <small>Boes. et Str.</small>
27. [熱帶塵蛛](../Page/熱帶塵蛛.md "wikilink") - *Cyclosa confusa* <small>Boes.
    et Str.</small>
28. [突尾塵蛛](../Page/突尾塵蛛.md "wikilink") - *Cyclosa conica*
    <small>(Pallas)</small>
29. [蓬萊塵蛛](../Page/蓬萊塵蛛.md "wikilink") - *Cyclosa formosana*
    <small>Tanikawa et Ono</small>
30. [長銀塵蛛](../Page/長銀塵蛛.md "wikilink") - *Cyclosa ginnaga*
    <small>Yaginuma</small>
31. [日本塵蛛](../Page/日本塵蛛.md "wikilink") - *Cyclosa japonica* <small>Boes.
    et Str.</small>
32. [科氏塵蛛](../Page/科氏塵蛛.md "wikilink") - *Cyclosa koi* <small>Tanikawa
    et Ono</small>
33. [寬尾塵蛛](../Page/寬尾塵蛛.md "wikilink") - *Cyclosa laticauda*
    <small>Boes. et Str.</small>
34. [山塵蛛](../Page/山塵蛛.md "wikilink") - *Cyclosa monticola* <small>Boes.
    et Str.</small>
35. [二角塵蛛](../Page/二角塵蛛.md "wikilink") - *Cyclosa mulmeinensis*
    <small>(Thorell)</small>
36. [八疣塵蛛](../Page/八疣塵蛛.md "wikilink") - *Cyclosa octotuberculata*
    <small>Karsch</small>
37. [長臉塵蛛](../Page/長臉塵蛛.md "wikilink") - *Cyclosa omonaga*
    <small>Tanikawa</small>
38. [五點塵蛛](../Page/五點塵蛛.md "wikilink") - *Cyclosa quinqueguttata*
    <small>(Thorell)</small>
39. [四突塵蛛](../Page/四突塵蛛.md "wikilink") - *Cyclosa sedeculata*
    <small>Karsch</small>
40. [條原氏塵蛛](../Page/條原氏塵蛛.md "wikilink") - *Cyclosa shinoharai*
    <small>Tanikawa et Ono</small>
41. [棘尾塵蛛](../Page/棘尾塵蛛.md "wikilink") - *Cyclosa spinosa*
    <small>Saito</small>
42. [三尾塵蛛](../Page/三尾塵蛛.md "wikilink") - *Cyclosa tricauda*
    <small>Saito</small>
43. [圓腹塵蛛](../Page/圓腹塵蛛.md "wikilink") - *Cyclosa vallata*
    <small>(keyserling)</small>
44. [鳥糞蛛](../Page/鳥糞蛛.md "wikilink") - *Cyrtarachne bufo* <small>(Boes.
    et Str.)</small>
45. [大鳥糞蛛](../Page/大鳥糞蛛.md "wikilink") - *Cyrtarachne inaequalis*
    <small>Thorell</small>
46. [方格雲斑蛛](../Page/方格雲斑蛛.md "wikilink") - *Cyrtophora exanthematica*
    <small>(Doleschall)</small>
47. [泉字雲斑蛛](../Page/泉字雲斑蛛.md "wikilink") - *Cyrtophora moluccensis*
    <small>(Doleschall)</small>
48. [單色雲斑蛛](../Page/單色雲斑蛛.md "wikilink") - *Cyrtophora unicolor*
    <small>(Doleschall)</small>
49. [乳頭棘蛛](../Page/乳頭棘蛛.md "wikilink") - *Gasteracantha cancriformis*
    <small>(Linnaeus)</small>
50. [尖棘蛛](../Page/尖棘蛛.md "wikilink") - *Gasteracantha fornicata*
    <small>(Fabricius)</small>
51. [古氏棘蛛](../Page/古氏棘蛛.md "wikilink") - *Gasteracantha kuhlii*
    <small>C. Koch</small>
52. [梭德氏棘蛛](../Page/梭德氏棘蛛.md "wikilink") - *Gasteracantha sauteri*
    <small>(Dahl)</small>
53. [四星紅蛛](../Page/四星紅蛛.md "wikilink") - *Hypsosinga pygmaea*
    <small>(Sundevall)</small>
54. [血色紅蛛](../Page/血色紅蛛.md "wikilink") - *Hypsosinga sanguinea*
    <small>(C. Koch)</small>
55. [偽黃金蛛](../Page/偽黃金蛛.md "wikilink") - *Lariniaria argiopiformis*
    <small>Boes. et Str.</small>
56. [線紋姬鬼蛛](../Page/線紋姬鬼蛛.md "wikilink") - *Neoscona adianta*
    <small>(Walckenaer)</small>
57. [黃姬鬼蛛](../Page/黃姬鬼蛛.md "wikilink") - *Neoscona doenitzi*
    <small>(Boes. et Str.)</small>
58. [寬腹姬鬼蛛](../Page/寬腹姬鬼蛛.md "wikilink") - *Neoscona fuscocoloratus*
    <small>(Boes. et Str.)</small>
59. [斑腳姬鬼蛛](../Page/斑腳姬鬼蛛.md "wikilink") - *Neoscona maculatipes*
    <small>(L. Koch)</small>
60. [黑側綠姬鬼蛛](../Page/黑側綠姬鬼蛛.md "wikilink") - *Neoscona melloteei*
    <small>(Simon)</small>
61. [簷下姬鬼蛛](../Page/簷下姬鬼蛛.md "wikilink") - *Neoscona nautica* <small>(L.
    Koch)</small>
62. [野姬鬼蛛](../Page/野姬鬼蛛.md "wikilink") - *Neoscona scylla*
    <small>(Karsch)</small>
63. [黃邊綠姬鬼蛛](../Page/黃邊綠姬鬼蛛.md "wikilink") - *Neoscona scylloides*
    <small>(Boes. et Str.)</small>
64. [白緣姬鬼蛛](../Page/白緣姬鬼蛛.md "wikilink") - *Neoscona subpullata*
    <small>(Boes. et Str.)</small>
65. [星條姬鬼蛛](../Page/星條姬鬼蛛.md "wikilink") - *Neoscona theisi*
    <small>(Walckenaer)</small>
66. [菱角蛛](../Page/菱角蛛.md "wikilink") - *Pasilobus bufoninus*
    <small>(Simon)</small>
67. [枯葉尖鼻蛛](../Page/枯葉尖鼻蛛.md "wikilink") - *Poltys idae*
    <small>(Ausserer)</small>
68. [無鱗尖鼻蛛](../Page/無鱗尖鼻蛛.md "wikilink") - *Poltys illepidus* <small>C.
    Koch</small>
69. [黑尖鼻蛛](../Page/黑尖鼻蛛.md "wikilink") - *Poltys nigrinus*
    <small>Saito</small>
70. [短棘蛛](../Page/短棘蛛.md "wikilink") - *Thelacantha brevispina*
    <small>(Doleschall)</small>
71. [黑頭鬼蛛](../Page/黑頭鬼蛛.md "wikilink") - *Yaginumia sia*
    <small>(Strand)</small>
72. [阿須寬肩鬼蛛](../Page/阿須寬肩鬼蛛.md "wikilink") - *Zilla astridae*
    <small>(Strand)</small>

<!-- end list -->

  - [皿網蛛科](../Page/皿網蛛科.md "wikilink") Linyphiidae

<!-- end list -->

1.  [小皿蛛](../Page/小皿蛛.md "wikilink") - *Aprifrontalia mascula*
    <small>(Karsch)</small>
2.  [台灣裂頭小皿蛛](../Page/台灣裂頭小皿蛛.md "wikilink") - *Callitrichia formosana*
    <small>Oi</small>
3.  [鋸胸微蛛](../Page/鋸胸微蛛.md "wikilink") - *Erigone koshiensis*
    <small>Oi</small>
4.  [小鋸胸微蛛](../Page/小鋸胸微蛛.md "wikilink") - *Erigone prominens*
    <small>Boes. et Str.</small>
5.  [捲葉姬微蛛](../Page/捲葉姬微蛛.md "wikilink") - *Erigonidium graminicolum*
    <small>(Sundevall)</small>
6.  [白緣皿蛛](../Page/白緣皿蛛.md "wikilink") - *Neriene albolimbata*
    <small>(Karsch)</small>
7.  [食蟲瘤胸蛛](../Page/食蟲瘤胸蛛.md "wikilink") - *Oedothorax insecticeps*
    <small>Boes. et Str.</small>

<!-- end list -->

  - [長腳蛛科](../Page/長腳蛛科.md "wikilink") Tetragnathidae

[Nephila_pilipes_02.jpg](https://zh.wikipedia.org/wiki/File:Nephila_pilipes_02.jpg "fig:Nephila_pilipes_02.jpg")

1.  [裂腹蛛](../Page/裂腹蛛.md "wikilink") - *Herennia ornatissima*
    <small>(Doleschall)</small>
2.  [肩斑銀腹蛛](../Page/肩斑銀腹蛛.md "wikilink") - *Leucauge blanda* <small>(L.
    Koch)</small>
3.  [尖尾銀腹蛛](../Page/尖尾銀腹蛛.md "wikilink") - *Leucauge decorata*
    <small>(Blackwall)</small>
4.  [大銀腹蛛](../Page/大銀腹蛛.md "wikilink") - *Leucauge magnifica*
    <small>Yaginuma</small>
5.  [脈斑銀腹蛛](../Page/脈斑銀腹蛛.md "wikilink") - *Leucauge venusta*
    <small>(Walckenacer)</small>
6.  [褐腹長蹠蛛](../Page/褐腹長蹠蛛.md "wikilink") - *Metleucauge chikunii*
    <small>Tanikawa</small>
7.  [金比羅長蹠蛛](../Page/金比羅長蹠蛛.md "wikilink") - *Metleucauge kompirensis*
    <small>(Boes. et Str.)</small>
8.  [眼鏡長蹠蛛](../Page/眼鏡長蹠蛛.md "wikilink") - *Metleucauge yunohamensis*
    <small>(Boes. et Str.)</small>
9.  [橫帶人面蜘蛛](../Page/橫帶人面蜘蛛.md "wikilink") - *Nephila clavata*
    <small>(Fabricius)</small>
10. [人面蜘蛛](../Page/人面蜘蛛.md "wikilink") - *Nephila maculata*
    <small>(Fabricius)</small>
11. [錫蘭長腳蛛](../Page/錫蘭長腳蛛.md "wikilink") - *Tetragnatha ceylonica*
    <small>Canbridge</small>
12. [細長腳蛛](../Page/細長腳蛛.md "wikilink") - *Tetragnatha chauliodus*
    <small>(Thorell)</small>
13. [江崎長腳蛛](../Page/江崎長腳蛛.md "wikilink") - *Tetragnatha esakii*
    <small>Okuma</small>
14. [銀長腳蛛](../Page/銀長腳蛛.md "wikilink") - *Tetragnatha gracilis*
    <small>Stoliczka</small>
15. [長沼氏長腳蛛](../Page/長沼氏長腳蛛.md "wikilink") - *Tetragnatha hiroshii*
    <small>Okuma</small>
16. [爪哇長腳蛛](../Page/爪哇長腳蛛.md "wikilink") - *Tetragnatha javana*
    <small>(Thorell)</small>
17. [方網長腳蛛](../Page/方網長腳蛛.md "wikilink") - *Tetragnatha lauta*
    <small>Yaginuma</small>
18. [大長腳蛛](../Page/大長腳蛛.md "wikilink") - *Tetragnatha mandibulata*
    <small>Walckenaer</small>
19. [日本長腳蛛](../Page/日本長腳蛛.md "wikilink") - *Tetragnatha maxillosa*
    <small>Thorell</small>
20. [台灣長腳蛛](../Page/台灣長腳蛛.md "wikilink") - *Tetragnatha nepiformis*
    <small>Doleschall</small>
21. [華麗長腳蛛](../Page/華麗長腳蛛.md "wikilink") - *Tetragnatha nitens*
    <small>(Audouin)</small>
22. [前齒長腳蛛](../Page/前齒長腳蛛.md "wikilink") - *Tetragnatha praedonia*
    <small>L. Koch</small>
23. [綠鱗長腳蛛](../Page/綠鱗長腳蛛.md "wikilink") - *Tetragnatha squamata*
    <small>Karsch</small>
24. [條紋高腹蛛](../Page/條紋高腹蛛.md "wikilink") - *Tylorida striata*
    <small>(Thorell)</small>
25. [橫帶高腹蛛](../Page/橫帶高腹蛛.md "wikilink") - *Tylorida ventralis*
    <small>Thorell</small>

<!-- end list -->

  - [姬蛛科](../Page/姬蛛科.md "wikilink") Theridiidae

<!-- end list -->

1.  [吊鐘姬蛛](../Page/吊鐘姬蛛.md "wikilink") - *Achaearanea angulithorax*
    <small>(Boes. et Str.)</small>
2.  [粗腿姬蛛](../Page/粗腿姬蛛.md "wikilink") - *Achaearanea ferrumequina*
    <small>(Boes. et Str.)</small>
3.  [日本姬蛛](../Page/日本姬蛛.md "wikilink") - *Achaearanea japonica*
    <small>(Boes. et Str.)</small>
4.  [三點姬蛛](../Page/三點姬蛛.md "wikilink") - *Achaearanea kompirensis*
    <small>(Boes. et Str.)</small>
5.  [月形姬蛛](../Page/月形姬蛛.md "wikilink") - *Achaearanea lunate*
    <small>(Clerck)</small>
6.  [大姬蛛](../Page/大姬蛛.md "wikilink") - *Achaearanea tepidariorum*
    <small>(C. Koch)</small>
7.  [台灣粗腳姬蛛](../Page/台灣粗腳姬蛛.md "wikilink") - *Anelosimus taiwanicus*
    <small>Yoshida</small>
8.  [銀腹寄居姬蛛](../Page/銀腹寄居姬蛛.md "wikilink") - *Argyrodes bonadea*
    <small>(Karsch)</small>
9.  [蚓腹寄居姬蛛](../Page/蚓腹寄居姬蛛.md "wikilink") - *Argyrodes cylindrogaster*
    <small>(Simon)</small>
10. [裂額寄居姬蛛](../Page/裂額寄居姬蛛.md "wikilink") - *Argyrodes fissifrons*
    <small>O.P. Cambridge</small>
11. [赤腹寄居姬蛛](../Page/赤腹寄居姬蛛.md "wikilink") - *Argyrodes miniaceus*
    <small>(Doleschall)</small>
12. [劍額寄居姬蛛](../Page/劍額寄居姬蛛.md "wikilink") - *Argyrodes xiphias*
    <small>Thorell</small>
13. [小高金姬蛛](../Page/小高金姬蛛.md "wikilink") - *Chrysso argyrodiformis*
    <small>(Yaginuma)</small>
14. [長尾金姬蛛](../Page/長尾金姬蛛.md "wikilink") - *Chrysso caudigera*
    <small>Yoshida</small>
15. [闊腹金姬蛛](../Page/闊腹金姬蛛.md "wikilink") - *Chrysso lativentris*
    <small>Yoshida</small>
16. [黑色金姬蛛](../Page/黑色金姬蛛.md "wikilink") - *Chrysso nigra* <small>(O.P.
    Cambridge)</small>
17. [斑點金姬蛛](../Page/斑點金姬蛛.md "wikilink") - *Chrysso punctifera*
    <small>(Yaginuma)</small>
18. [棘腹金姬蛛](../Page/棘腹金姬蛛.md "wikilink") - *Chrysso spiniventris*
    <small>(O.P. Cambridge)</small>
19. [三斑金姬蛛](../Page/三斑金姬蛛.md "wikilink") - *Chrysso trimaculata*
    <small>Zhu, Zhang et Xu</small>
20. [華麗金姬蛛](../Page/華麗金姬蛛.md "wikilink") - *Chrysso venusta*
    <small>(Yaginuma)</small>
21. [四疣金姬蛛](../Page/四疣金姬蛛.md "wikilink") - *Chrysso vesiculosa*
    <small>(Simon)</small>
22. [鞘腹姬蛛](../Page/鞘腹姬蛛.md "wikilink") - *Coleosoma blandum* <small>O.P.
    Cambridge</small>
23. [八星鞘腹姬蛛](../Page/八星鞘腹姬蛛.md "wikilink") - *Coleosoma octomaculatum*
    <small>(Boes. et Str.)</small>
24. [黃緣微姬蛛](../Page/黃緣微姬蛛.md "wikilink") - *Dipoena flavomarginate*
    <small>Boes. et Str.</small>
25. [日本微姬蛛](../Page/日本微姬蛛.md "wikilink") - *Dipoena japonica*
    <small>(Yoshida)</small>
26. [鼬形微姬蛛](../Page/鼬形微姬蛛.md "wikilink") - *Dipoena mustelina*
    <small>(Simon)</small>
27. [小紅微姬蛛](../Page/小紅微姬蛛.md "wikilink") - *Dipoena mutilata*
    <small>Boes. et Str.</small>
28. [菱蛛](../Page/菱蛛.md "wikilink") - *Episinus affinis* <small>Boes. et
    Str.</small>
29. [慎原氏菱蛛](../Page/慎原氏菱蛛.md "wikilink") - *Episinus makiharai*
    <small>Okuma</small>
30. [散斑菱蛛](../Page/散斑菱蛛.md "wikilink") - *Episinus punctisparsus*
    <small>Yoshida</small>
31. [吉田氏菱蛛](../Page/吉田氏菱蛛.md "wikilink") - *Episinus yoshidai*
    <small>Okuma</small>
32. [赤背寡婦蛛](../Page/赤背寡婦蛛.md "wikilink") - *Latrodectus mactans*
    <small>(Fabricius)</small>
33. [三棘姬蛛](../Page/三棘姬蛛.md "wikilink") - *Molione triacantha*
    <small>Thorell</small>
34. [擬短跗菱蛛](../Page/擬短跗菱蛛.md "wikilink") - *Moneta mirabilis*
    <small>(Boes. et Str.)</small>
35. [短跗菱蛛](../Page/短跗菱蛛.md "wikilink") - *Moneta spiniger* <small>O.P.
    Cambridge</small>
36. [古村氏短跗菱蛛](../Page/古村氏短跗菱蛛.md "wikilink") - *Moneta yoshimurai*
    <small>(Yoshida)</small>
37. [黑星盔姬蛛](../Page/黑星盔姬蛛.md "wikilink") - *Pholcomma nigromaculatum*
    <small>Yoshida</small>
38. [阿里山錐頭姬蛛](../Page/阿里山錐頭姬蛛.md "wikilink") - *Phoroncidia alishanense*
    <small>Chen</small>
39. [月斑鳴姬蛛](../Page/月斑鳴姬蛛.md "wikilink") - *Steatoda cavernicola*
    <small>(Boes. et Str.)</small>
40. [七星鳴姬蛛](../Page/七星鳴姬蛛.md "wikilink") - *Steatoda erigoniformis*
    <small>(O.P. Cambridge)</small>
41. [粉點姬蛛](../Page/粉點姬蛛.md "wikilink") - *Theridion elegantissium*
    <small>Roewer</small>
42. [紅腳姬蛛](../Page/紅腳姬蛛.md "wikilink") - *Theridion rufipes*
    <small>Lucas</small>
43. [灰姬蛛](../Page/灰姬蛛.md "wikilink") - *Theridion subpallens*
    <small>Boes. et Str.</small>

### 狼蛛總科 Lycosoidea

  - [絞蛛科](../Page/絞蛛科.md "wikilink") Ctenidae

<!-- end list -->

1.  [絞蛛](../Page/絞蛛.md "wikilink") - *Anahita fauna*
    <small>Karsch</small>

<!-- end list -->

  - [狼蛛科](../Page/狼蛛科.md "wikilink") Lycosidae

<!-- end list -->

1.  [草地馬蛛](../Page/草地馬蛛.md "wikilink") - *Hippasa agelenoides*
    <small>(Simon)</small>
2.  [長疣馬蛛](../Page/長疣馬蛛.md "wikilink") - *Hippasa holmerae*
    <small>Thorell</small>
3.  [黑腹狼蛛](../Page/黑腹狼蛛.md "wikilink") - *Lycosa coelestis* <small>L.
    Koch</small>
4.  [台灣狼蛛](../Page/台灣狼蛛.md "wikilink") - *Lycosa formosana*
    <small>Saito</small>
5.  [黑底狼蛛](../Page/黑底狼蛛.md "wikilink") - *Lycosa phipsoni*
    <small>Pocock</small>
6.  [黑豹蛛](../Page/黑豹蛛.md "wikilink") - *Pardosa astrigera* <small>L.
    Koch</small>
7.  [溝渠豹蛛](../Page/溝渠豹蛛.md "wikilink") - *Pardosa laura*
    <small>Karsch</small>
8.  [擬環紋豹蛛](../Page/擬環紋豹蛛.md "wikilink") - *Pardosa pseudoannulata*
    <small>(Boes. et Str.)</small>
9.  [沙地豹蛛](../Page/沙地豹蛛.md "wikilink") - *Pardosa takahashii*
    <small>(Saito)</small>
10. [克氏水狼蛛](../Page/克氏水狼蛛.md "wikilink") - *Pirata clercki*
    <small>(Boes.et Str.)</small>

<!-- end list -->

  - [貓蛛科](../Page/貓蛛科.md "wikilink") Oxyopidae

<!-- end list -->

1.  [條紋貓蛛](../Page/條紋貓蛛.md "wikilink") - *Oxyopes macilentus* <small>L.
    Koch</small>
2.  [斜紋貓蛛](../Page/斜紋貓蛛.md "wikilink") - *Oxyopes sertatus* <small>L.
    Koch</small>
3.  [台灣貓蛛](../Page/台灣貓蛛.md "wikilink") - *Peucetis formosensis*
    <small>Kishida</small>

<!-- end list -->

  - [跑蛛科](../Page/跑蛛科.md "wikilink") Pisauridae

<!-- end list -->

1.  [黑背跑蛛](../Page/黑背跑蛛.md "wikilink") - *Dolomedes horishanus*
    <small>Kishida</small>
2.  [褐腹跑蛛](../Page/褐腹跑蛛.md "wikilink") - *Dolomedes mizhoanus*
    <small>Kishida</small>
3.  [溪跑蛛](../Page/溪跑蛛.md "wikilink") - *Dolomedes raptor* <small>Boes.
    et Str.</small>
4.  [赤條跑蛛](../Page/赤條跑蛛.md "wikilink") - *Dolomedes saganus*
    <small>Boes. et Str.</small>
5.  [長觸肢跑蛛](../Page/長觸肢跑蛛.md "wikilink") - *Hygroposa higenaga*
    <small>(Kishida)</small>
6.  [白條跑蛛](../Page/白條跑蛛.md "wikilink") - *Thalassius phipsoni*
    <small>O.P. Cambridge</small>

<!-- end list -->

  - [褸網蛛科](../Page/褸網蛛科.md "wikilink") Psechridae

<!-- end list -->

1.  [中國褸網蛛](../Page/中國褸網蛛.md "wikilink") - *Psechrus sinensis*
    <small>Berland</small>

### 草蛛總科 Agelenoidea

  - [草蛛科](../Page/草蛛科.md "wikilink") Agelenidae

<!-- end list -->

1.  [草蛛](../Page/草蛛.md "wikilink") - *Agelena limbata*
    <small>Thorell</small>
2.  [小草蛛](../Page/小草蛛.md "wikilink") - *Agelena opulenta* <small>(L.
    Koch)</small>
3.  [棚蛛](../Page/棚蛛.md "wikilink") - *Tegenaria domestica*
    <small>(Clerck)</small>

### 葉蛛總科 Dictynoidea

  - [葉蛛科](../Page/葉蛛科.md "wikilink") Dictynidae

<!-- end list -->

1.  [葉蛛](../Page/葉蛛.md "wikilink") - *Dictyna felis* <small>Boes. et
    Str.</small>
2.  [潤枯葉蛛](../Page/潤枯葉蛛.md "wikilink") - *Lathys humilis*
    <small>Blackwall</small>
3.  [黃枯葉蛛](../Page/黃枯葉蛛.md "wikilink") - *Lathys puta* <small>(O.P.
    Cambridge)</small>

<!-- end list -->

  - [橫疣蛛科](../Page/橫疣蛛科.md "wikilink") Hahniidae

<!-- end list -->

1.  [橫疣蛛](../Page/橫疣蛛.md "wikilink") - *Hehnia corticicola* <small>Boes.
    et Str.</small>

### 巨蟹蛛總科 Sparassoidea

  - [高腳蛛科](../Page/高腳蛛科.md "wikilink") Heteropodidae

<!-- end list -->

1.  [鉗高腳蛛](../Page/鉗高腳蛛.md "wikilink") - *Heteropoda forcipata*
    <small>(Karsch)</small>
2.  [白額高腳蛛](../Page/白額高腳蛛.md "wikilink") - *Heteropoda venatoria*
    <small>(Linnaeus)</small>
3.  [草袋蛛](../Page/草袋蛛.md "wikilink") - *Thelcticopis severa* <small>(L.
    Koch)</small>

### 擬扁蛛總科 Selenopoidea

  - [擬扁蛛科](../Page/擬扁蛛科.md "wikilink") Selenopidae

<!-- end list -->

1.  [囊擬扁蛛](../Page/囊擬扁蛛.md "wikilink") - *Selenops bursarius*
    <small>Karsch</small>
2.  [台灣擬扁蛛](../Page/台灣擬扁蛛.md "wikilink") - *Selenops formosanus*
    <small>Kayashima</small>

### 擬平腹蛛總科 Zodaroidea

  - [法師蛛科](../Page/法師蛛科.md "wikilink") Zodariidae

<!-- end list -->

1.  [道士蛛](../Page/道士蛛.md "wikilink") - *Doosia japonica* <small>(Boes.
    et Str.)</small>

### 分類未定 incertae sedis

  - [袋蛛科](../Page/袋蛛科.md "wikilink") Clubionidae

<!-- end list -->

1.  [活潑紅螯蛛](../Page/活潑紅螯蛛.md "wikilink") - *Cheiracanthium lascivum*
    <small>Karsch</small>
2.  [雪山袋蛛](../Page/雪山袋蛛.md "wikilink") - *Clubiona asrevida*
    <small>Ono</small>
3.  [高山袋蛛](../Page/高山袋蛛.md "wikilink") - *Clubiona bonicula*
    <small>Ono</small>
4.  [斑袋蛛](../Page/斑袋蛛.md "wikilink") - *Clubiona deletrix* <small>O.P.
    Cambridge</small>
5.  [島嶼袋蛛](../Page/島嶼袋蛛.md "wikilink") - *Clubiona insulana*
    <small>Ono</small>
6.  [日本袋蛛](../Page/日本袋蛛.md "wikilink") - *Clubiona japonica* <small>L.
    Koch</small>
7.  [捲葉袋蛛](../Page/捲葉袋蛛.md "wikilink") - *Clubiona japonicola*
    <small>Boes. et Str.</small>
8.  [羽斑袋蛛](../Page/羽斑袋蛛.md "wikilink") - *Clubiona jucunda*
    <small>(Karsch)</small>
9.  [萱嶋氏袋蛛](../Page/萱嶋氏袋蛛.md "wikilink") - *Clubiona kayashimai*
    <small>Ono</small>
10. [關山袋蛛](../Page/關山袋蛛.md "wikilink") - *Clubiona kuanshanensis*
    <small>Ono</small>
11. [黑澤氏袋蛛](../Page/黑澤氏袋蛛.md "wikilink") - *Clubiona kurosawai*
    <small>Ono</small>
12. [台灣袋蛛](../Page/台灣袋蛛.md "wikilink") - *Clubiona taiwanica*
    <small>Ono</small>
13. [瀧川氏袋蛛](../Page/瀧川氏袋蛛.md "wikilink") - *Clubiona tanikawai*
    <small>Ono</small>
14. [八木沼袋蛛](../Page/八木沼袋蛛.md "wikilink") - *Clubiona yaginumai*
    <small>Hayashi</small>
15. [陽明山袋蛛](../Page/陽明山袋蛛.md "wikilink") - *Clubiona yangmingensis*
    <small>Hayashi et Yoshida</small>

### 崖地蛛總科 Titanoecoidea

  - [崖地蛛科](../Page/崖地蛛科.md "wikilink") Titanoecidae

<!-- end list -->

1.  [日本崖地蛛](../Page/日本崖地蛛.md "wikilink") - *Nurscia albofasciata*
    <small>(Strand)</small>
2.  [熱帶崖地蛛](../Page/熱帶崖地蛛.md "wikilink") - *Pandava laminata*
    <small>(Thorell)</small>

### 鷲蛛總科 Gnaphosoidea

  - [鷲蛛科](../Page/鷲蛛科.md "wikilink") Gnaphosidae

<!-- end list -->

1.  [亞洲狂蛛](../Page/亞洲狂蛛.md "wikilink") - *Zelotes asiaticus*
    <small>(Boes. et Str.)</small>

### 蟹蛛總科 Thomisoidea

  - [蝦蛛科](../Page/蝦蛛科.md "wikilink") Philodromidae

<!-- end list -->

1.  [樹皮蝦蛛](../Page/樹皮蝦蛛.md "wikilink") - *Philodromus spinitarsis*
    <small>Simon</small>

<!-- end list -->

  - [蟹蛛科](../Page/蟹蛛科.md "wikilink") Thomisidae

<!-- end list -->

1.  [擬姬花蛛](../Page/擬姬花蛛.md "wikilink") - *Misumenoides formosipes*
    <small>(Walckenaer)</small>
2.  [日本花蛛](../Page/日本花蛛.md "wikilink") - *Misumenops japonicus*
    <small>(Boes. et Str.)</small>
3.  [三突花蛛](../Page/三突花蛛.md "wikilink") - *Misumenops tricuspidatus*
    <small>(Fabricius)</small>
4.  [嫩葉蛛](../Page/嫩葉蛛.md "wikilink") - *Oxytate striatipes* <small>L.
    Koch</small>
5.  [白條鉅蟹蛛](../Page/白條鉅蟹蛛.md "wikilink") - *Runcinia albostriata*
    <small>Boes. et Str.</small>
6.  [小樹皮蟹蛛](../Page/小樹皮蟹蛛.md "wikilink") - *Takachihoa truciformis*
    <small>(Boes. et Str.)</small>
7.  [三角蟹蛛](../Page/三角蟹蛛.md "wikilink") - *Thomisus labefactus*
    <small>Karsch</small>
8.  [琉球三角蟹蛛](../Page/琉球三角蟹蛛.md "wikilink") - *Thomisus okinawensis*
    <small>Strand</small>
9.  [台灣虎斑蟹蛛](../Page/台灣虎斑蟹蛛.md "wikilink") - *Tmarus taiwanus*
    <small>Ono</small>
10. [朱氏蟹蛛](../Page/朱氏蟹蛛.md "wikilink") - *Xysticus chui*
    <small>Ono</small>

### 蠅虎總科 Salticoidea

  - [蠅虎科](../Page/蠅虎科.md "wikilink") Salticidae

[Hasarius.adansoni.male.1.jpg](https://zh.wikipedia.org/wiki/File:Hasarius.adansoni.male.1.jpg "fig:Hasarius.adansoni.male.1.jpg")

1.  [橙黃蠅虎](../Page/橙黃蠅虎.md "wikilink") - *Carrhotus xanthogramma*
    <small>(Latreille)</small>
2.  [粗腳條斑蠅虎](../Page/粗腳條斑蠅虎.md "wikilink") - *Evarcha crassipes*
    <small>(Karsch)</small>
3.  [安德遜蠅虎](../Page/安德遜蠅虎.md "wikilink") - *Hasarius adansoni*
    <small>(Audouin)</small>
4.  [箭形蠅虎](../Page/箭形蠅虎.md "wikilink") - *Marpissa elongata*
    <small>(Karsch)</small>
5.  [橫紋蠅虎](../Page/橫紋蠅虎.md "wikilink") - *Marpissa pulla*
    <small>(Karsch)</small>
6.  [白鬚蠅虎](../Page/白鬚蠅虎.md "wikilink") - *Menemerus confuses*
    <small>Boes. et Str.</small>
7.  [台灣蟻蛛](../Page/台灣蟻蛛.md "wikilink") - *Myrmarachne formosana*
    <small>(Saito)</small>
8.  [擬台灣蟻蛛](../Page/擬台灣蟻蛛.md "wikilink") - *Myrmarachne formosicola*
    <small>Strand</small>
9.  [黑色蟻蛛](../Page/黑色蟻蛛.md "wikilink") - *Myrmarachne innermichelis*
    <small>Boes. et Str.</small>
10. [日本蟻蛛](../Page/日本蟻蛛.md "wikilink") - *Myrmarachne japonica*
    <small>(Karsch)</small>
11. [大蟻蛛](../Page/大蟻蛛.md "wikilink") - *Myrmarachne magnus*
    <small>Saito</small>
12. [無刺蟻蛛](../Page/無刺蟻蛛.md "wikilink") - *Myrmarachne sansiborica*
    <small>Strand</small>
13. [異常黑條蠅虎](../Page/異常黑條蠅虎.md "wikilink") - *Phintella abnormis*
    <small>(Boes. et Str.)</small>
14. [直線黑條蠅虎](../Page/直線黑條蠅虎.md "wikilink") - *Phintella linea*
    <small>(Karsch)</small>
15. [眼鏡黑條蠅虎](../Page/眼鏡黑條蠅虎.md "wikilink") - *Phintella versicolor*
    <small>(C. Koch)</small>
16. [褐條斑蠅虎](../Page/褐條斑蠅虎.md "wikilink") - *Plexippus paykulli*
    <small>(Audouin)</small>
17. [寬胸蠅虎](../Page/寬胸蠅虎.md "wikilink") - *Rhene atrata*
    <small>(Karsch)</small>
18. [白星褐蠅虎](../Page/白星褐蠅虎.md "wikilink") - *Sitticus penicillatus*
    <small>(Simon)</small>

### 管蛛總科 Corinnoidea

  - [管蛛科](../Page/管蛛科.md "wikilink") Corinnidae

<!-- end list -->

1.  [台灣管蛛](../Page/台灣管蛛.md "wikilink") - *Trachelas taiwanicus*
    <small>Hayashi et Yoshida</small>

<!-- end list -->

  - [輝蛛科](../Page/輝蛛科.md "wikilink") Liocranidae

<!-- end list -->

1.  [草地鼬蛛](../Page/草地鼬蛛.md "wikilink") - *Itatsina praticola*
    <small>(Boes. et Str.)</small>
2.  [輝蛛](../Page/輝蛛.md "wikilink") - *Phrurolithus claripes*
    <small>(Doenitz et Strand)</small>
3.  [台灣輝蛛](../Page/台灣輝蛛.md "wikilink") - *Phrurolithus taiwanicus*
    <small>Hayashi et Yoshida</small>

## 參考文獻

  - 陳世煌，《台灣常見蜘蛛圖鑑》，2001年，行政院農委會出版 ISBN 957-02-8706-3
  - 李文貴等，《自然觀察圖鑑1-蜘蛛》，2002年，親親文化事業有限公司 ISBN 986-7988-11-6
  - 陳仁杰等，《自然觀察步道-台灣蜘蛛觀察入門》，2002年，串門企業有限公司 ISBN 957-99645-8-0

[Category:蜘蛛目](../Category/蜘蛛目.md "wikilink")
[列表](../Category/台灣動物.md "wikilink")
[Category:台灣動物列表](../Category/台灣動物列表.md "wikilink")