**10月16日**是[阳历一年中的第](../Page/阳历.md "wikilink")289天（[闰年第](../Page/闰年.md "wikilink")290天），离全年的结束还有76天。

## 大事记

### 前6世紀

  - [前531年](../Page/前531年.md "wikilink")：[楚师灭蔡](../Page/楚国.md "wikilink")，将[蔡的世子有抓获杀死以作牺牲](../Page/蔡國.md "wikilink")。

### 4世紀

  - [385年](../Page/385年.md "wikilink")：[后秦皇帝](../Page/后秦.md "wikilink")[姚苌缢杀](../Page/姚苌.md "wikilink")[前秦皇帝](../Page/前秦.md "wikilink")[苻坚于新平佛寺](../Page/苻坚.md "wikilink")。

### 7世紀

  - [690年](../Page/690年.md "wikilink")：[武则天登上](../Page/武则天.md "wikilink")[神都太初宫正门](../Page/洛阳市.md "wikilink")：则天门的门楼，宣布改“[唐](../Page/唐朝.md "wikilink")”为“[周](../Page/武周.md "wikilink")”，成为中国历史上被广泛认可的唯一的[女皇帝](../Page/女皇帝.md "wikilink")。

### 14世紀

  - [1384年](../Page/1384年.md "wikilink")：[雅德维加加冕为](../Page/雅德维加.md "wikilink")[波兰国王](../Page/波兰.md "wikilink")。

### 16世紀

  - [1555年](../Page/1555年.md "wikilink")：[毛利元就與](../Page/毛利元就.md "wikilink")[陶晴賢在](../Page/陶晴賢.md "wikilink")[嚴島爆發](../Page/嚴島.md "wikilink")[嚴島合戰](../Page/嚴島之戰.md "wikilink")，毛利軍獲勝。

### 18世紀

  - [1793年](../Page/1793年.md "wikilink")：在[法国大革命期间](../Page/法国大革命.md "wikilink")，王后[玛丽·安托瓦内特被送上](../Page/玛丽·安托瓦内特.md "wikilink")[断头台以](../Page/断头台.md "wikilink")[叛國罪名处决](../Page/叛國.md "wikilink")。

### 19世紀

  - [1813年](../Page/1813年.md "wikilink")：[拿破仑·波拿巴率领](../Page/拿破仑·波拿巴.md "wikilink")[法国军队在](../Page/法国.md "wikilink")[德國](../Page/德國.md "wikilink")[萊比錫与](../Page/萊比錫.md "wikilink")[第六次反法同盟作战](../Page/第六次反法同盟.md "wikilink")，[莱比锡战役爆发](../Page/莱比锡战役.md "wikilink")。
  - [1834年](../Page/1834年.md "wikilink")：英国[威斯敏斯特宫失火](../Page/威斯敏斯特宫.md "wikilink")，大部分建筑被烧毁。
  - [1841年](../Page/1841年.md "wikilink")：加拿大[皇后大学成立](../Page/皇后大學_\(加拿大\).md "wikilink")。
  - [1843年](../Page/1843年.md "wikilink")：[爱尔兰数学家](../Page/爱尔兰.md "wikilink")[哈密顿在](../Page/威廉·哈密頓.md "wikilink")[都柏林散步时首次发现](../Page/都柏林.md "wikilink")[四元数](../Page/四元数.md "wikilink")，他把[公式刻在了路过的桥上](../Page/公式.md "wikilink")。
  - [1846年](../Page/1846年.md "wikilink")：[美国牙科医生威廉](../Page/美國.md "wikilink")·托马斯·英顿博士在[波士顿的](../Page/波士顿.md "wikilink")[马萨诸塞州总医院所作的一次手术中](../Page/麻薩諸塞州.md "wikilink")，首次公开使用莱瑟昂（[乙醚](../Page/乙醚.md "wikilink")）止痛。
  - [1859年](../Page/1859年.md "wikilink")：美国反奴隶者[约翰·布朗宣布起义](../Page/约翰·布朗_\(废奴主义者\).md "wikilink")。起义者一度占领政府军库，解放了附近的奴隶。后起义失败，但促成美[南北战争爆发](../Page/南北战争.md "wikilink")。史称“[布朗起义](../Page/布朗起义.md "wikilink")”。
  - [1875年](../Page/1875年.md "wikilink")：美国[杨百翰大学于](../Page/杨百翰大学.md "wikilink")[犹他州](../Page/犹他州.md "wikilink")[普若佛正式成立](../Page/普若佛.md "wikilink")。

### 20世紀

  - [1916年](../Page/1916年.md "wikilink")：[计划生育组织的创立者美国妇女](../Page/计划生育组织.md "wikilink")[玛格丽特·桑格在纽约州布鲁克林开设了第一家](../Page/玛格丽特·桑格.md "wikilink")[计划生育门诊所](../Page/计划生育.md "wikilink")。
  - [1923年](../Page/1923年.md "wikilink")：[洛伊·O·迪士尼与](../Page/洛伊·O·迪士尼.md "wikilink")[华特·迪士尼在](../Page/华特·迪士尼.md "wikilink")[好莱坞创立了](../Page/好莱坞.md "wikilink")[华特迪士尼公司](../Page/华特迪士尼公司.md "wikilink")，后来发展成为全世界规模最大的媒体和娱乐公司之一。
  - [1925年](../Page/1925年.md "wikilink")：第一届[罗加诺会议在](../Page/罗加诺会议.md "wikilink")[伦敦举行](../Page/伦敦.md "wikilink")。
  - [1934年](../Page/1934年.md "wikilink")：[中国工农红军开始](../Page/中国工农红军.md "wikilink")[长征](../Page/长征.md "wikilink")。
  - [1942年](../Page/1942年.md "wikilink")：[飓风袭击](../Page/熱帶氣旋.md "wikilink")[印度](../Page/印度.md "wikilink")[孟加拉邦](../Page/孟加拉_\(地區\).md "wikilink")，11,000人丧生。
  - [1945年](../Page/1945年.md "wikilink")：[联合国粮食及农业组织成立](../Page/联合国粮食及农业组织.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[国民党](../Page/中國國民黨.md "wikilink")“[革命实践研究院](../Page/革命实践研究院.md "wikilink")”成立。
  - [1951年](../Page/1951年.md "wikilink")：[華東師範大學在](../Page/华东师范大学.md "wikilink")[上海舉行開學典禮](../Page/上海.md "wikilink")，成為[中華人民共和國建國后成立的第一所師範大學](../Page/中華人民共和國.md "wikilink")。
  - [1954年](../Page/1954年.md "wikilink")：[毛泽东给中央政治局同志的一封信](../Page/毛泽东.md "wikilink")：《关于（[红楼梦](../Page/红楼梦.md "wikilink")）研究问题的信》，提出对[胡适派资产阶级](../Page/胡適.md "wikilink")[唯心主义思想进行批判的任务](../Page/唯心主義.md "wikilink")。
  - [1964年](../Page/1964年.md "wikilink")：[中国在](../Page/中华人民共和国.md "wikilink")[新疆](../Page/新疆.md "wikilink")[罗布泊成功试爆了首枚](../Page/罗布泊.md "wikilink")[原子弹](../Page/原子弹.md "wikilink")，成为第5个拥有[核武器的国家](../Page/核武器.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：震撼世界的[石油危机爆发](../Page/能源危機.md "wikilink")。
  - [1973年](../Page/1973年.md "wikilink")：美国国务卿[基辛格与](../Page/亨利·基辛格.md "wikilink")[越南](../Page/越南.md "wikilink")[黎德寿因对实现](../Page/黎德寿.md "wikilink")[印度支那和平所作的努力而同获](../Page/中南半島.md "wikilink")[诺贝尔和平奖](../Page/诺贝尔和平奖.md "wikilink")。
  - [1978年](../Page/1978年.md "wikilink")：[波兰](../Page/波兰.md "wikilink")[克拉科夫](../Page/克拉科夫.md "wikilink")[枢机主教卡罗尔](../Page/枢机主教.md "wikilink")·沃伊蒂瓦当选为[教皇](../Page/教皇.md "wikilink")，取名为[若望·保禄二世](../Page/若望·保禄二世.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：坐落在[巴黎南部意大利广场附近的](../Page/巴黎.md "wikilink")[周恩来纪念碑揭幕](../Page/周恩来.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[中国进行目前为止最后一次大气层](../Page/中国.md "wikilink")[核试验](../Page/核试验.md "wikilink")。
  - [1982年](../Page/1982年.md "wikilink")：中国潜艇向预定海域发射运载火箭成功。
  - [1984年](../Page/1984年.md "wikilink")：[南非黑人主教德](../Page/南非共和国.md "wikilink")[杜圖获该年度诺贝尔和平奖](../Page/杜圖.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：中国第一座高能加速器[北京正负电子对撞机首次对撞成功](../Page/北京正负电子对撞机.md "wikilink")。
  - [1988年](../Page/1988年.md "wikilink")：一艘由[香港駛往](../Page/香港.md "wikilink")[澳門的水翼船天皇星號](../Page/澳門.md "wikilink")，在抵達[澳門港澳碼頭時失事撞向防波堤](../Page/外港客運碼頭.md "wikilink")，共有85名乘客受傷，包括商人[何鴻燊](../Page/何鴻燊.md "wikilink")。
  - [1994年](../Page/1994年.md "wikilink")：[新光人壽首度舉辦](../Page/新光人壽.md "wikilink")「[新光摩天大樓登高大賽](../Page/新光摩天大樓登高大賽.md "wikilink")」，創下台灣第一個企業舉辦高樓登高大賽的紀錄。
  - [1995年](../Page/1995年.md "wikilink")：美国百万黑人男子大游行。
  - [1996年](../Page/1996年.md "wikilink")：[危地马拉足球场大惨案](../Page/危地马拉足球场大惨案.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：新版《[辞海](../Page/辞海.md "wikilink")》面世。

### 21世紀

  - [2005年](../Page/2005年.md "wikilink")：中國[神舟六号載人飛船返回艙在](../Page/神舟六号.md "wikilink")[内蒙古自治区中部的](../Page/内蒙古自治区.md "wikilink")[四子王旗阿木古郎草原著陸](../Page/四子王旗.md "wikilink")。
  - 2005年：[伊拉克憲法公投順利完成](../Page/伊拉克.md "wikilink")，投票率達到70%。
  - [2006年](../Page/2006年.md "wikilink")：[美國人口破](../Page/美國人口.md "wikilink")3億大關。
  - 2006年：俄美科学家制造出第118号元素[Og](../Page/Og.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：第11届全运会在山东省开幕，国家主席胡锦涛出席开幕式并宣布开幕。
  - [2010年](../Page/2010年.md "wikilink")：上海世博会第169天，当日票检入园人数达到103.27万人次，累计入园人数达到6462.14万，双双打破1970年大阪世博会创下的历史最高记录，正式成为参观人数最多的一届世博会。
  - 2010年：在河北大学发生了一起[醉酒驾驶交通肇事逃逸案件](../Page/河北大学“10·16”交通肇事案.md "wikilink")，造成在校学生一死一伤，肇事者叫嚣“我爸是李刚”，其后媒体将事件称作“李刚门”。
  - [2012年](../Page/2012年.md "wikilink")：中国西气东输三线工程开工。
  - [2013年](../Page/2013年.md "wikilink")：中国首条跨省城市轨道交通，[上海轨道交通11号线江苏省昆山花桥延伸段开通](../Page/上海轨道交通11号线.md "wikilink")。
  - [2016年](../Page/2016年.md "wikilink")：臺灣[臺中都會區鐵路高架捷運化計畫第一階段通車啟用](../Page/臺中都會區鐵路高架捷運化計畫.md "wikilink")。

## 出生

  - [1430年](../Page/1430年.md "wikilink")：[詹姆斯二世](../Page/詹姆斯二世_\(苏格兰\).md "wikilink")，[苏格兰](../Page/苏格兰王国.md "wikilink")[斯图亚特王朝国王](../Page/斯图亚特王朝.md "wikilink")（[1460年去世](../Page/1460年.md "wikilink")）
  - [1535年](../Page/1535年.md "wikilink")：[丹羽長秀](../Page/丹羽長秀.md "wikilink")，日本武將、[織田氏的家臣](../Page/織田氏.md "wikilink")（[1585年去世](../Page/1585年.md "wikilink")）
  - [1758年](../Page/1758年.md "wikilink")：[諾亞·韋伯斯特](../Page/諾亞·韋伯斯特.md "wikilink")，美国辞典编纂者（[1843年去世](../Page/1843年.md "wikilink")）
  - [1799年](../Page/1799年.md "wikilink")：[田中久重](../Page/田中久重.md "wikilink")，[日本](../Page/日本.md "wikilink")[發明家](../Page/發明家.md "wikilink")，企业家（
    [1881年去世](../Page/1881年.md "wikilink")）
  - [1841年](../Page/1841年.md "wikilink")：[伊藤博文](../Page/伊藤博文.md "wikilink")，[日本首相](../Page/日本首相.md "wikilink")（[1909年去世](../Page/1909年.md "wikilink")）
  - [1854年](../Page/1854年.md "wikilink")：[卡爾·考茨基](../Page/卡爾·考茨基.md "wikilink")，[德国社会主义活动家](../Page/德国.md "wikilink")（[1938年去世](../Page/1938年.md "wikilink")）
  - [1854年](../Page/1854年.md "wikilink")：[奥斯卡·王尔德](../Page/奥斯卡·王尔德.md "wikilink")，[英国唯美主义运动的倡导者](../Page/英国.md "wikilink")，剧作家（[1900年去世](../Page/1900年.md "wikilink")）
  - [1886年](../Page/1886年.md "wikilink")：[戴维·本-古里安](../Page/戴维·本-古里安.md "wikilink")，[以色列政治人物](../Page/以色列.md "wikilink")（[1973年去世](../Page/1973年.md "wikilink")）
  - [1888年](../Page/1888年.md "wikilink")：[尤金·奥尼尔](../Page/尤金·奥尼尔.md "wikilink")，[美国悲剧作家](../Page/美國.md "wikilink")，1936年[諾貝爾文學獎得主](../Page/诺贝尔文学奖.md "wikilink")（[1953年去世](../Page/1953年.md "wikilink")）
  - [1890年](../Page/1890年.md "wikilink")：[胡階森](../Page/胡階森.md "wikilink")，[英國駐華臨時代辦](../Page/英國駐華臨時代辦.md "wikilink")（[1965年去世](../Page/1965年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[威廉·道格拉斯](../Page/威廉·道格拉斯.md "wikilink")，美国法官（[1980年去世](../Page/1980年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[李惠堂](../Page/李惠堂.md "wikilink")，[中國足球員](../Page/中國.md "wikilink")（[1979年去世](../Page/1979年.md "wikilink")）
  - [1908年](../Page/1908年.md "wikilink")：[恩維爾·霍查](../Page/恩維爾·霍查.md "wikilink")，[阿尔巴尼亚领导人](../Page/阿尔巴尼亚.md "wikilink")（[1985年去世](../Page/1985年.md "wikilink")）
  - [1913年](../Page/1913年.md "wikilink")：[钱三强](../Page/錢三強.md "wikilink")，[中国物理学家](../Page/中国.md "wikilink")（[1992年去世](../Page/1992年.md "wikilink")）
  - [1914年](../Page/1914年.md "wikilink")：[穆罕默德·查希爾·沙阿](../Page/穆罕默德·查希爾·沙阿.md "wikilink")，[阿富汗末代國王](../Page/阿富汗.md "wikilink")（[2007年去世](../Page/2007年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[麥理浩](../Page/麥理浩.md "wikilink")，[英國資深](../Page/英國.md "wikilink")[外交官](../Page/外交官.md "wikilink")，第25任[香港總督](../Page/香港總督.md "wikilink")（[2000年去世](../Page/2000年.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[陳敬熊](../Page/陳敬熊.md "wikilink")，電磁與微波技術專家
  - [1927年](../Page/1927年.md "wikilink")：[君特·格拉斯](../Page/君特·格拉斯.md "wikilink")，[德国作家](../Page/德国.md "wikilink")，1999年[諾貝爾文學獎得主](../Page/诺贝尔文学奖.md "wikilink")
  - [1933年](../Page/1933年.md "wikilink")：[大山羨代](../Page/大山羨代.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/日本配音員.md "wikilink")
  - [1940年](../Page/1940年.md "wikilink")：[何守信](../Page/何守信.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")
  - [1948年](../Page/1948年.md "wikilink")：[希瑪·馬連尼](../Page/希瑪·馬連尼.md "wikilink")，[印度女演員和议员](../Page/印度.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[徐少強](../Page/徐少強.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[朱宗慶](../Page/朱宗慶.md "wikilink")，[台灣音乐人](../Page/台灣.md "wikilink")
  - [1958年](../Page/1958年.md "wikilink")：[提姆·羅賓斯](../Page/蒂姆·罗宾斯.md "wikilink")，[美國電影演員](../Page/美國.md "wikilink")、導演
  - [1959年](../Page/1959年.md "wikilink")：[小森真奈美](../Page/小森真奈美.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")、作家、節目主持人
  - [1970年](../Page/1970年.md "wikilink")：[梅赫梅特·绍尔](../Page/梅赫梅特·绍尔.md "wikilink")，[德国](../Page/德国.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")、[歌手](../Page/歌手.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[泳兒](../Page/泳兒.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[金亞中](../Page/金亞中.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[喬丹·拉爾森](../Page/喬丹·拉爾森_\(排球運動員\).md "wikilink")，美國女子排球運動員
  - [1992年](../Page/1992年.md "wikilink")：[布萊斯·哈波](../Page/布萊斯·哈波.md "wikilink")，[美國](../Page/美國.md "wikilink")，[美國職棒大聯盟](../Page/美國職業棒球大聯盟.md "wikilink")，[華盛頓國民外野手](../Page/華盛頓國民.md "wikilink")，2010年[選秀狀元](../Page/選秀狀元.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[夏爾·勒克萊爾](../Page/夏爾·勒克萊爾.md "wikilink")，[摩納哥](../Page/摩納哥.md "wikilink")[一級方程式賽車車手](../Page/一級方程式賽車.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[大坂直美](../Page/大坂直美.md "wikilink")，[日本](../Page/日本.md "wikilink")[職業網球運動員](../Page/職業網球運動員.md "wikilink")，2018年[美國網球公開賽冠軍](../Page/美國網球公開賽.md "wikilink")，2019年[澳洲網球公開賽冠軍](../Page/澳洲網球公開賽.md "wikilink")，[WTA世界排名第一](../Page/WTA.md "wikilink")

## 逝世

  - [385年](../Page/385年.md "wikilink")：前秦宣昭帝[苻坚](../Page/苻坚.md "wikilink")，[前秦皇帝](../Page/前秦.md "wikilink")（[338年出生](../Page/338年.md "wikilink")）
  - [1642年](../Page/1642年.md "wikilink")：[田貴妃](../Page/田秀英.md "wikilink")，[明朝](../Page/明朝.md "wikilink")[明思宗的妃子](../Page/明思宗.md "wikilink")
  - [1793年](../Page/1793年.md "wikilink")：[瑪麗·安托瓦內特](../Page/玛丽·安托瓦内特.md "wikilink")，[法國國王](../Page/法国君主列表.md "wikilink")[路易十六的王后](../Page/路易十六.md "wikilink")（[1755年出生](../Page/1755年.md "wikilink")）
  - [1861年](../Page/1861年.md "wikilink"):
    [车金光](../Page/车金光.md "wikilink"),[清朝中国基督教新教第一位殉道士](../Page/清朝.md "wikilink")（[1800年出生](../Page/1800年.md "wikilink")）
  - [1918年](../Page/1918年.md "wikilink")：[汉斯·劳](../Page/汉斯·劳.md "wikilink")，[丹麦天文学家](../Page/丹麦.md "wikilink")（[1879年出生](../Page/1879年.md "wikilink")）
  - [1937年](../Page/1937年.md "wikilink")：[郝梦龄](../Page/郝梦龄.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军将领](../Page/国民革命军.md "wikilink")
  - [1937年](../Page/1937年.md "wikilink")：[刘家麒](../Page/刘家麒.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军将领](../Page/国民革命军.md "wikilink")
  - [1937年](../Page/1937年.md "wikilink")：[鄭廷珍](../Page/鄭廷珍.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军将领](../Page/国民革命军.md "wikilink")
  - [1943年](../Page/1943年.md "wikilink")：[柳原愛子](../Page/柳原愛子.md "wikilink")，[明治天皇的](../Page/明治天皇.md "wikilink")[典侍](../Page/典侍.md "wikilink")，[大正天皇的生母](../Page/大正天皇.md "wikilink")（[1859年出生](../Page/1859年.md "wikilink")）
  - [1959年](../Page/1959年.md "wikilink")：[马歇尔](../Page/乔治·卡特莱特·马歇尔.md "wikilink")，[美国前国务卿](../Page/美国.md "wikilink")、五星上将（生于[1880年](../Page/1880年.md "wikilink")）
  - [1988年](../Page/1988年.md "wikilink")：[法莉達
    (埃及王后)](../Page/法莉達_\(埃及王后\).md "wikilink")，[埃及王后](../Page/埃及.md "wikilink")（[1921年出生](../Page/1921年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[艾德·罗特](../Page/艾德·罗特.md "wikilink")，美國男演員（生于[1938年](../Page/1938年.md "wikilink")）
  - [2014年](../Page/2014年.md "wikilink")：[彭明聰](../Page/彭明聰.md "wikilink")，台灣[中央研究院院士](../Page/中央研究院.md "wikilink")、[國立臺灣大學](../Page/國立臺灣大學.md "wikilink")[醫學院院長](../Page/國立臺灣大學醫學院.md "wikilink")、馬偕醫護管理專科學校校長、家扶基金會董事長、兒福聯盟董事長、兒童燙傷基金會董事長（生于[1917年](../Page/1917年.md "wikilink")）
  - [2017年](../Page/2017年.md "wikilink")：[严顺开](../Page/严顺开.md "wikilink")，[中华人民共和国](../Page/中华人民共和国.md "wikilink")[喜剧](../Page/喜剧.md "wikilink")[影视](../Page/影视.md "wikilink")[演员](../Page/演员.md "wikilink")，以根据[鲁迅原著拍摄的电影](../Page/鲁迅.md "wikilink")《[阿Q正传](../Page/阿Q正传.md "wikilink")》中的[阿Q角色而闻名](../Page/阿Q.md "wikilink")（生于[1937年](../Page/1937年.md "wikilink")）

## 节假日和习俗

  - [世界粮食日](../Page/世界粮食日.md "wikilink")（World Food Day）