**自然大调**（经常简称为**大调**），即[中古调式中的](../Page/中古调式.md "wikilink")[伊奥尼亚调式](../Page/伊奥尼亚调式.md "wikilink")（），是[调式的一种](../Page/调式.md "wikilink")。自然大调包含七个[音符](../Page/音符.md "wikilink")，在[唱名中分別是Do](../Page/唱名.md "wikilink")、Re、Mi、Fa、Sol、La、Si（或Ti）。[C大调是大调中最](../Page/C大调.md "wikilink")“简单”的一个[调性](../Page/调性.md "wikilink")，因为它是唯一一个沒有任何[升號](../Page/升號.md "wikilink")（♯）和[降號](../Page/降號.md "wikilink")（♭）的大調，在[钢琴中只需弹奏白键](../Page/钢琴.md "wikilink")。

比某个大调低小三度的[小调](../Page/小调.md "wikilink")，称为该大调的[关系小调](../Page/平行大小调.md "wikilink")。

## 结构

自然大调中的音符从[主音开始](../Page/主音.md "wikilink")，按照音高依次为主音、[上主音](../Page/上主音.md "wikilink")、[中音](../Page/中音.md "wikilink")、[下属音](../Page/下屬音.md "wikilink")、[属音](../Page/属音.md "wikilink")、[下中音和](../Page/下中音.md "wikilink")[下主音](../Page/下主音.md "wikilink")，也可以称作主音、二级音、三级音、四级音等等。除了中音和下属音，下主音和主音之间是[小二度](../Page/小二度.md "wikilink")，其他相邻两音之间都是[大二度](../Page/大二度.md "wikilink")。
[CMajorScale.png](https://zh.wikipedia.org/wiki/File:CMajorScale.png "fig:CMajorScale.png")

## 各种大调

沒有任何升降号的自然大调只有[C大调一种](../Page/C大调.md "wikilink")，其关系小调是[a小调](../Page/a小调.md "wikilink")，平行小调是[c小调](../Page/c小调.md "wikilink")。

### 有升号的大调

共有七种带升号的大调，分别有一至七个升号。将某个带升号大调的属音作为主音，即构成比它增加一个升号的大调。在[调性标记中的升号就是按照这种顺序排列的](../Page/调性标记.md "wikilink")。

| 名称                                 | [关系小调](../Page/关系小调.md "wikilink") | [同名小调](../Page/同名小调.md "wikilink") | [音阶](../Page/音阶.md "wikilink")                                                                                |
| ---------------------------------- | ---------------------------------- | ---------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| [G大调](../Page/G大调.md "wikilink")   | [e小调](../Page/e小调.md "wikilink")   | [g小调](../Page/g小调.md "wikilink")   | [<File:GMajorScale.png>](https://zh.wikipedia.org/wiki/File:GMajorScale.png "fig:File:GMajorScale.png")       |
| [D大调](../Page/D大调.md "wikilink")   | [b小调](../Page/b小调.md "wikilink")   | [d小调](../Page/d小调.md "wikilink")   | [<File:DMajorScale.png>](https://zh.wikipedia.org/wiki/File:DMajorScale.png "fig:File:DMajorScale.png")       |
| [A大调](../Page/A大调.md "wikilink")   | [升f小调](../Page/升f小调.md "wikilink") | [a小调](../Page/a小调.md "wikilink")   | [<File:AMajorScale.png>](https://zh.wikipedia.org/wiki/File:AMajorScale.png "fig:File:AMajorScale.png")       |
| [E大调](../Page/E大调.md "wikilink")   | [升c小调](../Page/升c小调.md "wikilink") | [e小调](../Page/e小调.md "wikilink")   | [<File:EMajorScale.png>](https://zh.wikipedia.org/wiki/File:EMajorScale.png "fig:File:EMajorScale.png")       |
| [B大调](../Page/B大调.md "wikilink")   | [升g小调](../Page/升g小调.md "wikilink") | [b小调](../Page/b小调.md "wikilink")   | [<File:BMajorScale.png>](https://zh.wikipedia.org/wiki/File:BMajorScale.png "fig:File:BMajorScale.png")       |
| [升F大调](../Page/升F大调.md "wikilink") | [升d小调](../Page/升d小调.md "wikilink") | [升f小调](../Page/升f小调.md "wikilink") | [<File:FisMajorScale.png>](https://zh.wikipedia.org/wiki/File:FisMajorScale.png "fig:File:FisMajorScale.png") |
| [升C大调](../Page/升C大调.md "wikilink") | [升a小调](../Page/升a小调.md "wikilink") | [升c小调](../Page/升c小调.md "wikilink") | [<File:CisMajorScale.png>](https://zh.wikipedia.org/wiki/File:CisMajorScale.png "fig:File:CisMajorScale.png") |

### 有降号的大调

共有七种带降号的大调，分别有一至七个降号。将某个带降号大调的下属音作为主音，即构成比它增加一个降号的大调。在调性标记中的降号就是按照这种顺序排列的。

| 名称                                 | [关系小调](../Page/关系小调.md "wikilink") | [同名小调](../Page/同名小调.md "wikilink") | [音阶](../Page/音阶.md "wikilink")                                                                                               |
| ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| [F大調](../Page/F大調.md "wikilink")   | [d小調](../Page/d小調.md "wikilink")   | [f小調](../Page/f小調.md "wikilink")   | [<File:FMajorScale.png>](https://zh.wikipedia.org/wiki/File:FMajorScale.png "fig:File:FMajorScale.png")                      |
| [降B大調](../Page/降B大調.md "wikilink") | [g小調](../Page/g小調.md "wikilink")   | [降b小調](../Page/降b小調.md "wikilink") | [<File:B-flat> major scale.svg](https://zh.wikipedia.org/wiki/File:B-flat_major_scale.svg "fig:File:B-flat major scale.svg") |
| [降E大調](../Page/降E大調.md "wikilink") | [c小調](../Page/c小調.md "wikilink")   | [降e小調](../Page/降e小調.md "wikilink") | [<File:EesMajorScale.png>](https://zh.wikipedia.org/wiki/File:EesMajorScale.png "fig:File:EesMajorScale.png")                |
| [降A大調](../Page/降A大調.md "wikilink") | [f小調](../Page/f小調.md "wikilink")   | [降a小調](../Page/降a小調.md "wikilink") | [<File:AesMajorScale.png>](https://zh.wikipedia.org/wiki/File:AesMajorScale.png "fig:File:AesMajorScale.png")                |
| [降D大調](../Page/降D大調.md "wikilink") | [降b小調](../Page/降b小調.md "wikilink") | [升c小調](../Page/升c小調.md "wikilink") | [<File:DesMajorScale.png>](https://zh.wikipedia.org/wiki/File:DesMajorScale.png "fig:File:DesMajorScale.png")                |
| [降G大調](../Page/降G大調.md "wikilink") | [降e小調](../Page/降e小調.md "wikilink") | [升f小調](../Page/升f小調.md "wikilink") | [<File:GesMajorScale.png>](https://zh.wikipedia.org/wiki/File:GesMajorScale.png "fig:File:GesMajorScale.png")                |
| [降C大調](../Page/降C大調.md "wikilink") | [降a小調](../Page/降a小調.md "wikilink") | [b小調](../Page/b小調.md "wikilink")   | [<File:CesMajorScale.png>](https://zh.wikipedia.org/wiki/File:CesMajorScale.png "fig:File:CesMajorScale.png")                |

## 同音异名的大调

有三对大调是同音异名的，它们是B大调与降C大调、升F大调和降G大调，以及升C大调与降D大调。同音异名的调虽然给人不同的[调性感觉](../Page/调性感觉.md "wikilink")，但在[平均律中音高是完全相同的](../Page/平均律.md "wikilink")。

## 五度圈

[Fifths.png](https://zh.wikipedia.org/wiki/File:Fifths.png "fig:Fifths.png")
亦可以使用「[五度圈](../Page/五度圈.md "wikilink")」分辨各種大調，右邊為有升號的大調，左邊為有降號的大調。順時針方向是每一次升[純五度](../Page/純五度.md "wikilink")（如C的順時針方向下一個是G），逆時針方向是每一次降純五度（如C的逆時針方向下一個是F）。

## 關聯項目

  - [小調](../Page/小調.md "wikilink")

## 参考文献

<div class="references-small">

1.  Eric Taylor, *The AB Guide to Music Theory PART I*, the Associates
    Board of the Royal Schools of Music(Publishing) Ltd, 1989, Chapter
    2: Introduction to Pitch, 2/2 The major scale (P.11-12), Chapter4:
    More Scales, Keys and Clef, 4/1 Major scales and the circle of
    fifths, 4/2 Minor scales and keys, 4/3 Relative major/minor keys
    (P.21-27), ISBN 978-1-85472-446-5

2.  [关于大调与小调的简要说明](https://web.archive.org/web/20170113004952/http://www.flutefriends.com/flutebook/Major%26minor_explain.htm)

3.

</div>

[Category:音樂理論](../Category/音樂理論.md "wikilink")
[Category:音階](../Category/音階.md "wikilink")
[Category:調](../Category/調.md "wikilink")