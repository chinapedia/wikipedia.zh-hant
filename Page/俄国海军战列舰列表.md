这是一份1862年\~1948年间[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")/[苏联海军序列中的](../Page/苏联海军.md "wikilink")[战列舰列表](../Page/战列舰.md "wikilink")。

## 早期蒸汽战舰

|          艦名           |                         級別                          |                        附註                         |
| :-------------------: | :-------------------------------------------------: | :-----------------------------------------------: |
|      Севастополь      |     [塞瓦斯托波尔号](../Page/塞瓦斯托波尔号装甲舰.md "wikilink")     |                                                   |
|    «Петропавловск»    |  [彼得罗巴甫洛夫斯克号](../Page/彼得罗巴甫洛夫斯克号装甲舰.md "wikilink")  |                                                   |
|                       |      [佩尔维涅茨号](../Page/佩尔维涅茨号装甲舰.md "wikilink")      |     [佩尔维涅茨级](../Page/佩尔维涅茨级装甲舰.md "wikilink")     |
|                       |     [内特伦·门雅号](../Page/内特伦·门雅号装甲舰.md "wikilink")     |                      佩尔维涅茨级                       |
|                       |       [克里姆林号](../Page/克里姆林号装甲舰.md "wikilink")       |                      佩尔维涅茨级                       |
|   «Князь Пожарский»   |    [波扎尔斯基公爵号](../Page/波扎尔斯基公爵号装甲舰.md "wikilink")    |                                                   |
|        «Минин»        |         [米宁号](../Page/米宁号装甲舰.md "wikilink")         |                                                   |
|   «Генерал-адмирал»   |       [海军上将号](../Page/海军上将号装甲舰.md "wikilink")       |                                                   |
| «Герцог Эдинбургский» |      [爱丁堡公爵号](../Page/爱丁堡公爵号装甲舰.md "wikilink")      |                                                   |
|   «Дмитрий Донской»   |  [德米特里·顿斯科伊号](../Page/德米特里·顿斯科伊号装甲舰.md "wikilink")  | [德米特里·顿斯科伊级](../Page/德米特里·顿斯科伊级装甲舰.md "wikilink") |
|  «Владимир Мономах»   | [弗拉基米尔·莫诺马赫号](../Page/弗拉基米尔·莫诺马赫号装甲舰.md "wikilink") |                    德米特里·顿斯科伊级                     |
|   «Адмирал Нахимов»   |   [纳希莫夫海军上将号](../Page/纳希莫夫海军上将号装甲舰.md "wikilink")   |                                                   |
|                       |                                                     |                                                   |

## [前无畏舰](../Page/前无畏舰.md "wikilink")

|                       艦名                       |                                           級別                                           |                  附註（第一個年份為下水年份）                   |
| :--------------------------------------------: | :------------------------------------------------------------------------------------: | :-----------------------------------------------: |
|                 «Пётр Великий»                 |                        [彼得大帝号](../Page/彼得大帝号装甲舰.md "wikilink")                         |                                                   |
|                 «Екатерина II»                 |                     [叶卡捷琳娜二世号](../Page/叶卡捷琳娜二世号战列舰.md "wikilink")                      |   [叶卡捷琳娜二世级](../Page/叶卡捷琳娜二世级战列舰.md "wikilink")   |
|                    «Чесма»                     |                          [切斯马号](../Page/切斯馬號戰艦.md "wikilink")                          |                     叶卡捷琳娜二世级                      |
|                    «Синоп»                     |                         [锡诺普号](../Page/锡诺普号战列舰.md "wikilink")                          |                     叶卡捷琳娜二世级                      |
|             «Георгий Победоносец»              |                       [胜利者乔治号](../Page/胜利者乔治号战列舰.md "wikilink")                        |                     叶卡捷琳娜二世级                      |
|            «Император Александр II»            |                    [皇帝亚历山大二世号](../Page/皇帝亚历山大二世号战列舰.md "wikilink")                     |  [皇帝亚历山大二世级](../Page/皇帝亚历山大二世级战列舰.md "wikilink")  |
|             «Император Николай I»              |                [皇帝尼古拉一世号](../Page/皇帝尼古拉一世号战列舰_\(1889年\).md "wikilink")                 |                     皇帝亚历山大二世级                     |
|             «Двенадцать Апостолов»             |                        [十二使徒号](../Page/十二使徒号战列舰.md "wikilink")                         |                                                   |
|                    «Гангут»                    |                    [甘古特号](../Page/甘古特号战列舰_\(1888年\).md "wikilink")                     |                                                   |
|                   «Наварин»                    |                         [纳瓦林号](../Page/纳瓦林号战列舰.md "wikilink")                          |                                                   |
|                « Три Святителя»                |                         [三圣徒号](../Page/三圣徒号战列舰.md "wikilink")                          |                                                   |
|                «Сисой Великий»                 |                      [伟大的西索伊号](../Page/伟大的西索伊号战列舰.md "wikilink")                       |                                                   |
|                «Петропавловск»                 |              [彼得罗巴甫洛夫斯克号](../Page/彼得罗巴甫洛夫斯克号战列舰_\(1894年\).md "wikilink")               | [彼得罗巴甫洛夫斯克级](../Page/彼得罗巴甫洛夫斯克级战列舰.md "wikilink") |
|                   «Полтава»                    |                   [波尔塔瓦号](../Page/波尔塔瓦号战列舰_\(1894年\).md "wikilink")                    |                    彼得罗巴甫洛夫斯克级                     |
|                 «Севастополь»                  |                 [塞瓦斯托波尔号](../Page/塞瓦斯托波尔号战列舰_\(1895年\).md "wikilink")                  |                    彼得罗巴甫洛夫斯克级                     |
|                  «Ростислав»                   |                      [罗斯季斯拉夫号](../Page/罗斯季斯拉夫号战列舰.md "wikilink")                       |                                                   |
|                   «Пересвет»                   |                       [佩列斯韦特号](../Page/佩列斯韦特号战列舰.md "wikilink")                        |     [佩列斯韦特级](../Page/佩列斯韦特级战列舰.md "wikilink")     |
|                    «Ослябя»                    |                      [奥斯利亚比亚号](../Page/奥斯利雅维亚号战列舰.md "wikilink")                       |                      佩列斯韦特级                       |
|                    «Победа»                    |                     [胜利号](../Page/胜利号战列舰_\(1900年\).md "wikilink")                      |                      佩列斯韦特级                       |
| «Князь Потемкин-Таврический» （ «Пантелеймон» ） | [波将金号](../Page/波将金号战列舰.md "wikilink")（1905年改名為[潘捷列伊蒙號](../Page/波将金号战列舰.md "wikilink")） |                                                   |
|                   «Ретвизан»                   |                        [列特维赞号](../Page/列特维赞号战列舰.md "wikilink")                         |                                                   |
|                  «Цесаревич»                   |                         [皇太子号](../Page/皇太子号战列舰.md "wikilink")                          |                                                   |
|                   «Бородино»                   |                        [博罗季诺号](../Page/博罗季诺号战列舰.md "wikilink")                         |      [博罗季诺级](../Page/博罗季诺级战列舰.md "wikilink")      |
|           «Император Александр III»            |                    [皇帝亚历山大三世号](../Page/皇帝亚历山大三世号战列舰.md "wikilink")                     |                       博罗季诺级                       |
|                     «Орёл»                     |                      [鹰号（或音译，奥廖尔号）](../Page/鹰号战列舰.md "wikilink")                       |                       博罗季诺级                       |
|                «Князь Суворов»                 |                      [苏沃洛夫亲王号](../Page/苏沃洛夫亲王号战列舰.md "wikilink")                       |                       博罗季诺级                       |
|                    «Слава»                     |                          [光荣号](../Page/光荣号战列舰.md "wikilink")                           |                       博罗季诺级                       |
|                   «Евстафий»                   |                       [叶夫斯塔菲号](../Page/叶夫斯塔菲号战列舰.md "wikilink")                        |     [叶夫斯塔菲级](../Page/叶夫斯塔菲级战列舰.md "wikilink")     |
|                «Иоанн Златоуст»                |                        [金口约翰号](../Page/金口约翰号战列舰.md "wikilink")                         |                      叶夫斯塔菲级                       |
|             «Андрей Первозванный»              |                      [首召者安德列号](../Page/首召者安德列号战列舰.md "wikilink")                       |    [首召者安德列级](../Page/首召者安德列級戰艦.md "wikilink")     |
|              «Император Павел I»               |                      [皇帝保罗一世号](../Page/皇帝保罗一世号战列舰.md "wikilink")                       |                      首召者安德列级                      |
|                                                |                                                                                        |                                                   |

## [无畏舰](../Page/无畏舰.md "wikilink")

|               艦名                |                                                 級別                                                  |              附註（第一個年份為下水年份）               |
| :-----------------------------: | :-------------------------------------------------------------------------------------------------: | :---------------------------------------: |
|          «Севастополь»          |  [塞瓦斯托波尔号](../Page/塞瓦斯托波尔号战列舰_\(1911年\).md "wikilink") （后改舰名为[巴黎公社号](../Page/巴黎公社号.md "wikilink")）  |   [甘古特级](../Page/甘古特级战列舰.md "wikilink")   |
|            «Полтава»            |     [波尔塔瓦号](../Page/波尔塔瓦号战列舰_\(1911年\).md "wikilink") （后改舰名为[伏龙芝号](../Page/伏龙芝号.md "wikilink")）     |                   甘古特级                    |
|         «Петропавловск»         | [彼得罗巴甫洛夫斯克号](../Page/彼得罗巴甫洛夫斯克号战列舰_\(1914年\).md "wikilink") （后改舰名为[马拉号](../Page/马拉号.md "wikilink")） |                   甘古特级                    |
|            «Гангут»             |                           [甘古特号](../Page/甘古特号战列舰_\(1911年\).md "wikilink")                           |                   甘古特级                    |
| «Императрица Екатерина Великая» | [女皇叶卡捷琳娜大帝号](../Page/女皇叶卡捷琳娜大帝号战列舰.md "wikilink") （1917年改名为[自由俄罗斯号](../Page/自由俄罗斯号.md "wikilink")）  | [玛丽亚皇后级](../Page/玛丽亚皇后级战列舰.md "wikilink") |
|       «Императрица Мария»       |                              [玛丽亚皇后号](../Page/玛丽亚皇后号战列舰.md "wikilink")                              |                  玛丽亚皇后级                   |
|    «Император Александр III»    |                           [皇帝亚历山大三世号](../Page/皇帝亚历山大三世号战列舰.md "wikilink")                           |                  玛丽亚皇后级                   |
|      «Император Николай I»      |                       [皇帝尼古拉一世号](../Page/皇帝尼古拉一世號戰艦_\(1916年\).md "wikilink")                        |                 皇帝尼古拉一世级                  |
|                                 |                                                                                                     |                                           |

## 超无畏艦

|           艦名           |                                                             級別                                                             |                 附註                  |
| :--------------------: | :------------------------------------------------------------------------------------------------------------------------: | :---------------------------------: |
|    «Советский Союз»    |                                                            苏联号                                                             | [苏联级](../Page/苏联级战列舰.md "wikilink") |
|  «Советская Украина»   |                                                          苏维埃乌克兰号                                                           |                 苏联级                 |
| «Советская Белоруссия» |                                                          苏维埃白俄罗斯号                                                          |                 苏联级                 |
|   «Советская Россия»   |                                                          苏维埃俄罗斯号                                                           |                 苏联级                 |
|                        |  [阿尔汉格尔斯克号](../Page/阿尔汉格尔斯克号.md "wikilink") 即[英国皇家海军](../Page/英国皇家海军.md "wikilink")[君权号战列舰](../Page/君权号战列舰.md "wikilink")  |                                     |
|                        | [新罗西斯克号](../Page/新罗西斯克号.md "wikilink") 即[意大利海军](../Page/意大利海军.md "wikilink")[朱利奥·凯撒号战列舰](../Page/朱利奥·凯撒号战列舰.md "wikilink") |                                     |
|                        |                                                                                                                            |                                     |

## 外部链接

  - [Maritimequest Russian battleship
    index](http://www.maritimequest.com/warship_directory/russia/battleship_index.htm)

[Category:战列舰列表](../Category/战列舰列表.md "wikilink")
[Category:苏联军舰](../Category/苏联军舰.md "wikilink")
[Category:苏联军事列表](../Category/苏联军事列表.md "wikilink")
[Category:俄罗斯帝国海军战舰](../Category/俄罗斯帝国海军战舰.md "wikilink")