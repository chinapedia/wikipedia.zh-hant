**率芒溪**又名**士文溪**，位於[台灣南部](../Page/台灣.md "wikilink")，為一[縣市管河川](../Page/縣市管河川.md "wikilink")，幹流長度22.30公里，流域面積89.61平方公里\[1\]，分佈於[屏東縣中部地區](../Page/屏東縣.md "wikilink")，包含[枋山鄉北端](../Page/枋山鄉.md "wikilink")、[枋寮鄉南端](../Page/枋寮鄉.md "wikilink")、[春日鄉南半部及](../Page/春日鄉.md "wikilink")[獅子鄉北端](../Page/獅子鄉_\(台灣\).md "wikilink")。主流發源於標高1,688公尺之[大漢山西麓](../Page/大漢山.md "wikilink")，向西南西流經基基努古、舊古華、埤寮，於保生注入[台灣海峽](../Page/台灣海峽.md "wikilink")。

## 水系主要河川

  - **率芒溪**：[屏東縣](../Page/屏東縣.md "wikilink")[枋山鄉](../Page/枋山鄉.md "wikilink")、[枋寮鄉](../Page/枋寮鄉.md "wikilink")、[春日鄉](../Page/春日鄉.md "wikilink")、[獅子鄉](../Page/獅子鄉_\(台灣\).md "wikilink")
      - [草山溪](../Page/草山溪.md "wikilink")：枋山鄉、春日鄉、獅子鄉

## 相關條目

  - [縣市管河川](../Page/縣市管河川.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")

## 參考資料及註釋

<div class="references-small">

<references />

</div>

[category:台灣縣市管河川水系](../Page/category:台灣縣市管河川水系.md "wikilink")
[category:屏東縣河川](../Page/category:屏東縣河川.md "wikilink")

[Category:春日鄉](../Category/春日鄉.md "wikilink")

1.  經濟部水利署[水利櫥窗：讓我們看河去(縣市管河川)--
    楓港溪](http://www.wra.gov.tw/ct.asp?xItem=20068&CtNode=4390)