**乾明**（560年正月—八月）是[北齊廢帝](../Page/北齊.md "wikilink")[高殷的](../Page/高殷.md "wikilink")[年号](../Page/年号.md "wikilink")，歷時數月。

[段長基的](../Page/段長基.md "wikilink")《[歷代統紀表](../Page/歷代統紀表.md "wikilink")》卷七認爲[西魏君主](../Page/西魏.md "wikilink")[元欽有乾明元年及二年](../Page/元欽.md "wikilink")，但是其他诸书均无记载，都称元欽没有年号。

## 大事记

## 出生

## 逝世

## 纪年

| 乾明                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 560年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [天啓](../Page/天启_\(萧庄\).md "wikilink")（558年三月－560年二月）：[南朝梁政權永嘉王](../Page/南朝梁.md "wikilink")[蕭莊的年号](../Page/蕭莊.md "wikilink")
      - [大定](../Page/大定_\(萧詧\).md "wikilink")（555年正月－562年正月）：[西梁政權梁宣帝](../Page/西梁.md "wikilink")[蕭詧的年号](../Page/蕭詧.md "wikilink")
      - [天嘉](../Page/天嘉.md "wikilink")（560年正月－566年二月）：[南朝陈政權陳文帝](../Page/南朝陈.md "wikilink")[陈蒨的年号](../Page/陈蒨.md "wikilink")
      - [武成](../Page/武成_\(宇文毓\).md "wikilink")（559年八月－560年十二月）：[北周政权周明帝](../Page/北周.md "wikilink")[宇文毓年号](../Page/宇文毓.md "wikilink")
      - [建昌](../Page/建昌_\(麴寶茂\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴寶茂年号](../Page/麴寶茂.md "wikilink")
      - [開國](../Page/開國.md "wikilink")（551年－568年）：[新羅](../Page/新羅.md "wikilink")[真興王之年號](../Page/新羅真興王.md "wikilink")

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北齐年号](../Category/北齐年号.md "wikilink")
[Category:560年代中国政治](../Category/560年代中国政治.md "wikilink")
[Category:560年](../Category/560年.md "wikilink")