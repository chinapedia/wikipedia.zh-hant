**飯富虎昌**（1504年－1565年11月11日）是[日本戰國時代的武將](../Page/日本戰國時代.md "wikilink")。[甲斐國](../Page/甲斐國.md "wikilink")[武田氏重臣](../Page/武田氏.md "wikilink")。父親是[飯富道悦或](../Page/飯富道悦.md "wikilink")[飯富源四郎](../Page/飯富源四郎.md "wikilink")。

從[武田信虎時期已經擔任武田家的](../Page/武田信虎.md "wikilink")[譜代](../Page/譜代.md "wikilink")[家老](../Page/家老.md "wikilink")，領有[信濃國](../Page/信濃國.md "wikilink")。

出自，屬於[源義家的四男](../Page/源義家.md "wikilink")[義忠的兒子](../Page/源義忠.md "wikilink")[飯富忠宗的後裔](../Page/飯富忠宗.md "wikilink")；亦有出身於古代的一說。

## 生平

### 信虎時代

在[永正元年](../Page/永正.md "wikilink")（1504年）出生（有異說）。[永正](../Page/永正.md "wikilink")12年（1515年）10月17日，[飯富道閲在與西郡的](../Page/飯富道閲.md "wikilink")[國人](../Page/國人.md "wikilink")[大井信達戰鬥時](../Page/大井信達.md "wikilink")，與兒子「源四郎」一同戰死（『』），一說指[飯富源四郎才是虎昌和](../Page/飯富源四郎.md "wikilink")[山縣昌景兄弟的父親](../Page/山縣昌景.md "wikilink")。

[享祿](../Page/享祿.md "wikilink")4年（1531年），與[今井信元和](../Page/今井信元.md "wikilink")[栗原兵庫等人一同向](../Page/栗原兵庫.md "wikilink")[武田信虎舉起反旗](../Page/武田信虎.md "wikilink")，但是在投降後被原諒並臣從於信虎。[天文](../Page/天文_\(後奈良天皇\).md "wikilink")5年（1536年），在[北條氏綱侵攻](../Page/北條氏綱.md "wikilink")[駿河國時](../Page/駿河國.md "wikilink")，與信虎一同作為[今川氏的援軍參戰並大破北條軍](../Page/今川氏.md "wikilink")。天文7年（1538年），與[諏訪賴滿和](../Page/諏訪賴滿.md "wikilink")[村上義清的連合軍戰鬥](../Page/村上義清.md "wikilink")，以寡兵數度擊破連合軍，並取下97個首級而立下軍功。

天文10年（1541年），與武田家的[宿老和有力國人](../Page/宿老.md "wikilink")[板垣信方](../Page/板垣信方.md "wikilink")、[甘利虎泰一同擁立信虎的嫡男](../Page/甘利虎泰.md "wikilink")[晴信](../Page/武田晴信.md "wikilink")，並把信虎追放到駿河，以後成為武田家的宿老並仕於晴信。

### 信玄時代

[thumb所藏品](../Page/image:Obu-toramasa.jpg.md "wikilink")）\]\]
天文17年（1548年），在信方和虎泰於[上田原之戰中戰死後](../Page/上田原之戰.md "wikilink")，成為武田軍團中的核心。

天文22年（1553年），在自身守備的被[長尾景虎](../Page/長尾景虎.md "wikilink")（上杉謙信）和[村上義清的](../Page/村上義清.md "wikilink")8千士兵圍困時，以8百兵力將其撃退。[永祿](../Page/永祿.md "wikilink")4年（1561年），在第4次[川中島之戰中](../Page/川中島之戰.md "wikilink")，擔任攻撃的別働隊大將等，以重臣身份為[武田氏的發展盡力](../Page/武田氏.md "wikilink")，而且還被[信玄任命為嫡男](../Page/武田信玄.md "wikilink")[義信的傅役](../Page/武田義信.md "wikilink")（[後見人](../Page/後見人.md "wikilink")）等，作為武田氏第一宿老而相當有份量。

### 義信事件

不過，義信和信玄之間的關係不佳（『[甲陽軍鑑](../Page/甲陽軍鑑.md "wikilink")』）。在對[今川氏的方針上](../Page/今川氏.md "wikilink")，父子之間的對立加深後，在義信企圖謀反時被捕（告密者是弟弟（一說指是外甥）三郎兵衛，即後來的[山縣昌景](../Page/山縣昌景.md "wikilink")），在[永祿](../Page/永祿.md "wikilink")8年（1565年）10月19日負起責任而自殺。享年62歲。墓所在[山梨縣](../Page/山梨縣.md "wikilink")[甲斐市龜澤](../Page/甲斐市.md "wikilink")（舊[中巨摩郡](../Page/中巨摩郡.md "wikilink")）的天澤寺。

關於處刑日期，[平山優在](../Page/平山優.md "wikilink")2001年推測是在永祿8年9月至10月期間；[丸島和洋在](../Page/丸島和洋.md "wikilink")2006年根據[高野山](../Page/高野山.md "wikilink")[成慶院的](../Page/成慶院.md "wikilink")『[甲斐國供養帳](../Page/甲斐國供養帳.md "wikilink")』的記載，死忌是永祿8年10月15日。

此後，飯富家斷絶，家臣團中的由弟弟三郎兵衛繼承。

## 子孫

兒子有[昌時](../Page/飯富昌時.md "wikilink")，在義信事件後，依靠與[武田氏有親戚關係的](../Page/武田氏.md "wikilink")[公家](../Page/公家.md "wikilink")並改姓[古屋氏](../Page/古屋氏.md "wikilink")。另外，在[天文](../Page/天文_\(後奈良天皇\).md "wikilink")22年（1553年）9月的第一次川中島之戰中，被派遣前往救援受[長尾氏侵攻的](../Page/長尾氏.md "wikilink")[信濃國](../Page/信濃國.md "wikilink")[苅屋原城的](../Page/苅屋原城.md "wikilink")「飯富左京亮」，與在天文20年（1551年）殺死[東條氏的](../Page/東條氏.md "wikilink")「飯富藤藏（稻藏）」是同一人物，被認為是虎昌的兒子。飯富左京亮在永祿2年（1559年）的[相模國](../Page/相模國.md "wikilink")[後北條氏的](../Page/後北條氏.md "wikilink")『[小田原眾所領役帳](../Page/小田原眾所領役帳.md "wikilink")』中，被記載為「他國眾」。

## 自殺原因

虎昌的自殺理由有諸種説法：

  - 三郎兵衛的說法指謀反的計劃是由虎昌策劃；
  - 為了包庇義信，自認是首謀者；
  - 反對信玄的[信濃經略和與](../Page/信濃國.md "wikilink")[上杉謙信的數度抗爭](../Page/上杉謙信.md "wikilink")；
  - 還有因為在武田家中持有很大勢力，以義信事件為契機的信玄藉此來肅清虎昌

[穴山信君的弟弟](../Page/穴山信君.md "wikilink")[信嘉](../Page/穴山信嘉.md "wikilink")（信邦）亦因此事遭到連座而[切腹](../Page/切腹.md "wikilink")，所以亦可能是對親今川派[國人的反擊](../Page/國人.md "wikilink")。

## 人物

  - 仕於[武田信虎和](../Page/武田信虎.md "wikilink")[武田信玄兩代而成為](../Page/武田信玄.md "wikilink")[宿老中的宿老](../Page/宿老.md "wikilink")，因為剛勇而被稱為**甲山之猛虎**，是敵方甚至己方都對其恐懼的猛將，被列為[武田二十四將之一](../Page/武田二十四將.md "wikilink")。

<!-- end list -->

  - 率領的部隊全員都穿著赤紅色的軍裝，作為武田的[赤備](../Page/赤備.md "wikilink")，成為精強武田軍團的代名詞。後來由弟弟[山縣昌景開始](../Page/山縣昌景.md "wikilink")，[井伊直政和](../Page/井伊直政.md "wikilink")[真田信繁](../Page/真田信繁.md "wikilink")（幸村）等人亦有使用赤備的軍裝。

## 登場作品

  - 電視劇

<!-- end list -->

  - [天與地](../Page/天與地_\(大河劇\).md "wikilink")（1969年，[NHK大河劇](../Page/NHK大河劇.md "wikilink")，演員：[高橋昌也](../Page/高橋昌也.md "wikilink")）
  - [武田信玄](../Page/武田信玄_\(大河劇\).md "wikilink")（1988年，NHK大河劇，演員：[兒玉清](../Page/兒玉清.md "wikilink")）
  - [風林火山](../Page/風林火山_\(大河劇\).md "wikilink")（2007年，NHK大河劇，演員：[金田明夫](../Page/金田明夫.md "wikilink")）

## 外部連結

  - [武家家伝＿飯富氏](http://www2.harimaya.com/sengoku/html/k_obu.html)

## 相關條目

  -
  - [赤備](../Page/赤備.md "wikilink")

[category:武田二十四將](../Page/category:武田二十四將.md "wikilink")

[Category:飯富氏](../Category/飯富氏.md "wikilink")
[Category:1504年出生](../Category/1504年出生.md "wikilink")
[Category:1565年逝世](../Category/1565年逝世.md "wikilink")
[Category:日本自殺者](../Category/日本自殺者.md "wikilink")