**曾愷玹**（，），台灣[演員](../Page/演員.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")，因擔綱電影《[不能說的秘密](../Page/不能說的秘密.md "wikilink")》女配角而聞名；2013年12月23日於臉書上公佈結婚訊息\[1\]\[2\]。

## 出道

曾愷玹畢業於[世新大學觀光學系餐旅事業管理組](../Page/世新大學.md "wikilink")；出道前，曾經參加台灣第十三屆「才藝美少女」選拔，獲得第二名。由於外型清新、氣質出眾，曾初軒期以擔任多部[MV女主角演出](../Page/MV.md "wikilink")，包括在[古巨基的MV](../Page/古巨基.md "wikilink")《情歌王》和[周杰倫的MV](../Page/周杰倫.md "wikilink")《我不配》等。最為人熟知的是她在周杰倫所執導和演出的電影《[不能說的秘密](../Page/不能說的秘密.md "wikilink")》裡飾演晴依一角，入圍了第44屆金馬獎最佳女配角；後來逐漸於平面、電視廣告和[臺灣電視劇](../Page/臺灣電視劇.md "wikilink")、[偶像劇中擔綱要角](../Page/偶像劇.md "wikilink")。

## 家庭

2013年12月23日，在[臉書上公布與身為艺廊经营者的圈外男友陳冠宇結婚的訊息](../Page/臉書.md "wikilink")。2014年5月2日誕下長女。

## 作品

### 電影

  - 2007年：《[不能說的秘密](../Page/不能說的秘密.md "wikilink")》 飾演 晴依
  - 2007年：《[公主在台北徹夜未眠](../Page/公主在台北徹夜未眠.md "wikilink")》
    [Motorola](../Page/Motorola.md "wikilink") Moto Z8 手機電影，飾演 女主角
  - 2008年：《[我的最愛](../Page/我的最愛_\(電影\).md "wikilink")》 飾演 阿琪
  - 2008年：《[親愛的](../Page/親愛的_\(2008年電影\).md "wikilink")》 飾演 韋晴
  - 2008年：《[彈道](../Page/彈道.md "wikilink")》 飾演 陳青
  - 2009年：《[愛到底](../Page/愛到底.md "wikilink")》 飾演 愷愷
  - 2009年：《[雨過天晴](../Page/雨過天晴_\(電影\).md "wikilink")》
  - 2011年：《[第三個願望](../Page/第三個願望.md "wikilink")》飾演 宜瑩
  - 2012年：《[刷新3+7之騙不了](../Page/刷新3+7.md "wikilink")》飾-王璐
  - 2017年：《[吃吃的愛](../Page/吃吃的愛.md "wikilink")》客串-偶像劇女主角

### 電視劇

  - 2008年1月13日／19日：[民視](../Page/民視.md "wikilink")／[衛視中文台](../Page/衛視中文台.md "wikilink")《[原來我不帥](../Page/原來我不帥.md "wikilink")》
    飾演 徐心婷
  - 2008年：《[愛盛開](../Page/愛盛開.md "wikilink")》 飾演 谷幽蘭
  - 2009年1月24日：[公視](../Page/公視.md "wikilink")《[心星的淚光](../Page/心星的淚光.md "wikilink")》
    飾演 顏睿珊
  - 2009年12月7日：[央視一套](../Page/央視一套.md "wikilink")《[蓮花雨](../Page/蓮花雨.md "wikilink")》
    飾演 童真
  - 2010年1月31日：[華視](../Page/華視.md "wikilink")《[星光下的童話](../Page/星光下的童話.md "wikilink")》
    飾演 曾愷玹
  - 2010年5月30日：[中視](../Page/中視.md "wikilink")、[TVBS](../Page/TVBS歡樂台.md "wikilink")《[就是要香戀](../Page/就是要香戀.md "wikilink")》
    飾演 明天晴
  - 2011年：[八大綜合台](../Page/八大綜合台.md "wikilink")《[單數絕配](../Page/單數絕配.md "wikilink")》
    飾演 怀真
  - 2011年4月27日:
    [中天娛樂台](../Page/中天娛樂台.md "wikilink")《[戀戀阿里山](../Page/戀戀阿里山.md "wikilink")》
    飾演 曾家雲
  - 2012年：[公視](../Page/公視.md "wikilink")《[俗辣英雄](../Page/俗辣英雄.md "wikilink")》
    飾演 吳小莉
  - 2012年：[壹電視綜合台](../Page/壹電視綜合台.md "wikilink")《[小站](../Page/小站_\(電視劇\).md "wikilink")》
    飾演 王欣怡 心理醫師
  - 2013年：[壹電視](../Page/壹電視.md "wikilink")《[幸福蒲公英](../Page/幸福蒲公英.md "wikilink")》
    飾演 林佳冬/高旺家
  - 2016年：《[因為。愛](../Page/因為。愛.md "wikilink")》 飾演 宋蝶兒
  - 待播：《[水男孩](../Page/水男孩_\(電視劇\).md "wikilink")》 飾演 宮直美

### 音樂作品

  - 2008年：《撒嬌》（電影《親愛的》插曲／單曲）發行：[金牌大風](../Page/金牌大風.md "wikilink")、[擎天娛樂](../Page/擎天娛樂.md "wikilink")
  - 2008年：《我不在乎》（電影《親愛的》主題曲／單曲）與[安志傑合唱](../Page/安志傑.md "wikilink")
    發行：[金牌大風](../Page/金牌大風.md "wikilink")、[擎天娛樂](../Page/擎天娛樂.md "wikilink")
  - 2009年：《無聲的旋律》（偶像劇《心星的淚光》插曲／單曲）發行：[擎天娛樂](../Page/擎天娛樂.md "wikilink")

### 廣告

  - [P\&G](../Page/P&G.md "wikilink")
    飛柔創新洗潤系列（與[羅志祥合演](../Page/羅志祥.md "wikilink")）
  - OLAY 多元修護晚霜
  - [統一](../Page/統一企業.md "wikilink")《來一客杯麵》
  - [光泉](../Page/光泉.md "wikilink")「乳香世家」
  - [SYM](../Page/SYM.md "wikilink") 三陽機車（心情嫁掉了篇）
  - 香港煙頓廣告
  - Ebay廣告聯邦銀行分期0利卡（懷錶篇）
  - 無線收費台廣告
  - [美特斯·邦威](../Page/美特斯·邦威.md "wikilink")07年秋冬廣告（周杰倫）
  - 美白牙齒貼片 \[3\]
  - MC Cafe
  - 線上遊戲《天龍八部》代言人
  - 香港道地百果園 - 藍莓檸檬

### 音樂錄影帶

  - 2005年：[五月天](../Page/五月天.md "wikilink")《知足》
  - 2005年：[李翊君](../Page/李翊君.md "wikilink")《勇敢的愛》
  - 2007年：[吳建豪](../Page/吳建豪.md "wikilink")《放手》
  - 2007年：[柯有綸](../Page/柯有綸.md "wikilink")《Crazy》
  - 2007年：[陳柏宇](../Page/陳柏宇.md "wikilink")《永久保存》
  - 2007年：[周杰倫](../Page/周杰倫.md "wikilink")《我不配》
  - 2008年：[古巨基](../Page/古巨基.md "wikilink")《勁歌金曲2--情歌王》
  - 2008年：[羅志祥](../Page/羅志祥.md "wikilink")《搞笑》
  - 2008年：[吳克群](../Page/吳克群.md "wikilink")《牽牽牽手》
  - 2008年：[胡清藍](../Page/胡清藍.md "wikilink")《候補情人》
  - 2008年 : [林俊傑](../Page/林俊傑.md "wikilink")《期待你的愛》
  - 2008年 : [張信哲](../Page/張信哲.md "wikilink")《殘念》
  - 2008年 : [張信哲](../Page/張信哲.md "wikilink")《逃生》客串
  - 2013年：[林俊傑](../Page/林俊傑.md "wikilink")《十秒的衝動》

## 參與節目

  - 2009年8月13日《[康熙來了](../Page/康熙來了.md "wikilink")》 新生代美女大闖康熙
  - 2011年5月6日《[康熙來了](../Page/康熙來了.md "wikilink")》 演員螢幕一分鐘 驚險十分鐘！
  - 2015年1月27日《[康熙來了](../Page/康熙來了.md "wikilink")》 咖啡廳美食推薦

## 榮譽

  - 2007年：入圍第四十四屆[金馬獎](../Page/金馬獎.md "wikilink") 最佳女配角（《不能說的秘密》）

## 資料來源

## 外部連結

  -
  -
  -
  - [曾愷玹個人部落格](http://rui01.pixnet.net/blog)

[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Category:台灣電影女演員](../Category/台灣電影女演員.md "wikilink")
[Category:台灣女性模特兒](../Category/台灣女性模特兒.md "wikilink")
[Category:世新大學校友](../Category/世新大學校友.md "wikilink")
[Category:臺北市立華江高級中學校友](../Category/臺北市立華江高級中學校友.md "wikilink")
[K](../Category/曾姓.md "wikilink")

1.
2.

3.