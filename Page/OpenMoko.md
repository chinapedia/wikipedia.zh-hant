**OpenMoko**是一个遵从[FLOSS哲学](../Page/FLOSS.md "wikilink")、旨在建立一个开放的[GSM制式](../Page/GSM制式.md "wikilink")[智能手机平台的项目](../Page/智能手机.md "wikilink")。它运行于[Linux平台](../Page/Linux.md "wikilink")，使用[ipkg包管理器](../Page/ipkg.md "wikilink")。

## 软件

### 内核

[Linux](../Page/Linux.md "wikilink") 2.6.17.14

### Userland

  - [X.Org Server](../Page/X.Org_Server.md "wikilink") 7.1
  - [Matchbox窗口管理器](../Page/Matchbox_\(window_manager\).md "wikilink")
  - [GTK+](../Page/GTK+.md "wikilink") 2.6.10
  - [Evolution数据服务](../Page/Novell_Evolution.md "wikilink")

## 硬件

### Neo1973

第一款OpenMoko硬件将是由[大眾電腦出品的](../Page/大眾電腦.md "wikilink")[Neo1973](../Page/Neo1973.md "wikilink")，提供给开发者的版本将于2007年3月11日发布，2007年9月11日将向公众正式发布，预定售价350美元。\[1\]

Neo的名字来源于移动电话通讯的第一个年头。

### Neo FreeRunner

第二款OpenMoko硬體是[Neo
FreeRunner](../Page/Neo_FreeRunner.md "wikilink")，在2008
CES發佈，並於2008年7月4日量產上市。

## 参见

<references />

## 外部連結

  - [OpenMoko](http://www.openmoko.com/)
  - [OpenMoko訊息發佈](https://web.archive.org/web/20061205113324/http://www.openmoko.com/press/index.html)
  - [包含詳細硬體描述的WiKi頁面](https://web.archive.org/web/20070928131443/http://www.linuxtogo.org/gowiki/OpenMoko)
  - [LinuxDevices.com網站上的評論](https://archive.is/20120629120640/www.linuxdevices.com/news/NS2986976174.html)
  - [OpenMoko聲明](https://web.archive.org/web/20090319051223/http://lists.openmoko.org/pipermail/announce/2007-January/000000.html)
  - [詞解/OpenMoko名詞解釋：OpenMoko](https://web.archive.org/web/20071030061205/http://www.emb-kb.com/doku.php)
  - [詞解/TuxPhone
    TuxPhone，與OpenMoko意味相近的開放原碼專案](https://web.archive.org/web/20071030061205/http://www.emb-kb.com/doku.php)

[Category:智能手機](../Category/智能手機.md "wikilink")
[Category:Linux設備](../Category/Linux設備.md "wikilink")
[Category:Openmoko](../Category/Openmoko.md "wikilink")

1.  OpenMoko团队，2007年1月20日，[lists.openmoko.org](http://lists.openmoko.org/pipermail/community/2007-January/001586.html)