\-{樑}-姓}}

**梁姓**是一個[中文姓氏之一](../Page/中文姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》中排名第128位，在[中國大陸人口排名第](../Page/中國大陸.md "wikilink")22位，在[台灣排名第](../Page/台灣.md "wikilink")43位。

## 起源

出自[嬴姓](../Page/嬴姓.md "wikilink")，以國為氏。
據《[通誌](../Page/通誌.md "wikilink")·氏族略·以國為氏》所載，[周平王時](../Page/周平王.md "wikilink")。[秦仲討伐](../Page/秦仲.md "wikilink")[西戎有功](../Page/西戎.md "wikilink")，其少子[梁伯康](../Page/梁伯康.md "wikilink")，受封於夏陽[梁山](../Page/梁山.md "wikilink")（在今[陝西省](../Page/陝西省.md "wikilink")[韓城縣南](../Page/韓城縣.md "wikilink")）。[春秋時](../Page/春秋.md "wikilink")，
梁國亡於[秦國](../Page/秦國.md "wikilink")，其後有梁氏。

## 分佈

梁姓在中國沿海省份、[港澳地區是大姓](../Page/港澳地區.md "wikilink")，[四川](../Page/四川.md "wikilink")、[貴州](../Page/貴州.md "wikilink")、[廣西和](../Page/廣西.md "wikilink")[台灣近代移民也有分佈](../Page/台灣.md "wikilink")。[韓國的](../Page/韓國.md "wikilink")[濟州島及](../Page/濟州島.md "wikilink")[马来西亚亦有梁姓](../Page/马来西亚.md "wikilink")。

## 名人

### 歷史名人

  - [梁鸿](../Page/梁鸿.md "wikilink")：东汉诗人，成语举案齐眉故事原型
  - [梁鹄](../Page/梁鹄.md "wikilink")：东汉书法家
  - [梁冀](../Page/梁冀.md "wikilink")：东汉大将军，外戚
  - [梁習](../Page/梁習.md "wikilink")：曹魏大司農
  - [梁令瓒](../Page/梁令瓒.md "wikilink")：唐朝画家，天文仪器制造家。与一行共同设计制造了黄道游仪、浑仪、复矩等天文测量仪器
  - [梁红玉](../Page/梁红玉.md "wikilink")：南宋女將，[韓世忠之妻](../Page/韓世忠.md "wikilink")
  - [梁灏](../Page/梁灏.md "wikilink")：北宋状元。子梁固亦为状元。史称宋朝“父子状元”
  - [梁固](../Page/梁固.md "wikilink")：北宋状元
  - [梁楷](../Page/梁楷.md "wikilink")：南宋畫家，以減筆人物著稱
  - [梁储](../Page/梁储.md "wikilink")：明朝，礼部尚书，华盖殿大学士
  - [梁贊](../Page/梁赞_\(武术家\).md "wikilink")：晚清著名武術家，人稱詠春拳王
  - [梁啟超](../Page/梁啟超.md "wikilink")：清末戊戌變法領袖，资产阶级改良主义者
  - [梁漱溟](../Page/梁漱溟.md "wikilink")：現代著名思想家，哲學家，教育家
  - [梁起鐸](../Page/梁起鐸.md "wikilink")：韓國獨立運動家

### 現代名人

學術界：

  - 梁方仲： 經濟學家，祖籍廣州海珠區黃埔村，學術著作有《 明代糧長制度》， 《 中國歷代土地，田賦，戶口統計》等等
  - 梁承鄴，著名學者，梁方仲之子；主要著作有《無悔是書生》
  - 梁炳華，歷史學家，香港人，對香港歷史硏究頗有貢獻. 著作有《北區風物誌》 （ 一九九四年出版）《南區風物誌》（
    一九九六年出版），\<中西區風物誌》（ 一九九九年出版），

\< 觀塘區風物誌》（ 二零零一年出版）

〈 離島區風物誌〉（ 二零零二年出版）

` 《 深水埗區風物誌》（ 二零一零年出版）`

#### 政界

  - [梁光烈](../Page/梁光烈.md "wikilink")：[中華人民共和國國防部部長](../Page/中華人民共和國國防部部長.md "wikilink")、兼[國務委員](../Page/國務委員.md "wikilink")
  - [梁振英](../Page/梁振英.md "wikilink")：[香港特別行政區第四任](../Page/香港特別行政區.md "wikilink")[行政長官](../Page/行政長官.md "wikilink")、兼[國家領導人](../Page/黨和國家領導人.md "wikilink")
  - [周梁淑怡](../Page/周梁淑怡.md "wikilink")：資深傳媒人，曾任行政會議成員、立法會議員
  - [梁定邦
    (醫生)](../Page/梁定邦_\(醫生\).md "wikilink")：前[香港市政局主席](../Page/香港市政局.md "wikilink")
  - [梁定邦
    (大律師)](../Page/梁定邦_\(大律師\).md "wikilink")：香港執業資深大律師，現任中國證券監督管理委員會首席顧問
  - [梁國雄](../Page/梁國雄.md "wikilink")：香港立法會議員、[四五行動](../Page/四五行動.md "wikilink")/[社會民主連線成員](../Page/社會民主連線.md "wikilink")，綽號「長毛」
  - [梁耀忠](../Page/梁耀忠.md "wikilink")：香港立法會、區議會議員、[街坊工友服務處成員](../Page/街坊工友服務處.md "wikilink")
  - [梁愛詩](../Page/梁愛詩.md "wikilink")：香港特區政府前律政司司長、基本法委員會副主任
  - [梁錦松](../Page/梁錦松.md "wikilink")：香港特區政府前財政司司長
  - [梁家傑](../Page/梁家傑.md "wikilink")：香港資深大律師，[公民黨成員](../Page/公民黨.md "wikilink")，曾任公民黨黨魁及副主席（內務）、立法會議員、泛民會議及香港大律師公會主席。
  - [梁君彥](../Page/梁君彥.md "wikilink")：[香港立法會主席](../Page/香港立法會主席.md "wikilink")、中華人民共和國[全國政協常委](../Page/全國政協常委.md "wikilink")，[經民聯成員](../Page/經民聯.md "wikilink")
  - [梁美芬](../Page/梁美芬.md "wikilink")：香港立法會議員，九龍城區議員，[西九新動力召集人](../Page/西九新動力.md "wikilink")，[經民聯成員](../Page/經民聯.md "wikilink")
  - [梁繼昌](../Page/梁繼昌.md "wikilink")：[公共專業聯盟副主席](../Page/公共專業聯盟.md "wikilink")，香港立法會議員（會計界功能界別）
  - [梁志祥](../Page/梁志祥.md "wikilink")：香港立法會議員、新界社團聯會會長及民建聯元朗支部主席、元朗區議會議員
  - [梁福元](../Page/梁福元.md "wikilink")：香港新界元朗十八鄉鄉紳，新界西北區政治領袖，十八鄉鄉事委員會主席、元朗區議會議員
  - [梁天琦](../Page/梁天琦.md "wikilink")：香港本土派政治人物，本土民主前線發言人
  - [梁頌恆](../Page/梁頌恆.md "wikilink")：香港立法會議員，[青年新政成員](../Page/青年新政.md "wikilink")

#### 思想、文学界

  - [梁漱溟](../Page/梁漱溟.md "wikilink")：現代著名思想家，哲學家，教育家，现代新儒学的早期代表人物之一，社会活动家，爱国民主人士
  - [梁實秋](../Page/梁實秋.md "wikilink")：中国著名的散文家、学者、文学批评家、翻譯家
  - [梁文道](../Page/梁文道.md "wikilink")：香港作家
  - [梁芷珊](../Page/梁芷珊.md "wikilink")：香港作家
  - [梁小斌](../Page/梁小斌.md "wikilink")：现代诗人
  - [梁望峰](../Page/梁望峰.md "wikilink")：香港作家
  - [梁曉文](../Page/梁曉文.md "wikilink")：筆名李香蘭，香港漫畫家

#### 宗教界

  - [梁發](../Page/梁發.md "wikilink")：中國基督教歷史人物，首位華人牧師
  - [梁家麟](../Page/梁家麟.md "wikilink")：香港基督教宣道會牧師
  - [梁燕城](../Page/梁燕城.md "wikilink")：中國及香港基督教學者、多所大學的客座教授
  - [梁文道](../Page/梁文道.md "wikilink")：香港前天主教徒，2008年皈依[南傳佛教](../Page/南傳佛教.md "wikilink")
  - [梁元生](../Page/梁元生.md "wikilink")：香港中文大學崇基學院前院長，基督教徒

#### 科学技术界

  - [梁思成](../Page/梁思成.md "wikilink")：中国著名的建筑家
  - [梁思永](../Page/梁思永.md "wikilink")：中国著名的考古学家
  - [梁恩佐](../Page/梁恩佐.md "wikilink")：美国华人物理学家
  - [梁小民](../Page/梁小民.md "wikilink")：经济学家
  - [梁啟智](../Page/梁啟智.md "wikilink")：香港大學地理學系及嶺南大學文化研究學系兼任講師
  - [梁子超](../Page/梁子超.md "wikilink")：華南農業大學林學院教授，曾任華南農業大學林學系副主任

#### 商界

  - [梁幗馨](../Page/梁幗馨.md "wikilink")（狄娜）：中國企業家
  - [梁志堅](../Page/梁志堅_\(商人\).md "wikilink")：前任新世界發展及其集團成員董事，現任會德豐副主席
  - [梁文韜](../Page/梁文韜.md "wikilink")：香港食家、元朗大榮華酒樓董事總經理
  - [梁乃鵬](../Page/梁乃鵬.md "wikilink")：香港中文大學校董會主席，電視廣播有限公司前行政主席

#### 音乐及演艺界 、監製

  - [梁文音](../Page/梁文音.md "wikilink")：[臺灣著名華語流行音樂女歌手](../Page/臺灣.md "wikilink")
  - [梁一貞](../Page/梁一貞.md "wikilink")：[臺灣著名華語流行音樂女歌手](../Page/臺灣.md "wikilink")
  - [梁靖琪](../Page/梁靖琪.md "wikilink")：[香港女演員](../Page/香港.md "wikilink")、前香港組合女生宿舍成員之一
  - [梁弘志](../Page/梁弘志.md "wikilink")：已故[臺灣著名音樂人](../Page/臺灣.md "wikilink")
  - [梁偉文](../Page/梁偉文.md "wikilink")：筆名林夕，[香港首席填詞人](../Page/香港.md "wikilink")
  - [梁家輝](../Page/梁家輝.md "wikilink")：[香港電影演員](../Page/香港.md "wikilink")
  - [梁曉珺](../Page/梁曉珺.md "wikilink")：第五屆《超級星光大道》季軍
  - [梁朝偉](../Page/梁朝偉.md "wikilink")：[香港資深電影演員](../Page/香港.md "wikilink")
  - [梁醒波](../Page/梁醒波.md "wikilink")：已故粵劇演員
  - [梁舜燕](../Page/梁舜燕.md "wikilink")：[香港電視史上首位女演員](../Page/香港.md "wikilink")
  - [梁琤](../Page/梁琤.md "wikilink")：[香港女演員](../Page/香港.md "wikilink")
  - [梁志達](../Page/梁志達.md "wikilink")：[香港配音員](../Page/香港.md "wikilink")
  - [梁偉德](../Page/梁偉德.md "wikilink")：[香港配音員](../Page/香港.md "wikilink")
  - [梁少霞](../Page/梁少霞.md "wikilink")：[香港配音員](../Page/香港.md "wikilink")、天氣報告員
  - [梁樂華](../Page/梁樂華.md "wikilink")：藝名岳華，[香港演員](../Page/香港.md "wikilink")
  - [梁嘉琪](../Page/梁嘉琪.md "wikilink")：[香港無綫電視女藝員](../Page/香港.md "wikilink")
  - [梁家仁](../Page/梁家仁.md "wikilink")：無線資深演員
  - [梁政珏](../Page/梁政珏.md "wikilink")：[香港無綫電視女藝員](../Page/香港.md "wikilink")
  - [梁詠琪](../Page/梁詠琪.md "wikilink")：[香港歌手](../Page/香港.md "wikilink")
  - [梁榮忠](../Page/梁榮忠.md "wikilink")：[香港男藝員](../Page/香港.md "wikilink")
  - [梁靜茹](../Page/梁靜茹.md "wikilink")：[馬來西亞華僑](../Page/馬來西亞.md "wikilink")，台灣女歌手
  - [梁烈唯](../Page/梁烈唯.md "wikilink")：[香港](../Page/香港.md "wikilink")[TVB男藝員](../Page/TVB.md "wikilink")
  - [梁俊一](../Page/梁俊一.md "wikilink")：[香港演員](../Page/香港.md "wikilink")
  - [梁雁翎](../Page/梁雁翎.md "wikilink")：[中國歌手](../Page/中國.md "wikilink")
  - [梁漢文](../Page/梁漢文.md "wikilink")：[香港歌手](../Page/香港.md "wikilink")、藝人
  - 梁蘭思：香港資深演員，拍過電影「 老夫子奇趣錄」，在香港亞洲電視「金鳳凰」擔任二缐角色古橫波.

（
視頻資料：https://www.youtube.com/watch?v=cdu_P_M4wFo、https://www.youtube.com/watch?v=_BTeKdoSOF4)

  - [梁佩詩](../Page/梁佩詩.md "wikilink")：華裔[英國演員](../Page/英國.md "wikilink")
  - [梁洛施](../Page/梁洛施.md "wikilink")：[香港歌手](../Page/香港.md "wikilink")、電影《伊莎貝拉》女主角
  - [梁修身](../Page/梁修身.md "wikilink")：[臺灣電視劇演員](../Page/臺灣.md "wikilink")
  - [梁赫群](../Page/梁赫群.md "wikilink")：[臺灣綜藝節目製作人與主持人梁修身之子](../Page/臺灣.md "wikilink")
  - [梁小冰](../Page/梁小冰.md "wikilink")：[香港小姐季軍](../Page/香港.md "wikilink")
  - [梁思浩](../Page/梁思浩.md "wikilink")：[香港藝人](../Page/香港.md "wikilink")
  - [梁繼璋](../Page/梁繼璋.md "wikilink")：前香港電台第二台節目主持人
  - [梁志健](../Page/梁志健.md "wikilink")：藝名「森美」，香港藝人、電台唱片騎師
  - [梁鉉錫](../Page/梁鉉錫.md "wikilink")：韓國大型經紀公司YG
    Entertainment創辦人及現任社長，旗下藝人包含BIGBANG、2NE1、WINNER、樂童音樂家等等
  - [梁美京](../Page/梁美京.md "wikilink")：[韓國女演員](../Page/韓國.md "wikilink")、電視劇《大長今》中飾演韓白英韓尚宮一角

#### 體育界

  - [梁文博](../Page/梁文博.md "wikilink")：现役职业斯诺克台球选手
  - [梁能仁](../Page/梁能仁.md "wikilink")：香港80年代著名足球員
  - [梁孔德](../Page/梁孔德.md "wikilink")：香港足球總會主席

#### 醫學界

  - [梁永雄](../Page/梁永雄.md "wikilink")：香港著名心臟科醫生
  - [梁智仁](../Page/梁智仁.md "wikilink")：香港著名骨科醫生，現任香港公開大學校長
  - [梁卓偉](../Page/梁卓偉.md "wikilink")：香港大學李嘉誠醫學院院長，曾任行政長官辦公室主任及食物及衞生局副局長
  - [梁智鴻](../Page/梁智鴻.md "wikilink")：香港名醫，有「金刀梁」之稱
  - [梁家騮](../Page/梁家騮.md "wikilink")：前香港立法會議員（醫學界功能界別）

#### 虛構人物

  - [梁山伯](../Page/梁山伯.md "wikilink")：中國四大民間傳說梁山伯與祝英台中的主角人物

#### 其他

  - [梁挺](../Page/梁挺.md "wikilink")：推廣詠春拳著名人士
  - [梁寬](../Page/梁寬.md "wikilink")：著名武術家[黃飛鴻弟子](../Page/黃飛鴻.md "wikilink")
  - [梁淦堂](../Page/梁淦堂.md "wikilink")：即“黄胄”本姓梁，近代著名国画大师

## 相关条目

  - [梁书](../Page/梁书.md "wikilink")
      - [丹丹国](../Page/丹丹国.md "wikilink")

## 参考资料

  -
[梁姓](../Category/梁姓.md "wikilink")
[Category:漢字姓氏](../Category/漢字姓氏.md "wikilink")
[Category:國名姓氏](../Category/國名姓氏.md "wikilink")