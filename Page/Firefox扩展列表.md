**Firefox扩展**是为基于[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")[网页浏览器设计的附加组件](../Page/网页浏览器.md "wikilink")。一些Firefox扩展也可以在[SeaMonkey网页浏览器使用](../Page/SeaMonkey.md "wikilink")。

在Mozilla的附加组件官方網站可以找到完整的扩展列表\[1\]。截止2010年5月16日，这个网站共有17936个Firefox扩展。

## 广告屏蔽

  - [uBlock
    Origin](https://addons.mozilla.org/zh-CN/firefox/addon/ublock-origin/)
  - [Adblock
    Plus](https://addons.mozilla.org/zh-CN/firefox/addon/adblock-plus/)
  - [AdBlock](https://addons.mozilla.org/zh-CN/firefox/addon/adblock-for-firefox/)
  - [Poper
    Blocker](https://addons.mozilla.org/zh-TW/firefox/addon/poper-blocker-pop-up-blocker/)

## 隐私保护

  - [Ghostery](https://addons.mozilla.org/zh-CN/firefox/addon/ghostery/)
  - [Cookie
    AutoDelete](https://addons.mozilla.org/zh-CN/firefox/addon/cookie-autodelete/)
  - [Privacy
    Badger](https://addons.mozilla.org/zh-CN/firefox/addon/privacy-badger17/)
  - [NoScript](https://addons.mozilla.org/zh-CN/firefox/addon/noscript/)

## 代理服务器管理

  - [Proxy
    SwitchyOmega](https://addons.mozilla.org/zh-CN/firefox/addon/switchyomega/)
  - [FoxyProxy
    Standard](https://addons.mozilla.org/zh-CN/firefox/addon/foxyproxy-standard/)
  - [Proxy
    SmartProxy](https://addons.mozilla.org/zh-CN/firefox/addon/smartproxy/)

## 其他工具

  - [Gesturefy](https://addons.mozilla.org/zh-CN/firefox/addon/gesturefy/)：鼠标手势，摇摆手势，滚轮手势。
  - [smartUp
    手势](https://addons.mozilla.org/zh-CN/firefox/addon/smartup/)：鼠标手势，简易拖曳，超级拖曳，摇杆手势和滚轮手势。
  - [闪耀拖拽](https://addons.mozilla.org/zh-CN/firefox/addon/glitterdrag/)：通过拖拽文字、链接和图像执行相对应操作。
  - [Stylus](https://addons.mozilla.org/zh-CN/firefox/addon/styl-us/)：通过安装各类
    [CSS](../Page/层叠样式表.md "wikilink") 模板修改网页界面的用户样式管理器。
  - [Tampermonkey](https://addons.mozilla.org/zh-CN/firefox/addon/tampermonkey/)：[脚本语言管理器](../Page/脚本语言.md "wikilink")，用于改变网页界面。[Tampermonkey
    官网](https://tampermonkey.net/)
  - [Greasemonkey](https://addons.mozilla.org/zh-CN/firefox/addon/greasemonkey/)：用
    JavaScript 脚本自定义网页显示。
  - [Tabliss](https://addons.mozilla.org/zh-CN/firefox/addon/tabliss/)：在新标签页显示背景图片。
  - [Firefox Multi-Account
    Containers](https://addons.mozilla.org/zh-CN/firefox/addon/multi-account-containers/)：Mozilla
    官方开发的一个多账号的登录和切换扩展。
  - [Facebook
    Container](https://addons.mozilla.org/zh-CN/firefox/addon/facebook-container/)：限制
    [Facebook](../Page/Facebook.md "wikilink") 网站追踪个人信息。

## 参考文献

## 外部链接

  - Mozilla官方授权的扩展和插件下载站：
      - [Firefox 附加组件](https://addons.mozilla.org/)
      - [Mozilla Taiwan](http://www.moztw.org/)

## 相關條目

  - [Mozilla Add-ons](../Page/Mozilla_Add-ons.md "wikilink")

[\*](../Category/Firefox_附加组件.md "wikilink")
[Category:软件列表](../Category/软件列表.md "wikilink")

1.  [Firefox Add-ons Repository](https://addons.mozilla.org)
    *Mozilla.org*, Accessed 2月11日, 2008年