**建武**（386年十月－394年八月）是[西燕君主](../Page/西燕.md "wikilink")[慕容永的](../Page/慕容永.md "wikilink")[年号](../Page/年号.md "wikilink")，共计9年。

## 纪年

| 中興                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 386年                           | 387年                           | 388年                           | 389年                           | 390年                           | 391年                           | 392年                           | 393年                           | 394年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙戌](../Page/丙戌.md "wikilink") | [丁亥](../Page/丁亥.md "wikilink") | [戊子](../Page/戊子.md "wikilink") | [己丑](../Page/己丑.md "wikilink") | [庚寅](../Page/庚寅.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") | [甲午](../Page/甲午.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[中興年号](../Page/中兴_\(年号\).md "wikilink")
  - 同期存在的其他政权年号
      - [太元](../Page/太元_\(晋孝武帝\).md "wikilink")（376年正月－396年十二月）：東晉皇帝[晋孝武帝司马曜的年号](../Page/晋孝武帝.md "wikilink")
      - [太安](../Page/太安_\(苻丕\).md "wikilink")（385年八月－386年十月）：[前秦政权](../Page/前秦.md "wikilink")[苻丕年号](../Page/苻丕.md "wikilink")
      - [太初](../Page/太初_\(苻登\).md "wikilink")（386年十一月－394年六月）：[前秦政权](../Page/前秦.md "wikilink")[苻登年号](../Page/苻登.md "wikilink")
      - [延初](../Page/延初.md "wikilink")（394年七月－394年十月）：[前秦政权](../Page/前秦.md "wikilink")[苻崇年号](../Page/苻崇.md "wikilink")
      - [元光](../Page/元光_\(窦冲\).md "wikilink")（393年六月－394年七月）：[窦冲自立年号](../Page/窦冲.md "wikilink")
      - [建初](../Page/建初_\(姚萇\).md "wikilink")（386年四月－394年四月）：[后秦政权](../Page/后秦.md "wikilink")[姚苌年号](../Page/姚苌.md "wikilink")
      - [皇初](../Page/皇初.md "wikilink")（394年五月－399年九月）：[后秦政权](../Page/后秦.md "wikilink")[姚兴年号](../Page/姚兴.md "wikilink")
      - [建义](../Page/建义_\(西秦\).md "wikilink")（385年九月－388年六月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏国仁年号](../Page/乞伏国仁.md "wikilink")
      - [太初](../Page/太初_\(西秦\).md "wikilink")（388年六月－400年七月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏乾归年号](../Page/乞伏乾归.md "wikilink")
      - [建兴](../Page/建兴_\(慕容垂\).md "wikilink")（386年二月－396年四月）：[后燕政权](../Page/后燕.md "wikilink")[慕容垂年号](../Page/慕容垂.md "wikilink")
      - [凤凰](../Page/凤凰_\(张大豫\).md "wikilink")（386年二月-十一月）：[张大豫自立年号](../Page/张大豫.md "wikilink")
      - [太安](../Page/太安_\(呂光\).md "wikilink")（386年十月－389年正月）：[后凉政权](../Page/后凉.md "wikilink")[吕光年号](../Page/吕光.md "wikilink")
      - [麟嘉](../Page/麟嘉_\(后凉\).md "wikilink")（389年二月－396年六月）：[后凉政权](../Page/后凉.md "wikilink")[吕光年号](../Page/吕光.md "wikilink")
      - [登国](../Page/登国.md "wikilink")（386年－396年六月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋珪年号](../Page/拓跋珪.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:西燕年号](../Category/西燕年号.md "wikilink")
[Category:4世纪年号](../Category/4世纪年号.md "wikilink")
[Category:380年代中国政治](../Category/380年代中国政治.md "wikilink")
[Category:390年代中国政治](../Category/390年代中国政治.md "wikilink")