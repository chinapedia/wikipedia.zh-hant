[Central_nervous_system.svg](https://zh.wikipedia.org/wiki/File:Central_nervous_system.svg "fig:Central_nervous_system.svg")

**中枢神经系统**（英文：**central nervous
system**，縮寫：**CNS**）是[神经系统中](../Page/神经系统.md "wikilink")[神经细胞集中的结构](../Page/神经细胞.md "wikilink")，在[脊椎动物包括](../Page/脊椎动物.md "wikilink")[腦和](../Page/腦.md "wikilink")[脊髓](../Page/脊髓.md "wikilink")；在高等[无脊椎动物如](../Page/无脊椎动物.md "wikilink")[环节动物和](../Page/环节动物.md "wikilink")[昆虫等](../Page/昆虫.md "wikilink")，则主要包括腹神经索和一系列的神经节。

人的中枢神经系统构造最复杂而完整，特别是大脑半球的皮层获得高度的发展，成为神经系统最重要和高级的部分，保证了机体各器官的协调活动，以及机体与外界环境间的统一和协调。

中樞神經系統與[周围神经系统組成了](../Page/周围神经系统.md "wikilink")[神經系統](../Page/神經系統.md "wikilink")，控制了生物的行為。

整個中樞神經系統位於[背腔](../Page/背腔.md "wikilink")，腦在[顱腔](../Page/顱腔.md "wikilink")，脊髓在[脊椎管](../Page/脊椎管.md "wikilink")；[顱骨保護腦](../Page/顱骨.md "wikilink")，[脊椎保護脊髓](../Page/脊椎.md "wikilink")。

## 参考文献

## 參見

  - [腦脊液](../Page/腦脊液.md "wikilink")
  - [灰白質](../Page/灰白質.md "wikilink")

## 外部連結

  - [Overview of the Central Nervous
    System](http://nba.uth.tmc.edu/neuroscience/s2/chapter01.html),
    *Neuroscience Online* (electronic neuroscience textbook)
  - [High-Resolution Cytoarchitectural Primate Brain
    Atlases](http://primate-brain.org)
  - [Explaining the human nervous
    system](http://www.humannervoussystem.info).
  - The [Department of
    Neuroscience](../Page/v:Topic:Neuroscience.md "wikilink") at
    [Wikiversity](../Page/v:.md "wikilink")
  - [Central nervous system
    histology](http://www.histology-world.com/photoalbum/thumbnails.php?album=16)

[中樞神經系统](../Category/中樞神經系统.md "wikilink")
[Category:神经系统](../Category/神经系统.md "wikilink")