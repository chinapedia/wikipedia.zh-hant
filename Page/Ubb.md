**Unbibium**（**Ubb**）是一種未被發現的[化學元素的](../Page/化學元素.md "wikilink")[臨時命名](../Page/IUPAC元素系統命名法.md "wikilink")，其原子序為122。2008年，[希伯来大学的Amnon](../Page/希伯来大学.md "wikilink")
Marinov曾被认为发现了第一个[超锕系元素Ubb](../Page/超锕系元素.md "wikilink")。\[1\]但之后的研究认为这一发现并不足信\[2\]\[3\]。

## 合成嘗試

<table>
<thead>
<tr class="header">
<th><p>目標</p></th>
<th><p>發射體</p></th>
<th><p>CN</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><sup>208</sup>Pb</p></td>
<td><p><sup>94</sup>Zr</p></td>
<td><p><sup>302</sup>Ubb</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>232</sup>Th</p></td>
<td><p><sup>74</sup>Ge</p></td>
<td><p><sup>306</sup>Ubb</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>238</sup>U</p></td>
<td><p><sup>70</sup>Zn</p></td>
<td><p><sup>308</sup>Ubb</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>238</sup>U</p></td>
<td><p><sup>66</sup>Zn</p></td>
<td><p><sup>304</sup>Ubb</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>244</sup>Pu</p></td>
<td><p><sup>64</sup>Ni</p></td>
<td><p><sup>308</sup>Ubb</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><sup>248</sup>Cm</p></td>
<td><p><sup>58</sup>Fe</p></td>
<td><p><sup>306</sup>Ubb</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><sup>249</sup>Cf</p></td>
<td><p><sup>54</sup>Cr</p></td>
<td><p><sup>303</sup>Ubb</p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

<div class="references-small">

<references />

</div>

[8D](../Category/第8周期元素.md "wikilink")
[8D](../Category/化学元素.md "wikilink")

1.  Amnon Marinov, I. Rodushkin, D. Kolb, A. Pape, Y. Kashiv, R. Brandt,
    R.V. Gentry, H.W. Miller, [*Evidence for a long-lived superheavy
    nucleus with atomic mass number A=292 and atomic number Z≈122 in
    natural Th*](http://arxiv.org/abs/0804.3869), e-Print:
    arXiv:0804.3869 \[nucl-ex\], Apr 2008, 14 pp.
2.
3.