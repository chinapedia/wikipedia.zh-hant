**莫斯科足球俱乐部**（俄语：），简称**FC莫斯科**，是一支已解散的[俄罗斯](../Page/俄罗斯.md "wikilink")[足球俱乐部](../Page/足球.md "wikilink")，主场位于[莫斯科](../Page/莫斯科.md "wikilink")，成立于1997年。它的最好战绩是2005年俄罗斯超级联赛中获得第
5 名。该队的射手 Dmitry Kirichenko 成为同年的联赛射手王。

## 阵容

2007年[俄超官方网站](https://web.archive.org/web/20071015191211/http://rfpl.org/clubs.shtml?team=14&act=squad&year=2007)

预备队：

## 外部链接

  - [官方主页](http://www.fcmoscow.ru)
  - [MoscowLads主页](http://www.moscowlads.com)
  - [球迷主页](http://www.fcmoscow.com)

[Category:俄罗斯足球俱乐部](../Category/俄罗斯足球俱乐部.md "wikilink")
[Category:莫斯科足球俱乐部](../Category/莫斯科足球俱乐部.md "wikilink")
[Category:2004年建立的足球俱樂部](../Category/2004年建立的足球俱樂部.md "wikilink")
[Category:2004年俄羅斯建立](../Category/2004年俄羅斯建立.md "wikilink")