**Calligra Suite**是一套開放原始碼的辦公室套裝軟體（office
suite），可用於[Linux](../Page/Linux.md "wikilink")、[Mac OS
X](../Page/Mac_OS_X.md "wikilink")\[1\]、[Windows](../Page/Windows.md "wikilink")\[2\]和[Haiku](../Page/Haiku.md "wikilink")\[3\]等作業系統。其中包含了多種軟體，包括：[文字處理器](../Page/文字處理器.md "wikilink")、[電子數據表](../Page/電子數據表.md "wikilink")、簡報軟體、資料庫管理，以及[向量圖形和數位繪圖軟體](../Page/向量圖形.md "wikilink")。

Calligra使用[開放文件格式](../Page/開放文件格式.md "wikilink")（[ODF](../Page/ODF.md "wikilink")，OpenDocument
Format）作為預設的檔案格式，並能匯入其他格式，如[Microsoft
Office的檔案](../Page/Microsoft_Office.md "wikilink")。Calligra依賴於[KDE技術](../Page/KDE.md "wikilink")，並常與[KDE
Software
Compilation相結合](../Page/KDE_Software_Compilation.md "wikilink")。

## 支援的系統

[KOffice_viewer_on_Maemo5.jpg](https://zh.wikipedia.org/wiki/File:KOffice_viewer_on_Maemo5.jpg "fig:KOffice_viewer_on_Maemo5.jpg")
\]\] Calligra
Suite很大程度上依赖于[Qt](../Page/Qt.md "wikilink")，因此能够相对轻松地被移植到绝大部分支持Qt的平台上。

### 桌上型電腦

### 智慧手機

### 平板電腦

## 歷史

Calligra
Suite的前身是KOffice，其開發開始於1997年\[4\]。在1998年加入[KPresenter和](../Page/KPresenter.md "wikilink")[KWord](../Page/KWord.md "wikilink")\[5\]。

1999年，在美國對微軟反壟斷審判中KOffice被舉出作為證詞，被當時的微軟總裁Paul
Maritz作為在作業系統和辦公套裝競爭的證據。\[6\]

KOffice套裝的第一次正式發布是在2000年，作為KDE 2.0的一部分。\[7\]

2010年12月6日，KDE社区发布声明，成立Calligra Suite项目，作为KOffice项目的延续，并表示即将发布的KOffice
2.3仍将受到支持，而Calligra Suite的版本号将从2.4开始。

2010年12月31日，KOffice
2.3照常發佈，其後修正錯誤的版本（2.3.1–2.3.3）。仍然是KOffice和Calligra開發團隊合作努力的成果。它的開發者表示，此版本對實際用途有足夠穩定度，而Karbon14、Krita和KSpread建議可用於生產工作。\[8\]

Calligra團隊2011年5月18日開始發佈每月快照。\[9\]

2011年6月16日，發佈第二個快照。\[10\]

2011年7月13日，發佈第三個快照。\[11\]

2011年9月2日，發佈第四個快照。\[12\]

### Calligra Suite 2.4

2011年9月14日，Calligra Suite發佈第一個Beta版。\[13\]

2011年10月6日，發佈第二個Beta版。\[14\]

2011年10月26日，發佈第三個Beta版。\[15\]

2011年11月23日，發佈第四個Beta版。\[16\]

2011年12月15日，發佈第五個Beta版。\[17\]

2012年1月11日，發佈第六個Beta版。\[18\]

2012年2月1日，發佈第七個Beta版。\[19\]

2012年3月21日，發佈發行候選版。\[20\]Calligra Tables在此版以後改名為Calligra Sheets。

2012年4月11日，Calligra Suite首個穩定版釋出，版號：2.4。\[21\]

2012年4月25日，2.4.1版釋出，修正了數個問題。\[22\]

2012年5月30日，2.4.2版釋出，修正了數個問題。\[23\]

2012年6月27日，2.4.3版釋出，修正了數個問題。\[24\]

2013年1月23日，2.4.4版釋出，修正了數個問題。\[25\]

### Calligra Suite 2.5

2012年5月30日，發佈Alpha版。\[26\]

2012年6月23日，發佈Beta版。\[27\]

2012年7月18日，發佈發行候選版。\[28\]

2012年8月13日，發佈2.5.X第一個穩定版，較2.4版增許多功能。\[29\]

2012年8月29日，2.5.1版釋出，修正了數個問題。\[30\]

2012年9月13日，2.5.2版釋出，修正了數個問題。\[31\]

2012年11月6日，2.5.3版釋出，修正了數個問題。\[32\]

2012年11月21日，2.5.4版釋出，修正了數個問題。\[33\]

2013年1月23日，2.5.5版釋出，修正了數個問題。\[34\]

### Calligra Suite 2.6

2012年10月12日，發佈Alpha版。\[35\]加入[Calligra
Author新軟體](../Page/Calligra_Author.md "wikilink")。

2012年10月31日，發佈Beta版。\[36\]

2012年12月5日，發佈第一個發行候選版。\[37\]

2013年1月18日，發佈第三個發行候選版。\[38\]

2013年2月5日，發佈2.6.X第一個穩定版，較2.5版新增許多功能。\[39\]

2013年2月20日，2.6.1版釋出，修正了數個問題。\[40\]

2013年3月13日，2.6.2版釋出，修正了數個問題。\[41\]

2013年5月15日，2.6.3版釋出，修正了數個問題。\[42\]

2013年6月4日，2.6.4版釋出，修正了數個問題，另外為[Calligra
Author更換新的程式圖示](../Page/Calligra_Author.md "wikilink")。\[43\]

### Calligra Suite 2.7

2013年5月8日，發佈第一個Beta版。\[44\]

2013年5月28日，發佈第二個Beta版。\[45\]

2013年6月12日，發佈第三個Beta版。\[46\]

2013年8月1日，發佈2.7.X第一個穩定版，較2.6版新增許多功能，另外因外開發團隊在測試2.7.0時，發現該版本有些嚴重的問題，故將已釋出給各發行版測試的2.7.0標示為不穩定，修正後釋出2.7.1版。\[47\]

2013年8月26日，2.7.2版釋出，修正了數個問題。\[48\]

2013年10月11日，2.7.3版釋出，修正了數個問題。\[49\]

2013年10月17日，2.7.4版釋出，修正了數個問題。\[50\]

2013年11月27日，2.7.5版釋出，修正了數個問題。\[51\]

### Calligra Suite 2.8

2013年12月8日，發佈第一個Beta版。\[52\]

2014年1月22日，發佈第二個Beta版。\[53\]

2014年2月18日，發佈第三個Beta版。\[54\]

2014年3月5日，發佈2.8.X第一個穩定版，較2.7版新增許多功能。\[55\]

2014年3月28日，2.8.1版釋出，修正了數個問題。\[56\]

2014年4月16日，2.8.2版釋出，修正了數個問題。\[57\]

2014年5月15日，2.8.3版釋出，修正了數個問題。\[58\]

2014年7月5日，2.8.5版釋出，跳過2.8.4版的原因是，在將要釋出時發現了數個嚴重的問題，修正後釋出2.8.5版。\[59\]

2014年9月24日，2.8.6版釋出，修正了數個問題。\[60\]

2014年12月3日，2.8.7版釋出，修正了數個問題。\[61\]

### Calligra Suite 2.9

2014年12月14日，發佈第一個Beta版。\[62\]

2015年1月15日，發佈第二個Beta版。\[63\]

2015年2月13日，發佈第三個Beta版。\[64\]

2015年2月26日，發佈2.9.X第一個穩定版，較2.8版新增許多功能。\[65\]

2015年3月16日，2.9.1版釋出，修正了數個問題。\[66\]

2015年4月2日，2.9.2版釋出，修正了數個問題。\[67\]

2015年5月6日，2.9.4版釋出，修正了數個問題，\[68\]，2.9.3因在即將釋出時發現有一些問題待解決，故並未有官方釋出版本\[69\]。

2015年6月9日，2.9.5版釋出，修正了數個問題。\[70\]

2015年7月10日，2.9.6版釋出，修正了數個問題。\[71\]

2015年9月3日，2.9.7版釋出，修正了數個問題。\[72\]

2015年10月9日，2.9.8版釋出，修正了數個問題。\[73\]

2015年11月5日，2.9.9版釋出，修正了數個問題。\[74\]

2015年12月9日，2.9.10版釋出，修正了數個問題。\[75\]

2016年2月3日，2.9.11版釋出，修正了數個問題。\[76\]

## 功能

Calligra包括下列的應用程式：

  - **[Words](../Page/Calligra_Words.md "wikilink")**，一个具有樣式表和複雜版面的排版風格編輯器的[文本编辑器](../Page/文本编辑器.md "wikilink")，用于专业的标准文档。
  - **[Sheets](../Page/Calligra_Sheets.md "wikilink")**（過去稱為**KSpread**），强大的[-{zh-hans:电子制表软件;
    zh-hant:電子試算表;}-](../Page/電子數據表.md "wikilink")。支持多表格，模板和100多個[數學公式](../Page/數學.md "wikilink")。
  - **[Stage](../Page/Calligra_Stage.md "wikilink")**（過去稱為**KPresenter**），全功能的[幻灯演示程序](../Page/幻灯.md "wikilink")。
  - **[Kexi](../Page/Kexi.md "wikilink")**，集成的[数据库环境存取软件](../Page/数据库.md "wikilink")。designed
    as a [Microsoft Access](../Page/Microsoft_Access.md "wikilink") or
    [FileMaker](../Page/FileMaker.md "wikilink") competitor. It has
    limited compatibility with the MS Access file format.
  - **[Flow](../Page/Calligra_Flow.md "wikilink")**（過去稱為**Kivio**）, a
    programmable [flowchart](../Page/flowchart.md "wikilink") drawing
    program with dynamically loadable
    [stencils](../Page/stencil.md "wikilink"). Developed by
    [theKompany](../Page/theKompany.md "wikilink"), which offers
    additional (non-free) stencils for sale.
  - **[Karbon14](../Page/Karbon14.md "wikilink")**，矢量绘图程序。
  - **[Krita](../Page/Krita.md "wikilink")**（過去稱為Krayon和KImageshop），点阵图象处理程序。
  - **[Plan](../Page/Calligra_Plan.md "wikilink")**（過去稱為**KPlato**），可以產生[甘特圖](../Page/甘特圖.md "wikilink")（[Gantt](../Page/甘特图.md "wikilink")）的專案管理軟體。
  - **[Braindump](../Page/Braindump_\(software\).md "wikilink")**，筆記程序。
  - **[Author](../Page/Calligra_Author.md "wikilink")**，可以產生電子書的軟體。

## 技術細節

所有的套件都是採用自由軟體授權，並使用[開放文件格式作為原生格式](../Page/開放文件格式.md "wikilink")。

Calligra使用[Flake和](../Page/Flake.md "wikilink")[Pigment於應用程式上](../Page/Pigment.md "wikilink")。KOffice的開發計劃，以共享基礎設施，盡可能在應用程序之間減少錯誤和改善用戶體驗。\[77\]自動化測試任務，並可以使用D-Bus或腳本語言，像[Python](../Page/Python.md "wikilink")、[Ruby以及](../Page/Ruby.md "wikilink")[JavaScript增加自行定義功能](../Page/JavaScript.md "wikilink")。\[78\]

## 參見

  - [KOffice](../Page/KOffice.md "wikilink")
  - [辦公室套裝軟體比較](../Page/辦公室套裝軟體比較.md "wikilink")

## 參考

## 外部連結

  - [Calligra Suite website](http://www.calligra-suite.org/)

[Category:KDE](../Category/KDE.md "wikilink")
[Category:辦公室自動化軟體](../Category/辦公室自動化軟體.md "wikilink")
[Category:開放原始碼](../Category/開放原始碼.md "wikilink")
[\*](../Category/KOffice.md "wikilink")

1.  [Pencils Down for KOffice Summer of Code
    Students\!](http://dot.kde.org/1188249220/)
2.  [Another nightly build from Berlin |
    kdedevelopers.org](http://www.kdedevelopers.org/node/3077)
3.  [KDE applications available for
    Haiku\!](http://tiltos.com/drupal/node/17)
4.  <http://lists.kde.org/?t=88722491100011&r=139&w=2>
5.  <http://lists.kde.org/?t=88722654500008&r=180&w=2>
6.  <http://lists.kde.org/?l=koffice&m=92516339527344&w=2>
7.  <http://www.kde.org/announcements/announce-2.0.php>
8.  [Calligra-suite.org](http://www.calligra-suite.org/news/koffice-2-3-0-released/)

9.  <http://www.calligra-suite.org/news/calligra-announces-first-snapshot-release/>
10. <http://www.calligra.org/news/calligra-announces-second-snapshot-release/>
11. <http://www.calligra.org/news/announcements/calligra-announces-third-snapshot-release/>
12. <http://www.calligra.org/news/calligra-announces-fourth-snapshot-release/>
13. <http://www.calligra.org/news/first-beta-version-of-the-calligra-suite/>
14. <http://www.calligra.org/news/announcements/calligra-2-4-beta-2/>
15. <http://www.calligra.org/news/announcements/calligra-2-4-beta-3/>
16. <http://www.calligra.org/news/announcements/calligra-2-4-beta-4/>
17. <http://www.calligra.org/news/calligra-suite-2-4-beta-5/>
18. <http://www.calligra.org/news/announcements/calligra-2-4-beta-6/>
19. <http://www.calligra.org/news/announcements/calligra-2-4-beta-7/>
20. <http://www.calligra.org/news/announcements/calligra-2-4-release-candidate-2/>
21. <http://www.calligra.org/news/calligra-2-4-released/>
22. <http://www.calligra.org/news/calligra-2-4-1-released/>
23. <http://www.calligra.org/news/calligra-2-4-2-released/>
24. <http://www.calligra.org/news/calligra-2-4-3-released/>
25. <http://www.calligra.org/news/calligra-2-4-4-and-2-5-5-released/>
26. <http://www.calligra.org/news/calligra-2-5-alpha-released/>
27. <http://www.calligra.org/news/calligra-2-5-beta-released/>
28. <http://www.calligra.org/news/calligra-2-5-release-candidate/>
29. <http://www.calligra.org/news/calligra-2-5-released/>
30. <http://www.calligra.org/news/calligra-2-5-1-released/>
31. <http://www.calligra.org/news/calligra-2-5-2-released/>
32. <http://www.calligra.org/news/calligra-2-5-3-released/>
33. <http://www.calligra.org/news/calligra-2-5-4-released/>
34. <http://www.calligra.org/news/calligra-2-4-4-and-2-5-5-released/>
35. <http://www.calligra.org/news/calligra-2-6-alpha-released/>
36. <http://www.calligra.org/news/calligra-2-6-beta-released/>
37. <http://www.calligra.org/news/calligra-2-6-release-candidate-1/>
38. <http://www.calligra.org/uncategorized/calligra-2-6-release-candidate-3/>
39. <http://www.calligra.org/news/calligra-2-6-released/>
40. <http://www.calligra.org/news/calligra-2-6-1-released/>
41. <http://www.calligra.org/news/calligra-2-6-2-released/>
42. <http://www.calligra.org/news/calligra-2-6-3-released/>
43. <http://www.calligra.org/news/calligra-2-6-4-released/>
44. <http://www.calligra.org/news/calligra-2-7-beta-released/>
45. <http://www.calligra.org/news/calligra-2-7-beta2-released/>
46. <http://www.calligra.org/news/calligra-2-7-beta-3-released/>
47. <http://www.calligra.org/news/calligra-2-7-released/>
48. <http://www.calligra.org/news/calligra-2-7-2-released/>
49. <http://www.calligra.org/news/calligra-2-7-3-released/>
50. <http://www.calligra.org/news/calligra-2-7-4/>
51. <http://www.calligra.org/news/calligra-2-7-5-released/>
52. <http://www.calligra.org/news/calligra-2-8-beta-released/>
53. <http://www.calligra.org/news/calligra-2-8-beta-2-released/>
54. <http://www.calligra.org/news/calligra-2-8-beta-3-released/>
55. <http://www.calligra.org/news/calligra-2-8-released/>
56. <http://www.calligra.org/news/calligra-2-8-1-released/>
57. <http://www.calligra.org/news/calligra-2-8-2-released/>
58. <http://www.calligra.org/news/calligra-2-8-3-released/>
59. <https://www.calligra.org/news/calligra-2-8-5-released/>
60. <https://www.calligra.org/news/calligra-2-8-6-released/>
61. <https://www.calligra.org/news/calligra-2-8-7-released/>
62. <https://www.calligra.org/news/calligra-2-9-beta-released/>
63. <https://www.calligra.org/news/calligra-2-9-beta-2-released/>
64. <https://www.calligra.org/news/calligra-2-9-beta-3-released/>
65. <https://www.calligra.org/news/calligra-2-9-released/>
66. <https://www.calligra.org/news/announcements/stable/calligra-2-9-1-released/>
67. <https://www.calligra.org/news/calligra-2-9-2-released/>
68. <https://www.calligra.org/news/calligra-2-9-4-released/>
69.
70. <https://www.calligra.org/news/calligra-2-9-5-released/>
71. <https://www.calligra.org/news/calligra-2-9-6-released/>
72. <https://www.calligra.org/news/calligra-2-9-7-released/>
73. <https://www.calligra.org/news/calligra-2-9-8-released/>
74. <https://www.calligra.org/news/calligra-2-9-9-released/>
75. <https://www.calligra.org/news/calligra-2-9-10-released/>
76. <https://www.calligra.org/news/calligra-2-9-11-released/>
77. [The KOffice Project - KOffice 2.0 Alpha 5 Release
    Announcement](http://www.koffice.org/announcements/announce-2.0alpha5.php)

78. [Kross Scripting Framework](../Page/Kross.md "wikilink")