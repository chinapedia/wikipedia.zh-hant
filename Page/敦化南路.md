[Renai_Dunhua_Circle_(2).JPG](https://zh.wikipedia.org/wiki/File:Renai_Dunhua_Circle_\(2\).JPG "fig:Renai_Dunhua_Circle_(2).JPG")
[Dunhua_South_Road_east_buildings.jpg](https://zh.wikipedia.org/wiki/File:Dunhua_South_Road_east_buildings.jpg "fig:Dunhua_South_Road_east_buildings.jpg")
[敦化南路路中分隔島上的臺灣欒樹.jpg](https://zh.wikipedia.org/wiki/File:敦化南路路中分隔島上的臺灣欒樹.jpg "fig:敦化南路路中分隔島上的臺灣欒樹.jpg")上的[臺灣欒樹](../Page/臺灣欒樹.md "wikilink")\]\]
[Dunhua_South_Road_2.jpg](https://zh.wikipedia.org/wiki/File:Dunhua_South_Road_2.jpg "fig:Dunhua_South_Road_2.jpg")
**敦化南路**是[臺北市著名的](../Page/臺北市.md "wikilink")[林蔭大道](../Page/林蔭大道.md "wikilink")，屬雙向道路，北於[八德路口接](../Page/八德路_\(臺北市\).md "wikilink")[敦化北路](../Page/敦化北路.md "wikilink")，南抵[基隆路](../Page/基隆路.md "wikilink")，分為二段。

## 通過行政區

  - [松山區](../Page/松山區_\(臺北市\).md "wikilink")
  - [大安區](../Page/大安區_\(臺北市\).md "wikilink")

## 歷史

敦化南路並不是1932年都市計劃中的道路，因此開闢時遇上一些問題\[1\]。

  - 1957年底，初闢本路由[八德路至](../Page/八德路_\(臺北市\).md "wikilink")[仁愛路](../Page/仁愛路_\(台北市\).md "wikilink")。
  - 1959年2月6日，復旦橋通車。
  - 1972年，新闢仁愛路至[信義路段](../Page/信義路_\(台北市\).md "wikilink")。
  - 1976年，續闢信義路至四維路137巷段。
  - 1977年8月，新建至[和平東路時](../Page/和平東路.md "wikilink")，因[林安泰古厝的保留與否引發了無數爭議](../Page/林安泰古厝.md "wikilink")。直到1978年[臺北市政府決定將林安泰古厝遷建於](../Page/臺北市政府.md "wikilink")[濱江公園](../Page/濱江公園.md "wikilink")，敦化南路始闢至和平東路。
  - 1988年2月3日，和平東路至基隆路段通車，敦化南路全線打通。
  - 1991年1月3日，拆除復旦橋。
  - 1991年8月19日，重編門牌，原先不分段，門牌重編後以信義路為界分一、二段。

## 分段

敦化南路共分二段。

  - 一段：北起[八德路二](../Page/八德路_\(臺北市\).md "wikilink")、三段與[敦化北路相接](../Page/敦化北路.md "wikilink")，南至[信義路四段與敦化南路二段相接](../Page/信義路_\(台北市\).md "wikilink")。
  - 二段：北於信義路四段與敦化南路一段相接，南止於[基隆路](../Page/基隆路.md "wikilink")。

## 道路設計

### 車道數

  - 一段：雙向各五車道，南向第二車道為[公車專用道](../Page/公車專用道.md "wikilink")（北向車道因為一些因素而遲未興建）。
  - 二段：雙向各四車道。

### 號碼

  - 一段：單號1～339，雙號2～376
  - 二段：單號1～333，雙號2～238

## 沿線設施

（由北至南）

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li>一段
<ul>
<li><a href="../Page/臺灣土地銀行.md" title="wikilink">臺灣土地銀行松山分行</a>(1號1F)</li>
<li><a href="../Page/土地改革紀念館.md" title="wikilink">土地改革紀念館</a>(1號10F)</li>
<li>南山敦南大樓(2號，原名<a href="../Page/萬國商業大樓.md" title="wikilink">萬國商業大樓</a>)</li>
<li><a href="../Page/敦化南北路舊式公寓群.md" title="wikilink">敦化南北路舊式公寓群</a>（光武新村）</li>
<li>捷運<a href="../Page/忠孝敦化站.md" title="wikilink">忠孝敦化站</a>（門牌設在忠孝東路口，地址為忠孝東路四段182號）</li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統龍門廣場</a></li>
<li><a href="../Page/國泰一品大廈.md" title="wikilink">國泰一品大廈</a>(236巷)</li>
<li><a href="../Page/敦南金融大樓.md" title="wikilink">敦南金融大樓</a>(243、245號)</li>
<li><a href="../Page/太平洋崇光百貨.md" title="wikilink">太平洋SOGO百貨敦化館</a>（246號）</li>
<li><a href="../Page/復興中小學.md" title="wikilink">復興中小學</a>（262號）</li>
<li><a href="../Page/仁愛圓環.md" title="wikilink">仁愛圓環</a>（仁愛路口）</li>
<li><a href="../Page/外交部外交及國際事務學院.md" title="wikilink">外交部外交及國際事務學院</a>（280號）</li>
<li><a href="../Page/敦化南北路舊式公寓群.md" title="wikilink">敦化南北路舊式公寓群</a>（敦化南路一段295巷、東豐街）</li>
<li><a href="../Page/國泰商旅.md" title="wikilink">國泰商旅台北慕軒</a>(331號)</li>
</ul></li>
</ul></td>
<td><ul>
<li>二段
<ul>
<li><a href="../Page/裕隆汽車.md" title="wikilink">裕隆企業大樓</a>（2號）</li>
<li><a href="../Page/中華票券金融公司.md" title="wikilink">中華票券金融大樓</a>（14號）</li>
<li><a href="../Page/鑽石雙星大廈.md" title="wikilink">鑽石雙星大廈</a>（<a href="../Page/林安泰古厝.md" title="wikilink">林安泰古厝舊址</a>，56號、62號）</li>
<li><a href="../Page/臺銀人壽.md" title="wikilink">臺銀人壽</a>（69號2~8樓）</li>
<li><a href="../Page/中鼎大樓.md" title="wikilink">中鼎大樓</a>（<a href="../Page/中鼎工程.md" title="wikilink">中鼎工程總部舊址</a>，77號）</li>
<li><a href="../Page/臺灣中小企業銀行.md" title="wikilink">臺灣中小企業銀行大安分行</a>（92號）</li>
<li><a href="../Page/大陸工程.md" title="wikilink">大陸工程敦南辦公大樓</a>（93號、95號）</li>
<li><a href="../Page/東帝士摩天大樓.md" title="wikilink">東帝士摩天大樓</a>（97～101號）</li>
<li><a href="../Page/趨勢科技.md" title="wikilink">趨勢科技</a>（198號8樓）</li>
<li><a href="../Page/遠企中心辦公大樓.md" title="wikilink">台北遠東國際大飯店、遠企購物中心</a>（201號）</li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統敦化基隆路口</a></li>
<li><a href="../Page/國泰建設.md" title="wikilink">國泰建設總公司</a>(218號2F)</li>
<li><a href="../Page/梅花戲院.md" title="wikilink">梅花戲院</a>（和平東路口，地址為和平東路三段63號）</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 公共藝術

  - [敦化藝術通廊](https://web.archive.org/web/20071211180402/http://taipeipublicart.culture.gov.tw/tw/itinerary.php?ID=8)

<File:DH> 04.JPG|

<center>

山居

</center>

<File:TPE4057.JPG>|

<center>

時間斑馬線

</center>

<File:DH> 06a.JPG|

<center>

鳥籠外的花園1

</center>

<File:DH> 06b.JPG|

<center>

鳥籠外的花園2

</center>

<File:DH> 07.JPG|

<center>

自在

</center>

<File:DH> 08.JPG|

<center>

稻草人

</center>

<File:DH> 09.JPG|

<center>

源

</center>

## 圖片

<File:Dunhua> South Road 4.jpg|

<center>

敦化南路分隔島一景

</center>

<File:Dunhua> South Road 5.jpg|

<center>

敦化南路分隔島一景

</center>

<File:Cyclist> crossing on Dunhua South Road, Taipei City 20080805.jpg|

<center>

敦化南路上的斑馬線

</center>

## 參見

  - [臺北市主要道路列表](../Page/臺北市主要道路列表.md "wikilink")

## 參考資料

[D敦](../Category/臺北市街道.md "wikilink")

1.