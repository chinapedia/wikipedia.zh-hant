**衣魚**（[學名](../Page/學名.md "wikilink")：**），俗稱**蠹**、**蠹魚**、**白魚**、**壁魚**、**赤木蟲**、**書蟲**或**衣蟲**，古名**蛃**(音同“丙”)，是一種靈巧、怕光、而且無翅的[昆虫](../Page/昆虫.md "wikilink")，身體呈銀灰色，因此也有**白魚**的稱號，嗜食[糖類及](../Page/食糖.md "wikilink")[澱粉等](../Page/淀粉.md "wikilink")[碳水化合物](../Page/糖类.md "wikilink")，牠的體型細長，無翅，身體上佈滿了[鱗片](../Page/鳞.md "wikilink")，[口器為咀嚼式](../Page/口器.md "wikilink")。

衣魚在昆蟲界的分類是：[衣魚目](../Page/衣魚目.md "wikilink")；[總尾目](../Page/總尾目.md "wikilink")，在[地球上已經出現約三億年](../Page/地球.md "wikilink")。

## 外形

不計[肢體](../Page/肢體.md "wikilink")，衣魚的身長約一[釐米](../Page/釐米.md "wikilink")。[觸角又長又嫩](../Page/觸角.md "wikilink")，只有三節體節有足；[脫皮三次後](../Page/蛻皮動物.md "wikilink")，銀灰色的鱗片便會長成，使其身體帶有一種[金屬般的](../Page/金屬.md "wikilink")[光澤](../Page/光澤.md "wikilink")。

頭部有細長30節以上的絲狀觸角，多數有明顯的小型[複眼](../Page/複眼.md "wikilink")，[腹部有三對能疾走](../Page/腹部.md "wikilink")、[跳躍的腳](../Page/跳躍.md "wikilink")，行動敏捷，[尾部有三根](../Page/尾部.md "wikilink")[尾巴](../Page/尾巴.md "wikilink")，因此被稱為「總尾目」或「[纓尾目](../Page/纓尾目.md "wikilink")」。

## 生長

按不同生活環境而定，衣魚從[幼蟲變](../Page/幼蟲.md "wikilink")[成蟲需要至少四個月的時間](../Page/成蟲.md "wikilink")，不過有時候發育期會長達三年。在[室溫環境下](../Page/室溫.md "wikilink")，大概一年就發育為成蟲，壽命約為兩到八年。一條衣魚的一生裡會經歷大約八次[脫皮](../Page/蛻皮動物.md "wikilink")；不過衣魚不斷生長，一年脫皮四次也不足為奇。當溫度在[攝氏](../Page/攝氏.md "wikilink")25至30度，雌蟲就會在物件的裂縫裡產大約一百顆[卵](../Page/卵.md "wikilink")，當溫度過高，衣魚也無法存活；可是在寒冷或乾燥的環境下，衣魚是不會交配的，衣魚較喜好於潮濕的環境下生殖。

## 食物

衣魚愛好的[食物為充滿](../Page/食物.md "wikilink")[澱粉質或](../Page/淀粉.md "wikilink")[多糖的物品](../Page/多糖.md "wikilink")，如：[膠水裡的](../Page/黏合劑.md "wikilink")[葡聚糖](../Page/葡聚糖.md "wikilink")、[漿糊](../Page/糊精.md "wikilink")、[書籍裝訂物](../Page/图书.md "wikilink")、[照片](../Page/相片.md "wikilink")、[糖](../Page/食糖.md "wikilink")、[毛髮](../Page/體毛.md "wikilink")、[泥土等](../Page/土壤.md "wikilink")。可是牠們對[棉花](../Page/棉花.md "wikilink")、[亞麻布](../Page/亚麻.md "wikilink")、[絲和](../Page/丝绸.md "wikilink")[人造纖維等也毫不抗拒](../Page/人造纖維.md "wikilink")，甚至連其他[昆虫屍體](../Page/昆虫.md "wikilink")、自己脫的[皮也是照吃如饴](../Page/皮膚.md "wikilink")。飢餓時甚至連[皮革製品](../Page/皮革.md "wikilink")、[人造纖維布匹等也吃](../Page/人造纖維.md "wikilink")。不過衣魚能夠捱餓數個月，身體機能也不會受任何傷害。

## 生活環境

[Bruce_Chatwin's_Songlines_1987.jpg](https://zh.wikipedia.org/wiki/File:Bruce_Chatwin's_Songlines_1987.jpg "fig:Bruce_Chatwin's_Songlines_1987.jpg")
[A_silverfish_at_ONE-iAdvantage.jpg](https://zh.wikipedia.org/wiki/File:A_silverfish_at_ONE-iAdvantage.jpg "fig:A_silverfish_at_ONE-iAdvantage.jpg")
如[斑衣魚](../Page/斑衣魚.md "wikilink")（*Thermobia
domestica*）一樣，衣魚出沒於人類聚居地。家居裡的各種地方，如[冰箱底部](../Page/冰箱.md "wikilink")、開[暖气的](../Page/暖气.md "wikilink")[浴室](../Page/浴室.md "wikilink")、[地磚的裂縫裡都可能會有衣魚的蹤影](../Page/地板.md "wikilink")。衣魚喜歡咬破[書籍](../Page/图书.md "wikilink")、[纖維及](../Page/纖維.md "wikilink")[纺织品](../Page/纺织品.md "wikilink")。而斑衣魚則喜歡如麵包店等更溫暖的環境，牠們非常喜歡進食[麵粉和](../Page/麵粉.md "wikilink")[麵包](../Page/麵包.md "wikilink")，不過也會進食[動物製品](../Page/動物.md "wikilink")。

## 繁殖

[Lepisma_saccharina.jpg](https://zh.wikipedia.org/wiki/File:Lepisma_saccharina.jpg "fig:Lepisma_saccharina.jpg")
交配時，雄蟲跟雌蟲會到處竄動。雄蟲會產下一個用[薄紗包住的](../Page/膜.md "wikilink")[精囊](../Page/精囊.md "wikilink")；由於生理狀態成熟，雌蟲會找到該精囊，拾取作[受精用](../Page/受精.md "wikilink")。

## 天敵

衣魚最有名的天敵是[蠼螋](../Page/蠼螋.md "wikilink")。

衣魚為防止[蜘蛛](../Page/蜘蛛.md "wikilink")、[蠅虎等天敵捕食](../Page/蠅虎科.md "wikilink")，停息時總是不停地擺動著尾梢，誘使天敵將注意力集中到尾梢上來，當尾巴被天敵抓住，分節的尾毛即斷掉，主身體便可乘機逃脫。

## 防治

衣魚雖然對人類生活造成滋擾，但其實是無害的。

在建築物裡，衣魚必須要有潮濕及有空隙的環境才能生存；只要環境乾燥、建築物沒有裂縫，衣魚就會自然消失。以下一些撲滅或消除衣魚的方法，雖然有效，但都是治標不治本的：

  - 混合比例為1:1的[硼砂和](../Page/硼砂.md "wikilink")[砂糖](../Page/食糖.md "wikilink")，能有效殺除衣魚。
  - [氯化铵水的氣味應該能於](../Page/氯化铵.md "wikilink")24小時內驅趕衣魚。
  - 若要捕捉衣魚，將[石膏粉灑在浸濕的白棉布上](../Page/石膏.md "wikilink")，隔夜放在房間一角，放的地方要接近衣魚的藏身處。
  - 以下方法同樣有用：可以在衣魚藏身處旁邊放一塊木板，板上再放一顆稍微磨碎的馬鈴薯；衣魚晚上出沒時就會鑽進馬鈴薯裡面大快朵頤，次天早上，你就可以把馬鈴薯連同衣魚一起丟掉。
  - 使用[樟腦丸](../Page/樟腦丸.md "wikilink")（或[萘丸](../Page/萘.md "wikilink")）可以讓衣魚不敢靠近。

在中國古代，讀書人家會將一種叫[芸香草的植物放在自己的書櫃](../Page/芸香属.md "wikilink")，能夠驅除書蟲，且會發出一種淡淡的清香，所以一直到今還在延續“書香門第”這一說法。

現代人驅除書蟲用[樟腦丸](../Page/樟腦丸.md "wikilink")，或[香薰](../Page/芳香療法.md "wikilink")[精油](../Page/精油.md "wikilink")，也有人用[乾辣椒粉和](../Page/辣椒.md "wikilink")[蒜頭](../Page/蒜.md "wikilink")。書蟲名稱又叫書蠹，書蠹是蠹蟲龐大家族的一種。

## 参见

  - [本草綱目](../Page/本草綱目.md "wikilink")

## 外部連結

  - [Silberfische
    bekämpfen](http://www.hausgarten.net/gartenpflege/pflanzenschutz/silberfische-bekaempfung.html)
  - [衣魚簡介及其防治](http://myweb.hinet.net/home14/pests/f04.html)
    商業除蟲公司介紹衣魚的網頁
  - [能敵滅蟲](http://www.trulycare.com.hk/15.html) 商業除蟲公司介紹衣魚的網頁

[Category:昆蟲綱](../Category/昆蟲綱.md "wikilink")
[Category:动物药](../Category/动物药.md "wikilink")
[Category:害蟲](../Category/害蟲.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")