[HK_Nina_Tower_200803.jpg](https://zh.wikipedia.org/wiki/File:HK_Nina_Tower_200803.jpg "fig:HK_Nina_Tower_200803.jpg")\]\]
[Eden_Gate.JPG](https://zh.wikipedia.org/wiki/File:Eden_Gate.JPG "fig:Eden_Gate.JPG")\]\]
[Pictorial_Garden.jpg](https://zh.wikipedia.org/wiki/File:Pictorial_Garden.jpg "fig:Pictorial_Garden.jpg")\]\]
[HK_Lucky_Plaza_201004.jpg](https://zh.wikipedia.org/wiki/File:HK_Lucky_Plaza_201004.jpg "fig:HK_Lucky_Plaza_201004.jpg")\]\]
[GoldenLionGarden.jpg](https://zh.wikipedia.org/wiki/File:GoldenLionGarden.jpg "fig:GoldenLionGarden.jpg")\]\]
[Hong_Kong_Garden.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Garden.jpg "fig:Hong_Kong_Garden.jpg")\]\]
[Repluse_Bay_129.jpg](https://zh.wikipedia.org/wiki/File:Repluse_Bay_129.jpg "fig:Repluse_Bay_129.jpg")\]\]
[L'Hotel_Island_South_viewed_from_Bennet's_Hill.jpg](https://zh.wikipedia.org/wiki/File:L'Hotel_Island_South_viewed_from_Bennet's_Hill.jpg "fig:L'Hotel_Island_South_viewed_from_Bennet's_Hill.jpg")\]\]

**華懋集團**是[香港](../Page/香港.md "wikilink")[地產發展商](../Page/地產發展商.md "wikilink")，主要業務為[地產](../Page/地產.md "wikilink")[投資](../Page/投資.md "wikilink")、發展及[建築](../Page/建築.md "wikilink")，現時總部位於[新界](../Page/新界.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[如心廣場](../Page/如心廣場.md "wikilink")。由於華懋集團不是[香港上市公司](../Page/香港上市公司.md "wikilink")，所以新舊管理層是誰，是傳媒最關注之一點。集團原主席[龔如心自](../Page/龔如心.md "wikilink")2007年去世後，便由其弟[龔仁心接任](../Page/龔仁心.md "wikilink")[主席一職](../Page/董事局主席.md "wikilink")。

## 簡介

集團由[王德輝家族於](../Page/王德輝.md "wikilink")1960年創辦，早年由王德輝、[龔如心夫婦攜手經營](../Page/龔如心.md "wikilink")，1990年王德輝失蹤後由龔如心管理。

華懋集團發展的物業主要為住宅、寫字樓、購物商場、工貿大廈及酒店等共超過300座，業務除地產外亦包括[財務](../Page/財務.md "wikilink")、[保險](../Page/保險.md "wikilink")、[娛樂事業](../Page/娛樂.md "wikilink")、[資訊科技和](../Page/資訊科技.md "wikilink")[零售等](../Page/零售.md "wikilink")，員工超過4000人，華懋集團一直以來都沒有在[香港交易所](../Page/香港交易所.md "wikilink")[上市](../Page/上市.md "wikilink")。

華懋集團發展的物業價格較其他發展商的物業廉宜，但由於其位置、設計、用料及住客設備等未如理想，因此其升值能力亦不及其他發展商的物業。華懋集團早年低價在[新界購入大量](../Page/新界.md "wikilink")[地皮](../Page/地皮.md "wikilink")，並積極收集[乙種換地權益書](../Page/乙種換地權益書.md "wikilink")，因此華懋興建的物業主要分佈於[新界](../Page/新界.md "wikilink")，如[荃灣](../Page/荃灣.md "wikilink")、[沙田及](../Page/沙田.md "wikilink")[屯門](../Page/屯門.md "wikilink")。

1988年，王德輝、龔如心夫婦籌備將華懋集團上市，但後來他們認為[測計師對華懋撥作上市的物業估價不理想](../Page/測計師.md "wikilink")，便決定不上市\[1\]。

1994年，華懋集團宣佈以100億港元在[荃灣](../Page/荃灣.md "wikilink")[楊屋道興建樓高逾](../Page/楊屋道.md "wikilink")520米的全球最高建築物[如心廣場](../Page/如心廣場.md "wikilink")，但因[香港國際機場搬遷後荃灣位處主要航道](../Page/香港國際機場.md "wikilink")，被政府限制其高度，結果要一分為二興建，更因未能如期完成而被罰款5.6億港元。最終，如心廣場在2008年正式完工。

1997年，華懋集團以逾55億港元投得[淺水灣道129號的前英軍宿舍地皮](../Page/淺水灣道129號.md "wikilink")，其後地產市況逆轉，引致華懋帳面損失不少。建築物採下窄上闊的傾斜式設計，有「[百合花](../Page/百合花.md "wikilink")」之稱。華懋曾申請把這座[豪宅轉作](../Page/豪宅.md "wikilink")[酒店用途](../Page/酒店.md "wikilink")，但一直未獲批准，最後於2010年6月1日啟用。

2006年6月，華懋集團表示計劃將[上水名都商場](../Page/上水名都商場.md "wikilink")、[粉嶺名都商場](../Page/粉嶺名都商場.md "wikilink")、[屯門](../Page/屯門.md "wikilink")[康麗花園商場](../Page/康麗花園.md "wikilink")、[沙田](../Page/沙田.md "wikilink")[希爾頓中心商場](../Page/希爾頓中心.md "wikilink")、[元朗](../Page/元朗.md "wikilink")[好順福大廈商場的商場連車位](../Page/好順福大廈.md "wikilink")，以及[西貢花園](../Page/西貢花園.md "wikilink")、[青龍頭](../Page/青龍頭.md "wikilink")[豪景花園等一籃子街舖](../Page/豪景花園.md "wikilink")，分拆以[不動產投資信託形式上市](../Page/不動產投資信託.md "wikilink")。

2010年11月3日以21.7億元投得九龍塘[延文禮士道地皮](../Page/延文禮士道.md "wikilink")。

2012年1月12日全資附屬達潤投資以26億元成功投得西鐵[荃灣西站城畔物業發展項目](../Page/荃灣西站.md "wikilink")。「城畔」的住宅樓面面積6.6萬平方米，商業樓面面積1.1萬平方米，可以提供942個住宅單位，當中62%單位實用面積將不多於50平方米。

2013年3月6日華懋集團以十三億元獨資奪得，樓面呎價2876元投得西鐵朗屏站（南）項目，地盤面積為九萬零三百餘方呎，可建樓面逾四十五萬方呎，

2013年6月25日華懋集團以30億元，每呎樓面地價約3654元，投得將軍澳68B1區地皮，地皮面積283,115平方呎，最高可建樓面面積821,035平方呎

2018年1月2日，華懋集團委任前南豐董事總經理[蔡宏興為集團行政總裁兼執行董事](../Page/蔡宏興.md "wikilink")。

2018年1月24日[華懋集團以](../Page/華懋集團.md "wikilink")31.128億元，每呎樓面地價12003元，投得觀塘[安達臣道首幅私人用地](../Page/安達臣道.md "wikilink")，地盤面積57,630平方呎，指定作私人住宅用途。最低及最高的樓面面積分別為155,604平方呎及259,337平方呎。

2018年10月23日[華懋集團全資附屬公司堡雅有限公司以補地價](../Page/華懋集團.md "wikilink")74.8653億元，投得[何文田站第二期項目](../Page/何文田站.md "wikilink")，以可建樓面面積上限約639382平方呎計算，每方呎補地價11709元，創鐵路項目新高，發展商需要向港鐵提供25%的分紅比例，以及支付一筆額外款項作競投項目的條件之一

## 管理層

  - 華懋集團管治委員會主席兼[華懋慈善基金主席](../Page/華懋慈善基金.md "wikilink")：[龔仁心](../Page/龔仁心.md "wikilink")
  - [行政總裁兼執行董事](../Page/行政總裁.md "wikilink")：[蔡宏興](../Page/蔡宏興.md "wikilink")\[2\]
  - 執行董事兼財務總監：[陳鑑波](../Page/陳鑑波.md "wikilink")\[3\]
  - 執行董事兼行政部總監（協助管理[華懋慈善基金](../Page/華懋慈善基金.md "wikilink")）：[龔因心](../Page/龔因心.md "wikilink")\[4\]
  - 執行董事兼市場部總監：[龔中心](../Page/龔中心.md "wikilink")
  - 執行董事兼投資總監兼[安寧控股主席及代理行政總裁](../Page/安寧控股.md "wikilink")：[梁榮江](../Page/梁榮江.md "wikilink")\[5\]
  - 執行董事兼出納部主管：[李國祺](../Page/李國祺.md "wikilink")
  - 庫務總監及土地╱估值部主管：[梁煒才](../Page/梁煒才.md "wikilink")
  - 銷售部總監：[吳崇武](../Page/吳崇武.md "wikilink")
  - 海外顧問兼[娛樂部執行董事](../Page/華懋電影院線.md "wikilink")：[王禮泉](../Page/王禮泉.md "wikilink")\[6\]
  - 人事部主管：[劉元春](../Page/劉元春.md "wikilink")
  - 行政總裁助理：[龔飆](../Page/龔飆.md "wikilink")\[7\]
  - 物業管理部高級經理：[龔皓](../Page/龔皓.md "wikilink")

## 主要物業組合

### 香港商業項目

  - 荃灣[如心廣場](../Page/如心廣場.md "wikilink")
  - 荃灣[華懋荃灣廣場](../Page/華懋荃灣廣場.md "wikilink")
  - 尖沙咀[華懋金馬倫中心](../Page/華懋金馬倫中心.md "wikilink")
  - 尖沙咀[華懋廣場](../Page/華懋廣場.md "wikilink")
  - 尖沙咀赫德道[顯赫16](../Page/顯赫16.md "wikilink")
  - 中環[華懋中心一期及二期](../Page/華懋中心.md "wikilink")，前身為[永安人壽大廈及永安中區大廈](../Page/永安人壽大廈.md "wikilink")
  - 中環[華人銀行大廈](../Page/華人銀行大廈.md "wikilink")
  - 中環[華懋廣場II期](../Page/華懋廣場.md "wikilink")
  - 中環[華懋荷里活中心](../Page/華懋荷里活中心.md "wikilink")
  - 中環干諾道中34-37號[華懋大廈](../Page/華懋大廈.md "wikilink")
  - 灣仔軒尼詩道1號[One
    Hennessy](../Page/One_Hennessy.md "wikilink")，前身為[熙信大廈](../Page/熙信大廈.md "wikilink")
  - 灣仔告士打道[華懋世紀廣場](../Page/華懋世紀廣場.md "wikilink")
  - 灣仔[華懋莊士敦廣場](../Page/華懋莊士敦廣場.md "wikilink")
  - 銅鑼灣[華懋禮頓廣場](../Page/華懋禮頓廣場.md "wikilink")
  - 鰂魚涌[華懋交易廣場](../Page/華懋交易廣場.md "wikilink")
  - 北角[華懋交易廣場2期](../Page/華懋交易廣場2期.md "wikilink")
  - 長沙灣[華懋333廣場](../Page/華懋333廣場.md "wikilink")

### 商場

  - 沙田富豪花園商場
  - 沙田希爾頓中心商場
  - 沙田好運中心商場
  - 粉嶺名都商場
  - 上水名都商場
  - 馬鞍山迎濤灣商場
  - 元朗好順福購物中心
  - 豪景花園商場
  - 屯門巴黎倫敦紐約戲院購物中心

### 如心酒店管理有限公司

  - [如心海景酒店暨會議中心](../Page/如心海景酒店暨會議中心.md "wikilink")，1608間客房
  - [如心艾朗酒店](../Page/如心艾朗酒店.md "wikilink")
  - [如心銅鑼灣海景酒店](../Page/如心銅鑼灣海景酒店.md "wikilink")
  - [如心南灣海景酒店](../Page/如心南灣海景酒店.md "wikilink")
  - [旺角薈賢居](../Page/旺角薈賢居.md "wikilink")
  - [灣仔薈賢居](../Page/灣仔薈賢居.md "wikilink")

### 香港住宅項目

#### [港島區](../Page/港島區.md "wikilink")

  - [The Lily](../Page/The_Lily.md "wikilink")
  - [北角建華花園](../Page/北角.md "wikilink")
  - [灣仔](../Page/灣仔.md "wikilink") [泰和閣](../Page/泰和閣.md "wikilink")
  - [西環西寧閣](../Page/西環.md "wikilink")
  - [薄扶林](../Page/薄扶林.md "wikilink") [華亭閣](../Page/華亭閣.md "wikilink")
  - [薄扶林](../Page/薄扶林.md "wikilink")
    [域多利花園](../Page/域多利花園.md "wikilink")
  - [薄扶林碧荔道](../Page/薄扶林.md "wikilink")55至57號碧麗軒
  - [旭龢道](../Page/旭龢道.md "wikilink")[大學閣重建成](../Page/大學閣.md "wikilink")3幢14層高住宅連一層地庫及會所設施，總樓面約15.3萬平方呎
  - [赤柱山莊](../Page/赤柱山莊.md "wikilink")
  - [赤柱村道](../Page/赤柱村道.md "wikilink") 28號
  - [赤柱](../Page/赤柱.md "wikilink")[紅山半島](../Page/紅山半島.md "wikilink")
  - 麥當勞道3號

#### 九龍區

  - [九龍城](../Page/九龍城.md "wikilink")[御·豪門](../Page/御·豪門.md "wikilink")（實用率約75%）
  - [九龍城](../Page/九龍城.md "wikilink")[豪門](../Page/豪門.md "wikilink")（實用率約75%）
  - [九龍城](../Page/九龍城.md "wikilink")[御·門前](../Page/御·門前.md "wikilink")（實用率約75%）
  - [九龍城侯王道](../Page/九龍城.md "wikilink")28號[金‧御門](../Page/金‧御門.md "wikilink")
  - [黃大仙豪園](../Page/黃大仙.md "wikilink")（實用率約61-63%）
  - [黃大仙鳳錦樓](../Page/黃大仙.md "wikilink")
  - [深水埗寓](../Page/深水埗.md "wikilink")·弍捌（實用率約67%）
  - [九龍塘](../Page/九龍塘.md "wikilink")[義德道](../Page/義德道.md "wikilink")5、7、9及11號[雲門](../Page/雲門.md "wikilink")
  - [九龍塘](../Page/九龍塘.md "wikilink")[延文禮士道](../Page/延文禮士道.md "wikilink")38號賢文禮仕

#### 沙田區

  - [大圍](../Page/大圍.md "wikilink")[金獅花園](../Page/金獅花園.md "wikilink")（實用率約60-63%）
  - [富豪花園](../Page/富豪花園.md "wikilink")（實用率約68%）
  - [碧濤花園](../Page/碧濤花園.md "wikilink")（實用率約68-70%）
  - [火炭](../Page/火炭.md "wikilink")[碧霞花園](../Page/碧霞花園.md "wikilink")（實用率約77%）
  - [沙田圍](../Page/沙田圍.md "wikilink")[花園城](../Page/花園城.md "wikilink")（實用率約58-63%）
  - [沙田市中心](../Page/沙田市中心.md "wikilink")[希爾頓中心](../Page/希爾頓中心.md "wikilink")（實用率約61-63%）
  - [沙田市中心](../Page/沙田市中心.md "wikilink")[好運中心](../Page/好運中心.md "wikilink")（實用率約80%）
  - [馬鞍山](../Page/馬鞍山.md "wikilink")[迎濤灣](../Page/迎濤灣.md "wikilink")（實用率約75-80%）

#### [大埔區](../Page/大埔區.md "wikilink")

  - [富·盈門](../Page/富·盈門.md "wikilink")
  - [太和翠林閣](../Page/太和.md "wikilink")

#### [北區](../Page/北區_\(香港\).md "wikilink")

  - [粉嶺名都](../Page/粉嶺名都.md "wikilink")（實用率約76%）
  - [上水名都](../Page/上水名都.md "wikilink")（實用率約76%）
  - [花都廣場](../Page/花都廣場.md "wikilink")
  - [維也納花園](../Page/維也納花園.md "wikilink")
  - [威尼斯花園](../Page/威尼斯花園.md "wikilink")

#### 荃灣區

  - [豪輝花園](../Page/豪輝花園.md "wikilink")（實用率約61-63%）
  - [豪景花園](../Page/豪景花園.md "wikilink")（實用率約64-73%）
  - [傲庭峰](../Page/傲庭峰.md "wikilink")（豪景花園19-21座，實用率約70%）
  - [御濤·凱濤](../Page/御濤·凱濤.md "wikilink")（豪景花園27-28座，實用率約73%）
  - 荃灣西站[全城滙](../Page/全城滙.md "wikilink")

#### 屯門區

  - [康麗花園](../Page/康麗花園.md "wikilink")
  - [好勝大廈](../Page/好勝大廈.md "wikilink")
  - [南岸](../Page/南岸.md "wikilink")
  - [琨崙](../Page/琨崙.md "wikilink")

#### 元朗區

  - [好順意大廈](../Page/好順意大廈.md "wikilink")
  - [好順利大廈](../Page/好順利大廈.md "wikilink")
  - [好順景大廈](../Page/好順景大廈.md "wikilink")
  - [好順福大廈](../Page/好順福大廈.md "wikilink")
  - [好順泰大廈](../Page/好順泰大廈.md "wikilink")
  - [鳳翔大廈](../Page/鳳翔大廈.md "wikilink")
  - 西鐵朗屏站（南）項目[朗城匯](../Page/朗城匯.md "wikilink")

#### [西貢區](../Page/西貢區.md "wikilink")

  - [西貢花園](../Page/西貢花園.md "wikilink")
  - [西貢苑](../Page/西貢苑.md "wikilink")
  - [將軍澳中心](../Page/將軍澳中心.md "wikilink")
  - [將軍澳豪庭](../Page/將軍澳豪庭.md "wikilink")
  - [海翩滙](../Page/海翩滙.md "wikilink")
  - 西貢安寧徑一號洋房
  - 西貢碧沙路洋房

<includeonly></includeonly>

## 華懋概念股

  - [安寧控股](../Page/安寧控股.md "wikilink")（34.59%）（[龔如心遺產](../Page/龔如心.md "wikilink")，並透過
    Diamond Leaf Limited 及 Solution Bridge Limited 持有）\[8\]
  - [丹楓控股](../Page/丹楓控股.md "wikilink")（23.09%）（[龔如心遺產亦以](../Page/龔如心.md "wikilink")[王德輝夫人名義所擁有](../Page/王德輝.md "wikilink")，並透過
    Greenwood International Limited 及 Talbot Investment Limited 持有）\[9\]
  - [博富臨置業](../Page/博富臨置業.md "wikilink")（20.7%）（[龔如心遺產](../Page/龔如心.md "wikilink")，並透過
    Madison Profits Limited 持有）\[10\]
  - [宏霸數碼](../Page/宏霸數碼.md "wikilink")（6.55%）（[龔如心遺產亦以](../Page/龔如心.md "wikilink")[王德輝夫人名義所擁有](../Page/王德輝.md "wikilink")，並透過
    Veron International Limited 持有）\[11\]

註：個別股份的持股量已低於要披露的5%水平或已在市場上沽售。

## 参考文献

## 外部链接

  - [華懋集團官方网站](http://www.chinachemgroup.com/)

## 參見

  - [華懋慈善基金](../Page/華懋慈善基金.md "wikilink")
  - [王德輝](../Page/王德輝.md "wikilink")
  - [龔如心](../Page/龔如心.md "wikilink")

{{-}}

[Category:華懋集團](../Category/華懋集團.md "wikilink")
[Category:香港地產公司](../Category/香港地產公司.md "wikilink")
[Category:香港家族式企業](../Category/香港家族式企業.md "wikilink")

1.  韋基舜，《成報》專欄，吾土吾情：王氏夫婦邀談上市，2007年4月22日
2.  [蔡宏興：華懋變革 貨如輪轉 闢新路線 不跟風建「納米樓」 | 明報 | 經濟
    | 20180118](https://news.mingpao.com/pns/dailynews/web_tc/article/20180118/s00004/1516211686913)
3.  [《明報》李天命網上思考](http://leetm.mingpao.com/cfm/Forum3.cfm?CategoryID=4&TopicID=2083&TopicOrder=Desc&TopicPage=1)

4.  [龔因心證姊姊熱心行善 龔如心擬邀朱鎔基、安南管基金 | 蘋果日報 | 要聞港聞
    | 20090603](http://hk.apple.nextmedia.com/news/art/20090603/12833573)
5.  [管理層](http://www.enmholdings.com/trad/ourteam.htm)
6.  [王禮泉不知情 氣憤要「申訴」_星島日報_加拿大多倫多中文新聞網。 Canada Toronto Chinese
    newspaper](http://news.singtao.ca/toronto/2013-05-25/hongkong1369471039d4510965.html)
7.  [龔家盡佔華懋要職 | 蘋果日報 | 要聞港聞
    | 20130608](http://hk.apple.nextmedia.com/news/art/20130608/18289323)
8.  [安寧控股2014年年報](http://file.irasia.com/listco/hk/enmholdings/annual/2014/car2014.pdf)
9.  [丹楓控股2014年年報](http://www.danform.com.hk/netbuilder/Attach.nsf/AttachNodeByID/352250BF849B69ED48257E2D002D4A34/$File/C%20-%20Annual%20Report.pdf)
10. [博富臨置業2014年/2015年年報](http://www.hkexnews.hk/listedco/listconews/sehk/2015/1117/LTN20151117436_C.pdf)
11. [宏霸數碼2015年年報](http://www.hkexnews.hk/listedco/listconews/sehk/2015/0429/LTN20150429414_C.pdf)