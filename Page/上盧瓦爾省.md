**上盧瓦爾省**（[法文](../Page/法文.md "wikilink")：）是[法國](../Page/法國.md "wikilink")[奧文尼-隆-阿爾卑斯](../Page/奧文尼-隆-阿爾卑斯.md "wikilink")[大區所轄的](../Page/大區.md "wikilink")[省份](../Page/省_\(法国\).md "wikilink")。該省編號為43。

## 历史

上卢瓦尔省是在[1790年](../Page/1790.md "wikilink")[3月4号爆发的](../Page/3月4号.md "wikilink")[法国大革命中成立的最初的](../Page/法国大革命.md "wikilink")83个省份之一，包括旧省[奥弗涅](../Page/奥弗涅_\(旧省\).md "wikilink")，[朗格多克](../Page/朗格多克_\(旧省\).md "wikilink")，和[里昂的部分地域](../Page/里昂_\(旧省\).md "wikilink")。

## 地理

上卢瓦尔省属于[奥弗涅](../Page/奥弗涅.md "wikilink")*[大区](../Page/大区_\(法国\).md "wikilink")*的一部分，相邻的省份有[卢瓦尔省](../Page/卢瓦尔省.md "wikilink")、[阿尔代什省](../Page/阿尔代什省.md "wikilink")、[洛泽尔省](../Page/洛泽尔省.md "wikilink")、[康塔尔省](../Page/康塔尔省.md "wikilink")，和[多姆山省](../Page/多姆山省.md "wikilink")。

该省覆盖了卢瓦尔省的上部。

## 人口

该省驻民称为*Altiligériens*.

## 其他链接

  - [Prefecture website](http://www.haute-loire.pref.gouv.fr/) (in
    French)
  - [Conseil Général website](http://www.cg43.fr/) (in French)

[Category:法国省份](../Category/法国省份.md "wikilink")
[H](../Category/奧弗涅-羅納-阿爾卑斯大區.md "wikilink")