**[大型多人在线角色扮演游戏](../Page/大型多人在线角色扮演游戏.md "wikilink")**（**MMORPG**）為[電子遊戲的一種](../Page/電子遊戲.md "wikilink")，是[電子角色扮演遊戲按](../Page/電子角色扮演遊戲.md "wikilink")[電子遊戲人數分類分別出來的一種](../Page/電子遊戲#人數類型.md "wikilink")[網絡遊戲](../Page/網絡遊戲.md "wikilink")。在所有MMORPG中，玩家都可扮演一个或多个[虛擬角色](../Page/虛擬角色.md "wikilink")，并控制该角色的在遊戲中虛擬世界的活动與[行為](../Page/行為.md "wikilink")。大型多人在线角色扮演游戏与单机游戏（例如[三国志](../Page/三国志_\(游戏\).md "wikilink")、[魔獸爭霸系列](../Page/魔獸爭霸系列.md "wikilink")）和其他小型的、由多人参加的角色扮演游戏（例如[反恐精英](../Page/反恐精英.md "wikilink")）的区别在于：大型多人在线角色扮演游戏具有一个持续運行的虚拟世界，玩家离开游戏之后，这个虚拟世界仍在[网络游戏运营商提供的](../Page/网络游戏运营商.md "wikilink")[主机服务器里继续存在](../Page/伺服器.md "wikilink")，并且不断演进，直至遊戲終止運作。

本表列出了大型多人線上角色扮演遊戲。

## 休閒線上遊戲列表

<table>
<thead>
<tr class="header">
<th><p>遊戲名稱</p></th>
<th><p>遊戲類型</p></th>
<th><p>營運狀態</p></th>
<th><p>代理商/營運商</p></th>
<th><p>製作公司</p></th>
<th><p>製作公司所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/突擊風暴.md" title="wikilink">突擊風暴</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尬車Online.md" title="wikilink">尬車Online</a></p></td>
<td><p><a href="../Page/競速遊戲.md" title="wikilink">競速遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/紅心辣椒.md" title="wikilink">紅心辣椒</a></p></td>
<td><p><a href="../Page/ICE.md" title="wikilink">ICE</a></p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/絕對武力Online.md" title="wikilink">CSOnline</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/跑跑卡丁車.md" title="wikilink">跑跑卡丁車</a></p></td>
<td><p><a href="../Page/競速遊戲.md" title="wikilink">競速遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/勁舞團.md" title="wikilink">勁舞團</a></p></td>
<td><p><a href="../Page/跳舞遊戲.md" title="wikilink">跳舞遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/因思銳.md" title="wikilink">因思銳</a></p></td>
<td><p><a href="../Page/YEDANG.md" title="wikilink">T3</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/街头篮球_(游戏).md" title="wikilink">Free Style</a></p></td>
<td><p><a href="../Page/運動遊戲.md" title="wikilink">運動遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/紅心辣椒娛樂科技.md" title="wikilink">紅心辣椒娛樂科技</a></p></td>
<td><p><a href="../Page/Joy_City_Entertainment.md" title="wikilink">Joy City Entertainment</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SFonline.md" title="wikilink">SFOnline</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/華義國際.md" title="wikilink">華義國際</a></p></td>
<td><p><a href="../Page/Dragonfly.md" title="wikilink">Dragonfly</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/火線特戰隊.md" title="wikilink">火線特戰隊</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/Doobic.md" title="wikilink">Doobic</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/坦克王_Online.md" title="wikilink">坦克王 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/富格曼科技.md" title="wikilink">富格曼科技</a></p></td>
<td><p><a href="../Page/softnyx.md" title="wikilink">softnyx</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PangYa_魔法飛球.md" title="wikilink">PangYa 魔法飛球</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/NTREEV_SOFT.md" title="wikilink">NTREEV SOFT</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/永恆冒險.md" title="wikilink">永恆冒險</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/KOG_Studios.md" title="wikilink">KOG Studios</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HiYo衝天跑.md" title="wikilink">HiYo衝天跑</a></p></td>
<td></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/NOWCOM.md" title="wikilink">NOWCOM</a>&amp;<a href="../Page/戲谷.md" title="wikilink">戲谷</a></p></td>
<td><p><a href="../Page/Rhaon_Entertainment.md" title="wikilink">Rhaon Entertainment</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/全民打棒球.md" title="wikilink">全民打棒球</a><br />
（魔球魔球）<br />
</p></td>
<td><p><a href="../Page/運動遊戲.md" title="wikilink">運動遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/紅心辣椒.md" title="wikilink">紅心辣椒</a>&amp;<a href="../Page/CJ_Internet.md" title="wikilink">CJ Internet</a></p></td>
<td><p><a href="../Page/AniPark.md" title="wikilink">AniPark</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彩虹冒險.md" title="wikilink">彩虹冒險</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/猿人在線.md" title="wikilink">猿人在線</a></p></td>
<td><p><a href="../Page/Hanbit_Soft.md" title="wikilink">Hanbit Soft</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/網球拍拍_Online.md" title="wikilink">網球拍拍 Online</a></p></td>
<td><p><a href="../Page/運動遊戲.md" title="wikilink">運動遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲新幹線.md" title="wikilink">遊戲新幹線</a></p></td>
<td><p><a href="../Page/Npicsoft.md" title="wikilink">Npicsoft</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/爆爆王.md" title="wikilink">爆爆王</a></p></td>
<td></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格鬥王_Online.md" title="wikilink">格鬥王 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/松崗科技.md" title="wikilink">松崗科技</a></p></td>
<td><p><a href="../Page/GAME_NEAP.md" title="wikilink">GAME NEAP</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新瘋狂坦克.md" title="wikilink">新瘋狂坦克</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/戲谷.md" title="wikilink">戲谷</a></p></td>
<td><p><a href="../Page/CCR.md" title="wikilink">CCR</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/12起肖.md" title="wikilink">12起肖</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/松崗科技.md" title="wikilink">松崗科技</a></p></td>
<td><p><a href="../Page/Dreamedia.md" title="wikilink">Dreamedia</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/生存Online.md" title="wikilink">生存Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/瑪亞線上.md" title="wikilink">瑪亞線上</a></p></td>
<td><p><a href="../Page/HANEOL_SOFT.md" title="wikilink">HANEOL SOFT</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Kero_King.md" title="wikilink">Kero King</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/猿人在線.md" title="wikilink">猿人在線</a></p></td>
<td><p><a href="../Page/Program_Bank.md" title="wikilink">Program Bank</a>&amp;<a href="../Page/BANDAI_Korea.md" title="wikilink">BANDAI Korea</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/澎澎幫.md" title="wikilink">澎澎幫</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/崇凱科技.md" title="wikilink">崇凱科技</a></p></td>
<td><p><a href="../Page/AOZORA.md" title="wikilink">AOZORA</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/唯舞獨尊_Online.md" title="wikilink">唯舞獨尊 Online</a></p></td>
<td><p><a href="../Page/跳舞遊戲.md" title="wikilink">跳舞遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/鈊象電子.md" title="wikilink">鈊象電子</a></p></td>
<td><p><a href="../Page/鈊象電子.md" title="wikilink">鈊象電子</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三國策.md" title="wikilink">三國策Online</a></p></td>
<td></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/皓宇科技.md" title="wikilink">皓宇科技</a></p></td>
<td><p><a href="../Page/皓宇科技.md" title="wikilink">皓宇科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大富翁_Online.md" title="wikilink">大富翁 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/大宇資訊.md" title="wikilink">大宇資訊</a></p></td>
<td><p><a href="../Page/大宇資訊.md" title="wikilink">大宇資訊</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/O2勁樂團.md" title="wikilink">O2勁樂團</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/椰子罐頭Online.md" title="wikilink">椰子罐頭Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/富貴達人_Online.md" title="wikilink">富貴達人 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寶島俱樂部.md" title="wikilink">寶島俱樂部</a></p></td>
<td></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/華義國際.md" title="wikilink">華義國際</a></p></td>
<td><p><a href="../Page/華義國際.md" title="wikilink">華義國際</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瘋狂西遊_Online.md" title="wikilink">瘋狂西遊 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲新幹線.md" title="wikilink">遊戲新幹線</a></p></td>
<td><p><a href="../Page/智冠科技.md" title="wikilink">智冠科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中華職棒_Internet_game.md" title="wikilink">中華職棒 Internet game</a></p></td>
<td><p><a href="../Page/運動遊戲.md" title="wikilink">運動遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲新幹線.md" title="wikilink">遊戲新幹線</a></p></td>
<td><p><a href="../Page/東方演算.md" title="wikilink">東方演算</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/勇者泡泡龍_Online.md" title="wikilink">勇者泡泡龍 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/龍愛科技.md" title="wikilink">龍愛科技</a></p></td>
<td><p><a href="../Page/龍愛科技.md" title="wikilink">龍愛科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/勇者泡泡龍2_Online.md" title="wikilink">勇者泡泡龍2 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/龍愛科技.md" title="wikilink">龍愛科技</a>&amp;<a href="../Page/戲谷.md" title="wikilink">戲谷</a></p></td>
<td><p><a href="../Page/龍愛科技.md" title="wikilink">龍愛科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/衝鋒賽車.md" title="wikilink">衝鋒賽車</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瘋狂賽車_online.md" title="wikilink">瘋狂賽車 online</a></p></td>
<td><p><a href="../Page/競速遊戲.md" title="wikilink">競速遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/奧華科技.md" title="wikilink">玩到瘋</a></p></td>
<td><p><a href="../Page/奧華科技.md" title="wikilink">奧華科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/撞球風雲.md" title="wikilink">撞球風雲</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/天宇科技.md" title="wikilink">天宇科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/熱舞_Online.md" title="wikilink">熱舞 Online</a></p></td>
<td><p><a href="../Page/跳舞遊戲.md" title="wikilink">跳舞遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/易吉網.md" title="wikilink">易吉網</a></p></td>
<td><p><a href="../Page/久游網.md" title="wikilink">久游網</a></p></td>
<td><p><a href="../Page/中國大陸.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小鬥士大冒險Online.md" title="wikilink">小鬥士大冒險Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/燒雞數碼.md" title="wikilink">燒雞數碼</a></p></td>
<td><p><a href="../Page/越一科技.md" title="wikilink">越一科技</a></p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/龍虎門_Online.md" title="wikilink">龍虎門 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/魔碼奧義.md" title="wikilink">魔碼奧義</a></p></td>
<td><p><a href="../Page/中國大陸.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東方牌王Online.md" title="wikilink">東方牌王Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/因思銳.md" title="wikilink">因思銳</a></p></td>
<td><p><a href="../Page/瀚天科技.md" title="wikilink">瀚天科技</a></p></td>
<td><p><a href="../Page/中國大陸.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/三國爆爆堂.md" title="wikilink">三國爆爆堂</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/驊美資訊.md" title="wikilink">驊美資訊</a></p></td>
<td><p><a href="../Page/武漢銳游.md" title="wikilink">武漢銳游</a></p></td>
<td><p><a href="../Page/中國大陸.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/百變恰吉.md" title="wikilink">百變恰吉</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/聚樂亞洲.md" title="wikilink">聚樂亞洲</a></p></td>
<td><p><a href="../Page/Cyber_Step.md" title="wikilink">Cyber Step</a></p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/致命衝突_online(STING_online).md" title="wikilink">致命衝突 online(STING online)</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/YNK_Taiwan.md" title="wikilink">YNK Taiwan</a></p></td>
<td><p><a href="../Page/YNK_Korea.md" title="wikilink">YNK Korea</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賽灰機.md" title="wikilink">賽灰機</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/YNK_Taiwan.md" title="wikilink">YNK Taiwan</a></p></td>
<td><p><a href="../Page/YNK_Korea.md" title="wikilink">YNK Korea</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/終極_Online.md" title="wikilink">終極 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲新幹線.md" title="wikilink">遊戲新幹線</a></p></td>
<td><p><a href="../Page/智冠科技.md" title="wikilink">智冠科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紅不讓Online.md" title="wikilink">紅不讓Online</a>（Slugger）</p></td>
<td><p><a href="../Page/運動遊戲.md" title="wikilink">運動遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/NEOWIZ.md" title="wikilink">NEOWIZ</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鋼彈_Online.md" title="wikilink">鋼彈 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神影特攻.md" title="wikilink">神影特攻</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SOS_校園救難隊.md" title="wikilink">SOS 校園救難隊</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/首都高Online.md" title="wikilink">首都高Online</a></p></td>
<td><p><a href="../Page/競速遊戲.md" title="wikilink">競速遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魔法棒球.md" title="wikilink">魔法棒球</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/向擎科技.md" title="wikilink">向擎科技</a></p></td>
<td><p><a href="../Page/向擎科技.md" title="wikilink">向擎科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/AVA戰地之王Online.md" title="wikilink">AVA戰地之王Online</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/戰谷.md" title="wikilink">Garena台灣競舞</a></p></td>
<td></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尬舞Online.md" title="wikilink">尬舞Online</a></p></td>
<td><p><a href="../Page/跳舞遊戲.md" title="wikilink">跳舞遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲新幹線.md" title="wikilink">遊戲新幹線</a></p></td>
<td><p><a href="../Page/T3.md" title="wikilink">T3</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/SD_鋼彈_Online.md" title="wikilink">SD 鋼彈 Online</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/紅心辣椒.md" title="wikilink">紅心辣椒</a></p></td>
<td><p><a href="../Page/Softmax_(游戏开发商).md" title="wikilink">Softmax</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛的爆爆.md" title="wikilink">愛的爆爆</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/因思銳.md" title="wikilink">因思銳</a></p></td>
<td><p><a href="../Page/SK_Imedia.md" title="wikilink">SK Imedia</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/終極棒球Live.md" title="wikilink">終極棒球Live</a></p></td>
<td><p><a href="../Page/運動遊戲.md" title="wikilink">運動遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/動網數位.md" title="wikilink">動網數位</a></p></td>
<td><p><a href="../Page/Netamin.md" title="wikilink">Netamin</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彩虹冒險.md" title="wikilink">彩虹冒險</a></p></td>
<td></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/猿人在線.md" title="wikilink">猿人在線</a></p></td>
<td><p><a href="../Page/Hanbit_Soft.md" title="wikilink">Hanbit Soft</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第五大街.md" title="wikilink">第五大街</a></p></td>
<td><p><a href="../Page/跳舞遊戲.md" title="wikilink">跳舞遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/夢世紀數位娛樂.md" title="wikilink">夢世紀數位娛樂</a></p></td>
<td><p><a href="../Page/遊戲蝸牛.md" title="wikilink">遊戲蝸牛</a></p></td>
<td><p><a href="../Page/中國大陸.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/舞嘻哈.md" title="wikilink">舞嘻哈</a></p></td>
<td><p><a href="../Page/跳舞遊戲.md" title="wikilink">跳舞遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/易吉網.md" title="wikilink">易吉網</a></p></td>
<td><p><a href="../Page/ZNE_Entertainment.md" title="wikilink">ZNE Entertainment</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/極速快車手.md" title="wikilink">極速快車手</a></p></td>
<td><p><a href="../Page/競速遊戲.md" title="wikilink">競速遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/松崗科技.md" title="wikilink">松崗科技</a></p></td>
<td><p><a href="../Page/Npluto.md" title="wikilink">Npluto</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/籃球火_Online.md" title="wikilink">籃球火 Online</a></p></td>
<td><p><a href="../Page/運動遊戲.md" title="wikilink">運動遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲新幹線.md" title="wikilink">遊戲新幹線</a></p></td>
<td><p><a href="../Page/智冠科技.md" title="wikilink">智冠科技</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/勁舞團_2.md" title="wikilink">勁舞團 2</a></p></td>
<td><p><a href="../Page/跳舞遊戲.md" title="wikilink">跳舞遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/因思銳.md" title="wikilink">因思銳</a></p></td>
<td><p><a href="../Page/因思銳.md" title="wikilink">因思銳</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/光速城市.md" title="wikilink">光速城市</a></p></td>
<td><p><a href="../Page/競速遊戲.md" title="wikilink">競速遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/億泰利.md" title="wikilink">億泰利</a></p></td>
<td><p><a href="../Page/EA-J2M_Soft.md" title="wikilink">EA-J2M Soft</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夢三國.md" title="wikilink">夢三國</a></p></td>
<td><p><a href="../Page/策略模擬遊戲.md" title="wikilink">策略模擬遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/競舞.md" title="wikilink">競舞</a></p></td>
<td><p><a href="../Page/電魂科技.md" title="wikilink">電魂科技</a></p></td>
<td><p><a href="../Page/中國.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/遊戲王_Online.md" title="wikilink">遊戲王 Online</a></p></td>
<td><p><a href="../Page/益智遊戲.md" title="wikilink">益智遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/KONAMI.md" title="wikilink">KONAMI</a></p></td>
<td></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/幸福五角_Online.md" title="wikilink">幸福五角 Online</a></p></td>
<td><p><a href="../Page/策略模擬遊戲.md" title="wikilink">策略模擬遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/中華網龍.md" title="wikilink">中華網龍</a></p></td>
<td><p><a href="../Page/中華網龍.md" title="wikilink">中華網龍</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/烈日風暴_Online.md" title="wikilink">烈日風暴 Online</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td><p><a href="../Page/目標軟體.md" title="wikilink">目標軟體</a></p></td>
<td><p><a href="../Page/中國大陸.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鑽石俱樂部_Online.md" title="wikilink">鑽石俱樂部 Online</a></p></td>
<td><p><a href="../Page/益智遊戲.md" title="wikilink">益智遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/中華網龍.md" title="wikilink">中華網龍</a></p></td>
<td><p><a href="../Page/中華網龍.md" title="wikilink">中華網龍</a></p></td>
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/R_進化_Online.md" title="wikilink">R 進化 Online</a></p></td>
<td><p><a href="../Page/競速遊戲.md" title="wikilink">競速遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/松崗科技.md" title="wikilink">松崗科技</a></p></td>
<td><p><a href="../Page/Gamepot-Invictus_Games.md" title="wikilink">Gamepot-Invictus Games</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/穿越火線.md" title="wikilink">穿越火線</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/Me2_咪兔數位科技.md" title="wikilink">Me2 咪兔數位科技</a></p></td>
<td><p><a href="../Page/SmileGate.md" title="wikilink">SmileGate</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英雄聯盟.md" title="wikilink">英雄聯盟</a></p></td>
<td><p><a href="../Page/策略模擬.md" title="wikilink">策略模擬</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/Garena.md" title="wikilink">Garena 台灣競舞娛樂</a></p></td>
<td><p><a href="../Page/Riot_Games_DotA_All_Stars.md" title="wikilink">Riot Games DotA All Stars</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熱力排球_Online.md" title="wikilink">熱力排球 Online</a></p></td>
<td><p><a href="../Page/運動遊戲.md" title="wikilink">運動遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/京群超媒體.md" title="wikilink">京群超媒體</a></p></td>
<td><p><a href="../Page/光輝軟件.md" title="wikilink">光輝軟件</a></p></td>
<td><p><a href="../Page/中國大陸.md" title="wikilink">中國</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/自由之心.md" title="wikilink">自由之心</a></p></td>
<td><p><a href="../Page/第一人稱射擊遊戲.md" title="wikilink">第一人稱射擊遊戲</a></p></td>
<td><p>結束營運</p></td>
<td><p><a href="../Page/華義國際.md" title="wikilink">華義國際</a></p></td>
<td><p><a href="../Page/Zepetto.md" title="wikilink">Zepetto</a></p></td>
<td><p><a href="../Page/南韓.md" title="wikilink">韓國</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Mstar.md" title="wikilink">Mstar</a></p></td>
<td><p><a href="../Page/跳舞遊戲.md" title="wikilink">跳舞遊戲</a></p></td>
<td><p>營運中</p></td>
<td><p><a href="../Page/Garena.md" title="wikilink">Garena 台灣競舞娛樂</a></p></td>
<td><p><a href="../Page/nurien.md" title="wikilink">nurien</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
</tr>
</tbody>
</table>

## 相關條目

  - [線上遊戲](../Page/線上遊戲.md "wikilink")
  - [角色扮演線上遊戲列表](../Page/角色扮演線上遊戲列表.md "wikilink")

[Category:電子遊戲列表](../Category/電子遊戲列表.md "wikilink")
[Category:網路遊戲](../Category/網路遊戲.md "wikilink")