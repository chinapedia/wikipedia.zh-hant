| 行政区的位置                                                                                                                                                                                                                |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Lage_des_Regierungsbezirkes_Leipzig_in_Deutschland.GIF](https://zh.wikipedia.org/wiki/File:Lage_des_Regierungsbezirkes_Leipzig_in_Deutschland.GIF "fig:Lage_des_Regierungsbezirkes_Leipzig_in_Deutschland.GIF") |
| 基本数据                                                                                                                                                                                                                  |
| 所属联邦州:                                                                                                                                                                                                                |
| 首府:                                                                                                                                                                                                                   |
| 面积:                                                                                                                                                                                                                   |
| 人口:                                                                                                                                                                                                                   |
| 人口密度:                                                                                                                                                                                                                 |
| 下设行政区划:                                                                                                                                                                                                               |
| 政府网站:                                                                                                                                                                                                                 |
| 行政区地图                                                                                                                                                                                                                 |
| [Sachsen_rbleipzig.png](https://zh.wikipedia.org/wiki/File:Sachsen_rbleipzig.png "fig:Sachsen_rbleipzig.png")                                                                                                        |

**莱比锡行政区**（Regierungsbezirk
Leipzig）是[德国](../Page/德国.md "wikilink")[萨克森州的](../Page/萨克森州.md "wikilink")3个[行政区之一](../Page/行政区_\(德国\).md "wikilink")，位于该州的西北部，首府[莱比锡](../Page/莱比锡.md "wikilink")。

## 行政区划

莱比锡行政区下设5个县：

1.  [德利奇县](../Page/德利奇县.md "wikilink")（Landkreis
    Delitzsch），首府[德利奇](../Page/德利奇.md "wikilink")（Delitzsch）
2.  [德贝尔恩县](../Page/德贝尔恩县.md "wikilink")（Landkreis
    Döbeln），首府[德贝尔恩](../Page/德贝尔恩.md "wikilink")（Döbeln）
3.  [莱比锡县](../Page/莱比锡县.md "wikilink")（Landkreis Leipziger
    Land），首府[博尔纳](../Page/博尔纳.md "wikilink")（Borna）
4.  [穆尔登塔尔县](../Page/穆尔登塔尔县.md "wikilink")（Muldentalkreis），首府[格里马](../Page/格里马.md "wikilink")（Grimma）
5.  [托尔高-奥沙茨县](../Page/托尔高-奥沙茨县.md "wikilink")（Landkreis
    Torgau-Oschatz），首府[托尔高](../Page/托尔高.md "wikilink")（Torgau）

莱比锡行政区下设1个直辖市：

1.  [莱比锡市](../Page/莱比锡.md "wikilink")（Leipzig）

[萨克森州](../Category/德国行政区.md "wikilink")
[行政区](../Category/萨克森州行政区划.md "wikilink")