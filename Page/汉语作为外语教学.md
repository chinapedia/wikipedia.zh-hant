**汉语作为外语教学**（Teaching Chinese as a Second/Foreign
Language），台灣稱「華語作為第二語言教學」，也称**对外汉语教学**，內容指的是對[華語非](../Page/現代標準漢語.md "wikilink")[母語或](../Page/母語.md "wikilink")[第一語言的對象所進行的語文教學](../Page/第一語言.md "wikilink")，明確地說是「以華語文做為[第二語言或](../Page/第二語言.md "wikilink")[第二語言教學的](../Page/第二語言教學.md "wikilink")[語言教育](../Page/語言教育.md "wikilink")」。\[1\]

由於近年來受到[亞洲經濟崛起以及近](../Page/亞洲.md "wikilink")20年[中國快速發展的影響](../Page/中國.md "wikilink")，國際流行中國熱的同時也帶動了華語熱，使得華語學習及使用群體不再僅限於[華人](../Page/華人.md "wikilink")，擴大影響了非華人族群的華語學習熱潮\[2\]。早期，**對外漢語教學**關注的對象，多集中在歐美等西方國家與地區。隨著政策的方向（如[一帶一路和](../Page/一帶一路.md "wikilink")[新南向政策](../Page/新南向政策.md "wikilink")\[3\]\[4\]\[5\]），也開始擴及阿拉伯、拉美、非洲及東南亞地區。
對外華語文教學的對象可分為三類，其教學內容與方式也有所差異，對象分別是：

1.  非[華裔的純外籍人士](../Page/華裔.md "wikilink")
2.  海外華裔、華僑
3.  來華[新住民](../Page/新住民.md "wikilink")

## 名称与特点

在大陸，[吕必松认为应该采用](../Page/吕必松.md "wikilink")“对外汉语教学”作为学科名称，他主张无论“对外”还是“对内”，其专业宗旨都是与教学挂钩的。[潘文国则认为](../Page/潘文国.md "wikilink")“对外汉语”这个名称体现出了其与“对内汉语”的区别，因为两者的教学性质、目标、方法手段都有所不同。[周健认为在学术上使用](../Page/周健.md "wikilink")“对外汉语语言学”这个名称更能体现其和[语言学的关系以及学科本身的研究价值](../Page/语言学.md "wikilink")。总之，对外汉语教学的特点应该是向外族人提供组词达意的合法条件，培养他们的汉语交际能力，主要是实践性的。有人主张使用“汉语作为第二语言教学”。这个名称虽然绕开了“外”字的二义性问题，但又过于繁琐。最终，[中国国家汉办采用](../Page/国家汉办.md "wikilink")“汉语作为外语教学”作为该学科的正式称谓，但在各高校设置的相关专业名称仍沿用“对外汉语”。

台灣在一九五六年秋季，由國立[台灣師範大學國語中心最早開始進行對外漢語工作](../Page/台灣師範大學.md "wikilink")\[6\]，並於一九九五年成立華語文教學研究所，以培養學生具備跨領域的研究知能以及量化研究的方法，尤強調與[第二語言習得和之相關領域](../Page/第二語言習得.md "wikilink")，如心理學、認知語言等；並且培養學生華語文作為[第二語言教學知能與實務性教學訓練](../Page/第二語言教學.md "wikilink")，如多媒體教學、線上教學、行動研究、逆向教學設計
(backward design)
以及教學法實驗等\[7\]。各大學相關系所中文名稱包括：華語文教學系、應用華語文系、國際華語文系等，雖沒包含「對外」和「第二語言」等詞語，但實質上是有「對外」和「第二語言」的含義。

## 歷史

歷史上，對外教授漢語並不是一直像現代這麼開放。例如[清朝](../Page/清朝.md "wikilink")[乾隆年間不允許教授外國人華語](../Page/乾隆.md "wikilink")。東印度公司一個叫洪仁輝的[英國人](../Page/英國.md "wikilink")，因不滿地方官員勒索告御狀，結果被判刑三年，罪名是擅自學習漢字漢語。兩廣總督李侍堯在奏文中將事件原因歸結：
「細察根源，縱由內地奸民教唆引誘，行商通事不加管束稽查所致。查夷人遠處海外，本與中國語言不同，向之來廣貿販，唯藉諳曉夷語之行商通事為之交易，近如夷商洪仁輝於內地土音官話，無不通曉，甚至漢文字義，亦能明晰，此夷商中如洪仁輝之通曉語文言義者，亦尚有數人，設非漢奸潛滋教誘，何能熟悉？如奸民劉亞扁始則教授夷人讀書，圖謀財物，繼則主謀唆訟，代作控辭，由此類推，將無在不可以勾結教誘，實於地方大有關系。」\[8\]。

直到近代，[周祖謨](../Page/周祖謨.md "wikilink")1952年《[中國語文](../Page/中國語文.md "wikilink")》第7期刊載《教非漢族學生學習漢語的一些問題》，是中國歷史上對外漢語教學的第一篇論文，才開始重視對外漢語。对外汉语专业的宗旨是培养面向[母语非](../Page/母语.md "wikilink")[汉语](../Page/汉语.md "wikilink")（即把汉语作为外语）的人进行汉语教学的教师以及相关的研究人员。目前中華人民共和國的对外汉语教学工作以及[HSK考试和汉语作为外语教学能力考试等由](../Page/HSK.md "wikilink")[国家汉语国际推广领导小组办公室](../Page/国家汉语国际推广领导小组办公室.md "wikilink")（简称“国家汉办”）领导，开设对外汉语本科专业的高等学校已经超过60所。2006年，中華人民共和國正式认定对外汉语专业为二级学科，与[语言学和](../Page/语言学.md "wikilink")[应用语言学平级](../Page/应用语言学.md "wikilink")，并设置该专业硕士学位。

## 专业与课程设置

中国大陆地区第一批对外汉语本科专业于1985年由[国家教委批准在四所高等学校设立](../Page/中华人民共和国国家教育委员会.md "wikilink")，它们是：

  - [北京外国语学院](../Page/北京外国语学院.md "wikilink")（现：[北京外国语大学](../Page/北京外国语大学.md "wikilink")）
  - [北京语言学院](../Page/北京语言学院.md "wikilink")（现：[北京语言大学](../Page/北京语言大学.md "wikilink")）
  - [上海外国语学院](../Page/上海外国语学院.md "wikilink")（现：[上海外国语大学](../Page/上海外国语大学.md "wikilink")）
  - [华东师范大学](../Page/华东师范大学.md "wikilink")

而臺灣最早則是由[國立臺灣師範大學](../Page/國立臺灣師範大學.md "wikilink")（現任系主任由[國際與僑教學院院長](../Page/國際與僑教學院.md "wikilink")[陳振宇擔任](../Page/陳振宇.md "wikilink")）開設相關課程。

## 參考文獻

## 外部链接

  - [国家汉语国际推广领导小组办公室](http://www.hanban.edu.cn/)

  - [台灣華語文教學學會](https://web.archive.org/web/20140222064409/http://www.tcsl.ntnu.edu.tw/atcsl/?q=taxonomy%2Fterm%2F8)（Association
    of Teaching Chinese as a Second Language）

  - Moser, David (1991) “[Why Chinese is So Damn
    Hard](http://pinyin.info/readings/texts/moser.html)"
    ([Archive](http://www.webcitation.org/6WQVCTd2J)). In: Mair, Victor
    H. ([梅維恆](../Page/梅維恆.md "wikilink")) (ed.), [*Schriftfestschrift :
    Essays on Writing and Language in Honor of John DeFrancis on his
    Eightieth Birthday*. Sino-Platonic Papers No.
    27](http://sino-platonic.org/complete/spp027_john_defrancis.pdf)
    ([Archive](http://www.webcitation.org/6WQWnunR6))
    ([宾夕法尼亚大学](../Page/宾夕法尼亚大学.md "wikilink")).
    1991年8月31日. p. 59-70 (PDF document p. 71-82/260).

      - Jing Huang的中文翻译:
        [简体字](http://pinyin.info/readings/texts/moser_zhongwen_simplified.html)
        ([Archive](http://www.webcitation.org/6WQVHpPH3)),
        [繁体字](http://pinyin.info/readings/texts/moser_zhongwen_traditional.html)
        ([Archive](http://www.webcitation.org/6WQVNSIpy))

[Category:语言学](../Category/语言学.md "wikilink")
[Category:漢語教學](../Category/漢語教學.md "wikilink")

1.  \#
2.  漢語熱遍世界各地 當名漢語教師，你準備好了嗎
    <http://news.xinhuanet.com/abroad/2012-04/23/c_123018905.htm>
3.
4.
5.
6.
7.
8.