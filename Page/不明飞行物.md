[PurportedUFO2.jpg](https://zh.wikipedia.org/wiki/File:PurportedUFO2.jpg "fig:PurportedUFO2.jpg")[新澤西州所拍攝的不明飛行物](../Page/新澤西州.md "wikilink")\]\]

**不明飛行物**（**體**）或稱**未確認飛行物**（**體**）（[英文](../Page/英文.md "wikilink")：**U**nidentified
**F**lying **O**bject
[縮寫](../Page/縮寫.md "wikilink")：**UFO**），是指不明來歷、不明性質，漂浮及[飛行在](../Page/飛行.md "wikilink")[天空的物體](../Page/天空.md "wikilink")。意指是只要在觀察者眼中看不清或無法辨認的不詳物體都稱為UFO。

很多人將UFO視為等同於[高科技或](../Page/高科技.md "wikilink")[外星文明的](../Page/外星文明.md "wikilink")**飛碟**、**飛盤**（），香港稱為**飛碟**、台灣稱為**幽浮**。

一般人相信它是來自其他[行星的太空船或者未來的人來今日](../Page/行星.md "wikilink")[地球做研究所操控的](../Page/地球.md "wikilink")[時光機](../Page/時間旅行.md "wikilink")，一些人則認為是[大氣現象](../Page/大氣現象.md "wikilink")\[1\]，還有一些人則認為是來自地球本身的人造軍事飛碟，甚至純粹的[惡作劇等](../Page/惡作劇.md "wikilink")。許多不明飛行物照片經過專家鑑定為騙局或者誤會，但是始終有部分發現根據現存科學知識無法解釋，例如[鳳凰城光點](../Page/鳳凰城光點.md "wikilink")\[2\]及[華盛頓不明飛行物事件等](../Page/華盛頓不明飛行物事件.md "wikilink")。

在不明飛行物一詞出現以前，英文中只有**飛碟**一詞稱呼，但是經常造成誤解。20世紀開始，美國上空發現碟狀飛行物，當時稱為飛碟，以為是[蘇聯新式偵察武器](../Page/蘇聯.md "wikilink")。這是當代對不明飛行物的興趣的開端，後來人們著眼於世界各地的不明飛行物報告。

## 歷史

[Nuremberg_Apr_14_1561.jpg](https://zh.wikipedia.org/wiki/File:Nuremberg_Apr_14_1561.jpg "fig:Nuremberg_Apr_14_1561.jpg")的木刻版畫\]\]
[Hopeh_incident_1.jpeg](https://zh.wikipedia.org/wiki/File:Hopeh_incident_1.jpeg "fig:Hopeh_incident_1.jpeg")[河北目擊事件](../Page/河北目擊事件.md "wikilink")\]\]
[Ufo-brazil.jpg](https://zh.wikipedia.org/wiki/File:Ufo-brazil.jpg "fig:Ufo-brazil.jpg")拍攝到的不明飛行物\]\]

幽浮並非近代才出現的現象。北宋[蘇軾可能目擊過幽浮](../Page/蘇軾.md "wikilink")。他在《[遊金山寺](../Page/遊金山寺.md "wikilink")》詩中寫下當時奇遇：“二更月落天深黑。江心似有炬火明，飛焰照山棲鳥驚。悵然歸臥心莫識，非鬼非人竟何物？”亦有解釋「炬火明」是江中能發光的水生動物\[3\]。同時期的北宋朝科学家[沈括也在](../Page/沈括.md "wikilink")《[梦溪笔谈](../Page/梦溪笔谈.md "wikilink")》卷21中记载当时[天长县陂澤中的不明飞行物](../Page/天长县.md "wikilink")：“嘉祐中，揚州有一珠，甚大，天晦多見。初出於天長縣陂澤中，後轉入甓社湖，又後乃在新開湖中。”南宋[洪迈](../Page/洪迈.md "wikilink")《[夷坚志](../Page/夷坚志.md "wikilink")》一書亦有
UFO
的現象記載，如甲卷第十九〈晦日月光篇〉：「趙清憲賜第在京師府司巷，長女適史氏，以暑月不寐，啟戶納涼，見月滿中庭如晝，方歎曰：「大好月色。」俄廷，下漸暗，月痕稍稍縮小，斯須光滅，仰視，星斗粲然。而是夕乃晦日，竟不曉為何物光也。」辛卷第八〈星月之異篇〉：「乾道丁亥八月十五夜，天陰月昏，……仰頭而視，一輪如半月闊，散而為細星，百千萬顆，霄漢間翠碧霞采，光燦逼人，不可形容，……頃之，雲復環合，晦昧如初。」

《[資治通鑑](../Page/資治通鑑.md "wikilink")》是公認的正史，也曾記載疑似幽浮出沒的現象。例如《漢紀》記載：「四月戊申，有日夜出
」，「有日夜出」即：有看似太陽的物體在夜間出沒\[4\]，以科學角度來看可能是[超新星爆发或](../Page/超新星.md "wikilink")[海市蜃楼](../Page/海市蜃楼.md "wikilink")。\[5\]

近代最早出現不明飛行物的時間是1878年1月，一名美國農民在田裡耕種時，突然發現空中出現一個不明圓形物體。許多人也看見，這則新聞很快就刊載150家美國報紙上。

1947年6月24日，[美國人](../Page/美國人.md "wikilink")[肯尼士·阿諾德在](../Page/肯尼士·阿諾德.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[雷尼爾山上空驾驶着自用](../Page/雷尼爾山.md "wikilink")[飛機](../Page/飛機.md "wikilink")，突然發現有九個白色碟狀的不明飛行物體，根據他的目測，這些物體以約每小時1600或1900公里高速飛過，並轉眼消失。他向地面塔台喊出：（我看見了飛舞的碟子。）引起[美國極大的轟動](../Page/美國.md "wikilink")。由於飛碟這個名詞形容得很貼切，於是就在世界各地廣泛流傳。其後一名記者在報紙上首次使用了UFO這個縮寫，即不明飛行物，被人們一直沿用至今。

1952年7月19日晚上，[美國](../Page/美國.md "wikilink")[華盛頓上空多次出現不明飛行物](../Page/华盛顿哥伦比亚特区.md "wikilink")，美軍戰鬥機想擊落它們，卻以遠超過戰鬥機的速度移動並集體消失，為「[華盛頓不明飛行物事件](../Page/華盛頓不明飛行物事件.md "wikilink")」。

1980年12月24日聖誕夜英國蘇福克的美國空軍基地發生倫道森森林事件，在聖誕夜巡邏的兩位空軍衛兵發現附近森林出現奇怪亮光，他們前去查看時發現有一架三角形金屬物體停在森林裡，然後就朝空中飛去，他們回報後指揮部詢問[雷達站](../Page/雷達.md "wikilink")，是否看到發光物體飛過，雷達站人員事後對採訪鏡頭說當地雷達沒看到，但他改用英國最強的雷達掃描後就發現了有不明物體在空中，掃瞄兩次後都一樣。兩天後夜晚衛兵又看到森林裡有亮光，這時他們通知基地副指揮官何特中校，中校帶二十多人和他們一起去森林裡查看，結果看到空中有不明紅色光線，靠近後又有黃色和彩色光線，持續數分鐘後才消失，對於這是否為海邊[燈塔光線的質疑](../Page/燈塔.md "wikilink")，何特中校表示他知道那燈塔的位置，也知道那燈塔下面有個餐廳他還帶人去吃過飯，他們看到的光並非來自燈塔方向，對這事何特中校當時帶錄音機現場錄音紀錄看到的現象，並提交正式報告給軍方。

1989年底至1990年間，[比利時上空多次出現不明三角形飛行物](../Page/比利時.md "wikilink")，是少數擁有超過一千多名目擊者的不明飛行物體事件。當時不止一般[民眾及](../Page/群眾.md "wikilink")[警察目擊](../Page/警察.md "wikilink")，比利時軍方以及[北大西洋公約組織的雷達也偵測到這些不明飛行物體的存在](../Page/北大西洋公約組織.md "wikilink")，在當試以無線電聯絡失敗以後，比利時空軍多次派出[F-16](../Page/F-16.md "wikilink")[戰鬥機攔截](../Page/戰鬥機.md "wikilink")，其間F-16曾成功以機上雷達描定其中一架不明飛行物體，但是被其以極高速逃脫。在經過一個多小時追逐後，無功而返。事後比利時軍方釋出事件報告，史稱「[比利時不明飛行物體事件](../Page/比利時不明飛行物體事件.md "wikilink")」，這也是極少數獲得國家軍方承認的不明飛行物體事件。\[6\]\[7\]

1997年美國亞歷桑那州出現大型V字型幽浮為「[鳳凰城光點](../Page/鳳凰城光點.md "wikilink")」，飛行了美國幾個州，有數千位目擊者，並拍下部分影片，美國國家地理頻道在幾年後曾找大學教授、航管人員、影像專家研究此事件，排除這些[光點是軍方的](../Page/光點.md "wikilink")[照明彈](../Page/照明彈.md "wikilink")，而且也不是[人類現有的科技能製作的](../Page/人類.md "wikilink")[飛行器](../Page/飛行器.md "wikilink")。\[8\]

[二十一世紀解密的](../Page/二十一世紀.md "wikilink")[英國政府檔案中也有幽浮檔案](../Page/英國.md "wikilink")，其中一份解密檔案的飛行員在解密後接受[紀錄片訪問時指出](../Page/紀錄片.md "wikilink")1957年他的戰機雷達顯示空中有大型物體，體積幾乎等於海上的[航空母艦](../Page/航空母艦.md "wikilink")，他的長官要他追逐這幽浮，但這幽浮卻高速飛離，飛行速度將近音速十倍，十分驚人。

## [納粹飛碟](../Page/納粹飛碟.md "wikilink")

[Naziufo.png](https://zh.wikipedia.org/wiki/File:Naziufo.png "fig:Naziufo.png")
傳言西元1941年至1943年期間，納粹在[布拉格建立了飛行試驗基地](../Page/布拉格.md "wikilink")，並且在[特斯拉的帮助下反重力技術方面獲得重大突破](../Page/尼古拉·特斯拉.md "wikilink")。當時擔任該計劃設計師的是德國著名航太工程師奧托·哈本默霍，試飛員則是納粹王牌飛行員兼工程師魯道夫·斯利埃弗。有傳言稱，1944年2月，納粹德國製造的飛碟首次飛行即達到2000[公里](../Page/公里.md "wikilink")/小時的速度。

而在蘇俄紅軍攻入布拉格後，納粹科學家緊急銷燬當時眾多的原型、藍圖、樣本，根據傳言，這些頂尖的科學家在戰後被帶往美國從事秘密武器的研發。

但也有人認為，以當時的技術造出來的UFO，或許只是一堆沒用的垃圾。

### 傳聞型號

  - Sack A.S.6：配備Argus 10cc
    140匹馬力引擎，6.40公尺厚的圓形機翼，重量大約為750到800公斤，靠一具螺旋槳產生升力
  - 飛行陀螺
  - V-7火球式飛行器
  - 霍克飛行翼

## UFO的調查

目擊者描述飛碟的種類很多。其中有許多UFO形狀類似一隻發光的管子或碟子，飛行速度甚高，不是寧靜無息，便是有一種嘶嘶聲。它的出現會讓動物驚慌，使[無線電](../Page/無線電.md "wikilink")（[收音機等](../Page/收音機.md "wikilink")）產生[電干擾](../Page/電.md "wikilink")。有時且會登陸在地面留下痕跡。

由於1947年的[羅斯威爾飛碟墜毀事件發生於第二次世界大戰發生後兩年](../Page/羅斯威爾飛碟墜毀事件.md "wikilink")，有人認為UFO是受到[核子武器的](../Page/核子武器.md "wikilink")[放射線影響而墜毀](../Page/放射線.md "wikilink")。
[Nuvola_apps_konquest.svg](https://zh.wikipedia.org/wiki/File:Nuvola_apps_konquest.svg "fig:Nuvola_apps_konquest.svg")
近幾百年來，人們常報告看到天空中有神秘的物體。[二次世界大戰期間](../Page/二次世界大戰.md "wikilink")（1939 -
45），這類報告大增。許多軍方和民間的飛行員聲稱目擊奇怪且會移動的光。他們叫它做「[火焰戰鬥機](../Page/火焰戰鬥機.md "wikilink")」（Foo-Fighter）。至於別的報告（其中有的被叫做飛碟）發生在上世紀中期的美國及其他國家。這些報告許多是來自可靠的觀察家，有的人曾拍下看到的東西。

對於大多數的UFO報告，科學家已提供了合理的解釋。例如，在許多例子中，報告的UFO事後被認明是一顆[流星](../Page/流星.md "wikilink")、一顆[行星](../Page/行星.md "wikilink")、一個[火箭](../Page/火箭.md "wikilink")、一顆[人造衛星或是一個氣球](../Page/人造衛星.md "wikilink")，[飛機或其排出之尾跡](../Page/飛機.md "wikilink")，在異常照射情況下被人看了，也會當成UFO報告。此外，[大氣層狀態也會產生眼睛上的錯覺](../Page/大氣層.md "wikilink")，被誤認為UFO。但仍有5%未能確明是何種物體。

自1966年到1968年，[美國空軍發起了一項UFO研究工作](../Page/美國空軍.md "wikilink")，由科羅拉多大學的科學家來進行。科學家們無法解釋所有的UFO報告，可是證明不出UFO是來自別的行星。空軍也調查了發生於1947年到1969年的12610件報告。調查終了，結論是UFO對國家安全沒有威脅。

飛碟權威─[約瑟夫·艾倫·海尼克](../Page/約瑟夫·艾倫·海尼克.md "wikilink")（Allen
Hynek）博士，曾任白宮委員會幽浮聽證會與聯合國幽浮相關現象會議的發言人，1948年起為美國空軍飛碟研究顧問，審查所有飛碟及相似的第一手報告，他曾說，軍方對於任何突發的不明物目擊事件，如果很難解釋的話，他們立刻封鎖消息，不讓媒體接近，儘量不要讓大眾的情緒激動，那是他們的職責所在。他主張必要慎重對待飛碟事件，因此和空軍處得並不好。

2007年到2012年间，美国国防部开展了一项名为“[先进飞行威胁辨识计划](../Page/先进飞行威胁辨识计划.md "wikilink")”(Advanced
Aerospace Threat Identification
Program)的研究计划，研究和评估不明飞行物所构成的威胁，该项目每年预算约2000万美元。项目资金大多流入了毕格罗航空航天公司(Bigelow
Aerospace)。\[9\]

一些網路上的UFO新聞，多數來自惡搞新聞的《[世界新聞周刊](../Page/世界新聞周刊.md "wikilink")》被不明就裡的中文媒體引用報導，成為網路謠言。

## 與不明飞行物的接觸的說明\[10\]與舉例

### 第一類

本條目蒐集全世界各地發生的UFO事件。

#### 說明

第一類接觸，目擊一個或多個[不明飛行物體](../Page/不明飛行物體.md "wikilink")：

  - [飛碟](../Page/飛碟.md "wikilink")
  - 奇怪光體
  - 不屬於[人類科技技術的飛行物體](../Page/人類.md "wikilink")

#### 舉例

  - [華盛頓不明飛行物事件](../Page/華盛頓不明飛行物事件.md "wikilink")
  - [通古斯大爆炸](../Page/通古斯大爆炸.md "wikilink")--1908年6月30日[上午](../Page/上午.md "wikilink")7[時17分](../Page/小时.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")
    零時17分）發生在[俄羅斯](../Page/俄羅斯.md "wikilink")[西伯利亞](../Page/西伯利亞.md "wikilink")[埃文基自治區的一次大规模的爆炸](../Page/埃文基自治區.md "wikilink")，推测是一颗[彗星或者](../Page/彗星.md "wikilink")[流星体撞击](../Page/流星.md "wikilink")。但有研究[UFO学的人认为是一次UFO爆炸](../Page/UFO学.md "wikilink")。\[11\]\[12\]
  - [尼古拉斯罗瑞克目击事件](../Page/尼古拉斯罗瑞克目击事件.md "wikilink")--尼古拉斯罗瑞克的旅行日记中提到，他们的旅行小队遭遇了一个金属的银色飞碟，从[喜马拉雅山的山脈上空掠过](../Page/喜马拉雅山.md "wikilink")。他们通过[双筒望远镜观测了一段时间](../Page/双筒望远镜.md "wikilink")，直到飞碟在山顶消失。\[13\]
  - [花地瑪事件](../Page/花地瑪預言.md "wikilink")--在[葡萄牙](../Page/葡萄牙.md "wikilink")[法蒂瑪](../Page/法蒂瑪.md "wikilink")，有成千上万的人目击到[太阳旋转](../Page/太阳.md "wikilink")，下沉。这随后被Jacques
    Vallée，Joaquim Fernandes和Fina
    d'Armada推测为UFO事件，但因文化差异被否认。\[14\]\[15\]\[16\]
  - [乔斯-波尼拉观测报告](../Page/乔斯-波尼拉观测报告.md "wikilink")--1883年8月12日，天文学家乔斯波尼拉报告称，他在[墨西哥的](../Page/墨西哥.md "wikilink")[萨卡特卡斯天文台观测](../Page/萨卡特卡斯天文台.md "wikilink")[太阳黑子的时候](../Page/太阳黑子.md "wikilink")，看到了多于300个的黑色，无法分辨的物体正在太阳前面穿越。他设法用1/100秒的湿板曝光拍摄了一些照片。但后来这些黑点被确认为高空飞行的[天鹅](../Page/天鹅.md "wikilink")。\[17\]
  - 1974年5月[母親節那天](../Page/母親節.md "wikilink")，[帕斯卡古拉事件的被綁架者希克森在瓊斯縣的一場家庭聚會中](../Page/1973年帕斯卡古拉事件.md "wikilink")，他太太看到窗戶外有一艘飛碟狀的飛行物在他們的[汽車上方約](../Page/汽車.md "wikilink")50公尺處徘徊盤旋，希克森太太看到後非常害怕並尖叫。隨後這盤旋在空中的飛碟狀飛行物一會兒就消失了。\[18\]
  - [外太空UFO目击](../Page/外太空UFO目击.md "wikilink")--在2001年的一份据称是由Jeff
    Challender记录的关于STS-102太空任务的视频资料中，出现了3个执行了启动、加速、停止，并且进行了急转弯的闪光点。随后，一位名为Lan
    Fleming的人将其运动时间和其中一个亮点的轨迹改变与飞船推进器的启动时间进行了对比，得出的结论是当时已知的推进器无法做出如此复杂的运动。\[19\]\[20\]\[21\]\[22\]
  - [2004墨西哥UFO事件](../Page/2004墨西哥UFO事件.md "wikilink")--一架在空中执行[毒品走私巡逻任务的巡逻机用红外摄像机拍下了一些据称是UFO的不明物体](../Page/毒品.md "wikilink")。这份资料由Jaime
    Maussan放出，但一些人看了以后说这些闪光点可能是附近油田排放出的烟雾。\[23\]
  - 在[帕斯卡古拉事件的前一天](../Page/1973年帕斯卡古拉事件.md "wikilink")（1973年10月10日），有15個人，包括兩名[警察報告說](../Page/警察.md "wikilink")，看到[銀色飛碟慢慢地飛越在](../Page/銀色.md "wikilink")[路易斯安那州](../Page/路易斯安那州.md "wikilink")[新奧爾良市St](../Page/新奧爾良市.md "wikilink").Tammany教區。\[24\]
  - [2011年1月28日圆顶清真寺UFO目击事件](../Page/2011年1月28日圆顶清真寺UFO目击事件.md "wikilink")

### 第二類

#### 說明

第二類接觸，目擊一個或多個[不明飛行物體](../Page/不明飛行物體.md "wikilink")，並給[目擊者及週遭環境帶來相關的](../Page/證人.md "wikilink")[物理反應](../Page/物理.md "wikilink")，其中包括：

  - [熱力或](../Page/热能.md "wikilink")[輻射](../Page/輻射.md "wikilink")
  - 地形損毀
  - [身體麻痺](../Page/身體.md "wikilink")
  - 使[動物受驚嚇](../Page/動物.md "wikilink")
  - 干擾[引擎或](../Page/引擎.md "wikilink")[電視及](../Page/電視.md "wikilink")[電台的接收](../Page/電台.md "wikilink")
  - 使目擊者失去目擊不明飛行物那段時間的[記憶](../Page/記憶.md "wikilink")\[25\]
  - [磁場強烈或](../Page/磁場.md "wikilink")[異常](../Page/異常.md "wikilink")

#### 舉例

  - [马拉开波事件](../Page/马拉开波事件.md "wikilink")--在1886年12月18日发行的科普杂志"科学美国人"，第389页描述，美国在[委内瑞拉](../Page/委内瑞拉.md "wikilink")[马拉开波当地的领事报告发现UFO](../Page/马拉开波.md "wikilink")，并伴随了嗡嗡的声音，在一场雷雨后，出现在马拉开波的附近的一个农舍。随后，农舍中的人显示出辐射中毒的状况。9天之后，农舍周围的树枯萎并死亡。\[26\]\[27\]
  - [巴西柯拉瑞斯岛神秘光束](../Page/巴西柯拉瑞斯岛神秘光束.md "wikilink")--發生在西元1977年的[巴西](../Page/巴西.md "wikilink")[柯拉瑞斯島的](../Page/柯拉瑞斯島.md "wikilink")[光束攻擊人類事件](../Page/光束.md "wikilink")，許多人被這光束灼傷。根據當事者的描述，光束是從天上照下來的，並且光線曾經令他們無法動彈，似乎在吸吮他們的[血液](../Page/血液.md "wikilink")。\[28\]

### 第三類

#### 說明

第三類接觸，目睹一個有[生命的個體](../Page/生命.md "wikilink")，其包括一不明飛行物體目擊個案。這是真正意義上的接觸、心靈感應，與外星人交談。\[29\]

#### 舉例

  - 1974年2月有次[帕斯卡古拉事件被綁架者希克森稱他感受到好像有某種信號傳到他大腦](../Page/1973年帕斯卡古拉事件.md "wikilink")，那傳來的訊息信號似乎告訴他說：（大意是：我們選擇你、不喜歡你有任何傷害，你沒有必要擔心。你們的[世界需要幫助](../Page/世界.md "wikilink")、而我們也需幫助你們……以免為時已晚。）\[30\]
  - 1994年[黑龍江省](../Page/黑龍江省.md "wikilink")[孟照國事件](../Page/孟照國事件.md "wikilink")，其稱遇到鳳凰山幽浮和外星人並發生一系列事件，後來還與一個外星人進展到第七類接觸，且他在2003年通過測謊。

### 第四類

#### 說明

第四類接觸，人類直接与UFO或外星生物接触，其方式有被[劫持](../Page/外星人綁架.md "wikilink")、被检查、被进行[实验等](../Page/实验.md "wikilink")。此種類型的外星生物接觸是不包括於海尼克原先的分類方法上。

#### 舉例

  - **[帕斯卡古拉事件](../Page/1973年帕斯卡古拉事件.md "wikilink")**42[歲的希克森和](../Page/歲.md "wikilink")18歲的派克在1973年10月11日晚上據稱遇到了外星人綁架上UFO。

## UFO種類

[1871UFO.png](https://zh.wikipedia.org/wiki/File:1871UFO.png "fig:1871UFO.png")[新罕布希爾州所拍攝的雪茄型不明飛行物](../Page/新罕布希爾州.md "wikilink")\]\]
依照所蒐集的圖片、人們所訴的外觀來區分，：

  - 雞蛋型
  - 球型
  - 碟型
  - 圓圈型
  - 雪茄型（圓柱型）
  - 茶杯型
  - 飛拐型
  - 土星型
  - 半圓型
  - 陀螺型
  - 圓頂型
  - 橢圓型
  - 鐵餅型
  - 三角型
  - V字型
  - 迴旋鏢型
  - 金字塔型

[UFO-SHAPE-Disk-with-Three-Gaps.jpg](https://zh.wikipedia.org/wiki/File:UFO-SHAPE-Disk-with-Three-Gaps.jpg "fig:UFO-SHAPE-Disk-with-Three-Gaps.jpg")

## 飛碟近照

  - 1996年2月25日，哥倫比亞太空站拍到大量UFO在太空中飛行的畫面，此片也是近代史上少見能近距離觀看飛碟外型的影片。哥倫比亞太空站拍到眾多圓盤型飛碟，在他們釋出的繩子附近徘徊，以不規則的路徑方式，在他們釋出的繩子附近徘徊或經過。\[31\]另外，瑞典Treasure
    Hunters在2011年6月18日發現了一個巨大的圓形物在波羅的海芬蘭和瑞典之間，其外型也與STS-75目擊事件中的飛碟外型有類似之處。\[32\]網友將此影片製成3D模型，判斷飛碟共有三個缺口，其中二個缺口可以開合。\[33\]

## 流行文化

### [電影](../Page/電影.md "wikilink")

  - [烏龍派出所電影版02-UFO來襲
    龍捲風大作戰](../Page/烏龍派出所電影版02-UFO來襲_龍捲風大作戰.md "wikilink")（[日本電影](../Page/日本.md "wikilink")）\[34\]
  - [宇宙戰爭（1953年美國電影）](../Page/宇宙戰爭（1953年美國電影）.md "wikilink")（1953年[美國電影](../Page/美國.md "wikilink")）
  - [惑星大戰爭](../Page/惑星大戰爭.md "wikilink")（1977年[日本電影](../Page/日本.md "wikilink")）
  - [独立日](../Page/独立日_\(电影\).md "wikilink")（1996年[美国電影](../Page/美国.md "wikilink")）
  - [独立日：卷土重来](../Page/独立日：卷土重来.md "wikilink")（2016年[美国電影](../Page/美国.md "wikilink")）
  - [第三類接觸 (電影)](../Page/第三類接觸_\(電影\).md "wikilink")
  - [世界大战 (2005年电影)](../Page/世界大战_\(2005年电影\).md "wikilink")
  - [鋼鐵蒼穹](../Page/鋼鐵蒼穹.md "wikilink")（2012年電影）
  - [长江七号](../Page/长江七号.md "wikilink")（2008年中国电影）

### [戲劇](../Page/戲劇.md "wikilink")

  - [不明飞行物 (电视剧)](../Page/不明飞行物_\(电视剧\).md "wikilink")
  - [來自星星的你](../Page/來自星星的你.md "wikilink")
  - 夏米星小王子

### [動畫](../Page/動畫.md "wikilink")

  - [這是UFO！飛舞的圓盤](../Page/這是UFO！飛舞的圓盤.md "wikilink")（1975年3月21日[日本動畫電影](../Page/日本.md "wikilink")）
  - [UFO机器人 古连泰沙](../Page/UFO机器人_古连泰沙.md "wikilink")
  - [UFO戰士ダイアポロン](../Page/UFO戰士ダイアポロン.md "wikilink")（[日語](../Page/日語.md "wikilink")：UFO戦士ダイアポロン
    ）
  - 长江七号爱地球

### [輕小說](../Page/輕小說.md "wikilink")

  - [伊里野的天空、UFO的夏天](../Page/伊里野的天空、UFO的夏天.md "wikilink")

### 遊戲

  - [東方星蓮船 ～ Undefined Fantastic
    Object.](../Page/東方星蓮船_～_Undefined_Fantastic_Object..md "wikilink")
  - [UFO－生活的一天](../Page/UFO－生活的一天.md "wikilink")
  - [UFO超人ヤキソバン](../Page/UFO超人ヤキソバン.md "wikilink")（[日語](../Page/日語.md "wikilink")：UFO仮面ヤキソバン
    ）
  - [UFO CATCHER](../Page/UFO_CATCHER.md "wikilink")
  - [超級瑪利歐3D樂園](../Page/超級瑪利歐3D樂園.md "wikilink")（[日語](../Page/日語.md "wikilink")：スーパーマリオ3Dランド
    ）
  - [命令与征服
    红色警戒2：尤里的复仇](../Page/命令与征服_红色警戒2：尤里的复仇.md "wikilink")（游戏中尤里一方可以制造被译为“镭射幽浮”的飞碟）

## 相關條目

  - [中國的幽浮目擊](../Page/中國的幽浮目擊.md "wikilink")
  - [台灣飛碟學會](../Page/台灣飛碟學會.md "wikilink")
  - [世界幽浮史](../Page/世界幽浮史.md "wikilink")
  - [外星人](../Page/外星人.md "wikilink")
  - [小灰人](../Page/小灰人.md "wikilink")
  - [麥田圈](../Page/麥田圈.md "wikilink")
  - [美國國家航空航天局](../Page/美國國家航空航天局.md "wikilink")
  - [51區](../Page/51區.md "wikilink")
  - [陰謀論](../Page/陰謀論.md "wikilink")
  - [羅斯威爾飛碟墜毀事件](../Page/羅斯威爾飛碟墜毀事件.md "wikilink")
  - [華盛頓不明飛行物事件](../Page/華盛頓不明飛行物事件.md "wikilink")
  - [德國紐倫堡不明飛行物事件](../Page/德國紐倫堡不明飛行物事件.md "wikilink")
  - [比利時不明飛行物體事件](../Page/比利時不明飛行物體事件.md "wikilink")
  - [UFO事件列表](../Page/UFO事件列表.md "wikilink")
  - [飛碟學](../Page/飛碟學.md "wikilink")
  - [天體生物學](../Page/天體生物學.md "wikilink")
  - [宇宙語言學](../Page/宇宙語言學.md "wikilink")
  - [超常現象](../Page/超常現象.md "wikilink")
  - [未來世界](../Page/未來世界.md "wikilink")
  - [希爾夫婦被外星人綁架事件](../Page/希爾夫婦被外星人綁架事件.md "wikilink")
  - [巴西柯拉瑞斯島神秘光束](../Page/巴西柯拉瑞斯島神秘光束.md "wikilink")
  - [1973年帕斯卡古拉事件](../Page/1973年帕斯卡古拉事件.md "wikilink")
  - [莢狀雲](../Page/莢狀雲.md "wikilink")
  - [納粹飛碟](../Page/納粹飛碟.md "wikilink")
  - [飛行車](../Page/飛行車.md "wikilink")

## 参考文献

### 腳註

### 书籍

  -
  -
  -
## 外部連結

  - [Wikinews - UFO Archive](http://en.wikinews.org/wiki/Category:UFO)
  - [National Aviation Reporting Center on anomalous
    phenomena](http://narcap.org/)
  - [2](http://www.ncas.org/condon/) Condon Report, Dr. Edward U.
    Condon, Scientific Director, Daniel Gilmor, Editor (1968)
  - [UFO sightings at ICBM sites and nuclear weapons storage
    areas](http://www.nicap.org/babylon/missile_incidents.htm) by R.
    Hastings, NICAP
  - [藍皮書線上檔案](https://web.archive.org/web/20110223011701/http://bluebookarchive.org/)
    Online version of USAF Project Blue
  - [CIA educational summary on
    UFO](https://web.archive.org/web/20070509174747/https://www.cia.gov/csi/studies/97unclass/ufo.html)
  - [NASA Institute for Advanced Concepts](http://www.niac.usra.edu/)
  - [BBC article on Mexican Air-force
    videotape](http://news.bbc.co.uk/2/hi/americas/3707057.stm)
  - [*Popular Mechanics* article - When UFOs Arrive: February 2004 Cover
    Story](https://web.archive.org/web/20070927205802/http://www.popularmechanics.com/science/air_space/1283081.html?page=3)
  - [甘肅衛視-美國UFO研發計畫](https://www.youtube.com/watch?v=kxvYdAgurMU)
  - [太空總署UFO檔案](https://www.youtube.com/watch?v=KfHNy4Ivs3c)
  - [中國飛碟探索雜誌](https://archive.is/20130101145522/http://www.fdts.com.cn/)
  - [台灣飛碟學會](http://www.ufo.org.tw)
  - [星克空間飛碟學研究網（飛碟定理、發現的及靈異學）](https://web.archive.org/web/20161126172845/http://www.thinkerstar.com/)
  - [不明飛行物及超自然現象之探討！](https://web.archive.org/web/20120210052321/http://tw.myblog.yahoo.com/ufostory-1999)
  - [ufo110线索网](http://www.ufo110.net)

[不明飛行物體](../Category/不明飛行物體.md "wikilink")
[Category:超常現象](../Category/超常現象.md "wikilink")
[Category:未解決的問題](../Category/未解決的問題.md "wikilink")

1.  [英國X檔案揭秘，「幽浮」是大氣現象](http://www.tvbs.com.tw/news/news_list.asp?no=arieslu20060508222701)
2.  [鳳凰城幽浮事件　州長：我也看到！](http://www.tvbs.com.tw/news/news_list.asp?no=sunkiss20070322223707)記者:張家齊，外電報導
3.  [蘇軾《游金山寺》](http://www.cognitiohk.edu.hk/chilit/Poems/Song%20Shi/SuShi%20Shi/SuShi_43.htm)

4.  [大紀元時報-不再神祕的幽浮](http://www.epochtimes.com/b5/7/10/4/n1856295.htm)
5.  [太阳怎么晚上出来了？](http://bk.chinesehelper.cn/baike/dl/6129.aspx)
6.
7.
8.  國家地理頻道節目-寰宇搜秘：幽浮入侵[1](http://www.tudou.com/programs/view/WpLXhygE8Uc/)
9.  [追寻UFO：隐匿在五角大楼深处的神秘项目](https://cn.nytimes.com/usa/20171218/pentagon-program-ufo-harry-reid/)，纽约时报中文网，2017年12月18日。
10. [約瑟夫·艾倫·海尼克](../Page/約瑟夫·艾倫·海尼克.md "wikilink")（[J. Allen
    Hynek](../Page/J._Allen_Hynek.md "wikilink")）
    的天文學家和其他不明飛行物體研究者於1972年推出的著作：《经历UFO：科学调查》（*The
    UFO Experience: A Scientific Inquiry*）
11.
12. Lyne, J.E., Tauber, M. *[The Tunguska
    Event](http://web.utk.edu/~comet/papers/nature/TUNGUSKA.html)*
13. Nicholas Roerich, *Altai-Himalaya: A Travel Diary*, pp. 361–62.
14. Joaquim Fernandes and Fina d'Armada, *Heavenly Lights* 2005
15. Joaquim Fernandes and Fina d'Armada, *Celestial Secrets* 2007
16. Joaquim Fernandes, Fernando Fernandes and Raul Berenguel, *Fatima
    Revisited* 2008
17.
18. [Clarion Ledger
    article](http://www.ufocasebook.com/parkerhickson.html)
19.
20.
21.
22.
23.
24. [The 1973 Pascagoula, Mississippi Abduction
    (Hickson/Parker)](http://www.ufocasebook.com/Pascagoula.html)
25. not included in Hynek's original classification scheme.
26.
27. [Copy of
    Article](http://1.bp.blogspot.com/_-qWvml8_fAg/SGccRWGaJpI/AAAAAAAAAF8/J2QyUR-1d0E/s1600-h/SciAm2.JPG)
28. [中國經濟網--"吸血"的致命光束:難道是外星人正在做實驗](http://ufo.twup.org/news/2008/121208-2.htm)

29. *THE UFO EXPERIENCE: A scientific enquiry* (1972), ISBN
    978-1-56924-782-2
30. [  Clarion Ledger
    article](http://www.ufocasebook.com/parkerhickson.html)
31. <https://www.youtube.com/watch?v=JuFBUS0kiSA>
    影片:哥倫比亞大空站拍到眾多圓盤型飛碟，在他們釋出的繩子附近徘徊。
32. <http://www.aftonbladet.se/nyheter/article13263359.ab> 瑞典Treasure
    Hunters在2011年6月18日發現了一個巨大的圓形物在波羅的海芬蘭和瑞典之間。
33. <https://www.youtube.com/watch?v=7b-1OJi0b2M>
    影片:網友將STS-75目擊影片製成3D模型，判斷機尾缺口具備開合活動的功能。
34. [土豆網視頻－烏龍派出所電影版02-UFO來襲
    龍捲風大作戰](http://www.tudou.com/programs/view/dLwzbW-ZPeg/)