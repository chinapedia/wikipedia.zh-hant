**特克斯县**是[中国](../Page/中国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[伊犁哈萨克自治州所辖的一个](../Page/伊犁哈萨克自治州.md "wikilink")[县](../Page/县.md "wikilink")。位于[伊犁河上游的](../Page/伊犁河.md "wikilink")[特克斯河谷地东段](../Page/特克斯河谷.md "wikilink")\[1\]。

## 行政区划

下辖5个[镇](../Page/行政建制镇.md "wikilink")、1个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")、2个[民族乡](../Page/民族乡.md "wikilink")：

。

## 八卦城

1938年2月，原县府所在地科布前面临河、后面靠山，地域狭窄，难有大发展。当时任伊犁屯垦使兼警备司令的[邱宗浚到特克斯亲自勘查](../Page/邱宗浚.md "wikilink")，按照[易经](../Page/易经.md "wikilink")[八卦的形制建造](../Page/八卦.md "wikilink")[八卦城](../Page/八卦城.md "wikilink")，以放射状的4环、64条街路路相通。1939年夏，在时任县长班吉春的指挥下，聘请前[苏联水利技术人员帮助测量和打桩放线](../Page/苏联.md "wikilink")，用二十张牛犁沿着八卦图的八条射线犁出八卦城街道的雏形。当年10月22日，八卦城建成，县政府正式迁入办公。城市的现代病“拥堵”在这里不存在\[2\]\[3\]。2004年，特克斯县城被新疆维吾尔自治区政府列为自治区级历史文化名城（第一批），2007年被国务院列为[国家历史文化名城](../Page/国家历史文化名城.md "wikilink")\[4\]。特克斯县相规定在四环以内不准建5层以上建筑，以保证在观光塔上一览县城全貌。2009年7月又新建了八卦公园并设“大易碑廊”，将易经的三种版本刻于廊上，以弘扬《周易》文化\[5\]。

八卦城在今日的行政上屬於[特克斯鎮](../Page/特克斯鎮.md "wikilink")，八條主要街道的名稱按[周文王的](../Page/周文王.md "wikilink")[後天八卦順序](../Page/八卦#文王八卦/後天八卦.md "wikilink")，從北起順時針的順序分別如下\[6\]\[7\]：

  - 阿扎提街（坎街）\[8\]
  - 闊克阿尕什街（艮街）\[9\]\[10\]
  - 霍斯庫勒街（震街）\[11\]
  - 阿熱勒街（巽街）\[12\]
  - 博斯坦街（離街，亦作波斯坦街）
  - 闊布街（坤街，亦作闊步街）
  - 阿克奇街（兌街）
  - 古勒巴格街（-{乾}-街）\[13\]

### 軼事

文獻明白紀錄「八卦城」是20世紀時，[盛世才岳父邱宗浚依照](../Page/盛世才.md "wikilink")《[周易](../Page/周易.md "wikilink")》所設計。但當地人傳說卻稱，「八卦城」是[金朝末年](../Page/金朝.md "wikilink")，[華北](../Page/華北.md "wikilink")[道教](../Page/道教.md "wikilink")[全真七子之一的長春真人](../Page/全真七子.md "wikilink")[丘處機設計的](../Page/丘處機.md "wikilink")\[14\]。丘真人應[成吉思汗的邀請前往](../Page/成吉思汗.md "wikilink")[西域](../Page/西域.md "wikilink")，西遊[天山三年](../Page/天山.md "wikilink")，被途中的集山之剛氣、川之柔順、水之盛脈為一體的特克斯河谷所感動，便以此作為[八卦城](../Page/八卦城.md "wikilink")
的風水核心，確定了坎北、離南、震東、兌西四個方位。從此，是為「特克斯八卦城」最原始的雛形。但依《[長春真人西游記](../Page/長春真人西游記.md "wikilink")》記載，丘真人根本沒到特克斯河，丘处机建八卦城更无从谈及。其實是因邱宗浚為官甚為[貪腐](../Page/貪腐.md "wikilink")，人稱是「新疆第一貪」，當地[鄉民對他不滿](../Page/鄉民.md "wikilink")，不願意冠以「特克斯八卦城之父」的美譽，就改稱同姓源的[丘處機才是特克斯當初的規劃者](../Page/丘處機.md "wikilink")\[15\]\[16\]。

## 参考文献

## 外部链接

  - [特克斯县](http://www.zgtks.gov.cn/)

[特克斯县](../Category/特克斯县.md "wikilink")
[县](../Category/伊犁州直管县市.md "wikilink")
[伊犁](../Category/新疆县份.md "wikilink")
[新](../Category/国家历史文化名城.md "wikilink")

1.
2.  [特克斯
    我县概况](http://www.zgtks.gov.cn/zoujintekesi/quyugaikuang/2011-10-14/1815.html)


3.  [新疆特克斯县按“八
    神秘的特克斯县八卦城](http://www.chinanews.com/sh/2010/11-11/2650426.shtml)

4.  [国务院关于同意将新疆维吾尔自治区特克斯县列为国家历史文化名城的批复](http://www.gov.cn/zwgk/2007-05/11/content_611302.htm)

5.

6.

7.

8.  <http://img8.ph.126.net/OafahNTQzkagPav3goBRDg==/6597772354586210068.jpg>

9.
10.
11.
12. <http://img2.ph.126.net/r4JOddJfDyVlmVQe4w-pyA==/6597588736144371056.jpg>

13. <http://img1.ph.126.net/eI5IlREYqu_BVu1e_7MGgQ==/6597356739192288484.jpg>

14.

15. [八卦三題](http://www.tianshannet.com/big5/culture/content/2008-06/16/content_2642555.htm)

16. [指點江山 心事浩茫
    ——姜付炬《八卦三題》解讀](http://www.tianshannet.com/big5/culture/content/2008-06/16/content_2642703.htm)