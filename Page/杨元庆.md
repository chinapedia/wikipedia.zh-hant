**杨元庆**（），生于[中国](../Page/中国.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[合肥市](../Page/合肥市.md "wikilink")，中国大陆IT界泰斗，现任[联想集团](../Page/联想集团.md "wikilink")[董事局主席和](../Page/董事局主席.md "wikilink")[CEO](../Page/首席执行官.md "wikilink")，毕业于[上海交通大学](../Page/上海交通大学.md "wikilink")。

## 生平

### 早年

杨元庆[籍贯](../Page/籍贯.md "wikilink")[宁波市](../Page/宁波市.md "wikilink")[定海县](../Page/定海县.md "wikilink")（今[舟山定海区](../Page/舟山.md "wikilink")），生于安徽省[合肥市](../Page/合肥市.md "wikilink")，幼年时父母均是资优知识分子，从小学习成绩均十分优异，尤其理科成绩。1982年毕业于[合肥一中](../Page/合肥一中.md "wikilink")，并考入[上海交通大学计算机系](../Page/上海交通大学.md "wikilink")，1986年本科毕业。1989年，在[中国科学技术大学取得硕士学位](../Page/中国科学技术大学.md "wikilink")。

### 联想集团

  - 1989年，进入[联想集团工作](../Page/联想集团.md "wikilink")。

<!-- end list -->

  - 1994年，杨元庆任联想电脑公司总经理。

<!-- end list -->

  - 2001年，杨元庆出任联想集团[总裁兼](../Page/总裁.md "wikilink")[CEO](../Page/CEO.md "wikilink")。

<!-- end list -->

  - 2004年12月8日，杨元庆出任联想集团董事局主席。

<!-- end list -->

  - 2009年2月5日，联想集团宣布联想集团创始人兼董事[柳传志将重新担任公司董事局主席](../Page/柳传志.md "wikilink")，现任董事局主席杨元庆接替[威廉·阿梅里奥担任公司CEO](../Page/威廉·阿梅里奥.md "wikilink")。

<!-- end list -->

  - 2011年6月17日，聯想大股東聯想控股，在聲明中指出，楊元慶已從聯想控股購得7.97億股聯想股份，但未有披露作價。交易後，聯想控股佔聯想股權，從42.82%降至34.82%，但仍為最大股東，楊元慶個人持有的聯想股權則增加了8個百分點至8.7%。

<!-- end list -->

  - 2011年11月2日，杨元庆接替[柳传志](../Page/柳传志.md "wikilink")，再次成为联想集团董事局主席。

<!-- end list -->

  - 截至2013年11月20日，楊元慶持有聯想股權7.68%。

<!-- end list -->

  - 2014年5月30日，根据联想集团在港交所提交的报告，联想集团董事长兼CEO杨元庆2013年的薪酬达到2136万美元，约合人民币1.33亿元，较2012年的1460万美元年薪大幅上涨46%。联想集团称，由于在2013-2014财年内公司业绩出色，公司给予杨元庆的奖金和长期激励奖励大幅增长，推动其薪酬上涨。\[1\]

## 参考文献

  - [搜狐人物频道
    杨元庆简介](https://web.archive.org/web/20060625191207/http://index.it.sohu.com/person/plist.php?userid=273)
  - [杨元庆再次接下联想帅印
    柳传志将投身联想控股](http://news.sohu.com/20111103/n324364058.shtml)

## 外部链接

  -
[Category:中华人民共和国企业家](../Category/中华人民共和国企业家.md "wikilink")
[Category:中国程序员](../Category/中国程序员.md "wikilink")
[Category:联想人物](../Category/联想人物.md "wikilink")
[Category:上海交通大学校友](../Category/上海交通大学校友.md "wikilink")
[Category:中国科学技术大学校友](../Category/中国科学技术大学校友.md "wikilink")
[Category:合肥人](../Category/合肥人.md "wikilink")
[Category:舟山人](../Category/舟山人.md "wikilink")
[Category:定海人](../Category/定海人.md "wikilink")
[Yuanqing](../Category/楊姓.md "wikilink")
[Category:安徽企业家](../Category/安徽企业家.md "wikilink")
[Category:合肥市第一中学校友](../Category/合肥市第一中学校友.md "wikilink")
[Category:中国首席执行官](../Category/中国首席执行官.md "wikilink")

1.  [杨元庆去年年薪1.33亿
    联想称公司业绩出色助其涨薪](http://www.ce.cn/xwzx/gnsz/gdxw/201405/31/t20140531_2905117.shtml)，中国经济网，2014年5月31日