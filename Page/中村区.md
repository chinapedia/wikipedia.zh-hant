**中村區**（）為[日本](../Page/日本.md "wikilink")[名古屋市](../Page/名古屋市.md "wikilink")16個行政區之一。中村區之名驛一帶有[JR和](../Page/JR.md "wikilink")[名古屋鐵道和](../Page/名古屋鐵道.md "wikilink")[近畿日本鐵道的](../Page/近畿日本鐵道.md "wikilink")[名古屋站](../Page/名古屋站.md "wikilink")。

## 名人

[太閣](../Page/太閣.md "wikilink")[豐臣秀吉](../Page/豐臣秀吉.md "wikilink")
演員[玉木宏](../Page/玉木宏.md "wikilink")

## 重要企業

[森精机总部](../Page/森精机.md "wikilink")。\[1\]

## 参考资料及注释

[Category:名古屋市的區](../Category/名古屋市的區.md "wikilink")

1.  "[日本国内](http://www.moriseiki.com/chinese/support/tc_japan/index.html)
    ." [森精机](../Page/森精机.md "wikilink"). "名古屋总公司
    爱知县名古屋市中村区名站2-35-16（邮编450-0002）"