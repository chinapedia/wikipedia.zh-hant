**弗羅姆**（，，）是英格蘭[索美塞特郡東北方的城鎮與](../Page/索美塞特郡.md "wikilink")[民政教區](../Page/民政教區.md "wikilink")。坐落於東端，該鎮建立在不平坦的高地上，城鎮中心有。該鎮位於[巴斯南方約](../Page/巴斯_\(薩默塞特郡\).md "wikilink")、[湯頓](../Page/湯頓.md "wikilink")[郡治東方約](../Page/郡治.md "wikilink")，及[倫敦西方約](../Page/倫敦.md "wikilink")。2011年人口普查的人口數是26,203人。該鎮位於[索美塞特郡的](../Page/索美塞特郡.md "wikilink")區，且是[薩默頓和弗羅姆](../Page/薩默頓和弗羅姆_\(英國國會選區\).md "wikilink")[選區的一部份](../Page/選區.md "wikilink")。

## 參考資料

## 外部連結

  - [Frome History by D
    JSteward](https://web.archive.org/web/20080509160907/http://users.breathe.com/djsteward/)

  -
  - [Frome Town
    Council](https://web.archive.org/web/20080518100425/http://www.frome-tc.gov.uk/)

  - [Fairtrade Frome](http://www.fairtradefrome.org.uk/)

[Frome](../Category/弗羅姆.md "wikilink")
[Category:索美塞特郡民政教區](../Category/索美塞特郡民政教區.md "wikilink")
[Category:門迪普城鎮](../Category/門迪普城鎮.md "wikilink")