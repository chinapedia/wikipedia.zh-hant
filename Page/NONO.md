**陳宣裕**（），生於[臺灣](../Page/臺灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[瑞芳區](../Page/瑞芳區.md "wikilink")，是臺灣[主持人](../Page/主持人.md "wikilink")、[搞笑藝人](../Page/搞笑藝人.md "wikilink")、[商人](../Page/商人.md "wikilink")，藝名**NONO**。NONO是知名主持人[許效舜的徒弟](../Page/許效舜.md "wikilink")，也曾為綜藝天王[吳宗憲旗下](../Page/吳宗憲.md "wikilink")「[憲憲家族](../Page/吳宗憲#2001年〜2010年.md "wikilink")」重要成員\[1\]，是吳宗憲最知名的頂尖搭檔之一。2015年起，NONO多方發展個人副業。

## 演藝生涯

1990年代，[三立衛視](../Page/三立衛視.md "wikilink")（今[三立電視](../Page/三立電視.md "wikilink")）成立經紀部時簽下NONO，他成為首位進入其經紀部的藝人，當時的[日薪為](../Page/日薪.md "wikilink")[新台幣](../Page/新台幣.md "wikilink")2000元，且能參演三立的任何節目\[2\]；後NONO開始擔任攝影助理、道具工作人員，繼而在當時[三立綜藝台頗受好評的歌唱競賽節目](../Page/三立綜藝台.md "wikilink")《[21世紀新人歌唱排行榜](../Page/21世紀新人歌唱排行榜.md "wikilink")》擔任工作人員，NONO幾乎每一集都被該節目女主持人[馬維欣使喚上台曝光](../Page/馬維欣.md "wikilink")，在當時他已小有名氣。

NONO曾任許效舜的司機並拜其為師，在《[鐵獅玉玲瓏](../Page/鐵獅玉玲瓏.md "wikilink")》中也擔任炒熱氣氛的鼓手，並不時表演自創的「划船舞」(源自軍中習得的操槍專長，以槍代槳，屁股坐地伸縮腿模擬划船)。在《[黃金夜總會](../Page/黃金夜總會.md "wikilink")》的許多短劇單元中，NONO以出人意表的笑料展現[綜藝實力](../Page/綜藝.md "wikilink")。而後NONO加入吳宗憲旗下「[憲憲家族](../Page/吳宗憲#2001年〜2010年.md "wikilink")」，成為其中主要成員\[3\]，家族中的主持人又以同年出道的NONO和[康康最為耀眼](../Page/康康.md "wikilink")，他成為吳宗憲身邊最知名的頂尖搭檔之一。吳宗憲、康康和NONO的搭檔為廣受觀眾喜愛的經典組合，康康常被調侃為「外星混血」，NONO則是「幾光男孩」（陽光男孩、[膀胱男孩](../Page/膀胱.md "wikilink")、[走光男孩](../Page/走光_\(行為\).md "wikilink")、兩光男孩、輸光男孩）。

2008年，NONO被稱「B咖一哥」，在知名綜藝遊戲節目《[天才衝衝衝](../Page/天才衝衝衝.md "wikilink")》創立「嗯哈哈樂團」。因NONO從小就視知名歌手[庾澄慶為偶像](../Page/庾澄慶.md "wikilink")，因此亦常參演庾主持的[台視節目](../Page/台視.md "wikilink")《[百萬大歌星](../Page/百萬大歌星.md "wikilink")》，擔任固定來賓，自封「班長」。2015年起，NONO積極跨足餐飲產業，發展個人副業。

## 副業版圖

2009年，NONO開設潮牌服飾店，直到2012年6月仍未虧損\[4\]\[5\]。

2015年2月，NONO和商人劉明道、歌手[唐儷合夥投資創立連鎖](../Page/唐儷.md "wikilink")[雞排店](../Page/香雞排.md "wikilink")「艋舺雞排」；開幕試營運近半個月的每日營業額突破台幣6萬元\[6\]，NONO出任[董事](../Page/董事.md "wikilink")[品牌總監](../Page/品牌總監.md "wikilink")\[7\]\[8\]\[9\]\[10\]。2016年初，於中國大陸開設海外分店；2017年再於[美國](../Page/美國.md "wikilink")[亞特蘭大和](../Page/亞特蘭大.md "wikilink")[休士頓](../Page/休士頓.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")[多倫多](../Page/多倫多.md "wikilink")、[英國持續拓點](../Page/英國.md "wikilink")\[11\]\[12\]\[13\]。至2018年4月，全台自營與加盟店家數已達60家以上，其中3家店是直營店，其他店則為加盟店\[14\]\[15\]\[16\]；台灣跟海外共有78家店，當中共4家直營店、每月營收總計約台幣500萬元，加盟店則各自獨立營運獲利\[17\]。演藝圈許多藝人和[名媛](../Page/名媛.md "wikilink")[孫芸芸為常客](../Page/孫芸芸.md "wikilink")\[18\]。

2016年底，NONO與「艋舺雞排」經營團隊投資開設古早味蛋糕店「Sofia雞蛋妹蛋糕」，並與妻子[朱海君開設美式漢堡店](../Page/朱海君.md "wikilink")「桑威奇Only
Love美式餐廳」\[19\]。2018年1月，他和好友[歐漢聲合夥開設花果茶系列手搖飲料店](../Page/歐漢聲.md "wikilink")「花羨沐嵐」，由歐漢聲擔任[董事](../Page/董事.md "wikilink")\[20\]\[21\]\[22\]，至同年3月已有2家分店\[23\]\[24\]\[25\]。

## 個人生活

「NONO」這個小名來自父母在NONO年幼時怕他被人拐跑，告訴他如果遇到陌生人要說「NO！NO！」。其軍旅生涯在[憲兵單位服役](../Page/中華民國憲兵.md "wikilink")（忠貞435梯次），以[上兵軍階退伍](../Page/上兵.md "wikilink")。NONO多次在節目中表示，1991年（[中華民國](../Page/中華民國.md "wikilink")80年）[雙十國慶](../Page/雙十國慶.md "wikilink")[閱兵大典](../Page/閱兵.md "wikilink")（演習代號：華統演習）中，他曾擔任閱兵指揮部掌旗禮兵，負責執掌[空軍軍旗](../Page/中華民國空軍.md "wikilink")。

2011年11月3日，NONO在[台南棒球場](../Page/台南棒球場.md "wikilink")「2011明星公益棒球賽」上向交往2年多的女友歌手朱海君下跪求婚，而朱海君亦答應\[26\]。然而因朱海君的媽媽見女兒事業剛起步，不宜過早結婚而反對，兩人暫時分開四個月。最終NONO經師父許效舜和好友[曾國城等人的助陣到朱家提親成功](../Page/曾國城.md "wikilink")\[27\]，兩人在2013年7月12日結婚，伴郎是[歐漢聲和](../Page/歐漢聲.md "wikilink")[吳克羣](../Page/吳克羣.md "wikilink")，伴娘是[果凍姐姐和](../Page/果凍姐姐.md "wikilink")[愷樂](../Page/簡愷樂.md "wikilink")，媒人是[王彩樺](../Page/王彩樺.md "wikilink")\[28\]\[29\]。2014年3月2日，長女誕生，暱稱「NO妹」；2015年證實[乾爹是](../Page/乾爹.md "wikilink")[阿杜](../Page/阿杜.md "wikilink")\[30\]。

## 節目主持

|                                                                                           |                                        |
| ----------------------------------------------------------------------------------------- | -------------------------------------- |
| 頻道                                                                                        | 節目                                     |
| [中天娛樂台](../Page/中天娛樂台.md "wikilink")                                                      | 《[黃金B段班](../Page/黃金B段班.md "wikilink")》 |
| [民視](../Page/民視.md "wikilink")                                                            | 《綜藝戰艦》(代班主持)                           |
| [台視](../Page/台視.md "wikilink")                                                            | 《周六響叮噹》                                |
| 《綜藝旗艦 Hello jacky\!》                                                                      |                                        |
| 《歡樂艦隊》                                                                                    |                                        |
| 《綜藝旗艦》                                                                                    |                                        |
| 《綜藝最愛憲》                                                                                   |                                        |
| [中視](../Page/中視.md "wikilink")                                                            | 《[周日八點黨](../Page/周日八點黨.md "wikilink")》 |
| 《嘻哈幫》                                                                                     |                                        |
| 《綜藝哇沙米》                                                                                   |                                        |
| [華視](../Page/華視.md "wikilink")                                                            | 《好運旺旺來》(〈i love abc〉單元)                |
| 《[POWER星期天](../Page/POWER星期天.md "wikilink")》                                              |                                        |
| [三立電視](../Page/三立電視.md "wikilink")                                                        | 《台灣群星會》                                |
| 《台灣大廟口》                                                                                   |                                        |
| 《[黃金夜總會](../Page/黃金夜總會.md "wikilink")》                                                    |                                        |
| 《藝界人生》                                                                                    |                                        |
| 《石頭族樂園》                                                                                   |                                        |
| 《鳳中奇緣》                                                                                    |                                        |
| 《[超級偶像](../Page/超級偶像.md "wikilink")》                                                      |                                        |
| [TVBS](../Page/TVBS.md "wikilink")                                                        | 《娛樂新聞》                                 |
| [東森綜合台](../Page/東森綜合台.md "wikilink")                                                      | 《JACKY SHOW》                           |
| 《全球新人王》                                                                                   |                                        |
| 台視、華視、[ET Jacky](../Page/ET_Jacky.md "wikilink")、[新加坡電視機構](../Page/新加坡電視機構.md "wikilink") | 《超猛新人王》                                |
| [緯來綜合台](../Page/緯來綜合台.md "wikilink")                                                      | 《娛樂大網ㄎㄚ》                               |
| [重慶衛視](../Page/重慶衛視.md "wikilink")                                                        | 《娛樂星工廠》                                |
| [新传媒电视](../Page/新传媒电视.md "wikilink")                                                      | 《3个男人1张口》                              |

## 配音演出

|       |                                                          |                                  |
| ----- | -------------------------------------------------------- | -------------------------------- |
| 年份    | 片名                                                       | 角色                               |
| 2005年 | 《[紅孩兒：決戰火焰山](../Page/紅孩兒：決戰火焰山.md "wikilink")》           | [豬八戒](../Page/豬八戒.md "wikilink") |
| 2007年 | 《[料理鼠王](../Page/料理鼠王.md "wikilink")》                     | 大米                               |
| 2007年 | 《[亞瑟的奇幻王國：毫髮人的冒險](../Page/亞瑟的奇幻王國：毫髮人的冒險.md "wikilink")》 | 毫髮國小王子 貝達麥許                      |

## 影視作品

### 戲劇

|       |       |                                            |        |
| ----- | ----- | ------------------------------------------ | ------ |
| 年份    | 頻道    | 劇名                                         | 角色     |
| 1997年 |       | 《[大姐當家](../Page/大姐當家.md "wikilink")》       | 客串 鐵算盤 |
| 1999年 |       | 《鐵師玉玲瓏》                                    | 爵士鼓手   |
| 2002年 |       | 《聖夏零度C》                                    | 泰哥     |
| 2005年 |       | 《[好美麗診所](../Page/好美麗診所.md "wikilink")》     |        |
| 2012年 | 大愛    | 《[幸福好簡單](../Page/幸福好簡單.md "wikilink")》     | 陳宏昇    |
| 2013年 | 八大綜合台 | 《[終極一班3](../Page/終極一班3.md "wikilink")》     | 薩必四    |
| 2015年 |       | 《[星座愛情獅子女](../Page/星座愛情獅子女.md "wikilink")》 | 主持人    |
| 2016年 | 八大綜合台 | 《[終極一班4](../Page/終極一班4.md "wikilink")》     | 薩必四    |

### 電影

|       |                                               |          |
| ----- | --------------------------------------------- | -------- |
| 年份    | 片名                                            | 角色       |
| 1999年 | 《神探兩個半》                                       |          |
| 2007年 | 《四大金釵之夜明珠》                                    |          |
| 2007年 | 《傻傻愛》                                         |          |
| 2008年 | 《霹靂探哥》                                        |          |
| 2011年 | 《做人》(新加坡電影)                                   |          |
| 2012年 | 《贪心鬼见鬼》(新加坡电影)                                |          |
| 2014年 | 《[鐵獅玉玲瓏](../Page/鐵獅玉玲瓏_\(電影\).md "wikilink")》 | 阿添       |
| 2015年 | 《[十万夥急](../Page/十万夥急.md "wikilink")》          | 精神病院醫護人員 |

## 電視廣告

[董氏基金會](../Page/董氏基金會.md "wikilink") 公共場所禁菸-老大篇\[31\]

## 參考資料

## 外部連結

  -
  -
  -
  -
  -
  -
[C陳](../Category/瑞芳人.md "wikilink")
[C陳](../Category/台灣綜藝節目主持人.md "wikilink")
[C陳](../Category/臺灣電影男演員.md "wikilink")
[C陳](../Category/臺灣電視男演員.md "wikilink")
[N](../Category/陳姓.md "wikilink")
[Category:模仿藝人](../Category/模仿藝人.md "wikilink")
[Category:新北市立瑞芳高級工業職業學校校友](../Category/新北市立瑞芳高級工業職業學校校友.md "wikilink")

1.  [17年前來台沒名又沒錢…吳宗憲收留！　懷秋謝恩4前輩「一輩子憲憲家族」](https://star.ettoday.net/news/1411879/)，[ETtoday新聞雲](../Page/ETtoday新聞雲.md "wikilink")，2019-03-31

2.  [緯來綜合台製作](../Page/緯來綜合台.md "wikilink")，《[國人都叫好](../Page/國人都叫好.md "wikilink")》第十七集「綠葉主持的辛酸血淚史」，於2009年5月30日重播

3.
4.  [吳宗憲投資副業倒光光　子弟兵Nono投資副業：守成不易](https://star.ettoday.net/news/65136/)，[ETtoday新聞雲](../Page/ETtoday新聞雲.md "wikilink")，2012-06-26

5.  [NONO叫雞專線call客
    不忘暗虧吳宗憲](http://ent.ltn.com.tw/news/breakingnews/1231764/)，[自由時報電子報](../Page/自由時報電子報.md "wikilink")，2015-02-12

6.
7.  [躲過食安危機狂銷20萬片雞排　Nono把關「一定炸到熟」](https://star.ettoday.net/news/523236/)，[ETtoday新聞雲](../Page/ETtoday新聞雲.md "wikilink")，2015-06-20

8.  [加盟旋風！走出台灣、邁向世界　艋舺雞排力拚股票上市](https://www.nownews.com/news/20151209/1912445/)，[NOWnews
    今日新聞](../Page/NOWnews_今日新聞.md "wikilink")，2015-12-09

9.  [雞排大王NONO月賺5千萬
    不受禁宰令影響因...](http://www.chinatimes.com/realtimenews/20170218004534-260404/)，[中時電子報](../Page/中時電子報.md "wikilink")，2017-02-18

10. [天王、名媛都愛吃！NONO賣雞排　製作過程獨家大公開](https://www.setn.com/News.aspx?NewsID=296648/)，[三立新聞網](../Page/三立新聞網.md "wikilink")，2017-09-20

11. [NONO副業進軍加拿大
    炸雞排"台灣直送"](https://news.cts.com.tw/cts/entertain/201707/201707241880396.html#.W2dXg9UzbIU/)，[華視新聞](../Page/華視新聞.md "wikilink")，2017-07-24

12. [NONO雞排賣進加拿大
    自爆業績五千萬是膨風](https://stars.udn.com/star/story/10091/2603139/)，[聯合新聞網](../Page/聯合新聞網.md "wikilink")，2017-07-25

13. [NONO雞排店拓點北美
    笑撇月入5千萬](http://ent.ltn.com.tw/news/breakingnews/2142818/)，[自由時報電子報](../Page/自由時報電子報.md "wikilink")，2017-07-25

14. [密謀拉曾國城合作
    NONO想賣「雞排拌麵」](https://stars.udn.com/star/story/10091/3107241/)，[聯合新聞網](../Page/聯合新聞網.md "wikilink")，2018-04-25

15. [Nono雞排賣到國外去
    揪曾國城合作遭打槍](http://www.chinatimes.com/realtimenews/20180425003924-260404/)，[中時電子報](../Page/中時電子報.md "wikilink")，2018-04-25

16. [Nono雞排店狂開60間　想揪曾國城合作被打槍](https://www.nownews.com/news/20180425/2742155/)，[NOWnews
    今日新聞](../Page/NOWnews_今日新聞.md "wikilink")，2018-04-25

17. [Nono 雞排出海衝78店
    拉歐弟搶流水財](https://tw.appledaily.com/entertainment/daily/20180129/37917633/)，[蘋果日報
    (台灣)](../Page/蘋果日報_\(台灣\).md "wikilink")，2018-01-29

18.
19. [雞排生意大成功
    Nono港都賣蛋糕](http://www.chinatimes.com/newspapers/20170402000516-260112/)，[中時電子報](../Page/中時電子報.md "wikilink")，2017-04-02

20. [歐漢聲拒錄《爸爸去哪兒》　除非女兒符合Sandy的2條件](https://star.ettoday.net/news/1102354/)，[ETtoday新聞雲](../Page/ETtoday新聞雲.md "wikilink")，2018-01-28

21. [歐漢聲被問舊愛小嫻
    慌張這樣做](http://ent.ltn.com.tw/news/breakingnews/2325493/)，[自由時報電子報](../Page/自由時報電子報.md "wikilink")，2018-01-28

22.
23. [NONO高雄賣手搖飲　小三最愛黑糖波霸奶](https://tw.appledaily.com/new/realtime/20180325/1321943/)，[蘋果日報
    (台灣)](../Page/蘋果日報_\(台灣\).md "wikilink")，2018-03-25

24. [NONO副業再擴大！現做手搖飲料　竟對女主播扭動下半身](https://www.setn.com/News.aspx?NewsID=362814/)，[三立新聞網](../Page/三立新聞網.md "wikilink")，2018-03-28

25.
26. [NONO球場下跪求婚　抱朱海君衝上本壘](http://www.nownews.com/2011/12/04/340-2763459.htm)

27. [NoNo婚禮拖一年
    宣布7月12日迎娶朱海君(圖)](http://www.chinanews.com/yl/2013/04-02/4696960.shtml)

28. [NONO婚禮撞颱風 迎娶美嬌娘不延期](http://www.youtube.com/watch?v=bVOmQkVhvPw)

29. [伴郎歐漢聲 吳克羣 伴娘凍姐姐 愷樂(蝴蝶姐姐) NONO
    朱海君婚禮](http://www.youtube.com/watch?v=A3Y6nYWqDKk)

30. [阿杜「恐慌症」離開歌壇7年　第一個發現的人是NONO](http://www.ettoday.net/news/20150803/544419.htm)

31. [董氏基金會 公共場所禁菸-老大篇](http://www.youtube.com/watch?v=DFCDFzA4LlE)