COCl<sub>2</sub>}}

**氯化钴**，常称**氯化亚钴**、**二氯化钴**或**氯化鈷(II)**，化学式为[Co](../Page/鈷.md "wikilink")[Cl](../Page/氯.md "wikilink")<sub>2</sub>；無水的氯化鈷呈[藍色](../Page/藍色.md "wikilink")，它的水合物很多，常见者为粉紅色的六水合氯化鈷CoCl<sub>2</sub>·6H<sub>2</sub>O；無水物具吸濕性，水合物具[潮解性](../Page/潮解.md "wikilink")。

固态六水物中，四个水分子是[配位水](../Page/配位.md "wikilink")，兩個水分子是[結晶水](../Page/結晶水.md "wikilink")，即\[CoCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>\]·2H<sub>2</sub>O。有水物溶於水和[乙醇](../Page/乙醇.md "wikilink")。

在[歐盟的](../Page/歐盟.md "wikilink")「[關於化學品註冊、評估、許可和限制法案](../Page/關於化學品註冊、評估、許可和限制法案.md "wikilink")」（REACH）中，已將氯化钴列為高度關注物質(SVHC)。\[1\]

## 制备

无水氯化钴可由[钴与](../Page/钴.md "wikilink")[氯气反应制备](../Page/氯气.md "wikilink")：

  -
    [Co](../Page/钴.md "wikilink")([s](../Page/固态.md "wikilink")) +
    [Cl<sub>2</sub>](../Page/氯.md "wikilink")([g](../Page/气态.md "wikilink"))
    → CoCl<sub>2</sub>(s)

水合氯化钴则可通过[氧化钴(II)或](../Page/氧化钴\(II\).md "wikilink")[碳酸钴与](../Page/碳酸钴.md "wikilink")[盐酸反应制取](../Page/盐酸.md "wikilink")：

  -
    [CoO](../Page/氧化鈷\(II\).md "wikilink") +
    2[HCl](../Page/鹽酸.md "wikilink") → CoCl<sub>2</sub> +
    [H<sub>2</sub>O](../Page/水.md "wikilink")
    [CoCO<sub>3</sub>](../Page/碳酸鈷.md "wikilink") + 2HCl →
    CoCl<sub>2</sub> + [CO<sub>2</sub>](../Page/二氧化碳.md "wikilink") +
    H<sub>2</sub>O

## 反应

六水物和無水物都是弱[路易斯酸](../Page/路易斯酸.md "wikilink")，可以形成配合物。這些配合物多為[正四面體或](../Page/正四面體.md "wikilink")[正八面體結構](../Page/正八面體.md "wikilink")。

  -
    CoCl<sub>2</sub>·6H<sub>2</sub>O + 4
    [C<sub>5</sub>H<sub>5</sub>N](../Page/吡啶.md "wikilink") →
    CoCl<sub>2</sub>(C<sub>5</sub>H<sub>5</sub>N)<sub>4</sub> + 6
    H<sub>2</sub>O
    CoCl<sub>2</sub>·6H<sub>2</sub>O + 2
    [P(C<sub>6</sub>H<sub>5</sub>)<sub>3</sub>](../Page/三苯基膦.md "wikilink")
    →
    CoCl<sub>2</sub>{P(C<sub>6</sub>H<sub>5</sub>)<sub>3</sub>}<sub>2</sub>
    + 6 H<sub>2</sub>O
    CoCl<sub>2</sub> + 2 \[(C<sub>2</sub>H<sub>5</sub>)<sub>4</sub>N\]Cl
    →
    \[(C<sub>2</sub>H<sub>5</sub>)<sub>4</sub>N)\]<sub>2</sub>\[CoCl<sub>4</sub>\]\[2\]

钴(II)与[氨的](../Page/氨.md "wikilink")[配合物在](../Page/配合物.md "wikilink")[氧气存在下](../Page/氧气.md "wikilink")，很容易被氧化成钴(III)的配合物：

  -
    4 CoCl<sub>2</sub>·6H<sub>2</sub>O + 4
    [NH<sub>4</sub>Cl](../Page/氯化铵.md "wikilink") + 20
    [NH<sub>3</sub>](../Page/氨.md "wikilink") +
    [O<sub>2</sub>](../Page/氧.md "wikilink") → 4
    \[Co(NH<sub>3</sub>)<sub>6</sub>\]Cl<sub>3</sub> + 26
    [H<sub>2</sub>O](../Page/水分子.md "wikilink")

## 应用

  - 红色六水物CoCl<sub>2</sub>·6H<sub>2</sub>O可用作[水的顯示劑](../Page/水.md "wikilink")；
  - [无水氯化鈷試紙在干燥时为蓝色](../Page/无水氯化鈷試紙.md "wikilink")，潮湿时转为粉红色；
  - 在[硅胶中渗入一定量的氯化钴](../Page/硅胶.md "wikilink")，可借以指示[硅胶的吸湿程度](../Page/硅胶.md "wikilink")；当它由蓝色变为红色时，表明吸水已达到[饱和](../Page/饱和.md "wikilink")。将红色[硅胶在](../Page/硅胶.md "wikilink")120°C烘干，待蓝色恢复后仍可使用。
  - 氯化钴通常用作实验室试剂和制取其他钴化合物的原料。

## 参考资料

[Category:钴化合物](../Category/钴化合物.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:铁系元素卤化物](../Category/铁系元素卤化物.md "wikilink")
[Category:IARC第2B类致癌物质](../Category/IARC第2B类致癌物质.md "wikilink")
[Category:潮解物质](../Category/潮解物质.md "wikilink")

1.  <http://echa.Europa.eu/consultations/authorisation/svhc/svhc_cons_en.asp>

    [1](http://echa.Europa.eu/consultations/authorisation/svhc/svhc_cons_en.asp)

2.  Gill, N. S. and Taylor, F. B., "Tetrahalo Complexes of Dipositive
    Metals in the First Transition Series", Inorganic Syntheses, 1967,
    volume 9, pages 136-142.