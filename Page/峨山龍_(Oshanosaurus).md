**峨山龍**（屬名："Oshanosaurus"）意為「[峨山蜥蜴](../Page/峨山.md "wikilink")」，是個非正式名稱，目前還沒有經過正式研究。峨山龍是種[草食性恐龍](../Page/草食性.md "wikilink")，可能屬於[蜥腳下目](../Page/蜥腳下目.md "wikilink")，生存於早[侏儸紀的](../Page/侏儸紀.md "wikilink")[中國](../Page/中國.md "wikilink")，屬於[祿豐組下層](../Page/祿豐組.md "wikilink")，約1億9000萬年前。[模式種為](../Page/模式種.md "wikilink")**楊氏峨山龍**（"O.
youngi"），是在1985年由[趙喜進發現於中國](../Page/趙喜進.md "wikilink")[雲南省](../Page/雲南省.md "wikilink")[峨山彝族自治县](../Page/峨山彝族自治县.md "wikilink")\[1\]。過去曾被歸類於[鳥臀目](../Page/鳥臀目.md "wikilink")[畸齒龍科](../Page/畸齒龍科.md "wikilink")，這是因為與[滇中龍並置的關係](../Page/滇中龍.md "wikilink")；滇中龍過去曾被認為是畸齒龍科恐龍\[2\]。峨山龍身長可能約12公尺，因此不太可能是鳥臀目恐龍。

## 參考資料

## 外部連結

  - ["Oshanosaurus" @ DinoRuss
    Site](https://web.archive.org/web/20080304235027/http://www.dinoruss.com/de_4/5a8e234.htm)
  - [Dinosauromorpha](https://web.archive.org/web/20090327113542/http://www.users.qwest.net/~jstweet1/dinosauromorpha.htm)
  - ["Oshanosaurus" page (in
    Spanish)](http://www.duiops.net/dinos/oshanosaurus.html)

[Category:無資格的恐龍](../Category/無資格的恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:蜥腳下目](../Category/蜥腳下目.md "wikilink")

1.  Zhao, 1985. The reptilian fauna of the Jurassic in China. Pages
    286–289, 347 in Wang, Cheng and Wang (eds.). The Jurassic System
    of China. Geological Publishing House, Beijing.
2.