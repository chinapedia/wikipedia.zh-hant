**路易·沙夏**（，）是一位已退役的[法國職業足球員](../Page/法國.md "wikilink")，司職前鋒。曾效力[英超](../Page/英超.md "wikilink")[紐卡素](../Page/纽卡斯尔联足球俱乐部.md "wikilink")、[富咸](../Page/富咸.md "wikilink")、[曼聯](../Page/曼聯.md "wikilink")、[愛華頓](../Page/愛華頓.md "wikilink")、[熱刺及](../Page/熱刺.md "wikilink")[新特蘭等球會](../Page/桑德兰足球俱乐部.md "wikilink")。現已退役。

## 生平

### 球會

沙夏於1999年首次踏足[英格蘭超級聯賽替](../Page/英格蘭超級聯賽.md "wikilink")[紐卡素上陣](../Page/纽卡斯尔联足球俱乐部.md "wikilink")，該次是以借用形式從法國[梅斯加盟](../Page/梅斯足球俱乐部.md "wikilink")。該名射手上陣11場取得兩個入球，包括作客伊活公園（Edwood
Park）以一球令人驚嘆的美妙遠射將[-{zh-hans:布莱克本;zh-hk:布力般流浪}-淘汰出局](../Page/布莱克本流浪者足球俱乐部.md "wikilink")。

之後返回法國於該季合共替梅斯上陣22次攻入6球，並於2000年6月16日以210萬[英鎊轉會](../Page/英鎊.md "wikilink")[富咸](../Page/富勒姆足球俱乐部.md "wikilink")。加盟首季替富咸轟入32球，帶領球隊升上超級聯賽，並贏得2000年甲組聯賽最佳球員獎。該名前鋒首次引起[費格遜爵士的注意是於](../Page/費格遜爵士.md "wikilink")2001年1月7日主場對[曼聯的足總杯賽事](../Page/曼聯.md "wikilink")，沙夏讓曼聯後防疲於奔命，最終紅魔鬼以2-1勝出晉級。

於下一季沙夏和富咸於揭幕戰作客曼聯以高姿態宣告他們登陸超級聯賽。沙夏梅開二度不過曼聯憑將士用命最終以3-2擊敗該支升班馬。不過沙夏該季之後表現令人失望只取得9個入球，其中7球為聯賽入球。

不過於[2003至2004年球季上半季沙夏入球停不了](../Page/2003年至2004年英格蘭超級聯賽.md "wikilink")，致使曼聯付出1,280萬英鎊將他收購並簽約5年\[1\]。於簽約時沙夏表示：「我非常高興。這是夢想成真。我希望多謝我家人和支持我的朋友，我為這結果感到高興。要離開氣氛良好的富咸並不容易，不過我慶幸能加盟世界上最佳球會之一的曼聯。」

而他加盟曼聯後，獲派9號球衣，這也是自高爾於2001年12月轉會後，未有人穿著的球衣號碼。

不過，在曼联的四年半时间内，沙夏不斷受到傷患困擾，多次傳出將被曼聯出售或用來作交易球員的籌碼。2008年8月，他最终离开球队，加盟[埃弗顿](../Page/埃弗顿足球俱乐部.md "wikilink")\[2\]。他共为曼联上场124次（其中替补48次），攻入42个入球（替补入球4个）。

2008年9月1日，沙夏正式加盟[愛華頓](../Page/愛華頓.md "wikilink")，轉會費金額並沒有透露，合約為期2年另[愛華頓有優先權可延長合約多](../Page/愛華頓.md "wikilink")1年\[3\]。

2010年2月5日，[愛華頓與沙夏簽署一份為](../Page/愛華頓.md "wikilink")2年新合約，令沙夏可留隊至2012年\[4\]。

2012年2月1日，沙夏以免轉會費形式加盟[英超](../Page/英超.md "wikilink")[熱刺](../Page/熱刺.md "wikilink")，合約為期18個月\[5\]。

2012年5月29日，沙夏加盟[熱刺只有](../Page/熱刺.md "wikilink")5個月就被放棄，可自由轉會\[6\]。

2012年8月16日，沙夏以自由身身份加盟[新特蘭](../Page/桑德兰足球俱乐部.md "wikilink")，合約為期1年\[7\]。

2013年1月17日，傳媒佈導[新特蘭已通知沙夏可隨時離隊](../Page/桑德兰足球俱乐部.md "wikilink")\[8\]。

2013年1月31日，[新特蘭正式宣佈沙夏已離隊](../Page/桑德兰足球俱乐部.md "wikilink")\[9\]。

2013年2月7日，[意甲球隊](../Page/意甲.md "wikilink")[拉齐奥宣佈簽入沙夏至球季結束](../Page/拉齐奥足球俱乐部.md "wikilink")，原因是[拉齐奥首席射手](../Page/拉齐奥足球俱乐部.md "wikilink")[德國國脚](../Page/德國國家足球隊.md "wikilink")[-{zh-cn:克洛泽;zh-hk:高美斯;}-膝部受傷估計最少休息八星期](../Page/米罗斯拉夫·克洛泽.md "wikilink")，為免影響爭取下季參賽[歐聯資格](../Page/歐聯.md "wikilink")，故簽入沙夏\[10\]。

2013年8月8日，沙夏由[Twitter上宣佈結束他的職業足球員生涯](../Page/Twitter.md "wikilink")。\[11\]

### 國家隊

2004年2月沙夏首度代表[法國國家隊上陣](../Page/法國國家足球隊.md "wikilink")，並立即取得入球，協助球隊以2-0擊敗[比利時](../Page/比利時國家足球隊.md "wikilink")。他亦有入選[2006年世界盃法國的](../Page/2006年世界盃.md "wikilink")23人大軍名單，可惜於四強對[葡萄牙一役當中累積了兩面黃牌](../Page/葡萄牙國家足球隊.md "wikilink")，無緣出戰決賽賽事，最終法國獲得亞軍。

## 榮譽

### 球會

  - 富咸

<!-- end list -->

  - [英格蘭甲組聯賽冠軍](../Page/英格蘭足球聯賽.md "wikilink")：2000/01年；

<!-- end list -->

  - 曼联

<!-- end list -->

  - [英格兰足球超级联赛冠軍](../Page/英格兰足球超级联赛.md "wikilink")
    (2)：[2006/07年](../Page/2006年至2007年英格蘭超級聯賽.md "wikilink")、[2007/08年](../Page/2007年至2008年英格蘭超級聯賽.md "wikilink")；
  - [英格蘭聯賽盃冠軍](../Page/英格蘭聯賽盃.md "wikilink")：2006年；
  - [歐聯冠軍](../Page/歐聯.md "wikilink")：[2007/08年](../Page/2008年欧洲冠军联赛决赛.md "wikilink")；

<!-- end list -->

  - 愛華頓

<!-- end list -->

  - [英格蘭足總盃亞軍](../Page/英格蘭足總盃.md "wikilink")：[2009年](../Page/2009年英格蘭足總杯決賽.md "wikilink")；

### 國家隊

  - 法國

<!-- end list -->

  - [世界盃亞軍](../Page/世界盃足球賽.md "wikilink")：[2006年](../Page/2006年世界盃足球賽.md "wikilink")；

## 註腳

## 外部連結

  - [簡介及統計](http://www.footballdatabase.com/site/players/index.php?dumpPlayer=210)

[Category:瓜德羅普裔法國人](../Category/瓜德羅普裔法國人.md "wikilink")
[Category:巴黎人](../Category/巴黎人.md "wikilink")
[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:梅斯球員](../Category/梅斯球員.md "wikilink")
[Category:紐卡素球員](../Category/紐卡素球員.md "wikilink")
[Category:富咸球員](../Category/富咸球員.md "wikilink")
[Category:曼聯球員](../Category/曼聯球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:熱刺球員](../Category/熱刺球員.md "wikilink")
[Category:新特蘭球員](../Category/新特蘭球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")

1.  [Man Utd seal Saha
    deal](http://news.bbc.co.uk/sport2/hi/football/teams/m/man_utd/3412371.stm)
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.