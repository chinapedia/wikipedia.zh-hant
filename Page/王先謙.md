**王先謙**（），字**益吾**，号**葵园**，[湖南](../Page/湖南.md "wikilink")[長沙人](../Page/長沙.md "wikilink")，中國近代著名經學家。同治进士，官至[國子監](../Page/國子監.md "wikilink")[祭酒](../Page/祭酒.md "wikilink")。

## 生平

十八歲補廪膳生。咸丰十一年（1861年）赴[安徽](../Page/安徽.md "wikilink")[安庆任长江水师向导营书记](../Page/安庆.md "wikilink")，数月之后辞归。同治三年（1864年），在湖北提督[梁洪胜营充任幕僚](../Page/梁洪胜.md "wikilink")。同年中举人。次年联捷[進士](../Page/進士.md "wikilink")。历任[国史馆](../Page/国史馆.md "wikilink")[编修](../Page/编修.md "wikilink")、[翰林院](../Page/翰林院.md "wikilink")[侍读](../Page/侍读.md "wikilink")、[国子监](../Page/国子监.md "wikilink")[祭酒等职](../Page/祭酒.md "wikilink")。其博覽古今群書，治學重考據，[光绪時为](../Page/光绪.md "wikilink")[江苏](../Page/江苏.md "wikilink")[学政](../Page/学政.md "wikilink")，捐银一千两，设[南菁书院](../Page/南菁书院.md "wikilink")。[光绪十五年](../Page/光绪.md "wikilink")（1889年）辞官归里，回[长沙定居](../Page/长沙.md "wikilink")，任[岳麓書院](../Page/岳麓書院.md "wikilink")[山長達十年](../Page/山長.md "wikilink")。在[戊戌变法运动中](../Page/戊戌变法.md "wikilink")，反对[康有为](../Page/康有为.md "wikilink")、[梁启超的激进思想](../Page/梁启超.md "wikilink")。光绪二十六年（1900年），[唐才常](../Page/唐才常.md "wikilink")[自立军事](../Page/自立军.md "wikilink")，王先谦与[叶德辉向](../Page/叶德辉.md "wikilink")[湖南巡抚告密](../Page/湖南巡抚.md "wikilink")，唐黨被殺百余名。

[光绪二十八年](../Page/光绪.md "wikilink")（1902年），与[龙湛霖发起成立](../Page/龙湛霖.md "wikilink")[湖南炼矿总公司](../Page/湖南炼矿总公司.md "wikilink")，参与[粤汉铁路废约自办运动和](../Page/粤汉铁路废约自办运动.md "wikilink")[保路运动](../Page/保路运动.md "wikilink")。[光绪三十四年](../Page/光绪.md "wikilink")（1908年），授以[内阁学士衔](../Page/内阁学士.md "wikilink")。

[宣统二年](../Page/宣统.md "wikilink")（1910年），長沙爆发饥民[抢米风潮](../Page/抢米风潮.md "wikilink")，王先谦联合长沙士绅批评湖南巡抚，事后以“梗议义粜”罪名被降五级。

宣统三年（1911年），[辛亥革命](../Page/辛亥革命.md "wikilink")，乃改名遙，避禍隱居平江鄉野，閉門著書。民国三年（1914年）返长沙。

民國六年（1917）卒于长沙，年七十五。

## 學術

王先谦学术成就最大的方面是史学，著作包括《[漢書補注](../Page/漢書補注.md "wikilink")》、《[後漢書集解](../Page/後漢書集解.md "wikilink")》、《新旧唐书合注》、《十朝[东华录](../Page/东华录.md "wikilink")》、《[水經注合箋](../Page/水經注合箋.md "wikilink")》、《[荀子集解](../Page/荀子集解.md "wikilink")》、《[莊子集解](../Page/莊子集解.md "wikilink")》、《[詩三家義集疏](../Page/詩三家義集疏.md "wikilink")》等。其中最著名的是《漢書補注》和《後漢書集解》，乃是博采众长，集大成者，学术价值已经超过了[颜师古](../Page/颜师古.md "wikilink")、[李贤](../Page/李贤_\(唐\).md "wikilink")、[惠栋的评注](../Page/惠栋.md "wikilink")，被认为代表了注释《[汉书](../Page/汉书.md "wikilink")》和《[后汉书](../Page/后汉书.md "wikilink")》的最高成就。后代史学史专家评论“王氏所作补注、集解诸作，都是校注书中标准的著作。”

王先谦同时又是经学大师。繼[阮元之後輯](../Page/阮元.md "wikilink")《[續皇清經解](../Page/皇清經解續編.md "wikilink")》，[光绪十四年六月](../Page/光绪.md "wikilink")，全书辑刊完成。又繼[姚鼐之後編](../Page/姚鼐.md "wikilink")《[續古文辭類纂](../Page/續古文辭類纂.md "wikilink")》，[王闓運說他](../Page/王闓運.md "wikilink")“續編《皇清經解》，縱未能抗行芸台（阮元），續編《古文辭類纂》差足以肩惜抱（姚鼐）”。

在语言文字学方面，王先谦也颇有造诣，撰写了《释名疏证补》。此外，一生所作诗文共有44卷，1190篇，蔚蔚大观。
所著《虚收堂文集十六卷》和《虚收堂诗存十六卷》收入《续修四库全书》。

## 評價

  - [李肖聃](../Page/李肖聃.md "wikilink")《星廬筆記》說，王先謙的著述，不外“續、纂、選、輯”四種，又說王先謙本人不審外文，《五洲通鉴》、《外国地志》、《日本源流考》诸书，全是请助手编纂。

<!-- end list -->

  - 王先謙曾說：“學者苦志身後之名，後來者當共惜之。”[皮锡瑞高度评价王先谦的](../Page/皮锡瑞.md "wikilink")《尚书孔传参正》说，该书“兼疏今古文，说明精确，最为善本。”李肖聃在《湘学略·葵园学略》中赞他“上笺辟经，下征国史，旁论文章，用逮谱子。四十余年，楚学生光。”又說“长沙阁学，季清巨儒，著书满门，门庭广大”。非是过誉之辞，堪当确评。

<!-- end list -->

  - [李伯元](../Page/李伯元.md "wikilink")《[南亭笔记](../Page/南亭笔记.md "wikilink")》記載，[叶德辉刻意巴结王先谦](../Page/叶德辉.md "wikilink")，兩人狼狽為奸，喜欢干预地方事务，“久住省垣，广通声气，凡同事者无不仰其鼻息，供其指使，一有拂意，则必设法排出之而后快。”因此有人稱他是「楚学泰斗」，也有人骂他是「学匪」、「土霸王」。

## 外部链接

  - [王先謙的著作](http://ctext.org/library.pl?if=gb&author=%E7%8E%8B%E5%85%88%E8%AC%99)
    - [中國哲學書電子化計劃](../Page/中國哲學書電子化計劃.md "wikilink")
  - [史学大家王先谦](http://www.guoxue.com/master/wangxianqian/wxq01.htm)
  - [王先谦](https://web.archive.org/web/20150604085148/http://www.hnfnu.edu.cn/News.aspx?nid=10873)
    湖南第一師範學院
  - 罗志田：〈[思想观念与社会角色的错位:戊戌前后湖南新旧之争再思——侧重王先谦与叶德辉](http://www.nssd.org/articles/article_read.aspx?id=1002096104)〉。

[category:長沙人](../Page/category:長沙人.md "wikilink")

[Category:同治三年甲子科舉人](../Category/同治三年甲子科舉人.md "wikilink")
[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝國子監祭酒](../Category/清朝國子監祭酒.md "wikilink")
[Category:清朝江蘇學政](../Category/清朝江蘇學政.md "wikilink")
[Category:清朝经学家](../Category/清朝经学家.md "wikilink")
[Category:清朝歷史學家](../Category/清朝歷史學家.md "wikilink")
[Category:清朝儒學學者](../Category/清朝儒學學者.md "wikilink")
[X](../Category/王姓.md "wikilink")
[Category:湖南第一师范学院校长](../Category/湖南第一师范学院校长.md "wikilink")