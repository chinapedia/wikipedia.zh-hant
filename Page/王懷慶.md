[Wang_Huaiqing.jpg](https://zh.wikipedia.org/wiki/File:Wang_Huaiqing.jpg "fig:Wang_Huaiqing.jpg")》中的王懷慶照片\]\]
[Wang_Huaiqing2.jpg](https://zh.wikipedia.org/wiki/File:Wang_Huaiqing2.jpg "fig:Wang_Huaiqing2.jpg")

**王懷慶**（），[字](../Page/字.md "wikilink")**懋宣**，[直隷省](../Page/直隷省.md "wikilink")[河間府](../Page/河間府.md "wikilink")[寧晋縣人](../Page/寧晋縣.md "wikilink")，[清末](../Page/清.md "wikilink")[民初軍事將領](../Page/中華民国.md "wikilink")。

## 生平

王懷慶早年曾為牧童。1896年（[光緒](../Page/光緒.md "wikilink")22年）入伍，1900年（光緒26年）成爲[袁世凱的部下](../Page/袁世凱.md "wikilink")。1905年（光緒31年），任北洋常備軍騎兵第2協協統。1907年（光緒33年），任[東三省督署軍務處会辦兼奉天中路統領](../Page/東三省.md "wikilink")。1909年（[宣統元年](../Page/宣統.md "wikilink")），改任[淮軍統領](../Page/淮軍.md "wikilink")、通永鎮總兵。

1912年（[民国元年](../Page/民国紀元.md "wikilink")）1月，王懷慶被推舉為[灤州都督](../Page/灤州.md "wikilink")，残酷镇压了[滦州起义并诱杀起义领导人](../Page/滦州起义.md "wikilink")[王金铭](../Page/王金铭.md "wikilink")、[施从云](../Page/施从云.md "wikilink")、[白毓昆](../Page/白毓昆.md "wikilink")。3月，任[天津鎮總兵兼](../Page/天津市.md "wikilink")[密雲鎮守使](../Page/密雲縣.md "wikilink")、6月任薊榆鎮守使、多倫鎮守使。1914年（民国3年）9月，任冀南鎮守使。1915年（民国4年）5月，兼管外火器營事務及清理京城官戶處督辦。同年12月，袁世凱稱帝，获封二等男。

袁世凱死後的1916年（民国5年）6月，王懷慶任[大名鎮守使](../Page/大名縣.md "wikilink")，此後成爲[直系成員](../Page/直系.md "wikilink")。1918年（民国7年）2月任幫辦直隷軍務，翌年5月任步兵統領兼陸軍第13師師長。1920年（民国9年）7月的[直皖戰争中](../Page/直皖戰争.md "wikilink")，任京畿衛戌司令。1922年（民国11年）5月被任命為[熱河都統](../Page/熱河省.md "wikilink")，但仍任京畿衛戌司令（由熱河幇辦[米振標代理都統](../Page/米振標.md "wikilink")）。

1924年（民国13年）[第二次直奉戰争中](../Page/第二次直奉戰争.md "wikilink")，王懷慶被[吳佩孚委任為討逆軍第](../Page/吳佩孚.md "wikilink")2軍總司令，后又任陆军检阅使兼西北邊防督辦。後來敗於[奉軍](../Page/奉軍.md "wikilink")，被迫下野。1926年（民国15年）4月吳佩孚重新掌權，王懷慶任北京警衛司令（由衛戌司令更名而來）。此後在奉系壓迫下，同年11月他被迫辞任。

此後，王懷慶自軍政界引退，寓居天津。[抗日戰争期间日本軍支配華北之際](../Page/抗日戰争.md "wikilink")，王懷慶被任命為京漢路治安軍總司令，但沒有就任。

1953年，王怀庆在天津病逝。享年78嵗。

## 其他

  - 1919年秋，王懷慶僱用民夫偷盜[圓明園石料建立](../Page/圓明園.md "wikilink")[達園](../Page/達園.md "wikilink")，佔地12萬平方米。該園位于今[北京市](../Page/北京市.md "wikilink")[海淀区原](../Page/海淀区.md "wikilink")[海淀鎮北](../Page/海淀鎮.md "wikilink")1公里處，俗稱“王懷慶花園”。
  - 王懷慶生平喜好馬桶，常坐在馬桶上辦公，人稱「馬桶將軍」。

## 影视

  - [魏宗万](../Page/魏宗万.md "wikilink")：1991年[中國電影](../Page/中國電影.md "wikilink")：《[开天辟地](../Page/开天辟地.md "wikilink")》

## 註釋

## 参考文献

  -
  -
  -
## 外部链接

{{-}}

<table>
<thead>
<tr class="header">
<th><p>（<a href="../Page/北京政府.md" title="wikilink">北京政府</a>）     </p></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

[Category:淮軍将领](../Category/淮軍将领.md "wikilink")
[Category:直军将领](../Category/直军将领.md "wikilink")
[Category:通永鎮總兵](../Category/通永鎮總兵.md "wikilink")
[Category:天津鎮總兵](../Category/天津鎮總兵.md "wikilink")
[Category:密雲鎮守使](../Category/密雲鎮守使.md "wikilink")
[Category:薊榆鎮守使](../Category/薊榆鎮守使.md "wikilink")
[Category:多倫鎮守使](../Category/多倫鎮守使.md "wikilink")
[Category:冀南鎮守使](../Category/冀南鎮守使.md "wikilink")
[Category:寧晋人](../Category/寧晋人.md "wikilink")
[H](../Category/王姓.md "wikilink")