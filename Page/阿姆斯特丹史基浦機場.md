**阿姆斯特丹史基浦機場**（；）是[荷蘭首都](../Page/荷蘭.md "wikilink")[阿姆斯特丹的](../Page/阿姆斯特丹.md "wikilink")[主機場](../Page/機場.md "wikilink")，也是荷蘭主要的進出門戶。座落於阿姆斯特丹西南方的市郊，史基浦機場實際上位於[北荷蘭省](../Page/北荷蘭省.md "wikilink")[哈勒默梅爾](../Page/哈勒默梅爾.md "wikilink")（Haarlemmermeer）境內，目前擁有五條可以起降大型[民航機的主跑道](../Page/民用航空.md "wikilink")，與一條主要供[通用航空使用的輔助跑道](../Page/通用航空.md "wikilink")。

## 營運歷史

史基浦機場是[荷蘭皇家航空與其子公司](../Page/荷蘭皇家航空.md "wikilink")[馬丁航空](../Page/馬丁航空.md "wikilink")、[泛航航空的](../Page/泛航航空.md "wikilink")[枢纽機場](../Page/枢纽機場.md "wikilink")，而向來與[荷蘭皇家航空有深厚合作關係的](../Page/荷蘭皇家航空.md "wikilink")[美國](../Page/美國.md "wikilink")[西北航空](../Page/西北航空.md "wikilink")（後被[達美航空併購成為其一部分](../Page/達美航空.md "wikilink")）也以該機場作為在[歐洲地區的轉運樞紐](../Page/歐洲.md "wikilink")，因此每年都有大批旅客以該機場作為進入[歐陸地區的入口點](../Page/歐陸.md "wikilink")。依照2013年的統計，史基浦機場的旅客吞吐量达到了52,569,250人次，僅次於[倫敦希斯洛機場](../Page/倫敦希斯洛機場.md "wikilink")、[巴黎戴高樂機場與](../Page/巴黎戴高樂機場.md "wikilink")[法蘭克福國際機場](../Page/法蘭克福國際機場.md "wikilink")，排在歐洲第4位，世界[第14位](../Page/全世界最繁忙的机场_\(客量\).md "wikilink")。

2007年5月16日，史基浦機場在（Nationaal Coördinator
Terrorismebestrijding，縮寫為NCTb，已改名國家安全與反恐協調會NCTV）及荷蘭[海關單位的共同協助下](../Page/海關.md "wikilink")，成為世界上第一個使用[人體掃瞄安檢儀檢查過往旅客是否有攜帶違禁物品的機場](../Page/人體掃瞄安檢儀.md "wikilink")，大幅提升了機場[安檢的效率](../Page/安全檢查.md "wikilink")。

此機場位處於海平面以下，高度為海拔負3米（約負11呎），因此飛機降落時，因儀器未能顯示正確之水平，而須由機師以肉眼觀察。

## 航空公司及航点

## 空難

[以色列航空](../Page/以色列航空.md "wikilink")1862

## 註釋

## 參考文獻

  - Heuvel, Coen van den. Schiphol, een Wereldluchthaven in Beeld,
    Holkema & Warendorf, 1992, 978-9-0269-6271-4

## 外部链接

  - [阿姆斯特丹史基浦機場官方網站](http://www.schiphol.nl)
  - [荷兰国家旅游会议促进局官方网站](http://www.holland.com)
  - [Fire Brigade Amsterdam Airport
    Schiphol](http://www.brandweerschiphol.nl/)
  - [Live air traffic tracking on
    Schiphol](https://web.archive.org/web/20140723224111/http://www.casper.frontier.nl/)
    using Casper system integrated with [Google
    Maps](../Page/Google_Maps.md "wikilink")

[Category:荷兰机场](../Category/荷兰机场.md "wikilink")
[Category:阿姆斯特丹交通](../Category/阿姆斯特丹交通.md "wikilink")
[Category:1916年启用的机场](../Category/1916年启用的机场.md "wikilink")