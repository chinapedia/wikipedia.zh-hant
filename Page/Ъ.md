__NOTOC__
是一个[西里尔字母](../Page/西里尔字母.md "wikilink")。它在现代[俄语中称呼为](../Page/俄语.md "wikilink")
<span lang="ru">твёрдый знак</span>（*tvjordyj
znak*，即“硬音符号”），在[保加利亚语中称呼为](../Page/保加利亚语.md "wikilink")
<span lang="bg">ер голям</span>（*er goljam*; 大
jer）。这个字母在[古教会斯拉夫语和](../Page/古教会斯拉夫语.md "wikilink")[古俄语以及改革前的俄语中称为](../Page/古俄语.md "wikilink")“<span lang="ru">ер
обратный</span>”（*jer obratnyj*; 后
jer）。最初这个字母是代表非常短的或非重读的[中](../Page/中元音.md "wikilink")[圆唇元音](../Page/圆唇元音.md "wikilink")(//)。与Ъ/ъ相对应的是被叫做“前
jer”的[Ь](../Page/Ь.md "wikilink")/ь，也就是现代俄语中的软音符号或保加利亚语的“<span lang="bg">ер
малък</span>”（*er malyk*; 小
jer）。Ъ和Ь本来都是音值较弱的元音，后来音值逐渐消失，甚至前者在现代多种[斯拉夫语言中已经完全消失](../Page/斯拉夫语族.md "wikilink")，或仅作隔音符号；后者现在在几乎所有斯拉夫语中用于标记[辅音的](../Page/辅音.md "wikilink")[颚化](../Page/颚化.md "wikilink")（或称软化），除了在[塞尔维亚语和](../Page/塞尔维亚语.md "wikilink")[马其顿语中只出现在连体字母](../Page/马其顿语.md "wikilink")[Њ](../Page/Њ.md "wikilink")、[Љ而非独立存在](../Page/Љ.md "wikilink")。

## 俄语

### 古俄语：еръ

在古俄语和中古俄语中，在弱读时完全不发音，重读时会被非弱化元音所取代。因此现代俄语的屈折变化中有时会在原本Ъ发音或不发音的位置上用一个短元音取代。比如：

  - 古俄语   → 现代俄语   “睡觉”（第一格单数）
  - 古俄语   → 现代俄语   “睡觉”（第二格单数）
  - 古俄语   → 现代俄语   “角度”（第一格单数）
  - 古俄语   → 现代俄语   “角度”（第二格单数）

<!-- end list -->

  -
    第一格又叫[主格](../Page/主格.md "wikilink")，第二格又叫[属格](../Page/属格.md "wikilink")。

<span lang="ru"> Ъ和Ь所共用的的基本读音和弱化规则如下：

  - 重读时ъ读作о，Ь读作е或ё
  - 弱读时不发音，除了在ь表示软音记号时。
  - 值得注意的是无论ъ或ь被重读还是弱读，都有音节分割的功能。
  - 词末的ъ或ь弱读。
  - 跟随一个非弱化元音时为弱读ъ或ь
  - 弱读的ъ或ь前的ъ或ь为重读，重读的ъ或ь前的ъ或ь为弱读。

</span>

在1889年，捷克学者
(1855-1925)发现了他命名的ъ或ь的强弱发音判断规则。请参见[哈夫利克定律](../Page/哈夫利克定律.md "wikilink")()。

一般来说，大部分来自古俄语的含有ъ的单词，到了现代俄语中要么ъ变成元音，要么消失，尤其是词末的ъ。少数例外的单词还保留着ъ，但仅用于音节分割。这种例外被普遍认为是用于与某些形近字区别开来。词末的ъ的实际发音大约在15世纪时已经消亡，而19世纪时更完全消失，比如（人类）的读音变成了（人类）。[弗拉基米尔·达尔在](../Page/弗拉基米尔·达尔.md "wikilink")1880年到1882年间编纂的字典的前言中提到：

字母ъ最终在[布尔什维克政权上台后的](../Page/布尔什维克.md "wikilink")[1917年俄语正写法改革中被废除](../Page/1917年俄语正写法改革.md "wikilink")（随后又恢复使用），当时正处于[俄国内战](../Page/俄国内战.md "wikilink")。为了鼓励[彼得格勒那些顽固的印刷商使用新式字母表](../Page/彼得格勒.md "wikilink")，驻守当地的[红海军](../Page/红海军.md "wikilink")[波罗的海舰队派兵没收所有印刷商手上带有这个字母](../Page/波罗的海舰队.md "wikilink")（当局称之为“字母的寄生虫”）的印刷模具\[1\]
\[2\]。印刷商不得不用非标准的撇号（这个符号不是俄语的标点符号）来代替单词中用于分割音节的ъ。比如：

  - 改革前:
  - 改革期间:
  - 改革完成:

<!-- end list -->

  -
    以上是现代俄语中（会合、集中）在改革前后的写法。

内战过后ъ逐渐被恢复用作隔音符号，而改革期间临时使用的撇号在一些没有
的[打字机或印刷模具上仍旧使用](../Page/打字机.md "wikilink")，但相当罕见。

根据[列夫·乌斯品斯基的支持俄语改革的流行语言学著作](../Page/列夫·乌斯品斯基.md "wikilink")《关于文字的文字》（）的粗略估计，词末的ъ大约占了印刷文本中字母数量的3.5%，这意味着这个不发音的字母浪费了大量纸张，应从经济学的角度出发废除这个字母。印刷商最初不赞成布尔什维克对这个字母的废除，因而他们在改革后的一段时间仍坚持使用改革前的拼写法，但最后还是适应了改革后的新拼写方式。

现在词末的ъ有时用于商标名，比如著名的《生意人报》（），这种用法有时是象征对保守的俄罗斯价值观的认可，或者是表示幽默，但这些用法不一定符合改革前的ъ拼读规则\[3\]。一些以过去的俄罗斯为题材的当代俄罗斯艺术作品也会在标题中用上词末的ъ，如2008年俄罗斯反映俄罗斯海军上将[亚历山大·瓦西里耶维奇·高尔察克作为黑海舰队总司令的战斗生涯的电影](../Page/高尔察克.md "wikilink")《海军上将》（）。

### 现代俄语：硬音符号

“ъ”在现代俄语中被称为硬音符号（），本身不发音，纯粹是拼写法的工具。其功能为把辅音与后面由一个软元音开头的音素分离，因此这个字母通常出现在""、""、""和""
（在俄语和保加利亚语中读作*ja*、*jo*、*je*和*ju*）前。硬音符号能使这4个字母发音所带的音被当作重读发出。例如：

  - ：取下、拍摄、拓印下来、抄下来（动词完成体）

  - ：男性人名（Simon）的昵称

因此，这个字母等同于一个“分隔符”，自1918年改革后其用途仅限于一些特定情况（如区别上面提及的）。然而事实上硬音符号前的辅音字母在某个程度上会被后面的软元音所软化（颚化），因此在20世纪初苏俄政府内曾经存在计划打算彻底消灭硬音符号，以软音符号ь取而代之。不过部分由于ъ前元音软化并不是被广泛接受的发音规则，因此这个计划没有被执行。在现代俄语，ъ既存在于本土词汇，也存在于外来词的前缀。近年来，ъ有时用于外来单词的字母и前，使构成单词的音节分割得更明显，但这种用法没有正式地被写入字典中。

详见[俄语正寫法和](../Page/俄语正寫法.md "wikilink")[俄语正音法](../Page/俄语正音法.md "wikilink")。

## 保加利亚语

在[保加利亚语中](../Page/保加利亚语.md "wikilink")，（称作大Jer）为
（[半閉後不圓唇元音](../Page/半閉後不圓唇元音.md "wikilink")），音值相當於[現代標準漢語的](../Page/現代標準漢語.md "wikilink")
e， 有时也用作代表 。

保加利亚语中由于没有元音前软化（颚化）的规则，因此没有硬音符号，但在一些古老的单词会存在用撇号作类似硬音符号的音节分隔符。

## 白俄罗斯语和乌克兰语

这个字母在[白俄罗斯语和](../Page/白俄罗斯语.md "wikilink")[乌克兰语字母表中不存在](../Page/乌克兰语.md "wikilink")。在白俄罗斯语[西里尔字母中它的功能由撇号或字母](../Page/西里尔字母.md "wikilink")

取代，而在[拉丁字母中則用j来代替](../Page/拉丁字母.md "wikilink")，例如和Siamja。在乌克兰语中取而代之的也是撇号。

## 其他语言

在高加索语言如[车臣语](../Page/车臣语.md "wikilink")、[阿巴札语和](../Page/阿巴札语.md "wikilink")[阿瓦尔语](../Page/阿瓦尔语.md "wikilink")，此字母用作
。

## 转写

ъ 在[转写成拉丁字母时](../Page/转写.md "wikilink")，通常转写作双引号
ʺ（U+02BA），而在古教会斯拉夫语和保加利亚语中则可以是ŭ。

## 字符编码

## 参考文献

<div class="references-small">

<references />

</div>

## 参看

  - <span lang="ru" style="font-size:120%;">[Ь
    ь](../Page/Ь.md "wikilink")</span>（西里尔字母的软音符号）

[Category:西里尔字母](../Category/西里尔字母.md "wikilink")

1.  ["Лексикон" Валерия Скорбилина Архив выпусков программы, «ЛЕКСИКОН»
    № 238, интервью с Натальей Юдиной, деканом факультета русского
    языка и
    литературы](http://www.vladtv.ru/leksikon.shtml?news=131)
2.  *Слово о словах*, Лев Успенский, Лениздат, 1962, p. 156
3.  [Артемий Лебедев, Ководство, § 23. Немного о дореволюционной
    орфографии.](http://www.artlebedev.ru/kovodstvo/sections/23/)