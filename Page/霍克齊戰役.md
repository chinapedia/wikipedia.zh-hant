[La_Pegna_Überfall_bei_Hochkirch.jpg](https://zh.wikipedia.org/wiki/File:La_Pegna_Überfall_bei_Hochkirch.jpg "fig:La_Pegna_Überfall_bei_Hochkirch.jpg")
**霍克齊戰役**（Battle of
Hochkirch），是**七年戰爭**中[普魯士與](../Page/普魯士.md "wikilink")[奧地利在](../Page/奧地利.md "wikilink")1758年10月14日的戰役。以奧軍勝利作結。

在[曹恩道夫戰役後](../Page/曹恩道夫戰役.md "wikilink")，[俄軍暫時後撤](../Page/俄.md "wikilink")。南方戰場又一次告急，[腓特烈大帝馬上回兵南線對付道恩元帥的奧軍](../Page/腓特烈大帝.md "wikilink")。腓特烈率領15,000普軍趕回南線戰場，與弟弟[亨利親王在](../Page/亨利親王.md "wikilink")[薩克森首府](../Page/薩克森.md "wikilink")[德累斯頓會師](../Page/德累斯頓.md "wikilink")，僅以3萬人面對[道恩](../Page/道恩.md "wikilink")[元帥的](../Page/元帥.md "wikilink")8萬奧軍。一開始謹慎的道恩拒絕接受[會戰](../Page/會戰.md "wikilink")，僅僅有限後退，與普軍保持接觸。10月初腓特烈的3萬普軍在[霍克齊紮下營盤](../Page/霍克齊.md "wikilink")，而老謀深算的道恩元帥在10月14日乘夜以8萬大軍成4路[縱隊偷襲普軍營地](../Page/縱隊.md "wikilink")，打響霍克齊戰役。當時腓特烈還在睡覺，根本未加防範，普軍措手不及。道恩以[勞頓](../Page/勞頓.md "wikilink")[將軍](../Page/將軍.md "wikilink")（Loudon）率四個[師搶攻](../Page/師.md "wikilink")，普軍Krockow[少將率](../Page/少將.md "wikilink")[騎兵反攻陣亡](../Page/騎兵.md "wikilink")。[凱斯元帥聞報](../Page/凱斯.md "wikilink")，急忙帶親兵趕往戰場組織抵抗，為普軍主力展開爭取時間，結果凱斯身中兩槍當場陣亡。另一位普魯士元帥，[莫里茨親王也身受重傷](../Page/莫里茨親王.md "wikilink")，被抬下戰場。因為凱斯和莫里茨的抵抗，普軍爭取到時間，好不容易組織起後衛線，收攏殘兵，交替掩護著撤出戰場。這是普魯士的一場大敗，損失9,000人以上，包括兩位元帥。凱斯陣亡之後連屍體都沒能搶回，還是奧地利代為埋葬的。莫里茨親王重傷再也沒有康復，他在戰後被護送回[柏林的途中](../Page/柏林.md "wikilink")，遇到奧軍攔截[被俘](../Page/被俘.md "wikilink")，不久才被腓特烈[贖回](../Page/贖回.md "wikilink")。

霍克齊戰役之後，道恩仍然謹慎，並不積極進攻腓特烈，但是道恩卻是機動高手，腓特烈想繞過道恩，從薩克森東進西里西亞，後來又改變路線突然回軍薩克森，調動道恩，但是每次都被道恩搶先堵在前面。年底道恩回營地過[冬](../Page/冬.md "wikilink")，腓特烈也回到[西里西亞](../Page/西里西亞.md "wikilink")，結束了1758年[戰局](../Page/戰局.md "wikilink")。這一年雖然以失敗告終，但是有賴俄奧作戰不夠積極，腓特烈仍然保住了薩克森和西里西亞兩個[省份](../Page/省份.md "wikilink")。

## 參看

  - [七年戰爭](../Page/七年戰爭.md "wikilink")

## 參考文獻

  - 顧劍 《普魯士腓特烈大帝的生平戰役》
      - Dennis Showalter "The Wars of Frederick the Great" 1996年英文版
      - Christopher Duffy "Frederick the Great: A Military Life"
        1985年英文版
      - Theodore Dodge "Great Captains" 1889年英文版
      - 富勒 《西洋世界軍事史》 1981年中文版

[Category:奥地利战役](../Category/奥地利战役.md "wikilink")
[Category:德国战役](../Category/德国战役.md "wikilink")
[Category:七年戰爭戰役](../Category/七年戰爭戰役.md "wikilink")
[Category:1758年军事冲突](../Category/1758年军事冲突.md "wikilink")