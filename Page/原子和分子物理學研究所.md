[AMOLF_2012.jpg](https://zh.wikipedia.org/wiki/File:AMOLF_2012.jpg "fig:AMOLF_2012.jpg")

**原子和分子物理學研究所**( Institute for Atomic and Molecular Physics
)俗稱**AMOLF**，是由荷兰基础物质研究基金会（ Foundation for Fundamental Research on
Matter，also known as
FOM）运行的三个研究所之一。该研究所是欧洲物理与生物物理领域领先的研究所之一。它现今主要关注两个研究主题：[生物物理和](../Page/生物物理.md "wikilink")[纳米光子学](../Page/纳米光子学.md "wikilink")。原子和分子物理學研究所位在[荷蘭的](../Page/荷蘭.md "wikilink")[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")，專門研究複雜[原子和](../Page/原子.md "wikilink")[分子系統](../Page/分子.md "wikilink")。

## 历史

在1946年，荷兰政府建立了基础物质研究基金会（FOM）“旨在，广泛地推动荷兰境内物质的基础科学研究，以及支持高等教育”。随即，建立了质谱学基础物质实验室（FOM
Laboratory for Mass Spectrography）。1960年，这个研究所的名成被改为质量分离实验室（Laboratory
for Mass Separation），最终于1966年改称原子分子物理研究所（FOM Institute for Atomic and
Molecular Physics or AMOLF）。

最初的研究目标是铀同位素的电磁分离方法，这是一个在第二次世界大战以后具有特别重大战略意义的课题。为了达到这一目标，从质量分离仪器的开发开始，一系列现代分析仪器被发展出来。到了1953年，AMOLF成为欧洲第一个成功提纯铀的研究所。之后很快，开展了气体热扩散的研究，完成了超高速离心，阴极分散，高能离子激发气体和分子束的研究工作。AMOLF研制的气体超高速离心机为位于[阿尔默洛](../Page/阿尔默洛.md "wikilink")（Almelo）当今著名的[URENCO公司商业提纯铀](../Page/URENCO.md "wikilink")，提供了基础。

## 组织与结构

被公认为欧洲交叉学科研究最有声望的研究所之一，AMOLF由研究所所长来领导。[Huib
Bakker接替了](../Page/Huib_Bakker.md "wikilink")[Vinod
Subramaniam](../Page/Vinod_Subramaniam.md "wikilink")，自从2016年2月1日担任所长。\[1\]

## 研究

2007年的主要研究方向在 [光子学](../Page/光子学.md "wikilink"), 飞秒物理学, 以及“生命过程的物理学”。
它坐落在[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")[科学园](../Page/科学园.md "wikilink")（Amsterdam
Science Park），拥有员工大约175人，其中有100位科学家，50位工程师，和25位后勤员工。

## 延伸閱讀

  - [Atomic physics](../Page/Atomic_physics.md "wikilink")

<!-- end list -->

  - **AMOLF的著名学者**
      - [Daan Frenkel](../Page/Daan_Frenkel.md "wikilink")
      - [Ad Lagendijk](../Page/Ad_Lagendijk.md "wikilink")
      - [Albert Polman](../Page/Albert_Polman.md "wikilink")

## 参考文献

## 外部链接

  - [AMOLF 主页](http://www.amolf.nl/)

[Category:荷蘭研究機構](../Category/荷蘭研究機構.md "wikilink")

1.