[NVIDIA_BR02_HSI_High_Speed_Interconnect.jpg](https://zh.wikipedia.org/wiki/File:NVIDIA_BR02_HSI_High_Speed_Interconnect.jpg "fig:NVIDIA_BR02_HSI_High_Speed_Interconnect.jpg")

**BR02**全稱Bridge Revision
02，是[NVIDIA公司隨](../Page/NVIDIA.md "wikilink")[GeForce
PCX推出的接橋晶片](../Page/GeForce_PCX.md "wikilink")\[1\]，其別稱為**High
Speed
Interconnect**（HSI），由[IBM代工](../Page/IBM.md "wikilink")。它可以將[AGP訊号轉為](../Page/AGP.md "wikilink")[PCI-E訊号](../Page/PCI-E.md "wikilink")，亦可將[PCI-E訊号轉為](../Page/PCI-E.md "wikilink")[AGP訊号](../Page/AGP.md "wikilink")。BR02也可与顯示核心一起封裝，以淢低成本，例如[NV45/NV48晶片即是](../Page/GeForce_6.md "wikilink")[NV40晶片與BR](../Page/GeForce_6.md "wikilink")02的整合體。

初期NVIDIA使用此晶片，將AGP顯示卡转为PCI-E接口，推出了[GeForce
PCX系列](../Page/GeForce_PCX.md "wikilink")。但GeForce
PCX基於其並不成功的舊產品[GeForce
FX系列](../Page/GeForce_FX.md "wikilink")，而且通過其轉換效能更會略微損失，所以接橋晶片備受批評。而當時競爭對手[ATi亦挪喻NVIDIA](../Page/ATi.md "wikilink")「有路何需搭橋」，皆因其晶片均為原生，即一個晶片同時推出原生PCI-E與原生AGP版本。

在[GeForce 6及](../Page/GeForce_6.md "wikilink")[GeForce
7時期](../Page/GeForce_7.md "wikilink")，BR02接橋晶片的優勢終於體現出來，NVIDIA可以方便的將原生支援AGP的顯示卡轉換為PCI-E顯示卡，或將原生支援PCI-E的顯示卡轉換為AGP顯示卡以供老平臺使用。最後迫於成本壓力，[ATi亦开發了](../Page/ATi.md "wikilink")[Rialto單向接橋晶片](../Page/Rialto.md "wikilink")。

不同於ATi的Rialto，NVIDIA的BR02由於推出時間較早，無法支援GeForce
8系列及其之后的产品，NVIDIA亦曾打算升級至新的A05版本，但最后NVIDIA認為AGP已面臨淘汰，利潤不高而放棄。所以[GeForce
8及之後的產品並沒有AGP版本推出](../Page/GeForce_8.md "wikilink")。

## 相關條目

  - [BR03接橋晶片](../Page/NVIDIA_BR03.md "wikilink")
  - [BR04接橋晶片](../Page/NVIDIA_BR04.md "wikilink")
  - [Rialto接橋晶片](../Page/Rialto.md "wikilink")

## 參考連結

[Category:英伟达](../Category/英伟达.md "wikilink")

1.  [NVIDIA Introduces Industry's First Top-to-Bottom Family of PCI
    Express GPUs](http://www.nvidia.com/object/IO_11187.html)