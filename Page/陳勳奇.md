**陳勳奇**（**Frankie Chan Fan Kei**，），原名**陳永煜**，香港電影人。

其弟為香港資深男配音員[陳永信](../Page/陳永信.md "wikilink")，其已故女兒為演員[陳杏妍](../Page/陳杏妍.md "wikilink")。

## 獎項

  - [第十五屆香港電影金像獎最佳原創音樂](../Page/第15屆香港電影金像獎#最佳原創音樂.md "wikilink")(陳勳奇、Roel
    A. Garcia)

## 曾參與之作品

### 導演

#### 電視劇集

1.  [梦想的天空](../Page/梦想的天空.md "wikilink")（国内电视剧）（2012）
2.  [功夫小英雄](../Page/功夫小英雄.md "wikilink")（2006）
3.  [群英會](../Page/群英會.md "wikilink")（2003）

#### 電影

1.  [美麗戰爭](../Page/美麗戰爭.md "wikilink")(2017）
2.  [緣來是遊戲](../Page/緣來是遊戲.md "wikilink")(2014）
3.  [楊門女將之軍令如山](../Page/楊門女將之軍令如山.md "wikilink")（2011）
4.  [辣椒教室](../Page/辣椒教室.md "wikilink")（1999）
5.  [上海探戈](../Page/上海探戈.md "wikilink")（1996）
6.  [運財五福星](../Page/運財五福星.md "wikilink")（1996）
7.  [沒有老公的日子](../Page/沒有老公的日子.md "wikilink")（1995）
8.  [神探Power之問米追凶](../Page/神探Power之問米追凶.md "wikilink")（1994）
9.  [沉默的姑娘](../Page/沉默的姑娘.md "wikilink")（1994）
10. [辣椒教室](../Page/辣椒教室.md "wikilink")（1993）
11. [痴情快婿](../Page/痴情快婿.md "wikilink")（1992）
12. [飛鷹計劃](../Page/飛鷹計劃.md "wikilink")（1991）
13. [最佳賊拍檔](../Page/最佳賊拍檔.md "wikilink")（1990）
14. [龍之爭霸](../Page/龍之爭霸.md "wikilink")（1989）
15. [龍虎智多星](../Page/龍虎智多星.md "wikilink")（1988）
16. [鬼馬保鑣賊美人](../Page/鬼馬保鑣賊美人.md "wikilink")（1988）
17. [惡男](../Page/惡男.md "wikilink")（1986）
18. [我要金龜婿](../Page/我要金龜婿.md "wikilink")（1986）
19. [小狐仙](../Page/小狐仙.md "wikilink")（1985）
20. [伊人再見](../Page/伊人再見.md "wikilink")（1984）
21. [空心大少爺](../Page/空心大少爺.md "wikilink")（1983）
22. [佳人有約](../Page/佳人有約.md "wikilink")（1982）

### 演員

1.  梦想的天空 (国内 电视剧) (2012)飾 何兆飞
2.  電視劇集 群英會 (2003) 飾 寧天
3.  辣椒教室 (1999) 飾 歐綠峰
4.  上海探戈 (1998) 飾 任天翔
5.  [神探Power之問米追凶](../Page/神探Power之問米追凶.md "wikilink") (1994)
6.  黑衣部隊之手足情深 (1993) 飾 李恩澤
7.  [邊城浪子](../Page/邊城浪子#改編成電影及電視劇.md "wikilink") (1993)
8.  痴情快婿 (1992) 飾 楊義
9.  最佳賊拍檔 (1990) 飾 占士
10. 龍之爭霸 (1990) 飾 倪志孝
11. 花心夢中人 (1989)
12. 鬼馬保鑣賊美人 (1988)
13. 惡男 (1986)
14. 我要金龜婿 (1986)
15. 小狐仙 (1985)
16. 伊人再見 (1984)
17. 佳人有約 (1982)
18. [提防小手](../Page/提防小手.md "wikilink") (1982)
19. [敗家仔](../Page/敗家仔_\(1981年電影\).md "wikilink") (1981) 飾 倪飛
20. 孖寶闖八關 (1980)

### 監製

1.  龍之爭霸 (1989)
2.  花心夢中人 (1989)
3.  鬼馬保鑣賊美人 (1988)
4.  皇家飯 (1986)
5.  我要金龜婿 (1986)
6.  小狐仙 (1985)
7.  伊人再見 (1984)
8.  提防小手 (1982)

### 編劇

1.  邊城浪子 (1993)
2.  鬼馬保鑣賊美人 (1988)
3.  我要金龜婿 (1986)
4.  空心大少爺 (1983)
5.  電視劇集 群英會 (2003)

### 電影配樂

1.  勇者無懼 (1981)
2.  有料到 (1980)
3.  [師弟出馬](../Page/師弟出馬.md "wikilink") (1980)
4.  梁天來 (1979)
5.  生死門 (1979)
6.  油脂曱甴 (1979)
7.  螳螂 (1978)
8.  愛欲狂潮 (1978)
9.  [少林三十六房](../Page/少林三十六房.md "wikilink") (1978)
10. [楚留香](../Page/楚留香_\(1977年電影\).md "wikilink") (1977)
11. 陸阿采與黃飛鴻 (1976)
12. 紅孩兒 (1975)
13. [刺馬](../Page/刺馬_\(電影\).md "wikilink") (1973)
14. [七十二家房客](../Page/七十二家房客_\(1973年電影\).md "wikilink") (1973)
15. [大刀王五](../Page/大刀王五.md "wikilink") (1973)
16. 林沖夜奔 (1972)

### 動作/武術指導

1.  黑衣部隊之手足情深 (1993)(動作導縯)
2.  [霹靂火](../Page/霹靂火_\(1995年電影\).md "wikilink") (1995)

### 作曲

1.  [天下無雙](../Page/天下無雙.md "wikilink") (2002)
2.  [冒險王](../Page/冒險王.md "wikilink") (1996)
3.  [烈火戰車](../Page/烈火戰車.md "wikilink") (1995)
4.  [墮落天使](../Page/墮落天使.md "wikilink") (1995)
5.  [東邪西毒](../Page/東邪西毒.md "wikilink") (1994)
6.  [重慶森林](../Page/重慶森林.md "wikilink") (1994)

## 外部連結

  -
  -
  -
[Category:香港電影金像獎最佳電影配樂](../Category/香港電影金像獎最佳電影配樂.md "wikilink")
[Category:香港電影金像獎最佳原創電影音樂得主](../Category/香港電影金像獎最佳原創電影音樂得主.md "wikilink")
[X](../Category/陳姓.md "wikilink")
[Category:20世紀男演員](../Category/20世紀男演員.md "wikilink")
[Category:20世紀導演](../Category/20世紀導演.md "wikilink")
[Category:21世紀導演](../Category/21世紀導演.md "wikilink")
[Category:21世紀男演員](../Category/21世紀男演員.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:動作片演員](../Category/動作片演員.md "wikilink")
[Category:香港電影導演](../Category/香港電影導演.md "wikilink")
[Category:電影演員兼導演](../Category/電影演員兼導演.md "wikilink")
[Category:香港電影監製](../Category/香港電影監製.md "wikilink")
[Category:香港電影配樂家](../Category/香港電影配樂家.md "wikilink")
[Category:黃大仙天主教小學校友](../Category/黃大仙天主教小學校友.md "wikilink")