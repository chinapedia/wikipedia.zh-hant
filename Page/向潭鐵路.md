[Schematic_map_of_Xiangtang_railway_hub.svg](https://zh.wikipedia.org/wiki/File:Schematic_map_of_Xiangtang_railway_hub.svg "fig:Schematic_map_of_Xiangtang_railway_hub.svg")
**向潭铁路**是[中国](../Page/中国.md "wikilink")[江西省](../Page/江西省.md "wikilink")[南昌市](../Page/南昌市.md "wikilink")[南昌縣](../Page/南昌縣.md "wikilink")[向塘鎮](../Page/向塘鎮.md "wikilink")[向塘西站通往](../Page/向塘西站.md "wikilink")[南昌縣](../Page/南昌縣.md "wikilink")[广福镇](../Page/广福镇.md "wikilink")[潭崗村](../Page/潭崗村.md "wikilink")[潭崗站的铁路](../Page/潭崗站.md "wikilink")，長11千米，原為[浙赣铁路的一部份](../Page/浙赣铁路.md "wikilink")。由於浙贛铁路電力化改造中對[梁家渡站至潭崗站一段進行了新建取直](../Page/梁家渡站.md "wikilink")；自2006年8月13日梁家渡至潭岗直通线特大桥线路启用後，浙赣铁路定線不再繞經向塘西站，而是採用新的直通线特大桥，原有線路段（向塘西站至梁家渡站一段）被取締並新名命為[向梁铁路](../Page/向梁铁路.md "wikilink")、（向塘西站至潭崗站一段）被取締並新名命為向潭铁路。

向潭铁路沿線有向塘西站、江家站和潭崗站三個車站，當中有一小段與[京九鐵路並行](../Page/京九鐵路.md "wikilink")。

## 外部链接

  - [向潭鐵路线路图](http://www.openstreetmap.org/browse/relation/3190113) —
    在[Openstreetmap_logo.svg](https://zh.wikipedia.org/wiki/File:Openstreetmap_logo.svg "fig:Openstreetmap_logo.svg")[OpenStreetMap](../Page/OpenStreetMap.md "wikilink")

[Category:江西省铁路](../Category/江西省铁路.md "wikilink")
[Category:中国铁路线](../Category/中国铁路线.md "wikilink")