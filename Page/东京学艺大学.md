<div style="float: right; margin: 0 0 1em 2em; width: 25em; text-align: right; font-size: 0.86em; line-height: normal;">

<div style="border: 1px solid #ccd2d9; background: #f0f6fa; text-align: left; padding: 0.5em 1em; text-align: center;">

<big>**东京学艺大学**</big>

`   `

<table>
<thead>
<tr class="header">
<th><p>大学设立</p></th>
<th><p>1949年：<strong>东京学艺大学</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>创立</p></td>
<td><p>1873年：<strong>东京府小学教则讲习所</strong><br />
1920年：<strong>东京府立农业教员养成所</strong></p></td>
</tr>
<tr class="even">
<td><p>学校类别</p></td>
<td><p><a href="../Page/国立大学.md" title="wikilink">国立大学法人</a></p></td>
</tr>
<tr class="odd">
<td><p>校长</p></td>
<td><p>[出口利定]</p></td>
</tr>
<tr class="even">
<td><p>所在地</p></td>
<td><p><a href="../Page/东京都.md" title="wikilink">东京都</a><a href="../Page/小金井市.md" title="wikilink">小金井市贯井北町</a>4-1-1</p></td>
</tr>
<tr class="odd">
<td><p>校园<br />
</p></td>
<td><p>小金井校园 - <a href="../Page/东京都.md" title="wikilink">东京都</a><a href="../Page/小金井市.md" title="wikilink">小金井市</a></p></td>
</tr>
<tr class="even">
<td><p>学部</p></td>
<td><p>教育学部</p></td>
</tr>
<tr class="odd">
<td><p>大学院（研究生院）</p></td>
<td><p>教育学研究课<br />
联合学校教育学研究科</p></td>
</tr>
<tr class="even">
<td><p>附属</p></td>
<td><p>东京学艺大学附属世田谷小学校<br />
东京学艺大学附属世田谷中学校<br />
<a href="../Page/东京学艺大学附属高中.md" title="wikilink">东京学艺大学附属高中</a><br />
　<br />
东京学艺大学附属小金井小学校<br />
东京学艺大学附属小金井中学校<br />
东京学艺大学附属幼儿园小金井园<br />
　<br />
东京学艺大学附属大泉小学校<br />
东京学艺大学附属大泉中学校<br />
东京学艺大学附属高中大泉校园<br />
　<br />
东京学艺大学附属竹早小学校<br />
东京学艺大学附属竹早中学校<br />
东京学艺大学附属幼儿园竹早园<br />
　<br />
东京学艺大学附属养护学校</p></td>
</tr>
<tr class="odd">
<td><p>网页<br />
</p></td>
<td><p><a href="http://www.u-gakugei.ac.jp/">东京学艺大学</a></p></td>
</tr>
</tbody>
</table>

</div>

</div>

**东京学艺大学**（[日文](../Page/日文.md "wikilink")：<span lang=ja>とうきょうがくげいだいがく</span>、[英文](../Page/英文.md "wikilink")：Tokyo
Gakugei
University）是位于[日本](../Page/日本.md "wikilink")[东京都](../Page/东京都.md "wikilink")[小金井市的](../Page/小金井市.md "wikilink")，一所属于[师范类](../Page/师范.md "wikilink")[国立](../Page/国立大学.md "wikilink")[综合性大学](../Page/综合性大学.md "wikilink")。

东京学艺大学一般地被称呼为**“学大”**、或者**“学艺大”**、**“东学大”**等。

至今，东京学艺大学为日本教育界输送了不少教育性人材。
作为[教育学的实践性研究以及学生的实习机构](../Page/教育学.md "wikilink")，东京学艺大学还拥有2个附属[幼稚园](../Page/幼稚园.md "wikilink")，附属[小学](../Page/小学.md "wikilink")、[初中各](../Page/初中.md "wikilink")4所、2所附属[高中](../Page/高中.md "wikilink")、以及1所附属养护（残障护理）学校等。

## 学部・大学院・专攻科

### 教育学部

  - A类（初等教育教员养成课程：国语、社会、数学、理科、音乐、美术（图工）、保健体育、家庭、学校教育、幼儿园
  - B类（中等教育教员养成课程）：国语、社会、数学、理科、音乐、美术、保健体育、家庭、技术、英语、书法
  - C类（残障儿童教育教员养成课程）：听觉残疾儿童教育、残障护理学校教育、语言残疾儿童教育
  - L类（终生学习课程）：学习社会文化、生涯体育运动
  - N类（人类福利课程）：辅导、综合社会体系
  - K类（国际理解教育课程）：欧美研究、亚洲研究、日本研究、国际教育、多语言多文化、日本语教育
  - F类（环境教育课程）：环境教育、自然环境科学、文物科学
  - J类（情报教育课程）：情报教育
  - G类（艺术文化课程）：音乐、美术、书法、表现·交流

### 大学院教育学研究科（硕士课程）

### 大学院联合学校教育研究科（博士课程）

### 特殊教育特别专攻科

  -
    （参考）学部改组以前，A・B类各为小学教员养成课程、中学教员养成课程，其它的还有D类（特別教科教员养成课程）、E类（幼儿园教员养成课程）。学部改组之后E类合并于A类、D类则被合并于B类。

## 大学概要

现在的东京学艺大学，主要分为为培养[学校](../Page/学校.md "wikilink")（[幼稚园](../Page/幼稚园.md "wikilink")、[小学](../Page/小学.md "wikilink")、[初中](../Page/初中.md "wikilink")、[高中等](../Page/高中.md "wikilink")）教师的**教育系**和1980年代后半开始逐渐设立的**教养系**这两大系。虽然这都归属于[教育学部](../Page/教育学部.md "wikilink")，但实际上被认为是不同的[学科](../Page/学科.md "wikilink")。

**教育系**以培养教师为主要目的。学生毕业时必须取得[教师](../Page/教师.md "wikilink")[资格证](../Page/资格证.md "wikilink")。在日本，曾经有过这么一段“高中教师为[东京教育大学出身占大多数](../Page/东京教育大学.md "wikilink")、而小、中学校教师则为东京学艺大学出身者占大多数”的时期，但在东京教育大学被[筑波大学改组而成为](../Page/筑波大学.md "wikilink")[综合大学以后](../Page/综合大学.md "wikilink")，东京学艺大学实际上成了日本国内教育学科的代表，因而受到日本教育界的高度评价。最近，虽然有些东京学艺大学的毕业生也开始活跃于日本的政、官、财界，但东京学艺大学在教育界以外的知名度还不算很高。

**教养系**被通称为**“0资格课程”**，即学生在毕业时没有必要取得[教师资格证就可毕业](../Page/教师资格证.md "wikilink")。「教育系」的学科分为[人文科学](../Page/人文科学.md "wikilink")、[社会科学](../Page/社会科学.md "wikilink")、[自然科学](../Page/自然科学.md "wikilink")、[艺术](../Page/艺术.md "wikilink")・[体育系等](../Page/体育.md "wikilink")，涉及的范围广泛。因所学内容不受为取得教师资格而所学的限制，可潜心钻研“学艺”课程。

由此，东京学艺大学学生毕业时所授予的学位也是各不相同的。教育系的毕业生可被授予**“教育学士”**，而教养系的毕业生则被授予**“教养学士”**。也就是说现在的东京学艺大学不只是停留在狭义的培养教师的大学，而是一所如同**“学艺”**（[学问与](../Page/学问.md "wikilink")[艺术](../Page/艺术.md "wikilink")）其名而能够深入其中的大学。为此，大学内部也有人主张将教育学部改为[教育学部与](../Page/教育学部.md "wikilink")[教养学部二大学部体制](../Page/教养学部.md "wikilink")。

### 与他大学的提携

  - **多摩地区大学协议**

[多摩地区大学协议指的是由以下五所](../Page/多摩地区大学协议.md "wikilink")[国立大学以及一所](../Page/国立大学.md "wikilink")[私立大学之间组成的](../Page/私立大学.md "wikilink")[学分互換制度](../Page/学分互換.md "wikilink")。

  - [一桥大学](../Page/一桥大学.md "wikilink")
  - **东京学艺大学**
  - [东京农工大学](../Page/东京农工大学.md "wikilink")
  - [电气通信大学](../Page/电气通信大学.md "wikilink")
  - [东京外国语大学](../Page/东京外国语大学.md "wikilink")
  - [津田塾大学](../Page/津田塾大学.md "wikilink")

以上几校都是位于[多摩地区的大学](../Page/多摩地域.md "wikilink")。由于地理的接近，从而确定了“互换学分”等教育援助的计划。但是，其还尚未达到如同[一桥大学](../Page/一桥大学.md "wikilink")、[东京工业大学](../Page/东京工业大学.md "wikilink")、[东京医科齿科大学](../Page/东京医科齿科大学.md "wikilink")、[东京外国语大学所结成的](../Page/东京外国语大学.md "wikilink")“四大学联合”那般紧密地关系。

  - **大学院博士课程的联合大学院**

东京学艺大学的大学院博士课程，与以下大学的大学院提携，形成了联合大学院。

  - **东京学艺大学**
  - [横滨国立大学](../Page/横滨国立大学.md "wikilink")
  - [千叶大学](../Page/千叶大学.md "wikilink")
  - [埼玉大学](../Page/埼玉大学.md "wikilink")

以上大学在教育学方面也都是日本著名的国立大学。

## 著名教官

  - [荒井洋一](../Page/荒井洋一.md "wikilink") - 西洋哲学
  - [池田荣一](../Page/池田荣一.md "wikilink") - 英国文学
  - [古田悦三](../Page/古田悦三.md "wikilink") - 历史地理学
  - [角替晃](../Page/角替晃.md "wikilink") - 宪法、信息法、政治机构论。
  - [久迩良子](../Page/久迩良子.md "wikilink") - 政治学、国籍关系论、法国政治史
  - [刘德强](../Page/刘德强.md "wikilink") -
    国际经济学、开发经济学、（兼任[一桥大学研究生院教授](../Page/一桥大学.md "wikilink")）
  - [上野一彦](../Page/上野一彦.md "wikilink") - 教育心理学
  - [古谷惠太](../Page/古谷惠太.md "wikilink") - 教育哲学
  - [平野朗久](../Page/平野朗久.md "wikilink") - 教育方法学
  - [山田昌弘](../Page/山田昌弘.md "wikilink") - 家庭社会学、感情社会学、性学

## 著名校友

### 文化

  - [石飞博光](../Page/石飞博光.md "wikilink") - 书法家
  - [押井守](../Page/押井守.md "wikilink") - 卡通片导演
  - [金子修介](../Page/金子修介.md "wikilink") - 电影导演
  - [银林实](../Page/银林实.md "wikilink") - 作家
  - [须藤真澄](../Page/须藤真澄.md "wikilink") - 漫画家
  - [柿沼康二](../Page/柿沼康二.md "wikilink") - 书法家
  - [福山庸治](../Page/福山庸治.md "wikilink") - 漫画家
  - [竹下文子](../Page/竹下文子.md "wikilink") - 儿童文学作家

### 其他领域

  - [原岛宏治](../Page/原岛宏治.md "wikilink") -
    政治家、[公明党第一代中央执行委员长](../Page/公明党.md "wikilink")
  - [區諾軒](../Page/區諾軒.md "wikilink")-
    政治家、現任[香港立法會議員](../Page/香港立法會議員.md "wikilink")

## 外部链接

  - [东京学艺大学](http://www.u-gakugei.ac.jp/)

[Category:東京學藝大學](../Category/東京學藝大學.md "wikilink")
[Category:日本師範院校](../Category/日本師範院校.md "wikilink")
[Category:小金井市](../Category/小金井市.md "wikilink")