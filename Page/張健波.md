**張健波**，[香港資深傳媒人](../Page/香港.md "wikilink")，前《[明報](../Page/明報.md "wikilink")》[總編輯](../Page/總編輯.md "wikilink")，現為《明報》編務總監。2014年1月20日起，張本人兼任該報總編輯。\[1\]

## 簡歷

張健波在1978年畢業於[香港中文大學](../Page/香港中文大學.md "wikilink")[新亞書院](../Page/新亞書院.md "wikilink")，並於1980年曾出往[法國進修](../Page/法國.md "wikilink")，曾於[商業電台等媒體當記者](../Page/商業電台.md "wikilink")。

張於1986年10月加入《明報》，先當《[明報月刊](../Page/明報月刊.md "wikilink")》總編輯，至1988年9月才任明報副總編輯。張其後退出，又於1997年接受當時主席[張曉卿邀請](../Page/張曉卿.md "wikilink")，重返明報，出任總編輯。他亦曾於[查良鏞及](../Page/查良鏞.md "wikilink")[于品海時代出任總編輯或執行總編輯](../Page/于品海.md "wikilink")。

2012年1月1日，張卸任[明報總編輯](../Page/明報.md "wikilink")，改任編務總監。總編輯一職由前執行總編輯[劉進圖擔任](../Page/劉進圖.md "wikilink")。2014年1月20日，張再次出任總編輯，直至另行通告。

## 立場

張一直堅持明報走中立路線，曾經在文章中批評[黃毓民而被控告誹謗](../Page/黃毓民.md "wikilink")，最後[庭外和解及賠償](../Page/庭外和解.md "wikilink")15萬港元。惟他由於言論自由問題時立場頗硬，例如於《[香港基本法第二十三條](../Page/香港基本法第二十三條.md "wikilink")》立法時，《明報》曾經發表社評強烈批評政府，張本人也有出席2003年的[七一大遊行](../Page/七一大遊行.md "wikilink")。就釋放[程翔問題](../Page/程翔.md "wikilink")，張亦有參與[泛民發起的聯署](../Page/泛民.md "wikilink")。[冰點雜誌停刊](../Page/冰點.md "wikilink")，明報亦有作篇幅報導。

## 逸聞

  - 不少明報員工都稱呼他「張波」，甚至張自己也有時自稱「張波」。\[2\]
  - 2003年[愚人節時](../Page/愚人節.md "wikilink")，有學生製作假冒明報網站，指香港成為疫埠，引起軒然大波，有指張健波曾親自致電該名學生，發出警告，但並未得到證實。
  - 張健波曾在文章中强烈批评黃毓民，最後庭外和解及賠償十五萬元，近年立場多次批评社民連，招來社民连支持者反感。

## 資料來源

  - <http://www.southcn.com/news/hktwma/twmil/200206271865.htm>
  - <http://www.mingpaovan.com/htm/News/20070516/HK-gzv1.htm>

[Category:香港傳媒工作者](../Category/香港傳媒工作者.md "wikilink")
[Category:香港報界人士](../Category/香港報界人士.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[健波](../Category/張姓.md "wikilink")

1.  <http://rthk.hk/mobile/news/20140120/979010.htm>
2.  <http://210.17.183.115/talks/thread-896417-1-1.html>