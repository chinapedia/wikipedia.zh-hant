**NGC
2**，這是在[飛馬座的一個](../Page/飛馬座.md "wikilink")[漩渦星系](../Page/漩渦星系.md "wikilink")。它的[星等為](../Page/星等.md "wikilink")14.2等，[赤經為](../Page/赤經.md "wikilink")7分15.9秒，[赤緯為](../Page/赤緯.md "wikilink")27°42′31″。[紅位移約](../Page/紅位移.md "wikilink")
7550 [km](../Page/公里.md "wikilink")／s

## 参见

  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 參考文獻

## 外部連結

  - Source: [NGC/IC Project](http://www.ngcic.org/)

  -
[Category:飞马座NGC天体](../Category/飞马座NGC天体.md "wikilink")