[Gulangyu_Island,Xiamen_City.JPG](https://zh.wikipedia.org/wiki/File:Gulangyu_Island,Xiamen_City.JPG "fig:Gulangyu_Island,Xiamen_City.JPG")

**鼓浪屿**（[白話字](../Page/白話字.md "wikilink")：
**Kó͘-lōng-sū**；[邮政式拼音](../Page/邮政式拼音.md "wikilink")：Kulangsu）是位于中华人民共和国福建省[厦门市](../Page/厦门市.md "wikilink")[思明区的一个小岛](../Page/思明区.md "wikilink")，原鼓浪屿区后被撤销行政区并入思明管辖，現成為著名的风景区，面积不到2平方公里（0.77平方哩），人口约2万。有“海上花园”、“万国建筑博览会”、“钢琴之岛”之美称。除环岛[电动车外不允许机动车辆上岛](../Page/电动车.md "wikilink")，因此气氛幽静。2005年《[中国国家地理](../Page/中国国家地理.md "wikilink")》杂志将鼓浪屿评为“中国最美的[城区](../Page/城区.md "wikilink")”第一名。2017年7月在波兰克拉科夫举行的第41届世界遗产大会上被正式列入《世界遗产名录》。

## 历史

[Zheng_Chenggong.JPG](https://zh.wikipedia.org/wiki/File:Zheng_Chenggong.JPG "fig:Zheng_Chenggong.JPG")石像\]\]
鼓浪屿开拓于[宋末](../Page/宋朝.md "wikilink")[元初](../Page/元朝.md "wikilink")，原名“**圆沙洲**”、“**圆洲仔**”。[明朝改称](../Page/明朝.md "wikilink")“鼓浪屿”。[郑成功曾以此为军事据点屯兵操练](../Page/郑成功.md "wikilink")，对抗[清兵](../Page/清朝.md "wikilink")。目前日光岩上尚存有当时建造的水操台、石寨门故址。

[鸦片战争时期](../Page/鸦片战争.md "wikilink")，[英军曾占领鼓浪屿](../Page/英国.md "wikilink")，直到1845年撤军。1843年后，[厦门根据](../Page/厦门.md "wikilink")《[中英南京条约](../Page/中英南京条约.md "wikilink")》开辟为[通商口岸](../Page/通商口岸.md "wikilink")。[中日甲午战争以后](../Page/中日甲午战争.md "wikilink")，[日本占领](../Page/日本.md "wikilink")[台湾](../Page/台湾.md "wikilink")，为避免日本进一步觊觎厦门，[清朝政府决定请列强](../Page/清朝.md "wikilink")“兼护厦门”\[1\]。1902年1月10日（[光绪二十七年十二月初一日](../Page/光绪.md "wikilink")），[英国](../Page/英国.md "wikilink")、[美国](../Page/美国.md "wikilink")、[德国](../Page/德国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[丹麦](../Page/丹麦.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[瑞挪联盟](../Page/瑞挪联盟.md "wikilink")、[日本等](../Page/日本.md "wikilink")9国驻厦门领事与清朝[福建省兴泉永道台](../Page/福建省.md "wikilink")[延年在鼓浪屿日本领事馆签订](../Page/延年.md "wikilink")《[厦门鼓浪屿公共地界章程](../Page/厦门鼓浪屿公共地界章程.md "wikilink")》\[2\]，鼓浪屿成为[公共租界](../Page/鼓浪屿公共租界.md "wikilink")\[3\]，次年1月，鼓浪屿公共租界工部局成立。在此前后，陆续有英、美、法、德、日等13个国家先后在岛上设立[领事馆](../Page/领事馆.md "wikilink")。

这段时期，有很多[传教士来到鼓浪屿](../Page/传教士.md "wikilink")，他们建立的学校对中国现代教育有重大影响\[4\]，例如：1898年，英国牧师[韦玉振与夫人](../Page/韦玉振.md "wikilink")[韦爱莉到鼓浪屿传教时创办的](../Page/韦爱莉.md "wikilink")“怀德幼稚园”，是中国第一所[幼儿园](../Page/幼儿园.md "wikilink")（现已更名为日光幼儿园）。

1949年10月，[中国人民解放军](../Page/中国人民解放军.md "wikilink")[占领鼓浪屿](../Page/厦门战役_\(1949年\).md "wikilink")。

2003年5月经[国务院批准](../Page/国务院.md "wikilink")，厦门市的鼓浪屿区、思明区和[开元区合并成立新的思明区](../Page/开元区.md "wikilink")。\[5\]在这之前鼓浪屿区是全国面积最小、人口最少的副厅级行政区。\[6\]

2017年7月8日 「鼓浪嶼國際歷史社區」通過了世界遺產大會的終審，成功列入世界文化遺產名錄，成為中國第52項世界遺產專案\[7\]。

## 地理

面积1.77平方-{zh-hans:千米; zh-hant:公里;}-，与厦门市区相隔不到1,000-{zh-hans:米;
zh-hant:公尺;}-\[8\]。最高海拔92.7-{zh-hans:米; zh-hant:公尺;}-（日光岩顶）。
[Gulangyu_sunlight_rock_2011_12.jpg](https://zh.wikipedia.org/wiki/File:Gulangyu_sunlight_rock_2011_12.jpg "fig:Gulangyu_sunlight_rock_2011_12.jpg")
[鼓浪屿日光岩摩崖石刻.JPG](https://zh.wikipedia.org/wiki/File:鼓浪屿日光岩摩崖石刻.JPG "fig:鼓浪屿日光岩摩崖石刻.JPG")

## 风景名胜

  - [日光岩](../Page/日光岩.md "wikilink")，俗称“晃岩”，为鼓浪屿制高点。岩顶可观鼓浪屿全岛。
  - 毓园（林巧稚大夫纪念园）：纪念出生于鼓浪屿的[林巧稚医生](../Page/林巧稚.md "wikilink")。
  - 皓月园：园名取自[郑成功诗句](../Page/郑成功.md "wikilink")“思君寝不寐，皓月透素帷”。园中[郑成功石像为](../Page/郑成功.md "wikilink")1985年建成，高15.7-{zh-hans:米;
    zh-hant:公尺;}-（据说[郑成功本人身高](../Page/郑成功.md "wikilink")1.57-{zh-hans:米;
    zh-hant:公尺;}-）。[Shuzhuang_garden.jpg](https://zh.wikipedia.org/wiki/File:Shuzhuang_garden.jpg "fig:Shuzhuang_garden.jpg")
  - 菽庄花园：建于1913年，主人[林爾嘉](../Page/林爾嘉.md "wikilink")，字叔藏，又作菽庄。[甲午战争后](../Page/甲午战争.md "wikilink")，[台湾被割给](../Page/台湾.md "wikilink")[日本](../Page/日本.md "wikilink")，其父[板橋林家](../Page/板橋林家.md "wikilink")[林維源全家迁来鼓浪屿居住](../Page/林維源.md "wikilink")，修建菽庄花园。面积2万多平方-{zh-hans:米;
    zh-hant:公尺;}-（包括水域）。1951年林爾嘉在台湾病逝后，其夫人将菽庄花园捐献给厦门市政府\[9\]。
  - 厦门海底世界：1998年1月对外开放，展出鱼类350多种，一万多尾，并陈列有世界最大的[抹香鲸](../Page/抹香鲸.md "wikilink")[标本](../Page/标本.md "wikilink")。\[10\]

## 近代建筑群

[Haitiantanggou.JPG](https://zh.wikipedia.org/wiki/File:Haitiantanggou.JPG "fig:Haitiantanggou.JPG")
[Gulangyu_church.JPG](https://zh.wikipedia.org/wiki/File:Gulangyu_church.JPG "fig:Gulangyu_church.JPG")\]\]
与上述名胜景点不同，这些老房子目前大多为私人住宅，少部分为营业酒店，并不接受游客参观。

  - 林氏府（八角楼）：位于鹿礁路11-19号。原主人[板橋林家林尔嘉](../Page/板橋林家.md "wikilink")。林氏府副楼已于2006年5月倒塌\[11\]。
  - 英国领事馆：位于鹿礁路14号。
  - 日本领事馆：位于鹿礁路26号。
  - 天主堂：位于鹿礁路34号，建于1916年。为[哥特式建筑](../Page/哥特式建筑.md "wikilink")。
  - 美国领事馆：位于三明路26号。现为酒店。
  - 番婆楼：位于安海路36号，建于1927年。原主人旅菲华侨[许经权](../Page/许经权.md "wikilink")。
  - 杨家园：跨安海路4号至鼓新路27-29号。建于1913年。
  - 八卦楼：位于鼓新路43号。建于1907年。是鼓浪屿标志建筑。原主人为林尔嘉的堂弟林鹤寿。现在是[厦门市博物馆](../Page/厦门.md "wikilink")、[管风琴博物馆](../Page/管风琴.md "wikilink")。
  - 船楼：位于鼓新路48号。原主人医生[黄大辟](../Page/黄大辟.md "wikilink")。
  - 汇丰银行经理公馆：位于鼓新路57号。建于1876年。
  - 林屋：位于泉州路82号。原主人[林振勋](../Page/林振勋.md "wikilink")。
  - 金瓜楼：位于泉州路99号。原主人[菲律宾](../Page/菲律宾.md "wikilink")[华侨](../Page/华侨.md "wikilink")[黄赐敏](../Page/黄赐敏.md "wikilink")。
  - 观海别墅：位于田尾路17号。建于1918年。原为丹麦大北电报局公司经理公寓。
  - 三一堂：位于笔山洞口。建于1934年。
  - 怡园：位于福建路24号。原主人为诗人[林鹤年](../Page/林鹤年.md "wikilink")。
  - 黄荣远堂：位于福建路32号。原主人[菲律宾](../Page/菲律宾.md "wikilink")[华侨](../Page/华侨.md "wikilink")[施光](../Page/施光.md "wikilink")、[黄仲训](../Page/黄仲训.md "wikilink")。建于1920年。现为厦门演艺职业学院。
  - 海天堂构：位于福建路42号。建于上世纪20年代。原主人[菲律宾](../Page/菲律宾.md "wikilink")[华侨](../Page/华侨.md "wikilink")[黄秀烺和他的同乡](../Page/黄秀烺.md "wikilink")[黄念忆](../Page/黄念忆.md "wikilink")。
  - 白家别墅：位于复兴路，原主人[白登弼](../Page/白登弼.md "wikilink")。
  - 廖家别墅：位于漳州路44号，原主人[廖翠凤](../Page/廖翠凤.md "wikilink")。1919年8月，[林语堂和该楼主人家的小姐](../Page/林语堂.md "wikilink")[廖翠凤在此结婚](../Page/廖翠凤.md "wikilink")。
  - 瞰青别墅：位于永春路71号。建于1918年。原主人[黄仲训](../Page/黄仲训.md "wikilink")。1949年，瞰青别墅作为[蒋中正的](../Page/蒋中正.md "wikilink")“行辕”装修一新。后因局势变化太快，[蒋中正只于](../Page/蒋中正.md "wikilink")7月23日在此逗留一晚\[12\]。1962年，[郭沫若曾在此写](../Page/郭沫若.md "wikilink")《郑成功》剧本。此楼现为[郑成功纪念馆的藏书资料室](../Page/郑成功.md "wikilink")。
  - 西林别墅：位于永春路73号，建于1926年。现为[郑成功纪念馆](../Page/郑成功.md "wikilink")。
  - 圃庵：位于鸡山路16号，原主人钢琴家[殷承宗](../Page/殷承宗.md "wikilink")。
  - 容（榕）谷：位于旗山路7号，原主人[李清泉](../Page/李清泉.md "wikilink")。
  - 黄家花园：位于晃岩路25号，建于1918年至1923年间，原主人华侨巨商、“印尼糖王”[黄亦住](../Page/黄亦住.md "wikilink")。五十年代，这里为厦门政府的“国宾馆”，接待过[邓小平](../Page/邓小平.md "wikilink")、[尼克松](../Page/尼克松.md "wikilink")、[李光耀等国家政要](../Page/李光耀.md "wikilink")。
  - 公审会堂：位于笔山路1-3号。
  - 林文庆别墅：笔山路5号。原主人[新加坡](../Page/新加坡.md "wikilink")[華僑](../Page/華僑.md "wikilink")[林文庆](../Page/林文庆.md "wikilink")。
  - 观彩楼：位于笔山路6号。
  - 亦足山庄：位于笔山路9号，原主人越南华侨[许涧](../Page/许涧.md "wikilink")，建于20世纪20年代。
  - 春草堂：位于笔山路17号，原主人[厦门建筑公会会长](../Page/厦门.md "wikilink")[许春草](../Page/许春草.md "wikilink")。

## 相关名人

  - [卢戆章](../Page/卢戆章.md "wikilink")：[语言学家](../Page/语言学家.md "wikilink")。18岁[科举落第后由](../Page/科举.md "wikilink")[同安迁居鼓浪屿后](../Page/同安.md "wikilink")，终生居住在鼓浪屿上。1928年12月28日因病逝世，葬于鼓浪屿鸡山。现鼓浪屿有卢戆章雕像，从铜像到其墓地有一段长约500-{zh-hans:米;
    zh-hant:公尺;}-的刻有[标点符号和](../Page/标点符号.md "wikilink")[汉语拼音的石板小路以纪念他对](../Page/汉语拼音.md "wikilink")[汉语的贡献](../Page/汉语.md "wikilink")。
  - [马约翰](../Page/马约翰.md "wikilink")：[运动员](../Page/运动员.md "wikilink")、体育[教育家](../Page/教育家.md "wikilink")，曾任[清华大学教授](../Page/清华大学.md "wikilink")、[中华全国体育总会主任](../Page/中华全国体育总会.md "wikilink")。1882年出生于鼓浪屿。现鼓浪屿上建有马约翰广场。
  - [林巧稚](../Page/林巧稚.md "wikilink")：[妇科](../Page/妇科.md "wikilink")、[产科医生](../Page/产科.md "wikilink")，曾任[中国医学科学院副院长](../Page/中国医学科学院.md "wikilink")。1901年出生于鼓浪屿，从鼓浪屿女子师范学校毕业后报考北京协和医院。现鼓浪屿建有毓园纪念她。
  - [蔡其矫](../Page/蔡其矫.md "wikilink")：[诗人](../Page/诗人.md "wikilink")。1928年11岁时由侨居的[印尼回国就读于鼓浪屿福民小学](../Page/印尼.md "wikilink")（即现在的笔山小学）。1954年在鼓浪屿三一堂内开文学讲座。写有诗作《鼓浪屿》（1956年）流传较广，特别是其中写道一句“水上的鼓浪屿，一只彩色的楼船。”\[13\]
  - [舒婷](../Page/舒婷.md "wikilink")：诗人。在鼓浪屿上有祖宅，其大半生都生活在鼓浪屿，曾经有一位读者写信给舒婷说：“正是鼓浪屿花朝月夕，才熏陶出一颗玲珑剔透的心。”\[14\]\[15\]
  - [陈佐湟](../Page/陈佐湟.md "wikilink")：[指挥家](../Page/指挥家.md "wikilink")，曾出任[中央乐团交响乐队指挥](../Page/中国国家交响乐团.md "wikilink")。幼时随父母由上海搬迁至鼓浪屿居住，从此走上音乐道路\[16\]。
  - [连岳](../Page/连岳.md "wikilink")：著名的[批评家和](../Page/批评家.md "wikilink")[网志作者](../Page/网志作者.md "wikilink")。辞去公职后，迁至鼓浪屿居住。
  - [黃萱](../Page/黃萱.md "wikilink")：國學大師[陳寅恪的助手](../Page/陳寅恪.md "wikilink")。

## 其他

### 钢琴之岛

[Gulangyuconcert.jpg](https://zh.wikipedia.org/wiki/File:Gulangyuconcert.jpg "fig:Gulangyuconcert.jpg")
鼓浪屿的人均[钢琴拥有率为全国最高](../Page/钢琴.md "wikilink")，因1990年成立的[厦门大学附属鼓浪屿音乐学校与](../Page/厦门大学.md "wikilink")2006年成立的[中央音乐学院附属鼓浪屿钢琴学校坐落岛上而被美名](../Page/中央音乐学院.md "wikilink")“钢琴之岛”，也因众多国际知名的音乐家诞生于岛上。2002年鼓浪屿被中国音乐家协会命名为“音乐之岛”，岛上有两个著名的博物馆——钢琴博物馆、风琴博物馆（八卦楼）。另外，除了鼓浪屿音乐学校与鼓浪屿钢琴学校，岛上还有一所演艺职业学院。

### 鼓浪屿登岛轮渡及收费

[Gulangyu_Island_Ferry_Round_Trip_Ticket.jpg](https://zh.wikipedia.org/wiki/File:Gulangyu_Island_Ferry_Round_Trip_Ticket.jpg "fig:Gulangyu_Island_Ferry_Round_Trip_Ticket.jpg")
[Sanqiutian_Ferry_Terminal_(20170120173750).jpg](https://zh.wikipedia.org/wiki/File:Sanqiutian_Ferry_Terminal_\(20170120173750\).jpg "fig:Sanqiutian_Ferry_Terminal_(20170120173750).jpg")
2003年之前厦门至鼓浪屿的船票为往返程3元，2003年初，[厦门政府拟征收](../Page/厦门.md "wikilink")80元“上岛费”以增加旅游业收入，激起厦门民众普遍抗议，因为鼓浪屿并非纯粹旅游区，其仍然是城市整体功能区的一部分\[17\]。后仅船票升至往返8元，其余照旧\[18\]。

2014年10月20日，厦鼓轮渡航线调整，游客白天进入鼓浪屿的码头调整为湖里区的邮轮中心厦鼓码头及海沧区的嵩屿码头，思明区轮渡码头只保留居民专线、旅游客运夜航航班和异常天气及特殊情况应急疏运\[19\]。厦门市民乘船往返票价为8元，非厦门市民乘船票价为30元（嵩屿码头）、35元（邮轮中心厦鼓码头普通游船）、50元（邮轮中心厦鼓码头豪华游船）\[20\]。

### 日光岩索道

日光岩索道1993年由原鼓浪屿区政府投资建设，连接日光岩与英雄山两大景点，索道全长202米，曾是世界上最短的索道。1995年3月正式对外营运，运营16年共5.5万小时，运载游客上千万人次。因鼓浪屿申报[世界文化遗产的需要](../Page/世界文化遗产.md "wikilink")，根据《鼓浪屿历史文化遗产保护规划》及申遗专家的建议，鼓浪屿管委会将索道列入申遗整治项目，2012年6月8日索道開始拆除。\[21\]

### [故宫鼓浪屿外国文物馆](../Page/故宫鼓浪屿外国文物馆.md "wikilink")

故宫鼓浪屿外国文物馆，位于鼓浪屿鼓新路70-80号，是[故宫博物院的分馆](../Page/故宫博物院.md "wikilink")，主要展出故宫博物院收藏的外国[文物](../Page/文物.md "wikilink")。2017年5月13日正式开馆。

## 参见

  - [鼓浪屿之波](../Page/鼓浪屿之波.md "wikilink")

## 註釋

## 外部链接

  - [鼓浪屿门户网站](http://gly.cn)
  - [鼓浪屿官方旅游指南](http://www.kulangsuisland.org)

[cat:中国世界遗产](../Page/cat:中国世界遗产.md "wikilink")

[Category:厦门地理](../Category/厦门地理.md "wikilink")
[Category:福建岛屿](../Category/福建岛屿.md "wikilink")
[Category:思明区](../Category/思明区.md "wikilink")
[Category:臺灣海峽島嶼](../Category/臺灣海峽島嶼.md "wikilink")
[Category:厦门旅游](../Category/厦门旅游.md "wikilink")

1.  张震世：《公共租界时期的鼓浪屿》，《厦门文史资料》第3辑，第20页
2.  中国第一历史档案馆外务部档案，综合类，第4462号
3.  [厦门古代史](http://www.shixing.org.cn/book/u/4/archives/2008/68.htm)
4.
5.  [行政区划--中国厦门](http://www.xm.gov.cn/zjxm/xmgk/200708/t20070830_173886.htm)
6.  [11年后回头看 1+1+1确实大于3](http://roll.sohu.com/20140306/n396126036.shtml)
7.
8.  《[中国国家地理](../Page/中国国家地理.md "wikilink")》，2005年10月，p201，[舒婷](../Page/舒婷.md "wikilink")。
9.
10. [厦门海底世界网站](http://www.xmuww.com/)
11. [林尔嘉故居坍塌引关注　厦门老房子紧急呼救](http://www.csnn.com.cn/2006/ca471474.htm)
12. [日光岩差点成了他私家花园](http://www.fjhouse.cn/new/html/2004-7-16/200471693152.htm)
13.
14. [鼓浪嶼上的舒婷](http://www.people.com.cn/BIG5/channel6/32/20001205/338194.html)
15. [老房子的前世今生](http://www.eduww.com/rmwx/ShowArticle.asp?ArticleID=7160)

16. [陈佐湟：从鼓浪屿走向世界乐坛的指挥家](http://news.eastday.com/epublish/gb/paper148/20020610/class014800007/hwz688963.htm)
17. [初定80元鼓浪屿"上岛费"引争议](http://news.xinhuanet.com/travel/2004-12/11/content_2321131.htm)

18.
19. [厦鼓轮渡航线20日起调整
    游客将从东渡、嵩屿乘船上岛](http://news.xmnn.cn/a/xmxw/201410/t20141005_4123065.htm)
20. [厦门轮渡有限公司-邮轮中心厦鼓码头](http://www.xmferry.com/mtjjt/ylzhxmt/hxjpj/25329.htm)
21. [鼓浪屿申遗重点环境整治项目日光岩索道拆除工程启动](http://www.glysyw.com/html/sydt/2016/0311/2016.html)