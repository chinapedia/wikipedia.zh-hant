「**香港四大才子**」，又稱**香江四大才子**，是指[香港四位文人](../Page/香港.md "wikilink")，分別為[金庸](../Page/金庸.md "wikilink")、[倪匡](../Page/倪匡.md "wikilink")、[黃霑和](../Page/黃霑.md "wikilink")[蔡瀾](../Page/蔡瀾.md "wikilink")，四人各有所長，皆是腹中飽有學問之輩，而且分屬好友，因而被稱為「香港四大才子」\[1\]。四人之成就約於七、八十年代開始被廣為認識和推崇，而其中黃霑和金庸分別於2004年及2018年病逝，倪匡則已封筆，只有蔡瀾偶爾做做電視節目和寫美食書。

「香港四大才子」中的三人倪匡、黃霑和蔡瀾曾於1989年至1990年間共同主持[亞洲電視](../Page/亞洲電視.md "wikilink")（ATV）經典成人清談節目《[今夜不設防](../Page/今夜不設防_\(亞洲電視\).md "wikilink")》。2013年，在[黎智英的邀請下](../Page/黎智英.md "wikilink")，倪匡和蔡瀾現時在[壹傳媒主持每周一集的清談視像節目](../Page/壹傳媒.md "wikilink")《亂噏廿四》。\[2\]

## 四大才子

### 金庸

[金庸原名查良鏞](../Page/金庸.md "wikilink")，生於[浙江](../Page/浙江.md "wikilink")[海寧](../Page/海寧.md "wikilink")，1948年移居香港，2018年10月30日逝世，是當今[武俠小說作家之大師](../Page/武俠小說.md "wikilink")。他筆法鋪陳，描寫細致，所著[武俠小說在華人世界中廣泛流傳](../Page/武俠小說.md "wikilink")、家傳戶曉。他的十五部小說中，幾乎都被拍成[電影](../Page/電影.md "wikilink")、[電視劇](../Page/電視劇.md "wikilink")，作品被翻譯成十數種語言。其中尤以《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》、《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》、《[鹿鼎記](../Page/鹿鼎記.md "wikilink")》、《[笑傲江湖](../Page/笑傲江湖.md "wikilink")》、《[天龍八部](../Page/天龍八部_\(小說\).md "wikilink")》、《[倚天屠龍記](../Page/倚天屠龍記.md "wikilink")》等最廣為人知。

### 倪匡

[倪匡原名倪聰](../Page/倪匡.md "wikilink")，生於[浙江](../Page/浙江.md "wikilink")[寧波](../Page/寧波.md "wikilink")，1957年[偷渡到香港](../Page/偷渡.md "wikilink")，是華人[科幻小說界裡最具影響力的作家](../Page/科幻小說.md "wikilink")，他的作品的結局往往出人意表。他筆下有很多名作，其中以[衛斯理系列](../Page/衛斯理系列.md "wikilink")、[原振俠系列](../Page/原振俠系列.md "wikilink")、[浪子高達系列](../Page/浪子高達系列.md "wikilink")、[亞洲之鷹羅開系列](../Page/亞洲之鷹羅開系列.md "wikilink")、[女黑俠木蘭花系列](../Page/女黑俠木蘭花系列.md "wikilink")、[年輕人與公主系列](../Page/年輕人與公主系列.md "wikilink")、[俠盜影子系列等最廣為人知](../Page/俠盜影子系列.md "wikilink")。另外，也曾替[金庸](../Page/金庸.md "wikilink")[捉刀](../Page/捉刀.md "wikilink")《[天龍八部](../Page/天龍八部.md "wikilink")》的數回共四萬多字。1986年受洗為基督徒。\[3\]

### 黃霑

[黃霑原名黃湛森](../Page/黃霑.md "wikilink")，生於[廣東](../Page/廣東.md "wikilink")[廣州](../Page/廣州.md "wikilink")，1949年隨父母移居香港，2004年11月24日因[癌症逝世](../Page/癌症.md "wikilink")，是華人地區最具影響力的填詞人、廣告人、作家及傳媒創作人，也是當代[粵語流行歌曲重要人物之一](../Page/粵語流行歌曲.md "wikilink")，創作接近2000首流行曲。著名填詞作品包括《[獅子山下](../Page/獅子山下.md "wikilink")》、《問我》、《上海灘》、《[滄海一聲笑](../Page/滄海一聲笑.md "wikilink")》等；經典[廣告作品包括家計會](../Page/廣告.md "wikilink")「兩個夠晒數」、「[人頭馬一開](../Page/人頭馬.md "wikilink")，好事自然來」等廣告口號。

### 蔡瀾

[蔡瀾](../Page/蔡瀾.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")[潮州](../Page/潮州.md "wikilink")[華僑](../Page/華僑.md "wikilink")，1963年定居香港，是華人地區最具影響力的美食家、作家之一。他的父親[蔡文玄是一位](../Page/蔡文玄.md "wikilink")[詩人](../Page/詩人.md "wikilink")，受父親影響，他自小文采飛揚，著有不少散文集。美食方面他也具影響力，於各地設立[蔡瀾美食坊](../Page/蔡瀾美食坊.md "wikilink")，也經常主持電視台飲食節目，如《蔡瀾逛菜欄》等。

## 参考文献

<div class="references-small">

<references/>

</div>

[Category:香港作家](../Category/香港作家.md "wikilink") [Category:香港四大
(人物)](../Category/香港四大_\(人物\).md "wikilink")

1.  [黃霑駕鶴去　「巴蜀鬼才」痛惜「香港鬼才」新華網](http://www.sc.xinhuanet.com/content/2004-11/25/content_3280069.htm)

2.  [霑叔既死，「香港四大才子」终成温故符号](http://ent.163.com/ent/editor/music/041125/041125_346096.html)
3.