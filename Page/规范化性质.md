在[数理逻辑和](../Page/数理逻辑.md "wikilink")[理论计算机科学中](../Page/理论计算机科学.md "wikilink")，一个[重写系统有](../Page/重写系统.md "wikilink")**规范化性质**，如果所有项都是**强规范化**的；就是说所有重写序列都最终终止于[规范形式的项](../Page/规范形式.md "wikilink")。

纯粹无类型 [lambda 演算不是强规范化的](../Page/lambda_演算.md "wikilink")。考虑项
\(\lambda x . x x x\)。它有如下重写规则: 对于任何项 *t*，

  -
    \((\mathbf{\lambda} x . x x x) t \rightarrow t t t\)

但是考虑在应用 \(\lambda x . x x x\) 于自身时所发生的:

|                                                      |                                                                                                                              |
| ---------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| \((\mathbf{\lambda} x . x x x) (\lambda x . x x x)\) | \(\rightarrow (\mathbf{\lambda} x . x x x) (\lambda x . x x x) (\lambda x . x x x)\)                                         |
|                                                      | \(\rightarrow (\mathbf{\lambda} x . x x x) (\lambda x . x x x) (\lambda x . x x x) (\lambda x . x x x)\)                     |
|                                                      | \(\rightarrow (\mathbf{\lambda} x . x x x) (\lambda x . x x x) (\lambda x . x x x) (\lambda x . x x x) (\lambda x . x x x)\) |
|                                                      | \(\ldots\,\)                                                                                                                 |

所以项 \((\lambda x . x x x) (\lambda x . x x x)\) 不是规范化的。

各种**有类型 lambda 演算**系统包括[简单类型 lambda
演算](../Page/简单类型_lambda_演算.md "wikilink")，[Jean-Yves
Girard](../Page/Jean-Yves_Girard.md "wikilink")
的[系统F](../Page/系统F.md "wikilink")，和 [Thierry
Coquand](../Page/Thierry_Coquand.md "wikilink")
的[构造演算都有规范化性质](../Page/构造演算.md "wikilink")。

带有规范化性质的 lambda
演算系统可以被看作带有所有程序都[终止性质的编程语言](../Page/终止.md "wikilink")。尽管这是一个非常有用的性质，它也有缺点:
带有规范化性质的编程语言不可能是[图灵完全的](../Page/图灵完全.md "wikilink")。这意味着有可计算函数不能在简单类型的
lambda 演算中定义(类似的有可计算函数不能在构造演算或系统 F 中计算)。

## 参见

  - [lambda 演算](../Page/lambda_演算.md "wikilink")
  - [有类型 lambda 演算](../Page/有类型_lambda_演算.md "wikilink")
  - [重写系统](../Page/重写系统.md "wikilink")
  - [构造演算](../Page/构造演算.md "wikilink")

[Category:Lambda演算](../Category/Lambda演算.md "wikilink")