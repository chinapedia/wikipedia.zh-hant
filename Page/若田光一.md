**若田光一**（），出身於[日本](../Page/日本.md "wikilink")[埼玉縣](../Page/埼玉縣.md "wikilink")[大宮市](../Page/大宮市.md "wikilink")（今[埼玉市](../Page/埼玉市.md "wikilink")），[JAXA](../Page/日本宇宙航空研究開發機構.md "wikilink")[航天員](../Page/航天員.md "wikilink")。2013年至[國際太空站執行任務](../Page/國際太空站.md "wikilink")，並於2014年擔任了兩個月太空站站長，為首位日籍國際太空站站長。

## 生平

他於1987年在[九州大學取得航空工程學士學位](../Page/九州大學.md "wikilink")，1989年取得應用機械碩士學位，他於1992年被[美國太空總署選中並接受訓練](../Page/美國太空總署.md "wikilink")。2004年取得太空工程博士學位。

1996年，他是[奋进号航天飞机代号](../Page/奋进号航天飞机.md "wikilink")[STS-72飞行使命中的日本使命专家](../Page/STS-72.md "wikilink")，机组人员在那次使命中进行了两次太空行走，展示和评估了将用于安装[国际空间站的多项技术](../Page/国际空间站.md "wikilink")。2002年他执行「[发现号](../Page/发现号.md "wikilink")」[STS-92航天使命时](../Page/STS-92.md "wikilink")，成为参加空间站安装工作的第一位日本宇航员。
在历时13天的使命中，7名机组人员组装了空间站的第一主架部分──
[Z1桁架](../Page/Z1桁架.md "wikilink")，并进行了四次[太空行走](../Page/太空行走.md "wikilink")。2009年，若田光一在[STS-119航天使命中作为使命专家](../Page/STS-119.md "wikilink")，参与运送空间站主架的最后一部分，并在在空间站留驻\[1\]。

2013年年底在[国际空间站逗留](../Page/国际空间站.md "wikilink")6个月，并在最后两个月时间里担任站长，為首位日籍國際太空站站長\[2\]。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [NASA Biography
    (英語)](http://www.jsc.nasa.gov/Bios/htmlbios/wakata.html)

[Category:日本航空公司相關人物](../Category/日本航空公司相關人物.md "wikilink")
[Category:日本宇航员](../Category/日本宇航员.md "wikilink")
[Category:埼玉市出身人物](../Category/埼玉市出身人物.md "wikilink")
[Category:九州大學校友](../Category/九州大學校友.md "wikilink")
[Category:國際太空站人物](../Category/國際太空站人物.md "wikilink")

1.
2.