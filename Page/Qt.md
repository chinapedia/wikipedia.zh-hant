**Qt**（，發音同「cute」\[1\]\[2\]\[3\]）是一个[跨平台的](../Page/跨平台.md "wikilink")[C++應用程式開發框架](../Page/C++.md "wikilink")。廣泛用於開發GUI程式，這種情況下又被称为[部件工具箱](../Page/部件工具箱.md "wikilink")。也可用於開發非GUI程式，比如控制台工具和伺服器。Qt使用於[OPIE](../Page/OPIE.md "wikilink")、[Skype](../Page/Skype.md "wikilink")、[VLC
media player](../Page/VLC_media_player.md "wikilink")、[Adobe Photoshop
Elements](../Page/Adobe_Photoshop_Elements.md "wikilink")、[VirtualBox與](../Page/VirtualBox.md "wikilink")[Mathematica](../Page/Mathematica.md "wikilink")\[4\]以及被[Autodesk](../Page/Autodesk.md "wikilink")
\[5\]\[6\]、[歐洲太空總署](../Page/歐洲太空總署.md "wikilink")\[7\]、[夢工廠](../Page/夢工廠.md "wikilink")\[8\]\[9\]、[Google](../Page/Google.md "wikilink")、[HP](../Page/惠普公司.md "wikilink")\[10\]、[KDE](../Page/KDE.md "wikilink")、[盧卡斯影業](../Page/盧卡斯影業.md "wikilink")\[11\]、[西门子公司](../Page/西门子公司.md "wikilink")\[12\]、[沃尔沃集团](../Page/沃尔沃集团.md "wikilink")\[13\],
[华特迪士尼动画制作公司](../Page/华特迪士尼公司.md "wikilink")\[14\]、[三星集团](../Page/三星集团.md "wikilink")\[15\]、[飞利浦](../Page/飞利浦.md "wikilink")\[16\]、[Panasonic](../Page/Panasonic.md "wikilink")
\[17\]所使用。

它是[Digia公司的产品](../Page/Digia.md "wikilink")。Qt使用標準的[C++和特殊的代码生成扩展](../Page/C++.md "wikilink")（称为元对象编译器（Meta
Object Compiler,
moc））以及一些[巨集](../Page/巨集.md "wikilink")。通過語言綁定，其他的程式語言也可以使用Qt。

Qt是自由且開放原始碼的軟體，在[GNU較寬鬆公共許可證](../Page/GNU較寬鬆公共許可證.md "wikilink")（LGPL）條款下發布。所有版本都支援廣泛的編譯器，包括GCC的C++編譯器和Visual
Studio。

## 歷史

| 時間          | Qt版本     |
| ----------- | -------- |
| 1995年       | Qt 1.0   |
| 1998年7月     | Qt 1.4   |
| 1999年4月     | Qt 1.4.4 |
| 1999年6月     | Qt 2.0   |
| 2000年4月     | Qt 2.1   |
| 2000年9月6日   | Qt 2.2   |
| 2001年10月15日 | Qt 3.0   |
| 2002年11月    | Qt 3.1   |
| 2003年7月     | Qt 3.2   |
| 2004年2月     | Qt 3.3   |
| 2005年6月27日  | Qt 4.0   |
| 2005年12月    | Qt 4.1   |
| 2006年10月    | Qt 4.2   |
| 2007年5月     | Qt 4.3   |
| 2008年5月     | Qt 4.4   |
| 2009年3月     | Qt 4.5   |
| 2009年4月     | Qt 4.5.1 |
| 2009年12月    | Qt 4.6   |
| 2010年2月     | Qt 4.6.2 |
| 2010年6月     | Qt 4.6.3 |
| 2010年9月     | Qt 4.7   |
| 2011年5月4日   | Qt 4.7.3 |
| 2011年12月15日 | Qt 4.8   |
| 2012年5月22日  | Qt 4.8.2 |
| 2012年9月13日  | Qt 4.8.3 |
| 2012年12月19日 | Qt 5.0   |
| 2013年1月31日  | Qt 5.0.1 |
| 2013年4月10日  | Qt 5.0.2 |
| 2013年7月3日   | Qt 5.1   |
| 2013年8月28日  | Qt 5.1.1 |
| 2013年12月12日 | Qt 5.2   |
| 2014年5月20日  | Qt 5.3   |
| 2014年12月10日 | Qt 5.4   |
| 2015年7月1日   | Qt 5.5   |
| 2016年3月16日  | Qt 5.6   |
| 2016年6月16日  | Qt 5.7   |

[Haavard Nord和](../Page/Haavard_Nord.md "wikilink")[Eirik
Chambe-Eng於](../Page/Eirik_Chambe-Eng.md "wikilink")1991年開始開發「Qt」，1994年3月4日創立公司，最早名為Quasar
Technologies，然後更名為Troll
Tech，然後再改為Trolltech，中文名是「奇趣科技」，2008年6月17日被NOKIA公司收購，以增强该公司在[跨平台](../Page/跨平台.md "wikilink")[软件研发方面的实力](../Page/软件.md "wikilink")，更名[Qt
Software](../Page/Qt_Software.md "wikilink")。

該工具包名為Qt是因為字母[Q在Haavard的](../Page/Q.md "wikilink")[Emacs字體特別漂亮](../Page/Emacs.md "wikilink")，而“[t](../Page/t.md "wikilink")”代表“toolkit”，灵感來自[Xt](../Page/Intrinsics.md "wikilink")，X
toolkit\[18\]。

2009年5月11日，诺基亚[Qt
Software宣布Qt原始碼管理系统面向公众开放](../Page/Qt_Software.md "wikilink")，Qt开发人员可通过为Qt以及与Qt相关的项目贡献代码、翻译、示例以及其他内容，协助引导和塑造Qt未来的发展。为了便于这些内容的管理，Qt
Software启用了基于Git和Gitorious开源项目的Web原始碼管理系统。

在推出开放式Qt代码库的同时，Qt
Software在其網站发布了其产品规划（Roadmap）。其中概述了研发项目中的最新功能，展现了现阶段对Qt未来发展方向的观点，以期鼓励社区提供反馈和贡献代码，共同引导和塑造Qt的未来。2012年8月9日，[Digia宣布已完成对](../Page/Digia.md "wikilink")[诺基亚Qt业务及软件技术的全面收购](../Page/诺基亚.md "wikilink")，并计划将Qt应用到[Android](../Page/Android.md "wikilink")、[iOS及](../Page/iOS.md "wikilink")[Windows
8平台上](../Page/Windows_8.md "wikilink")。\[19\]

## 支持平台

使用Qt开发的[软件](../Page/软件.md "wikilink")，相同的程式碼可以在任何支援的[平台上](../Page/平台.md "wikilink")[編譯與執行](../Page/編譯.md "wikilink")，而不需要修改原始碼。會自動依平台的不同，表現平台特有的[图形界面风格](../Page/图形界面.md "wikilink")。

  - **Linux/X11**：用於[X Window
    System](../Page/X_Window_System.md "wikilink")（如[Solaris](../Page/Solaris.md "wikilink")、[AIX](../Page/AIX.md "wikilink")、[HP-UX](../Page/HP-UX.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[BSD](../Page/BSD.md "wikilink")）。支援[KDevelop和Eclipse](../Page/KDevelop.md "wikilink")
    IDE集成
  - **Mac**：用於Apple [Mac OS
    X](../Page/Mac_OS_X.md "wikilink")。基於[Cocoa框架](../Page/Cocoa.md "wikilink")。支援Universal
    Binary。支援以[Xcode编辑](../Page/Xcode.md "wikilink")、編譯和測試。
  - **Windows**：用於[Microsoft
    Windows](../Page/Microsoft_Windows.md "wikilink")。支援Visual
    Studio集成，也可以使用[MinGW編譯](../Page/MinGW.md "wikilink")
  - **Embedded
    Linux**：用於[嵌入式Linux](../Page/嵌入式Linux.md "wikilink")。可以透過編譯移除不常使用的组件與功能。透過自己的视窗系统[QWS](../Page/QWS.md "wikilink")，不需依賴X
    Window
    System，直接写入Linux帧缓冲。可以减少記憶體消耗。並提供虚拟帧缓冲[QVFb](../Page/QVFb.md "wikilink")，方便在桌面系统上進行嵌入式測試。
  - '''Windows CE / Mobile '''：用於[Windows
    CE](../Page/Windows_CE.md "wikilink")
  - **Symbian**：用於[Symbian
    platform](../Page/Symbian_platform.md "wikilink")
  - **Maemo/MeeGo**：用於[Maemo](../Page/Maemo.md "wikilink")
  - **Wayland**
    –用於[Wayland顯示伺服器](../Page/Wayland.md "wikilink")，Qt應用程式可以在運行時切換圖形後端，如X與Wayland。\[20\]\[21\]

### 外部移植

自從諾基亞開放了Qt的原始碼給社群後，[Gitorious上各種移植紛紛出現](../Page/Gitorious.md "wikilink")。下面是其中一部份：

  - **Qt for OpenSolaris**
    –用於[OpenSolaris](../Page/OpenSolaris.md "wikilink")\[22\]
  - **Qt for Haiku** –用於[Haiku OS](../Page/Haiku.md "wikilink")\[23\]
  - **Qt for OS/2** –仍然不完整的[OS/2 eCS
    platform移植](../Page/EComStation.md "wikilink")。\[24\]
  - **Qt-iPhone** –用於[iPhone的實驗中產品](../Page/iPhone.md "wikilink")。\[25\]
  - **Android-Lighthouse**
    –用於[Android的實驗中產品](../Page/Android.md "wikilink")。\[26\]
  - **Qt for webOS** –用於[Palm
    Pre上](../Page/Palm_Pre.md "wikilink")[webOS的實驗中產品](../Page/webOS.md "wikilink")。\[27\]
    \[28\]
  - **Qt for Amazon Kindle DX** –用於[Amazon Kindle
    DX的實驗中產品](../Page/Amazon_Kindle#Kindle_DX.md "wikilink")。\[29\]

## 授权模式

Qt[开放源代码](../Page/开放源代码.md "wikilink")，并且提供[自由软件的用户协议](../Page/自由软件.md "wikilink")。使得它可以被广泛地应用在各平台上的[开放源代码](../Page/开放源代码.md "wikilink")[软件开发中](../Page/软件.md "wikilink")。

Qt提供三种授权方式。三种授权方式的功能、性能都没有区别，僅在于授权协议的不同。LGPL和GPL是免費发布，商业版則需收取授权费。\[30\]：

  - Qt商业版 -
    Qt商业授权适用于开发专属和/或商业软件。此版本适用于不希望与他人共享源代码，或者遵循GNU宽通用公共许可证（LGPL）2.1版或GNU
    GPL 3.0版条款的开发人员。提供了技術支援服務。可以任意的修改Qt的原始碼，而不需要公開。

<!-- end list -->

  - [GNU LGPL](../Page/GNU_Lesser_General_Public_License.md "wikilink")
    v. 2.1- Qt 4.5.0及以后的版本开始遵循GNU
    LGPL。LGPL允許鏈結到它的軟體使用任意的許可證，可以被专属软件作为类库引用、发布和销售。可以购买支援服務。

<!-- end list -->

  - [GNU GPL](../Page/GNU_General_Public_License.md "wikilink") v. 3.0 -
    如果您希望将Qt应用程序与受GNU通用公共许可证（GPL）3.0版本条款限制的软件一同使用，或者您希望Qt应用程序遵循该GNU许可证版本的条款，则此版本Qt适用于开发此类Qt应用程序。可以购买支援服務。

## Qt模块

经过多年发展，Qt不但拥有了完善的[C++](../Page/C++.md "wikilink")[图形库](../Page/图形库.md "wikilink")，而且近年来的版本逐渐整合了[数据库](../Page/数据库.md "wikilink")、[OpenGL库](../Page/OpenGL.md "wikilink")、[多媒体库](../Page/多媒体.md "wikilink")、[网络](../Page/网络.md "wikilink")、[脚本库](../Page/脚本.md "wikilink")、[XML库](../Page/XML.md "wikilink")、[WebKit库等等](../Page/WebKit.md "wikilink")，其[核心库也加入了](../Page/核心.md "wikilink")[进程间通信](../Page/进程间通信.md "wikilink")、[多线程等模块](../Page/多线程.md "wikilink")，极大的丰富了Qt开发大规模复杂跨平台[应用程序的能力](../Page/应用程序.md "wikilink")，真正意义上实现了其研发宗旨“Code
Less; Create More; Deploy Anywhere.”。

由於各家編譯器規格不同，Qt本身為了跨平台相容性，-{只}-能以「最低相容規格」來設計。因此Qt必須具備RTTI、动态创建、Persistence/Serialization的基礎建設，以及建構出自己的容器元件。

  - 下列模組提供一般的軟體開發
      - QtCore—QtCore模組是所有基於Qt的應用程式的基礎，提供信號與槽的物件間通訊機制、IO、事件和物件處理、多執行緒
      - QtGui—包含了開發圖形使用者介面應用程式所需的功能。使用其支援的各個平台的原生圖形API。支援反鋸齒、向量形變。支持ARGB顶层widget
      - QtMultimedia—提供了用於多媒體內容處理的QML類型集和C++類集。同時提供訪問攝像頭及音頻功能的API。包含的Qt音頻引擎支持三維音頻回放及管理。
      - QtNetwork—提供了網路程式設計功能。支援通用協定，如HTTP、FTP和DNS，包括對非同步HTTP
        1.1的支援。與較低層的TCP/IP和UDP協議，如QTcpSocket、QTcpServer和QUdpSocket
      - QtOpenGL—提供在應用程式中使用OpenGL和OpenGL
        ES加入3D圖形。在Windows平台上亦支援[Direct3D](../Page/Direct3D.md "wikilink")
      - QtOpenVG-提供OpenVG绘图支持的一个插件
      - [QtScript](../Page/QtScript.md "wikilink")—包含完全整合的ECMA標準指令碼引擎。提供信號與槽機制簡化物件間通訊和QtScript偵錯程式。
      - QtScriptTools—額外的Qt Script組件
      - QtSql—將資料庫整合至應用程式。支援所有主要的資料庫驅動包括[ODBC](../Page/ODBC.md "wikilink")、[MySQL](../Page/MySQL.md "wikilink")、PSQL、[SQLite](../Page/SQLite.md "wikilink")、ibase、Oracle、Sybase、DB2。
      - QtSvg—支援[SVG格式](../Page/SVG.md "wikilink")
      - QtWebKit—整合WebKit，提供了HTML瀏覽器引擎，便於在原生應用程式中嵌入網路內容和服務。
      - QtXml—提供了XML文檔的閱讀器和編寫器、支持[SAX和](../Page/SAX.md "wikilink")[DOM](../Page/DOM.md "wikilink")。
      - QtXmlPatternsl—提供了[XQuery和](../Page/XQuery.md "wikilink")[XPath引擎支持](../Page/XPath.md "wikilink")。
      - [Phonon](../Page/Phonon_\(KDE\).md "wikilink")—整合Phonon，支援跨平台應用程式播放音訊和視訊內容。Qt5開始不支援Phonon。
      - Qt3Support—模塊提供兼容Qt 3.х.х版本的程式庫
      - QtDeclarative \[31\] - engine for declaratively building fluid
        user interfaces in [QML](../Page/QML.md "wikilink")

<!-- end list -->

  - 作業於Qt附帶工具的模組
      - QtDesigner—提供擴充Qt Designer的類別。
      - QtUiTools
      - QtHelp—協助整合線上文件到應用程式中。
      - QtTest—提供單元測試框架和滑鼠和鍵盤類比功能。整合[Visual
        Studio和](../Page/Visual_Studio.md "wikilink")[KDevelop](../Page/KDevelop.md "wikilink")。

<!-- end list -->

  - 下列模組用於Unix開發
      - QtDBus

<!-- end list -->

  - 下列模組用於Windows開發
      - QAxContainer
      - QAxServer

### 圖形使用者介面

Qt的圖形使用者介面的基礎是QWidget。Qt中所有類型的GUI組件如按鈕、標籤、工具列等都衍生自QWidget，而QWidget本身則為QObject的子類別。[Widget負責接收滑鼠](../Page/Widget.md "wikilink")，鍵盤和來自窗口系統的其他事件，並描繪了自身顯示在屏幕上。每一個GUI組件都是一個[widget](../Page/widget.md "wikilink")，[widget還可以作為容器](../Page/widget.md "wikilink")，在其內包含其他[Widget](../Page/Widget.md "wikilink")。

QWidget不是一個抽象類別。並且可以被放置在一個已存在的使用者介面中;若是[Widget沒有指定父](../Page/Widget.md "wikilink")[Widget](../Page/Widget.md "wikilink")，當它顯示時就是一個獨立的視窗、或是一個頂層[widget](../Page/widget.md "wikilink")。QWidget顯示能力包含了透明化及Double-Buffering。Qt提供一種託管機制，當[Widget於建立時指定父物件](../Page/Widget.md "wikilink")，就可把自己的生命週期交給上層物件管理，當上層物件被釋放時，自己也被釋放。確保物件不再使用時都會被刪除。

### 訊號與槽

Qt利用訊號與槽（signals/slots）機制取代傳統的callback來進行物件之間的溝通。当操作事件发生的时候，物件會發送出一個訊號（signal）；而槽（slot）則是一個函式接受特定信號並且執行槽本身設定的動作。信号與槽之間，則透過[QObject的靜態方法connect來連結](../Page/QObject.md "wikilink")。

訊號在任何執行點上皆可發射，甚至可以在槽裡再發射另一個訊號，訊號與槽的連結不限定為一對一的連結，一個訊號可以連結到多個槽或多個訊號連結到同一個槽，甚至訊號也可連接到訊號。

以往的callback缺乏类型安全，在呼叫處理函數時，無法確定是傳遞正確型態的參數。但訊號和其接受的槽之間傳遞的資料型態必須要相符合，否則編譯器會提出警告。訊號和槽可接受任何數量、任何型態的參數，所以訊號與槽机制是完全类型安全。

訊號與槽機制也確保了低耦合性，發送信号的類別並不知道是哪個槽會接受，也就是說一個信号可以呼叫所有可用的槽。此機制會確保當在"連接"信号和槽時，槽會接受信号的參數並且正確執行。

### 布局管理

布局管理類別用於描述一個應用程序的用戶界面中的Widget是如何放置。當視窗縮放時，布局管理器會自動調整widget的大小、位置或是字型大小，確保他們相對的排列和用戶界面整體仍然保有可用性。

Qt內建的布局管理類型有：QHBoxLayout、QVBoxLayout、QGridLayout和QFormLayout。這些類別繼承自QLayout，但QLayout非繼承自QWidget而是直接源於QObject。他們負責widget的幾何管理。想要創建更複雜的版面配置，可以繼承QLayout來自訂版面配置管理員。

  - QHBoxLayout：配置widget成橫向一列
  - QVBoxLayout：配置widget成垂直一行
  - QGridLayout：配置widget在平面網格
  - QFormLayout：配置widget用於2欄標籤- field

### Main Window

Qt提供了下列主視窗管理和相關的用戶界面組件的類別：

  - QMainWindow：提供一個標準的應用程式主視窗。當中可以包括選單、工具列、狀態列、停駐元件等元件。
  - QDockWidget：提供了一個可用於創建彈簧工具調色板或輔助窗口的widget。Dock
    widgets可以移、關閉、浮動為外部視窗。
  - QToolBar：提供了一個通用的工具欄widget，可以放入一些不同的action有關的工具，如按鈕、下拉選單、comboboxes和spin
    boxes。

### Graphics View

Graphics View提供了用于管理和交互大量定制的2D图形对象的平面以及可视化显示对象的视图widget，并支持缩放和旋转功能。

整个Graphics
View框架提供一個以Item為基礎的model-view設計。由3个主要的類別组成，分别是QGrphicsItem、QGraphicsScene和QGraphicsView。若干View可以顯示一個Scene，Scene中則包含不同幾何形狀的Item。

該框架包括一個事件傳播的架構，讓在Scene上的Item有雙精度的互動能力。Item可以處理鍵盤事件，鼠標按下、移動、釋放和雙擊事件，他們也可以跟踪鼠標移動。

Graphics
View使用BSP（二进制空间划分）树可非常快速地找到Item，因此即使是包含百万个Item的大型Scene，也能实时图形化显示。

KDE中的[Plasma亦是基於Graphics](../Page/Plasma_\(KDE\).md "wikilink") View實現的。

### 無障礙環境

無障礙環境需要無障礙兼容的應用程式、輔助技術、以及輔助工具之間的合作。應用程式通常不會直接溝通輔助工具，而是通過一個輔助技術，這是一個應用程式和工具之間資訊交流的橋樑。使用者界面元素相關訊息，例如按鈕和滾動條，使用輔助技術來顯示。Qt支持Windows上的Microsoft
Active Accessibility（MSAA）和Mac OS X上Mac OS X Accessibility。

無障礙相容的應用程式稱為AT-Servers，而輔助工具被稱為AT-Clients。Qt應用程式通常會是一個AT-Server，但特別的程式也可能如同AT-Client方式工作。

### 國際化

Qt的字體引擎能夠在同一時間正確的顯示各種不同的書寫系統。並且Qt內部使用Unicode編碼來儲存文字。

Qt的多國語言支援技術，可以讓應用程式中的文字全部使用英文撰寫，能夠在完全不需修改程式的狀況下，改變整個應用程式中的文字為另一個語系的文字，並能夠協助處理不同語言的單、複數問題。

獨立的翻譯檔案使得新增支援語言相當容易，同時翻譯檔案（.ts）為XML格式可以直接編輯或使用Qt
Liguist進行翻譯，可讓無程式開發能力的翻譯者亦能獨自完成翻譯。Qt附帶的工具程式就能夠自動抽取需要翻譯的文字產生翻譯檔案。

### 多執行緒

Qt的執行緒支持是獨立於平台的執行緒類別，采用訊號與槽机制，实现类型安全的執行緒间通讯。這使得它易於開發具可移植性的多執行緒Qt應用程式。並能充分利用多核架构，获得最佳运行性能，還能根据可用的处理器内核数自动调整使用的執行緒数。多執行緒程式設計也是一個执行耗時操作而不会冻结用户界面的有效典范。

### 語言綁定

除了[C++外](../Page/C++.md "wikilink")，Qt还为其它多种[计算机语言提供了](../Page/计算机语言.md "wikilink")[应用程序接口](../Page/API.md "wikilink")，您也可以使用这些语言开发Qt[应用程序](../Page/应用程序.md "wikilink")。

<table>
<caption>style="font-size: 1.25em;" | <strong>Qt語言綁定</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/程式語言.md" title="wikilink">語言</a></p></th>
<th><p>名稱 - 綁定描述</p></th>
<th><p>QtCore</p></th>
<th><p>QtDesigner</p></th>
<th><p>QtGui</p></th>
<th><p>QtNetwork</p></th>
<th><p>QtOpenGL</p></th>
<th><p>QtSql</p></th>
<th><p>QtScript</p></th>
<th><p>QtSvg</p></th>
<th><p>QtTest</p></th>
<th><p>QtUiTools</p></th>
<th><p>QtWebKit</p></th>
<th><p>QtXml</p></th>
<th><p><a href="../Page/開放原始碼.md" title="wikilink">開放原始碼軟體的</a><a href="../Page/軟體授權.md" title="wikilink">授權</a></p></th>
<th><p><a href="../Page/專有軟體.md" title="wikilink">專有軟體的</a><a href="../Page/軟體授權.md" title="wikilink">授權</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/Ada.md" title="wikilink">Ada</a></p></td>
<td><p><a href="https://web.archive.org/web/20090412122608/http://www.qtada.com/en/index.html">QtAda</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>[32]</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>+ fee</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/C++.md" title="wikilink">C++</a></p></td>
<td><p><a href="https://web.archive.org/web/20121226190411/http://qt.nokia.com/">Qt</a> – native C++</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/C_Sharp.md" title="wikilink">C#</a> &amp; <a href="../Page/.NET.md" title="wikilink">.NET</a></p></td>
<td><p><a href="../Page/Qyoto.md" title="wikilink">Qyoto</a> – See also <a href="https://web.archive.org/web/20100122081834/http://ekarchive.elikirk.com/david/qyoto/">Kimono</a> for <a href="../Page/KDE.md" title="wikilink">KDE</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>C# &amp; .NET</p></td>
<td><p><a href="http://code.google.com/p/qt4dotnet/">qt4dotnet</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/D語言.md" title="wikilink">D語言</a></p></td>
<td><p><a href="http://www.dsource.org/projects/qtd">QtD</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/道語言.md" title="wikilink">道語言</a></p></td>
<td><p><a href="http://code.google.com/p/daoqt/">DaoQt</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Haskell.md" title="wikilink">Haskell</a></p></td>
<td><p><a href="https://web.archive.org/web/20120105175400/http://qthaskell.berlios.de/">Qt Haskell</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Harbour_compiler.md" title="wikilink">Harbour</a></p></td>
<td><p><a href="https://web.archive.org/web/20120605041921/http://www.harbour-project.org/">hbqt</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Java.md" title="wikilink">Java</a></p></td>
<td><p><a href="../Page/Qt_Jambi.md" title="wikilink">Qt Jambi</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Javascript.md" title="wikilink">Javascript</a>（<a href="../Page/node.js.md" title="wikilink">node.js</a>）</p></td>
<td><p><a href="https://github.com/arturadib/node-qt">node-qt</a></p></td>
<td><p>僅有少數幾個類</p></td>
<td></td>
<td><p>僅有少數幾個類</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>僅有少數幾個類</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Lisp.md" title="wikilink">Lisp</a></p></td>
<td><p><a href="http://common-lisp.net/project/commonqt/">CommonQt</a> – Bindings for <a href="../Page/Common_Lisp.md" title="wikilink">Common Lisp</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Lua.md" title="wikilink">Lua</a></p></td>
<td><p><a href="http://code.google.com/p/lqt">lqt</a> - Bindings</p></td>
<td></td>
<td><p>[33]</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Lua</p></td>
<td><p><a href="http://www.nongnu.org/libqtlua">QtLua</a> - Bindings and script engine</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Pascal.md" title="wikilink">Pascal</a></p></td>
<td><p><a href="http://wiki.freepascal.org/Qt_Interface">FreePascal Qt4</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Perl.md" title="wikilink">Perl</a></p></td>
<td><p><a href="http://code.google.com/p/perlqt4/">PerlQt4</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PHP.md" title="wikilink">PHP</a></p></td>
<td><p><a href="https://www.openhub.net/p/php-qt">PHP-Qt</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Python.md" title="wikilink">Python</a></p></td>
<td><p><a href="../Page/PyQt.md" title="wikilink">PyQt</a> – has an associated text (ISBN 0132354187).</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>+ fee</p></td>
</tr>
<tr class="even">
<td><p>Python</p></td>
<td><p><a href="../Page/PySide.md" title="wikilink">PySide</a> – from OpenBossa (a subsidiary of nokia).</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Python</p></td>
<td><p><a href="http://pythonqt.sourceforge.net/">PythonQt</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/R語言.md" title="wikilink">R</a></p></td>
<td><p><a href="https://r-forge.r-project.org/projects/qtinterfaces">qtbase</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Ruby.md" title="wikilink">Ruby</a></p></td>
<td><p><a href="../Page/QtRuby.md" title="wikilink">QtRuby</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Tcl.md" title="wikilink">Tcl</a></p></td>
<td><p><a href="http://sourceforge.net/projects/qtcl/">qtcl</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/程式語言.md" title="wikilink">語言</a></p></td>
<td><p>名稱 - 綁定描述</p></td>
<td><p>QtCore</p></td>
<td><p>QtDesigner</p></td>
<td><p>QtGui</p></td>
<td><p>QtNetwork</p></td>
<td><p>QtOpenGL</p></td>
<td><p>QtSql</p></td>
<td><p>QtScript</p></td>
<td><p>QtSvg</p></td>
<td><p>QtTest</p></td>
<td><p>QtUiTools</p></td>
<td><p>QtWebKit</p></td>
<td><p>QtXml</p></td>
<td><p><a href="../Page/開放原始碼.md" title="wikilink">開放原始碼軟體的</a><a href="../Page/軟體授權.md" title="wikilink">授權</a></p></td>
<td><p><a href="../Page/專有軟體.md" title="wikilink">專有軟體的</a><a href="../Page/軟體授權.md" title="wikilink">授權</a></p></td>
</tr>
</tbody>
</table>

### Qt的hello world

将下面的代码保存到Hello.cpp中

``` cpp
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QLabel label("Hello, world!");
    label.show();
    return app.exec();
}
```

### 編譯與執行

1.  新建一個文件夾，命名為Hello
2.  將程式碼Hello.cpp放入Hello文件夾
3.  在Hello文件夾執行下列命令
    1.  ``` bash
        qmake -project
        ```

    2.  ``` bash
        qmake
        ```

    3.  ``` bash
        make/gmake/nmake
        ```

        这个取决于您的系统与编译器设置
4.  执行./release/Hello（在Windows中是release\\Hello.exe）

## 工具

Qt提供了一些命令列和圖形工具，以方便和加速開發的過程。

  - [Qt Creator](../Page/Qt_Creator.md "wikilink")：輕量級的Qt/C++
    [IDE開發環境](../Page/IDE.md "wikilink")。
  - [qmake](../Page/qmake.md "wikilink")：跨平台构建工具，可简化跨不同平台进行项目开发的构建过程。
  - Qt Designer：界面設計師。可以用拖拽的方式將Widget排放在界面上，支援版面配置，支援信號與槽編輯。
  - Qt Assistant：Qt助手。Qt在線幫助文件查看工具。
  - Qt Liguist：翻譯工具。讀取翻譯檔案（如.ts、.po）並為翻譯人員提供友好的翻譯介面。
  - lupdate：从原始碼文件或其他资源檔案中提取需要翻译的字符串，并将之存入xml格式的.ts檔案中。
  - lrelease：负责将.ts檔案转化为程序使用的.qm檔案。.qm檔會去掉.ts檔中所有的空白和未翻譯的內容，並將存儲格式壓縮。
  - lconvert：用於翻譯檔案之間的格式轉換。
  - QVFb：虚拟帧缓存设备，模擬framebuffer裝置（尺寸、色深），還可以透過skin模擬硬體鍵盤的布局（包括特殊的按鍵），可以便捷在桌面系統機器上開發嵌入式程式
  - makeqpf：創建用於嵌入式設備的qpf格式。qpf是一種預先渲染的字體，直接保存成二進位內容，使用的時候可以用記憶體對映載入，從載入到繪製的過程不需要計算。
  - uic：User Interface Compiler。從用戶界面的檔案（.ui）生成C++代碼。
  - rcc：Resource Compiler。rcc工具根据.qrc檔案的内容将相关的資源在編譯過程中嵌入到Qt應用程序。
  - qtconfig：基於X11的Qt的配置工具和在線幫助。
  - qconfig：Qt Embedded（Linux和Windows CE）配置工具。
  - qtdemo：Qt的實例和示範項目的加载器。
  - qt3to4：協助移植[Qt 3程式到](../Page/Qt_3.md "wikilink")[Qt
    4的工具](../Page/Qt_4.md "wikilink")。
  - qdbusxml2cpp：QtDBus XML compiler。将xml格式的D-Bus介面描述轉換成為C++原始碼
  - D-Bus Viewer：可以檢視D-Bus物件和信息的工具。
  - Qt Visual Studio Add-in：Visual Studio集成
  - Qt Eclipse Integration：Eclipse集成

## Qt Solutions

Qt Solutions提供Qt額外的組件和工具，使Qt的開發更有效率。在Qt 4.5之後，Qt Solutions加入了LGPL的授權

  - 平台和特定行業的組件和工具
  - 整合Qt與特定第三方產品的組件和工具
  - 尖端的元件和新的工具也會被直接加入在Qt框架中發布

## 使用

### 使用Qt的UI環境

  - [KDE Plasma
    Workspaces](../Page/KDE_Plasma_Workspaces.md "wikilink")：著名的跨平台圖型環境。
  - [MeeGo](../Page/MeeGo.md "wikilink")：基于Linux的开源手机操作系统
  - [Motorola
    A760](../Page/Motorola_A760.md "wikilink")：UI以Qt/Embedded建構
  - [OPIE](../Page/OPIE.md "wikilink")
  - [Qt Extended Improved](../Page/Qt_Extended_Improved.md "wikilink")
  - [Razor-qt](../Page/Razor-qt.md "wikilink")：桌面环境\[34\]
  - [Symbian自第四版开始](../Page/Symbian.md "wikilink")

### 視窗管理員

以下的視窗管理員，使用了Qt：

  - [EggWM](../Page/EggWM.md "wikilink")\[35\]
  - [integrity](../Page/integrity_\(window_manager\).md "wikilink")
  - [KWin](../Page/KWin.md "wikilink")
  - [qlwm](../Page/qlwm.md "wikilink")
  - [deepin-wm](../Page/deepin-wm.md "wikilink")

### 應用程式

一些出名的例子如下：

  - [3DSlicer](../Page/3DSlicer.md "wikilink"), a free open source
    software for visualization and medical image computing
  - [AcetoneISO](../Page/AcetoneISO.md "wikilink")：映像檔掛載軟體
  - [Adobe Photoshop
    Album](../Page/Adobe_Photoshop_Album.md "wikilink"), an image
    organizing application\[36\]
  - [Arora](../Page/Arora.md "wikilink")：一款跨平台的開源網頁瀏覽器
  - [Autodesk MotionBuilder](../Page/MotionBuilder.md "wikilink"),
    professional 3D character animation software
  - [Autodesk Maya](../Page/Maya.md "wikilink"), 3D建模和動畫軟體
  - [Avidemux](../Page/Avidemux.md "wikilink"), a [Free
    Software](../Page/Free_Software.md "wikilink") program designed for
    multi-purpose [video
    editing](../Page/non-linear_editing_system.md "wikilink") and
    processing
  - [Avogadro](../Page/Avogadro.md "wikilink")：進階分子編輯器
  - [BOUML](../Page/BOUML.md "wikilink")，a free uml toolbox
  - [chmcreator](../Page/chmcreator.md "wikilink")：开源的chm开发工具
  - [CineFX](../Page/Cinefx.md "wikilink")：一款跨平台、開源、免費、影片剪輯，特效與合成套裝
  - [CoCoA](../Page/CoCoA.md "wikilink"), a software for computations in
    commutative algebra
  - [Dash Express](../Page/Dash_Express.md "wikilink"), an
    Internet-enabled personal navigation device
  - [DAZ Studio](../Page/DAZ_Studio.md "wikilink"), a 3D figure
    illustration/animation application
  - [Doxygen](../Page/Doxygen.md "wikilink")：API文件產生器
  - [EAGLE](../Page/EAGLE.md "wikilink"), tool for designing printed
    circuit boards (PCBs)
  - [EiskaltDC++](../Page/EiskaltDC++.md "wikilink"), a program that
    uses the Direct Connect protocol.
  - [Emergent](../Page/Emergent.md "wikilink")：[神經網路模擬器](../Page/神經網路.md "wikilink")。
  - [FEKO](../Page/FEKO.md "wikilink"), a software product for the
    simulation of electromagnetic fields
  - [eva](../Page/Eva_IM_client.md "wikilink")：Linux版[QQ聊天軟體](../Page/QQ.md "wikilink")。
  - [FreeCAD](../Page/FreeCAD_\(Juergen_Riegel\).md "wikilink"), a free
    and open source 3D-Solid and general purpose design CAD/CAE
  - [FreeMat](../Page/FreeMat.md "wikilink")：一個自由開源的數值計算環境和程式語言
  - [Full Tilt Poker](../Page/Full_Tilt_Poker.md "wikilink"), one of the
    most popular online poker programs
  - [Gadu-Gadu](../Page/Gadu-Gadu.md "wikilink")：即時通訊軟體
  - [Gambas](../Page/Gambas.md "wikilink"), A free development
    environment based on a Basic interpreter
  - [GoldenDict](../Page/GoldenDict.md "wikilink")：一款開源的字典軟體
  - [Google地球](../Page/Google地球.md "wikilink")（Google Earth）：三維虛擬地圖軟體。
  - [GNS](../Page/GNS.md "wikilink")：Cisco网络模拟器。
  - [Guitar Pro 6](../Page/Guitar_Pro.md "wikilink"), a tablature editor
  - [刺蝟大作戰](../Page/刺蝟大作戰.md "wikilink")：一個基於百戰天蟲的開源遊戲。
  - [Hydrogen](../Page/Hydrogen.md "wikilink"), an advanced drum machine
  - [ImageVis3D](../Page/ImageVis3D.md "wikilink"), a volume ray-casting
    application
  - [Ipe](../Page/Ipe.md "wikilink")：自由的向量圖形編輯器
  - [ISE Webpack](../Page/ISE_Webpack.md "wikilink"), a freeware
    [EDA](../Page/電子設計自動化.md "wikilink") tool for Windows and Linux
    developed by [Xilinx](../Page/Xilinx.md "wikilink")
  - [Kadu](../Page/Kadu.md "wikilink"), a Polish instant messenger using
    the Gadu-Gadu protocol
  - [KDELibs](../Page/KDELibs.md "wikilink")：一個許多[KDE程式都使用的](../Page/KDE程序列表.md "wikilink")[共享庫](../Page/共享庫.md "wikilink")，如[Amarok](../Page/Amarok.md "wikilink")、[K3b](../Page/K3b.md "wikilink")、[KDevelop](../Page/KDevelop.md "wikilink")、[KOffice等](../Page/KOffice.md "wikilink")。
  - [KeePassX](../Page/KeePassX.md "wikilink"), a multi-platform port of
    KeePass, an open source password manager for Microsoft Windows
  - [Last.fm播放器](../Page/Last.fm.md "wikilink")：著名的網際網路音樂社群網站的桌面用戶端。
  - [Launchy](../Page/Launchy.md "wikilink")：一個[開放源碼的快捷啟動器](../Page/開放源碼.md "wikilink")
  - [LMMS](../Page/LMMS.md "wikilink")：一個[開放源碼的音樂編輯軟體](../Page/開放源碼.md "wikilink")
  - [LyX](../Page/LyX.md "wikilink")：使用Qt作為界面的[LaTeX軟體](../Page/LaTeX.md "wikilink")。
  - [Mathematica](../Page/Mathematica.md "wikilink")：Linux和Windows版本使用Qt作為GUI
  - [Maxwell Render](../Page/Maxwell_Render.md "wikilink"), a software
    package that aids in the production of photorealistic images from
    computer 3D model data
  - [Mixxx](../Page/Mixxx.md "wikilink")：跨平台的開放源碼DJ混音軟體
  - [MuseScore](../Page/MuseScore.md "wikilink")，一個[WYSIWYG的樂譜編輯器](../Page/WYSIWYG.md "wikilink")
  - [MythTV](../Page/MythTV.md "wikilink")：開源的數位視訊錄製軟體。
  - [Nuke](../Page/Nuke.md "wikilink"), a node-based compositor
  - [PDFedit](../Page/PDFedit.md "wikilink")：自由的PDF編輯器
  - [PokerTH](../Page/PokerTH.md "wikilink"), an open source Texas hold
    'em simulator
  - [Psi](../Page/Psi_\(即時通訊軟體\).md "wikilink")：一款XMPP網路協定的即時通訊軟體
  - [qBittorrent](../Page/qBittorrent.md "wikilink")：自由的BitTorrent
    P2P客戶端
  - [QCad](../Page/QCad.md "wikilink")：一個用於二維設計及繪圖的CAD軟體
  - [Qjackctl](../Page/Qjackctl.md "wikilink"), a tool for controlling
    the JACK Audio Connection Kit
  - [QSvn](../Page/QSvn.md "wikilink"), a GUI Subversion client for
    Linux, UNIX, Mac OS X and Windows
  - [Opera](../Page/Opera.md "wikilink")：著名的網頁瀏覽器。
  - [Qt Creator](../Page/Qt_Creator.md "wikilink"), the [free
    software](../Page/Free_and_open_source_software.md "wikilink")
    [cross-platform](../Page/cross-platform.md "wikilink") [integrated
    development
    environment](../Page/integrated_development_environment.md "wikilink")
    from [Nokia](../Page/Nokia.md "wikilink")
  - [Qterm](../Page/Qterm.md "wikilink")：跨平台的[BBS軟體](../Page/BBS.md "wikilink")。
  - [Quantum
    GIS](../Page/Quantum_GIS.md "wikilink")：自由的桌面[GIS](../Page/GIS.md "wikilink")
  - [Quassel IRC](../Page/Quassel_IRC.md "wikilink")：跨平台的IRC客戶端
  - [QupZilla](../Page/QupZilla.md "wikilink")：跨平台的開放原始碼的[Webkit網頁瀏覽器](../Page/Webkit.md "wikilink")。
  - [RealFlow](../Page/RealFlow.md "wikilink"), a fluid and dynamics
    simulator for the 3D industry
  - [Recoll](../Page/Recoll.md "wikilink")：桌面搜尋工具
  - [Rosegarden](../Page/Rosegarden.md "wikilink"), a free software
    digital audio workstation program
  - [SciDAVis](../Page/SciDAVis.md "wikilink"), a cross-platform
    plotting and data analysis program
  - [Scribus](../Page/Scribus.md "wikilink")：桌面排版軟體。
  - [Skype](../Page/Skype.md "wikilink")：一個使用人數眾多的基於[P2P的](../Page/P2P.md "wikilink")[VOIP聊天軟體](../Page/VOIP.md "wikilink")。
  - [SMPlayer](../Page/SMPlayer.md "wikilink")：跨平台多媒體播放器
  - [Spotify](../Page/Spotify.md "wikilink"), music streaming service.
  - [Stellarium](../Page/Stellarium.md "wikilink")：一款天文學的自由軟體
  - [TeamSpeak](../Page/TeamSpeak.md "wikilink")：跨平台的音效通訊軟體
  - [Texmaker](../Page/Texmaker.md "wikilink")：一款跨平台的開放源碼LaTeX編輯器
  - [TeXworks](../Page/TeXworks.md "wikilink"), It is a graphical user
    interface to the typesetting system TeX.
  - [Tlen.pl](../Page/Tlen.pl.md "wikilink")：波蘭人發明的即時通訊客戶端
  - [TOra](../Page/TOra.md "wikilink"), a database administration
    tool\[37\]
  - [UMPlayer](../Page/UMPlayer.md "wikilink"):基於Mplayer的美觀多媒體播放器。
  - [UniversalIndentGUI](../Page/UniversalIndentGUI.md "wikilink")，an
    application which helps the user to beautify, reformat or indent
    various kinds of code.
  - [Valknut](../Page/Valknut.md "wikilink"), a program that uses the
    Direct Connect protocol
  - [VirtualBox](../Page/VirtualBox.md "wikilink")：虛擬機器軟體。
  - [VisIt](../Page/VisIt.md "wikilink")：一個開源型互動式并行可視化與圖形分析工具，用於查看科學數據。
  - [VisTrails](../Page/VisTrails.md "wikilink"), a scientific workflow
    management and visualization system
  - [VLC多媒體播放器](../Page/VLC多媒體播放器.md "wikilink")：一個體積小巧、功能強大的開源媒體播放器。
  - [VoxOx](../Page/VoxOx.md "wikilink"), a unified communications
    software.
  - [WordPress](../Page/WordPress.md "wikilink"), based on Qt for Maemo
    and Symbian
  - [wpa supplicant](../Page/wpa_supplicant.md "wikilink"), a free
    software implementation of an IEEE 802.11i
  - [Xconfig](../Page/Xconfig.md "wikilink")：Linux的Kernel配置工具
  - [YY语音](../Page/YY语音.md "wikilink")：又名“歪歪语音”，是一个可以进行在线多人语音聊天和语音会议的免费软件。在中国大陆拥有庞大的用户群。
  - [咪咕音乐](../Page/咪咕音乐.md "wikilink")：咪咕音乐是中国移动倾力打造的正版音乐播放器\[38\]
  - [WPS
    Office](../Page/WPS_Office.md "wikilink")：金山公司（Kingsoft）出品的办公软件，与微软Office兼容性良好，个人版免费。

## 参见

  - [Qt Development
    Frameworks](../Page/Qt_Development_Frameworks.md "wikilink")
  - [Qt Quick](../Page/Qt_Quick.md "wikilink")
  - [QML](../Page/QML.md "wikilink")
  - [Advanced Component
    Framework](../Page/Advanced_Component_Framework.md "wikilink")

## 書目

  -
  -
  -
  -
  -
  -
  -
  -
## 参考文献

## 外部連結

  -   - [中文官方网站](https://www.qt.io/cn/)

[Category:Qt](../Category/Qt.md "wikilink")
[Category:C++](../Category/C++.md "wikilink")
[Category:部件工具箱](../Category/部件工具箱.md "wikilink")
[Category:软件开发](../Category/软件开发.md "wikilink")
[Category:应用程序接口](../Category/应用程序接口.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19. [Digia完成收购诺基亚已交出Qt全部业务_科技频道_凤凰网](http://tech.ifeng.com/digi/mobile/news/detail_2012_08/10/16695296_0.shtml)
20.
21.
22. [KDE on
    OpenSolaris](http://hub.opensolaris.org/bin/view/Project+kde/)
23.
24. [Qt 4 Application and UI Framework for
    eCS](http://svn.netlabs.org/qt4)
25.
26.
27. [Qt webOS port](http://gitorious.org/~darronb/qt/qt-palm-pre.git)
28. [Blog: Qt on the Palm
    Pre](http://www.griffin.net/2010/02/qt-on-the-palm-pre.html)
29. [Blog: Qt on Amazon Kindle
    DX](http://www.griffin.net/2010/01/hacking-the-amazon-kindle-dx-part-2-qt-and-sudoku.html)
30.
31.
32.  Supported Qt modules in QtAda
33. [1](http://code.google.com/p/lqt) Supported Qt modules in lqt
34.
35. [Egg Window
    Manager](http://qt-apps.org/content/show.php/Egg+Window+Manager?content=136862)
36. [Qt Software—Adobe Photoshop Elements
    Album](http://www.qtsoftware.com/qt-in-use/story/app/adobe-photoshop-album)

37. [TOra uses the Qt library](http://tora.sourceforge.net/)
38. [Qt为中国移动音乐客户端提供多平台支持](http://qt.nokia.com/about-us-cn/news/from-qt-blog-qt-in-use-qt-helps-power-china-mobile-music-client-for-multiple-platforms/)