**天王星號**或**尤利納斯號**（、192?年 -
1945年3月28日）是1932年[洛杉磯奧林匹克運動會](../Page/1932年夏季奧林匹克運動會.md "wikilink")[馬術障礙賽](../Page/馬術.md "wikilink")[金牌得主](../Page/金牌.md "wikilink")[日本陸軍](../Page/大日本帝國陸軍.md "wikilink")[大佐](../Page/上校.md "wikilink")[西竹一的愛馬](../Page/西竹一.md "wikilink")。於[法國出生](../Page/法國.md "wikilink")。品種是[盎格魯諾曼](../Page/盎格魯諾曼.md "wikilink")\<\!---\>アングロノルマン\<---\>，血統則不明。[馬毛色為深栗子色](../Page/馬毛色.md "wikilink")（）。

## 概要

1930年4月，天王星由西竹一於[意大利購入](../Page/意大利.md "wikilink")。當時原馬主今村安不能駕御天王星並想將其出售的事傳到西竹一那裡，就這樣西竹一在試騎後以私費支付了6000[里拉](../Page/意大利里拉.md "wikilink")。天王星的特徵為額上有[星形](../Page/星.md "wikilink")，身高（到肩為止的高度）達181厘米的頗大馬身。據說其性格相當激烈，除西竹一外無人能駕馭。西竹一與天王星曾多次在歐洲不同的大會中獲獎，而在洛杉磯奧林匹克運動會更獲得[金牌](../Page/金牌.md "wikilink")。其他的，是在4年後參加[柏林奧林匹克運動會等](../Page/1936年夏季奧林匹克運動會.md "wikilink")。而於洛杉磯奧林匹克運動會中，天王星在飛越160厘米障礙之際自行修正馬身而防範了錯誤為其留下的一大逸話。

天王星在引退後被送進[馬事公苑渡過余生](../Page/馬事公苑.md "wikilink")，其後西竹一於[硫磺岛之戰戰死](../Page/硫磺島戰役.md "wikilink")，天王星後來就像追隨西竹一樣而病死。其遺體有一說為埋葬於馬事公苑，而另一說為在[陸軍獸醫學校埋葬後因空襲而丟失](../Page/軍學校.md "wikilink")。戰後，西竹一在硫磺島臨終時所手握天王星的鬃毛被美國發現，現在收藏於[本別町歷史民俗資料館](https://web.archive.org/web/20070826111828/http://www.pref.hokkaido.jp/kseikatu/ks-bssbk/bunrec/sisetu/ho05/ho050001/index.html)之中。
[baronnishi.jpg](https://zh.wikipedia.org/wiki/File:baronnishi.jpg "fig:baronnishi.jpg")

## 關連項目

  - [騎兵](../Page/騎兵.md "wikilink")
  - [栗林忠道](../Page/栗林忠道.md "wikilink")
  - [來自硫磺島的信](../Page/來自硫磺島的信.md "wikilink") ※上述關於天王星及其小故事於劇中有所觸及。

## 外部連結

  -
[Uramesu](../Category/競賽馬匹.md "wikilink")
[Uramesu](../Category/1945年去世的动物.md "wikilink")
[Uramesu](../Category/名馬.md "wikilink")