**格拉玛号**（）是一艘[游艇](../Page/游艇.md "wikilink")，1956年它载着82位[古巴革命战士从](../Page/古巴革命.md "wikilink")[墨西哥前往](../Page/墨西哥.md "wikilink")[古巴](../Page/古巴.md "wikilink")。

格拉玛号建造于1943年，长63英尺（19.25米），设计容纳20人。由向[菲德尔·卡斯特罗提供武器的西班牙裔墨西哥人安东尼奥](../Page/菲德尔·卡斯特罗.md "wikilink")·德·康德（Antonio
del Conde）（卡斯特罗叫他El Cuate）借给卡斯特罗使用，希望帮助他带着士兵们重返古巴 。

安东尼奥·德·康德拥有墨西哥最后一家合法的武器店，他与[菲德尔·卡斯特罗相识后](../Page/菲德尔·卡斯特罗.md "wikilink")，表示愿意全力以赴帮助卡斯特罗和古巴人民进行革命。闲聊时，安东尼奥提起自己刚刚买了一辆游艇，准备带家人出游，卡斯特罗说：“你要是能把这艘游艇借我一用，这场革命我就能赢！”安东尼奥很爽快地把船借给了卡斯特罗，革命胜利之后，卡斯特罗买下了这艘游艇。

得到游艇后，在[菲德尔·卡斯特罗的领导下](../Page/菲德尔·卡斯特罗.md "wikilink")，82名在墨西哥接受了一年严格军事训练的优秀古巴士兵上了船，开始反抗古巴统治者[巴蒂斯塔的远征](../Page/巴蒂斯塔.md "wikilink")。他们被称为“格拉玛远征队”，其成员包括[切·格瓦拉](../Page/切·格瓦拉.md "wikilink")、[劳尔·卡斯特罗和](../Page/劳尔·卡斯特罗.md "wikilink")[卡米洛·西恩富戈斯等人](../Page/卡米洛·西恩富戈斯.md "wikilink")。1956年11月25日凌晨1点，雨夜，他们从[墨西哥城东北约三百英里的](../Page/墨西哥城.md "wikilink")[韦拉克鲁斯州的](../Page/韦拉克鲁斯州.md "wikilink")[图克斯潘港口启航前往古巴](../Page/图克斯潘.md "wikilink")。

安东尼奥顺着他们的航行路线在墨西哥一路开车护送船只，与他们保持通讯，确保他们的平安。直到到达墨西哥离古巴最近的港口，他无法再向前行驶，才与士兵们分开。由于FBI为当时的古巴政府提供了错误的情报，古巴政府一直以为这艘满载着士兵的船会是蓝顶的（实际上是绿顶），也一直以为这艘船会很大，因此他们靠岸时并没有遇到任何阻碍。船只在行驶了七天之后，于1956年12月2日抵达古巴，82名士兵登陆后，遭政府军伏击，损失惨重，仅剩十余人转入山区活动。

这艘游船于1976年11月14日开始，就在[哈瓦那的](../Page/哈瓦那.md "wikilink")[革命博物馆永久展出](../Page/革命博物馆_\(古巴\).md "wikilink")。为了纪念格拉玛号，这次远征队登陆的地方，原东方省的一部分改名为格拉玛省；格拉玛号登陆日，12月2日被定为[古巴革命武装力量的建军节](../Page/古巴革命武装力量.md "wikilink")；另外，[古巴共产党](../Page/古巴共产党.md "wikilink")[中央委员会机关报也称作](../Page/中央委员会.md "wikilink")《[格拉玛报](../Page/格拉玛报.md "wikilink")》。

## 外部链接

  - [¿Cómo se compró el Yate
    Granma?](http://www.tribuna.islagrande.cu/curiosas/yate3d.htm)

  - [格拉玛登陆，historyofcuba.com](http://www.historyofcuba.com/history/granma.htm)

  - [格拉玛纪念馆的照片l](https://web.archive.org/web/20070918082635/http://www.phogle.com/en/photos/2521.html)

  - [格拉玛号的照片](https://web.archive.org/web/20060519113752/http://www.cuba.cu/politica/webpcc/imagenes/yategran.gif)

  - [话剧《切·格瓦拉》第一幕：格拉玛号启航（中文）](http://ent.sina.com.cn/h/27317.html)

  - [古巴革命中的格瓦拉（中文）](http://book.sina.com.cn/longbook/cha/1111634578_Guevara/17.shtml)

[Category:遊艇](../Category/遊艇.md "wikilink")
[Category:古巴历史](../Category/古巴历史.md "wikilink")