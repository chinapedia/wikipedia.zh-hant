**包姚**（[匈牙利语](../Page/匈牙利语.md "wikilink")：****）是[匈牙利](../Page/匈牙利.md "wikilink")[巴奇-基什孔州的一座城市](../Page/巴奇-基什孔州.md "wikilink")，位于[多瑙河畔](../Page/多瑙河.md "wikilink")，面积177.61平方公里，人口37,690人（2005年）。

## 友好城市

  - [法国](../Page/法国.md "wikilink")[阿让唐](../Page/阿让唐.md "wikilink")

  - [塞尔维亚](../Page/塞尔维亚.md "wikilink")[松博尔](../Page/松博尔.md "wikilink")

  - [德国](../Page/德国.md "wikilink")[魏布林根](../Page/魏布林根.md "wikilink")

  - [匈牙利](../Page/匈牙利.md "wikilink")[霍德梅泽瓦沙海伊](../Page/霍德梅泽瓦沙海伊.md "wikilink")

  - [罗马尼亚](../Page/罗马尼亚.md "wikilink")[特尔古穆列什](../Page/特尔古穆列什.md "wikilink")

  - [罗马尼亚Sangeorgiu](../Page/罗马尼亚.md "wikilink") de Padure

  - [丹麦](../Page/丹麦.md "wikilink")[齐斯泰兹](../Page/齐斯泰兹.md "wikilink")

  - [克罗地亚](../Page/克罗地亚.md "wikilink")[拉宾](../Page/拉宾_\(克罗地亚\).md "wikilink")

## 外部链接

  - [Baja's public homepage](http://www.baja.hu)
  - [Ady Endre Library's home page (English; German
    available)](http://www.c3.hu/~aevk/english.htm)
  - [Baja & Bacska sportnews](http://www.bacskaisport.hu/)
  - [Baja Story magazine](http://story.bacskaisport.hu/)
  - [Home page of Hotel Duna\*\*\* (English; German
    available)](http://hotelduna.hu/index.php?aktualNyelv=english)
  - [Observatory Home Page; English as well.](http://www.bajaobs.hu)
  - [City
    map](https://web.archive.org/web/20060914171143/http://www.terkepcentrum.hu/index.asp?go=map&mid=3&tid=3522)
  - [Aerialphotographs from
    Baja](http://www.civertan.hu/legifoto/legifoto.php?page_level=38)

[B](../Category/匈牙利城市.md "wikilink")