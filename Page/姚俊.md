**姚俊**，上海人，是已退役的中国职业足球运动员，司职右后卫。

中国足球职业化伊始，姚俊入选[上海申花](../Page/上海申花.md "wikilink")，开始是右后卫上的绝对主力。他比较善于助攻，不过也曾因此回防不及而遭到主帅[徐根宝斥责](../Page/徐根宝.md "wikilink")。不过球迷始终记得他在1994年底对德甲[凯泽斯劳滕队的友谊赛中](../Page/凯泽斯劳滕.md "wikilink")，连过对方数人打入的精彩入球。

1998年，姚俊加盟[上海浦东](../Page/陕西中新.md "wikilink")，而申花则由此交易获得了[忻峰](../Page/忻峰.md "wikilink")、[卞军两个颇具潜力的年轻后卫](../Page/卞军.md "wikilink")。2000年联赛开始前他被租借到了[广州太阳神](../Page/广州太阳神.md "wikilink")，但是由于与联赛初期临时接任外教的主帅[周穗安不和](../Page/周穗安.md "wikilink")，仅上阵了一场对[长春亚泰的比赛即被退回了](../Page/长春亚泰.md "wikilink")[上海浦东](../Page/陕西中新.md "wikilink")。

退役后，姚俊留在了球队担任梯队教练。

[category:中国足球运动员](../Page/category:中国足球运动员.md "wikilink")
[category:上海申花球员](../Page/category:上海申花球员.md "wikilink")
[category:上海人](../Page/category:上海人.md "wikilink")
[J](../Page/category:姚姓.md "wikilink")

[Category:上海足球队球员](../Category/上海足球队球员.md "wikilink")
[Category:陕西中新球员](../Category/陕西中新球员.md "wikilink")
[Category:广州足球俱乐部球员](../Category/广州足球俱乐部球员.md "wikilink")