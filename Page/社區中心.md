[Tin_Ching_Community_Hall_interior_2014.jpg](https://zh.wikipedia.org/wiki/File:Tin_Ching_Community_Hall_interior_2014.jpg "fig:Tin_Ching_Community_Hall_interior_2014.jpg")
[Buergerhaus_Ronhausen.jpg](https://zh.wikipedia.org/wiki/File:Buergerhaus_Ronhausen.jpg "fig:Buergerhaus_Ronhausen.jpg")的社區中心\]\]
[Community_Centre_with_computers_in_Sabalibugu_Bamako_Mali.jpg](https://zh.wikipedia.org/wiki/File:Community_Centre_with_computers_in_Sabalibugu_Bamako_Mali.jpg "fig:Community_Centre_with_computers_in_Sabalibugu_Bamako_Mali.jpg")的社區中心\]\]
**社區中心**（）為提供[社區居民舉行康樂](../Page/社區.md "wikilink")、文化、公益等[公共事務的場所](../Page/公共事務.md "wikilink")，以及社區地方政府辦公及開會的地方。社區中心的概念源於[英國](../Page/英國.md "wikilink")，原意是[鎮政府辦公的地方](../Page/鎮.md "wikilink")，亦是居民進行大型活動（如[婚禮](../Page/婚禮.md "wikilink")）的地方。亦有一說指社區會堂的概念其實源自[猶太人日常聚會的](../Page/猶太人.md "wikilink")[猶太會堂](../Page/猶太會堂.md "wikilink")。

## 各地概況

### 香港

在香港1960-1990年代政府建設了不少社區中心，社區中心一般包括了政府福利部門辦事處及其他社會服務機構租用，如青少年中心、自修室、活動室、幼稚園等設施，通常也附設**[社區會堂](../Page/香港社區會堂.md "wikilink")**（Community
hall），2000年代政府主要在[公共屋邨](../Page/公共屋邨.md "wikilink")（如：屯門[龍逸邨的](../Page/龍逸邨.md "wikilink")[龍逸社區會堂](../Page/龍逸社區會堂.md "wikilink")）及私人屋苑（如：銅鑼灣[禮頓山的](../Page/禮頓山.md "wikilink")[禮頓山社區會堂](../Page/禮頓山社區會堂.md "wikilink")）興建社區會堂，並將社福設施打散，不再集中在社區中心，而是設在部分樓宇底層，讓社區設施更融入社區，而香港大部分的社區會堂，均由私人發展商及房屋署代為興建，落成後交由該區的民政事務署管理。

社區會堂內通常設有一個大[禮堂](../Page/禮堂.md "wikilink")，及活動室，讓居民可以在此舉行大型活動，如各種[表演](../Page/表演.md "wikilink")、[投票](../Page/投票.md "wikilink")、居民大會等。在比較大型的社區會堂，例如：香港[中環的](../Page/中環.md "wikilink")[大會堂](../Page/香港大會堂.md "wikilink")、[英國](../Page/英國.md "wikilink")[伯明翰的](../Page/伯明翰.md "wikilink")[伯明翰大會堂都設有](../Page/伯明翰大會堂.md "wikilink")[演奏廳](../Page/演奏廳.md "wikilink")。在[歐美的小鎮](../Page/歐美.md "wikilink")，社區會堂還包括了功能以外的其他服務，例如：[郵遞服務](../Page/郵遞.md "wikilink")。

在香港，現時所有社區會堂都由[香港政府](../Page/香港政府.md "wikilink")[民政事務總署管理](../Page/民政事務總署.md "wikilink")。

作為當地地方政府的代表，各地的社區會堂都有其獨特的建築特色。

### 台灣

社區中心在[台灣稱為](../Page/台灣.md "wikilink")**社區活動中心**，主要專指[封閉型社區的社區中心](../Page/封閉型社區.md "wikilink")。而各[縣](../Page/縣_\(中華民國\).md "wikilink")[市的](../Page/市_\(中華民國\).md "wikilink")[村](../Page/村_\(中華民國\).md "wikilink")、[里由](../Page/里_\(中華民國\).md "wikilink")[政府出資設置的](../Page/中華民國政府.md "wikilink")**村民活動中心**或**里民活動中心**（[基隆市稱為](../Page/基隆市.md "wikilink")「**里民大會堂**」），即具有社區中心的機能。

台灣的社區中心具有多功能用途，平時做為[村長](../Page/村長.md "wikilink")、[里長的](../Page/里長.md "wikilink")[辦公處](../Page/辦公處.md "wikilink")，或社區居民聯絡感情的地方；而許多社區活動中心為了增加使用率，多設有[卡拉OK等康樂設施](../Page/卡拉OK.md "wikilink")。而[選舉期間](../Page/台灣選舉.md "wikilink")，社區活動中心多被徵用作為臨時的[投開票所](../Page/投開票所.md "wikilink")。

## 參見

  - [市政廳](../Page/市政廳.md "wikilink")
  - [市議會](../Page/市議會.md "wikilink")
  - [梁顯利油麻地社區中心](../Page/梁顯利油麻地社區中心.md "wikilink")
  - [青年宮](../Page/青年宮.md "wikilink")
  - [少年宮](../Page/少年宮.md "wikilink")
  - [青年中心](../Page/青年中心.md "wikilink")
  - [香港中華基督教青年會九龍會所](../Page/香港中華基督教青年會九龍會所.md "wikilink")
  - [街坊福利會](../Page/街坊福利會.md "wikilink")

## 外部連結

  - [台灣:台灣社區通網站](http://sixstar.moc.gov.tw/)
  - [香港特別行政區政府 民政事務總署
    社區會堂/社區中心一覽表](http://www.had.gov.hk/tc/public_services/community_halls_centres/ccch.htm)

[\*](../Category/社區中心.md "wikilink")