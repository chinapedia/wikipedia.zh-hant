[Demchugdongrub.jpg](https://zh.wikipedia.org/wiki/File:Demchugdongrub.jpg "fig:Demchugdongrub.jpg")（中）和日本佔领军\]\]

**德穆楚克栋鲁普**（；），人称**德王**，[字](../Page/表字.md "wikilink")**希賢**。[內蒙古](../Page/內蒙古.md "wikilink")[錫林郭勒盟](../Page/錫林郭勒盟.md "wikilink")[蘇尼特右旗人](../Page/蘇尼特右旗.md "wikilink")，[苏尼特扎萨克杜棱郡王](../Page/苏尼特扎萨克杜棱郡王.md "wikilink")，曾任[锡林郭勒盟盟长](../Page/锡林郭勒盟.md "wikilink")，主张內蒙古獨立。20世纪30年代起与[大日本帝国陆军](../Page/大日本帝国陆军.md "wikilink")[關東軍合作](../Page/關東軍.md "wikilink")，在当时[中華民國的](../Page/中華民國.md "wikilink")[察哈尔省](../Page/察哈尔省.md "wikilink")、[绥远省等地建立了](../Page/绥远省.md "wikilink")[蒙疆联合自治政府](../Page/蒙疆联合自治政府.md "wikilink")，并担任要职。

## 生平

### 早年

德王出身內蒙古[錫林郭勒盟](../Page/錫林郭勒盟.md "wikilink")[蘇尼特右旗人](../Page/蘇尼特右旗.md "wikilink")，1902年2月8日出生，1908年襲爵[札薩克多羅杜稜郡王](../Page/苏尼特扎萨克杜棱郡王.md "wikilink")。1913年[北洋政府授為札薩克和碩杜稜親王](../Page/北洋政府.md "wikilink")。1919年執掌旗政。他熟悉儒家经书，是位有造诣的书法家。1924年任[錫林郭勒盟副盟長](../Page/錫林郭勒盟.md "wikilink")、[察哈尔省政府委员](../Page/察哈尔省.md "wikilink")。1925年2月，任[善後會議委員](../Page/善後會議.md "wikilink")，1927年出任參議院參政。1931年继任[錫林郭勒盟盟長](../Page/錫林郭勒盟.md "wikilink")。

### 日佔之前

德王主張推動[泛蒙古主義](../Page/泛蒙古主義.md "wikilink")，指導[內蒙古獨立運動](../Page/內蒙古獨立運動.md "wikilink")，曾經鼓吹[民族自決運動及自治運動](../Page/民族自決.md "wikilink")。[九一八事變後](../Page/九一八事變.md "wikilink")，德王與[日本軍方面連絡](../Page/日本軍.md "wikilink")，與[雲王](../Page/雲王.md "wikilink")（乌兰察布盟盟长[云端旺楚克](../Page/云端旺楚克.md "wikilink")）等王公於1933年組織內蒙古王公會議，並且向國民政府要求自治。

1934年4月，「蒙古地方自治政務委員會」（簡稱「蒙政會」）在[乌兰察布盟](../Page/乌兰察布盟.md "wikilink")[百灵庙宣告成立](../Page/百灵庙.md "wikilink")\[1\]。经[南京國民政府批准](../Page/南京國民政府.md "wikilink")，德王任秘书长，实际主持政务。11月7日，[蔣介石在](../Page/蔣介石.md "wikilink")[綏遠會見榮王](../Page/綏遠.md "wikilink")、德王、潘王等蒙人與黨政人員，並對各主席和邊外將領指導一切\[2\]。德王盛讚蔣是200餘年來第一位來蒙邊巡視之國家最高領袖\[3\]。

1936年，德王出任[察哈爾蒙政會副委員長](../Page/察哈爾.md "wikilink")，2月10日在日本[关东军支持下成立](../Page/关东军.md "wikilink")[蒙古軍政府任总司令](../Page/蒙古軍政府.md "wikilink")、总裁，[日本人](../Page/日本人.md "wikilink")[山内豊紀出任顾问](../Page/山内豊紀.md "wikilink")。11月24日，[傅作义调派](../Page/傅作义.md "wikilink")[国民革命军第三十五军主力攻克蒙古军政府第七师驻守的百灵庙](../Page/国民革命军第三十五军.md "wikilink")，是为“[绥远抗战](../Page/绥远抗战.md "wikilink")”。

1937年1月18日，德穆楚克棟魯普自稱「蒙古政治委員會主席」，令各旗限期遣壯丁入伍；「滿洲國」民政部、軍政部、蒙政部共同發布《青年訓練規定》；「協和會」建立48個青年訓練所，訓練6,000名青年\[4\]。1月20日，日軍開始運送大量汽油、煤油、武器等軍用品至「蒙古軍政府」主席德穆楚克棟魯普駐地[嘉卜寺](../Page/嘉卜寺.md "wikilink")\[5\]。1月28日，日機飛興和、集寧、原陽一帶偵察；「蒙軍」德穆楚克棟魯普移至化德，李守信部移至尚義，王英部移至商都\[6\]。2月5日，德王任「蒙軍」總司令，日本促其進攻綏東興和、紅格爾等地；日本特務機關長田中在嘉卜寺召開軍事會議，討論攻綏計劃\[7\]。3月26日，德王在嘉卜寺（化德）成立「察哈爾錫林郭勒盟軍政委員會」，自兼[委員長](../Page/委員長.md "wikilink")\[8\]。4月10日，「蒙古軍政會議」在嘉卜寺舉行，日本軍官、顧問、指導等多人及將領等出席，會議決議決不放棄察北六縣，並擬切斷山西、綏遠之聯繫\[9\]。4月16日，察北軍向綏東推進，德王部抵尚義，穆克登寶部抵商都，包悅卿部抵張北\[10\]。4月25日，日軍唆使「蒙軍」進犯綏遠\[11\]。5月6日，日本關東軍司令官[植田謙吉大將飛嘉卜寺](../Page/植田謙吉.md "wikilink")，與德王及[吳鶴齡](../Page/吳鶴齡.md "wikilink")、李守信等會晤，策劃綏蒙邊境軍事；決訓練蒙旗保安隊兩大隊，合計兩萬餘人，由德王任總隊長，[卓什海](../Page/卓什海.md "wikilink")、[包悅卿兩人任大隊長](../Page/包悅卿.md "wikilink")\[12\]。6月12日，察北軍一律改稱蒙古軍，德王任總司令，李守信任副總司令兼參謀長\[13\]。

### 日佔期間

[Mengjiang-2016.jpg](https://zh.wikipedia.org/wiki/File:Mengjiang-2016.jpg "fig:Mengjiang-2016.jpg")

1937年[七七事变后](../Page/七七事变.md "wikilink")，日本关东军很快就控制华北和内蒙古之间的[平绥铁路](../Page/京包铁路.md "wikilink")，10月17日占领[包头](../Page/包头.md "wikilink")。德王、[李守信等人投向日本人](../Page/李守信.md "wikilink")，出任自治政府首脑。[蒙古聯盟自治政府成立後](../Page/蒙古聯盟自治政府.md "wikilink")，德王出任副主席，1938年出任主席，后合併「察南自治政府」及「晉北自治政府」而成立“[蒙疆联合自治政府](../Page/蒙疆联合自治政府.md "wikilink")”，領有[巴彥塔拉](../Page/巴彥塔拉.md "wikilink")、[察哈尔](../Page/察哈尔.md "wikilink")、[锡林郭勒](../Page/锡林郭勒.md "wikilink")、[烏蘭察布](../Page/烏蘭察布.md "wikilink")、[伊克昭](../Page/伊克昭.md "wikilink")（今[鄂尔多斯](../Page/鄂尔多斯.md "wikilink")）等五個盟與[晋北](../Page/山西.md "wikilink")（[大同市](../Page/大同市.md "wikilink")）[察南](../Page/察哈尔.md "wikilink")（[张家口市](../Page/张家口市.md "wikilink")）兩政廳。联合政府的首都设于[呼和浩特](../Page/呼和浩特.md "wikilink")，名义上归[南京的](../Page/南京.md "wikilink")[汪精卫政府所辖](../Page/汪精卫政府.md "wikilink")。

由于日本的高压统治，1939年末，德王通过[戴笠](../Page/戴笠.md "wikilink")，同重庆政府取得了联系。蒋介石勉励他留在内蒙古同日本人维持表面上的合作。随后由于国民党特工人员被日本[宪兵队捕获](../Page/宪兵.md "wikilink")，德王、李守信只好向日军坦白，日本人鉴于德王的声望以及维护蒙古地区的稳定，未予深究。1941年8月4日，“蒙疆”成为[汪精卫政权下享受自治的](../Page/汪精卫政权.md "wikilink")“蒙古自治邦”。

### 西蒙自治运动

[De_Wang.jpg](https://zh.wikipedia.org/wiki/File:De_Wang.jpg "fig:De_Wang.jpg")》中的德王\]\]
1945年隨著[日本二戰投降後](../Page/日本二戰投降.md "wikilink")，蒙古自治邦解散，德王的政治活动受到限制，一直寓居[北平](../Page/北平.md "wikilink")。1949年1月1日，錫林郭勒盟之德穆楚克棟魯普﹙德王﹚由北平抵南京；1月20日，由蔣加予德王阿莽倉佛輔教宏覺禪師名號\[14\]在[國共內戰后期](../Page/國共內戰.md "wikilink")，1949年初北平被[解放军占领前夕](../Page/解放军.md "wikilink")，德王假道[兰州去了](../Page/兰州.md "wikilink")[阿拉善旗定远营](../Page/阿拉善.md "wikilink")（今阿拉善左旗[巴彦浩特](../Page/巴彦浩特.md "wikilink")），联合阿拉善旗的[札萨克](../Page/札萨克.md "wikilink")[达理扎雅等当地上层人士在中国局势即将发生剧变之时发起](../Page/达理扎雅.md "wikilink")“蒙古自治”。1949年4月13日召开会议，成立了“蒙古自治筹备委员会”，德王任副委员长。之后德王和[乌兰察布盟盟长](../Page/乌兰察布.md "wikilink")[林沁僧格前往](../Page/林沁僧格.md "wikilink")[广州](../Page/广州.md "wikilink")，向国民政府请愿，寻求承认、支持和资助。被国民政府[李宗仁](../Page/李宗仁.md "wikilink")、[阎锡山](../Page/阎锡山.md "wikilink")、[白云梯等人敷衍之后](../Page/白云梯.md "wikilink")，德王召集了一些旧部回到定远营。8月10日组成了“蒙古自治政府”，德王任主席，并向廣州的中華民國政府发出了自治通电。9月20日，因解放军逼近[银川](../Page/银川.md "wikilink")，德王、[李守信等逃往阿拉善旗西北部](../Page/李守信.md "wikilink")[拐子湖一带](../Page/拐子湖.md "wikilink")，成立了“蒙古军总司令部”。1950年初，得到蒙古人民革命党的邀请和保证后，德王、李守信出走[蒙古人民共和國](../Page/蒙古人民共和國.md "wikilink")，但不久便被蒙方逮捕审讯並转送至[中華人民共和國](../Page/中華人民共和國.md "wikilink")。

### 晚年

德王作为蒙疆首要战犯被[中华人民共和国政府關押](../Page/中华人民共和国.md "wikilink")，1950年被囚禁於[張家口](../Page/張家口.md "wikilink")。1963年被特赦釋放，並恢復政治權利。他被聘为[内蒙古自治区文史研究馆馆员](../Page/内蒙古自治区文史研究馆.md "wikilink")，曾主持编成《二十八卷本词典》（以[蒙文編寫](../Page/蒙文.md "wikilink")），著有晚年回忆录《[德穆楚克栋鲁普自述](../Page/德穆楚克栋鲁普自述.md "wikilink")》。1966年5月23日在[呼和浩特病亡](../Page/呼和浩特.md "wikilink")。

## 家庭

  - 父：[那木济勒旺楚克](../Page/那木济勒旺楚克.md "wikilink")
  - 母：索达那木朝（那木济勒旺楚克的第四夫人）
  - 第一任夫人：色布吉德玛
      - 长子：[都固尔苏隆](../Page/都固尔苏隆.md "wikilink")
  - 第二任夫人：额仁钦朝
      - 女儿：巴图那顺
      - 子：嘎拉僧吉格米德
      - 子：敖其尔巴图
      - 子：敖其尔胡雅嘎
      - 子：敖其尔赛和雅

## 注释

## 参考文献

## 外部链接

## 参见

  - [蒙疆聯合自治政府](../Page/蒙疆聯合自治政府.md "wikilink")

{{-}}

| [Menjiang_Flag_1945.svg](https://zh.wikipedia.org/wiki/File:Menjiang_Flag_1945.svg "fig:Menjiang_Flag_1945.svg") [蒙古聯盟自治政府](../Page/蒙古聯盟自治政府.md "wikilink")                  |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [Flag_of_the_Mengjiang.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Mengjiang.svg "fig:Flag_of_the_Mengjiang.svg") [蒙疆聯合自治政府](../Page/蒙疆聯合自治政府.md "wikilink")（蒙古自治邦） |

[Category:與大日本帝國合作的中國人](../Category/與大日本帝國合作的中國人.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:蒙古军政府人物](../Category/蒙古军政府人物.md "wikilink")
[Category:蒙古联盟自治政府主席](../Category/蒙古联盟自治政府主席.md "wikilink")
[Category:蒙疆联合自治政府主席](../Category/蒙疆联合自治政府主席.md "wikilink")
[Category:苏尼特右旗人](../Category/苏尼特右旗人.md "wikilink")
[Category:外藩蒙古多罗郡王](../Category/外藩蒙古多罗郡王.md "wikilink")
[Category:蒙古军将领](../Category/蒙古军将领.md "wikilink")

1.  羅敏：〈蔣介石的政治空間戰略觀念研究——以其「安內」政策為中心的探討〉，刊[呂芳上主編](../Page/呂芳上.md "wikilink")：《蔣介石的日常生活》，香港：天地圖書，2014年1月

2.
3.  [總統府事略室編](../Page/中華民國總統府_\(台北\).md "wikilink")：《蔣中正總統文物·事略稿本》第廿八冊，1934年11月7日，台北，[國史館](../Page/國史館.md "wikilink")，2003年

4.

5.
6.
7.
8.
9.
10.
11.
12.
13.
14.