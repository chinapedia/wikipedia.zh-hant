[Xanaduoutside80s.jpg](https://zh.wikipedia.org/wiki/File:Xanaduoutside80s.jpg "fig:Xanaduoutside80s.jpg")郊外。\]\]

**仙纳度屋**（[英文](../Page/英文.md "wikilink")：Xanadu
Houses，又稱**上都屋**）是一系列建構在[美國的實驗性家居](../Page/美國.md "wikilink")[住宅](../Page/住宅.md "wikilink")，以[電腦與](../Page/電腦.md "wikilink")[自動化技術為設計主體](../Page/自動化技術.md "wikilink")。這個建築計畫從1979年動工，直至到1980年代初期就已經有三間房子蓋好，分別坐落於[科羅拉多州的](../Page/科羅拉多州.md "wikilink")[基西米](../Page/基西米.md "wikilink")、[威斯康辛州的](../Page/威斯康辛州.md "wikilink")[威斯康辛德爾斯](../Page/威斯康辛德爾斯.md "wikilink")，以及[田納西州的](../Page/田納西州.md "wikilink")[加特林堡](../Page/加特林堡.md "wikilink")。這些房子以嶄新的結構和設計技術見稱，並在80年代時成為熱門的[旅遊勝地](../Page/旅遊勝地.md "wikilink")。

仙纳度屋的特別之處在於它以[聚氨酯泡沫隔熱材料造成的結構](../Page/聚氨酯.md "wikilink")，有別於一般建築物以[混凝土作為建築材料](../Page/混凝土.md "wikilink")。這種物料使建造過程變得快捷、方便，而且合乎成本效益。房子以[人因工程學的角度設計](../Page/人因工程學.md "wikilink")，並包含著最原始的[智能家居系統](../Page/智能家居.md "wikilink")。位於基西米的仙纳度屋在三間房子之中最為受歡迎，最高峰時期每天參觀人數高達1000人，由美國[建築師](../Page/建築師.md "wikilink")[羅伊·梅森所建](../Page/羅伊·梅森_\(建築師\).md "wikilink")。在威斯康辛德爾和斯加特林堡的兩間房子在1990年代初關閉繼而拆掉，而剩下的一間則在1996年停止運作，並在2005年步前兩間房子的後塵，遭受壞拆毀的命運。

## 歷史

## 設計

## 內部裝潢

## 壞處

## 書本上的仙納度屋

## 參見

  - [建築學](../Page/建築學.md "wikilink")
  - [住宅](../Page/住宅.md "wikilink")
  - [創新住宅](../Page/創新住宅.md "wikilink")
  - [旅遊勝地](../Page/旅遊勝地.md "wikilink")
  - [旅遊業](../Page/旅遊業.md "wikilink")
  - [獨石圓頂](../Page/獨石圓頂.md "wikilink")
  - [仙納度屋 2.0](../Page/仙納度屋_2.0.md "wikilink")

## 註解

<div class="references-small">

<references/>

</div>

## 參考資料

<div class="references-small">

  - Joseph A. Harb, *No place like home - beep - zzzt - "smart home"
    technology reviewed* (Nation's Business article, February, 1986)
  - Tom Halfhill. *Using Computers in the Home* (Compute Magazine
    Article, December 1982)
  - Catherine O'Neil *Computers Those Amazing Machines* (Book, 1985),
    Page 90, 92. (Computing the Future) ISBN 0-87044-574-X
  - Joseph J Corn, *Yesterday's Tomorrows: Past Visions of the American
    Future* (1984,1996), ISBN 0-8018-5399-0

</div>

## 外部連結

  - [1982 Compute Magazine
    Article](http://www.commodore.ca/history/other/1982_Future.htm)
    "Using Computers in the Home"
  - [Website about Xanadu, with forum](http://www.xanaduhome.com/)
  - [Xanadu home of the future yahoo
    group](http://groups.yahoo.com/group/Xanaduhome/)
  - [Xanadu : Home of the future](http://www.lostparks.com/xanadu.html)
  - [2005 photos of the demolition of
    Xanadu](http://www.ifalloutshelter.com/xanadu)
  - [Xanadu - Demolished
    October 7-10, 2005](http://www.jtlytle.com/jtlytle/xanadu/) — 2005
    photos of the demolished Xanadu
  - [Xanadu Photo
    Gallery](http://wackel.home.comcast.net/Xanadu/index.html) — Photos
    of Xanadu from July, 1994
  - [Kissimmee's Xanadu as
    of 2004](http://spork.no-ip.com/~uef/gallery/xanadu) — Urban
    Explorers photos of Xanadu in 2004.
  - [3 minute Xanadu video showing the Xanadu
    tour](http://www.thisexit.com/movies/FLKISxanadu.mov) (QuickTime
    movie)
  - [The Dilbert Ultimate
    House](http://dilbert.com/comics/dilbert/duh/), a very similar
    project
  - [JETSETMODERN's
    picture](https://web.archive.org/web/20060831081429/http://www.jetsetmodern.com/architecture.htm)

[Category:1979年完工建築物](../Category/1979年完工建築物.md "wikilink")
[Category:美国住宅](../Category/美国住宅.md "wikilink")
[Category:美國已拆除建築物](../Category/美國已拆除建築物.md "wikilink")
[Category:科罗拉多州建筑物](../Category/科罗拉多州建筑物.md "wikilink")
[Category:田納西州建築物](../Category/田納西州建築物.md "wikilink")
[Category:威斯康辛州建築物](../Category/威斯康辛州建築物.md "wikilink")
[Category:圓頂](../Category/圓頂.md "wikilink")
[Category:未來主義建築](../Category/未來主義建築.md "wikilink")