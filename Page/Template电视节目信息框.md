}}

`|2 = {{#if:``}}}}}}|`<small>`}}}}}}`</small>`|{{#if:``}}}|`<small>`}}}`</small>`}}}}}}`

|abovestyle =
font-size:12pt;background:}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}};{{\#if:}}}}}}}}}|color:
}}}}}}}}};}} |aboveclass = summary |image =
{{\#invoke:InfoboxImage|InfoboxImage|image=}}}}}}}}}}}}|size=}}}|sizedefault=frameless|upright=1.13|alt=}}}}}}}}
|caption = }}}}}}

|headerstyle =
font-size:100%;background:}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}};{{\#if:}}}}}}}}}|color:
}}}}}}}}};}}

|label1 = 别名 |data1 = }}}}}}}}} |label2 = 类型 |class2 = category |data2 =
}}}}}}}}}}}} |label3 = 格式 |class3 = category |data3 = }}} |label4 = 原作
|data4 = }}}}}}}}}}}}}}}}}} |label5 = 开创 |data5 = }}}}}} |label6 =
|data6 = }}}}}}}}}}}} |label7 = 编剧 |data7 = }}}}}} |label8 =  |class8 =
attendee |data8 = }}}}}} |label9 =  |class9 = attendee |data9 = }}}}}}
|label10 =  |class10 = attendee |data10 = }}}}}} |label11 = 创意总监
|class11 = attendee |data11 = }}}}}} |label12 =  |class12 = attendee
|data12 = }}}}}} |label13 =  |class13 = attendee |data13 = }}}}}}
|label14 = 主演 |class14 = attendee |data14 = }}}}}} |label15 = 评委
|class15 = attendee |data15 = }}}}}}}}}}}} |label16 = 配音 |class16 =
attendee |data16 = }}} |label17 = 旁白 |class17 = attendee |data17 = }}}
|label18 = }}}}}}}}}}}}}}}}}}}}}} | 制作|}}国家／地区}} |data18 = {{\#if:}}} |
}}} | {{\#if:}}}}}} | }}}}}} }}}} |label20 = 语言 |data20 = }}}}}}
|label21 = 字幕 |data21 = }}} |label22 =  |data22 = }}}}}}

|label23 = {{\#if:}}}}}}|季数|{{\#if:}}}}}}|辑数|{{\#if:}}}}}}|系列数}}}}}}
|data23 = {{\#if:}}}}}} | }}}}}} | {{\#if:}}}}}} | }}}}}} |
{{\#if:}}}}}} | }}}}}} | }}}}}} |label24 = 集数 |data24 =
}}}}}}}}}}}}}}}}}}}}}{{\#if:}}} | （\[\[}}}}}}}}}}}} |label26 =  |data26
= {{\#if:}}}}}} | }}}}}} | {{\#if:}}}}}}}}} | }}}}}}}}} }}}} |label27 =
|data27 = {{\#if:}}}}}} | }}}}}} | {{\#if:}}}}}} | }}}}}} }}}} |label28
=  |data28 = }}}}}} |label29 =  |data29 = }}}}}} |label30 =
-{zh-hk:填詞;zh-tw:作詞;zh-cn:作词;}- |data30 = }}}}}}}}}}}} |label31 =
-{zh-hans:演唱;zh-hant:主唱;}- |data31 = }}}}}} |label32 =  |data32 = }}}
|label33 =  |data33 = }}} |label34 = -{zh-hk:填詞;zh-tw:作詞;zh-cn:作词}-
|data34 = }}}}}}}}}}}} |label35 = -{zh-hans:演唱;zh-hant:主唱;}- |data35 =
}}}}}} |label36 =  |data36 = }}} |data37 =

|header40 = {{\#ifeq: {{\#expr: {{\#if:}}}}}}}}}}}}|1|0}} or
{{\#if:}}}}}}}}}}}}|1|0}} or {{\#if:}}}}}}|1|0}} or {{\#if:}}}}}}|1|0}}
or {{\#if:}}}}}}|1|0}} or {{\#if:}}}}}}|1|0}} or {{\#if:}}}}}}}}}|1|0}}
or {{\#if:}}}}}}}}}|1|0}} or {{\#if:}}}}}} |1|0}} or {{\#if:}}}}}}|1|0}}
}} | 1 |制作}}

|label41 =  |data41 = }}}}}}}}}}}} |label42 =
{{\#if:||-{zh-hans:制作;zh-hant:製作;zh-cn:制片;}-人}} |data42 =
}}}}}}}}}}}} |label43 = {{\#if:||人}} |data43 = }}}}}}}}}}}}}}}}}}}}}}}}
|label44 = 出品人 |data44 = }}} |label45 = 总监制 |data45 = }}}}}} |label46 =
监制 |data46 = }}}}}} |label47 =  |data47 = }}}}}} |label48 =  |data48 =
}}}}}} |data50 =

|label55 = }}}}}} |data55 = }}}}}}}}}}}}}}} |label56 = }}}}}} |data56 =
}}} |label57 = 编审 |data57 = }}}}}} |label58 =
-{zh-cn:剪辑;zh-hk:剪接;zh-tw:剪輯;}- |data58 = }}}}}}}}} |label59 =
拍攝地點 |data59 = }}}}}}}}} |label60 = 摄影 |data60 = }}}}}} |label61 = 机位
|data61 = }}}}}} |label62 =  |data62 = }}}}}} |label63 =  |data63 =
}}}}}} |label64 =  |data64 = }}}}}} |label65 = 预算 |data65 = }}}}}}
|data66 =

|header70style = {{\#if:}}}}}}}}}}}}}}}}}}}}} | |
background:transparent;}} |header70 = {{\#switch: {{\#expr:
{{\#if:}}}}}} }}}}}} }}}}}}}}} |1|0}}+{{\#if:}}}}}} }}}}}}}}}
}}}}}}|2|0}}}}

`|1 = 播映`
`|2 = `
`|3 = {{#if:``}}}}}} | ``}}}}}}首播 | {{#if:``}}}}}}}}} | ``}}}}}}}}}首播 | {{#if:``}}}}}} | ``}}}}}}首播 }}}}}}}}`

`}}}} ``}}}}}} ``}}}}}}}}} |1|0}}+{{#if:``}}}}}} ``}}}}}}}}} ``}}}}}}|2|0}}}}`
`|1 | {{#if:``}}}}}} | ``}}}}}} | {{#if:``}}}}}}}}} |``}}}}}}}}} | {{#if:``}}}}}} | ``}}}}}} }}}}}} | }}`
` | label2 = `
` | data2  = ``}}}}}}}}}}}}}}}`
` | label3 = `
` | data3  = ``}}}}}}}}}}}}}}}`
` | label4 = `
` | data4  = {{#if:``}}}}}}}}} | ``}}}}}}}}} | {{#if:``}}}}}}}}}}}} | ``}}}}}}}}}}}} }}}}`
` | label5 = 集数`
` | data5  = ``}}}}}}`

` | label7 = `
` | data7  = ``}}}}}}}}}}}}`

` | label8 = `
` | data8  = {{#if:``}}}}}}|``}}}}}}－``}}}}}}|{{#if:``}}}|``}}}}}}}`
` | label9 = `
` | data9  = ``}}}}}}`

}}

|header71style = {{\#if:}}}}}}}}}}}}}}}}}}}}} | |
background:transparent;}} |header71 = {{\#if:}}}}}}}}}}}}}}}}}}}}} |
}}}}}}}}}}}}}}}}}}}}}}}}}}}首播 }}}}}}}}}}}}}

` | label3 = `
` | data3  = ``}}}}}}}}}}}}}}}`
` | label4 = `
` | data4  = {{#if:``}}}}}}}}} | ``}}}}}}}}} | {{#if:``}}}}}}}}}}}} | ``}}}}}}}}}}}} }}}}`
` | label5 = 集数`
` | data5  = ``}}}}}}`
` | label7 = `
` | data7  = ``}}}}}}}}}}}}`
` | label8 = `
` | data8  = {{#if:``}}}}}}|``}}}}}}－``}}}}}}|{{#if:``}}}|``}}}}}}}`
` | label9 = `
` | data9  = ``}}}}}}`

}} }}

|header72style = {{\#if:}}}}}}}}}}}}}}}}}}}}} | |
background:transparent;}} |header72 = {{\#if:}}}}}}}}}}}}}}}}}}}}} |
}}}}}}}}}}}}}}}}}}}}}}}}}}}首播 }}}}}}}}}}}}}

` | label3 = `
` | data3  = ``}}}}}}}}}}}}}}}`
` | label4 = `
` | data4  = {{#if:``}}}}}}}}} | ``}}}}}}}}} | {{#if:``}}}}}}}}}}}} | ``}}}}}}}}}}}} }}}}`
` | label5 = 集数`
` | data5  = ``}}}}}}`
` | label7 = `
` | data7  = ``}}}}}}}}}}}}`
` | label8 = `
` | data8  = {{#if:``}}}}}}|``}}}}}}－``}}}}}}|{{#if:``}}}|``}}}}}}}`
` | label9 = `
` | data9  = ``}}}}}}`

}} }}

|header73style = {{\#if:}}}}}}}}}}}}}}}}}}}}} | |
background:transparent;}} |header73 = {{\#if:}}}}}}}}}}}}}}}}}}}}} |
}}}}}}}}}}}}}}}}}}}}}}}}}}}首播 }}}}}}}}}}}}}

` | label3 = `
` | data3  = ``}}}}}}}}}}}}}}}`
` | label4 = `
` | data4  = {{#if:``}}}}}}}}} | ``}}}}}}}}} | {{#if:``}}}}}}}}}}}} | ``}}}}}}}}}}}} }}}}`
` | label5 = 集数`
` | data5  = ``}}}}}} `
` | label7 = `
` | data7  = ``}}}}}}}}}}}}`
` | label8 = `
` | data8  = {{#if:``}}}}}}|``}}}}}}－``}}}}}}|{{#if:``}}}|``}}}}}}}`
` | label9 = `
` | data9  = ``}}}}}}`

}} }}

|header74style = {{\#if:}}}}}}}}}}}}}}}}}}}}} | |
background:transparent;}} |header74 = {{\#if:}}}}}}}}}}}}}}}}}}}}}
|}}}}}}}}}}}}}}}}}}}}}}}}}}}首播 }}}}}}}}}}}}}

` | label3 = `
` | data3  = ``}}}}}}}}}}}}}}}`
` | label4 = `
` | data4  = {{#if:``}}}}}}}}} | ``}}}}}}}}} | {{#if:``}}}}}}}}}}}} | ``}}}}}}}}}}}} }}}}`
` | label5 = 集数`
` | data5  = ``}}}}}} `
` | label7 = `
` | data7  = ``}}}}}}}}}}}}`
` | label8 = `
` | data8  = {{#if:``}}}}}}|``}}}}}}－``}}}}}}|{{#if:``}}}|``}}}}}}}`
` | label9 = `
` | data9  = ``}}}}}}`

}} }}

|header75style = {{\#if:}}}}}}}}}}}}}}}}}}}}} | |
background:transparent;}} |header75 = {{\#if:}}}}}}}}}}}}}}}}}}}}} |
}}}}}}}}}}}}}}}}}}}}}}}}}}}首播 }}}}}}}}}}}}}

` | label3 = `
` | data3  = ``}}}}}}}}}}}}}}}`
` | label4 = `
` | data4  = {{#if:``}}}}}}}}} | ``}}}}}}}}} | {{#if:``}}}}}}}}}}}} | ``}}}}}}}}}}}} }}}}`
` | label5 = 集数`
` | data5  = ``}}}}}} `
` | label7 = `
` | data7  = ``}}}}}}}}}}}}`
` | label8 = `
` | data8  = {{#if:``}}}}}}|``}}}}}}－``}}}}}}|{{#if:``}}}|``}}}}}}}`
` | label9 = `
` | data9  = ``}}}}}}`

}} }}

|header76style = {{\#if:}}}}}}}}}}}}}}}}}}}}} | |
background:transparent;}} |header76 = {{\#if:}}}}}}}}}}}}}}}}}}}}} |
}}}}}}}}}}}}}}}}}}}}}}}}}}}首播 }}}}}}}}}}}}}

` | label3 = `
` | data3  = ``}}}}}}}}}}}}}}}`
` | label4 = `
` | data4  = {{#if:``}}}}}}}}} | ``}}}}}}}}} | {{#if:``}}}}}}}}}}}} | ``}}}}}}}}}}}} }}}}`
` | label5 = 集数`
` | data5  = ``}}}}}} `
` | label7 = `
` | data7  = ``}}}}}}}}}}}}`
` | label8 = `
` | data8  = {{#if:``}}}}}}|``}}}}}}－``}}}}}}|{{#if:``}}}|``}}}}}}}`
` | label9 = `
` | data9  = ``}}}}}}`

}} }} |data77 =

|header78 = {{\#ifeq: {{\#expr: {{\#if: }}}}}}}}}}}}}}} |1|0}} or
{{\#if: }}}}}}}}}}}}}}}}}} |1|0}} or {{\#if: }}}}}}}}}}}}}}}}}} |1|0}}
}} | 1 | 相关节目}} |label79 = 前作 |data79 = }}}}}}}}}}}}}}} |label80 = 续作
|data80 = }}}}}}}}}}}}}}}}}} |label81 =  |data81 = }}}}}}}}}}}}}}}}}}

|header82 = {{\#if: }}}}}} }}}}}} }}}}}} }}}}}} }}}}}}}}} }}}}}} }}}}}}
}}}}}} }}}}}} |各地节目名称}}

|label83 =  |data83 =
{{\#if:}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}-}}
|label84 =  |data84 =
{{\#if:}}}}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}}}}-}}
|label85 =  |data85 =
{{\#if:}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}-}}
|label86 =  |data86
={{\#if:}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}-}}
|label87 =  |data87 =
{{\#if:}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}-}}
|label88 =  |data88 =
{{\#if:}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}-}}
|label89 =  |data89 =
{{\#if:}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}-}}
|label90 =  |data90 =
{{\#if:}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}-}}
|label91 =  |data91 =
{{\#if:}}}}}}|-{<includeonly>zh;zh-hans;zh-hant{{\!}}</includeonly>}}}}}}}-}}
|data92 =

|header100= {{\#ifeq: {{\#expr: {{\#if:}}}}}}}}}}}}|1|0}} or
{{\#if:}}}}}}|1|0}} or {{\#if:|1|0}} or {{\#if:|1|0}} }} | 1 |}}
|class101 = url |data101 = {{\#if:}}}}}}}}}}}} | \[}}}}}}}}}}}} 官方网站\]}}
|class102 = url |data102 = {{\#if:}}}}}} | \[}}}}}} 制作网站\]}} |data103 =
{{\#if: | [IMDb 介绍](IMDbTitle:{{{imdb_id}}}.md "wikilink")}} |data104 =
{{\#if: |
\[[http://www.tv.com/show/](http://www.tv.com/show/.md)/summary.html
TV.com 介绍\]}} |header105= {{\#if:}}}}}}|备注}} |data106 = }}}}}}
|data106style = text-align: left; }}<noinclude>  </noinclude>