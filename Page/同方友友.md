**同方友友控股有限公司**，前身「**真明麗控股有限公司**」（**Neo-Neon Holdings
Limited**，、）成立於1979年，由[台灣企業家](../Page/台灣.md "wikilink")[樊邦弘創辦](../Page/樊邦弘.md "wikilink")，是全球最大的[燈飾製造商之一](../Page/燈飾.md "wikilink")，業務為代理及生產燈飾，2006年於[香港交易所上市](../Page/香港交易所.md "wikilink")，現已回[臺灣發行](../Page/臺灣.md "wikilink")[臺灣存託憑證](../Page/臺灣存託憑證.md "wikilink")（[TDR](../Page/TDR.md "wikilink")）。總部設在[香港](../Page/香港.md "wikilink")，廠房則在[廣東](../Page/廣東.md "wikilink")[鶴山](../Page/鶴山.md "wikilink")。\[1\]\[2\]

2017年12月14日同方友友旗下同方證券管理的同方併購基金以39.7億人民幣完成收購兩直播平台北京飛流九天科技有限公司（即「飛流移動」）80%和思享時代(北京)科技有限公司（即「秀色直播」）65%的股份。然而北京工商信息查询http://qyxy.baic.gov.cn
显示以上两家公司并未过户至同方证券或其相关方名下。

## 外部連結

  - [同方友友控股有限公司 (英文)](http://www.neo-neon.com/)
  - [同方友友控股有限公司
    (中文)](https://web.archive.org/web/20090209031452/http://pro.neo-neon.com/)
  - [鶴山銀雨照明有限公司
    (中文)](https://web.archive.org/web/20090209080815/http://china.neo-neon.com/)
  - [广东银雨芯片半导体有限公司](http://www.neo-semi.com/)

## 参考文献

<div class="references-small">

<references />

</div>

[Category:香港製造公司](../Category/香港製造公司.md "wikilink")
[Category:香港上市工業公司](../Category/香港上市工業公司.md "wikilink")
[9](../Category/臺灣證券交易所上市公司.md "wikilink")
[Category:广东公司](../Category/广东公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:1979年成立的公司](../Category/1979年成立的公司.md "wikilink")

1.  [真明麗控股上市](http://paper.wenweipo.com/2006/11/29/FI0611290025.htm)
2.  [真明麗控股有限公司2006年公司年報](http://main.ednews.hk/listedco/listconews/sehk/20070416/LTN20070416195_C.htm)