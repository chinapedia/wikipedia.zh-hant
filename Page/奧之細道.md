**奧之細道**是[日本](../Page/日本.md "wikilink")[俳諧師](../Page/俳諧師.md "wikilink")[松尾芭蕉所著之紀行書](../Page/松尾芭蕉.md "wikilink")。於[元祿](../Page/元祿.md "wikilink")15年（1702年）印行，是松尾芭蕉的最有名的代表作。
書中記述松尾芭蕉與弟子[河合曾良於](../Page/河合曾良.md "wikilink")[元祿](../Page/元祿.md "wikilink")2年（1689年）從[江戶](../Page/江戶.md "wikilink")（[東京](../Page/東京.md "wikilink")）出發，遊歷[東北](../Page/東北地方.md "wikilink")、[北陸至](../Page/北陸地方.md "wikilink")[大垣](../Page/大垣市.md "wikilink")（[岐阜縣](../Page/岐阜縣.md "wikilink")）為止的見聞，與沿途有感而發撰寫的[俳句](../Page/俳句.md "wikilink")。后多次被改編成紀錄片。

## 參考文獻

  -
  -
  -
## 外部連結

  - [芭蕉翁「おくのほそ道」ネットワーク](http://basyoo.net/)　
  - [奥の細道](https://web.archive.org/web/20050701020315/http://www.ese.yamanashi.ac.jp/~itoyo/basho/okunohosomichi/okuindex.htm)
  - [芭蕉翁「おくのほそ道」ネットワーク](http://basyoo.net/)　
  - [おくのほそ道紀行300年記念事業・奥の細道サミット](http://www.bashouan.com/psBashou300.htm)
  - [おくのほそ道総合データベース・俳聖 松尾芭蕉
    みちのくの足跡](http://www.bashouan.com/psBashou.htm)
  - [奥の細道 朗読](http://hosomichi.roudokus.com/)
  - [芭蕉自筆本真贋論争考 ……栗林
    浩](http://weekly-haiku.blogspot.com/2007/07/blog-post_3396.html)
  - [天理大学附属天理図書館](http://www.tcl.gr.jp/)　曾良本（現代表記で「曽良本」、または天理本ともいう）を所蔵
  - [中尾松泉堂書店](http://www.shosendo.com/)　中尾本（推測で「野坡本」とも呼ばれる）を所有
  - [財団法人柿衞文庫](http://www.kakimori.jp/)　柿衞本（現代表記で「柿衛本」）を所蔵
  - [港都つるが株式会社](http://www.tmo-tsuruga.com/)　西村本を復刻頒布

[Category:江戶時代文學](../Category/江戶時代文學.md "wikilink")
[Category:松尾芭蕉](../Category/松尾芭蕉.md "wikilink")
[Category:俳句](../Category/俳句.md "wikilink")
[Category:旅行题材书籍](../Category/旅行题材书籍.md "wikilink")
[Category:18世紀書籍](../Category/18世紀書籍.md "wikilink")