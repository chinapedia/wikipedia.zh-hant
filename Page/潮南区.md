**潮南区**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[汕头市的一个市辖区](../Page/汕头市.md "wikilink")，位于[广东省的东南部](../Page/广东省.md "wikilink")，[潮汕平原的中心地带](../Page/潮汕.md "wikilink")。东临[南海](../Page/南海.md "wikilink")，西接[普宁市](../Page/普宁市.md "wikilink")，南邻[惠来县](../Page/惠来县.md "wikilink")，北与[潮阳区接壤](../Page/潮阳区.md "wikilink")，是2003年1月29日由原[潮阳市分出的新区](../Page/潮阳市.md "wikilink")。是中国著名侨乡之一，是中国文具生产基地\[1\]以及日化用品、内衣纺织服装重要生产基地。

## 行政区划

潮南区下辖1个街道：[峡山街道](../Page/峡山街道.md "wikilink")，10个镇：[井都镇](../Page/井都镇.md "wikilink")、[陇田镇](../Page/陇田镇.md "wikilink")、[雷岭镇](../Page/雷岭镇.md "wikilink")、[成田镇](../Page/成田镇.md "wikilink")、[红场镇](../Page/红场镇.md "wikilink")、[胪岗镇](../Page/胪岗镇.md "wikilink")、[两英镇](../Page/两英镇.md "wikilink")、[仙城镇](../Page/仙城镇.md "wikilink")、[陈店镇](../Page/陈店镇.md "wikilink")、[司马浦镇](../Page/司马浦镇.md "wikilink")。

| [行政区](../Page/行政区.md "wikilink")   | [面积](../Page/面积.md "wikilink") | [人口](../Page/人口.md "wikilink") | [居委会](../Page/居委会.md "wikilink") | [村委会](../Page/村委会.md "wikilink") | 备注          |
| ---------------------------------- | ------------------------------ | ------------------------------ | -------------------------------- | -------------------------------- | ----------- |
| [峡山街道](../Page/峡山街道.md "wikilink") | 46.4                           | 20.69                          | 12                               | 24                               | \-          |
| [井都镇](../Page/井都镇.md "wikilink")   | 43.5                           | 8.98                           | 4                                | 9                                | \-          |
| [陇田镇](../Page/陇田镇.md "wikilink")   | 70.96                          | 12.62                          | 8                                | 23                               | 由原沙陇镇与田心镇合并 |
| [雷岭镇](../Page/雷岭镇.md "wikilink")   | 65.8                           | 3.7                            | 1                                | 14                               | \-          |
| [红场镇](../Page/红场镇.md "wikilink")   | 85.3                           | 3.2                            | 1                                | 23                               | \-          |
| [成田镇](../Page/成田镇.md "wikilink")   | 56.77                          | 9.22                           | 3                                | 12                               | \-          |
| [胪岗镇](../Page/胪岗镇.md "wikilink")   | 50.4                           | 13.81                          | 4                                | 11                               | \-          |
| [两英镇](../Page/两英镇.md "wikilink")   | 72.4                           | 约20                            | 13                               | 17                               | \-          |
| [仙城镇](../Page/仙城镇.md "wikilink")   | 55.04                          | 9.55                           | 3                                | 9                                | \-          |
| [陈店镇](../Page/陈店镇.md "wikilink")   | 28.3                           | 10.20                          | 10                               | 13                               | \-          |
| [司马浦镇](../Page/司马浦镇.md "wikilink") | 30.5                           | 11.97                          | 6                                | 13                               | \-          |
|                                    |                                |                                |                                  |                                  |             |

  - （面积单位为平方公里，人口单位为万人，截至到2008年底）

## 自然地理

潮南区为沿海[丘陵](../Page/丘陵.md "wikilink")、[平原地区](../Page/平原.md "wikilink")，地势自西南向东北倾斜。地形特征为“一山一江一平原”，即大南山，起于红场镇潘岱村，自西向东延伸，主峰雷岭大山海拔521米。北部隔[练江与潮阳区相望](../Page/练江.md "wikilink")，练江自西向东横亘全境，形成练江平原。东部沿海为带状沙滩地。域内海岸线长14.7公里，海域面积4000多平方海里，山地面积38.23万亩，耕地面积20.82万亩。

潮南区属南亚热带季风气候带。年平均气温21.6℃，年平均降水量1700毫米左右，雨季多集中在4至9月。

## 经济

民营企业活跃，特色产业为[日用化工](../Page/日用化工.md "wikilink")、[文具](../Page/文具.md "wikilink")、[纺织服装](../Page/纺织服装.md "wikilink")、[内衣](../Page/内衣.md "wikilink")、[旅游用品](../Page/旅游用品.md "wikilink")。拥有“雅倩”、“拉芳”、“金万年”、“树德”、“曼妮芬”等众多全国著名品牌。

## 第三产业

在建五星级酒店二所，拥有四星级酒店三所，三星级四所\[2\]。

## 旅游

  - [仙城翠峰岩景区](../Page/仙城翠峰岩景区.md "wikilink")：位于仙城镇
  - [仙城仙湖景区](../Page/仙城仙湖景区.md "wikilink")：又名仙城八角亭、仙城半天佛仙湖文物风景区，位于仙城镇
  - [鹅地温泉](../Page/鹅地温泉.md "wikilink"):位于雷岭镇
  - 田心湾沙滩浴场：位于陇田镇

## 教育

省一级中学

  - [六都中学](../Page/六都中学.md "wikilink")

## 交通

全区虽在民间活跃资本的努力下基本上已实现村村通水泥路\[3\]。但由于粤东地区长期受省政府的漠视，潮南区得不到上级部门的重视，对外交通略显落后，缺少高速公路。区内交通方面受资金以及腐败困扰缺少[一级公路](../Page/一级公路.md "wikilink")，公交车方面也很不完善。区内现有100部潮南的士，按按照汕头市统一标准，起步价为2公里5元\[4\]。

  - <span style="background:red; color:white; font-size:smaller">G15</span>
    [深汕高速公路在本区陇田镇设有田心出入口](../Page/深汕高速公路.md "wikilink")。另拟再在雷岭镇（华湖）设置出入口。
  - <span style="background:red; color:white; font-size:smaller">G324</span>
    [324国道自东部的潮阳区和平镇進入潮南区](../Page/324国道.md "wikilink")，途经峡山街道，司马浦镇，陈店镇，向西进入普宁市占陇镇。
  - <span style="background:yellow; color:black; font-size:smaller">S235</span>[司神公路起点为区内的司马浦镇](../Page/司神公路.md "wikilink")、南接两英镇、红场镇、雷岭镇，向南进入惠来县华湖镇，是通往惠来县的主要道路。但路况失修已久，开始整修。
  - <span style="background:yellow; color:black; font-size:smaller">S237</span>[和惠公路自潮阳区和平镇进入潮南区](../Page/和惠公路.md "wikilink")，经胪岗、成田、陇田等镇，是通往惠来及深汕高速公路的主要通道。但路况失修已久，开始整修。
  - <span style="background:yellow; color:black; font-size:smaller">S337</span>[广葵公路自潮阳区城南街道进入潮南区](../Page/广葵公路.md "wikilink")，经井都、陇田等镇。
  - [一级公路](../Page/一级公路.md "wikilink")：[陈沙公路属省道田池线潮南区路段](../Page/陈沙公路.md "wikilink")。
  - 另有在建的[汕普高速公路](../Page/汕普高速公路.md "wikilink")，规划的[揭惠高速公路](../Page/揭惠高速公路.md "wikilink")

## 人物

主要出经商人才，代表人物有：

  - [周光镐](../Page/周光镐.md "wikilink")：峡山街道人，[明理学家](../Page/明.md "wikilink")，督战边疆，历任陕西[按察使](../Page/按察使.md "wikilink")，万历21年(1593)加[都察院](../Page/都察院.md "wikilink")[右都御史衔](../Page/右都御史.md "wikilink")，任宁夏[巡抚](../Page/巡抚.md "wikilink")。
  - [萧吉珊](../Page/萧吉珊.md "wikilink")：峡山街道人，原国民党中央执委
  - [郑铁如](../Page/郑铁如.md "wikilink")：祖籍潮阳县盐汀村（现潮南区盐汀村），中国银行董事、中银香港行长，外汇专家
  - [郑儒永](../Page/郑儒永.md "wikilink")：祖籍潮阳县盐汀村（现潮南区盐汀村），生于香港，中国科学院院士
  - 郑国忠：清华大学土木工程系教授，籍贯潮阳县盐汀村（现潮南区盐汀村）
  - [郑寿麟](../Page/郑寿麟.md "wikilink")：德国莱比锡大学博士，民国年间同济大学代校长。
  - [郑正秋](../Page/郑正秋.md "wikilink")：中国电影事业奠基人，祖籍潮阳县盐汀村（现潮南区盐汀村）
  - [郑小秋](../Page/郑小秋.md "wikilink")：上海导演，上海市一、二政协委员，祖籍潮阳县盐汀村（现潮南区盐汀村），郑正秋之子。
  - [马化腾](../Page/马化腾.md "wikilink")：[腾讯公司老总](../Page/腾讯.md "wikilink")
  - [连瀛洲](../Page/连瀛洲.md "wikilink")：已故，[新加坡四大银行](../Page/新加坡.md "wikilink")[华联银行创办人](../Page/华联银行.md "wikilink")。
  - [郑午楼](../Page/郑午楼.md "wikilink")：[泰国](../Page/泰国.md "wikilink")[京华银行董事长](../Page/京华银行.md "wikilink")
  - [李光隆](../Page/李光隆.md "wikilink")：泰国多家公司董事长
  - [陈弼臣](../Page/陈弼臣.md "wikilink")：泰国盘谷银行董事长
  - [廖烈文](../Page/廖烈文.md "wikilink")：泰国廖创兴银行总经理
  - [钟廷森](../Page/钟廷森.md "wikilink")：[金狮集团主席](../Page/金狮集团.md "wikilink")

## 相关条目

  - [潮阳区](../Page/潮阳区.md "wikilink")

## 外部链接

  - [潮南之窗](http://www.chaonan.gov.cn)

## 参考文献

[潮南区](../Page/category:潮南区.md "wikilink")
[区](../Page/category:汕头区县.md "wikilink")
[汕头](../Page/category:广东市辖区.md "wikilink")

1.
2.
3.
4.