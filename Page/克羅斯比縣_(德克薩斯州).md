**克勞斯貝縣**（）位[美國](../Page/美國.md "wikilink")[德克薩斯州西部的一個縣](../Page/德克薩斯州.md "wikilink")。面積2,335平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口7,072人。縣治[克勞斯貝頓](../Page/克勞斯貝頓.md "wikilink")（Crosbyton）。

成立於1876年8月21日，縣政府成立於1886年9月11日。縣名紀念德克薩斯土地辦公室秘書長史提芬·克勞斯貝。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[C](../Category/得克萨斯州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.