[TaiwanesePineappleCake.jpg](https://zh.wikipedia.org/wiki/File:TaiwanesePineappleCake.jpg "fig:TaiwanesePineappleCake.jpg")

**鳳梨酥**是來自[臺灣的點心](../Page/臺灣.md "wikilink")，其主要原料為[麵粉](../Page/麵粉.md "wikilink")、[奶油](../Page/奶油.md "wikilink")、[糖](../Page/糖.md "wikilink")、[蛋](../Page/蛋.md "wikilink")、[冬瓜醬](../Page/冬瓜.md "wikilink")（也可用純鳳梨\[1\]或冬瓜混合鳳梨製作）。外皮酥、內餡軟。以冬瓜餡製作的鳳梨酥有[蔬菜冬瓜清爽口感](../Page/蔬菜.md "wikilink")，也稱為冬瓜酥\[2\]；以冬瓜餡混合鳳梨餡製作的鳳梨酥帶有鳳梨甜香，或稱為冬瓜鳳梨酥；以純土鳳梨製成的鳳梨酥，其酸度較高，又稱土鳳梨酥。因為[鳳梨的](../Page/鳳梨.md "wikilink")[臺灣話諧音為](../Page/臺灣話.md "wikilink")「旺來」，帶有吉利興旺之意，而冬瓜有「好年冬」與甜甜蜜蜜的含意，帶有喜氣之意
\[3\]。

早年鳳梨酥大多為[冬瓜酥](../Page/冬瓜.md "wikilink")，其綿密彈牙的內餡為冬瓜醬製成，命名取其臺灣閩南話諧音「旺來」，但如果要正名回[冬瓜酥](../Page/冬瓜.md "wikilink")，不僅沒有旺來酥好聽，對長久以來接受旺來酥的民眾亦難以接受。

近年來則有不少商家改以純鳳梨製作內餡。台灣冬瓜是早期與現代台灣農民稻田休耕期間的輪作農產品\[4\]，所以全台各地有種稻米的地方，就幾乎有冬瓜的種植，產量豐富，為台灣大宗蔬菜之一，原料取得容易與經濟實惠，讓稻米農友創造額外收入，所以使用冬瓜當作食品原料也可以反映當時台灣農業土地的歷史與食品加工的背景。

以下為台灣地區鳳梨酥原料冬瓜產地，全年各地有生產：

1\.[宜蘭](../Page/宜蘭.md "wikilink")：羅東、頭城、五結、三星、礁溪、員山、冬山。

2\.[花蓮](../Page/花蓮.md "wikilink")：鳳林、玉里、新城、吉安、萬榮、卓溪、光復、富里。

3\.[台東](../Page/台東.md "wikilink")：關山、長演、鹿野、池上。

4\.[桃園](../Page/桃園.md "wikilink")：八德、龍潭、新屋、楊梅。

5\.[新竹](../Page/新竹.md "wikilink")：竹北、竹東、新埔、湖口、芎林、新豐。

6\.[苗栗](../Page/苗栗.md "wikilink")：苑裡、通宵、卓蘭。

7\.[台中](../Page/台中.md "wikilink")：大甲、霧峰、外埔、后里、大安、清水、烏日。

8\.[彰化](../Page/彰化.md "wikilink")：北斗、二林、埤頭、溪州、竹塘、芳苑。

9\.[雲林](../Page/雲林.md "wikilink")：西螺、元長、土庫、二崙、西螺、虎尾、大埤。

10\.[嘉義](../Page/嘉義.md "wikilink")：義竹、朴子、鹿草、六腳、太保、新港。

11\.[台南](../Page/台南.md "wikilink")：善化、後壁、歸仁、下營、六甲。

12\.[高雄](../Page/高雄.md "wikilink")：甲仙、六龜、梓官、彌陀、杉林、橋頭、大樹。

13\.[屏東](../Page/屏東.md "wikilink")：南州、新埤、林邊、恆春。

各縣市依季節不同生產之產季亦不同。\[5\]\[6\]

## 製作

  - 傳統鳳梨酥的外皮是採用豬油製成，早期鳳梨酥的內餡大多採用冬瓜或者冬瓜混合鳳梨製成。\[7\]
  - 早年台灣鳳梨成本過高，但台灣[冬瓜從百年前早期到現代都會輪作種植在全台的稻田上](../Page/冬瓜.md "wikilink")，為台灣稻農創造另一筆收入，所以產量豐富與經濟實惠，這也是冬瓜成為食品原料的由來，而冬瓜製作的內餡口感不僅棉密且Q彈。由於台灣早期食品加工將農產品當作製作成食品的原料，讓[冬瓜茶與](../Page/冬瓜茶.md "wikilink")[冬瓜鳳梨酥擁有百年製造歷史](../Page/冬瓜.md "wikilink")，將田裡的新鮮食材加工後能存放食用，是台灣百年前開墾時期的傳承。\[8\]
  - 近年有不少商家採用純鳳梨製作內餡，其口感雖然不及冬瓜餡綿密與Q彈，但純鳳梨餡特有的天然濃郁果香與酸甜滋味皆是冬瓜餡無法比擬的，因此冬瓜餡製成的鳳梨酥與純鳳梨餡製成的鳳梨酥各有其特定的擁護者。

## 資料來源

  - 注釋

<references/>

  - 書目

<!-- end list -->

  -
## 外部链接

[食](../Category/台中市文化.md "wikilink")
[Category:台北市文化](../Category/台北市文化.md "wikilink")
[Category:台灣糕點](../Category/台灣糕點.md "wikilink")
[Category:台灣飲食](../Category/台灣飲食.md "wikilink")

1.
2.  [ 農產品與烘焙結合古早味的創新-鳳梨酥](http://www.twvs.tnc.edu.tw/ufiles/sub/From_f3670884002844.pdf)

3.  [ 結婚習俗、結婚禁忌、訂婚禁忌、 新房佈置禁忌](http://live.ok123.com.tw/b9.htm)
4.  [耕作制度改善](http://book.tndais.gov.tw/yreport/87yearbook12.htm)
5.  [農業知識入口網](https://kmweb.coa.gov.tw/jigsaw2010/Detail.aspx?item=260002)
6.  [台中地區蔬菜栽培之現況與展望](http://ecaaser3.ecaa.ntu.edu.tw/weifang/lab551/vegetable/%E5%8F%B0%E4%B8%AD%E5%9C%B0%E5%8D%80%E8%94%AC%E8%8F%9C%E6%A0%BD%E5%9F%B9.htm)

7.   Taipei Pineapple Cake Cultural
    Festival|accessdate=2017-06-12|author=|date=|last=|first=|work=|publisher=|language=}}
8.  [果菜類/冬瓜/冬瓜簡介](http://web.tari.gov.tw/techcd/%E8%94%AC%E8%8F%9C/%E6%9E%9C%E8%8F%9C%E9%A1%9E/%E5%86%AC%E7%93%9C/%E5%86%AC%E7%93%9C%E7%B0%A1%E4%BB%8B.htm)