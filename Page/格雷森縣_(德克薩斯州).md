**格雷森縣**（**Grayson County,
Texas**）位[美國](../Page/美國.md "wikilink")[德克薩斯州東北部的一個縣](../Page/德克薩斯州.md "wikilink")，北鄰[奧克拉荷馬州](../Page/奧克拉荷馬州.md "wikilink")。面積2,536平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口110,595人。縣治[謝爾曼](../Page/謝爾曼_\(德克薩斯州\).md "wikilink")（Sherman）。

成立於1846年3月17日，縣政府成立於7月13日。縣名紀念[德克薩斯共和國司法部長](../Page/德克薩斯共和國.md "wikilink")[彼得·威廉·格雷森](../Page/彼得·威廉·格雷森.md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/得克萨斯州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.