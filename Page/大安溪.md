**大安溪**位於[臺灣中部](../Page/臺灣.md "wikilink")，屬於[中央管河川](../Page/中央管河川.md "wikilink")，主流河長95.76公里\[1\]，全臺第七大河川，流域面積758.47[平方公里](../Page/平方公里.md "wikilink")\[2\]，分佈於-{[苗栗縣](../Page/苗栗縣.md "wikilink")}-南部及[臺中市北部](../Page/臺中市.md "wikilink")。主流上流名為**[雪山溪](../Page/雪山溪.md "wikilink")**，發源於[雪山山脈之](../Page/雪山山脈.md "wikilink")[大霸尖山西側](../Page/大霸尖山.md "wikilink")，向西流至[東陽山北麓與支流](../Page/東陽山.md "wikilink")[馬達拉溪會合後](../Page/馬達拉溪.md "wikilink")，始稱大安溪。本流轉向西南流經雪見、大安（泰安鄉）、象鼻部落、士林部落『[士林水壩](../Page/士林攔河堰.md "wikilink")』後，形成-{[苗栗縣](../Page/苗栗縣.md "wikilink")}-、[臺中市交界](../Page/臺中市.md "wikilink")，為南北氣候的分水嶺，有「南邊太陽北邊雨」之稱，續流至烏石坑後，轉向西流至內灣（卓蘭鎮），轉西北西流經[卓蘭](../Page/卓蘭鎮.md "wikilink")、泰安、上館，隨後進入臺中市境，經[大甲區](../Page/大甲區.md "wikilink")[鐵砧山麓於](../Page/鐵砧山.md "wikilink")[大安區安田莊注入](../Page/大安區_\(臺中市\).md "wikilink")[臺灣海峽](../Page/臺灣海峽.md "wikilink")\[3\]。

## 分界

### 氣候

大安溪-火炎山-[大雪山是臺灣氣候最明顯的分界線](../Page/大雪山.md "wikilink")，大安溪北側的[三義由於受到大安溪和](../Page/三義.md "wikilink")[火炎山的影響](../Page/火炎山.md "wikilink")，當夏季[西南季風向北吹拂時](../Page/西南季風.md "wikilink")，直到火炎山才受到地形影響突然升高，上升的氣流隨著溫度下降，因而形成霧氣\[4\]；冬季東北季風夾帶冷鋒雲霧，亦常在三義附近終止。

### 行政區

大安溪的北邊是苗栗縣，南邊是臺中市，而臺中市唯一一塊土地在大安溪出海口的北端，就是[大甲區的](../Page/大甲區.md "wikilink")『溪北』，日南及西歧、銅安等里。

### 文化

進入政治[民主化](../Page/民主化.md "wikilink")[時代後](../Page/時代.md "wikilink")，所謂的[南綠北藍現象](../Page/南綠北藍.md "wikilink")，也是以濁水溪作為[政治](../Page/政治.md "wikilink")[名詞以引申為](../Page/名詞.md "wikilink")[泛綠和為](../Page/泛綠.md "wikilink")[泛藍的](../Page/泛藍.md "wikilink")[分界線或](../Page/分界線.md "wikilink")[分水嶺](../Page/分水嶺.md "wikilink")。濁水溪不但是[自然地理上的界線](../Page/自然地理學.md "wikilink")，而且也常是[政治地理上的分界](../Page/政治地理學.md "wikilink")。[2014年中華民國直轄市長及縣市長選舉中](../Page/2014年中華民國直轄市長及縣市長選舉.md "wikilink")，[中國國民黨慘敗](../Page/中國國民黨.md "wikilink")，使得[民主進步黨縣市版圖一舉突破](../Page/民主進步黨.md "wikilink")[濁水溪](../Page/濁水溪.md "wikilink")，還衝破大安溪，傳統的北藍南綠[分界線或](../Page/分界線.md "wikilink")[分水嶺大有以大安溪取代濁水溪的](../Page/分水嶺.md "wikilink")[趨勢](../Page/趨勢.md "wikilink")[現象](../Page/現象.md "wikilink")。

## 大安溪水系主要河川

以下由下游至源頭列出水系主要河川及其流域所屬行政區，其中粗體字為主流河道。

  - **大安溪**：-{[苗栗縣](../Page/苗栗縣.md "wikilink")}-、[臺中市](../Page/臺中市.md "wikilink")
      - [電火溪](../Page/電火溪.md "wikilink")（集會所溪）：臺中市[外埔區](../Page/外埔區.md "wikilink")、[后里區](../Page/后里區.md "wikilink")
      - [北門溝](../Page/北門溝.md "wikilink")：臺中市外埔區、后里區
      - [景山溪](../Page/景山溪.md "wikilink")：苗栗縣[三義鄉](../Page/三義鄉_\(臺灣\).md "wikilink")、[大湖鄉](../Page/大湖鄉_\(臺灣\).md "wikilink")、[卓蘭鎮](../Page/卓蘭鎮.md "wikilink")，附有[鯉魚潭水庫](../Page/鯉魚潭水庫.md "wikilink")
      - [大埔溪](../Page/大埔溪.md "wikilink")：臺中市后里區
      - [仙水坑溪](../Page/仙水坑溪.md "wikilink")：臺中市[東勢區](../Page/東勢區.md "wikilink")
      - [石壁坑溪](../Page/石壁坑溪.md "wikilink")：臺中市東勢區
          - [番埤溪](../Page/番埤溪.md "wikilink")：臺中市東勢區
          - [菜園溪](../Page/菜園溪.md "wikilink")：臺中市東勢區
      - [老庄溪](../Page/老庄溪.md "wikilink")：苗-{栗}-縣卓蘭鎮
      - [觀音山溪](../Page/觀音山溪.md "wikilink")：臺中市東勢區、臺中市[和平區](../Page/和平區_\(臺中市\).md "wikilink")
      - [烏石坑溪](../Page/烏石坑溪.md "wikilink")：臺中市和平區
          - [乾溪](../Page/乾溪_\(臺中縣\).md "wikilink")：臺中市和平區
      - [雪山坑溪](../Page/雪山坑溪.md "wikilink")：臺中市和平區
      - [麻必浩溪](../Page/麻必浩溪.md "wikilink")：苗-{栗}-縣[泰安鄉](../Page/泰安鄉.md "wikilink")
      - [南坑溪](../Page/南坑溪_\(苗栗縣\).md "wikilink")：苗-{栗}-縣泰安鄉
      - [北坑溪](../Page/北坑溪_\(苗栗縣\).md "wikilink")：苗-{栗}-縣泰安鄉
      - [大雪溪](../Page/大雪溪.md "wikilink")：苗-{栗}-縣泰安鄉
      - [馬達拉溪](../Page/馬達拉溪.md "wikilink")：苗-{栗}-縣泰安鄉
      - **[雪山溪](../Page/雪山溪.md "wikilink")**（次高溪）：苗-{栗}-縣泰安鄉

## 主要橋樑

以下由河口至源頭列出主流上之主要橋樑：

  - [大安溪橋](../Page/大安溪橋_\(台61線\).md "wikilink")（[省道](../Page/省道.md "wikilink")[台61線](../Page/台61線.md "wikilink")）
  - [大安溪橋](../Page/大安溪橋_\(台1線\).md "wikilink")（省道[台1線](../Page/台1線.md "wikilink")）
  - [舊大安溪橋](../Page/舊大安溪橋.md "wikilink")
  - [台鐵](../Page/台鐵.md "wikilink")[下大安溪橋](../Page/下大安溪橋.md "wikilink")（[台鐵](../Page/台鐵.md "wikilink")[西部幹線](../Page/西部幹線.md "wikilink")（海線））
  - [國道3號大安溪橋](../Page/國道3號大安溪橋.md "wikilink")（[國道3號](../Page/國道3號.md "wikilink")）
  - [高鐵大安溪橋](../Page/高鐵大安溪橋.md "wikilink")（[台灣高鐵](../Page/台灣高鐵.md "wikilink")）
  - [國道1號大安溪橋](../Page/國道1號大安溪橋.md "wikilink")（[國道1號](../Page/國道1號.md "wikilink")）
  - [新義里大橋](../Page/新義里大橋.md "wikilink")（省道[台13線](../Page/台13線.md "wikilink")）
  - [義里大橋](../Page/義里大橋.md "wikilink")
  - [台鐵](../Page/台鐵.md "wikilink")[大安溪橋](../Page/大安溪橋_\(台鐵\).md "wikilink")（[台鐵](../Page/台鐵.md "wikilink")[西部幹線](../Page/西部幹線.md "wikilink")（山線））
  - [大安溪鐵橋](../Page/大安溪鐵橋.md "wikilink")（[台鐵](../Page/台鐵.md "wikilink")[西部幹線](../Page/西部幹線.md "wikilink")（舊山線））
  - [卓蘭大橋](../Page/卓蘭大橋.md "wikilink")（省道[台3線](../Page/台3線.md "wikilink")）
  - [白布帆大橋](../Page/白布帆大橋.md "wikilink")（鄉道[苗58線](../Page/苗58線.md "wikilink")）
  - [士林壩](../Page/士林壩.md "wikilink")
  - [象鼻大橋](../Page/象鼻大橋.md "wikilink")
  - [象鼻吊橋](../Page/象鼻吊橋.md "wikilink")
  - [梅象橋](../Page/梅象橋.md "wikilink")

## 圖片集

<File:大安溪大峽谷.JPG>|[大安溪大峽谷](../Page/大安溪大峽谷.md "wikilink") <File:Da-An>
River,
Taiwan.jpg|遠眺[高鐵列車通過大安溪與](../Page/台灣高鐵.md "wikilink")[鐵砧山](../Page/鐵砧山.md "wikilink")
[File:HuoYanShan.jpg|大安溪與](File:HuoYanShan.jpg%7C大安溪與)[火炎山](../Page/火炎山_\(苗栗縣\).md "wikilink")
<File:Da-An> River, Taiwan
(3).jpg|-{[苗栗縣](../Page/苗栗縣.md "wikilink")[三義鄉](../Page/三義鄉_\(臺灣\).md "wikilink")}-[火炎山](../Page/火炎山_\(苗栗縣\).md "wikilink")（右）
\-{[臺中市](../Page/臺中市.md "wikilink")[大甲區](../Page/大甲區.md "wikilink")}-[鐵砧山](../Page/鐵砧山.md "wikilink")（左）File:Jingshan
River near Liyutan
Dam.JPG|[鯉魚潭水庫附近的大安溪支流景山溪](../Page/鯉魚潭水庫.md "wikilink")
[File:大安溪.jpg|大安溪大甲段，前段是國道三號，後段是台灣高鐵，遠方露出黃土即火炎山](File:大安溪.jpg%7C大安溪大甲段，前段是國道三號，後段是台灣高鐵，遠方露出黃土即火炎山)。
[File:馬那邦山眺望大安溪2017.jpg|馬那邦山眺望大安溪](File:馬那邦山眺望大安溪2017.jpg%7C馬那邦山眺望大安溪)

## 相關條目

  - [中央管河川](../Page/中央管河川.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")
  - [台灣河流長度列表](../Page/台灣河流長度列表.md "wikilink")
  - [大安溪大峽谷](../Page/大安溪大峽谷.md "wikilink")

## 註釋

[大安溪水系](../Category/大安溪水系.md "wikilink")
[Category:苗栗縣河川](../Category/苗栗縣河川.md "wikilink")
[Category:台中市河川](../Category/台中市河川.md "wikilink")
[Category:台灣海峽](../Category/台灣海峽.md "wikilink")

1.
2.
3.
4.