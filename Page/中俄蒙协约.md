[中俄蒙协约签订现场.jpg](https://zh.wikipedia.org/wiki/File:中俄蒙协约签订现场.jpg "fig:中俄蒙协约签订现场.jpg")和“少卿衔上大夫驻墨西哥特命全权公使”[陈箓](../Page/陈箓.md "wikilink")，俄方代表是“大俄罗斯帝国大皇帝特派驻蒙古外交官兼总领事国务正参议官”[亚力山大·密勒尔](../Page/亚力山大·密勒尔.md "wikilink")，代表外蒙古的是“外蒙古博克多哲布尊丹巴呼图克图汗特派司法副长额尔德尼卓囊贝子”[希尔宁达木定和](../Page/希尔宁达木定.md "wikilink")“财务长土谢图亲王”[察克都尔扎布](../Page/察克都尔扎布.md "wikilink")。这张照片是条约签订现场，坐在桌子一侧左起分别是陈箓、毕桂芳、密勒尔、希尔宁达木定和察克都尔扎布。\]\]
《**中俄蒙协约**》，即《**恰克图条约**》（；），是1915年[中国](../Page/中華民國_\(大陸時期\).md "wikilink")[北洋政府](../Page/北洋政府.md "wikilink")、[沙俄與](../Page/俄罗斯帝国.md "wikilink")[外蒙古签订的条约](../Page/外蒙古.md "wikilink")。

1911年下半年，[清朝灭亡](../Page/清朝.md "wikilink")，在沙俄的支持下，外蒙古[王公与](../Page/王公.md "wikilink")[博克多格根宣布](../Page/博克多格根.md "wikilink")[外蒙古独立](../Page/外蒙古独立.md "wikilink")，其後沙俄同蒙古签订《[俄蒙协约](../Page/俄蒙协约.md "wikilink")》和《[商務專條](../Page/商務專條.md "wikilink")》，引發中国政府反对。袁世凯政府为换取沙俄的援助和承认，于1913年11月5日与之签订了《[中俄声明文件](../Page/中俄声明文件.md "wikilink")》5条和《[中俄声明另件](../Page/中俄声明另件.md "wikilink")》4条。沙俄承认外蒙古为中国领土的一部分，而中国政府亦承认《俄蒙协约》的内容和外蒙古的自治，并不得在外蒙古设治、驻军、移民等，实际承认沙俄对外蒙古的控制权。

根据上列声明，中国全权专使[毕桂芳](../Page/毕桂芳.md "wikilink")、沙俄驻蒙外交官兼总[领事](../Page/领事.md "wikilink")[亚力山大·密勒尔及外蒙古代表](../Page/亚力山大·密勒尔.md "wikilink")[希尔宁达木定](../Page/希尔宁达木定.md "wikilink")，经过48次正式会议和40次私下会晤，于1915年6月7日在[恰克图签订此约](../Page/恰克图.md "wikilink")。共22条。主要内容是：

  - 外蒙古承认1913年11月5日《中俄声明文件》及《中俄声明另件》；
  - 外蒙古承认中国[宗主权](../Page/宗主权.md "wikilink")，中、俄承认外蒙古自治，为中国领土之一部分；
  - 自治外蒙无权与外国订立关于政治及土地关系的国际条约；
  - 中俄承认外蒙自治官府有办理一切[内政之权](../Page/内政.md "wikilink")；
  - 中国商民之货入自治外蒙古，无论何种出产，不纳[关税](../Page/关税.md "wikilink")，但须按照自治外蒙古人民所纳自治外蒙古已设及将来添设之各项内地[货捐一律交纳](../Page/货捐.md "wikilink")。
  - 博克多格根仍然是自治君主

通过这一协约，中国持有對外蒙古的宗主权，而沙俄则保持了在外蒙古的各种特权。

## 参考文献

## 参见

  - [中俄关系](../Page/中俄关系.md "wikilink")
  - [布连斯奇条约](../Page/布连斯奇条约.md "wikilink")
  - [外蒙古独立](../Page/外蒙古独立.md "wikilink")

{{-}}

[category:中俄条约](../Page/category:中俄条约.md "wikilink")

[Category:不平等条约](../Category/不平等条约.md "wikilink")
[Category:中華民國條約](../Category/中華民國條約.md "wikilink")
[Category:俄羅斯帝國條約](../Category/俄羅斯帝國條約.md "wikilink")
[Category:蒙古國條約](../Category/蒙古國條約.md "wikilink")
[Category:中国蒙古族历史](../Category/中国蒙古族历史.md "wikilink")
[Category:20世紀條約](../Category/20世紀條約.md "wikilink")
[Category:1915年6月](../Category/1915年6月.md "wikilink")