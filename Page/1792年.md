「」

## 大事记

  - [1月9日](../Page/1月9日.md "wikilink")——[奧圖曼帝國與](../Page/奧圖曼帝國.md "wikilink")[俄羅斯帝國簽署](../Page/俄羅斯帝國.md "wikilink")《[雅西和約](../Page/雅西和約.md "wikilink")》，[奧圖曼帝國割讓](../Page/奧圖曼帝國.md "wikilink")[德涅斯特河以東的廣闊領土](../Page/德涅斯特河.md "wikilink")，[第六次俄土戰爭結束](../Page/第六次俄土戰爭.md "wikilink")。
  - [4月20日](../Page/4月20日.md "wikilink")——[法國向](../Page/法国.md "wikilink")[奧地利宣戰](../Page/奥地利.md "wikilink")。
  - [4月25日](../Page/4月25日.md "wikilink")——[法國首次使用](../Page/法国.md "wikilink")[斷頭台執行](../Page/斷頭台.md "wikilink")[死刑](../Page/死刑.md "wikilink")。
  - [5月17日](../Page/5月17日.md "wikilink")——[纽约股票交易所成立](../Page/纽约证券交易所.md "wikilink")。
  - [6月6日](../Page/6月6日.md "wikilink")——[法蘭茲二世在](../Page/法蘭茲二世_\(神聖羅馬帝國\).md "wikilink")[布達經](../Page/布達.md "wikilink")[加冕儀式後](../Page/加冕.md "wikilink")，成為匈牙利及克羅埃西亞國王。
  - [7月1日](../Page/7月1日.md "wikilink")——[福康安](../Page/福康安.md "wikilink")、[海蘭察率清軍進兵](../Page/海蘭察.md "wikilink")[廓爾喀](../Page/廓爾喀.md "wikilink")，發動[熱索橋之戰](../Page/廓爾喀之役.md "wikilink")。
  - [7月11日](../Page/7月11日.md "wikilink")——[法國國民議會發布一篇名為](../Page/法國.md "wikilink")「[祖國處於危難之中](../Page/祖國處於危難之中.md "wikilink")」的宣言。
  - [7月14日](../Page/7月14日.md "wikilink")——[法蘭茲二世在](../Page/法蘭茲二世_\(神聖羅馬帝國\).md "wikilink")[法蘭克福經](../Page/法蘭克福.md "wikilink")[加冕儀式後](../Page/加冕.md "wikilink")，成為[神聖羅馬帝國皇帝](../Page/神聖羅馬帝國.md "wikilink")。
  - [8月9日](../Page/8月9日.md "wikilink")
      - [台灣](../Page/台灣.md "wikilink")[嘉義](../Page/嘉義縣.md "wikilink")[梅山地區發生](../Page/梅山鄉_\(臺灣\).md "wikilink")[大地震](../Page/1792年梅山大地震.md "wikilink"),
        專家推定其規模亦達7.1。
      - [法蘭茲二世在](../Page/法蘭茲二世_\(神聖羅馬帝國\).md "wikilink")[布拉格經](../Page/布拉格.md "wikilink")[加冕儀式後](../Page/加冕.md "wikilink")，成為波希米亞國王。
  - [8月10日](../Page/8月10日.md "wikilink")——[法国大革命](../Page/法国大革命.md "wikilink")：逮捕[路易十六](../Page/路易十六.md "wikilink")，建立[國民公會](../Page/國民公會.md "wikilink")。
  - [8月13日](../Page/8月13日.md "wikilink")——[法國大革命](../Page/法国大革命.md "wikilink")：[路易十六及其](../Page/路易十六.md "wikilink")[-{王后}-被關入監獄](../Page/玛丽·安托瓦内特.md "wikilink")。
  - [8月25日](../Page/8月25日.md "wikilink")——[普魯士元帥](../Page/普魯士.md "wikilink")[卡爾·威廉·斐迪南發表宣言](../Page/卡爾·威廉·斐迪南.md "wikilink")，聲稱如[法國國王](../Page/法國國王.md "wikilink")[路易十六受到傷害](../Page/路易十六.md "wikilink")，將懲罰[巴黎人民](../Page/巴黎.md "wikilink")。
  - [9月2日](../Page/9月2日.md "wikilink")——普奧聯軍進占[凡爾登](../Page/凡爾登.md "wikilink")，[巴黎面臨淪陷的危險](../Page/巴黎.md "wikilink")。
  - [9月20日](../Page/9月20日.md "wikilink")——[瓦爾密戰役](../Page/瓦爾密戰役.md "wikilink")，法軍在[凡爾登以南的](../Page/凡爾登.md "wikilink")[瓦爾密擊潰普奧聯軍](../Page/瓦爾密.md "wikilink")。
  - [9月21日](../Page/9月21日.md "wikilink")——[法國](../Page/法國.md "wikilink")[國民公會開幕](../Page/國民公會.md "wikilink")，確保了[吉倫特派掌握政權](../Page/吉倫特派.md "wikilink")。
  - [9月22日](../Page/9月22日.md "wikilink")——[法兰西第一共和国成立](../Page/法兰西第一共和国.md "wikilink")。
  - [9月23日](../Page/9月23日.md "wikilink")——廓爾喀管事頭人噶箕第烏達特塔巴來到清軍大營，請求歸誠，並代廓王、王叔進京朝覲。（參見[廓爾喀之役](../Page/廓爾喀之役.md "wikilink")）
  - [10月13日](../Page/10月13日.md "wikilink")——[美国](../Page/美國.md "wikilink")[白宫奠基](../Page/白宫.md "wikilink")。
  - [11月2日](../Page/11月2日.md "wikilink")——美國第二屆四年一度的[總統選舉於本日展開投票作業](../Page/1792年美国总统选举.md "wikilink")。
  - [12月3日](../Page/12月3日.md "wikilink")——美國第二屆[總統選舉完成投票作業](../Page/1792年美国总统选举.md "wikilink")。總統[喬治·華盛頓獲得選舉人團一致的投票](../Page/喬治·華盛頓.md "wikilink")，接續他的第二個任期。

## 出生

  - [2月29日](../Page/2月29日.md "wikilink")——[罗西尼](../Page/吉奥阿基诺·罗西尼.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[歌剧](../Page/歌剧.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")
  - [5月21日](../Page/5月21日.md "wikilink")——[科里奥利](../Page/贾斯帕-古斯塔夫·科里奥利.md "wikilink")，[法国科学家](../Page/法国.md "wikilink")
  - [8月4日](../Page/8月4日.md "wikilink")——-[雪莱](../Page/雪萊.md "wikilink")，英国诗人，（[1822年去世](../Page/1822年.md "wikilink")）
  - [12月1日](../Page/12月1日.md "wikilink")（[公曆](../Page/公历.md "wikilink")，合[儒略曆](../Page/儒略曆.md "wikilink")[11月20日](../Page/11月20日.md "wikilink")）——[罗巴切夫斯基](../Page/罗巴切夫斯基.md "wikilink")，俄国[数学家](../Page/数学家.md "wikilink")（[1856年逝世](../Page/1856年.md "wikilink")）

## 逝世

  - [4月30日](../Page/4月30日.md "wikilink")——[約翰·孟塔古，第四代三文治伯爵](../Page/約翰·孟塔古，第四代三明治伯爵.md "wikilink")，[英國](../Page/英国.md "wikilink")[政治家](../Page/政治家.md "wikilink")。（[1718年出生](../Page/1718年.md "wikilink")）
  - [8月5日](../Page/8月5日.md "wikilink")——[諾斯勳爵](../Page/腓特烈·諾斯，諾斯勳爵.md "wikilink")，前[英國首相](../Page/英国首相.md "wikilink")。（[1732年出生](../Page/1732年.md "wikilink")）
  - [9月16日](../Page/9月16日.md "wikilink")——[阮惠](../Page/阮惠.md "wikilink")，[越南](../Page/越南.md "wikilink")[西山朝第](../Page/西山朝.md "wikilink")2代皇帝。（[1753年出生](../Page/1753年.md "wikilink")）

[\*](../Category/1792年.md "wikilink")
[2年](../Category/1790年代.md "wikilink")
[9](../Category/18世纪各年.md "wikilink")