**多田骏**（）。宮城县人，[大日本帝国陆军大将](../Page/大日本帝国.md "wikilink")。[仙台陸軍地方幼年學校第一期生](../Page/仙台陸軍地方幼年學校.md "wikilink")、[日本陆军士官学校](../Page/日本陆军士官学校.md "wikilink")15期生、[日本陆军大学](../Page/日本陆军大学.md "wikilink")25期生。1945年被[远东国际军事法庭定为甲级战犯嫌疑人](../Page/远东国际军事法庭.md "wikilink")（未予立案，后被释）。

在[日本侵华战争中不支持战争扩大化](../Page/日本侵华战争.md "wikilink")，希望用谈判的方式使[蒋中正](../Page/蒋中正.md "wikilink")[国民政府屈服](../Page/国民政府.md "wikilink")。后因与[东条英机意见相左](../Page/东条英机.md "wikilink")，在东条担任首相后被转入[预备役直至战争结束](../Page/预备役.md "wikilink")。

## 生平

  - 1903年 陆军士官学校毕业
  - 1904年 作为陆军炮兵少尉参加[日俄战争](../Page/日俄战争.md "wikilink")
  - 1905年 任炮兵中尉
  - 1913年 任炮兵大尉，陆军大学毕业
  - 1917年 中国政府应聘（[北京陸軍大學教官](../Page/北京陸軍大學.md "wikilink")）
  - 1919年 任炮兵少佐，出任日本陆军大学教官
  - 1923年 任炮兵中佐
  - 1928年 野炮第4联队联队长
  - 1930年 陆军第16师团参谋长
  - 1932年 满洲国军政最高顾问
  - 1935年 [中國驻屯军司令官](../Page/中國驻屯军.md "wikilink")
  - 1936年 第11师团师团长
  - 1937年 [参谋本部次长](../Page/參謀本部_\(大日本帝國\).md "wikilink")、陆军大学校长（兼任）
  - 1938年 [華北方面军司令官](../Page/華北方面军.md "wikilink")
  - 1940年 被授予二级[金鵄勳章](../Page/金鵄勳章.md "wikilink")
  - 1941年 晋升为大将，同年转入预备役（以后务农生活）
  - 1945年 作为甲级战犯被逮捕
  - 1948年 患[胃癌去世](../Page/胃癌.md "wikilink")

## 侵华战争不扩大论者

1937年7月的[七七事变令](../Page/七七事变.md "wikilink")[侵华战争开始](../Page/日本侵華戰爭.md "wikilink")。可是比起蔣中正政府，多田駿更重视[苏联的威脅](../Page/苏联.md "wikilink")。因此，与参谋本部作战部长[石原莞尔少将](../Page/石原莞尔.md "wikilink")、陆军军务科长的[柴山兼四郎大佐一样反對扩大戰爭](../Page/柴山兼四郎.md "wikilink")。

1937年末，发现了跟蔣中正议和的机会，开展以[德国中間人的和平工作](../Page/德国.md "wikilink")（[陶德曼调停](../Page/陶德曼调停.md "wikilink")）。

1938年1月15日[大本營连络会议上作为参谋本部次长出席](../Page/大本營_\(大日本帝國\).md "wikilink")，力抗主张中止「陶德曼调停」的[近衞文麿首相](../Page/近衞文麿.md "wikilink")、[广田弘毅外务大臣](../Page/广田弘毅.md "wikilink")、[杉山元陆军大臣](../Page/杉山元.md "wikilink")、與[米内光政海军大臣](../Page/米内光政.md "wikilink")。儘管日本政府及参谋本部都不主张继续和平工作，但多田仍然獨自主张继续對蒋介石的和谈。1月16日，近卫首相宣言「不以蔣中正为谈判对手」，多田駿的努力宣告失敗。

## 其他

策划[刺杀张作霖事件的](../Page/皇姑屯事件.md "wikilink")[河本大作是其妻兄](../Page/河本大作.md "wikilink")。

在陸軍大學教官時代，曾經向學生提出「若手上有一萬名中國俘虜的話，該如何處理？」的問題，多田的標準答案是：「解除武裝後全部釋放，讓他們回家鄉自謀生計。」

[Category:日本陆军大将](../Category/日本陆军大将.md "wikilink")
[Category:中日戰爭人物](../Category/中日戰爭人物.md "wikilink")
[Category:日俄戰爭人物](../Category/日俄戰爭人物.md "wikilink")
[Category:滿洲國日本人](../Category/滿洲國日本人.md "wikilink")
[Category:在中華民國的日本人](../Category/在中華民國的日本人.md "wikilink")
[Category:日本陸軍大學校友](../Category/日本陸軍大學校友.md "wikilink")
[Category:日本陸軍士官學校校友](../Category/日本陸軍士官學校校友.md "wikilink")
[Category:宮城縣出身人物](../Category/宮城縣出身人物.md "wikilink")
[Category:甲級戰犯嫌疑人](../Category/甲級戰犯嫌疑人.md "wikilink")
[Category:罹患胃癌逝世者](../Category/罹患胃癌逝世者.md "wikilink")