**曹庆泽**（），[湖南](../Page/湖南.md "wikilink")[澧县人](../Page/澧县.md "wikilink")；大专学历。1952年加入[中国共产党](../Page/中国共产党.md "wikilink")；是[中共第十三](../Page/中共.md "wikilink")、十四、十五届[中纪委委员](../Page/中纪委.md "wikilink")，十四、十五届[中纪委副书记](../Page/中纪委.md "wikilink")。曾任[中华人民共和国监察部部长](../Page/中华人民共和国监察部.md "wikilink")，[中国监察学会名誉会长等职](../Page/中国监察学会.md "wikilink")。

## 生平

[中华人民共和国建国前夕](../Page/中华人民共和国.md "wikilink")，曹庆泽加入[中国人民解放军](../Page/中国人民解放军.md "wikilink")。后来，他一直在[四川工作](../Page/四川.md "wikilink")；历任[中共](../Page/中共.md "wikilink")[四川](../Page/四川.md "wikilink")[梁平县区委书记](../Page/梁平.md "wikilink")，[四川省委农村工作部副处长](../Page/四川.md "wikilink")，[四川省农业办公室副主任](../Page/四川省.md "wikilink")，省政府副秘书长，[中共](../Page/中共.md "wikilink")[四川省纪委副书记](../Page/四川.md "wikilink")、书记等职。1989年的“[六四](../Page/六四.md "wikilink")”事件之后，曹庆泽坚决力查[四川省的政治清查工作](../Page/四川省.md "wikilink")，他的表現获得时任[中组部部长](../Page/中组部.md "wikilink")[宋平的赞赏](../Page/宋平.md "wikilink")。于是在[宋平的推荐下](../Page/宋平.md "wikilink")，曹庆泽于1989年11月，在[中共十三届五中全会上](../Page/中共.md "wikilink")，被增补为[中纪委常委](../Page/中纪委.md "wikilink")。在1992年中共十四大上，当选正部级的中纪委副书记，排名第三。在1997年十五大上，曹庆泽当选[中纪委副书记](../Page/中纪委.md "wikilink")，排名副书记中第二位，位列[韩杼滨之后](../Page/韩杼滨.md "wikilink"),由于韩为[最高人民检察院检察长](../Page/最高人民检察院.md "wikilink")，曹成为中纪委的常务副书记。1993年3月到1998年3月间，曹庆泽还在[李鹏的第二届内阁中](../Page/李鹏.md "wikilink")，被任命为[监察部部长](../Page/监察部.md "wikilink")。

2015年2月19日，曹庆泽在北京去世\[1\]。

## 参考文献

[Category:中华人民共和国监察部部长](../Category/中华人民共和国监察部部长.md "wikilink")
[Category:中国共产党党员
(1952年入党)](../Category/中国共产党党员_\(1952年入党\).md "wikilink")
[Category:澧县人](../Category/澧县人.md "wikilink")
[Q](../Category/曹姓.md "wikilink")

1.