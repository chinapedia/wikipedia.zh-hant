《**Twins**》是[香港](../Page/香港.md "wikilink")[組合](../Page/組合.md "wikilink")[Twins的首張音樂專輯](../Page/Twins.md "wikilink")，於2001年8月15日在[香港發行](../Page/香港.md "wikilink")，憑《明愛暗戀補習社》及《女校男生》兩曲廣受[香港歡迎](../Page/香港.md "wikilink")。

## 曲目列表

## 曲目資料

  - 《明愛暗戀補習社》為[Twins以可愛著稱](../Page/Twins.md "wikilink")，並因其形象貼近校園少女而廣受學生的喜愛。

## 獎項

  - [IFPI全年最高銷量新人](../Page/IFPI.md "wikilink")
  - [IFPI全年最暢銷新人](../Page/IFPI.md "wikilink")

[Category:Twins音樂專輯](../Category/Twins音樂專輯.md "wikilink")
[Category:2001年音樂專輯](../Category/2001年音樂專輯.md "wikilink")
[Category:2001年迷你專輯](../Category/2001年迷你專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")