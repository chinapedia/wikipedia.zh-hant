**亦舒**（），原名**倪亦舒**，[华裔加拿大人](../Page/华裔加拿大人.md "wikilink")，生於[上海](../Page/上海.md "wikilink")，祖籍[浙江省](../Page/浙江省.md "wikilink")[寧波市](../Page/寧波市.md "wikilink")[镇海区](../Page/镇海区_\(宁波市\).md "wikilink")，著名[香港小說及散文女作家](../Page/香港.md "wikilink")，另有筆名**依莎貝**。她亦是作家[倪匡的妹妹](../Page/倪匡.md "wikilink")。

## 簡歷

1951年，5歲的亦舒隨同家人從[中國大陸](../Page/中國大陸.md "wikilink")[上海到](../Page/上海.md "wikilink")[香港定居](../Page/香港.md "wikilink")。她在香港曾經就讀[蘇浙小學幼稚園](../Page/蘇浙小學.md "wikilink")，[官立嘉道理爵士小學及](../Page/官立嘉道理爵士小學.md "wikilink")[北角官立小學](../Page/北角官立小學.md "wikilink")，並於1964年何東女子官立中學(現名[何東中學](../Page/何東中學.md "wikilink"))預科畢業。她在15歲唸中學時期已經開始寫稿，中學畢業後，曾任《[明報](../Page/明報.md "wikilink")》記者，及電影雜誌採訪和[編輯等](../Page/編輯.md "wikilink")。
1973年，亦舒赴[英國](../Page/英國.md "wikilink")[曼徹斯特荷令斯學院修讀](../Page/曼徹斯特.md "wikilink")[酒店及食物管理](../Page/酒店及食物管理.md "wikilink")。1977年於台灣與午馬合組電影公司，後轉任台灣圓山飯店女侍應總管。1978年9月任職香港中環[富麗華酒店](../Page/富麗華酒店.md "wikilink")（已拆卸）公關部，再轉至佳視任職編劇。之後，轉任[香港政府新聞處新聞官達七年半](../Page/香港政府新聞處.md "wikilink")，現時已經長居[北美洲](../Page/北美洲.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[西溫哥華](../Page/西溫哥華.md "wikilink")。

亦舒一直都是一位多產專業作家，除了小說作品外，亦以筆名「依莎貝」在《[明報周刊](../Page/明報周刊.md "wikilink")》撰寫專欄。她的科幻作品不時會看到兄長[倪匡作品的角色客串出現](../Page/倪匡.md "wikilink")，如[原振俠](../Page/原振俠.md "wikilink")，小郭等。而她筆下的女角色大部份自愛自強，特立獨行的姿態影響一眾華文讀者。

## 風格

亦舒小說的女主角多是都會裡聰敏幹練的白領女性，但在感情生活中卻往往飽經滄桑，或感到都市優秀男性的匱乏、苦於難覓知音。亦舒的語言風格也很有特色，潑辣、尖刻、逼真，常以三言兩語切中時弊，她的文字非常精煉，沒有任何多餘的聯接。亦舒的作品有著鮮明的時代印記，主要是以香港和歐美的現代都市作為背景。她描寫的愛情故事，主人公的感情發展與身邊的社會關係網絡有著重大的聯繫。其兄長倪匡曾說過：“亦舒自小在香港長大，她的小說，和香港人的脈搏頻率相同，是地道的香港文學。她的小說不矯揉造作，有著香港人的性格。”
2010年，有專欄表示亦舒是最時尚的小說家，因在資訊貧乏的年代，不少女士從亦舒小說中學懂穿衣技巧及配搭\[1\]。

## 香港天地圖書有限公司出版之《亦舒系列》（小說258本，散文48本）

1.  荳芽集（散文）
2.  荳芽集二集（散文）
3.  荳芽集三集（散文）
4.  家明與玫瑰（小說）
5.  [玫瑰的故事](../Page/玫瑰的故事.md "wikilink")（小說）
6.  自白書（散文）
7.  珍珠（小說）
8.  曼陀羅（小說）
9.  薔薇泡沫（小說）
10. 留英學生日誌（散文）
11. 獨身女人（小說）
12. 舒雲集（散文）
13. 舒服集（散文）
14. 我的前半生（小說）
15. 寶貝（小說）
16. 星之碎片（小說）
17. 香雪海（小說）
18. 歇腳處（散文）
19. 販駱駝誌（散文）
20. 兩個女人（小說）
21. 藍鳥記（小說）
22. 黑白講（散文）
23. 風信子（小說）
24. [喜寶](../Page/喜寶.md "wikilink")（小說）
25. 野孩子（小說）
26. 回南天（小說）
27. 五月與十二月（小說）
28. 今夜星光燦爛（小說）
29. 偶遇（小說）
30. 自得之場（散文）
31. 開到荼蘼（小說）
32. 舊歡如夢（小說）
33. 花事了（小說）
34. 散髮（小說）
35. 暮（小說）
36. 過客（小說）
37. 戲（小說）
38. [心之全蝕](../Page/心之全蝕.md "wikilink")（小說）
39. 退一步想（散文）
40. 璧人（小說）
41. [她比煙花寂寞](../Page/她比煙花寂寞.md "wikilink")（小說）
42. 可人兒（小說）
43. 白衣女郎（小說）
44. 傳奇（小說）
45. 胭脂（小說）
46. 今夜不（小說）
47. 剎那芳華（散文）
48. 琉璃世界（小說）
49. 杜鵑花日子（小說）
50. 小火焰（小說）
51. 不要放棄春天（小說）
52. 哀綠綺思（小說）
53. 蝴蝶吻（小說）
54. 無才可去補蒼天（散文）
55. 曾經深愛過（小說）
56. 豈有豪情似舊時（散文）
57. 阿細之戀（小說）
58. 雨花（小說）
59. [朝花夕拾](../Page/朝花夕拾_\(小说\).md "wikilink")（小說）
60. 我醜（小說）
61. 我之試寫室（散文）
62. 藍這個顏色（小說）
63. 惱人天氣（小說）
64. 沒有月亮的晚上（小說）
65. 貓兒眼（小說）
66. 拍案驚奇（小說）
67. 請勿收回（小說）
68. 精靈（小說）
69. 紅鞋兒（小說）
70. 圓舞（小說）
71. 試練（小說）
72. 戀後（小說）
73. 且自逍遙沒誰管（散文）
74. 金環蝕 （小說）
75. 安琪兒寫照（小說）
76. 小玩意（小說）
77. 樂未央（散文）
78. 玉梨魂（小說）
79. 伊人（小說）
80. 綺惑（小說）
81. 一個夏天（小說）
82. 說故事的人（小說）
83. 流金歲月（小說）
84. 一段雲（小說）
85. 花裙子（小說）
86. 異鄉人（小說）
87. 忽爾今夏（小說）
88. 嘆息橋（小說）
89. 得魚忘筌（散文）
90. 莫失莫忘（小說）
91. 意綿綿（散文）
92. 人淡如菊（小說）
93. 西岸陽光充沛（小說）
94. 有過去的女人（小說）
95. 石榴圖（小說）
96. 說明書（散文）
97. 可是當年人面（散文）
98. 譬如朝露（小說）
99. 風滿樓（小說）
100. 滿院落花簾不卷（小說）
101. 紅的燈綠的酒 (小說)
102. 烈火 (小說)
103. 古老誓約 (小說)
104. 七姐妹 (小說)
105. 玻璃珠的歎息 (小說)
106. 紫微願 (小說)
107. 三個願望 (小說)
108. 封面 (小說)
109. 阿修羅 (小說)
110. 推薦書 (散文)
111. [我們不是天使](../Page/我們不是天使.md "wikilink") (小說)
112. 小朋友 (小說)
113. 一把青雲 (小說)
114. 男男女女 (小說)
115. 練習本 (散文)
116. 聽我細說 (小說)
117. 傷城記 (小說)
118. 迷迭香 (小說)
119. 鍾情 (小說)
120. 癡情司 (小說)
121. 賣火柴女孩 (小說)
122. 連環 (小說)
123. 鏡子 (小說)
124. 弄潮兒 (小說)
125. 慰寂寥 (小說)
126. 預言 (小說)
127. 美麗的她 (小說)
128. 表態書 (散文)
129. 美麗新世界 (小說)
130. 晚兒 (小說)
131. [一千零一妙方](../Page/一千零一妙方.md "wikilink") (小說)
132. 金粉世界 (小說)
133. 求真記 (小說)
134. 心扉的信 (小說)
135. 仲夏日之夢 (小說)
136. 變遷 (小說)
137. 生活誌 (散文)
138. 不要愛上她 (小說)
139. 小宇宙 (小說)
140. 沒有季節的都會 (小說)
141. 老閒話 (散文)
142. 表演 (小說)
143. 他人的夢 (小說)
144. 尋芳記 (小說)
145. 天若有情 (小說)
146. 縱橫四海 (小說)
147. 年輕的心 (小說)
148. 假夢真淚 (小說)
149. 銀女 (小說)
150. 天秤座事故 (小說)
151. 流光 (小說)
152. 生活之旅 (小說)
153. 月亮背面 (小說)
154. 變形記 (小說)
155. 皮相 (散文)
156. 在那遙遠的地方 (小說)
157. 藍色都市 (小說)
158. 寂寞夜 (小說)
159. 絕對是個夢 (小說)
160. 禿筆 (散文)
161. 如何說再見 (小說)
162. 燈火闌珊處 (小說)
163. 偷窺 (小說)
164. 紅塵 (小說)
165. 仕女圖 (小說)
166. 舊雨 (散文)
167. 不易居 (小說)
168. 鏡花緣 (小說)
169. 隨想 (散文)
170. 承歡記 (小說)
171. 三小無猜 (小說)
172. 寒武紀 (散文)
173. 密碼 (小說)
174. 隨意 (散文)
175. 寂寞鴿子 (小說)
176. 隨心 (散文)
177. 憔悴三年 (小說)
178. 美嬌嬝 (小說)
179. 隨緣 (散文)
180. 綺色佳 (小說)
181. 我心 (小說)
182. 等待 (小說)
183. 花解語 (小說)
184. 真男人不哭泣 (小說)
185. 錯先生 (小說)
186. 假使蘇西墮落 (小說)
187. 我愛，我不愛 (小說)
188. 女神 (小說)
189. 鄰室的音樂 (小說)
190. 請你請你原諒我 (小說)
191. 直至海枯石爛 (小說)
192. 黑羊 (小說)
193. 老房子 (小說)
194. 不羈的風 (小說)
195. 故園 (小說)
196. 手倦拋書午夢長 (散文)
197. 尋找失貓 (小說)
198. 寂寞的心俱樂部 (小說)
199. 緊些，再緊些 (小說)
200. 如果牆會說話 (小說)
201. 這雙手雖然小 (小說)
202. 天上所有的星 (小說)
203. 要多美麗就多美麗 (小說)
204. 幽靈吉卜賽 (小說)
205. 我答應你 (小說)
206. 印度墨 (小說)
207. 蟬 (小說)
208. 艷陽天 (小說)
209. 一個女人兩張床 (小說)
210. 只有眼睛最真 (小說)
211. 小人兒 (小說)
212. 明年給你送花來 (小說)
213. 一點舊一點新 (小說)
214. 她成功了我沒有 (小說)
215. 悄悄的一線光 (小說)
216. 吃南瓜的人 (小說)
217. 花常好月常圓人長久 (小說)
218. 小紫荊 (小說)
219. 同門 (小說)
220. 我確是假裝 (小說)
221. 這樣的愛拖一天是錯一天 (小說)
222. 噓---- (小說)
223. 她的二三事 (小說)
224. 月是故鄉明 (散文)
225. 早上七八點鐘的太陽 (小說)
226. 鄰居太太的情人 (小說)
227. 如果你是安琪 (小說)
228. 紫色平原 (小說)
229. 我情願跳舞 (小說)
230. 只是比較喜歡寫 (散文)
231. 電光幻影 (小說)
232. 蓉島之春 (小說)
233. 愛可以下載嗎 (小說)
234. 雪肌 (小說)
235. 特首小姐你早 (小說)
236. 葡萄成熟的時候 (小說)
237. 此一時也彼一時也 (散文)
238. 剪刀替針做媒人 (小說)
239. 乒乓 (小說)
240. 恨煞 (小說)
241. 孿生 (小說)
242. 漫長迂迴的路 (小說)
243. 忘記他 (小說)
244. 愛情只是古老傳說 (小說)
245. 迷藏 (小說)
246. 靈心 (小說)
247. 大君 (小說)
248. 眾裡尋他 (小說)
249. 吻所有女孩 (小說)
250. 尚未打烊 (散文)
251. [一個複雜故事](../Page/一個複雜故事.md "wikilink") (小說)
252. 畫皮 (小說)
253. 愛情慢慢殺死你 (小說)
254. 你的素心 (小說)
255. 地盡頭 (小說)
256. 有時他們回家 (小說)
257. 禁足 (小說)
258. 謊容 (小說)
259. 不二心 (散文)
260. 潔如新 (小說)
261. 從前有一隻粉蝶 (小說)
262. 三思樓 (小說)
263. 四部曲 (小說)
264. 德芬郡奶油 (小說)
265. 君還記得我否 (小說)
266. 少年不愁(小說)
267. 塔裏的六月(小說)
268. 佩鎗的茱麗葉(小說)
269. 世界換你微笑(小說)
270. 天堂一樣 (小說)
271. 那一天，我對你說 (小說)
272. 蜜糖只有你 (小說)
273. 先吃甜品 (散文)
274. 掰(小說)
275. 我倆不是朋友(小說)
276. 外遇(小說)
277. 燦爛的美元(小說)
278. 代尋失去時光(小說)
279. 實在平凡的奇異遭遇(小說)
280. 櫻唇(小說)
281. 陌生人的糖果(小說)
282. 藍襪子之旅(小說)
283. 女記者手記(小說)
284. 佳偶(小說)
285. 向前走，不回頭(散文)
286. 紅杏與牆(小說)
287. 悠悠我心(小說)
288. 黑、白、許多灰(小說)
289. 無暇失戀(散文精選)
290. 那男孩(小說)
291. 露水的世(小說)
292. 紅到幾時(散文精選)
293. 有你，沒有你(小說)
294. 我哥(散文精選)
295. 大宅(小說)
296. 幸運星(小說)
297. 微積分(小說)
298. 如果有，還未找到(散文)
299. 某家的女兒(小説)
300. 衷心笑(小説)
301. 紅樓夢裏人(散文精選)
302. 阿波羅的神壇(小說)
303. 不一樣的口紅(小說)
304. 新女孩(小說)
305. 森莎拉(小說)
306. 珍瓏(小說)
307. 這是戰爭(小說)
308. 寫作這回事(散文)
309. 去年今日此門(小說)
310. 好好好(小說）
311. 結或不結 離或不離 (小說）

## 外部連結

  - [亦舒在新浪的BLOG (**非亦舒作者本人編寫**. 文章多從舊的散文集中取出來,
    如留英學生日誌)](http://blog.sina.com.cn/m/1shu)
  - [天地圖書網站](https://www.cosmosbooks.com.hk/)

## 參考

[Category:镇海县人](../Category/镇海县人.md "wikilink")
[N](../Category/香港宁波人.md "wikilink")
[N](../Category/宁波裔上海人.md "wikilink")
[Category:香港小說家](../Category/香港小說家.md "wikilink")
[Y](../Category/愛情小說作家.md "wikilink")
[Y](../Category/倪匡家族.md "wikilink")
[Y](../Category/寧波裔加拿大人.md "wikilink")
[Y](../Category/华裔加拿大人.md "wikilink")
[Y](../Category/大温华裔加拿大人.md "wikilink")
[Category:倪姓](../Category/倪姓.md "wikilink")
[Category:北角官立小學校友](../Category/北角官立小學校友.md "wikilink")
[Category:何東中學校友](../Category/何東中學校友.md "wikilink")

1.  [有一種虛榮叫「亦舒」｜Ming’s
    People](https://www.mings-fashion.com/%E4%BA%A6%E8%88%92-%E6%99%82%E8%A3%9D-%E5%93%81%E5%91%B3-8357/)