**傅蕾蕾**（**Jenny
Pat**，），原名畢蕾蕾，父母离异后随母姓，改名傅蕾蕾，出生於[香港](../Page/香港.md "wikilink")，為中日混血兒。是著名[中國](../Page/中國.md "wikilink")[國畫大師](../Page/國畫.md "wikilink")[傅抱石的外孫女](../Page/傅抱石.md "wikilink")。

母親傅益璇为[傅抱石之次女](../Page/傅抱石.md "wikilink")，1970年由[內地來港定居](../Page/中國內地.md "wikilink")，因此畢蕾蕾在香港出生\[1\]，曾在香港[瑪利諾修院學校的小学与中学部念书](../Page/瑪利諾修院學校.md "wikilink")。之后远赴加拿大入读威尔斯亲王中学，成绩优异。毕业后考取奖学金就读[加拿大](../Page/加拿大.md "wikilink")[溫哥華](../Page/溫哥華.md "wikilink")[英屬哥倫比亞大學](../Page/英屬哥倫比亞大學.md "wikilink")，获藝術歷史系与数学系双学士学位。\[2\]。1998年在温哥华生活期间，她半她工半读加入溫哥華中文電視台[新時代電視任天氣預報主播](../Page/新時代電視.md "wikilink")，其後轉任節目主持，曾主持多個受歡迎節目，包括2001年至2002年的《[螢幕八爪娛](../Page/螢幕八爪娛.md "wikilink")》(What's
On)，該節目曾培育出多名主持或[香港小姐](../Page/香港小姐.md "wikilink")，如前任有線娛樂新聞台主播[陳貝兒](../Page/陳貝兒.md "wikilink")、2006年香港小姐冠軍[陳茵媺](../Page/陳茵媺.md "wikilink")、2007年香港小姐冠軍[張嘉兒等](../Page/張嘉兒.md "wikilink")。除了电视台的工作，毕蕾更活跃于温哥华的演艺圈，为影视人协会会员，屡次参加演出舞台剧，主持大型节目，更曾获大学的华人歌唱比赛冠军。尽管如此，毕在温哥华的一次访问里向媒体表示，她一直只把演艺工作当作兴趣，而事业的志向还是在艺术工作方面，并无打算回港投身娱乐圈。

畢蕾蕾於2004年大學畢業後離職，並移居北京，在中国嘉德国际拍賣行工作，于2006回流香港定居，加入了國際知名拍賣行[佳士得任职中国书画鑑定師](../Page/佳士得.md "wikilink")，曾親自以天價拍賣出外祖父国画大师[傅抱石的作品](../Page/傅抱石.md "wikilink")，成為佳話。\[3\]

## 2008年意外

2008年2月9日，畢蕾蕾從所居住的香港半山區[羅便臣道](../Page/羅便臣道.md "wikilink")70號雍景臺1座三樓外牆處跌落至二樓簷篷，傷勢輕微。媒體報導當日中午12點畢蕾蕾從三樓失足落下被居民發現而報警。消防員到場把已陷入半昏迷的畢女救下，急送[瑪麗醫院搶救](../Page/瑪麗醫院.md "wikilink")。
\[4\]\[5\]\[6\]\[7\]\[8\]\[9\]

## 注释及参考

## 外部链接

  -
[F傅](../Category/鑑定師.md "wikilink")
[Category:加拿大新聞報導員](../Category/加拿大新聞報導員.md "wikilink")
[Category:前新時代電視藝員](../Category/前新時代電視藝員.md "wikilink")
[P](../Category/瑪利諾修院學校校友.md "wikilink")
[Category:不列顛哥倫比亞大學校友](../Category/不列顛哥倫比亞大學校友.md "wikilink")
[F傅](../Category/拍賣會主持人.md "wikilink")
[P](../Category/华裔混血儿.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:大温华裔加拿大人](../Category/大温华裔加拿大人.md "wikilink")
[Category:溫哥華人](../Category/溫哥華人.md "wikilink")
[Category:香港人](../Category/香港人.md "wikilink")
[L](../Category/畢姓.md "wikilink")
[Category:日裔混血儿](../Category/日裔混血儿.md "wikilink")

1.

2.

3.

4.
5.
6.

7.

8.  [Ming Pao article: "Ex-Fairchild Television presenter Jenny Pat
    fallsnews.sina.com.hk/cgi-bin/nw/show.cgi/2/1/1/634523/1.html Sina
    News article: "Fu Bao-shi granddaughter scales wall and
    injured"](http://www.mingpaonews.com/20080210/goa1.htm)

9.  [Wen Wei Po article: "Fu Bao-shi granddaughter Jenny Pat fell from
    building"](http://paper.wenweipo.com/2008/02/10/YO0802100014.htm)