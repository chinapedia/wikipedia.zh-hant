**筏馱摩那**\[1\]，或譯**笩駄摩那**、**伐達摩那**（[梵语](../Page/梵语.md "wikilink")：
，，意謂**光榮者**），原名**尼乾陀若提子**（），著名[古印度](../Page/古印度.md "wikilink")[宗教](../Page/宗教.md "wikilink")[思想家](../Page/思想家.md "wikilink")，[印度列国时代](../Page/印度列国时代.md "wikilink")[跋耆国人](../Page/跋耆.md "wikilink")，[耆那教的創始者](../Page/耆那教.md "wikilink")，被教徒尊稱為**大雄**（，音譯**摩訶毘羅**，即**偉大的英雄**）。[耆那教相信](../Page/耆那教.md "wikilink")，他是第24位并且是最后一位[蒂爾丹嘉拉](../Page/蒂爾丹嘉拉.md "wikilink")（tīrthankara，意謂**祖師**）。

筏馱摩那與[釋迦牟尼生活在同一個時代](../Page/釋迦牟尼.md "wikilink")，且与釋迦牟尼的生平类似，筏馱摩那被[佛教歸入](../Page/佛教.md "wikilink")[六師外道](../Page/六師外道.md "wikilink")。

## 名稱

[佛經記載](../Page/佛經.md "wikilink")，其名為**尼乾若提子**，或**尼乾陀若提子**。尼乾是他的教派名稱，其名若提子，名字來自其母親\[2\]\[3\]。

## 生平

筏馱摩那的出生地點，自古有許多說法，有三種說法最被人所熟知：包括今天的[瓦伊沙利縣](../Page/瓦伊沙利縣.md "wikilink")，[比哈爾邦](../Page/比哈爾邦.md "wikilink")[贾穆伊](../Page/贾穆伊.md "wikilink")，或是[王舍城附近的](../Page/王舍城.md "wikilink")[那爛陀](../Page/那爛陀.md "wikilink")。其中[比哈爾邦](../Page/比哈爾邦.md "wikilink")[贾穆伊即是](../Page/贾穆伊.md "wikilink")[十六雄国时期](../Page/十六雄国.md "wikilink")[跋耆国首都](../Page/跋耆.md "wikilink")[吠舍离外](../Page/吠舍离.md "wikilink")45公里的贡得村。

筏馱摩那與釋迦牟尼一樣，是部落[首領的兒子](../Page/首領.md "wikilink")，在良好的環境中長大。三十歲時筏馱摩那放棄了財產、家庭（他有一個妻子和女兒）和舒適的環境，決定出走尋求[真理](../Page/真理.md "wikilink")，追求精神上的覺醒。在之後的十二年半，他透過進行密集的[冥想和深度的](../Page/冥想.md "wikilink")[懺悔](../Page/懺悔.md "wikilink")，最終得到了[專一知識](../Page/專一知識.md "wikilink")（Kevala
Jñāna），也就是最終的啟示。之後，他用餘下的三十年生命遊方傳道，走遍了印度，傳授他的哲學，反對[種姓制度](../Page/種姓制度.md "wikilink")，主張[和平主義](../Page/和平主義.md "wikilink")，[反暴力](../Page/反暴力.md "wikilink")，還有[不殺生](../Page/不殺生.md "wikilink")、[不妄語](../Page/不妄語.md "wikilink")、[不偷盜](../Page/不偷盜.md "wikilink")、[不邪淫及](../Page/不邪淫.md "wikilink")[不執着的](../Page/不執着.md "wikilink")[五誓言](../Page/五誓言.md "wikilink")。筏馱摩那認為，透过[禅定和自我实现达到了](../Page/禅定.md "wikilink")[觉悟的人](../Page/觉悟.md "wikilink")，他们就是耆那（jina），即[勝者](../Page/勝者.md "wikilink")，故稱[耆那教](../Page/耆那教.md "wikilink")。

西元前527年，筏馱摩那辭世，年七十二。生前有弟子多人，賢者十一人，但只有[印德拉菩提](../Page/印德拉菩提.md "wikilink")（Indrabhūti）與[蘇達摩](../Page/蘇達摩.md "wikilink")（Sudharma）繼續傳道，因為筏馱摩那大多數弟子因修道嚴厲，斷食而死。佛教非常批判此般的苦行。

## 佛教記載

《[中阿含經](../Page/中阿含經.md "wikilink")·尼乾經》中記載了他的學說：

《[中阿含經](../Page/中阿含經.md "wikilink")·優婆離經（一三三經）》記載，筏馱摩那的死因，可能與其弟子[優婆離改信佛教](../Page/優婆離.md "wikilink")，他氣極[吐血有關](../Page/吐血.md "wikilink")：

## 註釋與引用

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - *Note: ISBN refers to the UK:Routledge (2001) reprint. URL is the
    scan version of the original 1884 reprint*

## 外部連結

  -
  -
  - [Harvard Pluralism Project:
    Jainism](http://www.fas.harvard.edu/~pluralsm/affiliates/jainism/)

[Category:宗教相關人物](../Category/宗教相關人物.md "wikilink")
[Category:印度人](../Category/印度人.md "wikilink")
[Category:印度哲学家](../Category/印度哲学家.md "wikilink")
[Category:耆那教](../Category/耆那教.md "wikilink")
[Category:宗教創始人](../Category/宗教創始人.md "wikilink")

1.  [全國宗教資訊網.筏馱摩那](https://religion.moi.gov.tw/Knowledge/Content?ci=2&cid=383)
2.  《慧琳音義》：「尼乾陀此-{云}-無繼，是外道總名也。若提-{云}-親友，是母名。」
3.  《玄應音義》：「尼乾或作尼乾陀，應言泥健連他，譯-{云}-不繫也。」