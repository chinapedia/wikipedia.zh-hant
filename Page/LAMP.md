[LAMP_software_bundle.svg](https://zh.wikipedia.org/wiki/File:LAMP_software_bundle.svg "fig:LAMP_software_bundle.svg")相结合\]\]
**LAMP**是指一组通常一起使用来运行动态网站或者服务器的[自由软件名称首字母缩写](../Page/自由软件.md "wikilink")：

  - [**L**inux](../Page/Linux.md "wikilink")，[操作系统](../Page/操作系统.md "wikilink")
  - [**A**pache](../Page/Apache_HTTP_Server.md "wikilink")，[网页服务器](../Page/网页服务器.md "wikilink")
  - [**M**ariaDB或](../Page/MariaDB.md "wikilink")[**M**ySQL](../Page/MySQL.md "wikilink")，[数据库管理系统](../Page/数据库管理系统.md "wikilink")（或者[数据库服务器](../Page/数据库服务器.md "wikilink")）
  - [**P**HP](../Page/PHP.md "wikilink")、[**P**erl或](../Page/Perl.md "wikilink")[**P**ython](../Page/Python.md "wikilink")，[脚本语言](../Page/脚本语言.md "wikilink")

虽然这些[开放源代码程序本身并不是专门设计成同另几个程序一起工作的](../Page/开放源代码.md "wikilink")，但由于它们的廉价和普遍，这个组合开始流行（大多数[Linux发行版本捆绑了这些软件](../Page/Linux发行版本.md "wikilink")）。当一起使用的时候，它们表现的像一个具有活力的“解决方案包”（Solution
Packages）。其他的方案包有[苹果的](../Page/苹果公司.md "wikilink")（最初是[应用服务器](../Page/应用服务器.md "wikilink")），[Java](../Page/Java.md "wikilink")／[J2EE和](../Page/J2EE.md "wikilink")[微软的](../Page/微软.md "wikilink")[.NET架构](../Page/.NET.md "wikilink")。

“LAMP包”的脚本组件中包括了[CGI](../Page/通用网关接口.md "wikilink")
[web接口](../Page/web.md "wikilink")，它在90年代初期变得流行。这个技术允许[网页浏览器的用户在服务器上执行一个程序](../Page/网页浏览器.md "wikilink")，并且和接受静态的内容一样接受动态的内容。程序员使用[脚本语言来创建这些程序因为它们能很容易有效的操作文本流](../Page/脚本语言.md "wikilink")，甚至当这些文本流并非源自程序自身时也是。正是由于这个原因系统设计者经常称这些脚本语言为[胶水语言](../Page/胶水语言.md "wikilink")。

在一篇为德国电脑杂志《》（1998，第12期，230页）而写的文章中使用了缩略语“LAMP”。这篇文章意在展示一系列的自由软件成为了商业包的替换物。由于IT世界众所周知的对缩写的爱好，Kunze提出“LAMP”这一容易被市场接受的术语来普及自由软件的使用。

## 变体

[O'Reilly和](../Page/O'Reilly.md "wikilink")[MySQL
AB在英语人群中普及了这个术语](../Page/MySQL_AB.md "wikilink")。MySQL
AB自己的市场推广在某种程度上基于LAMP包的推广，其他的项目和厂商则推行这个术语的一些变体，包括：

  - **LAPP**（以[**P**ostgreSQL替代MySQL](../Page/PostgreSQL.md "wikilink")）
  - **LAMP**（最后两个字母意味着[**M**iddleware和](../Page/中间件.md "wikilink")[**P**ostgreSQL](../Page/PostgreSQL.md "wikilink")）
  - **LNMP**\[1\]或**LEMP**\[2\]（以[**N**ginx替代Apache](../Page/Nginx.md "wikilink")）
  - **WAMP**（以[Microsoft
    **W**indows替代Linux](../Page/Microsoft_Windows.md "wikilink")）
  - **MAMP**（以[**M**acintosh替代Linux](../Page/Apple_Macintosh.md "wikilink")）
  - **LAMJ**（以**J**SP/servlet替代PHP）
  - **BAMP**（以[**B**SD替代Linux](../Page/BSD.md "wikilink")）
  - **WIMP**（指Microsoft **W**indows, Microsoft
    [**I**IS](../Page/IIS.md "wikilink")，**M**ySQL, **P**HP）
  - **AMP**（单指**A**pache, **M**ySQL和**P**HP）
  - **XAMP**（以[**X**ML替代Linux](../Page/XML.md "wikilink")）\[3\]

一些人借用*LAMP*来描述一类可定制组成的系统，而不是制造一系列新词，并用它来表示这些系统和统一打包的页面开发环境的不同（例如[ASP](../Page/ASP.md "wikilink")，.NET和J2EE）。

## 參考文獻

<div class="references-small">

<references />

</div>

## 參見

  - [AppServ](../Page/AppServ.md "wikilink")

[Category:软件开发](../Category/软件开发.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:合稱](../Category/合稱.md "wikilink")
[Category:Linux](../Category/Linux.md "wikilink")
[Category:Perl](../Category/Perl.md "wikilink")
[Category:GNU](../Category/GNU.md "wikilink")
[Category:LAMP](../Category/LAMP.md "wikilink")

1.
2.
3.