**駒野友一**，於廣島縣立吉田高等學校畢業的[日本足球員](../Page/日本.md "wikilink")，司職後衛，目前效力於[日職聯的](../Page/日職聯.md "wikilink")[磐田山葉](../Page/磐田山葉.md "wikilink")。

## 個人簡介

### 球會簡歷

在小學生時期，駒野友一的位置是前鋒，但在中學二年級時卻開始在邊線位置作賽。中學三年級時，駒野仍在時，已經收到[大阪飛腳](../Page/大阪飛腳.md "wikilink")、[千葉市原和](../Page/千葉市原.md "wikilink")[廣島三箭的青年隊招攬](../Page/廣島三箭.md "wikilink")。最初，駒野友一考慮加入與他家鄉相鄰的[大阪飛腳青年隊](../Page/大阪飛腳.md "wikilink")，但在經濟考慮下，駒野沒有加入[大阪飛腳](../Page/大阪飛腳.md "wikilink")。同時間，駒野曾經參加[千葉市原青年隊的訓練](../Page/千葉市原.md "wikilink")，可是在狀況不良的情況下落選了該隊青年隊。最終在一次測試下，得到[廣島三箭的青年隊招攬](../Page/廣島三箭.md "wikilink")。在1997年正式加入[廣島三箭青年隊](../Page/廣島三箭.md "wikilink")\[1\]。不幸的是在加入青年隊前，駒野友一的父親不幸身故\[2\]。在青年隊時期，駒野友一成為了出色的翼衛，在高校三年級時與同級生的[森崎和幸和](../Page/森崎和幸.md "wikilink")[浩司兩兄弟一同成為](../Page/森崎浩司.md "wikilink")[日職聯的](../Page/日職聯.md "wikilink")。

2000年，駒野友一正式與[廣島三箭簽訂職業球員合約](../Page/廣島三箭.md "wikilink")，同期加入球隊的還有森崎兄弟、、[松下裕樹](../Page/松下裕樹_\(サッカー選手\).md "wikilink")、和。

2001年，[尼邦尼亞基安排駒野友一擔任右後衛與](../Page/瓦列里·涅波姆尼亞奇.md "wikilink")[澤田謙太郎爭奪正選資格](../Page/澤田謙太郎.md "wikilink")。此後，駒野友一表現穩定，並為球隊帶來2002年次階段聯賽第三位的成績。即使往後被安排為右翼衛，表現仍然出色。

2003年8月16日，對[横濱FC的賽事](../Page/横濱FC.md "wikilink")，駒野友一弄傷了左膝前十字[韌帶](../Page/韌帶.md "wikilink")。在手術後留院其間，駒野友一突然患上，並有生命危險\[3\]。駒野友一積極面對危機，並努力為重返球場而戰，終於在2004年4月29日的[日本聯賽盃對](../Page/日本聯賽盃.md "wikilink")[橫濱水手一戰中復出](../Page/橫濱水手.md "wikilink")。駒野友一休養其間，廣島並沒有一個球員能成為常規的右後衛正選。

2004年8月18日，駒野友一在[雅典奧運會足球賽對](../Page/2004年夏季奧林匹克運動會足球比賽.md "wikilink")[加納一戰中](../Page/加納國家足球隊.md "wikilink")，左邊的[鎖骨骨折](../Page/鎖骨.md "wikilink")。同年9月21日，駒野友一患上眼角膜炎，一度陷入失明的危機\[4\]。2005年，駒野友一再次回到右後衛的位置，並以優異表現與[佐藤壽人產生化學作用](../Page/佐藤壽人.md "wikilink")，帶來大量的入球機會，而駒野友一為球隊帶來9次助攻。

2008年，由於廣島要降至[日乙](../Page/日乙.md "wikilink")，駒野友一把握機會轉投[磐田山葉](../Page/磐田山葉.md "wikilink")，可是球隊低迷的表現，使得駒野友一連續兩年得到護級的經驗。2009年，駒野友一的球衣號碼由25號轉為與廣島時代一樣的5號，而位置亦由中場轉為後衛（邊線）。2010年，駒野友一延續了在邊線後衛活躍的表現，但是在代表日本進行國際足球友誼賽其間受傷，引致他未能參與後半季的賽事。

2011年，駒野友一以正選右後衛的身份出陣了聯賽的所有賽事，並在攻守兩方面為球隊作出重大貢獻。

2012年，駒野友一再次出戰了聯賽所有賽事，並首次入選日職聯全年最佳陣容。

## 相關資料

### 廣告出演

  - [デオデオ](../Page/デオデオ.md "wikilink") (2006年 - 2007年)
    [下田崇との共演](../Page/下田崇.md "wikilink")、広島限定
  - [プーマジャパン株式会社](../Page/プーマ.md "wikilink") (2010年 - ) 「パラメヒコマン篇」
  - [静岡県](../Page/静岡県.md "wikilink") (2010年)
    「静岡県[人権週間](../Page/人権週間.md "wikilink")」

### 音樂錄像

  - [PUSHIM](../Page/PUSHIM.md "wikilink")「誓い feat.MIHIRO～マイロ～」 (2010年)

## 脚注

## 南非世界杯成绩

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>日期</p></th>
<th><p>地點</p></th>
<th><p>對手</p></th>
<th><p>比數</p></th>
<th><p>結果</p></th>
<th><p>比賽</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1.</p></td>
<td><p>2010年6月14日</p></td>
<td><p><a href="../Page/南非.md" title="wikilink">南非</a><a href="../Page/布隆泉.md" title="wikilink">布隆泉</a></p></td>
<td></td>
<td><p>1-0</p></td>
<td><p>勝出</p></td>
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年世界盃足球賽</a></p></td>
</tr>
<tr class="even">
<td><p>2.</p></td>
<td><p>2010年6月19日</p></td>
<td><p><a href="../Page/南非.md" title="wikilink">南非</a><a href="../Page/德班.md" title="wikilink">德班</a></p></td>
<td></td>
<td><p>0-1</p></td>
<td><p>负</p></td>
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年世界盃足球賽</a></p></td>
</tr>
<tr class="odd">
<td><p>3.</p></td>
<td><p>2010年6月24日</p></td>
<td><p><a href="../Page/南非.md" title="wikilink">南非</a><a href="../Page/勒斯滕堡.md" title="wikilink">勒斯滕堡</a></p></td>
<td></td>
<td><p>3-1</p></td>
<td><p>勝出</p></td>
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年世界盃足球賽</a></p></td>
</tr>
<tr class="even">
<td><p>4.</p></td>
<td><p>2010年6月29日</p></td>
<td><p><a href="../Page/南非.md" title="wikilink">南非</a><a href="../Page/普勒托利亞.md" title="wikilink">普勒托利亞</a></p></td>
<td></td>
<td><p>0-0（点球3-5）</p></td>
<td><p>负</p></td>
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年世界盃足球賽</a></p></td>
</tr>
</tbody>
</table>

[Category:日本足球運動員](../Category/日本足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:廣島三箭球員](../Category/廣島三箭球員.md "wikilink")
[Category:日職球員](../Category/日職球員.md "wikilink")
[Category:磐田喜悅球員](../Category/磐田喜悅球員.md "wikilink")
[Category:福岡黃蜂球員](../Category/福岡黃蜂球員.md "wikilink")
[Category:2004年夏季奧林匹克運動會足球運動員](../Category/2004年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2007年亞洲盃足球賽球員](../Category/2007年亞洲盃足球賽球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:日本國家足球隊成員](../Category/日本國家足球隊成員.md "wikilink")
[Category:日本奧運足球運動員](../Category/日本奧運足球運動員.md "wikilink")
[Category:和歌山縣出身人物](../Category/和歌山縣出身人物.md "wikilink")

1.  同期還有森崎兄弟和久保田學

2.  [ゴールデンルーキー
    サンフレ7人衆](http://www.chugoku-np.co.jp/Sanfre/rookie/sanfre4.html)
    [中国新聞](../Page/中国新聞.md "wikilink")

3.  [【世界盃選手特集：駒野友一（廣島）】生命危險和害怕失明，雅典奧運世代的唯一世界盃戰士。](http://www.jsgoal.jp/news/jsgoal/00033714.html)
     、2006.05.31

4.