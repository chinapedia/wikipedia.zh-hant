**Xserve**是[蘋果電腦所出品的](../Page/蘋果電腦.md "wikilink")[機架式](../Page/機架.md "wikilink")[伺服器](../Page/伺服器.md "wikilink")，使用[Mac
OS X
Server](../Page/Mac_OS_X_Server.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")。

當Xserve在2002年發表時，它是蘋果電腦在1996年的[Apple Network
Server之後第一次的伺服器硬體設計](../Page/Apple_Network_Server.md "wikilink")。它最早包含一或二顆[PowerPC
G4處理器](../Page/PowerPC_G4.md "wikilink")，但後來轉換為新的[Power PC
G5](../Page/Power_PC_G5.md "wikilink")，随后再次转换为兩顆四核心的Intel「Nehalem核心架構」處理器。Xserve可作多種應用，包括檔案伺服器，網頁伺服器，甚至是以叢集技術執行高效能運算應用－Xserve叢集，也提供不包含顯示卡與光碟機的Xserve叢集節點。

2010年11月5日，蘋果電腦的網站聲明了XServe將在2011年1月31日起停產，並建議客戶改選購安裝Mac OS X
Server的[Mac Pro或](../Page/Mac_Pro.md "wikilink")[Mac
Mini電腦](../Page/Mac_Mini.md "wikilink")\[1\]。

## 沿革

### PowerPC G4

[蘋果電腦於](../Page/蘋果電腦.md "wikilink")2002年5月14日推出原先的**Xserve**。蘋果電腦把他歸類為“高密度，1U高度的機架伺服器”。Xserve搭配1到2個時脈為1.0
GHz的[PowerPC G4處理器](../Page/PowerPC_G4.md "wikilink")。最大到2
GB的[PC-2700](../Page/PC-2700.md "wikilink")
[DDR](../Page/DDR.md "wikilink")
[記憶體](../Page/記憶體.md "wikilink")，支援64位元的記憶體匯流排。3個[FireWire](../Page/FireWire.md "wikilink")
400埠（一個在前面，兩個在後方），2個[USB](../Page/USB.md "wikilink")
1.1埠（後方），一個[RS-232管理界面](../Page/RS-232.md "wikilink")（後方），以及一個內建[gigabit網路不提供額外的連線能力](../Page/gigabit.md "wikilink")。提供2個64位元／66MHz的[PCI插槽以及](../Page/PCI.md "wikilink")1個32位元／66MHz的PCI/[AGP插槽](../Page/AGP.md "wikilink")。在預設的設定下，兩個PCI插槽都是被[ATI
Technologies](../Page/ATi.md "wikilink") [Rage
128顯示卡和額外的gigabit以太網路卡所佔用](../Page/Rage_128.md "wikilink")。最大支援到4個[ATA](../Page/ATA.md "wikilink")／100硬碟（60或120
GB），放至於前方的[熱抽換區域](../Page/熱抽換.md "wikilink")，允許產生軟體[磁碟陣列](../Page/磁碟陣列.md "wikilink")0,
1，和5的陣列。一個載盤式的光碟機則置於前方。

一開始，有兩種規格的選向：一個單一處理器的Xserve搭配256MB記憶體，售價為2999[美元](../Page/美元.md "wikilink")。雙處理器的Xserve搭配512MB記憶體，售價為3999[美元](../Page/美元.md "wikilink")。兩種都是單一個60
GB硬碟，和搭配[Mac OS X v10.2](../Page/Mac_OS_X.md "wikilink")
“Jaguar”伺服器版本一起出售。

2003年2月10日，他們推出更新跟擴充Xserve產品線。這些更新包含1或2個1.33 GHz Power PC
G4處理器，兩個Firewire 800埠（後方），和更高量的UltraATA/133硬碟機（80或160
GB）。一個新的型號，稱為Xserver叢集節點也公開了，跟單一處理器的Xserve是同樣的價格，特色是有兩個1.33
GHz處理器，沒有光碟機，單一個硬碟空間，沒有顯示卡或乙太網路卡，和10個客戶端版本的“Jaguar”伺服器作業系統。 {{-}}

### Xserve RAID

2003年4月2日，[Xserve
RAID推出](../Page/Xserve_RAID.md "wikilink")，提供給Xserve的磁碟子系統更大的容量和更高的生產量。
{{-}}

### PowerPC G5

2004年1月6日，蘋果推出Xserve G5，一種重新設計更高效能的Xserve。32位元的PowerPC G4處理器被1或2顆64位元2
GHz的[PowerPC 970處理器取代](../Page/PowerPC_970.md "wikilink")。最大支援到8
GB的PC-3200 [ECC記憶體和](../Page/ECC.md "wikilink")128位元的記憶體匯流排。一個Firewire
400埠（前方），兩個Firewire 800埠（後方），兩個[USB](../Page/USB.md "wikilink")
2.0埠（後方）和[RS-232管理界面](../Page/RS-232.md "wikilink")（後方），和兩個內建的gigabit以太網路埠（後方）搭配[TCP
offload提供更高的連線能力](../Page/TCP_offload.md "wikilink")。一個133MHz/64位元和100MHz/64位元的[PCI-X插槽rounded](../Page/PCI-X.md "wikilink")
out its expansion
options。透氣的因素限制了他的3個[SATA熱抽換硬碟盒](../Page/SATA.md "wikilink")（每一個80或250
GB），原本第四個磁碟的空間用來作排氣孔。一個吸入式光碟機（CD-ROM標準，DVD-ROM/CD-RW是選購）裝置在前方。

有三種可以選擇的配備：單一處理器的Xserve
G5搭配512MB記憶體，售價為2999[美元](../Page/美元.md "wikilink")，雙處理器Xserve
G5搭配1
GB記憶體，售價為3999[美元](../Page/美元.md "wikilink")，雙處理器的叢集節點型號，搭配512MB記憶體，沒有光碟機，只有單一個硬碟空間，和10的客戶端版本的“Panther”伺服器版本，售價為2999[美元](../Page/美元.md "wikilink")。

Xserve G5有更大的記憶體容量和頻寬，也有[PowerPC
970更強大的浮點運算效能](../Page/PowerPC_970.md "wikilink")，讓他可以更適合[高效能運算的應用](../Page/高效能運算.md "wikilink")。[System
X是一種利用Xserve所建造出來的](../Page/System_X.md "wikilink")[叢集電腦之一](../Page/叢集電腦.md "wikilink")。

2005年1月3日，蘋果電腦將Xserve G5速度提升到兩顆2.3 GHz的PowerPC 970處理器。400
GB的硬碟也可以使用，最大可以到1.2
TB的內部儲存空間。吸入式光碟機升級到DVD-ROM/CD-RW標準的複合式光碟機，而DVD-/+RW則是選購配備。

[Mac OS X v10.4](../Page/Mac_OS_X_v10.4.md "wikilink")
“Tiger”伺服器版本发布后，开始提供给Xserve。 {{-}}

### Intel Xserve

蘋果電腦在2006年8月7日的[全球開發者大會上發表了基於Intel的Xserve](../Page/WWDC.md "wikilink")，相較於Xserve
G4與G5有相當大的更新。它們使用[時脈](../Page/時脈.md "wikilink")2 GHz, 2.66 GHz或是3
GHz的[Intel](../Page/Intel.md "wikilink")
[Xeon](../Page/Xeon.md "wikilink")「[Woodcrest核心架構](../Page/Xeon#Woodcrest.md "wikilink")」處理器，PC2-5300
([DDR2](../Page/DDR2_SDRAM.md "wikilink")-667)
[ECC](../Page/ECC.md "wikilink")
[FB-DIMM](../Page/FB-DIMM.md "wikilink")，ATI Radeon
X1300圖形處理器，使用三台750GB硬碟可達到的2.25[TB最大內部儲存空間](../Page/Terabyte.md "wikilink")，選購的電源供應器，1U機架高度。相對於Xserve
G4/G5，Intel Xserve將圖形顯示卡內建，這意味著將不需佔用一個PCI插槽來增加圖形處理能力。

2008年1月8日，蘋果電腦將Xserve升級為[時脈](../Page/時脈.md "wikilink")2.8 GHz或3
GHz的[Intel](../Page/Intel.md "wikilink")
[Xeon](../Page/Xeon.md "wikilink")「[Harpertown核心架構](../Page/Xeon#Harpertown.md "wikilink")」處理器，PC2-6400
([DDR2](../Page/DDR2_SDRAM.md "wikilink")-800)
[ECC](../Page/ECC.md "wikilink")
[FB-DIMM](../Page/FB-DIMM.md "wikilink")，以及使用三台1TB硬碟可達到的3TB最大內部儲存空間。在先前版本中的前置FireWire
400埠被USB 2.0埠所取代。

2009年4月7日，蘋果電腦將Xserve升級為[時脈](../Page/時脈.md "wikilink")2.26 GHz, 2.66 或是3
GHz的[Intel](../Page/Intel.md "wikilink")
[Xeon](../Page/Xeon.md "wikilink")「[Gainestown核心架構](../Page/Xeon#Gainestown.md "wikilink")」處理器，PC3-8500
([DDR3](../Page/DDR3_SDRAM.md "wikilink")-1066)
[ECC](../Page/ECC.md "wikilink")
[DIMM](../Page/DIMM.md "wikilink")，兩個FireWire 800埠，附帶256 MB
GDDR3圖形記憶體的NVIDIA GeForce GT 120圖形處理器，以及一個Mini
DisplayPort輸出。這次更新也增加了最大內部儲存空間，從2TB到6TB（使用三台2TB硬碟）。另外還可選購一台不需佔用磁碟槽的128
GB的[固态硬盘作為開機磁碟](../Page/固态硬盘.md "wikilink")。這台選購的開機磁碟可讓系統在保持上線狀態時更換所有的磁碟機。這也是蘋果電腦第一次在Xserve上使用無PVC的內部連接線與組件，以及配合[RoHS規範](../Page/RoHS.md "wikilink")，不使用[溴化阻燃劑](../Page/溴化阻燃劑.md "wikilink")。

2009年8月28日，蘋果電腦升級XServe為預設搭載[Mac OS X Server
10.6無限用戶端版本](../Page/Mac_OS_X_Server#Mac_OS_X_Server_10.6_\(Snow_Leopard_Server\).md "wikilink")。Mac
OS X 10.6 Server可支援到96 GB的記憶體（即使Xserve的機版並不支援這種配置）。 {{-}}

## 參考資料

## 外部連結

[Category:麦金塔](../Category/麦金塔.md "wikilink")
[Category:伺服器](../Category/伺服器.md "wikilink")

1.  }