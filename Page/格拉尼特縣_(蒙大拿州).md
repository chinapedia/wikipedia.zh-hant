**格拉尼特縣**（**Granite County,
Montana**）是[美國](../Page/美國.md "wikilink")[蒙大拿州西部的一個縣](../Page/蒙大拿州.md "wikilink")。面積4,489平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口2,830人。治所[菲利普斯堡](../Page/菲利普斯堡_\(蒙大拿州\).md "wikilink")（Philipsburg）。

成立於1893年3月2日。\[1\]縣名的意思是「花崗岩」，是縣內[一座山嶺的名稱](../Page/格拉尼特嶺.md "wikilink")，以出產[銀著名](../Page/銀.md "wikilink")。\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/蒙大拿州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.
2.  [Montana Railroad History - MONTANA PLACE
    NAMES](http://www.montanarailroadhistory.info/MontanaPlaceNames.htm)