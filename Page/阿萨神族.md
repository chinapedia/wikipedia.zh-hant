[Baldr_dead_by_Eckersberg.jpg](https://zh.wikipedia.org/wiki/File:Baldr_dead_by_Eckersberg.jpg "fig:Baldr_dead_by_Eckersberg.jpg")的尸体周围。由[Christoffer
Wilhelm
Eckersberg](../Page/Christoffer_Wilhelm_Eckersberg.md "wikilink")
绘于1817年。\]\]

**阿萨神族**（**ǫ́ss**或**ás**，複數為**Æsir**或**æsir**，女性為**ásynja**，女性複數為**ásynjur**）是[北欧神话中兩大神族其中一支](../Page/北欧神话.md "wikilink")，也是最主要的神族，代表著世界秩序神格化的存在，由許多不同特質的神明所組成。

## 語源

一般相信，同屬「[印歐語系](../Page/印歐語系.md "wikilink")」的各語系間有著共同的文化，而**Aesir**這個字和[原始日耳曼語的](../Page/原始日耳曼語.md "wikilink")**ansuz**、印度[梵語的](../Page/梵語.md "wikilink")**asura**及[阿維斯陀語的ahura](../Page/阿維斯陀語.md "wikilink")(可見於[祆教善神](../Page/祆教.md "wikilink")[阿胡拉·馬茲達的名字之中](../Page/阿胡拉·馬茲達.md "wikilink"))有著相同的語源。asura在梵語中，最初的意思指的是神，也就是「[阿修羅](../Page/阿修羅.md "wikilink")」，原本為善神，其性情好勇鬥狠。後在[印度神話中轉化成](../Page/印度神話.md "wikilink")[非天或惡神](../Page/非天.md "wikilink")。

## 北歐神話中的故事

在北欧神话中的另一支神族为[华纳神族](../Page/华纳神族.md "wikilink")，雙方開始戰鬥的起因，似乎是由[華納神族中的女神](../Page/華納神族.md "wikilink")——[古爾維格](../Page/古爾維格.md "wikilink")（）引起的，阿薩神族認為對方來意不善，因此由奧丁擲長矛，以示宣戰。戰爭持續了很久，兩方都厭倦了，因此決定以交換人質講和。阿薩神族這邊提出的人選是[海尼爾](../Page/海尼爾.md "wikilink")（）和智慧巨人—[密米爾](../Page/密米爾.md "wikilink")（）。而[華納神族那邊的人選是](../Page/華納神族.md "wikilink")[尼約德](../Page/尼約德.md "wikilink")（）的子女—[弗雷](../Page/弗雷.md "wikilink")（）和[弗蕾亞](../Page/弗蕾亞.md "wikilink")（）。\[1\]

有一說指出，根據考據，其實對掌管豐饒的[華納神族的崇拜](../Page/華納神族.md "wikilink")，有可能比對好戰的阿萨神族還要早。神話中的戰爭可能是反應一場宗教衝突。

## 参考文献

## 参见

  - [北欧神话](../Page/北欧神话.md "wikilink")
      - [华纳神族](../Page/华纳神族.md "wikilink")

{{-}}

[A](../Category/北歐神祇.md "wikilink")
[Category:眾神](../Category/眾神.md "wikilink")
[Category:传说中的种族](../Category/传说中的种族.md "wikilink")

1.  白蓮欣著，《北歐神話故事》，好讀出版，ISBN 9574554198