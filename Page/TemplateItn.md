<div id='column-itn' class='knoblock {{{class|}}}'>

![Debates_of_Petro_Poroshenko_and_Vladimir_Zelensky_(2019-04-19)_02.jpg](https://zh.wikipedia.org/wiki/File:Debates_of_Petro_Poroshenko_and_Vladimir_Zelensky_\(2019-04-19\)_02.jpg
"Debates_of_Petro_Poroshenko_and_Vladimir_Zelensky_(2019-04-19)_02.jpg")
{{\*mp|4月21日}}
喜剧演员[-{zh-hans:弗拉基米尔·泽连斯基;zh-hant:弗拉迪米爾·澤連斯基;zh-tw:佛拉迪米爾·澤倫斯基;}-在](../Page/弗拉基米尔·泽连斯基.md "wikilink")**[總統選舉](../Page/2019年乌克兰总统选举.md "wikilink")**<small>（圖）</small>擊敗[-{zh-cn:彼得·波罗申科;zh-tw:彼得·波洛申科;zh-hk:彼得·波羅申科;}-](../Page/彼得·波洛申科.md "wikilink")，當選新任[烏克蘭總統](../Page/烏克蘭總統.md "wikilink")。
{{\*mp|4月21日}} 斯里蘭卡[-{zh-cn:科伦坡; zh-sg:哥伦坡; zh-tw:可倫坡;
zh-hk:科倫坡;}-](../Page/可倫坡.md "wikilink")、[內貢博](../Page/內貢博.md "wikilink")、[巴提卡洛阿等地發生](../Page/巴提卡洛阿.md "wikilink")**[連環爆炸](../Page/2019年斯里蘭卡連環爆炸.md "wikilink")**，造成至少200人死亡。
{{\*mp|4月18日}} 德國天文學團隊在[天鵝座的行星狀星雲](../Page/天鵝座.md "wikilink")[NGC
7027中首次發現](../Page/NGC_7027.md "wikilink")**[氦合氫離子](../Page/氦合氫離子.md "wikilink")**的存在。
{{\*mp|4月18日}}
[美國司法部公布特別檢察官](../Page/美國司法部.md "wikilink")[-{zh-tw:勞勃;zh-hant:羅伯特;zh-hk:羅拔;zh-hans:罗伯特;}-·-{zh-tw:穆勒;zh-hans:米勒;zh-hant:米勒;}-團隊針對](../Page/羅伯特·穆勒.md "wikilink")[俄羅斯干預總統選舉的](../Page/俄羅斯干預2016年美國總統選舉.md "wikilink")**[調查報告](../Page/特別顧問調查.md "wikilink")**。
{{\*mp|4月18日}}
印度尼西亞總統[佐科·維多多和對手](../Page/佐科·維多多.md "wikilink")[普拉博沃·蘇比安托先後宣布在](../Page/普拉博沃·蘇比安托.md "wikilink")**[總統選舉](../Page/2019年印度尼西亞總統選舉.md "wikilink")**中獲勝。

</div>

<div id="column-feature-more">

{{\#ifeq:  | 首页 | {{\*mp}} |
 }}<span class="column-feature-more-header">[**最近逝世**](../Page/{{CURRENTYEAR}}年{{CURRENTMONTHNAME}}逝世人物列表.md "wikilink")</span>：[肖揚](../Page/肖揚.md "wikilink")
— [羅琳·華倫](../Page/華倫夫婦.md "wikilink") —
[阿蘭·加西亞](../Page/阿蘭·加西亞·佩雷斯.md "wikilink")
— [喬吉姆](../Page/喬吉姆.md "wikilink")

</div>

<noinclude>

-----

</noinclude>