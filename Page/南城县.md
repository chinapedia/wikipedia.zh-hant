**南城县**是[中国](../Page/中国.md "wikilink")[江西省](../Page/江西省.md "wikilink")[抚州市所辖的一个](../Page/抚州市.md "wikilink")[县](../Page/县.md "wikilink")。总面积为1698平方公里，2010年人口为32.6万。

## 历史沿革

[汉高祖五年](../Page/汉高祖.md "wikilink")（公元前202年）置县。
[北宋](../Page/北宋.md "wikilink")[太平兴国三年](../Page/太平兴国.md "wikilink")（978年）设[建昌军](../Page/建昌军.md "wikilink")，治南城。
[明](../Page/明.md "wikilink")[洪武二年](../Page/洪武.md "wikilink")（1369年）设[建昌府](../Page/建昌府.md "wikilink")。
民国初年，废府，南城直隶于江西省。 建国后，南城先后属抚州专区（地区）、抚州市。

## 行政区划

南城县下辖10个镇2个乡。

## 地理

南城县位于抚州市中部，东邻资溪、黎川，南连南丰、黎川，西靠宜黄、临川，北靠临川、金溪。地势东、西高，中部成南北贯通的河谷平川地带，山地分布在东西两侧，盱江由南而北经城垣贯穿全境。

### 气候

南城县属中亚热带季风性湿润气候，四季分明，降水充沛。1月平均气温5.8℃，极端最低气温-10.9℃（1991年12月29日）。7月平均气温28.8℃，极端最高气温41.5℃（1953年8月10日）。年平均气温17.8℃。年均降水量1704.8毫米，主要集中在春夏季。

## 交通

  - [向莆铁路](../Page/向莆铁路.md "wikilink")
  - [鹰汕铁路](../Page/鹰汕铁路.md "wikilink")（规划中）
  - [京福高速公路](../Page/京福高速公路.md "wikilink")

## 经济

以农业为辅。工业重点发展

## 风景名胜

[Nancheng_Juxing_Ta_2014.05.29_16-27-35.jpg](https://zh.wikipedia.org/wiki/File:Nancheng_Juxing_Ta_2014.05.29_16-27-35.jpg "fig:Nancheng_Juxing_Ta_2014.05.29_16-27-35.jpg")\]\]

  - [明益藩王墓地](../Page/明益藩王墓地.md "wikilink")（洪门益王家族墓群、岳口益王家族墓群）
  - [万年桥和](../Page/万年桥_\(南城\).md "wikilink")[聚星塔](../Page/聚星塔.md "wikilink")
  - [麻姑山](../Page/麻姑山.md "wikilink")
  - [潮音洞石窟](../Page/潮音洞石窟.md "wikilink")
  - [太平桥](../Page/太平桥_\(南城\).md "wikilink")
  - [洪门水库](../Page/洪门水库.md "wikilink")
  - [廖坊水库](../Page/廖坊水库.md "wikilink")

## 历代名人

  - [罗英](http://baike.baidu.com/item/%E7%BD%97%E8%8B%B1/11980)
  - [罗汝芳](../Page/罗汝芳.md "wikilink")
  - [李觏](../Page/李觏.md "wikilink")
  - [陈彭年](../Page/陈彭年.md "wikilink")
  - [黄希宪](../Page/黄希宪.md "wikilink")
  - [包恢](../Page/包恢.md "wikilink")
  - [张升](../Page/张升.md "wikilink")

## 参考文献

[南城县](../Page/category:南城县.md "wikilink")
[县](../Page/category:抚州区县.md "wikilink")
[抚州](../Page/category:江西省县份.md "wikilink")