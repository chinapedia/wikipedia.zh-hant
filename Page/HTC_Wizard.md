[Cingular_8125_(htc_wizard).jpg](https://zh.wikipedia.org/wiki/File:Cingular_8125_\(htc_wizard\).jpg "fig:Cingular_8125_(htc_wizard).jpg")
**HTC Wizard**，原廠型號**HTC
P4300**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink")
5，配備[TI](../Page/TI.md "wikilink") OMAP850
201MHz處理器，配有側滑動式[QWERTY鍵盤](../Page/QWERTY.md "wikilink")，性能強大。2005年10月於歐洲首度發表。已知客製版本HTC
P4300，Qtek 9100，Qtek A9100，Dopod 838，i-mate K-JAM，T-Mobile MDA Vario，O2
Xda Mini Pro，O2 Xda Mini S，Orange SPV M3000，Orange SPV M6000，Cingular
8125，Vodafone VPA Compact II，E-Plus Pocket PDA。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：TI OMAP850 195MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 5.0 PocketPC Phone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：128MB，RAM：64MB
  - 尺寸：108mm X 58mm X 23.7mm
  - 重量：168g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：GSM/EDGE
  - [藍牙](../Page/藍牙.md "wikilink")：Bluetooth 1.2
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b
  - [相機](../Page/相機.md "wikilink")：130萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1250mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC P4300
    概觀](https://web.archive.org/web/20081115061326/http://www.htc.com/us/product.aspx?id=10956)
  - [HTC P4300
    技術規格](https://web.archive.org/web/20081204145609/http://www.htc.com/us/product.aspx?id=10960)

[H](../Category/智能手機.md "wikilink")
[Wizard](../Category/宏達電手機.md "wikilink")
[Category:2005年面世的手機](../Category/2005年面世的手機.md "wikilink")