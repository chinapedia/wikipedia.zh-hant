and (

`  ( {{#if:``|1|0}} and {{#ifexist:`` {{#switch: ``}}}}}}`

| t | trad | traditional | 繁 | s | simp | simplified | simpified | 簡 | 简
| 1 | yes | 是 | auto | 自動 | 自动 =(消歧义) |\#default= }}|0|1}} ) or (
{{\#if:|0|1}} and {{\#ifexist: (消歧义)|0|1}} ) ) |

<div class="NavFrame collapsed" style="position: relative; float: right;background-color: transparent; border: none; z-index: 1;z-index:2;">

<div class="NavHead" style="width: 10px; background-color: transparent; font-weight: normal;line-height: 1em; padding: 0;">

<span class="NavToggle" style="color:red;width:16em;">提示：目標頁面不存在<span class="toggleShow" style="display: none;">▼</span><span class="toggleHide">▲</span></span>

</div>

<div class="NavContent" style="position: absolute; display: none; right: 0; border: 1px gray solid; background-color: lightyellow; padding: 0.3em; width: 370px;">

參數所指定的目標頁面不存在，建議更正成存在頁面或直接建立下列**一個**頁面（建立前請先搜尋是否有合適的存在頁面可以取代）：
{{\#ifexpr:

`{{#if:``|1|0}}`
`and ({{#switch: ``}}}}}}`

| t | trad | traditional | 繁 | s | simp | simplified | simpified | 簡 | 简
| 1 | yes | 是 | auto | 自動 | 自动=0 |\#default=1 }} ) |

  - [}}}](:{{{1.md "wikilink")

|

  - 如果條目名稱是繁體的話：

<!-- end list -->

  -
    \-{[}}}](:{{#if:{{{1.md "wikilink")}-

<!-- end list -->

  - 如果條目名稱是簡體的話：

<!-- end list -->

  -
    \-{[}}}](:{{#if:{{{1.md "wikilink")}-

注意如果條目名稱是繁體字要使用繁体的“**-{消歧義}-**”，簡体字要使用簡体的“**-{消歧义}-**”；**<span style="color: #ff0000;">名稱繁簡混用的頁面將會被快速刪除</span>**，但如果條目名稱的繁簡寫法是相同的話，可任選其一。
}}

</div>

</div>

<includeonly>{{\#switch: |user|user_talk|user talk= |\#default=
}}</includeonly> }} ![Disambig_gray.svg](Disambig_gray.svg
"Disambig_gray.svg")  {{\#if:}}}|本文介紹的是}}}。}}關於}}}}}}|是|1|yes=《|\#default=「}}**}}**{{\#switch:}}}}}}|是|1|yes=》|\#default=」}}名称相近或相同的条目}}}，請見「**[}}}|
 {{\#switch: }}}}}} | t | trad | traditional | 繁 =(消歧義) | s | simp |
simplified | simpified | 簡 | 简 =(消歧义) | 1 | yes | 是 | auto | 自動 | 自动
=(消歧義) |\#default= }}|}} {{\#switch: }}}}}} | t | trad | traditional
| 繁 =(消歧義) | s | simp | simplified | simpified | 簡 | 简 =(消歧义) |\#default
=(消歧義) }} }}{{\#if:|{{\!}}}}](:{{#if:{{{1.md "wikilink")**」{{\#if:|。}}。
}}<noinclude></noinclude>

[}}}](../Category/{{#if:{{{want.md "wikilink")