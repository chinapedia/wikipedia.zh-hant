**新西伯利亚地鐵**（）是[俄羅斯](../Page/俄羅斯.md "wikilink")[新西伯利亞的地鐵系統](../Page/新西伯利亞.md "wikilink")。

## 歷史

[Metro_novosibirsk_berezevaya_rossha.jpg](https://zh.wikipedia.org/wiki/File:Metro_novosibirsk_berezevaya_rossha.jpg "fig:Metro_novosibirsk_berezevaya_rossha.jpg")

[新西伯利亞](../Page/新西伯利亞.md "wikilink")，建于1893年，是[西伯利亞两条大動脈](../Page/西伯利亞.md "wikilink")[西伯利亞鐵路和](../Page/西伯利亞鐵路.md "wikilink")[鄂畢河交叉口上的交通要冲](../Page/鄂畢河.md "wikilink")，自建立之后发展迅速，现在是俄羅斯第三大城市，人口140萬人。

1960年代後期，建造捷運系統的計劃開始形成，并于1979年5月12日动工。借鉴着從其他蘇聯地鐵中得到的廣泛經驗，首段包含5个地铁站的路线完成於1986年1月7日開幕，歷時7年半，成為蘇聯第十一個地鐵和俄羅斯第四個地鐵。之后工程迅速擴大，已完成原本的62公里、4條線路的計劃；但1990年代初的財政困難导致大部分的工程被凍結，直到最近才恢復工作。

## 现实情况

該地鐵包含了13個車站（实际为12站，轉乘站算為兩次），兩條路線，20部四车厢列車，共80车厢，日流量可达25万人次。車站按照後期蘇聯風格裝飾。在目前的12個車站中，有七個是支柱结构（[pillar-trispan](../Page/:en:pillar-trispan.md "wikilink")），四個是單拱頂结构，還有一個地上車站，连着2.145公里长的跨[鄂毕河大桥](../Page/鄂毕河.md "wikilink")。

## 路線

| \#     | 名稱                                                    | 開放年份   | 最新車站增加年份 | 長度                                  | 車站總數 |
| ------ | ----------------------------------------------------- | ------ | -------- | ----------------------------------- | ---- |
| **1**  | [列寧線](../Page/列寧線_\(新西伯利亞地鐵\).md "wikilink") ()       | 1986   | 1992     | 10.5 [公里](../Page/公里.md "wikilink") | 8    |
| **2**  | [捷爾任斯基線](../Page/捷爾任斯基線_\(新西伯利亞地鐵\).md "wikilink") () | 1987   | 2010     | 5.5 [公里](../Page/公里.md "wikilink")  | 5    |
| **總數** | **16 km**                                             | **13** |          |                                     |      |

## 外部連結

  - [Andrey Pozdnyakov's的網站](http://www.metronsk.narod.ru/)
  - [Urbanrail.net
    資訊](https://web.archive.org/web/20060924231320/http://www.urbanrail.net/as/novo/novosibirsk.htm)

[Category:俄羅斯地鐵](../Category/俄羅斯地鐵.md "wikilink")
[Category:新西伯利亞](../Category/新西伯利亞.md "wikilink")
[Category:1986年建立](../Category/1986年建立.md "wikilink")