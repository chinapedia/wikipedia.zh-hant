**馮曦妤**（英文名：****，），原名**馮翠樺**，是一名[香港](../Page/香港.md "wikilink")[歌手兼](../Page/歌手.md "wikilink")[作詞人](../Page/作詞人.md "wikilink")，以歌聲清脆而著名。

## 簡歷

馮曦妤在16歲時經朋友介紹下加入[陳光榮的錄音室](../Page/陳光榮.md "wikilink")，開始參與電影的配樂以及和音演唱的幕後工作，並牽起她對音樂的濃厚興趣。2000年，她加盟Click
Music成為旗下唱作人，自此便全心投入音樂創作行列，期間她完成的廣告配唱多達200首，亦為多套[夢工場動畫電影擔任Vocal](../Page/夢工場.md "wikilink")
Producer的工作。

2003年，她以**Fiona
Fung**的名義，憑一曲英文廣告歌《》而開始廣被認識，此曲亦是[容祖兒的](../Page/容祖兒.md "wikilink")《我的驕傲》及《揮着翅膀的女孩》的原曲，該曲更被[日本歌手](../Page/日本.md "wikilink")[藤田惠美翻唱](../Page/藤田惠美.md "wikilink")。她另一首知名歌曲《》，是[無綫電視劇集](../Page/無綫電視.md "wikilink")《[當四葉草碰上劍尖時](../Page/當四葉草碰上劍尖時.md "wikilink")》主題曲，亦曾被香港組合歌手[2R及](../Page/2R.md "wikilink")[Cookies翻唱過](../Page/Cookies_\(組合\).md "wikilink")。其續集《[赤沙印記@四葉草.2](../Page/赤沙印記@四葉草.2.md "wikilink")》的主題曲《》亦是由她主唱。另外，她曾經以[粵語演唱](../Page/粵語.md "wikilink")[康泰旅行社以及](../Page/康泰旅行社.md "wikilink")[黑人牙膏的廣告歌](../Page/黑人牙膏.md "wikilink")。

正因為馮曦妤初時做幕後工作，而且作風低調，甚少露面及公開表演，所以連其中文姓名也鮮為人知。她於2007年決定由幕後轉至幕前，並以「馮曦妤」作為藝名於2008年加盟[SONY
BMG進軍樂壇](../Page/新力博德曼_\(香港\).md "wikilink")。

轉至幕前之前，她並沒有個人[唱片](../Page/唱片.md "wikilink")，只有《》及《》收錄於雜錦碟《》，另一首由她填詞及主唱的英文作品《Forever
Friends》則收錄於精選專輯《True Colors》。

2008年11月20日，馮曦妤推出首張個人專輯《A Little Love》，除了收錄幾首中文流行曲外，亦收錄了過往的廣告及電影歌曲。

2010年6月10日，推出第二張個人專輯《Sweet Melody》。

## 曾參與作品

  - Shining Friends（電視劇[當四葉草碰上劍尖時主題曲](../Page/當四葉草碰上劍尖時.md "wikilink")）
  - Find Your Love（電視劇[赤沙印記@四葉草.2主題曲](../Page/赤沙印記@四葉草.2.md "wikilink")）
  - True Colors（翻唱）
  - Starfish
  - A Little love

### 派台歌曲成績

| **派台歌曲成績**                                           |
| ---------------------------------------------------- |
| 大碟                                                   |
| **2008年**                                            |
| [A Little Love](../Page/A_Little_Love.md "wikilink") |
| A Little Love                                        |
| **2009年**                                            |
| [Sweet Melody](../Page/Sweet_Melody.md "wikilink")   |
| **2010年**                                            |
| Sweet Melody                                         |
| Sweet Melody                                         |
|                                                      |
| **2012年**                                            |
|                                                      |
| **2014年**                                            |
|                                                      |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

（×）代表由於TVB與包括其所屬新力唱片的五大唱片公司的版稅紛爭而並無派上TVB

（\*）表示仍在榜上

### 電影配樂

| 年份                   | 電影          |
| -------------------- | ----------- |
| <small>2002年</small> | **《史力加》**   |
| <small>2003年</small> | **《史力加2》**  |
| <small>2005年</small> | **《沖出水世界》** |
| <small>2006年</small> | **《荒失失奇兵》** |
| <small>2007年</small> | **《反斗靈貓》**  |

### 廣告配樂

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>歌曲</strong></p></td>
<td><p><strong>廣告</strong></p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>Proud Of You</p></td>
<td><p><a href="../Page/泓景臺.md" title="wikilink">泓景臺樓盤廣告曲</a></p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>Seu am You</p></td>
<td><p>Pentene milky treatment洗髮露廣告歌</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>Shine Of The Century</p></td>
<td><p><a href="../Page/渣甸山名門.md" title="wikilink">渣甸山名門樓盤廣告曲</a></p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>A Wonderful Day</p></td>
<td><p><a href="../Page/美國強生視力護理.md" title="wikilink">1-day Acuvue廣告歌</a></p></td>
</tr>
<tr class="even">
<td><p>More Than Light</p></td>
<td><p><a href="../Page/中華電力.md" title="wikilink">中華電力廣告歌</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Special Crew</p></td>
<td><p><a href="../Page/麥當勞.md" title="wikilink">麥當勞廣告曲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>我在那一角落患過傷風</p></td>
<td><p><a href="../Page/广东步步高电子工业.md" title="wikilink">步步高音樂手機廣告曲</a></p></td>
</tr>
<tr class="odd">
<td><p>Fun For Sharing</p></td>
<td><p><a href="../Page/Canon.md" title="wikilink">Canon</a> SELPHY 相片打印機廣告歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p>MTR Love Story</p></td>
<td><p><a href="../Page/香港鐵路有限公司.md" title="wikilink">港鐵公司廣告曲</a></p></td>
</tr>
<tr class="odd">
<td><p>Forever Friends</p></td>
<td><p>白田天主教小學前英文校歌、<a href="../Page/嵐岸.md" title="wikilink">嵐岸樓盤廣告曲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>U Are My Everything</p></td>
<td><p><a href="../Page/道地.md" title="wikilink">道地綠茶廣告曲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>遙遠的...</p></td>
<td><p><a href="../Page/奇華.md" title="wikilink">奇華嫁囍禮餅廣告歌</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>不老的傳說</p></td>
<td><p><a href="../Page/聖安娜餅屋.md" title="wikilink">聖安娜月餅廣告歌</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>如果·陽光</p></td>
<td><p>檸檬茶廣告曲</p></td>
</tr>
<tr class="even">
<td><p>Just Be Yourself</p></td>
<td><p><a href="../Page/麥當勞.md" title="wikilink">麥當勞廣告曲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Joyful Day</p></td>
<td><p><a href="../Page/Swipe.md" title="wikilink">Swipe清潔用品廣告歌</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Little Johnny</p></td>
<td><p><a href="../Page/Canon.md" title="wikilink">Canon</a> LEGRIA HFM31 全高清攝錄機廣告歌</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Childhood</p></td>
<td><p><a href="../Page/寶潔.md" title="wikilink">寶潔中國</a>20周年廣告歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>世界的...我的</p></td>
<td><p><a href="../Page/交通銀行.md" title="wikilink">交通銀行信用卡廣告歌</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Athena</p></td>
<td><p><a href="../Page/萬利達.md" title="wikilink">萬利達音樂手機廣告歌</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p>Happy Fish</p></td>
<td><p><a href="../Page/海洋公園.md" title="wikilink">海洋公園夢幻水都廣告歌</a></p></td>
</tr>
<tr class="odd">
<td><p>千載不變</p></td>
<td><p><a href="../Page/大家樂.md" title="wikilink">大家樂至愛一哥焗豬扒飯廣告歌</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p>Joyful Day</p></td>
<td><p>佳之選締造更美滿廣告歌</p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p>For You</p></td>
<td><p><a href="../Page/Pizza_Hut.md" title="wikilink">Pizza Hut黃金芝脆扭紋批</a> 瑞士芝心鍋廣告歌曲</p></td>
</tr>
</tbody>
</table>

### 填詞作品

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>歌手</strong></p></td>
<td><p><strong>歌曲</strong></p></td>
<td><p><strong>收錄專輯</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/車婉婉.md" title="wikilink">車婉婉</a></p></td>
<td><p>我未夠秤</p></td>
<td><p><a href="../Page/車婉婉.md" title="wikilink">車婉婉專輯</a>《我未夠秤》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳文媛.md" title="wikilink">陳文媛</a></p></td>
<td><p>慶祝</p></td>
<td><p><a href="../Page/陳文媛.md" title="wikilink">陳文媛專輯</a>《Bounce》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/E-kids.md" title="wikilink">E-kids</a></p></td>
<td><p>開始戀愛</p></td>
<td><p><a href="../Page/E-kids.md" title="wikilink">E-kids專輯</a>《E-Kids Diary》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/E-kids.md" title="wikilink">E-kids</a></p></td>
<td><p>倉鼠俱樂部</p></td>
<td><p><a href="../Page/E-kids.md" title="wikilink">E-kids專輯</a>《Smile》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a></p></td>
<td><p>隨時候命</p></td>
<td><p><a href="../Page/群星.md" title="wikilink">群星專輯</a>《男人魅》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Twins.md" title="wikilink">Twins</a></p></td>
<td><p>救生圈<br />
狂想曲</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins專輯</a>《Samba!》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins</a></p></td>
<td><p>尋寶</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins專輯</a>《一時無兩》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a></p></td>
<td><p>JS精彩感覺<br />
感官世界</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周麗淇.md" title="wikilink">周麗淇</a></p></td>
<td><p>問號先生</p></td>
<td><p><a href="../Page/周麗淇.md" title="wikilink">周麗淇專輯</a>《Nikikaka》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>馮曦妤</p></td>
<td><p>Forever Friends</p></td>
<td><p>馮曦妤專輯《A Little Love》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td><p>認命</p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇專輯</a>《First Experience》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td><p>永久保存</p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇專輯</a>《First Experience》<small>（永久保存珍藏版）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪</a></p></td>
<td><p>六天</p></td>
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪專輯</a>《Beginning》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td><p>I Miss You<br />
I Miss You<small>（國）</small></p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇專輯</a>《Change》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪</a></p></td>
<td><p>傻小子</p></td>
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪專輯</a>《Continue》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>馮曦妤</p></td>
<td><p>陽光．雨<br />
遙遠的…<br />
火星原居民<br />
隻眼閉<br />
小傷口<br />
在你身邊<br />
一點點的回憶<br />
A Little Love<br />
避風港<br />
不做你的情人</p></td>
<td><p>馮曦妤專輯《A Little Love》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>馮曦妤</p></td>
<td><p>幸運兒</p></td>
<td><p>馮曦妤專輯《A Little Love》</p></td>
<td><p>電視劇《<a href="../Page/師父·明白了.md" title="wikilink">師父·明白了</a>》插曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪</a></p></td>
<td><p>Lovin' You<br />
夠鐘</p></td>
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪專輯</a>《Follow》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>馮曦妤featuring <a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td><p>U Are My Everything</p></td>
<td></td>
<td><p>Anders Lee、馮曦妤合填</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td><p>|Imaginary Love</p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇專輯</a>《Can't Be Half》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a></p></td>
<td><p>|幾多</p></td>
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健專輯</a>《Friends For Life》<small>（新曲+精選）</small></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張學友.md" title="wikilink">張學友</a></p></td>
<td><p>|尋福</p></td>
<td></td>
<td><p>許介濱、馮曦妤合填</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪</a></p></td>
<td><p>最好不過</p></td>
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪專輯</a>《Remembrance》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>馮曦妤</p></td>
<td><p>細細個<br />
愛情機器<br />
逃不掉<br />
天生不對<br />
我們的隊友</p></td>
<td><p>馮曦妤專輯《Sweet Melody》</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/舒淇.md" title="wikilink">舒淇</a></p></td>
<td><p>卡門<br />
再見<br />
快樂吧</p></td>
<td><p>《<a href="../Page/精武風雲·陳真.md" title="wikilink">精武風雲．陳真</a>》電影原聲帶</p></td>
<td><p>《<a href="../Page/精武風雲·陳真.md" title="wikilink">精武風雲．陳真</a>》電影主題曲、插曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/張學友.md" title="wikilink">張學友</a></p></td>
<td><p>無聲的吉他</p></td>
<td></td>
<td><p>《<a href="../Page/不再讓你孤單.md" title="wikilink">不再讓你孤單</a>》電影插曲</p></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/陳偉霆.md" title="wikilink">陳偉霆</a> Feat. <a href="../Page/泳兒.md" title="wikilink">泳兒</a></p></td>
<td><p>我是誰</p></td>
<td></td>
<td><p><a href="../Page/林日曦.md" title="wikilink">林日曦</a>、馮曦妤合填</p></td>
</tr>
<tr class="odd">
<td><p>馮曦妤</p></td>
<td><p>每天醒來第一件會做的事</p></td>
<td></td>
<td><p>國語填詞作品，兼親自作曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>馮曦妤</p></td>
<td><p>For You</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td><p>走到尾</p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇專輯</a>《Escape》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><a href="../Page/周柏豪.md" title="wikilink">周柏豪</a></p></td>
<td><p>不可能</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/許懷欣.md" title="wikilink">許懷欣</a> Feat. Pollie@小塵埃</p></td>
<td><p>Hear Me</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p><a href="../Page/陳綺貞.md" title="wikilink">陳綺貞</a></p></td>
<td><p>討厭喜歡你</p></td>
<td></td>
<td><p>《<a href="../Page/喜欢·你.md" title="wikilink">喜歡．你</a>》電影片尾曲</p></td>
</tr>
<tr class="odd">
<td><p>朱雋言</p></td>
<td><p>住在地球曬夕陽</p></td>
<td></td>
<td><p>《<a href="../Page/喜欢·你.md" title="wikilink">喜歡．你</a>》電影片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>E-KIDS</p></td>
<td><p>終止戀愛</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a>、<a href="../Page/張敬軒.md" title="wikilink">張敬軒</a></p></td>
<td><p>Make a Beat</p></td>
<td></td>
<td><p>陳耀森、馮曦妤、朱雋言合填</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a>、<a href="../Page/陳小春.md" title="wikilink">陳小春</a>、<a href="../Page/謝天華.md" title="wikilink">謝天華</a>、<a href="../Page/錢嘉樂.md" title="wikilink">錢嘉樂</a>、<a href="../Page/林曉峰.md" title="wikilink">林曉峰</a></p></td>
<td><p>一起衝一起闖</p></td>
<td></td>
<td><p>馮曦妤、陳光榮、朱雋言合填</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃劍文.md" title="wikilink">黃劍文</a></p></td>
<td><p>I Think It's Magic</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 和音作品

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>歌手</strong></p></td>
<td><p><strong>歌曲</strong></p></td>
<td><p><strong>收錄專輯</strong></p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p><a href="../Page/車婉婉.md" title="wikilink">車婉婉</a></p></td>
<td><p>彩色世界<br />
欠你一吻</p></td>
<td><p><a href="../Page/車婉婉.md" title="wikilink">車婉婉專輯</a>《Simply》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a></p></td>
<td><p>太陽出來了</p></td>
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健專輯</a>《Myself》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/陳文媛.md" title="wikilink">陳文媛</a></p></td>
<td><p>慶祝</p></td>
<td><p><a href="../Page/陳文媛.md" title="wikilink">陳文媛專輯</a>《Bounce》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/E-kids.md" title="wikilink">E-kids</a></p></td>
<td><p>開始戀愛</p></td>
<td><p><a href="../Page/E-kids.md" title="wikilink">E-kids專輯</a>《E-Kids Diary》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p><a href="../Page/成龍.md" title="wikilink">成　龍</a>、<a href="../Page/Twins.md" title="wikilink">Twins</a></p></td>
<td><p>變變變</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins專輯</a>《Touch Of Love》<small>（第二版）</small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郭富城.md" title="wikilink">郭富城</a></p></td>
<td><p>水手裝與機關槍</p></td>
<td><p><a href="../Page/郭富城.md" title="wikilink">郭富城專輯</a>《In The Still Of The Night》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳</a></p></td>
<td><p>嫁妝</p></td>
<td><p><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳專輯</a>《Red》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳</a></p></td>
<td><p>閣樓</p></td>
<td><p><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳專輯</a>《Stylish Index》</p></td>
</tr>
<tr class="even">
<td><p>Love Paradise</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>前所未見</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>紅絲帶</p></td>
<td><p><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳專輯</a>《Grace And Charm》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/方力申.md" title="wikilink">方力申</a></p></td>
<td><p>零藉口</p></td>
<td><p>小說配樂專輯《只能談情 不能說愛》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古巨基.md" title="wikilink">古巨基</a></p></td>
<td><p>借一秒</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林嘉欣.md" title="wikilink">林嘉欣</a></p></td>
<td><p>得人驚</p></td>
<td><p>合輯《愛唱主題曲》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a></p></td>
<td><p>內傷</p></td>
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健專輯</a>《Discover》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins</a></p></td>
<td><p>救生圈<br />
狂想曲<br />
一點一滴</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins專輯</a>《Samba!》</p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins</a></p></td>
<td><p>幼稚園<br />
尋寶</p></td>
<td><p><a href="../Page/Twins.md" title="wikilink">Twins專輯</a>《一時無兩》</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/周麗淇.md" title="wikilink">周麗淇</a></p></td>
<td><p>問號先生</p></td>
<td><p><a href="../Page/周麗淇.md" title="wikilink">周麗淇專輯</a>《Nikikaka》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>酸甜</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>問號先生 Remix - "太陽伯伯，你去咗邊？"</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a></p></td>
<td><p>陪我長大</p></td>
<td><p><a href="../Page/容祖兒.md" title="wikilink">容祖兒專輯</a>《Glow》</p></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/容祖兒.md" title="wikilink">容祖兒</a></p></td>
<td><p>陪我長大<small>（國）</small></p></td>
<td><p><a href="../Page/容祖兒.md" title="wikilink">容祖兒專輯</a>《Love Joey Love Four》</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇</a></p></td>
<td><p>I Miss You<br />
I Miss You<small>（國）</small><br />
I Miss You <small>（Late Night Version）</small></p></td>
<td><p><a href="../Page/陳柏宇.md" title="wikilink">陳柏宇專輯</a>《Change》</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 唱片

  - 《[A Little
    Love](../Page/A_Little_Love.md "wikilink")》（新曲+Demo）出版日期：2008年11月20日

<!-- end list -->

1.  The Glorious Death
2.  陽光·雨
3.  More Than Light
4.  遙遠的...
5.  A Wonderful Day
6.  火星原居民
7.  隻眼閉
8.  Hidden Romance
9.  幸運兒
10. Fun For Sharing
11. Proud Of You
12. 我在那一角落患過傷風
13. Shining Friends
14. 再見---警察
15. Forever Friends
16. Shine Of The Century
17. 小傷口
18. 在你身邊
19. 救生圈
20. 一點點的回憶
21. Flow
22. I Love Sunshine
23. A Little Love
24. Free
25. 避風港
26. Special Crew
27. 不做你的情人

<!-- end list -->

  - 《[Sweet
    Melody](../Page/Sweet_Melody.md "wikilink")》（新曲+Demo）出版日期：2010年6月10日

<!-- end list -->

1.  Prelude
2.  如果...陽光
3.  Just Be Yourself
4.  Joyful Day
5.  細細個
6.  Childhood
7.  遠視
8.  Happy Time
9.  我在夢中見過你
10. U Are My Everything
11. 幸せ笑顔
12. Little Johnny
13. 愛情機器
14. 逃不掉
15. 世界的...我的
16. 天生不對
17. 我們的隊友
18. Regret
19. You Are My Everything
20. Athena

## 其他演出

### 電視劇集

  - 2008年7月12日：[無綫電視單元劇](../Page/無綫電視.md "wikilink")《[盛裝舞步愛作戰](../Page/盛裝舞步愛作戰.md "wikilink")》第5集
    飾 售貨員（客串）

### 電視節目

  - 2009年9月10日：無綫電視「[勁歌金曲](../Page/勁歌金曲.md "wikilink")」
  - 2009年12月24日：無綫電視「勁歌金曲」
  - 2010年5月23日：[香港電台](../Page/香港電台.md "wikilink")「C+音樂 第5集」
  - 2010年6月6日：[亞洲電視](../Page/亞洲電視.md "wikilink")「[亞洲星光大道](../Page/亞洲星光大道.md "wikilink")」
  - 2010年6月15日：亞洲電視「時尚生活Guide」
  - 2010年6月30日：香港有線電視「攻星計」
  - 2010年7月4日：亞洲電視「[娛樂紅人館](../Page/娛樂紅人館.md "wikilink")」
  - 2010年7月23-24日：亞洲電視「今夜星光燦爛」
  - 2010年8月15日：亞洲電視「亞洲星光大道」
  - 2010年11月1-4日：[香港有線電視](../Page/香港有線電視.md "wikilink")「大流行」
  - 2010年11月12-13日：亞洲電視「今夜星光燦爛」
  - 2010年12月26日：亞洲電視「亞洲星光大道」

### 活動及傳媒專訪

  - 2004年7月31日：[九龍灣國際展貿中心展貿廳](../Page/九龍灣國際展貿中心.md "wikilink")「小春x伊健好兄弟音樂會」表現嘉賓
  - 2009年7月12日：[葵涌](../Page/葵涌.md "wikilink")[新都會廣場](../Page/新都會廣場.md "wikilink")「北海道‧星級盛會」表現嘉賓
  - 2010年3月4-6日：[紅磡體育館](../Page/紅磡體育館.md "wikilink")「鄭伊健Beautiful Day
    2011演唱會」表演嘉賓
  - 2010年5月29日：「[第十五屆十大電視廣告頒獎典禮](../Page/第十五屆十大電視廣告頒獎典禮.md "wikilink")」領獎暨表演嘉賓
  - 2010年6月11日：九龍灣國際展貿中心展貿廳「Fiona Fung Live 2010」
  - 2010年8月14日：九龍灣國際展貿中心展貿廳「陳柏宇x馮曦妤mini live音樂會」
  - 2010年11月19-20日：[香港藝術中心壽臣劇院](../Page/香港藝術中心.md "wikilink")「Sweet
    Sweet Melodies Live」首個個人音樂會
  - 2010年12月17日：[香港展覽中心](../Page/香港展覽中心.md "wikilink")「[Yahoo\!搜尋人氣大獎2010](../Page/2010年度YAHOO!搜尋人氣大獎得獎名單.md "wikilink")」頒獎典禮
  - 2011年7月21日：[九龍香格里拉酒店B](../Page/九龍香格里拉酒店.md "wikilink")1粉嶺廳「[2011
    TVB最受歡迎廣告頒獎典禮](../Page/TVB最受歡迎電視廣告大獎.md "wikilink")」
  - 2012年6月16日：「Scania 2012年駕駛挑戰賽頒獎禮」表演嘉賓

### 電台訪問

  - 2008年4月2日：接受[香港商業電台](../Page/香港商業電台.md "wikilink")[叱咤903節目](../Page/叱咤903.md "wikilink")《熱鬧樂壇》訪問
  - 2008年5月8日：接受[香港電台節目](../Page/香港電台.md "wikilink")《Gimme 5》訪問
  - 2010年3月28日：接受香港電台節目《有趣星星星》訪問
  - 2010年7月7日：接受香港電台節目《Gimme 5》訪問
  - 2012年6月26日：接受叱咤903節目《叱咤樂壇》訪問
  - 2014年1月7日：接受叱咤903節目《叱咤樂壇》訪問
  - 2014年1月16日：接受香港電台節目《Made in Hong Kong 李志剛》訪問

## 獲得獎項

  - 2008年
      - [兒歌金曲頒獎典禮](../Page/兒歌金曲頒獎典禮.md "wikilink")：十大兒歌金曲《陽光·雨》
      - [新城勁爆兒歌頒獎禮](../Page/新城勁爆兒歌頒獎禮.md "wikilink")：新城勁爆兒歌《陽光·雨》
      - [十大勁歌金曲頒獎典禮](../Page/2008年度十大勁歌金曲得獎名單.md "wikilink")：最受歡迎女新人－銅獎
      - [Yahoo\!搜尋人氣大獎](../Page/Yahoo!搜尋人氣大獎2008.md "wikilink")：樂壇新勢力（女歌手）

<!-- end list -->

  - 2009年
      - [第六屆勁歌王年度總選頒獎典禮](../Page/第六屆勁歌王年度總選頒獎典禮.md "wikilink")：最有前途新人獎

<!-- end list -->

  - 2010年
      - [第十五屆十大電視廣告頒獎典禮](../Page/第十五屆十大電視廣告頒獎典禮.md "wikilink")：最佳電視廣告歌曲大獎《如果…陽光》
      - [第七屆勁歌王年度總選頒獎典禮](../Page/第七屆勁歌王年度總選頒獎典禮.md "wikilink")：最受歡迎廣告歌《如果…陽光》
      - [Yahoo\!搜尋人氣大獎](../Page/2010年度YAHOO!搜尋人氣大獎得獎名單.md "wikilink")：Yahoo\!搜尋人氣廣告歌《如果…陽光》

<!-- end list -->

  - 2011年
      - [香港數碼音樂獎2010](../Page/香港數碼音樂獎2010.md "wikilink")：最高次數下載MV《如果…陽光》
      - 香港數碼音樂獎2010：最高次數接駁鈴聲歌曲《如果…陽光》
      - 香港數碼音樂獎2010：最高次數原音鈴聲歌曲《如果…陽光》
      - [TVB最受歡迎電視廣告大獎](../Page/TVB最受歡迎電視廣告大獎.md "wikilink")
        最受歡迎電視廣告歌曲《如果…陽光》

## 參考資料

<references/>

## 外部連結

  -
  -
  -
  -
  -
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:香港填詞人](../Category/香港填詞人.md "wikilink")
[Category:香港創作歌手](../Category/香港創作歌手.md "wikilink")
[X曦](../Category/馮姓.md "wikilink")
[Category:索尼音樂娛樂旗下藝人](../Category/索尼音樂娛樂旗下藝人.md "wikilink")
[Category:五旬節林漢光中學校友](../Category/五旬節林漢光中學校友.md "wikilink")