**六合县**，[中国旧](../Page/中国.md "wikilink")[县名](../Page/县.md "wikilink")。在今[江苏省](../Page/江苏省.md "wikilink")[南京市境](../Page/南京市.md "wikilink")。

六合历史悠久，根据考古和地质工作者及专家发掘、考证，在距今约一万年前就有原始氏族村落。六合古称棠邑。[秦始皇二十一年](../Page/秦始皇.md "wikilink")（前221年），始置[棠邑县](../Page/棠邑县.md "wikilink")。[隋朝废](../Page/隋朝.md "wikilink")[北周时所置](../Page/北周.md "wikilink")[六合郡](../Page/六合郡.md "wikilink")，[开皇四年](../Page/开皇.md "wikilink")（584年），废[尉氏县](../Page/尉氏县_\(东晋\).md "wikilink")、[堂邑县](../Page/堂邑县_\(西汉\).md "wikilink")、[方山县](../Page/方山县_\(北周\).md "wikilink")，合为**六合县**。唐朝时，属[扬州](../Page/扬州_\(古代\).md "wikilink")、[廣陵郡](../Page/廣陵郡.md "wikilink")。明代之前多属[扬州](../Page/扬州_\(古代\).md "wikilink")，洪武二十三年（1389年）始属[应天府](../Page/应天府_\(明朝\).md "wikilink")（治所在今[南京市](../Page/南京市.md "wikilink")）。

1949年1月25日，[中华民国政府六合县县长周君轸逃往南京](../Page/中華民國政府.md "wikilink")。26日，中共[江淮军区第一军分区司令员](../Page/江淮军区.md "wikilink")[艾明山等人率部进入六合县城](../Page/艾明山.md "wikilink")。同日，中共六合县委、县政府迁进县城办公。27日，成立六合县军事管制委员会。[中国共产党政权全面接管六合县](../Page/中国共产党政权.md "wikilink")\[1\]。

1966年3月15日，六合县、[江浦县](../Page/江浦县.md "wikilink")、[仪徵县](../Page/仪徵县.md "wikilink")、[金湖县](../Page/金湖县.md "wikilink")、[盱眙县五县成立](../Page/盱眙县.md "wikilink")[六合专区](../Page/六合专区.md "wikilink")，专区所在地六合县[六城镇](../Page/六城镇.md "wikilink")。1975年11月，六合属南京市管辖。1973年9月，原**六合县**[卸甲甸等地析出成立](../Page/卸甲甸.md "wikilink")[大厂区](../Page/大厂区.md "wikilink")，2002年4月，经国务院批准，**六合县**与[大厂区合并成立南京市](../Page/大厂区.md "wikilink")[六合区](../Page/六合区.md "wikilink")，全区辖8个街道、9个镇。

## 历史沿革

  - 六合古称棠邑，春秋战国时，棠邑先属楚，后属吴，再属越，至公元前334年复归于楚。
  - 隋[开皇四年](../Page/开皇.md "wikilink")（584年）定名六合。
  - 南唐时期六合县属[江宁府](../Page/江宁府.md "wikilink")（今[南京](../Page/南京.md "wikilink")），保大六年（948年）改六合为雄州。
  - 元朝，六合县属扬州。
  - 明洪武二十三年（1389年）复属应天府（今[南京](../Page/南京.md "wikilink")），直至民国初年。
  - 民国元年（1912年）废府，**六合县**属江苏省。
  - 1914年属江苏省金陵道。
  - 1949年属[皖北行署](../Page/皖北行署区.md "wikilink")[滁县专区](../Page/滁县专区.md "wikilink")，后改属[苏北行署区](../Page/苏北行署区.md "wikilink")[泰州专区](../Page/泰州专区.md "wikilink")。
  - 1953年属扬州。1956年属镇江。
  - 1957年属扬州。1958年属南京市。1962年属扬州。
  - 1966年3月15日，六合县、[江浦县](../Page/江浦县.md "wikilink")、[仪徵县](../Page/仪徵县.md "wikilink")、[金湖县](../Page/金湖县.md "wikilink")、[盱眙县五县成立](../Page/盱眙县.md "wikilink")**[六合专区](../Page/六合专区.md "wikilink")**，专区所在地六合县[六城镇](../Page/六城镇.md "wikilink")。
  - 1971年六合专区撤销，划属[扬州地区](../Page/扬州地区.md "wikilink")。
  - 1975年11月，六合重新划回南京市管辖。
  - 1973年9月，原六合县[卸甲甸等地析出成立](../Page/卸甲甸.md "wikilink")[大厂区](../Page/大厂区.md "wikilink")
  - 2002年4月，经[中国国务院批准](../Page/中华人民共和国国务院.md "wikilink")，六合县与大厂区合并成立南京市六合区，全区辖10个街道、9个镇。
  - 2012年8月六合区划进行调整，调整后的六合区辖10个街道、2个镇：雄州街道、金牛湖街道、横梁街道、程桥街道、大厂街道、葛塘街道、长芦街道、马鞍街道、龙袍街道、龙池街道；竹镇镇、冶山镇。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:隋朝县份](../Category/隋朝县份.md "wikilink")
[Category:唐朝县份](../Category/唐朝县份.md "wikilink")
[Category:南唐县份](../Category/南唐县份.md "wikilink")
[Category:宋朝县份](../Category/宋朝县份.md "wikilink")
[Category:元朝县份](../Category/元朝县份.md "wikilink")
[Category:明朝县份](../Category/明朝县份.md "wikilink")
[Category:清朝县份](../Category/清朝县份.md "wikilink")
[Category:中华民国江苏省县份](../Category/中华民国江苏省县份.md "wikilink")
[Category:已撤消的中华人民共和国江苏省县份](../Category/已撤消的中华人民共和国江苏省县份.md "wikilink")
[Category:南京行政区划史](../Category/南京行政区划史.md "wikilink")
[Category:584年建立的行政区划](../Category/584年建立的行政区划.md "wikilink")
[Category:2002年废除的行政区划](../Category/2002年废除的行政区划.md "wikilink")

1.