[Audi_Q7_offroad_style_S_line_3.0_TDI_quattro_tiptronic_Phantomschwarz_Interieur.JPG](https://zh.wikipedia.org/wiki/File:Audi_Q7_offroad_style_S_line_3.0_TDI_quattro_tiptronic_Phantomschwarz_Interieur.JPG "fig:Audi_Q7_offroad_style_S_line_3.0_TDI_quattro_tiptronic_Phantomschwarz_Interieur.JPG")
**奥迪Q7** (**Audi
Q7**）是一輛由[德國](../Page/德國.md "wikilink")[奥迪設計製造的豪华](../Page/奥迪.md "wikilink")[跨界休旅車](../Page/跨界休旅車.md "wikilink")，它的主要竞争对手包括[梅赛德斯-奔驰M级](../Page/梅赛德斯-奔驰M级.md "wikilink")、[BMW
X5](../Page/BMW_X5.md "wikilink")、[雷克萨斯RX及](../Page/雷克萨斯RX.md "wikilink")[富豪XC90](../Page/富豪XC90.md "wikilink")
。這款車型的設計與構造和同集團的[福斯Touraeg](../Page/大众途锐.md "wikilink")、[保時捷Cayenne系出同門](../Page/保時捷Cayenne.md "wikilink")。

引擎

3.0 V6 TDI

4.2 V8

Le Mans V12

[Category:2006年面世的汽車](../Category/2006年面世的汽車.md "wikilink")
[Category:奧迪車輛](../Category/奧迪車輛.md "wikilink")
[Category:跨界休旅車](../Category/跨界休旅車.md "wikilink")
[Category:运动型多用途车](../Category/运动型多用途车.md "wikilink")
[Category:前置引擎](../Category/前置引擎.md "wikilink")
[Category:前輪驅動](../Category/前輪驅動.md "wikilink")
[Category:四輪驅動](../Category/四輪驅動.md "wikilink")