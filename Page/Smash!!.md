**Smash\!\!**是來自[俄羅斯的男子雙人歌唱團體](../Page/俄羅斯.md "wikilink")，團員為黑髮的[賽奇·拉查列夫和金髮的](../Page/賽奇·拉查列夫.md "wikilink")[法蘭·特帕洛夫](../Page/法蘭·特帕洛夫.md "wikilink")，他們因演唱一曲{{〈}}Belle{{〉}}而竄紅，被簽入[環球唱片公司旗下](../Page/環球唱片.md "wikilink")。但這個團體在他們發行第二張專輯後就因為[賽奇·拉查列夫的離團而瀕臨瓦解](../Page/賽奇·拉查列夫.md "wikilink")。2005年[法蘭·特帕洛夫獨撐大局](../Page/法蘭·特帕洛夫.md "wikilink")，以Smash\!\!的名字發行了第三張專輯《Evolution》，之後[法蘭·特帕洛夫也開始以自己的名字發行歌曲專輯](../Page/法蘭·特帕洛夫.md "wikilink")，Smash\!\!正式解散。

他們主要以[英語歌唱](../Page/英語.md "wikilink")，唱片主要銷售到[東歐跟](../Page/東歐.md "wikilink")[東南亞](../Page/東南亞.md "wikilink")，他們在[俄羅斯與](../Page/俄羅斯.md "wikilink")[東南亞很受到青少年的歡迎](../Page/東南亞.md "wikilink")。

## 歷年專輯

  - 2003 Freeway
  - 2004 2Nite
  - 2005 Evolution

## 外部連結

  - \[<http://www.last.fm/music/Smash>\!\! Last.fm Page\]

[Category:俄罗斯男歌手](../Category/俄罗斯男歌手.md "wikilink")