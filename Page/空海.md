[Huushincho_1.jpg](https://zh.wikipedia.org/wiki/File:Huushincho_1.jpg "fig:Huushincho_1.jpg")
**空海**（；），俗名佐伯真魚，於唐朝為日本派遣僧，師學於今西安青龍寺，惠果阿闍黎門下，受獲傳承付法第一人，賜受法號**遍照金剛**，謚號**弘法大師**。

日本佛教僧侶，至中國學習唐密，傳承[金剛界與](../Page/金剛界.md "wikilink")[胎藏界二部](../Page/胎藏界.md "wikilink")[純密](../Page/純密.md "wikilink")，為[唐密第八祖](../Page/唐密.md "wikilink")，日本[佛教](../Page/佛教.md "wikilink")[真言宗的開山祖師](../Page/真言宗.md "wikilink")。也因其书法功底强，而被称为“五笔和尚”，與[嵯峨天皇](../Page/嵯峨天皇.md "wikilink")、[橘逸勢合稱平安時代](../Page/橘逸勢.md "wikilink")[三筆](../Page/三筆.md "wikilink")，著名作品為《[風信帖](../Page/風信帖.md "wikilink")》。

## 生平

[日本](../Page/日本.md "wikilink")[寶龜](../Page/寶龜.md "wikilink")5年[陰曆](../Page/陰曆.md "wikilink")[六月十五](../Page/六月十五.md "wikilink")（774年7月27日）出生於[讚岐國](../Page/讚岐國.md "wikilink")[多度郡屏風浦](../Page/多度郡.md "wikilink")（今[香川縣](../Page/香川縣.md "wikilink")[善通寺市](../Page/善通寺市.md "wikilink")），父亲是[豪族](../Page/豪族.md "wikilink")，母亲[阿刀氏是](../Page/阿刀氏.md "wikilink")[渡来人後人](../Page/渡来人.md "wikilink")。[延曆](../Page/延曆.md "wikilink")23年（804年），渡海至[大唐國](../Page/大唐國.md "wikilink")，於[长安](../Page/长安.md "wikilink")[青龙寺向](../Page/青龙寺_\(西安市\).md "wikilink")[惠果阿闍黎請法](../Page/惠果.md "wikilink")，学习[真言密教](../Page/唐密.md "wikilink")，後返日本開宗，為[真言宗](../Page/真言宗.md "wikilink")。」空海將密宗區分為「純密」與「雜密」，見其所著「三學錄」。

[弘仁](../Page/弘仁.md "wikilink")14年（823年），獲賜[東寺](../Page/東寺.md "wikilink")，作為真言密教之道場。之後，相對於稱作[台密的](../Page/台密.md "wikilink")[天台宗密教](../Page/天台宗.md "wikilink")，東寺的密教稱作[東密](../Page/東密.md "wikilink")。

傳說空海大師參考梵文將[日文字母以](../Page/日文.md "wikilink")[平假名排序成陣](../Page/平假名.md "wikilink")。此外他亦是有名的[書法家](../Page/書法.md "wikilink")，與[嵯峨天皇](../Page/嵯峨天皇.md "wikilink")、[橘逸勢共稱](../Page/橘逸勢.md "wikilink")[三筆](../Page/三筆.md "wikilink")。著有《[文镜秘府论](../Page/文镜秘府论.md "wikilink")》。

[承和](../Page/承和_\(日本\).md "wikilink")2年[三月廿一](../Page/三月廿一.md "wikilink")（835年4月22日）卒於[高野山](../Page/高野山.md "wikilink")。[諡號](../Page/諡號.md "wikilink")「**弘法大師**（）」。

## 判教

空海在对显宗、密宗二者判教时形成了其自己一套说法，其中最为重要的就是“十住心”说。这一学说可以以下表概括\[1\]：

| 学说名   | 住心    | 道      | 乘    | 显密 |
| ----- | ----- | ------ | ---- | -- |
| 十住心   | 异生羝羊心 | 诸趣外道   | 世间三心 | 九显 |
| 愚童持斋心 | 人趣    |        |      |    |
| 愚童无畏心 | 天趣    |        |      |    |
| 唯蕴无我心 | 声闻    | 小乘     |      |    |
| 拔业因种心 | 缘觉    |        |      |    |
| 他缘大乘心 | 唯识    | 大乘四心   |      |    |
| 觉心不生心 | 三论    |        |      |    |
| 一道无为心 | 天台    |        |      |    |
| 极无自性心 | 华严    |        |      |    |
| 秘密庄严心 | 真言    | 无上秘密乘心 | 一密   |    |

十住心

## 各地影響

在[日本](../Page/日本.md "wikilink")，「大師」一詞多專指空海。[日語中就有一句](../Page/日語.md "wikilink")[格言](../Page/格言.md "wikilink")：「弘法奪『大師』之名，[秀吉奪](../Page/豐臣秀吉.md "wikilink")『[太閤](../Page/太閤.md "wikilink")』之名，[玄奘奪](../Page/玄奘.md "wikilink")『[三藏](../Page/三藏.md "wikilink")』之名」（）。

在[台灣](../Page/台灣.md "wikilink")，因[臺灣日治時期的影響](../Page/臺灣日治時期.md "wikilink")，有祭祀弘法大師的寺廟，在[台北市的](../Page/台北市.md "wikilink")[西門町媽祖廟奉有空海大師法相](../Page/西門町媽祖廟.md "wikilink")，以紀念[真言宗建廟之德](../Page/真言宗.md "wikilink")。在北投丹鳳山上亦有紀念弘法大師之石碑。[花蓮縣](../Page/花蓮縣.md "wikilink")[吉安鄉則有](../Page/吉安鄉.md "wikilink")[慶修院](../Page/慶修院.md "wikilink")，[台南市則有](../Page/台南市.md "wikilink")[開元寺奉有弘法大師法相](../Page/開元寺.md "wikilink")。

在[四國地區](../Page/四國.md "wikilink")，有88所與空海有淵源的寺廟被通稱為[四國八十八箇所](../Page/四國八十八箇所.md "wikilink")，日後也成為日本佛教徒的朝聖地點。

## 主要作品

  - [三教指歸](../Page/三教指歸.md "wikilink")
  - [十住心論](../Page/十住心論.md "wikilink")
  - [風信帖](../Page/風信帖.md "wikilink")
  - [文鏡秘府論](../Page/文鏡秘府論.md "wikilink")

## 相關小說

  - 空海之風景（1976年 [司馬遼太郎](../Page/司馬遼太郎.md "wikilink")）
  - 曼陀羅之人―空海求法傳（1984年 [陳舜臣](../Page/陳舜臣.md "wikilink")）
  - 空海（2005年 [三田誠廣](../Page/三田誠廣.md "wikilink")）

<!-- end list -->

  - 四國遍路的近現代―「現代遍路」開始至「療癒之旅」（2005年 森正人）
  - 沙門空海（[夢枕獏](../Page/夢枕獏.md "wikilink")）

## 電影

  - 空海（1984年[東映](../Page/東映.md "wikilink")
    執導：[佐藤純彌](../Page/佐藤純彌.md "wikilink")
    劇本：[早坂曉](../Page/早坂曉.md "wikilink")）
  - 曼荼羅／若き日の弘法大師・空海（1991年[東宝東和](../Page/東宝東和.md "wikilink")
    監督：[テン・ウェンジャ](../Page/テン・ウェンジャ.md "wikilink")
    音樂：喜多嶋修）
  - [妖猫传](../Page/妖猫传.md "wikilink")（2017年）

## 動漫

  - [宇野比呂士的漫畫](../Page/宇野比呂士.md "wikilink")\<<秘石戰記>\>中，將空海塑造為法力高強的法師

<!-- end list -->

  - SFC遊戲\<<伊忍道>\>中，高等級的僧侶角色

<!-- end list -->

  - LoveLive\! SunShine\!\! 動畫第五話，有稍微提及到 弘法大師 空海(位於5:09處)

<!-- end list -->

  - 在動畫\<<鬼神童子>\>中，主角一行人受黃金龍指引回到過去，時間軸約在日本的平安時代，一行人在路上遇到一位僧侶自稱"貧僧法號空海"

<!-- end list -->

  - 在動畫《魔道書7使者》中，於“暴食”書庫中以真言術始祖“佐伯真魚”為名。

## 相關條目

  - [四國八十八箇所](../Page/四國八十八箇所.md "wikilink")
  - [西門町媽祖廟](../Page/西門町媽祖廟.md "wikilink")
  - [台南開元寺](../Page/台南開元寺.md "wikilink")
  - [慶修院](../Page/慶修院.md "wikilink")
  - [風信帖](../Page/風信帖.md "wikilink")

## 外部連結

  - [「空海の風景」1/7司馬遼太郎
    "街道を行く"](http://www.youtube.com/watch?v=8SxY4hShxUc)
  - [空海精神](http://www.mikkyo21f.gr.jp/)
  - [弘法大師空海與高野山之旅](http://kobodaishi.xrea.jp/)
  - [弘法大師 : 般若心經秘鍵](http://image.bukon.idv.tw/pic1/pic1_2.htm)
  - [秘藏記](http://base1.nijl.ac.jp/iview/Frame.jsp?DB_ID=G0003917KTM&C_CODE=XSE1-02503&IMG_SIZE=&PROC_TYPE=null&SHOMEI=%E3%80%90%E7%A7%98%E8%94%B5%E8%A8%98%E3%80%91&REQUEST_MARK=null&OWNER=null&IMG_NO=1)

## 参考文献

[Category:平安時代僧人](../Category/平安時代僧人.md "wikilink")
[Category:讚岐國出身人物](../Category/讚岐國出身人物.md "wikilink")
[Category:774年出生](../Category/774年出生.md "wikilink")
[Category:835年逝世](../Category/835年逝世.md "wikilink")
[Category:遣唐使](../Category/遣唐使.md "wikilink")
[Category:日本書法家](../Category/日本書法家.md "wikilink")
[Category:日本思想家](../Category/日本思想家.md "wikilink")
[Category:字词典编纂者](../Category/字词典编纂者.md "wikilink")
[Category:佐伯氏](../Category/佐伯氏.md "wikilink")

1.