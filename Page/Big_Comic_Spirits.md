《**Big Comic
Spirits**》（）是[日本出版社](../Page/日本.md "wikilink")[小學館所推出的](../Page/小學館.md "wikilink")[漫畫週刊雜誌](../Page/漫畫.md "wikilink")，鎖定的閱讀對象為年齡層較高的青年讀者群。

1980年10月創刊，創刊當初是月刊（每月14日發售），1981年6月開始轉為雙週刊，1986年4月起成為週刊（每週一發售）。該雜誌連載的著名作品，有[浦澤直樹的](../Page/浦澤直樹.md "wikilink")《[20世紀少年](../Page/20世紀少年.md "wikilink")》等。

## 連載

### 通常連載

  - [気まぐれコンセプト](../Page/気まぐれコンセプト.md "wikilink")（[ホイチョイ・プロダクションズ](../Page/ホイチョイ・プロダクションズ.md "wikilink")）1981年14号
    -
  - [美味大挑戰](../Page/美味大挑戰.md "wikilink")（原作：[雁屋哲](../Page/雁屋哲.md "wikilink")、作画：[花咲アキラ](../Page/花咲アキラ.md "wikilink")）1983年20号
    -
  - [じみへん](../Page/じみへん.md "wikilink")（[中崎タツヤ](../Page/中崎タツヤ.md "wikilink")）1989年21号
    -
  - [团地友夫](../Page/团地友夫.md "wikilink")（[小田扉](../Page/小田扉.md "wikilink")）2003年34号
    -
  - [最後一局](../Page/最後一局.md "wikilink")（原作：[神尾龍](../Page/神尾龍.md "wikilink")、作画：[中原裕](../Page/中原裕.md "wikilink")、監修：[加藤潔](../Page/加藤潔.md "wikilink")）2004年5号
    -
  - [黑金丑島君](../Page/黑金丑島君.md "wikilink")（[真鍋昌平](../Page/真鍋昌平.md "wikilink")）2004年24号
    -
  - [土竜（モグラ）の唄](../Page/土竜の唄.md "wikilink")（[高橋のぼる](../Page/高橋のぼる.md "wikilink")）[週刊Young
    Sunday](../Page/週刊Young_Sunday.md "wikilink")2005年38号 -
    2008年35号、ビッグコミックスピリッツ2008年41号 -
  - [請叫我英雄](../Page/請叫我英雄.md "wikilink")（[花沢健吾](../Page/花沢健吾.md "wikilink")）2009年22・23合併号
    -
  - [オーミ先生の微熱](../Page/オーミ先生の微熱.md "wikilink")（[河内遙](../Page/河内遙.md "wikilink")）2010年30号
    - （長期休載中）
  - [薙刀社青春日記](../Page/薙刀社青春日記.md "wikilink")（[こざき亜衣](../Page/こざき亜衣.md "wikilink")）2011年8号
    -
  - [るみちゃんの事象](../Page/るみちゃんの事象.md "wikilink")（[原克玄](../Page/原克玄.md "wikilink")）2011年14号
    -
  - [DRAGON
    JAM](../Page/DRAGON_JAM.md "wikilink")（[藤井五成](../Page/藤井五成.md "wikilink")）2011年23号
    - ※『[月刊Big Comic
    Spirits](../Page/月刊Big_Comic_Spirits.md "wikilink")』より転籍
  - [週刊事件予報](../Page/週刊事件予報.md "wikilink")（複数の漫画家が週替わりで担当）2011年xx号 -
    ※目次ページ掲載の1コマ漫画
  - [おかゆネコ](../Page/おかゆネコ.md "wikilink")（[吉田戦車](../Page/吉田戦車.md "wikilink")）2012年18号
    -
  - [明日にはあがります。](../Page/明日にはあがります。.md "wikilink")（[水口尚樹](../Page/水口尚樹.md "wikilink")）2012年20号
    -
  - [花もて語れ](../Page/花もて語れ.md "wikilink")（[片山ユキヲ](../Page/片山ユキヲ.md "wikilink")、朗読協力・朗読原案：[東百道](../Page/東百道.md "wikilink")）2012年25号
    - ※『月刊\!スピリッツ』より転籍
  - [くーねるまるた](../Page/くーねるまるた.md "wikilink")（[高尾じんぐ](../Page/高尾じんぐ.md "wikilink")）2012年36・37合併号
    -
  - [マガツクニ風土記](../Page/マガツクニ風土記.md "wikilink")（原作：[あまやゆうき](../Page/あまやゆうき.md "wikilink")、作画：[吉田史朗](../Page/吉田史朗.md "wikilink")）2013年6・7合併号
    -
  - [みどりの星](../Page/みどりの星.md "wikilink")（[真造圭伍](../Page/真造圭伍.md "wikilink")）2013年13号
    -
  - [王様達のヴァイキング](../Page/王様達のヴァイキング.md "wikilink")（[さだやす](../Page/さだやす.md "wikilink")、アイデア協力：[深見真](../Page/深見真.md "wikilink")）2013年14号
    -
  - [火線上のハテルマ](../Page/火線上のハテルマ.md "wikilink")（[せきやてつじ](../Page/せきやてつじ.md "wikilink")）2013年36・37合併号
    -
  - [白暮（はくぼ）のクロニクル](../Page/白暮のクロニクル.md "wikilink")（[結城正美](../Page/結城正美.md "wikilink")）2013年39号
    -
  - [ピカロ](../Page/ピカロ.md "wikilink")（[白瀬透](../Page/白瀬透.md "wikilink")）2013年40号
    -
  - [クリームソーダシティ](../Page/クリームソーダシティ.md "wikilink")（[長尾謙一郎](../Page/長尾謙一郎.md "wikilink")）2013年41号
    -
  - [ルームメイト](../Page/ルームメイト_\(今邑彩\)#漫画.md "wikilink")（[武富健治](../Page/武富健治.md "wikilink")、原作：[今邑彩](../Page/今邑彩.md "wikilink")）2013年42号
    -
  - [偉人住宅ツバキヒルズ](../Page/偉人住宅ツバキヒルズ.md "wikilink")（[野田宏](../Page/野田宏.md "wikilink")）2013年50号
    -
  - [キミ\!さいよー](../Page/キミ!さいよー.md "wikilink")（[石原まこちん](../Page/石原まこちん.md "wikilink")）2013年50号
    -
  - [ちぽさんぽ](../Page/ちぽさんぽ.md "wikilink")（[竹本友二](../Page/竹本友二.md "wikilink")）2013年50号
    -
  - [黒鉄（クロガネ）ボブスレー](../Page/黒鉄ボブスレー.md "wikilink")（[土屋雄民](../Page/土屋雄民.md "wikilink")）2013年52号
    -
  - [夕空のクライフイズム](../Page/夕空のクライフイズム.md "wikilink")（[手原和憲](../Page/手原和憲.md "wikilink")）2014年2・3合併号
    -
  - [東伍郎とまろすけ](../Page/東伍郎とまろすけ.md "wikilink")（[長月キュー](../Page/長月キュー.md "wikilink")）2014年4・5合併号
    -

### 隔週連載

  - [鈴里高校書道部](../Page/鈴里高校書道部.md "wikilink")（[河合克敏](../Page/河合克敏.md "wikilink")）週刊ヤングサンデー2007年2号
    - 2008年35号、ビッグコミックスピリッツ2008年41号 -
  - [ちいさこべえ](../Page/ちいさこべ#漫画版.md "wikilink")（[望月ミネタロウ](../Page/望月ミネタロウ.md "wikilink")、原作：[山本周五郎](../Page/山本周五郎.md "wikilink")）2012年40号
    -、暫く休載を経て2013年12号より隔週連載

### 不定期連載

  - [花と奥たん](../Page/花と奥たん.md "wikilink")（[高橋真](../Page/高橋真.md "wikilink")）2008年2号
    -
  - [時空建築幻視譚](../Page/時空建築幻視譚.md "wikilink")（[冬目景](../Page/冬目景.md "wikilink")）2010年9号
    - 2015年16號
  - [デモクラティア](../Page/デモクラティア.md "wikilink")（[間瀬元朗](../Page/間瀬元朗.md "wikilink")）2013年38号
    -

雖然通常連載作において、これらの不定期連載が掲載される関係上ローテーションで休載が行われる。

## 歴代編集長

1.  [白井勝也](../Page/白井勝也.md "wikilink")（1980年 - 1990年）
2.  [亀井修](../Page/亀井修.md "wikilink")（1990年 - 1996年）
3.  久保田滋夫（1996年 - 1998年）
4.  片寄聰（1998年 - 1999年7月）
5.  [長崎尚志](../Page/長崎尚志.md "wikilink")（1999年8月 - 2001年7月）
6.  武藤伸之（2001年8月 - 2003年7月）
7.  立川義剛（2003年8月 - 2010年7月）
8.  堀靖樹（2010年8月 - 2013年5月）
9.  村山広（2013年6月 - ）

## 过去连载的作品

### あ行

  - 相原コージのなにがオモロイの? - [相原コージ](../Page/相原コージ.md "wikilink")
  - アイ・ラブ・ユー - [盛田賢司](../Page/盛田賢司.md "wikilink")
  - 青空 - [原秀則](../Page/原秀則.md "wikilink")
  - 青の戦士 -
    [狩撫麻礼](../Page/狩撫麻礼.md "wikilink")+[谷口治郎](../Page/谷口治郎.md "wikilink")
  - [アグネス仮面](../Page/アグネス仮面.md "wikilink") -
    [ヒラマツ・ミノル](../Page/ヒラマツ・ミノル.md "wikilink")
  - 悪魔のうたたね - [喜国雅彦](../Page/喜国雅彦.md "wikilink")
  - [あさってDANCE](../Page/あさってDANCE.md "wikilink") -
    [山本直樹](../Page/山本直樹.md "wikilink")
  - [明日のない空](../Page/明日のない空.md "wikilink") -
    [塀内夏子](../Page/塀内夏子.md "wikilink")
  - [愛情白皮書](../Page/愛情白皮書.md "wikilink") -
    [柴门文](../Page/柴门文.md "wikilink")
  - [赤い鳩](../Page/赤い鳩.md "wikilink") -
    [小池一夫](../Page/小池一夫.md "wikilink")+[池上辽一](../Page/池上辽一.md "wikilink")
  - [いいひと。](../Page/いいひと。.md "wikilink") -
    [高橋真](../Page/高橋真.md "wikilink")
  - [イオナ](../Page/イオナ_\(漫画\).md "wikilink") -
    [澤井健](../Page/澤井健.md "wikilink")
  - [イカロスの娘](../Page/イカロスの娘.md "wikilink") -
    [御厨さと美](../Page/御厨さと美.md "wikilink")
  - [死亡預告](../Page/死亡預告.md "wikilink") -
    [間瀬元朗](../Page/間瀬元朗.md "wikilink") ※『週刊ヤングサンデー』より転籍
  - 以蔵のキモチ（[佐藤宏之](../Page/佐藤宏之.md "wikilink")）
  - [愛しのアイリーン](../Page/愛しのアイリーン.md "wikilink") -
    [新井英樹](../Page/新井英樹.md "wikilink")
  - [ヴィルトゥス](../Page/ヴィルトゥス_\(漫画\).md "wikilink") - 義凡+信濃川日出雄
  - [ウシハル](../Page/ウシハル.md "wikilink") - ゴトウユキコ ※『月刊\!スピリッツ』より転籍
  - [伝染るんです。](../Page/伝染るんです。.md "wikilink") -
    [吉田戦車](../Page/吉田戦車.md "wikilink")
  - [SP (電視劇)](../Page/SP_\(電視劇\).md "wikilink") -
    [金城一紀](../Page/金城一紀.md "wikilink")+[灰原薬](../Page/灰原薬.md "wikilink")
  - 江戸前・あ・めーりかん - [藤波俊彦](../Page/藤波俊彦.md "wikilink")
  - [江戸むらさき特急](../Page/江戸むらさき特急.md "wikilink") -
    [ほりのぶゆき](../Page/ほりのぶゆき.md "wikilink")
  - [エバタのロック](../Page/エバタのロック.md "wikilink") -
    [室井大資](../Page/室井大資.md "wikilink")
  - [F](../Page/F_\(漫画\).md "wikilink") -
    [六田登](../Page/六田登.md "wikilink")
  - [おごってジャンケン隊](../Page/おごってジャンケン隊.md "wikilink") -
    [現代洋子](../Page/現代洋子.md "wikilink")
  - [おたんこナース](../Page/おたんこナース.md "wikilink") -
    [小林光恵](../Page/小林光恵.md "wikilink")+[佐佐木倫子](../Page/佐佐木倫子.md "wikilink")
  - [男樹](../Page/男樹.md "wikilink")（単行本2巻以降） -
    [本宮ひろ志](../Page/本宮ひろ志.md "wikilink")
  - 鬼虫 - [柏木ハルコ](../Page/柏木ハルコ.md "wikilink")
  - オヤジ高校生 - [川三番地](../Page/川三番地.md "wikilink")
  - [おやすみプンプン](../Page/おやすみプンプン.md "wikilink") -
    [浅野いにお](../Page/浅野いにお.md "wikilink")
    ※『週刊ヤングサンデー』より転籍
  - 俺の女たち - [本宮ひろ志](../Page/本宮ひろ志.md "wikilink")
  - [オメガトライブ](../Page/オメガトライブ_\(漫画\).md "wikilink") -
    [玉井雪雄](../Page/玉井雪雄.md "wikilink")

### か行

  - [怪獣人生](../Page/怪獣人生.md "wikilink") - ほりのぶゆき
  - [格闘太陽伝ガチ](../Page/格闘太陽伝ガチ.md "wikilink") -
    [青山広美](../Page/青山広美.md "wikilink")
  - [風します?](../Page/風します?.md "wikilink") -
    [小道迷子](../Page/小道迷子.md "wikilink")
  - [ガチャバイ](../Page/ガチャバイ.md "wikilink") -
    [はるき悦巳](../Page/はるき悦巳.md "wikilink")
  - [神のちから](../Page/神のちから.md "wikilink") -
    [櫻桃子](../Page/櫻桃子.md "wikilink")
  - [神之左手，惡魔之右手](../Page/神之左手，惡魔之右手.md "wikilink") -
    [楳圖一雄](../Page/楳圖一雄.md "wikilink")
  - [かもめ☆チャンス](../Page/かもめ☆チャンス.md "wikilink") -
    [玉井雪雄](../Page/玉井雪雄.md "wikilink")
  - [カラブキ](../Page/カラブキ.md "wikilink") -
    [中川いさみ](../Page/中川いさみ.md "wikilink")
  - [軽井沢シンドローム](../Page/軽井沢シンドローム.md "wikilink") -
    [たがみよしひさ](../Page/たがみよしひさ.md "wikilink")
  - [キクニの全県ラン
    走りたおすぜJAPAN\!](../Page/キクニの全県ラン_走りたおすぜJAPAN!.md "wikilink")
    - [喜国雅彦](../Page/喜国雅彦.md "wikilink")
  - [傷追い人](../Page/傷追い人.md "wikilink") -
    [小池一夫](../Page/小池一夫.md "wikilink")+[池上辽一](../Page/池上辽一.md "wikilink")
  - [キスより簡単](../Page/キスより簡単.md "wikilink") -
    [石坂啓](../Page/石坂啓.md "wikilink")
  - [キックのお姉さん](../Page/キックのお姉さん.md "wikilink") -
    [稲井雄人](../Page/稲井雄人.md "wikilink")
  - [ギャラリーフェイク](../Page/ギャラリーフェイク.md "wikilink") -
    [細野不二彥](../Page/細野不二彥.md "wikilink")
  - [ギャル男VS宇宙人](../Page/ギャル男VS宇宙人.md "wikilink") -
    [𠮷沢潤一](../Page/吉沢潤一.md "wikilink")
  - [ギョ](../Page/ギョ.md "wikilink") -
    [伊藤润二](../Page/伊藤润二.md "wikilink")\<\!--
  - [今日あそべる\!?](../Page/今日あそべる!?.md "wikilink") - のりつけ雅春
    単行本が出るようなら表へ出す--\>
  - [強制ヒーロー](../Page/強制ヒーロー.md "wikilink") -
    [宮下裕樹](../Page/宮下裕樹.md "wikilink") ※『月刊\!スピリッツ』へ転籍
  - [鬼龍院冴子探偵事務所](../Page/鬼龍院冴子探偵事務所.md "wikilink") -
    [三上龍哉](../Page/三上龍哉.md "wikilink")
  - [禁ドン\!](../Page/禁ドン!.md "wikilink")
    -[立沢直也](../Page/立沢直也.md "wikilink")
  - [哭泣杀神](../Page/哭泣杀神.md "wikilink") -
    [小池一夫](../Page/小池一夫.md "wikilink")+[池上辽一](../Page/池上辽一.md "wikilink")
  - [月下の棋士](../Page/月下の棋士.md "wikilink") -
    [能條純一](../Page/能條純一.md "wikilink")
  - [クマのプー太郎](../Page/クマのプー太郎.md "wikilink") - 中川いさみ
  - [元気があってよろしいっ\!](../Page/元気があってよろしいっ!.md "wikilink") -
    [原律子](../Page/原律子.md "wikilink")
  - [現在官僚系 もふ](../Page/現在官僚系_もふ.md "wikilink") -
    [鍋田吉郎](../Page/鍋田吉郎.md "wikilink")+[並木洋美](../Page/並木洋美.md "wikilink")
  - [GOGO\!生活非安全課](../Page/GOGO!生活非安全課.md "wikilink") -
    [ロドリゲス井之介](../Page/ロドリゲス井之介.md "wikilink")
  - [GO\!GO\!HEAVEN\!](../Page/GO!GO!HEAVEN!.md "wikilink") -
    [小原信治](../Page/小原信治.md "wikilink")+[海埜ゆうこ](../Page/海埜ゆうこ.md "wikilink")
  - [高校アフロ田中](../Page/アフロ田中シリーズ.md "wikilink") -
    [のりつけ雅春](../Page/のりつけ雅春.md "wikilink")
  - [高校球児ザワさん](../Page/高校球児ザワさん.md "wikilink") -
    [三島衛里子](../Page/三島衛里子.md "wikilink")
  - [合同ナイン](../Page/合同ナイン.md "wikilink") -
    [若狭たけし](../Page/若狭たけし.md "wikilink")
  - [コージ苑](../Page/コージ苑.md "wikilink") - 相原コージ
  - [極道一直線](../Page/極道一直線.md "wikilink") - 三上龍哉
  - [ココナッツピリオド
    -地球温暖化を止めるウサギ-](../Page/ココナッツピリオド_-地球温暖化を止めるウサギ-.md "wikilink")
    - [山田玲司](../Page/山田玲司.md "wikilink")
  - [QUOJUZ-コジューツ-](../Page/QUOJUZ-コジューツ-.md "wikilink") -
    [柏木ハルコ](../Page/柏木ハルコ.md "wikilink")
  - [56マイルの悪魔](../Page/56マイルの悪魔.md "wikilink") -
    [御厨さと美](../Page/御厨さと美.md "wikilink")
  - [このSを、見よ\!](../Page/このSを、見よ!.md "wikilink") -
    [北崎拓](../Page/北崎拓.md "wikilink")

### さ行

  - [サーティーガールズ](../Page/サーティーガールズ.md "wikilink") -
    [北沢未也](../Page/北沢未也.md "wikilink")+若狭たけし
  - [最终兵器少女](../Page/最终兵器少女.md "wikilink") - 高橋しん
  - [早乙女タイフーン](../Page/早乙女タイフーン.md "wikilink") -
    [鯨井意玖子](../Page/鯨井意玖子.md "wikilink")
  - [さすらいアフロ田中](../Page/アフロ田中シリーズ.md "wikilink") -
    [のりつけ雅春](../Page/のりつけ雅春.md "wikilink")
  - [サユリ1号](../Page/サユリ1号.md "wikilink") -
    [村上かつら](../Page/村上かつら.md "wikilink")
  - [サヨナラレフティ](../Page/サヨナラレフティ.md "wikilink") -
    [小倉和之](../Page/小倉和之.md "wikilink")＋[山本おさむ](../Page/山本おさむ.md "wikilink")
  - [サルでも描けるまんが教室](../Page/サルでも描けるまんが教室.md "wikilink") -
    [竹熊健太郎](../Page/竹熊健太郎.md "wikilink")+相原コージ
  - [THE3名様](../Page/THE3名様.md "wikilink") -
    [石原まこちん](../Page/石原まこちん.md "wikilink")
  - [空姐真命苦](../Page/空姐真命苦.md "wikilink") -
  - [CB感。reborn](../Page/CB感。reborn.md "wikilink") -
    [東本昌平](../Page/東本昌平.md "wikilink")
  - [GTR -GREAT TARO
    REVOLUTION-](../Page/GTR_-GREAT_TARO_REVOLUTION-.md "wikilink") -
    [椎名理央](../Page/椎名理央.md "wikilink")+[戸田尚伸](../Page/戸田尚伸.md "wikilink")
  - [J.BOY](../Page/J.BOY_\(漫画\).md "wikilink") - 能條純一
  - [7人のシェイクスピア](../Page/7人のシェイクスピア.md "wikilink") -
    [ハロルド作石](../Page/ハロルド作石.md "wikilink")
  - [疾風迅雷](../Page/疾風迅雷.md "wikilink")
    -[もりやまつる](../Page/もりやまつる.md "wikilink")
  - [しっぷうどとう](../Page/しっぷうどとう.md "wikilink") - 盛田賢司
  - [邪眼は月輪に飛ぶ](../Page/邪眼は月輪に飛ぶ.md "wikilink") -
    [藤田和日郎](../Page/藤田和日郎.md "wikilink")
  - [シュトヘル](../Page/シュトヘル.md "wikilink") -
    [伊藤悠](../Page/伊藤悠.md "wikilink") ※『月刊\!スピリッツ』へ転籍
  - [ショコラ](../Page/流氓蛋糕店.md "wikilink") -
    [窪之內英策](../Page/窪之內英策.md "wikilink")
  - [SHOP自分](../Page/SHOP自分.md "wikilink") -
    [柳沢きみお](../Page/柳沢きみお.md "wikilink")
  - [上京アフロ田中](../Page/アフロ田中シリーズ.md "wikilink") - のりつけ雅春
  - [新クロサギ](../Page/诈欺猎人.md "wikilink") -
    [黒丸](../Page/黑丸.md "wikilink")、原案：[夏原武](../Page/夏原武.md "wikilink")
  - [新ブラックジャックによろしく](../Page/醫界風雲.md "wikilink") -
    [佐藤秀峰](../Page/佐藤秀峰.md "wikilink")
  - [スキエンティア](../Page/スキエンティア.md "wikilink") -
    [戸田誠二](../Page/戸田誠二.md "wikilink") ※『月刊\!スピリッツ』へ転籍
  - [水晶（スジョン） 日韓恋愛狂詩曲](../Page/水晶_日韓恋愛狂詩曲.md "wikilink") -
    [猪熊しのぶ](../Page/猪熊しのぶ.md "wikilink")、原案：[TK<sup>2</sup>](../Page/江夏賢.md "wikilink")
  - [昴（スバル）](../Page/昴_\(漫画\).md "wikilink") -
    [曽田正人](../Page/曽田正人.md "wikilink")
  - [スポーツポン](../Page/スポーツポン.md "wikilink") -
    [吉田戦車](../Page/吉田戦車.md "wikilink")
  - [スローニン](../Page/スローニン.md "wikilink") -
    [吉田聡](../Page/吉田聡.md "wikilink")
  - [寸前爆発](../Page/寸前爆発.md "wikilink") -
    [常盤雅幸](../Page/常盤雅幸.md "wikilink")
  - [正義の味方モンキーズ](../Page/正義の味方モンキーズ.md "wikilink") -
    [山本康人](../Page/山本康人.md "wikilink")
  - [ゼブラーマン](../Page/ゼブラーマン_\(漫画\).md "wikilink") -
    [宮藤官九郎](../Page/宮藤官九郎.md "wikilink")+[山田玲司](../Page/山田玲司.md "wikilink")
  - [セルフ](../Page/セルフ_\(漫画\).md "wikilink") -
    [朔ユキ蔵](../Page/朔ユキ蔵.md "wikilink")
  - [センチメントの季節](../Page/センチメントの季節.md "wikilink") -
    [榎本ナリコ](../Page/榎本ナリコ.md "wikilink")
  - [村塾物語](../Page/村塾物語.md "wikilink") - はしもといわお

### た行

  - [就業向前衝](../Page/就業向前衝.md "wikilink") -
    [山口かつみ](../Page/山口かつみ.md "wikilink")
  - [竹光侍](../Page/竹光侍.md "wikilink") -
    原作：[永福一成](../Page/永福一成.md "wikilink")、漫画：[松本大洋](../Page/松本大洋.md "wikilink")
  - [旅マン](../Page/旅マン.md "wikilink") - ほりのぶゆき
  - [男魂\!\!インポッシブル](../Page/男魂!!インポッシブル.md "wikilink") -
    [深海魚](../Page/深海魚_\(漫画家\).md "wikilink")
  - [地平線でダンス](../Page/地平線でダンス.md "wikilink") -
    [柏木ハルコ](../Page/柏木ハルコ.md "wikilink")
  - [ちゃんどら](../Page/ちゃんどら.md "wikilink") -
    [いしかわじゅん](../Page/いしかわじゅん.md "wikilink")
  - [チャンネルはそのまま\!](../Page/チャンネルはそのまま!.md "wikilink") -
    [佐佐木倫子](../Page/佐佐木倫子.md "wikilink")
  - [駐禁ウォーズ\!\!](../Page/駐禁ウォーズ!!.md "wikilink") -
    [今井亮一](../Page/今井亮一.md "wikilink")+[ウヒョ助](../Page/ウヒョ助.md "wikilink")
  - [中退アフロ田中](../Page/アフロ田中シリーズ.md "wikilink") - のりつけ雅春
  - [チューロウ](../Page/チューロウ.md "wikilink") - 盛田賢司
    ※『[ビッグコミックスピリッツ21](../Page/ビッグコミックスピリッツ21.md "wikilink")』へ転籍
  - [超人ウタダ](../Page/超人ウタダ.md "wikilink") - 山本康人
  - [ちょんまげどん](../Page/ちょんまげどん.md "wikilink") - ほりのぶゆき
  - [妻をめとらば](../Page/妻をめとらば.md "wikilink") -
    [柳沢きみお](../Page/柳沢きみお.md "wikilink")
  - [つゆダク](../Page/つゆダク.md "wikilink") -
    [朔ユキ蔵](../Page/朔ユキ蔵.md "wikilink")
  - [ツルモク独身寮](../Page/ツルモク独身寮.md "wikilink") -
    [窪之內英策](../Page/窪之內英策.md "wikilink")
  - [DINO（ディーノ）](../Page/DINO_\(漫画\).md "wikilink") - 柳沢きみお
  - [ティーンズブルース](../Page/ティーンズブルース.md "wikilink") -
    [コージィ城倉](../Page/コージィ城倉.md "wikilink")
  - [Dの魔王 ジョーカー・ゲーム](../Page/ジョーカー・ゲーム.md "wikilink") -
    [柳廣司](../Page/柳廣司.md "wikilink")+[霜月かよ子](../Page/霜月かよ子.md "wikilink")
    ※『月刊\!スピリッツ』へ転籍
  - [帝王](../Page/帝王_\(漫画\).md "wikilink") -
    [倉科遼](../Page/倉科遼.md "wikilink")+[関口太郎](../Page/関口太郎_\(漫画家\).md "wikilink")
  - [惡童](../Page/惡童.md "wikilink") - [松本大洋](../Page/松本大洋.md "wikilink")
  - [鉄腕バーディー EVOLUTION](../Page/鉄腕バーディー_EVOLUTION.md "wikilink") -
    [結城正美](../Page/結城正美.md "wikilink")
  - [出直しといで\!](../Page/出直しといで!.md "wikilink") -
    [一色真人](../Page/一色真人.md "wikilink")
  - [出るトコ出ましょ\!](../Page/出るトコ出ましょ!.md "wikilink") -
    [稲光伸二](../Page/稲光伸二.md "wikilink")
  - [テレキネシス 山手テレビキネマ室](../Page/テレキネシス_山手テレビキネマ室.md "wikilink") -
    [芳崎せいむ](../Page/芳崎せいむ.md "wikilink")+[東周斎雅楽](../Page/東周斎雅楽.md "wikilink")
  - [転校生 オレのあそこがあいつのアレで](../Page/転校生_オレのあそこがあいつのアレで.md "wikilink") -
    [古泉智浩](../Page/古泉智浩.md "wikilink")
  - [電光石火](../Page/電光石火.md "wikilink") - 盛田賢司
  - [電波の城](../Page/電波の城.md "wikilink") - 細野不二彦
  - [東京エイティーズ](../Page/東京エイティーズ.md "wikilink") -
    [安童夕馬](../Page/樹林伸.md "wikilink")+大石知征
  - [東京大学物語](../Page/東京大学物語.md "wikilink") -
    [江川達也](../Page/江川達也.md "wikilink")
  - [東京愛情故事](../Page/東京愛情故事.md "wikilink") - 柴門ふみ
  - [逃走鉄馬バイソン](../Page/逃走鉄馬バイソン.md "wikilink") -
    [カサギヒロシ](../Page/カサギヒロシ.md "wikilink")
  - [遠い星から来たALICE](../Page/遠い星から来たALICE.md "wikilink") -
    [藤泽亨](../Page/藤泽亨.md "wikilink")
  - [DAWN -陽はまた昇る-](../Page/DAWN_-陽はまた昇る-.md "wikilink") -
    [倉科遼](../Page/倉科遼.md "wikilink")+[ナカタニD.](../Page/ナカタニD..md "wikilink")
  - [とつぜんDr.](../Page/とつぜんDr..md "wikilink") -
    [吾妻ひでお](../Page/吾妻ひでお.md "wikilink")
  - [トラキーヨ](../Page/トラキーヨ.md "wikilink") - 吉田聡
  - [虎の娘](../Page/虎の娘.md "wikilink") -
    [宮谷一彦](../Page/宮谷一彦.md "wikilink")

### な行

  - [奈緒子](../Page/奈緒子.md "wikilink") -
    [坂田信弘](../Page/坂田信弘.md "wikilink")+中原裕
  - [永沢君](../Page/永沢君.md "wikilink") - さくらももこ
  - [殴るぞ](../Page/殴るぞ.md "wikilink") - 吉田戦車
  - [なぜか笑介](../Page/なぜか笑介.md "wikilink") -
    [聖日出夫](../Page/聖日出夫.md "wikilink")
  - [にじこしいいんちょう](../Page/にじこしいいんちょう.md "wikilink") -
    [のぶみ](../Page/のぶみ.md "wikilink")
  - [21世紀少年](../Page/20世紀少年.md "wikilink") -
    [浦澤直樹](../Page/浦澤直樹.md "wikilink")
  - [20世紀少年](../Page/20世紀少年.md "wikilink") - 浦沢直樹
  - [日露戦争物語](../Page/日露戦争物語.md "wikilink") -
    [江川達也](../Page/江川達也.md "wikilink")
  - [日曜、午後、六時半。](../Page/日曜、午後、六時半。.md "wikilink") -
    [浅野いにお](../Page/浅野いにお.md "wikilink")
  - [二匹のブル](../Page/二匹のブル.md "wikilink") -
    [瀬叩龍](../Page/瀬叩龍.md "wikilink")+[岩重孝](../Page/いわしげ孝.md "wikilink")
  - [日本沈没](../Page/日本沉沒_\(漫畫\).md "wikilink") -
    [小松左京](../Page/小松左京.md "wikilink")+[一色登希彦](../Page/一色登希彦.md "wikilink")
  - [のぼうの城](../Page/のぼうの城.md "wikilink") -
    [和田竜](../Page/和田竜.md "wikilink")+花咲アキラ

### は行

  - [ハーフな分だけ](../Page/ハーフな分だけ.md "wikilink") -
    [星里望留](../Page/星里望留.md "wikilink")
  - [π（パイ）](../Page/π（漫画）.md "wikilink") -
    [古屋兔丸](../Page/古屋兔丸.md "wikilink")
  - [HIDEOUT](../Page/HIDEOUT.md "wikilink") -
    [柿崎正澄](../Page/柿崎正澄.md "wikilink")
  - [パギャル\!](../Page/パギャル!.md "wikilink") -
    [浜田ブリトニー](../Page/浜田ブリトニー.md "wikilink")
  - [ハクバノ王子サマ](../Page/ハクバノ王子サマ.md "wikilink") - 朔ユキ蔵
  - [バケツでごはん](../Page/バケツでごはん.md "wikilink") -
    [玖保キリコ](../Page/玖保キリコ.md "wikilink")
  - [ハッスル](../Page/ハッスル_\(漫画\).md "wikilink") - 一色まこと
  - [はっぱ64](../Page/はっぱ64.md "wikilink") - 山本直樹
  - [Happy\!](../Page/Happy!.md "wikilink") -
    [浦澤直樹](../Page/浦澤直樹.md "wikilink")
  - [バドフライ](../Page/バドフライ.md "wikilink") -
    [イワシタシゲユキ](../Page/イワシタシゲユキ.md "wikilink")
  - [花園メリーゴーランド](../Page/花園メリーゴーランド.md "wikilink") - 柏木ハルコ
  - [パパがも一度恋をした](../Page/パパがも一度恋をした.md "wikilink") -
    [阿部潤](../Page/阿部潤.md "wikilink")
  - [パパはニューギニア](../Page/パパはニューギニア.md "wikilink") -
    [高野聖ーナ](../Page/高野聖ーナ.md "wikilink")
  - [パパリンコ物語](../Page/パパリンコ物語.md "wikilink") -
    [江口寿史](../Page/江口寿史.md "wikilink")
  - [パラダイス通信](../Page/パラダイス通信.md "wikilink") -
    [神戸さくみ](../Page/神戸さくみ.md "wikilink")
  - [はるちゃん](../Page/はるちゃん.md "wikilink") -
    [青柳裕介](../Page/青柳裕介.md "wikilink")
  - [バロンドリロンド](../Page/バロンドリロンド.md "wikilink") -
    北沢未也+[梶川卓郎](../Page/梶川卓郎.md "wikilink")
  - [料理新鮮人](../Page/料理新鮮人.md "wikilink") -
    [せきやてつじ](../Page/せきやてつじ.md "wikilink")
      - バンビ〜ノ\! SECONDO
  - [ヒガンバナの女王](../Page/ヒガンバナの女王.md "wikilink") -
    [岡仁志太郎](../Page/岡仁志太郎.md "wikilink")
  - [美大受験戦記 アリエネ](../Page/美大受験戦記_アリエネ.md "wikilink") -
    [山田玲司](../Page/山田玲司.md "wikilink") ※『月刊\!スピリッツ』へ転籍
  - [100億の男](../Page/100億の男.md "wikilink") -
    [国友やすゆき](../Page/国友やすゆき.md "wikilink")
  - [ひらけ相合傘](../Page/ひらけ相合傘.md "wikilink") -
    [吉田戦車](../Page/吉田戦車.md "wikilink")
  - [ビリーバーズ](../Page/ビリーバーズ.md "wikilink") - 山本直樹
  - [ピンポン](../Page/乒乓_\(電影\).md "wikilink") -
    [松本大洋](../Page/松本大洋.md "wikilink")
  - [fine.](../Page/fine..md "wikilink")
    -[ファイン](../Page/ファイン.md "wikilink") -
    [信濃川日出雄](../Page/信濃川日出雄.md "wikilink")
  - [ブラック&ホワイト](../Page/ブラック&ホワイト_\(漫画\).md "wikilink") -
    [高橋春男](../Page/高橋春男.md "wikilink") ※『ブラックあんどホワイト』と表記されることもある
  - [プラモ男子とプリチー女子-ミズオとイエナの1年戦争-](../Page/プラモ男子とプリチー女子-ミズオとイエナの1年戦争-.md "wikilink")
    -
    [ゆきもり](../Page/ゆきもり.md "wikilink")+[ソラキスズ](../Page/ソラキスズ.md "wikilink")
  - [ぷりぷり県](../Page/ぷりぷり県.md "wikilink") - 吉田戦車
  - [ブルー・ジーン](../Page/ブルー・ジーン.md "wikilink") -
    [くじらいいくこ](../Page/鯨井意玖子.md "wikilink")
  - [ブルーダー](../Page/ブルーダー.md "wikilink") -
    [周防瞭](../Page/周防瞭.md "wikilink")+盛田賢司
  - [Heaven?](../Page/Heaven?.md "wikilink") - 佐々木倫子
  - [編集王](../Page/編集王.md "wikilink") -
    [土田世紀](../Page/土田世紀.md "wikilink")
  - [14歳（フォーティーン）](../Page/14歳_\(漫画\).md "wikilink") - 楳図かずお
  - [ペット](../Page/ペット_\(漫画\).md "wikilink") -
    [三宅乱丈](../Page/三宅乱丈.md "wikilink")
  - [冒険してもいい頃](../Page/冒険してもいい頃.md "wikilink") -
    [みやすのんき](../Page/みやすのんき.md "wikilink")
  - [敏行快跑](../Page/敏行快跑.md "wikilink") -
    [花沢健吾](../Page/花沢健吾.md "wikilink")
  - [BOXERケン](../Page/ケンとエリカ.md "wikilink") - 江口寿史
  - [僕BOKU](../Page/僕BOKU.md "wikilink") -
    [山本康人](../Page/山本康人.md "wikilink")
  - [ぼくらのフンカ祭](../Page/ぼくらのフンカ祭.md "wikilink") -
    [真造圭伍](../Page/真造圭伍.md "wikilink")
  - [僕らはみんな生きている](../Page/僕らはみんな生きている.md "wikilink") -
    [一色伸幸](../Page/一色伸幸.md "wikilink")+山本直樹
  - [ぼくんち](../Page/ぼくんち.md "wikilink") -
    [西原理恵子](../Page/西原理恵子.md "wikilink")
  - [ポコあポコ](../Page/ポコあポコ.md "wikilink") - 小道迷子
  - [ぼっけもん](../Page/ぼっけもん.md "wikilink") - 岩重孝
  - [保土ヶ谷最中速派セブン](../Page/保土ヶ谷最中速派セブン.md "wikilink") -
    [中野ハジメ](../Page/中野ハジメ.md "wikilink")
  - [ボブとゆかいな仲間たち](../Page/ボブとゆかいな仲間たち.md "wikilink") -
    [パンチョ近藤](../Page/パンチョ近藤.md "wikilink")
  - [ホムンクルス](../Page/ホムンクルス_\(漫画\).md "wikilink") -
    [山本英夫](../Page/山本英夫.md "wikilink")

### ま行

  - [禍MAGA](../Page/禍MAGA.md "wikilink") -
    [若桑一人](../Page/若桑一人.md "wikilink")+[石川賢](../Page/石川賢.md "wikilink")
  - [マドンナ](../Page/マドンナ_\(漫画・くじらいいく子\).md "wikilink") -
    [鯨井意玖子](../Page/鯨井意玖子.md "wikilink")
  - [マネームーン](../Page/マネームーン.md "wikilink") - 石坂啓
  - [美咲 No.1](../Page/美咲_No.1.md "wikilink") -
    [藤崎聖人](../Page/藤崎聖人.md "wikilink")
  - [店もん](../Page/店もん.md "wikilink") - ほりのぶゆき
  - [みんな生きてる](../Page/みんな生きてる.md "wikilink") -
    [原克玄](../Page/原克玄.md "wikilink")
  - [みんな元気か\!](../Page/みんな元気か!.md "wikilink") -
    [やまさき十三](../Page/やまさき十三.md "wikilink")+[ひきの真二](../Page/引野真二.md "wikilink")
  - [MOON -昴 ソリチュード スタンディング-](../Page/昴_\(漫画\).md "wikilink") -
    [曽田正人](../Page/曽田正人.md "wikilink")
  - [ムッチィにご用心\!\!](../Page/ムッチィにご用心!!.md "wikilink") - むつ利之
  - [むねあつ](../Page/むねあつ.md "wikilink") -
    [村岡ユウ](../Page/村岡ユウ.md "wikilink")
  - [めぞん一刻](../Page/めぞん一刻.md "wikilink") -
    [高桥留美子](../Page/高桥留美子.md "wikilink")
  - [モッブ](../Page/モッブ_\(漫画\).md "wikilink") -
    [滝沢解](../Page/滝沢解.md "wikilink")+池上遼一
  - [もにもに](../Page/もにもに.md "wikilink") -
    [相原コージ](../Page/相原コージ.md "wikilink")
      - （外部リンク）
  - [ももんち](../Page/ももんち.md "wikilink") -
    [冬目景](../Page/冬目景.md "wikilink")

### や行

  - [やったろうじゃん\!\!](../Page/やったろうじゃん!!.md "wikilink") -
    [原秀則](../Page/原秀則.md "wikilink")
  - [以柔克剛](../Page/以柔克剛.md "wikilink") -
    [浦澤直樹](../Page/浦澤直樹.md "wikilink")
  - [夢みるトマト](../Page/夢みるトマト.md "wikilink") - 石坂啓
  - [よいこ](../Page/乖小孩.md "wikilink") -
    [石川優吾](../Page/石川優吾.md "wikilink")

### ら行

  - [ラブレター](../Page/ラブレター_\(漫画\).md "wikilink") -
    [じんのひろあき](../Page/じんのひろあき.md "wikilink")+若狭たけし
  - [無殼蝸牛連環泡](../Page/無殼蝸牛連環泡.md "wikilink") -
    [星里望留](../Page/星里望留.md "wikilink")
  - [岡野玲子](../Page/岡野玲子.md "wikilink") -
    [岡野玲子](../Page/岡野玲子.md "wikilink")
  - [瑠璃色ゼネレーション](../Page/瑠璃色ゼネレーション.md "wikilink") - 柳沢きみお
  - [少年犯之七人](../Page/少年犯之七人.md "wikilink") -
    [安部譲二](../Page/安部譲二.md "wikilink")+[柿崎正澄](../Page/柿崎正澄.md "wikilink")
  - [ロープボール](../Page/ロープボール.md "wikilink") -
    [川原裕聖](../Page/川原裕聖.md "wikilink")+[波多野秀行](../Page/波多野秀行.md "wikilink")
  - [LOST MAN](../Page/LOST_MAN.md "wikilink") -
    [草場道輝](../Page/草場道輝.md "wikilink") ※『週刊ヤングサンデー』より転籍

### わ行

  - [我が名は海師](../Page/我が名は海師.md "wikilink")（原作：[小森陽一](../Page/小森陽一_\(漫画原作者\).md "wikilink")+[武村勇治](../Page/武村勇治.md "wikilink")）
  - [わさび](../Page/わさび_\(漫画\).md "wikilink") -
    [一條裕子](../Page/一條裕子.md "wikilink")
  - [わたしは真悟](../Page/わたしは真悟.md "wikilink") - 楳図かずお
  - [ワタナベ](../Page/ワタナベ.md "wikilink") -
    [窪之內英策](../Page/窪之內英策.md "wikilink")
  - [灣岸競速](../Page/灣岸競速.md "wikilink") -
    [楠みちはる](../Page/楠みちはる.md "wikilink")
    ※[讲谈社の](../Page/讲谈社.md "wikilink")『[週刊Young
    Magazine](../Page/週刊Young_Magazine.md "wikilink")』へ転籍
  - [ONE\&ONLY](../Page/ONE&ONLY.md "wikilink") -
    [山田貴敏](../Page/山田貴敏.md "wikilink")

## 发行部数

  - 2004年（2003年9月 - 2004年8月） 460,354部\[1\]
  - 2005年（2004年9月 - 2005年8月） 416,625部\[2\]
  - 2006年（2005年9月 - 2006年8月） 394,042部\[3\]
  - 2007年（2006年9月 - 2007年8月） 373,500部\[4\]
  - 2008年（2007年10月 - 2008年9月） 355,062部\[5\]

|       | 1〜3月      | 4〜6月      | 7〜9月      | 10〜12月    |
| ----- | --------- | --------- | --------- | --------- |
| 2008年 |           | 346,750 部 | 360,750 部 | 318,834 部 |
| 2009年 | 308,750 部 | 299,167 部 | 288,917 部 | 275,417 部 |
| 2010年 | 257,834 部 | 253,917 部 | 251,834 部 | 251,667 部 |
| 2011年 | 248,250 部 | 244,000 部 | 241,834 部 | 241,417 部 |
| 2012年 | 233,750 部 | 220,167 部 | 216,834 部 | 212,231 部 |
| 2013年 | 208,182 部 | 205,417 部 | 201,847 部 |           |

发行部数（2008年4月）（[社団法人日本雑誌協会](https://web.archive.org/web/20120226141842/http://www.j-magazine.or.jp/magdata/index.php?module=list&action=list)）

## 関連項目

  - [週刊Young Sunday](../Page/週刊Young_Sunday.md "wikilink")
  - [月刊IKKI](../Page/月刊IKKI.md "wikilink")
  - [Big Comic](../Page/Big_Comic.md "wikilink")
  - [Big Comic Original](../Page/Big_Comic_Original.md "wikilink")
  - [Big Comic Superior](../Page/Big_Comic_Superior.md "wikilink")
  - [やわらかスピリッツ](../Page/やわらかスピリッツ.md "wikilink")
  - [JUNK](../Page/JUNK.md "wikilink")（[TBSラジオ](../Page/TBSラジオ&コミュニケーションズ.md "wikilink")）

## 參考資料

  - [博客來網路書店檢索系統](http://www.books.com.tw/)（中文譯名）

## 外部链接

  - [Big Comic Spirits網路版](http://spi-net.jp/)

  -
[Category:小學館的漫畫雜誌](../Category/小學館的漫畫雜誌.md "wikilink")
[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:青年漫畫雜誌](../Category/青年漫畫雜誌.md "wikilink")
[Category:週刊漫畫雜誌](../Category/週刊漫畫雜誌.md "wikilink")

1.  [社団法人日本雑誌協会](http://www.j-magazine.or.jp/)JMPAマガジンデータによる該当期間中に発売された雑誌1号当たりの平均印刷部数。

2.
3.
4.
5.