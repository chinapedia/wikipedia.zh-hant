**CXCL1**（）是一小分子的[细胞因子属于CXC](../Page/细胞因子.md "wikilink")[趋化因子家族](../Page/趋化因子.md "wikilink")\[1\]。又被称作**生长调节致癌基因α**
(Growth-regulated oncogene alpha, GROα)\[2\]\[3\]\[4\]。
趋化因子CXCL1是由[巨噬细胞](../Page/巨噬细胞.md "wikilink")，[中性粒细胞和](../Page/中性粒细胞.md "wikilink")[上皮细胞表达的](../Page/上皮细胞.md "wikilink")\[5\]\[6\]。趋化因子CXCL1对中性粒细胞有细胞趋化作用\[7\]。CXCL1结合到趋化因子受体[CXCR2上而起细胞趋化作用](../Page/CXCR2.md "wikilink")\[8\]。人类的CXCL1与许多CXC类的趋化因子基因相邻聚集在第四染色体上\[9\]
。CXCL1的主要作用包括新血管形成、炎症反应、伤口愈合\[10\]和肿瘤形成\[11\]\[12\]。

## 参见

  - [CXCR2](../Page/CXCR2.md "wikilink")
  - [趋化因子](../Page/趋化因子.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:细胞因子](../Category/细胞因子.md "wikilink")
[Category:免疫学](../Category/免疫学.md "wikilink")

1.  Moser B, Clark-Lewis I, Zwahlen R, Baggiolini M.
    Neutrophil-activating properties of the melanoma growth-stimulatory
    activity. J Exp Med. 1990 May 1;171(5):1797-802.
2.  Moser B, Clark-Lewis I, Zwahlen R, Baggiolini M.
    Neutrophil-activating properties of the melanoma growth-stimulatory
    activity. J Exp Med. 1990 May 1;171(5):1797-802.
3.  Anisowicz, A., Bardwell, L., Sager, R. Constitutive overexpression
    of a growth-regulated gene in transformed Chinese hamster and human
    cells. Proc. Nat. Acad. Sci. 84: 7188-7192, 1987.
4.  Richmond, A., Thomas, H. G. (1988) Melanoma growth stimulatory
    activity: isolation from human melanoma tumors and characterization
    of tissue distribution J. Cell. Biochem. 36,185-198
5.  Iida N, Grotendorst GR. Cloning and sequencing of a new gro
    transcript from activated human monocytes: expression in leukocytes
    and wound tissue. Mol Cell Biol. 1990 Oct;10(10):5596-9.
6.  Becker S, Quay J, Koren HS, Haskill JS. Constitutive and stimulated
    MCP-1, GRO alpha, beta, and gamma expression in human airway
    epithelium and bronchoalveolar macrophages. Am J Physiol. 1994
    Mar;266(3 Pt 1):L278-86.
7.  Moser B, Clark-Lewis I, Zwahlen R, Baggiolini M.
    Neutrophil-activating properties of the melanoma growth-stimulatory
    activity. J Exp Med. 1990 May 1;171(5):1797-802.
8.  Devalaraja, R. M., Nanney, L. B., Du, J., Qian, Q., Yu, Y.,
    Devalaraja, M. N., Richmond, A. (2000) Delayed wound healing in
    CXCR2 knockout mice J. Investig. Dermatol. 115,234-244
9.  Richmond A, Balentien E, Thomas HG, Flaggs G, Barton DE, Spiess J,
    Bordoni R, Francke U, Derynck R. Molecular characterization and
    chromosomal mapping of melanoma growth stimulatory activity, a
    growth factor structurally related to beta-thromboglobulin. EMBO J.
    1988 Jul;7(7):2025-33.
10. Devalaraja, R. M., Nanney, L. B., Du, J., Qian, Q., Yu, Y.,
    Devalaraja, M. N., Richmond, A. (2000) Delayed wound healing in
    CXCR2 knockout mice J. Investig. Dermatol. 115,234-244
11. Anisowicz, A., Bardwell, L., Sager, R. Constitutive overexpression
    of a growth-regulated gene in transformed Chinese hamster and human
    cells. Proc. Nat. Acad. Sci. 84: 7188-7192, 1987.
12. Richmond, A., Thomas, H. G. (1988) Melanoma growth stimulatory
    activity: isolation from human melanoma tumors and characterization
    of tissue distribution J. Cell. Biochem. 36,185-198