**麻坡**（馬來語：Bandar Muar），又稱**香妃城**（Bandar Maharani）、**皇城**（Bandar
Diraja），是位於[馬來西亞](../Page/马来西亚.md "wikilink")[柔佛州西北部的都市](../Page/柔佛.md "wikilink")，也是[麻坡縣的縣府](../Page/麻坡縣.md "wikilink")，人口大約25萬，[華族人口過半](../Page/華族.md "wikilink")。為柔佛州第3大城，全市面積為135.2平方公里。隶属于麻坡市议会管辖范围。麻坡也荣获了东南亚最干净的城市。世界家具之根也在麻坡这个小城市，看来麻坡是世界瞩目景点好去处。

麻坡因是歷史古城、常年舉辦文創活動、有著豐富的美食、麻坡这个城市有个特长就是有著非常浓厚气息温馨之城。戰前遺蹟林立而成為馬來西亞熱門旅遊景點之一。麻坡有香妃城之稱，在2012年2月5日柔佛州[蘇丹正式的宣佈下成為](../Page/蘇丹.md "wikilink")「柔佛州皇城」，因此升格為香妃皇城。

## 历史

[Ptolemy_Asia_detail.jpg](https://zh.wikipedia.org/wiki/File:Ptolemy_Asia_detail.jpg "fig:Ptolemy_Asia_detail.jpg")

### 早期歷史

早在公元一世紀的希臘文明，埃及天文學家[克劳狄乌斯·托勒密在製作世界地圖的時候](../Page/克劳狄乌斯·托勒密.md "wikilink")，把[馬來半島命名為](../Page/馬來半島.md "wikilink")[黃金半島](../Page/黃金半島.md "wikilink")。地圖裡馬來半島上有條由北向南的河流，其中由東向西的分流被多位史學家相信即是現今的。

在當今[森美蘭州](../Page/森美蘭州.md "wikilink")的，麻河的上游緊臨著馬來半島上由西向東流的大河——[彭亨河的二級支流斯汀河](../Page/彭亨河.md "wikilink")（Sungai
Serting）上游。商人們由[馬六甲海峽途徑麻河流域](../Page/馬六甲海峽.md "wikilink")、馬口、彭亨河流域前往[南中國海的西海岸](../Page/南中國海.md "wikilink")。此通道被後人稱作“彭麻古商道”。在[馬來西亞國家博物館展出的越南](../Page/馬來西亞國家博物館.md "wikilink")[東山文化的東山古鐘也是在麻河旁的班萃小鎮找到的](../Page/東山文化.md "wikilink")，越南文化的歷史產物出現在麻河旁，大大加深了彭麻古商道的說法\[1\]。

這種種的記載也指向了，麻坡早在公元前的年代已經有人在這裡居住了。

到了公元689年，位於印尼[巨港的](../Page/巨港.md "wikilink")[室利佛逝王國崛起](../Page/室利佛逝.md "wikilink")。馬來半島西海岸的大半部分都是該王國的領土。馬六甲海峽被室利佛逝佔領了接近兩百五十年（775-1006年）。海峽上的一個著名的港口被當時的人們稱作Coli或Kole，也就是現今的麻坡。直至後期取代室利佛逝的[滿者伯夷王國](../Page/滿者伯夷.md "wikilink")，麻坡也是該王朝在馬來半島的據點之一。

麻坡見證著馬來半島最輝煌的年代即[馬六甲王朝的興起及滅亡](../Page/馬六甲王朝.md "wikilink")。馬六甲王朝開國皇帝[拜里米蘇拉在還沒到](../Page/拜里米蘇拉.md "wikilink")[馬六甲立國時](../Page/馬六甲.md "wikilink")，已在麻坡住了六年。在馬六甲王朝，皇室人員視麻坡為一處皇家重地。位於麻坡市區東北處的老港（Kampung
Raja）就有著馬六甲第七任[蘇丹阿拉烏丁利亞沙的皇陵](../Page/蘇丹阿拉烏丁利亞沙.md "wikilink")。

### 歐洲人入侵

當馬六甲因葡萄牙入侵而淪陷後，[蘇丹馬末沙退守巴莪等待時機反攻馬六甲](../Page/蘇丹馬末沙.md "wikilink")。當時蘇丹馬末沙在文打煙建起了防禦木堡壘防禦葡萄牙的進攻。1511年尾在葡萄牙的猛烈進攻下，蘇丹被迫沿色丁河撤退至[彭亨州](../Page/彭亨州.md "wikilink")。到了1512年才在汉纳汀（Hang
Nadim）海軍司令的幫助下奪回巴莪。接著，蘇丹馬末沙又在1512、1519和1520年分別與葡萄牙發生了幾場戰爭，史稱“麻坡－巴莪之戰”（Perang
Muar-Pagor）。但最終還是失敗了，蘇丹馬末沙撤退至Bentan，於1528年在今印尼的[甘巴](../Page/甘巴.md "wikilink")（Kampar）駕崩。蘇丹的王子[阿拉烏丁二世逃往柔佛南部開創新的](../Page/阿拉烏丁二世.md "wikilink")[柔佛王朝](../Page/柔佛王朝.md "wikilink")。

正當西方帝國主義在馬來半島抬頭時，麻坡也因地理位置非常有戰略性而被殖民國重視。葡萄牙統治馬來亞時所建築的不只是馬六甲的古[法摩沙堡](../Page/法摩沙堡.md "wikilink")（A-Fomosa）。更有史書記載著葡萄牙也在麻坡建設了一座堡壘即“Fortaleza
De Muar”。可惜的是當[亞齊蘇丹國佔領麻坡時摧毀了](../Page/亞齊蘇丹國.md "wikilink")。

到了荷蘭時代，柔佛王朝因為與荷蘭政府的關係良好，讓柔佛政權可以在一連串與葡萄牙的戰爭傷害中得到喘息。柔佛王朝開始大肆發展。但是好景不長，1782年荷蘭因為越界收取稅務激怒了當時的柔佛苏丹拉惹哈芝（Raja
Haji）。以致拉惹哈芝以麻坡為基地進攻了馬六甲的荷蘭政權，但是最終還是失敗了，拉惹哈芝戰死，其部下也相續退守至[廖內](../Page/廖內.md "wikilink")。

麻坡也是柔佛天猛公王朝統一柔佛最後的城市。1855年，柔佛宰相王朝的版圖經過一次談判後大幅縮小至只剩下麻坡。事情源自於[蘇丹阿里](../Page/蘇丹阿里.md "wikilink")（[蘇丹胡先的繼任者](../Page/蘇丹胡先.md "wikilink")）與勢力龐大的新加坡英國人簽署條約，同意將麻坡以外的柔佛統治權拱手交予英國政府支持的[天猛公阿都拉曼之子](../Page/天猛公阿都拉曼.md "wikilink")[達因依布拉欣](../Page/達因依布拉欣.md "wikilink")（Daeng
Ibrahim），而柔佛蘇丹阿里本身的統治僅限於麻坡地區。1866年，達因依布拉欣之子[蘇丹阿布·巴卡正式登基為柔佛天猛公王朝第一任蘇丹](../Page/蘇丹阿布·巴卡.md "wikilink")，並於1877年蘇丹阿里駕崩後收回麻坡的統治權。麻坡才正式加入天猛公王朝。“統一”後，柔佛[蘇丹阿布·巴卡於](../Page/蘇丹阿布·巴卡.md "wikilink")1885年正式大興土木的發展麻坡，現今的市區面貌也在當時有了雛形。也因為柔佛蘇丹的[港主制度](../Page/港主制度.md "wikilink")，大量的[華人從](../Page/華人.md "wikilink")[中國南來麻坡開始討生活](../Page/中國.md "wikilink")。

早期華人多從事[甘密及](../Page/甘密.md "wikilink")[胡椒種植](../Page/胡椒.md "wikilink")，港主制因甘密种植的发达而产生，麻坡市也因种植甘蜜而崛起。坐落在麻河畔的麻坡市，是由港主[林东连所开辟的](../Page/林东连.md "wikilink")。那时华人在麻坡经济的开拓扮演着举足轻重的角色。除了甘密，其他商业也因此渐渐发达，河岸也林立了许多商店，其中一些街道也有其特产，比如海产街、面线街、打瓷街等等。1887年8月10日，柔佛蘇丹阿布峇卡帶著[苏丹-{后}-法蒂玛](../Page/苏丹后法蒂玛.md "wikilink")（Sultanah
Fatimah）乘搭著名為Pantie的船來到了麻坡，在儀式上，蘇丹命麻坡市區為香妃城以紀念當時的蘇丹-{后}-法蒂瑪。

### 20世紀迄今

[Muar_Municipal_Council.JPG](https://zh.wikipedia.org/wiki/File:Muar_Municipal_Council.JPG "fig:Muar_Municipal_Council.JPG")
到了第二次世界大戰時期，[日本因](../Page/日本.md "wikilink")[七七盧溝橋事件開始侵犯中國](../Page/七七盧溝橋事件.md "wikilink")。麻坡華社當時也不落人後，大量的捐出物質及錢財幫助中國對抗日軍的入侵。捐款的數量驚人以致麻坡成為籌募救國基金的模範區。麻坡華社的救國精神，終於激怒了日軍，以致日本在佔領麻坡時，屠殺了眾多的華社領袖。

20世紀初的麻坡無論在人口、經濟、文化、工業上的地位都是排在柔佛第一。早期有許多名人到訪過麻坡，如[中華民國國父](../Page/中華民國.md "wikilink")[孫中山於](../Page/孫中山.md "wikilink")1905年6月、1906年7月和1908年7月三度到訪麻坡\[2\]。1913年[諾貝爾文學獎得獎人兼亞洲首位](../Page/諾貝爾文學獎.md "wikilink")[諾貝爾獎得獎人](../Page/諾貝爾獎.md "wikilink")，印度詩聖[泰戈爾於](../Page/泰戈爾.md "wikilink")1927年7月28日蒞臨麻坡\[3\]。

麻坡有许多二战前建筑物，如麻坡[漳泉公会现址](../Page/漳泉公会.md "wikilink")、政府大厦、麻坡市议会大厦、关税大厦（前身为火车站）、麻法庭、南亭寺（善才爷庙）、[麻坡高级中学以及许多旧店屋](../Page/麻坡高级中学.md "wikilink")。开埠以来，麻坡曾经历两次大浩劫：一次是1914年，麻市大部分店屋被大火吞噬；另一次二战时日军在这里展开的大屠杀，使许多人流离失所。

2006年12月19日，一场突如其来的暴风雨淹没了[柔佛州许多主要地区](../Page/柔佛州.md "wikilink")，其中包括麻坡。灾情较严重的区域有[巴莪](../Page/巴莪.md "wikilink")、[班卒以及当时仍属麻坡的](../Page/班卒.md "wikilink")[东甲县等地区](../Page/东甲县.md "wikilink")。麻坡大約两万两千人被疏散到安全地区，许多学校，民众会堂和活动中心都成为救灾中心。接踵而来的严重大制水几乎让整个麻坡的清洁水源都断了，持续了五天直到[圣诞节前夕](../Page/圣诞节.md "wikilink")，还没见有水源供应。大部分地区，人们见不到政府任何的食水派送，只能等待雨水的降临来获得食水。

2012年，柔佛現任[蘇丹依布拉欣·依斯邁冊封麻坡為](../Page/蘇丹依布拉欣·依斯邁.md "wikilink")“皇城”。蘇丹也經常來訪麻坡，甚至舉辦過多項皇室慶典，如開齋節開放門戶，華誕慶典。這是麻坡繼十九世紀被封為“香妃城”後獲得的第二個皇家稱號。麻坡目前的稱號為“香妃皇城”。

## 人口

麻坡市的人口是由51.5%[華人](../Page/華人.md "wikilink")、36%[馬來人](../Page/馬來人.md "wikilink")、8.5%[印度](../Page/印度.md "wikilink")[淡米爾人和](../Page/淡米爾人.md "wikilink")4%[印度](../Page/印度.md "wikilink")[錫克人構成](../Page/錫克人.md "wikilink")。人口當中，除了[馬來人多數居住在周邊鄉村](../Page/馬來人.md "wikilink")，其餘族群多數住在城裡。

在麻坡市区以华人居多；其中[福建人占大多数](../Page/閩南民系.md "wikilink")，占华人人口总数50%。[潮州人其次](../Page/潮州人.md "wikilink")，大约佔20-25%。\[4\]
麻坡印裔多在麻市经营小生意。

### 语言

麻坡华人年轻一代多数用[华语沟通](../Page/馬來西亞華語.md "wikilink")，而年长一辈则用各自方言。其中多数使用福建话的人居多。马来人与印裔则使用各自语言：马来语、淡米尔語和旁遮普語，还有少许使用英文者。

### 宗教与文化

麻坡华人多数信仰[佛教或](../Page/佛教.md "wikilink")[道教](../Page/道教.md "wikilink")，也有少些[基督新教与](../Page/基督新教.md "wikilink")[天主教信徒](../Page/天主教.md "wikilink")。马来人信奉[回教](../Page/回教.md "wikilink")，印裔则奉[印度教](../Page/印度教.md "wikilink")、[錫克教或](../Page/錫克教.md "wikilink")[基督教](../Page/基督教.md "wikilink")。

麻坡文教团体林立，宗乡会馆也不少。其中有[福建会馆](../Page/福建会馆.md "wikilink")、[永春会馆](../Page/永春会馆.md "wikilink")、[潮州会馆](../Page/潮州会馆.md "wikilink")、[广东会馆](../Page/广东会馆.md "wikilink")、[廣西會館](../Page/廣西會館.md "wikilink")、[客家公會](../Page/客家公會.md "wikilink")、[雷州會館等](../Page/雷州會館.md "wikilink")。[麻坡关圣宫龙狮团连夺二十次世界醒狮赛冠军](../Page/麻坡关圣宫龙狮团.md "wikilink")。而麻坡舞龙公会（前称野野街小龙队）及中华国术进社舞龙队也颇有名气。

## 交通與公共設施

除了聯邦大道五號公路貫穿麻坡市中心，麻坡皆可經馬來半島西海岸高速公路[南北大道的東甲](../Page/南北大道.md "wikilink")（235出口）、武吉甘蜜（236出口）和巴莪（238出口）抵達。

橫跨麻河的蘇丹依斯邁大橋是麻坡的重要地標，麻橋始建於1962年，1967年4月15日正式通車。新的麻坡第二大橋連接北部的巴力文莪、南部的山芭湖和全長13.1公里的麻坡環市公路，於2005年10月28日正式通車。這座大橋是麻坡最新的地標。

麻坡还有一个码头，可由此搭船前往[印尼](../Page/印尼.md "wikilink")。

在1889至1925年，麻坡曾经有一列火车川行[苏来曼路和](../Page/苏来曼路.md "wikilink")[巴冬](../Page/巴冬.md "wikilink")，后来更衔接[双溪峇廊和](../Page/双溪峇廊.md "wikilink")[双溪蒲莱](../Page/双溪蒲莱.md "wikilink")，铁道全长22.5公里。也因此现今的苏来曼路又长又直，又被称为火城路。

现今麻坡市有六条主要街道：大马路（海乾街）、二马路、三马路、四马路、五马路以及六马路、这六条马路是由战前建筑物组成，道路四四方方似九宫格般非常整齐。如从马来西亚南北部欲往麻坡，可通过[南北大道进入](../Page/南北大道.md "wikilink")[东甲或](../Page/东甲.md "wikilink")[巴莪](../Page/巴莪.md "wikilink")。除此之外，从北可通过[马六甲市经联邦道路进入麻坡](../Page/马六甲.md "wikilink")，途经麻坡大桥。从南部则可从永平或[峇株巴辖市前往](../Page/峇株巴辖.md "wikilink")。

麻坡是早期柔佛最為重要的貿易與行政都市，為當時的殖民地地主英國人和舊柔佛王朝作出了貢獻，也塑造了該鎮的景觀。

黄金丹绒（Tanjung Emas）靠近海边与麻河间，是市区内的主要公园。可以步行到猴子园而前方有儿童游戏设施与运动设施。

麻市有两座[巴刹](../Page/巴刹.md "wikilink")，大巴刹位于大马路，另一个则在四马路。

## 经济

[wetex.jpg](https://zh.wikipedia.org/wiki/File:wetex.jpg "fig:wetex.jpg")
[giantmuar.jpg](https://zh.wikipedia.org/wiki/File:giantmuar.jpg "fig:giantmuar.jpg")

### 经济中心

  - 麻坡贸易中心（Muar Trade Centre，于2006年成立，该中心内有小食店，银行，旅馆等。

### 购物中心

  - 威德百利广场——里面有环球购物中心，还有许多小型商店、小贩中心、游戏中心及一间麦当劳。
  - ASTAKA购物中心——座落在麻坡大巴刹楼上,一所中型百货。
  - 宜康省——于2006年9月开幕，座落于Jalan Haji Jaib。
  - 巨人霸市——于2007年7月开幕，座落于麻桥河畔，风景宜人。
  - 同发霸级市场（TF）——于2015年开幕，是市内最大的市场。
  - 峇吉里宜康省- 於2018年六月位於峇吉里路開幕

### 外资工厂

  - ST Microelectronics
  - Pioneer

## 美食

[otak1.jpg](https://zh.wikipedia.org/wiki/File:otak1.jpg "fig:otak1.jpg")
[Prawn_mee.jpg](https://zh.wikipedia.org/wiki/File:Prawn_mee.jpg "fig:Prawn_mee.jpg")
麻坡四马路也被称为「贪食街」，是麻坡历史性的饮食街道。那里有乌打、沙爹、蚝煎、卤鸭、五香和许多的道地美食和糕点。另一个著名小饭中心是文打烟小贩中心，同样可找到以上美食。

麻坡著名美食包括：

  - [肉骨茶](../Page/肉骨茶.md "wikilink")
  - [油条](../Page/油条.md "wikilink")
  - [乌打](../Page/乌打.md "wikilink")
  - [蚝煎](../Page/蚝煎.md "wikilink")
  - [烧鱼](../Page/烧鱼.md "wikilink")
  - [海南鸡饭球](../Page/海南鸡饭球.md "wikilink")
  - [沙爹](../Page/沙爹.md "wikilink")
  - [虾面](../Page/虾面.md "wikilink")：麻坡的虾面有别于[马来西亚其它地方](../Page/马来西亚.md "wikilink")，虾面是炒的。其它地方除非有特别注明是炒虾面，一般都是汤的。
  - 源珍香肉干
  - 荣成饼家
  - 芳盛面包西菓
  - [马来西亚菜](../Page/马来西亚菜.md "wikilink")：由于多元种族关系，麻坡也不乏异族美食，如[爪哇面](../Page/爪哇面.md "wikilink")（Mee
    Rebus）、饭团 Lontong，椰浆饭（Nasi Lemak）等等
  - 烧肉饭
  - 巴冬啊叁鱼

## 旅游景点

[nantingshi.jpg](https://zh.wikipedia.org/wiki/File:nantingshi.jpg "fig:nantingshi.jpg")

  - 黄金丹绒——位于麻河边，环境优美。周末有许多人在此垂釣，是休闲好去处。在此可乘船游览麻河美景，一睹麻市风采。
  - 苏丹阿拉丁坟墓——坐落于巴莪，是马六甲皇朝苏丹的坟墓。
  - 壁画 -- 位于麻坡市区，最有名的壁画就是“两姐妹”的壁画。
  - 东湖侏罗纪花园 -- 免费景点，是亲子游的首选，可以来这里和恐龙拍拍照。

## 教育

麻坡有完善的教育体系，居民可在此完成从小学直到中学的学业。以下是一些学校：

### 小学

麻坡是华人人口聚集的城市，因此有多间华文小学。这些华小扮演重要的角色，栽培华社的更具有素质的华人学子。以下是一些华文小学：

  - 辅南小学（峇吉里吧口新村）
  - 中化一小A校 和 中化一小B 校
  - [中化二小](../Page/中化二小.md "wikilink") A和B 校
  - 中化三小
  - 中华基小
  - 善才小学
  - 育英小学
  - 培养小学
  - 启新小学
  - 培华小学
  - 中华小学(实廊)
  - 爱华小学(班卒)
  - 训蒙小学（芭莪）
  - 育侨小学
  - 岭章小学
  - 训正小学
  - 培才小学
  - 醒华小学
  - 育人小学(麻坡武吉巴西)
  - 岭嘉小学

### 中学

[2](File:Highschoolmuar.jpg%7Cright%7Cthumb%7C麻坡高级中学)\]

  - [中化中学](../Page/中化中学.md "wikilink")<small>*（Chung Hwa High
    School）*</small>
  - [麻坡高级中学](../Page/麻坡高级中学.md "wikilink")<small>*（Muar High
    School）*</small>
  - 康文女中<small>*（SMK Convent Muar）*</small>
  - 苏丹阿步峇卡女中<small>*（SMK Sultan Abdul Bakar）*</small>
  - 峇吉里吧口敦伊斯迈医生国中<small>*（SMK Tun Dr Ismail,Bukit Bakri）*</small>
  - 巴莪沙惜国中<small>*（SMK Sultan Alauddin Riayat Shah 1, Pagoh）*</small>
  - 敦马末国中<small>*（Tun Mamat Sacodary School）*</small>
  - 圣安德烈国中<small>*（SMK St. Andrew）*</small>
  - 拿督斯里阿玛国中<small>*（SMK Dato'Sri Amar Diraja）*</small>
  - <small>*（SMK Tun Sulaiman Ninam Shah）*</small>
  - 斯里麻国中<small>*（SMK Sri Muar）*</small>
  - 东姑马哥打国中<small>*（SMK Tengku Mahkota）*</small>
  - 巴力文莪国中<small>*（SMK Parit Bunga）*</small>
  - 新加望国中<small>*（SMK Sungai Abong）*</small>
  - 香妃城国中<small>*（SMK Bandar Maharani）*</small>
  - 敦霹雳国中<small>*（SMK Tun Perak）*</small>

## 收视覆盖率

鉴于麻坡地理的优势，能够收看得到鄰国[印尼和](../Page/印尼.md "wikilink")[新加坡的电视台](../Page/新加坡.md "wikilink")，但无法收听得到电台。

[印尼和](../Page/印尼.md "wikilink")[新加坡的电视台在日间的收视率极差](../Page/新加坡.md "wikilink")
; 而夜间，因电波反射的缘故，收视率有部分清澈。若在无云的夜间下，收视画面可达零干扰。

### 中英电台

  - 以下为本地较多人收听的电台

| 电台名称      | 电台频率      | 官方网站                            | 详情       |
| --------- | --------- | ------------------------------- | -------- |
| One FM    | 88.1 MHz  | [网址](http://www.onefm.com.my/)  | 本地首要媒体   |
| 988 FM    | 98.2 MHz  | [网址](http://www.988.com.my/)    | 本地新报集团   |
| Ai FM/爱FM | 100.4 MHz | [网址](http://aifm.rtm.gov.my/)   | 本地国营     |
| MY FM     | 106.4 MHz | [网址](http://www.my.com.my/)     | 本地 Astro |
| Melody FM | 107.3 MHz | [网址](http://www.melody.com.my/) | 本地 Astro |

| 电台名称    | 电台频率     | 官方网站                                                                     | 所属国家/集团  |
| ------- | -------- | ------------------------------------------------------------------------ | -------- |
| Hitz.FM | 93.0 MHz | [网址](http://www.hitz.fm/)                                                | 本地 Astro |
| Fly FM  | 94.0 MHz | [网址](http://www.flyfm.com.my/)                                           | 本地首要媒体   |
| Mix FM  | 91.1 MHz | [网址](http://www.mix.fm/)                                                 | 本地 Astro |
| Lite FM | 92.2 MHz | [网址](https://web.archive.org/web/20140826120820/http://www.lite.com.my/) | 本地 Astro |

### 电视台

  - 以下频率只限在麻坡市区, 其他视地区而定

| 电视名称              | 电视频率       | 官方网站                                  | 详情          |
| ----------------- | ---------- | ------------------------------------- | ----------- |
| TV1               | 182.25 MHz | [网址](http://tv1.rtm.gov.my/)          | 国语/本地国营     |
| TV2               | 189.30 MHz | [网址](http://tv2.rtm.gov.my/)          | 国,华,印语/本地国营 |
| TV3               | 100.4 MHz  | [网址](http://www.tv3.com.my/)          | 国语/本地首要媒体   |
| 8TV八度空间           | 224.35 MHz | [网址](http://www.8tv.com.my/)          | 华语/本地首要媒体   |
| 5频道/Ch5           | 687.40 MHz | [网址](http://www.toggle.sg/ch5)        | 英语/新加坡新传媒   |
| Okto              | 000 MHz    | [网址](http://www.toggle.sg/okto)       | 英语/新加坡新传媒   |
| NTV7              | 000 MHz    | [网址](http://www.ntv7.com.my/)         | 华语/本地首要媒体   |
| 8频道/Ch8           | 000 MHz    | [网址](http://www.toggle.sg/ch8)        | 华语/新加坡新传媒   |
| TV9               | 000 MHz    | [网址](http://www.tv9.com.my/)          | 国语/本地首要媒体   |
| 亚洲新闻台/Ch NewsAsia | 000 MHz    | [网址](http://www.channelnewsasia.com/) | 新加坡新传媒      |
| SCTV              | 000 MHz    | [网址](http://www.sctv.co.id/)          | 印尼语/印尼 SCTV |

## 著名人物

  - [拿督](../Page/拿督.md "wikilink")[賽阿都卡迪爾](../Page/賽阿都卡迪爾.md "wikilink")－[柔佛州第](../Page/柔佛州.md "wikilink")8任州务大臣（1952/02/20
    - 1955/06/05）
  - [拿督](../Page/拿督.md "wikilink")[旺依德利斯](../Page/旺依德利斯.md "wikilink")－[柔佛州第](../Page/柔佛州.md "wikilink")9任州务大臣（1955/10/01
    - 1959/06/16）
  - [丹斯里](../Page/丹斯里.md "wikilink")[哈山·尤諾斯](../Page/哈山·尤諾斯.md "wikilink")－[柔佛州第](../Page/柔佛州.md "wikilink")10任州务大臣（1959/06/27
    - 1967/01/31）
  - [丹斯里](../Page/丹斯里.md "wikilink")[奧斯曼·沙阿](../Page/奧斯曼·沙阿.md "wikilink")－[柔佛州第](../Page/柔佛州.md "wikilink")11任州務大臣（1967/02/01
    - 1982/04/29）
  - [丹斯里](../Page/丹斯里.md "wikilink")[慕尤丁](../Page/慕尤丁.md "wikilink")－[柔佛州第](../Page/柔佛州.md "wikilink")13任州務大臣（1986/08/12
    - 1995/05/03）、[馬來西亞第](../Page/馬來西亞.md "wikilink")10任副首相（2009/04/10 -
    2015/07/28）

<!-- end list -->

  - [拿督斯里](../Page/拿督斯里.md "wikilink")[卡立諾丁](../Page/卡立諾丁.md "wikilink")－[柔佛州第](../Page/柔佛州.md "wikilink")15任州務大臣（2013/05/08
    - 現在）
  - [丹斯里](../Page/丹斯里.md "wikilink")－[馬來西亞首任](../Page/馬來西亞.md "wikilink")[下議院議長](../Page/下議院議長.md "wikilink")、[阿都拉萨和](../Page/阿都拉萨.md "wikilink")[胡先翁的岳父](../Page/胡先翁.md "wikilink")、[纳吉的外公](../Page/纳吉·阿都拉萨.md "wikilink")。
  - [敦](../Page/敦_\(头衔\).md "wikilink")－[阿都拉萨之妻](../Page/阿都拉萨.md "wikilink")、[纳吉的母親](../Page/纳吉·阿都拉萨.md "wikilink")、莫哈末諾之女。
  - [敦](../Page/敦_\(头衔\).md "wikilink")－[胡先翁之妻](../Page/胡先翁.md "wikilink")、[希山慕丁之母親](../Page/希山慕丁.md "wikilink")、莫哈末諾之女。
  - [敦](../Page/敦_\(头衔\).md "wikilink")[阿旺·哈山](../Page/阿旺·哈山.md "wikilink")－[檳城州第](../Page/檳城州.md "wikilink")5任[州元首](../Page/州元首.md "wikilink")
  - [敦](../Page/敦_\(头衔\).md "wikilink")－前國會上議院副議長、[巫統大會主席](../Page/巫統.md "wikilink")(1978-2003)
  - [拿督](../Page/拿督.md "wikilink")[梁维泮](../Page/梁维泮.md "wikilink")－前[馬來西亞](../Page/馬來西亞.md "wikilink")[房屋及地方政府部长](../Page/房屋及地方政府部长.md "wikilink")、前[馬華副總會長](../Page/馬華.md "wikilink")、[馬華代總會長](../Page/馬華.md "wikilink")
  - [拿督](../Page/拿督.md "wikilink")[蔡銳明](../Page/蔡銳明.md "wikilink")－前[馬來西亞](../Page/馬來西亞.md "wikilink")[衛生部長](../Page/衛生部長.md "wikilink")、前[馬華副總會長](../Page/馬華.md "wikilink")、前[人民公正黨副主席](../Page/人民公正黨.md "wikilink")
  - [敦](../Page/敦_\(头衔\).md "wikilink")－前[馬來西亞](../Page/馬來西亞.md "wikilink")[社會福利部部長](../Page/社會福利部.md "wikilink")、[新聞及廣播部部長](../Page/新聞及廣播部.md "wikilink")、前[巫統婦女組主席](../Page/巫統婦女組.md "wikilink")，亦是[馬來西亞首位女性部長](../Page/馬來西亞.md "wikilink")
  - [雅丝敏·阿莫](../Page/雅丝敏·阿莫.md "wikilink")－已故著名导演及制作人
  - [郭清江](../Page/郭清江.md "wikilink") —
    《[星洲日報](../Page/星洲日報.md "wikilink")》總編輯
  - [許國偉](../Page/許國偉.md "wikilink") —
    《[南洋商报](../Page/南洋商报.md "wikilink")》新闻编辑、前988電台主持人、著名時事評論員
  - [李万千](../Page/李万千.md "wikilink") — 著名時事評論員、前左翼運動之星、華教鬥士
  - [拿督](../Page/拿督.md "wikilink")[蔡兆源](../Page/蔡兆源.md "wikilink")－[馬來西亞著名](../Page/馬來西亞.md "wikilink")[經濟學家](../Page/經濟學家.md "wikilink")、[商人](../Page/商人.md "wikilink")，馬來西亞和新加坡[漢堡王](../Page/漢堡王.md "wikilink")([Burger
    King](../Page/Burger_King.md "wikilink"))的擁有者。
  - [張鍾霖](../Page/張鍾霖.md "wikilink") —
    [马来西亚国营电视台TV](../Page/马来西亚国营电视台.md "wikilink")2华语新闻主播、制作人
  - [莊文傑](../Page/莊文傑.md "wikilink") —
    [Astro](../Page/Astro頻道.md "wikilink")[AEC頻道－華語新聞主播](../Page/AEC頻道－.md "wikilink")
  - [娜娜·馬哈贊](../Page/娜娜·馬哈贊.md "wikilink") — XFM電台主持人、電視節目主持人，演員，歌手
  - [蔡沁凯](../Page/蔡沁凯.md "wikilink") —
    [新傳媒八頻道](../Page/新傳媒.md "wikilink")《獅城有約》主持人兼製作人
  - [黄明志](../Page/黄明志.md "wikilink")－网络歌手，音乐人，导演，制作人
  - [叶锦福](../Page/叶锦福.md "wikilink")－前国家羽球队队员，与谢顺吉搭档，多次获得国际羽球赛事的冠、亚军，包括1996年为马来西亚夺下奥运史上的首枚银牌
  - [凌霄](../Page/凌霄.md "wikilink")－资深歌手、演员，已移民新加坡
  - [何蔚庭](../Page/何蔚庭.md "wikilink")－旅居台湾的著名电影导演，2005年凭短片《呼吸》夺得[坎城影展国际影评人周两个奖](../Page/坎城影展.md "wikilink")，及凭长片《[台北星期天](../Page/台北星期天.md "wikilink")》夺得2010年[金马奖最佳新导演](../Page/金马奖.md "wikilink")
  - [范俊福](../Page/范俊福.md "wikilink")－已故唱片制作人、词曲作者，是马来西亚首位将本地福建歌曲创作和歌手带往台湾发片的音乐人
  - [顏慧萍](../Page/顏慧萍.md "wikilink")－2011年[Astro新秀大賽獲得冠軍](../Page/Astro新秀大賽.md "wikilink")，第一屆[華人星光大道第五名](../Page/華人星光大道.md "wikilink")。
  - [蔡佩璇](../Page/蔡佩璇.md "wikilink")－马来西亚著名演员

## 影视

  - 2011年 [正义武馆](../Page/正义武馆.md "wikilink")-在永春会馆取景
  - 2012年 [半边天](../Page/半边天.md "wikilink")
  - 2012年 [猪仔馆人家](../Page/猪仔馆人家.md "wikilink")-在位于黄金丹绒的一间旧洋房
  - 2013年 大舞獅-關聖宮-麻坡關聖宮,麻坡大马路等

## 世界第一

[麻坡關聖宮龍獅團曾獲得过](../Page/麻坡關聖宮龍獅團.md "wikilink")36次世界冠軍，45次國內冠軍。
在[雲頂世界獅王爭霸賽从第](../Page/雲頂世界獅王爭霸賽.md "wikilink")1届到第9届冠军,
都由关圣宫获得。

## 姐妹城市

  - [巴生市](../Page/巴生市.md "wikilink")

  - [安顺](../Page/安顺_\(马来西亚\).md "wikilink")

## 照片集

<File:muarbuilding.jpg>| <File:muarbulding1.jpg>|
<File:muaroldshop.jpg>| <File:muarbulding3.jpg>|
<File:muaroldshop3.jpg>| <File:muaroldshop2.jpg>| <File:Zhang>
Quan.jpg|漳泉公会 <File:Maur> Government.jpg|政府大厦
[File:Majli_Perbandaran,_Muar.jpg|麻坡市议会](File:Majli_Perbandaran,_Muar.jpg%7C麻坡市议会)
[File:custommuar.jpg|关税局](File:custommuar.jpg%7C关税局)
[File:highcourtmuar.jpg|法庭](File:highcourtmuar.jpg%7C法庭)
<File:Jlnabdullah.jpg>| 麻坡二马路（布綻街）

## 参见

  - [麻坡县](../Page/麻坡县.md "wikilink")

## 参考文献

### 引用

<div class="references-small">

<references>

</references>

</div>

### 书籍

  - 麦留芳, *方言群认同 - 早期星马华人的分类法厠*, 1987 中央研究院民族学研究所印行 (Institute of
    Ethnology Academia Sinica, Monograph Series B, No. 14)

## 外部链接

  - [麻驿-麻坡旅游资讯站(中文)](https://www.facebook.com/muaryi)（脸书）
  - [麻坡论坛(中文)](http://www.muartalks.com)
  - [麻坡地图(英文)](http://www.malaxi.com/map_jb_muar.html)
  - [麻坡网(中/英文)](http://www.muar.net)
  - [麻坡人网(巫文)](http://menu.blogdrive.com)
  - [麻坡气象資料(英文)](http://www.streetdirectory.com/travel/weather/malaysia/muar.php)（天气）
  - [2012年麻坡旅游地图](http://www.chrislee.my/muar-map/muar-map.html)（地图）
  - [绿化麻坡](https://www.facebook.com/muargreen?ref=ts)（脸书）
  - [麻坡的华语](https://www.youtube.com/watch?v=nmmjPbYU1og)（YouTube）
  - [麻坡一日游游记(中文)](http://www.xinaday.com/muar-1-day-trip/)
  - [麻坡條路附上典故從路牌看歷史](http://www.chinapress.com.my/20141201/麻坡條路附上典故從路牌看歷史/)

[\*](../Category/麻坡县.md "wikilink")
[Category:马来西亚市镇](../Category/马来西亚市镇.md "wikilink")

1.  [1](http://www.muziumnegara.gov.my/gallery/items/Loceng_Dong_Son_34/)|Loceng
    Dong Son ditemui di Kampung Pencu, Muar.
2.  <http://www.sinchew.com.my/node/1236920>
3.  <http://www.sinchew.com.my/node/243454>
4.  麦留芳 (1987), "表3.19， 柔佛州内的中国方言分布"，p.89