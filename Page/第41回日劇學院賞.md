[←第40回](../Page/第40回日劇學院賞名單.md "wikilink") - **第41回** -
[第42回→](../Page/第42回日劇學院賞名單.md "wikilink")

**第41回日劇學院賞**，針對2004年4月到6月在[日本播出的連續劇做出投票與評比](../Page/日本.md "wikilink")。

## 得獎名單

### 最優秀作品賞

1.  **[與光同行](../Page/與光同行.md "wikilink")**
2.  [Orange Days](../Page/Orange_Days.md "wikilink")
3.  [老公當家](../Page/老公當家.md "wikilink")

<!-- end list -->

  - 讀者票：1.[HOME
    DRAMA](../Page/HOME_DRAMA.md "wikilink")；2.[讓愛看得見](../Page/讓愛看得見.md "wikilink")；3.Orange
    Days
  - 記者票：1.與光同行 ；2.Orange Days；3.老公當家
  - 評審票：1.與光同行 ；2.Orange Days；3.老公當家

### 主演男優賞

1.  **[妻夫木聰](../Page/妻夫木聰.md "wikilink")**（[Orange
    Days](../Page/Orange_Days.md "wikilink")）
2.  [阿部寬](../Page/阿部寬.md "wikilink")（[老公當家](../Page/老公當家.md "wikilink")）
3.  [藤木直人](../Page/藤木直人.md "wikilink")（[讓愛看得見](../Page/讓愛看得見.md "wikilink")）

<!-- end list -->

  - 讀者票：1.[堂本剛](../Page/堂本剛.md "wikilink")（HOME
    DRAMA）；2.藤木直人（讓愛看得見）；3.妻夫木聰（Orange
    Days）
  - 記者票：1.阿部寬（老公當家） ；2.妻夫木聰（Orange Days）；3.藤木直人（讓愛看得見）
  - 評審票：1.阿部寬（老公當家） ；2.妻夫木聰（Orange Days）；3.宮迫博之（老公當家）

### 主演女優賞

1.  **[篠原涼子](../Page/篠原涼子.md "wikilink")**（[與光同行](../Page/與光同行.md "wikilink")）
2.  [菅野美穗](../Page/菅野美穗.md "wikilink")（[讓愛看得見](../Page/讓愛看得見.md "wikilink")）
3.  [天海祐希](../Page/天海祐希.md "wikilink")（[離婚女律師](../Page/離婚女律師.md "wikilink")）

<!-- end list -->

  - 讀者票：1.菅野美穗（讓愛看得見）；2.篠原涼子（與光同行）；3.[安倍夏美](../Page/安倍夏美.md "wikilink")（[小狗華爾滋](../Page/小狗華爾滋.md "wikilink")）
  - 記者票：1.篠原涼子（與光同行）；2.菅野美穗（讓愛看得見）；3.天海祐希（離婚女律師）
  - 評審票：1.篠原涼子（與光同行）；2.天海祐希（離婚女律師）；3.菅野美穗（讓愛看得見）

### 助演男優賞

1.  **[齋藤隆成](../Page/齋藤隆成.md "wikilink")**（[與光同行](../Page/與光同行.md "wikilink")）
2.  [中山裕介](../Page/中山裕介.md "wikilink")（[HOME
    DRAMA\!](../Page/HOME_DRAMA!.md "wikilink")）
3.  [泉谷茂](../Page/泉谷茂.md "wikilink")（[讓愛看得見](../Page/讓愛看得見.md "wikilink")）
4.  [成宮寬貴](../Page/成宮寬貴.md "wikilink")（[Orange
    Days](../Page/Orange_Days.md "wikilink")）

<!-- end list -->

  - 讀者票：1.中山裕介（HOME DRAMA）；2.泉谷茂（讓愛看得見）；3.田村高廣（HOME DRAMA）
  - 記者票：1.齋藤隆成（與光同行）；2.泉谷茂（讓愛看得見）；3.中山裕介（HOME DRAMA）
  - 評審票：1.成宮寬貴（Orange
    Days）；2.齋藤隆成（與光同行）；3.[岡本健一](../Page/岡本健一.md "wikilink")（小狗華爾滋）

### 助演女優賞

1.  **[小林聰美](../Page/小林聰美.md "wikilink")**（[與光同行](../Page/與光同行.md "wikilink")）
2.  [柴咲幸](../Page/柴咲幸.md "wikilink")（[Orange
    Days](../Page/Orange_Days.md "wikilink")）
3.  [篠原涼子](../Page/篠原涼子.md "wikilink")（[老公當家](../Page/老公當家.md "wikilink")）

<!-- end list -->

  - 讀者票：1.柴咲幸（Orange
    Days）；2.小林聰美（與光同行）；3.[八千草薫](../Page/八千草薫.md "wikilink")（讓愛看得見）
  - 記者票：1.柴咲幸（Orange Days）；2.小林聰美（與光同行）；3.篠原涼子（老公當家）
  - 評審票：1.小林聰美（與光同行）；2.柴咲幸（Orange Days）；3.篠原涼子（老公當家）

### 其他

  - **主題歌賞：[Mr.Children](../Page/Mr.Children.md "wikilink")：[Sign](../Page/Sign_\(Mr.Children單曲\).md "wikilink")**（[Orange
    Days](../Page/Orange_Days.md "wikilink")）
  - **新人俳優賞：[近野成美](../Page/近野成美.md "wikilink")**（[小狗華爾滋](../Page/小狗華爾滋.md "wikilink")）
  - **最佳服裝賞：[天海祐希](../Page/天海祐希.md "wikilink")**（[離婚女律師](../Page/離婚女律師.md "wikilink")）
  - **腳本賞：[北川悅吏子](../Page/北川悅吏子.md "wikilink")**（[Orange
    Days](../Page/Orange_Days.md "wikilink")）
  - **監督賞：[生野慈朗](../Page/生野慈朗.md "wikilink")、[土井裕泰](../Page/土井裕泰.md "wikilink")、[今井夏木](../Page/今井夏木.md "wikilink")**（[Orange
    Days](../Page/Orange_Days.md "wikilink")）
  - **劇中音樂賞：[藤原いくろう](../Page/藤原いくろう.md "wikilink")**（[讓愛看得見](../Page/讓愛看得見.md "wikilink")）
  - **卡司賞：[Orange Days](../Page/Orange_Days.md "wikilink")**
  - **片頭片尾賞：[生野慈朗](../Page/生野慈朗.md "wikilink")**（[Orange
    Days](../Page/Orange_Days.md "wikilink")）
  - **電視週刊特別賞：[手語指導](../Page/手語.md "wikilink")（[南留花](../Page/南留花.md "wikilink")、[佐藤由紀惠](../Page/佐藤由紀惠.md "wikilink")、[鹽谷武志](../Page/鹽谷武志.md "wikilink")）**

[41](../Category/日劇學院賞.md "wikilink")
[Category:2004年日本](../Category/2004年日本.md "wikilink")
[Category:2004年电视奖项](../Category/2004年电视奖项.md "wikilink")