[Little_Sheep.svg](https://zh.wikipedia.org/wiki/File:Little_Sheep.svg "fig:Little_Sheep.svg")
**小肥羊餐饮连锁有限公司**（簡稱**小肥羊**，港交所除牌時0968.HK）曾经是[中國最大的](../Page/中國.md "wikilink")[涮羊肉](../Page/涮羊肉.md "wikilink")[連鎖店企業](../Page/連鎖店.md "wikilink")，1999年由[張鋼及](../Page/張鋼.md "wikilink")[陳洪凱在](../Page/陳洪凱.md "wikilink")[內蒙古創辦](../Page/內蒙古.md "wikilink")，2006年在[中國大陸](../Page/中國大陸.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[香港及](../Page/香港.md "wikilink")[美國](../Page/美國.md "wikilink")、[加拿大和](../Page/加拿大.md "wikilink")[日本已開設逾](../Page/日本.md "wikilink")721間分店，2006年營業額為57.5億元[人民幣](../Page/人民幣.md "wikilink")，跻身中國最有價值[品牌](../Page/品牌.md "wikilink")500強。\[1\]

## 历史

成立于内蒙古包头市，内蒙古小肥羊餐饮连锁有限公司自1999年8月起，以经营特色火锅及特许经营为主业，兼营调味品研发及销售。2008年6月小肥羊集团有限公司（“小肥羊”）在香港上市，是中国首家在香港上市的品牌餐饮企业。

2012年2月，[百胜餐饮集团以协议计划方式私有化小肥羊的交易顺利完成](../Page/百胜餐饮集团.md "wikilink")。小肥羊从2月2日起正式在香港联交所除牌，并成为百胜餐饮集团旗下一个新的餐饮品牌。
目前，小肥羊的餐饮店面已遍布全国，而且连锁店已经进入美国、加拿大、日本、港澳等国家和地区。

## 商标

小肥羊于1999年开始向国家商标局多次提交“小肥羊”商标注册申请，均被以缺乏显著性而驳回。于此同时，国内出现成千的“小肥羊”仿冒者。2004年11月，根据新商标法，“小肥羊”被认定为驰名商标，为公司进一步维权提供了支持。2006年“小肥羊”被商标局正式认定为注册商标。小肥羊餐饮连锁有限公司为“小肥羊”商标的唯一合法使用者\[2\]。

## 分店 \[3\]

### 门店图片

HK Causeway Bay Plaza Two n New York Theatre at
night.JPG|[香港島](../Page/香港島.md "wikilink")[銅鑼灣廣場](../Page/銅鑼灣廣場.md "wikilink")2期店（已停业）
HK Mongkok Langham Place night Portland Street Little Sheep Hot Pot
Restaurant.JPG|香港[九龍](../Page/九龍.md "wikilink")[旺角](../Page/旺角.md "wikilink")[亞皆老街店](../Page/亞皆老街.md "wikilink")（已停业）
Xinhui 新會城 仁壽路 Renshou Lu 小肥羊 old
building.JPG|[广东](../Page/广东.md "wikilink")[江门](../Page/江门.md "wikilink")[新会仁壽路店](../Page/新会.md "wikilink")（已停业）
LittleSheepHouston.JPG|[休斯敦店](../Page/休斯敦.md "wikilink") Little Sheep in
Flushing,
NY.JPG|[纽约](../Page/纽约.md "wikilink")[法拉盛店](../Page/法拉盛.md "wikilink")
Little Sheep in Richmond, BC.JPG|[列治文店](../Page/列治文.md "wikilink")
Little Sheep in Metrotown,
BC.JPG|[本拿比](../Page/本拿比.md "wikilink")[铁道镇店](../Page/铁道镇.md "wikilink")
Little Sheep Coquitlam.jpg|高贵林店 Little Sheep at Chinatown,
Montreal.jpg|[蒙特利尔](../Page/蒙特利尔.md "wikilink")[唐人街店](../Page/唐人街.md "wikilink")

## 參考

<div class="references-small">

<references />

</div>

## 外部連結

  - [小肥羊官方網站](http://www.littlesheep.com)

[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:中国连锁餐厅](../Category/中国连锁餐厅.md "wikilink")
[Category:包头公司](../Category/包头公司.md "wikilink")
[Category:中华餐饮名店](../Category/中华餐饮名店.md "wikilink")
[Category:火锅品牌](../Category/火锅品牌.md "wikilink")
[Category:總部在中國的跨國公司](../Category/總部在中國的跨國公司.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:百勝餐飲集團](../Category/百勝餐飲集團.md "wikilink")
[Category:1999年成立的公司](../Category/1999年成立的公司.md "wikilink")

1.  [內蒙古日報：小肥羊公司去年實現營業收入57.5億元](http://www.nmg.xinhuanet.com/nmgdcy/yswh/2007-01/16/content_9054211.htm)
     2007-01-16
2.  [CCTV:经济与法"小肥羊"争夺战](http://law.cctv.com/20070425/101165.shtml)
    2007年4月25日
3.  [餐厅查询](http://www.littlesheep.com/ResTuarant)