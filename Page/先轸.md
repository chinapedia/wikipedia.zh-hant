**先轸**（前683年—前627年），也作**䢾轸**、**兟轸**、**\[兟阝\]轸**，[曲沃](../Page/曲沃.md "wikilink")（今[山西](../Page/山西.md "wikilink")[闻喜](../Page/闻喜.md "wikilink")）人，因采邑在[原邑](../Page/原邑.md "wikilink")（今[河南](../Page/河南.md "wikilink")[济源](../Page/济源.md "wikilink")），又称**原轸**，[晋国大夫](../Page/晋国.md "wikilink")。在[骊姬之乱时](../Page/骊姬之乱.md "wikilink")，先轸同[重耳逃走](../Page/晋文公.md "wikilink")，在外流亡十九年。先轸是中国历史上第一位同时拥有[元帅头衔和元帅战绩的军事统帅](../Page/元帅.md "wikilink")。他为人公忠耿正，因為曾與[晉襄公起衝突](../Page/晉襄公.md "wikilink")，但並未受到襄公懲罰，心中不安，故意在無武裝狀態衝入敵陣，[陣亡](../Page/陣亡.md "wikilink")。

## 簡介

前633年，晋国讨伐[曹国和](../Page/曹国.md "wikilink")[衞國](../Page/衞國.md "wikilink")，先轸为[下军佐](../Page/下军佐.md "wikilink")，占据了衞國的[五鹿](../Page/五鹿.md "wikilink")。

前632年，[中军将](../Page/中军将.md "wikilink")[郤縠去世](../Page/郤縠.md "wikilink")，先轸被超拔为中军将，率领晋军打赢[城濮之战](../Page/城濮之战.md "wikilink")。

前627年，[晋国在](../Page/晋国.md "wikilink")[殽之战打败](../Page/殽之战.md "wikilink")[秦国](../Page/秦国.md "wikilink")，生俘[秦国将军](../Page/秦国.md "wikilink")[孟明視](../Page/孟明視.md "wikilink")、[西乞术](../Page/西乞术.md "wikilink")、[白乙丙](../Page/白乙丙.md "wikilink")。[文嬴请求她儿子晋襄公释放秦国统帅](../Page/文嬴.md "wikilink")，晋襄公答应了。先轸大怒，出言不逊，直言进谏，而且当着晋襄公的面往地上吐了一口口水，這屬於[大不敬之罪](../Page/大不敬.md "wikilink")，但晋襄公自認理虧，掩面向先轸致意，並且派[阳处父追孟明视等人](../Page/阳处父.md "wikilink")，没能追上。

[狄人攻打](../Page/狄.md "wikilink")[晋国](../Page/晋国.md "wikilink")，在[箕交战](../Page/箕.md "wikilink")。先轸因为对襄公无礼却没有受到惩罚，神志烦躁，寝食不安。他决定以死谢罪，他脱下了头盔和铠甲（“免胄”）衝入敵阵，即刻战死沙场。狄人將他的[首級送还晋军](../Page/首級.md "wikilink")。

其子[先且居繼任晉中軍元帥](../Page/先且居.md "wikilink")。

## 參考書目

  - 《春秋左傳》之僖公

[Category:先姓](../Category/先姓.md "wikilink")
[X先](../Category/春秋人.md "wikilink")
[X先](../Category/前627年逝世.md "wikilink")
[Category:战争身亡者](../Category/战争身亡者.md "wikilink")
[Category:晋国军事人物](../Category/晋国军事人物.md "wikilink")