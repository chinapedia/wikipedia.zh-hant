**黃珊**（，），[香港新聞工作者](../Page/香港.md "wikilink")。現為[無綫電視新聞主播](../Page/無綫電視新聞.md "wikilink")。

## 簡歷

黃珊畢業於[保良局羅傑承（一九八三）中學](../Page/保良局羅傑承（一九八三）中學.md "wikilink")，2001年畢業於[香港浸會大學傳理學院傳播系](../Page/香港浸會大學傳理學院.md "wikilink")，主修公關及廣告。她曾任職[港龍航空空中服務員](../Page/港龍航空.md "wikilink")（2003年3月至2004年10月）\[1\]，後獲[香港有線電視聘用](../Page/香港有線電視.md "wikilink")，並與[譚結琳](../Page/譚結琳.md "wikilink")（已轉投[香港警務處](../Page/香港警務處.md "wikilink")）、[曾美華](../Page/曾美華.md "wikilink")（曾繼續效力[有線新聞](../Page/有線新聞.md "wikilink")）一起接受新聞主播訓練，實習期間被調派往[有線新聞不同時段擔任主播工作](../Page/有線新聞.md "wikilink")。她於有線電視任職不足3個月便加入[亞視新聞](../Page/亞視新聞.md "wikilink")，在[24小時亞視新聞台及簡稱](../Page/24小時亞視新聞台.md "wikilink")「aTV2」的[亞洲電視新聞財經頻道](../Page/亞洲電視新聞財經頻道.md "wikilink")（後改稱[亞洲高清台](../Page/亞洲高清台.md "wikilink")，簡稱「ATV12」）擔任[主播至](../Page/主播.md "wikilink")2010年9月14日，期間更負責為24小時亞視新聞台主持停播前最後一節新聞。

黃珊曾夥拍[胡燕泳](../Page/胡燕泳.md "wikilink")、[黃雅宇及](../Page/黃雅宇.md "wikilink")[陳興昌主持的新節目](../Page/陳興昌.md "wikilink")《[主播天下](../Page/主播天下.md "wikilink")》（2008年3月3日啟播，2010年9月14日退出），亦已於同月月尾離職，並在同年10月1日轉職至[華納音樂](../Page/華納音樂.md "wikilink")，擔任首席助理機構傳訊總經理，但於同月月中再轉職到[耀才證券擔任公關經理](../Page/耀才證券.md "wikilink")\[2\]。2008年10月1日至2008年10月2日，她曾擔任[亞洲電視本港台晚間新聞的主播](../Page/亞洲電視本港台.md "wikilink")，也主持過[亞洲電視本港台的節目](../Page/亞洲電視本港台.md "wikilink")《不老傳說》，以及[本港台](../Page/本港台.md "wikilink")、[亞洲高清台節目](../Page/亞洲高清台.md "wikilink")《[新聞830](../Page/新聞830.md "wikilink")》。

2011年3月28日至2015年10月8日，黃珊加入[鳳凰衛視香港台](../Page/鳳凰衛視香港台.md "wikilink")\[3\]，惟只工作了4年便因受鳳凰衛視裁員問題影響而轉投[無綫新聞](../Page/無綫新聞.md "wikilink")。2016年1月2日，她首度主持《[午間新聞](../Page/無綫電視午間新聞.md "wikilink")》，同年2月起正式調往[互動新聞台擔任首席主播及主持](../Page/互動新聞台.md "wikilink")[J5節目](../Page/J5.md "wikilink")《[智富之道](../Page/智富之道.md "wikilink")》，但4月20日又返回[鳳凰衛視香港台工作](../Page/鳳凰衛視香港台.md "wikilink")。直到2017年6月14日，她再重返[無綫新聞報道](../Page/無綫新聞.md "wikilink")「[格倫費爾塔火災](../Page/格倫費爾塔火災.md "wikilink")」，同年8月15日主持節目《[升學無彊界](../Page/升學無彊界.md "wikilink")》。

翌年3月7日，黃珊再度主持《[午間新聞](../Page/無綫電視午間新聞.md "wikilink")》，同年7月2日首次主持《[六點半新聞報道](../Page/六點半新聞報道.md "wikilink")》；8月18日則首次擔任《[晚間新聞](../Page/無綫電視晚間新聞.md "wikilink")》主播，暫代原主播[李卓謙](../Page/李卓謙.md "wikilink")。其後，她與[潘蔚林一起主持](../Page/潘蔚林.md "wikilink")《2018香港大事回顧》，接替原來主持[藍可盈](../Page/藍可盈.md "wikilink")。另外，她曾經接受[香港電台電視部節目](../Page/香港電台.md "wikilink")《[傳媒春秋](../Page/傳媒春秋.md "wikilink")》及[亞洲電視節目](../Page/亞洲電視.md "wikilink")《[電視風雲50年](../Page/電視風雲50年.md "wikilink")》訪問\[4\]，分享新聞從業經歷。

## 參考來源

## 外部連結

  -
  -
  -
  -
  - [Toni Wong -
    Linkedin](https://hk.linkedin.com/in/toni-wong-54432b3b)

[Shan珊](../Category/黃姓.md "wikilink")
[Category:香港記者](../Category/香港記者.md "wikilink")
[Category:香港電視主持人](../Category/香港電視主持人.md "wikilink")
[Category:香港財經界人士](../Category/香港財經界人士.md "wikilink")
[Category:無綫電視新聞報導員](../Category/無綫電視新聞報導員.md "wikilink")
[Category:前亞洲電視新聞報導員](../Category/前亞洲電視新聞報導員.md "wikilink")
[Category:前有線新聞報導員](../Category/前有線新聞報導員.md "wikilink")
[Category:保良局羅傑承（一九八三）中學校友](../Category/保良局羅傑承（一九八三）中學校友.md "wikilink")
[Category:香港浸會大學校友](../Category/香港浸會大學校友.md "wikilink")

1.  [女主播渴市
    有線秘密武器起底](https://hk.lifestyle.appledaily.com/nextplus/magazine/article/20041124/2_4457468/%E5%A5%B3-%E4%B8%BB-%E6%92%AD-%E6%B8%B4-%E5%B8%82-%E6%9C%89-%E7%B7%9A-%E7%A7%98-%E5%AF%86-%E6%AD%A6-%E5%99%A8-%E8%B5%B7-%E5%BA%95)，[壹週刊](../Page/壹週刊_\(香港\).md "wikilink")，2004年11月24日
2.  [中環在線：主播黃珊去耀才做公關 -
    李華華](https://hk.finance.appledaily.com/finance/daily/article/20101109/14641573)，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")，2010年11月9日
3.  [隔牆有耳：女主播變知客 黃珊被高幹調戲 -
    李八方](https://hk.news.appledaily.com/local/daily/article/20120111/15972515)，蘋果日報，2012年1月11日
4.  [電視風雲50年 -
    電視台新聞主播](https://web.archive.org/web/20131123132343/http://www.56.com/u28/v_MzA1MDI1Nzc.html)