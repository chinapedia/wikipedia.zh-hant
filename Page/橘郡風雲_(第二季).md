[200px-OCcastS2.jpg](https://zh.wikipedia.org/wiki/File:200px-OCcastS2.jpg "fig:200px-OCcastS2.jpg")
第二季由2004年11月播至2005年5月，共有[24集](../Page/24.md "wikilink")。季末時，故事最終發展再一次要下回分解。

## 主要角色

## 客串角色（按出場序）

  - [Michael
    Cassidy](../Page/:en:Michael_Cassidy_\(actor\).md "wikilink")
    飾演蕭笙（[Zach
    Stephens](../Page/:en:Zach_Stephens.md "wikilink")），在[高申於第一季末離開後和](../Page/高申.md "wikilink")[樂心瑪談戀愛](../Page/樂心瑪.md "wikilink")，之後發覺高申和樂心瑪二人仍相愛對方，故退出這三角關係。創作者說「他去了[马林县](../Page/马林县_\(加利福尼亚州\).md "wikilink")（Marin
    County）替[佐治·魯卡斯的電影公司工作](../Page/佐治·魯卡斯.md "wikilink")」。
  - [Nicholas Gonzalez](../Page/:en:Nicholas_Gonzalez.md "wikilink") 飾演
    DJ，李家的園丁，於第二季初和[瑪莎相戀](../Page/顧瑪莎.md "wikilink")。
  - [Shannon Lucio](../Page/:en:Shannon_Lucio.md "wikilink")
    飾演靈思（[Lindsay
    Gardner](../Page/:en:Lindsay_Gardner.md "wikilink")），[李奇立私生女及](../Page/李奇立.md "wikilink")[高姬絲的同父異母妹妹](../Page/高姬絲.md "wikilink")，曾與[歐華恩相戀](../Page/歐華恩.md "wikilink")，後和其母
    Renee Wheeler 搬至[芝加哥居住](../Page/芝加哥.md "wikilink")。
  - [Kathleen York](../Page/:en:Kathleen_York.md "wikilink") 飾演韋詠蘭[Renee
    Wheeler](../Page/:en:Renee_Wheeler.md "wikilink")，和李奇立發生婚外情，誕下靈思。
  - [Olivia Wilde](../Page/:en:Olivia_Wilde.md "wikilink") 飾演雅莉（[Alex
    Kelly](../Page/:en:Alex_Kelly.md "wikilink")），在魚餌店（The Bait
    Shop）工作，分別為高申及顧瑪莎的前女友。
  - [Kim Delaney](../Page/:en:Kim_Delaney.md "wikilink") 飾演布慧冰（[Rebecca
    Bloom](../Page/:en:Rebecca_Bloom.md "wikilink")），[高誠舊情人](../Page/高誠.md "wikilink")，曾介入高氏夫婦的婚姻。
  - [Billy Campbell](../Page/:en:Billy_Campbell.md "wikilink")
    飾演畢卡特（[Carter
    Buckley](../Page/:en:Carter_Buckley.md "wikilink")），雜誌《紐波特生活（Newport
    Living Magazine）》前編輯，轉職前和高姬絲的關係曖昧。
  - [Johnny Messner](../Page/:en:Johnny_Messner.md "wikilink")
    飾演歷斯（[Lance
    Baldwin](../Page/:en:Lance_Baldwin.md "wikilink")），[顧茱莉的初戀情人](../Page/顧茱莉.md "wikilink")，一個[機會主義者](../Page/機會主義.md "wikilink")。
  - [Logan
    Marshall-Green](../Page/:en:Logan_Marshall-Green.md "wikilink")
    飾演[歐程](../Page/歐程.md "wikilink")（前譯歐格雷）（[Trey
    Atwood](../Page/:en:Trey_Atwood.md "wikilink")），歐華恩親兄，剛刑滿出獄。第二季末被顧瑪莎槍傷，及後私自離開醫院前往[拉斯維加斯](../Page/拉斯維加斯.md "wikilink")。
  - [Marguerite Moreau](../Page/:en:Marguerite_Moreau.md "wikilink")
    飾演簡蘊德（[Reed
    Carlson](../Page/:en:Reed_Carlson.md "wikilink")），壞科學出版社行政人員，負責出版高申及簫笙合著的[繪本小說](../Page/繪本小說.md "wikilink")。
  - [Nikki Griffin](../Page/:en:Nikki_Griffin.md "wikilink") 飾演沙靜思（[Jess
    Sather](../Page/:en:Jess_Sather.md "wikilink")），一名有[可卡因](../Page/可卡因.md "wikilink")[毒癮的性感女郎](../Page/毒癮.md "wikilink")，歐程對她亦大有好感。在第三季回來，歐華恩替她處理感情問題。

## 集数列表

<onlyinclude>{| class="wikitable plainrowheaders" style="background:
\#FFF;" width=99% |- style="color:white" \! style="background:
\#4761B1;" | 总集数 \! style="background: \#4761B1;" | 集数 \!
style="background: \#4761B1;" | 标题 \! style="background: \#4761B1;" | 导演
\! style="background: \#4761B1;" | 编剧 \! style="background: \#4761B1;" |
收视人数
(百万) \! style="background: \#4761B1;" | 首播日期 \! style="background:
\#4761B1;" | 制作代码

|}</onlyinclude>

  -

    \- These episodes were first shown in Canada at 8:00 p.m.
    [ET](../Page/Eastern_Time_Zone.md "wikilink") on
    [CTV](../Page/CTV_Television_Network.md "wikilink"). In America the
    airing of "The Return of the Nana" was postponed due to a [press
    conference](../Page/press_conference.md "wikilink") by [President
    Bush](../Page/George_W._Bush.md "wikilink").\[1\] Instead it aired
    the following week at 8:00 p.m. ET immediately followed by "The
    Showdown" at 9:00 p.m.\[2\]

## 重要情節

  - 翠莎向歐華恩謊稱她意外流產。
  - 高申在第一季末遠走到波特蘭，暫住胡洛及其父的家，高誠來說項時他拒絕回去。
  - 高申和歐華恩最終都返回紐波特灘。
  - 兩名新男友出場，分別為樂心瑪男友簫笙（白晢版高申）及顧瑪莎男友 D.J.（後因得悉顧瑪莎利用他來激怒顧茱莉，而與顧瑪莎分手）。
  - 歐華恩與同班的高材生靈思來往，後來發現原來靈思是高姬絲同父異母的妹妹。
  - 李奇立被控[賄賂罪及其他相關罪名](../Page/賄賂.md "wikilink")，因其前僱員 Renee Wheeler
    承認他們之前有姦情而[無罪釋放](../Page/無罪釋放.md "wikilink")。
  - 李奇立辭去紐波特集團[行政總裁之位](../Page/行政總裁.md "wikilink")，指派顧茱莉任[行政總裁及高姬絲任](../Page/行政總裁.md "wikilink")[財政總裁](../Page/財政總裁.md "wikilink")。
  - 17歲的雅莉和高申一起在魚餌店工作，二人繼而產生感情。
  - 李奇立和 Renee Wheeler 的戀情曝光，他們的女兒靈思希望李奇立可以收養她，從而接近及了解她親父。
  - 顧占美和顧茱莉舊情後熾，但當前者得悉顧茱莉想和李奇立維持婚姻關係後，便離開紐波特到[夏威夷](../Page/夏威夷.md "wikilink")
    [毛伊島](../Page/:en:Maui.md "wikilink") 發展事業。
  - 顧茱莉計劃出版一本名為《紐波特生活》的生活潮流雜誌，但被認為用來表現自我居多。
  - 簫笙和高申（和樂心瑪）根據他們的家人朋友的生活，創作了一本[漫畫](../Page/漫畫.md "wikilink")（及後[繪本小說](../Page/繪本小說.md "wikilink")）。
  - 雅莉和顧瑪莎短暫熱戀，之後雅莉靜悄悄地離開紐波特比奇。
  - 李奇立一次輕微[心臟病發](../Page/心臟病.md "wikilink")，令他將家庭放回第一位。
  - 布慧冰（高誠舊情人）希望高誠幫她洗脫多年前犯的罪名，後因布父（高誠的[大學教授](../Page/大學.md "wikilink")）逝世而選擇離開紐波特比奇。
  - 基因測試證實靈思確為李奇立親女，但靈思卻決定跟其母搬往[芝加哥](../Page/芝加哥.md "wikilink")。
  - 樂心瑪發覺她仍深愛高申，遂與簫笙分手，而和高申復合。
  - 歷斯（顧茱莉的初戀情人）利用八十年代時顧茱莉及他合演的小電影《The Porn
    Identity》向顧茱莉[勒索](../Page/勒索.md "wikilink")，勒索不成後歷斯在《紐波特生活》發佈會上當眾發佈該錄影帶。
  - 畢卡特（《紐波特生活》編輯）與高姬絲之間有著曖昧關係。
  - 歐程刑滿出獄後暫住高家，及後搬進雅莉舊居。
  - 在歐程的廿一歲生日派對中，沙靜詩服食過量[毒品](../Page/毒品.md "wikilink")，差點在泳池中溺斃。此事驚動了警方，歐程因而被拘捕（後又無罪釋放）。而他和沙靜詩亦開始來往。
  - 李奇立和顧茱莉關係變差，李奇立更要脅要[離婚](../Page/離婚.md "wikilink")（部份原因是顧茱莉與前夫舊情復熾），但未及簽紙李奇立便去世。
  - 受[毒品影響下](../Page/毒品.md "wikilink")，歐程企圖[強姦顧瑪莎](../Page/強姦.md "wikilink")。
  - 高姬絲對[酒精上癮](../Page/酒精.md "wikilink")，引致車禍發生。
  - 由原本高申與[佐治·魯卡斯見面而簫笙與樂心瑪去高中舞會](../Page/佐治·魯卡斯.md "wikilink")，變成最後簫笙往見佐治·魯卡斯而高申與樂心瑪一起。
  - 李奇立因心臟病發逝世。

季末：

  - 顧占美和李凱莉回來出席李奇立的[葬禮](../Page/葬禮.md "wikilink")。
  - 高姬絲又再[酗酒](../Page/酗酒.md "wikilink")，高誠決定送她到[戒酒中心](../Page/戒酒中心.md "wikilink")，並聯同家人一起說服高姬絲接受治療。
  - 歐程和沙靜詩在魚餌店牽涉[毒品買賣](../Page/毒品.md "wikilink")。
  - 歐華思發現歐程企圖[強姦顧瑪莎](../Page/強姦.md "wikilink")。他倆兄弟由質問變成打架，顧瑪莎趕到時歐程正勒著歐華思的頸。顧瑪莎向歐程開槍，歐程生死未卜。

## 另見

  - [The O.C. (第一季)](../Page/The_O.C._\(第一季\).md "wikilink")
  - [The O.C. (第三季)](../Page/The_O.C._\(第三季\).md "wikilink")

## 参考资料

[Season 2](../Category/橘郡风云.md "wikilink")
[Category:2004年播出的电视剧季度](../Category/2004年播出的电视剧季度.md "wikilink")
[Category:2005年播出的电视剧季度](../Category/2005年播出的电视剧季度.md "wikilink")
[Category:美國電視劇各集列表](../Category/美國電視劇各集列表.md "wikilink")

1.

2.