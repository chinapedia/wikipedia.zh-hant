## 大事记

  - [楚逞嘉措成为第十世](../Page/楚逞嘉措.md "wikilink")[达赖喇嘛](../Page/达赖喇嘛.md "wikilink")。
  - [恰卡建立](../Page/恰卡.md "wikilink")[祖魯王國](../Page/祖魯.md "wikilink")。
  - [勒内·拉内克发明](../Page/勒内·拉内克.md "wikilink")[听诊器](../Page/听诊器.md "wikilink")。
  - [坦博拉火山的爆发造成全球性气温下降](../Page/坦博拉火山.md "wikilink")，此後1816年被稱為「無夏之年」。\[1\]
  - [2月8日](../Page/2月8日.md "wikilink")——[英国国王派使节](../Page/英国.md "wikilink")[阿美士德赴](../Page/阿美士德.md "wikilink")[中国谈判贸易问题](../Page/中国.md "wikilink")。
  - [3月3日](../Page/3月3日.md "wikilink")——英国与[尼泊尔签定的](../Page/尼泊尔.md "wikilink")[塞哥里条约生效](../Page/塞哥里条约.md "wikilink")。
  - [7月9日](../Page/7月9日.md "wikilink")——[阿根廷宣布独立](../Page/阿根廷.md "wikilink")。
  - [9月5日](../Page/9月5日.md "wikilink")——[法國國王](../Page/法國國王.md "wikilink")[路易十八解散](../Page/路易十八.md "wikilink")「無雙議會」。
  - [9月26日](../Page/9月26日.md "wikilink")——[德意志邦聯議會開幕](../Page/德意志邦聯.md "wikilink")。
  - 11月——[詹姆斯·门罗在](../Page/詹姆斯·门罗.md "wikilink")[美国总统大选中获胜](../Page/美国总统.md "wikilink")。
  - [12月11日](../Page/12月11日.md "wikilink")——[印第安那州成为](../Page/印第安纳州.md "wikilink")[美国第](../Page/美國.md "wikilink")19个州。

## 出生

  - [4月21日](../Page/4月21日.md "wikilink")——[夏洛特·勃朗特](../Page/夏洛特·勃朗特.md "wikilink")，英国作家（逝世于[1855年](../Page/1855年.md "wikilink")）
  - [9月11日](../Page/9月11日.md "wikilink")——[卡尔·蔡司](../Page/卡尔·蔡司.md "wikilink")，德国机械师和企业家（逝世于[1888年](../Page/1888年.md "wikilink")）
  - [10月4日](../Page/10月4日.md "wikilink")——[欧仁·鲍狄埃](../Page/欧仁·鲍狄埃.md "wikilink")，法国诗人（逝世于[1887年](../Page/1887年.md "wikilink")）
  - [10月29日](../Page/10月29日.md "wikilink")——[费迪南二世](../Page/斐迪南二世.md "wikilink")，葡萄牙国王（逝世于[1885年](../Page/1885年.md "wikilink")）
  - [11月3日](../Page/11月3日.md "wikilink")——[具伯·爾利](../Page/具伯·爾利.md "wikilink")，[南北戰爭期間的](../Page/南北战争.md "wikilink")[邦聯重要將領之一](../Page/邦聯.md "wikilink")（逝世于[1894年](../Page/1894年.md "wikilink")）
  - [12月6日](../Page/12月6日.md "wikilink")——[约翰·布朗爵士](../Page/约翰·布朗.md "wikilink")，英国实业家（逝世于[1896年](../Page/1896年.md "wikilink"))
  - [12月13日](../Page/12月13日.md "wikilink")——[韦尔纳·冯·西门子](../Page/韦尔纳·冯·西门子.md "wikilink")，德国发明家和企业家（逝世于[1892年](../Page/1892年.md "wikilink")）

## 逝世

## 參考文獻

[\*](../Category/1816年.md "wikilink")
[6年](../Category/1810年代.md "wikilink")
[1](../Category/19世纪各年.md "wikilink")

1.