**讀賣新聞集團本社**（）是[日本](../Page/日本.md "wikilink")[讀賣新聞集團的](../Page/讀賣新聞.md "wikilink")[控股公司](../Page/控股公司.md "wikilink")，主要持股與管理讀賣新聞集團旗下的各家[報社](../Page/報社.md "wikilink")，同時具有整個集團的[母公司功能](../Page/母公司.md "wikilink")。

## 沿革

  - 1870年（[明治](../Page/明治.md "wikilink")3年） -
    [合名會社](../Page/合名會社.md "wikilink")「日就社」在[橫濱創立](../Page/橫濱.md "wikilink")，當時為一家[印刷業者](../Page/印刷.md "wikilink")。
  - 1874年（明治7年）11月2日 - 日就社創立「讀賣新聞」。
  - 1917年（[大正](../Page/大正.md "wikilink")6年）12月1日 - 日就社將商號改為「讀賣新聞社」。
  - 1924年（大正13年）2月25日 -
    由於經營困難，讀賣新聞社由曾任[警視廳警務部部長的](../Page/警視廳.md "wikilink")[正力松太郎接手經營](../Page/正力松太郎.md "wikilink")。
  - 1934年（[昭和](../Page/昭和.md "wikilink")9年）12月26日 -
    正力松太郎創立「大日本東京棒球俱樂部」，即日後的[日本職棒](../Page/日本職棒.md "wikilink")[讀賣巨人隊](../Page/讀賣巨人.md "wikilink")。
  - 1942年（昭和17年）8月5日 - 讀賣新聞社合併[報知新聞社](../Page/報知新聞.md "wikilink")。
  - 1947年（昭和22年）2月15日 - 讀賣新聞社買下大日本東京棒球俱樂部的全部股權，其商號則更改為「讀賣興業」。
  - 1950年（昭和25年）1月25日 - 讀賣巨人從讀賣興業獨立為個別企業。
  - 1951年（昭和26年）3月30日 - 讀賣巨人的企業主體併回讀賣興業。
  - 1952年（昭和27年）11月25日 -
    [子公司](../Page/子公司.md "wikilink")「大阪讀賣新聞社」開始在[大阪市發行以](../Page/大阪市.md "wikilink")[關西地方為對象的](../Page/關西地方.md "wikilink")「大阪讀賣新聞」。
  - 1959年（昭和34年）5月1日 -
    讀賣新聞社在[北海道](../Page/北海道.md "wikilink")[札幌市設立](../Page/札幌市.md "wikilink")「讀賣新聞北海道支社」，開始讀賣新聞的現地印刷。
  - 1961年（昭和36年）5月25日 -
    讀賣新聞社在[富山縣](../Page/富山縣.md "wikilink")[高岡市設立](../Page/高岡市.md "wikilink")「讀賣新聞北陸支社」，開始讀賣新聞的現地印刷。
  - 1964年（昭和39年）9月23日 -
    讀賣興業在[北九州市以](../Page/北九州市.md "wikilink")「讀賣新聞西部本社」之名，開始讀賣新聞的現地印刷。
  - 1975年（昭和50年）3月25日 -
    子公司「中部讀賣新聞社」開始在[名古屋市發行以](../Page/名古屋市.md "wikilink")[中部地方為對象的](../Page/中部地方.md "wikilink")「中部讀賣新聞」。
  - 1988年（昭和63年）2月1日 - 中部讀賣新聞社併入讀賣興業。
  - 1992年（[平成](../Page/平成.md "wikilink")4年）6月22日 - 讀賣興業更名為「株式會社讀賣」（）。
  - 1999年（平成11年）2月1日 -
    買下陷入經營困難的[中央公論社](../Page/中央公論社.md "wikilink")，另設[中央公論新社負責經營](../Page/中央公論新社.md "wikilink")。
  - 2002年（平成14年）7月1日 -
    讀賣集團進行企業重組：讀賣新聞社改組為擔負控股功能的「讀賣新聞集團本社」，原有的報紙出版業務由新成立的「讀賣新聞東京本社」承接；旗下肩負多項事業經營的株式會社讀賣則分割解散。
  - 2010年（平成22年）10月1日 -
    隨著原在東京[大手町的總部大樓進行改建](../Page/大手町_\(千代田區\).md "wikilink")，暫時遷址至[銀座的原](../Page/銀座.md "wikilink")[日產汽車總部大樓](../Page/日產汽車.md "wikilink")（[東京都](../Page/東京都.md "wikilink")[中央區](../Page/中央區_\(東京都\).md "wikilink")[銀座](../Page/銀座.md "wikilink")6丁目17番1號；電話號碼維持原號，專用[郵遞區號則改為](../Page/郵遞區號.md "wikilink")「104-8243」）。
  - 2014年（平成26年）1月6日 - 總部大樓改建完成，讀賣新聞集團本社與東京本社遷回原址\[1\]。

<File:The> Yomiuri Shimbun Holdings new head
office.JPG|位於[東京](../Page/東京.md "wikilink")[大手町的讀賣新聞大樓](../Page/大手町_\(千代田區\).md "wikilink")，為讀賣新聞集團本社與東京本社所在地
<File:Yomiuri> shimbun head office.jpg|已改建的舊讀賣新聞集團本社大樓
[File:Nissan.jpg|讀賣新聞集團本社大樓改建期間，暫遷東京](File:Nissan.jpg%7C讀賣新聞集團本社大樓改建期間，暫遷東京)[銀座的原](../Page/銀座.md "wikilink")[日產汽車總部大樓](../Page/日產汽車.md "wikilink")

## 集團核心企業

  - 株式會社[讀賣新聞東京本社](../Page/讀賣新聞東京本社.md "wikilink")
      - [北海道支社](../Page/讀賣新聞北海道支社.md "wikilink")
      - [北陸支社](../Page/讀賣新聞北陸支社.md "wikilink")
      - [中部支社](../Page/讀賣新聞中部支社.md "wikilink")
  - 株式會社[讀賣新聞大阪本社](../Page/讀賣新聞大阪本社.md "wikilink")
  - 株式會社[讀賣新聞西部本社](../Page/讀賣新聞西部本社.md "wikilink")
  - 株式會社[中央公論新社](../Page/中央公論新社.md "wikilink")
  - 株式會社[讀賣巨人軍](../Page/讀賣巨人軍.md "wikilink")

## 其他關係企業、法人

  - 株式會社[報知新聞社](../Page/報知新聞社.md "wikilink")
      - 株式會社[體育報知西部本社](../Page/體育報知西部本社.md "wikilink")
  - 株式會社福島民友新聞社（[福島民友](../Page/福島民友.md "wikilink")）
  - 株式會社[旅行讀賣出版社](../Page/旅行讀賣出版社.md "wikilink")
  - [東京媒體製作株式會社](../Page/東京媒體製作.md "wikilink")
  - [日本電視控股株式會社](../Page/日本電視控股.md "wikilink")
  - [讀賣電視放送株式會社](../Page/讀賣電視台.md "wikilink")
  - 株式會社[CS日本](../Page/CS日本.md "wikilink")
  - 株式會社[伊卡洛斯](../Page/伊卡洛斯_\(日本企業\).md "wikilink")
  - 株式會社[讀賣情報開發](../Page/讀賣情報開發.md "wikilink")
  - 株式會社[讀賣TASK](../Page/讀賣TASK.md "wikilink")
  - 株式會社[讀賣資訊服務](../Page/讀賣資訊服務.md "wikilink")
  - 株式會社[讀賣電腦](../Page/讀賣電腦.md "wikilink")
  - 株式會社[讀賣樂園](../Page/讀賣樂園.md "wikilink")
  - 株式會社[讀賣旅行](../Page/讀賣旅行.md "wikilink")
  - 讀賣高爾夫株式會社（[讀賣鄉村俱樂部](../Page/讀賣鄉村俱樂部.md "wikilink")）
  - 財團法人[讀賣日本交響樂團](../Page/讀賣日本交響樂團.md "wikilink")
  - [讀賣文化中心聯盟](../Page/讀賣文化中心聯盟.md "wikilink")
      - 株式會社讀賣・日視文化中心
      - 株式會社大阪讀賣文化中心
      - 株式會社讀賣文化中心（讀賣文化中心千里中央）
      - 讀賣[FBS文化中心株式會社](../Page/福岡放送.md "wikilink")
      - 西部讀賣文化中心株式會社（讀賣[KRY文化中心](../Page/山口放送.md "wikilink")）
  - [學校法人](../Page/學校法人.md "wikilink")[讀賣理工学院](../Page/讀賣理工学院.md "wikilink")
  - 株式會社[銀座春天百貨](../Page/銀座春天百貨.md "wikilink")
  - 株式會社[東京讀賣服務](../Page/東京讀賣服務.md "wikilink")
  - [大阪讀賣不誤株式會社](../Page/大阪讀賣不誤.md "wikilink")
  - [讀賣不動産株式會社](../Page/讀賣不動産.md "wikilink")
  - 株式會社[讀賣媒體中心](../Page/讀賣媒體中心.md "wikilink")
  - 株式會社[YOMIX](../Page/YOMIX.md "wikilink")
  - 株式會社[讀賣系統科技](../Page/讀賣系統科技.md "wikilink")
  - 讀賣香港有限公司
  - 株式會社[讀賣AGENCY](../Page/讀賣AGENCY.md "wikilink")
  - [社会福祉法人讀賣光與愛的事業團](../Page/社会福祉法人.md "wikilink")

## 參考資料

## 外部連結

  - [YOMIURI ONLINE](http://www.yomiuri.co.jp/)
  - [讀賣新聞企業網站](http://info.yomiuri.co.jp/)

[Category:日本報社](../Category/日本報社.md "wikilink")
[\*](../Category/讀賣集團.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:日本媒體公司](../Category/日本媒體公司.md "wikilink")
[Category:日本控股公司](../Category/日本控股公司.md "wikilink")

1.