[数学领域](../Page/数学.md "wikilink")[泛函分析中](../Page/泛函分析.md "wikilink")，最著名的悬而未决的问题之一就是**不变子空间问题**，有时被乐观地称为**不变子空间猜想**。这个问题就是如下命题是否成立：

  -
    给定一个复[希尔伯特空间](../Page/希尔伯特空间.md "wikilink")*H*，其维度\>1，以及一个[有界线性算子](../Page/有界算子.md "wikilink")*T* : *H* → *H*，则*H*有一个非平凡[闭](../Page/闭集.md "wikilink")[*T*-不变子空间](../Page/不变子空间.md "wikilink")，也即存在一个*H*的闭[线性子空间](../Page/线性子空间.md "wikilink")*W*，而且它不同于{0}和*H*，且使得*T*(*W*)
    ⊆ *W*。

该命题对于所有2维以上有限维复[向量空间是成立的](../Page/向量空间.md "wikilink")：一个[线性算子](../Page/线性算子.md "wikilink")（矩阵）的[特征值是其](../Page/特征值.md "wikilink")[特征多项式的零点](../Page/特征多项式.md "wikilink")；根据[代数基本定理](../Page/代数基本定理.md "wikilink")，这个多项式存在零点；一个对应的[特征向量可以张成一个不变子空间](../Page/特征向量.md "wikilink")。该命题也很容易成立如果*W*不必是闭的：取任意*H*中非零向量*x*并考虑*H*的由{*T*<sup> *n*</sup>(*x*)
: *n* ≥ 0}线性张成的子空间*W*.

虽然该猜想的一般情况未获证明，但已经可以列出命题成立的一些特殊情况：

  - 在希尔伯特空间*H*[可分的情况下该猜想相对比较容易证明](../Page/可分空间.md "wikilink")（也即，如果它又一个[不可数](../Page/不可数.md "wikilink")[正交基](../Page/正交基.md "wikilink")。
  - [谱定理表明所有](../Page/谱定理.md "wikilink")[正则算子有不变子空间](../Page/正则算子.md "wikilink")。
  - 每个[紧算子有不变子空间](../Page/紧算子.md "wikilink")，由Aronszajn和Smith于1954年证明。紧算子理论在很多方面和有限维空间算子理论相类似，所以该结果并不令人惊讶。
  - 波恩斯坦和洛宾逊于1966年证明若*T*<sup> *n*</sup>对于某个正整数*n*是紧致的，则*T*有不变子空间。
  - V. I. 罗门诺所夫（Lomonosov）于1973年证明若*T*和某个非零紧算子可交换，则*T*有不变子空间。

近年来，有些数学家试图采用[随机矩阵理论来构造该猜想的](../Page/随机矩阵.md "wikilink")[反例](../Page/反例.md "wikilink")。

如果考虑[巴拿赫空间而不是](../Page/巴拿赫空间.md "wikilink")[希尔伯特空间](../Page/希尔伯特空间.md "wikilink")，则该猜想不成立；[P.
Enflo于](../Page/P._Enflo.md "wikilink")1975年给出了没有非平凡不变子空间的有界算子的显式例子，[Charles
Read于](../Page/Charles_Read_\(mathematician\).md "wikilink")1984年也给出一个反例。但是，该命题对于[算子的特定类别是成立的](../Page/算子.md "wikilink")。

1964年，[Louis de
Branges发表了不变子空间猜想的可能证明](../Page/Louis_de_Branges.md "wikilink")，但后来被发现是错误的。他最近在他的网站上发表了一个新的可能证明[1](http://www.math.purdue.edu/~branges/invariantsubspaceconjecture.pdf)；但他的证明还未经过同行评审。

## 参考

  - [Paul Halmos](../Page/Paul_Halmos.md "wikilink")。Invariant
    Subspaces. *[American Mathematical
    Monthly](../Page/American_Mathematical_Monthly.md "wikilink")*, Vol.
    85, No. 3 (March 1978), pages 182-183.
  - B. S. Yadav. The present state and heritages of the invariant
    subspace problem. *Milan J. Math.* 73 (2005), pages 289-316.
  - Piotr Sniady. Generalized Cauchy identities, trees and
    multidimensional Brownian motions. Part I: bijective proof of
    generalized Cauchy identities. Section 1.5.
    [Preprint 2004](http://arxiv.org/abs/math.CO/0412043)。
  - Enflo, P. On the invariant subspace problem in Banach spaces.
    Séminaire Maurey--Schwartz (1975-1976) Espaces *Lp* applications
    radonifiantes et géométrie des espaces de Banach, Exp. Nos. 14-15,
    Centre Math., École Polytech., Palaiseau, 1976.

[B](../Category/泛函分析.md "wikilink")
[B](../Category/数学中未解决的问题.md "wikilink")