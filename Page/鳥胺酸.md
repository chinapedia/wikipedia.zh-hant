**鳥氨酸**（）是一種α-[胺基酸](../Page/胺基酸.md "wikilink")，其結構為NH<sub>2</sub>-CH<sub>2</sub>-CH<sub>2</sub>-CH<sub>2</sub>-CHNH<sub>2</sub>-COOH。

## 在尿素循环中的作用

鳥氨酸是[精氨酸酶在催化](../Page/精氨酸酶.md "wikilink")[精氨酸產生尿素時同時產生](../Page/精氨酸.md "wikilink")。因此，鳥氨酸是[尿素循環的中央部份](../Page/尿素循環.md "wikilink")，以排出多餘的[氮](../Page/氮.md "wikilink")。

由於鳥氨酸不是為[DNA而編碼](../Page/DNA.md "wikilink")，所以是不會涉及[蛋白質生物合成內](../Page/蛋白質生物合成.md "wikilink")。但是，在[哺乳動物的非肝臟組織中](../Page/哺乳動物.md "wikilink")，鳥氨酸作為[精氨酸生物合成的中介物而負起重要的角色](../Page/精氨酸.md "wikilink")。一般相信鳥氨酸沒有編碼，是因為多胜肽包含了進行[乳胺化的無保護鳥氨酸](../Page/乳胺.md "wikilink")。這證明了將鳥氨酸人工置入為第21種[胺基酸會是一個問題](../Page/胺基酸.md "wikilink")。

[Ornithine_lactamization.svg](https://zh.wikipedia.org/wiki/File:Ornithine_lactamization.svg "fig:Ornithine_lactamization.svg")

[鳥胺酸去羧化酶催化鳥氨酸是合成](../Page/鳥胺酸去羧化酶.md "wikilink")[聚胺](../Page/聚胺.md "wikilink")，如[甲烯二胺](../Page/甲烯二胺.md "wikilink")（PUT）的開端。

## 其他反应

一些[細菌](../Page/細菌.md "wikilink")，如[大腸桿菌](../Page/大腸桿菌.md "wikilink")，可以從[穀氨酸中生成鳥氨酸](../Page/穀氨酸.md "wikilink")。

## 参考文献

[分類:α-胺基酸](../Page/分類:α-胺基酸.md "wikilink")
[分類:碱性氨基酸](../Page/分類:碱性氨基酸.md "wikilink")
[分類:尿素循环](../Page/分類:尿素循环.md "wikilink")