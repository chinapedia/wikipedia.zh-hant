****是历史上第83次航天飞机任务，也是[亚特兰蒂斯号航天飞机的第](../Page/亞特蘭提斯號太空梭.md "wikilink")19次太空飞行。

## 任务成员

  - **[查尔斯·普里克特](../Page/查尔斯·普里克特.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[艾琳·科林斯](../Page/艾琳·科林斯.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[卡洛斯·诺里加](../Page/卡洛斯·诺里加.md "wikilink")**（，曾执行以及任务），任务专家
  - **[卢杰](../Page/盧傑_\(太空人\).md "wikilink")**（曾执行、、以及[远征7号任务](../Page/远征7号.md "wikilink")），任务专家
  - **[让-弗朗西斯·格列福瑞](../Page/让-弗朗西斯·格列福瑞.md "wikilink")**（，[法国宇航员](../Page/法国.md "wikilink")，曾执行、以及任务），任务专家
  - **[叶莲娜·康达科娃](../Page/叶莲娜·康达科娃.md "wikilink")**（，[俄罗斯宇航员](../Page/俄罗斯.md "wikilink")，曾执行、[和平号以及](../Page/和平號太空站.md "wikilink")任务），有效载荷专家

### 发射后停留在和平号空间站

  - **[迈克尔·福奥勒](../Page/迈克尔·福奥勒.md "wikilink")**（，曾执行、、、、以及[远征8号任务](../Page/远征8号.md "wikilink")），任务专家

### 从和平号空间站返回

  - **[杰瑞·林恩格](../Page/杰瑞·林恩格.md "wikilink")**（，曾执行以及任务），任务专家

[Category:载人航天](../Category/载人航天.md "wikilink")
[Category:航天飞机任务](../Category/航天飞机任务.md "wikilink")
[Category:1997年](../Category/1997年.md "wikilink")