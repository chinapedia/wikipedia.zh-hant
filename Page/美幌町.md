**美幌町**（）位於[北海道](../Page/北海道.md "wikilink")[鄂霍次克綜合振興局東南部](../Page/鄂霍次克綜合振興局.md "wikilink")，位於[網走市和](../Page/網走市.md "wikilink")[北見市的中間](../Page/北見市.md "wikilink")，地形以[平原為主](../Page/平原.md "wikilink")，有網走川與美幌川流經平原，南部以美幌鞍部與[釧路支廳相接](../Page/釧路支廳.md "wikilink")。

町名源自於[阿伊努語的](../Page/阿伊努語.md "wikilink")「pe-poro」（水多的地方）或「pi-poro」（石頭多的地方）\[1\]\[2\]。

## 歷史

  - 1887年7月：設置美幌外5村戶長役場。\[3\]
  - 1915年4月1日：杵端邊村、古梅村、美幌村合併為美幌村，並成為北海道二級村。\[4\]
  - 1919年4月1日：活汲村、達媚村、飜木禽村和美幌村的部份區域（舊美幌村區域）合併為津別村（現在的[津別町](../Page/津別町.md "wikilink")）。
  - 1920年：美幌峰道路開通。
  - 1921年：津別村的部份區域被併入美幌村。
  - 1923年4月1日：改制為一級町美幌町。
  - 1946年：女滿別村（現在的[大空町](../Page/大空町.md "wikilink")）的高野地區被併入美幌村。
  - 1953年：制定美幌町町歌。武滿徹作曲（武満徹）（Toru Takemitsu）。

## 產業

以農業為主，農產包括[甜菜](../Page/甜菜.md "wikilink")、[小麥](../Page/小麥.md "wikilink")、[馬鈴薯](../Page/馬鈴薯.md "wikilink")、[洋蔥](../Page/洋蔥.md "wikilink")、[豆等](../Page/豆.md "wikilink")。

## 交通

### 機場

  - [女滿別機場](../Page/女滿別機場.md "wikilink")（位於[大空町](../Page/大空町.md "wikilink")）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [石北本線](../Page/石北本線.md "wikilink")：[美幌車站](../Page/美幌車站.md "wikilink")
  - 曾經有[相生線通過](../Page/相生線.md "wikilink")，現已廢線。

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/一般國道.md" title="wikilink">一般國道</a></dt>

</dl>
<ul>
<li><a href="../Page/國道39號.md" title="wikilink">國道39號</a></li>
<li><a href="../Page/國道240號.md" title="wikilink">國道240號</a></li>
<li><a href="../Page/國道243號.md" title="wikilink">國道243號</a></li>
<li><a href="../Page/國道334號.md" title="wikilink">國道334號</a></li>
</ul>
<dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道122號北見端野美幌線</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道217號北見美幌線</li>
<li>北海道道248號嘉多山美幌線</li>
<li>北海道道249號福住女滿別線</li>
<li>北海道道309號美幌停車場線</li>
<li>北海道道995號東藻琴豐富線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

[Lake_Kussharo.jpg](https://zh.wikipedia.org/wiki/File:Lake_Kussharo.jpg "fig:Lake_Kussharo.jpg")\]\]

  - 美幌嶺之湯
  - 美幌航空公園
  - [美幌峠](../Page/美幌峠.md "wikilink")
  - 美幌農業館、博物館

## 教育

### 高等學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>道立北海道美幌高等學校</li>
</ul></td>
<td><ul>
<li>道立北海道美幌農業高等學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>美幌町立美幌中學校</li>
</ul></td>
<td><ul>
<li>美幌町立美幌北中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>美幌町立美幌小學校</li>
<li>美幌町立旭小學校</li>
<li>美幌町立東陽小學校</li>
</ul></td>
<td><ul>
<li>美幌町立上美幌小學校</li>
<li>美幌町立福豐小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [湧水町](../Page/湧水町.md "wikilink")（[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")）

### 海外

  - [Cambridge](http://www.cambridge.co.nz)（[紐西蘭](../Page/紐西蘭.md "wikilink")
    [懷卡托](../Page/懷卡托.md "wikilink")）\[5\]

## 本地出身的名人

  - [北之富士勝昭](../Page/北之富士勝昭.md "wikilink")：[相撲力士](../Page/相撲力士.md "wikilink")，第52代[橫綱](../Page/橫綱.md "wikilink")
  - [木村章司](../Page/木村章司.md "wikilink")：[拳擊手](../Page/拳擊手.md "wikilink")
  - [山口昌男](../Page/山口昌男.md "wikilink")：文化人類學者
  - [內藤克](../Page/內藤克.md "wikilink")：HBC北海道放送[播音員](../Page/播音員.md "wikilink")
  - Sepia'n Roses：音樂人，四名成員中有三人是出身自美幌町

## 參考資料

## 外部連結

  - [美幌町民的網頁](https://web.archive.org/web/20070707202319/http://bihorojin.hp.infoseek.co.jp/)

  - [美幌町的部落格](https://web.archive.org/web/20070222190842/http://blog.goo.ne.jp/qrium)

  - [美幌町圖書館](http://www.town.bihoro.hokkaido.jp/library/index.htm)

  - [美幌町町歌](http://masaokato.jp/bihoro/bihoro)

[\*](../Category/美幌町.md "wikilink")

1.
2.
3.
4.
5.