**格利澤710**是位於[巨蛇座尾部的一顆恆星](../Page/巨蛇座.md "wikilink")，[視星等](../Page/視星等.md "wikilink")9.69等，[光譜類型為K](../Page/光譜類型.md "wikilink")7Vk\[1\]，這意味著它是一顆以核心的氫進行[熱核融合做為能量來源的](../Page/核聚變.md "wikilink")[主序星](../Page/主序星.md "wikilink")
(尾碼的k顯示光譜中有星際物質吸收的譜線)。這顆恆星的質量大約是[太陽質量的](../Page/太陽質量.md "wikilink")60%
\[2\]，而估計半徑是[太陽半徑的](../Page/太陽半徑.md "wikilink")67%\[3\]。它可能是一顆光度在9.65-9.69之間的疑似變星。

這顆恆星目前至地球的距離是63.8光年
(19.6秒差距)，但是依據過去和現在[依巴谷衛星的資料指出](../Page/依巴谷衛星.md "wikilink")，以它的[自行和](../Page/自行.md "wikilink")[徑向速度](../Page/徑向速度.md "wikilink")\[4\]，它將在140萬年後接近[太陽至很近的距離](../Page/太陽.md "wikilink")
－
或許少於一光年\[5\]。在最接近的時候，它的光度將達到1等星的亮度，如同[心宿二一樣的亮](../Page/心宿二.md "wikilink")。在目前的距離上，格利澤710的自行非常小，這意味著它幾乎是直接朝向著我們的視線方向移動著，可以與牧夫座的[大角星比較](../Page/大角星.md "wikilink")。

從現在開始在±1,000萬年的時間內，結合格利澤710這顆恆星的質量和距離的接近將對[太陽系造成最大的](../Page/太陽系.md "wikilink")[萬有引力攝動](../Page/萬有引力.md "wikilink")。

## 最接近的詳細資料和計算

葛利斯710有能力對假設的[歐特雲造成](../Page/歐特雲.md "wikilink")[攝動送出大量的](../Page/攝動.md "wikilink")[彗星進入內太陽系](../Page/彗星.md "wikilink")，並可能造成[撞擊事件](../Page/撞擊事件.md "wikilink")。然而，以García-Sánchez等人的動力學模型在1999年的模擬，由於葛利斯710的接近造成撞擊坑的淨增加率不會超過5%\[6\]。他們估計最接近的時間在136萬年後，這顆恆星將接近太陽至0.337 ± 0.177[秒差距的距離](../Page/秒差距.md "wikilink")
(1.1光年) \[7\]。

[Kuiper_oort-en.svg](https://zh.wikipedia.org/wiki/File:Kuiper_oort-en.svg "fig:Kuiper_oort-en.svg")和[古柏帶](../Page/古柏帶.md "wikilink")
(插圖)。\]\]

博貝列夫 (Bobylev)
在2010年的計算指出，若考虑歐特雲是包圍著太陽的球體，半長軸和半短軸分別是100,000和80,000[天文單位](../Page/天文單位.md "wikilink")，葛利斯710將有86%的機會從歐特雲經過。葛利斯710最接近的距離很難精確的計算，因為他非常敏感的取決於目前的位置和速度。博貝列夫估計它會在際離太陽0.311
± 0.167秒差距 (1.01±
0.54光年)的距離內經過\[8\]；這顆恆星甚至還有1/10,000的機會進入對[古柏帶有顯著影響的距離內](../Page/古柏帶.md "wikilink")
(d \< 1,000 AU) \[9\]。

這顆恆星可能是在前後1,000萬年內，繼[大陵五之後](../Page/大陵五.md "wikilink")，對太陽系攝動第二大的恆星\[10\]。
在730萬年前，大陵五接近太陽的距離小於9.8光年，但其總系統質量是太陽質量的5.8倍，故攝動的影響可能大於葛利斯710。大陵五在當時就造成了許多位於歐特雲的彗星改變軌道而往[內太陽系而來](../Page/內太陽系.md "wikilink")。

## 相關條目

[近距離恆星列表](../Page/近距離恆星列表.md "wikilink")

## 參考資料

## 延伸讀物

  -
<!-- end list -->

  -
<!-- end list -->

  -
## 外部連結

  - [SolStation.com](http://www.solstation.com/stars2/gl710.htm)
  - [VizieR variable star
    database](http://vizier.u-strasbg.fr/viz-bin/VizieR-S?NSV%2010635)
  - Wikisky
    [image](http://www.wikisky.org/?ra=18.33079&de=-1.9386054999999998&zoom=8&show_grid=1&show_constellation_lines=1&show_constellation_boundaries=1&show_const_names=0&show_galaxies=1&show_box=1&box_ra=18.33079&box_de=-1.9386055&box_width=50&box_height=50&img_source=DSS2)
    of HD 168442 (Gliese 710)

[BD-01 03474](../Category/BD、CD和CP天体.md "wikilink")
[168442](../Category/HD和HDE天體.md "wikilink")
[089825](../Category/HIP天體.md "wikilink")
[Category:橙矮星](../Category/橙矮星.md "wikilink")
[Category:巨蛇座](../Category/巨蛇座.md "wikilink")
[Category:紅矮星](../Category/紅矮星.md "wikilink")
[0710](../Category/格利泽和GJ天体.md "wikilink")

1.
2.
3.
4.  See also: [Stellar
    kinematics](../Page/Stellar_kinematics.md "wikilink").

5.
6.
7.

8.

9.
10.