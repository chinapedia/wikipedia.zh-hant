[Alfonso_I_el_Batallador_01.jpg](https://zh.wikipedia.org/wiki/File:Alfonso_I_el_Batallador_01.jpg "fig:Alfonso_I_el_Batallador_01.jpg")
**（威武的）阿方索一世 Alfonso I el
Batallador**（约1073年—1134年9月）[阿拉贡和](../Page/阿拉贡.md "wikilink")[纳瓦拉国王](../Page/纳瓦拉.md "wikilink")（1104年—1134年在位）。

阿方索一世为阿拉贡国王[桑乔一世之子](../Page/桑乔一世_\(阿拉贡\).md "wikilink")。他与[卡斯蒂利亚王国的女继承人](../Page/卡斯蒂利亚王国.md "wikilink")[烏拉卡](../Page/烏拉卡_\(卡斯蒂利亞\).md "wikilink")（[勃艮第伯爵雷蒙的遺孀](../Page/勃艮第.md "wikilink")）结婚。在卡斯蒂利亚国王[阿方索六世去世后](../Page/阿方索六世_\(卡斯蒂利亚\).md "wikilink")，（阿拉贡的）阿方索一世实际上继承了[伊比利亚半岛上所有四个](../Page/伊比利亚半岛.md "wikilink")[基督教国家](../Page/基督教.md "wikilink")（阿拉贡、纳瓦拉、[莱昂](../Page/莱昂.md "wikilink")、卡斯蒂利亚）。但由于卡斯蒂利亚人拒绝接受他的统治，阿方索一世不得不放弃了莱昂和卡斯蒂利亚的王位，把它们让给了乌拉尔卡与前夫所生的儿子[阿方索七世](../Page/阿方索七世_\(卡斯蒂利亚\).md "wikilink")。

阿方索一世是[西班牙](../Page/西班牙.md "wikilink")[收复失地运动的代表人物](../Page/收复失地运动.md "wikilink")。1118年他从[摩尔人手中夺回了](../Page/摩尔人.md "wikilink")[萨拉戈萨](../Page/萨拉戈萨.md "wikilink")，1120年又击退了围攻萨拉戈萨的[穆拉比特王朝的大军](../Page/穆拉比特王朝.md "wikilink")。阿方索一世多次进军[安达卢西亚乃至](../Page/安达卢西亚.md "wikilink")[法国南部](../Page/法国.md "wikilink")。

1134年，阿方索一世在[弗腊加战役中身受重伤](../Page/弗腊加战役.md "wikilink")，不久即去世。

|-

[Category:阿拉贡君主](../Category/阿拉贡君主.md "wikilink")
[Category:纳瓦拉君主](../Category/纳瓦拉君主.md "wikilink")
[A](../Category/1134年逝世.md "wikilink")