**Internet Explorer for Mac**（亦稱為**Internet Explorer:mac**或**Internet
Explorer麥金塔版本**）是[微軟公司為](../Page/微軟.md "wikilink")[麥金塔平台而開發的一款](../Page/麥金塔.md "wikilink")[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")。最初的版本是建基於Windows版本的[Internet
Explorer而開發的](../Page/Internet_Explorer.md "wikilink")，但後期的版本如5.0版則使用新的[排版引擎名為](../Page/排版引擎.md "wikilink")[Tasman](../Page/Tasman.md "wikilink")\[1\]。

[蘋果電腦和微軟在](../Page/蘋果電腦.md "wikilink")1997年同意將Internet Explorer作為[Mac
OS的預設瀏覽器](../Page/Mac_OS.md "wikilink")\[2\]，直至2003年被由蘋果電腦自家開發的[Safari瀏覽器所取代](../Page/Safari.md "wikilink")。

在2003年6月13日，微軟宣佈終止開發Internet Explorer for
Mac\[3\]。微軟隨後在2005年12月31日終止支援，並於2006年1月31日不再提供其下載，通知Mac使用者改用Safari\[4\]。在[Mac
OS X v10.4以後的版本都不再包含IE](../Page/Mac_OS_X_v10.4.md "wikilink")。

## 版本摘要

|                                                                            **Internet Explorer for Mac版本概述**                                                                             |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| Mac OS [7](../Page/System_7.md "wikilink")、[8](../Page/Mac_OS_8.md "wikilink")、[9於](../Page/Mac_OS_9.md "wikilink")[68k和](../Page/68k.md "wikilink")[PPC](../Page/PowerPC.md "wikilink") |
|                                                                                           *版本*                                                                                           |
|                                                                                          版本2.0                                                                                           |
|                                                                                          版本2.1                                                                                           |
|                                                                                          版本3.0                                                                                           |
|                                                                                          版本3.01                                                                                          |
|                                                                                          版本4.0                                                                                           |
|                                                                                          版本4.5                                                                                           |
|                                                                                          版本5.0                                                                                           |
|                                                                                          版本5.1                                                                                           |
|                                                                                         版本5.1.4                                                                                          |
|                                                                                         版本5.1.5                                                                                          |
|                                                                                         版本5.1.6                                                                                          |
|                                                                                         版本5.1.7                                                                                          |
|                                                                      [Mac OS X於PPC](../Page/Mac_OS_X.md "wikilink")                                                                      |
|                                                                                           *版本*                                                                                           |
|                                                                                           版本5                                                                                            |
|                                                                                         版本5.1.1                                                                                          |
|                                                                                         版本5.1.2                                                                                          |
|                                                                                         版本5.1.3                                                                                          |
|                                                                                          版本5.2                                                                                           |
|                                                                                         版本5.2.1                                                                                          |
|                                                                                         版本5.2.2                                                                                          |
|                                                                                         版本5.2.3                                                                                          |

## 參考資料

## 參見

  - [網頁瀏覽器列表](../Page/網頁瀏覽器列表.md "wikilink")
  - [網頁瀏覽器比較](../Page/網頁瀏覽器比較.md "wikilink")

## 外部連結

  - [Internet Explorer 5 for
    Mac非官方下載頁](http://www.pure-mac.com/webb.html#mie)

[Category:Internet
Explorer](../Category/Internet_Explorer.md "wikilink")
[Category:已停止開發的微軟軟體](../Category/已停止開發的微軟軟體.md "wikilink")

1.
2.
3.
4.