**礼器**是[中国古代在](../Page/中国.md "wikilink")[祭祀](../Page/祭祀.md "wikilink")、[宴飨](../Page/宴飨.md "wikilink")、[丧葬以及](../Page/丧葬.md "wikilink")[征伐等礼仪活动中使用的器具](../Page/征伐.md "wikilink")，其使用的规格有严格的等级限制，用以表明使用者的地位、身份、权力。在周礼中，礼器是宗教和政治的结合点。早期礼器重[玉器](../Page/玉器.md "wikilink")，因其在原始巫术中有通灵的功效。随着商周之际政治宗教的发展，玉器逐渐从神权的象征演化为王权的象征，而越来越多的礼器使用青铜器铸造，统称为[尊彝](../Page/彝器.md "wikilink")。而[秦](../Page/秦朝.md "wikilink")[汉以后](../Page/汉朝.md "wikilink")，青铜礼器的使用逐渐减少。

礼器的早期功用在于沟通天地鬼神，《[礼记](../Page/礼记.md "wikilink")·礼器》：禮也者，合於天時，設於地財，順於鬼神，合於人心，理萬物者也。而在三代晚期，礼器的主要功能转变为“纪”。所谓“纪”有三层含义：一是纪神物，以使人有所畏惧\[1\]，商代晚期的青铜器尤其具有代表性，往往以细密的云雷纹作为地纹，再在上面施以凸出于器表的繁缛的纹饰，形式有兽面纹、龙纹、夔纹、凤鸟纹、蝉纹等主题纹饰；二是纪等宜、明尊卑；三是纪功赏，在礼器上铭刻铸器事由，以见德功之美、勋赏之名。

按照使用的场合，礼器可以分为宗器、旅器和媵器。宗器用于宗庙祭祀之礼、旅器用于师旅征伐之礼、媵器用于婚嫁之礼。从种类和功能上分，[玉器礼器的种类有](../Page/玉器.md "wikilink")[璧](../Page/璧.md "wikilink")、[琮](../Page/琮.md "wikilink")、[圭](../Page/圭.md "wikilink")、[璋等](../Page/璋.md "wikilink")；[青铜器礼器数量更多](../Page/青铜器.md "wikilink")，可分为[炊器](../Page/炊器.md "wikilink")、[食器](../Page/食器.md "wikilink")、[酒器](../Page/酒器.md "wikilink")、[水器](../Page/水器.md "wikilink")、[乐器](../Page/乐器.md "wikilink")（也有将乐器和礼器并列的）。炊器为做饭之用，包括[鼎](../Page/鼎.md "wikilink")、[甗等](../Page/甗.md "wikilink")，食器为饮食之用，包括[豆](../Page/豆_\(器皿\).md "wikilink")、[簋等](../Page/簋.md "wikilink")，酒器包括[爵](../Page/爵.md "wikilink")、[尊](../Page/尊.md "wikilink")、[壶等](../Page/壶.md "wikilink")，水器包括[盘](../Page/盘.md "wikilink")、[匜等](../Page/匜.md "wikilink")，乐器有[鐘](../Page/鐘_\(敲擊器\).md "wikilink")、[铙等](../Page/铙.md "wikilink")。

## 注释

## 参考

1.
2.
[en:List of Chinese names of bronze
vessels](../Page/en:List_of_Chinese_names_of_bronze_vessels.md "wikilink")

[中國傳統禮器](../Category/中國傳統禮器.md "wikilink")

1.  《左传·宣工三年》：“昔夏之方有德也，遠方圖物，貢金九牧，鑄鼎象物，百物而為之備，使民知神姦，故民入川澤山林，不逢不若，螭魅罔兩，莫能逢之，用能協于上下，以承天休”