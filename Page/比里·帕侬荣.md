**比里·帕侬荣**（，，），又名**鑾巴立·玛奴探**，[泰國政治家](../Page/泰國.md "wikilink")，第八、九、十任[泰国总理](../Page/泰国总理.md "wikilink")，是第七位泰國总理。漢名**陳嘉祥**、**陳璋茂**。\[1\]

帕侬荣的父亲是個稻米批發商人，拥有華人和暹羅人的血統，而母亲是暹羅人。曾祖父陳盛于（Tan Nai
Kok）出生于[廣東](../Page/廣東.md "wikilink")[潮州府](../Page/潮州府.md "wikilink")[澄海县上華鎮下陳村](../Page/澄海区.md "wikilink")（现广东[汕头市澄海区](../Page/汕头市.md "wikilink")），在[拉玛二世在位時移民到暹羅](../Page/拉玛二世.md "wikilink")。\[2\]\[3\]帕侬荣生於泰國古都[阿瑜陀耶城](../Page/阿瑜陀耶.md "wikilink")。

他是暹罗1920年首批留法学生，获得[巴黎大學法學博士和一个经济学高级学位](../Page/巴黎大學.md "wikilink")。他在巴黎留法学生中深孚众望，先后被选为暹罗在法留学生联合会秘书和主席。他受法国[空想社会主义和](../Page/空想社会主义.md "wikilink")[孙中山](../Page/孙中山.md "wikilink")[三民主义思想影响较深](../Page/三民主义.md "wikilink")，是暹罗传播西方民主思想的先驱。

1927年，帕侬荣等人在[巴黎創建了](../Page/巴黎.md "wikilink")[暹羅人民黨](../Page/暹羅人民黨.md "wikilink")（Khana
Ratsadon，又稱民黨），民黨分帕侬荣为首的“文治派”（左翼）和[披耶帕凤上校为首的](../Page/披耶帕凤.md "wikilink")“军事派”（右翼）。1932年6月24日[披耶帕凤上校为首的人民黨](../Page/披耶帕凤.md "wikilink")“军事派”发动不流血[政变](../Page/1932年泰国六二四政变.md "wikilink")，成立了临时军政府，并发表了帕侬荣起草的政变宣言，宣布民黨为合法政党，要以民主政府取代君主专制政府。6月27日[拉玛七世被迫签署了由帕侬荣主持订制的临时宪法](../Page/拉玛七世.md "wikilink")，帕侬荣在[披耶·玛奴巴功內閣中任职并参加九人制宪委员会编制永久宪法](../Page/披耶·玛奴巴功.md "wikilink")。1933年帕侬荣提出的20点经济计划被总理披耶·玛奴巴功为首的保皇派称为“共产党人计划”，4月1日披耶·玛奴巴功改组内阁，帕侬荣被逐出内阁，4月12日前往法国。

1933年6月20日[披耶帕凤再次发动政变](../Page/披耶帕凤.md "wikilink")，推翻披耶·玛奴巴功政府，组成新的立宪政府。1934年帕侬荣回国任[披耶帕凤政府的內務部長](../Page/披耶帕凤.md "wikilink")，1935－1937年任外交部长，1938年任财政部长。外交部长期间帕侬荣与12个国家签署条约废除了自拉玛四世以来的不平等条约，泰国收回法律管辖和税收主权。

[銮披汶·颂堪政府时期](../Page/銮披汶·颂堪.md "wikilink")，帕侬荣作为财政部长主张抗日，反对[銮披汶·颂堪的亲日政策](../Page/銮披汶·颂堪.md "wikilink")，两人的友谊逐渐破裂。1942年1月25日，[銮披汶·颂堪对英美宣战](../Page/銮披汶·颂堪.md "wikilink")，帕侬荣拒绝签字。1944年帕侬荣辞去财政部长，担任游学在瑞士的[拉玛八世国王的摄政](../Page/拉玛八世.md "wikilink")。日本投降后，帕侬荣作为摄政，公布了和平宣言，宣称“宣战非法和无效”，否定了銮披汶·颂堪政府和日本的所有协议。1945年12月拉玛八世回国，帕侬荣从摄政位子上退下，担任顾问。1946年组建了职联党，在8月的大选中囊括了189个席位中的的49席，成为第三大党。

[二战后](../Page/二战.md "wikilink")，帕侬荣出任三届[泰国总理](../Page/泰国总理.md "wikilink")（1946年3月24日～6月3日，1946年6月8～9日，1946年6月10～20日）。1947年11月8日发生军事政變，他在英国和美国特工的帮助下逃到新加坡，随后避難中國，僑居21年，1970年移居[巴黎](../Page/巴黎.md "wikilink")。1983年5月2日在巴黎逝世，終年82歲。

[Monument_of_Pridi_Phanomyong_&_Dome_of_Thammasat_University.jpg](https://zh.wikipedia.org/wiki/File:Monument_of_Pridi_Phanomyong_&_Dome_of_Thammasat_University.jpg "fig:Monument_of_Pridi_Phanomyong_&_Dome_of_Thammasat_University.jpg")比里·帕侬荣塑像.\]\]
[泰国法政大学](../Page/泰国法政大学.md "wikilink")（Thammasat
University，即淡马锡大学）由帕侬荣在1934年创建。

## 參考資料

<div class="references-small">

<references/>

</div>

[Jia嘉](../Category/陈姓.md "wikilink")
[Category:澄海人](../Category/澄海人.md "wikilink")
[Category:泰國潮汕人](../Category/泰國潮汕人.md "wikilink")
[Category:大城府人](../Category/大城府人.md "wikilink")
[Category:泰國華人](../Category/泰國華人.md "wikilink")
[Category:华裔混血兒](../Category/华裔混血兒.md "wikilink")
[Category:泰族裔混血兒](../Category/泰族裔混血兒.md "wikilink")
[Category:巴黎大學校友](../Category/巴黎大學校友.md "wikilink")
[Category:泰國律師](../Category/泰國律師.md "wikilink")
[Category:泰國內政部長](../Category/泰國內政部長.md "wikilink")
[Category:泰國外交部長](../Category/泰國外交部長.md "wikilink")
[Category:泰国总理](../Category/泰国总理.md "wikilink")
[Category:勳一等旭日大綬章獲得者](../Category/勳一等旭日大綬章獲得者.md "wikilink")

1.
2.
3.  [ปรีดี
    พนมยงค์](https://web.archive.org/web/20020207171455/http://www.geocities.com/thaifreeman/pridi/pridi.html)