[MET_Hall_New_YorkCity.jpg](https://zh.wikipedia.org/wiki/File:MET_Hall_New_YorkCity.jpg "fig:MET_Hall_New_YorkCity.jpg")
**大都會藝術博物館**（，昵称The
Met）位於[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")[曼哈頓](../Page/曼哈頓.md "wikilink")[中央公園旁](../Page/中央公園_\(紐約市\).md "wikilink")，是世界上最大的、[參觀人數最多的](../Page/世界參觀人數最多的藝術博物館列表.md "wikilink")[藝術博物館之一](../Page/藝術博物館.md "wikilink")。\[1\]主建築物面積約有8公頃，展出面積有20多公頃。館藏超過二百萬件藝術品\[2\]，整個博物館被劃分為十七個館部。\[3\]主除了主館外，還有位於[曼哈頓上城區](../Page/曼哈頓上城.md "wikilink")[修道院博物館的第二分館](../Page/修道院博物館.md "wikilink")。那裡主要展出中世紀的藝術品。

在眾多永久藝術收藏品中，包括许多出众的古典藝術品、[古埃及藝術品](../Page/古埃及.md "wikilink")、几乎所有歐洲大師的油畫及大量美國[視覺藝術和現代藝術作品](../Page/視覺藝術.md "wikilink")。博物館還收藏有大量的[非洲](../Page/非洲.md "wikilink")、[亞洲](../Page/亞洲.md "wikilink")、[大洋洲](../Page/大洋洲.md "wikilink")、[拜占庭和](../Page/拜占庭.md "wikilink")[伊斯蘭藝術品](../Page/伊斯蘭.md "wikilink")。\[4\]博物馆同时也是世界[樂器](../Page/樂器.md "wikilink")、[服裝](../Page/服裝.md "wikilink")、[飾物](../Page/飾物.md "wikilink")、[武器](../Page/武器.md "wikilink")、[盔甲的大總匯](../Page/盔甲.md "wikilink")。\[5\]
博物館的室內設計模仿不同历史时期的风格，从1世紀的羅馬風格延续至現代美國。\[6\]

大都會藝術博物館由一群美國公民於1870年發起构建。當時的發起人包括了商人、理財家、卓越的藝術家與思想家。他們期望博物館能夠给予美國公民有关藝術與藝術教育的熏陶。\[7\]最後大都會藝術博物館於1872年2月20日開幕，當時的博物館位於[第五大道](../Page/第五大道.md "wikilink")681號。

在2007年時，大都會藝術博物館已經有接近四分之一英里長及佔地二百萬平方英尺，是博物館最初大小的二十倍。\[8\]

## 歷史

[Metropolitan_opening_reception.jpg](https://zh.wikipedia.org/wiki/File:Metropolitan_opening_reception.jpg "fig:Metropolitan_opening_reception.jpg")》出版\]\]

1870年4月13日[紐約州議會為大都會藝術博物館頒發一項法令](../Page/紐約州議會.md "wikilink")，“為了在城市中建立和維持一个藝術博物館和圖書館，為了鼓勵和發展美術研究和藝術在生產和自然生活的應用，為了增加相关科目的一般知識，也為了向公众提供教育和娛樂。”\[9\]

該博物館於1872年2月20日首次開放，當時位於紐約市第五大道681號的一幢大廈之內。鐵路职员将個人藝術收藏品提供给博物館作為最早的館藏，並且担任首任馆长。而出版商成了創立時期的監督人。藝術家擔任博物館的共同創辦人。在他們的指導下，博物館的館藏，由最初的羅馬石石棺和大部分來自歐洲的174幅繪畫，迅速增長並填滿了可用的空間。1873年，博物館採購了[盧吉·帕爾瑪·德·塞斯諾拉](../Page/盧吉·帕爾瑪·德·塞斯諾拉.md "wikilink")（Luigi
Palma di
Cesnola）所收集的[賽浦路斯文物](../Page/賽浦路斯.md "wikilink")。同年，博物館從第五大道搬遷到第14街128號道格拉斯大廈。可是新館址很快就不敷應用。

1871年，博物館與紐約市商議後，得到中央公園東側的一片土地作為永久館址。其建築的紅磚[新歌德式外型由美國建築師](../Page/新歌德式建築.md "wikilink")[卡爾弗特·沃克斯](../Page/卡爾弗特·沃克斯.md "wikilink")（Calvert
Vaux）和合夥人Jacob Wrey
Mould設計。\[10\]沃克斯的大膽新歌德式設計不獲欣賞，在建築完成時被認為設計過時，更被博物館的主席評為「一個錯誤」。\[11\]

博物館自此不斷擴建，譬如由[理查德·莫里斯·亨特所設計的新古典學院派](../Page/理查德·莫里斯·亨特.md "wikilink")[布雜藝術的立面](../Page/布雜藝術.md "wikilink")。\[12\]這個擴展部分由1912年開始興建，到1926年完工，採用了來自印第安那州的灰色石灰石。南翼建於1911年，北翼建於1913年。兩翼都是由[麥金米德與懷特事務所](../Page/麥金米德與懷特事務所.md "wikilink")（McKim,
Mead &
White）負責興建。1963年，在作家[安德烈·馬爾羅驅使下](../Page/安德烈·馬爾羅.md "wikilink")，成千上萬的訪客到博物館一睹《[蒙娜麗莎](../Page/蒙娜麗莎.md "wikilink")》。\[13\]

1971年，新建築計劃得到批准並且交給凱文·羅區－約翰·汀克羅事務所負責，工程為期超過20年。\[14\]計劃目標是為了市民更容易接近展品，研究人員更方便使用設施，讓整個博物館更有趣更有教育意義。

在很多的新計劃中，也包括了於1975完工的「羅伯特雷曼翼」，那裡珍藏了很多歐洲油畫大師的名作，當中包括了[印象派](../Page/印象派.md "wikilink")、[後印象派畫家](../Page/後印象派.md "wikilink")；安置[丹鐸廟的](../Page/丹鐸廟.md "wikilink")「賽克勒翼」，完成於1978年；「美國翼」於1980年對外開放，那裡全面展示美國日常生活藝術歷史；「邁克爾C洛克菲勒翼」，自從1982年，收藏了來自非洲、大洋洲、美洲的文物；「莉拉艾奇遜華萊士翼」，自1987年收藏了很多現代藝術作品；而「亨利河克拉維斯翼」則展出自文藝復興至20世紀初的歐洲雕塑與裝飾藝術。

這些新建築物落成後，博物館在這些空間裡重新組織收藏品。1998年6月，韓國藝術廊對外開放，完整了亞洲藝術的展覽系列。1999年，近東方文物、希臘與羅馬文物展館的重新裝修已經展開。展覽古希臘藝術的「羅伯特和蕾妮展覽廳」在1996年開幕，新希臘畫廊於1999年4月開幕，而[塞浦路斯畫廊於](../Page/塞浦路斯.md "wikilink")2000年開幕。

2006年，博物館的建築物總長度差不多400米，佔地180000平方米，比1880年代的館址大20倍。\[15\]

## 館藏

大都會的永久收藏品被分為十九個館部，每個館部都有專門的館長、修復人員及研究人員。\[16\]
永久收藏品的代表有古代物品及[古埃及的藝術品](../Page/古埃及.md "wikilink")，歐洲名師的[油畫及](../Page/油畫.md "wikilink")[雕塑](../Page/雕塑.md "wikilink")，還有大量的美洲及[現代藝術展品](../Page/現代藝術.md "wikilink")。\[17\]博物館亦收藏了不同的[樂器](../Page/樂器.md "wikilink")、[服裝](../Page/服裝.md "wikilink")、[飾物](../Page/飾物.md "wikilink")，以及古代的[武器及](../Page/武器.md "wikilink")[盔甲](../Page/盔甲.md "wikilink")。\[18\]博物館的畫廊永久地配置了一些著名的內部裝飾，從1世紀羅馬到現代美國的設計都有。
\[19\]

大都會藝術博物館有相当出色的中国藏品，据研究者指出，“大都會藝術博物館有号称是中国以外最好的中国佛教雕塑收藏，在塞克勒（Sackler）厅中摆满了大大小小佛像，包括山西广胜寺的元代彩画大约60平方米，以及大量从山西掠走的佛头。”\[20\]

除了常設展覽外，大都會藝術博物館還全年組織與舉辦大型的巡迴展覽。\[21\]

[詹姆斯·羅瑞墨](../Page/詹姆斯·羅瑞墨.md "wikilink")（*James J.
Rorimer*）是博物館1955年到1966年的主管。\[22\]在他死後，Thomas
Hoving成為下一任的主管，他的任期為1967年3月17日到1977年6月30日。曾長時間擔任管理員的[菲力普·德·蒙特貝羅](../Page/菲力普·德·蒙特貝羅.md "wikilink")，於2008年1月8日上任主管，並計劃於2008年年尾時退任。截至2009年1月，現任博物館主管是[托馬斯·P·坎貝爾](../Page/托馬斯·P·坎貝爾.md "wikilink")。\[23\]\[24\]

### 美國裝飾藝術館部

美國裝飾藝術館部收藏了大約12000件美國裝飾藝術品。它們都來自17世紀到20世紀初。雖然大都會藝術博物館首批收藏品都是1909年美國理財家[羅素·賽奇的妻子](../Page/羅素·賽奇.md "wikilink")－[馬格麗特·奧麗維婭·斯洛卡姆·塞奇所捐贈的](../Page/馬格麗特·奧麗維婭·斯洛卡姆·塞奇.md "wikilink")，但直到1934年才有專門保存美國裝飾藝術的部門。該館部獲得的其中一個獎項就是因為收藏了大量的美國彩繪玻璃。這是世界上最豐富最完備的館藏，有些藝術品來自[蒂芙尼公司](../Page/蒂芙尼公司.md "wikilink")。該館部的25個代表不同時間的展廳都配以當時的擺設與室內設計。現在收藏了很多銀器，都是保羅里維的收藏，而且是[蒂芙尼公司的出品](../Page/蒂芙尼公司.md "wikilink")。\[25\]

### 美國油畫與雕塑館部

[Washington_Crossing_the_Delaware_by_Emanuel_Leutze,_MMA-NYC,_1851.jpg](https://zh.wikipedia.org/wiki/File:Washington_Crossing_the_Delaware_by_Emanuel_Leutze,_MMA-NYC,_1851.jpg "fig:Washington_Crossing_the_Delaware_by_Emanuel_Leutze,_MMA-NYC,_1851.jpg")所畫的《[華盛頓橫渡特拉华河](../Page/華盛頓橫渡特拉华河.md "wikilink")》\]\]
自從博物館創立，特別注意收藏美國藝術。首先成為館藏的是1870年所購得的[希倫·包爾斯所製作富有寓言的雕塑](../Page/希倫·包爾斯.md "wikilink")－《加州》。現在還在博物館內展出。數十年來，美國油畫與雕塑館部的收藏量不斷增加。現在多達1000幅畫，600個雕塑，2600幅繪圖，涵蓋了殖民時期到20世紀初期。最著名的有《[華盛頓橫渡特拉华河](../Page/華盛頓橫渡特拉华河.md "wikilink")》，由[吉伯特·斯圖亞特與](../Page/吉伯特·斯圖亞特.md "wikilink")[埃玛纽埃尔·洛伊茨所畫](../Page/埃玛纽埃尔·洛伊茨.md "wikilink")，描繪了當時[喬治·華盛頓橫渡](../Page/喬治·華盛頓.md "wikilink")[德拉瓦河流的景象](../Page/德拉瓦州.md "wikilink")。除此之外，還有著名美國畫家[溫斯洛·荷馬](../Page/溫斯洛·荷馬.md "wikilink")（Winslow
Homer）、[喬治·加勒伯·賓漢](../Page/喬治·加勒伯·賓漢.md "wikilink")（George Caleb
Bingham）、約翰·辛格·薩金特（John Singer
Sargent）、[惠斯勒](../Page/詹姆斯·阿博特·麥克尼爾·惠斯勒.md "wikilink")（James
McNeill Whistler）及[湯姆·艾金斯](../Page/湯姆·艾金斯.md "wikilink")（Thomas
Eakins）。\[26\]

### 古代近東藝術館部

[Neoassyrian.jpg](https://zh.wikipedia.org/wiki/File:Neoassyrian.jpg "fig:Neoassyrian.jpg")石像，而左方的是[捨杜](../Page/捨杜.md "wikilink")。\[27\]\]\]
自開幕以來，大都會藝術博物館開始收集近東地區的古代藝術與文物。現在有關的館藏已經超過7000件，譬如[楔形文字的泥版與](../Page/楔形文字.md "wikilink")[印章](../Page/印章.md "wikilink")。有些文物來自[新石器時代](../Page/新石器時代.md "wikilink")，有些則來自[薩珊王朝](../Page/薩珊王朝.md "wikilink")，還有屬於[古代美索不達米亞的王朝](../Page/兩河文明.md "wikilink")，譬如[蘇美爾](../Page/蘇美爾.md "wikilink")、[赫梯](../Page/赫梯.md "wikilink")、[亞述](../Page/亞述.md "wikilink")、[巴比倫尼亞](../Page/巴比倫尼亞.md "wikilink")，有些則屬於[埃蘭語文化](../Page/埃蘭語.md "wikilink")，還有大量來自[青銅時代](../Page/青銅時代.md "wikilink")。當中最著名的就是來自[新亞述時期的君主](../Page/亞述.md "wikilink")－[亞述那西爾帕二世所興建的西北王宮的巨大石像](../Page/亞述那西爾帕二世.md "wikilink")－[拉瑪蘇](../Page/拉瑪蘇.md "wikilink")（lammasu）。\[28\]

### 武器和盔甲館部

[Middle_Age_Main_Hall.jpg](https://zh.wikipedia.org/wiki/File:Middle_Age_Main_Hall.jpg "fig:Middle_Age_Main_Hall.jpg")
[武器和](../Page/武器.md "wikilink")[盔甲是博物館最流行的收藏品](../Page/盔甲.md "wikilink")。在一樓，收藏品以獨特的“巡遊”方式展示在馬背上身穿裝甲的人物，成為博物館的武器和盔甲展廊中最易識別的印象。該部的重點是“傑出的工藝和裝飾”，這些裝飾物品只作展示之用而沒有實際用途。雖然收藏品集中在中世紀晚期歐洲和日本，從公元前4世紀到19世紀都有。然而，並非只展出武器和盔甲所代表的文化；它的收集範圍幾乎比任何其他部門的地理跨越性更大，包括了埃及、古希臘、羅馬帝國、古代近東地區、非洲、大洋洲和美洲，以及19和20世紀的美國槍械（特別是柯爾特槍）。在15,000多件文物中，大部份是給國王和王子所用，包括屬於英國[亨利八世](../Page/亨利八世.md "wikilink")，法國[亨利二世和德國](../Page/亨利二世_\(法蘭西\).md "wikilink")[費迪南一世的裝甲](../Page/斐迪南一世_\(神聖羅馬帝國\).md "wikilink")。\[29\]

### 非洲、大洋洲、美洲藝術館部

[Queen_Mother_Pendant_Mask-_Iyoba_MET_DP231460.jpg](https://zh.wikipedia.org/wiki/File:Queen_Mother_Pendant_Mask-_Iyoba_MET_DP231460.jpg "fig:Queen_Mother_Pendant_Mask-_Iyoba_MET_DP231460.jpg")[貝寧帝國法院](../Page/貝寧帝國.md "wikilink")，由象牙與銅所製的面具\]\]

雖然自從1882年開館以來已經搜購了一批[祕魯文物](../Page/祕魯.md "wikilink")，但仍未積極蒐集來自非洲、大洋洲、美洲藝術品。直到1969年，美國商人及慈善家－[納爾遜·洛克斐勒捐贈了](../Page/纳尔逊·洛克菲勒.md "wikilink")3000件藝術品給博物館，情況才開始轉變。今天，博物館已經收藏了超過11000件來自[撒哈拉以南的非洲](../Page/撒哈拉以南非洲.md "wikilink")、太平洋的島嶼、美洲的文物，並且為此在博物館南端興建了佔地4000平方米的「洛克斐勒翼」。這批文物包括了來自澳洲土著的壁畫、由[新幾內亞](../Page/新幾內亞.md "wikilink")[阿斯馬族所雕刻的一群](../Page/阿斯馬族.md "wikilink")15呎高的紀念柱、由德國藝術商人[卡士·皮爾士所捐贈的一批無價文物](../Page/卡士·皮爾士.md "wikilink")，它們來自[尼日利亞](../Page/尼日利亞.md "wikilink")[貝寧帝國的法院](../Page/貝寧帝國.md "wikilink")。\[30\]這些文物，從珍貴金屬到豪猪的刺都有，無疑是館內最多樣化的館藏。\[31\]

### 亞洲藝術館部

[Tsunami_by_hokusai_19th_century.jpg](https://zh.wikipedia.org/wiki/File:Tsunami_by_hokusai_19th_century.jpg "fig:Tsunami_by_hokusai_19th_century.jpg")的《[神奈川沖浪裡](../Page/神奈川沖浪裡.md "wikilink")》\]\]
大都會博物館有關亞洲藝術的館藏是西方國家中最大和最全面的。亞洲每個文明都有其代表性的優秀作品，帶來了差不多半個世界的，無論在質或量都是無可比擬的藝術傳統體驗。超過60000件物品，自公元前2000年到20世紀初，包括來自來自東亞、南亞、[東南亞和](../Page/東南亞.md "wikilink")[喜馬拉雅山區的](../Page/喜馬拉雅山.md "wikilink")[油畫](../Page/油畫.md "wikilink")、[版畫](../Page/版畫.md "wikilink")、[書法](../Page/書法.md "wikilink")、[雕塑](../Page/雕塑.md "wikilink")、金屬製品、[陶瓷](../Page/陶瓷.md "wikilink")、[油漆](../Page/油漆.md "wikilink")、裝飾藝術作品和紡織品。該館部是以書法和繪畫作品而著名，無論巨大的風景和模仿自然的，以及日本屏風和版畫，都是集功能儀式豪華於一身。來自南亞及東南亞的石雕和金屬雕塑，[尼泊爾和](../Page/尼泊爾.md "wikilink")[西藏等其他地區的早期繪畫](../Page/西藏.md "wikilink")，都是館藏的優勝之處。其中亞斯特庭院更是根據[中国](../Page/中国.md "wikilink")[蘇州的](../Page/蘇州.md "wikilink")[網師園而設計](../Page/網師園.md "wikilink")，作為展覽中國木製家具。\[32\]

### 服裝研究館部

[Robe_à_la_française_1740s.jpg](https://zh.wikipedia.org/wiki/File:Robe_à_la_française_1740s.jpg "fig:Robe_à_la_française_1740s.jpg")
服裝藝術博物館是由[愛蓮·伯恩斯坦](../Page/愛蓮·伯恩斯坦.md "wikilink")（Aline
Bernstein）與[愛蓮·路易松](../Page/愛蓮·路易松.md "wikilink")（Irene
Lewisohn）所成立。\[33\]1937年，它併入大都會藝術博物館，成為服裝研究館部。今天，它收藏了超過80000服裝與飾物。由於這些館藏十分脆弱，不會作長期展覽。因而，每年在藝術廊展出兩次，每次都配以特定的設計師或者主題。去年，就展示了由[可可·香奈尔與](../Page/可可·香奈尔.md "wikilink")[詹尼·范思哲所設計的服裝](../Page/詹尼·范思哲.md "wikilink")，引來大量訪客。至於每年由美國時裝雜誌Vogue的主編－Anna
Wintour所主持的比賽是時裝界的盛事。譬如2007年的700張入場券，每張價值高達6500美元。\[34\]

### 繪畫及印刷品館部

[Dürer_Melancholia_I.jpg](https://zh.wikipedia.org/wiki/File:Dürer_Melancholia_I.jpg "fig:Dürer_Melancholia_I.jpg")所畫的《梅倫科利亞一世》（Melencolia
I）\]\]

雖然其他館部都藏有大量繪畫及印刷品，但是繪畫及印刷品館部專門收集北美與西歐在[中世紀之後的作品](../Page/中世紀.md "wikilink")。現在，該館部藏有超過11000幅繪畫，12000種圖冊。自從1880年[康內留斯·范德比爾特逝世後所捐獻的](../Page/康內留斯·范德比爾特.md "wikilink")670幅繪畫，館藏越來越豐富，展出歐洲繪畫大師的素描與畫稿更多於該大師的油畫。譬如[米開朗基羅](../Page/米開朗基羅.md "wikilink")、[達文西](../Page/達文西.md "wikilink")、[林布蘭](../Page/林布蘭.md "wikilink")、[安東尼·凡·戴克](../Page/安東尼·凡·戴克.md "wikilink")、[阿爾布雷希特·杜勒](../Page/阿爾布雷希特·杜勒.md "wikilink")、[艾德嘉·竇加等等](../Page/艾德嘉·竇加.md "wikilink")。

### 古埃及藝術館部

[Standing_Hippopotamus_MET_DP248993.jpg](https://zh.wikipedia.org/wiki/File:Standing_Hippopotamus_MET_DP248993.jpg "fig:Standing_Hippopotamus_MET_DP248993.jpg")
大都會藝術博物館所收集的古埃及藝術品，是開羅以外最豐富的館藏。大約36000件展品包括了藝術品、歷史文物、對文化十分重要的物品，林林種種，來自[舊石器時代到](../Page/舊石器時代.md "wikilink")[古羅馬時期都有](../Page/古羅馬時期.md "wikilink")。

自1906年，公眾對古埃及文化日益增加，為了回應訴求，博物館在埃及開始了長達35年的考古工作。一半以上的館藏都是來自這些考古工作。今天，幾乎整個館藏在32個展廳和8個研究展廳以時序來展出。總體而言，館藏反映了美學價值、歷史、宗教信仰和古埃及人的日常生活。\[35\]

## 歷任主管

[Cesnola.jpg](https://zh.wikipedia.org/wiki/File:Cesnola.jpg "fig:Cesnola.jpg")

迄今（2009年），博物館共有9位主管。第一位是義大利籍美國陸軍將軍－塞斯諾拉。現任主管是2009年1月才上任的[托馬斯·坎貝爾](../Page/托馬斯·坎貝爾.md "wikilink")。他取代了1977年被委任的前主管－菲力普·德·蒙特貝羅。這位前主管原籍法國，曾是休斯敦美術博物館主管。

### 塞斯諾拉

[塞斯諾拉](../Page/塞斯諾拉.md "wikilink")（1832-1904）在1879年至1904年出任首屆主管。出身在軍事世家的他曾參與[奧地利軍的](../Page/奧地利.md "wikilink")[克里米亞戰爭](../Page/克里米亞戰爭.md "wikilink")。1860年，移居到美國。他在紐約創立軍官學校。在[美國內戰期間](../Page/美國內戰.md "wikilink")，擔任陸軍騎兵隊上校，並且獲得英勇勳章。他的軍人生涯到1865年結束。當時他被委任為駐[塞浦路斯的領事](../Page/塞浦路斯.md "wikilink")。他熱愛考古學，促使他在當地展開挖掘工作，期間發掘大約30000件文物。這些文物被博物館收購了。1879年，他開始出任首屆主管。這次委任惹起爭論，因為很多歷史學家認為他的挖掘工作等於掠奪行為。\[36\]\[37\]

### 克拉克

英國[卡斯帕·克拉克爵士](../Page/卡斯帕·克拉克.md "wikilink")（1846年 -
1911年）由1904年至1910年擔任第二任主管。\[38\]1867年，他首先在[倫敦的](../Page/倫敦.md "wikilink")[南肯辛頓博物館工作](../Page/南肯辛頓博物館.md "wikilink")。1896年，開始擔任領導角色。1910年因健康理由辭職，返回倫敦。

### 羅賓遜

自1910年至1931年，主管一職由[愛德華·羅賓遜擔任](../Page/愛德華·羅賓遜.md "wikilink")。他是考古學專家，擅長於古希臘文物。1885年成為[波士頓美術館的文物保護員](../Page/波士頓美術館.md "wikilink")，1902年晉升為該館主管。不久便加入了大都會藝術博物館，擔任處理主管。1910年晉升為主管。\[39\]\[40\]

### 文洛克

[赫伯特·文洛克是大名鼎鼎的](../Page/赫伯特·文洛克.md "wikilink")[埃及學家](../Page/埃及學.md "wikilink")，他在1932年到1939年出任主管，他負責埃及藝術的考古工作，特別考察[底比斯地區的藝術](../Page/底比斯.md "wikilink")。
\[41\]

### 泰勒

自從1940年到1955年，[弗朗西斯·亨利·泰勒出任主管](../Page/弗朗西斯·亨利·泰勒.md "wikilink")。在此之前，他曾先後擔任[費城藝術博物館主管](../Page/費城藝術博物館.md "wikilink")，[麻省](../Page/麻省.md "wikilink")[伍斯特藝術博物館主管](../Page/伍斯特藝術博物館.md "wikilink")。他慢慢建立自己的構思，認為博物館不是單純藝術品的累積，而且是學術機構或者一個公共服務。他經過多番努力，把博物館入場人數增加一倍，達到每年230萬人次。\[42\]

### 羅瑞墨

[羅瑞墨自從](../Page/羅瑞墨.md "wikilink")1955年開始擔任主管，直到1966年去世為止。他貢獻了自己的一生。1927年，他完成自己的研究後，便加入大都會藝術博物館的裝飾藝術館部，成為一名助理。1934年，晉升為中世紀藝術館部館長。1943年，因為第二次世界大戰而短暫離開博物館，但協助美軍保存和研究曾被納粹黨偷去的藝術品。1949年，返回博物館，出任修道院的理事直到1955年。

### 霍文

[湯瑪斯·霍文生於](../Page/湯瑪斯·霍文.md "wikilink")1931年，1959年在[普林斯頓大學完成博士課程後](../Page/普林斯頓大學.md "wikilink")，加入中世紀藝術館部，1965年成為該館部館長。1966年加入紐約市長[約翰·林得瑟的團隊](../Page/約翰·林得瑟.md "wikilink")。可是當收到羅瑞墨的死訊，便返回博物館擔任主管。

### 蒙特貝羅

菲力普·德·蒙特貝羅在1977年至2008年間，擔任博物館主管。這位出生於巴黎的法國人是蒙特貝羅公爵的後人。1951年跟從家人抵達美國，1955年入籍美國。他在紐約就讀法國人學校攻讀學士學位。1958年畢業於[哈佛大學](../Page/哈佛大學.md "wikilink")。後來在[紐約大學的藝術學院取得博士學位](../Page/紐約大學.md "wikilink")。1963年，加入博物館，成為館部助理，然後晉升為歐洲油畫館部的準館長。1969年至1974年，被委任為[休士頓藝術博物館的主管](../Page/休士頓藝術博物館.md "wikilink")。1974年，返回大都會藝術博物館擔任副主管，統籌主管及教育事務。在他的領導下，博物館擴展了一倍，19世紀歐洲油畫的藏量更豐。
\[43\]

## 參考資料

## 延伸閱讀

  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [大都會藝術博物館官方網站](http://www.metmuseum.org)

[Category:曼哈顿博物馆](../Category/曼哈顿博物馆.md "wikilink")
[Category:1872年完工建築物](../Category/1872年完工建築物.md "wikilink")
[Category:美國地標](../Category/美國地標.md "wikilink")
[Category:理查德·莫里斯·亨特設計的建築](../Category/理查德·莫里斯·亨特設計的建築.md "wikilink")

1.

2.

3.

4.
5.
6.
7.  [Brief History of The
    Museum](http://www.metmuseum.org/press_room/full_release.asp?prid=%7B8EAB3ECC-C29C-4B26-ABC0-70D58D45E5E3%7D)


8.  [The Metropolitan Museum of Art at
    HumanitiesWeb](http://www.humanitiesweb.org/human.php?s=g&p=a&a=i&ID=1138)

9.  Disturnell, John [*New York as it was and as it
    is*](http://books.google.com/books?id=Z16LK1yEhBUC&pg=RA1-PA101), D.
    Van Nostrand, New York, 1876. "Metropolitan Museum of Art" page 101.

10. [Visitor's Information at the Metropolitan Museum of Art
    website](http://www.metmuseum.org/visitor/faq_hist.htm#arch)

11.

12.

13.

14.

15.  [Le *Metropolitan Museum of Art* sur
    HumanitiesWeb](http://www.humanitiesweb.org/human.php?s=g&p=a&a=i&ID=1138)

16.
17.

18.

19.

20. [略特：盗掘与盗购－－关于美国推迟对中国文物进口限制的禁令](http://humanities.cn/modules/article/view.article.php?91)

21. [Current Special
    Exhibitions](http://www.metmuseum.org/special/index.asp?HomePageLink=special_l)


22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.