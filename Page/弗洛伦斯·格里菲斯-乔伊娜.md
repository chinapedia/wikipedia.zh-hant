**弗洛伦斯·格里菲斯-乔伊娜**（，）是[美国著名](../Page/美国.md "wikilink")[田径运动员](../Page/田径.md "wikilink")，暱稱「Flo-Jo」、「花蝴蝶」。

格里菲斯1959年出生于[洛杉矶](../Page/洛杉矶.md "wikilink")，原名**多洛雷斯·弗洛伦斯·格里菲斯**（Delorez
Florence
Griffith）。1983年，她在首届[世界田径锦标赛上获得](../Page/世界田径锦标赛.md "wikilink")[100米短跑第](../Page/100米短跑.md "wikilink")4名，次年她在[洛杉矶奥运会上获得](../Page/1984年夏季奥林匹克运动会.md "wikilink")100米的亚军。由於她在賽場上的繽紛耀眼的服裝、光鮮亮麗的彩繪指甲，使得她被封為**花蝴蝶**，其后她与该届奥运会男子[三级跳远冠军阿尔](../Page/三级跳远.md "wikilink")·乔伊纳结婚，并一度隐退。

1987年，格里菲斯复出，并在当年的世锦赛上获得了[200米短跑的亚军](../Page/200米短跑.md "wikilink")。次年，在美国奥运选拔赛上，格里菲斯令人吃惊的在100米比赛中跑出10秒49的成绩，这个成绩时至今日仍是[世界纪录](../Page/世界纪录.md "wikilink")。但是令人怀疑的是，当天其他项目的比赛中都测到了超过世界纪录认证标准（顺风2米/秒）的风速，但是唯独格里菲斯创纪录时风速测试仪显示风速为0米，因此包括格里菲斯教练在内的很多人都认为格里菲斯的这个纪录实际上是借助了顺风的帮助才创下的。

在[1988年汉城奥运上](../Page/1988年夏季奥林匹克运动会.md "wikilink")，格里菲斯获得了100米、200米、4x100米接力三枚金牌和4x400米接力银牌，并以21秒34的成绩创造了200米世界纪录，这个世界纪录同样也保持到了现在。

该届奥运会后，格里菲斯退役，从事时装设计，她著名的作品包括她自己的单腿比赛服和[NBA](../Page/NBA.md "wikilink")[-{zh-hans:印第安纳步行者;zh-hk:印第安納溜馬;zh-tw:印第安納溜馬;}-比赛服](../Page/印第安纳步行者队.md "wikilink")。

1998年9月21日，格里菲斯在睡梦中猝死。据尸体解剖显示，她死于[癫痫突发而导致的](../Page/癫痫.md "wikilink")[窒息](../Page/窒息.md "wikilink")。格里菲斯的病历显示，她曾经于1990年发作过一次癫痫，并在93年和94年接受过癫痫治疗。但是，仍有人将其猝死归咎于[兴奋剂](../Page/兴奋剂.md "wikilink")。

格里菲斯的成绩一直处于兴奋剂的阴影下，她在1988年前的最好成绩仅为10.98秒，而在1989年[国际田径联合会决定实行飞行药检后](../Page/国际田径联合会.md "wikilink")，格里菲斯立即宣布退役。这些都加深了外界的怀疑。

## 參考資料

## 外部連結

  -
  - [官方网站](https://web.archive.org/web/20180507095250/http://www.florencegriffithjoyner.com/)

  - [Florence Griffith Joyner's U.S. Olympic Team
    bio](https://archive.is/20070814233913/http://www.usolympicteam.com/26_38163.htm)

  - [Florence Griffith Joyner at
    Find-A-Grave](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=6706)

  - [Florence Griffith Joyner is breaking 100m World Record.
    Video](https://web.archive.org/web/20070927050046/http://www.sprintic.com/worldrecordsvideo/100m_women_1049_griffith_joyner/)

  - [Florence Griffith Joyner is breaking 200m World Record.
    Video](https://web.archive.org/web/20070927051115/http://www.sprintic.com/worldrecordsvideo/200m_women_2134_griffith_joyner_1988/)

[Category:美国田径运动员](../Category/美国田径运动员.md "wikilink")
[Category:美國奧運田徑運動員](../Category/美國奧運田徑運動員.md "wikilink")
[Category:田径项目世界记录保持者](../Category/田径项目世界记录保持者.md "wikilink")
[Category:美國奧林匹克運動會金牌得主](../Category/美國奧林匹克運動會金牌得主.md "wikilink")
[Category:美國奧林匹克運動會銀牌得主](../Category/美國奧林匹克運動會銀牌得主.md "wikilink")
[Category:1984年夏季奧林匹克運動會獎牌得主](../Category/1984年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1988年夏季奧林匹克運動會獎牌得主](../Category/1988年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1984年夏季奧林匹克運動會田徑運動員](../Category/1984年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:1988年夏季奧林匹克運動會田徑運動員](../Category/1988年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:奥林匹克运动会田径金牌得主](../Category/奥林匹克运动会田径金牌得主.md "wikilink")
[Category:奥林匹克运动会田径银牌得主](../Category/奥林匹克运动会田径银牌得主.md "wikilink")
[Category:世界田徑錦標賽獎牌得主](../Category/世界田徑錦標賽獎牌得主.md "wikilink")
[J](../Category/死于癫痫.md "wikilink")