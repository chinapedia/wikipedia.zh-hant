**折扇叶科**又名香灌木科，只有1[属](../Page/属.md "wikilink")—[折扇叶属](../Page/折扇叶属.md "wikilink")（*Myrothamnus*）共2[种](../Page/种.md "wikilink")，都是生长在[非洲东部](../Page/非洲.md "wikilink")、西南沿海和[马达加斯加岛西部](../Page/马达加斯加岛.md "wikilink")。

本科[植物是芳香的](../Page/植物.md "wikilink")[灌木](../Page/灌木.md "wikilink")，单[叶对生](../Page/叶.md "wikilink")，有长叶鞘，有托叶，叶边有齿，在旱季叶子干燥，一旦降水出现，立即可以膨胀恢复生气。

1981年的[克朗奎斯特分类法将本](../Page/克朗奎斯特分类法.md "wikilink")[科分入](../Page/科.md "wikilink")[金缕梅目](../Page/金缕梅目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为无法将其分入任何一](../Page/APG_分类法.md "wikilink")[目](../Page/目.md "wikilink")，直接放到[核心真双子叶植物分支下面](../Page/核心真双子叶植物分支.md "wikilink")，2003年经过修订的[APG
II
分类法将其编入新设立的](../Page/APG_II_分类法.md "wikilink")[大叶草目](../Page/大叶草目.md "wikilink")，但认为可以选择性地与[大叶草科合并](../Page/大叶草科.md "wikilink")。

## 外部链接

  - 在[Stevens, P. F. （2001年）APG
    网站](http://www.mobot.org/MOBOT/research/APweb)中的[折扇叶科](http://www.mobot.org/MOBOT/Research/APweb/orders/gunneralesweb.htm#Myrothamnaceae)
  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[折扇叶科](http://delta-intkey.com/angio/www/myrotham.htm)
  - [NCBI中的折扇叶科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=83221&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL中的折扇叶科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Myrothamnaceae)

[\*](../Category/折扇叶科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")