**丹尼爾·博內拉**（，），乃一名[意大利足球運動員](../Page/意大利.md "wikilink")，司職後衛，出身[布雷西亞青訓系統](../Page/布雷西亞足球俱樂部.md "wikilink")，現效力[西甲球會](../Page/西甲.md "wikilink")[比利亞雷阿爾](../Page/比利亞雷阿爾足球俱樂部.md "wikilink")。曾效力[意甲球會](../Page/意甲.md "wikilink")[帕爾馬和](../Page/帕爾馬足球會.md "wikilink")[AC米兰](../Page/AC米兰.md "wikilink")。

## 生平

博内拉之前曾效力過意甲一些中下游球會，如[布雷西亞和](../Page/布雷西亞足球俱樂部.md "wikilink")[帕爾馬](../Page/帕爾馬足球會.md "wikilink")，亦曾代表過意大利國家青年隊出賽，贏得過2004年歐洲青年錦標賽冠軍，隨後他又隨義大利隊在[雅典奧運會上奪得銅牌](../Page/雅典奧運會.md "wikilink")。他代表過正式國家隊11次，由於國家隊已有[尼斯達和](../Page/亞歷山德羅·內斯塔.md "wikilink")[簡拿華路等星級球員](../Page/簡拿華路.md "wikilink")，他未有參與2006年世界杯任何賽事。

2006年他加盟AC米蘭，由於傷患影響，加盟首季內，聯賽出場次數只稍逾一半。倘單以名義上說，他則是2007年球會取得歐冠杯的成員之一。至2014/15季尾被AC米蘭放棄，同年8月下旬自由轉會至[比利亞雷阿爾](../Page/比利亞雷阿爾足球俱樂部.md "wikilink")。

## 獎項

  - 歐洲青年錦標賽冠軍：2004
  - 雅典奧運會銅牌：2004
  - 歐洲聯賽冠軍杯：2007
  - 意大利足球甲级联赛冠军：2010-11
  - 意大利超级杯冠军：2011

## 外部链接

[Category:意大利足球運動員](../Category/意大利足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:布雷西亞球員](../Category/布雷西亞球員.md "wikilink")
[Category:帕爾馬球員](../Category/帕爾馬球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:維拉利爾球員](../Category/維拉利爾球員.md "wikilink")
[Category:意大利奧林匹克運動會銅牌得主](../Category/意大利奧林匹克運動會銅牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會獎牌得主](../Category/2004年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會足球運動員](../Category/2004年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:布雷西亞人](../Category/布雷西亞人.md "wikilink")