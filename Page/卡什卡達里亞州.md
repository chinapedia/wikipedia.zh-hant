<table>
<thead>
<tr class="header">
<th><p>卡什卡達里亞州<br />
Qashqadaryo viloyati</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Qashqadaryo_Viloyati_in_Uzbekistan.svg" title="fig:Qashqadaryo_Viloyati_in_Uzbekistan.svg">Qashqadaryo_Viloyati_in_Uzbekistan.svg</a></p></td>
</tr>
<tr class="even">
<td><p>基本統計資料</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/首府.md" title="wikilink">首府</a>:</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/面積.md" title="wikilink">面積</a> :</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人口.md" title="wikilink">人口</a>(2005):<br />
 · <a href="../Page/人口密度.md" title="wikilink">密度</a> :</p></td>
</tr>
<tr class="even">
<td><p>主要<a href="../Page/民族.md" title="wikilink">民族</a>:</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ISO_3166-2.md" title="wikilink">行政區代碼</a>:</p></td>
</tr>
</tbody>
</table>

**卡什卡達里亞州**（[烏茲別克語](../Page/烏茲別克語.md "wikilink")：Qashqadaryo
viloyati）是[烏茲別克十二個州份之一](../Page/烏茲別克.md "wikilink")。它涵蓋28,400平方公里，有人口2,029,000。卡什卡達里亞州下轄14個縣，[首府設於](../Page/首府.md "wikilink")[卡爾希市](../Page/卡爾希.md "wikilink")。

## 地理位置

卡什卡達里亞州位於[烏茲別克南部](../Page/烏茲別克.md "wikilink")。它與以下州份或國家相連（從北開始逆時針）：[撒馬爾罕州](../Page/撒馬爾罕州.md "wikilink")、[納沃伊州](../Page/納沃伊州.md "wikilink")、[布哈拉州](../Page/布哈拉州.md "wikilink")、[土庫曼](../Page/土庫曼.md "wikilink")、[蘇爾漢河州](../Page/蘇爾漢河州.md "wikilink")、[塔吉克](../Page/塔吉克.md "wikilink")。公路全長4,000公里，另外[鐵路](../Page/鐵路.md "wikilink")350公里。

石油产业发达，1972此地建成了当时世界上最大规模天然气加工厂之一 穆巴列克天然气加工厂。

## 參考

  - Umid World

[Category:烏茲別克行政區劃](../Category/烏茲別克行政區劃.md "wikilink")