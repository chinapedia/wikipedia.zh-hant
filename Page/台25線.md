**台25線**，是[台灣的一條](../Page/台灣.md "wikilink")[省道](../Page/台灣省道.md "wikilink")，又稱**鳳林公路**，北起[高雄市](../Page/高雄市.md "wikilink")[鳳山區](../Page/鳳山區.md "wikilink")[台1線](../Page/台1線.md "wikilink")，南至高雄市[林園區與](../Page/林園區.md "wikilink")[台17線交會](../Page/台17線.md "wikilink")，全長18.223公里，其中台25線在[鳳山區與](../Page/鳳山區.md "wikilink")[市道183號](../Page/市道183號.md "wikilink")、[台1戊線共線](../Page/台1線.md "wikilink")，為往來[大寮區](../Page/大寮區.md "wikilink")、[林園區南北向聯絡公路](../Page/林園區.md "wikilink")。是第二短的主線省道（最短的主線省道是[台37線](../Page/台37線.md "wikilink")）。

## 行經行政區域

## 歷史沿革

台25線原為[縣道181號](../Page/縣道181號#歷史沿革.md "wikilink")**左營－汕尾**線之**鳳山－林園**段，全線均在[高雄市內](../Page/高雄市.md "wikilink")。1979年7月1日[高雄市升格改制直轄市](../Page/高雄市_\(1945年-2010年\).md "wikilink")，**左營－鳳山**段解編成市區道路。**鳳山－林園**段在1983年至1986年間進行道路拓寬和路線調整改行外環道路，並於1986年公告編號為今日的台25線。

## 交流道

  - [TW_PHW88.svg](https://zh.wikipedia.org/wiki/File:TW_PHW88.svg "fig:TW_PHW88.svg")[大寮交流道](../Page/大寮交流道.md "wikilink")

[Category:高雄市省道](../Category/高雄市省道.md "wikilink")
[台25線](../Category/台25線.md "wikilink")