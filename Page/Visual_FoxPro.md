**Visual FoxPro**原名**FoxBase**，是[美国](../Page/美国.md "wikilink")[Fox
Software公司推出的](../Page/Fox_Software.md "wikilink")[编程语言](../Page/编程语言.md "wikilink")，主要用于[数据库](../Page/数据库.md "wikilink")，在[DOS上运行](../Page/DOS.md "wikilink")，与[xBase系列相容](../Page/xBase.md "wikilink")。[FoxPro原來是FoxBase的加强版](../Page/FoxPro.md "wikilink")，最高版本曾出过2.6。之后，[Fox
Software被](../Page/Fox_Software.md "wikilink")[微软收购](../Page/微软.md "wikilink")，加以发展，使其可以在[Windows上运行](../Page/Windows.md "wikilink")[16bit版本為FoxPro](../Page/16位元應用程式.md "wikilink")
for Windows Ver 2.6，中文版為FoxPro for Windows
2.5b，之後[32bit版本并且更名为Visual](../Page/32位元應用程式.md "wikilink")
FoxPro。

Visual FoxPro
3.0一推出，就受到广大用户的欢迎。因为xBase类的语言，如[dBase](../Page/dBase.md "wikilink")、[Clipper](../Page/Clipper.md "wikilink")、[InterBase](../Page/InterBase.md "wikilink")、[Paradox等](../Page/Paradox.md "wikilink")，当时还无法在Windows上运行，于是Visual
FoxPro成为市场上的霸主。

微软后来又顺势将Visual FoxPro包入[Visual
Studio中](../Page/Visual_Studio.md "wikilink")。到7.0的时候，為了因應[.NET的发展策略](../Page/.NET.md "wikilink")，又将Visual
FoxPro移出Visual Studio，并将Visual Studio更名为Visual Studio.Net。

目前最新的版本是9.0（发布于2007年）。

2007年3月，在微軟官方網站釋出了一份公告「A Message to the Community」\[1\]，說明未來將不會再推出VFP
10，並且持續VFP 9的支援到2015年。只會在2007年夏季推出SP2。2006年進行的Sedna專案則是增強VFP對[SQL
Server 2005與](../Page/SQL_Server.md "wikilink")[Windows
Vista的支援](../Page/Windows_Vista.md "wikilink")，其他的一些專案則已經開源到[Codeplex](../Page/Codeplex.md "wikilink")。

## 参考文献

## 外部連結

  - 官方網站
      - [官網](http://msdn.microsoft.com/en-ca/vfoxpro/)
      - [台灣官網](http://msdn.microsoft.com/zh-tw/vfoxpro/x)
  - [Visual-Foxpro-Wiki](http://fox.wikis.com/wc.dll?Wiki~VisualFoxProWiki)

## 参见

  - [xBase](../Page/xBase.md "wikilink")
  - [dBase](../Page/dBase.md "wikilink")
  - [Clipper](../Page/Clipper.md "wikilink")
  - [InterBase](../Page/InterBase.md "wikilink")
  - [Paradox](../Page/Paradox.md "wikilink")

{{-}}

[Category:数据库软件](../Category/数据库软件.md "wikilink")
[Category:XBase程式語言家族](../Category/XBase程式語言家族.md "wikilink")

1.  <http://msdn2.microsoft.com/en-us/vfoxpro/bb308952.aspx>