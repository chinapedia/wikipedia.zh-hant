[Court_of_Final_Appeal.jpg](https://zh.wikipedia.org/wiki/File:Court_of_Final_Appeal.jpg "fig:Court_of_Final_Appeal.jpg")的舊址\]\]

**香港特別行政區終審法院**（）是[香港特別行政區法庭制度內的最高](../Page/香港特別行政區.md "wikilink")[上訴法院](../Page/上訴法院.md "wikilink")\[1\]，相當於其他國家的[最高法院](../Page/最高法院.md "wikilink")。終審法院聆訊來自[香港高等法院](../Page/香港高等法院.md "wikilink")（包括[上訴法庭及](../Page/上訴法庭.md "wikilink")[原訟法庭](../Page/香港特別行政區高等法院原訟法庭.md "wikilink")）的[民事及](../Page/民事.md "wikilink")[刑事](../Page/刑事.md "wikilink")[上訴案件](../Page/上訴.md "wikilink")，對香港[司法管轄權範圍內的訴訟有最終審判權](../Page/司法管轄權.md "wikilink")。

香港終審法院從1997年7月1日至2015年9月在[中環](../Page/中環.md "wikilink")[炮台里的](../Page/炮台里.md "wikilink")[前法國外方傳道會大樓辦公](../Page/前法國外方傳道會大樓.md "wikilink")\[2\]。當[香港立法會](../Page/香港立法會.md "wikilink")2011年遷往[立法會綜合大樓](../Page/立法會綜合大樓.md "wikilink")、而[舊最高法院大樓經過復修後](../Page/終審法院大樓.md "wikilink")，終審法院於2015年9月7日遷往舊最高法院大樓\[3\]，而原址亦將移交其他部門或[香港聖公會使用](../Page/香港聖公會.md "wikilink")。\[4\]

## 沿革

1997年[香港主權移交](../Page/香港主權移交.md "wikilink")[中華人民共和國之前](../Page/中華人民共和國.md "wikilink")，香港的司法終審權屬於[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")[樞密院司法委員會](../Page/樞密院司法委員會.md "wikilink")\[5\]。為體現[一國兩制之下香港的獨立](../Page/一國兩制.md "wikilink")**終審權**\[6\]，香港於1997年7月1日成立了自己的終審法院。

根據《[香港特別行政區基本法](../Page/香港特別行政區基本法.md "wikilink")》，香港保留原有法律，繼續實行[普通法制度](../Page/普通法.md "wikilink")。[法院判案時可以引用其他普通法地區](../Page/法院.md "wikilink")（如[英格蘭](../Page/英格蘭.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")）的案例\[7\]，亦可聘用其他普通法地區的司法人員\[8\]。

## 釋法權

香港終審法院對除了《[香港特別行政區基本法](../Page/香港特別行政區基本法.md "wikilink")》外的[香港法律具有最終解釋權](../Page/香港法律.md "wikilink")\[9\]。基本法第158條規定，基本法解釋權屬於中華人民共和國[全國人民代表大會常務委員會](../Page/全國人民代表大會常務委員會.md "wikilink")。

全国人大常委会授權香港法院自行解釋基本法關於香港[自治範圍內的條款](../Page/自治.md "wikilink")。然而，凡涉及中央人民政府管理的事務，或中央和香港特別行政區關係的條款，則必須先由終審法院提請全國人大常委會作出解釋。\[10\]

由於香港終審法院解釋基本法的權力源自全国人大常委会，全国人大常委会對基本法的解釋可推翻香港終審法院對基本法的解釋，但對在解釋前已作出的終審判決不受影響。

## 爭議

1999年1月29日，香港終審法院就[香港居留權爭議作出的裁決](../Page/香港居留權爭議.md "wikilink")，導致第一次[全国人大常委会解释香港特别行政区基本法](../Page/全国人大常委会解释香港特别行政区基本法.md "wikilink")，有人認為釋法大大衝擊了香港的法治，香港司法界發起黑衣遊行抗議人大釋法，此次釋法後，這亦意味著日後具爭議的案件在終審法院作出判決後，除終審法院外，香港政府亦可以提請人大釋法，甚至由全国人大常委会直接主動進行釋法。

## 法官

### 委任

根據《[基本法](../Page/香港基本法.md "wikilink")》及《香港終審法院條例》，終審法院法官須根據[司法人員推薦委員會推薦](../Page/司法人員推薦委員會.md "wikilink")，由[行政長官任命](../Page/香港行政長官.md "wikilink")。行政長官就終審法院法官的任命或免職，須徵得[立法會同意及報](../Page/香港立法會.md "wikilink")[全國人大常委會備案](../Page/全國人大常委會.md "wikilink")。

### 資格

1.  任何以下人士均有資格獲委任為首席法官：
      - 常任法官；
      - 高等法院首席法官、上訴法庭法官或原訟法庭法官；或
      - 在香港以大律師或律師身分執業最少10年的大律師。
2.  任何以下人士均有資格獲委任為常任法官
      - 高等法院首席法官、上訴法庭法官或原訟法庭法官；
      - 在香港以大律師或律師身分執業最少10年的律師。
3.  任何以下人士均有資格獲委任為非常任香港法官，不論他是否通常居住於香港
      - 已退休的高等法院首席法官；
      - 已退休的終審法院首席法官；
      - 已退休的終審法院常任法官；
      - 現職或已退休的上訴法庭法官；或
      - 在香港以大律師或律師身分執業最少10年的大律師。
4.  任何符合以下條件的人士均有資格獲委任為其他普通法適用地區法官
      - 屬其他普通法適用地區的民事或刑事司法管轄權不設限的法院的現職或已退休法官者；
      - 他通常居住於香港以外地方；
      - 他從未在香港擔任過高等法院法官、區域法院法官或常任裁判官。

此外，首席法官須由在外國無居留權的[香港永久性居民中的](../Page/香港永久性居民.md "wikilink")[中國公民擔任](../Page/中國公民.md "wikilink")。

### 任期

在獲委任時未年滿65歲的首席法官及常任法官須於年滿65歲時離任，但任期可由行政長官分別根據司法人員推薦委員會或首席法官的建議延期不超過2次，每次3年。而在獲委任時已達65歲的首席法官及常任法官，其任期為3年，其後可再延續3年。

非常任法官沒有指定退休年齡，他們的任期為3年，但行政長官可根據首席法官的建議，將任期不限次數延續，每次續期為3年。

### 法官列表

#### 首席法官

| 中文姓名                                     | 英文姓名                                                | 任期                   |
| ---------------------------------------- | --------------------------------------------------- | -------------------- |
| [李國能首席法官](../Page/李國能.md "wikilink")     | The Honourable Chief Justice Andrew Li Kwok-nang    | 1997年7月1日－2010年8月31日 |
| **[馬道立首席法官](../Page/馬道立.md "wikilink")** | **The Honourable Chief Justice Geoffrey Ma Tao-li** | 2010年9月1日－           |

#### 常任法官

| 中文姓名                                           | 英文姓名                                                           | 任期                      |
| ---------------------------------------------- | -------------------------------------------------------------- | ----------------------- |
| [烈顯倫法官](../Page/烈顯倫.md "wikilink")             | The Honourable Mr Justice Henry Denis Litton                   | 1997年7月1日－2000年9月14日    |
| [沈澄法官](../Page/沈澄.md "wikilink")               | The Honourable Mr Justice Charles Arthur Ching                 | 1997年7月1日－2000年10月7日    |
| [包致金法官](../Page/包致金.md "wikilink")             | The Honourable Mr Justice Syed Kemal Shah Bokhary              | 1997年7月1日－2012年10月24日   |
| [陳兆愷法官](../Page/陳兆愷.md "wikilink")             | The Honourable Mr Justice Patrick Chan Siu-oi                  | 2000年9月1日－2013年10月20日   |
| 鄧楨（[鄧國楨](../Page/鄧國楨.md "wikilink")）法官         | The Honourable Mr Justice Robert Tang Ching                    | 2012年10月25日－2018年10月24日 |
| '''[李義法官](../Page/李義_\(法官\).md "wikilink") ''' | **The Honourable Mr Justice Roberto Alexandre Vieira Ribeiro** | 2000年9月1日－              |
| **[霍兆剛法官](../Page/霍兆剛.md "wikilink")**         | **The Honourable Mr Justice Joseph Paul Fok**                  | 2013年10月21日－            |
| **[張舉能法官](../Page/張舉能.md "wikilink")**         | **The Honourable Mr Justice Andrew Cheung**                    | 2018年10月25日－            |

#### 非常任法官

根據《香港法例》第484章《香港終審法院條例》第10條《非常任法官人數的限制》規定，非常任法官任何時候都不能超過30名。

| 非常任香港法官                                   | 其他普通法適用地區法官                                           |
| ----------------------------------------- | ----------------------------------------------------- |
| 中文姓名                                      | 英文姓名                                                  |
| [羅弼時爵士](../Page/羅弼時.md "wikilink")        | Sir Denys Tudor Emil Roberts                          |
| [赫健士爵士](../Page/赫健士.md "wikilink")        | Sir Alan Armstrong Huggins                            |
| [麥慕年先生](../Page/麥慕年.md "wikilink")        | Mr Art Michael McMulin                                |
| [康士爵士](../Page/康士.md "wikilink")          | Sir Derek Cons                                        |
| [邵祺先生](../Page/邵祺.md "wikilink")          | Mr William James Silke                                |
| [傅雅德先生](../Page/傅雅德.md "wikilink")        | Mr Kutlu Tekin Fuad                                   |
| [郭樂富先生](../Page/郭樂富.md "wikilink")        | Mr Philip Gerard Clough                               |
| [麥德高先生](../Page/麥德高.md "wikilink")        | Mr Neil MacDougall                                    |
| [鮑偉華爵士](../Page/鮑偉華.md "wikilink")        | Sir Noel Plunkett Power                               |
| [黎守律先生](../Page/黎守律.md "wikilink")        | Mr Gerald Paul Nazareth                               |
| [馬天敏先生](../Page/馬天敏.md "wikilink")        | Mr John Barry Mortimer                                |
| [烈顯倫先生](../Page/烈顯倫.md "wikilink")        | Mr Henry Denis Litton                                 |
| [沈澄先生](../Page/沈澄.md "wikilink")          | Mr Charles Arthur Ching                               |
| **[鄧楨](../Page/鄧楨.md "wikilink")（鄧國楨）法官** | **The Honourable Mr Justice Robert Tang Ching**       |
| **[司徒敬法官](../Page/司徒敬.md "wikilink")**    | **The Honourable Mr Justice Frank Stock**             |
| [夏正民法官](../Page/夏正民.md "wikilink")        | The Honourable Mr Justice Michael John Hartmann       |
| **[包致金法官](../Page/包致金.md "wikilink")**    | **The Honourable Mr Justice Syed Kemal Shah Bokhary** |
| **[陳兆愷法官](../Page/陳兆愷.md "wikilink")**    | **The Honourable Mr Justice Patrick Chan Siu-oi**     |
|                                           |                                                       |
|                                           |                                                       |
|                                           |                                                       |
|                                           |                                                       |
|                                           |                                                       |
|                                           |                                                       |
|                                           |                                                       |
|                                           |                                                       |

## 其他

  - [終審法院首席法官會獲編配](../Page/香港終審法院首席法官.md "wikilink")[官邸](../Page/官邸.md "wikilink")，官邸位於香港島[歌賦山道](../Page/歌賦山道.md "wikilink")18號。
  - 終審法院首席法官坐駕的專用[車牌是CJ](../Page/香港車輛登記號碼#.E9.9B.99.E5.AD.97.E6.AF.8D.md "wikilink")，是英語「Chief
    Justice」的簡寫。

## 注释

## 参考文献

## 外部連結

  - [終審法院](http://www.hkcfa.hk/)
  - [電子版香港法例](https://www.elegislation.gov.hk/)

## 參見

  - [香港高等法院](../Page/香港特別行政區高等法院.md "wikilink")
  - [香港司法機構](../Page/香港司法機構.md "wikilink")
  - [澳門終審法院](../Page/澳門終審法院.md "wikilink")

{{-}}

[港](../Category/中华人民共和国终审法院.md "wikilink")
[終](../Category/香港特别行政区法院.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")

1.

2.

3.

4.

5.

6.  《基本法》第八十二條

7.  《基本法》第八十四條

8.
9.

10. 《基本法》第一百五十八條