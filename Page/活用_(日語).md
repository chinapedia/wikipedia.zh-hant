**活用**（）指的是[日語中](../Page/日語.md "wikilink")，[用言](../Page/用言.md "wikilink")（[動詞](../Page/動詞.md "wikilink")、[形容詞](../Page/形容詞.md "wikilink")、[形容动词](../Page/形容动词.md "wikilink")）和[助動詞等的](../Page/助動詞.md "wikilink")[词形变化](../Page/词形变化.md "wikilink")（[变位](../Page/变位.md "wikilink")）。在進行變化時，這些詞語的語尾（也就是[送假名](../Page/送假名.md "wikilink")），甚至是整個詞語都會發生變化。在日语中，能夠活用的词包括了动词、形容词、[形容动词及](../Page/形容动词.md "wikilink")[助動詞](../Page/助動詞.md "wikilink")。這些詞可以接續某些詞語來表示[时态變化](../Page/时态.md "wikilink")、词类變化、[語態等](../Page/語態.md "wikilink")[文法上的功能](../Page/文法.md "wikilink")。在連接不同的[助詞時](../Page/助詞.md "wikilink")，有時也需要將詞語活用變化。几乎所有的活用都是规律的。

[AMB_Japanese_Verbs.pdf](https://zh.wikipedia.org/wiki/File:AMB_Japanese_Verbs.pdf "fig:AMB_Japanese_Verbs.pdf")

## 学校文法和教育文法的术语差异

日语传统的文法论，例如具有代表性的以[橋本進吉的桥本文法为基础的学校文法中](../Page/橋本進吉.md "wikilink")，将活用形分为**未然形**、**连用形**、**終止形**、**连体形**、**已然形**（现代语中为**假定形**）和**命令形**。

而基于学校文法，针对非日语母语者的**教育文法**中，则主要有两类，一类是按照顺序从第一变化到第二变化分别命名，另一种是按照其特点和接续形分别命名。后者常被批评的一点是容易引发歧义，例如「」究竟指的形式是否接续了「」呢，具体地，「」还是前者去掉「」后的「」，即**连用形**呢？在教科书中，往往两种含义会被混用，因而导致混乱。\[1\]

## 活用形概要

用言在活用後所產生的各種不同型態就被稱作**活用形**。基本上，每個用言都有六種不同的活用形，但是實際上六種活用形完全不同的只有古典日文中的「」（死亡）、「」（去；前往）、「」、「」四個字而已。而其他用言的活用形則多少有重複。

  - **未然形**\[2\]：主要用來接續表示否定的助動詞「」，被動、可能助動詞「‧」，使役助動詞「‧」，意志、推量助動詞「」等等。
  - **連用形**\[3\]：用來接續其他用言，以及表示過去・完了的助動詞「」等。也有不接續其他詞語，而直接拿來作為名詞的用法，例如「」（由動詞的連用形而來，作為名詞「休息」）。
  - **終止形**\[4\]：不接續其他助詞，或著接續[終助詞而置於句末的型態](../Page/終助詞.md "wikilink")。也是用言尚未活用前的基本型態。
  - **連體形**\[5\]：用來接續其他的[體言](../Page/體言.md "wikilink")。幾乎與終止形相同，除了形容動詞（[な形容词](../Page/日语语法#形容词与形容动词.md "wikilink")）。
  - **假定形**（古典日文中的**已然形**）\[6\]：接續在現代日文中表示可能‧條件、古典日文中表示原因‧理由的助詞「」。而在文言文中的假定表現要使用未然形來接續「」。
  - **命令形**\[7\]：不接續其他助詞，或接續終助詞來表示「命令」。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>辞書形<br />
<em>終止形</em></p></th>
<th><p>ます形<br />
<em>連用形</em></p></th>
<th><p>否定形<br />
<em>未然形</em></p></th>
<th><p>て形<br />
<em>"te"形</em></p></th>
<th><p>た形<br />
<em>"ta"形</em></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>～う <em>-u</em></p></td>
<td><p>～います <em>-imasu</em></p></td>
<td><p>～わない <em>-wanai</em></p></td>
<td><p>～って <em>-tte</em></p></td>
<td><p>～った <em>-tta</em></p></td>
</tr>
<tr class="even">
<td><p>～つ <em>-tsu</em></p></td>
<td><p>～ちます <em>-chimasu</em></p></td>
<td><p>～たない <em>-tanai</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>～る <em>-ru</em></p></td>
<td><p>～ります <em>-rimasu</em></p></td>
<td><p>～らない <em>-ranai</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>～く <em>-ku</em></p></td>
<td><p>～きます <em>-kimasu</em></p></td>
<td><p>～かない <em>-kanai</em></p></td>
<td><p>～いて <em>-ite</em></p></td>
<td><p>～いた <em>-ita</em></p></td>
</tr>
<tr class="odd">
<td><p>～ぐ <em>-gu</em></p></td>
<td><p>～ぎます <em>-gimasu</em></p></td>
<td><p>～がない <em>-ganai</em></p></td>
<td><p>～いで <em>-ide</em></p></td>
<td><p>～いだ <em>-ida</em></p></td>
</tr>
<tr class="even">
<td><p>～ぶ <em>-bu</em></p></td>
<td><p>～びます <em>-bimasu</em></p></td>
<td><p>～ばない <em>-banai</em></p></td>
<td><p>～んで <em>-nde</em></p></td>
<td><p>～んだ <em>-nda</em></p></td>
</tr>
<tr class="odd">
<td><p>～む <em>-mu</em></p></td>
<td><p>～みます <em>-mimasu</em></p></td>
<td><p>～まない <em>-manai</em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>～す <em>-su</em></p></td>
<td><p>～します <em>-shimasu</em></p></td>
<td><p>～さない <em>-sanai</em></p></td>
<td><p>～して <em>-shite</em></p></td>
<td><p>～した <em>-shita</em></p></td>
</tr>
<tr class="odd">
<td><p>(～い)る <em>-iru</em></p></td>
<td><p>～ます <em>-masu</em></p></td>
<td><p>～ない <em>-nai</em></p></td>
<td><p>～て <em>-te</em></p></td>
<td><p>～た <em>-ta</em></p></td>
</tr>
<tr class="even">
<td><p>(～え)る <em>-eru</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>～する <em>-suru</em></p></td>
<td><p>～します <em>-shimasu</em></p></td>
<td><p>～しない <em>-shinai</em></p></td>
<td><p>～して <em>-shite</em></p></td>
<td><p>～した <em>-shita</em></p></td>
</tr>
<tr class="even">
<td><p>～くる <em>-kuru</em></p></td>
<td><p>～きます <em>-kimasu</em></p></td>
<td><p>～こない <em>-konai</em></p></td>
<td><p>～きて <em>-kite</em></p></td>
<td><p>～きた <em>-kita</em></p></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>辞書形<br />
<em>終止形</em></p></th>
<th><p>可能形<br />
<em>已然形</em></p></th>
<th><p>条件形<br />
<em>假定形</em></p></th>
<th><p>意向形<br />
<em>推量形</em></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>～う <em>-u</em></p></td>
<td><p>～える <em>-eru</em></p></td>
<td><p>～えば <em>-eba</em></p></td>
<td><p>～おう <em>-ō</em></p></td>
</tr>
<tr class="even">
<td><p>～つ <em>-tsu</em></p></td>
<td><p>～てる <em>-teru</em></p></td>
<td><p>～てば <em>-teba</em></p></td>
<td><p>～とう <em>-tō</em></p></td>
</tr>
<tr class="odd">
<td><p>～る <em>-ru</em></p></td>
<td><p>～れる <em>-reru</em></p></td>
<td><p>～れば <em>-reba</em></p></td>
<td><p>～ろう <em>-rō</em></p></td>
</tr>
<tr class="even">
<td><p>～く <em>-ku</em></p></td>
<td><p>～ける <em>-keru</em></p></td>
<td><p>～けば <em>-keba</em></p></td>
<td><p>～こう <em>-kō</em></p></td>
</tr>
<tr class="odd">
<td><p>～ぐ <em>-gu</em></p></td>
<td><p>～げる <em>-geru</em></p></td>
<td><p>～げば <em>-geba</em></p></td>
<td><p>～ごう <em>-gō</em></p></td>
</tr>
<tr class="even">
<td><p>～ぶ <em>-bu</em></p></td>
<td><p>～べる <em>-beru</em></p></td>
<td><p>～べば <em>-beba</em></p></td>
<td><p>～ぼう <em>-bō</em></p></td>
</tr>
<tr class="odd">
<td><p>～む <em>-mu</em></p></td>
<td><p>～める <em>-meru</em></p></td>
<td><p>～めば <em>-meba</em></p></td>
<td><p>～もう <em>-mō</em></p></td>
</tr>
<tr class="even">
<td><p>～す <em>-su</em></p></td>
<td><p>～せる <em>-seru</em></p></td>
<td><p>～せば <em>-seba</em></p></td>
<td><p>～そう <em>-sō</em></p></td>
</tr>
<tr class="odd">
<td><p>(～い)る <em>-iru</em></p></td>
<td><p>～られる <em>-rareru</em></p></td>
<td><p>～れば <em>-reba</em></p></td>
<td><p>～よう <em>-yō</em></p></td>
</tr>
<tr class="even">
<td><p>(～え)る <em>-eru</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>する <em>suru</em></p></td>
<td><p>できる <em>dekiru</em></p></td>
<td><p>すれば <em>sureba</em></p></td>
<td><p>しよう <em>shiyō</em></p></td>
</tr>
<tr class="even">
<td><p>くる <em>kuru</em></p></td>
<td><p>こられる <em>korareru</em></p></td>
<td><p>くれば <em>kureba</em></p></td>
<td><p>こよう <em>koyō</em></p></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td><table>
<thead>
<tr class="header">
<th><p>辭典字例子</p></th>
<th><p>連用形例子</p></th>
<th><p>未然形例子</p></th>
<th><p>分詞形例子</p></th>
<th><p>完成形例子</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>あら<strong>う</strong> <em>ara<strong>u</strong></em> <small>"洗"</small></p></td>
<td><p>あら<strong>います</strong> <em>ara<strong>imasu</strong></em></p></td>
<td><p>あら<strong>わない</strong> <em>ara<strong>wanai</strong></em></p></td>
<td><p>あら<strong>って</strong> <em>ara<strong>tte</strong></em></p></td>
<td><p>あら<strong>った</strong> <em>ara<strong>tta</strong></em></p></td>
</tr>
<tr class="even">
<td><p>ま<strong>つ</strong> <em>ma<strong>tsu</strong></em> <small>"等待"</small></p></td>
<td><p>ま<strong>ちます</strong> <em>ma<strong>chimasu</strong></em></p></td>
<td><p>ま<strong>たない</strong> <em>ma<strong>tanai</strong></em></p></td>
<td><p>ま<strong>って</strong> <em>ma<strong>tte</strong></em></p></td>
<td><p>ま<strong>った</strong> <em>ma<strong>tta</strong></em></p></td>
</tr>
<tr class="odd">
<td><p>と<strong>る</strong> <em>to<strong>ru</strong></em> <small>"採取"</small></p></td>
<td><p>と<strong>ります</strong> <em>to<strong>rimasu</strong></em></p></td>
<td><p>と<strong>らない</strong> <em>to<strong>ranai</strong></em></p></td>
<td><p>と<strong>って</strong> <em>to<strong>tte</strong></em></p></td>
<td><p>と<strong>った</strong> <em>to<strong>tta</strong></em></p></td>
</tr>
<tr class="even">
<td><p>か<strong>く</strong> <em>ka<strong>ku</strong></em> <small>"寫"</small></p></td>
<td><p>か<strong>きます</strong> <em>ka<strong>kimasu</strong></em></p></td>
<td><p>か<strong>かない</strong> <em>ka<strong>kanai</strong></em></p></td>
<td><p>か<strong>いて</strong> <em>ka<strong>ite</strong></em></p></td>
<td><p>か<strong>いた</strong> <em>ka<strong>ita</strong></em></p></td>
</tr>
<tr class="odd">
<td><p>いそ<strong>ぐ</strong> <em>iso<strong>gu</strong></em> <small>"匆忙"</small></p></td>
<td><p>いそ<strong>ぎます</strong> <em>iso<strong>gimasu</strong></em></p></td>
<td><p>いそ<strong>がない</strong> <em>iso<strong>ganai</strong></em></p></td>
<td><p>いそ<strong>いで</strong> <em>iso<strong>ide</strong></em></p></td>
<td><p>いそ<strong>いだ</strong> <em>iso<strong>ida</strong></em></p></td>
</tr>
<tr class="even">
<td><p>し<strong>ぬ</strong> <em>shi<strong>nu</strong></em> <small>"死"</small></p></td>
<td><p>し<strong>にます</strong> <em>shi<strong>nimasu</strong></em></p></td>
<td><p>し<strong>なない</strong> <em>shi<strong>nanai</strong></em></p></td>
<td><p>し<strong>んで</strong> <em>shi<strong>nde</strong></em></p></td>
<td><p>し<strong>んだ</strong> <em>shi<strong>nda</strong></em></p></td>
</tr>
<tr class="odd">
<td><p>よ<strong>ぶ</strong> <em>yo<strong>bu</strong></em> <small>"大喊"</small></p></td>
<td><p>よ<strong>びます</strong> <em>yo<strong>bimasu</strong></em></p></td>
<td><p>よ<strong>ばない</strong> <em>yo<strong>banai</strong></em></p></td>
<td><p>よ<strong>んで</strong> <em>yo<strong>nde</strong></em></p></td>
<td><p>よ<strong>んだ</strong> <em>yo<strong>nda</strong></em></p></td>
</tr>
<tr class="even">
<td><p>の<strong>む</strong> <em>no<strong>mu</strong></em> <small>"喝"</small></p></td>
<td><p>の<strong>みます</strong> <em>no<strong>mimasu</strong></em></p></td>
<td><p>の<strong>まない</strong> <em>no<strong>manai</strong></em></p></td>
<td><p>の<strong>んで</strong> <em>no<strong>nde</strong></em></p></td>
<td><p>の<strong>んだ</strong> <em>no<strong>nda</strong></em></p></td>
</tr>
<tr class="odd">
<td><p>はな<strong>す</strong> <em>hana<strong>su</strong></em> <small>"說話"</small></p></td>
<td><p>はな<strong>します</strong> <em>hana<strong>shimasu</strong></em></p></td>
<td><p>はな<strong>さない</strong> <em>hana<strong>sanai</strong></em></p></td>
<td><p>はな<strong>して</strong> <em>hana<strong>shite</strong></em></p></td>
<td><p>はな<strong>した</strong> <em>hana<strong>shita</strong></em></p></td>
</tr>
<tr class="even">
<td><p>み<strong>る</strong> <em>mi<strong>ru</strong></em> <small>"看到"</small></p></td>
<td><p>み<strong>ます</strong> <em>mi<strong>masu</strong></em></p></td>
<td><p>み<strong>ない</strong> <em>mi<strong>nai</strong></em></p></td>
<td><p>み<strong>て</strong> <em>mi<strong>te</strong></em></p></td>
<td><p>み<strong>た</strong> <em>mi<strong>ta</strong></em></p></td>
</tr>
<tr class="odd">
<td><p>たべ<strong>る</strong> <em>tabe<strong>ru</strong></em> <small>"吃"</small></p></td>
<td><p>たべ<strong>ます</strong> <em>tabe<strong>masu</strong></em></p></td>
<td><p>たべ<strong>ない</strong> <em>tabe<strong>nai</strong></em></p></td>
<td><p>たべ<strong>て</strong> <em>tabe<strong>te</strong></em></p></td>
<td><p>たべ<strong>た</strong> <em>tabe<strong>ta</strong></em></p></td>
</tr>
<tr class="even">
<td><p>する <em>suru</em> <small>"做"</small></p></td>
<td><p>します <em>shimasu</em></p></td>
<td><p>しない <em>shinai</em></p></td>
<td><p>して <em>shite</em></p></td>
<td><p>した <em>shita</em></p></td>
</tr>
<tr class="odd">
<td><p>勉強 <em>benkyou</em> <small>"研究"</small></p></td>
<td><p>勉強<strong>します</strong> <em>benkyou<strong>shimasu</strong></em></p></td>
<td><p>勉強<strong>しない</strong> <em>benkyou<strong>shinai</strong></em></p></td>
<td><p>勉強<strong>して</strong> <em>benkyou<strong>shite</strong></em></p></td>
<td><p>勉強<strong>した</strong> <em>benkyou<strong>shita</strong></em></p></td>
</tr>
<tr class="even">
<td><p>くる <em>kuru</em> <small>"來"</small></p></td>
<td><p>きます <em>kimasu</em></p></td>
<td><p>こない <em>konai</em></p></td>
<td><p>きて <em>kite</em></p></td>
<td><p>きた <em>kita</em></p></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>辞書形<br />
<em>終止形</em></p></th>
<th><p>受身･尊敬<br />
<em>passive verb</em></p></th>
<th><p>使役<br />
<em>causative verb</em></p></th>
<th><p>禁止形<br />
<em>prohibitive form</em></p></th>
<th><p>命令形<br />
<em>imperative form</em></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>～う <em>-u</em></p></td>
<td><p>～われる <em>-wareru</em></p></td>
<td><p>～わせる <em>-waseru</em></p></td>
<td><p>～うな <em>-u na</em></p></td>
<td><p>～え <em>-e</em></p></td>
</tr>
<tr class="even">
<td><p>～つ <em>-tsu</em></p></td>
<td><p>～たれる <em>-tareru</em></p></td>
<td><p>～たせる <em>-taseru</em></p></td>
<td><p>～つな <em>-tsu na</em></p></td>
<td><p>～て <em>-te</em></p></td>
</tr>
<tr class="odd">
<td><p>～る <em>-ru</em></p></td>
<td><p>～られる <em>-rareru</em></p></td>
<td><p>～らせる <em>-raseru</em></p></td>
<td><p>～るな <em>-ru na</em></p></td>
<td><p>～れ <em>-re</em></p></td>
</tr>
<tr class="even">
<td><p>～く <em>-ku</em></p></td>
<td><p>～かれる <em>-kareru</em></p></td>
<td><p>～かせる <em>-kaseru</em></p></td>
<td><p>～くな <em>-ku na</em></p></td>
<td><p>～け <em>-ke</em></p></td>
</tr>
<tr class="odd">
<td><p>～ぐ <em>-gu</em></p></td>
<td><p>～がれる <em>-gareru</em></p></td>
<td><p>～がせる <em>-gaseru</em></p></td>
<td><p>～ぐな <em>-gu na</em></p></td>
<td><p>～げ <em>-ge</em></p></td>
</tr>
<tr class="even">
<td><p>～ぶ <em>-bu</em></p></td>
<td><p>～ばれる <em>-bareru</em></p></td>
<td><p>～ばせる <em>-baseru</em></p></td>
<td><p>～ぶな <em>-bu na</em></p></td>
<td><p>～べ <em>-be</em></p></td>
</tr>
<tr class="odd">
<td><p>～む <em>-mu</em></p></td>
<td><p>～まれる <em>-mareru</em></p></td>
<td><p>～ませる <em>-maseru</em></p></td>
<td><p>～むな <em>-mu na</em></p></td>
<td><p>～め <em>-me</em></p></td>
</tr>
<tr class="even">
<td><p>～す <em>-su</em></p></td>
<td><p>～される <em>-sareru</em></p></td>
<td><p>～させる <em>-saseru</em></p></td>
<td><p>～すな <em>-su na</em></p></td>
<td><p>～せ <em>-se</em></p></td>
</tr>
<tr class="odd">
<td><p>(～い)る <em>-iru</em></p></td>
<td><p>～られる <em>-rareru</em></p></td>
<td><p>(～い)るな <em>-iru na</em></p></td>
<td><p>～ろ <em>-ro</em></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>(～え)る <em>-eru</em></p></td>
<td><p>(～え)るな <em>-eru na</em></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>する <em>suru</em></p></td>
<td><p>される <em>sareru</em></p></td>
<td><p>させる <em>saseru</em></p></td>
<td><p>するな <em>suru na</em></p></td>
<td><p>しろ <em>shiro</em></p></td>
</tr>
<tr class="even">
<td><p>くる <em>kuru</em></p></td>
<td><p>こられる <em>korareru</em></p></td>
<td><p>こさせる <em>kosaseru</em></p></td>
<td><p>くるな <em>kuru na</em></p></td>
<td><p>こい <em>koi</em></p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

### 例子

以動詞「」（，跑）的六種活用形造句：

<table>
<tbody>
<tr class="odd">
<td><p>活用形</p></td>
<td><p>句子</p></td>
<td><p>中譯</p></td>
</tr>
<tr class="even">
<td><p>未然形</p></td>
<td><p><br />
mō <U>Hashira</U>naide.</p></td>
<td><p>別再<U>跑</U>了。<br />
（接表否定的）</p></td>
</tr>
<tr class="odd">
<td><p>連用形</p></td>
<td><p><br />
Gōru ni tsuku made ni, <U>Hashitte</U> iku.</p></td>
<td><p>在到達終點之前，要繼續<U>跑</U>下去。又譯：要一直/繼續跑到終點。<br />
（接補助動詞，表繼續）</p></td>
</tr>
<tr class="even">
<td><p>終止形</p></td>
<td><p><br />
Inu ga <U>hashiru</U>.</p></td>
<td><p>狗（在）<U>跑</U>。</p></td>
</tr>
<tr class="odd">
<td><p>連體形</p></td>
<td><p><br />
<U>Hashiru</U> koto ga dekiru.</p></td>
<td><p>直譯：「<U>跑</U>」這件事，做得到。意譯：能（夠）跑。<br />
（接表示名詞化的）</p></td>
</tr>
<tr class="even">
<td><p>假定形</p></td>
<td><p><br />
Mainichi <U>hashire</U>ba, karada ga yoku naru.</p></td>
<td><p>（如果）每天<U>跑步</U>，（身體）就能健康。<br />
（接表假設的）</p></td>
</tr>
<tr class="odd">
<td><p>命令形</p></td>
<td><p><br />
Hayaku <U>hashire</U>.</p></td>
<td><p>快（點）<U>跑</U>！<br />
（帶命令語氣）</p></td>
</tr>
</tbody>
</table>

由例句可知，即使中文翻譯同樣都是「跑」，但在日文中要接續不同的詞時，就必須要變成不同的活用形。

## 活用類型

日語**國文法**中，將古典日語（）和現代日語（）的活用，根據規則的差異分別分爲不同的活用類型，具體的活用規則見下文。下表是古典日語和現代日語動詞活用的對照。

<table>
<thead>
<tr class="header">
<th><p><strong>日本語動詞活用類型</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>古典日語</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/四段活用.md" title="wikilink">四段活用</a><br />
<a href="../Page/ナ行變格活用.md" title="wikilink">ナ行變格活用</a><br />
<a href="../Page/ラ行變格活用.md" title="wikilink">ラ行變格活用</a><br />
<a href="../Page/下一段活用.md" title="wikilink">下一段活用</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/下二段活用.md" title="wikilink">下二段活用</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/上一段活用.md" title="wikilink">上一段活用</a><br />
<a href="../Page/上二段活用.md" title="wikilink">上二段活用</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/カ行變格活用.md" title="wikilink">カ行變格活用</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/サ行變格活用.md" title="wikilink">サ行變格活用</a></p></td>
</tr>
</tbody>
</table>

## 現代日语的活用

以下列出各種用言在現代日文中活用的基本規則。

### 動詞

動詞的活用是較為複雜的。依照活用規則的不同，可以將動詞分為五個種類：五段動詞、上一段動詞、下一段動詞、行变格活用動詞和行变格活用動詞。其中，「」的意思是「不规则」，而其余则为「」即「规则」\[8\]\[9\]。而各種動詞活用方式都具備有完整的六個活用形，但是活用的規則不同。

#### 五段活用

發生五段活用的動詞稱為[五段動詞](../Page/五段動詞.md "wikilink")（或I類動詞）。這一類動詞的語尾在活用時會在五個段上變化，因而得名\[10\]。其基本規則如下：

有几点需要注意的地方：

  - 五段活用动词的连用形有两种形式，一种就是上表中提到的形式（教育文法中称为「形」）；另一种形式用于接续部分助词和助动词，如等（教育文法中称为「形」或「形」），这种形式即音便形，标准语中（包括东日本方言）主要有促音便、音便、ウ音便和撥音便等，參見下表：

<!-- end list -->

  -
    {| class="wikitable"

`! 音便类型 || 發生於 || 动词（终止形） || 连用形接续「て」 || 音便结果 `
`|-`
`| rowspan="4" | 促音便 `
`| タ行 || `` || `` || `
`|-`
`| ラ行 || `` || `` || `
`|-`
`| ワア行`\[11\]` || `` || `` || `
`|-`
`| カ行個別 || `` || `` || `
`|-`
`| rowspan="2" | ``音便`
`| カ行`\[12\]` || `` || `` || `
`|-`
`| ガ行 || `` || `` || `
`|-`
`| rowspan="3" | 拨音便`
`| ナ行 || `` || `` || `
`|-`
`| バ行 || `` || `` || `
`|-`
`| マ行 || `` || `` || `
`|-`
`| ウ音便 || ワア行個別 || `` || `` || `\[13\]
`|-`
`| 不發生 || サ行 || `` || 探`**`し`**`て || 探して`
`|}`

  - （irassharu，來的[敬語](../Page/日語敬語.md "wikilink")）、（kudasaru，給的敬語）、（nasaru，做的敬語）、（Ossharu，說的敬語），這四個[敬語動詞的](../Page/日語敬語.md "wikilink")形（连用形）為，及命令形為，但形不变\[14\]。

  - 西日本方言中，ワア段有ウ音便的，此時「ワローテ（笑うて/わろうて）」「ユーテ（言うて/いうて）」「ソーテ（沿うて/そうて）」這樣發音。

<!-- end list -->

  - 教育文法中，将動詞的否定形直接视为表示不存在的形容词。

#### 上一段、下一段活用

發生上一段活用的動詞被分類為[上一段動詞](../Page/上一段動詞.md "wikilink")（或Ⅱ類動詞）。這一類動詞的語尾都是，且前一個字都在[五十音表的](../Page/五十音.md "wikilink")段上，因而得名（段是動詞標準語尾段音的上一段）。其規則如下：

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p>未然形</p></td>
<td><p>連用形</p></td>
<td><p>終止形</p></td>
<td><p>連體形</p></td>
<td><p>假定形</p></td>
<td><p>命令形</p></td>
</tr>
<tr class="even">
<td><p>語尾改變</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>例</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
</p></td>
</tr>
</tbody>
</table>

而發生下一段活用的動詞被分類為[下一段動詞](../Page/下一段動詞.md "wikilink")（也被歸入Ⅱ類動詞）。這一類動詞的語尾都是，且前一個字位於五十音表的段上，因而得名（段是動詞標準語尾段音的下一段）。其規則與上一段動詞相同。

但是這個規則有一個例外：

  - （，kureru，給）的命令形是。

#### 变格活用

行变格活用動詞只有一字，（，kuru，來）。這個動詞在变成各種活用形時會在整個上作变化，且為整個動詞發生改变，因而得名。其規則如下：

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p>未然形</p></td>
<td><p>連用形</p></td>
<td><p>終止形</p></td>
<td><p>連體形</p></td>
<td><p>假定形</p></td>
<td><p>命令形</p></td>
</tr>
<tr class="even">
<td><p>整個動詞改變</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>發音</p></td>
<td><p><br />
ko</p></td>
<td><p><br />
ki</p></td>
<td><p><br />
kuru</p></td>
<td><p><br />
kuru</p></td>
<td><p><br />
kure</p></td>
<td><p><br />
koi</p></td>
</tr>
</tbody>
</table>

#### 变格活用

行变格活用動詞包括了（suru，漢字寫成「」，有「作...」的意思）一字，以及與此字結合而成的複合動詞（例如：等等）。行变格活用動詞在變成不同的活用形時也是整個（或是該動詞的兩字部分）在行上進行变化。其活用規則如下：
{| border="1" style="background: white; border: 1px solid \#88a;
border-collapse: collapse; margin: auto;" cellspacing="1"
cellpadding="2" |- style="background: \#ccf; text-align: center;" | \!
未然形 \! 連用形 \! 終止形 \! 連體形 \! 假定形 \! 命令形 |- \! 整個動詞改變 |  |  |  |  |  |
／ |- \! 舉例 |
benkyōshi.se.sa |
benkyōshi |
benkyōsuru |
benkyōsuru |
benkyōsure |
benkyōshiro.seyo |}
\* 三種未然形分別用於接續不同的助動詞。例如接續文言否定助動詞時要用，而接續一般否定助動詞時用。而接續使役助動詞時則用。

### 形容詞

現代日文中形容詞的活用形只有五種，欠缺命令形。其活用規則如下。如欲使用命令形，可以使用「連用形-(的命令形)」。

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p>未然形</p></td>
<td><p>連用形</p></td>
<td><p>終止形</p></td>
<td><p>連體形</p></td>
<td><p>假定形</p></td>
</tr>
<tr class="even">
<td><p>語尾改變</p></td>
<td><p>-</p></td>
<td><p>-、-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>舉例</p></td>
<td><p><br />
samukaro</p></td>
<td><p><br />
samuku.samuka</p></td>
<td><p><br />
samui</p></td>
<td><p><br />
samui</p></td>
<td><p><br />
samukere</p></td>
</tr>
</tbody>
</table>


\*兩種連用形分別用於連接不同助詞時。「-」主要用來連接形成[中止形](../Page/中止形.md "wikilink")，以及作為副詞修飾動詞；其他助詞、助動詞則用-連接。

### 形容動詞

現代形容動詞的活用與形容詞相同，沒有命令形。而現代日語中形容動詞的活用分為兩種，都是從古典日文用法變化而成的\[15\]，分別介紹如下。
如欲使用命令形，可以使用「連用形-(的命令形)」。

####

形容動詞的是由文語中的演變而來，且揉合了斷定助動詞的用法。其規則如下：

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p>未然形</p></td>
<td><p>連用形</p></td>
<td><p>終止形</p></td>
<td><p>連體形</p></td>
<td><p>假定形</p></td>
</tr>
<tr class="even">
<td><p>語尾改變</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>舉例</p></td>
<td><p><br />
kireidaro</p></td>
<td><p><br />
kireide.da.ni</p></td>
<td><p><br />
kireida</p></td>
<td><p><br />
kireina</p></td>
<td><p><br />
kireinara</p></td>
</tr>
</tbody>
</table>


\*三種連用形也分別用於連接不同助詞時。「-」主要用來連接，或是作為中止形；其他助詞、助動詞則用-連接。而「-」作為副詞形來修飾動詞。

  - 連體形特殊的形容動詞 -
    「（onajida，相同的）」有兩種連體形。第一個主要是在連接格助詞「」、接續助詞「」、「」的時候使用「」。第二是在連接體言時使用「」。也有人將後著分類於[連體詞](../Page/連體詞.md "wikilink")。除了「」之外，「（kon'nada，這樣的）」「（son'nada，那樣的）」「（an'nada，那樣的）」「（don'nada，哪樣的）」之類的，也都一樣。不過，「」的意思像「(一樣是～的話)」和「同樣、反正」的時候，「同じ」屬於[副詞](../Page/副詞.md "wikilink")。
  - 只有連體形的形容動詞 -
    「（ōkina，大的）」也有人分類於[連體詞](../Page/連體詞.md "wikilink")。但是，它和[連體詞的不同](../Page/連體詞.md "wikilink")，可能成為被修飾語（例：「」）。再者、從原本屬於「」形容動詞的角度來看，本來「」是形容動詞的說法，不過其[活用形幾乎都消失了](../Page/活用.md "wikilink")，只剩下連體形「」存在於現代日語。這個說法也沒有什麼矛盾。除了「」，還有「（chiisana，小的）」「（okashina，可笑的、奇怪的）」之類的，也都一樣。

#### 型活用

形容動詞的型活用是文語中的活用殘留下來的一部分\[16\]。常常用在標題、標語等需要表達強烈感情之處或是公文等正式嚴肅之處。型活用的詞往往是漢字疊字詞或是以「」結尾的詞，例如「」「」「」「」等等，其規則如下：

<table>
<thead>
<tr class="header">
<th><p>活用形</p></th>
<th><p>未然形</p></th>
<th><p>連用形</p></th>
<th><p>終止形</p></th>
<th><p>連體形</p></th>
<th><p>假定形</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>詞尾</p></td>
<td><p>○</p></td>
<td><p>-</p></td>
<td><p>○</p></td>
<td><p>-</p></td>
<td><p>○</p></td>
</tr>
<tr class="even">
<td><p>例詞</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

學校文法中，將「」「」等視爲副詞，「」「」等視爲連體詞。

### 助動詞

  - 五段型：[たがる](../Page/wikt:たがる.md "wikilink")、[やがる](../Page/wikt:やがる.md "wikilink")
  - 下一段型：（[ら](../Page/wikt:られる.md "wikilink")）[れる](../Page/wikt:れる.md "wikilink")、（[さ](../Page/wikt:させる.md "wikilink")）[せる](../Page/wikt:せる.md "wikilink")
  - 形容詞型：[ない](../Page/wikt:ない.md "wikilink")、[たい](../Page/wikt:たい.md "wikilink")、[らしい](../Page/wikt:たい.md "wikilink")
  - 形容動詞型：[だ](../Page/wikt:だ.md "wikilink")、[ようだ](../Page/wikt:ようだ.md "wikilink")、[です](../Page/wikt:です.md "wikilink")
  - 不変化型：（[よ](../Page/wikt:よう.md "wikilink")）[う](../Page/wikt:う.md "wikilink")、[まい](../Page/wikt:ない.md "wikilink")
  - 特殊型：[ぬ](../Page/wikt:ぬ.md "wikilink")（[ん](../Page/wikt:ん.md "wikilink")）、[た](../Page/wikt:た.md "wikilink")、[ます](../Page/wikt:ます.md "wikilink")

## 古典日语的活用

### 四段活用

由于活用语尾在四段中发生活用，因此名为**四段活用**。相当于现代日语（現代假名遣\[17\]）中的**[五段活用](../Page/活用_\(日語\)#五段活用.md "wikilink")**。

### ラ行變格活用

由于活用语尾在ラ行，并且属于變格活用，因此名为**ラ行變格活用**。主要有「あり」「をり」「はべり」「いまそかり（いますがり）」及由他们构成的复合动词等。现代日语中同化合并入**[五段活用](../Page/活用_\(日語\)#五段活用.md "wikilink")**。

### ナ行變格活用 

由于活用语尾在ナ行，并且属于變格活用，因此名为**ナ行變格活用**。只有「死（し）ぬ」「往（い）ぬ」。现代日语中「死（し）ぬ」同化合并入**[五段活用](../Page/活用_\(日語\)#五段活用.md "wikilink")**，「往（い）ぬ」不再被使用。

### 下一段活用

由于活用语尾在中发生活用，因此名为**下一段活用**\[18\]。只有“蹴（け）る”一個詞。现代日语中「蹴（け）る」同化合并入**[五段活用](../Page/活用_\(日語\)#五段活用.md "wikilink")**。

| 行  | 基本形   | [活用形](../Page/活用_\(日語\)#活用形概要.md "wikilink") |
| -- | ----- | -------------------------------------------- |
| 詞幹 | 未然形   | 連用形                                          |
| カ行 | 蹴（け）る | （蹴）                                          |

### 下二段活用 

由于活用语尾在两段中发生活用，因此名为**下二段活用**\[19\]。现代日语中同化合并入**[下一段活用](../Page/活用_\(日語\)#上一段、下一段活用.md "wikilink")**。

### 上一段活用

由于活用语尾在中发生活用，因此名为**上一段活用**\[20\]。

| 行                              | 基本形    | [活用形](../Page/活用_\(日語\)#活用形概要.md "wikilink") |
| ------------------------------ | ------ | -------------------------------------------- |
| 詞幹                             | 未然形    | 連用形                                          |
| [カ行](../Page/カ行.md "wikilink") | 着（き）る  | （着）                                          |
| [ナ行](../Page/ナ行.md "wikilink") | 似（に）る  | （似）                                          |
| [ハ行](../Page/ハ行.md "wikilink") | 干（ひ）る  | （干）                                          |
| [マ行](../Page/マ行.md "wikilink") | 見（み）る  | （見）                                          |
| [ヤ行](../Page/ヤ行.md "wikilink") | |射（い）る | （射）                                          |
| [ワ行](../Page/ワ行.md "wikilink") | 居（ゐ）る  | （居）                                          |

### 上二段活用

由于活用语尾在两段中发生活用，因此名为**上二段活用**\[21\]。

### カ行變格活用 

由于活用语尾在カ行，并且属于變格活用，因此名为**カ行變格活用**。

### サ行變格活用 

由于活用语尾在サ行，并且属于變格活用，因此名为**サ行變格活用**。

## 參見

  - [日語語法](../Page/日語語法.md "wikilink")

## 注釋

<div class="references-2column">

<references/>

</div>

## 參考書籍

  -   -
      -
      -
  -
  -
  -
  -
[ja:活用\#日本語の活用](../Page/ja:活用#日本語の活用.md "wikilink")

[活用\*](../Category/日語語法.md "wikilink")
[Category:構詞學](../Category/構詞學.md "wikilink")
[Category:各語言動詞](../Category/各語言動詞.md "wikilink")

1.  市面上中文的日语教材，主要使用后一种形式，例如《新中日交流标准日本语》和《大家的日本语》 ，另可參見《大家的日本語初級I‧II
    文法解說》，大新書局；《新日本語の基礎II 文法解說》，大新書局；《日語檢定4級文法一次過關》，張文朝，旺文出版社等

2.  在部分書籍中，未然形被稱作**第一變化**、****或**否定形**。另外，用语接续意志、推量助动词「」的活用语尾变化为「オ段」的形式，则与「」一同称为**意志形**，例如「書く」的意志形为「書こう」。

3.  在部分書籍中，連用形被稱作**第二變化**或****

4.  在部分書籍中，終止形被稱作**第三變化**，也被稱為**辭書形**、**基本形**或**原形**，這是由於所有日文字典中都是以此形作為用言的詞條。參考書籍同上。

5.  在部分書籍中，也被稱作**第四變化**。

6.  在部分書籍中，也被稱作**第五變化**。

7.  在部分書籍中，也被稱作**第六變化**。

8.

9.  教育文法中將之分為三個種類，五段活用動詞被分為I類動詞，上一段及下一段活用併為Ⅱ類動詞，而行变格及行变格活用動詞被併入Ⅲ類動詞。可以參見《大家的日本語初級I‧II
    文法解說》，大新書局；《用五十音學文法》，曾元宏，我識出版社

10. 事实上，五段活用与四段活用的差异并不在语法上，而是在[假名遣上](../Page/假名遣.md "wikilink")。具体来说，未然形的ア段活用词尾遇到「う」后，发音演变为「オー」（双元音变为长元音），又因为[现代假名遣的一定表音主义成分](../Page/现代假名遣.md "wikilink")，最终在书写上将「ア段+う」变成「オ段+う」，例如「書かう」变成「書こう」，最终从アイウエ四个段变成アイウエオ五个段。

11. `「``う」「``う」「``う」等除外`

12. `除「行く」外`

13. `不算詞尾前一音的輔音的話，讀若「オー」，而其終止形讀若「オウ」`

14. 不過「いらっしゃって」也可以說成「いらして」。

15. 蔡佩青，《あはん 日本語完全攻略Ⅲ》，上澤社 ISBN 978-957-28912-9-2

16. 蔡佩青，《あはん 日本語完全攻略Ⅲ》，上澤社 ISBN 978-957-28912-9-2

17.
18. 以五十音圖的「ウ段」爲中心，最末音或「る」「れ」「よ」「ろ」前的音是，「イ段」的爲上一段，「イ段・ウ段」的爲上二段、「エ段」的爲下一段、「ウ段・エ段」的爲下二段。

19.
20.
21.