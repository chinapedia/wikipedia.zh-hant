**亚伦·孟肯**（，），出生于[美国](../Page/美国.md "wikilink")[纽约州](../Page/纽约州.md "wikilink")[新罗谢尔市](../Page/新羅謝爾_\(紐約州\).md "wikilink")，著名的[钢琴家](../Page/钢琴家.md "wikilink")、[百老汇及](../Page/百老汇.md "wikilink")[电影配乐](../Page/电影配乐.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")。他至今已獲8項[奥斯卡獎](../Page/奥斯卡金像奖.md "wikilink")，是歷史上獲得最多奧斯卡獎的在世人物。

## 生平

亚伦·孟肯很小的时候就表现出[音乐天赋](../Page/音乐.md "wikilink")。他在中学时期开始学习[钢琴和](../Page/钢琴.md "wikilink")[小提琴](../Page/小提琴.md "wikilink")。成年后，孟肯进入[纽约大学的医学院预科班学习](../Page/纽约大学.md "wikilink")[医学](../Page/医学.md "wikilink")，但是他的兴趣一直集中于音乐上。大学毕业后，他加入了，他在那遇见了[剧作家兼](../Page/剧作家.md "wikilink")[作词家](../Page/作词家.md "wikilink")。这段经历奠定了他未来舞台乐创作的职业生涯的基础。

当时孟肯的工作以作曲为主。他频繁地在当地的俱乐部演出，同时也积极地投身于舞台乐创作。其间孟肯创作了许多成功的作品，但一直得不到推出。1979年，他与霍华德·爱许曼德首次合作的由WPA剧场出品的《》在[外百老汇首场演出后获得了好评](../Page/外百老汇音乐剧.md "wikilink")。1982年，孟肯与爱许曼的另一部更为成功的外百老汇[音乐剧作品](../Page/音乐剧.md "wikilink")《》使孟肯获得了的提名，1986年更搬上大螢幕拍攝電影版本《[異形奇花](../Page/異形奇花.md "wikilink")》，成為八十年代歌舞類型文藝復興的探路先鋒。八十年代孟肯的其他几部备受青睐的音乐剧也为他带来了BMI职业成就奖等殊荣。

不过，真正使亚伦·孟肯闻名于世的，是他为[华特·迪士尼公司一系列](../Page/华特·迪士尼公司.md "wikilink")[经典动画长片的配乐](../Page/经典动画长片.md "wikilink")，包括与爱许曼合作的《[小美人鱼](../Page/小美人鱼.md "wikilink")》（1989）、《[美女与野兽](../Page/美女与野兽.md "wikilink")》（1991）和《[阿拉丁](../Page/阿拉丁_\(电影\).md "wikilink")》\[1\]（1992），与合作的《[风中奇缘](../Page/风中奇缘.md "wikilink")》（1995）和《[钟楼怪人](../Page/钟楼怪人_\(1996年电影\).md "wikilink")》（1996），以及《[大力士](../Page/大力士.md "wikilink")》（1997）、《[放牛吃草](../Page/放牛吃草.md "wikilink")》（2004）、《[曼哈頓奇緣](../Page/曼哈頓奇緣.md "wikilink")》（2007）、《[魔髮奇緣](../Page/魔髮奇緣.md "wikilink")》（2010）等。这些作品为孟肯带来了八项[奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")，使他成为电影配乐产业中获得奥斯卡奖的数量最多的作曲家之一。其中《小美人鱼》、《美女与野兽》、《阿拉丁》和《钟楼怪人》在后来的十几年内又被分别改编为[百老汇音乐剧版](../Page/百老汇.md "wikilink")，也是由孟肯作曲。

亚伦·孟肯的妻子詹妮丝·孟肯（Janis
Menken）原是名[芭蕾舞演员](../Page/芭蕾舞.md "wikilink")。他们于1972年结婚，與两个孩子一起生活在[纽约](../Page/纽约.md "wikilink")。

## 主要作品

  - （1982年外百老汇音乐剧，1986年电影，2003年百老汇再映）

  - [小美人鱼](../Page/小美人鱼.md "wikilink")（1989年电影，2008年百老汇音乐剧）

  - [美女与野兽](../Page/美女与野兽.md "wikilink")（1991年电影，1994年百老汇音乐剧）

  - [报童传奇](../Page/报童传奇.md "wikilink")（1992年电影）

  - [阿拉丁](../Page/阿拉丁_\(电影\).md "wikilink")（1992年电影，2003年音乐剧）

  - [圣诞颂歌](../Page/圣诞颂歌_\(2004年电影\).md "wikilink")（1995年百老汇音乐剧，2004年电影）

  - [风中奇缘](../Page/风中奇缘.md "wikilink")（1995年电影）

  - [钟楼怪人](../Page/钟楼怪人_\(1996年电影\).md "wikilink")（1996年电影，1999年上演于[德国的音乐剧](../Page/德国.md "wikilink")）

  - [大力士](../Page/大力士.md "wikilink")（1997年电影）

  - [大卫王](../Page/大卫王_\(音乐剧\).md "wikilink")（1997年百老汇音乐剧）

  - [最后的城堡](../Page/最后的城堡.md "wikilink")（2001年电影）

  - [放牛吃草](../Page/放牛吃草.md "wikilink")（2004年电影）

  - [长毛狗](../Page/长毛狗_\(2006年电影\).md "wikilink")（2006年电影）

  - [音乐剧修女也疯狂](../Page/音乐剧修女也疯狂.md "wikilink")（2006年音乐剧）

  - [曼哈頓奇緣](../Page/曼哈頓奇緣.md "wikilink")（2007年电影）

  - [魔髮奇緣](../Page/魔髮奇緣.md "wikilink")（2010年电影）

  - [腸腸搞轟趴](../Page/腸腸搞轟趴.md "wikilink") (2016年電影)

  - [美女与野兽](../Page/美女与野兽.md "wikilink")（1991年电影，2017年电影真人版）

## 获奖纪录

### [奥斯卡奖提名及获奖](../Page/奥斯卡奖.md "wikilink")

  - 1986年：
      - 提名，[最佳歌曲奖](../Page/奥斯卡最佳歌曲奖.md "wikilink")（和[霍华德·爱许曼](../Page/霍华德·爱许曼.md "wikilink")）
        - 《[恐怖小店](../Page/恐怖小店.md "wikilink")》的《Mean Green Mother from
        Outer Space》
  - 1989年：
      - **获奖**，[最佳原创音乐奖](../Page/奥斯卡最佳原创音乐奖.md "wikilink") -
        《[小美人鱼](../Page/小美人鱼.md "wikilink")》
      - **获奖**，最佳歌曲奖（和霍华德·爱许曼） - 《小美人鱼》的《Under the Sea》
      - 提名，最佳歌曲奖（和霍华德·爱许曼） - 《小美人鱼》的《Kiss the Girl》
  - 1991年：
      - **获奖**，最佳原创音乐奖 -
        《[美女与野兽](../Page/美女與野獸_\(1991年電影\).md "wikilink")》
      - **获奖**，最佳歌曲奖（和霍华德·爱许曼） - 《美女与野兽》的《Beauty and the Beast》
      - 提名，最佳歌曲奖（和霍华德·爱许曼） - 《美女与野兽》的《Belle》
      - 提名，最佳歌曲奖（和霍华德·爱许曼） - 《美女与野兽》的《Be Our Guest》
  - 1992年：
      - **获奖**，最佳原创音乐奖 - 《[阿拉丁](../Page/阿拉丁_\(电影\).md "wikilink")》
      - **获奖**，最佳歌曲奖（和[蒂姆·赖斯](../Page/蒂姆·赖斯.md "wikilink")） - 《阿拉丁》的《A
        Whole New World》
      - 提名，最佳歌曲奖（和霍华德·爱许曼） - 《阿拉丁》的《Friend Like Me》
  - 1995年：
      - **获奖**，最佳歌曲奖（和[史蒂芬·史华兹](../Page/史蒂芬·史华兹.md "wikilink")） -
        《[风中奇缘](../Page/風中奇緣_\(電影\).md "wikilink")》的《Colors of
        the Wind》
      - **获奖**，最佳音乐剧或喜剧音乐奖（和史蒂芬·史华兹） - 《风中奇缘》
  - 1996年：
      - 提名，最佳音乐剧或喜剧音乐奖（和史蒂芬·史华兹） -
        《[钟楼怪人](../Page/钟楼怪人_\(1996年电影\).md "wikilink")》
  - 1997年：
      - 提名，最佳歌曲奖（和[大卫·吉波](../Page/大卫·吉波.md "wikilink")） -
        《[大力士](../Page/大力士_\(1997年電影\).md "wikilink")》的《Go the
        Distance》
  - 2007年：
      - 提名，最佳歌曲奖（和[史蒂芬·史华兹](../Page/史蒂芬·史华兹.md "wikilink")） -
        《[曼哈頓奇緣](../Page/曼哈頓奇緣.md "wikilink")》的《Happy
        Working Song》
      - 提名，最佳歌曲奖（和史蒂芬·史华兹） - 《曼哈頓奇緣》的《So Close》
      - 提名，最佳歌曲奖（和史蒂芬·史华兹） - 《曼哈頓奇緣》的《That's How You Know》
  - 2011年：
      - 提名，最佳原創歌曲獎 - 《[魔髮奇緣](../Page/魔髮奇緣.md "wikilink")》的《[I See the
        Light](../Page/I_See_the_Light.md "wikilink")》

### [东尼奖及](../Page/东尼奖.md "wikilink")[剧评人奖](../Page/剧评人奖.md "wikilink")

  - 1983年提名剧评人奖之杰出音乐奖 - 《恐怖小店》
  - 1994年提名东尼奖之最佳原创音乐奖 - 《美女与野兽》
  - 1994年提名剧评人奖之杰出音乐奖 - 《美女与野兽》

## 注解

<div class="references-small">

<references />

</div>

## 站外链接

  - [亚伦·孟肯(Alan
    Menken)](https://archive.is/20021102093331/http://disneynotredame.netfirms.com/music/alan.htm)
  - [迪士尼百科之亚伦·孟肯](https://web.archive.org/web/20071026041544/http://www.disneybox.com/wiki/index.php?title=Alan_Menken)
  - [Walt Disney Records: Biography of Alan
    Menken](https://web.archive.org/web/20071019222323/http://www.britannica.com/eb/article-9112996)
  - [The Whole New World of Alan
    Menken（非官方）](http://www.alanmenken.info/)

[Category:迪士尼人物](../Category/迪士尼人物.md "wikilink")
[Menken](../Category/美国音乐家.md "wikilink")
[Menken](../Category/美国钢琴家.md "wikilink")
[Menken](../Category/葛莱美奖获得者.md "wikilink")
[Menken](../Category/奥斯卡最佳歌曲奖获奖作曲家.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")

1.  1991年霍华德·爱许曼逝世，《阿拉丁》部分歌曲的词由[蒂姆·賴斯填写](../Page/蒂姆·賴斯.md "wikilink")。