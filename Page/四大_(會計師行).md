**四大**（），又譯為**四大會計師事務所**、**四大審計公司**，合稱**EDPK**，是指4家最大的[國際](../Page/跨國公司.md "wikilink")網路，由[二戰後的八大會計師事務所](../Page/戰後.md "wikilink")（）多次[合併而成](../Page/合併.md "wikilink")。他們原本都是[會計師事務所](../Page/會計師事務所.md "wikilink")，但因業務擴及、[稅務](../Page/稅.md "wikilink")、[管理顧問](../Page/管理顧問.md "wikilink")、諮詢、[精算](../Page/精算學.md "wikilink")、企業財務、法律服務等整合性的，因而以「專業服務」稱之。有別於一般的[跨國公司](../Page/跨國公司.md "wikilink")，其於世界各地以類似[加盟的形式設立據點](../Page/加盟.md "wikilink")，各地據點在經營上擁有一定的自主權。

四大為許多國際知名的[上市公司與](../Page/上市公司.md "wikilink")[私人公司處理絕大多數的財務](../Page/私人公司.md "wikilink")[審計](../Page/審計.md "wikilink")，在全球專業服務業界形成[寡佔](../Page/寡佔.md "wikilink")。報導指出99%的[富時100指數企業](../Page/富時100指數.md "wikilink")、以及96%的[富時250指數企業的審計業務均由四大負責](../Page/富時250指數.md "wikilink")\[1\]。

| 名稱                                 | 原名       | 收益(USD) | 職員數     | 每職員產生收益  | 財務年度 | 總部位置                           | 來源    |
| ---------------------------------- | -------- | ------- | ------- | -------- | ---- | ------------------------------ | ----- |
| [德勤](../Page/德勤.md "wikilink")     | Deloitte | $34.2bn | 210,000 | $162,857 | 2014 | [美國](../Page/美國.md "wikilink") | \[2\] |
| [普华永道](../Page/普华永道.md "wikilink") | PwC      | $34.0bn | 195,000 | $174,359 | 2014 | [英國](../Page/英國.md "wikilink") | \[3\] |
| [安永](../Page/安永.md "wikilink")     | EY       | $27.4bn | 190,000 | $144,211 | 2014 | [英國](../Page/英國.md "wikilink") | \[4\] |
| [畢馬威](../Page/畢馬威.md "wikilink")   | KPMG     | $24.8bn | 162,000 | $153,209 | 2014 | [荷蘭](../Page/荷蘭.md "wikilink") | \[5\] |

[BDO](../Page/BDO國際.md "wikilink")（BDO Global）與（Grant
Thornton）分別為第五大和第六大公司。

## 八大（1970年代-1989）

自[二戰後冒升的十大合併而成](../Page/二戰.md "wikilink")：

1.  [安達信會計師事務所](../Page/安達信會計師事務所.md "wikilink")
2.  [Arthur Young & Company](../Page/安永.md "wikilink")
3.  [容永道會計師事務所](../Page/普華永道.md "wikilink")
4.  [Ernst & Whinney](../Page/安永.md "wikilink")
5.  [德勤会计师事务所](../Page/德勤会计师事务所.md "wikilink")
6.  [Touche Ross](../Page/德勤.md "wikilink")
7.  [毕马威會計師事務所](../Page/毕马威會計師事務所.md "wikilink")
8.  [普華會計師事務所](../Page/普華永道.md "wikilink")

## 六大（1989-1998）

隨著競爭激烈，Arthur **Young**與**Ernst** & Whinney合併而成Ernst &
Young（[安永會計師事務所](../Page/安永會計師事務所.md "wikilink")）；Deloitte、Haskins
& Sells及Touche Ross合併成Deloitte Touche
Tohmatsu（[德勤會計師行](../Page/德勤會計師行.md "wikilink")），自此大幅拉開與二線（2nd
Tier）會計師行的距離：

1.  安達信會計師事務所
2.  安永會計師事務所
3.  容永道會計師事務所
4.  德勤會計師行
5.  畢馬威會計師事務所
6.  普華會計師事務所

## 五大（1998-2002）

1998年7月容永道會計師事務所及普華會計師事務所所合併成[普華永道會計師事務所](../Page/普華永道會計師事務所.md "wikilink")：

1.  安達信會計師事務所
2.  安永會計師事務所
3.  德勤會計師行
4.  畢馬威會計師事務所
5.  普華永道聯合會計師事務所

## 四大（2002-迄今）

2001年底，[安達信會計師事務所因](../Page/安達信會計師事務所.md "wikilink")[安隆案而崩潰](../Page/安隆.md "wikilink")，業務分別被其餘大所購併（例如美國部份為安永所收購），但安達信管理顧問業務因在崩潰前分拆出Andersen
Consulting（現稱[埃森哲](../Page/埃森哲.md "wikilink")）而得以保留，成為商業顧問龍頭之一。

1.  普華永道聯合會計師事務所
2.  德勤聯合會計師事務所
3.  安永聯合會計師事務所
4.  畢馬威聯合會計師事務所

## 參見

  -
  -
## 參考資料

## 外部連結

  - [Deloitte](../Page/Deloitte.md "wikilink")

  - [Ernst & Young](../Page/Ernst_&_Young.md "wikilink")

  - [PriceWaterhouseCoopers](../Page/PriceWaterhouseCoopers.md "wikilink")

  - [KPMG](../Page/KPMG.md "wikilink")

[+](../Category/会计师事务所.md "wikilink")
[務](../Category/公司列表.md "wikilink")
[Category:八大](../Category/八大.md "wikilink")
[Category:六大](../Category/六大.md "wikilink")
[Category:五大](../Category/五大.md "wikilink")
[Category:四大](../Category/四大.md "wikilink")

1.
2.
3.
4.
5.