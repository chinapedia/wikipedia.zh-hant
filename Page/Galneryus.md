**Galneryus**，是[日本的](../Page/日本.md "wikilink")[重金屬音樂](../Page/重金屬音樂.md "wikilink")、[力量金屬](../Page/力量金屬.md "wikilink")、[新古典金屬](../Page/新古典金屬.md "wikilink")、[旋律金屬樂隊](../Page/旋律金屬.md "wikilink")。2001年於[大阪組成](../Page/大阪.md "wikilink")，2003年由所屬唱片公司[Vap正式推廌出道](../Page/Vap.md "wikilink")。

## 成員

  - [小野正利](../Page/小野正利.md "wikilink")（[主唱](../Page/主唱.md "wikilink")）
  - [Syu](../Page/Syu.md "wikilink")（[吉他](../Page/吉他.md "wikilink")）
  - YUHKI（[鍵盤](../Page/鍵盤.md "wikilink")）
  - FUMIYA（[鼓](../Page/鼓.md "wikilink")）
  - TAKA（[貝斯](../Page/貝斯.md "wikilink")）

### 舊成員

  - TSUI（貝斯）
  - YAMA-B（主唱）
  - Yu-to (Leda\[1\])（貝斯）（吉他）
  - JUNICHI（鼓）

## 來歷

「Galneryus」這個名字源自史上最偉大的製琴名家之一，[意大利的](../Page/意大利.md "wikilink")[Guarneri家族製作的](../Page/Guarneri.md "wikilink")[Guarnerius](../Page/Guarnerius.md "wikilink")[小提琴](../Page/小提琴.md "wikilink")，是由雙親均擔任[鋼琴教師](../Page/鋼琴.md "wikilink")，從小在[古典音樂氛圍濃郁的家庭成長起來的SYU所命名](../Page/古典音樂.md "wikilink")，爲了描繪SYU本人對於音樂的理解，并將音樂的美好傳達給樂迷們。

在2001年，從小喜愛著[日本](../Page/日本.md "wikilink")[視覺系鼻祖樂隊](../Page/視覺系.md "wikilink")[X
JAPAN以及其靈魂人物](../Page/X_JAPAN.md "wikilink")[YOSHIKI的SYU](../Page/YOSHIKI.md "wikilink")，將自己的視覺系樂隊「VALKYR」解散了，根據他在採訪中公開的原因是：「隊員的實力不濟，樂隊吸引不了觀眾，視覺系樂隊大多只是看上去很耀目而已」。

在他流連于LIVE
HOUSE之際，結識了以「GUNBRIDGE」為名義活動，集[作詞](../Page/作詞.md "wikilink")、[作曲](../Page/作曲.md "wikilink")、[演奏](../Page/演奏.md "wikilink")、[演唱于一身的SOLO歌手YAMA](../Page/演唱.md "wikilink")-B（山口將宏），兩人經過一番接觸并合作了一曲[Stratovarius的Father](../Page/Stratovarius.md "wikilink")
Time后，決定組成 Galneryus。

其後 SYU
召集了包括其舊識在內的一些不知名樂手作為臨時成員，錄製了樂隊的首隻[單曲](../Page/單曲.md "wikilink")《United
Flag》。這支單曲吸引了日後成為 Galneryus
製作人的日本著名音樂人[久武賴正的目光](../Page/久武賴正.md "wikilink")。樂隊隨後進入他旗下的[品牌](../Page/品牌.md "wikilink")
IRON SHOCK，并發行第二張錄音室單曲《Rebel Flag》。這期間，Galneryus
也積極參加各種規模不等的演出，吸引到了越來越多真正樂迷的關注。2003年
GALNERYUS 與 VAP 唱片公司簽約，正式出道。

此時，樂隊已經確定了以 SYU 為[吉他手](../Page/吉他手.md "wikilink")，YAMA-B 為主唱，原「ARK
STORM」的 YUHKI 擔任鍵盤手，原「Concerto Moon」的 JUNICHI SATO 擔任鼓手，以及 TSUI
擔任貝斯手的正式陣容。從首張專輯《The Flag of
Punishment》開始，隊邀請了日本著名[插畫家](../Page/插畫家.md "wikilink")[天野喜孝為他們繪製](../Page/天野喜孝.md "wikilink")[唱片](../Page/唱片.md "wikilink")[封面](../Page/封面.md "wikilink")。

2006年，經過三張具有震撼性的專輯后，樂隊的知名度和 SYU
的影響力都達到了一個小高峰，[ESP為其設計的簽名琴CRYING](../Page/ESP.md "wikilink")
STAR登場，在當年LIVE FOR REBIRTH 的巡演中被 SYU 所使用，在逐漸成為 ESP
頂級[代言人的同時](../Page/代言人.md "wikilink")，日本著名搖滾吉他雜誌《YOUNG
GUITAR》也為 SYU 出版了個人專題的特刊〈SYU 100%〉。

在日本的金屬界里，作為力量金屬的代表性樂隊，GALNERYUS 也以成員過硬強悍的技術，華麗恢弘的編曲，美妙激昂的旋律，以及大量的 SYU
招牌性的動聽
SOLO，形成了以自己為標籤的日本[新古典](../Page/新古典.md "wikilink")。在同年的巡演后，貝斯手TSUI
退出樂隊，由和 SYU 有著師徒關係的吉他手 YUTO(Leda) 頂替貝斯手的位置，並在2007年發表了第四張專輯。

由此時開始，GALNERYUS
的音樂變得更加趨於[簡單和](../Page/簡單.md "wikilink")[大眾化](../Page/大眾化.md "wikilink")，不僅在歌迷中引發[耳語和](../Page/耳語.md "wikilink")[爭議](../Page/爭議.md "wikilink")，2008年，主唱
YAMA-B 在第五張專輯的錄製中向外公開了“音樂理念不合”，並將在巡演后退出 GALNERYUS 的聲明。一石激起千層浪，樂迷們對 SYU
的質疑之聲頓時不絕於耳，加之其後不久貝斯手 YUTO(Leda) 也宣佈離隊，外界變開始猜測 GALNERYUS
將面臨一段遙遙無期的沉寂。

2009年，因為受邀參加 PURE ROCK JAPAN，SYU
和久武賴正商議后決定邀請日本著名的高音男歌手[小野正利擔任臨時主唱](../Page/小野正利.md "wikilink")，并招募了先前還默默無名的貝斯手
TAKA，以便順利出演。

這次演出成為了兩位成員正式加入 GALNERYUS 的契機和試驗。2010年，GALNERYUS
推出了可能是目前銷量最高的第六張錄音室專輯《Resurrection》，
在[日本公信榜首周排名](../Page/日本公信榜.md "wikilink")35位，并在一個月後得到了在[NHK電視臺音樂節目](../Page/NHK.md "wikilink")《MUSIC
JAPAN》做現場表演的機會。

2011年，因受311[東日本大地震的影響](../Page/東日本大地震.md "wikilink")，GALNERYUS
將新專輯的主題定義“為日本加油”，[ORICON](../Page/ORICON.md "wikilink")
初登場17位，有望打破前作的銷量記錄。同時，小野正利作為 SOLO 歌手也在積極開拓個人的事業，并直接為想要開拓音樂版圖，挑戰主流音樂界的
GALNERYUS 作更多的宣傳。

作為一隻高產的樂隊，每年都馬不停蹄的發行一張全新的高質量原創專輯已經是GALNERYUS的慣例，2012年樂隊的第七張錄音室大碟ANGEL OF
SALVATION
橫空出世，其中時長15分鐘的同名曲目以柴可夫斯基D大调第三小提琴协奏曲为创作背景，将力量金属的氣勢和古典音樂的優雅巧妙的融合在一起，為整張
作品帶來最好的結尾。雖然主唱小野正利在個人FACEBOOK發表的一系列針對領土爭端及二戰歷史的政治言論，導致新專輯的銷量在樂隊僅次於日本的
第二大市場的韓國國內急劇下滑，但在發行當天ANGEL OF
SALVATION卻也破天荒在ORICON排行榜上雄踞TOP10的位置。轉瞬進入2013年，早已順理成章躋身
日本大牌金屬樂隊行列的GALNERYUS，迎來了在VAP旗下出道十周年，樂隊繼續對聽眾的耳膜實施連番轟炸，將過去十年樂迷記憶最深刻的歌曲重新編排，并相繼
發行了2張去芜存菁的SELF-COVER專輯。同年5月首次參加了移師日本舉辦的OZZFEST，在歷來以流行金屬和金屬核吸引眼球的音樂節上獻唱了偉大的
ANGEL OF SALVATION，驚豔全場。

2013年10月14日，例行舉辦的全國巡演在東京涉谷公會堂落下帷幕，這是迄今GALNERYUS所舉辦的規模最大，製作最精良的專場演出，樂隊成員與到場
的2000名觀眾一起共襄盛舉，為十周年的壓軸節目畫下了一個完美的句號。

2016年6月18日，GALNERYUS 官方facebook和twitter宣佈鼓手JUNICHI 離開GALNERYUS
的消息，同時加入了新的鼓手FUMIYA 。

## 作品

### 專輯

  - 2003年
  - 2005年
  - 2006年
  - 2007年 [ONE FOR ALL - ALL FOR
    ONE](../Page/ONE_FOR_ALL_-_ALL_FOR_ONE.md "wikilink")
  - 2008年
  - 2010年 [Resurrection](../Page/Resurrection.md "wikilink")
  - 2011年
  - 2012年 [絆](../Page/絆.md "wikilink")(迷你專輯)
  - 2012年
  - 2013年 [THE IRONHEARTED FLAG Vol.1 : REGENERATION
    SIDE(完全生産限定盤)](../Page/THE_IRONHEARTED_FLAG_Vol.1_:_REGENERATION_SIDE\(完全生産限定盤\).md "wikilink")
  - 2013年 [THE IRONHEARTED FLAG Vol.2 : REFORMATION
    SIDE(完全生産限定盤)](../Page/THE_IRONHEARTED_FLAG_Vol.2_:_REFORMATION_SIDE\(完全生産限定盤\).md "wikilink")
  - 2014年
  - 2015年
  - 2017年

### 精選專輯

  - 2009年
  - 2009年

### 單曲

  - 2001年 [United Flag](../Page/United_Flag.md "wikilink")
  - 2002年 [Rebel Flag](../Page/Rebel_Flag.md "wikilink")
  - 2007年 [Everlasting](../Page/Everlasting.md "wikilink")
  - 2008年 [Alsatia /Cause
    Disarray](../Page/Alsatia_/Cause_Disarray.md "wikilink")（[動畫](../Page/動畫.md "wikilink")《[記憶女神的女兒們](../Page/記憶女神的女兒們.md "wikilink")》片頭曲、片尾曲）
  - 2008年 [Shining Moments](../Page/Shining_Moments.md "wikilink")
  - 2010年 [A FAR-OFF
    DISTANCE](../Page/A_FAR-OFF_DISTANCE.md "wikilink")（[動畫](../Page/動畫.md "wikilink")《[少年犯罪檔案](../Page/少年犯罪檔案.md "wikilink")》片尾曲）
  - 2011年 [Future Never Dies](../Page/Future_Never_Dies.md "wikilink")
  - 2011年 [絆 FIST OF THE BLUE
    SKY](../Page/絆_FIST_OF_THE_BLUE_SKY.md "wikilink")（[柏青哥](../Page/柏青哥.md "wikilink")《ぱちんこ
    CR 蒼天の拳2》主题曲）
  - 2012年 [Hunting For Your
    Dream](../Page/Hunting_For_Your_Dream.md "wikilink")
  - 2014年 [ATTITUDE TO
    LIFE](../Page/ATTITUDE_TO_LIFE.md "wikilink")（[動畫](../Page/動畫.md "wikilink")《[笑傲曇天](../Page/笑傲曇天.md "wikilink")》片尾曲）

### 其他

  - 2002年 （合輯、Stratovarius的「Black Diamond」翻唱曲）
  - 2002年 （合輯、LOUDNESS的「Soldier Of Fortune」翻唱曲）
  - 2006年 The songs for DEATH NOTE the movie \~the Last name
    TRIBUTE\~（合輯、以Serenade（D.N.Mix）參加）
  - 2007年 Voice From The Past
  - 2008年 Voice From The Past Ⅱ
  - 2010年 Voice From The Past Ⅲ
  - 2013年 GALNERYUS GUITAR BOOK feat. Syu (DVD付) (シンコー・ミュージックMOOK)

### DVD

  - 2006年 LIVE FOR REBIRTH
  - 2008年 LIVE FOR ALL - LIVE FOR ONE
  - 2010年 LIVE IN THE MOMENT OF THE RESURRECTION

### 電視節目

  - （第1話嘉賓）

## 關聯樂隊

  - [Animetal](../Page/Animetal.md "wikilink")／Syu
  - [AUSHVITZ](../Page/AUSHVITZ.md "wikilink")／Syu
  - [VALKYR](../Page/VALKYR.md "wikilink")／Syu
  - [GUNBRIDGE](../Page/GUNBRIDGE.md "wikilink")／YAMA-B
  - [AXBITES](../Page/AXBITES.md "wikilink")／YAMA-B
  - [ARK STORM](../Page/ARK_STORM.md "wikilink")／YUHKI
  - [ALHAMBRA](../Page/ALHAMBRA.md "wikilink")／YUHKI
  - [PROPHESIA](../Page/PROPHESIA.md "wikilink")／YUHKI
  - [Concerto Moon](../Page/Concerto_Moon.md "wikilink")／JUNICHI（佐藤潤一）
  - [SOH-BAND](../Page/SOH-BAND.md "wikilink")／JUNICHI（佐藤潤一）
  - [DELUHI](../Page/DELUHI.md "wikilink")（原為GRAVE
    SEED）／Yu-to（以Yuuto或LEDA＊的名義）
  - [UNDIVIDE](../Page/UNDIVIDE.md "wikilink")／Yu-to（以LEDA名義）
  - [BABYMETAL](../Page/BABYMETAL.md "wikilink")／Yu-to（以LEDA名義）

## 參考資料

## 外部連結

  - [Galneryus Official Web Site](http://galneryusyumacher.com/)
  - [Galneryus（VAP）](http://www.vap.co.jp/galneryus/)
  - [Syu Personal
    Site](https://web.archive.org/web/20080708062132/http://blogs.yahoo.co.jp/syugalspinal)
  - [YAMA-B Personal
    Site](https://web.archive.org/web/20080708214316/http://www.eonet.ne.jp/~yama-b/)
  - [JUNICHI Personal
    Site](https://web.archive.org/web/20080622140014/http://www7.plala.or.jp/zun-s/)
  - [YUHKI Personal
    Site](https://web.archive.org/web/20080430102420/http://www.alhambra.ws/yuhki/index2.html)
  - [TAKA Personal Site](http://tiproject.blog.shinobi.jp/)

[Category:日本樂團](../Category/日本樂團.md "wikilink")
[Category:重金屬樂團](../Category/重金屬樂團.md "wikilink")
[Category:日本重金屬樂團](../Category/日本重金屬樂團.md "wikilink")
[Category:VAP旗下藝人](../Category/VAP旗下藝人.md "wikilink")

1.