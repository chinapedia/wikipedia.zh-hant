**Opaku's Train
Kit**（[日文為](../Page/日文.md "wikilink")**おぱく堂版・電車走行キット**或**電車走行キット**），簡稱
**Train Kit** 或 **TK** ，是一套由日本人**Mr.
Opaku**（[日文為](../Page/日文.md "wikilink")**おぱく堂**）開發的模擬[鐵路行走的](../Page/鐵路.md "wikilink")[網頁](../Page/網頁.md "wikilink")[動畫](../Page/動畫.md "wikilink")。

## 特點

**Train Kit**
是一套以規格***10cm[=](../Page/等於.md "wikilink")[1px](../Page/像素.md "wikilink")***繪製的動畫，可以放在網頁上供點綴或裝飾，用以模擬鐵路列車行駛的情況。加上製作簡單，[日本已有不少](../Page/日本.md "wikilink")'''
Train Kit '''網站。

## 構造

**Train Kit**
由**背景**、**列車**、**物件**及**前景**<font face=標楷體>（如有）</font>組成，每部份都是由一<font face=標楷體>（或以上）</font>幅圖組成。再加上網頁原本的[原始檔](../Page/原始檔.md "wikilink")，即成一
**Train Kit** 動畫。因此，製作非常簡單，只須畫好圖，設定好原始檔內容，快則一小時即可完成。

## 展示

**Train Kit**
動畫是一個[網頁檔](../Page/網頁.md "wikilink")，可以輕易地放到[-{zh-hk:互聯網;zh-tw:網際網絡;}-](../Page/網際網絡.md "wikilink")，供網友觀看。（唯上載時須連同**<u>所有</u>**圖檔）

## 分類

**Train Kit**
主要分為「**A類**」和「**M類**」。**A類**是[自動行駛](../Page/自動.md "wikilink")，即是只供觀看；**M類**是[手動行駛](../Page/手動.md "wikilink")，即列車由網友操作。分類的方法只須看[版本編號有](../Page/版本.md "wikilink")**A**字或是**M**字。例如**A01**、**A02**、**A667**等都是自動行駛；**M01**、**M02**、**M10SL**等都是手動行駛。還有一種叫做「**簡易行駛**」，識別方法是看有否*'
00 **或** CA
**字樣，如：**LA00**、**TA00SL**、**CA50SL**等。此外，「**SL*'」代表[蒸氣](../Page/蒸氣.md "wikilink")[機車](../Page/機車.md "wikilink")

### 介紹

以下簡介各種''' Train Kit '''版本的不同。注意：每個版本是獨立的。

#### 自動行駛

  - **A01**

<!-- end list -->

  -
    最基本的版本。自動行駛，可以設定信號、[平交道](../Page/平交道.md "wikilink")、月台等。此版本只支援動力分散列車（EMU）行走。

<!-- end list -->

  -
    公開時間：2001年4月與[M01一起](../Page/#手動行駛.md "wikilink")

<!-- end list -->

  - **A02**

<!-- end list -->

  -
    與**A01**差不多，都是有信號、平交道、月台等等。但此版本可以設定[柴油及](../Page/柴油.md "wikilink")[內燃機車行走](../Page/內燃機車.md "wikilink")，每一次出現時[機車都會在編成最前方](../Page/機車.md "wikilink")。

<!-- end list -->

  - **A02SL**

<!-- end list -->

  -
    與**A02**差不多，但是可以設定[蒸氣機車行走](../Page/蒸氣機車.md "wikilink")，有蒸氣機車的黑煙及「動輪」效果。

<!-- end list -->

  -
    公開時間：2003年5月

<!-- end list -->

  - **A02SL-Expert**

<!-- end list -->

  -

<!-- end list -->

  - **A02SL-Kids**

<!-- end list -->

  -
    與**A02SL**差不多，但是改為鐵路模型規格（官方網頁稱為「**Kids規格**」，即[TOMY列車](../Page/TOMY.md "wikilink")），並不能設定黑煙效果。

<!-- end list -->

  - **A03**

<!-- end list -->

  -

<!-- end list -->

  - **A03-TEC**

<!-- end list -->

  -
    即**A03T**，是**A03**「[新幹線](../Page/新幹線.md "wikilink")」版。加入了大量高速鐵路使用的圖檔，可設定[月台閘門](../Page/月台閘門.md "wikilink")（APG），省略了[平交道](../Page/平交道.md "wikilink")，亦配備高速列車的柔邊效果。

<!-- end list -->

  -
    公開時間：[2004年3月](../Page/2004年3月.md "wikilink")

<!-- end list -->

  - **A04** *（未公開）*

<!-- end list -->

  -

<!-- end list -->

  - **A05**

<!-- end list -->

  -

<!-- end list -->

  - **A06**

<!-- end list -->

  -

<!-- end list -->

  - **A07**

<!-- end list -->

  -
    A01[路面電車版](../Page/路面電車.md "wikilink")。可設定多種路面電車專用的物件，包括兩款開門檔案（前方／後方），路面電車專用信號（[馬路](../Page/馬路.md "wikilink")[紅綠燈](../Page/紅綠燈.md "wikilink")）等。

<!-- end list -->

  -
    公開時間：2003年11月

<!-- end list -->

  - **A08**

<!-- end list -->

  -
    A01[地下鐵版](../Page/地下鐵.md "wikilink")。增設大量地下鐵專用的效果，如較暗的列車、較光的車站等。此外，也成功刪除了一些錯誤（bug）。

<!-- end list -->

  -
    公開時間：2003年11月

<!-- end list -->

  - **A09SL** *（未公開）*

<!-- end list -->

  -

<!-- end list -->

  - **A11** *（未公開）*

<!-- end list -->

  -

<!-- end list -->

  - **A14** *（未公開）*

<!-- end list -->

  -

<!-- end list -->

  - **GA30SL**

<!-- end list -->

  -

<!-- end list -->

  - **UA01**

<!-- end list -->

  -

<!-- end list -->

  - **IA01**

<!-- end list -->

  -

<!-- end list -->

  - **A225**

<!-- end list -->

  -

<!-- end list -->

  - **A667**

#### 手動行駛

  - **M01**

<!-- end list -->

  -
    手動行駛基礎版本。可設定的圖像和列車與A01相同，但是可以由使用者操作列車，而版面亦固定為跟隨列車而非之設的固定風景。

<!-- end list -->

  -
    公開時間：2001年4月與[A01一起](../Page/#自動行駛.md "wikilink")

<!-- end list -->

  - '''M01D

<!-- end list -->

  -
    與M01相同,但像BEV

<!-- end list -->

  - **M01SL-Kids** *（未公開）*

<!-- end list -->

  -
    **Kids規格**（即[TOMY列車](../Page/TOMY.md "wikilink")）的手動行駛版本，**Opaku先生**正有意開發。

<!-- end list -->

  - **M02**

<!-- end list -->

  -

<!-- end list -->

  - **M02W**

<!-- end list -->

  -

<!-- end list -->

  - **M03**

<!-- end list -->

  -

<!-- end list -->

  - **M04**

<!-- end list -->

  -

<!-- end list -->

  - **M10SL**

<!-- end list -->

  -

<!-- end list -->

  - **M20** ''

<!-- end list -->

  -

<!-- end list -->

  - **M094** *（未公開）*

<!-- end list -->

  -

<!-- end list -->

  - **M539**

<!-- end list -->

  -
    即TKHK的M418

#### 簡易行駛

  - **M00N·M00B**

<!-- end list -->

  -

<!-- end list -->

  - **KA00**

<!-- end list -->

  -

<!-- end list -->

  - **LA00**

<!-- end list -->

  -

<!-- end list -->

  - **A00Z with SceneChanger**

<!-- end list -->

  -

<!-- end list -->

  - **M00SL**

<!-- end list -->

  -

<!-- end list -->

  - **A00SL**

<!-- end list -->

  -

<!-- end list -->

  - **RA00SL**

<!-- end list -->

  -

<!-- end list -->

  - **TA00SL**

<!-- end list -->

  -

<!-- end list -->

  - **CA50SL**

<!-- end list -->

  -

<!-- end list -->

  - **CA51SL**

<!-- end list -->

  -

<!-- end list -->

  - **CA52SL**

<!-- end list -->

  -

<!-- end list -->

  - **CA53SL**

<!-- end list -->

  -

<!-- end list -->

  - **CA54SL**

<!-- end list -->

  -

<!-- end list -->

  - **CA10** *（未公開）*

<!-- end list -->

  -

## 比較

### 優點

  - 可直接放到-{zh-hans:?;zh-hk:互聯網;zh-tw:網際網絡;}-
  - 製作簡單

### 缺點

  - 像真度較低
  - 沒有聲音

## 標誌

以下展示的是 **Train Kit** 的一些[標誌](../Page/標誌.md "wikilink")。所有有關 **Train
Kit** 的網頁均要顯示任何一個 **TK** 標誌。（注意：以下列的只是較常用的標誌）

<File:TKbannerS001.gif>|

<div style="text-align: center;">

常用''' TK '''標誌(1)

</div>

<File:TKbannerM001.gif>|

<div style="text-align: center;">

常用''' TK '''標誌(2)

</div>

<File:TKbannerM002.gif>|

<div style="text-align: center;">

常用''' TK '''標誌(3)

</div>

<File:TKbannerS004.gif>|

<div style="text-align: center;">

常用''' TK '''標誌(4)

</div>

## 下載

下載點為官方網站。''' Train Kit
'''是一套可供免費[下載而又合法的](../Page/下載.md "wikilink")[軟體](../Page/軟體.md "wikilink")。

  - [基本組套（A01+M01+X01）](http://www.mars.dti.ne.jp/~opaku/zigzag/railway/kitBasicset.html)
  - [擴張組合（其他）](http://www.mars.dti.ne.jp/~opaku/zigzag/railway/kitExtension.html)

## 外部連結

  - [Opaku's Train Kit
    官方網站](http://www.mars.dti.ne.jp/~opaku/zigzag/railway/)

  - [TrainKit@HK](http://www.geocities.co.jp/train_kit_hk/) - 香港''' TK
    '''網頁

  - [香港模擬鐵路聯盟（BVEHK）](https://web.archive.org/web/20070312021922/http://www.bvehk.net/)
    -
    香港[鐵路及](../Page/鐵路.md "wikilink")[BVE討論區](../Page/BVE.md "wikilink")（C4版區是關於
    **TK** 的）

[Category:铁路模拟游戏](../Category/铁路模拟游戏.md "wikilink")