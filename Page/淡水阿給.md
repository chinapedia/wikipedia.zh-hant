[Agei.JPG](https://zh.wikipedia.org/wiki/File:Agei.JPG "fig:Agei.JPG")
**淡水阿給**，是[台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[淡水區有名的](../Page/淡水區.md "wikilink")[小吃之一](../Page/小吃.md "wikilink")。[阿給](../Page/阿給_\(消歧義\).md "wikilink")（）的做法是將[油豆腐的中間挖空](../Page/豆卜.md "wikilink")，然後填充炒過的[冬粉](../Page/冬粉.md "wikilink")（有些店家使用的是沒有炒過的冬粉）、浸泡過[滷汁](../Page/滷汁.md "wikilink")，以[魚漿封口](../Page/魚漿.md "wikilink")，加以[蒸熟](../Page/蒸.md "wikilink")，食用前淋上[甜辣醬或其他特殊醬汁](../Page/甜辣醬.md "wikilink")。

## 名由

「阿給」是日本語「油炸豆腐皮」的音譯簡稱（日语：[油揚げ](../Page/:ja:油揚げ.md "wikilink")（[abura a
ge](../Page/:en:aburaage.md "wikilink")），簡稱 （a ge））。

## 參見

  - [非發酵豆製品](../Page/豆制品#非发酵豆制品.md "wikilink")
      - [豆卜、油豆腐](../Page/豆卜.md "wikilink")，一般由[豆腐經過高溫油炸而成](../Page/豆腐.md "wikilink")。
          - 薄切的豆卜（[油揚げ、あぶらあげ](../Page/:ja:油揚げ.md "wikilink")）
          - 厚切的豆卜（[厚い油揚げ、厚揚げ、あつあげ](../Page/:ja:厚揚げ.md "wikilink")）

<!-- end list -->

  - <small>另见</small>

<!-- end list -->

  - [炸、油炸](../Page/炸.md "wikilink")，一种中華料理烹調方法。日语[あげる](../Page/:ja:揚げる.md "wikilink")——“”音同“阿給”。
  - [唐扬](../Page/唐扬.md "wikilink")（），日本的一種與油炸相關的烹調法。

## 腳注

  - [分辨系列之淡水人篇　「真的很愛吃阿給」](http://www.appledaily.com.tw/realtimenews/article/new/20150112/540453/).蘋果日報.2015-01-12

## 外部連結

[Category:台灣小吃](../Category/台灣小吃.md "wikilink")
[Category:豆腐](../Category/豆腐.md "wikilink")
[Category:淡水區](../Category/淡水區.md "wikilink")
[食](../Category/台灣發明.md "wikilink")