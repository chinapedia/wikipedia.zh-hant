（带[尖音符的](../Page/尖音符.md "wikilink")[o](../Page/o.md "wikilink")）是[法罗语](../Page/法罗语.md "wikilink")、[匈牙利语](../Page/匈牙利语.md "wikilink")、[冰岛语](../Page/冰岛语.md "wikilink")、[卡舒比语](../Page/卡舒比语.md "wikilink")、[波兰语](../Page/波兰语.md "wikilink")、[捷克语](../Page/捷克语.md "wikilink")、[斯洛伐克语](../Page/斯洛伐克语.md "wikilink")、[上索布语](../Page/上索布语.md "wikilink")、[下索布语和](../Page/下索布语.md "wikilink")[鞑靼语的一个](../Page/鞑靼语.md "wikilink")[字母](../Page/字母.md "wikilink")。这个字母在[加泰罗尼亚语](../Page/加泰罗尼亚语.md "wikilink")、[爱尔兰语](../Page/爱尔兰语.md "wikilink")、[葡萄牙语](../Page/葡萄牙语.md "wikilink")、[越南语中](../Page/越南语.md "wikilink")，也作[变音字母使用](../Page/变音符号.md "wikilink")。

  - 在法罗语中，这个字母排在字母表的第 18 位，表示  音或  音。
  - 在匈牙利语中，这个字母排在字母表的第 25 位，表示  音（长／o／音）。
  - 在冰岛语中，这个字母排在字母表的第 19 位，表示  音。
  - 在波兰语中，这个字母排在字母表的第 21 位，表示  音。
  - 在捷克语中，这个字母排在字母表的第 24 位，表示  音。
  - 在斯洛伐克语中，这个字母排在字母表的第 28 位，表示  音。
  - 在[上索布语中](../Page/上索布语.md "wikilink")，这个字母表示  音。
  - 在[下索布语中](../Page/下索布语.md "wikilink")，这个字母表示  音或  音。
  - 在鞑靼语中，这个字母用来书写外来词用。
  - 在[越南语音系中](../Page/越南语音系.md "wikilink")， 是 o 的锐声（阴去声）。
  - 在[汉语拼音中](../Page/汉语拼音.md "wikilink")，
    作为[韵母](../Page/韵母.md "wikilink") o 的阳平声。

## 字符编码

<table>
<thead>
<tr class="header">
<th><p>字符编码</p></th>
<th><p><a href="../Page/Unicode.md" title="wikilink">Unicode</a></p></th>
<th><p><a href="../Page/ISO/IEC_8859.md" title="wikilink">ISO 人大8859</a>-<a href="../Page/ISO/IEC_8859-1.md" title="wikilink">1</a>，<a href="../Page/ISO/IEC_8859-2.md" title="wikilink">2</a>，<a href="../Page/ISO/IEC_8859-3.md" title="wikilink">3</a>，<a href="../Page/ISO/IEC_8859-9.md" title="wikilink">9</a>，<br />
<a href="../Page/ISO/IEC_8859-10.md" title="wikilink">决人10</a>，<a href="../Page/ISO/IEC_8859-13.md" title="wikilink">13</a>，<a href="../Page/ISO/IEC_8859-14.md" title="wikilink">14</a>，<a href="../Page/ISO/IEC_8859-15.md" title="wikilink">15</a>，<a href="../Page/ISO/IEC_8859-16.md" title="wikilink">16</a></p></th>
<th><p><a href="../Page/VISCII.md" title="wikilink">VISCII</a></p></th>
<th><p><a href="../Page/GB_2312.md" title="wikilink">GB 2312</a></p></th>
<th><p><a href="../Page/香港增補字符集.md" title="wikilink">HKSCS</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/大寫字母.md" title="wikilink">大写</a> </p></td>
<td><p>U+00D3</p></td>
<td><p>D3</p></td>
<td><p>D3</p></td>
<td><p>/</p></td>
<td><p>885F</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小寫字母.md" title="wikilink">小写</a> </p></td>
<td><p>U+00F3</p></td>
<td><p>F3</p></td>
<td><p>F3</p></td>
<td><p>A8AE</p></td>
<td><p>8875</p></td>
</tr>
</tbody>
</table>

[OÓ](../Category/衍生拉丁字母.md "wikilink")