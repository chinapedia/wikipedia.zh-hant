**氟伏沙明**（**Fluvoxamine**）是一种[选择性5-羟色胺再吸收抑制剂](../Page/选择性5-羟色胺再吸收抑制剂.md "wikilink")（SSRI）型的[抗抑郁药](../Page/抗抑郁药.md "wikilink")，其药物形态为**马来酸氟伏沙明**（**Fluvoxamine
Maleate**），商品名为“**兰释**”（**Luvox**）。在临床上常用于[抑郁症及相关症状和](../Page/抑郁症.md "wikilink")[强迫症的治疗](../Page/强迫症.md "wikilink")。同时，兰释也可以治疗暴饮暴食症。

[category:选择性血清素重摄取抑制剂](../Page/category:选择性血清素重摄取抑制剂.md "wikilink")

[Category:Σ激动剂](../Category/Σ激动剂.md "wikilink")
[Category:肟](../Category/肟.md "wikilink")
[Category:醚](../Category/醚.md "wikilink")
[Category:有机氟化合物](../Category/有机氟化合物.md "wikilink")
[Category:三氟甲基化合物](../Category/三氟甲基化合物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")