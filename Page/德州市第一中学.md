**德州市第一中学**（简称：德州一中）于1929年建校，是[山东省](../Page/山东省.md "wikilink")19所首批办好的[重点中学之一](../Page/重点中学.md "wikilink")，省级规范化学校，省级精神文明先进单位。德州一中现有东、西两校区。德州一中位于[德州市德城区解放路](../Page/德州市.md "wikilink")，东校区位于德州市经济开发区新河东路。在校生约有5000余人。目前学校只开设[高中阶段教学](../Page/高中.md "wikilink")。

## 概况

[缩略图](https://zh.wikipedia.org/wiki/File:德州一中老校区西门.jpg "fig:缩略图")
德州一中位于[德城区市区](../Page/德城区.md "wikilink")，毗邻德州市美丽华大酒店，学校周围大多为商业区和居民区。目前学校拥有两栋学生[宿舍楼](../Page/宿舍.md "wikilink")，一栋餐厅楼，三栋教学楼，一栋办公楼，以及[操场与篮球场](../Page/操场.md "wikilink")。东校区位于德州市經濟開發區新河東路。東校區建筑面积约11万平方米，现有教职工150多人，目前有31个教学班，学生1800多名。

### 教学

目前德州一中只开设[高中阶段教育](../Page/高中.md "wikilink")，另外德州一中曾经开设过[初中阶段教育](../Page/初中.md "wikilink")，但已经停止，目前学校内只设高中三个年级以及[复课班](../Page/复课班.md "wikilink")。
每年高考结束后，学校的升学率是衡量[高级中学教学质量的重要标准](../Page/高级中学.md "wikilink")。德州一中在升学率方面一般处在德州市前列。

## 其他

[缩略图](https://zh.wikipedia.org/wiki/File:一中火腿.jpg "fig:缩略图")
**德州一中食品厂**为德州一中的校办企业，主要生产[火腿](../Page/火腿.md "wikilink")，火腿商标名称为“[一中火腿](../Page/一中火腿.md "wikilink")”，并在学校附近设有专卖店。

## 参看

  - [德州市](../Page/德州市.md "wikilink")

## 外部链接

  - [德州一中网站](http://www.dzyz.cn)

[Category:山东中等学校](../Category/山东中等学校.md "wikilink")
[Category:德州学校](../Category/德州学校.md "wikilink")
[Category:德城区](../Category/德城区.md "wikilink")
[Category:1928年中國建立](../Category/1928年中國建立.md "wikilink")
[Category:1928年創建的教育機構](../Category/1928年創建的教育機構.md "wikilink")