**人類生殖科技管理局**（）是一個[香港](../Page/香港.md "wikilink")[法定組織](../Page/法定組織.md "wikilink")，於2001年4月根據《[人類生殖科技條例](../Page/人類生殖科技條例.md "wikilink")》（第561章）第4條設立，目的是規管[生殖科技程序](../Page/生殖.md "wikilink")，提供審批進行人類生殖科技活動的牌照申請，以及向有關人士發出業務守則。人類生殖科技管理局的現任主席是[吳馬太](../Page/吳馬太.md "wikilink")[醫生](../Page/醫生.md "wikilink")。

人類生殖科技管理局主席:

  - [梁智鴻](../Page/梁智鴻.md "wikilink")(2001年-2010年)
  - [梁永立](../Page/梁永立.md "wikilink")(2010年-2016年)
  - [吳馬太](../Page/吳馬太.md "wikilink")(2016年-)

## 參考

  - [人類生殖科技管理局任命 2018-04-06](http://www.info.gov.hk/gia/general/201804/06/P2018040600302.htm)
  - [雪藏胚胎代母掀道德爭議](http://www.mingpaotor.com/htm/News/20131021/HK-gfn2_er.htm?m=0)
  - [生殖科技局設小組跟進](https://web.archive.org/web/20140512230959/http://news.sina.com.hk/news/20101202/-2-1937451/1.html?rtext)
  - [訂做胚胎之迷思](http://www.mingpaohealth.com/cfm/Archive1.cfm%3FFile%3D20130909/pafe/yca1h.txt+&cd=55&hl=zh-CN&ct=clnk&gl=cn)