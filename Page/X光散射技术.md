[X-ray_diffraction_pattern_3clpro.jpg](https://zh.wikipedia.org/wiki/File:X-ray_diffraction_pattern_3clpro.jpg "fig:X-ray_diffraction_pattern_3clpro.jpg")而形成。\]\]
**X光散射技术**或**X射线衍射技术**（）是一系列常用的非破壞性分析技術，可用於揭示物質的[晶體結構](../Page/晶體.md "wikilink")、化學組成以及物理性質。这些技术都是以观测[X射线穿过样品后的](../Page/X射线.md "wikilink")[散射](../Page/散射.md "wikilink")[强度为基础](../Page/强度.md "wikilink")，并根据散射角度、极化度和入射X光[波长对实验结果进行分析](../Page/波长.md "wikilink")。X光散射技术可在許多不同的條件下進行分析，例如不同的[溫度或](../Page/溫度.md "wikilink")[壓力](../Page/壓力.md "wikilink")。

## X光繞射技术

X光繞射（X-ray
diffraction）技术可以用于研究[分子的](../Page/分子.md "wikilink")[构象或形态](../Page/构象.md "wikilink")。X光繞射技术是基于X光在穿过[长程有序物质所发生的](../Page/晶体.md "wikilink")[弹性散射](../Page/弹性碰撞.md "wikilink")。“[繞射动力学理论](../Page/繞射动力学理论.md "wikilink")”对晶体的散射现象给出了更为复杂的描述\[1\]。以下列出的是X光繞射的相关技术：

  - [单晶X射线繞射](../Page/X射线晶体学.md "wikilink")：用于解析晶态物质中分子的整体结构，研究范围可以从小的[无机小分子到复杂的](../Page/无机.md "wikilink")[大分子](../Page/大分子.md "wikilink")，如[蛋白质](../Page/蛋白质.md "wikilink")；可用单色性X光（德拜法）或连续波长X光（即“劳厄法”）进行研究。
  - [粉末衍射](../Page/粉末衍射.md "wikilink")：也是一种获得晶体（[微晶](../Page/微晶.md "wikilink")）结构的方法，所用样品为多晶态或粉末固态晶体。粉末繞射通常用于鉴定未知物质，主要通过将衍射数据与[繞射数据国际中心](../Page/繞射数据国际中心.md "wikilink")（International
    Centre for Diffraction
    Data，ICDD）中的繞射数据库进行比较。这一技术或可用于鉴定非均一态的固体混合物，确定其中含量相对丰富的晶态物质；而且，当与网格修正技术（如Rietveld修正）连用时，还可以提供未知物质的结构信息。粉末繞射也是确定晶态物质[晶系的常用方法](../Page/晶系.md "wikilink")，并可用于测定晶体颗粒的大小。
  - 薄膜繞射。
  - X射线[极图分析](../Page/极图.md "wikilink")：用于分析和测定晶态薄膜样品中晶态方位。
  - X射线回摆曲线分析法：用于定量测量晶态物质的粒度大小和镶嵌度散布。

## 散射技术

### 弹性散射

即使是非晶态物质（非长程有序），也可能可以用依赖于单色性X光的弹性散射的方法来研究：

  - [小角X射线散射](../Page/小角X射线散射.md "wikilink")：在散射角2θ接近0°时，对样品的X射线散射强度进行测量，以获取[纳米到](../Page/纳米.md "wikilink")[微米量级上的分子结构信息](../Page/微米.md "wikilink")。\[2\]
  - [X射线反射率](../Page/X射线反射率.md "wikilink")：用于分析和测定单层或多层薄膜的厚度、粗糙度和密度。\[3\]
  - [广角X射线散射](../Page/广角X射线散射.md "wikilink")：测量散射角2θ大于5°。

### 非弹性散射

当[非弹性散射的X射线的能量和角度被监测时](../Page/非弹性碰撞.md "wikilink")，相关的散射技术就可以用于探测物质的[能带结构](../Page/能带结构.md "wikilink")：

  - [康普頓散射](../Page/康普頓散射.md "wikilink")。
  - [共振非弹性X射线散射](../Page/共振非弹性X射线散射.md "wikilink")。
  - [X射线拉曼散射](../Page/X射线拉曼散射.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

## 參見

  - [X射线](../Page/X射线.md "wikilink")
  - [X射线晶體學](../Page/X射线晶體學.md "wikilink")
  - [结构测定](../Page/结构测定.md "wikilink")

## 外部連結

  - [国际晶体学联合会（International Union of
    Crystallography）](https://web.archive.org/web/20070518023635/http://www.iucr.ac.uk/)
  - [X光繞射分析儀](https://web.archive.org/web/20071017025056/http://elearning.stut.edu.tw/caster/3/no4/4-1.htm)
  - [X光繞射](https://web.archive.org/web/20070717065614/http://www.phys.nthu.edu.tw/~thlu/x-ray/2-3.htm)

[Category:晶體學](../Category/晶體學.md "wikilink")
[Category:光譜學](../Category/光譜學.md "wikilink")
[Category:放射線學](../Category/放射線學.md "wikilink")
[Category:繞射](../Category/繞射.md "wikilink")
[Category:散射](../Category/散射.md "wikilink")

1.
2.
3.   Holy, V. et al. Phys. Rev. B. **47**, 15896 (1993).