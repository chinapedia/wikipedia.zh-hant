**第谷环形山**（Tycho）是[月球正面南半部一座醒目的大](../Page/月球正面.md "wikilink")[撞击坑](../Page/撞击坑.md "wikilink")，约形成于[哥白尼纪](../Page/哥白尼纪.md "wikilink")\[1\]，其名称取自16世纪[丹麦](../Page/丹麦.md "wikilink")[天文学家暨](../Page/天文学家.md "wikilink")[炼金术士](../Page/炼金术.md "wikilink")[第谷·布拉赫](../Page/第谷·布拉赫.md "wikilink")（1546年-1601年），它是[月球上最有趣的](../Page/月球.md "wikilink")[陨石坑之一](../Page/月球陨石坑.md "wikilink")：环绕着一圈最明亮的[射纹系统](../Page/射纹系统.md "wikilink")，特别在[满月时清晰可见](../Page/满月.md "wikilink")，即便在仅有[地球反射光的背景下也可被分辨出](../Page/行星照.md "wikilink")。

## 年龄与特征

[LRO_Tycho_crater_central_peak_summit.jpg](https://zh.wikipedia.org/wiki/File:LRO_Tycho_crater_central_peak_summit.jpg "fig:LRO_Tycho_crater_central_peak_summit.jpg")拍摄。\]\]
[第谷环形山.jpg](https://zh.wikipedia.org/wiki/File:第谷环形山.jpg "fig:第谷环形山.jpg")[辉夜姬号拍摄的第谷环形山坑内](../Page/月亮女神_\(航天器\).md "wikilink")\]\]
[Tycho_Crater.jpg](https://zh.wikipedia.org/wiki/File:Tycho_Crater.jpg "fig:Tycho_Crater.jpg")。\]\]
[LRO_Tycho_Central_Peak_0.25.jpg](https://zh.wikipedia.org/wiki/File:LRO_Tycho_Central_Peak_0.25.jpg "fig:LRO_Tycho_Central_Peak_0.25.jpg")拍摄。\]\]
根据分析[阿波罗16号任务期间所采集的第谷环形山射纹样本](../Page/阿波罗16号.md "wikilink")，估计该陨坑约形成于1.09亿年前±400万年\[2\]的[哥白尼纪](../Page/哥白尼纪.md "wikilink")\[3\]，是一座相对年轻的陨石坑。这一地质年龄表明，形成该陨坑的撞击体可能是来自一颗“巴普提斯蒂娜族”的[小行星](../Page/小行星.md "wikilink")，但撞击体的构成仍是未知，因此，这只是目前的一种推测。然而，模拟研究的结果显示，形成第谷环形山的撞击体约有70%的可能性是源自形成了[298
巴普提斯蒂娜的同一天体](../Page/小行星298.md "wikilink")（一颗直径170公里，在1.6亿年前与另一更小天体相撞而解体的小天体）的碎片\[4\]。据信该小行星族中的另一块更大的碎片在6500万年前撞击了[地球](../Page/地球.md "wikilink")，形成了[希克苏鲁伯陨石坑和](../Page/希克苏鲁伯陨石坑.md "wikilink")[恐龙的灭绝](../Page/白垩纪－第三纪灭绝事件.md "wikilink")，但2011年[广域红外线巡天探测卫星获得的新数据推翻了这一假说](../Page/广域红外线巡天探测卫星.md "wikilink")（原天体撞击解体的时间由1.6亿年前修改为8000万年前）。

第谷环形山周边密布着众多大小不同的撞击坑，很多重叠在更古老的陨坑之上，其中一些较小的为第谷环形山形成时所溅射的大块[喷出物撞击出的](../Page/喷发.md "wikilink")[次生坑](../Page/次级撞击坑.md "wikilink")。它的西侧靠近古老的[威廉环形山](../Page/威廉环形山.md "wikilink")、西北毗邻[海因修斯环形山](../Page/海因修斯环形山.md "wikilink")、已磨损的[萨瑟里德斯环形山和](../Page/萨瑟里德斯环形山.md "wikilink")[奥龙斯环形山分别位于它的东北偏北和东北](../Page/奥龙斯环形山.md "wikilink")，沿它的东面、南面及西南分别坐落了较小的[皮克泰环形山](../Page/皮克泰环形山.md "wikilink")、[斯特里特环形山和](../Page/斯特里特环形山.md "wikilink")[布朗陨石坑](../Page/布朗陨石坑.md "wikilink")，而北面则濒临[云海](../Page/云海.md "wikilink")\[5\]。该陨坑中心[月面坐标为](../Page/月面坐标.md "wikilink")，直径85.29公里，表面深约2.81公里\[6\]。

第谷环形山是月球上一座最年轻的大撞击坑，保存状况极好，不像那些已磨损退化的古老陨坑，它没有受到后续[撞击的侵蚀](../Page/撞击事件.md "wikilink")，边缘范围较为清晰，环陨坑分布着一圈由独特的[射纹系统构成的绵长辐射纹](../Page/射纹系统.md "wikilink")，当太阳直射时，它高[反照率的坑底显得格外突出](../Page/反照率.md "wikilink")。第谷环形山的内侧壁已坍塌并明显呈阶坡状结构，坑壁最大高出周边地形1390米\[7\]，其中壁顶与坑底平均落差4.6公里\[8\]（最高点超过5公里\[9\]
），内部容积约6869.92公里<sup>3</sup>\[10\]。粗糙但大体平坦的坑底上散布有许多小穹丘，地表显示过去曾有过[火山活动迹象](../Page/火山活动.md "wikilink")，极可能为撞击融化的岩石。清晰的坑底照片展示了地面覆盖着一系列纵横交叉的裂缝和小山丘，坑内坐落了一座由含80-85%[斜长石的辉长](../Page/斜长石.md "wikilink")-苏长-橄长斜长岩（GNTA2）、斜长辉长岩（AG）和斜长辉长苏长岩（AGN）及[辉长岩](../Page/辉长岩.md "wikilink")（G）\[11\]构成的高约2.26公里的大中央峰，在它的东北还矗立有一座较小的山峰。该陨坑坑底非常明亮，但坑外环绕了一圈60公里宽的暗环，也许这圈暗淡的区域可能覆盖着撞击时凿掘出的深层矿物，更外围是明亮的地表区-光轮，进入覆盖了100多座撞击坑的射纹区。根据目前最普遍的看法，第谷环形山射纹束最长向外延伸达4000公里（约为3/4月球子午线长度），将[云海一分为二](../Page/云海.md "wikilink")，但这些射纹线的起点并非始于第谷坑本身，而是距离它较远（位于云海边缘），且不排除其起源与[门纳劳斯陨石坑有关联](../Page/门纳劳斯陨石坑.md "wikilink")\[12\]\[13\]。

第谷环形山已被月球和行星观测协会（ALPO）列入《带有明亮射纹系统的撞击坑列表》\[14\]。

第谷环形山是一座典型的，具有代表性的陨石坑，其特点是直径超过50公里、拥有一座大中央峰、内侧壁明显呈阶坡结构以及相对平坦的坑底。该类撞击坑被归类为"[TYC
型](../Page/月球陨石坑#陨石坑分类.md "wikilink")"（以它的名称所命名）。

[月食期间对月球表面的](../Page/月食.md "wikilink")[红外观测显示](../Page/红外线.md "wikilink")，第谷环形山坑底表面冷却速度比周围其它地区要稍慢，使它成为了月表上的一处“热点”，该现象主要缘于陨坑所覆盖的物质不同而形成的差异。

## 研究和命名

[Lage_des_Mondkraters_Tycho.jpg](https://zh.wikipedia.org/wiki/File:Lage_des_Mondkraters_Tycho.jpg "fig:Lage_des_Mondkraters_Tycho.jpg")看到的第谷环形山的位置\]\]
第谷陨石坑及其周围的[射纹系统早在十七世纪就被绘入月图](../Page/射纹系统.md "wikilink")，先后出现在1629年-1930年[意大利](../Page/意大利.md "wikilink")[天文学家弗朗切斯科](../Page/天文学家.md "wikilink")·丰塔纳（Francesco
Fontana）和1645年[捷克](../Page/捷克.md "wikilink")[天文学家安东](../Page/天文学家.md "wikilink")·玛丽亚·谢尔乌斯·德·里伊塔（Anton
Maria Schyrleus de Rheita）绘制的月面图中\[15\]
。早期的一些月图绘制者也赋予了该陨坑不同的名字，如[皮埃尔·伽桑狄在](../Page/皮埃尔·伽桑狄.md "wikilink")1636年发表的月面图中称它为"月球之[脐](../Page/肚脐.md "wikilink")"\[16\]；1645年[荷兰](../Page/荷兰.md "wikilink")[天文学家及制图师](../Page/天文学家.md "wikilink")[米迦勒·弗洛伦特·范·朗伦以](../Page/米迦勒·弗洛伦特·范·朗伦.md "wikilink")[波兰君主](../Page/波兰君主列表.md "wikilink")[瓦迪斯瓦夫四世·瓦萨之名称之为](../Page/瓦迪斯瓦夫四世.md "wikilink")"瓦迪斯瓦夫四世"（Vladislai
IV）\[17\]\[18\]；而[波兰](../Page/波兰.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")[约翰·赫维留在](../Page/约翰·赫维留.md "wikilink")1647年则将它命名为"[西奈山](../Page/西奈山.md "wikilink")"（Mons
Sinai）\[19\]。

1651年，[意大利](../Page/意大利.md "wikilink")[耶稣会](../Page/耶稣会.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")，现代月球表面特征命名体系创始人[乔万尼·巴蒂斯塔·里乔利以](../Page/乔万尼·巴蒂斯塔·里乔利.md "wikilink")[丹麦](../Page/丹麦.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")"[第谷·布拉赫](../Page/第谷·布拉赫.md "wikilink")"之名命名了它\[20\]，并一直被沿用至今，1935年[国际天文学联合会对该命名正式予以接受](../Page/国际天文学联合会.md "wikilink")\[21\]
。此外[火星上直径](../Page/火星.md "wikilink")105公里[第谷·布拉赫撞击坑也是以他的名字所命名](../Page/第谷·布拉赫撞击坑.md "wikilink")\[22\]。

## 探测器着陆点

[Tycho_Crater_Panorama.jpg](https://zh.wikipedia.org/wiki/File:Tycho_Crater_Panorama.jpg "fig:Tycho_Crater_Panorama.jpg")拍攝的月表全景图，着陆点距第谷环形山北侧坑壁29公里。\]\]
[美国太空探测器](../Page/美国.md "wikilink")[勘测者7号曾将该陨坑的边缘区列为主要的探测目标](../Page/勘测者7号.md "wikilink")，1968年1月该太空探测器成功降落在第谷环形山北部边缘附近，并传回了该陨坑周边[月壤的化学成分数据及照片](../Page/月壤.md "wikilink")。发现了与[月海不同的化学成分](../Page/月海.md "wikilink")。从中了解到高地主要矿物成分为[斜长石](../Page/斜长石.md "wikilink")—一种富含[铝的矿岩](../Page/铝.md "wikilink")。[月球轨道器5号也对该陨坑进行了很详细的拍摄](../Page/月球轨道器5号.md "wikilink")。

1972年，在距第谷环形山2250公里处（[澄海边缘](../Page/澄海.md "wikilink")），[阿波罗17号](../Page/阿波罗17号.md "wikilink")[宇航员采集了来自该陨坑射纹中](../Page/宇航员.md "wikilink")，可帮助确定其地质龄的岩石样本\[23\]。

从上世纪50年代到90年代，[美国宇航局空气动力学家迪安](../Page/美国宇航局.md "wikilink")·查普曼和其他人进一步提出了[似黑曜岩月球起源说](../Page/玻璃陨石.md "wikilink")。查普曼使用复杂的电脑轨道模型和大量的风洞实验来支持他所谓"澳大利亚粗糙带鳞状的似曜石来自于第谷环形山的罗斯溅射纹"(Rosse
ejecta ray)的理论，除非能取得罗斯射纹样本，否则目前尚无法排除似曜石起源于月球的说法。

## 月球瞬变现象

[月食期间曾观察到该陨坑暗淡的轮廓外观发生了变化](../Page/月食.md "wikilink")，并在[地球阴影的笼罩下闪烁亮光的](../Page/地球.md "wikilink")[瞬变现象](../Page/月球瞬变现象.md "wikilink")。

## 通俗作品引用

  - 在[亚瑟·查理斯·克拉克的](../Page/亚瑟·查理斯·克拉克.md "wikilink")[2001太空漫游小说和](../Page/2001太空漫游_\(小说\).md "wikilink")[同名电影中](../Page/2001太空漫游_\(电影\).md "wikilink")，"第谷磁异常一号"（TMA-1）就位于第谷环形山中；
  - 电影《[星际旅行VIII：第一次接触](../Page/星际旅行VIII：第一次接触.md "wikilink")》中，24世纪月球城-第谷城就位于第谷环形山；
  - 电影《[黑衣人](../Page/黑衣人.md "wikilink")》中，[汤米·李·琼斯扮演的探员凯提到](../Page/汤米·李·琼斯.md "wikilink")[外星人违反了第谷条约第](../Page/外星人.md "wikilink")4153条规定；
  - 1961年克利福德·唐纳德·西马克创作了神奇故事《第谷坑的麻烦》，俄语版译名为《危险的第谷环形山》；
  - 在[前苏联马丁诺夫](../Page/前苏联.md "wikilink")·格奥尔基·谢尔盖耶维奇创作的科幻小说-《吉尼亚》（Gianeya）中，第谷环形山中有一座外星人的秘密太空基地；
  - 艾德蒙·汉弥顿创作的系列小说中，第谷环形山是超级英雄-未来船长克提斯·牛顿在月球上的基地。

## 卫星陨石坑

按惯例，最靠近第谷环形山的卫星坑将在月图上以字母标注在它的中心点旁边.
[Tycho_sattelite_craters_map.jpg](https://zh.wikipedia.org/wiki/File:Tycho_sattelite_craters_map.jpg "fig:Tycho_sattelite_craters_map.jpg")

| 第谷 | 緯度      | 經度      | 直徑   |
| -- | ------- | ------- | ---- |
| A  | 39.9° S | 12.0° W | 31公里 |
| B  | 43.9° S | 13.9° W | 13公里 |
| C  | 44.3° S | 13.7° W | 7公里  |
| D  | 45.6° S | 14.0° W | 27公里 |
| E  | 42.2° S | 13.5° W | 14公里 |
| F  | 40.9° S | 13.1° W | 16公里 |
| H  | 45.2° S | 15.8° W | 8公里  |
| J  | 42.5° S | 15.3° W | 11公里 |
| K  | 45.1° S | 14.3° W | 6公里  |
| P  | 45.3° S | 13.0° W | 8公里  |
| Q  | 42.5° S | 15.9° W | 21公里 |
| R  | 41.8° S | 13.6° W | 5公里  |
| S  | 43.4° S | 16.1° W | 3公里  |
| T  | 41.2° S | 12.5° W | 14公里 |
| U  | 41.0° S | 13.8° W | 19公里 |
| V  | 41.7° S | 15.3° W | 4公里  |
| W  | 43.2° S | 15.3° W | 19公里 |
| X  | 43.8° S | 15.2° W | 13公里 |
| Y  | 44.1° S | 15.8° W | 19公里 |
| Z  | 43.1° S | 16.2° W | 24公里 |

## 图集

Image:Lunar2007
eclipse-LiamG.jpg|2007年3月的[月全食](../Page/月全食.md "wikilink")，地球移动的阴影帶出了月表的细节，第谷环形山巨大的[射紋系统成为月球南半球一处醒目的特征](../Page/射紋系统.md "wikilink")。
<File:Tycho> crater 4119
h2.jpg|1967年[月球轨道器4号拍摄的图像](../Page/月球轨道器4号.md "wikilink")
<File:Tycho> crater floor 5125
h2.jpg|[月球轨道器5号拍摄的东北坑底](../Page/月球轨道器5号.md "wikilink")，显示了撞击熔化表面不规则的裂纹，光照源来自右下方。
<File:Tycho>
crater.png|thumb|位于月球正面南部，以16世纪丹麦天文学家暨炼金术士第谷·布拉赫命名的直径85公里的撞击陨石坑。
<File:LRO> Tycho crater central peak summit
boulder.jpg|中央峰峰顶上一块120米大的巨岩，[月球勘测轨道飞行器拍摄](../Page/月球勘测轨道飞行器.md "wikilink")。
<File:LRO> Polygonal fractures on Tycho ejecta
deposits.jpg|第谷环形山外龟裂的熔融表面.

## 相关条目

  - 小行星[1677 第谷·布拉赫](../Page/小行星1677.md "wikilink")
  - 火星上的[第谷·布拉赫撞击坑](../Page/第谷·布拉赫撞击坑.md "wikilink")
  - 明亮的超新星[第谷超新星](../Page/SN_1572.md "wikilink")

## 参考文献

ˌ

## 另请参阅

## 外部链接

  - [月球数码摄影图集.](http://www.lpi.usra.edu/resources/lunar_orbiter/bin/srch_nam.shtml?Tycho%7C0)
  - [阿波罗10、14号拍摄的第谷环形山照片.](http://www.lpi.usra.edu/resources/apollo/search/feature/?feature=Tycho)
  - 地图：[陨石坑](http://planetarynames.wr.usgs.gov/images/Lunar/lac_112_wac.pdf)及[西侧相邻区](http://planetarynames.wr.usgs.gov/images/Lunar/lac_111_wac.pdf)
  - [第谷环形山周边月面图.](http://www.lpi.usra.edu/resources/mapcatalog/usgs/I713/)
  - [壮观的第谷中央峰\!](http://lroc.sese.asu.edu/posts/384)
  - [第谷环形山内混沌的坑底.](http://lroc.sese.asu.edu/posts/162)
  - [第谷环形山喷发沉积物上不规则裂缝.](http://lroc.sese.asu.edu/posts/159)
  - [第谷坑内的喷出物.](http://lroc.sese.asu.edu/posts/253)
  - [第谷环形山坑底撞击熔融特征.](http://lroc.sese.asu.edu/posts/201)
  - [变形图.](https://lpod.wikispaces.com/July+17%2C+2008)
  - [轰然倒塌的坑壁.](https://lpod.wikispaces.com/July+18%2C+2008)
  - [撞击倾角.](https://lpod.wikispaces.com/August+29%2C+2008)
  - [什么？更多的第谷坑?](https://lpod.wikispaces.com/August+30%2C+2008)
  - [磨蚀与犀牛皮.](https://lpod.wikispaces.com/May+10%2C+2009)
  - [14英寸镜头中的第谷坑.](http://lpod.wikispaces.com/July+31%2C+2013)
  - [维基月球在线解说-第谷环形山.](http://the-moon.wikispaces.com/Tycho)
  - [Andersson, L.E., and E.A. Whitaker,美国宇航局月球地名目录,
    美国宇航局参考出版物1097, 1982年10月.](http://planet4589.org/astro/lunar/RP-1097.pdf)

[T](../Category/月球環形山.md "wikilink")

1.  The geologic history of the Moon, 1987, [Wilhelms, Don
    E.](../Page/Donald_Wilhelms.md "wikilink"); with sections by
    McCauley, John F.; Trask, Newell J.
    [USGS](../Page/United_States_Geological_Survey.md "wikilink")
    Professional Paper: 1348. Plate 11: Copernican System
    （[online](http://pubs.er.usgs.gov/publication/pp1348)）

2.

3.  Lunar Impact Crater Database

4.

5.  [Crater Tycho on the
    mapLAC-112.](http://planetarynames.wr.usgs.gov/images/Lunar/lac_112_wac.pdf)

6.  Lunar Impact Crater Database

7.
8.

9.

10.
11. Stefanie Tompkins and Carle M. Pieters （1999） Mineralogy of the
    lunar crust: Results from Clementine Meteoritics & Planetary
    Science, vol. 34, pp. 25-41 .

12.

13.

14. [List of craters with a bright system of rays of the Association of
    Lunar and Planetary Astronomy
    （ALPO）](http://www.zone-vx.com/alpo-rays-table.pdf)

15.

16.
17. [Map of the Moon, Compiled by Michael van
    Langren（1645）](../Page/:commons:File:Langrenus_map_of_the_Moon_1645.jpg.md "wikilink")

18.
19. [Hevelius map of the Moon
    （1647）](../Page/:commons:File:Hevelius_Map_of_the_Moon_1647.jpg.md "wikilink")

20. [Map of the Moon, compiled by Giovanni
    Riccioli（1651）](../Page/:commons:File:Riccioli1651MoonMap.jpg.md "wikilink")

21.

22. According to [Directory of the International Astronomical
    Union](http://planetarynames.wr.usgs.gov) by 2014.

23.