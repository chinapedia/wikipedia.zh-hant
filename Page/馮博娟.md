**馮博娟**（，）是[香港的](../Page/香港.md "wikilink")[流行曲](../Page/流行曲.md "wikilink")[歌手](../Page/歌手.md "wikilink")。

## 出道發展

馮博娟於[模特兒界出身](../Page/模特兒.md "wikilink")，後於1994年被發掘為歌手，同年飛圖唱片發掘的歌手有[趙學而](../Page/趙學而.md "wikilink")。不久[飛圖唱片替她推出專輯](../Page/飛圖唱片.md "wikilink")《愛得太天真》，以《不是擺花街》為第一主打，此曲改編同公司的[梁雁翎的](../Page/梁雁翎.md "wikilink")《好聚好散》、其後以同名歌曲《愛得太天真》作第二主打，但因歌曲未得到注意而未曾得獎。同年灌錄國語單曲《讓我此刻不平凡》及《誰的眼淚在飛》（原唱：[孟庭葦](../Page/孟庭葦.md "wikilink")，[上華時期](../Page/上華.md "wikilink")）後與飛圖唱片解約，不知去向。\[1\]

同時，她是飛圖唱片首批與[無綫電視簽約的歌手之一](../Page/無綫電視.md "wikilink")。

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/愛得太天真.md" title="wikilink">愛得太天真</a></p></td>
<td style="text-align: left;"><p>粵語 / 國語</p></td>
<td style="text-align: left;"><p><a href="../Page/飛圖唱片.md" title="wikilink">飛圖唱片</a></p></td>
<td style="text-align: left;"><p>1994年3月11日</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 合輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p><a href="../Page/飛圖超白金精選7.md" title="wikilink">飛圖超白金精選7</a></p></td>
<td style="text-align: left;"><p>粵語 / 國語新曲 + 精選</p></td>
<td style="text-align: left;"><p><a href="../Page/飛圖唱片.md" title="wikilink">飛圖唱片</a></p></td>
<td style="text-align: left;"><p>1993年12月23日</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/飛圖超白金精選8.md" title="wikilink">飛圖超白金精選8</a></p></td>
<td style="text-align: left;"><p>粵語 / 國語新曲 + 精選</p></td>
<td style="text-align: left;"><p><a href="../Page/飛圖唱片.md" title="wikilink">飛圖唱片</a></p></td>
<td style="text-align: left;"><p>1994年5月31日</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

## 參考

[po](../Category/馮姓.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:前英皇藝人](../Category/前英皇藝人.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")

1.  [1](http://blog.sina.com.cn/s/blog_4540d7e30102v6d5.html)