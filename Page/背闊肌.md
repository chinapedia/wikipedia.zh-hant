**背闊肌**（latissimus
dorsi），是將手臂向下和下後拉的背部肌肉。這兩塊[三角肌各有一闊端附著於脊椎和骨盆帶上](../Page/三角肌.md "wikilink")，較窄的一端則在肩下延伸到肱骨。其[拉丁語](../Page/拉丁語.md "wikilink")（及[英語](../Page/英語.md "wikilink")）名稱「*latissimus
dorsi*」意為「背部之最寬闊者」。背闊肌可支撐舉過頭部的手臂，並將手臂從抬高的位置向下移動。如果你手臂用力向側面伸開，就會感到背闊肌的繃緊。

## 來源

  - 《人體學習百科》，貓頭鷹出版社，ISBN 957-9684-11-1

## 外部連結

  - [解剖學(Anatomy)-肌肉(muscle)-Latissimus Dorsi
    Muscle(背闊肌)](http://smallcollation.blogspot.com/2013/01/anatomy-muscle-latissimus-dorsi-muscle.html)

  -
  - \- "Superficial layer of the extrinsic muscles of the back."

  -

[Category:上肢肌肉](../Category/上肢肌肉.md "wikilink")
[Category:肩部收肌](../Category/肩部收肌.md "wikilink")
[Category:肩部伸肌](../Category/肩部伸肌.md "wikilink")
[Category:肩部內旋肌](../Category/肩部內旋肌.md "wikilink")
[Category:肩部屈肌](../Category/肩部屈肌.md "wikilink")
[Category:脊伸肌](../Category/脊伸肌.md "wikilink")
[Category:脊侧屈肌](../Category/脊侧屈肌.md "wikilink")