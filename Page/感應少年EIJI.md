《**感應少年EIJI**》（）是1996年至2000年於[週刊少年Magazine連載的](../Page/週刊少年Magazine.md "wikilink")[日本漫畫](../Page/日本漫畫.md "wikilink")。[安童夕馬原作](../Page/安童夕馬.md "wikilink")、[朝基勝士作畫](../Page/朝基勝士.md "wikilink")。於1997年和1999年曾兩度在[日本電視台系列播出由](../Page/日本電視台.md "wikilink")[松岡昌宏主演的](../Page/松岡昌宏.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")。

2010年10月，安童在Twitter上表示自己最近開始執筆感應少年第二部，隔年在2011年4月25日發售的『[週刊Young
Magazine](../Page/週刊Young_Magazine.md "wikilink")』21・22合併號上開始了距離約有10年的新連載。台灣由[東立出版社代理](../Page/東立出版社.md "wikilink")，命名為《**感應少年EIJI
新章**》出版。

## 登場人物

### 主要人物

  -
    主角，擁有感應能力的高中二年級學生。和**志摩**在「殺人鬼[梅比斯](../Page/莫比烏斯帶.md "wikilink")」事件當中認識，偶爾會幫忙事件的搜查。是個不良少年，學校成績不好，也沒什麼朋友。曾經用感應能力獲得高成績。很重視友情，正義感強烈。總是用感應能力先一步讀取出敵人的攻擊，打架超強。
    曾經和友人**江川徹**單獨兩人殲滅整個不良少年集團，面對受過專業戰鬥訓練的人也毫不遜色。意外地頭腦很不錯，能從志摩的說明中總結出結論，並用手法騙過**澤木**。一開始感應能力只能通過手來施展，但後來發展到可以通過全身來施展。另外，也有過在感應到對方的神經性創傷及惡意而無意識地施展出感應能力（也就是說，對無惡意的人則不能施展）。感應能力重複施展的話能力會增強，但映兒事後會陷入昏睡失控的狀態。
    在學校很受女生歡迎，但實際上一次也沒和女孩交往過。中學時因為無法控制自己的感應能力，腦中無時無刻都會浮現出每一個人醜陋又真實的內心，讓他過著墮落、頹廢的日子，但是後來遇見**赤樹**刑警，學會控制之後便收斂了許多。
    和**葛西裕介**兩人有在玩[樂團](../Page/樂團.md "wikilink")，他擔任[貝斯手及主唱](../Page/貝斯手.md "wikilink")。駕駛的[摩托車為](../Page/摩托車.md "wikilink")[KAWASAKI的](../Page/KAWASAKI.md "wikilink")[zephyr1100RS](../Page/KAWASAKI#電單車.md "wikilink")。
    電視劇第二部中變成了以[樂團樂手為目標的自由人](../Page/音樂家.md "wikilink")，住在[Livehouse中靠打工維生](../Page/音樂展演空間.md "wikilink")。電視劇中只能用左手施展感應能力，第二部中添加了聲音感應能力，在漫畫版中也沿用了這個設定。
    在新章中，映兒未能順利畢業，留級一年，目前是高中三年級。同時也是在和江川徹、葛西裕介、田宮章吉這些固定主角班底中，唯二的高中延畢生(另一位是田宮章吉)。

<!-- end list -->

  -
    警視廳搜查一課的刑事，職位是警部補。25歲的巨乳美人，東都大學畢業，畢業主題是研究感應能力在搜查上的應用，擅長[側寫](../Page/罪犯側寫.md "wikilink")。調查案件不喜照本宣科，而是按照自己的感覺、依靠頭腦和雙腳去偵察。
    私生活一團糟，但因為祖母經常會到家裡來玩，所以房子打掃的很乾淨。但是料理很糟糕，冰箱裡面只有啤酒。
    和**映兒**雖然互相鬥氣，但似乎互相對對方也有著好感。
    在第一部的最後，升職成為[警部](../Page/警部.md "wikilink")。

### 映兒的友人或家族

****

  -
    高中二年級，**映兒**小學時認識的童年玩伴，知道映兒有感應能力，少數讓映兒敞開心胸的人。頭腦很好，是成績排名全學年第一的好學生，對映兒來說，是很重要、無法取代的朋友。希望成為心理學者，和映兒兩人積極的協助志摩的搜查。有時候會提出與他個性完全不符的大膽主意。暗戀**惠美**。
    和映兒兩人有在玩樂團，擔任[鍵盤手及](../Page/鍵盤手.md "wikilink")[作曲](../Page/作曲.md "wikilink")。
    在新章裡，順利畢業，目前是東大一年級，擔任惠美的家庭教師。

****

  -
    **映兒**的同班同學。空手道的高手，長髮的帥哥。雖然和映兒同年級，但實際上已經留級一年，比映兒大一歲。初期只是個見到美女前髮就會觸角化蠕動的單純的朋友，從「殺人鬼JUSTICE」事件後升格成為第二主角。將涉谷的不良少年們集結成｢聯盟｣，擔任領導人。在漫畫原著中有不少故事是和葛西裕介、明日真映兒兩人同時登場，同時在新章裡，也是三人中第二位具有高中畢業學歷。(另一位是葛西裕介)新章推出時，此設定也引起漫畫迷討論：「阿徹是如何畢業的?」在網路上成為漫迷之間的趣談。
    政治家的私生子，有個同父異母的哥哥。在母親經營的義大利餐廳打工當服務生，意外的很有生活品味。追逐他的女孩子不少，自稱「涉谷最想擁抱的男人第一名」，但是他抱持有好感的女孩子，連續兩次被捲入事件中殺害，就這點來說，沒什麼戀愛運。
    因為有和幾島相處過的經驗，隱約察覺到映兒有感應能力，直到CANNABIS事件，才對映兒這麼說：「我能理解你不想說的心情」。之後，偶爾也會利用聯盟的情報網幫助**志摩**搜查。
    在新章裡，順利畢業後，自己向銀行貸款及朋友們投資的資金，開了一家咖啡館。
    於王牌至尊中也有出場。
    在電視劇中因為節約了演員預算，戲份皆由**田宮章吉**頂替。但漫迷們無法接受，不承認這個設定，並戲稱為「徹哥和章魚」合體，將此設定的更改視同黑歷史。

****

  -
    **映兒**的友人，是個性格單純的笨蛋。其實是名門之後，但因為素行不良被親人放棄。
    在新章裡，和映兒一樣，沒能畢業，留級一年，現在是高中三年級。

****

  -
    **映兒**的童年玩伴，將頭髮染成紅色並剃成平頭、斜長雙眼的少年。比起思考身體先行動的類型。傳說剛出生就一腳將產婆踢開。和母親一起經營蕎麥屋。之後對政治產生興趣，成為**江川徹**的父親牧原代議士的學生。
    王牌至尊的主角。

****

  -
    高中一年級，**映兒**的父親再婚對象的女兒，與映兒沒有血緣關係的妹妹，暗戀映兒，常和**志摩**爭風吃醋。完全沒發現到**裕介**的心意。
    由於爸爸在外地工作，媽媽經常跟去照顧他而不在家，所以家務都是由惠美在做。

****

  -
    **惠美**的同班同學，聲稱自己有通靈能力，擁有前世的記憶，曾經說過自己前世和**映兒**及惠美有關係。家中經營醫院，姐姐是醫師。曾經用通靈能力救了瀕死的**裕介**一命。

### 犯罪者的重要人物

****

  -
    **志摩**的大學同學，對志摩抱持些許好感。但是，其實是個為了自己的研究殺害多少人都面不改色的邪惡、冷酷的人。小時候放火燒死了自己的母親和養父，在「裝有定時裝置的蘋果」事件中，炸死想阻止他的親生父親：**若林十藏**教授。
    智商200以上的犯罪策劃者，擅長以心理學操縱他人，也很精通電子機器。通稱「APPLE」。一度被逮捕入獄，但之後順利逃獄，像是在嘲笑志摩和**映兒**一樣消失無蹤。最後，消失在夜晚的大海……

註：該角色戲份在1999年日劇版第二期中，被國際罪犯卡洛斯取代。

****

  -
    **江川徹**中學時認識的人。感應能力比**映兒**更強，不需要接觸就可以感應。曾經利用感應能力操縱涉谷的不良少年們，但是擁有強烈自我的阿徹並沒有被操縱，即使如此，他還是對幾島抱持著強烈的信賴。和阿徹混在一起的那段時間雖然不長，但給阿徹的生存方式造成很大影響。之後使用感應能力遊走在黑暗世界裡。
    非常照顧江川徹。

****

  -
    **澤木**主辦的演講的學生。之後成為他的得力助手。

****

  -
    和**映兒**們同年級的同學。膽小的天文部社員。映兒曾經一度從欺負他的學生們手中幫助過他。
    在衝動下殺害總是虐待自己的母親之後，為了自殺躲進體育館的地板下時發現了會引起特殊幻覺的蘑菇，利用這些蘑菇製成的餅乾將那些曾經欺負他的人洗腦，間接殺害了他們。最後在映兒的感應能力反動下，用刀子刺傷自己，被逮捕。

****

  -
    曾經是**志摩**新人時代的教育負責人，在某個事件中因為射殺了身為犯人的少年，從搜查一課辭職異動到少年課。雖然那之後就發誓不再用槍，但孫女**加藤舞**因為被強姦而自殺，正在保護管束中的五位少年毫無反省之意，更說「反正我們就是畜生」，忍不住使用準備用來自殺的托卡列夫手槍射殺少年。為了復仇而辭去警察一職，對志摩自稱是「復仇的恐怖分子」而再度持槍狙擊少年們。殺害最後一名少年時，正當和志摩對峙，遭到SAT一齊射擊死亡。其實他手中的槍已經沒有子彈。
    還在一課時，被稱為「搜查一課先生」的傳說中的人物。武術、射擊、搜查能力都非常優秀，過去曾獲得奧林匹克的銀牌，曾擔任海外教官。就算是不知道害怕的**映兒**也因為「到底是怎樣的怪物」從心底感到恐怖。

**CANNABIS**

  -
    世界級的犯罪者。長相漂亮的美青年，為了封印自己的凶暴性及殺人慾望，平常以美女的姿態出現。擁有常人所沒有的腕力，使用改造過後的手機上的鋼線輕易的擰斷人類的脖子殺人。
    本名是**宏明**，商人的兒子，一家搭乘直升機時遭遇意外墜毀，雙親被殺，和妹妹分離，被游擊隊養大。但是他並沒有忘記要復仇，18歲生日那天他將游擊隊全員殲滅。之後就住在波哥大的死人街成為犯罪請託人。但是，在殲滅游擊隊時，以火焰發射器燒死了同樣被游擊隊養大的妹妹，之後就對火焰抱持著異常的恐懼感。

****

  -
    **映兒**和**國光**的童年玩伴，煙火師父。祖父被希望開發城市的不動產公司害死之後，他便利用製造煙火的技術製作炸彈一一殺害那些人。從小就被祖父對於日本政治的憤怒影響，最終目的是用命名為「革命菊」的大型炸彈爆破國會議事堂，殺害所有的政治家。最後映兒與國光說服了他，自首後被逮捕。之後影響了國光立志成為政治家。

### 其他

  -
    擁有感應能力，是開導**映兒**控制感應能力的啟蒙老師。
    發現警界內部有著不為人知的重大計畫暗自展開調查，和**立花**、**志摩**及**澤木**四人曾是很要好的朋友。
    為了救受襲的映兒，死在**澤木晃**精心計畫的意外下。

<!-- end list -->

  -
    **志摩**的前男友，為了追查赤樹的死因以及內部的非法計畫，以詐死的方式展開調查。

<!-- end list -->

  -
    **赤樹宗一郎**的妹妹，**映兒**國中時代的朋友，喜歡映兒。偶像團體SQUARE DOLL的一員。
    哥哥去世後，為了討生活而進入演藝圈，卻在經紀公司的同意下，被偶像團體RUTE的日下部淳也強暴。

<!-- end list -->

  -
    **江川徹**聯盟中的一員，姊姊是SQUARE DOLL裡面的**清水晶子**。
    （但在日劇第一部中名字改為**赤樹貴志**，而**赤樹理惠**為其姊姊。）

<!-- end list -->

  -
    **志摩**的上司，搜查一課的警部。

<!-- end list -->

  -
    有變裝女性癖，多次以『**福島滿子**』的身分誤打誤撞破解許多命案。

<!-- end list -->

  -
    **江川徹**同父異母的哥哥。其實很想要有個弟弟，但是為了自己母親，強迫自己討厭阿徹。

## 感應能力

能夠藉由觸摸物體來讀取到殘留在上面的片段畫面和記憶， 越是強烈的外力和情感越能清楚浮現。

使用時伴隨著強烈的頭疼， 隨著讀取時間越長疼痛越加劇烈，直到使用者無法忍受放開為止。

若是使用的時間過長及讀取量過多，會有昏厥跟短暫失憶的反作用。

## 出版書籍

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p><a href="../Page/講談社.md" title="wikilink">講談社</a></p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>感應少年EIJI</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>1996年7月17日</p></td>
<td><p>ISBN 4-06-312295-6</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>1996年9月17日</p></td>
<td><p>ISBN 4-06-312317-0</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>1996年11月15日</p></td>
<td><p>ISBN 4-06-312339-1</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>1997年1月17日</p></td>
<td><p>ISBN 4-06-312366-9</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>1997年3月17日</p></td>
<td><p>ISBN 4-06-312384-7</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>1997年6月17日</p></td>
<td><p>ISBN 4-06-312420-7</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>1997年8月12日</p></td>
<td><p>ISBN 4-06-312442-8</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>1997年11月17日</p></td>
<td><p>ISBN 4-06-312476-2</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>1998年1月16日</p></td>
<td><p>ISBN 4-06-312499-1</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>1998年3月17日</p></td>
<td><p>ISBN 4-06-312521-1</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>1998年5月15日</p></td>
<td><p>ISBN 4-06-312544-0</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>1998年8月17日</p></td>
<td><p>ISBN 4-06-312579-3</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>1998年10月16日</p></td>
<td><p>ISBN 4-06-312604-8</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>1998年12月16日</p></td>
<td><p>ISBN 4-06-312630-7</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>1999年2月17日</p></td>
<td><p>ISBN 4-06-312654-4</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>1999年4月16日</p></td>
<td><p>ISBN 4-06-312675-7</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>1999年6月17日</p></td>
<td><p>ISBN 4-06-312699-4</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>1999年9月16日</p></td>
<td><p>ISBN 4-06-312725-7</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>1999年10月15日</p></td>
<td><p>ISBN 4-06-312745-1</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>1999年12月15日</p></td>
<td><p>ISBN 4-06-312783-4</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>2000年2月17日</p></td>
<td><p>ISBN 4-06-312801-6</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>2000年4月14日</p></td>
<td><p>ISBN 4-06-312823-7</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>2000年6月16日</p></td>
<td><p>ISBN 4-06-312847-4</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>2000年8月10日</p></td>
<td><p>ISBN 4-06-312869-5</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>2000年10月17日</p></td>
<td><p>ISBN 4-06-312889-X</p></td>
</tr>
<tr class="even">
<td><p>感應少年EIJI 新章</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>2011年10月6日</p></td>
<td><p>ISBN 978-4-06-382092-8</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>2011年11月4日</p></td>
<td><p>ISBN 978-4-06-382102-4</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>2012年2月6日</p></td>
<td><p>ISBN 978-4-06-382135-2</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>2012年4月6日</p></td>
<td><p>ISBN 978-4-06-382160-4</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>2012年7月6日</p></td>
<td><p>ISBN 978-4-06-382196-3</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2012年9月6日</p></td>
<td><p>ISBN 978-4-06-382216-8</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2013年3月6日</p></td>
<td><p>ISBN 978-4-06-382274-8</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2013年4月5日</p></td>
<td><p>ISBN 978-4-06-382283-0</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2013年5月2日</p></td>
<td><p>ISBN 978-4-06-382296-0</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2013年10月4日</p></td>
<td><p>ISBN 978-4-06-382360-8</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>2013年11月6日</p></td>
<td><p>ISBN 978-4-06-382371-4</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>2014年2月6日</p></td>
<td><p>ISBN 978-4-06-382426-1</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>2014年5月2日</p></td>
<td><p>ISBN 978-4-06-382462-9</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>2014年7月4日</p></td>
<td><p>ISBN 978-4-06-382486-5</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>2014年12月5日</p></td>
<td><p>ISBN 978-4-06-382526-8</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 電視劇

### 演員

#### 主要人物

  - 明日真映兒：[松岡昌宏](../Page/松岡昌宏.md "wikilink")（[TOKIO](../Page/TOKIO.md "wikilink")）
  - 志摩亮子：[大塚寧寧](../Page/大塚寧寧.md "wikilink")（第一部） →
    [工藤静香](../Page/工藤静香.md "wikilink")（第二部）
  - 田宮章吉：[井之原快彥](../Page/井之原快彥.md "wikilink")（[V6](../Page/V6.md "wikilink")）
  - 葛西裕介：[小原裕貴](../Page/小原裕貴.md "wikilink")
  - 明日真惠美：[松本惠](../Page/松本惠.md "wikilink")（第一部） →
    [後藤理沙](../Page/後藤理沙.md "wikilink")（第二部） →
    [酒井彩名](../Page/酒井彩名.md "wikilink")（SP）
  - 澤木晃（Apple）：[田邊誠一](../Page/田邊誠一.md "wikilink")
  - Pi刑警：[市川勉](../Page/市川勉.md "wikilink")
  - 羽根山末吉：[永澤俊矢](../Page/永澤俊矢.md "wikilink")（第一部） →
    [加藤茶](../Page/加藤茶.md "wikilink")（第二部）

#### 第2期起加入的人物

  - Meg：[淺見麗奈](../Page/淺見麗奈.md "wikilink")
  - Yomogida刑事：[宇梶剛士](../Page/宇梶剛士.md "wikilink")
  - Yatagai刑事：[伊藤正之](../Page/伊藤正之.md "wikilink")
  - 深海龍彦：[生田斗真](../Page/生田斗真.md "wikilink")
  - 実相寺碧：[黑澤明日香](../Page/黑澤明日香.md "wikilink")（黒沢あすか）
  - 幾島丈二（卡洛斯）：[真木藏人](../Page/真木藏人.md "wikilink")

<!-- end list -->

  - 城北label
      - [山崎裕太](../Page/山崎裕太.md "wikilink")
      - [鮫島巧](../Page/鮫島巧.md "wikilink")
      - [逆瀨川了](../Page/逆瀨川了.md "wikilink")
      - [忍成修吾](../Page/忍成修吾.md "wikilink")
      - [丹直樹](../Page/丹直樹.md "wikilink")
      - [山田孝之](../Page/山田孝之.md "wikilink")

<!-- end list -->

  - 西京守衛
      - B.J - [MAKOTO](../Page/越中睦.md "wikilink")
      - [塚本高史](../Page/塚本高史.md "wikilink")
      - [松嶋亮太](../Page/松嶋亮太.md "wikilink")
      - [根岸勇人](../Page/根岸勇人.md "wikilink")
      - [三上大和](../Page/三上大和.md "wikilink")
      - [大海慎介](../Page/大海慎介.md "wikilink")

#### 客串人物

##### 第1期

  - CASE 1

<!-- end list -->

  - 遠藤菊男（莫比烏斯）：[田口浩正](../Page/田口浩正.md "wikilink")
  - 三之輪浩市：[長江英和](../Page/長江英和.md "wikilink")
  - 遠藤正子：[原知佐子](../Page/原知佐子.md "wikilink")
  - 雪（ユキ）：[栗山千明](../Page/栗山千明.md "wikilink")
  - 中華屋店主：[笑福亭笑瓶](../Page/笑福亭笑瓶.md "wikilink")
  - 拉麵攤老闆：[麿赤兒](../Page/麿赤兒.md "wikilink")

<!-- end list -->

  - CASE 2

<!-- end list -->

  - 若林十藏：[寺田稔](../Page/寺田稔.md "wikilink")
  - 布袋結次：[高杢禎彦](../Page/高杢禎彦.md "wikilink")
  - 西卷丈治：[阿南健治](../Page/阿南健治.md "wikilink")
  - 夏目比美子：[田中廣子](../Page/田中廣子.md "wikilink")
  - 鳥居茜（あかね）：[井上彩名](../Page/井上彩名.md "wikilink")
  - 銀行分店長：[大河内浩](../Page/大河内浩.md "wikilink")

<!-- end list -->

  - CASE 3

<!-- end list -->

  - 大平瑞樹 / 加藤和樹：[伊藤隆大](../Page/伊藤隆大.md "wikilink")（一人飾演兩角）
  - 加藤佳奈子：[大場久美子](../Page/大場久美子.md "wikilink")

<!-- end list -->

  - CASE 4

<!-- end list -->

  - 赤樹理惠（リエ）：[佐藤仁美](../Page/佐藤仁美.md "wikilink")
  - 山村朝美：[岡田美幸](../Page/岡田美幸.md "wikilink")
  - 岡村泉（いずみ）：[小野麻亞矢](../Page/小野麻亞矢.md "wikilink")
  - 清水晶子：[中西禮奈](../Page/中西禮奈.md "wikilink")
  - 日下部淳也：[村松亮太郎](../Page/村松亮太郎.md "wikilink")
  - 赤樹隆（タカシ）：[三浦勉](../Page/三浦勉.md "wikilink")
  - 犬塚竹男：[田口智朗](../Page/田口智朗.md "wikilink")
  - 荒井弓子（無聲跟蹤狂）：[汀夏子](../Page/汀夏子.md "wikilink")
  - 看守：[渡部哲](../Page/渡部哲.md "wikilink")

<!-- end list -->

  - CASE 5

<!-- end list -->

  - 川邊：[和気優](../Page/和気優.md "wikilink")
  - 明（アキラ）：[田鍋謙一郎](../Page/田鍋謙一郎.md "wikilink")
  - 日比野一馬：[小橋賢兒](../Page/小橋賢兒.md "wikilink")
  - 橋慶太郎：[山田吾一](../Page/山田吾一.md "wikilink")

<!-- end list -->

  - CASE 6

<!-- end list -->

  - 渡邊先生：[佐戶井憲太](../Page/佐戶井憲太.md "wikilink")
  - 羽根山美佐子：[鳥越麻里](../Page/鳥越麻里.md "wikilink")（鳥越まり）
  - 山口專員：[大平透](../Page/大平透.md "wikilink")

##### 第2期

  - CASE 1

<!-- end list -->

  - 玉置（Justice）：[有薗芳記](../Page/有薗芳記.md "wikilink")
  - 山本警官：[桂嶋幸季](../Page/桂嶋幸季.md "wikilink")（桂嶋こうき）
  - 百瀨純（ジュン）：[岡元夕紀子](../Page/岡元夕紀子.md "wikilink")
  - 齊藤店長：[板尾創路](../Page/板尾創路.md "wikilink")

<!-- end list -->

  - CASE 2

<!-- end list -->

  - 千堂（猶大）：[市川勇](../Page/市川勇.md "wikilink")
  - 富永美紀：[小出由佳](../Page/小出由佳.md "wikilink")
  - 註冊商：[谷津勲](../Page/谷津勲.md "wikilink")
  - 警衛：[青柳克巳](../Page/青柳克巳.md "wikilink")

<!-- end list -->

  - CASE 3

<!-- end list -->

  - 中澤洋子：[仲根可澄美](../Page/仲根可澄美.md "wikilink")
  - 公佳：[深海理繪](../Page/深海理繪.md "wikilink")
  - 中谷久三：[秋野太作](../Page/秋野太作.md "wikilink")
  - 白雲山：[大富士](../Page/大富士.md "wikilink")
  - 縣令：[岸本功](../Page/岸本功.md "wikilink")

<!-- end list -->

  - CASE 4

<!-- end list -->

  - 加藤舞：[邑野未亞](../Page/邑野未亞.md "wikilink")（幼年期：[宮崎野惠瑠](../Page/廠野惠瑠.md "wikilink")、宮崎のえる）
  - 二階堂博士：[内田滋啓](../Page/内田滋啓.md "wikilink")
  - 長谷川大悟（ダイゴ）：[前田英二](../Page/前田英二.md "wikilink")
  - 轟省吾：[夏八木勳](../Page/夏八木勳.md "wikilink")
  - 理髪師：[仲本工事](../Page/仲本工事.md "wikilink")

<!-- end list -->

  - CASE 5

<!-- end list -->

  - 長谷百合子：[藤森美雪](../Page/藤森美雪.md "wikilink")（みゆき）
  - 細川郁美：[塚越小幸](../Page/塚越小幸.md "wikilink")
  - 青旬（アオジュン）：[KEE](../Page/KEE.md "wikilink")
  - 渡邊孝之：[松山幸次](../Page/松山幸次.md "wikilink")
  - 三浦良和（Poison）：[黑田勇樹](../Page/黑田勇樹.md "wikilink")

<!-- end list -->

  - CASE 6

<!-- end list -->

  - 牧原劍之介：[森次晃嗣](../Page/森次晃嗣.md "wikilink")

##### 特別篇

  - 田宮章吉母親：[坂口良子](../Page/坂口良子.md "wikilink")
  - 西澤真紀：[榎本加奈子](../Page/榎本加奈子.md "wikilink")
  - 松永：[桑野信義](../Page/桑野信義.md "wikilink")
  - 食品公司代表：[伊武雅刀](../Page/伊武雅刀.md "wikilink")
  - 乾洗店老闆（川邊）：[茄子](../Page/濱津智明.md "wikilink")（なすび）
  - 其他：[古尾谷雅人](../Page/古尾谷雅人.md "wikilink")、[千田光雄](../Page/千田光雄.md "wikilink")（せんだみつお）

### 製作團隊

  - 劇本：[小原信治](../Page/小原信治.md "wikilink")、[田子明弘](../Page/田子明弘.md "wikilink")、[大石哲也](../Page/大石哲也.md "wikilink")（1、SP）、[坂東賢治](../Page/坂東賢治.md "wikilink")（2）
  - 音樂：[寺田創一](../Page/寺田創一.md "wikilink")（1）、[DJ
    KRUSH](../Page/DJ_KRUSH.md "wikilink")（1）、[仲西匡](../Page/仲西匡.md "wikilink")（2）
      - 吉他：[吉川忠英](../Page/吉川忠英.md "wikilink")（1）、字串安排：[沢田完](../Page/沢田完.md "wikilink")（2）
  - 導演：[堤幸彥](../Page/堤幸彥.md "wikilink")（1）、[佐藤東彌](../Page/佐藤東彌.md "wikilink")（1）、[大谷太郎](../Page/大谷太郎.md "wikilink")（1）、[猪股隆一](../Page/猪股隆一.md "wikilink")（2）、[五木田亮一](../Page/五木田亮一.md "wikilink")（2）、[大根仁](../Page/大根仁.md "wikilink")（2）、[都築淳一](../Page/都築淳一.md "wikilink")（SP）
  - 主題曲
      - 第1期：[TOKIO](../Page/TOKIO.md "wikilink")「[フラれて元気](../Page/フラれて元気.md "wikilink")」
      - 第2期：TOKIO「[愛の嵐](../Page/愛の嵐_\(TOKIOの曲\).md "wikilink")」
  - 企劃合作：[樹林伸](../Page/樹林伸.md "wikilink")、[菅原章](../Page/菅原章.md "wikilink")
  - 音樂製作：[志田博英](../Page/志田博英.md "wikilink")、[松根文](../Page/松根文.md "wikilink")
  - 動作導演：[多賀谷渉](../Page/多賀谷渉.md "wikilink")
  - 美術：大竹潤一郎、 [高野雅裕](../Page/高野雅裕.md "wikilink")
  - 執行製作人：[小杉善信](../Page/小杉善信.md "wikilink")（1）、[佐藤敦](../Page/佐藤敦.md "wikilink")（2）
  - 製作人：[櫨山裕子](../Page/櫨山裕子.md "wikilink")、[蒔田光治](../Page/蒔田光治.md "wikilink")（1）、[伊藤響](../Page/伊藤響.md "wikilink")（2）、[内山雅博](../Page/内山雅博.md "wikilink")（2）、鈴木聡（SP）、中川順平（SP）
  - 製作協力：[Office Crescendo](../Page/Office_Crescendo.md "wikilink")、[K
    Factory](../Page/K_Factory.md "wikilink")

### 放送日程

#### 第1期（1997年）

<table>
<thead>
<tr class="header">
<th><p>播出回數</p></th>
<th><p>播出日期</p></th>
<th><p>標題</p></th>
<th><p>劇本</p></th>
<th><p>導演</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1 （CASE 1）</p></td>
<td><p>1997年1月11日</p></td>
<td><p>殺手莫比烏斯（前篇）</p></td>
<td><p>小原信治</p></td>
<td><p>堤幸彦</p></td>
<td><p>17.8%</p></td>
</tr>
<tr class="even">
<td><p>2 （CASE 1）</p></td>
<td><p>1997年1月18日</p></td>
<td><p>殺手莫比烏斯（後篇）</p></td>
<td><p>17.6%</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3 （CASE 2）</p></td>
<td><p>1997年1月25日</p></td>
<td><p>發條的蘋果（前篇）</p></td>
<td><p>田子明弘</p></td>
<td><p>佐藤東弥</p></td>
<td><p>16.4%</p></td>
</tr>
<tr class="even">
<td><p>4 （CASE 2）</p></td>
<td><p>1997年2月1日</p></td>
<td><p>發條的蘋果（後篇）</p></td>
<td><p>17.3%</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5 （CASE 3）</p></td>
<td><p>1997年2月8日</p></td>
<td><p>不要殺我</p></td>
<td><p>小原信治</p></td>
<td><p>堤幸彦</p></td>
<td><p>16.2%</p></td>
</tr>
<tr class="even">
<td><p>6 （CASE 4）</p></td>
<td><p>1997年2月15日</p></td>
<td><p>被狙擊的偶像（前篇）</p></td>
<td><p>田子明弘</p></td>
<td><p>大谷太郎</p></td>
<td><p>17.2%</p></td>
</tr>
<tr class="odd">
<td><p>7 （CASE 4）</p></td>
<td><p>1997年2月22日</p></td>
<td><p>被狙擊的偶像（後篇）</p></td>
<td><p>17.2%</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8 （CASE 5）</p></td>
<td><p>1997年3月1日</p></td>
<td><p>24HOURS 無面兇手</p></td>
<td><p>大石哲也</p></td>
<td><p>佐藤東弥</p></td>
<td><p>17.5%</p></td>
</tr>
<tr class="odd">
<td><p>9 （CASE 6）</p></td>
<td><p>1997年3月8日</p></td>
<td><p>THE LAST SEVEN DAYS（前篇）</p></td>
<td><p>小原信治</p></td>
<td><p>堤幸彦</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10 （CASE 6）</p></td>
<td><p>1997年3月15日</p></td>
<td><p>THE LAST SEVEN DAYS（後篇）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>平均視聴率 17.1%（視聴率は<a href="../Page/関東地方.md" title="wikilink">関東地区</a>・<a href="../Page/ビデオリサーチ.md" title="wikilink">ビデオリサーチ社調べ</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 第2期（1999年）

<table>
<thead>
<tr class="header">
<th><p>播出回數</p></th>
<th><p>播出日期</p></th>
<th><p>標題</p></th>
<th><p>劇本</p></th>
<th><p>導演</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1 （CASE 1）</p></td>
<td><p>1999年10月16日</p></td>
<td><p>殺手Justice（前編）</p></td>
<td><p>小原信治</p></td>
<td><p>猪股隆一</p></td>
<td><p>17.0%</p></td>
</tr>
<tr class="even">
<td><p>2 （CASE 1）</p></td>
<td><p>1999年10月23日</p></td>
<td><p>殺手Justice（後編）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3 （CASE 2）</p></td>
<td><p>1999年10月30日</p></td>
<td><p>猶大啟示錄（前編）</p></td>
<td><p>田子明弘</p></td>
<td><p>14.9%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4 （CASE 2）</p></td>
<td><p>1999年11月6日</p></td>
<td><p>猶大啟示錄（後編）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5 （CASE 3）</p></td>
<td><p>1999年11月13日</p></td>
<td><p>蒼白的手</p></td>
<td><p>坂東賢治</p></td>
<td><p>五木田亮一</p></td>
<td><p>16.0%</p></td>
</tr>
<tr class="even">
<td><p>6 （CASE 4）</p></td>
<td><p>1999年11月20日</p></td>
<td><p>恐怖分子的輓歌（前編）</p></td>
<td><p>小原信治</p></td>
<td><p>16.3%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7 （CASE 4）</p></td>
<td><p>1999年11月27日</p></td>
<td><p>恐怖分子的輓歌（後編）</p></td>
<td><p>14.9%</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8 （CASE 5）</p></td>
<td><p>1999年12月4日</p></td>
<td><p>笑的屍體</p></td>
<td><p>田子明弘</p></td>
<td><p>大根仁</p></td>
<td><p>14.5%</p></td>
</tr>
<tr class="odd">
<td><p>9 （CASE 6）</p></td>
<td><p>1999年12月11日</p></td>
<td><p>Last Revolution（前編）</p></td>
<td><p>小原信治</p></td>
<td><p>猪股隆一</p></td>
<td><p>14.5%</p></td>
</tr>
<tr class="even">
<td><p>10 （CASE 6）</p></td>
<td><p>1999年12月18日</p></td>
<td><p>Last Revolution（後編）</p></td>
<td><p>15.0%</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>平均視聴率 15.4%（視聴率は<a href="../Page/関東地方.md" title="wikilink">関東地区</a>・<a href="../Page/ビデオリサーチ.md" title="wikilink">ビデオリサーチ社調べ</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 特別篇

| 播出日期       | 標題      | 劇本   | 導演   | 收視率   |
| ---------- | ------- | ---- | ---- | ----- |
| 2000年9月24日 | 戰慄的殺人廚師 | 大石哲也 | 都築淳一 | 16.3% |

## 遊戲

1999年2月18日，[講談社發售](../Page/講談社.md "wikilink")[PlayStation遊戲光碟](../Page/PlayStation.md "wikilink")《感應少年EIJI（）》。

  - 配音

<!-- end list -->

  - 明日真 映兒 CV：[子安武人](../Page/子安武人.md "wikilink")
  - 志摩 亮子 CV：[三石琴乃](../Page/三石琴乃.md "wikilink")

## 註釋

## 外部連結

  -
[category:驚悚漫畫](../Page/category:驚悚漫畫.md "wikilink")
[category:日本科幻劇](../Page/category:日本科幻劇.md "wikilink")
[category:超能力題材電視劇](../Page/category:超能力題材電視劇.md "wikilink")

[Category:亞樹直](../Category/亞樹直.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:講談社](../Category/講談社.md "wikilink")
[Category:講談社漫畫](../Category/講談社漫畫.md "wikilink")
[Category:少年漫畫](../Category/少年漫畫.md "wikilink")
[Category:週刊少年Magazine連載作品](../Category/週刊少年Magazine連載作品.md "wikilink")
[Category:超能力題材漫畫](../Category/超能力題材漫畫.md "wikilink")
[Category:日本電視台週六連續劇](../Category/日本電視台週六連續劇.md "wikilink")
[Category:1997年日本電視劇集](../Category/1997年日本電視劇集.md "wikilink")
[Category:1999年日本電視劇集](../Category/1999年日本電視劇集.md "wikilink")
[Category:推理漫畫改編電視劇](../Category/推理漫畫改編電視劇.md "wikilink")
[Category:驚悚電視劇](../Category/驚悚電視劇.md "wikilink")
[Category:警察題材電視劇](../Category/警察題材電視劇.md "wikilink")
[Category:PlayStation
(游戏机)游戏](../Category/PlayStation_\(游戏机\)游戏.md "wikilink")
[Category:冒險遊戲](../Category/冒險遊戲.md "wikilink")
[Category:1999年电子游戏](../Category/1999年电子游戏.md "wikilink")
[Category:東京都背景作品](../Category/東京都背景作品.md "wikilink")
[Category:驚悚遊戲](../Category/驚悚遊戲.md "wikilink")