**高碘酸**，也称**过碘酸**，是[碘](../Page/碘.md "wikilink")(VII)的[含氧酸](../Page/含氧酸.md "wikilink")，有两种形式，[化学式分别为HIO](../Page/化学式.md "wikilink")<sub>4</sub>及H<sub>5</sub>IO<sub>6</sub>。

高碘酸的稀溶液中存在H<sup>+</sup>和IO<sub>4</sub><sup>−</sup>离子。随着浓度的上升，高碘酸开始以**正高碘酸**H<sub>5</sub>IO<sub>6</sub>的形式存在，可以从溶液析出晶体。

正高碘酸脱水得到**偏高碘酸**HIO<sub>4</sub>，进一步脱水则生成七氧化二碘(无色粘稠液体)，七氧化二碘分解後则得到[五氧化二碘](../Page/五氧化二碘.md "wikilink")（I<sub>2</sub>O<sub>5</sub>）和[氧气](../Page/氧气.md "wikilink")。

正高碘酸和偏高碘酸可分别生成盐类，称为“正高碘酸盐”及“偏高碘酸盐”。偏高碘酸盐的[溶解度和](../Page/溶解度.md "wikilink")[化学性质与同族卤素的](../Page/化学性质.md "wikilink")[高氯酸盐类似](../Page/高氯酸盐.md "wikilink")，但阴离子半径更大，[氧化性较弱](../Page/氧化性.md "wikilink")。

[有机化学中](../Page/有机化学.md "wikilink")，高碘酸可氧化[邻](../Page/邻.md "wikilink")[二醇并使碳](../Page/二醇.md "wikilink")-碳键断裂，生成两个[醛](../Page/醛.md "wikilink")，用于确定[糖类的结构](../Page/糖类.md "wikilink")。

  -
    [Periodic-acid-3D-balls.png](https://zh.wikipedia.org/wiki/File:Periodic-acid-3D-balls.png "fig:Periodic-acid-3D-balls.png")

## 命名

英语中，高碘酸的名称（*Periodic acid*）由习惯命名法得来：

  -
    *Per* + *iod* + *ic*
    “高” + 碘 + 五价卤素[含氧酸的特定后缀](../Page/含氧酸.md "wikilink")

组成*Periodic*，恰好与英语中的“周期的”（*Periodic*，*Period-ic*）一词相同。

## 化学性质

高碘酸有强氧化性，在溶液中，可以将二价的锰离子氧化为高锰酸根离子：\[1\]

  -
    5 H<sub>5</sub>IO<sub>6</sub> + 2 Mn<sup>2+</sup> → 2
    MnO<sub>4</sub><sup>-</sup> + 5 IO<sub>3</sub><sup>-</sup> + 7
    H<sub>2</sub>O + 11 H<sup>+</sup>

## 参见

  - [高溴酸](../Page/高溴酸.md "wikilink")、[高氯酸](../Page/高氯酸.md "wikilink")
  - [碘酸](../Page/碘酸.md "wikilink")、[次碘酸](../Page/次碘酸.md "wikilink")

## 参考资料

<div class="references-small">

  -

</div>

[\*](../Category/高碘酸盐.md "wikilink") [碘](../Category/无机酸.md "wikilink")
[碘](../Category/腐蝕性化學品.md "wikilink")
[Category:潮解物质](../Category/潮解物质.md "wikilink")

1.  《无机化学》（第五版）. 大连理工大学无机化学教研室 编. 高等教育出版社. 第三篇 元素化学. P<sub>524</sub>.
    1.卤素的氧化物