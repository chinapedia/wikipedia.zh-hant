<includeonly>}}}}

| list1name = 信仰 | list1title = [信仰](伊瑪尼.md "wikilink") | list1 =

  - [神的](伊斯蘭教的神.md "wikilink")[獨一性](認主學.md "wikilink")

<!-- end list -->

  - [先知](伊斯蘭教的先知.md "wikilink")
  - [啟示之書](伊斯蘭教聖書.md "wikilink")

<!-- end list -->

  - [天使](伊斯蘭教中的天使.md "wikilink")
  - [宿命論](伊斯蘭教宿命論.md "wikilink")

<!-- end list -->

  - [末日審判](伊斯蘭教末世論.md "wikilink")

| list2name = 習俗 | list2title = [習俗](五功.md "wikilink") | list2 =

  - [證信](清真言.md "wikilink")
  - [禮拜](禮拜_\(伊斯蘭教\).md "wikilink")

<!-- end list -->

  - [齋戒](齋戒_\(伊斯蘭教\).md "wikilink")
  - [施捨](天課.md "wikilink")
  - [朝覲](朝覲_\(伊斯蘭教\).md "wikilink")

| list3name = 典籍 | list3title =
[典籍及](伊斯蘭教典籍列表.md "wikilink")[法律](伊斯蘭教法.md "wikilink")
| list3 =

  - [古蘭經](古蘭經.md "wikilink")
  - [聖行](聖行.md "wikilink")
  - [聖訓](聖訓.md "wikilink")

<!-- end list -->

  - [沙里亞 {{small](伊斯蘭教法.md "wikilink")
  - [費格赫 {{small](伊斯蘭教法學.md "wikilink")

<!-- end list -->

  - \[\[伊斯蘭教義學|凱拉姆 </includeonly><noinclude>

</noinclude>