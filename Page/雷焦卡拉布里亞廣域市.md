**雷焦卡拉布里亞廣域市**（）是[意大利](../Page/意大利.md "wikilink")[卡拉布里亞的一個](../Page/卡拉布里亞.md "wikilink")[广域市](../Page/广域市_\(意大利\).md "wikilink")。面積3,183平方公里，2005年人口565,866人。首府[雷焦卡拉布里亞](../Page/雷焦卡拉布里亞.md "wikilink")。前身为[雷焦卡拉布里亞省](../Page/雷焦卡拉布里亞省.md "wikilink")。

下分97市鎮。

[雷焦卡拉布里亞廣域市](../Category/雷焦卡拉布里亞廣域市.md "wikilink")
[Category:意大利廣域市](../Category/意大利廣域市.md "wikilink")