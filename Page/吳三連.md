<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>臺灣總督府國語師範學校畢業</li>
<li>日本東京商科大學（今<a href="../Page/一橋大學.md" title="wikilink">一橋大學</a>）畢業<br />
<span style="color: blue;">(1925年)</span></li>
</ul></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>大阪《<a href="../Page/每日新聞.md" title="wikilink">每日新聞</a>》記者</li>
<li>《台灣新民報》總編<br />
<span style="color: blue;">(1932年-)</span></li>
<li>大阪《每日新聞》東京分社分社長<br />
<span style="color: blue;">(1933年-1940年)</span></li>
<li>天津臺灣同鄉會會長1945-</li>
<li>第一屆國大代表<br />
<span style="color: blue;">(1947年-)</span></li>
<li>臺北市（省轄市）第三任市長<br />
<span style="color: blue;">(1950年2月6日-1950年11月)</span></li>
<li>臺北市（省轄市）第四任（首屆）市長<br />
<span style="color: blue;">(1951年2月1日-1954年6月1日)</span></li>
<li>臺灣省臨時會議員</li>
<li>第二、第三屆省參議會議員</li>
<li><a href="../Page/台南紡織.md" title="wikilink">台南紡織公司董事長</a></li>
<li><a href="../Page/環球水泥.md" title="wikilink">環球水泥公司董事長</a></li>
<li><a href="../Page/大臺北瓦斯.md" title="wikilink">大臺北瓦斯公司董事長</a></li>
<li><a href="../Page/南紡企業.md" title="wikilink">南紡企業董事長</a></li>
<li><a href="../Page/太子龍紡織.md" title="wikilink">太子龍紡織公司董事長</a></li>
<li><a href="../Page/中和紡織.md" title="wikilink">中和紡織公司常務董事</a></li>
<li><a href="../Page/國賓大飯店.md" title="wikilink">國賓大飯店常務董事</a></li>
<li><a href="../Page/泰安產物保險公司.md" title="wikilink">泰安產物保險公司常務董事</a></li>
<li><a href="../Page/彰化商業銀行.md" title="wikilink">彰化商業銀行董事</a></li>
<li>國貨推動中心董事</li>
<li>棉業倉儲公司董事</li>
<li><a href="../Page/自立晚報.md" title="wikilink">自立晚報發行人</a><br />
<span style="color: blue;">(1959年)</span></li>
<li><a href="../Page/台灣日報.md" title="wikilink">台灣日報常務董事</a></li>
<li><a href="../Page/臺灣時報.md" title="wikilink">臺灣時報常務董事</a></li>
<li>新生報業公司監察人</li>
<li><a href="../Page/延平中學.md" title="wikilink">延平中學董事長</a></li>
<li><a href="../Page/天仁工商高級職業學校.md" title="wikilink">天仁工商高級職業學校董事長</a></li>
<li><a href="../Page/南臺工業專科學校.md" title="wikilink">南臺工業專科學校董事長</a><br />
<span style="color: blue;">(1965年-)</span></li>
<li>吳三連文藝獎基金會創辦人<br />
<span style="color: blue;">(1978年)</span></li>
</ul>
</div></td>
</tr>
</tbody>
</table>

**吳三連**（），[臺灣政治人物](../Page/臺灣.md "wikilink")、報人。[臺南](../Page/臺南市.md "wikilink")[學甲人](../Page/學甲區.md "wikilink")，東京商科大學（今[一橋大學](../Page/一橋大學.md "wikilink")）畢業。日治時期曾當過《[每日新聞](../Page/每日新聞.md "wikilink")》記者、參與《[台灣新民報](../Page/台灣新民報.md "wikilink")》。戰後出任[國大代表](../Page/國大代表.md "wikilink")、[臺北市長](../Page/臺北市長.md "wikilink")、[臺灣省議會議員](../Page/臺灣省議會.md "wikilink")，為[日治時期與戰後臺灣政治](../Page/臺灣日治時期.md "wikilink")[民主化運動](../Page/民主化.md "wikilink")、[臺灣本土化運動](../Page/台灣本土化運動.md "wikilink")、及社會運動的重要先驅人物。

吳三連亦曾參與創辦[台南紡織及](../Page/台南紡織.md "wikilink")《[自立晚報](../Page/自立晚報.md "wikilink")》，在[臺灣教育上](../Page/臺灣教育.md "wikilink")，吳三連也是南台工專（今[南台科技大學](../Page/南台科技大學.md "wikilink")）、[天仁工商](../Page/天仁工商.md "wikilink")、[延平中學等三所學校之共同創辦人](../Page/延平中學.md "wikilink")。

## 早年生平

吳三連於1899年（[明治三十二年](../Page/明治.md "wikilink")）出生於臺南-{[學甲庄](../Page/學甲庄.md "wikilink")}-一個貧瘠的村-{庄}-「頭港仔」，父親吳徙以木匠為業，自幼家裡貧困。

1911年（明治四十四年），吳三連入[學甲公學校](../Page/學甲公學校.md "wikilink")，1915年（[大正四年](../Page/大正.md "wikilink")）畢業，考入[臺北](../Page/臺北.md "wikilink")[國語學校](../Page/臺灣總督府國語學校.md "wikilink")。1919年（大正八年）自臺北國語學校國語部畢業。

1919年，吳三連考上[板橋林家](../Page/板橋林家.md "wikilink")[林熊徵獎學金留學](../Page/林熊徵.md "wikilink")[日本](../Page/日本.md "wikilink")，考入[東京高商預科](../Page/東京高商.md "wikilink")。留學期間吳三連曾經參與臺灣人對日本的民族運動。1920年（大正九年）加入東京留學生組織改革臺灣的運動團體「[新民會](../Page/新民會.md "wikilink")」、「[東京台灣青年會](../Page/東京台灣青年會.md "wikilink")」等組織，在1920年[臺灣總督府為臺灣留學生舉辦的招待會上](../Page/臺灣總督府.md "wikilink")，吳三連即於會議上當著臺灣總督府總務長官[下村宏面前批評日本殖民統治的高壓政策](../Page/下村宏.md "wikilink")，成為吳三連記憶中第一次與日本殖民政府的衝突\[1\]。之後吳也積極參與過[林獻堂的](../Page/林獻堂.md "wikilink")「[臺灣議會設置請願運動](../Page/臺灣議會設置請願運動.md "wikilink")」。

## 參與新聞業

1925年（大正十四年）3月，吳三連自[東京商科大學畢業](../Page/一橋大學.md "wikilink")，7月出任《[大阪每日新聞](../Page/大阪每日新聞.md "wikilink")》任經濟新聞[記者](../Page/記者.md "wikilink")。在擔任記者其間，吳三連在[蔡培火介紹下](../Page/蔡培火.md "wikilink")，於1927年（[昭和二年](../Page/昭和.md "wikilink")）與府城商人李兆偉之女李菱結婚。婚後定居日本。\[2\]

1932年（昭和七年），《[台灣新民報](../Page/台灣新民報.md "wikilink")》創刊。由於此時只有吳三連有記者經驗，遂獲得推薦。在[林獻堂](../Page/林獻堂.md "wikilink")、[蔡培火堅持下](../Page/蔡培火.md "wikilink")，吳三連最後返臺出任《[台灣新民報](../Page/台灣新民報.md "wikilink")》編輯，兼任論說委員、整理部長、政治部長。並開闢專欄「爆彈」批評時政。\[3\]1933年（昭和八年），《台灣新民報》在[東京設立支局](../Page/東京.md "wikilink")，吳三連遂出任東京支局長。\[4\]

1938年（昭和十三年）1月18日，吳三連遭[東京警視廳拘押](../Page/東京警視廳.md "wikilink")21天，一般認為吳三連遭拘押的原因是反對米穀政策，然而吳三連在其回憶錄對此說法質疑，因為在審訊過程中並未提及米穀政策。\[5\]同時拘押的台灣人還有蔡培火。之後吳三連於1939年（昭和十四年）撰文〈台灣米穀政策之檢討〉，批判日本政府的米穀政策。\[6\]

1940年（昭和十五年）2月，吳三連被迫離職。1941年前往日軍占領的[北京](../Page/北京.md "wikilink")，任職於大治株式會社。1942年至[天津](../Page/天津.md "wikilink")，與連襟陳火碑合營合豐行，買賣染料。

## 二次戰後

1946年吳三連在臺南縣選區參選[制憲國大代表選舉並以](../Page/制憲國大代表.md "wikilink")23萬多票，全臺第一高票當選，1947年又高票當選中華民國[第一屆國大代表](../Page/第一屆國大代表.md "wikilink")，目睹[李宗仁與](../Page/李宗仁.md "wikilink")[孫科爭奪副總統職位的鬥爭](../Page/孫科.md "wikilink")。

## 臺北市長

1950年吳三連獲得[蔣介石召見](../Page/蔣介石.md "wikilink")，2月出任官派臺北市長，任內致力於安置[國共內戰後到臺灣的國軍](../Page/國共內戰.md "wikilink")，當時臺北市民由1945年的33萬餘人爆增至1950年的50萬多人。

1950年吳三連參加[臺北市長民選](../Page/1950年－1951年中華民國縣市長選舉.md "wikilink")，辭去市長由[項昌權代理](../Page/項昌權.md "wikilink")，吳三連以小冊子《市政三年》為競選政見，獲得選民支持。1951年1月7日開票當天，吳三連得到九萬兩千餘票，以得票率65.5%擊敗[高玉樹等強敵當選](../Page/高玉樹.md "wikilink")。2月1日就職。市長期間面臨違章建築氾濫，吳三連拓寬[羅斯福路一小段即任滿](../Page/羅斯福路.md "wikilink")。由於深感精神與體力磨損\[7\]，吳三連遂不參選1954年的市長選舉。

| 1951年臺北市市長選舉結果                       |
| ------------------------------------ |
| 號次                                   |
| 1                                    |
| 2                                    |
| 3                                    |
| 4                                    |
| 5                                    |
| 6                                    |
| 7                                    |
| **選舉人數**                             |
| **投票數**                              |
| **有效票**                              |
| **投票率**                              |
| 資料來源：《臺北市志·卷三·政制志選舉篇》，黃振超，1987年，頁249 |

1952年吳三連長子[吳逸民就讀台大](../Page/吳逸民.md "wikilink")，因受同學[歐振隆介紹加入](../Page/歐振隆.md "wikilink")[台湾民主自治同盟](../Page/台湾民主自治同盟.md "wikilink")，後省工委台大法學院支部案遭破獲後，[吳逸民亦遭到保密局逮捕](../Page/吳逸民.md "wikilink")，當時吳三連曾託人出面說情，但時值國共內戰結束之際，許多資料顯示共諜滲透致使戰事失利，如傅作義女兒洩漏軍情，蔣介石非常痛恨共產黨，同時也正處於韓戰爆發，大肆搜捕共黨的階段，台電董事長劉晉鈺甚至因為兒子的家書而遭懷疑被策反而槍決，無法說動高層，吳三連只得用市長身分去台北監獄視察，趁機偷偷去看[吳逸民](../Page/吳逸民.md "wikilink")。\[8\]

1954年吳三連在臺南縣議會議長邀請下參選臺灣省議員並當選，1957年連任。任內與[郭雨新](../Page/郭雨新.md "wikilink")、[郭國基](../Page/郭國基.md "wikilink")、[李源棧](../Page/李源棧.md "wikilink")、[李萬居並稱為](../Page/李萬居.md "wikilink")「黨外五虎將」，吳三連在這段期間不但投入《[自立晚報](../Page/自立晚報.md "wikilink")》的經營，也參與[雷震的組黨運動](../Page/雷震.md "wikilink")\[9\]。1960年在第二任省議員任期屆滿，由於感到體力不能承受，加上選風敗壞，以及背負太多「人情債」，吳三連決定不再尋求連任\[10\]。

## 卸任後

吳三連為「[台南幫](../Page/台南幫.md "wikilink")」重要精神領袖、地位崇高，在實業界有相當分量。早在1954年，即與台南幫的[吳尊賢](../Page/吳尊賢.md "wikilink")、[侯雨利](../Page/侯雨利.md "wikilink")、[吳修齊創立](../Page/吳修齊.md "wikilink")[台南紡織](../Page/台南紡織.md "wikilink")，吳三連擔任董事長，吳修齊任總經理。1960年3月更參與[環球水泥公司的成立](../Page/環球水泥.md "wikilink")。退出政壇後，吳三連即專心從商。

此外在1965年《[自立晚報](../Page/自立晚報.md "wikilink")》改組後，吳三連擔任發行人兼社長，1966年專任發行人，經營《自立晚報》。在經營上吳三連也秉持客觀、中立、本土的原則，為當時的臺灣留下一小片[言論自由的空間](../Page/言論自由.md "wikilink")。而在1980年代[黨外運動勃興時](../Page/黨外運動.md "wikilink")，吳三連也多次扮演調和鼎鼐的角色。吳三連不但資助傑出的新聞媒體編寫者，也獎勵本土研究，以關懷臺灣本土文化。另外，吳三連也創立台南紡織、[環球水泥](../Page/環球水泥.md "wikilink")，興辦南台工專（[南台科技大學前身](../Page/南台科技大學.md "wikilink")）、[天仁工商](../Page/天仁工商.md "wikilink")、[延平中學等三所學校](../Page/延平中學.md "wikilink")。

吳三連認­為[文學和](../Page/文學.md "wikilink")[藝術是民族長遠生存發展的命脈](../Page/藝術.md "wikilink")，也是一個國家人文精神的寄託，­所以全心推動相關活動，1978年1月30日成立「財團法人吳三連先生文藝獎基金會」，頒發「[吳三連文藝獎](../Page/吳三連文藝獎.md "wikilink")」，豐富了本土文學和藝術的內涵；吳三連去世後的1989年，獎項擴增到人文社會科學獎、數理生物科學獎、醫學獎、實業獎、社會服務獎等，故更名為「財團法人吳三連獎基金會」。

吳三連晚年曾出任[國策顧問](../Page/國策顧問.md "wikilink")，1988年12月29日10時15分因[心臟衰竭逝於](../Page/心臟.md "wikilink")[臺大醫院](../Page/國立臺灣大學醫學院附設醫院.md "wikilink")。身後歸葬故里學甲鎮頭港-{里}-淳吉堂墓園。

## 參見

  - [吳三連獎](../Page/吳三連獎.md "wikilink")

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 傳記及研究書目

  - 吳三連口述，[吳豐山撰述](../Page/吳豐山.md "wikilink")，1991，《吳三連回憶錄》，台北：[自立晚報](../Page/自立晚報.md "wikilink")。

## 外部連結

  - [吳三連簡介](https://web.archive.org/web/20130810074135/http://www.wusanlien.org.tw/01aboutus/01biographywu_a.htm)
  - [吳三連獎基金會](http://www.wusanlien.org.tw/)
  - [吳三連台灣史料基金會](http://www.twcenter.org.tw/)

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:第1屆臺灣省議員](../Category/第1屆臺灣省議員.md "wikilink")
[Category:第3屆臺灣省臨時議會議員](../Category/第3屆臺灣省臨時議會議員.md "wikilink")
[Category:第2屆臺灣省臨時議會議員](../Category/第2屆臺灣省臨時議會議員.md "wikilink")
[Category:台北市市長](../Category/台北市市長.md "wikilink")
[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:制憲國民大會代表](../Category/制憲國民大會代表.md "wikilink")
[Category:台灣企業家](../Category/台灣企業家.md "wikilink")
[Category:臺灣民主運動者](../Category/臺灣民主運動者.md "wikilink")
[Category:黨外運動人物](../Category/黨外運動人物.md "wikilink")
[Category:台灣白色恐怖時期異議人士](../Category/台灣白色恐怖時期異議人士.md "wikilink")
[Category:臺灣教育工作者](../Category/臺灣教育工作者.md "wikilink")
[Category:臺灣日治時期記者](../Category/臺灣日治時期記者.md "wikilink")
[Category:一橋大學校友](../Category/一橋大學校友.md "wikilink")
[Category:國立臺北教育大學校友](../Category/國立臺北教育大學校友.md "wikilink")
[Category:臺北市立教育大學校友](../Category/臺北市立教育大學校友.md "wikilink")
[Category:學甲人](../Category/學甲人.md "wikilink")
[S三](../Category/吳姓.md "wikilink")
[Category:半山](../Category/半山.md "wikilink")
[Category:台灣地方志先賢](../Category/台灣地方志先賢.md "wikilink")
[Category:台南幫人物](../Category/台南幫人物.md "wikilink")
[Category:閩南裔臺灣人](../Category/閩南裔臺灣人.md "wikilink")

1.  《吳三連回憶錄》吳三連口述，吳豐山撰述，《[自立晚報](../Page/自立晚報.md "wikilink")》出版，1991年，頁38
2.  《吳三連回憶錄》同上，頁66-67
3.  《吳三連回憶錄》同上，頁71
4.  《吳三連回憶錄》同上，頁82
5.  《吳三連回憶錄》同上，頁93-94
6.  《吳三連回憶錄》同上，頁91
7.  《吳三連回憶錄》同上，頁152
8.
9.  [李筱峰](../Page/李筱峰.md "wikilink"),
    [台灣戒嚴時期政治案件的類型](http://www.newtaiwan.com.tw/bulletinview.jsp?bulletinid=24454)
    、[新台灣新聞周刊](../Page/新台灣新聞周刊.md "wikilink")，2000/11/23
10. 《吳三連回憶錄》同上，頁156