[Venus_symbol.svg](https://zh.wikipedia.org/wiki/File:Venus_symbol.svg "fig:Venus_symbol.svg")的美神、[愛神](../Page/愛神.md "wikilink")[維納斯的符號](../Page/維納斯.md "wikilink")，經常用作代表女性，也象徵[金星以及](../Page/金星.md "wikilink")[鍊金術符號中的](../Page/鍊金術.md "wikilink")[銅](../Page/銅.md "wikilink")\]\]
**女性**，是指[雌性的](../Page/雌性.md "wikilink")[人類](../Page/人.md "wikilink")，與[雄性人類即](../Page/雄性.md "wikilink")[男性作區別](../Page/男性.md "wikilink")。女性這個名詞是用來表示[生物學上的](../Page/生物學.md "wikilink")[性別劃分](../Page/性別.md "wikilink")，正式只適用於稱呼人類，但有時侯也會用作稱呼其他[生物](../Page/生物.md "wikilink")，同時亦可指[文化上的](../Page/文化.md "wikilink")[性別角色](../Page/性別角色.md "wikilink")。

和其他大部份的[哺乳類一様](../Page/哺乳類.md "wikilink")，女性的[基因組中包括一個來自母親的](../Page/基因組.md "wikilink")[X染色體以及一個來自父親的](../Page/X染色體.md "wikilink")[X染色體](../Page/X染色體.md "wikilink")。相較於男性胚胎，女性胚胎會分泌較多[雌激素](../Page/雌激素.md "wikilink")．較少[雄激素](../Page/雄激素.md "wikilink")。而[性類固醇量的相對差異是造成女性和男性](../Page/性類固醇.md "wikilink")[生理學差異的主要原因](../Page/生理學.md "wikilink")。在青春期時，荷爾蒙會刺激身體分泌雌激素，造成[第二性徵的發展](../Page/第二性徵.md "wikilink")，因此兩性會有更明顯的差異。

「女人」通常是專指女性[成年人](../Page/成年人.md "wikilink")，與男人相對。「[女孩](../Page/女孩.md "wikilink")」是年輕[未成年的女性](../Page/未成年.md "wikilink")，或者剛成年的年輕女性。「**[女生](../Page/女生.md "wikilink")**」传统含义为「女性学生」，现在則可泛指任何年青的女性。

[Woman_Montage_(1).jpg](https://zh.wikipedia.org/wiki/File:Woman_Montage_\(1\).jpg "fig:Woman_Montage_(1).jpg")
{{•}} [维纳斯](../Page/维纳斯.md "wikilink") {{•}}
[聖女貞德](../Page/聖女貞德.md "wikilink") {{•}}
[伊娃·裴隆](../Page/伊娃·裴隆.md "wikilink")
（第二排）[玛丽·居里](../Page/玛丽·居里.md "wikilink") {{•}}
[英迪拉·甘地](../Page/英迪拉·甘地.md "wikilink") {{•}}
[維倫多爾夫的維納斯](../Page/維倫多爾夫的維納斯.md "wikilink") {{•}}
[旺加里·马塔伊](../Page/旺加里·马塔伊.md "wikilink")
（第三排）[德蕾莎修女](../Page/德蕾莎修女.md "wikilink") {{•}}
[葛麗絲·霍普](../Page/葛麗絲·霍普.md "wikilink") {{•}}
一名[艺妓](../Page/艺妓.md "wikilink") {{•}} 西藏農婦
（第四排）[玛丽莲·梦露](../Page/玛丽莲·梦露.md "wikilink") {{•}}
[奧花·雲費](../Page/奧花·雲費.md "wikilink") {{•}}
[翁山蘇姬](../Page/翁山蘇姬.md "wikilink") {{•}}
[約瑟芬·貝克](../Page/約瑟芬·貝克.md "wikilink")
（第五排）[伊西絲](../Page/伊西絲.md "wikilink") {{•}}
[示巴女王](../Page/示巴女王.md "wikilink") {{•}}
[伊丽莎白一世](../Page/伊丽莎白一世_\(英格兰\).md "wikilink") {{•}} \]\]

## 生物學與性別

[Scheme_female_reproductive_system-zh_yue.svg](https://zh.wikipedia.org/wiki/File:Scheme_female_reproductive_system-zh_yue.svg "fig:Scheme_female_reproductive_system-zh_yue.svg")\]\]
[Sky_spectral_karyotype.png](https://zh.wikipedia.org/wiki/File:Sky_spectral_karyotype.png "fig:Sky_spectral_karyotype.png")\]\]
在[生物學上](../Page/生物學.md "wikilink")，女性的[性器官是指其生殖系統](../Page/性器官.md "wikilink")，而[第二性徵則是指和養育嬰孩或](../Page/第二性徵.md "wikilink")（在某些文化中）吸引異性有關的部位。[卵巢會產生女性的](../Page/卵巢.md "wikilink")[生殖細胞](../Page/生殖細胞.md "wikilink")，稱為[卵子](../Page/卵子.md "wikilink")，和男性產生的[精子受精後](../Page/精子.md "wikilink")，可以成長形成獨立的生物體。[子宮是受精卵成長的環境](../Page/子宮.md "wikilink")，可以保護受精卵並提供養份，當分娩時子宮會收縮，產出嬰兒。[陰道是精子進入子宮的通道](../Page/陰道.md "wikilink")，也是嬰兒出生的通道。[乳房有乳腺](../Page/乳房.md "wikilink")，在嬰兒誕生後可以分泌乳汁，是哺乳類動物不同於其他動物的最大特徵。一般而言，成年女性的乳房都較其他哺乳類動物要大，其原因可能不是因為哺乳需要，有可能是[性選擇後的結果](../Page/性選擇.md "wikilink")。

在胚胎早期發育時，兩性的胚胎是一樣的（即性別[中性](../Page/中性_\(性別\).md "wikilink")）。
若胚胎在有相當[睪酮的環境下成長](../Page/睪酮.md "wikilink")（一般是由於來自父親的Y染色體），會長成為男性，否則，會長成為女性。後者一般是由父親遺傳到X染色體，但也可能是父親提供了X染色體及Y染色體以外的染色體。在青春期時，[雌二醇使女孩性成熟](../Page/雌二醇.md "wikilink")，並且發育第二性徵。

母親的荷爾蒙量以及化學物質（或藥物）可能會影響胎兒的第二性徵。大部份的女性染色體[核型為](../Page/核型.md "wikilink")46,XX，但有千分之一的機率會是[47,XXX](../Page/X-三體.md "wikilink")，有二千五百分之一的機率會是[45,X](../Page/透納氏綜合症.md "wikilink")。和46,XX相對的核型是XY，因此一般會用[x染色體和](../Page/x染色體.md "wikilink")[Y染色體來識別女性及男性](../Page/Y染色體.md "wikilink")。由於人類的粒線體DNA只來自母親的卵子，因此基因學中的母系研究一般會以來分析。

與其他哺乳類動物不同，生物學上的因素並不是人類性別的決定性因素。有些女人先天性擁有非正常的[荷爾蒙或](../Page/激素.md "wikilink")[染色體狀況](../Page/染色體.md "wikilink")（比如[先天性腎上腺增生症或者](../Page/先天性腎上腺增生症.md "wikilink")[雙性人](../Page/雙性人.md "wikilink")），也有些女人後天進行[性別重塑手術而缺乏典型的女性生理特徵](../Page/性別重置手術.md "wikilink")（[跨性別女性](../Page/跨性別女性.md "wikilink")）。現今社會上的跨性別女性主要是以一些社會因素去決定性別。例如在香港，女性的定義以入境事務審裁處的身份證登記的性別記錄為準。而任何市民如進行性別重塑手術有責任向入境處申請更改性別紀錄。\[1\]

雖然在出生時女嬰的數量會略少於男嬰（其比例約為1:1.05），但因為女性的平均壽命較長，因此每超過六十歲的女性和男性比例約為（100:81）。女性的平均壽命一般要比男性要長\[2\]。造成此現象的原因有幾個：[基因](../Page/基因.md "wikilink")（女性[性染色體上的冗餘及多様的基因](../Page/性染色體.md "wikilink")）、社會學（大多數國家的[徵兵制只考慮男性](../Page/徵兵制.md "wikilink")）、健康有關的生活選擇（如[自殺或是煙酒的使用](../Page/自殺.md "wikilink")）、[雌二醇的影響](../Page/雌二醇.md "wikilink")（在停經前可以保護心臟）及[雄激素對男性的影響](../Page/雄激素.md "wikilink")。在世界總人口中，男女比例約為101.3:100\[3\]。

[Anterior_view_of_human_female_and_male,_with_labels.png](https://zh.wikipedia.org/wiki/File:Anterior_view_of_human_female_and_male,_with_labels.png "fig:Anterior_view_of_human_female_and_male,_with_labels.png")
女孩的身體在[青春期時會有逐漸的變化](../Page/青春期.md "wikilink")。青春期是一個身體變化的過程，由小孩的身體成長為成人的身體，可以進行[有性生殖使卵子](../Page/有性生殖.md "wikilink")[受精](../Page/受精.md "wikilink")。青春期是由大腦產生給[生殖腺的荷爾蒙信號所引發](../Page/生殖腺.md "wikilink")，生殖腺因而產生荷爾蒙，激起[性衝動以及](../Page/性衝動.md "wikilink")[大腦](../Page/大腦.md "wikilink")、[骨骼](../Page/骨骼.md "wikilink")、[肌肉](../Page/肌肉.md "wikilink")、[血液](../Page/血液.md "wikilink")、[皮膚](../Page/皮膚.md "wikilink")、[頭髮](../Page/頭髮.md "wikilink")、[胸及](../Page/胸.md "wikilink")[性器官的成長](../Page/性器官.md "wikilink")。在青春期的前半，身高及體重的成長會加速，青春期結束時就已經具有成人的體格。在生殖能力成熟之前，青春期前男孩和女孩的身體差異只有[生殖器官](../Page/生殖器官.md "wikilink")。青春期一般會是在十歲到十六歲之間，但時間因人而異。女孩青春期的重要事件是[初潮](../Page/初潮.md "wikilink")，也就是月經的開始，約在十二歲到十三歲\[4\]\[5\]\[6\]\[7\]。

大部份女性會在青春期有[初潮](../Page/初潮.md "wikilink")，之後身體的發育已經可以[懷孕及](../Page/懷孕.md "wikilink")[分娩](../Page/分娩.md "wikilink")\[8\]。[懷孕及](../Page/懷孕.md "wikilink")[分娩一般是需要透過](../Page/分娩.md "wikilink")[性交使精子和卵子在女性體內受精](../Page/性交.md "wikilink")，不過也可能是用[人工受精或是用手術方式植入現有的胚胎](../Page/人工受精.md "wikilink")（參見）有關女性生殖系統及性器官的醫學稱為[婦科學](../Page/婦科學.md "wikilink")。

### [跨性別女性](../Page/跨性別女性.md "wikilink")

[出生性別和長大後對自己的](../Page/出生性別.md "wikilink")[性別認同不一定相同](../Page/性別認同.md "wikilink")。[跨性別女性在出生時被視為男性](../Page/跨性別女性.md "wikilink")，或是有男性的身體，但其性別認同為女性。有關此議題有許多不同的社會、法律及個人的定義。

同時具有男性及女性生理或基因特徵的[雙性人](../Page/雙性人.md "wikilink")，可能需要用其他方式確認其性別。嬰兒出生時，會依其生殖器官而賦與一個性別，有時即使嬰兒有XX染色體，但出生時有陰莖，可能會視為男嬰\[9\]。一個帶有男性[基因的](../Page/基因.md "wikilink")[胎兒會先發展出女性的](../Page/胎兒.md "wikilink")[性器官](../Page/性器官.md "wikilink")，直到Y染色體上的[性別決定基因](../Page/SRY基因.md "wikilink")（簡稱SRY）將男性的性徵打開，胎兒的睪丸才開始成長及分泌睪固酮。

雖然[子宮內部是一個充滿](../Page/子宮.md "wikilink")[女性荷爾蒙的環境](../Page/女性荷爾蒙.md "wikilink")，但若果[母親子宮內的女性荷爾蒙過多](../Page/母親.md "wikilink")，會讓[胎兒日後出現女性化的行為](../Page/胎兒.md "wikilink")。不過，決定胎兒從原始的性別轉為男性及女性，仍取決於[遺傳基因](../Page/遺傳基因.md "wikilink")、[心理學](../Page/心理學.md "wikilink")、[社會等多種因素](../Page/社會.md "wikilink")。在她們年少的時侯已表現出來，使用女性的裝扮、女性的髮型，穿著女性的內衣。部分[跨性別女性會選擇](../Page/跨性別女性.md "wikilink")[變性](../Page/變性.md "wikilink")，亦有推出[自傳講述自己的想法](../Page/自傳.md "wikilink")、經歷。

## 健康

在病理學上，除了感染性疾病女性免疫力较高外，大部份病男女感染機率相若。但有一些健康議題特別和女性有關，由於性激素的影響，有一些疾病是女性較容易患上的，如[紅斑性狼瘡](../Page/紅斑性狼瘡.md "wikilink")\[10\]、[類風濕性關節炎](../Page/類風濕性關節炎.md "wikilink")\[11\]。而有些較常出現在女性身上，甚至是只發生在女性身上，例如[乳癌](../Page/乳癌.md "wikilink")、[子宮頸癌](../Page/子宮頸癌.md "wikilink")、[卵巢癌等](../Page/卵巢癌.md "wikilink")。同一種疾病，女性和男性的症狀可能不同，對於醫療後的反應也可能有所不同，這個領域的醫學研究稱為。

[世界衛生組織對於](../Page/世界衛生組織.md "wikilink")[孕產婦死亡的定義是](../Page/孕產婦死亡.md "wikilink")「孕產婦在孕期中死亡，或是孕期結束的42天內死亡，不論其孕期的長短或懷孕的部位，包括任何和懷孕有關，或是分娩管理有關的原因，但不包括意外的原因\[12\]\[13\]。大約99%的孕產婦死亡都是來自開發中國家，其中超過半數都發生在[撒哈拉以南非洲](../Page/撒哈拉以南非洲.md "wikilink")，將近三分之一發生在[南亞](../Page/南亞.md "wikilink")。孕產婦死亡的主要原因是大量的出血（多半在嬰兒出生後）、感染（多半在嬰兒出生後）、[妊娠毒血症](../Page/妊娠毒血症.md "wikilink")、[子癇](../Page/子癇.md "wikilink")、不安全的墮胎或是[瘧疾或](../Page/瘧疾.md "wikilink")[艾滋病分娩的併發症](../Page/艾滋病.md "wikilink")\[14\]。大部份歐洲國家、澳洲、新加坡及日本的孕產婦死亡率都很低，比例最高的是撒哈拉以南非洲的國家。\[15\]。

## 性別角色

[Vietnam_veil.jpg](https://zh.wikipedia.org/wiki/File:Vietnam_veil.jpg "fig:Vietnam_veil.jpg")
女性，是[異性戀與](../Page/異性戀.md "wikilink")[雙性戀](../Page/雙性戀.md "wikilink")[男性以及](../Page/男性.md "wikilink")[同性戀與](../Page/女同性戀.md "wikilink")[雙性戀女性原始慾望追求的對象](../Page/雙性戀.md "wikilink")。情慾本為[物種](../Page/物種.md "wikilink")[繁衍的必經過程](../Page/繁衍.md "wikilink")，中國最早的詩歌文學代表《[詩經](../Page/詩經.md "wikilink")》首篇即歌詠「窈窕淑女，君子好逑。」（《詩經》〈關雎〉）意思是說：體態美好、氣質出眾的女子，是品德高尚男子的好伴侶。

依照[猶太教和](../Page/猶太教.md "wikilink")[基督教](../Page/基督教.md "wikilink")《[聖經](../Page/聖經.md "wikilink")》《[創世紀](../Page/創世紀.md "wikilink")》的說法：上帝創造了第一個男人[亞當後](../Page/亞當.md "wikilink")，為了避免亞當寂寞，便取了亞當的一根[肋骨](../Page/肋骨.md "wikilink")，製造了他的[伴侶](../Page/伴侶.md "wikilink")——第一個女人[夏娃](../Page/夏娃.md "wikilink")。至此男人和女人結合，生全不息通過[性行為](../Page/性行為.md "wikilink")，女性擁有[卵巢](../Page/卵巢.md "wikilink")、[子宮等孕育人類後代的器官](../Page/子宮.md "wikilink")，當[男性和女性進行性行為後](../Page/男性.md "wikilink")，[受精卵在子宮著床](../Page/受精卵.md "wikilink")，經過[細胞](../Page/細胞.md "wikilink")[有絲分裂的過程](../Page/有絲分裂.md "wikilink")，發育成人類[胎兒](../Page/胎兒.md "wikilink")，女性經歷漫長的[十月懷胎](../Page/懷孕.md "wikilink")，胎兒在子宮中慢慢長大，最後，爬出狹窄的[產道](../Page/陰道.md "wikilink")，脫離母體，切斷[臍帶](../Page/臍帶.md "wikilink")，成為完整的[自然人](../Page/自然人.md "wikilink")。因此，女性的性別角色，很難輕易和[母親的角色分離](../Page/母親.md "wikilink")，[十月懷胎的不便](../Page/妊娠.md "wikilink")、[分娩的痛楚](../Page/分娩.md "wikilink")，使她對孩子有一份極難切割的牽連。經歷懷孕也是男性和女性在生理上最大的不同之處。

自從20世紀[女性主義抬頭後](../Page/女性主義.md "wikilink")，以及社會[價值觀的轉變](../Page/價值觀.md "wikilink")，男性和女性的社會地位日趨平等，女性獲得更多平等機會，[離婚率的增加](../Page/離婚率.md "wikilink")，投入職場的女性愈來愈多，抱持[不婚主義的女性也不在少數](../Page/單身.md "wikilink")，女性的性別角色，在當今時代有著愈來愈多元的思維空間。在[性別平等的角度](../Page/性別平等.md "wikilink")，女人不應被視為男人的對立體，女人指的是『生理女性』，[西蒙波娃就曾在](../Page/西蒙波娃.md "wikilink")《[第二性](../Page/第二性.md "wikilink")》中說過「我們不是生來就是女人，我們學會成為女人。」意指女性在社會角色的不平等。[社會的女性是社會建構出來](../Page/社會性別.md "wikilink")。

## 社會問題

在史書中，常有「姦淫」二字出現，[性暴力一直就是嚴重的社會問題](../Page/性暴力.md "wikilink")，[強姦及](../Page/強姦.md "wikilink")[非禮的個案不計其數](../Page/非禮.md "wikilink")，因為男性和女性的生理天生不同，性暴力被認為是體現男性在性別角色優越於女性的權力。因此，主動侵犯對方的大多數是[男性](../Page/男性.md "wikilink")，因素有很多，包括因為生殖機會是一種很[稀缺的](../Page/稀缺性.md "wikilink")[資源](../Page/資源.md "wikilink")，而強姦不必付出[金錢](../Page/金錢.md "wikilink")，以及男性在向女性求偶時通常所需要的[前戲時間](../Page/前戲.md "wikilink")。強姦可增加異性交媾的數量，以有更多機會產出自己血緣的後代。現在，很多國家都在強烈打擊性侵犯的行為，但還是常有[性侵犯的個案發生](../Page/性侵犯.md "wikilink")。

## 女性與社會文化

在人類社會若干的文化中，歷史上女性一直是受輕視和壓制的一群。社會大部分的權力都集中在男性身上（亦即所謂[男性沙文主義](../Page/男性沙文主義.md "wikilink")），女性沒有得到應有尊重的身份、地位和權利。\[16\]

在古老的[中國](../Page/中國.md "wikilink")，《[禮記](../Page/禮記.md "wikilink")》等書籍包含很多生活方式的細節供百姓參考，[三從四德是](../Page/三從四德.md "wikilink")[宋](../Page/宋朝.md "wikilink")[明開始的女子行為規範](../Page/明朝.md "wikilink")，受[宋明理學的影響](../Page/宋明理學.md "wikilink")，讓女性承擔了過多苛刻的要求。

在近代的教育和[女權分子的努力下](../Page/女性主義.md "wikilink")，女性的社會權利在大多數國家已獲得基本上的保護和平等。[性別平等在](../Page/性別平等.md "wikilink")《[联合国宪章](../Page/联合国宪章.md "wikilink")》序言中与基本[人权](../Page/人权.md "wikilink")、人格尊严与价值、大小各国平等被并列重申。

不過，基於宗教的教義，若干宗教均壓制女性。[伊斯蘭教教義則強調男女兩性應當按實體的表現功能給予不同的權利和權力](../Page/伊斯蘭教.md "wikilink")，以保護男女兩性生理上賦予的生存尊嚴與精神尊嚴，例如：女性[穆斯林或在伊斯蘭教國家的所有女性](../Page/穆斯林.md "wikilink")，出行服飾均遮蓋住頭髮、雙手和雙腿，甚至連臉孔和眼晴也蓋住，務求減少男性人員生慾念而使自身被侵犯的機會；另外在涉及女性人員被強暴的罪案審判中，每兩位女性證人的供證效力僅等於一位[法人的供證效力](../Page/法人.md "wikilink")，以減少女性人員因情緒較易動蕩而使供詞較易失準的機會。

性別學家，也提出"女人是複數的（women are
many）"這個觀點，女性不應該被視為一個"整體"，而為複雜個體的集合，因此也引發白人研究的學派興起，重新建構[本位主義思考脈絡](../Page/本位主义.md "wikilink")。

[File:Woman.svg|被送到外太空的](File:Woman.svg%7C被送到外太空的)[先鋒十號所攜載之](../Page/先鋒十號.md "wikilink")[鋁質鍍](../Page/鋁.md "wikilink")[金板上的女性圖像](../Page/金.md "wikilink")
<File:Chinese> beautiful girl 2.jpg|[黃種女性](../Page/黃種人.md "wikilink")
<File:Julia> Zange.jpg|[白種女性](../Page/白種人.md "wikilink")
<File:William-Adolphe> Bouguereau (1825-1905) - Bather
(1870).jpg|[威廉·阿道夫·布格羅的裸女作品](../Page/威廉·阿道夫·布格羅.md "wikilink")
<File:Wongwt> 官也街 (16664268454).jpg|穿着[裙子的女士](../Page/裙子.md "wikilink")

## 黃道十二宮

  - [處女宮](../Page/處女宮.md "wikilink") 

## 參考文獻

## 外部連結

  - [FemBio - Notable Women Internationall](http://www.fembio.org/)

## 参见

  - [女孩](../Page/女孩.md "wikilink")
  - [雌性](../Page/雌性.md "wikilink")
  - [男性](../Page/男性.md "wikilink")
  - [姑娘](../Page/姑娘.md "wikilink")
  - [小姐](../Page/小姐.md "wikilink")
  - [女士](../Page/女士.md "wikilink")
  - [跨性女](../Page/跨性女.md "wikilink")
  - [女性人物列表](../Category/女性人物列表.md "wikilink")

**醫學：**

  -
  - [分娩](../Page/分娩.md "wikilink")

  -
  - [產科學](../Page/產科學.md "wikilink")

  -
**社會學：**

  - [女性化](../Page/女性化.md "wikilink")

  - [母權](../Page/母權.md "wikilink")

  - [厌女症](../Page/厌女症.md "wikilink")

  - [粒線體夏娃](../Page/粒線體夏娃.md "wikilink")

  - [性別歧視主義](../Page/性別歧視主義.md "wikilink")

  - [科學領域中的女性](../Page/科學領域中的女性.md "wikilink")

  -
**政治：**

  - [女性主義](../Page/女性主義.md "wikilink")
  - [性別研究](../Page/性別研究.md "wikilink")

[女性](../Category/女性.md "wikilink")
[Category:性別](../Category/性別.md "wikilink")

1.
2.
3.  2001 World Almanac
4.  (Tanner, 1990).
5.
6.
7.
8.  許多雙性人或跨性別人，以及[原發性閉經的女性都沒有初潮及月經](../Page/閉經.md "wikilink")
9.  Fausto-Sterling, Anne “Of Gender and Genitals” from Sexing the body:
    gender politics and the construction of sexuality New York, NY:
    Basic Books, 2000, \[Chapter 3, pp. 44-77\]
10.
11.
12.
13.
14.
15.
16.