|                     |                     |                                       |                        |                               |                            |                              |                           |
| ------------------- | ------------------- | ------------------------------------- | ---------------------- | ----------------------------- | -------------------------- | ---------------------------- | ------------------------- |
| {{Infobox Biography | subject_name = 杨格非 | image_name = Griffith-John-China.jpg | image_caption = 来华传教士 | date_of_birth = 1831年12月14日 | place_of_birth = 南威尔士斯温西 | date_of_death = 1912年7月25日 | place_of_death = 英格兰 }} |
|                     |                     |                                       |                        |                               |                            |                              |                           |

**杨格非**（**格里菲斯·约翰**，****，）又译杨约翰、杨笃信，[英国](../Page/英国.md "wikilink")[伦敦会著名的来华](../Page/伦敦会.md "wikilink")[传教士之一](../Page/传教士.md "wikilink")，[中国](../Page/中国.md "wikilink")[华中地区](../Page/华中.md "wikilink")[基督教事业的开创者](../Page/基督教.md "wikilink")。

1831年12月14日，杨格非出生于[英国威尔士斯温西一个](../Page/英国.md "wikilink")[公理会基督徒的家庭](../Page/公理会.md "wikilink")，家境比较贫穷。在8岁时被公理会教堂接纳为正式成员，14岁时在一次祷告聚会中进行了第一次讲道，16岁就开始正式在教堂里定期讲道。随后进入威尔士和英格兰学习神学。

1850年自学考入大学，后来学习神学。1853年，杨格非加入了最早来华的[伦敦会](../Page/伦敦会.md "wikilink")。1855年被按立为[牧师](../Page/牧师.md "wikilink")，同年，他和一位传教士的女儿Jane
Griffith结婚。他准备前往[马达加斯加传教](../Page/马达加斯加.md "wikilink")，但是[伦敦会](../Page/伦敦会.md "wikilink")[差会说服了他](../Page/差会.md "wikilink")，前往中国传教。

1855年9月，新婚夫妇航海到达中国，先在[上海附近活动](../Page/上海.md "wikilink")。1857年进入太平天国辖区考察，1860年8月到[苏州会见](../Page/苏州.md "wikilink")[太平天国忠王](../Page/太平天国.md "wikilink")[李秀成](../Page/李秀成.md "wikilink")，后又见干王[洪仁玕](../Page/洪仁玕.md "wikilink")，然后前往南京，获得在太平天国境内自由传教的权利\[1\]。

1860年，[北京条约签订](../Page/北京条约.md "wikilink")，规定中国内地（通商口岸以外的地方）向传教士开放。杨格非是最早进入中国内地省份的传教士之一。1861年，他从上海出发，6月21日，来到[汉口](../Page/汉口.md "wikilink")，成为第一个进入华中地区的基督教传教士，在沈家庙金庭公店的住宅开展布道工作。此后直到1912年离开，汉口一直是他的工作基地。他在这里创办了学校、医院和训练中心。只是在1863年住在武昌，而1867年住在汉阳。

1862年，英国[循道会传教士](../Page/循道会.md "wikilink")[郭修理自](../Page/郭修理.md "wikilink")[广东来汉](../Page/广东.md "wikilink")，杨格非将金庭公店房屋以及[汉正街一带让给循道会布道](../Page/汉正街.md "wikilink")，自己去下游的[花楼街购地](../Page/花楼街.md "wikilink")，建造花楼总堂（后来迁往模范区，名[格非堂](../Page/格非堂.md "wikilink")，即今日黄石路口的荣光堂），创办仁济医院（汉口协和医院的前身）。1863年，在汉口夹街建造华中第一所教堂——首恩堂。1864年7月，在当时尚未列为商埠的[武昌戈甲营建造了](../Page/武昌.md "wikilink")[湖北省城的第一所教堂](../Page/湖北.md "wikilink")（崇真堂，2000年恢复使用），在附近的[昙华林](../Page/昙华林.md "wikilink")，也建造了一所仁济医院。不久，又先后在[孝感](../Page/孝感.md "wikilink")、[天门](../Page/天门.md "wikilink")、皂市以及黄陂、汉阳等地建立教堂。他和他的同事在湖北省建立了至少100个传教站,被称为“街头布道家”。

杨格非以在广阔的中国内地旅行布道而知名，有时旅行距离远达3000英里以上。他是第一个进入湖北、[湖南](../Page/湖南.md "wikilink")、[四川](../Page/四川.md "wikilink")3个省份的新教传教士。

杨格非对于中国教会的另一项主要贡献是写作和翻译。杨格非通过努力，能够熟练地使用中文，说、写都非常流利。他雄辩的口才，使得他在中国人中深受欢迎，吸引了大批群众来听他的讲道。1876年，杨格非在汉口创办了华中圣教书会，专门编印布道单张与小册子，供外出传教散发，以补外国传教士运用汉语不便之弱点（据1899年统计，全国基督教所用的街头布道文字材料80％以上为该会印刷）。杨格非不仅多年担任圣教书会的负责人，他自己也是一个多产的小册子作者，写作了难以计数的广受欢迎的福音小册子。他还从事[圣经的翻译工作](../Page/圣经.md "wikilink")，曾经将[新约和部分旧约翻译成一种以上的中国方言](../Page/新约.md "wikilink")，又将[新约](../Page/新约.md "wikilink")、[诗篇和](../Page/诗篇.md "wikilink")[箴言翻译成](../Page/箴言.md "wikilink")[浅文理译本](../Page/杨格非文理新约.md "wikilink")，1885年出版。他也曾翻译了官话新约，在1889年出版。杨格非在训练和指导众多的中国传道人方面做得特别成功，他在[长江流域创办了一所以他名字命名的培养本地传道人的神学院](../Page/长江流域.md "wikilink")。1899年，他在汉口后花楼居巷（今交通巷）创办博学书院（英文校名：Griffith
John College 杨格非学院，[武汉市第四中学的前身](../Page/武汉市第四中学.md "wikilink")）。

1905年在主日讲道时突然中风，偏瘫卧床。1911年10月武昌首义爆发，由其女婿施伯珩等护送乘英轮离开汉口，1912年1月返回英国。这时杨格非已经在中国定居了55年，其间回国只有3次。

1889年，杨格非被选为[英格兰与威尔士公理会全国协会主席](../Page/英格兰与威尔士公理会全国协会.md "wikilink")，但他拒绝了这个荣誉，继续留在汉口，住在他所热爱的中国人中间。同年，出于对他在中国传教工作的赏识，[爱丁堡大学授予他神学博士学位](../Page/爱丁堡大学.md "wikilink")。

## 参考

  - R. Wardlaw Thompson，杨格非：the story of fifty years in China, London,
    1906
  - Noel Gibbard，杨格非：华中的使徒，1998年
  - [戴德生](../Page/戴德生.md "wikilink") & China’s Open Century Volume Six:
    Assault On The Nine; [Alfred James
    Broomhall](../Page/Alfred_James_Broomhall.md "wikilink")；Hodder and
    Stoughton and Overseas Missionary Fellowship, 1988

## 參考資料

<div class="references-small">

<references />

</div>

[Y](../Category/在华基督教传教士.md "wikilink")
[Y](../Category/英國基督徒.md "wikilink")
[Y](../Category/1831年出生.md "wikilink")
[Y](../Category/1912年逝世.md "wikilink")
[Category:武汉宗教](../Category/武汉宗教.md "wikilink")

1.  [基督敎敎育与中国社会变迁](http://books.google.cn/books?id=Su-N4vpLvRQC&pg=PA82&lpg=PA82&dq=%E6%9D%A8%E6%A0%BC%E9%9D%9E%E8%BF%9B%E5%85%A5%E5%A4%AA%E5%B9%B3%E5%A4%A9%E5%9B%BD&source=bl&ots=-SIY34yPr8&sig=bCvr0JDMX5kMG71o1JHQCD5RJ0E&hl=zh-CN&ei=FyCsScazJ8-_kAXypbHXDQ&sa=X&oi=book_result&resnum=5&ct=result#PPA83,M1)