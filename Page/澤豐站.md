**澤豐站**（[英文](../Page/英文.md "wikilink")：**Affluence
Stop**）是[輕鐵車站](../Page/香港輕鐵.md "wikilink")，代號080，屬於單程車票[第2收費區](../Page/輕鐵第2收費區.md "wikilink")（北行方向是最後一個屬單程車票第2收費區的車站），共有2個月台，共有2條[輕鐵路線途經此站](../Page/香港輕鐵.md "wikilink")。車站位於[屯門河西面](../Page/屯門河.md "wikilink")，在[澤豐花園對出](../Page/澤豐花園.md "wikilink")。

## 車站結構

### 車站樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td></td>
<td><p><a href="../Page/澤豐花園.md" title="wikilink">澤豐花園</a></p></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p> 往<a href="../Page/天逸站.md" title="wikilink">天逸</a>（<a href="../Page/屯門醫院站.md" title="wikilink">屯門醫院</a>）|}}</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p> 往<a href="../Page/屯門碼頭站.md" title="wikilink">屯門碼頭</a>（<a href="../Page/銀圍站.md" title="wikilink">銀圍</a>） {{!}}  往<a href="../Page/友愛站.md" title="wikilink">友愛</a>　（<a href="../Page/蔡意橋站.md" title="wikilink">蔡意橋</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/屯門河.md" title="wikilink">屯門河</a>、<a href="../Page/屯門河畔公園.md" title="wikilink">屯門河畔公園</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 車站周邊

  - [澤豐花園](../Page/澤豐花園.md "wikilink")
  - 澤豐花園商場
  - 澤豐花園街市
  - [屯門河畔公園](../Page/屯門河畔公園.md "wikilink")
  - [屯門鄧肇堅運動場](../Page/屯門鄧肇堅運動場.md "wikilink")
  - [屯門醫院宿舍](../Page/屯門醫院.md "wikilink")
  - [屯門消防宿舍](../Page/屯門消防宿舍.md "wikilink")
  - [中華基督教會譚李麗芬紀念中學](../Page/中華基督教會譚李麗芬紀念中學.md "wikilink")

## 鄰近車站

## 歷史

原稱**河景站**，後因旁邊興建[澤豐花園而改為現名](../Page/澤豐花園.md "wikilink")。

## 外部連結

  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵街道圖](http://mtr.com.hk/archive/ch/services/maps/06.gif)
  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵行車時間表](http://www.mtr.com.hk/chi/lr_bus/schedule/schedule_index.html)

[Category:大興 (香港)](../Category/大興_\(香港\).md "wikilink")
[Category:以屋苑命名的香港輕鐵車站](../Category/以屋苑命名的香港輕鐵車站.md "wikilink")
[Category:1988年启用的铁路车站](../Category/1988年启用的铁路车站.md "wikilink")
[Category:屯門區鐵路車站](../Category/屯門區鐵路車站.md "wikilink")