[Sea_Salt_(Okinawa).JPG](https://zh.wikipedia.org/wiki/File:Sea_Salt_\(Okinawa\).JPG "fig:Sea_Salt_(Okinawa).JPG")的食用海鹽\]\]
**海盐**为[食用盐的一种](../Page/食用盐.md "wikilink")，通过晒干海水以获得海水中的盐分。可以用于烹调和化妆。虽然很多精盐也来自海盐，但并未表明为海盐，颗粒细小。市售海盐一般颗粒较大，价钱比一般的食盐要贵很多。和精盐相比，海盐的主要成分也是[氯化钠](../Page/氯化钠.md "wikilink")，并没有证据显示其对人体有特殊的好处。而且海盐中可能缺[碘](../Page/碘.md "wikilink")，所以不宜完全取代碘盐。

## 成分

海水鹽離子列表，單位是重量百分比：\[1\]

|                                                                 |        |
| --------------------------------------------------------------- | ------ |
| [氯離子](../Page/氯離子.md "wikilink")（Cl<sup>-</sup>）                | 55.03% |
| [鈉離子](../Page/鈉離子.md "wikilink")（Na<sup>+</sup>）                | 30.59% |
| [硫酸根](../Page/硫酸根.md "wikilink")（SO<sub>4</sub><sup>2-</sup>）   | 7.68%  |
| [鎂離子](../Page/鎂.md "wikilink")（Mg<sup>2+</sup>）                 | 3.68%  |
| [鈣離子](../Page/鈣.md "wikilink")（Ca<sup>2+</sup>）                 | 1.18%  |
| [鉀離子](../Page/鉀離子.md "wikilink")（K<sup>+</sup>）                 | 1.11%  |
| [碳酸氫根](../Page/碳酸氫根.md "wikilink")（HCO<sub>3</sub><sup>-</sup>） | 0.41%  |
| [溴離子](../Page/溴離子.md "wikilink")（Br<sup>-</sup>）                | 0.19%  |
| [硼酸根](../Page/硼酸根.md "wikilink")（BO<sub>3</sub><sup>3-</sup>）   | 0.08%  |
| [鍶離子](../Page/鍶.md "wikilink")（Sr<sup>2+</sup>）                 | 0.04%  |
| 其他                                                              | 0.01%  |

雖然全球各海域有不同的鹹度，但生產的海鹽具有相同的離子組成。

## 相关

  - [食用盐](../Page/食用盐.md "wikilink")

## 參考資料

[Category:食鹽](../Category/食鹽.md "wikilink")
[Category:調味料](../Category/調味料.md "wikilink")

1.  [The chemical composition of
    seawater](http://www.seafriends.org.nz/oceano/seawater.htm)