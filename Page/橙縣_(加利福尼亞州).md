**橘縣**（**Orange
County**），中文又音譯為**奧蘭治縣**，是位於[美國](../Page/美國.md "wikilink")[加利福尼亞州南部](../Page/加利福尼亞州.md "wikilink")、相對富裕的一個郡，西瀕[太平洋](../Page/太平洋.md "wikilink")，位處[洛杉磯郡的東南方](../Page/洛杉磯郡.md "wikilink")，面積2,455平方公里，屬於「大洛杉磯地區」的一部分。郡內許多小城市多為新興規劃之市郊型住宅區，因此吸引不少中上階層家庭定居於此，根據2010年全美人口普查結果，該郡共有3,010,232人，是加州人口第三多的郡，僅次於北鄰的[洛杉磯郡與南鄰的](../Page/洛杉磯郡.md "wikilink")[聖地牙哥郡](../Page/聖地牙哥郡.md "wikilink")\[1\]。橙縣亦是美國人口第六多的縣份。郡政府位於[聖塔安娜市](../Page/圣安娜_\(加利福尼亚州\).md "wikilink")（Santa
Ana）。

## 歷史

[OrangeCountyCA_Map.gif](https://zh.wikipedia.org/wiki/File:OrangeCountyCA_Map.gif "fig:OrangeCountyCA_Map.gif")
[Aerial_view_of_central_Orange_County_overlooking_South_Coast_Metro,_John_Wayne_Airport,_and_the_Irvine_business_district.JPG](https://zh.wikipedia.org/wiki/File:Aerial_view_of_central_Orange_County_overlooking_South_Coast_Metro,_John_Wayne_Airport,_and_the_Irvine_business_district.JPG "fig:Aerial_view_of_central_Orange_County_overlooking_South_Coast_Metro,_John_Wayne_Airport,_and_the_Irvine_business_district.JPG")\]\]

橘郡成立於1889年3月11日，名稱來自當時的主要農作物—[橘子](../Page/橘子.md "wikilink")。[洛杉磯市郊的](../Page/洛杉磯.md "wikilink")[加州迪士尼樂園即位於橘郡的第二大城](../Page/迪士尼樂園.md "wikilink")[安納海姆市](../Page/安納罕.md "wikilink")（Anaheim，該市亦是[美國職棒大聯盟](../Page/美國職棒大聯盟.md "wikilink")[洛杉磯天使主場所在地](../Page/洛杉磯天使.md "wikilink")），而郡內的[華裔人口則多聚居於橘郡中部](../Page/華裔美國人.md "wikilink")、富裕的新興規劃城市—[爾灣市](../Page/爾灣市.md "wikilink")（Irvine）。

除了華裔之外，橘郡尚聚居了其他的亞裔，包括北部的[富樂頓市](../Page/富勒頓_\(加利福尼亞州\).md "wikilink")（Fullerton）、[普安那公園市](../Page/普安那公園_\(加利福尼亞州\).md "wikilink")（Buena
Park）一帶有相當多的[韓裔](../Page/韓裔美國人.md "wikilink")；而西北部的[西敏市](../Page/威斯敏斯特_\(加利福尼亞州\).md "wikilink")（Westminster）、[園林市](../Page/園林市.md "wikilink")（Garden
Grove）附近則有[越南本土以外最大的](../Page/越南.md "wikilink")[越裔聚居地](../Page/越僑.md "wikilink")\[2\]，該區素有「[小西貢](../Page/小西貢.md "wikilink")」（Little
Saigon）之稱。橘郡也有為數不少的[日裔和](../Page/日裔美國人.md "wikilink")[印度裔居民](../Page/印度裔美國人.md "wikilink")，惟其居住相對分散。

## 地理

### 城市

1.  [亞里索維耶霍／亞里索維耶荷](../Page/亞里索維耶荷_\(加利福尼亞州\).md "wikilink")（Aliso
    Viejo）
2.  [安那翰／安納海姆](../Page/安那翰_\(加利福尼亞州\).md "wikilink")（Anaheim）
3.  [布雷亞](../Page/布雷亞_\(加利福尼亞州\).md "wikilink")（Brea）
4.  [普安那公園](../Page/普安那公園_\(加利福尼亞州\).md "wikilink")（Buena Park）
5.  [科斯塔梅薩](../Page/科斯塔梅薩.md "wikilink")（Costa Mesa）
6.  [赛普里斯／柏市](../Page/賽普里斯_\(加利福尼亞州\).md "wikilink")（Cypress）
7.  [達納角](../Page/達納角_\(加利福尼亞州\).md "wikilink")（Dana Point）
8.  [芳泉谷](../Page/芳泉谷_\(加利福尼亞州\).md "wikilink")（Fountain Valley）
9.  [富勒頓／富樂敦](../Page/富勒頓_\(加利福尼亞州\).md "wikilink")（Fullerton）
10. [加登格羅夫／園林市](../Page/加登格羅夫_\(加利福尼亞州\).md "wikilink")（Garden Grove）
11. [杭亭頓灘](../Page/亨廷頓比奇.md "wikilink")（Huntington Beach）
12. [爾灣／欧文](../Page/爾灣_\(加利福尼亞州\).md "wikilink")（Irvine）
13. [拉哈布拉](../Page/拉哈布拉_\(加利福尼亞州\).md "wikilink")（La Habra）
14. [拉帕爾馬](../Page/拉帕爾馬_\(加利福尼亞州\).md "wikilink")（La Palma）
15. [拉古納灘](../Page/拉古納海灘_\(加利福尼亞州\).md "wikilink")（Laguna Beach）
16. [拉古納山](../Page/拉古納山_\(加利福尼亞州\).md "wikilink")（Laguna Hills）
17. [拉古納湖](../Page/尼古湖_\(加利福尼亞州\).md "wikilink")（Laguna Niguel）
18. [拉古納林](../Page/拉古納伍茲_\(加利福尼亞州\).md "wikilink")（Laguna Woods）
19. [森林湖](../Page/森林湖_\(加利福尼亞州\).md "wikilink")（Lake Forest）
20. [洛斯阿拉米托斯](../Page/洛斯阿拉米托斯_\(加利福尼亞州\).md "wikilink")（Los Alamitos）
21. [米申維耶霍／米申維耶荷](../Page/米申維耶霍_\(加利福尼亞州\).md "wikilink")（Mission Viejo）
22. [新港灘／纽波特灘](../Page/新港滩.md "wikilink")（Newport Beach）
23. [橙市／奧蘭治](../Page/橙市_\(加利福尼亞州\).md "wikilink")（Orange）
24. [普拉森](../Page/普拉森_\(加利福尼亞州\).md "wikilink")（Placentia）
25. [蘭喬聖瑪格麗塔／蘭喬聖塔瑪格麗塔](../Page/蘭喬聖瑪格麗塔.md "wikilink")（Rancho Santa
    Margarita）
26. [聖克萊門特](../Page/聖克萊門特_\(加利福尼亞州\).md "wikilink")（San Clemente）
27. [聖胡安-卡皮斯特拉諾](../Page/聖胡安_-_卡皮斯特拉諾_\(加利福尼亞州\).md "wikilink")（San Juan
    Capistrano）
28. [聖安娜／聖塔安娜](../Page/聖安娜_\(加利福尼亞州\).md "wikilink")（Santa Ana）
29. [海豹灘](../Page/海豹灘_\(加利福尼亞州\).md "wikilink")（Seal Beach）
30. [斯坦頓](../Page/斯坦頓_\(加利福尼亞州\).md "wikilink")（Stanton）
31. [塔斯廷](../Page/塔斯廷.md "wikilink")（Tustin）
32. [維拉公園](../Page/維拉公園_\(加利福尼亞州\).md "wikilink")（Villa Park）
33. [威斯敏斯特／西敏市](../Page/威斯敏斯特_\(加利福尼亞州\).md "wikilink")（Westminster）
34. [約巴林達](../Page/約巴林達.md "wikilink")（Yorba Linda）

## 政治

橘郡相較於鄰近的洛杉磯，被認為是全加州傾向[共和黨最明顯的郡之一](../Page/共和黨_\(美國\).md "wikilink")。由近幾十年地方或聯邦選舉來看（詳見表單），大部分的投票結果皆由共和黨籍候選人勝出，而在2016年總統選舉之前只有[1932年](../Page/1932年美國總統選舉.md "wikilink")\[3\]和[1936年](../Page/1936年美國總統選舉.md "wikilink")\[4\]，[罗斯福參與的美國總統選舉中由民主黨勝出](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")（目前該郡七位[聯邦眾議員中有四位是共和黨人](../Page/美國眾議院.md "wikilink")）\[5\]。一般認為，這樣的政治傾向來自於當地居民在政治、經濟和社會議題上的[保守態度](../Page/保守主義.md "wikilink")，而這樣的態度正是共和黨傳統所持的立場。惟近年來不少[拉丁裔和](../Page/拉丁裔美國人.md "wikilink")[亞裔移居至此](../Page/亞裔美國人.md "wikilink")，而其政治立場多傾向更加強調平權、社會福利、照顧少數族裔的民主黨，因此共和黨在橘郡的優勢有衰退趨勢。[2016年美國總統选举](../Page/2016年美國總統选举.md "wikilink")，民主黨更自1936年後首次在橘郡得到過半數選票。

1994年橘郡因市政府財務主管投資[衍生性金融商品而發生大量虧損](../Page/衍生性金融商品.md "wikilink")，橘郡政府只好宣布[破產](../Page/破產.md "wikilink")，並以裁員和縮減市政服務來償還債務。\[6\]

另外，橙縣的居民有時會被媒體統稱為「橙縣人」（Orange Countians）。\[7\]

| 年份                                                                        | [共和黨](../Page/共和黨_\(美國\).md "wikilink")        | [民主黨](../Page/民主黨_\(美國\).md "wikilink")        | 其他              |
| ------------------------------------------------------------------------- | ---------------------------------------------- | ---------------------------------------------- | --------------- |
| style="text-align:center; |**[2016年](../Page/2016年美國總統選舉.md "wikilink")** | style="text-align:center; |42.3% *507,148*     | style="text-align:center; |**50.9%** *609,961* | 6.7% *80,412*   |
| style="text-align:center; |**[2012年](../Page/2012年美國總統選舉.md "wikilink")** | style="text-align:center; |**51.9%** *582,332* | style="text-align:center; |45.8% *512,440*     | 2.5% *25,271*   |
| style="text-align:center; |**[2008年](../Page/2008年美國總統選舉.md "wikilink")** | style="text-align:center; |**50.4%** *578,171* | style="text-align:center; |47.8% *548,246*     | 1.8% *21,530*   |
| style="text-align:center; |**[2004年](../Page/2004年美國總統選舉.md "wikilink")** | style="text-align:center; |**59.7%** *641,832* | style="text-align:center; |39.0% *419,239*     | 1.3% *14,328*   |
| style="text-align:center; |**[2000年](../Page/2000年美國總統選舉.md "wikilink")** | style="text-align:center; |**55.8%** *541,299* | style="text-align:center; |40.4% *391,819*     | 3.9% *37,787*   |
| style="text-align:center; |**[1996年](../Page/1996年美國總統選舉.md "wikilink")** | style="text-align:center; |**51.7%** *446,717* | style="text-align:center; |37.9% *327,485*     | 10.5% *90,374*  |
| style="text-align:center; |**[1992年](../Page/1992年美國總統選舉.md "wikilink")** | style="text-align:center; |**43.9%** *426,613* | style="text-align:center; |31.6% *306,930*     | 24.6% *239,006* |
| style="text-align:center; |**[1988年](../Page/1988年美國總統選舉.md "wikilink")** | style="text-align:center; |**67.7%** *586,230* | style="text-align:center; |31.1% *269,013*     | 1.2% *10,064*   |
| style="text-align:center; |**[1984年](../Page/1984年美國總統選舉.md "wikilink")** | style="text-align:center; |**74.7%** *635,013* | style="text-align:center; |24.3% *206,272*     | 1.0% *8,792*    |
| style="text-align:center; |**[1980年](../Page/1980年美國總統選舉.md "wikilink")** | style="text-align:center; |**67.9%** *529,797* | style="text-align:center; |22.6% *176,704*     | 9.5% *73,711*   |
| style="text-align:center; |**[1976年](../Page/1976年美國總統選舉.md "wikilink")** | style="text-align:center; |**62.2%** *408,632* | style="text-align:center; |35.3% *232,246*     | 2.5% *16,555*   |
| style="text-align:center; |**[1972年](../Page/1972年美國總統選舉.md "wikilink")** | style="text-align:center; |**68.3%** *448,291* | style="text-align:center; |26.9% *176,847*     | 4.8% *31,515*   |
| style="text-align:center; |**[1968年](../Page/1968年美國總統選舉.md "wikilink")** | style="text-align:center; |**63.1%** *314,905* | style="text-align:center; |29.9% *148,869*     | 7.0% *34,933*   |
| style="text-align:center; |**[1964年](../Page/1964年美國總統選舉.md "wikilink")** | style="text-align:center; |**55.9%** *224,196* | style="text-align:center; |44.0% *176,539*     | 0.1% *430*      |
| style="text-align:center; |**[1960年](../Page/1960年美國總統選舉.md "wikilink")** | style="text-align:center; |**60.8%** *174,891* | style="text-align:center; |38.9% *112,007*     | 0.2% *701*      |
| style="text-align:center; |**[1956年](../Page/1956年美國總統選舉.md "wikilink")** | style="text-align:center; |**66.8%** *113,510* | style="text-align:center; |32.3% *54,895*      | 0.9% *1,474*    |
| style="text-align:center; |**[1952年](../Page/1952年美國總統選舉.md "wikilink")** | style="text-align:center; |**70.3%** *80,994*  | style="text-align:center; |29.0% *33,397*      | 0.7% *844*      |
| style="text-align:center; |**[1948年](../Page/1948年美國總統選舉.md "wikilink")** | style="text-align:center; |**60.9%** *48,587*  | style="text-align:center; |36.4% *29,018*      | 2.8% *2,209*    |
| style="text-align:center; |**[1944年](../Page/1944年美國總統選舉.md "wikilink")** | style="text-align:center; |**56.9%** *38,394*  | style="text-align:center; |42.5% *28,649*      | 0.6% *407*      |
| style="text-align:center; |**[1940年](../Page/1940年美國總統選舉.md "wikilink")** | style="text-align:center; |**55.5%** *36,070*  | style="text-align:center; |43.4% *28,236*      | 1.1% *691*      |
| style="text-align:center; |**[1936年](../Page/1936年美國總統選舉.md "wikilink")** | style="text-align:center; |43.3% *23,494*      | style="text-align:center; |**55.0%** *29,836*  | 1.7% *921*      |
| style="text-align:center; |**[1932年](../Page/1932年美國總統選舉.md "wikilink")** | style="text-align:center; |45.9% *22,623*      | style="text-align:center; |**48.4%** *23,835*  | 5.7% *2,818*    |
| style="text-align:center; |**[1928年](../Page/1928年美國總統選舉.md "wikilink")** | style="text-align:center; |**79.4%** *30,572*  | style="text-align:center; |19.8% *7,611*       | 0.9% *344*      |
| style="text-align:center; |**[1924年](../Page/1924年美國總統選舉.md "wikilink")** | style="text-align:center; |**67.4%** *19,913*  | style="text-align:center; |8.7% *2,565*        | 24.0% *7,088*   |
| style="text-align:center; |**[1920年](../Page/1920年美國總統選舉.md "wikilink")** | style="text-align:center; |**71.5%** *12,797*  | style="text-align:center; |19.6% *3,502*       | 8.9% *1,594*    |

**橙縣於各屆總統選舉中的投票統計**

[第115届美国国会期间](../Page/第115届美国国会.md "wikilink")，在[众议院中橙县被分为以下](../Page/United_States_House_of_Representatives.md "wikilink")7个国会选区:\[8\]

  - ：由民主党人代表

  - ：由共和党人[埃德·羅伊斯代表](../Page/埃德·羅伊斯.md "wikilink")

  - ：由共和党人[米米·沃尔特斯代表](../Page/米米·沃尔特斯.md "wikilink")

  - ：由民主党人代表

  - ：由民主党人代表

  - ：由共和党人[达纳·罗拉巴克代表](../Page/达纳·罗拉巴克.md "wikilink")，及

  - ：由共和党人[達雷爾·伊薩代表](../Page/達雷爾·伊薩.md "wikilink")。

在[2018年中期选举后所有共和党人均退休或败选](../Page/2018年美国众议院选举.md "wikilink")。[第116届美国国会中橙县的众议院代表如下](../Page/第116届美国国会.md "wikilink")：\[9\]

  - ：由民主党人代表

  - ：由民主党人代表

  - ：由民主党人代表

  - ：由民主党人代表

  - ：由民主党人代表

  - ：由民主党人代表，及

  - ：由民主党人代表。

## 人口

### 2010年

根據[2010年人口普查](../Page/2010年美國人口普查.md "wikilink")，橙縣擁有3,010,232居民。橙縣的人口是由1,830,758（60.8%）[白人](../Page/歐裔美國人.md "wikilink")、67,708（2.2%）[黑人](../Page/非裔美國人.md "wikilink")、18,132（0.6%）[原住民](../Page/美國原住民.md "wikilink")、537,804（17.9%）[亞洲人](../Page/亞裔美國人.md "wikilink")、9,354（0.3%）[大洋洲人](../Page/大洋洲.md "wikilink")、435,641（14.5%）其他[種族和](../Page/種族.md "wikilink")127,799（4.2%）[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。聖貝納迪諾縣總共擁有1,012,973（33.7%[西班牙裔和](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人](../Page/拉丁美洲人.md "wikilink")。\[10\]

在西班牙裔和拉美裔人口中，有28.5%是[墨西哥人](../Page/墨西哥人.md "wikilink")、0.8%是[薩爾瓦多人](../Page/薩爾瓦多人.md "wikilink")、0.5%是[危地馬拉人](../Page/危地馬拉人.md "wikilink")、0.4%是[波多黎各人](../Page/波多黎各人.md "wikilink")、0.3%是[古巴人](../Page/古巴人.md "wikilink")、0.3%是[哥倫比亞人以及](../Page/哥倫比亞人.md "wikilink")0.3%是[秘魯人](../Page/秘魯人.md "wikilink")。\[11\]

在亞裔人口中，有6.1%為[越南人](../Page/越南人.md "wikilink")、2.9%為[韓國人](../Page/韓國人.md "wikilink")、2.7%為[中國人](../Page/中國人.md "wikilink")、2.4%為[菲律賓人](../Page/菲律賓人.md "wikilink")、1.4%為[印度人](../Page/印度人.md "wikilink")、1.1%為[日本人](../Page/日本人.md "wikilink")、0.2%為[柬埔寨人](../Page/柬埔寨人.md "wikilink")、0.2%為[巴基斯坦人](../Page/巴基斯坦人.md "wikilink")、0.1%)為[泰國人](../Page/泰國人.md "wikilink")、0.1%為[印尼人](../Page/印尼人.md "wikilink")、以及0.1%為[寮國人](../Page/寮國人.md "wikilink")。\[12\]

### 2000年

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，橙縣擁有2,846,289居民、935,287住戶和667,7946家庭。\[13\]。其[人口密度為每平方英里](../Page/人口密度.md "wikilink")1,392居民（每平方公里3,606居民），是加州人口最集中的縣份。橙縣擁有969,484間房屋单位，其密度為每平方英里474間（每平方公里1,228間）。橙縣的人口是由64.8%
[白人](../Page/歐裔美國人.md "wikilink")、13.6%[亞洲人](../Page/亞裔美國人.md "wikilink")、1.7%[黑人](../Page/非裔美國人.md "wikilink")、0.7%[土著](../Page/美國土著.md "wikilink")、0.3%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、14.8%其他[種族和](../Page/種族.md "wikilink")4.1%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")30.8%。而在64.8%[白人當中](../Page/歐裔美國人.md "wikilink")，有8.9%為[德國人](../Page/德國人.md "wikilink")、6.9%[英國人和](../Page/英國人.md "wikilink")6.0%[愛爾蘭人](../Page/愛爾蘭人.md "wikilink")。於2,846,289居民當中，有58.6%母語為[英語](../Page/英語.md "wikilink")、25.3%[西班牙語](../Page/西班牙語.md "wikilink")、4.7%
[越南語](../Page/越南語.md "wikilink")、1.9%[朝鮮語](../Page/朝鮮語.md "wikilink")、1.5%[中文](../Page/中文.md "wikilink")、1.2%
[他加祿語](../Page/他加祿語.md "wikilink")。\[14\]

在935,287住户中，有37.0%擁有一個或以上的兒童（18歲以下）、55.9%為夫妻、10.7%為單親家庭、而有28.6%為非家庭。其中21.1%住户為獨居，7.2%為同居長者。平均每戶有3.00人，而平均每個家庭則有3.48人。在2,846,289居民中，有27.0%為18歲以下、9.4%為18至24歲、33.2%為25至44歲、20.6%為45至64歲以及9.9%為65歲以上。人口的年齡中位數為33歲，而每100個女性就有99.0個男性。而每100個女性兒童（18歲以下）就有96.7個男性兒童。\[15\]

民族多樣性的變化已經改變了人口結構。截至2009年，母語為[英語的人口下降至約](../Page/英語.md "wikilink")55%，而母語非英語的人口則上升至近45%。白人人口更由2000年的64.8%急跌至45%。相反，西班牙裔、韓裔、華裔和越南裔的人口則穩定地持續上升。外國出生的居民的比例由1970年的6%急升至2008年的30%。[爾灣市兩位市長](../Page/爾灣_\(加利福尼亞州\).md "wikilink")，姜淑熙\[16\]和蔡瑞浩\[17\]，更是首兩個韓裔美國市長。姜淑熙更揚言「我們這個城市擁有35種語言」。\[18\]

里弗賽德縣的住戶收入中位數為$61,8997，而家庭收入中位數則為$75,700（這些數字於2007年估計中分別上升至$71,601和$81,260）\[19\]男性的收入中位數為$45,059，而女性的收入中位數則為$34,026。橙縣的[人均收入為](../Page/人均收入.md "wikilink")$25,826。約7.0%家庭和10.3%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")。

## 參見

  - 《[-{zh-hans:橘郡风云;zh-hk:OC;zh-tw:玩酷世代;}-](../Page/OC.md "wikilink")》（*The
    O.C.*）：一齣以「Orange County」作為片名與主要場景的美國影集。

## 參考資料

<div class="references-small">

<references />

</div>

[O](../Category/加利福尼亞州行政區劃.md "wikilink")
[Category:大洛杉矶地区](../Category/大洛杉矶地区.md "wikilink")
[Category:南加利福尼亚州行政区划](../Category/南加利福尼亚州行政区划.md "wikilink")

1.

2.

3.  [1932 popular vote by
    counties](http://geoelections.free.fr/USA/elec_comtes/1932.htm)

4.  [1936 popular vote by
    counties](http://geoelections.free.fr/USA/elec_comtes/1936.htm)

5.  [ATLAS OF THE U.S. PRESIDENTIAL ELECTIONS
    SINCE 1789](http://geoelections.free.fr/index.htm)

6.

7.

8.

9.

10. ["2010 Census P.L. 94-171 Summary File Data". United States Census
    Bureau.](http://www2.census.gov/census_2010/01-Redistricting_File--PL_94-171/California/)

11.

12.
13. [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

14. [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

15.

16. [irvine mayor sukhee kang to run for
    congress](http://www.theliberaloc.com/2011/07/06/irvine-mayor-sukhee-kang-to-run-for-congress)

17. [Steven Choi - Official campaign web
    site](http://www.stevenchoi.org/)

18. Adam Nagourney, "Orange County Is No Longer Nixon Country," [*New
    York Times,*
    2010-8-30](http://www.nytimes.com/2010/08/30/us/politics/30orange.html?hp).

19.