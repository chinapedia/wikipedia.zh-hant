，1994年3月12日的[多啦A夢](../Page/多啦A夢.md "wikilink")·[多啦A夢電影作品](../Page/多啦A夢電影作品.md "wikilink")15周年記念作品。同時放映的作品為「[多啦美與青色稻草人](../Page/哆啦美與青色稻草人.md "wikilink")」和「ウメ星デンカ
宇宙の果てからパンパロパン\!」

[電影](../Page/電影.md "wikilink")[票房](../Page/票房.md "wikilink")13億3000萬元，觀眾共270萬人。原作是[藤子·F·不二雄](../Page/藤子·F·不二雄.md "wikilink")、監督是[芝山努](../Page/芝山努.md "wikilink")、发行商是[東寶](../Page/東寶.md "wikilink")。

## 故事概要

现实世界里的大雄一事无成，利用哆啦A梦的造梦机，在梦境世界却成为英雄剑士，与妖怪军团作战，但是梦境渐渐与现实交错，发生不可思议的事情。

大雄使用了多啦A夢的任意選擇夢境機套裝，並配上「夢幻三劍士」的盒帶，把他帶進了一個超真實的夢幻世界，在夢之國當中--猶美倫國正被妖靈大帝侵略。而大雄為救靜香公主並與她結婚，決定起肩負打倒妖靈大帝柯多來武的重任。
到底得到白銀之劍而變身成白銀劍士的大雄，與好友小夫、胖虎、靜香及變身成魔法師的多啦A夢在夢境中會遇到什麼困難呢……

## 夢中的角色

### 夢幻三劍士及其他角色

<table>
<thead>
<tr class="header">
<th><p>角色</p></th>
<th><p>夢境角色</p></th>
<th><p>配音員<br />
</p></th>
<th><p>備註</p></th>
<th><p>背景</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>香港</p></td>
<td><p>台灣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>野比大雄</strong></p></td>
<td><p>港譯：大雄尼安<br />
台譯：諾比尼亞</p></td>
<td><p><a href="../Page/曾慶珏.md" title="wikilink">曾慶珏</a></p></td>
<td><p><a href="../Page/楊凱凱.md" title="wikilink">楊凱凱</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>多啦A夢</strong></p></td>
<td><p>|魔法師哆啦夢</p></td>
<td><p><a href="../Page/林保全.md" title="wikilink">林保全</a></p></td>
<td><p><a href="../Page/陳美貞.md" title="wikilink">陳美貞</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>源靜香</strong></p></td>
<td><p>港譯：靜香格爾/靜香莉亞<br />
台譯：卡魯/靜亞</p></td>
<td><p><a href="../Page/梁少霞.md" title="wikilink">梁少霞</a></p></td>
<td><p><a href="../Page/許淑嬪.md" title="wikilink">許淑嬪</a></p></td>
<td><p>分別飾演一隻小精靈叫靜絲（台譯：西露可）</p></td>
</tr>
<tr class="odd">
<td><p><strong>剛田武</strong></p></td>
<td><p>港譯：胖虎托司<br />
台譯：賈托司</p></td>
<td><p><a href="../Page/陳卓智.md" title="wikilink">陳卓智</a></p></td>
<td><p><a href="../Page/于正昌.md" title="wikilink">于正昌</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>骨川小夫</strong></p></td>
<td><p>港譯：小夫密士<br />
台譯：斯內密斯</p></td>
<td><p><a href="../Page/黃鳳英.md" title="wikilink">黃鳳英</a></p></td>
<td><p><a href="../Page/劉傑.md" title="wikilink">劉傑</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>大雄老師</strong></p></td>
<td><p>國王/靜香格爾之父</p></td>
<td><p><a href="../Page/陳永信.md" title="wikilink">陳永信</a></p></td>
<td><p><a href="../Page/劉傑.md" title="wikilink">劉傑</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 本片敵人

| 角色                      |                配音員                 | 介紹                                                                                                                         |
| ----------------------- | :--------------------------------: | -------------------------------------------------------------------------------------------------------------------------- |
| **妖靈大帝奧多羅姆**（妖霊大帝オドローム） | [家弓家正](../Page/家弓家正.md "wikilink") | 妖靈軍團的首領，亞米魯曼王國的大敵，擁有強大魔力，其手杖可以讓物體化為灰燼，企圖藉由遊戲統治真實人們的夢境，但一直十分害怕傳說中的銀劍客。在本片的最後，銀劍客抵達城堡「幽冥宮」時，他親自出馬收拾銀劍客，結果被卡魯以放大燈將銀劍放大直接貫穿殺死。 |
| **史派達爾將軍**（スパイドル將軍）     | [屋良有作](../Page/屋良有作.md "wikilink") | 揮舞著六支毒劍的妖怪，討厭水但不怕水，率領土精軍團。最後由於土精軍團全軍覆沒，又敗給銀劍客被迫吐出蜘蛛絲逃走，回去之後卻因失敗而被妖靈大帝以殺死做為刑罰，其形象和名字取自蜘蛛。                                   |
| **加波士將軍**（ジャンボス将軍）      | [鄉里大輔](../Page/鄉里大輔.md "wikilink") | 大象妖怪，因史派達爾將軍的失敗而取代他並率領水精軍團、鐵精軍團去攻打夢幻三劍客，最後被銀劍客切斷耳朵後墜落到森林死亡。原作漫畫中是被銀劍客切斷耳朵掉下去把自己巨大化企圖再度挑戰，後來被哆啦A夢們用「游泳粉末」沉下去而亡。             |
| **鳥人**（トリホー）            | [田村錦人](../Page/田村錦人.md "wikilink") | 鳥妖，飛的時候喜歡笑，被奧多羅姆指示偷走了魔法師哆啦夢的百寶袋。在電影版中和最後登場的百貨公司職員臉一樣。另外，把大雄帶到夢境世界的人就是牠。                                                    |

## 其他

本片集早於1994年12月29日至1995年1月7日在香港各大院線放映（上映時劇名為《**叮噹劇場版之魔域三劍俠**》），是自1982年[大雄的恐龍後](../Page/大雄的恐龍.md "wikilink")12年以來首次在香港戲院院線上映的多啦A夢電影，亦是香港首次与日本同年上映哆啦A梦电影作品。其後更於2006年12月26日和2008年8月28日在[無線電視翡翠台放映](../Page/無線電視翡翠台.md "wikilink")。

## 外部連結

  -
  -
  -
[1994](../Category/哆啦A梦电影作品.md "wikilink")
[Category:1994年日本劇場動畫](../Category/1994年日本劇場動畫.md "wikilink")
[Category:芝山努電影](../Category/芝山努電影.md "wikilink")
[Category:夢題材動畫電影](../Category/夢題材動畫電影.md "wikilink")
[Category:三劍客題材電影](../Category/三劍客題材電影.md "wikilink")
[Category:1990年代科幻片](../Category/1990年代科幻片.md "wikilink")
[Category:1990年代奇幻片](../Category/1990年代奇幻片.md "wikilink")
[Category:1990年代冒險片](../Category/1990年代冒險片.md "wikilink")