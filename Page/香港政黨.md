**香港政黨**可依照政治主張的不同分為四大類：[建制派](../Page/建制派.md "wikilink")、[民主派](../Page/民主派_\(香港\).md "wikilink")（包括[泛民主派和](../Page/泛民主派.md "wikilink")[自決派](../Page/自決派.md "wikilink")）、[本土派](../Page/本土派_\(香港\).md "wikilink")（包括[民族自決派](../Page/民族自決派.md "wikilink")、[港獨派](../Page/港獨派.md "wikilink")）及[中間派](../Page/中間派_\(香港\).md "wikilink")。

因為[香港並無參政團體以爭取執政為長遠目標](../Page/香港.md "wikilink")，所以“[政黨](../Page/政黨.md "wikilink")”一詞在香港很多時也包括主要知名的參選政治團體，與此詞一般的語意有所不同。

香港的政黨立場，在一定程度上並沒有像其他國家或地區般左中右派旗幟鮮明，加上由於香港現時並沒有政黨法\[1\]，故正式來說難以界定何為[政黨](../Page/政黨.md "wikilink")。下面列出較知名，或有現任成員為[立法會及或](../Page/香港立法會.md "wikilink")[區議會議員的團體](../Page/香港區議會.md "wikilink")。準確點說，[香港的](../Page/香港.md "wikilink")「政黨」其實都只是根據《[香港公司條例](../Page/香港公司條例.md "wikilink")》或《社團條例》登記，所以香港存在的「政黨」在法律上的地位都是「[公司](../Page/公司.md "wikilink")」或「[社團](../Page/社会团体.md "wikilink")」。[支聯會可能是第一個根據](../Page/支聯會.md "wikilink")《公司條例》註冊成立的政治組織，此後的香港政黨大多按這個方法成立。[香港中文大學政治與行政學系高級導師](../Page/香港中文大學.md "wikilink")[蔡子強認為香港有需要盡快制訂政黨法](../Page/蔡子強.md "wikilink")，因為政黨以公司法、社團方式註冊都有漏洞，黨員身份可能曝光\[2\]。前[立法局議員](../Page/香港立法局.md "wikilink")[陸恭蕙曾促請政府盡快制訂政黨法](../Page/陸恭蕙.md "wikilink")，但被當局以「沒有需要」為由拒絕\[3\]。

## 拥有立法会议席的政党

### 建制派

香港的建制派政黨如下，現時建制派由工會和社團、工業界和商界（工商派）、新界鄉事勢力（鄉事派）組成，政治立場由[左至](../Page/左派.md "wikilink")[右皆有](../Page/右派.md "wikilink")，共同點是順從甚至主動迎合[香港中聯辦當局指揮](../Page/香港中聯辦.md "wikilink")，為香港政府大部分政策護航，被視為[執政聯盟](../Page/執政聯盟.md "wikilink")。

**具有立法會議席的[傳統親共政黨如下](../Page/香港親共團體.md "wikilink")**

  - [民主建港協進聯盟](../Page/民主建港協進聯盟.md "wikilink")（主席：[李慧琼](../Page/李慧琼.md "wikilink")）
      - [NTAS_Logo.svg](https://zh.wikipedia.org/wiki/File:NTAS_Logo.svg "fig:NTAS_Logo.svg")[新界社团联会](../Page/新界社团联会.md "wikilink")（会长：[梁志祥](../Page/梁志祥.md "wikilink")）
  - [Hong_Kong_Federation_of_Trade_Unions_Logo.svg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Federation_of_Trade_Unions_Logo.svg "fig:Hong_Kong_Federation_of_Trade_Unions_Logo.svg")[香港工會聯合會](../Page/香港工會聯合會.md "wikilink")（會長：[吳秋北](../Page/吳秋北.md "wikilink")）
  - [港九勞工社團聯會](../Page/港九勞工社團聯會.md "wikilink")（主席：[吴慧仪](../Page/吴慧仪.md "wikilink")）

**具有立法會議席的[商界政黨如下](../Page/商界_\(香港政治派别\).md "wikilink")**

  - [BPAHK_logo.svg](https://zh.wikipedia.org/wiki/File:BPAHK_logo.svg "fig:BPAHK_logo.svg")[香港經濟民生聯盟](../Page/香港經濟民生聯盟.md "wikilink")（主席：[盧偉國](../Page/盧偉國.md "wikilink")）
      - [西九新動力](../Page/西九新動力.md "wikilink")（主席：[梁美芬](../Page/梁美芬.md "wikilink")）
  - [LiberalParty_HK.svg](https://zh.wikipedia.org/wiki/File:LiberalParty_HK.svg "fig:LiberalParty_HK.svg")[自由黨](../Page/自由黨_\(香港\).md "wikilink")（黨魁：[鍾國斌](../Page/鍾國斌.md "wikilink")；主席：[張宇人](../Page/張宇人.md "wikilink")）
  - [新民黨](../Page/新民黨_\(香港\).md "wikilink")（主席：[叶刘淑仪](../Page/叶刘淑仪.md "wikilink")）
  - [Logo_of_New_Century_Forum.svg](https://zh.wikipedia.org/wiki/File:Logo_of_New_Century_Forum.svg "fig:Logo_of_New_Century_Forum.svg")[新世紀論壇](../Page/新世紀論壇.md "wikilink")（召集人：[馬逢國](../Page/馬逢國.md "wikilink")）

### 民主派

民主派政黨主張香港公民有權透過[民主政治自主決定香港的事情](../Page/民主政治.md "wikilink")。親共傳媒稱之為[反對派](../Page/反對派.md "wikilink")。政治立場涵蓋[中間偏左至](../Page/中間偏左.md "wikilink")[左翼](../Page/左翼.md "wikilink")。

傳統泛民主派政黨可分為[溫和民主派和](../Page/溫和民主派_\(香港\).md "wikilink")[激進民主派](../Page/激進民主派_\(香港\).md "wikilink")。在雨傘革命後，出現[自決派](../Page/自決派.md "wikilink")。

**具有立法會議席的溫和民主派政黨如下**

  - （主席：[胡志偉](../Page/胡志偉.md "wikilink")）

  - （主席：[梁家傑](../Page/梁家傑.md "wikilink")；黨魁：[楊岳橋](../Page/楊岳橋.md "wikilink")）

  - （主席：[吳永輝](../Page/吳永輝.md "wikilink")）

  - （主席：[郭永健](../Page/郭永健.md "wikilink")）

  - [街坊工友服務處](../Page/街坊工友服務處.md "wikilink")（主席：[盧藝賢](../Page/盧藝賢.md "wikilink")\[4\]）

  - （會長：[馮偉華](../Page/馮偉華.md "wikilink")）

**具有立法會議席的激進民主派政黨如下**

  - （主席：[陳志全](../Page/陳志全.md "wikilink")）

### 本土派

本土派部分政黨被中國當局和親共傳媒歸類為[港獨人士](../Page/香港獨立運動.md "wikilink")，或港獨派、反對派，但事實上本土派各政團對香港前途的立場各有不同，它們不都一定支持港獨。

  - [熱血公民](../Page/熱血公民.md "wikilink")（主席：[鄭松泰](../Page/鄭松泰.md "wikilink")）

## 其它活躍政治組織

### 建制派

  - [創建力量](../Page/創建力量.md "wikilink")（有立法会议员联系，未申报）
  - [新界關注大聯盟](../Page/新界關注大聯盟.md "wikilink")（有立法会议员联系，未申报）
  - [九龙社团联会](../Page/九龙社团联会.md "wikilink")
  - [社區18](../Page/社區18.md "wikilink")
  - [香港島各界聯合會](../Page/香港島各界聯合會.md "wikilink")
  - [城鄉居民共和協會](../Page/城鄉居民共和協會.md "wikilink")
  - [公屋聯會](../Page/公屋聯會.md "wikilink")
  - [新進步聯盟](../Page/新進步聯盟.md "wikilink")
  - [香港中華總商會](../Page/香港中華總商會.md "wikilink")
  - [香港中華廠商聯合會](../Page/香港中華廠商聯合會.md "wikilink")
  - [香港總商會](../Page/香港總商會.md "wikilink")
  - [香港工業總會](../Page/香港工業總會.md "wikilink")
  - [香港教育工作者聯會](../Page/香港教育工作者聯會.md "wikilink")
  - [香港公民協會](../Page/香港公民協會.md "wikilink")
  - [公民力量](../Page/公民力量_\(香港\).md "wikilink")
  - [獅子山學會](../Page/獅子山學會.md "wikilink")
  - [香港華人革新協會](../Page/香港華人革新協會.md "wikilink")
  - [亞洲黨](../Page/亞洲黨.md "wikilink")
  - [工商專業聯會](../Page/工商專業聯會.md "wikilink")
  - [新青年論壇](../Page/新青年論壇.md "wikilink")
  - [香港三十會](../Page/香港三十會.md "wikilink")
  - [民主思路](../Page/民主思路.md "wikilink")

#### 激进建制派

  - [愛護香港力量](../Page/愛護香港力量.md "wikilink")
  - [愛港之聲](../Page/愛港之聲.md "wikilink")
  - [保衛香港運動](../Page/保衛香港運動.md "wikilink")
  - [幫幫香港出聲行動](../Page/幫幫香港出聲行動.md "wikilink")
  - [香港政研會](../Page/香港政研會.md "wikilink")
  - [香港青年關愛協會](../Page/香港青年關愛協會.md "wikilink")
  - [正義聯盟](../Page/正義聯盟_\(香港\).md "wikilink")
  - [撐警大聯盟](../Page/撐警大聯盟.md "wikilink")

### 泛民主派

#### 泛民會議友好

  - [专业议政](../Page/专业议政.md "wikilink")（有立法会议员联系，未申报）

  - [社工复兴运动](../Page/社工复兴运动.md "wikilink")（有立法会议员联系，未申报）

  - [选民力量](../Page/选民力量.md "wikilink")（有立法会议员联系，未申报）

  - [小丽民主教室](../Page/小丽民主教室.md "wikilink")

  - [香港众志](../Page/香港众志.md "wikilink")

  - [进步民主连线](../Page/进步民主连线.md "wikilink")

  - [香港民主民生协进会](../Page/香港民主民生协进会.md "wikilink")

  - [新民主同盟](../Page/新民主同盟.md "wikilink")

  - [新退党同盟](../Page/新退党同盟.md "wikilink")

  - [前綫](../Page/前綫_\(政黨\).md "wikilink")

  - [西環飛躍動力](../Page/西環飛躍動力.md "wikilink")

  - [將軍澳民生關注組](../Page/將軍澳民生關注組.md "wikilink")

  - [灣仔好日誌](../Page/灣仔好日誌.md "wikilink")

  - [香港民主宣傳小組](../Page/香港民主宣傳小組.md "wikilink")

  - [香港市民支援愛國民主運動聯合會](../Page/香港市民支援愛國民主運動聯合會.md "wikilink")

  - [保釣行動委員會](../Page/保釣行動委員會.md "wikilink")

  - [保護海港協會](../Page/保護海港協會.md "wikilink")

  - [南方民主同盟](../Page/南方民主同盟.md "wikilink")

  - [民間人權陣線](../Page/民間人權陣線.md "wikilink")

  - [香港民主促進會](../Page/香港民主促進會.md "wikilink")

  - [香港職工會聯盟](../Page/香港職工會聯盟.md "wikilink")

  - [香港專上學生聯會](../Page/香港專上學生聯會.md "wikilink")

  - [香港人權監察](../Page/香港人權監察.md "wikilink")

  - [民主動力](../Page/民主動力.md "wikilink")

  - [維園衝鋒](../Page/維園衝鋒.md "wikilink")

  -
  - [精算思政](../Page/精算思政.md "wikilink")

  - [進步會師](../Page/進步會師.md "wikilink")

  - [園境願景](../Page/園境願景.md "wikilink")

  - [思政築覺](../Page/思政築覺.md "wikilink")

  - [藝界起動](../Page/藝界起動.md "wikilink")

  - [思言財雋](../Page/思言財雋.md "wikilink")

  - [前線科技人員](../Page/前線科技人員.md "wikilink")

  - [良心理政](../Page/良心理政.md "wikilink")

  - [保險起動](../Page/保險起動.md "wikilink")

  - [IT-Voice](../Page/IT-Voice.md "wikilink")

  - [杏林覺醒](../Page/杏林覺醒.md "wikilink")

  - [護士政改關注組](../Page/護士政改關注組.md "wikilink")

  - [規言劃政](../Page/規言劃政.md "wikilink")

  - [法政匯思](../Page/法政匯思.md "wikilink")

  - [進步教師同盟](../Page/進步教師同盟.md "wikilink")

  - [放射良心](../Page/放射良心.md "wikilink")

  - [量心思政](../Page/量心思政.md "wikilink")

  - [青年重奪未來](../Page/青年重奪未來.md "wikilink")

  - [大專政改關注組](../Page/大專政改關注組.md "wikilink")

  - [香港本土力量 (2015年)](../Page/香港本土力量_\(2015年\).md "wikilink")

  - [莅地基督徒](../Page/莅地基督徒.md "wikilink")

  - [捍衛基層住屋權益聯盟](../Page/捍衛基層住屋權益聯盟.md "wikilink")

  - [新力量網絡](../Page/新力量網絡.md "wikilink")

  - [香港人權聯委會](../Page/香港人權聯委會.md "wikilink")

#### 左翼民主派

  - [土地正义联盟](../Page/土地正义联盟.md "wikilink")（有立法会议员联系，未申报）
  - [四五行动](../Page/四五行动.md "wikilink")
  - [左翼21](../Page/左翼21.md "wikilink")
  - [社会主义行动](../Page/社会主义行动.md "wikilink")
  - [中国革命共产党](../Page/中国革命共产党.md "wikilink")
  - [先驱社](../Page/先驱社.md "wikilink")

#### 親台團體

  - [港九工團聯合總會](../Page/港九工團聯合總會.md "wikilink")
  - [民主陣線](../Page/民主陣線.md "wikilink")
  - [神州青年服務社](../Page/神州青年服務社.md "wikilink")

### 本土派

#### 港獨派

  - [我是香港人連線](../Page/我是香港人連線.md "wikilink")
  - [香港民族黨](../Page/香港民族黨.md "wikilink") (被香港政府認定為非法組織)
  - [學生動源](../Page/學生動源.md "wikilink")
  - [勇武前綫](../Page/勇武前綫.md "wikilink")
  - [香港獨立黨](../Page/香港獨立黨.md "wikilink") (英國註冊政黨)
  - [香港獨立聯盟](../Page/香港獨立聯盟.md "wikilink")
  - [香港民族陣綫](../Page/香港民族陣綫.md "wikilink")
  - [香港人主場](../Page/香港人主場.md "wikilink")
  - [香港民族獨立](../Page/香港民族獨立.md "wikilink")
  - [學生獨立聯盟](../Page/學生獨立聯盟.md "wikilink")

#### 歸英派

  - [保守黨](../Page/保守黨_\(香港\).md "wikilink")
  - [香港歸英獨立聯盟](../Page/香港歸英獨立聯盟.md "wikilink")

#### 民族自決派

  - [青年新政](../Page/青年新政.md "wikilink")
  - [本土民主前線](../Page/本土民主前線.md "wikilink")
  - [東九龍社區關注組](../Page/東九龍社區關注組.md "wikilink")
  - [天水圍民生關注平台](../Page/天水圍民生關注平台.md "wikilink")
  - [慈雲山建設力量](../Page/慈雲山建設力量.md "wikilink")
  - [屯門社區關注組](../Page/屯門社區關注組.md "wikilink")
  - [長沙灣社區發展力量](../Page/長沙灣社區發展力量.md "wikilink")

#### 制憲派

  - [普羅政治學苑](../Page/普羅政治學苑.md "wikilink")

#### 城邦派

  - [香港復興會](../Page/香港復興會.md "wikilink")

#### 其它

  - [北區水貨客關注組](../Page/北區水貨客關注組.md "wikilink")
  - [港人自決、藍色起義](../Page/港人自決、藍色起義.md "wikilink")
  - [月光抗爭支援](../Page/月光抗爭支援.md "wikilink")
  - [鍵盤戰線](../Page/鍵盤戰線.md "wikilink")
  - [八十後浪](../Page/八十後浪.md "wikilink")
  - [本土公民](../Page/本土公民.md "wikilink")
  - [正義行動](../Page/正義行動_\(香港\).md "wikilink")
  - [真心愛國愛黨聯盟](../Page/真心愛國愛黨聯盟.md "wikilink")
  - [調理農務蘭花系](../Page/調理農務蘭花系.md "wikilink")

#### 社區組織

  - [社區網絡聯盟](../Page/社區網絡聯盟.md "wikilink")
      - [沙田社區網絡](../Page/沙田社區網絡.md "wikilink")
      - [荃灣社區網絡](../Page/荃灣社區網絡.md "wikilink")
      - [天水圍社區發展網絡](../Page/天水圍社區發展網絡.md "wikilink")
      - [屯門社區網絡](../Page/屯門社區網絡.md "wikilink")
      - [葵青連結動力](../Page/葵青連結動力.md "wikilink")
      - [藍田社區網絡](../Page/藍田社區網絡.md "wikilink")
  - [埔向晴天](../Page/埔向晴天.md "wikilink")
  - [荃灣民生動力](../Page/荃灣民生動力.md "wikilink")
  - [北區動源](../Page/北區動源.md "wikilink")
  - [維多利亞社區協會](../Page/維多利亞社區協會.md "wikilink")
  - [南區社區網絡](../Page/南區社區網絡.md "wikilink")
  - [紅磡人紅磡事](../Page/紅磡人紅磡事.md "wikilink")
  - [元朗區議會作戰小組](../Page/元朗區議會作戰小組.md "wikilink")
  - [旺角人旺角事](../Page/旺角人旺角事.md "wikilink")
  - [將軍澳青年力量](../Page/將軍澳青年力量.md "wikilink")
  - [將軍澳本土網絡](../Page/將軍澳本土網絡.md "wikilink")

### 中間派

  - [新思維](../Page/新思維_\(香港\).md "wikilink")
  - [專業動力](../Page/專業動力.md "wikilink")
  - [全民在野党](../Page/全民在野党.md "wikilink")

## 其它在不活躍政治组织

### 建制派

  - [民主公義派](../Page/民主公義派.md "wikilink")
  - [香港中外聯盟](../Page/香港中外聯盟.md "wikilink")
  - [民生聯盟](../Page/民生聯盟.md "wikilink")
  - [港人協會](../Page/港人協會.md "wikilink")

### 泛民主派

  - [民主救港力量](../Page/民主救港力量.md "wikilink")
  - [公民起動](../Page/公民起動.md "wikilink")
  - [七一人民批](../Page/七一人民批.md "wikilink")
  - [七一連線](../Page/七一連線.md "wikilink")
  - [爭取民主政制大聯盟](../Page/爭取民主政制大聯盟.md "wikilink")
  - [蟻聯](../Page/蟻聯.md "wikilink")
  - [民主2000聯盟](../Page/民主2000聯盟.md "wikilink")
  - [民主民權網絡](../Page/民主民權網絡.md "wikilink")

## 已消亡的的政治组织

以下政黨因為合併、解散或其它原因而不再存在。

### 建制派

  - [香港励进会](../Page/香港励进会.md "wikilink")
  - [啟聯資源中心](../Page/啟聯資源中心.md "wikilink")
  - [專業會議](../Page/專業會議.md "wikilink")
  - [香港自由民主聯會](../Page/香港自由民主聯會.md "wikilink")
  - [香港協進聯盟](../Page/香港協進聯盟.md "wikilink")
  - [新香港聯盟](../Page/新香港聯盟.md "wikilink")
  - [穩定香港協會](../Page/穩定香港協會.md "wikilink")
  - [经济动力](../Page/经济动力.md "wikilink")
  - [香港革新會](../Page/香港革新會.md "wikilink")

### 泛民主派

  - [香港民主同盟](../Page/香港民主同盟.md "wikilink")
  - [匯點](../Page/匯點.md "wikilink")
  - [社會民主論壇](../Page/社會民主論壇.md "wikilink")
  - [民權黨](../Page/民權黨.md "wikilink")
  - [前綫 (1996年)](../Page/前綫_\(1996年\).md "wikilink")
  - [四十五条关注组](../Page/四十五条关注组.md "wikilink")
  - [民主進步黨](../Page/民主進步黨_\(香港\).md "wikilink")

#### 亲台团体

  - [一二三民主联盟](../Page/一二三民主联盟.md "wikilink")

### 本土派

  - [香港本土力量 (2011年)](../Page/香港本土力量_\(2011年\).md "wikilink")
  - [香港民主自治黨](../Page/香港民主自治黨.md "wikilink")
  - [聯合國香港協會](../Page/聯合國香港協會.md "wikilink")

### 托派

  - [中国国际主义工人党](../Page/中国国际主义工人党.md "wikilink")
  - [革命国际主义同盟](../Page/革命国际主义同盟.md "wikilink")
  - [复醒社](../Page/复醒社.md "wikilink")
  - [革命马克思主义者同盟 (1975年)](../Page/革命马克思主义者同盟_\(1975年\).md "wikilink")
  - [革命马克思主义者同盟](../Page/革命马克思主义者同盟.md "wikilink")
  - [中国无产者协会](../Page/中国无产者协会.md "wikilink")

## 参见

  - [中國政黨](../Page/中國政黨.md "wikilink")
  - [中華民國政黨](../Page/中華民國政黨.md "wikilink")
  - [中華人民共和國政黨](../Page/中華人民共和國政黨.md "wikilink")
  - [澳門政黨](../Page/澳門政黨.md "wikilink")

## 参考资料

{{-}}

[政](../Category/香港政治相關列表.md "wikilink")
[Category:政黨列表](../Category/政黨列表.md "wikilink")
[香港政黨](../Category/香港政黨.md "wikilink")

1.

2.

3.
4.