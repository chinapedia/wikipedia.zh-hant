****是历史上第七十七次航天飞机任务，也是[哥伦比亚号航天飞机的第十七次太空飞行](../Page/哥倫比亞號太空梭.md "wikilink")。

## 任务成员

  - **[特伦斯·亨里克斯](../Page/特伦斯·亨里克斯.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[凯文·克雷格](../Page/凯文·克雷格.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[苏珊·赫尔姆斯](../Page/苏珊·赫尔姆斯.md "wikilink")**（，曾执行、、、、、以及任务），飞行工程师
  - **[理查德·林奈](../Page/理查德·林奈.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[查尔斯·布拉迪](../Page/查尔斯·布拉迪.md "wikilink")**（，、曾执行任务），任务专家
  - **[让-雅克·法维埃](../Page/让-雅克·法维埃.md "wikilink")**（，[法国宇航员](../Page/法国.md "wikilink")，曾执行任务），有效载荷专家
  - **[罗伯特·瑟斯克](../Page/罗伯特·瑟斯克.md "wikilink")**（，[加拿大宇航员](../Page/加拿大.md "wikilink")，曾执行任务），有效载荷专家

### 替补有效载荷专家

  - **[佩德罗·杜克](../Page/佩德罗·杜克.md "wikilink")**（，[西班牙宇航员](../Page/西班牙.md "wikilink")，曾执行、[联盟TMA-3以及](../Page/联盟TMA-3.md "wikilink")[联盟TMA-2任务](../Page/联盟TMA-2.md "wikilink")）
  - **[卢卡·厄巴尼](../Page/卢卡·厄巴尼.md "wikilink")**（，[意大利宇航员](../Page/意大利.md "wikilink")）

[Category:1996年佛罗里达州](../Category/1996年佛罗里达州.md "wikilink")
[Category:1996年科学](../Category/1996年科学.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1996年6月](../Category/1996年6月.md "wikilink")
[Category:1996年7月](../Category/1996年7月.md "wikilink")