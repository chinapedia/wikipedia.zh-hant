**城巴788線**是[香港島一條特快巴士路線](../Page/香港島.md "wikilink")，來往[小西灣](../Page/小西灣.md "wikilink")（[藍灣半島](../Page/藍灣半島.md "wikilink")）及[中環](../Page/中環.md "wikilink")（[港澳碼頭](../Page/港澳碼頭_\(香港\).md "wikilink")）。

## 簡介及歷史

788線於1993年8月2日由[中巴開辦](../Page/中華汽車有限公司.md "wikilink")，早期來往[小西灣邨及中環](../Page/小西灣邨.md "wikilink")[永和街](../Page/永和街_\(香港\).md "wikilink")，只於平日早上提供往中環及黃昏往小西灣的服務。雖然當時本線的總站跟[780線相同](../Page/城巴780線.md "wikilink")，但本線卻取道[杏花邨一段東區走廊及永泰道](../Page/杏花邨.md "wikilink")，不經[柴灣道大斜路及](../Page/柴灣道.md "wikilink")[漁灣邨](../Page/漁灣邨.md "wikilink")，行車時間比780線短，因此深受乘客歡迎，早上小西灣總站經常有「[打蛇餅](../Page/打蛇餅.md "wikilink")」的情況出現。後來增設早上往小西灣及黃昏往中環的服務，方便到柴灣東工業區上班的民眾。

1998年9月1日中巴喪失專營權，改由[城巴接辦及提供全空調服務](../Page/城巴.md "wikilink")，1998年10月19日提升至每天全日服務，中環總站遷往港澳碼頭，2001年7月22日起遷往[藍灣半島巴士總站](../Page/藍灣半島.md "wikilink")。提升至全日服務後，原有780線的乘客進一步流向本線，令本線的客量繼續上升，最終780線於2004年5月31日縮短至柴灣東總站，小西灣往返中環的服務就由本線獨力承擔。

早期本線往中環方向，需在[金鐘停站](../Page/金鐘.md "wikilink")，後來為縮短行車時間，於平日早上至黃昏分拆出一條[789線](../Page/城巴789線.md "wikilink")，專門服務金鐘的乘客，788線在789線服務時段內不停金鐘。這營運方式後來被新巴套用到[720線上](../Page/新巴720線.md "wikilink")，照版煮碗地分拆出一條720A線服務金鐘。後來789線提升至每天全日服務，788線便只於每日清晨往中環的班次才在金鐘停站。

本路線是由[香港藝術發展局主辦的](../Page/香港藝術發展局.md "wikilink")「[動感藝廊2008](../Page/動感藝廊2008.md "wikilink")」的巴士車身設計比賽得獎作品的指定展出路線，於2008年6月14日至12月13日期間於行走本路線上的巴士上展出。[香港藝術發展局
- 動感藝廊2008](http://www.hkadc.org.hk/mobileart/index.php?v=about&l=tc)

本路線於2009年3月9日起，部分用車開始測試城巴自動報站系統。

## 服務時間及班次

| [小西灣](../Page/小西灣.md "wikilink")（[藍灣半島](../Page/藍灣半島.md "wikilink")）開 | [中環](../Page/中環.md "wikilink")（[港澳碼頭](../Page/港澳碼頭_\(香港\).md "wikilink")）開 |
| --------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| **日期**                                                                | **服務時間**                                                                   |
| 星期一至五                                                                 | 05:30-06:00                                                                |
| 06:00-10:35                                                           | 3-9                                                                        |
| 10:35-16:05                                                           | 10                                                                         |
| 16:05-18:25                                                           | 4-7                                                                        |
| 18:25-22:55                                                           | 10                                                                         |
| 22:55-23:40                                                           | 15                                                                         |
| 星期六                                                                   | 05:30-06:00                                                                |
| 06:00-18:05                                                           | 4-10                                                                       |
| 18:05-22:55                                                           | 10                                                                         |
| 22:55-23:40                                                           | 15                                                                         |
| 星期日及公眾假期                                                              | 05:30-08:30                                                                |
| 08:30-09:55                                                           | 8-9                                                                        |
| 09:55-22:35                                                           | 10                                                                         |
| 22:35-23:40                                                           | 13                                                                         |

## 收費

全程：$6.7

  - 舊灣仔警署往中環（港澳碼頭）：$3.7
  - 柴灣工業城往小西灣（藍灣半島）：$3.3

### 八達通轉乘優惠

乘客登上本線後指定時間內以同一張八達通卡轉乘以下路線，或從以下路線登車後指定時間內以同一張八達通卡轉乘本線，次程可獲車資折扣優惠：

  - 788←→780，次程車資全免，轉乘時限為90分鐘

<!-- end list -->

  - **788往港澳碼頭** → 780往中環碼頭
  - 780往柴灣（東） → **788往藍灣半島**

<!-- end list -->

  - 788←→A11、A12，兩程總車資$45

<!-- end list -->

  - **788往港澳碼頭** → A11或A12往機場，轉乘時限為90分鐘
  - A11往北角碼頭或A12往藍灣半島 → **788往藍灣半島**，轉乘時限為120分鐘

<!-- end list -->

  - 788←→5X、18P/18X，兩程總車資$9.5

<!-- end list -->

  - **788往港澳碼頭** → 5X、18P/18X往堅尼地城，轉乘時限為90分鐘
  - 5X往銅鑼灣 (威非路道),18P/18X往北角或筲箕灣 → **788往藍灣半島**，轉乘時限為90分鐘

## 使用車輛

本線主要用車為[Enviro 500](../Page/Enviro_500.md "wikilink") 12米（81XX-8319），
[Enviro 500 MMC](../Page/Enviro_500_MMC.md "wikilink") 12米 (8320-8536),
而[Enviro 500 MMC](../Page/Enviro_500_MMC.md "wikilink") 12.8米
(63XX)亦不時行走本線。

全港首部瑞典[斯堪尼亞K280UD](../Page/斯堪尼亞K280UD.md "wikilink")
12米低地台雙層空調巴士樣版車（8900／PX3555），為城巴車隊中首輛使用EEV級別（即廢氣排放標準更高於歐盟5型）引擎的巴士，配上葡萄牙Salvador
Caetano車身，於2011年7月14日於本路線首航。

## 行車路線

[CTB788RtMap.png](https://zh.wikipedia.org/wiki/File:CTB788RtMap.png "fig:CTB788RtMap.png")
**小西灣（藍灣半島）開**經：（[富欣街](../Page/富欣街.md "wikilink")、）[小西灣道](../Page/小西灣道.md "wikilink")、[小西灣邨巴士總站](../Page/小西灣邨巴士總站.md "wikilink")、小西灣道、[柴灣道](../Page/柴灣道.md "wikilink")、[永泰道天橋](../Page/永泰道天橋.md "wikilink")、[永泰道](../Page/永泰道.md "wikilink")、[東區走廊](../Page/東區走廊.md "wikilink")、[維園道](../Page/維園道.md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[夏愨道](../Page/夏愨道.md "wikilink")（*[紅棉路支路](../Page/紅棉路.md "wikilink")、[琳寶徑](../Page/琳寶徑.md "wikilink")、[遮打街](../Page/遮打街.md "wikilink")、[昃臣道](../Page/昃臣道.md "wikilink")*）、[干諾道中及](../Page/干諾道中.md "wikilink")[港澳碼頭通道](../Page/港澳碼頭通道.md "wikilink")。

  -
    逢星期一至六（公眾假期除外）09:00前由藍灣半島開出的班次將改經富欣街前往小西灣道
    每日05:30-06:25由藍灣半島開出的班次抵夏慤道後將改經*斜體字*之路段

**中環（港澳碼頭）開**經：港澳碼頭通道、干諾道中、[林士街](../Page/林士街.md "wikilink")、[德輔道中](../Page/德輔道中.md "wikilink")、[畢打街](../Page/畢打街.md "wikilink")、干諾道中、夏愨道、告士打道、*[菲林明道](../Page/菲林明道.md "wikilink")、[會議道](../Page/會議道.md "wikilink")、[鴻興道](../Page/鴻興道.md "wikilink")、天橋、告士打道*、維園道、東區走廊、永泰道、[嘉業街](../Page/嘉業街.md "wikilink")、[常安街](../Page/常安街.md "wikilink")、柴灣道及小西灣道。

  -
    逢星期一至六（公眾假期除外）09:00前由中環開出的班次不駛經*斜體字*之路段

### 沿線車站

| [小西灣](../Page/小西灣.md "wikilink")（[藍灣半島](../Page/藍灣半島.md "wikilink")）開 | [中環](../Page/中環.md "wikilink")（[港澳碼頭](../Page/港澳碼頭_\(香港\).md "wikilink")）開 |
| --------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| **序號**                                                                | **車站名稱**                                                                   |
| 1                                                                     | [小西灣](../Page/小西灣.md "wikilink")（[藍灣半島](../Page/藍灣半島.md "wikilink")）       |
| 2\*                                                                   | [富景花園](../Page/富景花園.md "wikilink")                                         |
| 3                                                                     | [小西灣邨](../Page/小西灣邨.md "wikilink")                                         |
| 4\*\~                                                                 | [曉翠街](../Page/曉翠街.md "wikilink")                                           |
| 5                                                                     | [富城閣](../Page/富城閣.md "wikilink")                                           |
| 6                                                                     | [翠灣邨](../Page/翠灣邨.md "wikilink")                                           |
| 7\*\~                                                                 | [舊灣仔警署](../Page/舊灣仔警署.md "wikilink")                                       |
| 8                                                                     | [分域街](../Page/分域街.md "wikilink")                                           |
| ^                                                                     | [海富中心](../Page/海富中心.md "wikilink")                                         |
| 9                                                                     | [皇后像廣場](../Page/皇后像廣場.md "wikilink")                                       |
| 10                                                                    | [砵典乍街](../Page/砵典乍街.md "wikilink")                                         |
| 11                                                                    | [租庇利街](../Page/租庇利街.md "wikilink")                                         |
| 12                                                                    | [永和街](../Page/永和街.md "wikilink")                                           |
| 13                                                                    | 中環（港澳碼頭）                                                                   |
|                                                                       | 15                                                                         |

  - 註：

<!-- end list -->

1.  逢星期一至六（公眾假期除外）09:00前由小西灣及中環開出的班次不停有 \* 號之車站
2.  逢星期一至六（公眾假期除外）07:30-09:00由小西灣開出的班次不停有 \*\~ 號之車站
3.  有 ^ 號之車站祇限每日05:30-06:25由小西灣開出的班次停靠

## 參考資料

<references/>

  - 《二十世紀港島區巴士路線發展史》，容偉釗編著，BSI出版
  - 《香港島巴士路線與社區發展》，ISBN：9789888310067，出版商：中華書局(香港)有限公司，出版日期：2014年11月

## 外部連結

  - [城巴官方路線資料：788](http://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=788&route=788&routetype=D&company=5&exactMatch=yes)
  - [城巴官方網站788線資料（PDF乳豬紙版）](https://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-CTB-788-788-D.pdf)
  - [i-busnet－港島區路線788線資料](http://www.i-busnet.com/busroute/ctb/ctbr788.php)
  - [681巴士總站－城巴788線](http://www.681busterminal.com/hk-788.html)
  - [運輸署新聞公報：永泰道行車天橋星期日通車](http://www.td.gov.hk/tc/publications_and_press_releases/press_releases/transport_department/index_id_706.html)

[788](../Category/城巴及新世界第一巴士路線.md "wikilink")
[788](../Category/香港東區巴士路線.md "wikilink")
[788](../Category/中西區巴士路線.md "wikilink")
[788](../Category/前中華巴士路線.md "wikilink")