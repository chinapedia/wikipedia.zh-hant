[Lyon_Playfair.jpg](https://zh.wikipedia.org/wiki/File:Lyon_Playfair.jpg "fig:Lyon_Playfair.jpg")
**莱昂·普莱费尔**，[GCB](../Page/巴思勋章.md "wikilink")，[PC](../Page/英国枢密院.md "wikilink")，[FRS](../Page/英国皇家学会.md "wikilink")，（[英语](../Page/英语.md "wikilink")：****，1818年5月1日[孟加拉国](../Page/孟加拉国.md "wikilink")
-
1898年5月29日[英国](../Page/英国.md "wikilink")[南肯辛顿](../Page/南肯辛顿.md "wikilink")），[英国科学家](../Page/英国.md "wikilink")、政治家。

普莱费尔出生于孟加拉国，曾前后在[圣安德鲁斯大学](../Page/圣安德鲁斯大学.md "wikilink")、[安德森学院和](../Page/斯特拉思克莱德大学.md "wikilink")[爱丁堡大学学习](../Page/爱丁堡大学.md "wikilink")。在1837年去过[加尔各答之后](../Page/加尔各答.md "wikilink")，普莱费尔成为[伦敦大学学院](../Page/伦敦大学学院.md "wikilink")[托马斯·格雷姆的私人实验室助理](../Page/托马斯·格雷姆.md "wikilink")，之后又在1839年前往[吉森大学](../Page/吉森大学.md "wikilink")[尤斯图斯·冯·李比希手下工作](../Page/尤斯图斯·冯·李比希.md "wikilink")。

返回英国后，普莱费尔成为[克利瑟罗附近一家印花布工厂的经理](../Page/克利瑟罗.md "wikilink")。1843年，普莱费尔被任命为[皇家曼彻斯特学院的化学教授](../Page/皇家曼彻斯特学院.md "wikilink")，助手[罗伯特·安格斯·史密斯](../Page/罗伯特·安格斯·史密斯.md "wikilink")。1848年，他入选[英国皇家学会](../Page/英国皇家学会.md "wikilink")，两年后又成为[万国工业博览会执行委员会成员](../Page/万国工业博览会.md "wikilink")。

1853年，普莱费尔被任命为英国科学大臣，在职期间他一直鼓吹在[克里米亚战争中使用毒气杀害](../Page/克里米亚战争.md "wikilink")[俄罗斯人](../Page/俄罗斯人.md "wikilink")。1855年，普莱费尔成为[世界博览会委员](../Page/世界博览会.md "wikilink")。两年后，普莱费尔担任化学学会会长。1858年，他回到爱丁堡大学并担任化学教授。

1868年，普莱费尔当选[英国自由党下议院议员并于](../Page/英国自由党.md "wikilink")1873年担任[威廉·尤尔特·格拉德斯通内阁的](../Page/威廉·尤尔特·格拉德斯通.md "wikilink")[邮政大臣](../Page/英国邮政大臣.md "wikilink")。1880年，自由党重新执政，普莱费尔被任命为[英国下议院代理议长](../Page/英国下议院议长.md "wikilink")。1885年他担任英国先进科学协会主席，次年又成为[英国枢密院委员会负责教育的副主席](../Page/英国枢密院.md "wikilink")。1889年，普莱费尔成为[康沃尔公国委员会成员](../Page/康沃尔公国.md "wikilink")。

1892年，普莱费尔从下议院退出并在1892年封为“[普莱费尔男爵](../Page/普莱费尔男爵.md "wikilink")”。

[P](../Category/1818年出生.md "wikilink")
[P](../Category/1898年逝世.md "wikilink")
[P](../Category/英国邮政大臣.md "wikilink")
[P](../Category/聖安德魯斯大學校友.md "wikilink")
[P](../Category/愛丁堡大學校友.md "wikilink")