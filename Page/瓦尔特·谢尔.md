**瓦尔特·谢尔**（****，）生於[索林根](../Page/索林根.md "wikilink")，[德国](../Page/德国.md "wikilink")[政治家](../Page/政治家.md "wikilink")，[德国自由民主党人](../Page/德国自由民主党.md "wikilink")。1961年至1966年任联邦经济合作部长，1969年至1974年任外交部部长和联邦副总理，1974年至1979年任[联邦总统](../Page/德国联邦总统.md "wikilink")。

2016年8月24日逝世，享年97岁。\[1\]\[2\]

## 著作

  - with [Karl-Hermann Flach](../Page/Karl-Hermann_Flach.md "wikilink")
    and [Werner Maihofer](../Page/Werner_Maihofer.md "wikilink"): *Die
    Freiburger Thesen der Liberalen.* Rowohlt, Hamburg 1972, ISBN
    3-499-11545-X.
  - *Die Zukunft der Freiheit – Vom Denken und Handeln in unserer
    Demokratie.* Econ, 1979.
  - *Wen schmerzt noch Deutschlands Teilung? 2 Reden zum 17. Juni*,
    Rowohlt, Reinbek 1986, ISBN 3-499-18346-3.
  - with [Otto Graf
    Lambsdorff](../Page/Otto_Graf_Lambsdorff.md "wikilink"): *Freiheit
    in Verantwortung, Deutscher Liberalismus seit 1945.* Bleicher, 1988,
    ISBN 3-88350-047-X.
  - with Jürgen Engert: *Erinnerungen und Einsichten*. Hohenheim-Verlag,
    Stuttgart 2004, ISBN 3-89850-115-9.
  - with Tobias Thalhammer: *Gemeinsam sind wir stärker – Zwölf
    erfreuliche Geschichten über Jung und Alt*. Allpart Media, Berlin
    2010, ISBN 978-3-86214-011-4.

## 文学作品

  - [Hans-Dietrich
    Genscher](../Page/Hans-Dietrich_Genscher.md "wikilink") (Hrsg.):
    *Heiterkeit und Härte: Walter Scheel in seinen Reden und im Urteil
    von Zeitgenossen*. Deutsche Verlagsanstalt, Stuttgart 1984, ISBN
    3-421-06218-8.
  - Hans-Roderich Schneider: *Präsident des Ausgleichs. Bundespräsident
    Walter Scheel. Ein liberaler Politiker.* Verlag Bonn aktuell,
    Stuttgart 1975, ISBN 3-87959-045-1.

## 参考资料

## 外部链接

[Category:德国联邦总统](../Category/德国联邦总统.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")
[Category:德國副總理](../Category/德國副總理.md "wikilink")
[Category:德意志联邦共和国外交部长](../Category/德意志联邦共和国外交部长.md "wikilink")
[Category:德國自由民主黨黨員](../Category/德國自由民主黨黨員.md "wikilink")
[Category:德國第二次世界大戰人物](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:索林根人](../Category/索林根人.md "wikilink")
[Category:查理曼獎得主](../Category/查理曼獎得主.md "wikilink")

1.
2.