**格利澤229** (也可以寫成**[Gl](../Page/星表#Gl,_GJ,_Wo.md "wikilink") 229**或**GJ
229**)
是在[天兔座內距離](../Page/天兔座.md "wikilink")[地球大約](../Page/地球.md "wikilink")19[光年的一顆](../Page/光年.md "wikilink")[紅矮星](../Page/紅矮星.md "wikilink")。它的[質量是](../Page/質量.md "wikilink")58%[太陽質量](../Page/太陽質量.md "wikilink")\[1\]，半徑是
69%[太陽半徑](../Page/太陽半徑.md "wikilink")\[2\]，和在赤道只有1公里/秒，非常低的[投影轉速](../Page/恆星自轉.md "wikilink")\[3\]。

已知這顆[恆星是低活動的](../Page/恆星.md "wikilink")[焰星](../Page/耀星.md "wikilink")，這意味著它曾經是一顆表面亮度會因為[磁場活動而隨意變化的恆星](../Page/恆星磁場.md "wikilink")。頻譜中顯示出鈣的H和K發射譜線，在[冕部的光譜中也曾經檢測出](../Page/日冕.md "wikilink")[X射線](../Page/X射線.md "wikilink")\[4\]，這可能是[磁場](../Page/磁場.md "wikilink")[迴圈與](../Page/迴圈.md "wikilink")[恆星外圍的](../Page/恆星.md "wikilink")[大氣層交互作用引起的](../Page/大氣層.md "wikilink")，但沒有檢測到大規模的[黑子活動](../Page/黑子.md "wikilink")\[5\]。

在1994年拍到次恆星伴星**格利澤229B**的影像，並在1995年獲得證實。它是一顆[棕矮星](../Page/棕矮星.md "wikilink")。雖然是一顆質量太小，無法維持[氫的](../Page/氫.md "wikilink")[核融合的](../Page/核融合.md "wikilink")[恆星](../Page/棕矮星.md "wikilink")
，它的質量大約是[木星的](../Page/木星.md "wikilink")20至50倍，對行星而言是太重了。格利澤229B是第一顆被確認的次恆星質量的天體，表面[溫度大約是](../Page/溫度.md "wikilink")950[K](../Page/熱力學溫標.md "wikilink")\[6\]。

這顆恆星的[空間速度分量是U](../Page/空間速度.md "wikilink") = +12，V = –11，和W = –12
公里/秒\[7\]。這顆恆星以0.07的離心率和0.005的[軌道傾角在](../Page/軌道傾角.md "wikilink")[銀河系中穿越著](../Page/銀河系.md "wikilink")\[8\]。

## 參考資料

## 外部連結

  - [Astronomers Announce First Clear Evidence of a Brown
    Dwarf](http://hubblesite.org/newscenter/newsdesk/archive/releases/1995/48/)
    – [STScI](../Page/Space_Telescope_Science_Institute.md "wikilink")
    news release STScI-1995-48 (November 29, 1995)
  - [Brown dwarfs
    (NASA)](https://web.archive.org/web/20041031004701/http://starchild.gsfc.nasa.gov/docs/StarChild/shadow/questions/brown_dwarf.html)
  - [Extrasolar Visions –
    Gliese 229](https://web.archive.org/web/20070930032923/http://www.extrasolar.net/startour.asp?StarCatId=browndwarf&StarId=29)
  - [Extrasolar Visions – Gliese 229
    b](http://www.extrasolar.net/planettour.asp?StarCatId=browndwarf&PlanetId=25)

[Category:聯星](../Category/聯星.md "wikilink")
[Category:棕矮星](../Category/棕矮星.md "wikilink")
[Category:耀星](../Category/耀星.md "wikilink")
[Category:天兔座](../Category/天兔座.md "wikilink")
[Category:紅矮星](../Category/紅矮星.md "wikilink")
[0229](../Category/格利泽和GJ天体.md "wikilink")

1.
2.
3.
4.  {{ cite journal |author=Schmitt JHMM, Fleming TA, Giampapa MS
    |title=The X-Ray View of the Low-Mass Stars in the Solar
    Neighborhood |journal=Ap J. |volume=450 |issue=9 |pages=392–400
    |bibcode=1995ApJ...450..392S |doi=10.1086/176149 |date=September
    1995}}

5.

6.

7.

8.