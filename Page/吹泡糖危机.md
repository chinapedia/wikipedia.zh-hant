《**吹泡糖危机**》（，Bubblegum
Crisis）是一部[日本的](../Page/日本.md "wikilink")[人工智能](../Page/人工智能.md "wikilink")[機器人動畫作品](../Page/機器人動畫.md "wikilink")，為1987年至1991年由[AIC发布的](../Page/AIC.md "wikilink")[OVA系列](../Page/OVA.md "wikilink")，全8集。後來在1998年重拍成電視版《**[吹泡糖危機2040](../Page/吹泡糖危機2040.md "wikilink")**》，但日本播放時收視屢創新低，結局兩集也遭停播。共二十四集加上最後結局兩集，最後結局兩集以附在[VHS或](../Page/VHS.md "wikilink")[LD的OVA形式發售](../Page/LD.md "wikilink")。

以未來大災難之後的[东京为舞台](../Page/东京.md "wikilink")，描寫黑暗中的正義維護者塞柏斯騎士的故事。該動畫除了在日本之外，在西方也受到很大的歡迎。

[台灣由](../Page/台灣.md "wikilink")[衛視中文台播映](../Page/衛視中文台.md "wikilink")，[亞藝國際代理](../Page/亞藝國際.md "wikilink")[VCD版](../Page/VCD.md "wikilink")。在[香港曾經由](../Page/香港.md "wikilink")[翡翠台播放過電視版](../Page/翡翠台.md "wikilink")《吹泡糖危機2040》，但並未播放或出版其結局兩集。

因為獲得很高的人氣，2009年由[新加坡](../Page/新加坡.md "wikilink")、日本、[加拿大](../Page/加拿大.md "wikilink")、[澳大利亞](../Page/澳大利亞.md "wikilink")、[中國](../Page/中華人民共和國.md "wikilink")、[英國等](../Page/英國.md "wikilink")6個國家宣布決定拍攝真人電影，目標2012年公開上映\[1\]。至2019年2月現在狀況不明。

## 故事大要

21世紀初，[國際企業](../Page/國際企業.md "wikilink")「系統公司」(Genom)的昆西(Quincy)和史汀古尼(Stingray)
[博士發明了稱為](../Page/博士.md "wikilink")「Boomer」的[機器人](../Page/機器人.md "wikilink")。後來史汀古尼[意外](../Page/意外.md "wikilink")[死亡](../Page/死亡.md "wikilink")，而昆西成為了系統公司的總裁。後來因為[地震而需要很多機器人救援和重建](../Page/地震.md "wikilink")，使得系統公司成為日本最大的機器人生產商，並做出了在[太空和海底工作的機器人](../Page/太空.md "wikilink")。

當昆西半[退休後](../Page/退休.md "wikilink")，野心家馬辛(Mason)接手了系統公司，並暗中發展把Boomer成為武器和[仿生人](../Page/仿生人.md "wikilink")，造成很多神秘的機器人意外。警方成立了稱為「AD
Police」的對應部隊，應對機器人事件。

而史汀古尼的千金施莉亞（Sylia
Stingray）聯同三位好友少女，發展了戰鬥用的[動力服](../Page/動力服.md "wikilink")，成立了一組自稱「塞柏斯騎士」(Knight
Sabers)的小隊，輔助AD Police，並暗中調查系統公司的秘密。

## 工作人員

  - 原作：[鈴木敏充](../Page/鈴木敏充.md "wikilink")
  - 劇本：[松崎健一](../Page/松崎健一.md "wikilink")、[荒牧伸志](../Page/荒牧伸志.md "wikilink")、[柿沼秀樹](../Page/柿沼秀樹.md "wikilink")、[秋山勝仁](../Page/秋山勝仁.md "wikilink")
  - 監督：[秋山勝仁](../Page/秋山勝仁.md "wikilink")
  - 人物設計：[園田健一](../Page/園田健一.md "wikilink")
  - 製作設計：園田健一、柿沼秀樹、荒牧伸志
  - 作畫監督：田中正弘、仲盛文
  - 音響監督：松浦典良
  - 音樂：馬飼野康二

## 參考來源

## 外部連結

  - [泡泡糖危機官方網站](https://web.archive.org/web/20080924051328/http://www.anime-int.com/works/bubblegum/crisis/)AIC

[Category:1987年日本OVA動畫](../Category/1987年日本OVA動畫.md "wikilink")
[Category:AIC](../Category/AIC.md "wikilink")
[Category:東京都背景作品](../Category/東京都背景作品.md "wikilink")
[Category:2030年代背景作品](../Category/2030年代背景作品.md "wikilink")
[Category:赛博朋克作品](../Category/赛博朋克作品.md "wikilink")

1.  [人気アニメ「バブルガムクライシス」が実写映画化決定！6か国の共同製作 -
    シネマトゥデイ](http://www.cinematoday.jp/page/N0018102)