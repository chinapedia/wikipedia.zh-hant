**娜妲麗·華**（，）[美國知名電視](../Page/美國.md "wikilink")、電影演員；曾經榮獲[金球獎最佳女主角獎及兩次](../Page/金球獎.md "wikilink")[奧斯卡最佳女主角獎及一次](../Page/奧斯卡最佳女主角獎.md "wikilink")[奧斯卡最佳女配角獎提名](../Page/奧斯卡最佳女配角獎.md "wikilink")，著名代表作是歌舞片《[西城故事](../Page/西城故事_\(電影\).md "wikilink")》；1981年在加州搭乘[遊艇時因意外而溺斃](../Page/遊艇.md "wikilink")。

## 早年

娜妲麗·華父母是来自[俄國的移民](../Page/俄國.md "wikilink")，她们先是移民到加拿大，后来移民到美国，娜塔莉有一个同母异父的姐姐和一个同父同母的妹妹。她在旧金山出生。
1943年娜塔莉伍德在电影《快乐大陆》中饰演一个掉了冰淇淋的小女孩而出道，虽然只有15秒的镜头。

## 電影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1943</p></td>
<td><p><a href="../Page/快乐的大陆.md" title="wikilink">快乐的大陆</a></p></td>
<td><p>Little girl who drops ice cream cone</p></td>
<td><p>uncredited; film debut</p></td>
</tr>
<tr class="even">
<td><p>1946</p></td>
<td><p><a href="../Page/胭脂馬.md" title="wikilink">胭脂馬</a></p></td>
<td><p>Carol Warren</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1946</p></td>
<td><p><a href="../Page/春閨淚痕.md" title="wikilink">春閨淚痕</a></p></td>
<td><p>Margaret Ludwig</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1947</p></td>
<td><p><a href="../Page/浮木.md" title="wikilink">浮木</a></p></td>
<td><p>Jenny Hollingsworth</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1947</p></td>
<td><p><a href="../Page/寡婦情深結鬼緣.md" title="wikilink">寡婦情深結鬼緣</a></p></td>
<td><p>Anna Muir as a child</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1947</p></td>
<td><p><a href="../Page/三十四街奇蹟.md" title="wikilink">三十四街奇蹟</a></p></td>
<td><p>Susan Walker</p></td>
<td><p>First starring role</p></td>
</tr>
<tr class="odd">
<td><p>1948</p></td>
<td><p><a href="../Page/斯库达，嚯！斯库达，嗨！.md" title="wikilink">斯库达，嚯！斯库达，嗨！</a></p></td>
<td><p>Bean McGill</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1949</p></td>
<td><p><a href="../Page/玉女懷春.md" title="wikilink">玉女懷春</a></p></td>
<td><p>Ellen Cooper</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1949</p></td>
<td><p><a href="../Page/绿色的希望.md" title="wikilink">绿色的希望</a></p></td>
<td><p>Susan Anastasia Matthews</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1949</p></td>
<td><p><a href="../Page/快樂無疆.md" title="wikilink">快樂無疆</a></p></td>
<td><p>Ruth Hefferan</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1950</p></td>
<td><p><a href="../Page/牛郎淑女.md" title="wikilink">牛郎淑女</a></p></td>
<td><p>Nancy 'Nan' Howard</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1950</p></td>
<td><p><a href="../Page/財神玩世記.md" title="wikilink">財神玩世記</a></p></td>
<td><p>Phyllis Lawrence</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1950</p></td>
<td><p><a href="../Page/親情深似海.md" title="wikilink">親情深似海</a></p></td>
<td><p>Penny Macaulay</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1950</p></td>
<td><p><a href="../Page/一朝春盡紅顏老.md" title="wikilink">一朝春盡紅顏老</a></p></td>
<td><p>Polly Scott</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1951</p></td>
<td><p><a href="../Page/青紗紅淚.md" title="wikilink">青紗紅淚</a></p></td>
<td><p>Stephanie Rawlins</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1951</p></td>
<td><p><a href="../Page/風流小姨.md" title="wikilink">風流小姨</a></p></td>
<td><p>Pauline Jones</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1952</p></td>
<td><p><a href="../Page/銀海滄桑.md" title="wikilink">銀海滄桑</a></p></td>
<td><p>Gretchen Drew</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1952</p></td>
<td><p><a href="../Page/高歌艷舞樂璇宮.md" title="wikilink">高歌艷舞樂璇宮</a></p></td>
<td><p>Barbara Blake</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1952</p></td>
<td><p><a href="../Page/玫瑰碗的故事.md" title="wikilink">玫瑰碗的故事</a></p></td>
<td><p>Sally Burke</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1954</p></td>
<td><p><a href="../Page/聖杯妖魔.md" title="wikilink">聖杯妖魔</a></p></td>
<td><p>Helena as a child</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1955</p></td>
<td><p><a href="../Page/阿飛正傳_(1955年電影).md" title="wikilink">青春的迷茫</a></p></td>
<td><p>Judy</p></td>
<td><p>提名<a href="../Page/奧斯卡最佳女配角獎.md" title="wikilink">奧斯卡最佳女配角獎</a></p></td>
</tr>
<tr class="even">
<td><p>1955</p></td>
<td><p><a href="../Page/一个愿望.md" title="wikilink">一个愿望</a></p></td>
<td><p>Seely Dowder</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1956</p></td>
<td><p><a href="../Page/少爺兵艷史.md" title="wikilink">少爺兵艷史</a></p></td>
<td><p>Susan Daniels</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1956</p></td>
<td><p><a href="../Page/血山情燄.md" title="wikilink">血山情燄</a></p></td>
<td><p>Maria Christina Colton</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1956</p></td>
<td><p><a href="../Page/血濺姻緣路.md" title="wikilink">血濺姻緣路</a></p></td>
<td><p>Liz Taggert</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1956</p></td>
<td><p><a href="../Page/搜索者.md" title="wikilink">搜索者</a></p></td>
<td><p>Debbie Edwards (older)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1957</p></td>
<td><p><a href="../Page/越洲霸王.md" title="wikilink">越洲霸王</a></p></td>
<td><p>Lois Brennan</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1958</p></td>
<td><p><a href="../Page/戰地雙雄.md" title="wikilink">戰地雙雄</a></p></td>
<td><p>Monique Blair</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1958</p></td>
<td><p><a href="../Page/痴鳳啼痕.md" title="wikilink">痴鳳啼痕</a></p></td>
<td><p>Marjorie Morgenstern</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1960</p></td>
<td><p><a href="../Page/情海波瀾.md" title="wikilink">情海波瀾</a></p></td>
<td><p>Sarah 'Salome' Davis</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1960</p></td>
<td><p><a href="../Page/我不負卿.md" title="wikilink">我不負卿</a></p></td>
<td><p>Lory Austen</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1961</p></td>
<td><p><a href="../Page/西城故事_(電影).md" title="wikilink">西区故事</a></p></td>
<td><p>Maria</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1961</p></td>
<td><p><a href="../Page/天涯何处觅知音.md" title="wikilink">天涯何处觅知音</a></p></td>
<td><p>Wilma Dean Loomis</p></td>
<td><p>提名<a href="../Page/奧斯卡最佳女主角.md" title="wikilink">奧斯卡最佳女主角</a><br />
提名<a href="../Page/英國電影學院獎最佳女主角.md" title="wikilink">英國電影學院獎最佳女主角</a><br />
提名<a href="../Page/金球獎最佳戲劇類電影女主角.md" title="wikilink">金球獎最佳戲劇類電影女主角</a></p></td>
</tr>
<tr class="even">
<td><p>1962</p></td>
<td><p><a href="../Page/玫瑰舞后.md" title="wikilink">玫瑰舞后</a></p></td>
<td><p>Louise</p></td>
<td><p>提名<a href="../Page/金球獎最佳音樂及喜劇類電影女主角.md" title="wikilink">金球獎最佳音樂及喜劇類電影女主角</a></p></td>
</tr>
<tr class="odd">
<td><p>1963</p></td>
<td><p><a href="../Page/烈火乾柴.md" title="wikilink">烈火乾柴</a></p></td>
<td><p>Angie Rossini</p></td>
<td><p>提名<a href="../Page/奧斯卡最佳女主角.md" title="wikilink">奧斯卡最佳女主角</a><br />
提名<a href="../Page/金球獎最佳戲劇類電影女主角.md" title="wikilink">金球獎最佳戲劇類電影女主角</a></p></td>
</tr>
<tr class="even">
<td><p>1964</p></td>
<td><p><a href="../Page/彩鳳春情.md" title="wikilink">彩鳳春情</a></p></td>
<td><p><a href="../Page/Helen_Gurley_Brown.md" title="wikilink">Helen Gurley Brown</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1965</p></td>
<td><p><a href="../Page/飛女黛絲.md" title="wikilink">飛女黛絲</a></p></td>
<td><p>Daisy Clover</p></td>
<td><p>提名<a href="../Page/金球獎最佳音樂及喜劇類電影女主角.md" title="wikilink">金球獎最佳音樂及喜劇類電影女主角</a><br />
Won—World Film Favorite – Female</p></td>
</tr>
<tr class="even">
<td><p>1965</p></td>
<td><p><a href="../Page/瘋狂大賽車.md" title="wikilink">瘋狂大賽車</a></p></td>
<td><p>Maggie DuBois</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1966</p></td>
<td><p><a href="../Page/千面艷盜.md" title="wikilink">千面艷盜</a></p></td>
<td><p>Penelope Elcott</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1966</p></td>
<td><p><a href="../Page/蓬門綺夢.md" title="wikilink">蓬門綺夢</a></p></td>
<td><p>Alva Starr</p></td>
<td><p>提名<a href="../Page/金球獎最佳戲劇類電影女主角.md" title="wikilink">金球獎最佳戲劇類電影女主角</a></p></td>
</tr>
<tr class="odd">
<td><p>1969</p></td>
<td><p><a href="../Page/兩對鴛鴦一張床.md" title="wikilink">兩對鴛鴦一張床</a></p></td>
<td><p>Carol Sanders</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1972</p></td>
<td><p><a href="../Page/總統候選人.md" title="wikilink">總統候選人</a></p></td>
<td><p>Herself</p></td>
<td><p>cameo</p></td>
</tr>
<tr class="odd">
<td><p>1973</p></td>
<td><p><a href="../Page/多情自古空餘恨.md" title="wikilink">多情自古空餘恨</a></p></td>
<td><p>Courtney Patterson</p></td>
<td><p>TV movie</p></td>
</tr>
<tr class="even">
<td><p>1975</p></td>
<td><p><a href="../Page/蠱惑神探.md" title="wikilink">蠱惑神探</a></p></td>
<td><p>Ellen Prendergast</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1976</p></td>
<td><p><a href="../Page/朱门巧妇.md" title="wikilink">朱门巧妇</a></p></td>
<td><p>Maggie</p></td>
<td><p>TV movie</p></td>
</tr>
<tr class="even">
<td><p>1979</p></td>
<td><p><a href="../Page/From_Here_to_Eternity_(TV_series).md" title="wikilink">乱世忠魂</a></p></td>
<td><p>Karen Holmes</p></td>
<td><p><a href="../Page/Golden_Globe_Award_for_Best_Actress_–_Television_Series_Drama.md" title="wikilink">Golden Globe Award for Best Actress – Television Series Drama</a></p></td>
</tr>
<tr class="odd">
<td><p>1979</p></td>
<td><p><a href="../Page/缺後重圓.md" title="wikilink">缺後重圓</a></p></td>
<td><p>Cassie Barrett</p></td>
<td><p>TV movie</p></td>
</tr>
<tr class="even">
<td><p>1979</p></td>
<td><p><a href="../Page/地球浩劫.md" title="wikilink">地球浩劫</a></p></td>
<td><p>Tatiana Nikolaevna Donskaya</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1980</p></td>
<td><p><a href="../Page/嫁錯丈夫娶錯妻.md" title="wikilink">嫁錯丈夫娶錯妻</a></p></td>
<td><p>Mari Thompson</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1980</p></td>
<td><p><a href="../Page/鑽石走私奇案.md" title="wikilink">鑽石走私奇案</a></p></td>
<td><p>Eva/Claire Ryker</p></td>
<td><p>TV movie</p></td>
</tr>
<tr class="odd">
<td><p>1980</p></td>
<td><p><a href="../Page/情難斷.md" title="wikilink">情難斷</a></p></td>
<td><p>Herself</p></td>
<td><p>(cameo)</p></td>
</tr>
<tr class="even">
<td><p>1983</p></td>
<td><p><a href="../Page/腦震盪_(電影).md" title="wikilink">尖端大风暴</a></p></td>
<td><p>Karen Brace</p></td>
<td><p>提名—<a href="../Page/Saturn_Award_for_Best_Supporting_Actress.md" title="wikilink">Saturn Award for Best Supporting Actress</a></p></td>
</tr>
</tbody>
</table>

## 電視

## 花邊新聞

  - 與影星[勞勃·韋納兩次](../Page/勞勃·韋納.md "wikilink")15年婚姻頗為知名

## 獎項

## 意外溺斃

  - 1981年11月，[好萊塢先後殞落兩位明星](../Page/好萊塢.md "wikilink")：16日是[奧斯卡影帝](../Page/奧斯卡.md "wikilink")[威廉·荷頓屍體被發現陳屍於自宅](../Page/威廉·荷頓.md "wikilink")；28日則是娜妲麗·華浮屍於海上。
  - 當年[感恩節](../Page/感恩節.md "wikilink")，[勞勃·韋納夫妻邀請](../Page/勞勃·韋納.md "wikilink")1978年[奧斯卡最佳男配角](../Page/奧斯卡.md "wikilink")[克里斯多夫·華肯週末到他們](../Page/克里斯多夫·華肯.md "wikilink")55英尺長，大型有臥室[遊艇出海觀光](../Page/遊艇.md "wikilink")；船員一人駕駛載他們三人前往[卡塔麗娜島](../Page/卡塔麗娜島.md "wikilink")(Catalina
    Island)。
  - 因為船拋錨送修緣故，他們四人在11月28日午後16時還在島港口Reef餐廳用餐；他們三人在餐廳喝了不少[香檳](../Page/香檳.md "wikilink")、大聲喧鬧甚至嘔吐；據說娜妲麗·華還相當快樂地不停拍打嬉鬧小她5歲的華肯，引人側目，離開餐廳他們就返回碼頭搭乘舶靠遊艇上。
  - 在船上，船長在半夜0時20分發現遊艇上少了一艘救生艇，起初不以為意，但也直覺認為是娜妲麗·華搭乘它－因為她有搭乘救生艇出遊海上賞星星習慣；直到數分鐘後[勞勃·韋納也在找娜妲麗](../Page/勞勃·韋納.md "wikilink")·華，才發現出事了。通知海岸巡防隊，在29日早上7時透過直昇機發現娜妲麗·華臉朝上浮屍及小艇出現在離遊艇數百米外，無明顯外傷，右眼下方有瘀傷。
  - 法醫驗屍研判：娜妲麗·華是想回岸上休息，自行搭小艇離去時可能滑倒落水，臉頰碰到小艇所以有瘀傷；可是身上衣服厚重吸水導致難以掙扎，且冬天海水冰冷造成失溫溺斃。

## 參考聯結

  -
  - [Foul Play on Catalina
    Island?](http://www.franksreelreviews.com/shorttakes/nataliewood/nataliewood.htm)
    The Mysterious Death of Natalie Wood

  - [Natalie
    Wood](http://findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=1124&pt=Natalie%20Wood)
    at [Find a Grave](http://www.findagrave.com)

  - [*Natalie Wood Style and Beauty
    Page*](http://www.stealtheirstyle.co.uk/classic%20stars/Natalie%20Wood%20look.htm)

  - [Who2](http://www.who2.com/nataliewood.html)

[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")
[Category:未解决死亡案与谋杀案](../Category/未解决死亡案与谋杀案.md "wikilink")
[Category:俄羅斯裔美國人](../Category/俄羅斯裔美國人.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:溺死者](../Category/溺死者.md "wikilink")