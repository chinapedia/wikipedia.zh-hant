| 代碼       | 機場                                                 | 城市                                                                     | 国家和地区                                                              |
| -------- | -------------------------------------------------- | ---------------------------------------------------------------------- | ------------------------------------------------------------------ |
| LAS      | [麥卡倫國際機場](../Page/麥卡倫國際機場.md "wikilink")           | [拉斯維加斯](../Page/拉斯維加斯.md "wikilink")                                   | [美國](../Page/美國.md "wikilink")                                     |
| LAX      | [洛杉磯國際機場](../Page/洛杉磯國際機場.md "wikilink")           | [洛杉矶](../Page/洛杉矶.md "wikilink")                                       | [美國](../Page/美國.md "wikilink")                                     |
| LCX      | [龍岩冠豸山機場](../Page/龍岩冠豸山機場.md "wikilink")           | [龍岩市](../Page/龍岩市.md "wikilink")                                       | [中国](../Page/中華人民共和國.md "wikilink")                                |
| LED\[1\] | [普爾科沃機場](../Page/普爾科沃機場.md "wikilink")             | [聖彼得堡](../Page/聖彼得堡.md "wikilink")                                     | [俄羅斯](../Page/俄羅斯.md "wikilink")                                   |
| LEJ      | [萊比錫/哈雷機場](../Page/萊比錫/哈雷機場.md "wikilink")         | [萊比錫](../Page/萊比錫.md "wikilink")/[哈雷](../Page/哈雷_\(德國\).md "wikilink") | [德國](../Page/德國.md "wikilink")                                     |
| LEX      | [藍草機場](../Page/藍草機場.md "wikilink")                 | [勒星頓](../Page/勒星頓.md "wikilink")                                       | [美國](../Page/美國.md "wikilink")                                     |
| LGA      | [拉瓜地亞機場](../Page/拉瓜地亞機場.md "wikilink")             | [纽约市](../Page/纽约市.md "wikilink")                                       | [美國](../Page/美國.md "wikilink")                                     |
| LGB      | [長灘機場](../Page/長灘機場.md "wikilink")                 | [長灘](../Page/長灘_\(加利福尼亞州\).md "wikilink")                              | [美國](../Page/美國.md "wikilink")                                     |
| LGK      | [蘭卡威國際機場](../Page/蘭卡威國際機場.md "wikilink")           | [蘭卡威](../Page/蘭卡威.md "wikilink")                                       | [馬來西亞](../Page/馬來西亞.md "wikilink")                                 |
| LGW      | [格域機場](../Page/格域機場.md "wikilink")                 | [伦敦](../Page/伦敦.md "wikilink")                                         | [英国](../Page/英国.md "wikilink")                                     |
| LHR      | [倫敦希斯路機場](../Page/倫敦希斯路機場.md "wikilink")           | [伦敦](../Page/伦敦.md "wikilink")                                         | [英国](../Page/英国.md "wikilink")                                     |
| LIM      | [豪爾赫·查韋斯國際機場](../Page/豪爾赫·查韋斯國際機場.md "wikilink")   | [利馬](../Page/利馬.md "wikilink")                                         | [祕魯](../Page/祕魯.md "wikilink")                                     |
| LIT      | [小岩城機場](../Page/小岩城機場.md "wikilink")               | [小岩城](../Page/小岩城_\(阿肯色州\).md "wikilink")                              | [美國](../Page/美國.md "wikilink")                                     |
| LJG      | [麗江三義機場](../Page/麗江三義機場.md "wikilink")             | [麗江市](../Page/麗江市.md "wikilink")                                       | [中國](../Page/中華人民共和國.md "wikilink")                                |
| LLW      | [里朗威國際機場](../Page/里朗威國際機場.md "wikilink")           | [里朗威](../Page/里朗威.md "wikilink")                                       | [馬拉威](../Page/馬拉威.md "wikilink")                                   |
| LOS      | [穆塔拉·穆罕默德國際機場](../Page/穆塔拉·穆罕默德國際機場.md "wikilink") | [拉哥斯](../Page/拉哥斯.md "wikilink")                                       | [奈及利亞](../Page/奈及利亞.md "wikilink")                                 |
| LPA      | [大加那利机场](../Page/大加那利机场.md "wikilink")             | [大加那利岛](../Page/大加那利岛.md "wikilink")                                   | [西班牙](../Page/西班牙.md "wikilink")                                   |
| LPL      | [利物浦約翰藍儂機場](../Page/利物浦約翰藍儂機場.md "wikilink")       | [利物浦](../Page/利物浦.md "wikilink")                                       | [英國](../Page/英國.md "wikilink")                                     |
| LUN      | [路沙卡國際機場](../Page/路沙卡國際機場.md "wikilink")           | [路沙卡](../Page/路沙卡.md "wikilink")                                       | [尚比亞](../Page/尚比亞.md "wikilink")                                   |
| LUX      | [盧森堡-芬戴爾國際機場](../Page/盧森堡-芬戴爾國際機場.md "wikilink")   | [盧森堡市](../Page/盧森堡市.md "wikilink")                                     | [盧森堡](../Page/盧森堡.md "wikilink")                                   |
| LXA      | [拉薩貢嘎機場](../Page/拉薩貢嘎機場.md "wikilink")             | [西藏/拉薩市](../Page/西藏/拉薩市.md "wikilink")                                 | [西藏](../Page/西藏.md "wikilink")                                     |
| LYG      | [连云港白塔埠机场](../Page/连云港白塔埠机场.md "wikilink")         | [连云港市](../Page/连云港市.md "wikilink")                                     | [中国](../Page/中国.md "wikilink")                                     |
| LYI      | [臨沂沭埠嶺機場](../Page/臨沂沭埠嶺機場.md "wikilink")           | [臨沂市](../Page/臨沂市.md "wikilink")                                       | [中国](../Page/中国.md "wikilink")                                     |
| LZN      | [馬祖南竿機場](../Page/馬祖南竿機場.md "wikilink")             | [南竿鄉](../Page/南竿鄉.md "wikilink")                                       | [中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")） |
| LZY      | [林芝米林機場](../Page/林芝米林機場.md "wikilink")             | [西藏/林芝縣](../Page/西藏/林芝縣.md "wikilink")                                 | [中国](../Page/中国.md "wikilink")                                     |
| LAD      | Fevereiro                                          | Luanda                                                                 | [安哥拉](../Page/安哥拉.md "wikilink")                                   |
| LAF      | Purdue University Airport                          | 拉法葉                                                                    | [美國](../Page/美國.md "wikilink")                                     |
| LAN      | Capital City Airport                               | 蘭辛                                                                     | [美國](../Page/美國.md "wikilink")                                     |
| LAP      | Aeropuerto General Marquez De Leon                 | La Paz                                                                 | [墨西哥](../Page/墨西哥.md "wikilink")                                   |
| LAR      | General Brees Field                                | Laramie                                                                | [美國](../Page/美國.md "wikilink")                                     |
| LAW      | Municipal                                          | Lawton                                                                 | [美國](../Page/美國.md "wikilink")                                     |
| LBA      |                                                    | 里茲/Bradford                                                            | [英國](../Page/英國.md "wikilink")                                     |
| LBB      | Lubbock International Airport                      | Lubbock                                                                | [美國](../Page/美國.md "wikilink")                                     |
| LBE      | Westmoreland County                                | Latrobe                                                                | [美國](../Page/美國.md "wikilink")                                     |
| LBL      | Glenn L Martin Terminal                            | Liberal                                                                | [美國](../Page/美國.md "wikilink")                                     |
| LBU      |                                                    | Labuan                                                                 | [馬來西亞](../Page/馬來西亞.md "wikilink")                                 |
| LBV      | Libreville                                         | Libreville                                                             | [加蓬](../Page/加蓬.md "wikilink")                                     |
| LCA      | Intl                                               | 拉納卡                                                                    | [賽普勒斯](../Page/賽普勒斯.md "wikilink")                                 |
| LCE      | International                                      | La Ceiba                                                               | [洪都拉斯](../Page/洪都拉斯.md "wikilink")                                 |
| LCG      | La Coruna                                          | La Coruna                                                              | [西班牙](../Page/西班牙.md "wikilink")                                   |
| LCH      | Municipal                                          | Lake Charles                                                           | [美國](../Page/美國.md "wikilink")                                     |
| LCY      | [倫敦城市機場](../Page/倫敦城市機場.md "wikilink")             | 倫敦                                                                     | [英國](../Page/英國.md "wikilink")                                     |
| LDB      | Londrina                                           | Londrina                                                               | [巴西](../Page/巴西.md "wikilink")                                     |
| LDE      | Tarbes International                               | Lourdes/Tarbes                                                         | [法國](../Page/法國.md "wikilink")                                     |
| LDU      | Lahad Datu                                         | Lahad Datu                                                             | [馬來西亞](../Page/馬來西亞.md "wikilink")                                 |
| LEA      |                                                    | Learmonth                                                              | [澳大利亞](../Page/澳大利亞.md "wikilink")                                 |
| LEB      | Lebanon Regional                                   | Lebanon/Hanover/White River                                            | [美國](../Page/美國.md "wikilink")                                     |
| LEH      | Le Havre                                           | Le Havre                                                               | [法國](../Page/法國.md "wikilink")                                     |
| LET      | Gen Av Cob0                                        | Leticia                                                                | [哥倫比亞](../Page/哥倫比亞.md "wikilink")                                 |
| LFT      | Municipal                                          | 拉法夷特 / New Iberia                                                      | [美國](../Page/美國.md "wikilink")                                     |
| LFW      | Lome                                               | 洛美                                                                     | [多哥](../Page/多哥.md "wikilink")                                     |
| LGG      | Bierset                                            | Liege                                                                  | [比利時](../Page/比利時.md "wikilink")                                   |
| LGP      | Legaspi                                            | Legaspi                                                                | [菲律賓](../Page/菲律賓.md "wikilink")                                   |
| LHE      | Lahore                                             | Lahore                                                                 | [巴基斯坦](../Page/巴基斯坦.md "wikilink")                                 |
| LIG      | Bellegarde                                         | Limoges                                                                | [法國](../Page/法國.md "wikilink")                                     |
| LIR      | Liberia                                            | Liberia                                                                | [哥斯大黎加](../Page/哥斯大黎加.md "wikilink")                               |
| LIS      | Lisboa                                             | 里斯本                                                                    | [葡萄牙](../Page/葡萄牙.md "wikilink")                                   |
| LJU      | Brnik                                              | Ljubljana                                                              | [斯洛文尼亞](../Page/斯洛文尼亞.md "wikilink")                               |
| LKN      | Leknes                                             | Leknes                                                                 | [挪威](../Page/挪威.md "wikilink")                                     |
| LKO      |                                                    | Lucknow                                                                | [印度](../Page/印度.md "wikilink")                                     |
| LLA      | Kallax                                             | Lulea                                                                  | [瑞典](../Page/瑞典.md "wikilink")                                     |
| LLF      | 零陵机场                                               | 永州                                                                     | [中国](../Page/中国.md "wikilink")                                     |
| LMM      |                                                    | Los Mochis                                                             | [墨西哥](../Page/墨西哥.md "wikilink")                                   |
| LMN      | Limbang                                            | Limbang                                                                | [馬來西亞](../Page/馬來西亞.md "wikilink")                                 |
| LNK      | Municipal Airport                                  | 林肯                                                                     | [美國](../Page/美國.md "wikilink")                                     |
| LNS      | Lancaster                                          | Lancaster                                                              | [美國](../Page/美國.md "wikilink")                                     |
| LNV      | Londolovit                                         | Londolovit                                                             | [巴布亚新几内亚](../Page/巴布亚新几内亚.md "wikilink")                           |
| LNY      | Lanai                                              | Lanai City                                                             | [美國](../Page/美國.md "wikilink")                                     |
| LNZ      | Linz                                               | Linz                                                                   | [奧地利](../Page/奧地利.md "wikilink")                                   |
| LPB      | El Alto                                            | La Paz                                                                 | [玻利維亞](../Page/玻利維亞.md "wikilink")                                 |
| LPI      | Saab                                               | Linkoping                                                              | [瑞典](../Page/瑞典.md "wikilink")                                     |
| LRD      | International                                      | Laredo                                                                 | [美國](../Page/美國.md "wikilink")                                     |
| LRH      | Laleu                                              | La Rochelle                                                            | [法國](../Page/法國.md "wikilink")                                     |
| LRS      | Leros                                              | Leros                                                                  | [希臘](../Page/希臘.md "wikilink")                                     |
| LRT      | Lann                                               | Lorient                                                                | [法國](../Page/法國.md "wikilink")                                     |
| LRU      |                                                    | Las Cruces                                                             | [美國](../Page/美國.md "wikilink")                                     |
| LSC      | La Florida                                         | La Serena                                                              | [智利](../Page/智利.md "wikilink")                                     |
| LSE      | La Crosse Municipal                                | La Crosse                                                              | [美國](../Page/美國.md "wikilink")                                     |
| LSP      | Josefa Camejo                                      | Las Piedras                                                            | [委內瑞拉](../Page/委內瑞拉.md "wikilink")                                 |
| LSQ      | Maria Dolores                                      | Los Angeles                                                            | [智利](../Page/智利.md "wikilink")                                     |
| LST      | Launceston                                         | Launceston                                                             | [澳大利亞](../Page/澳大利亞.md "wikilink")                                 |
| LSY      | Lismore                                            | Lismore                                                                | [澳大利亞](../Page/澳大利亞.md "wikilink")                                 |
| LTN      | Luton International                                | 倫敦                                                                     | [英國](../Page/英國.md "wikilink")                                     |
| LTO      |                                                    | Loreto                                                                 | [墨西哥](../Page/墨西哥.md "wikilink")                                   |
| LUA      | Lukla                                              | Lukla                                                                  | [尼泊爾](../Page/尼泊爾.md "wikilink")                                   |
| LUD      | Luderitz                                           | Luderitz                                                               | [納米比亞](../Page/納米比亞.md "wikilink")                                 |
| LUG      | Agno                                               | Lugano                                                                 | [瑞士](../Page/瑞士.md "wikilink")                                     |
| LVI      | Livingstone                                        | Livingstone                                                            | [尚比亞](../Page/尚比亞.md "wikilink")                                   |
| LWO      | Snilow                                             | Lvov                                                                   | [烏克蘭](../Page/烏克蘭.md "wikilink")                                   |
| LWS      | Lewiston                                           | Lewiston                                                               | [美國](../Page/美國.md "wikilink")                                     |
| LWT      | Municipal                                          | Lewistown                                                              | [美國](../Page/美國.md "wikilink")                                     |
| LWY      | Lawas                                              | Lawas                                                                  | [馬來西亞](../Page/馬來西亞.md "wikilink")                                 |
| LXR      | Luxor                                              | Luxor                                                                  | [埃及](../Page/埃及.md "wikilink")                                     |
| LYH      | Municipal Airport                                  | Lynchburg                                                              | [美國](../Page/美國.md "wikilink")                                     |
| LYR      | Svalbard                                           | Longyearbyen                                                           | [挪威](../Page/挪威.md "wikilink")                                     |
| LYS      | Satolas                                            | 里昂                                                                     | [法國](../Page/法國.md "wikilink")                                     |
| LZC      | Na                                                 | Lazaro Cardenas                                                        | [墨西哥](../Page/墨西哥.md "wikilink")                                   |

## 注釋

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")

1.  IATA代碼使用聖彼得堡的舊稱列寧格勒（Leningrad）之縮寫