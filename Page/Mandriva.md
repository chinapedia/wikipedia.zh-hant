**Mandriva** S.A.
是一間[法國的](../Page/法國.md "wikilink")[軟體公司](../Page/軟體公司.md "wikilink")，企業總部位於[巴黎](../Page/巴黎.md "wikilink")，研發總部位於巴西[庫里奇巴](../Page/庫里奇巴.md "wikilink")。他們創造了[Mandriva
Linux](../Page/Mandriva_Linux.md "wikilink")，並負責維持與研發。

它的前身是創建於1998年的**MandrakeSoft**。曾有七十個員工，其中45個是工程師，在法國、美國及巴西都有辦公室。

## 歷史

Mandriva S.A.的前身是創建於1998年的MandrakeSoft。

2004年2月，[赫斯特国际集团宣稱擁有MandrakeSoft的](../Page/赫斯特国际集团.md "wikilink")[商標權](../Page/商標權.md "wikilink")，與MandrakeSoft展開[訴訟](../Page/訴訟.md "wikilink")。

2005年1月24日，MandrakeSoft併購了[Conectiva](../Page/Conectiva.md "wikilink")。同年，MandrakeSoft[上訴後仍被判敗訴](../Page/上訴.md "wikilink")，被迫改名為Mandriva。

2010年7月8日，为了应对财政问题，Mandriva宣布被法国IF Research、英国Lightapp和俄国Pingwin
soft组成的财团重组\[1\]。

2015年5月22日起，Mandriva公司已辦理[清算並終止營運](../Page/清算.md "wikilink")。\[2\]

## 外部連結

<references />

[Category:Linux公司](../Category/Linux公司.md "wikilink")
[M](../Category/法國電子公司.md "wikilink")
[Category:1998年成立的公司](../Category/1998年成立的公司.md "wikilink")
[Category:2015年結業公司](../Category/2015年結業公司.md "wikilink")

1.  [Mandriva restructures to establish European
    leadership](http://www2.mandriva.com/zh/news/press/?p=138)
2.