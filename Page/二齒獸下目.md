**二齒獸下目**（Dicynodontia）是群[似哺乳爬行動物](../Page/似哺乳爬行動物.md "wikilink")，屬於[獸孔目](../Page/獸孔目.md "wikilink")[異齒亞目](../Page/異齒亞目.md "wikilink")。二齒獸類是一群體型從大型到小型、長者兩支長牙的[草食性動物](../Page/草食性.md "wikilink")。牠們也是最成功且多樣性的獸孔目（不計算[哺乳類的狀況](../Page/哺乳類.md "wikilink")）動物，已知超過70屬，大小從[老鼠到](../Page/老鼠.md "wikilink")[象都有](../Page/象.md "wikilink")。

## 特徵

[Dinodontosaurus_museu2x.jpg](https://zh.wikipedia.org/wiki/File:Dinodontosaurus_museu2x.jpg "fig:Dinodontosaurus_museu2x.jpg")的化石\]\]
二齒獸類的頭骨有高度特化的特徵：輕但強壯、頭骨後方的[顳顬孔變的較大](../Page/顳顬孔.md "wikilink")，可容納更大的下頜肌肉。

頭骨與下頜的前段通常是狹窄的、缺乏牙齒；原始物種的嘴部前段仍有牙齒。嘴巴前部有者角狀的喙狀嘴，就像[烏龜與](../Page/烏龜.md "wikilink")[角龍類一樣](../Page/角龍類.md "wikilink")。當嘴巴閉起時，下頜閉起產生的強力切割動作，讓二齒獸類可處理堅硬的陸生植物。許多屬擁有一對長牙，可能是種[兩性異形的特徵](../Page/兩性異形.md "wikilink")。

二齒獸類的身體是短、笨重、水桶腰，四肢強壯。大型物種（例如[恐齒龍獸](../Page/恐齒龍獸.md "wikilink")）的後肢直立於身體之下，但前肢的肘部彎曲。[肩胛骨與](../Page/肩胛骨.md "wikilink")[腸骨都是大而粗壯](../Page/腸骨.md "wikilink")。尾巴很短。

## 演化史

[Eodicynodon_BW.jpg](https://zh.wikipedia.org/wiki/File:Eodicynodon_BW.jpg "fig:Eodicynodon_BW.jpg")、一種發現於中[二疊紀](../Page/二疊紀.md "wikilink")[南非的原始二齒獸類](../Page/南非.md "wikilink")\]\]
二齒獸類在[二疊紀中期首次出現](../Page/二疊紀.md "wikilink")，在一陣快速的[演化輻射後](../Page/演化輻射.md "wikilink")，成為晚[二疊紀最成功且繁盛的陸地](../Page/二疊紀.md "wikilink")[脊椎動物](../Page/脊椎動物.md "wikilink")。在這段期間，牠們佔據大量多樣性的[生態位](../Page/生態位.md "wikilink")，包括大型、中型、小型、與短腿穴居的動物。
[Dicynodont_from_PolandDB.jpg](https://zh.wikipedia.org/wiki/File:Dicynodont_from_PolandDB.jpg "fig:Dicynodont_from_PolandDB.jpg")，三叠纪体型最大的陆栖动物之一，演化出了独特的四肢直立姿势，属于[史達勒克獸科](../Page/史達勒克獸科.md "wikilink")，生存於[三疊紀晚期的](../Page/三疊紀.md "wikilink")[波蘭](../Page/波蘭.md "wikilink")\]\]
只有兩科在[二疊紀-三疊紀滅絕事件中存活下來](../Page/二疊紀-三疊紀滅絕事件.md "wikilink")，其中的[水龍獸科是](../Page/水龍獸科.md "wikilink")[三疊紀最早期](../Page/三疊紀.md "wikilink")（[印度階](../Page/印度階.md "wikilink")）最常見且廣佈的草食性動物。這些中等大小動物演化出[肯氏獸科](../Page/肯氏獸科.md "wikilink")，最後被肯氏獸科取代。[肯氏獸科是體型笨重](../Page/肯氏獸科.md "wikilink")、接近[豬到](../Page/豬.md "wikilink")[牛大小的草食性動物](../Page/牛.md "wikilink")，是[奧倫尼克階到](../Page/奧倫尼克階.md "wikilink")[拉丁階期間的最繁盛且廣佈的動物](../Page/拉丁階.md "wikilink")。到了[卡尼階](../Page/卡尼階.md "wikilink")，肯氏獸科被[犬齒獸類的](../Page/犬齒獸類.md "wikilink")[橫齒獸科](../Page/橫齒獸科.md "wikilink")（Traversodontidae）、[三稜龍類超越](../Page/三稜龍類.md "wikilink")。在[三疊紀晚期](../Page/三疊紀.md "wikilink")（[瑞替阶](../Page/瑞替阶.md "wikilink")），可能因為逐漸的乾燥，牠們迅速地衰落，而大型草食性動物的位置被[蜥腳形亞目](../Page/蜥腳形亞目.md "wikilink")[恐龍所取代](../Page/恐龍.md "wikilink")。

隨者[肯氏獸科的衰落與滅亡](../Page/肯氏獸科.md "wikilink")，合弓綱不再是大型優勢草食性動物。直到[古新世中期](../Page/古新世.md "wikilink")，[犬齒獸類的直系後代](../Page/犬齒獸類.md "wikilink")[哺乳類興起](../Page/哺乳類.md "wikilink")，在[恐龍滅亡後快速地繁盛](../Page/恐龍.md "wikilink")、多樣化。

過去認為二齒獸類在[三疊紀末期完全絕種](../Page/三疊紀.md "wikilink")。但是最近的證據顯示二齒獸類在[岡瓦那大陸南部](../Page/岡瓦那大陸.md "wikilink")（現在[昆士蘭](../Page/昆士蘭.md "wikilink")）存活下來。如果屬實，這將是地質歷史上的另一個「[拉撒路物種](../Page/拉撒路物種.md "wikilink")」（Lazarus
taxon）實例；拉撒路物種意思是那些在化石紀錄中突然消失又出現的物種。

## 分類學與種系發生學

[Wadiasaurus1DB.jpg](https://zh.wikipedia.org/wiki/File:Wadiasaurus1DB.jpg "fig:Wadiasaurus1DB.jpg")\]\]
[KingoriaDB.jpg](https://zh.wikipedia.org/wiki/File:KingoriaDB.jpg "fig:KingoriaDB.jpg")，生存於[二疊紀晚期的](../Page/二疊紀.md "wikilink")[非洲的小型二齒獸類](../Page/非洲.md "wikilink")\]\]
[Placerias1DB.jpg](https://zh.wikipedia.org/wiki/File:Placerias1DB.jpg "fig:Placerias1DB.jpg")\]\]
[Myosaurus.jpg](https://zh.wikipedia.org/wiki/File:Myosaurus.jpg "fig:Myosaurus.jpg")

  - **二齒獸下目 Dicynodontia**
      - *Colobodectes*
      - **始二齒獸超科 Eodicynodontoidea**
          - [始二齒獸科](../Page/始二齒獸科.md "wikilink") Eodicynodontidae
      - **金氏獸超科 Kingorioidea**
          - **[金氏獸科](../Page/金氏獸科.md "wikilink") Kingoriidae**
      - **雙齒獸類 Diictodontia**
          - **Emydopoidea超科**
              - [小頭獸科](../Page/小頭獸科.md "wikilink") Cistecephalidae
              - Emydopidae科
          - **羅伯特獸超科 Robertoidea**
              - [雙齒獸科](../Page/雙齒獸科.md "wikilink") Diictodontidae
              - [羅伯特獸科](../Page/羅伯特獸科.md "wikilink") Robertiidae
      - '''鋸齒獸類 Pristerodontia
          - [恐異齒獸](../Page/恐異齒獸.md "wikilink") *Dinanomodon*
          - *Odontocyclops*
          - *Propelanomodon*
          - Aulacocephalodontidae科
          - [二齒獸科](../Page/二齒獸科.md "wikilink") Dicynodontidae
          - [肯氏獸科](../Page/肯氏獸科.md "wikilink") Kannemeyeriidae
          - [水龍獸科](../Page/水龍獸科.md "wikilink") Lystrosauridae
          - [無齒獸科](../Page/無齒獸科.md "wikilink") Oudenodontidae
          - [鋸齒獸科](../Page/鋸齒獸科.md "wikilink") Pristerodontidae
          - [山西獸科](../Page/山西獸科.md "wikilink") Shansiodontidae
          - [史達勒克獸科](../Page/史達勒克獸科.md "wikilink") Stahleckeriidae

### 系統發生學

以下[支序圖取自於](../Page/支序圖.md "wikilink")[Mikko's Phylogeny
Archive](https://web.archive.org/web/20071025071349/http://www.fmnh.helsinki.fi/users/haaramo/Metazoa/Deuterostoma/Chordata/Synapsida/Anomodontia/Dicynodontia_1.htm)網站的整理：

## 參考資料

  - Robert L. Carroll (1988), *Vertebrate Paleontology and Evolution*,
    WH Freeman & Co.
  - Edwin H. Colbert, (1969), *Evolution of the Vertebrates*, John Wiley
    & Sons Inc (2nd ed.)
  - Cox, B., Savage, R.J.G., Gardiner, B., Harrison, C. and Palmer, D.
    (1988) *The Marshall illustrated encyclopedia of dinosaurs &
    prehistoric animals*, 2nd Edition, Marshall Publishing
  - Crompton, A. W, and Hotton, N. 1967. Functional morphology of the
    masticatory apparatus of two dicynodonts (Reptilia, Therapsida).
    *Postilla*, 109:1–51.
  - Gillian King, "Anomodontia" Part 17 C, *Encyclopedia of
    Paleoherpetology*, Gutsav Fischer Verlag, Stuttgart and New York,
    1988
  - \-- -- , 1990, *the Dicynodonts: A Study in Palaeobiology*, Chapman
    and Hall, London and New York
  - Tony Thulborn and Susan Turner, 2003, "The last dicynodont: an
    Australian Cretaceous relict" *Proceedings: Biological Sciences* Vol
    270, No 1518 / May 07, 2003; pp 985 - 993

## 外部連結

  - [Therapsida : Neotherapsida :
    Dicynodontia](https://web.archive.org/web/20060222040556/http://www.palaeos.com/Vertebrates/Units/400Therapsida/400.725.html)
    - Palaeos

[二齒獸下目](../Category/二齒獸下目.md "wikilink")