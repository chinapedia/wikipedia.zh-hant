**粉紅邦妮**（BONNIE
PINK；）是一位[日本女性](../Page/日本.md "wikilink")[創作歌手](../Page/創作歌手.md "wikilink")，出身於[京都府南丹市八木町日置](../Page/京都府.md "wikilink")。所屬事務所為Taisuke，所屬唱片公司為日本[華納音樂](../Page/華納音樂.md "wikilink")，過去則為[Pony
Canyon旗下歌手](../Page/Pony_Canyon.md "wikilink")。畢業於京都教育大學附屬高等學校、大阪教育大學。[血型為A型](../Page/血型.md "wikilink")，身高約160\~161cm。

## 事業經歷

在大學求學時，還未進行各種正式音樂活動，也未屬於任何音樂事務所時，便在1995年9月21日發行了自己作詞作曲的專輯「Blue
Jam」，因而出道。在這之後，當時在日本也頗有名氣的製作人Tore Johansson便為她製作了「Do You
Crash」（1996年）、「Heaven's Kitchen」（1997年），因此受到矚目。其中「Heaven's
Kitchen」專輯更賣出了30萬張的成績，而同名歌曲也成為BONNIE PINK的代表歌曲之一。

然而，在BONNIE
PINK的名字廣為人之後，她感覺到她與真正的自己"淺田香織"間的距離越來越大，她也漸漸感到自己無法樂於音樂活動。在1998年時發行了「evil
and flowers」以後，便以修養與進修為理由隻身前往紐約，隔年也將唱片合約移轉至現在的唱片公司。有著這樣的經驗，也讓她的音樂視野更加寬廣。

2006年時，歌曲「A Perfect
Sky」搭配了當紅模特兒[蛯原友里所演出的](../Page/蛯原友里.md "wikilink")[資生堂](../Page/資生堂.md "wikilink")「ANESSA」廣告，而隨後發行的精選「Every
Single Day -Complete BONNIE PINK（1995-2006）-」在首周賣出了16萬張的好成績。

## 人物簡介

  - 因受到西洋音樂的影響，初期作品大都為英文歌曲。
  - 第一次買西洋專輯是在小學六年級的時候。當時買的專輯是[王子的Purple](../Page/王子_\(音樂家\).md "wikilink")
    Rain。她是因其封面有著華麗的色彩以及王子騎摩托車的英姿而買的。
  - 高中時參加了輕音樂部，非常喜歡唱歌。當時她只聽音樂而還未開始譜曲。
  - 大學時參加了朋友的樂團因而有了出道的契機。她的歌聲馬上受到了矚目而在合輯專輯中以本名（浅田香織）的名義發表了兩首作品。她說，在這之後，她便決定要出道了。
  - 錄音時，主要都在[瑞典](../Page/瑞典.md "wikilink")、[紐約](../Page/紐約.md "wikilink")、或[洛杉磯錄音](../Page/洛杉磯.md "wikilink")。
  - 2015年於20周年個唱時宣佈自己已婚數月。\[1\]
  - 2017年4月父親因病離世, 同月以44歲之齡誕下女兒。\[2\]\[3\]

## 個人作品

關於作品的全部細節，請參見[BONNIE PINK 作品](../Page/BONNIE_PINK_作品.md "wikilink")。

### 原創錄音室專輯

  - 1995: [Blue Jam](../Page/Blue_Jam.md "wikilink")
  - 1997: [Heaven's Kitchen](../Page/Heaven's_Kitchen.md "wikilink")
  - 1998: [evil and flowers](../Page/evil_and_flowers.md "wikilink")
  - 2000: [Let go](../Page/Let_go.md "wikilink")
  - 2001: [Just a Girl](../Page/Just_a_Girl.md "wikilink")
  - 2003: [Present](../Page/Present.md "wikilink")
  - 2004: [Even So](../Page/Even_So.md "wikilink")
  - 2005: [Golden Tears](../Page/Golden_Tears.md "wikilink")
  - 2007: [Thinking Out
    Loud](../Page/Thinking_Out_Loud_\(BONNIE_PINK專輯\).md "wikilink")
  - 2009: [ONE](../Page/ONE_\(BONNIE_PINK專輯\).md "wikilink")

### 其他專輯

  - 1997: [Lie Lie Lie](../Page/Lie_Lie_Lie.md "wikilink") (*原聲帶*)
  - 1999: [Bonnie's
    Kitchen\#1](../Page/Bonnie's_Kitchen#1.md "wikilink") (*精選輯*)
  - 1999: [Bonnie's
    Kitchen\#2](../Page/Bonnie's_Kitchen#2.md "wikilink") (*精選輯*)
  - 2002: [re\*PINK BONNIE PINK
    REMIXES](../Page/re*PINK_BONNIE_PINK_REMIXES.md "wikilink") (*混音專輯*)
  - 2003: [Pink in Red Live](../Page/Pink_in_Red_Live.md "wikilink")
    (*現場演場專輯*)
  - 2005: [REMINISCENCE](../Page/REMINISCENCE.md "wikilink") (*翻唱專輯*)
  - 2006: [Every Single Day -Complete BONNIE
    PINK（1995-2006）](../Page/Every_Single_Day_-Complete_BONNIE_PINK（1995-2006）.md "wikilink")
    (*精選輯*)
  - 2008: [CHAIN](../Page/CHAIN.md "wikilink") (*聖誕節迷你專輯*)

### 曾進入ORICON TOP 10的單曲

  - 1999: Daisy
  - 2006: A Perfect Sky
  - 2007: Anything For You
  - 2007: Water Me
  - 2008:

## 參與作品

### 客座主唱

1.  LADIES IN MOTION - freedom（作詞）、CRAZY FLIGHT（作詞）

      - 以本名名義參與的女性歌手合輯。

2.  George Martin - [披頭四](../Page/披頭四.md "wikilink")「BLACKBIRD」

      - 同時收錄於Bonnie's Kitchen\#2。

3.  WORLD FAMOUS - 「PLASTIC DUMMY」

4.  \- 「」

5.  DJ HASEBE - 「Get out\!」「red hot shoes」

6.  atami - 「Under the Sun」

7.  What's Love? -
    [松田聖子](../Page/松田聖子.md "wikilink")「[紅色的麝香豌豆](../Page/紅色的麝香豌豆.md "wikilink")」

8.  「HEDWIG AND THE ANGRY INCH TRIBUTE」 - 「ORIGIN OF LOVE」

9.  化學超男子 - 「Dance with me」（DefStar Records）

10. meister - 「above the clouds」（DefStar Records）

11. KREVA - 「YOU ARE THE No.1」

12. HONESTY - 「Tokyo Girl」

13. [約翰藍儂](../Page/披頭四.md "wikilink") - 「Revolution」

14. Fantastic Plastic Machine - 「a world without love」

15. 電影「」 - 披頭四「Please Mr.Postman」

16. m-flo loves BONNIE PINK - 「Love Song」（作詞、作曲）

17. Craig David - 「All The Way featuring BONNIE PINK」

18. Curly Giraffe - 「Spilt Milk featuring BONNIE PINK」

### 提供作曲

1.  [上原多香子](../Page/上原多香子.md "wikilink") - 「Ladybug」
2.  YOSHIKA - 「Call Me」
3.  244 ENDLI-x - 「Say Anything」

## 其他活動

  - \- 「」形象代言人（2005年8月1日－2006年1月31日）

  - Dream Power約翰藍儂特別演唱會（2005年10月7日）

  - 電影「[令人討厭的松子的一生](../Page/令人討厭的松子的一生#.E9.9B.BB.E5.BD.B1.md "wikilink")」-出演「土耳其浴女郎
    綾乃」

  - BONNIE PINK STYLE BOOK My life's in the bag ～～（2006年5月23日發行、講談社）ISBN
    4-06-213442-X

## 參考來源

## 外部連結

  - [BONNIE PINK官方網站](http://www.bonniepink.jp/)
  - [musicJAPANplus艺人资料库 - BONNIE
    PINK](http://www.musicjapanplus.jp/artistdb/?artist_id=37)
  - [日本華納音樂BONNIE PINK的頁面](http://wmg.jp/bonniepink/)

[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本創作歌手](../Category/日本創作歌手.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:大阪教育大學校友](../Category/大阪教育大學校友.md "wikilink")

1.
2.
3.