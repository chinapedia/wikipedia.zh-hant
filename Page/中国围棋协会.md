**中国围棋协会**简称：中围协，英文译名：(CHINESE WEIQI
ASSOCIATION缩写CWA)。协会是具有独立法人资格的全国性群众体育社会团体；是[中华全国体育总会的团体会员](../Page/中华全国体育总会.md "wikilink")。本协会由围棋管理人员、职业棋手、业余棋手及全国围棋单位自愿组成，是非营利性的，具有专业性的社会群众团体。是全国性群众体育组织，中华全国体育总会的团体会员，总部设[北京](../Page/北京.md "wikilink")。

## 主要职能

中国围棋协会于1962年在[安徽](../Page/安徽.md "wikilink")[合肥市正式成立](../Page/合肥市.md "wikilink")，职能是：研究制定发展计划、竞赛训练规定以及全年竞赛规程和规则，推动普及围棋运动；制订管理全国围棋法规；审批职业棋手、业余棋手及裁判员的技术等级；选拔和推荐国家队和推荐国家队及集训队运动员、教练员等。

## 理事成员

第一任中国围棋协会名誉主席为[陈毅](../Page/陈毅.md "wikilink")，任期为1962年-1971年。

第二任中国围棋协会名誉主席为[方毅](../Page/方毅.md "wikilink")，任期为1971年-1998年。

第一任中国围棋协会主席为[李梦华](../Page/李梦华.md "wikilink")（原国家体委主任，中华全国体育总会主席），任期为1962年-1988年。

第二任中国围棋协会主席为[陈祖德](../Page/陈祖德.md "wikilink")，任期从1988年至2006年。

第三任中国围棋协会主席为[王汝南](../Page/王汝南.md "wikilink")，任期从2006年至2017年。

第四任中国围棋协会主席为[林建超](../Page/林建超.md "wikilink")，任期从2017年至今。
2017年12月29日，中国围棋协会换届会议在[中国棋院举行](../Page/中国棋院.md "wikilink")，林建超接替王汝南，成为新任中国围棋协会主席。罗超毅、[聂卫平](../Page/聂卫平.md "wikilink")、孙光明、雷翔、[常昊担任副主席](../Page/常昊.md "wikilink")，罗超毅兼任秘书长。

## 参考资料

  - [搜狐体育·中国围棋协会简介](http://sports.sohu.com/20060712/n244221800.shtml)
  - [新浪体育·中国围棋协会换届林建超接任主席](http://sports.sina.com.cn/go/2017-12-29/doc-ifypyuve2937110.shtml)

## 外部链接

  - [中國棋院](https://archive.is/20121208131844/http://www.chinaqiyuan.com/)（[國家體育總局棋牌運動管理中心](http://games.sports.cn/)）
  - [中国棋院 -
    中國圍棋協會](https://archive.is/20121205145250/http://www.weiqi.cc/)

[Category:围棋协会](../Category/围棋协会.md "wikilink")
[Category:中华人民共和国组织](../Category/中华人民共和国组织.md "wikilink")
[Category:中國棋院](../Category/中國棋院.md "wikilink")
[Category:中华人民共和国体育协会](../Category/中华人民共和国体育协会.md "wikilink")
[Category:中国围棋协会](../Category/中国围棋协会.md "wikilink")