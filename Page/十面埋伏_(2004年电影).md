《**十面埋伏**》（）是一部2004年[张艺谋导演的](../Page/张艺谋.md "wikilink")[中国電影](../Page/中国電影.md "wikilink")，[程小东担任动作指导](../Page/程小东.md "wikilink")，由[刘德华](../Page/刘德华.md "wikilink")、[金城武和](../Page/金城武.md "wikilink")[章子怡主演](../Page/章子怡.md "wikilink")。该片是继2002年的《[英雄](../Page/英雄_\(电影\).md "wikilink")》之後，[张艺谋执导的第二部武俠片](../Page/张艺谋.md "wikilink")。

## 情节

故事講述[唐代](../Page/唐代.md "wikilink")[大中十三年](../Page/大中_\(唐朝\).md "wikilink")，[朝政腐敗](../Page/朝政.md "wikilink")，民間出現不少反官府的組織，以飛刀門的勢力最大，朝廷深以為患，飛刀門幫主柳雲飛在一場戰鬥中犧牲，但在新任幫主領導之下，飛刀門的勢力更大。於是[奉天縣兩大捕頭劉捕頭和金捕頭奉命於十日之內](../Page/奉天.md "wikilink")，將[黑社會組織](../Page/黑社會.md "wikilink")「飛刀門」新任幫主緝拿歸案。劉捕頭在[牡丹坊擒下飛刀門前幫主柳雲飛之女舞妓小妹](../Page/牡丹.md "wikilink")，並設下圈套，由金捕頭偽裝成隨風大俠，乘暗夜劫獄，救出小妹，成為小妹身邊的[臥底](../Page/臥底.md "wikilink")，希望能查出飛刀門的總部，以便一舉剿滅。逃亡途中，隨風對小妹呵護有加，因而彼此動了真情；而此案事實上是案外有案、局中有局，除金捕頭之外，包括小妹和劉捕頭在內的很多角色都有著雙重身份。本片將“臥底”這個身份發揮到極致。

## 角色

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>介紹</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a></p></td>
<td><p>劉捕头</p></td>
<td><p>飛刀門混入县衙的卧底</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金城武.md" title="wikilink">金城武</a><br />
（粵語配音：<a href="../Page/王祖藍.md" title="wikilink">王祖藍</a>）</p></td>
<td><p>金捕头</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/章子怡.md" title="wikilink">章子怡</a></p></td>
<td><p>歌妓小妹</p></td>
<td><p>假扮飛刀門前幫主柳雲飛之女</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宋丹丹.md" title="wikilink">宋丹丹</a></p></td>
<td><p>老鴇</p></td>
<td><p>飛刀門中代替大姐的人</p></td>
</tr>
</tbody>
</table>

## 创作

2003年9月11日开拍\[1\]。主要拍摄地包括[乌克兰](../Page/乌克兰.md "wikilink")[利沃夫](../Page/利沃夫.md "wikilink")\[2\]与克索沃国家森林公园[1](http://blog.renren.com/share/249763677/777849046)、北京和[重庆](../Page/重庆.md "wikilink")[永川](../Page/永川.md "wikilink")。精彩的竹林武打场面是在永川的著名景点——[茶山竹海拍的](../Page/茶山竹海.md "wikilink")\[3\]，而片中最后刘金两人的打斗戏份是在[乌克兰拍摄的](../Page/乌克兰.md "wikilink")\[4\]。

2004年1月14日于[北京电影制片厂正式关机](../Page/北京电影制片厂.md "wikilink")。后期工作在[澳大利亚制作完成](../Page/澳大利亚.md "wikilink")\[5\]\[6\]。

### 梅艷芳辭演

[梅艷芳生前曾獲張藝謀邀請參演](../Page/梅艷芳.md "wikilink")《十面埋伏》，後來因為患子宮頸癌病重，加上朋友和醫生極力勸喻，最終辭演。为了表达剧组的惋惜和悼念梅艳芳，导演张艺谋最终决定临时修改剧本，将大姐一角永远留给梅艳芳，而电影片尾序幕亦打出“谨以此片缅怀梅艳芳女士”的字幕。

## 原聲大碟

1.  Opening Title
2.  佳人曲 - 章子怡演唱
3.  The Echo Game
4.  The Peonyhouse
5.  Battle In The Forest
6.  Taking Her Hand
7.  Leos Eyes
8.  Lvers-Flower Garden-
9.  No Way Out
10. Lovers-Erfu Solo-
11. Farewell-1
12. Bamboo Forest
13. 十面埋伏
14. Leos Theme
15. Mei And Leo
16. The House Of Flying Daggers
17. Lovers-Mmei And Jin-
18. Farewell-2
19. Until The End
20. Title Song“Lovers”

## 评价

2004年5月亮相[戛纳国际电影节](../Page/戛纳国际电影节.md "wikilink")\[7\]获得了热烈的反响。\[8\]\[9\]观众看完后起立鼓掌达20分钟之久。\[10\]

在国内，一般影評家對此片的評價毁誉参半。不过该片在欧美等国上映后大多评价甚高，跟当年对《英雄》的评价情况相类似：在中国争议严重，在西方则普遍获得赞美之声，海外票房也很可观。电影获得了美国影评人的广泛称赞。\[11\]
在电影评论网站[Metacritic上](../Page/Metacritic.md "wikilink")，根据发表的37个评论，它收获了平均89分的高分。\[12\]在[烂番茄上](../Page/烂番茄.md "wikilink")，160个专业评论中有88%的评论给予了积极正面的评价\[13\]；[Metacritic把它列为](../Page/Metacritic.md "wikilink")2004年度好评排行第六的电影\[14\]。

2005年美国《[时代周刊](../Page/时代周刊.md "wikilink")》评选2004年全球十佳电影，将《英雄》和《十面埋伏》并列为第一名\[15\]。

## 票房

中国大陆票房为1.53亿人民币，2004年度票房排名第二，华语片排名第一。香港票房1500多万。

2004年12月3日在北美15家影院上映，首周获得US$397,472
(平均每厅收获$26,498)。总共在北美收获$11,050,094，是2004年北美票房第三高的外语片；北美以外地区收获US$81,751,003，因此全球票房为$92,801,097美元。

## 奖项

### 获奖

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/波士顿影评人协会.md" title="wikilink">波士顿影评人协会</a></strong></p></td>
<td><p>最佳导演</p></td>
<td><p>张艺谋</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳外语片(中国/香港)</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳摄影</p></td>
<td><p>赵小丁</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/洛杉矶影评人协会.md" title="wikilink">洛杉矶影评人协会</a></strong></p></td>
<td><p>最佳外语片 (中国/香港)</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/Motion_Picture_Sound_Editors.md" title="wikilink">Motion Picture Sound Editors</a></strong></p></td>
<td><p>外语片最佳音效剪辑</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/国家评论协会奖.md" title="wikilink">国家评论协会奖</a>（National Board of Review）</strong></p></td>
<td><p>杰出美术指导</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/国家影评人协会.md" title="wikilink">国家影评人协会</a>（National Society of Film Critics）</strong></p></td>
<td><p>最佳导演</p></td>
<td><p>张艺谋</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳摄影</p></td>
<td><p>赵小丁</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/卫星奖.md" title="wikilink">卫星奖</a></strong></p></td>
<td><p>最佳摄影</p></td>
<td><p>赵小丁</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳视觉效果</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>第11届中国电影<a href="../Page/华表奖.md" title="wikilink">华表奖</a></strong></p></td>
<td><p>优秀女演员</p></td>
<td><p>章子怡</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>优秀故事片技术奖</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 提名

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/第24届香港电影金像奖.md" title="wikilink">第24届香港电影金像奖</a></strong></p></td>
<td><p>最佳亚洲电影</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/第77届奥斯卡金像奖.md" title="wikilink">第77届奥斯卡金像奖</a></strong></p></td>
<td><p>最佳摄影</p></td>
<td><p><a href="../Page/赵小丁.md" title="wikilink">赵小丁</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/Academy_of_Science_Fiction,_Fantasy_&amp;_Horror_Films.md" title="wikilink">Academy of Science Fiction, Fantasy &amp; Horror Films</a></strong></p></td>
<td><p>最佳导演</p></td>
<td><p>张艺谋</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳女主角</p></td>
<td><p>章子怡</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳作曲</p></td>
<td><p>Emi Wada</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳幻想电影</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/英国电影和电视艺术学院.md" title="wikilink">英国电影和电视艺术学院</a></strong></p></td>
<td><p>最佳外语片</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳女主角</p></td>
<td><p>章子怡</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳视觉特效</p></td>
<td><p>Angie Lam, Andy Brown, Kirsty Millar &amp; Luke Hetherington</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳摄影</p></td>
<td><p>赵小丁</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳作曲</p></td>
<td><p>Emi Wada</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳剪辑</p></td>
<td><p>Long Cheng</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳化妆</p></td>
<td><p>Lee-na Kwan, Xiaohai Yang &amp; Siu-Mui Chau</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳美术指导</p></td>
<td><p><a href="../Page/霍廷霄.md" title="wikilink">霍廷霄</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳音效</p></td>
<td><p>陶经 &amp; Roger Savage</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/伦敦影评人协会.md" title="wikilink">伦敦影评人协会</a></strong></p></td>
<td><p>年度电影</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>年度电影导演</p></td>
<td><p>张艺谋</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>年度外语片</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/卫星奖.md" title="wikilink">卫星奖</a></strong></p></td>
<td><p>最佳艺术指导</p></td>
<td><p>Zhong Han</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳作曲</p></td>
<td><p>Emi Wada</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳剪辑</p></td>
<td><p>Long Cheng</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳外语动作片</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳音效剪辑</p></td>
<td><p>陶经</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/广播影评人协会奖.md" title="wikilink">广播影评人协会奖</a></strong></p></td>
<td><p>最佳外语片</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/在线影评人协会奖.md" title="wikilink">在线影评人协会奖</a></strong></p></td>
<td><p>最佳摄影</p></td>
<td><p>赵小丁</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳剪辑</p></td>
<td><p>Long Cheng</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳外语片(中国)</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/欧洲电影奖.md" title="wikilink">欧洲电影奖</a></strong></p></td>
<td><p>最佳非欧洲电影- Prix Screen International</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部链接

  -
  -
  -
  -
  -
  -
  - [中國新浪網官方網站 - 十面埋伏](http://ent.sina.com.cn/m/c/f/smmf/index.html)

[分类:中国电影作品](../Page/分类:中国电影作品.md "wikilink")
[分类:安乐电影](../Page/分类:安乐电影.md "wikilink")
[分类:新画面电影](../Page/分类:新画面电影.md "wikilink")

[Category:张艺谋电影](../Category/张艺谋电影.md "wikilink")
[Category:2004年電影](../Category/2004年電影.md "wikilink")
[Category:中港合拍電影](../Category/中港合拍電影.md "wikilink")
[Category:武俠片](../Category/武俠片.md "wikilink")
[Category:唐朝背景電影](../Category/唐朝背景電影.md "wikilink")
[Category:中國動作片](../Category/中國動作片.md "wikilink")
[Category:2000年代動作片](../Category/2000年代動作片.md "wikilink")
[Category:重庆取景电影](../Category/重庆取景电影.md "wikilink")
[Category:乌克兰取景电影](../Category/乌克兰取景电影.md "wikilink")
[Category:北京市取景电影](../Category/北京市取景电影.md "wikilink")
[Category:卧底题材电影](../Category/卧底题材电影.md "wikilink")
[Category:悲剧电影](../Category/悲剧电影.md "wikilink")
[Category:米拉麥克斯電影](../Category/米拉麥克斯電影.md "wikilink")
[Category:波士顿影评人协会奖最佳外语片](../Category/波士顿影评人协会奖最佳外语片.md "wikilink")
[Category:洛杉矶影评人协会奖最佳外语片](../Category/洛杉矶影评人协会奖最佳外语片.md "wikilink")

1.

2.
3.

4.

5.

6.

7.

8.

9.

10.

11.

12.
13.

14.

15. [美国《时代》杂志评2004年十大最佳电影
    《十面埋伏》与《英雄》并列第一](http://news.bandao.cn/newsdetail.asp?id=79420)