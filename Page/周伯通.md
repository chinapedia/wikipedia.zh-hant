**周伯通**，[金朝](../Page/金朝.md "wikilink")[山東](../Page/山東.md "wikilink")[寧海](../Page/寧海.md "wikilink")（今山東[牟平](../Page/牟平.md "wikilink")）人，知名[全真道](../Page/全真道.md "wikilink")[居士](../Page/居士.md "wikilink")。

## 史實

[王重陽始創](../Page/王重陽.md "wikilink")[全真道時曾受其資助](../Page/全真道.md "wikilink")。周伯通是全真道[北七真中](../Page/北七真.md "wikilink")[馬鈺](../Page/馬鈺.md "wikilink")、[譚處端](../Page/譚處端.md "wikilink")、[王處一](../Page/王處一.md "wikilink")、[郝大通](../Page/郝大通.md "wikilink")、[孫不二的](../Page/孫不二.md "wikilink")[同鄉](../Page/同鄉.md "wikilink")。當時在寧海、文登等地傳道的王重陽先後收了[丘處機](../Page/丘處機.md "wikilink")、譚處端、馬鈺、王處一、郝大通為徒。1169年春（金[大定九年巳丑四月](../Page/大定_\(金\).md "wikilink")），王重陽帶領馬、譚、丘、郝四弟子回到寧海，周伯通築庵請王重陽居住，名曰「金蓮堂」。不久馬鈺的妻子孫不二也在金蓮堂[出家](../Page/出家.md "wikilink")。八月，王重陽在金蓮堂成立「三教金蓮會」。隨後王重陽赴[福山](../Page/福山.md "wikilink")、[登州](../Page/登州.md "wikilink")、[萊州等地繼續傳道](../Page/萊州.md "wikilink")，收[劉處玄](../Page/劉處玄.md "wikilink")。

## [金庸武俠小說裡的周伯通](../Page/金庸.md "wikilink")

在[金庸的](../Page/金庸.md "wikilink")《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》及《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》中，周伯通被演繹為[王重陽](../Page/王重陽.md "wikilink")（書中為「[天下五絕](../Page/天下五絕.md "wikilink")」之首，有「[中神通](../Page/中神通.md "wikilink")」之號）的師弟，[全真七子的師叔](../Page/全真七子.md "wikilink")。為金庸小說中武功最絕頂的高手之一。
周伯通滿臉花影，髮鬚烏黑，雖然甚長，卻未見斑白。

登場回數:(射)16-19,22-23,33,35,38-39,(神)16-17,34-35,38-40

### 生平

周伯通天性純真，
又如孩童般任意妄為，常不看場面緩急狀況下胡鬧，故有「**[老頑童](../Page/老頑童.md "wikilink")**」之稱。他不拘小節，好武成癡，當被生平未見的武功吸引時，有甘願向施功者磕頭拜師求教的舉動。（如初見[歐陽鋒施展的輕功](../Page/歐陽鋒.md "wikilink")、[郭靖所練的](../Page/郭靖.md "wikilink")[降龍十八掌](../Page/降龍十八掌.md "wikilink")、[楊過獨創的](../Page/楊過.md "wikilink")[黯然銷魂掌等](../Page/黯然銷魂掌.md "wikilink")）。

與晚輩[郭靖](../Page/郭靖.md "wikilink")（《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》男主角）結拜為義兄弟。

天生是個樂天派，喜歡無拘無束地玩；雖未隨王重陽出家入道，卻在全真道門內深得道家養生要旨，長壽有道，逍遙自在地生活在天地之間。

武功方面，周伯通早已不下於「[天下五絕](../Page/天下五絕.md "wikilink")」，後來因自創絕學「[空明拳](../Page/空明拳.md "wikilink")」、「[雙手互搏](../Page/雙手互搏.md "wikilink")」令他的武功早與「天下五絕」不相伯仲，於教授[郭靖上下卷](../Page/郭靖.md "wikilink")《[九陰真經](../Page/九陰真經.md "wikilink")》時不知不覺間習得，令他武功幾屬天下第一，尤在師兄[王重陽之上](../Page/王重陽.md "wikilink")。於《神鵰俠侶》結尾時，繼承師兄王重陽「中神通」地位得「**中頑童**」之號，成為了「[天下五絕](../Page/天下五絕.md "wikilink")」之首。

### 背景

周伯通在王重陽成立[全真教之前兩人已經熟識](../Page/全真教.md "wikilink")，且由[王重陽授予武功](../Page/王重陽.md "wikilink")，在王重陽成為道士建立全真教之後，其熟知周伯通好動和痴於習武的習性不適於[道家清淨無為的要求](../Page/道家.md "wikilink")，因此不讓周伯通當[道士](../Page/道士.md "wikilink")，而這也符合周伯通的心意。

在第一次「[華山論劍](../Page/華山論劍.md "wikilink")」中，「中神通」[王重陽贏得武功秘笈](../Page/王重陽.md "wikilink")《[九陰真經](../Page/九陰真經.md "wikilink")》，數年後王重陽逝世，死前交代周伯通將經書上下冊分開藏匿。

周伯通在運送《[九陰真經](../Page/九陰真經.md "wikilink")》下冊至藏匿地點的途中遇到「[東邪](../Page/東邪.md "wikilink")」[黃藥師與其新婚](../Page/黃藥師.md "wikilink")[妻子](../Page/妻子.md "wikilink")[馮氏](../Page/黃夫人_\(射鵰英雄傳\).md "wikilink")，此二人施奸計騙得《[九陰真經](../Page/九陰真經.md "wikilink")》下冊經文的內容，周伯通中計下毀去經書。

日後周伯通得知其受騙於黃藥師二人，為防上冊經書被盜，於是攜帶《[九陰真經](../Page/九陰真經.md "wikilink")》上冊經書前往黃藥師居所[桃花島追討下冊經文](../Page/桃花島_\(金庸小說\).md "wikilink")，與黃藥師展開激烈交戰，周伯通最後不敵更遭黃藥師的「[彈指神通](../Page/彈指神通.md "wikilink")」射傷雙腳，從此被困於島上十五年。

周伯通首次出現於《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》第十六回「[九陰真經](../Page/九陰真經.md "wikilink")」裡頭，當故事男主角[郭靖在島上初遇到周伯通時](../Page/郭靖.md "wikilink")，他已經被困在島上十五年，這期間周伯通為打發時間自創七十二路「[空明拳](../Page/空明拳.md "wikilink")」及「[雙手互搏](../Page/雙手互搏.md "wikilink")」，同時也熟讀《[九陰真經](../Page/九陰真經.md "wikilink")》上冊經文，另外也從[郭靖處取得下冊經文](../Page/郭靖.md "wikilink")。

天真爛漫的周伯通與[郭靖成為結拜兄弟](../Page/郭靖.md "wikilink")，並且授予「[空明拳](../Page/空明拳.md "wikilink")」和「[雙手互搏](../Page/雙手互搏.md "wikilink")」，出於作弄心理周伯通在未告知郭靖的情形下要他詳記《[九陰真經](../Page/九陰真經.md "wikilink")》的經文內容，同時也教導其九陰真經內之武功，只是周伯通自己也在不知不覺的情形下習得了《[九陰真經](../Page/九陰真經.md "wikilink")》上所記載的武功。

此書首次交代周伯通與[瑛姑的感情糾紛](../Page/瑛姑.md "wikilink")，在小說最後周伯通雖然與瑛姑在[華山上重逢](../Page/華山.md "wikilink")，但是仍然逃脫，同時也替下部小說《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》預留了伏筆。

周伯通再次於《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》第十六回「殺父深仇」登場，淘氣的周伯通在[忽必烈的營帳戲弄](../Page/忽必烈.md "wikilink")[蒙古軍營的眾位武林高手](../Page/蒙古.md "wikilink")，其後因為在[絕情谷搗亂不慎被人用漁網擄去](../Page/絕情谷.md "wikilink")，使[楊過追逐至絕情谷](../Page/楊過.md "wikilink")，與[小龍女團圓](../Page/小龍女.md "wikilink")；逃脫後，周伯通繼續遊戲[江湖](../Page/江湖.md "wikilink")，並遇上[小龍女](../Page/小龍女.md "wikilink")，在她身上學會了指揮玉蜂的技藝。

周伯通雖然天性胡鬧，但反而收了穩重的[耶律齊為徒](../Page/耶律齊.md "wikilink")，令他感到很諷刺，於是吩咐耶律齊不得向人透露師父的身份，不過聰明絕頂的[黃蓉卻猜到了耶律齊的師父就是周伯通](../Page/黃蓉.md "wikilink")。

故事後期，在[楊過告知且從中斡旋下](../Page/楊過.md "wikilink")，周伯通得悉與[一燈大師和](../Page/一燈大師.md "wikilink")[瑛姑的交錯關係並跟二人相見](../Page/劉瑛.md "wikilink")，三人數十年來的恩怨纏綿亦終告和氣解決。

尾聲時，周伯通獲得位高於「[天下五絕](../Page/天下五絕.md "wikilink")」之首的「**中頑童**」稱號。

### 武功

  - 《[九陰真經](../Page/九陰真經.md "wikilink")》

武學巨著，包含精奇奧妙、變幻無方的最上乘武學，精奧無比，能將修真之士所遇的幻象之類，轉為神通，更糾正了道家武學偏重陰柔的流弊，實現了陰陽互濟、剛柔並重的武學最高境界。

:;──蛇行狸翻

  -
    即便在地上翻滾，也是靈動異常。
      - ──大伏魔拳法
    穩實剛猛之氣的拳法，招數神妙無方，拳力籠罩之下威不可擋。

<!-- end list -->

  - [左右互搏](../Page/雙手互搏.md "wikilink")

關鍵訣竅全在「分心二用」四字，雙手分使兩般武功，威力頓時倍增。

  - [空明拳](../Page/空明拳.md "wikilink")

天下至柔的拳，共七十二路，以虛擊實，以不足勝有餘，要旨是“以空而明”“空、柔、鬆”。

柔中帶韌的拳，拳力若有若無，出拳勁道要虛，身子柔軟如蟲，拳招糊里糊塗。

### 相關人物

  - 師兄：[王重陽](../Page/王重陽.md "wikilink")(實為師傅)
  - 徒弟：[耶律齊](../Page/耶律齊.md "wikilink")
  - 愛人：[瑛姑](../Page/瑛姑.md "wikilink")
  - [東邪](../Page/東邪.md "wikilink")：[黃藥師](../Page/黃藥師.md "wikilink")
  - [西狂](../Page/西狂.md "wikilink")：[楊過](../Page/楊過.md "wikilink")
  - [南帝](../Page/南帝.md "wikilink")（後改為南僧）：[一燈大師](../Page/一燈大師.md "wikilink")
  - [北俠](../Page/北俠.md "wikilink")：[郭靖](../Page/郭靖.md "wikilink")(亦是義弟)

## 影视形象

### 电视剧

  - 1976 [秦煌](../Page/秦煌.md "wikilink")
    香港佳视电视剧《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1976 [秦煌](../Page/秦煌.md "wikilink")
    香港佳视电视剧《[神雕侠侣](../Page/神雕侠侣.md "wikilink")》
  - 1983 [秦煌](../Page/秦煌.md "wikilink")
    香港无线电视剧《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1983 [秦煌](../Page/秦煌.md "wikilink")
    香港无线电视剧《[神雕侠侣](../Page/神雕侠侣.md "wikilink")》
  - 1984 [龙冠武](../Page/龙冠武.md "wikilink")
    台湾中视电视剧《[神雕侠侣](../Page/神雕侠侣.md "wikilink")》
  - 1988 [龙冠武](../Page/龙冠武.md "wikilink")
    台湾中视电视剧《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1992 [黎耀祥](../Page/黎耀祥.md "wikilink")
    香港无线电视剧《[中神通王重阳](../Page/中神通王重阳.md "wikilink")》
  - 1993 [黎耀祥](../Page/黎耀祥.md "wikilink")
    香港无线电视剧《[射雕英雄传之南帝北丐](../Page/射雕英雄传之南帝北丐.md "wikilink")》
  - 1994 [黎耀祥](../Page/黎耀祥.md "wikilink")
    香港无线电视剧《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1995 [黎耀祥](../Page/黎耀祥.md "wikilink")
    香港无线电视剧《[神雕侠侣](../Page/神雕侠侣.md "wikilink")》
  - 1998
    [麦皓为新加坡电视剧](../Page/麦皓为.md "wikilink")《[神雕侠侣](../Page/神雕侠侣.md "wikilink")》
  - 2003 [赵亮](../Page/赵亮.md "wikilink")
    中國电视剧《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 2006 [赵亮](../Page/赵亮.md "wikilink")
    中國电视剧《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 2008
    [李彧中國电视剧](../Page/李彧.md "wikilink")《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 2015 [周德华](../Page/周德华.md "wikilink")
    中國电视剧《[神雕侠侣](../Page/神雕侠侣.md "wikilink")》
  - 2017 [宁文彤](../Page/宁文彤.md "wikilink")
    中國电视剧《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》

### 电影

  - 1958 [刘少文](../Page/刘少文.md "wikilink")
    香港峨嵋电影《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1977 [郭追](../Page/郭追.md "wikilink")
    香港邵氏电影《[射雕英雄传](../Page/射雕英雄传.md "wikilink")》
  - 1994 [刘嘉玲](../Page/刘嘉玲.md "wikilink")
    香港电影《[东成西就](../Page/东成西就.md "wikilink")》

## 參考文獻

  - 《金蓮正宗仙源像傳》
  - 《終南山重陽真人全真教祖碑》

[C](../Category/射鵰英雄傳角色.md "wikilink")
[C](../Category/神鵰侠侣角色.md "wikilink")
[射](../Category/金庸筆下真實人物.md "wikilink")