[MorpholinoHeteroduplex.png](https://zh.wikipedia.org/wiki/File:MorpholinoHeteroduplex.png "fig:MorpholinoHeteroduplex.png")

**嗎啉基**又稱**嗎啉**（英語：**Morpholino**），是一種用來修飾[基因表現的分子](../Page/基因表現.md "wikilink")，嗎啉基[寡核苷酸是一種](../Page/寡核苷酸.md "wikilink")[反義技術](../Page/反義.md "wikilink")，可用來阻礙其他分子與特定核酸序列的結合。可阻擋[RNA上約](../Page/RNA.md "wikilink")25個鹼基的區域。此分子可應用在一些模式生物的研究上，包括[小鼠](../Page/小鼠.md "wikilink")、[斑馬魚](../Page/斑馬魚.md "wikilink")、[非洲爪蟾等](../Page/非洲爪蟾.md "wikilink")\[1\]。

除了組合成寡核苷酸之外，嗎啉基也可能出現在其他分子中。但「Morpholino」指其寡核苷酸形式，也就是磷醯二胺嗎啉代寡核苷酸（phosphorodiamidate
morpholino
oligomers，PMO）。吗啉基是对天然核苷酸结构重新设计获得的合成分子。长度多为25个碱基，以标准核苷酸碱基配对的方式与RNA互补结合。在结构上来看，吗啉基与DNA的区别在于尽管前者有标准的核苷酸碱基，这些碱基却连接在吗啉环上，而非脱氧核糖环。吗啉通过占据体内mRNA与其它分子作用位点来发生作用。

## 參見

  - [嗎啉](../Page/嗎啉.md "wikilink")

## 參考文獻

## 延伸閱讀

  - [Wiley-Liss, Inc. Special Issue: Morpholino Gene Knockdowns of
    genesis Volume 30, Issue 3 Pages 89-200
    (July 2001)](http://www3.interscience.wiley.com/cgi-bin/issuetoc?ID=85006212).
    A special issue of *Genesis*, comprised of a series of peer-reviewed
    short papers utilizing morpholino knock downs of gene function in
    various animal and tissue culture systems.

<!-- end list -->

  -
[Category:氨基磷酸酯](../Category/氨基磷酸酯.md "wikilink")
[Category:分子遺傳學](../Category/分子遺傳學.md "wikilink")
[Category:核酸](../Category/核酸.md "wikilink")
[Category:吗啉](../Category/吗啉.md "wikilink")

1.