[AMC_Empire_25_NYC.jpg](https://zh.wikipedia.org/wiki/File:AMC_Empire_25_NYC.jpg "fig:AMC_Empire_25_NYC.jpg")[第42街Empire](../Page/42街_\(曼哈頓\).md "wikilink")
Theatre\]\]
[AMC_Pacific_Place_2017.jpg](https://zh.wikipedia.org/wiki/File:AMC_Pacific_Place_2017.jpg "fig:AMC_Pacific_Place_2017.jpg")[太古廣場AMC電影院](../Page/太古廣場.md "wikilink")，在2016年翻新\]\]
[HK_Festival_Walk_amc_cinema_200907.jpg](https://zh.wikipedia.org/wiki/File:HK_Festival_Walk_amc_cinema_200907.jpg "fig:HK_Festival_Walk_amc_cinema_200907.jpg")
**AMC電影院**（**AMC Theatres**，**A**merican
**M**ulti-**C**inema）是一間美國的連鎖[電影院](../Page/電影院.md "wikilink")，屬於[中國](../Page/中國.md "wikilink")[大連萬達集團旗下](../Page/大連萬達集團.md "wikilink")[子公司](../Page/子公司.md "wikilink")。AMC是世界最大的连锁院线，在[美國市場佔有率第一](../Page/美國.md "wikilink")，共有8218块屏幕和661家影院。\[1\]除了美國以外，AMC在[英國](../Page/英國.md "wikilink")、[法國和](../Page/法國.md "wikilink")[香港各有據點](../Page/香港.md "wikilink")。此外其母公司万达院线在[中國則擁有](../Page/中國.md "wikilink")348余家電影院。\[2\]AMC曾進駐巴西、加拿大、智利、日本、墨西哥、葡萄牙、南韓、西班牙、瑞典和匈牙利等國。\[3\]\[4\]\[5\]AMC的美國總公司位於[堪薩斯城](../Page/堪薩斯城_\(密蘇里州\).md "wikilink")。

AMC娱乐公司财报显示，公司2018财年第一财季营业收入为13.836亿美元，同比上涨8%，其中净利润为1770万美元。目前，AMC在美国会员数已经超过了1300万。

## 收購

2012年5月21日，[萬達集團以](../Page/萬達集團.md "wikilink")26億美元（201.86億港元）收購該公司100%股權，這是自從[吉利汽車收購](../Page/吉利汽車.md "wikilink")[沃尔沃汽车後](../Page/沃尔沃汽车.md "wikilink")，中国企业进行的第二次海外最大規模收購行動\[6\]。

2016年3月3日，**AMC電影院**以11億美元現金收購[卡邁克電影院](../Page/卡麦克院线.md "wikilink")（Carmike
Cinemas）組成美國以至全球的最大連鎖院線。

2016年7月12日，**AMC電影院**以9.21億英鎊(約94億港元)收購歐洲第一大院線 [Odeon &
UCI院線](../Page/欧典院线.md "wikilink")。 Odeon &
UCI院線是歐洲最大電影院線，擁有242家影院2236塊銀幕，近12個月收入約11.56億美元，佔據歐洲約20%的市場份額。

2016年7月25日，**AMC電影院**上調对[卡邁克電影院](../Page/卡麦克院线.md "wikilink")（Carmike
Cinemas）的收購價，由每股30美元提高到每股33.06美元或1.0819份AMC股份。總交易額約12億美元，包括5.85億現金、2.5億股票和債務。

2017年1月23日，**AMC電影院**以9.3億美元收購北欧最大院線北欧院线集团 (Nordic Cinema Group)。
北欧院线集团是北欧市场份额排名第一的院线，在北欧50个主要大中城市拥有118家影院、664块银幕。

## 香港的AMC

現時香港有一間AMC戲院：

  - AMC Pacific
    Place（[金鐘](../Page/金鐘.md "wikilink")[太古廣場](../Page/太古廣場.md "wikilink")，6間影院共599個座位）

（House 1：39, House 2：126, House 3：111, House 4：72, House 5：109, House
6：142）

位於九龍塘的AMC又一城是香港第一家AMC電影院，於1998年12月17日開幕。當時其豪華的設備遠超本地其他影院，為本港高檔次影院的先驅之一。此影院開幕時共有11個放映廳，可容納1,950人，後經過裝修改為現有7個放映院，約1,200個座位。唯2015年11月27日AMC突然於官方
Facebook
宣佈，由於租約期滿，營業至2016年1月3日，由[洲立影藝旗下的MCL院線接手營業](../Page/洲立影藝.md "wikilink")。\[7\]。

2006年1月，AMC將於香港的電影院業務交予Suntech International Management Limited（下稱
Suntech）管理，但影院名稱維持不變。Suntech屬[安樂影片有限公司旗下](../Page/安樂影片有限公司.md "wikilink")，該公司亦在香港經營[百老匯院線](../Page/百老匯院線.md "wikilink")。

同年12月9日，位於[金鐘](../Page/金鐘.md "wikilink")[太古廣場的AMC](../Page/太古廣場.md "wikilink")
Pacific Place開幕。此影院共有六個放映廳約600個座位，包括一個只有39個座位的精選影院，可供舉行私人聚會。AMC Pacific
Place貫徹 AMC 於本地的高檔次形象，坐位舒適寬闊，音響設備完善，力足與港島區另一高價影院[Palace
IFC看齊](../Page/Palace_IFC.md "wikilink")\[8\]。

## 參考

## 參見

  - [安樂影片有限公司](../Page/安樂影片有限公司.md "wikilink")
  - [香港戲院列表](../Page/香港戲院列表.md "wikilink")
  - [香港已結業戲院列表](../Page/香港已結業戲院列表.md "wikilink")

## 外部連結

  - [美國AMC](https://www.amctheatres.com)
  - [香港AMC](http://www.amccinemas.com.hk/)
  - [AMC又一城結業](https://hk.news.yahoo.com/%E5%8F%88-%E5%9F%8Eamc%E6%88%B2%E9%99%A2%E5%AE%A3%E5%B8%83%E6%98%8E%E5%B9%B41%E6%9C%88%E7%B5%90%E6%A5%AD-135600260.html)

[Category:香港電影院線](../Category/香港電影院線.md "wikilink")
[Category:加拿大電影院](../Category/加拿大電影院.md "wikilink")
[Category:美國連鎖電影院公司](../Category/美國連鎖電影院公司.md "wikilink")
[Category:密蘇里州公司](../Category/密蘇里州公司.md "wikilink")
[Category:1920年成立的公司](../Category/1920年成立的公司.md "wikilink")
[Category:万达集团](../Category/万达集团.md "wikilink")

1.  [NATO - Statistics -Top Ten
    Circuits](http://www.natoonline.org/statisticscircuits.htm)
2.
3.  "[AMC International -
    Locations](https://web.archive.org/web/20010128100200/http://amctheatres.com/international/int_locations.html)."
    *AMC Theatres*. January 28, 2001.
4.  "[AMC, Loews Cineplex to
    merge](http://www.usatoday.com/money/media/2005-06-21-loews-amc_x.htm)."
    *[USA Today](../Page/USA_Today.md "wikilink")*. June 21, 2005.
5.  "[AMC International -
    Locations](https://web.archive.org/web/20030608143432/http://www.amctheatres.com/international/int_locations.html)."
    *AMC Theatres*. June 8, 2003.
6.  [大連萬達201億購AMC
    東方日報 2012-05-22](http://orientaldaily.on.cc/cnt/finance/20120522/00202_043.html)
7.  [AMC又一城結業
    星島日報 2015年11月27日](https://hk.news.yahoo.com/%E5%8F%88-%E5%9F%8Eamc%E6%88%B2%E9%99%A2%E5%AE%A3%E5%B8%83%E6%98%8E%E5%B9%B41%E6%9C%88%E7%B5%90%E6%A5%AD-135600260.html)
8.  [香港貿發局屬下網頁內有關 AMC Pacific Place
    開業之新聞](http://www.hkfilmart.com/newsread.asp?newsid=1950)