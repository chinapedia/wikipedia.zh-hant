**简正坐标**又叫做正则坐标，是用来描述和计算分子内部运动的一个坐标体系。

## 简正坐标的导出

用[质量加权坐标表示的](../Page/质量加权坐标.md "wikilink")[分子内部运动的](../Page/分子.md "wikilink")[动能](../Page/动能.md "wikilink")：

\[T=\frac{1}{2}\sum_{i=1}^{3N} \left( \frac{dq_i}{dt} \right)^2\]

用质量加权坐标表示的分子内部[势能](../Page/势能.md "wikilink")

\[V=\frac{1}{2}\sum_{i,j} f_{ij} q_i q_j\]

其中势能公式中用到的力常数可以用矩阵的形式表示出来：

\[\mathfrak{F}=\begin{bmatrix} f_{1,1} & \cdots & f_{1,3N} \\ \vdots &
\ddots & \vdots \\ f_{3N,1} & \cdots & f_{3N,3N}\end{bmatrix}\]

由力常数的数学表达式可以知道\(f_{ij}=f_{ji}\)因而矩阵\(\mathfrak{F}\)为一个[正交矩阵通过](../Page/正交矩阵.md "wikilink")[酉变换可以把矩阵](../Page/酉变换.md "wikilink")\(\mathfrak{F}\)变形成为对角矩阵的形式：\(\boldsymbol{\Lambda}\)。则有：

\[\mathfrak{F}=\mathfrak{L}^{-1} \boldsymbol{\Lambda} \mathfrak{L}\]

且可以证明其中的过渡矩阵\(\mathfrak{L}\)为正交矩阵，有\(\mathfrak{L}^{-1}= \mathfrak{L}^T\)

则用矩阵乘法的方式表示分子内部势能：

\[2V=Q^T \mathfrak{F} Q\]

其中的Q为由分子内所有质量加权坐标构成的列矩阵

\[\begin{matrix}2V&=& Q^T \mathfrak{F} Q
&=& Q^T \mathfrak{L}^T \boldsymbol{\Lambda} \mathfrak{L}  Q
&=& (\mathfrak{L} Q)^T \boldsymbol{\Lambda} \mathfrak{L} Q
&=& \mathfrak{Q}^T \boldsymbol{\Lambda} \mathfrak{Q}
\end{matrix}\]

其中的\(\mathfrak{Q}=\mathfrak{L} Q\)，是一个列矩阵，它的每一个矩阵元都是分子所有质量加权坐标的线性组合，总的矩阵元的数量恰巧等于质量加权坐标的个数，这些矩阵元就被称作**简正坐标**

## 简正坐标的物理意义

简正坐标是分子所有质量加权坐标的线性组合，每个质量加权坐标表征的是构成分子的一个原子在一个坐标方向上的振动特性。因此每个简正坐标表征的是一套分子内部运动的组合，而这种组合一定是符合分子所属的[对称性群的一个](../Page/对称性群.md "wikilink")[对称类的](../Page/对称类.md "wikilink")。

画出一个分子可能的结构，就能够根据这个结构求算出分子的简正坐标，通过考查分子的简正坐标可以了解分子内部运动的能量，可以预测分子在[红外光谱和](../Page/红外光谱.md "wikilink")[拉曼光谱中的特征吸收峰](../Page/拉曼光谱.md "wikilink")。

## 参见

  - [质量加权坐标](../Page/质量加权坐标.md "wikilink")

[category:计算化学](../Page/category:计算化学.md "wikilink")

[Category:坐标系](../Category/坐标系.md "wikilink")