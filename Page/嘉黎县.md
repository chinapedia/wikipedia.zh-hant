**嘉黎县**（）是[中華人民共和國](../Page/中華人民共和國.md "wikilink")[西藏自治区](../Page/西藏自治区.md "wikilink")[那曲市的一個](../Page/那曲市.md "wikilink")[縣](../Page/縣.md "wikilink")。十一世班禅喇嘛即出生于此地。「嘉黎」在[藏语意为](../Page/藏语.md "wikilink")“神山”。

## 行政区划

下辖：\[1\] 。

## 参考资料

  - A. Gruschke: *The Cultural Monuments of Tibet’s Outer Provinces:
    Kham - Volume 1. The Xizang Part of Kham (TAR)*, White Lotus Press,
    Bangkok 2004. ISBN 974-480-049-6
  - Tsering Shakya: *The Dragon in the Land of Snows. A History of
    Modern Tibet Since 1947*, London 1999, ISBN 0-14-019615-3

[嘉黎县](../Category/嘉黎县.md "wikilink")
[Category:那曲地区县份](../Category/那曲地区县份.md "wikilink")
[Category:西康省縣份](../Category/西康省縣份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.