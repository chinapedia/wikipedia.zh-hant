**宮村優子**（），[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市生](../Page/神戶市.md "wikilink")，[日本女](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")、[演員](../Page/演員.md "wikilink")，O型[血](../Page/血型.md "wikilink")。

## 人物介紹

  - 暱稱「」，據說是由在[愛天使傳說中共同演出的](../Page/愛天使傳說.md "wikilink")[野上尤加奈所命名的](../Page/野上尤加奈.md "wikilink")。
  - 曾為[卡通](../Page/卡通.md "wikilink")《根性戰隊》擔任主角，並為其中文版唱主題曲。她那爆笑的走音[廣東話在不少人心中留下深刻印象](../Page/廣東話.md "wikilink")。
  - 代表作有[動畫](../Page/動畫.md "wikilink")《[名偵探柯南](../Page/名偵探柯南.md "wikilink")》裡的[遠山和葉和](../Page/遠山和葉.md "wikilink")《[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")》中的[明日香](../Page/惣流·明日香·蘭格雷.md "wikilink")。也曾為[韓國電影](../Page/韓國電影.md "wikilink")《[火山高校](../Page/火山高校.md "wikilink")》的日語版配音。
  - 某一時期與漫畫家"ナカタニD."結婚，由於許多事情閃電離婚。
  - 在電影《[大逃殺](../Page/大逃殺_\(電影\).md "wikilink")》中演出解說員一角。
  - 2004年與同隸屬於JAE的男性代演[關隆行再婚](../Page/關隆行.md "wikilink")。同年9月1日女兒出生。
  - 有時也會以本名**關優子**名義擔任音響監督（如《[LOVELESS](../Page/LOVELESS.md "wikilink")》等）。
  - 2006年現在於「[大阪動畫學校](../Page/大阪動畫學校.md "wikilink")」擔任動畫聲優科二年級的特別講師。
  - 2016年7月18日在個人的推特上發表離婚的消息。\[1\]

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 1994年

<!-- end list -->

  - [勇者警察](../Page/勇者警察.md "wikilink")（レジーナ）※出道的第一部作品

<!-- end list -->

  - 1995年

<!-- end list -->

  - [黃金勇者](../Page/黃金勇者.md "wikilink")（クリス）
  - [十二生肖爆烈戰士](../Page/十二生肖爆烈戰士.md "wikilink")（スフレ）
  - [愛天使傳說](../Page/愛天使傳說.md "wikilink")（**珠野雛菊／天使德姬**）
  - [新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")（**[惣流·明日香·蘭格雷](../Page/惣流·明日香·蘭格雷.md "wikilink")**）
  - [Mojacko](../Page/Mojacko.md "wikilink")（アップル）

<!-- end list -->

  - 1996年

<!-- end list -->

  - [VS騎士檸檬汽水&40炎](../Page/VS騎士檸檬汽水&40炎.md "wikilink")（**-{冰淇淋}-**）
  - [妖精狩獵者](../Page/妖精狩獵者.md "wikilink")（**井上律子**）

<!-- end list -->

  - 1997年

<!-- end list -->

  - [貓狐警探](../Page/貓狐警探.md "wikilink")（**笹原夏姬**）
  - [CLAMP學園偵探團](../Page/CLAMP學園偵探團.md "wikilink")（**大川詠心**）
  - [妖精狩獵者II](../Page/妖精狩獵者.md "wikilink")（**井上律子**）
  - [烙印勇士](../Page/烙印勇士.md "wikilink")（**卡思嘉**）
  - [超魔神英雄傳](../Page/超魔神英雄傳.md "wikilink")（スズメ）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [星方武俠](../Page/星方武俠.md "wikilink")（エイシャ・クランクラン）
  - [南海奇皇](../Page/南海奇皇.md "wikilink")（**島原海潮**）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（**[遠山和葉](../Page/遠山和葉.md "wikilink")**）
  - [螺絲俠'98](../Page/螺絲俠'98.md "wikilink")（マイラ）(なお、この宮村が登場する話では[新世紀福音戰士で競演したことのある](../Page/新世紀福音戰士.md "wikilink")[子安武人が戀人役](../Page/子安武人.md "wikilink")、[立木文彥が父親役で登場する](../Page/立木文彥.md "wikilink")。)

<!-- end list -->

  - 1999年

<!-- end list -->

  - [宇宙戰艦山本洋子](../Page/宇宙戰艦山本洋子.md "wikilink")（**白鳳院綾乃·伊利莎白**）
  - [月光假面](../Page/月光假面.md "wikilink")（**山本ナオト（初代）**）
  - [麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")（水樹奈奈子）

<!-- end list -->

  - 2000年

<!-- end list -->

  - [神奇寶貝](../Page/神奇寶貝.md "wikilink")（[小茜](../Page/小茜.md "wikilink")）
  - [NieA 7](../Page/NieA_7.md "wikilink")（ニア）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [アストロボーイ・鉄腕アトム](../Page/アストロボーイ・鉄腕アトム.md "wikilink")（ミミ）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [舞-HiME](../Page/舞-HiME.md "wikilink")（愛莉莎·西爾斯）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [新幹線戰士](../Page/新幹線戰士.md "wikilink")（[惣流·明日香·蘭格雷](../Page/惣流·明日香·蘭格雷.md "wikilink")）（客串）

### OVA

  - [魔域幽靈](../Page/魔域幽靈.md "wikilink")（[レイレイ](../Page/レイレイ.md "wikilink")）
  - [宇宙戰艦山本洋子](../Page/宇宙戰艦山本洋子.md "wikilink")（白鳳院綾乃エリザベス）
  - [でたとこプリンセス](../Page/でたとこプリンセス.md "wikilink")（ラピス）
  - [ぼくのマリー](../Page/ぼくのマリー.md "wikilink")（**雁狩マリ**）
  - [綾音ちゃんハイキック\!](../Page/綾音ちゃんハイキック!.md "wikilink")（**三井綾音**）
  - [名偵探柯南 追蹤消失的鑽石！柯南、平次 vs.
    基德](../Page/名偵探柯南.md "wikilink")（[遠山和葉](../Page/遠山和葉.md "wikilink")）
  - [名偵探柯南 16個嫌疑人](../Page/名偵探柯南.md "wikilink")（遠山和葉）
  - [名偵探柯南 柯南、平次與消失的少年](../Page/名偵探柯南.md "wikilink")（遠山和葉）
  - [課本沒教的事](../Page/課本沒教的事.md "wikilink")（**白樺綾**）
  - [VS騎士檸檬汽水&40炎 FRESH](../Page/VS騎士檸檬汽水&40炎.md "wikilink")（冰淇淋）
  - [ヴァリアブル・ジオ](../Page/ヴァリアブル・ジオ.md "wikilink")（**八島聰美**）
  - [美少女游擊隊](../Page/美少女游擊隊.md "wikilink")（榊志穗子）
  - [天使禁獵區](../Page/天使禁獵區.md "wikilink")（九雷）

### 劇場版動畫

**1997年**

  - [新世紀福音戰士劇場版：THE END OF
    EVANGELION](../Page/新世紀福音戰士劇場版：THE_END_OF_EVANGELION.md "wikilink")（[惣流·明日香·蘭格雷](../Page/惣流·明日香·蘭格雷.md "wikilink")）
  - [新世紀福音戰士劇場版：死與新生](../Page/新世紀福音戰士劇場版：死與新生.md "wikilink")（[惣流·明日香·蘭格雷](../Page/惣流·明日香·蘭格雷.md "wikilink")）
  - [神劍闖江湖 給維新志士的鎮魂歌](../Page/神劍闖江湖.md "wikilink")（高槻朱鷺）

**1998年**

  - [新機動戰記鋼彈W 無盡的華爾茲](../Page/新機動戰記鋼彈W_無盡的華爾茲.md "wikilink")（女の子A）

**1999年**

  - [名偵探柯南
    世紀末的魔術師](../Page/名偵探柯南_世紀末的魔術師.md "wikilink")（[遠山和葉](../Page/遠山和葉.md "wikilink")）

**2003年**

  - [名偵探柯南
    迷宮的十字路](../Page/名偵探柯南_迷宮的十字路.md "wikilink")（[遠山和葉](../Page/遠山和葉.md "wikilink")）

**2006年**

  - [名偵探柯南
    偵探們的鎮魂歌](../Page/名偵探柯南_偵探們的鎮魂歌.md "wikilink")（[遠山和葉](../Page/遠山和葉.md "wikilink")）

**2007年**

  - [福音戰士新劇場版：序](../Page/福音戰士新劇場版：序.md "wikilink") （式波・明日香・蘭格雷）

**2009年**

  - [福音戰士新劇場版：破](../Page/福音戰士新劇場版：破.md "wikilink")（式波・明日香・蘭格雷）
  - [名偵探柯南
    漆黑的追跡者](../Page/名偵探柯南_漆黑的追跡者.md "wikilink")（[遠山和葉](../Page/遠山和葉.md "wikilink")）

**2010年**

  - [名偵探柯南
    天空的遇難船](../Page/名偵探柯南_天空的遇難船.md "wikilink")（[遠山和葉](../Page/遠山和葉.md "wikilink")）

**2012年**

  - [福音戰士新劇場版：Q](../Page/福音戰士新劇場版：Q.md "wikilink")（式波・明日香・蘭格雷）

**2013年**

  - [名偵探柯南
    遠海的偵探](../Page/名偵探柯南_遠海的偵探.md "wikilink")（[遠山和葉](../Page/遠山和葉.md "wikilink")）

**2017年**

  - [名偵探柯南
    唐紅的戀歌](../Page/名偵探柯南_唐紅的戀歌.md "wikilink")（[遠山和葉](../Page/遠山和葉.md "wikilink")）

### 遊戲

  - [お嬢様特急](../Page/お嬢様特急.md "wikilink")（伊東小麥）
  - [王國之心 Re:chain of
    memories](../Page/王國之心_Re:chain_of_memories.md "wikilink")（ラクシーヌ）
  - [王國之心 358/2Days](../Page/王國之心_358/2Days.md "wikilink")（ラクシーヌ）
  - [金田一少年之事件簿系列遊戲](../Page/金田一少年之事件簿.md "wikilink")（七瀨美雪）
      - 金田一少年之事件簿 悲報島 新的慘劇
      - 金田一少年之事件簿2 地獄遊園殺人事件
      - 金田一少年之事件簿3 青龍傳說殺人事件
  - [新紀幻想スペクトラルソウルズII](../Page/新紀幻想スペクトラルソウルズII.md "wikilink")（ヒロ）
  - [新世紀福音戰士系列遊戲](../Page/新世紀福音戰士.md "wikilink")（[惣流·明日香·蘭格蕾](../Page/惣流·明日香·蘭格蕾.md "wikilink")）
      - [新世紀福音戰士
        綾波育成計畫和明日香補完計畫](../Page/新世紀福音戰士_綾波育成計畫和明日香補完計畫.md "wikilink")（[惣流·明日香·蘭格蕾](../Page/惣流·明日香·蘭格蕾.md "wikilink")
      - [新世紀福音戰士 鋼鐵戀人](../Page/新世紀福音戰士_鋼鐵戀人.md "wikilink")
      - 新世紀福音戰士 鋼鐵戀人2nd
      - [名偵探福音戰士](../Page/名偵探福音戰士.md "wikilink")
  - [超級機器人大戰系列遊戲](../Page/超級機器人大戰.md "wikilink")（主要為惣流·明日香·蘭格蕾）
      - [超級機器人大戰F](../Page/超級機器人大戰F.md "wikilink")（グレース・ウリジン、惣流·明日香·蘭格蕾）
      - [超級機器人大戰F完結篇](../Page/超級機器人大戰F完結篇.md "wikilink")（グレース・ウリジン、惣流·明日香·蘭格蕾）
      - [超級機器人大戰α](../Page/超級機器人大戰α.md "wikilink")（惣流·明日香·蘭格蕾）
      - [超級機器人大戰α for
        Dreamcast](../Page/超級機器人大戰α_for_Dreamcast.md "wikilink")（惣流·明日香·蘭格蕾）
      - [超級機器人大戰Scramble
        Commander](../Page/超級機器人大戰Scramble_Commander.md "wikilink")（惣流·明日香·蘭格蕾）
      - [超級機器人大戰MX](../Page/超級機器人大戰MX.md "wikilink")（惣流·明日香·蘭格蕾）
      - [第3次超級機器人大戰α
        終焉之銀河](../Page/第3次超級機器人大戰α_終焉之銀河.md "wikilink")（惣流·明日香·蘭格蕾）
      - [超級機器人大戰MX 攜帶版](../Page/超級機器人大戰MX.md "wikilink")（惣流·明日香·蘭格蕾）
      - [超級機器人大戰V](../Page/超級機器人大戰V.md "wikilink")（惣流·明日香·蘭格蕾）
  - [快打旋風ZERO](../Page/快打旋風ZERO.md "wikilink")（[春麗](../Page/春麗.md "wikilink")）
  - [聖魔戰記系列遊戲](../Page/聖魔戰記.md "wikilink")（ヒロ）
      - [聖魔戰記3](../Page/聖魔戰記3.md "wikilink")（カルチャ）
  - [ソウルエッジ（プレイステーション版）](../Page/ソウルエッジ.md "wikilink")（ソン・ミナ）
  - [テイルコンチェルト](../Page/テイルコンチェルト.md "wikilink")（アリシア・プリス）
  - [ドキドキプリティリーグ](../Page/ドキドキプリティリーグ.md "wikilink")（千堂アリサ）
  - [ドラッグオンドラグーン](../Page/ドラッグオンドラグーン.md "wikilink")（フェアリー）
  - [バーニングレンジャー](../Page/バーニングレンジャー.md "wikilink")（ティリス）
  - [機甲戰線REVELLION](../Page/機甲戰線.md "wikilink")（菲·馬利尼納）
  - [ひざの上の同居人～Kitty on your
    lap～](../Page/ひざの上の同居人～Kitty_on_your_lap～.md "wikilink")（ミミ）
  - [ファイアーウーマン纏組](../Page/ファイアーウーマン纏組.md "wikilink")（岡田花奈）
  - [ブラックマトリクス](../Page/ブラックマトリクス.md "wikilink")（ミシェット）
  - [武藏傳](../Page/武藏傳.md "wikilink")（リキュール）
  - [ポケットファイター](../Page/ポケットファイター.md "wikilink")（春麗（チュンリー））
  - [ポケットラブ](../Page/ポケットラブ.md "wikilink")（松田由美）
  - [舞-HiME 命運之系統樹](../Page/舞-HiME_命運之系統樹.md "wikilink")（アリッサ・シアーズ）
  - [MARICA 真實的世界](../Page/MARICA_真實的世界.md "wikilink")（神崎まりか）
  - [Missing Blue](../Page/Missing_Blue.md "wikilink")（丹雫瑠羽奈）
  - [悠久幻想曲系列遊戲](../Page/悠久幻想曲.md "wikilink")（セリーヌ・ホワイトスノウ）
  - [雷弩機兵ガイブレイブ](../Page/雷弩機兵ガイブレイブ.md "wikilink")（スカリー）
  - [リフレインラブ](../Page/リフレインラブ.md "wikilink")（安井舞美\[1&2\]、久志原美紀\[1のみ\]）
  - [Dancing Blade
    任性桃天使\!](../Page/Dancing_Blade_任性桃天使!.md "wikilink")（幼竹）

### 柏青哥、角子機

  - 系列（[**惣流**／**惣流·明日香·蘭格雷**](../Page/惣流·明日香·蘭格雷.md "wikilink")）

  - 系列（**惣流**／**惣流·明日香·蘭格雷**）

  - CR（）

  - 春麗（**春麗**）

### 其它

  - 6年生presentu6年DVD 夢家編（樫原綾乃）

  -
### 影像商品

  - DVD（2003年3月28日）

## 音樂作品

### 單曲

<table>
<thead>
<tr class="header">
<th><p>枚數</p></th>
<th><p>發行日期</p></th>
<th><p>單曲名稱（日文名稱）</p></th>
<th><p>規格編號</p></th>
<th><p>最高排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>1995年7月21日</p></td>
<td><p><strong>若是戀愛了</strong>（）</p></td>
<td><p>VIDL-10679</p></td>
<td><p>圈外</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>1996年5月22日</p></td>
<td><p><strong>ENDLESS PARTY/HELLO,STRANGE DAYS</strong></p></td>
<td><p>VIDL-10760</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>1996年11月21日</p></td>
<td><p><strong>/KISS許</strong></p></td>
<td><p>VIDL-10828</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>1997年3月5日</p></td>
<td><p><strong>根性戰隊氣魄人</strong>（）</p></td>
<td><p>VIDL-10848</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>1997年5月21日</p></td>
<td><p><strong>平成偉人傳</strong>（）</p></td>
<td><p>VIDL-30019</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p>1997年8月21日</p></td>
<td><p><strong>KANON</strong></p></td>
<td><p>VIDL-30053</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p>1997年11月6日</p></td>
<td><p><strong>Pi·Pi·Pi/街森月</strong></p></td>
<td><p>VIDL-30076</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p>1998年2月21日</p></td>
<td><p><strong>Mother</strong></p></td>
<td><p>VIDL-30182</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9th</p></td>
<td><p>1998年5月21日</p></td>
<td><p><strong>戀愛與欲望</strong>（）</p></td>
<td><p>VIDL-30219</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10th</p></td>
<td><p>1998年6月24日</p></td>
<td><p><strong>謝謝</strong>（）</p></td>
<td><p>VICL-35030</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>※</p></td>
<td><p>1998年9月23日</p></td>
<td><p>/<strong></strong>/SLAVE&amp;QUEEN（//SLAVE&amp;QUEEN）</p></td>
<td><p>VIDL-30357</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>11th</p></td>
<td><p>1999年7月23日</p></td>
<td><p><strong>名探偵人生答</strong></p></td>
<td><p>VIDL-30434</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>與岩田光央</p></td>
<td><p>1999年7月23日</p></td>
<td><p><strong>Growin' Up！</strong></p></td>
<td><p>VIDL-30435</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12th</p></td>
<td><p>2012年1月11日</p></td>
<td><p><strong>讓心回歸初衷 ～2012 version～</strong>（ ～2012 version～）</p></td>
<td><p>KICM-1343</p></td>
<td><p>第34名</p></td>
</tr>
</tbody>
</table>

### 專輯

#### 原創專輯

<table>
<thead>
<tr class="header">
<th><p>枚數</p></th>
<th><p>發行日期</p></th>
<th><p>專輯名稱（日文名稱）</p></th>
<th><p>規格編號</p></th>
<th><p>最高排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>1996年6月21日</p></td>
<td><p><strong></strong>（）</p></td>
<td><p>VICL-758</p></td>
<td><p>第25名</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>1997年9月22日</p></td>
<td><p><strong></strong>（）</p></td>
<td><p>VICL-60090</p></td>
<td><p>第18名</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>1998年7月23日</p></td>
<td><p><strong></strong><br />
（ ～Thank You～）</p></td>
<td><p>VICL-60261<br />
VIZL-33（限定盤）</p></td>
<td><p>圈外</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>1999年8月25日</p></td>
<td><p><strong></strong></p></td>
<td><p>VICL-60445</p></td>
<td></td>
</tr>
</tbody>
</table>

#### 迷你專輯

| 枚數  | 發行日期        | 專輯名稱（日文名稱） | 規格編號       |
| --- | ----------- | ---------- | ---------- |
| 1st | 1996年12月18日 | ****（）     | VICL-23118 |
| 2nd | 1998年3月25日  | ****（）     | VICL-60182 |
| 3rd | 1999年5月21日  | ****（）     | VICL-60380 |

#### 精選專輯

| 枚數  | 發行日期       | 專輯名稱（日文名稱） | 規格編號       |
| --- | ---------- | ---------- | ---------- |
| 1st | 1998年12月2日 | ****（）     | VICL-60319 |
| 2nd | 1999年12月1日 | ****（）     | VOCL-60502 |

## 其它

### CD-ROM

  - ！（1996年5月14日）

### 漫畫原作

  - （畫：[島本和彥](../Page/島本和彥.md "wikilink")）

### 影像作品

  - 宮村失蹤事件（自作自演）1997年3月29日（VHS和LD兩種版本）

### 書籍

  - ASUKA―新世紀福音戰士文庫寫真集（1997年2月／[角川文庫](../Page/角川文庫.md "wikilink")）

  - 宮村優子第一本寫真集 真正的宮村（1997年4月30日／[角川書店](../Page/角川書店.md "wikilink")）
    攝影：石倉和夫

  - 宮村優子 根性戰隊氣魄人大百科（1997年5月15日／彩文館出版） 《》企劃手冊

  - VOICE OF 宮村優子（1997年11月10日／[德間書店](../Page/德間書店.md "wikilink")） 寫真集

  - （1998年10月10日／德間書店） 寫真集、隨筆集

  - 宮村優子（1999年12月4日／[Media
    Factory](../Page/Media_Factory.md "wikilink")） 寫真集、專訪

  - 直球手帳（2000年9月1日／） 電台節目《》節目手冊

## 參考文獻

## 外部連結

  - [Ameba上的官方blog](http://ameblo.jp/yuko-miyamura)

  - ─宮村優子個人網頁

  - [親子知新
    (blog)](https://web.archive.org/web/20100209010326/http://technosound.co.jp/onkochishin/)


  - [人物简介](https://web.archive.org/web/20120119054620/http://www.japanactionenterprise.com/jae/miyamura.html)（JAEプロモーション）


  -

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:漫画原作者](../Category/漫画原作者.md "wikilink")
[Category:音效指導](../Category/音效指導.md "wikilink")
[Category:兵庫縣出身人物](../Category/兵庫縣出身人物.md "wikilink")
[Category:神户市出身人物](../Category/神户市出身人物.md "wikilink")
[Category:桐朋学园艺术短期大学校友](../Category/桐朋学园艺术短期大学校友.md "wikilink")

1.