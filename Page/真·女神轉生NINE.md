《**真女神转生NINE**》是Atlus在Xbox上推出的唯一一部真女神系列游戏也是Xbox上难得一见的日式[RPG游戏](../Page/電子角色扮演遊戲.md "wikilink")。这个作品只在日本发售而已。这部名为真女神转生NINE的游戏，一样由真女神系列的原班人马制作，而且游戏架构广大，如下介绍：

1.  真女神转生NINE将有九大属性，包含中庸、秩序、混沌、光明、黑暗······等九种。
2.  游戏分为真实世界、虚拟世界两种，而且有真实的3D舞台场景。
3.  全新战斗系统（RTS），提供游戏乐趣。

## 相關條目

  - [女神轉生](../Page/女神轉生.md "wikilink")

[S](../Page/category:女神轉生系列電子遊戲.md "wikilink")

[S](../Category/2002年电子游戏.md "wikilink")
[S](../Category/電子角色扮演遊戲.md "wikilink")
[S](../Category/Xbox遊戲.md "wikilink")
[Category:Xbox独占游戏](../Category/Xbox独占游戏.md "wikilink")