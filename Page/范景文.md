**范景文**（），字夢章，號思仁，[直隸](../Page/北直隸.md "wikilink")[吳橋縣](../Page/吳橋縣.md "wikilink")（今[河北](../Page/河北.md "wikilink")[吳橋縣](../Page/吳橋縣.md "wikilink")）人，[明朝政治人物](../Page/明朝.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。官至兵部尚書兼東閣大學士，人稱『二不尚書』：不受囑，不受饋。

## 生平

父[范永年](../Page/范永年.md "wikilink")，字延齡，號仁元，官至[南寧府](../Page/南寧府.md "wikilink")[知府](../Page/知府.md "wikilink")。[萬曆四十一年](../Page/萬曆.md "wikilink")（1613年）登癸丑科[進士](../Page/進士.md "wikilink")，授[東昌府](../Page/東昌府.md "wikilink")[推官](../Page/推官.md "wikilink")，[天啟五年](../Page/天啟.md "wikilink")（1625年）二月，為[吏部](../Page/吏部.md "wikilink")[文選](../Page/文選.md "wikilink")[郎中](../Page/郎中.md "wikilink")，不依[魏忠賢](../Page/魏忠賢.md "wikilink")，亦不附[東林黨](../Page/東林黨.md "wikilink")，尝言：“天地人才，当为天地惜之。[朝廷名器](../Page/朝廷.md "wikilink")，当为朝廷守之。天下万世是非公论，当与天下万世共之。”以病辭去。

[崇禎時](../Page/崇禎.md "wikilink")，召为[太常寺](../Page/太常寺.md "wikilink")[少卿](../Page/少卿.md "wikilink")，崇禎二年（1629年）七月，擢[右佥都御史](../Page/右佥都御史.md "wikilink")，當時[清兵南掠](../Page/清兵.md "wikilink")，[京师戒严](../Page/京师.md "wikilink")，景文率所部八千人勤王，军无所犯。崇禎三年（1630年）三月，擢[兵部添注](../Page/兵部.md "wikilink")[左侍郎](../Page/左侍郎.md "wikilink")。後以父丧去官。崇禎七年（1634年）冬天，起[南京](../Page/南京.md "wikilink")[右都御史](../Page/右都御史.md "wikilink")。不久，拜[兵部尚書兼](../Page/兵部尚書.md "wikilink")[東閣大學士](../Page/東閣大學士.md "wikilink")，遣兵戍[池河](../Page/池河.md "wikilink")、[浦口](../Page/浦口.md "wikilink")。[杨嗣昌](../Page/杨嗣昌.md "wikilink")[夺情辅政](../Page/夺情.md "wikilink")，景文與之不合，被削籍为民\[1\]。

崇禎十七年（1644年）[李自成破](../Page/李自成.md "wikilink")[宣府](../Page/宣府.md "wikilink")，烽火逼[京师](../Page/京师.md "wikilink")，眾臣請帝南幸，景文勸帝“固结人心，坚守待援”，不久京師陷落，[明思宗自縊](../Page/明思宗.md "wikilink")。景文留下遺書曰：“身为大臣，不能灭贼雪耻，死有余恨。”後前往双塔寺旁的古井下自殺。赠[太傅](../Page/太傅.md "wikilink")，[諡](../Page/諡.md "wikilink")**文貞**。[清朝追](../Page/清朝.md "wikilink")[諡](../Page/諡.md "wikilink")**文忠**\[2\]。

## 著作

善[繪畫](../Page/繪畫.md "wikilink")，崇禎十二年（1639年）作《五大夫松圖》。有詩五首，收錄於《[列朝詩集](../Page/列朝詩集.md "wikilink")》[丁集第十一](../Page/s:列朝詩集/丁11.md "wikilink")。

## 注釋

## 参考文献

  - [張廷玉等](../Page/張廷玉.md "wikilink")，《明史》，中華書局點校本

[Category:明朝東昌府推官](../Category/明朝東昌府推官.md "wikilink")
[Category:明朝吏部郎中](../Category/明朝吏部郎中.md "wikilink")
[Category:明朝右佥都御史](../Category/明朝右佥都御史.md "wikilink")
[Category:南京右都御史](../Category/南京右都御史.md "wikilink")
[Category:明朝兵部尚書](../Category/明朝兵部尚書.md "wikilink")
[Category:明朝東閣大學士](../Category/明朝東閣大學士.md "wikilink")
[Category:明朝自殺人物](../Category/明朝自殺人物.md "wikilink")
[Category:吴桥人](../Category/吴桥人.md "wikilink")
[K](../Category/范姓.md "wikilink")
[Category:諡文貞](../Category/諡文貞.md "wikilink")
[Category:諡文忠](../Category/諡文忠.md "wikilink")

1.  [清](../Page/清.md "wikilink")·[張廷玉等](../Page/張廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷265）：“父永年，南寧知府。景文幼負器識，登萬曆四十一年進士，授東昌推官。以名節自勵，苞苴無敢及其門。歲大饑，盡心振救，闔郡賴之。用治行高等，擢吏部稽勳主事，歷文選員外郎，署選事。泰昌時，羣賢登進，景文力為多，尋乞假去。天啟五年二月起文選郎中。魏忠賢暨魏廣微中外用事，景文同鄉，不一詣其門，亦不附東林，孤立行意而已。嘗言：「天地人才，當為天地惜之。朝廷名器，當為朝廷守之。天下萬世是非公論，當與天下萬世共之。」時以為名言。視事未彌月，謝病去。崇禎初，用薦召為太常少卿。二年七月擢右僉都御史，巡撫河南。京師戒嚴，率所部八千人勤王，餉皆自齎。抵涿州，四方援兵多剽掠，獨河南軍無所犯。移駐都門，再移昌平，遠近恃以無恐。明年三月擢兵部添注左侍郎，練兵通州。通鎮初設，兵皆召募，景文綜理有法，軍特精。嘗請有司實行一條鞭法，徭役歸之官，民稍助其費，供應平買，不立官價名。帝令永著為例。居二年，以父喪去官。七年冬，起南京右都御史。未幾，就拜兵部尚書，參贊機務。屢遣兵戍池河、浦口，援廬州，扼滁陽，有警輒發，節制精明。嘗與南京戶部尚書錢春以軍食相訐奏，坐鐫秩視事。已，敍援剿功，復故秩。十一年冬，京師戒嚴，遣兵入衞。楊嗣昌奪情輔政，廷臣力爭多被謫，景文倡同列合詞論救。帝不悅，詰首謀，則自引罪，且以眾論僉同為言。帝益怒，削籍為民”。
2.  [清](../Page/清.md "wikilink")·[張廷玉等](../Page/張廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷265）：“景文曰：“固结人心，坚守待援而已，此外非臣所知。”及都城陷，趋至宫门，宫人曰：“驾出矣。”复趋朝房，贼已塞道。从者请易服还邸，景文曰：“驾出安归？”就道旁廟草遺疏，復大書曰：「身為大臣，不能滅賊雪恥，死有餘恨。」遂至演象所拜辭闕墓，赴雙塔寺旁古井死。景文死時，猶謂帝南幸也。贈太傅，諡文貞。本朝賜諡文忠。