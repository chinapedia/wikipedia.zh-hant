**隱龍屬**（屬名：*Yinlong*）意為“隱藏的龍”，是基礎[角龍類的一屬](../Page/角龍類.md "wikilink")，生存於[侏儸紀晚期的](../Page/侏儸紀.md "wikilink")[中亞](../Page/中亞.md "wikilink")。牠們是種小型、原始、二足的[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，身長約1.2公尺。隱龍是目前已知最古老、最原始的[角龍下目恐龍](../Page/角龍下目.md "wikilink")。

## 發現與種

[Yinlong_skull.jpg](https://zh.wikipedia.org/wiki/File:Yinlong_skull.jpg "fig:Yinlong_skull.jpg")
[Yinlong_BW.jpg](https://zh.wikipedia.org/wiki/File:Yinlong_BW.jpg "fig:Yinlong_BW.jpg")
一個由[美國與](../Page/美國.md "wikilink")[中國](../Page/中國.md "wikilink")[古生物學家組成的聯隊](../Page/古生物學家.md "wikilink")，包括[徐星](../Page/徐星.md "wikilink")、[凱薩琳·福斯特](../Page/凱薩琳·福斯特.md "wikilink")（Catherine
Forster）、[吉姆·克拉克](../Page/吉姆·克拉克.md "wikilink")（Jim
Clark）、以及[莫進尤等人](../Page/莫進尤.md "wikilink")，在2006年敘述並命名隱龍。隱龍的屬名來自於[普通話的](../Page/普通話.md "wikilink")「隱」及「龍」兩字，並參考了[電影](../Page/電影.md "wikilink")《[臥虎藏龍](../Page/臥虎藏龍_\(電影\).md "wikilink")》，因隱龍的發現地點接近《臥虎藏龍》在[新疆的拍攝地點](../Page/新疆.md "wikilink")。「龍」是中國媒體常用來稱呼恐龍的字，因此也用在數個中國恐龍的名稱中（[帝龍](../Page/帝龍.md "wikilink")、[寐龍](../Page/寐龍.md "wikilink")、[五彩冠龍](../Page/五彩冠龍.md "wikilink")、[北山龍](../Page/北山龍.md "wikilink")）。

隱龍的[模式種為](../Page/模式種.md "wikilink")**當氏隱龍**（*Y. downsi*） ，種名是由已逝的Will
Downs為名，他在發現隱龍的前一年逝世，曾參加過許多中國古生物挖掘活動。

隱龍的[化石為一個保存異常良好的骨骸與完整的](../Page/化石.md "wikilink")[頭顱骨](../Page/頭顱骨.md "wikilink")，之後發現其他化石。[正模標本是接近成年的個體](../Page/正模標本.md "wikilink")，是在2004年發現於[中國](../Page/中國.md "wikilink")[新疆](../Page/新疆.md "wikilink")[侏儸紀中到晚期的](../Page/侏儸紀.md "wikilink")[石樹溝組地層](../Page/石樹溝組.md "wikilink")。隱龍發現於該組地層的上部，該地質年代為晚[侏儸紀](../Page/侏儸紀.md "wikilink")[牛津階](../Page/牛津階.md "wikilink")，接近1億6120萬到1億5570萬年前\[1\]。所有其他的[角龍類都是發現於較晚的](../Page/角龍類.md "wikilink")[白堊紀](../Page/白堊紀.md "wikilink")。

## 分類

隱龍的上頜末端的[喙骨](../Page/喙骨.md "wikilink")（Rostral
bone）可確定隱龍為[角龍下目](../Page/角龍下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，但是隱龍[頭顱骨的數個特徵](../Page/頭顱骨.md "wikilink")，尤其是頭頂[方軛骨的裝飾](../Page/方軛骨.md "wikilink")，起初都被認為是[厚頭龍下目的特徵](../Page/厚頭龍下目.md "wikilink")。隱龍的這些特徵顯示它們為[頭飾龍類的](../Page/頭飾龍類.md "wikilink")[共有衍徵](../Page/共有衍徵.md "wikilink")，但這些特徵在所有從隱龍[演化出的角龍下目恐龍身上消失了](../Page/演化.md "wikilink")；頭飾龍類包含了[角龍下目](../Page/角龍下目.md "wikilink")、[厚頭龍下目](../Page/厚頭龍下目.md "wikilink")。這些特徵更證實了頭飾龍類的存在。隱龍也保存了[畸齒龍科的頭顱骨特徵](../Page/畸齒龍科.md "wikilink")，支持了畸齒龍科與頭飾龍類有緊密關係的假說\[2\]\[3\]\[4\]。[畸齒龍形類](../Page/畸齒龍形類.md "wikilink")（*Heterodontosauriformes*）包含了畸齒龍科、頭飾龍類\[5\]。

## 飲食

隱龍的腹腔曾發現7個[胃石](../Page/胃石.md "wikilink")。胃石可協助磨碎[消化系統中的植物](../Page/消化系統.md "wikilink")。在其他[角龍類如](../Page/角龍類.md "wikilink")[鸚鵡嘴龍也發現了胃石](../Page/鸚鵡嘴龍.md "wikilink")，大部分其他恐龍、許多[鳥類中也發現了胃石](../Page/鳥類.md "wikilink")。

## 大眾文化

隱龍首次出現於大眾媒體，是在[國家地理頻道的電視節目](../Page/國家地理頻道.md "wikilink")《恐龍大解密》（*Dino
Death
Trap*）。在該節目中，隱龍遭到[五彩冠龍的獵食](../Page/五彩冠龍.md "wikilink")。該節目宣稱隱龍是[三角龍的小型祖先](../Page/三角龍.md "wikilink")，並比較兩者的頭部形狀\[6\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [喬治華盛頓大學新聞中心](http://www.gwu.edu/~newsctr/fossilfind/)，介紹隱龍發現時的原始報告、圖片（[喬治華盛頓大學的網站](../Page/喬治華盛頓大學.md "wikilink")）。

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:角龍下目](../Category/角龍下目.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")

1.  Holtz, Thomas R. Jr. (2011) *Dinosaurs: The Most Complete,
    Up-to-Date Encyclopedia for Dinosaur Lovers of All Ages,*
    [Winter 2010
    Appendix.](http://www.geol.umd.edu/~tholtz/dinoappendix/HoltzappendixWinter2010.pdf)
2.
3.
4.
5.
6.