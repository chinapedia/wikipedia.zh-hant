**氮-14**（）是氮的[同位素之一](../Page/同位素.md "wikilink")，為穩定同位素，原子核包含了七個[質子和七的](../Page/質子.md "wikilink")[中子](../Page/中子.md "wikilink")，是最常見的同位素，[豐度有](../Page/豐度.md "wikilink")99%，在高空中，氮-14被[宇宙射線轟擊](../Page/宇宙射線.md "wikilink")，並產生有放射性的[碳-14](../Page/碳-14.md "wikilink")。一般來說，氮-14在宇宙中是被[恆星合成的](../Page/恆星.md "wikilink")，在[碳氮氧循環中扮演重要的角色](../Page/碳氮氧循環.md "wikilink")。

## 參見

  - [氮-13](../Page/氮-13.md "wikilink")
  - [氮-15](../Page/氮-15.md "wikilink")
  - [氮-16](../Page/氮-16.md "wikilink")
  - [碳-14](../Page/碳-14.md "wikilink")
  - [碳氮氧循環](../Page/碳氮氧循環.md "wikilink")

[en:Isotopes of
nitrogen\#Nitrogen-14](../Page/en:Isotopes_of_nitrogen#Nitrogen-14.md "wikilink")

[Category:氮的同位素](../Category/氮的同位素.md "wikilink")