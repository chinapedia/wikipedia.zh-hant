**橫濱DeNA海灣之星（、Yokohama DeNA
BayStars）**，是一支隸屬[日本職棒](../Page/日本職棒.md "wikilink")[中央聯盟的球隊](../Page/中央聯盟.md "wikilink")，1950年加入中央聯盟，至今拿過兩次日本大賽冠軍。
[ホッシー_(hosshey)_in_2010.08.08.JPG](https://zh.wikipedia.org/wiki/File:ホッシー_\(hosshey\)_in_2010.08.08.JPG "fig:ホッシー_(hosshey)_in_2010.08.08.JPG")
[20151128_'diana'_cheer_team_of_the_Yokohama_DeNA_BayStars_at_Yokohama_Stadium.JPG](https://zh.wikipedia.org/wiki/File:20151128_'diana'_cheer_team_of_the_Yokohama_DeNA_BayStars_at_Yokohama_Stadium.JPG "fig:20151128_'diana'_cheer_team_of_the_Yokohama_DeNA_BayStars_at_Yokohama_Stadium.JPG")

## 球隊名沿革

  - 1950年：大洋鯨隊
  - 1953年：與松竹知更鳥隊合併，更名為大洋松竹知更鳥隊
  - 1955年：更名大洋鯨隊
  - 1978年：更名橫濱大洋鯨隊
  - 1993年：更名橫濱海灣星隊
  - 2012年：更名**橫濱DeNA海灣之星**

## 球場

  - 1950年-1952年：下關球場
  - 1953年-1954年：大阪球場
  - 1955年-1977年：川崎球場
  - 1978年-現在：橫濱球場

[Yokohama_Stadium_2007_-3.jpg](https://zh.wikipedia.org/wiki/File:Yokohama_Stadium_2007_-3.jpg "fig:Yokohama_Stadium_2007_-3.jpg")
[Yokohama_BayStars_Ranking.svg](https://zh.wikipedia.org/wiki/File:Yokohama_BayStars_Ranking.svg "fig:Yokohama_BayStars_Ranking.svg")變遷圖。紅點表示獲得日本大賽優勝。\]\]

## 總教練

|                                              |                                      |                                      |     |      |
| -------------------------------------------- | ------------------------------------ | ------------------------------------ | --- | ---- |
| 教練名                                          | 接任年份                                 | 解任年份                                 | 日本一 | 備註   |
| [渡辺大陸](../Page/渡辺大陸.md "wikilink")           | [1950年](../Page/1950年.md "wikilink") | [1950年](../Page/1950年.md "wikilink") |     |      |
| [中島治康](../Page/中島治康.md "wikilink")           | [1951年](../Page/1951年.md "wikilink") | [1951年](../Page/1951年.md "wikilink") |     | 季中解任 |
| [有馬義一](../Page/有馬義一.md "wikilink")           | [1951年](../Page/1951年.md "wikilink") | [1951年](../Page/1951年.md "wikilink") |     | 季中接任 |
| [小西得郎](../Page/小西得郎.md "wikilink")           | [1952年](../Page/1952年.md "wikilink") | [1953年](../Page/1953年.md "wikilink") |     |      |
| [永澤武夫](../Page/永澤武夫.md "wikilink")           | [1954年](../Page/1954年.md "wikilink") | [1954年](../Page/1954年.md "wikilink") |     |      |
| [藤井勇](../Page/藤井勇.md "wikilink")             | [1955年](../Page/1955年.md "wikilink") | [1955年](../Page/1955年.md "wikilink") |     |      |
| [迫畑正巳](../Page/迫畑正巳.md "wikilink")           | [1956年](../Page/1956年.md "wikilink") | [1958年](../Page/1958年.md "wikilink") |     |      |
| [森茂雄](../Page/森茂雄.md "wikilink")             | [1959年](../Page/1959年.md "wikilink") | [1959年](../Page/1959年.md "wikilink") |     |      |
| [三原脩](../Page/三原脩.md "wikilink")             | [1960年](../Page/1960年.md "wikilink") | [1967年](../Page/1967年.md "wikilink") | 1次  |      |
| [別当薫](../Page/別当薫.md "wikilink")             | [1968年](../Page/1968年.md "wikilink") | [1972年](../Page/1972年.md "wikilink") |     | 第1次  |
| [青田昇](../Page/青田昇.md "wikilink")             | [1972年](../Page/1972年.md "wikilink") | [1972年](../Page/1972年.md "wikilink") |     | 代理   |
| [宮崎剛](../Page/宮崎剛.md "wikilink")             | [1972年](../Page/1972年.md "wikilink") | [1972年](../Page/1972年.md "wikilink") |     | 代理   |
| [青田昇](../Page/青田昇.md "wikilink")             | [1973年](../Page/1973年.md "wikilink") | [1973年](../Page/1973年.md "wikilink") |     |      |
| [宮崎剛](../Page/宮崎剛.md "wikilink")             | [1974年](../Page/1974年.md "wikilink") | [1974年](../Page/1974年.md "wikilink") |     |      |
| [秋山登](../Page/秋山登.md "wikilink")             | [1975年](../Page/1975年.md "wikilink") | [1976年](../Page/1976年.md "wikilink") |     |      |
| [別当薫](../Page/別当薫.md "wikilink")             | [1977年](../Page/1977年.md "wikilink") | [1979年](../Page/1979年.md "wikilink") |     | 第2次  |
| [土井淳](../Page/土井淳.md "wikilink")             | [1980年](../Page/1980年.md "wikilink") | [1981年](../Page/1981年.md "wikilink") |     |      |
| [山根俊英](../Page/山根俊英.md "wikilink")           | [1981年](../Page/1981年.md "wikilink") | [1981年](../Page/1981年.md "wikilink") |     | 代理   |
| [関根潤三](../Page/関根潤三.md "wikilink")           | [1982年](../Page/1982年.md "wikilink") | [1984年](../Page/1984年.md "wikilink") |     |      |
| [近藤貞雄](../Page/近藤貞雄.md "wikilink")           | [1985年](../Page/1985年.md "wikilink") | [1986年](../Page/1986年.md "wikilink") |     |      |
| [古葉竹識](../Page/古葉竹識.md "wikilink")           | [1987年](../Page/1987年.md "wikilink") | [1989年](../Page/1989年.md "wikilink") |     |      |
| [須藤豊](../Page/須藤豊.md "wikilink")             | [1990年](../Page/1990年.md "wikilink") | [1992年](../Page/1992年.md "wikilink") |     | 季中解任 |
| [江尻亮](../Page/江尻亮.md "wikilink")             | [1992年](../Page/1992年.md "wikilink") | [1992年](../Page/1992年.md "wikilink") |     | 季中接任 |
| [近藤昭仁](../Page/近藤昭仁.md "wikilink")           | [1993年](../Page/1993年.md "wikilink") | [1995年](../Page/1995年.md "wikilink") |     |      |
| [大矢明彦](../Page/大矢明彦.md "wikilink")           | [1996年](../Page/1996年.md "wikilink") | [1997年](../Page/1997年.md "wikilink") |     | 第1次  |
| [権藤博](../Page/権藤博.md "wikilink")             | [1998年](../Page/1998年.md "wikilink") | [2000年](../Page/2000年.md "wikilink") | 1次  |      |
| [森祗晶](../Page/森祗晶.md "wikilink")             | [2001年](../Page/2001年.md "wikilink") | [2002年](../Page/2002年.md "wikilink") |     |      |
| [黒江透修](../Page/黒江透修.md "wikilink")           | [2002年](../Page/2002年.md "wikilink") | [2002年](../Page/2002年.md "wikilink") |     | 代理   |
| [山下大輔](../Page/山下大輔.md "wikilink")           | [2003年](../Page/2003年.md "wikilink") | [2004年](../Page/2004年.md "wikilink") |     |      |
| [牛島和彦](../Page/牛島和彦.md "wikilink")           | [2005年](../Page/2005年.md "wikilink") | [2006年](../Page/2006年.md "wikilink") |     |      |
| [大矢明彦](../Page/大矢明彦.md "wikilink")           | [2007年](../Page/2007年.md "wikilink") | [2009年](../Page/2009年.md "wikilink") |     | 第2次  |
| [田代富雄](../Page/田代富雄.md "wikilink")           | [2009年](../Page/2009年.md "wikilink") | [2009年](../Page/2009年.md "wikilink") |     | 代理   |
| [尾花高夫](../Page/尾花高夫.md "wikilink")           | [2010年](../Page/2010年.md "wikilink") | [2011年](../Page/2011年.md "wikilink") |     |      |
| [中畑清](../Page/中畑清.md "wikilink")             | [2012年](../Page/2012年.md "wikilink") | [2015年](../Page/2015年.md "wikilink") |     |      |
| [亞力克斯·拉米瑞茲](../Page/亞力克斯·拉米瑞茲.md "wikilink") | [2016年](../Page/2016年.md "wikilink") |                                      |     | 現任   |

<noinclude></noinclude>

## 陣中台灣籍員

  - [王溢正](../Page/王溢正.md "wikilink")(2010\~2013)
  - [陳冠宇](../Page/陳冠宇.md "wikilink")(2011\~2014)
  - [鄭凱文](../Page/鄭凱文.md "wikilink")(2013)

## 外部連結

  - [橫濱海灣之星官方網站](http://www.baystars.co.jp/index.php)

[Category:紀錄模板](../Category/紀錄模板.md "wikilink")
[Category:日本職棒球隊](../Category/日本職棒球隊.md "wikilink")
[Category:中區 (橫濱市)](../Category/中區_\(橫濱市\).md "wikilink")
[Category:1950年建立](../Category/1950年建立.md "wikilink")