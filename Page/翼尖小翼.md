[Cathay_Pacific_inaugural_flight_25_March_(39217305050).jpg](https://zh.wikipedia.org/wiki/File:Cathay_Pacific_inaugural_flight_25_March_\(39217305050\).jpg "fig:Cathay_Pacific_inaugural_flight_25_March_(39217305050).jpg")[空中巴士A350的翼尖小翼](../Page/空中巴士A350.md "wikilink")\]\]
**翼尖小翼**（winglet或wingtip），又稱作**翼梢小翼**、**翼尖帆**或**翼端帆**，通常用于提高固定翼[航空器](../Page/航空器.md "wikilink")[机翼的效率](../Page/机翼.md "wikilink")，也可用来改善航空器的操纵特性。

## 原理

飛機維持正常飛行時所需的升力是靠機翼上下表面的壓力差產生的，由於上下表面壓差的存在，翼尖附近機翼下表面空氣會繞流到上表面，形成[翼尖渦流](../Page/翼尖渦流.md "wikilink")，致使翼尖附近區域機翼上下表面的壓差降低，從而導致這一區域產生的升力降低。為了削弱這種繞流現象對升力的影響，很多飛機的翼尖都安裝了翼尖小翼(又稱為「翼尖整流片」)，用以阻礙上下表面的空氣繞流，降低因翼尖渦流造成的升力誘導阻力，減少繞流對升力的破壞，提高昇阻比，達到增加升力的目的。對於有動力航空器來說還可降低油耗。不過增加翼尖小翼，翼端結構為了組裝翼尖小翼會比較複雜，也會增加一些額外的重量，若是裝了翼尖小翼都只飛兩小時內短程航線，油耗反而會增加，飛長程航線才能得到降低油耗的效益。

## 規格

翼尖小翼形狀因各種飛機型號不同而有所變異。因為國際油價抬升，所以各航空公司紛紛要求廠商加裝翼尖帆，有些機型如[空中巴士A300](../Page/空中巴士A300.md "wikilink")、[波音727](../Page/波音727.md "wikilink")、[波音757和](../Page/波音757.md "wikilink")[波音767原設計並無翼尖小翼](../Page/波音767.md "wikilink")，後來才加裝上去。

典型的翼尖帆如[波音公司](../Page/波音.md "wikilink")（Boeing）生產的[747-400型](../Page/波音747#747-400.md "wikilink")（專為日本國內航線設計的747-400D型除外）飛機和[空中巴士](../Page/空中巴士.md "wikilink")（AIRBUS）的[A330和](../Page/空中巴士A330.md "wikilink")[A340系列飛機](../Page/空中巴士A340.md "wikilink")。另一種則是空中巴士[A320系列](../Page/空中巴士A320.md "wikilink")、[A310和](../Page/空中巴士A310.md "wikilink")[A380的三角型翼尖帆](../Page/空中巴士A380.md "wikilink")，這款翼尖帆可說是空中巴士的標準形式。還有新一代[波音737-800](../Page/波音737新世代#737-800.md "wikilink")、[900](../Page/波音737#737-900.md "wikilink")（ER）、[灣流V公務機](../Page/灣流噴射機.md "wikilink")、[空中巴士A320neo系列的大型翼尖帆及](../Page/空中巴士A320neo系列.md "wikilink")[MD-11](../Page/麥道MD-11.md "wikilink")、[波音737MAX上下兩片式翼尖帆](../Page/波音737MAX.md "wikilink")。而波音的新設計客機如[波音777](../Page/波音777.md "wikilink")、[波音787](../Page/波音787.md "wikilink")、[波音747-8都以特殊的上帆角](../Page/波音747-8.md "wikilink")（raked
winglet）設置代替翼尖帆。

一般的軍用機中，[戰鬥機由於涉及到](../Page/戰鬥機.md "wikilink")[空氣動力學](../Page/空氣動力學.md "wikilink")、[超音速巡航與近距高速空中](../Page/超音速巡航.md "wikilink")[纏鬥的需求](../Page/纏鬥.md "wikilink")，且航程（作戰半徑）較短，故無安裝翼尖小翼的需求，部分在[航空母艦上起降的軍機亦無翼尖小翼的設計](../Page/航空母艦.md "wikilink")；而針對長途飛行任務設計的[戰略轟炸機](../Page/戰略轟炸機.md "wikilink")、[空中加油機](../Page/空中加油機.md "wikilink")、[運輸機](../Page/運輸機.md "wikilink")、[空中預警機](../Page/空中預警機.md "wikilink")，為了增加續航力而加裝了翼尖小翼。

除了飛機以外，現今大多數賽車也會在後擾流器兩端裝上翼尖小翼。部份[風力發電機也會在葉片尖端安裝翼尖帆](../Page/風力發電機.md "wikilink")。

## 圖片

<File:Rutan.variEze.g-veze.arp.jpg>|[Rutan
VariEze為最早使用翼尖小翼](../Page/Rutan_VariEze.md "wikilink")（1975年）的飛機
[File:Wingletdetail.jpg|空中巴士A320、310及380的翼尖小翼](File:Wingletdetail.jpg%7C空中巴士A320、310及380的翼尖小翼)
<File:Boeing> 787 Roll-out.jpg|787的上帆角 <File:Jordan> noses and front
wings.jpg|賽車的翼尖小翼 <File:WindTurbine> Rotor Winglet.JPG|風力發電機的翼尖小翼

[Category:机翼](../Category/机翼.md "wikilink")
[Category:飞行器布局](../Category/飞行器布局.md "wikilink")