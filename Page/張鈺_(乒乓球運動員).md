**張鈺**（，\[1\]），出生于中国江苏省，是一名已退役的香港男子乒乓球[运动员](../Page/运动员.md "wikilink")\[2\]，他擅長以左手橫板快攻結合弧圈打法\[3\]。张钰的最高世界排名是第12名\[4\]，他曾参加过3届[奥运会](../Page/奥运会.md "wikilink")、4届亚运会，总共获得4枚乒乓球世锦赛铜牌、4枚亚运会奖牌，其中一枚金牌是他和帖雅娜在釜山亚运会乒乓球混双比赛上获得的，这枚金牌也是香港队在亚运会乒乓球项目上获得的第一枚金牌。

## 运动生涯

### 1999年－2001年：初出茅庐

张钰出生于江苏省。1999年，他来到香港，并加入香港乒乓球队\[5\]。他在国际主要赛场上的首次亮相是在[马来西亚](../Page/马来西亚.md "wikilink")[吉隆坡举办的](../Page/吉隆坡.md "wikilink")[2000年世界乒乓球锦标赛](../Page/2000年世界乒乓球锦标赛.md "wikilink")\[6\]。2000年9月，他参加了自己运动生涯里的首届[奥运会](../Page/2000年夏季奥林匹克运动会乒乓球比赛.md "wikilink")。首先在[双打比赛中](../Page/2000年夏季奧林匹克運動會乒乓球比賽－男子雙打.md "wikilink")，他和搭档[梁柱恩在预赛中以第一名的身份晋级](../Page/梁柱恩.md "wikilink")，但在八分之一决赛中他们以2比3的比分不敌[韩国组合](../Page/韩国.md "wikilink")[柳承敏和](../Page/柳承敏.md "wikilink")[李哲承](../Page/李哲承_\(乒乓球运动员\).md "wikilink")，未能晋级8强\[7\]。之后在男子单打比赛中，他同样以小组第一的身份晋级淘汰赛，但在十六分之一决赛中以0比3的比分不敌[日本选手](../Page/日本.md "wikilink")[松下浩二无缘下一轮](../Page/松下浩二.md "wikilink")\[8\]。2001年，他参加了在日本[大阪举办的](../Page/大阪.md "wikilink")[世界乒乓球锦标赛](../Page/2001年世界乒乓球锦标赛.md "wikilink")，在男子单打、男子双打以及混合双打这三个项目中分别止步于32强、16强以及32强\[9\]。

### 2002年－2004年：釜山亚运混双金牌、雅典奥运

2002年10月，张钰赴韩国[釜山参加亚运会](../Page/釜山.md "wikilink")。他参加的第一个项目是男子团体比赛，在半决赛中，中国香港队以大比分0比3负于中国队，最终获得铜牌，这是张钰获得的首枚亚运会奖牌\[10\]。在之后进行的混合双打比赛中，他和搭档[帖雅娜表现出色](../Page/帖雅娜.md "wikilink")，在决赛中以4比3的比分险胜韩国的[柳承敏和](../Page/柳承敏.md "wikilink")[柳智惠](../Page/柳智惠.md "wikilink")，为香港队夺得队史上首枚亚运会乒乓球项目的金牌\[11\]\[12\]。而在之后的男子双打和男子单打比赛中，张钰均止步于8强\[13\]。一年后，他参加了在法国[巴黎举办的](../Page/巴黎.md "wikilink")[世界乒乓球锦标赛](../Page/2003年世界乒乓球锦标赛.md "wikilink")，在男子单打、男子双打以及混合双打这三个项目中分别止步于64强、8强以及16强\[14\]。2004年8月，张钰再度联同梁柱恩，以五号种子的身份参加[雅典奥运会](../Page/2004年夏季奥林匹克运动会乒乓球比赛.md "wikilink")，然而他们在第三轮比赛中以0比4的比分不敌俄罗斯组合[迪米特里·马祖诺夫](../Page/迪米特里·马祖诺夫.md "wikilink")（）和[阿里克谢·斯米尔诺夫](../Page/阿里克谢·斯米尔诺夫.md "wikilink")，提前结束奥运之旅\[15\]\[16\]。

### 2006年－2008年：首夺世锦赛奖牌、多哈亚运、北京奥运

2006年4月，由[李静](../Page/李静_\(乒乓球运动员\).md "wikilink")、张钰、[高礼泽](../Page/高礼泽.md "wikilink")、梁柱恩、[谢嘉俊组成的香港男子乒乓球队参加了当年在](../Page/谢嘉俊.md "wikilink")[德国](../Page/德国.md "wikilink")[不来梅举办的世界乒乓球锦标赛](../Page/不来梅.md "wikilink")，在半决赛中他们遇上韩国队，结果他们以0比3落败，获得一枚铜牌\[17\]，这枚铜牌是张钰获得的第一枚乒乓球世锦赛奖牌\[18\]。同年11月底至12月初，张钰参加了在[多哈举办的亚洲运动会](../Page/多哈.md "wikilink")，首先在男子团体赛中，他和队友在半决赛中以0比3负于韩国队，无缘决赛，中国香港队也直接获得一枚铜牌\[19\]。在之后的男子双打和男子单打比赛中，张钰仍未能实现突破，两项目均在四分之一决赛中被淘汰出局\[20\]\[21\]。

2008年，张钰和队友参加了在[广东](../Page/广东.md "wikilink")[广州举办的乒乓球团体世锦赛](../Page/广州.md "wikilink")，在半决赛中，中国香港队最终以0比3的比分不敌中国队，无缘决赛，和日本队并列第三名\[22\]。同年7月，他的男子单打世界排名上升到了第12位\[23\]\[24\]。同年8月，他来到[北京](../Page/北京.md "wikilink")，参加了自己运动生涯中的最后一届[奥运会](../Page/2008年夏季奥林匹克运动会乒乓球比赛.md "wikilink")。他的第一个参赛项目是[男子团体](../Page/2008年夏季奧林匹克運動會乒乓球比賽－男子團體.md "wikilink")，在预赛中，他和队友以小组第二的身份晋级铜牌附加赛，然而他们在第二轮比赛中以1比3的比分负于韩国队，无缘铜牌赛决赛。之后他以9号种子的身份参加[单打项目的比赛](../Page/2008年夏季奧林匹克運動會乒乓球比賽－男子單打.md "wikilink")，然而他在第三轮比赛中以1比4的比分不敌瑞典选手[约尔根·佩尔森](../Page/约尔根·佩尔森.md "wikilink")，结束了北京奥运之旅。\[25\]

### 2009年－2014年：广州亚运混双银牌、两届世锦赛混双铜牌

2009年12月，张钰参加了在香港举办的东亚运动会，获得了男子单打和男子双打这两个项目的铜牌\[26\]。翌年5月，由[唐鹏](../Page/唐鹏.md "wikilink")、张钰、李静、[江天一](../Page/江天一.md "wikilink")、高礼泽组成的中国香港男队参加了在[俄罗斯](../Page/俄罗斯.md "wikilink")[莫斯科举行的](../Page/莫斯科.md "wikilink")[乒乓球团体世锦赛](../Page/2010年世界乒乓球锦标赛.md "wikilink")，在四分之一决赛中，他们输给了日本队，没能夺得奖牌\[27\]。同年11月，张钰出战广州亚运会。在他参加的第一个项目男子团体中，中国香港队0比3负于韩国，止步8强\[28\]。而在之后的混双比赛中，张钰和搭档[姜华珺发挥出色](../Page/姜华珺.md "wikilink")，闯入决赛，但在决赛中不敌中国的[许昕和](../Page/许昕.md "wikilink")[郭焱](../Page/郭焱.md "wikilink")，获得亚军\[29\]。男子双打方面，张钰和搭档李静在四分之一决赛中以0比3负于[王皓和](../Page/王皓.md "wikilink")[张继科](../Page/张继科.md "wikilink")，无缘半决赛。\[30\]2011年5月，张钰参加在[荷兰](../Page/荷兰.md "wikilink")[鹿特丹举办的乒乓球世锦赛](../Page/鹿特丹.md "wikilink")，在混双项目上，他和搭档姜华珺在半决赛中被[张超和](../Page/张超_\(乒乓球运动员\).md "wikilink")[曹臻击败](../Page/曹臻.md "wikilink")，虽无缘决赛，但张钰首次获得了乒乓球世锦赛双打项目的奖牌\[31\]。

2012年3月，张钰和队友代表中国香港参加在德国[多特蒙德举办的](../Page/多特蒙德.md "wikilink")[乒乓球团体世锦赛](../Page/2012年世界乒乓球团体锦标赛.md "wikilink")，在淘汰赛第一轮中国香港队以2比3不敌中华台北队\[32\]，最终仅获得第9名\[33\]。一年后，张钰再度出战乒乓球世锦赛，在混双项目上，他再度搭档姜华珺，在四分之一决赛上以4比3险胜同队的江天一和[李皓晴](../Page/李皓晴.md "wikilink")，在半决赛上，他们对阵[朝鲜的](../Page/朝鲜.md "wikilink")[金赫峰和](../Page/金赫峰.md "wikilink")[金仲](../Page/金仲.md "wikilink")，尽管他们在前三局均战胜对手，但在后四局遭对手逆转，再度与决赛无缘，并摘得铜牌\[34\]。2014年9月，张钰前往韩国[仁川出战](../Page/仁川.md "wikilink")[亚运会](../Page/2014年亚洲运动会乒乓球比赛.md "wikilink")，首先在男子团体赛中，他和队友在四分之一决赛中以2比3的比分负于中华台北队\[35\]，之后在混合双打比赛中，他和搭档姜华珺在四分之一决赛上未能战胜日本的[福原爱和](../Page/福原爱.md "wikilink")[岸川圣也](../Page/岸川圣也.md "wikilink")，止步8强，结束该届亚运会之旅\[36\]。

## 家庭

在北京奥运会举办的3年之前，张钰被教练安排到德国去参加联赛，在那里他认识了他的妻子\[37\]。2009年9月，他们的儿子出生\[38\]。

## 榮譽

  - 香港杰出新秀运动员（2001年，现[香港最具潜质运动员](../Page/香港最具潜质运动员.md "wikilink")）\[39\]
  - **香港特區政府嘉獎**

<!-- end list -->

  -
    [行政長官社區服務獎狀](../Page/行政長官社區服務獎狀.md "wikilink")（2011年）\[40\]

## 注释

## 參考資料

[Category:香港退役乒乓球運動員](../Category/香港退役乒乓球運動員.md "wikilink")
[Category:香港最具潛質運動員](../Category/香港最具潛質運動員.md "wikilink")
[Category:香港奧林匹克運動會乒乓球選手](../Category/香港奧林匹克運動會乒乓球選手.md "wikilink")
[Category:2002年亚洲运动会金牌得主](../Category/2002年亚洲运动会金牌得主.md "wikilink")
[Category:2010年亚洲运动会銀牌得主](../Category/2010年亚洲运动会銀牌得主.md "wikilink")
[Y](../Category/張姓.md "wikilink")
[Category:2006年亚洲运动会铜牌得主](../Category/2006年亚洲运动会铜牌得主.md "wikilink")
[Category:2000年夏季奥林匹克运动会乒乓球运动员](../Category/2000年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:2004年夏季奥林匹克运动会乒乓球运动员](../Category/2004年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:2008年夏季奥林匹克运动会乒乓球运动员](../Category/2008年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:亞洲運動會桌球獎牌得主](../Category/亞洲運動會桌球獎牌得主.md "wikilink")
[Category:2010年亞洲運動會桌球運動員](../Category/2010年亞洲運動會桌球運動員.md "wikilink")
[Category:2014年亞洲運動會桌球運動員](../Category/2014年亞洲運動會桌球運動員.md "wikilink")
[Category:江苏籍运动员](../Category/江苏籍运动员.md "wikilink")
[Category:2002年亞洲運動會銅牌得主](../Category/2002年亞洲運動會銅牌得主.md "wikilink")

1.
2.

3.

4.
5.

6.

7.

8.
9.
10.

11.

12.

13.
14.
15.

16.

17.

18.
19.

20.

21.

22.

23.
24.

25.
26.

27.

28.

29.

30.

31.

32.

33.
34.

35.

36.

37.

38.

39.

40.