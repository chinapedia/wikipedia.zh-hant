**大秦铁路股份有限公司**（）是中国的上市铁路公司之一，负责[大秦铁路的运营](../Page/大秦铁路.md "wikilink")。公司成立于2004年10月26日，由[北京铁路局及多家煤電企業合資成立](../Page/北京铁路局.md "wikilink")，其前身为北京铁路局大同铁路分局。2005年1月，公司收购了[丰沙大铁路](../Page/丰沙大铁路.md "wikilink")（大同—郭磊庄段）、北同蒲铁路（大同—[宁武段](../Page/宁武.md "wikilink")）、口泉铁路支线和宁岢铁路支线，囊括整个大同铁路分局的经营性资产。2005年3月北京铁路局分立出太原铁路局后，大秦铁路（公司）被划归至太原局。2006年7月，大秦铁路股份有限公司在[上海证券交易所公开发行股票](../Page/上海证券交易所.md "wikilink")，并于当年8月1日上市交易，是繼[廣深鐵路股份有限公司](../Page/廣深鐵路股份有限公司.md "wikilink")1996年上市後，第二間中國國鐵轄下鐵路公司分拆上市。

## 外部链接

  - [大秦铁路股份有限公司](https://web.archive.org/web/20080329091127/http://www.daqintielu.com/)

[Category:太原铁路局](../Category/太原铁路局.md "wikilink")
[Category:中国铁路公司](../Category/中国铁路公司.md "wikilink")
[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")
[Category:山西公司](../Category/山西公司.md "wikilink")
[Category:2004年成立的公司](../Category/2004年成立的公司.md "wikilink")