[Unix_history-simple.svg](https://zh.wikipedia.org/wiki/File:Unix_history-simple.svg "fig:Unix_history-simple.svg")
**类Unix系统**（）经常被称为 **UN\*X** 或
**\*nix**，指各种[Unix的衍生系统](../Page/Unix.md "wikilink")，比如[FreeBSD](../Page/FreeBSD.md "wikilink")、[OpenBSD](../Page/OpenBSD.md "wikilink")、[SUN公司的](../Page/SUN.md "wikilink")[Solaris](../Page/Solaris.md "wikilink")，以及各种与传统Unix类似的系统，例如[Minix](../Page/Minix.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、[QNX等](../Page/QNX.md "wikilink")。它们虽然有的是[自由软件](../Page/自由软件.md "wikilink")，有的是[私有软件](../Page/私有软件.md "wikilink")，但都相当程度地继承了原始UNIX的特性，有许多相似处，并且都在一定程度上遵守[POSIX规范](../Page/POSIX.md "wikilink")。

UNIX的商標權由[國際開放標準組織所擁有](../Page/國際開放標準組織.md "wikilink")，只有符合[單一UNIX規範的UNIX系統才能使用UNIX這個名稱](../Page/單一UNIX規範.md "wikilink")，否則只能稱為類UNIX（UNIX-like）。

## 分類和例子

### 自由软件/開源軟件

  - [Agnix](../Page/Agnix.md "wikilink")（教育用）
  - [386BSD及其子類](../Page/386BSD.md "wikilink")（[BSD系统](../Page/BSD.md "wikilink")）:
      - [FreeBSD及其子類](../Page/FreeBSD.md "wikilink")：
          - [ClosedBSD](../Page/ClosedBSD.md "wikilink")
          - [Apple Darwin](../Page/Apple_Darwin.md "wikilink")
          - [DragonFly BSD](../Page/DragonFly_BSD.md "wikilink")
          - [GNU/kFreeBSD](../Page/GNU/kFreeBSD.md "wikilink")
          - [PC-BSD](../Page/PC-BSD.md "wikilink")
      - [NetBSD及其子類](../Page/NetBSD.md "wikilink")：
          - [GNU/kNetBSD](../Page/GNU/kNetBSD.md "wikilink")
      - [OpenBSD及其子類](../Page/OpenBSD.md "wikilink")：
          - [ekkoBSD](../Page/ekkoBSD.md "wikilink")
          - [MicroBSD](../Page/MicroBSD.md "wikilink")
          - [MirOS BSD](../Page/MirOS_BSD.md "wikilink")
  - [GNU](../Page/GNU.md "wikilink")
      - [GNU Hurd](../Page/GNU_Hurd.md "wikilink")
      - [GNU/kFreeBSD](../Page/Debian_GNU/kFreeBSD.md "wikilink")
      - [GNU/kNetBSD](../Page/GNU/NetBSD.md "wikilink")
      - [Linux](../Page/Linux.md "wikilink")（又称[GNU/Linux](../Page/GNU/Linux.md "wikilink")）
      - [GNU/OpenSolaris](../Page/Nexenta_OS.md "wikilink")
  - [LUnix](../Page/LUnix.md "wikilink")
  - [MINIX及其子類](../Page/Minix.md "wikilink")：
      - [Minix-vmd](../Page/Minix-vmd.md "wikilink")
      - [MINIX 3](../Page/MINIX_3.md "wikilink")
  - [OpenSolaris](../Page/OpenSolaris.md "wikilink") - 建基於[System
    V](../Page/System_V.md "wikilink")
  - [Phoenix-RTOS](../Page/Phoenix-RTOS.md "wikilink")
  - [九号计划](../Page/贝尔实验室九号计划.md "wikilink")：Unix的后继者，采用UNIX设计与哲学，但更一致地套用至整个分布式系统，功能上并不完全相同。
      - [Inferno](../Page/Inferno_\(operating_system\).md "wikilink")：Plan
        9衍生出的分布式操作系统，原本由贝尔实验室开发，现在被[Vita
        Nuova拥有](../Page/Vita_Nuova.md "wikilink")。
      - [Plan B](../Page/Plan_B_\(operating_system\).md "wikilink")：Plan
        9衍生出的分布式操作系统[1](http://lsub.org/ls/planb.html))
  - [Syllable](../Page/Syllable_\(operating_system\).md "wikilink")：99%
    [POSIX依從](../Page/POSIX.md "wikilink")
  - [VSTa](../Page/VSTa.md "wikilink")：大致[POSIX依從](../Page/POSIX.md "wikilink")
  - [Maemo](../Page/Maemo.md "wikilink")：诺基亚的开源系统

### 私有软件

  - [IBM](../Page/IBM.md "wikilink") [AIX](../Page/AIX.md "wikilink")\*
    - 建基於[System V](../Page/System_V.md "wikilink") Release 3
  - [HP](../Page/HP.md "wikilink")
    [HP-UX](../Page/HP-UX.md "wikilink")\*
  - [SGI](../Page/SGI.md "wikilink")
    [IRIX](../Page/IRIX.md "wikilink")\*
  - [Apple](../Page/Apple.md "wikilink")
    [macOS](../Page/Mac_OS_X.md "wikilink") - 建基於[Apple
    Darwin](../Page/Apple_Darwin.md "wikilink") (自 10.5 開始符合單一UNIX規範）
  - [Apple](../Page/Apple.md "wikilink")
    [iOS](../Page/iOS.md "wikilink") - 建基於[Apple
    Darwin](../Page/Apple_Darwin.md "wikilink")
  - [LynxOS RTOS](../Page/LynxOS.md "wikilink")
  - [QNX](../Page/QNX.md "wikilink") - 全部重写，没有UNIX相关的代码
  - [SkyOS](../Page/SkyOS.md "wikilink") -
    大致[POSIX依從](../Page/POSIX.md "wikilink")
  - [Sun](../Page/Sun_Microsystems.md "wikilink")
      - [SunOS](../Page/SunOS.md "wikilink") -
        建基於[BSD](../Page/BSD.md "wikilink")
      - [Solaris](../Page/Solaris.md "wikilink")\* - 建基於[System
        V](../Page/System_V.md "wikilink") Release 4
  - [Compaq](../Page/Compaq.md "wikilink")
    [Tru64](../Page/Tru64.md "wikilink")\* -
    建基於[OSF/1](../Page/OSF/1.md "wikilink")
  - [Microsoft](../Page/Microsoft.md "wikilink")
    [Xenix](../Page/Xenix.md "wikilink")
  - [VxWorks](../Page/VxWorks.md "wikilink")
  - [Google](../Page/Google.md "wikilink")
    [Android](../Page/Android.md "wikilink")

<!-- end list -->

  -
    \* *UNIX® branded systems*

## 参考文献

## 參見

  - [操作系统列表](../Page/操作系统列表.md "wikilink")

{{-}}

[类Unix系统](../Category/类Unix系统.md "wikilink")