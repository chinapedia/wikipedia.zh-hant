\-{T|zh:α衰变;zh-hant:α衰變;zh-hans:α衰变}-
[Alpha_Decay.svg](https://zh.wikipedia.org/wiki/File:Alpha_Decay.svg "fig:Alpha_Decay.svg")

**α衰變**（Alpha
decay，又稱**alpha衰變**）是一種放射性[衰變](../Page/衰變.md "wikilink")（核衰變）；發生α衰變時，一顆[α粒子會從](../Page/α粒子.md "wikilink")[原子核中射出](../Page/原子核.md "wikilink")（附註：α粒子，又名阿爾法粒子，即氦-4核，
，即一顆由2顆[質子和](../Page/質子.md "wikilink")2顆[中子組成的原子核](../Page/中子.md "wikilink")）；
α衰變發生後，原子核的[質量數會減少](../Page/質量數.md "wikilink")4個單位，其[原子序也會減少了](../Page/原子序.md "wikilink")2個單位。

下面之反應式式（I）是α衰變的一個例子。例如，鈾-238通過α粒子發射的衰減以形成釷-234可以表示為：\[1\]
\({}^2{}^{38}_{92}\hbox{U}\;\to\;{}^2{}^{34}_{90}\hbox{Th}\;+\;{}^4_2\hbox{He}\)

式（I）也可寫成式（II）：

\({}^{238}\hbox{U}\;\to\;^{234}\hbox{Th}\;+\;\alpha\)

α衰變是一種核分裂，當中涉及[量子物理學中的](../Page/量子物理學.md "wikilink")[穿隧效應](../Page/穿隧效應.md "wikilink")，和[β衰變不同的是α衰變是由](../Page/β衰變.md "wikilink")[強核力](../Page/強核力.md "wikilink")[力場產生和控制](../Page/力場.md "wikilink")。

一顆α粒子帶有5[兆电子伏特的](../Page/電子伏特.md "wikilink")[動能](../Page/動能.md "wikilink")（約等於一顆α粒子的總能量的0.13%），其移動速度是每秒15,000公里，即是只達到5%[光速](../Page/光速.md "wikilink")（光速是時速1,079,252,848.8公里）；由於α粒子相對大的[質量](../Page/質量.md "wikilink")，其+2的電荷，以及相對慢的移動速度，它們實在太容易就會和其他原子核和粒子反應及失去其能量，α粒子在幾厘米厚度的空氣內就會被吸收。

地球上大多數的[氦氣都是來自地下蘊藏的](../Page/氦氣.md "wikilink")[礦物](../Page/礦物.md "wikilink")，如[鈾和](../Page/鈾.md "wikilink")[釷的α衰變產生的](../Page/釷.md "wikilink")。

## 参看

  - [α粒子](../Page/α粒子.md "wikilink")
  - [Β衰变](../Page/Β衰变.md "wikilink")（beta decay，即β衰变）

## 参考文献

[Category:放射線學](../Category/放射線學.md "wikilink")
[Category:原子核物理学](../Category/原子核物理学.md "wikilink")

1.  Suchocki, John. *Conceptual Chemistry*, 2007. Page 119.