**龐巴迪Dash 8**（Bombardier Dash
8，在-{zh-tw:中國大陸;zh-cn:中国;}-又常譯稱為**冲-8**\[1\]或**冲八**\[2\]，舊稱哈維蘭DHC-8）是[加拿大](../Page/加拿大.md "wikilink")[哈維蘭飛機公司設計](../Page/加拿大哈維蘭飛機公司.md "wikilink")，[龐巴迪宇航所生產的一種雙](../Page/龐巴迪宇航.md "wikilink")[渦槳發動機動力](../Page/渦輪螺旋槳發動機.md "wikilink")[區域航線用客機系列](../Page/區域航線客機.md "wikilink")，也是該公司最暢銷的機種。至2011年為止，發展出-100、-200、-300及-400和-Q400等5種民用型號和CC-142、CT-142、E-9A等3種軍用型號，總共出售超過1083架予全球超過83個用戶。

## 特性

Dash
8型螺旋槳飛機除了可以滿足嚴苛作業環境外，更非常適合高密度起降、潮溼及酷熱的作業特性，配合高可靠率的[普惠](../Page/普惠.md "wikilink")[加拿大公司的PW](../Page/加拿大.md "wikilink")123系列[引擎](../Page/引擎.md "wikilink")，截至2003年6月資料，全球Dash8機隊平均派遣率高達98.9%。除了高可靠度之外，Dash
8的平直主翼及大型垂直尾翼提供非常良好的低速特性，大量使用高強度鋁合金及先進複合材料，不但節省重量而且極為耐久，Dash
8的機身壽命高達8萬飛行小時／6萬次起降／32年，遠遠超過同級競爭機種的平均水準。

## 目前概況

2013年，龐巴迪正式發布Dash
8系列的第六代機型Q400新世代，相比於前代Q400，新世代Q400擁有全新設計的內飾和燈光，以及更大的窗戶和重新設計的起落架。同時全新的新世代Q400將會比前代更加省油，且客艙面積也會更大。

2015年，基於Q400的Q400CC發布。相比於標準版Q400，Q400CC減少了座位數量，但增加了貨艙面積，以方便北美各大航空公司的短程貨運航班（北美航空公司的貨運往往都是與客運航班共用同一架飛機的）

2018年11月，龐巴迪突然宣佈將Dash
8項目全部售予位於[英屬哥倫比亞省](../Page/英屬哥倫比亞省.md "wikilink")[悉尼市的](../Page/悉尼_\(卑詩省\).md "wikilink")[維京航空製造公司](../Page/維京航空_\(加拿大\).md "wikilink")\[3\]。

## 性能

[De_Havilland_Canada_DHC-8-400_(ANA)_with_Co-Pilot.jpg](https://zh.wikipedia.org/wiki/File:De_Havilland_Canada_DHC-8-400_\(ANA\)_with_Co-Pilot.jpg "fig:De_Havilland_Canada_DHC-8-400_(ANA)_with_Co-Pilot.jpg")
[Dash8_402Q_2006-04-07.jpg](https://zh.wikipedia.org/wiki/File:Dash8_402Q_2006-04-07.jpg "fig:Dash8_402Q_2006-04-07.jpg")Q400\]\]
[Dash_8.jpg](https://zh.wikipedia.org/wiki/File:Dash_8.jpg "fig:Dash_8.jpg")
[Scandinavian_Commuter_Dash_8_Q400_interior.jpg](https://zh.wikipedia.org/wiki/File:Scandinavian_Commuter_Dash_8_Q400_interior.jpg "fig:Scandinavian_Commuter_Dash_8_Q400_interior.jpg")

  - 特殊的連體懸梯式登機門，使旅客上下機便捷迅速。
  - 特殊的機身結構設計，具有最少8萬飛行小時或6萬航次之耐用性。
  - 特殊噪音振動抑制系統，使客艙寧靜舒適。

## 機型規格

|                                              | 100\[4\]                                                             | 200\[5\]                                                         | 300\[6\]                                                       | 400\[7\]                                                       |
| -------------------------------------------- | -------------------------------------------------------------------- | ---------------------------------------------------------------- | -------------------------------------------------------------- | -------------------------------------------------------------- |
| 單位造價(US$)                                    | $1250萬                                                               | $1300萬                                                           | $1700萬                                                         | $2700萬                                                         |
| 服役時間                                         | 1984                                                                 | 1995                                                             | 1989                                                           | 2000                                                           |
| 長度                                           | 22.25 m (73 ft)                                                      | 25.68 m (84 ft 3 in)                                             | 32.81 m (107ft 8 in)                                           |                                                                |
| 高度                                           | 7.49 m (24 ft 7 in)                                                  | 8.3 m (27 ft 3 in)                                               |                                                                |                                                                |
| 機身半徑                                         | m (8 ft 10 in2.69)                                                   |                                                                  |                                                                |                                                                |
| 最大客艙寬度                                       | 8 ft 3 in (2.51 m)                                                   |                                                                  |                                                                |                                                                |
| 客艙長度                                         | 9.1 m (29 ft 10 in)                                                  | 12.6 m (41 ft 4 in)                                              | 18.8 m (61 ft 8 in)                                            |                                                                |
| 翼展                                           | 25.89 m (84 ft 11 in)                                                | 27.43 m (90 ft)                                                  | 28.4 m (93 ft 2 in)                                            |                                                                |
| 機翼面積                                         | 54.4 m² (585.55 ft²)                                                 | 56.2 m² (604.93 ft²)                                             | 63.1 m² (679.20 ft²)                                           |                                                                |
| 機師數目                                         | 2                                                                    |                                                                  |                                                                |                                                                |
| 客艙服務人員                                       | 1                                                                    | 1–2                                                              | 2–3                                                            |                                                                |
| [發動機](../Page/發動機.md "wikilink")             | 2 [PW120A/PW121](../Page/Pratt_&_Whitney_Canada_PW100.md "wikilink") | 2 [PW123C/D](../Page/Pratt_&_Whitney_Canada_PW100.md "wikilink") | 2 [PW123B](../Page/Pratt_&_Whitney_Canada_PW100.md "wikilink") | 2 [PW150A](../Page/Pratt_&_Whitney_Canada_PW150.md "wikilink") |
| 載客量                                          | 37                                                                   | 50                                                               | 78                                                             |                                                                |
| 典型巡航速度                                       | 500 km/h (310 mph) 269 knots                                         | 537 km/h (334 mph) 290 knots                                     | 528 km/h (328 mph) 285 knots                                   | 667 km/h (414 mph) 360 knots                                   |
| 最大巡航高度                                       | 7,620 m (25,000 ft)                                                  | 8,230 m (27,000 ft)                                              |                                                                |                                                                |
| 續航力                                          | 1,889 km (1,174 miles)                                               | 1,713 km (1,065 miles)                                           | 1,558 km (968 miles)                                           | 2,522 km (1,567 miles)                                         |
| 續航力(裝備左右油箱)                                  | n/a                                                                  | 2,034 km (1,264 miles)                                           | n/a                                                            |                                                                |
| MTOW時所需跑道長度                                  | 800 m (2,625 ft)                                                     | 1,178 m (3,865 ft)                                               | 1,402 m (4,600 ft)                                             |                                                                |
| [最大起飛重量（MTOW）](../Page/最大起飛重量.md "wikilink") | 16,470 kg (36,300 lb)                                                | 19,500 kg (43,000 lb)                                            | 29,260 kg (64,500 lb)                                          |                                                                |
| 最大降落重量（MLW）                                  | 15,650 kg (34,500 lb)                                                | 19,050 kg (42,000 lb)                                            | 28,010 kg (61,750 lb)                                          |                                                                |
| 無燃料重量（ZFW）                                   | 14,700 kg (32,400 lb)                                                | 17,920 kg (39,500 lb)                                            | 25,850 kg (57,000 lb)                                          |                                                                |
| 最大燃料容量                                       | 3,210 L (848 US gal)                                                 | | 6,616 L (1,748 US gal)                                         |                                                                |                                                                |
| 操作空重                                         | 10,483 kg (23,111 lb)                                                | 11,791 kg (25,995 lb)                                            | 17,185 kg (37,886 lb)                                          |                                                                |

## 意外事件

### 主要意外

  - 1990年11月21日，[曼谷航空](../Page/曼谷航空.md "wikilink")125号班机由编号为HS-SKI的DHC-8-103执行，在暴风雨中降落[苏梅岛机场时坠毁](../Page/苏梅岛.md "wikilink")，机上38人全部遇难。\[8\]
  - 1993年1月6日，[汉莎城际航空5634号班机](../Page/汉莎城际航空5634号班机空难.md "wikilink")（编号D-BEAT的Dash
    8-311）在[巴黎夏尔·戴高乐机场的跑道外坠毁](../Page/巴黎夏尔·戴高乐机场.md "wikilink")，导致4人死亡。\[9\]
  - 1995年6月9日，[新西兰安捷航空703号班机](../Page/新西兰安捷航空703号班机空难.md "wikilink")（一架Dash
    8–102，编号ZK-NEY）由[奧克蘭飛往](../Page/奧克蘭機場.md "wikilink")[北帕莫斯顿](../Page/北帕莫斯顿.md "wikilink")，在惡劣天氣中降落時墜毀於塔拉魯瓦嶺以西（北帕莫斯頓機場以東16公里處），导致4人死亡。\[10\]
  - 2000年2月22日，[中航](../Page/中国民航.md "wikilink")[浙江航空公司一架Dash](../Page/浙江航空.md "wikilink")
    8担当杭州飞往温州的F65576次航班在[笕桥机场起飞](../Page/笕桥机场.md "wikilink")15分钟、爬升至3300米后左侧发动机内部滑油泄漏，引起发动机失效自动停车。飞机随后紧急返回笕桥机场并安全降落\[11\]\[12\]。
  - 2009年2月12日，[大陆航空快运3407号班机](../Page/科爾根航空3407號班機.md "wikilink")（编号为N200WQ的Q400）由纽瓦克飞往布法罗，在降落前失速坠毁，导致机上49人及地面1人遇难。\[13\]\[14\]\[15\]\[16\]
  - 2018年2月17日，[加拿大航空快运8585号班机事故中](../Page/加拿大航空快运8585号班机事故.md "wikilink")，一架隶属于加拿大航空快运的庞巴迪Dash
    8 Q400在萨斯卡通起飞后引擎突然起火，最终紧急返回萨斯卡通并成功迫降，机上人员无一伤亡\[17\]。
  - 2018年3月12日，一架载有71人的[美國-孟加拉航空211號班機在](../Page/美國-孟加拉航空211號班機空難.md "wikilink")[加德满都](../Page/加德满都.md "wikilink")[特里布萬國際機場冲出跑道并迅速爆炸起火](../Page/特里布萬國際機場.md "wikilink")，49人遇难。\[18\]
  - 2018年8月10日，一名（Horizon
    Air）的地勤人員擅自自[西雅圖機場開走一架Dash](../Page/西雅圖機場.md "wikilink")-8-Q400客機，逼迫機場航管人員暫停整座機場的飛機起降。[北美空防司令部](../Page/北美空防司令部.md "wikilink")（NORAD）派出兩架[F-15鷹式戰鬥機升空追查](../Page/F-15鷹式戰鬥機.md "wikilink")，客機在進行了一些類似[特技飛行般的動作之後墜毀於西雅圖機場南方約](../Page/特技飛行.md "wikilink")40英里的（Ketron
    Island, WA）上之樹林中。29歲飛機師死亡\[19\]。

### 起落架故障

2007年9月，[北欧航空的两架Dash](../Page/北欧航空.md "wikilink")
8因起落架故障先后发生两次孤立的事故，同年10月又有另外一架Dash
8发生起落架故障，北欧航空由此将其同型客机全部退役。

## 參考文獻

## 外部連結

  - [Bombardier Q
    Series](http://www.bombardier.com/en/aerospace/products/commercial-aircraft/q-series)
  - [Q400
    Website](https://web.archive.org/web/20080706133252/http://www.q400.com/)
  - [Currently flying
    aircraft](http://flightaware.com/live/aircrafttype/DH8A)
  - [DHC-8-100/200
    information](http://www.airliners.net/info/stats.main?id=120)
  - [Type
    certificate](https://web.archive.org/web/20090224014111/http://www.tc.gc.ca/aviation/applications/nico-celn/en/getfile_inc.asp?x_lang=e&AFCflag=Y&doc_type=pdf&aprv_num=A-142&isu_num=30&isu_dt_start=2007%2D04%2D12+00%3A00%3A00&proj_num=070037)
  - [Project information on the Bombardier Q400 Dash 8 Turboprop
    Regional
    Airliner](http://www.aerospace-technology.com/projects/dash8/)

[Category:支线客机](../Category/支线客机.md "wikilink")
[Category:加拿大航空器](../Category/加拿大航空器.md "wikilink")

1.  [商用飞机系列介绍 - 庞巴迪
    (中国)的官方网页](http://cn.bombardier.com/cn/products_ba_CommercialAircraft.htm)

2.  [冲八-400型飞机失事事故调查 -
    中国民用航空局](http://www.caac.gov.cn/dev/ICAO/GZDT/201005/t20100526_32711.html)


3.

4.
5.  ["Bombardier Q200
    specifications."](http://www.bombardier.com/index.jsp?id=3_0&lang=en&file=/en/3_0/3_1/3_1_2_4_3.html)
     *bombardier.com.* Retrieved: April 6, 2008.

6.  ["Bombardier Q300
    specifications."](http://www.bombardier.com/index.jsp?id=3_0&lang=en&file=/en/3_0/3_1/3_1_2_5_3.html)
     *bombardier.com.* Retrieved: April 6, 2008.

7.  ["Bombardier Q400
    specifications."](http://www.q400.com/q400/en/specifications.jsp)
    *bombardier.com.* Retrieved: April 6, 2008.

8.  [Koh "Samui crash."](http://www.planecrashinfo.com/1990/1990-67.htm)
    *planecrashinfo.com*. Retrieved: December 24, 2009.

9.  ["Accident Report: de Havilland Canada DHC-8-3110,
    January 6, 1993."](http://aviation-safety.net/database/record.php?id=19930106-0)
    *Aviation Safety Network*. Retrieved: July 17, 2011.

10. [Aviation
    Reports](http://www.taic.org.nz/AviationReports/tabid/78/ctl/Detail/mid/482/InvNumber/1995-011/Page/14/Default.aspx)


11.

12.

13. ["'Ice build-up' on crashed
    plane."](http://news.bbc.co.uk/2/hi/americas/7889764.stm) *BBC,*
    February 14, 2009. Retrieved: July 17, 2011.

14. ["Plane crash in NY state
    kills 49."](http://news.bbc.co.uk/1/hi/world/americas/7887555.stm)
    *BBC News*, February 13, 2009. Retrieved: August 15, 2009.

15. ["Pilot error caused 2009 crash near Buffalo, NTSB
    rules."](http://www.cnn.com/2010/TRAVEL/02/02/continental.crash.inquiry/index.html?hpt=T2)
    *CNN,* 2 February 2010.

16. Grady, Mary. ["NTSB Concludes Pilot's Actions Caused Colgan
    Crash."](http://www.avweb.com/avwebflash/news/NTSBConcludesPilotsActionsCausedColganCrash_201945-1.html)
    *avweb.com*, February 2010. Retrieved: February 3, 2010.

17.

18. ["US Bangla Bombardier Dash 8 S2-AGU crashes at TIA during
    landing."](https://www.aviationnepal.com/us-bangla-bombardier-dash-8-s2-agu-crashes-at-tia-during-landing/)
    *www.aviationnepal.com* , 2018-03-12.

19.