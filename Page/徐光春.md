**徐光春**（），[浙江](../Page/浙江省.md "wikilink")[绍兴人](../Page/绍兴.md "wikilink")；[中国人民大学新闻系毕业](../Page/中国人民大学.md "wikilink")。1973年加入[中国共产党](../Page/中国共产党.md "wikilink")，是中共第十五届中央纪委委员，第十六、十七屆中央委員。现任[第十一届全国人大财经委员会副主任委员](../Page/第十一届全国人大.md "wikilink")。曾长期在中共报刊宣传部门任职，历任[中宣部副部长](../Page/中宣部.md "wikilink")、[国家广播电影电视总局局长](../Page/国家广播电影电视总局.md "wikilink")、[中共河南省委书记等职](../Page/中共河南省委.md "wikilink")。

## 简历

徐光春1969年大学毕业。曾在农村、工厂、军队工作。1975年调安徽新闻图片社任记者；四年后，进入[新华社安徽分社](../Page/新华社.md "wikilink")；并历任记者、组长、分社党组书记兼副社长。1982年加入[中国摄影家协会](../Page/中国摄影家协会.md "wikilink")，任安徽新闻摄影研究会会长。

1984年进入政界，当选为中共安徽省委候补委员；次年，出任新华社上海分社社长、兼党组书记；1988年，转任北京分社社长、兼党组书记；1991年出任《[光明日报](../Page/光明日报.md "wikilink")》社副总编辑、高级记者；两年后，升任报社总编辑。

1993年7月，徐光春进入政府部门，担任国务院环境保护委员会委员；1995年，任中共中央宣传部副部长；2000年6月任[国家广播电影电视总局局长](../Page/国家广播电影电视总局.md "wikilink")。2004年12月，60岁的徐光春接替调职的[李克强](../Page/李克强.md "wikilink")，出任中共河南省委书记；并在次年1月，当选为河南省人大常委会主任。

2009年11月30日，年届65岁、即将退休的徐光春，被宣布免去河南省的领导职务；并请辞河南省人大常委会主任。一个月后，他被任命为第十一届全国人大财经委员会副主任委员。

## 參考文獻

  - [新华网：徐光春](http://news.xinhuanet.com/ziliao/2002-03/05/content_300373.htm)
  - [徐光春辞去河南人大主任职务](http://news.sina.com.cn/o/2010-01-20/073016961607s.shtml)

{{-}}

[Category:绍兴人](../Category/绍兴人.md "wikilink")
[Category:杭州高级中学校友](../Category/杭州高级中学校友.md "wikilink")
[Category:中国人民大学校友](../Category/中国人民大学校友.md "wikilink")
[Category:中共河南省委书记](../Category/中共河南省委书记.md "wikilink")
[Category:中共中央宣传部副部长](../Category/中共中央宣传部副部长.md "wikilink")
[Category:国家广播电影电视总局局长](../Category/国家广播电影电视总局局长.md "wikilink")
[Category:中华人民共和国编辑](../Category/中华人民共和国编辑.md "wikilink")
[Category:中華人民共和國記者](../Category/中華人民共和國記者.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:第十届全国人大代表](../Category/第十届全国人大代表.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[G](../Category/徐姓.md "wikilink")
[Category:光明日报社总编辑](../Category/光明日报社总编辑.md "wikilink")
[Category:中共十九大代表](../Category/中共十九大代表.md "wikilink")
[Category:河南省人大常委会主任](../Category/河南省人大常委会主任.md "wikilink")