**尹蔚民**（），[河北](../Page/河北省.md "wikilink")[灵寿县人](../Page/灵寿县.md "wikilink")，[在职研究生学历](../Page/在职研究生.md "wikilink")，[经济学](../Page/经济学.md "wikilink")[硕士學歷](../Page/硕士.md "wikilink")，1973年加入[中国共产党](../Page/中国共产党.md "wikilink")。是中国共产党十七届、十八届中央委员。曾任[國家公務員局局長](../Page/國家公務員局.md "wikilink")、[中華人民共和國人力資源和社會保障部部長](../Page/中華人民共和國人力資源和社會保障部.md "wikilink")。

## 簡歷

历任[中共中央组织部经济干部局干事](../Page/中共中央组织部.md "wikilink")、轻工交通处副处长、中央机关干部局五处副处长、中央组织部办公厅副处级秘书、任中央组织部[党政外事干部局四处副处长](../Page/党政外事干部局.md "wikilink")、处长、党政外事干部局副局长、局长、中央组织部[经济科技干部局局长](../Page/经济科技干部局.md "wikilink")、中央组织部干部四局局长兼企业干部工作办公室主任。

2000年11月至2005年3月任人事部副部长、党组成员，2005年3月后人事部副部长、党组副书记。

2007年8月30日，十届[全国人大常委会第二十九次会议决定](../Page/全国人大常委会.md "wikilink")，任命尹蔚民为[人事部部长](../Page/中華人民共和國人事部.md "wikilink")。并担任[中共中央组织部副部长](../Page/中共中央组织部.md "wikilink")。

根據2008年3月十一屆[全國人大一次會議通過的](../Page/全國人大.md "wikilink")[國務院機構改革方案](../Page/國務院機構改革方案.md "wikilink")，人事部與[勞動和社會保障部合併為人力資源和社會保障部](../Page/中華人民共和國勞動和社會保障部.md "wikilink")，3月17日，[第十一屆全國人民代表大會第一次全體會議決定任命尹蔚民为人力資源和社會保障部部長](../Page/第十一屆全國人民代表大會.md "wikilink")，3月19日。[中共中央決定](../Page/中共中央.md "wikilink")，尹蔚民任人力資源和社會保障部黨組書記，兼任[國家公務員局局長](../Page/中華人民共和國國家公務員局.md "wikilink")。2014年9月12日，國務院免去尹蔚民兼任的國家公務員局局長職務。2018年1月，当选[第十三届全国政协委员](../Page/中国人民政治协商会议第十三届全国委员会委员名单.md "wikilink")\[1\]。

## 参考文献

  - [人民网：尹蔚民简历](http://politics.people.com.cn/GB/shizheng/252/9667/9683/6197520.html)
  - [中華人民共和國中央人民政府官方網站：人力資源和社會保障部領導班子組成](http://www.gov.cn/rsrm/2008-03/21/content_925689.htm)

{{-}}

[W](../Category/尹姓.md "wikilink") [Y](../Category/灵寿人.md "wikilink")
[Category:吉林大学校友](../Category/吉林大学校友.md "wikilink")
[Y](../Category/中華人民共和國政府官员.md "wikilink")
[Y](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Y](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:中华人民共和国人力资源和社会保障部部长](../Category/中华人民共和国人力资源和社会保障部部长.md "wikilink")
[Category:中华人民共和国人事部部长](../Category/中华人民共和国人事部部长.md "wikilink")
[Category:中共中央组织部副部长](../Category/中共中央组织部副部长.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")

1.