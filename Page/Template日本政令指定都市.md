[千葉市](千葉市.md "wikilink")<span style="font-size:smaller;">（[千葉縣](千葉縣.md "wikilink")）</span>{{\!w}}
[橫濱市](橫濱市.md "wikilink"){{\\w}} [川崎市](川崎市.md "wikilink"){{\\w}}
[相模原市](相模原市.md "wikilink")<span style="font-size:smaller;">（[神奈川縣](神奈川縣.md "wikilink")）</span>
|group4=[中部地方](中部地方.md "wikilink")
|list4=[新潟市](新潟市.md "wikilink")<span style="font-size:smaller;">（[新潟縣](新潟縣.md "wikilink")）</span>{{\!w}}
[靜岡市](靜岡市.md "wikilink"){{\\w}}
[濱松市](濱松市.md "wikilink")<span style="font-size:smaller;">（[靜岡縣](靜岡縣.md "wikilink")）</span>{{\!w}}
[名古屋市](名古屋市.md "wikilink")<span style="font-size:smaller;">（[愛知縣](愛知縣.md "wikilink")）</span>
|group5=[近畿地方](近畿地方.md "wikilink")
|list5=[京都市](京都市.md "wikilink")<span style="font-size:smaller;">（[京都府](京都府.md "wikilink")）</span>{{\!w}}
[大阪市](大阪市.md "wikilink"){{\\w}}
[堺市](堺市.md "wikilink")<span style="font-size:smaller;">（[大阪府](大阪府.md "wikilink")）</span>{{\!w}}
[神户市](神户市.md "wikilink")<span style="font-size:smaller;">（[兵庫縣](兵庫縣.md "wikilink")）</span>
|group6=[中國地方](中國地方.md "wikilink")
|list6=[岡山市](岡山市.md "wikilink")<span style="font-size:smaller;">（[岡山縣](岡山縣.md "wikilink")）</span>{{\!w}}
[廣島市](廣島市.md "wikilink")<span style="font-size:smaller;">（[廣島縣](廣島縣.md "wikilink")）</span>
|group7=[四國地方](四國地方.md "wikilink") |list7=無
|group8=[九州地方](九州地方.md "wikilink")
|list8=[北九州市](北九州市.md "wikilink"){{\\w}}
[福岡市](福岡市.md "wikilink")<span style="font-size:smaller;">（[福岡縣](福岡縣.md "wikilink")）</span>{{\!w}}
[熊本市](熊本市.md "wikilink")<span style="font-size:smaller;">（[熊本縣](熊本縣.md "wikilink")）</span>
|below=日本行政區劃：[都道府縣](都道府縣.md "wikilink"){{-w}}
[市](市.md "wikilink")（[政令指定都市](政令指定都市.md "wikilink")－[中核市](中核市.md "wikilink")－[特例市](特例市.md "wikilink")）/[町](町_\(行政區劃\).md "wikilink")/[村](村_\(日本\).md "wikilink")
{{-w}}（[區](區.md "wikilink")）{{-w}}
[町](町_\(行政區劃\).md "wikilink")/[大字](大字.md "wikilink"){{-w}}
[丁目](丁目.md "wikilink")/[小字](小字.md "wikilink")
}}<includeonly></includeonly><noinclude>

</noinclude>

[Category:政令指定都市](../Category/政令指定都市.md "wikilink")
[日本行政區劃模板](../Category/日本行政區劃模板.md "wikilink")