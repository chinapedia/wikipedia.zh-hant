**王僧辯**（），[南梁將領](../Page/南梁.md "wikilink")。字君才，太原祁（今[山西](../Page/山西.md "wikilink")[祁縣](../Page/祁縣.md "wikilink")）人，右衛將軍[王神念之子](../Page/王神念.md "wikilink")\[1\]。

## 生平

王僧辯早年隨父親南歸南梁，一直跟隨著湘東王[蕭繹](../Page/蕭繹.md "wikilink")，歷任中兵參軍及貞毅府諮議參軍，後任[竟陵](../Page/竟陵.md "wikilink")[太守](../Page/太守.md "wikilink")。梁大寶元年（550年），發生[侯景之亂](../Page/侯景之亂.md "wikilink")，[侯景發兵攻至](../Page/侯景.md "wikilink")[台城](../Page/台城.md "wikilink")，包圍梁武帝，後[梁武帝餓死台城](../Page/梁武帝.md "wikilink")。[陳霸先在始興](../Page/陳霸先.md "wikilink")（今[廣東](../Page/廣東.md "wikilink")[韶關西南](../Page/韶關.md "wikilink")）起兵，受湘東王[蕭繹節制](../Page/蕭繹.md "wikilink")。

大寶二年（551年），蕭繹命王僧辯與陳霸先會合，東下進攻盤據建康（今江蘇[南京](../Page/南京.md "wikilink")）已四年之久的侯景，侯景戰敗而逃，被部下所殺。不久，蕭繹就在江陵自立為帝，是為梁元帝。西元554年，[西魏趁機發兵江陵](../Page/西魏.md "wikilink")，蕭繹被殺，陳霸先、王僧辯立蕭繹第九子[蕭方智](../Page/蕭方智.md "wikilink")，稱梁王於建康。

天成元年（555年）王僧辯單獨与[北齊](../Page/北齊.md "wikilink")[媾和](../Page/媾和.md "wikilink")，扶植梁武帝姪兒貞陽侯[蕭淵明為帝](../Page/蕭淵明.md "wikilink")，引發陳霸先不滿。陈霸先苦劝无效，遂于是年9月起兵诛杀王僧辯，仍立蕭方智為帝。後萧方智禅位，陈霸先代梁称帝建立[陈朝](../Page/陈朝.md "wikilink")，是為陳武帝。\[2\]

## 家族

长子[王顗](../Page/王顗.md "wikilink")。

次子[王颁](../Page/王颁.md "wikilink")。王颁随[隋军伐](../Page/隋朝.md "wikilink")[陈](../Page/陈朝.md "wikilink")，其后掘开[陈霸先墓](../Page/陈霸先.md "wikilink")，将骨骸焚化成灰，加水喝进肚中。

三子王頠。

有子王颙，隋礼部侍郎。王颙有一八世孙女嫁文林郎、试左武卫兵曹参军[彭城刘思友](../Page/彭城.md "wikilink")，另一八世孙女嫁杞王府咨议、分司东都弘农杨某。

有子[王頍](../Page/王頍.md "wikilink")，后因参与隋汉王[杨谅作乱被杀](../Page/杨谅.md "wikilink")。

[唐睿宗妃嫔王德妃](../Page/唐睿宗.md "wikilink")、贤妃王芳媚姐妹二人都是王僧辩后裔。

## 註解

[W王](../Page/category:南北朝军事人物.md "wikilink")
[W王](../Page/category:南北朝戰爭身亡者.md "wikilink")
[W王](../Page/category:祁县人.md "wikilink")
[S僧](../Page/category:王姓.md "wikilink")
[category:太原王氏](../Page/category:太原王氏.md "wikilink")

1.  [南史第六十三卷](../Page/南史.md "wikilink")。
2.  [陳書第一卷](../Page/陳書.md "wikilink")；
    [南史第九卷](../Page/南史.md "wikilink")。