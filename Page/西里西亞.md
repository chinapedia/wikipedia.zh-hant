[XilixiyaMap.png](https://zh.wikipedia.org/wiki/File:XilixiyaMap.png "fig:XilixiyaMap.png")前属于奥地利的西里西亚。\]\]
**西里西亞**（[波兰语](../Page/波兰语.md "wikilink")：*Śląsk*；[德语](../Page/德语.md "wikilink")：*Schlesien*；[捷克语](../Page/捷克语.md "wikilink")：*Slezsko*；[西里西亚语](../Page/西里西亚语.md "wikilink")：*Ślonsk*
/
*Ślónsk*；[拉丁语](../Page/拉丁语.md "wikilink")：Silesia）是[中欧的一个历史地域名称](../Page/中欧.md "wikilink")。目前，该地域的绝大部分地区属于[波兰西南部](../Page/波兰.md "wikilink")，小部分则属于[捷克和](../Page/捷克.md "wikilink")[德国](../Page/德国.md "wikilink")。[奥得河及其](../Page/奥得河.md "wikilink")[支流几乎流经整个地区](../Page/支流.md "wikilink")，两岸都有许多重要城市。该地沿着[苏台德山脉](../Page/苏台德山脉.md "wikilink")，其南部与[波希米亚和](../Page/波希米亚.md "wikilink")[摩拉维亚接壤](../Page/摩拉维亚.md "wikilink")。西里西亚现在最大的城市是历史名城[弗罗茨瓦夫和](../Page/弗罗茨瓦夫.md "wikilink")[卡托维兹](../Page/卡托维兹.md "wikilink")。

在[中世纪](../Page/中世纪.md "wikilink")，西里西亚最先属于波兰[皮亚斯特王朝](../Page/皮亚斯特王朝.md "wikilink")，后来为[波希米亚王国夺得并成为](../Page/波希米亚王国.md "wikilink")[神圣罗马帝国的一部分](../Page/神圣罗马帝国.md "wikilink")。1526年起，它随着波希米亚王国归于奥地利[哈布斯堡王朝统治](../Page/哈布斯堡王朝.md "wikilink")。1742年，[普鲁士的](../Page/普鲁士.md "wikilink")[腓特烈大帝在](../Page/腓特烈大帝.md "wikilink")[奥地利王位继承战争取胜](../Page/奥地利王位继承战争.md "wikilink")，从奥地利获得西里西亚的大部分。这些地区后来组成了普鲁士的[西里西亚省](../Page/西里西亚省.md "wikilink")。

1945年之后，西里西亚绝大部分并入[波兰](../Page/波兰.md "wikilink")（见右图）。小部分位于德国[萨克森自由州](../Page/萨克森.md "wikilink")，而[奥匈帝国统治的部分现在位于](../Page/奥匈帝国.md "wikilink")[捷克](../Page/捷克.md "wikilink")。

现在，西里西亚大部分地区位于波兰，目前被分为数个省（Voivodship）：

  - [大波兰省](../Page/大波兰省.md "wikilink")
  - [小波兰省](../Page/小波兰省.md "wikilink")
  - [下西里西亚省](../Page/下西里西亚省.md "wikilink")
  - [盧布斯卡省](../Page/盧布斯卡省.md "wikilink")
  - [奥波莱省](../Page/奥波莱省.md "wikilink")
  - [西里西亚省](../Page/西里西亚省.md "wikilink")

其中[奥波莱省和](../Page/奥波莱省.md "wikilink")[西里西亚省又称为](../Page/西里西亚省.md "wikilink")[上西里西亚](../Page/上西里西亚.md "wikilink")。捷克境内的小部分西里西亚部分称为“[捷克西里西亚](../Page/捷克西里西亚.md "wikilink")”，现在主要是[摩拉维亚-西里西亚省的一部分](../Page/摩拉维亚-西里西亚省.md "wikilink")，剩余部分位于[奥洛穆茨省](../Page/奥洛穆茨省.md "wikilink")。

传统上，西里西亚位于Kwisa河与Bobr河之间，而前者西方称为上[卢萨蒂亚](../Page/卢萨蒂亚.md "wikilink")（Lusatia；曾称为*Milsko*）。由于普鲁士曾拥有下西里西亚，今德国的[下西里西亚上卢萨蒂亚](../Page/下西里西亚上卢萨蒂亚.md "wikilink")（Niederschlesischer
Oberlausitzkreis）和[霍耶斯韦达](../Page/霍耶斯韦达.md "wikilink")（Hoyerswerda）也被认为属于西里西亚。该两区和下西里西亚省称为[下西里西亚](../Page/下西里西亚.md "wikilink")。

## 名称

有人认为“西里西亚”（Silesia）来自[斯林加俄人](../Page/斯林加俄人.md "wikilink")（Silingi；拉丁语：Silingae），是一个[汪达尔人族群](../Page/汪达尔人.md "wikilink")，可能在2世纪于[波罗的海南岸的](../Page/波罗的海.md "wikilink")[易北河](../Page/易北河.md "wikilink")、[奥德河和](../Page/奥德河.md "wikilink")[维斯瓦河聚居](../Page/维斯瓦河.md "wikilink")。[欧洲移民时期](../Page/:en:Migration_Period.md "wikilink")，他们离开家园，留下他们社会的遗蹟。

最明显的遗产是地名——都是斯拉夫裔新居民采用的斯拉夫式名字（[波兰语](../Page/波兰语.md "wikilink")：Śląsk、[古波兰语](../Page/古波兰语.md "wikilink")：Śląžsk
\[-o\]，而[古斯拉夫语](../Page/古斯拉夫语.md "wikilink")：Sьlьąžьskъ
\[\<\*sǐlęgǐskǔ\]则来自古[汪达尔语](../Page/汪达尔语.md "wikilink")：Siling-isk
\[land\]）。这些居民于是被称为[西里西亚人](../Page/西里西亚人.md "wikilink")（波兰语：Ślężanie），虽然与最初的居民没有共同之处。7世纪至8世纪的遗蹟，显示一些曾经有大量人口的地区，从西到南更有密集的堡垒。北与东方没有此類堡垒，這证明从5世纪到13世纪，[斯拉夫人在西里西亚聚居](../Page/斯拉夫人.md "wikilink")。不過由於日耳曼民族的[哥德人在东方](../Page/哥德人.md "wikilink")，而斯拉夫的[文德人](../Page/文德人.md "wikilink")（Wend）则在西方，故此堡垒不支持任何[民族主义理論](../Page/民族主义.md "wikilink")。

## 历史

### 早期居民

早在[石器时代](../Page/石器时代.md "wikilink")、[青铜时代和](../Page/青铜时代.md "wikilink")[铁器时代](../Page/铁器时代.md "wikilink")，已经有人类在该地居住。

首批描述西里西亚的文献，出自埃及人[托勒密和罗马人](../Page/托勒密.md "wikilink")[塔西佗](../Page/塔西佗.md "wikilink")。塔西佗指出，1世纪时西里西亚居民分为多个种族，以[鲁基人](../Page/鲁基人.md "wikilink")（Lugii）为首。斯林加俄人也属于这团体。其他[东日耳曼族群也在该人口稀少的地区聚居](../Page/东日耳曼族群.md "wikilink")。

### 中世纪

公元500年后，欧洲移民时期开始，大部分东日耳曼族群离开西里西亚，迁移到南欧。来自东方的斯拉夫族群成为新居民。

早期文献指出，当时的居民可能大多是斯拉夫族群。大约在845年推出的[巴伐利亚](../Page/巴伐利亚.md "wikilink")[地理学家](../Page/地理学家.md "wikilink")（Bayerischer
Geograph）指出，这些族群包括Slenzanie、Dzhadoshanie、Opolanie、Lupiglaa和Golenshitse。1086年，一位[布拉格主教指出还有Zlasane](../Page/布拉格.md "wikilink")、Trebovyane、Poborane和Dedositze。

9至10世纪，今[捷克境内的](../Page/捷克.md "wikilink")[大摩拉维亚](../Page/大摩拉维亚.md "wikilink")、[摩拉维亚和](../Page/摩拉维亚.md "wikilink")[波希米亚先后统治西里西亚](../Page/波希米亚.md "wikilink")。约990年，[皮亚斯特王朝的](../Page/皮亚斯特王朝.md "wikilink")[梅什科一世将西里西亚并入](../Page/梅什科一世.md "wikilink")[波兰](../Page/波兰.md "wikilink")。不过，一些历史学家认为，应该是他的儿子[波列斯瓦夫一世在](../Page/波列斯瓦夫一世.md "wikilink")999年兼并该地。波兰分裂期间（1138-1320年），该王朝不同的支系将该地分为[公国统治](../Page/公国.md "wikilink")。

1146年，波兰的[瓦迪斯瓦夫二世大公向](../Page/瓦迪斯瓦夫二世.md "wikilink")[神圣罗马帝国称臣](../Page/神圣罗马帝国.md "wikilink")，但是被亲弟驱逐。1163年，他的两名儿子得到[波列斯瓦夫四世批准](../Page/波列斯瓦夫四世.md "wikilink")，分治上和下西里西亚。于是，他们成为皮亚斯特王朝的两大支系，亦即[弗罗茨瓦夫](../Page/弗罗茨瓦夫.md "wikilink")（Wrocławska）和[奥波莱](../Page/奥波莱.md "wikilink")-[拉齐布日](../Page/拉齐布日.md "wikilink")（Opolsko-Raciborska）。这两支系继续细分，直到1390年代共有16个公国。

1241年，[蒙古族袭击](../Page/蒙古族.md "wikilink")[小波兰](../Page/小波兰.md "wikilink")，然后入侵西里西亚，引起居民恐慌和逃亡潮。蒙古族在该地大部分地区抢掠，但可能因为被Blessed
Cheslav的“神奇火球”击退，故此放弃攻打[弗罗茨瓦夫的堡垒](../Page/弗罗茨瓦夫.md "wikilink")。然后，他们在[列格尼卡战役消灭波兰与德意志联军](../Page/列格尼卡战役.md "wikilink")。[窝阔台死后](../Page/窝阔台.md "wikilink")，蒙古人决定不继续进攻欧洲，改为返回东方选出新[可汗](../Page/可汗.md "wikilink")。

西里西亚的统治者决定重建城市，采用新的行政区划。他们建立和重建160个城市和1500个市镇，并采用[德意志城邦法令](../Page/德意志城邦法令.md "wikilink")（[马德堡法令和Środa](../Page/马德堡法令.md "wikilink")
Śląska法令），取代了故旧、按惯例的斯拉夫式和波兰式法令。而且，从13世纪末到14世纪初，他们也鼓励新居民迁到该地，大多是从神圣罗马帝国而来的[德国人和](../Page/德国人.md "wikilink")[荷兰人](../Page/荷兰人.md "wikilink")。从13世纪末到14世纪初，西里西亚的公爵吸引很多德国人到该地定居。德国人、[犹太人和](../Page/犹太人.md "wikilink")[捷克人多数都在城市定局](../Page/捷克人.md "wikilink")。在郊区，尤其是上西里西亚，波兰裔居民仍占大多数。邀请德国人开拓和灌溉贫瘠土地；统治阶层与德国和斯拉夫居民同化——这成为后来19世纪与20世纪初，波兰和德国[民族主义者](../Page/民族主义.md "wikilink")[意识形态的纷争](../Page/意识形态.md "wikilink")。

13世纪后期，、[医院骑士团和](../Page/医院骑士团.md "wikilink")[条顿骑士团先后在西里西亚定居](../Page/条顿骑士团.md "wikilink")。

很多皮亚斯特公爵尝试将西里西亚并入波兰，甚至统一波兰本身。西里西亚公爵[亨里克四世率先作此尝试](../Page/亨里克四世.md "wikilink")，但在1290年去世，未能完成目标。然后，[大波兰公爵](../Page/大波兰.md "wikilink")[普热梅斯瓦夫二世统一了西里西亚两个省](../Page/普热梅斯瓦夫二世.md "wikilink")，在1295加冕为王，却在翌年被谋杀。根据他的遗愿，[格沃古夫](../Page/格沃古夫.md "wikilink")（Głogów）公爵[亨利克·格沃格夫斯基将会继承大波兰治权](../Page/亨利克·格沃格夫斯基.md "wikilink")。格沃格夫斯基也想统一波兰，甚至向问鼎波兰公爵之位。不过，大波兰大多数贵族支持[库亚维](../Page/库亚维.md "wikilink")（Kujawy；Kuyavia）支系的皮亚斯特公爵[瓦迪斯瓦夫一世](../Page/瓦迪斯瓦夫一世.md "wikilink")。瓦迪斯瓦夫有较多贵族支持，故此成功。这个时候，[波希米亚的](../Page/波希米亚.md "wikilink")[瓦茨拉夫二世决定扩张版图](../Page/瓦茨拉夫二世.md "wikilink")，在1302年自封波兰国王。往后的半世纪，瓦迪斯瓦夫一世和他的儿子[卡齐米日三世与波希米亚](../Page/卡齐米日三世.md "wikilink")、[勃兰登堡和](../Page/勃兰登堡.md "wikilink")[条顿骑士团参与战争](../Page/条顿骑士团.md "wikilink")，各方都希望瓜分波兰。很多西里西亚公爵统治小型公国，虽然与波兰有联系，但未能与她联合，反而受到波希米亚的支配或统治。

1335年，弗洛茨瓦夫公爵亨利六世和上西里西亚公爵向[波希米亚王](../Page/波希米亚.md "wikilink")[卢森堡的约翰称臣](../Page/约翰_\(波希米亚\).md "wikilink")。1348年，卡西米尔三世被迫接受波希米亚支配西里西亚。1368年，该地独立的皮亚斯特公国全数灭亡。不过，西里西亚支系的皮亚斯特王朝在14至16世纪渐渐衰落，到1675年才绝嗣：

  - 1335年，弗洛茨瓦夫支系
  - 1368年，[希维德尼察](../Page/希维德尼察.md "wikilink")（Świdnica）支系
  - 1476年，[奥勒希尼察](../Page/奥勒希尼察.md "wikilink")（Oleśnicka）和[格沃古夫](../Page/格沃古夫.md "wikilink")（Głogów）支系
  - 1504年，[沙加尼](../Page/沙加尼.md "wikilink")（Żagań）支系
  - 1532年，[奥波莱支系](../Page/奥波莱.md "wikilink")
  - 1625年，[切申](../Page/切申.md "wikilink")（Cieszyn）支系
  - 1675年，[布热格](../Page/布热格.md "wikilink")（Brzeg）-[莱格尼察](../Page/莱格尼察.md "wikilink")（Legnica）支系

[切申公国最后男性成员](../Page/切申公国.md "wikilink")在1625年已去世，但其姊妹继续统治公国，直到她在1653年去世。

西里西亚也成为[神圣罗马帝国的领土](../Page/神圣罗马帝国.md "wikilink")，不过波希米亚则属于帝国的自治省。西里西亚属于波希米亚，直到1740年后，则由捷克、波兰和普魯士统治。[神圣罗马帝国皇帝](../Page/神圣罗马帝国皇帝.md "wikilink")[查理四世统治期间](../Page/查理四世_\(神圣罗马帝国\).md "wikilink")，西里西亚和弗罗茨瓦夫地位提高，当地增建了很多宏伟建筑物和[哥特式教堂](../Page/哥特式建筑.md "wikilink")。当地与居民，以至其语言和文化，则受德国移民影响。

1420至1434年，波希米亚发生[胡斯战争](../Page/胡斯战争.md "wikilink")（Hussite
Wars），该地受到破坏。[胡斯派](../Page/胡斯派.md "wikilink")（Hussites）针对德裔人口。尤其是上西里西亚的一些地区中，斯拉夫人口再度占一席位。战争结束后，多数西里西亚人口仍然信奉[天主教](../Page/天主教.md "wikilink")。不过在，愈来愈多人信奉[信义宗](../Page/信义宗.md "wikilink")。

### 宗教改革

西里西亚继续与邻近地区保持紧密经济关系，例如波兰。尤其在[文艺复兴及以后](../Page/文艺复兴.md "wikilink")，西里西亚城市的[犹太裔商人增加经济活动](../Page/犹太裔.md "wikilink")。

16世纪，[马丁·路德的宗教改革运动率先在西里西亚开始](../Page/马丁·路德.md "wikilink")，大部分的居民都随之皈依了新教。很多新教牧师促进当地斯拉夫文化和语言发展。

1526年，[奥地利皇帝](../Page/奥地利.md "wikilink")[斐迪南一世将原本以选举产生王位的](../Page/斐迪南一世_\(神圣罗马帝国\).md "wikilink")[波希米亚王国](../Page/波希米亚王国.md "wikilink")，改为由哈布斯堡王朝继承。1537年，皮亚斯特家族的[布热格公爵](../Page/布热格.md "wikilink")（或稱西里西亞公爵，領地佔全[西里西亞的六分之一](../Page/西里西亞.md "wikilink")）[腓特烈二世和](../Page/腓特烈二世_\(布热格\).md "wikilink")[勃兰登堡选帝侯](../Page/勃兰登堡选帝侯.md "wikilink")[约阿希姆二世达成协定](../Page/约阿希姆二世.md "wikilink")，规定如若皮亚斯特家族绝嗣，勃兰登堡的[霍亨索伦家族成员可以继承公国](../Page/霍亨索伦家族.md "wikilink")，但是斐迪南一世拒绝承认此协定，後代皇帝就在1675年併吞絕嗣的[布热格公國](../Page/布热格.md "wikilink")。

17世纪初开始，[基督教改革派和](../Page/基督教.md "wikilink")[反改革派展开宗教冲突和战争](../Page/反宗教改革.md "wikilink")，迫使很多捷克（包括[日耳曼人与](../Page/日耳曼人.md "wikilink")[斯拉夫人](../Page/斯拉夫人.md "wikilink")）和西里西亚[新教徒逃到当时宗教政策较宽容的](../Page/新教.md "wikilink")[波兰-立陶宛联邦](../Page/波兰-立陶宛联邦.md "wikilink")（Polish-Lithuanian
Commonwealth）。许多新教徒在[大波兰省定居](../Page/大波兰.md "wikilink")，并得到新教权贵的保护，例如[拉法瓦·勒施钦斯基](../Page/拉法瓦·勒施钦斯基.md "wikilink")（Rafał
Leszczyński；1579-1636年）。势力壮大的[捷克新教徒](../Page/捷克友爱会.md "wikilink")（Union
of the
Brethren）迁到了[莱什诺](../Page/莱什诺.md "wikilink")（Leszno），例如[夸美纽斯](../Page/夸美纽斯.md "wikilink")。为了规避宗教限制法规，西里西亚的新教徒常常把他们的教堂建在与波兰接壤的边界上。

### 三十年战争

[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")[斐迪南二世企图复兴天主教](../Page/斐迪南二世_\(神圣罗马帝国\).md "wikilink")，镇压新教。结果在1618年，[布拉格拋窗事件发生](../Page/布拉格拋窗事件.md "wikilink")，间接酿成[三十年战争](../Page/三十年战争.md "wikilink")。

捷克新教徒得到德国教友和匈牙利贵族的支持。[特兰西瓦尼亚王子](../Page/特兰西瓦尼亚.md "wikilink")、匈牙利贵族[拜特伦·加波尔](../Page/拜特伦·加波尔.md "wikilink")（Bethlen
Gabor）率军围攻[维也纳](../Page/维也纳.md "wikilink")，尝试扩展特兰西瓦尼亚以至[奥斯曼帝国的版图](../Page/奥斯曼帝国.md "wikilink")，兼并波希米亚和西里西亚。波兰贵族至少口头上支持捷克人。他们认为捷克和匈牙利贵族是支持[自由的人](../Page/自由.md "wikilink")，抵抗[专制君主](../Page/君主专制.md "wikilink")，正好响应波兰[施拉赫塔](../Page/施拉赫塔.md "wikilink")（Szlachta；中世纪后期波兰贵族阶层）[黄金自由的理想](../Page/黄金自由.md "wikilink")。这些贵族不支持攻打新教徒，而[波兰众议院](../Page/波兰众议院.md "wikilink")（Sejm）甚至不准国王[齐格蒙特三世帮助其天主教盟友](../Page/齐格蒙特三世.md "wikilink")[哈布斯堡王朝](../Page/哈布斯堡王朝.md "wikilink")。国王希望帮助奥地利，但因为签署了防卫条约，他只能拒绝[马提亚斯皇帝](../Page/马提亚斯_\(神圣罗马帝国\).md "wikilink")（Matthias,
Holy Roman
Emperor）聘用[波兰-立陶宛的军队](../Page/波兰立陶宛联邦.md "wikilink")。这是因为波兰-立陶宛曾分别与[瑞典和](../Page/波兰-立陶宛-瑞典战争.md "wikilink")[莫斯科公国交战而军力疲乏](../Page/波兰-莫斯科公国战争.md "wikilink")，更因[贵族战争在摩达维亚而与](../Page/贵族战争在摩达维亚.md "wikilink")[奥斯曼帝国关系变差](../Page/奥斯曼帝国.md "wikilink")。

最后在1619年末，西格蒙德三世决定帮助奥地利，但只是派出私人雇佣兵[Lisowczycy](../Page/Lisowczycy.md "wikilink")。其中一个原因就是，这些士兵在对莫斯科战争后失业，转而到立陶宛大肆抢掠；而且，他也认为可以从奥地利得到部分西里西亚作为报酬。虽然Lisowczycy在[白山之役](../Page/白山之役.md "wikilink")（Battle
of White Mountain）奠定奥地利的胜利，但波兰[瓦萨王朝](../Page/瓦萨王朝.md "wikilink")（House
of Vasa）并没有实际得益。奥地利只是含糊其辞，给他们一些承诺，并让几位公主与波兰皇室联婚，保持两国联盟。

1619年中，波兰国王在前往西里西亚途中，与长子瓦迪斯瓦夫王子（亦即后来的波兰国王[瓦迪斯瓦夫四世](../Page/瓦迪斯瓦夫四世.md "wikilink")；Władysław
IV
Waza）商议，最后决定由波兰军队暂时占领西里西亚部分地区，希望能将它们与波兰合并。战争期间，神圣罗马帝国饱受严重破坏，一些西里西亚公爵——尤其属于皮亚斯特王朝的——和弗罗茨瓦夫主教[奥地利的查理](../Page/奥地利的查理.md "wikilink")，都希望得到波兰-立陶宛联邦的保护，避免被战争波及。奥地利的查理向上级波兰[格涅兹诺](../Page/格涅兹诺.md "wikilink")（Gniezno）教区求助，希望它与西格蒙德三世商议保护主教。1619年5月，瓦迪斯瓦夫王子应叔父查理的邀请，离开[华沙](../Page/华沙.md "wikilink")，前往西里西亚。

7月，捷克新教徒起义对抗[斐迪南二世](../Page/斐迪南二世_\(神圣罗马帝国\).md "wikilink")，并请求[选帝侯](../Page/选帝侯.md "wikilink")[腓特烈五世担任波希米亚国王](../Page/腓特烈五世_\(普法尔茨\).md "wikilink")。9月27日，瓦迪斯瓦夫王子和其叔父可能得知消息，马上离开西里西亚，在10月7返回华沙。12月，查理选择王子年幼弟弟[卡洛尔·斐迪南·瓦萨](../Page/卡洛尔·斐迪南·瓦萨.md "wikilink")（奥波莱公爵）作为弗罗茨瓦夫代理主教，并获得波兰主教团接受。1620年，原先独立的弗罗茨瓦夫教区重归格涅兹诺教区管理。不过，直到1632年前，前者实际上由西格蒙德三世控制，而非大主教或主教。1625年，卡洛尔·斐迪南·瓦萨成为弗罗茨瓦夫的主教。

但是，随着奥地利的形势转好，斐迪南二世拒绝割让西里西亚，也不再参与对奥斯曼帝国的战争。这是因为波兰支持奥地利，而其实奥斯曼的哈布斯堡特务也支持他的决定。

斐迪南二世提议[瓦迪斯瓦夫四世迎娶他的幼女](../Page/瓦迪斯瓦夫四世.md "wikilink")[塞西莉亚·雷纳塔公主](../Page/塞西莉亚·雷纳塔.md "wikilink")
(Cecilia
Renata；后来[斐迪南三世的妹妹](../Page/斐迪南三世_\(神圣罗马帝国\).md "wikilink"))，消息于1636年春传到波兰政府。1636年10月26日，派遣亲信[嘉布遣兄弟会](../Page/嘉布遣兄弟会.md "wikilink")（Capuchin
religious
order）瓦勒里安（Walerian）神父到[雷根斯堡与帝国谈判](../Page/雷根斯堡.md "wikilink")。公主的[嫁妆定为十万](../Page/嫁妆.md "wikilink")[兹罗提](../Page/波兰兹罗提.md "wikilink")（Złoty；波兰货币）。斐迪南二世也承诺为前波兰国王西格蒙德三世的两位妻子付出嫁妆，亦即[安娜](../Page/安娜_\(哈布斯堡\).md "wikilink")（Anna
of Habsburg）和[康斯坦泽](../Page/康斯坦泽_\(哈布斯堡\).md "wikilink")（Constance of
Habsburg）。而且，瓦迪斯瓦夫四世如若与公主结婚，他们将来的儿子会获得位于西里西亚的[奥波莱-拉齐布日公国](../Page/奥波莱-拉齐布日公国.md "wikilink")（Księstwo
Opolsko-Raciborskie）。但在成事之前，斐迪南二世在1637年2月去世。继任的斐迪南三世拒绝让瓦迪斯瓦夫四世儿子取得该公国，改为以[波希米亚封地](../Page/波希米亚.md "wikilink")[特热邦](../Page/特热邦.md "wikilink")（Třeboň）为嫁妆。二人在9月结婚。

1638年，瓦迪斯瓦夫四世宣布，父亲两位妻子那仍未偿还的嫁妆由一个西里西亚公国保管，而且他偏好奥波莱-拉齐布日公国。1642年，他提议把瑞典王位转送给哈布斯堡王朝，希望得到西里西亚。1644年夏，波兰派遣Ludovico
Fantoni到奥地利，建议以特热邦封地所得收入换取奥波莱-拉齐布日公国和[切申公国](../Page/切申公国.md "wikilink")。

1645年初，瓦迪斯瓦夫四世不想维也纳政府继续拖延，于是向神圣罗马帝国驻华沙使者[马克西米利安·迪特里希斯泰因](../Page/马克西米利安·迪特里希斯泰因.md "wikilink")（Maximilian
Dietrichstein）表示，波兰会与瑞典合作。这暗示波兰会与瑞典联手夺取西里西亚，故此威胁奥地利。1645年3月6日，瑞典陆军元帅[连纳尔特·托尔斯滕松](../Page/连纳尔特·托尔斯滕松.md "wikilink")（Lennart
Torstensson）在[扬科夫战役](../Page/扬科夫战役.md "wikilink")（Battle of
Jankov）击败神圣罗马帝国的巴伐利亚与萨克森联军，并进军维也纳。斐迪南三世于是愿意谈判，并在4月派遣[约翰内斯·普兹·冯·阿德勒尔图姆](../Page/约翰内斯·普兹·冯·阿德勒尔图姆.md "wikilink")（Johannes
Putz von Adlertum）到华沙，给瓦迪斯瓦夫四世一些特权，把奥波莱-拉齐布日公国权益赋予其子西格蒙德·卡兹米耶日（Zygmunt
Kazimierz），作为世袭[采邑](../Page/采邑.md "wikilink")。

最后，哈布斯堡在谈判取胜。该公国并不作为世袭采邑，只是外借五十年，而且其主人要向波希米亚国王称臣，故此波兰国王不能接受采邑。但是，瓦迪斯瓦夫四世在儿子成人前仍会统治该公国。另外，他也答应借钱给斐迪南三世，数额为一百万兹沃塔，减去上述仍未付还的三项嫁妆。

1648年，三十年战争结束，哈布斯堡王朝大力提倡天主教，并令西里西亚六成人口改信[天主教](../Page/天主教.md "wikilink")。1675年，皮亚斯特王朝的西里西亚支系灭亡，[哈布斯堡皇帝不顧](../Page/哈布斯堡.md "wikilink")[勃兰登堡选帝侯](../Page/勃兰登堡选帝侯.md "wikilink")[腓特烈·威廉](../Page/腓特烈·威廉_\(布蘭登堡\).md "wikilink")（大選侯）的抗議，自行併吞了絕嗣的[布热格公國](../Page/布热格.md "wikilink")（或稱西里西亞公國）。

### 普鲁士王国

1740年，[腓特烈大帝夺得西里西亚](../Page/腓特烈大帝.md "wikilink")，受到当地居民的欢迎——不单是新教人口和德裔人口。根据[布热格条约](../Page/布热格条约.md "wikilink")（见[宗教改革](../Page/西里西亞#宗教改革.md "wikilink")），他以布热格主权为借口发动[奥地利王位继承战争](../Page/奥地利王位继承战争.md "wikilink")。战争在1748年结束。普鲁士夺得西里西亚绝大部分地区，除了其东南方的[切申公国和](../Page/切申公国.md "wikilink")[奥帕瓦公国](../Page/奥帕瓦公国.md "wikilink")（Duchy
of
Opava）仍然属于奥地利。[七年战争中](../Page/七年战争.md "wikilink")，普鲁士再度击败奥地利，巩固对西里西亚的统治。西里西亚此后成为对普鲁士最忠诚的省份之一。1815年，原本属于[萨克森](../Page/萨克森.md "wikilink")，[格尔利茨附近的地区在](../Page/格尔利茨.md "wikilink")[拿破仑战争后并入西里西亚](../Page/拿破仑战争.md "wikilink")。这时，[德语已经成为](../Page/德语.md "wikilink")[下西里西亚最主要语言](../Page/下西里西亚.md "wikilink")；郊区的绝大多数居民，则常用受德语影响的[波兰语和](../Page/波兰语.md "wikilink")[捷克语](../Page/捷克语.md "wikilink")。在多数西里西亚的城市，德语是最常用的语言。

### 德奥统治

[Schlesien_1905.png](https://zh.wikipedia.org/wiki/File:Schlesien_1905.png "fig:Schlesien_1905.png")
1871年，[德意志帝国成立](../Page/德意志帝国.md "wikilink")，德国统一。普鲁士拥有的西里西亚成为德国的[省份](../Page/西里西亚省.md "wikilink")。上西里西亚经历大规模[工业化](../Page/工业化.md "wikilink")，吸引不少居民到当地居住。[下西里西亚的人口大多数是德国人和](../Page/下西里西亚.md "wikilink")[信义宗教徒](../Page/信义宗.md "wikilink")，也包括[弗罗茨瓦夫](../Page/弗罗茨瓦夫.md "wikilink")——那个自中世纪后期称为[布雷斯劳](../Page/布雷斯劳.md "wikilink")（Breslau）的城市。不过，在一些地区，例如[奥波莱行政区](../Page/奥波莱.md "wikilink")（Landkreis；后来是奥波伦政府区Regierungsbezirk
Oppeln）和上西里西亚的郊区，很多居民甚至大多数人口都是斯拉夫裔和天主教徒。整体而言，西里西亚人口有三成是波兰人，而且他们多数居住在[卡托维兹附近](../Page/卡托维兹.md "wikilink")，亦即上西里西亚东南部。[德国总理](../Page/德国总理.md "wikilink")[奥托·冯·俾斯麦发动](../Page/奥托·冯·俾斯麦.md "wikilink")[文化斗争](../Page/文化斗争.md "wikilink")（Kulturkampf），压制天主教在德国的发展，同时又引起波兰人的反抗。1884年，Hovevei
Zion团体在卡托维兹召开首轮会议，开启[锡安主义的先声](../Page/锡安主义.md "wikilink")。

这时，奥地利西里西亚的[奥斯特拉瓦和](../Page/奥斯特拉瓦.md "wikilink")[卡尔维纳](../Page/卡尔维纳.md "wikilink")（Karviná）都开始发展工业。这些地区的波兰人却大多是信义宗教徒，相对于拥有大多数德裔、信奉天主教人口的[奥匈帝国](../Page/奥匈帝国.md "wikilink")。

德国与奥匈帝国在[第一次世界大战战败](../Page/第一次世界大战.md "wikilink")。根据[凡尔赛条约](../Page/凡尔赛条约.md "wikilink")，[上西里西亚应举行公投](../Page/上西里西亚.md "wikilink")，决定该地的未来，但不包括333平方公里的[赫鲁钦地区](../Page/赫鲁钦.md "wikilink")（Hlučín；Hultschiner
Ländchen）——它直接由[捷克斯洛伐克接管](../Page/捷克斯洛伐克.md "wikilink")，尽管当地人口多数是德国人。[国际联盟最终在](../Page/国际联盟.md "wikilink")1921年举行公投。

在切申西里西亚，*Rada Narodowa Księstwa Cieszyńskiego*和*Národním Výborem pro
Slezsko*计划将[切申公国按照种族划分界线](../Page/切申公国.md "wikilink")，不过被[捷克斯洛伐克政府否定](../Page/捷克斯洛伐克.md "wikilink")。1919年1月23日，捷克斯洛伐克攻击切申西里西亚，不过在1月30日于[斯科茨佐夫](../Page/斯科茨佐夫.md "wikilink")（Skoczów）附近的[维斯瓦河撤军](../Page/维斯瓦河.md "wikilink")。国际联盟原本计划为切申西里西亚举行公投，但没有实行。1920年7月28日，凡尔赛条约公使议会决定把它分割。该分界组成了今天[波兰和](../Page/波兰.md "wikilink")[捷克共和国部分边界](../Page/捷克共和国.md "wikilink")。

### 两次大战之间

公投后，波兰民族主义者发动了三次[西里西亚起义](../Page/西里西亚起义.md "wikilink")。

  - 第一次：1919年8月16日-8月26日
  - 第二次：1920年8月19日-8月25日
  - 第三次：1921年5月2日-7月5日

结果，国际联盟决定再细分西里西亚。在上西里西亚的最东部，当地大多数居民投票支持德国统治，但仍然成为波兰[西里西亚省的自治区](../Page/西里西亚省.md "wikilink")（*Wojewodztwo
Śląskie*）。[沃伊切赫·科尔凡蒂](../Page/沃伊切赫·科尔凡蒂.md "wikilink")（Wojciech
Korfanty）是其中一位导致这些影响的政治人物。

仍属于德国的西里西亚，重新划分为[上西里西亚省和](../Page/上西里西亚省.md "wikilink")[下西里西亚省](../Page/下西里西亚省.md "wikilink")。在1938年的[水晶之夜](../Page/水晶之夜.md "wikilink")（Kristallnacht），布雷斯劳和其他西里西亚城市的[犹太会堂](../Page/犹太会堂.md "wikilink")（Synagogue）遭到破坏。10月，波兰按[慕尼黑协定占领捷克斯洛伐克的](../Page/慕尼黑协定.md "wikilink")[切申西里西亚](../Page/切申西里西亚.md "wikilink")，亦即[奥尔谢河](../Page/奥尔谢河.md "wikilink")（Olza）西部受争议的地区，称为[扎奥尔齐耶](../Page/扎奥尔齐耶.md "wikilink")（Zaozlie）。该地面积为906平方公里，有258,000名居民。

### 二次大战

1939年9月，[纳粹德国发动](../Page/纳粹德国.md "wikilink")[波兰战役](../Page/波兰战役.md "wikilink")，占领上述的西里西亚地区，并引起了[二次大战](../Page/二次大战.md "wikilink")。当地居民欢迎[德国防卫军](../Page/德国防卫军.md "wikilink")。1940年，德国在上西里西亚建造了著名的[奥斯维辛集中营](../Page/奥斯维辛集中营.md "wikilink")，是实行[屠杀犹太人的主要场地](../Page/犹太人大屠杀.md "wikilink")；同年，在下西里西亚建造了[格罗斯-罗森集中营](../Page/格罗斯-罗森集中营.md "wikilink")（Gross-Rosen
concentration camp），后来更在附近城市建造集中营分部。

### 二战后的西里西亚

1945年，[苏联](../Page/苏联.md "wikilink")[红军占领西里西亚全境](../Page/苏联红军.md "wikilink")。很多德国居民都逃离西里西亚，希望能逃避苏军的攻击。德国投降后，很多居民都返回家园。然而，根据1944年[雅尔塔会议和](../Page/雅尔塔会议.md "wikilink")1945年的[波茨坦协定](../Page/波茨坦协定.md "wikilink")，[奥德河和](../Page/奥德河.md "wikilink")[尼斯河](../Page/尼斯河.md "wikilink")（波兰语：Nysa
Łużycka）以东的德属西里西亚都归于波兰（详见[奥德河-尼斯河线](../Page/奥德河-尼斯河线.md "wikilink")）。战前，西里西亚的德裔居民有四百万人；战后，绝大部分仍留在当地的都被苏军[驱逐](../Page/驱逐德国人事件.md "wikilink")。

战后，西里西亚的工业重建，很多波兰人迁居于此，当中很多人来自被苏联占领的波兰领土。今天，波兰近两成人口居住在西里西亚，但很多家庭都不是[原居民](../Page/西里西亚人.md "wikilink")。但西里西亞地區仍深受長久的德國文化影響，從語言服飾、歷史建物、基礎建設到教育體制，都有很深的德國文化刻鑿。

少数说德语的德裔居民仍居住在[奥波莱附近](../Page/奥波莱.md "wikilink")。在上西里西亚，也有一些说斯拉夫语言或双语皆懂的居民。

## 自然资源

西里西亚是一个资源丰富、人口众多的地区。它盛产[煤](../Page/煤.md "wikilink")、[铁资源](../Page/铁.md "wikilink")，[制造业发达](../Page/制造业.md "wikilink")。在共产党统治期间，很多老旧的工业设施导致了[环境污染](../Page/环境污染.md "wikilink")。当地[农业正进步中](../Page/农业.md "wikilink")，主要的[农产品有](../Page/农产品.md "wikilink")：[谷物](../Page/谷物.md "wikilink")、[土豆和](../Page/土豆.md "wikilink")[甜菜](../Page/甜菜.md "wikilink")。


## 人口

现代西里西亚居民多数是[波兰人和](../Page/波兰人.md "wikilink")[西里西亚人](../Page/西里西亚人.md "wikilink")；[日耳曼](../Page/德国人.md "wikilink")、[捷克](../Page/捷克人.md "wikilink")、[摩拉维亚则是](../Page/摩拉维亚人.md "wikilink")[少数民族](../Page/少数民族.md "wikilink")。根据2002年波兰全国最新[人口普查](../Page/人口普查.md "wikilink")：斯拉夫西里西亚人是波兰第一大少数民族，[德国人居第二位](../Page/德国人.md "wikilink")——他们多数都在西里西亚聚居。捷克西里西亚的主要居民为捷克人、摩拉维亚人和波兰人。

[第二次世界大战前](../Page/第二次世界大战.md "wikilink")，西里西亚的主要居民是德国人，波兰人和捷克人。1905年人口普查显示，约百分之七十五的居民是德国人，百分之二十五是波兰人。二次大战结束后，大部分德裔居民逃离故土，或被苏联红军驱逐，或是移民外国。大部分西里西亚人今天居住在[德国](../Page/德国.md "wikilink")；很多人在[鲁尔区做矿工](../Page/鲁尔区.md "wikilink")，正是祖先在西里西亚的旧业。1945年后，[西德政府为了令他们更快融入当地社会](../Page/西德.md "wikilink")，成立并资助一些组织，例如[西里西亚领土协会](../Page/西里西亚领土协会.md "wikilink")（Landsmannschaft
Schlesien）。该协会一位著名但又常引发争议的[发言人](../Page/发言人.md "wikilink")，就是[德国基督教民主联盟成员](../Page/德国基督教民主联盟.md "wikilink")[赫尔伯特·胡普卡](../Page/赫尔伯特·胡普卡.md "wikilink")（Herbert
Hupka）。主流德国民意认为，这些组织会与波兰西里西亚人建立和谐关系，这正在逐步实现。

## 主要城市

以下城市分别在[波兰](../Page/波兰.md "wikilink")、[德国与](../Page/德国.md "wikilink")[捷克](../Page/捷克.md "wikilink")。

  - [布熱格](../Page/布熱格.md "wikilink")（Brzeg）
  - [比托姆](../Page/比托姆.md "wikilink")（Bytom）
  - [格利維采](../Page/格利維采.md "wikilink")（Gliwice）
  - [格沃古夫](../Page/格沃古夫.md "wikilink")（Głogów）
  - [格尔利茨](../Page/格尔利茨.md "wikilink")（Görlitz）
  - [耶萊尼亞古拉](../Page/耶萊尼亞古拉.md "wikilink")（Jelenia Góra）
  - [卡托维兹](../Page/卡托维兹.md "wikilink")（Katowice）
  - [克沃茲科](../Page/克沃茲科.md "wikilink")（Kłodzko）
  - [萊格尼察](../Page/萊格尼察.md "wikilink")（Legnica）
  - [尼斯](../Page/尼斯_\(波蘭\).md "wikilink")（Nysa）
  - [奧帕瓦](../Page/奧帕瓦.md "wikilink")（Opava）
  - [奥波莱](../Page/奥波莱.md "wikilink")（Opole）
  - [俄斯特拉发](../Page/俄斯特拉发.md "wikilink")（Ostrava）
  - [拉齐布日](../Page/拉齐布日.md "wikilink")（Racibórz）
  - [雷布尼克](../Page/雷布尼克.md "wikilink")（Rybnik）
  - [瓦烏布日赫](../Page/瓦烏布日赫.md "wikilink")（Wałbrzych）
  - [弗罗茨瓦夫](../Page/弗罗茨瓦夫.md "wikilink")（Wrocław）
  - [茲格热莱茨](../Page/茲格热莱茨.md "wikilink")（Zgorzelec）
  - [綠山城](../Page/綠山城.md "wikilink")（Zielona Góra）
  - [沙加尼](../Page/沙加尼.md "wikilink")（Żagań）

## 参看

  - [德国历史](../Page/德国历史.md "wikilink")

  - [西里西亚名人列表](../Page/西里西亚名人列表.md "wikilink")

  - [西里西亚起义](../Page/西里西亚起义.md "wikilink")

  -
## 参考

  - *Documents on British Foreign Policy 1919-1939*, 1st Series, volume
    XI, *Upper Silesia, Poland, and the Baltic States, January
    1920-March 1921*, edited by Rohan Butler, MA, J.P.T.Bury, MA, &
    M.E.Lambert, MA, Her Majesty's Stationary Office
    ([HMSO](../Page/HMSO.md "wikilink")), London, 1961 (amended edition
    1974), ISBN 0-11-591511-7\*

<!-- end list -->

  - *Documents on British Foreign Policy 1919-1939*, 1st Series, volume
    XVI, *Upper Silesia, March 1921 - November 1922*, edited by
    W.N.Medlicott, MA, D.Lit., Douglas Dakin, MA, PhD, & M.E.Lambert,
    MA, [HMSO](../Page/HMSO.md "wikilink"), London, 1968.

<!-- end list -->

  - *Microcosm, Portrait of a Central European City*, by
    [诺曼·戴维斯](../Page/诺曼·戴维斯.md "wikilink")（Norman Davies）and
    [Roger Moorhouse](../Page/Roger_Moorhouse.md "wikilink")（[Jonathan
    Cape](../Page/Jonathan_Cape.md "wikilink"), 2002）ISBN 0-224-06243-3

## 外部链接

  - [2000年西里西亚地图](http://www.tr62.de/maps/Silesia-2000.html)
  - [1763年西里西亚地图](http://www.hoeckmann.de/germany/silesia.htm)
  - [1](http://www.korfu-4u.de/Korfu)

[\*](../Category/西里西亞.md "wikilink")
[Category:波蘭地區](../Category/波蘭地區.md "wikilink")
[Category:捷克地区](../Category/捷克地区.md "wikilink")
[Category:德國地區](../Category/德國地區.md "wikilink")
[Category:欧洲分裂地区](../Category/欧洲分裂地区.md "wikilink")
[Category:神聖羅馬帝國諸侯國](../Category/神聖羅馬帝國諸侯國.md "wikilink")