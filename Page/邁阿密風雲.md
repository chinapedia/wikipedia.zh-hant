《**邁阿密風暴**》（），美國1984年播映的[犯罪](../Page/犯罪片.md "wikilink")、[動作電視影集](../Page/動作片.md "wikilink")。

[刑警由美國演員](../Page/刑警.md "wikilink")[唐·強生及黑人男星](../Page/唐·強生.md "wikilink")[菲利浦·麥可·湯瑪斯及](../Page/菲利浦·麥可·湯瑪斯.md "wikilink")[西班牙裔](../Page/西班牙.md "wikilink")、沉默卻兇猛冷靜刑事組長（[愛德華·詹姆斯·奧莫斯飾演](../Page/愛德華·詹姆斯·奧莫斯.md "wikilink")）領導他們，三大族群代表共同演出；劇情講述美國[佛羅里達州海邊大都市](../Page/佛羅里達州.md "wikilink")[邁阿密刑警苦戰龐大國際黑幫犯罪故事](../Page/邁阿密.md "wikilink")；全片寫實描繪現代黑幫高科技電子犯罪手法絲絲入扣，邁阿密風光明媚，海灘[椰子樹邊辣妹嬉笑](../Page/椰子.md "wikilink")、噴速[汽艇海上跳飛追逐戰](../Page/汽艇.md "wikilink")、[動物園](../Page/動物園.md "wikilink")[紅鶴群](../Page/紅鶴.md "wikilink")、金錢[洗錢](../Page/洗錢.md "wikilink")，以及[西班牙裔美國](../Page/西班牙.md "wikilink")[重工業都市黑幫的兇猛剽悍](../Page/重工業.md "wikilink")。

在[黑幫電影裡](../Page/黑幫電影.md "wikilink")，演、導、劇本俱優，是部當年影響同[電影類型極為成功的經典代表佳作](../Page/電影類型.md "wikilink")；因此大導演[麥可·曼執導](../Page/麥可·曼.md "wikilink")[改編電影作品](../Page/迈阿密风暴_\(电影\).md "wikilink")，於2006年7月28日上映，惟一般影評仍以原先電視影集為佳讚。

## 電視

  - 影集

英文原名《邁阿密重案組》，台譯《邁阿密風雲》及《邁阿密天龍》，港譯《神探勇闖罪惡城》

  -
    [台灣最初於](../Page/台灣.md "wikilink")1985年10月1號在[台視的午夜時段播出](../Page/台視.md "wikilink")，而後1986年播出第二季時改在當時週六最熱門的影集時段播出；全輯以英語原音播送；片中以粉彩色佈景為主調配合邁阿密的明媚風光，穿著灑脫的巡警加上法拉利跑車，流行歌曲配樂，特殊角度取鏡以及引人的劇情結構；此片對整個成長在80年代的MTV世代形成了極大的流行文化與影響；並於全世界無數次重播

2000年以[國語重播](../Page/國語.md "wikilink")。

  -
    影集裡出現數支動聽流行歌曲，搭配烘托劇情起伏，如：《[加勒比海皇后](../Page/加勒比海皇后.md "wikilink")》、《》...

## 獎項

## 外部連結

  - [台灣電視資料庫－邁阿密風雲](http://tv.nccu.edu.tw/radioProgram_preview.htm?PROGRAMNAME=%C1%DA%AA%FC%B1K%AD%B7%B6%B3&STATION=&YEAR=1964&MONTH=1&DAY=1&YEAR2=2002&MONTH2=12&DAY2=31&CATALOG=&COUNTRY=)
  - [台灣電視資料庫－邁阿密天龍](http://tv.nccu.edu.tw/radioProgram_preview.htm?PROGRAMNAME=%C1%DA%AA%FC%B1K%A4%D1%C0s&STATION=&YEAR=1964&MONTH=1&DAY=1&YEAR2=2002&MONTH2=12&DAY2=31&CATALOG=&COUNTRY=)
  - [80年代復古影集《邁阿密風雲》：記得小時候，那部看起來很沉重的警匪劇。](https://www.gq.com.tw/blog/demo_content.asp?ids=3745&checkcode=709ae0a0e6ffdc505b444f16110f509a)

## 注释

<references />

[Category:1984年電視](../Category/1984年電視.md "wikilink")
[Category:美國電視劇](../Category/美國電視劇.md "wikilink")
[Category:動作電視劇](../Category/動作電視劇.md "wikilink")
[Category:警匪電視劇](../Category/警匪電視劇.md "wikilink")
[Category:NBC電視節目](../Category/NBC電視節目.md "wikilink")
[Category:佛羅里達州影視](../Category/佛羅里達州影視.md "wikilink")