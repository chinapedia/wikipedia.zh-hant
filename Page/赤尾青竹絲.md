**赤尾青竹絲**（[學名](../Page/學名.md "wikilink")：*Trimeresurus*
*stejnegeri*）又名**福建竹葉青蛇**、**中國竹葉青蛇**，是一種分佈於[印度](../Page/印度.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[臺灣的](../Page/臺灣.md "wikilink")[蝮亞科](../Page/蝮亞科.md "wikilink")[竹葉青屬](../Page/竹葉青屬.md "wikilink")[毒蛇](../Page/毒蛇.md "wikilink")，有三個亞種，為台灣六大毒蛇之一。\[1\]。
[赤尾呆.JPG](https://zh.wikipedia.org/wiki/File:赤尾呆.JPG "fig:赤尾呆.JPG")

## 描述

中小型蛇類，最大體長約90公分，體型瘦長，頭部呈三角形，頸部細長，眼睛為紅色，眼睛和鼻孔間有可感熱的頰窩。身體背部為翠綠色或深綠色，腹側有一條白色細縱線，雄性在白線下還有一條紅縱線，腹部為淡黃綠色，尾部為磚紅色。

## 習性

白天夜晚均會活動，但以夜間活動較為頻繁。通常以蛙類、蜥蜴、鳥類、老鼠及鼩鼱為食，其中又以蛙類為主。以卵胎生方式繁殖，一般於秋季交配，隔年的夏季生產。每窩可產2\~15條幼蛇，初生幼蛇長約26公分，一年半左右即可達到性成熟\[2\]。

雖然毒性較弱，但因為體色與植物相近，是台灣咬人紀錄最多的蛇類。毒液中含有出血性毒素，被咬的傷口會腫痛、發癢、瘀血或起水泡。\[3\]

## 棲地

棲地形態極為廣泛，由低海拔次生林到2,000多公尺山區的各類型環境皆能發現到牠的蹤跡。\[4\]

## 參考文獻

## 進階閱讀

<div class="references-small">

  - Creer, S.; Malhotra, A.; Thorpe, R.S.; Chou, W.H. 2001 Multiple
    causation of phylogeographical pattern as revealed by nested clade
    analysis of the bamboo viper (*Trimeresurus stejnegeri*) within
    Taiwan. Molecular Ecology 10(8):1967-1981
  - Malhotra, Anita & Roger S. Thorpe 2004 Maximizing information in
    systematic revisions: a combined molecular and morphological
    analysis of a cryptic green Pit Viper complex (*Trimeresurus
    stejnegeri*).Biological Journal of the Linnean Society 82 (2): 219
  - Parkinson,C.L. 1999 Molecular systematics and biogeographical
    history of Pit Vipers as determined by mitochondrial ribosomal DNA
    sequences. Copeia 1999 (3): 576-586
  - Peng, G. & Fuji, Z. 2001 Comparative studies on hemipenes of four
    species of *Trimeresurus* (sensu stricto) (Serpentes: Crotalinae).
    Amphibia-Reptilia 22 (1): 113-117
  - Tu, M.-C. et al. 2000 Phylogeny, Taxonomy, and Biogeography of the
    Oriental Pit Vipers of the Genus *Trimeresurus* (Reptilia:
    ViperidaCrotalinae): A Molecular Perspective. ZOOLOGICAL SCIENCE 17:
    1147-1157

</div>

[Category:蝮亞科](../Category/蝮亞科.md "wikilink")
[Category:中國爬行動物](../Category/中國爬行動物.md "wikilink")
[Category:印度爬行動物](../Category/印度爬行動物.md "wikilink")
[中](../Category/竹叶青蛇属.md "wikilink")

1.

2.
3.
4.