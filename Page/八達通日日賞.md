[HK_Wellcome_Octopus_Reward_Station.JPG](https://zh.wikipedia.org/wiki/File:HK_Wellcome_Octopus_Reward_Station.JPG "fig:HK_Wellcome_Octopus_Reward_Station.JPG")
**八達通日日賞**是[八達通推出的一個計劃](../Page/八達通.md "wikilink")，目的是收集[香港](../Page/香港.md "wikilink")[消費者的](../Page/消費者_\(經濟\).md "wikilink")[消費行為的](../Page/消費者行為.md "wikilink")[數據](../Page/數據.md "wikilink")，經過[資料挖掘](../Page/資料挖掘.md "wikilink")（Data
Mining）分析，可以作為未來[產品](../Page/產品.md "wikilink")、[服務推廣等用途](../Page/服務.md "wikilink")。而營運公司為八達通獎賞有限公司，是八達通卡有限公司的全資附屬公司。

日日賞始於2005年11月，已經登記的八達通卡，只要在指定商店購物付款時，拍卡便可以獲得積分，記錄在其卡內，作為獎勵。這亦是八達通推出以來，八達通公司本身推出第一項積分計劃。

## 運作模式

一般而言，參與者於不同指定商店購物時，每消費$1，可獲$1日日賞$。日日賞$不可兌換現金，只可以在指定商店代替現金用作購物之用，而現時每$1日日賞$可代表不少於$1的港幣價值。在個別商店，有時會推出若干倍數的日日賞$的優惠。

參與者除了可以透過已登記的[八達通於指定的商戶消費以賺取日日賞積分外](../Page/八達通.md "wikilink")，八達通公司亦會不定期電郵問卷調查予參與者，參與者只要回覆有關的問卷調查，即有機會獲得日日賞積分，成功獲得積分的參與者將會獲電郵通知，並需於指定時間內到日日賞積分機中下載所獲得的積分。

但亦有人對八達通公司的資料挖掘行為，對私隱的侵犯感到憂慮。亦有個別商戶，因無替顧客存入積分，而私下把相當於惠顧可獲得的積分，以現金方式回饋（比如消費$1則回饋$1）。

必須注意的是，「**八達通·嶺南通**」聯名卡、「**嶺南通·八達通**」聯名卡及「**互通行**」卡(八達通及深圳通聯名卡)於推出初期不能登記是項服務，直至2013年1月31日起，以上跨境聯名卡方開通是項服務\[1\]。

## 私隱資料事件

2010年7月，八達通公司被揭發自2002年起將「日日賞計劃」會員、個人八達通卡主及自動增值服務使用者的[個人資料](../Page/個人資料.md "wikilink")，售予六間合作商戶作產品及服務推廣之用，並從中獲利4400萬。事件引起社會及傳媒廣泛關注，[私隱專員公署也對此事件展開調查及公開聆訊](../Page/私隱專員公署.md "wikilink")。八達通[行政總裁](../Page/行政總裁.md "wikilink")[陳碧鏵在聆訊前](../Page/陳碧鏵.md "wikilink")，仍然表示沒有出售個人資料，在聆訊時才終於向公眾承認事件。\[2\]

## 參與商戶

  - 超級市場：[惠康超級市場](../Page/惠康.md "wikilink")（包括旗下的[Market Place by
    Jasons及](../Page/Market_Place_by_Jasons.md "wikilink") Jasons -
    Food & Living ）
  - 麵包西餅店：[東海堂](../Page/東海堂.md "wikilink")
  - 童裝店：[CHICKEEDUCK](../Page/CHICKEEDUCK.md "wikilink")、[Sanrio
    Kids](../Page/Sanrio_Kids.md "wikilink")
  - 銀行：[香港上海滙豐銀行](../Page/香港上海滙豐銀行.md "wikilink")（HSBC）、[中國建設銀行(亞洲)](../Page/中國建設銀行\(亞洲\).md "wikilink")
  - 飲品：[可口可樂公司](../Page/可口可樂公司.md "wikilink") (飲品膠樽回收計劃)
  - 商場：[德福廣場](../Page/德福廣場.md "wikilink")、[青衣城](../Page/青衣城.md "wikilink")、[杏花新城及](../Page/杏花新城.md "wikilink")[綠楊坊](../Page/綠楊坊.md "wikilink")
  - 寵物店：[五方寵物有限公司](../Page/五方寵物有限公司.md "wikilink")（包括旗下的[Q-PETs](../Page/Q-PETs.md "wikilink")、[紅蘿蔔](../Page/紅蘿蔔.md "wikilink")、[寵物森林](../Page/寵物森林.md "wikilink")、[龍貓樂園](../Page/龍貓樂園.md "wikilink")、[寵物便利及](../Page/寵物便利.md "wikilink")[寵物易購網](../Page/寵物易購網.md "wikilink")）
  - 電器：[中原電器](../Page/中原電器.md "wikilink")
  - 旅遊：[星晨旅遊](../Page/星晨旅遊.md "wikilink")

### 曾參與商戶

  - 個人護理商店：[屈臣氏](../Page/屈臣氏.md "wikilink")，於2007年12月17日與八達通日日賞結束合作伙伴關係
  - 港式快餐：[大快活](../Page/大快活.md "wikilink")，於2009年10月1日與八達通日日賞結束合作伙伴關係
  - 相片沖印店：[快圖美](../Page/快圖美.md "wikilink")，於2009年12月1日與八達通日日賞結束合作伙伴關係
  - 法式快餐：[Délifrance](../Page/Délifrance.md "wikilink")，於2010年4月1日與八達通日日賞結束合作伙伴關係
  - 電視：[Now](../Page/Now寬頻電視.md "wikilink")[101台遊戲節目](../Page/Now101台.md "wikilink")[撳錢](../Page/撳錢.md "wikilink")
    （2011年4月12日至8月31日）及 [撚價](../Page/撚價.md "wikilink")
  - 人壽保險：[信諾環球人壽保險有限公司](../Page/信諾環球人壽保險有限公司.md "wikilink")
  - 商場：[君薈坊](../Page/君薈坊.md "wikilink")
  - 美式快餐：[麥當勞](../Page/麥當勞.md "wikilink")，於2013年8月26日與八達通日日賞結束合作伙伴關係
  - 電訊：[one2free](../Page/one2free.md "wikilink")
  - 電影院：[娛藝戲院](../Page/娛藝戲院.md "wikilink")（UA
    CINEMAS），於2017年6月19日與八達通日日賞結束合作伙伴關係

## 相關

  - 其他積分優惠卡—[易賞錢](../Page/易賞錢.md "wikilink")
  - [收銀機](../Page/收銀機.md "wikilink")

## 外部参考

  - [八達通日日賞](https://web.archive.org/web/20070517032604/http://www.octopusrewards.com.hk/what/tc/index.jsp)
  - [東方日報：八達通出售客戶個人資料圖利事件發展](http://orientaldaily.on.cc/cnt/news/20100731/00176_006.html?pubdate=20100731)

<references/>

[Category:八達通](../Category/八達通.md "wikilink")
[Category:市場學](../Category/市場學.md "wikilink")

1.  [跨境八達通推出新增服務　為用戶帶來更多方便和優惠
    八達通卡有限公司(2013年1月30日)](http://www.octopus.com.hk/release/detail/2013/tc/20130130.html)

2.  [經濟日報：八達通講大話
    日日賞變日日賺](http://hk.news.yahoo.com/article/100726/23/jd4r.html)
    ，2010年7月27日。