**南達科他州**（）是[美國中西部平原上地勢較高的一](../Page/美國.md "wikilink")[州](../Page/州.md "wikilink")，過去曾是美國[印地安人](../Page/印地安人.md "wikilink")[蘇族中](../Page/蘇族.md "wikilink")[拉科他族](../Page/拉科他族.md "wikilink")（Lakota）的聚落所在。南達科他州在1889年11月2日加入[美國](../Page/美國.md "wikilink")[聯邦](../Page/聯邦.md "wikilink")，也是在同一天被命名的。

南達科他州的北邊是[北達科他州](../Page/北達科他州.md "wikilink")，南邊是[內布拉斯加州](../Page/內布拉斯加州.md "wikilink")，東邊則緊鄰[愛荷華州與](../Page/愛荷華州.md "wikilink")[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")，西邊是[懷俄明州和](../Page/懷俄明州.md "wikilink")[蒙大拿州](../Page/蒙大拿州.md "wikilink")，它同時也是美國上被稱為[邊疆帶六州中的一州](../Page/邊疆帶.md "wikilink")。

美國戰艦[南達科他號就是以此州命名的](../Page/南達科他號戰艦_\(BB-57\).md "wikilink")。

該州分為66個郡，請參看。

## 歷史

美國原住民生活在現在的北達科他比歐洲移民早幾千年，他們的部落包括曼丹人（Mandan），達科他人和Yanktonai。

美國總統[本傑明·哈里森於](../Page/本傑明·哈里森.md "wikilink")1889年11月2日簽署了正式接納[北達科他州和南達科他州為聯盟](../Page/北達科他州.md "wikilink")，由於南北兩州沒有明確記錄加入順序，南達科他州因字母順序列為美國第40州。

1890年12月29日，在松脊印第安保護區發生大屠殺。
通常被稱為美國與拉科塔蘇族國家之間的最後一次主要武裝衝突，大屠殺導致至少146名蘇族人死亡，其中許多人是婦女和兒童。31名美國士兵也在衝突中喪生。

在20世紀初，氣候變乾燥和經濟衰退的衝擊下導致南達科他州的人口在1930年到1940年間下降7％以上。
第二次世界大戰後，隨金融業和旅遊業的發展，南達科他州從一個以農業為主的經濟轉變為一個多樣化的經濟發展。

“達科他”(Dakota)源自[印第安人](../Page/印第安人.md "wikilink")[苏族的語言](../Page/苏族.md "wikilink")，意思是「朋友」。

## 法律與政府

[PierreSD_Capitol.jpg](https://zh.wikipedia.org/wiki/File:PierreSD_Capitol.jpg "fig:PierreSD_Capitol.jpg")

## 地理

南達科他州位於美國中央平原的一部份，高度朝向洛磯山脈緩慢的升高。南部以密蘇里河分隔。密蘇里河以東，是冰河地形的遺址，有山丘和大約120個小湖。密蘇里河以西則是草原，邊緣地區有一些山脈。

## 旅遊與觀光資訊

### 國家公園

  - [羅斯摩爾山國家紀念公園又常稱呼為](../Page/羅斯摩爾山國家紀念公園.md "wikilink")「總統山國家公園」。

[South_Dakota_general_map_2.png](https://zh.wikipedia.org/wiki/File:South_Dakota_general_map_2.png "fig:South_Dakota_general_map_2.png")

[Sioux_Falls-waterfall.jpeg](https://zh.wikipedia.org/wiki/File:Sioux_Falls-waterfall.jpeg "fig:Sioux_Falls-waterfall.jpeg")

[Dean_Franklin_-_06.04.03_Mount_Rushmore_Monument_(by-sa).jpg](https://zh.wikipedia.org/wiki/File:Dean_Franklin_-_06.04.03_Mount_Rushmore_Monument_\(by-sa\).jpg "fig:Dean_Franklin_-_06.04.03_Mount_Rushmore_Monument_(by-sa).jpg")\]\]

## 經濟

以农业为主。
[SDethnl1.jpg](https://zh.wikipedia.org/wiki/File:SDethnl1.jpg "fig:SDethnl1.jpg")的[生質燃料廠](../Page/生質燃料.md "wikilink")\]\]

## 人口

2003年人口普查，南達科他州有764,309人。與1990年代的人口數量比較，成長率為8.5%。

目前南達科他州的種族比例可分為：

  - 88%白人
  - 8.3%[印地安原住民](../Page/印第安人.md "wikilink")
  - 1.4%[拉美裔人](../Page/拉美裔人.md "wikilink")
  - 0.6%非裔人
  - 0.6%亞裔人
  - 1.3%混合的種族

[Downtown_Sioux_Falls_61.jpg](https://zh.wikipedia.org/wiki/File:Downtown_Sioux_Falls_61.jpg "fig:Downtown_Sioux_Falls_61.jpg")人口16萬\]\]

南達科他州的前五大居民祖先為：

  - [德國人](../Page/德國.md "wikilink")（40.7%）
  - [挪威人](../Page/挪威.md "wikilink")（15.3%）
  - [愛爾蘭人](../Page/愛爾蘭.md "wikilink")（10.4%）
  - [印地安原住民](../Page/印第安人.md "wikilink")（8.3%）
  - [英國人](../Page/英國.md "wikilink")（7.1%）

此外，五歲以下的居民占有6.8%，18歲以下則有26.8%。至於65歲以上則是14.3%。南達科他州的女性略多於男性，百分比為50.4%。

在收入方面，南達科他州的平均家庭年收入為35,282美元，個人年收入為17,562美元；該州有13.2%的人口在屬於低收入戶（在美國[贫困线之下](../Page/贫困线.md "wikilink")）。

美国2014年人口估算显示，该州人口达到853,175人。\[1\]

[宗教上](../Page/宗教.md "wikilink")，南達科他州人口信仰比例為：

  - 67%新教徒
  - 25%[羅馬天主教徒](../Page/羅馬天主教.md "wikilink")
  - 2%其他基督教派的教徒
  - 1%其他宗教教徒
  - 4%無信仰者

南達科他州前三大新教宗派是：[浸禮會](../Page/浸禮會.md "wikilink")（43%）、[路德會](../Page/路德會.md "wikilink")（30%）、[衛理公會派](../Page/衛理公會派.md "wikilink")（11%）。

## 重要城鎮

  - 州首府：皮爾（Pierre）
  - 蘇族瀑布（Sioux Falls，蘇瀑）

## 教育

### 州立大學

  - [南達科塔大學](../Page/南達科塔大學.md "wikilink")
  - [南達科塔州立大學](../Page/南達科塔州立大學.md "wikilink")
  - [南達科達礦業及理工學院](../Page/南達科達礦業及理工學院.md "wikilink")（[南達科他礦業及理工學院](../Page/南達科他礦業及理工學院.md "wikilink")）
  - [黑丘州立大學](../Page/黑丘州立大學.md "wikilink")（）
  - [達科達州立大學](../Page/達科達州立大學.md "wikilink")（）
  - [北方州立大學](../Page/北方州立大學.md "wikilink")（）

### 私立大學

  - [奧古斯塔納學院](../Page/奧古斯塔納學院.md "wikilink")（）
  - [蘇族瀑布學院](../Page/蘇族瀑布學院.md "wikilink")（）
  - [達科達衛斯里昂大學](../Page/達科達衛斯里昂大學.md "wikilink")（）
  - [修倫學院](../Page/修倫學院.md "wikilink")（）
  - [瑪莉山學院](../Page/瑪莉山學院.md "wikilink")（）
  - [國家學院](../Page/國家學院.md "wikilink")（）
  - [表演學院](../Page/表演學院.md "wikilink")（）
  - [辛特格雷斯克學院](../Page/辛特格雷斯克學院.md "wikilink")（）

## 重要體育團體

### 美式足球

  - [NCAA](../Page/NCAA.md "wikilink")
      - University of South Dakota（Coyotes）

### 篮球

  - NCAA
      - University of South Dakota（Coyotes）

## 名人

  - [詹紐瑞·瓊斯](../Page/詹紐瑞·瓊斯.md "wikilink")（1978－）：演員
  - [欧内斯特·劳伦斯](../Page/欧内斯特·劳伦斯.md "wikilink")（1901－1958），1939年[诺贝尔物理学奖获得者](../Page/诺贝尔物理学奖.md "wikilink")

## 其他資訊

### 重要高速公路

  - [29號州際公路](../Page/29號州際公路.md "wikilink") I-29
  - [90號州際公路](../Page/90號州際公路.md "wikilink") I-90

## 參考文獻

## 外部連結

  - [官方站点](http://www.southdakota.us)（英文）
  - [南达科达大学（University of South Dakota）](http://www.usd.edu/)（英文）

[Category:美国州份](../Category/美国州份.md "wikilink")
[\*](../Category/南达科他州.md "wikilink")
[Category:美国中西部](../Category/美国中西部.md "wikilink")

1.