**旭町**可能是指：

## 自治体

  - 旭町 (千葉縣海上郡) -
    [千葉縣](../Page/千葉縣.md "wikilink")[海上郡に所在](../Page/海上郡.md "wikilink")。
    → **[旭市](../Page/旭市.md "wikilink")**
  - 旭町 (千葉縣夷隅郡) -
    [千葉縣](../Page/千葉縣.md "wikilink")[夷隅郡に所在](../Page/夷隅郡.md "wikilink")。現・[夷隅市](../Page/夷隅市.md "wikilink")。
    → **[長者町](../Page/長者町.md "wikilink")**
  - [旭町 (愛知縣東加茂郡)](../Page/旭町_\(愛知縣東加茂郡\).md "wikilink") -
    [愛知縣](../Page/愛知縣.md "wikilink")[東加茂郡に所在](../Page/東加茂郡.md "wikilink")。現・[丰田市](../Page/丰田市.md "wikilink")。
  - [旭町 (愛知縣知多郡)](../Page/旭町_\(愛知縣知多郡\).md "wikilink") -
    [愛知縣](../Page/愛知縣.md "wikilink")[知多郡に所在](../Page/知多郡.md "wikilink")。現・[知多市](../Page/知多市.md "wikilink")。
  - 旭町 (愛知縣東春日井郡) -
    [愛知縣](../Page/愛知縣.md "wikilink")[東春日井郡に所在](../Page/東春日井郡.md "wikilink")。
    → **[尾張旭市](../Page/尾張旭市.md "wikilink")**
  - [旭町 (島根縣)](../Page/旭町_\(島根縣\).md "wikilink") -
    [島根縣](../Page/島根縣.md "wikilink")[那賀郡に所在](../Page/那賀郡_\(島根縣\).md "wikilink")。現・[濱田市](../Page/濱田市.md "wikilink")。
  - [旭町 (岡山縣)](../Page/旭町_\(岡山縣\).md "wikilink") -
    [岡山縣](../Page/岡山縣.md "wikilink")[久米郡に所在](../Page/久米郡.md "wikilink")。現・[美咲町](../Page/美咲町.md "wikilink")。
  - 旭町 (愛媛縣) -
    [愛媛縣](../Page/愛媛縣.md "wikilink")[北宇和郡に所在](../Page/北宇和郡.md "wikilink")。現[鬼北町](../Page/鬼北町.md "wikilink")。
    → **[近永町](../Page/近永町.md "wikilink")**

## 地名

### 日本

  - [旭町 (札幌市)](../Page/旭町_\(札幌市\).md "wikilink") -
    [北海道](../Page/北海道.md "wikilink")[札幌市](../Page/札幌市.md "wikilink")[豐平区的地名](../Page/豐平区.md "wikilink")。
  - [旭町 (旭川市)](../Page/旭町_\(旭川市\).md "wikilink") -
    北海道[旭川市的地名](../Page/旭川市.md "wikilink")。
  - [旭町 (小樽市)](../Page/旭町_\(小樽市\).md "wikilink") -
    北海道[小樽市的地名](../Page/小樽市.md "wikilink")。
  - [旭町 (釧路市)](../Page/旭町_\(釧路市\).md "wikilink") -
    北海道[釧路市的地名](../Page/釧路市.md "wikilink")。
  - [旭町 (芦別市)](../Page/旭町_\(芦別市\).md "wikilink") -
    北海道[芦別市的地名](../Page/芦別市.md "wikilink")。
  - [旭町 (北海道伊達市)](../Page/旭町_\(北海道伊達市\).md "wikilink") -
    北海道[伊達市的地名](../Page/伊達市_\(北海道\).md "wikilink")。
  - [旭町 (苫小牧市)](../Page/旭町_\(苫小牧市\).md "wikilink") -
    北海道[苫小牧市的地名](../Page/苫小牧市.md "wikilink")。
  - [旭町 (函館市)](../Page/旭町_\(函館市\).md "wikilink") -
    北海道[函館市的地名](../Page/函館市.md "wikilink")。
  - [旭町 (夕張市)](../Page/旭町_\(夕張市\).md "wikilink") -
    北海道[夕張市的地名](../Page/夕張市.md "wikilink")。
  - [旭町 (留萌市)](../Page/旭町_\(留萌市\).md "wikilink") -
    北海道[留萌市的地名](../Page/留萌市.md "wikilink")。
  - [旭町 (足寄町)](../Page/旭町_\(足寄町\).md "wikilink") -
    北海道[足寄郡](../Page/足寄郡.md "wikilink")[足寄町的地名](../Page/足寄町.md "wikilink")。
  - [旭町 (陸別町)](../Page/旭町_\(陸別町\).md "wikilink") -
    北海道足寄郡[陸別町的地名](../Page/陸別町.md "wikilink")。
  - [旭町 (津別町)](../Page/旭町_\(津別町\).md "wikilink") -
    北海道[網走郡](../Page/網走郡.md "wikilink")[津別町的地名](../Page/津別町.md "wikilink")。
  - [旭町 (喜茂別町)](../Page/旭町_\(喜茂別町\).md "wikilink") -
    北海道[虻田郡](../Page/虻田郡.md "wikilink")[喜茂別町的地名](../Page/喜茂別町.md "wikilink")。
  - [旭町 (洞爺湖町)](../Page/旭町_\(洞爺湖町\).md "wikilink") -
    北海道虻田郡[洞爺湖町的地名](../Page/洞爺湖町.md "wikilink")。
  - [旭町 (豊浦町)](../Page/旭町_\(豊浦町\).md "wikilink") -
    北海道虻田郡[豐浦町的地名](../Page/豐浦町.md "wikilink")。
  - [旭町 (浦河町)](../Page/旭町_\(浦河町\).md "wikilink") -
    北海道[浦河郡](../Page/浦河郡.md "wikilink")[浦河町的地名](../Page/浦河町.md "wikilink")。
  - [旭町 (沼田町)](../Page/旭町_\(沼田町\).md "wikilink") -
    北海道[雨龍郡](../Page/雨龍郡.md "wikilink")[沼田町的地名](../Page/沼田町.md "wikilink")。
  - [旭町 (上川町)](../Page/旭町_\(上川町\).md "wikilink") -
    北海道[上川郡](../Page/上川郡_\(石狩国\).md "wikilink")[上川町的地名](../Page/上川町.md "wikilink")。
  - [旭町 (美瑛町)](../Page/旭町_\(美瑛町\).md "wikilink") -
    北海道[上川郡](../Page/上川郡_\(石狩国\).md "wikilink")[美瑛町的地名](../Page/美瑛町.md "wikilink")。
  - [旭町 (剣淵町)](../Page/旭町_\(剣淵町\).md "wikilink") -
    北海道[上川郡](../Page/上川郡_\(天塩国\).md "wikilink")[劍淵町的地名](../Page/劍淵町.md "wikilink")。
  - [旭町 (下川町)](../Page/旭町_\(下川町\).md "wikilink") -
    北海道[上川郡](../Page/上川郡_\(天塩国\).md "wikilink")[下川町的地名](../Page/下川町.md "wikilink")。
  - [旭町 (日高町)](../Page/旭町_\(日高町\).md "wikilink") -
    北海道[沙流郡](../Page/沙流郡.md "wikilink")[日高町的地名](../Page/日高町_\(北海道\).md "wikilink")。
  - [旭町 (今金町)](../Page/旭町_\(今金町\).md "wikilink") -
    北海道[瀨棚郡](../Page/瀨棚郡.md "wikilink")[今金町的地名](../Page/今金町.md "wikilink")。
  - [旭町 (上富良野町)](../Page/旭町_\(上富良野町\).md "wikilink") -
    北海道[空知郡](../Page/空知郡.md "wikilink")[上富良野町的地名](../Page/上富良野町.md "wikilink")。
  - [旭町 (中富良野町)](../Page/旭町_\(中富良野町\).md "wikilink") -
    北海道[空知郡](../Page/空知郡.md "wikilink")[中富良野町的地名](../Page/中富良野町.md "wikilink")。
  - [旭町 (訓子府町)](../Page/旭町_\(訓子府町\).md "wikilink") -
    北海道[常呂郡](../Page/常呂郡.md "wikilink")[訓子府町的地名](../Page/訓子府町.md "wikilink")。
  - [旭町 (池田町)](../Page/旭町_\(池田町\).md "wikilink") -
    北海道[中川郡](../Page/中川郡_\(十勝国\).md "wikilink")[池田町的地名](../Page/池田町_\(北海道\).md "wikilink")。
  - [旭町 (幕別町)](../Page/旭町_\(幕別町\).md "wikilink") -
    北海道[中川郡](../Page/中川郡_\(十勝国\).md "wikilink")[幕別町的地名](../Page/幕別町.md "wikilink")。
  - [旭町 (瀧上町)](../Page/旭町_\(瀧上町\).md "wikilink") -
    北海道[紋別郡](../Page/紋別郡.md "wikilink")[瀧上町的地名](../Page/瀧上町.md "wikilink")。
  - [旭町 (長沼町)](../Page/旭町_\(長沼町\).md "wikilink") -
    北海道[夕張郡](../Page/夕張郡.md "wikilink")[長沼町的地名](../Page/長沼町.md "wikilink")。
  - [旭町 (青森市)](../Page/旭町_\(青森市\).md "wikilink") -
    [青森縣](../Page/青森縣.md "wikilink")[青森市的地名](../Page/青森市.md "wikilink")。
  - [旭町 (黑石市)](../Page/旭町_\(黑石市\).md "wikilink") -
    青森縣[黑石市的地名](../Page/黑石市.md "wikilink")。
  - [旭町 (五所川原市)](../Page/旭町_\(五所川原市\).md "wikilink") -
    青森縣[五所川原市的地名](../Page/五所川原市.md "wikilink")。
  - [旭町 (陸奧市)](../Page/旭町_\(陸奧市\).md "wikilink") -
    青森縣[陸奧市的地名](../Page/陸奧市.md "wikilink")。
  - [旭町 (一関市)](../Page/旭町_\(一関市\).md "wikilink") -
    [岩手縣](../Page/岩手縣.md "wikilink")[一関市的地名](../Page/一関市.md "wikilink")。
  - [旭町 (久慈市)](../Page/旭町_\(久慈市\).md "wikilink") -
    岩手縣[久慈市的地名](../Page/久慈市.md "wikilink")。
  - [旭町 (石巻市)](../Page/旭町_\(石巻市\).md "wikilink") -
    [宮城縣](../Page/宮城縣.md "wikilink")[石巻市的地名](../Page/石巻市.md "wikilink")。
  - [旭町 (鹽竈市)](../Page/旭町_\(鹽竈市\).md "wikilink") -
    宮城縣[鹽竈市的地名](../Page/鹽竈市.md "wikilink")。
  - [旭町 (白石市)](../Page/旭町_\(白石市\).md "wikilink") -
    宮城縣[白石市的地名](../Page/白石市.md "wikilink")。
  - [旭町 (大河原町)](../Page/旭町_\(大河原町\).md "wikilink") -
    宮城縣[柴田郡](../Page/柴田郡.md "wikilink")[大河原町的地名](../Page/大河原町.md "wikilink")。
  - [旭町 (北秋田市)](../Page/旭町_\(北秋田市\).md "wikilink") -
    [秋田縣](../Page/秋田縣.md "wikilink")[北秋田市的地名](../Page/北秋田市.md "wikilink")。
  - [旭町 (上山市)](../Page/旭町_\(上山市\).md "wikilink") -
    [山形縣](../Page/山形縣.md "wikilink")[上山市的地名](../Page/上山市.md "wikilink")。
  - [旭町 (福島市)](../Page/旭町_\(福島市\).md "wikilink") -
    [福島縣](../Page/福島縣.md "wikilink")[福島市的地名](../Page/福島市.md "wikilink")。
  - [旭町 (会津若松市)](../Page/旭町_\(会津若松市\).md "wikilink") -
    福島縣[会津若松市的地名](../Page/会津若松市.md "wikilink")。
  - [旭町 (白河市)](../Page/旭町_\(白河市\).md "wikilink") -
    福島縣[白河市的地名](../Page/白河市.md "wikilink")。
  - [旭町 (須賀川市)](../Page/旭町_\(須賀川市\).md "wikilink") -
    福島縣[須賀川市的地名](../Page/須賀川市.md "wikilink")。
  - [旭町 (南相馬市)](../Page/旭町_\(南相馬市\).md "wikilink") -
    福島縣[南相馬市原町区的地名](../Page/南相馬市.md "wikilink")。
  - [旭町 (鑑石町)](../Page/旭町_\(鑑石町\).md "wikilink") -
    福島縣[岩瀨郡](../Page/岩瀨郡.md "wikilink")[鏡石町的地名](../Page/鏡石町.md "wikilink")。
  - [旭町 (笠間市)](../Page/旭町_\(笠間市\).md "wikilink") -
    [茨城縣](../Page/茨城縣.md "wikilink")[笠間市的地名](../Page/笠間市.md "wikilink")。
  - [旭町 (古河市)](../Page/旭町_\(古河市\).md "wikilink") -
    茨城縣[古河市的地名](../Page/古河市.md "wikilink")。
  - [旭町 (日立市)](../Page/旭町_\(日立市\).md "wikilink") -
    茨城縣[日立市的地名](../Page/日立市.md "wikilink")。
  - [旭町 (栃木市)](../Page/旭町_\(栃木市\).md "wikilink") -
    [栃木縣](../Page/栃木縣.md "wikilink")[栃木市的地名](../Page/栃木市.md "wikilink")。
  - [旭町 (足利市)](../Page/旭町_\(足利市\).md "wikilink") -
    栃木縣[足利市的地名](../Page/足利市.md "wikilink")。
  - [旭町 (高崎市)](../Page/旭町_\(高崎市\).md "wikilink") -
    [群馬縣](../Page/群馬縣.md "wikilink")[高崎市的地名](../Page/高崎市.md "wikilink")。
  - [旭町 (桐生市)](../Page/旭町_\(桐生市\).md "wikilink") -
    群馬縣[桐生市的地名](../Page/桐生市.md "wikilink")。
  - [旭町 (所泽市)](../Page/旭町_\(所泽市\).md "wikilink") -
    [埼玉縣](../Page/埼玉縣.md "wikilink")[所泽市的地名](../Page/所泽市.md "wikilink")。
  - [旭町 (川越市)](../Page/旭町_\(川越市\).md "wikilink") -
    埼玉縣[川越市的地名](../Page/川越市.md "wikilink")。
  - [旭町 (行田市)](../Page/旭町_\(行田市\).md "wikilink") -
    埼玉縣[行田市的地名](../Page/行田市.md "wikilink")。
  - [旭町 (草加市)](../Page/旭町_\(草加市\).md "wikilink") -
    埼玉縣[草加市的地名](../Page/草加市.md "wikilink")。
  - [旭町 (千葉市)](../Page/旭町_\(千葉市\).md "wikilink") -
    [千葉縣](../Page/千葉縣.md "wikilink")[千葉市](../Page/千葉市.md "wikilink")[中央区的地名](../Page/中央区_\(千葉市\).md "wikilink")。
  - [旭町 (柏市)](../Page/旭町_\(柏市\).md "wikilink") -
    千葉縣[柏市的地名](../Page/柏市.md "wikilink")。
  - [旭町 (船橋市)](../Page/旭町_\(船橋市\).md "wikilink") -
    千葉縣[船橋市的地名](../Page/船橋市.md "wikilink")。
  - [旭町 (松戸市)](../Page/旭町_\(松戸市\).md "wikilink") -
    千葉縣[松戶市的地名](../Page/松戶市.md "wikilink")。
  - [旭町 (練馬区)](../Page/旭町_\(練馬区\).md "wikilink") -
    [東京都](../Page/東京都.md "wikilink")[練馬区的町名](../Page/練馬区.md "wikilink")。
  - [旭町 (八王子市)](../Page/旭町_\(八王子市\).md "wikilink") -
    東京都[八王子市的地名](../Page/八王子市.md "wikilink")。
  - [旭町 (町田市)](../Page/旭町_\(町田市\).md "wikilink") -
    東京都[町田市的地名](../Page/町田市.md "wikilink")。
  - [旭町 (川崎市)](../Page/旭町_\(川崎市\).md "wikilink") -
    [神奈川縣](../Page/神奈川縣.md "wikilink")[川崎市](../Page/川崎市.md "wikilink")[川崎区的地名](../Page/川崎区.md "wikilink")。
  - [旭町 (厚木市)](../Page/旭町_\(厚木市\).md "wikilink") -
    神奈川縣[厚木市的地名](../Page/厚木市.md "wikilink")。
  - [旭町 (相模原市)](../Page/旭町_\(相模原市\).md "wikilink") -
    神奈川縣[相模原市](../Page/相模原市.md "wikilink")[南区的地名](../Page/南区_\(相模原市\).md "wikilink")。
  - [旭町 (長岡市)](../Page/旭町_\(長岡市\).md "wikilink") -
    [新潟縣](../Page/新潟縣.md "wikilink")[長岡市的地名](../Page/長岡市.md "wikilink")。
  - [旭町 (南魚沼市)](../Page/旭町_\(南魚沼市\).md "wikilink") -
    新潟縣[南魚沼市的地名](../Page/南魚沼市.md "wikilink")。
  - [旭町 (小千谷市)](../Page/旭町_\(小千谷市\).md "wikilink") -
    新潟縣[小千谷市的地名](../Page/小千谷市.md "wikilink")。
  - [旭町 (加茂市)](../Page/旭町_\(加茂市\).md "wikilink") -
    新潟縣[加茂市的地名](../Page/加茂市.md "wikilink")。
  - [旭町 (五泉市)](../Page/旭町_\(五泉市\).md "wikilink") -
    新潟縣[五泉市的地名](../Page/五泉市.md "wikilink")。
  - [旭町 (三条市)](../Page/旭町_\(三条市\).md "wikilink") -
    新潟縣[三条市的地名](../Page/三条市.md "wikilink")。
  - [旭町 (富山市)](../Page/旭町_\(富山市\).md "wikilink") -
    [富山縣](../Page/富山縣.md "wikilink")[富山市的地名](../Page/富山市.md "wikilink")。
  - [旭町 (上市町)](../Page/旭町_\(上市町\).md "wikilink") -
    富山縣[中新川郡](../Page/中新川郡.md "wikilink")[上市町的地名](../Page/上市町.md "wikilink")。
  - [旭町 (金泽市)](../Page/旭町_\(金泽市\).md "wikilink") -
    [石川縣](../Page/石川縣.md "wikilink")[金澤市的地名](../Page/金澤市.md "wikilink")。
  - [旭町 (小松市)](../Page/旭町_\(小松市\).md "wikilink") -
    石川縣[小松市的地名](../Page/小松市.md "wikilink")。
  - [旭町 (七尾市)](../Page/旭町_\(七尾市\).md "wikilink") -
    石川縣[七尾市的地名](../Page/七尾市.md "wikilink")。
  - [旭町 (羽咋市)](../Page/旭町_\(羽咋市\).md "wikilink") -
    石川縣[羽咋市的地名](../Page/羽咋市.md "wikilink")。
  - [旭町 (白山市)](../Page/旭町_\(白山市\).md "wikilink") -
    石川縣[白山市的地名](../Page/白山市.md "wikilink")。
  - [旭町 (勝山市)](../Page/旭町_\(勝山市\).md "wikilink") -
    [福井縣](../Page/福井縣.md "wikilink")[勝山市的地名](../Page/勝山市.md "wikilink")。
  - [旭町 (鯖江市)](../Page/旭町_\(鯖江市\).md "wikilink") -
    福井縣[鯖江市的地名](../Page/鯖江市.md "wikilink")。
  - [旭町 (長野市)](../Page/旭町_\(長野市\).md "wikilink") -
    [長野縣](../Page/長野縣.md "wikilink")[長野市的地名](../Page/長野市.md "wikilink")。
  - [旭町 (飯田市)](../Page/旭町_\(飯田市\).md "wikilink") -
    長野縣[飯田市的地名](../Page/飯田市.md "wikilink")。
  - [旭町 (上松町)](../Page/旭町_\(上松町\).md "wikilink") -
    長野縣[木曾郡](../Page/木曾郡.md "wikilink")[上松町的地名](../Page/上松町.md "wikilink")。
  - [旭町 (大垣市)](../Page/旭町_\(大垣市\).md "wikilink") -
    [岐阜縣](../Page/岐阜縣.md "wikilink")[大垣市的地名](../Page/大垣市.md "wikilink")。
  - [旭町 (静岡市)](../Page/旭町_\(静岡市\).md "wikilink") -
    [静岡縣](../Page/静岡縣.md "wikilink")[静岡市](../Page/静岡市.md "wikilink")[清水区的地名](../Page/清水区_\(日本\).md "wikilink")。
  - [旭町 (沼津市)](../Page/旭町_\(沼津市\).md "wikilink") -
    静岡縣[沼津市的地名](../Page/沼津市.md "wikilink")。
  - [旭町 (浜松市)](../Page/旭町_\(浜松市\).md "wikilink") -
    静岡縣[濱松市](../Page/濱松市.md "wikilink")[中区的地名](../Page/中區_\(濱松市\).md "wikilink")。
  - [旭町 (袋井市)](../Page/旭町_\(袋井市\).md "wikilink") -
    静岡縣[袋井市的地名](../Page/袋井市.md "wikilink")。
  - [旭町 (岩倉市)](../Page/旭町_\(岩倉市\).md "wikilink") -
    [愛知縣](../Page/愛知縣.md "wikilink")[岩倉市的地名](../Page/岩倉市.md "wikilink")。
  - [旭町 (蒲郡市)](../Page/旭町_\(蒲郡市\).md "wikilink") -
    愛知縣[蒲郡市的地名](../Page/蒲郡市.md "wikilink")。
  - [旭町 (豊川市)](../Page/旭町_\(豊川市\).md "wikilink") -
    愛知縣[豐川市的地名](../Page/豐川市.md "wikilink")。
  - [旭町 (豊橋市)](../Page/旭町_\(豊橋市\).md "wikilink") -
    愛知縣[豐橋市的地名](../Page/豐橋市.md "wikilink")。
  - [旭町 (半田市)](../Page/旭町_\(半田市\).md "wikilink") -
    愛知縣[半田市的地名](../Page/半田市.md "wikilink")。
  - [旭町 (碧南市)](../Page/旭町_\(碧南市\).md "wikilink") -
    愛知縣[碧南市的地名](../Page/碧南市.md "wikilink")。
  - [旭町 (春日井市)](../Page/旭町_\(春日井市\).md "wikilink") -
    愛知縣[春日井市的地名](../Page/春日井市.md "wikilink")。
  - [旭町 (伊勢市)](../Page/旭町_\(伊勢市\).md "wikilink") -
    [三重縣](../Page/三重縣.md "wikilink")[伊勢市的地名](../Page/伊勢市.md "wikilink")。
  - [旭町 (彦根市)](../Page/旭町_\(彦根市\).md "wikilink") -
    [滋賀縣](../Page/滋賀縣.md "wikilink")[彦根市的地名](../Page/彦根市.md "wikilink")。
  - [旭町 (綾部市)](../Page/旭町_\(綾部市\).md "wikilink") -
    [京都府](../Page/京都府.md "wikilink")[綾部市的地名](../Page/綾部市.md "wikilink")。
  - [旭町 (龟岡市)](../Page/旭町_\(龟岡市\).md "wikilink") -
    京都府[龟岡市的地名](../Page/龟岡市.md "wikilink")。
  - 旭町 (大阪市) -
    [大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[阿倍野区的地名](../Page/阿倍野区.md "wikilink")。→[金塚地区を参照](../Page/金塚地区.md "wikilink")。
  - [旭町 (泉大津市)](../Page/旭町_\(泉大津市\).md "wikilink") -
    大阪府[泉大津市的地名](../Page/泉大津市.md "wikilink")。
  - [旭町 (泉佐野市)](../Page/旭町_\(泉佐野市\).md "wikilink") -
    大阪府[泉佐野市的地名](../Page/泉佐野市.md "wikilink")。
  - [旭町 (東大阪市)](../Page/旭町_\(東大阪市\).md "wikilink") -
    大阪府[東大阪市的地名](../Page/東大阪市.md "wikilink")。
  - [旭町 (小野市)](../Page/旭町_\(小野市\).md "wikilink") -
    [兵庫縣](../Page/兵庫縣.md "wikilink")[小野市的地名](../Page/小野市.md "wikilink")。
  - [旭町 (宝塚市)](../Page/旭町_\(宝塚市\).md "wikilink") -
    兵庫縣[宝塚市的地名](../Page/宝塚市.md "wikilink")。
  - [旭町 (御所市)](../Page/旭町_\(御所市\).md "wikilink") -
    [奈良縣](../Page/奈良縣.md "wikilink")[御所市的地名](../Page/御所市.md "wikilink")。
  - [旭町 (田原本町)](../Page/旭町_\(田原本町\).md "wikilink") -
    奈良縣[磯城郡](../Page/磯城郡.md "wikilink")[田原本町的地名](../Page/田原本町.md "wikilink")。
  - [旭町 (岡山市)](../Page/旭町_\(岡山市\).md "wikilink") -
    [岡山縣](../Page/岡山縣.md "wikilink")[岡山市](../Page/岡山市.md "wikilink")[北区的地名](../Page/北区_\(岡山市\).md "wikilink")。
  - [旭町 (高梁市)](../Page/旭町_\(高梁市\).md "wikilink") -
    岡山縣[高梁市的地名](../Page/高梁市.md "wikilink")。
  - [旭町 (福山市)](../Page/旭町_\(福山市\).md "wikilink") -
    [廣島縣](../Page/廣島縣.md "wikilink")[福山市的地名](../Page/福山市.md "wikilink")。
  - [旭町 (三原市)](../Page/旭町_\(三原市\).md "wikilink") -
    廣島縣[三原市的地名](../Page/三原市.md "wikilink")。
  - [旭町 (岩国市)](../Page/旭町_\(岩国市\).md "wikilink") -
    [山口縣](../Page/山口縣.md "wikilink")[岩国市的地名](../Page/岩国市.md "wikilink")。
  - [旭町 (山陽小野田市)](../Page/旭町_\(山陽小野田市\).md "wikilink") -
    山口縣[山陽小野田市的地名](../Page/山陽小野田市.md "wikilink")。
  - [旭町 (坂出市)](../Page/旭町_\(坂出市\).md "wikilink") -
    [香川縣](../Page/香川縣.md "wikilink")[坂出市的地名](../Page/坂出市.md "wikilink")。
  - [旭町 (松山市)](../Page/旭町_\(松山市\).md "wikilink") -
    [愛媛縣](../Page/愛媛縣.md "wikilink")[松山市的地名](../Page/松山市.md "wikilink")。
  - [旭町 (今治市)](../Page/旭町_\(今治市\).md "wikilink") -
    愛媛縣[今治市的地名](../Page/今治市.md "wikilink")。
  - [旭町 (八幡浜市)](../Page/旭町_\(八幡浜市\).md "wikilink") -
    愛媛縣[八幡濱市的地名](../Page/八幡濱市.md "wikilink")。
  - [旭町 (高知市)](../Page/旭町_\(高知市\).md "wikilink") -
    [高知縣](../Page/高知縣.md "wikilink")[高知市的地名](../Page/高知市.md "wikilink")。
  - [旭町 (土佐清水市)](../Page/旭町_\(土佐清水市\).md "wikilink") -
    高知縣[土佐清水市的地名](../Page/土佐清水市.md "wikilink")。
  - [旭町 (伊野町)](../Page/旭町_\(伊野町\).md "wikilink") -
    高知縣[吾川郡](../Page/吾川郡.md "wikilink")[伊野町的地名](../Page/伊野町.md "wikilink")。
  - [旭町 (北九州市)](../Page/旭町_\(北九州市\).md "wikilink") -
    [福岡縣](../Page/福岡縣.md "wikilink")[北九州市](../Page/北九州市.md "wikilink")[戶畑区的地名](../Page/戶畑区.md "wikilink")。
  - [旭町 (大牟田市)](../Page/旭町_\(大牟田市\).md "wikilink") -
    福岡縣[大牟田市的地名](../Page/大牟田市.md "wikilink")。
  - [旭町 (久留米市)](../Page/旭町_\(久留米市\).md "wikilink") -
    福岡縣[久留米市的地名](../Page/久留米市.md "wikilink")。
  - [旭町 (柳川市)](../Page/旭町_\(柳川市\).md "wikilink") -
    福岡縣[柳川市的地名](../Page/柳川市.md "wikilink")。
  - [旭町 (長崎市)](../Page/旭町_\(長崎市\).md "wikilink") -
    [長崎縣](../Page/長崎縣.md "wikilink")[長崎市的地名](../Page/長崎市.md "wikilink")。
  - [旭町 (諫早市)](../Page/旭町_\(諫早市\).md "wikilink") -
    長崎縣[諫早市的地名](../Page/諫早市.md "wikilink")。
  - [旭町 (天草市)](../Page/旭町_\(天草市\).md "wikilink") -
    [熊本縣](../Page/熊本縣.md "wikilink")[天草市的地名](../Page/天草市.md "wikilink")。
  - [旭町 (宇土市)](../Page/旭町_\(宇土市\).md "wikilink") -
    熊本縣[宇土市的地名](../Page/宇土市.md "wikilink")。
  - [旭町 (水俣市)](../Page/旭町_\(水俣市\).md "wikilink") -
    熊本縣[水俣市的地名](../Page/水俣市.md "wikilink")。
  - [旭町 (大分市)](../Page/旭町_\(大分市\).md "wikilink") -
    [大分縣](../Page/大分縣.md "wikilink")[大分市的地名](../Page/大分市.md "wikilink")。
  - [旭町 (延岡市)](../Page/旭町_\(延岡市\).md "wikilink") -
    [宮崎縣](../Page/宮崎縣.md "wikilink")[延岡市的地名](../Page/延岡市.md "wikilink")。
  - [旭町 (市來串木野市)](../Page/旭町_\(市來串木野市\).md "wikilink") -
    [鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[市來串木野市的地名](../Page/市來串木野市.md "wikilink")。
  - [旭町 (垂水市)](../Page/旭町_\(垂水市\).md "wikilink") -
    鹿児島縣[垂水市的地名](../Page/垂水市.md "wikilink")。
  - [旭町 (枕崎市)](../Page/旭町_\(枕崎市\).md "wikilink") -
    鹿児島縣[枕崎市的地名](../Page/枕崎市.md "wikilink")。
  - [旭町 (薩摩町)](../Page/旭町_\(薩摩町\).md "wikilink") -
    鹿児島縣[薩摩郡](../Page/薩摩郡.md "wikilink")[薩摩町的地名](../Page/薩摩町.md "wikilink")。
  - [旭町 (那覇市)](../Page/旭町_\(那覇市\).md "wikilink") -
    [沖縄縣](../Page/沖縄縣.md "wikilink")[那覇市的地名](../Page/那覇市.md "wikilink")。
  - 東京都[新宿区的旧地名](../Page/新宿区.md "wikilink")。[新宿区\#消滅した町名参照](../Page/新宿区#消滅した町名.md "wikilink")。
  - 札幌市[厚別区的旧地名](../Page/厚別区.md "wikilink")。現在的厚別区[厚別中央的一部](../Page/厚別中央.md "wikilink")。
  - 宮城縣[仙台市的旧地名](../Page/仙台市.md "wikilink")。現在的仙台市[若林区白萩町](../Page/若林区.md "wikilink")。
  - 廣島縣廣島市的旧地名。現在的[南区](../Page/南区_\(廣島市\).md "wikilink")[旭・西旭町](../Page/旭_\(廣島市\).md "wikilink")。

### 臺灣

  - [旭町 (宜蘭市)](../Page/旭町_\(宜蘭市\).md "wikilink")：臺灣日治時期臺北州宜蘭市的一個行政區劃。
  - [旭町
    (基隆市)](../Page/旭町_\(基隆市\).md "wikilink")：[臺灣](../Page/臺灣.md "wikilink")[日治時期](../Page/台灣日治時期.md "wikilink")[基隆市的一個行政區劃](../Page/基隆市_\(州轄市\).md "wikilink")。
  - [旭町
    (台北市)](../Page/旭町_\(台北市\).md "wikilink")：臺灣日治時期[臺北市的一個行政區劃](../Page/臺北市_\(州轄市\).md "wikilink")。
  - [旭町
    (新竹市)](../Page/旭町_\(新竹市\).md "wikilink")：臺灣日治時期[新竹市的一個行政區劃](../Page/新竹市_\(州轄市\).md "wikilink")。
  - [旭町
    (臺中市)](../Page/旭町_\(臺中市\).md "wikilink")：臺灣日治時期[臺中市的一個行政區劃](../Page/臺中市_\(州轄市\).md "wikilink")。
  - [旭町 (彰化市)](../Page/旭町_\(彰化市\).md "wikilink")：臺灣日治時期臺中州彰化市的一個行政區劃。
  - [旭町
    (臺南市)](../Page/旭町_\(臺南市\).md "wikilink")：臺灣日治時期[臺南市的一個行政區劃](../Page/臺南市_\(州轄市\).md "wikilink")。
  - [旭町 (屏東市)](../Page/旭町_\(屏東市\).md "wikilink")：臺灣日治時期高雄州屏東市的一個行政區劃。

## 相关条目

  - [千住旭町](../Page/千住旭町.md "wikilink") -
    東京都荒川区的町名→[千住参照](../Page/千住.md "wikilink")。
  - [羽田旭町](../Page/羽田旭町.md "wikilink") - 東京都大田区的町名。

[Category:二字地名消歧义](../Category/二字地名消歧义.md "wikilink")