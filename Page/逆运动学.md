**逆运动学**（Inverse
kinematics）是决定要达成所需要的姿势所要设置的关节可活动对象的参数的过程。例如，给定一个人体的三维模型，如何设置手腕和手肘的角度以便把手从放松位置变成挥手的姿势？这个问题在[机器人学中是很关键的](../Page/机器人学.md "wikilink")，因为操纵机械手臂通过关节角度来控制。逆运动学在[游戏编程和](../Page/游戏编程.md "wikilink")[三维建模中也很重要](../Page/三维模型.md "wikilink")，虽然其重要性因为[运动捕获数据的大型数据库越来越多的使用而降低了](../Page/运动捕获.md "wikilink")。

以关节连接的物体由一组通过关节连接的刚性片段组成。变换关节的角度可以产生无穷的形状。正向[运动学问题的解](../Page/运动学.md "wikilink")，是给定这些角度时物体的姿势。“逆运动学问题”的难度更高的解是给定物体的姿势时——例如，给定终端效果器（end-effector）的位置时——找到关节的角度。一般情况下，逆运动学问题没有解析解。但是，逆运动学可以通过[非线性编程技术来解决](../Page/非线性编程.md "wikilink")。特定的特殊运动链—那些带有[球形腕的](../Page/球形腕.md "wikilink")—允许[运动去耦合](../Page/运动去耦合.md "wikilink")。这使得我们可以把终端效果器的朝向和位置独立的处理，并导致一个高效的闭形式解。

对于动画家（animator），逆运动学问题很重要。这些[艺术家发现表达空间的形象比控制关节角度来要容易得多](../Page/艺术家.md "wikilink")。逆运动学算法的应用包括[交互操纵](../Page/交互操纵.md "wikilink")，[动画控制和](../Page/动画控制.md "wikilink")[碰撞避免](../Page/碰撞检测.md "wikilink")。

## 参见条目

  - [计算机动画](../Page/计算机动画.md "wikilink")
  - [物理引擎](../Page/物理引擎.md "wikilink")
  - [布娃娃系统](../Page/布娃娃系统.md "wikilink")

## 外部链接

  - [逆运动学算法](https://www.webcitation.org/60uCEXySZ?url=http://freespace.virgin.net/hugo.elias/models/m_ik.htm)
  - [机器人逆运动学](http://www.learnaboutrobots.com/inverseKinematics.htm)
  - [HowStuffWorks.com条目“动画游戏中的任务如何流畅的运动”](http://entertainment.howstuffworks.com/question538.htm)有一个逆运动学的解释。

[Category:机器人学](../Category/机器人学.md "wikilink")
[Category:计算机图形学](../Category/计算机图形学.md "wikilink")
[Category:计算物理学](../Category/计算物理学.md "wikilink")
[Category:三维计算机图形学](../Category/三维计算机图形学.md "wikilink")