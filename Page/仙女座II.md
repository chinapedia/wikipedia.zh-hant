**仙女座
Ⅱ**是位於[仙女座的一個矮橢球星系](../Page/仙女座.md "wikilink")，距離大約222萬光年。它是[本星系群的一員](../Page/本星系群.md "wikilink")，也是[M31的](../Page/仙女座星系.md "wikilink")[衛星星系](../Page/衛星星系.md "wikilink")，但是他也很接近[M33](../Page/M33.md "wikilink")，所以也能是M33的衛星星系，而這一點還不是很清楚。

它是[Sydney van Der
Bergh在](../Page/Sydney_van_Der_Bergh.md "wikilink")1970年和1971年使用[帕羅馬天文台](../Page/帕羅馬天文台.md "wikilink")48英吋（1.2公尺）的[施密特望遠鏡進行攝影乾板巡天時](../Page/施密特望遠鏡.md "wikilink")，與[仙女座
I和](../Page/仙女座_I.md "wikilink")[仙女座
Ⅲ一起發現的](../Page/仙女座_Ⅲ.md "wikilink")。\[1\] 而推測[仙女座
Ⅳ可能也是在背景中的一個星系](../Page/仙女座_Ⅳ.md "wikilink")。（van dan Bergh 1972）

## 相關條目

  - [仙女座星系的衛星星系](../Page/仙女座星系的衛星星系.md "wikilink")

## 外部連結

  - [**SEDS**: Dwarf Spheroidal Galaxy Andromeda
    II](https://web.archive.org/web/20060213164841/http://www.seds.org/~spider/spider/LG/and2.html)

## 參考資料

[Category:矮橢球星系](../Category/矮橢球星系.md "wikilink")
[Category:仙女座次集團](../Category/仙女座次集團.md "wikilink")
[Andromeda 02](../Category/仙女座.md "wikilink")

1.