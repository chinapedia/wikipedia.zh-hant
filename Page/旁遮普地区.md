[Punjab_1903.gif](https://zh.wikipedia.org/wiki/File:Punjab_1903.gif "fig:Punjab_1903.gif")[Punjab_1909.jpg](https://zh.wikipedia.org/wiki/File:Punjab_1909.jpg "fig:Punjab_1909.jpg")
**旁遮普**，[波斯語](../Page/波斯語.md "wikilink")（panj-āb，[英語化成Punjab](../Page/英語化.md "wikilink")）意為「五[川](../Page/河川.md "wikilink")」（panj是五，āb是川）\[1\]，由[突厥裔波斯統治者命名](../Page/突厥.md "wikilink")\[2\]，至蒙古裔的[蒙兀兒帝國時期廣泛採用](../Page/蒙兀兒帝國.md "wikilink")\[3\]\[4\]。

旁遮普指[印度河中上游眾多支流的一帶地區](../Page/印度河.md "wikilink")，在下游眾支流合一的沿岸地區通常不算作旁遮普。旁遮普地區横跨[印度西北和](../Page/印度.md "wikilink")[巴基斯坦東北兩國](../Page/巴基斯坦.md "wikilink")。这一地区自亞歷山大遠-{征}-軍到來以前即具有悠久的历史和文化。主要民族为[旁遮普人](../Page/旁遮普人.md "wikilink")，主要语言为[旁遮普语](../Page/旁遮普语.md "wikilink")。印度籍的旁遮普人，主要信仰为[锡克教和](../Page/锡克教.md "wikilink")[印度教](../Page/印度教.md "wikilink")，巴基斯坦籍則主要為[伊斯兰教](../Page/伊斯兰教.md "wikilink")。

古代旁遮普地区（大旁遮普地区）地域宽广，包括了：今印度北部、巴基斯坦东部和[阿富汗部分地区](../Page/阿富汗.md "wikilink")，最东处曾一度延伸至[亚穆纳河](../Page/亚穆纳河.md "wikilink")。

旁遮普人在古代也被称为或，这些称谓糅合了包括[健馱邏人](../Page/健馱邏國.md "wikilink")、、、[伊朗人和境外的](../Page/伊朗人.md "wikilink")、、波斯化的[爱奥尼亚人等等诸民族](../Page/爱奥尼亚人.md "wikilink")。\[5\]

旁遮普地区[人种构成上为](../Page/人种.md "wikilink")[雅利安人](../Page/雅利安人.md "wikilink")，不同的历史时期由不同的[民族统治](../Page/民族.md "wikilink")，包括[古希腊人](../Page/古希腊人.md "wikilink")、[波斯人](../Page/波斯人.md "wikilink")、[阿拉伯人](../Page/阿拉伯人.md "wikilink")、[突厥人](../Page/突厥人.md "wikilink")、[莫卧儿人](../Page/莫卧儿人.md "wikilink")、[阿富汗人](../Page/阿富汗人.md "wikilink")、[锡克人和](../Page/锡克人.md "wikilink")[英国人](../Page/英国人.md "wikilink")。

历史上，位于[印度次大陆西北部的旁遮普地区以亚穆纳河和](../Page/印度次大陆.md "wikilink")[印度河为界](../Page/印度河.md "wikilink")，也是前15世纪以后，早期雅里安人定居以来的印度河谷史前文明的中心地带。8世纪开始，随着莫卧儿人的统治，伊斯兰教取得主导地位，旁遮普地区成为印度次大陆的文化中心。往后，由于锡克人的频频反叛，加速了英国人对这一地区的征服，最终随著[印巴分治而分属二个不同的](../Page/印巴分治.md "wikilink")[国家](../Page/国家.md "wikilink")，即“**东旁遮普**”和“**西旁遮普**”二大地区。

  - **西旁遮普地区**：即巴基斯坦[旁遮普省](../Page/旁遮普省.md "wikilink")，占旁遮普地区主要部分，面积205,344平方公里，人口86,084,000人(2005年)，约90％的人讲旁遮普语；西旁遮普语使用[波斯-阿拉伯字母书写](../Page/波斯-阿拉伯字母.md "wikilink")。
  - **东旁遮普地区**：即印度旁遮普地区，包括[旁遮普邦](../Page/旁遮普邦.md "wikilink")、[哈里亚纳邦](../Page/哈里亚纳邦.md "wikilink")、[喜马偕尔邦和](../Page/喜马偕尔邦.md "wikilink")[德里中央直辖区](../Page/德里.md "wikilink")；[旁遮普邦共计面积](../Page/旁遮普邦.md "wikilink")50,362平方公里，人口24,289,296人(2000年)，约92.2％的人讲旁遮普语，东旁遮普语使用[古木基文书写](../Page/古木基文.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

</div>

[Category:旁遮普](../Category/旁遮普.md "wikilink")
[Category:印度地理](../Category/印度地理.md "wikilink")
[Category:巴基斯坦地理](../Category/巴基斯坦地理.md "wikilink")

1.

2.

3.
4.

5.  参见: Evolution of Heroic Tradition in Ancient Panjab, 1971, p 53, Dr
    Buddha Parkash.