**塞文河**（英語：**River Severn**；威爾斯語：**Afon
Hafren**；拉丁語：**Sabrina**）是[大不列顛最長的](../Page/大不列顛.md "wikilink")[河流](../Page/河流.md "wikilink")，長約354公里\[1\]。發源於[威爾斯中部地帶](../Page/威爾斯.md "wikilink")，海拔610公尺，[坎布里安山脉东麓一帶](../Page/坎布里安山脉.md "wikilink")；河道經過的主要城鎮包括[舒茲伯利](../Page/舒茲伯利.md "wikilink")、[伍斯特以及](../Page/伍斯特.md "wikilink")[格洛斯特等](../Page/格洛斯特.md "wikilink")；注入[布里斯托尔湾](../Page/布里斯托尔湾.md "wikilink")，下游有长约80公里的三角湾。由河口上溯可通航约80公里。

## 語源學

“Severn”之名可能來自[凱爾特語](../Page/布立吞亞支.md "wikilink")“\*sabrinn-â”
，其意義不明。\[2\]
除去英語名外，在其他語言中，塞文河的寫法也各不相同，例如[威爾士語寫作](../Page/威爾士語.md "wikilink")“Hafren”。後世有神話稱其名是來自一位叫做塞布麗娜（Sabrina）的[寧芙](../Page/寧芙.md "wikilink")，後者在這條河溺亡。\[3\]
此外塞布麗娜也是[凱爾特神話中一名女神的名字](../Page/凱爾特神話.md "wikilink")。[約翰·彌爾頓在](../Page/約翰·彌爾頓.md "wikilink")《[科莫斯](../Page/科莫斯_\(約翰·彌爾頓\).md "wikilink")》一書中提到過這個故事。\[4\]

## 參考文獻

## 外部連結

  - [Tales of the River Severn](http://www.severntales.co.uk) From
    source to sea along Britain's longest river
  - [Portishead and Bristol
    Lifeboat](http://www.portishead-lifeboat.org.uk)
  - [ITV Local
    footage](http://www.itvlocal.com/central/news/?player=CEN_News_26&void=113448/)
    ITV's Keith Wilkinson and Jennifer Binns canoe the Severn
  - [The Severn Bore](http://www.severn-bore.co.uk)
  - [Environment Agency - Severn Bore and Trent
    Aegir](https://web.archive.org/web/20071230141229/http://www.environment-agency.gov.uk/regions/midlands/434823/)
  - [Surfing The Severn
    Bore](https://web.archive.org/web/20080828044914/http://boreriders.com/)
    note warning in text from Gloucester Harbour Trustees
  - [Woodend, a hamlet washed away by the River
    Severn](http://www.abandonedcommunities.co.uk/woodend.html)

[Category:英國河流](../Category/英國河流.md "wikilink")
[Category:威爾斯河川](../Category/威爾斯河川.md "wikilink")
[Category:英格蘭河川](../Category/英格蘭河川.md "wikilink")

1.
2.  [Etymology of
    *Hafren*](http://kimkat.org/amryw/1_vortaroy/geiriadur_cymraeg_saesneg_BAEDD_h_1038e.htm)
3.
4.