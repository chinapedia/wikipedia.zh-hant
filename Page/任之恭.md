**任之恭**（\[1\]\[2\]）是一位中国[物理学家](../Page/物理学家.md "wikilink")。任之恭提供了[电离层存在的最早的实验证据之一](../Page/电离层.md "wikilink")。他对于[氢原子](../Page/氢原子.md "wikilink")[电离能谱的理论计算](../Page/电离能谱.md "wikilink")，在[量子力学的早期发展中也有重要意义](../Page/量子力学.md "wikilink")。他长期执教于清华大学和西南联合大学，在[物理学](../Page/物理学.md "wikilink")、[电机工程等领域为中国培养了大批人才](../Page/电机工程.md "wikilink")。

## 生平

生于[山西省](../Page/山西省.md "wikilink")[沁源县](../Page/沁源县.md "wikilink")。1926年毕业于[清华学校高等科](../Page/清华学校.md "wikilink")。曾先后在[北京](../Page/北京.md "wikilink")[清华大学](../Page/清华大学_\(北京\).md "wikilink")、[西南联合大学](../Page/西南联合大学.md "wikilink")、[美国](../Page/美国.md "wikilink")[哈佛大学](../Page/哈佛大学.md "wikilink")、[约翰·霍普金斯大学等担任教职并进行实验研究工作](../Page/约翰·霍普金斯大学.md "wikilink")。[二战后](../Page/二战.md "wikilink")，他于1946年出国研究，1955年留居美国，主要从事[微波光谱学方面的研究工作](../Page/微波光谱学.md "wikilink")。1972年[美国总统](../Page/美国总统.md "wikilink")[尼克松访华以后](../Page/尼克松访华.md "wikilink")，任之恭为促进中美两国科学、教育、文化等方面的交流付出了极大努力，曾组织美籍华裔学者访华，先后会见过[周恩来](../Page/周恩来.md "wikilink")、[邓小平等中国领导人](../Page/邓小平.md "wikilink")。

## 奖学金

以任之恭亲属的捐赠为基础，清华大学设立了“清华大学任之恭奖学金”，纪念任之恭先生对中国教育和科技事业的贡献，鼓励清华大学物理系学生勤奋学习。

## 參考文獻

  - [中國大百科智慧藏（97.）：任之恭](http://140.128.103.128/web/Content.asp?ID=44884&Query=)

## 外部链接

[Category:中国物理学家](../Category/中国物理学家.md "wikilink")
[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:中国教育家](../Category/中国教育家.md "wikilink")
[Category:中央研究院數理科學組院士](../Category/中央研究院數理科學組院士.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:國立清華大学校友](../Category/國立清華大学校友.md "wikilink")
[Category:约翰霍普金斯大学教师](../Category/约翰霍普金斯大学教师.md "wikilink")
[Category:国立西南联合大学教授](../Category/国立西南联合大学教授.md "wikilink")
[Category:歸化美國公民的中華民國人](../Category/歸化美國公民的中華民國人.md "wikilink")
[Category:华人物理学家](../Category/华人物理学家.md "wikilink")
[Category:庚子赔款留学生](../Category/庚子赔款留学生.md "wikilink")
[Category:沁源人](../Category/沁源人.md "wikilink")
[Z](../Category/任姓.md "wikilink")

1.  亦有紀錄說明其出生日為1906年8月15日。1906年10月2日即[农历](../Page/农历.md "wikilink")[八月十五日](../Page/八月十五日.md "wikilink")。中国传统上使用农历[生日](../Page/生日.md "wikilink")。
2.  [任之恭 逝世院士一覽表](https://academicians.sinica.edu.tw/index.php?func=1-D)
    中央研究院