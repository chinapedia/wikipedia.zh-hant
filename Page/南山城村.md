**南山城村**（）是位于[京都府東南部的行政區劃](../Page/京都府.md "wikilink")，也是京都府內唯一的[村級行政區劃](../Page/村.md "wikilink")。

## 歷史

現在的南山城村範圍在[江戶時代為](../Page/江戶時代.md "wikilink")[柳生藩的領地](../Page/柳生藩.md "wikilink")，藩主為歷代皆擔任將軍家劍術指導的。[明治維新後](../Page/明治維新.md "wikilink")，柳生藩於1871年改設為柳生縣，但僅四個月後即併入[京都府](../Page/京都府.md "wikilink")。

1889年實施[町村制](../Page/町村制.md "wikilink")，現在的南山城村北部在當時屬於，南部則屬於，兩村於1955年合併為現在的**南山城村**。\[1\]

## 交通

[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")[關西本線沿著](../Page/關西本線.md "wikilink")以東西向通過轄內，並設有及。

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）
      - [關西本線](../Page/關西本線.md "wikilink")：（←[伊賀市](../Page/伊賀市.md "wikilink")）
        -  -  - （[笠置町](../Page/笠置町.md "wikilink")→）

<File:Jrwest> okawara stn.jpg| 大河原車站

## 參考資料

## 相關條目

## 外部連結

  - [相樂東部廣域連合](http://www.rengou.jp/)

<!-- end list -->

1.