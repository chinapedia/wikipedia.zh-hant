**波斯尼亞和黑塞哥維那國家足球隊**（[波士尼亞語](../Page/波士尼亞語.md "wikilink")／[克羅埃西亞語](../Page/克羅埃西亞語.md "wikilink")／[塞爾維亞語](../Page/塞爾維亞語.md "wikilink")：Nogometna/Fudbalska
reprezentacija Bosne i
Hercegovine；[西里爾字母](../Page/西里爾字母.md "wikilink")：Ногометна/Фудбалска
репрезентација Боснe и
Херцеговинe）乃東歐國家[波斯尼亞和黑塞哥維那的官方](../Page/波斯尼亞和黑塞哥維那.md "wikilink")[足球隊](../Page/足球.md "wikilink")，由[波斯尼亞和黑塞哥維那足球協會管理](../Page/波斯尼亞和黑塞哥維那足球協會.md "wikilink")。由於其國家乃前[南斯拉夫一部份](../Page/南斯拉夫.md "wikilink")，當[波斯尼亞和黑塞哥維那於](../Page/波斯尼亞和黑塞哥維那.md "wikilink")1995年從南斯拉夫分裂出來後，才演變成現在的「波斯尼亞和黑塞哥維那國家足球隊」。雖然從南斯拉夫分裂出來，成立也不過十年多，波斯尼亞和黑塞哥維那至2013年未打入任何重要的國際賽，包括[世界杯和](../Page/世界杯.md "wikilink")[歐洲國家杯](../Page/歐洲國家杯.md "wikilink")。

近年來從波黑出產了不少好手如[萨利哈米季奇](../Page/哈桑·萨利哈米季奇.md "wikilink")，[斯帕希奇](../Page/埃米尔·斯帕希奇.md "wikilink")，[哲科](../Page/哲科.md "wikilink")，[米西莫维奇](../Page/兹夫耶兹丹·米西莫维奇.md "wikilink")，[伊比舍维奇](../Page/韦达德·伊比舍维奇.md "wikilink")，經過接連於2010年世界盃外圍賽及2012年歐洲國家盃外圍賽敗給[葡萄牙後](../Page/葡萄牙國家足球隊.md "wikilink")，憑著全队的出色發揮，波斯尼亞獲得2014年世界杯的出線資格，這是該國首次參與世界杯決賽周。2015年歐洲國家盃16外圍賽，敗給[愛爾蘭](../Page/愛爾蘭國家足球隊.md "wikilink")。

## 世界杯紀錄

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/世界盃足球賽.md" title="wikilink">世界盃參賽紀錄</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2022年世界盃足球賽.md" title="wikilink">2022年</a></p></td>
</tr>
<tr class="even">
<td><p><strong>總結</strong></p></td>
</tr>
</tbody>
</table>

## 歐洲國家杯紀錄

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/欧洲足球锦标赛.md" title="wikilink">歐國盃參賽紀錄</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1960年歐洲國家盃.md" title="wikilink">1960年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1964年歐洲國家盃.md" title="wikilink">1964年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年歐洲國家盃.md" title="wikilink">1968年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1972年歐洲國家盃.md" title="wikilink">1972年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1976年歐洲國家盃.md" title="wikilink">1976年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1980年歐洲國家盃.md" title="wikilink">1980年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1984年歐洲國家盃.md" title="wikilink">1984年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1988年歐洲國家盃.md" title="wikilink">1988年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992年歐洲國家盃.md" title="wikilink">1992年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1996年歐洲國家盃.md" title="wikilink">1996年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年歐洲國家盃.md" title="wikilink">2000年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年歐洲國家盃.md" title="wikilink">2004年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年歐洲國家盃.md" title="wikilink">2008年</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2012年歐洲國家盃.md" title="wikilink">2012年</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年歐洲國家盃.md" title="wikilink">2016年</a></p></td>
</tr>
<tr class="odd">
<td><p>總結</p></td>
</tr>
</tbody>
</table>

## 近賽成績

### 2014年世界盃

  - [外圍賽G組](../Page/2014年世界盃外圍賽歐洲區G組.md "wikilink")

### 2016年歐洲國家盃

## 球員名單

最新一期球員名單更新至2014年世界盃,出賽與進球數更新至2014年6月25日小組賽對[伊朗](../Page/伊朗國家足球隊.md "wikilink")。
    |----- \! colspan="9" bgcolor="\#B0D3FB" align="left"| |-----
bgcolor="\#DFEDFD"         |----- \! colspan="9" bgcolor="\#B0D3FB"
align="left"| |----- bgcolor="\#DFEDFD"           |----- \! colspan="9"
bgcolor="\#B0D3FB" align="left"| |----- bgcolor="\#DFEDFD"

## 著名球員

  -
  - [沙歷咸美斯](../Page/哈桑·薩利哈米季奇.md "wikilink")

  - [埃丁·哲科](../Page/埃丁·哲科.md "wikilink")

## 參考資料

## 外部連結

  - [國家隊官方網站](http://www.nfsbih.ba/)
  - [歐洲足協網站介紹](http://www.uefa.com/footballeurope/countries/association=60094/index.html)

[Category:波斯尼亞和黑塞哥維那足球](../Category/波斯尼亞和黑塞哥維那足球.md "wikilink")
[Category:歐洲國家足球隊](../Category/歐洲國家足球隊.md "wikilink")
[Category:波士尼亞與赫塞哥維納體育國家隊](../Category/波士尼亞與赫塞哥維納體育國家隊.md "wikilink")