**苯并环丁烯**是最简单的多环[芳香烃](../Page/芳香环.md "wikilink")，由一个[苯环和一个](../Page/苯.md "wikilink")[环丁烷组成](../Page/环丁烷.md "wikilink")\[1\]。

苯并环丁烯经常被用来制造感光性[聚合体](../Page/聚合体.md "wikilink")。以苯并环丁烯为基础的聚合体[电介质能适应多种微电机系统或微电子处理中用的感光](../Page/电介质.md "wikilink")[底片](../Page/底片.md "wikilink")。应用包括片结合，光学互接，低K电介质，甚至包括脑神经植入,达高特用10多年在这方面优化生产工艺。

## 参见

  - [苯并环丙烯](../Page/苯并环丙烯.md "wikilink")
  - [苯并环戊烯](../Page/苯并环戊烯.md "wikilink")

## 参考资料

[ar:بنزو سيكلو بيوتين](../Page/ar:بنزو_سيكلو_بيوتين.md "wikilink")

[Category:芳香烃](../Category/芳香烃.md "wikilink")
[Category:四元环](../Category/四元环.md "wikilink")

1.  [164410
    Benzocyclobutene 98%](http://www.sigmaaldrich.com/catalog/search/ProductDetail/ALDRICH/164410)