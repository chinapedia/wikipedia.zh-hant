**穴蝰**是[蛇亞目](../Page/蛇亞目.md "wikilink")[穴蝰科下的一個](../Page/穴蝰科.md "wikilink")[有毒蛇種](../Page/毒蛇.md "wikilink")，主要分布於[非洲](../Page/非洲.md "wikilink")。目前未有任何亞種。\[1\]

## 特徵

穴蝰體長平均有30至50[公分](../Page/公分.md "wikilink")，最長紀錄為70公分。身體普遍顏色偏[褐](../Page/褐.md "wikilink")[紫色](../Page/紫.md "wikilink")、[灰色或](../Page/灰色.md "wikilink")[黑色](../Page/黑色.md "wikilink")，腹部呈[棕色或](../Page/棕色.md "wikilink")[白色](../Page/白色.md "wikilink")，表皮遍佈斑點。\[2\]牠們主要聚居於半[沙漠](../Page/沙漠.md "wikilink")、[熱帶草原與及林木地帶](../Page/熱帶草原.md "wikilink")。

## 地理分布

穴蝰主要分布於[南非](../Page/南非.md "wikilink")、[納米比亞中部](../Page/納米比亞.md "wikilink")、[剛果共和國](../Page/剛果共和國.md "wikilink")、[烏干達](../Page/烏干達.md "wikilink")、[坦桑尼亞東部](../Page/坦桑尼亞.md "wikilink")、[肯雅海岸與及](../Page/肯雅.md "wikilink")[索馬里的南部海岸等地區](../Page/索馬里.md "wikilink")。\[3\]

## 毒性

穴蝰擁有強烈毒性，不過毒素分泌量並不多。在很多地區都發生過穴蝰咬人事件，有不少人並不了解穴蝰可以在被人箝制[頸部的情況下依然能向對方作出咬擊](../Page/頸部.md "wikilink")。被穴蝰所咬後的中毒徵狀主要有痛楚、傷口腫脹、起泡、[肌肉壞死與及部分區域出現淋巴結病](../Page/肌肉.md "wikilink")。不過至目前為止，仍未有人類被穴蝰咬擊致死的個案紀錄。\[4\]

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：穴蝰](http://reptile-database.reptarium.cz/species.php?genus=Atractaspis&species=bibronii)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:穴蝰科](../Category/穴蝰科.md "wikilink")

1.

2.  Spawls S、Branch B：《The Dangerous Snakes of Africa. Ralph Curtis
    Books》頁192，[杜拜](../Page/杜拜.md "wikilink")：Oriental Press，1995年。ISBN
    0-88359-029-8

3.
4.