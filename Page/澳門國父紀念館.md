**澳門國父紀念館**（），是位於[澳門特別行政區](../Page/澳門特別行政區.md "wikilink")[文第士街紀念](../Page/文第士街.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[國父](../Page/國父.md "wikilink")[孫中山的](../Page/孫中山.md "wikilink")[紀念館](../Page/紀念館.md "wikilink")，係由[中華民國](../Page/中華民國.md "wikilink")[行政院大陸委員會港澳署](../Page/行政院大陸委員會.md "wikilink")[澳門事務處所管理](../Page/行政院大陸委員會澳門事務處.md "wikilink")。

## 沿革

建築物早建於1918年，由[孫中山先生的胞兄](../Page/孫中山.md "wikilink")[孫眉斥資興建](../Page/孫眉.md "wikilink")，原為孫中山與其家人之寓所。於1931年，曾因遭附近的[澳門總督官邸](../Page/澳門總督.md "wikilink")[火藥庫爆炸所波及](../Page/二龍喉火藥庫爆炸事故.md "wikilink")，後來澳葡政府向孫中山的兒子[孫科賠償](../Page/孫科.md "wikilink")，孫科利用賠償，自己另外斥資九萬銀元，將其重建成現建築物樣貌。現時的3層高建築物是一座[伊斯蘭教摩爾式建築](../Page/伊斯蘭教.md "wikilink")。直至1932年，孫中山之子[孫哲生與孫中山之元配夫人](../Page/孫哲生.md "wikilink")[盧慕貞在此居住](../Page/盧慕貞.md "wikilink")。及至盧慕貞在1952年9月逝世後，於1958年易名為國父紀念館，門前匾額由[中華民國首任監察院長](../Page/中華民國監察院.md "wikilink")[于右任題字](../Page/于右任.md "wikilink")。

## 特色

[Mosunysmuseum01.JPG](https://zh.wikipedia.org/wiki/File:Mosunysmuseum01.JPG "fig:Mosunysmuseum01.JPG")
[Mosunysmuseum02.JPG](https://zh.wikipedia.org/wiki/File:Mosunysmuseum02.JPG "fig:Mosunysmuseum02.JPG")

### 展品

國父紀念館保留原貌，大部分房間保持原有佈置。展覽品有孫中山在澳門行醫時所用的物品、在[廣州起義時之家具](../Page/廣州.md "wikilink")、孫中山的真跡和生前照片。

### 孫中山銅像

在紀念館側的花園豎立了一尊孫中山的全身銅像及“天下為公”四個大字。銅像乃孫中山之日籍友人梅屋所獻的紀念品，銅像由[日本](../Page/日本.md "wikilink")[雕塑家牧田祥哉所設計](../Page/雕塑家.md "wikilink")。

### 鐵門

鐵門兩邊原本鑲有[中華民國國徽](../Page/中華民國國徽.md "wikilink")。[“一二·三”事件發生後](../Page/一二·三事件.md "wikilink")，當時的澳門市行政局在1967年1月2日發出公告，禁止在澳門懸掛和顯示敵對[中華人民共和國的外國旗幟](../Page/中華人民共和國.md "wikilink")、徽章和標誌。次日，澳門政府派員到該館拆除鐵門，並以專門訂造的素色鐵門取代。不過，展覽館內孫中山先生半身像兩旁仍然可以展示[中華民國國旗](../Page/中華民國國旗.md "wikilink")，成為澳門唯一一處可以公開展示中華民國國旗的地方。

### 閱覽室

閱覽室對外開放，提供與孫中山先生和[臺灣有關的書刊](../Page/臺灣.md "wikilink")。

## 相關資料

### 拾圓紙幣

澳門大西洋銀行於1991年7月與2001年1月發行之拾圓澳門幣紙幣的正面為國父紀念館。\[1\]

## 相關條目

  - [國立國父紀念館](../Page/國立國父紀念館.md "wikilink")
  - [澳門博物館列表](../Page/澳門博物館列表.md "wikilink")

## 參考資料

  -
[Category:澳门博物馆](../Category/澳门博物馆.md "wikilink")
[Category:澳门名人故居](../Category/澳门名人故居.md "wikilink")
[Category:孫中山紀念館](../Category/孫中山紀念館.md "wikilink")

1.  <http://www.sinobanknote.net/mop/banknote/MacauP065-10Patacas-1991_a.jpg>