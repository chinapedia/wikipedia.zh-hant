《**安平追想曲**》（）是1951年所作的一首[台語歌曲](../Page/台語.md "wikilink")。為[臺灣知名填詞人](../Page/臺灣.md "wikilink")[陳達儒與作曲家](../Page/陳達儒.md "wikilink")[許石的作品](../Page/許石.md "wikilink")。該曲描述的是一個[清代](../Page/清代.md "wikilink")[臺南](../Page/臺南.md "wikilink")[安平港](../Page/安平港.md "wikilink")[買辦](../Page/買辦.md "wikilink")[商人的女兒](../Page/商人.md "wikilink")，與一位負心的[荷蘭](../Page/荷蘭.md "wikilink")[醫師生下一個金髮女郎](../Page/醫師.md "wikilink")，而金髮女郎又一如母親，陷入[愛人的背叛](../Page/愛人.md "wikilink")。該曲所描述的時空背景常被誤認為是[十七世紀](../Page/十七世紀.md "wikilink")[荷領時期](../Page/台灣荷蘭統治時期.md "wikilink")，事實上是發生於[十九世紀末的](../Page/十九世紀.md "wikilink")[清朝](../Page/台灣清治時期.md "wikilink")。此題材不僅於[華語歌壇相當獨特](../Page/華語.md "wikilink")，也為全世界少有。

## 詞曲背景

[FortZelandia_S.jpg](https://zh.wikipedia.org/wiki/File:FortZelandia_S.jpg "fig:FortZelandia_S.jpg")
1950年代[中華民國政府推行](../Page/中華民國政府.md "wikilink")[中華民國國語](../Page/中華民國國語.md "wikilink")，採取種種措施，限制臺語歌曲於[廣播媒體傳唱](../Page/廣播.md "wikilink")。1930年代即以臺語歌曲填詞人為生的陳達儒只得以「新臺灣歌謠社」的名義發行歌本，藉由街頭賣唱，推銷這些歌仔簿（歌詞簿）。

1951年，[臺南音樂家](../Page/臺南.md "wikilink")[許石](../Page/許石.md "wikilink")（其膾炙人口的作品還有《[鑼聲若響](../Page/鑼聲若響.md "wikilink")》、《夜半路燈》、《漂亮你一人》、《初戀日記》、《風雨夜曲》等），以小步舞曲的調子譜成一首曲，他想要找一位名家來填詞，於是他就央請臺南文人[許丙丁動筆](../Page/許丙丁.md "wikilink")，但許丙丁卻推薦另一位[臺北](../Page/臺北.md "wikilink")[艋舺的名人陳達儒先生](../Page/艋舺.md "wikilink")，因為許石要求填詞不能更動曲子的旋律，以至陳達儒雖稱捷才，卻也未能立刻交稿。後來，陳達儒過年時陪太太回臺南娘家，據說和朋友去西門圓環寶美樓喝酒，聽到女侍講起安平金小姐的故事，他遂至安平尋訪這段傳奇，結果只看到黃土一坯，回來之後依許石的曲調，寫下這一段兩代女人戀情的故事，傳詠至今。

同年，該歌曲經由許石以舉辦作品公開發表會發表，因題材新穎，歌詞曲調婉轉動人，頗受歡迎。安平追想曲隨後也由許石創辦的中國唱片公司（後改名為大王唱片公司）收錄出版，首度演唱者為鍾瑛。

該曲自發行出版後，不僅知名的早期臺語歌手[楊麗花](../Page/楊麗花.md "wikilink")，鍾瑛，[劉福助](../Page/劉福助.md "wikilink")，[陳芬蘭等人演唱過](../Page/陳芬蘭.md "wikilink")。1980年代至2000年代流行樂壇的[鄧麗君](../Page/鄧麗君.md "wikilink")、[鳳飛飛與](../Page/鳳飛飛.md "wikilink")[張清芳及後來的](../Page/張清芳.md "wikilink")[江蕙也分別重唱過](../Page/江蕙.md "wikilink")，歌詞中所敘述臺荷混血女子的故事，至今大多數[臺灣人相信是真人真事改編的歌謠](../Page/臺灣人.md "wikilink")，而非想像的創作，學界亦不斷探究歷史上是否真有其人存在。而當地耆老一致表示確有其事，甚至有「金小姐」後代嫁給[陳姓男子](../Page/陳姓.md "wikilink")，仍今仍在臺南市工作的說法，更添傳奇\[1\]。

## 歌詞賞析

安平追想曲所描述，並非一般想像中17世紀[荷治時期之事](../Page/台灣荷蘭統治時期.md "wikilink")，乃為19世紀末的故事。[安平於](../Page/安平區.md "wikilink")[清](../Page/清.md "wikilink")[同治四年](../Page/同治.md "wikilink")（1865年）[元月因](../Page/元月.md "wikilink")[天津條約而開港通商](../Page/天津條約.md "wikilink")，當時一位[安平港](../Page/安平港.md "wikilink")[買辦商人的女兒](../Page/買辦.md "wikilink")，與一位[荷蘭隨船](../Page/荷蘭.md "wikilink")[醫師戀愛後](../Page/醫師.md "wikilink")，生下一個金髮女孩「金小姐」。長大後的金小姐又面臨與母親相同的命運，愛上途經[安平港的](../Page/安平港.md "wikilink")[外國人](../Page/外國人.md "wikilink")，而那外國[男友也如金小姐父親一般](../Page/男友.md "wikilink")，無情地離她而去。

歌詞中，首先利用「[金髮](../Page/金髮.md "wikilink")」兩字點明歌詞主人翁的身分，藉由反差，點破歌曲的獨特性。這些描寫[混血兒與當時社會不同的字詞](../Page/混血兒.md "wikilink")，除了[金髮之外還有歌曲中的](../Page/金髮.md "wikilink")「金十字」（[黃金](../Page/黃金.md "wikilink")[十字架](../Page/十字架.md "wikilink")，代表[基督宗教](../Page/基督宗教.md "wikilink")。不同於[臺灣漢族信仰](../Page/臺灣民間信仰.md "wikilink")），[私生兒](../Page/私生子.md "wikilink")，荷蘭船醫等。

除了描述金小姐的感情世界外，也有幾句描述著安平的地方民情，例如「伊是行船堵風浪」、「相思寄著海邊風」、「等你入港銅鑼聲」、「早日回歸[安平城](../Page/安平城.md "wikilink")」，安平的風與浪素來有名，臺灣俗語「[基隆雨](../Page/基隆市.md "wikilink")、[新竹風](../Page/新竹.md "wikilink")、[安平涌](../Page/安平區.md "wikilink")」，安平的海浪是西南季風造成的，這裡沿岸潮衝擊著沙灘，形成台灣著名的[沙岸侵蝕地形](../Page/沙岸.md "wikilink")，風大浪大，古稱天險，[清代許多臺灣官員詩中均有描述](../Page/清代.md "wikilink")。

## 其他

  - 奇美文化基金會委託雕刻家陳正雄創作「安平金小姐」雕像贈送給[臺南市政府](../Page/臺南市政府.md "wikilink")，於[東興洋行前豎立](../Page/原德商東興洋行.md "wikilink")「金小姐」母女雕像，2011年8月13日揭幕。\[2\]
  - 《安平追想曲》又有一[粵語版本](../Page/粵語.md "wikilink")《[離別的叮嚀](../Page/離別的叮嚀.md "wikilink")》，由香港歌手[譚炳文及](../Page/譚炳文.md "wikilink")[鳴茜於](../Page/鳴茜.md "wikilink")1971年原主唱，收錄於鳴茜的《往事今時回味》專輯內；1973年灌錄於譚炳文《喃嘸阿彌陀佛》專輯內；1978年由另一香港歌手[張偉文於](../Page/張偉文.md "wikilink")[無線電視主辦的](../Page/無線電視.md "wikilink")「業餘歌唱大賽」翻唱，另其名聲日紅。《離別的叮嚀》一曲並由張偉文填詞。

## 註釋

<references />

## 參考資料

  - 郭麗娟：《台灣歌謠臉譜》，台北，玉山社 ISBN 957824679X

  - 《赤崁的主張》，台南，赤崁文史工作室，2003年。

  - [古雅臺語人](https://web.archive.org/web/20070609090815/http://staff.whsh.tc.edu.tw/~huanyin/music/a/anping.php)
    安平追想曲歌詞與歌曲試聽(江蕙版)

  -
## 參見

  - [安平港](../Page/安平港.md "wikilink")
  - [陳達儒](../Page/陳達儒.md "wikilink")
  - [天津條約](../Page/天津條約.md "wikilink")
  - [南都夜曲](../Page/南都夜曲.md "wikilink")
  - [回來安平港](../Page/回來安平港.md "wikilink")

[Category:台語歌曲](../Category/台語歌曲.md "wikilink")
[Category:台語歌詞文學](../Category/台語歌詞文學.md "wikilink")
[Category:台灣歌曲](../Category/台灣歌曲.md "wikilink")
[Category:台南市背景作品](../Category/台南市背景作品.md "wikilink")
[Category:台灣清治後期背景作品](../Category/台灣清治後期背景作品.md "wikilink")
[Category:異族戀題材作品](../Category/異族戀題材作品.md "wikilink")

1.  [安平追想曲
    「金」有其人](http://www.libertytimes.com.tw/2008/new/jun/1/today-so5.htm)
    ，自由時報
2.  [安平追想曲
    金小姐雕像揭幕](http://news.chinatimes.com/society/11050301/112011081400182.html)，中國時報