**贝伊**（），又作“贝格”（Beg），在[哈萨克语中称作](../Page/哈萨克语.md "wikilink")“比”（），是[突厥语中](../Page/突厥语.md "wikilink")“首领”或“酋长”之意，后来成为[奥斯曼帝国属地以及](../Page/奥斯曼帝国.md "wikilink")[中亚](../Page/中亚.md "wikilink")、[南亚地区](../Page/南亚.md "wikilink")[伊斯兰教人士的一种头衔](../Page/伊斯兰教.md "wikilink")，有“总督”、“老爷”等意思。\[1\]

在奥斯曼帝国时期，该词先是对贵族或旁系王子的尊称，次于[汗或](../Page/可汗.md "wikilink")[帕夏](../Page/帕夏.md "wikilink")，一般置于名前；后来泛指各省区的执政者，曾广泛使用于伊斯兰国家。\[2\]\[3\]奥斯曼帝国的前几位君主的头衔就是“贝伊”，1383年[穆拉德一世被](../Page/穆拉德一世.md "wikilink")[哈里发授予](../Page/哈里发.md "wikilink")[苏丹称号后方停用](../Page/苏丹_\(称谓\).md "wikilink")。此后，贝伊成为次于[帕夏的一种称呼](../Page/帕夏.md "wikilink")。在[穆罕默德·阿里统治下的](../Page/穆罕默德·阿里_\(埃及\).md "wikilink")[埃及](../Page/埃及.md "wikilink")，贝伊成为低于帕夏的一个贵族级别。

在[突尼斯](../Page/突尼斯.md "wikilink")，1705年突尼斯王朝统治者称“贝伊”，贝伊成为君主及其家族的头衔。\[4\]从1705年至1957年，“贝伊”代替“台伊”作为对突尼斯统治者的称号。\[5\]

1934年，[土耳其共和国政府明令将](../Page/土耳其共和国.md "wikilink")“贝伊”改为“[巴依](../Page/巴依.md "wikilink")”（Bay），置于名后作为对成年男性的一般尊称，相当于“先生”，已经失去首领的意思。\[6\]\[7\]

中国[新疆](../Page/新疆.md "wikilink")[维吾尔族](../Page/维吾尔族.md "wikilink")[穆斯林称](../Page/穆斯林.md "wikilink")“[伯克](../Page/伯克制.md "wikilink")”（Beg或Bek，同“贝伊”），泛指官吏或统治者；对一些地主或宗教上层人士有时也称“[巴依](../Page/巴依.md "wikilink")”（Bay）或“巴依老爷”。\[8\]\[9\]

贝伊一词的[语源还存有争议](../Page/语源.md "wikilink")。有说法认为源自古代突厥官号“匐”。\[10\][乌孙的](../Page/乌孙.md "wikilink")[靡也与其有关](../Page/靡.md "wikilink")。

著名的贝伊有[阿尔巴尼亚的民族英雄](../Page/阿尔巴尼亚.md "wikilink")[斯坎德培与](../Page/斯坎德培.md "wikilink")[浩罕的](../Page/浩罕.md "wikilink")[阿古柏](../Page/阿古柏.md "wikilink")。

貝伊的女性化叫法為貝姬，是中亞與南亞高社會等級的女性穆斯林的頭銜。

## 参考文献

  - \[<http://www.4dw.net/royalark/Egypt/glossary.htm>| 埃及贵族制度\] &
    [突尼斯贵族制度](http://www.4dw.net/royalark/Tunisia/glossary.htm)
    & [奥斯曼土耳其贵族制度](http://www.4dw.net/royalark/Turkey/turkey.htm)
  - Westermann *Großer Atlas zur Weltgeschichte* (德语)

## 参见

  - [伯克制](../Page/伯克制.md "wikilink")

[Category:贵族头衔](../Category/贵族头衔.md "wikilink")
[Category:埃及头衔](../Category/埃及头衔.md "wikilink")
[Category:土耳其头衔](../Category/土耳其头衔.md "wikilink")
[Category:黎巴嫩头衔](../Category/黎巴嫩头衔.md "wikilink")

1.  中国伊斯兰百科全书，四川辞书出版社，2007年

2.
3.  陈瑞云，大学历史词典，黑龙江人民出版社，1988年

4.
5.
6.
7.
8.
9.  郭晔旻，左宗棠收复新疆始末，历史解密2011年第3期

10. 丝绸之路大辞典，陕西人民出版社，2006年