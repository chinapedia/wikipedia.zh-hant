**王應麟**（），字**伯厚**，號**深寧**，[慶元府](../Page/慶元府.md "wikilink")[鄞縣县城](../Page/鄞縣.md "wikilink")（今属[宁波市](../Page/宁波市.md "wikilink")[海曙区](../Page/海曙区.md "wikilink")）人，[南宋末年的政治人物和經史學者以及文字學家](../Page/南宋.md "wikilink")。

## 生平

南宋[嘉定十六年](../Page/嘉定.md "wikilink")（1223年），七月十九日生於慶元府鄞縣（今寧波市鄞州區），父[王撝是](../Page/王撝.md "wikilink")[樓昉學生](../Page/樓昉.md "wikilink")，曾任[溫州知州](../Page/溫州.md "wikilink")，從小受其培養教育。

[淳祐元年](../Page/淳祐.md "wikilink")（1241年）[進士](../Page/進士.md "wikilink").
淳祐三年,
21歲在衢州任[主薄](../Page/主薄.md "wikilink")，受到程朱學派[王埜](../Page/王埜.md "wikilink")、[真德秀等人影響](../Page/真德秀.md "wikilink")，任官同時勤於讀經史.

寶祐四年(1256年)考上[博學宏詞科](../Page/博學宏詞.md "wikilink")，官至[禮部尚書兼](../Page/禮部尚書.md "wikilink")[给事中](../Page/给事中.md "wikilink")。時蒙古入侵，內有權臣[丁大全](../Page/丁大全.md "wikilink")、[賈似道等主政](../Page/賈似道.md "wikilink")，他曾上書論邊防和批判當時政治。

祥興二年（1279年）, 二月宋亡後, 他在家鄉隱居講述經史二十年。宋亡後不出，作品只寫甲子不寫年號，以示不稱臣。

元成宗元貞二年（1296年）, 六月十二日卒。

## 著作

其著作學甚多且學術價值甚高，到[清朝時才開始較為人所重視](../Page/清朝.md "wikilink")，其中《[玉海](../Page/玉海.md "wikilink")》為[类书](../Page/类书.md "wikilink")，為其準備博學宏詞考試時所整理的。《[困學紀聞](../Page/困學紀聞.md "wikilink")》是[筆記類的著作](../Page/筆記.md "wikilink")，集合其大量經史研究的心得成果。《漢制考》為歷史著作。《[通鑑地理通釋](../Page/通鑑地理通釋.md "wikilink")》是[歷史地理學的著作](../Page/歷史地理學.md "wikilink")。《[小學绀珠](../Page/小學绀珠.md "wikilink")》則是關於[文字學的类书](../Page/文字學.md "wikilink")。

  - 另說王应麟是《[三字經](../Page/三字經.md "wikilink")》、《[百家姓](../Page/百家姓.md "wikilink")》之作者\[1\]，但並無足夠之證據可供考證\[2\]。

## 評價

  - [王鸣盛在](../Page/王鸣盛.md "wikilink")《十七史商榷·汉艺文志考证》中说：“王应麟《汉艺文志考证》十卷，所采掇亦甚博雅。”
  - [章學誠指出](../Page/章學誠.md "wikilink")：“今之博雅君子，疲惫精力于经传子史，而终身无得于学者，正坐宗仰王氏，而误执求知之功力，以为学即在是耳。”\[3\]

## 注釋

<div class="references-small">

<references />

</div>

[W](../Page/category:宋朝政治人物.md "wikilink")
[W](../Page/category:宋朝作家.md "wikilink")
[W](../Page/category:宋朝歷史學家.md "wikilink")
[W](../Page/category:浙东史学.md "wikilink")
[W](../Page/category:宋朝经学家.md "wikilink")
[category:淳祐元年辛丑科進士](../Page/category:淳祐元年辛丑科進士.md "wikilink")
[W](../Page/category:鄞县人.md "wikilink")
[Y](../Page/category:王姓.md "wikilink")

1.  清代贺兴思《〈三字经〉注解备要叙》：“宋儒王伯厚先生《三字经》一出，海内外子弟之发蒙者，咸恭若球刀。”
2.  《三字经》作者是何人大致有四种說法：一、明代[黄佐](../Page/黄佐.md "wikilink")《广州人物传》卷十，明末[屈大均](../Page/屈大均.md "wikilink")《广东新语》卷十一，清代[恽敬](../Page/恽敬.md "wikilink")《大云山房记》卷二，都以为作者应是宋末[区适子](../Page/区适子.md "wikilink")；二、清代邵晋涵诗：“读得贞黎三字训”，自注：“《三字经》，南海黎贞撰。”即明代[黎贞撰](../Page/黎贞.md "wikilink")；三、也有人說是区适子所撰，黎贞增广之说；四、世传王应麟撰，這是最普遍的說法。
3.  章学诚《章氏遗书》卷2，〈文史通义内篇〉二，《博约中》