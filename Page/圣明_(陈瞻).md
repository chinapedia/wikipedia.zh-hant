**圣明**（506年正月-七月）是[北魏時期泾州](../Page/北魏.md "wikilink")（今[甘肃](../Page/甘肃.md "wikilink")[泾川](../Page/泾川.md "wikilink")）[屠各族領導](../Page/屠各族.md "wikilink")[陈瞻的](../Page/陈瞻.md "wikilink")[年号](../Page/年号.md "wikilink")，共计數月。

## 大事记

## 出生

## 逝世

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|圣明||元年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||[506年](../Page/506年.md "wikilink")
|- style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[丙戌](../Page/丙戌.md "wikilink")
|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|}

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [天監](../Page/天監.md "wikilink")（[502年四月](../Page/502年.md "wikilink")—[519年十二月](../Page/519年.md "wikilink")）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [建明](../Page/建明_\(呂苟兒\).md "wikilink")（[506年正月](../Page/506年.md "wikilink")-七月）：[北魏時期](../Page/北魏.md "wikilink")[呂苟兒](../Page/呂苟兒.md "wikilink")、[王法智年号](../Page/王法智.md "wikilink")
      - [正始](../Page/正始_\(北魏宣武帝\).md "wikilink")（[504年正月](../Page/504年.md "wikilink")-[508年八月](../Page/508年.md "wikilink")）：[北魏政权](../Page/北魏.md "wikilink")[北魏宣武帝元恪年号](../Page/北魏宣武帝.md "wikilink")
      - [始平](../Page/始平.md "wikilink")（[506年](../Page/506年.md "wikilink")-[507年](../Page/507年.md "wikilink")）：[柔然政权佗汗可汗](../Page/柔然.md "wikilink")[郁久闾伏图年号](../Page/郁久闾伏图.md "wikilink")
      - [承平](../Page/承平_\(麴嘉\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴嘉年号](../Page/麴嘉.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北魏民变政权年号](../Category/北魏民变政权年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:500年代中国政治](../Category/500年代中国政治.md "wikilink")
[Category:506年](../Category/506年.md "wikilink")