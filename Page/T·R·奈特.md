**TR奈特**（，），本名西奥多·雷蒙·奈特（），是一位[美國](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")。出生於[明尼苏达州的](../Page/明尼苏达州.md "wikilink")[明尼阿波利斯](../Page/明尼阿波利斯.md "wikilink")，從五歲起，他就在當地的[格思里劇院](../Page/格思里劇院.md "wikilink")（Guthrie
Theater）中演出，展開演員生涯。他曾在[ABC頻道的影集](../Page/美國廣播公司.md "wikilink")《[實習醫生](../Page/實習醫生_\(電視劇\).md "wikilink")》中飾演[喬治·歐麥立一角](../Page/喬治·歐麥立.md "wikilink")。他也經常在其他影集中客串演出。此外，奈特也是一位經驗豐富的舞台演員。高中畢業後，奈特並沒有繼續就讀大學，而是到包括格思里在內的多間劇院學習演出技巧。之後他搬到[紐約](../Page/紐約.md "wikilink")，並在那裡參與[百老匯的舞台演出](../Page/百老匯.md "wikilink")。他最近期的演出是在2003年間，於舞台劇《[偽善者](../Page/偽善者.md "wikilink")》（*Tartuffe*）中飾演達米斯（Damis）一角。他也曾在2003年獲得「[劇評家獎](../Page/劇評家獎.md "wikilink")」（Drama
Desk Award）的提名。

## 参考资料

## 外部連結

  -
  -
[Category:美國舞台男演員](../Category/美國舞台男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:男同性戀演員](../Category/男同性戀演員.md "wikilink")
[Category:美國同性戀者](../Category/美國同性戀者.md "wikilink")
[Category:明尼蘇達州人](../Category/明尼蘇達州人.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:明尼蘇達大學校友](../Category/明尼蘇達大學校友.md "wikilink")
[Category:美國天主教徒](../Category/美國天主教徒.md "wikilink")