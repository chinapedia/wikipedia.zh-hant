[缩略图](https://zh.wikipedia.org/wiki/File:Chinese_orchestra_in_Edmonton_2.jpg "fig:缩略图")
**中樂團（香港）**，又稱為**華樂團（馬來西亞、新加坡）**、**國樂團（台灣）**或**民族管弦樂團（簡稱民樂，中國）**，是中國近代以[中國民族樂器為基礎的大型合奏形式](../Page/中國民族樂器.md "wikilink")，編制模仿[西洋管弦樂團而成](../Page/管弦樂團.md "wikilink")。

## 名稱

關於中樂團在中文地區各地有數個不同的習稱，可參考[中国民族音乐](../Page/中国民族音乐#名稱.md "wikilink")。[台灣初年因](../Page/台灣.md "wikilink")[中華民國政府提倡](../Page/中華民國.md "wikilink")，亦稱**國樂團**。在[中國大陸稱為](../Page/中國大陸.md "wikilink")**民樂團**或**民族管弦樂團。**現今在[香港稱為](../Page/香港.md "wikilink")**中樂團**，在[東南亞](../Page/東南亞.md "wikilink")（特別是馬來西亞和新加坡）稱為**華樂團**，亦有人認為應該稱為**現代中華管弦樂團**（）以與中國傳統合奏區別。

## 沿革

這種中國大型器樂合奏是二十世紀才漸漸發展而成的新型中國器樂合奏形式，可謂「既不民間，亦不傳統」，形成後卻得到廣泛傳播及官方支持（如中華民國稱之為國樂團）。一些學者提出民族管弦樂團是一種「泛中國」的表演形式。\[1\]

在1920年代在[上海](../Page/上海.md "wikilink")，由[鄭覲文等人成立了](../Page/鄭覲文.md "wikilink")[大同樂會](../Page/大同樂會.md "wikilink")，致力於傳統樂器改造和創新，並組成了一個樂隊，分爲吹、彈、拉、打4組，當時樂團編制約30人。大同樂會根據中國傳統音樂改編一批適合於這種新型樂隊演奏的合奏作品，其中最爲著名的是由鄭覲文、[柳堯章根據](../Page/柳堯章.md "wikilink")[琵琶曲](../Page/琵琶.md "wikilink")《[潯陽夜月](../Page/潯陽夜月.md "wikilink")》改編而成的合奏曲《[春江花月夜](../Page/春江花月夜.md "wikilink")》。

1935年，[中央廣播電臺國樂團成立於](../Page/中廣國樂團.md "wikilink")[南京](../Page/南京.md "wikilink")，為中國最早的正式國樂團，後來在[對日抗戰期間遷往](../Page/中國抗日戰爭.md "wikilink")[重慶](../Page/重慶.md "wikilink")，一直努力於樂團編制的改進以及樂器的改革，包括音域的增加、中、低音胡琴的改革等。1949年以後，中國廣播國樂團隨[中華民國政府遷往](../Page/中華民國.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")。[台灣大學於](../Page/台灣大學.md "wikilink")1952年，師範大學於1953年成立國樂社。

中國大陸於1952年成立了最早的大型民族樂團[上海民族樂團](../Page/上海民族樂團.md "wikilink")。1953年，[中國廣播民族樂團在](../Page/中國廣播民族樂團.md "wikilink")[北京成立](../Page/北京.md "wikilink")，在[彭修文等人的努力下](../Page/彭修文.md "wikilink")，在樂團聲部編制、樂曲創作，以及樂器的改進上都有很大的成績，中國廣播民族樂團的編制，成為後來中樂團的基礎編制，至今未有重大的改變。

## 中樂團的特色

現代中樂團編制，一般分為由[彭修文訂立的吹](../Page/彭修文.md "wikilink")、拉、彈、打四組。以[胡琴對應交響樂團的](../Page/胡琴.md "wikilink")[小提琴](../Page/小提琴.md "wikilink")、[中提琴](../Page/中提琴.md "wikilink")，並以[大提琴或](../Page/大提琴.md "wikilink")[革胡及](../Page/革胡.md "wikilink")[低音提琴或](../Page/低音提琴.md "wikilink")[低音革胡代替早期的](../Page/低音革胡.md "wikilink")[低胡以加強](../Page/低胡.md "wikilink")[低音聲部](../Page/低音聲部.md "wikilink")。[嗩吶和](../Page/嗩吶.md "wikilink")[笙對應](../Page/笙.md "wikilink")[銅管及](../Page/銅管.md "wikilink")[木管樂](../Page/木管樂.md "wikilink")。打擊樂器同時使用[堂鼓](../Page/堂鼓.md "wikilink")、[定音鼓](../Page/定音鼓.md "wikilink")、[軍鼓等中](../Page/軍鼓.md "wikilink")、西樂器。最大的特色在於[彈撥樂器的大量使用](../Page/彈撥樂器.md "wikilink")，以及大量中國特色的[打擊樂器的使用](../Page/打擊樂器.md "wikilink")。

中樂團一般指60人編制的樂團，演奏大型合奏曲（又稱作民族管絃樂曲）及大型[協奏曲為主](../Page/協奏曲.md "wikilink")，部份中樂團同時設有[絲竹樂團](../Page/絲竹樂團.md "wikilink")，以大約10-30人的較小型編制，來演奏傳統絲竹樂或新作的小型合奏曲。

中樂團演奏的大型合奏曲，在旋律上大多是取材於中國傳統的[民歌](../Page/民歌.md "wikilink")、[戲曲](../Page/戲曲.md "wikilink")、[器樂獨奏](../Page/器樂.md "wikilink")（如[古琴](../Page/古琴.md "wikilink")、[箏](../Page/箏.md "wikilink")、[琵琶等](../Page/琵琶.md "wikilink")）及絲竹曲（如[廣東音樂](../Page/廣東音樂.md "wikilink")、[江南絲竹](../Page/江南絲竹.md "wikilink")）中的旋律，或依照中國音樂中常用的[五聲音階為基礎所新創的旋律](../Page/五聲音階.md "wikilink")；但在作曲方式上，則主要採用西方交響樂中的[曲式](../Page/曲式.md "wikilink")、[調性](../Page/調性.md "wikilink")、[配器法](../Page/配器法.md "wikilink")、[合聲](../Page/合聲.md "wikilink")、[對位等作曲的原則](../Page/對位.md "wikilink")。

和傳統絲竹合奏比較，現代中樂團除了編制較大之外，有以下幾點不同：

  - 傳統絲竹合奏以單線條旋律加花演奏，形成支聲複調；在現代中樂團的聲部則以和聲造成音樂織體。
  - 傳統絲竹合奏沒有總譜、沒有專責的指揮，往往由[笛子](../Page/笛子.md "wikilink")、[高胡](../Page/高胡.md "wikilink")，或[掌板擔當領導的角色](../Page/掌板.md "wikilink")；現代中樂團由指揮領導演奏
  - 傳統樂器以高中音為主，缺少低音樂器，演出的編制有彈性，並非固定；現代中樂團模仿交響樂團分為吹、彈、打、拉4聲部。樂團的整體音域較大，每個樂器家族都有高、中、低音樂器，並且應用近代發明的低音鍵笙、低音嗩吶、革胡等等樂器。
  - 傳統絲竹合奏以[五聲音階為主](../Page/五聲音階.md "wikilink")，有時運用乙反調的[七聲音階](../Page/七聲音階.md "wikilink")；現代中樂團偏向以[十二平均律為標準](../Page/十二平均律.md "wikilink")，不少改革樂器能演奏[半音音階](../Page/半音音階.md "wikilink")，亦引入功能和聲，以及20世紀的現代音樂寫作手法。
  - 很多傳統樂曲是藉著不停的演奏，口傳心授，流派風格的演變而來，並沒有專責作曲的概念；現代不少中樂團都會委約作曲家專為樂團創作新作品。

## 一般編制

[Chinese_orchestra_3_erhu_players.jpg](https://zh.wikipedia.org/wiki/File:Chinese_orchestra_3_erhu_players.jpg "fig:Chinese_orchestra_3_erhu_players.jpg")
[Plucked_strings_in_a_Chinese_orchestra.jpg](https://zh.wikipedia.org/wiki/File:Plucked_strings_in_a_Chinese_orchestra.jpg "fig:Plucked_strings_in_a_Chinese_orchestra.jpg")
[Dizi_players_in_a_Chinese_orchestra.jpg](https://zh.wikipedia.org/wiki/File:Dizi_players_in_a_Chinese_orchestra.jpg "fig:Dizi_players_in_a_Chinese_orchestra.jpg")
一般專業樂團的人數大約60-80人左右。

  - [拉弦樂器](../Page/拉弦樂器.md "wikilink")

<!-- end list -->

  - [高胡](../Page/高胡.md "wikilink")（約3-7人）
  - [二胡](../Page/二胡.md "wikilink")（約10-20人；常分為二胡I及二胡II，類似於西洋交響樂團的小提琴）
  - [中胡](../Page/中胡.md "wikilink")（約3-5人）
  - [大提琴](../Page/大提琴.md "wikilink")（或[革胡](../Page/革胡.md "wikilink")；6-10人）
  - [低音提琴](../Page/低音提琴.md "wikilink")（或[低音革胡](../Page/低音革胡.md "wikilink")；3-5人）

<!-- end list -->

  - [彈撥樂器](../Page/彈撥樂器.md "wikilink")

<!-- end list -->

  - [柳琴](../Page/柳琴.md "wikilink")（約2-4人）
  - [琵琶](../Page/琵琶.md "wikilink")（約4-6人）
  - [中阮](../Page/阮_\(乐器\).md "wikilink")（約4-6人）
  - [大阮](../Page/阮_\(乐器\).md "wikilink")（約4-6人）
  - [揚琴](../Page/揚琴.md "wikilink")（2-4人；揚琴雖然在原理上是[擊絃樂器](../Page/擊絃樂器.md "wikilink")，但在聲部的作用上與彈撥樂器相同）
  - [三弦](../Page/三弦.md "wikilink")（1人；部分曲目使用，通常由阮咸演奏員兼任）
  - [古箏](../Page/古箏.md "wikilink")（1人；部分曲目使用）
  - [箜篌](../Page/箜篌.md "wikilink")（1人；部分曲目使用）
  - [豎琴](../Page/豎琴.md "wikilink")（1人；部分曲目使用）

<!-- end list -->

  - [吹管樂器](../Page/吹管樂器.md "wikilink")

<!-- end list -->

  - [梆笛](../Page/梆笛.md "wikilink")（2-3人）
  - [曲笛](../Page/曲笛.md "wikilink")（2-3人）
  - [新笛](../Page/新笛.md "wikilink")（1-2人）
  - [高音笙](../Page/笙.md "wikilink")（2-3人）
  - [中音笙](../Page/笙.md "wikilink")（2-3人）
  - [低音笙](../Page/笙.md "wikilink")（1-2人）
  - [高音嗩吶](../Page/高音嗩吶.md "wikilink")（2-3人）
  - [中音嗩吶](../Page/中音嗩吶.md "wikilink")（2-3人）
  - [次中音嗩吶](../Page/次中音嗩吶.md "wikilink")（1-2人；部分曲目使用）
  - [低音嗩吶](../Page/低音嗩吶.md "wikilink")（1人）
  - [高音管](../Page/管子_\(乐器\).md "wikilink")（1-2人；部分曲目使用）
  - [中音管](../Page/管子_\(乐器\).md "wikilink")（1-2人；部分曲目使用）
  - [低音管](../Page/管子_\(乐器\).md "wikilink")（1-2人；部分曲目使用）

<!-- end list -->

  - [打擊樂器](../Page/打擊樂器.md "wikilink")

共約3-10人，通常1人演奏多項樂器。中樂團使用的打擊樂器包括中華民族打擊樂器及西洋打擊樂器，如：[定音鼓](../Page/定音鼓.md "wikilink")、[大軍鼓](../Page/大軍鼓.md "wikilink")、[小軍鼓](../Page/小軍鼓.md "wikilink")、[大堂鼓](../Page/大堂鼓.md "wikilink")、[小堂鼓](../Page/小堂鼓.md "wikilink")、[排鼓](../Page/排鼓.md "wikilink")、[板鼓](../Page/板鼓.md "wikilink")、[鈴鼓](../Page/鈴鼓.md "wikilink")、[康佳鼓](../Page/康佳鼓.md "wikilink")、[邦哥鼓](../Page/邦哥鼓.md "wikilink")、[嗵嗵鼓](../Page/嗵嗵鼓.md "wikilink")、[新疆手鼓](../Page/新疆手鼓.md "wikilink")、[馬林巴](../Page/馬林巴.md "wikilink")、[木琴](../Page/木琴.md "wikilink")、[鍾琴](../Page/鍾琴.md "wikilink")、[顫音琴](../Page/顫音琴.md "wikilink")、[管鐘](../Page/管鐘.md "wikilink")、[編鐘](../Page/編鐘.md "wikilink")、[西洋大鈸](../Page/西洋大鈸.md "wikilink")、[吊鈸](../Page/吊鈸.md "wikilink")、[中國大鈸](../Page/中國大鈸.md "wikilink")、[中國小鈸](../Page/中國小鈸.md "wikilink")、[大鑼](../Page/大鑼.md "wikilink")、[小鑼](../Page/小鑼.md "wikilink")、[泰來鑼](../Page/泰來鑼.md "wikilink")、[風鑼](../Page/風鑼.md "wikilink")、[雲鑼](../Page/雲鑼.md "wikilink")、[十面鑼](../Page/十面鑼.md "wikilink")、[西洋木魚](../Page/西洋木魚.md "wikilink")、[中國木魚](../Page/中國木魚.md "wikilink")、[沙錘](../Page/沙錘.md "wikilink")、[彈簧盒](../Page/彈簧盒.md "wikilink")、[音樹](../Page/音樹.md "wikilink")、[三角鐵](../Page/三角鐵.md "wikilink")、[響板](../Page/響板.md "wikilink")、[牛鈴](../Page/牛鈴.md "wikilink")、[碰鈴](../Page/碰鈴.md "wikilink")、[馬鈴](../Page/馬鈴.md "wikilink")、[鐵沙鈴](../Page/鐵沙鈴.md "wikilink")、[雨聲器](../Page/雨聲器.md "wikilink")、[刮瓜](../Page/刮瓜.md "wikilink")
等。

## 職業樂團

  - 中國大陆

<!-- end list -->

  - [中國廣播民族樂團](../Page/中國廣播民族樂團.md "wikilink")
  - [中央民族樂團](../Page/中央民族樂團.md "wikilink")
  - [上海民族樂團](../Page/上海民族樂團.md "wikilink")
  - [濟南軍區前衛民族樂團](../Page/濟南軍區前衛民族樂團.md "wikilink")
  - [中國歌舞劇院民族樂團](../Page/中國歌舞劇院民族樂團.md "wikilink")
  - [廣東民族樂團](../Page/廣東民族樂團.md "wikilink")
  - [成都民族樂團](../Page/成都民族樂團.md "wikilink")
  - [浙江省歌舞團民族樂團](../Page/浙江省歌舞團民族樂團.md "wikilink")
  - [中国青年民族乐团](../Page/中国青年民族乐团.md "wikilink")

<!-- end list -->

  - 臺灣

<!-- end list -->

  - [國家國樂團](../Page/國家國樂團.md "wikilink")
  - [臺北市立國樂團](../Page/臺北市立國樂團.md "wikilink")
  - [桃園市國樂團](../Page/桃園市國樂團.md "wikilink")
  - [高雄市國樂團](../Page/高雄市國樂團.md "wikilink")
  - [台南市立民族管絃樂團](../Page/台南市立民族管絃樂團.md "wikilink")

<!-- end list -->

  - 香港

<!-- end list -->

  - [香港中樂團](../Page/香港中樂團.md "wikilink")

<!-- end list -->

  - 澳門

<!-- end list -->

  - [澳門中樂團](../Page/澳門中樂團.md "wikilink")

<!-- end list -->

  - 新加坡

<!-- end list -->

  - [新加坡華樂團](../Page/新加坡華樂團.md "wikilink")

## 業餘樂團

  - 中國內地

<!-- end list -->

  - [中国人民大学民乐团](../Page/中国人民大学民乐团.md "wikilink")
  - [清华大学民乐团](../Page/清华大学民乐团.md "wikilink")
  - [北京大学民乐团](../Page/北京大学民乐团.md "wikilink")
  - [四川大学民乐团](../Page/四川大学民乐团.md "wikilink")
  - [天津大學北洋民樂團](../Page/天津大學北洋民樂團.md "wikilink")
  - [中国地质大学（武汉）民乐团](../Page/中国地质大学（武汉）民乐团.md "wikilink")
  - [北京十二中民乐团](../Page/北京十二中民乐团.md "wikilink")
  - [北京翠微小学民乐团](../Page/北京翠微小学民乐团.md "wikilink")

<!-- end list -->

  - 澳門

<!-- end list -->

  - [澳門青韻中樂團](../Page/澳門青韻中樂團.md "wikilink")

<!-- end list -->

  - 台灣

<!-- end list -->

  - [台北青年國樂團](../Page/台北青年國樂團.md "wikilink")
  - [九歌民族管絃樂團](../Page/九歌民族管絃樂團.md "wikilink")
  - [中華國樂團](../Page/中華國樂團.md "wikilink")
  - [台大薰風國樂團](../Page/台大薰風國樂團.md "wikilink")
  - [國立成功大學國樂團](../Page/國立成功大學國樂團.md "wikilink")
  - [新竹縣立國樂團](../Page/新竹縣立國樂團.md "wikilink")
  - [新樂國樂團](../Page/新樂國樂團.md "wikilink")
  - [桃園樂友絲竹室內樂團](../Page/桃園樂友絲竹室內樂團.md "wikilink")
  - [新竹青年國樂團](../Page/新竹青年國樂團.md "wikilink")
  - [新竹青少年國樂團](../Page/新竹青少年國樂團.md "wikilink")

<!-- end list -->

  - 香港

<!-- end list -->

  - [香港愛樂民樂團](../Page/香港愛樂民樂團.md "wikilink")
  - [香港青年音樂協會](../Page/香港青年音樂協會.md "wikilink")
  - [宏光國樂團](../Page/宏光國樂團.md "wikilink")
  - [新聲國樂團](../Page/新聲國樂團.md "wikilink")
  - [香港青年中樂團](../Page/香港青年中樂團.md "wikilink")
  - 新界青年中樂團
  - [香港女青中樂團](../Page/香港女青中樂團.md "wikilink")
  - [香港樂樂國樂團](../Page/香港樂樂國樂團.md "wikilink")
  - [香港青少年國樂團](../Page/香港青少年國樂團.md "wikilink")
  - [喇沙書院中樂團](../Page/喇沙書院中樂團.md "wikilink")
  - [庇理羅士女子中樂團](../Page/庇理羅士女子中樂團.md "wikilink")
  - [拔萃男書院國樂會](../Page/拔萃男書院國樂會.md "wikilink")
  - [佛教青年協會中樂團](../Page/佛教青年協會中樂團.md "wikilink")

<!-- end list -->

  - 加拿大

<!-- end list -->

  - [庇詩中樂團](../Page/庇詩中樂團.md "wikilink")
  - [多倫多中樂團](../Page/多倫多中樂團.md "wikilink")

<!-- end list -->

  - 美國

<!-- end list -->

  - [加州青年國樂團](../Page/加州青年國樂團.md "wikilink")
  - [火鳳青年國樂團](../Page/火鳳青年國樂團.md "wikilink")

<!-- end list -->

  - 澳大利亞

<!-- end list -->

  - [江河中樂社](../Page/江河中樂社.md "wikilink")

<!-- end list -->

  - 马来西亚

<!-- end list -->

  - [艺演华乐团](../Page/艺演华乐团.md "wikilink")
  - [大乐乐创意音乐工作室](http://www.vivomusicbox.com/)
  - [专艺民族乐团](../Page/专艺民族乐团.md "wikilink")
  - [南藝華樂團](https://www.facebook.com/SouthernChineseOrchestra/)

<!-- end list -->

  - 菲律宾

<!-- end list -->

  - [菲律宾侨中学院民乐团](../Page/菲律宾侨中学院民乐团.md "wikilink")

## 著名作品

大型合奏曲：

  - [趙季平](../Page/趙季平.md "wikilink")
      - 第二交響樂《和平頌》
          - I：《金陵·大江》
          - II：《江淚》
          - III：《江怨》
          - IV：《江怒》
          - V：《和平頌》
      - 《大漠孤煙直》組曲
          - II：音詩《覓》
          - IV：《悼歌》
      - 《慶典序曲》
      - 《古槐尋根》（交響樂《華夏之根》第六樂章）
      - 《大宅門寫意－蘆溝曉月》
  - [關迺忠](../Page/關迺忠.md "wikilink")
      - 《[拉薩行](../Page/拉薩行.md "wikilink")》
          - I：《布達拉宮》
          - II：《雅魯藏布江》
          - III：《天葬》
          - IV：《打鬼》
      - 《豐年祭》
      - 《祈雨》
  - 金湘
      - 交響音畫《塔克拉瑪幹掠影》
          - I：《漠原》
          - II：《漠樓》
          - III：《漠舟》
          - IV：《漠洲》
  - 景建樹
      - 《堯天舜日》（交響樂《華夏之根》第一樂章）
      - 《晉商情懷》（交響樂《華夏之根》第五樂章）
  - [王立平](../Page/王立平.md "wikilink")
      - 《紅樓夢組曲》
          - I：《序曲》
          - II：《紫菱洲歌》
          - III：《紅豆曲》
          - IV：《晴雯歌》
          - V：《大出殯》
          - VI：《劉姥姥》
          - VII：《題帕三絕》
          - VIII：《枉凝眉》
          - IX：《分骨肉》
          - X：《嘆香菱》
          - XI：《聰明累》
          - XII：《寶黛情》
          - XIII：《上元節》
          - XIV：《秋窗風雨夕》
          - XV：《葬花吟》
  - [盧亮輝](../Page/盧亮輝.md "wikilink")
      - 《[春](../Page/春_\(樂曲\).md "wikilink")》
      - 《夏》
      - 《秋》
      - 《冬》
      - 《[酒歌](../Page/酒歌.md "wikilink")》（又名「彝族酒歌」）
      - 《[童年的回憶](../Page/童年的回憶.md "wikilink")》
      - 《羽調》
  - 姜瑩
      - 樂劇《印象國樂》
          - I：《小鳥樂》
          - II：《前世今生》
          - III：《大曲》
      - 《絲綢之路》（又名「庫姆塔格」）
  - [谭盾](../Page/谭盾.md "wikilink")
      - 《[西北組曲](../Page/西北組曲.md "wikilink")》（又名「西北第一組曲」、「黃土地組曲」）
          - I：《老天爺下甘雨》
          - II：《鬧洞房》
          - III：《想親親》
          - IV：《石板腰鼓》
  - 劉長遠
      - 《抒情變奏曲》
          - I：第一樂章
          - II：第二樂章
          - III：第三樂章
      - 《憶》
  - 程大兆
      - 《雲岡印象》（交響樂《華夏之根》第四樂章）
      - 《黃河暢想》（交響樂《華夏之根》第七樂章）
  - [彭修文](../Page/彭修文.md "wikilink")
      - 第一交響樂《金陵》
          - I：《懷古》
          - II：《秦淮》
          - III：《滄桑》
      - 幻想曲《秦·兵馬俑》
  - [蘇文慶](../Page/蘇文慶.md "wikilink")
      - 《[臺灣追想曲](../Page/臺灣追想曲.md "wikilink")》
      - 《風獅爺傳奇》
      - 《山海印象》
      - 《[噶瑪蘭幻想曲](../Page/噶馬蘭.md "wikilink")》
  - [郭文景](../Page/郭文景.md "wikilink")
      - 《滇西土風三首》
          - I：《阿佤山》
          - II：《基諾舞》
          - III：《祭祀－火把－烈酒》
  - 劉湲
      - 音詩《沙迪爾傳奇》（又名「維吾爾音詩」）
      - 交響幻想史詩《馬可波羅與卜魯罕公主》
          - I：《盛朝大典》
          - II：《馬可波羅與卜魯罕公主》
          - III：《陸路風情》
          - IV：《海路風險》
          - V：《土著之舞》
          - VI：《東方伊甸樂園》
          - VII：《地久天長》
  - 唐建平
      - 《后土》
      - 《新世紀音樂會序曲》
  - [劉文金](../Page/劉文金.md "wikilink")
      - 《長城隨想》（原為四樂章二胡協奏曲，作曲家後為民族管弦樂團編為單樂章作品）
      - 《[太行印象](../Page/太行印象.md "wikilink")》
      - 《難忘的潑水節》
  - [顧冠仁](../Page/顧冠仁.md "wikilink")
      - 《春天組曲》
          - I：《杜鵑花開》
          - II：《駿馬奔馳》
          - III：《苗嶺春早》
          - IV：《水鄉綠野》
          - V：《天山聖會》
  - 韓蘭魁
      - 《鹽池勞作圖》（交響樂《華夏之根》第二樂章）
  - [劉錫津](../Page/劉錫津.md "wikilink")
      - 《靺鞨組曲》
          - I：《武士》
          - II：《公主》
          - III：《百戲童》
          - IV：《酒舞》
          - V：《樺林大戰》
          - VI：《踏垂舞》
  - [何訓田](../Page/何訓田.md "wikilink")
      - 《達勃河隨想曲》
          - I：第一樂章
          - II：第二樂章
  - [劉星](../Page/劉星.md "wikilink")
      - 《邊巴》
  - [羅偉倫](../Page/羅偉倫.md "wikilink")
      - 幻想交響史詩《海上第一人－鄭和》
          - I：《海葬》
          - II：《海魂》
          - III：《海路》
          - IV：《海緣》
          - V：《海險》
          - VI：《海誓》
      - 詩樂《天網》
  - 張堅
      - 《晉國雄風》（交響樂《華夏之根》第三樂章）
  - [陳能濟](../Page/陳能濟.md "wikilink")
      - 《城寨風情》組曲
          - I：《城寨之歌》
          - II：《三寸金蓮》
          - III：《月荷之歌》
          - IV：《海盜之歌》
          - V：《何處覓知心》
      - 交響詩《赤壁》
          - I：序曲《大江東去》
          - II：《臉譜》
          - III：《好戲連場》
          - IV：《赤壁之戰》
      - 《龍的傳奇》
      - 《夢蝶》
  - 王丹紅
      - 《太陽頌》
          - I：《踏江》
          - II：《挑山》
          - III：《思念》
          - IV：《太陽頌》
      - 《弦上秧歌》
      - 《雲山雁邈》
      - 《澳門隨想曲》
          - I：《教堂之光》
          - II：《葡萄牙節日》
          - III：《漁歌》
          - IV：《憧憬》
          - V：《焰火》
  - 張朝
      - 《干將·莫邪幻想曲》
          - I：《誓言》
          - II：《山雪》
          - III：《烈火》
          - IV：《涅槃》
  - [徐景新](../Page/徐景新.md "wikilink")、[陳大偉](../Page/陳大偉_\(中國音樂家\).md "wikilink")
      - 《[飛天](../Page/飛天_\(樂曲\).md "wikilink")》
  - 顧冠仁、馬聖龍
      - 《[東海漁歌](../Page/東海漁歌.md "wikilink")》

大型[協奏曲](../Page/協奏曲.md "wikilink")：

  - [劉星](../Page/劉星.md "wikilink")：《雲南回憶》中阮協奏曲
  - [關迺忠](../Page/關迺忠.md "wikilink")：《龍年新世紀》雙敲擊協奏曲
  - [劉文金](../Page/劉文金.md "wikilink")：《[長城隨想](../Page/長城隨想.md "wikilink")》二胡協奏曲
  - [盧亮輝](../Page/盧亮輝.md "wikilink")：《喜》笛子協奏曲、《怒》琵琶協奏曲、《哀》塤協奏曲、《樂》嗩吶協奏曲
  - [趙季平](../Page/趙季平.md "wikilink")：《絲綢之路幻想組曲》管子協奏曲
  - 王月明：《西域隨想》低音二胡協奏曲
  - 王丹紅：《狂想曲》揚琴協奏曲
  - [何占豪](../Page/何占豪.md "wikilink")：《[莫愁女幻想曲](../Page/莫愁女幻想曲.md "wikilink")》二胡協奏曲
  - [顧冠仁](../Page/顧冠仁.md "wikilink")：《花木蘭》琵琶協奏曲
  - [李煥之](../Page/李煥之.md "wikilink")：《汨羅江幻想曲》（箏協奏曲；原為西洋交響樂團伴奏，後改編為中樂團伴奏）
  - [蘇文慶](../Page/蘇文慶.md "wikilink")：《雨後庭院》柳琴協奏曲、《燕子》二胡協奏曲、《笑傲江湖》笙協奏曲
  - [劉锡津](../Page/劉锡津.md "wikilink")：《北方民族生活素描》月琴協奏曲
  - [張曉峰](../Page/張曉峰.md "wikilink")、朱曉谷：《新婚別》二胡協奏曲
  - [俞遜發](../Page/俞遜發.md "wikilink")、[瞿春泉](../Page/瞿春泉.md "wikilink")：《匯流》笛子協奏曲
  - [羅偉倫](../Page/羅偉倫.md "wikilink")、鄭濟民：《白蛇傳》笛子協奏曲

## 參考來源

## 外部連結

  -
[Category:音乐团体类型](../Category/音乐团体类型.md "wikilink")
[中國傳統音樂團體](../Category/中國傳統音樂團體.md "wikilink")
[Category:中國民族音樂](../Category/中國民族音樂.md "wikilink")

1.