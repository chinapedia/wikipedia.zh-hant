[合歡群峰.JPG](https://zh.wikipedia.org/wiki/File:合歡群峰.JPG "fig:合歡群峰.JPG")、遠處為[合歡北峰](../Page/合歡北峰.md "wikilink")。\]\]

**北合歡山**，又稱**合歡北峰**、**合歡山北峰**，為[台灣百岳之一](../Page/台灣百岳.md "wikilink")，排名第34。海拔高度3,422公尺，為[合歡群峰當中最高峰](../Page/合歡群峰.md "wikilink")，屬於[中央山脈的支稜](../Page/中央山脈.md "wikilink")，其[行政區劃是](../Page/台灣行政區劃.md "wikilink")[南投縣](../Page/南投縣.md "wikilink")[仁愛鄉與](../Page/仁愛鄉.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")[秀林鄉交界](../Page/秀林鄉.md "wikilink")。山頂立有二等三角點。其特色為[中橫公路](../Page/中橫公路.md "wikilink")[霧社支線經過](../Page/霧社支線.md "wikilink")，蠻容易攀爬，來回約4.7公里，約需要4個小時路程。登頂途中高山草原綿延不絕，景色壯麗；山頂視野遼闊，可以遠眺中央山脈與[雪山山脈](../Page/雪山山脈.md "wikilink")。主要有五個攝影點：[合歡北峰山腳(關原雲海咖啡上坡處)、小風口停車場、北峰登山口附近的崖壁杜鵑、枯木杜鵑、東坡杜鵑谷及碧池，
合歡北峰山腳(關原雲海咖啡上坡處)、小風口停車場、北峰登山口附近的崖壁杜鵑、枯木杜鵑、東坡杜鵑谷及碧池，](https://cristaljuan01.blogspot.com/2017/05/blog-post_28.html)

## 參考文獻

  - 楊建夫，《台灣的山脈》，2001年，臺北，遠足文化公司

<https://cristaljuan01.blogspot.com/2017/05/blog-post_28.html>

[Category:合歡群峰](../Category/合歡群峰.md "wikilink")
[Category:台灣百岳](../Category/台灣百岳.md "wikilink")