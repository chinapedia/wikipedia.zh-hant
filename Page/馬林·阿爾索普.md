[Marin_aslop.jpg](https://zh.wikipedia.org/wiki/File:Marin_aslop.jpg "fig:Marin_aslop.jpg")
**馬林‧阿爾索普**([英語](../Page/英語.md "wikilink")：****，)是一位[美國](../Page/美國.md "wikilink")[指揮家與](../Page/指揮家.md "wikilink")[小提琴家](../Page/小提琴家.md "wikilink")。

## 生平

阿爾索普出生於[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")[曼哈頓區的音樂世家](../Page/曼哈頓區.md "wikilink")，父母都是職業音樂家。

阿爾索普起先就讀[耶魯大學](../Page/耶魯大學.md "wikilink")，後來轉至[朱利亞德學院並獲得小提琴](../Page/朱利亞德學院.md "wikilink")[學士與](../Page/學士.md "wikilink")[碩士](../Page/碩士.md "wikilink")，於1981年成立「發燒弦樂團」(String
Fever)。她在位於[壇格塢音樂中心的](../Page/壇格塢.md "wikilink")1989[孔斯維茲基指揮大賽獲得首獎](../Page/孔斯維茲基.md "wikilink")，並在當地受教於[倫納德·伯恩斯坦](../Page/倫納德·伯恩斯坦.md "wikilink")、[小澤征爾](../Page/小澤征爾.md "wikilink")、[葛斯達夫·邁爾等人](../Page/葛斯達夫·邁爾.md "wikilink")。

阿爾索普自1991年起擔任[聖塔克魯茲](../Page/聖塔克魯茲_\(加利福尼亞州\).md "wikilink")[卡碧蘿音樂節的音樂總監](../Page/卡碧蘿音樂節.md "wikilink")，這個音樂節特別重視[現代音樂](../Page/現代音樂.md "wikilink")。她任職於[科羅拉多交響樂團十二年](../Page/科羅拉多交響樂團.md "wikilink")，從首席指揮升任至音樂總監，並獲得桂冠指揮家的榮譽。她也擔任[俄勒岡州](../Page/俄勒岡州.md "wikilink")[尤金市尤金交響樂團的音樂總監](../Page/尤金_\(俄勒岡州\).md "wikilink")，並在1988至1990年間擔任[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[里奇蒙市交響樂團的助理指揮](../Page/里奇蒙_\(維吉尼亞州\).md "wikilink")。

阿爾索普在英國擔任[皇家蘇格蘭國家管弦樂團及](../Page/皇家蘇格蘭國家管弦樂團.md "wikilink")[倫敦市室內管弦樂團的首席客席指揮](../Page/倫敦市室內管弦樂團.md "wikilink")，於2002年秋季被任命[伯恩茅斯交響樂團的首席指揮](../Page/伯恩茅斯.md "wikilink")，於2003年被票選為[留聲機雜誌的年度藝術家](../Page/留聲機雜誌.md "wikilink")，同年獲頒[皇家愛樂協會指揮家獎](../Page/皇家愛樂協會.md "wikilink")。她同[伯恩茅斯交響樂團的合約到](../Page/伯恩茅斯.md "wikilink")2008年為止，並不再續任。

2007年四月，阿爾索普成為八位參與十年音樂推廣計畫－「建設卓越：21世紀的管弦樂團」的英國指揮家之一，將在英國以贈送學童音樂會票劵的方式推廣古典音樂。

2005年七月，阿爾索普被提名為[巴爾的摩交響樂團](../Page/巴爾的摩交響樂團.md "wikilink")(BSO)第十二任音樂總監，使阿爾索普成為第二位帶領美國主要樂團的女性指揮。2006-2007樂季，阿爾索普就以準音樂總監的身分帶領樂團，而07-08樂季則正式成為音樂總監，同時是該團第一位女性音樂總監。這次任命是美國25大交響樂團第一次任命女指揮，而成為一個歷史性的紀錄。許多團員在任命之初，對於阿爾索普有明顯的反對態度，然而經過磨合，阿爾索普目前與巴爾的摩交響樂團有相當成功的合作。

2005年九月20日，阿爾索普成為第一位獲得[麥克阿瑟獎的指揮家](../Page/麥克阿瑟#麥克阿瑟獎.md "wikilink")。

阿爾索普對於美國音樂的擁護與詮釋均是為人稱道的，但她對於傳統曲目也有獨到之處。她是第一位錄製[布拉姆斯交響曲全集](../Page/布拉姆斯.md "wikilink")(在[Naxos下與](../Page/Naxos.md "wikilink")[倫敦愛樂交響樂團錄製](../Page/倫敦愛樂交響樂團.md "wikilink"))和[馬勒交響曲](../Page/古斯塔夫·馬勒.md "wikilink")(與[倫敦交響樂團錄製](../Page/倫敦交響樂團.md "wikilink")[第五號交響曲](../Page/第五號交響曲_\(馬勒\).md "wikilink")，由LSO
Live發行)的女性指揮。

2007年十一月7日，阿爾索普獲得了[伯恩茅斯大學榮譽音樂博士學位](../Page/伯恩茅斯大學.md "wikilink")。

[Category:美國小提琴家](../Category/美國小提琴家.md "wikilink")
[Category:美國指揮家](../Category/美國指揮家.md "wikilink")
[Category:女指揮家](../Category/女指揮家.md "wikilink")