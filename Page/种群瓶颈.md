[Cuello_de_botella.jpg](https://zh.wikipedia.org/wiki/File:Cuello_de_botella.jpg "fig:Cuello_de_botella.jpg")

**种群瓶颈效应**或**人口瓶颈**是指某个种群的数量由于突然的災難所造成的死亡或不能[生育造成减少](../Page/生育.md "wikilink")50%以上或者[数量级减少的事件](../Page/数量级.md "wikilink")。种群瓶颈可能促成[遗传漂变](../Page/遗传漂变.md "wikilink")。种群瓶颈發生後，可能造成种群的[灭绝](../Page/灭绝.md "wikilink")，或种群恢复但僅存有限的[遺傳多樣性](../Page/遺傳多樣性.md "wikilink")。

## 动物

一個例子是[獵豹](../Page/獵豹.md "wikilink")（*Acinonyx
jubatus*），由於獵豹彼此之間的關係過於相近，獵豹之間的直接[皮膚移植已經不會引發任何](../Page/植皮.md "wikilink")[免疫反應](../Page/免疫反應.md "wikilink")。獵豹也遭受低[精子濃度](../Page/精子.md "wikilink")、低精子活動力及畸形精子鞭毛等問題\[1\]。科學家推測，獵豹約在一萬年前的上一個[冰河期中經歷了种群瓶颈](../Page/冰河期.md "wikilink")，加上長年[近亲交配](../Page/近亲交配.md "wikilink")，因而造成其[基因庫寡少情形](../Page/基因庫.md "wikilink")，使得族群對於疾病沒有什麼應變能力；如致命的[貓傳染性腹膜炎](../Page/貓傳染性腹膜炎.md "wikilink")（Feline
infectious
peritonitis），在[家貓中的致病率約](../Page/家貓.md "wikilink")1%－5%，但在獵豹中的發病率高達50%－60%。

根據2002年的一份研究報告，4万3000年前[大熊猫遭遇了种群瓶颈效应](../Page/大熊猫.md "wikilink")，造成现在濒于绝迹的状况\[2\]。

## 其他例子

  - [歐洲野牛](../Page/歐洲野牛.md "wikilink")（*Bison
    bonasus*）：現存的族群是由當初12頭個體繁衍而來。
  - [美洲野牛](../Page/美洲野牛.md "wikilink")（*Bison bison*）：在1890年代數量曾少於1000頭。
  - [北象海豹](../Page/北象海豹.md "wikilink")（*Mirounga
    angustirostris*）的數量在1890年左右曾經降至只剩30頭。
  - [敘利亞倉鼠](../Page/敘利亞倉鼠.md "wikilink")（*Mesocricetus
    auratus*）：今天几乎所有的人工饲养的敘利亞倉鼠都來自於1930年在敘利亞發現的一隻母鼠及其一窩幼鼠。
  - [查岛鸲鹟](../Page/查岛鸲鹟.md "wikilink")（*Petroica
    traversi*）：1980年代只餘5隻，其中有生育能力的母鳥只有一隻。
  - [列山島野鴨](../Page/列山島野鴨.md "wikilink")（*Anas
    laysanensis*）：1912年只存7隻成鳥和5隻幼鳥。
  - [毛里求斯隼](../Page/毛里求斯隼.md "wikilink")（*Falco punctatus*）：1974年只餘4隻。
  - [华南虎](../Page/华南虎.md "wikilink")：现存仅有人工饲养华南虎，全部是陆续捕获的8\~9只野生华南虎的后代。
  - [朱鹮](../Page/朱鹮.md "wikilink")：现存近2000只朱鹮，1982年最少时仅7只。
  - [柴犬](../Page/柴犬.md "wikilink"):
    柴犬在[第二次世界大戰期間因食物短缺和戰後](../Page/第二次世界大戰.md "wikilink")[犬瘟热爆發而近乎絕種](../Page/犬瘟热.md "wikilink")\[3\]

## 參見

  - [遺傳漂變](../Page/遺傳漂變.md "wikilink")
  - [奠基者效應](../Page/奠基者效應.md "wikilink")

## 参考文献

[Category:生物学](../Category/生物学.md "wikilink")
[Category:遗传学](../Category/遗传学.md "wikilink")

1.
2.
3.