**周國豐**（**Brian
Chow**，），1980年代曾是香港童星，2017元旦起接任[香港電台電台及節目策劃總監](../Page/香港電台.md "wikilink")，曾任[香港電台第二台及](../Page/香港電台第二台.md "wikilink")[香港電台普通話台台長](../Page/香港電台普通話台.md "wikilink")，也是一位[DJ](../Page/DJ.md "wikilink")、電視節目主持人。

## 家庭

### 婚姻

2005年2月，經[香港電台同事](../Page/香港電台.md "wikilink")[羅啟新認識藝人](../Page/羅啟新.md "wikilink")[利嘉兒](../Page/利嘉兒.md "wikilink")，繼而迅速發展戀情，拍拖不夠3個月已視對方為理想對象。2005年11月9日，周國豐與利嘉兒結婚，在[太平山的](../Page/太平山_\(香港\).md "wikilink")[山頂餐廳舉行婚宴](../Page/山頂餐廳.md "wikilink")，但沒有正式註冊。由於周是港台高層，圈中到賀的人特別多。婚後，周與利在[跑馬地居住](../Page/跑馬地.md "wikilink")，常被拍到兩人拖手逛街。直至2008年4月，二人宣布因相處不來已經分開，因當日沒有註冊結婚，故無需辦理離婚手續。現時，雙方聲稱彼此只是好友\[1\]。

## 節目主持

### 電台

  - 《[中文歌曲龍虎榜](../Page/中文歌曲龍虎榜.md "wikilink")》
  - 《[國風國豐](../Page/國風國豐.md "wikilink")》
  - 《[娛樂滿天星](../Page/娛樂滿天星.md "wikilink")》

### 電視台

  - 《[冷知識衝動](../Page/冷知識衝動.md "wikilink")》
  - 《[亞洲星光大道](../Page/亞洲星光大道.md "wikilink")》評審

## 參考資料

## 外部連結

  - [香港電台 -
    周國豐介紹](http://programme.rthk.hk/channel/presenters/profiles.php?c=pth&m=djprofiles&a=showdj&djid=209)
  - [周國豐 新浪微博](http://www.weibo.com/brianchow)

[Category:香港前兒童演員](../Category/香港前兒童演員.md "wikilink")
[C](../Category/民生書院校友.md "wikilink")
[Category:彩虹邨天主教英文中學校友](../Category/彩虹邨天主教英文中學校友.md "wikilink")
[Category:香港電台主持人](../Category/香港電台主持人.md "wikilink")
[K](../Category/周姓.md "wikilink")

1.  [與周國豐結婚 有名無實](http://hk.news.yahoo.com/article/081024/3/8wgb.html)
    《星島日報》2008年10月25日