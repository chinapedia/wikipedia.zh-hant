**KinKi Single
Selection**是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")1張[單曲精選](../Page/單曲.md "wikilink")[專輯](../Page/音樂專輯.md "wikilink")。於2000年5月17日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

本專輯在1997年的[CD出道後約](../Page/CD.md "wikilink")3年推出，收錄了到此為止推出過的9張單曲A面曲，是KinKi
Kids的第1張單曲精選專輯。

單是首批銷量已經超越了70萬張，以累積銷量計算是KinKi
Kids的專輯中最高銷量的一張作品。初回版隨碟附送一本特製的寫真集。由於歌詞本是另外分開的，所以採用了CD盒和歌詞一併放在一個特製紙盒的包裝。而且封面使用的照片經過特別加工，會因應檢視角度而呈現不同樣子。據說用作CD封面的照片是他們在不知情的情況下被拍下的。雖然不知道是否因為在工作繁忙時推出無暇拍攝而選用了舊照片，還是特意地選出一張自然的照片，不過[堂本光一在自己主持的](../Page/堂本光一.md "wikilink")[富士電視台音樂節目](../Page/富士電視台.md "wikilink")『新堂本兄弟』上說過「其實（工作人員們）可以用一張比較好的啊」。不知是否這個原因，於2004年推出的第二張單曲精選專輯『[KinKi
Single Selection
II](../Page/KinKi_Single_Selection_II.md "wikilink")』封面照片是全新拍攝的。

本專輯基本上會收錄過往單曲的所有A面曲，但當時最新的一張單曲「[越來越喜歡
越來越愛／KinKi充滿幹勁之歌](../Page/越來越喜歡_越來越愛/KinKi充滿幹勁之歌.md "wikilink")」卻有例外，只收錄了第1首歌「越來越喜歡
越來越愛」。取而代之是收錄了本專輯第1首歌的純音樂歌曲。不過由於KinKi
Kids二人並沒有參與製作這首歌曲，歌曲似乎是一種序言的形式存在。至於真正沒有收錄「KinKi充滿幹勁之歌」的原因，一般認為因為以樂曲作為[動畫歌曲的身份](../Page/動畫.md "wikilink")，[歌詞內容明顯地比其他歌曲不同](../Page/歌詞.md "wikilink")。而且以發售次序來排列的話，這首歌就會成為了專輯的最後一曲。由於以這首歌曲作結不太合適，所以特意地抽出來不被收錄的可能性較高。結果實際上，由於沒被本專輯收錄，這首歌截至現時為止只作過單曲收錄曲。雖然在2004年例外地單曲「越來越喜歡
越來越愛／KinKi充滿幹勁之歌」作第二次翻印，不過由於製作量少及不至於大受歡迎，所以即使現在購入這張單曲並不比以前來得容易。

歌詞本方面首次使用了紅色字代表光一歌唱部份；藍色字代表剛歌唱部分；黑色字代表二人合唱部份。唯一在歌曲「青之時代」上因為全曲都由二人合唱而只使用黑色字標示。另外，在歌詞本的後部刊出了自出道到專輯發售為止，KinKi
Kids的活動記錄及唱片記錄。至於4年後推出的『KinKi Single Selection
II』就記載了本專輯發售後至該專輯發售期間的活動內容。

在本專輯收錄的樂曲，由於沒有進行過為了在專輯收錄而設的重新錄音，所以所有樂曲的音質並沒有被特別提高。而且歌曲間的音量大小也有著些微的差別。

在本專輯推出後的兩個月，推出了除純音樂外收錄曲目一樣的單曲精選[卡拉OK專輯](../Page/卡拉OK.md "wikilink")『[KinKi
KaraoKe Single
Selection](../Page/KinKi_KaraoKe_Single_Selection.md "wikilink")』。基本上是本專輯內收錄曲目除掉人聲的純音樂版本，是一張純為卡拉OK而設的專輯。

至於專輯的名字，若果只取第一個字來作縮略的話就會是「KiSS」，是一般歌迷對這張專輯的暱稱。

## 收錄歌曲

1.  **Theme of KinKi Kids '00**
      - 作曲：[長岡成貢](../Page/長岡成貢.md "wikilink")
      - 編曲：長岡成貢
      - 旁白：[Lonnie Hirsch](../Page/Lonnie_Hirsch.md "wikilink")
2.  **玻璃少年**（****）
      - 作曲：山下達郎
      - 作詞：松本隆
      - 編曲：山下達郎
3.  **被愛不如愛人**（****）
      - ※KinKi
        Kids二人參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『我們的勇氣
        未滿都市』主題曲。
      - 作曲：[馬飼野康二](../Page/馬飼野康二.md "wikilink")
      - 作詞：[森　浩美](../Page/森浩美.md "wikilink")
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
      - 和音編排：[岩田雅之](../Page/岩田雅之.md "wikilink")
4.  **雲霄飛車羅曼史**（**Jetcoaster Romance；**）
      - ※KinKi
        Kids參與演出的日本[全日空](../Page/全日本空輸.md "wikilink")[航空公司廣告](../Page/航空公司.md "wikilink")『'98
        Paradise [沖繩](../Page/沖繩.md "wikilink")』主題曲。
      - 作曲：[山下達郎](../Page/山下達郎.md "wikilink")
      - 作詞：[松本　隆](../Page/松本隆.md "wikilink")
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
5.  **擁抱全部**（****）
      - ※KinKi Kids參與演出的[富士電視台音樂節目](../Page/富士電視台.md "wikilink")『LOVE
        LOVE我愛你』主題曲。
      - 作曲：[吉田拓郎](../Page/吉田拓郎.md "wikilink")
      - 作詞：[康　珍化](../Page/康珍化.md "wikilink")
      - 編曲：[武部聰志](../Page/武部聰志.md "wikilink")
6.  **青春時代**（****）
      - ※[堂本剛參與演出的](../Page/堂本剛.md "wikilink")[東京放送](../Page/東京放送.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『青之時代』主題曲。
      - 作曲：[canna](../Page/canna.md "wikilink")
      - 作詞：canna
      - 編曲：[新川　博](../Page/新川博.md "wikilink")
7.  **Happy Happy Greeting**
      - ※KinKi
        Kids參與演出的[Panasonic廣告](../Page/松下電器.md "wikilink")『數碼攝錄機』主題曲。
      - 作曲：[山下達郎](../Page/山下達郎.md "wikilink")
      - 作詞：[松本　隆](../Page/松本隆.md "wikilink")
      - 編曲：[山下達郎](../Page/山下達郎.md "wikilink")
8.  **Cinderella Christmas**（****）
      - 作曲：[谷本　新](../Page/谷本新.md "wikilink")
      - 作詞：[松本　隆](../Page/松本隆.md "wikilink")
      - 編曲：[長岡成貢](../Page/長岡成貢.md "wikilink")
9.  **不要停止的純真**（****，**PURE**）
      - ※堂本剛參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『為了與你的未來～I'll
        be back～』主題曲。
      - 作曲：[筒美京平](../Page/筒美京平.md "wikilink")
      - 作詞：[伊達　步](../Page/伊達步.md "wikilink")
      - 編曲：[WACKY KAKI](../Page/WACKY_KAKI.md "wikilink")
10. **Flower**（****）
      - ※KinKi
        Kids參與演出的日本[全日空](../Page/全日本空輸.md "wikilink")[航空公司廣告](../Page/航空公司.md "wikilink")『'99
        Paradise [沖繩](../Page/沖繩.md "wikilink")』主題曲。
      - 作曲：[HΛL](../Page/HΛL.md "wikilink")、[音妃](../Page/音妃.md "wikilink")
      - 作詞：[HΛL](../Page/HΛL.md "wikilink")
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
      - 和音編排：[松下　誠](../Page/松下誠.md "wikilink")
11. **雨的旋律**（**Melody**）
      - 作曲：[坂井秀陽](../Page/坂井秀陽.md "wikilink")、[武藤敏史](../Page/武藤敏史.md "wikilink")
      - 作詞：[康　珍化](../Page/康珍化.md "wikilink")
      - 編曲：[有賀啟雄](../Page/有賀啟雄.md "wikilink")
12. **to Heart**
      - ※堂本剛參與演出的[東京放送](../Page/東京放送.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『[to
        Heart～至死不渝～](../Page/To_Heart_\(電視劇\).md "wikilink")』主題曲。
      - 作曲：[宮崎　步](../Page/宮崎步.md "wikilink")
      - 作詞：[久保田洋司](../Page/久保田洋司.md "wikilink")、E.Komatsu
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
13. **越來越喜歡　越來越愛**（****）
      - ※KinKi Kids參與演出的[富士電視台音樂節目](../Page/富士電視台.md "wikilink")『LOVE
        LOVE我愛你』第2代主題曲。
      - 作曲：[堂本光一](../Page/堂本光一.md "wikilink")
      - 作詞：[堂本　剛](../Page/堂本剛.md "wikilink")
      - 編曲：[武部聰志](../Page/武部聰志.md "wikilink")

[Category:近畿小子專輯](../Category/近畿小子專輯.md "wikilink")
[Category:2000年音樂專輯](../Category/2000年音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:Oricon百萬銷量達成專輯](../Category/Oricon百萬銷量達成專輯.md "wikilink")
[Category:2000年Oricon專輯月榜冠軍作品](../Category/2000年Oricon專輯月榜冠軍作品.md "wikilink")
[Category:2000年Oricon專輯週榜冠軍作品](../Category/2000年Oricon專輯週榜冠軍作品.md "wikilink")