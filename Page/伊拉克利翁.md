[Heraklion_map.png](https://zh.wikipedia.org/wiki/File:Heraklion_map.png "fig:Heraklion_map.png")
**伊拉克利翁**（；[英語](../Page/英語.md "wikilink")：Heraklion）是[希腊位於](../Page/希腊.md "wikilink")[克里特島上的最大的城市](../Page/克里特島.md "wikilink")，同时也是[克里特大区和](../Page/克里特大区.md "wikilink")[伊拉克利翁州的首府](../Page/伊拉克利翁州.md "wikilink")，人口142,112（2001年）。

## 历史

[Morozini_Fountain_Heraklion.jpg](https://zh.wikipedia.org/wiki/File:Morozini_Fountain_Heraklion.jpg "fig:Morozini_Fountain_Heraklion.jpg")喷泉\]\]
伊拉克利翁是[弥诺斯文明时期](../Page/弥诺斯文明.md "wikilink")[克里特岛最大的人口中心](../Page/克里特岛.md "wikilink")，據說[古希臘神話中](../Page/古希臘神話.md "wikilink")[米諾斯王的王宮](../Page/米諾斯.md "wikilink")，[米諾斯迷宮就建在這裡](../Page/米諾斯迷宮.md "wikilink")。由[阿瑟·埃文斯发现并发掘的](../Page/阿瑟·埃文斯.md "wikilink")[克诺索斯宫殿遗址就位于附近](../Page/克诺索斯.md "wikilink")。由此可以推断大约在公元前2000年左右这里应该有港口存在，但这一推断并未得到考古证据证明。

现代的伊拉克利翁于824年由[撒拉逊人建立](../Page/撒拉逊人.md "wikilink")，命名为“****”。他们在城市周边挖了护城河并允许[海盗将其作为避风港](../Page/海盗.md "wikilink")。

961年，[拜占庭帝国攻占伊拉克利翁](../Page/拜占庭帝国.md "wikilink")，大肆屠杀撒拉逊人，将城市付之一炬并在其重建后对其进行了长达243年的控制。

1203年，[第四次东征十字军扶持已被废黜的](../Page/第四次十字军东征.md "wikilink")[拜占庭皇帝](../Page/拜占庭皇帝.md "wikilink")[伊萨克二世重登皇位](../Page/伊萨克二世.md "wikilink")。作为此次政治交易的一部分，次年，协助过十字军的[威尼斯共和国控制了伊拉克利翁](../Page/威尼斯共和国.md "wikilink")。威尼斯人改进加固了城市防御工事并将其更名为[意大利语](../Page/意大利语.md "wikilink")“**[-{zh-hans:干地亚;
zh-hant:甘地亞;}-](../Page/干地亞.md "wikilink")**（Candia）”，至今仍有人用此来称呼克里特岛。

1647年，[奥斯曼帝国对伊拉克利翁进行了长达](../Page/奥斯曼帝国.md "wikilink")[22年的围攻](../Page/干地亚围攻战.md "wikilink")，血腥的战争使30,000克里特岛人和120,000土耳其人死亡。威尼斯人最终于1669年投降，奥斯曼帝国占领城市并其及整个克里特岛更名为“**Kania**”。后来，伊拉克利翁港逐渐淤塞，奥斯曼人将大部分生意都转到了岛西部的[干尼亚](../Page/干尼亚.md "wikilink")。

1898年，奥斯曼人撤离，伊拉克利翁获得独立。1913年，城市成为[希腊王国的成员并更名为现名](../Page/希腊王国.md "wikilink")，意为[赫拉克勒斯之城](../Page/赫拉克勒斯.md "wikilink")（City
of Heracles）。

## 基础设施

[Esglesia_a_Herakleion.jpg](https://zh.wikipedia.org/wiki/File:Esglesia_a_Herakleion.jpg "fig:Esglesia_a_Herakleion.jpg")
伊拉克利翁是重要的海运港口和渡口，人们可以在这里乘渡船前往[锡拉岛](../Page/锡拉岛.md "wikilink")、[罗德岛](../Page/罗德岛.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[海法和希腊大陆](../Page/海法.md "wikilink")。

城市还拥有[伊拉克利翁国际机场和](../Page/伊拉克利翁国际机场.md "wikilink")[希腊空军基地](../Page/希腊空军基地.md "wikilink")。

## 名人

  - [尼科斯·卡赞察基斯](../Page/尼科斯·卡赞察基斯.md "wikilink")：作家
  - [扬娜·安耶洛普洛斯-扎斯卡拉基](../Page/扬娜·安耶洛普洛斯-扎斯卡拉基.md "wikilink")：2004年雅典奥运会组委会主席
  - [格列柯](../Page/格列柯.md "wikilink")：西班牙三大画家之一
  - [奥德修斯·埃里蒂斯](../Page/奥德修斯·埃里蒂斯.md "wikilink")：诗人，诺贝尔文学奖获得者

## 氣候

由於[緯度相當低](../Page/緯度.md "wikilink")，終年氣溫很高。

## 經濟

### 特產

除了[橄欖栽培和發達的漁業之外](../Page/橄欖.md "wikilink")，還盛產高級[染料](../Page/染料.md "wikilink")，如[貝紫](../Page/貝紫.md "wikilink")、[波斯漿果](../Page/波斯漿果.md "wikilink")。

## 外部链接

  - [伊拉克利翁网站](http://www.heraklion-city.gr)

[\*](../Category/伊拉克利翁.md "wikilink")
[H](../Category/希臘城市.md "wikilink")
[Category:希腊大区首府](../Category/希腊大区首府.md "wikilink")
[Category:希腊州府](../Category/希腊州府.md "wikilink")
[Category:沿愛琴海城市](../Category/沿愛琴海城市.md "wikilink")