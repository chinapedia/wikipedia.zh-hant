**爱尔兰人**，狭义上指[爱尔兰共和国的](../Page/爱尔兰共和国.md "wikilink")[公民](../Page/公民.md "wikilink")，广义上来讲是自古居住在[爱尔兰岛上的人们](../Page/爱尔兰岛.md "wikilink")。广义的爱尔兰人属于[凯尔特](../Page/凯尔特.md "wikilink")(Celtic)人种。爱尔兰岛分为两部分，南部是[爱尔兰共和国](../Page/爱尔兰共和国.md "wikilink")，北部是[北爱尔兰](../Page/北爱尔兰.md "wikilink")，由[英国统治](../Page/英国.md "wikilink")。但无论南北，都属于广义的爱尔兰人。

爱尔兰人（Irish: Muintir na hÉireann or na hÉireannaigh; Ulster-Scots:
Airisch or Airish
fowk）是欧洲西北部的爱尔兰岛的一个种族。爱尔兰岛有人居住的历史达9000年（根据考古学研究，详见[史前爱尔兰](../Page/史前爱尔兰.md "wikilink")）

关于爱尔兰人的祖先，传说中他们是
[Nemedians族](../Page/Nemedians.md "wikilink")、[佛摩尔族](../Page/佛摩尔族.md "wikilink")、[伯克族](../Page/伯克族.md "wikilink")、
[Tuatha Dé
Danann族和米利都人中一族的后人](../Page/Tuatha_Dé_Danann.md "wikilink")。一本爱尔兰神学书[Lebor
Gabála Érenn说](../Page/Lebor_Gabála_Érenn.md "wikilink")[Tuatha Dé
Dananns是](../Page/Tuatha_Dé_Dananns.md "wikilink")[Scythian](../Page/Scythian.md "wikilink")
人的后代。

与爱尔兰主族的相互影响的有皮克特人、苏格兰人和维京人。归功于这中影响，冰岛人也以有爱尔兰血统而著称。

## 參考資料

[愛爾蘭](../Category/愛爾蘭.md "wikilink")
[愛爾蘭人](../Category/愛爾蘭人.md "wikilink")
[Category:凱爾特人](../Category/凱爾特人.md "wikilink")