**埃里希·萊因斯多夫** (Erich Leinsdorf 原名 **Erich
Landauer**，)生于[维也纳](../Page/维也纳.md "wikilink")，是一位[奥地利](../Page/奥地利.md "wikilink")[指挥家](../Page/指挥家.md "wikilink")，1938年“[德奧合併](../Page/德奧合併.md "wikilink")”時，因其[犹太人身份而逃往美国](../Page/犹太人.md "wikilink")，并在1942年入籍。

他在维也纳完成学业，并于1933年获Diplom（相等于硕士）。在接下来的一年他在[萨尔斯堡音乐节上成为](../Page/萨尔斯堡音乐节.md "wikilink")[布鲁诺·瓦尔特和](../Page/布鲁诺·瓦尔特.md "wikilink")[托斯卡尼尼的助手](../Page/托斯卡尼尼.md "wikilink")。1937年他在[大都会歌剧院首演](../Page/大都会歌剧院.md "wikilink")，但就在下一年，他就成了欧洲难民而在此长期任职。战后他也间或回欧洲演出，但他的事业重心还是在美国。他被认为是[瓦格纳专家](../Page/瓦格纳.md "wikilink")，其实他指挥的“[图兰多](../Page/图兰多.md "wikilink")”也很具水平(主角为[比尔吉特·尼尔森](../Page/比尔吉特·尼尔森.md "wikilink")，[约西·毕约林和](../Page/约西·毕约林.md "wikilink")[苔巴尔蒂](../Page/苔巴尔蒂.md "wikilink"))。一方面由于他接受的教育，另一方面他的剧目，莱因斯多夫被认为是典型的歌剧指挥家。

1962年他继[查尔斯·孟许后成为](../Page/查尔斯·孟许.md "wikilink")[波士顿交响乐团指挥](../Page/波士顿交响乐团.md "wikilink")。1978-1980年他担任[柏林广播交响乐团首席指挥](../Page/柏林广播交响乐团.md "wikilink")。逝世于瑞士[苏黎世](../Page/苏黎世.md "wikilink")。

## 演出

<small>以下曲目来源为 Horst Seeger的《歌剧百科全书》, Henschelverlag Berlin (DDR), 4.
Auflage 1989</small>

  - [瓦格纳](../Page/瓦格纳.md "wikilink"):
    [女武神](../Page/女武神.md "wikilink")，[罗恩格林](../Page/罗恩格林.md "wikilink")，[特里斯坦与依索尔德](../Page/特里斯坦与依索尔德.md "wikilink")，[纽伦堡的名歌手](../Page/纽伦堡的名歌手.md "wikilink")
  - [普契尼](../Page/普契尼.md "wikilink"): 托斯卡，蝴蝶夫人，图兰多，波西米亚人，
  - [理查德·施特劳斯](../Page/理查德·施特劳斯.md "wikilink"): 阿里阿德涅在纳克索斯，莎乐美
  - [威尔第](../Page/威尔第.md "wikilink"): 阿依达，假面舞会, 马克白
  - [莫扎特](../Page/莫扎特.md "wikilink"): 女人心,唐璜,费加罗的婚礼
  - [埃里希·沃尔夫冈·康歌德](../Page/埃里希·沃尔夫冈·康歌德.md "wikilink"):死城
  - [唐尼采蒂](../Page/唐尼采蒂.md "wikilink"): 拉美末的露西娅
  - [罗西尼](../Page/罗西尼.md "wikilink"): 塞维利亚理发师

## 连接

  - [Biografie mit
    Fotos](https://web.archive.org/web/20060519071530/http://www.maurice-abravanel.com/leinsdorf__english.html)
  - [Gespräch mit Erich Leinsdorf
    (PDF-Link)](https://web.archive.org/web/20100611164834/http://www.lbjlib.utexas.edu/johnson/archives.hom/oralhistory.hom/Leinsdorf-E/Leinsdorf.asp)

[Category:奥地利指挥家](../Category/奥地利指挥家.md "wikilink")
[Category:美国指挥家](../Category/美国指挥家.md "wikilink")
[Category:20世紀指揮家](../Category/20世紀指揮家.md "wikilink")
[Category:奧地利猶太人](../Category/奧地利猶太人.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:归化美国公民的奥地利人](../Category/归化美国公民的奥地利人.md "wikilink")
[Category:維也納人](../Category/維也納人.md "wikilink")