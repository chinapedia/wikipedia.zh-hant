[魏禧.jpg](https://zh.wikipedia.org/wiki/File:魏禧.jpg "fig:魏禧.jpg")》第一集之魏禧像\]\]
**魏禧**（），[字](../Page/表字.md "wikilink")**冰叔**，一字**凝叔**，[號](../Page/號.md "wikilink")**裕齋**，亦號**勺庭先生**。[江西](../Page/江西.md "wikilink")[宁都人](../Page/宁都.md "wikilink")。[明末](../Page/明.md "wikilink")[清初著名的](../Page/清.md "wikilink")[散文家](../Page/散文家.md "wikilink")。與[侯朝宗](../Page/侯朝宗.md "wikilink")、[汪琬合稱](../Page/汪琬.md "wikilink")「明末清初散文三大家」。与兄[魏祥](../Page/魏祥.md "wikilink")、弟[魏礼並美](../Page/魏礼.md "wikilink")，世称“寧都三魏”。

## 生平

[明末](../Page/明.md "wikilink")[诸生](../Page/诸生.md "wikilink")，明亡后不仕，隐居翠微峰。所居之地名勺庭，又称“勺庭先生”。又以齋號「裕齋」，人稱“裕齋先生”。

魏禧重氣節，束身砥行，“以經濟有用之[文學](../Page/文學.md "wikilink")，顯天下百餘年”\[1\]，文章有凌厉雄杰、刚劲慷慨之气，與[侯方域](../Page/侯方域.md "wikilink")、[汪琬合稱](../Page/汪琬.md "wikilink")「清初三大家」。[散文作品有](../Page/散文.md "wikilink")《江天一传》、《刘文炳传》、《朱参军家传》、《邱维屏传》、《[大铁椎传](../Page/s:大铁椎传.md "wikilink")》，文笔简炼，叙事如绘，内容多表彰民族大義，叙事简洁，又善议论，“踔厉森峭而指事精切”\[2\]。又精於史學，“举数千年，治乱兴衰，得失消长之故，穷究而贯通之。”\[3\]。魏禧自言“少好《[左傳](../Page/左傳.md "wikilink")》、[蘇老泉](../Page/蘇洵.md "wikilink")（洵），中年稍涉他氏。然文無專嗜，唯擇吾所雅愛賞者”\[4\]。但也有些则是轻率之作，曾受[钱澄之](../Page/钱澄之.md "wikilink")、[王庆麟等人批评](../Page/王庆麟.md "wikilink")。

魏禧与兄[魏祥](../Page/魏祥.md "wikilink")、弟[魏礼](../Page/魏礼.md "wikilink")，都能文章，世称“三魏”，三魏兄弟與[彭士望](../Page/彭士望.md "wikilink")、[林时益](../Page/林时益.md "wikilink")、[李腾蛟](../Page/李腾蛟.md "wikilink")、[邱维屏](../Page/邱维屏.md "wikilink")、[彭任](../Page/彭任.md "wikilink")、[曾灿等合稱](../Page/曾灿.md "wikilink")“[易堂九子](../Page/易堂九子.md "wikilink")”\[5\]。

[康熙十一年](../Page/康熙.md "wikilink")（1669年）認識[歸莊](../Page/歸莊.md "wikilink")，晚年有頭風症狀，頗為所苦。[康熙十八年](../Page/康熙.md "wikilink")（1680年）[公家徵為](../Page/公家.md "wikilink")[博学鸿词科](../Page/博学鸿词科.md "wikilink")，不就。[康熙十九年](../Page/康熙.md "wikilink")（1681年）十一月十七日，客死[真州](../Page/真州.md "wikilink")（今[江蘇](../Page/江蘇.md "wikilink")[儀徵](../Page/儀徵.md "wikilink")）。

## 著作

文集合編為《寧都三魏全集》。著有《魏叔子文集》、《诗集》、《日录》、《左传经世》、《兵谋》、《兵法》、《兵迹》。

## 愚孝邪说

魏禧说：“父母欲以非礼杀子，子不当怨。盖我本无身，因父母而后有；杀之不过与未生一样。”

## 参考文献

## 外部链接

{{-}}

[Category:清朝作家](../Category/清朝作家.md "wikilink")
[Category:清朝散文家](../Category/清朝散文家.md "wikilink")
[Category:寧都人](../Category/寧都人.md "wikilink")
[X](../Category/魏姓.md "wikilink")

1.  尚熔《書魏叔子文集後》
2.  [宋荦](../Page/宋荦.md "wikilink")《三家文钞序》
3.  《听松庐文钞》
4.  《與諸子世傑論文書》
5.  [李元度](../Page/李元度.md "wikilink")《国朝先正事略·文苑·魏叔子先生事略》条：“易堂九子，自三魏及躬庵、确斋外，曰李腾蛟咸斋，邱维屏邦士，彭任中叔，曾灿青藜。敦古友谊，如骨肉子弟，无恒父师。高僧无可，尝至山中，叹曰：‘易堂真气，天下无两矣。’”