**UUCall**是“中華電信（香港）國際有限公司”（原[悠优](../Page/悠优.md "wikilink")）推出的即时语音通讯软件。

## 优点

1.  可以直接拨打固定电话和部分移动电话，也可拨打[境外电话](../Page/境外.md "wikilink")；
2.  UUCall软件小，无需安装即可使用；
3.  境内电话、通往发达国家与地区的国际长途以及港澳台电话价格低廉。

## 缺点

1.  软件每一版本的升级较慢；
2.  软件的界面较为大众化，不够新颖；
3.  目前只有PC客户端以及安卓客户端。

## 特殊服务

提供两个特殊服务号码（免费）：

  - 80000——提供优质的超级小霸王服务；
  - 80099——提供音质极高的语音测试功能。

## 关于“中華電信（香港）國際有限公司”

2009年12月28日，UUCall被并入“中華電信（香港）國際有限公司”。该公司在[香港电讯管理局的注册为](../Page/电讯管理局.md "wikilink")“1533
-{中華電信（香港）國際有限公司}-”\[1\]，英文为“1533 China Telecom (Hong Kong)
International Group Company
Limited”（类似于[中国电信（香港）国际有限公司的](../Page/中国电信#控股子公司.md "wikilink")“China
Telecom (Hong Kong) Int'l Limited”\[2\]）。

## 竞争对手

  - [Skype](../Page/Skype.md "wikilink")

  - [阿里通](../Page/阿里通.md "wikilink")

  - [爱聊](../Page/爱聊.md "wikilink")

  - [Mediaring Talk](../Page/Mediaring_Talk.md "wikilink")

  - [Globe7](../Page/Globe7.md "wikilink")

  -
  - [KC](../Page/KC.md "wikilink")

## 外部链接

  - [uucall网络电话官网](http://www.uucall.com/)

[Category:VoIP](../Category/VoIP.md "wikilink")

1.  [電訊管理局 -
    對外電訊服務營辦商](http://www.ofta.gov.hk/zh/tele-lic/operator-licensees/pnets-ets.html)

2.  [OFTA - External Telecommunications Services (ETS)
    Operators](http://www.ofta.gov.hk/en/tele-lic/operator-licensees/pnets-ets.html)