[Omotesando_Hills_Void_View_2018.jpg](https://zh.wikipedia.org/wiki/File:Omotesando_Hills_Void_View_2018.jpg "fig:Omotesando_Hills_Void_View_2018.jpg")
**表參道Hills**（，），中文經常譯為**表參道之丘**，位於[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")[原宿](../Page/原宿.md "wikilink")，是在「[同潤會青山公寓](../Page/同潤會.md "wikilink")」舊址上所進行的都市再開發計畫，因位於東京著名景點之一的[表參道而得名](../Page/表參道.md "wikilink")。由[森大廈公司進行開發](../Page/森大廈.md "wikilink")，建築師[安藤忠雄設計](../Page/安藤忠雄.md "wikilink")。建築區域是道路旁邊的狹窄細長的區域，並且由於是斜坡的地區，不能建造高層建築，可說是很難處理的建築腹地。於是設計師利用一整排的[櫸樹](../Page/櫸樹.md "wikilink")，和分三階段漸次降低的建築樓層來調和景觀。

## 簡介

建築已經超過80年的[同潤會青山公寓](../Page/同潤會.md "wikilink")（1927年竣工），被拆除之後在原本的位址上建立的複合建築設施就是**表參道Hills**。於2006年2月11日開幕，全長約250公尺，為地上3層、地下3層的建築。其中進駐了國內外知名的名牌服飾精品店。建築內部為將地上和地下共六層樓挑高的構造，由於表參道是一個傾斜的腹地，因此建築內的地面也有許多斜坡。此外，在東側也保留了以前青山公寓建築的一部份，用來作為店面使用。

  - 建築主：神宮前四丁目地区市街地再開発組合
  - 設計：[安藤忠雄建築研究所](../Page/安藤忠雄.md "wikilink")、森建築設計共同企業体
  - 施工：株式会社大林組、株式会社関電工、高砂熱学工業株式会社、三建設備工業株式会社
  - 商店數量：設101間商店（2006年6月）
  - 住宅數量：提供38戶，稱為「Zelkova Terrace」，簡稱「樺木樹住宅（ケヤキの住居）」。
  - 停車場：可停泊私家車182台、電單車35台
  - 樓面數：共有12個樓面。（住宅3個樓面、商業設施6個樓面、停車場3個樓面）
  - 面積：建築設施面積33,916平方公尺、商店面積9,959平方公尺。

<File:Omotesando> Hills Entrance to Basement 201306.jpg|購物商場入口
<File:Omotesando> Hills Roof Garden 201306.jpg|頂層花園 <File:Omotesando>
Hills Exterior Night view 201306.jpg|晚上購物商場外貌

[Omotesando_Hills_003.jpg](https://zh.wikipedia.org/wiki/File:Omotesando_Hills_003.jpg "fig:Omotesando_Hills_003.jpg")

## 營業時間

  - 商店：11:00 - 21:00
  - 餐廳：11:00 - 24:00
  - 咖啡廳（Cafe）：8:00 - 23:00
  - 休館日：表參道Hills約每半年會有1\~2天的全館休息日。第一個休息日訂在2006年的8月22日和23日。

## 鄰近車站

  - [東京地下鐵](../Page/東京地下鐵.md "wikilink")[千代田線](../Page/東京地下鐵千代田線.md "wikilink")、[銀座線](../Page/東京地下鐵銀座線.md "wikilink")、[半藏門線](../Page/東京地下鐵半藏門線.md "wikilink")
    - [表參道站](../Page/表參道站.md "wikilink") A2出口徒步2分
  - [東京地下鐵](../Page/東京地下鐵.md "wikilink")[千代田線](../Page/東京地下鐵千代田線.md "wikilink")、[副都心線](../Page/東京地下鐵副都心線.md "wikilink")-
    [明治神宮前站](../Page/明治神宮前站.md "wikilink") 5號出口徒步3分
  - [JR東日本](../Page/東日本旅客鐵道.md "wikilink")[山手線](../Page/JR山手線.md "wikilink")
    - [原宿站](../Page/原宿站.md "wikilink") 明治神宮口徒步7分

## 參見

  - [六本木新城](../Page/六本木新城.md "wikilink")

## 外部連結

  - [表參道Hills官方網站](http://www.omotesandohills.com/)
  - [「當地居民的表參道Hills專門攻略全記載」部落格](http://blog.goo.ne.jp/bvcinc)

[Category:東京都都市更新地區](../Category/東京都都市更新地區.md "wikilink")
[Category:東京都觀光地](../Category/東京都觀光地.md "wikilink")
[Category:東京都建築物](../Category/東京都建築物.md "wikilink") [Category:神宮前
(澀谷區)](../Category/神宮前_\(澀谷區\).md "wikilink")
[Category:原宿](../Category/原宿.md "wikilink")
[Category:森大廈](../Category/森大廈.md "wikilink")
[Category:2006年完工建築物](../Category/2006年完工建築物.md "wikilink")