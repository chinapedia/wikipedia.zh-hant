**朴敏慶**（，，英文譯音：**葛瑞絲·朴**
，\[1\]\[2\]）是一名[美國及](../Page/美國.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[韓裔演員](../Page/韓裔.md "wikilink")，以她於電視影集《[太空堡垒卡拉狄加](../Page/2005星際大爭霸.md "wikilink")》中扮演的角色夏倫·法洛利（Sharon
"Boomer"
Valerii），與《[檀島警騎2.0](../Page/檀島警騎2.0.md "wikilink")》中所扮演的科諾·卡拉卡瓦（Kono
Kalakaua）警官等角色最為人熟識。

她亦曾於[遊戲](../Page/電子遊戲.md "wikilink")《[命令与征服3：泰伯利亚之战](../Page/終極動員令3：泰伯倫戰爭.md "wikilink")》亮相。\[3\]\[4\]

## 影视作品

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>名称</p></th>
<th><p>角色</p></th>
<th><p>注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2000</p></td>
<td><p><em><a href="../Page/Romeo_Must_Die.md" title="wikilink">Romeo Must Die</a></em></p></td>
<td><p>Asian Dancer</p></td>
<td><p>Film</p></td>
</tr>
<tr class="even">
<td><p><em>The Immortal</em></p></td>
<td><p>Mikikio</p></td>
<td><p>TV series–Recurring character</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/末世黑天使.md" title="wikilink">末世黑天使</a>: "<a href="../Page/List_of_Dark_Angel_episodes#Season_2_.282001.E2.80.932002.29.md" title="wikilink">Designate This</a>"</p></td>
<td><p>Female Breeding X5</p></td>
<td><p>TV series–Ending Credits</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/Edgemont_(TV_series).md" title="wikilink">Edgemont</a></em></p></td>
<td><p>Shannon Ng</p></td>
<td><p>TV series–Regular character</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星際之門：SG-1.md" title="wikilink">星際之門：SG-1</a> 第五季 'Proving Ground'</p></td>
<td><p>Lt. Satterfield</p></td>
<td><p>电视连续剧-客串</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><em><a href="../Page/Battlestar_Galactica_(TV_miniseries).md" title="wikilink">Battlestar Galactica</a></em></p></td>
<td><p><a href="../Page/Number_Eight_(Battlestar_Galactica).md" title="wikilink">Number Eight</a></p></td>
<td><p>TV series–Regular character</p></td>
</tr>
<tr class="odd">
<td><p><em><a href="../Page/Jake_2.0.md" title="wikilink">Jake 2.0</a></em></p></td>
<td><p>Fran Yoshida</p></td>
<td><p>TV series–Recurring character</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><em><a href="../Page/Andromeda_(TV_series).md" title="wikilink">Andromeda</a></em></p></td>
<td><p>Doctor 26-Carol</p></td>
<td><p>TV series–Guest appearance</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><em><a href="../Page/West_32nd.md" title="wikilink">West 32nd</a></em></p></td>
<td><p>Lila Lee</p></td>
<td><p>Film</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/命令与征服3：泰伯利亚之战.md" title="wikilink">命令与征服3：泰伯利亚之战</a></p></td>
<td><p><a href="../Page/GDI_characters_of_Command_&amp;_Conquer#Lt._Sandra_Telfair.md" title="wikilink">Sandra Telfair</a></p></td>
<td><p><a href="../Page/电子游戏.md" title="wikilink">电子游戏</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td><p><em><a href="../Page/The_Cleaner_(TV_series).md" title="wikilink">The Cleaner</a></em></p></td>
<td><p>Akani Cuesta</p></td>
<td><p>TV series–Regular character</p></td>
</tr>
<tr class="even">
<td><p><em><a href="../Page/The_Border_(TV_series).md" title="wikilink">The Border</a></em></p></td>
<td><p>Liz Carver</p></td>
<td><p>TV series–Recurring character</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/犯罪现场调查.md" title="wikilink">犯罪现场调查</a>: "A Space Oddity"</p></td>
<td><p>Convention Attendee<br />
(uncredited)</p></td>
<td><p>TV series–Guest appearance</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/太空堡垒卡拉狄加：计划.md" title="wikilink">太空堡垒卡拉狄加：计划</a></p></td>
<td><p><a href="../Page/Number_Eight_(Battlestar_Galactica).md" title="wikilink">Number Eight</a></p></td>
<td><p>TV film</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><em><a href="../Page/Human_Target_(2010_TV_series).md" title="wikilink">Human Target</a></em></p></td>
<td><p>Eva Kahn</p></td>
<td><p>TV series–Guest appearance</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天堂执法者.md" title="wikilink">天堂执法者</a></p></td>
<td><p>Kona "Kono" Kalakaua</p></td>
<td><p>TV series</p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

<div class="references-small">

<references/>

</div>

## 外部链接

  -
  -
  - [朴敏庆 位於filmbug的個人資料](http://www.filmbug.com/db/343503)

[Category:加拿大電影演員](../Category/加拿大電影演員.md "wikilink")
[Category:加拿大電視演員](../Category/加拿大電視演員.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:不列顛哥倫比亞大學校友](../Category/不列顛哥倫比亞大學校友.md "wikilink")
[Category:韓裔加拿大人](../Category/韓裔加拿大人.md "wikilink")
[Category:韓裔美國人](../Category/韓裔美國人.md "wikilink")
[Category:美國裔加拿大人](../Category/美國裔加拿大人.md "wikilink")
[Category:洛杉磯人](../Category/洛杉磯人.md "wikilink")
[Grace](../Category/歸化加拿大公民.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")

1.  [資料來源：朴敏庆 於
    BattlestarWiki](http://en.battlestarwiki.org/wiki/Sources:Grace_Park)

2.  \[<http://www.tv.com/grace-park-blog/the-confusion-over-grace-parks-real-birthday/topic/83722-645911/show_blog_entry.html&topic_id=645911>|
    The confusion over Grace Park's real birthday\]
3.
4.