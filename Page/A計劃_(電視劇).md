《**A計劃**》（；[香港譯名](../Page/香港.md "wikilink")《**盜海奇兵**》）是一部於2007年出品的華語電視劇，此劇為[臺灣](../Page/臺灣.md "wikilink")、[香港與](../Page/香港.md "wikilink")[中国大陆合資拍攝製作](../Page/中国大陆.md "wikilink")，全劇共32集，由[張衛健](../Page/張衛健.md "wikilink")、[鍾欣潼](../Page/鍾欣潼.md "wikilink")、[王晶](../Page/王晶.md "wikilink")、[元秋和](../Page/元秋.md "wikilink")[譚耀文等擔任演出](../Page/譚耀文.md "wikilink")。

此劇於2006年8月20日至2006年12月20日期間在[北京](../Page/北京.md "wikilink")[横店影视城取景拍攝](../Page/横店影视城.md "wikilink")。

[台灣首播方面](../Page/台灣.md "wikilink")，於2007年7月5日起在[中視無線台逢星期一至五](../Page/中視無線台.md "wikilink")20:00首播，每集播放約90分鐘，最終於19集播放完畢，2007年7月31日結束；[香港首播方面](../Page/香港.md "wikilink")，將原本的劇名改為《盜海奇兵》，於2007年8月13日起在[亞洲電視](../Page/亞洲電視.md "wikilink")[本港台逢星期一至五](../Page/本港台.md "wikilink")20:30首播，每集播放約90分鐘，最終於20集播放完畢，2007年9月7日結束。

## 劇情內容

小喇叭(張衛健 飾)從小立志做警員懲惡揚善，進入水警後經常捉弄懶散又怕事的水警幫辦唐郎(王晶 飾)。賀爵士次子賀定國(譚耀文
飾)留英回港途中邂逅了志趣相投、美麗動人的高曼妮(鍾欣桐
飾)，並擒獲了前來劫船的海盜古陛A成為轟動全港的風頭人物，並榮膺陸警總警司。在一次陸警與水警的爭鬥中定國與小喇叭不打不相識，結為莫逆之交。

海盜頭子古標(梁家仁 飾)為救弟弟古陛A派徒弟大岳及兩名女兒古莉(張齡心 飾)與古慈(劉園園
飾)前往暗殺定國。古莉混入賀家當婢女，被定國的英雄氣概深深吸引;古慈混入水警，認識了小喇叭，二人互生好感。

定國同父異母的哥哥安邦(吳慶哲
飾)本有未婚妻，但卻對曼妮一見鍾情，因此嫉恨定國與曼妮的感情和事業的風光，于是設計害死未婚妻馬莉，以搏取曼妮的同情並離間定國與曼妮的關系。另一方
面，小喇叭夾在竇太太(元秋 飾)和古慈這一對麻辣婆媳之間也是麻煩不斷，生出釵h波折。

安邦為謀家產找人綁架父親，不料意外殺死爵士，遂嫁禍定國、小喇叭、唐郎等人，三人被迫逃離香港。定國、唐郎、小喇叭立誓報仇。定國得到馬莉父親的幫忙，逃往南洋。唐郎、小喇叭幾經辛苦各尋達人學藝，終于學成。定國以大馬商人鴨都拿拉曼的身份回港，進行復仇計畫。

## 演員表

### 海盜

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梁家仁.md" title="wikilink">梁家仁</a></p></td>
<td><p>古標</p></td>
<td><p>古莉、古慈之父<br />
後被大岳殺死</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張齡心.md" title="wikilink">張齡心</a></p></td>
<td><p>古莉(小莉)</p></td>
<td><p>古標之女<br />
古慈之姊<br />
大岳之妻</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉園園.md" title="wikilink">劉園園</a></p></td>
<td><p>古慈(小慈)</p></td>
<td><p>古標之女<br />
古莉之妹<br />
參見<a href="../Page/#竇家.md" title="wikilink">竇家</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吳浩康.md" title="wikilink">吳浩康</a></p></td>
<td><p>岳飛龍(大岳)</p></td>
<td><p>古莉之夫</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杜泓君.md" title="wikilink">杜泓君</a></p></td>
<td><p>崩牙佬</p></td>
<td><p>崩牙婆之夫</p></td>
</tr>
</tbody>
</table>

### 竇家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/元秋.md" title="wikilink">元　秋</a></p></td>
<td><p>竇太</p></td>
<td><p>唐郎之師姐<br />
小喇叭之母</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張衛健.md" title="wikilink">張衛健</a></p></td>
<td><p>竇巴(小喇叭)</p></td>
<td><p>竇太之子<br />
古慈之夫<br />
賀定國之好友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉園園.md" title="wikilink">劉園園</a></p></td>
<td><p>古慈(小慈)</p></td>
<td><p>小喇叭之妻</p></td>
</tr>
</tbody>
</table>

### 賀家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李國華.md" title="wikilink">李國華</a></p></td>
<td><p>賀良才</p></td>
<td><p>賀安邦、定國之父<br />
</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/徐玉蘭.md" title="wikilink">徐玉蘭</a></p></td>
<td><p>媚姨</p></td>
<td><p>賀良才之二姨太、賀定國之母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳慶哲.md" title="wikilink">吳慶哲</a></p></td>
<td><p>賀安邦</p></td>
<td><p>賀良才之子、賀定國之同父異母之兄<br />
高曼妮之夫<br />
賀新生之父<br />
大結局自殺</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍾欣潼.md" title="wikilink">鍾欣潼</a></p></td>
<td><p>高曼妮(MANDY)</p></td>
<td><p>高大文之女<br />
賀定國舊情人<br />
賀安邦之妻</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/譚耀文.md" title="wikilink">譚耀文</a></p></td>
<td><p>賀定國</p></td>
<td><p>賀良才之子、賀安邦之同父異母之弟<br />
高曼妮之舊情人<br />
小喇叭之好友</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 水警

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王晶.md" title="wikilink">王　晶</a></p></td>
<td><p>唐郎</p></td>
<td><p>青年由<a href="../Page/聶遠.md" title="wikilink">聶遠飾演</a><br />
竇太之師弟<br />
曾深愛柳如煙</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳煒.md" title="wikilink">陳　煒</a></p></td>
<td><p>柳如煙(柳姐)</p></td>
<td><p>唐郎之妻<br />
被賀安邦殺害</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭曉東.md" title="wikilink">鄭曉東</a></p></td>
<td><p>紅豆糕</p></td>
<td><p>小喇叭同事及好友<br />
曾喜歡玫瑰、牡丹<br />
被人殺害</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏妮.md" title="wikilink">夏妮</a></p></td>
<td><p>瑪莉</p></td>
<td><p>賀安邦未婚妻<br />
被賀安邦殺死</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張春仲.md" title="wikilink">張春仲</a></p></td>
<td><p>方鐵指</p></td>
<td><p>袁世凱之手下</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫美林.md" title="wikilink">莫美林</a></p></td>
<td><p>卓銅皮</p></td>
<td><p>袁世凱之手下</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張琦.md" title="wikilink">張琦</a></p></td>
<td><p>大口九</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 收視

以下為該劇於[香港](../Page/香港.md "wikilink")[亞洲電視本港台之收視紀錄](../Page/亞洲電視本港台.md "wikilink")：

| 週次 | 日期               | 平均收視                            |
| -- | ---------------- | ------------------------------- |
| 1  | 2007年8月13日－8月17日 | 5[點](../Page/收視點.md "wikilink") |
| 2  | 2007年8月20日－8月24日 | 4[點](../Page/收視點.md "wikilink") |
| 3  | 2007年8月27日－8月31日 | 4[點](../Page/收視點.md "wikilink") |
| 4  | 2007年9月3日－9月7日   | 4[點](../Page/收視點.md "wikilink") |

## 中國大陸版本

此劇製作的中國大陸版本共分為兩個集數版本：36集版本以審查形式製作，曾在中國大陸各地電視台播出；42集版本以影碟形式製作，在中國大陸發行影碟銷售。

## 參考資料

## 電視節目的變遷

[Category:亞洲電視外購劇集](../Category/亞洲電視外購劇集.md "wikilink")
[Category:中文連續劇](../Category/中文連續劇.md "wikilink")
[Category:中視外購電視劇](../Category/中視外購電視劇.md "wikilink")