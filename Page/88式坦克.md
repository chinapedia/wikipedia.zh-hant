**88式坦克**是中國国产二代[主戰坦克](../Page/主戰坦克.md "wikilink")，按中国国家军用标准武器装备命名规范其标准型号名称是“**ZTZ-88**”一般简称为“88式坦克”，是[80式主战坦克发展至](../Page/80式主战坦克.md "wikilink")1987年最终定型后的名称。成为[中国人民解放军正式装备部队的第一型二代主战坦克](../Page/中国人民解放军.md "wikilink")。88式系列坦克在1995年停產，在[中国人民解放军陆军部队服役的](../Page/中国人民解放军陆军.md "wikilink")88式系列坦克約有300至400辆。

## 歷史

1960年代[中蘇交惡後](../Page/中蘇交惡.md "wikilink")，尤其是1969年[中蘇邊界衝突后](../Page/中蘇邊界衝突.md "wikilink")，1970年代雙方部署重兵對峙，中國當時最先進的只有[59式坦克](../Page/59式坦克.md "wikilink")（仿制[蘇聯](../Page/蘇聯.md "wikilink")[T-54](../Page/T-54.md "wikilink")），比蘇聯的[T-62](../Page/T-62.md "wikilink")、[T-64及](../Page/T-64.md "wikilink")[T-72作戰性能相差太遠](../Page/T-72.md "wikilink")。
617廠（現稱內蒙古第一機械集團公司）根据军方要求開發新型坦克以對抗蘇聯坦克威胁，借鉴在[珍宝岛事件中俘获來的蘇聯T](../Page/珍宝岛事件.md "wikilink")-62坦克技術，在59式坦克基础上研制出[69式坦克](../Page/69式坦克.md "wikilink")，于1974年设计定型，但依然落后于世界先进水平较多，解放軍對其并不滿意。

1978年代号“WZ1224”的二代主战坦克原理样车的研发正式开始，1980年代初还开始了代号“WZ1226”的二代主战坦克另一初样车的研制设计工作。由于坦克技术储备不足，为加快二代坦克研制，617厂提出在69式改型坦克的基础上，分步移植成熟的先进技术，即“WZ1223”/“WZ1225”坦克样车。在1980年11月召开69（改）坦克方案论证会上，由当时的国防部长[张爱萍将军提议命名为](../Page/张爱萍.md "wikilink")“[80式坦克](../Page/80式坦克.md "wikilink")”。1981年确定总体设计方案，试验样车一些研究的成果应用到80式坦克上。研發以617厂承担主要工作，447厂、616厂、201所等单位予以协助，总设计师是方慰先\[1\]。1986年展开整车定型试验，1987年装甲兵定型委员会通过定型审查，1988年按《全军武器装备命名规定》正式命名为“ZTZ-88坦克”，也就是所谓的88式坦克。
二代主战坦克装备时正值中国军费压缩让步经济建设的时期，所以产量都不大。

## 概貌

80/88式坦克继承了59式/69式坦克的整体布局形式和半球形铸造炮塔的结构，但是采用了全新设计的有六对小直径负重轮的履带式底盘，功率为730马力的柴油发动机，车体首上使用复合装甲提高防护力，车体两侧带屏蔽裙板。装备了采用引进技术的北约标准的[105毫米](../Page/L7線膛炮.md "wikilink")[口径](../Page/口径.md "wikilink")[线膛炮](../Page/线膛炮.md "wikilink")。配备扰动式简易火控系统，观瞄、激光测距、微光夜视仪三合一炮长瞄准镜。新增超压的集体“三防”系统，改进灭火抑爆系统。\[2\]

## 型號

### 原型车

以80式坦克的設計改進完善。

### 88B

88B式坦克是88式坦克的量产型号。88B装备了下反式稳像火控系统，改进微光夜视装置，车内的布置有所改进。外观上砲塔后面有置物架，烟幕弹发射装置移动了安装位置。可加裝爆炸反應裝甲，是中國解放軍第一種裝有爆炸反應裝甲服役的坦克。[83式105毫米坦克炮](../Page/83式105毫米坦克炮.md "wikilink")。

### 88A

88A在88B之後才推出的改进型，装备有加長炮管改良性能的83A式105毫米线膛炮，安裝的FY系列双防反應裝甲版以對抗[穿甲弹和](../Page/穿甲弹.md "wikilink")[破甲彈攻擊](../Page/破甲彈.md "wikilink")。

### 88C

88C式是[ZTZ-96主战坦克的非正式称呼](../Page/ZTZ-96主战坦克.md "wikilink")，由外贸型85-IIM结合中国军队自用型ZTZ-88改進而成，裝备125毫米口径滑膛炮以及自动装弹机，发动机與88式相同。

## 使用國

  - \- 300\~400輛88式坦克

  - \- 230+ 輛88B式坦克

## 参考文献

<div class="references-small">

<references />

  -

</div>

## 外部連結

  - [China-Defense.com-88C式](https://web.archive.org/web/20101117235110/http://china-defense.com/armor/type88c/88C01.html)

  - [GlobalSecurity.org-80式／88式](http://www.globalsecurity.org/military/world/china/type-80.htm)

  - [GlobalSecurity.org-96式](http://www.globalsecurity.org/military/world/china/type-96.htm)

  - [Chinese Defense
    Today-88式](https://web.archive.org/web/20100607130435/http://www.sinodefence.com/army/tank/type88.asp)

  - [Chinese Defence
    Today-96式](https://web.archive.org/web/20070629210025/http://www.sinodefence.com/army/tank/type96.asp)

  - [Deagel.com-96式](http://www.deagel.com/pandora/type-96_pm00285001.aspx)

  - [Modellversium.de-96式](http://www.modellversium.de/galerie/artikel.php?id=1344)

  - [Global Defence
    Forum-96式](http://www.globaldefenceforum.com/showthread.php?t=1224)

[Category:中國坦克](../Category/中國坦克.md "wikilink")
[Category:主战坦克](../Category/主战坦克.md "wikilink")

1.  [mdc.idv.tw-88式坦克](http://www.mdc.idv.tw/mdc/army/ct80-88.htm)
2.  [ZTZ88式揭秘](http://military.china.com/zh_cn/critical3/27/20051222/12968720.html)
    ，坦克装甲车辆。