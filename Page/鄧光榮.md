**鄧光榮**（人稱**大哥**，被譽為「學生王子」，洋名****，），出生於中國[廣東](../Page/廣東.md "wikilink")[順德](../Page/順德.md "wikilink")，香港著名[演員](../Page/演員.md "wikilink")、[製片人](../Page/電影監製.md "wikilink")、[導演及](../Page/電影導演.md "wikilink")[三合會會員](../Page/三合會.md "wikilink")。\[1\]曾與[張沖](../Page/張沖_\(演員\).md "wikilink")、[謝賢](../Page/謝賢.md "wikilink")、[陳自強](../Page/陳自強.md "wikilink")、[陳浩](../Page/陳浩.md "wikilink")、[秦祥林](../Page/秦祥林.md "wikilink")、[沈殿霞結拜](../Page/沈殿霞.md "wikilink")，組成六男一女的**銀色鼠隊**。

## 早年生活

鄧光榮在家中排行第四，有兩個哥哥，一個姐姐。他在旺角長大，畢業於[新法書院](../Page/新法書院.md "wikilink")，在校時與藝人[Joe
Junior為同學](../Page/Joe_Junior.md "wikilink")。

在成為演員之前，鄧光榮曾在[電台從事](../Page/電台.md "wikilink")[英文](../Page/英文.md "wikilink")[翻譯](../Page/翻譯.md "wikilink")[粵語的工作](../Page/粵語.md "wikilink")，同時也擔任過[模特兒](../Page/模特兒.md "wikilink")。原本想做一名單純的[公務員](../Page/公務員.md "wikilink")，1963年，[嶺光公司為新片](../Page/嶺光公司.md "wikilink")《[學生王子](../Page/學生王子.md "wikilink")》招考[演員](../Page/演員.md "wikilink")，鄧光榮在同學集體簽名推薦下報名參選，結果從上千名應試者中脫穎而出，因而參加演出，並且表現不俗。

1963年，當時17歲的他首次主演《學生王子》，自此開始他的演藝生涯。由於這部電影，使他獲得「學生王子」的綽號。

60年代於[香港電台擔演一套廣播劇](../Page/香港電台.md "wikilink")，名為《龍約翰》。故事講述私家偵探龍約翰，擁有超凡的身手，喜歡以硬幣當作暗器；而且還有特殊裝備，他的腋下藏有超迷你[手鎗](../Page/手鎗.md "wikilink")，他的鱷魚皮皮鞋底，也是藏了迷你手鎗，可以不動聲息地，任意射殺前面、或是後面的敵人。

2011年3月29日晚上9時許\[2\]，鄧於家中睡眠時因[心臟病突發](../Page/心臟病.md "wikilink")，家人發現其毫無反應，召救護員到場後證實死亡，享年64歲。

## 電影事業

中學畢業後，鄧光榮與[蕭芳芳](../Page/蕭芳芳.md "wikilink")、[甄珍及](../Page/甄珍.md "wikilink")[陳寶珠等於](../Page/陳寶珠.md "wikilink")60年代合作，拍攝不少以青春為題材的電影。鄧光榮經常被電影雜誌選為「最佳男演員」。

1972年應中影之邀，往台灣參加改編自女作家[玄小佛小說](../Page/玄小佛.md "wikilink")、由白景瑞導演的《[白屋之戀](../Page/白屋之戀.md "wikilink")》。也從《白屋之戀》開始，陸續在[台灣拍了](../Page/台灣.md "wikilink")30多部[電影](../Page/電影.md "wikilink")，除了[李行](../Page/李行.md "wikilink")[導演的](../Page/導演.md "wikilink")《[吾土吾民](../Page/吾土吾民.md "wikilink")》是[抗日](../Page/抗日.md "wikilink")[電影外](../Page/電影.md "wikilink")，清一色幾乎是愛情文藝片，與他搭檔的女主角有[甄珍](../Page/甄珍.md "wikilink")、[林青霞](../Page/林青霞.md "wikilink")、[林鳳嬌](../Page/林鳳嬌.md "wikilink")、[張艾嘉](../Page/張艾嘉.md "wikilink")、[恬妞](../Page/恬妞.md "wikilink")、[胡燕妮](../Page/胡燕妮.md "wikilink")、[甄妮](../Page/甄妮.md "wikilink")、[余安安](../Page/余安安.md "wikilink")、[王祖賢等等](../Page/王祖賢.md "wikilink")。

有報道指由於其受歡迎程度，鄧光榮每部電影的片酬為港幣$150,000。在一篇1974年的文章中，他曾說同時參與六部電影，不過，他僅每天為一部電影拍攝，令導演覺得困難。1974年，鄧光榮不單與甄珍拍攝《冬戀》，更擔任該電影的製片。1970年代後期，持續在港台拍攝多部作品，奠定其華人影壇巨星的地位。1977年與甫出道的余安安合作，演出羅馬導演的《今生今世》，於澳門取景，開創往後事業進軍澳門的先機。當年年僅十七歲的余安安，亦成為鄧光榮愛情戲中最年輕的女主角。

1977年返回香港後，與胞兄[鄧光宙合組](../Page/鄧光宙.md "wikilink")**[大榮電影公司](../Page/大榮電影公司.md "wikilink")**，拍攝題材多以黑道居多，像《[無毒不丈夫](../Page/無毒不丈夫.md "wikilink")》、《血洗唐人街》、《怒拔太陽旗》、《義蓋雲天》、《江湖龍虎鬥》。

1987年，推出的《[江湖龍虎鬥](../Page/江湖龍虎鬥.md "wikilink")》，編劇為[王家衛](../Page/王家衛.md "wikilink")，此片讓鄧光榮發現王家衛的才華。鄧光榮於是資助王家衛開拍《[旺角卡門](../Page/旺角卡門.md "wikilink")》與《[阿飛正傳](../Page/阿飛正傳.md "wikilink")》。1990年上映的《[阿飛正傳](../Page/阿飛正傳.md "wikilink")》，拍片耗費4000萬港幣，但票房只有900萬港幣，讓鄧光榮嚴重虧損。但是此片拿下第十屆香港電影金像獎最佳電影、最佳導演及最佳男主角三個大獎。之後又來獲金紫荊「十年內最佳香港電影獎」，奠定了王家衛日後在國際電影界的聲譽。

## 軼事

1989年[六四事件後](../Page/六四事件.md "wikilink")，即[黃雀行動之初](../Page/黃雀行動.md "wikilink")，[岑建勳找曾是](../Page/岑建勳.md "wikilink")[聯公樂龍頭的鄧光榮幫忙聯絡](../Page/聯公樂.md "wikilink")[陳達鉦](../Page/陳達鉦.md "wikilink")，以打通「運輸」網絡，營救民運人士；而已故[民主派立法會議員](../Page/香港民主派.md "wikilink")[司徒華在回憶錄中指](../Page/司徒華.md "wikilink")，鄧光榮在行動中甚為低調，從未邀功，是極具義氣的人。\[3\]

2008年3月2日，在契妹[沈殿霞的追思會上](../Page/沈殿霞.md "wikilink")，鄧光榮公開要求沈的前夫[鄭少秋上台交代](../Page/鄭少秋.md "wikilink")，多年來有沒有理會沈殿霞和照顧女兒[鄭欣宜](../Page/鄭欣宜.md "wikilink")。當時台下有觀眾拍掌支持，網民則對其言論褒貶不一。

鄧光榮的契女為前香港著名電影演員[羅美薇](../Page/羅美薇.md "wikilink")，而他又曾監製電影《[旺角卡門](../Page/旺角卡門.md "wikilink")》和《[阿飛正傳](../Page/阿飛正傳.md "wikilink")》，與主角[張學友](../Page/張學友.md "wikilink")（羅夫）交情非淺，兩家來往頻繁。2011年4月，張學友在[演唱會中亦以名曲](../Page/张学友1/2世纪世界巡回演唱会.md "wikilink")《[愛是永恆](../Page/愛是永恆.md "wikilink")》獻給剛剛離世的鄧光榮\[4\]。

## 性醜聞

2018年1月10日，娱乐记者[卓伟在新浪微博公布了一个访问视频](../Page/卓伟.md "wikilink")，内容为[蓝洁瑛亲述自己早年遭到](../Page/蓝洁瑛.md "wikilink")[曾志伟和邓光荣性侵的经历](../Page/曾志伟.md "wikilink")。\[5\]\[6\]\[7\]

## 作品

### 演員

  - 1964年

<!-- end list -->

  - 學生王子

<!-- end list -->

  - 1967年

<!-- end list -->

  - 兔女郎
  - 金色聖誕夜

<!-- end list -->

  - 1968年

<!-- end list -->

  - 春曉人歸時
  - 鐵二胡

<!-- end list -->

  - 1969年

<!-- end list -->

  - 紅燈綠燈
  - 黃衫客
  - 飛男飛女
  - [合歡歌舞慶華年](../Page/合歡歌舞慶華年.md "wikilink")

<!-- end list -->

  - 1970年

<!-- end list -->

  - 歡樂時光
  - 快樂時光
  - 學府新潮
  - 瘋狂酒吧
  - 小姐不在家

<!-- end list -->

  - 1971年

<!-- end list -->

  - 我為你痴迷
  - 我的情人
  - 無敵鐵沙掌
  - 日月神童
  - 浪子與修女
  - 淘氣女歌手

<!-- end list -->

  - 1972年

<!-- end list -->

  - 偷情世界
  - 鬼馬女歌手
  - 火戀
  - 色字頭上一把刀
  - [騙術大觀](../Page/騙術大觀.md "wikilink")
  - [三十六彈腿](../Page/三十六彈腿.md "wikilink")
  - 辣手強徒
  - [白屋之戀](../Page/白屋之戀.md "wikilink")
  - 情變
  - 血愛

<!-- end list -->

  - 1973年

<!-- end list -->

  - 血洒後街
  - 蕩寇三狼
  - [彩雲飛](../Page/彩雲飛.md "wikilink")
  - [明日天涯](../Page/明日天涯.md "wikilink")
  - 小偷鬥大賊

<!-- end list -->

  - 1974年

<!-- end list -->

  - 別了親人
  - [天堂](../Page/天堂_\(電影\).md "wikilink")
  - [運財童子小祖宗](../Page/運財童子小祖宗.md "wikilink")
  - [海鷗飛處](../Page/海鷗飛處.md "wikilink")
  - 近水樓台
  - 早婚

<!-- end list -->

  - 1975年

<!-- end list -->

  - [吾土吾民](../Page/吾土吾民.md "wikilink")
  - 翩翩情

<!-- end list -->

  - 1976年

<!-- end list -->

  - 楓葉情
  - 愛的迷藏
  - 金色的影子
  - 狼來的時候
  - 海天一色
  - 戀愛功夫

<!-- end list -->

  - 1977年

<!-- end list -->

  - 波斯夕陽情
  - 今生今世
  - 牛郎艷遇
  - 出冊
  - 幽蘭在雨中

<!-- end list -->

  - 1978年

<!-- end list -->

  - 愛情九點零三分
  - [白粉雙雄](../Page/白粉雙雄.md "wikilink")
  - 追
  - 大冧巴
  - [林亞珍](../Page/林亞珍.md "wikilink")

<!-- end list -->

  - 1979年

<!-- end list -->

  - [發窮惡](../Page/發窮惡.md "wikilink")

<!-- end list -->

  - 1981年

<!-- end list -->

  - 知法犯法

<!-- end list -->

  - 1982年

<!-- end list -->

  - [血洗唐人街](../Page/血洗唐人街.md "wikilink")

<!-- end list -->

  - 1983年

<!-- end list -->

  - [怒拔太陽旗](../Page/怒拔太陽旗.md "wikilink")

<!-- end list -->

  - 1984年

<!-- end list -->

  - 有 Friend 冇驚
  - 黃禍

<!-- end list -->

  - 1987年

<!-- end list -->

  - 香港小姐寫真
  - [江湖龍虎斗](../Page/江湖龍虎斗.md "wikilink")

<!-- end list -->

  - 1990年

<!-- end list -->

  - [再戰江湖](../Page/再戰江湖.md "wikilink")

<!-- end list -->

  - 1992年

<!-- end list -->

  - 五湖四海
  - 龍騰四海

<!-- end list -->

  - 1993年

<!-- end list -->

  - [黑豹天下](../Page/黑豹天下.md "wikilink")

### 導演

  - 1977年：[出册](../Page/出册.md "wikilink")
  - 1979年：[家法](../Page/家法.md "wikilink")

### 監製

  - 1973年：亡命浪子
  - 1983年：怒拔太陽旗
  - 1984年：有Friend冇驚
  - 1985年：開心三響炮
  - 1985年：求愛反斗星
  - 1987年：江湖龍虎斗
  - 1987年：猛鬼差館
  - 1987年：香港小姐寫真
  - 1988年：我要富貴
  - 1988年：[旺角卡門](../Page/旺角卡門.md "wikilink")
  - 1989年：捉鬼大師
  - 1990年：再戰江湖
  - 1991年：[阿飛正傳](../Page/阿飛正傳.md "wikilink")
  - 1992年：龍騰四海
  - 1993年：黑豹天下

## 參考資料

## 外部链接

  - [中文電影資料庫─鄧光榮](http://www.dianying.com/ft/person/DengGuangrong)

[Guang光榮](../Category/鄧姓.md "wikilink")
[Category:香港順德人](../Category/香港順德人.md "wikilink")
[T](../Category/新法書院校友.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:香港男性模特兒](../Category/香港男性模特兒.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:文藝片演員](../Category/文藝片演員.md "wikilink")
[Category:香港導演](../Category/香港導演.md "wikilink")
[Category:電影監製](../Category/電影監製.md "wikilink")
[Category:香港泛民主派人士](../Category/香港泛民主派人士.md "wikilink")
[Category:香港三合會成員](../Category/香港三合會成員.md "wikilink")

1.  [鄧光榮電影作品列表](http://yule.sohu.com/20080304/n255524174.shtml)
2.
3.  [參與黃雀行動　救民運人士](http://hk.apple.nextmedia.com/news/art/20110330/15121133)，2011年3月30日蘋果日報
4.  [《南京日報》张学友：唱起见证岁月的老歌](http://news.163.com/11/0419/09/7209ETA100014AED.html)
5.
6.
7.  <https://weibo.com/6245651750/FDG9H1Xvp>