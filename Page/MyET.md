**MyET**是一個由[艾爾科技](../Page/艾爾科技.md "wikilink")（L Labs
Inc.）出品的專業[線上英語學習](../Page/線上英語學習.md "wikilink")[軟體](../Page/軟體.md "wikilink")，目前僅適用於[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")。

## 歷史

MyET最初是由[林宜敬博士於](../Page/林宜敬.md "wikilink")2001年所構思設計，並在[張家麟博士](../Page/張家麟.md "wikilink")、[劉建坊](../Page/劉建坊.md "wikilink")、以及[陳文仲的協助之下](../Page/陳文仲.md "wikilink")，於2002年9月完成並正式發表第1版。MyET是My
English Tutor的縮寫，最初的產品定位是一個專業英語口說訓練軟體，但是在2005年6月MyET
2.0發佈之後，產品開始強調包含聽力訓練。而在2007年6月第三版發佈之後，產品中加入了漢語學習的功能，並正式改稱為[MyET-MyCT](../Page/MyET-MyCT.md "wikilink")，除了線上英語及漢語學習之外，MyET-MyCT
3.0也強調線上學習社群以及線上[語言交換的服務](../Page/語言交換.md "wikilink")。

## 特性

MyET的核心是一個稱做[ASAS](../Page/ASAS.md "wikilink")（Automatic Speech Analysis
System）的語音分析引擎，該技術強調可以在分析學習者的發音之後，做出準確評分，並指出學習者的問題是發生在哪一個音節或是哪一個音，而問題是在發音、音調、節拍、或是重音等等。MyET並試圖提供改善建議，告訴學習者如何改善。

MyET的製造商艾爾科技本身並不製作語言學習教材，但是透過合作，目前提供了[ICRT](../Page/ICRT.md "wikilink")、[空中英語教室](../Page/空中英語教室.md "wikilink")、[常春藤美語](../Page/常春藤美語.md "wikilink")、[Longman
Pearson](../Page/Longman_Pearson.md "wikilink")、[吉的堡](../Page/吉的堡.md "wikilink")、[劍橋少兒英語](../Page/劍橋少兒英語.md "wikilink")、[志鴻](../Page/志鴻.md "wikilink")、[台灣大學華語研習所](../Page/台灣大學華語研習所.md "wikilink")、[台灣師範大學國語中心](../Page/台灣師範大學國語中心.md "wikilink")、韓國[Winglish等出版社及內容提供商的英語及華語學習教材](../Page/Winglish.md "wikilink")，因此，MyET可以被視為一個線上語言學習平臺，而不是一個單純的語言學習軟體。

一般認為，MyET的缺點在於文法及寫作部分，在這個部分，系統提供的功能不多。這可能跟程式的設計者[林宜敬偏向於](../Page/林宜敬.md "wikilink")[聽說學習法](../Page/聽說學習法.md "wikilink")（Audio
Lingual Method）與[溝通式學習觀](../Page/溝通式學習觀.md "wikilink")（Communicative
Approach），而反對[文法翻譯法](../Page/文法翻譯法.md "wikilink")（Grammar-Translation
Method）有關。

MyET目前提供繁體中文、簡體中文、英語、日語、以及韓語五種使用者介面，程式本身可以免費下載，但是某些較完整或進階的課程必須再付費之後才能使用。

## 相關鏈結

  - [官方網站](http://www.myet.com/)
  - [艾爾科技網站](http://www.llabs.com/)

[ko MyET](../Page/ko_MyET.md "wikilink")

[分類:Windows軟體](../Page/分類:Windows軟體.md "wikilink")
[分類:專有軟體](../Page/分類:專有軟體.md "wikilink")

[Category:語言學習軟體](../Category/語言學習軟體.md "wikilink")