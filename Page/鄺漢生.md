**鄺漢生**（Kwong
Hon-sang，），前[香港政府官員](../Page/香港政府.md "wikilink")，中學畢業於[九龍華仁書院](../Page/九龍華仁書院.md "wikilink")。1963年畢業於[香港大學](../Page/香港大學.md "wikilink")，同年加入政府為見習工程師。1992年晉升為首席政府工程師，1993年12月晉升[路政署署長](../Page/路政署.md "wikilink")。1995年10月出任[工務司](../Page/工務局.md "wikilink")，1997年7月出任[香港特區政府](../Page/香港特別行政區政府.md "wikilink")[工務局局長](../Page/工務局.md "wikilink")。

鄺漢生1998年年屆六十歲退休年齡，但因職務需要獲留任一年。於1999年8月7日開始退休前休假。退休後曾任[賽馬會滑坡防治研究及資訊中心董事局主席及](../Page/賽馬會.md "wikilink")[博愛醫院名譽顧問](../Page/博愛醫院.md "wikilink")。

## 榮譽

  - [金紫荊星章](../Page/金紫荊星章.md "wikilink") (1999年)
  - 非官守[太平紳士](../Page/太平紳士.md "wikilink") (2000年)

[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[H](../Category/鄺姓.md "wikilink")
[Category:九龍華仁書院校友](../Category/九龍華仁書院校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:伯明翰大學校友](../Category/伯明翰大學校友.md "wikilink")