**华罗庚**（）生于[江苏](../Page/江苏.md "wikilink")[金坛](../Page/金坛.md "wikilink")，卒于[日本](../Page/日本.md "wikilink")[东京](../Page/东京.md "wikilink")。[中国现代著名](../Page/中国.md "wikilink")[数学家和](../Page/数学家.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")，[中国科学院院士](../Page/中国科学院院士.md "wikilink")，[美国国家科学院外籍院士](../Page/美国国家科学院.md "wikilink")，[全国政协前副主席](../Page/全国政协副主席.md "wikilink")，[全国人大常委会前委员](../Page/全国人大常委会.md "wikilink")。他是中国[解析数论](../Page/解析数论.md "wikilink")、[典型群](../Page/典型群.md "wikilink")、[矩阵几何学](../Page/矩阵几何学.md "wikilink")、[自守函数论与](../Page/自守函数论.md "wikilink")[多元复变函数等很多方面研究的创始人与奠基者](../Page/多元复变函数.md "wikilink")，也是中国在世界上最有影响力的[数学家之一](../Page/数学家.md "wikilink")。

## 生平

### 早年

华罗庚童年时就读于金坛仁劬小学。1922年，华罗庚进入[金坛县立初级中学](../Page/金坛县.md "wikilink")，1925年毕业。由于家中贫穷，他只好到免学费的上海中华职业学校就读，但华罗庚还是因为家中無力提供杂费和住宿费而退学。

1926年，16岁的华罗庚回到金坛后，帮助父亲料理杂货铺，同时开始自学数学。1929年12月，华罗庚在《[科学](../Page/科学_\(中国杂志\).md "wikilink")》杂志第14卷第14期上发表《[施图姆定理之研究](../Page/施图姆定理.md "wikilink")》。1930年12月，华罗庚在《科学》第15卷第2期上发表《[苏家驹之代数的五次方程式解法不能成立之理由](../Page/苏家驹.md "wikilink")》。华罗庚崭露头角，被[清华大学数学系主任](../Page/清华大学.md "wikilink")[熊庆来教授发现](../Page/熊庆来.md "wikilink")，邀请他来清华大学。

### 成年

从1931年起，华罗庚在[清华大学边学习边工作](../Page/清华大学.md "wikilink")，仅仅用一年半时间就学完了[数学系的全部课程](../Page/清华大学数学系.md "wikilink")，并同时自学了英、法、德、日语，在国际学术杂志上发表了三篇论文。1933年被[熊庆来破格任用为](../Page/熊庆来.md "wikilink")[助教](../Page/助教.md "wikilink")，1934年9月被提拔为讲师。

1935年，[诺伯特·维纳访华](../Page/诺伯特·维纳.md "wikilink")，注意到了华罗庚，并向当时英国著名数学家[戈弗雷·哈罗德·哈代极力推荐](../Page/戈弗雷·哈罗德·哈代.md "wikilink")。1936年，华罗庚访问[剑桥大学](../Page/剑桥大学.md "wikilink")，受到了[哈代](../Page/戈弗雷·哈罗德·哈代.md "wikilink")-[李特尔伍德学派的影响](../Page/约翰·伊登斯尔·利特尔伍德.md "wikilink")，获益匪浅，在那至少发表了15篇论文。1937年回国，任清华大学正教授直到1945年，期间历经[西南联合大学时期](../Page/西南联合大学.md "wikilink")。1946年2月至5月在苏联访问，9月前往[普林斯顿高等研究院访问](../Page/普林斯顿高等研究院.md "wikilink")。1948年被[伊利諾大學聘为正教授至](../Page/伊利諾大學.md "wikilink")1950年。同年，獲選[中華民國](../Page/中華民國.md "wikilink")[中央研究院第](../Page/中央研究院.md "wikilink")1屆（數理科學組）院士\[1\]。

1950年回国，担任[清华大学数学系主任](../Page/清华大学.md "wikilink")。1952年7月，受中科院院長郭沫若之邀建立中国科学院数学研究所，并担任所长，9月加入民盟。1953年赴苏联访问，并出席了在匈牙利召开的世界数学家代表大会，以及亚太和平会议、世界和平理事会。1955年被选聘为中科院院士。由于[中国科学技术大学建校所确定的所系结合的思想](../Page/中国科学技术大学.md "wikilink")，于1958年主持创立中国科技大学数学系，并任中国科技大学副校长兼数学系主任。因此，中国科技大学设立“华罗庚大师讲席”，并于2009年与中国科学院数学与系统科学研究院合作，在数学系设立了“华罗庚班”，班上的学生在中国科大合肥校本部学习基础课，而在北京[数学与系统科学研究院学习专业选修课](../Page/中国科学院数学与系统科学研究院.md "wikilink")。\[2\]

根据[周鲸文](../Page/周鲸文.md "wikilink")《[风暴十年](../Page/风暴十年.md "wikilink")》一书指出，1950年3月16日，華羅庚一片热心的由美国回来投奔祖国，不久就碰上[文化大革命](../Page/文化大革命.md "wikilink")。因为他还保存着原有的出国[护照](../Page/护照.md "wikilink")，未加焚毁。由这个引线起就对他展开了无情的斗争，说他有“投靠[帝国主义的](../Page/帝国主义.md "wikilink")[思想](../Page/思想.md "wikilink")”，为“留后路”的打算。在斗争会上他有口难辩，觉得生活在这社会没什么意义，于是愤而[自杀](../Page/自杀.md "wikilink")，幸被发现及时，保存了生命。在文化大革命期间，凭借个人影响力亲自带队推广优选法和统筹法。

他还是著名的社会活动家，[北伐时期](../Page/北伐.md "wikilink")（1926年）加入[中国国民党](../Page/中国国民党.md "wikilink")。北伐后没有参加登记自动脱党。1942年12月，由[朱家骅介绍重新入党](../Page/朱家骅.md "wikilink")，1945年国民党六大召开前夕，被朱家骅与[陈立夫联名列入向](../Page/陈立夫.md "wikilink")[蒋介石推荐](../Page/蒋介石.md "wikilink")98名“最优秀教授党员”\[3\]。中华人民共和国建国后曾当选为一至六届[全国人大常委会委员](../Page/全国人大常委会.md "wikilink")，第六届[全国政协副主席](../Page/全国政协.md "wikilink")（1985年4月），[中国民主同盟副主席](../Page/中国民主同盟.md "wikilink")（1979年）。1979年加入[中国共产党](../Page/中国共产党.md "wikilink")。

### 晚年

1983年，被评为[第三世界科学院院士](../Page/第三世界科学院.md "wikilink")。1984年，被评为[美国科学院外籍院士](../Page/美国科学院.md "wikilink")。1985年，当选为联邦德国巴伐利亚科学院院士。1985年6月12日，華羅庚應邀到[日本](../Page/日本.md "wikilink")[东京大学作學術報告](../Page/东京大学.md "wikilink")，報告結束後由于急性心肌梗塞倒在講台上，送院後證實不治，終年74歲\[4\]。

## 贡献

[1964-07_1964年_华罗庚与数学竞赛优等奖学生进行交流.jpg](https://zh.wikipedia.org/wiki/File:1964-07_1964年_华罗庚与数学竞赛优等奖学生进行交流.jpg "fig:1964-07_1964年_华罗庚与数学竞赛优等奖学生进行交流.jpg")
他在[解析數論方面的成就尤其廣為人知](../Page/解析數論.md "wikilink")，國際間頗具名氣的「[中國解析數論學派](../Page/中國解析數論學派.md "wikilink")」即以華羅庚為首開創的學派，該學派對於[質數分佈問題與](../Page/質數分佈問題.md "wikilink")[哥德巴赫猜想作出了許多重大貢獻](../Page/哥德巴赫猜想.md "wikilink")。他在[多元複变数函数论方面的贡献](../Page/多元複变数函数论.md "wikilink")，更是影响到了世界数学的发展。按[丘成桐的看法](../Page/丘成桐.md "wikilink")，他是三个对当代世界数学潮流有影响的中国数学家之一。另两个人是[陈省身和](../Page/陈省身.md "wikilink")[冯康](../Page/冯康.md "wikilink")。同时他还培养了一批优秀学生，如[陈景润](../Page/陈景润.md "wikilink")、[王元](../Page/王元_\(院士\).md "wikilink")、[龚昇等](../Page/龚昇.md "wikilink")。

他的研究成果被国际数学界命名为“华氏定理”、“[布劳威尔-加当-华定理](../Page/布劳威尔-加当-华定理.md "wikilink")”、“华—王方法”、“”、“[華氏引理](../Page/華氏引理.md "wikilink")”等。他一生著有200篇学术论文，10部专著，还有10余部科普作品。

## 著作

科普类：

  - 《统筹方法平话》
  - 《统筹方法平话及其补充》
  - 《从杨辉三角谈起》
  - 《优选法》
  - 《优选法平话》
  - 《华罗庚科普著作选集》
  - 《从孙子的神奇妙算谈起》
  - 《聪明在于勤奋天才在于积累》

非科普类：

  - 《[数论导引](../Page/数论导引.md "wikilink")》
  - 《多复变数函数论中的曲型域的调和分析》
  - 《[高等数学引论](../Page/高等数学引论.md "wikilink")》
  - 《典型群》（与万哲先合著）
  - 《堆垒素数论》
  - 《从单位圆谈起》
  - 《典型域上的调和分析》
  - 《优选学》

## 家庭

华罗庚父亲华瑞栋，以经营小杂货铺为生。华罗庚与[吴筱之于](../Page/吴筱之.md "wikilink")1927年结婚，育有三男（华俊东、华陵、华光）三女（华顺、华苏、华密）。

## 身后纪念

  - [华罗庚数学奖](../Page/华罗庚数学奖.md "wikilink")：1992年设立，由[中国数学会与](../Page/中国数学会.md "wikilink")[湖南教育出版社共同主办](../Page/湖南教育出版社.md "wikilink")。

<!-- end list -->

  - [华罗庚公园](../Page/华罗庚公园.md "wikilink")：位于[常州市](../Page/常州市.md "wikilink")[金坛区](../Page/金坛区.md "wikilink")

<!-- end list -->

  - [华罗庚中学](../Page/华罗庚中学.md "wikilink")：母校江苏金坛县中学為了紀念华罗庚改名为华罗庚中学。

<!-- end list -->

  - “华罗庚”号综合试验舰（华罗庚舰）：[中国人民解放军海军](../Page/中国人民解放军海军.md "wikilink")[909A型综合试验舰中的](../Page/909A型.md "wikilink")892号试验舰以其名命名\[5\]。

## 参考文献

## 外部链接

  -
  - [华罗庚简介](https://web.archive.org/web/20090219033833/http://news.21cn.com/domestic/shiyong/2007/12/07/4003585.shtml)

  - [華羅庚生平](https://web.archive.org/web/20110823101057/http://www.scope.org.cn/shengpin.htm)

  - [傳記片《数学之神华罗庚》](http://jishi.cntv.cn/shuxuezhishenhualuogeng/videopage/index.shtml)
    - CNTV

  - [华罗庚](http://www.nichinoken.co.jp/column/essay/sansu/2010_m04.html)
    - 日能研

  - [叶企孙：最后的大师](https://web.archive.org/web/20111114195357/http://www.tianjindaily.com.cn/tianjin/tbbd/201008/t20100828_1585319.html)

## 相关主题

  - [中国数学史](../Page/中国数学史.md "wikilink")
  - [华罗庚数学奖](../Page/华罗庚数学奖.md "wikilink")

{{-}}

[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:數論學家](../Category/數論學家.md "wikilink")
[Category:中央研究院數理科學組院士](../Category/中央研究院數理科學組院士.md "wikilink")
[Category:中華人民共和國數學家](../Category/中華人民共和國數學家.md "wikilink")
[Category:中華民國數學家](../Category/中華民國數學家.md "wikilink")
[Category:中华人民共和国党和国家领导人
(中国科学院院士)](../Category/中华人民共和国党和国家领导人_\(中国科学院院士\).md "wikilink")
[Category:第六届全国政协副主席](../Category/第六届全国政协副主席.md "wikilink")
[Category:中国民主同盟副主席](../Category/中国民主同盟副主席.md "wikilink")
[Category:中國科學技術大學教授](../Category/中國科學技術大學教授.md "wikilink")
[Category:國立清華大學教授](../Category/國立清華大學教授.md "wikilink")
[Category:国立西南联合大学教授](../Category/国立西南联合大学教授.md "wikilink")
[Category:世界科学院院士](../Category/世界科学院院士.md "wikilink")
[Category:伊利諾大學教師](../Category/伊利諾大學教師.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:中国数学会理事长](../Category/中国数学会理事长.md "wikilink")
[Category:第六屆全國人大代表](../Category/第六屆全國人大代表.md "wikilink")
[Category:第五屆全國人大代表](../Category/第五屆全國人大代表.md "wikilink")
[Category:第四屆全國人大代表](../Category/第四屆全國人大代表.md "wikilink")
[Category:第三屆全國人大代表](../Category/第三屆全國人大代表.md "wikilink")
[Category:第二屆全國人大代表](../Category/第二屆全國人大代表.md "wikilink")
[Category:第一屆全國人大代表](../Category/第一屆全國人大代表.md "wikilink")
[Category:中国国民党党员](../Category/中国国民党党员.md "wikilink") [Category:中国共产党党员
(1979年入党)](../Category/中国共产党党员_\(1979年入党\).md "wikilink")
[Category:死在国外的中华人民共和国人](../Category/死在国外的中华人民共和国人.md "wikilink")
[Category:死在日本的中国人](../Category/死在日本的中国人.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:國立清華大學校友](../Category/國立清華大學校友.md "wikilink")
[Category:庚子赔款留学生](../Category/庚子赔款留学生.md "wikilink")
[Category:金坛人](../Category/金坛人.md "wikilink")
[L](../Category/華姓.md "wikilink")

1.  [華羅庚 逝世院士一覽表
    中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
2.  [2009中国科学技术大学
    新增设“华罗庚班”](http://news.xinhuanet.com/edu/2009-04/15/content_11188683.htm)
3.  [45年国民党“最优秀教授党员”：华罗庚陈寅恪冯友兰](http://news.ifeng.com/history/zhongguojindaishi/detail_2013_04/16/24258575_0.shtml)
4.  [华罗庚的最后一天](http://www.chinanews.com/gn/cns60/news/47.shtml).中国新闻网.
5.