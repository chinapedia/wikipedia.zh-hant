**非洲堇屬**（[學名](../Page/學名.md "wikilink")：**），也稱作**非洲紫蘿蘭**(African
Violet)，是[苦苣苔科下的一屬](../Page/苦苣苔科.md "wikilink")，原產東非，廣泛用於裁培園藝。（上述分类方法为APG
III系统）
[African_violet_(Saintpaulia).jpg](https://zh.wikipedia.org/wiki/File:African_violet_\(Saintpaulia\).jpg "fig:African_violet_(Saintpaulia).jpg")
[Saintpaulia_closeup.jpg](https://zh.wikipedia.org/wiki/File:Saintpaulia_closeup.jpg "fig:Saintpaulia_closeup.jpg")
[Saintpaulia_rupicola_-_Berlin_Botanical_Garden_-_IMG_8705.JPG](https://zh.wikipedia.org/wiki/File:Saintpaulia_rupicola_-_Berlin_Botanical_Garden_-_IMG_8705.JPG "fig:Saintpaulia_rupicola_-_Berlin_Botanical_Garden_-_IMG_8705.JPG")
[Saintpaulia_'Pink_Amiss'_02.jpg](https://zh.wikipedia.org/wiki/File:Saintpaulia_'Pink_Amiss'_02.jpg "fig:Saintpaulia_'Pink_Amiss'_02.jpg")
非洲紫蘿蘭為[雙子葉植物](../Page/雙子葉.md "wikilink")，即種子有兩片子葉；花瓣數目為四、五或其倍數。此屬特點是葉被剪下後，能在水中長出根，以至整株新植物。花顏色有紫、紅、白、綠等。
[Saintpaulia_after_propagation.jpg](https://zh.wikipedia.org/wiki/File:Saintpaulia_after_propagation.jpg "fig:Saintpaulia_after_propagation.jpg")


## 外部連結

  - [台灣非洲堇聯誼會](https://web.archive.org/web/20080415222720/http://tavcctfu.myweb.hinet.net/)
  - [非洲紫羅蘭之友 -
    新品種分享](https://web.archive.org/web/20100612160912/http://www.stanko.coz.bz/)

[Category:苦苣苔科](../Category/苦苣苔科.md "wikilink")
[Category:观赏植物](../Category/观赏植物.md "wikilink")