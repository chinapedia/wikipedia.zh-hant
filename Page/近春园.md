[近春园.png](https://zh.wikipedia.org/wiki/File:近春园.png "fig:近春园.png")**近春园**是清朝皇家园林。现位于[清华大学校内](../Page/清华大学.md "wikilink")。

## 历史

近春园原为[清](../Page/清朝.md "wikilink")[咸丰皇帝做皇子时的旧居](../Page/咸丰皇帝.md "wikilink")，原为[康熙皇帝的](../Page/康熙皇帝.md "wikilink")[熙春园的中心地带](../Page/熙春园.md "wikilink")，属于“圆明五园”之一。

清朝[道光二年](../Page/道光.md "wikilink")（1822年），熙春园分为东西两园，[工字厅以西部分称近春园](../Page/工字厅.md "wikilink")，以东称[清华园](../Page/清华园.md "wikilink")，清华园赐五皇子奕综，近春园四皇子赐奕詝（咸豐皇帝），俗称为“四爷园”（[清华大学苗日新老师查阅史料后提出当时西部称](../Page/清华大学.md "wikilink")“春泽园”，而东部称“涵德园”，清华园赐惇亲王绵恺，近春园赐瑞亲王绵忻。《清华园风物志》作者黄延復也认为“四爷”指绵忻而非奕詝）。近春园园志上写着：“水木清华，为一时之繁囿胜地。”

[咸丰十年](../Page/咸丰_\(年号\).md "wikilink")（1860年），[英法联军入侵北京](../Page/英法联军.md "wikilink")，火烧[圆明园](../Page/圆明园.md "wikilink")，近春园得以幸免。[同治时拟重修圆明园](../Page/同治.md "wikilink")，但由于经费不足以支付从新疆运送材料的运费，决定拆毁近春园，将石材用于圆明园的修缮。但拆毁近春园后由于[太平天国运动](../Page/太平天国运动.md "wikilink")，清朝国力衰微，重修圆明园计划被搁置，从此近春园沦为“荒岛”达120余年。[TsinghuaUniversitypic7.jpg](https://zh.wikipedia.org/wiki/File:TsinghuaUniversitypic7.jpg "fig:TsinghuaUniversitypic7.jpg")1913年近春园并入清华大学校区，统称清华园（在作为清代皇家园林的时代，近春园原本不是清华园的一部分）。此一景点在清华校内常被称作荒岛或荷塘，1979年清华大学重修荒岛上原有的建筑，成为近春园遗址公园，现常作为校内师生员工休闲的场所。

## 景观

近春园荷塘是[清华园水系两湖一河之一](../Page/清华园.md "wikilink")（[水木清华荷花池](../Page/水木清华.md "wikilink")、近春园荷塘和[万泉河](../Page/万泉河_\(北京\).md "wikilink")）。[朱自清教授的名篇](../Page/朱自清.md "wikilink")《[荷塘月色](../Page/荷塘月色.md "wikilink")》中的荷塘就是指这里：
[TsinghuaUniversitypic8.jpg](https://zh.wikipedia.org/wiki/File:TsinghuaUniversitypic8.jpg "fig:TsinghuaUniversitypic8.jpg")

近春园景点的核心景观是被一诺大荷塘包围的一座岛，此岛在西北侧通过一座汉白玉拱桥与岸边相连，岛东南侧另有一短桥“莲桥”。岛上有高低的山丘和树林掩映，建有“荷塘月色亭”、纪念[吴晗先生的](../Page/吴晗.md "wikilink")“晗亭”与吴晗先生雕像，并有近春园遗址纪念石碑。岛上还陈列着1979年重修荒岛时发掘出的少量近春园残垣与残存的石窗与门券。岛西南侧有一古式长廊“临漪榭”，是仿原有同名建筑旧制修复，按清宫法式，歇山起脊，金线苏彩，也是近春园内唯一象征性的遗址修复。[Tsinghua_Garden4.jpg](https://zh.wikipedia.org/wiki/File:Tsinghua_Garden4.jpg "fig:Tsinghua_Garden4.jpg")

### 荷塘

近春园荷塘因散文名篇《[荷塘月色](../Page/荷塘月色.md "wikilink")》而闻名，1927年夏，居于附近的[朱自清先生漫步于荷塘西北角](../Page/朱自清.md "wikilink")，写下《荷塘月色》一文。现在岛上建有“荷塘月色亭”以资纪念，亭内有朱自清先生手迹。但许多人常误将“[水木清华](../Page/水木清华.md "wikilink")”处的荷塘误认为是《荷塘月色》中的荷塘，这也是“清华四怪”之一：“两处荷塘争月色”。

[Category:清华大学建筑物](../Category/清华大学建筑物.md "wikilink")
[Category:清朝皇家园林](../Category/清朝皇家园林.md "wikilink")