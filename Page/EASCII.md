（，延伸美国标准信息交换码）是将[ASCII码由](../Page/ASCII.md "wikilink")7[位扩充为](../Page/位.md "wikilink")8位而成。EASCII的内码是由0到255共有256个字符组成。EASCII码比ASCII码扩充出来的符号包括表格符号、计算符号、希腊字母和特殊的拉丁符号。

## Code Page 437

[代码页437](../Page/代碼頁437.md "wikilink")（[Code
page 437](https://en.wikipedia.org/wiki/Code_page_437)）是始祖[IBM
PC](../Page/IBM_PC.md "wikilink")（[个人电脑](../Page/个人电脑.md "wikilink")）或[MS-DOS使用的字元编码](../Page/MS-DOS.md "wikilink")。又名为CP437、OEM
437\[1\] PC-8\[2\]、或MS-DOS Latin
US\[3\]。该字集包含[ASCII由](../Page/ASCII.md "wikilink")32–126的字码、附加符号、一些[希腊字母](../Page/希腊字母.md "wikilink")、图示以及制图符号。其有时也称为“OEM字型”或“high
ASCII”或“extended ASCII”\[2\]（互不兼容的众多ASCII扩充字集之一）。

[Codepage-437.png](https://zh.wikipedia.org/wiki/File:Codepage-437.png "fig:Codepage-437.png")以[VGA顯示卡顯現的](../Page/VGA.md "wikilink")「代碼頁437」\]\]

## ISO/IEC 8859-1

[ISO/IEC
8859是最常見的](../Page/ISO/IEC_8859.md "wikilink")8位字符編碼。除此之外，不同的操作系統都會有它的8位字符編碼。

下列符号是[ISO/IEC
8859-1所包含的符号](../Page/ISO/IEC_8859-1.md "wikilink")，是在[万维网和中使用的扩展](../Page/万维网.md "wikilink")[ASCII](../Page/ASCII.md "wikilink")[字符集中最常见的的符号](../Page/字符集.md "wikilink")。

<table class="wikitable">

<tr>

<th>

[符号](../Page/符号.md "wikilink")

<th>

十六进制

<th>

十进制

<th>

表示方法

<th>

名称

<tr>

<td>

 

<td>

00A0

<td>

[160](../Page/160.md "wikilink")

<td>

NBSP

<td>

[不換行空格](../Page/不換行空格.md "wikilink")

<tr>

<td>

¡

<td>

00A1

<td>

[161](../Page/161.md "wikilink")

<td>

¡

<td>

[倒感叹号](../Page/倒感叹号.md "wikilink")

<tr>

<td>

¢

<td>

00A2

<td>

[162](../Page/162.md "wikilink")

<td>

¢

<td>

[英分](../Page/英分.md "wikilink")

<tr>

<td>

£

<td>

00A3

<td>

[163](../Page/163.md "wikilink")

<td>

£

<td>

[英镑](../Page/英镑.md "wikilink")

<tr>

<td>

¤

<td>

00A4

<td>

[164](../Page/164.md "wikilink")

<td>

¤

<td>

[貨幣記號](../Page/貨幣記號.md "wikilink")

<tr>

<td>

¥

<td>

00A5

<td>

[165](../Page/165.md "wikilink")

<td>

¥

<td>

[人民币](../Page/人民币.md "wikilink")/[日元](../Page/日元.md "wikilink")

<tr>

<td>

¦

<td>

00A6

<td>

[166](../Page/166.md "wikilink")

<td>

¦

<td>

[斷豎線](../Page/斷豎線.md "wikilink")

<tr>

<td>

§

<td>

00A7

<td>

[167](../Page/167.md "wikilink")

<td>

§

<td>

[小节符](../Page/小节符.md "wikilink")

<tr>

<td>

¨

<td>

00A8

<td>

[168](../Page/168.md "wikilink")

<td>

¨

<td>

[分音符](../Page/分音符.md "wikilink")（[元音变音](../Page/元音变音.md "wikilink")）

<tr>

<td>

©

<td>

00A9

<td>

[169](../Page/169.md "wikilink")

<td>

©

<td>

[版权符](../Page/版权符.md "wikilink")

<tr>

<td>

ª

<td>

00AA

<td>

[170](../Page/170.md "wikilink")

<td>

ª

<td>

[阴性序數記號](../Page/阴性序數記號.md "wikilink")

<tr>

<td>

«

<td>

00AB

<td>

[171](../Page/171.md "wikilink")

<td>

«

<td>

[左指双尖引号](../Page/左指双尖引号.md "wikilink")

<tr>

<td>

¬

<td>

00AC

<td>

[172](../Page/172.md "wikilink")

<td>

¬

<td>

[非标记](../Page/非标记.md "wikilink")

<tr>

<td>

­

<td>

00AD

<td>

[173](../Page/173.md "wikilink")

<td>

­SHY

<td>

選擇性[連接號](../Page/連接號.md "wikilink")

<tr>

<td>

®

<td>

00AE

<td>

[174](../Page/174.md "wikilink")

<td>

®

<td>

[注册商标](../Page/注册商标.md "wikilink")

<tr>

<td>

¯

<td>

00AF

<td>

[175](../Page/175.md "wikilink")

<td>

¯

<td>

[長音符](../Page/長音符.md "wikilink")

<tr>

<td>

°

<td>

00B0

<td>

[176](../Page/176.md "wikilink")

<td>

°

<td>

[度](../Page/度.md "wikilink")

<tr>

<td>

±

<td>

00B1

<td>

[177](../Page/177.md "wikilink")

<td>

±

<td>

[正负号](../Page/正负号.md "wikilink")

<tr>

<td>

²

<td>

00B2

<td>

[178](../Page/178.md "wikilink")

<td>

²

<td>

[二次方號](../Page/二次方號.md "wikilink")

<tr>

<td>

³

<td>

00B3

<td>

[179](../Page/179.md "wikilink")

<td>

³

<td>

[三次方號](../Page/三次方號.md "wikilink")

<tr>

<td>

´

<td>

00B4

<td>

[180](../Page/180.md "wikilink")

<td>

´

<td>

[锐音符](../Page/锐音符.md "wikilink")

<tr>

<td>

µ

<td>

00B5

<td>

[181](../Page/181.md "wikilink")

<td>

µ

<td>

[微符](../Page/微符.md "wikilink")

<tr>

<td>

¶

<td>

00B6

<td>

[182](../Page/182.md "wikilink")

<td>

¶

<td>

[段落标记](../Page/段落标记.md "wikilink")

<tr>

<td>

·

<td>

00B7

<td>

[183](../Page/183.md "wikilink")

<td>

·

<td>

[中心点](../Page/中心点.md "wikilink")

<tr>

<td>

¸

<td>

00B8

<td>

[184](../Page/184.md "wikilink")

<td>

¸

<td>

[軟音符](../Page/軟音符.md "wikilink")

<tr>

<td>

¹

<td>

00B9

<td>

[185](../Page/185.md "wikilink")

<td>

¹

<td>

[一次方號](../Page/一次方號.md "wikilink")

<tr>

<td>

º

<td>

00BA

<td>

[186](../Page/186.md "wikilink")

<td>

º

<td>

[阳性序數記號](../Page/阳性序數記號.md "wikilink")

<tr>

<td>

»

<td>

00BB

<td>

[187](../Page/187.md "wikilink")

<td>

»

<td>

[右指双尖引号](../Page/右指双尖引号.md "wikilink")

<tr>

<td>

¼

<td>

00BC

<td>

[188](../Page/188.md "wikilink")

<td>

¼

<td>

[四分之一](../Page/四分之一.md "wikilink")

<tr>

<td>

½

<td>

00BD

<td>

[189](../Page/189.md "wikilink")

<td>

½

<td>

[二分之一](../Page/二分之一.md "wikilink")

<tr>

<td>

¾

<td>

00BE

<td>

[190](../Page/190.md "wikilink")

<td>

¾

<td>

[四分之三](../Page/四分之三.md "wikilink")

<tr>

<td>

¿

<td>

00BF

<td>

[191](../Page/191.md "wikilink")

<td>

¿

<td>

[竖翻问号](../Page/竖翻问号.md "wikilink")

<tr>

<td>

À

<td>

00C0

<td>

[192](../Page/192.md "wikilink")

<td>

À

<td>

[带抑音符的A](../Page/À.md "wikilink")

<tr>

<td>

Á

<td>

00C1

<td>

[193](../Page/193.md "wikilink")

<td>

Á

<td>

[带锐音符的A](../Page/Á.md "wikilink")

<tr>

<td>

Â

<td>

00C2

<td>

[194](../Page/194.md "wikilink")

<td>

Â

<td>

[带扬抑符的A](../Page/Â.md "wikilink")

<tr>

<td>

Ã

<td>

00C3

<td>

[195](../Page/195.md "wikilink")

<td>

Ã

<td>

[带颚化符的A](../Page/Ã.md "wikilink")

<tr>

<td>

Ä

<td>

00C4

<td>

[196](../Page/196.md "wikilink")

<td>

Ä

<td>

[带分音符的A](../Page/Ä.md "wikilink")

<tr>

<td>

Å

<td>

00C5

<td>

[197](../Page/197.md "wikilink")

<td>

Å

<td>

[带上圆圈的A](../Page/Å.md "wikilink")

<tr>

<td>

Æ

<td>

00C6

<td>

[198](../Page/198.md "wikilink")

<td>

Æ

<td>

[大写连字AE](../Page/Æ.md "wikilink")

<tr>

<td>

Ç

<td>

00C7

<td>

[199](../Page/199.md "wikilink")

<td>

Ç

<td>

[带下加符的C](../Page/Ç.md "wikilink")

<tr>

<td>

È

<td>

00C8

<td>

[200](../Page/200.md "wikilink")

<td>

È

<td>

[带抑音符的E](../Page/È.md "wikilink")

<tr>

<td>

É

<td>

00C9

<td>

[201](../Page/201.md "wikilink")

<td>

É

<td>

[带锐音符的E](../Page/É.md "wikilink")

<tr>

<td>

Ê

<td>

00CA

<td>

[202](../Page/202.md "wikilink")

<td>

Ê

<td>

[带扬抑符的E](../Page/Ê.md "wikilink")

<tr>

<td>

Ë

<td>

00CB

<td>

[203](../Page/203.md "wikilink")

<td>

Ë

<td>

[带分音符的E](../Page/Ë.md "wikilink")

<tr>

<td>

Ì

<td>

00CC

<td>

[204](../Page/204.md "wikilink")

<td>

Ì

<td>

[带抑音符的I](../Page/Ì.md "wikilink")

<tr>

<td>

Í

<td>

00CD

<td>

[205](../Page/205.md "wikilink")

<td>

Í

<td>

[带锐音符的I](../Page/Í.md "wikilink")

<tr>

<td>

Î

<td>

00CE

<td>

[206](../Page/206.md "wikilink")

<td>

Î

<td>

[带扬抑符的I](../Page/Î.md "wikilink")

<tr>

<td>

Ï

<td>

00CF

<td>

[207](../Page/207.md "wikilink")

<td>

Ï

<td>

[带分音符的I](../Page/Ï.md "wikilink")

<tr>

<td>

Ð

<td>

00D0

<td>

[208](../Page/208.md "wikilink")

<td>

Ð

<td>

[带橫線符的D](../Page/Ð.md "wikilink")

<tr>

<td>

Ñ

<td>

00D1

<td>

[209](../Page/209.md "wikilink")

<td>

Ñ

<td>

[带颚化符的N](../Page/Ñ.md "wikilink")

<tr>

<td>

Ò

<td>

00D2

<td>

[210](../Page/210.md "wikilink")

<td>

Ò

<td>

[带抑音符的O](../Page/Ò.md "wikilink")

<tr>

<td>

Ó

<td>

00D3

<td>

[211](../Page/211.md "wikilink")

<td>

Ó

<td>

[带锐音符的O](../Page/Ó.md "wikilink")

<tr>

<td>

Ô

<td>

00D4

<td>

[212](../Page/212.md "wikilink")

<td>

Ô

<td>

[带扬抑符的O](../Page/Ô.md "wikilink")

<tr>

<td>

Õ

<td>

00D5

<td>

[213](../Page/213.md "wikilink")

<td>

Õ

<td>

[带颚化符的O](../Page/Õ.md "wikilink")

<tr>

<td>

Ö

<td>

00D6

<td>

[214](../Page/214.md "wikilink")

<td>

Ö

<td>

[带分音符的O](../Page/Ö.md "wikilink")

<tr>

<td>

×

<td>

00D7

<td>

[215](../Page/215.md "wikilink")

<td>

×

<td>

[乘號](../Page/乘號.md "wikilink")

<tr>

<td>

Ø

<td>

00D8

<td>

[216](../Page/216.md "wikilink")

<td>

Ø

<td>

[带斜线的O](../Page/Ø.md "wikilink")

<tr>

<td>

Ù

<td>

00D9

<td>

[217](../Page/217.md "wikilink")

<td>

Ù

<td>

[带抑音符的U](../Page/Ù.md "wikilink")

<tr>

<td>

Ú

<td>

00DA

<td>

[218](../Page/218.md "wikilink")

<td>

Ú

<td>

[带锐音符的U](../Page/Ú.md "wikilink")

<tr>

<td>

Û

<td>

00DB

<td>

[219](../Page/219.md "wikilink")

<td>

Û

<td>

[带扬抑符的U](../Page/Û.md "wikilink")

<tr>

<td>

Ü

<td>

00DC

<td>

[220](../Page/220.md "wikilink")

<td>

Ü

<td>

[带分音符的U](../Page/Ü.md "wikilink")

<tr>

<td>

Ý

<td>

00DD

<td>

[221](../Page/221.md "wikilink")

<td>

Ý

<td>

[带锐音符的Y](../Page/Ý.md "wikilink")

<tr>

<td>

Þ

<td>

00DE

<td>

[222](../Page/222.md "wikilink")

<td>

Þ

<td>

[清音p](../Page/Þ.md "wikilink")

<tr>

<td>

ß

<td>

00DF

<td>

[223](../Page/223.md "wikilink")

<td>

ß

<td>

[清音s](../Page/ß.md "wikilink")

<tr>

<td>

à

<td>

00E0

<td>

[224](../Page/224.md "wikilink")

<td>

à

<td>

[带抑音符的a](../Page/à.md "wikilink")

<tr>

<td>

á

<td>

00E1

<td>

[225](../Page/225.md "wikilink")

<td>

á

<td>

[带锐音符的a](../Page/á.md "wikilink")

<tr>

<td>

â

<td>

00E2

<td>

[226](../Page/226.md "wikilink")

<td>

â

<td>

[带扬抑符的a](../Page/â.md "wikilink")

<tr>

<td>

ã

<td>

00E3

<td>

[227](../Page/227.md "wikilink")

<td>

ã

<td>

[带颚化符的a](../Page/ã.md "wikilink")

<tr>

<td>

ä

<td>

00E4

<td>

[228](../Page/228.md "wikilink")

<td>

ä

<td>

[带分音符的a](../Page/ä.md "wikilink")

<tr>

<td>

å

<td>

00E5

<td>

[229](../Page/229.md "wikilink")

<td>

å

<td>

[带分音符的a](../Page/å.md "wikilink")

<tr>

<td>

æ

<td>

00E6

<td>

[230](../Page/230.md "wikilink")

<td>

æ

<td>

[小写连字AE](../Page/æ.md "wikilink")

<tr>

<td>

ç

<td>

00E7

<td>

[231](../Page/231.md "wikilink")

<td>

ç

<td>

[带下加符的c](../Page/ç.md "wikilink")

<tr>

<td>

è

<td>

00E8

<td>

[232](../Page/232.md "wikilink")

<td>

è

<td>

[带抑音符的e](../Page/è.md "wikilink")

<tr>

<td>

é

<td>

00E9

<td>

[233](../Page/233.md "wikilink")

<td>

é

<td>

[带锐音符的e](../Page/é.md "wikilink")

<tr>

<td>

ê

<td>

00EA

<td>

[234](../Page/234.md "wikilink")

<td>

ê

<td>

[带扬抑符的e](../Page/ê.md "wikilink")

<tr>

<td>

ë

<td>

00EB

<td>

[235](../Page/235.md "wikilink")

<td>

ë

<td>

[带分音符的e](../Page/ë.md "wikilink")

<tr>

<td>

ì

<td>

00EC

<td>

[236](../Page/236.md "wikilink")

<td>

ì

<td>

[带抑音符的i](../Page/ì.md "wikilink")

<tr>

<td>

í

<td>

00ED

<td>

[237](../Page/237.md "wikilink")

<td>

í

<td>

[带锐音符的i](../Page/í.md "wikilink")

<tr>

<td>

î

<td>

00EE

<td>

[238](../Page/238.md "wikilink")

<td>

î

<td>

[带扬抑符的i](../Page/î.md "wikilink")

<tr>

<td>

ï

<td>

00EF

<td>

[239](../Page/239.md "wikilink")

<td>

ï

<td>

[带分音符的i](../Page/ï.md "wikilink")

<tr>

<td>

ð

<td>

00F0

<td>

[240](../Page/240.md "wikilink")

<td>

ð

<td>

[带斜線的d](../Page/ð.md "wikilink")

<tr>

<td>

ñ

<td>

00F1

<td>

[241](../Page/241.md "wikilink")

<td>

ñ

<td>

[带颚化符的n](../Page/ñ.md "wikilink")

<tr>

<td>

ò

<td>

00F2

<td>

[242](../Page/242.md "wikilink")

<td>

ò

<td>

[带抑音符的o](../Page/ò.md "wikilink")

<tr>

<td>

ó

<td>

00F3

<td>

[243](../Page/243.md "wikilink")

<td>

ó

<td>

[带锐音符的o](../Page/ó.md "wikilink")

<tr>

<td>

ô

<td>

00F4

<td>

[244](../Page/244.md "wikilink")

<td>

ô

<td>

[带扬抑符的o](../Page/ô.md "wikilink")

<tr>

<td>

õ

<td>

00F5

<td>

[245](../Page/245.md "wikilink")

<td>

õ

<td>

[带颚化符的o](../Page/õ.md "wikilink")

<tr>

<td>

ö

<td>

00F6

<td>

[246](../Page/246.md "wikilink")

<td>

ö

<td>

[带分音符的o](../Page/ö.md "wikilink")

<tr>

<td>

÷

<td>

00F7

<td>

[247](../Page/247.md "wikilink")

<td>

÷

<td>

[除号](../Page/÷.md "wikilink")

<tr>

<td>

ø

<td>

00F8

<td>

[248](../Page/248.md "wikilink")

<td>

ø

<td>

[带斜线的o](../Page/ø.md "wikilink")

<tr>

<td>

ù

<td>

00F9

<td>

[249](../Page/249.md "wikilink")

<td>

ù

<td>

[带抑音符的u](../Page/ù.md "wikilink")

<tr>

<td>

ú

<td>

00FA

<td>

[250](../Page/250.md "wikilink")

<td>

ú

<td>

[带锐音符的u](../Page/ú.md "wikilink")

<tr>

<td>

û

<td>

00FB

<td>

[251](../Page/251.md "wikilink")

<td>

û

<td>

[带扬抑符的u](../Page/û.md "wikilink")

<tr>

<td>

ü

<td>

00FC

<td>

[252](../Page/252.md "wikilink")

<td>

ü

<td>

[带分音符的u](../Page/ü.md "wikilink")

<tr>

<td>

ý

<td>

00FD

<td>

[253](../Page/253.md "wikilink")

<td>

ý

<td>

[带锐音符的y](../Page/ý.md "wikilink")

<tr>

<td>

þ

<td>

00FE

<td>

[254](../Page/254.md "wikilink")

<td>

þ

<td>

[小写字母Thorn](../Page/þ.md "wikilink")

<tr>

<td>

ÿ

<td>

00FF

<td>

[255](../Page/255.md "wikilink")

<td>

ÿ

<td>

[带分音符的y](../Page/ÿ.md "wikilink")

</table>

## 在计算机可读语言中的应用

在例如[C和](../Page/C.md "wikilink")[HTML这样的编程语言和文档语中](../Page/HTML.md "wikilink")，EASCII的编码原理起着重要作用。它使得计算机可读语言的解释器能以较小的开发代价支持众多编码，达到多语言支持。

EASCII的编码原理即：

  - 所有的ASCII字节(0x00至0x7F) 在所有不同的EASCII字符编码中均具有相同含义。
  - 非ASCII字节只在一般文本中使用，不在标签、关键词、或者其他对解释器有特殊含义的功能处使用。

## 相关链接

  - [ASCII Table](http://www.asciitable.com/)
  - [ASCII Code - The extended ASCII table](http://www.ascii-code.com/)

-----

## EASCII字符快速输入法

在[Windows下](../Page/Windows.md "wikilink")，打开[小键盘](../Page/小键盘.md "wikilink")，按住ALT键，然后输入EASCII码［如按住ALT键并依次输入数字键区数字2、5、5可输入带[分音符的y](../Page/分音符.md "wikilink")（[ÿ](../Page/ÿ.md "wikilink")）］。

在[Vim中](../Page/Vim.md "wikilink")，在插入模式下，可以先按Ctrl-V，再输入代表EASCII码的（至多三位）[十进制数字](../Page/十进制.md "wikilink")。

[Category:字符集](../Category/字符集.md "wikilink")