SAP NetWeaver是[SAP的集成技术平台](../Page/SAP公司.md "wikilink")，是自从[SAP Business
Suite以来的所有SAP应用的技术基础](../Page/SAP_Business_Suite.md "wikilink")。SAP
NetWeaver是一个[面向服务的应用和集成平台](../Page/面向服务.md "wikilink"),为SAP的应用提供开发和运行环境，也可以用来和其它应用和系统进行自定义的开发和集成。SAP
NetWeaver是使用开放标准和事实上的工业标准进行开发的，可以用[Microsoft](../Page/Microsoft.md "wikilink")
[.NET](../Page/Microsoft_.NET.md "wikilink")，[Sun](../Page/Sun_Microsystems.md "wikilink")
[Java
EE](../Page/Java_EE.md "wikilink")，和[IBM](../Page/IBM.md "wikilink")
[WebSphere等这些技术平台进行扩展和互操作](../Page/WebSphere.md "wikilink")。

## 概要

NetWeaver的发布被认为是SAP的一个战略举动以促使企业把他们的业务都运行在一个单一的，集成的平台上。业内分析师认为这种类型的平台实际上提供了一种“applistructure”（应用+底层设施）。通常认为这是受企业迫切要降低IT成本的需求而产生的一种新的企业架构。这种架构

1.  更灵活；
2.  能和应用更好的结合；
3.  建立在开放标准上，可以保证将来的互操作能力和横向集成；
4.  由一个财务上可以长期信赖的供应商提供。

.

SAP正在发展和系统集成商和独立软件制造商的关系，后者很多都变成了“Powered by SAP
Netweaver”。这个强大的推动力也显示了SAP对集成软件解决方案市场的控制力。

SAP
Netweaver是SAP往一个更开放的，面向服务的架构的迁移计划的一部分，也展示了把应用放在一个单一集成的平台上的技术基础和常规发布周期。

## 组成

NetWeaver本质上是SAP一系列技术产品的集成。[SAP
Web应用服务器](../Page/SAP_Web应用服务器.md "wikilink")（有时称为WebAS）是SAP应用的运行时环境--所有的[mySAP商务套件解决方案](../Page/mySAP.md "wikilink")（[SRM](../Page/SRM.md "wikilink")，[CRM](../Page/CRM.md "wikilink")，[SCM](../Page/SCM.md "wikilink")，[PLM](../Page/PLM.md "wikilink")，[ERP](../Page/ERP.md "wikilink")）都运行在SAP
WebAS上。

### 产品

组成SAP NetWeaver的核心产品包括：

  - [SAP Web应用服务器](../Page/SAP_Web应用服务器.md "wikilink")
  - [SAP交换基础设施（XI）](../Page/交换基础设施.md "wikilink")
  - [SAP企业门户](../Page/SAP企业门户.md "wikilink")
  - [SAP主数据管理（MDM）](../Page/SAP主数据管理.md "wikilink")
  - [SAP移动基础设施（MI）](../Page/SAP移动基础设施.md "wikilink")
  - [SAP商务信息仓库](../Page/SAP商务信息仓库.md "wikilink")
  - [SAP知识仓库（KW）](../Page/SAP知识仓库.md "wikilink")
  - [TREX引擎](../Page/TREX引擎.md "wikilink")

SAP也和一些硬件厂商，比如HP和IBM，一起向客户提供一套完整设备，以简化并增强NetWeaver组件的部署。这种设备的例子包括：

  - [SAP BI Accelerator](../Page/SAP_BI_Accelerator.md "wikilink")

### 开发工具

  - [SAP Enterprise Portal Content
    Studio](../Page/SAP_Enterprise_Portal_Content_Studio.md "wikilink")
  - [Visual Composer](../Page/Visual_Composer.md "wikilink")
  - [SAP NetWeaver Developer
    Studio](../Page/SAP_NetWeaver_Developer_Studio.md "wikilink")
  - [SAP Netweaver Development Infrastructure
    (NWDI)](../Page/SAP_Netweaver_Development_Infrastructure_\(NWDI\).md "wikilink")
  - Java PDK
  - [Web Dynpro](../Page/Web_Dynpro.md "wikilink")
  - Web Dynpro for Java
  - Web Dynpro for ABAP
  - [ABAP Workbench](../Page/ABAP_Workbench.md "wikilink")
  - .NET PDK

<!-- end list -->

  - [SAP Composite Application
    Framework](../Page/SAP_Composite_Application_Framework.md "wikilink")
    - 一个设计和使用[混合应用的环境](../Page/混合应用.md "wikilink")

## 特色

  - 支持[SOAP和](../Page/SOAP.md "wikilink")[Web服务](../Page/Web服务.md "wikilink")
  - 和[Java
    EE](../Page/Java_EE.md "wikilink")（[WebSphere](../Page/WebSphere.md "wikilink")）的互操作能力
  - 和[.NET](../Page/Microsoft_.NET.md "wikilink")（[Microsoft](../Page/Microsoft.md "wikilink")）的互操作能力
  - 集成[商务智能](../Page/商务智能.md "wikilink")
  - [xApps](../Page/xApps.md "wikilink")
  - [Duet](https://web.archive.org/web/20070708151335/http://www.duet.com/)

实际上，ERP正在被扩展为[商务过程管理系统](../Page/商务过程管理.md "wikilink")（BPMS），并且由于BPMS在新的应用中采用了主导的技术平台，所以不远的将来ERP的架构也应该会发生很大的变化。Allen
Davis，一个NetWeaver
xMII专家，已成功的把六西格玛场景作为一种信息应用到实时车间控制系统和工厂质量控制的实验室实验测试中。这种技术也被应用到大范围的产业和应用中。

SAP Netweaver平台仍然兼容SAP的自定义开发语言[ABAP](../Page/ABAP.md "wikilink")。

## 引用

  - Steffen Karch, Loren Heilig: *SAP NetWeaver Roadmap.* Galileo Press,
    2005, ISBN 1-59229-041-8

## 外部链接

  - [SAP
    Netweaver相关信息](https://web.archive.org/web/20051219161700/http://www.sap.com/solutions/netweaver/index.epx)
  - [SDN, SAP Developer Network - 有关于SAP
    Netweaver的大量资源](http://sdn.sap.com/)
  - [SAP的帮助文档门户](http://help.sap.com/)

[\*](../Category/SAP_NetWeaver.md "wikilink") [Category:SOA-related
product](../Category/SOA-related_product.md "wikilink")
[Category:SAP公司产品](../Category/SAP公司产品.md "wikilink")