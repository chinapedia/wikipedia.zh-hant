**SharpDevelop**是個自由的開放原始碼[整合開發環境](../Page/集成开发环境.md "wikilink")，主要用來開發支援[.NET
Framework的](../Page/.NET_Framework.md "wikilink")[C\#](../Page/C♯.md "wikilink")，[Visual
Basic
.NET](../Page/Visual_Basic_.NET.md "wikilink")（VB.NET）與[BOO等](../Page/BOO.md "wikilink")[程式語言](../Page/程式語言.md "wikilink")。

它可以用來作為[Microsoft](../Page/Microsoft.md "wikilink") [Visual Studio
.NET的替代品](../Page/Visual_Studio_.NET.md "wikilink")，[MonoDevelop曾由此分支出來](../Page/MonoDevelop.md "wikilink")。

SharpDevelop使用自己的[C\#](../Page/C♯.md "wikilink")、[Visual Basic
.NET解析器來進行代碼自動完成](../Page/Visual_Basic_.NET.md "wikilink")。這部份主要是使用了修改過的Linz大學製作的[Coco/R編譯器](../Page/Coco/R.md "wikilink")，SharpDevelop原始碼包含了這個產生器。[BOO的解析器則是由Boo編譯器提供](../Page/BOO.md "wikilink")，但運算式的型別與型別介面的推導則是另外撰寫程式來完成，以便支援型別的後期賦值。

SharpDevelop 1.1可以匯入[Visual Studio
.NET的專案](../Page/Visual_Studio_.NET.md "wikilink")，SharpDevelop
2.0則是可以直接開啟並編輯。它也可以用來把原本使用VB.NET的專案轉換為使用C\#（或是反過來）。

SharpDevelop 2.0開始有了整合在裡面的除錯工具，它使用了自己的除錯函式庫，除錯函式庫主要是使用COM操作與.NET通訊。

即使SharpDevelop 2.0使用了與[Visual Studio
.NET相同的MSBuild](../Page/Visual_Studio_.NET.md "wikilink")，它仍然可以使用較舊的Framework版本（1.0、1.1）或是[Mono來進行編譯](../Page/Mono.md "wikilink")。

目前最新的穩定版本是5.1.0，已於2016年4月14日正式推出。

## 特色

SharpDevelop整合了[C\#](../Page/C♯.md "wikilink")、[Visual Basic
.NET等程式語言的Windows](../Page/Visual_Basic_.NET.md "wikilink")
Forms設計工具，除此之外還有整合好的除錯工具。[其他特色](http://www.icsharpcode.net/OpenSource/SD/Features.aspx)都列在官方網頁。

## 參見

  - [MonoDevelop](../Page/MonoDevelop.md "wikilink")

## 外部連結

  - [官方網頁](http://www.sharpdevelop.com/OpenSource/SD/)

[Category:集成开发环境](../Category/集成开发环境.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:自由整合開發環境](../Category/自由整合開發環境.md "wikilink")