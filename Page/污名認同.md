**污名認同**指一個[社群或](../Page/社群.md "wikilink")[族群對自己產生負面的](../Page/族群.md "wikilink")[認同感](../Page/認同感.md "wikilink")，因而竭力排斥自己的[母文化及表徵](../Page/母文化.md "wikilink")，並向另一[文化迅速靠攏或同化](../Page/文化.md "wikilink")。中文著作裏最早出現於[臺灣](../Page/臺灣.md "wikilink")[人類學家](../Page/人類學家.md "wikilink")[謝世忠所著](../Page/謝世忠.md "wikilink")《[認同的污名——台灣原住民的族群變遷](../Page/認同的污名——台灣原住民的族群變遷.md "wikilink")》一書，此概念亦常以其他的字眼表達，如「自我負面標籤」等，但沒有產生和「污名認同」一樣的固定術語。

## 概念

「污名認同」最早用於闡析[台灣原住民的文化現象](../Page/台灣原住民.md "wikilink")，指出由於原住民長期身處[社會的](../Page/社會.md "wikilink")[非主流的邊緣位置](../Page/非主流.md "wikilink")，不管於[經濟](../Page/經濟.md "wikilink")、社會、[政治](../Page/政治.md "wikilink")、文化等各方面皆處於劣勢，於社會多屬於低下階層，因此令原住民對自己的文化產生厭惡，認同主流社會的對自身的歧視，認為自己的文化落後、卑微、老土，結果形成對原住民文化的排斥，並刻意的迅速向[漢文化靠攏](../Page/漢文化.md "wikilink")。其後「污名認同」一詞亦有應用於其他社群（如[妓女](../Page/妓女.md "wikilink")）的研究分析之中。「污名認同」是[貼標籤和](../Page/貼標籤.md "wikilink")[污名化等](../Page/污名化.md "wikilink")[人身攻擊行為的產物](../Page/人身攻擊.md "wikilink")。

污名認同往往是文化[殖民的手段之一](../Page/殖民.md "wikilink")。殖民者透過教育、行政、法令等途徑，使殖民者的語言文化成為社會的高階文化，及知識份子的表徵，令本土文化相對成為落後、低下的符號，使殖民地人民對自身文化產生污名認同，從而產生對殖民者文化的崇敬之情。然而，由於殖民地中殖民者的統治族群一般於整個社會中佔絕對少數，因此雖然能令被統治的主流族群產生對殖民者文化的崇敬，但較難使主流族群快迅轉向至殖民者的文化。於此情況下，被殖民者的文化依然保持強大的活力，與殖民者文化分佔不同的社會階層。例如在[印度](../Page/印度.md "wikilink")，[英國文化和](../Page/英國.md "wikilink")[語言雖然享有較高的聲望](../Page/語言.md "wikilink")，但卻一直只限英國人及少數英國化的印度高尚階層所有，廣大的印度平民雖然亦經常受英國文化影響，也較崇敬英國文化，但卻沒有因此放棄自己的文化。

如果一個國家內沒有予非主流族群平等的語言和文化權益，也往往使非主流族群產生污名認同，產生類殖民效果。例如中國一些[少數民族](../Page/少數民族.md "wikilink")，如[苗族](../Page/苗族.md "wikilink")、[瑤族的若干支系](../Page/瑤族.md "wikilink")，由於其民族語言沒有通用的[文字](../Page/文字.md "wikilink")，不能以自己母語接受中學以上的教育，亦缺乏自己[母語的傳媒](../Page/母語.md "wikilink")，加上於當地社會中他們一般處於較低下的社會階層，因此在接觸漢文化較多的地方，如縣城，本土民族的家庭在家裏往往不願意教自己的子女說母語，而寧願用不純正的漢語和子女溝通。

於[社會語言學中](../Page/社會語言學.md "wikilink")，對母語的「污名認同」往往是導致社會語言轉換的主因，例如[南京人認為南京話不好聽](../Page/南京.md "wikilink")，令南京[方言迅速向](../Page/方言.md "wikilink")[普通話靠攏](../Page/普通話.md "wikilink")。

中國於清末產生對自己文化的排斥，如[五四運動提出](../Page/五四運動.md "wikilink")「[全盤西化](../Page/全盤西化.md "wikilink")」或[文化大革命提出](../Page/文化大革命.md "wikilink")「[破四舊](../Page/破四舊.md "wikilink")」。

二戰後中華民國對台灣人實行「國語運動」，在校園中汙名化[台語及其他方言](../Page/台語.md "wikilink")，說台語者則課罰金、掛狗牌等，使得台灣人對自己的族群與文化自卑，甚至認為台語就是低等人、沒水準的語言；另有部分台灣人懷念且推崇日本的殖民統治而走向極端親日，都可視為污名認同的反映，但「[狗去豬來](../Page/狗去豬來.md "wikilink")」應該是主要因素，一種汙名台灣文化、反對中國文化、認同日本文化的特殊變體。

污名認同往往會帶來嚴重的文化危機。首先在放棄自己母文化向優勢文化靠攏時，由於族群對優勢文化的認知多侷限於顯性文化方面（如語言、衣著、行為），因此在模仿優勢文化只能模仿得表面，而未能學會優勢文化更深入的[文學](../Page/文學.md "wikilink")[藝術](../Page/藝術.md "wikilink")、[道德價值](../Page/道德價值.md "wikilink")、思想[哲學](../Page/哲學.md "wikilink")，但同時又丟失了本身的文化優秀面，結果使族群文化割裂、精神空虛、道德價值淪喪。

另一個危機是自我認同的危機，劣勢族群因污名認同向優勢族群靠攏，但優勢族群依然不接納對方為自己的一份子，使劣勢族群產生自卑、自殘的傾向。於兩個族群膚色不同時，此現象尤其明顯。

## 相關條目

  - [種族主義](../Page/種族主義.md "wikilink")
  - [歧視](../Page/歧視.md "wikilink")
  - [支那](../Page/支那.md "wikilink")

[Category:文化人类学](../Category/文化人类学.md "wikilink")
[Category:社会学](../Category/社会学.md "wikilink")
[Category:種族主義](../Category/種族主義.md "wikilink")