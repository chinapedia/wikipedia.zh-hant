**新移動通-{}-訊有限公司**（[英文](../Page/英文.md "wikilink")：**Sun Mobile
Limited**）是[香港一家](../Page/香港.md "wikilink")[流動通訊服務品牌](../Page/流動通訊.md "wikilink")，定位主要為[話音通訊](../Page/語音.md "wikilink")，走大眾化市場路線，主力發展[2G](../Page/2G.md "wikilink")、基本[3G](../Page/3G.md "wikilink")（384kbps）及基本[4G](../Page/4G.md "wikilink")（速度最高為42Mbps）服務，在香港目前共有63間門市（12間位於香港島、23間位於九龍、28間位於新界）。

## 歷史

公司於1997年8月成立，原屬[新世界移動控股有限公司](../Page/新世界移動控股有限公司.md "wikilink")（新移動，）旗下的[新世界流動電話有限公司所有](../Page/新世界流動電話有限公司.md "wikilink")，透過[PCS網絡](../Page/PCS.md "wikilink")（即[GSM-1800](../Page/GSM.md "wikilink")）在香港提供[流動電話網絡服務](../Page/流動電話.md "wikilink")，曾使用「PCS
009」為[品牌名稱](../Page/品牌.md "wikilink")。2003年11月，其流動增值服務易名為「M記」，其後推出以年青人為主的手機增值服務，包括[Twins等](../Page/Twins.md "wikilink")[英皇娛樂旗下藝人作為主題的娛樂資訊服務](../Page/英皇娛樂.md "wikilink")「Twins
Mobile」及「星Mobile」。新世界傳動網的廣告宣傳曾獲得多個獎項。

2006年3月，新世界流動電話有限公司與[香港流動通訊有限公司](../Page/CSL_\(電訊\).md "wikilink")（CSL）完成合併。新世界傳動網成為香港流動通訊有限公司旗下流動網絡服務品牌（其餘兩個品牌為[1010及](../Page/1010.md "wikilink")[One2Free](../Page/One2Free.md "wikilink")），而該公司是[澳洲電訊](../Page/Telstra.md "wikilink")（Telstra）及新世界移動控股有限公司之合營公司[CSL
New World Mobility
Group](../Page/CSL_New_World_Mobility_Group.md "wikilink")。

2006年11月22日，新世界移動控股有限公司將其持有的CSL New World Mobility
Group共23.6%股權悉數售予新世界發展。\[1\]

新公司CSL New World Mobility
Group擁有提供[2G及](../Page/2G.md "wikilink")[3G服務的新世界傳動網](../Page/3G.md "wikilink")、[One2Free及](../Page/One2Free.md "wikilink")[1010三個品牌](../Page/1010.md "wikilink")，成為香港最大流動電話服務商，佔據市場超過34%。[CSL於](../Page/CSL.md "wikilink")2012年2月回應[數碼通指其營運](../Page/數碼通.md "wikilink")「好絕望」，指集團有超過320萬名客戶，冠絕全港。\[2\]

2008年10月，[CSL與](../Page/CSL.md "wikilink")[電訊數碼移動有限公司](../Page/電訊數碼.md "wikilink")（[電訊數碼](../Page/電訊數碼.md "wikilink")）成立合營公司「新世界傳動網有限公司」。

2014年5月起，屬[香港電訊旗下並持六成股權](../Page/香港電訊.md "wikilink")，[電訊數碼繼續持有其四成股權](../Page/電訊數碼.md "wikilink")。同年9月25日起，新世界傳動網有限公司（New
World Mobility）改名為新移動通訊有限公司（SUN Mobile）。

## 服務計劃

新移動通訊有限公司（SUN Mobile）的定位以低價作招徠，因此大部份月費計劃均比其他電訊商便宜。由於新移動通訊有限公司（SUN
Mobile）與[csl.及](../Page/csl..md "wikilink")[1010同屬母公司](../Page/1010.md "wikilink")[電訊盈科](../Page/電訊盈科.md "wikilink")，故此所有新移動通訊有限公司（SUN
Mobile）的客戶均可免費發送短訊至[csl.及](../Page/csl..md "wikilink")[1010網絡用戶](../Page/1010.md "wikilink")（網內短訊）。

### 2G計劃

新移動通訊有限公司（SUN
Mobile）是現時少數尚有提供純[2G語音通話計劃的電訊商之一](../Page/2G.md "wikilink")（其他尚有此類的電訊商包括有[數碼通及](../Page/數碼通.md "wikilink")[中國移動香港](../Page/中國移動香港.md "wikilink")），但因其設有特低用量計劃及收費最為低廉，故此吸引不少低用量用戶選用。此等計劃在2013年10月加價。

  - $34元（已包行政費）：全基本300分鐘\[3\]
  - $41元（已包行政費）：全基本1200分鐘\[4\]
  - $68元（豁免行政費）：全基本2300分鐘\[5\]

2014年11月1日起，流動通訊服務牌照及行政費將會調整至每月$18。

### 4G任用計劃

2011年4月28日，新世界傳動網推出了2G（[EDGE](../Page/EDGE.md "wikilink")）無限數據上網服務，是繼[中國移動香港後第二家提供廉價無限上網的公司](../Page/中國移動香港.md "wikilink")；同年12月8日再將網絡升級，成為香港首間提供[3G限速不限量上網服務的公司](../Page/3G.md "wikilink")，其低廉收費隨即吸引不少人轉台。由於此計劃成功吸引大量客戶，其後[3香港及](../Page/3香港.md "wikilink")[數碼通均推出類似的月費計劃吸客](../Page/數碼通.md "wikilink")。

而[中國移動香港雖然是最早推出低速不限量的公司](../Page/中國移動香港.md "wikilink")，但早前推出的只是[EDGE網絡無限上網](../Page/EDGE.md "wikilink")，自新移動通訊有限公司（SUN
Mobile）於12月更新系統至3G後顧客人數一直下跌。為了重新吸收舊客，在不擁有香港3G牌照下以租用[PCCW
Mobile的](../Page/電訊盈科流動通訊.md "wikilink")3G網絡方式來提供此類月費計劃，以加強競爭力。

[數碼通總裁黎大鈞曾指](../Page/數碼通.md "wikilink")，此類計劃開拓了以往市場較少接觸的低用量新客群市場。

  - 「隨意用」任用數據服務:$96（已包行政費）:無限數據(首3GB：最高下載速度為384
    kbps，其後用量：流動數據傳輸速度將會被降低及限制，但上載及下載速度不會被施加限制低於128kbps。)及全基本1700分鐘，60個多媒體短訊（MMS），上網速度上限為384kbps(首2GB)。csl.
    Wi-Fi、來電接承組合、留言信箱、來電顯示、來電待接及電話會議服務已包括在內。合約年期為一年。

2014年11月1日起，流動通訊服務牌照及行政費將會調整至每月$18。

2015年2月27日日起，更新系統至4G，成為香港首間提供4G限速不限量上網服務的公司。

新移動通訊有限公司（SUN
Mobile）有時在節日中更會回贈不同的現金劵予新上台的客戶（如在2013年新年期間新上台客戶回贈$100惠康禮劵及2張「百星酒店戲票」），故令其計劃更顯吸引。

儘管其他電訊商相繼推出如出一轍的基本3G/4G計劃，但由於其服務最為寬鬆，尤其可以作熱點分享（Tethering）（雖然官方指為維持網絡負荷正常，並不鼓勵此做法），及無硬性規定付費方法，使得一些用戶仍然以新移動通訊有限公司（SUN
Mobile）的基本3G/4G計劃戶作首選。

2016年11月5日，新移動通訊有限公司（SUN Mobile）正式公佈推出 VoLTE
語音服務，用戶可免費升級使用。享受到高速的話音通話接駁服務及清晰的視像通話。

### 4G計劃

2013年10月，新移動通訊有限公司（SUN
Mobile）推出了限速限量[4G計劃](../Page/4G.md "wikilink")，稱為4G
Lite計劃。

  - $100（已包行政費）：1GB數據及全基本2500分鐘，上網速度上限為1Mbps。合約年期為一年。
  - $150（已包行政費）：2GB數據及全基本2500分鐘，上網速度上限為2Mbps。合約年期為一年。
  - $200（已包行政費）：3GB數據及全基本3500分鐘，上網速度上限為3Mbps。合約年期為一年。

2014年11月1日起，流動通訊服務牌照及行政費將會調整至每月$18。

2014年12月5日起，4G Lite計劃被42Mbps數據服務計劃取代。

  - $146（已包行政費）：1GB數據及全基本2500分鐘，上網速度上限為42Mbps。合約年期為一年。
  - $196（已包行政費）：3GB數據及全基本3000分鐘，上網速度上限為42Mbps。合約年期為一年。
  - $256（已包行政費）：6GB數據及全基本3500分鐘，上網速度上限為42Mbps。合約年期為一年。

## 軼事

2007年7月4日，新世界傳動網在[中環](../Page/中環.md "wikilink")、[灣仔](../Page/灣仔.md "wikilink")、[尖沙咀](../Page/尖沙咀.md "wikilink")、[旺角](../Page/旺角.md "wikilink")、[上水](../Page/上水.md "wikilink")、[元朗等](../Page/元朗.md "wikilink")16家分店遭人淋[紅油及白油破壞](../Page/油漆.md "wikilink")。[香港警方已列入](../Page/香港警方.md "wikilink")[刑事毀壞案](../Page/刑事毀壞.md "wikilink")，由[西九龍總區重案組接手調查](../Page/西九龍總區.md "wikilink")。

## 參考資料

## 外部链接

  - [SUN Mobile](http://www.sunmobile.com.hk/)

[Category:澳大利亚电信](../Category/澳大利亚电信.md "wikilink")
[Category:香港電訊公司](../Category/香港電訊公司.md "wikilink")
[Category:香港移動通訊](../Category/香港移動通訊.md "wikilink")
[Category:移动电话运营商](../Category/移动电话运营商.md "wikilink")
[Category:1998年成立的公司](../Category/1998年成立的公司.md "wikilink")

1.  [新世界移動與新世界發展的買賣協議](http://www.nwd.com.hk/download/2003/a061123c_NWMH.pdf)

2.  [CSL：客戶達320萬，2011年下半年銷售收入上升12%，EBITDA增長48%](http://chinese.engadget.com/2012/02/21/csl-response-to-smartone-press-con/)
3.  [新移動通訊有限公司（SUN
    Mobile）2G月費計劃](http://www.sunmobile.com.hk/plan01/523.jhtml)
4.  [新移動通訊有限公司（SUN
    Mobile）2G月費計劃](http://www.sunmobile.com.hk/plan01/523.jhtml)
5.  [新移動通訊有限公司（SUN
    Mobile）2G月費計劃](http://www.sunmobile.com.hk/plan01/523.jhtml)