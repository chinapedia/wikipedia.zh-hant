**城区**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[石家庄市下辖的](../Page/石家庄市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。位于河北西部偏南，[滹沱河上游](../Page/滹沱河.md "wikilink")\[1\]。

## 历史

[西汉](../Page/西汉.md "wikilink")[元鼎四年](../Page/元鼎.md "wikilink")（前113年），始置城县（治所丘头）；元太祖时始称藁城县；1989年7月撤县设藁城市（县级）\[2\]，2014年9月撤市设石家庄市藁城区。\[3\]

## 行政区划

全区辖13个[镇](../Page/镇.md "wikilink")、1个[民族乡](../Page/民族乡.md "wikilink")。

  - 镇：[廉州镇](../Page/廉州镇_\(石家庄市\).md "wikilink")、[岗上镇](../Page/岗上镇.md "wikilink")、[南董镇](../Page/南董镇.md "wikilink")、[兴安镇](../Page/兴安镇_\(石家庄市\).md "wikilink")、[南孟镇](../Page/南孟镇_\(石家庄市\).md "wikilink")、[梅花镇](../Page/梅花镇_\(石家庄市\).md "wikilink")、[常安镇](../Page/常安镇_\(石家庄市\).md "wikilink")、[增村镇](../Page/增村镇.md "wikilink")、[西关镇](../Page/西关镇.md "wikilink")、[张家庄镇](../Page/张家庄镇.md "wikilink")、[贾市庄镇](../Page/贾市庄镇.md "wikilink")、[丘头镇](../Page/丘头镇.md "wikilink")、[南营镇](../Page/南营镇.md "wikilink")。
  - 民族乡：[九门回族乡](../Page/九门回族乡.md "wikilink")

## 交通

  - 铁路：[石德铁路](../Page/石德铁路.md "wikilink")（石家庄－[德州](../Page/德州市.md "wikilink")）[藁城站](../Page/藁城站.md "wikilink")
  - 公路：、、、、

## 名人

  - [何基沣](../Page/:何基灃.md "wikilink")（1898-1980），字芑荪，原国军将领，中华人民共和国政府官员。
  - [郝梦龄](../Page/郝梦龄.md "wikilink")（1892年-1937年），字锡九。抗战初期牺牲的第一位[军长](../Page/军长.md "wikilink")。
  - [赵自强](../Page/赵自强.md "wikilink")（1965年7月9日－），台湾知名舞台剧演员、儿童节目主持人。
  - [石阁老石珤](../Page/:石珤.md "wikilink")（bǎo）（公元1464——1528年），字邦彦，藁城人，代表作品《熊峰集》。因喜爱封龙山熊耳峰风景，所以别号熊峰，人称“熊峰先生”。。
  - [王若虚](../Page/:王若虛.md "wikilink")（1174年－1243年），字从之，号慵夫。河北藁城人。
    早年好学，以周昂、刘中为师。金章宗承安二年（1197年）进士，调鄜州录事，历管城、门山县令，有惠政，升国史院编修。曾奉命出使西夏，授同知泗州军州事，留为著作佐郎。参预修《宣宗实录》，迁平凉府判官，召为左司谏，转延州刺史，入翰林直学士。
  - [卢金堂](../Page/卢金堂.md "wikilink")（1910年-1986年），北京市人大前代表，河北师范大学前校长。

## 参考文献

[藁城区](../Category/藁城区.md "wikilink")
[区](../Category/石家庄县级行政区.md "wikilink")
[石家庄](../Category/河北市辖区.md "wikilink")

1.
2.
3.