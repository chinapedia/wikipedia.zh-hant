PigWIN是一个[模块化的](../Page/模块化.md "wikilink")[猪场管理软件](../Page/猪场管理软件.md "wikilink")，用户可以根据需要单独[购买相应的](../Page/购买.md "wikilink")[模块或直接购买整个](../Page/模块.md "wikilink")[套装](../Page/套装.md "wikilink")\[1\]。

## 模块列表

  - PigLITTER - [种猪群管理](../Page/种猪.md "wikilink")
  - PigGAIN - [肥育猪及销售管理](../Page/肥育猪.md "wikilink")
  - PigPAD - 数据输入及查询
  - PigHERD - 数据提取及分析
  - PigMAIN - [授权及安装](../Page/授权.md "wikilink")
  - PigBATCH - 批量数据录入
  - PigRANK - 场间对比

## 应用情况

Pigwin曾由[中国农业部饲料工业中心](../Page/中国农业部.md "wikilink")[汉化在全国推广](../Page/汉化.md "wikilink")，并在中国大型[种猪企业广泛应用](../Page/种猪.md "wikilink")。\[2\]

## 外部链接

  - [PigWIN官方首页](http://www.farmpro.co.nz/)

## 参考文献

<div class="references-small">

<references />

</div>

[Category:猪场管理软件](../Category/猪场管理软件.md "wikilink")

1.
2.