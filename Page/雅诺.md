**亞爾諾**（*Arnor*），或譯**雅諾**，是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·瑞爾·托爾金的](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")《[魔戒](../Page/魔戒.md "wikilink")》登史詩式奇幻小說裡，位於[中土大陸北方](../Page/中土大陸.md "wikilink")[伊利雅德地區的](../Page/伊利雅德.md "wikilink")[登丹人王國](../Page/登丹人.md "wikilink")。“亞爾諾”的意思是「君王的领土」（*Land
of the
King*），由[辛達林語的](../Page/辛達林語.md "wikilink")*Ara-*（高等*high,kingly*）和*dor*（土地*land*）結合而成。
[Enedwaith.jpg](https://zh.wikipedia.org/wiki/File:Enedwaith.jpg "fig:Enedwaith.jpg")

## 歷史

亞爾諾和[剛鐸於](../Page/剛鐸.md "wikilink")[第二紀元](../Page/第二紀元.md "wikilink")3320年由[伊蘭迪爾和他的兩個兒子](../Page/伊蘭迪爾.md "wikilink")[埃西鐸及](../Page/埃西鐸.md "wikilink")[安那瑞安共同建立的](../Page/安那瑞安.md "wikilink")。二個王國歷史交錯，為人所知兩國都是登丹人的流亡國度。

在亞爾諾建立之前，已經有規模可觀的[努曼諾爾殖民人口居住在那裡](../Page/努曼諾爾.md "wikilink")，他們是在[努曼諾爾帝國皇帝](../Page/努曼諾爾帝國皇帝.md "wikilink")[塔爾-曼奈德和](../Page/塔爾-曼奈德.md "wikilink")[塔爾-阿達瑞安在位時期開始移民到達亞爾諾](../Page/塔爾-阿達瑞安.md "wikilink")。但登丹人到達之時，與登丹人同一祖先的當地居民[伊甸人後裔也在](../Page/伊甸人.md "wikilink")，他們並且與早期殖民結合，很快混和出土產人口。

亞爾諾最初傾向在南部的地區和剛鐸邊界附近建立勢力，因為[諾多族最高君王](../Page/諾多族.md "wikilink")[吉爾加拉德居住在](../Page/吉爾加拉德.md "wikilink")[隆恩河](../Page/隆恩河.md "wikilink")*Lhûn*。但是，[索倫的陰影安頓在南部](../Page/索倫.md "wikilink")，這導致了人口往北遷移，亞爾諾人不同於剛鐸人，他們仍然是精靈的主要朋友，更多的知識被保存了。

亞爾諾的第二任國王埃西鐸（並且同時是剛鐸國王）於[第三紀元](../Page/第三紀元.md "wikilink")2年被[半獸人殺害](../Page/半獸人.md "wikilink")，當埃西鐸到[格拉頓平原](../Page/格拉頓平原.md "wikilink")，被半獸人伏擊，在危急之際埃西鐸戴上[至尊魔戒隱形](../Page/至尊魔戒.md "wikilink")，但至尊魔戒背叛了他，至尊魔戒變大滑落了他的手指，埃西鐸因此被發現而亂箭射死，他的三個兒子也被杀死，留在[瑞文戴爾的幼子](../Page/瑞文戴爾.md "wikilink")[瓦蘭迪爾繼承了亞爾諾和登丹人最高君王的王位](../Page/瓦蘭迪爾.md "wikilink")。但由於瓦蘭迪爾的疏忽，他和其後的承繼人一直沒有要求奪回剛鐸王位，南北王國因此分裂，但亞爾諾的統治者保留了登丹人最高君王名銜，雖然南方王國的實力遠比亞爾諾強大。

## 衰落

亞爾諾原先的首都是[伊凡丁湖](../Page/伊凡丁湖.md "wikilink")*Lake
Evendim*旁的[安努米那斯](../Page/安努米那斯.md "wikilink")，但在[第三紀元](../Page/第三紀元.md "wikilink")861年遷都於[佛諾斯特](../Page/佛諾斯特.md "wikilink")，安努米那斯人口漸漸減少。

在亞爾諾的第十任國王[艾蘭多在第三紀元](../Page/艾蘭多.md "wikilink")861年駕崩以後，他的三個兒子之間互相爭位，引發內戰。長子[艾姆拉斯要求亞爾諾王位](../Page/艾姆拉斯.md "wikilink")，但最後-{只}-能統治[雅西頓地區作為他的王國](../Page/雅西頓.md "wikilink")，另外兩個兄弟則建立了[卡多蘭和](../Page/卡多蘭.md "wikilink")[魯道爾王國](../Page/魯道爾.md "wikilink")。

重新稱呼自己為亞爾諾國王的是雅西頓國王[亞瑞吉來布一世](../Page/亞瑞吉來布一世.md "wikilink")，卡多蘭向雅西頓稱臣。但是，雅西頓最終還是被毀壞了，成了廢墟。加上國土曾遭到東北安格馬邪惡勢力的入侵，亞爾諾的人民漸漸消亡，不過王國名義上的附庸[哈比人並不受影響](../Page/哈比人.md "wikilink")，依然繼續在[夏爾過著與世無爭的悠閒生活](../Page/夏爾.md "wikilink")。而另一些人類，則改為生活在如[布理的村莊中](../Page/布理.md "wikilink")，亞爾諾僅餘的少量的登丹人移居到瑞文戴爾南部，成為行蹤飄忽的[北方遊俠](../Page/北方遊俠.md "wikilink")。

## 重聯王國

登丹人酋長[亞拉岡二世](../Page/亞拉岡二世.md "wikilink")，即剛鐸的[伊力薩王](../Page/伊力薩.md "wikilink")，統一了剛鐸及亞爾諾，成為[重聯王國國王](../Page/重聯王國.md "wikilink")。他重新下令阿努米那斯成為名義上的首都，不過伊力薩王自己依然居住在剛鐸，亞爾諾人口從此再度增長，不過領土由於夏爾的獨立而有所減少。

## 參見

  - [精靈寶鑽](../Page/精靈寶鑽.md "wikilink")
  - [魔戒](../Page/魔戒.md "wikilink")
  - [亞爾諾國王](../Page/亞爾諾國王.md "wikilink")
  - [登丹人酋長](../Page/登丹人酋長.md "wikilink")
  - [北方遊俠](../Page/北方遊俠.md "wikilink")

[de:Regionen und Orte in Tolkiens
Welt\#Arnor](../Page/de:Regionen_und_Orte_in_Tolkiens_Welt#Arnor.md "wikilink")

[A](../Category/中土大陸.md "wikilink") [A](../Category/虛構國家.md "wikilink")