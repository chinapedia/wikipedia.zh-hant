《**驚聲尖叫**》（），是於[1996年上映的](../Page/1996年電影.md "wikilink")[美國](../Page/美国.md "wikilink")[恐怖電影](../Page/恐怖電影.md "wikilink")，由[衛斯·克萊文執導](../Page/衛斯·克萊文.md "wikilink")，[凱文·威廉森擔任編劇](../Page/凱文·威廉森.md "wikilink")，主演則有[妮芙·坎貝爾](../Page/妮芙·坎貝爾.md "wikilink")、[大衛·艾奎特](../Page/大衛·艾奎特.md "wikilink")、[茱兒·芭莉摩](../Page/茱兒·芭莉摩.md "wikilink")、[柯特妮·考克斯](../Page/柯特妮·考克斯.md "wikilink")、[李佛·薛柏和](../Page/李佛·薛柏.md "wikilink")[琳達·布萊爾](../Page/琳達·布萊爾.md "wikilink")。這部電影成功於90年代復興驚悚血腥電影，和1978年電影《[月光光心慌慌](../Page/月光光心慌慌.md "wikilink")》有些相似。這部電影更以演員間的對白，藉機諷刺驚悚血腥電影的逐漸公式化。
這部電影在推出後成為了當年市場的主要商品，同時也成為1996年票房榜中最賣座的電影之一，整系列也是有史以來「最賣座的[恐怖電影](../Page/恐怖電影.md "wikilink")」。這部電影大受影評家的讚賞，他們都欣賞電影中的手法。由於這部電影在推出後大受歡迎，電影公司於1997年及2000年分別推出續集《[惊声尖叫2](../Page/惊声尖叫2.md "wikilink")》及《[惊声尖叫3](../Page/惊声尖叫3.md "wikilink")》，更在與第一部時隔15年的2011年，推出最新續集《[驚聲尖叫4](../Page/奪命狂呼4.md "wikilink")》（與2000年推出的第三集相距11年）。而劇中殺人-{}-魔戴的鬼臉面具更成為了[萬聖節的經典商品](../Page/萬聖夜.md "wikilink")。

這部電影也衍生出一些嘲諷、模仿的（黑色）喜劇，例如《[驚聲尖笑](../Page/驚聲尖笑.md "wikilink")》、《麻辣搞鬼派》等電影，其票房成績也不俗，更開拍多部續集。

## 故事簡介

本電影是一部有關殺人狂魔和驚慄的青春劇電影。故事一開始凱西在一個父母外出的夜晚，突然接到一通騷擾電話所延續下來的故事。電影中會出現大量的血腥死亡情節，而且會和一般驚慄電影的情節一樣，到了電影末段才會表明兇手的真正身份。

## 人物角色

<table>
<thead>
<tr class="header">
<th><p>角色名稱</p></th>
<th><p>演員</p></th>
<th><p>介紹</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/席妮·皮斯考克.md" title="wikilink">席妮·皮斯考克</a>（Sidney Prescott）</p></td>
<td><p><a href="../Page/妮芙·坎貝爾.md" title="wikilink">妮芙·坎貝爾</a></p></td>
<td><p>主角</p></td>
</tr>
<tr class="even">
<td><p>杜懷特“杜威”·瑞利（Dwight "Dewey" Riley）</p></td>
<td><p><a href="../Page/大衛·艾奎特.md" title="wikilink">大衛·艾奎特</a></p></td>
<td><p>警察，泰坦的哥哥</p></td>
</tr>
<tr class="odd">
<td><p>蓋兒·魏德斯（Gale Weathers）</p></td>
<td><p><a href="../Page/柯特妮·考克斯.md" title="wikilink">柯特妮·考克斯</a></p></td>
<td><p>電台記者</p></td>
</tr>
<tr class="even">
<td><p>史都·馬修（Stu Macher）</p></td>
<td><p><a href="../Page/馬修·里沃德.md" title="wikilink">馬修·利拉德</a></p></td>
<td><p>比利的好友，泰坦的男友</p></td>
</tr>
<tr class="odd">
<td><p>泰坦·瑞利（Tatum Riley）</p></td>
<td><p><a href="../Page/罗丝·麦高恩.md" title="wikilink">蘿絲·麥高文</a></p></td>
<td><p>席妮的好友，史都華女友</p></td>
</tr>
<tr class="even">
<td><p>比利·魯米斯（Billy Loomis）</p></td>
<td><p><a href="../Page/史基特·烏瑞奇.md" title="wikilink">史基特·烏瑞奇</a></p></td>
<td><p>席妮的男友</p></td>
</tr>
<tr class="odd">
<td><p>藍迪·米克斯（Randy Meeks）</p></td>
<td><p><a href="../Page/傑米·甘迺迪.md" title="wikilink">傑米·甘迺迪</a></p></td>
<td><p>席妮的同學，喜歡席妮</p></td>
</tr>
<tr class="even">
<td><p>凱西·貝克（Casey Becker）</p></td>
<td><p><a href="../Page/茱兒·芭莉摩.md" title="wikilink">茱兒·芭莉摩</a></p></td>
<td><p>席妮的同學</p></td>
</tr>
<tr class="odd">
<td><p>亨布利校長（Principal Himbry）</p></td>
<td><p><a href="../Page/亨利·溫克勒.md" title="wikilink">亨利·溫克勒</a></p></td>
<td><p>校長</p></td>
</tr>
<tr class="even">
<td><p>肯尼（Kenny）</p></td>
<td></td>
<td><p>蓋兒的同事，攝影師</p></td>
</tr>
<tr class="odd">
<td><p>史蒂芬·厄斯（Steve Orth）</p></td>
<td></td>
<td><p>凱西的男友</p></td>
</tr>
<tr class="even">
<td><p>寇頓·威瑞（Cotton Weary）</p></td>
<td><p><a href="../Page/李佛·薛柏.md" title="wikilink">李佛·薛柏</a></p></td>
<td><p>席妮母親的外遇對象</p></td>
</tr>
<tr class="odd">
<td><p>記者</p></td>
<td><p><a href="../Page/琳達·布萊爾.md" title="wikilink">琳達·布萊爾</a></p></td>
<td><p>記者</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 生存規則

## 經典名句

  - 陌生人的電話：
    你喜歡恐怖電影嗎？（Do you like scary movies?）
    哪部是你最喜歡的恐怖電影？（What's your favorite scary movie?）
  - 電影宣傳標語：
    不要接電話（Don't Answer The Phone.）
    千萬別出門（Don't Open The Door.）
    別試著逃跑（Don't Try To Escape.）

## 獲獎

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>大獎</p></th>
<th><p>獎項</p></th>
<th><p>入圍</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1996</p></td>
<td></td>
<td><p>最佳電影</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/土星獎.md" title="wikilink">土星獎</a></p></td>
<td><p>最佳女演員</p></td>
<td><p><a href="../Page/妮芙·坎貝爾.md" title="wikilink">妮芙·坎貝爾</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳導演</p></td>
<td><p><a href="../Page/衛斯·克萊文.md" title="wikilink">衛斯·克萊文</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳恐怖電影</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳男配角</p></td>
<td><p><a href="../Page/史基特·烏瑞奇.md" title="wikilink">史基特·烏瑞奇</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳女配角</p></td>
<td><p><a href="../Page/茱兒·芭莉摩.md" title="wikilink">茱兒·芭莉摩</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳編劇</p></td>
<td><p><a href="../Page/凱文·威廉森.md" title="wikilink">凱文·威廉森</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p><a href="../Page/MTV電影大獎.md" title="wikilink">MTV電影大獎</a></p></td>
<td><p>最佳女演員</p></td>
<td><p>妮芙·坎貝爾</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳電影</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大獎</p></td>
<td><p>衛斯·克萊文</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 資料來源

## 外部連結

  -
  -
  -
  -
  -
[Category:1996年電影](../Category/1996年電影.md "wikilink")
[Category:1990年代恐怖片](../Category/1990年代恐怖片.md "wikilink")
[Category:1990年代驚悚片](../Category/1990年代驚悚片.md "wikilink")
[Category:1990年代懸疑片](../Category/1990年代懸疑片.md "wikilink")
[Category:美國喜劇恐怖片](../Category/美國喜劇恐怖片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國驚悚片](../Category/美國驚悚片.md "wikilink")
[Category:次元影業電影](../Category/次元影業電影.md "wikilink")
[Category:報復題材電影](../Category/報復題材電影.md "wikilink")
[Category:驚聲尖叫](../Category/驚聲尖叫.md "wikilink")