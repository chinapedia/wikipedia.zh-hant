**後北條氏**是[日本](../Page/日本.md "wikilink")[關東地方的氏族](../Page/關東地方.md "wikilink")，特別活躍於[日本戰國時代](../Page/日本戰國時代.md "wikilink")，由於與[鎌倉時代的執權](../Page/鎌倉時代.md "wikilink")[北條氏沒有直接的血緣關係](../Page/北條氏.md "wikilink")，故特此區分，或以居城為名，被稱為**小田原北條氏**，原姓伊勢。家紋與北條氏一樣，同樣為三塊鱗（形狀稍有不同）。

## 發展

原為京都的[御家人的伊勢宗瑞](../Page/御家人.md "wikilink")（即[北條早雲](../Page/北條早雲.md "wikilink")），因為個人問題辭職，後來來到了東海地區[大名](../Page/大名_\(稱謂\).md "wikilink")[今川氏的領地](../Page/今川氏.md "wikilink")，因為宗瑞解決了今川家內部的問題，宗瑞自此獲得了領地最初的本據城為[興國寺城](../Page/興國寺城.md "wikilink")，後來開始向關東相模地區侵略，1495年奪取了大森氏[小田原城](../Page/小田原城.md "wikilink")，確立了他們的根據地，宗瑞時代，已經將整個相模統一。第二代[北條氏綱將姓氏改姓北條](../Page/北條氏綱.md "wikilink")。此後，消滅了不少傳統守護代的勢力，北條氏曾經與武田軍、上杉軍還有
里見軍經交戰，北條氏的領地多由關東地區，主要由東擴張，版圖最大的時期，擁有了[相模](../Page/相模國.md "wikilink")、[伊豆](../Page/伊豆國.md "wikilink")、[武藏](../Page/武藏國.md "wikilink")、[下總和](../Page/下總國.md "wikilink")[上野](../Page/上野國.md "wikilink")，此外還擁有一部分的下野、駿河、甲斐和常陸的領土。

不過在1589年，因為[名胡桃城奪取事件](../Page/名胡桃城奪取事件.md "wikilink")，導致惹來秀吉的不滿，後北條氏決定向[豐臣秀吉抗戰](../Page/豐臣秀吉.md "wikilink")（[小田原之戰](../Page/小田原之戰.md "wikilink")），不過最終1590年7月開城投降，[氏直雖沒被處死](../Page/北條氏直.md "wikilink")，但是被流放到高野山，失去了大名的資格，一年後在高野山病死。原本打算讓氏直復歸大名的計劃被迫取消。

## 江戶時代的後北條氏

當中，[北條氏規在小田原之戰時向豐臣軍投降](../Page/北條氏規.md "wikilink")，擁有七千石的領地，嫡男[北條氏盛擁有四千石](../Page/北條氏盛.md "wikilink")，[關原之戰前](../Page/關原之戰.md "wikilink")，氏規病死一個月後，家康承認他與其父的七千石的領地，合共1萬1千石，自此成為[狹山藩](../Page/狹山藩.md "wikilink")（今[大阪府](../Page/大阪府.md "wikilink")[狹山市](../Page/狹山市.md "wikilink")）的藩主，直到幕府，子孫仍然擁有該藩的內政權。明治時代更新[華族的身分成為了子爵](../Page/華族.md "wikilink")。

此外[北條氏邦](../Page/北條氏邦.md "wikilink")（氏政之弟）的義子[北條直定成為德川家的家臣](../Page/北條直定.md "wikilink")，成為了紀州德川家的藩士。氏直之弟直重成為了[阿波國](../Page/阿波國.md "wikilink")[蜂須賀家政的家臣](../Page/蜂須賀家政.md "wikilink")，後來姓氏改回伊勢。

除了狹山藩的藩主，後北條氏子孫曾成為大名，[北條綱成兒子](../Page/北條綱成.md "wikilink")[北條氏長的兒子](../Page/北條氏長.md "wikilink")[北條氏勝成為了](../Page/北條氏勝.md "wikilink")[岩富藩的大名](../Page/岩富藩.md "wikilink")，後來義弟[北條氏重繼承](../Page/北條氏重.md "wikilink")，經過了多次調藩以後，最終成為了[掛川藩三萬石的大名](../Page/掛川藩.md "wikilink")。但是因為沒有子孫的繼承，領地被迫改易。近親[北條氏長被幕府以](../Page/北條氏長.md "wikilink")500石登用。後來成為了4千石的[旗本](../Page/旗本.md "wikilink")。

## 北條氏主要人物

[tree_of_late_hojo_clan.jpg](https://zh.wikipedia.org/wiki/File:tree_of_late_hojo_clan.jpg "fig:tree_of_late_hojo_clan.jpg")

  - [北條早雲](../Page/北條早雲.md "wikilink")（伊勢宗瑞） - 【第一代領主】
  - [北條氏綱](../Page/北條氏綱.md "wikilink") - 【第二代領主】
  - [北條氏時](../Page/北條氏時.md "wikilink") - 宗瑞次男，初代玉繩城主。
  - [北條氏廣](../Page/北條氏廣.md "wikilink") （葛山氏廣） - 宗瑞三男，繼嗣葛山家。
  - [北條幻庵](../Page/北條幻庵.md "wikilink") - 宗瑞四男。
  - [北條氏康](../Page/北條氏康.md "wikilink") - 【第三代領主】 氏綱長男。
  - [北條綱高](../Page/北條綱高.md "wikilink") （高橋綱種） - 氏綱養子，高橋高種之子。（生母為宗瑞之女）
  - [北條綱成](../Page/北條綱成.md "wikilink") - 氏綱婿養子，繼承為昌的玉繩城及宗嗣。（玉繩北條家）
  - [北條為昌](../Page/北條為昌.md "wikilink") - 氏綱三男，玉繩城城主。
  - [北條氏堯](../Page/北條氏堯.md "wikilink") - 氏綱四男。
  - [北條氏政](../Page/北條氏政.md "wikilink") - 【第四代領主】氏康次男（因長男早夭而繼為嫡子）。
  - [北條氏照](../Page/北條氏照.md "wikilink") （大石氏照） - 氏康三男，繼嗣大石家。
  - [北條氏邦](../Page/北條氏邦.md "wikilink") （藤田氏邦） - 氏康四男，繼嗣藤田家。
  - [北條氏規](../Page/北條氏規.md "wikilink") - 氏康五男。
  - [北條氏忠](../Page/北條氏忠.md "wikilink") （佐野氏忠）-　氏康六男，繼嗣佐野家。
  - [北條三郎 （上杉景虎）](../Page/上杉景虎.md "wikilink") -
    氏康八男，後來成為了[上杉謙信的養子](../Page/上杉謙信.md "wikilink")。
  - [北條氏光](../Page/北條氏光.md "wikilink") - 氏康九男。
  - [北條氏直](../Page/北條氏直.md "wikilink") - 【第五代領主】，氏政次男（因長男早夭而繼為嫡子）。
  - [太田源五郎](../Page/太田源五郎.md "wikilink") - 氏政三男，本名無從考據，只記載繼嗣武藏太田家。
  - [北條氏房](../Page/北條氏房.md "wikilink") - 氏政四男，繼嗣武藏太田家，一說與太田源五郎是同一人。
  - [北條直重 （千葉直重）](../Page/千葉直重.md "wikilink") - 氏政五男，繼嗣下總千葉家。
  - [北條直定](../Page/北條直定.md "wikilink") - 氏政六男。
  - [北條氏繁](../Page/北條氏繁.md "wikilink") - 綱成長男。
  - [北條氏舜](../Page/北條氏舜.md "wikilink") - 氏繁長男。
  - [北條氏勝](../Page/北條氏勝.md "wikilink") - 氏繁次男。
  - [北條氏成](../Page/北條氏成.md "wikilink")（直重） - 氏繁三男。
  - 千葉直胤（北條直胤） - 氏繁四男（繼嗣武蔵千葉氏）
  - [北條繁廣](../Page/北條繁廣.md "wikilink") - 氏繁五男
  - [北條氏次](../Page/北條氏次.md "wikilink") - 氏直嫡子，後北條氏衰沒之後流落仙台。

## 主要家臣

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/御宿氏.md" title="wikilink">御宿氏</a>　</li>
<li><a href="../Page/松田氏.md" title="wikilink">松田氏</a></li>
<li><a href="../Page/風魔氏.md" title="wikilink">風魔氏</a></li>
<li><a href="../Page/笠原氏.md" title="wikilink">笠原氏</a></li>
<li><a href="../Page/大道寺氏.md" title="wikilink">大道寺氏</a></li>
<li><a href="../Page/遠山氏.md" title="wikilink">遠山氏</a></li>
<li><a href="../Page/富永氏.md" title="wikilink">富永氏</a></li>
<li><a href="../Page/上田氏.md" title="wikilink">上田氏</a></li>
<li><a href="../Page/成田氏.md" title="wikilink">成田氏</a></li>
<li><a href="../Page/宇野氏.md" title="wikilink">宇野氏</a></li>
<li><a href="../Page/梶原氏.md" title="wikilink">梶原氏</a></li>
<li><a href="../Page/愛蘇氏.md" title="wikilink">愛蘇氏</a></li>
<li><a href="../Page/大石氏.md" title="wikilink">大石氏</a></li>
<li><a href="../Page/中山氏.md" title="wikilink">中山氏</a></li>
<li>下總<a href="../Page/原氏.md" title="wikilink">原氏</a></li>
<li><a href="../Page/海保氏.md" title="wikilink">海保氏</a></li>
<li><a href="../Page/玉繩北條氏.md" title="wikilink">玉繩北條氏</a></li>
</ul></td>
<td><p>御由緒家</p>
<ul>
<li><a href="../Page/大道寺重時.md" title="wikilink">大道寺重時</a></li>
<li><a href="../Page/大道寺盛昌.md" title="wikilink">大道寺盛昌</a></li>
<li><a href="../Page/大道寺政繁.md" title="wikilink">大道寺政繁</a></li>
<li><a href="../Page/大道寺直次.md" title="wikilink">大道寺直次</a></li>
<li><a href="../Page/多目元忠.md" title="wikilink">多目元忠</a></li>
</ul>
<p>御家中眾</p>
<ul>
<li><a href="../Page/伊勢貞辰.md" title="wikilink">伊勢貞辰</a></li>
<li><a href="../Page/伊勢貞運.md" title="wikilink">伊勢貞運</a></li>
<li><a href="../Page/小笠原元續.md" title="wikilink">小笠原元續</a></li>
<li><a href="../Page/小笠原康廣.md" title="wikilink">小笠原康廣</a></li>
<li><a href="../Page/小笠原長房.md" title="wikilink">小笠原長房</a></li>
</ul>
<p>江戶眾←伊豆眾</p>
<ul>
<li><a href="../Page/遠山直景.md" title="wikilink">遠山直景</a></li>
<li><a href="../Page/遠山綱景.md" title="wikilink">遠山綱景</a></li>
<li><a href="../Page/遠山政景.md" title="wikilink">遠山政景</a></li>
<li><a href="../Page/遠山康光.md" title="wikilink">遠山康光</a></li>
<li><a href="../Page/富永直勝.md" title="wikilink">富永直勝</a></li>
</ul>
<p>江戶眾</p>
<ul>
<li><a href="../Page/太田資高.md" title="wikilink">太田資高</a></li>
<li><a href="../Page/太田康資.md" title="wikilink">太田康資</a></li>
</ul>
<p>氏邦眾</p>
<ul>
<li><a href="../Page/猪俣邦憲.md" title="wikilink">猪俣邦憲</a></li>
</ul></td>
<td><p>小田原眾</p>
<ul>
<li><a href="../Page/松田盛秀.md" title="wikilink">松田盛秀</a></li>
<li><a href="../Page/松田憲秀.md" title="wikilink">松田憲秀</a></li>
<li><a href="../Page/松田秀治.md" title="wikilink">松田秀治</a></li>
<li><a href="../Page/松田康長.md" title="wikilink">松田康長</a></li>
<li><a href="../Page/松田康鄉.md" title="wikilink">松田康鄉</a></li>
<li><a href="../Page/板部岡江雪齋.md" title="wikilink">板部岡江雪齋</a></li>
</ul>
<p>御馬廻眾</p>
<ul>
<li><a href="../Page/石卷家貞.md" title="wikilink">石卷家貞</a></li>
<li><a href="../Page/石卷康保.md" title="wikilink">石卷康保</a></li>
<li><a href="../Page/石卷康敬.md" title="wikilink">石卷康敬</a></li>
</ul>
<p>伊豆眾</p>
<ul>
<li><a href="../Page/清水康英.md" title="wikilink">清水康英</a></li>
<li><a href="../Page/清水太郎左衛門尉.md" title="wikilink">清水太郎左衛門尉</a></li>
<li><a href="../Page/笠原信為.md" title="wikilink">笠原信為</a></li>
<li><a href="../Page/笠原康勝.md" title="wikilink">笠原康勝</a></li>
<li><a href="../Page/笠原政堯.md" title="wikilink">笠原政堯</a></li>
<li><a href="../Page/笠原康明.md" title="wikilink">笠原康明</a></li>
<li><a href="../Page/笠原綱信.md" title="wikilink">笠原綱信</a></li>
<li><a href="../Page/富永政家.md" title="wikilink">富永政家</a></li>
</ul>
<p>津久井眾</p>
<ul>
<li><a href="../Page/內藤綱秀.md" title="wikilink">內藤綱秀</a></li>
</ul></td>
<td><p>松山眾</p>
<ul>
<li><a href="../Page/垪和氏續.md" title="wikilink">垪和氏續</a></li>
<li><a href="../Page/垪和康忠.md" title="wikilink">垪和康忠</a></li>
<li><a href="../Page/上田朝直.md" title="wikilink">上田朝直</a></li>
<li><a href="../Page/上田憲定.md" title="wikilink">上田憲定</a></li>
<li><a href="../Page/狩野泰光.md" title="wikilink">狩野泰光</a></li>
</ul>
<p>玉繩眾</p>
<ul>
<li><a href="../Page/北條氏繁.md" title="wikilink">北條氏繁</a></li>
<li><a href="../Page/福島勝廣.md" title="wikilink">福島勝廣</a>（北條綱房）</li>
</ul>
<p>御馬廻眾</p>
<ul>
<li><a href="../Page/山角康定.md" title="wikilink">山角康定</a></li>
<li><a href="../Page/山角定勝.md" title="wikilink">山角定勝</a></li>
</ul>
<p>足輕眾</p>
<ul>
<li><a href="../Page/大藤信基.md" title="wikilink">大藤信基</a>（根來金石齋）</li>
<li><a href="../Page/大藤秀信.md" title="wikilink">大藤秀信</a></li>
<li><a href="../Page/大藤政信.md" title="wikilink">大藤政信</a></li>
<li><a href="../Page/北條綱高.md" title="wikilink">北條綱高</a></li>
<li><a href="../Page/安藤良整.md" title="wikilink">安藤良整</a></li>
<li><a href="../Page/狩野介.md" title="wikilink">狩野介</a></li>
<li><a href="../Page/大草康盛.md" title="wikilink">大草康盛</a></li>
<li><a href="../Page/伊東政世.md" title="wikilink">伊東政世</a></li>
<li><a href="../Page/師岡將景.md" title="wikilink">師岡將景</a></li>
<li><a href="../Page/御宿政友.md" title="wikilink">御宿勘兵衛</a></li>
<li><a href="../Page/宇田川氏.md" title="wikilink">宇田川喜兵衛</a></li>
</ul></td>
<td><p>水軍</p>
<ul>
<li><a href="../Page/梶原景宗.md" title="wikilink">梶原景宗</a></li>
</ul>
<p>忍者</p>
<ul>
<li><a href="../Page/風魔小太郎.md" title="wikilink">風魔小太郎</a></li>
</ul>
<p>北條氏照家臣</p>
<ul>
<li><a href="../Page/近藤綱秀.md" title="wikilink">近藤綱秀</a></li>
<li><a href="../Page/中山家範.md" title="wikilink">中山家範</a></li>
</ul>
<p>影響下</p>
<ul>
<li><a href="../Page/吉良氏朝.md" title="wikilink">吉良氏朝</a></li>
<li><a href="../Page/吉良賴康.md" title="wikilink">吉良賴康</a></li>
<li><a href="../Page/江戶賴忠.md" title="wikilink">江戶賴忠</a></li>
<li><a href="../Page/江戶朝忠.md" title="wikilink">江戶朝忠</a></li>
<li><a href="../Page/由良成繁.md" title="wikilink">由良成繁</a></li>
<li><a href="../Page/由良國繁.md" title="wikilink">由良國繁</a></li>
<li><a href="../Page/成田長泰.md" title="wikilink">成田長泰</a></li>
<li><a href="../Page/成田氏長.md" title="wikilink">成田氏長</a></li>
<li><a href="../Page/成田長忠.md" title="wikilink">成田長忠</a></li>
<li><a href="../Page/大石定久.md" title="wikilink">大石定久</a></li>
<li><a href="../Page/藤田康邦.md" title="wikilink">藤田康邦</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 主要根據地

  - 居城

<!-- end list -->

  - [興國寺城](../Page/興國寺城.md "wikilink")：今川氏分封給早雲的城池。
  - [韮山城](../Page/韮山城.md "wikilink")：早雲攻略相模國時代在伊豆國的本據城。
  - [小田原城](../Page/小田原城.md "wikilink")：自佔領相模國後成為了後北條氏的本據城。

<!-- end list -->

  - 主要支城

<!-- end list -->

  - 【相模】

　■ [玉繩城](../Page/玉繩城.md "wikilink")　■ [河村城](../Page/河村城.md "wikilink")　■
[三崎城](../Page/三崎城.md "wikilink")　■ [津久井城](../Page/津久井城.md "wikilink")

  - 【伊豆】

　■ [山中城](../Page/山中城.md "wikilink")　■ [下田城](../Page/下田城.md "wikilink")　

  - 【武藏】

　■ [江戶城](../Page/江戶城.md "wikilink")　■ [瀧山城](../Page/瀧山城.md "wikilink")　■
[八王子城](../Page/八王子城.md "wikilink")　■ [鉢形城](../Page/鉢形城.md "wikilink")　■
[岩槻城](../Page/岩槻城.md "wikilink")([岩付城](../Page/岩付城.md "wikilink"))　■
[川越城](../Page/川越城.md "wikilink")(河越城) 　■
[天神山城](../Page/天神山城_\(武蔵国\).md "wikilink")　■
[松山城](../Page/松山城.md "wikilink")　■
[忍城](../Page/忍城.md "wikilink")　■
[小机城](../Page/小机城.md "wikilink")

  - 【上野】

　■ [平井城](../Page/平井城.md "wikilink")　■
[厩橋城](../Page/厩橋城.md "wikilink")　■館林城

  - 【下總】

　■ [臼井城](../Page/臼井城.md "wikilink")　■ [関宿城](../Page/関宿城.md "wikilink")　■
[佐倉城](../Page/佐倉城.md "wikilink")(本佐倉城)　■ 矢作城　■ 助崎城　■ 小金城　■ 守谷城　■ 坂田城

  - 【上總】

　■ 土氣城　■ 東金城　■ 庁南城

  - 【常陸】

　■ 府川城　■ 江戸崎城　■ 牛久城　■ 足高城

## 明治時代及以後的主要後代

  - [北條浩](../Page/北條浩.md "wikilink")
    第四代[創價學會會長](../Page/創價學會.md "wikilink")。

## 註腳

<references/>

## 參考文獻

戰國之魁 早雲與北條一族 [新人物往來社](../Page/新人物往來社.md "wikilink") ISBN 4-404-03316-8

## 相關條目

  - [戰國時代 (日本)](../Page/戰國時代_\(日本\).md "wikilink")
  - [關東地方](../Page/關東地方.md "wikilink")
  - [關東公方](../Page/關東公方.md "wikilink")
  - [關東管領](../Page/關東管領.md "wikilink")
  - [北條氏](../Page/北條氏.md "wikilink")

[Hojyo-go](../Category/日本氏族.md "wikilink")
[Category:後北條氏](../Category/後北條氏.md "wikilink")