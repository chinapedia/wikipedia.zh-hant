**卡尔加里**（，，-{zh-hant:中國譯為**卡爾加里**;
zh-hans:[当地华人及](../Page/華裔加拿大人.md "wikilink")[港](../Page/香港.md "wikilink")[澳](../Page/澳門.md "wikilink")[台均译为](../Page/台湾.md "wikilink")**卡加利**}-，又譯**卡加立**\[1\]、**卡技利**，簡稱**卡城**）是一座位於[加拿大](../Page/加拿大.md "wikilink")[亞伯達省南部](../Page/亞伯達省.md "wikilink")[洛磯山脈的城市](../Page/洛磯山脈.md "wikilink")，亦是該省的最大城市。該市面积789.9[平方公里](../Page/平方公里.md "wikilink")，海拔约1048[米](../Page/米.md "wikilink")。

地理位置上，卡加利是位于加拿大第三大城市[溫哥華以东和第一大城市](../Page/溫哥華.md "wikilink")[多倫多以西之间最大的城市](../Page/多倫多.md "wikilink")，根據2016年的[人口普查](../Page/人口普查.md "wikilink")，市內人口有1239000人，较2006年的人口988,812人增长了10.9%\[2\]。卡加利是艾伯塔省的经济、金融和文化中心。卡加利的工程师密度是全加第一。多次被评为“世界上最干净的城市”\[3\]。1988年第15届[冬奥会在这里举行](../Page/冬奥会.md "wikilink")。2009年第40屆[國際技能競賽在這裡舉行](../Page/國際技能競賽.md "wikilink")。

## 歷史

在[歐洲人定居卡尔加里之前](../Page/歐洲.md "wikilink")，該地附近的地區便已有[原住民居住](../Page/原住民.md "wikilink")。在1787年，探險家[大衛·湯普森](../Page/大衛·湯普森.md "wikilink")（）在進行測繪期間於[弓河岸邊度過了寒冬](../Page/弓河.md "wikilink")，為有記錄首名到達卡尔加里一帶的歐洲人，而首名定居卡尔加里的歐洲人則為[約翰·格連](../Page/約翰·格連.md "wikilink")（），時為1873年\[4\]。1873年卡尔加里成为了西北骑警（，現[加拿大皇家骑警](../Page/加拿大皇家骑警.md "wikilink")）的一所驿站，當時名為比斯波堡（Fort
Brisebois），後來在1876年於[麥克勞上校](../Page/麥克勞.md "wikilink")（）的領導下以[蘇格蘭](../Page/蘇格蘭.md "wikilink")[茂爾島](../Page/茂爾島.md "wikilink")（Isle
of Mull）的地名卡加利為該驿站命名。

及后[加拿大太平洋铁路修建至此](../Page/加拿大太平洋铁路.md "wikilink")，卡尔加里逐渐发展成商業及農業重鎮，而該鐵路綫的總部至今仍設於卡加利。卡加利在1884年正式取得[鎮](../Page/鎮.md "wikilink")（）地位，選出了首任鎮長，而於10年後更取得[市](../Page/市.md "wikilink")（）的地位。

1941年这里发现了丰富的[石油和](../Page/石油.md "wikilink")[天然气](../Page/天然气.md "wikilink")，从此該市得到了迅速的发展。世界上众多的石油公司都在这里设有常驻机构，很多大的石油公司的总部就设在該市，因此卡尔加里也被称作加拿大的能源中心。1988年該市举办过[第十五届冬季奥林匹克运动会](../Page/第十五届冬季奥林匹克运动会.md "wikilink")，当时作为赛场的室内滑冰场位于知名学府[卡尔加里大学的校区内](../Page/卡尔加里大学.md "wikilink")。

## 地理

卡加利屬[洛磯山脈的山麓丘陵地帶](../Page/洛磯山脈.md "wikilink")。[Canada_35_bg_061904.jpg](https://zh.wikipedia.org/wiki/File:Canada_35_bg_061904.jpg "fig:Canada_35_bg_061904.jpg")
城市以西的[班夫国家公园是世界著名的自然风景区](../Page/班夫.md "wikilink")，也是加拿大第一所国家公园，各国游客亦慕名而来。

### 气候

卡加利的[天气素以多变著称](../Page/天气.md "wikilink")，甚至市內流傳著「一天之中你会体会到一年四季」的說法。當地[夏天出现冰雹的機率也比较大](../Page/夏天.md "wikilink")。卡加利四季分明，
[春](../Page/春.md "wikilink")、[秋](../Page/秋.md "wikilink")、[冬三季较长](../Page/冬.md "wikilink")，夏季较短，往往到了5月初还会有飘[雪的日子](../Page/雪.md "wikilink")，而最冷的月份一般集中在2月和3月。該市[阳光充沛](../Page/阳光.md "wikilink")，平均年日照时间居全国之首。

## 分區

卡加利分為東南（SE）、東北（NE）、西南（SW）、西北（NW）4個區（Quadrants），主要以[弓河](../Page/弓河.md "wikilink")（Bow
River）作南北分界，中央大街（Centre St）和南部的MacLeod徑（Macleod Trail）作東西分界。市中心（City
Centre，通稱Downtown）主要位於東南和西南區。本市共有超過180小區（Neighbourhoods），列表如下：   **NW**

  - Arbour Lake
  - Banff Trail
  - Beddington Heights
  - Bowness
  - Brentwood
  - Cambrian Heights
  - Capitol Hill
  - Charleswood
  - Charleswood Heights
  - Citadel
  - Collingwood
  - Country Hills
  - Crescent Heights
  - Crowfoot
  - Dalhousie
  - Edgemont
  - Evanston
  - Foothills Estates
  - Greenbriar
  - Greenwood
  - Hamptons
  - Hawkwood
  - Hidden Valley
  - Highwood
  - Hillhurst
  - Hounsfield Heights/Briar Hill
  - Huntington Hills
  - MacEwan
  - Montgomery
  - Kincora
  - Mount Pleasant
  - North Haven
  - North Haven Upper
  - North Mount Pleasant
  - Panorama Hills
  - Parkdale
  - Pleasant Heights
  - Point Mckay
  - Ranchlands
  - Rocky Ridge
  - Rosedale
  - Rosemont
  - Royal Mews
  - Royal Oak
  - Sandstone Valley
  - Scenic Acres
  - Silver Springs
  - St. Andrews Heights
  - Sunnyside
  - Symons Valley
      - Sherwood
      - Kincora
      - Nolan/Sage Hill
  - Thorncliffe Heights
  - Tuscany
  - University Heights
  - Upper Hillhurst
  - Valley Ridge
  - Varsity
      - Varsity Acres
      - Varsity Estates
      - Varsity Village
  - West Hillhurst

**NE**

  - Aurora Business Park
  - Abbeydale
  - Beddington Heights
  - Bridgeland
      - The Bridges
  - Castleridge
  - Coral Springs
  - Coventry Hills
  - Falconridge
  - Freeport Business Park
  - Greenview
  - Harvest Hills
  - Highland Park
  - Huntington Hills
  - Marlborough
  - Marlborough Park
  - Martindale
  - Mayland Heights
  - Monterey Park
  - Mountview
  - Pineridge
  - Regal Terrace
  - Renfrew
  - Riverside
  - Rundle
  - Saddleridge
  - Sunridge
  - St. Georges Heights
  - Taradale
  - Temple
  - Tuxedo Park
  - Vista Heights
  - Whitehorn
  - Winston Heights

**SW**

  - Altadore
      - Garrison Woods
  - Aspen Woods
      - Aspen Estates
      - Aspen Stone
  - Bankview
  - Bayview
  - Belaire
  - Braeside
  - Bridlewood
  - Britannia
  - Canyon Meadows
  - Cedarbrae
  - Chinook Park
  - Christie Park
  - Cliff Bungalow
  - Coach Hill
  - Cougar Ridge
  - Crestmont
  - Discovery Ridge
  - Eagle Ridge
  - East Springbank
  - Elbow Park
  - Elboya
  - Erlton
  - Evergreen
      - Evergreen Estates
  - Glamorgan
  - Glenbrook
  - Glencoe
  - Glendale Meadows
  - Glengary
  - Haysboro
  - Kelvin Grove
  - Killarney
  - Kingsland
  - Lakeview
  - Lincoln Park
  - Lower Mount Royal
  - Mayfair
  - Meadow Lark Park
  - Millrise
  - Mission
  - North Glenmore
  - Oakridge
  - Palliser
  - Park Hill
  - Patterson Heights
  - Prominence Point
  - Pump Hill
  - Rideau Park
  - Richmond
  - Rosscarrock
  - Roxboro
  - Rutland Park
  - Scarboro
  - Shawnessy
  - Sienna Hills
  - Signal Ridge
  - Silverado
  - Slopes
  - Somerset
  - Springbank Hill
  - Springside
  - Spruce Cliff
  - Shaganappi
  - Shawnee Slopes
  - South Calgary
  - Southwood
  - Stanley Park
  - Strathcona Heights
  - Strathcona Park
  - Sunalta
  - Sunalta West
  - Upper Mount Royal
  - Wildwood
  - Windsor Park
  - West Springs
      - Wentworth
      - West Park
      - Springside
  - Westgate
  - Woodbine
  - Woodlands

**SE**

  - Acadia
  - Albert Park
  - Applewood Park
  - Auburn Bay
  - Bonivista Downs
  - Chaparral
  - Copperfield
  - Cranston
  - Deer Ridge
  - Deer Run
  - Douglas Glen
  - Douglasdale Estates
  - Dover
  - Dover Glen
  - Erin Woods
  - Fairview
  - Forest Heights
  - Forest Lawn
  - Grandview
  - Inglewood
  - Lake Bonaventure
  - Lake Bonavista
  - Lynnwood
  - Lynnwood Ridge
  - Mahogany
  - Maple Ridge
  - McKenzie Lake
  - McKenzie Towne
  - Midnapore
  - Millican Estate
  - Mills Estates
  - Mountain Park
  - New Brighton
  - Ogden
  - Quarry Park
  - Parkland
  - Penbrooke Meadows
  - Queensland Downs
  - Radisson Heights
  - Ramsay
  - Red Carpet
  - Riverbend
  - Seton
  - Sundance
  - Valleyfield Industrial Park
  - West Dover
  - Willow Park

[Calgary_street_map.png](https://zh.wikipedia.org/wiki/File:Calgary_street_map.png "fig:Calgary_street_map.png")

## 經濟

[Calgary_Tower_on_4th_August,2008.jpg](https://zh.wikipedia.org/wiki/File:Calgary_Tower_on_4th_August,2008.jpg "fig:Calgary_Tower_on_4th_August,2008.jpg")\]\]
卡加利經濟一向以來都是以石油和天然氣以及油砂為主，有大量相關公司都於卡加利設總部，並且有大量上市公司集中在卡加利市中心。

## 教育

  - [卡尔加里大学](../Page/卡尔加里大学.md "wikilink")
  - [南亞省理工學院](../Page/南亞省理工學院.md "wikilink") （Southern Alberta Institute
    of Technology）
  - [皇家山大學](../Page/皇家山大學.md "wikilink")
  - [亞伯達藝術設計學院](../Page/亞伯達藝術設計學院.md "wikilink") （Alberta College of Art
    + Design）

## 交通

卡加利是[加拿大西部的主要運輸中心](../Page/加拿大.md "wikilink")。該市東北區的[卡加利國際機場](../Page/卡加利國際機場.md "wikilink")（YYC）是加國第4大的機場，也是一個主要貨運[物流中心](../Page/物流.md "wikilink")。卡加利國際機場有直飛航班前往加國各大主要城市、[美國](../Page/美國.md "wikilink")、[歐洲和](../Page/歐洲.md "wikilink")[中美洲等地](../Page/中美洲.md "wikilink")，亦有貨運航班往[亞洲](../Page/亞洲.md "wikilink")。由於卡加利也位於[橫加公路](../Page/橫加公路.md "wikilink")（1號公路）和[加拿大太平洋鐵路](../Page/加拿大太平洋鐵路.md "wikilink")（CPR）上，因此也是個主要物流中心。

卡城也有一個完善的街道及快速公路系統。大部份街道以數字及區別命名（例 14 Street SW 西南14街、7 Avenue SW
西南7大道，Streets街是南北走向，Avenues大道是東西走向），但在住宅小區內則大部份以小區名命名（例
西南**Wood**bine小區有**Woodbine** Blvd SW、**Wood**mont Way SW等）。
主要高速公路有[橫加公路](../Page/橫加公路.md "wikilink")、鴉童徑（Crowchild
Trail）、 鹿蹄徑（Deerfoot
Trail，向北可通往省會[愛民頓](../Page/愛民頓.md "wikilink")，往南可到达加美边境）、弓徑（Bow
Trail）、鏡蕪徑（Glenmore Trail，亞省8號公路）等。

卡城是[北美洲第一批興建現代](../Page/北美.md "wikilink")[輕鐵系統的城市之一](../Page/輕軌運輸.md "wikilink")。系統名為[C-train](../Page/卡加利輕鐵.md "wikilink")，總長60km，設有2條主要在地面營運路線（201及202），由市營的卡加利運輸局營運。該部門亦擁有800輛[巴士和營運市內超過](../Page/巴士.md "wikilink")160條巴士路綫；巴士和輕鐵系統相輔相承。

## 藝術及文化

### 特色節日

七月初，一年一度的[卡尔加里牛仔节是該市最著名的节日](../Page/卡尔加里牛仔节.md "wikilink")，每年都能吸引来自世界各地但主要是美国的游客。

### 博物館

市中心內共有數座博物館，當中[葛倫堡博物館為西加拿大最大規模的美術館](../Page/葛倫堡博物館.md "wikilink")。市內其他主要博物館包括位於[華埠的](../Page/卡加利華埠.md "wikilink")[卡城中華文化中心](../Page/卡城中華文化中心.md "wikilink")（佔地七萬平方呎）、位於奧林匹克公園的加拿大體育名人堂、軍事博物館、機場南面的機庫航空博物館（前稱：卡加利航空航天博物館）、以及[民俗公園歷史村](../Page/民俗公園歷史村.md "wikilink")。

## 體育及消遣

[NHL的](../Page/NHL.md "wikilink")[卡加利火焰亦以該市的](../Page/卡加利火焰.md "wikilink")[豐業銀行馬鞍體育館為主場](../Page/豐業銀行馬鞍體育館.md "wikilink")。

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/鳳凰城_(亞利桑那州).md" title="wikilink">鳳凰城</a></p></li>
<li><p><a href="../Page/大邱.md" title="wikilink">大邱</a></p></li>
<li><p><a href="../Page/萨拉热窝.md" title="wikilink">萨拉热窝</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/大庆.md" title="wikilink">大庆</a></p></li>
<li><p><a href="../Page/斋浦尔.md" title="wikilink">斋浦尔</a></p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/瑙卡尔潘.md" title="wikilink">瑙卡尔潘</a></p></li>
<li><p><a href="../Page/魁北克城.md" title="wikilink">魁北克城</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料及註釋

## 外部链接

  - [卡城华人网](http://www.calgarychinese.com)
  - [卡加利市政局](http://www.calgary.ca)
  - [卡加利中文网](http://www.calgarychina.ca)
  - [卡尔加里中文网](http://www.calgarychina.net)

[卡加利](../Category/卡加利.md "wikilink")
[Category:冬季奥林匹克运动会主办城市](../Category/冬季奥林匹克运动会主办城市.md "wikilink")

1.  [董事局成員](http://www.mtr.com.hk/chi/investrelation/governance.php#04)
    - [港鐵公司網頁](../Page/港鐵公司.md "wikilink")（參看《張佑啟教授》一條）
2.
3.
4.