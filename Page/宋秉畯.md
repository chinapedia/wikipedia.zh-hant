**宋秉畯**（，）是[朝鮮王朝末年的政治人物](../Page/朝鮮王朝.md "wikilink")，日本名**野田秉畯**（日韓合併前叫「野田平次郎」\[1\]），被視為[親日派與](../Page/親日派.md "wikilink")[賣國賊](../Page/叛國.md "wikilink")，**[丁未七賊](../Page/丁未七賊.md "wikilink")**之一。號**濟庵**，出身[咸鏡南道](../Page/咸鏡南道.md "wikilink")[長津](../Page/長津.md "wikilink")。

## 簡歷

宋秉畯於1871年中[武舉](../Page/武舉.md "wikilink")，出任守門官、[訓練院判官](../Page/訓練院.md "wikilink")、[司憲府監察等職](../Page/司憲府.md "wikilink")；1882年[大院君發動政變時](../Page/興宣大院君.md "wikilink")，宋秉畯逃亡而撿回一命；1884年宋秉畯前往[日本刺殺](../Page/日本.md "wikilink")[金玉均](../Page/金玉均.md "wikilink")，但由於受到金玉均人品與見解的吸引而成為金玉均的同志。1886年宋秉畯回國，被視為金玉均的同黨而入獄，但因為[閔永煥力保而出獄](../Page/閔永煥.md "wikilink")。之後出任[興海郡守等職](../Page/興海郡.md "wikilink")，但也被朝廷列入黑名單。

宋秉畯之後前往日本並在[萩從事](../Page/萩.md "wikilink")[養蠶事業](../Page/養蠶.md "wikilink")，1904年復出，並於[日俄戰爭時出任日軍](../Page/日俄戰爭.md "wikilink")[翻譯](../Page/翻譯.md "wikilink")，隨軍回國，並組織[一進會](../Page/一進會.md "wikilink")、[維新會與愛國團體](../Page/維新會.md "wikilink")[輔安會对抗](../Page/輔安會.md "wikilink")。在日本與韓國簽定[乙巳五條約後](../Page/第二次日韓協約.md "wikilink")，宋秉畯主張[大韓帝國出讓外交權給日本](../Page/大韓帝國.md "wikilink")，之後在李完用的邀請下入閣，並且於1907年[海牙密使事件中要求](../Page/海牙密使事件.md "wikilink")[高宗退位](../Page/朝鮮高宗.md "wikilink")、支持簽署[丁未七條約](../Page/第三次日韓協約.md "wikilink")。

[純宗即位後](../Page/朝鮮純宗.md "wikilink")，宋秉畯出任內部大臣與農工部大臣，後出任一進會總裁；之後前往日本，游說成立日韓聯邦，並向當時的日本首相[桂太郎提出一億日圓出賣朝鮮的構想](../Page/桂太郎.md "wikilink")。[伊藤博文遇刺後](../Page/伊藤博文.md "wikilink")，與[李容九等](../Page/李容九.md "wikilink")[一進會成員聯署](../Page/一進會.md "wikilink")「[要求韓日聯邦聲明書](../Page/要求韓日聯邦聲明書.md "wikilink")」，向[純宗](../Page/朝鮮純宗.md "wikilink")、[韓國統監](../Page/統監府.md "wikilink")[曾禰荒助](../Page/曾禰荒助.md "wikilink")、[內閣總理大臣](../Page/內閣總理大臣.md "wikilink")[李完用等人提出](../Page/李完用.md "wikilink")。[日韓併合後](../Page/日韓併合.md "wikilink")，宋秉畯被封為子爵，1920年被封為伯爵\[2\]，1925年死後追贈日本[正三位勳一等旭日桐綬章](../Page/正三位.md "wikilink")。襲爵的長子[宋鍾憲](../Page/宋鍾憲.md "wikilink")（野田鍾憲）\[3\]伯爵之後成為[貴族院議員](../Page/貴族院.md "wikilink")。

## 死後評價

宋秉畯名列韓國[親日反民族行為者名單第](../Page/親日反民族行為者.md "wikilink")290號\[4\]，理由包括：

  - [丁未七賊及](../Page/丁未七賊.md "wikilink")[一進會的成員](../Page/一進會.md "wikilink")（第1條）
  - 日本受爵者（第2條）
  - 曾於[朝鮮總督府中樞院任職](../Page/朝鮮總督府中樞院.md "wikilink")（第4條第2項）
  - 身為親日團體成員（第4條第5項）

2007年5月2日，[親日反民族行為者財産調査委員會決定沒收宋秉畯及宋鍾憲的財産](../Page/親日反民族行為者財産調査委員會.md "wikilink")。委員會在11月22日選定他為第3次沒收財産對象，同時也沒收自[親日反民族行為者財產歸屬特別法實行以後經由第三者處理的財產](../Page/親日反民族行為者財產歸屬特別法.md "wikilink")。

## 参考文献

## 参见

  - [丁未七賊](../Page/丁未七賊.md "wikilink")

[Category:朝鮮總督府中樞院顧問](../Category/朝鮮總督府中樞院顧問.md "wikilink")
[Category:朝鮮貴族 (日治時期)](../Category/朝鮮貴族_\(日治時期\).md "wikilink")
[Category:大韓帝國政治人物](../Category/大韓帝國政治人物.md "wikilink")
[Category:朝鮮王朝政治人物](../Category/朝鮮王朝政治人物.md "wikilink")
[Category:朝鮮王朝武官](../Category/朝鮮王朝武官.md "wikilink")
[송병준](../Category/咸鏡南道出身人物.md "wikilink")
[Byung-jun](../Category/宋姓.md "wikilink")
[Category:親日反民族行為認定者](../Category/親日反民族行為認定者.md "wikilink")

1.

2.  キム・サムン、『親日政治100年史』(ドンプン、1995年) 58、80頁

3.
4.