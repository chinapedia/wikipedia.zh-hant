**斗門區**是[中國](../Page/中國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[珠海市下辖](../Page/珠海市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，曾經是[縣](../Page/縣.md "wikilink")。斗門區政府驻地位於[井岸鎮](../Page/井岸鎮.md "wikilink")，現任[区委书记是](../Page/区委书记.md "wikilink")[周海金](../Page/周海金.md "wikilink")，区长是[马洪胜](../Page/马洪胜.md "wikilink")。\[1\]\[2\]，斗門有很多[趙姓的人](../Page/趙姓.md "wikilink")，因為[南宋時皇室曾逃難到這裡](../Page/南宋.md "wikilink")\[3\]。

## 历史

  - 宋朝以前，斗门一带称黄字围，属[新会潮居都](../Page/新会.md "wikilink")。
  - 宋朝绍兴二十二年（1152）黄杨山附近岛屿划归[香山县管辖](../Page/香山县.md "wikilink")，仍称潮居乡。
  - 明朝洪武十四年（1381）香山县潮居乡改称黄梁都。大沙、马墩、上横属新会县潮居都。
  - 清朝光绪六年（1880）黄梁都改称黄梁镇。清宣统二年（1910）改镇为区，按数字编列，称香山县为第八区。
  - 1925年，改称为中山县八区。
  - 1930年，改称为中山县黄梁区。
  - 1931年，区名按数字编列，称[中山县第八区](../Page/中山县.md "wikilink")。上横、大沙、马墩称新会县第八区\[4\]。
  - 1951年3月，仍为中山县第八区。上横、大沙、马墩称新会县第九区。
  - 1958年11月，称中山县斗门大公社，为中山县7个大公社之一。横粉乡、大沙乡属新会县睦洲人民公社。
  - 1965年7月19日，经国务院批准，成立斗门县，隶属广东省佛山地区。
  - 1983年7月，斗门县归属珠海经济特区，成为珠海唯一的市辖县。
  - 2001年4月，国务院批准斗门撤县建区，同年12月斗门区正式挂牌成立，称为珠海市斗门区\[5\]。

## 行政區劃

斗门区下辖5个镇和一个街道办事处。

  - [井岸镇](../Page/井岸镇.md "wikilink")
  - [乾务镇](../Page/乾务镇.md "wikilink")
  - [斗门镇](../Page/斗门镇_\(珠海市\).md "wikilink")
  - [莲洲镇](../Page/莲洲镇.md "wikilink")
  - [白蕉镇](../Page/白蕉镇.md "wikilink")
  - [白藤街道](../Page/白藤街道.md "wikilink")\[6\]

## 人口

斗门区共有常住人口340377人，其中汉族人口有337665人，占斗门区总人口99.21%；斗门区有30个少数民族，共有少数民族人口2712人，总人口0.8
%\[7\]。

## 语言

本地通行[四邑话](../Page/四邑话.md "wikilink")，斗门地区人民俗称斗门话，与[标准粤语](../Page/标准粤语.md "wikilink")（[广州话](../Page/广州话.md "wikilink")）有一定的差距。另外还有使用[蜑家話](../Page/蜑家話.md "wikilink")（水上话）和[客家话](../Page/客家话.md "wikilink")。\[8\]

## 旅遊

### 斗门十景

  - [御温泉度假村](../Page/御温泉度假村.md "wikilink")
  - [黄杨山](../Page/黄杨山.md "wikilink")
  - [耀朗假日休闲俱乐部](../Page/耀朗假日休闲俱乐部.md "wikilink")
  - [替代=位于黄杨山上的金台寺](https://zh.wikipedia.org/wiki/File:Jin_Tai_Tample_in_Zhuhai.jpg "fig:替代=位于黄杨山上的金台寺")[金台寺](../Page/金台寺.md "wikilink")
  - [菉猗堂](../Page/菉猗堂.md "wikilink")
  - [十里莲江](../Page/十里莲江.md "wikilink")
  - [替代=斗门古街的骑楼](https://zh.wikipedia.org/wiki/File:Doumen_Veranda_-_20181118-1.jpg "fig:替代=斗门古街的骑楼")[斗门古街](../Page/斗门古街.md "wikilink")
  - [尖峰山森林公园](../Page/尖峰山森林公园.md "wikilink")
  - [灯笼沙水乡](../Page/灯笼沙水乡.md "wikilink")
  - [排山古村](../Page/排山古村.md "wikilink") \[9\]

### 地方特产

  - [禾虫](../Page/禾虫.md "wikilink")
  - [蔗子狸](../Page/蔗子狸.md "wikilink")
  - [斗门南美白对虾](../Page/斗门南美白对虾.md "wikilink")
  - [黄金凤鳝](../Page/黄金凤鳝.md "wikilink")
  - [白藤莲藕](../Page/白藤莲藕.md "wikilink")
  - [斗门海鲈](../Page/斗门海鲈.md "wikilink")\[10\]
  - [黃沙蜆](../Page/黃沙蜆.md "wikilink")

## 教育

  - [斗门区第一中学](../Page/斗门区第一中学.md "wikilink")
  - [斗门区田家炳中学](../Page/斗门区田家炳中学.md "wikilink")
  - [斗门区和风中学](../Page/斗门区和风中学.md "wikilink")
  - [斗门区实验中学](../Page/斗门区实验中学.md "wikilink")
  - [斗门区第二中学](../Page/斗门区第二中学.md "wikilink")
  - [斗门区第四中学](../Page/斗门区第四中学.md "wikilink")
  - [斗门区城东中学](../Page/斗门区城东中学.md "wikilink")
  - [斗门区白藤湖中学](../Page/斗门区白藤湖中学.md "wikilink")
  - [斗门区实验小学](../Page/斗门区实验小学.md "wikilink")
  - [斗门区城南学校](../Page/斗门区城南学校.md "wikilink")
  - [斗门区白藤湖中心小学](../Page/斗门区白藤湖中心小学.md "wikilink")
  - [斗门区白藤湖幸福小学](../Page/斗门区白藤湖幸福小学.md "wikilink")
  - [斗门区教师进修学校](../Page/斗门区教师进修学校.md "wikilink")
  - [斗门区广播电视大学](../Page/斗门区广播电视大学.md "wikilink")
  - [斗门区机关幼儿园](../Page/斗门区机关幼儿园.md "wikilink")\[11\]

## 参考文献

## 外部链接

  - [珠海市斗门区政府公众信息网](http://www.doumen.gov.cn/)

[斗门区](../Category/斗门区.md "wikilink")
[Category:珠海市辖区](../Category/珠海市辖区.md "wikilink")

1.  [区长 马洪胜](http://www.doumen.gov.cn/zwgk/ldbz/zf/)
    ，珠海市斗门区政府网站，2017年07月25日。

2.  [区委书记 周海金](http://www.doumen.gov.cn/zwgk/ldbz/qw/)
    ，珠海市斗门区政府网站，2017年07月25日。

3.  [宋皇室赵氏在斗门的繁衍](http://10000xing.cn/x001/2013/0420201730.html)

4.  [斗门历史](http://www.doumen.gov.cn/zjdm/lsrw/)

5.
6.  [斗门行政规划](http://www.doumen.gov.cn/zjdm/xzhf/ja/)

7.  [斗门民族分布情况](http://www.doumen.gov.cn/zjdm/rkmz/)

8.  [斗门方言](http://www.doumentown.gov.cn/Item/Show.asp?m=1&d=1275)

9.  [斗门十景，金羊网](http://www.doumen.gov.cn/dmly/dftc/)

10. [斗门特产](http://www.doumen.gov.cn/dmly/dftc/)

11. [斗门教育总概况](http://www.dmjy.net/Item/4145.aspx)