**遠東航空104號班機**是由[高雄國際機場飛往](../Page/高雄國際機場.md "wikilink")[台北松山機場的班機](../Page/台北松山機場.md "wikilink")。1969年2月24日，一架[遠東航空的](../Page/遠東航空.md "wikilink")（）型飛機執行此航班任務時，由於引擎故障，迫降失敗，在[台灣](../Page/台灣.md "wikilink")[台南市](../Page/台南市.md "wikilink")[歸仁區山溝中墜毀](../Page/歸仁區.md "wikilink")。

## 飛機基本資料

  - 飛機機型：亨德里·佩奇信使者（）
  - 製造序號（msn）：157
  - 機身編號：B-2009

## 概要

B-2009飛機於1969年2月24日執行疏運春節假期結束的FE104航班飛行任務，從[高雄國際機場飛往](../Page/高雄國際機場.md "wikilink")[台北松山機場](../Page/台北松山機場.md "wikilink")。FE104較表定起飛時間上午11點50分延誤13分鐘，即於中午12點03分起飛。起飛10分鐘後，機長向[台南機場塔台表示](../Page/台南機場.md "wikilink")，引擎故障需要迫降，但隨即失去聯絡。不到2分鐘後，機長欲迫降在[台南市](../Page/台南市.md "wikilink")[歸仁區一處農田](../Page/歸仁區.md "wikilink")，卻落入山溝中，飛機立刻起火燃燒，機上乘員全數罹難。

## 失事原因

右發動機嚴重故障，導致飛機迅速失去高度。

## 外部連結

  - [Aviation Safety
    Net](http://aviation-safety.net/database/record.php?id=19690224-0)
    事故資料

[分類:1969年台灣](../Page/分類:1969年台灣.md "wikilink")
[分類:台南市歷史](../Page/分類:台南市歷史.md "wikilink")
[分類:歸仁區](../Page/分類:歸仁區.md "wikilink")

[Category:機械故障觸發的航空事故](../Category/機械故障觸發的航空事故.md "wikilink")
[Category:1969年航空事故](../Category/1969年航空事故.md "wikilink")