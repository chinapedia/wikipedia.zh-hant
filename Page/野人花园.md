**野人花园**（****）是一組已经解散的[澳大利亚雙人音乐團體](../Page/澳大利亚.md "wikilink")，由主唱[戴伦·海斯](../Page/戴伦·海斯.md "wikilink")（Darren
Hayes）和吉他手兼键盘手[丹尼尔·琼斯](../Page/丹尼尔·琼斯.md "wikilink")（Daniel
Jones）组成。野人花园于1994年成立，2001年巅峰之际宣布解散，是澳洲历年来最成功的音乐团体之一。其中著名的歌曲包括：“*Truly
Madly Deeply*”、“*I Want You*”、“*To The Moon & Back*”、“*I knew I Loved
You*”、“*The Animal
Song*”等皆是当年的冠军歌曲，不但刷新了许多乐史记录，[全球累计总销量亦超过两千三百万张](../Page/全球.md "wikilink")。

## 历史

1993年澳洲布里斯本的Red
Edge樂團徵求主唱，當時在上大学的[戴倫·海斯在報上看到了徵人啟事](../Page/戴倫·海斯.md "wikilink")，便去應徵。他與吉他手[丹尼爾·瓊斯一拍即合](../Page/丹尼爾·瓊斯.md "wikilink")，加入了Red
Edge擔任主唱。不久他們創作了第一首歌曲：[A Thousand
Words](../Page/A_Thousand_Words.md "wikilink")，後來戴倫·海斯及丹尼爾·瓊斯就離開了Red
Edge，組了自己的樂團。

戴倫·海斯當時著迷於[安·萊絲的](../Page/安·萊絲.md "wikilink")[吸血鬼編年史系列小說](../Page/吸血鬼編年史.md "wikilink")，其中的《[夜訪吸血鬼](../Page/夜訪吸血鬼_\(小說\).md "wikilink")》和《[吸血鬼黎斯特](../Page/吸血鬼黎斯特.md "wikilink")》都有提到[Savage
Garden](../Page/Savage_Garden.md "wikilink")，那是一個蠻荒、嗜血的失樂園，後來他們就用Savage
Garden當成樂團的名稱。

1996年，首支單曲[I want
you](../Page/I_want_you.md "wikilink")，在澳洲大大成功，後有唱片製作人接洽錄製專輯，也就是《野人花園同名專輯》，在澳洲本土、美國、英國、亞洲都有很好的表現。1997年，野人花園得到澳洲[ARIA音樂獎十三項獎的殊榮](../Page/ARIA.md "wikilink")，包括最佳樂團、最暢銷單曲等。

1998年一月二十八日從澳洲展開[The Future of
Delites全球巡迴演唱會](../Page/The_Future_of_Delites.md "wikilink")，自澳洲出發前往亞洲、美國等地區。同年推出[To
the Moon and Back單曲及](../Page/To_the_Moon_and_Back.md "wikilink")[Truly
Madly
Deeply](../Page/Truly_Madly_Deeply.md "wikilink")，後者為[告示牌排行榜冠軍](../Page/告示牌排行榜.md "wikilink")，打敗[艾爾頓強的](../Page/艾爾頓強.md "wikilink")[風中之燭](../Page/風中之燭.md "wikilink")，也使野人花園在國際一舉成名。

1999年11月，野人花園推出第二張專輯《[认定](../Page/认定.md "wikilink")》，首支單曲[I knew I loved
you在告示牌排行榜連續四週冠軍](../Page/I_knew_I_loved_you.md "wikilink")。[The Animal
Song更是成為電影](../Page/The_Animal_Song.md "wikilink")《[The Other
Sister](../Page/The_Other_Sister.md "wikilink")》「愛情DIY」的主題曲。

2001年就在野人花園音樂事業正大紅大紫時，團員丹尼爾·瓊斯宣告退出，野人花園隨即宣告解散。解散後丹尼爾·瓊斯往幕後製作發展；戴倫·海斯繼續個人的歌唱生涯。2002年3月出了《[Spin](../Page/Spin.md "wikilink")》心靈節奏專輯；2004年發行《[The
Tension and the
Spark](../Page/The_Tension_and_the_Spark.md "wikilink")》「心電感應」專輯。解散後四年，2005年十一月，野人花園推出野人花園最終精選《[Truly
Madly Completely──The Best of Savage
Garden](../Page/Truly_Madly_Completely──The_Best_of_Savage_Garden.md "wikilink")》，其中包含野人花園冠軍暢銷歌曲及戴倫·海斯單飛後新作，算是替倉促解散的野人花園畫下完满的句點。

## 风格

**野人花园**拥有极具创意的音乐创作，力图在摇滚与主流音乐中寻找平衡点，与其他歌手相比歌曲显得清新、人性化，散发出无法抗拒的流行魅力。

## 专辑

  - *[Savage Garden](../Page/Savage_Garden_\(专辑\).md "wikilink")*
    (1997年) \#3 US \[7x Platinum\], \#2 UK, ***\#1*** Australia
  - *[Affirmation](../Page/Affirmation.md "wikilink")* (1999年) \#6 US
    \[3x Platinum\], \#7 UK, ***\#1*** Australia

## 单曲

  - *I Want You* (1996年) \#4 US, \#11 UK, ***\#1*** Australia
  - *To the Moon and Back* (1996年) \# US, \#3 UK, ***\#1*** Australia
  - *Truly Madly Deeply* (1997年) ***\#1*** (2周) US, \#4 UK, \#1
    Australia
  - *Break Me Shake Me* (1997年) \#7 Australia
  - *Universe* (1997年) \#26 Australia
  - *Tears Of Pearls* (1998年) \# US
  - *Santa Monica* (1998年) \# US
  - *I Want You '98* (1998年) \#12 UK
  - *The Animal Song* (1999年) \#19 US, \#16 UK, \#3 Australia
  - *I Knew I Loved You* (1999年) ***\#1*** (4周) US, \#10 UK, \#4
    Australia
  - *Affirmation* (2000年) \#6 US, \#8 UK, \#16 Australia
  - *Crash & Burn* (2000年) \#24 US, \#14 UK, \#16 Australia
  - *Chained To You* (2000年) \#21 Australia
  - *Hold Me* (2000年) \#16 UK
  - *The Best Thing* (2001年) \#35 UK

## 链接

  - [Timeline of Savage
    Garden](http://www.rockonthenet.com/artists-s/savagegarden_main.htm)
  - [Savage Garden Music Dot Com - unofficial
    fansite](https://web.archive.org/web/20110611082401/http://www.savagegardenmusic.com/)
  - [野人花园歌詞](http://www.rockpoplyrics.com/savagegarden.php)

[Category:1994年成立的音樂團體](../Category/1994年成立的音樂團體.md "wikilink")
[Category:2001年解散的音樂團體](../Category/2001年解散的音樂團體.md "wikilink")
[S](../Category/澳大利亚乐团.md "wikilink")
[S](../Category/男子演唱團體.md "wikilink")
[Category:世界音樂獎獲得者](../Category/世界音樂獎獲得者.md "wikilink")