**馬亞爾II**（**[M31的](../Page/仙女座星系.md "wikilink")[G1](../Page/衛星星系.md "wikilink")**），也稱為**NGC-224-G1**、**SKHB
1**、**GSC 2788:2139**、**HBK 0-1**、**M31GC
J003247+393440**或**仙女的星團**，是一個繞著M31，也就是[仙女座星系的](../Page/仙女座星系.md "wikilink")[球狀星團](../Page/球狀星團.md "wikilink")。

它的位置在距離仙女座星系核心130,000[光年](../Page/光年.md "wikilink")（40Kpc）\[1\]，並且是[本星系群中](../Page/本星系群.md "wikilink")[絕對星等最亮](../Page/絕對星等.md "wikilink")\[2\]的球狀星團，[視星等為](../Page/視星等.md "wikilink")13.7等。G
1的質量被認為是[半人馬座ω的兩倍](../Page/半人馬座ω.md "wikilink")，並且在核心可能有中介質量的黑洞（∼ 2
[M<sub>⊙</sub>](../Page/太陽質量.md "wikilink")）\[3\]。

它是由尼古拉斯馬亞爾（Nicholas Mayall）和O.J.
Eggen在1953年首度從[帕洛馬山天文台](../Page/帕洛馬山天文台.md "wikilink")48吋施密特望遠鏡在1948年拍攝的乾版上檢出的。

由於恆星[金屬量的分布很廣](../Page/金屬量.md "wikilink")，包含了數個世代的[恆星和許多的恆星創生期間](../Page/恆星.md "wikilink")，顯示經歷了數個世代的[恆星創生](../Page/恆星.md "wikilink")，所以許多人主張他不是真正的球狀星團，許多人認為他不是個真正的球狀星團，而是被仙女座星系吞噬的[矮星系殘留下的星系核心殘骸](../Page/矮星系.md "wikilink")\[4\]\[5\]。

## 名稱的來源

  - 1991年，馬亞爾II因[約翰·修茲勞](../Page/約翰·修茲勞.md "wikilink")、J.P. Brodie、S.M.
    Kent被稱為HBK 0-1.
  - 1953年，[尼古拉斯·U·馬亞爾和O](../Page/尼古拉斯·U·馬亞爾.md "wikilink").J.
    Eggen發現馬亞爾II。
  - 1977年，馬亞爾II因[华莱士·萨金特](../Page/华莱士·萨金特.md "wikilink")、C.T.
    Kowal、F.D.A. Hartwick和Sidney van den Bergh被稱為SKHB 1，也稱之為G1。

## 相關條目

  - [M54](../Page/M54.md "wikilink")
  - [半人馬座ω](../Page/半人馬座ω.md "wikilink")
  - [馬亞爾III](../Page/馬亞爾III.md "wikilink")（[M31
    G2](../Page/M31_G2.md "wikilink")、[G2](../Page/M31_G2.md "wikilink")、[HBK
    0-2](../Page/HBK_0-2.md "wikilink")、[SKHB
    2](../Page/SKHB_2.md "wikilink")、[M31GC
    J003334+393119](../Page/M31GC_J003334+393119.md "wikilink")）
  - [馬亞爾IV](../Page/馬亞爾IV.md "wikilink")（[HBK
    358-219](../Page/HBK_358-219.md "wikilink")、[SKHB
    219](../Page/SKHB_219.md "wikilink")、[Bol
    358](../Page/Bol_358.md "wikilink")、[M31GC
    J004318+394914](../Page/M31GC_J004318+394914.md "wikilink")）
  - [馬亞爾V](../Page/馬亞爾V.md "wikilink")（[HBK
    407-352](../Page/HBK_407-352.md "wikilink")、[SKHB
    352](../Page/SKHB_352.md "wikilink")、[Bol
    407](../Page/Bol_407.md "wikilink")、[M31GC
    J005010+414101](../Page/M31GC_J005010+414101.md "wikilink")）
  - [馬亞爾VI](../Page/馬亞爾VI.md "wikilink")（[HBK
    0-327](../Page/HBK_0-327.md "wikilink")、[SKHB
    327](../Page/SKHB_327.md "wikilink")、[BA
    3-29](../Page/BA_3-29.md "wikilink")、[M31GC
    J004650+424445](../Page/M31GC_J004650+424445.md "wikilink")）
  - [馬亞爾的天體](../Page/馬亞爾的天體.md "wikilink")（[阿普148或](../Page/阿普148.md "wikilink")[APG
    148](../Page/APG_148.md "wikilink")、[VV32](../Page/VV32.md "wikilink")）

## 參考資料

## 外部連結

  - \[<http://adsabs.harvard.edu/cgi-bin/bib_query?1991ApJ>...370..495H
    Astrophysical Journal, Vol. 370, p. 495-504\]
  - \[<http://adsabs.harvard.edu/cgi-bin/bib_query?1953PASP>...65...24M
    Publications of the Astronomical Society of the Pacific, Vol. 65,
    No. 382, p. 24-29\]
  - \[<http://adsabs.harvard.edu/cgi-bin/bib_query?1977AJ>.....82..947S
    Astronomical Journal, vol. 82, p. 947-953\]
  - [NightSkyInfo.com: Mayall
    II](http://www.nightskyinfo.com/archive/g1_globular_cluster/)

[Category:仙女座星系](../Category/仙女座星系.md "wikilink")
[Category:球狀星團](../Category/球狀星團.md "wikilink")

1.
2.
3.
4.
5.