<span lang="{{lc:{{{1}}}}}" xml:lang="{{lc:{{{1}}}}}">{{\#switch:

`|zh|gan|zh-yue = `
`|jp|jap = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：`[`日文請使用`](日文.md "wikilink")**`ja`**</span>
`|gr = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：`[`希臘文請使用`](希臘文.md "wikilink")**`el`**`或`**`gre`**</span>
`|kp = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：`[`朝鮮文請使用`](朝鮮文.md "wikilink")**`ko`**`或`**`kor`**</span>
`|po = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：`[`波蘭文請使用`](波蘭文.md "wikilink")**`pl`**`或`**`pol`**</span>
`|sp = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：`[`西班牙文請使用`](西班牙文.md "wikilink")**`es`**`或`**`spa`**</span>
`|cz = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：`[`捷克文請使用`](捷克文.md "wikilink")**`cs`**`或`**`cze`**</span>
`|kz = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：`[`哈薩克文請使用`](哈薩克文.md "wikilink")**`kk`**`或`**`kaz`**</span>
`|dk = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：`[`丹麥文請使用`](丹麥文.md "wikilink")**`da`**</span>
`|unicode|ipa = -``}-`<span class="error" style="font-size:smaller;">`模板參數錯誤：請檢查更正引用語言代碼`</span>

| zh-classical =
<span class="error" style="font-size:smaller;">模板參數錯誤：[文言文請使用](文言文.md "wikilink")**zh-lzh**或**lzh**</span>

`|#default = -``}-}}`</span>`{{#switch:`
`|ar|arc|arz|az|ckb|dv|fa|glk|ha|he|ks|ku|mzn|pnb|ps|ug|ur|yi = ‎`
`|#default = }}`
` }}的條目]]`

| nocat =  }}<noinclude></noinclude>