**張正憲**（）為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[中華職棒](../Page/中華職棒.md "wikilink")[時報鷹](../Page/時報鷹.md "wikilink")，守備位置為[捕手](../Page/捕手.md "wikilink")，但後來因[職棒簽賭案而遭到](../Page/職棒簽賭案.md "wikilink")[中華職棒終身禁賽](../Page/中華職棒.md "wikilink")。

## 經歷

  - [嘉義縣朴子國小少棒隊](../Page/嘉義縣.md "wikilink")
  - [台北市華興中學青少棒隊](../Page/台北市.md "wikilink")
  - [台北市華興中學青棒隊](../Page/台北市.md "wikilink")
  - [輔仁大學棒球隊](../Page/輔仁大學.md "wikilink")（榮工）
  - 黑鷹棒球隊
  - [中華職棒](../Page/中華職棒.md "wikilink")[時報鷹隊](../Page/時報鷹.md "wikilink")（1993年－1997年）
  - [時報鷹棒球教練隊](../Page/時報鷹棒球教練隊.md "wikilink")
  - [台北市](../Page/台北市.md "wikilink")[大理高中青棒隊教練](../Page/大理高中.md "wikilink")
  - [宜蘭縣中道中學青棒隊教練](../Page/宜蘭縣.md "wikilink")
  - [宜蘭縣馬賽國小棒球隊教練](../Page/宜蘭縣.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                               | 出賽  | 打數  | 安打  | 全壘打 | 打點 | 盜壘 | 四死 | 三振 | 壘打數 | 雙殺打 | 打擊率   |
| ----- | -------------------------------- | --- | --- | --- | --- | -- | -- | -- | -- | --- | --- | ----- |
| 1993年 | [時報鷹](../Page/時報鷹.md "wikilink") | 56  | 146 | 28  | 2   | 10 | 1  | 14 | 20 | 42  | 6   | 0.197 |
| 1994年 | [時報鷹](../Page/時報鷹.md "wikilink") | 62  | 155 | 33  | 2   | 11 | 1  | 13 | 22 | 43  | 4   | 0.213 |
| 1995年 | [時報鷹](../Page/時報鷹.md "wikilink") | 57  | 124 | 26  | 2   | 12 | 1  | 12 | 9  | 38  | 3   | 0.210 |
| 1996年 | [時報鷹](../Page/時報鷹.md "wikilink") | 88  | 253 | 70  | 6   | 32 | 3  | 15 | 20 | 106 | 10  | 0.277 |
| 合計    | 四年                               | 263 | 678 | 157 | 12  | 65 | 6  | 54 | 71 | 229 | 23  | 0.232 |

## 特殊事蹟

## 外部連結

[Zhengxian](../Category/張姓.md "wikilink")
[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:時報鷹隊球員](../Category/時報鷹隊球員.md "wikilink")
[Category:中華成棒隊球員](../Category/中華成棒隊球員.md "wikilink")
[Category:輔仁大學校友](../Category/輔仁大學校友.md "wikilink")
[Category:台灣奧林匹克運動會銀牌得主](../Category/台灣奧林匹克運動會銀牌得主.md "wikilink")
[Category:1992年夏季奧林匹克運動會獎牌得主](../Category/1992年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:中華職棒終身禁賽名單](../Category/中華職棒終身禁賽名單.md "wikilink")