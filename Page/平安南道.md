**平安南道**（），是[朝鲜民主主义人民共和国西北部的地方行政区](../Page/朝鲜民主主义人民共和国.md "wikilink")。面积12,330
平方公里，人口3,597,557人。道政府所在[平城市](../Page/平城.md "wikilink")。

## 地理

西面[黄海](../Page/黄海.md "wikilink")（朝鲜名称，西海），南邻首都[平壤和](../Page/平壤.md "wikilink")[黄海北道](../Page/黄海北道.md "wikilink")，东接[咸镜南道](../Page/咸镜南道.md "wikilink")，北接[平安北道和](../Page/平安北道.md "wikilink")[慈江道](../Page/慈江道.md "wikilink")。

## 历史

  - 1896年把[平安道南北两分而成立](../Page/平安道.md "wikilink")。
  - 1946年9月平壤成为特别市，脱离平安南道。
  - 1980年代[南浦市脱离平安南道](../Page/南浦.md "wikilink")，成为直辖市
  - 2004年1月9日南浦做为特级市、重新隶属平安南道。
  - 2010年，江西、大安、温泉、龙岗、千里马五郡劃入南浦特別市。

## 行政区划

[缩略图](https://zh.wikipedia.org/wiki/File:Heian-Nan_Provincial_Office.JPG "fig:缩略图")平安南道廳\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Pyongan-Namdo-Un-north-korea.png "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Anjucity.jpg "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Dock_No._2._Nampo,_North_Korea2.jpg "fig:缩略图")

### 市部

  - [平城市](../Page/平城市.md "wikilink")
  - [顺川市](../Page/顺川市.md "wikilink")
  - [德川市](../Page/德川市.md "wikilink")
  - [安州市](../Page/安州市.md "wikilink")
  - \-{[价川市](../Page/价川市.md "wikilink")}-

### 地区

  - [清南区](../Page/清南区.md "wikilink")

### 郡部

  - [大同郡](../Page/大同郡.md "wikilink") [甑山郡](../Page/甑山郡.md "wikilink")
    [平原郡](../Page/平原郡_\(朝鮮\).md "wikilink")
    [肃川郡](../Page/肃川郡.md "wikilink")
    [文德郡](../Page/文德郡.md "wikilink")
    [成川郡](../Page/成川郡.md "wikilink")
    [桧仓郡](../Page/桧仓郡.md "wikilink")
    [新阳郡](../Page/新阳郡.md "wikilink")
    [阳德郡](../Page/阳德郡.md "wikilink")
    [北仓郡](../Page/北仓郡.md "wikilink")
    [孟山郡](../Page/孟山郡.md "wikilink")
    [宁远郡](../Page/宁远郡.md "wikilink")
    [大兴郡](../Page/大興郡.md "wikilink")
    [殷山郡](../Page/殷山郡.md "wikilink")

## 觀光熱點

  - [石湯溫泉](../Page/石湯溫泉.md "wikilink")：位於[陽德郡](../Page/陽德郡.md "wikilink")[溫井-{里}-境內](../Page/溫井里.md "wikilink")。水溫73-78攝氏度，[硫化氫為](../Page/硫化氫.md "wikilink")1公升中含有2.0至3.5毫克，[礦物質為](../Page/礦物質.md "wikilink")1公升中含有0.4至0.5克。據說對各種[皮膚病](../Page/皮膚病.md "wikilink")、神經痛、關節發炎、慢性濕疹、婦科病等有療效。
  - [龍崗溫泉](../Page/龍崗溫泉.md "wikilink")（Ryonggang Hot Spring）：位於「溫泉郡」。
  - [法興寺](../Page/法興寺_\(朝鮮\).md "wikilink")（Pophung
    Temple）：位於「江龍山」的山腳的[高句麗時代之](../Page/高句麗.md "wikilink")[佛教寺廟](../Page/佛教.md "wikilink")。
  - [藥水-{里}-壁畫古墓](../Page/藥水里壁畫古墓.md "wikilink")：[高句麗時代的壁畫](../Page/高句麗.md "wikilink")[墳墓](../Page/墳墓.md "wikilink")

## 天然風物

  - 北倉朝鮮刺槐群落：位于「北倉郡」的「南陽里」境內。刺槐群落發現于1919年間，樹高30-50厘米，地下莖中生出新枝。刺槐開花，很適宜栽種于園林。

<!-- end list -->

  - 龍浦[細葉松樹林](../Page/細葉松樹.md "wikilink")：位于「北倉郡」的「芢浦勞動者區」境內，此樹最高30米，周長2.4米，樹冠直徑4米，此樹根部較發達，不被風吹倒。

<!-- end list -->

  - 安國寺[銀杏樹](../Page/銀杏.md "wikilink")：位于平城市的鳳鶴洞境內。此樹種植于14世紀。樹高27米，周長10米，樹冠直徑18米左右，此樹枝杈有10多個。

<!-- end list -->

  - [白鷺](../Page/白鷺.md "wikilink")、[蒼鷺繁殖地](../Page/蒼鷺.md "wikilink")：[价川市的](../Page/价川.md "wikilink")「龍雲里」境內，妙香山脈南西走向，「龍雲里」東坡上有[白鷺](../Page/白鷺.md "wikilink")、[蒼鷺棲息處](../Page/蒼鷺.md "wikilink")。從每年3月到10月初[白鷺](../Page/白鷺.md "wikilink")、[蒼鷺到此棲息繁殖](../Page/蒼鷺.md "wikilink")。

## 特產

  - [成川栗](../Page/成川栗.md "wikilink")：又稱「葯栗」，產於[成川郡](../Page/成川郡.md "wikilink")
    ，每顆約7-8克。
  - [東陽泉水](../Page/東陽泉水.md "wikilink")：位於[陽德郡的](../Page/陽德郡.md "wikilink")[雙龍山的泉水](../Page/雙龍山.md "wikilink")。
  - [新德山冷泉](../Page/新德山冷泉.md "wikilink")：位於[龍岡郡境內](../Page/龍岡郡.md "wikilink")，從深1,000米的[花崗石層涌出的](../Page/花崗石.md "wikilink")[冷泉](../Page/冷泉.md "wikilink")，含有[鉀](../Page/鉀.md "wikilink")、[鈣等對人體有好處的物質](../Page/鈣.md "wikilink")。

## 高等学校

  - [肅川農業大學](../Page/肅川農業大學.md "wikilink")
  - [平城理科大學](../Page/平城理科大學.md "wikilink")

## 参考文献

## 参见

  - [平安南道 (韩国)](../Page/平安南道_\(韩国\).md "wikilink")

{{-}}

[Category:朝鲜的道](../Category/朝鲜的道.md "wikilink")
[平安南道](../Category/平安南道.md "wikilink")
[Category:1896年建立的行政區劃](../Category/1896年建立的行政區劃.md "wikilink")