[GeorgiatTmes.svg](https://zh.wikipedia.org/wiki/File:GeorgiatTmes.svg "fig:GeorgiatTmes.svg")的字體大小和間距比較\]\]

**Times New
Roman**（泰晤士新罗马），可能是最常見且廣為人知的[襯線字體之一](../Page/襯線字體.md "wikilink")，在字体设计上属于过渡型[衬线體](../Page/衬线體.md "wikilink")，對後來的[字型產生了很深遠的影響](../Page/字型.md "wikilink")。另外由於其中規中矩、四平八穩的經典外觀，所以經常被選擇為標準[字體之一](../Page/字體.md "wikilink")。

## 由来

Times New
Roman為、和[维克多·拉登特共同創造](../Page/维克多·拉登特.md "wikilink")，由[Monotype公司於](../Page/蒙納公司.md "wikilink")1932年發表，並為[英国的](../Page/英国.md "wikilink")《[泰晤士報](../Page/泰晤士報.md "wikilink")》首次採用。

Times New Roman的最初原稿在斯坦利·莫里森的監製之下，由维克多·拉登特完成。Times New
Roman主要以[Perpetua和](../Page/Perpetua.md "wikilink")[Plantin為藍本](../Page/Plantin.md "wikilink")，因此具有許多古老字體的特色，但是在字迹清析度及成本效益方面卻有更加出色的表現。

## 应用

《[泰晤士報](../Page/泰晤士報.md "wikilink")》首次採用了Times New
Roman後，這個字型很快地博得了大眾的青睞，獲得了極大的成功。儘管泰晤士報已不再使用Times
New Roman，但Times New Roman已成為經典字型之一，迄今仍廣泛使用在圖書、雜誌、報告、公文、廣告、螢幕顯示等。

## 电脑字体

[Times_Roman_vs_Times_New_Roman.png](https://zh.wikipedia.org/wiki/File:Times_Roman_vs_Times_New_Roman.png "fig:Times_Roman_vs_Times_New_Roman.png")
[视窗系统中的字体是](../Page/视窗.md "wikilink")[Monotype公司为](../Page/Monotype.md "wikilink")[微软公司制作的Times](../Page/微软.md "wikilink")
New Roman
PS（[TrueType字体](../Page/TrueType.md "wikilink")），[视窗系统从](../Page/视窗.md "wikilink")3.1版本开始就一直附带这个字体。而在[苹果电脑公司的](../Page/苹果电脑公司.md "wikilink")[麦金塔系统中使用的是Linotype公司的Times](../Page/麦金塔.md "wikilink")
Roman（在Macintosh系统中直接简称为“Times”）。[开放源代码的](../Page/开放源代码.md "wikilink")[操作系统中一般使用](../Page/操作系统.md "wikilink")的[Nimbus
Roman No. 9 L](../Page/Nimbus_Roman_No._9_L.md "wikilink")，它是Times
Roman的URW
PostScript版本，是基于[GNU通用公共许可证发布的](../Page/GNU通用公共许可证.md "wikilink")。

在[电脑方面](../Page/电脑.md "wikilink")，现在很多[应用程序](../Page/应用程序.md "wikilink")，还有[网页浏览器和](../Page/网页浏览器.md "wikilink")[文字处理软件都是用它作为默认字体](../Page/文字处理.md "wikilink")。

需要注意的是，[Linotype公司的Times](../Page/Linotype.md "wikilink")
Roman字体，对于一般使用者来说，它和Times New Roman
PS几乎没有区别，二者的区别，一般是在涉及注册商标的时候才提及，虽然在设计方面的确有细微的差别，比如Linotype公司的字体中大写S的[衬线是倾斜的](../Page/衬线.md "wikilink")，而Monotype公司的是垂直的；二者在小写字母z的斜体字重等方面的处理也不一样。原先Times
New Roman和Times
Roman字宽不一样，当[微软在](../Page/微软.md "wikilink")[Windows系统中采用Times](../Page/Windows.md "wikilink")
New
Roman时，他们让Monotype公司修改PostScript字体，使其与[Adobe](../Page/Adobe.md "wikilink")／[Linotype的字体保持一样的字宽](../Page/Linotype.md "wikilink")；所以现在看到的字体宽度都是一样的。

另外方正公司的方正小标宋、方正宋三字体的英文部分也使用Times Roman（但是方正宋三字体中阿拉伯数字使用的是Bodoni MT字体）

## 外部連結

  - [Times New
    Roman（經典版本）](http://store.adobe.com/type/browser/F/TMMQ/F_TMMQ-10005000.html)
  - [Times New
    Roman字型資訊](http://www.microsoft.com/typography/fonts/font.aspx?FID=9&FNAME=Times%20New%20Roman)
  - [Times New
    Roman下載](http://prdownloads.sourceforge.net/corefonts/times32.exe?download)
  - [Times Roman和Times New
    Roman的比較](http://www.truetype.demon.co.uk/articles/times.htm)
  - [European Union Expansion Font
    Update](http://www.microsoft.com/en-us/download/details.aspx?id=16083)（含Times
    New Roman）

[Category:網頁核心字型](../Category/網頁核心字型.md "wikilink") [Category:Windows
XP字體](../Category/Windows_XP字體.md "wikilink")
[Category:衬线字体](../Category/衬线字体.md "wikilink")
[Category:蒙纳字体](../Category/蒙纳字体.md "wikilink")