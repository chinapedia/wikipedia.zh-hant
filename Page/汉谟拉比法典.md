《**漢摩拉比法典**》是[古巴比伦第六代国王](../Page/古巴比伦.md "wikilink")[漢摩拉比颁布的一部](../Page/漢摩拉比.md "wikilink")[法律](../Page/法律.md "wikilink")，被认为是世界上最早的一部比较具有系统的[法典](../Page/法典.md "wikilink")，約公元前1772年頒布。

## 历史

1901年在[埃兰古城](../Page/埃兰.md "wikilink")[苏萨](../Page/苏萨.md "wikilink")（今属于[伊朗](../Page/伊朗.md "wikilink")）发现，为一[黑色的](../Page/黑色.md "wikilink")[玄武岩圆柱](../Page/玄武岩.md "wikilink")，现存[法国](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[羅浮宫博物馆](../Page/卢浮宫.md "wikilink")。圆柱上端有[汉摩拉比从](../Page/汉摩拉比.md "wikilink")[太阳神](../Page/太阳神.md "wikilink")[夏馬修](../Page/沙瑪什.md "wikilink")（Shamash），手中接过权杖的浮雕，下面用[楔形文字铭刻法典全文](../Page/楔形文字.md "wikilink")，除序言與结语外全文共收錄282条条文，範疇包括[诉讼手续](../Page/诉讼.md "wikilink")、损害赔偿、租佃关系、[债权](../Page/债权.md "wikilink")[债务](../Page/债务.md "wikilink")、财产继承、处罚[奴隶等](../Page/奴隶.md "wikilink")。\[1\]\[2\]

汉摩拉比法典将人分为三种等级：

1.  有[公民权的](../Page/公民.md "wikilink")[自由民](../Page/自由民.md "wikilink")
2.  无公民权的自由民
3.  奴隶
    1.  王室奴隶
    2.  自由民所属奴隶
    3.  公民私人奴隶

汉谟拉比法典全文結構可分為序言,正文和結語三部分：

1.  以牙還牙，[以眼還眼](../Page/以眼還眼.md "wikilink")
2.  盡自身所有身家以恩報恩

## 画廊

<File:Codice> di hammurabi
01.JPG|thumb|[羅浮宫内的汉摩拉比法典](../Page/羅浮宫.md "wikilink")
[File:CodeOfHammurabi.jpg|thumb|拓片](File:CodeOfHammurabi.jpg%7Cthumb%7C拓片)

## 另见

  - [吾珥南模法典](../Page/吾珥南模法典.md "wikilink")
  - [希波克拉底誓词](../Page/希波克拉底誓词.md "wikilink")
  - [伊施嫩納法典](../Page/伊施嫩納法典.md "wikilink")
  - [乌鲁卡基那](../Page/乌鲁卡基那.md "wikilink")
  - [里辟伊士他法典](../Page/里辟伊士他法典.md "wikilink")

## 参考来源

[Category:巴比倫尼亞](../Category/巴比倫尼亞.md "wikilink")
[Category:已废止的法律](../Category/已废止的法律.md "wikilink")
[Category:法典](../Category/法典.md "wikilink")

1.  [1](http://www.general-intelligence.com/library/hr.pdf)，论文。
2.  [2](http://www.louvre.fr/en/oeuvre-notices/law-code-hammurabi-king-babylon)，相关报道。