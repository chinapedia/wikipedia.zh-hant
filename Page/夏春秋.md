**夏春秋**（，），原名**吳錦泉**，花名「秋冬夏」、「冬叔」、「財神」，[香港電視節目主持人](../Page/香港.md "wikilink")，藝人[吳君如](../Page/吳君如.md "wikilink")、[吳君祥](../Page/吳君祥.md "wikilink")（[江美儀的前夫](../Page/江美儀.md "wikilink")）的父親。

## 生平

夏春秋祖籍廣州番禺區，1955年拍攝[電影](../Page/電影.md "wikilink")，其後1957年加入[亞洲電視前身是](../Page/亞洲電視.md "wikilink")[麗的電視](../Page/麗的電視.md "wikilink")，至2000年離開[亞洲電視](../Page/亞洲電視.md "wikilink")，於這同這家電視台工作是長達43年，演出無數戲劇、綜藝、婦女等節目，期間曾長時間主持[六合彩攪珠的直播節目](../Page/六合彩.md "wikilink")，所以被稱為**財神**，又是一年有四季春夏秋冬，夏春秋（無冬），所以又稱**秋冬夏**、**冬叔**、**財神**。

已移民定居[加拿大](../Page/加拿大.md "wikilink")，並於當地華人[電台主持節目](../Page/電台.md "wikilink")。

2005年[六合彩](../Page/六合彩.md "wikilink")30周年紀念、2013年六合彩中秋金多寶、2016年六合彩40周年金多寶，以及[香港賽馬會與](../Page/香港賽馬會.md "wikilink")[無綫電視製作的節目](../Page/無綫電視.md "wikilink")，亦有邀請他擔任特別嘉賓，2017年加入[亞洲電視數碼媒體](../Page/亞洲電視數碼媒體.md "wikilink")。

## 演出作品

### 電視劇（[麗的電視](../Page/麗的電視.md "wikilink")、[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - 1957年：《[幸福的家庭](../Page/幸福的家庭.md "wikilink")》
  - 1978年：《[鱷魚淚](../Page/鱷魚淚.md "wikilink")》
  - 1980年：《[彩雲深處](../Page/彩雲深處.md "wikilink")》
  - 1980年：《[小小心願](../Page/小小心願.md "wikilink")》飾 葉生
  - 1981年：《[青春三重奏](../Page/青春三重奏.md "wikilink")》飾 張經理
  - 1986年：《[秦始皇](../Page/秦始皇_\(香港電視劇\).md "wikilink")》飾
    [趙孝成王](../Page/趙孝成王.md "wikilink")
  - 1991年：《[四驅橋聖](../Page/四驅橋聖.md "wikilink")》
  - 1993年：《[中國教父再見黃埔灘](../Page/中國教父再見黃埔灘.md "wikilink")》飾 陳發
  - 1993年：《[天蠶變之再與天比高](../Page/天蠶變之再與天比高.md "wikilink")》飾 周千總
  - 1993年：《[今裝戲寶之阿蘭嫁阿瑞](../Page/今裝戲寶.md "wikilink")》飾 阿仁
  - 1995年：《[九王奪位](../Page/九王奪位.md "wikilink")》飾 襄親王
  - 1995年：《[新包青天](../Page/新包青天.md "wikilink")－殺母狀元》飾 章天成
  - 1995年：《[新包青天](../Page/新包青天.md "wikilink")－告親夫》飾 孫秀
  - 1995年：《[新包青天](../Page/新包青天.md "wikilink")－殉情記》飾 楊士林
  - 1995年：《[新包青天](../Page/新包青天.md "wikilink")－俠骨神算》飾 房如晦
  - 1995年：《[新包青天](../Page/新包青天.md "wikilink")－滄海月明珠有淚》飾 錢鈞
  - 1995年：《[新包青天](../Page/新包青天.md "wikilink")－仇之劍》飾 宋波
  - 1996年：《[千王之王重出江湖](../Page/千王之王重出江湖.md "wikilink")》飾 -{余}-筒
  - 1996年：《[誰是兇手](../Page/誰是兇手.md "wikilink")》飾 馬警司
  - 1997年：《[肥貓正傳](../Page/肥貓正傳.md "wikilink")》飾 丁澤邦
  - 1997年：《[等著你回來](../Page/等著你回來.md "wikilink")》飾 爺爺
  - 1998年：《[非常女警](../Page/非常女警.md "wikilink")》

### 節目主持（[麗的電視](../Page/麗的電視.md "wikilink")、[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - [下午茶](../Page/下午茶_\(電視節目\).md "wikilink")（1975年－1992年）

### 節目主持（[麗的電視](../Page/麗的電視.md "wikilink")、[亞洲電視](../Page/亞洲電視.md "wikilink")）

  - [六合彩攪珠直播節目](../Page/六合彩.md "wikilink")（1976年－1993年）
  - [六合彩特別節目春秋有得Fun](../Page/六合彩.md "wikilink")（2013年）

### 電影

  - [可憐的媽媽](../Page/可憐的媽媽.md "wikilink")
  - [花田囍事](../Page/花田囍事.md "wikilink")
  - [金雞2](../Page/金雞2.md "wikilink")
  - [春田花花同學會](../Page/春田花花同學會.md "wikilink") 飾 中醫師
  - [最愛女人購物狂](../Page/最愛女人購物狂.md "wikilink")
  - [喱咕喱咕對對碰](../Page/喱咕喱咕對對碰.md "wikilink") 飾 老爺
  - [家有囍事2009](../Page/家有囍事2009.md "wikilink")
  - [花田囍事2010](../Page/花田囍事2010.md "wikilink")
  - [歲月神偷](../Page/歲月神偷.md "wikilink")
  - [最強囍事](../Page/最強囍事.md "wikilink")
  - [八星抱喜](../Page/八星抱喜.md "wikilink") 飾 證婚律師
  - [鬼賭鬼](../Page/鬼賭鬼.md "wikilink") 飾 夏春秋
  - [百星酒店](../Page/百星酒店.md "wikilink") 飾 寶寶的爺爺
  - [惡人谷](../Page/惡人谷_\(電影\).md "wikilink") 飾 官家

## 廣告作品

  - [香港家庭計劃指導會廣告](../Page/香港家庭計劃指導會.md "wikilink")：六合彩（2003年－2006年）

## 流行文化

《夏春秋》－香港電子音樂組合PixelToy以六合彩主題音樂配以原創歌詞的作品，講述市民在電視機前等候夏春秋主持六合彩攪珠的一段集體回憶。

## 外部連結

### 媒體連結

  - [香港家庭計劃指導會 -
    廣告](https://web.archive.org/web/20060312231140/http://www.famplan.org.hk/fpahk/common/videos/tv-2003-subfertility-chi.wmv)
  - [夏春秋真名是吳錦泉](https://web.archive.org/web/20090211162932/http://hongkongmovie.com/official/juliet/gcasting.htm)

[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:前麗的電視藝員](../Category/前麗的電視藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:番禺人](../Category/番禺人.md "wikilink")
[Category:吳姓](../Category/吳姓.md "wikilink")
[Category:六合彩](../Category/六合彩.md "wikilink")