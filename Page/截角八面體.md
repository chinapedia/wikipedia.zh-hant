在[幾何學中](../Page/幾何學.md "wikilink")，**截角八面體**\[1\]是一種具有十四個面的[半正多面體](../Page/半正多面體.md "wikilink")，屬於[阿基米德立體也是個](../Page/阿基米德立體.md "wikilink")[平行多面體](../Page/平行多面體.md "wikilink")。由6個[正方形和](../Page/正方形.md "wikilink")8個[正六邊形組成](../Page/正六邊形.md "wikilink")，共有[14個](../Page/14.md "wikilink")[面](../Page/平面.md "wikilink")、[36個](../Page/36.md "wikilink")[邊以及](../Page/邊.md "wikilink")[24個](../Page/24.md "wikilink")[頂點](../Page/頂點.md "wikilink")\[2\]。因為每個面皆具點對稱性質，因此截角八面體也是一種[環帶多面體](../Page/環帶多面體.md "wikilink")。同時，因為它具有正方形和六邊形面，因此也是一種[戈德堡多面體](../Page/戈德堡多面體.md "wikilink")，其[戈德堡符號為G](../Page/戈德堡多面體.md "wikilink")<sub>IV</sub>(1,1)。另外，由於截角八面體也是一種\[3\]\[4\]，因此可以獨立填滿整個三維空間\[5\]，而由截角八面體堆成的圖形稱為[截角八面體堆砌](../Page/截角八面體堆砌.md "wikilink")\[6\]。

截角八面體的對偶多面體為[四角化六面體](../Page/四角化六面體.md "wikilink")。若截角八面體的[邊長為單位長](../Page/邊長.md "wikilink")，則其對偶多面體[四角化六面體的邊長會變成](../Page/四角化六面體.md "wikilink")\(\tfrac{9}{8}\scriptstyle {\sqrt{2}}\)和\(\tfrac{3}{2}\scriptstyle{\sqrt{2}}\)個單位長。

## 性質

截角八面體僅具有點可遞性質，也就是截角八面體每一個頂點相鄰面的組成都是一樣的，都是一個四邊形和兩個六邊形的公共頂點。但截角八面體不具面可遞和邊可遞性質，因為截角八面體有兩種面，四邊形和六邊形，邊也不可遞，因為截角八面體並不是所有組成邊的相鄰面都只有一種，截角八面體共有兩種稜，一種為六邊形與六邊形的公共稜、另一種為六邊形與四邊形的公共稜。

由於截角八面體僅具有點可遞性質，因此只能算是[均勻多面體](../Page/均勻多面體.md "wikilink")\[7\]中的[半正多面體](../Page/半正多面體.md "wikilink")，不具[擬正多面體性質](../Page/擬正多面體.md "wikilink")。但這個多面體是阿幾米德研究的13種半正多面體之一，因此截角八面體也是一種[阿基米德立體](../Page/阿基米德立體.md "wikilink")\[8\]。

### 結構

|                                                                                                                            |  |                                                                            |
| -------------------------------------------------------------------------------------------------------------------------- |  | -------------------------------------------------------------------------- |
| [image:Truncated Octahedron with Construction.svg](../Page/image:Truncated_Octahedron_with_Construction.svg.md "wikilink") |  | [image:Square Pyramid.svg](../Page/image:Square_Pyramid.svg.md "wikilink") |

截角八面體可以從邊長3a的[正八面體切去六個底邊長為a的](../Page/正八面體.md "wikilink")[四角錐構成](../Page/四角錐.md "wikilink")。這些被切下來的棱錐體的底與側面邊長皆等長，因此其側面皆為正三角形，底邊長為a、底面積為a<sup>2</sup>，這些四角錐是[正四角錐](../Page/正四角錐.md "wikilink")，是第一種[詹森多面體](../Page/詹森多面體.md "wikilink")，J<sub>1</sub>。

這些被截下來的[正四角錐其高h與斜高s為](../Page/正四角錐.md "wikilink")：

\[h = \sqrt{e^2-\frac{1}{2}a^2}=\frac{\sqrt{2}}{2}a\]

\[s = \sqrt{h^2 + \frac{1}{4}a^2} = \sqrt{\frac{1}{2}a^2 + \frac{1}{4}a^2}=\frac{\sqrt{3}}{2}a\]

這些數據則確定能從正八面體構成截角八面體的[截角切割深度](../Page/截角_\(幾何\).md "wikilink")。若太深則會變成[截半八面體](../Page/截半立方體.md "wikilink")。

  -
    [100px](../Page/image:Cuboctahedron_by_Cutting_Rhombic_Dodecahedron.svg.md "wikilink")\[9\]

### 座標

|                                                                                                                                                                   |                                                                                                                                                                 |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Truncated_octahedron_in_unit_cube.png](https://zh.wikipedia.org/wiki/File:Truncated_octahedron_in_unit_cube.png "fig:Truncated_octahedron_in_unit_cube.png") | [Triangulated_truncated_octahedron.png](https://zh.wikipedia.org/wiki/File:Triangulated_truncated_octahedron.png "fig:Triangulated_truncated_octahedron.png") |
| 在(±2,±2,±2)範圍內的平行投影                                                                                                                                               | 每個六邊形面切割成六個正三角形產生了八個新的頂點，他們分別為(±1,±1,±1)的所有組合。                                                                                                                  |

邊長為[2的平方根且](../Page/2的平方根.md "wikilink")[幾何中心位於](../Page/幾何中心.md "wikilink")[原點的截角八面體其頂點座標為](../Page/原點.md "wikilink")(0,
±1, ±2)的所有排列。

### 體積與表面積

截角立方體的體積\(8\sqrt{2} a^3\)，表面積\(6+12\sqrt{3} a^2\)，其中\(a\)是該截半立方體的邊長\[10\]。

  -
    表面積 = \(6+12\sqrt{3} a^2\)≈\(26.785a^2\)
    體積 = \(8\sqrt{2} a^3\)≈\(11.314a^3\)

## 作法

將[正八面體進行](../Page/正八面體.md "wikilink")[截角操作](../Page/截角_\(幾何\).md "wikilink")，也就是將正八面體的六個[頂點切去並在被切掉的地方建立六個](../Page/頂點.md "wikilink")[正方形面即可得到一個](../Page/正方形.md "wikilink")**截角八面體**。

## 正交投影

<table>
<caption>截角八面體的正交投影</caption>
<thead>
<tr class="header">
<th><p>建立於</p></th>
<th><p>頂點</p></th>
<th><p>邊<br />
4-6</p></th>
<th><p>邊<br />
6-6</p></th>
<th><p>面<br />
正方形</p></th>
<th><p>面<br />
正六邊形</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>截角八面體</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Cube_t12_v.png" title="fig:Cube_t12_v.png">Cube_t12_v.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Cube_t12_e46.png" title="fig:Cube_t12_e46.png">Cube_t12_e46.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Cube_t12_e66.png" title="fig:Cube_t12_e66.png">Cube_t12_e66.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:3-cube_t12_B2.svg" title="fig:3-cube_t12_B2.svg">3-cube_t12_B2.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:3-cube_t12.svg" title="fig:3-cube_t12.svg">3-cube_t12.svg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/四角化六面體.md" title="wikilink">四角化六面體</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dual_cube_t12_v.png" title="fig:Dual_cube_t12_v.png">Dual_cube_t12_v.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dual_cube_t12_e46.png" title="fig:Dual_cube_t12_e46.png">Dual_cube_t12_e46.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dual_cube_t12_e66.png" title="fig:Dual_cube_t12_e66.png">Dual_cube_t12_e66.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dual_cube_t12_B2.png" title="fig:Dual_cube_t12_B2.png">Dual_cube_t12_B2.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dual_cube_t12.png" title="fig:Dual_cube_t12.png">Dual_cube_t12.png</a></p></td>
</tr>
<tr class="odd">
<td><p>投影<br />
對稱性</p></td>
<td><p>[2]</p></td>
<td><p>[2]</p></td>
<td><p>[2]</p></td>
<td><p>[4]</p></td>
<td><p>[6]</p></td>
</tr>
</tbody>
</table>

## 球面鑲嵌

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Uniform_tiling_432-t12.png" title="fig:Uniform_tiling_432-t12.png">Uniform_tiling_432-t12.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:truncated_octahedron_stereographic_projection_square.png" title="fig:truncated_octahedron_stereographic_projection_square.png">truncated_octahedron_stereographic_projection_square.png</a><br />
以<a href="../Page/正方形.md" title="wikilink">正方形面為中心</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:truncated_octahedron_stereographic_projection_hexagon.png" title="fig:truncated_octahedron_stereographic_projection_hexagon.png">truncated_octahedron_stereographic_projection_hexagon.png</a><br />
以<a href="../Page/正六邊形.md" title="wikilink">正六邊形面為中心</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/平行投影.md" title="wikilink">平行投影</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 分割

截角八面體可分割成正中央一個正八面體、其餘每個面切成8三角帳塔，剩餘的部分在分割成6個[正四角錐](../Page/正四角錐.md "wikilink")。\[11\]

| 虧格 2                                                                                                                                                      | 虧格 3                                                                                                                                                      |
| --------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------- |
|                                                                                                                                                           |                                                                                                                                                           |
| [D<sub>3d</sub>](../Page/Dihedral_symmetry.md "wikilink"), \[2<sup>+</sup>,6\], (2\*3), order 12                                                          | [T<sub>d</sub>](../Page/Tetrahedral_symmetry.md "wikilink"), \[3,3\], (\*332), order 24                                                                   |
| [Excavated_truncated_octahedron1.png](https://zh.wikipedia.org/wiki/File:Excavated_truncated_octahedron1.png "fig:Excavated_truncated_octahedron1.png") | [Excavated_truncated_octahedron2.png](https://zh.wikipedia.org/wiki/File:Excavated_truncated_octahedron2.png "fig:Excavated_truncated_octahedron2.png") |

## 排列多面體

截角八面體是一種\[12\]\[13\]，可以以更「對稱」的形式表示：四維空間中，(1,2,3,4)所有排列的坐標在三維子空間\(x+y+z+w=10\)組成截角八面體。（對應的[二維形狀是](../Page/二維.md "wikilink")[正六邊形](../Page/正六邊形.md "wikilink")：三維空間中，(1,2,3)所有排列的坐標在二維子空間\(x+y+z=6\)組成正六邊形。）

[Permutohedron.svg](https://zh.wikipedia.org/wiki/File:Permutohedron.svg "fig:Permutohedron.svg")

## 相關多面體及鑲嵌

### 堆砌

[Truncated_octahedra.jpg](https://zh.wikipedia.org/wiki/File:Truncated_octahedra.jpg "fig:Truncated_octahedra.jpg")三維空間。\]\]

  - [截角八面體堆砌](../Page/截角八面體堆砌.md "wikilink")

截角八面體可以獨立填滿整個三維空間，而這種由截角八面體[堆砌出來的幾何圖形稱為](../Page/堆砌.md "wikilink")[截角八面體堆砌](../Page/截角八面體堆砌.md "wikilink")。

[截角八面體堆砌是](../Page/截角八面體堆砌.md "wikilink")[三維空間內](../Page/三維空間.md "wikilink")28個半正密鋪之一，由截角八面體獨立堆積而成，雖然他每個[胞都全等](../Page/胞.md "wikilink")、每[邊皆等長](../Page/邊.md "wikilink")，但其不能稱為正密鋪，因為雖然她只由一種胞，截角八面體組成，但是該胞不是[正多面體](../Page/正多面體.md "wikilink")，因此並非所有“面”皆全等，因此截角八面體堆砌只能稱為半正堆砌。

  - 其他堆砌

| [截角八面體堆砌](../Page/截角八面體堆砌.md "wikilink")                                                                                                      | [小斜方截半正方體堆砌](../Page/小斜方截半正方體堆砌.md "wikilink")                                                                                                         | [截角交錯立方體堆砌](../Page/截角交錯立方體堆砌.md "wikilink")                                                                                                                              |
| --------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Bitruncated_Cubic_Honeycomb.svg](https://zh.wikipedia.org/wiki/File:Bitruncated_Cubic_Honeycomb.svg "fig:Bitruncated_Cubic_Honeycomb.svg") | [Cantitruncated_Cubic_Honeycomb.svg](https://zh.wikipedia.org/wiki/File:Cantitruncated_Cubic_Honeycomb.svg "fig:Cantitruncated_Cubic_Honeycomb.svg") | [Truncated_Alternated_Cubic_Honeycomb.svg](https://zh.wikipedia.org/wiki/File:Truncated_Alternated_Cubic_Honeycomb.svg "fig:Truncated_Alternated_Cubic_Honeycomb.svg") |

### [多胞體](../Page/多胞體.md "wikilink")

<table>
<caption>過截角超方形</caption>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:3-cube_t12.svg" title="fig:3-cube_t12.svg">3-cube_t12.svg</a><a href="https://zh.wikipedia.org/wiki/File:Truncated_octahedron.png" title="fig:Truncated_octahedron.png">Truncated_octahedron.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:4-cube_t12.svg" title="fig:4-cube_t12.svg">4-cube_t12.svg</a><a href="https://zh.wikipedia.org/wiki/File:Schlegel_half-solid_bitruncated_8-cell.png" title="fig:Schlegel_half-solid_bitruncated_8-cell.png">Schlegel_half-solid_bitruncated_8-cell.png</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:5-cube_t12.svg" title="fig:5-cube_t12.svg">5-cube_t12.svg</a><a href="https://zh.wikipedia.org/wiki/File:5-cube_t12_A3.svg" title="fig:5-cube_t12_A3.svg">5-cube_t12_A3.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:6-cube_t12.svg" title="fig:6-cube_t12.svg">6-cube_t12.svg</a><a href="https://zh.wikipedia.org/wiki/File:6-cube_t12_A5.svg" title="fig:6-cube_t12_A5.svg">6-cube_t12_A5.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:7-cube_t12.svg" title="fig:7-cube_t12.svg">7-cube_t12.svg</a><a href="https://zh.wikipedia.org/wiki/File:7-cube_t12_A5.svg" title="fig:7-cube_t12_A5.svg">7-cube_t12_A5.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:8-cube_t12.svg" title="fig:8-cube_t12.svg">8-cube_t12.svg</a><a href="https://zh.wikipedia.org/wiki/File:8-cube_t12_A7.svg" title="fig:8-cube_t12_A7.svg">8-cube_t12_A7.svg</a></p></td>
<td><p>...</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/截角八面体.md" title="wikilink">過截角立方體</a></p></td>
<td><p><a href="../Page/過截角超立方體.md" title="wikilink">過截角超立方體</a></p></td>
<td><p><a href="../Page/過截角五維超立方體.md" title="wikilink">過截角五維超立方體</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [正八面體](../Page/正八面體.md "wikilink")
  - [立方體](../Page/立方體.md "wikilink")

## 參考文獻

  -
  -
  -
## 外部連結

  -
  -
  -
  - [Editable printable net of a truncated octahedron with
    interactive 3D
    view](http://www.dr-mikes-math-games-for-kids.com/polyhedral-nets.html?net=3UtM7vifCnAn5PXmSbX99Eu3LJZAs0nAWn3JyT7et98rnPxTGYml4FXjuQ2tE4viYN0KMgAstBRd0otTWLThQWl9BNNC4uigRoZQUQOQibYqtCuLQw9Ui3OofXtQPEsqQ7#applet)

  - 截角八面體形狀的[扭計骰](../Page/扭計骰.md "wikilink")：[Fisher's Truncated
    Octahedron](http://www.speedcubing.com/ton/collection/Skewbs/pages/Fisher's%20Truncated%20Octahedron.htm)、[Truncated
    Octaminx](http://www.speedcubing.com/ton/collection/Skewbs/pages/Truncated%20Octaminx.htm)

[Category:半正多面體](../Category/半正多面體.md "wikilink")
[Category:空間填充多面體](../Category/空間填充多面體.md "wikilink")
[Category:阿基米德立體](../Category/阿基米德立體.md "wikilink")

1.   (Section 3-9)

2.
3.

4.  Cayley graph of S<sub>4</sub>. This Cayley graph labeling is shown,
    e.g., by .

5.

6.  [John H. Conway](../Page/John_Horton_Conway.md "wikilink"), Heidi
    Burgiel, Chaim Goodman-Strauss, (2008) *The Symmetries of Things*,
    ISBN 978-1-56881-220-5 (Chapter 21, Naming the Archimedean and
    Catalan polyhedra and tilings, Architectonic and Catoptric
    tessellations, p 292-298, includes all the nonprismatic forms)

7.

8.  Cromwell, P. *Polyhedra*, CUP hbk (1997), pbk. (1999). Ch.2 p.79-86
    *Archimedean solids*

9.

10.

11. 1 | url= <http://www.doskey.com/polyhedra/Stewart05.html> |work =
    Alexander's Polyhedra |publisher= doskey.com| date=2006 |
    accessdate=2016-01-30 }}

12.
13.