**神农氏**，又称**烈山氏**，或稱**[连山氏](../Page/连山.md "wikilink")**\[1\]，相傳生存年代在[夏朝](../Page/夏朝.md "wikilink")\[2\]以前，現存文字记载多出現在[戰國以後](../Page/戰國.md "wikilink")\[3\]。相傳“神农尝百草”、教人[医疗与](../Page/医疗.md "wikilink")[农耕](../Page/农耕.md "wikilink")，中國人视之為传说中的[农业和](../Page/农业.md "wikilink")[医药的发明者](../Page/医药.md "wikilink")、守护神，尊称为「[药王](../Page/药王.md "wikilink")」、「[五谷王](../Page/五谷.md "wikilink")」、「五谷先帝」、「神农大帝」等。

## 传说

在戰國至漢朝，神農氏傳說出現在文字記錄中：

  - 《[易经](../Page/易经.md "wikilink")·系辞》：「神农氏作，斫木为耜，揉木为耒，耒耜之利，以教天下。」
  - 《[白虎通](../Page/白虎通.md "wikilink")》：「古之人皆食禽兽肉。至于神农，人民众多，禽兽不足，至是神农因天之时，分地之利，制耒耜，教民农耕。」
  - 《[太平御览](../Page/太平御览.md "wikilink")》引《[周书](../Page/周书.md "wikilink")》：「神农耕而作陶。」
  - 《[史记](../Page/史记.md "wikilink")·补三皇本纪》：「神农始尝百草，始有医药。」
  - 《[世本](../Page/世本.md "wikilink")》：「神农和药济人。」
  - 《[淮南子](../Page/淮南子.md "wikilink")》：「尝百草之滋味，水泉之甘苦……一日而遇七十毒。」

据说神农氏的样貌很奇特，身材瘦削，全身除了頭和四肢外，都是透明的（水晶肚），因此内脏清晰可见。神农氏尝尽百草，只要药草是有毒的，服下后他的内脏就会呈现黑色，因此什么药草对于人体哪一个部位有影响就可以轻易地知道了。后来，由于神农氏服太多种毒药，积毒太深，最後因為[斷腸草](../Page/斷腸草.md "wikilink")（有人說是百足蟲）而身亡。

### 教民耕种

相傳是神農氏發明耕種的方法，他命百姓把穀種收集，然後播在開墾過的田土上，以後百姓便照這方法耕植五穀了，神農氏之稱一源為此。

### 发明耒耜

神农氏首创木制的[耒耜](../Page/耒耜.md "wikilink")，被认为是农业发明之始。

### 尝百草

相传神农氏为辨别各类[草药](../Page/草药.md "wikilink")，亲自尝试，最后试到一种含有剧毒的草药（傳說為[斷腸草或是百足蟲](../Page/斷腸草.md "wikilink")），无法可解，最终便牺牲了生命。神农氏有一条紅色神鞭，名为赭鞭，用来鞭打各类花草，可令到花草的寒、热等特性显露出来。

  - 《[神农本草经](../Page/神农本草经.md "wikilink")》

### 发明陶器

神农氏还发明了陶器，被誉为继火的使用之后的又一大创举。

## 神農氏的称号

神農氏有很多稱號，比如：[稷王](../Page/稷王.md "wikilink")、[五穀王](../Page/五穀王.md "wikilink")、[五穀爺](../Page/五穀爺.md "wikilink")、五穀先帝、開天炎帝、神農仙帝、田主、田祖等等。由於有神農嚐百草的傳說，所以民間又尊稱其為藥王大帝、藥仙等\[4\]。

## 考證

[楊伯峻認為](../Page/楊伯峻.md "wikilink")，在文獻中，神農氏傳說首次出現於《[易經](../Page/易經.md "wikilink")》〈[繫辭](../Page/繫辭.md "wikilink")〉，為[戰國時代的傳說](../Page/戰國.md "wikilink")，在[西周時尚未出現](../Page/西周.md "wikilink")\[5\]。

## 神农洞和神农碑

距离随州市区五十五公里处的烈山上，洞中原有石桌、石凳、石碗及石榻等，传说是神农氏所用的器物。烈山还有神农井、神农宅、神农观、炎帝庙等古建筑。厉山镇北有“炎帝神农氏”碑一座，保存至今。

## 神农故居

相传神农氏出生于烈山，故又稱「烈山氏」，或稱連山氏\[6\]\[7\]。有人在湖北距[随州北四十公里的厉山镇列山](../Page/随州.md "wikilink")[神农洞修建了](../Page/神农洞.md "wikilink")“神农故居”。

“神农故居”，设有神农洞二处（一为谷物[药材贮藏](../Page/药.md "wikilink")，一为居住），并有神农亭、神农塔、神农庙、山南建神农茶室、神农花卉、九龙亭及山北神农母安登浴池，百草园等十数处。

[湖北省西部山区](../Page/湖北省.md "wikilink")，也有一地称为“[神农架](../Page/神农架.md "wikilink")”，也与神农氏有关，缘于神农氏曾到此地搭架采药之传说。

## 神农后裔

相传炎黄时期，炎帝和黄帝皆神农氏之后\[8\]，两部落联合形成[华夏部族](../Page/华夏.md "wikilink")。

炎帝败于黄帝，黄帝为[共主](../Page/共主.md "wikilink")，炎帝部落一部分迁离了[黄河流域](../Page/黄河流域.md "wikilink")。

[蚩尤亦神农氏之后](../Page/蚩尤.md "wikilink")，蚩尤部落在黄河流域战败后，一部分融入黄帝部落，一部分南迁，后又西迁。
今[湘](../Page/湖南.md "wikilink")、[贵间均有](../Page/贵州.md "wikilink")[苗族生活](../Page/苗族.md "wikilink")，自稱为蚩尤之后，因先祖以务农，故而得名。蚩尤为神农氏后裔，蚩尤部落文明程度本来较高，但因战败后后裔居于偏远之山间，日久之后，文化并无进步，抑且退化，反而成为了化外之民。

代表性傳說人物

  - 1.神農化身或蚩尤的漂白：爭議性人物[炎帝](../Page/炎帝.md "wikilink")
  - 2.黃帝元配[嫘祖](../Page/嫘祖.md "wikilink")
  - 3.九黎領袖：爭議性人物[蚩尤](../Page/蚩尤.md "wikilink")
  - 4.三苗領袖或叛軍頭目[驩兜](../Page/驩兜.md "wikilink")
  - 5\.[涇陽王](../Page/涇陽王.md "wikilink")[祿續](../Page/祿續.md "wikilink")（[越南傳說](../Page/越南.md "wikilink")）
  - 6\.[百越之祖](../Page/百越之祖.md "wikilink")[貉龍君](../Page/貉龍君.md "wikilink")（[越南傳說](../Page/越南.md "wikilink")）

代表性民族

  - [汉族](../Page/汉族.md "wikilink")
  - [苗族](../Page/苗族.md "wikilink")
  - [瑤族](../Page/瑤族.md "wikilink")
  - [畲族](../Page/畲族.md "wikilink")
  - [京族](../Page/京族.md "wikilink")
  - [傣族](../Page/傣族.md "wikilink")

## 越南

[越南人的傳說裡記載](../Page/越南人.md "wikilink")[神農氏為其初祖](../Page/神農氏.md "wikilink")。（見[炎帝條目](../Page/炎帝.md "wikilink")）

## 日本

[日本人也信奉神农](../Page/日本人.md "wikilink")，尤其[药商](../Page/药.md "wikilink")、[医师](../Page/医师.md "wikilink")、[摊贩](../Page/摊贩.md "wikilink")，在[东京的](../Page/东京.md "wikilink")[汤岛圣堂有](../Page/汤岛圣堂.md "wikilink")[神农节](../Page/神农节.md "wikilink")
。基于古时的[摊贩团体演变为](../Page/摊贩.md "wikilink")[江湖](../Page/江湖.md "wikilink")[极道团体](../Page/极道.md "wikilink")，连[黑道](../Page/黑道.md "wikilink")[指定暴力团与](../Page/指定暴力团.md "wikilink")[赌博中人也崇信神农](../Page/赌博.md "wikilink")。

## 神農傳說衍生作品列表

### 電視劇

  - 《[遠古的傳說](../Page/遠古的傳說.md "wikilink")》（2010年，[中國中央電視台](../Page/中國中央電視台.md "wikilink")）

## 参考来源

## 参见

  - [炎帝](../Page/炎帝.md "wikilink")
  - [先农](../Page/先农.md "wikilink")
  - [先啬宫](../Page/先啬宫.md "wikilink")
  - [左營豐穀宮](../Page/左營豐穀宮.md "wikilink")
  - [五穀王廟](../Page/五穀王廟.md "wikilink")
  - [炎黃子孫](../Page/炎黃子孫.md "wikilink")
  - [黄帝](../Page/黄帝.md "wikilink")：[黃帝內經](../Page/黃帝內經.md "wikilink")
  - [伏羲](../Page/伏羲.md "wikilink")：[八卦](../Page/八卦.md "wikilink")、[萊布尼茲](../Page/萊布尼茲.md "wikilink")
  - [神農](../Page/神農.md "wikilink")：[神農本草經](../Page/神農本草經.md "wikilink")
  - [三墳五典](../Page/三墳五典.md "wikilink")
  - [神农架](../Page/神农架.md "wikilink")

{{-}}

[Category:中国上古人物](../Category/中国上古人物.md "wikilink")
[S](../Category/医药之神.md "wikilink")
[S](../Category/农耕神.md "wikilink")
[Category:中国神祇](../Category/中国神祇.md "wikilink")
[S](../Category/丰穰神.md "wikilink") [S](../Category/中国人物神.md "wikilink")
[Category:中国农业史](../Category/中国农业史.md "wikilink")
[Category:入祀历代帝王庙景德崇圣殿](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")

1.
2.
3.

4.

5.  楊伯峻《春秋左傳注》襄公29年注：「焦，今河南三門峽市東二里。據史記，周武王封神農之後於焦，此說不可信。蓋神農之名初見於易繫辭，史記封禪書言……，亦託之管仲耳。焦為姬姓國，左傳明言之，何得為神農之後，且周武王時更不知有神農氏也。」

6.  《世譜》云“神農，一曰連山氏，一曰烈山氏”

7.

8.  《漢書·古今人表》炎帝神農氏，張晏注曰：「以火德王，故號曰炎帝。作耒耜，故曰神農。」