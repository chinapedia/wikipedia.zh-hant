[Transistor_Count_and_Moore's_Law_-_2011.svg](https://zh.wikipedia.org/wiki/File:Transistor_Count_and_Moore's_Law_-_2011.svg "fig:Transistor_Count_and_Moore's_Law_-_2011.svg")中[晶體管数目的](../Page/晶體管.md "wikilink")[指數增長曲线符合摩尔定律](../Page/指數增長.md "wikilink")\]\]
**摩尔定律**（）是由[英特尔](../Page/英特尔.md "wikilink")（Intel）创始人之一[戈登·摩尔提出的](../Page/戈登·摩尔.md "wikilink")。其内容为：[集成电路上可容纳的](../Page/集成电路.md "wikilink")[晶体管数目](../Page/晶体管.md "wikilink")，约每隔兩年便会增加一倍；经常被引用的“18个月”，是由英特尔[首席执行官](../Page/首席执行官.md "wikilink")[大衛·豪斯](../Page/大衛·豪斯.md "wikilink")（David
House）提出：预计18个月会将芯片的性能提高一倍（即更多的晶体管使其更快）。\[1\]

半导体行业大致按照摩尔定律发展了半个多世纪，对二十世纪后半叶的世界经济增长做出了贡献，并驱动了一系列科技创新、社会改革、生产效率的提高和经济增长。[个人电脑](../Page/个人电脑.md "wikilink")、[因特网](../Page/因特网.md "wikilink")、[智能手机等技术改善和创新都离不开摩尔定律的延续](../Page/智能手机.md "wikilink")\[2\]。

尽管摩尔定律的现象已经被观察到了数十年，摩尔定律仍应该被视为是对现象的观测或对未来的推测，而不是一个物理定律或自然界的规律，从另一角度看，未来的增长率在逻辑上无法保证会跟过去的数据一样，也就是逻辑上无法保证摩尔定律会持续下去。虽然预计摩尔定律将持续到至少2020年。\[3\]然而，2010年[国际半导体技术发展路线图的更新增长已经在](../Page/国际半导体技术发展蓝图.md "wikilink")2013年年底放缓\[4\]；又比如说英特尔在22奈米跟14奈米的CPU製程上已经放慢了技术更新的脚步，

## 发展历程与未来展望

### 过去的发展历程

随着器件尺寸越来越接近物理极限，研发新一代的工艺节点时，仅缩小器件尺寸是不够的。多家研究机构和半导体公司都在试图改善晶体管结构设计，以尽可能地延续摩尔定律。

高介电常数（high-k）[闸极介电层](../Page/闸极介电层.md "wikilink")（gate
dielectric）的出现使得[閘极对电流的控制更有效](../Page/閘极.md "wikilink")，从45nm节点开始被采用。\[5\]

[多闸极晶体管将](../Page/多闸极晶体管.md "wikilink")[閘极对电流的控制从](../Page/閘极.md "wikilink")[沟道的一个表面增加到了三个表面](../Page/沟道.md "wikilink")，从22nm节点开始被采用。
\[6\]

[Threshold_formation_nowatermark.gif](https://zh.wikipedia.org/wiki/File:Threshold_formation_nowatermark.gif "fig:Threshold_formation_nowatermark.gif")晶体管\(I_{DS}-V_{GS}\)计算机仿真曲线。[临界电压在](../Page/阈值电压.md "wikilink")0.45V左右。右图展现的[纳米线MOSFET中反型沟道的形成](../Page/纳米线.md "wikilink")（电子密度的变化）。\]\]

### 未来展望

为了让摩尔定律延续到更小的器件尺度，学术界和工业界在不同的材料、器件结构和工作原理方面的探索一直在进行中。

探索的问题之一是晶体管的閘極设计。随着器件尺寸越来越小，能否有效的控制晶体管中的电流变得越来越重要。相比于三面都有閘極的[多閘極晶体管](../Page/多閘極晶体管.md "wikilink")，[纳米线晶体管将閘極四面围住](../Page/纳米线.md "wikilink")，从而进一步改善了閘極对电流的控制。

### 摩尔定律还能持续多久

随着新工艺节点的不断推出，晶体管中原子的数量已经越来越少，种种物理极限制约着其进一步发展。比如当閘极长度足够短的时候，[量子隧穿效应就会发生](../Page/量子隧穿效应.md "wikilink")，会导致漏电流增加。关于摩尔定律的终点究竟还有多远，看法并不一致。有预测认为摩尔定律的极限将在2025年左右到来，但也有更乐观的预测认为还能持续更久。AMD
EPYC CPU 64C 128T (ES)已經遺反了\[7\]\[8\]\[9\]

## 概述

1965年4月19日，《电子学》杂志（Electronics
Magazine）第114页发表了摩尔（時任[仙童半導體公司工程师](../Page/仙童半導體公司.md "wikilink")）撰写的文章〈让集成电路填满更多的元件〉，文中预言半导体芯片上集成的晶体管和电阻数量将每年增加一倍。

1975年，摩尔在[IEEE](../Page/IEEE.md "wikilink")[國際電子元件大會上提交了一篇论文](../Page/國際電子元件大會.md "wikilink")\[10\]，根据当时的实际情况对摩尔定律进行了修正，把“每年增加一倍”改为“每两年增加一倍”，而现在普遍流行的说法是“每18个月增加一倍”。但1997年9月，摩尔在接受一次采访时声明，他从来没有说过“每18个月增加一倍”，而且SEMATECH路線圖跟隨24個月的週期。

摩爾定律是簡單評估半導體技術進展的經驗法則，其重要的意義在於大抵而言，若在相同面積的晶圓下生產同樣規格的IC，隨著製程技術的進步，每隔一年半，IC產出量就可增加一倍，換算為成本，即每隔一年半成本可降低五成，平均每年成本可降低三成多。就摩爾定律延伸，IC技術每隔一年半推進一個世代。

1998年時，[台積電董事長](../Page/台積電.md "wikilink")[張忠謀曾表示](../Page/張忠謀.md "wikilink")，摩爾定律在過去30年相當有效，未來10到15年應依然適用。\[11\]

2009年時IBM的研究員預測，“摩爾定律”的時代將會結束，因為研究和實驗室的成本需求十分高昂，而有財力投資在建立和維護晶片工廠的企業很少。\[12\]而且製程也越來越接近半導體的物理極限，將會難以再縮小下去。

由于集成度越高，晶体管的价格越便宜，这样也就引出了摩尔定律的经济学效益，在20世纪60年代初，一个晶体管要10美元左右，但随着晶体管越来越小，小到一根头发丝上可以放1000个晶体管时，每个晶体管的价格只有千分之一美分。据有关统计，按运算10万次乘法的价格算，IBM704电脑为85美分，IBM709降到17美分，而60年代中期IBM耗资50亿研制的IBM360系统电脑已变为3.0美分。

摩爾定律的定义归纳起来，主要有以下三种版本：

1.  積體電路上可容納的電晶體數目，約每隔18個月便增加一倍。
2.  微处理器的性能每隔18个月提高一倍，或价格下降一半。
3.  相同價格所买的电脑，性能每隔18个月增加一倍。

以上几种说法中，以第一种说法最为普遍；第二、三两种说法涉及到价格因素，其实质是一样的。三种说法虽然各有千秋，但在一点上是共同的，即“增加一倍”的周期都是18个月；至于增加一倍的是積體電路上所集成的“電晶體”，是整个“计算机的性能”、还是“一个美元所能买到的性能”，就见仁见智。

## 另一种说法

摩尔定律虽然以[戈登·摩尔的名字命名](../Page/戈登·摩尔.md "wikilink")，但最早提出摩尔定律相关内容的并非摩尔，而是[加州理工学院教授](../Page/加州理工学院.md "wikilink")[卡弗·米德](../Page/卡弗·米德.md "wikilink")。米德是最早关注到摩尔定律所提出的晶体管之类的产量增加，就会引起其价格下降的现象。米德指出，如果给定价格的电脑处理能力每两年提高一倍，那么这一价位的电脑处理装置同期就会降价一半。\[13\]

## 參見

  - [技術奇異點](../Page/技術奇異點.md "wikilink")
  - [馬太效應](../Page/馬太效應.md "wikilink")
  - [维尔特定律](../Page/維爾特定律.md "wikilink")

## 参考来源

## 外部連結

  - [摩爾定律邁入40週年](http://www.intel.com/cd/corporate/pressroom/apac/zht/date/2005/213111.htm)

  - [Computer History Museum - The Silicon Engine 1965 - Moore's Law
    Predicts the Future of Integrated
    Circuits](http://www.computerhistory.org/semiconductor/timeline/1965-Moore.html)

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[Category:指数](../Category/指数.md "wikilink")
[Category:计算机技术](../Category/计算机技术.md "wikilink")
[Category:经验法则](../Category/经验法则.md "wikilink")

1.  <http://news.cnet.com/2100-1001-984051.html>
2.
3.  <http://news.cnet.com/New-life-for-Moores-Law/2009-1006_3-5672485.html>
4.
5.
6.  <http://www.intel.com/content/www/us/en/silicon-innovations/intel-22nm-technology.html>
7.
8.  [The chips are down for Moore’s
    law](http://www.nature.com/news/the-chips-are-down-for-moore-s-law-1.19338)
    Nature, February 2016
9.  [Smaller, Faster, Cheaper, Over: The Future of Computer
    Chips](http://www.nytimes.com/2015/09/27/technology/smaller-faster-cheaper-over-the-future-of-computer-chips.html?&moduleDetail=section-news-2&action=click&contentCollection=Business%20Day&region=Footer&module=MoreInSection&version=WhatsNext&contentID=WhatsNext&pgtype=article&_r=0)
    NY Times, September 2015
10. Moore, Gordon. "Progress in Digital Integrated Electronics" IEEE,
    IEDM Tech Digest (1975) pp.11-13.
11. [半導體產業未來發展（09/02/2011） -
    TSMC](http://www.tsmc.com/chinese/event/chairman/1000902.pdf)
12. [IBM研究員預測：“摩爾定律”的時代將要結束](http://www.cns.hk:89/it/itxw/news/2009/04-11/1641445.shtml)

13. 克里斯·安德森，《免费：商业的未来》，北京：中信出版社，2009年第95页