**馬納薩斯帕克**（Manassas Park,
Virginia）是[美國](../Page/美國.md "wikilink")[維吉尼亞州東北部的一個獨立城市](../Page/維吉尼亞州.md "wikilink")。面積6.4平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口10,290人。

馬納薩斯帕克成立於1975年6月1日。城名以[馬納薩斯命名](../Page/馬納薩斯_\(維吉尼亞州\).md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[M](../Category/弗吉尼亚州城市.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.