[Dove_Sent_Forth_from_the_Ark.png](https://zh.wikipedia.org/wiki/File:Dove_Sent_Forth_from_the_Ark.png "fig:Dove_Sent_Forth_from_the_Ark.png")所绘的诺亚方舟想象图\]\]
**挪亞方舟**（，转写：**；），又譯**諾亞方舟**，是《[希伯来圣经](../Page/希伯來聖經.md "wikilink")·[創世紀](../Page/創世紀.md "wikilink")》中的故事，一艘根據[上帝的指示而建造的大](../Page/上帝.md "wikilink")[船](../Page/船.md "wikilink")，其依原說記載為[方形船隻](../Page/矩形.md "wikilink")，但也有許多的形象[繪畫描繪為近似船形船隻](../Page/绘画.md "wikilink")，其建造的目的是為了讓[諾亞與他的家人](../Page/挪亞.md "wikilink")，以及世界上的各種陸上[生物能夠躲避一場上帝因故而造的](../Page/生物.md "wikilink")[大洪水](../Page/大洪水.md "wikilink")[災難](../Page/灾害.md "wikilink")，記載中诺亚方舟花了几十年才建成（a，创6：18，上帝吩咐诺亚建造方舟时，提到诺亚的儿子和儿媳。b，创7：6，洪水降下诺亚600岁。c，创11：10，两年后诺亚602岁，闪100岁，诺亚502岁生闪。d，因此，诺亚480岁，上帝决定采取行动；诺亚502岁生闪；假设闪30岁结婚，然后上帝吩咐诺亚开始建造方舟，方舟建造的时间是几十年。建造时间最多是100年，减去三个儿子都结婚的年龄。），這段故事分別被紀錄在《希伯来圣经·創世記》（《[旧约圣经](../Page/旧约圣经.md "wikilink")·创世记》）以及[伊斯蘭教的](../Page/伊斯蘭教.md "wikilink")《[古蘭經](../Page/古兰经.md "wikilink")》。

根據[底本學說的推論](../Page/底本學說.md "wikilink")，《創世紀》中的方舟故事，可能具有許多相似但分別獨立的來源；而一般的正統[猶太教與](../Page/犹太教.md "wikilink")[基督教](../Page/基督教.md "wikilink")，則認為方舟的故事只有一位作者，[聖經直譯主義者認為方舟確實停留在](../Page/聖經直譯主義.md "wikilink")[土耳其東北方](../Page/土耳其.md "wikilink")[厄德爾省的](../Page/厄德尔省.md "wikilink")[亚拉拉特山區](../Page/亞拉拉特山.md "wikilink")。[亞伯拉罕諸教對於故事中某些問題已經有了神學上的解釋](../Page/亞伯拉罕諸教.md "wikilink")，例如將方舟解釋為[教會的](../Page/教会.md "wikilink")[預表](../Page/预表.md "wikilink")。

在[美索不達米亞文明中](../Page/美索不达米亚.md "wikilink")，也有與《創世紀》的記載平行的故事，例如中記載一位叫[祖蘇德拉的人](../Page/祖蘇德拉.md "wikilink")，受[神明的警告而建造了一艘船艦](../Page/神.md "wikilink")，並因此逃過了一場將人類消滅的洪水，此外在其他地區，也有許多相似的故事，是世界上廣泛流傳的相似傳說故事之一。

## 方舟设计结构

[Französischer_Meister_um_1675_001.jpg](https://zh.wikipedia.org/wiki/File:Französischer_Meister_um_1675_001.jpg "fig:Französischer_Meister_um_1675_001.jpg")建造船時的故事\]\]
根据《圣经》上的记載，方舟長300[腕尺](../Page/肘_\(單位\).md "wikilink")\[1\]（[肘](../Page/肘_\(單位\).md "wikilink")，以0.445米計算，133.5[米](../Page/米.md "wikilink")，437.99英尺），寬50腕尺（22.3米，73.163英尺），高30腕尺（13.4米，43.963英尺），方舟长度大约是邮轮一半长。若這艘方舟真的曾建成，其大小及排水量約為著名的[鐵達尼號](../Page/泰坦尼克号.md "wikilink")（排水量約5萬3千公頓）的五分之三。方舟总的容积达40000[立方米](../Page/立方米.md "wikilink")，底仓面积8900[平方米](../Page/平方米.md "wikilink")。

方舟設有三層，這樣做不但能够穩定船身，也能使總面積增加至差不多8900平方米。方舟採用柏木一類的防水高脂樹木建造，方舟內外都塗上[焦油](../Page/焦油.md "wikilink")。（創世記6：14－16）聖經沒有提及諾亞怎樣把木材固定在一起，但聖經論述有關洪水的記載以前，已經提及打造各樣銅鐵器具的人。\[2\]

方舟的長度是寬度的6倍、高度的10倍。

其實譯做“方舟”的希伯來語詞，跟譯做[摩西在嬰孩時所藏身的](../Page/摩西.md "wikilink")“箱子”是同一個詞。摩西的母親在箱子塗上[瀝青](../Page/瀝青.md "wikilink")[柏油](../Page/柏油.md "wikilink")，使它可以在[尼羅河漂浮](../Page/尼罗河.md "wikilink")。\[3\]

方舟的形狀有如箱子一般。諾亞看來把動物和超過一年的[糧食平均地安置在方舟上](../Page/糧食.md "wikilink")。紀錄中，载了诺亚一家八口，以及各种飞禽走兽，不洁净动物雌雄各一对，洁净动物雌雄各七对，在洪水来临之时，大地全部被洪水淹没，只有诺亚方舟上的各种生物得以幸免，在洪水过后，诺亚方舟搁浅在了[亚拉拉特山上](../Page/亞拉拉特山.md "wikilink")，最後，上帝以[彩虹為立約的記號](../Page/彩虹.md "wikilink")，不再因人的緣故詛咒大地，並使各種生物存留永不停息。

## 故事敘述

[Michelangelo_Buonarroti_020.jpg](https://zh.wikipedia.org/wiki/File:Michelangelo_Buonarroti_020.jpg "fig:Michelangelo_Buonarroti_020.jpg")的作品《大洪水》（*The
Deluge*）繪於[梵蒂岡的](../Page/梵蒂岡.md "wikilink")[西絲汀教堂](../Page/西絲汀教堂.md "wikilink")。\]\]

《創世紀》第6章到第9章記載了諾亞方舟的故事。创造世界万物的上帝[耶和华見到地上充滿敗壞](../Page/耶和华.md "wikilink")、強暴和不法的邪惡行為，于是計畫用洪水消滅惡人。同時他也發現，人類之中有一位叫做諾亞的好人。《創世紀》記載：「諾亞是個義人，在當時的世代是個完全人」\[4\]。耶和华神指示諾亞建造一艘方舟，並帶著他的妻子、兒子（[閃](../Page/閃姆.md "wikilink")、[含與](../Page/含姆.md "wikilink")[雅弗](../Page/雅弗.md "wikilink")）與媳婦。同時神也指示諾亞將牲畜與鳥類等動物帶上方舟，且必須包括雌性與雄性。

當方舟建造完成時，大洪水也開始了，這時諾亞與他的家人，以及動物們皆已進入了方舟。《創世紀》如此形容洪水剛開始的景況：「當諾亞六百歲，二月十七日那一天，大淵的泉源都裂開了，天上的窗戶也敞開了。四十晝夜降大雨在地上」\[5\]。洪水淹沒了最高的山，在陸地上的生物全部死亡，只有諾亞一家人與方舟中的生命得以存活。

在220天之後，方舟在[亞拉臘山附近停下](../Page/亞拉拉特山.md "wikilink")，且洪水也開始消退。又經過了40天之後，亞拉臘山的山頂才露出。這時諾亞放出了一隻[烏鴉](../Page/烏鴉.md "wikilink")，到處飛卻不願意飛回來，後來放出一隻鴿子，但牠並沒有找到可以棲息的陸地就飛回來了。7天之後諾亞又再次放出鴿子，這次牠立刻就帶回了[橄欖樹的枝條](../Page/油橄欖.md "wikilink")，諾亞這時知道洪水已經散去。又等了7天之後，諾亞最後一次放出鴿子，這次牠便不再回方舟了。諾亞一家人與各種動物便走出方舟。

離開方舟之後，諾亞將一隻祭品獻給神。耶和华闻见献祭的香气決定不再用洪水毀滅世界，並在天空製造了一道彩虹，作為保證。在《創世紀》中，神如此保證：「我使雲彩蓋地的時候，必有虹現在雲彩中，我便紀念我與你們和各樣有血肉的活物所立的約，水就再不氾濫、毀壞一切有血肉的物了」\[6\]。

## 方舟中的日子

在創世記7：11至8：4論到這場大洪水，聖經説：“諾亞活到六百歲那一年［公元前2370年］，［民曆］2月17日那一天，浩瀚深淵的泉源盡都裂開，天上的水閘都打開了。”（）

諾亞把一年分為12個月，每個月30天。在古代，每年的第一個月大約始於現今陽曆的9月中旬。在公元前2370年11月和12月間，大雨在民曆“2月17日那一天”降下，並連續降在地上四十晝夜。

“洪水淹沒了大地150天。……水從地上退去，漸漸下降。過了150天，水就退了很多。［民曆］7月17日，方舟停在亞拉臘山上。”（）從洪水淹沒了大地，到水從地上退去，共150天，也就是五個月。公元前2369年4月，方舟停在亞拉臘山上。

創世記8：5-17接着记录。在差不多兩個半月（73天）之後，在民曆“10月1日”（公曆6月），山頂就都露出來了。（）三個月（90天）之後，在“諾亞六百零一歲那一年，［民曆］1月1日”（公元前2369年9月中旬），諾亞打開方舟的蓋，“見地面已經乾了”。（）一個月又27天（57天）之後，民曆“2月27日”（公元前2369年11月中旬），大地全都乾了。諾亞和家人從方舟裏出來，踏在乾地上。諾亞和家人在方舟裏過了一個陰曆年零10天（370天）。（）

## 方舟停泊之地

在[外高加索一帶](../Page/外高加索.md "wikilink")，曾有不少地方聲稱是諾亞方舟停泊之地，或有相關的民間傳說在流傳，當中比較著名的有[納希契凡](../Page/納希契凡.md "wikilink")。

2000年代初，[香港的基督教學術演講者](../Page/香港.md "wikilink")[梁燕城在考證過當時流行的傳說](../Page/梁燕城.md "wikilink")，以及當時流傳的[衛星圖片](../Page/衛星.md "wikilink")，斷定方舟最後在[土耳其及](../Page/土耳其.md "wikilink")[亞美尼亞邊境的](../Page/亞美尼亞.md "wikilink")[亞拉臘山山頂停下](../Page/亞拉臘山.md "wikilink")。為解開方舟之謎，基督教的諾亞方舟國際事工自從首支華人探索隊於2004年首次登山尋找方舟後，計劃進行方舟探索。2006年8月，一名[庫爾德族的探索家於亞拉臘山上一個洞內發現不明物體](../Page/庫爾德族.md "wikilink")，懷疑是木塊。他立刻聯絡相熟的香港探索隊隊員，並於9月把樣本送往香港作科學分析。[香港大學](../Page/香港大學.md "wikilink")[地球科學系應用地球科學中心對該樣本進行岩相分析](../Page/地球科學.md "wikilink")，鑒定它為石化木結構。香港的[馬灣公園館內的導覽冊子根據這個研究把諾亞方舟當作歷史看待](../Page/馬灣公園.md "wikilink")，引來外界不滿。

2010年4月28日国外媒体报道，[香港和](../Page/香港.md "wikilink")[土耳其的探险队员表示](../Page/土耳其.md "wikilink")，他们在土耳其东部的[亞拉臘山附近找到了传说中的诺亚方舟的船身残骸](../Page/亞拉臘山.md "wikilink")，並拍下現場探勘的實際影片，测试採樣木片後，发现这些残骸的年代可以追溯至4800年前，即《[创世纪](../Page/创世纪.md "wikilink")》中所描述的諾亞方舟的存在时期。香港[导演](../Page/导演.md "wikilink")[杨永祥说](../Page/杨永祥.md "wikilink")：「虽然我们不能百分之百确定它就是诺亚方舟，但可能性达到99.9%。」。但這種說法也受到[英國](../Page/英國.md "wikilink")[牛津大學](../Page/牛津大學.md "wikilink")[古代史講師尼古拉斯](../Page/古代史.md "wikilink")‧普塞爾質疑，他說：「如果公元前2800年歐亞大陸已被3000多米深的洪水所覆蓋，在那之前已存在數個世紀的[埃及和](../Page/埃及.md "wikilink")[美索不達米亞文明如何可以生存](../Page/美索不達米亞.md "wikilink")？」。\[7\]\[8\]

2010年5月1日有一位叫 Randall Price
的宗教教授稱[影音使團稱有關發現是偽造](../Page/影音使團.md "wikilink")（this
is all reported to be a
fake）。他的電郵指出相片全是在[黑海某地拍攝](../Page/黑海.md "wikilink")，在2008年夏天有10名庫爾德人告訴他，一名中國人委聘的嚮導，聘請他們把黑海附近的木樑搬到亞拉臘山山洞。\[9\]

## 對方舟傳說的科學上質疑

諾亞方舟神話，有相當多地方皆與今日的科學認知有所衝突：

  - 史前由於[冰河期的迅速消退](../Page/冰河期.md "wikilink")，故許多古文明都普遍有大洪水的傳說。如[西亞的](../Page/西亞.md "wikilink")[蘇美人](../Page/蘇美人.md "wikilink")、[美洲的](../Page/美洲.md "wikilink")[印地安人](../Page/印地安人.md "wikilink")，都留有大洪水傳說，而古[中國也有](../Page/中國.md "wikilink")[大禹治水的記載](../Page/大禹.md "wikilink")。不過除了諾亞傳說外，這類大洪水傳說，水量都並未到達滅絕全人類的程度，大多是只要躲在高處即可倖存。而且如果這麼多文明都有人從大洪水倖存，則《舊約聖經》記載全世界只有諾亞一家8人存活的故事，就明顯有誤，並不真實。

<!-- end list -->

  - 諾亞方舟就算再巨大，空間也畢竟仍舊十分有限，故它所能攜帶的動物種類勢必不可能超過萬種。這並無法解釋如今地球地表上仍存在的幾百萬種[物種](../Page/物種.md "wikilink")，當初究竟是如何逃過《聖經》上大洪水的。

<!-- end list -->

  - 諾亞在方舟內漂流、生活了1年才著陸，舟內的[草食動物或許可吃乾草勉強撐過](../Page/草食動物.md "wikilink")1年，但[食物鏈上高階的](../Page/食物鏈.md "wikilink")[肉食動物如](../Page/肉食動物.md "wikilink")[獅子](../Page/獅子.md "wikilink")、[老虎](../Page/老虎.md "wikilink")、[豹](../Page/豹.md "wikilink")、[狼](../Page/狼.md "wikilink")、[熊](../Page/熊.md "wikilink")、[鷹類等](../Page/鷹.md "wikilink")，則需犧牲大量的其他動物才得以存活下去，在科學上難以解釋這些肉類來源──就算方舟內可能已導致許多物種大量消滅，也很難維持近1年時間的食物鏈平衡。

<!-- end list -->

  - [熱帶動物與](../Page/熱帶.md "wikilink")[寒帶動物](../Page/寒带.md "wikilink")，其生理構造、生活條件截然不同，在不適合的環境生存下將導致大量死亡。諾亞木造方舟的簡陋環境，在理論上並無法同時兼顧必須在寒冷環境才能生存之[北極熊](../Page/北極熊.md "wikilink")、[南極的](../Page/南極.md "wikilink")[企鵝等](../Page/企鵝.md "wikilink")，以及只能在高溫熱帶地區生活的[長頸鹿](../Page/長頸鹿.md "wikilink")、[斑馬](../Page/斑馬.md "wikilink")、[大象](../Page/大象.md "wikilink")、[鱷魚等](../Page/鱷魚.md "wikilink")。就算姑且寒帶的動物不論，一個木造的方舟內實際上也難以同時存在[熱帶雨林與熱帶](../Page/熱帶雨林.md "wikilink")[沙漠這兩種極端的生態環境的生存空間](../Page/沙漠.md "wikilink")。

<!-- end list -->

  - 高達4000公尺的全球性大洪水，並不可能輕易在短短1年的時間內迅速消退，理由是地球上此時並無他處可供宣洩這些比現在[海洋總容量還高幾千倍的驚人水量](../Page/海洋.md "wikilink")，除非它們是被高溫蒸發到太空中──但若如此高溫，則會導致所有地球生物全部滅絕。

<!-- end list -->

  - 假若當時全世界被洪水淹沒，應該全世界也對這事有記載，但現在只有兩本。

[大英百科全書](../Page/大英百科全书.md "wikilink")1771年版還認為方舟的大小和容量與科學並未有衝突。此後，「[阿塔納斯·珂雪和Buteo已證明了](../Page/阿塔納斯·珂雪.md "wikilink")……被送上船的動物，假設一肘為1.5英尺，動物物種的總數將會被發現比人們想像中少得很多。」

## 底本學說的解釋

[Torah.jpg](https://zh.wikipedia.org/wiki/File:Torah.jpg "fig:Torah.jpg")
[底本學說認為](../Page/底本學說.md "wikilink")《[摩西五經](../Page/摩西五經.md "wikilink")》是在公元前5世紀的時候編輯而成，且擁有4個獨立的來源。而諾亞方舟的故事，則被認為是兩個材料的組合，分別是《[祭司典](../Page/祭司典.md "wikilink")》（英文：）與《[雅威典](../Page/雅威典.md "wikilink")》（英文：）。

年代較早的《雅威典》，是在[猶大王國時期組合而成](../Page/猶大王國.md "wikilink")，其來源可追溯自公元前920年，也就是[猶太民族剛分裂為猶大國與以色列國的不久之後](../Page/以色列歷史.md "wikilink")。《雅威典》的故事敘述較《祭司典》簡單，其中說道：神降下了40天的洪水，諾亞一家與方舟的動物們則活下來。而且這些動物上方舟時，是潔淨的動物各七對，不潔淨的動物各一對。洪水結束之後諾亞建造了祭壇，並獻上牲品。神則決心不再以洪水毀滅世界。《雅威典》並沒有提到神對諾亞的約定。

## 大眾文化

  - 電影

<!-- end list -->

  - 《[挪亞方舟](../Page/挪亞：滅世啟示.md "wikilink")》
  - 《[2012](../Page/2012_\(電影\).md "wikilink")》

## 参考文献

<div class="references-small">

<references />

</div>

### 书籍

  -
  -
  -
## 外部链接

  - [Meredith Kline, "By My
    Spirit"](http://www.kerux.com/documents/KeruxV9N2A1.asp)
  - [S. Najm & Ph. Guillaume, *Jubilee Calendar Rescued from the Flood
    Narrative*, Journal of Hebrew Scriptures
    (2004)](http://www.arts.ualberta.ca/JHS/Articles/article_31.pdf) The
    calendar(s) used in the flood narrative
  - [Depictions of Noah's Ark through
    history](http://www.worldwideflood.com/general/ark_history.htm) at
    World Wide Flood
  - [Problems with a Global
    Flood](http://www.talkorigins.org/faqs/faq-noahs-ark.html) on
    TalkOrigins Archive
  - [Parallels between flood
    myths](https://web.archive.org/web/20060829004437/http://www.noahs-ark-flood.com/parallels.htm)
    Comparison of equivalent lines in six ancient versions of the flood
    story
  - [History of the Collapse of "Flood Geology" and a Young
    Earth](https://web.archive.org/web/20070331124027/http://www.bringyou.to/apologetics/p82.htm),
    adapted from the book *The Biblical Flood* by Davis A. Young
  - [挪亞方舟](https://tr.kingdomsalvation.org/sanbu-004.html)

[N](../Category/基督教神话.md "wikilink")
[A](../Category/伊斯兰教神话.md "wikilink")
[N](../Category/犹太教神话.md "wikilink")
[N](../Category/大洪水.md "wikilink")
[N](../Category/圣经短语.md "wikilink")
[A](../Category/考古學造假案.md "wikilink")
[N](../Category/神话传说中的船.md "wikilink")
[N](../Category/洪水題材作品.md "wikilink")

1.  参看条目中对古代以色列人使用的这个长度单位的解释
2.  参看
3.  参看对摩西的记载
4.
5.
6.
7.
8.
9.