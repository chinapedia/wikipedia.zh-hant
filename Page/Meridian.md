**《Meridian》**\[1\]是[香港](../Page/香港.md "wikilink")[天后](../Page/天后.md "wikilink")[楊千嬅於](../Page/楊千嬅.md "wikilink")2007年8月14日發行的個人粵語大碟，為天價加盟[黎明旗下](../Page/黎明.md "wikilink")[A
music後首張發行的專輯](../Page/東亞唱片_\(製作\).md "wikilink")。唱片名稱有[子午線及全盛期之意](../Page/子午線.md "wikilink")。此唱片亦是近年來[楊千嬅首次不參與唱片製作](../Page/楊千嬅.md "wikilink")，交予唱片公司A
music負責。

## 簡介

唱片在製作人員上大換血，合作多年的人山人海首次不在大碟的創作名單上，大碟製作由王凱文負責。專輯中更首次起用了一些未合作過甚至新人，而楊千嬅更首次拋低製作人的身份不再參與歌曲的製作，只負責唱歌的部分，因此與整體的曲風與她原來的音樂個性有很大的不同。[林夕在上一張大碟](../Page/林夕.md "wikilink")《[Unlimited](../Page/Unlimited_\(楊千嬅專輯\).md "wikilink")》負責了全部填詞，這張大碟也是如此。作曲方面，原來的固定班底[于逸堯](../Page/于逸堯.md "wikilink")，[C.Y.
Kong和](../Page/江志仁.md "wikilink")[Eric
Kowk等也沒有參與創作](../Page/郭偉亮.md "wikilink")。大碟中只有[雷頌德和](../Page/雷頌德.md "wikilink")[伍樂城是固有的班底](../Page/伍樂城.md "wikilink")。

于京延的「化」（國語版為「人情世故」）成為了[四台冠軍歌曲](../Page/2007年度香港四台冠軍歌曲列表.md "wikilink")。[黃思彥第一次與楊千嬅合作](../Page/黃思彥.md "wikilink")，寫下搖滾味道的「別阻我開心」。舊班底雷頌德和伍樂城一共貢獻了三首歌，前者寫了大路情歌「陌路」和富有民族色彩的「不認不認還需認」，亦是同年《楊千嬅All
About Love 2007演唱會》主題曲。後者則寫了「傑出青年」。

新班底方面，[陳台證寫下古典風格的](../Page/陳台證.md "wikilink")「集體回憶」，林夕的詞與「再見二丁目」和[王菲的](../Page/王菲.md "wikilink")「約定」上作了一個回應，被喻為兩首歌的續集。[馮翰銘首次為楊千嬅寫了](../Page/馮翰銘.md "wikilink")「不談風月只談戀愛」。[陳浩賢則寫了](../Page/陳浩賢.md "wikilink")「最後今天」，以及新合作的Robert
Lay寫了「知情識趣」。此外，大碟亦翻唱了70年代[薰妮的名曲](../Page/薰妮.md "wikilink")「每當變幻時」，亦是楊千嬅領銜主演電影《[每當變幻時](../Page/每當變幻時_\(電影\).md "wikilink")》的主題曲。

## 曲目

## 流行榜與獎項

|           歌曲           |          最高位置          |          獎項          |
| :--------------------: | :--------------------: | :------------------: |
| <small>中文歌曲龍虎榜</small> | <small>903專業推介</small> | <small>勁爆本地榜</small> |
|         **化**          |         **1**          |        **1**         |
|        **集體回憶**        |           6            |          3           |
|      **不認不認還需認**       |           3            |          4           |
|         **陌路**         |         **1**          |          10          |
|      **只談風月不談戀愛**      |          \--           |         \--          |

  - 第五派台作品{{〈}}只談風月不談戀愛{{〉}}派台後不久便改派另一單曲{{〈}}All About
    Love{{〉}}，故沒有上榜的紀錄；此單曲收錄於2009年出版的專輯《[原來過得很快樂](../Page/原來過得很快樂.md "wikilink")》。

## 唱片獎項

  - 雪碧榜年度總頒獎禮 - 香港地區優秀專輯獎《[Meridian](../Page/Meridian.md "wikilink")》

## 參考文獻

## 參考

  - [【Meridian】全碟歌詞](http://www.littleoslo.com/lyc/home/?p=518)
  - [樂評︰這是黎明眼中楊千嬅的高峰？](http://3cmusic.com/home/?p=729)
  - [MOOV
    Live](http://moov.netvigator.com/music/static/moovlive/aftershow_miriam.jsp)
  - [新浪網唱片介紹](https://web.archive.org/web/20070820184004/http://music.sina.com.hk/mu/supplement07/miriam/)
  - [電子明週評論](http://www.mingpaoweekly.com/htm/2024/da02.htm)
  - [騰迅音樂評論](http://ent.qq.com/a/20070824/000076.htm)
  - [TOM.com音樂評論](https://web.archive.org/web/20081007010214/http://music.ent.tom.com/2007-08-28/002F/12206884.html)
  - [南方都市報評論](http://www.nanfangdaily.com.cn/southnews/dd/dsb/B10/200708250230.asp)

[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:2007年音樂專輯](../Category/2007年音樂專輯.md "wikilink")
[Category:楊千嬅音樂專輯](../Category/楊千嬅音樂專輯.md "wikilink")

1.