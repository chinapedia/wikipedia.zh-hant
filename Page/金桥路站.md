**金桥路站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[张杨北路](../Page/张杨北路.md "wikilink")[金桥路](../Page/金桥路.md "wikilink")，为[上海轨道交通6号线的地下](../Page/上海轨道交通6号线.md "wikilink")[侧式车站](../Page/侧式站台.md "wikilink")，于2007年12月29日启用。车站以绿色作為佈置的主要色彩。

## 公交换乘

59、573、638、777、778、874、991、993、上川专线、新川专线、浦东6路、金桥1路

## 车站出口

  - 1、2号口：张杨北路南侧，金桥路东（自西向东）
  - 3、4号口：张杨北路北侧，金桥路东（自东向西）

## 图片

[File:JQRSPassing.jpg|收费区地下过街通道](File:JQRSPassing.jpg%7C收费区地下过街通道)
[File:JQRSExit2.jpg|2号出口](File:JQRSExit2.jpg%7C2号出口)

[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")