[Needles_(for_sewing).jpg](https://zh.wikipedia.org/wiki/File:Needles_\(for_sewing\).jpg "fig:Needles_(for_sewing).jpg")
**針**是人類最早期的縫紉工具。針的歷史最早可推至舊石器時代的砭石、石針、[骨針等](../Page/骨針.md "wikilink")。

## 歷史

1930年在北京郊區房山縣周口店龍骨山發現[山頂洞人使用的骨針](../Page/山頂洞人.md "wikilink")，長82毫米，直徑3毫米，針孔殘缺。

## 醫療

注射用品，[注射器必須是中空的](../Page/注射器.md "wikilink")，而且使用前必須消毒。
在中醫方面有[針灸](../Page/針灸.md "wikilink")。

## 釣具

釣針是一種垂釣用的工具。

## 文化

  - 14世纪时，因针极其宝贵，[英国售针仅限于](../Page/英国.md "wikilink")1月1日及1月2日两天，用针者仅限于富人。故通常结婚时，夫家均给予妻子一笔款项作为购针之用，谓之针线钱。“针线钱”这一名词今日在英国仍甚普遍，多指贵妇人用以购买奢侈品之钱，或丈夫给予妻女的零用钱。\[1\]

<!-- end list -->

  - 针是生活中常见的日用品，其特殊的形状常用作俗语。例如中国俗谚“只要功夫深，铁[杵磨成针](../Page/杵.md "wikilink")”、成语“绵里藏针”、“针锋相对”等。

<!-- end list -->

  - 《[圣经](../Page/圣经.md "wikilink")》中写道“骆驼穿过针的眼， 比财主进神的国还容易呢”。\[2\]
  - 在台灣，經歷過[台灣日治時代的長者](../Page/台灣日治時代.md "wikilink")，多會使用「針」來當作貨幣的單位，相當於現代常用的「[分](../Page/分_\(貨幣\).md "wikilink")」。

## 基本縫：七大縫法

  - ①：疏縫
  - ②：平針縫
  - ③：回針縫
  - ④：斜針縫
  - ⑤：交叉縫
  - ⑥：藏針縫
  - ⑦：毛邊縫
  - ⑧：鈕扣縫法
  - ⑨：裙鉤縫法
  - ⑩：暗扣縫法

<!-- end list -->

  - [基本縫-七大縫法第一集\~H14](https://www.youtube.com/watch?v=kCb7zLVbGP8)（YouTube）
  - [毛邊縫步驟示範](https://www.youtube.com/watch?v=UwLbRQPAXQs)（YouTube）
  - [本縫-鈕扣/裙鉤/暗扣縫法第二集\~H14](https://www.youtube.com/watch?v=zBzvD20XKIM)（YouTube）

## 其他

  - [大頭針](../Page/大頭針.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:縫紉工具](../Category/縫紉工具.md "wikilink")

1.  王科一，《傲慢与偏见》译注，上海译文出版社2006年8月第一版，p348，ISBN 978-7-5327-3978-3
2.  见《新约》：太19:24、可10:25、路18:25。也有解释此处针眼并非指缝衣针的孔，而是指[耶路撒冷一座城门的名字](../Page/耶路撒冷.md "wikilink")。