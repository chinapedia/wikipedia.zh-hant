在计算机领域，**CAB**是[微软视窗自带的压缩文件格式](../Page/微软视窗.md "wikilink")，它支援压缩与数字签名并在几种不同的[微软安装工具中都有应用](../Page/微软.md "wikilink")：[Setup
API](../Page/Windows_Installer.md "wikilink")、、（用于从[Internet
Explorer安装](../Page/Internet_Explorer.md "wikilink")[ActiveX组件](../Page/ActiveX.md "wikilink")）以及[Windows
Installer](../Page/Windows_Installer.md "wikilink")。

CAB文件格式与需使用三种[数据压缩方法](../Page/数据压缩.md "wikilink")：

  - [DEFLATE](../Page/DEFLATE.md "wikilink")，由[ZIP](../Page/ZIP.md "wikilink")
    [文件格式的作者](../Page/文件格式.md "wikilink")[Phil
    Katz开发](../Page/Phil_Katz.md "wikilink")。
  - [Quantum](../Page/Quantum_compression.md "wikilink")，licensed from
    David Stafford, the author of the Quantum archiver.
  - [LZX](../Page/LZX.md "wikilink")，由与Tomi
    Poutanen一起开发，在Jonathan加盟到[微软时交付微软公司使用](../Page/微软.md "wikilink")。

CAB文件扩展名也在许多安装工具中使用（如[InstallShield等](../Page/InstallShield.md "wikilink")），但是文件格式并不相同。

## 参见

  - [文件格式列表](../Page/文件格式列表.md "wikilink")

## 外部链接

  - [Microsoft Cabinet
    SDK](http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncabsdk/html/cabdl.asp)
  - [7-Zip](http://www.7-zip.org/) is an
    [LGPL](../Page/LGPL.md "wikilink") licensed utility that supports
    [7z](../Page/7z.md "wikilink")，[ZIP](../Page/ZIP格式.md "wikilink")，CAB,
    [ARJ](../Page/ARJ.md "wikilink")，[GZIP](../Page/Gzip.md "wikilink")，[BZIP2](../Page/Bzip2.md "wikilink")，[TAR](../Page/Tar.md "wikilink")，[Cpio](../Page/Cpio.md "wikilink")，[RPM](../Page/RPM_Package_Manager.md "wikilink")
    and [DEB](../Page/Dpkg.md "wikilink")。
  - [cabextract](http://www.kyz.uklinux.net/cabextract.php) is Free
    Software for unpacking cabinet files in UNIX
  - [libmspack](https://archive.is/20030812080557/http://www.kyz.uklinux.net/libmspack/)
    is an LGPL licensed, portable library for creating and extracting
    CAB files and other Microsoft misc. formats.

[Category:归档格式](../Category/归档格式.md "wikilink")
[Category:视窗管理](../Category/视窗管理.md "wikilink")