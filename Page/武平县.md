**武平县**是[中国](../Page/中华人民共和国.md "wikilink")[福建省](../Page/福建省.md "wikilink")[龙岩市下辖的一个县](../Page/龙岩市.md "wikilink")。

## 行政区划

辖1个街道、13个镇、3个乡：\[1\] 。

## 地理

武平县位于[福建省西南部](../Page/福建省.md "wikilink")，东靠[上杭县](../Page/上杭县.md "wikilink")，南与[广东省](../Page/广东省.md "wikilink")[蕉岭县](../Page/蕉岭县.md "wikilink")、[平远县相接](../Page/平远县.md "wikilink")，西邻[江西省](../Page/江西省.md "wikilink")[会昌县](../Page/会昌县.md "wikilink")、[寻邬县](../Page/寻邬县.md "wikilink")，北与[长汀县相连](../Page/长汀县.md "wikilink")。

## 人物

武平著名人物有[中共中央文化部副部长](../Page/中共中央文化部副部长.md "wikilink")[林默涵](../Page/林默涵.md "wikilink")[中国人民解放军空军司令员](../Page/中国人民解放军空军司令员.md "wikilink")[刘亚楼以及第五任](../Page/刘亚楼.md "wikilink")[黄埔军校校长](../Page/黄埔军校.md "wikilink")[謝肇齊](../Page/謝肇齊.md "wikilink")。拥有何仙姑和客家信仰定光古佛等。

## 特产

[武平绿茶为](../Page/绿茶.md "wikilink")[中国地理标志产品](../Page/中国地理标志产品.md "wikilink")\[2\]。

## 参考资料

## 外部链接

  - [闽粤赣边——武平](http://wuping.gov.cn/)
  - [武平县志](http://www.fjsq.gov.cn/ShowBook.asp?BookType=%B8%A3%BD%A8%CA%A1_%CF%D8%CA%D0%B5%D8%D6%BE&Bookno=3054)
    [武平县志(1988～2000)](http://www.fjsq.gov.cn/ShowBook.asp?BookType=%B8%A3%BD%A8%CA%A1_%CF%D8%CA%D0%B5%D8%D6%BE&Bookno=3093)

[武平县](../Category/武平县.md "wikilink")
[县](../Category/龙岩区县市.md "wikilink")
[龙岩](../Category/福建省县份.md "wikilink")
[5](../Category/全国文明城市.md "wikilink")

1.
2.  [武平绿茶获得地理标志产品保护](http://news.fjii.com/2009/07/16/720777.htm)