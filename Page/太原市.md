**太原市**（[太原话](../Page/太原话.md "wikilink")：），简称**-{并}-**（），古称**晋阳**，别称**并州**，[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山西省的省会](../Page/山西省.md "wikilink")，中部地区重要的中心[城市](../Page/城市.md "wikilink")，全国重要的新材料和先进制造业基地，历史悠久的文化古都。太原市是中华人民共和国山西省的政治、文化和国际交流中心，同时是山西[经济](../Page/经济.md "wikilink")[金融的决策和管理中心](../Page/金融.md "wikilink")。[华北地区重要城市](../Page/华北.md "wikilink")，太原都市圈核心区域。

太原市拥有2500多年的建城史，境内的[晋阳古城是中國歷史上多個朝代的](../Page/晋阳古城.md "wikilink")[都城](../Page/都城.md "wikilink")，故太原又被稱為“龍城”\[1\]\[2\]。它為中国拥有文化遗产项目数最多的城市之一，具有一定的国际影响力。其亦是[中国优秀旅游城市](../Page/中国优秀旅游城市.md "wikilink")、[国家园林城市](../Page/国家园林城市.md "wikilink")、[国家历史文化名城](../Page/国家历史文化名城.md "wikilink")。

## 历史

太原是一座具有两千五百余年历史的古城，也是中国著名历史古都之一，[趙國](../Page/趙國.md "wikilink")、[前秦](../Page/前秦.md "wikilink")、[东魏](../Page/东魏.md "wikilink")、[北齐](../Page/北齐.md "wikilink")、北[晋](../Page/晋.md "wikilink")、[后唐](../Page/后唐.md "wikilink")、[后晉](../Page/后晉.md "wikilink")、[后汉](../Page/后汉.md "wikilink")、[北漢等都曾在此定都](../Page/北漢.md "wikilink")。這使得其成為中国古代北方重要的政治、军事、经济、文化中心。
[Jin_Temple_entrance.JPG](https://zh.wikipedia.org/wiki/File:Jin_Temple_entrance.JPG "fig:Jin_Temple_entrance.JPG")《[史记](../Page/史记.md "wikilink")》记载的、[周朝建立的](../Page/周朝.md "wikilink")[晋祠](../Page/晋祠.md "wikilink")，为纪念[周成王弟](../Page/周成王.md "wikilink")[叔虞](../Page/叔虞.md "wikilink")\]\]

### 秦朝以前

春秋时期，[鲁昭公元年](../Page/鲁昭公.md "wikilink")（公元前541年），晋国[荀吴率兵北征](../Page/荀吴.md "wikilink")，大败占据今太原一带地区的无终及赤狄别族，太原地区始入于中原晋国版图。公元前497年，[赵简子家臣](../Page/赵简子.md "wikilink")[董安-{于}-始建城池](../Page/董安于.md "wikilink")，为[晋阳城](../Page/晋阳城.md "wikilink")，城周四里，高四丈。前455年，[智伯联合韩](../Page/智伯.md "wikilink")、魏瑶攻打赵氏，以[汾水淹灌晋阳城](../Page/汾水.md "wikilink")。后来[赵襄子说服韩](../Page/赵襄子.md "wikilink")、魏两家倒戈，放水倒灌智伯军营，大破智伯军，擒杀智伯瑶，史称[晋阳之战](../Page/晋阳之战.md "wikilink")。晋阳古城在[战国初期](../Page/战国.md "wikilink")，曾做过赵国的都城七十二年之久，赵国也成为战国七雄之一。公元前425年，[赵献子由晋阳迁都](../Page/赵献子.md "wikilink")[中牟](../Page/中牟.md "wikilink")（今河南鹤壁市西）。

### 秦汉南北朝时期

[秦始皇时为全国三十六](../Page/秦始皇.md "wikilink")[郡之一](../Page/郡.md "wikilink")，[汉朝设](../Page/汉朝.md "wikilink")[并州](../Page/并州刺史部.md "wikilink")，为全国十三[州之一](../Page/州.md "wikilink")，太原也因此称为“[并州](../Page/并州.md "wikilink")”，西汉初曾将[并州和](../Page/并州.md "wikilink")[雁门郡合并](../Page/雁门郡.md "wikilink")，设立[代国](../Page/代国.md "wikilink")，国都晋阳，汉文帝、汉景帝即位前均受封于代国。自此晋阳一直为并州的[州府](../Page/州府.md "wikilink")。[西晋时](../Page/西晋.md "wikilink")，匈奴族[刘渊在左国城](../Page/刘渊.md "wikilink")(今[吕梁市](../Page/吕梁市.md "wikilink")[离石区](../Page/离石区.md "wikilink"))另立汉国，矛头指向晋阳，并州刺史[刘琨为御强敌](../Page/刘琨.md "wikilink")，展扩晋阳城，周二十七里，称为州城。[十六国时期势力最大的](../Page/十六国.md "wikilink")[前秦在](../Page/前秦.md "wikilink")[淝水之战战败后](../Page/淝水之战.md "wikilink")，由[苻丕为首的政权曾以此为都](../Page/苻丕.md "wikilink")，后迁都长安。

[南北朝北魏后期](../Page/南北朝.md "wikilink")，[尔朱荣和](../Page/尔朱荣.md "wikilink")[高欢相继控制](../Page/高欢.md "wikilink")[北魏朝政](../Page/北魏.md "wikilink")，在晋阳城内筑[大丞相府](../Page/大丞相府.md "wikilink")，晋阳取代洛阳成为北魏实际政治中心。之后，晋阳为[东魏和](../Page/东魏.md "wikilink")[北齐的](../Page/北齐.md "wikilink")[别都](../Page/别都.md "wikilink")，始终保持着“霸府”的地位，其实际地位甚至高于国都[邺城](../Page/邺城.md "wikilink")。北齐[河清四年](../Page/河清.md "wikilink")（565年），改晋阳县为龙山县，移晋阳县于汾东。东魏孝静帝时，权臣[高欢在晋阳筑规模宏大的晋阳宫](../Page/高欢.md "wikilink")，在天龙山始凿石窟，建避暑宫。

### 隋唐时期

[Feitian_apsaras.jpg](https://zh.wikipedia.org/wiki/File:Feitian_apsaras.jpg "fig:Feitian_apsaras.jpg")制[太原府](../Page/太原府.md "wikilink")[天龙山佛教](../Page/天龙山石窟.md "wikilink")[飞天石刻](../Page/飞天.md "wikilink")\]\]
[隋朝时](../Page/隋朝.md "wikilink")，[太原城成为仅次于](../Page/晋阳城.md "wikilink")[长安和](../Page/大兴城.md "wikilink")[洛阳的全国第三大城市](../Page/隋唐洛阳城.md "wikilink")。[开皇九年](../Page/开皇.md "wikilink")（590年），晋王[杨广扩建](../Page/杨广.md "wikilink")[晋阳宫并在晋阳宫外筑周七里](../Page/晋阳宫.md "wikilink")、高四丈的宫墙，先叫“宫城”，后隋文帝更名为“新城”，与原有的[大明城相对应](../Page/大明城.md "wikilink")。开皇十六年（596年），晋王杨广下令建筑仓城，使得仓城东城墙与新城西城墙相连，城周八里，高四丈。隋炀帝还于晋阳城南潜丘筑大兴国观。

[唐朝发祥于太原](../Page/唐朝.md "wikilink")，因此唐朝初期的几位帝王曾数次扩建晋阳城，与京都长安、东都洛阳并称“三都”、“三京”。[贞观十一年](../Page/貞觀_\(唐朝\).md "wikilink")（637年），并州长史英国公[李绩于汾河东展筑东城](../Page/李绩.md "wikilink")，南北约八里半，东西约五至六里，称“东城”，又因州城在汾河之西，亦称“西城”。武则天在位时，于天授元年升太原府为“北都”，设西城为[晋阳县](../Page/晋阳县_\(秦国\).md "wikilink")，东城为[太原县](../Page/太原县_\(隋朝\).md "wikilink")，又令并州长史[崔神庆又在东城](../Page/崔神庆.md "wikilink")、西城之间以“跨水连堞”之法修建“中城”，正好跨在汾河之上，因此西城又称“都城”。唐玄宗天宝元年再升太原为“北京”，规定晋阳，太原两县为京县，其余太原府属县为畿县，各种规制均与京洛二府相同。至此，太原城达到古代历史上最大规模，由中、东、西三城相连而成：开城门达24座，城墙高三丈五尺，比长安城还要高出一丈五尺；西城内还有大明城（大明宫）、新城（晋阳宫）、仓城，城外西北方还有用于防洪，军事防卫的[罗城](../Page/罗城.md "wikilink")。[安史之乱时](../Page/安史之乱.md "wikilink")，[李光弼率领不足一万官兵成功抵御](../Page/李光弼.md "wikilink")[史思明的十万军队围攻](../Page/史思明.md "wikilink")，获得[太原之战胜利](../Page/太原之战.md "wikilink")。

[五代十国时期](../Page/五代十国.md "wikilink")，太原先后为[后唐](../Page/后唐.md "wikilink")，[后晋](../Page/后晋.md "wikilink")，[后汉三代的发祥地和别都](../Page/后汉.md "wikilink")，仍称为“北京”，后又为[北汉的国都](../Page/北汉.md "wikilink")，故又有“龙城”的称号。

### 宋元时期

[北宋太平兴国四年](../Page/北宋.md "wikilink")（979年），[宋太宗赵光义进攻以晋阳为都的](../Page/宋太宗.md "wikilink")[北汉政权](../Page/北汉.md "wikilink")，最终统一全国。由于战事的激烈程度超过预料，盛怒之下的赵光义下令将并州治移至榆次，随后火烧晋阳城，又引汾、晋之水夷晋阳城为废墟。三年后，赵光义“以榆次地非要会”命[潘美在原晋阳城北四十里处的古镇](../Page/潘美.md "wikilink")[唐明镇基础上](../Page/唐明镇.md "wikilink")，新建[太原城](../Page/太原城.md "wikilink")，并派大将潘美、[杨业镇守](../Page/杨业.md "wikilink")。1023年[宋仁宗即位后](../Page/宋仁宗.md "wikilink")，对太原又进行了修建，太原知州[陈尧佐为了防阻汾水泛滥](../Page/陈尧佐.md "wikilink")，在汾河东岸筑了长堤，并引水潴成湖泊，湖堤畔栽种了许多柳树，名曰“柳溪”，又于东西两山广植松柏槐杏桃等树，两山因此得名“锦绣岭”。[嘉祐四年](../Page/嘉祐.md "wikilink")（1059年），重新设立太原府，府治设于太原城内，随后[晋祠](../Page/晋祠.md "wikilink")[圣母殿](../Page/圣母殿.md "wikilink")、[芳林寺相继竣工](../Page/芳林寺.md "wikilink")。宋朝一代，太原的手工业较为强大，设在孟家井、冶峪的官窑生产的“木理纹瓷”更是闻名全国，史称“花花正定府，锦绣太原城”。宋[崇宁年间](../Page/崇宁.md "wikilink")，太原建制为大都督府，辖十县。

1125年，[金朝部队分两路大举南侵](../Page/金朝.md "wikilink")，西路由[粘罕率军抵达太原城下](../Page/粘罕.md "wikilink")，河东宣抚使[童贯逃跑](../Page/童贯.md "wikilink")，太原知府[张孝纯率众奋力抵抗](../Page/张孝纯.md "wikilink")，在被围二百五十余日后，终因粮绝人亡而城破。1126年，宋钦宗[赵恒下诏将中山](../Page/赵恒.md "wikilink")、河间、太原三城割予金朝。此后的金、元时期，太原城由于地处边疆，遭受战火破坏严重，城市大幅衰落；元朝，太原改冀宁路，直属中书省，设[河东山西道肃政廉访司](../Page/河东山西道宣慰司.md "wikilink")，为元朝御史台直辖的内八道之一。

### 明清时期

[Chinese_Bombard_1377.jpg](https://zh.wikipedia.org/wiki/File:Chinese_Bombard_1377.jpg "fig:Chinese_Bombard_1377.jpg")
[明朝初年](../Page/明朝.md "wikilink")，太原被定为九边重镇之一，[朱元璋封其三子](../Page/朱元璋.md "wikilink")[朱棡为](../Page/朱棡.md "wikilink")[晋王](../Page/晋王.md "wikilink")，并令[谢成扩建太原城](../Page/谢成.md "wikilink")：谢成在原太原城基础上向东、南、北三面扩展，建成了周围14公里，高约18米的城墙，城外城壕深达十米，城头四角建角楼四座，城楼八座，小楼九十二座，敌台三十二座，成为重要的军事防御地区。晋王朱棢并在城内构建王宫，为明朝各王府规模之首。晋王还为母亲[马皇后修建了规模宏大的](../Page/孝慈高皇后_\(明朝\).md "wikilink")[崇善寺](../Page/崇善寺.md "wikilink")，至今仍为太原最重要的佛寺。除了手工业外，明朝时期太原的兵器制造业非常发达。

[清朝初年](../Page/清朝.md "wikilink")，太原受到战火牵连，晋王府和崇善寺均毁于大火。清代中叶，太原手工业进一步发展，此外炼铁、硫磺、煤炭业也很发达。商业方面，亦出现了同行会、银钱、典当。随着[晋商的兴盛](../Page/晋商.md "wikilink")，太原也成为[北方的](../Page/北方.md "wikilink")[商业和](../Page/商业.md "wikilink")[手工业中心](../Page/手工业.md "wikilink")，是中国早期的[工业](../Page/工业.md "wikilink")[城市](../Page/城市.md "wikilink")，太原府所辖[平遥](../Page/平遥县.md "wikilink")、[太谷](../Page/太谷县.md "wikilink")、[祁县成为全国的金融中心](../Page/祁县.md "wikilink")。

### 中华民国

[1948_taiyuan.jpg](https://zh.wikipedia.org/wiki/File:1948_taiyuan.jpg "fig:1948_taiyuan.jpg")期间，[中华民国国军在太原外围](../Page/中华民国国军.md "wikilink")[双塔寺构建的碉堡](../Page/永祚寺.md "wikilink")\]\]

自[辛亥革命后](../Page/辛亥革命.md "wikilink")，[阎锡山长期统治着山西](../Page/阎锡山.md "wikilink")，并以太原为中心，[晋系历史历经](../Page/晋系.md "wikilink")[北洋政府](../Page/北洋政府.md "wikilink")、[中原大战和](../Page/中原大战.md "wikilink")[抗日战争](../Page/抗日战争.md "wikilink")，是中国非常重要的地方[军阀之一](../Page/军阀.md "wikilink")。太原又是当时中国北方極為重要的工业基地，太原兵工厂是当时中国[长城以南最大的兵工厂](../Page/长城.md "wikilink")\[3\]。[抗日战争初期](../Page/中国抗日战争.md "wikilink")，阎锡山与共产党人[薄一波等在太原组建](../Page/薄一波.md "wikilink")[牺牲救国同盟会](../Page/牺牲救国同盟会.md "wikilink")、[山西新军等](../Page/山西新军.md "wikilink")，使得山西境内阎锡山部与八路军组成抗日同盟。1937年11月，[太原会战中最后一场战役](../Page/太原会战.md "wikilink")——[太原保卫战结束](../Page/太原保卫战.md "wikilink")，太原城被[侵华日军占领](../Page/侵华日军.md "wikilink")。抗日结束后，阎锡山率晋绥军接收太原，并开始在太原修建了大纵深的环形防御体系，设了“碉堡建设局”修建和加强了大量碉堡。[第二次国共内战时](../Page/第二次国共内战.md "wikilink")，[徐向前指挥率领的三十余万](../Page/徐向前.md "wikilink")[中国人民解放军部队](../Page/中国人民解放军.md "wikilink")，激战半年后，攻占省会太原，史称[太原战役](../Page/太原战役.md "wikilink")。

### 中华人民共和国

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，继续将太原作为重要的工业城市。\[4\]自1949年至1956年第一个五年计划结束，太原市建设了[太原化工厂](../Page/太原化工厂.md "wikilink")、[太原化肥厂](../Page/太原化肥厂.md "wikilink")、[太原第一发电厂](../Page/太原第一发电厂.md "wikilink")、[太原磷肥厂](../Page/太原磷肥厂.md "wikilink")、[太原重型机械厂](../Page/太原重型机械厂.md "wikilink")、[山西纺织厂](../Page/山西纺织厂.md "wikilink")、[太原第二发电厂](../Page/太原第二发电厂.md "wikilink")、[大众机械厂](../Page/大众机械厂.md "wikilink")、[太原矿棉制品厂等](../Page/太原矿棉制品厂.md "wikilink")28家大型骨干企业。\[5\]

由于地理原因和依靠单一的重工业和煤炭资源，太原一度陷入经济发展缓慢和污染严重的窘境。随着经济结构的调整和政府部门对环境保护的重视，城市经济发展及基础设施建设速度加快，环境不断改善。

## 地理

### 地形

[Taiyuan_112.55998E_37.86930N.jpg](https://zh.wikipedia.org/wiki/File:Taiyuan_112.55998E_37.86930N.jpg "fig:Taiyuan_112.55998E_37.86930N.jpg")
太原市位于[黄土高原东部](../Page/黄土高原.md "wikilink")，[太原盆地北端](../Page/太原盆地.md "wikilink")。北、西、东三面环山，中部和南部一角开阔的[汾河河谷平原](../Page/汾河.md "wikilink")，整个地形北高南低呈簸箕形。海拔最高点为2,670米，最低点为760米，平均海拔800米左右。毗邻[忻州市](../Page/忻州市.md "wikilink")、[吕梁市](../Page/吕梁市.md "wikilink")、[晋中市](../Page/晋中市.md "wikilink")、[阳泉市](../Page/阳泉市.md "wikilink")。

山西最大的河流[汾河](../Page/汾河.md "wikilink")，由[静乐入境自西向东至](../Page/静乐.md "wikilink")[上兰村南折流经太原市城区](../Page/上兰村.md "wikilink")，自北而南纵贯全市，流经约100公里。以丘陵和山地为主，平川约占五分之一。此外还有支流[潇河](../Page/潇河.md "wikilink")、屯兰河、大川河、[柳林河](../Page/柳林河.md "wikilink")、凌井河、杨兴河等。山麓地带泉水较为丰富，尤以[晋祠和兰村泉水流量最大](../Page/晋祠.md "wikilink")，境内较大湖泊六处，以[晋阳湖为最](../Page/晋阳湖.md "wikilink")。汾河两岸沟渠纵横，给工农业用水带来很大方便。

### 气候

太原市属[温带大陆性季风气候](../Page/温带季风气候.md "wikilink")。四季分明，日照充足，太阳能资源丰富。春季升温快，多大风、沙尘天气。夏季相比同纬度之[华北平原地区较为凉爽](../Page/华北平原.md "wikilink")，很少出现气温在35°C以上的高温天气。秋季晴朗干燥。冬季较为寒冷。最冷月（1月）平均气温-5.5°C，极端最低气温-25.5°C（1958年1月16日）。最热月（7月）平均气温23.4°C，极端最高气温39.4°C（1955年7月24日）。\[6\]年平均气温10.0°C。无霜期140－190天，结冰期约120天。年平均降水量425.5毫米。

### 环境污染

太原市空气质量较差。在[中华人民共和国生态环境部公布的](../Page/中华人民共和国生态环境部.md "wikilink")2018年空气质量排名中，太原市在169个重点城市中排名第163位。\[7\]

## 政治

### 现任领导

<table>
<caption>太原市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党太原市委员会.md" title="wikilink">中国共产党<br />
太原市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/太原市人民代表大会.md" title="wikilink">太原市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/太原市人民政府.md" title="wikilink">太原市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议太原市委员会.md" title="wikilink">中国人民政治协商会议<br />
太原市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/罗清宇.md" title="wikilink">罗清宇</a>[8]</p></td>
<td><p><a href="../Page/弓跃.md" title="wikilink">弓跃</a>[9]</p></td>
<td><p><a href="../Page/李晓波.md" title="wikilink">李晓波</a>[10]</p></td>
<td><p><a href="../Page/张明星.md" title="wikilink">张明星</a>[11]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/山西省.md" title="wikilink">山西省</a><a href="../Page/原平市.md" title="wikilink">原平市</a></p></td>
<td><p>山西省<a href="../Page/寿阳县.md" title="wikilink">寿阳县</a></p></td>
<td><p><a href="../Page/内蒙古自治区.md" title="wikilink">内蒙古自治区</a><a href="../Page/清水河县.md" title="wikilink">清水河县</a></p></td>
<td><p>山西省<a href="../Page/保德县.md" title="wikilink">保德县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年6月</p></td>
<td><p>2017年1月</p></td>
<td><p>2019年1月</p></td>
<td><p>2017年3月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市下辖6个[市辖区](../Page/市辖区.md "wikilink")、3个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")\[12\]。

  - 市辖区：[小店区](../Page/小店区.md "wikilink")、[迎泽区](../Page/迎泽区.md "wikilink")、[杏花岭区](../Page/杏花岭区.md "wikilink")、[尖草坪区](../Page/尖草坪区.md "wikilink")、[万柏林区](../Page/万柏林区.md "wikilink")、[晋源区](../Page/晋源区.md "wikilink")
  - 县级市：[古交市](../Page/古交市.md "wikilink")
  - 县：[清徐县](../Page/清徐县.md "wikilink")、[阳曲县](../Page/阳曲县.md "wikilink")、[娄烦县](../Page/娄烦县.md "wikilink")

太原市的經濟管理區約有數十個，其中最重要的國家級經濟管理區為[太原经济技术开发区](../Page/太原经济技术开发区.md "wikilink")、[太原高新技术产业开发区和](../Page/太原高新技术产业开发区.md "wikilink")[太原武宿综合保税区](../Page/太原武宿综合保税区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>太原市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[13]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>140100</p></td>
</tr>
<tr class="odd">
<td><p>140105</p></td>
</tr>
<tr class="even">
<td><p>140106</p></td>
</tr>
<tr class="odd">
<td><p>140107</p></td>
</tr>
<tr class="even">
<td><p>140108</p></td>
</tr>
<tr class="odd">
<td><p>140109</p></td>
</tr>
<tr class="even">
<td><p>140110</p></td>
</tr>
<tr class="odd">
<td><p>140121</p></td>
</tr>
<tr class="even">
<td><p>140122</p></td>
</tr>
<tr class="odd">
<td><p>140123</p></td>
</tr>
<tr class="even">
<td><p>140181</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

### 城市佈局

[Yinze_District,_Taiyuan,_Shanxi.JPG](https://zh.wikipedia.org/wiki/File:Yinze_District,_Taiyuan,_Shanxi.JPG "fig:Yinze_District,_Taiyuan,_Shanxi.JPG")
[汾河将整个太原市区分为东西两个部分](../Page/汾河.md "wikilink")。市区街道整齐，南北为路、东西为街。城北主要为以太钢为主的老工业区和老城生活居住区、城南主要为商业区、新城区、小店城区和开发区，城西为老工业区、新城区、晋源城区。城市发展重心逐渐南移，并与榆次开始同城化进程。

## 交通

### 公路

[太原公交.jpg](https://zh.wikipedia.org/wiki/File:太原公交.jpg "fig:太原公交.jpg")
太原市是山西省的交通樞紐，城区的路网结构以网状为主，最外為[太原环城高速公路](../Page/太原环城高速公路.md "wikilink")。其境內的公路長度達\[14\]，高速公路長度達\[15\]。不過橋樑較少，在約2012年時太原市的橋樑長度僅有，後來經過擴建，，橋樑長度達到約，歸市政部門管轄的橋樑達到177座\[16\]。

與此相應，太原市也有諸多的汽車客運站連接到周邊城市，其中以[太原市新西客站為最大](../Page/太原市新西客站.md "wikilink")，它也是華北地區最大的汽車客運站\[17\]。[太原公交服務由](../Page/太原公交.md "wikilink")[太原市公共交通公司提供](../Page/太原市公共交通公司.md "wikilink")，是較為重要的市民出行工具之一。出租車數量多年來維持在8000輛左右未有變化，因此隨著城市擴張而漸漸不敷使用。\[18\]
此外太原市的民用汽車數量也在不斷增加，，已經達到接近90萬輛，其中屬於個人的將近75萬\[19\]。

### 铁路

[Taiyuan_South_Railway_Station.jpg](https://zh.wikipedia.org/wiki/File:Taiyuan_South_Railway_Station.jpg "fig:Taiyuan_South_Railway_Station.jpg")\]\]
除去公路之外，包括[太中银铁路](../Page/太中银铁路.md "wikilink")、[北同蒲铁路等在內的多条铁路干线汇集于此](../Page/北同蒲铁路.md "wikilink")。交通枢纽有[太原站](../Page/太原站.md "wikilink")、[太原南站为主要的客运火车站](../Page/太原南站.md "wikilink")，其中太原南站将建成华北第二大综合交通枢纽，此外规划了[太原新西站](../Page/太原新西站.md "wikilink")。[太原东站是山西最大货运枢纽](../Page/太原东站.md "wikilink")、[太原北站是华北地区最大货运枢纽](../Page/太原北站.md "wikilink")、[汾河站](../Page/汾河站.md "wikilink")、[晋祠站](../Page/晋祠站.md "wikilink")、[太原西站等为市内环城铁路枢纽](../Page/太原西站.md "wikilink")。[太原轨道交通已经开工勘探](../Page/太原轨道交通.md "wikilink")。

### 机场

[Taiyuan_airport_(6246642416).jpg](https://zh.wikipedia.org/wiki/File:Taiyuan_airport_\(6246642416\).jpg "fig:Taiyuan_airport_(6246642416).jpg")\]\]
[太原武宿国际机场是山西第一大国际机场](../Page/太原武宿国际机场.md "wikilink")，通往国内大部分城市、香港和一些其他國家，东航山西分公司、山西航空有限责任公司駐於此地。2017年旅客吞吐量达到1000万人次。\[20\]

### 地铁

## 经济

2018年，太原市地区生产总值3,884.48亿元，较上一年增长9.2%。第一产业增加值41.05亿元，年增速0.7%；第二产业增加值1,439.13亿元，增长10.3%；第三产业增加值2,404.30亿元，增长8.8%。三次产业增加值占地区生产总值的比例为1.1：37.0：61.9。\[21\]2018年上半年，太原市地区生产总值排名中国第61位，在上榜的100个城市中太原市GDP实际增速排名第五位。\[22\]2017年，太原市地区生产总值在中国26个省会城市中排名第20位。\[23\]

太原是中国重要工业基地之一，以能源、冶金、机械、化工为支柱，纺织、轻工、医药、电子、食品、建材、精密仪器等门类较齐全的工业体系，加上科研机构和大专院校集中及商业物资供应中心的优势，近年逐步形成不锈钢生产加工基地、新型装备制造工业基地和镁铝合金加工制造基地。有代表性的企业包括世界最大的不锈钢生产基地：[太原钢铁集团](../Page/太原钢铁集团.md "wikilink")；镁铝合金加工和研发基地[太原科技工业园](../Page/太原科技工业园.md "wikilink")；中国最大的主焦煤生产基地[山西焦煤集团](../Page/山西焦煤集团.md "wikilink")；制造曾托起[神舟七号升空的航天发射装置和](../Page/神舟七号.md "wikilink")[三峡水电站](../Page/三峡水电站.md "wikilink")1200吨桥式起重机的[太原重型机械集团公司](../Page/太原重型机械集团公司.md "wikilink")。还有中国最大的不锈钢综合销售企业[太原希尔发集团](../Page/太原希尔发集团.md "wikilink")。

[柳巷是太原市中心最繁華](../Page/柳巷.md "wikilink")、最古老的購物街之一，在明朝年間已經出現，\[24\]
因為清朝[光緒年間因為洪水摧毀了城南的商業街](../Page/光緒.md "wikilink")，使得商賈漸漸聚集於此。[辛亥革命之後發展尤其迅速](../Page/辛亥革命.md "wikilink")，並在二十年間發展成太原市的商業中心。\[25\]
除去柳巷外，朝陽街及柳巷附近的[迎澤大街也商鋪雲集](../Page/迎澤大街.md "wikilink")。\[26\]

## 科技

[太原卫星发射中心是中国第二个](../Page/太原卫星发射中心.md "wikilink")[卫星发射基地](../Page/航天發射中心.md "wikilink")，1968年12月18日首次成功发射液体中程运载火箭。1976年成立华北导弹试验基地\[27\]。太原卫星发射中心可以发射多种[卫星](../Page/卫星.md "wikilink")，已成功发射了所有中国的[太阳同步轨道](../Page/太阳同步轨道.md "wikilink")[气象卫星和](../Page/气象卫星.md "wikilink")12颗[美国的](../Page/美国.md "wikilink")[铱星](../Page/铱星.md "wikilink")。

## 人口

<table>
<caption><strong>太原市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[28]（2010年11月）</p></th>
<th><p>户籍人口[29]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="odd">
<td><p>太原市</p></td>
<td><p>4201592</p></td>
<td><p>100</p></td>
</tr>
<tr class="even">
<td><p>小店区</p></td>
<td><p>804537</p></td>
<td><p>19.15</p></td>
</tr>
<tr class="odd">
<td><p>迎泽区</p></td>
<td><p>592007</p></td>
<td><p>14.09</p></td>
</tr>
<tr class="even">
<td><p>杏花岭区</p></td>
<td><p>643584</p></td>
<td><p>15.32</p></td>
</tr>
<tr class="odd">
<td><p>尖草坪区</p></td>
<td><p>415705</p></td>
<td><p>9.89</p></td>
</tr>
<tr class="even">
<td><p>万柏林区</p></td>
<td><p>749255</p></td>
<td><p>17.83</p></td>
</tr>
<tr class="odd">
<td><p>晋源区</p></td>
<td><p>221431</p></td>
<td><p>5.27</p></td>
</tr>
<tr class="even">
<td><p>清徐县</p></td>
<td><p>343861</p></td>
<td><p>8.18</p></td>
</tr>
<tr class="odd">
<td><p>阳曲县</p></td>
<td><p>120228</p></td>
<td><p>2.86</p></td>
</tr>
<tr class="even">
<td><p>娄烦县</p></td>
<td><p>105841</p></td>
<td><p>2.52</p></td>
</tr>
<tr class="odd">
<td><p>古交市</p></td>
<td><p>205143</p></td>
<td><p>4.88</p></td>
</tr>
</tbody>
</table>

根据[第六次全国人口普查公告](../Page/第六次全国人口普查.md "wikilink")，2010年，太原市[常住人口](../Page/常住人口.md "wikilink")4201591人，其中男性人口215.2万人，女性人口205万人，0-14岁人口56.7万人，占13.49%，15-64岁人口330.1万人，占78.58%，65岁及以上人口33.3万人，占7.93%，具有大学程度的人口988552人。根据《中华人民共和国全国分县市人口统计资料2010》，截止2010年底，太原市区非农业户籍人口302万人，居全国第22位，中部地区第2位。\[30\]

### 方言

太原境内的[汉语方言基本上属于](../Page/汉语方言.md "wikilink")[晋语](../Page/晋语.md "wikilink")[并州片](../Page/并州片.md "wikilink")，[太原话是指现今在中国山西省太原市区](../Page/太原话.md "wikilink")
(主要是迎泽区、杏花岭区，以及小店区、尖草坪区、万柏林区的部分地区)的城市居民所通用的方言，是20世纪后逐渐形成的，在太原本地土话的基础上受到外来移民及普通话推广等因素影响而形成的一种新方言。相较并州片其他地区，太原话与普通话的差异要小许多，外地人容易听懂。广义上的太原话也可包括市区城中村、郊区以及远郊的晋源区、古交市、阳曲县、清徐县等地的方言。

## 文化

[Pagodas_of_Taiyuan.jpg](https://zh.wikipedia.org/wiki/File:Pagodas_of_Taiyuan.jpg "fig:Pagodas_of_Taiyuan.jpg")\]\]
[Yingze_Park_8.JPG](https://zh.wikipedia.org/wiki/File:Yingze_Park_8.JPG "fig:Yingze_Park_8.JPG")

### 宗教

太原最初是以佛教为中心。清朝末期基督教大量传入太原，外来的传教士分别属于[罗马天主教会](../Page/罗马天主教会.md "wikilink")、[中国内地会](../Page/中国内地会.md "wikilink")、[英国浸礼会](../Page/英国浸礼会.md "wikilink")、[美国公理会和](../Page/美国公理会.md "wikilink")[寿阳宣教会](../Page/寿阳宣教会.md "wikilink")。1930年代天主教在太原设立[天主教太原总教区](../Page/天主教太原总教区.md "wikilink")，下有[太原圣母无染原罪主教座堂](../Page/太原圣母无染原罪主教座堂.md "wikilink")、[方济各堂和全国著名的](../Page/方济各堂.md "wikilink")[板寺山圣母堂等教堂](../Page/板寺山圣母堂.md "wikilink")。

### 文物保護單位

[Riders_on_Horseback,_Northern_Qi_Dynasty.jpg](https://zh.wikipedia.org/wiki/File:Riders_on_Horseback,_Northern_Qi_Dynasty.jpg "fig:Riders_on_Horseback,_Northern_Qi_Dynasty.jpg")为[国家一级文物中的孤品](../Page/国家一级文物.md "wikilink")\]\]
太原有三十三个[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[晋祠](../Page/晋祠.md "wikilink")、[龙山道教石窟](../Page/龙山道教石窟.md "wikilink")、[晋阳古城遗址](../Page/晋阳古城遗址.md "wikilink")、[窦大夫祠](../Page/窦大夫祠.md "wikilink")、[天龙山石窟](../Page/天龙山石窟.md "wikilink")、[王家峰墓群](../Page/王家峰墓群.md "wikilink")、[狐突庙](../Page/狐突庙.md "wikilink")、[净因寺](../Page/净因寺.md "wikilink")、[清源文庙](../Page/清源文庙.md "wikilink")、[不二寺](../Page/不二寺.md "wikilink")、[明秀寺](../Page/明秀寺.md "wikilink")、[多福寺](../Page/多福寺.md "wikilink")、[永祚寺](../Page/永祚寺.md "wikilink")、[楼烦古城遗址](../Page/楼烦古城遗址.md "wikilink")、[古交遗址](../Page/古交遗址.md "wikilink")、[古交千佛寺](../Page/古交千佛寺.md "wikilink")、阳曲[帖木儿塔](../Page/帖木儿塔.md "wikilink")、前斧柯[悬泉寺](../Page/悬泉寺.md "wikilink")、[辛庄开化寺](../Page/辛庄开化寺.md "wikilink")、[阳曲大王庙大殿](../Page/阳曲大王庙.md "wikilink")、[崇善寺](../Page/崇善寺.md "wikilink")、[太原府文庙](../Page/太原府文庙.md "wikilink")、太原[大关帝庙](../Page/大关帝庙.md "wikilink")、[太原纯阳宫](../Page/纯阳宫_\(太原\).md "wikilink")、[太原天主堂](../Page/太原天主堂.md "wikilink")、太原[唱经楼](../Page/唱经楼.md "wikilink")、[太原清真古寺](../Page/太原清真古寺.md "wikilink")、[中共太原支部旧址](../Page/中共太原支部旧址.md "wikilink")（山西省立一中旧址）、[山西大学堂旧址](../Page/山西大学堂旧址.md "wikilink")、[晋源阿育王塔](../Page/晋源阿育王塔.md "wikilink")、[太山龙泉寺](../Page/太山龙泉寺.md "wikilink")、[晋源文庙](../Page/晋源文庙.md "wikilink")、[清徐尧庙](../Page/清徐尧庙.md "wikilink")。

[晋祠是首批](../Page/晋祠.md "wikilink")[国家AAAA级旅游区之一](../Page/国家AAAA级旅游区.md "wikilink")，原为纪念[周成王之弟](../Page/周成王.md "wikilink")[叔虞所建](../Page/叔虞.md "wikilink")。叔虞之子燮，因境内有晋水，改国号为晋。后人为了奉祀叔虞，在晋水源头建立了祠宇，称唐叔虞祠，后改名晋祠，“周柏隋槐”，“圣母殿侍女像”，“难老泉”为“晋祠三绝”，圣母殿，献殿，鱼沼飞梁，舍利生生塔，水镜台等为国宝级建筑。[双塔寺意喻](../Page/永祚寺.md "wikilink")“水锡祚胤”，始建于[明朝](../Page/明朝.md "wikilink")[万历中叶](../Page/万历.md "wikilink")，当时仅有文峰塔，为“永明寺”。[朱敏淳请](../Page/朱敏淳.md "wikilink")[五台山](../Page/五台山.md "wikilink")[显通寺主持](../Page/显通寺.md "wikilink")[福登和尚](../Page/福登.md "wikilink")，来太原主持扩建，形成“凌霄双塔”，为“太原八景”之一，也成为了太原城市的标志。[崇善寺建于明初](../Page/崇善寺.md "wikilink")，是晋王[朱棡为纪念其生母皇后马氏而建](../Page/朱棡.md "wikilink")，原规模达12万平方米，为六进院落，清初大部分毁于大火，仅存大悲殿及其附属建筑，为原寺总面积的六十分之一。寺内藏有千手千眼观音像等国宝级文物，现为山西省佛教协会所在地。[崛围山位于太原西北呼延村和兰村境内](../Page/崛围山.md "wikilink")，是正在建设中的国家级风景名胜区，其上有多福寺，窦大夫祠，净因寺等国保文物单位，并与汾河二库—柳林河景区相连，具有较高的历史价值和欣赏价值。[天龙山位于太原西南晋源区境内](../Page/天龙山.md "wikilink")，与晋祠景区相邻，为国家级森林公园，保存有大片天然森林以及众多历朝历代文物遗存，北齐时曾建有高欢避暑宫。天龙山最重要的景点当属天龙山石窟，其始建于东魏，历经北齐，隋，唐建成，保存有大量精美的雕塑，被誉为“东方雕塑艺术宝库”。[蒙山大佛位于太原西南部晋源区境内](../Page/蒙山大佛.md "wikilink")，与天龙山景区相邻，“蒙山晓月”为“晋阳八景”之一。蒙山大佛始建于北齐文宣年间，高达63米，仅次于乐山大佛，但建设年代却比乐山大佛早160年，为中国现存最早的大佛，此外山中还有珍贵文物开化寺连理塔，开化寺等。[龙山石窟位于太原西南部晋源区境内](../Page/龙山石窟.md "wikilink")，为全国现存唯一的道教石窟，始建于元代，由宋德芳主持开凿。

### 市徽

1985年4月15日，太原市正式公布自己的[市徽](../Page/市徽.md "wikilink")，在全国首开先河，太原是[中华人民共和国第一座拥有市徽的城市](../Page/中华人民共和国.md "wikilink")。市徽图案由双塔（代表永祚寺）、“并”字、煤层和火焰组成，象征太原市是一座历史悠久、[煤炭资源丰富的能源重化工基地的中心城市和现代化建设蒸蒸日上的新气象](../Page/煤炭.md "wikilink")。

### 饮食

[FuShanTouNao.jpg](https://zh.wikipedia.org/wiki/File:FuShanTouNao.jpg "fig:FuShanTouNao.jpg")为[傅山发明研制的一种颇有滋补效用的汤食](../Page/傅山.md "wikilink")\]\]
晋系菜肴以咸香味为主，很多晋系菜肴中多添加[陈醋](../Page/陈醋.md "wikilink")，而[山西老陈醋亦为中国四大醋品之首](../Page/山西老陈醋.md "wikilink")。太原著名的醋品有宁化府陈醋、清徐东湖陈醋、水塔陈醋等，这些陈醋均闻名中国，甚至远销欧美。元末明初，清源县“美和居”醯坊结合当时的工艺发明了熏蒸法，酿出的醋色呈酱红色，口味醇厚、香、酸、宜气扑鼻。官府遂以其醋荐于明晋王[宁化王府](../Page/宁化王.md "wikilink")，倍受青睐，该熏蒸法亦传于民间，晋地制醯者尽皆习之，名声大振。

[傅山头脑](../Page/傅山头脑.md "wikilink")，又称头脑，是山西太原特有的一种汤状食品。该汤由明末清初文学家、医学家[傅山发明](../Page/傅山.md "wikilink")，以羊肉、羊髓、酒糟、煨面、藕根、长山药，黄芪、良姜等制成，吃时须佐以腌韭，类似药引子，有滋补之用。[傅山将其配料授予清和元饭店](../Page/傅山.md "wikilink")，并在其上写有“头脑杂割”，即“头脑杂割清和元”，表明不与清朝合作态度。此外，太原的[灌肠也很有特色](../Page/灌肠.md "wikilink")，是用荞麦面蒸熟制成的碟形薄片。

[过油肉是山西太原的一道名菜](../Page/过油肉.md "wikilink")。相传最早出现于南北朝時期，本來是平阳（今[临汾](../Page/临汾.md "wikilink")）的一道官菜，后来流传到太原一帶。\[31\]

### 博物館與公園

[Taiyuan_20180607.jpg](https://zh.wikipedia.org/wiki/File:Taiyuan_20180607.jpg "fig:Taiyuan_20180607.jpg")
[山西博物院是国家首批一级博物馆](../Page/山西博物院.md "wikilink")，总藏品達60余万件,珍贵藏品约20万件。其中颇具特色包括新石器时代陶寺遗址文物、商代方国文物、西周、春秋战国时期的晋国及三晋
文物、北朝石刻造像、明清晋商文物等。附属图书馆占地面积超过1000平方米，藏书16万余册，古籍11万余册，包括善本888函、5043册。其保留有[涅槃變相碑](../Page/涅槃變相碑.md "wikilink")、[常阳太尊石像等](../Page/常阳太尊石像.md "wikilink")[国家一级文物中的孤品](../Page/国家一级文物.md "wikilink")。

此外山西省考古研究所馆藏的[北齐时代的](../Page/北齐.md "wikilink")[娄睿墓鞍马出行图壁画](../Page/鞍马出行图.md "wikilink")，堪称中国美术史上的杰作，亦为[中华人民共和国禁止出国（境）展览文物之一](../Page/禁止出国（境）展览文物.md "wikilink")。另外，位于太原的博物馆还有[晋祠博物馆](../Page/晋祠博物馆.md "wikilink")、[中国煤炭博物馆](../Page/中国煤炭博物馆.md "wikilink")、[山西省民俗博物馆](../Page/山西省民俗博物馆.md "wikilink")、[山西省艺术博物馆](../Page/山西省艺术博物馆.md "wikilink")、[太原晋商博物馆](../Page/太原晋商博物馆.md "wikilink")、[太原博物馆](../Page/太原博物馆.md "wikilink")、[太原美术馆](../Page/太原美术馆.md "wikilink")、[山西省科技馆](../Page/山西省科技馆.md "wikilink")、[山西省工艺美术馆等等](../Page/山西省工艺美术馆.md "wikilink")。

太原有為數不少的公園，其中最古老的是[文瀛公園](../Page/文瀛公園.md "wikilink")，歷史可以追溯到明朝時期的海子堰。位於市中心幹道迎澤大街附近的[迎澤公園亦常常遊客雲集](../Page/迎澤公園.md "wikilink")。1990年代末始建於汾河兩邊的[汾河公園現長度已達](../Page/汾河公園.md "wikilink")40里，是[國家水利風景區之一](../Page/國家水利風景區.md "wikilink")。

## 教育

[晋溪书院.JPG](https://zh.wikipedia.org/wiki/File:晋溪书院.JPG "fig:晋溪书院.JPG")
明朝[嘉靖五年](../Page/嘉靖.md "wikilink")，[王子乔后裔](../Page/王子乔.md "wikilink")[王瑶在](../Page/王瑶.md "wikilink")[晋祠圣母殿右下侧](../Page/晋祠.md "wikilink")，建晋溪书院，则广招学子，振兴教育。1902年，清朝以[庚子赔款](../Page/庚子赔款.md "wikilink")，在山西太原兴建[山西大学堂](../Page/山西大学堂.md "wikilink")，后成为[国立山西大学](../Page/国立山西大学.md "wikilink")，建国后其文理学院成为了今天[山西大学的基础](../Page/山西大学.md "wikilink")，工学院成为[太原理工大学的基础](../Page/太原理工大学.md "wikilink")，医学院成为[山西医科大学的基础](../Page/山西医科大学.md "wikilink")。[晋系军阀](../Page/晋系.md "wikilink")[阎锡山在近代创建](../Page/阎锡山.md "wikilink")[山西省立国民师范学校](../Page/山西省立国民师范学校.md "wikilink")，[徐向前](../Page/徐向前.md "wikilink")、[薄一波](../Page/薄一波.md "wikilink")、[程子华](../Page/程子华.md "wikilink")、[王世英均由该校毕业](../Page/王世英.md "wikilink")。

太原的教育由[太原市教育局統籌管理](../Page/太原市教育局.md "wikilink")，除去山西大学和太原理工大学外，太原市還有[中北大学](../Page/中北大学.md "wikilink")、[山西医科大学](../Page/山西医科大学.md "wikilink")、[山西财经大学](../Page/山西财经大学.md "wikilink")、[太原科技大学](../Page/太原科技大学.md "wikilink")、[太原师范学院](../Page/太原师范学院.md "wikilink")、[山西中医学院](../Page/山西中医学院.md "wikilink")、[太原工业学院](../Page/太原工业学院.md "wikilink")、[太原学院](../Page/太原学院.md "wikilink")。其中山西大學和中北大學屬於[省部共建大學](../Page/省部共建大學.md "wikilink")，而太原理工大學是山西唯一的[211工程重點院校](../Page/211工程.md "wikilink")。[山西省实验中学](../Page/山西省实验中学.md "wikilink")、[太原成成中学](../Page/太原成成中学.md "wikilink")、[太原五中](../Page/太原五中.md "wikilink")、[山西大学附属中学等多家省级重点中学也位於太原市](../Page/山西大学附属中学.md "wikilink")。這些院校中有一部分已搬入[太原教育園區](../Page/太原教育園區.md "wikilink")。

太原的图书馆有[山西省图书馆](../Page/山西省图书馆.md "wikilink")、[太原市图书馆](../Page/太原市图书馆.md "wikilink")、山西博物院古籍图书馆、山西大学图书馆、太原理工大学图书馆等。[太原市图书馆为](../Page/太原市图书馆.md "wikilink")[中国国家一级图书馆](../Page/国家一级图书馆.md "wikilink")。太原的体育馆主要有[山西体育场](../Page/山西体育场.md "wikilink")、[滨河体育中心](../Page/滨河体育中心.md "wikilink")、山西省体育馆，新建的山西体育中心也已对外开放，并且已经承办了多项国际国内赛事。

## 友好城市

太原與下列城市互為[友好城市](../Page/友好城市.md "wikilink")(按缔结友好城市关系时间顺序排列)：\[32\]\[33\]\[34\]

  - 英國[紐卡斯爾](../Page/紐卡斯爾.md "wikilink")(缔结友好城市关系时间:1985年4月14日)

  - 日本[姬路市](../Page/姬路市.md "wikilink")(缔结友好城市关系时间:1987年5月19日)

  - 俄羅斯[瑟克特夫卡尔](../Page/瑟克特夫卡尔.md "wikilink")(缔结友好城市关系时间:1994年9月1日)

  - 澳大利亞[朗塞斯顿](../Page/朗塞斯顿.md "wikilink")(缔结友好城市关系时间:1995年11月28日)

  - 俄罗斯[萨拉托夫](../Page/萨拉托夫.md "wikilink")(缔结友好城市关系时间:1995年12月8日)

  - 德國[开姆尼茨](../Page/开姆尼茨.md "wikilink")(缔结友好城市关系时间:1999年5月17日)

  - 喀麥隆[杜阿拉](../Page/杜阿拉.md "wikilink")(缔结友好城市关系时间:1999年10月12日)

  - 美國[纳什维尔](../Page/纳什维尔.md "wikilink")(缔结友好城市关系时间:2007年4月18日)\[35\]

  - 法国[圣但尼](../Page/圣但尼_\(留尼汪\).md "wikilink")(2008年6月签署意向书，2012年3月2日正式结好)\[36\]

  - 乌克兰[顿涅茨克](../Page/顿涅茨克.md "wikilink")(1993年5月签署友好城市意向书，2012年8月25日正式结好)

  - 塔吉克斯坦[胡占德](../Page/胡占德.md "wikilink")(缔结友好城市关系时间:2017年8月31日)\[37\]

## 參見

  - [龍城](../Page/龍城.md "wikilink")

## 參考文獻

## 外部链接

  - [并州文化（太原市圖書館）](https://web.archive.org/web/20110103041159/http://www.tylib.org/bzwh/lsyg.htm)
  - [山西体育事业发展60年](http://www.sxsport.gov.cn/show_hdr.php?xname=6QR8D11&dname=JQH2B11&xpos=6116)

[Category:太原](../Category/太原.md "wikilink")
[Category:山西地级市](../Category/山西地级市.md "wikilink")
[Category:中国省会城市](../Category/中国省会城市.md "wikilink")
[晋](../Category/国家历史文化名城.md "wikilink")

1.

2.  [明代太原的城市建设](http://cpfd.cnki.com.cn/Article/CPFDTOTAL-GDCX200308001026.htm)，高春平，
    《中国古都研究（第二十辑）——中国古都学会2003年年会暨纪念太原建成2500年学术研讨会论文集》，2003年

3.

4.

5.

6.  [中国气象科学数据共享服务网](http://cdc.cma.gov.cn/home.do)

7.

8.

9.

10.

11.

12.

13.

14.

15.
16.

17.

18.

19.
20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.