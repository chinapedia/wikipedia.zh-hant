**東四站**位于[北京市](../Page/北京市.md "wikilink")[东城区](../Page/东城区_\(北京市\).md "wikilink")，是[北京地铁](../Page/北京地铁.md "wikilink")[5号线和](../Page/北京地铁5号线.md "wikilink")[6号线的一座](../Page/北京地铁6号线.md "wikilink")[换乘站](../Page/换乘站.md "wikilink")。车站名取自当地“[东四](../Page/东四.md "wikilink")”的地名。
6号线东四站初步设计时曾称为“隆福寺站”。

## 位置

这个站位于东四路口，即[东四西大街](../Page/东四西大街.md "wikilink")－[朝阳门内大街](../Page/朝阳门内大街.md "wikilink")（东西向）和[东四北大街](../Page/东四北大街.md "wikilink")－[东四南大街](../Page/东四南大街.md "wikilink")（南北向）交叉路口处。5号线车站主体位于路口下方偏东，沿东四南、北大街，呈南北向布置；6号线车站主体位于路口西侧，沿东四西大街在道路下方偏南侧，呈东西向布置；两线车站大致呈“丁”字形分布。\[1\]

## 结构

5号线车站为地下三层车站，底板最深处距地面27米，[岛式站台设计](../Page/岛式站台.md "wikilink")\[2\]，站台宽14米\[3\]。车站工程总长197米，两端明挖法施工，中间暗挖法施工，明挖段为三层三跨框架结构，设置站厅，宽26.2米；暗挖段为单层三拱结构，宽22.9米。

6号线车站为地下二层车站，岛式站台设计。顶板距地面13.5至14米，底板最深处距地面34米，为北京地铁最深的车站。车站主体为三连拱结构，车站全部为暗挖法施工，车站总长192.8米，总宽23.3米，有效站台长度158米，站台宽14米。\[4\]

东四站采取通道换乘，6号线站台位于5号线站台西侧，6号线站厅往5号线站厅设有两个换乘通道，长度分别为60米和110米。5号线车站设有4个出入口，6号线车站设有3个。

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td><p>出入口</p></td>
<td><p>A-G出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一层</strong></p></td>
<td><p>5号线站厅，换乘通道</p></td>
<td><p>客务中心、自动售票机、进出站闸机</p></td>
</tr>
<tr class="odd">
<td><p><strong>地下二层</strong></p></td>
<td><p>6号线出口缓冲层</p></td>
<td><p>客务中心、自动售票机、进出站闸机</p></td>
</tr>
<tr class="even">
<td><p><strong>地下三层</strong></p></td>
<td><p>6号线站厅，换乘通道</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5号线站台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>北</p></td>
<td><p>列车往<a href="../Page/天通苑北站.md" title="wikilink">天通苑北方向</a><small>（<a href="../Page/张自忠路站.md" title="wikilink">张自忠路</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>島式月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南</p></td>
<td><p>列车往<a href="../Page/宋家庄站_(北京市).md" title="wikilink">宋家庄站方向</a><small>（<a href="../Page/灯市口站.md" title="wikilink">灯市口</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>地下四层</strong></p></td>
<td><p>6号线站台</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>西</p></td>
<td><p>列车往<a href="../Page/金安桥站.md" title="wikilink">金安桥方向</a><small>（<a href="../Page/南锣鼓巷站.md" title="wikilink">南锣鼓巷</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>島式月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>东</p></td>
<td><p>列车往<a href="../Page/潞城站_(北京市).md" title="wikilink">潞城方向</a><small>（<a href="../Page/朝阳门站_(北京市).md" title="wikilink">朝阳门</a>）</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 装饰

5号线车站采用浅蓝色色调，站内站台地面拼花铺设有表现老北京[市井文化的](../Page/市井文化.md "wikilink")[中国象棋棋盘残局的装饰](../Page/中国象棋.md "wikilink")。
6号线车站设计为原木颜色的[牌楼结构造型装饰](../Page/牌楼.md "wikilink")，装修着重表现东四地域的文化特色。以古城青砖的元素为主要墙面装饰\[5\]，站厅墙体有表现老北京民俗的“大市街”浮雕壁画装饰。

<File:Platform> of Dongsi Station (Line 5) 20190323.jpg|东四站5号线站台
<File:Dongsi> Station (Line 6) Hall 20131109.JPG|东四站6号线站厅 <File:Dongsi>
Station (Line 6) Platform 20131109.JPG|东四站6号线站台

## 四线换乘

6号线站厅东端与5号线车站相连，西端预留前往[3号线](../Page/北京地铁3号线.md "wikilink")[中国美术馆站的通道](../Page/中国美术馆站.md "wikilink")。在这一规划中藏，3号线车站西端又与[8号线中国美术馆站相连](../Page/北京地铁8号线.md "wikilink")，形成4线换乘。但是，这只是方便5号线与6号线，6号线与3号线，以及3号线与8号线，之间的换乘，其他方向换乘较远，其中5号线和8号线换乘过远，不如借助6号线转车再换乘效率更高。\[6\]

但是，由于3号线张自忠路以西改线经由[平安大街](../Page/平安大街.md "wikilink")，这样的四线换乘方案将不能实现。

## 注释

## 参考资料

[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:北京市东城区地铁车站](../Category/北京市东城区地铁车站.md "wikilink")

1.
2.
3.
4.
5.  [地铁6号线一期车站装修全面展开](http://bjrb.bjd.com.cn/html/2012-05/11/content_83154.htm)
    ，涂露芳，北京日报，2012年05月11日。
6.