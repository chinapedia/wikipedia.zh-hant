**雅各布·赫爾曼**(Jakob Hermann,
），生於[瑞士](../Page/瑞士.md "wikilink")[巴塞爾](../Page/巴塞爾.md "wikilink")，是一位傑出的[數學家](../Page/數學家.md "wikilink")。有關於[經典力學的問題是他的專門研究之一](../Page/經典力學.md "wikilink")。他可能是最先表明[拉普拉斯-龍格-冷次向量守恆的科學家](../Page/拉普拉斯-龍格-冷次向量.md "wikilink")：在[反平方](../Page/反平方定律.md "wikilink")[連心力作用下](../Page/連心力.md "wikilink")，[拉普拉斯-龍格-冷次向量是一個](../Page/拉普拉斯-龍格-冷次向量.md "wikilink")[運動常數](../Page/運動常數.md "wikilink")\[1\]\[2\]。

赫爾曼的啟蒙老師是[雅各布·白努利](../Page/雅各布·伯努利.md "wikilink")。1695
年，他畢業於[巴塞爾大學](../Page/巴塞爾大學.md "wikilink")。1701
年，[普魯士王國立國那年](../Page/普魯士王國.md "wikilink")，赫爾曼被遴選為[柏林科學院](../Page/柏林科學院.md "wikilink")
() 的院士。1707
年，因為數學大師[萊布尼茨的推薦](../Page/萊布尼茨.md "wikilink")，他任職於[義大利的](../Page/義大利.md "wikilink")[帕多瓦大學](../Page/帕多瓦大學.md "wikilink")
() ，專門教授數學。1713
年，他又搬到德國[奧德河畔法蘭克福居住](../Page/奧德河畔法蘭克福.md "wikilink")。1724
年，他被聘請為[聖彼得堡科學院的高等數學教授](../Page/俄羅斯科學院.md "wikilink")；成為[彼得二世·阿列克謝耶維奇](../Page/彼得二世_\(俄羅斯\).md "wikilink")（[彼得大帝的孫子](../Page/彼得大帝.md "wikilink")）的數學老師。1730
年1月30日，[彼得二世在大喜之日](../Page/彼得二世_\(俄羅斯\).md "wikilink")，因痪[天花駕崩](../Page/天花.md "wikilink")。隔年，赫爾曼告老還鄉，返回[巴塞爾大學當](../Page/巴塞爾大學.md "wikilink")[倫理學與自然定律學教授](../Page/倫理學.md "wikilink")。

赫爾曼逝世於 1733 年．那年，他當選為[法國科學院](../Page/法國科學院.md "wikilink") () 的院士。

赫爾曼是[萊昂哈德·歐拉的遠親](../Page/萊昂哈德·歐拉.md "wikilink")。

## 參考

## 外部連結

  -
[H](../Category/瑞士數學家.md "wikilink")
[Category:巴塞爾大學校友](../Category/巴塞爾大學校友.md "wikilink")

1.
2.