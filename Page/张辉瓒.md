**張輝瓚**（），[字石侯](../Page/表字.md "wikilink")，[湖南](../Page/湖南.md "wikilink")[长沙人](../Page/长沙.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民政府](../Page/国民政府.md "wikilink")、[国民革命军高级将领](../Page/国民革命军.md "wikilink")。

## 生平

张辉瓒早年于[湖南兵目学堂毕业](../Page/湖南兵目学堂.md "wikilink")，后留学于[日本军校](../Page/日本.md "wikilink")，[辛亥革命后回国](../Page/辛亥革命.md "wikilink")，任湖南陆军参谋，后又一度前往[德国进修](../Page/德国.md "wikilink")。回国后于1920年出任湖南陆军第四混成旅旅长，与[毛泽东等一同参加了驱逐](../Page/毛泽东.md "wikilink")[张敬尧的](../Page/张敬尧.md "wikilink")[驱张运动](../Page/驱张运动.md "wikilink")。1924年升任湖南第九师师长。1926年率部参加[北伐](../Page/北伐.md "wikilink")，被任命为[国民革命军第二军第四师师长](../Page/国民革命军第二军.md "wikilink")，1929年改任第十八师师长，军衔中将，后因[鲁案](../Page/鲁案.md "wikilink")，随长官[鲁涤平调防](../Page/鲁涤平.md "wikilink")[江西](../Page/江西.md "wikilink")。
[张辉瓒驳壳枪怀表.jpg](https://zh.wikipedia.org/wiki/File:张辉瓒驳壳枪怀表.jpg "fig:张辉瓒驳壳枪怀表.jpg")
1930年底，被任命为前敌总指挥，统领约10万人对[中国工农红军发动围剿](../Page/中国工农红军.md "wikilink")，史称[第一次江西剿共戰爭](../Page/第一次江西剿共戰爭.md "wikilink")。12月30日，率领第十八师在[龙冈战斗中遭到红军主力围攻](../Page/龙冈战斗.md "wikilink")，兵败被俘。后经[蒋介石](../Page/蒋介石.md "wikilink")、鲁涤平等人多方营救未果，于1931年1月28日在吉安县东固召开的反“围剿”胜利祝捷大会上被公审之后，当场遭到砍头处决。之后，将张的头装进一只竹笼，顺[赣江抛之](../Page/赣江.md "wikilink")\[1\]。2月2日在吉安的神岗山附近国民党兵发现这只竹笼并打捞上来。按照蒋介石命令，厚葬于其故乡长沙[岳麓山](../Page/岳麓山.md "wikilink")。其陵墓在「[文化大革命](../Page/文化大革命.md "wikilink")」中被毁，2008年5月由長沙市政府從新修繕並立碑以銘。

张辉瓒与毛泽东是同乡旧友，曾在湖南一起参加驱张运动。据说毛泽东曾说不杀张辉瓒，但是最终他还是让[何长工押送张辉瓒参加公审大会](../Page/何长工.md "wikilink")，后又因会场失控，群众杀死了张辉瓒。张辉瓒被杀后，國民政府進行报复行動，将南昌下沙窝监狱囚禁的100多名共产党政治犯，用电击昏装进麻袋丢进了赣江。还动用大量军警在南昌、上海、武汉、全国各地搜捕共产党。\[2\]

## 张辉瓒墓

[张辉瓒墓.jpg](https://zh.wikipedia.org/wiki/File:张辉瓒墓.jpg "fig:张辉瓒墓.jpg")

  - 座落在长沙岳麓山半山腰，毁于文革，2008年5月重修。
  - 墓前竖立的青石碑正面刻有蒋介石“魂兮归来”的题字（重修时，多了“张公石侯之墓”几字，题字被移到底下）。

## 参考文献

  - 星岛日报：《[砍了張輝瓚
    中共遭到嚴厲報復](https://web.archive.org/web/20080630030516/http://www.stnn.cc:82/reveal/200702/t20070225_474855.html)》

[H](../Category/张姓.md "wikilink")
[Category:保定陆军军官学校校友](../Category/保定陆军军官学校校友.md "wikilink")
[Category:日本陆军士官学校校友](../Category/日本陆军士官学校校友.md "wikilink")
[Category:中華民國陸軍中將](../Category/中華民國陸軍中將.md "wikilink")
[Category:国民革命军将领](../Category/国民革命军将领.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:中國被斬首者](../Category/中國被斬首者.md "wikilink")
[Category:中华民国大陆时期被中国共产党杀害者](../Category/中华民国大陆时期被中国共产党杀害者.md "wikilink")
[Category:長沙人](../Category/長沙人.md "wikilink")

1.  郜合启：《[张辉瓒被俘虏和处死密闻](http://cpc.people.com.cn/GB/68742/70423/70424/5398059.html)》，中国共产党新闻网
2.  郜合启：《[张辉瓒被俘虏和处死密闻](http://cpc.people.com.cn/GB/68742/70423/70424/5398059.html)》，中国共产党新闻网