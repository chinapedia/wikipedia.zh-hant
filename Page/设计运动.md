**设计运动**指的是因为制作手段、工艺改进和消费水平不同，而导致的国际性[设计潮流和风格改变的](../Page/设计.md "wikilink")[艺术运动](../Page/艺术运动.md "wikilink")。主要地區在歐美。大致可分為下幾類的設計運動，有時也稱為**設計流派**或是**視覺藝術運動**（Visual
Art Movement）。

要注意的是，設計運動與設計流派並不是絕對的，在不同的設計評論家、設計史家當中、有不同的歸納做法。在此列出是廣為人知的整理。越晚期的設計運動就越有爭議性，也分的越細。而且會與文化運動、藝術運動混合在一起。

## 簡單的設計流派整理

### 早期

  - 1880年到1910年：[工艺美术运动](../Page/工艺美术运动.md "wikilink")
  - 1880年代：[新艺术运动时期](../Page/新艺术运动.md "wikilink")，主要在1892年至1902年达到顶峰（1900年，[巴黎](../Page/巴黎.md "wikilink")[世界博览会時](../Page/世界博览会.md "wikilink")），大约1907年以后就开始被忽视。
      - 下列流派也被歸類在在新艺术运动當中：

新艺术在德国被称作“青年风格”\[JUGENDSTIL\]， 在法国被称作“现代风格”\[MODERN STYLE\]，
在意大利被称亻“自由风格”\[STILE LIBERTY\]。

  -   - 维也纳分离派运动
      - 德国青年运动
      - 意大利自由风格

  - 1925年到1930年代：[装饰艺术运动时期](../Page/装饰艺术运动.md "wikilink")

      - 在[美國發展的比較久](../Page/美國.md "wikilink")，一直到1960年代達到巔峰，代表為。

### 中期

  - 1930年代；[现代主义运动](../Page/现代主义.md "wikilink")，1890年開始持續到1945年
      - 下列流派也被歸類在在现代主义运动當中：
          - [荷兰风格派运动](../Page/荷兰风格派运动.md "wikilink")

          - [俄国构成主义设计](../Page/構成主義_\(藝術\).md "wikilink")

          - [德意志工藝聯盟](../Page/德意志工藝聯盟.md "wikilink")

          -
### 近代

  - 1960年代：[波普艺术](../Page/波普艺术.md "wikilink")
  - 1970年代：[后现代主义](../Page/后现代主义.md "wikilink")
  - 1980年代：[解构主义在](../Page/解构主义.md "wikilink")1960年代晚期就醞釀。

### 建筑设计

  -   - 建筑師[罗伯特·斯坦恩在](../Page/罗伯特·斯坦恩.md "wikilink")[Modern
        Classicism](http://www.amazon.com/exec/obidos/tg/detail/-/0847808483/qid=1103546263/sr=1-23/ref=sr_1_23/102-8793905-2284942?v=glance&s=books)書中的分类

          - [冷嘲热讽古典主义](../Page/冷嘲热讽古典主义.md "wikilink")(Ironic
            Classicism)或符号性古典主义(Semiotic Classism)
          - [潜伏古典主义](../Page/潜伏古典主义.md "wikilink")(Latent Classicism)
          - [原教旨古典主义](../Page/原教旨古典主义.md "wikilink")(Fundamentalish
            Classicism)
          - [规范古典主义](../Page/规范古典主义.md "wikilink")(Canonic Classicism)

      - 的分类

          - [原教旨古典主义](../Page/原教旨古典主义.md "wikilink")(Fundamentalish
            Classicism)
          - [复兴古典主义](../Page/复兴古典主义.md "wikilink")(Revivalist
            Classicism)
          - [都市古典主义](../Page/都市古典主义.md "wikilink")(Urbanist Classicism)
          - [折衷古典主义](../Page/折衷古典主义.md "wikilink")(Eclectic Classicism)

### 产品设计

所谓产品，是指人类生产制造的物质产品，是人类设计思想付之于实施的产物。产品设计所着重考虑的是产品的造型、结构和功能等，设计的目的是生产制造出符合人们需要的实用、经济、美观的产品，从原始人敲砸而成的石器，到今天的飞机、汽车、计算机都是人类产品设计的结晶。

从生产方式的角度看，产品设计包括手工业加工的手工艺设计及现代大机器生产的工业设计两大类。

  - [高科技风格设计](../Page/高科技风格设计.md "wikilink")(High-Tech)
  - [过渡高科技风格设计](../Page/过渡高科技风格设计.md "wikilink")(Trans High Tech)
  - [减少主义风格设计](../Page/减少主义风格设计.md "wikilink")(Minimalism)
  - [建筑风格设计](../Page/建筑风格设计.md "wikilink")(Archetypes)
  - [微建筑风格设计](../Page/微建筑风格设计.md "wikilink")(Micro-Architecture)
  - [微电子风格设计](../Page/微电子风格设计.md "wikilink")(Micro-Electronics)

## 參見

  - [新现代主义](../Page/新现代主义.md "wikilink")

[Category:设计](../Category/设计.md "wikilink")
[Category:藝術運動](../Category/藝術運動.md "wikilink")