**縣道196號
三星－下清水**，西起[宜蘭縣](../Page/宜蘭縣.md "wikilink")[三星鄉](../Page/三星鄉_\(台灣\).md "wikilink")，東至宜蘭縣[五結鄉下清水](../Page/五結鄉.md "wikilink")，全長共計22.231公里\[1\]。

## 行經行政區域

  - 全線均位於[宜蘭縣境內](../Page/宜蘭縣.md "wikilink")

<!-- end list -->

  - [三星鄉](../Page/三星鄉_\(台灣\).md "wikilink")：[三星](../Page/三星_\(大字\).md "wikilink")0.0k（岔路）→月眉1.18k→[大洲](../Page/大洲_\(宜蘭縣\).md "wikilink")9.28k→清洲12.15k
  - [羅東鎮](../Page/羅東鎮_\(臺灣\).md "wikilink")：復興純精路口14.611k（岔路）
  - [五結鄉](../Page/五結鄉.md "wikilink")：福興村17.5k（岔路）→五結市區18.095k（鄉道宜25線交叉）→孝威21.33k（岔路）→
    [下清水](../Page/下清水.md "wikilink")（[宜22線岔路](../Page/宜22線.md "wikilink")，五結防潮閘門，[蘭陽溪口](../Page/蘭陽溪.md "wikilink")）22.231k

## 支線

### 甲線

[TW_CHW196a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW196a.svg "fig:TW_CHW196a.svg")
**縣道196甲線
東安－季新**，西起[宜蘭縣](../Page/宜蘭縣.md "wikilink")[羅東鎮光榮路](../Page/羅東鎮.md "wikilink")，往東沿羅東連絡道（傳藝路）經國道5號[羅東交流道](../Page/羅東交流道.md "wikilink")，至宜蘭縣[五結鄉銜接](../Page/五結鄉.md "wikilink")[台2線後](../Page/台2線.md "wikilink")，往北與台2線共線1.2公里，至縣道196號岔路口止，全長共計6.238公里\[2\]。2018年1月12日納編\[3\]。本路段曾被[宜蘭縣政府先行編為縣道](../Page/宜蘭縣政府.md "wikilink")206號並掛牌，但編號201以上為離島縣道專用，後來取消並拆除。

## 沿線風景

  - [五結防潮閘門](../Page/五結防潮閘門.md "wikilink")
  - [國立傳統藝術中心](../Page/國立傳統藝術中心.md "wikilink")
  - [羅東林業文化園區](../Page/羅東林業文化園區.md "wikilink")
  - [羅東攔河堰](../Page/羅東攔河堰.md "wikilink")
  - [三星青蔥文化館](../Page/三星青蔥文化館.md "wikilink")

## 交流道

  - [TWHW5.svg](https://zh.wikipedia.org/wiki/File:TWHW5.svg "fig:TWHW5.svg")[羅東交流道](../Page/羅東交流道.md "wikilink")

## 參考來源

## 相關連結

[Category:台灣縣道](../Category/台灣縣道.md "wikilink")
[Category:宜蘭縣道路](../Category/宜蘭縣道路.md "wikilink")

1.
2.
3.