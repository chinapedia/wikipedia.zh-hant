[Monte_Cassino_Opactwo_1.JPG](https://zh.wikipedia.org/wiki/File:Monte_Cassino_Opactwo_1.JPG "fig:Monte_Cassino_Opactwo_1.JPG")上的本笃修道院。\]\]

**本笃会**（）又譯為**本尼狄克派**，亦被稱為“黑修士”（得名于修會僧袍的顏色），是[天主教的一個](../Page/天主教.md "wikilink")[隐修会](../Page/修道運動.md "wikilink")，是在529年由意大利人[圣本笃在](../Page/圣本笃.md "wikilink")[意大利中部](../Page/意大利.md "wikilink")[卡西诺山所创](../Page/卡西诺山.md "wikilink")，遵循中世紀初流行於[義大利和](../Page/義大利.md "wikilink")[高盧的隱修活動](../Page/高盧.md "wikilink")。其規章成為西歐和北歐隱修的主要規章。本笃会隐修院的象征是十字架及耕地的犁。

## 会规

[圣本笃所订的会规共七十三章](../Page/圣本笃.md "wikilink")。非常生活化，照顾到祈祷、求知、劳动各方面，并提倡适度的灵修。会规规定会士不可婚娶，不可有私财，一切服从长上，称此为“发三愿”。
本笃会会士每日必须按时进经堂诵经，咏唱“大日课”，余暇时从事各种劳动。会规要求[祈祷不忘工作](../Page/祈祷.md "wikilink")，视游手好闲为罪恶。后来该会规成为[天主教](../Page/天主教.md "wikilink")[修会制度的范本](../Page/修会.md "wikilink")。本笃会传统重视教会音乐，今[法国](../Page/法国.md "wikilink")、[比利时和](../Page/比利时.md "wikilink")[意大利等国的本笃会修院仍保持这一传统](../Page/意大利.md "wikilink")。
本笃会规是要建立一种生活方式，就是借着基督、在基督内、靠着圣神回归到上帝面前。这样规律的生活结构是为了要随时随地意识到天主的临在。而在团体生活中，每个人都必须要有内在的改变，才能持守对基督的誓约。并借着对地方和人的恒常关系，来达成这个永久的盟约.

## 历史

9世紀本篤會漸漸興起，9世纪后，许多修院会规松弛。

10世纪时，法国[克吕尼隐修院首先发起改革运动](../Page/克吕尼隐修院.md "wikilink")，称“重修本笃会”。11世纪初在法国[第戎附近的熙笃旷野又有](../Page/第戎.md "wikilink")[熙笃会产生](../Page/熙笃会.md "wikilink")，在法国[查尔特勒山](../Page/查尔特勒山脉.md "wikilink")[圣勃路诺创办有](../Page/圣勃路诺.md "wikilink")[加多森会](../Page/加尔都西会.md "wikilink")。15世纪－16世纪时，因会士到[殖民地传教](../Page/殖民地.md "wikilink")，该会的隐修性质逐渐消失。

本笃会产生过24位[教宗](../Page/教宗.md "wikilink")，4600主教，五千多位圣人。

## 本笃会在中国

1925年，[美国本笃会的神父来到中国](../Page/美国.md "wikilink")[北京](../Page/北京.md "wikilink")，创建了[辅仁大学](../Page/辅仁大学.md "wikilink")。不久移交给[圣言会负责](../Page/圣言会.md "wikilink")，还在[河南](../Page/河南.md "wikilink")[开封传教](../Page/开封.md "wikilink")。1930年代退出中国。

## 延伸閱讀

  - Dom Columba Marmion OSB, *Christ the Ideal of the Monk – Spiritual
    Conferences on the Monastic and Religious Life* (Engl. edition
    London 1926, trsl. from the French by a nun of Tyburn Convent).
  - Mariano Dell'Omo, *Storia del monachesimo occidentale dal medioevo
    all'età contemporanea. Il carisma di san Benedetto tra VI e XX
    secolo*. Jaca Book, Milano 2011. ISBN 978-88-16-30493-2

## 外部連結

  - [R Dom Luke Dysinger, *The Benedictine Family
    Tree*](http://www.valyermo.com/int-cong.html)
  - [本篤會：簡介](http://www.osb.org/gen/benedictines.html)
  - ["本篤令"](http://www.newadvent.org/cathen/02443a.htm) *天主教百科全書*
  - [*Confoederatio Benedictina Ordinis Sancti Benedicti*, the
    Benedictine Confederation of
    Congregations](https://web.archive.org/web/20080704171611/http://osb-international.info/)
  - [Links of the
    Congregations](http://www.osb.org/intl/confed/confed.html)

## 參見

  - [修道 (基督教)](../Page/修道_\(基督教\).md "wikilink")
  - [圣本笃](../Page/圣本笃.md "wikilink")
  - [雪夫特蘭修道院](../Page/雪夫特蘭修道院.md "wikilink")(Schäftlarn Abbey)

{{-}}

[Category:天主教修会](../Category/天主教修会.md "wikilink")
[本笃会](../Category/本笃会.md "wikilink")