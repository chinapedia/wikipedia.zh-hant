**拉贾斯坦邦**（[印地语](../Page/印地语.md "wikilink")：****，[拉丁字母转写](../Page/拉丁字母.md "wikilink")：）位于[印度西部](../Page/印度.md "wikilink")，与[巴基斯坦相接壤](../Page/巴基斯坦.md "wikilink")，是印度境内的一个邦。该邦官方语言是[拉贾斯坦语而除此之外包括](../Page/拉贾斯坦语.md "wikilink")[信德語](../Page/信德語.md "wikilink")、[古吉拉特語和](../Page/古吉拉特語.md "wikilink")[旁遮普語在內的其它語言亦通用於該邦](../Page/旁遮普語.md "wikilink")。現在的拉賈斯坦邦建於1956年，\[1\]西鄰[巴基斯坦](../Page/巴基斯坦.md "wikilink")。設有200席的立法議會。邦內河流交錯，西部是[塔爾沙漠](../Page/塔爾沙漠.md "wikilink")，南部是[安納瓦利山脈](../Page/安納瓦利山脈.md "wikilink")。当地最大民族是[拉其普特人](../Page/拉其普特人.md "wikilink")。

## 轄縣

下分33縣。

## 人口

2011年人口68,621,012，88.8%信仰[印度教](../Page/印度教.md "wikilink")，8.5%信仰[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")，1.4%信仰[錫克教](../Page/錫克教.md "wikilink")。\[2\]1991年的平均[識字率是](../Page/識字率.md "wikilink")38.55%，到2011年已提高至67.06%。\[3\]

## 農產品

農產品有[甘蔗](../Page/甘蔗.md "wikilink")、[油菜籽](../Page/油菜籽.md "wikilink")、[豆類及](../Page/豆類.md "wikilink")[棉花](../Page/棉花.md "wikilink")。印度每年生產10.0-12.5萬盧比噸（1
-
125萬噸）瓜爾豆膠（[關華豆膠](../Page/關華豆膠.md "wikilink")），使印度成為最大的生產國，約佔世界總產量的80％。

## 工業產品

有[食糖](../Page/食糖.md "wikilink")、[水泥](../Page/水泥.md "wikilink")、[玻璃及](../Page/玻璃.md "wikilink")[紡織品](../Page/紡織品.md "wikilink")。

## 礦產資源

有[鹽](../Page/鹽.md "wikilink")、[長石](../Page/長石.md "wikilink")、[石棉](../Page/石棉.md "wikilink")、[磷](../Page/磷.md "wikilink")、[銀](../Page/銀.md "wikilink")、[銅](../Page/銅.md "wikilink")、[石灰石及](../Page/石灰石.md "wikilink")[長石](../Page/長石.md "wikilink")。

## 旅游

拉贾斯坦邦旅游景点众多，是印度主要的旅游目的地之一。

<File:UmaidBhawan> Exterior
1.jpg|[焦特布尔](../Page/焦特布尔.md "wikilink")[乌迈德宫](../Page/乌迈德宫.md "wikilink")
<File:Udaipur> Lake
Palace.jpg|[乌代布尔](../Page/乌代布尔.md "wikilink")[湖之宫殿](../Page/湖之宫殿.md "wikilink")
<File:Hawa> Mahal on a stormy
afternoon.jpg|[斋浦尔](../Page/斋浦尔.md "wikilink")[风之宫](../Page/风之宫.md "wikilink")
<File:Showroom.png>|[斋浦尔](../Page/斋浦尔.md "wikilink")[Rambagh
Palace](../Page/Rambagh_Palace.md "wikilink")
<File:JagMandir.jpg>|[乌代布尔](../Page/乌代布尔.md "wikilink")[岛之宫殿](../Page/岛之宫殿.md "wikilink")
<File:Amber> Palace Jaipur
Pano.JPG|[斋浦尔](../Page/斋浦尔.md "wikilink")[琥珀堡](../Page/琥珀堡.md "wikilink")
<File:Jal> Mahal in Man Sagar
Lake.jpg|[斋浦尔](../Page/斋浦尔.md "wikilink")[水之宫殿](../Page/水之宫殿.md "wikilink")

## 參考文獻

## 外部連結

  - [邦政府網站](https://web.archive.org/web/20150610203750/http://rajasthan.gov.in/Pages/Home.aspx)
      - [About
        Rajasthan](https://web.archive.org/web/20150219070039/http://rajasthan.gov.in/StateProfile/GlimpseofRajasthan/Pages/AboutRajasthan.aspx)

[\*](../Category/拉贾斯坦邦.md "wikilink")
[邦](../Category/印度的邦和中央直辖区.md "wikilink")

1.
2.

3.