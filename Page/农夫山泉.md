[NongfuSpringWaterBottles.jpg](https://zh.wikipedia.org/wiki/File:NongfuSpringWaterBottles.jpg "fig:NongfuSpringWaterBottles.jpg")
[ChinaCRCC100.jpg](https://zh.wikipedia.org/wiki/File:ChinaCRCC100.jpg "fig:ChinaCRCC100.jpg")
**农夫山泉股份有限公司**（Nongfu Spring Co.,
Ltd.）是一家[中国大陆的](../Page/中国大陆.md "wikilink")[饮用水和](../Page/饮用水.md "wikilink")[饮料生产企业](../Page/饮料.md "wikilink")，成立于1996年9月26日，原名为“浙江千岛湖养生堂饮用水有限公司”，所在地为[杭州](../Page/杭州.md "wikilink")，是[养生堂有限公司旗下的控股公司](../Page/养生堂有限公司.md "wikilink")。公司拥有“**农夫山泉**”等中国著名[品牌](../Page/品牌.md "wikilink")。

## 产品

1.  其传统产品主要为瓶装[矿泉水](../Page/矿泉水.md "wikilink")，水源地最早设在中国一级水资源保护区[浙江](../Page/浙江.md "wikilink")[千岛湖](../Page/千岛湖.md "wikilink")，取深层湖水；后相继在[吉林](../Page/吉林.md "wikilink")[长白山矿泉水保护区](../Page/长白山.md "wikilink")、[湖北](../Page/湖北.md "wikilink")[丹江口水库](../Page/丹江口水库.md "wikilink")、[广东](../Page/广东.md "wikilink")[万绿湖](../Page/新丰江水库.md "wikilink")、[四川](../Page/四川.md "wikilink")[峨眉山设厂取水](../Page/峨眉山.md "wikilink")。
2.  2003年又推出以“**[农夫果园](../Page/农夫果园.md "wikilink")**”为品牌的混合[果汁](../Page/果汁.md "wikilink")[饮料](../Page/饮料.md "wikilink")；
3.  其它产品还有“尖叫”、“农夫茶（[东方树叶](../Page/东方树叶.md "wikilink")）”、“农夫汽水”、“[水溶C100](../Page/水溶C100.md "wikilink")”、“[茶π](../Page/茶π.md "wikilink")”、“[打奶茶](../Page/打奶茶.md "wikilink")”等饮料。\[1\]

## 争议事件

2013年上半年，因遭到合作交易拒绝，[21世纪经济报道开始对农夫山泉进行一系列负面批评](../Page/21世纪经济报道.md "wikilink")。2013年3月8日，21世纪发布消费者李女士投诉称所购买多瓶未开封农夫山泉380ml饮用天然水中出现黑色不明物。3月15日，农夫山泉通过其官方微博回应称获悉此事后，已将相关产品送检第三方权威检测机构，结果显示符合国家各项安全指标\[2\]。随后，一个名为“中国饮用水之殇”的网页出现在了网络上。网页罗列了中国近十年几乎所有的[水污染事件](../Page/水污染事故列表.md "wikilink")，包括：[大气水污染](../Page/大气污染.md "wikilink")、[酸雨](../Page/酸雨.md "wikilink")、[城市水污染](../Page/城市水污染.md "wikilink")、[地下水污染](../Page/地下水污染.md "wikilink")、[管道污染等等](../Page/管道污染.md "wikilink")。网页呈现了一派中国的大自然已经被破坏殆尽，中国处处都有水污染的图片，并以醒目的大标题指出“从大自然搬运过来的水你还敢喝吗”。而据农夫山泉介绍，他们从3月开始关注到有竞争对手在全国范围分发以“大自然搬运过来的水你还敢喝吗？”为主体的宣传单页\[3\]。3月25日，21世纪继续发布《农夫山泉丹江口水源地垃圾围城，水质堪忧》。随后，中国网记者采访了农夫山泉新闻发言人周力。周力表示新闻中的图片有不实成分，并称商业竞争不能以消费者对食品安全的恐慌为代价\[4\]。4月，农夫山泉被指责饮用水不符合国家标准。随后的3个月内，农夫山泉因继续拒绝21世纪经济报道合作，后者对农夫山泉的水源、质量标准等问题总共做了19篇负面报道。因此农夫山泉遭受巨大质疑，销售受到很大影响，据估算利润损失达数亿元\[5\]。2014年9月，21世纪报系总编沈颢、总经理陈东阳25日下午被上海市公安局带走\[6\]。2015年，21世纪网被责令停办，《理财周报》被吊销出版许可证，《21世纪经济报道》被责令整顿\[7\]。

## 参考资料

## 外部链接

  - [农夫山泉股份有限公司网站](http://www.nongfuspring.com)
  - [养生堂有限公司网站内关于农夫山泉的介绍](https://web.archive.org/web/20090627124039/http://www.yst.com.cn/water.asp)

[Category:1996年成立的公司](../Category/1996年成立的公司.md "wikilink")
[Category:中国民营企业](../Category/中国民营企业.md "wikilink")
[Category:中国饮料公司](../Category/中国饮料公司.md "wikilink")
[Category:杭州公司](../Category/杭州公司.md "wikilink")
[Category:瓶装水品牌](../Category/瓶装水品牌.md "wikilink")

1.
2.
3.
4.
5.
6.
7.