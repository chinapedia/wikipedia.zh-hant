**工布江达县**（）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[西藏自治区](../Page/西藏自治区.md "wikilink")[林芝市下属的一个](../Page/林芝市.md "wikilink")[县](../Page/县级行政区.md "wikilink")，原为西藏噶厦政府设置的[工布江达宗](../Page/工布江达宗.md "wikilink")。[藏语中意思是](../Page/藏语.md "wikilink")“凹地大谷口”。

## 行政区划

下辖：\[1\] 。

## 参考资料

[工布江达县](../Category/工布江达县.md "wikilink")
[Category:林芝县份](../Category/林芝县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.