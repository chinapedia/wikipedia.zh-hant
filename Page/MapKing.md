[MapKing_logo.jpg](https://zh.wikipedia.org/wiki/File:MapKing_logo.jpg "fig:MapKing_logo.jpg")
**MapKing**
是以亞太地區為主攻市場的[電子地圖和](../Page/電子地圖.md "wikilink")[智慧城市服務公司](../Page/智慧城市.md "wikilink")。以[導航軟體產品打出品牌](../Page/導航.md "wikilink")。MapKing創業期是專業的地理訊息系統
[(Geographic Information
System)服務公司](../Page/\(Geographic_Information_System\).md "wikilink")，第二階段是作為附有[全球衛星定位系統](../Page/全球衛星定位系統.md "wikilink")（Global
Positioning
System，簡稱GPS）的[電子手帳及](../Page/電子手帳.md "wikilink")[智能手機的隨機贈送軟件而廣為人知](../Page/智能手機.md "wikilink")。近年主力從事[智慧城市](../Page/智慧城市.md "wikilink")、[智能交通](../Page/智能交通.md "wikilink")、[車聯網及](../Page/車聯網.md "wikilink")[車隊管理業務](../Page/車隊管理.md "wikilink")，開始[人功智能及](../Page/人功智能.md "wikilink")[無人駕駛研發](../Page/無人駕駛.md "wikilink")。

MapKing（國圖顧問有限公司，[1](http://www.MapAsia.com)）亦是[Apple](../Page/蘋果公司.md "wikilink")、Google[谷哥地圖及百度的資料供應商](../Page/谷哥地圖.md "wikilink")，及為亞洲區域的電訊公司、地鐵公司、物流車隊、的士公司等中大型機構提供相關服務。參與2009至2011年中原、黃頁、有線、壹地圖香港網上地圖之爭。MapKing曾得到《Smartphone
and Pocket PC Magazine》2006最佳旅遊類別軟體提名。\[1\]MapKing也曾得到《香港無間斷城市》2008軟體獎項。

MapKing提供[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[台北](../Page/台北.md "wikilink")、[北京](../Page/北京.md "wikilink")、[廣州](../Page/廣州.md "wikilink")、[南京](../Page/南京.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[上海](../Page/上海.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[印度](../Page/印度.md "wikilink")、[東京](../Page/東京.md "wikilink")、[胡志明市](../Page/胡志明市.md "wikilink")、[迪拜](../Page/迪拜.md "wikilink")、[巴林](../Page/巴林.md "wikilink")、[汶萊](../Page/汶萊.md "wikilink")、[阿布達比](../Page/阿布達比.md "wikilink")、[首爾等地方之地圖](../Page/首爾.md "wikilink")，並推出可在[個人電腦](../Page/個人電腦.md "wikilink")、[Pocket
PC](../Page/Pocket_PC.md "wikilink")、[PND-Personal Naviagation
Device及](../Page/PND-Personal_Naviagation_Device.md "wikilink")[智能手機的平台上運行的不同版本](../Page/智能手機.md "wikilink")。

2010年，MapKing成為荷蘭智能交通研發中心HTAS項目的亞洲區域夥伴公司。在2011年成為Intel智能互聯汽車中國應用展示夥伴。2012年，MapKing銷售[澳洲](../Page/澳洲.md "wikilink")、[中東GCC國家](../Page/中東GCC國家.md "wikilink")、[歐洲等地圖](../Page/歐洲.md "wikilink")。2012年6月，MapKing推出
東南亞首部車聯網GPS導航機,開啟車聯網時代。\[2\]。2015 香港政府推介 MapKing
\[3\]。2017年開始第二代實時交通系統整合研發。

## 參見

  - [衛星地圖](../Page/衛星地圖.md "wikilink")
  - [電子地圖](../Page/電子地圖.md "wikilink")

2017

· 2017 香港 MapKing 與香港的士業議會 \[4\] 合作的士網約車業務

· 2017 香港 MapKing 客戶富衛保險 FWD 的智駕保获得最佳智慧香港大獎

· 2017 吉隆坡 MapKing 與吉隆坡5家的士公司合營網約車業務 \[5\]

2016

· 2016 香港 MapKing(實時公共交通警示推算應用)获得最佳智慧香港(公共資料應用) 銅獎

· 2016 新加坡 – 開始提供公共交通換乘數據服務給[Apple公司](../Page/蘋果公司.md "wikilink")

· 2016 香港 – 開始提供部分實時公共交通警示資訊給手機上的 Google公共交通導航

· 2016 在香港 (中國以外) 為客戶提供

`   `[`Google``   ``Business``
 ``View`](http://mapkinggbv.wix.com/googlebusinessview)` 谷歌商業攝映服務  `

2015

· 2015 為多家領先的物流服務公司、跨境大巴、小巴、專業公司提供高端車隊管理服務

· 2015 MapKing 香港政府推介 \[6\]

· 2015 MapKing 為百度地圖提供服務

2013

· 2013 MapKing 取得吉隆坡市政府智能交通網站平台項目合約，推出馬來西亞、雅加達、香港Google Play Market車聯網導航

· 2013 香港最佳流動應用程式獎獎項(流動資訊娛樂)-車聯網導航平台

· 2013 香港資訊及通訊科技獎特別嘉許(無線定位應用)獎

2012

· 騰訊微搏中國即時交通信息合作夥伴

`       `<http://www.techweb.com.cn/news/2012-12-05/1260700.shtml>

· 推出汽車互聯導航物流平臺MapKing Pro Connected及榮獲香港貿易發展局重點介紹

2011

· 在馬來西亞推出第一個帶有門牌查詢功能的導航系統

· 提供帶有3D大廈模型以及公交換轉系統的香港地圖給Google map用戶使用

· 華為手機捆綁MapKing UAE地圖在GCC地區出售。英特爾官式軟體夥伴

2010

· 英特爾車載互聯應用論壇（2010年12月）

· 新加坡陸路交通管理局即時交通論壇 (LTA) Live Traffic Show & Forum

· 獲荷蘭HTAS邀請參加國際短期車通訊技術實現方案StimoCC (Short Term Implementation of
Connected Car).參與北京,臺北,香港,新加坡巡迴展覽

· 馬來西亞Proton
25周年慶限量版車安裝MapKing導http://www.mapking.com/ad/News_10/index.html

· iPhone MapKing Go\! 系列 推出，Go\! Shanghai Expo成為世博會期間10大最高下載

·
香港地政總署『斜坡維修責任資訊系統SMRIS專案』持續技術支援http://www.slope.landsd.gov.hk/smris/index.html

· UPS Logistics夥伴 推出MapKing Pro. MapKing Android, MapKing Live, Ghost
Hunter遊戲

2009

· MapKing取得“Image of
Singapore”專案http://www.mapasia.com/AD/2010NEWS/Factsheet_iSingapore_001_3.pdf

· 為馬來西亞雪蘭莪及吉隆玻政府提供電子地圖

·
推出https://web.archive.org/web/20120604230051/http://maps.701panduan.com/
(新加坡報業集團專案www.sph.com.sg )

· 取得Smartone Vodafone澳門專案。成立迪拜分支機搆

2008

· MapKing Singapore取得SMRT的士專案.

· 第一家在東南亞區域（新加坡） 推出可支援即時交通導航應用測試 (2007/08)

· 取得Smartone Vodafone香港項目

·
吉隆玻政府智慧交通專案https://web.archive.org/web/20130129054638/http://www.itis.com.my/atis/journeyplanner.jsf

2007

· 推出MapKing Tracker V2.81,香港政府人員GPS跟蹤系統專案,推出MapKing Nokia Symbian.

· 成立MapKing (Singapore) Pte. Ltd./ MapKing (Malaysia) Sdn. Bhd.

2006

· 導航產品系列 美國最佳軟體提名2006

· HP Hw6965 in Asia Pacific 17 countries/regions.

· 推出www.China-infoMap.com.

2001

· 成立“廣州國圖軟體有限公司”

1996

· 公司成立–原名『國圖顧問有限公司City Country Consultancy Limited』

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [MapKing.com](http://www.MapKing.com)
  - [MapAsia.com](http://www.Mapasia.com)
  - [2007亞洲首個實時交通導航亮相新加坡](https://web.archive.org/web/20131029193538/http://www.directionsmag.com/pressreleases/asian146s-first-real-time-traffic-gps-navigation-mapking-singapore/111744)
  - [新加坡政府與MapKing合作項目](http://www.sla.gov.sg/htm/new/new2007/new0326.htm)
  - [2013香港最佳流動應用程式(流動資訊娛樂)銅獎](http://www.mapking.com/ad/2013news/Pressrelease/press_release_of_ICT_Award_TC.html)
  - [2013香港資訊及通訊科技獎特別嘉許(無線定位應用)獎](http://www.mapking.com/ad/2013news/Pressrelease/press_release_of_ICT_Award_TC.html)
  - [騰訊微搏與MapKing在實時交通信息領域合作](http://tech.qq.com/a/20121205/000085.htm)
  - [CNN比較主要由MapKing提供的香港 Google Map 和iPhone
    Map](http://edition.cnn.com/2012/12/13/tech/mobile/review-google-apple-maps/index.html)
  - [富衛保險智駕保](https://www.fwd.com.hk/drivamatics/tc)

[Category:中文軟體](../Category/中文軟體.md "wikilink")
[Category:付費軟體](../Category/付費軟體.md "wikilink") [Category:Pocket
PC軟體](../Category/Pocket_PC軟體.md "wikilink")
[Category:智能手機軟體](../Category/智能手機軟體.md "wikilink")
[Category:LBS](../Category/LBS.md "wikilink")
[Category:導航系統](../Category/導航系統.md "wikilink")
[Category:智能交通](../Category/智能交通.md "wikilink")
[Category:無線定位](../Category/無線定位.md "wikilink")
[Category:智能交通系統](../Category/智能交通系統.md "wikilink")
[Category:智能公共交通訊息警示服務](../Category/智能公共交通訊息警示服務.md "wikilink")
[Category:iPhone服務](../Category/iPhone服務.md "wikilink")
[Category:Google服務](../Category/Google服務.md "wikilink")
[Category:智慧城市服務](../Category/智慧城市服務.md "wikilink")
[Category:智慧型大廈服務](../Category/智慧型大廈服務.md "wikilink")

1.  [pocketpcmag.com：2006年獎項](http://www.pocketpcmag.com/awards/category_2006.asp?catid=43)

2.  [car1.hk：2012年度推介](http://www.car1.hk/news/southeast-asias-first-connected-gps-navigator-mapking-open-car-networking-era/)
3.  [香港政府DataOne](https://data.gov.hk/en/node/14/)
4.  [香港的士業議會](http://www.hk-tc.org)
5.  [PICKnGO](http://www.pickngo.my/)
6.  [香港政府DataOne](https://data.gov.hk/en/node/14/)