[A_View_of_Lianjiang_River_throught_Urban_of_Puning_from_Guangda_Avenue.jpg](https://zh.wikipedia.org/wiki/File:A_View_of_Lianjiang_River_throught_Urban_of_Puning_from_Guangda_Avenue.jpg "fig:A_View_of_Lianjiang_River_throught_Urban_of_Puning_from_Guangda_Avenue.jpg")市区的练江

</center>

\]\]
**练江**是[广东东部沿海的一条河流](../Page/广东.md "wikilink")，发源于[普宁市](../Page/普宁市.md "wikilink")，源头为五峰山寒妈径，流经[汕头市](../Page/汕头市.md "wikilink")[潮南区](../Page/潮南区.md "wikilink")，在汕头市[潮阳区](../Page/潮阳区.md "wikilink")[海门镇海门湾入海](../Page/海门镇.md "wikilink")。是潮阳区、潮南区、普宁市的母亲河。练江全长72[公里](../Page/公里.md "wikilink")，[流域面积达](../Page/流域面积.md "wikilink")1353[平方公里](../Page/平方公里.md "wikilink")，其中在汕头市[潮阳区长约](../Page/潮阳区.md "wikilink")40公里，流域面积约839平方公里

## 流经区域

练江上游在普宁市流经流沙市区、池尾街道、[大南山镇](../Page/大南山镇.md "wikilink")、[占陇镇](../Page/占陇镇.md "wikilink")、[南径镇](../Page/南径镇.md "wikilink")、[麒麟镇](../Page/麒麟镇.md "wikilink")、[下架山镇](../Page/下架山镇.md "wikilink")、[军埠镇](../Page/军埠镇.md "wikilink")、[燎原镇](../Page/燎原镇.md "wikilink")、[大坝镇](../Page/大坝镇.md "wikilink")、[云落镇](../Page/云落镇.md "wikilink")、[梅塘镇](../Page/梅塘镇.md "wikilink")，在潮南区流经[陈店镇](../Page/陈店镇.md "wikilink")、[司马浦镇](../Page/司马浦镇.md "wikilink")、[峡山街道](../Page/峡山街道.md "wikilink")、[井都镇](../Page/井都镇.md "wikilink")、[胪岗镇](../Page/胪岗镇.md "wikilink")，在潮阳区流经[贵屿镇](../Page/贵屿镇.md "wikilink")、[铜盂镇](../Page/铜盂镇.md "wikilink")、[和平镇](../Page/和平镇.md "wikilink")、[金浦街道](../Page/金浦街道.md "wikilink")、棉城、[海门镇](../Page/海门镇.md "wikilink")。练江流域人口达到200多万人，人口密度为广东省平均水平的5倍，环境承载压力非常大。

## 污染问题

由于缺乏水源以及生活污水、污染企业废水直排（练江沿岸有众多的再生纸制造企业，印染企业以及废旧电子电器拆解等企业），生活垃圾直倒，造成练江污染严重，整条江几乎成了“黑江”，[水质为劣五类](../Page/水质.md "wikilink")。是[粤东五大河流中污染指数最严重的河流](../Page/粤东.md "wikilink")。更加重中加重的是，人口大区潮阳区、潮南区，以及现中国人口第一县级市普宁市都没有[污水处理厂](../Page/污水处理厂.md "wikilink")。由于污染严重，水体不能饮用，也威胁着流域内居民的健康。特别是练江流域内以电子洋垃圾闻名全球的[贵屿镇要靠向外地买水来饮用](../Page/贵屿镇.md "wikilink")，而且群众[癌症发病率高](../Page/癌症.md "wikilink")。

## 治理历史

  - 1988年时[潮阳县就集资](../Page/潮阳县.md "wikilink")505.67万元来治理练江\[1\]。
  - 1997年，练江的治理被列入[广东省环保局的](../Page/广东省环保局.md "wikilink")“碧水工程计划”；
  - 2001年，广东省人民政府特别制定了《练江流域水质保护》：计划2005年前将入练江污染物控制不高于1998年的水平；使练江污染逐步减轻，至2010年前实现练江的水质达三类标准。
  - 2004年4月，汕头市政府制定《汕头市练江流域综合整治工程实施方案》：在6年的内投资19.43亿，计划建14座污水处理厂和14座生活垃圾集中填埋场，实现练江水质全面达标。\[2\]。

## 治理困难

由于练江分属普宁市与汕头市管辖，在统筹处理方面上存在问题。另外治理费用庞大，而且练江流域的环境治理宣传还需大力加强。

按照潮阳籍广东省[政协委员吴锡炎在](../Page/政协委员.md "wikilink")2009年相关提案中所指出的练江治理流于形式：**当地政府和有关部门对练江污染问题的整治还存在着在口头上和书面上重视、在行动上和落实上轻视，困难多、借口多的现象**。……企业经营者守法意识淡薄，特别是遵守环境影响评价法的意识更为淡薄，存有侥幸和法不责众的心理，偷排或擅自设置污染工序的情况屡禁不止\[3\]。

## 延伸阅读

  - [劣V类:练江污染别再漠视](https://web.archive.org/web/20101118232348/http://nf.nfdaily.cn/nanfangdaily/nfrb/200902150033.asp)

## 参考资料

<div class="references-small">

<references group="参"/>

</div>

[L](../Page/category:汕头河流.md "wikilink")

[L](../Category/潮阳.md "wikilink")

1.  2005年版《潮阳大事记》
2.
3.