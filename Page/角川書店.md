[Kadokawa_Shoten_(head_office).jpg](https://zh.wikipedia.org/wiki/File:Kadokawa_Shoten_\(head_office\).jpg "fig:Kadokawa_Shoten_(head_office).jpg")
**角川書店**（、）是[日本著名的](../Page/日本.md "wikilink")[出版社之一](../Page/出版社.md "wikilink")，總部位於[東京](../Page/東京.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")。現是[角川集團旗下的事業品牌](../Page/角川集團.md "wikilink")。

## 概要

  - 起初以出版[日本文學作品為其主要業務](../Page/日本文學.md "wikilink")。直到1970年代，角川春樹以[橫溝正史系列作向普羅大眾推廣](../Page/橫溝正史.md "wikilink")，而創造了該公司的巔峰時期；70年代後半，角川涉入電影界，讓市場掀起一股波瀾。
  - 該公司除了經營出版業務以外，亦經由母公司[角川控股集團跨足](../Page/角川控股集團.md "wikilink")[平面媒體](../Page/平面媒體.md "wikilink")、[電影](../Page/電影.md "wikilink")、[軟體等領域](../Page/軟體.md "wikilink")。

## 歷史

  - 1945年11月，日本學者[角川源義以其姓氏創立角川書店](../Page/角川源義.md "wikilink")。
  - 1954年4月，成立「株式會社角川書店」。
  - 1991年3月，分公司「[富士見書房](../Page/富士見書房.md "wikilink")」併入為「富士見事業部」。
  - 1992年，原社長[角川春樹之弟](../Page/角川春樹.md "wikilink")[角川歷彥辭職](../Page/角川歷彥.md "wikilink")（原副社長），脫離角川書店，另成立「[MediaWorks](../Page/MediaWorks.md "wikilink")」。
  - 1993年，角川春樹因違反《麻藥法》、《關稅法》被逮捕，角川歷彥回歸角川書店接任社長，MediaWorks則併入角川集團旗下。
  - 1998年11月，股票在[東京證券交易所市場第二部上市](../Page/東京證券交易所.md "wikilink")。
  - 1999年4月，在台灣設立「[台灣國際角川書店](../Page/台灣角川書店.md "wikilink")」；與[東芝合資成立](../Page/東芝.md "wikilink")\[1\]。
  - 2002年11月，收購，其公司[法人](../Page/法人.md "wikilink")、商號變更為。
  - 2003年4月\[2\]，由於[控股公司制度的開放](../Page/控股公司.md "wikilink")，組織變更為，公司法人改組、[子公司化](../Page/子公司.md "wikilink")。
  - 2005年10月，富士見事業部獨立出來，再度成為富士見書房。
  - 2007年1月\[3\]，從角川書店分割出、，公司法人再度改組。
  - 2011年1月，因應組織重整，「[角川映畫](../Page/角川映畫.md "wikilink")」併入角川書店而成為電影事業品牌。\[4\]
  - 2013年4月，從角川書店分割出來為。
  - 2013年10月，併入成為事業品牌，公司法人消滅。

## 原旗下關係企業

  - 原角川書店系：

<!-- end list -->

  - 角川学藝出版
  - chara-ani
  - 台灣角川
  - 台灣安利美特

<!-- end list -->

  - 原角川映畫系：

<!-- end list -->

  - 日本映画基金
  - Angel Cinema
  - 角川影城
  - Glovision
  - 日本映画衛星放送
  - 角川集團美國（香港）
  - 角川集团亞洲
      - 角川洲立集團
      - 香港角川
      - 天聞角川

## 參見

  - [角川集團](../Page/角川集團.md "wikilink")
  - [角川映畫](../Page/角川映畫.md "wikilink")
  - [台灣角川](../Page/台灣角川.md "wikilink")
  - [角川洲立](../Page/角川洲立.md "wikilink")
  - [角川集团亞洲](../Page/角川集团亞洲.md "wikilink")
  - [广州天闻角川动漫有限公司](../Page/广州天闻角川动漫有限公司.md "wikilink")
  - [安利美特](../Page/安利美特#海外.md "wikilink")

## 註釋

## 外部連結

  - [](http://shoten.kadokawa.co.jp/)

  -
[Category:1945年成立的公司](../Category/1945年成立的公司.md "wikilink")
[Category:角川集團](../Category/角川集團.md "wikilink")
[Category:角川書店](../Category/角川書店.md "wikilink")
[Category:日本出版社](../Category/日本出版社.md "wikilink")
[Category:日本已結業公司](../Category/日本已結業公司.md "wikilink")
[Category:2013年結業公司](../Category/2013年結業公司.md "wikilink")
[Category:1945年日本建立](../Category/1945年日本建立.md "wikilink")

1.  ，社名取自「**TOS**hiba＋**KA**dokawa＋domain」。
2.  2003年4月以前，公司沿革與[角川控股集團共通](../Page/角川控股集團.md "wikilink")。
3.  2007年1月以前，公司沿革與角川出版集團共通。
4.