**山田南平**（本名：西山 由夏\[1\]（舊姓：山田
由夏\[2\]）\[3\]）、[日本漫畫家](../Page/日本漫畫家.md "wikilink")。[神奈川縣出身](../Page/神奈川縣.md "wikilink")。[血型B型](../Page/血型.md "wikilink")。

1991年、「花とゆめプラネット増刊」春季號（[白泉社](../Page/白泉社.md "wikilink")）に掲載の《48ロマンス》でデビュー。
以後、「[花とゆめ](../Page/花とゆめ.md "wikilink")」を中心とした白泉社の雑誌で活躍。思春期の少年少女の成長を描いた『[オトナになる方法](../Page/オトナになる方法.md "wikilink")』などで人気を集めた。

《空色海岸》現在於「[別冊花與夢](../Page/別冊花與夢.md "wikilink")」連載中。

## 歷屆作品

  - 花與夢作品

<!-- end list -->

  - 「久美子與真吾」系列〈1997年 -
    2004年、[花與夢](../Page/花與夢.md "wikilink")、[白泉社](../Page/白泉社.md "wikilink")，共15冊〉
    1.  《130公分的帥哥》（全一冊）
    2.  《來玩躲避球》（全一冊）
    3.  《未來預想圖》（全一冊）
    4.  《STEP UP》（全一冊）
    5.  《巴黎之約》（全一冊）
    6.  《成長蜜方》（全10冊）
  - [紅茶王子](../Page/紅茶王子.md "wikilink")（1997年 - 2004年、花とゆめ）-全25冊
  - [三人吉三](../Page/三人吉三.md "wikilink")（2004年 - 2006年、花とゆめ）-全4冊
  - [紅茶王子的公主](../Page/紅茶王子的公主.md "wikilink")（2006年、・花とゆめ、共に白泉社）-全一冊
  - [空色海岸](../Page/空色海岸.md "wikilink") （2006年 - 2008、、白泉社）-全6冊
  - [橘子巧克力](../Page/橘子巧克力.md "wikilink")（2012年 - 2015年、別冊花とゆめ）- 全13冊
  - [櫻花紅茶王子](../Page/櫻花紅茶王子.md "wikilink") (2014年 - 連載中) - 1-2冊，未完

<!-- end list -->

  - 白泉社文庫

<!-- end list -->

  - オトナになる方法 -全8冊〔シリーズ全体で〕
  - 紅茶王子

<!-- end list -->

  - 畫集

<!-- end list -->

  - カードギャラリー 山田南平（1994年、白泉社）
  - 山田南平画集 Quality Seasons（1999年、白泉社）

## 脚注

## 外部連結

  - [Hot Water Jug](http://nanpei.net/)（山田南平的官方網站）
  - [Knowledge of NANPEI YAMADA](http://nanpei.info/)（ファンサイト）
  - [コミケイトインタビュー（No.52）](https://web.archive.org/web/20090622015124/http://www.hakusensha.co.jp/comicate/comi_no52/yamada/nanpei.html)
  - [反面教師・山田南平](https://web.archive.org/web/20070627114119/http://nampei.fc2web.com/)
    - 問題とみなされた言動をまとめたサイト。
  - [オトナになれない山田南平氏](https://web.archive.org/web/20070629231810/http://www.interline.or.jp/~k-z31/ankoku/zatu/zatu17.html)-
    同じく問題とみなされた言動をまとめたサイト。

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")

1.  《來玩躲避球》p.185
2.  《130公分的帥哥》p.22
3.  紅茶王子1冊