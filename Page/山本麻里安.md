**山本麻里安**（、）是[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")，[東京都出身](../Page/東京都.md "wikilink")，從屬[81
Produce事務所](../Page/81_Produce.md "wikilink")。[血型A型](../Page/血型.md "wikilink")。[東京女子大學畢業](../Page/東京女子大學.md "wikilink")。

## 經歷

  - 曾和[井上喜久子共同主持電台節目](../Page/井上喜久子.md "wikilink")《かきくけ喜久子のさしすせSonata》，兩人互称姐妹。當時山本麻里安17歳，井上喜久子也稱自己是17歳，從此“井上喜久子17歳”成了井上本人的介紹詞。

<!-- end list -->

  - 成为[声优的契机是中学时看到的动画](../Page/声优.md "wikilink")[美少女戰士](../Page/美少女戰士.md "wikilink")。当时的山本正在为学习和朋友之间的关系烦恼，看到动画以后觉得“和自己年龄相同，有着同样苦恼的少女，却能变身勇敢的战斗……我也要努力。”于是她领会到动画可以拯救人心。\[1\]
  - 2008年8月31日，退出長年所屬的[I'm
    Enterprise](../Page/I'm_Enterprise.md "wikilink")，經自由時期，同年10月1日起進入[81
    Produce](../Page/81_Produce.md "wikilink")。
  - 是一名[腐女子](../Page/腐女子.md "wikilink")，家里的书架上堆满了[BL读物](../Page/BL.md "wikilink")。
  - 2012年12月，在其粉絲俱樂部宣布婚訊，[丈夫是](../Page/丈夫.md "wikilink")[上班族](../Page/上班族.md "wikilink")\[2\]。

## 趣聞

由神戶守監督的動畫作品中，只要山本麻里安（以下簡稱山本）擔任配音之角色全部都會死亡，有這樣一個不成文的「法則」存在。在妖精的旋律動畫化之前，神戶監督的作品中同時也有山本擔任配音演出，其符合條件者僅有「救難小英雄」的早乙女亞希（さおとめ
あき）這個角色，當時還並沒有「法則」這樣的稱呼。自從妖精的旋律在AT-X開始播映之後，凡是山本在劇中所飾演的角色，最後的結局全部都是死亡（見下段敘述）。「**神戶守監督作品中，凡是山本擔任演出的角色最後必定都會死亡**」的法則，從此便很自然地誕生。

山本麻里安在妖精的旋律中共擔任**四位角色**的配音：藏間的秘書-如月、耕太的妹妹-香苗、研究員-齊藤、繪畫少女-高田愛子，如此<span style="text-decoration: underline;">一人飾演多角</span>的案例，在日本動畫界中是屬於較罕見的情形。而山本所飾演的這四位角色，最後全部都死亡：如月被露西斷頭殺死、香苗被露西断腰殺死、齊藤被真理子截體殺死、繪畫少女被狙擊中彈而不治死亡。由於這個緣故，上述之「法則」便自然而然地產生。
關於山本最後在OVA版所演出的繪畫少女，其實在漫畫原著的最終卷中有活下來，不過由於動畫版的故事劇情並沒有進展到那裡，所以繪畫少女被狙擊中彈後，最後的設定是全力搶救但仍然回天乏術。

## 主要出演作品

**粗體字**為主要角色

### [電視動畫](../Page/電視動畫.md "wikilink")

**1998年**

  - [男女蹺蹺板](../Page/男女蹺蹺板.md "wikilink")（宮澤花野、女孩子A、侍應生、女學生A、女學生C）

**1999年**

  - [六翼天使之聲](../Page/六翼天使之聲.md "wikilink")（）

**2000年**

  - [六門天外](../Page/六門天外.md "wikilink")（）
  - [袖珍女侍小梅](../Page/袖珍女侍小梅.md "wikilink")（小梅）

**2001年**

  - [魔法少女猫](../Page/魔法少女猫.md "wikilink")（）
  - [チャンス〜トライアングルセッション〜](../Page/チャンス〜トライアングルセッション〜.md "wikilink")（）
  - [破邪巨星G彈劾凰](../Page/破邪巨星G彈劾凰.md "wikilink")（）

**2002年**

  - [超齡細公主](../Page/超齡細公主.md "wikilink")（**優希**）

**2003年**

  - [機甲露寶](../Page/機甲露寶.md "wikilink")（早乙女亞季）
  - [神奇寶貝動畫版](../Page/神奇寶貝動畫版.md "wikilink")（）
  - [闇與帽子與書的旅人](../Page/闇與帽子與書的旅人.md "wikilink")（）

**2004年**

  - [流星戰隊Musumet](../Page/流星戰隊Musumet.md "wikilink")（中川忍）
  - [校園迷糊大王](../Page/校園迷糊大王.md "wikilink")（、電視記者）
  - [Get Ride\! アムドライバー](../Page/Get_Ride!_アムドライバー.md "wikilink")（）
  - [妖精的旋律](../Page/妖精的旋律.md "wikilink")（香苗、如月、、齋藤、繪畫少女（高田愛子））
  - [Keroro軍曹](../Page/Keroro軍曹_\(電視動畫\).md "wikilink")（女僕長）※第49話

**2005年**

  - [奇幻魔法Melody](../Page/奇幻魔法Melody.md "wikilink")（）

**2006年**

  - [校園迷糊大王 二學期](../Page/校園迷糊大王.md "wikilink")（隣子（礪波順子）、冴子、）
  - [声優白書](../Page/声優白書.md "wikilink")（**大原天音**）
  - [驅魔少年](../Page/驅魔少年.md "wikilink")（）
  - [パピヨンローゼ New Season](../Page/パピヨンローゼ_New_Season.md "wikilink")（）

**2007年**

  - [魔女獵人](../Page/魔女獵人.md "wikilink")（）
  - [桃華月憚](../Page/桃華月憚.md "wikilink")（）

**2008年**

  - [ゴルゴ13](../Page/ゴルゴ13.md "wikilink")（女性秘書）
  - [稻草男](../Page/稻草男.md "wikilink")（）
  - [波菲的漫長旅程](../Page/波菲的漫長旅程.md "wikilink")（蘇菲亞）

**2009年**

  - [毒蛇信條](../Page/VIPER'S_CREED.md "wikilink")（）
  - [戀愛班長](../Page/戀愛班長.md "wikilink")（戶田菜花）
  - [秀逗魔導士 EVOLUTION-R](../Page/秀逗魔導士_\(動畫\).md "wikilink")（塔佛拉西亞王國國民F）
  - [元素高手](../Page/元素獵人.md "wikilink")（娜塔夏）

**2010年**

  - 戀愛班長 Second collection（戶田菜花）

**2011年**

  - [戰國鬼才傳](../Page/戰國鬼才傳.md "wikilink")（千）

**2012年**

  - [黑魔女學園](../Page/黑魔女學園.md "wikilink")（****）
  - [人類衰退之後](../Page/人類衰退之後.md "wikilink")（）

### [OVA](../Page/OVA.md "wikilink")

**1999年**

  - [菜菜子解體診書](../Page/菜菜子解體診書.md "wikilink")（**七千草菜菜子**）

**2001年**

  - [秋之回憶系列](../Page/秋之回憶.md "wikilink")（檜月彩花）
      - 秋之回憶 不停之雨〜唯笑篇〜
      - 秋之回憶 假面之心〜詩音篇〜
      - 秋之回憶 黃金之海〜美奈裳篇〜

**2003年**

  - [守護之心](../Page/守護之心.md "wikilink")（紅葉綾）

**2005年**

  - [永遠のアセリア](../Page/永遠のアセリア.md "wikilink") 『求め』の声（レスティーナ・ダイ・ラキオス）
  - [守護之心 Power Up\!](../Page/守護之心.md "wikilink")（紅葉綾）

**2006年**

  - [今天的五年二班](../Page/今天的五年二班.md "wikilink")（平川夏美）
  - [鬼公子炎魔](../Page/鬼公子炎魔.md "wikilink")（サッちゃん）
  - [METAL GEAR SOLID 2 BANDE
    DESSINEE](../Page/潛龍諜影.md "wikilink")（エマ・エメリッヒ・ダンジガー）

### 遊戲

  - [西遊記](../Page/西遊記_\(遊戲\).md "wikilink")（朱涼鈴）
  - [少女義經傳](../Page/少女義經傳.md "wikilink")（源九羅香）
  - [新世紀福音戰士 碇真嗣育成計畫](../Page/新世紀福音戰士_碇真嗣育成計畫.md "wikilink")（大井サツキ）
  - [名偵探福音戰士](../Page/名偵探福音戰士.md "wikilink")（大井サツキ）
  - [潛龍諜影](../Page/潛龍諜影.md "wikilink")（艾瑪・艾美里希）
  - [秋之回憶](../Page/秋之回憶.md "wikilink")（**檜月彩花**）
  - [超魔法大戰](../Page/超魔法大戰.md "wikilink")（セシリス）
  - [美少女夢工廠4](../Page/美少女夢工廠4.md "wikilink")（マリー）
  - [聖靈之心](../Page/聖靈之心.md "wikilink")（菲歐娜‧梅菲德、春日鼓音）
  - [L的季節](../Page/L的季節.md "wikilink")（リ・サ）
  - [Ys I & II: Eternal
    Story](../Page/Ys_I_&_II:_Eternal_Story.md "wikilink")（莉莉亞）
  - [真愛物語3](../Page/真愛物語3.md "wikilink")（本條笑）
  - [HEXAMOON GUARDIANS](../Page/HEXAMOON_GUARDIANS.md "wikilink")（セレン）

### 同人作品

**2009年**

  - [東方奇闘劇3](../Page/東方奇闘劇3.md "wikilink")（**雲居一輪**）

## 参考资料

<div class="references-small">

<references />

</div>

## 外部連結

  - [官方後援會網站「まりあ大牧場」](http://bewe.sc/maria/)
  - [個人twitter](https://twitter.com/mariamilkymilk)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:東京女子大學校友](../Category/東京女子大學校友.md "wikilink")

1.  [采访山本麻里安](http://sankei.jp.msn.com/entertainments/game/080126/gam0801261236000-n1.htm)
    产经新闻
2.