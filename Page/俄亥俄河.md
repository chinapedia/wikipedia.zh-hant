**俄亥俄河**（）是[美國](../Page/美國.md "wikilink")[東部的一條](../Page/美國東部.md "wikilink")[河流](../Page/河流.md "wikilink")，是[密西西比河最東的支流](../Page/密西西比河.md "wikilink")。發源於[匹茲堡](../Page/匹茲堡.md "wikilink")，初向西北流，在[賓夕法尼亞州](../Page/賓夕法尼亞州.md "wikilink")、[西維吉尼亞州和](../Page/西維吉尼亞州.md "wikilink")[俄亥俄州的邊界以下轉向西南方](../Page/俄亥俄州.md "wikilink")，並流經[肯塔基州北界](../Page/肯塔基州.md "wikilink")（同時是[印地安那州和](../Page/印地安那州.md "wikilink")[伊利諾伊州南界](../Page/伊利諾伊州.md "wikilink")），在肯塔基州與[田納西河匯合](../Page/田納西河.md "wikilink")，最後在[開羅與密西西比河匯合](../Page/開羅_\(伊利諾伊州\).md "wikilink")。全長1,579公里，流域面積490,603平方公里。

歷史上是[西北領地的南界](../Page/西北領地.md "wikilink")，也是[美國南北的分界](../Page/美國內戰.md "wikilink")。

## 參考資料

[Category:密西西比河支流](../Category/密西西比河支流.md "wikilink")
[\*](../Category/俄亥俄河.md "wikilink")
[Category:賓夕法尼亞州河流](../Category/賓夕法尼亞州河流.md "wikilink")
[Category:西維吉尼亞州河流](../Category/西維吉尼亞州河流.md "wikilink")
[Category:俄亥俄州河流](../Category/俄亥俄州河流.md "wikilink")
[Category:肯塔基州河流](../Category/肯塔基州河流.md "wikilink")
[Category:印第安納州河流](../Category/印第安納州河流.md "wikilink")
[Category:伊利諾州河流](../Category/伊利諾州河流.md "wikilink")
[Category:西維吉尼亞州邊界](../Category/西維吉尼亞州邊界.md "wikilink")
[Category:俄亥俄州邊界](../Category/俄亥俄州邊界.md "wikilink")
[Category:肯塔基州邊界](../Category/肯塔基州邊界.md "wikilink")
[Category:印第安納州邊界](../Category/印第安納州邊界.md "wikilink")
[Category:伊利諾州邊界](../Category/伊利諾州邊界.md "wikilink")