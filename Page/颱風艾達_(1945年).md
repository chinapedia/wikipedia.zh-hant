**枕崎颱風**（、）於1945年9月17日14時在[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[枕崎市附近登陸](../Page/枕崎市.md "wikilink")。與[伊勢灣颱風](../Page/伊勢灣颱風.md "wikilink")、[室戶颱風并列為昭和](../Page/室戶颱風.md "wikilink")[三大台風](../Page/日本三大一覽.md "wikilink")。登陸時中心氣壓為916.3[帕斯卡](../Page/帕斯卡.md "wikilink")。造成2,473人死亡，1,283人失蹤、2,452人受傷。由于當時日本剛剛戰敗，防災體制不健全，在各地都造成很大災害。特別是在[廣島縣](../Page/廣島縣.md "wikilink")，共計有超過2000人因此死亡或失蹤，給剛剛遭受原子彈轟炸的廣島雪上加霜。的小說《[空白的天氣圖](../Page/空白的天氣圖.md "wikilink")（）》即以此為題材。這颱風雖然單在[廣島市內就造成了](../Page/廣島市.md "wikilink")1157人死亡，但認為這颱風沖洗走了市內的放射性物質，而使廣島市重新適合居住。

## 外部連結

  - [災害をもたらした気象事例（枕崎台風）](http://www.data.jma.go.jp/obd/stats/data/bosai/report/1945/19450917/19450917.html)
    - [気象庁 ホームページ](http://www.jma.go.jp/)
  - [お天気豆知識
    終戦と台風](http://www.bioweather.net/column/weather/contents/mame068.htm)
    - [バイオウェザーサービス](http://www.bioweather.net/)
  - [京都大学生協「教職員情報」 /
    花谷会館（生協本部）](https://web.archive.org/web/20120622035522/http://www.s-coop.net/faculty/0708/7.htm)
  - [広島平和記念資料館企画展「原子爆弾ナリト認ム」](http://www.pcf.city.hiroshima.jp/virtual/VirtualMuseum_j/exhibit/exh0307/exh03075.html)

[Category:1945年日本](../Category/1945年日本.md "wikilink")
[Category:一级热带气旋](../Category/一级热带气旋.md "wikilink")
[Category:影響日本的熱帶氣旋](../Category/影響日本的熱帶氣旋.md "wikilink")