[Pippit-closer.jpg](https://zh.wikipedia.org/wiki/File:Pippit-closer.jpg "fig:Pippit-closer.jpg")
**鹡鸰科**（[学名](../Page/学名.md "wikilink")：）是[鸟纲](../Page/鸟纲.md "wikilink")[雀形目的一个](../Page/雀形目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，又名**雃**(qian1)。全球共5属62种，在[中国有](../Page/中国.md "wikilink")3属20种分布。

本科鸟类身体纤细，尾较长，在地面上筑巢，以[昆虫为食](../Page/昆虫.md "wikilink")。

## 分类

  - [鹨属](../Page/鹨属.md "wikilink") *Anthus*：大约40种；
  - [金鹨属](../Page/金鹨属.md "wikilink") *Tmetothylacus*
      - [金鹨](../Page/金鹨.md "wikilink") '' Tmetothylacus tenellus''
  - [鹡鸰属](../Page/鹡鸰属.md "wikilink") *Motacilla*
      - [白鹡鸰](../Page/白鹡鸰.md "wikilink") *Motacilla alba*
      - [日本鹡鸰](../Page/日本鹡鸰.md "wikilink") *Motacilla grandis*
      - [印度鹡鸰](../Page/印度鹡鸰.md "wikilink") *Motacilla madaraspatensis*
      - [湄公鹡鸰](../Page/湄公鹡鸰.md "wikilink") *Motacilla samveasnae*
      - [非洲斑鹡鸰](../Page/非洲斑鹡鸰.md "wikilink") *Motacilla aguimp*
      - [黄头鹡鸰](../Page/黄头鹡鸰.md "wikilink") *Motacilla citreola*
      - [黄鹡鸰](../Page/黄鹡鸰.md "wikilink") *Motacilla flava*
      - [灰鹡鸰](../Page/灰鹡鸰.md "wikilink") *Motacilla cinerea*
      - [海角鹡鸰](../Page/海角鹡鸰.md "wikilink") *Motacilla capensis*
      - [马岛鹡鸰](../Page/马岛鹡鸰.md "wikilink") *Motacilla flaviventris*
      - [非洲山鹡鸰](../Page/非洲山鹡鸰.md "wikilink") *Motacilla clara*
  - [山鹡鸰属](../Page/山鹡鸰属.md "wikilink") *Dendronanthus*
      - [山鹡鸰](../Page/山鹡鸰.md "wikilink") '' Dendronanthus indicus''
  - [长爪鹡鸰属](../Page/长爪鹡鸰属.md "wikilink") *Macronyx*
      - [橙喉长爪鹡鸰](../Page/橙喉长爪鹡鸰.md "wikilink") *Macronyx capensis*
      - [黄喉长爪鹡鸰](../Page/黄喉长爪鹡鸰.md "wikilink") *Macronyx croceus*
      - [福氏长爪鹡鸰](../Page/福氏长爪鹡鸰.md "wikilink") *Macronyx fuellebornii*
      - [沙氏长爪鹡鸰](../Page/沙氏长爪鹡鸰.md "wikilink") *Macronyx sharpei*
      - [黄颈长爪鹡鸰](../Page/黄颈长爪鹡鸰.md "wikilink") *Macronyx flavicollis*
      - [橘红长爪鹡鸰](../Page/橘红长爪鹡鸰.md "wikilink") *Macronyx aurantiigula*
      - [红胸长爪鹡鸰](../Page/红胸长爪鹡鸰.md "wikilink") *Macronyx ameliae*
      - [格氏长爪鹡鸰](../Page/格氏长爪鹡鸰.md "wikilink") *Macronyx grimwoodi*

## 外部链接

  - [鹡鸰科视频](http://ibc.hbw.com/ibc/phtml/familia.phtml?idFamilia=123)

[鹡鸰科](../Category/鹡鸰科.md "wikilink")
[Category:雀形目](../Category/雀形目.md "wikilink")