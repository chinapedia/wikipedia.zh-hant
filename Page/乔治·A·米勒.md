**乔治·阿米蒂奇·米勒**（****，）是[普林斯顿大学的](../Page/普林斯顿大学.md "wikilink")[心理学教授](../Page/心理学.md "wikilink")。曾经担任[洛克斐勒大学](../Page/洛克斐勒大学.md "wikilink")、[麻省理工学院心理学教授以及](../Page/麻省理工学院.md "wikilink")[哈佛大学心理学系主任](../Page/哈佛大学.md "wikilink")。他也是[牛津大学的](../Page/牛津大学.md "wikilink")[傅爾布萊特計畫研究伙伴](../Page/傅爾布萊特計畫.md "wikilink")，以及美国心理学会会长。他最著名的著作是《[神奇的数字：7±2——我们信息加工能力的局限](../Page/神奇的数字：7±2.md "wikilink")》，1956年发表于《心理学评论》（*The
Psychological Review*）。

## 生平

1920年出生在美国[西弗吉尼亚州的](../Page/西弗吉尼亚州.md "wikilink")[查尔斯顿](../Page/查尔斯顿_\(西弗吉尼亚州\).md "wikilink")，1960年，米勒和心理学家[杰罗姆·布鲁纳一起创立了哈佛大学认知研究中心](../Page/杰罗姆·布鲁纳.md "wikilink")。同年他出版了与加兰特（Eugene
Galanter）、普里布拉姆（Karl H. Pribram）合著的《计划和行为的结构》（Plans and the Structure of
Behaviour），概述了他们的[认知心理学的概念](../Page/认知心理学.md "wikilink")。

在[语言学领域](../Page/语言学.md "wikilink")，米勒以负责开发[英语的语义网络](../Page/英语.md "wikilink")[WordNet著称](../Page/WordNet.md "wikilink")，该项目开始于1985年，主要由于政府机构对[机器翻译的兴趣](../Page/机器翻译.md "wikilink")，获得了300万美元经费。他花费后半生来建立、扩展这一数据库。他还基于WordNet开发了许多商业应用软件，例如和[布朗大学的许多教授和研究生](../Page/布朗大学.md "wikilink")，开发了早期网络搜索和交易工具Simpli，[Google的广告技术](../Page/Google.md "wikilink")[AdSense就直接源自于WordNet和Simpli](../Page/AdSense.md "wikilink").\[1\]。

1991年，米勒获得[美国国家科学奖章](../Page/美国国家科学奖章.md "wikilink")。

他还以提出而著称：要想理解别人在说什么，首先你要假定他说的是真的（不要先入为主的判定是假的），并尝试的去想象，如果是真的话，将会怎么样.

## 神奇的数字：7±2

1956年，米勒最早对短时记忆能力进行了定量研究——“[神奇的数字：7±2](../Page/神奇的数字：7±2.md "wikilink")”\[2\]。他注意到年轻人的记忆广度大约为7个单位（阿拉伯数字、字母、单词或其他单位），称为组块。后来的研究显示广度与组块的类别有关，例如阿拉伯数字为7个，字母为6个，单词为5个，而较长词汇的记忆广度低于较短词汇的记忆广度。通常，口头内容的记忆广度（阿拉伯数字、字母、单词等）强烈取决于朗读这些内容的时间\[3\]。其他一些因素也影响到人类标准广度，因此难以将[短时记忆或](../Page/短时记忆.md "wikilink")[工作记忆的能力限制在许多组块内](../Page/工作记忆.md "wikilink")。但是，Cowan
(2001)\[4\]认为年轻人的[工作记忆能力为](../Page/工作记忆.md "wikilink")4个组块（儿童和老人较低）。



## 参考文献

[Category:美国心理学家](../Category/美国心理学家.md "wikilink")
[Category:认知心理学](../Category/认知心理学.md "wikilink")
[Category:认知科学家](../Category/认知科学家.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:傅爾布萊特學者](../Category/傅爾布萊特學者.md "wikilink")
[Category:普林斯顿大学教授](../Category/普林斯顿大学教授.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:阿拉巴馬大學校友](../Category/阿拉巴馬大學校友.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:行为科学高等研究中心学者](../Category/行为科学高等研究中心学者.md "wikilink")

1.
2.  Miller, G. A. (1956). [神奇的数字：7±2——我们信息加工能力的局限（The magical number
    seven, plus or minus two: Some limits on our capacity for processing
    information）](http://www.well.com/user/smalin/miller.html)心理学评论，63,
    81-97
3.  Hulme, C., Roodenrys, S., Brown, G., & Mercer, R.
    (1995).[长时记忆在记忆广度中的任务](../Page/长时记忆.md "wikilink")。英国心理学杂志，86,
    527-536.
4.  Cowan, N. (2001).神奇的数字4 in 短时记忆：A reconsideration of mental storage
    capacity. Behavioral and Brain Sciences, 24, 87-185