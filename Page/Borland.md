**Borland**软件公司（有译**宝蓝**公司或**寶蘭**公司，**Borland** Software
Corporation，[NASDAQ](../Page/NASDAQ.md "wikilink")：[BORL](http://quotes.nasdaq.com/asp/SummaryQuote.asp?symbol=BORL&selected=BORL)），是一家总部位于[美国加利福尼亚州的软件公司](../Page/美国.md "wikilink")，以提供[软件开发生命周期所需的各种工具](../Page/软件.md "wikilink")，以及已经变成今天的[Delphi的](../Page/Delphi.md "wikilink")[Turbo
Pascal而闻名于世](../Page/Turbo_Pascal.md "wikilink")。2009年5月6日，Micro
Focus宣布以7500万美元收购Borland。

## 1980年代：创立

Borland成立于1983年，创立者是[Philippe
Kahn](../Page/Philippe_Kahn.md "wikilink")，他领导Borland开发了一系列受人尊重的软件开发工具产品。公司的第一个产品是[Turbo
Pascal](../Page/Turbo_Pascal.md "wikilink")，该软件最初由[安德斯·海尔斯伯格开发](../Page/安德斯·海尔斯伯格.md "wikilink")。1984年Borland发布了[SideKick](../Page/SideKick.md "wikilink")，这是一个结合了日程表、记事本和计算器功能的工具软件，其显著的特点是[記憶體驻留](../Page/記憶體驻留.md "wikilink")，从而可以打断正在正常执行的前台程序。1987年Borland收购了[Ansa-Software公司及其](../Page/Ansa-Software.md "wikilink")[数据库管理系统](../Page/数据库.md "wikilink")[Paradox](../Page/Borland_Paradox.md "wikilink")（第2.0版）。1989年Borland发布了[Quattro
Pro电子表格软件](../Page/Quattro_Pro.md "wikilink")，其强大的绘图功能在当时非常引人注目。

Borland发布Turbo Pascal之前，微软公司是编程语言领域的市场领先者；但在Turbo
Pascal发布之后，由于其[集成开发环境（IDE）要比微软的基于命令行界面的](../Page/集成开发环境.md "wikilink")[编译器以及](../Page/编译器.md "wikilink")[解释器更加方便好用](../Page/解释器.md "wikilink")，迫使微软不久将开发的重点转到了[操作系统和](../Page/操作系统.md "wikilink")[应用软件方面](../Page/应用软件.md "wikilink")。

## 1990年代：起伏

1991年9月Borland为[dBase和Interbase数据库软件买下](../Page/dBase.md "wikilink")[Ashton-Tate](../Page/Ashton-Tate.md "wikilink")。但是价钱很贵，使得Borland财务紧张，周转不灵。[Microsoft随即出版](../Page/Microsoft.md "wikilink")[Microsoft
Access](../Page/Microsoft_Access.md "wikilink")，并且买下和dBase相似的[FoxPro](../Page/FoxPro.md "wikilink")，压低数据库软件价格。Borland失去更多利润。

在1990年代早期，人们认为Borland公司的[C++工具要优于当时的处于市场追赶地位的Microsoft](../Page/C++.md "wikilink")。另外，Borland的[Paradox及其](../Page/Paradox.md "wikilink")[ObjectPAL编程语言与Microsoft的软件尤其是Access出现了激烈的竞争](../Page/ObjectPAL.md "wikilink")。

到了1990年代中期，Borland开始从软件工具市场的巅峰位置滑落，人们认为这是来自于Microsoft的竞争带来的后果。

另外一些人认为[Philippe
Kahn将公司的资源投入到了太多的项目上](../Page/Philippe_Kahn.md "wikilink")，以与Microsoft在多个层面展开竞争。在Kahn的任期内，Borland引进了许多当时还很模糊、市场还没有接受的概念，持续关注这些技术的耗尽了Borland的资源，而其它公司则抓住了将这些新概念变成资本的契机。

但是仍然有其他人相信是市场环境的变化使Borland从霸主的位置上跌落下来。Phillipe
Kahn已经把注意力放在了计算机程序员市场上。在80年代，没有几个公司能意识到个人电脑市场的增长，所以很多技术人员都有购买他们认为他们需要的任何软件的自由。Borland在具有高度技术偏好人员市场上的确做的不错。但是到90年代中期，很多公司都开始质疑在这波不受他们控制的PC软件购买热潮中，他们投资了这么多，究竟得到了什么。公司的决策层开始询问这些技术人员很难回答的问题，所以一些共同的标准就开始制定了。这要求软件厂商提供一种新的市场和支持资源，但此时在Kahn领导下的Borland没有迅速的意识到市场已经改变了。一些竞争对手如微软等却做的很棒，他们看到了这种变化并且开始向PC厂商提供他们正在寻找的信息。

Kahn的个性的确也有点太让人琢磨不透，这让公司的很多执行人员很难忍受。Kahn在1994年离开了Borland，他说他不想成为公司的绊脚石。

[Delphi
1快速应用开发](../Page/Delphi_programming_language.md "wikilink")（[RAD](../Page/RAD.md "wikilink")）环境在[Anders
Hejlsberg的领导下于](../Page/Anders_Hejlsberg.md "wikilink")1995年发布。

## Inprise时期

1997年11月Borland收购了一家专注于[CORBA实现的中间件公司](../Page/CORBA.md "wikilink")[Visigenic](../Page/Visigenic.md "wikilink")。

1998年4月29日，Borland重新将它的业务重心集中于企业应用软件开发，并且将名字变更为[Inprise公司](../Page/Inprise.md "wikilink")，这个名字来自于他们的口号*企业集成*。其理念是将Borland的工具Delphi、C++
Builder以及JBuilder与企业软件环境集成在一起，这些企业软件包括Visigenic用于C与Java的CORBA实现[Visibroker以及新出现的产品](../Page/Visibroker.md "wikilink")[Application
Server](../Page/Application_Server.md "wikilink")。

在使用Inprise名字的时期以及之前许多年间，Borland都遭受了严峻的财政损失以及公共形象受损严重。当它的名字更改到Inprise的时候，许多人认为Borland公司已经破产。

1996年10月，Paradox与Quattro
Pro卖给了[Corel公司](../Page/Corel.md "wikilink")，Corel另外还购买了[WordPerfect完成了对于以前名为Borland](../Page/WordPerfect.md "wikilink")
Office的软件包的收购。现在WordPerfect、Paradox以及Quattro Pro在作为Corel Office进行销售。

dBase在1999年被卖掉，当时Inprise决定要集中于软件开发工具业务。

1999年，在Borland名号的危急时刻，[Dale L
Fuller取代](../Page/Dale_L_Fuller.md "wikilink")[Del
Yokam成为新的CEO](../Page/Del_Yokam.md "wikilink")。当时Fuller的职位是“过渡时期的总裁与CEO”，几年之后去掉了“过渡”这个词。

在2000年2月份曾经宣布Inprise要与Corel合并，集中精力生产基于[Linux的产品](../Page/Linux.md "wikilink")，但是后来由于Corel的股票价格下滑这项计划没有进行下去。

2000年7月[InterBase
6.0成为了一项](../Page/InterBase.md "wikilink")[开放源代码产品](../Page/开放源代码.md "wikilink")。

## Borland的名字和声誉重振

2001年1月，Borland重新从Inprise改名为Borland（Borland Software Corporation）。

今天，在Borland的名字和由总裁和CEO[Dale L.
Fuller为首的新管理层的领导下](../Page/Dale_L._Fuller.md "wikilink")，一个规模稍小一点但赢利的Borland持续在做Delphi的开发工作。他们以统一的名字[Kylix开发了Delphi和C](../Page/Kylix_programming_tool.md "wikilink")++
Builder的Linux版本。这也是他们首次为Linux平台带来了Borland在[集成开发环境上的专业技术](../Page/集成开发环境.md "wikilink")。Kylix於2001年发布。

原准备将生产[InterBase的部门分拆单独组建一个新公司的计划被放弃](../Page/InterBase.md "wikilink")，因为Borland和将要运营新公司的人员在一些分拆条款上不能达成一致。在新的管理层的治理下，该部门也重新注入了活力。Borland停止了InterBase源代码开放版本的发行，但也加快了新版本的开发和销售。

Borland在Delphi
6中增加了[web服务的功能](../Page/web服务.md "wikilink")，这也是第一个支持web服务的集成开发环境。现在他们所有当前的开发平台都支持web服务。

[C\#Builder在](../Page/CSharpBuilder.md "wikilink")2003年发布。作为Borland自己的一个[C\#开发工具](../Page/C♯.md "wikilink")，它将和微软的[Visual
Studio
.NET做正面的竞争](../Page/Visual_Studio_.NET.md "wikilink")。C\#Builder，Win32平台Delphi和.NET平台Delphi被整合为一个单独的IDE--Delphi
2005。对web服务和.NET的支持大大增强了Borland在业内的形象。随着这些产品的持续赢利，在2002年晚些时候，Borland并购了设计工具开发厂商TogetherSoft和工具发布厂商[Starbase](../Page/Starbase_Corporation.md "wikilink")，后者是配置管理工具[StarTeam和需求管理工具](../Page/StarTeam.md "wikilink")[CaliberRM的开发商](../Page/CaliberRM.md "wikilink")。[JBuilder和](../Page/JBuilder.md "wikilink")[Delphi的后续版本集成了这些工具](../Page/Delphi.md "wikilink")，这让开发人员在开发时可以使用更多的工具。

全面的产品工具集让Borland有了宣布进入应用生命周期管理（ALM）市场的能力，这要提供从需求分析，设计开发到测试发布所需要的一系列的软件开发工具。2004年，Borland公布了它的软件付运优化（SDO）市场计划，这是一个包含了ALM和更高级软件生产概念如项目组合管理和评估工具的计划。

2005年7月Borland前任CEO Dale Fuller在犯下一系列的财务与商务错误之后突然被解职，但是保留了董事会成员的身份。前任COO
Scott Arnold接任过渡时期的总裁与CEO的头衔，2005年11月8日Borland宣布[Tod
Nielsen将在](../Page/Tod_Nielsen.md "wikilink")2005年11月9日接任CEO的职务。

2005年10月，Borland收购了Legadero将它的IT Management and
Governance（ITM\&G）包Tempo加入到Borland的产品线。

2006年2月8日，Borland宣布剥离其IDE部门，这包括[Borland
Delphi](../Page/Borland_Delphi.md "wikilink")、[JBuilder与](../Page/JBuilder.md "wikilink")[InterBase](../Page/InterBase.md "wikilink")。同时宣布即将收购[Segue
Software这家软件测试与质量工具制造商](../Page/Segue_Software.md "wikilink")，把精力集中于应用产品管理（ALM）。剥离出的部分将组成[CodeGear](../Page/CodeGear.md "wikilink")。

2006年3月20日，Borland宣布已经收购Gauntlet Systems, a provider of technology that
screens software under development for quality and security.

2006年11月14日，Borland宣布将开发工具组剥离出去组成一个全资子公司集中于提升开发人员的生产效率。新建立的公司[CodeGear将负责先前与Borland集成开发环境业务相关的四种主要产品线的发展](../Page/CodeGear.md "wikilink")。

2008年5月7日宣布卖给了。\[1\]2008年6月30日完成交割，价格约2450万美元。\[2\][Delphi](../Page/Delphi.md "wikilink")、[C++Builder等产品属于](../Page/C++Builder.md "wikilink")。

## 2009，收购

2009年5月6日，英国软件商Micro
Focus宣布，公司将以7500万美元现金收购Borland软件公司。该笔收购已经获得两家公司董事会的批准，整体并购事宜将在2009年第二季度完成。

## 目前产品

Borland目前产品线包含：

  - [C++ Builder](../Page/C++_Builder.md "wikilink")
  - [CaliberRM](../Page/CaliberRM.md "wikilink")
  - [Delphi](../Page/Delphi.md "wikilink")
  - [Turbo Delphi](../Page/Turbo_Delphi.md "wikilink")
  - [JBuilder](../Page/JBuilder.md "wikilink")
  - [Kylix](../Page/Kylix_programming_tool.md "wikilink")
  - [Optimizeit Suite](../Page/Optimizeit_Suite.md "wikilink")
  - [InterBase](../Page/InterBase.md "wikilink")
  - [JDataStore](../Page/JDataStore.md "wikilink")
  - [Borland Enterprise
    Studio](../Page/Borland_Enterprise_Studio.md "wikilink")，for C++,
    Mobile and Java
  - [Borland Enterprise
    Server](../Page/Borland_Enterprise_Server.md "wikilink")
  - [StarTeam](../Page/StarTeam.md "wikilink")
  - [Together](../Page/Borland_Together.md "wikilink")

## 以前的产品，通常运行在DOS系统下

编程工具：

  - [CodeWright](../Page/CodeWright.md "wikilink")
  - [C\#Builder](../Page/CSharpBuilder.md "wikilink")（Now part of
    [Delphi](../Page/Delphi_programming_language.md "wikilink")）
  - [Turbo Pascal](../Page/Turbo_Pascal.md "wikilink")
  - [Turbo Assembler](../Page/Turbo_Assembler.md "wikilink")
  - [Turbo Debugger](../Page/Turbo_Debugger.md "wikilink")
  - [Turbo C](../Page/Turbo_C.md "wikilink")
  - [Turbo BASIC](../Page/Turbo_BASIC.md "wikilink")
  - [Turbo Prolog](../Page/Turbo_Prolog.md "wikilink")
  - [Turbo C++](../Page/Turbo_C++.md "wikilink")
  - [Borland C++](../Page/Borland_C++.md "wikilink")

通用软件：

  - [SideKick](../Page/SideKick.md "wikilink")

应用软件：

  - [Reflex](../Page/Borland_Reflex.md "wikilink")
  - [Quattro Pro](../Page/Quattro_Pro.md "wikilink")
  - [Paradox](../Page/Borland_Paradox.md "wikilink")
  - [dBase](../Page/DBASE.md "wikilink")

## 參考資料

## 外部链接

  - [Borland软件公司](https://web.archive.org/web/20121105211827/http://www.borland.com/)。
  - [Borland开发者网络](https://web.archive.org/web/20050523003830/http://bdn.borland.com/)。
  - [Borland代码中心](https://web.archive.org/web/20050513235143/http://cc.borland.com/)。
  - [Borland历史：*Will The Real Frank Borland Please Stand
    Up?*](http://community.borland.com/article/0,1410,20283,00.html) –
    By David Intersimone.

## 参考书籍

  - Borland传奇

[Borland](../Category/Borland.md "wikilink")
[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[Category:软件公司](../Category/软件公司.md "wikilink")

1.  [Silicon Valley Technology News, Embarcadero Technologies buys
    CodeGear](http://www.silicontap.com/embarcadero_technologies_buys_codegear/s-0015183.html)
2.