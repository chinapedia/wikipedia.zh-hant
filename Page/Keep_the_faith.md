『**Keep the
faith**』是[日本](../Page/日本.md "wikilink")[男性偶像團體](../Page/男性偶像.md "wikilink")、[KAT-TUN的第](../Page/KAT-TUN.md "wikilink")5張[單曲](../Page/單曲.md "wikilink")。

## 概要

  - 由成員[赤西仁及](../Page/赤西仁.md "wikilink")[田口淳之介主演的](../Page/田口淳之介.md "wikilink")[有閑俱樂部主題曲](../Page/有閑俱樂部.md "wikilink")
  - 由[冰室京介作曲作詞](../Page/冰室京介.md "wikilink")。為近年傑尼斯發行單曲特例，原訂發售日與同公司前輩[TOKIO的](../Page/TOKIO.md "wikilink")『[青春
    SEISYuN](../Page/青春_SEISYuN.md "wikilink")』同一天發行。而後11月4日傑尼斯官網公告[TOKIO的](../Page/TOKIO.md "wikilink")『[青春
    SEISYuN](../Page/青春_SEISYuN.md "wikilink")』延後至為11月28日發行。而這兩張單曲均分別獲得了當週的榜首。
  - 獲得[ORICON](../Page/ORICON.md "wikilink") 2007年年終單曲榜第5名。

## 收錄曲

### 初回限定盤

  - CD

<!-- end list -->

1.  **Keep the faith**
      - 作詞：[冰室京介](../Page/冰室京介.md "wikilink")・SPIN、作曲：冰室京介、編曲：ha-j
      - 由KAT-TUN其中的[赤西仁和](../Page/赤西仁.md "wikilink")[田口淳之介](../Page/田口淳之介.md "wikilink")，與[關西傑尼斯8的](../Page/關西傑尼斯8.md "wikilink")[橫山裕主演的](../Page/橫山裕.md "wikilink")[日本電視台](../Page/日本電視台.md "wikilink")「[有閑俱樂部](../Page/有閑俱樂部.md "wikilink")」[主題歌](../Page/主題歌.md "wikilink")
2.  **Crazy Love**

<!-- end list -->

  - DVD

<!-- end list -->

  - 「Keep the faith」VIDEO CLIP

### 通常盤初回PLUS

1.  **Keep the faith**
2.  **Crazy Love**
3.  **Lovin'U**
4.  **Keep the faith（Original・卡拉OK）**
5.  **Crazy Love（Original・卡拉OK）**
6.  **Lovin'U（Original・卡拉OK）**

### 通常盤

1.  **Keep the faith**
2.  **Crazy Love**
3.  **Keep the faith（Original・卡拉OK）**
4.  **Crazy Love（Original・卡拉OK）**

[Category:KAT-TUN歌曲](../Category/KAT-TUN歌曲.md "wikilink")
[Category:2007年單曲](../Category/2007年單曲.md "wikilink")
[Category:日本電視台電視劇主題曲](../Category/日本電視台電視劇主題曲.md "wikilink")
[Category:2007年Oricon單曲月榜冠軍作品](../Category/2007年Oricon單曲月榜冠軍作品.md "wikilink")
[Category:2007年Oricon單曲週榜冠軍作品](../Category/2007年Oricon單曲週榜冠軍作品.md "wikilink")