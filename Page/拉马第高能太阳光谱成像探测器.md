[Goddard](../Page/Goddard_Space_Flight_Center.md "wikilink")
[Space Sciences
Laboratory](../Page/Space_Sciences_Laboratory.md "wikilink") |
COSPAR_ID = 2002-004A | SATCAT = 27370 | website =
<http://hesperia.gsfc.nasa.gov/rhessi3/> | mission_duration = Planned:
2 years\[1\]
Elapsed:

| manufacturer = [Spectrum
Astro](../Page/Spectrum_Astro.md "wikilink")\[2\] | launch_mass = \[3\]
| dimensions = \[4\] | power = 414 W\[5\]

| launch_date =  [UTC](../Page/UTC.md "wikilink")\[6\] | launch_rocket
= [Pegasus XL](../Page/Pegasus_\(rocket\).md "wikilink") | launch_site
= *[Stargazer](../Page/Stargazer_\(aircraft\).md "wikilink")*, [Cape
Canaveral](../Page/Cape_Canaveral_Air_Force_Station.md "wikilink") |
launch_contractor = [Orbital
Sciences](../Page/Orbital_Sciences_Corporation.md "wikilink")

| disposal_type = | deactivated = | last_contact = | decay_date =

| orbit_reference =
[Geocentric](../Page/Geocentric_orbit.md "wikilink") | orbit_regime =
[Low Earth](../Page/Low_Earth_orbit.md "wikilink") | orbit_semimajor =
| orbit_eccentricity = 0.0011 | orbit_periapsis =  | orbit_apoapsis =
 | orbit_inclination = 38.0367° | orbit_period = 94.5667 min |
orbit_RAAN = 59.1113° | orbit_arg_periapsis = 152.3223° |
orbit_mean_anomaly = 207.8129° | orbit_mean_motion = 15.2265 rev/day
| orbit_epoch = 2 September 2015, 12:16:06 UTC\[7\] |
orbit_rev_number = 74636 | apsis = gee

| telescope_type = [Coded aperture
mask](../Page/Coded_aperture.md "wikilink") | telescope_focal_length=
| telescope_area =  | telescope_wavelength =
[X-ray](../Page/X-ray.md "wikilink"){{\\}}[γ-ray](../Page/Gamma_ray.md "wikilink")
| telescope_resolution = 2 arcsec up to 100 keV
7 arcsec up to 400 keV
36 arcsec above 1 MeV\[8\]

| programme = [Small Explorer
program](../Page/Small_Explorer_program.md "wikilink") |
previous_mission =
[WIRE](../Page/Wide_Field_Infrared_Explorer.md "wikilink") |
next_mission = [GALEX](../Page/GALEX.md "wikilink") }}
[Rhessi.jpg](https://zh.wikipedia.org/wiki/File:Rhessi.jpg "fig:Rhessi.jpg")
**拉马第高能太阳光谱成像探测器**（缩写为）是[美国宇航局于](../Page/美国宇航局.md "wikilink")2002年2月5日发射的一颗[太阳探测卫星](../Page/太阳探测卫星.md "wikilink")，主要目的是研究[太阳耀斑中的粒子加速和能量释放过程](../Page/太阳耀斑.md "wikilink")。这颗卫星原名为高能太阳光谱成像探测器（），为纪念太阳高能物理领域的先驱人物[魯文·拉馬第](../Page/魯文·拉馬第.md "wikilink")（）而更名为。其观测范围覆盖了从3
keV的[软X射线波段到](../Page/软X射线.md "wikilink")20
MeV的[伽玛射线波段](../Page/伽玛射线.md "wikilink")，并具有极高的谱分辨本领。美国宇航局[戈達德太空飛行中心](../Page/戈達德太空飛行中心.md "wikilink")、美国[伯克利加州大学](../Page/伯克利加州大学.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")[苏黎世联邦理工学院等机构参与了卫星的设计和建造](../Page/苏黎世联邦理工学院.md "wikilink")。

## 參見

  - [太陽系探測器列表](../Page/太陽系探測器列表.md "wikilink")

## 外部链接

  -
  -
[RHESSI](../Category/探险者计划.md "wikilink")
[Category:美國太空總署探測器](../Category/美國太空總署探測器.md "wikilink")
[R](../Category/太阳探测卫星.md "wikilink")
[R](../Category/苏黎世联邦理工学院.md "wikilink")

1.

2.
3.  Goddard |accessdate=3 September 2015}}

4.
5.
6.

7.

8.