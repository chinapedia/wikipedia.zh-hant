[神岡線鐵軌.jpg](https://zh.wikipedia.org/wiki/File:神岡線鐵軌.jpg "fig:神岡線鐵軌.jpg")
**神岡線**是一條由[台鐵](../Page/台灣鐵路管理局.md "wikilink")、[國防部共同經營的軍用鐵路貨運支線](../Page/中華民國國防部.md "wikilink")。路線自[潭子站分歧後](../Page/潭子車站.md "wikilink")，向西行進至油庫終點站。全線於1957年3月5日通車，1999年7月1日停駛。舊路基後來變成自行車專用道路「[潭雅神自行車道](../Page/潭雅神自行車道.md "wikilink")」。

## 路線資料

  - 經營管轄：[國防部](../Page/中華民國國防部.md "wikilink")（所有權屬於軍方）
  - 路線距離：潭子＝油庫間13.4km 神岡=基地1.6km
  - [軌距](../Page/軌距.md "wikilink")：1,067毫米
  - 車站數：5（含起訖車站；廢止時數目）
  - 雙線區間：無，全線單線
  - [電化區間](../Page/電氣化鐵路.md "wikilink")：無
  - 開業日期:1957年3月5日
  - 廢止日期:1999年7月1日

## 使用車輛

  - 車頭：R20型[柴電機車](../Page/柴電機車.md "wikilink")
  - 車廂：平車

## 簡介

  - 於1950年代末完工通車\[1\]。興建時代稱**陽明支線**、或**陽明山計畫鐵路**（與[臺北](../Page/臺北.md "wikilink")[陽明山無關](../Page/陽明山.md "wikilink")）。任務為支援清泉崗基地的軍用物資及官兵，包括在[越南戰爭時期載運美軍物資](../Page/越南戰爭.md "wikilink")、國軍車輛的運輸等。

<!-- end list -->

  - 由[潭子站往北前進約](../Page/潭子車站.md "wikilink")800公尺，在台3線（中山路）平交道離開台中線，進入神岡線。
  - 因業務量縮減，加上功能逐漸由公路取代，在1999年7月1日宣告停駛，2002年拆除完畢。

## 車站一覽

（包括1999年(民國88年)7月1日停駛前已廢站者）

<table>
<thead>
<tr class="header">
<th><p>車站名稱<br />
<font size="-2"><span style="font-weight: 400">（國音電碼）</span></font></p></th>
<th><p>停駛時英譯站名（現行<a href="../Page/漢語拼音.md" title="wikilink">漢語拼音</a>）</p></th>
<th><p>營業里程<font size="-2"><span style="font-weight: 400"><br />
潭子起</span><br />
</font></p></th>
<th><p>續接／交會路線及備註</p></th>
<th><p>所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/潭子車站.md" title="wikilink">潭子</a><font size="-2"><span style="font-weight: 400">（ㄊㄗ）</span></font></p></td>
<td><p>Tantze<br />
<small>(Tanzi)</small></p></td>
<td><p>0.0</p></td>
<td><p>神岡線起點；可轉乘<a href="../Page/臺中線.md" title="wikilink">臺中線</a>。</p></td>
<td><p><a href="../Page/臺中縣.md" title="wikilink">臺中縣</a> <a href="../Page/潭子鄉.md" title="wikilink">潭子鄉</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/社口號誌站.md" title="wikilink">社口號誌</a><br />
<font size="-2"><span style="font-weight: 400">（ㄜㄎ）</span></font></p></td>
<td><p>（查證中）<br />
<small>(Shekou)</small></p></td>
<td><p>4.2</p></td>
<td><p>號誌站</p></td>
<td><p>臺中縣<a href="../Page/神岡鄉.md" title="wikilink">神岡鄉</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/神岡車站.md" title="wikilink">神岡</a><font size="-2"><span style="font-weight: 400">（ㄕㄍ）</span></font></p></td>
<td><p>Shenkang<br />
<small>(Shengang)</small></p></td>
<td><p>8.7</p></td>
<td><p>1957年8月21日設站</p></td>
<td><p>臺中縣神岡鄉</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/清泉岡車站.md" title="wikilink">清泉崗</a><font size="-2"><span style="font-weight: 400">（ㄑㄍ）</span></font></p></td>
<td><p>Chingchuankang<br />
<small>(Qingquangang)</small></p></td>
<td><p>12.1</p></td>
<td><p>貨運站</p></td>
<td><p>臺中縣<a href="../Page/大雅鄉.md" title="wikilink">大雅鄉</a><br />
台中縣<a href="../Page/沙鹿鎮.md" title="wikilink">沙鹿鎮</a></p></td>
</tr>
<tr class="odd">
<td><p>油庫</p></td>
<td><p>（查證中）<br />
<small>(Depot)</small></p></td>
<td><p>13.4</p></td>
<td><p>神岡線終點；貨運站。</p></td>
<td><p>臺中縣大雅鄉</p></td>
</tr>
</tbody>
</table>

## 參考文獻

  - 楊鵬飛著，《臺灣區鐵道古今站名詞典》，1999
  - [鄧志忠](../Page/鄧志忠.md "wikilink")/[古庭維](../Page/古庭維.md "wikilink")，《臺灣舊鐵道散步地圖》，2010,頁98-105
  - [神岡線](http://content.edu.tw/local/changhwa/dachu/lanhouse/total/tc-12/Default.htm)

## 相關條目

[歷史](../Category/台中市交通.md "wikilink")
[Category:臺灣鐵路廢線](../Category/臺灣鐵路廢線.md "wikilink")
[廢](../Category/臺灣鐵路管理局路線.md "wikilink")

1.  請參閱http://content.edu.tw/local/changhwa/dachu/lanhouse/total/tc-12/Default.htm