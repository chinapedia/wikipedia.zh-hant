[Iodate-3D-vdW.png](https://zh.wikipedia.org/wiki/File:Iodate-3D-vdW.png "fig:Iodate-3D-vdW.png")。\]\]
**碘酸盐**是[碘酸所成的](../Page/碘酸.md "wikilink")[盐类](../Page/盐.md "wikilink")，\[1\]含有[三角锥型的](../Page/三角锥.md "wikilink")**碘酸根**离子—IO<sub>3</sub><sup>−</sup>，其中碘的[化合价为](../Page/化合价.md "wikilink")+5。

碘酸盐可由用[硫醚还原](../Page/硫醚.md "wikilink")[高碘酸盐制得](../Page/高碘酸盐.md "wikilink")，副产物为[亚砜](../Page/亚砜.md "wikilink")。\[2\]

碘酸盐的例子有：

  - [碘酸钠](../Page/碘酸钠.md "wikilink")—NaIO<sub>3</sub>
  - [碘酸钾](../Page/碘酸钾.md "wikilink")—KIO<sub>3</sub>
  - [碘酸银](../Page/碘酸银.md "wikilink")—AgIO<sub>3</sub>
  - [碘酸钙](../Page/碘酸钙.md "wikilink")—Ca(IO<sub>3</sub>)<sub>2</sub>
  - 碘酸氢钾—KH(IO<sub>3</sub>)<sub>2</sub>，[碘酸钾和碘酸的](../Page/碘酸钾.md "wikilink")[复盐](../Page/复盐.md "wikilink")

同为[卤素的](../Page/卤素.md "wikilink")[氯和](../Page/氯.md "wikilink")[溴也可形成](../Page/溴.md "wikilink")[氧化态为V的酸盐](../Page/氧化态.md "wikilink")，称为[氯酸盐和](../Page/氯酸盐.md "wikilink")[溴酸盐](../Page/溴酸盐.md "wikilink")，与碘酸盐的性质类似。

碘酸盐与酸反应生成[碘酸](../Page/碘酸.md "wikilink")。它是[碘钟反应反应物之一](../Page/碘钟反应.md "wikilink")，有时可用于[食盐加碘](../Page/食盐加碘.md "wikilink")。

## 参考资料

<div class="references-small">

<references/>

</div>

[Category:按阴离子分类的化合物](../Category/按阴离子分类的化合物.md "wikilink")
[\*](../Category/碘酸盐.md "wikilink")

1.  [Merriam-Webster
    definition](http://www.m-w.com/cgi-bin/dictionary?book=Dictionary&va=iodate)
2.