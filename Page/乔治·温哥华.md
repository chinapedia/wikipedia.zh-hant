**喬治·溫哥華**（，），[英国](../Page/英国.md "wikilink")[皇家海军军官](../Page/英国皇家海军.md "wikilink")。以其对[北美](../Page/北美.md "wikilink")[太平洋海岸](../Page/太平洋.md "wikilink")（即現時[加拿大](../Page/加拿大.md "wikilink")[卑詩省和](../Page/卑詩省.md "wikilink")[美國](../Page/美國.md "wikilink")[華盛頓](../Page/華盛頓州.md "wikilink")、[俄勒岡及](../Page/俄勒岡州.md "wikilink")[阿拉斯加州對出海岸](../Page/阿拉斯加州.md "wikilink")）的勘测活动而出名。

## 生平

温哥华出生于[诺福克郡](../Page/诺福克郡.md "wikilink")[金斯林](../Page/金斯林.md "wikilink")，15岁时即随[詹姆斯·库克船长远航太平洋](../Page/詹姆斯·库克.md "wikilink")。其后，他作为英国海军的一员，参与了在[西印度群岛对](../Page/西印度群岛.md "wikilink")[法国舰队的作战](../Page/法国.md "wikilink")。1789年，英国和[西班牙为争夺西北北美洲的殖民权发生冲突](../Page/西班牙.md "wikilink")，温哥华被派往[努特卡湾服役](../Page/努特卡湾.md "wikilink")。当这一危机解除后，温哥华受命率[发现号军舰对努特卡湾和附近海岸进行勘查以宣示主权](../Page/发现号.md "wikilink")。

1792年，当温哥华沿着今日俄勒冈州和华盛顿州海岸北上时，遇到了美国海军船长[罗伯特·格雷](../Page/罗伯特·格雷.md "wikilink")。格雷其后转而溯[哥伦比亚河而上](../Page/哥伦比亚河.md "wikilink")，勘测内地。温哥华接着北上，考察了分割[温哥华岛和北美大陆的](../Page/温哥华岛.md "wikilink")[胡安·德富卡海峡](../Page/胡安·德富卡海峡.md "wikilink")。结束考察以后，他前往努特卡湾，接受按照两国协定而由西班牙返还给英国的定居点房屋。当年冬天，他前往[加利福尼亚海岸](../Page/加利福尼亚.md "wikilink")，其间曾一度前往[夏威夷群岛过冬并考察](../Page/夏威夷群岛.md "wikilink")。

1793年，温哥华再一次向北航行，最远到达北纬56度，当年冬天再次前往夏威夷越冬。1794年，他又一次向北考察，到达了[库克湾](../Page/库克湾.md "wikilink")，此后率船经[合恩角回国](../Page/合恩角.md "wikilink")，其後在1798年5月12日於倫敦去世。

在温哥华的考察报告中，他指出[西北航道不存在于大家原以为的纬度](../Page/西北航道.md "wikilink")。为了纪念他的功绩，北美太平洋西北地區多處地方以其名命名，包括温哥华岛和兩個温哥华市（[一個位於卑詩省](../Page/溫哥華.md "wikilink")，[另一個則位於華盛頓州](../Page/溫哥華_\(華盛頓州\).md "wikilink")）等等。

## 外部链接

  - [美洲西北海岸和亚洲东北部分地区地图，标绘皇家海军舰艇 Resolution（果决号）和 Discovery（发现号）帆船自 1778
    年 5 月到 10 月的行程路线](http://www.wdl.org/zh/item/6771/)

[V](../Category/英国航海家.md "wikilink")
[Category:英格蘭探險家](../Category/英格蘭探險家.md "wikilink")
[Category:葬于伦敦](../Category/葬于伦敦.md "wikilink")
[Category:詹姆斯·库克](../Category/詹姆斯·库克.md "wikilink")