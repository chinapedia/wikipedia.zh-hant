**鳳飛颺**，本名**林鴻棠**（Kempis
Lim），[台灣](../Page/台灣.md "wikilink")[桃園](../Page/桃園市.md "wikilink")[大溪人](../Page/大溪區.md "wikilink")，為著名歌星[鳳飛飛的胞弟](../Page/鳳飛飛.md "wikilink")。已婚，育有一子。

## 經歷

鳳飛颺早年追隨其姊鳳飛飛，進入台灣[演藝圈](../Page/演藝圈.md "wikilink")。1983年10月25日，在鳳飛飛主持的[中國電視公司](../Page/中國電視公司.md "wikilink")[台灣光復節](../Page/台灣光復節.md "wikilink")[特別節目](../Page/特別節目.md "wikilink")《鳳懷鄉土情（二）》中，鳳飛飛與鳳飛颺飾演一對[情侶](../Page/情侶.md "wikilink")，是鳳飛颺首度露面。1984年，鳳飛颺推出首張專輯《你我手牽手》。

1988年後，鳳飛颺逐漸淡出演藝圈，轉做[禮品生意失敗](../Page/禮品.md "wikilink")。鳳飛飛與[趙宏琦](../Page/趙宏琦.md "wikilink")[結婚之後](../Page/結婚.md "wikilink")，鳳飛飛帶鳳飛颺去[香港向趙宏琦學習](../Page/香港.md "wikilink")[經營](../Page/經營.md "wikilink")，但是鳳飛颺適應不良而返回台灣。鳳飛颺回到台灣之後，開[計程車維生](../Page/計程車.md "wikilink")，姊弟之間的[親情依舊良好](../Page/親情.md "wikilink")。2005年，鳳飛飛[演唱會上](../Page/演唱會.md "wikilink")，是鳳飛颺最後一次出現在公開場合。

2006年12月初，鳳飛颺[手術過後返家休養](../Page/手術.md "wikilink")，持續昏睡未醒，被家人立即送往[馬偕紀念醫院台北院區](../Page/馬偕紀念醫院.md "wikilink")[急救](../Page/急救.md "wikilink")。2006年12月12日，鳳飛颺因[腮腺](../Page/腮腺.md "wikilink")[癌併發](../Page/癌.md "wikilink")[猛爆性肝炎](../Page/猛爆性肝炎.md "wikilink")，經[換血搶救無效](../Page/換血.md "wikilink")，病逝於馬偕紀念醫院台北院區，享年49歲，當時其獨子12歲。2006年12月23日，鳳飛颺的家人在[臺北市第二殯儀館舉辦鳳飛颺](../Page/臺北市第二殯儀館.md "wikilink")[告別式](../Page/告別式.md "wikilink")。

## 唱片作品

  - 《不說話的愛情》：[北聯唱片發行](../Page/北聯唱片.md "wikilink")。
  - 《豆芽夢》：1985年，北聯唱片發行。
  - 《你我手牽手》：1985年，[華倫有限公司發行](../Page/華倫有限公司.md "wikilink")，製作人是[陳彼得](../Page/陳彼得.md "wikilink")。

## 主持作品

### 中國電視公司

  - 《又見彩虹》：1984年，和[鳳飛飛主持](../Page/鳳飛飛.md "wikilink")。
  - 《鳳懷鄉土情》：1984年，和[鳳飛飛主持](../Page/鳳飛飛.md "wikilink")。
  - 《四海同心》：1985年，和[鳳飛飛主持](../Page/鳳飛飛.md "wikilink")。

## 電影作品

  - 《六祖[惠能傳](../Page/惠能.md "wikilink")》
  - 《豆芽夢》（與[楊林主演](../Page/楊林_\(歌手\).md "wikilink")）
  - 《[國父傳](../Page/孫文.md "wikilink")》（與[陳凱倫主演](../Page/陳凱倫.md "wikilink")）
  - 《失蹤人口》
  - 《情與法》

## 資料來源

  - 陳慧貞 台北報導，[〈鳳飛飛嚎哭送弟
    照顧12歲小侄子〉](http://www.libertytimes.com.tw/2006/new/dec/15/today-show2.htm)，《[自由時報](../Page/自由時報.md "wikilink")》，2006年12月15日
  - 秦綾謙、林鴻昌
    台北報導，〈鳳飛飛弟病逝！鳳飛颺猛爆肝炎〉，[年代新聞台](../Page/年代新聞台.md "wikilink")《年代晚報》，2006年12月某日
  - 特勤組、王慧倫、吳禮強 台北報導，[〈鳳飛飛快閃
    手稿送亡弟〉](http://tw.nextmedia.com/applenews/article/art_id/3129229/IssueID/20061224)，《[蘋果日報](../Page/蘋果日報_\(台灣\).md "wikilink")》，2006年12月24日
  - 吳禮強 台北報導，[〈鳳飛颺
    曾被姊姊吼到哭〉](http://tw.nextmedia.com/applenews/article/art_id/3129252/IssueID/20061224)，《蘋果日報》，2006年12月24日

[F鳳](../Category/台湾男歌手.md "wikilink")
[Category:林姓](../Category/林姓.md "wikilink")
[Category:大溪人](../Category/大溪人.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[Category:閩南裔臺灣人](../Category/閩南裔臺灣人.md "wikilink")