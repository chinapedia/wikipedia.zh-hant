<File:巴山中路> - panoramio.jpg
**安康市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[陕西省下辖的](../Page/陕西省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于陕西南部偏东，北依[秦岭](../Page/秦岭.md "wikilink")，南沿[大巴山](../Page/大巴山.md "wikilink")-{A|zh-hans:余脉;zh-hant:餘脈}-。

## 历史

[西汉时](../Page/西汉.md "wikilink")，[西城县隶汉中郡](../Page/西城县_\(西汉\).md "wikilink")。[北朝北周初撤县](../Page/北朝.md "wikilink")。[隋复设西城县](../Page/隋.md "wikilink")，唐朝是[金州州治](../Page/金州.md "wikilink")，之后州、县名多变。[清乾隆四十八年](../Page/清.md "wikilink")（1783年）置安康县。1950年设安康市，1954年撤消，1988年复设安康市。

安康历史悠久，人文史可上溯到[石器时代](../Page/石器时代.md "wikilink")。[夏代](../Page/夏代.md "wikilink")，安康属[梁州的一部分](../Page/梁州.md "wikilink")。据中国最早的地理著作《[尚书](../Page/尚书.md "wikilink")·[禹贡](../Page/禹贡.md "wikilink")》记载，将全国分为九州，称“华阳黑水惟梁州”，唐代学者认为梁州为今[汉中](../Page/汉中.md "wikilink")、安康、[商洛一带](../Page/商洛.md "wikilink")。[商](../Page/商.md "wikilink")[周时安康成为](../Page/周.md "wikilink")[庸国的封地](../Page/庸国.md "wikilink")，史称上庸。据《太平环宇记》载：“金州于[战国为](../Page/战国.md "wikilink")[楚地](../Page/楚.md "wikilink")，附庸地，后为楚所灭，复为楚地”。

公元前312年，[秦惠王在安康](../Page/秦惠王.md "wikilink")[汉江北岸台地设西城县](../Page/汉江.md "wikilink")，[汉中郡设在西城](../Page/汉中郡.md "wikilink")。[秦统一六国](../Page/秦.md "wikilink")（公元前221年），西城县为汉中郡治，辖安康、汉阴、石泉、紫阳、岚皋、平利、镇坪七县。

[西汉沿袭秦制](../Page/西汉.md "wikilink")，汉中郡下设五县，[东汉](../Page/东汉.md "wikilink")[建武元年](../Page/建武.md "wikilink")(公元25年)将汉中郡治迁移至汉中南郑县。东汉[建安二十一年](../Page/建安.md "wikilink")(公元215年)，[曹操攻占汉中](../Page/曹操.md "wikilink")，分郡之东部即安康为西城郡。

[三国](../Page/三国.md "wikilink")[曹魏](../Page/曹魏.md "wikilink")[黄初二年](../Page/黄初.md "wikilink")（221年），取“曹魏兴盛”之义，设[魏兴郡](../Page/魏兴郡.md "wikilink")，辖七县，[西晋沿之](../Page/西晋.md "wikilink")。据《兴安府志》载：[晋武帝](../Page/晋武帝.md "wikilink")[太康元年](../Page/太康.md "wikilink")（公元280年）为安置巴山一带流民，取“万年丰乐、安宁康泰”之意，将安阳县更名为安康县，“安康”从此得名。

[南北朝时](../Page/南北朝.md "wikilink")，安康先属南朝，后属北朝，先称直州，[西魏废帝三年](../Page/西魏.md "wikilink")（554年）设[金州](../Page/金州_\(西魏\).md "wikilink")，因越河川道出麸金得州名，隶属魏兴郡。

[隋朝复设西城郡](../Page/隋朝.md "wikilink")。[唐](../Page/唐.md "wikilink")、[五代](../Page/五代.md "wikilink")、[宋设金州安康郡](../Page/宋.md "wikilink")，辖六县。[元朝设金州](../Page/元朝.md "wikilink")，[明代前仍设金州](../Page/明代.md "wikilink")，[万历十一年](../Page/万历.md "wikilink")（公元1583年）汉江洪水覆没金州城，遂于城南赵台山下筑新城，并易名为兴安州。

[清](../Page/清.md "wikilink")[顺治四年](../Page/顺治.md "wikilink")（公元1647年）州府迁回老城。[乾隆四十七年](../Page/乾隆.md "wikilink")（公元1782年）改设[兴安府](../Page/兴安府.md "wikilink")。

[辛亥革命后](../Page/辛亥革命.md "wikilink")，废府设道，在清代六县基础上新增四县归[汉中道](../Page/汉中道.md "wikilink")。[民国二十二年](../Page/民国.md "wikilink")（公元1933年），废道而直隶于省；二十四年（公元1935年）设为陕西省第五行政区行政督察专员公署。

[第二次国共内战后期](../Page/第二次国共内战.md "wikilink")，中共政权逐步攻占中华民国政府辖下县份，于1949年设[安康分区](../Page/安康分区.md "wikilink")，隶属于[陕南行政区](../Page/陕南行政区.md "wikilink")。[陕南军区](../Page/陕南军区.md "wikilink")[第19军于](../Page/中国人民解放军第19军.md "wikilink")5月24日攻占[白河县](../Page/白河县.md "wikilink")，7月10日攻占[平利县](../Page/平利县.md "wikilink")，7月25日第一次攻占安康县。11月28日，第19军55师攻占[岚皋县](../Page/岚皋县.md "wikilink")，30日攻占[紫阳县](../Page/紫阳县.md "wikilink")。第19军57师于11月26日攻占[洵阳县](../Page/洵阳县.md "wikilink")，27日再度攻占安康县\[1\]。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")（1949年10月），于1949年12月，安康分区改为陕西省安康行政督察专员公署。1950年1月，解放军攻占今安康市最南瑞的[镇坪县](../Page/镇坪县.md "wikilink")，至此，安康暨陕西全境“解放”。1950年7月，改称陕西省人民政府[安康专员公署](../Page/安康专区.md "wikilink")，“文革”期间改称[安康地区革命委员会](../Page/安康地区.md "wikilink")，“文革”结束后的第三年（1979年）改为[安康地区行政公署](../Page/安康地区.md "wikilink")。中共安康地委和地区行政公署驻地安康县。1988年，安康县撤县建市，是为县级安康市。

2000年，中华人民共和国国务院批准安康地区撤地设市。新组建的中共安康市委员会、中共安康市纪律检查委员会于2000年12月正式对外办公，撤地建市后，原县级安康市改为[汉滨区](../Page/汉滨区.md "wikilink")。地级安康市于2001年元月1日成立。

## 地理

## 政治

### 现任领导

<table>
<caption>安康市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党安康市委员会.md" title="wikilink">中国共产党<br />
安康市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/安康市人民代表大会.md" title="wikilink">安康市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/安康市人民政府.md" title="wikilink">安康市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议安康市委员会.md" title="wikilink">中国人民政治协商会议<br />
安康市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/郭青.md" title="wikilink">郭青</a>[2]</p></td>
<td><p><a href="../Page/陈勇_(1961年).md" title="wikilink">陈勇</a>[3]</p></td>
<td><p><a href="../Page/赵俊民.md" title="wikilink">赵俊民</a>[4]</p></td>
<td><p><a href="../Page/陈勇_(1962年).md" title="wikilink">陈勇</a>[5]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/故城县.md" title="wikilink">故城县</a></p></td>
<td><p><a href="../Page/陕西省.md" title="wikilink">陕西省</a><a href="../Page/安康市.md" title="wikilink">安康市</a><a href="../Page/汉滨区.md" title="wikilink">汉滨区</a></p></td>
<td><p><a href="../Page/河南省.md" title="wikilink">河南省</a><a href="../Page/扶沟县.md" title="wikilink">扶沟县</a></p></td>
<td><p>陕西省<a href="../Page/汉中市.md" title="wikilink">汉中市</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2013年3月</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年7月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

全市辖1个[市辖区](../Page/市辖区.md "wikilink")、9个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[汉滨区](../Page/汉滨区.md "wikilink")
  - 县：[汉阴县](../Page/汉阴县.md "wikilink")、[石泉县](../Page/石泉县.md "wikilink")、[宁陕县](../Page/宁陕县.md "wikilink")、[紫阳县](../Page/紫阳县.md "wikilink")、[岚皋县](../Page/岚皋县.md "wikilink")、[平利县](../Page/平利县.md "wikilink")、[镇坪县](../Page/镇坪县.md "wikilink")、[旬阳县](../Page/旬阳县.md "wikilink")、[白河县](../Page/白河县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>安康市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[6]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>610900</p></td>
</tr>
<tr class="odd">
<td><p>610902</p></td>
</tr>
<tr class="even">
<td><p>610921</p></td>
</tr>
<tr class="odd">
<td><p>610922</p></td>
</tr>
<tr class="even">
<td><p>610923</p></td>
</tr>
<tr class="odd">
<td><p>610924</p></td>
</tr>
<tr class="even">
<td><p>610925</p></td>
</tr>
<tr class="odd">
<td><p>610926</p></td>
</tr>
<tr class="even">
<td><p>610927</p></td>
</tr>
<tr class="odd">
<td><p>610928</p></td>
</tr>
<tr class="even">
<td><p>610929</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>安康市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[7]（2010年11月）</p></th>
<th><p>户籍人口[8]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>安康市</p></td>
<td><p>2629906</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>汉滨区</p></td>
<td><p>870126</p></td>
<td><p>33.09</p></td>
</tr>
<tr class="even">
<td><p>汉阴县</p></td>
<td><p>246147</p></td>
<td><p>9.36</p></td>
</tr>
<tr class="odd">
<td><p>石泉县</p></td>
<td><p>171097</p></td>
<td><p>6.51</p></td>
</tr>
<tr class="even">
<td><p>宁陕县</p></td>
<td><p>70435</p></td>
<td><p>2.68</p></td>
</tr>
<tr class="odd">
<td><p>紫阳县</p></td>
<td><p>283947</p></td>
<td><p>10.80</p></td>
</tr>
<tr class="even">
<td><p>岚皋县</p></td>
<td><p>154157</p></td>
<td><p>5.86</p></td>
</tr>
<tr class="odd">
<td><p>平利县</p></td>
<td><p>192959</p></td>
<td><p>7.34</p></td>
</tr>
<tr class="even">
<td><p>镇坪县</p></td>
<td><p>50966</p></td>
<td><p>1.94</p></td>
</tr>
<tr class="odd">
<td><p>旬阳县</p></td>
<td><p>426677</p></td>
<td><p>16.22</p></td>
</tr>
<tr class="even">
<td><p>白河县</p></td>
<td><p>163395</p></td>
<td><p>6.21</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")2629906人\[9\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共减少35762人，下降1.34%，年平均下降0.13%。其中，男性人口为1389650人，占52.84%；女性人口为1240256人，占47.16%。总人口性别比（以女性为100）为112.05。0－14岁人口为436360人，占16.59%；15－64岁人口为1957654人，占74.44%；65岁及以上人口为235892人，占8.97%。

参见：[陝西安康孕婦遭強制引產事件](../Page/陝西安康孕婦遭強制引產事件.md "wikilink")

## 经济

工业以丝纺、化工、建材、食品、采矿为主，全市電力均由在[嵐河沿岸所建立的水力發電廠提供](../Page/嵐河.md "wikilink")\[10\]。不過由於這些電站大多是[引水式電站](../Page/引水式電站.md "wikilink")，裝機容量通常較小，發電機組裝在河道旁，上游則建有[水壩](../Page/水壩.md "wikilink")，水壩將水攔截後，通過人工渠道注入發電機組，利用自然落差發電\[11\]，所以發電規模也較小之餘，嵐河的盲目斷流令河流的魚類滅絕，造成生態大災難。

## 交通

  - [襄渝铁路](../Page/襄渝铁路.md "wikilink")（[湖北省](../Page/湖北省.md "wikilink")[襄陽市](../Page/襄陽市.md "wikilink")—[重慶市](../Page/重慶市.md "wikilink")）、[西康铁路](../Page/西康铁路.md "wikilink")（[陝西省](../Page/陝西省.md "wikilink")[西安市](../Page/西安市.md "wikilink")—陝西省[安康市](../Page/安康市.md "wikilink")）、[阳安铁路](../Page/阳安铁路.md "wikilink")（陝西省[漢中市](../Page/漢中市.md "wikilink")[陽平關](../Page/陽平關.md "wikilink")—安康市）三条电气化铁路在此交汇，是陕西省南部的重要交通枢纽。
      - 未来还将建成和推进西康二线、阳安复线、安康至[湖南省](../Page/湖南省.md "wikilink")[张家界](../Page/张家界.md "wikilink")、西安至[四川省](../Page/四川省.md "wikilink")[成都](../Page/成都.md "wikilink")、西安至武汉、西安至重庆客运专线6条铁路。\[12\]
  - [210国道和](../Page/210国道.md "wikilink")[316国道通过本市](../Page/316国道.md "wikilink")，[包茂高速](../Page/包茂高速.md "wikilink")（[內蒙古](../Page/內蒙古.md "wikilink")[包頭市](../Page/包頭市.md "wikilink")—[廣東省](../Page/廣東省.md "wikilink")[茂名市](../Page/茂名市.md "wikilink")）中的[西康高速公路](../Page/西康高速公路.md "wikilink")（陝西省西安市—安康市）2009年5月28日正式通车。
      - 未来还将建成和推进陕鄂安康至[湖北省](../Page/湖北省.md "wikilink")[十堰市](../Page/十堰市.md "wikilink")[竹溪縣](../Page/竹溪縣.md "wikilink")、安康至[廣西](../Page/廣西.md "wikilink")[北海市](../Page/北海市.md "wikilink")、[寧夏](../Page/寧夏.md "wikilink")[银川市至](../Page/银川市.md "wikilink")[廣西](../Page/廣西.md "wikilink")[百色市高速安康段](../Page/百色市.md "wikilink")。\[13\]
  - 最靠近的機場是位於安康市東方205公里的[湖北省](../Page/湖北省.md "wikilink")[十堰武当山机场及西方偏北](../Page/十堰武当山机场.md "wikilink")210公里的[陝西省](../Page/陝西省.md "wikilink")[汉中城固机场](../Page/汉中城固机场.md "wikilink")。
      - 位於安康市[漢濱區的](../Page/漢濱區.md "wikilink")[安康富强机场將取代](../Page/安康富强机场.md "wikilink")2013年起停航的[安康五里舖機場](../Page/安康五里舖機場.md "wikilink")，目前规划中有5条航线，分别飞往[西安](../Page/西安.md "wikilink")、[北京](../Page/北京.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[成都和](../Page/成都.md "wikilink")[武汉](../Page/武汉.md "wikilink")。\[14\]
  - 未来还将建成和推进[汉江高等级区域航道](../Page/汉江_\(中国\).md "wikilink")。\[15\]

## 物产

  - 矿产有沙金、重晶石、金红石、叶蜡石、膨润土、硅石、矾矿等。
  - 可作工业原料的林产品有蚕茧、生漆、油桐等。
  - 果品有核桃、板栗、柑橘、猕猴桃等。
  - 中草药主要有天麻、黄连、当归、党参，绞股蓝等。
  - 土特产品有白厂丝、中华猕猴桃干酒等。

## 旅游

  - [南宫山国家森林公园](../Page/南宫山.md "wikilink")
  - [香溪洞风景名胜区](../Page/香溪洞.md "wikilink")
  - [瀛湖风景区](../Page/瀛湖.md "wikilink")
  - 旬阳太极城景观

## 教育

  - 安康市属第二批本科院校 - [安康学院](../Page/安康学院.md "wikilink")
  - 安康市属中学 - [安康中学](../Page/安康中学.md "wikilink")

## 名人

  - [祝塏](../Page/祝塏.md "wikilink")，清朝政治人物、進士出身。
    生於道光六年（1826年），道光二十七年（1847年）中進士。曾任大順廣道、長蘆鹽運使等職。以戰功獲二品頂戴。
  - [张紫樵](../Page/张紫樵.md "wikilink")，晚清进士。
  - [陈树藩](../Page/陈树藩.md "wikilink")，清末民初安康县人，曾任北洋政府督理陕西军务兼民政长。
  - [张宝麟](../Page/张宝麟.md "wikilink")，陕西省紫阳县洞河乡人。清末民国时期军事、商界人物。
  - [周正龙](../Page/周正龙.md "wikilink") 镇坪县城关镇文采村村民,
    参见[华南虎照片事件](../Page/华南虎照片事件.md "wikilink")
  - [谢亚龙](../Page/谢亚龙.md "wikilink")
    2002-2004年挂职任市委副书记，之后被调任为中国足协专职副主席，国家足球运动管理中心主任、党委副书记。
  - [邹东涛](../Page/邹东涛.md "wikilink")
    汉阴县人，现任中国社会科学文献出版社总编辑，曾任[中国社会科学院研究生院常务副院长](../Page/中国社会科学院研究生院.md "wikilink")。
  - [杨达才](../Page/杨达才.md "wikilink")，微笑哥，表哥。
  - [张涛](../Page/张涛_\(化学家\).md "wikilink")，化学家，[中国科学院院士](../Page/中国科学院院士.md "wikilink")。
  - [柯尊平](../Page/柯尊平.md "wikilink")（1956年9月－），陝西安康人；[西南交通大學物理師資班畢業](../Page/西南交通大學.md "wikilink")，工學博士。
  - [劉建明](../Page/劉建明.md "wikilink")（1953年－），陝西安康人，漢族，中華人民共和國政治人物、第十一屆全國人民代表大會陝西地區代表。
  - [南嶽懷讓](../Page/南嶽懷讓.md "wikilink")，
    俗姓杜，唐代金州安康（今陝西安康）人，禪宗高僧，為六祖惠能門下，與青原行思形成兩大支派。
  - [徐啟方](../Page/徐啟方.md "wikilink")（1965年8月—），陝西漢濱人，1986年7月參加工作，1986年7月加入中共，在職中南財經政法大學工商管理碩士。
  - [馬青年](../Page/馬青年.md "wikilink")（1917年－1997年），又名馬青廉，經名撒力海，祖籍河南孟縣，出生陝西省安康縣人，中華人民共和國政治人物。
  - [徐山林](../Page/徐山林.md "wikilink")（1935年10月－），陝西安康市人，中華人民共和國政治人物。
  - [張寶麟](../Page/張寶麟.md "wikilink")（1883～1946），原名允贊，字仲仁，陝西省紫陽縣洞河鄉人。清末民國時期軍事、商界人物。
  - [雷鍾德](../Page/雷鍾德.md "wikilink")（1842年12月30日－1910年），派名潤身，字子脩，又字仲宣，號禹門，行二，陝西省興安府安康縣人，原籍西安府渭南縣，同治三年甲子科舉人，同治十年辛未科進士，欽點翰林院庶吉士。
  - [李襲志](../Page/李襲志.md "wikilink")，字重光，本隴西郡狄道人。五葉祖李景避地安康，稱金州安康縣人。隋朝、唐朝官員。
  - [劉小燕](../Page/劉小燕.md "wikilink")（1958年11月－），陝西安康人。女，漢族。1980年2月參加工作，1985年11月加入中國共產黨。
  - [毛一豸](../Page/毛一豸.md "wikilink")，山西澤州人，清朝政治人物、進士出身。
    順治三年，登進士。順治十三年，以陝西布政司參議任關南分守道。後因救火憂病，逝於紫陽官舍。
  - [謝馨](../Page/謝馨.md "wikilink")（1870年－1952年），中國清朝翰林、官員，字宗山，一字紹宣，號伯蘭。
  - [阮善繼](../Page/阮善繼.md "wikilink")，陝西省興安府安康縣人，清朝政治人物、同進士出身。
  - [張繼信](../Page/張繼信.md "wikilink")，陝西省興安府安康縣人，清朝政治人物、同進士出身。
  - [雷寶荃](../Page/雷寶荃.md "wikilink")，陝西省興安府安康縣人，清朝政治人物、同進士出身。
  - [謝裕楷](../Page/謝裕楷.md "wikilink")，陝西省興安府安康縣人，清朝政治人物、同進士出身。
  - [劉應祥](../Page/劉應祥.md "wikilink")，字雲皋，湖南新化人，清朝政治人物，同進士出身。
    清道光二十四年（1844）登進士，授陝西城固、安康知縣，升任寧羌州知州。
  - [余寶蔆](../Page/余寶蔆.md "wikilink")，陝西省興安府安康縣人，清朝政治人物、同進士出身。
  - [萬武義](../Page/萬武義.md "wikilink")（1952年3月12日－），陝西安康人，中共中央宣傳部新聞協調小組組長、新華社國內部主任，據傳其於2010年出逃英國。
  - [田寶蓉](../Page/田寶蓉.md "wikilink")，陝西省興安府安康縣人，清朝政治人物、同進士出身。
  - [懷讓](../Page/懷讓.md "wikilink")（公元677～744年），安康人。著名唐朝高僧。
  - [王樾](../Page/王樾.md "wikilink")（1869年－1941年）字嵐溪，號蔭芝。陝西省磚坪廳落金坪人。中華民國政治人物、中醫醫生。

## 参考文献

## 外部链接

  - [安康市市政府網頁](http://www.ankang.gov.cn/)

[安康](../Category/安康.md "wikilink")
[Category:陕西地级市](../Category/陕西地级市.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.
12. [安康富强机场年内开工
    规划5条航线](http://sn.ifeng.com/shanxidifangzixun/ankang/detail_2013_10/30/1391791_0.shtml)鳳凰網，2013年10月30日

13.
14.
15.