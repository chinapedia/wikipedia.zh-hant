**俄歇效应**（Auger
effect）是[原子发射的一个](../Page/原子.md "wikilink")[电子导致另一个电子被发射出来的](../Page/电子.md "wikilink")[物理现象](../Page/物理.md "wikilink")。\[1\]

当一个处于内层[电子被移除后](../Page/电子.md "wikilink")，留下一个空位，高能级的电子就会填补这个空位，同时释放[能量](../Page/能量.md "wikilink")。通常能量以发射光子的形式释放，但也可以通过发射原子中的一个电子来释放。第二个被发射的电子叫做**[俄歇电子](../Page/俄歇电子.md "wikilink")**。\[2\]

被发射时，俄歇电子的动能等于第一次[电子跃迁的能量与俄歇电子的](../Page/电子跃迁.md "wikilink")[离子能之间的能差](../Page/离子能.md "wikilink")。这些能级的大小取决于原子类型和原子所处的化学环境。俄歇电子谱，是用[X射线或高能电子束来产生俄歇电子](../Page/X射线.md "wikilink")，测量其强度和能量的关系而得到的谱线。其结果可以用来识别原子及其原子周围的环境。

**俄歇复合**是[半导体中一个类似的俄歇现象](../Page/半导体.md "wikilink")：一个电子和[空穴](../Page/空穴.md "wikilink")（电子空穴对）可以复合并通过在[能带内发射电子来释放能量](../Page/能带.md "wikilink")，从而增加能带的能量。其逆效应称作[碰撞电离](../Page/碰撞电离.md "wikilink")。

“俄歇效应”是以其发现者，法国人[皮埃爾·維克托·俄歇](../Page/皮埃爾·維克托·俄歇.md "wikilink")（Pierre
Victor Auger）的名字命名的。

## 发现

俄歇过程是在1920年由奥地利科学家[莉泽·迈特纳发现的](../Page/莉泽·迈特纳.md "wikilink")。俄歇效应是1925年，[Pierre
Victor
Auger在分析了Wilson](../Page/Pierre_Victor_Auger.md "wikilink")[云室实验的结果后发现的](../Page/云室.md "wikilink")。实验中用了高能X射线来电子气体，并观察到了[光电子](../Page/光电子.md "wikilink")。对电子的测量表明其轨迹与入射光子的频率无关，这表明电子电离的机制是原子内部能量交换或无辐射跃迁。运用基本量子力学计算出跃迁率和跃迁概率，以及进一步的实验和理论研究表明，该效应的机制是无辐射跃迁，而非内部能量交换。(ref
1)

## 参考文献

  - "The Auger Effect and Other Radiationless Transitions". Burhop,
    E.H.S., Cambridge Monographs on Physics, 1952

<references/>

[de:Augerelektronenspektroskopie\#Auger-Effekt](../Page/de:Augerelektronenspektroskopie#Auger-Effekt.md "wikilink")

[Category:原子物理学](../Category/原子物理学.md "wikilink")
[Category:基础量子物理学](../Category/基础量子物理学.md "wikilink")
[Category:科学技术](../Category/科学技术.md "wikilink")

1.  Auger effect <http://goldbook.iupac.org/A00520.html>
2.  Auger effect <http://goldbook.iupac.org/A00521.html>