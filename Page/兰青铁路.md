**兰青铁路**是连接[中国西北](../Page/中国.md "wikilink")[甘肃](../Page/甘肃.md "wikilink")[蘭州市](../Page/蘭州市.md "wikilink")[西固區和](../Page/西固區.md "wikilink")[青海](../Page/青海.md "wikilink")[西宁的一条铁路](../Page/西宁.md "wikilink")。兰青铁路东起兰州的[河口南站](../Page/河口南站.md "wikilink")，向西横跨[黄河](../Page/黄河.md "wikilink")，沿[湟水河向西](../Page/湟水河.md "wikilink")，跨越[大通河到西宁市](../Page/大通河.md "wikilink")，全长187.4公里，于1959年10月1日开通，[兰新第二双线开通前是青海与内地唯一的铁路连接](../Page/兰新第二双线.md "wikilink")。

2006年4月24日，兰青铁路第二线及电气化开工，概算总额28.05亿元，其中电气化改造1.38亿元，总工期30个月。2008年6月30日，[西宁站至](../Page/西宁站.md "wikilink")[海石湾站复线完成](../Page/海石湾站.md "wikilink")，9月14日海石湾至水车湾区复线开通，与[兰州铁路局接轨](../Page/兰州铁路局.md "wikilink")。2007年3月18日电气化工程开始，2009年4月1日全线电气化\[1\]。

电气化改造完成之后兰青铁路东起改为[甘肃](../Page/甘肃.md "wikilink")[河口南站](../Page/河口南站.md "wikilink")，西至[青海省](../Page/青海省.md "wikilink")[西宁站](../Page/西宁站.md "wikilink")，全程缩短至174公里。列车牵引定数为4000吨，设计时速160公里/小时，年输送能力8000万吨以上。

## 参考资料

[L](../Category/甘肃省铁路线.md "wikilink")
[L](../Category/青海省铁路线.md "wikilink")
[L](../Category/中国铁路支线.md "wikilink")

1.