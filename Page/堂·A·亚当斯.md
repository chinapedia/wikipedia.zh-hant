**唐·A·亚当斯**（Don Alden
Adams）是截至2005年的[守望台圣经书社社长](../Page/守望台圣经书社.md "wikilink")\[1\],
[耶和华见证人的合法机构中一个重要职务](../Page/耶和华见证人的合法机构.md "wikilink")。\[2\]\[3\]\[4\]"他曾经担任[内森·诺尔的秘书](../Page/内森·霍默·诺尔.md "wikilink")，美国伯特利家庭成员，区域监督。

## 家庭背景

唐亚当斯于大约 1925年
出生在[美国的](../Page/美国.md "wikilink")[伊利诺斯州的一个大家庭](../Page/伊利诺斯州.md "wikilink")。
父亲威廉·卡尔·亚当斯和母亲玛丽有五个孩子，三个男孩分别是唐、乔尔和卡尔，妹妹乔伊年纪最小，若塔则最大。他的家庭原本属于[圣公会](../Page/圣公会.md "wikilink")。由于家人对教堂神父的政治态度过于积极而脱离教会。

他的母亲首先对[耶和华见证人的观点产生兴趣](../Page/耶和华见证人.md "wikilink")，之后就是孩子们。他的父亲开始時对耶和华见证人的信仰不感兴趣。堂作为先驱1943年1月开始全时传道工作因而获得[兵役豁免](../Page/义务兵役制.md "wikilink")。但他的一个弟弟没有获得豁免。这样他的父亲就把政府告上法庭。
后来他还帮助其他见证人处理这样的问题。由於對见证人和聖經的了解，他后来也受浸成为一名耶和华见证人。他的姐姐若塔1950年成为海外传道员在[多米尼加共和国工作](../Page/多米尼加共和国.md "wikilink")。\[5\]
\[6\] \[7\]\[8\]

## 服侍职务

堂和两个兄弟开始在 [纽约总部服务](../Page/纽约.md "wikilink")，他在相当一段时间服务于不同的部门。
1950年开始他们兄弟三人都在纽约的伯特立之家服务。\[9\]在先前的职务里, 唐作为秘书为守望台社社长
[内森·霍默·诺尔服务](../Page/内森·霍默·诺尔.md "wikilink")，督導全球的传道工作。唐不时奉派探访其他国家地区的分部和当地的海外传道员。
\[10\] \[11\] \[12\][亚洲](../Page/亚洲.md "wikilink")、
[非洲](../Page/非洲.md "wikilink")、
[欧洲和](../Page/欧洲.md "wikilink")[南北美洲各处都有他的足迹](../Page/美洲.md "wikilink")。唐的妻子多洛蕾丝一直陪伴他。

2000年末,他继 [米尔顿·乔治·韩素尔之后](../Page/米尔顿·乔治·韩素尔.md "wikilink")，被委任为守望台社总经理
，他是守望台圣经书社改革后第一任总经理，从他开始守望台社的社长不再兼任总经理职务。

## 参考文献

## 參考資料

  - 《守望台──宣扬耶和华的王国》，2003年3月1日，26-28页，唐·亚当斯妹妹若塔的自述文章，宾夕法尼亚州守望台圣经书社出版

## 参看

  - [耶和华见证人](../Page/耶和华见证人.md "wikilink")
  - [耶和华见证人的治理机构](../Page/耶和华见证人的治理机构.md "wikilink")
  - [米尔顿·乔治·韩素尔](../Page/米尔顿·乔治·韩素尔.md "wikilink")
  - [内森·霍默·诺尔](../Page/内森·霍默·诺尔.md "wikilink")
  - [守望台圣经书社](../Page/守望台圣经书社.md "wikilink")

[Category:1925年出生](../Category/1925年出生.md "wikilink")
[Category:耶和华见证人](../Category/耶和华见证人.md "wikilink")
[Category:守望台社社长](../Category/守望台社社长.md "wikilink")

1.  The World Almanac and Book of Facts, Volume 2003 by World Almanac
    Education Group, Inc., ©2003, Press Pub. Co. (The New York World)
2.  Theologische Realenzyklopädie, Volume 36 by Gerhard Krause, Gerhard
    Müller, ©2004, Walter de Gruyter, ISBN 3110178427, 9783110178425,
    page 661
3.  Lexikon neureligiöser Gruppen, Szenen und Weltanschauungen by Harald
    Baer, ©2005, Herder, ISBN 3451282569, 9783451282560, page 1413
4.  "Headquarters of Selected Religious Groups in Canada", The History
    Channel, As Retrieved 2009-09-01, "Jehovah's Witnesses (1879),
    Canadian office: Box 4100, Halton Hills, ON L7G 4Y4; Pres., Don
    Adams
5.  The Watchtower, June 1, 1985, page 28
6.  The Watchtower, December 15, 2000
7.  The Watchtower, April 1, 1957, page 200
8.  The Watchtower, March 1, 2003, page 23, "Seeking First the Kingdom—A
    Secure and Happy Life"
9.  "New Missionaries Urged: “Stay Loyal\!”", The Watchtower, November
    15, 1982, page 27
10. "Guyana", 2005年耶和华见证人年鉴, page 196
11. "Venezuela", 1996年耶和华见证人年鉴, page 226
12. "Mozambique", 1996年耶和华见证人年鉴, page 165