在古代[中國](../Page/中國.md "wikilink")，尤其在[春秋戰國時代](../Page/春秋戰國.md "wikilink")，**小說家**為[諸子百家中的其中一家](../Page/諸子百家.md "wikilink")，據[班固所著](../Page/班固.md "wikilink")《[漢書](../Page/漢書.md "wikilink")·[藝文志](../Page/藝文志.md "wikilink")》曰：「小說家者流，蓋出於稗官；街談巷語，道聽途說者之所造也。」，意即小說家所做的事以記錄民間街談巷語，並呈報上級等為主，然而小說家雖然自成一家，但被視為不入流者，故有「[九流十家](../Page/九流十家.md "wikilink")」之說。

古中國小說家所著之書，今多已亡佚，故其學說，已難考查。[班固寫](../Page/班固.md "wikilink")《[漢書](../Page/漢書.md "wikilink")·[藝文志](../Page/藝文志.md "wikilink")》時，撮了今已亡佚的古書《[七略](../Page/七略.md "wikilink")》所錄，將十五本著作編入「小說家」的名下，並以自己的理解作註解。分別是：

| 書名         | 篇章數目   | [班固與](../Page/班固.md "wikilink")[唐侍郎](../Page/唐朝.md "wikilink")[顏師古](../Page/顏師古.md "wikilink")，在[藝文志上的註解](../Page/藝文志.md "wikilink")                                                                                       |
| ---------- | ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 《伊尹說》      | 二十七篇   | 其語淺薄，似依託也                                                                                                                                                                                                                  |
| 《鬻子說》      | 十九篇    | 後世所加                                                                                                                                                                                                                       |
| 《周考》       | 七十六篇   | 其[周事也](../Page/周朝.md "wikilink")                                                                                                                                                                                           |
| 《青史子》      | 五十七篇   | 古[史官記事也](../Page/史官.md "wikilink")                                                                                                                                                                                         |
| 《師曠》       | 六篇     | 見《[春秋](../Page/春秋.md "wikilink")》，其言淺薄，本與此同，似因託之。                                                                                                                                                                          |
| 《務成子》      | 十一篇    | 稱[堯問](../Page/堯.md "wikilink")，非古語                                                                                                                                                                                         |
| 《宋子》       | 十八篇    | 孫卿道宋子，其言[黃老意](../Page/黃老.md "wikilink")                                                                                                                                                                                    |
| 《天乙》       | 三篇     | 天乙謂[湯](../Page/湯_\(商\).md "wikilink")，其言殷時者，皆依託也。                                                                                                                                                                          |
| 《黃帝說》      | 四十篇    | 迂誕依託。                                                                                                                                                                                                                      |
| 《封禪方說》     | 十八篇    | 武帝時。                                                                                                                                                                                                                       |
| 《待詔臣饒心術》   | 二十五篇   | 武帝時。師古曰，[劉向](../Page/劉向.md "wikilink")《[別錄](../Page/別錄.md "wikilink")》云：「饒，齊人也，不知其姓，武帝時時詔，作書，名曰《心術》。」                                                                                                                      |
| 《待詔臣安成未央術》 | 一篇     | 應劭曰，道家也，好養生事，為未央之術。                                                                                                                                                                                                        |
| 《臣壽周紀》     | 七篇     | 項國圉人，宣帝時                                                                                                                                                                                                                   |
| 《虞初周說》     | 九百四十三篇 | （[虞初](../Page/虞初.md "wikilink")）[河南人](../Page/河南.md "wikilink")，武帝時以方士侍郎，號黃車使者。應劭曰：其說以《周書》為本。師古曰，《史記》云：「[虞初](../Page/虞初.md "wikilink")，洛陽人。」即[張衡](../Page/張衡.md "wikilink")《西京賦》「小說九百，本自[虞初](../Page/虞初.md "wikilink")」者也。 |
| 《百家》       | 一百三十九卷 |                                                                                                                                                                                                                            |

根據上述，列為**小說家**的作品十五家，共有一千三百八十篇章的著作。

## 參看

  - [小說家](../Page/小說家.md "wikilink")
  - [九流十家](../Page/九流十家.md "wikilink")