**F-Spot**是一套設計為[GNOME桌面環境上所使用的個人相片管理軟體](../Page/GNOME.md "wikilink")。

## 軟體特色

F-Spot具有簡單易用的介面，但亦有著進階的功能，如圖像注釋、顯示與匯出[Exif和](../Page/EXIF.md "wikilink")[XMP的](../Page/XMP.md "wikilink")[元數據](../Page/元數據.md "wikilink")（metadata）。

其支持所有主要的相像格式，包括[JPEG](../Page/JPEG.md "wikilink")、[PNG](../Page/Portable_Network_Graphics.md "wikilink")、[TIFF](../Page/Tagged_Image_File_Format.md "wikilink")、[DNG與少數廠商定制的](../Page/Digital_Negative_Specification.md "wikilink")[普及細緻影像貯存格式](../Page/普及細緻影像貯存格式.md "wikilink")（[RAW
image
format](../Page/RAW.md "wikilink")），如CR2、PEF、ORF、SRF、CRW、MRW與RAF。此外，其亦支持[GIF](../Page/Graphics_Interchange_Format.md "wikilink")、[SVG與PPM](../Page/可缩放矢量图形.md "wikilink")。

其支持直接由數碼相機匯入相片，而其驅動支持是由[libgphoto2所提供](../Page/libgphoto2.md "wikilink")。

基本的功能如剪下、旋轉與變更大小和進階的功能如消除紅眼、變型等一應俱全。其亦支持顏色修正，包括亮度、對比度、色調、飽和度與色溫的調整。此外，其亦可將多張相片匯合成相片光碟。

在F-Spot媒體庫內的相片可以上載至多個網上相片儲存空間，其中較為著名的有[Flickr](../Page/Flickr.md "wikilink")、[Picasa网络相册等](../Page/Picasa网络相册.md "wikilink")。此外，F-Spot亦可將相片上傳至建基於[Gallery或O](../Page/Gallery.md "wikilink").r.i.g.i.n.a.l.\[1\]的網站。

## 技術資訊

F-Spot是為了GNOME桌面環境而設計的，並以
[C\#程式語言配合](../Page/C_Sharp.md "wikilink")[Mono來編寫](../Page/Mono.md "wikilink")。

F-Spot計劃最初是由[Ettore
Perazzoli所發起](../Page/Ettore_Perazzoli.md "wikilink")，而現在則由[Larry
Ewing所維護](../Page/拉里·厄文.md "wikilink")。

## 相似软件

  - [Photoshop
    Album](../Page/Photoshop_Album.md "wikilink")，由[Adobe發行](../Page/Adobe_Systems.md "wikilink")
  - [iPhoto](../Page/iPhoto.md "wikilink")，由[蘋果電腦發行](../Page/蘋果電腦.md "wikilink")
  - [Picasa](../Page/Picasa.md "wikilink")，由[Google發行](../Page/Google.md "wikilink")

## 参考

## 外部連結

  - [F-Spot首頁](http://f-spot.org/)
  - [F-Spot郵件列表](http://mail.gnome.org/mailman/listinfo/f-spot-list)

[Category:圖像檢視器](../Category/圖像檢視器.md "wikilink")
[Category:GNOME](../Category/GNOME.md "wikilink")

1.  <http://jimmac.musichall.cz/original.php>