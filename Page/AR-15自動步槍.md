**AR-15**是一种輕量化的[氣動式](../Page/氣動.md "wikilink")[突擊步槍](../Page/突擊步槍.md "wikilink")，它發射[小口徑步槍彈](../Page/小口徑步槍彈.md "wikilink")，以[弹匣供弹](../Page/弹匣.md "wikilink")，具备半自动或全自动射击模式，並使用[中心底火式子弹](../Page/中心底火式.md "wikilink")。最初的[阿玛莱特](../Page/阿玛莱特.md "wikilink")/[柯尔特AR](../Page/柯尔特.md "wikilink")-15是一种可选射击模式的原型步枪，原本计划用作陆军士兵步枪，并与由美國柯爾特製造公司生产的民用型号AR-15还有AR-15A2区分开来。

它的軍用型版本為[M16突擊步槍](../Page/M16突擊步槍.md "wikilink")。

## 历史

AR-15是在7.62毫米口径的[AR-10基础上](../Page/AR-10自動步槍.md "wikilink")，由[费尔柴德阿玛莱特公司的](../Page/费尔柴德.md "wikilink")[尤金·斯通纳所设计](../Page/尤金·M·斯通纳.md "wikilink")。AR-15被发展成基于AR-10的一种使用5.56毫米子弹的轻量型号。（AR-15中的“AR”来自阿玛莱特的英文名称**A**rmalite
**R**ifle，而不是[突击步枪](../Page/突击步枪.md "wikilink")**A**ssault
**R**ifle）。由于是第一种使用5.56毫米口径的步枪，它又被誉为开创小口径化先河的步枪。

[阿玛莱特于](../Page/阿玛莱特.md "wikilink")1959年将其AR-10和AR-15的生产权卖给了[柯尔特公司](../Page/美國柯爾特製造公司.md "wikilink")。柯尔特公司将AR-15步枪卖给全世界许多军队组织，包括[美国空军](../Page/美国空军.md "wikilink")、[陆军以及](../Page/美国陆军.md "wikilink")[海军陆战队](../Page/美国海军陆战队.md "wikilink")。AR-15之后逐渐被[美国军方采购](../Page/美国.md "wikilink")，但却是使用[M16的名称](../Page/M16突击步枪.md "wikilink")。然而，柯尔特公司还是在向民众和执法机关的买主提供的该枪的[半自动型号](../Page/半自动射击.md "wikilink")（AR-15,
AR-15A2）中使用AR-15的商标。最初的AR-15是一支非常轻巧的武器，在装空弹匣的时候只有不到6磅重。当然之后重枪管版本的民用AR-15可以重达8.5磅。

今天，民用版本的AR-15和其改型由多家公司制造，并受到世界范围内射击运动爱好者以及[警察们的青睐](../Page/警察.md "wikilink")，因为它们便宜精准而且是模块化设计。（请参见[M16以了解更多关于AR](../Page/M16突击步枪.md "wikilink")-15和其派生型号的发展与演变的历史）

某些革命性，或者至少是重要的AR-15的特征包括：

  - 航空级铝材的[机匣](../Page/机匣.md "wikilink")；
  - 模块化的设计使得多种配件的使用成为可能，并且带来容易维护的优点；
  - 小口径、精准、高弹速；
  - 合成的枪托和握把不容易变形和破裂；
  - [准星可以调整仰角](../Page/准星.md "wikilink")；
  - [表尺可以调整](../Page/表尺.md "wikilink")[风力修正量和射程](../Page/风力修正量.md "wikilink")；
  - 一系列的光学器件可以用来配合或者取代[机械瞄具](../Page/机械瞄具.md "wikilink")；
  - [導氣管式自动方式](../Page/導氣管式.md "wikilink")。

AR-15半自动和全自动的改型外观上有细微的区别，全自动型号在“Semi”图标上面有连发阻铁的固定销，而半自动型号则连固定销安装孔也没有。全自动改型具有一个[选择射击的旋转开关](../Page/选择射击.md "wikilink")，可以让使用人员在三种设计模式中选择：安全、半自动、以及依型号而定的全自动或三发连发。在半自动改型中，这个开关只能在安全和半自动模式中选择。绝大多数AR-15在未上膛的情况下都不可以将保险拨到安全位置。另外，半自动型号的AR-15可以通过更换全自动型的BCG与快慢机，再放入Drop
in auto sear，很简单的变成一只全自动武器，且外观上与半自动型号几乎没有任何差异，因此获得一些不法分子的青睐（私自改造枪支乃是重罪）。

## 技术资料

  - 口径：[.223雷明登及](../Page/.223_Remington.md "wikilink")[5.56x45毫米北约](../Page/5.56×45mm_NATO.md "wikilink")（其他口径的许多改型由多家厂商制造。）
  - 枪长：39英寸（991毫米）
  - 质量／重量：（参见文字）
  - 枪管：标准20英寸（508毫米），16英寸（406毫米）和14.5英寸（368毫米）
  - [膛线](../Page/膛线.md "wikilink")：最早的型号具有1:14的膛线缠距，之后因为使用55格令（3.6克）子弹而改成1:12。后来新型号为了使用62格令的M855和63.5格令的M856，通常使用1:9或1:7的缠距。一般来说，1:7的缠距可以提供更快的自旋速度，从而让射出的重弹头飞行更稳定。
  - 弹匣容量：5、10、20、30、40、60、90、100（见下文）

标准的配发弹匣为20或30发[双排双进弹匣](../Page/弹匣.md "wikilink")。另外Surefire公司提供了60发和100发的四排双进大容量弹匣。另外，50、60、90或100发容量的[弹鼓也可以使用](../Page/弹鼓.md "wikilink")，例如[C-Mag彈鼓或者Magpul公司的D](../Page/C-Mag彈鼓.md "wikilink")60弹鼓。小容量的弹匣也被提供，以符合某些地区的法律，或者是为了打猎，还有就是大弹匣往往不能进行精确依托射击。

针对AR-15设计的具有不同重量与长度的枪管，以及能使用不同口径弹药的[上机匣](../Page/上机匣.md "wikilink")，大量出现在售后市场上。得益于该枪的模組化设计，它们很容易被安装，价格也很适宜。这些弹药包括（以口径大小排序）：

  - [.22 LR](../Page/.22_LR.md "wikilink")
  - [格伦德尔6.5](../Page/格伦德尔6.5.md "wikilink")（.264口径）
  - [6.8 SPC](../Page/6.8_SPC.md "wikilink")（.270口径）
  - [.300 Whisper](../Page/.300_Whisper.md "wikilink")
  - [7.62×39毫米](../Page/7.62×39mm.md "wikilink")
  - [.308温彻斯特](../Page/.308_Winchester.md "wikilink")
  - [9毫米帕拉贝伦](../Page/9毫米鲁格弹.md "wikilink")
  - [.45 ACP](../Page/.45_ACP.md "wikilink")
  - [SOCOM .458](../Page/SOCOM_.458.md "wikilink")
  - [.50 AE](../Page/.50_AE.md "wikilink")
  - [.50 Beowulf](../Page/.50_Beowulf.md "wikilink")
  - [.50 BMG](../Page/.50_BMG.md "wikilink")

当安装一个全新的[上机匣](../Page/上机匣.md "wikilink")，特别是当它设计为可以使用不同口径弹药时（也就是除了雷明登.223或5.56×45毫米北约之外的），下机匣可能也需要根据这种转换进行一定的修改。例如，当转换成9毫米的上机匣的时候，通常需要安装弹匣井块（以适应更小的9毫米弹匣，例如[烏茲衝鋒槍或者](../Page/烏茲衝鋒槍.md "wikilink")[柯爾特9毫米衝鋒槍](../Page/柯爾特9毫米衝鋒槍.md "wikilink")），将.223[击锤更换成为](../Page/击锤.md "wikilink")9毫米弹药设计的击锤，以及根据你原来的[枪托](../Page/枪托.md "wikilink")，更换为你的新的9毫米口径AR-15而设计的[缓冲器](../Page/缓冲器.md "wikilink")、[枪机弹簧和](../Page/枪机弹簧.md "wikilink")[枪托垫片](../Page/枪托垫片.md "wikilink")。

## 改型

[Modified_AR-15.jpeg](https://zh.wikipedia.org/wiki/File:Modified_AR-15.jpeg "fig:Modified_AR-15.jpeg")生產的附件的AR-15改型，當中包括由馬格普生產的[槍托](../Page/槍托.md "wikilink")、、[直角前握把和](../Page/直角前握把.md "wikilink")[護木片](../Page/護木.md "wikilink")\]\]
[Flickr_-_\~Steve_Z\~_-_IMG_4170.jpg](https://zh.wikipedia.org/wiki/File:Flickr_-_~Steve_Z~_-_IMG_4170.jpg "fig:Flickr_-_~Steve_Z~_-_IMG_4170.jpg")
有许多家公司制造大量不同配置的AR-15。这些配置包括从具有可调节枪托和[光学瞄准具的短](../Page/光学瞄准具.md "wikilink")[卡宾枪](../Page/卡宾枪.md "wikilink")，到重枪管的型号，等等。

“AR15”或“AR-15”的[商标是属于](../Page/商标.md "wikilink")[美國柯爾特製造公司所有](../Page/美國柯爾特製造公司.md "wikilink")，且该公司声称这个名称应该只用于他们的产品。其他的AR-15制造商以其他的代号制造AR-15的翻版，但通俗说法中它们仍有时候被叫做“AR-15”。

该步枪使用的运作模式是气吹式。当子弹穿过位于枪支上方的气孔的时候，气体从枪管内排出。然后，该气体涌向该气孔并且进入枪管上方的气管。这根气管从准星基座一直通到这支AR-15的上机匣。在这儿，这根气管嵌进一个“气键”，后者引入气体并令其通过漏斗型的管道进入[枪栓连动座](../Page/枪栓连动座.md "wikilink")。进入后的气体使得枪栓和枪机朝相反方向移动。当枪机向枪支的后段移动的时候，枪栓开始转向并从枪管延伸处释放。[凸轮撞针负责枪栓的转动](../Page/凸轮撞针.md "wikilink")，后者沿着一段凹槽进入螺旋形的枪机而使得枪栓释放。当枪栓被释放后，枪机继续向后段移动，同时子弹壳被分离出来。

一个位于缓冲器之后的复位弹簧其后将枪栓连动座推回膛室。一段进入上机匣的凹槽挡住凸轮撞针并防止其与枪栓旋转进入一个闭合位置。枪栓的闭锁座接着将另一颗子弹推入[上膛坡道](../Page/上膛坡道.md "wikilink")，并进入膛室。当枪栓闭锁座通过机匣延伸部的时候，凸轮撞针可以旋转进入一个研磨接入上机匣的容器。这个旋转的动作伴随着凹槽进入枪机，并使得枪栓旋转并锁入枪管的延伸处。

## 使用國或地区

  -   - 巴哈馬皇家國防軍

  -   - 武裝警察單位

  -   - 波黑武裝部隊

  -   - [加拿大皇家騎警](../Page/加拿大皇家騎警.md "wikilink")
      - 懲教署

  -   - 多米尼加共和國陸軍

  -   - [警察機動部隊](../Page/警察機動部隊.md "wikilink")
      - [機場特警組](../Page/機場特警組.md "wikilink")
      - [特別任務連](../Page/特別任務連.md "wikilink")
      - [懲教署](../Page/懲教署.md "wikilink")

  -   - [印尼武裝部隊](../Page/印尼軍事.md "wikilink")

  -   - 約旦皇家陸軍

  -   - 警察機關

  -   - 墨西哥聯邦警察

  -   -
  -   - [中華民國空降特戰部隊](../Page/中華民國空降特戰部隊.md "wikilink")

  - ：採用由本土及外國生產的改型。

      - [聯邦安全局特種作戰中心](../Page/聯邦安全局.md "wikilink")、[冰雹特種部隊](../Page/冰雹特種部隊.md "wikilink")
      - [車臣共和國](../Page/車臣共和國.md "wikilink")[特別即時反應單位](../Page/特別即時反應單位.md "wikilink")「托列克」

  -   - [新加坡武裝部隊](../Page/新加坡武裝部隊.md "wikilink")

  -   - [越南共和國軍](../Page/越南共和國軍.md "wikilink")

  -   - [泰國皇家陸軍](../Page/泰國皇家陸軍.md "wikilink")

  -   - 特種部隊指揮部

  -   - 多個地方警察部門（採用LMT公司生產的改型）
      - [特种空勤团](../Page/特种空勤团.md "wikilink")（目前已退役）

  - ：採用由柯爾特及多間公司生產的改型。

      - 多個地方警察局
      - [美國國家稅務局刑事調查部](../Page/美國國家稅務局.md "wikilink")

  -   - 水上警察

## 在美国境内的合法性

在[美国](../Page/美国.md "wikilink")，具有可折叠枪托、、等的改型在1994－2004年间是被禁止销售给民众的。这是基于1994年颁布的[暴力犯罪控制和执法法案的禁止攻击武器条款](../Page/暴力犯罪控制和执法法案.md "wikilink")。由于该条款在2004年9月过期，\[1\]这些条款现在只在某些州被视为非法。\[2\]

[加利福尼亚州最近对于AR](../Page/加利福尼亚州.md "wikilink")-15的禁止令重新引起了人们对它的兴趣。将一支标准AR-15的下机匣更换成具有固定弹数（10发）的机匣（见下文的说明）可以使得该枪在加州变得合法，并可以继续使用被禁止的特性，例如叠缩的枪托以及手枪枪柄。该弹匣不可拆卸，因此装弹时射手必须拉下后方拆卸闩，将上机匣靠铰链在前拆卸闩上移动，然后用拆卸夹或者手装入暴露的弹匣，然后合上。为该用途设计的较流行的下机匣由[斯塔格武器公司](../Page/斯塔格武器公司.md "wikilink")、[富尔顿兵工厂](../Page/富尔顿兵工厂.md "wikilink")、[迪恩兵工厂](../Page/迪恩兵工厂.md "wikilink")、[麦伽](../Page/麦伽.md "wikilink")、还有阿密特格制造。斯塔格制造名为STAG-15的下机匣，其被加州[司法部认为是](../Page/司法部.md "wikilink")[「折扣」机匣](http://www.calguns.net/a_california_arak.htm)，而且是暂时合法的。到2006年12月为止，双星，斯塔格，CMMG和麦伽制造的产品在加州全都属于合法的下机匣。如果满足以下的要求，那么这个机匣就可以做成一支完整的步枪：该机匣具有固定的弹匣但是包括不超过10颗的弹药
- 如果这支步枪具有手柄、可折叠式的枪托，等等的话；或者，该机匣具有可拆卸的弹匣，但是不能使用任何种类的附件，例如手柄、可折叠式的枪托，等等。

## 关注

当制造完整武器以及之后的后勤供给的生产商变得过剩的时候，膛室的规格因此出现了混乱。民用（SAAMI）.223雷鸣登和北约5.56毫米都是可用的。虽然这两种枪膛通常可以接受两种弹药，但是在民用标准的枪膛中使用军用规格的子弹会带来比设计标准更大的[膛压](../Page/膛压.md "wikilink")。\[3\]军用标准的膛室通常具有一个较宽敞的喉道，因此产生的气压较小，也同时使用于两种弹药。极少数的制造商（例如[罗克河武器公司](../Page/罗克河武器公司.md "wikilink")）使用了一种名为“韦德”的混合式膛室，既可以提供类似于SAAMI的精确度，又可以安全的使用军用标准的子弹（北约5.56毫米）。膛室的种类、制造商、以及膛线缠距通常都被印在准星前方的枪膛内。

设计中另一点值得关注的就是惯性[击针的采用](../Page/击针.md "wikilink")。一颗轻的击针在[枪栓中移动而不受限制](../Page/枪栓.md "wikilink")。当装弹时枪栓向前锁定时，击针通常会向前移动并撞击已装填弹药的[底火](../Page/底火.md "wikilink")。在军用标准或者高等级民用标准的弹药中，这还不足以击发子弹，只会在底火上留下小小的一声。但是使用更敏感的底火或者没有正确安置底火时，这会导致装弹时的[猛击走火](../Page/猛击走火.md "wikilink")。\[4\]

如同许多其他的小口径武器一样，AR-15在枪膛有水的时候开火会造成枪管破裂。在AR-15步枪侵入水中或者怀疑枪管内有水时，建议将它退弹，枪口朝下，将装弹手柄拉向后方，以使其可以排净积水。

## 流行文化

AR-15樣式的步槍在美國境內非常流行於民間市場，因此在一些電影、電視劇和遊戲中均有出現。但當中只有少數為柯爾特和阿瑪萊特生產的原裝槍，其餘的都是其他廠商生產的仿製型和改良型，而這些槍械一般有自己的命名方式，並被視為獨立的武器。

### 電影

  - 2016年—《[-{zh-hk:13小時：班加西無名英雄;zh-tw:13小時：班加西的秘密士兵;zh-cn:危机13小时;}-](../Page/13小時：班加西的秘密士兵.md "wikilink")》：為突出武器國際（Salient
    Arms International）公司生產的型號“Tier One AR-15”，有兩種版本：
      - 裝上[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、[AN/PEQ-15雷射瞄準器和槍口制動器的版本被泰羅恩](../Page/AN/PEQ-15雷射瞄準器.md "wikilink")·S·「羅恩」·伍茲（[詹姆斯·貝吉·戴爾飾演](../Page/詹姆斯·貝吉·戴爾.md "wikilink")）所使用。
      - 裝上[ACOG光學瞄準鏡和](../Page/ACOG光學瞄準鏡.md "wikilink")[抑制器的版本被戴夫](../Page/抑制器.md "wikilink")·「布恩」·班頓（[大衛·丹曼飾演](../Page/大衛·丹曼.md "wikilink")）所使用。
      - 另外由於該槍在現實中於2014年才推出，出現在這部以2012年為背景的電影可被視為一種時代錯誤。

<!-- end list -->

  - 2017年—《[-{zh-cn:疾速特攻; zh-tw:捍衛任務2：殺神回歸; zh-hk:殺神John Wick
    2;}-](../Page/捍衛任務2：殺神回歸.md "wikilink")》：為塔蘭戰術創新公司（Taran Tactical
    Innovations ，TTI）公司生產的型號“TTI TR-1 AR-15”，裝上Trijicon Accupoint
    1-6×24光學瞄準鏡及[垂直握把](../Page/輔助握把.md "wikilink")，並以增大容量的並聯式馬格普P-MAG彈匣供彈，被強納森·「約翰」·維克（[基努·李維飾演](../Page/基努·李維.md "wikilink")）於[羅馬行動期間所使用](../Page/羅馬.md "wikilink")。

### 電子遊戲

  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：为突出武器國際（Salient
    Arms International）公司生產的型號“GRY AR-15”，命名为“SAI GRY
    AR-15”，哑黑色枪身并使用40发[Magpul](../Page/马格普工业公司.md "wikilink")[弹匣装填](../Page/弹匣.md "wikilink")。为步枪手专用武器，大师级解锁，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、突击消音器、[突击制退器](../Page/炮口制动器.md "wikilink")、SAI
    Jailbreak枪口装置）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、突击握把、突击握把架、[枪挂型榴弹发射器](../Page/FN_EGLM附加型榴弹发射器.md "wikilink")）以及瞄准镜（[EoTech
    553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[ELCAN
    SpecterDS瞄准镜](../Page/C79光学瞄准镜.md "wikilink")、[红点瞄准镜](../Page/红点镜.md "wikilink")、[步枪高级瞄准镜](../Page/ELCAN_SpecterDR光学瞄准镜.md "wikilink")、[Trijicon
    ACOG瞄准镜](../Page/先進戰鬥光學瞄準鏡.md "wikilink")），默认自带SAI Jailbreak枪口装置。
  - 2015年—《[未轉變者](../Page/未轉變者.md "wikilink")》:命名為「Eaglefire」，分類為突擊步槍，等級為稀有等級槍枝，使用30發軍用彈匣或者100發軍用彈鼓，可以改瞄準鏡以及戰術導軌配件。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;zh-hant:虹彩六號：圍攻行動}-](../Page/彩虹六号：围攻.md "wikilink")》（Tom
    Clancy's Rainbow Six: Siege）：于第三年追加下载内容“暗空行动”（Operation Grim
    Sky）中登场，为亚历山大武器（Alexander Arms）公司生产的型号“.50
    Beowulf”，命名为“AR-15
    .50”，使用半自动射击并以10+1发[弹匣装填](../Page/弹匣.md "wikilink")，被城市战术反应部队“暗空”
    GSUTR（[美国陆军特种部队](../Page/美国陆军特种部队.md "wikilink")[三角洲部队](../Page/三角洲部队.md "wikilink")）的特勤干员Maverick用作他的其中一款主武器。
  - 2016年（台版2017）—《[少女前線](../Page/少女前線.md "wikilink")》：為尖锋战术（Spike's
    Tactical）公司所生产的型号“ST-15”十周年纪念款，命名为“ST
    AR-15”，為劇情贈送4星AR角色，不可建造，遊戲設定中，為AR小隊中的成員之一。改造后会再装备相同公司所出产的Pipe
    Hitters短管步枪。

## 参见

  - [9×45毫米摩斯](../Page/9×45毫米摩斯.md "wikilink")

  - [M4卡宾枪](../Page/M4卡宾枪.md "wikilink")

  - [M16突击步枪](../Page/M16突击步枪.md "wikilink")

  - [SR-47突擊步槍](../Page/SR-47突擊步槍.md "wikilink")

  - （Colt's Manufacturing Company）

  - [儒格Mini-14半自動步槍](../Page/儒格Mini-14半自動步槍.md "wikilink")

## 参考资料

## 外部链接

  - [AR Optics Explained](https://gunnewsdaily.com/best-ar-15-scopes/)

  - [Modern Firearms article](http://world.guns.ru/assault/as18-e.htm)

  - [AR15.com: a firearms discussion forum](http://www.ar15.com)

  - [Eugene Stoner
    AR15](https://archive.is/20130102041904/http://www.bobtuley.com/stoner.htm)

  - [Terminal Ballistics of
    AR15](https://archive.is/20130102045926/http://www.bobtuley.com/terminal.htm)

### 制造商

  - [亚历山大武器公司（Alexander Arms）](http://www.alexanderarms.com)

  - [阿玛莱特](http://www.ArmaLite.com)

  - [Bushmaster](http://www.bushmaster.com)

  - [柯尔特](http://www.colt.com)

  - [比赛射击运动公司（Competition Shooting
    Sports）](https://web.archive.org/web/20170724050734/http://www.competitionshootingsports.com/)

  - [DPMS（防务采购制造服务）](http://www.dpmsinc.com)

  - [雷斯·巴尔公司（Les Baer Custom）](http://www.lesbaer.com)

  - [路易斯机器和工具公司（Lewis Machine & Tool）](http://www.lewismachine.net)

  - [LRB武器公司（LRB Arms）](http://www.lrbarms.com)

  - [欧博蓝武器公司（Oberland Arms）](http://www.oberlandarms.com)

  - [奥林匹克武器公司（Olympic Arms）](http://www.olyarms.com)

  - [罗克河武器公司（Rock River Arms）](http://www.rockriverarms.com)

  - [軍刀防御工业公司（Sabre Defense
    Industries）](https://web.archive.org/web/20070126044942/http://www.sabre-defence.com/Home.aspx)

  - [史密斯-韦森](http://www.smith-wesson.com)

  - [SSK工业公司（SSK Industries）](http://www.sskindustries.com)

  - [斯塔格武器公司（Stag Arms）](http://www.stagarms.com)

  - [战术公司（Tactics LLC）](http://www.tacticsllc.com)

[Category:自动步枪](../Category/自动步枪.md "wikilink")
[Category:半自动步枪](../Category/半自动步枪.md "wikilink")
[Category:突擊步枪](../Category/突擊步枪.md "wikilink")
[Category:導氣式槍械](../Category/導氣式槍械.md "wikilink")
[Category:5.56×45毫米槍械](../Category/5.56×45毫米槍械.md "wikilink")
[Category:美國槍械](../Category/美國槍械.md "wikilink")
[Category:美國步槍](../Category/美國步槍.md "wikilink")
[Category:越戰武器](../Category/越戰武器.md "wikilink")

1.
2.
3.
4.