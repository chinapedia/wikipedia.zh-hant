**六柱制**是[粵劇內有關](../Page/粵劇.md "wikilink")[行當的戲班制度](../Page/行當.md "wikilink")。粵劇的行當原為「[末](../Page/末.md "wikilink")」、「[生](../Page/生.md "wikilink")」、「[旦](../Page/旦.md "wikilink")」、「[淨](../Page/淨.md "wikilink")」、「-{[丑](../Page/丑.md "wikilink")}-」、「外」、「小」、「夫」、「[貼](../Page/貼.md "wikilink")」、「[雜](../Page/雜.md "wikilink")」的[十大行當](../Page/十大行當.md "wikilink")。因為戲班常常遠赴外地表演，大型編制使戲班支出龐大。\[1\]後來，經過[抗日戰爭和](../Page/抗日戰爭.md "wikilink")[電影興起後](../Page/電影.md "wikilink")，戲班收入不斷減少。戲班的編制被精簡為以六位台柱為主，即「[文武生](../Page/文武生.md "wikilink")」、「[小生](../Page/小生.md "wikilink")」、「[正印花旦](../Page/旦角.md "wikilink")」、「[二幫花旦](../Page/二幫花旦.md "wikilink")」、「[-{丑}-生](../Page/丑角.md "wikilink")」、「[武生](../Page/武生.md "wikilink")」。\[2\]\[3\]\[4\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:粵劇](../Category/粵劇.md "wikilink")

1.  [粤劇服裝與年代的考證](http://www.huadu.gov.cn:8080/was40/detail?record=3581&channelid=4374)
    ，花都區政府
2.  [粵劇](http://www.digilib.sh.cn/dl/jsp/DQTJZJsp.jsp?type=%D4%C1%BE%E7)，上海數字圖書館
3.  [粵劇名詞](http://www.yue-ju.com/article_view.asp?id=42) ，粵劇藝術網
4.  [中國之音 華夏圖文](http://pics.chinavoc.cn/Gallery/PicShow.aspx?ID=179)