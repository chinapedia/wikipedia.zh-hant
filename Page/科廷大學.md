[Curtin_Building_408_in_2006.jpg](https://zh.wikipedia.org/wiki/File:Curtin_Building_408_in_2006.jpg "fig:Curtin_Building_408_in_2006.jpg")
**科廷大學**（Curtin
University），是位於[澳大利亞](../Page/澳大利亞.md "wikilink")[西澳大利亚州的治所](../Page/西澳大利亚州.md "wikilink")[珀斯之公立](../Page/珀斯.md "wikilink")[大學](../Page/大學.md "wikilink")，2013年獲英倫《[泰晤士報](../Page/泰晤士報.md "wikilink")》選為全球排名第284，其[工商管理碩士課程](../Page/工商管理碩士.md "wikilink")（MBA）為[亞太地區第八名](../Page/亞太地區.md "wikilink")；[2010年1月科廷商學院](../Page/2010年1月.md "wikilink")，在[西班牙](../Page/西班牙.md "wikilink")[世界大學網路排名的商學院排名中](../Page/世界大學網路排名.md "wikilink")，全澳排名第十\[1\]。

## 歷史

1966年創校，當時名為**西澳科技學院**；1986年升格為大學，並重新命名為「Curtin」，是為**科廷科技大學**，取自於前[澳大利亞總理](../Page/澳大利亞總理.md "wikilink")[約翰·科廷](../Page/約翰·柯廷.md "wikilink")（John
Curtin）之名。2010年，更名為科廷大學。

## 校務發展定位

科廷大學亦是全澳洲只有六所大學提供專業精算學課程的其中之一。另外,以珀斯為首的城市持續發展,其造價工程師專業(construction
management and economics)在澳洲業界享有盛名。

## 盟校

澳洲科技大學聯盟（Australian Technology Network of Universities, ATN）

  - [Curtin University](http://www.curtin.edu.au/) **科廷大學**
  - [Queensland University of Technology](http://www.qut.edu.au/)
    [昆士蘭科技大學](../Page/昆士蘭科技大學.md "wikilink")
  - [University of South Australia](http://www.unisa.edu.au/)
    [南澳大學](../Page/南澳大學.md "wikilink")
  - [Royal Melbourne Institute of Technology(RMIT
    University)](http://www.rmit.edu.au/)
    [墨爾本皇家理工大學](../Page/墨爾本皇家理工大學.md "wikilink")
  - [University of Technology, Sydney](http://www.uts.edu.au/)
    [雪梨科技大學](../Page/雪梨科技大學.md "wikilink")

## 學院

  - '''原住民研究中心
  - '''科廷商學院
      - 會計學院
      - 法學和稅收商學院
      - 經濟學和金融學院
      - 信息系統學校
      - 管理學院
      - 市場營銷學院
      - 研究生商學院
  - '''健康科學系
      - 國際健康中心
      - 護理助產學院
      - 職業與社會工作學院
      - 藥學院
      - 物理治療學院
      - 心理學和言語病理學學院
      - 公共衛生學院
  - '''人文學院
      - 建築環境學院
      - 與藝術設計學院
      - 媒體，文化和創意藝術學院
      - 教育學院
      - 社會科學和亞洲語言學院
      - 科廷英語語言中心
      - 人權教育中心
  - '''科學與工程學院
      - 理學院
      - 化學與石油工程學院
      - 土木及機械工程學院
      - 電氣工程和計算機學院
      - 農業與環境學院
      - 西澳大利亞礦業學院

## 校友

在科廷科技大學畢業的知名校友包括以下:

  - [约翰巴特勒](../Page/:en:John_Butler_\(musician\).md "wikilink") 音樂家
  - [Priya Cooper](../Page/:en:Priya_Cooper.md "wikilink"), [Gold
    medal](../Page/Gold_medal.md "wikilink")
    [swimmer](../Page/swimming.md "wikilink") at the
    [Sydney](../Page/Sydney.md "wikilink") [Paralympic
    Games游泳選手](../Page/Paralympic_Games.md "wikilink")
  - [Richard Curry](../Page/:en:Richard_Curry.md "wikilink")
    澳洲原住民族事務委員會委員長
  - [Martin Dougiamas](../Page/:en:Martin_Dougiamas.md "wikilink"),
    [moodle教育軟件創辦人](../Page/moodle.md "wikilink")
  - [Amanda Higgs](../Page/:en:Amanda_Higgs.md "wikilink") 《The Secret
    Life Of Us》電視節目製作人
  - [Sheila McHale](../Page/:en:Sheila_McHale.md "wikilink") 西澳政府閣員、從政人士
  - [Frances O'Connor](../Page/:en:Frances_O'Connor.md "wikilink") 演員
  - [Ljiljanna Ravlich](../Page/:en:Ljiljanna_Ravlich.md "wikilink")
    西澳政府閣員、從政人士
  - [Tony Ronaldson](../Page/:en:Tony_Ronaldson.md "wikilink") [Perth
    Wildcats隊籃球員](../Page/Perth_Wildcats.md "wikilink")
  - [提姆‧溫頓](../Page/:en:Tim_Winton.md "wikilink") 作家
  - [John Worsfold](../Page/:en:John_Worsfold.md "wikilink"), [West
    Coast Eagles隊總教練](../Page/West_Coast_Eagles.md "wikilink")
  - [Natalia Cooper](../Page/:en:Natalia_Cooper.md "wikilink") 《National
    Nine News》Nine Perth氣象播報員

## 参考文献

## 外部链接

  - [科廷大學官方網站](http://www.curtin.edu.au/)
  - [科廷商學院官方網站](http://www.cbs.curtin.edu.au/)（MBA學位課程）
  - [科廷大學沙勞越分校](http://www.curtin.edu.my/)

{{-}}

[category:珀斯](../Page/category:珀斯.md "wikilink")

[Category:科技大學](../Category/科技大學.md "wikilink")
[Category:1986年創建的教育機構](../Category/1986年創建的教育機構.md "wikilink")

1.  [Rank of Business School in
    Australia](http://business-schools.webometrics.info/rank_by_country.asp?country=au)