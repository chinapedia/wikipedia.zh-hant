[Tolo_Harbour_2011.jpg](https://zh.wikipedia.org/wiki/File:Tolo_Harbour_2011.jpg "fig:Tolo_Harbour_2011.jpg")
[Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_1.jpg](https://zh.wikipedia.org/wiki/File:Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_1.jpg "fig:Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_1.jpg")》中之吐露港（Tolo
Harbour）\]\]
[Tolo_Harbour_2013.jpg](https://zh.wikipedia.org/wiki/File:Tolo_Harbour_2013.jpg "fig:Tolo_Harbour_2013.jpg")[Tolo_Harbour_&_Tolo_Channel.jpg](https://zh.wikipedia.org/wiki/File:Tolo_Harbour_&_Tolo_Channel.jpg "fig:Tolo_Harbour_&_Tolo_Channel.jpg")
**吐露港**（）原稱**大埔海**，古稱**大步海**，是[香港](../Page/香港.md "wikilink")[新界的主要](../Page/新界.md "wikilink")[內港之一](../Page/內港.md "wikilink")，位於[沙田區以北](../Page/沙田區.md "wikilink")，[大埔區以東](../Page/大埔區.md "wikilink")，海港呈西南-東北走向，出口處為[大赤門](../Page/大赤門.md "wikilink")，海水從此處流進香港東北面的[大鵬灣](../Page/大鵬灣_\(南海\).md "wikilink")。有多條河流流向吐露港，包括沙田[城門河及大埔](../Page/城門河.md "wikilink")[林村河](../Page/林村河.md "wikilink")。

## 歷史

963年，[南漢在](../Page/南漢.md "wikilink")[大埔置媚川都](../Page/大埔_\(香港\).md "wikilink")，在大埔海採集[珍珠](../Page/珍珠.md "wikilink")，到了[宋朝為巔峰期](../Page/宋朝.md "wikilink")，與[廣東](../Page/廣東.md "wikilink")[合浦](../Page/合浦.md "wikilink")（即今日[廣西](../Page/廣西.md "wikilink")[北海市](../Page/北海市.md "wikilink")[合浦縣](../Page/合浦縣.md "wikilink")）齊名。由於[採珠業發達](../Page/採珠業.md "wikilink")，大埔海亦稱**媚珠池**，當地漁民則稱呼其為**珍珠江**。當地的採珠事業一直持續至[明朝中葉才日漸式微](../Page/明朝.md "wikilink")。明朝[洪武七年](../Page/洪武.md "wikilink")，因為採珠5個月僅得半斤，官方認為產珠已盡，決定放棄，並且遷往合浦。

1899年，剛租借[新界的](../Page/新界.md "wikilink")[英國人在大埔海東北岸](../Page/英國人.md "wikilink")（[香港回歸紀念塔現址](../Page/香港回歸紀念塔.md "wikilink")；大埔原海岸線約為今汀角路旁邊，現海濱公園當時位處海中心，故回歸塔現址不可能是英人登岸之地）登岸，但是遭到大埔村民抵抗，爆發[新界六日戰](../Page/新界六日戰.md "wikilink")。[英國軍隊遂出動戰艦掩護](../Page/英國軍隊.md "wikilink")，方才可以強行登陸。英國人佔領新界後，大埔海被更改名稱為吐露港。

1937年，香港發生[丁丑風災](../Page/丁丑風災.md "wikilink")，強烈颱風在1937年9月2日凌晨正面吹襲香港，造成廣泛破壞。颱風在吐露港引起逾6米高的風暴潮，大埔一帶傷亡慘重。

在吐露港北面[大尾篤一帶的](../Page/大尾篤.md "wikilink")[船灣](../Page/船灣.md "wikilink")，昔日為港內一個大[海灣](../Page/海灣.md "wikilink")，於1960年代由於香港水源缺乏，[香港政府在船灣興建一條堤壩把海灣完全封閉](../Page/香港政府.md "wikilink")，並且把海灣內海水抽走興建成為[船灣淡水湖](../Page/船灣淡水湖.md "wikilink")。船灣沿岸曾經有多條鄉村，在興建淡水湖時被拆遷。

1982年，為了改善新界東部來往九龍、新界西和港島的交通，香港政府在沙田與大埔之間的吐露港南面沿岸興建[吐露港公路至](../Page/吐露港公路.md "wikilink")1985年為止，並於中文大學教職員宿舍附近挖採泥石作填海工程之用。

2000年，香港政府為[吐露港公路進行擴闊工程](../Page/吐露港公路.md "wikilink")，在[馬料水一帶](../Page/馬料水.md "wikilink")[填海](../Page/填海.md "wikilink")，興建[香港科學園](../Page/香港科學園.md "wikilink")。

2012年，香港政府為了提高[香港土地供應](../Page/香港土地供應.md "wikilink")，建議在[白石角東部臨海地更改劃作](../Page/白石角.md "wikilink")「住宅(乙類)5」用途。[2013年1月](../Page/2013年1月.md "wikilink")，[創建香港文件指出](../Page/創建香港.md "wikilink")，香港欠缺水上活動設施，遊艇會只供富豪享用，即使鄰近有[大美督水上活動中心](../Page/大美督水上活動中心.md "wikilink")，白石角仍可以發展為公共海事設施中心，包括行政樓、水上活動中心、生態旅遊及售票中心，以及船隻存放中心，海上有浮橋供泊船。創建香港向[城市規劃委員會申請將白石角毗鄰香港科學園空地](../Page/城市規劃委員會.md "wikilink")（[天賦海灣及香港科學園之間一幅達](../Page/天賦海灣.md "wikilink")28萬平方米用地，更改劃為「休憩用地」以及「其他指定用途」，註明海事設施中心）及對出吐露港海面改變用途，發展公共海事設施中心，變身成為可以停泊400艘遊艇及存放200艘船的儲存倉。文件指出，創建香港無興趣參與興建或者營運項目，認為香港政府於日後售賣鄰近住宅地時，可以加設條款促使私人發展商興建，落成後由非牟利團體管理，或者使用[民間興建營運後轉移模式發展](../Page/民間興建營運後轉移模式.md "wikilink")，使到設施可以便宜收費，供予大眾享用。有關公眾諮詢期於[2013年2月月中屆滿](../Page/2013年2月.md "wikilink")，收到364份意見，地區人士及[新界漁民聯會均對此計劃表示反對](../Page/新界漁民聯會.md "wikilink")。2013年4月19日，城規會會審議上述計劃\[1\]，其後城規會亦反對創建香港提出的計劃。

## 鄰近主要地方

[Tolo_Harbour_cycling_track_2017.jpg](https://zh.wikipedia.org/wiki/File:Tolo_Harbour_cycling_track_2017.jpg "fig:Tolo_Harbour_cycling_track_2017.jpg")

  - [大埔](../Page/大埔_\(香港\).md "wikilink")
  - [白石角](../Page/白石角.md "wikilink")
  - [大埔滘](../Page/大埔滘.md "wikilink")
  - [汀角](../Page/汀角.md "wikilink")
  - [船灣](../Page/船灣.md "wikilink")
  - [大尾篤](../Page/大尾篤.md "wikilink")
  - [八仙嶺](../Page/八仙嶺.md "wikilink")
  - [沙田](../Page/沙田.md "wikilink")
  - [馬料水](../Page/馬料水.md "wikilink")
  - [白石角](../Page/白石角.md "wikilink")
  - [馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")
  - [烏溪沙](../Page/烏溪沙.md "wikilink")

## 參考注釋

## 外部連結

  - [吐露港在2009年3月的影像](http://www.youtube.com/watch?v=Lro5v9V9t4g)

[Category:香港港灣](../Category/香港港灣.md "wikilink")
[Category:沙田區](../Category/沙田區.md "wikilink")
[Category:大埔區](../Category/大埔區.md "wikilink")

1.  [吐露港建遊艇泊位遭反對](http://hk.news.yahoo.com/吐露港建遊艇泊位遭反對-220311629.html)
    《星島日報》 2013年3月18日