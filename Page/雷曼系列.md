是[育碧软件的](../Page/育碧软件.md "wikilink")[电子游戏系列](../Page/电子游戏.md "wikilink")，由[米歇尔·安西尔](../Page/米歇尔·安西尔.md "wikilink")（Michel
Ancel）设计。

## 系列作品

  - [雷曼](../Page/雷曼_\(游戏\).md "wikilink")（Rayman）
  - 金雷曼（Rayman Gold）
  - [雷曼2：胜利大逃亡](../Page/雷曼2：胜利大逃亡.md "wikilink")（Rayman 2: The Great
    Escape）
  - 雷曼DS版（Rayman DS）
  - 雷曼3：强盗侵袭（Rayman 3: Hoodlum Havoc）
  - 雷曼3：暴徒的反击（Rayman: Hoodlums' Revenge）
  - 雷曼竞技场/雷曼快冲（Rayman Arena／Rayman M/Rayman Rush）
  - [雷曼：疯狂的兔子](../Page/雷曼：疯狂的兔子.md "wikilink")（Rayman Raving
    Rabbids）（[Wii](../Page/Wii.md "wikilink")）
  - 雷曼：疯狂的兔子2（Rayman Raving Rabbids 2）（Wii）
  - 雷曼：疯狂兔子电视聚会（Rayman Raving Rabbids TV Party）（Wii）
  - 雷曼：起源（Rayman Origins）（[PS3](../Page/PS3.md "wikilink")、[Xbox
    360](../Page/Xbox_360.md "wikilink")、[PS
    Vita](../Page/PS_Vita.md "wikilink")、[3DS](../Page/3DS.md "wikilink")、Windows、[Wii](../Page/Wii.md "wikilink")）
  - 雷曼：丛林探险（Rayman Jungle
    Run）（[iOS](../Page/iOS.md "wikilink")、[Android](../Page/Android.md "wikilink")、[Windows
    RT](../Page/Windows_RT.md "wikilink")、[Windows
    Phone](../Page/Windows_Phone.md "wikilink")）
  - 雷曼：传奇（Rayman Legends）（[Wii
    U](../Page/Wii_U.md "wikilink")、PS3、PS4、[Xbox
    360](../Page/Xbox_360.md "wikilink")、Xbox One、PS Vita、Windows）\[1\]
  - 雷曼：競速嘉年華（Rayman Fiesta
    Run）（[iOS](../Page/iOS.md "wikilink")、[Android](../Page/Android.md "wikilink")、[Windows
    RT](../Page/Windows_RT.md "wikilink")、[Windows
    Phone](../Page/Windows_Phone.md "wikilink")）
  - 雷曼：大冒險（Rayman
    Adventures）（[iOS](../Page/iOS.md "wikilink")、[Android](../Page/Android.md "wikilink")、[Apple
    TV 4](../Page/Apple_TV_4.md "wikilink")）
  - 雷曼4 (Rayman 4)

## 参考资料

## 外部链接

  -
  -
[雷曼系列](../Category/雷曼系列.md "wikilink")
[Category:1995年首发的电子游戏系列](../Category/1995年首发的电子游戏系列.md "wikilink")
[Category:平台游戏](../Category/平台游戏.md "wikilink")
[Category:育碧電子遊戲系列](../Category/育碧電子遊戲系列.md "wikilink")
[Category:電子遊戲系列](../Category/電子遊戲系列.md "wikilink")

1.