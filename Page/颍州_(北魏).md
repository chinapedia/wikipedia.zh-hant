**颍州**，[中国](../Page/中国.md "wikilink")[南北朝时设置的](../Page/南北朝.md "wikilink")[州](../Page/州.md "wikilink")。

[北魏](../Page/北魏.md "wikilink")[孝昌四年](../Page/孝昌_\(年号\).md "wikilink")（528年）置，[治所在](../Page/治所.md "wikilink")[汝阴县](../Page/汝阴县_\(秦朝\).md "wikilink")（在今[安徽省](../Page/安徽省.md "wikilink")[阜阳市境](../Page/阜阳市.md "wikilink")）。[北齐废](../Page/北齐.md "wikilink")。[隋朝](../Page/隋朝.md "wikilink")[大业年间](../Page/大业_\(年号\).md "wikilink")，改颍州为[汝阴郡](../Page/汝阴郡_\(曹魏\).md "wikilink")。[唐朝](../Page/唐朝.md "wikilink")[武德四年](../Page/武德.md "wikilink")（621年）置[信州](../Page/信州_\(安徽\).md "wikilink")，六年复改颍州。下领四县：汝阴县、[颍上县](../Page/颍上县.md "wikilink")、[下蔡县](../Page/下蔡县.md "wikilink")、[沈丘县](../Page/沈丘县.md "wikilink")\[1\]。辖境相当今安徽省[阜阳](../Page/阜阳市.md "wikilink")、[颍上](../Page/颍上县.md "wikilink")、[阜南](../Page/阜南县.md "wikilink")、[太和](../Page/太和县.md "wikilink")、[界首](../Page/界首市.md "wikilink")、[临泉等市县地](../Page/临泉县.md "wikilink")。

[北宋](../Page/北宋.md "wikilink")[政和六年](../Page/政和_\(年号\).md "wikilink")（1116年）改为[顺昌府](../Page/顺昌府.md "wikilink")，[金朝时复为颍州](../Page/金朝.md "wikilink")。[元朝时](../Page/元朝.md "wikilink")，属[汝宁府](../Page/汝宁府.md "wikilink")。[明朝](../Page/明朝.md "wikilink")[洪武四年](../Page/洪武.md "wikilink")（1371年）二月，归属[中都](../Page/明中都.md "wikilink")，即[凤阳府](../Page/凤阳府.md "wikilink")。下领两县：颍上县、[太和县](../Page/太和县.md "wikilink")\[2\]。[清朝](../Page/清朝.md "wikilink")[雍正十三年](../Page/雍正.md "wikilink")（1735年），升为[颍州府](../Page/颍州府.md "wikilink")。

## 注释

[Category:北魏的州](../Category/北魏的州.md "wikilink")
[Category:东魏的州](../Category/东魏的州.md "wikilink")
[Category:北齐的州](../Category/北齐的州.md "wikilink")
[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:五代十国的州](../Category/五代十国的州.md "wikilink")
[Category:北宋的州](../Category/北宋的州.md "wikilink")
[Category:金朝的州](../Category/金朝的州.md "wikilink")
[Category:元朝的州](../Category/元朝的州.md "wikilink")
[Category:明朝的州](../Category/明朝的州.md "wikilink")
[Category:清朝的州](../Category/清朝的州.md "wikilink")
[Category:安徽的州](../Category/安徽的州.md "wikilink")
[Category:河南的州](../Category/河南的州.md "wikilink")
[Category:淮南行政区划史](../Category/淮南行政区划史.md "wikilink")
[Category:阜阳行政区划史](../Category/阜阳行政区划史.md "wikilink")
[Category:周口行政区划史](../Category/周口行政区划史.md "wikilink")
[Category:528年建立的行政区划](../Category/528年建立的行政区划.md "wikilink")
[Category:1735年废除的行政区划](../Category/1735年废除的行政区划.md "wikilink")

1.  《[新唐书](../Page/新唐书.md "wikilink")·志第二十八·地理二》颍州汝阴郡，上。本信州，武德四年置，六年更名。土贡：施、绵、糟白鱼。户三万七百七，口二十万二千八百九十。县四：汝阴，紧。武德初有永安、高唐、永乐、清丘、颍阳等县，六年省永安、高唐、永乐，贞观元年省清丘、颍阳，皆入汝阴。南三十五里有椒陂塘，引润水溉田二百顷，永徽中，刺史柳宝积修。颍上。上。下蔡，上。武德四年置涡州，八年州废。西北百二十里有大崇陂，八十里有鸡陂，六十里有黄陂，东北八十里有湄陂，皆隋末废，唐复之，溉田数百顷。沈丘。中。本阝州，领沈丘、宛丘。唐初州废，以宛丘隶陈州，沈丘来属。后省沈丘入汝阴，神龙二年复置......
2.  《[明史](../Page/明史.md "wikilink")·志第十六·地理一》凤阳府......颍州元属汝宁府。洪武四年二月来属。淮河在南，自河南固始县流入，下流合大河入海。又南有汝水，自河南息县流入，经朱皋镇入淮。又北有颍河，自河南沈丘县流入。洪武二十四年，黄河决於河南，由陈州合颍，径太和县，又经州城北，又经颍上县，至寿州同入於淮。永乐九年，河复故道。宣德、正统、成化、正德间，河、颍时通时塞，俗亦称颍为小黄河。西北又有沈丘镇巡检司。东距府四百四十里。领县二：颍上州东南。东有颍河。南有淮河。东北有西肥水。太和州西北。南有颍水，亦名沙河。北有西肥水。又有洪山、北原和二巡检司......