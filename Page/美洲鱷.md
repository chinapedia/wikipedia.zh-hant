**美洲鱷**（學名：**）是[新热带界的一种大型](../Page/新热带界.md "wikilink")[鱷魚](../Page/鱷魚.md "wikilink")，為四種现存的[美洲鱷魚中分布最广泛的一種](../Page/美洲.md "wikilink")，主要分佈在由[北美洲](../Page/北美洲.md "wikilink")[美國](../Page/美國.md "wikilink")[佛羅里達州及](../Page/佛羅里達州.md "wikilink")[墨西哥沿](../Page/墨西哥.md "wikilink")[太平洋到](../Page/太平洋.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")[秘鲁](../Page/秘鲁.md "wikilink")、[委內瑞拉一帶](../Page/委內瑞拉.md "wikilink")，棲息於[鹹](../Page/鹹水.md "wikilink")[淡水交界的](../Page/淡水.md "wikilink")[紅樹林](../Page/紅樹林.md "wikilink")、[沼澤等等的](../Page/沼澤.md "wikilink")[濕地](../Page/濕地.md "wikilink")。在美国境内，只分布在[佛罗里达州南部](../Page/佛罗里达州.md "wikilink")，估计数量为2000头。美洲鳄的体型比其他鳄鱼都大，南部和中部美洲的雄性体长可达6.1米。

## 描述

[Crocodylus_acutus_in_La_Manzanilla.jpg](https://zh.wikipedia.org/wiki/File:Crocodylus_acutus_in_La_Manzanilla.jpg "fig:Crocodylus_acutus_in_La_Manzanilla.jpg")
与其他所有[鳄鱼一样](../Page/鳄目.md "wikilink")，美洲鳄是一个[四足动物](../Page/四足动物.md "wikilink")，有四条短且分叉的腿，一个长且强有力的尾巴，背部和尾部布满[鳞甲](../Page/鳞.md "wikilink")。\[1\]口鼻部较长，有一对强壮的颚骨。眼睛有[瞬膜和](../Page/瞬膜.md "wikilink")[泪器保护](../Page/泪器.md "wikilink")，泪器用以产生眼泪。

[鼻孔](../Page/鼻孔.md "wikilink")、[眼睛和](../Page/眼睛.md "wikilink")[耳朵都位于其头部顶部](../Page/耳朵.md "wikilink")，因此身体的其余部分可隐蔽水下利于突袭。\[2\][伪装有助于它们捕食猎物](../Page/伪装.md "wikilink")。其口鼻部比[美国短吻鳄相对长且窄](../Page/美国短吻鳄.md "wikilink")，比[奥利诺科鳄宽](../Page/奥利诺科鳄.md "wikilink")。\[3\]美洲鳄比起其他深色鳄鱼的颜色要浅并且灰一些。这个物种通常用腹部匍匐而行，它们也可以用脚行走。\[4\]体型大的个体速度可以达到每小时10英里（16公里）\[5\]它们通过移动身体和尾巴的方式游泳，速度可达每小时20英里，但是不能长时保持这个速度。\[6\]

美洲鳄比美国短吻鳄对于低温更敏感。\[7\]美国短吻鳄可以在7.2 °C (45.0
°F)的环境下呆一阵子，美洲鳄却会死亡。\[8\]美洲鳄的生长速度比美国短吻鳄快，并且更能适应咸水。\[9\]

其他鳄鱼有时通过鸟类来清理寄生虫，美洲鳄则更多地依靠鱼。\[10\]

## 参考资料

[Category:鱷科](../Category/鱷科.md "wikilink")
[Category:海生爬行動物](../Category/海生爬行動物.md "wikilink")

1.

2.
3.  [1](http://en.wikipedia.org/w/index.php?title=American_crocodile&action=edit&section=1)
    (2011).

4.

5.

6.

7.

8.
9.
10.