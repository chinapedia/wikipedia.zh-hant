[一级方程式赛车](../Page/一级方程式赛车.md "wikilink")**馬來西亞大獎賽**从1999年起开始举办至今\[1\]\[2\]。

  - 地点:1999年–2017年:[雪邦國際賽道](../Page/雪邦國際賽道.md "wikilink")(位于[吉隆坡郊区](../Page/吉隆坡.md "wikilink"))。

馬來西亞大獎賽历届冠军:

| 年                                              | 主車手                                                                                   | 国家                               | 車隊名稱                                   | 报告                                       |
| ---------------------------------------------- | ------------------------------------------------------------------------------------- | -------------------------------- | -------------------------------------- | ---------------------------------------- |
| [2017年](../Page/2017年世界一级方程式锦标赛.md "wikilink") | [马克斯·维斯塔潘](../Page/马克斯·维斯塔潘.md "wikilink")                                            | [荷兰](../Page/荷兰.md "wikilink")   | [红牛车队](../Page/红牛车队.md "wikilink")     | [报告](../Page/2017年馬來西亞大獎賽.md "wikilink") |
| [2016年](../Page/2016年世界一级方程式锦标赛.md "wikilink") | [丹尼尔·里卡多](../Page/丹尼尔·里卡多.md "wikilink")                                              | [澳洲](../Page/澳洲.md "wikilink")   | [红牛车队](../Page/红牛车队.md "wikilink")     | [报告](../Page/2016年馬來西亞大獎賽.md "wikilink") |
| [2015年](../Page/2015年世界一级方程式锦标赛.md "wikilink") | [塞巴斯蒂安·维特尔](../Page/塞巴斯蒂安·维特尔.md "wikilink")                                          | [德国](../Page/德国.md "wikilink")   | [法拉利车队](../Page/法拉利车队.md "wikilink")   | [报告](../Page/2015年馬來西亞大獎賽.md "wikilink") |
| [2014年](../Page/2014年世界一级方程式锦标赛.md "wikilink") | [路易斯·汉密尔顿](../Page/路易斯·汉密尔顿.md "wikilink")                                            | [英国](../Page/英国.md "wikilink")   | [梅赛德斯车队](../Page/梅赛德斯车队.md "wikilink") | [报告](../Page/2014年馬來西亞大獎賽.md "wikilink") |
| [2013年](../Page/2013年世界一级方程式锦标赛.md "wikilink") | [塞巴斯蒂安·维特尔](../Page/塞巴斯蒂安·维特尔.md "wikilink")                                          | [德国](../Page/德国.md "wikilink")   | [红牛车队](../Page/红牛车队.md "wikilink")     | [报告](../Page/2013年馬來西亞大獎賽.md "wikilink") |
| [2012年](../Page/2012年世界一级方程式锦标赛.md "wikilink") | [费尔南多·阿隆索](../Page/费尔南多·阿隆索.md "wikilink")                                            | [西班牙](../Page/西班牙.md "wikilink") | [法拉利车队](../Page/法拉利车队.md "wikilink")   | [报告](../Page/2012年馬來西亞大獎賽.md "wikilink") |
| [2011年](../Page/2011年世界一级方程式锦标赛.md "wikilink") | [塞巴斯蒂安·维特尔](../Page/塞巴斯蒂安·维特尔.md "wikilink")                                          | [德国](../Page/德国.md "wikilink")   | [红牛车队](../Page/红牛车队.md "wikilink")     | [报告](../Page/2011年馬來西亞大獎賽.md "wikilink") |
| [2010年](../Page/2010年世界一级方程式锦标赛.md "wikilink") | [塞巴斯蒂安·维特尔](../Page/塞巴斯蒂安·维特尔.md "wikilink")                                          | [德国](../Page/德国.md "wikilink")   | [红牛车队](../Page/红牛车队.md "wikilink")     | [报告](../Page/2010年馬來西亞大獎賽.md "wikilink") |
| [2009年](../Page/2009年世界一级方程式锦标赛.md "wikilink") | [简森·巴顿](../Page/简森·巴顿.md "wikilink")                                                  | [英国](../Page/英国.md "wikilink")   | [布朗车队](../Page/布朗车队.md "wikilink")     | [报告](../Page/2009年馬來西亞大獎賽.md "wikilink") |
| [2008年](../Page/2008年世界一级方程式锦标赛.md "wikilink") | [基米·莱科宁](../Page/基米·莱科宁.md "wikilink")                                                | [芬兰](../Page/芬兰.md "wikilink")   | [法拉利车队](../Page/法拉利车队.md "wikilink")   | [报告](../Page/2008年馬來西亞大獎賽.md "wikilink") |
| [2007年](../Page/2007年世界一级方程式锦标赛.md "wikilink") | [费尔南多·阿隆索](../Page/费尔南多·阿隆索.md "wikilink")                                            | [西班牙](../Page/西班牙.md "wikilink") | [迈凯伦车队](../Page/迈凯伦车队.md "wikilink")   | [报告](../Page/2007年馬來西亞大獎賽.md "wikilink") |
| [2006年](../Page/2006年一级方程式赛车比赛.md "wikilink")  | [-{zh-hans:詹卡洛·费斯切拉;zh-hk:雲加羅·費斯卓拿;zh-tw:尚卡羅·費希切拉;}-](../Page/詹卡洛·费斯切拉.md "wikilink") | [意大利](../Page/意大利.md "wikilink") | [雷诺车队](../Page/雷诺车队.md "wikilink")     | [报告](../Page/2006年馬來西亞大獎賽.md "wikilink") |
| [2005年](../Page/2005年一级方程式赛车比赛.md "wikilink")  | [费尔南多·阿隆索](../Page/费尔南多·阿隆索.md "wikilink")                                            | [西班牙](../Page/西班牙.md "wikilink") | [雷诺车队](../Page/雷诺车队.md "wikilink")     | [报告](../Page/2005年馬來西亞大獎賽.md "wikilink") |
| 2004年                                          | [迈克尔·舒马赫](../Page/迈克尔·舒马赫.md "wikilink")                                              | [德国](../Page/德国.md "wikilink")   | [法拉利車隊](../Page/法拉利車隊.md "wikilink")   | [报告](../Page/2004年馬來西亞大獎賽.md "wikilink") |
| 2003年                                          | [奇米·雷克南](../Page/奇米·雷克南.md "wikilink")                                                | [芬兰](../Page/芬兰.md "wikilink")   | [邁凱輪車隊](../Page/邁凱輪車隊.md "wikilink")   | [报告](../Page/2003年馬來西亞大獎賽.md "wikilink") |
| 2002年                                          | [拉爾夫·舒馬赫](../Page/拉爾夫·舒馬赫.md "wikilink")                                              | [德国](../Page/德国.md "wikilink")   | [威廉姆斯車隊](../Page/威廉姆斯車隊.md "wikilink") | [报告](../Page/2002年馬來西亞大獎賽.md "wikilink") |
| 2001年                                          | [迈克尔·舒马赫](../Page/迈克尔·舒马赫.md "wikilink")                                              | [德国](../Page/德国.md "wikilink")   | [法拉利車隊](../Page/法拉利車隊.md "wikilink")   | [报告](../Page/2001年馬來西亞大獎賽.md "wikilink") |
| 2000年                                          | [迈克尔·舒马赫](../Page/迈克尔·舒马赫.md "wikilink")                                              | [德国](../Page/德国.md "wikilink")   | [法拉利車隊](../Page/法拉利車隊.md "wikilink")   | [报告](../Page/2000年馬來西亞大獎賽.md "wikilink") |
| 1999年                                          | [埃迪·埃尔文](../Page/埃迪·埃尔文.md "wikilink")                                                | [爱尔兰](../Page/爱尔兰.md "wikilink") | [法拉利車隊](../Page/法拉利車隊.md "wikilink")   | [报告](../Page/1999年馬來西亞大獎賽.md "wikilink") |

## 赛季结束

马来西亚政府宣布2017年为一级方程式最后赛季。等2017赛季结束后，2018年开始就不会在马来西亚举办一级方程式。

## 参考资料

<https://www.formula1.com/en/latest/headlines/2017/4/2017-race-to-be-malaysias-f1-farewell.html>

## 外部链接

  - [雪邦国际赛道官方网站](http://www.malaysiangp.com.my/)

[category:一級方程式大獎賽](../Page/category:一級方程式大獎賽.md "wikilink")

[Category:馬來西亞大獎賽](../Category/馬來西亞大獎賽.md "wikilink")

1.
2.