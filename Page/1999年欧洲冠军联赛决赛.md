**[1998/99赛季](../Page/1998–99年欧洲冠军联赛.md "wikilink")[欧洲冠军联赛](../Page/欧洲冠军联赛.md "wikilink")**的决赛，於1999年5月26日在[西班牙](../Page/西班牙.md "wikilink")[加泰羅尼亞](../Page/加泰羅尼亞.md "wikilink")[巴塞罗那的](../Page/巴塞罗那.md "wikilink")[诺坎普球场举行](../Page/诺坎普球场.md "wikilink")，最后[英超的](../Page/英超.md "wikilink")[曼彻斯特联在伤停补时阶段连续攻入两球](../Page/曼彻斯特联.md "wikilink")，以2：1击败[德甲的](../Page/德甲.md "wikilink")[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")，历史上第二次获得欧洲冠军杯。这场比赛的跌宕起伏经常被称之为足球经典，甚至是超越足球与体育范畴的奇迹，经常在各项评选中获得提名。当值主裁著名裁判[皮耶路易吉·科利纳就称这是他职业裁判生涯中最难忘的比赛](../Page/皮耶路易吉·科利纳.md "wikilink")，而对于绝大多数曼联的球迷这则是俱乐部一百多年历史上最激动人心的奇迹时刻。

## 比赛经过

### 賽前

比賽前，曼聯已經完成了國內聯賽、盃賽雙冠王，而拜仁提前三輪奪得聯賽冠軍，也進入了兩周後舉行的德國盃的決賽。兩支球隊的目標都是歐洲足壇最高榮譽的「[三冠王](../Page/三冠王_\(足球\).md "wikilink")」。然而，曼聯方面卻接連傳出不利消息。首先，由於上一輪四強戰中與祖雲達斯殊死血戰，主力正選球員隊長[堅尼](../Page/堅尼.md "wikilink")、中場大腦[史高斯俱因累積黃牌而雙雙停賽](../Page/史高斯.md "wikilink")。繼而慣常搭檔[史譚鎮守中路的](../Page/史譚.md "wikilink")[貝治又因傷缺陣](../Page/貝治.md "wikilink")，攻守兩端同時受創。加上分組賽時兩戰皆未分勝負，曼聯對陣德國球隊往績也毫不光采，故在賽前外界多數認為拜仁稍為佔優。

在人員缺損之下，[費格遜仍然排出了慣常的](../Page/費格遜.md "wikilink") 4-4-2
陣式，由[畢特代替](../Page/畢特.md "wikilink")[堅尼出任防中](../Page/堅尼.md "wikilink")，又把唯一在傳送方面能夠與[史高斯比肩的黃金右腳](../Page/史高斯.md "wikilink")[碧咸移入中路校炮](../Page/碧咸.md "wikilink")，其空缺由左翼[傑斯調至右側頂替](../Page/傑斯.md "wikilink")，左翼位置則以當季來投的[布隆基斯出任](../Page/布隆基斯.md "wikilink")，後防中路以[朗尼·莊臣代替](../Page/朗尼·莊臣.md "wikilink")[貝治](../Page/貝治.md "wikilink")，伙拍[史譚鎮守](../Page/史譚.md "wikilink")。在人員上作出了大幅度調整。反觀拜仁方面，只有[史高爾稍微帶傷](../Page/史高爾.md "wikilink")，暫時充當後備，堪稱人腳完整。

### 上半场

比赛开始仅六分钟，曼联后卫[朗尼·莊臣在禁区线外中路左侧对拜仁前锋](../Page/朗尼·莊臣.md "wikilink")[卡斯滕·扬克尔犯规](../Page/卡斯滕·扬克尔.md "wikilink")，拜仁获得位置较好的任意球。[巴士拿踢出一个低平球](../Page/巴士拿.md "wikilink")，看似被人墙折射后，以一个出色的弧线打入曼联球门左下死角，曼联著名门将[彼得·舒梅切尔都没有扑救的机会](../Page/彼得·舒梅切尔.md "wikilink")。

拜仁早早获得领先后，显示出了明显的优势。曼联由于中场两位绝对主力球员俱乐部队长[罗伊·基恩和](../Page/罗伊·基恩.md "wikilink")[保罗·斯科尔斯在本场比赛前累计黄牌而停赛](../Page/保罗·斯科尔斯.md "wikilink")，在中场争夺中力不从心，代替他们出场的[尼基·巴特和](../Page/尼基·巴特.md "wikilink")[杰斯普·布鲁姆奎斯特](../Page/杰斯普·布鲁姆奎斯特.md "wikilink")，面对著名自由人[洛塔尔·马特乌斯和中场悍将](../Page/洛塔尔·马特乌斯.md "wikilink")[斯特凡·埃芬伯格的有力断抢发挥大受影响](../Page/斯特凡·埃芬伯格.md "wikilink")。只有[瑞恩·吉格斯和](../Page/瑞恩·吉格斯.md "wikilink")[大卫·贝克汉姆尚能发挥自己的特长](../Page/大卫·贝克汉姆.md "wikilink")，但是他们的突破和传中都没有造成射门机会。

### 下半场

下半场开始后，场上局面仍然是拜仁占据优势。第67分钟，曼联主帅[亚历克斯·弗格森换上了当时](../Page/亚历克斯·弗格森.md "wikilink")33岁的老将[特迪·谢林汉姆](../Page/特迪·谢林汉姆.md "wikilink")，试图加强进攻。拜仁主帅[奥特马·希斯菲尔德则换上](../Page/奥特马·希斯菲尔德.md "wikilink")[梅梅特·绍尔加强中场组织](../Page/梅梅特·绍尔.md "wikilink")，后者几分钟后在巴斯勒突破后获得破门良机，但是他20码外的吊射被一米九三的舒梅切尔用指尖碰触之后，打在了曼联球门立柱上弹回，被舒梅切尔如释重负的抱住。81分钟，曼联再换上[索尔斯克亚](../Page/索尔斯克亚.md "wikilink")，打起三前锋开始大举进攻，但却又是拜仁获得破门良机，高中锋扬克尔极其灵巧的一个倒钩射门，球重重的击中球门横梁弹回，曼联再次逃过一劫。虽然此后谢林汉姆和索尔斯克亚的射门连续迫使拜仁门将[奥利弗·卡恩做出扑救](../Page/奥利弗·卡恩.md "wikilink")，而拜仁也逐渐开始退缩防守，但是直到比赛最后几分钟拜仁仍然保持着领先优势，距离冠军只有咫尺之遥。第89分钟，希斯菲尔德做出最后的换人，用边卫[哈桑·萨利哈米季奇换下全场表现最为出色的巴斯勒](../Page/哈桑·萨利哈米季奇.md "wikilink")，加强防守的同时耗费掉最后几分钟的比赛时间。

### 伤停补时

第四官员此时举牌示意[伤停补时三分钟](../Page/伤停补时.md "wikilink")。[欧洲足联主席](../Page/欧洲足联.md "wikilink")从主席台站起来，准备坐电梯下看台为拜仁慕尼黑颁奖，在走过曼联名宿[博比·查尔顿面前时](../Page/博比·查尔顿.md "wikilink")，还对他说了一句“抱歉”以示安慰，而欧足联的工作人员已经开始将拜仁色彩的丝带系到著名的“大耳朵”奖杯上。而德国的电视评论则开始引用退役的英格兰前锋[加里·莱因克尔的名言](../Page/加里·莱因克尔.md "wikilink")：“足球是一项简单的运动，22个男人追着皮球跑90分钟，最终德国人获胜。”

但是此时曼联通过右后卫[加利·尼維利的左路传中获得角球](../Page/加利·尼維利.md "wikilink")，由[贝克汉姆主罚](../Page/贝克汉姆.md "wikilink")，门将[舒梅切尔也冲到了对手的禁区](../Page/舒梅切尔.md "wikilink")，试图重现他多年前神奇头球扳平的一幕。此时英国著名足球評述員，自己就是曼联球迷的在电视上说：“曼联能进球吗？他们总能进球！（Can
Manchester United score? They always
score.）”碧咸开出角球后，舒梅切尔巨大的身躯在禁区中路造成了一片混乱，结果谁都没有争顶到，[德怀特·约克在远点勉强将球碰回中路](../Page/德怀特·约克.md "wikilink")，但被拜仁防守的解围。不过球未踢远，大禁区线上的[吉格斯迎球右脚凌空扫射](../Page/吉格斯.md "wikilink")，没有打上力量，皮球来到小禁区外的谢林汉姆的面前。谢林汉姆顺势180度转身射门，球打入拜仁球门右下死角，[奥利弗·卡恩毫无办法](../Page/奥利弗·卡恩.md "wikilink")，曼联全场落后了84分钟后，在伤停补时第一分钟扳平比分。

拜仁中场开球，被曼联断下后长传到前场左路的[索尔斯克亚](../Page/索尔斯克亚.md "wikilink")。索尔斯克亚面对拜仁后卫[萨穆埃尔·库福尔的防守](../Page/萨穆埃尔·库福尔.md "wikilink")，坚决下底，但传中球被挡后曼联再次获得角球。贝克汉姆再次开出角球，谢林汉姆在中路前点高高跃起蹭到皮球，球改变方向后落到了小禁区线后点的索尔斯克亚面前。索尔斯克亚用“大脚趾”\[1\]将球凌空垫入球门，站在后门柱补位的拜仁后卫[迈克尔·塔纳特眼看球从头顶飞过而毫无办法](../Page/迈克尔·塔纳特.md "wikilink")。提尔德斯利激动万分的喊道：“……索尔斯克亚尔赢了它（……and
Solskjær has won it.）！”“曼联終於來到了應許之地！（Manchester United have reached the
Promised Land.）”“曼聯再次稱霸歐洲，而且永遠再沒有人能夠比他們更戲劇性地贏下歐聯決賽。（Manchester United
are the champion of Europe again and nobody will ever win the European
Cup final more dramatically than this.）”

而欧足联主席约翰森此时才从看台出入口走出来，后来他回忆当时第一个念头就是：“太不可思议了！赢球的在痛哭，输球的在跳舞！”拜仁多名球员，包括前锋扬克尔、中卫库福尔和之前已经被换下场的马特乌斯，接受不了如此残酷的打击，当场崩溃，甚至失声痛哭倒地不起。主裁科利纳甚至拉起了几名已经站不起来的拜仁球员，让比赛短暂继续，但很快吹响了终场哨音。

## 赛事重要性

这场比赛之前，三个欧洲三冠王得主的都是改制前的欧洲杯冠军，而且也从未有过任何一支来自于[欧洲五大联赛的球队能够完成此壮举](../Page/欧洲五大联赛.md "wikilink")。最终，曼联第一个创造了这一非凡的成绩。而拜仁受此打击，在两周后的德国杯决赛的互射点球中负于[云达不莱梅](../Page/云达不莱梅.md "wikilink")，最终只获得了联赛冠军。

同年6月12日，曼联主帅[亚历克斯·弗格森因为对足球的贡献被授予](../Page/亚历克斯·弗格森.md "wikilink")[下级勋位爵士](../Page/下级勋位爵士.md "wikilink")，从此被尊称为弗格森爵士。

这个冠军杯是自从1985年[海塞尔惨案后曾被无限期禁赛的英格兰球队第一次打入决赛和获得的第一个欧洲冠军](../Page/海塞尔惨案.md "wikilink")，也是英格兰足球重新崛起的标志之一。

这支曼联常常被认为是足球史上最伟大的俱乐部球队之一，在赛后成为众多讨论的话题。\[2\]在2002年英国电视台举行的“”评选中名列第四，为所有俱乐部足球之最。\[3\]

这是[欧洲冠军联赛](../Page/欧洲冠军联赛.md "wikilink")1992年开始采用分组循环和淘汰赛混合赛制后第一次在决赛中出现反败为胜的逆转。同时，曼联也是历史上第一支以非国内联赛冠军身份（1997/98赛季曼联只获得[英超亚军](../Page/英超.md "wikilink")）参加改制后的冠军联赛并夺得冠军的球队。

## 轶事

颁奖仪式上，曼联的场上队长[舒梅切尔与主教练](../Page/舒梅切尔.md "wikilink")[弗格森一起举起了冠军奖杯](../Page/弗格森.md "wikilink")，这也是曼联历史上最出色的门将之一舒梅切尔最后一次代表曼联出赛。因累积两张黄牌停赛的俱乐部队长[基恩和](../Page/罗伊·基恩.md "wikilink")[斯科尔斯穿西装领取了奖牌](../Page/保罗·斯科尔斯.md "wikilink")。

曼联主教练弗格森在这场比赛前后的很多言论被传为经典。他在中场休息时对队员说：“如果你们输了，你可以走到离冠军奖杯六英寸的地方，却连碰它一下的资格都没有！”据多名队员回忆，这是他们在最后几分钟完全疯狂的进攻的动力。弗格森在赛后即时采访中的第一句话也成为名言：“Football,
bloody hell.”

比赛当天还是已故曼联传奇主帅[马特·巴斯比爵士的九十周年诞辰纪念日](../Page/马特·巴斯比.md "wikilink")。弗格森事后多次将这次意义特殊的胜利与巴斯比爵士联系起来，认为他“在天上一定干了不少事”（done
a lot of kicking up there）。

[索尔斯克亚攻入致胜球之后](../Page/索尔斯克亚.md "wikilink")，双膝跪地长距离滑行庆祝的场景成为后来标志性的镜头之一。索尔斯克亚因滑鏟扭傷韌帶，需要休息2至3周，並缺陣國家隊的比賽。后来蘇斯克查职业生涯中多次受膝伤困扰直至2007年退役，曾经有足球记者质疑是否与这次著名的庆祝有关，但他本人認為兩者無關。到[2008年欧洲冠军联赛决赛前](../Page/2008年欧洲冠军联赛决赛.md "wikilink")，《泰晤士报》还特地发文，评论索尔斯克亚用几十秒钟的时间，将自己永远留在了曼联球迷的心中。

欧洲冠军杯是拜仁名将[世界杯冠军](../Page/世界杯.md "wikilink")、[世界足球先生](../Page/世界足球先生.md "wikilink")[马特乌斯耀眼的足球生涯中唯一没有获得过的重要荣誉](../Page/马特乌斯.md "wikilink")，曼联获胜后镜头停留在他伤感的脸上。解说提尔德斯利说：“想象一下马特乌斯这时候在想什么！不过满怀敬意的说，谁在乎呢？”

## 比赛综览

|                |               |                            |               |                  |               |                   |                 |                |                                                 |                |                                 |                                |                                 |                                 |                                 |                  |               |                   |                 |                |                                                 |
| -------------- | ------------- | -------------------------- | ------------- | ---------------- | ------------- | ----------------- | --------------- | -------------- | ----------------------------------------------- | -------------- | ------------------------------- | ------------------------------ | ------------------------------- | ------------------------------- | ------------------------------- | ---------------- | ------------- | ----------------- | --------------- | -------------- | ----------------------------------------------- |
| {{Football kit | pattern_la = | pattern_b = _collarwhite | pattern_ra = | leftarm = FF0000 | body = FF0000 | rightarm = FF0000 | shorts = FFFFFF | socks = FFFFFF | title = [曼彻斯特联](../Page/曼彻斯特联.md "wikilink") }} | {{Football kit | pattern_la = _FCBAYERN_9899t | pattern_b = _FCBAYERN_9899t | pattern_ra = _FCBAYERN_9899t | pattern_sh = _FCBAYERN_9899t | pattern_so = _FCBAYERN_9899t | leftarm = FFFFFF | body = FFFFFF | rightarm = FFFFFF | shorts = FFFFFF | socks = 8b7f84 | title = [拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink") }} |

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 0%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td><p><strong>曼彻斯特联：</strong></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>守门员</p></td>
</tr>
<tr class="even">
<td><p>右后卫</p></td>
</tr>
<tr class="odd">
<td><p>中后卫</p></td>
</tr>
<tr class="even">
<td><p>中后卫</p></td>
</tr>
<tr class="odd">
<td><p>左后卫</p></td>
</tr>
<tr class="even">
<td><p>右前卫</p></td>
</tr>
<tr class="odd">
<td><p>中前卫</p></td>
</tr>
<tr class="even">
<td><p>中前卫</p></td>
</tr>
<tr class="odd">
<td><p>左前卫</p></td>
</tr>
<tr class="even">
<td><p>前锋</p></td>
</tr>
<tr class="odd">
<td><p>前锋</p></td>
</tr>
<tr class="even">
<td><p><strong>替补：</strong></p></td>
</tr>
<tr class="odd">
<td><p>守门员</p></td>
</tr>
<tr class="even">
<td><p>后卫</p></td>
</tr>
<tr class="odd">
<td><p>后卫</p></td>
</tr>
<tr class="even">
<td><p>后卫</p></td>
</tr>
<tr class="odd">
<td><p>中场</p></td>
</tr>
<tr class="even">
<td><p>前锋</p></td>
</tr>
<tr class="odd">
<td><p>前锋</p></td>
</tr>
<tr class="even">
<td><p><strong>主教练：</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿历克斯·弗格森.md" title="wikilink">阿历克斯·弗格森</a></p></td>
</tr>
</tbody>
</table></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Man_Utd_vs_Bayern_Munich_1999-05-26.svg" title="fig:Man_Utd_vs_Bayern_Munich_1999-05-26.svg">Man_Utd_vs_Bayern_Munich_1999-05-26.svg</a></p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p><strong>拜仁幕尼黑：</strong></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>守门员</p></td>
</tr>
<tr class="even">
<td><p>自由人</p></td>
</tr>
<tr class="odd">
<td><p>右后卫</p></td>
</tr>
<tr class="even">
<td><p>中后卫</p></td>
</tr>
<tr class="odd">
<td><p>中后卫</p></td>
</tr>
<tr class="even">
<td><p>左后卫</p></td>
</tr>
<tr class="odd">
<td><p>中前卫</p></td>
</tr>
<tr class="even">
<td><p>中前卫</p></td>
</tr>
<tr class="odd">
<td><p>右边锋</p></td>
</tr>
<tr class="even">
<td><p>中锋</p></td>
</tr>
<tr class="odd">
<td><p>左边锋</p></td>
</tr>
<tr class="even">
<td><p><strong>替补：</strong></p></td>
</tr>
<tr class="odd">
<td><p>守门员</p></td>
</tr>
<tr class="even">
<td><p>后卫</p></td>
</tr>
<tr class="odd">
<td><p>中场</p></td>
</tr>
<tr class="even">
<td><p>中场</p></td>
</tr>
<tr class="odd">
<td><p>中场</p></td>
</tr>
<tr class="even">
<td><p>中场</p></td>
</tr>
<tr class="odd">
<td><p>前锋</p></td>
</tr>
<tr class="even">
<td><p><strong>主教练：</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥特马·希斯菲尔德.md" title="wikilink">奥特马·希斯菲尔德</a></p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p><br />
<strong>本场最佳球员：</strong><br />
 <br />
<br />
<strong>巡边员：</strong><br />
 <br />
 <br />
</p></td>
</tr>
</tbody>
</table>

## 注释列表

## 参见

  - [1998/99赛季欧洲冠军联赛](../Page/1998/99赛季欧洲冠军联赛.md "wikilink")
  - [曼彻斯特联队历史](../Page/曼彻斯特联队历史.md "wikilink")
  - [三冠王 (足球)](../Page/三冠王_\(足球\).md "wikilink")

[UEFA Champions League Final](../Category/1999年足球.md "wikilink")
[Category:欧洲冠军联赛决赛](../Category/欧洲冠军联赛决赛.md "wikilink")
[Category:曼彻斯特联比赛](../Category/曼彻斯特联比赛.md "wikilink")
[Category:拜仁慕尼黑比赛](../Category/拜仁慕尼黑比赛.md "wikilink")

1.  2008年退役后接受采访时，索尔斯克亚称是用 "my big toe" 捅射的。
2.
3.  前三中的两个是英格兰国家队在1966年和2001年击败德国队，第一是[雷德格雷夫爵士在连续五届奥运会中夺得金牌](../Page/史蒂芬·雷德格雷夫.md "wikilink")。