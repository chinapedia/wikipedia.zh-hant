[May_15_Incident.jpg](https://zh.wikipedia.org/wiki/File:May_15_Incident.jpg "fig:May_15_Incident.jpg")[每日新聞社對](../Page/每日新聞.md "wikilink")5月15日事件的描述和[首相](../Page/日本內閣總理大臣.md "wikilink")[犬養毅遭到暗殺的報導](../Page/犬養毅.md "wikilink")\]\]

**五一五事件**是1932年（[大日本帝國](../Page/大日本帝國.md "wikilink")[昭和](../Page/昭和.md "wikilink")7年）5月15日以[大日本帝國海軍](../Page/大日本帝國海軍.md "wikilink")[少壯派軍人為首](../Page/少壯派.md "wikilink")，闖入[总理大臣官邸](../Page/总理大臣官邸.md "wikilink")，刺殺了[護憲運動領導者](../Page/護憲運動.md "wikilink")[犬養毅](../Page/犬養毅.md "wikilink")[首相的暴亂事件](../Page/日本內閣總理大臣.md "wikilink")。

## 背景

五一五事件是發生於1932年（昭和7年）5月15日由[日本](../Page/日本.md "wikilink")[帝國海軍基層軍官發動的流產](../Page/大日本帝國海軍.md "wikilink")[政變](../Page/政變.md "wikilink")。同時參與政變者還有[日本](../Page/日本.md "wikilink")[帝國陸軍候補生與](../Page/大日本帝國陸軍.md "wikilink")的殘存分子。

在[倫敦海軍條約簽署後](../Page/倫敦海軍條約.md "wikilink")，[日本](../Page/日本.md "wikilink")[海軍的規模遭到更進一步的限制](../Page/海軍.md "wikilink")，引起基層官兵的不滿，興起推翻[政府的風潮與運動](../Page/日本政府.md "wikilink")，並改為由[軍法統治](../Page/軍法.md "wikilink")。這項運動與[日本帝國陸軍內部的秘密組織](../Page/大日本帝國.md "wikilink")[櫻會的宗旨目的不謀而合](../Page/櫻會.md "wikilink")。這些海軍軍官與[井上日昭以及由井上所帶領的血盟團搭上線](../Page/井上日昭.md "wikilink")，他們認同井上日昭對於發起[昭和復興的看法](../Page/昭和復興.md "wikilink")，並認為「剷除」[政府首長與](../Page/政府.md "wikilink")[財閥人物是有其必要的](../Page/財閥.md "wikilink")。

1932年3月，爆發，井上的人馬原本預定大規模刺殺二十個目標人物，不過最後只刺殺了前任的[大藏大臣](../Page/大藏大臣.md "wikilink")、[立憲民政黨高层](../Page/立憲民政黨.md "wikilink")，以及[三井集團的局長](../Page/三井集團.md "wikilink")[團琢磨](../Page/團琢磨.md "wikilink")。

## 計劃與过程

1932年5月15日，日本帝國海軍軍官，日本陸軍候補生，以及民間右翼分子（包括[大川周明](../Page/大川周明.md "wikilink")、[頭山滿等人](../Page/頭山滿.md "wikilink")）將策劃已久的暗殺行動重新搬上檯面，以酬血盟團未竟之志。

當時的政變主謀[三上卓將參與者分成三隊](../Page/三上卓.md "wikilink")，分別襲擊[總理大臣官邸](../Page/總理大臣官邸.md "wikilink")、[内大臣](../Page/内大臣.md "wikilink")[牧野伸显官邸](../Page/牧野伸显.md "wikilink")、執政黨[立憲政友會的總部](../Page/立憲政友會.md "wikilink")、[三菱銀行及](../Page/三菱銀行.md "wikilink")[警視廳還有幾個](../Page/警視廳.md "wikilink")[东京都的變電所](../Page/东京都.md "wikilink")，有意使首都東京陷入混亂的狀況。

### 總理官邸

[Inukai_Tsuyoshi.jpg](https://zh.wikipedia.org/wiki/File:Inukai_Tsuyoshi.jpg "fig:Inukai_Tsuyoshi.jpg")

當時的内阁总理大臣[犬养毅就是被](../Page/犬养毅.md "wikilink")11名廿歲出頭的海軍軍官在總理官邸刺殺。臨終前的遺言「有話好說」（）以及暴徒的答覆「毋須贅言，下手！」（）成為著名語錄。早先的刺殺計畫還包括刺殺著名的諧星[查理·卓別林](../Page/查理·卓別林.md "wikilink")，因为他是“在日本流布颓废文化的元凶”，當時他剛好拜訪日本。不過就在犬养毅不幸遇害的同時，他的三子[犬養健正在跟卓別林觀賞](../Page/犬養健.md "wikilink")[相撲而逃過一劫](../Page/相撲.md "wikilink")。

### 總理官邸以外

[Nobuaki_Makino_in_later_years.jpg](https://zh.wikipedia.org/wiki/File:Nobuaki_Makino_in_later_years.jpg "fig:Nobuaki_Makino_in_later_years.jpg")

暴徒也攻擊了内大臣[牧野伸显的住處](../Page/牧野伸显.md "wikilink")，還有[警视厅](../Page/警视厅.md "wikilink")，元老、[立憲政友会前黨魁](../Page/立憲政友会.md "wikilink")[西園寺公望的住處與辦公室](../Page/西園寺公望.md "wikilink")，對[三菱银行還有幾個](../Page/三菱银行.md "wikilink")[东京周围變電所投擲手榴彈](../Page/东京.md "wikilink")。

## 審判

撇開殺害首相不談，由于政变规模小，預謀的政變到頭來還是一無所有；又缺乏建立政权的具体计划，所以完全失敗。發動政變的暴徒事後搭乘計程車到警察總部，被[憲兵隊圍起來之後束手就擒](../Page/日本憲兵.md "wikilink")。

十一個殺害犬養的兇手遭到軍法起訴；但是，在審判前，一份由三十五萬人以鮮血署名的請願書被送到法庭，請願書是由全日本各地的同情兇手的民眾發起簽署，請求法庭從寬發落。在審判過程中，兇手們反而利用法庭作為宣傳舞台，「弘揚」他們一片對[天皇的赤誠與耿耿忠心](../Page/天皇.md "wikilink")，激起大眾更多的同情心，呼籲改革政府與經濟。除了請願書之外，法庭還收到另一份求情書，是由十一位[新潟縣的年輕人寄來的](../Page/新潟縣.md "wikilink")。他們請求代替十一位軍官一死，並同時附上十一根手指表示他們的衷意。

## 結果

[法院最後果然](../Page/東京地方裁判所.md "wikilink")「從（極）寬發落」，媒體也對殺害首相的兇手關沒幾年就會被放出來表示毫無疑問。對於這個五一五事件的陰謀者日本軍部來說，這樣的重案卻有這樣的輕判就是對與軍權對抗下的法制與民主政府更進一步的侵蝕。五一五事件的结果就是在同年5月26日成立以海军大将[斋藤实为首的所谓](../Page/斋藤实.md "wikilink")「举国一致」的[内阁](../Page/斋藤内阁.md "wikilink")，日本政党内阁时代结束。間接地，五一五事件導致了1936年的[二二六兵變的發生](../Page/二二六兵變.md "wikilink")，以及日本[軍國主義的發展](../Page/軍國主義.md "wikilink")。

## 相关人物

### 政变者

#### 首相官邸襲擊隊

  - [三上卓](../Page/三上卓.md "wikilink")：海軍中尉，[巡洋艦](../Page/巡洋艦.md "wikilink")[妙高號船員](../Page/妙高號重巡洋艦.md "wikilink")，以[叛亂罪起訴](../Page/叛亂罪.md "wikilink")，判處15年[有期徒刑](../Page/有期徒刑.md "wikilink")。（出獄後，活躍於右翼活動，後與1961年有關）
  - [山岸宏](../Page/山岸宏.md "wikilink")：海軍中尉
  - [村山格之](../Page/村山格之.md "wikilink")：海軍少尉
  - [黑岩勇](../Page/黑岩勇.md "wikilink")：[預備役海軍少尉](../Page/預備役.md "wikilink")，以叛亂罪起訴，判處13年有期徒刑。
  - [野村三郎](../Page/野村三郎.md "wikilink")：[陸軍士官学校本科生](../Page/日本陸軍士官學校.md "wikilink")
  - [後藤映範](../Page/後藤映範.md "wikilink")：陸軍士官学校本科生
  - [篠原市之助](../Page/篠原市之助.md "wikilink")：陸軍士官学校本科生
  - [石關榮](../Page/石關榮.md "wikilink")：陸軍士官学校本科生
  - [八木春男](../Page/八木春男.md "wikilink")：陸軍士官学校本科生

#### 内大臣官邸襲擊隊

  - [古贺清志](../Page/古贺清志.md "wikilink")：海軍中尉，以叛亂罪起訴，判處15年有期徒刑。
  - [坂元兼一](../Page/坂元兼一.md "wikilink")：陸軍士官学校本科生
  - [菅勤](../Page/菅勤.md "wikilink")：陸軍士官学校本科生
  - [西川武敏](../Page/西川武敏.md "wikilink")：陸軍士官学校本科生
  - [池松武志](../Page/池松武志.md "wikilink")：元陸軍士官学校本科生

#### 立憲政友会本部襲擊隊

  - [中村义雄](../Page/中村义雄.md "wikilink")：海軍中尉
  - [中島忠秋](../Page/中島忠秋.md "wikilink")：陸軍士官学校本科生
  - [金清豊](../Page/金清豊.md "wikilink")：陸軍士官学校本科生
  - [吉原政巳](../Page/吉原政巳.md "wikilink")：陸軍士官学校本科生

#### 民間人士

  - [橘孝三郎](../Page/橘孝三郎.md "wikilink")：「[愛鄉塾](../Page/愛鄉塾.md "wikilink")」負責人，以違反[刑法爆裂物取締罰則](../Page/刑法.md "wikilink")，殺人罪及殺人未遂罪起訴，判處[無期徒刑](../Page/無期徒刑.md "wikilink")。
  - [大川周明](../Page/大川周明.md "wikilink")：以叛亂罪起訴，判處5年有期徒刑。
  - [本間憲一郎](../Page/本間憲一郎.md "wikilink")
  - [頭山秀三](../Page/頭山秀三.md "wikilink")：[玄洋社社員](../Page/玄洋社.md "wikilink")。[頭山滿的三子](../Page/頭山滿.md "wikilink")。

### 司法界人士

  - [高須四郎](../Page/高須四郎.md "wikilink")：海軍[横須賀鎮守府軍法會議判士長](../Page/横須賀鎮守府.md "wikilink")(主席)・海軍[大佐](../Page/大佐.md "wikilink")
  - [西村琢磨](../Page/西村琢磨.md "wikilink")：陸軍[第一師團軍法會議判士長](../Page/第1師團_\(日本陸軍\).md "wikilink")・陸軍[砲兵](../Page/砲兵.md "wikilink")[中佐](../Page/中佐.md "wikilink")
  - [神垣秀六](../Page/神垣秀六.md "wikilink")：東京地方裁判所裁判長（庭長）・判事（[法官](../Page/法官.md "wikilink")）
  - [清瀬一郎](../Page/清瀬一郎.md "wikilink")：辯護人（[律師](../Page/律師.md "wikilink")）
  - [林逸郎](../Page/林逸郎.md "wikilink")：辯護人
  - [花井忠](../Page/花井忠.md "wikilink")：辯護人

## 相关艺术作品

  - [攻壳机动队](../Page/攻壳机动队.md "wikilink")

## 參考文獻

  -
  -
  -
  -
  -
  - {{ cite book

` | last = Tolland`
` | first = John`
` | title = `*`The``   ``Rising``   ``Sun:``   ``The``   ``Decline``
 ``and``   ``Fall``   ``of``   ``the``   ``Japanese``   ``Empire,``
 ``1936-1945`*
` | publisher = Modern Library`
` | date = 2003 reprint`
` | pages =`
` | isbn = 0812968581}}`

## 参见

  - [五私鐵疑獄事件](../Page/五私鐵疑獄事件.md "wikilink")
  - [二二六事件](../Page/二二六事件.md "wikilink")
  - [九一八事變](../Page/九一八事變.md "wikilink")
  - [大日本帝國海軍](../Page/大日本帝國海軍.md "wikilink")

{{-}}

[Category:1932年5月](../Category/1932年5月.md "wikilink")
[Category:1932年日本政治事件](../Category/1932年日本政治事件.md "wikilink")
[Category:日本政变](../Category/日本政变.md "wikilink")
[Category:日本叛亂](../Category/日本叛亂.md "wikilink")
[Category:日本謀殺案](../Category/日本謀殺案.md "wikilink")
[Category:日本武器犯罪](../Category/日本武器犯罪.md "wikilink")
[Category:大日本帝国海军事件](../Category/大日本帝国海军事件.md "wikilink")
[Category:暗殺](../Category/暗殺.md "wikilink")
[Category:農本主義](../Category/農本主義.md "wikilink")
[Category:日本恐怖活动](../Category/日本恐怖活动.md "wikilink")
[Category:永田町](../Category/永田町.md "wikilink")
[Category:東京都歷史](../Category/東京都歷史.md "wikilink")