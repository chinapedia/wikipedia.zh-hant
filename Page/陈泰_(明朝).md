**陳泰**（），字**吉亨**，是[中國](../Page/中國.md "wikilink")[明朝官員](../Page/明朝.md "wikilink")。[福建](../Page/福建.md "wikilink")[光泽人](../Page/光泽.md "wikilink")\[1\]。

## 生平

他幼年時[過繼外家](../Page/過繼.md "wikilink")[曹姓](../Page/曹姓.md "wikilink")。舉[鄉試第一](../Page/鄉試.md "wikilink")，回復原姓，任安慶府學[訓導](../Page/訓導.md "wikilink")。[明英宗正統初年](../Page/明英宗.md "wikilink")，陳泰被擢為[御史](../Page/御史.md "wikilink")，巡按貴州。當時官軍-{征}-[麓川](../Page/麓川.md "wikilink")，每年取當地人二千為鄉導，遇到作戰失利就往往殺了那班人以冒功，經陳泰上奏後終止了這種做法。

陳泰後至山西[巡按](../Page/巡按.md "wikilink")。當時百官俸薄，折鈔又不能即得。巡按泰上章請求「量增祿廩，俾足養廉，然後治贓污，則貪風自息」，沒有實行。正統六年夏天又上言：「連歲災異，咎在廷臣，請敕御史給事中糾彈大臣，去其尤不職者，而後所司各考核其屬。」英宗接納他的意見，於是御史馬謹等交章彈劾吏部尚書郭璡等數十人。陳泰後來又到山東巡按。《明史》稱他「三為巡按，懲-{奸}-去貪，威稜甚峻。」正統九年（1444年），陳泰被擢為四川[按察使](../Page/按察使.md "wikilink")，與鎮守[都御史寇深不和](../Page/都御史.md "wikilink")。十二年八月，參議陳敏按寇希之意彈劾陳泰「擅杖武職，毆輿夫至死」。陳泰被逮入[刑部獄](../Page/刑部.md "wikilink")，坐斬。[代宗監國時](../Page/明代宗.md "wikilink")，赦免了陳泰並回復他的官職。[-{于}-謙推薦陳泰守](../Page/于謙.md "wikilink")[紫荊關](../Page/紫荊關.md "wikilink")。[瓦剌軍進犯北京](../Page/瓦剌.md "wikilink")，紫荊關失守，泰又再被論死。代宗寬免了陳泰，命充為事官，從總兵官顧興祖築關隘。

[景泰元年](../Page/景泰_\(明朝\).md "wikilink")（1450年）擢為大理右少卿，守備白羊口。四月，都督同知劉安巡備涿、易、真、保諸城，命泰以右僉[都御史參其軍務](../Page/都御史.md "wikilink")。三年兼巡撫保定六府。隨後被任命督治河道。自儀真（今江蘇[儀征](../Page/儀征.md "wikilink")）至[淮安](../Page/淮安.md "wikilink")，浚渠一百八十里，塞決口九，築壩三，役六萬人，數月完工。七年移撫蘇、-{松}-。英宗復辟後，陳泰改任[廣東副使](../Page/廣東.md "wikilink")，「以憂去」。後來被起用到四川[巡撫](../Page/巡撫.md "wikilink")。天順八年進右副都御史，總督漕運兼巡撫淮、揚諸府。他在淮地三年後離開，在[明憲宗](../Page/明憲宗.md "wikilink")[成化六年](../Page/成化.md "wikilink")（1470年）去世。

## 參考文獻

<references/>

  - 《[明史](../Page/明史.md "wikilink")》列傳第四十七

[Category:明朝監察御史](../Category/明朝監察御史.md "wikilink")
[Category:明朝四川按察使](../Category/明朝四川按察使.md "wikilink")
[Category:明朝大理寺少卿](../Category/明朝大理寺少卿.md "wikilink")
[Category:明朝副都御史](../Category/明朝副都御史.md "wikilink")
[Category:明朝廣東按察使司副使](../Category/明朝廣東按察使司副使.md "wikilink")
[Category:明朝四川巡撫](../Category/明朝四川巡撫.md "wikilink")
[Category:明朝漕運總督](../Category/明朝漕運總督.md "wikilink")
[C陈](../Category/光泽人.md "wikilink") [T泰](../Category/陈姓.md "wikilink")

1.