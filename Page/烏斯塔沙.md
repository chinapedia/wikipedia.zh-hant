[Ustaše_symbol.svg](https://zh.wikipedia.org/wiki/File:Ustaše_symbol.svg "fig:Ustaše_symbol.svg")

**烏斯塔沙**（**Ustaša**，**Ustaše**，又譯**烏斯塔莎**、**烏斯達沙**、**烏斯達莎**），為[克羅埃西亞的獨立運動組織](../Page/克羅埃西亞.md "wikilink")，Ustasha本意就有起義的意思。

## 歷史

烏斯塔沙於1929年4月20日在[保加利亞的](../Page/保加利亞.md "wikilink")[索菲亞成立](../Page/索菲亞.md "wikilink")，其目標是讓克羅埃西亞由[南斯拉夫獨立](../Page/南斯拉夫.md "wikilink")，其領導人[安特·帕維里奇與](../Page/安特·帕維里奇.md "wikilink")[墨索里尼的](../Page/墨索里尼.md "wikilink")[義大利](../Page/義大利.md "wikilink")[法西斯黨有密切關係](../Page/法西斯黨.md "wikilink")\[1\]，並且領取其津貼。1941年[納粹德國與](../Page/納粹德國.md "wikilink")[義大利王國及其](../Page/意大利王國_\(1861年–1946年\).md "wikilink")[盟國進攻南斯拉夫](../Page/轴心國.md "wikilink")，烏斯塔沙組織的軍隊便趁此時宣佈克羅埃西亞獨立，並成立[克羅埃西亞獨立國](../Page/克羅埃西亞獨立國.md "wikilink")，並加入[軸心國陣營](../Page/軸心國.md "wikilink")。而且烏斯塔沙組織也受到天主教會的支持。1945年烏斯塔沙被由[狄托率領的人民軍擊潰](../Page/狄托.md "wikilink")，克羅埃西亞再度併入南斯拉夫。

烏斯塔沙組織雖然達成了克羅埃西亞的獨立，但是烏斯塔沙政權卻同時殘酷地鎮壓[塞爾維亞人](../Page/塞爾維亞人.md "wikilink")、[猶太人和](../Page/猶太人.md "wikilink")[吉普賽人](../Page/吉普賽人.md "wikilink")\[2\]\[3\]\[4\]，根據統計，烏斯塔沙建立超過十個[集中營](../Page/集中營.md "wikilink")，殺害達九萬三千人；但是塞爾維亞人的[切特尼克](../Page/切特尼克.md "wikilink")（一个名義上反纳粹德国，實際上反共的民族组织）也殺害不少[克羅埃西亞人](../Page/克羅埃西亞人.md "wikilink")，這都是導致南斯拉夫族群問題進一步惡化的歷史原因之一。

## 參考資料

## 外部連結

  - [Jasenovac Research Institute](http://www.jasenovac.org/) — "a
    non-profit human rights organization and research institute
    committed to establishing the truth about the Holocaust in
    Yugoslavia"
  - [Holocaust era in Croatia:
    Jasenovac 1941–1945](http://www.ushmm.org/museum/exhibit/online/jasenovac),
    an on-line museum by the [United States Holocaust Memorial
    Museum](../Page/United_States_Holocaust_Memorial_Museum.md "wikilink")
  - [Fund For Genocide Research, Jasenovac death
    camp](https://web.archive.org/web/20051031151251/http://guskova.ru/misc/balcan/2004-04-22e),
    guskova.ru
  - [Eichmann Trial, Tel
    Aviv 1961](http://www.nizkor.org/hweb/people/e/eichmann-adolf/transcripts/Sessions/Session-046-01.html),
    nizkor.org
  - [Lawsuit against the Vatican Bank and Franciscans for return of the
    Ustasha Treasury by Holocaust
    victims](http://www.vaticanbankclaims.com)

[烏斯塔沙](../Category/烏斯塔沙.md "wikilink")
[Category:1929年克羅埃西亞建立](../Category/1929年克羅埃西亞建立.md "wikilink")
[Category:1945年克羅埃西亞廢除](../Category/1945年克羅埃西亞廢除.md "wikilink")
[Category:克罗地亚歷史](../Category/克罗地亚歷史.md "wikilink")
[Category:法西斯主義](../Category/法西斯主義.md "wikilink")

1.
2.  Ladislaus Hory und Martin Broszat. *Der kroatische Ustascha-Staat*,
    Deutsche Verlag-Anstalt, Stuttgart, 2. Auflage 1965, pp. 13–38,
    75–80.
3.
4.