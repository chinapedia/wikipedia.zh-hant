**翁文波**（），[浙江鄞县人](../Page/浙江.md "wikilink")，中国地球物理学家，中国科学院地学部院士\[1\]。

## 年表

1912年，生于浙江宁波鄞县（今属[宁波市](../Page/宁波市.md "wikilink")[海曙区](../Page/海曙区.md "wikilink")）。

1934年，毕业于[清华大学](../Page/清华大学.md "wikilink")[物理学系](../Page/物理学.md "wikilink")。同年任[北平研究院物理研究所助理员](../Page/北平研究院.md "wikilink")。

1936年，远赴[英国留学](../Page/英国.md "wikilink")。1939年，荣获[英国](../Page/英国.md "wikilink")[帝国理工](../Page/帝国理工.md "wikilink")[博士学位](../Page/博士.md "wikilink")。

1939年\~1940年，任教于时[国立中央大学](../Page/国立中央大学_\(南京\).md "wikilink")（[重庆](../Page/重庆.md "wikilink")），随后于玉门油矿任工程师。

1946年，始任职于上海中国石油公司勘探室。1949年10月1日后：历任燃料工业部石油总局勘探处副处长，石油工业部勘探司、石油科学研究院、石油勘探开发科学研究院总工程师。

1980年，当选为中国科学院地学部委员（院士）。

## 任职

历任：

  - 中华人民共和国第三届全国人大代表
  - 中华人民共和国第五届全国政协委员
  - 中华人民共和国第六届全国政协委员
  - 中国石油学会副理事长
  - 中国地球物理学会理事长
  - 中国科协委员

## 著作

论文： 《地震的远期预报》、《纬度变化和地极运动》、 《地球的化学成因》、《反射地震勘探中用直射线的计算法》、
《地球科学中有关原子核性质的几个问题》等。

书目：《地球形态的发展》、 《预测论基础》、 《初级数据的分布》等。

## 参考文献

<references/>

[W翁](../Category/中華人民共和國地質學家.md "wikilink")
[W翁](../Category/清华大学校友.md "wikilink")
[W](../Category/國立清華大学校友.md "wikilink")
[W翁](../Category/1994年逝世.md "wikilink")
[W翁](../Category/1912年出生.md "wikilink")
[W翁](../Category/中国物理学家.md "wikilink")
[W翁文波](../Category/石塘翁氏.md "wikilink")
[W翁](../Category/海曙区人.md "wikilink")
[W翁](../Category/中国科学院地学部院士.md "wikilink")
[Category:倫敦帝國學院校友](../Category/倫敦帝國學院校友.md "wikilink")
[Category:翁姓](../Category/翁姓.md "wikilink")

1.