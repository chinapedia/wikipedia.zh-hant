**陳瑞隆**（[英語](../Page/英語.md "wikilink")：Steve Ruey-long
Chen，），[國立中興大學法商學院](../Page/國立中興大學法商學院.md "wikilink")（今[國立臺北大學](../Page/國立臺北大學.md "wikilink")）經濟系畢業，曾任[中華民國](../Page/中華民國.md "wikilink")[經濟部部長](../Page/中華民國經濟部.md "wikilink")、[資訊工業策進會董事長](../Page/資訊工業策進會.md "wikilink")。現任[智仁科技代表人](../Page/智仁科技.md "wikilink")。2012年11月，[力晶科技股票下櫃](../Page/力晶科技.md "wikilink")，董事長[黃崇仁請辭](../Page/黃崇仁.md "wikilink")，陳瑞隆擔任董事長。2004年獲頒第四屆[國立臺北大學傑出校友](../Page/國立臺北大學.md "wikilink")。2012年獲頒[國立中興大學第十六屆傑出校友](../Page/國立中興大學.md "wikilink")。
  |- |colspan="3"
style="text-align:center;"|**[ROC_Executive_Yuan_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Executive_Yuan_Logo.svg "fig:ROC_Executive_Yuan_Logo.svg")[行政院](../Page/行政院.md "wikilink")**
|-

[Category:資訊工業策進會董事長](../Category/資訊工業策進會董事長.md "wikilink")
[Category:中華民國經濟部部長](../Category/中華民國經濟部部長.md "wikilink")
[Category:中華民國經濟部次長](../Category/中華民國經濟部次長.md "wikilink")
[Category:中華民國經濟部國際貿易局局長](../Category/中華民國經濟部國際貿易局局長.md "wikilink")
[Category:國立中興大學法商學院校友](../Category/國立中興大學法商學院校友.md "wikilink")
[Category:嘉義市人](../Category/嘉義市人.md "wikilink")
[Rui瑞](../Category/陳姓.md "wikilink")