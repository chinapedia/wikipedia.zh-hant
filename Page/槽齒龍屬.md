**槽齒龍屬**（[屬名](../Page/屬.md "wikilink")：）是種[草食性恐龍](../Page/草食性.md "wikilink")，生存於晚[三疊紀](../Page/三疊紀.md "wikilink")[諾利階與](../Page/諾利階.md "wikilink")[瑞提階](../Page/瑞提階.md "wikilink")。槽齒龍的[化石大部分發現於南](../Page/化石.md "wikilink")[英格蘭與](../Page/英格蘭.md "wikilink")[威爾斯的](../Page/威爾斯.md "wikilink")[三疊紀地層](../Page/三疊紀.md "wikilink")。這個時期的地球氣候較為溫暖、乾燥。晚[三疊紀的優勢](../Page/三疊紀.md "wikilink")[肉食性動物仍為](../Page/肉食性.md "wikilink")[勞氏鱷目等](../Page/勞氏鱷目.md "wikilink")[鑲嵌踝類主龍](../Page/鑲嵌踝類主龍.md "wikilink")，而非剛出現的小型肉食性恐龍；而[蜥腳形亞目恐龍已取代](../Page/蜥腳形亞目.md "wikilink")[二齒獸類](../Page/二齒獸類.md "wikilink")，成為優勢[草食性動物](../Page/草食性.md "wikilink")。

## 特徵

[Thecodontosaurus_antiquus_skeleton.png](https://zh.wikipedia.org/wiki/File:Thecodontosaurus_antiquus_skeleton.png "fig:Thecodontosaurus_antiquus_skeleton.png")
槽齒龍平均身長為1.2公尺，高度約30公分，體重為30公斤。牠們擁有小型頭部、大型拇指尖爪、修長的後肢、長頸部、前肢比後肢短、長尾巴。牠們是二足恐龍。槽齒龍前掌有五個手指，後腳掌有五個腳趾。

槽齒龍是[草食性恐龍](../Page/草食性.md "wikilink")，牙齒呈葉狀，有鋸齒狀邊緣，且位於齒槽內；這也是槽齒龍的名稱來源。牠們的[齒骨長度不到下頜長度的一半](../Page/齒骨.md "wikilink")。下頜前端稍微往下彎。與[近蜥龍相比](../Page/近蜥龍.md "wikilink")，槽齒龍有較多的牙齒，頭部較長、較狹窄。

槽齒龍的[頸椎上有長的](../Page/頸椎.md "wikilink")[椎弓](../Page/椎弓.md "wikilink")（Neural
Arch），以及前後排列的長[神經棘](../Page/神經棘.md "wikilink")（Neural
spine）。[背椎有強化的](../Page/背椎.md "wikilink")[橫突](../Page/橫突.md "wikilink")（Transverse
processes）。[薦椎可能有](../Page/薦椎.md "wikilink")3個。[肩胛骨寬廣](../Page/肩胛骨.md "wikilink")、彎曲，稍呈板狀。[肱骨有明顯](../Page/肱骨.md "wikilink")[三角嵴](../Page/三角嵴.md "wikilink")（Deltopectoral
crest）。[尺骨的橫切面呈三角形](../Page/尺骨.md "wikilink")。[橈骨有大幅度縮小](../Page/橈骨.md "wikilink")。

雖然槽齒龍並非最早的[蜥腳形亞目恐龍](../Page/蜥腳形亞目.md "wikilink")（目前發現最早的是在[馬達加斯加的未命名屬](../Page/馬達加斯加.md "wikilink")），但在原始蜥腳形亞目恐龍中是最著名的一屬。

## 分类

牠們起初被分類於[原蜥腳下目](../Page/原蜥腳下目.md "wikilink")，但在2003年，耶茲與基欽的研究顯示槽齒龍與牠的近親早於原蜥腳類的出現。新的重建顯示槽齒龍的頸部與身體比例，比更先進的早期蜥腳形亞目恐龍的頸部與身體比例還短。

## 發現歷史

槽齒龍是由Henry Riley與Samuel
Stutchbury在1836年所命名。槽齒龍是第四個被命名的恐龍，前三個分別為[斑龍](../Page/斑龍.md "wikilink")（1824年）、[禽龍](../Page/禽龍.md "wikilink")（1825年）、[林龍](../Page/林龍.md "wikilink")（1833年）；槽齒龍也是第一個被敘述的[三疊紀恐龍](../Page/三疊紀.md "wikilink")。

當[理查·歐文在](../Page/理查·歐文.md "wikilink")1842年建立[恐龍總目](../Page/恐龍總目.md "wikilink")（Dinosauria）時，他並沒有將槽齒龍列入，他僅將斑龍、禽龍、林龍列入。他認為有小型頜部的槽齒龍，不可能屬於體型龐大的恐龍。他認為槽齒龍是種低等、由鱗片覆蓋的爬蟲類，與[鱷魚](../Page/鱷魚.md "wikilink")、[蜥蜴](../Page/蜥蜴.md "wikilink")、[喙頭龍目](../Page/喙頭龍目.md "wikilink")、恐龍外表相似。直到1870年，歐文的敵手[湯瑪斯·亨利·赫胥黎](../Page/湯瑪斯·亨利·赫胥黎.md "wikilink")（Thomas
Henry Huxley）將槽齒龍分類於恐龍。

槽齒龍最初的[模式標本](../Page/模式標本.md "wikilink")，與其他相關物品，在1940年遭到[德國衝炸摧毀](../Page/德國.md "wikilink")，成為[第二次世界大戰中的受害品](../Page/第二次世界大戰.md "wikilink")。然而，目前已在許多地點發現化石，包括[布里斯托與](../Page/布里斯托.md "wikilink")[威爾斯](../Page/威爾斯.md "wikilink")，該地於晚三疊紀可能是乾燥環境。在這些新發現化石中，有一個可能是未成年個體的標本，可能屬於一個不同的種*T.
caducus*。在[澳洲發現的](../Page/澳洲.md "wikilink")*Agrosaurus
macgillivrayi*，可能是古槽齒龍（*T.
antiquus*）的[異名](../Page/異名.md "wikilink")。

在2007年，[亞當·耶茨](../Page/亞當·耶茨.md "wikilink")（Adam
Yates）、[彼得·加爾東](../Page/彼得·加爾東.md "wikilink")（Peter
Galton）、以及Kermack提出一個研究，宣稱槽齒龍的其中一種*T.
caducus*屬於一個不同的屬，並由他們將這個屬命名為*Pantydraco*。

槽齒龍目前只有一個種，[模式種](../Page/模式種.md "wikilink")**古槽齒龍**（*T. antiquus*）。

## 參考資料

  - John Flynn and Andre Wyss (2002) Madagascar's Mesozoic Secrets.
    *Scientific American* 286: 54-63 February 2002.
  - Paul Upchurch (1998), The phylogenetic relationships of sauropod
    dinosaurs. *Zoological Journal of the Linnean Society* 124: 43–103.
  - Adam Yates, (2003) A new species of the primitive dinosaur
    Thecodontosaurus (Saurischia: Sauropodomorpha) and its implications
    for the systematics of early dinosaurs. *Journal of Systematic
    Palaeontology* Vol. 1, no.1, pp. 1-42
  - Yates, A.M. & James William Kitching (2003) The earliest known
    sauropod dinosaur and the first steps towards sauropod locomotion.
    *Proceedings of the Royal Society of London: B Biol Sci.* 2003 Aug
    22; 270(1525): 1753–8.
  - Galton, P.M., Yates, A.M. & Kermack, D. (2007) Pantydraco n. gen.
    for Thecodontosaurus caducus Yates, 2003, a basal sauropodomorph
    dinosaur from the Upper Triassic or Lower Jurassic of South Wales,
    UK. Neues Jahrbuch für GEologie und Paläontologie, abh., 243:
    119-125
  - [槽齒龍](http://www.enchantedlearning.com/subjects/dinosaurs/dinos/Thecodontosaurus.shtml)
    - Enchanted Learning網站
  - [槽齒龍簡介](http://www.dinoruss.com/de_4/5a6b5b9.htm) (英文)
  - [槽齒龍、原蜥腳下目、近蜥龍、黑丘龍科](https://web.archive.org/web/20060313154831/http://www.palaeos.com/Vertebrates/Units/330Sauropodomorpha/330.100.html)
    Palaeos網站

## 外部連結

  - [槽齒龍圖片](http://www.picsearch.com/pictures/animals/dinosaurs/thecodontosaurus.html)
  - [布里斯托恐龍網站](http://palaeo.gly.bris.ac.uk/bristoldinosaur)
  - [See entry on *Thecodontosaurus* at
    DinoData](http://www.dinodata.org/index.php) (registration required,
    free).
  - [蜥腳形亞目](https://web.archive.org/web/20071227143521/http://www.users.qwest.net/~jstweet1/sauropodomorpha.htm)
  - [New genus Pantydraco at
    DinoData](http://www.dinodata.org/index.php?option=com_content&task=view&id=9561&Itemid=103)
    (registration required, free)

[Category:三疊紀恐龍](../Category/三疊紀恐龍.md "wikilink")
[Category:蜥腳形亞目](../Category/蜥腳形亞目.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")