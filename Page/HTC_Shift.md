**HTC Shift**，研發代號**HTC
Clio**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[UMPC掌上型電腦](../Page/UMPC.md "wikilink")，同時具有手機功能，搭載微軟[Windows
Vista](../Page/Windows_Vista.md "wikilink") Business與[Windows
Mobile](../Page/Windows_Mobile.md "wikilink")6雙作業系統（無法同時使用），擁有7吋VGA觸控式螢幕，滑蓋式
QWERTY
鍵盤，搭載[SnapVUE技術即使裝置並未開啟也能夠使網路永遠保持連線狀態](../Page/SnapVUE.md "wikilink")，內建40GB微型硬碟，具有指紋辨識器。2007年10月於歐洲首度發表。已知客製版本HTC
X9500，HTC Shift，Rogers＆HTC Shift。

## HTC Shift GSM技術規格

  - [處理器](../Page/處理器.md "wikilink")：Intel A110 800MHz，Qualcomm MSM7200
    400MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Vista Business，Windows
    Mobile 6.0 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：RAM：1GB DDR2 microDIMM（Vista專用）+64
    MB（SnapVUE專用）ROM：128 MB（SnapVUE專用）
  - [硬碟](../Page/硬碟.md "wikilink")：1.8 吋 40 GB
  - 尺寸：207mm X 129mm X 25mm
  - 重量：800g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：WVGA 解析度、7.0 吋 TFT-LCD 平面式觸控感應螢幕
  - [操控](../Page/操控.md "wikilink")：滑蓋式 QWERTY 鍵盤
  - [網路](../Page/網路.md "wikilink")：HSUPA/HSDPA/WCDMA/EDGE/GPRS/GSM
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：300萬畫素相機，支援自動對焦功能
  - 擴充槽：SDIO插槽，支援熱插拔功能
  - [電池](../Page/電池.md "wikilink")：2700mAh充電式鋰或鋰聚合物電池

## HTC Shift CDMA技術規格

  - [處理器](../Page/處理器.md "wikilink")：Intel A110 800MHz，Qualcomm MSM7500
    400MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Vista Business，Windows
    Mobile 6.0 Professional
  - \[\[可支援windows 7以上版本(建議windows 7)
  - [記憶體](../Page/記憶體.md "wikilink")：RAM：1GB DDR2 microDIMM（Vista專用）+64
    MB（SnapVUE專用）ROM：128 MB（SnapVUE專用）
  - [硬碟](../Page/硬碟.md "wikilink")：1.8 吋HDD 40 GB
  - 尺寸：207mm X 129mm X 25mm
  - 重量：800g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：WVGA 解析度、7.0 吋 TFT-LCD 平面式觸控感應螢幕
  - [操控](../Page/操控.md "wikilink")：滑蓋式 QWERTY 鍵盤
  - [網路](../Page/網路.md "wikilink")：CDMA 1x RTT/CDMA 1x EvDO
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：300萬畫素相機，支援自動對焦功能
  - 擴充槽：SDIO插槽，支援熱插拔功能
  - [電池](../Page/電池.md "wikilink")：2700mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")
  - [SnapVUE](../Page/SnapVUE.md "wikilink")
  - [超级移动电脑](../Page/超级移动电脑.md "wikilink")

## 外部連結

  - [HTC Shift 系列概觀](http://www.htc.com/tw/product.aspx?id=22732)
  - [HTC Shift 系列技術規格](http://www.htc.com/tw/product.aspx?id=22738)

[Category:超级移动电脑](../Category/超级移动电脑.md "wikilink")