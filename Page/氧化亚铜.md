**氧化亞銅**是一价[銅的](../Page/銅.md "wikilink")[氧化物](../Page/氧化物.md "wikilink")，分子式為Cu<sub>2</sub>O，紅色至紅褐色結晶或粉末。它不溶於水及有機[溶劑](../Page/溶劑.md "wikilink")，但可溶於稀鹽酸、稀硫酸、氯化銨溶液。溶於濃[氨溶液形成無色](../Page/氨.md "wikilink")[配合物Cu](../Page/配合物.md "wikilink")(NH<sub>3</sub>)<sub>2</sub><sup>+</sup>，其在空氣中被[氧化為藍色的](../Page/氧化.md "wikilink")\[Cu(NH<sub>3</sub>)<sub>4</sub>(H<sub>2</sub>O)<sub>2</sub>\]<sup>2+</sup>。氧化亞銅在1800℃分解成[銅和](../Page/銅.md "wikilink")[氧](../Page/氧.md "wikilink")，其在乾燥空氣中穩定，但在潮濕空氣中被慢慢氧化為[氧化銅](../Page/氧化銅.md "wikilink")。

氧化亞銅可溶於[鹽酸生成HCuCl](../Page/鹽酸.md "wikilink")<sub>2</sub>（[氯化亞銅的配合物](../Page/氯化亞銅.md "wikilink")），也可溶於[硫酸及](../Page/硫酸.md "wikilink")[硝酸分別形成](../Page/硝酸.md "wikilink")[硫酸銅及](../Page/硫酸銅.md "wikilink")[硝酸銅](../Page/硝酸銅.md "wikilink")。

## 參見

  - [氧化銅](../Page/氧化銅.md "wikilink")
  - [赤铜矿](../Page/赤铜矿.md "wikilink")

## 參考

1.  N. N. Greenwood, A. Earnshaw, *Chemistry of the Elements*, 2nd ed.,
    Butterworth-Heinemann, Oxford, UK, 1997.
2.  *Handbook of Chemistry and Physics*, 71st edition, [CRC
    Press](../Page/CRC_Press.md "wikilink"), Ann Arbor, Michigan, 1990.
3.  *The Merck Index*, 7th edition, Merck & Co, Rahway, [New
    Jersey](../Page/New_Jersey.md "wikilink"),
    [USA](../Page/USA.md "wikilink"), 1960.
4.  D. Nicholls, *Complexes and First-Row Transition Elements*,
    Macmillan Press, London, 1973.
5.  P.W. Baumeister: Optical Absorption of Cuprous Oxide, *Phys. Rev.*
    **121** (1961), 359.
6.  L. Brillouin: *Wave Propagation and Group Velocity*, [Academic
    Press](../Page/Academic_Press.md "wikilink"), [New
    York](../Page/New_York.md "wikilink"), 1960.
7.  D. Fröhlich, A. Kulik, B. Uebbing, and A. Mysyrovicz: Coherent
    Propagation and Quantum Beats of Quadrupole Polaritons in
    Cu<sub>2</sub>O, *[Phys. Rev.
    Lett.](../Page/Phys._Rev._Lett..md "wikilink")* **67** (1991), 2343.
8.  L. Hanke: Transformation von Licht in Wärme in Kristallen - Lineare
    Absorption in Cu<sub>2</sub>O, ISBN 3-8265-7269-6, Shaker, Aachen,
    2000; (Transformation of light into heat in crystals - Linear
    absorption in Cu<sub>2</sub>O).
9.  L. Hanke, D. Fröhlich, A.L. Ivanov, P.B. Littlewood, and H. Stolz:
    LA-Phonoritons in Cu<sub>2</sub>O, *[Phys. Rev.
    Lett.](../Page/Phys._Rev._Lett..md "wikilink")* **83** (1999), 4365.
10. L. Hanke, D. Fröhlich, and H. Stolz: Direct observation of
    longitudinal [acoustic
    phonon](../Page/acoustic_phonon.md "wikilink") absorption to the
    1S-exciton in Cu<sub>2</sub>O, *Sol. Stat. Comm.* **112** (1999),
    455.
11. J.J. Hopfield, Theory of the Contribution of Excitons to the Complex
    Dielectric Constant of Crystals, *[Phys.
    Rev.](../Page/Phys._Rev..md "wikilink")* **112** (1958), 1555.
12. J.P. Wolfe and A. Mysyrowicz: Excitonic Matter, *[Scientific
    American](../Page/Scientific_American.md "wikilink")* **250**
    (1984), No. 3, 98.

## 外部連結

  - [National Pollutant Inventory - Copper and compounds fact
    sheet](https://web.archive.org/web/20080302034606/http://www.npi.gov.au/database/substance-info/profiles/27.html)

[Category:氧化物](../Category/氧化物.md "wikilink")
[Category:一价铜化合物](../Category/一价铜化合物.md "wikilink")
[Category:半导体材料](../Category/半导体材料.md "wikilink")