**兰开斯特**（）位于[美国](../Page/美国.md "wikilink")[新罕布什尔州北部](../Page/新罕布什尔州.md "wikilink")，是[库斯县的县治所在](../Page/库斯县_\(新罕布什尔州\).md "wikilink")，面积132.8平方公里。根据[2000年美国人口普查](../Page/2000年美国人口普查.md "wikilink")，共有3,280人，其中[白人占](../Page/白人.md "wikilink")98.08%。這個數字於2010年上升至3,507人\[1\]，是該縣人口第二多城市\[2\]

## 地理

根據[2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，本城面積為，其中為陸地，為水域。\[3\]

## 參考文獻

## 外部链接

  - [Town of Lancaster](http://www.lancasternh.org/)
  - [Lancaster
    Fair](https://web.archive.org/web/20080514162354/http://www.lancasterfair.com/index.htm)
  - [Terraserver interactive map of Lancaster
    area](http://terraserver-usa.com/image.aspx?T=2&S=15&Z=19&X=46&Y=769&W=1)
  - [New Hampshire Economic and Labor Market Information Bureau
    Profile](http://www.nh.gov/nhes/elmi/htmlprofiles/lancaster.html)

[L](../Category/新罕布什尔州城市.md "wikilink")

1.  [2010 census factfinder2](http://factfinder2.census.gov/)
2.  United States Census Bureau, [American
    FactFinder](http://factfinder2.census.gov/main.html), 2010 Census
    figures. Retrieved March 23, 2011.
3.