[Under_the_section_of_the_old_city_wall,_the_side_of_Na_Tcha_Temple_and_the_back_of_Ruínas_de_S.Paulo.JPG](https://zh.wikipedia.org/wiki/File:Under_the_section_of_the_old_city_wall,_the_side_of_Na_Tcha_Temple_and_the_back_of_Ruínas_de_S.Paulo.JPG "fig:Under_the_section_of_the_old_city_wall,_the_side_of_Na_Tcha_Temple_and_the_back_of_Ruínas_de_S.Paulo.JPG")下看[大三巴哪吒廟與](../Page/大三巴哪吒廟.md "wikilink")[大三巴牌坊](../Page/大三巴牌坊.md "wikilink")\]\]
**澳門建築**是描述[澳門特別行政區內的建築歷史及](../Page/澳門特別行政區.md "wikilink")[建築風格](../Page/建築風格.md "wikilink")。悠久以來，澳門已有[華人聚居的村落](../Page/華人.md "wikilink")，及至16世紀起[葡萄牙人獲准在此居住及進行建設](../Page/葡萄牙人.md "wikilink")。葡萄牙人在澳門定居，標誌著中式建築與西式建築文化的交接發展。因澳門成為對外貿易外港及其獨特的政治發展因素，在建築上充分反映了中西建築文化上的共融和歷史價值。澳門建築的歷史可以粗略分成四個時期，[明朝](../Page/明朝.md "wikilink")、[清朝前期](../Page/清朝.md "wikilink")、清朝後期、近代建築時期。\[1\]其建築功能多樣化，包括中式[廟宇](../Page/廟宇.md "wikilink")、西式[教堂](../Page/教堂.md "wikilink")、居住建築、公共建築和商業建築等。

隨著城市的發展，城市建築上形成多元文化的共生，中西混合的建築更形成了澳門獨特的建築風格。\[2\]城市發展所興建的新建築物，與具歷史價值的建築物和諧共融。一些具價値的建築物通過內部改建以適合現代社會用途，這既保留了建築物的外貌，亦保存了其歷史文化元素。由於在[澳門回歸後推動旅遊博彩業和人口的上升](../Page/澳門回歸.md "wikilink")，澳門進而有多項大型[基建工程](../Page/基建.md "wikilink")、[酒店項目和新型的住宅建築群](../Page/酒店.md "wikilink")，這都為澳門建築注入了更多現代化都市元素。具歷史價值的古老建築物與不同時期興建的新式建築的並存，共同印證了澳門的城市歷史變遷與建築發展。

## 種類

[Linfungtemple.JPG](https://zh.wikipedia.org/wiki/File:Linfungtemple.JPG "fig:Linfungtemple.JPG")

### 中式廟宇

中式廟宇在各歷史時期都有修建，一般規模較小，有部分得以擴建至現時規模。[廟宇基本為中國傳統廟宇風格](../Page/廟宇.md "wikilink")，其中以硬山式與歇山式屋頂居多。廟宇主入口多用方形石柱和石枋，其上常有橫匾和石雕裝飾；建築結構仍採用抬樑式木構架，屋脊高起透空。澳門中式廟宇比較典型例子，有[媽閣廟和](../Page/媽閣廟.md "wikilink")[蓮峰廟等](../Page/蓮峰廟.md "wikilink")。昔日華人商賈有習慣在重要廟宇議事，因而出現了結合議事和廟宇功能的建築。其中知名的[三街會館](../Page/三街會館.md "wikilink")（關帝古廟）就屬此類多功能的埸所，連昔日政府也曾以此為聯繫華人的機關。

[Mocathedral.JPG](https://zh.wikipedia.org/wiki/File:Mocathedral.JPG "fig:Mocathedral.JPG")

### 西式教堂

早在16世紀後期，[教堂與](../Page/教堂.md "wikilink")[炮台都在具海防作用的山脊上陸續興建](../Page/澳門炮台.md "wikilink")。這些教堂建築同時勾勒了早期[澳門舊城區的雛型](../Page/澳門舊城.md "wikilink")，成為其後城市發展的骨架。19世紀中期至20世紀初期，不少[天主教及](../Page/天主教.md "wikilink")[基督新教的教堂都在此時期修建](../Page/基督新教.md "wikilink")。其建築風格呈多樣化，有[新古典式](../Page/新古典式.md "wikilink")(Neo-Classic
Style)的望廈嘉諾撒修院和[聖母聖誕堂](../Page/聖母聖誕堂.md "wikilink")、[羅馬式](../Page/羅馬式.md "wikilink")(Romanesque
Style)的[基督教墳場馬禮遜堂和](../Page/基督教墳場.md "wikilink")[西望洋聖堂](../Page/西望洋聖堂.md "wikilink")、[巴洛克風格](../Page/巴洛克建筑.md "wikilink")(Baroque
Style)的[大三巴](../Page/大三巴.md "wikilink")、[哥德式](../Page/哥特式建筑.md "wikilink")(Gothic
Style)的[聖味基墳場小堂等](../Page/聖味基墳場.md "wikilink")。而[聖母雪地殿教堂內部建築保留了](../Page/聖母雪地殿教堂.md "wikilink")17世紀葡萄牙的[修院特色](../Page/修院.md "wikilink")，祭衣房的[壁畫則揉合了中西畫技](../Page/壁畫.md "wikilink")，其藝術價值更見突顯。

### 居住建築

[Sun_Yat_Sen_Memorial_House.jpg](https://zh.wikipedia.org/wiki/File:Sun_Yat_Sen_Memorial_House.jpg "fig:Sun_Yat_Sen_Memorial_House.jpg")
[Taipa_Houses.jpg](https://zh.wikipedia.org/wiki/File:Taipa_Houses.jpg "fig:Taipa_Houses.jpg")
澳門華人古舊的民居住宅建築，一般有兩種形式。一為院落式建築，磚雕大門，雙重板瓦透空屋面，此例子有[鄭家大屋](../Page/鄭家大屋.md "wikilink")。另一種為聯排式竹筒形鋪屋，青瓦屋頂，建築呈長方形，兩側與其他住宅相連，採用承重牆結構，住宅實例有[福隆新街的民居村里](../Page/福隆新街.md "wikilink")。至於一些華人住宅，則引進不同的建築風格在其住宅建築上。[伊斯蘭建築風格的](../Page/伊斯蘭建築.md "wikilink")[澳門國父紀念館建築及蘇州古典](../Page/澳門國父紀念館.md "wikilink")[園林式的](../Page/园林景观设计.md "wikilink")[盧廉若公園](../Page/盧廉若公園.md "wikilink")，兩者可謂各具特色。而在中式典型民居建築基礎上結合西方設計特色的例子，則有[盧家大屋和](../Page/盧家大屋.md "wikilink")[鄭家大屋](../Page/澳門鄭家大屋.md "wikilink")。

澳門葡人古舊的民居住宅建築，普遍仿照西方式樣，反映葡萄牙的傳統建築風格。例如[南灣沿岸貴族住宅](../Page/南灣_\(澳門\).md "wikilink")，建築物帶有殖民地色彩，強調入口的三角形門楣，進門後為前廳，廳內有樓梯通往二樓起居主層。葡萄牙別墅式的住宅是[土生葡人經常採用的建築形式](../Page/土生葡人.md "wikilink")，典型例子[龍環葡韻住宅式博物館採用簡化了古典元素](../Page/龍環葡韻住宅式博物館.md "wikilink")，帶有[南歐鄉土特色](../Page/南歐.md "wikilink")。

及至現代，住宅普遍已是不同的[現代式的](../Page/现代主义建筑.md "wikilink")[大廈樓宇為主](../Page/大廈.md "wikilink")。隨著土地資源缺乏及人口上升等問題，建築師及發展商在設計上引進[空中花園概念](../Page/空中花園.md "wikilink")，以求充分利用空間角落突破土地空間的限制。\[3\]住宅建築物已由單棟數層的[唐樓轉為高層大廈](../Page/唐樓.md "wikilink")，再繼而發展為[屋苑式建築群](../Page/屋苑.md "wikilink")，善用平台創造花園和康樂設施空間。不同時期的現代式的住宅樓宇除了解決基本的住屋問題外，亦推進了現代化居住建築的發展。

[Red_market.jpg](https://zh.wikipedia.org/wiki/File:Red_market.jpg "fig:Red_market.jpg")

### 公共建築

早期中式的官衙建築有[議事亭](../Page/議事亭.md "wikilink")、[澳門關部行台和稅館等](../Page/澳門關部行台.md "wikilink")，可惜至今已不復存在。至於西式的公共建築，則隨葡萄牙在澳門的政治擴張而得以建設。西式公共建築例子豐富，有西方古典式的[民政總署大樓和](../Page/民政總署大樓.md "wikilink")[郵政總局大樓](../Page/澳門郵政總局大樓.md "wikilink")、伊斯蘭建築風格的[港務局大樓](../Page/港務局大樓.md "wikilink")、新古典風格的[仁慈堂大樓和](../Page/仁慈堂.md "wikilink")[崗頂劇院](../Page/崗頂劇院.md "wikilink")、[折衷主義的](../Page/折衷主義.md "wikilink")[澳門中央圖書館大樓](../Page/澳門中央圖書館.md "wikilink")、葡萄牙古典式的[澳督府](../Page/澳督府.md "wikilink")、葡萄牙殖民式的[葡萄牙駐港澳總領事館大樓和](../Page/葡萄牙駐港澳總領事館.md "wikilink")[澳門演藝學院大樓](../Page/澳門演藝學院.md "wikilink")、[裝飾藝術](../Page/裝飾藝術.md "wikilink")(Art
Deco)風格的[紅街市大樓等](../Page/紅街市大樓.md "wikilink")，公共建築風格多變而呈現不同的特色面貌。及至現代，政府機構不少設在現代式的大廈內，若如[水坑尾街的公共行政大樓的全棟建築](../Page/水坑尾街.md "wikilink")，普遍屬多功能的綜合體。

### 商業建築

[Bnu-tower01.jpg](https://zh.wikipedia.org/wiki/File:Bnu-tower01.jpg "fig:Bnu-tower01.jpg")澳門分行總部\]\]
澳門作為最早與葡萄牙進行貿易的主要補給[港口](../Page/港口.md "wikilink")，在明清時期已有商業性質的建築物。由於中西貿易主要為海路，商業建築都基本散布在碼頭附近。例如在[內港與貿易有關的建築](../Page/澳門內港.md "wikilink")，就包括了商行和[倉庫](../Page/倉庫.md "wikilink")、昔日亦有[攤販](../Page/攤販.md "wikilink")。至於人口密集的華人社區，則有與店舖結合的住宅建築。而澳門自博彩業在葡萄牙的管治之下合法化，各式的賭場、銀行和商廈建築都隨城市經濟發展而興建，地標式的建築包括有[葡京酒店和](../Page/葡京酒店.md "wikilink")[大西洋銀行](../Page/大西洋銀行.md "wikilink")。另外，[澳門舊城內外都在城市建築時期開始建成不少富有特色的](../Page/澳門舊城.md "wikilink")[當鋪和](../Page/當鋪.md "wikilink")[酒店建築](../Page/酒店.md "wikilink")，包括[德成按](../Page/德成按.md "wikilink")、[六國飯店和](../Page/六國飯店.md "wikilink")[總統酒店等](../Page/新中央酒店.md "wikilink")。自[澳門政府在](../Page/澳門政府.md "wikilink")2002年開放賭權，新式的賭場酒店在[新口岸新填海區和](../Page/新口岸新填海區.md "wikilink")[路氹城一帶陸續落成](../Page/路氹城.md "wikilink")，其中不少更仿照[拉斯維加斯的建築形式](../Page/拉斯維加斯.md "wikilink")。

## 相關連結

  - [澳門歷史城區](../Page/澳門歷史城區.md "wikilink")
  - [澳門填海造地](../Page/澳門填海造地.md "wikilink")
  - [澳門舊城](../Page/澳門舊城.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連線

  - [澳門文物網 -
    澳門文物](https://web.archive.org/web/20080908081104/http://www.macauheritage.net/info/)
  - [澳門高層住宅建築](http://www.macauapartments.blogspot.com)

[澳門建築](../Category/澳門建築.md "wikilink")
[Category:澳門文化](../Category/澳門文化.md "wikilink")

1.  澳門歷史十五講，華夏文化藝術出版社，邢榮發編，ISBN 962-8700-72-3
2.  澳門建築文化遺產，東南大學出版社，ISBN 7-81089-786-1
3.  [「空中花園與優質生活」研討會](http://www.macaudeveloper.com/activities/news200503.htm)，建置季刊
    2005年3月18日，澳門建築置業商會