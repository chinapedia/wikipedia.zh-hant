{{ RoughTranslation }}

**萨拉热窝事件**（），又称**弗朗茨·斐迪南大公及苏菲，霍恩贝格公爵夫人遇刺案**（），是指1914年6月28日[奥匈帝国皇储](../Page/奥匈帝国.md "wikilink")[弗朗茨·斐迪南及其妻子霍恩贝格公爵夫人](../Page/弗朗茨·斐迪南.md "wikilink")遭到萨拉热窝刺客六人组（五名[塞尔维亚人](../Page/塞尔维亚人.md "wikilink")，一名[波斯尼亚人](../Page/波斯尼亚人.md "wikilink")）成员之一的[加夫里洛·普林西普枪杀的事件](../Page/加夫里洛·普林西普.md "wikilink")。该刺客组织由[波斯尼亚塞尔维亚人](../Page/波斯尼亚与赫塞哥维纳的塞尔维亚人.md "wikilink")、黑手秘密社团成员领导。刺杀事件的政治目标是断绝[南部斯拉夫民族省份与奧匈帝国的联系](../Page/南部斯拉夫民族.md "wikilink")，给合并成[南斯拉夫带来可能](../Page/南斯拉夫.md "wikilink")。刺客的动机与后来的波斯尼亚青年运动一致。

這次刺殺事件導致奧匈帝國決定向塞爾維亞開戰，在1914年7月28日，奥匈帝国向[塞尔维亚王国发出的最后通牒被部分驳回后](../Page/塞尔维亚王国_\(近代\).md "wikilink")，奧匈向塞爾維亞宣戰，一星期內奧匈的盟友[德國](../Page/德國.md "wikilink")，與塞爾維亞的盟友[俄羅斯](../Page/俄羅斯.md "wikilink")，以及俄羅斯的盟友[法國與](../Page/法國.md "wikilink")[英國紛紛加入戰爭](../Page/英國.md "wikilink")，直接导致了[第一次世界大战的爆发](../Page/第一次世界大战.md "wikilink")。

[Sarajevo_princip_bruecke.jpg](https://zh.wikipedia.org/wiki/File:Sarajevo_princip_bruecke.jpg "fig:Sarajevo_princip_bruecke.jpg")。\]\]

## 背景

[Franz_ferdinand.jpg](https://zh.wikipedia.org/wiki/File:Franz_ferdinand.jpg "fig:Franz_ferdinand.jpg")
根据《[1878年柏林条约](../Page/1878年柏林条约.md "wikilink")》，奥匈帝国获得占领管理奥斯曼[波斯尼亚的授权](../Page/波斯尼亚.md "wikilink")，而[奥斯曼帝国保留官方主权](../Page/奥斯曼帝国.md "wikilink")。根据同一条约，大国集团（奥匈帝国、[英国](../Page/大不列顛及愛爾蘭聯合王國.md "wikilink")、[法国](../Page/法蘭西第三共和國.md "wikilink")、[德国](../Page/德意志帝國.md "wikilink")、[意大利](../Page/意大利王國_\(1861年–1946年\).md "wikilink")、奥斯曼帝国和[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")）官方上认可[塞尔维亚公国为完全主权国家](../Page/塞尔维亚公国.md "wikilink")。四年後，塞尔维亚公国國君[米蘭一世將國家由公國變為](../Page/米蘭一世_\(塞爾維亞\).md "wikilink")[王国](../Page/王国.md "wikilink")，米蘭一世亦成為獨立後首個塞爾維亞國王。当时塞尔维亚的君主隶属于奥布仁诺维奇王室，仍与奥匈帝国保持着密切的外交关系，在米蘭一世及其繼位者[亞歷山大一世在位時](../Page/亞歷山大一世_\(塞爾維亞\).md "wikilink")，塞爾維亞满足于统治条约所界定边境的领土。

1903年發生的塞爾維亞五月政變，改變了塞爾維亞的對外關係。當时塞尔维亚军官德拉古廷·迪米特里耶维奇率兵攻占塞尔维亚皇宫，並殺死国王亞歷山大一世及王后，奥布仁诺维奇王朝終結。軍人擁立十九世紀初帶領塞爾維亞成功爭取自治的民族英雄[卡拉喬爾傑·彼得羅維奇之後人](../Page/卡拉喬爾傑·彼得羅維奇.md "wikilink")[彼得一世為新國王](../Page/彼得一世_\(塞爾維亞\).md "wikilink")。新王朝建立後，塞爾維亞更加奉行[民族主义](../Page/民族主义.md "wikilink")，与俄罗斯关系和善，却对奥匈帝国不太友好。随后的十年中，塞尔维亚与邻国之间的纷争不断，塞尔维亚转为建立权利，逐步恢复14世纪的[帝国](../Page/塞尔维亚帝国.md "wikilink")。这些冲突包括1906年与奥匈帝国爆发的[关税之争](../Page/海关.md "wikilink")（史称[猪战](../Page/猪战.md "wikilink")）、塞尔维亚假装抗议奥匈帝国吞并[波斯尼亚和](../Page/波斯尼亚.md "wikilink")[黑塞哥维那的](../Page/赫塞哥維納.md "wikilink")1908年-1909年[波斯尼亚危机](../Page/波斯尼亚危机.md "wikilink")（最终于1909年3月以塞尔维亚默许不索偿而结束）以及1912年-1913年塞尔维亚从奥斯曼帝国的手中征服[马其顿和](../Page/马其顿.md "wikilink")[科索沃](../Page/科索沃.md "wikilink")，驱逐[保加利亚的](../Page/保加利亚.md "wikilink")[巴尔干战争](../Page/巴尔干战争.md "wikilink")。

[gavrilloprincip.jpg](https://zh.wikipedia.org/wiki/File:gavrilloprincip.jpg "fig:gavrilloprincip.jpg")

塞尔维亚的军事胜利与塞尔维亚人对奥匈帝国吞并波黑的愤慨之情，给国内和奥匈帝国塞族中的民族主义者壮了胆，奥匈帝国塞族对奥匈帝国的统治恼怒成羞，他们的民族主义情绪被塞族“文化”组织煽动。接下来的五年引发1914年孤胆刺客——大部分是奥匈帝国的塞族公民——在克罗地亚和波黑开展一系列针对奥匈帝国官员的暗杀行动，但均未获成功。刺客受到塞尔维亚的零星支持。

1910年6月15日，谋杀波黑铁腕首长未遂。22岁的泽拉吉奇是黑塞哥维那[內韦西涅的东正教徒](../Page/內韦西涅.md "wikilink")，就读于，经常前往[贝尔格莱德](../Page/贝尔格莱德.md "wikilink")。泽拉吉奇身中五枪，脑部的致命一枪启发包括普林西普及其同伙卡布里诺维奇等后来的刺客。普林西普表示：“泽拉吉奇是我第一个模范。17岁时我路过他的坟墓，反映出我们的悲惨状况和他们的思想。正是在那儿我下定决心迟早犯下暴行。”

1913年，皇帝[弗朗茨·约瑟夫吩咐斐迪南大公计划于](../Page/弗朗茨·约瑟夫一世.md "wikilink")1914年6月视察波斯尼亚的军事演习。演习完毕后，斐迪南计划携妻参观[萨拉热窝在新址建设的新国家博物馆](../Page/萨拉热窝.md "wikilink")。公爵夫人索菲的大儿子马克西米连公爵表示：“母亲担心父亲的安全，就陪着他。”

由于“捷克的伯爵夫人被奥地利法院视为平民”，弗朗茨·约瑟夫皇帝才同意两人的婚事，但开出子孙永远不能继承皇位的条件。6月28日是这两人[贵庶通婚的](../Page/贵庶通婚.md "wikilink")14周年纪念日。正如历史学家[A·J·P·泰勒指出](../Page/A·J·P·泰勒.md "wikilink")：“

[Postcard_for_the_assassination_of_Archduke_Franz_Ferdinand_in_Sarajevo.jpg](https://zh.wikipedia.org/wiki/File:Postcard_for_the_assassination_of_Archduke_Franz_Ferdinand_in_Sarajevo.jpg "fig:Postcard_for_the_assassination_of_Archduke_Franz_Ferdinand_in_Sarajevo.jpg")

弗朗茨·斐迪南是[发展联邦制的拥护者](../Page/大奥地利合众国.md "wikilink")，被认为广泛赞成三权分立，即在奥匈帝国的统治下可将奥匈帝国境内的斯拉夫族地区重组为第三个皇室。斯拉夫王国可能是针对塞族[领土收复主义的堡垒](../Page/领土收复主义.md "wikilink")，斐迪南大公因此被认为同是领土收复主义者的威胁。普林西普后来向法院陈述，防止斐迪南大公计划改革是他的动机之一。

暗杀当天的6月28日（[儒略历](../Page/儒略历.md "wikilink")6月15日）是[圣维特节](../Page/圣维特.md "wikilink")。在塞尔维亚它被称为Vidovdan，用来纪念1389年因一名塞族人在帐篷内被苏丹人刺杀而爆发的、反对奥斯曼帝国的[科索沃战役](../Page/科索沃战役_\(1389年\).md "wikilink")。

## 准备

## 事發經過

[Uniform_worn_by_Ferdinand_when_he_was_assassinated_in_Sarajevo.jpg](https://zh.wikipedia.org/wiki/File:Uniform_worn_by_Ferdinand_when_he_was_assassinated_in_Sarajevo.jpg "fig:Uniform_worn_by_Ferdinand_when_he_was_assassinated_in_Sarajevo.jpg")\]\]
1914年6月，奥匈帝国皇储斐迪南大公到新近被奥匈帝国吞并的波斯尼亚检阅军事演习。这次军事演习又是以塞尔维亚为假想敌人的，因而引起塞尔维亚民族主义者的仇恨。

1914年6月28日上午約十點，奧國王儲斐迪南大公夫婦抵達萨拉热窝火車站。數百名民眾熱情的迎接，而在人群中則有七名刺客在伺機而動。斐迪南夫妇坐上敞蓬禮車前往市府大廳，途中經過其中兩位刺客面前，但二人因膽怯沒有下手，後來斐迪南夫妇的車隊遭到第三名刺客[内德利科·查布林諾維奇](../Page/内德利科·查布林諾維奇.md "wikilink")（Nedeljko
Čabrinović）用[手榴弹袭击](../Page/手榴弹.md "wikilink")，但大公下意識的手一揮，手榴弹滾到了車後，立即爆炸。夫婦二人幸運避过一劫，只有一些護衛及圍觀群眾受輕傷。刺客内德利科·查布林諾維奇馬上服下氰化物並跳河企圖自殺不遂，其後被趕至軍警拘捕。

之後，斐迪南大公改變預定行程，決定要到醫院探視一名受輕傷的副官，但隨從人員忘了通知領頭車隊，因此車隊依然照原訂計畫行駛。不幸的是，大公的敞蓬禮車在前往拉丁大橋時，於街角停下，一名叫[普林西普的刺客以離斐迪南王儲夫婦不到兩米的距離用手槍向兩人各射出一發子彈](../Page/普林西普.md "wikilink")，行兇後[普林西普服下氰化物企圖自殺](../Page/普林西普.md "wikilink")，但因為藥物過期而失敗，手槍被打落地後被逮捕，而斐迪南大公夫婦則雙雙送醫不治身亡。

## 影视作品

  - [南斯拉夫](../Page/南斯拉夫.md "wikilink")1975年电影：《[萨拉热窝谋杀事件](../Page/萨拉热窝谋杀事件.md "wikilink")》

## 脚注

## 参考文献

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部链接

  - [斐迪南大公遇刺时的欧洲地图](http://maps.omniatlas.com/europe/19140628/)
  - [有关斐迪南大公遇刺的新闻纪录片](http://www.europeanfilmgateway.eu/node/33/franz%20ferdinand%20efg1914/multilingual:1/showOnly:video)
  - [遇刺案后在监狱访问普林西普的报道](https://libcom.org/history/did-teenage-anarchists-trigger-world-war-one-what-was-politics-assassins-franz-ferdinand)

[Category:奧匈帝國與第一次世界大戰](../Category/奧匈帝國與第一次世界大戰.md "wikilink")
[Category:萨拉热窝历史](../Category/萨拉热窝历史.md "wikilink")
[Category:暗殺](../Category/暗殺.md "wikilink")
[Category:简易爆炸装置爆炸事件](../Category/简易爆炸装置爆炸事件.md "wikilink")
[Category:1914年欧洲](../Category/1914年欧洲.md "wikilink")
[Category:1914年6月](../Category/1914年6月.md "wikilink")