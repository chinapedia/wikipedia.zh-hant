**德国铁路605型**是一款高速[柴聯車](../Page/柴聯車.md "wikilink")，于[德国铁路和](../Page/德国铁路.md "wikilink")[丹麦国家铁路服役](../Page/丹麦国家铁路.md "wikilink")，通常以**ICE-TD**的名称为人所知。ICE-TD與[ICE-T一樣屬於](../Page/ICE-T列車.md "wikilink")[傾斜技術](../Page/擺式列車.md "wikilink")，它的設計同樣是以行走已鋪設了的路線，在地理上多彎、上落斜度大的山林區，以及未電氣化的路段為主。但是由於在環保和列車結構方面仍存在不少缺陷，曾有段時間不使用ICE-TD列車作正常營運（客運），只作為貨運使用於東部[開姆尼茨地區](../Page/開姆尼茨.md "wikilink")。由2006年復活節開始，ICE-TD開始重新被使用，屬於特殊及加班列車，在[2006年世界盃足球賽期間完成了出色的運載任務](../Page/2006年世界盃足球賽.md "wikilink")。

## 开发

[ICE-TD_Berlin_Ostbahnhof.jpg](https://zh.wikipedia.org/wiki/File:ICE-TD_Berlin_Ostbahnhof.jpg "fig:ICE-TD_Berlin_Ostbahnhof.jpg")
1991年成功创立了[城际特快列车](../Page/城际特快列车.md "wikilink")（ICE）系统和着手开发[ICE-2列车后](../Page/ICE-2列车.md "wikilink")，德国铁路于1994年开始规划对行走传统线路的长途服务升级。通过使用[鐘擺式](../Page/摆式列车.md "wikilink")[電聯車替代](../Page/電聯車.md "wikilink")[机车牵引的](../Page/机车.md "wikilink")[城际列车](../Page/城际列车.md "wikilink")（IC）和[区际列车](../Page/区际列车.md "wikilink")（IR），获得更高的速度，使舒适度提升至接近[ICE的水平](../Page/城际特快列车.md "wikilink")。德国铁路以“IC-T”作为项目名称，其中的“T”代表*Triebzug*（動力分散式）。这一研发计划的产物便是[ICE-T列车](../Page/ICE-T列车.md "wikilink")。

后来，德铁同样发现在非电气化区间亦有对相似新型列车的需求，并启动了“*ICT-VT*”项目（“VT”代表*Verbrennungstriebwagen*，即“柴聯車”）。
[ICE_TD_2.Klasse.jpg](https://zh.wikipedia.org/wiki/File:ICE_TD_2.Klasse.jpg "fig:ICE_TD_2.Klasse.jpg")
德铁同时推进[ICE-3](../Page/ICE-3列车.md "wikilink")、IC-T和ICT-VT三个项目。上述车型共用同一设计理念，最为显著的是在列车前端设有可展望前方轨道的豪华坐席，与列车驾驶室仅有一道玻璃墙分隔。且这些车型在组件和布局设计方面拥有不少共通之处，尤其是[动力分散式设计](../Page/动力分散式.md "wikilink")——相对于[ICE-1和](../Page/ICE-1列车.md "wikilink")[ICE-2的设计](../Page/ICE-2列车.md "wikilink")，新列车未设专门机车，而是用分散的车底动力装置取代，以降低轴重（对摆式列车相当重要）和提升牵引效率。

ICT-VT选用了4节编组方案，未编入餐车；此外德铁以与IC-T外观和最大化技术通用性为目标，甚至可将电动和柴油动力列车重联运行。

## 生产

1996年德国铁路向以西门子公司为首的财团提交了20列柴聯車的订单。
[Wikimedia_Chapters_Conference_151.JPG](https://zh.wikipedia.org/wiki/File:Wikimedia_Chapters_Conference_151.JPG "fig:Wikimedia_Chapters_Conference_151.JPG")
[ICE-D_BR605_Drehgestell_LWS1339.jpg](https://zh.wikipedia.org/wiki/File:ICE-D_BR605_Drehgestell_LWS1339.jpg "fig:ICE-D_BR605_Drehgestell_LWS1339.jpg")
在[ICE-T生产中](../Page/ICE-T列车.md "wikilink")，DWA（德国铁路车辆，庞巴迪子公司）制造两端车厢，西门子负责制造中间车厢。ICE-TD以西门子研发的电气-机械摆动机构代替了ICE-T上的[菲亚特](../Page/菲亚特.md "wikilink")（[阿尔斯通](../Page/阿尔斯通.md "wikilink")）的[潘多利诺](../Page/潘多利诺.md "wikilink")（Pendolino）液压系统。转向架与车体间的次级悬挂亦从金属弹簧改为空气悬挂，以提供更高的舒适度。\[1\]

摆动机构也在转向架的两轴为电动机留下了空间，故[柴电列车的每节车厢都有一动力转向架和一无动力转向架](../Page/柴电动力.md "wikilink")（2'Bo'
配置）。\[2\]此车型电动机所使用的电力来自于4台柴油机（单台功率560kW，每节车厢各一），这些柴油机是基于卡车发动机而来。列车从中分为两组独立的动力单元（两节车厢一组），为日后在中间加入拖车作为第5节车厢提供了理论可能。

第一列ICE-TD于1998年组装，次年4月开始路上测试；并在2000年1月13日的一次测试运行中达到了222km/h的速度。\[3\]\[4\]

在1999年第一列IC-T投入运营之前，电动和柴油列车分别更名为*ICE-T*和*ICE-TD*；“T”被改为代表“[摆式列车](../Page/摆式列车.md "wikilink")”（*tilt(ing)*），“TD”则是“摆式-柴油”（*tilt(ing)-diesel*）。

## 服务历史

[ICE-TD_in_Ferry.JPG](https://zh.wikipedia.org/wiki/File:ICE-TD_in_Ferry.JPG "fig:ICE-TD_in_Ferry.JPG")的渡轮上\]\]
2001年，全部20列ICE-TD交付予德铁，并被编为605型。在试运行后，当年6月10日，列车在[德累斯顿与](../Page/德累斯顿.md "wikilink")[慕尼黑间的ICE](../Page/慕尼黑.md "wikilink")
17线投入服务。稍后列车也运用于[慕尼黑和](../Page/慕尼黑.md "wikilink")[苏黎世之间](../Page/苏黎世.md "wikilink")。

605型的服役历史相当不幸。自始ICE-TD便被技术问题所困扰。

在2002年12月2日发生了车轴断裂事故后，所有现存的19列编组（另1列在2001年9月从工作平台上坠落，致使其中两节车厢被迫报废拆解\[5\]）停运。尽管在一年后ICE-TD恢复服务，但德铁认定运营此车型过于昂贵——德铁需要为燃料缴付全额[燃油税](../Page/燃油税.md "wikilink")。

为应对[2006年德国世界杯带来的额外交通需求](../Page/世界杯足球赛.md "wikilink")，ICE-TD被重新启用。自当年复活节起，它被用于包租和疏导班次。
[ICE_TD_in_Copenhagen_20090503-DSCF2037.jpg](https://zh.wikipedia.org/wiki/File:ICE_TD_in_Copenhagen_20090503-DSCF2037.jpg "fig:ICE_TD_in_Copenhagen_20090503-DSCF2037.jpg")
自2007年年末，605型（ICE-TD）被用于行走[柏林](../Page/柏林.md "wikilink")-[汉堡](../Page/汉堡.md "wikilink")-[哥本哈根的常规路线](../Page/哥本哈根.md "wikilink")，作为的替代（本应于更早前被汰换，但其因技术问题而未被丹麦国铁接收）。用于此服务的10列编组605型装备了丹麦的[列车自动控制系统](../Page/列车自动控制系统.md "wikilink")。2008年起，兼容丹麦系统的605型也被用于行走柏林-汉堡-[奥胡斯的班次](../Page/奥胡斯.md "wikilink")（此前IC3被运用于[弗伦斯堡](../Page/弗伦斯堡.md "wikilink")-[奥胡斯](../Page/奥胡斯.md "wikilink")，以及汉堡-弗伦斯堡）。自2009年年中，再有3列ICE-TD编组被运用于德国-丹麦间的列车服务，令丹麦国铁可抽调更多的IC3型列车用于本地服务。\[6\]

## 参见

  - [高速铁路列表](../Page/高速铁路列表.md "wikilink")
  - [城际特快列车](../Page/城际特快列车.md "wikilink")
  - [摆式列车](../Page/摆式列车.md "wikilink")
  - [ICE-T列车](../Page/ICE-T列车.md "wikilink")

## 参考来源

## 外部链接

  - [ICE
    TD](https://web.archive.org/web/20080510113637/http://references.transportation.siemens.com/refdb/showReference.do?r=293&div=7&l=en)
    Siemens Page
  - [ICT-VT - tilting InterCity DMU
    (Class 605)](http://www.railfaneurope.net/ice/ict-d.html) at [The
    ICE Pages](http://www.railfaneurope.net/ice/ice.html)
  - [ICE-TD列車（海子鐵路網）](https://web.archive.org/web/20070927002318/http://bbs.hasea.com/viewthread.php?tid=170756)

[Category:城际快车](../Category/城际快车.md "wikilink")
[Category:摆式列车](../Category/摆式列车.md "wikilink")
[Category:西門子製鐵路車輛](../Category/西門子製鐵路車輛.md "wikilink")
[Category:龐巴迪製鐵路車輛](../Category/龐巴迪製鐵路車輛.md "wikilink")
[Category:德國柴油動車組](../Category/德國柴油動車組.md "wikilink")

1.
2.

3.
4.  Heinz Kurz: InterCityExpress - Die Entwicklung des
    Hochgeschwindigkeitsverkehrs in Deutschland, Seite 264

5.  Bei Abweichungen der Wagenreihung gegenüber der ursprünglichen
    Zulassung respektive Auslieferung sind die nachträglich getauschten
    Wagen **fett** dargestellt. <small>(Stand: Dezember 2012)</small>

6.