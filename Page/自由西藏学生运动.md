[Students_for_a_Free_Tibet_members_protested_against_China_in_front_of_the_White_House_自由西藏學生運動成員於美國白宮前抗議中共與胡錦濤.jpg](https://zh.wikipedia.org/wiki/File:Students_for_a_Free_Tibet_members_protested_against_China_in_front_of_the_White_House_自由西藏學生運動成員於美國白宮前抗議中共與胡錦濤.jpg "fig:Students_for_a_Free_Tibet_members_protested_against_China_in_front_of_the_White_House_自由西藏學生運動成員於美國白宮前抗議中共與胡錦濤.jpg")[白宮前抗議中共與胡錦濤](../Page/白宮.md "wikilink")\]\]
[Students_for_a_Free_Tibet_Protesters_marched_to_Lafayette_Park_from_the_Chinese_Embassy_in_D.C._自由西藏學生運動抗議者於美國華府從中國大使館遊行至拉法葉公園.jpg](https://zh.wikipedia.org/wiki/File:Students_for_a_Free_Tibet_Protesters_marched_to_Lafayette_Park_from_the_Chinese_Embassy_in_D.C._自由西藏學生運動抗議者於美國華府從中國大使館遊行至拉法葉公園.jpg "fig:Students_for_a_Free_Tibet_Protesters_marched_to_Lafayette_Park_from_the_Chinese_Embassy_in_D.C._自由西藏學生運動抗議者於美國華府從中國大使館遊行至拉法葉公園.jpg")從中國大使館遊行至拉法葉公園\]\]
**自由西藏学生运动**（**Students for a Free
Tibet**，简称**SFT**）是一个学生领导的[非盈利性组织](../Page/非盈利性组织.md "wikilink")。其宗旨是推进[西藏独立](../Page/西藏独立.md "wikilink")。

## 历史

自由西藏学生运动于1994年成立于[纽约市](../Page/纽约市.md "wikilink")。其发起人，包括[藏人](../Page/藏人.md "wikilink")，学生及其支持者，希望通过青年人的影响力来促成西藏的[人权保护和独立](../Page/人权.md "wikilink")。该组织最早的活动包括通过一些音乐巡回演出（例如“西藏自由音乐会”）在青年学生中推广对[西藏问题的知晓度](../Page/西藏问题.md "wikilink")。

## 活动

自由西藏学生运动的活动大致可分为两大部分：**任务**和**领导能力培训**。

SFT的任务包括三个方面：政治，经济和人权。政治任务主要是对中华人民共和国官方以及其政府代表在西藏人权和主权问题上施加压力。SFT最广为知晓的活动是在中华人民共和国高级官员出访他国时在其途经处进行抗议活动。SFT的经济任务主要是阻碍外国企业和政府对资助中华人民共和国在西藏的管理和建设，具体措施的例子包括抵制中国生产的产品，以及成功阻碍[世界银行对中国在西藏的一些有争议的建设项目的贷款](../Page/世界银行.md "wikilink")。\[[https://web.archive.org/web/20070805061153/http://www.ciel.org/Ifi/pressreleasefinaltibet.html\]SFT的人权任务包括呼吁提高西藏人的自由与人权，以及向中国政府施压要求其释放相关的](https://web.archive.org/web/20070805061153/http://www.ciel.org/Ifi/pressreleasefinaltibet.html%5DSFT的人权任务包括呼吁提高西藏人的自由与人权，以及向中国政府施压要求其释放相关的)[政治犯和](../Page/政治犯.md "wikilink")[异议者](../Page/持不同政见者.md "wikilink")。

SFT的领导能力培训项目着眼于发展未来的活动领导人。

## 參見條目

  - [自由西藏](../Page/自由西藏.md "wikilink")

## 外部链接

  - [自由西藏学生运动网站](http://studentsforafreetibet.org/)
  - [自由西藏學生運動的活動紀錄](http://www.flickr.com/people/sfthq/)

[Category:非营利组织](../Category/非营利组织.md "wikilink")
[Category:西藏獨立運動組織](../Category/西藏獨立運動組織.md "wikilink")