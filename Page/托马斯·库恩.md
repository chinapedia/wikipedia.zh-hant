**托马斯·塞缪尔·库恩**（，），[美国](../Page/美国.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")、[科學史學家和](../Page/科學史學家.md "wikilink")[科学哲学家](../Page/科学哲学家.md "wikilink")，代表作为《哥白尼革命》和《科学革命的结构》。

其最有名的著作《[科学革命的结构](../Page/科学革命的结构.md "wikilink")》（*The Structure of
Scientific
Revolutions*，1962年），為當代的科學思想研究建立了一個廣為人知的討論基礎；不論是贊成或是批評，因此可以說是最有影響力的科學史及科學哲學家，其著作也被引用到科學史之外的其他廣泛領域中。紐約時報認為，因為库恩的這本著作，讓[範式](../Page/範式.md "wikilink")（*paradigm*
）這個詞彙變成當代最常出現的詞彙之一。

## 《科学革命的结构》

《科學革命的結構》最初是由維也納學派的邏輯實證公佈的書《統一科學國際百科全書》其中的一篇文章。
庫恩認為科學不是通過新知識的線性積累進步，而是經歷週期性的革命，也被稱為“[典範轉移](../Page/典範轉移.md "wikilink")”\[1\]，
而其中科學探究的本質內場業主突然轉化。整体上，科學被分成三個不同的階段。第一階段是缺乏中央範式的“先見之明”。其次是當科學家試圖通過“解謎”來擴大該中心的範式的“常規科學”。
在常規科學時期，未能符合範式的結果不是被看成駁斥範例，而是研究者的錯誤。不斷的反常的結果會終究建立起科學的危機。一旦這發生，新的典範可被接受，而這將被稱為革命性的科學。

## 著作

  - The Copernican Revolution (1957)
  - The Structure of Scientific Revolutions (Chicago: University of
    Chicago Press, 1962) (ISBN 0-226-45808-3)
      - 繁體中文 〈科學革命的結構〉- (台北市：遠流，1989) (ISBN 957-32-0811-3)
  - The Essential Tension: Selected Studies in Scientific Tradition and
    Change (1977)
  - Black-Body Theory and the Quantum Discontinuity, 1894-1912 (Chicago,
    1987)
  - The Road Since Structure: Philosophical Essays, 1970-1993 (Chicago:
    University of Chicago Press, 2000) (ISBN 0-226-45798-2)

## 參考資料

## 参见

  - [范式转移](../Page/范式转移.md "wikilink")
  - [科学哲学](../Page/科学哲学.md "wikilink")
  - [科学革命的结构](../Page/科学革命的结构.md "wikilink")
  - [科学革命](../Page/科学革命.md "wikilink")

[湯瑪斯·孔恩](../Category/湯瑪斯·孔恩.md "wikilink")
[Category:美国科学史家](../Category/美国科学史家.md "wikilink")
[Category:美国科学哲学家](../Category/美国科学哲学家.md "wikilink")
[Category:科学哲学家](../Category/科学哲学家.md "wikilink")
[Category:猶太哲學家](../Category/猶太哲學家.md "wikilink")
[Category:美国不可知论者](../Category/美国不可知论者.md "wikilink")
[Category:麻省理工學院教師](../Category/麻省理工學院教師.md "wikilink")
[Category:普林斯頓大學教師](../Category/普林斯頓大學教師.md "wikilink")
[Category:柏克萊加州大學教師](../Category/柏克萊加州大學教師.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:罹患肺癌逝世者](../Category/罹患肺癌逝世者.md "wikilink")
[Category:欧洲文理科学院院士](../Category/欧洲文理科学院院士.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")
[Category:行为科学高等研究中心学者](../Category/行为科学高等研究中心学者.md "wikilink")
[Category:20世纪哲学家](../Category/20世纪哲学家.md "wikilink")

1.