[李先念](../Page/李先念.md "wikilink") |alongside =
[杨静仁](../Page/杨静仁.md "wikilink")、[王任重等](../Page/王任重.md "wikilink")
|office2 = [陕西省委员会第一书记](../Page/中国共产党陕西省委员会.md "wikilink") |term_start2
= 1978年12月 |term_end2 = 1984年5月 |deputy2 =
[于明涛](../Page/于明涛.md "wikilink"){{→}}[李庆伟](../Page/李庆伟.md "wikilink")（省长）
|predecessor2 = [王任重](../Page/王任重.md "wikilink") |successor2 =
[白纪年](../Page/白纪年.md "wikilink") |office3 =
[劳动部部长](../Page/中华人民共和国劳动部.md "wikilink")
|term_start3 = 1954年9月 |term_end3 = 1966年冬 |premier3 =
[周恩来](../Page/周恩来.md "wikilink") |predecessor3 =
[李立三](../Page/李立三.md "wikilink")
（[中央人民政府劳动部](../Page/中央人民政府劳动部.md "wikilink")）
|successor3 = 机构撤销，职能并入[国家计划委员会](../Page/中华人民共和国国家计划委员会.md "wikilink")
|birth_date =  |birth_place =
[Flag_of_the_Republic_of_China_1912-1928.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Republic_of_China_1912-1928.svg "fig:Flag_of_the_Republic_of_China_1912-1928.svg")[中华民国](../Page/中华民国.md "wikilink")[陕西](../Page/陕西.md "wikilink")[米脂](../Page/米脂.md "wikilink")
|death_date =  |death_place = [北京市](../Page/北京市.md "wikilink")
|nationality = |spouse = 孙铭 |party =  |children = 马晓文（子）、马晓力（女）
|alma_mater =
[延安](../Page/延安.md "wikilink")[抗日军政大学](../Page/抗日军政大学.md "wikilink")
|occupation = [革命家](../Page/革命家.md "wikilink") |profession = |signature
= |footnote = }}
**马文瑞**（），[陕西](../Page/陕西.md "wikilink")[子洲人](../Page/子洲.md "wikilink")，[延安](../Page/延安.md "wikilink")[抗日军政大学毕业](../Page/抗日军政大学.md "wikilink")。1926年加入[中国共产主义青年团](../Page/中国共产主义青年团.md "wikilink")，1928年转入[中国共产党](../Page/中国共产党.md "wikilink")，是中共第八届候补中央委员，第十一、十二届中央委员。曾任第六、第七届[全国政协副主席](../Page/全国政协副主席.md "wikilink")，[中共陕西省委](../Page/中共陕西省委.md "wikilink")[第一书记](../Page/第一书记.md "wikilink")，[陕西省人大常委会主任等职](../Page/陕西省人大常委会主任列表.md "wikilink")。

2004年1月3日在北京病逝，享年91岁。

## 生平

马文瑞于[壬子年九月二十六日](../Page/壬子.md "wikilink")（公元1912年11月4日）出生在[陕西](../Page/陕西.md "wikilink")[米脂县马家阳湾村](../Page/米脂县.md "wikilink")（今属[子洲县](../Page/子洲县.md "wikilink")）一个家境殷实的农民家庭。祖父马沼兰，父亲马彦举，以种地为生，兼做小买卖。4岁时，母亲离世；7岁时，家道中落；14岁时，开始接触[马克思主义](../Page/马克思主义.md "wikilink")。\[1\]

早在[大革命时期](../Page/大革命.md "wikilink")，马文瑞就开始参加爱国[学生运动](../Page/学生运动.md "wikilink")；于1926年加入共产主义青年团；1928年11月，当选[共青团](../Page/共青团.md "wikilink")[绥德县委书记](../Page/绥德.md "wikilink")；后不久，转为[中国共产党党员](../Page/中国共产党.md "wikilink")。此后，历任[中共](../Page/中共.md "wikilink")[安定县](../Page/安定县.md "wikilink")（今[子长县](../Page/子长县.md "wikilink")）北区区委书记、县委宣传部长、县委书记，[中共](../Page/中共.md "wikilink")[陕北特委委员](../Page/陕北.md "wikilink")、[陕北特委南路特派员](../Page/陕北.md "wikilink")，[共青团](../Page/共青团.md "wikilink")[陕北特委书记](../Page/陕北.md "wikilink")。1935年，马文瑞当选[陕甘边区东部地区革命委员会主席](../Page/陕甘宁边区.md "wikilink")，创建[赤川](../Page/宜川县.md "wikilink")、[红泉两县的苏维埃政权](../Page/甘泉县.md "wikilink")，时年23岁。1937年，进入[延安](../Page/延安.md "wikilink")[抗日军政大学学习](../Page/抗日军政大学.md "wikilink")。\[2\]

抗日战争时期，马文瑞主要主持[陕北西部](../Page/陕北.md "wikilink")、陇东地区的工作；期间，曾带职入中央党校学习。自1944年秋，历任[中共中央西北局组织部副部长](../Page/中共中央西北局.md "wikilink")、部长，西北局常务委员，西北局党校校长，西北局纪律检查委员会书记，西北人民监察委员会主任；是当时中共中央西北局的主要领导人之一。\[3\]\[4\]

[中华人民共和国建国后](../Page/中华人民共和国.md "wikilink")，1952年冬，在[习仲勋调中央工作后](../Page/习仲勋.md "wikilink")，马文瑞晋升为[中共中央西北局副书记](../Page/中共中央西北局.md "wikilink")、兼组织部部长；1954年9月，在[国务院成立后](../Page/中华人民共和国国务院.md "wikilink")，出任[中华人民共和国劳动部首任部长](../Page/中华人民共和国劳动部.md "wikilink")；于1956年召开的[中共八大上](../Page/中共八大.md "wikilink")，当选中央候补委员。\[5\]

“[文化大革命](../Page/文化大革命.md "wikilink")”开始后，马文瑞受到迫害；于1966年12月免职；在[中共八届十中全会上](../Page/中国共产党第八次全国代表大会#十中全会.md "wikilink")，被确定为
“[习仲勋](../Page/习仲勋.md "wikilink")、[贾拓夫](../Page/贾拓夫.md "wikilink")、[刘景范反党集团](../Page/刘景范.md "wikilink")”成员；在贾拓夫死后，成为“习、马、刘反党集团”的头目之一\[6\]。1977年恢复工作，出任[国家计划委员会副主任](../Page/国家计划委员会.md "wikilink")；1977年底，任[中共中央党校副校长](../Page/中共中央党校.md "wikilink")。\[7\]

1978年12月，马文瑞回到陕西，出任[中共陕西省委](../Page/中共陕西省委.md "wikilink")[第一书记](../Page/第一书记.md "wikilink")、[陕西省军分区第一](../Page/兰州军区.md "wikilink")[政治委员](../Page/政治委员.md "wikilink")；后又当选第五届[陕西省](../Page/陕西省.md "wikilink")[人大常委会主任](../Page/人大常委会.md "wikilink")。任职期间，马文瑞主持了全省的[拨乱反正工作](../Page/拨乱反正.md "wikilink")，争取到拨款整修[西安城墙](../Page/西安城墙.md "wikilink")；并于1983年，首次提出在[咸阳原空军专用机场旧址](../Page/咸阳.md "wikilink")，兴建[新西安机场](../Page/西安咸阳国际机场.md "wikilink")。\[8\]

1984年5月，在[全国政协第六届委员会第二次会议上](../Page/中国人民政治协商会议第六届全国委员会委员名单.md "wikilink")，马文瑞被增补为全国委员会副主席；从此离开陕西，但对陕西工作仍时时关注。\[9\]并于1988年，连任第七届[全国政协副主席](../Page/全国政协副主席.md "wikilink")，同时兼任政协法制委员会主任。1990年初，和[彭真等人倡议并成立了](../Page/彭真.md "wikilink")[中国延安精神研究会](../Page/中国延安精神研究会.md "wikilink")；当年5月，被选举为首任会长。\[10\]\[11\]

## 参考文献

{{-}}

{{-}}

[M马](../Category/中华人民共和国领导人.md "wikilink")
[M马](../Category/中国人民抗日军事政治大学校友.md "wikilink")
[M马](../Category/子洲人.md "wikilink") [W文](../Category/马姓.md "wikilink")
[Category:第七届全国政协副主席](../Category/第七届全国政协副主席.md "wikilink")
[Category:第六届全国政协副主席](../Category/第六届全国政协副主席.md "wikilink")
[Category:中华人民共和国劳动部部长](../Category/中华人民共和国劳动部部长.md "wikilink")
[Category:中共中央党校副校长](../Category/中共中央党校副校长.md "wikilink")
[Category:国家计划委员会副主任](../Category/国家计划委员会副主任.md "wikilink")
[Category:中共陕西省委书记](../Category/中共陕西省委书记.md "wikilink")
[Category:陕西省人大常委会主任](../Category/陕西省人大常委会主任.md "wikilink")

1.

2.
3.  [第六、七届全国政协副主席马文瑞简介](http://www.no1story.com/html/report/7414-1.htm)

4.
5.
6.  [《反党小说＜刘志丹＞案实录》节选](http://www.htqly.org/detail.aspx?DocumentId=528)
    李建彤 著

7.
8.  [“我想延安”－马文瑞和中国延安精神研究会](http://old.globalview.cn/ReadNews.asp?NewsID=3351)
     原载 2005年第1期《中华魂》杂志

9.  [第六届全国政协历次会议](http://news.xinhuanet.com/ziliao/2002-02/20/content_283440.htm)

10.
11.