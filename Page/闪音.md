**闪音**［，也称**弹音**（）］是[发音方法之一](../Page/发音方法.md "wikilink")，属于[辅音](../Page/辅音.md "wikilink")。发音时，[舌尖向上卷起](../Page/舌.md "wikilink")，但不直接接触[齿](../Page/齿.md "wikilink")[龈或者上](../Page/牙龈.md "wikilink")[颚](../Page/腭.md "wikilink")。气流冲出时，舌尖轻微闪颤一下，与齿龈或上颚接触，瞬间即离开。

[日语的](../Page/日语.md "wikilink")"r-"（ら行）以及[韓語](../Page/韓語.md "wikilink")作初聲時也是[齿龈边闪音](../Page/齿龈边闪音.md "wikilink")。

## IPA 音标

被公认的闪音或弹音的[国际音标是](../Page/国际音标.md "wikilink")：

<table>
<thead>
<tr class="header">
<th><p>IPA</p></th>
<th><p>解释</p></th>
<th><p>例</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>语言</p></td>
<td><p>正字</p></td>
<td><p>IPA</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/齿龈闪音.md" title="wikilink">齿龈闪音</a></p></td>
<td><p><a href="../Page/北美英语.md" title="wikilink">北美英语</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/齿龈边闪音.md" title="wikilink">齿龈边闪音</a></p></td>
<td><p><a href="../Page/日语.md" title="wikilink">日语</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/卷舌闪音.md" title="wikilink">卷舌闪音</a></p></td>
<td><p><a href="../Page/乌尔都语.md" title="wikilink">乌尔都语</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Labiodental_flap_(Gentium).svg" title="fig:Labiodental_flap_(Gentium).svg">Labiodental_flap_(Gentium).svg</a></p></td>
<td><p><a href="../Page/唇齿闪音.md" title="wikilink">唇齿闪音</a></p></td>
<td><p><a href="../Page/莫诺语.md" title="wikilink">莫诺语</a>（<a href="../Page/刚果.md" title="wikilink">刚果</a>）</p></td>
</tr>
</tbody>
</table>

## 闪音的种类

### 齿龈闪音

比较[西班牙语中的](../Page/西班牙语.md "wikilink")[颤音](../Page/颤音.md "wikilink")*pe**rr**o*
 “狗”和齿龈闪音的*pe**r**o* “但”。在[日耳曼语族里](../Page/日耳曼语族.md "wikilink"),
此[同位异音可在](../Page/同位异音.md "wikilink")[美国英语和](../Page/美国英语.md "wikilink")[低地德语中看到](../Page/低地德语.md "wikilink")。

## 參考文獻

  -
{{-}}

[\*](../Category/閃音.md "wikilink")
[Category:辅音](../Category/辅音.md "wikilink")
[Category:發音方法](../Category/發音方法.md "wikilink")