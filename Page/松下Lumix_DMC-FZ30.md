**Panasonic Lumix DMC-FZ30**
是[松下](../Page/松下.md "wikilink")[Lumix系列的一款准专业长焦](../Page/Lumix.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")，于2005年7月发布。它是前作[Panasonic
Lumix
DMC-FZ20的后续产品](../Page/Panasonic_Lumix_DMC-FZ20.md "wikilink")，相对FZ20，FZ30将CCD从1/2.5
'加大到1/1.8 '，像素也提升至800万级别。FZ30的后续作品是[Panasonic Lumix
DMC-FZ50](../Page/Panasonic_Lumix_DMC-FZ50.md "wikilink")。

广受好评的12倍光学变焦（等效35mm相机焦距为35mm ～
420mm）保留在FZ30上，依旧有着Leica标志，但是少了FZ20上的恒定F2.8镜头——在长焦端的420mm，最大光圈为F3.7。依旧配备松下自己的O.I.S.防抖系统，使用锂离子电池和SD卡存储，2英寸的可翻转LCD相对之前嵌入式的LCD，更方便取景。

## 主要参数

  - 成像元件：1/1.8英寸[CCD](../Page/CCD.md "wikilink")
  - 显示：2英寸可旋转TFT显示屏
  - 12倍光学变焦，F/2.8～F/3.7 [光圈](../Page/光圈.md "wikilink")，OIS光学防抖
  - 维纳斯二代影像处理器
  - 短片拍摄：Motion JPEG格式（640×480、30fps），单声道
  - 使用[SD卡作为存储介质](../Page/SD卡.md "wikilink")
  - 使用[锂离子电池](../Page/锂离子电池.md "wikilink")

<File:DMC-FZ30_front2.jpg> <File:DMC-FZ30_profile2.jpg>
<File:DMC-FZ30_top2.jpg> <File:DMC-FZ30_rear2.jpg>

## 参见

  - [Lumix](../Page/Lumix.md "wikilink")
  - [数码相机](../Page/数码相机.md "wikilink")
  - [Panasonic](../Page/松下.md "wikilink")

## 外部链接

  - [松下新高端FZ30真实试用评价](https://web.archive.org/web/20071218183905/http://www.beareyes.com.cn/2/lib/200511/09/20051109028.htm)——小熊在线

[Category:Panasonic數位相機](../Category/Panasonic數位相機.md "wikilink")