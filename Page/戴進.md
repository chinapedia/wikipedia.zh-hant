**戴进**（）[字](../Page/表字.md "wikilink")**文进**，[号](../Page/号.md "wikilink")**静庵**、**玉泉山人**。[浙江](../Page/浙江.md "wikilink")[杭州府](../Page/杭州府.md "wikilink")[錢塘縣人](../Page/錢塘縣.md "wikilink")。[明代](../Page/明代.md "wikilink")[画家](../Page/画家.md "wikilink")。[浙派创始人](../Page/浙派.md "wikilink")\[1\]。

## 生平

早年是金銀首飾工匠，后改学绘画，以賣畫為生，[宣德年间供奉](../Page/宣德.md "wikilink")[宫廷](../Page/宫廷.md "wikilink")，因画艺高超而遭妒忌，\[2\]遂被斥退，寓[京十余年](../Page/京.md "wikilink")。晚年回乡以卖画为生，[天顺六年](../Page/天顺.md "wikilink")（1462天）秋天卒。\[3\]葬于[西湖附近洪春桥畔](../Page/西湖.md "wikilink")\[4\]。

## 画风

擅[山水](../Page/山水.md "wikilink")、[人物](../Page/人物.md "wikilink")、花果、翎毛，用笔劲挺方硬，除了兼法[宋](../Page/宋.md "wikilink")、[元諸家以外](../Page/元.md "wikilink")，发展了[马远](../Page/马远.md "wikilink")、[夏圭](../Page/夏圭.md "wikilink")「院體」一派传统，也吸取北宋[李成](../Page/李成.md "wikilink")、[范寬](../Page/范寬.md "wikilink")、[郭熙](../Page/郭熙.md "wikilink")，俱遒勁蒼潤。人物画师法[唐宋传统](../Page/唐宋.md "wikilink")，遠師[吳道子](../Page/吳道子.md "wikilink")、[李龍眠](../Page/李公麟.md "wikilink")，自創“**蠶頭鼠尾描**”。

戴进的绘画在当时影响很大，追随者甚眾，影響方鉞、戴泉、夏芷、何適、謝賓舉、[謝時臣](../Page/謝時臣.md "wikilink")、汪肇、蔣嵩、夏蔡、[吳偉](../Page/吳偉.md "wikilink")、[張路](../Page/張路.md "wikilink")、王世祥等人，因他是[浙江人](../Page/浙江.md "wikilink")，人称“浙派”，是为[明代前期画坛主流](../Page/明代.md "wikilink")。作品有《春山积翠图》、[《风雨归舟图》](http://www.zwbk.org/MyLemmaShow.aspx?zh=zh-tw&lid=213686)
、《三顾茅庐图》、《达摩至惠能六代像》、《南屏雅集图》、《归田祝寿图》、《葵石蛱蝶图》、《三鹭图》等。

Dai Jin-Wind and Rain Returning by
Boat.jpg|風雨歸舟，台北[国立故宫博物院藏](../Page/国立故宫博物院.md "wikilink")
Dai Jin-Tall Pine Trees and Five Deer.jpg|长松五鹿图，台北国立故宫博物院藏 Dai
Jin-Dropping a Fishing Line on the Bank of the Wei
River.jpg|渭濱垂釣圖，絹本設色，台北国立故宫博物院藏 Dai
Jin-Traveling in Spring Returning at Night.jpg|春遊晚歸图，絹本設色 纵167.9厘米
横83.1厘米 台北國立故宮博物院藏 Dai Jin-Stream Bridge Whipping on a
Donkey.jpg|溪橋策蹇图，絹本設色 纵137.5厘米 横63.1厘米 台北國立故宮博物院藏 Dai
Jin-Luohan.jpg|畫羅漢，紙本設色 纵113.9厘米 横36.7厘米 台北國立故宮博物院藏 Dai Jin. The Night
Excursion of Zhong Kui. 189,7x120,2. Palace Museum,
Beijing.jpg|[钟馗出山图](../Page/钟馗.md "wikilink")
[北京故宫博物院藏](../Page/北京故宫博物院.md "wikilink") Dai Jin.
Travelers Through Mountain Passes. 61,8x29,7 Palace Museum,
Beijing..jpg|关山行旅图 北京故宫博物院藏 Dai Jin-Hollyhock and
Butterflies.jpg|蜀葵蛺蝶圖，紙本設色 纵115厘米 横39.6厘米 北京故宫博物院藏 Dai
Jin-Looking Three Times at the Thatched Hut.jpg|三顧茅廬圖，絹本墨筆 纵172.2厘米
横107厘米 北京故宫博物院藏 Dai Jin-Deep House of Poetry.jpg|深堂詩意圖，絹本設色 纵194厘米
横104厘米 [遼寧省博物館藏](../Page/遼寧省博物館.md "wikilink") Dai Jin-Employing
Virtue.jpg|聘賢圖，絹本設色 纵132.5厘米 横71.5厘米
[瀋陽故宮博物院藏](../Page/瀋陽故宮博物院.md "wikilink")
Dai Jin-Dense Green Covering the Spring Mountains.jpg|春山積翠圖，紙本墨筆 纵141厘米
横53.4厘米 [上海博物館藏](../Page/上海博物館.md "wikilink") Dai Jin. Six Patriarchs
of Chan. Liaoning Provincial
Museum..jpg|[达摩至](../Page/达摩.md "wikilink")[惠能六代像](../Page/惠能.md "wikilink")，
[遼寧省博物館藏](../Page/遼寧省博物館.md "wikilink") 月下泊舟圖.jpg|月下泊舟圖，軸‧絹本‧水墨‧淡彩，59.5
x 42.5 公分王季遷明德堂，[美國](../Page/美國.md "wikilink")

## 参考资料

<references/>

{{-}}

[D](../Category/明朝畫家.md "wikilink")
[Category:杭州人](../Category/杭州人.md "wikilink")

1.  单国强 《中国美术·明清至近代》第三章 ISBN 7-300-05513-3
2.  《画史会要》卷四记载：“戴进，字文进……宣庙喜绘事，一时待诏有谢廷循、倪端、石锐、李在，皆有名。文进入京，众工妒之。”
3.  [郎瑛](../Page/郎瑛.md "wikilink"):《七修续稿》卷六《戴进传》
4.  任道斌 《丹青趣味:中国绘画的源与流》浙江大学出版社 2004年09月1日 ISBN 7308037096