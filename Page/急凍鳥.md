**急凍鳥**（，），為種[寶可夢中的一種](../Page/寶可夢列表.md "wikilink")，與[闪电鸟](../Page/闪电鸟.md "wikilink")、[火焰鸟同為傳說中的三聖鳥](../Page/火焰鸟.md "wikilink")[寶可夢](../Page/寶可夢.md "wikilink")，最早出現於精靈寶可夢[紅版及](../Page/精靈寶可夢_紅·綠.md "wikilink")[綠版之中](../Page/精靈寶可夢_紅·綠.md "wikilink")。

## 簡介

**急凍鳥**是[冰屬性寶可夢](../Page/冰.md "wikilink")，全國圖鑑編號為144，豐緣地區編號為299，和其他寶可夢並沒有[進化上的關係](../Page/進化.md "wikilink")。牠在很多[精靈寶可夢的產品中出現](../Page/精靈寶可夢.md "wikilink")，包括由[田尻智所創作的](../Page/田尻智.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")、[動畫](../Page/日本動畫.md "wikilink")、[漫畫和](../Page/日本漫畫.md "wikilink")[參考書等](../Page/圖書.md "wikilink")。

### 特徵

**急凍鳥**是屬於冰凍的寶可夢，因為[藍色的身體給人帶來寒冷的感覺](../Page/藍色.md "wikilink")。牠有如[鳳凰般的外表](../Page/鳳凰.md "wikilink")，在[天空中](../Page/天空.md "wikilink")[自由地飛翔](../Page/自由.md "wikilink")。牠有一個像三隻角般的雞冠，和一雙[寶石似的大大的](../Page/寶石.md "wikilink")[紅色的](../Page/紅色.md "wikilink")[眼睛](../Page/眼睛.md "wikilink")，一對又大又長的[翅膀上整整齊齊地排著很多](../Page/翅膀.md "wikilink")[羽毛](../Page/羽毛.md "wikilink")，[黑色的](../Page/黑色.md "wikilink")[腳上有很尖的爪](../Page/腳.md "wikilink")，而且最具特色的是牠有一條極長又極柔順、[頭髮般的](../Page/頭髮.md "wikilink")[尾巴](../Page/尾巴.md "wikilink")。雖然牠是[鳥類](../Page/鳥類.md "wikilink")，但牠總是在[風雪中出現](../Page/雪.md "wikilink")，可見牠不但不怕寒冷，而且很喜歡這種[氣溫](../Page/溫度.md "wikilink")。

### 命名由來

牠的[日文名字](../Page/日語.md "wikilink")的[發音是](../Page/語音學.md "wikilink")****，意思是指[冷凍器](../Page/冰箱.md "wikilink")。[英文名](../Page/英語.md "wikilink")****方面則是用「[北極](../Page/北極.md "wikilink")」的英文字「」作為開頭，加上在[西班牙文中代表](../Page/西班牙文.md "wikilink")「一」的「」在名字的尾，意思是指牠是三聖鳥之中的第一隻、屬於[冰屬性的聖鳥](../Page/冰.md "wikilink")。不論在[香港](../Page/香港.md "wikilink")、[中國大陸或](../Page/中國大陸.md "wikilink")[台灣](../Page/台灣.md "wikilink")，都是以**急凍鳥**作為官方的[中文名字](../Page/漢語.md "wikilink")，不過在坊間某些[網頁中](../Page/網頁.md "wikilink")，有些人叫牠做**冰鳥**。

## 電子遊戲

**急凍鳥**在遊戲中的[屬性是](../Page/神奇寶貝屬性.md "wikilink")「[冰](../Page/冰.md "wikilink")」和「飛行」。在很多款[任天堂](../Page/任天堂.md "wikilink")[遊戲機的](../Page/遊戲機.md "wikilink")[精靈寶可夢遊戲中都可以飼養](../Page/神奇寶貝遊戲列表.md "wikilink")。牠最初出現在第一世代\[1\]的「[紅](../Page/紅色.md "wikilink")」、「[綠](../Page/綠色.md "wikilink")」、「[藍](../Page/藍色.md "wikilink")」和「[黃](../Page/黃色.md "wikilink")」版之中。

急凍鳥合計的種族值\[2\]有580，和其餘兩隻聖鳥[閃電鳥和](../Page/閃電鳥.md "wikilink")[火鳥相同](../Page/火鳥.md "wikilink")，亦和第二世代中的三聖獸\[3\]、第三世代中的三聖柱以及第四世代中的三聖神有相同的總種族值。牠有極高的能力\[4\]，加上牠的飛行屬性令牠可以回避所有地面屬性的招式，所以很多玩家都會餵養一隻來和其他人戰鬥。急凍鳥戰鬥時會怕[火](../Page/火.md "wikilink")、[電以及較冷門的](../Page/電.md "wikilink")[鋼屬性](../Page/鋼.md "wikilink")，而且牠有一個大的弱點，就是「[冰](../Page/冰.md "wikilink")」屬性和「飛行」屬性的配搭會令牠被[岩石屬性招式攻擊時](../Page/岩石.md "wikilink")，傷害會乘以4，會使用強勁岩石屬性的寶可夢是牠的剋星。尤以高速度的化石翼龍為最大敵人。

由「火紅」、「葉綠」版開始，急凍鳥可以同時習得「心之眼」和「[絕對零度](../Page/絕對零度.md "wikilink")」兩招。「絕對零度」一招可以令被擊中的對手立即[死亡](../Page/死亡.md "wikilink")\[5\]，雖然也有其它寶可夢能夠使出，但此招式的命中機率之低，只有30%，所以效率並不高。「心之眼」一招被使出後，對手會被一直望著，下一回合自己所使出的招式必定會打中對手。急凍鳥是唯一一隻可以同時學會該兩招的寶可夢，當牠使出「心之眼」後，下回合自己再使出的「絕對零度」必定會打中，對手就必定會立即死亡。正因如此，急凍鳥被稱為招式上最強的寶可夢，只要牠能夠成功先後使出該兩招就能得勝。要破解急凍鳥這種組合招式，只要在牠使出兩招之前打敗掉牠就可，亦有一些精靈的特性如「結實」是可以防禦這種一擊必殺的招式。急凍鳥優勝之處只是招式的組合性，牠的攻擊和特攻值都不是太高，不能和總能力、特攻和[速度等方面都是最好的](../Page/速度.md "wikilink")[超夢比較](../Page/超夢.md "wikilink")。

由第三世代或以後\[6\]，急凍鳥的基本親密度是35，即是比普遍的寶可夢為低。牠的特性\[7\]是「[壓迫感](../Page/壓迫感.md "wikilink")」，即是當受到攻擊的招式，攻擊牠的對手會消耗多一點PP，冒險時將急凍鳥放在首位的話，則不會遇到該區最低等級的野生寶可夢，而且有一半的機會率會遇到該區最高等級的野生寶可夢。由於牠是屬於[冰屬性的寶可夢](../Page/冰.md "wikilink")，因此牠不會被對手的招式弄至冰凍，而且在風雪中不會被落下來的[冰雹弄傷](../Page/冰雹.md "wikilink")。

### 捕捉方式

**急凍鳥**雖然是一隻傳說的寶可夢，卻不像[夢幻](../Page/夢幻.md "wikilink")、[時拉比屬於限量版的寶可夢](../Page/時拉比.md "wikilink")，玩家可以從正常的方法之中捉到牠，不過只可以在[Game
Boy的紅](../Page/Game_Boy.md "wikilink")、綠、藍和黃版、[Game Boy
Advance的火紅](../Page/Game_Boy_Advance.md "wikilink")、葉綠版，以及[GameCube的](../Page/GameCube.md "wikilink")《精靈寶可夢XD
闇之旋風：闇黑洛奇亞》中捕捉到急凍鳥。

在上述各版本中，只要進入雙子島後，先將所有可以推動的[岩石推進洞](../Page/岩石.md "wikilink")，然後到山洞最深處就可以直接捕捉牠。以上各版中出現的的急凍鳥等級都是50。由於急凍鳥不會被冰凍，不易被[精靈球收服](../Page/精靈球.md "wikilink")，只有在打傷牠的同時令牠[睡眠或麻痺](../Page/睡眠.md "wikilink")，才可以有多些機會捉到牠。

[GameCube版中](../Page/GameCube.md "wikilink")，取得方法就是要打敗最後一個頭目，在和牠的對戰中淨化牠餵養的黑暗急凍鳥。這個方法的成功率十分低，所以要在這隻遊戲中捉到牠遠比在其他遊戲中困難。

急凍鳥沒有[性別](../Page/性別.md "wikilink")，而且不能生[蛋](../Page/蛋.md "wikilink")，所以玩家不能從正常遊戲中取得第二隻\[8\]。若玩家在雙子島中把唯一急凍鳥的HP歸零，除非重新開始遊戲，否則將無法再次遇到甚至收服急凍鳥。

### 招式

**急凍鳥**在各個版本的[精靈寶可夢遊戲中所學到的招式都有所不同](../Page/神奇寶貝遊戲列表.md "wikilink")，但都是較為強力的招式。由於牠是[冰屬性和飛行屬性的寶可夢](../Page/冰.md "wikilink")，牠使用該屬性的招式時威力會較大。牠的代表邵包括「冰凍光束」和「暴風雪」，同時牠亦擅長使用各種變化類招式，正如上文所說的「心之眼」與「[絕對零度](../Page/絕對零度.md "wikilink")」的組合招式。以下列出的招式以[珍珠](../Page/精靈寶可夢_鑽石·珍珠.md "wikilink")、[鑽石版為主](../Page/精靈寶可夢_鑽石·珍珠.md "wikilink")\[9\]：

牠還可以學會一些招式學習器和秘傳學習器，例如用以來往各城市的招式「飛翔」。

### 不可思議的迷宮

**急凍鳥**亦會在《精靈寶可夢不可思議的迷宮》中出現，但只有一隻。在遊戲中，牠是其中一個迷宮的首腦，必須要打敗牠才可以過關。在完成遊戲後，玩家可以回到該迷宮中再挑戰這隻急凍鳥，有機會捉到牠，但因為牠的身型很大，會佔用較多的空間，和牠戰鬥時必須留有4個單位的空間才可以令牠加入自己的隊伍。牠的等級是53，是遊戲中最高等級的傳說的寶可夢，但要捕捉牠的話玩家的寶可夢等級不必比牠高。

正如上文所說，急凍鳥已經能夠同時學會「心之眼」與「[絕對零度](../Page/絕對零度.md "wikilink")」兩招，可以作為一個必殺的組合。更厲害的是，這個遊戲中玩家可以將寶可夢的招式設定為組合\[10\]，令精靈在同一回合中連續使出兩招或更多的招式。此時急凍鳥只需在一次出招的機會中就可以使出這個連續技，在一回合中令對手[死亡](../Page/死亡.md "wikilink")。

## 動畫

**急凍鳥**在[精靈寶可夢動畫版中很少出現](../Page/精靈寶可夢動畫版.md "wikilink")，但在多個開場畫面中露面。急凍鳥在[動畫中作為重要](../Page/動畫.md "wikilink")[角色的單元僅有於成都聯盟故事中的第](../Page/角色.md "wikilink")190集和《[精靈寶可夢超世代](../Page/神奇寶貝超世代.md "wikilink")》的第134、135集登場，在第135集中急凍鳥和小智的[噴火龍戰鬥](../Page/噴火龍.md "wikilink")。不論牠在故事中擔當[正派還是](../Page/英雄.md "wikilink")[反派的](../Page/反英雄.md "wikilink")[角色](../Page/角色.md "wikilink")，牠都是一隻十分珍貴的傳說的寶可夢。

### [劇場版](../Page/劇場版.md "wikilink")

**急凍鳥**在1999年首映的[電影](../Page/電影.md "wikilink")《[神奇寶貝之路基亞爆誕](../Page/神奇寶貝電影版.md "wikilink")》中出現。故事中，即[精靈寶可夢動畫版身處的](../Page/精靈寶可夢動畫版.md "wikilink")[世界之中](../Page/世界.md "wikilink")，急凍鳥正要逃避收藏家[獵人吉爾露太的追捕](../Page/獵人.md "wikilink")，並遇到小智等人。那個收藏家已經捉了[閃電鳥和](../Page/閃電鳥.md "wikilink")[火焰鳥兩隻聖鳥](../Page/火焰鳥.md "wikilink")，想把急凍鳥一起捉去以儲齊三隻，並引出幻之寶可夢[洛奇亞](../Page/洛奇亞.md "wikilink")，因此小智等人合力拯救牠。除此之外急凍鳥就沒有在其他[電影之中登場的機會了](../Page/電影.md "wikilink")。

### [聲優](../Page/聲優.md "wikilink")

  - [日本](../Page/日本.md "wikilink")：
      - 第190集：[冬馬由美](../Page/冬馬由美.md "wikilink")
      - 第413集：[小西克幸](../Page/小西克幸.md "wikilink")

## 漫畫

### [神奇寶貝特別篇](../Page/神奇寶貝特別篇.md "wikilink")

在第一章中初登場，原棲息於雙子島，因不想被[火箭兵團捉去](../Page/火箭.md "wikilink")，而把自己冰封，後來被小智的[暴鯉龍的攻擊而解封並成功逃脫](../Page/暴鯉龍.md "wikilink")。不過不久之後，被前[毒屬性道館館主](../Page/毒.md "wikilink")、[火箭兵團的幹部](../Page/火箭.md "wikilink")[忍者阿桔捉去](../Page/忍者.md "wikilink")\[11\]。在西爾佛總公司的決鬥，因關都地方的七枚徽章的力量，與閃電鳥、火焰鳥合為一體，令牠變成邪惡的寶可夢，後被小智、小茂和小藍打敗，並得以從火箭兵團手上解放。第三章中被小藍捕獲，於桐樹林與小智一起對抗鳳王和洛奇亞。

### 神奇寶貝

在《神奇寶貝》之中，有一隻急凍鳥挑戰[皮卡丘](../Page/皮卡丘.md "wikilink")、皮皮和胖丁，說要吃完50個[雪錐才可以放走](../Page/雪.md "wikilink")[扁桃和](../Page/扁桃.md "wikilink")[傑尼龜等人](../Page/傑尼龜.md "wikilink")。最後急凍鳥竟然輸了，雖想賴帳，但還是放走各人，讓他們坐在牠的背上飛回家。

## 卡片遊戲

在[神奇寶貝集換式卡片遊戲之中](../Page/神奇寶貝集換式卡片遊戲.md "wikilink")，急凍鳥首次出現[化石系列之中](../Page/化石.md "wikilink")，是一張有[全息摄影技術的閃卡](../Page/全息摄影.md "wikilink")，之後還出現過很多張急凍鳥的卡。作為遊戲卡的時候，急凍鳥通常是屬於[水](../Page/水.md "wikilink")[屬性的](../Page/神奇寶貝屬性.md "wikilink")。

## 註解

## 外部連結

  - [神奇寶貝日本官方網站](http://www.pokemon.co.jp/)
  - [神奇寶貝英文官方網站](http://www.pokemon.com/)
  - [Serebii.net 之中的急凍鳥](http://www.serebii.net/pokedex-dp/144.shtml)
  - [Aucy's Pokemon - NDS精靈 -
    フリーザー](http://pokemon.aucy.com/nds/pokemon.php?n=144)
  - [神奇寶貝集換式卡片遊戲的資料庫](http://pokebeach.com/cdex/)

[es:Anexo:Pokémon de la primera
generación\#Articuno](../Page/es:Anexo:Pokémon_de_la_primera_generación#Articuno.md "wikilink")
[fi:Luettelo Pokémon-lajeista
(121–151)\#Articuno](../Page/fi:Luettelo_Pokémon-lajeista_\(121–151\)#Articuno.md "wikilink")
[simple:Legendary
Pokémon\#Kanto](../Page/simple:Legendary_Pokémon#Kanto.md "wikilink")

[Category:第一世代寶可夢](../Category/第一世代寶可夢.md "wikilink")
[Category:虛構鳥類](../Category/虛構鳥類.md "wikilink")
[Category:冰屬性寶可夢](../Category/冰屬性寶可夢.md "wikilink")
[Category:飛行屬性寶可夢](../Category/飛行屬性寶可夢.md "wikilink")
[Category:1996年推出的電子遊戲角色](../Category/1996年推出的電子遊戲角色.md "wikilink")

1.  第一世代就是指自精靈寶可夢「[紅](../Page/紅色.md "wikilink")」、「[綠](../Page/綠色.md "wikilink")」版開始出現的寶可夢，全國圖鑑編號1～151。
2.  種族值包括HP、攻擊、防禦、特攻、特防和[速度](../Page/速度.md "wikilink")。
3.  三聖獸包括[雷公](../Page/雷公_\(神奇寶貝\).md "wikilink")、[炎帝和](../Page/炎帝_\(神奇寶貝\).md "wikilink")[水君](../Page/水君.md "wikilink")。
4.  在第一世代，有比三聖鳥更高種族值的寶可夢只有[超夢和](../Page/超夢.md "wikilink")[夢幻這兩隻傳說的寶可夢](../Page/夢幻.md "wikilink")，而且當時夢幻還是限量版的寶可夢。
5.  現實世界中，任何物質在[絕對零度的情況下都不能活動](../Page/絕對零度.md "wikilink")。
6.  在[金](../Page/精靈寶可夢_金·銀.md "wikilink")、[銀和](../Page/精靈寶可夢_金·銀.md "wikilink")[水晶版中都有親密度](../Page/精靈寶可夢_金·銀.md "wikilink")，不過所有寶可夢的基本數值都預設為70。
7.  只有在第三世代或以後的遊戲才有特性這回事。
8.  但有些玩家用非正式的方法如利用遊戲中的[程序錯誤複製已有的急凍鳥](../Page/程序錯誤.md "wikilink")。
9.  有關各招式的[日文原名及](../Page/日語.md "wikilink")[英文譯名](../Page/英語.md "wikilink")，可參考[Aucy's
    Pokemon](http://pokemon.aucy.com/nds/pokemon.php?n=144)。
10. 要組合招式，可以在招式處理店中設定，或使用「連結箱」來處理。
11. 另外兩隻聖鳥[閃電鳥和](../Page/閃電鳥.md "wikilink")[火焰鳥分別被](../Page/火焰鳥.md "wikilink")[電屬性道館館主馬志](../Page/電.md "wikilink")-{}-士和[超能力屬性道館館主娜姿所捉去](../Page/超能力.md "wikilink")。