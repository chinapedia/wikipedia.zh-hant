[Ukraine_krementchuk_lenin.jpg](https://zh.wikipedia.org/wiki/File:Ukraine_krementchuk_lenin.jpg "fig:Ukraine_krementchuk_lenin.jpg")
**克列緬丘格**（**克列門楚克**，，）是[烏克蘭中部的一個重要的工業城市](../Page/烏克蘭.md "wikilink")，由[波尔塔瓦州负责管辖](../Page/波尔塔瓦州.md "wikilink")，以金屬與機械工業著稱。

## 參考文獻

## 外部連結

  - [Kremenchug-克列緬丘格](https://loso.wordpress.com/2017/12/24/kremenchug-克列緬丘格-～-遊踪/)

[Category:波爾塔瓦州城市](../Category/波爾塔瓦州城市.md "wikilink")
[Category:1571年建立的聚居地](../Category/1571年建立的聚居地.md "wikilink")