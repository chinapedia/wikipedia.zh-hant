**陰道**（）是一種纖維肌形成有彈性的柱狀通道的[性器官](../Page/性器官.md "wikilink")，主要利於雌雄[性交與](../Page/性交.md "wikilink")[分娩時的](../Page/分娩.md "wikilink")**產道**。在胎盤[哺乳動物中](../Page/哺乳動物.md "wikilink")（特別是[灵长類](../Page/灵长目.md "wikilink")），[月经常是代表](../Page/月经.md "wikilink")[生殖繁衍能力的象徵](../Page/生育能力.md "wikilink")，也是陰道另一個主要功能——週期性從[子宮內膜剝落的](../Page/子宮內膜.md "wikilink")[黏膜組織和](../Page/黏膜.md "wikilink")[血液藉由](../Page/血液.md "wikilink")[陰道排出](../Page/陰道.md "wikilink")。陰道的型態大小與部位隨物種而不同，甚至同一物種在大小上亦有差異。人類陰道介於陰戶的開口到[子宮之間](../Page/子宮.md "wikilink")，但陰道的末端止於[子宮頸](../Page/子宮頸.md "wikilink")。

在[尿生殖部](../Page/尿生殖三角.md "wikilink")（），不像雄性的哺乳動物常是以尿道出口作為單獨的外部尿生殖孔，雌性的哺乳動物常是兩個尿生殖孔，分別為尿道、陰道之用。[陰道的開口比尿道孔大很多](../Page/陰道口.md "wikilink")，兩者外有[陰唇保護](../Page/陰唇.md "wikilink")\[1\]\[2\]。而在[兩棲動物](../Page/两栖动物.md "wikilink")、[鳥類](../Page/鳥類.md "wikilink")、[爬行动物及](../Page/爬行动物.md "wikilink")[單孔目的哺乳類動物的雌性身上](../Page/单孔目.md "wikilink")，[動物學家會稱之為](../Page/動物學家.md "wikilink")[泄殖腔的單獨開孔](../Page/泄殖腔.md "wikilink")，功能上為腸道、尿道及生殖道共用。

陰道在[女人的性與](../Page/人类的性.md "wikilink")[歡愉扮演要角](../Page/性愉悅.md "wikilink")。在人類或其他動物的[性喚起過程中](../Page/性喚起.md "wikilink")，[陰道分泌液逐漸增加了陰道濕潤程度](../Page/陰道分泌液.md "wikilink")、減低摩擦、使通道環境平滑，而在[性行為时](../Page/性行為.md "wikilink")，[阴茎受到分泌液濕潤程度不一的摩擦助其](../Page/阴茎.md "wikilink")[勃射](../Page/射精.md "wikilink")，使雌雄生殖細胞[受精](../Page/受精.md "wikilink")\[3\]。另外，除了[功能障碍的因素](../Page/女性性功能障碍.md "wikilink")，會影響陰道的風險尚有各種[性傳染疾病](../Page/性傳播疾病.md "wikilink")（STIs/STDs），因此有關當局如[世界卫生组织等公共衛生與保健部門宣導推行](../Page/世界卫生组织.md "wikilink")[安全性行為](../Page/安全性行為.md "wikilink")。

自古以來陰道在文化上存有若干認知上刻板概念，比如將陰道視為[性渴望的焦點](../Page/性渴望.md "wikilink")、生命至出生的譬喻象徵、地位屈在阴茎之下的器官、視覺上不討喜或其他不入流的代稱。

## 结构

### 解剖特徵

陰道長度約7到15公分左右，直徑約2.5公分左右的空腔器官。
其主要的[組織為彈性很高的](../Page/組織_\(生物學\).md "wikilink")[平滑肌](../Page/平滑肌.md "wikilink")，尤其是[陰道口部分分布有大量的](../Page/陰道口.md "wikilink")[神經末梢](../Page/神經末梢.md "wikilink")。哺乳動物的陰道內部表面是[黏膜構成](../Page/黏膜.md "wikilink")，顏色通常是粉紅色。

當婦女平躺時，陰道向下与水平方向约15度，與子宮頸的夾角略大於90度。[陰道開口位於](../Page/陰道口.md "wikilink")[女陰後方](../Page/女陰.md "wikilink")，在[尿道開口之後](../Page/尿道.md "wikilink")。陰道的長度，直徑和形狀可有很大的變化。當婦女性交時陰道會隨之增大，在生產時甚至可以擴張到可讓嬰兒產出的程度。在平時它的形狀是扁平的，[陰道口是封閉的而陰道壁也緊靠在一起](../Page/陰道口.md "wikilink")。

### 一般结构

阴道是由胚胎的所發育而成\[4\]。人的阴道是弹性肌肉管狀組織，從[宫颈延伸到](../Page/宫颈.md "wikilink")[外阴](../Page/外阴.md "wikilink")\[5\]。阴道和外阴的内部的顏色是带红色的粉红色，它連接外阴以及子宫颈。

阴道其内層表面為[複層扁平上皮](../Page/複層扁平上皮.md "wikilink")\[6\]。在这下面為[平滑肌](../Page/平滑肌.md "wikilink")，在阴道性交及[分娩时會收缩](../Page/分娩.md "wikilink")。下方的肌肉是称为的[结缔组织](../Page/结缔组织.md "wikilink")\[7\]。

### 區域及層次

阴道包圍[子宫颈](../Page/子宫颈.md "wikilink")，[陰道穹分为四个区域](../Page/陰道穹.md "wikilink")；分別是前部、後部和兩邊側部\[8\]。阴道在其上方三分之一，中间三分之一与下三分之一都有支撐。上方的三分之一是由和韧带所支撐，这些地区也被描述为在[主韧带横向和](../Page/主韧带.md "wikilink")[宫骶韧带后外侧](../Page/宫骶韧带.md "wikilink")。阴道的中部三分之一涉及。下部三分之一的阴道和会阴体有關，它有时被描述为含有会阴体，盆腔隔膜和泌尿生殖隔膜\[9\]\[10\]。

阴道壁可以分為四層：第一层是鳞状上皮，其形成折痕或皱褶，并讓阴道在生育時可以扩张到足够大的程度。该皱褶是一系列由阴道的外三分之一的壁的折叠产生的脊，這些横向上皮脊的功能是提供阴道在延伸和拉伸時有足夠的表面积。阴道的第二层是结缔组织，其中含有血管。第三层是在肌肉层，它是纵行肌的外层，以及环形肌的内层。第四层是结缔组织的外层，它连接到骨盆的其它器官，是由血液和[淋巴管和肌肉纖維組成](../Page/淋巴管.md "wikilink")\[11\]\[12\]。

[阴道口是在外阴的尾端](../Page/阴道口.md "wikilink")，[尿道开口的后面](../Page/尿道.md "wikilink")。阴道上部的四分之一和直肠之間有將兩者隔開。在阴道上方有一層稱為[阴阜的脂肪垫](../Page/阴阜.md "wikilink")，包围[耻骨](../Page/耻骨.md "wikilink")，並在阴道性交过程中提供保护。

### 潤滑

[前庭大腺位于阴道口和子宫颈附近](../Page/前庭大腺.md "wikilink")，原本被认为是[陰道分泌液的主要来源](../Page/陰道分泌液.md "wikilink")，但它们仅提供了几滴[粘液润滑阴道](../Page/粘液.md "wikilink")\[13\]一般认为陰道润滑液的主要成份是由从阴道壁渗流而出，即所谓「阴道渗出」。阴道渗出一開始是汗状液滴，由阴道的引起的，这导致[毛细血管通过阴道上皮增加渗出的压力](../Page/毛細管.md "wikilink")\[14\]\[15\]。

在[排卵期前和排卵期中](../Page/排卵.md "wikilink")，子宫颈的粘液腺分泌粘液，使阴道腔成為有利[精子存活的](../Page/精子.md "wikilink")[碱性环境](../Page/碱性.md "wikilink")。「阴道润滑一般随妇女的年龄而減少，但是这是一种自然的物理变化，不意味着有任何身体或心理问题。[更年期后](../Page/更年期.md "wikilink")，体内的雌激素減少，除非進行雌激素替代疗法的补偿，否則阴道壁會显著变薄。\[16\]」

### 变化和大小

[Illu_repdt_female_zh.jpg](https://zh.wikipedia.org/wiki/File:Illu_repdt_female_zh.jpg "fig:Illu_repdt_female_zh.jpg")
在正常状态下，在生育年龄的妇女，其阴道长度會有不同。阴道前壁长度约7.5厘米，阴道後壁长度约9厘米，因此后穹窿比前穹窿更深\[17\]。若有[性刺激](../Page/性刺激.md "wikilink")，阴道的长度和宽度都會膨胀。

如果一个妇女直立时，阴道會是向上及向后的方向，和子宫的角度微大于45度，和水平的角度約60度\[18\]。

### 处女膜

[处女膜是包围或部分覆盖外部](../Page/处女膜.md "wikilink")[阴道口的薄膜组织](../Page/阴道口.md "wikilink")。[阴道性交和](../Page/阴道性交.md "wikilink")[分娩对处女膜的影響因人而異](../Page/分娩.md "wikilink")。若处女膜的弹性足够，可能可以恢复到接近原来的状态。在其他情况下，可能会有残余（[处女膜痕](../Page/处女膜痕.md "wikilink")），也可能在多次的性交或分娩後消失\[19\]\[20\]。此外，处女膜也可能因為疾病、受伤、体检、[自慰或運動而撕裂](../Page/自慰.md "wikilink")。由于这些原因，不太可能利用处女膜明确确定一名女性是否是[处女](../Page/处女.md "wikilink")\[21\]\[22\]\[23\]\[24\]。

## 功能性

阴道有許多不同的功能，如性交等

### 月經排出

[月經的經血及組織可以由阴道排出體外](../Page/月經.md "wikilink")。在現代社會中，會用[衛生棉](../Page/衛生棉.md "wikilink")、[衛生棉條及](../Page/衛生棉條.md "wikilink")[月經杯等用具來防止月經流出](../Page/月經杯.md "wikilink")。

### 性活動

聚集於[陰道開口附近的神經末梢](../Page/陰道口.md "wikilink")，使女性在性行為時，若以適當的方式給予性刺激，得以獲得愉悅感，而且許多女性在陰道性交時感受到親密及丰满的感覺\[25\]\[26\]。不過陰道本身的神經末梢數量較少，因此女性若只進行阴道性活动，不易得到足夠的性興奮以及[性高潮](../Page/性高潮.md "wikilink")\[27\]\[28\]\[29\]\[30\]。陰道的前三分之一，特別是靠近陰道口處，是神經集中之處，碰觸此處比碰觸陰道的後三分之二更加敏感\[31\]\[32\]\[33\]。若陰道所有部份都有神經末梢，在性交時固然更加興奮，但這也意味著在生產過程中會更加疼痛\[34\]\[35\]\[36\]。

在性刺激，尤其是對[陰核的刺激下](../Page/陰核.md "wikilink")，陰道壁會分泌[陰道分泌液](../Page/陰道分泌液.md "wikilink")，以降低性行為過程中的摩擦。阴道内有一个俗稱為「[G点](../Page/G点.md "wikilink")」的[性感帶](../Page/性感帶.md "wikilink")。当受到性刺激时，部分女性会感受到強烈的興奮，由於G點[性高潮可能產生](../Page/性高潮.md "wikilink")[雌性潮射](../Page/雌性潮射.md "wikilink")（潮吹），使得某些研究者認為G點興奮實際上是來自[斯基恩氏腺](../Page/斯基恩氏腺.md "wikilink")，一種女性體內相當於前列腺的器官，而不是在陰道壁上的某個點\[37\]\[38\]\[39\]。此外，有一些研究者則認為「G點」並不存在\[40\]。

女性阴道内的酸硷度，年龄较轻者呈较酸性，以保护身体洁净。当年龄过了二十岁後，阴道内的体液会自然稍微偏向中性以利受孕。若较年轻的女性与成熟的男人性交——三十岁以内的女人，对象是很猛壮的男人；或者二十岁的女人，对象是健壮的男人——较容易[发炎与阴道裂伤](../Page/陰道炎.md "wikilink")（较成熟的女人则较不会），彼此的过度激情也常是原因之一。

洗澡，尤其是泡澡後，女人阴道内多呈无菌状态，此时若[以手指挑逗或性交](../Page/指交.md "wikilink")，则比较容易[感染發炎](../Page/陰道炎.md "wikilink")，严重者还可能造成[子宫内膜炎](../Page/子宫内膜炎.md "wikilink")、[腹膜炎或](../Page/腹膜.md "wikilink")[肺炎等](../Page/肺炎.md "wikilink")。\[41\]

## 临床意义

[Vagina_US.png](https://zh.wikipedia.org/wiki/File:Vagina_US.png "fig:Vagina_US.png")、子宮及陰道\]\]

阴道自净，因此通常不需要特殊治疗。医生一般不鼓励冲洗的做法\[42\]。因为健康的阴道是存在相互[共生菌群](../Page/共生.md "wikilink")（[陰道菌群](../Page/陰道菌群.md "wikilink")），保护阴道免於致病微生物的進入，试图破坏这种平衡可能会造成许多不良后果，包括异常[分泌物和](../Page/阴道分泌物.md "wikilink")[念珠菌症](../Page/念珠菌症.md "wikilink")。

在[妇科检查阴道時](../Page/妇科.md "wikilink")，经常會使用，使阴道打開可以目視子宫颈或取得檢體（[巴氏塗片檢查](../Page/巴氏塗片檢查.md "wikilink")）。和阴道有關的醫療行為，包括检查、医学的给药、及进行分泌物检查，一般會稱為per
vaginam\[43\]。

### pH值

生育年龄的健康妇女，其阴道呈酸性，pH值一般3.8和4.5之间不等\[44\]。这是由分泌的乳酸酶使得[糖原降解為](../Page/糖原.md "wikilink")[乳酸](../Page/乳酸.md "wikilink")。这是阴道的[偏利共生](../Page/偏利共生.md "wikilink")。酸度使得病原微生物的菌株無法生长\[45\]。

若阴道的pH值增加（一般會以4.5或更高值為準），可能是因為细菌过度生长引起的，例如[细菌性阴道炎和](../Page/细菌性阴道炎.md "wikilink")[毛滴虫病](../Page/毛滴虫病.md "wikilink")，或是生產前的[胎膜破裂](../Page/胎膜破裂.md "wikilink")（破水）\[46\]。

### 阴道痉挛

阴道痉挛是指此區域肌肉的[条件反射造成阴道的不自主紧缩](../Page/条件反射.md "wikilink")。它可以影响任何一種將外物插入阴道的行為，包括阴道性交，插入[衛生棉條和](../Page/衛生棉條.md "wikilink")[月经杯](../Page/月经杯.md "wikilink")，以及妇科的侵入性检查。許多心理和生理治疗可以减緩此情形。

### 疾病的征兆

#### 肿块

在阴道壁或底部出現肿块都是表示有异常情形。其中最常见的是[前庭大腺囊肿](../Page/前庭大腺囊肿.md "wikilink")\[47\]。這種囊肿大小類似一个豌豆，是由腺体的正常出口堵塞形成的。可以小手术或是[硝酸银進行治疗](../Page/硝酸银.md "wikilink")。其他较少见小疙瘩或水疱的原因是[单纯疱疹](../Page/单纯疱疹.md "wikilink")。它们通常不止一個，非常疼痛，而且有透明的液體流出。可能与全身水肿有关，而且非常柔軟。与阴道壁癌症相关的肿块非常罕见，而且平均发病年龄为七十歲\[48\]。
最常见的形式是[鳞状细胞癌](../Page/鳞状细胞癌.md "wikilink")，再來是，最少見的是[黑色素瘤](../Page/黑色素瘤.md "wikilink")。

#### 分泌物

大部分阴道的分泌物是由于正常的身体功能，如[经期或](../Page/经期.md "wikilink")[性兴奋](../Page/性兴奋.md "wikilink")。不過异常分泌物往往意味著疾病。

正常阴道排出物包括血液或从子宫中排出的经血液，透明的液体可能是[性刺激的結果或是子宮頸的分泌物](../Page/性刺激.md "wikilink")。其他非感染性原因包括[皮膚炎](../Page/皮膚炎.md "wikilink")。非[性傳播疾病的分泌物可能是](../Page/性傳播疾病.md "wikilink")[陰道細菌增生症](../Page/陰道細菌增生症.md "wikilink")、[鹅口疮或是](../Page/鹅口疮.md "wikilink")[念珠菌症](../Page/念珠菌症.md "wikilink")。像[淋病](../Page/淋病.md "wikilink")、[衣原体感染或](../Page/衣原体感染.md "wikilink")[毛滴虫病等](../Page/毛滴虫病.md "wikilink")[性傳播疾病也會有分泌物](../Page/性傳播疾病.md "wikilink")。鹅口疮的分泌物是略有刺鼻的白色液體，滴虫病的分泌物是有惡臭的绿色液體\[49\]。

#### 疮

阴道的疮是指阴道壁的细膜崩溃。最常见的是擦伤和外伤引起的小溃疡。强姦过程會造成阴道的疮，但大部份是由服装或不当插入[卫生棉条所造成的](../Page/卫生棉条.md "wikilink")。典型由[梅毒引起的溃疡多半是无痛](../Page/梅毒.md "wikilink")，且边缘有凸起。而且大多发生在阴道内，因此往往未被发现。[疱疹的囊泡往往非常柔軟而且會有肿胀](../Page/疱疹.md "wikilink")，使得排尿困难。在[開發中國家一種稱為](../Page/開發中國家.md "wikilink")[利什曼病的寄生虫病也会引起阴道溃疡](../Page/利什曼病.md "wikilink")，但在西方國家很少遇到。[艾滋病毒可以通过阴道性交傳染](../Page/艾滋病毒.md "wikilink")，但不會造成阴道或外阴的疾病\[50\]。這些局部的外阴或阴道疾病都很容易治疗，但患者常常會因為害羞而不進行治疗\[51\]。

### 给药途径

[阴道内给药是一種要將藥物施加在阴道內的](../Page/阴道内给药.md "wikilink")[給藥途徑](../Page/給藥途徑.md "wikilink")。药理学上，它具有潜在的优势，藥物主要會在阴道内或附近的结构（例如子宫颈）作用，相較於其他给药途径，造成的[不良反应會比較小](../Page/不良反应_\(医学\).md "wikilink")。像[陰道環就是阴道内给药的裝置](../Page/陰道環.md "wikilink")，可以治療更年期的[萎縮性陰道炎](../Page/萎縮性陰道炎.md "wikilink")，也有避孕用的[陰道避孕環](../Page/陰道避孕環.md "wikilink")。

## 參考文獻

## 參見

  - [陰道穹](../Page/陰道穹.md "wikilink")
  - [有牙陰道](../Page/有牙陰道.md "wikilink")
  - [陰戶口交](../Page/陰戶口交.md "wikilink")
  - [屄](../Page/屄.md "wikilink")
  - [雌蕊群](../Page/雌蕊群.md "wikilink")
  - [陰道排氣](../Page/陰道排氣.md "wikilink")
  - [凱格爾運動](../Page/凱格爾運動.md "wikilink")
  - [陰毛](../Page/陰毛.md "wikilink")
  - [阴道独白](../Page/阴道独白.md "wikilink")

[Category:女性生殖系统](../Category/女性生殖系统.md "wikilink")
[阴道](../Category/阴道.md "wikilink")

1.  Clinical pediatric urology: A. Barry Belman, Lowell R. King, Stephen
    Alan Kramer (2002)

2.

3.

4.

5.

6.

7.

8.
9.
10.

11.
12.

13.

14.

15.

16.

17.
18.
19.

20.

21.
22.
23.

24.

25.

26.

27.
28.

29.

30.

31.
32.
33.
34.
35.

36.

37.

38.

39.

40.

41.

42.

43. See, e.g., Colin Hinrichsen, Peter Lisowski, *Anatomy Workbook*
    (2007), p. 101: "Digital examination *per vaginam* are made by
    placing one or two fingers in the vagina".

44. [Vaginal pH
    Test](http://labmed.ucsf.edu/sfghlab/test/pdf%5CpH_Vaginal_Test_POLICY.pdf)
    from *Point of Care Testing*, July 2009, at: University of
    California, San Francisco – Department of Laboratory Medicine.
    Prepared by: Patricia Nassos, PhD, MT and Clayton Hooper, RN.

45.

46.
47.

48.

49.

50.

51.