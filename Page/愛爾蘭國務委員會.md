**愛爾蘭國務委員會**（英语：；[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：）是[愛爾蘭共和國的一個政府組織](../Page/愛爾蘭共和國.md "wikilink")，目的是在[愛爾蘭總統使用預留權利前給予忠告](../Page/愛爾蘭總統.md "wikilink")，也會必要時代替總統執行職務。

## 組成人員

愛爾蘭國務委員會由以下的人士組成：

### 官員

  - [總理](../Page/愛爾蘭總理.md "wikilink")
    (現由[利奧·瓦拉德卡擔任](../Page/利奧·瓦拉德卡.md "wikilink"))
  - [副總理](../Page/愛爾蘭副總理.md "wikilink") (現由[Simon
    Coveney擔任](../Page/Simon_Coveney.md "wikilink"))
  - [爱尔兰众议院议长](../Page/爱尔兰众议院.md "wikilink")
    (現由[斯恩·巴雷特擔任](../Page/斯恩·巴雷特.md "wikilink"))
  - [爱尔兰参议院议长](../Page/爱尔兰参议院.md "wikilink")
    (現由[派迪·柏克擔任](../Page/派迪·柏克.md "wikilink"))
  - [愛爾蘭最高法院首席大法官](../Page/愛爾蘭最高法院首席大法官.md "wikilink")
    (現由[蘇珊·鄧咸擔任](../Page/蘇珊·鄧咸.md "wikilink"))
  - [愛爾蘭高等法院主席](../Page/愛爾蘭高等法院主席.md "wikilink")
    (現由[尼古拉斯·科恩擔任](../Page/尼古拉斯·科恩.md "wikilink"))
  - [愛爾蘭律政司](../Page/愛爾蘭律政司.md "wikilink")
    (現由[邁爾·維蘭擔任](../Page/邁爾·維蘭.md "wikilink"))

### 前官員

  - 前愛爾蘭總統
      - [瑪麗·羅賓遜](../Page/瑪麗·羅賓遜.md "wikilink")
      - [瑪麗·麥卡利斯](../Page/瑪麗·麥卡利斯.md "wikilink") (2011年11月11日開始)
  - 前愛爾蘭總理
      - [約翰·阿洛伊西斯·科斯特洛](../Page/約翰·阿洛伊西斯·科斯特洛.md "wikilink")
      - [加勒特·菲茨傑拉德](../Page/加勒特·菲茨傑拉德.md "wikilink")
      - [艾伯特·雷諾茲](../Page/艾伯特·雷諾茲.md "wikilink")
      - [約翰·布魯頓](../Page/約翰·布魯頓.md "wikilink")
      - [伯蒂·埃亨](../Page/伯蒂·埃亨.md "wikilink")
      - [布賴恩·考恩](../Page/布賴恩·考恩.md "wikilink")
  - 前愛爾蘭最高法院首席大法官
      - [托瑪斯·芬尼](../Page/托瑪斯·芬尼.md "wikilink") (Thomas Finlay)
      - [羅蘭·堅](../Page/羅蘭·堅.md "wikilink") (Ronan Keane)
      - [約翰·萊奧納·美利](../Page/約翰·萊奧納·美利.md "wikilink") (John Loyola
        Murray)
  - 前[愛爾蘭自由邦行政會議主席](../Page/愛爾蘭自由邦.md "wikilink")
      - *永久懸空。最後一名自由邦行政會議主席已於1975年去世。*

### 「總統七人組」

  - 米高·費路 (Michael Farrell，愛爾蘭及[北愛爾蘭民運人士](../Page/北愛爾蘭.md "wikilink"))
  - 迪爾德麗·希南 (Deirdre Heenan，[阿爾斯特大學副校長](../Page/阿爾斯特大學.md "wikilink"))
  - 嘉芙蓮·麥堅尼斯 (Catherine McGuinness，前愛爾蘭最高法院法官)
  - 勞利·麥科爾南 (Ruairí McKiernan，[公益創業家](../Page/公益創業.md "wikilink"))
  - 莎莉·馬爾雷迪 (Sally
    Mulready，[英國地區政客](../Page/英國.md "wikilink")，為居於英國的愛爾蘭僑胞服務多年。是委員會裡唯一一個居於愛爾蘭以外的委員)
  - 傑阿羅伊德·奧杜哈 (Gearóid Ó
    Tuathaigh，[愛爾蘭國立大學戈爾韋榮譽歷史教授](../Page/愛爾蘭國立大學戈爾韋.md "wikilink"))
  - 傑拉德·奎恩 (Gerard Quinn，愛爾蘭國立大學戈爾韋法律教授)

## 會議

  - [愛爾蘭國務委員會2004年會議](../Page/愛爾蘭國務委員會2004年會議.md "wikilink")

{{-}}