[Overlook_Java_Road_at_night.jpg](https://zh.wikipedia.org/wiki/File:Overlook_Java_Road_at_night.jpg "fig:Overlook_Java_Road_at_night.jpg")
[Java_Road_near_ICAC_Headquarters.jpg](https://zh.wikipedia.org/wiki/File:Java_Road_near_ICAC_Headquarters.jpg "fig:Java_Road_near_ICAC_Headquarters.jpg")\]\]

**渣華道**是[香港](../Page/香港.md "wikilink")[東區的一條著名道路](../Page/東區_\(香港\).md "wikilink")，位於[北角](../Page/北角.md "wikilink")[英皇道之北](../Page/英皇道.md "wikilink")，由西至東連接[炮台山及](../Page/炮台山.md "wikilink")[鰂魚涌](../Page/鰂魚涌.md "wikilink")，於西端與[電氣道連接](../Page/電氣道.md "wikilink")，並連接東區走廊直達九龍東、筲箕灣、柴灣和小西灣，長約1,700米。

## 歷史

渣華道初期稱為「爪哇道」，指[印尼](../Page/印尼.md "wikilink")[爪哇島](../Page/爪哇島.md "wikilink")。1900年代起，有一間「爪哇輪船公司（Royal
Interocean Lines -
RIL）」於北角設立辦事處，並開辦往來[上海](../Page/上海.md "wikilink")、[香港](../Page/香港.md "wikilink")、[雅加達](../Page/雅加達.md "wikilink")、[萬隆](../Page/萬隆.md "wikilink")、[泗水等地方的輪船航線](../Page/泗水_\(印尼\).md "wikilink")。1933年，渣華道落成，[香港政府便以](../Page/香港政府.md "wikilink")「爪哇輪船公司」之名，將道路命名為爪哇道。亦有一說，爪哇道的名稱是源自[郭春秧所興建的](../Page/郭春秧.md "wikilink")「爪哇運糖碼頭」\[1\]。1950年代，[香港政府大規模發展](../Page/香港政府.md "wikilink")[北角區](../Page/北角.md "wikilink")，爪哇道亦改名為較為文雅的渣華道。

## 沿途地標

  - [皇都戲院大廈](../Page/皇都戲院.md "wikilink")
  - [福建中學](../Page/福建中學.md "wikilink")
  - [糖水道](../Page/糖水道.md "wikilink")[東區走廊出入口](../Page/東區走廊.md "wikilink")

東區走廊渣華道出口

  - [書局街](../Page/書局街.md "wikilink")[渣華道市政大廈](../Page/渣華道市政大廈.md "wikilink")
  - [北角渡輪碼頭及北角巴士總站](../Page/北角渡輪碼頭.md "wikilink")
  - 宜必思香港北角酒店
  - [北角邨地皮綜合發展項目](../Page/北角邨地皮綜合發展項目.md "wikilink")（[北角邨舊址](../Page/北角邨.md "wikilink")）
  - [港濤軒](../Page/港濤軒.md "wikilink")
  - [香港中國旅行社北角分社及](../Page/香港中國旅行社.md "wikilink")[北角街坊會陳樹渠大會堂](../Page/北角街坊會.md "wikilink")
  - 香港海關總部大樓
  - [民康街](../Page/民康街.md "wikilink")[嘉華國際中心](../Page/嘉華國際中心.md "wikilink")

東區走廊民康街出入口

  - [廉政公署總部大樓](../Page/廉政公署總部大樓.md "wikilink")
  - [英皇道625號](../Page/英皇道625號.md "wikilink")
  - [北角政府合署](../Page/北角政府合署.md "wikilink")
  - [北角海逸酒店](../Page/北角海逸酒店.md "wikilink")
  - 香港警察東區總部及[北角分區警署](../Page/北角分區警署.md "wikilink")
  - [泓富產業千禧廣場](../Page/泓富產業千禧廣場.md "wikilink")
  - [香港殯儀館](../Page/香港殯儀館.md "wikilink")
  - [渣華道遊樂場](../Page/渣華道遊樂場.md "wikilink")
  - [粵華酒店](../Page/粵華酒店.md "wikilink")
  - [中華煤氣行政大樓](../Page/北角地盤升降機墜下事故.md "wikilink")

## 沿途建築物

<File:Ibis> Hong Kong North Point Hotel
2017.jpg|[宜必思香港北角酒店](../Page/宜必思酒店.md "wikilink")
<File:North> Point Welfare Assoication
2017.jpg|[北角街坊會陳樹渠大會堂](../Page/北角街坊會.md "wikilink")
<File:Customs> Headquarters Building (Hong
Kong).jpg|[香港海關總部大樓](../Page/香港海關.md "wikilink")
<File:HK> ICAC HQBuilding.JPG|[廉政公署總部大樓](../Page/廉政公署總部大樓.md "wikilink")
<File:HK> NP Java Road Ka Wah Centre
o.JPG|[嘉華國際中心](../Page/嘉華國際中心.md "wikilink")

## 資料來源

## 參見

  - [英皇道](../Page/英皇道.md "wikilink")
  - [電氣道](../Page/電氣道.md "wikilink")

[Category:北角街道](../Category/北角街道.md "wikilink")

1.