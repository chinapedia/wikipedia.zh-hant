**黃立極**（），[字](../Page/表字.md "wikilink")**石笥**，又字**中五**，[號](../Page/號.md "wikilink")**我范**，明朝政治人物，[直隸](../Page/北直隸.md "wikilink")[元城縣](../Page/元城縣.md "wikilink")（今[河北](../Page/河北.md "wikilink")[大名](../Page/大名县.md "wikilink")）人。

## 生平

[萬曆三十二年](../Page/萬曆.md "wikilink")（1604年）登甲辰科進士。累官[少詹事](../Page/少詹事.md "wikilink")、[禮部侍郎](../Page/禮部侍郎.md "wikilink")。[明熹宗即位後](../Page/明熹宗.md "wikilink")，以同鄉成為[魏忠賢的親信](../Page/魏忠賢.md "wikilink")，[天启五年](../Page/天启_\(明朝\).md "wikilink")（1625年）擢[禮部尚書](../Page/禮部尚書.md "wikilink")，兼[東閣大學士](../Page/東閣大學士.md "wikilink")，入閣參預機務，不久晉升[太子太保](../Page/太子太保.md "wikilink")，[文淵閣大學士](../Page/文淵閣大學士.md "wikilink")。次年遷[武英殿](../Page/武英殿.md "wikilink")，[建極殿大學士](../Page/建極殿大學士.md "wikilink")，取代[顧秉謙為](../Page/顧秉謙.md "wikilink")[首輔](../Page/首輔.md "wikilink")，以“夜半片紙了當之”一語促魏忠賢於半夜誣殺[熊廷弼](../Page/熊廷弼.md "wikilink")。\[1\]天啟七年（1627年）熹宗病危，召諭廷臣說，魏忠贤等“皆恪谨忠贞、可计大事。”黃立極等對曰：“陛下任贤勿贰，诸臣敢不仰体？”熹宗聽了高興，又下一道遗诏说：“以皇五弟信王由检嗣皇帝位。”\[2\]

[崇禎帝即位](../Page/崇禎帝.md "wikilink")，力除魏黨，有[山陰監生](../Page/山陰.md "wikilink")[胡煥猷越俎上书](../Page/胡煥猷.md "wikilink")，弹劾黃立極、[施鳳來](../Page/施鳳來.md "wikilink")、[張瑞圖等](../Page/張瑞圖.md "wikilink")“身居揆席，漫无主持，甚至顾命之重臣毙於詔狱。滥加阉寺五等之爵、尚公之尊，生祠碑颂无所不至。”\[3\]黃立極心不自安，上疏乞休，不久罷職。餘事不詳。\[4\]

## 注釋

## 參考資料

  - 《明史》列傳第一百九十四 閹黨

[Category:明朝詹事府少詹事](../Category/明朝詹事府少詹事.md "wikilink")
[Category:明朝禮部侍郎](../Category/明朝禮部侍郎.md "wikilink")
[Category:明朝禮部尚書](../Category/明朝禮部尚書.md "wikilink")
[Category:明朝東閣大學士](../Category/明朝東閣大學士.md "wikilink")
[Category:明朝内阁首辅](../Category/明朝内阁首辅.md "wikilink")
[Category:明朝文淵閣大學士](../Category/明朝文淵閣大學士.md "wikilink")
[Category:明朝建極殿大學士](../Category/明朝建極殿大學士.md "wikilink")
[Category:大名人](../Category/大名人.md "wikilink")
[L](../Category/黃姓.md "wikilink")

1.  [吴应箕](../Page/吴应箕.md "wikilink")：《两朝剥复录》，卷2
2.  《亁隆御批綱鑑》卷八
3.  《崇祯實錄》卷三
4.  《漢籍電子文獻資料庫-明人傳記資料索引》6786：黃立極，字中五，元城人。萬曆三十二年進士，累官禮部侍郎，天啟間魏忠賢以同鄕故，擢禮部尚書，兼東閣大學士，已而為首輔，晉建極殿大學士。後逆案定，落職閒住。