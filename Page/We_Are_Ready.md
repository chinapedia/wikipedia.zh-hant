《****》是[2008年北京奧運會組委會在](../Page/2008年北京奧運會.md "wikilink")[2007年8月為迎合奧運會倒數一周年而製作的歌曲](../Page/2007年8月.md "wikilink")。

主題歌曲的名字為「We Are
Ready」，中文意思就是「我們已作好準備」，加上以英文作歌名亦特顯出向世界宣告的意義，表示主辦單位已對於準備好奧運會的來臨。歌曲將於中國及世界各地播放。

這次的合唱歌曲邀請了來自[香港](../Page/香港.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[台灣三個地方](../Page/台灣.md "wikilink")133位歌手，香港方面有[譚詠麟](../Page/譚詠麟.md "wikilink")、[李克勤](../Page/李克勤.md "wikilink")、[陳奕迅](../Page/陳奕迅.md "wikilink")、[梁詠琪](../Page/梁詠琪.md "wikilink")、[容祖兒及](../Page/容祖兒.md "wikilink")[古巨基](../Page/古巨基.md "wikilink")\[1\]，中國大陸方面有[黃曉明](../Page/黃曉明.md "wikilink")、[李宇春及](../Page/李宇春.md "wikilink")[周筆暢等](../Page/周筆暢.md "wikilink")，而台灣方面則有[蕭亞軒](../Page/蕭亞軒.md "wikilink")、[游鴻明及](../Page/游鴻明.md "wikilink")[黃大煒等](../Page/黃大煒.md "wikilink")，有指這是20多年來參加歌手最多的一次大合唱。

製作這首歌曲的包括有统筹[李再唐](../Page/李再唐.md "wikilink")，作曲的[金培達及填詞的](../Page/金培達.md "wikilink")[陳少琪](../Page/陳少琪.md "wikilink")，他們三人早前已合作過[香港特別行政區成立十周年紀念的主題曲](../Page/香港特別行政區成立十周年紀念.md "wikilink")《[始終有你](../Page/始終有你.md "wikilink")》。三人稱只用了18小時就完成曲詞，而特別之處是统筹[李再唐提議加插了](../Page/李再唐.md "wikilink")[永樂大鐘的鐘聲作為前奏](../Page/永樂大鐘.md "wikilink")，增加了歌曲的中國色彩。

2007年，中國第一顆探月衛星[嫦娥一號中](../Page/嫦娥一號.md "wikilink")，也特別選用這首歌曲搭載。

到了2008年，[香港電台獲](../Page/香港電台.md "wikilink")[2008年北京奧運會組委會授權](../Page/第29屆奧林匹克運動會組織委員會.md "wikilink")，在同年1月1日和25日製作並發佈了這首歌的英文版本和粵語版本。

## 词曲作者

作曲：[金培达](../Page/金培达.md "wikilink") 作词：[陈少琪](../Page/陈少琪.md "wikilink")
\[2\]

## 演唱歌手

官方版本演唱歌手如下\[3\]：
[汪峰](../Page/汪峰.md "wikilink")、[张靓颖](../Page/张靓颖.md "wikilink")、[李宇春](../Page/李宇春.md "wikilink")、[陈奕迅](../Page/陈奕迅.md "wikilink")、[李克勤](../Page/李克勤.md "wikilink")、[古巨基](../Page/古巨基.md "wikilink")、[容祖儿](../Page/容祖儿.md "wikilink")、[黄晓明](../Page/黄晓明.md "wikilink")、[周笔畅](../Page/周笔畅.md "wikilink")、[胡彦斌](../Page/胡彦斌.md "wikilink")、[孙悦](../Page/孙悦_\(歌手\).md "wikilink")、[成方圆](../Page/成方圆.md "wikilink")、[杭天琪](../Page/杭天琪.md "wikilink")、[冯小泉](../Page/冯小泉.md "wikilink")、[曾格格](../Page/曾格格.md "wikilink")、[谭咏麟](../Page/谭咏麟.md "wikilink")、[萧亚轩](../Page/萧亚轩.md "wikilink")、[黄大炜](../Page/黄大炜.md "wikilink")、[游鸿明](../Page/游鸿明.md "wikilink")、[梁咏琪](../Page/梁咏琪.md "wikilink")、[柯以敏](../Page/柯以敏.md "wikilink")、[黄晓明](../Page/黄晓明.md "wikilink")、[朴树](../Page/朴树.md "wikilink")、[小柯](../Page/小柯.md "wikilink")、[庞龙](../Page/庞龙.md "wikilink")、[纪敏佳](../Page/纪敏佳.md "wikilink")、[李慧珍](../Page/李慧珍.md "wikilink")、[尹相杰](../Page/尹相杰.md "wikilink")、[吴彤](../Page/吴彤.md "wikilink")、[罗中旭](../Page/罗中旭.md "wikilink")、[郭蓉](../Page/郭蓉.md "wikilink")、[汪正正](../Page/汪正正.md "wikilink")、[峦树](../Page/峦树.md "wikilink")、[陈红](../Page/陈红_\(歌手\).md "wikilink")、[林萍](../Page/林萍.md "wikilink")、[魏金栋](../Page/魏金栋.md "wikilink")、[韦嘉](../Page/韦嘉.md "wikilink")、[熊汝霖](../Page/熊汝霖.md "wikilink")、[海明威](../Page/海明威.md "wikilink")、[尚雯捷](../Page/尚雯捷.md "wikilink")、[乔任梁](../Page/乔任梁.md "wikilink")、[邵雨涵](../Page/邵雨涵.md "wikilink")、[关哲](../Page/关哲.md "wikilink")、[李健](../Page/李健.md "wikilink")、[黄琦雯](../Page/黄琦雯.md "wikilink")、[杨臣刚](../Page/杨臣刚.md "wikilink")、[李汶](../Page/李汶.md "wikilink")、[李殊](../Page/李殊.md "wikilink")、[衡越](../Page/衡越.md "wikilink")、[李延亮](../Page/李延亮.md "wikilink")、[浩天](../Page/浩天.md "wikilink")、[谭维维](../Page/谭维维.md "wikilink")、[厉娜](../Page/厉娜.md "wikilink")、[刘嘉亮](../Page/刘嘉亮.md "wikilink")、[侯旭](../Page/侯旭.md "wikilink")、[王强](../Page/王强.md "wikilink")、[胡杨林](../Page/胡杨林.md "wikilink")、[师鹏](../Page/师鹏.md "wikilink")、[无极生](../Page/无极生.md "wikilink")、[马天宇](../Page/马天宇.md "wikilink")、[誓言](../Page/誓言.md "wikilink")、[严当当](../Page/严当当.md "wikilink")、[胡维娜](../Page/胡维娜.md "wikilink")、[亚民](../Page/亚民.md "wikilink")、[傅莉珊](../Page/傅莉珊.md "wikilink")、[胡力](../Page/胡力.md "wikilink")、[王莹菲](../Page/王莹菲.md "wikilink")、[麦穗](../Page/麦穗.md "wikilink")、[董路](../Page/董路.md "wikilink")、[徐莉](../Page/徐莉.md "wikilink")、[湘海](../Page/湘海.md "wikilink")、[梅琳](../Page/梅琳.md "wikilink")、[韩晶](../Page/韩晶.md "wikilink")、[红霞](../Page/红霞.md "wikilink")、[萨仁呼](../Page/萨仁呼.md "wikilink")、[周奇奇](../Page/周奇奇.md "wikilink")、[董成鹏](../Page/董成鹏.md "wikilink")、[齐峰](../Page/齐峰.md "wikilink")、[雪莲三姐妹](../Page/雪莲三姐妹.md "wikilink")、[蚂蚁组合](../Page/蚂蚁组合.md "wikilink")、[花儿乐队](../Page/花儿乐队.md "wikilink")、[天山雪莲](../Page/天山雪莲.md "wikilink")、[和平组合](../Page/和平组合.md "wikilink")、[蝌蚪组合](../Page/蝌蚪组合.md "wikilink")、[风云组合](../Page/风云组合.md "wikilink")、[吉祥三宝](../Page/吉祥三宝.md "wikilink")、[子日乐队](../Page/子日乐队.md "wikilink")、[零点乐队](../Page/零点乐队.md "wikilink")、[中国辣妹](../Page/中国辣妹.md "wikilink")、[东来东往](../Page/东来东往.md "wikilink")、[凤凰传奇](../Page/凤凰传奇.md "wikilink")、[丁香小小](../Page/丁香小小.md "wikilink")、[水木年华](../Page/水木年华.md "wikilink")、[扎西顿珠](../Page/扎西顿珠.md "wikilink")、[便利商店乐队](../Page/便利商店乐队.md "wikilink")、[牛奶咖啡组合](../Page/牛奶咖啡组合.md "wikilink")

## 參考文獻

  - [奧運會倒計時一週年官方活動](https://web.archive.org/web/20080907015939/http://www.beijing2008.cn/1year/officialcampaign/)，北京2008年第29屆奧運會官方網站
  - [百名歌星傾情獻聲《We Are
    Ready》](https://web.archive.org/web/20080106113224/http://big5.cctv.com/program/zykb/20070801/102938.shtml)，央視國際，2007-08-01
  - [隔牆有耳：We Are Ready
    暗藏普選玄機](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070805&sec_id=4104&subsec_id=15333&art_id=7406842&cat_id=45&coln_id=20)，《蘋果日報》，2007-08-05
  - [Theme song for the one-year countdown
    released](http://www.chinadaily.com.cn/language_tips/2007-08/06/content_5444219.htm)，China
    Daily, 2007-08-06
  - [奧運倒數一周年振奮人心](https://web.archive.org/web/20070927201151/http://www.takungpao.com:82/news/07/08/09/LTA-777640.htm)，《大公報》，2007-08-09
  - Johnny Neihu: [Johnny Neihu's NewsWatch: The Olympic decathlon of
    death](http://www.taipeitimes.com/News/editorials/archives/2007/08/11/2003373694)，Taipei
    Times, 2007-08-11

## 参看

  - [北京欢迎你](../Page/北京欢迎你.md "wikilink")
  - [北京2008年奥运会歌曲专辑](../Page/北京2008年奥运会歌曲专辑.md "wikilink")

## 外部連結

  - [We Are Ready](http://zh.olympic2008.wikia.com/wiki/We_Are_Ready)
  - [北京奧運會倒計時一週年主題曲《we are
    ready》](https://web.archive.org/web/20080906213556/http://www.beijing2008.cn/video/promotional/weareready/)，北京2008年第29屆奧運會官方網站
  - [We Are Ready (2008 Beijing Olympics Theme
    Song)](http://www.youtube.com/watch?v=Sq4oNqkZQbE)，YouTube
  - [Theme Song for Beijing Olympic 1 Yr Countdown - We are
    Ready](http://www.youtube.com/watch?v=1XOkYTzMHWc)，YouTube
  - [周筆暢070808天安門倒計時一週年晚會演唱WE ARE
    READY](http://www.youtube.com/watch?v=tDqYZn_a6hI)，YouTube
  - [奧運風采 香港電台 - We are
    ready（原裝普通話版本）](http://app1.rthk.org.hk/special/beijingolympic/gallery.php?name=weareready&media=video)
  - [奧運風采 香港電台 - We are
    ready（英文版本）](http://app1.rthk.org.hk/special/beijingolympic/gallery.php?name=weareready_int&media=audio)
  - [奧運風采 香港電台 - We are
    ready（粵語版本）](http://app1.rthk.org.hk/special/beijingolympic/gallery.php?name=weareready_cantonese&media=audio)

[Category:2008年夏季奧林匹克运动会歌曲](../Category/2008年夏季奧林匹克运动会歌曲.md "wikilink")
[Category:中华人民共和国歌曲作品](../Category/中华人民共和国歌曲作品.md "wikilink")
[Category:中文流行歌曲](../Category/中文流行歌曲.md "wikilink")

1.  [陳奕迅不會補送婚戒，《明報》，2007-07-30](http://ol.mingpao.com/cfm/Archive1.cfm?File=20070730/saa01/mbc1.txt)
2.  [奥运歌曲MV——《we are
    ready》](http://www.beijing2008.cn/video/promotional/we-are-ready/)

3.  [奥运歌曲MV——《we are
    ready》](http://www.beijing2008.cn/video/promotional/we-are-ready/)