**瑪俐亞**（，），暱稱**「肥媽」**，為生於[澳門的](../Page/澳門.md "wikilink")[土生葡人](../Page/澳門土生葡人.md "wikilink")，[香港著名藝員](../Page/香港.md "wikilink")，發展遍及樂壇、食評、節目主持及影視。

## 背景

肥媽早年在[香港島](../Page/香港島.md "wikilink")[西營盤和](../Page/西營盤.md "wikilink")[中環居住](../Page/中環.md "wikilink")，畢業於官立嘉道理爵士小學和[官立嘉道理爵士中學](../Page/官立嘉道理爵士中學（西九龍）.md "wikilink")\[1\]。早逝的父親是在[澳門工作的](../Page/澳門.md "wikilink")[葡萄牙人](../Page/葡萄牙人.md "wikilink")，而母親則是[華人](../Page/華人.md "wikilink")，但她常被誤會為[菲律賓人](../Page/菲律賓人.md "wikilink")。肥媽在十一歲的時候喪父，之後她背負起養活一家七口的重任，曾在碼頭當苦力。进入[香港娱乐圈前](../Page/香港娱乐圈.md "wikilink")，她曾为生計糊口做过廚師，因此煮功厨艺了得。後來因為聲線嘹亮而被發掘到酒廊當歌手。期間她在各大夜總會收集歌手的資料，再引介歌手到餐廳演唱，充當經紀人抽佣金，她的出道經過源於有一次有位酒廊歌手放鴿子，樂手要肥媽出場，她從此便成為一名酒廊歌手。及後被[岑建勋](../Page/岑建勋.md "wikilink")、[陈欣健和](../Page/陈欣健.md "wikilink")[泰迪·罗宾发掘](../Page/泰迪·罗宾.md "wikilink")，开始了她的演艺人生。

1983年入行前曾任職一間菲律賓餐廳，曾接受[亞洲電視的](../Page/亞洲電視.md "wikilink")[下午茶訪問](../Page/下午茶_\(亞洲電視節目\).md "wikilink")。

1987年她憑著在[周潤發電影](../Page/周潤發.md "wikilink")《[監獄風雲](../Page/監獄風雲.md "wikilink")》中，主唱《[友誼之光](../Page/綠島小夜曲.md "wikilink")》及《[充滿希望](../Page/充滿希望.md "wikilink")》而成名，成為香港樂壇的另类歌手，《[友誼之光](../Page/綠島小夜曲.md "wikilink")》更成为香港乐坛的经典作品。瑪俐亞善於演繹[黑人](../Page/黑人.md "wikilink")[爵士風格](../Page/爵士樂.md "wikilink")[歌曲](../Page/歌曲.md "wikilink")，在1980年代未能為大部份香港人接受，而她的歌曲多半已經絕版，只可以間中在當時的香港電影內聽到，例如《褲甲天下》中的《痴心當泥「辦」》、《法內情大結局》的《媽媽I
Love You》等。肥媽曾在自己的音樂特輯中透露除非徇眾要求，否則自己盡量不會在公開場合演唱《媽媽I Love
You》，因為此曲會令她感動落淚。

她在1990年代末，開始受聘於[香港有線電視](../Page/香港有線電視.md "wikilink")，並在[有線娛樂台開台時的娛樂節目](../Page/有線娛樂台.md "wikilink")——《肥媽Phone
Show》擔任主持人。因為《肥媽Phone
Show》節目大受歡迎，所以在2001年左右，瑪俐亞又在[香港有線電視同](../Page/香港有線電視.md "wikilink")[梁家仁主持另一個節目](../Page/梁家仁.md "wikilink")——《肥媽一家人》，節目的主要內容就是探討親子問題與及一週內的社會新聞，而每星期都有一個特定的主題。2003年4月至2009年11月15日，她為有線電視主持了長達七年的烹飪節目《[肥媽私房菜](../Page/肥媽私房菜.md "wikilink")》，在此期間她成功減肥，並出書講述有關的經歷。2009年，瑪俐亞為[亞洲電視](../Page/亞洲電視.md "wikilink")[選秀節目](../Page/選秀.md "wikilink")[亞洲星光大道首席評判](../Page/亞洲星光大道.md "wikilink")，獲得好評。其後，她轉往[亞洲電視主持](../Page/亞洲電視.md "wikilink")《肥媽老友記》節目。

2008年她獲得[香港特別行政區政府頒授](../Page/香港特別行政區政府.md "wikilink")[榮譽勳章](../Page/榮譽勳章_\(香港\).md "wikilink")。2012年，她回「娘家」[無線電視](../Page/電視廣播有限公司.md "wikilink")，拍攝飲食節目《[搵食](../Page/搵食.md "wikilink")》、《[食平D](../Page/食平D.md "wikilink")》等節目。翌年，參與
《澳門街》一戲，成為第五十六屆亞太影展之焦點電影。電影未正式上演，瑪俐亞在戲中的演技已得到各國電影人之讚賞。之後她因為主持的飲食節目《[食平D](../Page/食平D.md "wikilink")》獲得觀眾歡迎，所以在2014年及2015年分别獲安排主持《[食平DD](../Page/食平DD.md "wikilink")》以及《[食平3D](../Page/食平3D.md "wikilink")》，觀眾反應依然良好。

2014年她再獲[香港特別行政區政府頒授銅紫荊星章](../Page/香港特別行政區政府.md "wikilink")\[2\]。於2015年6月8日和[陸浩明上契](../Page/陸浩明.md "wikilink")，而陸浩明是肥媽第一個正式上契的契仔。

2016年3月，在[香港體育館舉行唯一一次個人演唱會](../Page/香港體育館.md "wikilink")。於《[勁歌金曲](../Page/勁歌金曲.md "wikilink")》澄清自己並非封咪，仍會參與慈善、小型演出等活動，更表示不會放棄烹飪節目，並於8月播出[食平D系列第五輯](../Page/食平D系列.md "wikilink")《[今個夏天食平D](../Page/今個夏天食平D.md "wikilink")》。

2017年4月，為[無綫電視拍攝第六輯](../Page/無綫電視.md "wikilink")[食平D系列](../Page/食平D系列.md "wikilink")《[阿媽教落食平D](../Page/阿媽教落食平D.md "wikilink")》。

2018年10月，再次為無綫電視拍攝食平D系列《[肥媽優質食好D](../Page/肥媽優質食好D.md "wikilink")》。

## 音樂作品

### 唱片

| 發行日期     | 專輯名稱                                                         | 發行公司                                                                           | 備註   |
| -------- | ------------------------------------------------------------ | ------------------------------------------------------------------------------ | ---- |
| 1987年6月  | [監獄風雲](../Page/監獄風雲_\(EP\).md "wikilink")                    | [銀星唱片](../Page/銀星唱片.md "wikilink")                                             | 粵語大碟 |
| 1987年12月 | [Maria Cordero](../Page/Maria_Cordero.md "wikilink")         | 銀星唱片                                                                           | 粵語大碟 |
| 1989年    | [三千日後盡豪情](../Page/三千日後盡豪情.md "wikilink")                     | 現代唱片                                                                           | 粵語大碟 |
| 1990年1月  | [新歌風雲集](../Page/新歌風雲集.md "wikilink")                         | 銀星唱片                                                                           | 粵語大碟 |
| 1991年1月  | [幾許情深](../Page/幾許情深.md "wikilink")                           | 現代唱片                                                                           | 粵語大碟 |
| 1993年    | [轟轟烈烈愛一場](../Page/轟轟烈烈愛一場.md "wikilink")                     | [波麗佳音](../Page/波麗佳音.md "wikilink")                                             | 國語大碟 |
| 2000年12月 | [Maria And Friends](../Page/Maria_And_Friends.md "wikilink") | [百利唱片](../Page/百利唱片.md "wikilink")                                             | 粵語大碟 |
| 2005年6月  | [肥媽正傳](../Page/肥媽正傳.md "wikilink")                           | [塗鴉文化](../Page/塗鴉文化.md "wikilink")                                             | 國語大碟 |
| 2012年    | [Maria](../Page/Maria.md "wikilink")                         | [New Century Workshop (HK)](../Page/New_Century_Workshop_\(HK\).md "wikilink") | 粵語大碟 |

### 其他歌曲

  - 1994年：《[子夜情人](../Page/城中姐妹花2之子夜情人.md "wikilink")》主題曲《戀戀紅塵》
  - 2002年：亞視劇集《[法內情2002](../Page/法內情2002.md "wikilink")》插曲、[民主倒董力量會歌](../Page/民主倒董力量.md "wikilink")、[香港人民廣播電台節目](../Page/香港人民廣播電台.md "wikilink")《風蕭蕭》主題曲《為了夢想》。
  - 2009年：[東亞運動會主題曲](../Page/東亞運動會.md "wikilink")《[衝出世界](../Page/衝出世界.md "wikilink")》
  - 2017年：無綫劇集《[味想天開](../Page/味想天開.md "wikilink")》主題曲《天賜的滋味》

## 演出

### 電影

  - 1986年：《[神探朱古力](../Page/神探朱古力.md "wikilink")》
  - 1987年：《[龍虎風雲](../Page/龍虎風雲.md "wikilink")》
  - 1987年：《[你OK，我OK\!](../Page/你OK，我OK!.md "wikilink")》
  - 1987年：《[精裝追女仔](../Page/精裝追女仔.md "wikilink")》
  - 1988年：《[鬼馬保鑣賊美人](../Page/鬼馬保鑣賊美人.md "wikilink")》
  - 1988年：《[女子監獄](../Page/女子監獄_\(香港電影\).md "wikilink")》 飾 獄卒蔡姑娘
  - 1988年：《[獵鷹行動](../Page/獵鷹行動.md "wikilink")》
  - 1988年：《[火舞風雲](../Page/火舞風雲.md "wikilink")》
  - 1988年：《[褲甲天下](../Page/褲甲天下.md "wikilink")》
  - 1989年：《[群龍戲鳳](../Page/群龍戲鳳.md "wikilink")》
  - 1989年：《[專釣大鱷](../Page/專釣大鱷.md "wikilink")》飾 瑪利亞
  - 1989年：《[新最佳拍檔](../Page/新最佳拍檔.md "wikilink")》
  - 1989年：《[小小小警察](../Page/小小小警察.md "wikilink")》
  - 1989年：《[黑道福星](../Page/黑道福星.md "wikilink")》
  - 1989年：《[大男人小傳](../Page/大男人小傳.md "wikilink")》
  - 1989年：《[合家歡](../Page/合家歡.md "wikilink")》飾 鞋店老闆娘
  - 1989年：《[一眉道人](../Page/一眉道人.md "wikilink")》飾 教堂修女長
  - 1989年：《[開心巨無霸](../Page/開心巨無霸.md "wikilink")》飾 何小珊
  - 1990年：《[監獄不設防](../Page/監獄不設防.md "wikilink")》飾 大家姐
  - 1990年：《[小心間諜](../Page/小心間諜.md "wikilink")》
  - 1990年：《[老虎出更2](../Page/老虎出更2.md "wikilink")》
  - 1991年：《[豪門夜宴](../Page/豪門夜宴.md "wikilink")》
  - 1992年：《[阿二一族](../Page/阿二一族.md "wikilink")》飾 陳志強妻
  - 1992年：《[童黨之街頭霸王](../Page/童黨之街頭霸王.md "wikilink")》飾 肥媽
  - 1993年：《[飛越謎情](../Page/飛越謎情.md "wikilink")》
  - 1993年：《[北妹](../Page/北妹.md "wikilink")》
  - 1993年：《[危情](../Page/危情.md "wikilink")》
  - 1994年：《[奸人世家](../Page/奸人世家.md "wikilink")》
  - 1995年：《[街坊差人](../Page/街坊差人.md "wikilink")》
  - 1996年：《[1/2 次同床](../Page/1/2_次同床.md "wikilink")》
  - 1996年：《[天涯海角](../Page/天涯海角.md "wikilink")》飾 珍媽
  - 1996年：《[古惑女之決戰江湖](../Page/古惑女之決戰江湖.md "wikilink")》飾 肥媽
  - 1996年：《[紅燈區](../Page/紅燈區_\(電影\).md "wikilink")》
  - 1997年：《[最佳拍檔之醉街拍檔](../Page/最佳拍檔之醉街拍檔.md "wikilink")》
  - 2001年：《[Fing頭K王之王](../Page/Fing頭K王之王.md "wikilink")》
  - 2002年：《[一屋貪錢人](../Page/一屋貪錢人.md "wikilink")》
  - 2003年：《[大丈夫](../Page/大丈夫_\(電影\).md "wikilink")》飾Jo Jo
  - 2003年：《[我要結婚](../Page/我要結婚.md "wikilink")》
  - 2003年：《[好郁](../Page/好郁.md "wikilink")》
  - 2004年：《[天作之盒](../Page/天作之盒.md "wikilink")》飾 珠寶店女老闆
  - 2004年：《[浪漫春情](../Page/浪漫春情.md "wikilink")》
  - 2004年：《[游龍戲鳳](../Page/游龍戲鳳_\(2009年電影\).md "wikilink")》
  - 2008年：《[奪標](../Page/奪標_\(電影\).md "wikilink")》
  - 2010年：《[72家租客](../Page/72家租客.md "wikilink")》飾 開Phone府員工
  - 2012年：《[八星抱喜](../Page/八星抱喜.md "wikilink")》
  - 2012年：《[2012我愛HK 喜上加囍](../Page/2012我愛HK_喜上加囍.md "wikilink")》
  - 2012年：《[爛賭夫鬥爛賭妻](../Page/爛賭夫鬥爛賭妻.md "wikilink")》
  - 2014年：《[賭城風雲](../Page/賭城風雲_\(2014年電影\).md "wikilink")》 飾 蕭雲
  - 2016年：《[賭城風雲III](../Page/賭城風雲III.md "wikilink")》 飾 獄警
  - 2016年：《[打開我天空](../Page/打開我天空.md "wikilink")》飾 婆婆

### 電視劇

  - 1988年：[無綫電視](../Page/TVB.md "wikilink")《[季節](../Page/季節.md "wikilink")》客串媽打新請回來的菲傭，並在年初一吃到有金幣的一個湯圓，亦因此經常被當作菲律賓人\[3\]
  - 1992年：[廉政公署](../Page/廉政公署.md "wikilink")《[廉政行動](../Page/廉政行動_\(電視劇\).md "wikilink")》飾演
    周師奶
  - 1994年：[亞洲電視](../Page/亞洲電視.md "wikilink")《[子夜情人](../Page/城中姐妹花2之子夜情人.md "wikilink")》飾演
    肥媽
  - 1994年：[亞洲電視](../Page/亞洲電視.md "wikilink")《[戲王之王](../Page/戲王之王.md "wikilink")》(客串第1集)
  - 2000年：[亞洲電視](../Page/亞洲電視.md "wikilink")《[快活谷](../Page/快活谷_\(電視劇\).md "wikilink")》飾演
    紅豆冰
  - 2010年：[亞洲電視](../Page/亞洲電視.md "wikilink")《[香港GoGoGo](../Page/香港GoGoGo.md "wikilink")》(客串第74集)
  - 2014年：[SBS](../Page/SBS.md "wikilink")《[誘惑](../Page/誘惑_\(韓國電視劇\).md "wikilink")》飾演
    Tina (客串第1集)
  - 2017年：[無綫電視](../Page/無綫電視.md "wikilink")《[味想天開](../Page/味想天開.md "wikilink")》飾演
    何大莉
  - 2017年：[無綫電視](../Page/無綫電視.md "wikilink")《[溏心風暴3](../Page/溏心風暴3.md "wikilink")》飾演
    馮青 (客串第1集)

### 電視節目（主持）

  - [有線電視](../Page/香港有線電視.md "wikilink")[娛樂台](../Page/有線娛樂台.md "wikilink")：《肥媽Phone
    Show》
  - [有線電視](../Page/香港有線電視.md "wikilink")[娛樂台](../Page/有線娛樂台.md "wikilink")：《娛樂熱賣10點半》
  - [有線電視](../Page/香港有線電視.md "wikilink")[娛樂台](../Page/有線娛樂台.md "wikilink")：《肥媽一家仁/一家人》
  - [有線電視](../Page/香港有線電視.md "wikilink")[娛樂台](../Page/有線娛樂台.md "wikilink")：《[肥媽私房菜](../Page/肥媽私房菜.md "wikilink")》
  - [亞洲電視](../Page/亞洲電視.md "wikilink")[本港台](../Page/本港台.md "wikilink")：《[亞洲星光大道](../Page/亞洲星光大道.md "wikilink")》首席評判
  - [亞洲電視](../Page/亞洲電視.md "wikilink")[本港台](../Page/本港台.md "wikilink")：《[肥媽老友記](../Page/肥媽老友記.md "wikilink")》
  - [亞洲電視](../Page/亞洲電視.md "wikilink")[本港台](../Page/本港台.md "wikilink")：《[亞洲星光大道4](../Page/亞洲星光大道4.md "wikilink")》評判
  - [有線電視](../Page/香港有線電視.md "wikilink")[娛樂台](../Page/有線娛樂台.md "wikilink")：《肥媽當家》
  - [無綫電視](../Page/無綫電視.md "wikilink")[翡翠台](../Page/翡翠台.md "wikilink")：《[搵食](../Page/搵食.md "wikilink")》
  - 無綫電視翡翠台：《[食平D](../Page/食平D.md "wikilink")》
  - 無綫電視翡翠台：《[食平DD](../Page/食平DD.md "wikilink")》（第2輯）
  - 無綫電視翡翠台：《[食平3D](../Page/食平3D.md "wikilink")》（第3輯）
  - 無綫電視翡翠台：《[暖DD·食平D](../Page/暖DD·食平D.md "wikilink")》（第4輯）
  - 無綫電視翡翠台：《[今個夏天食平D](../Page/今個夏天食平D.md "wikilink")》（第5輯）
  - 無綫電視翡翠台：《[阿媽教落食平D](../Page/阿媽教落食平D.md "wikilink")》（第6輯）
  - 無綫電視[Astro](../Page/Astro.md "wikilink")：《肥媽新年新煮意》
  - 無綫電視翡翠台：《[食好D 食平D](../Page/食好D_食平D.md "wikilink")》（第7輯）
  - 無綫電視翡翠台：[大灣區 活好D](../Page/大灣區_活好D.md "wikilink")
  - 無綫電視翡翠台：[肥媽優質食好D](../Page/肥媽優質食好D.md "wikilink")
  - 無綫電視翡翠台：《[食好D 食平D](../Page/食好D_食平D.md "wikilink")》（第二輯）

### 綜藝節目

  - 2014年：[無綫電視](../Page/無綫電視.md "wikilink")《[Sunday扮嘢王](../Page/Sunday扮嘢王.md "wikilink")》
  - 2015年：[無綫電視](../Page/無綫電視.md "wikilink")《[網絡挑機](../Page/網絡挑機.md "wikilink")》《[Sunday靚聲王](../Page/Sunday靚聲王.md "wikilink")》
  - 2016年：[無綫電視](../Page/無綫電視.md "wikilink")《肥媽開心唱音樂特輯》《[勁歌金曲](../Page/勁歌金曲.md "wikilink")》《[為食3姊妹](../Page/為食3姊妹.md "wikilink")》《[男人食堂](../Page/男人食堂.md "wikilink")》《[娛樂3兄弟](../Page/娛樂3兄弟.md "wikilink")》《[我愛香港](../Page/我愛香港_\(電視節目\).md "wikilink")》
  - 2017年：[無綫電視](../Page/無綫電視.md "wikilink")《[流行經典50年](../Page/流行經典50年.md "wikilink")》
  - [\#後生仔失驚無神賀中秋](../Page/♯後生仔傾吓偈.md "wikilink")

### 演唱會

  - 2009年：**三個女人一個噓**（[葉麗儀](../Page/葉麗儀.md "wikilink")、[陳潔靈](../Page/陳潔靈.md "wikilink")），地點：[香港體育館](../Page/香港體育館.md "wikilink")
  - 2016年：**肥媽友誼長存30週年演唱會**，地點：[香港體育館](../Page/香港體育館.md "wikilink")
  - 2017年：**黑·媽媽笑住唱演唱會2017**（[李麗霞](../Page/李麗霞.md "wikilink")），地點：[伊利沙伯體育館](../Page/伊利沙伯體育館.md "wikilink")

### 廣告

  - [亞洲電視第一](../Page/亞洲電視.md "wikilink")[十大電視廣告頒獎典禮](../Page/十大電視廣告頒獎典禮.md "wikilink")
  - [樂聲牌大電芯廣告](../Page/樂聲牌.md "wikilink")
  - 2006年：Diamond 鑽石健康水香港代言人
  - 變靚D美容醫療中心纖體代言人
  - 2014年：LEDUS 神奇燈膽
  - 2015年：雅窗牌廚具
  - 2015年：三花牌奶粉
  - 2015年：海天堂龜苓膏
  - 2015年：德國寶廚具
  - 2017年：百福豆腐

## 個人著作

  - 2003年7月：《肥媽私房菜1》　
  - 2003年10月：《肥媽私房菜2》　
  - 2004年1月：《肥媽私房菜3》　
  - 2004年7月：《肥媽極速瘦身天書》　
  - 2004年7月：《肥媽私房菜4》　
  - 2005年2月：《肥媽廚藝坊系列1私房滋補湯》　
  - 2005年9月：《肥媽廚藝坊系列2滋味家常菜》
  - 2006年4月：《媽寶食譜》
  - 2006年7月：《肥媽廚房之親子小吃》
  - 2010年7月：《肥媽傳統小吃》（與Ada Wong合著）
  - 2013年7月：《肥媽廚藝坊3煮得慳》
  - 2014年4月：《肥媽廚藝坊4煮得慳2》
  - 2014年7月：《肥媽私房菜精華版》
  - 2015年7月：《肥媽廚藝坊5煮得慳3》

## 獎項

  - [十大中文金曲歌曲獎](../Page/十大中文金曲.md "wikilink")《[友誼之光](../Page/綠島小夜曲.md "wikilink")》
  - [TVB 馬來西亞星光薈萃頒獎典禮2017](../Page/TVB_馬來西亞星光薈萃頒獎典禮2017.md "wikilink")
    最喜愛TVB節目主持

## 家庭生活

瑪俐亞1972年結婚，與前夫育有多名子女，其後於1989年離婚。\[4\]1999年與現時的丈夫Rick結成夫妻。\[5\]\[6\]

長子高進一（）為香港曲棍球代表隊成員，並在[2009年東亞運](../Page/2009年東亞運動會.md "wikilink")[曲棍球賽中協助港隊奪得銅牌](../Page/2009年東亞運動會曲棍球比賽.md "wikilink")\[7\]。

## 爭議

  - 肥媽之政治立場，近年被不少網民批評。
      - 在[2018年11月香港立法會九龍西地方選區補選](../Page/2018年11月香港立法會九龍西地方選區補選.md "wikilink")，她和肥媽、[袁詠儀](../Page/袁詠儀.md "wikilink")、[張智霖和](../Page/張智霖.md "wikilink")[區瑞強為民建聯](../Page/區瑞強.md "wikilink")、經民聯等支持的建制派候選人[陳凱欣站台](../Page/陳凱欣.md "wikilink")。\[8\]
      - 近年肥媽主要為《食平D》節目系作主持，其中對香港社會問題發表不少意見，反比網民批評無知，支持助長社會問題惡化之政黨\[9\]\[10\]，更有網民建議杯葛其代言品牌。\[11\]
  - 於《食平D》節目系列，有網民批評肥媽能夠買到較便宜的食材，是因為街市攤檔主動給予肥媽「友情價」，一般人去不可能如此便宜。肥媽對此作出否認。\[12\]\[13\]

## 參考

<span style="font-size:smaller;">

<div class="references-column">

<references/>

</div>

  - [肥媽正傳
    專輯簡介(簡體中文)](http://tuya.cn/ArticleShow.asp?ArticleID=26)</span>

## 外部連結

  -
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:葡萄牙裔香港人](../Category/葡萄牙裔香港人.md "wikilink")
[Category:土生葡人](../Category/土生葡人.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:美食評論家](../Category/美食評論家.md "wikilink")
[Category:無綫電視女藝員](../Category/無綫電視女藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:前有線電視藝員](../Category/前有線電視藝員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:官立嘉道理爵士中學（西九龍）校友](../Category/官立嘉道理爵士中學（西九龍）校友.md "wikilink")

1.  [Sir Ellis Kadoorie Secondary School (West Kowloon)
    Parents-teachers’Association *Newsletter* Issue No.
    18，2010年7月](http://www.seksswk.edu.hk/doc/Newsletter%20No18.pdf)
2.
3.
4.  [女人唔易做瑪利亞](http://www.xpweekly.com/share/xpweekly/0326/ent/20041116ent0054/content.htm)
    2004-11-16
5.  [肥媽離婚怪自己
    為朋友孭七百萬債](http://nextplus.nextmedia.com/news/ent/20160207/349236)
    2016-02-07 壹周刊
6.  [肥媽重提婚變：以為自己係好老婆](http://hk.on.cc/hk/bkn/cnt/entertainment/20160207/bkn-20160207140049386-0207_00862_001.html)
    2016-02-07 東方日報
7.
8.
9.  <https://www.facebook.com/civicshiphk/photos/a.2215745281982118/2255412954682017/?type=3&theater>
10. <https://www.facebook.com/civicshiphk/photos/a.2215745281982118/2215745231982123/?type=3&theater>
11. <https://www.facebook.com/HKFutureConcern/photos/a.516576328679714/814938555510155/?type=3&theater>
12. <http://www.ihktv.com/sudden-928-maria-cordero.html>
13. <http://www.youtube.com/watch?v=vxYPu2cBXmQ>