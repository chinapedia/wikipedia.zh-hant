**布里斯科縣**（**Briscoe County,
Texas**）位[美國](../Page/美國.md "wikilink")[德克薩斯州](../Page/德克薩斯州.md "wikilink")[北部狹地的一個縣](../Page/北部狹地.md "wikilink")。面積2,335平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口1,790人。縣治[錫爾弗頓](../Page/錫爾弗頓_\(德克薩斯州\).md "wikilink")（Silverton）。

成立於1876年8月21日，縣政府成立於1892年1月11日。縣名紀念任[德克薩斯獨立宣言簽署者之一](../Page/德克薩斯獨立宣言.md "wikilink")[安德魯·布里斯科](../Page/安德魯·布里斯科.md "wikilink")（Andrew
Briscoe）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[B](../Category/得克萨斯州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.