****是历史上第四十九次航天飞机任务，也是[亚特兰蒂斯号航天飞机的第十二次太空飞行](../Page/亞特蘭提斯號太空梭.md "wikilink")。

## 任务成员

  - **[罗伦·施里弗](../Page/罗伦·施里弗.md "wikilink")**（，曾执行、以及任务），指令长
  - **[安德鲁·艾伦](../Page/安德鲁·艾伦.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[杰弗利·霍夫曼](../Page/杰弗利·霍夫曼.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[张福林](../Page/張福林.md "wikilink")**（，曾执行、、、、、以及任务），任务专家
  - **[克劳德·尼克列](../Page/克劳德·尼克列.md "wikilink")**（，[瑞士宇航员](../Page/瑞士.md "wikilink")，曾执行、、以及任务），任务专家
  - **[玛莎·埃文斯](../Page/玛莎·埃文斯.md "wikilink")**（，曾执行、、、以及任务），有效载荷专家
  - **[弗兰科·马勒巴](../Page/弗兰科·马勒巴.md "wikilink")**（，[意大利宇航员](../Page/意大利.md "wikilink")，曾执行任务），有效载荷专家

[Category:航天飞机任务](../Category/航天飞机任务.md "wikilink")
[Category:1992年佛罗里达州](../Category/1992年佛罗里达州.md "wikilink")
[Category:1992年科技](../Category/1992年科技.md "wikilink")
[Category:1992年7月](../Category/1992年7月.md "wikilink")
[Category:1992年8月](../Category/1992年8月.md "wikilink")