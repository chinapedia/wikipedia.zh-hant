**75**是[74与](../Page/74.md "wikilink")[76之间的](../Page/76.md "wikilink")[自然数](../Page/自然数.md "wikilink")。

## 在数学中

  - 頭5個[五角數之和](../Page/五角數.md "wikilink")：1+5+12+22+35=75，因此75也是[五角錐數](../Page/五角錐數.md "wikilink")。
  - [48和](../Page/48.md "wikilink")75組成最小的一對[婚約數](../Page/婚約數.md "wikilink")
  - 第5個[九邊形數](../Page/九邊形數.md "wikilink")。

## 在人类文化中

### 人名

  - [七十五](../Page/七十五_\(将军\).md "wikilink")，清朝將軍。
  - 七十五，清朝官員，曾任福建道御史、掌福建道監察御史。

## 在科学中

  - [錸的](../Page/錸.md "wikilink")[原子序數](../Page/原子序數.md "wikilink")\[1\]。
  - [哈雷彗星公轉周期約為](../Page/哈雷彗星.md "wikilink")75.3年\[2\]，每隔75到76年能從地球上看見\[3\]。

## 在其它领域中

  - [城巴75線](../Page/城巴75線.md "wikilink")
  - [北75線](../Page/北75線.md "wikilink")

## 参考文献

1.
2.
3.