**2007年亞洲足協盃**是[亞洲足協舉辦的第四屆](../Page/亞洲足協.md "wikilink")[亞洲足協盃](../Page/亞洲足協盃.md "wikilink")，賽事共有來自
14 個[亞洲國家的](../Page/亞洲.md "wikilink") 24 間足球會參加。

## 參賽球會

13
個「發展中國家」亞洲國家的應屆本土聯賽冠軍及盃賽會獲得[亞洲足協邀請](../Page/亞洲足協.md "wikilink")2007年亞洲足協盃。上屆冠軍約旦球會[阿爾費薩里亦會自動成為參賽球隊之一](../Page/阿爾費薩里安曼體育會.md "wikilink")。由於澳洲加入亞洲足協，並且澳洲球會直接參加[亞洲聯賽冠軍盃的關係](../Page/亞洲聯賽冠軍盃.md "wikilink")，原本參加亞冠盃的[越南及](../Page/越南足球總會.md "wikilink")[泰國球會因而降格參加本屆賽事](../Page/泰國足球協會.md "wikilink")，[孟加拉球會於本屆賽事退出參賽](../Page/孟加拉.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><p><strong>西亞</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴林足球協會.md" title="wikilink">巴林</a> (1)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/約旦足球協會.md" title="wikilink">約旦</a> (3)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎巴嫩足球總會.md" title="wikilink">黎巴嫩</a> (2)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿曼足球協會.md" title="wikilink">阿曼</a> (2)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/也門足球協會.md" title="wikilink">也門</a> (2)</p></td>
</tr>
<tr class="odd">
<td><p><strong>中亞</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/土庫曼足球協會.md" title="wikilink">土庫曼</a> (2)</p></td>
</tr>
<tr class="odd">
<td><p><strong>東亞</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香港足球總會.md" title="wikilink">香港</a> (2)</p></td>
</tr>
<tr class="odd">
<td><p><strong>南亞</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孟加拉足球總會.md" title="wikilink">孟加拉</a> (0)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/全印度足球協會.md" title="wikilink">印度</a> (2)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬爾代夫足球協會.md" title="wikilink">馬爾代夫</a> (2)</p></td>
</tr>
<tr class="odd">
<td><p><strong>東南亞</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬來西亞足球協會.md" title="wikilink">馬來西亞</a> (2)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新加坡足球協會.md" title="wikilink">新加坡</a> (2)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/泰國足球協會.md" title="wikilink">泰國</a> (1)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/越南足球總會.md" title="wikilink">越南</a> (1)</p></td>
</tr>
</tbody>
</table>

抽籤儀式於2006年12月22日在[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡進行](../Page/吉隆坡.md "wikilink")。\[1\]

## 賽事

**分數榜中的顏色代表：**

  - ***綠色***代表以首名晉身八強。
  - ***藍色***代表以最佳次名晉身八強。
  - ***紅色***代表出局。

### 小組賽

#### A組

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>分數</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>進球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/內澤默體育會.md" title="wikilink">阿爾內澤默</a></p></td>
<td><p><strong>15</strong></p></td>
<td><p>6</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>8</p></td>
<td><p>5</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沙巴阿羅登足球會.md" title="wikilink">沙巴阿羅登</a></p></td>
<td><p><strong>13</strong></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>8</p></td>
<td><p>3</p></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬斯喀特足球會.md" title="wikilink">馬斯喀特</a></p></td>
<td><p><strong>4</strong></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>6</p></td>
<td><p>−2</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/艾沙克體育會.md" title="wikilink">艾沙克</a></p></td>
<td><p><strong>2</strong></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
<td><p>11</p></td>
<td><p>−6</p></td>
</tr>
</tbody>
</table>

#### B組

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>分數</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>進球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/艾華達體育會.md" title="wikilink">艾華達</a></p></td>
<td><p><strong>14</strong></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>16</p></td>
<td><p>8</p></td>
<td><p>8</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿爾慕哈瑞克體育會.md" title="wikilink">慕哈瑞克</a></p></td>
<td><p><strong>11</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>10</p></td>
<td><p>7</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/耶迪根足球隊.md" title="wikilink">HTTU阿什哈巴德</a></p></td>
<td><p><strong>6</strong></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>10</p></td>
<td><p>12</p></td>
<td><p>−2</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/艾喜拉艾沙希利足球俱樂部.md" title="wikilink">艾喜拉</a></p></td>
<td><p><strong>2</strong></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>6</p></td>
<td><p>15</p></td>
<td><p>−9</p></td>
</tr>
</tbody>
</table>

#### C組

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>分數</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>進球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿爾費薩里安曼體育會.md" title="wikilink">阿爾費薩里</a></p></td>
<td><p><strong>11</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>7</p></td>
<td><p>3</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/迪赫弗體育文化及社區足球會.md" title="wikilink">迪赫弗</a></p></td>
<td><p><strong>10</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
<td><p>−1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿爾安薩爾體育會.md" title="wikilink">安薩爾</a></p></td>
<td><p><strong>9</strong></p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>8</p></td>
<td><p>5</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴爾幹足球會.md" title="wikilink">內比治</a></p></td>
<td><p><strong>2</strong></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>8</p></td>
<td><p>−6</p></td>
</tr>
</tbody>
</table>

#### D組

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>分數</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>進球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/晨曦體育會.md" title="wikilink">晨曦</a></p></td>
<td><p><strong>15</strong></p></td>
<td><p>6</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>15</p></td>
<td><p>6</p></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/森美蘭足球會.md" title="wikilink">森美蘭</a></p></td>
<td><p><strong>7</strong></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
<td><p>−1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/勝利競技俱樂部.md" title="wikilink">勝利競技</a></p></td>
<td><p><strong>6</strong></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
<td><p>9</p></td>
<td><p>−2</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/河北河內足球俱樂部.md" title="wikilink">河北河內</a></p></td>
<td><p><strong>3</strong></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>7</p></td>
<td><p>13</p></td>
<td><p>−6</p></td>
</tr>
</tbody>
</table>

*The match between [勝利競技](../Page/勝利競技俱樂部.md "wikilink") and
[晨曦](../Page/晨曦體育會.md "wikilink") was postponed to May 9 because the
assistant referees from [Indonesia](../Page/Indonesia.md "wikilink")
were unable to arrive [Maldives](../Page/Maldives.md "wikilink") on
time.*\[2\]\[3\]

#### E組

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>分數</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>進球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/新加坡武裝部隊足球俱樂部.md" title="wikilink">新加坡武裝部隊</a></p></td>
<td><p><strong>15</strong></p></td>
<td><p>6</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>13</p></td>
<td><p>7</p></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬辛德拉聯足球會.md" title="wikilink">馬辛德拉聯</a></p></td>
<td><p><strong>12</strong></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>9</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愉園體育會.md" title="wikilink">愉園</a></p></td>
<td><p><strong>9</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>9</p></td>
<td><p>11</p></td>
<td><p>−2</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/新拉迪安特體育俱樂部.md" title="wikilink">拉迪恩</a></p></td>
<td><p><strong>0</strong></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>13</p></td>
<td><p>−9</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### F組

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>分數</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>進球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/淡濱尼流浪足球會.md" title="wikilink">淡濱尼流浪</a></p></td>
<td><p><strong>13</strong></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>10</p></td>
<td><p>5</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫亨巴根體育會.md" title="wikilink">莫亨巴根</a></p></td>
<td><p><strong>11</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奧斯特斯帕足球俱樂部.md" title="wikilink">奧斯特斯帕</a></p></td>
<td><p><strong>10</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>12</p></td>
<td><p>3</p></td>
<td><p>9</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彭亨足球會.md" title="wikilink">彭亨</a></p></td>
<td><p><strong>0</strong></p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>6</p></td>
<td><p>2</p></td>
<td><p>18</p></td>
<td><p>−14</p></td>
</tr>
</tbody>
</table>

#### 最佳次名

有兩個最佳次名可以晉身八強，一個來自A、B、C組，另一個則來自D、E、F組。

  - A、B、C組（中亞、西亞區）

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>分數</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>進球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/沙巴阿羅登足球會.md" title="wikilink">沙巴阿羅登</a></p></td>
<td><p><strong>13</strong></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>8</p></td>
<td><p>3</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿爾慕哈瑞克體育會.md" title="wikilink">慕哈瑞克</a></p></td>
<td><p><strong>11</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>10</p></td>
<td><p>7</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/迪赫弗體育文化及社區足球會.md" title="wikilink">迪赫弗</a></p></td>
<td><p><strong>10</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
<td><p>−1</p></td>
</tr>
</tbody>
</table>

  - D、E、F組（東亞、東南亞區）

<table>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>分數</p></th>
<th><p>賽</p></th>
<th><p>勝</p></th>
<th><p>和</p></th>
<th><p>負</p></th>
<th><p>進球</p></th>
<th><p>失球</p></th>
<th><p>球差</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/馬辛德拉聯足球會.md" title="wikilink">馬辛德拉聯</a></p></td>
<td><p><strong>12</strong></p></td>
<td><p>6</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>9</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫亨巴根體育會.md" title="wikilink">莫亨巴根</a></p></td>
<td><p><strong>11</strong></p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>5</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/森美蘭足球會.md" title="wikilink">森美蘭</a></p></td>
<td><p><strong>7</strong></p></td>
<td><p>6</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>5</p></td>
<td><p>−1</p></td>
</tr>
</tbody>
</table>

### 淘汰賽

#### 半準決賽

|}

  - 首回合

<!-- end list -->

  - 次回合

#### 準決賽

|}

  - 首回合

<!-- end list -->

  - 次回合

#### 決賽

|}

  - 首回合

<!-- end list -->

  - 次回合

<table>
<thead>
<tr class="header">
<th><p>2007年<br />
亞洲足協盃<br />
冠軍</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Shabab_Al_Ordon.jpg" title="fig:Shabab_Al_Ordon.jpg">Shabab_Al_Ordon.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/沙巴阿羅登足球會.md" title="wikilink">沙巴阿羅登</a>（, Shabab Al Ordon）</strong><br />
<strong>第一次奪冠</strong></p></td>
</tr>
</tbody>
</table>

## 參考

<references />

## 參見

  - [2007年亞洲足球冠軍聯賽](../Page/2007年亞洲足球冠軍聯賽.md "wikilink")

## 外部連結

  - [官方網站](https://web.archive.org/web/20070921065725/http://www.the-afc.com/english/competitions/afcCup/default.asp?hittype=banner)
  - [AFC 2007年日程表 Calendar of
    Events 2007](https://web.archive.org/web/20070930235432/http://www.the-afc.com/english/calendarevent/pdf/AFC_Comps_calendar_2007.pdf)

[Category:2007年足球](../Category/2007年足球.md "wikilink")
[Category:亞洲足協盃](../Category/亞洲足協盃.md "wikilink")

1.   2007年亞洲足協盃抽籤儀式。
2.  "[AFC Cup: Victory, Sun Hei match
    postponed](http://www.the-afc.com/english/competitions/afcCup/2007/news/default.asp?action=newsDetails&newsID=8554)
    ". Retrieved on May 8, 2007.
3.  "[Victory, Sun Hei match
    postponed](http://www.victory.mv/Victory/news/080507/default.html)
    ", *[The Official Website of Victory SC](http://www.victory.mv/) *,
    May 8, 2007. Retrieved on May 10, 2007.