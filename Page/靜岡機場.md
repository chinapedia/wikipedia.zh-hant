**靜岡機場**（，），位處[靜岡縣](../Page/靜岡縣.md "wikilink")[島田市與](../Page/島田市.md "wikilink")[牧之原市之間](../Page/牧之原市.md "wikilink")，於2009年6月4日啟用，由靜岡縣與縣內企業共同出資成立的富士山靜岡機場股份有限公司負責機場的營運管理。

根據[日本機場分類法](../Page/日本機場列表.md "wikilink")，靜岡機場劃分為第三類機場，設有一條跑道以及5個停機位，每日上午八時至下午八時運作。[靜岡縣政府曾預期每年約有一百四十萬旅客使用靜岡機場](../Page/靜岡縣.md "wikilink")\[1\]。然而機場啟用至今，據統計2017-2018年度只有約67萬人次旅客曾經使用\[2\]。

靜岡機場現有5家航空公司開設定期客運航班，來往[福岡](../Page/福岡.md "wikilink")、[那霸](../Page/那霸.md "wikilink")、[新千歲](../Page/新千歲.md "wikilink")、[上海](../Page/上海.md "wikilink")、[台北和](../Page/台北.md "wikilink")[首爾](../Page/首爾.md "wikilink")，當中包括以靜岡機場作[樞紐的](../Page/樞紐機場.md "wikilink")[富士夢幻航空](../Page/富士夢幻航空.md "wikilink")。此外，亦有包機來往[高雄](../Page/高雄.md "wikilink")、[嘉義](../Page/嘉義.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門及](../Page/澳門.md "wikilink")[天津等地](../Page/天津.md "wikilink")。

## 歷史

## 客運設施

靜岡機場設有一個客運大樓，建築面積達71萬平方米。設施如下：

  - G/F - 巴士總站、的士站、旅客登記櫃位、登記/接機大堂、詢問處、便利店、警局
  - 1/F -
    離境大堂、便利店、商店、免税店（位於國際航缐候機室内）、[靜岡縣資訊中心](../Page/靜岡縣.md "wikilink")
  - 2/F - 觀景廊、食堂

另一方面，靜岡機場亦設有一個貨運站。

## 交通配套

  - 巴士
      - [靜鐵Justline](../Page/靜鐵Justline.md "wikilink")（）
          - 靜岡機場 - [靜岡站](../Page/靜岡站.md "wikilink")
          - 靜岡機場 - [島田站](../Page/島田車站_\(靜岡縣\).md "wikilink")
          - 靜岡機場 - [菊川站](../Page/菊川車站_\(靜岡縣\).md "wikilink") -
            [掛川站](../Page/掛川站.md "wikilink")
      - [遠鐵巴士](../Page/遠鐵巴士.md "wikilink")（）
          - 靜岡機場 - [掛川站](../Page/掛川站.md "wikilink") -
            [東名高速公路濱松西交匯處](../Page/東名高速公路濱松西交匯處.md "wikilink")（）
              - 途經：[濱松站](../Page/濱松站.md "wikilink")、[中部國際機場](../Page/中部國際機場.md "wikilink")、[奥濱名湖](../Page/奥濱名湖.md "wikilink")（）、[館山寺温泉](../Page/館山寺温泉.md "wikilink")（）

<!-- end list -->

  -   - [富士急行有限公司](../Page/富士急行.md "wikilink")
          - 靜岡機場・[静岡站南口](../Page/靜岡站.md "wikilink") -
            河口湖・[富士急遊樂場](../Page/富士急遊樂場.md "wikilink")
              - 靜岡機場 -
                [静岡站南口](../Page/靜岡站.md "wikilink")、[新富士站](../Page/新富士車站_\(靜岡縣\).md "wikilink")、[富士宮站](../Page/富士宮站.md "wikilink")、[朝霧高原](../Page/朝霧高原.md "wikilink")、[河口湖站](../Page/河口湖站.md "wikilink")

<!-- end list -->

  - 鐵路
      - 最近的火車站是[東海道本線與](../Page/東海道本線.md "wikilink")[大井川鐵道的](../Page/大井川鐵道.md "wikilink")[金谷站](../Page/金谷站.md "wikilink")（直線距離6公里）及東海道新幹線的[掛川車站](../Page/掛川車站.md "wikilink")（約14公里）。亦有巴士連接附近的火車站。事實上，[東海道新幹線於機場下方隧道穿越](../Page/東海道新幹線.md "wikilink")\[3\]，因此地方有要求設站的呼聲\[4\]，然而[JR東海已多次表示並無計劃於靜岡機場建站](../Page/JR東海.md "wikilink")\[5\]。

<!-- end list -->

  - 的士
      - 從[靜岡站出發](../Page/靜岡站.md "wikilink")，或途經該站，劃一收費8500日元。

<!-- end list -->

  - 停車場

靜岡機場提供5個停車場，合共2200個免費停車位。

## 航空公司與航點

[FDA_Embraer_170_JA02FJ_RJNS.jpg](https://zh.wikipedia.org/wiki/File:FDA_Embraer_170_JA02FJ_RJNS.jpg "fig:FDA_Embraer_170_JA02FJ_RJNS.jpg")客机滑行中\]\]
 **使用代碼共享飛航航空公司**

<table>
<thead>
<tr class="header">
<th><p>航空公司</p></th>
<th><p><a href="../Page/代碼共享.md" title="wikilink">代碼共享航空公司</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/日本航空.md" title="wikilink">日本航空</a></p></td>
<td><p><a href="../Page/富士夢幻航空.md" title="wikilink">富士夢幻航空</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日本航空.md" title="wikilink">日本航空</a></p></td>
<td><p><a href="../Page/中國東方航空.md" title="wikilink">中國東方航空</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/日本航空.md" title="wikilink">日本航空</a></p></td>
<td><p><a href="../Page/中華航空.md" title="wikilink">中華航空</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/荷蘭皇家航空.md" title="wikilink">荷蘭皇家航空</a></p></td>
<td><p><a href="../Page/中華航空.md" title="wikilink">中華航空</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/韓亞航空.md" title="wikilink">韓亞航空</a></p></td>
<td><p><a href="../Page/首爾航空.md" title="wikilink">首爾航空</a></p></td>
</tr>
</tbody>
</table>

## 相關條目

  - [嘉義機場](../Page/嘉義機場.md "wikilink")

## 參考來源

  -
## 外部連結

  -
  - [靜岡機場官方網站](http://www.mtfuji-shizuokaairport.jp/)

{{-}}

[Category:中部地方機場](../Category/中部地方機場.md "wikilink")
[Category:靜岡縣交通](../Category/靜岡縣交通.md "wikilink")
[Category:2009年啟用的機場](../Category/2009年啟用的機場.md "wikilink")
[Category:牧之原市](../Category/牧之原市.md "wikilink")
[Category:島田市](../Category/島田市.md "wikilink")

1.
2.  [靜岡機場旅客統計
    ](http://www2.pref.shizuoka.jp/all/kisha18.nsf/9f1acbc926670f74492568010000c977/895bebbdca578f6f49258265001e71fe?OpenDocument)
3.  [東海道新幹線路線圖](http://www.mtfuji-shizuokaairport.jp/english/access/car_motorcycle/index.html)

4.  [静岡新聞8月13日](http://www.shizushin.com/news/pol_eco/shizuoka/20090813000000000047.htm)
5.