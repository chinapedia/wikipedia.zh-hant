在[俄语中](../Page/俄语.md "wikilink")，[动词用来表示事物的行为或状态](../Page/动词.md "wikilink")。**俄语动词**按人称和数的变化而变位，动词的原始形式称作动词不定式。

俄语动词变化的部分称作动词人称词尾。

根据动词人称词尾变化规律的不同，可以分为第一变位法和第二变位法。

## 第一变位法

第一变位法的动词多以**-ать**，**-ять**结尾，变位时去掉**ть**，加上人称词尾。另外，所有以**-ти**，**-чь**结尾的动词都是第一变位法。

大部分不定式以－ать, -ять 结尾的动词及所有的以–ти, -чь结尾的动词属于第一变位法。第一变位法动词变位时，先去掉不定式中的-ть,
-ти, -чь, 再在元音后加入词尾-ю, -ешь, -ет, -ем, -ете, -ют（如работать）或在辅音后加入词尾-у,
-ешь, -ет, -ем, -ете, -ут.（如重音在词尾，则为–у, -ёшь, -ёт, -ём, -ёте,
-ут，（如идти）。

`    читать: читаю, читаешь, читает, читаем, читаете, читают`

`    работать: работаю, работаешь, работает, работаем, работаете, работают`

`    идти: иду, идёшь, идёт, идём, идёте, идут`

<table>
<tbody>
<tr class="odd">
<td><p><strong>数</strong></p></td>
<td><p><strong>人称</strong></p></td>
<td><p><strong>词尾</strong></p></td>
<td><p><strong>例子</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>Читать</strong></p></td>
<td><p><strong>Вернуть</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>单数</strong></p></td>
<td><p>第一人称（я）<br />
第二人称（ты）<br />
第三人称（он，она，оно）</p></td>
<td><p>（元音字母）<strong>-ю</strong>，（辅音字母）<strong>-у</strong><br />
<strong>-ешь</strong>(<strong>-ëшь</strong>)<br />
<strong>-ет</strong>(<strong>-ëт</strong>)</p></td>
<td><p>чита<strong>ю</strong><br />
чита<strong>ешь</strong><br />
чита<strong>ет</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>复数</strong></p></td>
<td><p>第一人称（Мы）<br />
第二人称（Вы）<br />
第三人称（они）</p></td>
<td><p><strong>-ем</strong>(<strong>-ëм</strong>)<br />
<strong>-ете</strong>(<strong>-ёте</strong>)<br />
（元音字母）<strong>-ют</strong>，（辅音字母）<strong>-ут</strong></p></td>
<td><p>чита<strong>ем</strong><br />
чита<strong>ете</strong><br />
чита<strong>ют</strong></p></td>
</tr>
</tbody>
</table>

`注：(1)带-ся的动词的变位与不带–ся的动词相同，只是–ся在元音后改为–сь.如： заниматься: занимаюсь, занимаешься, занимается,`

занимаемся, занимаетесь, занимаются

учиться: учусь, учишься, учится, учимся, учитесь, учатся

`     (2)有些动词的变位与一般动词变位有所不同，如：`

писать: пишу, пишешь, пишет, пишем, пишете, пишут

`     вставать: встаю, встаёшь, встаёт, встаём, встаёте, встают`

`     жить: живу, живёшь, живёт, живём, живёте, живут`

звать: зову, зовёшь, зовёт, зовём, зовёте, зовут

（3）去掉不定式中的-ть, -ти, -чь后单词的最后一个字母是元音字母，单数第一人称加词尾-ю,
复数第三人称加-ют（如работать）；如果是辅音字母则加词尾-у,
复数第三人称则是 –ут，如重音在词尾，则为–у, -ёшь, -ёт, -ём, -ёте, -ут（如идти）。

## 第二变位法

第二变位法的动词多以**-ить**结尾，变位时去掉**-ить**，加上人称词尾。

第二变位法包括所有不定式以-ить结尾的动词以及少数不定式以–ать, -есть结尾的动词，如：лежать, смотреть。

第二变位法变位时除了去掉ть外，还要去掉其前面的元音字母，然后再加第二变位法的人称词尾。去掉ть及其前面的元音字母后，

如果最后一个字母是ж, ш, ч, ц时，第一人称词尾加-у，复数第三人称加-ат。

<table>
<tbody>
<tr class="odd">
<td><p><strong>数</strong></p></td>
<td><p><strong>人称</strong></p></td>
<td><p><strong>词尾</strong></p></td>
<td><p><strong>例子</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>Говорить</strong></p></td>
<td><p><strong>Лежать</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>单数</strong></p></td>
<td><p>第一人称（я）<br />
第二人称（ты）<br />
第三人称（он，она，оно）</p></td>
<td><p><strong>-ю</strong>，（<a href="../Page/唏音.md" title="wikilink">唏音</a>）<strong>-у</strong><br />
<strong>-ишь</strong><br />
<strong>-ит</strong></p></td>
<td><p>говор<strong>ю</strong><br />
говор<strong>ишь</strong><br />
говор<strong>ит</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>复数</strong></p></td>
<td><p>第一人称（Мы）<br />
第二人称（Вы）<br />
第三人称（они）</p></td>
<td><p><strong>-им</strong><br />
<strong>-ите</strong><br />
（元音字母）<strong>-ят</strong>，（唏音）<strong>-ат</strong></p></td>
<td><p>говор<strong>им</strong><br />
говор<strong>ите</strong><br />
говор<strong>ят</strong></p></td>
</tr>
</tbody>
</table>

## 第一变位和第二变位法不同之处

4．第二变位法动词与第一变位法动词的不同处为：

` 1）动词的原型形式大多不同。`

2）变位时候去掉不定式中的-ть外，还要去掉其前面的一个元音字母。

3）人称词尾不同。

4）动词变位有时伴有音的变化（音的交替），这种变化在第一变位法中发生在所有人称形式，而在第二变位法动词中只发生在单数第一人称形式。如：

`      писать: пишу, пишешь, пишет, пишем, пишете, пишут`

`      сидеть: сижу, сидишь, сидит, сидим, сидите, сидят`

`      любить: люблю, любишь, любит, любим, любите, любят`

动词发生音变时某些音变规则一般是：

с—ш, г, д, з—ж, т—ч, ск, ст—щ, б, п, в, ф, м—бл, пл, вл, фл, мл...

например: любить, готовить, сидеть...; писать, сказать...

带**-ся**动词的变位和不带**-ся**动词的变位相同，只是**-ся**在元音后变为**-сь**，在辅音之后则仍为**-ся**。

## 变位时的音变

一些动词变位时，在人称词尾前的辅音会发生音变。其规律如下：

|               |                    |
| ------------- | ------------------ |
| **г，д，з**     | **ж**              |
| **к，т**       | **ч**              |
| **с，х**       | **ш**              |
| **ск，т，ст**   | **щ**              |
| **б，в，м，п，ф** | **бл，вл，мл，пл，фл** |

第一变位法的动词每个人称形式都发生音变，第二变位法的动词则只有单数第一人称发生音变。

## 参看

  - [俄语词类](../Page/俄语词类.md "wikilink")
  - [俄语语法](../Page/俄语语法.md "wikilink")
  - [俄语正寫法](../Page/俄语正寫法.md "wikilink")

## 參考文獻

  -
  -
  - [О структуре русского
    глагола](http://www.philology.ru/linguistics2/jakobson-85c.htm)

[category:俄语](../Page/category:俄语.md "wikilink")