[para_riemann.png](https://zh.wikipedia.org/wiki/File:para_riemann.png "fig:para_riemann.png")

微分幾何中，**黎曼幾何**（英語：Riemannian
geometry）研究具有[黎曼度量的光滑](../Page/黎曼度量.md "wikilink")[流形](../Page/流形.md "wikilink")，即流形切空間上二次形式的選擇。它特別關注于角度、弧線長度及體積。把每个微小部分加起來而得出整體的數量。

19世紀，[波恩哈德·黎曼把這個概念加以推广](../Page/波恩哈德·黎曼.md "wikilink")。

任意平滑流形容許[黎曼度量及這個額外結構幫助解決微分拓扑問題](../Page/黎曼度量.md "wikilink")。它成為[伪黎曼流形複雜結構的入門](../Page/伪黎曼流形.md "wikilink")。其中大部分都是[廣義相對論的四維研究对象](../Page/廣義相對論.md "wikilink")。

**黎曼幾何**与以下主題有关：

1.  [度量張量](../Page/度量張量.md "wikilink")
2.  [黎曼流形](../Page/黎曼流形.md "wikilink")
3.  [列维-奇维塔联络](../Page/列维-奇维塔联络.md "wikilink")
4.  [曲率](../Page/曲率.md "wikilink")
5.  [曲率張量](../Page/曲率張量.md "wikilink")

参看：

1.  [微分幾何主題列表](../Page/微分幾何主題列表.md "wikilink")
2.  [黎曼及度量幾何詞表](../Page/黎曼及度量幾何詞表.md "wikilink")

## 黎曼几何古典理論

[Georg_Friedrich_Bernhard_Riemann.jpeg](https://zh.wikipedia.org/wiki/File:Georg_Friedrich_Bernhard_Riemann.jpeg "fig:Georg_Friedrich_Bernhard_Riemann.jpeg")\]\]

下面给出部分的黎曼几何古典理論。

### 一般理論

1.  **[高斯-博内定理](../Page/高斯-博内定理.md "wikilink")**：紧致二維[黎曼流形上](../Page/黎曼流形.md "wikilink")[高斯曲率的积分等於](../Page/高斯曲率.md "wikilink")\(2\pi\chi(M)\)這裡的\(\chi(M)\)記作*M*的[欧拉示性数](../Page/欧拉示性数.md "wikilink")。
2.  **[纳什嵌入定理](../Page/纳什嵌入定理.md "wikilink")**（两个）被稱為[黎曼幾何的基礎理論](../Page/黎曼幾何.md "wikilink")。他們表明每個[黎曼流形可以是嵌入](../Page/黎曼流形.md "wikilink")[歐幾里得空間](../Page/歐幾里得空間.md "wikilink")**R**<sup>*n*</sup>.

### 理論

所有给出的定理中，都将用用空间的局部行为（通常用曲率假设表述）来推出空间的整体结构的一些信息，包括流形的拓扑类型和"足够大"距离的点间的关系。

#### 受限[截面曲率](../Page/截面曲率.md "wikilink")

1.  **1/4-受限 球定理.**若*M*是完备*n*-维黎曼流形，其截面曲率严格限制于1和4之间，则*M*同胚于*n*-球。

<!-- end list -->

1.  **Cheeger's有限定理.**给定常数*C*和*D*，只有有限个（微分同胚的流形算作一个）紧*n*-维黎曼流形，其截面曲率\(|K|\le C\)并且直径\(\le D\)。

<!-- end list -->

1.  **[Gromov的几乎平坦流形](../Page/几乎平坦流形.md "wikilink").**存在一个\(\epsilon_n>0\)使得如果一个*n*-维黎曼流形其度量的截面曲率\(|K|\le \epsilon_n\)且直径\(\le 1\)，则其有限覆盖微分同胚于一个[零流形](../Page/Glossary_of_Riemannian_and_metric_geometry.md "wikilink").

#### 正曲率

##### 正[截面曲率](../Page/截面曲率.md "wikilink")

1.  **[灵魂定理](../Page/灵魂定理.md "wikilink")**若*M*是一个不紧的完备正曲率*n*-维黎曼流形，则它微分同胚于**R**<sup>n</sup>.
2.  **Gromov的贝蒂数定理**有一个常数*C=C(n)*使得若*M*是一个由正截面曲率的紧连通*n*-维黎曼流形，则它的[贝蒂数之和不超过](../Page/贝蒂数.md "wikilink")*C*.

##### 正[里奇曲率](../Page/里奇曲率.md "wikilink")

1.  **[Myers定理](../Page/Myers定理.md "wikilink").**若一个紧黎曼流形有正Ricci曲率则它的[基本群有限](../Page/基本群.md "wikilink")。

<!-- end list -->

1.  **[分裂定理](../Page/分裂定理.md "wikilink").**若一个完备的*n*-维黎曼流形有非负Ricci曲率和一条直线（在任何区间上的距离都极小的测地线）则它等度同胚于一条实直线和一个有非负Ricci曲率的完备(*n*-1)-维黎曼流形的直积。

<!-- end list -->

1.  **[Bishop's不等式](../Page/Bishop-Gromov不等式.md "wikilink").**半径为*r*的球在一个有正Ricci曲率的完备*n*-维黎曼流形中的体积不超过欧几里得空间中同样半径的球的体积。

<!-- end list -->

1.  **[Gromov's紧致性定理](../Page/Gromov's紧致性定理.md "wikilink").**所有正Ricci曲率且直径不超过*D*的黎曼流形在[Gromov-Hausdorff度量下是](../Page/Gromov-Hausdorff度量.md "wikilink")[仿紧的](../Page/度量空间.md "wikilink")。

##### [数量曲率](../Page/数量曲率.md "wikilink")

1.  *n*-维环不存在有正数量曲率的度量。

<!-- end list -->

1.  若一个紧*n*-维黎曼流形的单射半径\(\ge \pi\)，则数量曲率的平均值不超过*n*（*n*-1）。

#### 負曲率

##### 負[截面曲率](../Page/截面曲率.md "wikilink")

1.  任何有非正截面曲率的单连通黎曼流形的两点有唯一的测地线连接。

<!-- end list -->

1.  若*M*是一个有负截面曲率的完备黎曼流形，则[基本群的任何](../Page/基本群.md "wikilink")[可交换子群同构于整数群](../Page/可交换.md "wikilink")**Z**。

<!-- end list -->

1.  设V<sup>\*</sup>是一\(\mathbb{R}\)-rank\(\geq\)2的紧致不可约局部对称空间，设V是一截面曲率\(K\leq 0\)的紧致\(C^{\infty}\)黎曼流形，若\(vol(V)=vol(V^*)\)，且\(\pi_1(V)=\pi_1(V^*)\)，则\(V\)与\(V^*\)等距。

##### 負[里奇曲率](../Page/里奇曲率.md "wikilink")

1.  任何有负里奇曲率的紧黎曼流形有一个离散的[等距同胚群](../Page/等距同胚群.md "wikilink")。
2.  任何光滑流形可以加入有负里奇曲率的黎曼度量。

## 參考文献

  - Marcel Berger, *Riemannian Geometry During the Second Half of the
    Twentieth Century*, (2000) University Lecture Series vol. 17,
    American Mathematical Society, Rhode Island, ISBN 0-8218-2052-4.
    *(Provides a historical review and survey, including hundreds of
    references.)*
  - Jurgen Jost, *Riemannian Geometry and Geometric Analysis*, (2002)
    Springer-Verlag, Berlin ISBN 3-540-4267-2 *(Provides a formal
    introduction, written at the grad-student level.)*
  - Peter Peterson, *Riemannian Geometry*, (1998) Springer-Verlag,
    Berlin ISBN 0-387-98212-4. *(Provides an introduction, presented at
    an undergrad level.)*

{{-}}

[黎曼几何](../Category/黎曼几何.md "wikilink")