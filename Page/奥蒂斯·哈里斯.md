**奥蒂斯·哈里斯**（，），生于[密西西比州爱德华兹](../Page/密西西比州.md "wikilink")，[美国](../Page/美国.md "wikilink")[田径运动员](../Page/田径.md "wikilink")。他曾获2004年夏季奥林匹克运动会男子400米银牌和男子4×400米接力金牌。

## 參考資料

## 外部連結

  -
[Category:非洲裔美國田徑運動員](../Category/非洲裔美國田徑運動員.md "wikilink")
[Category:美國奧林匹克運動會金牌得主](../Category/美國奧林匹克運動會金牌得主.md "wikilink")
[Category:美國奧林匹克運動會銀牌得主](../Category/美國奧林匹克運動會銀牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會田徑運動員](../Category/2004年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:奥林匹克运动会田径金牌得主](../Category/奥林匹克运动会田径金牌得主.md "wikilink")
[Category:奥林匹克运动会田径银牌得主](../Category/奥林匹克运动会田径银牌得主.md "wikilink")
[Category:密西西比州人](../Category/密西西比州人.md "wikilink")