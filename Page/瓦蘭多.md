<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

**瓦蘭多**（**Valandur**），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·瑞爾·托爾金](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")（J.R.R.
Tolkien）的史詩式奇幻作品《[魔戒三部曲](../Page/魔戒.md "wikilink")》中的虛構人物。

他是[亞爾諾](../Page/亞爾諾.md "wikilink")（Arnor）第八任國王。

## 名字

**瓦蘭多**（Valandur）一名是[昆雅語](../Page/昆雅語.md "wikilink")，意思是「[維拉之僕](../Page/維拉.md "wikilink")」*Servant
of the Valar*\[1\]。

## 總覽

瓦蘭多是亞爾諾的[登丹人](../Page/登丹人.md "wikilink")（Dúnedain），是國王[塔龍鐸](../Page/塔龍鐸_\(亞爾諾\).md "wikilink")（Tarondor）的兒子。\[2\]不清楚他有沒有兄弟姊妹。他的兒子是[伊蘭多](../Page/伊蘭多.md "wikilink")（Elendur）。\[3\]

他是亞爾諾第八任國王。他任內亞爾諾繼續享受[精靈及人類最後同盟](../Page/精靈及人類最後同盟.md "wikilink")（Last
Alliance of Elves and Men）勝利帶來的和平，沒有發生任何大事，瓦蘭多也沒立下任何引人注目的功績。

他生於第三紀元462年，死於652年，享年190歲。\[4\]

## 生平

瓦蘭多在[第三紀元](../Page/第三紀元.md "wikilink")462年出生，\[5\]應該是生於首都[安努米那斯](../Page/安努米那斯.md "wikilink")（Annúminas）。602年，他的父親塔龍鐸去世，由瓦蘭多繼承王位。\[6\]

第三紀元652年，瓦蘭多因為不明原因而被殺，結束了50年的統治，由他的兒子伊蘭多繼承。\[7\]

## 半精靈家系

## 參考

  - 《[中土世界的歷史](../Page/中土世界的歷史.md "wikilink")》
      - 《[中土世界的民族](../Page/中土世界的民族.md "wikilink")》
  - 《[魔戒三部曲](../Page/魔戒.md "wikilink")》及其附錄

## 資料來源

[category:中土大陸的角色](../Page/category:中土大陸的角色.md "wikilink")
[category:中土大陸的登丹人](../Page/category:中土大陸的登丹人.md "wikilink")
[category:魔戒三部曲中的人物](../Page/category:魔戒三部曲中的人物.md "wikilink")

[pl:Królowie
Arnoru\#Valandur](../Page/pl:Królowie_Arnoru#Valandur.md "wikilink")

1.  [Thain's Book:
    Valandur](http://www.tuckborough.net/dunedain.html#Valandur)

2.  《[魔戒三部曲](../Page/魔戒三部曲.md "wikilink")》 2001年 聯經初版翻譯 附錄一帝王本紀及年表

3.
4.
5.
6.
7.