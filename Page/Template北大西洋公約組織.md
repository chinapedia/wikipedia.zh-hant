<noinclude>

## 使用方法

本模板会自动把引用它的页面加入分类。引用本模板时使用下列语法，可以避免加入分类：<small>（注意nocat必须为英文小写）</small>

{{|nocat}}

</noinclude>

[军](../Category/国际组织导航模板.md "wikilink")
[\*](../Category/北大西洋公约组织.md "wikilink")
[Category:国家模板](../Category/国家模板.md "wikilink")