**Athlon
64**是美國[AMD公司的](../Page/AMD.md "wikilink")64位[微處理器型號之一](../Page/微處理器.md "wikilink")，它支援[AMD64架構](../Page/AMD64.md "wikilink")，用於針對個人客戶的64位處理器市場。早期AMD
K8開發計劃中，K8代號為**Hammer**，並使用與[IBM共同開發的](../Page/IBM.md "wikilink")**[SOI](../Page/SOI.md "wikilink")**（絕緣上覆矽）技術。

**Athlon
64**分為[64](../Page/Athlon_64.md "wikilink")、[FX及](../Page/Athlon_64_FX.md "wikilink")[X2三個版本](../Page/Athlon_64_X2.md "wikilink")，當中以Athlon
64-FX的效能為最高，與[Opteron相似](../Page/Opteron.md "wikilink")。Athlon
64除支援AMD64外，還兼容16位和32位的[x86平台](../Page/x86.md "wikilink")。

此外，Athlon
64有一種名為[Cool'n'Quiet的技術](../Page/Cool'n'Quiet.md "wikilink")，當用户執行一些對處理器負荷較少的程式時，處理器的速度和電壓相應降低，從而達到省電的效果。

**Athlon
64**使用**[HyperTransport](../Page/HyperTransport.md "wikilink")**总线技术，从而提高效能。

Socket754的**Athlon 64**大多为ClawHammer核心，封装为mPGA。内置单通道DDR400内存控制器。

Socket939的**Athlon
64**大多为Winchester核心，封装为mPGA。功耗较ClawHammer核心小。内置双通道DDR400内存控制器。

Socket940的FX－51为SledgeHammer核心；FX－53、FX－55为ClawHammer核心；FX-57則為San
Diego核心。封装为CuPGA。内置双通道DDR400内存控制器。支持ECC校检。

## 插座

[Athlon-64-Lenara-CG.jpg](https://zh.wikipedia.org/wiki/File:Athlon-64-Lenara-CG.jpg "fig:Athlon-64-Lenara-CG.jpg")
Athlon 64處理器使用以下的插座。

  - [Socket 754](../Page/Socket_754.md "wikilink")
  - [Socket 939](../Page/Socket_939.md "wikilink")
  - [Socket 940](../Page/Socket_940.md "wikilink") (FX51, 53)
  - [Socket F](../Page/Socket_F.md "wikilink")（FX-70或以上）
  - [Socket AM2](../Page/Socket_AM2.md "wikilink")
  - [Socket
    AM2+](../Page/Socket_AM2+.md "wikilink")（Phenom）它們都是採用[HyperTransport技术来实现处理器与其他设备的连接](../Page/HyperTransport.md "wikilink")。

在A64於2003年9月問世時，僅S754及S940（Opteron用）兩款插座可供用家選擇，而當時的記憶體控制器，在A64剛推出時並未支援在雙通道狀態下，執行未經緩衝的記憶體，S754版的A64在這個時候推出，正好彌補了這個不足。另外，AMD也推出非多處理器版本的Opteron，稱為Athlon
64
FX，它使用S940插座，其倍頻沒有鎖定，特別為愛好[超頻的高階用者而設計](../Page/超頻.md "wikilink")，與Intel的Pentium
4 Extreme Edition競爭。

2004年6月，AMD推出S939插座，給新型號的A64使用，並把S754降格給較慢的A64及Sempron，以取代Socket
A，而S940則供伺服器用的Opteron使用。S939支援雙通道記憶體。

2006年5月，AMD發行Socket
AM2版本的處理器，這款插座支持[DDR2記憶體](../Page/DDR2_SDRAM.md "wikilink")。

2008年，AMD發行Socket
AM2+版本的處理器，這款插座支援[Phenom處理器和HyperTransport](../Page/Phenom.md "wikilink")
3.0協定。

## 核心

### ClawHammer (130 nm SOI)

  - 處理器步進：C0, CG
  - 一級緩存：64 + 64 KB
  - 二級緩存：1024 KB，全速
  - 指令集：[MMX](../Page/MMX.md "wikilink")，Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")，[NX
    Bit (only **CG**)](../Page/NX_Bit.md "wikilink")
  - 插座
      - [Socket 754](../Page/Socket_754.md "wikilink")，800 MHz
        [HyperTransport](../Page/HyperTransport.md "wikilink") (HT800)
      - [Socket 939](../Page/Socket_939.md "wikilink")，1000 MHz
        [HyperTransport](../Page/HyperTransport.md "wikilink") (HT1000)
  - 核心電壓 VCore: 1.50 V
  - 功耗 (TDP)：最大 89 W
  - 首次發表：2003年9月23日
  - 時脈：2000 - 2600 MHz

### Newcastle (130 nm SOI)

  - 處理器步進：CG
  - 一級緩存：64 + 64 KB
  - 二級緩存：512 KB，全速
  - 指令集：[MMX](../Page/MMX.md "wikilink")，Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")，[NX
    Bit (only **CG**)](../Page/NX_Bit.md "wikilink")
  - 插座
      - [Socket 754](../Page/Socket_754.md "wikilink")，800 MHz
        [HyperTransport](../Page/HyperTransport.md "wikilink") (HT800)
      - [Socket 939](../Page/Socket_939.md "wikilink")，1000 MHz
        [HyperTransport](../Page/HyperTransport.md "wikilink") (HT1000)
  - 核心電壓 VCore: 1.50 V
  - 功耗 (TDP)：最大 89 W
  - 首次發表：2004年
  - 時脈：1800 - 2400 MHz

### Winchester (90 nm SOI)

  - 處理器步進：D0
  - 一級緩存：64 + 64 KB
  - 二級緩存：512 KB，全速
  - 指令集：[MMX](../Page/MMX.md "wikilink")，Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")，[NX
    Bit](../Page/NX_Bit.md "wikilink")
  - 插座：[Socket 939](../Page/Socket_939.md "wikilink")，1000 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink") (HT1000)
  - 核心電壓 VCore: 1.40 V
  - 功耗 (TDP)：最大 67 W
  - 首次發表：2004年
  - 時脈：1800 - 2200 MHz

### Venice (90 nm SOI)

  - 處理器步進：E3, E6
  - 一級緩存：64 + 64 KB
  - 二級緩存：512 KB，全速
  - 指令集：[MMX](../Page/MMX.md "wikilink")，Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")，[NX
    Bit](../Page/NX_Bit.md "wikilink")
  - 插座：
      - [Socket 754](../Page/Socket_754.md "wikilink"), 800 MHz
        [HyperTransport](../Page/HyperTransport.md "wikilink") (HT800)
      - [Socket 939](../Page/Socket_939.md "wikilink")，1000 MHz
        [HyperTransport](../Page/HyperTransport.md "wikilink") (HT1000)
  - 核心電壓 VCore: 1.35 / 1.40 V
  - 功耗 (TDP)：最大 67 W
  - 首次發表：2005年4月4日
  - 時脈：1800 - 2400 MHz

### San Diego (90 nm SOI)

  - 處理器步進：E4, E6
  - 一級緩存：64 + 64 KB
  - 二級緩存：1024 KB，全速
  - 指令集：[MMX](../Page/MMX.md "wikilink")，Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")，[NX
    Bit](../Page/NX_Bit.md "wikilink")
  - 插座：[Socket 939](../Page/Socket_939.md "wikilink")，1000 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink") (HT1000)
  - 核心電壓 VCore: 1.35 / 1.40 V
  - 功耗 (TDP)：最大 67 W
  - 首次發表：2005年4月15日
  - 時脈：2200 - 2800 MHz

### Orleans (90 nm SOI)

  - 處理器步進：F2, F3
  - 一級緩存：64 + 64 KB
  - 二級緩存：512 KB，全速
  - 指令集：[MMX](../Page/MMX.md "wikilink")，Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")，[NX
    Bit](../Page/NX_Bit.md "wikilink")，[Pacifica](../Page/Pacifica.md "wikilink")
  - 插座：[Socket AM2](../Page/Socket_AM2.md "wikilink")，2000 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink") (HT2000)
  - 核心電壓 VCore: 1.20 / 1.25 / 1.35 / 1.40 V
  - 功耗 (TDP)：最大 62 W 最少 35 W
  - 首次發表：2006年5月23日
  - 時脈：1800 - 2600 MHz

### Lima (65 nm SOI)

  - 處理器步進：G1、G2
  - 一級緩存：64 + 64 KB
  - 二級緩存：512 KB，全速
  - 指令集：[MMX](../Page/MMX.md "wikilink")，Extended
    [3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")，[NX
    Bit](../Page/NX_Bit.md "wikilink")，[AMD
    Virtualization](../Page/AMD_Virtualization.md "wikilink")
  - 插座：[Socket AM2](../Page/Socket_AM2.md "wikilink")，2000 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink") (HT2000)
  - 核心電壓 VCore: 0.8 / 1.25 / 1.35 / 1.40 V
  - 功耗 (TDP)：最大 45 W
  - 首次發表：2007年2月20日
  - 時脈：1000 - 2400 MHz

## 參見

  - [AMD处理器列表](../Page/AMD处理器列表.md "wikilink")
  - [AMD Athlon 64處理器列表](../Page/AMD_Athlon_64處理器列表.md "wikilink")

## 外部連結

  - [AMD.com.cn -
    Athlon 64官方中文資料](http://www.amd.com.cn/CHCN/Processors/ProductInformation/0,,30_118_9484,00.html)

[Category:AMD处理器](../Category/AMD处理器.md "wikilink")