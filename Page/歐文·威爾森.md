**歐文·威爾森**（，）是一位[美國男演員和編劇](../Page/美國.md "wikilink")，曾經以2001年電影《[天才一族](../Page/天才一族.md "wikilink")》的劇本入圍[奧斯卡](../Page/奧斯卡金像獎.md "wikilink")[最佳原創劇本獎](../Page/奧斯卡最佳原創劇本獎.md "wikilink")。不過威爾森最有名的還是他所演出的喜劇作品，如2001年電影《[名模大間諜](../Page/名模大間諜.md "wikilink")》（*Zoolander*）與2005年的《[婚禮終結者](../Page/婚禮終結者.md "wikilink")》，以及让他获得金球奖最佳男演员提名的2011年電影《[午夜巴黎](../Page/午夜巴黎.md "wikilink")》，該片由[伍迪·艾伦指导](../Page/伍迪·艾伦.md "wikilink")。

2015年，威爾森主演了恐怖活動題材的動作驚悚片《[無處可逃](../Page/無處可逃.md "wikilink")》。此外，他回歸演出了2016年的《[名模大間諜2](../Page/名模大間諜2.md "wikilink")》。

## 早年生活

歐文出生於[美國](../Page/美國.md "wikilink")[德州](../Page/德州.md "wikilink")[達拉斯](../Page/達拉斯.md "wikilink")，[愛爾蘭裔美國人](../Page/愛爾蘭.md "wikilink")，家庭信仰[羅馬天主教](../Page/羅馬天主教.md "wikilink")。母親[蘿拉·康寧漢·威爾森](../Page/蘿拉·康寧漢·威爾森.md "wikilink")（Laura
Cunningham
Wilson）是一名知名[攝影師](../Page/攝影師.md "wikilink")，父親[羅伯特·安德魯·威爾森](../Page/羅伯特·安德魯·威爾森.md "wikilink")（Robert
Andrew Wilson）是一名[廣告行政人員及一間公共電視台的導播](../Page/廣告.md "wikilink")。

歐文曾進入[蘭普萊特學校](../Page/蘭普萊特學校.md "wikilink")（The Lamplighter
School）就讀，2005年曾經造訪母校；也曾就讀[德州聖馬克學校](../Page/德州聖馬克學校.md "wikilink")（[St.
Mark's School of
Texas](../Page/:en:St._Mark's_School_of_Texas.md "wikilink")），不過在高一時期因為了數學作業而偷竊數學老師的教科書，所以被踢出學校\[1\]。之後畢業於[新墨西哥軍校](../Page/新墨西哥軍校.md "wikilink")（[New
Mexico Military
Institute](../Page/:en:New_Mexico_Military_Institute.md "wikilink")）。

1991年，歐文與兩個兄弟[盧克·威爾遜](../Page/盧克·威爾遜.md "wikilink")（Luke）以及安德魯（Andrew）一同搬到[洛杉磯](../Page/洛杉磯.md "wikilink")，展開演藝生涯。

## 私人生活

### 感情生活

威爾森曾與[黛咪·摩爾和歌手](../Page/黛咪·摩爾.md "wikilink")[雪瑞兒·可洛](../Page/雪瑞兒·可洛.md "wikilink")（[Sheryl
Crow](../Page/:en:Sheryl_Crow.md "wikilink")）傳出緋聞，最近則是與女演員[凱特·哈德森交往](../Page/凱特·哈德森.md "wikilink")，不過在2007年5月畫下句點。

### 自殺事件

威爾森於2007年8月26日被送往[聖塔莫尼卡的聖約翰醫院當中](../Page/聖塔莫尼卡.md "wikilink")，根據了解，是由別人在威爾森的[聖塔莫尼卡家中報案](../Page/聖塔莫尼卡.md "wikilink")。聖塔莫尼卡警局宣稱，威爾森以割腕和服藥過量的方式企圖自殺。情況逐步穩定後，威爾森轉送到洛杉磯[席達西尼醫學中心](../Page/席達西尼醫學中心.md "wikilink")（Cedars-Sinai
Medical Center），情況曾一度很危險，不過後來病況逐漸穩定。

威爾森在8月27日發布聲明：「我誠摯的希望媒體能夠在這個艱難期給我私人的空間接受治療與照顧。」，8月29日時，威爾森推掉了《[開麥拉驚魂](../Page/開麥拉驚魂.md "wikilink")》的客串演出（此角色最後由[馬修·麥康納取代](../Page/馬修·麥康納.md "wikilink")）\[2\]。威爾森仍持續接受觀察當中。10月4日，威爾森出席《[大吉嶺有限公司](../Page/大吉嶺有限公司.md "wikilink")》的洛杉磯首映會，這是他開始接受治療後第一次現身於公共埸合。\[3\]

## 作品列表

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>譯名</p></td>
<td><p>原名</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p>《<a href="../Page/脫線沖天炮.md" title="wikilink">脫線沖天炮</a>》</p></td>
<td><p><a href="../Page/:en:Bottle_Rocket.md" title="wikilink"><em>Bottle Rocket</em></a></p></td>
<td><p>迪格南<br />
Dignan</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/王牌特派員.md" title="wikilink">王牌特派員</a>》</p></td>
<td><p><em><a href="../Page/:en:The_Cable_Guy.md" title="wikilink">The Cable Guy</a></em></p></td>
<td><p>羅賓的會面對象<br />
Robin's date</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p>《<a href="../Page/大蟒蛇－神出鬼沒.md" title="wikilink">大蟒蛇－神出鬼沒</a>》</p></td>
<td><p><em><a href="../Page/:en:Anaconda_(film).md" title="wikilink">Anaconda</a></em></p></td>
<td><p>蓋瑞·狄森<br />
Gary Dixon</p></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td></td>
<td><p><a href="../Page/:en:Permanent_Midnight.md" title="wikilink"><em>Permanent Midnight</em></a></p></td>
<td><p>尼基<br />
Nicky</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/絕世天劫.md" title="wikilink">世界末日</a>》</p></td>
<td><p><em><a href="../Page/:en:Armageddon_(1998_film).md" title="wikilink">Armageddon</a></em></p></td>
<td><p>奧斯卡·崔<br />
Oscar</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td></td>
<td><p><a href="../Page/:en:Heat_Vision_and_Jack.md" title="wikilink"><em>Heat Vision and Jack</em></a></p></td>
<td><p>道格<br />
Doug</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/鬼入侵.md" title="wikilink">鬼入侵</a>》</p></td>
<td><p><em><a href="../Page/:en:The_Haunting_(1999_film).md" title="wikilink">The Haunting</a></em></p></td>
<td><p>路克·桑德森<br />
Luke Sanderson</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/冠軍的早餐.md" title="wikilink">冠軍的早餐</a>》</p></td>
<td><p><em><a href="../Page/:en:Breakfast_of_Champions_(film).md" title="wikilink">Breakfast of Champions</a></em></p></td>
<td><p>孟特·雷比<br />
Monte Rapid</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/心理陷阱.md" title="wikilink">心理陷阱</a>》</p></td>
<td><p><a href="../Page/:en:The_Minus_Man.md" title="wikilink"><em>The Minus Man</em></a></p></td>
<td><p>范恩·席格<br />
Vann Siegert</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p>《<a href="../Page/門當父不對.md" title="wikilink">門當父不對</a>》</p></td>
<td><p><a href="../Page/:en:Meet_the_Parents.md" title="wikilink"><em>Meet the Parents</em></a></p></td>
<td><p>凱文·羅利<br />
Kevin Rawley</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/西域威龍.md" title="wikilink">西域威龍</a>》</p></td>
<td><p><em>Shanghai Noon</em></p></td>
<td><p>洛伊·歐班儂<br />
Roy O'Bannon</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p>《<a href="../Page/衝出封鎖線.md" title="wikilink">衝出封鎖線</a>》</p></td>
<td><p><em><a href="../Page/:en:Behind_Enemy_Lines_(film).md" title="wikilink">Behind Enemy Lines</a></em></p></td>
<td><p>克里斯·柏奈特上尉<br />
Lt. Chris Burnett</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/天才一族.md" title="wikilink">天才一族</a>》</p></td>
<td><p><em><a href="../Page/:en:The_Royal_Tenenbaums.md" title="wikilink">The Royal Tenenbaums</a></em></p></td>
<td><p>艾里·凱許<br />
Eli Cash</p></td>
<td><p>提名-<a href="../Page/奧斯卡最佳原創劇本獎.md" title="wikilink">奧斯卡最佳原創劇本獎</a><br />
提名-<a href="../Page/英國電影學院獎最佳原創劇本.md" title="wikilink">英國電影學院獎最佳原創劇本</a></p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/名模大間諜.md" title="wikilink">名模大間諜</a>》</p></td>
<td><p><em><a href="../Page/:en:Zoolander.md" title="wikilink">Zoolander</a></em></p></td>
<td><p>韓賽爾·麥當勞<br />
Hansel McDonald</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p>《<a href="../Page/金牌間諜.md" title="wikilink">金牌間諜</a>》</p></td>
<td><p><em><a href="../Page/:en:I_Spy_(film).md" title="wikilink">I Spy</a></em></p></td>
<td><p>亞歷克斯·史考特<br />
Alex Scott</p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p>《<a href="../Page/皇家威龍.md" title="wikilink">皇家威龍</a>》</p></td>
<td><p><em><a href="../Page/:en:Shanghai_Knights.md" title="wikilink">Shanghai Knights</a></em></p></td>
<td><p>洛伊·歐班儂<br />
Roy O'Bannon</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/:en:Yeah_Right!.md" title="wikilink"><em>Yeah Right!</em></a></p></td>
<td><p>本人</p></td>
<td><p>客串演出</p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p>《<a href="../Page/海海人生.md" title="wikilink">海海人生</a>》</p></td>
<td><p><em><a href="../Page/:en:The_Life_Aquatic_with_Steve_Zissou.md" title="wikilink">The Life Aquatic with Steve Zissou</a></em></p></td>
<td><p>奈德·普林頓<br />
Ned Plimpton</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/親家路窄.md" title="wikilink">親家路窄</a>》</p></td>
<td><p><em>Meet the Fockers</em></p></td>
<td><p>凱文·羅利<br />
Kevin Rawley</p></td>
<td><p>客串演出</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/环游世界八十天_(2004年电影).md" title="wikilink">環遊世界八十天</a>》</p></td>
<td><p><em><a href="../Page/:en:Around_the_World_in_80_Days_(2004_film).md" title="wikilink">Around the World in 80 Days</a></em></p></td>
<td><p><a href="../Page/萊特兄弟.md" title="wikilink">維爾伯·萊特</a></p></td>
<td><p>客串演出</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/警網雙雄.md" title="wikilink">警網雙雄</a>》</p></td>
<td><p><em><a href="../Page/:en:Starsky_&amp;_Hutch_(film).md" title="wikilink">Starsky &amp; Hutch</a></em></p></td>
<td><p>肯·賀區森警探<br />
Ken Hutchinson</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/夏威夷生死鬥.md" title="wikilink">夏威夷生死鬥</a>》</p></td>
<td><p><em><a href="../Page/:en:The_Big_Bounce_(2004_film).md" title="wikilink">The Big Bounce</a></em></p></td>
<td><p>傑克·萊恩<br />
Jack Ryan</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p>《<a href="../Page/愛情奧客.md" title="wikilink">愛情奧客</a>》</p></td>
<td><p><em><a href="../Page/:en:The_Wendell_Baker_Story.md" title="wikilink">The Wendell Baker Story</a></em></p></td>
<td><p>尼爾·金<br />
Neil King</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/冒牌伴郎生擒姊妹團.md" title="wikilink">冒牌伴郎生擒姊妹團</a>》</p></td>
<td><p><em>Wedding Crashers</em></p></td>
<td><p>約翰·貝克維<br />
John Beckwith</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p>《<a href="../Page/博物館驚魂夜.md" title="wikilink">博物館驚魂夜</a>》</p></td>
<td><p><em>Night at the Museum</em></p></td>
<td><p>牛仔傑迪戴亞·史密斯<br />
<a href="../Page/:en:Jedediah_Smith.md" title="wikilink">Jedediah Smith</a></p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/新婚奧客.md" title="wikilink">新婚奧客</a>》</p></td>
<td><p><em><a href="../Page/:en:You,_Me_and_Dupree.md" title="wikilink">You, Me and Dupree</a></em></p></td>
<td><p>藍道夫·杜痞<br />
Randolph Dupree</p></td>
<td><p>兼監製</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/汽車總動員.md" title="wikilink">汽車總動員</a>》</p></td>
<td><p><em>Cars</em></p></td>
<td><p><a href="../Page/閃電麥坤.md" title="wikilink">閃電麥坤</a><br />
<a href="../Page/:en:Lightning_McQueen.md" title="wikilink">Lightning McQueen</a></p></td>
<td><p>配音</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p>《<a href="../Page/大吉嶺有限公司.md" title="wikilink">大吉嶺有限公司</a>》</p></td>
<td><p><em><a href="../Page/:en:The_Darjeeling_Limited.md" title="wikilink">The Darjeeling Limited</a></em></p></td>
<td><p>法蘭西斯·惠特曼<br />
Francis Whitman</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>《<a href="../Page/廉價保鑣.md" title="wikilink">廉價保鑣</a>》</p></td>
<td><p><em><a href="../Page/:en:Drillbit_Taylor.md" title="wikilink">Drillbit Taylor</a></em></p></td>
<td><p>崔爾比·泰勒<br />
Drillbit Taylor</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/馬利與我.md" title="wikilink">馬利與我</a>》</p></td>
<td><p><em><a href="../Page/:en:Marley_&amp;_Me_(film).md" title="wikilink">Marley &amp; Me</a></em></p></td>
<td><p>約翰·葛洛根<br />
John Grogan</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p>《<a href="../Page/博物館驚魂夜2.md" title="wikilink">博物館驚魂夜2</a>》</p></td>
<td><p><em>Night at the Museum 2: Battle of the Smithsonian</em></p></td>
<td><p>牛仔傑迪戴亞·史密斯<br />
<a href="../Page/:en:Jedediah_Smith.md" title="wikilink">Jedediah Smith</a></p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/了不起的狐狸爸爸.md" title="wikilink">了不起的狐狸爸爸</a>》</p></td>
<td><p><em>Fantastic Mr. Fox</em></p></td>
<td><p>Coach Skip</p></td>
<td><p>配音</p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p>《<a href="../Page/拜見岳父大人3.md" title="wikilink">拜見岳父大人3</a>》</p></td>
<td><p><em>Little Fockers</em></p></td>
<td><p>凱文·羅利<br />
Kevin Rawley</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/酷狗馬馬杜.md" title="wikilink">酷狗馬馬杜</a>》</p></td>
<td><p><em><a href="../Page/:en:Marmaduke_(film).md" title="wikilink">Marmaduke</a></em></p></td>
<td><p>馬默杜克<br />
Marmaduke</p></td>
<td><p>配音</p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/愛在心裡怎知道.md" title="wikilink">愛在心裡怎知道</a>》</p></td>
<td><p><em><a href="../Page/:en:How_Do_You_Know_(film).md" title="wikilink">How Do You Know</a></em></p></td>
<td><p>曼尼<br />
Manny</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p>《<a href="../Page/汽車總動員2.md" title="wikilink">汽車總動員2</a>》</p></td>
<td><p><em>Cars 2</em></p></td>
<td><p><a href="../Page/閃電麥坤.md" title="wikilink">閃電麥坤</a><br />
<a href="../Page/:en:Lightning_McQueen.md" title="wikilink">Lightning McQueen</a></p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/嘿咻卡.md" title="wikilink">嘿咻卡</a>》</p></td>
<td><p><em><a href="../Page/Hall_Pass_(2011_film).md" title="wikilink">Hall Pass</a></em></p></td>
<td><p>瑞克<br />
Rick Mills</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/午夜巴黎.md" title="wikilink">午夜巴黎</a>》</p></td>
<td><p><em><a href="../Page/:en:Midnight_in_Paris.md" title="wikilink">Midnight in Paris</a></em></p></td>
<td><p>吉爾<br />
Gil Pender</p></td>
<td><p>提名-<a href="../Page/喜劇獎.md" title="wikilink">喜劇獎最佳喜劇男主角</a><br />
提名-<a href="../Page/金球獎最佳音樂及喜劇類電影男主角.md" title="wikilink">金球獎最佳音樂及喜劇類電影男主角</a><br />
提名-<a href="../Page/美國演員工會獎.md" title="wikilink">美國演員工會獎最佳整體演出</a><br />
提名-<a href="../Page/鳳凰城影評人協會.md" title="wikilink">鳳凰城影評人協會獎最佳整體演出</a></p></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/年度鳥事.md" title="wikilink">年度鳥事</a>》</p></td>
<td><p><em><a href="../Page/:en:The_Big_Year.md" title="wikilink">The Big Year</a></em></p></td>
<td><p>肯尼·博斯蒂克<br />
Kenny Bostick</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p>《<a href="../Page/實習大叔.md" title="wikilink">實習大叔</a>》</p></td>
<td><p><em>The Internship</em></p></td>
<td><p>尼克<br />
Nick</p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p>《<a href="../Page/人生大逆轉.md" title="wikilink">人生大逆轉</a>》</p></td>
<td><p><em>Are You Here</em></p></td>
<td><p>史蒂夫·達拉斯<br />
Steve Dallas</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/歡迎來到布達佩斯大飯店.md" title="wikilink">歡迎來到布達佩斯大飯店</a>》</p></td>
<td><p><em>The Grand Budapest Hotel</em></p></td>
<td><p>查克<br />
M. Chuck</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/小蠟筆們的彩色世界大冒險.md" title="wikilink">小蠟筆們的彩色世界大冒險</a>》</p></td>
<td><p><em>The Hero of Color City</em></p></td>
<td><p>瑞奇<br />
Ricky The Dragon</p></td>
<td><p>配音</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/性本惡.md" title="wikilink">性本惡</a>》</p></td>
<td><p><em>Inherent Vice</em></p></td>
<td><p>科伊·哈靈根<br />
Coy Harlingen</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>《<a href="../Page/博物館驚魂夜3.md" title="wikilink">博物館驚魂夜3</a>》</p></td>
<td><p><em>Night at the Museum 3: Secret of the Tomb</em></p></td>
<td><p>牛仔傑迪戴亞·史密斯<br />
<a href="../Page/:en:Jedediah_Smith.md" title="wikilink">Jedediah Smith</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014-至今</p></td>
<td><p>《<a href="../Page/拖线狂想曲.md" title="wikilink">拖線狂想曲：傳說中的水箱溫泉</a>》</p></td>
<td><p><em>Cars Toons: Tales From Radiator Springs</em></p></td>
<td><p><a href="../Page/閃電麥坤.md" title="wikilink">閃電麥坤</a><br />
<a href="../Page/:en:Lightning_McQueen.md" title="wikilink">Lightning McQueen</a></p></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p>《<a href="../Page/百老匯風流記.md" title="wikilink">百老匯風流記</a>》</p></td>
<td><p><em>She's Funny That Way</em></p></td>
<td><p>阿諾·艾伯森<br />
Arnold Albertson</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/無處可逃.md" title="wikilink">無處可逃</a>》</p></td>
<td><p><em>No Escape</em></p></td>
<td><p>傑克·杜威<br />
Jack Dwyer</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p>《<a href="../Page/名模大間諜2.md" title="wikilink">名模大間諜2</a>》</p></td>
<td><p><em>Zoolander 2</em></p></td>
<td><p>韓賽爾·麥當勞<br />
Hansel McDonald</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/笨賊樂盜家.md" title="wikilink">笨賊樂盜家</a>》</p></td>
<td><p><em>Masterminds</em></p></td>
<td><p>史蒂夫·錢伯斯<br />
Steve Chambers</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p>《<a href="../Page/奇蹟男孩.md" title="wikilink">奇蹟男孩</a>》</p></td>
<td><p><em>Wonder</em></p></td>
<td><p>奈特·普曼Nate Pullman</p></td>
</tr>
<tr class="odd">
<td><p>《<a href="../Page/父親形象.md" title="wikilink">父親形象</a>》</p></td>
<td><p><em>Father Figures</em></p></td>
<td><p>凱爾·雷諾茲<br />
Kyle Reynolds</p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部連結

  -
[W](../Category/美国電影演員.md "wikilink")
[W](../Category/美国作家.md "wikilink")
[W](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:美国男配音演员](../Category/美国男配音演员.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")

1.  [AskMen.com - Owen
    Wilson](http://ca.askmen.com/men/entertainment_150/188c_owen_wilson.html)

2.
3.