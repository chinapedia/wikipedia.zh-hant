**天康**（566年二月—十二月）是[南朝陳政權](../Page/南朝陳.md "wikilink")，世祖文皇帝[陈蒨的](../Page/陈蒨.md "wikilink")[年号](../Page/年号.md "wikilink")，共计近1年。

天康元年春二月丙子，诏曰：“朕以寡德，纂承洪绪，日昃劬劳，思弘景业，而政道多昧，黎庶未康，兼疹患淹时，亢阳累月，百姓何咎，实由朕躬，念兹在兹，痛如疾首。可大赦天下，改天嘉七年为天康元年。”

天康元年四月陈废帝[陳伯宗即位沿用](../Page/陳伯宗.md "wikilink")。

## 大事记

## 出生

## 逝世

## 纪年

| 天康                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 566年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙戌](../Page/丙戌.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他使用[天康年號的政權](../Page/天康.md "wikilink")
  - 同期存在的其他政权年号
      - [天保](../Page/天保_\(萧岿\).md "wikilink")（562年二月—585年十二月）：[西梁政權梁明帝](../Page/西梁.md "wikilink")[萧岿的年号](../Page/萧岿.md "wikilink")
      - [天统](../Page/天统_\(高纬\).md "wikilink")（565年四月—569年十二月）：[北齊政权齊後主](../Page/北齊.md "wikilink")[高纬年号](../Page/高纬.md "wikilink")
      - [天和](../Page/天和_\(北周\).md "wikilink")（566年正月—572年三月）：[北周政权周武帝](../Page/北周.md "wikilink")[宇文邕年号](../Page/宇文邕.md "wikilink")
      - [延昌](../Page/延昌_\(麴乾固\).md "wikilink")（561年—601年）：[高昌政权](../Page/高昌.md "wikilink")[麴-{乾}-固年号](../Page/麴乾固.md "wikilink")
      - [開國](../Page/開國.md "wikilink")（551年—568年）：[新羅](../Page/新羅.md "wikilink")[真興王的年號](../Page/新羅真興王.md "wikilink")

## 參考文獻

  - 徐红岚，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN 7538246193
  - 松橋達良，《》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:南朝陳年號](../Category/南朝陳年號.md "wikilink")
[Category:560年代中国政治](../Category/560年代中国政治.md "wikilink")
[Category:566年](../Category/566年.md "wikilink")