**劉懋**（），[字](../Page/表字.md "wikilink")**黽夫**，[號](../Page/號.md "wikilink")**養衷**。[陕西](../Page/陕西.md "wikilink")[临潼人](../Page/临潼.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[直隸](../Page/南直隸.md "wikilink")[鳳陽](../Page/鳳陽.md "wikilink")（今[安徽](../Page/安徽.md "wikilink")），[明朝政治人物](../Page/明朝.md "wikilink")，同[進士出身](../Page/進士.md "wikilink")。

[籍貫](../Page/籍貫.md "wikilink")[直隸](../Page/南直隸.md "wikilink")[鳳陽](../Page/鳳陽.md "wikilink")（今[安徽](../Page/安徽.md "wikilink")），父輩時徙[临潼亨利里](../Page/临潼.md "wikilink")。[萬曆四十一年](../Page/萬曆.md "wikilink")（1613年）登癸丑科進士，歷任[项城](../Page/项城.md "wikilink")、[宁陵](../Page/宁陵.md "wikilink")、[新安诸](../Page/新安縣.md "wikilink")[县](../Page/县.md "wikilink")[知县](../Page/知县.md "wikilink")，升[禮科](../Page/禮科.md "wikilink")[给事中](../Page/给事中.md "wikilink")。因得罪[魏忠贤而削职](../Page/魏忠贤.md "wikilink")。[崇禎初年](../Page/崇禎.md "wikilink")（1628年）任[刑科](../Page/刑科.md "wikilink")[右給事中](../Page/右給事中.md "wikilink")，[崇祯二年](../Page/崇祯.md "wikilink")（1629年）[御史](../Page/御史.md "wikilink")[毛羽健上](../Page/毛羽健.md "wikilink")[疏说](../Page/奏疏.md "wikilink")“驿递一事，最为民害”，接著刘懋又给上疏称：“当今天下[州县困于](../Page/州.md "wikilink")[驿站的约十之七八](../Page/驿站.md "wikilink")，而驿站用于公务的仅十分之二，用于私事的占十分之八”，[崇禎帝以此事問](../Page/崇禎帝.md "wikilink")[韓爌](../Page/韓爌.md "wikilink")，韓爌稱：“汰兵止當清佔冒及增設冗兵爾。衝地額兵不可汰也。”\[1\]。

同年五月正式议裁，劉懋改任[兵科](../Page/兵科.md "wikilink")[左給事中](../Page/左給事中.md "wikilink")，專管驛遞整頓事務。此舉卻招致極大反彈，劉懋不久上疏：“游滑不得料理[里甲也](../Page/里甲.md "wikilink")，则怨；驿所[官吏不得索长例也](../Page/官吏.md "wikilink")，则怨；各[衙门承舍不得勒占马匹也](../Page/衙门.md "wikilink")，则怨；州县吏不得私折夫马也，则怨；[道](../Page/道_\(行政區劃\).md "wikilink")[府](../Page/府_\(行政區劃\).md "wikilink")[廳不得擅用滥用也](../Page/廳_\(行政區劃\).md "wikilink")，则怨；即按抚与臣同事者不得私差多差也，则怨。所不怨者独里中[农民耳](../Page/农民.md "wikilink")！”，[崇祯四年](../Page/崇祯.md "wikilink")（1631年）二月上報，节[銀六十八万五千余](../Page/白銀.md "wikilink")[两](../Page/两.md "wikilink")\[2\]，“即臣所请借抵新饷以宽民力者”，
却“嗣因边事孔亟，始移为修防之需”“臣非不知皇上不得已之苦心，乃国家自有经长之制，原不在加派之间”。

裁驿有害于[明朝](../Page/明朝.md "wikilink")[政权的稳定](../Page/政权.md "wikilink")，《[明季北略](../Page/明季北略.md "wikilink")》就說：“祖宗设立驿站，所以笼络强有力之人，使之肩挑背负，耗其精力，销其岁月，糊其口腹，使不敢为非，原有妙用。”[杨士聪说](../Page/杨士聪.md "wikilink")：“天生此食力之民，往来道路，博分文以给朝夕。一旦无所施其力，不去为贼，将安所得乎？后有自[秦](../Page/陝西.md "wikilink")，[晋](../Page/山西.md "wikilink")、[中州来者](../Page/中州.md "wikilink")，言所擒之贼，多系驿递夫役，其肩有痕，易辨也”。[李自成因驛站被裁](../Page/李自成.md "wikilink")，最後鋌而走險，成為農民軍領袖。如史籍所说：“李自成一[银川驿之马夫耳](../Page/银川.md "wikilink")，奋臂大呼，[九州幅裂](../Page/九州_\(神州\).md "wikilink")。”在百官唾骂声中，刘懋辭官返鄉，不久憂鬱死。棺木運至[山东](../Page/山东.md "wikilink")，家人竟然雇不到一人辇负，以致寄存旅舍，经年不得归葬鄉里\[3\]。

## 注釋

## 參考文獻

  - [張廷玉等](../Page/張廷玉.md "wikilink")，《明史》，中華書局點校本
  - [計六奇](../Page/計六奇.md "wikilink")，《明季北略》

## 參見

  - [中國驛站史](../Page/中國驛站史.md "wikilink")

[Category:明朝寧陵縣知縣](../Category/明朝寧陵縣知縣.md "wikilink")
[Category:明朝禮科給事中](../Category/明朝禮科給事中.md "wikilink")
[Category:明朝刑科給事中](../Category/明朝刑科給事中.md "wikilink")
[Category:明朝兵科給事中](../Category/明朝兵科給事中.md "wikilink")
[Category:臨潼人](../Category/臨潼人.md "wikilink")
[M](../Category/劉姓.md "wikilink")

1.  [清](../Page/清.md "wikilink")·[張廷玉等](../Page/張廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷240）：“時遼事急，朝議汰各鎮兵。又以兵科給事中劉懋疏，議裁驛卒。帝以問爌，爌言：「汰兵止當清占冒及增設冗兵爾。沖地額兵不可汰也。驛傳疲累，當責按臣核減，以蘇民困，其所節省，仍還之民。」
2.  崇祯四年二月刘懋《驿递裁扣事竣疏》
3.  [清](../Page/清.md "wikilink")·[計六奇](../Page/計六奇.md "wikilink")，《[明季北略](../Page/明季北略.md "wikilink")》（卷5）：“给事中刘懋上疏，请裁驿递，可岁省金钱数十余万。上喜，着为令，有滥予者，罪不赦。部科监司，多以此获遣去。天下惴惴奉法。顾秦、晋士瘠，无田可耕，其民饶膂力，贫无赖者，藉水陆舟车奔走自给，至是遂无所得食，未几，秦中叠饥，斗米千钱，民不聊生，草根树皮，剥削殆尽。上命御史吴牲赉银十万两往赈，然不能救。又失驿站生计，所在溃兵煽之，遂相聚为盗，而全陕无宁土矣。给事中许国荣、御史姜思睿等知其故，具言驿站不当罢，上皆不允，众共切齿于懋，呼其名而诅咒之，图其形而丛射之，懋以是自恨死。棺至山东，莫肯为辇负者，至委棺旅舍，经年不得归。”