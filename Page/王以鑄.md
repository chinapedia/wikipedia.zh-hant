**王以铸**[天津人](../Page/天津人.md "wikilink")，毕业于[北京大学英文系](../Page/北京大学.md "wikilink")，中国大陆翻译家。王以铸通晓[中文](../Page/中文.md "wikilink")、[古希腊文](../Page/古希腊文.md "wikilink")、[拉丁文](../Page/拉丁文.md "wikilink")、[英文](../Page/英文.md "wikilink")、[法文](../Page/法文.md "wikilink")、[日文](../Page/日文.md "wikilink")、[德文](../Page/德文.md "wikilink")、[俄文共](../Page/俄文.md "wikilink")8种语言、略通[希伯来文](../Page/希伯来文.md "wikilink")、[西班牙文](../Page/西班牙文.md "wikilink")，被认为是唯一可以和[钱锺书用不同语言](../Page/钱锺书.md "wikilink")“交流沟通”的学者。

王從古希臘原文譯出[希羅多德的](../Page/希羅多德.md "wikilink")《[歷史](../Page/歷史.md "wikilink")》，並以文言文譯出日本文學名著、[吉田兼好的随笔集子](../Page/吉田兼好.md "wikilink")《[徒然草](../Page/徒然草.md "wikilink")》，2002年獲中國翻譯協會表彰，授與「資深翻譯家」的稱號。家境富裕，幼年在私塾度過，好舊體詩，詩詞集《傾蓋集》收有他的詩作。

## 生平

1925年，王以铸出生在[天津](../Page/天津.md "wikilink")，父亲是一名商人，思想开放，姐姐没有裹脚。
儿时，王以铸住在[天津](../Page/天津.md "wikilink")[八国租界](../Page/天津租界.md "wikilink")，各种语言的书店数不胜数，培养了他的[英文和](../Page/英文.md "wikilink")[法文能力](../Page/法文.md "wikilink")。

王以铸毕业于[北京大学英文系](../Page/北京大学.md "wikilink")。后来在天津的家里自学7年，自学了[日文](../Page/日文.md "wikilink")、[德文和](../Page/德文.md "wikilink")[意大利文](../Page/意大利文.md "wikilink")。

王以铸曾在天津[南开中学等教书](../Page/南开中学.md "wikilink")。

[中华人民共和国建国后](../Page/中华人民共和国.md "wikilink")，王以铸在[人民文学出版社工作](../Page/人民文学出版社.md "wikilink")。

1966年，[毛泽东发起](../Page/毛泽东.md "wikilink")[文化大革命](../Page/文化大革命.md "wikilink")，王以铸被下放到[咸宁市](../Page/咸宁市.md "wikilink")[五七干校](../Page/五七干校.md "wikilink")[劳动改造](../Page/劳动改造.md "wikilink")，期间他主要研究[唐诗和唐朝](../Page/唐诗.md "wikilink")[方言](../Page/方言.md "wikilink")。

## 家庭

王以铸妻子[崔妙音](../Page/崔妙音.md "wikilink")，毕业于北京[辅仁大学英文系](../Page/辅仁大学.md "wikilink")、[北京师范大学](../Page/北京师范大学.md "wikilink")，通晓英文、德文、法文。

## 譯著

  - 《[历史](../Page/希羅多德歷史.md "wikilink")》（[古希腊](../Page/古希腊.md "wikilink")[希罗多德](../Page/希罗多德.md "wikilink")）\[1\]
  - 《[编年史](../Page/塔西佗編年史.md "wikilink")》（[古罗马](../Page/古罗马.md "wikilink")[塔西佗](../Page/塔西佗.md "wikilink")）\[2\]
  - 《[战争史](../Page/战争史.md "wikilink")》（第九卷為《[秘史](../Page/秘史_\(普洛科皮烏斯\).md "wikilink")》）（[拜占庭](../Page/拜占庭.md "wikilink")[普罗科匹乌斯](../Page/普罗科匹乌斯.md "wikilink")）\[3\]
  - 《[古代东方史](../Page/古代东方史.md "wikilink")》（[俄罗斯](../Page/俄罗斯.md "wikilink")[阿甫基耶夫](../Page/阿甫基耶夫.md "wikilink")）\[4\]
  - 《[古代罗马史](../Page/古代罗马史.md "wikilink")》（俄罗斯[科瓦略夫](../Page/科瓦略夫.md "wikilink")）\[5\]
  - 《[喀提林阴谋](../Page/喀提林阴谋_\(书籍\).md "wikilink")　[朱古达战争](../Page/朱古达战争_\(书籍\).md "wikilink")》（[撒路斯提乌斯](../Page/撒路斯提乌斯.md "wikilink")）\[6\]
  - 《[凯撒评传](../Page/凯撒评传.md "wikilink")》（[谢·勒·乌特琴柯](../Page/谢·勒·乌特琴柯.md "wikilink")）\[7\]
  - 《[奧古斯都](../Page/奥古斯都_\(史學著作\).md "wikilink")》（[特威兹穆尔](../Page/特威兹穆尔.md "wikilink")）\[8\]
  - 《歌德席勒叙事谣曲选》（[德国](../Page/德国.md "wikilink")[歌德](../Page/歌德.md "wikilink")、[席勒](../Page/席勒.md "wikilink")）
  - 《[徒然草](../Page/徒然草.md "wikilink")》（[日本](../Page/日本.md "wikilink")[吉田兼好](../Page/吉田兼好.md "wikilink")）
  - 《[王尔德诗歌](../Page/王尔德诗歌.md "wikilink")》（[英国](../Page/英国.md "wikilink")[王尔德](../Page/王尔德.md "wikilink")）

## 奖项

  - 2004年，中国翻译协会“资深翻译家”荣誉称号。

## 参考文献

## 外部链接

  - 《南方都市報》，2007年5月9日，B14版。
  - 《中國翻譯家辭典》，中國對外翻譯出版公司，1988。

[Category:1925年出生](../Category/1925年出生.md "wikilink")
[Category:天津人](../Category/天津人.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:中华人民共和国翻译家](../Category/中华人民共和国翻译家.md "wikilink")
[Y以](../Category/王姓.md "wikilink")
[Category:古希腊文—中文翻译家](../Category/古希腊文—中文翻译家.md "wikilink")
[Category:拉丁文—中文翻译家](../Category/拉丁文—中文翻译家.md "wikilink")
[Category:英文—中文翻译家](../Category/英文—中文翻译家.md "wikilink")
[Category:法文—中文翻译家](../Category/法文—中文翻译家.md "wikilink")
[Category:德文—中文翻译家](../Category/德文—中文翻译家.md "wikilink")
[Category:日文—中文翻译家](../Category/日文—中文翻译家.md "wikilink")
[Category:俄文—中文翻译家](../Category/俄文—中文翻译家.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.