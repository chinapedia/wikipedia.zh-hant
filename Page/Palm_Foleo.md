Palm
Foleo是一款基於[Linux的](../Page/Linux.md "wikilink")[次筆記型電腦](../Page/次筆記型電腦.md "wikilink")，由[Palm公司於](../Page/Palm公司.md "wikilink")2007年5月30日對外宣佈\[1\]，意在成為[智能手機的夥伴裝置](../Page/智能手機.md "wikilink")。

Foleo包含256
MB快閃記憶體，並會保持啓動而不需開機與關機，擁有[藍芽與](../Page/藍芽.md "wikilink")[Wi-Fi無線通訊能力](../Page/Wi-Fi.md "wikilink")，據報作業系統會運行一個經修改的[Linux核心](../Page/Linux.md "wikilink")\[2\]。整合的軟體包括一個[電子郵件客戶端](../Page/電子郵件.md "wikilink")、[Opera瀏覽器與Documents](../Page/Opera.md "wikilink")
To Go辦公室套件。

約重2.5盎司（1.1
[公斤](../Page/公斤.md "wikilink")）初期價格在扣除100元回郵[折扣後為](../Page/折扣.md "wikilink")499美元。根據Palm公司的說法將於2007年夏季發售。但由于市場定位失敗，3個月后計劃就被取消了，但是後來[小筆電和](../Page/小筆電.md "wikilink")[平板電腦的興起電腦家庭雜誌評論認為Foleo可說是一種啟蒙產品](../Page/平板電腦.md "wikilink")，失敗點在於採用特化的[Linux而非微軟系統顯得不夠親民](../Page/Linux.md "wikilink")，且性能規格又稍微差了一點，再加上宣傳為「手機的週邊附屬品」讓人有累贅之感，而小筆電則為一種小版筆記型電腦的感覺。

## 新聞界的反應

商貿報刊對Foleo有多種初步的反應，特別因為這是一款外觀尺寸不屬於智能手機或全功能筆記型電腦等已制定種類的次筆記型電腦\[3\]。數個評論員指出他們不會樂意多攜帶一款額外的裝置\[4\]。調查集團[Gartner其中一名副總裁指出Palm](../Page/Gartner.md "wikilink")"製造了一種既不便攜又缺乏完整功能的裝置(created
a device that's not quite pocketable, but its not quite full function,
either)"\[5\]。

## 參考

<references/>

## 外部連結

  - [官方網站](https://web.archive.org/web/20070602002245/http://www.palm.com/us/products/mobilecompanion/foleo/)

[Category:Linux](../Category/Linux.md "wikilink")
[Category:Netbook](../Category/Netbook.md "wikilink")
[Category:筆記型電腦](../Category/筆記型電腦.md "wikilink")
[Category:Palm公司](../Category/Palm公司.md "wikilink")

1.
2.
3.  <http://www.informationweek.com/news/showArticle.jhtml?articleID=199703590>
4.  <http://www.engadget.com/2007/05/30/palm-foleo-hands-on>
5.  <http://www.pcmag.com/article2/0,1895,2139017,00.asp>