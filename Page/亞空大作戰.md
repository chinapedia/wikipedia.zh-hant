《**亞空大作戰**》（*Dungeons &
Dragons*）是部於1983年以著名的[桌上角色扮演遊戲](../Page/桌上角色扮演遊戲.md "wikilink")《[龍與地下城](../Page/龍與地下城.md "wikilink")》改編而成的[美國](../Page/美國.md "wikilink")[電視動畫](../Page/電視動畫.md "wikilink")。[台灣](../Page/台灣.md "wikilink")[中視於](../Page/中視.md "wikilink")1985年引進，在每[週日下午](../Page/週日.md "wikilink")5:00至5:30名為《**[卡通假期](../Page/卡通假期.md "wikilink")**》的時段播出。

## 開場白

### 台灣版

### 中國大陸版

## 故事

**韓克**與**戴安娜**、**施拉**、**柏士圖**、**伊力**以及**鮑比**等人在遊樂場中參觀**地獄飛龍**（Dungeons &
Dragons）遊戲，不料飛車在黑暗中出軌，被摔落於茫茫無知之地獄中，在他們覓路返家途中，得**地獄王**之協助，並各獲防身法寶一件，鮑比並得小獨角馬－**獨角兒**為伴，一行人遂以**韓克**為首，一同與惡魔**溫吉爾**對抗並尋找返家之路……

## 人物

  - **地獄王**（Dungeon Master）

<!-- end list -->

  -

:\***韓克**（Hank, the Ranger）

  -

      -
        **地獄王**賦予其**神弓**（Ranger）

:\***伊力**（Eric, the Cavalier）

  -

      -
        **地獄王**賦予其**武士**（Cavalier）

:\***黛安娜**（Diana, the Acrobat）

  -

      -
        **地獄王**賦予其**魔棒**（Acrobat）

:\***柏士圖**（Presto, the Magician）

  -

      -
        **地獄王**賦予其**魔術師**（Magician）

:\***施拉**（Sheila, the Thief）

  -

      -
        **地獄王**賦予其**隱身衣**（Thief）

:\***鮑比**（Bobby, the Barbarian）

  -

      -
        **地獄王**賦予其**小海盜**（Barbarian），為**施拉**之弟。

::\***獨角兒**（Uni）

  -

      -

-----

  - 復仇者**溫吉爾**（Venger）

<!-- end list -->

  -

## 相關條目

  - [龍與地下城](../Page/龍與地下城.md "wikilink")

## 外部連結

  - [**1980-11-食品電視廣告集錦篇** - 英倫卡通包
    亞空大作戰](http://www.youtube.com/watch?v=PQ3gtpa1eHo)
  - [**台灣囝仔(懷舊.蒐藏.交流)** -
    亞空大作戰尪仔仙](https://web.archive.org/web/20070428022447/http://www.taiwan-kids.why3s.net/p3-page/play94.htm)

[Category:美國動畫影集](../Category/美國動畫影集.md "wikilink")
[Category:中視外購動畫](../Category/中視外購動畫.md "wikilink")