[Tai_Hang.JPG](https://zh.wikipedia.org/wiki/File:Tai_Hang.JPG "fig:Tai_Hang.JPG")
[Tai_Hang_Fire_Dragon_5.gif](https://zh.wikipedia.org/wiki/File:Tai_Hang_Fire_Dragon_5.gif "fig:Tai_Hang_Fire_Dragon_5.gif")
[Tai_Hang_Fire_Dragon_1.jpg](https://zh.wikipedia.org/wiki/File:Tai_Hang_Fire_Dragon_1.jpg "fig:Tai_Hang_Fire_Dragon_1.jpg")
[HK_大坑_Tai_Hang_尚巒_Warrenwoods_FV_view_old_zone_Apr-2014_tang_building.JPG](https://zh.wikipedia.org/wiki/File:HK_大坑_Tai_Hang_尚巒_Warrenwoods_FV_view_old_zone_Apr-2014_tang_building.JPG "fig:HK_大坑_Tai_Hang_尚巒_Warrenwoods_FV_view_old_zone_Apr-2014_tang_building.JPG")\]\]
[The_Legend_at_Jardine's_Lookout,_Tai_Hang_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:The_Legend_at_Jardine's_Lookout,_Tai_Hang_\(Hong_Kong\).jpg "fig:The_Legend_at_Jardine's_Lookout,_Tai_Hang_(Hong_Kong).jpg")向南望[渣甸山名門一帶住宅群](../Page/渣甸山名門.md "wikilink")。\]\]
[TaiHangResidentsWelfareAssociation.jpg](https://zh.wikipedia.org/wiki/File:TaiHangResidentsWelfareAssociation.jpg "fig:TaiHangResidentsWelfareAssociation.jpg")
[Shanty_housing_in_Hong_Kong.jpeg](https://zh.wikipedia.org/wiki/File:Shanty_housing_in_Hong_Kong.jpeg "fig:Shanty_housing_in_Hong_Kong.jpeg")
**大坑**（）位於[香港島](../Page/香港島.md "wikilink")[銅鑼灣以南](../Page/銅鑼灣.md "wikilink")、[掃桿埔以北](../Page/掃桿埔.md "wikilink")，是[香港的傳統住宅區](../Page/香港.md "wikilink")。

## 地理

大坑是指[銅鑼灣道](../Page/銅鑼灣道.md "wikilink")、[香港中央圖書館以南及](../Page/香港中央圖書館.md "wikilink")[大坑道一帶的地區](../Page/大坑道.md "wikilink")，大坑道一帶屬於住宅區，沿路也有不少大單位老牌[豪宅](../Page/豪宅.md "wikilink")，例如[龍園](../Page/龍園.md "wikilink")，不少樓齡超過30年。大坑北部接鄰[天后](../Page/天后_\(香港\).md "wikilink")，南部則接鄰[渣甸山](../Page/渣甸山.md "wikilink")，西北面則是[香港島主要購物](../Page/香港島.md "wikilink")、娛樂及旅遊區[銅鑼灣](../Page/銅鑼灣.md "wikilink")。

雖然大坑多被認爲是銅鑼灣的一部分，但過去區內地方在[區議會分別歸入](../Page/區議會.md "wikilink")[東區和](../Page/東區_\(香港\).md "wikilink")[灣仔區](../Page/灣仔區.md "wikilink")：[大坑徑](../Page/大坑徑.md "wikilink")、[虎豹別墅及](../Page/虎豹別墅_\(香港\).md "wikilink")[渣甸山名門均位於灣仔](../Page/渣甸山名門.md "wikilink")[渣甸山選區](../Page/渣甸山.md "wikilink")，以及多數地區歸入灣仔區；少數地區如[勵德邨](../Page/勵德邨.md "wikilink")及[上林等則歸入東區](../Page/上林.md "wikilink")。\[1\]2016年開始，原屬東區的「維園」和「天后」兩區改為歸入灣仔區，亦即差不多整個大坑，包括勵德邨及上林等亦納入灣仔區範圍。

## 歷史

大坑原本是香港島一個傳統村落，有超過100年歷史。今日的[維多利亞公園及](../Page/維多利亞公園.md "wikilink")[皇仁書院一帶土地是](../Page/皇仁書院.md "wikilink")[填海得來](../Page/填海.md "wikilink")，昔日的[海岸線沿著今日的](../Page/海岸線.md "wikilink")[銅鑼灣道](../Page/銅鑼灣道.md "wikilink")。早年有一條大坑從[畢拿山流經大坑一帶再流入海中](../Page/畢拿山.md "wikilink")，大坑因而得名，現時該條水坑被稱為[大坑明渠](../Page/大坑明渠.md "wikilink")。

大坑在每年[中秋節都會舉行](../Page/中秋節.md "wikilink")[舞火龍活動](../Page/大坑舞火龍.md "wikilink")，傳說在1880年8月，有村民死於[瘟疫](../Page/瘟疫.md "wikilink")，後有村民在中秋節舞動火龍及燃放鞭炮，瘟疫便消失，自此大坑居民每年便在中秋節舉行舞火龍，但中途曾經停辦，近年又恢復。2003年[SARS肆虐香港](../Page/严重急性呼吸道综合症.md "wikilink")，大坑亦曾經特別舉行一場舞火龍以求消災。每年的舞火龍是由當地一個地區組織[大坑街坊福利會主辦](../Page/大坑街坊福利會.md "wikilink")，是項活動也是中外[攝影愛好者的盛事之一](../Page/攝影.md "wikilink")。

## 現況

大坑可分為兩部份:
山上的屬於[住宅區](../Page/住宅.md "wikilink")，不少[中產人士居住於此](../Page/中產.md "wikilink")，例如沿着[大坑道而建的屋苑](../Page/大坑道.md "wikilink")[光明臺](../Page/光明臺.md "wikilink")、-{[帝后臺](../Page/帝后臺.md "wikilink")}-及[Y.I](../Page/Y.I.md "wikilink")。

在銅鑼灣道以南的平地則以[唐樓為主](../Page/唐樓.md "wikilink")，這個區域原本開始老化，但因其較低廉租金及方便的交通，近年吸引了不少年青人，如[設計師等居住](../Page/設計師.md "wikilink")。

除此之外，沿着[浣紗街及鄰近街道](../Page/浣紗街.md "wikilink")，泊車十分方便，加上有不少特色的美食小店進駐，成為年青人及一家人飲食的好去處。包括豬扒麵「炳記茶檔」、民聲冰室、家庭式經營的康記粥店、金龍泰美食、源品車仔麵等。小甜谷和Lab
Made分子雪糕已結業。

不過到2015年起，業主眼見該處生意興旺，决定大幅加租高達2-3倍，加上零售業處於寒冬，現時已有多間食肆結業，包括大坑京街開業35年的康記粥店，不過現已重開新康記。\[2\]

## 區內及鄰近建築

### 著名私人屋苑

  - [Y.I](../Page/Y.I.md "wikilink")
    ([大坑道](../Page/大坑道.md "wikilink")10號)
  - [光明臺](../Page/光明臺.md "wikilink")（大坑道5-7號）
  - [帝-{后}-臺](../Page/帝后臺.md "wikilink")（大坑道26號）
  - [上林](../Page/上林_\(香港\).md "wikilink")（大坑道11號）
  - [龍園](../Page/龍園.md "wikilink")（大坑道春暉台1號）
  - [渣甸山名門](../Page/名門_\(香港\).md "wikilink")（[大坑徑](../Page/大坑徑.md "wikilink")23號）
  - [龍華花園](../Page/龍華花園.md "wikilink")（大坑徑25號）
  - [尚巒](../Page/尚巒.md "wikilink")
  - [龍濤苑](../Page/龍濤苑.md "wikilink")（大坑浣紗街23號）

### 公共屋邨

  - [勵德邨](../Page/勵德邨.md "wikilink")

### 教堂、寺院、醫院

  - [聖公會聖馬利亞堂](../Page/聖公會聖馬利亞堂.md "wikilink")
  - [天主教基督君王小堂](../Page/天主教.md "wikilink")
  - [聖保祿醫院](../Page/聖保祿醫院.md "wikilink")
  - [蓮花宮](../Page/蓮花宮.md "wikilink")

### 學校

  - [香港真光中學](../Page/香港真光中學.md "wikilink")
  - [中華基督教會公理高中書院](../Page/中華基督教會公理高中書院.md "wikilink")
  - [李陞大坑小學](../Page/李陞大坑小學.md "wikilink")

#### 大坑選區內

大坑選區內，但[銅鑼灣道以北屬](../Page/銅鑼灣道.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")，不過大坑屬銅鑼灣分區，[渣甸山](../Page/渣甸山.md "wikilink")(渣甸山選區因選區人口太少亦劃入部份大坑地方)則屬[跑馬地](../Page/跑馬地.md "wikilink")

  - [聖保祿學校](../Page/聖保祿學校_\(香港\).md "wikilink")
  - [皇仁書院](../Page/皇仁書院.md "wikilink")

### 其他

  - [虎豹別墅](../Page/虎豹別墅_\(香港\).md "wikilink")
  - [渣甸山居民協會](../Page/渣甸山.md "wikilink")
  - 中華遊樂會
  - [維景酒店](../Page/香港銅鑼灣維景酒店.md "wikilink")
  - 摩頓台天橋

## 景觀

## 交通

### 主要交通幹道

  - [大坑道](../Page/大坑道.md "wikilink")
  - [銅鑼灣道](../Page/銅鑼灣道.md "wikilink")
  - [浣紗街](../Page/浣紗街.md "wikilink")

### 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[天后站](../Page/天后站.md "wikilink")（沿[銅鑼灣道步行前往](../Page/銅鑼灣道.md "wikilink")）

<!-- end list -->

  - [香港電車](../Page/香港電車.md "wikilink")(沿銅鑼灣道步行前往[高士威道電車站](../Page/高士威道.md "wikilink")）
    [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  -
    以上只列出全日行走的巴士及小巴路線

</div>

</div>

## 區議會議席分佈

為方便比較，以下列表會包括大坑、[禮頓山](../Page/禮頓山.md "wikilink")、[加路連山](../Page/加路連山.md "wikilink")、[掃桿埔](../Page/掃桿埔.md "wikilink")、[渣甸山等範圍](../Page/渣甸山.md "wikilink")，即由[紀利華木球會](../Page/紀利華木球會.md "wikilink")[禮頓道交界起計](../Page/禮頓道.md "wikilink")[禮頓道](../Page/禮頓道.md "wikilink")、[高士威道以南至沿](../Page/高士威道.md "wikilink")[大坑道全段為範圍](../Page/大坑道.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>年度/範圍</p></th>
<th><p>2000-2003</p></th>
<th><p>2004-2007</p></th>
<th><p>2008-2011</p></th>
<th><p>2012-2015</p></th>
<th><p>2016-2019</p></th>
<th><p>2020-2023</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/連道.md" title="wikilink">連道</a>、<a href="../Page/樂活道.md" title="wikilink">樂活道沿線</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加路連山道.md" title="wikilink">加路連山道</a>、<a href="../Page/東院道.md" title="wikilink">東院道沿線</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/高士威道.md" title="wikilink">高士威道以南至</a><a href="../Page/大坑道.md" title="wikilink">大坑道</a><a href="../Page/香港真光中學.md" title="wikilink">香港真光中學為止</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大坑道.md" title="wikilink">大坑道自</a><a href="../Page/香港真光中學.md" title="wikilink">香港真光中學沿線</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

註：以上主要範圍尚有其他細微調整（包括編號），請參閱有關區議會選舉選區分界地圖及條目。

## 註釋

## 参考资料

## 參看

  - [灣仔區](../Page/灣仔區.md "wikilink")
  - [銅鑼灣](../Page/銅鑼灣.md "wikilink")
  - [跑馬地](../Page/跑馬地.md "wikilink")
  - [大坑明渠](../Page/大坑明渠.md "wikilink")

{{-}}

{{-}}

[大坑](../Category/大坑.md "wikilink")
[Category:銅鑼灣](../Category/銅鑼灣.md "wikilink")

1.
2.  [【動畫】大坑沒落》食街變鬼城 神秘鋪王掃37物業狂加租
    壹週刊 2016-08-10](http://nextplus.nextmedia.com/news/spot/20160810/417204)