**路易·布莱叶**（，，又译**布拉耶**，）是世界通用的[盲人及视觉障碍者使用的文字系统](../Page/盲人.md "wikilink")[布莱叶点字法的发明者](../Page/布莱叶点字法.md "wikilink")。布莱叶点字法是一种通过阅读者用手指触摸由突起的点组成的文字进行[阅读的方法](../Page/阅读.md "wikilink")。这种系统已经适用于几乎所有的已知[语言](../Page/语言.md "wikilink")。

## 生平

布莱叶出生在[法国](../Page/法国.md "wikilink")[巴黎东部的一个小镇](../Page/巴黎.md "wikilink")[考普瓦利](../Page/考普瓦利.md "wikilink")。他的父亲西蒙-瑞恩·布莱叶是一位马鞍匠。三岁时，布莱叶在他父親的工铺中試用缝纫锥時誤将左[眼戳伤](../Page/眼.md "wikilink")。这件事毁了他的左眼，不久右眼受感染，患上[神经性眼炎](../Page/神经性眼炎.md "wikilink")，也失明了。四岁时布莱叶已经彻底成为一个盲人。布莱叶虽已[残疾](../Page/残疾.md "wikilink")，但仍在父母的支持下坚持上学，被得到附近教堂一位神父的支持，令直到他能够读和写。

十岁时，布莱叶获得了去往巴黎学习的奖学金。此学院是世界上第一所该种类型的学校。这笔奖学金使他的命运不同于其他在巴黎街道乞讨的盲人。然而学校里的情况也好不了多少：他被供给变味的面包和水，学生们时常被殴打和关禁闭。

聪明并有创造力的布莱叶成为了一名出色的[大提琴演奏家和](../Page/大提琴.md "wikilink")[风琴演奏家](../Page/风琴.md "wikilink")，并为整个法国的教堂演奏。

在该学校里，学生们可学习到一些基础的[工艺技能和简单的](../Page/工艺.md "wikilink")[贸易知识](../Page/贸易.md "wikilink")。他们也学习了通过感觉突起字母（一种由该学校创建者[Valentin
Haüy发明的阅读系统](../Page/華倫泰·阿羽依.md "wikilink")）来进行阅读。然而，这种突起字母都由[铜丝在](../Page/铜.md "wikilink")[纸上压出](../Page/纸.md "wikilink")，因此学生们从未学习如何写出这种文字。

1821年，一名叫[夏爾·巴比尔的退伍海军军官访问了这所学校](../Page/夏爾·巴比尔.md "wikilink")。巴比尔向大家展示了他发明的[夜间书写文字代码](../Page/夜间书写.md "wikilink")。该系统通过12个凸点的不同排列表达意思，用于士兵在战场上不说话而传达高级机密。这种代码对一般的士兵来说都很难掌握，但布莱叶却迅速掌握了它。

[LouisBraille.png](https://zh.wikipedia.org/wiki/File:LouisBraille.png "fig:LouisBraille.png")
这一年，布莱叶开始使用他父亲的缝纫锥来发明他自己的凸点代码系统，他15岁时完成了发明工作。布莱叶的这种代码系统，即[布莱叶点字法](../Page/布莱叶点字法.md "wikilink")，仅用6个凸点来组成每一个相应的[字母](../Page/字母.md "wikilink")，而巴比尔的系统则是用12个点来表示相应的发音。这种六点的系统使得人们通过手指的触摸就能识别字母并一次理解所有点所代表的意思，并不需要移动或重置等12点法中必需的步骤。布莱叶的系统也具有众多优于Valentin
Haüy系统的特点，最显著的优点即是人们使用布莱叶点字法，既可读又可写。

后来布莱叶将他的代码系统推广至[数学和](../Page/数学.md "wikilink")[音乐符号](../Page/音乐.md "wikilink")。第一本使用布莱叶点字法的书籍是于1827年出版的《为盲人准备并供盲人使用，通过点来写字、谱曲、写简单的歌的方法》。1839年布莱叶发表了关于他发展的与有视力的人沟通的方法及规范布莱叶点字法印刷形式的一些细节。布莱叶与他的朋友皮埃尔·福柯又接着研制能够让大家更快地书写该种代码的器械。

布莱叶也成为了一名备受尊重的教师。他从盲人学校毕业后就一直留在该校任教。虽然他受到他的学生们的敬重和爱戴，但他的点字系统在他生前并没有被允许传授给那里的学生，因此很多学生在校外秘密向布莱叶学习。布莱叶备受病痛煎熬，1852年，43岁的他因肺结核逝世于巴黎；他的遗体于1952年被法国政府掘出并迁往用于安葬法国历史上重要伟人的巴黎[先贤祠](../Page/先贤祠.md "wikilink")。

## 遗产

布莱叶点字法的重要性直到1854年才被人们所鉴识。这年，[托马斯·阿米塔奇博士与四名盲人一起建立了](../Page/:W:en:Thomas_Armitage.md "wikilink")“不列颠及外国盲人浮雕文学促进会”（the
British and Foreign Society for Improving the Embossed Literature of the
Blind, 即后来的[皇家国家盲人协会](../Page/皇家国家盲人协会.md "wikilink")，[Royal National
Institute of the
Blind](../Page/:W:en:Royal_National_Institute_of_the_Blind.md "wikilink")），这个机构以布莱叶盲文出版书籍。

1879年在[柏林举行的](../Page/柏林.md "wikilink")[国际盲人教师代表大会上决定采用布莱叶点字法对盲人进行教学](../Page/国际盲人教师代表大会.md "wikilink")。1887年布莱叶点字法获国际公认。现在，布莱叶点字法已经适用于几乎所有的已知语言，并成为全世界视觉障碍者书面沟通的主要方法。

在考普瓦利有一条“布莱叶街”，村里有“布莱叶纪念馆”。路易·布莱叶本人也被当今世界各国人们所敬重。

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
## 延伸閱讀

  - [*La vie et l'oeuvre de Louis Braille: Inventeur de l'alphabet des
    aveugles
    (1809–1852)*](http://www.worldcat.org/title/vie-et-loeuvre-de-louis-braille-inventeur-de-lalphabet-des-aveugles-1809-1852/oclc/299733373/editions?editionsView=true&referer=br)
    by Pierre Henri. Paris: Pr. universit. de France (1952).

## 参见

  - [布莱叶点字法](../Page/布莱叶点字法.md "wikilink")
  - [无障碍](../Page/无障碍.md "wikilink")
  - [摩尔斯电码](../Page/摩尔斯电码.md "wikilink")

## 外部链接

  - [路易·布莱叶传记——美国盲人基金会](http://www.afb.org/braillebug/louis_braille_bio.asp)

  - [路易·布莱叶的人生——皇家国家盲人协会](http://www.rnib.org.uk/xpedio/groups/public/documents/publicwebsite/public_braille.hcsp)

  - [路易·布莱叶传记](http://www.braillerman.com/louis.htm)

  - [布莱叶点字法字母表](http://www.nbp.org/ic/nbp/braille/index.html)

  - [大英百科全书中的路易·布莱叶条目](http://search.eb.com/eb/article-9016177)

  - [路易斯·布莱叶——CCTV.com](http://www.cctv.com/program/dssgsw/20060327/102004.shtml)

[Category:法国盲人](../Category/法国盲人.md "wikilink")
[Category:法国发明家](../Category/法国发明家.md "wikilink")
[Category:葬於先賢祠](../Category/葬於先賢祠.md "wikilink")
[Category:19世纪死于肺结核](../Category/19世纪死于肺结核.md "wikilink")