[Casio_G-SHOCK_MR-G_The_G_MRG-7100BJ-1AJF02s5.jpg](https://zh.wikipedia.org/wiki/File:Casio_G-SHOCK_MR-G_The_G_MRG-7100BJ-1AJF02s5.jpg "fig:Casio_G-SHOCK_MR-G_The_G_MRG-7100BJ-1AJF02s5.jpg")系列MR-G
MRG-7100BJ-1AJF\]\]
[Casio_Mudman_3031.jpg](https://zh.wikipedia.org/wiki/File:Casio_Mudman_3031.jpg "fig:Casio_Mudman_3031.jpg")

**G-Shock**，是[日本](../Page/日本.md "wikilink")[卡西欧所生产的一個](../Page/卡西欧.md "wikilink")[手表](../Page/手表.md "wikilink")[品牌](../Page/品牌.md "wikilink")，亦為當中最知名的品牌，強調耐衝擊，適合於[運動](../Page/運動.md "wikilink")、[戶外探險以至於](../Page/戶外.md "wikilink")[軍事等場合用途](../Page/軍事.md "wikilink")。自1983年問世至2014年，G-Shock於全世界共出售達7,000萬隻。

## 主要系列

  - G steel
  - MR-G 顶级全[金属系列](../Page/金属.md "wikilink")
  - MT-G 顶级[商务系列](../Page/商务.md "wikilink")
  - GIEZ 顶级潮流系列
  - G-COOL 顶级概念系列
  - STANDARD 标准系列
  - COCKPIT [赛车计时系列](../Page/赛车.md "wikilink")
  - BABY-G [女士系列](../Page/女士.md "wikilink")
  - FROGMAN（フロッグマン） [蛙人系列](../Page/蛙人.md "wikilink")（老款停产，新款在售）
  - MUDMAN（マッドマン） 泥人系列（老款停产，新款在售）
  - FISHERMAN（フィッシャーマン） [渔人系列](../Page/渔人.md "wikilink")（停产）
  - RANGEMAN
  - RISEMAN（ライズマン） 飞人系列（老款停产，新款在售）
  - GAUSSMAN（ガウスマン） 磁人系列（停产）
  - RAYSMAN（レイズマン） 光人系列（停产）
  - WADEMAN（ウェイドマン） 渡人系列（停产）
  - GULFMAN（ガルフマン） 湾人系列（老款停产，新款在售）
  - REVMAN（レブマン） 旋人系列（停产）
  - LUNGMAN（ラングマン） 肺人系列（停产）
  - ANTMAN（アントマン） 波人系列（停产）
  - SKYFORCE（スカイフォース） [空军系列](../Page/空军.md "wikilink")（停产）
  - FOX FIRE（フォックスファイア） 狐火系列（日本本土代号）
  - nexax（ネグザクス） 概念系列（停产）
  - CODE NAME（コードネーム） 代号系列（停产）
  - X-treme（エクストリーム） [极限系列](../Page/极限.md "wikilink")（停产）
  - G-LIDE（Gライド） [冲浪系列](../Page/冲浪.md "wikilink")（老款停产，新款在售）
  - G’MIX（Gミックス） [音乐系列](../Page/音乐.md "wikilink")（停产）
  - TACTICIAN（タクティシャン） [技师系列](../Page/技师.md "wikilink")（停产）
  - SILENCER（サイレンサー） [消音系列](../Page/消音.md "wikilink")（老款停产，新款在售）

## 歷史

G-Shock由[日本](../Page/日本.md "wikilink")[工程師伊部菊雄設計](../Page/工程師.md "wikilink")，首隻於1983年問世\[1\]。

2014年，G-Shock推出了首個女性系列──S，性能與男性系列一樣，差別在於錶殼尺寸縮小，而且顏色款式更加豐富，並且增添時尚風格\[2\]。

## 參考注釋

## 外部链接

  - [卡西欧 G-Shock官方网站 (繁體中文)](http://www.casio.com.tw/g-shock//)
  - [卡西欧 G-Shock官方网站 (日文)](http://www.gshock.jp/)
  - [卡西欧 G-Shock官方网站 (英文)](http://www.gshock.com/)

[Category:1983年面世的产品](../Category/1983年面世的产品.md "wikilink")
[Category:卡西歐產品](../Category/卡西歐產品.md "wikilink")
[Category:時鐘](../Category/時鐘.md "wikilink")

1.  [G-Shock無懼競爭](http://hk.apple.nextmedia.com/financeestate/art/20141120/18941194)
    《蘋果日報》 2014年11月20日
2.  [G-Shock推女錶
    內衣辣模助陣](http://www.appledaily.com.tw/realtimenews/article/new/20140803/445050/)
    《蘋果日報》 2014年8月3日