**肯頓县**（**Kenton County,
Kentucky**）是位於[美國](../Page/美國.md "wikilink")[肯塔基州北部的一個縣](../Page/肯塔基州.md "wikilink")，北隔[俄亥俄河與](../Page/俄亥俄河.md "wikilink")[俄亥俄州相望](../Page/俄亥俄州.md "wikilink")。面積426平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口151,464人。縣治[科溫頓](../Page/科溫頓_\(肯塔基州\).md "wikilink")
（Covington）和[獨立城](../Page/獨立城_\(肯塔基州\).md "wikilink") （Independence）。

成立於1840年1月29日。縣名紀念拓荒者、民兵領袖[西門·肯頓](../Page/西門·肯頓.md "wikilink") （Simon
Kenton）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[K](../Category/肯塔基州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.