在[物理學裏](../Page/物理學.md "wikilink")，**作用量**（英语：**action**）是一個很特別、很抽象的[物理量](../Page/物理量.md "wikilink")。它表示著一個[動力物理系統內在的演化趨向](../Page/動力學.md "wikilink")。雖然與[微分方程式方法大不相同](../Page/微分方程式.md "wikilink")，作用量也可以被用來分析物理系統的運動，所得到的答案是相同的。只需要設定系統在兩個點的狀態，初始狀態與最終狀態，然後，經過求解作用量的[平穩值](../Page/平穩值.md "wikilink")，就可以得到系統在兩個點之間每個點的狀態。

## 歷史

[皮埃爾·德·費馬於](../Page/皮埃爾·德·費馬.md "wikilink")1662年發表了[費馬原理](../Page/費馬原理.md "wikilink")。這原理闡明：光傳播的正確路徑，所需的時間必定是[極值](../Page/極值.md "wikilink")。這原理在物理學界造成了很大的震撼。不同於[牛頓運動定律的機械性](../Page/牛頓運動定律.md "wikilink")，現今，一個物理系統的運動擁有了展望與目標。

[戈特弗里德·萊布尼茨不同意費馬的理論](../Page/戈特弗里德·萊布尼茨.md "wikilink")。他認為光應該選擇最容易傳播的路徑。他於1682年發表了他的理論：光傳播的正確路徑應該是阻礙最小的路徑；更精確地說，阻礙與徑長的乘積是最小值的路徑。這理論有一個難題，如果要符合實驗的結果，玻璃的阻礙必須小於空氣的阻礙；但是，玻璃的密度大於空氣，應該玻璃的阻礙會大於空氣的阻礙。萊布尼茨為此提供了一個令人百思的辯解。較大的阻礙使得光較不容易[擴散](../Page/擴散.md "wikilink")；因此，光被約束在一個很窄的路徑內。假若，河道變窄，水的流速會增加；同樣地，光的路徑變窄，所以光的速度變快了。

1744年，[皮埃爾·莫佩爾蒂在一篇論文](../Page/皮埃爾·莫佩爾蒂.md "wikilink")《The agreement
between the different laws of Nature that had, until now,seemed
incompatiable》中，發表了[最小作用量原理](../Page/最小作用量原理.md "wikilink")：光選擇的傳播路徑，作用量最小。他定義作用量為移動速度與移動距離的乘積。用這原理，他證明了費馬原理：光傳播的正確路徑，所需的時間是[極值](../Page/極值.md "wikilink")；他也計算出光在[反射與同](../Page/反射.md "wikilink")[介質傳播時的正確路徑](../Page/介質.md "wikilink")。1747年，莫佩爾蒂在另一篇論文《On
the laws of motion and of
rest》中，應用這原理於[碰撞](../Page/碰撞.md "wikilink")，正確地分析了彈性碰撞與非弹性碰撞；這兩種碰撞不再需要用不同的理論來解釋。

[萊昂哈德·歐拉在同年發表了一篇論文](../Page/萊昂哈德·歐拉.md "wikilink")《Method for finding
curves having a minimal or maximal property or solutions to
isoperimetric problems in the broadest accepted sense》
；其中，他表明物體的運動遵守某種物理量極值定律，而這物理量是\(\int_{path}\ v^2\ dt\,\!\)。應用這理論，歐拉成功的計算出，當粒子受到[連心力作用時](../Page/連心力.md "wikilink")，正確的拋射體運動。

在此以後，許多物理學家，包括[約瑟夫·拉格朗日](../Page/約瑟夫·拉格朗日.md "wikilink")、[威廉·哈密頓](../Page/威廉·哈密頓.md "wikilink")、[理查德·費曼等等](../Page/理查德·費曼.md "wikilink")，對於作用量都有很不同的見解。這些見解對於物理學的發展貢獻甚多。

## 概念

微分方程式時常被用來表述物理定律。微分方程式指定出，隨著極小的時間、位置、或其他變數的變化，一個物理變數如何改變。總合這些極小的改變，再加上這物理變數在某些點的已知數值或已知導數值，就能求得物理變數在任何點的數值。

作用量方法是一種全然不同的方法，它能夠描述物理系統的運動，而且只需要設定物理變數在兩點的數值，稱為初始值與最終值。經過作用量平穩的演算，可以得到，此變數在這兩點之間任何點的數值。而且，作用量方法與微分方程式方法所得到的答案完全相同。

[哈密頓原理闡明了這兩種方法在物理學價位的等價](../Page/哈密頓原理.md "wikilink")：描述物理系統運動的[微分方程式](../Page/微分方程式.md "wikilink")，也可以用一個等價的[積分方程式來描述](../Page/積分方程式.md "wikilink")。無論是關於[經典力學中的一個單獨粒子](../Page/經典力學.md "wikilink")、關於[經典場像](../Page/場.md "wikilink")[電磁場或](../Page/電磁場.md "wikilink")[重力場](../Page/重力場.md "wikilink")，這描述都是正確的。更加地，哈密頓原理已經延伸至[量子力學與](../Page/量子力學.md "wikilink")[量子場論了](../Page/量子場論.md "wikilink")。

用[變分法數學語言來描述](../Page/變分法.md "wikilink")，求解一個物理系統作用量的[平穩值](../Page/平穩值.md "wikilink")（通常是最小值），可以得到這系統隨時間的演化（就是說，系統怎樣從一個狀態演化到另外一個狀態）。更廣義地，系統的正確演化對於任何[微擾必須是](../Page/微擾理論.md "wikilink")[平穩的](../Page/駐點.md "wikilink")。這要求導致出描述正確演化的微分方程式。

## 作用量形式

在經典物理裏，作用量這術語至少有七種不同的意義。每一種不同的意義有它不同的表達形式。

### 作用量（泛函）

最常見的作用量是一個[泛函](../Page/泛函.md "wikilink")\(\mathcal{S}\,\!\)，輸入是參數為時間與空間的[函數](../Page/函數.md "wikilink")，輸出是一個[純量](../Page/純量.md "wikilink")。在經典力學裏，輸入函數是物理系統在兩個時間點\(t_{1}\,\!\)，\(t_{2}\,\!\)之間[廣義座標](../Page/廣義座標.md "wikilink")\(\mathbf{q}(t)\,\!\)的演變。

作用量\(\mathcal{S}\,\!\)定義為，在兩個時間點之間，系統的[拉格朗日量](../Page/拉格朗日量.md "wikilink")\(L\,\!\)對於時間的積分：

\[\mathcal{S}[\mathbf{q}(t)] = \int_{t_1}^{t_2} L[\mathbf{q},\ \dot{\mathbf{q}},\ t]\, \mathrm{d}t\,\!\]。

根據[哈密頓原理](../Page/哈密頓原理.md "wikilink")，正確的演化\(\mathbf{q}_{\mathrm{true}}(t)\,\!\)要求[平穩的作用量](../Page/駐點.md "wikilink")\(\mathcal{S}\,\!\)（最小值、最大值、[鞍值](../Page/鞍點.md "wikilink")）。經過運算，結果就是[拉格朗日方程式](../Page/拉格朗日方程式.md "wikilink")。

### 簡略作用量（泛函）

**簡略作用量**也是一個泛涵，通常標記為\(\mathcal{S}_{0}\,\!\)。這裏，輸入函數是物理系統移動的一條路徑，完全不考慮時間參數。舉例而言，一個行星軌道的路徑是個橢圓，一個粒子在均勻重力場的路徑是拋物線；在這兩種狀況，路徑都跟粒子的移動速度無關。簡略作用量\(\mathcal{S}_{0}\,\!\)定義為[廣義動量](../Page/廣義動量.md "wikilink")\(\mathbf{p}\,\!\)延著路徑的積分：

\[\mathcal{S}_{0} = \int \mathbf{p}\,\mathrm{d}\mathbf{q}\,\!\]；

其中，\(\mathbf{q}\,\!\)是廣義座標．根據[莫佩爾蒂原理](../Page/莫佩爾蒂原理.md "wikilink")，正確路徑的簡略作用量\(\mathcal{S}_{0}\,\!\)是平穩的。

### 哈密頓主函數

  -
    主條目：[哈密頓主函數](../Page/哈密頓-雅可比方程式.md "wikilink")。

**哈密頓主函數**是由哈密頓-雅可比方程式定義的。哈密頓-雅可比方程式是經典力學的另一種表述。哈密頓主函數\(S\,\!\)與泛涵\(\mathcal{S}\,\!\)有密切的關係。固定住初始時間\(t_{1}\,\!\)和其對應的座標點\(\mathbf{q}_{1}\,\!\)；而准許時間上限\(t_{2}\,\!\)和其對應的座標點\(\mathbf{q}_{2}\,\!\)的改變。取\(t_{2}\,\!\)和\(\mathbf{q}_{2}\,\!\)為函數\(S\,\!\)的參數。換句話說，作用量函數\(S\,\!\)是[拉格朗日量對於時間的](../Page/拉格朗日量.md "wikilink")[不定積分](../Page/不定積分.md "wikilink")：

\[S(\mathbf{q},\ \mathbf{P},\ t) = \int L[\mathbf{q},\ \dot{\mathbf{q}},\ t]\, \mathrm{d}t\,\!\]。

更加地，可以證明\(\mathbf{P}\,\!\)是某常數向量\(\mathbf{a}\,\!\)。所以，

\[S(\mathbf{q},\ \mathbf{P},\ t) = S(\mathbf{q},\ \mathbf{a},\ t)\,\!\]。

### 哈密頓特徵函數

  -
    主條目：[哈密頓特徵函數](../Page/哈密頓-亞可比方程式.md "wikilink")。

假若，[哈密頓量](../Page/哈密頓量.md "wikilink")\(H\,\!\)是守恆的；

\[H=\alpha\,\!\]；

其中，\(\alpha\,\!\)是常數。

設定**哈密頓特徵函數**\(W\,\!\)為

\[W(\mathbf{q},\ \mathbf{a}) = S(\mathbf{q},\ \mathbf{a},\ t) - \alpha t\,\!\]。

則哈密頓特徵函數\(W\,\!\)是一個作用量。

更加地，

\[\frac{dW}{dt}=\frac{\partial W}{\partial \mathbf{q}}\dot{\mathbf{q}}=\mathbf{p}\dot{\mathbf{q}}\,\!\]。

對於時間積分：

\[W(\mathbf{q},\ \mathbf{a})=\int\mathbf{p}\dot{\mathbf{q}}dt=\int \mathbf{p}\,d\mathbf{q}\,\!\]。

這正是[簡略作用量的方程式](../Page/#簡略作用量_\(泛函\).md "wikilink")。

### 哈密頓-雅可比方程式解答

  -
    主條目：[哈密頓-雅可比方程式](../Page/哈密頓-雅可比方程式.md "wikilink")。

[哈密頓-雅可比方程式是經典力學的一種表述](../Page/哈密頓-雅可比方程式.md "wikilink")。假若，哈密頓-雅可比方程式是完全可分的；則哈密頓主函數\(S(\mathbf{q},\ \mathbf{P},\ t)\,\!\)分出的每一個項目\(S_{k}(q_{k},\ \mathbf{P},\ t)\,\!\)也稱為"作用量"。

### 作用量-角度座標

  -
    主條目：[作用量-角度座標](../Page/作用量-角度座標.md "wikilink")。思考一個[作用量-角度座標的廣義動量變數](../Page/作用量-角度座標.md "wikilink")\(J_{k}\,\!\)，定義為在[相空間內](../Page/相空間.md "wikilink")，關於轉動運動或振蕩運動，廣義動量的[閉路徑積分](../Page/路徑積分.md "wikilink")：
    \(J_{k} = \oint p_{k} \mathrm{d}q_{k}\,\!\)。

這變數\(J_{k}\,\!\)稱為廣義座標\(q_{k}\,\!\)的作用量；相應的[正則座標是](../Page/正則座標.md "wikilink")**角度**\(w_{k}\,\!\)。不同於前面簡略作用量泛函地用點積來積分向量；這裏，只有一個純量變數\(q_{k}\,\!\)被用來積分。作用量\(J_{k}\,\!\)等於，隨著\(q_{k}\,\!\)沿著閉路徑，\(S_{k}(q_{k})\,\!\)的改變。應用於幾個有趣的物理系統，\(J_{k}\,\!\)或者是常數，或者改變非常地慢。因此，\(J_{k}\,\!\)時常應用於[微擾理論與](../Page/微擾理論.md "wikilink")[緩漸不變量的研究](../Page/緩漸不變量.md "wikilink")。

### 哈密頓流作用量

參閱[重言1形式](../Page/重言1形式.md "wikilink")。

## 數學導引

哈密頓原理闡明，如果一個物理系統在兩個時間點\(t_{1}\,\!\)、\(t_{2}\,\!\)的運動是正確運動，則作用量[泛函](../Page/泛函.md "wikilink")\(\mathcal{S}\,\!\)的[一次變分](../Page/一次變分.md "wikilink")\(\delta\mathcal{S}\,\!\)為零。用數學方程式表示，定義作用量為

\[\mathcal{S}\ \stackrel{\mathrm{def}}{=}\  \int_{t_{1}}^{t_{2}} L(\mathbf{q},\ \dot{\mathbf{q}},\ t)\,dt\,\!\]。

其中，\(L(\mathbf{q},\ \dot{\mathbf{q}},\ t)\,\!\)是系統的[拉格朗日函數](../Page/拉格朗日函數.md "wikilink")，[廣義座標](../Page/廣義座標.md "wikilink")\(\mathbf{q} = \left(q_{1},\ q_{2},\ \ldots,\ q_{N}\right)\,\!\)是時間的函數。

假若，\(\mathbf{q}(t)\,\!\)乃系統的正確運動，則\(\delta \mathcal{S}=0\,\!\)。

從哈密頓原理可以導引出拉格朗日方程式．假設\(\mathbf{q}(t)\,\!\)是系統的正確運動，讓\(\boldsymbol\varepsilon (t)\,\!\)成為一個微擾\(\delta\mathbf{q}\,\!\)；微擾在軌道兩個端點的值是零：

\[\boldsymbol\varepsilon(t_{1})=\boldsymbol\varepsilon(t_{2})\ \stackrel{\mathrm{def}}{=}\ 0\,\!\]。

取至\(\boldsymbol\varepsilon (t)\,\!\)的一階微擾，作用量泛函的[一次變分為](../Page/一次變分.md "wikilink")

\[\delta \mathcal{S} = \int_{t_{1}}^{t_{2}}\;
\left[ L(\mathbf{q}+\boldsymbol{\varepsilon},\ \dot\mathbf{q} +\dot\boldsymbol{\varepsilon}) - L(\mathbf{q},\ \dot\mathbf{q})\right]dt = \int_{t_{1}}^{t_{2}}\; \left(
\boldsymbol\varepsilon \cdot \frac{\partial L}{\partial \mathbf{q}} +
\dot\boldsymbol\varepsilon \cdot \frac{\partial L}{\partial \dot\mathbf{q}}  \right)\,dt
\,\!\]。

這裏，將拉格朗日量\(L\,\!\)展開至\(\boldsymbol\varepsilon (t)\,\!\)的一階微擾。

應用[分部積分法於最右邊項目](../Page/分部積分法.md "wikilink")，

\[\delta \mathcal{S} =
\left[ \boldsymbol\varepsilon \cdot \frac{\partial L}{\partial \dot\mathbf{q}}\right]_{t_{1}}^{t_{2}} + \int_{t_{1}}^{t_{2}}\;
\left(\boldsymbol\varepsilon \cdot \frac{\partial L}{\partial \mathbf{q}}
- \boldsymbol\varepsilon \cdot \frac{d}{dt} \frac{\partial L}{\partial \dot\mathbf{q}} \right)\,dt\,\!\]。

邊界條件\(\boldsymbol\varepsilon(t_{1}) = \boldsymbol\varepsilon(t_{2})\ \stackrel{\mathrm{def}}{=}\  0\,\!\)使第一個項目歸零。所以，

\[\delta \mathcal{S} = \int_{t_{1}}^{t_{2}}\; \boldsymbol\varepsilon \cdot
\left(\frac{\partial L}{\partial \mathbf{q}} - \frac{d}{dt} \frac{\partial L}{\partial \dot\mathbf{q}} \right)\,dt\,\!\]。

要求作用量泛函\(\mathcal{S}\,\!\)平穩。這意味著，對於正確運動的任意微擾\(\boldsymbol\varepsilon (t)\,\!\)，一次變分\(\delta \mathcal{S}\,\!\)必須等於零：

\[\delta \mathcal{S} = \int_{t_{1}}^{t_{2}}\; \boldsymbol\varepsilon \cdot
\left(\frac{\partial L}{\partial \mathbf{q}} - \frac{d}{dt} \frac{\partial L}{\partial \dot\mathbf{q}} \right)\,dt=0\,\!\]。

請注意，還沒有對廣義座標\(\mathbf{q}(t)\,\!\)做任何要求。現在，要求所有的廣義座標都互相無關（[完整限制](../Page/完整系統.md "wikilink")）。這樣，根據[變分法基本引理](../Page/變分法基本引理.md "wikilink")，可以得到拉格朗日方程式：

\[\frac{\partial L}{\partial \mathbf{q}} -
\frac{d}{dt}\frac{\partial L}{\partial \dot\mathbf{q}} = \mathbf{0}\,\!\]。

在各個物理學領域，拉格朗日方程式都被認為是非常重要的方程式，能夠用來精確地理論分析許多物理系統。

對應於[廣義座標](../Page/廣義座標.md "wikilink")\(q_{k}\,\!\)的[廣義動量](../Page/廣義動量.md "wikilink")\(p_{k}\,\!\)，又稱為**共軛動量**，定義為

\[p_{k} \ \stackrel{\mathrm{def}}{=}\ \frac{\partial L}{\partial\dot q_{k}}\,\!\]。

假設\(L\,\!\)不顯性地跟廣義座標\(q_{k}\,\!\)有關，

  -
    \(\frac{\partial L}{\partial q_{k}}=0\,\!\)，

則廣義動量\(p_{k} \ \stackrel{\mathrm{def}}{=}\  \frac{\partial L}{\partial\dot q_{k}}\,\!\)是常數。在此種狀況，座標\(q_{k}\,\!\)稱為**循環座標**。舉例而言，如果用[極座標系](../Page/極座標系.md "wikilink")\((r,\ \theta,\ h)\,\!\)來描述一個粒子的平面運動，而\(L\,\!\)與\(\theta\,\!\)無關，則廣義動量是守恆的[角動量](../Page/角動量.md "wikilink")。

## 參閱

  - [拉格朗日力學](../Page/拉格朗日力學.md "wikilink")
  - [哈密頓力學](../Page/哈密頓力學.md "wikilink")
  - [諾特定理](../Page/諾特定理.md "wikilink")
  - [愛因斯坦-希爾伯特作用量](../Page/愛因斯坦-希爾伯特作用量.md "wikilink")
  - [最小作用量原理](../Page/最小作用量原理.md "wikilink")

## 外部連結

  - [1](http://www.eftaylor.com/pub/BibliogLeastAction12.pdf)，Edwin F.
    Taylor加了註釋的參考書目。
  - [最小作用量原理](http://www.eftaylor.com/software/ActionApplets/LeastAction.html)非常好地互動解釋。

## 參考文獻

  - Cornelius Lanczos, "The Variational Principles of Mechanics",（Dover
    Publications, New York, 1986）, ISBN 0-486-65067-7.這領域最常引用的參考書。
  - [列夫·朗道and](../Page/列夫·朗道.md "wikilink") E. M. Lifshitz, "Mechanics,
    Course of Theoretical Physics", 3rd ed., Vol.
    1,（Butterworth-Heinenann, 1976）, ISBN
    0-7506-2896-0.這本書一開始就講解最小作用量原理。
  - Herbert Goldstein "Classical Mechanics", 2nd ed.,（Addison Wesley,
    1980）, pp. 35-69。
  - Thomas A. Moore "Least-Action Principle" in Macmillan Encyclopedia
    of Physics, Volume 2,（Simon & Schuster Macmillan, 1996）, ISBN
    0-02-897359-3, , pages 840–842。
  - Robert Weinstock, "Calculus of Variations, with Applications to
    Physics and Engineering",（Dover Publications, 1974）, ISBN
    0-486-63069-2。非常好的古早書。
  - Dugas, René, "A History of Mechanics",（Dover, 1988）, ISBN
    0-486-65632-2, pp. 254-275。

[Z](../Category/基本物理概念.md "wikilink") [Z](../Category/力學.md "wikilink")
[Z](../Category/經典力學.md "wikilink")
[Z](../Category/拉格朗日力學.md "wikilink")
[Z](../Category/哈密顿力学.md "wikilink")
[Z](../Category/變分法.md "wikilink")