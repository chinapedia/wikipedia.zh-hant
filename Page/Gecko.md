**Gecko**是一套[自由及開放原始碼](../Page/自由及開放原始碼軟體.md "wikilink")、以[C++編寫的](../Page/C++.md "wikilink")[排版引擎](../Page/排版引擎.md "wikilink")，目前為[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")[網頁瀏覽器以及](../Page/網頁瀏覽器.md "wikilink")[Mozilla
Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink")[電子郵件客戶端等](../Page/電子郵件客戶端.md "wikilink")[Mozilla基金會相關產品所使用](../Page/Mozilla基金會.md "wikilink")。Gecko原本由[網景通訊公司開發](../Page/網景通訊公司.md "wikilink")，現在則由Mozilla基金會維護。

這套排版引擎提供了一個豐富的[應用程式介面以供](../Page/應用程式介面.md "wikilink")[網際網路相關的](../Page/網際網路.md "wikilink")[應用程式使用](../Page/應用程式.md "wikilink")，例如：[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")、[HTML编辑器](../Page/HTML编辑器.md "wikilink")、[客戶端](../Page/客戶端.md "wikilink")／[伺服器等等](../Page/伺服器.md "wikilink")\[1\]。雖然最初的主要使用對象為Netscape和Mozilla
Firefox網頁瀏覽器，但現在已有很多其他軟體在使用這個排版引擎。Gecko是[跨平台的](../Page/跨平台.md "wikilink")，能在[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")、[Linux和](../Page/Linux.md "wikilink")[Mac
OS
X等主要](../Page/Mac_OS_X.md "wikilink")[作業系統上運行](../Page/作業系統.md "wikilink")。

## 發展史

1997年，[網景領航員在各方面的表現已經比不上它的主要競爭對手](../Page/網景領航員.md "wikilink")[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")，這包括程式的執行速度、對[W3C標準的支援度等等](../Page/W3C.md "wikilink")。於是，網景收購了[DigitalStyle](../Page/DigitalStyle.md "wikilink")，開始研發下一代的排版引擎，並期望把新的排版引擎應用於下一版本的網景瀏覽器上。

1998年初，這個新的排版引擎名為Raptor，以開放原始碼的方式發放於網路上。後來，因為[商標問題](../Page/商標.md "wikilink")，Raptor改名為NGLayout（即Next
Generation
Layout之意）。而最後NGLayout被網景市場部門重新命名為Gecko。但由於Gecko為網景公司的商標，所以有一段時期[Mozilla.org](../Page/Mozilla.org.md "wikilink")（網景成立的非正式組織，Mozilla基金會前身）以NGLayout來稱呼這個新的排版引擎\[2\]。

1998年10月，網景公佈下一版的瀏覽器將會使用這個排版引擎，而該瀏覽器亦需要被大幅度重寫。對於致力推動網路標準的人，這是一個令人振奮的-{消息}-。然而，對於網景開發者而言，這是一個長達六個月的大工程\[3\]，而他們在[Netscape
5.0上](../Page/Netscape_5.0.md "wikilink")（包括舊有的[Mariner排版引擎](../Page/Mariner_\(排版引擎\).md "wikilink")）所花的心血亦被白白浪費，結果導致採用Gecko引擎的[Netscape
6.0在](../Page/Netscape_6.0.md "wikilink")2000年11月才被正式發佈。

隨著Gecko排版引擎的開發，越來越多應用程式開始使用它。[美國線上作為網景的母公司](../Page/美國線上.md "wikilink")，終於在[CompuServe](../Page/CompuServe.md "wikilink")
7.0和AOL for Mac OS X上使用了Gecko。可惜，Windows版的AOL瀏覽器始終沒有使用過Gecko。

2003年7月15日，美國線上解散了網景公司，大部分網景開發者被解雇。而Mozilla基金會亦在當天成立，繼續推動著Gecko的發展。直到今天，Gecko排版引擎繼續由Mozilla員工和社群所維護和發展。

## 標準支援

  - [CSS](../Page/CSS.md "wikilink") Level 2.1（支援部份CSS 3）\[4\]
  - [DOM](../Page/DOM.md "wikilink") Level 1和2（支援部份DOM 3）
  - [HTML](../Page/HTML.md "wikilink")
    4.01（支援部分[HTML5](../Page/HTML5.md "wikilink")）
  - [JavaScript](../Page/JavaScript.md "wikilink") 1.8.5（完全支援ECMAScript
    5.1\[5\]）由[SpiderMonkey實現](../Page/SpiderMonkey.md "wikilink")
  - [MathML](../Page/MathML.md "wikilink")
  - [RDF](../Page/資源描述框架.md "wikilink")
  - [XForms](../Page/XForms.md "wikilink")（藉由官方的擴充套件）
  - [XHTML](../Page/XHTML.md "wikilink") 1.0
  - [XML](../Page/XML.md "wikilink") 1.0
  - [XSLT和](../Page/XSLT.md "wikilink")[XPath由TransforMiiX實現](../Page/XPath.md "wikilink")
  - [SVG](../Page/SVG.md "wikilink")（支援部份SVG 1.1）\[6\]\[7\]

Gecko將會繼續支援更多的網路標準，例如：[XForms和](../Page/XForms.md "wikilink")[SVG](../Page/SVG.md "wikilink")。Mozilla基金會身為[WHATWG組織的成員](../Page/WHATWG.md "wikilink")，Gecko和其他排版引擎將會率先支援WHATWG所定下的規格，例如：可供繪畫的[Canvas](../Page/Canvas_\(HTML元素\).md "wikilink")。

Gecko的繪圖引擎在1.9版有重大的改變。它使用跨平台的[Cairo繪圖引擎來取代作業平台的繪圖介面](../Page/Cairo_\(繪圖\).md "wikilink")。這個改變將會令Gecko擁有更佳的繪圖能力。而加上的話，更可利用3D硬體加速。而所有多媒體內容（如HTML／CSS、Canvas、SVG等）將可使用同一管道作出渲染，[SVG的特效亦可以應用於](../Page/SVG.md "wikilink")[HTML上](../Page/HTML.md "wikilink")。因為使用[Cairo的關係](../Page/Cairo_\(绘图\).md "wikilink")，圖像亦可以被輸出作[PNG和](../Page/PNG.md "wikilink")[PDF](../Page/PDF.md "wikilink")，亦有可能達成「另存本頁為PDF」的功能。

## 使用

Gecko主要用於[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")，最早使用於[Netscape
6和](../Page/Netscape_6.md "wikilink")[Mozilla
Suite](../Page/Mozilla_Suite.md "wikilink")（後來改名為[SeaMonkey](../Page/SeaMonkey.md "wikilink")）。Mozilla還在其它瀏覽器衍生產品使用它，如[Firefox和](../Page/Firefox.md "wikilink")[Firefox行動版](../Page/Firefox_for_Android.md "wikilink")。Mozilla也在其[Thunderbird電子郵件客戶端使用](../Page/Thunderbird.md "wikilink")。

使用Gecko的其他網頁瀏覽器包括Airfox、[Waterfox](../Page/Waterfox.md "wikilink")、[K-Meleon](../Page/K-Meleon.md "wikilink")、[Lunascape](../Page/Lunascape.md "wikilink")、[Pale
Moon](../Page/蒼月瀏覽器.md "wikilink")、[Firefox
Portable](../Page/Firefox_Portable.md "wikilink")、、、 、HP Secure Web
Browser，Oxygen和Sylera（用於行動裝置）。

其他使用Gecko的產品包括、Oxygen、、[Instantbird和Google的圖片管理軟體](../Page/Instantbird.md "wikilink")[Picasa](../Page/Picasa.md "wikilink")（用於Linux）\[8\]。

是用於API文件的GTK+/GNOME瀏覽器，使用Gecko來渲染文件\[9\]。

Gecko也被[Sugar用於](../Page/Sugar_\(用戶界面\).md "wikilink")[OLPC
XO-1電腦](../Page/XO-1.md "wikilink")\[10\]。

歷史上使用Gecko的產品包括[Songbird](../Page/Songbird.md "wikilink")、[Epiphany](../Page/Web_\(瀏覽器\).md "wikilink")（現在稱為GNOME
Web，使用WebKitGTK+）、[Sunbird](../Page/Mozilla_Sunbird.md "wikilink")，以及其他網頁瀏覽器，包括、[Flock](../Page/Flock.md "wikilink")、[Galeon](../Page/Galeon.md "wikilink")、[Camino](../Page/Camino.md "wikilink")、[Minimo](../Page/Minimo.md "wikilink")、[Beonex
Communicator](../Page/Beonex_Communicator.md "wikilink")、和。

在Netscape時代，由於糟糕的技術和管理決策導致了Gecko的\[11\]\[12\]\[13\]。因此，2001年，[Apple選擇了](../Page/蘋果公司.md "wikilink")[KHTML分支而不是Gecko](../Page/KHTML.md "wikilink")，來為其[Safari瀏覽器建構](../Page/Safari.md "wikilink")[WebKit引擎](../Page/WebKit.md "wikilink")\[14\]\[15\]。然而，到2008年，Mozilla已經解決了一些膨脹問題，導致Gecko的性能大幅的提升\[16\]。

## 參考資料

## 外部連結

  - [Gecko - Mozilla Developer
    Network](https://developer.mozilla.org/en/docs/Gecko)
  - [Gecko Wiki](https://wiki.mozilla.org/Gecko:Home_Page)
  - [Glimpse Of The
    Future](http://weblogs.mozillazine.org/roc/archives/2005/04/glimpse_of_the.html)
  - [Mozilla2:GFXEvolution](https://wiki.mozilla.org/Mozilla2:GFXEvolution)

## 參見

  - [排版引擎列表](../Page/排版引擎列表.md "wikilink")

  -
[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:Mozilla](../Category/Mozilla.md "wikilink")
[Category:Gecko衍生軟體](../Category/Gecko衍生軟體.md "wikilink")
[Category:自由排版引擎](../Category/自由排版引擎.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:使用Cairo的軟體](../Category/使用Cairo的軟體.md "wikilink")

1.

2.

3.

4.

5.

6.  The SVG font, color profile, animation, view, and cursor modules are
    yet to be implemented and the filter and text modules are only
    partially implemented. The extensibility module is also implemented
    but is currently disabled

7.

8.

9.

10.

11.

12.
13.

14.

15.
16.