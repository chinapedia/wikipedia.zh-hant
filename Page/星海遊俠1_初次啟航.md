是[東星軟體開發](../Page/東星軟體.md "wikilink")，[史克威爾艾尼克斯於](../Page/史克威爾艾尼克斯.md "wikilink")2007年12月發售的[PlayStation
Portable遊戲](../Page/PlayStation_Portable.md "wikilink")。游戏是[超級任天堂的](../Page/超級任天堂.md "wikilink")《[星海遊俠](../Page/星海遊俠.md "wikilink")》全新重製版。遊戲全部對白都加入語音，繪製新圖像，亦加入新的故事，可算是一款重新製作的新作，遊戲動畫由[Production
I.G製作](../Page/Production_I.G.md "wikilink")。冒險的舞台及背景地圖都是重新製作的3D圖像，玩家可以深入走入背景，開發者城，《星海遊俠1
初次啟航》的「動畫及語音使遊戲變得更有臨場的感覺」。

《星海遊俠1 初次啟航》與《[星海遊俠2
第二個故事](../Page/星海遊俠2_第二個故事.md "wikilink")》的重製版《[星海遊俠2
二次進化](../Page/星海遊俠2_二次進化.md "wikilink")》於「SQUARE ENIX PARTY 2007」中一起發表。

## 登場人物

  -

  -

  -

  -

  -

  -

  -

  -

  -

  -

  -

  -

  -

  -

## 音乐

游戏主题曲《Heart》由演唱。

## 外部連結

  - [星海遊俠1 First Departure
    官方網站](http://www.eternalsphere.com/so1/index.html)

  - [「星海遊俠」攤位特報
    PSP版「1」及「2」的配音員對話，主題曲初披露。](https://web.archive.org/web/20071012004846/http://watch.impress.co.jp/game/docs/20070512/star.htm)

[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink") [1 First
Departure](../Category/星海遊俠系列電子遊戲.md "wikilink") [Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:复刻游戏](../Category/复刻游戏.md "wikilink")
[Category:时间旅行游戏](../Category/时间旅行游戏.md "wikilink")
[Category:電子角色扮演遊戲](../Category/電子角色扮演遊戲.md "wikilink")