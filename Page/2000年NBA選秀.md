**2000年[NBA选秀](../Page/NBA选秀.md "wikilink")**于2000年6月24日举行，[肯揚·馬丁是这一年的状元新秀](../Page/肯揚·馬丁.md "wikilink")。

## 選秀

[Kenyon_Martin_Nuggets.jpg](https://zh.wikipedia.org/wiki/File:Kenyon_Martin_Nuggets.jpg "fig:Kenyon_Martin_Nuggets.jpg")
[Mike_Miller.jpg](https://zh.wikipedia.org/wiki/File:Mike_Miller.jpg "fig:Mike_Miller.jpg")
[Jamal_Crawford_2010_cropped.jpg](https://zh.wikipedia.org/wiki/File:Jamal_Crawford_2010_cropped.jpg "fig:Jamal_Crawford_2010_cropped.jpg")
[Hedo_Turkoglu_point_guard_11-27-08.jpg](https://zh.wikipedia.org/wiki/File:Hedo_Turkoglu_point_guard_11-27-08.jpg "fig:Hedo_Turkoglu_point_guard_11-27-08.jpg")
[Desmond_Mason.jpg](https://zh.wikipedia.org/wiki/File:Desmond_Mason.jpg "fig:Desmond_Mason.jpg")
[Michael_Redd_Beijing_Olympics_Men's_Semifinal_Basketball.jpg](https://zh.wikipedia.org/wiki/File:Michael_Redd_Beijing_Olympics_Men's_Semifinal_Basketball.jpg "fig:Michael_Redd_Beijing_Olympics_Men's_Semifinal_Basketball.jpg")

|    |                                    |    |                                    |    |                                  |    |                                  |   |                                       |
| -- | ---------------------------------- | -- | ---------------------------------- | -- | -------------------------------- | -- | -------------------------------- | - | ------------------------------------- |
| PG | [控球后卫](../Page/控球后卫.md "wikilink") | SG | [得分后卫](../Page/得分后卫.md "wikilink") | SF | [小前锋](../Page/小前锋.md "wikilink") | PF | [大前锋](../Page/大前锋.md "wikilink") | C | [中锋](../Page/中锋_\(篮球\).md "wikilink") |

|               |                        |
| ------------- | ---------------------- |
| ^             | 表示此球员已被列入篮球名人堂         |
| \*            | 表示此球员被选入参加全明星赛并进入全明星阵容 |
| <sup>+</sup>  | 表示此球员被选入参加全明星赛         |
| <sup>x</sup>  | 表示此球员进入全明星阵容           |
| <sup>\#</sup> | 表示此球员从未在NBA打球          |

<table style="width:130%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 20%" />
<col style="width: 10%" />
<col style="width: 20%" />
<col style="width: 35%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>轮数</p></th>
<th><p>顺位</p></th>
<th><p>球员</p></th>
<th><p>位置</p></th>
<th><p>国籍</p></th>
<th><p>NBA球队</p></th>
<th><p>学校／前属俱乐部</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>1</p></td>
<td><p><a href="../Page/肯揚·馬丁.md" title="wikilink">肯揚·馬丁</a><sup>+</sup></p></td>
<td><p>PF</p></td>
<td></td>
<td><p><a href="../Page/新泽西网.md" title="wikilink">新泽西网</a></p></td>
<td><p><a href="../Page/辛辛那提大學.md" title="wikilink">辛辛那提大學</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2</p></td>
<td><p><a href="../Page/斯特羅邁爾·斯威夫特.md" title="wikilink">斯特羅邁爾·斯威夫特</a></p></td>
<td><p>PF/C</p></td>
<td></td>
<td><p><a href="../Page/温哥华灰熊.md" title="wikilink">温哥华灰熊</a></p></td>
<td><p><a href="../Page/路易斯安那州立大學.md" title="wikilink">路易斯安那州立大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>3</p></td>
<td><p><a href="../Page/丹尼斯·邁爾斯.md" title="wikilink">丹尼斯·邁爾斯</a></p></td>
<td><p>SF/SG</p></td>
<td></td>
<td><p><a href="../Page/洛杉矶快船.md" title="wikilink">洛杉矶快船</a></p></td>
<td><p><small>(<a href="../Page/伊利諾州.md" title="wikilink">伊利諾州</a>，<a href="../Page/東聖路易斯_(伊利諾州).md" title="wikilink">東聖路易斯</a>)</small></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>4</p></td>
<td><p><a href="../Page/馬可斯·費瑟.md" title="wikilink">馬可斯·費瑟</a></p></td>
<td><p>PF</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p><a href="../Page/愛荷華州立大學.md" title="wikilink">愛荷華州立大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>5</p></td>
<td><p><a href="../Page/邁克·米勒.md" title="wikilink">邁克·米勒</a></p></td>
<td><p>SF/SG</p></td>
<td></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
<td><p><a href="../Page/佛羅里達大學.md" title="wikilink">佛羅里達大學</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>6</p></td>
<td><p><a href="../Page/德馬耳·強生.md" title="wikilink">德馬耳·強生</a></p></td>
<td><p>SG/SF</p></td>
<td></td>
<td><p><a href="../Page/亚特兰大鹰.md" title="wikilink">亚特兰大鹰</a></p></td>
<td><p><a href="../Page/辛辛那提大學.md" title="wikilink">辛辛那提大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>7</p></td>
<td><p><a href="../Page/克里斯·米恩.md" title="wikilink">克里斯·米恩</a></p></td>
<td><p>C/PF</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p><a href="../Page/德州農工大學.md" title="wikilink">德州農工大學</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>8</p></td>
<td><p><a href="../Page/賈馬爾·克勞福德.md" title="wikilink">賈馬爾·克勞福德</a></p></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/克里夫兰骑士.md" title="wikilink">克里夫兰骑士</a></p></td>
<td><p><a href="../Page/密西根大學.md" title="wikilink">密西根大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>9</p></td>
<td></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a></p></td>
<td><p><a href="../Page/明尼蘇達大學.md" title="wikilink">明尼蘇達大學</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>10</p></td>
<td><p><a href="../Page/肯揚·杜林.md" title="wikilink">肯揚·杜林</a></p></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
<td><p><a href="../Page/密蘇里大學.md" title="wikilink">密蘇里大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>11</p></td>
<td><p><a href="../Page/傑羅梅·莫伊索.md" title="wikilink">傑羅梅·莫伊索</a></p></td>
<td><p>PF</p></td>
<td></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a></p></td>
<td><p><a href="../Page/加州大學洛杉磯分校.md" title="wikilink">加州大學洛杉磯分校</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>12</p></td>
<td></td>
<td><p>PF/C</p></td>
<td></td>
<td><p><a href="../Page/达拉斯小牛.md" title="wikilink">达拉斯小牛</a></p></td>
<td><p><a href="../Page/雪城大學.md" title="wikilink">雪城大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>13</p></td>
<td><p><a href="../Page/康尼·亞歷山大.md" title="wikilink">康尼·亞歷山大</a></p></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
<td><p><a href="../Page/加利福尼亞州立大學.md" title="wikilink">加利福尼亞州立大學</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>14</p></td>
<td><p><a href="../Page/馬特恩·克里弗斯.md" title="wikilink">馬特恩·克里弗斯</a></p></td>
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
<td><p><a href="../Page/密西根大學.md" title="wikilink">密西根大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>15</p></td>
<td><p><a href="../Page/傑森·柯林斯.md" title="wikilink">傑森·柯林斯</a></p></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/密尔沃基雄鹿.md" title="wikilink">密尔沃基雄鹿</a></p></td>
<td><p><a href="../Page/喬治亞理工學院.md" title="wikilink">喬治亞理工學院</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>16</p></td>
<td><p><a href="../Page/希达耶特·特科格鲁.md" title="wikilink">希达耶特·特科格鲁</a></p></td>
<td><p>SF</p></td>
<td></td>
<td><p><a href="../Page/萨克拉门托国王.md" title="wikilink">萨克拉门托国王</a></p></td>
<td><p><a href="../Page/安納托利亞艾菲斯隊.md" title="wikilink">安納托利亞艾菲斯隊</a><small>()</small></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>17</p></td>
<td><p><a href="../Page/德斯蒙德·梅森.md" title="wikilink">德斯蒙德·梅森</a></p></td>
<td><p>SF/SG</p></td>
<td></td>
<td><p><a href="../Page/西雅图超音速.md" title="wikilink">西雅图超音速</a></p></td>
<td><p><a href="../Page/東南俄克拉荷馬州立大學.md" title="wikilink">東南俄克拉荷馬州立大學</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>18</p></td>
<td></td>
<td><p>SF/SG</p></td>
<td></td>
<td><p><a href="../Page/洛杉矶快船.md" title="wikilink">洛杉矶快船</a></p></td>
<td><p><a href="../Page/德保罗大學.md" title="wikilink">德保罗大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>19</p></td>
<td><p><sup>+</sup></p></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/夏洛特黃蜂.md" title="wikilink">夏洛特黃蜂</a></p></td>
<td><p><a href="../Page/東肯塔基大學.md" title="wikilink">東肯塔基大學</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>20</p></td>
<td><p><a href="../Page/斯皮迪·克雷斯頓.md" title="wikilink">斯皮迪·克雷斯頓</a></p></td>
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a></p></td>
<td><p><a href="../Page/霍夫斯特拉大學.md" title="wikilink">霍夫斯特拉大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>21</p></td>
<td></td>
<td><p>SF/SG</p></td>
<td></td>
<td><p><a href="../Page/多伦多猛龙.md" title="wikilink">多伦多猛龙</a></p></td>
<td><p><a href="../Page/密西根大學.md" title="wikilink">密西根大學</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>22</p></td>
<td><p><a href="../Page/唐尼爾·哈維.md" title="wikilink">唐尼爾·哈維</a></p></td>
<td><p>SF</p></td>
<td></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a><small>(與一起交易至<a href="../Page/達拉斯小牛隊.md" title="wikilink">達拉斯小牛隊</a>，交換與)</small></p></td>
<td><p><a href="../Page/佛羅里達大學.md" title="wikilink">佛羅里達大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>23</p></td>
<td></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/犹他爵士.md" title="wikilink">犹他爵士</a><small>(來自<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>)</small></p></td>
<td><p><small>(<a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州</a>，<a href="../Page/佛雷斯諾.md" title="wikilink">佛雷斯諾</a>)</small></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>24</p></td>
<td><p><a href="../Page/戴利波爾·巴格里奇.md" title="wikilink">戴利波爾·巴格里奇</a></p></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a> <small>(來自<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>)</small></p></td>
<td><p><small>(<a href="../Page/克羅埃西亞.md" title="wikilink">克羅埃西亞</a>)</small></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>25</p></td>
<td><p><a href="../Page/傑克·查卡里迪斯.md" title="wikilink">傑克·查卡里迪斯</a></p></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a></p></td>
<td><p><small>(<a href="../Page/希臘籃球甲級聯賽.md" title="wikilink">希臘籃球甲級聯賽</a>)</small></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>26</p></td>
<td></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/丹佛掘金.md" title="wikilink">丹佛掘金</a><small>(來自<a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>)</small></p></td>
<td><p><a href="../Page/奧本大學.md" title="wikilink">奧本大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>27</p></td>
<td><p><a href="../Page/普里莫茲·布雷澤克.md" title="wikilink">普里莫茲·布雷澤克</a></p></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/印第安纳步行者.md" title="wikilink">印第安纳步行者</a></p></td>
<td><p><a href="../Page/奧林比查.md" title="wikilink">奧林比查</a> <small>()</small></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>28</p></td>
<td><p><a href="../Page/埃里克·巴克利.md" title="wikilink">埃里克·巴克利</a></p></td>
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/波特兰开拓者.md" title="wikilink">波特兰开拓者</a></p></td>
<td><p><a href="../Page/聖若望大學.md" title="wikilink">聖若望大學</a></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>29</p></td>
<td></td>
<td><p>PF</p></td>
<td></td>
<td><p><a href="../Page/洛杉矶湖人.md" title="wikilink">洛杉矶湖人</a></p></td>
<td><p><a href="../Page/史丹佛大學.md" title="wikilink">史丹佛大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>30</p></td>
<td><p><a href="../Page/馬科·雅里奇.md" title="wikilink">馬科·雅里奇</a></p></td>
<td><p>SG</p></td>
<td><p><br />
</p></td>
<td><p><a href="../Page/洛杉矶快船.md" title="wikilink">洛杉矶快船</a></p></td>
<td><p><small>(<a href="../Page/義大利籃球甲級聯賽.md" title="wikilink">義大利籃球甲級聯賽</a>)</small></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>31</p></td>
<td><p><a href="../Page/丹·朗吉.md" title="wikilink">丹·朗吉</a></p></td>
<td><p>PF</p></td>
<td></td>
<td><p><a href="../Page/达拉斯小牛.md" title="wikilink">达拉斯小牛</a><small>(來自<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>, 交易至<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>，交換與一個未來第二輪選秀權)</small></p></td>
<td><p><a href="../Page/范德堡大学.md" title="wikilink">范德堡大学</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>32</p></td>
<td><p><a href="../Page/A·J·蓋頓.md" title="wikilink">A·J·蓋頓</a></p></td>
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><small>(來自<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>)</small></p></td>
<td><p><a href="../Page/印第安納大學.md" title="wikilink">印第安納大學布魯明頓校區</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>33</p></td>
<td></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a> <small>(來自<a href="../Page/温哥华灰熊.md" title="wikilink">温哥华灰熊經</a><a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a>)</small></p></td>
<td><p><a href="../Page/康涅狄格大學.md" title="wikilink">康涅狄格大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>34</p></td>
<td><p><a href="../Page/克哈瑞德·艾爾·阿敏.md" title="wikilink">克哈瑞德·艾爾·阿敏</a></p></td>
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a><small>(來自<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>)</small></p></td>
<td><p><a href="../Page/康涅狄格大學.md" title="wikilink">康涅狄格大學</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>35</p></td>
<td></td>
<td><p>SF</p></td>
<td></td>
<td><p><a href="../Page/华盛顿奇才.md" title="wikilink">华盛顿奇才</a></p></td>
<td><p><a href="../Page/路易斯安那州門羅大學.md" title="wikilink">路易斯安那州門羅大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>36</p></td>
<td></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a></p></td>
<td><p><small>()</small></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>37</p></td>
<td><p><a href="../Page/埃迪·豪斯.md" title="wikilink">埃迪·豪斯</a></p></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a> <small>(來自<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士經</a><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>)</small></p></td>
<td><p><a href="../Page/亞利桑那州立大學.md" title="wikilink">亞利桑那州立大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>38</p></td>
<td></td>
<td><p>SF</p></td>
<td></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a> <small>(與一個未來第二輪選秀權交易至达拉斯小牛，交換<a href="../Page/丹·朗吉.md" title="wikilink">丹·朗吉</a>)</small></p></td>
<td><p><a href="../Page/奧克拉荷馬大學.md" title="wikilink">奧克拉荷馬大學</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>39</p></td>
<td></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a> <small>(來自<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>)</small></p></td>
<td><p><a href="../Page/聖若望大學.md" title="wikilink">聖若望大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>40</p></td>
<td><p><a href="../Page/汉诺·莫托拉.md" title="wikilink">汉诺·莫托拉</a></p></td>
<td><p>SF/PF</p></td>
<td></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a> <small>(來自<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>)</small></p></td>
<td><p><a href="../Page/猶他大學.md" title="wikilink">猶他大學</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>41</p></td>
<td><p><sup>#</sup></p></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a> <small>(來自<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>)</small></p></td>
<td><p><a href="../Page/杜克大學.md" title="wikilink">杜克大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>42</p></td>
<td><p><a href="../Page/奧盧米德·奧德傑.md" title="wikilink">奧盧米德·奧德傑</a></p></td>
<td><p>PF</p></td>
<td></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a></p></td>
<td><p><small>(<a href="../Page/德國籃球甲級聯賽.md" title="wikilink">德國籃球甲級聯賽</a>)</small></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>43</p></td>
<td><p><a href="../Page/迈克尔·里德.md" title="wikilink">迈克尔·里德</a><sup>*</sup></p></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/密爾沃基公鹿.md" title="wikilink">密爾沃基公鹿</a></p></td>
<td><p><a href="../Page/俄亥俄州立大學.md" title="wikilink">俄亥俄州立大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>44</p></td>
<td><p><a href="../Page/布萊恩·卡迪納爾.md" title="wikilink">布萊恩·卡迪納爾</a></p></td>
<td><p>PF</p></td>
<td></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a></p></td>
<td><p><a href="../Page/普渡大學.md" title="wikilink">普渡大學</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>45</p></td>
<td></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a></p></td>
<td><p><a href="../Page/路易斯安那州立大學.md" title="wikilink">路易斯安那州立大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>46</p></td>
<td><p><sup>#</sup></p></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>47</p></td>
<td><p><sup>#</sup></p></td>
<td><p>SG</p></td>
<td></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a> <small>(交易至<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>，交換兩個未來第二輪選秀權)</small></p></td>
<td><p><small>(<a href="../Page/克羅埃西亞.md" title="wikilink">克羅埃西亞</a>)</small></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>48</p></td>
<td><p><sup>#</sup></p></td>
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/费城76人.md" title="wikilink">费城76人</a></p></td>
<td><p><a href="../Page/天普大學.md" title="wikilink">天普大學</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>49</p></td>
<td><p><a href="../Page/贾森·哈特.md" title="wikilink">贾森·哈特</a></p></td>
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a> <small>(來自<a href="../Page/紐奧良黃蜂.md" title="wikilink">紐奧良黃蜂</a>)</small></p></td>
<td><p><a href="../Page/雪城大學.md" title="wikilink">雪城大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>50</p></td>
<td><p><a href="../Page/坎尼尔·迪肯斯.md" title="wikilink">坎尼尔·迪肯斯</a></p></td>
<td><p>SF/PF</p></td>
<td></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a> <small>(來自<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>)</small></p></td>
<td><p><a href="../Page/愛達荷大學.md" title="wikilink">愛達荷大學</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>51</p></td>
<td></td>
<td><p>PG</p></td>
<td></td>
<td><p><a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a></p></td>
<td><p><a href="../Page/紅星.md" title="wikilink">紅星</a><small>(<a href="../Page/南斯拉夫.md" title="wikilink">南斯拉夫</a>、<a href="../Page/塞爾維亞與蒙特內哥羅.md" title="wikilink">塞爾維亞與蒙特內哥羅</a>)</small> 1978</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>52</p></td>
<td><p><a href="../Page/欧尼斯特·布朗.md" title="wikilink">欧尼斯特·布朗</a></p></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>53</p></td>
<td></td>
<td><p>C</p></td>
<td></td>
<td><p><a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a> <small>(來自<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>)</small></p></td>
<td><p><a href="../Page/北亞利桑那大學.md" title="wikilink">北亞利桑那大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>54</p></td>
<td><p><sup>#</sup></p></td>
<td><p>SG/SF</p></td>
<td></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a> <small>(交易至<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>，交換兩個未來第二輪選秀權)</small></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>55</p></td>
<td></td>
<td><p>F</p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a> <small>(來自<a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>)</small></p></td>
<td><p><a href="../Page/奧本大學.md" title="wikilink">奧本大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>56</p></td>
<td><p><sup>#</sup></p></td>
<td><p>G</p></td>
<td></td>
<td><p><a href="../Page/印第安納溜馬.md" title="wikilink">印第安納溜馬</a></p></td>
<td><p><a href="../Page/科羅拉多大學波德分校.md" title="wikilink">科羅拉多大學波德分校</a></p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>57</p></td>
<td><p><sup>#</sup></p></td>
<td><p>G</p></td>
<td></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></p></td>
<td><p><a href="../Page/俄亥俄州立大學.md" title="wikilink">俄亥俄州立大學</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>58</p></td>
<td><p><sup>#</sup></p></td>
<td><p>F</p></td>
<td></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a> <small>(來自<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人與</a>被交易至<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>，交換與<a href="../Page/唐尼爾·哈維.md" title="wikilink">唐尼爾·哈維</a>)</small></p></td>
<td><p><a href="../Page/辛辛納提大學.md" title="wikilink">辛辛納提大學</a></p></td>
</tr>
</tbody>
</table>

## 参考资料

[Category:NBA选秀](../Category/NBA选秀.md "wikilink")