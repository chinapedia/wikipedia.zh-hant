**哲別**（，），[别速惕氏](../Page/尼倫.md "wikilink")\[1\]，《[蒙古秘史](../Page/蒙古秘史.md "wikilink")》作**者别**，原名**只儿豁阿歹**\[2\]，臣服於[铁木真後被赐名](../Page/铁木真.md "wikilink")**哲别**，意為“箭頭”。並有[蒙古帝国第一猛将稱號](../Page/蒙古帝国.md "wikilink")，亦是[四狗](../Page/四犬.md "wikilink")（四勇、四虎将、四先锋）之一。

## 生平

哲别骁勇善战，精於射术，能百发百中地射击飞动中的碗，曾於战斗中，徒手抓著敌军射来的無箭头的箭，仍搭上弓射去，竟能一箭穿透敌方主将心窝。此外哲别的刀法也是非常了得，在一生中的南征北討，哲别手持蒙古弯刀殺敵，如入无人之境。

### 征金、西遼

哲別原是[泰赤烏部的屬民](../Page/泰赤烏部.md "wikilink")，1202年[铁木真收服](../Page/成吉思汗.md "wikilink")[泰赤乌部後](../Page/泰赤乌部.md "wikilink")，臣服於铁木真。1211年隨成吉思汗征金，充當先鋒。\[3\]1218年奉成吉思汗之令，率兵兩萬追殺[乃蠻王子](../Page/乃蠻.md "wikilink")[屈出律](../Page/屈出律.md "wikilink")，消滅[西遼](../Page/西遼.md "wikilink")，掌握[絲路中](../Page/丝绸之路.md "wikilink")、東商貿，為蒙古軍隊即將開始的[西征先行佈署](../Page/蒙古征戰.md "wikilink")。

### 征花剌子模、欽察、斡羅思

1219年，從成吉思汗西征，次年奉命與[速不台追擊](../Page/速不台.md "wikilink")[花剌子模君主](../Page/花剌子模王朝.md "wikilink")[摩訶末](../Page/摩訶末.md "wikilink")。後又攻略[阿塞拜疆](../Page/阿塞拜疆.md "wikilink")、[格魯吉亞](../Page/格魯吉亞.md "wikilink")、[阿速](../Page/奄蔡.md "wikilink")、[欽察等地](../Page/欽察.md "wikilink")。\[4\]1223年哲别與[速不台於](../Page/速不台.md "wikilink")[迦勒迦河之战](../Page/迦勒迦河之战.md "wikilink")（今[乌克兰](../Page/乌克兰.md "wikilink")[马里乌波尔市北](../Page/马里乌波尔.md "wikilink")）中击溃[斡羅思诸国王公与](../Page/基辅罗斯.md "wikilink")[钦察](../Page/欽察.md "wikilink")[忽炭汗的联军](../Page/忽炭汗.md "wikilink")，途經[斡羅思南境](../Page/基辅罗斯.md "wikilink")，又转攻也的里河（今[伏尔加河的突厥名](../Page/伏尔加河.md "wikilink")，又译亦的勒）上的[伏爾加保加利亞](../Page/伏爾加保加利亞.md "wikilink")。1224年，於回國途中病逝。成吉思汗以国葬厚葬哲别。

## 參見

  - [四犬](../Page/四犬.md "wikilink")

## 注释

## 參考文獻

  - 《[蒙古秘史](../Page/蒙古秘史.md "wikilink")》，余大鈞譯，河北人民出版社，2001年5月出版

[Z哲](../Category/1223年逝世.md "wikilink")
[Category:蒙古帝國人物](../Category/蒙古帝國人物.md "wikilink")
[Category:蒙古族人物](../Category/蒙古族人物.md "wikilink")
[Z哲](../Category/射鵰英雄傳角色.md "wikilink")
[射](../Category/金庸筆下真實人物.md "wikilink")

1.  察剌孩領忽之子別速台的後裔
2.  《[蒙古秘史](../Page/蒙古秘史.md "wikilink")》，余大鈞譯，196頁
3.  《[蒙古秘史](../Page/蒙古秘史.md "wikilink")》，余大鈞譯，196頁
4.  《[蒙古秘史](../Page/蒙古秘史.md "wikilink")》，余大鈞譯，196頁