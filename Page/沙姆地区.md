[Modern_Levant.PNG](https://zh.wikipedia.org/wiki/File:Modern_Levant.PNG "fig:Modern_Levant.PNG")

**沙姆地区**或**沙姆**（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：****或****，[拉丁化](../Page/拉丁化.md "wikilink")：**''***或*****''），是[阿拉伯世界对于](../Page/阿拉伯世界.md "wikilink")[地中海东岸的整个](../Page/地中海.md "wikilink")[累范特地区或](../Page/累范特.md "wikilink")**[大叙利亚](../Page/大叙利亚.md "wikilink")**地区的称呼，而随着历史的发展，所指亦有所不同。如今[阿拉伯人所说的沙姆地区一般包括](../Page/阿拉伯人.md "wikilink")[叙利亚](../Page/叙利亚.md "wikilink")、[约旦](../Page/约旦.md "wikilink")、[黎巴嫩和](../Page/黎巴嫩.md "wikilink")[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")（[巴勒斯坦国](../Page/巴勒斯坦国.md "wikilink")）。

## 词源概述

“**沙姆**”（****）一词源于[阿拉伯半岛的](../Page/阿拉伯半岛.md "wikilink")[汉志地区](../Page/汉志_\(沙特阿拉伯\).md "wikilink")（今属[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")），最初指的是“左手的土地”，因为对于汉志的一些人来说，当他们面对[东方](../Page/东方.md "wikilink")，半岛[北方地区就在](../Page/北方.md "wikilink")[左边](../Page/左.md "wikilink")（相对的，[也门则指](../Page/也门.md "wikilink")“右手的土地”，因为它在半岛南方）。

由此可见，“沙姆”一词最初没有确知哪个地区，只是指半岛北方。但随着[阿拉伯帝国的北征受阻于](../Page/阿拉伯帝国.md "wikilink")[拜占庭帝国的](../Page/拜占庭帝国.md "wikilink")[小亚细亚](../Page/小亚细亚.md "wikilink")，“沙姆”一词开始指称由阿拉伯人实际控制的阿拉伯半岛北方地区，而这一地区的大部分在古代都称[叙利亚](../Page/叙利亚.md "wikilink")，所以“沙姆”一词也代指叙利亚。叙利亚的[大马士革](../Page/大马士革.md "wikilink")（今叙利亚[首都](../Page/首都.md "wikilink")）则向来是这一地区的最重要的区域中心[城市](../Page/城市.md "wikilink")，因此“沙姆”一词也可以指大马士革城。

沙姆地区不是一直和“[大叙利亚](../Page/大叙利亚.md "wikilink")”或“[累范特](../Page/累范特.md "wikilink")”所指的区域相吻合的，因为大叙利亚可以指更小的区域，而累范特则可包括更大的区域。如今，沙姆地区一词最常被描绘早期的这一地区的[历史学家所使用](../Page/历史学家.md "wikilink")，因为在[中东地区的很长一段历史中](../Page/中东地区.md "wikilink")，沙姆地区的各个[国家或](../Page/国家.md "wikilink")[民族都有着共同的](../Page/民族.md "wikilink")[文化和相似的](../Page/文化.md "wikilink")[经济发展水平](../Page/经济.md "wikilink")，或[统一在同一个](../Page/统一.md "wikilink")[国家](../Page/国家.md "wikilink")（如阿拉伯帝国和[奥斯曼帝国](../Page/奥斯曼帝国.md "wikilink")）。随着[第一次世界大战以前这一地区的](../Page/第一次世界大战.md "wikilink")[殖民主义和一批新兴国家](../Page/殖民主义.md "wikilink")（尤其是非阿拉伯的[以色列国](../Page/以色列国.md "wikilink")）的兴起，这种相似和统一终结了。但对于历史学家来说，整个20世纪这一地区仍然是一个整体。如今，“沙姆地区”一词仍然被广泛使用，尤其是在文化和历史领域。

今天阿拉伯人所说的沙姆地区包括叙利亚、[约旦](../Page/约旦.md "wikilink")、[黎巴嫩和](../Page/黎巴嫩.md "wikilink")[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")（[巴勒斯坦国](../Page/巴勒斯坦国.md "wikilink")），而其他地区的人偶尔也会把[以色列](../Page/以色列.md "wikilink")（大多数[阿拉伯国家不承认](../Page/阿拉伯国家.md "wikilink")）算在内，历史学家则把现代叙利亚东北和[伊拉克西北的](../Page/伊拉克.md "wikilink")[贾齐拉地区包括在内](../Page/贾齐拉地区.md "wikilink")。

“沙姆”一词的[形容词形式](../Page/形容词.md "wikilink")****则指这一地区的[人](../Page/人.md "wikilink")。因为沙姆也是叙利亚的代称，所以也可以指叙利亚人。

## “叙利亚”與“沙姆”

叙利亚的国名在[阿拉伯语中为](../Page/阿拉伯语.md "wikilink")****或****，但是在大约1870年以前，这个词在说阿拉伯语的[穆斯林中使用不是很广泛](../Page/穆斯林.md "wikilink")，而是更早地在说阿拉伯语的[基督徒中广泛使用](../Page/基督徒.md "wikilink")。叙利亚的[东正教](../Page/东正教.md "wikilink")（[叙利亚东正教](../Page/叙利亚东正教.md "wikilink")）[教会则用](../Page/教会.md "wikilink")“叙利亚”的形容词形式****（复数：****，如今表示所有“叙利亚人”）表示[基督教早期](../Page/基督教.md "wikilink")（[东西教会大分裂以前](../Page/东西教会大分裂.md "wikilink")）的叙利亚教徒，而使用专有的阿拉伯词语****（复数：****）表示东西教会分裂后的叙利亚东正教教徒。

现在****一词表示叙利亚这个国家，与[大叙利亚相对](../Page/大叙利亚.md "wikilink")，它也被称为**沙姆国家或[地区](../Page/地区.md "wikilink")**（****）。这个差别在20世纪中叶以前不是很明显，之后，随着第一次世界大战后《[塞克斯-皮克特协定](../Page/塞克斯-皮克特协定.md "wikilink")》的签署，以及1936年原来分裂的这一地区成为[法国的](../Page/法国.md "wikilink")[委任统治地](../Page/委任统治地.md "wikilink")，成为一个统一的[政治](../Page/政治.md "wikilink")[实体](../Page/实体.md "wikilink")，[哈希姆王室如果沒有建立](../Page/哈希姆王室.md "wikilink")[叙利亚阿拉伯王国](../Page/叙利亚阿拉伯王国.md "wikilink")。从此，大叙利亚的概念逐渐被沙姆地区所取代。

## 注释

## 参考文献

  - 《[伊斯兰百科全书](../Page/伊斯兰百科全书.md "wikilink")》第9卷第261页，[C.E.波斯华斯](../Page/C.E.波斯华斯.md "wikilink")，1997年
  - 《[现代书面阿拉伯语词典](../Page/现代书面阿拉伯语词典.md "wikilink")》第4版，[汉斯·威尔](../Page/汉斯·威尔.md "wikilink")，1994年
  - 《阿拉伯语汉语词典》，[北京大学东方语言系阿拉伯语教研室编](../Page/北京大学.md "wikilink")

## 参见

  - [阿拉伯东方](../Page/阿拉伯东方.md "wikilink")
  - [累范特](../Page/累范特.md "wikilink")
  - [大以色列](../Page/大以色列.md "wikilink")
  - [大叙利亚](../Page/大叙利亚.md "wikilink")
  - [迦南](../Page/迦南.md "wikilink")
  - [以色列国土](../Page/以色列国土.md "wikilink")
  - [巴勒斯坦领土](../Page/巴勒斯坦领土.md "wikilink")
  - [中东](../Page/中东.md "wikilink")
  - [大中东](../Page/大中东.md "wikilink")

{{-}}

[Category:敘利亞歷史](../Category/敘利亞歷史.md "wikilink")
[Category:民族统一主义](../Category/民族统一主义.md "wikilink")