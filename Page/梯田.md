[Terrace_field_guangxi_longji_china.jpg](https://zh.wikipedia.org/wiki/File:Terrace_field_guangxi_longji_china.jpg "fig:Terrace_field_guangxi_longji_china.jpg")[广西上的](../Page/广西.md "wikilink")[龙脊梯田](../Page/龙脊梯田.md "wikilink")\]\]
**梯田**是一种分层的[丘陵](../Page/丘陵.md "wikilink")[种植区](../Page/种植区.md "wikilink")。梯田一般采用人工或者管道输送水措施分层保持水土，避免[灌溉用水造成快速地面](../Page/灌溉.md "wikilink")[逕流](../Page/逕流.md "wikilink")。

## 歷史

中国在公元前2世纪前后便有梯田的雏形。《[诗经](../Page/诗经.md "wikilink")·小雅》记载：“瞻彼阪田，有菀其特”（《正月》）“彪池北流，浸彼稻田”（《白桦》），说明在古代[西周时期已经开始形成梯田雏形](../Page/西周.md "wikilink")。《[氾胜之书](../Page/氾勝之書.md "wikilink")》记载：”区田以粪气为美，非必须良田也。诸山陵近邑高危倾阪及丘城上,皆可区田。“作者[氾胜之把区田方法应用于](../Page/氾勝之.md "wikilink")[陕西黄土区的梯田上](../Page/陕西省.md "wikilink")，可见[汉代梯田的开垦范围进一步扩大](../Page/汉朝.md "wikilink")，不仅可在山地而且在缓斜度的平原上也辟有梯田，梯田技术有了显著进步。\[1\]\[2\]

## 分佈

中国梯田主要分布在江南山岭地区，其中广西、云南居多，这是因为这些地方雨水比较多，又多山，梯田依山而建，其中以[云南](../Page/云南.md "wikilink")[哀牢山和](../Page/哀牢山.md "wikilink")[广西龙胜龙脊梯田较为出名](../Page/广西.md "wikilink")。

  - [哈尼梯田](../Page/哈尼梯田.md "wikilink")：云南多山，亦多梯田，哀牢山哈尼梯田为云南梯田的代表作，被誉为“中国最美的山岭雕刻”。
  - [龙胜](../Page/龙胜.md "wikilink")[龙脊梯田景区是广西著名风景区](../Page/龙脊梯田.md "wikilink")，始建于元朝，距今已有近700年的历史。她是[壮族](../Page/壮族.md "wikilink")、[瑶族勤劳智慧的结晶](../Page/瑶族.md "wikilink")。龙脊梯田景区主要分为平安壮寨梯田和金坑红瑶梯田。平安壮寨梯田有“七星伴月”和“九龙五虎”两大著名景观，景色秀美飘逸；金坑红瑶梯田有“大界千层天梯”，“西山韶乐”和“金佛顶”三大著名景观，气势磅礴，直上云端。整个龙脊梯田景区山青水秀，瀑布成群，春如层层银带，夏滚道道绿波，秋叠座座金塔，冬似群龙戏水，四季各有神韵。这里的壮、瑶民族世世代代居住在青瓦木楼之中，男耕女织，淳朴善良。金竹壮寨被[联合国教科文组织称为北壮的楷模](../Page/联合国教科文组织.md "wikilink")；黄洛红瑶寨获上海大世界吉尼斯集体长发村之最；龙脊古壮寨至今保留着许多文物古迹。

<table>
<tbody>
<tr class="odd">
<td><p><code>Image:Terrace field yunnan china.jpg|</code><a href="../Page/中国.md" title="wikilink"><code>中国</code></a><a href="../Page/云南.md" title="wikilink"><code>云南</code></a><a href="../Page/元阳.md" title="wikilink"><code>元阳的</code></a><a href="../Page/水稻.md" title="wikilink"><code>水稻梯田</code></a><br />
<code>Image:Landscape in Sa Pa (Vietnam).jpg|越南</code><a href="../Page/w:Sa_Pa.md" title="wikilink"><code>沙坝梯田</code></a><br />
<code>Image:Terraced_vineyards_in_Switzerland.jpg|</code><a href="../Page/瑞士.md" title="wikilink"><code>瑞士</code></a><a href="../Page/洛桑.md" title="wikilink"><code>洛桑附近的</code></a><a href="../Page/葡萄.md" title="wikilink"><code>葡萄梯田</code></a><br />
<code>Image:Bangali, a village in Uttaranchal near Nandprayag.jpg|</code><a href="../Page/印度.md" title="wikilink"><code>印度</code></a><a href="../Page/w:Nandaprayag.md" title="wikilink"><code>Nandaprayag梯田</code></a><code> </code><br />
<code>Image:Longsheng_pano.jpg|桂林一帶梯田</code><br />
<a href="File:Rice"><code>File:Rice</code></a><code> Terraces Banaue.jpg|</code><a href="../Page/菲律宾.md" title="wikilink"><code>菲律宾</code></a><a href="../Page/吕宋岛.md" title="wikilink"><code>吕宋岛上的稻梯田</code></a><br />
</p></td>
</tr>
</tbody>
</table>

## 世界三大梯田

  - [哀牢山](../Page/哀牢山.md "wikilink")[红河哈尼梯田](../Page/红河哈尼梯田.md "wikilink")

[_Rice_terraces.png](https://zh.wikipedia.org/wiki/File:_Rice_terraces.png "fig:_Rice_terraces.png")\]\]

  - [菲律宾巴纳韦梯田](../Page/菲律賓科迪勒拉山水稻梯田.md "wikilink")
  - [瑞士](../Page/瑞士.md "wikilink")[拉沃梯田](../Page/拉沃梯田.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[梯田](../Category/梯田.md "wikilink")

1.  张蜜.
    [中国古代梯田的起源与发展](http://www.cnki.net/KCMS/detail/detail.aspx?QueryID=2&CurRec=1&recid=&filename=NNNM201505029&dbname=CJFDLAST2015&dbcode=CJFQ&pr=&urlid=&yx=&v=MDQ5NjYrZVorWnRGeUhsV3IzT0t5UEZZN0c0SDlUTXFvOUhiWVI4ZVgxTHV4WVM3RGgxVDNxVHJXTTFGckNVUkw=)\[J\].
    农村.农业.农民(A版),2015,05:58-59.
2.  姚云峰,王礼先.
    [我国梯田的形成与发展](http://www.cnki.net/KCMS/detail/detail.aspx?recid=&filename=ZGSB199106020&dbname=CJFD1991&dbcode=CJFD&id=&file=ZGSB199106020&urlid=&yx=&v=MTY5NDR1eFlTN0RoMVQzcVRyV00xRnJDVVJMK2VaK1p0RnlIbVVyN09QeXJZYkxLeEY5RE1xWTlIWklSOGVYMUw=)\[J\].
    中国水土保持,1991,06:56-58.