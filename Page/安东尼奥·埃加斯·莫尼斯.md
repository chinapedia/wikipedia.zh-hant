**安东尼奥·埃加斯·莫尼斯**（**António Egas Moniz**，），全名**António Caetano de Abreu
Freire Egas
Moniz**，[葡萄牙](../Page/葡萄牙.md "wikilink")[精神病学家和](../Page/精神病.md "wikilink")[神经外科医生](../Page/神经外科.md "wikilink")。

## 生平

他由于发现[前脑叶白质切除术对某些心理疾病的治疗效果而获得](../Page/前脑叶白质切除术.md "wikilink")1949年[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。他是第一位获得[诺贝尔奖的葡萄牙人](../Page/诺贝尔奖.md "wikilink")。

## 参考资料

  - [诺贝尔奖官方网站关于安东尼奥·埃加斯·莫尼斯生平介绍](http://nobelprize.org/nobel_prizes/medicine/laureates/1949/moniz-bio.html)

[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:葡萄牙諾貝爾獎獲得者](../Category/葡萄牙諾貝爾獎獲得者.md "wikilink")
[Category:科英布拉大學校友](../Category/科英布拉大學校友.md "wikilink")
[Category:里斯本大學校友](../Category/里斯本大學校友.md "wikilink")
[Category:葡萄牙神经科学家](../Category/葡萄牙神经科学家.md "wikilink")
[Category:外科醫生](../Category/外科醫生.md "wikilink")