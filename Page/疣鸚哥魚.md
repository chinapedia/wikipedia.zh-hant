**疣鸚嘴魚**，又名瘤綠鸚嘴魚，俗名叩頭鸚哥、鸚哥、青衣，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[鸚哥魚科的其中一](../Page/鸚哥魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度西](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[琉球群島](../Page/琉球群島.md "wikilink")、[菲律賓等海域](../Page/菲律賓.md "wikilink")。

## 深度

水深3至30公尺。

## 特徵

本魚體延長而略側扁。後鼻孔並不明顯的大於前鼻孔。齒板之外表面平滑，上齒板不完全被上唇所覆蓋；後端具2顆犬齒；每一上咽骨具1列臼齒狀之咽頭齒。齒黃[白色](../Page/白色.md "wikilink")，但近唇為[藍綠色](../Page/藍綠色.md "wikilink")。體深藍綠色略灰。各鱗片具[淡紅色緣](../Page/淡紅色.md "wikilink")。頭部及各鰭條較暗色。前額向前突出，突出部分在眼睛水平之上。尾鰭截型，隨體長增長而呈雙凹型尾。背鰭硬棘9枚、背鰭軟條10枚、臀鰭硬棘3枚、臀鰭軟條9枚。體長可達40公分。

## 生態

喜歡生活在沿礁或珊瑚礁區。啃食[珊瑚](../Page/珊瑚.md "wikilink")，以珊瑚共生藻為生。

## 經濟利用

中型食用魚類。可作成[魚鬆](../Page/魚鬆.md "wikilink")，食用時可用[紅燒或](../Page/紅燒.md "wikilink")[清蒸](../Page/清蒸.md "wikilink")。另外可當成[觀賞魚觀賞](../Page/觀賞魚.md "wikilink")。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[oedema](../Category/綠鸚嘴魚屬.md "wikilink")