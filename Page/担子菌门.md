[Agaricales.jpg](https://zh.wikipedia.org/wiki/File:Agaricales.jpg "fig:Agaricales.jpg")
*Amanita muscaria* ([傘菌目](../Page/傘菌目.md "wikilink"))\]\]
**担子菌门**（[学名](../Page/学名.md "wikilink")：）是一类高等[真菌](../Page/真菌.md "wikilink")，构成[雙核亞界](../Page/雙核亞界.md "wikilink")，包含2万多种，包括[蘑菇](../Page/蘑菇.md "wikilink")、[木耳等主要食用菌](../Page/木耳.md "wikilink")。更具体地说，担子菌门包括以下组：[蘑菇](../Page/蘑菇.md "wikilink")，[马勃](../Page/马勃.md "wikilink")，stinkhorns（[鬼筆科](../Page/鬼筆科.md "wikilink")），，和人体致病酵母[隐球菌属等等](../Page/隐球菌属.md "wikilink")。

担子菌门的真菌基本全为陆生品种，主要特征是由多细胞，有横隔膜的菌丝体组成，菌丝分为两种，初生菌丝体的[细胞只有一个](../Page/细胞.md "wikilink")[细胞核](../Page/细胞核.md "wikilink")，次生菌丝体的细胞有两个核，两个核的次生菌丝体可以形成一种子实体，称为[担子果](../Page/担子果.md "wikilink")（），经过有性繁殖过程，在担子上生成4个有性[担孢子](../Page/担孢子.md "wikilink")（）；沒有無性孢子。

担子菌门的真菌种类繁多，有可以食用的，有可以药用的，也有许多种类有毒，与人类的生活关系较大。担子负于担子果上，除锈菌和黑粉菌外担子果多大而显著。

## 分类

由67位真菌学家一个联合所采用的最新的分类\[1\]认可三种亚门（[柄锈菌亚门](../Page/柄锈菌亚门.md "wikilink")，[黑粉菌亚门](../Page/黑粉菌亚门.md "wikilink")，[伞菌亚门](../Page/伞菌亚门.md "wikilink")）和两个其他[纲级别的类群](../Page/纲_\(生物\).md "wikilink")（[节担菌纲](../Page/节担菌纲.md "wikilink")，[根肿黑粉菌纲](../Page/根肿黑粉菌纲.md "wikilink")），除了担子菌门之外。截至目前分类的亚门合并，和削减了在以前常用来描述担子菌门的各种过时的分类种群（见下文）。根据2008年的估计，担子菌门包括三个亚门（其中包括6名未分配的[纲](../Page/纲_\(生物\).md "wikilink")）16个纲，52个[目](../Page/目_\(生物\).md "wikilink")，177个科，1589个属，31515个物种\[2\]。

传统上，担子菌门分为现在已废弃的两大类：

  - [异担子菌纲](../Page/异担子菌纲.md "wikilink") (Heterobasidiomycetes)
  - [同担子菌纲](../Page/同担子菌纲.md "wikilink") (Homobasidiomycetes)

## 参考资料

[担子菌门](../Category/担子菌门.md "wikilink")
[Category:植物病原体](../Category/植物病原体.md "wikilink")

1.
2.  Kirk, P.M., P.F. Cannon & J.A. Stalpers 2008. Dictionary of the
    Fungi, 10th ed., CABI. pp. 78–79.