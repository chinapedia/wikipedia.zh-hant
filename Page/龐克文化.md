[Punk_fashion_circa_1986.jpg](https://zh.wikipedia.org/wiki/File:Punk_fashion_circa_1986.jpg "fig:Punk_fashion_circa_1986.jpg")
**-{zh-hans:朋克; zh-hant:龐克; zh-sg: 庞克;}-**（**punk**），又译“-{zh-hans:朋克;
zh-hant:龐克;
zh-sg:庞克;}-”、“[鬅客](../Page/鬅客.md "wikilink")”。朋克文化是一種起源於1970年代的[次文化](../Page/次文化.md "wikilink")。最早源起於[音樂界](../Page/音樂.md "wikilink")，但逐漸轉換成一種整合音樂、[服裝與個人意識主張的廣義文化風格](../Page/服裝.md "wikilink")。

## 朋克音乐

今天我們所见到的龐克文化始于1970年代，由英國開始，最初的時候它是一種音樂上的叛逆運動，主旨是在抗拒反對一些包括[前衛搖滾](../Page/前衛搖滾.md "wikilink")（progressive
rock）、[重金属](../Page/重金属音乐.md "wikilink")（heavy
metal）在內，已經既存的流行音樂形式。在龐克文化的訴求中，這些流行音樂的歌手們早已因為過分英雄崇拜式的[偶像塑造](../Page/偶像.md "wikilink")，而與他們的歌迷們疏離太遠。朋克文化的追随者以[地下音樂](../Page/地下音樂.md "wikilink")（underground）、[極簡搖滾](../Page/極簡搖滾.md "wikilink")（minimalist
rock）等音樂形式為基礎，发展出他们自己[反乌托邦风格的音樂](../Page/反乌托邦.md "wikilink")。在龐克搖滾領域，[性手槍](../Page/性手槍.md "wikilink")（Sex
Pistols）與[雷蒙斯合唱團](../Page/雷蒙斯合唱團.md "wikilink")（The
Ramones）是龐克音樂啟蒙時期較為人所熟悉的代表性樂團。

渐渐的，朋克变得更加多样化并且更少最低限要求主义，这是随着像[The
Clash这样的乐队融入了如](../Page/The_Clash.md "wikilink")[ska和](../Page/ska.md "wikilink")[rockabilly甚或](../Page/rockabilly.md "wikilink")[hip-hop这样的其他地下音乐影响而发生的](../Page/hip-hop.md "wikilink")，但是音乐所散发的讯息还是一样的；它带有颠覆性、叛逆性以及[无政府主义](../Page/无政府主义.md "wikilink")。它取材于诸如面对社会的问题、下层阶级的被压迫等等这样的主题。朋克文化是对[社会的一个讯息](../Page/社会.md "wikilink")，也就是，不是所有人都过得很好、不是所有人都一样。

[英国的朋克文化在](../Page/英国.md "wikilink")1980年代和1990年代弥漫到了[北美](../Page/北美.md "wikilink")，在那里人们创造了一种新的音乐风格，称为[硬核朋克](../Page/硬核朋克.md "wikilink")（硬核、hardcore)、因为比[欧洲的同类音乐更快和更富进攻性的节奏而著称](../Page/欧洲.md "wikilink")。从这一新流派产生的风格包括[滑板朋克](../Page/滑板朋克.md "wikilink")（skate
punk）和[straight edge](../Page/straight_edge.md "wikilink")（直线边缘）。

从朋克的开始时起，主流唱片公司就试图从地下朋克文化获利。很多时候这遇到了许多阻力，因为朋克的理念在于音乐的纯洁性，而朋克常常觉得这受到了唱片公司牟利动机的威胁。

从1980年代晚期开始，美国[西北太平洋地区](../Page/西北太平洋地区.md "wikilink")（Pacific
Northwest）的朋克音乐"[油漬搖滾](../Page/油漬搖滾.md "wikilink")（grunge）"[上市](../Page/市场.md "wikilink")。成功的油漬搖滾乐队如[涅磐](../Page/涅槃乐队.md "wikilink")（Nirvana）和[Pearl
Jam他们的音乐明顯受到朋克风格影响](../Page/Pearl_Jam.md "wikilink")。这些早期的商业成功导致了另一朋克风格在主流中的成功，它被称为强力流行乐或者[流行朋克](../Page/流行朋克.md "wikilink")。流行朋克的乐队的例子有[Green
Day和](../Page/Green_Day.md "wikilink")[Offspring](../Page/Offspring.md "wikilink").

很多初期朋克[亚文化的追随者觉得朋克的商业化带来幻灭的感觉](../Page/亚文化.md "wikilink")。他们声称朋克按照定义就应该是不流行的(把"流行朋克"视为自相矛盾)，所以应该保持这个状态，因为它给主流文化带来了所需的挑战。

## 朋克设计

早期朋克的服装赋予日常物品以美学价值，比如将衣物撕裂再系以别针或捆带，使用颜料在衣物上作画和装饰，把黑色的垃圾塑料袋作为衬衫或衣裙，把扣针、剃须刀等作为饰物，他们还常常有意使用[皮革](../Page/皮革.md "wikilink")、[橡胶](../Page/橡胶.md "wikilink")、[乙烯基塑料作为衣物](../Page/乙烯基塑料.md "wikilink")，使人联想起犯罪倾向、[性暴力与](../Page/性暴力.md "wikilink")[性变态](../Page/性变态.md "wikilink")。总之是使自己的造型极度夸张、放荡不羁、充满挑衅性。但显而易见，朋克尽管气势咄咄、对日常生活及其中之用品，以及被附于其中的意义和标志胡捣乱搅，却从不拒斥它们。该运动本身就很商业化。随着经济的复苏，朋克激烈的标新立异也被融入[主流文化中](../Page/主流文化.md "wikilink")，成为[时尚](../Page/时尚.md "wikilink")。那种玩世不恭的时髦味和颠覆味的朋克语汇为[时装设计和](../Page/时装设计.md "wikilink")[时尚刊物带来的影响最显而易见](../Page/时尚刊物.md "wikilink")。把朋克服装或新浪潮服装带进主流文化的最重要设计师是英国人[薇薇安·魏斯伍德](../Page/薇薇安·魏斯伍德.md "wikilink")。另一位英国朋克服装设计师[赞德拉·罗德斯](../Page/赞德拉·罗德斯.md "wikilink")（Zandra
Rhodes，1940年生）作品风格更加温和，得到更多富人和名流认可。在视觉传达领域中，朋克同样经历了一个被驯化的过程，这尤其凸现于早期的[朋克刊物和后来出现的](../Page/朋克刊物.md "wikilink")、往往被误以为属于朋克文化的[时尚杂志之间的对比](../Page/时尚杂志.md "wikilink")。美国的《[朋克](../Page/龐克_\(雜誌\).md "wikilink")》（Punk）、英国的《[吸之毒](../Page/吸之毒.md "wikilink")》（Sniffin’
Glue）是具有代表性的朋克刊物，但均随着朋克文化被驯化这一过程而停刊。\[1\]

## 參看

  - 颜勇、黄虹编著：《西方设计：一部为生活制作艺术的历史》第五章第五节，长沙，湖南科学技术出版社，2010年。
  - [流行龐克](../Page/流行龐克.md "wikilink")

## 參考資料

[Category:次文化](../Category/次文化.md "wikilink")
[Category:搖滾樂](../Category/搖滾樂.md "wikilink")
[龐克](../Category/龐克.md "wikilink")

1.  参阅颜勇、黄虹编著：《西方设计：一部为生活制作艺术的历史》第586-592页，长沙，湖南科学技术出版社，2010年。