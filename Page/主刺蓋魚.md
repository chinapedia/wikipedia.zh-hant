**主刺蓋魚**（[学名](../Page/学名.md "wikilink")：，又名**條紋蓋刺魚**、**條紋棘蝶魚**，俗名**皇后神仙**、大花面）為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[鱸亞目](../Page/鱸亞目.md "wikilink")[蓋刺魚科](../Page/蓋刺魚科.md "wikilink")[刺盖鱼属的的其中一](../Page/刺盖鱼属.md "wikilink")[種](../Page/種.md "wikilink")[鱼类](../Page/鱼类.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[南非](../Page/南非.md "wikilink")、[東非](../Page/東非.md "wikilink")、[留尼旺](../Page/留尼旺.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[波斯灣](../Page/波斯灣.md "wikilink")、[阿曼灣](../Page/阿曼灣.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[印度](../Page/印度.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[西沙群岛](../Page/西沙群岛.md "wikilink")、[聖誕島](../Page/聖誕島.md "wikilink")、[可可群島](../Page/可可群島.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[中國](../Page/中國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[新幾內亞](../Page/新幾內亞.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")[昆士兰](../Page/昆士兰.md "wikilink")、[庫克群島](../Page/庫克群島.md "wikilink")、[阿莫土群岛](../Page/阿莫土群岛.md "wikilink")、[社会群岛](../Page/社会群岛.md "wikilink")、[關島](../Page/關島.md "wikilink")、[薩摩亞群島](../Page/薩摩亞群島.md "wikilink")、[馬里亞納群島](../Page/馬里亞納群島.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[夏威夷群島](../Page/夏威夷群島.md "wikilink")、[吉里巴斯](../Page/吉里巴斯.md "wikilink")、[東加](../Page/東加.md "wikilink")、[萬納杜](../Page/萬納杜.md "wikilink")、[法屬波里尼西亞等海域皆有其蹤跡](../Page/法屬波里尼西亞.md "wikilink")。该物种的模式产地在日本。\[1\]

## 深度

水深3\~70公尺。

## 特徵

本魚體略高而呈卵圓形；背部輪廓略突出，頭背於眼上方平直。吻鈍而小。眶前骨寬突，不游離；前鰓蓋骨後緣及下緣具弱鋸齒，具一長棘；鰓蓋骨後緣平滑。

幼魚體呈深[藍色底色](../Page/藍色.md "wikilink")，頭及身上有許多亮蓝色和[白色圈纹](../Page/白色.md "wikilink")。幼魚的身上橫紋在體後端呈弧形或環狀，隨成長而逐漸轉為15\~25條縱紋且顏色變黃。一般需要四年才完全变为成年色泽。家养的主刺盖鱼有可能保留幼年花色。

[Pomacanthus_imperator_(Emperor_angelfish)_juvenile.jpg](https://zh.wikipedia.org/wiki/File:Pomacanthus_imperator_\(Emperor_angelfish\)_juvenile.jpg "fig:Pomacanthus_imperator_(Emperor_angelfish)_juvenile.jpg")

成魚體為黃藍相間的縱紋，尾鰭[黃色](../Page/黃色.md "wikilink")，具黑眼帶，在胸鰭處有塊大橫斑，但此橫斑未達背部。體被中型圓鱗，具數個副鱗；頭具絨毛狀鱗，頰部與奇鰭具小鱗，前鼻孔大於後鼻孔，奇鰭鰭條在成長後會延長。背鰭硬棘14枚，軟條20\~22枚；臀鰭硬棘3枚，軟條20枚；背鰭與臀鰭軟條部後端截平；腹鰭尖，第一軟條延長，幾達臀鰭；尾鰭鈍圓形。體長可達40公分。

## 生態

本魚分布在有珊瑚礁的海域，遇驚嚇時成魚會發出「咯咯」聲嚇退來者，屬雜食性，以[海綿](../Page/海綿.md "wikilink")、附著生物和[藻類為食](../Page/藻類.md "wikilink")。具有領域性。

## 經濟利用

體色鮮明，為極受歡迎的[觀賞魚](../Page/觀賞魚.md "wikilink")。

## 参考文献

  -
<!-- end list -->

  -
[imperator](../Category/刺蓋魚屬.md "wikilink")
[PI](../Category/觀賞魚.md "wikilink")

1.