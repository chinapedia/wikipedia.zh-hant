**安格馬**（**Angmar**）（[辛達林語](../Page/辛達林語.md "wikilink")：鐵要塞）是[托爾金](../Page/托爾金.md "wikilink")（J.
R. R. Tolkien）小說[中土大陸的虛構王國](../Page/中土大陸.md "wikilink")\[1\]。

安格馬建立於[第三紀元](../Page/第三紀元.md "wikilink")1300年，位於[迷霧山脈](../Page/迷霧山脈.md "wikilink")（Misty
Mountains）極北附近，由[戒靈](../Page/戒靈.md "wikilink")（Ringwraiths）首領統治，他就是[安格馬巫王](../Page/安格馬巫王.md "wikilink")（Witch-king
of
Angmar），效力於黑暗魔君[索倫](../Page/索倫_\(魔戒\).md "wikilink")（Sauron）\[2\]。安格馬對[亞爾諾](../Page/亞爾諾.md "wikilink")（Arnor）後繼國的戰爭可推測是索倫為了擊敗[剛鐸](../Page/剛鐸.md "wikilink")（Gondor）的盟國。

## 地理歷史

安格馬的首都是[卡恩督](../Page/卡恩督.md "wikilink")（Carn
Dûm），主要居民是邪惡[人類](../Page/人類_\(中土大陸\).md "wikilink")。

安格馬建立後，旋即對[登丹人](../Page/登丹人.md "wikilink")（Dúnedain）王國[雅西頓](../Page/雅西頓.md "wikilink")（Arthedain）、[卡多蘭](../Page/卡多蘭.md "wikilink")（Cardolan）及[魯道爾](../Page/魯道爾.md "wikilink")（Rhudaur）進行侵略。安格馬征服了三國中最弱的魯道爾，以當地的山地人取代魯道爾的登丹國王。山地人（Hillmen）可能是[東方人首領](../Page/東方人_\(中土大陸\).md "wikilink")[烏格番](../Page/烏格番.md "wikilink")（Ulfang）的後裔。

在安格馬的控制下，魯道爾於第三紀元1356年入侵雅西頓，雅西頓國王[亞瑞吉來布一世](../Page/亞瑞吉來布一世.md "wikilink")（Argeleb
I）戰死。在卡多蘭的援軍協助下，雅西頓仍能維持[風雲頂](../Page/風雲頂.md "wikilink")（Weathertop）一線的防守。

第三紀元1409年，安格馬奇襲卡多蘭，卡多蘭覆亡。在這時候，魯道爾已不存在，只剩下登丹人在亞爾諾的最後一個王國雅西頓。雅西頓失去了盟國，獨力支撐了約500年。第三紀元1974年，安格馬積聚兵力，向雅西頓發動了最後的攻擊，攻陷雅西頓首都[佛諾斯特](../Page/佛諾斯特.md "wikilink")（Fornost），雅西頓覆亡。

翌年，剛鐸王子[埃阿努爾](../Page/埃阿努爾.md "wikilink")（Eärnur）率軍向雅西頓增援，但他的來援太遲了。他的軍隊聯同登丹人殘軍、[林頓](../Page/林頓.md "wikilink")（Lindon）的精靈及由[葛羅芬戴爾](../Page/葛羅芬戴爾.md "wikilink")（Glorfindel）率領的軍隊於[佛諾斯特戰役](../Page/佛諾斯特戰役.md "wikilink")（Battle
of
Fornost）擊敗了安格馬，但安格馬巫王並沒有戰死，他逃進了[魔多](../Page/魔多.md "wikilink")，安格馬覆亡。戰後，葛羅芬戴爾作出了一個著名的預言，指沒有任何男子能夠殺死安格馬巫王。安格馬雖然覆亡，但安格馬巫王完成了他的使命：登丹人在北方的力量永遠消失。

佛諾斯特戰役後，安格馬的力量徹底在迷霧山脈以西消失。同時，位於迷霧山脈以東的安格馬殘餘勢力亦被[洛汗人的祖先清除](../Page/洛汗人.md "wikilink")。

## 相關改編及影響

安格馬有出現在電子遊戲《[指环王Online：安格玛之影](../Page/指环王Online：安格玛之影.md "wikilink")》和《》\[3\]中。而在《[哈比人](../Page/哈比人電影系列.md "wikilink")》電影系列中，安格馬也有被簡單提及。

[土衛六上的](../Page/土衛六.md "wikilink")是以安格馬命名的\[4\]。

## 參考資料

<references />

[sv:Platser i Tolkiens
värld\#Angmar](../Page/sv:Platser_i_Tolkiens_värld#Angmar.md "wikilink")

[A](../Category/中土大陸的地理.md "wikilink")

1.
2.  David Day . *Tolkien : the Illustrated encyclopedia*. p. 60-61
3.
4.