**大奴湖**（，）是[加拿大第二大湖](../Page/加拿大.md "wikilink")（仅次于[大熊湖](../Page/大熊湖.md "wikilink")）。面积2.86
万平方千米，最大深度614米，蓄水量
2.088万亿立方米。上承[奴河](../Page/奴河.md "wikilink")，下注[马更些河](../Page/马更些河.md "wikilink")，冻期长，东北岸[耶洛奈夫为加拿大西北地区首府](../Page/耶洛奈夫.md "wikilink")。

## 參考文獻

[Category:加拿大湖泊](../Category/加拿大湖泊.md "wikilink")
[Category:馬更些河](../Category/馬更些河.md "wikilink")
[Category:西北地區地理](../Category/西北地區地理.md "wikilink")
[Category:冰河湖](../Category/冰河湖.md "wikilink")