**8月2日**是[阳历年的第](../Page/阳历.md "wikilink")214天（[闰年是](../Page/闰年.md "wikilink")215天），离一年的结束还有151天。

## 大事记

### 前4世紀

  - [前338年](../Page/前338年.md "wikilink")：[古希臘地區](../Page/古希臘.md "wikilink")[喀羅尼亞戰役](../Page/喀羅尼亞戰役.md "wikilink")，[馬其頓擊敗](../Page/馬其頓.md "wikilink")[雅典和](../Page/雅典.md "wikilink")[底比斯](../Page/底比斯_\(希臘\).md "wikilink")。

### 前3世紀

  - [前216年](../Page/前216年.md "wikilink")：在[第二次布匿战争中](../Page/第二次布匿战争.md "wikilink")，[汉尼拔率领的](../Page/汉尼拔.md "wikilink")[迦太基軍隊在](../Page/迦太基.md "wikilink")[坎尼战役中擊败](../Page/坎尼战役.md "wikilink")[罗马共和国的軍隊](../Page/罗马共和国.md "wikilink")。

### 17世紀

  - [1608年](../Page/1608年.md "wikilink")：[英国屠杀在](../Page/英国.md "wikilink")[托里岛避难的](../Page/托里岛.md "wikilink")[爱尔兰造反者](../Page/爱尔兰.md "wikilink")。

### 18世紀

  - [1790年](../Page/1790年.md "wikilink")：美国第一次[人口普查开始](../Page/人口普查.md "wikilink")。

### 19世紀

  - [1815年](../Page/1815年.md "wikilink")：[普鲁士](../Page/普鲁士.md "wikilink")、[奥地利](../Page/奥地利.md "wikilink")、英国和[俄国一致同意由英国作出关于囚禁](../Page/俄国.md "wikilink")[拿破仑一世的决定](../Page/拿破仑一世.md "wikilink")，于是拿破仑被放逐到[圣赫勒拿岛](../Page/圣赫勒拿岛.md "wikilink")。
  - [1830年](../Page/1830年.md "wikilink")：[法国国王](../Page/法国.md "wikilink")[查理十世在](../Page/查理十世_\(法兰西\).md "wikilink")[七月革命的压力下被迫退位](../Page/七月革命.md "wikilink")，流亡[英国](../Page/英国.md "wikilink")。
  - [1860年](../Page/1860年.md "wikilink")：[太平天国忠王](../Page/太平天国.md "wikilink")[李秀成大破](../Page/李秀成.md "wikilink")[洋枪队于](../Page/洋枪队.md "wikilink")[江苏](../Page/江苏.md "wikilink")[青浦](../Page/青浦.md "wikilink")，兵临[上海城下](../Page/上海.md "wikilink")。

### 20世紀

  - [1911年](../Page/1911年.md "wikilink")：[赵尔丰任](../Page/赵尔丰.md "wikilink")[四川总督](../Page/四川总督.md "wikilink")。
  - [1914年](../Page/1914年.md "wikilink")：[一战](../Page/一战.md "wikilink")：[德國軍隊入侵](../Page/德意志帝國.md "wikilink")[盧森堡](../Page/盧森堡.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：[庄士敦成为](../Page/庄士敦.md "wikilink")[溥仪的](../Page/溥仪.md "wikilink")[英语教师](../Page/英语.md "wikilink")。
  - [1921年](../Page/1921年.md "wikilink")：[中国共产党第一次全国代表大会在](../Page/中国共产党第一次全国代表大会.md "wikilink")[浙江](../Page/浙江.md "wikilink")[嘉兴南湖闭幕](../Page/嘉兴南湖.md "wikilink")。
  - [1922年](../Page/1922年.md "wikilink")：被[中国气象局列为](../Page/中国气象局.md "wikilink")20世纪十大气象灾害之首的[1922年汕头台风吹袭](../Page/1922年汕头台风.md "wikilink")[汕头](../Page/汕头.md "wikilink")，死亡5－10万人。
  - [1923年](../Page/1923年.md "wikilink")：[卡尔文·柯立芝继任](../Page/卡尔文·柯立芝.md "wikilink")[美国总统](../Page/美国总统.md "wikilink")。
  - [1927年](../Page/1927年.md "wikilink")：奉天（今[沈阳](../Page/沈阳.md "wikilink")）十万市民游行示威，反对[日本强行在](../Page/日本.md "wikilink")[吉林省](../Page/吉林省.md "wikilink")[临江县设立领事分馆](../Page/临江县.md "wikilink")。
  - [1934年](../Page/1934年.md "wikilink")：[德国](../Page/德国.md "wikilink")[聯邦大總統](../Page/聯邦大總統.md "wikilink")[兴登堡去世](../Page/保罗·冯·兴登堡.md "wikilink")，[总理](../Page/德国总理.md "wikilink")[希特勒接管总统职权](../Page/阿道夫·希特勒.md "wikilink")，成为[元首兼帝国总理](../Page/元首兼帝国总理.md "wikilink")。
  - [1935年](../Page/1935年.md "wikilink")：英国制定，把[印度划分为](../Page/印度.md "wikilink")11个自治省，但[英国国会保留最后控制权](../Page/英国国会.md "wikilink")。
  - [1939年](../Page/1939年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：物理学家[阿尔伯特·爱因斯坦致函](../Page/阿尔伯特·爱因斯坦.md "wikilink")[美国总统](../Page/美国总统.md "wikilink")[罗斯福](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")，认为[美国应当开始](../Page/美国.md "wikilink")[原子弹研究计划](../Page/原子弹.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：第二次世界大战：美国总统[杜鲁门](../Page/杜鲁门.md "wikilink")、[英国首相](../Page/英国首相.md "wikilink")[艾德礼及](../Page/艾德礼.md "wikilink")[苏联领袖](../Page/苏联.md "wikilink")[斯大林发表三国](../Page/斯大林.md "wikilink")《[波茨坦宣言](../Page/波茨坦宣言.md "wikilink")》。
  - [1949年](../Page/1949年.md "wikilink")：美国驻[中华民国大使](../Page/中华民国.md "wikilink")[司徒雷登离华返美](../Page/司徒雷登.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")：[中共中央在](../Page/中共中央.md "wikilink")[庐山召开八届八中全会](../Page/庐山.md "wikilink")（[庐山会议](../Page/庐山会议_\(1959年\).md "wikilink")），全会指责[彭德怀](../Page/彭德怀.md "wikilink")“右倾机会主义向党进攻，妄图篡党夺权的纲领”，宣称彭德怀、[黄克诚](../Page/黄克诚.md "wikilink")、[张闻天](../Page/张闻天.md "wikilink")、[周小舟结成了](../Page/周小舟.md "wikilink")“军事俱乐部”性质的反党集团。
  - [1972年](../Page/1972年.md "wikilink")：[香港海底隧道啟用](../Page/香港海底隧道.md "wikilink")，象徵[香港島與](../Page/香港島.md "wikilink")[九龍半島連成一體](../Page/九龍.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：[颱風荷貝吹襲](../Page/颱風荷貝.md "wikilink")[香港](../Page/香港.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：第65届[世界语大会在](../Page/世界语.md "wikilink")[斯德哥尔摩举行](../Page/斯德哥尔摩.md "wikilink")。
  - [1980年](../Page/1980年.md "wikilink")：[意大利](../Page/意大利.md "wikilink")[恐怖分子在](../Page/恐怖分子.md "wikilink")[-{zh-hant:波隆那;
    zh-hans:博洛尼亚;}-火车站制造](../Page/博洛尼亚.md "wikilink")[爆炸事件](../Page/博洛尼亞慘案.md "wikilink")，184人伤亡。
  - [1985年](../Page/1985年.md "wikilink")：[南非总统](../Page/南非.md "wikilink")[博塔宣布在南非](../Page/博塔.md "wikilink")36个城镇实行紧急状态法。
  - [1985年](../Page/1985年.md "wikilink")：由[洛克希德L-1011執行的](../Page/洛克希德L-1011.md "wikilink")[達美航空191號班機在降落](../Page/達美航空191號班機.md "wikilink")[達拉斯-沃斯堡國際機場時因](../Page/達拉斯-沃斯堡國際機場.md "wikilink")[微下擊暴流墜毀](../Page/微下擊暴流.md "wikilink")，136人（含1名地面人員）不幸罹難。
  - [1990年](../Page/1990年.md "wikilink")：[伊拉克入侵](../Page/伊拉克.md "wikilink")[科威特](../Page/科威特.md "wikilink")，[-{zh:海湾战争;
    zh-hans:海湾战争; zh-hant:波斯灣戰爭;}-爆发](../Page/海湾战争.md "wikilink")。
  - [1996年](../Page/1996年.md "wikilink")：[滑浪風帆選手](../Page/滑浪風帆.md "wikilink")[李麗珊在](../Page/李麗珊.md "wikilink")[亞特蘭大奧運會上為](../Page/亞特蘭大奧運會.md "wikilink")[香港奪得首枚奧運金牌](../Page/香港.md "wikilink")
  - [2000年](../Page/2000年.md "wikilink")：[香港](../Page/香港.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[入境事務大樓發生](../Page/入境事務大樓.md "wikilink")[縱火案](../Page/入境事務大樓縱火案.md "wikilink")，導致一名高級入境事務主任[梁錦光和爭取居港權人士](../Page/梁錦光.md "wikilink")[林小星被燒死](../Page/林小星.md "wikilink")。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：[联合国批准向](../Page/联合国.md "wikilink")[利比里亚派遣国际和平部队以维护当地秩序](../Page/利比里亚.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[法國航空358號班機在](../Page/法國航空358號班機事故.md "wikilink")[多倫多國際機場因天氣惡劣墜毀](../Page/多倫多國際機場.md "wikilink")，機上309人全數生還。
  - [2008年](../Page/2008年.md "wikilink")：[中国](../Page/中国.md "wikilink")[重庆第一贪](../Page/重庆.md "wikilink")[巫山县交通局原局长](../Page/巫山县.md "wikilink")[晏大彬被判死刑](../Page/晏大彬.md "wikilink")，其妻成全国洗钱犯第一人。

## 出生

  - [1891年](../Page/1891年.md "wikilink")：[阿瑟·布利斯爵士](../Page/阿瑟·布利斯.md "wikilink")，英國作曲家（[1975年逝世](../Page/1975年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[傑克·華納](../Page/傑克·華納.md "wikilink")，[華納兄弟電影公司創辦人](../Page/華納兄弟.md "wikilink")（[1978年逝世](../Page/1978年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[龚品梅](../Page/龚品梅.md "wikilink")，中国天主教[枢机](../Page/枢机.md "wikilink")（[2000年逝世](../Page/2000年.md "wikilink")）
  - [1923年](../Page/1923年.md "wikilink")：[方心讓](../Page/方心讓.md "wikilink")，[香港醫學界](../Page/香港.md "wikilink")、政界人物（[2009年逝世](../Page/2009年.md "wikilink")）
  - [1924年](../Page/1924年.md "wikilink")：[詹姆斯·鮑德溫](../Page/詹姆斯·鮑德溫.md "wikilink")，美國作家、小說家、詩人、劇作家和社會活動家（[1987年逝世](../Page/1987年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[彼得·奧圖](../Page/彼得·奧圖.md "wikilink")，[好萊塢演員](../Page/好萊塢.md "wikilink")（[2013年逝世](../Page/2013年.md "wikilink")）
  - [1942年](../Page/1942年.md "wikilink")：[伊莎貝·阿言德](../Page/伊莎貝·阿言德.md "wikilink")，[智利作家](../Page/智利.md "wikilink")
  - [1958年](../Page/1958年.md "wikilink")：[速水獎](../Page/速水獎.md "wikilink")，[日本動畫](../Page/日本動畫.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[崔健](../Page/崔健.md "wikilink")，中國搖滾歌手
  - [1963年](../Page/1963年.md "wikilink")：[鄭華娟](../Page/鄭華娟.md "wikilink")，台灣民歌手、詞曲作者、作家
  - [1964年](../Page/1964年.md "wikilink")：[瑪麗-露易斯·帕克](../Page/瑪麗-露易斯·帕克.md "wikilink")，美國演員
  - [1965年](../Page/1965年.md "wikilink")：[吳君如](../Page/吳君如.md "wikilink")，香港演員
  - [1969年](../Page/1969年.md "wikilink")：[安洁丽卡·里维拉](../Page/安洁丽卡·里维拉.md "wikilink")，[墨西哥](../Page/墨西哥.md "wikilink")[第一夫人](../Page/第一夫人.md "wikilink")、演員、歌手
  - 1969年：[加藤優子](../Page/加藤優子.md "wikilink")，[日本動畫](../Page/日本動畫.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[凱文·史密斯](../Page/凱文·史密斯.md "wikilink")，美國男導演
  - [1976年](../Page/1976年.md "wikilink")：[山姆·沃辛頓](../Page/山姆·沃辛頓.md "wikilink")，澳大利亚演员
  - [1979年](../Page/1979年.md "wikilink")：[白吉勝](../Page/白吉勝.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[梁正群](../Page/梁正群.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[吳鳳](../Page/吳承鳳.md "wikilink")，[土耳其主持人](../Page/土耳其.md "wikilink")、演員
  - [1981年](../Page/1981年.md "wikilink")：[朱尉銘](../Page/朱尉銘.md "wikilink")，[台灣棒球選手](../Page/台灣.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[林亞若](../Page/林亞若.md "wikilink")，台灣女作家
  - [1983年](../Page/1983年.md "wikilink")：[金廷娥](../Page/金廷娥.md "wikilink")，韓國女子團體[After
    School成员](../Page/After_School.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[逢坂良太](../Page/逢坂良太.md "wikilink")，[日本](../Page/日本.md "wikilink")[男性](../Page/男性.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[蔡昌憲](../Page/蔡昌憲.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[周湯豪](../Page/周湯豪.md "wikilink")，[台灣男歌手](../Page/台灣.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[李博翔](../Page/李博翔.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[曾文輝](../Page/曾文輝.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[查莉
    XCX](../Page/查莉_XCX.md "wikilink")，英国女歌手
  - [1996年](../Page/1996年.md "wikilink")：[賴慧如](../Page/賴慧如.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")、女歌手
  - [1998年](../Page/1998年.md "wikilink")：[蔡徐坤](../Page/蔡徐坤.md "wikilink")，中国男演員、男歌手，[NINE
    PERCENT隊長](../Page/NINE_PERCENT.md "wikilink")
  - [1999年](../Page/1999年.md "wikilink")：[李敏亨](../Page/李敏亨.md "wikilink")，韓國男子組合[NCT成員](../Page/NCT.md "wikilink")

## 逝世

  - [1589年](../Page/1589年.md "wikilink")：[亨利三世](../Page/亨利三世_\(法兰西\).md "wikilink")，[法國國王](../Page/法國國王.md "wikilink")（[1551年出生](../Page/1551年.md "wikilink")）
  - [1611年](../Page/1611年.md "wikilink")：[加藤清正](../Page/加藤清正.md "wikilink")，[日本戰國時代](../Page/日本戰國時代.md "wikilink")、[江戶時代初期的武將和](../Page/江戶時代.md "wikilink")[大名](../Page/大名.md "wikilink")，[熊本藩初代藩主](../Page/熊本藩.md "wikilink")，參與興建[熊本城](../Page/熊本城.md "wikilink")、[蔚山倭城](../Page/蔚山倭城.md "wikilink")、[江戶城](../Page/江戶城.md "wikilink")、[名古屋城等等的築城高手](../Page/名古屋城.md "wikilink")（[1562年出生](../Page/1562年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[亚历山大·格拉汉姆·贝尔](../Page/亚历山大·格拉汉姆·贝尔.md "wikilink")，[美国科学家](../Page/美国.md "wikilink")（[1847年出生](../Page/1847年.md "wikilink")）
  - [1923年](../Page/1923年.md "wikilink")：[沃伦·G·哈定](../Page/沃伦·G·哈定.md "wikilink")，[美國總統](../Page/美國總統.md "wikilink")（[1865年出生](../Page/1865年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[馮平山](../Page/馮平山.md "wikilink")，[香港企業家](../Page/香港.md "wikilink")、慈善家（[1860年出生](../Page/1860年.md "wikilink")）
  - [1934年](../Page/1934年.md "wikilink")：[保罗·冯·兴登堡](../Page/保罗·冯·兴登堡.md "wikilink")，[德国总统](../Page/德国总统.md "wikilink")（[1847年出生](../Page/1847年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[赵一曼](../Page/赵一曼.md "wikilink")，[东北抗日联军第三军第二团政委](../Page/东北抗日联军.md "wikilink")，在与[日军战斗中负伤被俘](../Page/日军.md "wikilink")，於當日在[珠河縣被處決](../Page/珠河縣.md "wikilink")（[1905年出生](../Page/1905年.md "wikilink")）
  - [1947年](../Page/1947年.md "wikilink")：[沈兼士](../Page/沈兼士.md "wikilink")，中国[语言文字学家](../Page/语言文字.md "wikilink")、[文献档案学家](../Page/文献.md "wikilink")。（[1887年出生](../Page/1887年.md "wikilink")）
  - [1981年](../Page/1981年.md "wikilink")：，[北爱尔兰绝食者](../Page/北爱尔兰.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[鄭君綿](../Page/鄭君綿.md "wikilink")，[香港演員](../Page/香港.md "wikilink")（[1917年出生](../Page/1917年.md "wikilink")）
  - [1996年](../Page/1996年.md "wikilink")：[穆罕默德·法拉赫·艾迪德](../Page/穆罕默德·法拉赫·艾迪德.md "wikilink")，[索马里军阀](../Page/索马里.md "wikilink")（[1934年出生](../Page/1934年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[威廉·柏洛茲](../Page/威廉·柏洛茲.md "wikilink"),
    美国作家（[1914年出生](../Page/1914年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[白文彪](../Page/白文彪.md "wikilink")，[香港演員](../Page/香港.md "wikilink")（[1921年出生](../Page/1921年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[赤塚不二夫](../Page/赤塚不二夫.md "wikilink")，[日本漫畫家](../Page/日本漫畫家.md "wikilink")（[1935年出生](../Page/1935年.md "wikilink")）

## 节假日和习俗

  -
## 參考資料