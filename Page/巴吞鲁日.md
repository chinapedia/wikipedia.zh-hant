**巴吞鲁日**（，，意为“红棍”）是[美国](../Page/美国.md "wikilink")[路易斯安那州首府](../Page/路易斯安那州.md "wikilink")、第二大城市和现时人口最多的城市，是[東巴吞魯日縣縣治](../Page/東巴吞魯日縣_\(路易斯安那州\).md "wikilink")。2005年人口為222,064人，该城原为路易斯安那州仅次于[新奥尔良的第二大城](../Page/新奥尔良.md "wikilink")，2005年[卡特里娜飓风袭击新奥尔良后](../Page/卡特里娜飓风.md "wikilink")，大批难民从新奥尔良逃亡至巴吞鲁日，使得巴吞鲁日人口暂时性地超过新奥尔良。

巴吞鲁日位于路易斯安那州东南部，[密西西比河南岸](../Page/密西西比河.md "wikilink")，面积204.8平方公里，2013年城区人口统计594309人，全市人口820159人。

## 姐妹城市

  - －[普羅旺斯地區艾克斯](../Page/普羅旺斯地區艾克斯.md "wikilink")

  - －[科爾多瓦](../Page/科爾多瓦.md "wikilink")

  - －[太子港](../Page/太子港.md "wikilink")

  - －[台中市](../Page/台中市.md "wikilink")

  - －[菏泽市](../Page/菏泽市.md "wikilink")

## 另見

  - [癌巷](../Page/癌巷.md "wikilink")

## 外部链接

  - [市政府官方网站](https://web.archive.org/web/20101026050217/http://brgov.com/)

[B](../Category/路易斯安那州城市.md "wikilink")
[B](../Category/美国各州首府.md "wikilink")