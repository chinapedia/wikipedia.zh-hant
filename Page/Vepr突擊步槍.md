**Vepr**（[烏克蘭語解為](../Page/烏克蘭語.md "wikilink")「野豬」）突擊步槍，是[烏克蘭在](../Page/烏克蘭.md "wikilink")2003年發佈的[突擊步槍](../Page/突擊步槍.md "wikilink")。

## 設計與歷史

目前烏克蘭軍隊主要採用[華沙公約國常用](../Page/华沙条约组织.md "wikilink")[AKM及](../Page/AKM突击步枪.md "wikilink")[AK-74作制式武器](../Page/AK-74突击步枪.md "wikilink")，而烏克蘭國防部宣佈Vepr突擊步槍預計在2010年裝備烏克蘭軍隊。\[1\]然而，由於烏克蘭的政治浪潮正在朝著北約，並可能会加入[歐洲聯盟](../Page/歐洲聯盟.md "wikilink")，加上烏克蘭槍械製造商RPC
Fort已經提供了一些使用西方口徑的槍械，（例如經[以色列軍事工業授權生產的](../Page/以色列軍事工業.md "wikilink")[IMI
TAR-21突擊步槍](../Page/IMI_TAR-21突擊步槍.md "wikilink")、[IMI
Galil狙擊步槍和](../Page/IMI_Galil突擊步槍.md "wikilink")[IMI
Negev輕機槍等](../Page/內蓋夫輕機槍.md "wikilink")）可能会令Vepr在烏克蘭軍中服務的地位受到威脅。現在除了一些海外部署的士兵会採用[TAR-21外](../Page/TAR-21.md "wikilink")，大部份的烏克蘭士兵仍旧是採用[AK-74](../Page/AK-74.md "wikilink")，而Vepr只会少量地裝備[特种部隊](../Page/特种部隊.md "wikilink")。

Vepr突擊步槍以[AK-74改造而成](../Page/AK-74突击步枪.md "wikilink")，大部份零件與AK-74相同，[5.45×39毫米口徑](../Page/5.45×39mm.md "wikilink")，由於改用[犢牛式設計](../Page/犢牛式.md "wikilink")，氣動系統和[轉拴式槍機的位置改為](../Page/轉拴式槍機.md "wikilink")[機匣後部](../Page/機匣.md "wikilink")，並加裝緩衝膠托，原有的改為設置在前護木左面，槍管長415毫米，可下掛[榴彈發射器](../Page/榴彈發射器.md "wikilink")。Vepr配有由烏克蘭自制的[紅點瞄準鏡](../Page/紅點鏡.md "wikilink")。

## 相關條目

  - [AK-74](../Page/AK-74突击步枪.md "wikilink")
  - [A-91](../Page/A-91突击步枪.md "wikilink")
  - [TKB-022](../Page/TKB-022突击步枪.md "wikilink")
  - [TKB-059](../Page/TKB-059三管突击步枪.md "wikilink")
  - [86S式](../Page/86S式自動步槍.md "wikilink")
  - [Bakalov](../Page/Bakalov突擊步槍.md "wikilink")
  - [OTs-14 Groza](../Page/OTs-14突擊步槍.md "wikilink")
  - [瓦爾梅特M82](../Page/瓦爾梅特M82突擊步槍.md "wikilink")
  - [Grad](../Page/Grad突擊步槍.md "wikilink")
  - [K-3](../Page/K3突擊步槍.md "wikilink")
  - [Khazri](../Page/Khazri突擊步槍.md "wikilink")
  - [Fort-221](../Page/Fort-221突擊步槍.md "wikilink")
  - [AKU-94](../Page/AKU-94突擊步槍.md "wikilink")

## 图片

<center>

<File:VEPR7.png> <File:VEPR6.png> <File:VEPR3.png> <File:VEPR5.png>

</center>

## 註釋

## 參考文獻

  - —[Modern
    Firearms-VEPR](https://web.archive.org/web/20070516153043/http://world.guns.ru/assault/as68-e.htm)</small>

## 外部链接

  - —[烏克蘭政府頁面](http://www.kmu.gov.ua/control/en/publish/article?art_id=2705680&cat_id=32598)

  - —[D boy gun
    world-Vepr突击步枪](http://firearmsworld.net/ukraine/vepr/vepr.htm)

[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:5.45×39毫米槍械](../Category/5.45×39毫米槍械.md "wikilink")
[Category:烏克蘭槍械](../Category/烏克蘭槍械.md "wikilink")

1.  [烏克蘭政府頁面-Experts with center for army, conversion and disarmament
    studies say Ukraine has good chances for getting established on
    international market of individual
    firearms](http://www.kmu.gov.ua/control/en/publish/printable_article?art_id=2705680)