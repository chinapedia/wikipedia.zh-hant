[APiTA.kounannishi1.JPG](https://zh.wikipedia.org/wiki/File:APiTA.kounannishi1.JPG "fig:APiTA.kounannishi1.JPG")
[UNY_Lok_Fu_Plaza_Store_Level_3_Enterance.jpg](https://zh.wikipedia.org/wiki/File:UNY_Lok_Fu_Plaza_Store_Level_3_Enterance.jpg "fig:UNY_Lok_Fu_Plaza_Store_Level_3_Enterance.jpg")[樂富廣場分店](../Page/樂富廣場.md "wikilink")\]\]
[APITA_Gifu_20130921.jpg](https://zh.wikipedia.org/wiki/File:APITA_Gifu_20130921.jpg "fig:APITA_Gifu_20130921.jpg")[岐阜市分店](../Page/岐阜市.md "wikilink")\]\]
[APITA_Supermarket_inside_Cityplaza_2014.jpg](https://zh.wikipedia.org/wiki/File:APITA_Supermarket_inside_Cityplaza_2014.jpg "fig:APITA_Supermarket_inside_Cityplaza_2014.jpg")超級市場\]\]

**生活創庫**（**UNY**，[日文](../Page/日文.md "wikilink")：****）是一家[日本的](../Page/日本.md "wikilink")[超級市場集團](../Page/超級市場.md "wikilink")，於日本[中部地方及](../Page/中部地方.md "wikilink")[關東地區經營](../Page/關東地區.md "wikilink")234家購物中心和超級市場、6300家便利店及1,500家專賣店。總部位於[愛知縣](../Page/愛知縣.md "wikilink")[稻澤市](../Page/稻澤市.md "wikilink")。

2015年10月15日[FamilyMart以每](../Page/FamilyMart.md "wikilink")0.138股換取生活創庫（Uny
Group）一股的比例，斥資1710億日圓（折合約111.98億港元）收購生活創庫，令FamilyMart成為日本第二大連鎖便利店經營商。

2016年9月1日：[FamilyMart和Circle](../Page/FamilyMart.md "wikilink") K
Sunkus（OK便利店）母公司UNY今日正式合併成Uny FamilyMart Holdings，旗下的「Circle
K」和「Sunkus」2個便利店品牌統一改用「FamilyMart」。

2017年，[唐吉訶德 (企業)与](../Page/唐吉訶德_\(企業\).md "wikilink") FamilyMart UNY
控股达成合作，获得 UNY 40% 的股份，双方联合以「唐吉軻德全家便利商店（MEGA Don Quijote
Uny）品牌联合运营店铺

2018年10月13日[FamilyMart以](../Page/FamilyMart.md "wikilink")2119亿日元（18.9億美元）入股[唐吉訶德
(企業)最多](../Page/唐吉訶德_\(企業\).md "wikilink") 20.17%的股份，预计成为后者最大股东，同時[唐吉訶德
(企業)](../Page/唐吉訶德_\(企業\).md "wikilink")，將支付2.56億美元，收購Uny剩餘60%股權

## 香港分店

[uny_logo.gif](https://zh.wikipedia.org/wiki/File:uny_logo.gif "fig:uny_logo.gif")
UNY於[香港的](../Page/香港.md "wikilink")[太古城中心二期](../Page/太古城中心.md "wikilink")，[德福廣場二期及](../Page/德福廣場.md "wikilink")[樂富廣場A區設有三間海外分店](../Page/樂富廣場.md "wikilink")，其中香港太古城分店於2007年5月2日起由UNY改為APITA。

2018年5月，[恒基發展以](../Page/恒基發展.md "wikilink")3億港元收購UNY香港零售業務。\[1\]

### UNY

以「獨特的商品推廣及市場營銷的零售商」為企業概念，由“UNIQUE”， “UNITED”， “UNIVERSAL”， “UNITY”，
“UNIFY”
五個英文單字已取名為UNY。「UNY」這個名稱被載入香港著名組合[軟硬天師的著名歌曲之一](../Page/軟硬天師.md "wikilink")《川保久齡大戰山本耀司》。UNY生活創庫[樂富店](../Page/樂富廣場.md "wikilink")\[2\]\[3\]於2010年6月1日起試業，並於6月5日開業，該店面積大約11萬呎。\[4\]

### PIAGO

[piago_logo.gif](https://zh.wikipedia.org/wiki/File:piago_logo.gif "fig:piago_logo.gif")
**PIAGO**為日式家居生活百貨，品牌名稱的靈感來自[意大利文](../Page/意大利文.md "wikilink")「Piacevole」和「Luogo」。
有「快樂」的「地方」之意，表示顧客在PIAGO購物能為他們帶來最愉快稱心的消費體驗。PIAGO[德福店於](../Page/德福廣場.md "wikilink")2010年12月17日起試業，並於12月20日開業，樓面面積為7萬平方呎。\[5\]\[6\]設超級市場、美食廣場、家品部和精品部，到2019年3月尾結業。\[7\]

### APITA

[About-1-2.gif](https://zh.wikipedia.org/wiki/File:About-1-2.gif "fig:About-1-2.gif")
為UNY集團的走高檔路線的品牌。APITA由[意大利文Apice](../Page/意大利文.md "wikilink")（先驅）及Tasca（口袋）兩個字組合而成，字面意思是「先驅的口袋」，即搜羅時代最先進的資訊集於口袋的形象。

APITA[太古城店由](../Page/太古城中心.md "wikilink")1987年開業的舊「生活創庫」UNY改裝，該店亦為UNY香港總部，至2013年7月1日才把總部遷入英皇道東達中心。太古城總店面積達18萬平方呎，設有四層。地庫為[超級市場](../Page/超級市場.md "wikilink")、家品電器及美食廣場；地面為玩具部及服裝部；一及二樓為服裝部及化妝品部。

2014年1月，業主太古地產為商場進行重組租戶工程，故APITA將於2月9日起暫時關閉，復活節再重開。工程完成後，APITA的地面和地庫兩層的超市、家品、文具、玩具、電器及美食廣場部份會保留，樓面面積為12萬平方呎。

### 私と生活

私と生活是UNY旗下的生活雜貨店，大部分貨品向日本100円雜貨店Seria（セリア）取貨並以12元發售。設有一間分店，位於UNY樂富店。

## 上海分店

上海首間分店於2010年開業。\[8\]

## 參見

  - [一田百貨](../Page/一田百貨.md "wikilink")

## 參考來源

<references />

## 外部連結

  - [名古屋巨蛋](../Page/名古屋巨蛋.md "wikilink")・天井付近の広告掲示地点
  - [中日龍](../Page/中日龍.md "wikilink")
  - [生活創庫](http://www.uny.co.jp/)
  - [生活創庫香港](http://www.unyhk.com/)

[Category:恒基兆業](../Category/恒基兆業.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:香港超級市場](../Category/香港超級市場.md "wikilink")
[Category:東京證券交易所上市公司](../Category/東京證券交易所上市公司.md "wikilink")
[Category:日本零售商](../Category/日本零售商.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:日本品牌](../Category/日本品牌.md "wikilink")
[Category:愛知縣公司](../Category/愛知縣公司.md "wikilink")

1.  [四叔斥三億購UNY港業務
    包括APITA及PIAGO](https://hk.news.appledaily.com/local/daily/article/20180525/20401049)，蘋果日報，2018年5月25日
2.  [《APiTA億元旗艦店
    進駐樂富》](http://hk.news.yahoo.com/article/080910/3/85r9.html)
    ，星島日報，2008年9月11日
3.  另於樂富廣場內之圍板所標示的品牌是『UNY生活創庫』而非『APITA』。
4.  [1](http://www.dahsing.com/en/pdf/credit_card/cc_promo_father_1005.pdf)
5.  [《PIAGO首登陸香港
    選址德福》](http://news.sina.com.hk/cgi-bin/nw/show.cgi/19/1/1/1491033/1.html)，明報，2010年4月16日
6.  [《港鐵11商場聖誕投資額2500萬》](http://property.mpfinance.com/cfm/pa3.cfm?File=20101108/pad01/m.txt)，明報，2010年11月8日
7.
8.  \[日式百貨店APiTA入駐上海
    <http://paper.wenweipo.com/2010/12/13/QB1012130001.htm>\]