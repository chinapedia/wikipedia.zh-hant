[俄羅斯科學院](../Page/俄羅斯科學院.md "wikilink")**克尔德什应用数学研究所**（[俄語](../Page/俄語.md "wikilink")：Институт
прикладной математики имени
М.В.Келдыша）是一所专门研究[計算数学的研究所](../Page/計算数学.md "wikilink")。研究所位於[俄罗斯](../Page/俄罗斯.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")，以[姆斯季斯拉夫·弗謝沃洛多維奇·克爾德什命名](../Page/姆斯季斯拉夫·弗謝沃洛多維奇·克爾德什.md "wikilink")。1966年研究所自[斯捷克洛夫數學研究所分离出來而成立](../Page/斯捷克洛夫數學研究所.md "wikilink")。

## 外部链接

  - [研究所网址](http://www.keldysh.ru/)

[К](../Category/数学研究所.md "wikilink")
[К](../Category/俄羅斯科學院.md "wikilink")
[К](../Category/1966年創建的教育機構.md "wikilink")