[Turgut_Özal_cropped.jpg](https://zh.wikipedia.org/wiki/File:Turgut_Özal_cropped.jpg "fig:Turgut_Özal_cropped.jpg")

**图尔古特·厄扎尔**（**Turgut
Özal**，），[土耳其政治领袖](../Page/土耳其.md "wikilink")，曾任[土耳其总理及](../Page/土耳其总理.md "wikilink")[总统](../Page/土耳其總統.md "wikilink")。

## 政治生涯

厄扎尔生于[马拉蒂亚省](../Page/马拉蒂亚省.md "wikilink")，大學畢業後曾經在美國攻讀工程。他長期在政府機關工作，並曾經擔任土耳其總理[苏莱曼·德米雷尔的助理](../Page/苏莱曼·德米雷尔.md "wikilink")。

在1980年軍事政變後，政變領袖任命其為內閣成員，並於1980年9月至1982年7月任副总理兼外交部长，1983年5月组建[祖国党](../Page/祖国党_\(土耳其\).md "wikilink")，并任主席。同年12月任政府总理。他於1983年及1987年的兩次大選中領導祖國黨勝出。1989年11月，獲大國民議會选为土耳其第八任总統。

厄扎尔最大的政績，是使大量土耳其的國營企業民營化，同時在民營化的過程中產生了許多新的中產階級，這些中產階級多半是安納托利亞半島上的傳統伊斯蘭教徒，因此也有人批評他劫掠傳統以[伊斯坦堡為根據地的資產階級的利益來培養他的支持者](../Page/伊斯坦堡.md "wikilink")。[蘇聯解體後](../Page/蘇聯解體.md "wikilink")，厄扎尔一直與[北約維持關係](../Page/北約.md "wikilink")，也試圖加強土耳其跟中亞突厥語系國家的聯繫，並規劃建立一個類似[歐盟的突厥共同體](../Page/歐盟.md "wikilink")。

他在1993年總統任內死於心臟病，其家人懷疑他是被暗殺，但之後的化驗結果卻未能證實。\[1\]\[2\]

## 資料來源

[Category:1927年出生](../Category/1927年出生.md "wikilink")
[Category:1993年逝世](../Category/1993年逝世.md "wikilink")
[Category:土耳其總理](../Category/土耳其總理.md "wikilink")
[Category:土耳其总统](../Category/土耳其总统.md "wikilink")
[Category:伊斯坦布尔理工大学校友](../Category/伊斯坦布尔理工大学校友.md "wikilink")
[Category:土耳其庫德人](../Category/土耳其庫德人.md "wikilink")

1.
2.