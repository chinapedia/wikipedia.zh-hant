**NHK-FM頻率**（****）為[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）的[超短波放送](../Page/超短波放送.md "wikilink")（FM放送）頻率。與[民間放送](../Page/民間放送.md "wikilink")（民放）的FM廣播不同的是，除了流行音樂以外，也有[古典音樂及](../Page/古典音樂.md "wikilink")[世界音樂](../Page/世界音樂.md "wikilink")、[邦樂](../Page/邦樂.md "wikilink")、[民謡](../Page/民謡.md "wikilink")、古典藝能、[廣播劇等節目播出](../Page/廣播劇.md "wikilink")，另有部分时段同步播出NHK第1广播的全国及地方新闻。

## 各地播出频率

**粗体**为基干局，标有“※”的为1969年3月1日正式开播，标有“◆”的为小功率覆盖。

<table>
<thead>
<tr class="header">
<th><p>地区</p></th>
<th><p>放送局</p></th>
<th><p>呼号</p></th>
<th><p>频率<br />
（MHz）</p></th>
<th><p>功率<br />
（W）</p></th>
<th><p>开播日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a></p></td>
<td><p><strong><a href="../Page/NHK札幌放送局.md" title="wikilink">札幌</a></strong>※</p></td>
<td><p>JOIK-FM</p></td>
<td><p>85.2</p></td>
<td><p>5k</p></td>
<td><p>1962年12月2日</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK函館放送局.md" title="wikilink">函館</a>※</p></td>
<td><p>JOVK-FM</p></td>
<td><p>87.0</p></td>
<td><p>250</p></td>
<td><p>1964年12月2日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK旭川放送局.md" title="wikilink">旭川</a>※</p></td>
<td><p>JOCG-FM</p></td>
<td><p>85.8</p></td>
<td><p>500</p></td>
<td><p>1964年6月25日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>※</p></td>
<td><p>JOOG-FM</p></td>
<td><p>87.5</p></td>
<td><p>250</p></td>
<td><p>1964年12月2日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK釧路放送局.md" title="wikilink">釧路</a>※</p></td>
<td><p>JOPG-FM</p></td>
<td><p>88.5</p></td>
<td><p>250</p></td>
<td><p>1965年2月5日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK北見放送局.md" title="wikilink">北見</a>※（網走送信所）</p></td>
<td><p>JOKP-FM</p></td>
<td><p>86.0</p></td>
<td><p>250</p></td>
<td><p>1965年3月27日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK室蘭放送局.md" title="wikilink">室蘭</a>※</p></td>
<td><p>JOIQ-FM</p></td>
<td><p>88.0</p></td>
<td><p>250</p></td>
<td><p>1964年12月2日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/东北地方_(日本).md" title="wikilink">東北</a></p></td>
<td><p><strong><a href="../Page/NHK仙台放送局.md" title="wikilink">仙台</a></strong>※</p></td>
<td><p>JOHK-FM</p></td>
<td><p>82.5</p></td>
<td><p>5k</p></td>
<td><p>1962年12月2日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK秋田放送局.md" title="wikilink">秋田</a>※</p></td>
<td><p>JOUK-FM</p></td>
<td><p>86.7</p></td>
<td><p>3k</p></td>
<td><p>1964年6月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK山形放送局.md" title="wikilink">山形</a>※</p></td>
<td><p>JOJG-FM</p></td>
<td><p>82.1</p></td>
<td><p>1k</p></td>
<td><p>1964年12月2日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK盛岡放送局.md" title="wikilink">盛岡</a>※</p></td>
<td><p>JOQG-FM</p></td>
<td><p>83.1</p></td>
<td><p>1k</p></td>
<td><p>1964年6月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK福島放送局.md" title="wikilink">福島</a>※</p></td>
<td><p>JOFP-FM</p></td>
<td><p>85.3</p></td>
<td><p>1k</p></td>
<td><p>1964年7月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK青森放送局.md" title="wikilink">青森</a>※</p></td>
<td><p>JOTG-FM</p></td>
<td><p>86.0</p></td>
<td><p>3k</p></td>
<td><p>1964年12月2日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/关东地方.md" title="wikilink">关东</a>、<a href="../Page/甲信越.md" title="wikilink">甲信越</a></p></td>
<td><p><strong><a href="../Page/NHK放送中心.md" title="wikilink">東京</a></strong>※</p></td>
<td><p>JOAK-FM</p></td>
<td><p>82.5</p></td>
<td><p>7k</p></td>
<td><p>1957年12月24日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK長野放送局.md" title="wikilink">長野</a>※</p></td>
<td><p>JONK-FM</p></td>
<td><p>84.0</p></td>
<td><p>500◆</p></td>
<td><p>1964年7月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK新潟放送局.md" title="wikilink">新潟</a>※</p></td>
<td><p>JOQK-FM</p></td>
<td><p>82.3</p></td>
<td><p>1k</p></td>
<td><p>1964年7月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK甲府放送局.md" title="wikilink">甲府</a>※</p></td>
<td><p>JOKG-FM</p></td>
<td><p>85.6</p></td>
<td><p>1k</p></td>
<td><p>1965年3月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK横浜放送局.md" title="wikilink">横浜</a></p></td>
<td><p>JOGP-FM</p></td>
<td><p>81.9</p></td>
<td><p>5k</p></td>
<td><p>1970年6月22日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK前橋放送局.md" title="wikilink">前橋</a></p></td>
<td><p>JOTP-FM</p></td>
<td><p>81.6</p></td>
<td><p>1k</p></td>
<td><p>1970年3月20日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK水户放送局.md" title="wikilink">水户</a></p></td>
<td><p>JOEP-FM</p></td>
<td><p>83.2</p></td>
<td><p>1k</p></td>
<td><p>1970年3月28日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK千葉放送局.md" title="wikilink">千葉</a></p></td>
<td><p>JOMP-FM</p></td>
<td><p>80.7</p></td>
<td><p>5k</p></td>
<td><p>1971年3月28日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK宇都宫放送局.md" title="wikilink">宇都宫</a></p></td>
<td><p>JOBP-FM</p></td>
<td><p>80.3</p></td>
<td><p>1k</p></td>
<td><p>1970年3月31日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK琦玉放送局.md" title="wikilink">琦玉</a></p></td>
<td><p>JOLP-FM</p></td>
<td><p>85.1</p></td>
<td><p>5k</p></td>
<td><p>1971年3月26日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中部地方.md" title="wikilink">中部</a></p></td>
<td><p><strong><a href="../Page/NHK名古屋放送局.md" title="wikilink">名古屋</a></strong>※</p></td>
<td><p>JOCK-FM</p></td>
<td><p>82.5</p></td>
<td><p>10k</p></td>
<td><p>1962年12月2日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK金泽放送局.md" title="wikilink">金泽</a>※</p></td>
<td><p>JOJK-FM</p></td>
<td><p>82.2</p></td>
<td><p>1k</p></td>
<td><p>1964年7月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK静岡放送局.md" title="wikilink">静岡</a>※</p></td>
<td><p>JOPK-FM</p></td>
<td><p>88.8</p></td>
<td><p>1k</p></td>
<td><p>1964年4月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK福井放送局.md" title="wikilink">福井</a>※</p></td>
<td><p>JOFG-FM</p></td>
<td><p>83.4</p></td>
<td><p>1k</p></td>
<td><p>1965年2月5日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK富山放送局.md" title="wikilink">富山</a>※</p></td>
<td><p>JOIG-FM</p></td>
<td><p>81.5</p></td>
<td><p>1k</p></td>
<td><p>1964年7月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK津放送局.md" title="wikilink">津</a></p></td>
<td><p>JONP-FM</p></td>
<td><p>81.8</p></td>
<td><p>3k</p></td>
<td><p>1970年3月28日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK岐阜放送局.md" title="wikilink">岐阜</a></p></td>
<td><p>JOOP-FM</p></td>
<td><p>83.6</p></td>
<td><p>1k</p></td>
<td><p>1971年3月26日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/近畿地方.md" title="wikilink">近畿</a></p></td>
<td><p><strong><a href="../Page/NHK大阪放送局.md" title="wikilink">大阪</a></strong>※</p></td>
<td><p>JOBK-FM</p></td>
<td><p>88.1</p></td>
<td><p>10k</p></td>
<td><p>1958年2月20日</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK京都放送局.md" title="wikilink">京都</a></p></td>
<td><p>JOOK-FM</p></td>
<td><p>82.8</p></td>
<td><p>1k◆</p></td>
<td><p>1971年3月27日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK神户放送局.md" title="wikilink">神户</a></p></td>
<td><p>JOPP-FM</p></td>
<td><p>86.5</p></td>
<td><p>500◆</p></td>
<td><p>1970年3月27日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK和歌山放送局.md" title="wikilink">和歌山</a></p></td>
<td><p>JORP-FM</p></td>
<td><p>84.7</p></td>
<td><p>500</p></td>
<td><p>1970年4月20日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK奈良放送局.md" title="wikilink">奈良</a></p></td>
<td><p>JOUP-FM</p></td>
<td><p>87.4</p></td>
<td><p>500</p></td>
<td><p>1971年3月27日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK大津放送局.md" title="wikilink">大津</a></p></td>
<td><p>JOQP-FM</p></td>
<td><p>84.0</p></td>
<td><p>1k</p></td>
<td><p>1970年12月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中国地方.md" title="wikilink">中国</a></p></td>
<td><p><strong><a href="../Page/NHK广岛放送局.md" title="wikilink">广岛</a></strong>※</p></td>
<td><p>JOFK-FM</p></td>
<td><p>88.3</p></td>
<td><p>1k</p></td>
<td><p>1962年9月17日</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK岡山放送局.md" title="wikilink">岡山</a>※</p></td>
<td><p>JOKK-FM</p></td>
<td><p>88.7</p></td>
<td><p>1k</p></td>
<td><p>1964年4月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK松江放送局.md" title="wikilink">松江</a>※</p></td>
<td><p>JOTK-FM</p></td>
<td><p>84.5</p></td>
<td><p>500</p></td>
<td><p>1964年7月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK鳥取放送局.md" title="wikilink">鳥取</a>※</p></td>
<td><p>JOLG-FM</p></td>
<td><p>85.8</p></td>
<td><p>500</p></td>
<td><p>1964年12月2日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK山口放送局.md" title="wikilink">山口</a>※</p></td>
<td><p>JOUG-FM</p></td>
<td><p>85.3</p></td>
<td><p>500◆</p></td>
<td><p>1964年4月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/四国地方.md" title="wikilink">四国</a></p></td>
<td><p><strong><a href="../Page/NHK松山放送局.md" title="wikilink">松山</a></strong>※</p></td>
<td><p>JOZK-FM</p></td>
<td><p>87.7</p></td>
<td><p>1k</p></td>
<td><p>1962年12月2日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK高知放送局.md" title="wikilink">高知</a>※</p></td>
<td><p>JORK-FM</p></td>
<td><p>87.5</p></td>
<td><p>500</p></td>
<td><p>1964年4月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK德島放送局.md" title="wikilink">德島</a>※</p></td>
<td><p>JOXK-FM</p></td>
<td><p>83.4</p></td>
<td><p>1k</p></td>
<td><p>1965年3月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK高松放送局.md" title="wikilink">高松</a>※</p></td>
<td><p>JOHP-FM</p></td>
<td><p>86.0</p></td>
<td><p>1k</p></td>
<td><p>1965年3月22日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/九州地方.md" title="wikilink">九州</a>、<a href="../Page/冲绳.md" title="wikilink">冲绳</a></p></td>
<td><p><strong><a href="../Page/NHK福岡放送局.md" title="wikilink">福岡</a></strong>※</p></td>
<td><p>JOLK-FM</p></td>
<td><p>84.8</p></td>
<td><p>3k</p></td>
<td><p>1962年9月17日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK北九州放送局.md" title="wikilink">北九州</a>※</p></td>
<td><p>JOSK-FM</p></td>
<td><p>85.7</p></td>
<td><p>250</p></td>
<td><p>1964年5月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK熊本放送局.md" title="wikilink">熊本</a>※</p></td>
<td><p>JOGK-FM</p></td>
<td><p>85.4</p></td>
<td><p>1k</p></td>
<td><p>1962年12月2日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK長崎放送局.md" title="wikilink">長崎</a>※</p></td>
<td><p>JOAG-FM</p></td>
<td><p>84.5</p></td>
<td><p>500◆</p></td>
<td><p>1964年5月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK鹿儿島放送局.md" title="wikilink">鹿儿島</a>※</p></td>
<td><p>JOHG-FM</p></td>
<td><p>85.6</p></td>
<td><p>1k</p></td>
<td><p>1964年7月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK宫崎放送局.md" title="wikilink">宫崎</a>※</p></td>
<td><p>JOMG-FM</p></td>
<td><p>86.2</p></td>
<td><p>500◆</p></td>
<td><p>1964年7月1日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/NHK大分放送局.md" title="wikilink">大分</a>※</p></td>
<td><p>JOIP-FM</p></td>
<td><p>88.9</p></td>
<td><p>1k</p></td>
<td><p>1965年3月1日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/NHK佐賀放送局.md" title="wikilink">佐賀</a>※</p></td>
<td><p>JOSP-FM</p></td>
<td><p>81.6</p></td>
<td><p>500</p></td>
<td><p>1965年3月22日</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>JOAP-FM</p></td>
<td><p>88.1</p></td>
<td><p>1k</p></td>
<td><p>1974年3月24日</p></td>
<td></td>
</tr>
</tbody>
</table>

## 与NHK广播第1频率同步的时段

  - 平日：1:00-5:00、6:55-7:25、11:50-13:00、18:50-19:30
  - 周末：1:00-5:00、6:55-7:20、11:50-13:00、18:50-19:20

## 外部链接

  - [NHK-FM](http://www.nhk.or.jp/fm/)

[Category:NHK](../Category/NHK.md "wikilink")