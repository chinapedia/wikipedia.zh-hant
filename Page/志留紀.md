**志留紀**（**Silurian**）是[地质时代表](../Page/地质时代表.md "wikilink")[古生代的第三个纪](../Page/古生代.md "wikilink")，约开始于4.4亿年前，结束于4.1亿年前。

志留系一名源于威尔士地区一个古老部族。1835年，英国R.I.莫企逊，在[威尔士地区建立了广义的志留系](../Page/威尔士.md "wikilink")，对岩系作了划分，用笔石与壳相化石进行对比。1879年，把莫企逊广义的志留系下层命名为奥陶系。中文名源自旧时日本人使用日语汉字音读的音译名“志留纪”（音读：シルキ，罗马字：siruki）。

## 地层

志留纪地层在世界分布较广，浅海沉积在亚、欧、美洲的大部分地区，及澳大利亚的部分地区。非洲、南极洲大部分为陆地。

志留纪的分层系统，[笔石为首选门类](../Page/笔石.md "wikilink")，兰多维利、文洛克、罗德洛3个统均在英国确立。此外，在挪威南部、加拿大东部的安蒂科斯蒂岛、瑞典的哥德兰岛、乌克兰的波多利亚地区、捷克和斯洛伐克的布拉格地区，都有发育良好的志留纪地层、生物群。

志留系的顶界，已选定均一单笔石（Monoraptusuniformis）生物带的底为界，选择捷克、斯洛伐克的巴兰德地区克伦克剖面作为其界线层型剖面。志留系的底界，用笔石（Parakidoraptus
acuminatus）带的底界为界，界线层型选在苏格兰莫发地区的道勃斯林（Dobs
Linn）剖面。由于该剖面地质构造复杂，岩相单一，化石单调，沉积环境不适宜于底栖生物，所以这个方案尚待继续检验。

志留系的再分，包括4个统：

  - 兰多维利统。标准地区在英国威尔士南部达费德的兰多维利镇周围。分3个阶：鲁丹阶、阿埃罗尼安阶、特利奇阶。
  - 文洛克统。标准地区在英格兰什罗普郡的文洛克地区。分2个阶：申伍德阶、侯默阶。
  - 罗德洛统。标准地区在英格兰什罗普郡的罗德洛地区。分2个阶，戈斯特阶、卢德福德阶。
  - 普里多利统。标准地区在捷克、斯洛伐克的巴兰德地区。

## 生物

志留纪的无脊椎动物，与奥陶纪生物关系密切，许多种类在经历奥陶纪末灭绝事件后，进入一个新的复苏阶段。

[笔石是海洋漂浮生态域中引人注目的生物](../Page/笔石.md "wikilink")，笔石以[单笔石类为主](../Page/单笔石.md "wikilink")，如单笔石、[弓笔石](../Page/弓笔石.md "wikilink")、[锯笔石](../Page/锯笔石.md "wikilink")、[耙笔石等](../Page/耙笔石.md "wikilink")。笔石分布广，演化快，在地层对比中有独特的价值。

在浅海[底栖生物中](../Page/底栖生物.md "wikilink")，[腕足动物的数量最多](../Page/腕足动物.md "wikilink")。壳长可超过80厘米，发育匙形台、[五房贝族是最具特征的一类](../Page/五房贝.md "wikilink")，始于[晚奥陶世](../Page/晚奥陶世.md "wikilink")，到志留纪鼎盛。[无洞贝族](../Page/无洞贝族.md "wikilink")、[无窗贝族](../Page/无窗贝族.md "wikilink")、[石燕族](../Page/石燕族.md "wikilink")，自[奥陶纪之后](../Page/奥陶纪.md "wikilink")，稳定发展，后来成为腕足动物的主要族群。在奥陶纪大量繁殖的[正形贝](../Page/正形贝.md "wikilink")、[扭月贝](../Page/扭月贝.md "wikilink")，到志留纪明显衰落。

[珊瑚和](../Page/珊瑚.md "wikilink")[层孔虫也较繁盛](../Page/层孔虫.md "wikilink")，常形成生物礁、生物丘、生物层。[四射珊瑚](../Page/四射珊瑚.md "wikilink")、[床板珊瑚](../Page/床板珊瑚.md "wikilink")、[日射珊瑚常发现于](../Page/日射珊瑚.md "wikilink")[兰多弗里世晚期以来的海中](../Page/兰多弗里世.md "wikilink")，数量种类繁多，这几种珊瑚、层孔虫至[泥盆纪到达鼎盛](../Page/泥盆纪.md "wikilink")，但今早已灭绝。

与奥陶纪相比，[鹦鹉螺明显减少](../Page/鹦鹉螺.md "wikilink")，许多种类已告灭绝；中国扬子区盛产以[四川角石](../Page/四川角石.md "wikilink")(Sichunocers)为主的鹦鹉螺。[海百合类是最为繁盛的一种棘皮动物](../Page/海百合.md "wikilink")，在中国[兰多维利统中常见的](../Page/兰多维利统.md "wikilink")[花瓣海百合](../Page/花瓣海百合.md "wikilink")（Petlocrinus）；个体形状与现代的差别很大。[三叶虫到志留纪明显衰落](../Page/三叶虫.md "wikilink")；只在局部可见，如中国扬子区的[王冠虫](../Page/王冠虫.md "wikilink")(Coronocephlus)；[板足鲎类](../Page/板足鲎.md "wikilink")(Eurypterids)是志留纪[无脊椎动物中的食肉类代表](../Page/无脊椎动物.md "wikilink")，能游泳，栖息于海洋，半咸水甚至淡水中。

[牙形石演化快](../Page/牙形石.md "wikilink")、分布广，继笔石之后，对比志留纪地层的又一重要的化石。志留纪保存了可靠的最早[鱼类化石](../Page/鱼类.md "wikilink")，但数量少；中国志留纪的鱼化石相对较多，最早见于兰多弗里世的晚期地层。

## 地史

志留纪初期，南极冰盖迅速消融，导致志留纪海洋、大气环流减弱，纬向气候分带不明显，深海部分相对较暖，含氧量较低，易成滞流。除高纬度的[冈瓦纳大陆外](../Page/冈瓦纳大陆.md "wikilink")，大都处于干热、温暖的气候下。

  - 在兰多维利世初期，海平面快速上升，带来普遍的缺氧环境，广布的黑色笔石页岩；
  - 碳酸盐岩和生物礁的广泛分布，在北美、北欧尤为明显，显示了较温暖的古气候特点；
  - 志留纪的红层普遍发育，在中国华南、[西伯利亚](../Page/西伯利亚.md "wikilink")、[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")、[波罗的海等板块中均有发现](../Page/波罗的海.md "wikilink")；
  - 志留纪的蒸发岩发育在西伯利亚、澳大利亚等，反映了干旱、炎热的古气候面貌。

## 参考文献

  - Cesare Emiliani.(1992).*Planet Earth : Cosmology, Geology,& the
    Evolution of Life & the Environment*. Cambridge University
    Press.(Paperback Edition ISBN 0-521-40949-7)
  - Mikulic, DG, DEG Briggs, and J Kluessendorf. 1985. A new
    exceptionally preserved biota from the Lower Silurian of Wisconsin,
    USA. Philosophical Transactions of the Royal Society of London,
    311B:75-86.
  - Moore, RA, DEG Briggs, SJ Braddy, LI Anderson, DG Mikulic, and J
    Kluessendorf. 2005. A new synziphosurine (Chelicerata: Xiphosura)
    from the Late Llandovery (Silurian) Waukesha Lagerstatte, Wisconsin,
    USA. Journal of Paleontology:79(2), pp. 242-250.
  - Ogg, Jim; June, 2004, *Overview of Global Boundary Stratotype
    Sections and Points (GSSP's)* <http://www.stratigraphy.org/gssp.htm>
    Accessed April 30, 2006.

## 外部連結

  - [Palaeos:](https://web.archive.org/web/20100202003709/http://www.palaeos.com/Paleozoic/Silurian/Silurian.htm#history)
    Silurian

  - [UCMP
    Berkeley:](http://www.ucmp.berkeley.edu/silurian/silurian.html) The
    Silurian

  - [Paleoportal: Silurian strata in U.S., state by
    state](https://web.archive.org/web/20040704011423/http://www.paleoportal.org/time_space/period.php?period_id=14)

  - [USGS:Silurian and Devonian Rocks
    (U.S.)](https://web.archive.org/web/20060824004451/http://tapestry.usgs.gov/ages/silurdevon.html)

  -
  - [Examples of Silurian
    Fossils](http://www.geo-lieven.com/erdzeitalter/silur/silur.htm)

  - [GeoWhen Database for the
    Silurian](http://www.stratigraphy.org/geowhen/stages/Silurian.html)

[纪](../Category/地质年代.md "wikilink")