**哥利·史提夫**（**Colly Barnes
Ezeh**，），生於[尼日利亞](../Page/尼日利亞.md "wikilink")，[尼日利亞裔](../Page/尼日利亞.md "wikilink")[香港足球運動員](../Page/香港.md "wikilink")，原為[尼日利亞公民](../Page/尼日利亞.md "wikilink")，居港滿7年後已擁有[香港特別行政區護照](../Page/香港特別行政區護照.md "wikilink")，成為[香港永久性居民](../Page/香港永久性居民.md "wikilink")。曾代表[香港出賽](../Page/香港足球代表隊.md "wikilink")[亞洲盃賽事](../Page/亞洲盃.md "wikilink")，現時效力於[香港丁組足球聯賽球會光華](../Page/香港丁組足球聯賽.md "wikilink")。

## 球員生涯

史提夫生於[尼日利亞](../Page/尼日利亞.md "wikilink")，首先效力[西貢朋友和](../Page/西貢朋友足球會.md "wikilink")[流浪](../Page/香港流浪足球會.md "wikilink")，之後轉戰[印度加盟](../Page/印度.md "wikilink")[莫亨巴根球會](../Page/莫亨巴根競技俱樂部.md "wikilink")。2001年，重返[香港效力](../Page/香港.md "wikilink")[花花及](../Page/花花足球會.md "wikilink")[愉園](../Page/愉園體育會.md "wikilink")。2002年，史提夫前往[孟加拉加盟](../Page/孟加拉.md "wikilink")[阿巴罕尼](../Page/阿巴罕尼達卡足球會.md "wikilink")，翌年再次回歸[香港](../Page/香港.md "wikilink")，先後效力過[愉園](../Page/愉園體育會.md "wikilink")、[香雪晨曦](../Page/晨曦足球會.md "wikilink")、[沙田](../Page/沙田體育會.md "wikilink")、[淦源等多支球隊](../Page/淦源足球會.md "wikilink")。2009年夏天，史堤夫轉投[香港乙組足球聯賽球隊](../Page/香港乙組足球聯賽.md "wikilink")[福建](../Page/福建體育會.md "wikilink")。

## 國際賽生涯

2006年，史堤夫代表[香港出賽](../Page/香港足球代表隊.md "wikilink")[2007年亞洲盃](../Page/2007年亞洲盃外圍賽.md "wikilink")。2006年11月15日，史堤夫在[2007年亞洲盃外圍賽對的賽事末段後備入替](../Page/2007年亞洲盃外圍賽.md "wikilink")，[香港隊以](../Page/香港足球代表隊.md "wikilink")2–0擊敗[孟加拉](../Page/孟加拉國家足球隊.md "wikilink")，在[亞洲盃外圍賽F組賽事](../Page/2007年亞洲盃外圍賽.md "wikilink")6戰得8分，以第3名完成小組所有賽事。\[1\]

## 參考資料

## 外部連結

  - [香港足總網頁球員註冊資料](http://www.hkfa.com/zh-hk/player_view.php?player_id=95)

[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:愉園球員](../Category/愉園球員.md "wikilink")
[Category:晨曦球員](../Category/晨曦球員.md "wikilink")
[Category:沙田球員](../Category/沙田球員.md "wikilink")
[Category:淦源球員](../Category/淦源球員.md "wikilink")
[Category:福建球員](../Category/福建球員.md "wikilink")
[Category:屯門球員](../Category/屯門球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")
[Category:香港外籍足球運動員](../Category/香港外籍足球運動員.md "wikilink")
[Category:印度外籍足球運動員](../Category/印度外籍足球運動員.md "wikilink")
[Category:孟加拉外籍足球運動員](../Category/孟加拉外籍足球運動員.md "wikilink")
[Category:香港入籍足球運動員](../Category/香港入籍足球運動員.md "wikilink")
[Category:尼日利亞足球運動員](../Category/尼日利亞足球運動員.md "wikilink")
[Category:尼日利亞旅外足球運動員](../Category/尼日利亞旅外足球運動員.md "wikilink")
[Category:歸化中華人民共和國之香港居民](../Category/歸化中華人民共和國之香港居民.md "wikilink")

1.  [亞洲盃外圍賽-香港 2:0 孟加拉](http://www.hkfa.com/news_details/4355)
    HKFA-News 2006-11-15