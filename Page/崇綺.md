**崇綺**（），[字](../Page/表字.md "wikilink")**古琴**，号**文山**，\[1\]阿魯特氏，出身[蒙古正藍旗](../Page/蒙古正藍旗.md "wikilink")，後抬入[滿洲鑲黃旗](../Page/滿洲鑲黃旗.md "wikilink")。[晚清](../Page/晚清.md "wikilink")[狀元](../Page/狀元.md "wikilink")、政治人物。

## 生平

[咸丰四年](../Page/咸丰_\(年號\).md "wikilink")（1854年），充督练旗兵处文案。后叙[兵部七品](../Page/兵部.md "wikilink")[笔帖式](../Page/笔帖式.md "wikilink")。[英国兵舰入](../Page/英国.md "wikilink")[天津时](../Page/天津.md "wikilink")，因守城之功擢[主事](../Page/主事.md "wikilink")。迁[员外郎](../Page/员外郎.md "wikilink")。[同治三年](../Page/同治.md "wikilink")（1865年）一甲第一名進士（[狀元](../Page/狀元.md "wikilink")），是[清朝唯一的](../Page/清朝.md "wikilink")[蒙古族狀元](../Page/蒙古族.md "wikilink")。也是唯一一位非漢籍的漢榜狀元。\[2\][同治九年](../Page/同治.md "wikilink")，迁[侍讲](../Page/侍讲.md "wikilink")。[同治十一年诏册其女为皇后](../Page/同治.md "wikilink")，赐三等承恩公。累官至[内阁学士](../Page/内阁学士.md "wikilink")，[吏部](../Page/吏部.md "wikilink")、[戶部侍郎](../Page/戶部侍郎.md "wikilink")。

[光绪二年](../Page/光绪.md "wikilink")，充[会试副考官](../Page/会试.md "wikilink")，补[镶黄旗汉军](../Page/镶黄旗汉军.md "wikilink")[副都统](../Page/副都统.md "wikilink")。五年，出为[热河](../Page/热河.md "wikilink")[都统](../Page/都统.md "wikilink")。七年，调[盛京将军](../Page/盛京将军.md "wikilink")。九年，称病隐退，授予[户部尚书一职](../Page/户部尚书.md "wikilink")。二十六年[翰林院](../Page/翰林院.md "wikilink")[掌院学士](../Page/掌院学士.md "wikilink")。

崇綺思想守舊，反對[康有為等提倡變法](../Page/康有為.md "wikilink")；[戊戌政變後](../Page/戊戌政變.md "wikilink")，主張廢[光緒帝](../Page/光緒帝.md "wikilink")。[庚子拳變時](../Page/義和團運動.md "wikilink")，支持[義和團](../Page/義和團.md "wikilink")“[扶清滅洋](../Page/扶清滅洋.md "wikilink")”。[八國聯軍進京](../Page/八國聯軍.md "wikilink")，崇綺留守[北京](../Page/北京.md "wikilink")，不久逃至[保定](../Page/保定.md "wikilink")。崇綺留在北京的全家自盡，消息傳到[保定](../Page/保定.md "wikilink")，他留下「聖駕西幸，未敢即死，恢復無力，以身殉之」的遺書後，于[莲池书院](../Page/莲池书院.md "wikilink")[自盡身亡](../Page/自盡.md "wikilink")。[谥](../Page/谥.md "wikilink")**文节**。入祀[昭忠祠](../Page/昭忠祠.md "wikilink")。\[3\]

## 家庭

  - 父：[賽尚阿](../Page/賽尚阿.md "wikilink")，[大學士](../Page/大學士.md "wikilink")
  - 发妻：愛新覺羅氏，[鄭親王](../Page/鄭親王.md "wikilink")[端華與鈕鈷祿氏之女](../Page/端華.md "wikilink")，鈕鈷祿氏即[慈安父](../Page/慈安.md "wikilink")[穆揚阿姐妹](../Page/穆揚阿.md "wikilink")、策普坦之女。
  - 继室：瓜爾佳氏，[榮祿妹妹](../Page/榮祿.md "wikilink")，愛新覺羅昆岡是其姐妹之丈夫。
  - 女儿：[孝哲毅皇后](../Page/孝哲毅皇后.md "wikilink")
  - 妹：[莊和皇貴妃](../Page/莊和皇貴妃.md "wikilink")
  - 子：[葆初](../Page/葆初.md "wikilink")

## 參考文獻

## 外部連結

  - [崇綺](http://archive.ihp.sinica.edu.tw/ttscgi/ttsquery?0:0:mctauac:TM%3D%B1R%BA%F6)
    中研院史語所

{{-}}

[Category:滿洲鑲黃旗人](../Category/滿洲鑲黃旗人.md "wikilink")
[Category:清朝狀元](../Category/清朝狀元.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝三等公](../Category/清朝三等公.md "wikilink")
[Category:阿魯特氏](../Category/阿魯特氏.md "wikilink")
[Category:清朝戶部尚書](../Category/清朝戶部尚書.md "wikilink")
[Category:清朝吏部尚書](../Category/清朝吏部尚書.md "wikilink")
[Category:留京辦事大臣](../Category/留京辦事大臣.md "wikilink")
[Category:諡文節](../Category/諡文節.md "wikilink")
[Category:清朝自殺人物](../Category/清朝自殺人物.md "wikilink")

1.
2.  清史稿校註編纂小組編纂，《清史稿校註》〈崇綺傳〉：「立國二百數十年，滿、蒙人試漢文獲授修撰者，止崇綺一人，士論榮之」，
3.