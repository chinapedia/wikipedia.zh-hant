**长坂坡之战**发生于中国[东汉末年](../Page/东汉末年.md "wikilink")，[曹操軍在](../Page/曹操.md "wikilink")[荆州](../Page/荆州.md "wikilink")[当阳附近的长坂坡追擊](../Page/当阳.md "wikilink")[劉備軍](../Page/劉備.md "wikilink")。[赤壁之戰之前哨戰](../Page/赤壁之戰.md "wikilink")。

## 背景

[漢獻帝](../Page/漢獻帝.md "wikilink")[建安七年](../Page/建安_\(東漢\).md "wikilink")（202年），[夏侯惇率領曹軍與](../Page/夏侯惇.md "wikilink")[劉備軍](../Page/劉備.md "wikilink")[戰於博望坡](../Page/博望坡之戰.md "wikilink")，之後兩軍撤退\[1\]。在[白狼山之戰平定北方之後](../Page/白狼山之戰.md "wikilink")，208年，[曹操親自率軍南下](../Page/曹操.md "wikilink")[荊州](../Page/荊州_\(古代\).md "wikilink")。當年八月，荊州牧[劉表病逝](../Page/劉表.md "wikilink")，次子[劉琮即位後即不戰而降](../Page/劉琮.md "wikilink")，駐紮在[樊城的劉表客卿劉備聞訊向](../Page/樊城.md "wikilink")[江陵](../Page/江陵.md "wikilink")（故楚[郢都](../Page/郢都.md "wikilink")）撤退，同時通知[關羽率水軍到江陵會合](../Page/關羽.md "wikilink")，民眾十餘萬隨行。江陵有劉表糧儲、器械，九月，曹操恐劉備據江陵軍實，與[曹純領五千](../Page/曹純.md "wikilink")[虎豹精騎急襲追擊](../Page/虎豹精騎.md "wikilink")，在當陽附近追上劉備軍。

## 戰事

劉備軍被甲士兵少，一觸即潰，劉備被逼棄百姓逃走，令[張飛率二十騎拒後](../Page/張飛.md "wikilink")；張飛以大喝嚇阻曹軍，以助[趙雲護送劉備妻甘夫人和](../Page/趙雲.md "wikilink")[劉禪](../Page/劉禪.md "wikilink")。此外兩名劉備之女被擄。

劉備放棄逃往江陵之計劃，轉為提早會合關羽水軍，一同逃往[江夏和劉表長子](../Page/江夏.md "wikilink")[劉琦會師](../Page/劉琦.md "wikilink")。

## 影響

曹仁（曹操堂弟）和曹純攻入江陵，江陵守軍不戰而降。趙雲因護送劉備家眷有功升任[牙門將軍](../Page/牙門將軍.md "wikilink")。[徐庶之母在此戰之中被曹操俘獲](../Page/徐庶.md "wikilink")，以致後來徐庶轉投曹操。

東吳弔唁劉表的使者[魯肅勸說劉備聯合](../Page/魯肅.md "wikilink")[孫權共抗曹操](../Page/孫權.md "wikilink")，[諸葛亮被派往](../Page/諸葛亮.md "wikilink")[東吳商談聯盟事宜](../Page/東吳.md "wikilink")。

## 參戰人物

<table>
<thead>
<tr class="header">
<th><p>曹操軍</p></th>
<th><p>劉備軍</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/曹操.md" title="wikilink">曹操</a><br />
<a href="../Page/曹純.md" title="wikilink">曹純</a><br />
<a href="../Page/賈詡.md" title="wikilink">賈詡</a><br />
<a href="../Page/夏侯惇.md" title="wikilink">夏侯惇</a><br />
<a href="../Page/夏侯霸.md" title="wikilink">夏侯霸</a><br />
<a href="../Page/曹休.md" title="wikilink">曹休</a><br />
<a href="../Page/朱靈.md" title="wikilink">朱靈</a></p></td>
<td><p><a href="../Page/劉備.md" title="wikilink">劉備</a><br />
<a href="../Page/趙雲.md" title="wikilink">趙雲</a><br />
<a href="../Page/張飛.md" title="wikilink">張飛</a><br />
<a href="../Page/諸葛亮.md" title="wikilink">諸葛亮</a></p></td>
</tr>
<tr class="even">
<td><p>救援軍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉琦.md" title="wikilink">劉琦</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 民間藝術

### 三國演義

[三国演义中记述](../Page/三国演义.md "wikilink")，麋夫人和刘禅被曹军围困，麋夫人投井自尽，赵云救出刘禅，途中斩[夏侯恩获](../Page/夏侯恩.md "wikilink")[青釭劍](../Page/青釭劍.md "wikilink")。张飞在[长坂桥断后吓阻曹军](../Page/长坂桥.md "wikilink")，[夏侯杰被张飞大喝惊吓](../Page/夏侯杰.md "wikilink")，坠马而亡。曹操记起关羽昔日评论，下令慎进，但是张飞断桥之后被曹操看破，曹操追刘军至[汉津](../Page/汉津.md "wikilink")，見刘備會合关羽，恐防中伏而撤退。

## 参考

  - 三国志·曹仁传附曹纯传
  - 三国志·先主传
  - 三国志·后主传
  - 三国志·諸葛亮傳
  - 三国志·关张马黄赵传

[掇刀概况·历史沿革](https://web.archive.org/web/20111022145205/http://www.duodao.gov.cn/category.do?categoryid=50)

  - [資治通鑑](../Page/資治通鑑.md "wikilink")
  - [后汉书](../Page/后汉书.md "wikilink")·袁绍刘表列传

[C](../Category/东汉战役.md "wikilink") [C](../Category/曹魏战役.md "wikilink")
[C](../Category/蜀汉战役.md "wikilink")
[C](../Category/湖北历次战争与战役.md "wikilink")
[C](../Category/荆门历史.md "wikilink") [C](../Category/掇刀区.md "wikilink")
[C](../Category/208年.md "wikilink")

1.  [s:三國志無載年份](../Page/s:三國志.md "wikilink")。[s:資治通鑒_(胡三省音注)/卷064](../Page/s:資治通鑒_\(胡三省音注\)/卷064.md "wikilink")：建安七年……劉表使劉備北侵，至葉，曹操遣夏侯惇、于禁等拒之。備一旦燒屯去，惇等追之。裨將軍鉅鹿李典曰：「賊無故退，疑必有伏。南道窄狹，草木深，不可追也。」惇等不聽，使典留守而追之，果入伏裏，兵大敗。典往救之，備乃退。