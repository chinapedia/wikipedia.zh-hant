**張緝**（），字**敬仲**，[馮翊](../Page/馮翊.md "wikilink")[高陵人](../Page/高陵.md "wikilink")。三國時[曹魏官員](../Page/曹魏.md "wikilink")，[涼州](../Page/涼州.md "wikilink")[刺史](../Page/刺史.md "wikilink")[張既之子](../Page/張既.md "wikilink")，曹芳[張皇后之父](../Page/张皇后_\(曹芳\).md "wikilink")，即[曹芳之皇丈](../Page/曹芳.md "wikilink")。

## 生平

張緝繼承了父親張既的爵位，**西鄉侯**。在[太和年間](../Page/太和_\(曹叡\).md "wikilink")，張緝曾任[溫縣](../Page/温县.md "wikilink")[縣令](../Page/縣令.md "wikilink")，因其管治能力而有名氣。當時正值[蜀漢](../Page/蜀漢.md "wikilink")[丞相](../Page/丞相.md "wikilink")[諸葛亮領兵](../Page/諸葛亮.md "wikilink")[北伐](../Page/诸葛亮北伐.md "wikilink")，張緝上書獻計，魏明帝[曹叡於是詢問](../Page/曹叡.md "wikilink")[中書令](../Page/中書令.md "wikilink")[孫資的意見](../Page/孫資.md "wikilink")，孫資則認為張緝有謀略。明帝於是命張緝為[騎都尉](../Page/騎都尉.md "wikilink")，參與對蜀作戰。戰後改任[尚書郎](../Page/尚書郎.md "wikilink")，因稱職而獲明帝所認識。後張緝以[中書郎遷任](../Page/中書郎.md "wikilink")[東莞郡](../Page/東莞郡.md "wikilink")（今[山東省](../Page/山東省.md "wikilink")[沂水縣東北](../Page/沂水县.md "wikilink")）[太守](../Page/太守.md "wikilink")，期間多次向朝廷分析與[東吳和蜀漢的形勢](../Page/孙吴.md "wikilink")。[嘉平四年](../Page/嘉平_\(曹魏\).md "wikilink")（252年），曹芳立張緝之女張氏為[皇后](../Page/皇后.md "wikilink")，張緝任[光祿大夫](../Page/光祿大夫.md "wikilink")，位[特進](../Page/特進.md "wikilink")。不久曹魏進攻東吳，被東吳[太傅](../Page/太傅.md "wikilink")[諸葛恪於](../Page/諸葛恪.md "wikilink")[東興擊敗](../Page/東興.md "wikilink")；但此時張緝卻向[大將軍](../Page/大将军_\(东亚\).md "wikilink")[司馬師認為諸葛恪功高震主](../Page/司馬師.md "wikilink")，不久必會被誅殺。次年諸葛恪圍攻[合肥](../Page/合肥.md "wikilink")，遇疫病被逼撤軍後果然被[孫峻殺死](../Page/孫峻.md "wikilink")。司馬師於是讚嘆張緝比諸葛恪更聰明\[1\]。

張緝與中書令[李豐是世交和同鄉](../Page/李丰_\(曹魏\).md "wikilink")，彼此住處亦相近，李豐於是聯合張緝和[夏侯玄](../Page/夏侯玄.md "wikilink")，打算推翻[司馬師](../Page/司馬師.md "wikilink")，改以夏侯玄為大將軍。張緝因在朝中不得意，而李豐掌握權力，彼此亦是同鄉，他的兒子[李韜又娶了](../Page/李韜.md "wikilink")[齊長公主](../Page/齊長公主.md "wikilink")，於是聽信。嘉平六年（254年），李豐設下伏兵打算誅殺司馬師，以夏侯玄為大將軍，張緝為[驃騎將軍](../Page/骠骑将军.md "wikilink")，但司馬師聽到風聲，召見李豐並將[李豐殺害](../Page/李丰_\(曹魏\).md "wikilink")，及後又收捕夏侯玄和張緝等人，收歸[廷尉審理](../Page/廷尉.md "wikilink")。最終張緝在獄中被賜死，诸子也被杀，但未被灭族，其孙[张殷在西晋年间为梁州刺史](../Page/张殷.md "wikilink")。

## 家庭

### 父親

  - [張既](../Page/張既.md "wikilink")，曹魏涼州刺史。

### 妻子

  - [向氏](../Page/向氏.md "wikilink")，女兒被立為皇后後獲封為安城鄉君。

### 子女

  - [張皇后](../Page/张皇后_\(曹芳\).md "wikilink")，齊王曹芳的皇后，張緝死後不久被廢。
  - [張邈](../Page/張邈_\(曹魏\).md "wikilink")，張緝之子，曾被張緝派遺與李豐通訊，事敗時[張邈被誅殺](../Page/張邈_\(曹魏\).md "wikilink")。

### 孫兒

  - [张殷](../Page/张殷.md "wikilink")，西晉[梁州刺史](../Page/梁州刺史.md "wikilink")。

## 參考書目

  - 《三國志·魏書·張既傳》裴注《魏略》
  - 《三國志·魏書·夏侯玄傳》
  - 《三國志·魏書·三少帝紀》

<references/>

[Z](../Category/曹魏政治人物.md "wikilink")
[Z](../Category/三國被賜死人物.md "wikilink")
[Z](../Category/高陵人.md "wikilink")
[J](../Category/張姓.md "wikilink")

1.  大將軍聞恪死，謂眾人曰：「諸葛恪多輩耳！近張敬仲懸論恪，以為必見殺，今果然如此。敬仲之智為勝恪也。」