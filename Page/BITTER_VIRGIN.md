《**BITTER
VIRGIN**》是[楠桂的](../Page/楠桂.md "wikilink")[漫畫作品](../Page/漫畫.md "wikilink")。自[YOUNG
GANGAN](../Page/YOUNG_GANGAN.md "wikilink")（[Square
Enix](../Page/Square_Enix.md "wikilink")）2005年5號至2008年6號連載。[單行本全](../Page/單行本.md "wikilink")4卷。

## 概要

主角·諏訪大介與同班三位女同學為中心的愛情故事。

## 登場角色

  -
    16歲。家裡經營食堂。父親已逝世。想要離開鄉下到都市去一人生活。自從知道雛子的秘密後，對雛子對帶有特別的感情。

<!-- end list -->

  -
    大介的同學。中學時受到繼父的[性侵犯而](../Page/性侵犯.md "wikilink")[懷孕](../Page/懷孕.md "wikilink")，[流產後再次懷孕](../Page/流產.md "wikilink")，鑒於對身體的負擔考量而把孩子生下來，不過馬上就讓別人寄養。有嚴重的男性恐怖症（大介除外）。現在一個人生活。

<!-- end list -->

  -
    大介的同學，外表豔麗的女生。第4話中被大介擁抱後，以此為契機自認為是大介的女朋友。

<!-- end list -->

  -
    大介的同學及[青梅竹馬](../Page/青梅竹馬.md "wikilink")。

<!-- end list -->

  -
    諏訪大介的姐姐。外遇未婚生子，結果孩子出生後便是死胎。一個人住在東京。

## 單行本

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p><a href="../Page/史克威尔艾尼克斯.md" title="wikilink">SQEX</a></p></th>
<th><p><a href="../Page/青文出版社.md" title="wikilink">青文出版社</a></p></th>
<th><p><a href="../Page/玉皇朝.md" title="wikilink">玉皇朝</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2006年4月27日</p></td>
<td><p>ISBN 978-4-7575-1674-8</p></td>
<td><p>2008年8月12日</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2007年1月25日</p></td>
<td><p>ISBN 978-4-7575-1916-9</p></td>
<td><p>2008年9月24日</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2007年8月25日</p></td>
<td><p>ISBN 978-4-7575-2086-8</p></td>
<td><p>2008年10月24日</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2008年6月25日</p></td>
<td><p>ISBN 978-4-7575-2315-9</p></td>
<td><p>2008年11月20日</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [YOUNG GANGAN - BITTER
    VIRGIN](http://www.square-enix.co.jp/magazine/yg/introduction/bitter/)

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink") [Category:YOUNG
GANGAN](../Category/YOUNG_GANGAN.md "wikilink")
[Category:戀愛漫畫](../Category/戀愛漫畫.md "wikilink")
[Category:兒童性虐待作品](../Category/兒童性虐待作品.md "wikilink")
[Category:虚构强奸题材作品](../Category/虚构强奸题材作品.md "wikilink")