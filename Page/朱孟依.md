**朱孟依**（），[廣東](../Page/廣東.md "wikilink")[豐順縣](../Page/豐順縣.md "wikilink")[留隍镇口铺村人](../Page/留隍镇.md "wikilink")，[合生创展集团](../Page/合生创展集团.md "wikilink")、[珠江投资集团创始人](../Page/珠江投资集团.md "wikilink")。從事建築、投資、不動產產業而致富。珠江投资管理集团是朱孟依麾下的多元化產業投資集团，旗下投資包括交通基建、能源、教育、科技、地產等\[1\]。外界指[珠江投资集团是朱孟依的一個隱形財富帝國](../Page/珠江投资集团.md "wikilink")
\[2\]\[3\]

2008年，当选第十一届全国政协委员\[4\]，代表特别邀请人士，分入第五十八组。并担任经济委员会专委。\[5\]

2009年2月，朱孟依因涉嫌[黄光裕案被中国公安部门限制出境](../Page/黄光裕案.md "wikilink")。\[6\][黄光裕](../Page/黄光裕.md "wikilink")、[郑少东和朱孟依兄弟都是](../Page/郑少东.md "wikilink")[广东潮汕同乡](../Page/广东.md "wikilink")。

## 参考文献

## 外部連結

  - [朱孟依王国为何盛极而衰 中国地产界“隐形航母”的隐形跌落](http://www.infzm.com/content/93198) -
    南方周末

[Category:中国企业家](../Category/中国企业家.md "wikilink")
[Category:中国房地产商](../Category/中国房地产商.md "wikilink")
[Category:中國慈善家](../Category/中國慈善家.md "wikilink")
[Category:中華人民共和國億萬富豪](../Category/中華人民共和國億萬富豪.md "wikilink")
[Category:潮商](../Category/潮商.md "wikilink")
[Category:豐順人](../Category/豐順人.md "wikilink")
[M](../Category/朱姓.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:第十二屆全國政協委員](../Category/第十二屆全國政協委員.md "wikilink")
[Category:潮汕人](../Category/潮汕人.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")

1.  [朱孟依的家族商業帝國](http://big5.huaxia.com/tslj/rdrw/2012/04/2816593.html)
2.  [广东珠江投资管理集团](http://www.gdprm.com/)
3.  騰訊財經，朱孟依简介，http://finance.qq.com/a/20051012/000479.htm
4.
5.
6.  [腾讯财经
    传合生创展主席朱孟依因涉黄光裕案遭调查](http://finance.qq.com/a/20090220/003146.htm)