{{ 日本區

`| 自治體名=幸區`

|圖像=[TOSHIBA_research_and_development_center_Komukaitoshiba.jpg](https://zh.wikipedia.org/wiki/File:TOSHIBA_research_and_development_center_Komukaitoshiba.jpg "fig:TOSHIBA_research_and_development_center_Komukaitoshiba.jpg")
|圖像說明=[東芝](../Page/東芝.md "wikilink")[株式會社](../Page/株式會社.md "wikilink")[研究開發中心暨小向工廠的東南門](../Page/研究開發.md "wikilink")（2007年）

`| 區章=`
`| 日文原名=幸区`
`| 平假名=さいわいく`
`| 羅馬字拼音=Saiwai-ku`
`| 都道府縣=神奈川縣`
`| 支廳=`
`| 市=川崎市`
`| 編號=14132-1`
`| 面積=10.09`
`| 邊界未定=`
`| 人口=149,244`
`| 統計時間=2008年5月1日`
`| 自治體=`[`川崎市`](../Page/川崎市.md "wikilink")`（`[`川崎區`](../Page/川崎區.md "wikilink")`、`[`中原區`](../Page/中原區_\(日本\).md "wikilink")`）、`[`橫濱市`](../Page/橫濱市.md "wikilink")`（`[`鶴見區`](../Page/鶴見區_\(橫濱市\).md "wikilink")`、`[`港北區`](../Page/港北區_\(日本\).md "wikilink")`）`
[`東京都`](../Page/東京都.md "wikilink")`：`[`大田區`](../Page/大田區.md "wikilink")
`| 樹=`
`| 花=`
`| 其他象徵物=`
`| 其他象徵=`
`| 郵遞區號=212-8570`
`| 所在地=幸區戶手本町一丁目11番地1`
`| 電話號碼=44-556-6666`
`| 外部連結=`<http://www.city.kawasaki.jp/63/63saiwai/home/index.html>
`| 經度=`
`| 緯度=`
`| 地圖=`

}}

**幸區**（）是[川崎市的](../Page/川崎市.md "wikilink")7区之一。

## 交通

### 鐵路

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")

<!-- end list -->

  - [JR_JO_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JO_line_symbol.svg "fig:JR_JO_line_symbol.svg")
    [橫須賀線](../Page/橫須賀線.md "wikilink")（品鶴線）：[新川崎站](../Page/新川崎站.md "wikilink")
  - [JR_JN_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JN_line_symbol.svg "fig:JR_JN_line_symbol.svg")
    [南武線](../Page/南武線.md "wikilink")：[鹿島田站](../Page/鹿島田站.md "wikilink")
    -（[矢向站](../Page/矢向站.md "wikilink")） -
    [尻手站](../Page/尻手站.md "wikilink")
      - 矢向站位於[橫濱市](../Page/橫濱市.md "wikilink")[鶴見區](../Page/鶴見區_\(橫濱市\).md "wikilink")。
      - 尻手站之名來自橫濱市鶴見區的地名尻手，但車站位於川崎市幸區。
      - [川崎站位於](../Page/川崎站.md "wikilink")[川崎區](../Page/川崎區.md "wikilink")，部分位於幸區。
  - [武藏野線](../Page/武藏野線.md "wikilink")：構內

## 外部連結