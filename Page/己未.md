**己未**为[干支之一](../Page/干支.md "wikilink")，顺序为第56个。前一位是[戊午](../Page/戊午.md "wikilink")，后一位是[庚申](../Page/庚申.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之己屬陰之土](../Page/天干.md "wikilink")，[地支之未屬陰之土](../Page/地支.md "wikilink")，是比例和好。

## 己未年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")56年称“**己未年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘59，或年份數減3，除以10的餘數是6，除以12的餘數是8，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“己未年”：

<table>
<caption><strong>己未年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li>59年</li>
<li>119年</li>
<li>179年</li>
<li>239年</li>
<li>299年</li>
<li>359年</li>
<li>419年</li>
<li>479年</li>
<li>539年</li>
<li>599年</li>
<li>659年</li>
<li>719年</li>
<li>779年</li>
<li>839年</li>
<li>899年</li>
<li>959年</li>
</ul></td>
<td><ul>
<li>1019年</li>
<li>1079年</li>
<li>1139年</li>
<li>1199年</li>
<li>1259年</li>
<li>1319年</li>
<li>1379年</li>
<li>1439年</li>
<li>1499年</li>
<li>1559年</li>
<li>1619年</li>
<li>1679年</li>
<li>1739年</li>
<li>1799年</li>
<li>1859年</li>
<li>1919年</li>
<li>1979年</li>
</ul></td>
<td><ul>
<li>2039年</li>
<li>2099年</li>
<li>2159年</li>
<li>2219年</li>
<li>2279年</li>
<li>2339年</li>
<li>2399年</li>
<li>2459年</li>
<li>2519年</li>
<li>2579年</li>
<li>2639年</li>
<li>2699年</li>
<li>2759年</li>
<li>2819年</li>
<li>2879年</li>
<li>2939年</li>
<li>2999年</li>
</ul></td>
</tr>
</tbody>
</table>

## 己未月

天干戊年和癸年，[小暑到](../Page/小暑.md "wikilink")[立秋的時間段](../Page/立秋.md "wikilink")，就是**己未月**：

  - ……
  - 1978年7月小暑到8月立秋
  - 1983年7月小暑到8月立秋
  - 1988年7月小暑到8月立秋
  - 1993年7月小暑到8月立秋
  - 1998年7月小暑到8月立秋
  - 2003年7月小暑到8月立秋
  - 2008年7月小暑到8月立秋
  - ……

## 己未日

## 己未時

天干戊日和癸日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）13時到15時，就是**己未時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/己未年.md "wikilink")