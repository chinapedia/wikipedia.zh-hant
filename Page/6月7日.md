**6月7日**是[公历一年中的第](../Page/公历.md "wikilink")158天（[闰年第](../Page/闰年.md "wikilink")159天），離全年結束還有207天。

## 大事記

### 11世紀

  - [1099年](../Page/1099年.md "wikilink")：第一次[十字军东征的隊伍開始圍攻](../Page/十字军东征.md "wikilink")[耶路撒冷](../Page/耶路撒冷.md "wikilink")。

### 15世紀

  - [1494年](../Page/1494年.md "wikilink")：[西班牙和](../Page/西班牙.md "wikilink")[葡萄牙達成瓜分](../Page/葡萄牙.md "wikilink")[歐洲以外新發現陸地的](../Page/欧洲.md "wikilink")《[托尔德西里亚斯条约](../Page/托尔德西里亚斯条约.md "wikilink")》。

### 19世紀

  - [1895年](../Page/1895年.md "wikilink")：[日本軍隊根據](../Page/日本軍隊.md "wikilink")《[馬關條約](../Page/馬關條約.md "wikilink")》的規定完全佔領[台灣](../Page/台灣.md "wikilink")。

### 20世紀

  - [1905年](../Page/1905年.md "wikilink")：[挪威宣布脱離](../Page/挪威.md "wikilink")[瑞典和挪威聯盟](../Page/瑞典-挪威联盟.md "wikilink")，成為[主權](../Page/主權.md "wikilink")[獨立的國家](../Page/獨立.md "wikilink")，[瑞典-挪威聯合解體](../Page/瑞典-挪威聯合解體.md "wikilink")。
  - [1906年](../Page/1906年.md "wikilink")：[英国](../Page/英国.md "wikilink")[盧西塔尼亞號](../Page/卢西塔尼亚号.md "wikilink")[郵輪在](../Page/邮轮.md "wikilink")[蘇格蘭](../Page/苏格兰.md "wikilink")[格拉斯哥下水](../Page/格拉斯哥.md "wikilink")。
  - [1915年](../Page/1915年.md "wikilink")：[中俄蒙协约簽訂](../Page/中俄蒙协约.md "wikilink")，[俄国同意](../Page/俄国.md "wikilink")[外蒙古取消獨立](../Page/外蒙古.md "wikilink")，[中国許諾給予外蒙古高度自治](../Page/中国.md "wikilink")。
  - [1929年](../Page/1929年.md "wikilink")：[意大利承認](../Page/意大利.md "wikilink")[梵蒂岡獨立](../Page/梵蒂岡.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[湘西會戰結束](../Page/湘西会战.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[國民革命軍勝利](../Page/國民革命軍.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[以色列空軍發動](../Page/以色列空军.md "wikilink")[巴比倫行動](../Page/巴比倫行動.md "wikilink")，出動14架[戰鬥機摧毁](../Page/战斗机.md "wikilink")[伊拉克的](../Page/伊拉克.md "wikilink")[核反應堆](../Page/核反应堆.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：[香港總督](../Page/香港總督.md "wikilink")[尤德為](../Page/尤德.md "wikilink")[東區走廊第一段主持開幕儀式](../Page/東區走廊.md "wikilink")。到第二日下午兩點半，該段東區走廊正式開放通車。第一段由[銅鑼灣至](../Page/銅鑼灣.md "wikilink")[太古城](../Page/太古城.md "wikilink")，全長3.7公里。東區走廊通車後，[港島東區的交通大為改善](../Page/東區_\(香港\).md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：[旺角](../Page/旺角.md "wikilink")[彌敦道一帶](../Page/彌敦道.md "wikilink")[香港市民悼念](../Page/香港.md "wikilink")[六四事件的活動演變成騷亂](../Page/六四事件.md "wikilink")，警方調派七百人到場，最後要發射催淚彈，才將滋事者驅散。
  - [1989年](../Page/1989年.md "wikilink")：[蘇里南航空764號班機在](../Page/蘇里南航空764號班機空難.md "wikilink")[帕拉马里博墜毀](../Page/帕拉马里博.md "wikilink")，包括15名[蘇里南](../Page/蘇里南.md "wikilink")[足球運動員在內的](../Page/足球.md "wikilink")176人遇難。
  - [1999年](../Page/1999年.md "wikilink")：[最高人民法院舉行新聞發佈會](../Page/最高人民法院.md "wikilink")，公布[廣東](../Page/廣東.md "wikilink")[湛江特大](../Page/湛江.md "wikilink")[走私受賄案的香港主犯李深](../Page/走私.md "wikilink")，以及香港和內地多名[走私份子](../Page/走私.md "wikilink")，在廣東被執行[死刑](../Page/死刑.md "wikilink")。案件涉及1996年初至1998年中期間，香港走私份子賄賂收買湛江海關邊防等部門官員，進行走私偷逃關稅等活動，涉及金額十幾億[人民幣](../Page/人民幣.md "wikilink")。令國家蒙受重大經濟損失。

### 21世紀

  - [2007年](../Page/2007年.md "wikilink")：[中華民國宣佈與](../Page/中華民國.md "wikilink")[哥斯達黎加斷交](../Page/哥斯達黎加.md "wikilink")，因哥國與[中華人民共和國建交](../Page/中华人民共和国.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：[張韶涵受邀參與](../Page/張韶涵.md "wikilink")[北京奥运会火炬傳遞](../Page/北京奥运会.md "wikilink")，成為第一位[台灣籍藝人火炬手](../Page/臺灣.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：在2009年[法国网球公开赛男单决赛中](../Page/法国网球公开赛.md "wikilink")，瑞士天王[费德勒以](../Page/费德勒.md "wikilink")3:0的比分横扫了赛会的最大黑马[索德林](../Page/索德林.md "wikilink")，职业生涯首度在法网捧起火枪手杯。至此，他不仅追平了[桑普拉斯的](../Page/桑普拉斯.md "wikilink")14个大满贯头衔记录，更成为历史上的第六位完成职业生涯[全满贯的选手](../Page/全满贯.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[中国](../Page/中国.md "wikilink")[福建省](../Page/福建省.md "wikilink")[厦门市发生](../Page/厦门市.md "wikilink")[快速公交纵火事件](../Page/2013年厦门公交车纵火案.md "wikilink")，造成47人死亡，34受伤。

## 出生

  - [1003年](../Page/1003年.md "wikilink")：[夏景宗李元昊](../Page/夏景宗.md "wikilink")，西夏开国皇帝（[1048年逝世](../Page/1048年.md "wikilink")）
  - [1594年](../Page/1594年.md "wikilink")：[塞萨尔·德·波旁
    (旺多姆公爵)](../Page/塞萨尔·德·波旁_\(旺多姆公爵\).md "wikilink")，法国国王亨利四世私生子
  - [1757年](../Page/1757年.md "wikilink")：[喬治亞娜·卡文迪什，德文郡公爵夫人](../Page/喬治亞娜·卡文迪什，德文郡公爵夫人.md "wikilink")，英國德文郡公爵夫人（[1806年逝世](../Page/1806年.md "wikilink")）
  - [1777年](../Page/1777年.md "wikilink")：[罗伯特·詹金逊，第二代利物浦伯爵](../Page/罗伯特·詹金逊，第二代利物浦伯爵.md "wikilink")，英國利物浦伯爵阁下，最年轻英国首相（[1828年逝世](../Page/1828年.md "wikilink")）
  - [1848年](../Page/1848年.md "wikilink")：[保羅·高更](../Page/保羅·高更.md "wikilink")，法國印象派畫家（[1903年逝世](../Page/1903年.md "wikilink")）
  - [1845年](../Page/1845年.md "wikilink")：[莱奥波德·奥尔](../Page/莱奥波德·奥尔.md "wikilink")，匈牙利小提琴演奏家、作曲家、音乐教师（[1930年逝世](../Page/1930年.md "wikilink")）
  - [1862年](../Page/1862年.md "wikilink")：[菲利普·莱纳德](../Page/菲利普·莱纳德.md "wikilink")，德国物理学家，1905年诺贝尔物理学奖获得者
  - [1868年](../Page/1868年.md "wikilink")：[查爾斯麥金塔](../Page/查爾斯麥金塔.md "wikilink")，蘇格蘭建築師（[1928年逝世](../Page/1928年.md "wikilink")）
  - [1883年](../Page/1883年.md "wikilink")：[西尔韦纳斯·莫利](../Page/西尔韦纳斯·莫利.md "wikilink")，美国考古学家、铭文学家和玛雅文化研究专家，对20世纪初期的前哥伦布时期玛雅文化研究做出了巨大贡献（[1948年逝世](../Page/1948年.md "wikilink")）
  - [1884年](../Page/1884年.md "wikilink")：[鲁尔夫·盖苓](../Page/鲁尔夫·盖苓.md "wikilink")，奥匈帝国建筑师（[1952年逝世](../Page/1952年.md "wikilink")）
  - [1886年](../Page/1886年.md "wikilink")：[亨利·科安德](../Page/亨利·科安德.md "wikilink")，罗马尼亚发明家，空气动力学先锋和现代喷气式飞机之父。（[1972年逝世](../Page/1972年.md "wikilink")）
  - [1893年](../Page/1893年.md "wikilink")：[吉利斯·格拉夫斯特伦](../Page/吉利斯·格拉夫斯特伦.md "wikilink")，世界知名花样滑冰运动员（[1938年逝世](../Page/1938年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[伊姆雷·纳吉](../Page/伊姆雷·纳吉.md "wikilink")，匈牙利政治家（[1958年逝世](../Page/1958年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[罗伯特·S·马利肯](../Page/罗伯特·S·马利肯.md "wikilink")，美国物理学家、化学家，1966年获诺贝尔化学奖
  - [1897年](../Page/1897年.md "wikilink")：[基里尔·阿法纳西耶维奇·梅列茨科夫](../Page/基里尔·阿法纳西耶维奇·梅列茨科夫.md "wikilink")：苏联军事领导人，苏联元帅
  - [1897年](../Page/1897年.md "wikilink")：[乔治·塞尔](../Page/乔治·塞尔.md "wikilink")，美国籍的匈牙利指挥家（[1970年逝世](../Page/1970年.md "wikilink")）
  - [1909年](../Page/1909年.md "wikilink")：[洁西卡·坦迪](../Page/洁西卡·坦迪.md "wikilink")，英国女演员，曾获得奥斯卡最佳女主角奖（[1994年逝世](../Page/1994年.md "wikilink")）
  - [1909年](../Page/1909年.md "wikilink")：[维珍尼亚·阿普伽](../Page/维珍尼亚·阿普伽.md "wikilink")：美国医生，专科麻醉及儿科。她是麻醉学及畸形学的领先学者，被认为是新生婴儿科的始创人
  - [1929年](../Page/1929年.md "wikilink")：[约翰·内皮尔·特纳](../Page/约翰·内皮尔·特纳.md "wikilink")：第17任加拿大总理
  - [1938年](../Page/1938年.md "wikilink")：[古龍](../Page/古龍.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[武俠小說作家](../Page/武俠小說.md "wikilink")（[1985年逝世](../Page/1985年.md "wikilink")）
  - [1940年](../Page/1940年.md "wikilink")：[汤姆·琼斯](../Page/湯姆·瓊斯_\(歌手\).md "wikilink")，英国威尔士歌手，被授予英女王OBE
  - [1942年](../Page/1942年.md "wikilink")：[穆阿迈尔·卡扎菲](../Page/穆阿迈尔·卡扎菲.md "wikilink")，利比亚前最高领导人（[2011年逝世](../Page/2011年.md "wikilink")）
  - [1943年](../Page/1943年.md "wikilink")：[陳鴻烈](../Page/陳鴻烈.md "wikilink")，香港演員（[2009年逝世](../Page/2009年.md "wikilink")）
  - [1945年](../Page/1945年.md "wikilink")：[沃尔夫冈·许塞尔](../Page/沃尔夫冈·许塞尔.md "wikilink")，奥地利人民党政治家，1995年任奥地利人民党主席。1989年起先后担任联邦经济部长、联邦副总理兼外交部长
  - [1947年](../Page/1947年.md "wikilink")：[劉兆佳](../Page/劉兆佳.md "wikilink")，香港中文大學教授、中央政策組首席顧問
  - [1948年](../Page/1948年.md "wikilink")：[吉姆·沃尔顿](../Page/吉姆·沃尔顿.md "wikilink")，超级巨富，阿维斯特银行CEO，沃尔玛总裁
  - [1952年](../Page/1952年.md "wikilink")：[奥尔汗·帕穆克](../Page/奥尔汗·帕穆克.md "wikilink")，哥伦比亚大学教授，土耳其作家，2006年诺贝尔文学奖得主
  - [1952年](../Page/1952年.md "wikilink")：[連恩·尼遜](../Page/連恩·尼遜.md "wikilink")，北愛爾蘭知名演員
  - [1953年](../Page/1953年.md "wikilink")：[马可·穆勒](../Page/马可·穆勒.md "wikilink")，意大利电影制片人、电影史家、影评人
  - [1957年](../Page/1957年.md "wikilink")：[侯友宜](../Page/侯友宜.md "wikilink")，台灣警官、前內政部警政署署長，現任新北市長
  - [1958年](../Page/1958年.md "wikilink")：[王子](../Page/王子_\(音樂家\).md "wikilink")，多次获奖，完美主义者，被普遍认为是一位音乐天才与艺术家
  - [1959年](../Page/1959年.md "wikilink")：[迈克·彭斯](../Page/迈克·彭斯.md "wikilink")，美國副總統
  - [1960年](../Page/1960年.md "wikilink")：[荒木飛呂彥](../Page/荒木飛呂彥.md "wikilink")，日本漫畫家
  - [1961年](../Page/1961年.md "wikilink")：[朱立倫](../Page/朱立倫.md "wikilink")，曾任桃園縣縣長、行政院副院長、中國國民黨主席，新北市市長
  - [1963年](../Page/1963年.md "wikilink")：[罗伯托·阿蓝尼亚](../Page/罗伯托·阿蓝尼亚.md "wikilink")：著名法籍歌剧男高音歌唱家
  - [1963年](../Page/1963年.md "wikilink")：[廈門潤](../Page/廈門潤.md "wikilink")，日本漫畫家
  - [1964年](../Page/1964年.md "wikilink")：[林强](../Page/林强.md "wikilink")，台湾歌手、演员、音乐创作人
  - [1965年](../Page/1965年.md "wikilink")：[米克·佛利](../Page/米克·佛利.md "wikilink")：美国职业摔角手及作家，为现任世界摔角娱乐旗下摔角节目WWE
    Saturday Morning Slam总经理，目前效力于世界摔角娱乐 （WWE）
  - [1965年](../Page/1965年.md "wikilink")：[吳國敬](../Page/吳國敬.md "wikilink")，香港歌手、作曲人
  - [1965年](../Page/1965年.md "wikilink")：[达米恩·赫斯特](../Page/达米恩·赫斯特.md "wikilink")，是新一代英国艺术家的主要代表人物之一
  - [1966年](../Page/1966年.md "wikilink")：[張雨生](../Page/張雨生.md "wikilink")，台灣音樂人、創作歌手（[1997年逝世](../Page/1997年.md "wikilink")）
  - [1966年](../Page/1966年.md "wikilink")：[喜田亞由美](../Page/喜田亞由美.md "wikilink")，日本女性聲優
  - [1966年](../Page/1966年.md "wikilink")：[高怡平](../Page/高怡平.md "wikilink")，台灣藝人
  - [1969年](../Page/1969年.md "wikilink")：[周傳雄](../Page/周傳雄.md "wikilink")，台灣音樂人、創作歌手
  - [1969年](../Page/1969年.md "wikilink")：[约阿基姆王子
    (丹麦)](../Page/约阿基姆王子_\(丹麦\).md "wikilink")，丹麦王子
  - [1970年](../Page/1970年.md "wikilink")：[车胜元](../Page/车胜元.md "wikilink")，韩国演员
  - [1970年](../Page/1970年.md "wikilink")：[卡福](../Page/卡福.md "wikilink")，巴西足球运动员
  - [1970年](../Page/1970年.md "wikilink")：[卡尔·厄本](../Page/卡尔·厄本.md "wikilink")，新西兰演员
  - [1973年](../Page/1973年.md "wikilink")：[宋允兒](../Page/宋允兒.md "wikilink")，韓國才女，女演員
  - [1974年](../Page/1974年.md "wikilink")：[贝尔·格里尔斯](../Page/贝尔·格里尔斯.md "wikilink")，英国探险家、作家、电视主持人、童军总领袖
  - [1975年](../Page/1975年.md "wikilink")：[艾倫·艾佛森](../Page/艾倫·艾佛森.md "wikilink")，NBA状元
  - [1975年](../Page/1975年.md "wikilink")：[陳仙梅](../Page/陳仙梅.md "wikilink")，台灣女演員
  - [1977年](../Page/1977年.md "wikilink")：[马尔钦·巴什琴斯基](../Page/马尔钦·巴什琴斯基.md "wikilink")，波兰足球员
  - [1977年](../Page/1977年.md "wikilink")：[安胜浩](../Page/安胜浩.md "wikilink")，韩国歌手兼TN
    Entertainment、Skoolooks、School store代表
  - [1977年](../Page/1977年.md "wikilink")：[任港秀](../Page/任港秀.md "wikilink")，前香港演員
  - [1977年](../Page/1977年.md "wikilink")：[容海恩](../Page/容海恩.md "wikilink")，香港法律界人士、立法會議員
  - [1978年](../Page/1978年.md "wikilink")：[林韋君](../Page/林韋君.md "wikilink")，台灣女演員
  - [1979年](../Page/1979年.md "wikilink")：[羅泳嫻](../Page/羅泳嫻.md "wikilink")，香港無綫電視演員
  - [1979年](../Page/1979年.md "wikilink")：[周浩鼎](../Page/周浩鼎.md "wikilink")，香港法律界人士、立法會議員
  - [1979年](../Page/1979年.md "wikilink")：[奇云·贺夫兰](../Page/奇云·贺夫兰.md "wikilink")，荷兰足球员
  - [1981年](../Page/1981年.md "wikilink")：[安娜·庫妮可娃](../Page/安娜·庫妮可娃.md "wikilink")，俄羅斯網球運動員
  - [1982年](../Page/1982年.md "wikilink")：[羅巧倫](../Page/羅巧倫.md "wikilink")，台灣女演員
  - [1982年](../Page/1982年.md "wikilink")：[淺見麗奈](../Page/淺見麗奈.md "wikilink")，日本女演员
  - [1982年](../Page/1982年.md "wikilink")：[赫尔曼·卢克斯](../Page/赫尔曼·卢克斯.md "wikilink")，足球运动员
  - [1985年](../Page/1985年.md "wikilink")：[丹妮·艾凡斯](../Page/丹妮·艾凡斯.md "wikilink")，美国模特
  - [1985年](../Page/1985年.md "wikilink")：[三宅麻理惠](../Page/三宅麻理惠.md "wikilink")，日本女性聲優
  - [1986年](../Page/1986年.md "wikilink")：[基冈·布拉德利](../Page/基冈·布拉德利.md "wikilink")，高尔夫球运动员
  - [1987年](../Page/1987年.md "wikilink")：[苏茜薇](../Page/苏茜薇.md "wikilink")，中国演员
  - [1990年](../Page/1990年.md "wikilink")：[庞盼盼](../Page/庞盼盼.md "wikilink")，前中国体操运动员
  - [1990年](../Page/1990年.md "wikilink")：[艾莉森·施米特](../Page/艾莉森·施米特.md "wikilink")，美国运动员
  - [1990年](../Page/1990年.md "wikilink")：[伊姬·阿潔莉亞](../Page/伊姬·阿潔莉亞.md "wikilink")，澳洲藝人和模特兒
  - [1991年](../Page/1991年.md "wikilink")：[艾蜜莉·瑞特考斯基](../Page/艾蜜莉·瑞特考斯基.md "wikilink")，美國模特兒及女演員
  - [1993年](../Page/1993年.md "wikilink")：[朴芝妍](../Page/朴芝妍.md "wikilink")，韓國女子偶像團體[T-ara前成員](../Page/T-ara.md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[Sayuri](../Page/Sayuri.md "wikilink")，日本創作歌手
  - [1996年](../Page/1996年.md "wikilink")：[李抒澔](../Page/李抒澔.md "wikilink")，韓國男子偶像團體[ONEUS成員](../Page/ONEUS.md "wikilink")

## 逝世

  - [1329年](../Page/1329年.md "wikilink")：[罗伯特·布鲁斯](../Page/罗伯特·布鲁斯.md "wikilink")，苏格兰国王，独立运动领袖（[1274年出生](../Page/1274年.md "wikilink")）
  - [1848年](../Page/1848年.md "wikilink")：[别林斯基](../Page/别林斯基.md "wikilink")，俄国文学评论家（[1811年出生](../Page/1811年.md "wikilink")）
  - [1935年](../Page/1935年.md "wikilink")：[米丘林](../Page/米丘林.md "wikilink")，俄国园艺学家（[1855年出生](../Page/1855年.md "wikilink")）
  - [1954年](../Page/1954年.md "wikilink")：[艾倫·圖靈](../Page/艾倫·圖靈.md "wikilink")
    ，[英國](../Page/英國.md "wikilink")[數學家](../Page/數學家.md "wikilink")（[1912年出生](../Page/1912年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[潘希真](../Page/潘希真.md "wikilink")，筆名琦君，台灣作家（[1917年出生](../Page/1917年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[畢漪汶](../Page/畢漪汶.md "wikilink")，澳門教育家，澳門東南學校創校校長（[1924年出生](../Page/1924年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[阿布·穆萨布·扎卡维](../Page/阿布·穆萨布·扎卡维.md "wikilink")，伊拉克基地组织首领（[1966年出生](../Page/1966年.md "wikilink")）
  - [2011年](../Page/2011年.md "wikilink")：[药家鑫](../Page/药家鑫.md "wikilink")，中国杀人犯（[1989年出生](../Page/1989年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[克里斯托弗·李](../Page/克里斯托弗·李.md "wikilink")，英国演员（[1922年出生](../Page/1922年.md "wikilink")）
  - [2018年](../Page/2018年.md "wikilink")：[傅達仁](../Page/傅達仁.md "wikilink")，台灣節目主持人（[1933年出生](../Page/1933年.md "wikilink")）

## 节日、风俗习惯

中华人民共和国[普通高等学校招生全国统一考试](../Page/普通高等学校招生全国统一考试.md "wikilink")（高考）考试开始第一天。

  - [2019年](../Page/2019年.md "wikilink")[端午節](../Page/端午節.md "wikilink")