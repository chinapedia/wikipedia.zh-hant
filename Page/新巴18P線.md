**新巴18P線**是新巴18線的全日特快班次，來往[堅尼地城](../Page/堅尼地城.md "wikilink")（[卑路乍灣](../Page/卑路乍灣.md "wikilink")）及[北角](../Page/北角.md "wikilink")（[健康中街](../Page/健康中街.md "wikilink")），途經[林士街天橋](../Page/香港4號幹線.md "wikilink")，來回程不經上環。此外，亦可選乘更快速的路線[18X](../Page/新巴18X線.md "wikilink")。

## 簡介及歷史

2003年3月10日，新巴於早上繁忙時間，在18號線增設特別車18P，由堅尼地城單-{向}-往北角健康中街，直接到達灣仔北主要商業區，不在中環區停站，深受乘客歡迎。所以在2004年5月31日的路線重組中，18P線提升至每天早上至黃昏雙向服務，除早上繁忙時間外總站遷往[北角碼頭](../Page/北角碼頭.md "wikilink")，提供一個來往北角及西區的更快選擇。由2010年8月16日起，為提升晚間由北角往堅尼地城的特快巴士服務，新巴修改了18及18P線的末班車開出時間\[1\]。18線的末趟車提早至晚上八時開出，而18P的末趟車則延至凌晨零時從北角健康中街開出往堅尼地城。這項安排，形成了晚上八時後，東行「有慢無快」、西行「有快無慢」的情況。

  - 2011年11月14日：18P線提早每日由堅尼地城（卑路乍灣）開出的頭班車至06:45並加密星期一至五07:46－08:26的班次\[2\]。
  - 2015年5月10日：因應[港鐵](../Page/港鐵.md "wikilink")[西港島綫通車後的巴士路線重組](../Page/港島綫西延.md "wikilink")，本線作出以下改動\[3\]：
      - 北角區總站全日延長至健康中街
      - 延長堅尼地城的尾班車時間至23:00
      - 往北角方向改經[正街前往干諾道西](../Page/正街.md "wikilink")，並改經[永興街及](../Page/永興街.md "wikilink")[英皇道](../Page/英皇道.md "wikilink")，不再途經[歌頓道及](../Page/歌頓道.md "wikilink")[電氣道](../Page/電氣道.md "wikilink")。
  - 2019年1月20日：本線調整收費，全程新收費為$5.5；為配合[林士街天橋東行落橋位於同日封閉](../Page/林士街天橋.md "wikilink")，往北角在林士街天橋後，改經民寶街、耀星街、龍和道、分域碼頭街及夏愨道返回原有路線，沿途巴士站維持不變。

## 服務時間及班次

| [堅尼地城](../Page/堅尼地城.md "wikilink")（[卑路乍灣](../Page/卑路乍灣.md "wikilink")）開 | [北角](../Page/北角.md "wikilink")（[健康中街](../Page/健康中街.md "wikilink")）開 |
| ----------------------------------------------------------------------- | ------------------------------------------------------------------- |
| **日期**                                                                  | **服務時間**                                                            |
| 星期一至五                                                                   | 06:00-07:30                                                         |
| 07:44                                                                   | 07:00-07:24                                                         |
| 07:56-09:20                                                             | 12                                                                  |
| 09:20-10:00                                                             | 10                                                                  |
| 10:00-17:00                                                             | 12                                                                  |
| 17:00-18:00                                                             | 15                                                                  |
| 18:00-20:00                                                             | 20                                                                  |
| 20:00-23:00                                                             | 15                                                                  |
|                                                                         | 17:00-00:00                                                         |
| 星期六                                                                     | 06:00-07:30                                                         |
| 07:30-10:00                                                             | 12-15                                                               |
| 10:00-19:00                                                             | 12                                                                  |
| 19:00-23:00                                                             | 15                                                                  |
| 星期日及公眾假期                                                                | 06:00-09:00                                                         |
| 09:00-19:00                                                             | 12                                                                  |
| 19:00-23:00                                                             | 15                                                                  |

## 收費

全程：$5.5

  - [永興街往北角](../Page/永興街.md "wikilink")（健康中街）：$4.3
  - [百德新街往堅尼地城](../Page/百德新街.md "wikilink")（卑路乍灣）：$4.8
  - [高樂花園往堅尼地城](../Page/高樂花園.md "wikilink")（卑路乍灣）：$4.0

### 八達通轉乘優惠

乘客登上本線後90分鐘內以同一張八達通卡轉乘以下路線，或從以下路線登車後90分鐘內以同一張八達通卡轉乘本線，次程可獲車資折扣優惠：

  - 18P往堅尼地城（卑路乍灣）→ 1或5B往堅尼地城，次程車資全免

<!-- end list -->

  - 18P←→4/4X、30X，次程可獲$1折扣優惠

<!-- end list -->

  - **18P任何方向** → 4/4X往華富或30X往數碼港
  - 4/4X或30X往中環 → **18P任何方向**

<!-- end list -->

  - 18P←→8P、27、722，次程可獲$1折扣優惠

<!-- end list -->

  - **18P往北角** → 8P往小西灣、27往寶馬山或722往耀東邨
  - 8P往灣仔碼頭、27往北角碼頭或722往中環碼頭 → **18P往堅尼地城**

## 使用車輛

本線現時主要用車為[Neoplan
Centroliner](../Page/Neoplan_Centroliner.md "wikilink")12米（60XX）、[亞歷山大丹尼士Enviro
500
12米或11.3米](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")（40XX、55XX）、[亞歷山大丹尼士Enviro
500 MMC
12米或11.3米](../Page/亞歷山大丹尼士Enviro_500_MMC.md "wikilink")（40XX、55XX/56XX/57XX）及[富豪超級奧林比安](../Page/富豪超級奧林比安.md "wikilink")（50XX/51XX）。此外，本線常與18X及113線共用車輛。

## 行車路線

**堅尼地城（卑路乍灣）開**經：[城西道](../Page/城西道.md "wikilink")、[西祥街北](../Page/西祥街.md "wikilink")、西祥街、[卑路乍街](../Page/卑路乍街.md "wikilink")、[荷蘭街](../Page/荷蘭街.md "wikilink")、[堅彌地城海旁](../Page/堅彌地城海旁.md "wikilink")、[德輔道西](../Page/德輔道西.md "wikilink")、[正街](../Page/正街.md "wikilink")、[干諾道西](../Page/干諾道西.md "wikilink")、[林士街天橋](../Page/林士街天橋.md "wikilink")、[干諾道中](../Page/干諾道中.md "wikilink")、[畢打街隧道](../Page/畢打街隧道.md "wikilink")、干諾道中、[夏慤道](../Page/夏慤道.md "wikilink")、[告士打道](../Page/告士打道.md "wikilink")、[維園道](../Page/維園道.md "wikilink")、[永興街](../Page/永興街.md "wikilink")、[英皇道](../Page/英皇道.md "wikilink")、[電照街](../Page/電照街.md "wikilink")、[渣華道](../Page/渣華道.md "wikilink")、[民康街](../Page/民康街.md "wikilink")、[健康西街](../Page/健康西街.md "wikilink")、[百福道及](../Page/百福道.md "wikilink")[健康中街](../Page/健康中街.md "wikilink")。

**北角（健康中街）開**經：健康中街、英皇道、[清風街天橋](../Page/清風街.md "wikilink")、維園道、告士打道、[杜老誌道天橋](../Page/杜老誌道.md "wikilink")、港灣道、[分域碼頭街](../Page/分域碼頭街.md "wikilink")、天橋、夏慤道、干諾道中、林士街天橋、干諾道西、[嘉安街](../Page/嘉安街.md "wikilink")、德輔道西、堅彌地城海旁、[山市街](../Page/山市街.md "wikilink")、卑路乍街、[加多近街](../Page/加多近街.md "wikilink")、堅彌地城新海旁及城西道。

### 沿線車站

[NWFB18PRtMap.png](https://zh.wikipedia.org/wiki/File:NWFB18PRtMap.png "fig:NWFB18PRtMap.png")

| [堅尼地城](../Page/堅尼地城.md "wikilink")（[卑路乍灣](../Page/卑路乍灣.md "wikilink")）開 | [北角](../Page/北角.md "wikilink")（[健康中街](../Page/健康中街.md "wikilink")）開 |
| ----------------------------------------------------------------------- | ------------------------------------------------------------------- |
| **序號**                                                                  | **車站名稱**                                                            |
| 1                                                                       | [卑路乍灣](../Page/卑路乍灣.md "wikilink")                                  |
| 2                                                                       | [荷蘭街](../Page/荷蘭街.md "wikilink")                                    |
| 3                                                                       | [西祥街](../Page/西祥街.md "wikilink")                                    |
| 4                                                                       | [歌連臣街](../Page/歌連臣街.md "wikilink")                                  |
| 5                                                                       | \-{[皇后大道西](../Page/皇后大道西.md "wikilink")}-                           |
| 6                                                                       | [山道](../Page/山道_\(香港\).md "wikilink")                               |
| 7                                                                       | [嘉安街](../Page/嘉安街.md "wikilink")                                    |
| 8                                                                       | [西區警署](../Page/西區警署.md "wikilink")                                  |
| 9                                                                       | [正街](../Page/正街.md "wikilink")                                      |
| 10                                                                      | [中山紀念公園](../Page/中山紀念公園.md "wikilink")                              |
| 11                                                                      | [香港演藝學院](../Page/香港演藝學院.md "wikilink")                              |
| 12                                                                      | [入境事務大樓](../Page/入境事務大樓.md "wikilink")                              |
| 13                                                                      | [銅鑼灣避風塘](../Page/銅鑼灣避風塘.md "wikilink")                              |
| 14                                                                      | [永興街](../Page/永興街.md "wikilink")                                    |
| 15                                                                      | [七海商業中心](../Page/七海商業中心.md "wikilink")                              |
| 16                                                                      | [電廠街](../Page/電廠街.md "wikilink")                                    |
| 17                                                                      | [糖水道](../Page/糖水道.md "wikilink")                                    |
| 18                                                                      | [琴行街](../Page/琴行街.md "wikilink")                                    |
| 19                                                                      | [港運城](../Page/港運城.md "wikilink")                                    |
| 20                                                                      | 北角（健康中街）                                                            |

## 參考資料及注釋

  - 《二十世紀港島區巴士路線發展史》，容偉釗編著，BSI出版
  - 《香港島巴士路線與社區發展》，ISBN：9789888310067，出版商：中華書局(香港)有限公司，出版日期：2014年11月

## 外部連結

  - 新巴

<!-- end list -->

  - [新巴18P線](https://web.archive.org/web/20160304073518/http://mobileapp.nwstbus.com.hk/nw/?l=0&f=1&r=18P&v=D)
  - [新巴18P線（特別班次）](https://web.archive.org/web/20160304085959/http://mobileapp.nwstbus.com.hk/nw/?l=0&f=1&r=18P&v=M)
  - [新巴18P線路線圖](http://www.nwstbus.com.hk/en/uploadedFiles/cust_notice/RI-NWFB-18-18P-D.pdf)

<!-- end list -->

  - 其他

<!-- end list -->

  - [新巴18P線－681巴士總站](http://681busterminal.com/hk-18p.html)

[018P](../Category/城巴及新世界第一巴士路線.md "wikilink")
[018P](../Category/中西區巴士路線.md "wikilink")
[018P](../Category/香港東區巴士路線.md "wikilink")

1.  [新巴18及18P號線調整服務時間](http://www.nwstbus.com.hk/en/uploadedFiles/~CustomerNotice/10082010-CB.pdf)，2010年8月10日，城巴／新巴
2.  [新巴18P號線加強服務](http://www.nwstbus.com.hk/en/uploadedFiles/~CustomerNotice/10112011-CB.pdf)，2011年11月10日，[城巴](../Page/城巴.md "wikilink")／[新巴](../Page/新巴.md "wikilink")
3.  [新巴乘客通告](http://www.nwstbus.com.hk/en/uploadedFiles/Poster/WIE0501NWFB.pdf)