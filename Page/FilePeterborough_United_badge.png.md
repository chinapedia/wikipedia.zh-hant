<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p><a href="../Page/皮特堡联足球俱乐部.md" title="wikilink">皮特堡联足球俱乐部會徽</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p>英文維基 Image:Peterborough United badge.png</p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2007.12.05</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p><a href="../Page/:en:User:The_Wilky_Bar_Kid.md" title="wikilink">The Wilky Bar Kid</a></p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>

### Fair-use rationale

1.  obtained from the club website
2.  low resolution image
3.  no non-copyright version available, by definition
4.  the logo is only being used for informational purposes
5.  its inclusion in the article adds significantly to the article
    because it is the primary means of identifying the subject of this
    article