**提婆**（，意为“圣-天”），[藏傳佛教譯為](../Page/藏傳佛教.md "wikilink")**聖天**、**聖提婆**，约[公元3世纪](../Page/公元3世纪.md "wikilink")[印度](../Page/古印度.md "wikilink")[大乘佛教人物](../Page/大乘佛教.md "wikilink")，[中觀派](../Page/中觀派.md "wikilink")[龍樹之徒](../Page/龍樹.md "wikilink")。因为他有一只[眼睛](../Page/眼睛.md "wikilink")[失明](../Page/失明.md "wikilink")，所以又被称为**片目天**（，音译为迦那提婆）；被佛教徒尊稱為**提婆菩萨**；在藏地，與其師[龍樹為被列入](../Page/龍樹.md "wikilink")**[二勝六莊嚴](../Page/二勝六莊嚴.md "wikilink")**之一。

## 简介

圣天是[大乘佛教](../Page/大乘佛教.md "wikilink")[中观派创始人](../Page/中观派.md "wikilink")[龙树的弟子](../Page/龙树.md "wikilink")。他的出身不详，中观派的另一位重要学者[月称认为](../Page/月称.md "wikilink")，圣天生于[斯里兰卡](../Page/斯里兰卡.md "wikilink")，而且是一位王子；而汉译的《[提婆菩萨传](../Page/提婆菩萨传.md "wikilink")》则认为他出生于[印度南部的一个](../Page/印度.md "wikilink")[婆罗门家庭](../Page/婆罗门.md "wikilink")。圣天从龙树处学习大乘般若之学。汉文资料说，圣天非常精于辩论。

圣天是许多重要的中观派论著的作者。他的著作的[梵文原本都没有保存下来](../Page/梵文.md "wikilink")，现在研究者能看到的是用[汉语和](../Page/汉语.md "wikilink")[藏语翻译过来的版本](../Page/藏语.md "wikilink")。但是对比可知，它们也不是完全翻译（尤其是汉译圣天著作）。圣天最有名的著述是《[四百论](../Page/四百论.md "wikilink")》，一共有16品，中文翻译了后8品（所谓《[广百论本](../Page/广百论本.md "wikilink")》）。

还有很多著作被归入圣天名下，但较严肃的西方研究者认为他们不是圣天本人写的。

## 生平

提婆是[龍樹的繼承人](../Page/龍樹.md "wikilink")，其與龍樹之分別，在於提婆激烈地批判[婆羅門](../Page/婆羅門.md "wikilink")「外道」，因此最後被婆羅門弟子所謀殺(見[鳩摩羅什](../Page/鳩摩羅什.md "wikilink")[提婆菩薩傳](../Page/提婆菩薩傳.md "wikilink"))。

## 著作

  - [百论](../Page/百论.md "wikilink")
  - [四百论](../Page/四百论.md "wikilink")\[1\]
  - [百字论](../Page/百字论.md "wikilink")\[2\]
  - [掌中论](../Page/掌中论.md "wikilink")\[3\]

## 注释

<references/>

## 资料来源

  - 《[辞海](../Page/辞海.md "wikilink")》，1979年版，705页提婆条
  - 朗，《圣天的四百论》，[哥本哈根](../Page/哥本哈根.md "wikilink")
  - 魏德迈，Aryadeva's Lamp that Integrates the Practices: The Gradual Path
    of Vajrayana Buddhism according to the Esoteric Community Noble
    Tradition，[纽约哥伦比亚大学出版社](../Page/纽约.md "wikilink")

## 外部链接

  - [圣天](https://web.archive.org/web/20070927024707/http://carolaroloff.de/uploads/Texte/TIBU77pp31-33Aryadeva.pdf)

[category:菩薩](../Page/category:菩薩.md "wikilink")
[category:中觀派學者](../Page/category:中觀派學者.md "wikilink")

[A](../Category/印度佛教出家眾.md "wikilink")
[Category:大成就者](../Category/大成就者.md "wikilink")
[Category:佛教哲學家](../Category/佛教哲學家.md "wikilink")

1.  月称认为此书与百论实际是一本书。
2.  [藏传佛教认为这是龙树的作品](../Page/藏传佛教.md "wikilink")。
3.  有研究者认为这是[陈那的作品](../Page/陈那.md "wikilink")。