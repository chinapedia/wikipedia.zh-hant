[T-moenycard.jpg](https://zh.wikipedia.org/wiki/File:T-moenycard.jpg "fig:T-moenycard.jpg")

**T-money**是一種在[韓國](../Page/韓國.md "wikilink")[首爾及鄰近城市使用的公共交通](../Page/首爾.md "wikilink")[智能卡系統](../Page/智能卡.md "wikilink")。使用非接觸式智能卡設計，基本卡售價為3,000[韓圆](../Page/韓圆.md "wikilink")，需另外增值；在首爾市內的[便利店](../Page/便利店.md "wikilink")、[7-11](../Page/7-11.md "wikilink")、[首爾地鐵各車站票務處等地發售及增值](../Page/首爾地鐵.md "wikilink")。
本智慧卡系統由韓國智慧卡公司負責營運與發行，該公司由[首爾市政府](../Page/首爾市政府.md "wikilink")34.4%控股、LG
CNS 31.85%控股以及信用卡聯盟15.71%控股。

## 使用範圍

  - 所有[首爾](../Page/首爾.md "wikilink")，[京畿道](../Page/京畿道.md "wikilink")，[仁川](../Page/仁川.md "wikilink")，
    [釜山](../Page/釜山市.md "wikilink"),[大邱](../Page/大邱.md "wikilink"),
    [大田和](../Page/大田广域市.md "wikilink")[光州巴士](../Page/光州.md "wikilink")
  - [首爾](../Page/首爾地鐵.md "wikilink")，[仁川](../Page/仁川地鐵.md "wikilink")，[釜山](../Page/釜山都市鐵道.md "wikilink")，[大邱](../Page/大邱地鐵.md "wikilink")，[大田以及](../Page/大田都市鐵道.md "wikilink")[光州都市鐵道網絡](../Page/光州都市鐵道.md "wikilink")
  - [仁川國際機場鐵道](../Page/仁川國際機場鐵道.md "wikilink")，[議政府輕電鐵](../Page/議政府輕電鐵.md "wikilink"),
    [龍仁輕電鐵](../Page/龍仁輕電鐵.md "wikilink"),
    [新盆唐線以及](../Page/新盆唐線.md "wikilink")[釜山－金海輕軌](../Page/釜山－金海輕軌.md "wikilink")
  - 所有[世宗特別自治市](../Page/世宗特別自治市.md "wikilink")，[忠清道以及](../Page/忠清道.md "wikilink")[全羅北道巴士](../Page/全羅北道.md "wikilink")
  - 所有[江原道巴士](../Page/江原道.md "wikilink")
  - 所有在[慶尙北道內的](../Page/慶尙北道.md "wikilink")(軍威,盈德,英陽,青松除外)的城市巴士
  - [慶尙南道城市巴士](../Page/慶尙南道.md "wikilink")
  - [全羅南道城市巴士](../Page/全羅南道.md "wikilink")(珍島,莞島除外)
  - 所有[濟州特別自治道巴士](../Page/濟州特別自治道.md "wikilink")

包括首爾四大宮殿([慶熙宮除外](../Page/慶熙宮.md "wikilink"))、部分景點和商店([教保文庫](../Page/教保文庫.md "wikilink")，[樂天世界主題公園](../Page/樂天世界.md "wikilink")，[GS25](../Page/GS25.md "wikilink")，CU/[FamilyMart](../Page/FamilyMart.md "wikilink"))也是接受T-money作為付款方式。

## 卡片種類

### 標準卡

T-money卡價格為2,500 -
4,000[韓圓](../Page/韓圓.md "wikilink")。可以在鐵路站，銀行櫃員機，便利店以及位於公共汽車站附近的信息亭進行購買和充值。自助充值機設置在首爾和釜山鐵道站內。

在2014年，T-money的「一卡通用」版正式推出。\[1\]
這有「一卡通用」的標誌以及和相比普通的T-money在卡內有略有不同的系統。在2016年3月，T-money的「一卡通用」版被主要的零售商接受，高速巴士及城際的一些公交路線，快速公路收費站，[韓國鐵道公社火車站](../Page/韓國鐵道公社.md "wikilink")，所有的地鐵系統以及除了金海市之外的所有公交系統。

### 優惠卡

優惠卡推出了兩種，一種是青年
(年齡為13-18)。另一個兒童(年齡為7-12)。兩者皆需要在購買時出示如青年卡或學生證等身份證明文。優惠卡必須在首次使用後10天內在互聯網進行註冊登記。註冊時需要提供卡號，國民身份證號或外國人註冊號。或者there
is a form available on the t-money website (under English Errors) that
can be emailed to customer service along with supporting documentation.
年長市民可享有免費交通，如持有身分證明文件也可在地鐵站的售票機購買免費票。

### T-money交通配件

也有更小、更耐用、附繩以便掛於手機上的T-money卡，售價約為6000至8000韓圓。也有內含T-money晶片的手錶、玩偶、MP3播放器、隨身碟、戒指及手環等。

### 相關卡

  - 定期票
    (정기권)：為[首爾和](../Page/首爾.md "wikilink")[仁川](../Page/仁川廣域市.md "wikilink")[地鐵的月票](../Page/地鐵.md "wikilink")。
  - T-money
    Mpass：與[首爾市](../Page/首爾.md "wikilink")[公車合作](../Page/公車.md "wikilink")，單日最多可搭乘大眾交通工具二十次。
  - Seoul Citypass Plus：同為T-money卡，於觀光景點有額外優惠。
  - Mobile T-money： is a
    RF-/[NFC](../Page/Near_field_communication.md "wikilink")-[Subscriber
    Identity Module](../Page/Subscriber_Identity_Module.md "wikilink")
    based T-money service. Mobile T-money application is available on
    [Google Play](../Page/Google_Play.md "wikilink") and each mobile
    service provider's ESD.

### 其他

  - eB Card(eBest Card, used in Gyeonggi & Incheon province), now
    discontinued. → See [cashBee](../Page/cashBee.md "wikilink").
  - Sensepass T-money, previously Topcash T-money, sold in the
    Gyeongsangnam and Gyeongsangbuk Provinces except Andong. Not wholly
    compatible with Smart T-money.
  - Hankkumi card, sold in Daejeon. This is a rebranded version of Smart
    T-money.
  - POP card, sold in [GS25](../Page/GS25.md "wikilink"). This is a
    rebranded Smart T-money with
    [GS\&Point](../Page/GS_Caltex.md "wikilink") and Happy Point
    [loyalty card](../Page/Loyalty_program.md "wikilink").
  - Narasarang Card, issued by [Shinhan
    Card](../Page/Shinhan_Card.md "wikilink")·[Shinhan
    Bank](../Page/Shinhan_Bank.md "wikilink") and
    [NMD/MMA](../Page/Ministry_of_National_Defence_\(Republic_of_Korea\).md "wikilink").
    This is a [K-Cash](../Page/K-Cash.md "wikilink") enabled [debit
    card](../Page/debit_card.md "wikilink") with POP card functionality.
    This card is only issued to ROTC officers and [ROK Armed
    Forces](../Page/Republic_of_Korea_Armed_Forces.md "wikilink")
    enlisted person including [KATUSA](../Page/KATUSA.md "wikilink").

## 歷史

  - 2004年4月22日 : 市政府宣佈將新交通卡命名為T-money。
    'T'代表英文的旅遊(Travel)、接觸(Touch)、交通(Traffic)以及科技(Technology)。
  - 2004年6月 : T-money閘機開始在鐵道站內安裝。幾個錯誤在完全運作之前被修正。
  - 2004年7月1日 : 系統正式開幕，這一天所有的搭乘均免費。
  - 2005年10月15日 : 仁川公共運輸系統開始接受T-money作付款方式。
  - 2005年12月6日 : T-money網上充值系統開始運行。
  - 2006年11月13日 : 京畿道公共運輸系統開始局部地接受T-money作付款方式。
  - 2008年8月4日 : 釜山都會區巴士開始接受T-money作付款方式。

## 參見

  - [RFID](../Page/無線射頻辨識.md "wikilink")

## 參考資料

## 外部連結

  - [T-money官方網站（韓語）](http://www.t-money.co.kr/)以及[（英語）](http://eng.koreasmartcard.com)
  - [首爾城市通行證](http://www.seoulcitypass.com/jsp/citypass/citypass_eng.jsp)
  - [T-Money
    首尔文化网](https://archive.is/20130502134818/http://www.visitseoul.net/ck/article/article.do?_method=view&m=0004007003005&p=07&art_id=38502&lang=ck)
  - [Hankkumi card官方網站](http://www.hankkumicard.co.kr)

[Category:乘車智能卡](../Category/乘車智能卡.md "wikilink")
[Category:首爾交通](../Category/首爾交通.md "wikilink")

1.