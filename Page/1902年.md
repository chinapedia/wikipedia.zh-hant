## 大事记

  - [布尔战争结束](../Page/布尔战争.md "wikilink")。
  - 地球上一次改變[克魯特尼的軌道](../Page/小行星3753.md "wikilink")。
  - [英國和](../Page/英國.md "wikilink")[日本兩國結成](../Page/日本.md "wikilink")[英日同盟](../Page/英日同盟.md "wikilink")。
  - [德](../Page/德國.md "wikilink")[英](../Page/英國.md "wikilink")[意三国舰队封锁](../Page/意大利.md "wikilink")[委内瑞拉海岸並炮轟](../Page/委内瑞拉.md "wikilink")[卡貝略港](../Page/卡貝略港.md "wikilink")。
  - [京師大學堂師範館成立](../Page/京師大學堂.md "wikilink")。
  - [1月8日](../Page/1月8日.md "wikilink")——[慈禧太后和](../Page/慈禧太后.md "wikilink")[光緒皇帝回到](../Page/光緒皇帝.md "wikilink")[北京](../Page/北京.md "wikilink")。
  - [1月18日](../Page/1月18日.md "wikilink")——慈禧太后第一次撤帘露面，召见各国驻华使节。
  - [1月28日](../Page/1月28日.md "wikilink")——[卡耐基华盛顿研究所成立](../Page/卡耐基华盛顿研究所.md "wikilink")。
  - [2月1日](../Page/2月1日.md "wikilink")——清廷准许[汉](../Page/汉族.md "wikilink")[满通婚](../Page/满族.md "wikilink")。
  - [2月8日](../Page/2月8日.md "wikilink")——[梁启超在](../Page/梁启超.md "wikilink")[日本创办](../Page/日本.md "wikilink")《[新民丛报](../Page/新民丛报.md "wikilink")》。
  - [4月8日](../Page/4月8日.md "wikilink")——[中国与](../Page/中国.md "wikilink")[俄国签署](../Page/俄罗斯帝国.md "wikilink")《[交收东三省条约](../Page/交收东三省条约.md "wikilink")》。
  - [5月8日](../Page/5月8日.md "wikilink")——[马提尼克岛上的](../Page/馬提尼克.md "wikilink")[培雷火山爆发](../Page/培雷火山.md "wikilink")，三万多人遇难。
  - [5月8日](../Page/5月8日.md "wikilink")——英国人[李提摩太和](../Page/李提摩太.md "wikilink")[山西巡抚](../Page/山西省.md "wikilink")[岑春煊共同创办](../Page/岑春煊.md "wikilink")[山西大学堂](../Page/山西大学堂.md "wikilink")(即现在的[山西大学](../Page/山西大学.md "wikilink"))。
  - [5月20日](../Page/5月20日.md "wikilink")——[古巴独立](../Page/古巴.md "wikilink")。
  - [5月21日](../Page/5月21日.md "wikilink")——[张之洞创立](../Page/张之洞.md "wikilink")[湖北师范学堂](../Page/湖北师范学堂.md "wikilink")。
  - [8月9日](../Page/8月9日.md "wikilink")——[爱德华七世被加冕为英国国王](../Page/爱德华七世_\(英国\).md "wikilink")。
  - [9月29日](../Page/9月29日.md "wikilink")——美国[百老汇第一个剧场开始营业](../Page/百老匯.md "wikilink")。
  - [10月16日](../Page/10月16日.md "wikilink")——[袁世凯向慈禧太后上书并創立直隶农务学堂](../Page/袁世凯.md "wikilink")（[河北农业大学前身](../Page/河北农业大学.md "wikilink")），位于[直隶省](../Page/直隶省.md "wikilink")（今[河北省](../Page/河北省.md "wikilink")）[保定市](../Page/保定市.md "wikilink")。
  - [11月24日](../Page/11月24日.md "wikilink")——袁世凯創立[北洋軍醫學堂](../Page/北洋軍醫學堂.md "wikilink")，位於[天津東門外海運局](../Page/天津市.md "wikilink")。

## 出生

  - [1月4日](../Page/1月4日.md "wikilink")——[廖繼春](../Page/廖繼春.md "wikilink")，台灣畫家（[1976年逝世](../Page/1976年.md "wikilink")）
  - [2月8日](../Page/2月8日.md "wikilink")——[德王](../Page/德王.md "wikilink")，內蒙古王公（[1966年逝世](../Page/1966年.md "wikilink")）
  - [2月10日](../Page/2月10日.md "wikilink")——[沃尔特·布喇顿](../Page/沃尔特·布喇顿.md "wikilink")，美国物理学家（[1987年逝世](../Page/1987年.md "wikilink")）
  - [3月7日](../Page/3月7日.md "wikilink")——[海茨·乌曼](../Page/海茨·乌曼.md "wikilink")，德国电影演员（[1994年逝世](../Page/1994年.md "wikilink")）
  - [3月13日](../Page/3月13日.md "wikilink")——[李梅樹](../Page/李梅樹.md "wikilink")，台灣畫家（[1983年逝世](../Page/1983年.md "wikilink")）
  - [4月23日](../Page/4月23日.md "wikilink")——[哈尔多尔·拉克斯内斯](../Page/哈尔多尔·拉克斯内斯.md "wikilink")，[冰岛小说家](../Page/冰岛.md "wikilink")，[1955年](../Page/1955年.md "wikilink")[诺贝尔文学奖获得者](../Page/诺贝尔文学奖.md "wikilink")（[1998年逝世](../Page/1998年.md "wikilink")）
  - [5月2日](../Page/5月2日.md "wikilink")——[阿圖羅·里卡沓](../Page/阿圖羅·里卡沓.md "wikilink")，曾是全世界最長壽在世男性。（[2014年逝世](../Page/2014年.md "wikilink")）
  - [5月30日](../Page/5月30日.md "wikilink")——（Giuseppina
    Projetto-Frau），義大利[超級人瑞](../Page/超級人瑞.md "wikilink")，最後一位出生於1902年之人類。（[2018年逝世](../Page/2018年.md "wikilink")）
  - [8月22日](../Page/8月22日.md "wikilink")——[莱妮·里芬斯塔尔](../Page/莱妮·里芬斯塔尔.md "wikilink")，德国电影导演和演员（[2003年逝世](../Page/2003年.md "wikilink")）
  - [8月28日](../Page/8月28日.md "wikilink")——[周培源](../Page/周培源.md "wikilink")，中国物理学家（[1993年逝世](../Page/1993年.md "wikilink")）
  - [9月20日](../Page/9月20日.md "wikilink")——[吴学周](../Page/吴学周.md "wikilink")，中国化学家（逝世[1983年](../Page/1983年.md "wikilink")）
  - [9月21日](../Page/9月21日.md "wikilink")——[艾德華·伊凡普理查](../Page/艾德華·伊凡-普理查.md "wikilink")，[英國](../Page/英国.md "wikilink")[人類學家](../Page/人类学家.md "wikilink")（[1973年逝世](../Page/1973年.md "wikilink")）
  - [9月23日](../Page/9月23日.md "wikilink")——[苏步青](../Page/苏步青.md "wikilink")，中国数学家（[2003年逝世](../Page/2003年.md "wikilink")）
  - [10月17日](../Page/10月17日.md "wikilink")——[梅爾福德·史蒂文森爵士](../Page/梅爾福德·史蒂文森.md "wikilink")，英國[高等法院法官](../Page/英格蘭及威爾斯高等法院.md "wikilink")（[1987年逝世](../Page/1987年.md "wikilink")）
  - [10月18日](../Page/10月18日.md "wikilink")——[帕斯库尔·约当](../Page/帕斯库尔·约当.md "wikilink")，德国物理学家（[1980年逝世](../Page/1980年.md "wikilink")）
  - [11月3日](../Page/11月3日.md "wikilink")——[王白淵](../Page/王白淵.md "wikilink")，台灣詩人（[1965年逝世](../Page/1965年.md "wikilink")）
  - [11月4日](../Page/11月4日.md "wikilink")——[雷哲·迪安](../Page/雷哲·迪安.md "wikilink")，英國最長壽男人（[2013年逝世](../Page/2013年.md "wikilink")）
  - [11月26日](../Page/11月26日.md "wikilink")——[罗荣桓](../Page/罗荣桓.md "wikilink")，[中國人民解放軍元帅](../Page/中国人民解放军.md "wikilink")（[1963年逝世](../Page/1963年.md "wikilink")）
  - [12月28日](../Page/12月28日.md "wikilink")——[沈从文](../Page/沈从文.md "wikilink")，[中国文学家](../Page/中国.md "wikilink")（[1988年逝世](../Page/1988年.md "wikilink")）

## 逝世

  - [9月29日](../Page/9月29日.md "wikilink")——[埃米尔·左拉](../Page/埃米尔·左拉.md "wikilink")，法国作家。（出生[1840年](../Page/1840年.md "wikilink")）
  - [9月19日](../Page/9月19日.md "wikilink")——[正岡子規](../Page/正岡子規.md "wikilink")，日本俳句中興改革者。（出生[1867年](../Page/1867年.md "wikilink")）
  - [10月6日](../Page/10月6日.md "wikilink")——[劉坤一](../Page/劉坤一.md "wikilink")，[清朝重要官員](../Page/清朝.md "wikilink")，[洋務運動後期領導者](../Page/洋務運動.md "wikilink")。（出生[1830年](../Page/1830年.md "wikilink")）

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[亨德里克·勞侖茲和](../Page/亨德里克·勞侖茲.md "wikilink")[彼得·塞曼](../Page/彼得·塞曼.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[赫尔曼·埃米尔·费歇尔](../Page/赫尔曼·埃米尔·费歇尔.md "wikilink")
  - [生理和医学](../Page/诺贝尔生理学或医学奖.md "wikilink")：[罗纳德·罗斯](../Page/罗纳德·罗斯.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[特奥多尔·蒙森](../Page/特奥多尔·蒙森.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[埃利·迪科门和](../Page/埃利·迪科门.md "wikilink")[夏尔莱·阿尔贝特·戈巴特](../Page/夏尔莱·阿尔贝特·戈巴特.md "wikilink")

[\*](../Category/1902年.md "wikilink")
[2年](../Category/1900年代.md "wikilink")
[0](../Category/20世纪各年.md "wikilink")