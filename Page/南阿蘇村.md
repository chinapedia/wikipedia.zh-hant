**南阿蘇村**（）是位于[日本](../Page/日本.md "wikilink")[熊本縣東北部](../Page/熊本縣.md "wikilink")[阿蘇火山](../Page/阿蘇火山.md "wikilink")[破火山口南部的一個](../Page/破火山口.md "wikilink")[村](../Page/村.md "wikilink")，屬[阿蘇郡](../Page/阿蘇郡.md "wikilink")。

南阿蘇村是[平成大合併中第一個合併後新設置的](../Page/平成大合併.md "wikilink")「村」（之後陸續有[福岡縣](../Page/福岡縣.md "wikilink")[東峰村和](../Page/東峰村.md "wikilink")[長野縣](../Page/長野縣.md "wikilink")[筑北村](../Page/筑北村.md "wikilink")）人口雖然比相鄰的[高森町](../Page/高森町_\(熊本縣\).md "wikilink")（約7500人）還多，但為了表現出更接近大自然的感覺，因此選擇繼續使用「村」。

## 歷史

### 年表

  - 1876年：實施[郡區町村編制法](../Page/郡區町村編制法.md "wikilink")，現在的轄區在當時分屬兩併村、白川村、吉田村、一關村、中松村、河陰村、久石村、河陽村、長野村、下野村。\[1\]
  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，重新整合為三村。
      - 兩併村、白川村、吉田村、一關村、中松村整合為[白水村](../Page/白水村.md "wikilink")。
      - 河陰村、久石村整合為[久木野村](../Page/久木野村.md "wikilink")。
      - 河陽村、長野村、下野村整合為[長陽村](../Page/長陽村.md "wikilink")。
  - 1956年8月1日：[菊池郡瀨田村被廢除](../Page/菊池郡.md "wikilink")，部份區域被併入長陽村，其餘地區被合併為[大津町](../Page/大津町.md "wikilink")。
  - 2005年2月13日：長陽村、白水村、久木野村[合併為南阿蘇村](../Page/市町村合併.md "wikilink")。

### 演變表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 -1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1999年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>吉田村、兩併村、白川村、<br />
一關村、中松村</p></td>
<td><p>白水村</p></td>
<td><p>2005年2月13日<br />
南阿蘇村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>河陽村、長野村、下野村</p></td>
<td><p>長陽村</p></td>
<td><p>1956年8月1日<br />
菊池郡瀬田村部分地區被併入</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>久石村、河陰村</p></td>
<td><p>久木野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [豐肥本線](../Page/豐肥本線.md "wikilink")：[立野車站](../Page/立野車站_\(熊本縣\).md "wikilink")
  - [南阿蘇鐵道](../Page/南阿蘇鐵道.md "wikilink")
      - [高森線](../Page/高森線.md "wikilink")：立野車站 -
        [長陽車站](../Page/長陽車站.md "wikilink") -
        [加勢車站](../Page/加勢車站.md "wikilink") -  -
        [南阿蘇水之誕生-{里}-白水高原站](../Page/南阿蘇水之誕生里白水高原站.md "wikilink")
        - [中松車站](../Page/中松車站.md "wikilink") -
        [阿蘇白川車站](../Page/阿蘇白川站.md "wikilink") -
        [見晴台車站](../Page/見晴台車站.md "wikilink")

## 觀光資源

  - [阿蘇山登山路線](../Page/阿蘇山.md "wikilink")－吉田線
  - 白川吉見神社
  - [白川水源](../Page/白川水源.md "wikilink")：[名水百選之一](../Page/名水百選.md "wikilink")
  - [保木下井手](../Page/保木下井手.md "wikilink")：[渠道百選之一](../Page/渠道百選.md "wikilink")
  - [地獄溫泉](../Page/地獄溫泉.md "wikilink")
  - [垂玉溫泉](../Page/垂玉溫泉.md "wikilink")
  - 阿蘇白水溫泉 瑠璃
  - [南阿蘇Luna天文台](../Page/南阿蘇Luna天文台.md "wikilink")：九州最大天文望遠鏡
  - 一心行之大櫻花樹：樹齢約400多年

<File:Shirakawa> Spring 02.jpg|白川水源 <File:一心行の大桜>
(熊本県南阿蘇村)2.jpg|一心行的大[櫻花樹與](../Page/櫻花.md "wikilink")[油菜花叢](../Page/油菜花.md "wikilink")

## 教育

### 大學

  - [東海大學農學部](../Page/東海大學_\(日本\).md "wikilink")

### 高等學校

  - 株式會社立[熊本清陵高等學校](../Page/熊本清陵高等學校.md "wikilink")

## 參考資料

## 外部連結

  - [南阿蘇村商工會](http://www.kumashoko.or.jp/minamiaso/)

  - [南阿蘇村觀光協會](http://www.minamiasokanko.jp/)

<!-- end list -->

1.