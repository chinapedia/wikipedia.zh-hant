新加坡武裝部隊的榮譽制度

## 獎章列表

| \# | 漢語名稱                                                      | 英語名稱 (漢語譯稱)                                                                     | 馬來語名稱                                     |
| -- | --------------------------------------------------------- | ------------------------------------------------------------------------------- | ----------------------------------------- |
| 1  | [卓越服務勳章 (武裝部隊)](../Page/新加坡武裝部隊卓越服務勳章.md "wikilink")      | Distinguished Service Order (Military) (卓越服務勳章 \[軍隊\])                          | Darjah Utama Bakti Cemerlang (Tentera)    |
| 2  | [英勇勳章 (武裝部隊)](../Page/新加坡武裝部隊英勇勳章.md "wikilink")          | Conspicuous Gallantry Medal (Military) (明顯英勇獎章 \[軍隊\])                          | Pingat Gagah Perkasa (Tentera)            |
| 3  | [卓越功績服務勳章 (武裝部隊)](../Page/新加坡武裝部隊卓越功績服務勳章.md "wikilink")  | Meritorious Service Medal (Military) (卓越服務勳章 \[軍隊\])                            | Pingat Jasa Gemilang (Tentera)            |
| 4  | [行政功績獎章 (金，武裝部隊)](../Page/新加坡武裝部隊行政功績金獎章.md "wikilink")   | Public Administration Medal, Gold (Military) (公共管理獎章 \[金，軍隊\])                  | Pingat Pentadbiran Awam, Emas (Tentera)   |
| 5  | [行政功績獎章 (銀，武裝部隊)](../Page/新加坡武裝部隊行政功績銀獎章.md "wikilink")   | Public Administration Medal, Silver (Military) ((公共管理獎章 \[銀，軍隊\])               | Pingat Pentadbiran Awam, Perak (Tentera)  |
| 6  | [行政功績獎章 (銅，武裝部隊)](../Page/新加坡武裝部隊行政功績銅獎章.md "wikilink")   | Public Administration Medal, Bronze (Military) ((公共管理獎章 \[銅，軍隊\])               | Pingat Pentadbiran Awam, Gangsa (Tentera) |
| 7  | [表揚獎章 (武裝部隊)](../Page/新加坡武裝部隊表揚獎章.md "wikilink")          | The Commendation Medal (Military) (表揚獎章 \[軍隊\])                                 | Pingat Kepujian (Tentera))                |
| 8  | [效率獎章](../Page/新加坡武裝部隊效率獎章.md "wikilink")                 | Efficiency Medal (Military) (效率獎章 \[軍隊\])                                       | Pingat Berkebolehan (Tentera)             |
| 9  | [新加坡武裝部隊海外獎章 (服務)](../Page/新加坡武裝部隊海外服務獎章.md "wikilink")   | Singapore Armed Forces Overseas Medal (Operational) (新加坡武裝部隊海外獎章 \[服務\])        |                                           |
| 10 | [新加坡武裝部隊海外獎章 (非服務)](../Page/新加坡武裝部隊海外非服務獎章.md "wikilink") | Singapore Armed Forces Overseas Medal (Non-operational) (新加坡武裝部隊海外獎章 \[非服務\])   |                                           |
| 11 | [長期服務和良好表現獎章](../Page/新加坡武裝部隊長期服務和良好表現獎章.md "wikilink")   | Singapore Armed Forces Long Service and Good Conduct Medal (新加坡武裝部隊長期服務和良好表現獎章) |                                           |
| 12 | [良好服務獎章](../Page/新加坡武裝部隊良好服務獎章.md "wikilink")             | Singapore Armed Forces Good Service Medal (新加坡武裝部隊良好服務獎章)                       |                                           |

## 参见

  - [新加坡荣誉制度](../Page/新加坡荣誉制度.md "wikilink")

[Category:新加坡军事](../Category/新加坡军事.md "wikilink")
[Category:新加坡荣誉制度](../Category/新加坡荣誉制度.md "wikilink")
[Category:軍事獎項及獎章](../Category/軍事獎項及獎章.md "wikilink")