**葛禮山**，又譯**沃尔特·昆廷·格雷沙姆**（，），[美国律师](../Page/美国.md "wikilink")、政治家，曾任[美国邮政部长](../Page/美国邮政部长.md "wikilink")（1883年-1884年）、[美国财政部长](../Page/美国财政部长.md "wikilink")（1884年）和[美国国务卿](../Page/美国国务卿.md "wikilink")（1893年-1895年）。

[Category:美国邮政部长](../Category/美国邮政部长.md "wikilink")
[Category:美国财政部长](../Category/美国财政部长.md "wikilink")
[Category:美国国务卿](../Category/美国国务卿.md "wikilink")
[Category:印第安纳州民主党人](../Category/印第安纳州民主党人.md "wikilink")
[Category:北军将领](../Category/北军将领.md "wikilink")