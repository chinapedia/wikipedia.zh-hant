**RKC華域克**是位於[荷蘭](../Page/荷蘭.md "wikilink")[瓦爾韋克的](../Page/瓦爾韋克.md "wikilink")[足球會](../Page/足球會.md "wikilink")。名字中的RKC全寫是'Rooms
Katholieke
Combinatie'，即[羅馬天主教的荷蘭文串法](../Page/羅馬天主教.md "wikilink")。華域克還是球會HEC、WVB和Hercules合併後的產物。球會在1940年8月26日成立，當年在Olympiaweg作為球會主場。後來在1996年，球會的主場搬至曼德馬爾斯球場，擁有6,200個座位，首場賽事是對[洛達](../Page/洛達.md "wikilink")。華域克被視為小球會，但在荷甲已停留了一段長時間。他們的主場顏色由黃色和藍色組成，在2004/05球季的平均入座人數為5,800人。

## 外部連結

  - [rkcwaalwijk.nl - 官方網站](http://www.rkcwaalwijk.nl/)

[R](../Category/荷蘭足球俱樂部.md "wikilink")
[Category:1940年建立的足球會](../Category/1940年建立的足球會.md "wikilink")