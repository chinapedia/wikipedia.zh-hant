**河東碧梧桐**（[日文](../Page/日文.md "wikilink")：*かわひがし へきごとう*），1873年2月26日出生 -
1937年2月1日逝世，[日本的俳人](../Page/日本.md "wikilink")、隨筆作家。

[日本](../Page/日本.md "wikilink")[愛媛縣](../Page/愛媛縣.md "wikilink")[溫泉郡千船町](../Page/溫泉郡_\(日本\).md "wikilink")（現[松山市千舟町](../Page/松山市.md "wikilink")）出生，[伊予松山藩藩士](../Page/伊予松山藩.md "wikilink")、明教館教授河東坤的五男，本名**兼五郎**（[日文](../Page/日文.md "wikilink")：*へいごろう*）。從小和[秋山真之海軍中將](../Page/秋山真之.md "wikilink")（也是[正岡子規的好友](../Page/正岡子規.md "wikilink")）非常親暱，暱稱他「阿淳」。

## 經歷

1888年進入伊予尋常中學（現[愛媛縣立松山東高等學校](../Page/愛媛縣立松山東高等學校.md "wikilink")）。1889年因為[正岡子規教他](../Page/正岡子規.md "wikilink")[棒球](../Page/棒球.md "wikilink")，而把[高濱虛子帶來跟](../Page/高濱虛子.md "wikilink")[正岡子規學俳句](../Page/正岡子規.md "wikilink")。1893年進入[第三高等學校](../Page/舊制第三高等學校_\(日本\).md "wikilink")，因學制改變轉至[第二高等學校後退學](../Page/舊制第二高等學校_\(日本\).md "wikilink")。

1902年[正岡子規去世後](../Page/正岡子規.md "wikilink")，繼子規後擔任新聞日本的俳句欄編輯。1905年開始推廣不依五七五調而作的**新傾向俳句**，1906年至1911年還為了宣傳新傾向俳句進行兩次的全國俳句行。

1933年3月25日的六十歲生日慶祝會上，發表從俳壇引退的消息。1937年1月因為[傷寒引發](../Page/傷寒.md "wikilink")[敗血症](../Page/敗血症.md "wikilink")，2月1日65歲的河東碧梧桐去世。遺骨分放於父母長眠的[松山市](../Page/松山市.md "wikilink")[寶塔寺及](../Page/寶塔寺.md "wikilink")[東京都](../Page/東京都.md "wikilink")[台東區的](../Page/台東區.md "wikilink")[梅林寺](../Page/梅林寺.md "wikilink")。

## 評論

河東碧梧桐與[高濱虛子為](../Page/高濱虛子.md "wikilink")[正岡子規門下的雙壁](../Page/正岡子規.md "wikilink")，但碧梧桐與擁護傳統五七五調的守舊派[高濱虛子有著激烈的對立](../Page/高濱虛子.md "wikilink")。
河東碧梧桐的新傾向俳句，起初是與主宰不遵守定型與季題、自由吟詠生活感情的[自由律俳句雜誌](../Page/自由律.md "wikilink")《層雲》的[荻原井泉水一起活動](../Page/荻原井泉水.md "wikilink")。
但是1915年因為和[荻原井泉水的意見不合而離開](../Page/荻原井泉水.md "wikilink")《層雲》，並於同年3月開始以俳句雜誌《海紅》為中心活動。碧梧桐離開《海紅》後則把工作交給[中塚一壁樓](../Page/中塚一壁樓.md "wikilink")。
昭和初年[風間直德試作了用漢語](../Page/風間直德.md "wikilink")[旁註標記的旁註俳句](../Page/旁註標記.md "wikilink")，對此贊同的碧梧桐也試著做了一些，不過並沒有得到太大的支持。

之後從俳壇引退這件事，可以說是對俳句創作的熱情衰退，也有跟虛子抗議的成分在。

[正岡子規評論河東碧梧桐跟](../Page/正岡子規.md "wikilink")[高濱虛子時曾說過](../Page/高濱虛子.md "wikilink")：「虛子炙熱如火；碧梧桐則涼冷如冰。」之語。

## 代表作

  - 蕎麦白き道すがらなり観音寺
  - 赤い椿白い椿と落ちにけり
  - 相撲乗せし便船のなど時化<small>（しけ）</small>となり
  - 雪チラチラ岩手颪<small>（おろし）</small>にならで止む
  - ミモーザを活けて一日留守にしたベットの白く
  - 曳かれる牛が辻でずっと見回した秋空だ

### 作品集

  - 《三千里》
  - 《新傾向句集》
  - 《八年間》

## 相關條目

  - [俳人列表](../Page/俳人列表.md "wikilink")

## 外部連結

  - [河東碧梧桐：作家別作品列表](http://www.aozora.gr.jp/index_pages/person510.html)（[青空文庫](../Page/青空文庫.md "wikilink")）
  - [自由律俳句《海紅》](http://www.kaikoh.com/)

[Category:日本詩人](../Category/日本詩人.md "wikilink")
[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:愛媛縣出身人物](../Category/愛媛縣出身人物.md "wikilink")
[Category:1873年出生](../Category/1873年出生.md "wikilink")
[Category:1937年逝世](../Category/1937年逝世.md "wikilink")