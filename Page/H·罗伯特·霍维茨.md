**霍华德·罗伯特·霍维茨**（，），[美国生物学家](../Page/美国.md "wikilink")，以研究[线虫动物门的](../Page/线虫动物门.md "wikilink")[秀丽隐杆线虫而著名](../Page/秀丽隐杆线虫.md "wikilink")。因发现器官发育和细胞程序性细胞死亡（细胞程序化凋亡）的遗传调控机理，与[悉尼·布伦纳](../Page/悉尼·布伦纳.md "wikilink")、[约翰·E·苏尔斯顿一起获得](../Page/约翰·E·苏尔斯顿.md "wikilink")2002年[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。

## 参考资料

  - [诺贝尔官方网站H·罗伯特·霍维茨自传](http://nobelprize.org/nobel_prizes/medicine/laureates/2002/horvitz-autobio.html)

[H](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[H](../Category/美国生物学家.md "wikilink")
[H](../Category/霍维茨奖获得者.md "wikilink")
[H](../Category/麻省理工學院校友.md "wikilink")
[H](../Category/哈佛大學校友.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")
[Category:格鲁伯遗传学奖获得者](../Category/格鲁伯遗传学奖获得者.md "wikilink")