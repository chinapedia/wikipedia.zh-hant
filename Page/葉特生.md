**葉特生**（），前[香港](../Page/香港.md "wikilink")[電視節目主持人](../Page/電視.md "wikilink")，1987至90年間與[佛教徒](../Page/佛教徒.md "wikilink")[莊文清一起淡出電視圈](../Page/莊文清.md "wikilink")、轉往宗教界發展。\[1\]

他畢業於[荔枝角衛民學校及](../Page/荔枝角衛民學校.md "wikilink")[金文泰中學](../Page/金文泰中學.md "wikilink")。1981年加入[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")，主持《[香港早晨](../Page/香港早晨.md "wikilink")》而廣為人認識。1986年11月一次身體檢查後證實患上[胃癌](../Page/胃癌.md "wikilink")，其後淡出幕前。

曾任職報刊記者、電台、電視台記者及採訪主任，電台監製，電視台早晨節目主持人等，兼任大專院校大眾傳播系講師；後來更把兼職徹底辭去，並轉職到真證傳播有限公司出任總幹事。現時亦是基督教的傳道者。1997年移居[美國](../Page/美國.md "wikilink")[三藩市](../Page/三藩市.md "wikilink")，亦有在當地傳道。

## 演出作品

### 節目主持（無綫電視）

  - [香港早晨](../Page/香港早晨.md "wikilink")（1981）
  - [香港小姐](../Page/香港小姐.md "wikilink")（1982）

## 寫作作品

  - 早晨葉特生（1984）
  - 小子踏八仙（1985）
  - 浮過生命海（1988）
  - 生命的活水（1988）
  - 生命的再思：一段出死入生的真實事蹟（1988）
  - 信是有情（1990）
  - 我想進天國（1991）
  - 人生路（1992）
  - 歲月留情（1992）
  - 養生道（1992）

## 參考

[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:宗教人物](../Category/宗教人物.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:金文泰中學校友](../Category/金文泰中學校友.md "wikilink")
[tak](../Category/葉姓.md "wikilink")

1.  [1](https://www.thestandnews.com/media/%E6%88%91%E5%9C%A8%E9%9B%BB%E8%A6%96%E5%8F%B0%E7%9A%84%E6%97%A5%E5%AD%90-%E4%B8%83-%E8%91%89%E7%89%B9%E7%94%9F/)