**C值**（英語：**C-value**）是指[真核生物](../Page/真核生物.md "wikilink")[細胞中](../Page/細胞.md "wikilink")，[單倍](../Page/單倍體.md "wikilink")[細胞核](../Page/細胞核.md "wikilink")（受精卵或[二倍體](../Page/二倍體.md "wikilink")[體細胞中的一半量](../Page/體細胞.md "wikilink")）裡所擁有的[DNA含量](../Page/DNA.md "wikilink")。有時候C值和[基因組大小兩個用詞可替換使用](../Page/基因組大小.md "wikilink")，不過對於[多倍體而言](../Page/多倍體.md "wikilink")，C值可能是指同一個細胞核中的兩個基因組。
一个物种单倍体基因组的DNA含量是相对的恒定的，它通常称为该物种DNA的C值。

## 詞源

有些人誤以為C值中的「C」是指「characteristic」、「content」或「complement」。當初於1950年鑄造此一用語的Hewson
Swift，並未精確地給出定義，在他的原始論文中，曾使用1C值、2C值等詞，是「classes」之意。

## 文獻

  - Bennett, M.D. and I.J. Leitch. 2005. Genome size evolution in
    plants. In *[The Evolution of the
    Genome](../Page/The_Evolution_of_the_Genome.md "wikilink")* (ed.
    T.R. Gregory), pp. 89-162. Elsevier, San Diego.
  - Gregory, T.R. 2001. Coincidence, coevolution, or causation? DNA
    content, cell size, and the C-value enigma. *Biological Reviews* 76:
    65-101.
  - Gregory, T.R. 2002. A bird's-eye view of the C-value enigma: genome
    size, cell size, and metabolic rate in the class Aves. *Evolution*
    56: 121-130.
  - Gregory, T.R. 2005. Genome size evolution in animals. In *[The
    Evolution of the
    Genome](../Page/The_Evolution_of_the_Genome.md "wikilink")* (ed.
    T.R. Gregory), pp. 3-87. Elsevier, San Diego.
  - Greilhuber, J., J. Dolezel, M. Lysak, and M.D. Bennett. 2005. The
    origin, evolution and proposed stabilization of the terms 'genome
    size' and 'C-value' to describe nuclear DNA contents. *Annals of
    Botany* 95: 255-260.
  - Swift, H. 1950. The constancy of deoxyribose nucleic acid in plant
    nuclei. *Proceedings of the National Academy of Sciences of the USA*
    36: 643-654.
  - Vendrely, R. and C. Vendrely. 1948. La teneur du noyau cellulaire en
    acide désoxyribonucléique à travers les organes, les individus et
    les espèces animales : Techniques et premiers résultats.
    *Experientia* 4: 434-436.

## 參見

  - [C值迷](../Page/C值迷.md "wikilink")
  - [基因組](../Page/基因組.md "wikilink")

## 外部連結

  - [Animal Genome Size Database](http://www.genomesize.com)
  - [Plant DNA C-values
    Database](https://web.archive.org/web/20050901105257/http://www.rbgkew.org.uk/cval/homepage.html)
  - [Fungal Genome Size
    Database](http://www.zbi.ee/fungal-genomesize/index.php)

[Category:DNA](../Category/DNA.md "wikilink")