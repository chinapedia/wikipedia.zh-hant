[Wang_Fai_Centre,_Wang_Tau_Hom_Estate_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Wang_Fai_Centre,_Wang_Tau_Hom_Estate_\(Hong_Kong\).jpg "fig:Wang_Fai_Centre,_Wang_Tau_Hom_Estate_(Hong_Kong).jpg")
[Wang_Tau_Hom_Estate_Plaza.jpg](https://zh.wikipedia.org/wiki/File:Wang_Tau_Hom_Estate_Plaza.jpg "fig:Wang_Tau_Hom_Estate_Plaza.jpg")
[Wang_Tau_Hom_Estate_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Wang_Tau_Hom_Estate_Basketball_Court.jpg "fig:Wang_Tau_Hom_Estate_Basketball_Court.jpg")
[Wang_Tau_Hom_Estate_Gym_Zone.jpg](https://zh.wikipedia.org/wiki/File:Wang_Tau_Hom_Estate_Gym_Zone.jpg "fig:Wang_Tau_Hom_Estate_Gym_Zone.jpg")
[Wang_Tau_Hom_Estate_Playground.jpg](https://zh.wikipedia.org/wiki/File:Wang_Tau_Hom_Estate_Playground.jpg "fig:Wang_Tau_Hom_Estate_Playground.jpg")
**橫頭磡邨**（**Wang Tau Hom
Estate**）原是一個[徙置屋邨](../Page/徙置屋邨.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[橫頭磡](../Page/橫頭磡.md "wikilink")，即[黃大仙西北部近](../Page/黃大仙.md "wikilink")[龍翔道的位置](../Page/龍翔道.md "wikilink")，總共有26座，於1962年建成。1980年代初[香港政府開始計劃重建](../Page/香港政府.md "wikilink")，建成現在的屋邨。新樓宇於1982年至1994年期間落成，現由[中國海外物業管理有限公司負責屋邨管理](../Page/中國海外物業管理有限公司.md "wikilink")。

在橫頭磡邨內的**富強苑**（**Fu Keung Court**）、**嘉強苑**（**Ka Keung
Court**）和**德強苑**（**Tak Keung
Court**）是[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")，由房屋署總建築師（3）設計，於1991年、1998年及2001年落成。當中德強苑及富強苑均是[樂富](../Page/樂富.md "wikilink")/橫頭磡綠表市場成交價最高的屋苑，也是九龍以至全香港綠表最高成交價最高的屋苑之一，\[1\]\[2\]\[3\]，未補地價每實呎成交價超過一萬港元。\[4\]

## 歷史

[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，大量內地難民移港定居，由於當時[香港政府沒有一個房屋政策來為低下階層而設的](../Page/香港政府.md "wikilink")，所以很多難民在1949年後在山邊興建[木屋來居住](../Page/寮屋.md "wikilink")，但這些木屋設備簡陋，衛生環境惡劣，加上木屋的密度高，所以經常發生火警，最終於1953年的聖誕夜，[石硤尾木屋區發生了史無前例的](../Page/石硤尾.md "wikilink")[大火](../Page/石硤尾大火.md "wikilink")，令5萬多人無家可歸，於是政府在原址興建29座[徙置大廈](../Page/徙置大廈.md "wikilink")，用以安置災民。自此以後，徙置大廈的興建奠定[香港公共房屋發展基礎](../Page/香港公共房屋.md "wikilink")。

除了在石硤尾、大坑東和李鄭屋一帶興建徙置大廈外，政府還在老虎岩（現稱[樂富](../Page/樂富.md "wikilink")）及本邨現址興建徙置大廈，為低下階層提供穩定居所，今天本邨所有徙置大廈已拆卸，當時這些徙置大廈由[徙置事務處管理](../Page/徙置事務處.md "wikilink")。1973年[香港房屋委員會成立](../Page/香港房屋委員會.md "wikilink")，所有徙置大廈和廉租屋大廈是由[香港房屋委員會統一管理](../Page/香港房屋委員會.md "wikilink")。

1980年代初，政府開始為徙置區屋邨進行計劃重建。然而，七層高廉租屋（徙置大廈）的外形大多以長型設計，加上本邨的位置鄰近[啟德機場](../Page/啟德機場.md "wikilink")，樓宇高度受限制，所以本邨大部分樓宇便使用了外觀較長而窄的[相連長型樓宇設計](../Page/相連長型.md "wikilink")，而本邨其中的-{宏}-光樓及-{宏}-顯樓，採用了相連長型樓宇類型中較少使用的「L」款的樓宇，相連長型L款樓宇全港公共屋邨中只有4座，還有[大窩口邨的富秀樓及富平樓使用相連長型L款樓宇](../Page/大窩口邨.md "wikilink")。而於1989年後才開始重建工程的樓宇，皆使用與外型與相連長型較相似的和諧三型樓宇設計。而樂富中心附近的樂富邨-{宏}-康樓、-{宏}-逸樓、-{宏}-順樓、-{宏}-達樓、-{宏}-旭樓及-{宏}-樂樓，原址皆位於重建前的橫頭磡邨，重建後該樓宇仍屬橫頭磡邨的，樓宇興建期間亦以「-{宏}-」命名，因管理上需要於1991年改屬[樂富邨](../Page/樂富邨.md "wikilink")。

由於基層市民獲得政府以遠低於市價的價錢獲得廉價租用居所的權利，令基層市民無需像西九龍的基層一樣繳付高昂租金住[板間房](../Page/板間房.md "wikilink")，反而公屋基層市民可以比私樓租客更容易累積財富，甚至將累積的財富給子女作首期，用作購買自置物業，提升社會階梯。可見香港政府對本區的房屋政策做得非常成功。

Wang Tau Hom Estate.jpg|橫頭磡邨北面，相連長型第一款樓宇 Wang Hin
House.jpg|橫頭磡邨-{宏}-顯樓，相連長型L款大廈 Wang Tau Ham Estate
open space 201710.jpg|宏業樓和宏基樓之間的休憩空間 Wang Tau Ham Estate open space1
2017.jpg|宏德樓對出的休憩空間 Wang Tau Hom Estate Playground (2).jpg|兒童遊樂場（2） Wang
Tau Hom Estate Playground (3).jpg|兒童遊樂場（3） Wang Tau Hom Estate
Playground (4).jpg|兒童遊樂場（4） Wang Tau Hom Estate Playground
(5).jpg|兒童遊樂場（5） Wang Tau Hom Estate Pavilion.jpg|涼亭 Wang
Tau Hom Estate Car Park.jpg|停車場

## 屋邨資料

### 重建後

  - 橫頭磡邨

| 樓宇名稱（座號）       | 樓宇類型                                   | 落成年份 |
| -------------- | -------------------------------------- | ---- |
| \-{宏}-暉樓       | [雙工字型](../Page/雙工字型.md "wikilink")     | 1982 |
| \-{宏}-顯樓（第5座）  | [相連長型L款](../Page/相連長型大廈.md "wikilink") | 1987 |
| \-{宏}-光樓（第6座）  |                                        |      |
| \-{宏}-澤樓（第4座）  | 相連長型第一款                                |      |
| \-{宏}-富樓（第2座）  |                                        |      |
| \-{宏}-興樓（第1座）  |                                        |      |
| \-{宏}-業樓（第9座）  | 1989                                   |      |
| \-{宏}-基樓（第8座）  |                                        |      |
| \-{宏}-亮樓（第13座） | 1990                                   |      |
| \-{宏}-頌樓（第10座） |                                        |      |
| \-{宏}-照樓（第12座） |                                        |      |
| \-{宏}-孝樓（第11座） |                                        |      |
| \-{宏}-安樓（第3座）  | 1988                                   |      |
| \-{宏}-德樓（第7座）  | 相連長型第三款                                |      |
| \-{宏}-耀樓（第15座） | [和諧三型](../Page/和諧三型.md "wikilink")     | 1994 |
| \-{宏}-禮樓（第14座） |                                        |      |
| \-{宏}-祖樓（第16座） |                                        |      |
| \-{宏}-偉樓（第17座） |                                        |      |
|                |                                        |      |

  - 富強苑

| 樓宇名稱（座別） | 樓宇類型                               | 落成年份      |
| -------- | ---------------------------------- | --------- |
| 富裕閣（A座）  | [新十字型](../Page/新十字型.md "wikilink") | 1991-1993 |
| 富寧閣（B座）  |                                    |           |
| 富康閣（C座）  |                                    |           |
| 富和閣（D座）  |                                    |           |
| 富逸閣（E座）  |                                    |           |
| 富雅閣（F座）  |                                    |           |
|          |                                    |           |

  - [嘉強苑](../Page/嘉強苑.md "wikilink")

| 樓宇名稱（座別） | 樓宇類型 | 落成年份 |
| -------- | ---- | ---- |
| 嘉匯閣（A座）  | 新十字型 | 1998 |
| 嘉盈閣（B座）  |      |      |
|          |      |      |

  - 德強苑

| 樓宇名稱（座別） | 樓宇類型 | 落成年份 |
| -------- | ---- | ---- |
| 德賢閣（A座）  | 新十字型 | 2001 |
| 德華閣（B座）  |      |      |
|          |      |      |

### 歷代樓宇

| 重建前的橫頭磡邨 |
| -------- |
| 樓宇座號     |
| 第1座      |
| 第2座      |
| 第3座      |
| 第4座      |
| 第5座      |
| 第6座      |
| 第7座      |
| 第8座      |
| 第9座      |
| 第10座     |
| 第11座     |
| 第12座     |
| 第13座     |
| 第14座     |
| 第15座     |
| 第16座     |
| 第17座     |
| 第18座     |
| 第19座     |
| 第20座     |
| 第21座     |
| 第22座     |
| 第23座     |
| 第24座     |
| 第25座     |
| 第26座     |

## 教育及福利設施

### 幼稚園

  - [五邑工商總會幼稚園](http://www.fdkg.edu.hk)（1988年創辦）（位於宏澤樓地下）
  - [樂富禮賢會幼稚園](http://lfkg.rhenish.org)（1993年創辦）（位於宏偉樓地下1-9室）
  - [禮賢會樂富幼兒園](http://lfc.ppe.rhenish.org)（1994年創辦）（位於宏祖樓地下）

<!-- end list -->

  - *已結束*

<!-- end list -->

  - 神召會華惠幼稚園（1988年創辦）（位於宏安樓地下）

**已結束的學校**

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>信生小學（橫頭磡1座天台）</li>
<li>紫陽學校/伯特利小學（橫頭磡2座天台）</li>
<li>使徒信心學校（橫頭磡3座天台）</li>
<li>使徒信心學校（3座天台）</li>
<li>神召會 華惠小學（橫頭磡4座天台）</li>
<li>潮光小學（橫頭磡5座天台）</li>
<li>博智小學（橫頭磡6座地下）</li>
<li>基英小學（橫頭磡6座地下）</li>
<li>孔教學院三樂小學（橫頭磡7座地下）</li>
<li>聖基學校/使徒信心會幼稚園（橫頭磡7座天台）</li>
<li>中華傳道會拔士小學（橫頭磡8座天台）</li>
<li>聖公會基心小學（橫頭磡8座地下）</li>
<li>中道學校（橫頭磡9座天台）</li>
<li>萬慈小學（橫頭磡9座地下）</li>
<li>信義學校（橫頭磡10座天台）</li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li>道教萬德幼稚園暨小學（橫頭磡11座天台）</li>
<li>伯大尼學校/孔仲岐紀念學校/培德學校（橫頭磡12座天台）</li>
<li>慈孝學校/育英小學（橫頭磡13座天台）</li>
<li>康樂小學（橫頭磡14座天台）</li>
<li>眉山小學（橫頭磡16座天台）</li>
<li>聖博德彌撒中心托兒所幼稚園（橫頭磡17座天台）</li>
<li>樂善堂橫頭磡小學（橫頭磡18座地下）</li>
<li>循道小學（橫頭磡19座地下）</li>
<li>飛鵬學校（橫頭磡20座天台）</li>
<li>基督佳音學校（橫頭磡21座天台）</li>
<li>神召會美基小學/康樂小學（橫頭磡22座天台）</li>
<li>宣道會宣恩小學（橫頭磡23座天台）</li>
<li>錫安傳道會幼稚園（橫頭磡24座天台）</li>
<li>紫陽學校（橫頭磡26座天台）</li>
<li>五邑工商總會托兒所（橫頭磡26座地下）</li>
</ul></td>
</tr>
</tbody>
</table>

### 中學

  - [樂善堂余近卿中學](../Page/樂善堂余近卿中學.md "wikilink")
  - [潔心林炳炎中學](../Page/潔心林炳炎中學.md "wikilink")
  - [中華基督教會扶輪中學](../Page/中華基督教會扶輪中學.md "wikilink")
  - [五旬節聖潔會永光書院](../Page/五旬節聖潔會永光書院.md "wikilink")

### 綜合青少年服務中心

  - [香港青年協會賽馬會橫頭磡青年空間](https://wth.hkfyg.org.hk)（位於宏光樓地下）

### 兒童中心

  - [安徒生會包威信中心](http://www.hac.org.hk/html/b5_se_01.php?catid=2&subcatid=2)（1985年成立）（位於宏暉樓地下）

### 綜合家居照顧服務

  - [鄰舍輔導會黃大仙區綜合家居照顧服務中心](https://www.naac.org.hk/index.php?id=640#.Wswtm4hubIU)（位於宏耀樓地下G01室）

### 長者鄰舍中心

  - [嗇色園主辦可平耆英鄰舍中心](https://www.e123.hk/site/ssyhp/home)（位於宏耀樓地下2號）

### 長者日間護理中心

  - [基督教家庭服務中心橫頭磡老人日間護理中心](http://www.cfsc.org.hk/unitweb/service/serv5/501.php?company_id=SRV5_5)（位於宏頌樓地下）

## 途經的公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: Yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: Yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

交通路線列表

</div>

<div class="NavContent" style="background-color: Yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[樂富站](../Page/樂富站.md "wikilink")

<!-- end list -->

  - [樂富巴士總站](../Page/樂富巴士總站.md "wikilink")

<!-- end list -->

  - [樂富廣場](../Page/樂富廣場.md "wikilink")2期專線小巴總站

<!-- end list -->

  - 樂富廣場至[廣播道線](../Page/廣播道.md "wikilink")
  - 樂富廣場至[筆架山線](../Page/筆架山.md "wikilink")

<!-- end list -->

  - 宏德樓/宏順樓專線小巴總站

<!-- end list -->

  - [聯合道](../Page/聯合道.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - [荃灣至](../Page/荃灣.md "wikilink")[九龍城](../Page/九龍城.md "wikilink")/[黃大仙線](../Page/黃大仙.md "wikilink")
    (下午至晚上時段)\[5\]

<!-- end list -->

  - [富美街](../Page/富美街.md "wikilink")

<!-- end list -->

  - [鳳舞街](../Page/鳳舞街.md "wikilink")/[杏林街](../Page/杏林街.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/香港小巴.md "wikilink")

<!-- end list -->

  - [荃灣至](../Page/荃灣.md "wikilink")[九龍城](../Page/九龍城.md "wikilink")/[黃大仙線](../Page/黃大仙.md "wikilink")
    (下午至晚上時段)

<!-- end list -->

  - [龍翔道](../Page/龍翔道.md "wikilink")

</div>

</div>

## 流行文化

  - 《[獅子山下](../Page/獅子山下.md "wikilink")》：1970年代的單元，曾以重建前的橫頭磡徙置區第24座作背景，而橫頭磡正位處獅子山之下　

## 知名人士

  - [吳業坤](../Page/吳業坤.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")\[6\]

## 資料來源

## 外部連結

  - [房屋署橫頭磡邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2709)
  - [香港地方 -
    有關樂富中心第一期的樓宇](http://www.hk-place.com/vp.php?board=1&id=2513-1)

[Category:橫頭磡](../Category/橫頭磡.md "wikilink")
[Category:1962年完工建築物](../Category/1962年完工建築物.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  [居屋德強苑實呎破萬創指標](http://orientaldaily.on.cc/cnt/finance/20150519/00202_003.html)
2.  [綠表客623.8萬購富強苑三房大單位](http://ps.hket.com/content/1580456/%E3%80%90%E5%B1%85%E4%BA%8C%E5%83%B9%E6%96%B0%E9%AB%98%E3%80%91%E7%B6%A0%E8%A1%A8%E5%AE%A2623.8%E8%90%AC%E8%B3%BC%E5%AF%8C%E5%BC%B7%E8%8B%91%E4%B8%89%E6%88%BF%E5%A4%A7%E5%96%AE%E4%BD%8D/)
3.  [富強苑綠表650萬沽
    東九居屋王](http://ps.hket.com/content/1637408/%E5%AF%8C%E5%BC%B7%E8%8B%91%E7%B6%A0%E8%A1%A8650%E8%90%AC%E6%B2%BD%20%E6%9D%B1%E4%B9%9D%E5%B1%85%E5%B1%8B%E7%8E%8B)
4.  [樂富德強苑788萬沽
    膺綠表居屋王](http://ps.hket.com/article/1779901/%E6%A8%82%E5%AF%8C%E5%BE%B7%E5%BC%B7%E8%8B%91788%E8%90%AC%E6%B2%BD%20%E8%86%BA%E7%B6%A0%E8%A1%A8%E5%B1%85%E5%B1%8B%E7%8E%8B)
5.  [荃灣荃灣街市街　—　黃大仙及九龍城](http://www.16seats.net/chi/rmb/r_kn45.html)
6.  [吳業坤以公屋仔為榮：我唔會自卑](http://hk.on.cc/hk/bkn/cnt/entertainment/20151231/bkn-20151231080129988-1231_00862_001.html)