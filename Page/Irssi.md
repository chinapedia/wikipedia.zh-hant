**Irssi**是一个[文本用户界面的](../Page/文本用户界面.md "wikilink")[IRC客户端程序](../Page/IRC.md "wikilink")，遵循[GPL发布](../Page/GPL.md "wikilink")。

最初Irssi是为了运行在[Unix-like操作系统上的](../Page/Unix-like.md "wikilink")（包括[Android](../Page/Android.md "wikilink")），但也能运行于[Microsoft
Windows上的](../Page/Microsoft_Windows.md "wikilink")[Cygwin](../Page/Cygwin.md "wikilink")。[OS
X平台上提供全功能的Irssi](../Page/OS_X.md "wikilink")，包括文本模式下使用Fink的版本，一个原生[GUI叫做Mac](../Page/GUI.md "wikilink")
Irssi的版本，还有从前的Cocoa客户端[Colloquy](../Page/Colloquy.md "wikilink")（基于他们自己的IRC库的实现）。

与其他[IRC客戶端程式不同之處是](../Page/IRC.md "wikilink")，Irssi 並非是基于
[ircII](../Page/ircII.md "wikilink")
代码，而是全部重新開發的。这使得开发者不用受已經存在的程式碼的限制。讓開發者可以維護並加強控制像是安全性還有客制化的問題。數量很多的模組還有[Perl脚本客制化了Irssi的運作還有介面](../Page/Perl.md "wikilink")。

Irssi 可以透過本身的使用者介面來修改設定，必要時也可以透過手動修改設定檔，該設定檔使用類似 Perl 的結構。

2002年5月Irssi的[autoconf操作檔configure被發現被植入](../Page/autoconf.md "wikilink")[後門長達兩個月](../Page/後門.md "wikilink")，然而[二進位執行檔沒有被植入](../Page/二進位.md "wikilink")。這個安全問題在被發現後馬上就被修正。

## 外部链接

  - [Official website](http://irssi.org/)
  - [Freshmeat project page](http://freshmeat.net/projects/irssi/)
  - [MacIrssi's home
    page](https://web.archive.org/web/20070403111534/http://www.g1m0.se/macirssi/)
  - [Cygwin/Windows
    binaries](http://anti.teamidiot.de/nei/2007/01/irssi_0810_for_windows_cygwinw/)
  - [SILC encryption plugin](http://penguin-breeder.org/silc/)
  - [FiSH encryption
    plugin](https://web.archive.org/web/20070324225410/http://fish.sekure.us/)
  - [ICQ plugin
    sourcecode](https://web.archive.org/web/20060719195902/http://developer.berlios.de/projects/irssi-icq/)

[Category:IRC客户端](../Category/IRC客户端.md "wikilink")