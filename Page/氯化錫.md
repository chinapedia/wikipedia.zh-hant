**氯化錫**，化學式[Sn](../Page/錫.md "wikilink")[Cl](../Page/氯.md "wikilink")，為無色發煙性液體。

## 製備

可於[氯化亞錫溶液中](../Page/氯化亞錫.md "wikilink")，通入過量的[氯气而製得](../Page/氯气.md "wikilink")。反應式：

  -
    \(\ SnCl_2+Cl_2\to SnCl_4\)

## 用途

氯化錫的蒸氣與[氨及](../Page/氨.md "wikilink")[水氣混合](../Page/水.md "wikilink")，生成[氫氧化錫及](../Page/氫氧化錫.md "wikilink")[氯化銨之微粒而呈濃煙狀](../Page/氯化銨.md "wikilink")，[軍事上用以製作](../Page/軍事.md "wikilink")[煙幕彈](../Page/煙幕彈.md "wikilink")。反應式：

  -
    \(\ SnCl_4+4NH_3+4H_2O\to Sn(OH)_4+4NH_4Cl\)

## 參考資料

[Category:锡化合物](../Category/锡化合物.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:金属卤化物](../Category/金属卤化物.md "wikilink")