**東區**（[臺灣客家語](../Page/臺灣客家語.md "wikilink")[海陸腔](../Page/海陸腔.md "wikilink")：dungˋ
kiˋ）[台灣](../Page/台灣.md "wikilink")[新竹市的一個市轄區](../Page/新竹市.md "wikilink")，位於該市東側，其西北為[北區](../Page/北區_\(新竹市\).md "wikilink")，西南為[香山區](../Page/香山區.md "wikilink")，南與[新竹縣](../Page/新竹縣.md "wikilink")[寶山鄉為鄰](../Page/寶山鄉_\(台灣\).md "wikilink")，東以[柯子湖溪與](../Page/柯子湖溪.md "wikilink")[竹東鎮交界](../Page/竹東鎮.md "wikilink")，東北隔[頭前溪與](../Page/頭前溪.md "wikilink")[竹北市相望](../Page/竹北市.md "wikilink")。境內有[國立清華大學和](../Page/國立清華大學.md "wikilink")[國立交通大學](../Page/國立交通大學.md "wikilink")，又有[新竹科學園區](../Page/新竹科學園區.md "wikilink")，為北台灣知名的高科技重鎮。人口數而言，東區是[市級市轄區第一大區](../Page/市_\(中華民國\).md "wikilink")，全國排名第24位\[1\]。

## 歷史

戰後，統合[日治時代](../Page/台灣日治時期.md "wikilink")[昭和](../Page/昭和.md "wikilink")10年（1935年）[町名改正設置的](../Page/町名改正.md "wikilink")[錦町](../Page/錦町_\(新竹市\).md "wikilink")、[旭町](../Page/旭町_\(新竹市\).md "wikilink")、[榮町](../Page/榮町_\(新竹市\).md "wikilink")、[東門町](../Page/東門町_\(新竹市\).md "wikilink")、[南門町](../Page/南門町.md "wikilink")、[花園町](../Page/花園町_\(新竹市\).md "wikilink")、[黑金町](../Page/黑金町.md "wikilink")、[新興町](../Page/新興町.md "wikilink")、[東勢](../Page/東勢_\(新竹市\).md "wikilink")、[赤土崎](../Page/赤土崎.md "wikilink")、[埔頂](../Page/埔頂.md "wikilink")、[柴梳山](../Page/柴梳山.md "wikilink")、[金山面](../Page/金山面.md "wikilink")、[溪埔子](../Page/溪埔子.md "wikilink")、[二十張犁](../Page/二十張犁.md "wikilink")、[九甲埔](../Page/九甲埔.md "wikilink")、[青草湖設置東區](../Page/青草湖_\(台灣\).md "wikilink")。

## 行政區

[Hsinchu_East_villages2.svg](https://zh.wikipedia.org/wiki/File:Hsinchu_East_villages2.svg "fig:Hsinchu_East_villages2.svg")

  - 聯里列表：\[2\]
      -
        東門聯-{里}-：親仁-{里}-、錦華-{里}-、復興-{里}-、中正-{里}-、文華-{里}-、復中-{里}-、東門-{里}-、榮光-{里}-、成功-{里}-、育賢-{里}-、三民-{里}-
        東勢聯-{里}-：前溪-{里}-、東園-{里}-、東勢-{里}-、光復-{里}-、公園-{里}-、建華-{里}-、東山-{里}-、綠水-{里}-、水源-{里}-、千甲-{里}-
        建功聯-{里}-：豐功-{里}-、武功-{里}-、軍功-{里}-、建功-{里}-、立功-{里}-、光明-{里}-
        埔頂聯-{里}-：仙宮-{里}-、科園-{里}-、埔頂-{里}-、龍山-{里}-、新莊-{里}-、關東-{里}-、仙水-{里}-、金山-{里}-、關新-{里}-
        南門聯-{里}-：南門-{里}-、關帝-{里}-、南市-{里}-、福德-{里}-、振興-{里}-、新興-{里}-
        竹蓮聯-{里}-：寺前-{里}-、竹蓮-{里}-、下竹-{里}-、頂竹-{里}-、南大-{里}-、光鎮-{里}-、新光-{里}-、湖濱-{里}-、高峰-{里}-、明湖-{里}-、柴橋-{里}-

## 人口

## 教育

<small>參考資料：</small>\[3\]

## 交通

[台鐵新竹站.JPG](https://zh.wikipedia.org/wiki/File:台鐵新竹站.JPG "fig:台鐵新竹站.JPG")\]\]

  - 鐵路
    [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [縱貫線](../Page/縱貫線_\(北段\).md "wikilink")：[新竹車站](../Page/新竹車站.md "wikilink")
    - [北新竹車站](../Page/北新竹車站.md "wikilink")
  - [內灣線](../Page/內灣線.md "wikilink")：[北新竹車站](../Page/北新竹車站.md "wikilink")
    - [千甲車站](../Page/千甲車站.md "wikilink") -
    [新莊車站](../Page/新莊車站_\(新竹市\).md "wikilink")

<!-- end list -->

  - 公路

<!-- end list -->

  - ：[新竹交流道](../Page/新竹交流道.md "wikilink")（95A、95B）

  - ：[茄苳交流道](../Page/茄苳交流道.md "wikilink")（103）

  -
  - （東西向快速公路，南寮－[竹東段](../Page/竹東鎮.md "wikilink")）

      - 新竹二交流道（7）
      - 竹科交流道（9）

  - ：竹北市－新竹市東區

  - ：新竹市北區－新竹市東區－竹東鎮

<!-- end list -->

  - 公共自行車

截至2018年1月10日，YouBike微笑單車在東區共設有31個站點。

## 旅遊

  - [北門街商圈](../Page/北門街商圈.md "wikilink")
  - [新竹關帝廟](../Page/新竹關帝廟.md "wikilink")
  - [竹塹城迎曦門](../Page/竹塹城.md "wikilink")
  - [新竹車站](../Page/新竹車站.md "wikilink")
  - [新竹市立動物園](../Page/新竹市立動物園.md "wikilink")
  - [福龍山宮](../Page/福龍山宮.md "wikilink")

<!-- end list -->

  - [新竹竹蓮寺](../Page/新竹竹蓮寺.md "wikilink")
  - [南壇大眾廟](../Page/南壇大眾廟.md "wikilink")
  - [新竹孔廟](../Page/新竹孔廟.md "wikilink")
  - [新竹神社](../Page/新竹神社.md "wikilink")
  - [青草湖](../Page/青草湖_\(台灣\).md "wikilink")
  - [靈隱寺](../Page/靈隱寺_\(新竹\).md "wikilink")

<!-- end list -->

  - [古奇峰](../Page/古奇峰.md "wikilink")
  - [鄭用錫墓](../Page/鄭用錫.md "wikilink")
  - [新竹市立玻璃工藝博物館](../Page/新竹市立玻璃工藝博物館.md "wikilink")
  - [埔頂金雞母](../Page/埔頂金雞母.md "wikilink")
  - [金山集福宮](../Page/金山集福宮.md "wikilink")
  - [義勇忠祠](../Page/義勇忠祠.md "wikilink")
  - [風空開山伯公](../Page/風空開山伯公.md "wikilink")

## 參考資料

## 外部連結

  - [東區區公所](http://dep-e-district.hccg.gov.tw/)

[Category:新竹市行政區劃](../Category/新竹市行政區劃.md "wikilink")
[竹](../Category/臺灣客家文化重點發展區.md "wikilink")
[東區_(新竹市)](../Category/東區_\(新竹市\).md "wikilink")

1.
2.  [新竹市東區戶政事務所-本轄區各聯里名稱](http://e-household.hccg.gov.tw/web/SG?pageID=20673&FP=D40000001923000007_2)

3.