是一部以香港漫畫家[馬榮成漫畫](../Page/馬榮成.md "wikilink")《[風雲](../Page/風雲_\(漫畫\).md "wikilink")》為基礎所改編的玄幻武俠電影，是1998年香港電影《[風雲：雄霸天下](../Page/風雲：雄霸天下_\(1998年電影\).md "wikilink")》的續集，在前作中擔當主演的[郭富城和](../Page/郭富城.md "wikilink")[鄭伊健在本片中再次攜手](../Page/鄭伊健.md "wikilink")，另有[任达华](../Page/任达华.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")、[何家勁](../Page/何家勁.md "wikilink")、[唐嫣等明星加盟其中](../Page/唐嫣.md "wikilink")。本片於2008年3月18日舉行新聞發布會\[1\]，3月底在泰國正式開拍，同年7月2日殺青\[2\]，2009年12月9日下午兩點在中國大陸正式上映\[3\]。

## 故事大綱

故事講述[聶風](../Page/聶風.md "wikilink")（[鄭伊健飾](../Page/鄭伊健.md "wikilink")）為了對付野心勃勃的絕無神（[任達華飾](../Page/任達華.md "wikilink")），不惜以身犯險修練魔功，[步驚雲](../Page/步驚雲.md "wikilink")（[郭富城飾](../Page/郭富城.md "wikilink")）、聶風兩人約定如聶風成魔後變成一個六親不認的魔頭，便由步驚雲殺死聶風，一場驚天地，決生死的終極之戰旋即展開。

## 演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>簡介</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭富城.md" title="wikilink">郭富城</a></p></td>
<td><p><a href="../Page/步驚雲.md" title="wikilink">步驚雲</a></p></td>
<td><p>聶風師兄<br />
學成新劍法後,實力大增</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭伊健.md" title="wikilink">鄭伊健</a></p></td>
<td><p><a href="../Page/聶風.md" title="wikilink">聶風</a></p></td>
<td><p>步驚雲師弟<br />
為打敗絕無神而入魔,但入魔未成便強行出關。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/任達華.md" title="wikilink">任達華</a></p></td>
<td><p>絕無神</p></td>
<td><p>絕心之父<br />
擁有強大無比的武功,意佂服中原,成為武林霸主。捉拿了不少武林人仕,包括無名和步驚雲,以逼眾人就範,讓計劃得以順利進行。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡卓妍.md" title="wikilink">蔡卓妍</a></p></td>
<td><p>第二夢</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/唐嫣.md" title="wikilink">唐　嫣</a></p></td>
<td><p>楚楚</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何家勁.md" title="wikilink">何家勁</a></p></td>
<td><p>無名</p></td>
<td><p>武林中的神話,外號天劍，武功超凡入聖<br />
在與絕無神一戰中戰敗重傷,其後傳授了自己的劍法予步驚雲。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彭冠期.md" title="wikilink">彭冠期</a></p></td>
<td><p>天行</p></td>
<td><p>絕無神手下</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁俊一.md" title="wikilink">梁俊一</a></p></td>
<td><p>絕地</p></td>
<td><p>絕無神手下</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/謝霆鋒.md" title="wikilink">謝霆鋒</a></p></td>
<td><p>絕心</p></td>
<td><p>絕無神之子<br />
替其父殺害了許多武林人仕</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃德斌.md" title="wikilink">黃德斌</a></p></td>
<td><p>邪皇</p></td>
<td><p>豬皇師兄<br />
曾好勝而走入魔道,但不能好好操控力量,而作出一些傷天害理的事,最後因避免復發不惜自斷雙臂。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林雪.md" title="wikilink">林　雪</a></p></td>
<td><p>豬皇</p></td>
<td><p>邪皇師弟<br />
樣貌醜陋,但本性善良。<br />
受無名所託,曾替風雲二人引見邪皇。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/譚耀文.md" title="wikilink">譚耀文</a></p></td>
<td><p>中原皇帝</p></td>
<td><p>被絕無神一眾脅逼打開皇宮後的龍塚之秘,其家屬亦受牽連。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳榮峻.md" title="wikilink">陳榮峻</a></p></td>
<td><p>曹公公</p></td>
<td><p>絕無神手下<br />
絕無神派往朝廷的內奸</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尹天照.md" title="wikilink">尹天照</a></p></td>
<td><p>威將軍</p></td>
<td></td>
</tr>
</tbody>
</table>

## 電影歌曲

粵語及國語主題曲〈**風雲義**〉

  - 作曲：[陳光榮](../Page/陳光榮.md "wikilink")
  - 作詞：[林　夕](../Page/林夕.md "wikilink")
  - 主唱：[郭富城](../Page/郭富城.md "wikilink")、[鄭伊健](../Page/鄭伊健.md "wikilink")

## 各地電影分級制度

| 國家／地區                    | 級別                       |
| ------------------------ | ------------------------ |
| <span style=>香港</span>   | <span style=>IIB</span>  |
| <span style=>台灣</span>   | <span style=>護</span>    |
| <span style=>新加坡</span>  | <span style=>PG</span>   |
| <span style=>馬來西亞</span> | <span style=>PG13</span> |
|                          |                          |

## 獎項

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/第29屆香港電影金像獎.md" title="wikilink">第29屆香港電影金像獎</a></p></td>
<td><p>最佳美術指導</p></td>
<td><p><a href="../Page/奚仲文.md" title="wikilink">奚仲文</a>、<a href="../Page/劉敏雄.md" title="wikilink">劉敏雄</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳服裝造型設計</p></td>
<td><p><a href="../Page/奚仲文.md" title="wikilink">奚仲文</a>、<a href="../Page/吳里璐.md" title="wikilink">吳里璐</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳動作設計</p></td>
<td><p><a href="../Page/馬玉成.md" title="wikilink">馬玉成</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳音響效果</p></td>
<td><p><a href="../Page/王慶生.md" title="wikilink">王慶生</a>、<a href="../Page/鄭穎園.md" title="wikilink">鄭穎園</a>、<a href="../Page/林紹儒.md" title="wikilink">林紹儒</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳視覺效果</p></td>
<td><p><a href="../Page/吳炫輝.md" title="wikilink">吳炫輝</a>、<a href="../Page/鄒志盛.md" title="wikilink">鄒志盛</a>、<a href="../Page/譚啟昆.md" title="wikilink">譚啟昆</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [《風雲Ⅱ》電影官方網站](https://web.archive.org/web/20110517083744/http://www.thestormwarriors.com/)

  - [《風雲II》新浪專題](http://ent.sina.com.cn/f/m/fengyun2/)

  -
  - {{@movies|ffhk31186371|風雲II}}

  -
  -
  -
  -
  -
  -
  - [《風雲II》](http://epg.i-cable.com/new/movie/the_storm_warriors.php?from_ch=041)有線寬頻
    i-CABLE

[Category:武俠片](../Category/武俠片.md "wikilink")
[Category:香港动作片](../Category/香港动作片.md "wikilink")
[Category:奇幻冒險電影](../Category/奇幻冒險電影.md "wikilink")
[Category:風雲](../Category/風雲.md "wikilink")
[Category:香港續集電影](../Category/香港續集電影.md "wikilink")
[9](../Category/2000年代香港電影作品.md "wikilink")
[Category:香港有線電視節目](../Category/香港有線電視節目.md "wikilink")
[Category:香港電影金像獎最佳視覺效果獲獎電影](../Category/香港電影金像獎最佳視覺效果獲獎電影.md "wikilink")
[Category:彭氏兄弟电影](../Category/彭氏兄弟电影.md "wikilink")

1.
2.
3.