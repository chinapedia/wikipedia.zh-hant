**林順潮**，祖籍潮州，[英皇書院及](../Page/英皇書院.md "wikilink")[香港大學畢業](../Page/香港大學.md "wikilink")，[香港著名](../Page/香港.md "wikilink")[眼科](../Page/眼.md "wikilink")[醫生](../Page/醫生.md "wikilink")。[香港中文大學眼科及視覺科學系前主任](../Page/香港中文大學.md "wikilink")、商人、[香港眼科醫院醫療部主管](../Page/香港眼科醫院.md "wikilink")，[深圳希瑪林順潮眼科醫院院長](../Page/深圳希瑪林順潮眼科醫院.md "wikilink")。

## 生平

1984年於[香港大學取得](../Page/香港大學.md "wikilink")[內外全科醫學士學位](../Page/內外全科醫學士.md "wikilink")，1998年起出任[香港中文大學眼科及視覺科學系主任](../Page/香港中文大學.md "wikilink")，他並擔任多項職位，包括[汕頭大學](../Page/汕頭大學.md "wikilink")—香港中文大學聯合汕頭國際眼科中心院長。

林順潮有參與多項[眼科](../Page/眼科.md "wikilink")[醫學及](../Page/醫學.md "wikilink")[手術研究](../Page/手術.md "wikilink")，他曾進行多項眼科病例手術，包括香港首宗[變形蟲上眼手術](../Page/變形蟲.md "wikilink")，及全球首宗眼眶神經鞘纖維水囊切除手術。他的研究範圍亦包括[近視及其相關眼疾](../Page/近視.md "wikilink")。

林順潮熱心於與眼科有關的公益事務，積極參與香港與中國內地的護眼活動，並親身到中國內地進行眼科手術，教育內地醫生；1997年參與[健康快車籌備工作](../Page/健康快車.md "wikilink")，1999年成立[光明行動護眼基金](../Page/光明行動.md "wikilink")。2004年，他策劃的「關心是潮流」農村扶貧醫療計劃，獲得[李嘉誠基金會捐款](../Page/李嘉誠基金會.md "wikilink")1000萬[人民幣支持](../Page/人民幣.md "wikilink")。2006年11月，與其他香港醫學界人士發起[亮睛工程](../Page/亮睛工程.md "wikilink")，主力在中國內地進行扶貧及除盲的工作。計劃未來5年在內地貧困地區的醫院設立100個[白內障手術站](../Page/白內障.md "wikilink")，籌募的經費共1億[人民幣](../Page/人民幣.md "wikilink")。

林順潮於1994年獲選為[香港十大傑出青年](../Page/香港十大傑出青年.md "wikilink")，1995年當選[世界傑出青年](../Page/世界傑出青年.md "wikilink")，1998年獲得香港傑出青年領袖獎，2000年獲選為[明日世界領袖](../Page/明日世界領袖.md "wikilink")。他曾把自己的童年及求學時期回憶編寫成書。2008年1月，他當選[港區全國人大代表](../Page/港區全國人大代表.md "wikilink")。同年5月，獲選為[北京2008年奧運會香港區火炬接力火炬手](../Page/北京2008年奧運會香港區火炬接力.md "wikilink")。
林順潮其開設的香港(國際)眼科醫療集團有限公司，遭病人Wu Lai
Ying入稟高等法院，指兩被告於2013年3月26日至2015年8月5日替其治療期間出現醫療失誤，令其身體受傷，案件正於高等法院排期開審。\[1\]據林順潮於聯交所招股書的文件披露，該病人患有乾眼症，並接受林順潮於深圳進行激光繳視手術失敗，其後有嚴重後遺症，案件預計於2019年開庭審訊。

林於2017月12月19日獲選為第十三届港區人大代表。

## 家庭

林順潮曾被傳媒形容為「鑽石王老五」。2006年3月26日，林順潮與祖籍[遼寧](../Page/遼寧.md "wikilink")[瀋陽的](../Page/瀋陽.md "wikilink")[李肖婷結婚](../Page/李肖婷.md "wikilink")，李肖婷曾代表[紐西蘭](../Page/紐西蘭.md "wikilink")[奧克蘭參選](../Page/奧克蘭.md "wikilink")2004年[國際華裔小姐競選但落選](../Page/國際華裔小姐.md "wikilink")，並認識身為評判的林順潮。李肖婷其後加入中大醫學院眼科及視覺科學系，擔任計劃協調員。2005年7月，二人在[西藏參加健康快車義工活動中互生情愫](../Page/西藏.md "wikilink")，至[聖誕節](../Page/聖誕節.md "wikilink")，他親身前往瀋陽求婚。林順潮曾在2006年[情人節前夕](../Page/情人節.md "wikilink")，於[香港電台節目中向當時的未婚妻發出](../Page/香港電台.md "wikilink")「每天愛你多一些」甜蜜宣言。

## 書籍作品

  - 窮小子·傑青醫生--我的成長剪影 ISBN 9624519293
  - 香港醫療面面觀 ISBN 9621786371

## 公職

  - [健康快車創會執委會委員](../Page/健康快車.md "wikilink")
  - [光明行動護眼基金創會主席](../Page/光明行動.md "wikilink")
  - [香港盲人輔導會副贊助人兼醫療顧問](../Page/香港盲人輔導會.md "wikilink")
  - [香港獅子會眼庫名譽醫療顧問暨執委委員及研究總監](../Page/香港獅子會.md "wikilink")
  - [環境諮詢委員會委員](../Page/環境諮詢委員會.md "wikilink")(任期已滿)

## 榮譽

  - [香港十大傑出青年](../Page/香港十大傑出青年.md "wikilink")（1994年）
  - [世界十大傑出青年](../Page/世界十大傑出青年.md "wikilink")（1995年）
  - 香港傑出青年領袖獎（1998年）
  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（2004年）

## 資料來源

  - [光明行動](https://web.archive.org/web/20070611082246/http://www.afv.org.hk/introduction_c.htm)
  - [十大傑出青年系列（四之四）林順潮：不只為別人帶來光明](http://www.jiujik.com/jsexpertsdetails.php?lcid=HK.B5&artid=3000004704)
    招職
  - [逆流而上 林順潮](http://www.com.cuhk.edu.hk/ubeat/001139/school.htm) 大學線
    第39期
  - [淡泊名利，帶動"關心是潮流"-訪香港中文大學醫學院副院長林順潮](http://www.zijing.com.cn/GB/channel3/50/200506/06/1596.html)
    紫荊雜誌
  - [關心是潮流計劃](http://www.lksf.org/big5/media/press/20041226.shtml)
    李嘉誠基金會
  - [林順潮下月娶華姐](http://news.sina.com.hk/cgi-bin/news/show_news.cgi?date=2006-02-14&type=headlines&ct=headlines&id=1755023)
    星島日報，2006年2月14日
  - [林順潮娶華姐中大下屬 任選美評判結識
    否認引薦入中大](https://web.archive.org/web/20160414173036/http://www.mingpaohealth.com/cfm/news3.cfm?File=20060214%2Fnews%2Fgsa1.txt)
    明報，2006年2月14日
  - [林順潮3月做「老襯」](http://www.singpao.com/20060215/local/812532.html)
    成報，2006年2月15日
  - [亮睛工程啟動籌得一千萬](http://www.takungpao.com/news/06/11/06/GW-646628.htm)
    大公報，2006年11月6日
  - [港眼科專家創建亮睛工程每年為十萬患者做手術](http://www1.chinesenewsnet.com/MainNews/SocDigest/Technology/zxs_2007_949078.shtml)
    多維新聞網
  - [中大研究抑制近視加深新藥的成效](http://www.cuhk.edu.hk/ipro/010403c.htm) 香港中文大學
  - [眼科手术后受伤　病人入禀向名医林顺潮索偿，東網， 2016](http://hk.on.cc/hk/bkn/cnt/news/20160324/bkn-20160324114132818-0324_00822_001_cn.html)
  - [林順潮涉醫療失誤
    遭病人入稟索償，巴士的報，2016](http://mxiii.bastillepost.com/china/3-%E7%A4%BE%E6%9C%83%E4%BA%8B/1075396-%E6%9E%97%E9%A0%86%E6%BD%AE%E6%B6%89%E9%86%AB%E7%99%82%E5%A4%B1%E8%AA%A4-%E9%81%AD%E7%97%85%E4%BA%BA%E5%85%A5%E7%A8%9F%E7%B4%A2%E5%84%9F?r=w)

[Category:醫生出身的政治人物](../Category/醫生出身的政治人物.md "wikilink")
[Category:香港醫學界人士](../Category/香港醫學界人士.md "wikilink")
[Category:香港科學家](../Category/香港科學家.md "wikilink")
[Category:香港中文大學教授](../Category/香港中文大學教授.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:英皇書院校友](../Category/英皇書院校友.md "wikilink")
[Category:第十一屆港區全國人大代表](../Category/第十一屆港區全國人大代表.md "wikilink")
[Category:第十二屆港區全國人大代表](../Category/第十二屆港區全國人大代表.md "wikilink")
[Category:香港十大傑出青年](../Category/香港十大傑出青年.md "wikilink")
[Category:世界傑出青年](../Category/世界傑出青年.md "wikilink")
[Category:未來全球領袖](../Category/未來全球領袖.md "wikilink")
[Category:第十三屆港區全國人大代表](../Category/第十三屆港區全國人大代表.md "wikilink")
[Category:醫生出身的商人](../Category/醫生出身的商人.md "wikilink")
[Category:潮州人](../Category/潮州人.md "wikilink")
[S](../Category/林姓.md "wikilink")

1.