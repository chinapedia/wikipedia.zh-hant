**八束郡**是過去隸屬[島根縣的一](../Page/島根縣.md "wikilink")[郡](../Page/郡.md "wikilink")。

廢止前轄有以下1町：

  - [東出雲町](../Page/東出雲町.md "wikilink")

過去的範圍還包括現在的[松江市全域以及](../Page/松江市.md "wikilink")[出雲市東北部的伊野地區](../Page/出雲市.md "wikilink")。

## 歷史

### 年表

  - 1896年4月1日：[島根郡](../Page/島根郡.md "wikilink")、[意宇郡與](../Page/意宇郡.md "wikilink")[秋鹿郡合併為](../Page/秋鹿郡.md "wikilink")**八束郡**，下轄：[法吉村](../Page/法吉村.md "wikilink")、[生馬村](../Page/生馬村.md "wikilink")、[本庄村](../Page/本庄村.md "wikilink")、[持田村](../Page/持田村.md "wikilink")、[朝酌村](../Page/朝酌村.md "wikilink")、[東川津村](../Page/東川津村.md "wikilink")、[西川津村](../Page/西川津村.md "wikilink")、[講武村](../Page/講武村.md "wikilink")、[御津村](../Page/御津村.md "wikilink")、[野波村](../Page/野波村.md "wikilink")、[大蘆村](../Page/大蘆村.md "wikilink")、[加賀村](../Page/加賀村.md "wikilink")、美保關村、[千酌村](../Page/千酌村.md "wikilink")、[片江村](../Page/片江村.md "wikilink")、[森山村](../Page/森山村.md "wikilink")、[津田村](../Page/津田村.md "wikilink")、[竹矢村](../Page/竹矢村.md "wikilink")、[大庭村](../Page/大庭村.md "wikilink")、[乃木村](../Page/乃木村.md "wikilink")、[忌部村](../Page/忌部村.md "wikilink")、[玉造村](../Page/玉造村.md "wikilink")、[湯町村](../Page/湯町村.md "wikilink")、宍道村、來待村、[波入村](../Page/波入村.md "wikilink")、[二子村](../Page/二子村.md "wikilink")、[岩坂村](../Page/岩坂村.md "wikilink")、[熊野村](../Page/熊野村.md "wikilink")、揖屋村、[意東村](../Page/意東村.md "wikilink")、[出雲鄉村](../Page/出雲鄉村.md "wikilink")、[大野村](../Page/大野村.md "wikilink")、[秋鹿村](../Page/秋鹿村.md "wikilink")、[古志村](../Page/古志村.md "wikilink")、[古曾志村](../Page/古曾志村.md "wikilink")、[長江村](../Page/長江村.md "wikilink")、惠曇村、[佐太村](../Page/佐太村.md "wikilink")、[伊野村](../Page/伊野村.md "wikilink")。（40村）
  - 1903年4月1日：東川津村和西川津村[合併為](../Page/市町村合併.md "wikilink")[川津村](../Page/川津村.md "wikilink")。（39村）
  - 1905年4月1日：玉造村和湯町村合併為玉湯村。（38村）
  - 1908年5月1日：古志村、古曾志村、長江村合併為[古江村](../Page/古江村.md "wikilink")。（36村）
  - 1924年1月1日：美保關村改制為[美保關町](../Page/美保關町.md "wikilink")。（1町35村）
  - 1927年11月1日：宍道村改制為[宍道町](../Page/宍道町.md "wikilink")。（2町34村）
  - 1929年1月1日：波入村和二子村合併為八束村。（2町33村）
  - 1934年12月1日：津田村被併入[松江市](../Page/松江市.md "wikilink")。\[1\]（2町32村）
  - 1935年1月1日：揖屋村改制為[揖屋町](../Page/揖屋町.md "wikilink")。（3町31村）
  - 1939年2月11日：川津村被併入松江市。（3町30村）
  - 1939年11月1日：朝酌村被併入松江市。（3町29村）
  - 1947年12月28日：惠曇村改制為[惠曇町](../Page/惠曇町.md "wikilink")。（4町28村）
  - 1948年10月10日：法吉村被併入松江市。（4町27村）
  - 1950年9月21日：竹矢村和乃木村被併入松江市。（4町25村）
  - 1951年4月1日：（4町22村）
      - 大庭村被廢除。
      - 岩坂村、熊野村與大庭村的部分地區合併為[八雲村](../Page/八雲村_\(島根縣\).md "wikilink")。
      - 忌部村與大庭村的其他地區被併入松江市。
  - 1953年4月1日：生馬村和持田村被併入松江市。（4町20村）
  - 1954年4月1日：揖屋町、意東村、出雲鄉村合併為[東出雲町](../Page/東出雲町.md "wikilink")。\[2\]（4町18村）
  - 1955年3月10日：本庄村和古江村被併入松江市。（4町16村）
  - 1955年4月3日：宍道町和來待村合併為新設立的宍道町。（4町15村）
  - 1955年4月13日：美保關町、千酌村、片江村、森山村合併為新設立的美保關町。（4町12村）
  - 1956年1月10日：野波村、大蘆村、加賀村合併為島根村。（4町10村）
  - 1956年3月3日：惠曇町、佐太村、講武村、御津村合併為[鹿島町](../Page/鹿島町_\(島根縣\).md "wikilink")。（4町7村）
  - 1959年1月1日：玉湯村改制為[玉湯町](../Page/玉湯町.md "wikilink")。（5町6村）
  - 1960年4月1日：伊野村被併入平田市。（5町5村）
  - 1960年8月1日：大野村和秋鹿村被併入松江市。（5町3村）
  - 1969年4月1日：島根村改制為[島根町](../Page/島根町.md "wikilink")。（6町2村）
  - 1970年4月1日：八束村改制為[八束町](../Page/八束町.md "wikilink")。（7町1村）
  - 2005年3月31日：鹿島町、島根町、宍道町、玉湯町、美保關町、八束町、八雲村與松江市合併為新設立的松江市。（1町）
  - 2011年8月1日：東出雲町被併入松江市，同時八束郡因無管轄町村而廢除。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>意宇郡</p></td>
<td><p>揖屋村</p></td>
<td><p>1935年1月1日<br />
揖屋町</p></td>
<td><p>1954年4月1日<br />
東出雲町</p></td>
<td><p>2011年8月1日<br />
併入松江市</p></td>
<td><p>松江市</p></td>
</tr>
<tr class="even">
<td><p>意東村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>出雲鄉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>玉造村</p></td>
<td><p>1905年4月1日<br />
玉湯村</p></td>
<td><p>1959年1月1日<br />
玉湯町</p></td>
<td><p>2005年3月31日<br />
松江市</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>湯町村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>宍道村</p></td>
<td><p>1927年11月1日<br />
宍道町</p></td>
<td><p>1955年4月3日<br />
宍道町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>來待村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>波入村</p></td>
<td><p>1929年1月1日<br />
八束村</p></td>
<td><p>1970年4月1日<br />
八束町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>二子村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>岩坂村</p></td>
<td><p><br />
1951年4月1日<br />
八雲村<br />
</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>熊野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大庭村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1951年4月1日<br />
松江市</p></td>
<td><p>松江市</p></td>
<td><p>松江市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>忌部村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>竹矢村</p></td>
<td><p>1950年9月21日<br />
松江市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>乃木村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>津田村</p></td>
<td><p>1934年12月1日<br />
松江市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>松江市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>rowspan= 16 | 島根郡</p></td>
<td><p>東川津村</p></td>
<td><p>1903年4月1日<br />
川津村</p></td>
<td><p>1939年2月11日<br />
松江市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>西川津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>朝酌村</p></td>
<td><p>1939年11月1日<br />
松江市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>法吉村</p></td>
<td><p>1948年10月10日<br />
松江市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>生馬村</p></td>
<td><p>1953年4月1日<br />
松江市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>持田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>本庄村</p></td>
<td><p>1955年3月10日<br />
松江市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>野波村</p></td>
<td><p>1956年1月10日<br />
島根村</p></td>
<td><p>1969年4月1日<br />
島根町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大蘆村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>加賀村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>美保關村</p></td>
<td><p>1924年1月1日<br />
美保關町</p></td>
<td><p>1955年4月13日<br />
美保關町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>千酌村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>片江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>森山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>講武村</p></td>
<td><p>1956年3月3日<br />
鹿島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>御津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>秋鹿郡</p></td>
<td><p>惠曇村</p></td>
<td><p>1947年12月28日<br />
惠曇町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>佐太村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大野村</p></td>
<td><p>1960年8月1日<br />
松江市</p></td>
<td><p>松江市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>秋鹿村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>古志村</p></td>
<td><p>1908年5月1日<br />
古江村</p></td>
<td><p>1955年3月10日<br />
松江市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>古曾志村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>長江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>伊野村</p></td>
<td><p>1960年4月1日<br />
被併入平田市</p></td>
<td><p>2005年3月22日<br />
合併為出雲市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

1.
2.