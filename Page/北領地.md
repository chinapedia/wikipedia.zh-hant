**北領地**（[英語](../Page/英語.md "wikilink")：Northern
Territory），[澳大利亚的一个自治地区](../Page/澳大利亚.md "wikilink")，位于大陆北部的中央，北为[帝汶海](../Page/帝汶海.md "wikilink")、[阿拉弗拉海以及](../Page/阿拉弗拉海.md "wikilink")[卡奔塔利亚湾](../Page/卡奔塔利亚湾.md "wikilink")，西壤[西澳大利亚](../Page/西澳大利亚.md "wikilink")，南接[南澳大利亚](../Page/南澳大利亚.md "wikilink")，东臨[昆士兰州](../Page/昆士兰州.md "wikilink")，面積约为1,420,968平方公-{里}-，约占澳大利亚大陆总面积的17.5%，海岸线长约6,200千米，總人口24萬餘（2007年）。首府[达尔文](../Page/达尔文_\(澳大利亚\).md "wikilink")，舊稱帕馬斯頓（Palmerston）；1911年改名為達爾文，是澳大利亚北部的優良港口之一，人口有145,916（2016）。

## 概況

達爾文的所在地區自然生態保護得很好；由於是澳大利亚北部的補給及船運中心，[二次大戰期間為軍事基地](../Page/二次大戰.md "wikilink")，因此在1942年間曾遭[日本猛烈的轟炸](../Page/日本.md "wikilink")，後經大規模重建，1959年-{升}-格為市。1990年代後，因北領地屬偏遠地區，澳大利亚政府為提升當地工商業和政府部門的營運績效，在此投下鉅額經費用於「知識經濟建設」，使得北領地的經濟型態進入一個嶄新的階段，目前[達爾文為澳洲最現代化的城市之一](../Page/達爾文_\(澳洲\).md "wikilink")。區內的主要城鎮還有[愛麗斯泉](../Page/愛麗斯泉.md "wikilink")（位於北領地之南的沙漠中，距南澳海岸1,500公-{里}-）。

## 簡史

北領地最初屬於[新南威尔士州](../Page/新南威尔士州.md "wikilink")；1863年劃為[南澳的一部分](../Page/南澳大利亚州.md "wikilink")；1911年轉由聯邦直轄。二次大戰期間，曾受日本轟炸，後來聯軍即以[達爾文為基地往北反攻](../Page/达尔文_\(澳大利亚\).md "wikilink")。1978年，北領地取得自治權。1998年北领地全民公决否决了升格為「州」的提议，以后此议题虽时有提起，但至今没有再度付诸公投。2015年澳大利亚政府会议（即联邦政府、各州政府及自治领地政府）一致同意谋求在2018年7月1日之前让北领地成为一州。

## 經濟

北領地人口稀少，屬炎熱地區，因此經濟以畜牧業、礦業、旅遊業和政府服務業為主。

## 外部链接

[Category:北領地](../Category/北領地.md "wikilink")
[Category:澳大利亚州份和领地](../Category/澳大利亚州份和领地.md "wikilink")