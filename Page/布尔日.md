**布尔日**（[法语](../Page/法语.md "wikilink")：****）是位于[法国中部](../Page/法国.md "wikilink")[中央-卢瓦尔河谷大区的一个](../Page/中央-卢瓦尔河谷大区.md "wikilink")[市镇](../Page/市镇_\(法国\).md "wikilink")，也是[谢尔省的首府](../Page/谢尔省.md "wikilink")。该市镇总面积平方公里，年时的人口为人。\[1\]

[布尔日大教堂于](../Page/布尔日大教堂.md "wikilink")1992年被[联合国教科文组织列入](../Page/联合国教科文组织.md "wikilink")[世界遗产](../Page/世界遗产.md "wikilink")。

## 气候

## 交通

[布尔日站每天开行前往巴黎](../Page/布尔日站.md "wikilink")、里昂、南特、图尔等地的列车。

## 名人

  - [路易十一](../Page/路易十一.md "wikilink")：法国[瓦卢瓦王朝国王](../Page/瓦卢瓦王朝.md "wikilink")

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/德国.md" title="wikilink">德国</a><a href="../Page/奥格斯堡.md" title="wikilink">奥格斯堡</a></p></li>
<li><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a><a href="../Page/阿威鲁.md" title="wikilink">阿威鲁</a></p></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><p><a href="../Page/意大利.md" title="wikilink">意大利</a><a href="../Page/弗利.md" title="wikilink">弗利</a></p></li>
<li><p><a href="../Page/波兰.md" title="wikilink">波兰</a><a href="../Page/科沙林.md" title="wikilink">科沙林</a></p></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li><p><a href="../Page/西班牙.md" title="wikilink">西班牙</a><a href="../Page/帕伦西亚.md" title="wikilink">帕伦西亚</a></p></li>
<li><p><a href="../Page/英国.md" title="wikilink">英国</a><a href="../Page/彼得伯勒.md" title="wikilink">彼得伯勒</a></p></li>
</ul></td>
</tr>
</tbody>
</table>

## 人口

布尔热（Bourges）人口变化图示
[Population_-_Municipality_code_18033.svg](https://zh.wikipedia.org/wiki/File:Population_-_Municipality_code_18033.svg "fig:Population_-_Municipality_code_18033.svg")

## 参见

  - [谢尔省市镇列表](../Page/谢尔省市镇列表.md "wikilink")

## 参考文献

## 外部链接

  - [布尔日官方网站](http://www.ville-bourges.fr)

[B](../Category/谢尔省市镇.md "wikilink")

1.  [法国INSEE人口数据-2009年](http://www.insee.fr/fr/ppp/bases-de-donnees/recensement/populations-legales/france-departements.asp?annee=2009)