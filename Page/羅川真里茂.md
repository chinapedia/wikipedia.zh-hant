**羅川真里茂**，[日本漫畫家](../Page/日本漫畫家.md "wikilink")，[青森縣](../Page/青森縣.md "wikilink")[八戶市出身](../Page/八戶市.md "wikilink")，女性，代表作是《[天才寶貝](../Page/天才寶貝.md "wikilink")》、《真白之音》。

## 經歷

1995年以《天才寶貝》獲得第40回[小學館漫畫賞少女向部門的獎項](../Page/小学馆漫画赏#第40回.md "wikilink")。

2012年以《[真白之音](../Page/真白之音.md "wikilink")》獲得[講談社漫畫賞少年向部門賞](../Page/講談社漫畫賞.md "wikilink")。
\[1\]

## 作品

  - 漫畫

<table>
<thead>
<tr class="header">
<th><p>中文書名</p></th>
<th><p>日文書名</p></th>
<th><p>冊數</p></th>
<th><p>漫畫類型</p></th>
<th><p>連載雜誌</p></th>
<th><p>連載期間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/天才寶貝.md" title="wikilink">天才寶貝</a>（大然版）<br />
家有天才寶貝（尖端版）</p></td>
<td></td>
<td><p>全18冊</p></td>
<td><p>家族+校園</p></td>
<td><p><a href="../Page/花與夢.md" title="wikilink">花與夢</a></p></td>
<td><p>1992年<br />
～1997年</p></td>
<td><p>有改編動畫</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛情心晴天.md" title="wikilink">愛情心晴天</a>（大然版）<br />
<a href="../Page/總是晴朗好心情.md" title="wikilink">總是晴朗好心情</a>（長鴻版）</p></td>
<td></td>
<td><p>6冊未完</p></td>
<td><p>校園</p></td>
<td><p>花與夢</p></td>
<td><p>1994年～</p></td>
<td><p>|大然版只到2冊，長鴻版5冊</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/紐約·紐約.md" title="wikilink">紐約·紐約</a>（大然版）</p></td>
<td></td>
<td><p>全4冊</p></td>
<td><p><a href="../Page/BL_(和製英語).md" title="wikilink">BL</a></p></td>
<td><p><a href="../Page/JETS_COMICS.md" title="wikilink">JETS COMICS</a></p></td>
<td><p>1998年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/藍天下的網球場.md" title="wikilink">藍天下的網球場</a>(玉皇朝出版)<br />
<a href="../Page/百分百網球.md" title="wikilink">百分百網球</a>（長鴻版）<br />
<a href="../Page/為愛向前衝.md" title="wikilink">為愛向前衝</a>（大然版）</p></td>
<td></td>
<td><p>全32冊</p></td>
<td><p>高校網球</p></td>
<td><p>花與夢</p></td>
<td><p>1998年<br />
～2009年</p></td>
<td><p>大然版只到14冊</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東京少年物語.md" title="wikilink">東京少年物語</a>（東立版）</p></td>
<td><p>東京少年物語</p></td>
<td><p>全1冊</p></td>
<td></td>
<td><p>花與夢</p></td>
<td><p>2002年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/迎向晨曦的每一天.md" title="wikilink">迎向晨曦的每一天</a>（尖端版）</p></td>
<td></td>
<td><p>全1冊</p></td>
<td></td>
<td><p>花與夢</p></td>
<td><p>2010年</p></td>
<td><p>短篇集</p></td>
</tr>
<tr class="odd">
<td><p>藥師波特(東立版)</p></td>
<td></td>
<td><p>全1冊</p></td>
<td><p>異世界</p></td>
<td></td>
<td><p>2010年</p></td>
<td><p>全一冊</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/真白之音.md" title="wikilink">真白之音</a>(東立版)</p></td>
<td></td>
<td><p>21冊待續</p></td>
<td><p>日本古樂</p></td>
<td><p><a href="../Page/月刊少年Magazine.md" title="wikilink">月刊少年Magazine</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 畫冊

<!-- end list -->

  -
  -
## 外部連結

  - [PariPariMarimosenbei](http://www.ragawa.co.jp/) - 作者的官方網站

  -
  -
  -
  -
## 參考來源

[Category:日本女性漫画家](../Category/日本女性漫画家.md "wikilink")
[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:青森縣出身人物](../Category/青森縣出身人物.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")

1.  [歴代の講談社漫画賞](http://www.mangaspider.net/)