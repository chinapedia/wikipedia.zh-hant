**gretl**（意為*[Gnu](../Page/GNU通用公共许可证.md "wikilink")* *Regression*,
*Econometrics* and *Time-series*
*Library*，以此[縮寫作為軟體名](../Page/縮寫.md "wikilink")）是一种可以编纂和解析[计量经济学数据的](../Page/计量经济学.md "wikilink")[开放源代码](../Page/开放源代码.md "wikilink")[软件](../Page/软件.md "wikilink")。它可以和[X-12-ARMA](../Page/X-12-ARMA.md "wikilink")、[TRAMO](../Page/TRAMO.md "wikilink")／[SEATS和](../Page/SEATS.md "wikilink")[R语言一起使用](../Page/R语言.md "wikilink")。gretl用[C语言写成](../Page/C语言.md "wikilink")，使用[gnuplot制图](../Page/gnuplot.md "wikilink")。

gretl自身的文件格式是[XML](../Page/XML.md "wikilink")，但它还可输入[Excel](../Page/Excel.md "wikilink")、[Gnumeric](../Page/Gnumeric.md "wikilink")、[Stata](../Page/Stata.md "wikilink")、[EViews](../Page/EViews.md "wikilink")、[RATS](../Page/RATS.md "wikilink")、[GNU
Octave](../Page/GNU_Octave.md "wikilink")、[Comma Separated
Values](../Page/Csv.md "wikilink")、[PcGive](../Page/PcGive.md "wikilink")、[JMulTi](../Page/JMulTi.md "wikilink")、[SPSS和](../Page/SPSS.md "wikilink")[ASCII文件](../Page/ASCII.md "wikilink")，也可以输出到[GNU
Octave](../Page/GNU_Octave.md "wikilink")、[GNU
R](../Page/R语言.md "wikilink")、[JMulTi和](../Page/JMulTi.md "wikilink")[PcGive文件格式](../Page/PcGive.md "wikilink")。

## 另见

  - [JMulTi](../Page/JMulTi.md "wikilink")

## 外部链接

  - [Gretl](http://gretl.sourceforge.net/)
  - [gretl project page at
    SourceForge](http://sourceforge.net/projects/gretl/)
  - [Gretl使用者指南](http://ricardo.ecn.wfu.edu/pub//gretl/manual/en/gretl-guide.pdf)（PDF）
  - [Gretl指令参照](http://ricardo.ecn.wfu.edu/pub//gretl/manual/en/gretl-ref.pdf)（PDF）

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:统计软件](../Category/统计软件.md "wikilink")
[Category:时间序列](../Category/时间序列.md "wikilink")
[Category:自由圖表軟件](../Category/自由圖表軟件.md "wikilink")