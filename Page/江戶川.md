[Kanamachi-water_purification_plant-Edo_river.JPG](https://zh.wikipedia.org/wiki/File:Kanamachi-water_purification_plant-Edo_river.JPG "fig:Kanamachi-water_purification_plant-Edo_river.JPG")
**江戶川**（）是[日本一條河流](../Page/日本.md "wikilink")，全長59.5[公里](../Page/公里.md "wikilink")（到舊江戶川河口）、[流域面積約](../Page/流域.md "wikilink")200km²，覆蓋[茨城縣](../Page/茨城縣.md "wikilink")、[千葉縣](../Page/千葉縣.md "wikilink")、[埼玉縣](../Page/埼玉縣.md "wikilink")、[東京都](../Page/東京都.md "wikilink")。

## 地理

發源於[茨城縣五霞町](../Page/茨城縣.md "wikilink")、千葉縣野田市的關宿分基點，向南流經[茨城縣](../Page/茨城縣.md "wikilink")、[千葉縣](../Page/千葉縣.md "wikilink")、[埼玉縣](../Page/埼玉縣.md "wikilink")、[東京都](../Page/東京都.md "wikilink")，在千葉縣[市川市附近分為](../Page/市川市.md "wikilink")「江戶川放水路」和原流路的[舊江戶川](../Page/舊江戶川.md "wikilink")。「江戶川放水路」從行德可動堰（江戶川河口堰）開始，從市川市、[船橋市方面的河口開始](../Page/船橋市.md "wikilink")；舊江戶川在與[新中川合併後](../Page/新中川.md "wikilink")，從[浦安市](../Page/浦安市.md "wikilink")、東京都[江戶川區方面的河口分別注入](../Page/江戶川區.md "wikilink")[東京灣](../Page/東京灣.md "wikilink")。

另外，舊江戶川河口的堀江量水標零點稱為Y.P.（即Yedogawa
Peil之略），是測量[利根川](../Page/利根川.md "wikilink")、江戶川、[霞浦](../Page/霞浦_\(日本\).md "wikilink")、[那珂川等水位的基準面](../Page/那珂川.md "wikilink")。Y.P.0m＝T.P.-0.840m（T.P即Tokyo
Peil，東京灣平均潮位的零點，是日本國內測量山峰標高、河川等的水位的基準面）。

## 作用

從利根川流入江戶川的河水與「北千葉導水路」合併，成為日本首都圈上水道的重要來源。但是由於江戶川人口相對較多，水質不是很好，設有[淨水場進行高度處理](../Page/淨水場.md "wikilink")。

## 歷史

歷史上從利根川，一直是從日本[東北地方](../Page/東北地方.md "wikilink")、[關東北部運送物資的流通幹線](../Page/關東.md "wikilink")，直到[明治時代被鐵道取代](../Page/明治時代.md "wikilink")。

江戶川原來是[渡良瀨川的下流的一部分](../Page/渡良瀨川.md "wikilink")，經過1641年人工挖掘，連通利根川後直通[東京灣](../Page/東京灣.md "wikilink")。之後在1919年開鑿「江戶川放水路」新成了兩個入海口的局面。

## 主要支流

  - 座生川
  - 利根運河
  - 今上落
  - 坂川
  - 真間川

## 主要水利設施

  - 關宿水門
  - [首都圈外郭放水路](../Page/首都圈外郭放水路.md "wikilink")
  - 北千葉導水路
  - 三鄉放水路
  - 江戶川水門
  - 行德可動堰（江戶川河口堰）

## 主要橋樑

  - 市川大橋
  - 新行德橋
  - 行德橋
  - 江戶川大橋
  - 市川橋
  - 新葛飾橋
  - 葛飾大橋
  - 葛飾橋
  - 上葛飾橋
  - 流山橋
  - 玉葉橋
  - 野田橋
  - 金野井大橋
  - 寶珠花橋
  - 關宿橋

### 舊江戶川

舞濱大橋、浦安橋、今井橋

## 參見

  - [中川](../Page/中川.md "wikilink")
  - [綾瀨川](../Page/綾瀨川.md "wikilink")

## 外部链接

  - [日本國土交通省關東地方整備局江戶川河川事務所](http://www.ktr.mlit.go.jp/edogawa/)

[Category:東京都河川](../Category/東京都河川.md "wikilink")
[Category:千葉縣河川](../Category/千葉縣河川.md "wikilink")
[Category:埼玉縣河川](../Category/埼玉縣河川.md "wikilink")
[Category:茨城縣河川](../Category/茨城縣河川.md "wikilink")