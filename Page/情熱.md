**情熱**（****）是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")12張[單曲](../Page/單曲.md "wikilink")。於2001年5月23日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

上兩張單曲『[夏之王者／除了你誰都不愛](../Page/夏之王者/除了你誰都不愛.md "wikilink")』與『[在我背上的翅膀](../Page/在我背上的翅膀.md "wikilink")』的發售時間相隔了半年以上，但本單曲與上一張單曲的發售時間卻只相隔了短短三個月而已。上一張單曲是由[堂本剛主演的](../Page/堂本剛.md "wikilink")[連續劇主題曲](../Page/連續劇.md "wikilink")，而本單曲則是由[堂本光一主演的連續劇主題曲](../Page/堂本光一.md "wikilink")，而恰巧下一張單曲將會再次是堂本剛主演的連續劇主題曲。

在[Oricon的總銷量只錄得](../Page/Oricon.md "wikilink")59萬張，比起上一張大幅回落。但這也是[CD市場本身在銷量上縮小伴隨之下的結果](../Page/CD.md "wikilink")，所以連帶KinKi
Kids的唱片銷量亦隨之而降。當然，從上張單曲開始二人改變路線，亦對銷量上有相當的影響。而且恰巧的是，在Oricon銷量榜上已經是連續三張單曲，在發售前一週均是由[濱崎步佔據第一位](../Page/濱崎步.md "wikilink")。

在堂本光一主演的連續劇『Rookie\!』（台譯：菜鳥刑警）大結局中，剛客串飾演一名高買CD的疑犯與光一有一面之緣的場面。那個時候被偷去的那張CD正是本單曲『情熱』。

『情熱』是由一名[克羅地亞人Boris](../Page/克羅地亞.md "wikilink") Dudevic作曲。在同年發售的『[E
album](../Page/E_album.md "wikilink")』除收錄原裝版本之外，更另外收錄了一個抒情版本。

## 名稱

  - 日語原名：****
  - 中文意思：**熱情**
  - [香港](../Page/香港.md "wikilink")[正東譯名](../Page/正東唱片.md "wikilink")：**熱情**
  - [台灣](../Page/台灣.md "wikilink")[艾迴譯名](../Page/艾迴唱片.md "wikilink")：**情熱**

## 收錄歌曲

1.  **熱情**（****）
      - ※堂本光一參與演出的[富士電視台](../Page/富士電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『Rookie\!』主題曲。
      - 作曲：[Boris Dudevic](../Page/Boris_Dudevic.md "wikilink")
      - 作詞：[Satomi](../Page/Satomi.md "wikilink")
      - 編曲：[松本良喜](../Page/松本良喜.md "wikilink")
2.  **為你而唱**（****）
      - 作曲、作詞：[周水](../Page/周水.md "wikilink")
      - 編曲：[南利一](../Page/南利一.md "wikilink")
3.  **熱情 (Instrumental)**
      - 作曲：Boris Dudevic
      - 編曲：松本良喜
4.  **為你而唱 (Instrumental)**
      - 作曲：周水
      - 編曲：南利一

[Category:近畿小子歌曲](../Category/近畿小子歌曲.md "wikilink")
[Category:富士火十劇主題曲](../Category/富士火十劇主題曲.md "wikilink")
[Category:2001年單曲](../Category/2001年單曲.md "wikilink")
[Category:2001年Oricon單曲月榜冠軍作品](../Category/2001年Oricon單曲月榜冠軍作品.md "wikilink")
[Category:2001年Oricon單曲週榜冠軍作品](../Category/2001年Oricon單曲週榜冠軍作品.md "wikilink")