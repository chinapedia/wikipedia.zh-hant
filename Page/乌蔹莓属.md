**乌蔹莓属**（[學名](../Page/學名.md "wikilink")：*Cayratia*）為[葡萄科的一個](../Page/葡萄科.md "wikilink")[屬](../Page/屬.md "wikilink")，其下約有45種物種，分布於亞洲、非洲、澳洲與太平洋島嶼的[熱帶與](../Page/熱帶.md "wikilink")[亞熱帶地區](../Page/亞熱帶.md "wikilink")\[1\]。[分子系統發生學研究顯示在葡萄科中](../Page/分子系統發生學.md "wikilink")，乌蔹莓属與[葡萄瓮属和](../Page/葡萄瓮属.md "wikilink")[崖爬藤屬的親緣關係最接近](../Page/崖爬藤屬.md "wikilink")，不過乌蔹莓属有可能不是[單系群](../Page/單系群.md "wikilink")\[2\]。

## 物種

[Cayratia_formosana_(WilsonKao)_002.jpg](https://zh.wikipedia.org/wiki/File:Cayratia_formosana_\(WilsonKao\)_002.jpg "fig:Cayratia_formosana_(WilsonKao)_002.jpg")
[Cayratia_trifolia_leaves_and_flower_buds.jpg](https://zh.wikipedia.org/wiki/File:Cayratia_trifolia_leaves_and_flower_buds.jpg "fig:Cayratia_trifolia_leaves_and_flower_buds.jpg")

  - *[C. acris](../Page/Cayratia_acris.md "wikilink")*
  - *[C. clematidea](../Page/Cayratia_clematidea.md "wikilink")*
  - *[C. debilis](../Page/Cayratia_debilis.md "wikilink")*
  - *[C. delicatula](../Page/Cayratia_delicatula.md "wikilink")*
  - *[C. eurynema](../Page/Cayratia_eurynema.md "wikilink")*
  - [膝曲烏蘞莓](../Page/膝曲烏蘞莓.md "wikilink") *C. geniculata*
  - *[C. gracilis](../Page/Cayratia_gracilis.md "wikilink")*
  - *[C. ibuensis](../Page/Cayratia_ibuensis.md "wikilink")*
  - [烏蘞苺](../Page/烏蘞苺.md "wikilink") *C. japonica*
  - [墨脫烏蘞莓](../Page/墨脫烏蘞莓.md "wikilink") *C. medogensis*
  - [勐臘烏蘞莓](../Page/勐臘烏蘞莓.md "wikilink") *C. menglaensis*
  - *[C. mollissima](../Page/Cayratia_mollissima.md "wikilink")*
  - [華中烏蘞苺](../Page/華中烏蘞苺.md "wikilink") *C. oligocarpa*
  - [台灣烏蘝莓](../Page/台灣烏蘝莓.md "wikilink") *C. formosana*
  - [三葉烏蘞莓](../Page/三葉烏蘞莓.md "wikilink") *C. trifolia*

## 參考資料

[\*](../Category/乌蔹莓属.md "wikilink")

1.
2.