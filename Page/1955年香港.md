## 大事記

  - [1月20日](../Page/1月20日.md "wikilink")，[塔門發生](../Page/塔門.md "wikilink")[大火](../Page/塔門大火.md "wikilink")，焚斃4人。\[1\]
  - [4月11日](../Page/4月11日.md "wikilink")，一架從北京經香港飛往印尼耶加達的民航包機*喀什米爾公主號*在香港啟德機場停留期間，被中國國民黨特務成功潛上飛機放置定時炸彈。飛機在接近印尼海岸時[爆炸](../Page/克什米爾公主號事件.md "wikilink")，機上除3名機員生還外，11名乘客及5名機組人員罹難。
  - [4月14日](../Page/4月14日.md "wikilink")，一日內發生兩宗慘劇，凌晨一時許，[士丹頓街發生](../Page/士丹頓街.md "wikilink")[塌屋事件](../Page/士丹頓街塌屋事故.md "wikilink")，6死27傷。同日下午4時45分，[李鄭屋村一間藤器店](../Page/李鄭屋村.md "wikilink")[發生大火](../Page/李鄭屋村藤器店大火.md "wikilink")，7死1傷。
  - [6月28日](../Page/6月28日.md "wikilink")，10人在一間印刷公司飲用豬腳湯後[嚴重中毒](../Page/五經堂印刷所中毒事件.md "wikilink")，9死1傷。
  - [8月1日](../Page/8月1日.md "wikilink")，[九龍仔西綏巷](../Page/九龍仔.md "wikilink")89號木屋發生雙屍情殺案，一對男女熟睡中被斬死。\[2\]

[Lcutomb_1955.jpg](https://zh.wikipedia.org/wiki/File:Lcutomb_1955.jpg "fig:Lcutomb_1955.jpg")

  - [8月9日](../Page/8月9日.md "wikilink")，政府在李鄭屋村興建徙置大廈時，無意發現了一個[東漢](../Page/東漢.md "wikilink")[墓穴](../Page/墓穴.md "wikilink")，被稱為[李鄭屋漢墓](../Page/李鄭屋漢墓.md "wikilink")。
  - [8月27日](../Page/8月27日.md "wikilink")，[元朗](../Page/元朗.md "wikilink")[屏山](../Page/屏山.md "wikilink")[唐人新村](../Page/唐人新村.md "wikilink")27號李苑發生劫殺案，抗日國軍名將[余程萬被殺](../Page/余程萬.md "wikilink")。
  - [8月28日](../Page/8月28日.md "wikilink")，發生[嚴重的山洪暴發事件](../Page/大埔滘山洪暴發事件.md "wikilink")，一間小學師生及[九廣鐵路的員工親屬到](../Page/九廣鐵路.md "wikilink")[大埔滘](../Page/大埔滘.md "wikilink")[松仔園遊玩期間遇上大雨](../Page/松仔園.md "wikilink")，山洪暴漲沖走多人，最後證實共28人喪生。
  - [10月11日](../Page/10月11日.md "wikilink")，[深水埗](../Page/深水埗.md "wikilink")[青山道](../Page/青山道.md "wikilink")473號益豐搪瓷廠1名工會主席被左派工人殺死。\[3\]
  - [11月1日](../Page/11月1日.md "wikilink")，[九龍](../Page/九龍.md "wikilink")[花墟村木屋區大火](../Page/花墟村木屋區大火.md "wikilink")，5死20餘傷，災民達6,810人。
  - [11月4日](../Page/11月4日.md "wikilink")，[新填地街舊樓大火](../Page/新填地街舊樓大火.md "wikilink")，3人燒死。
  - [11月12日](../Page/11月12日.md "wikilink")，[新界](../Page/新界.md "wikilink")[九龍坑發生](../Page/九龍坑.md "wikilink")[九廣鐵路火車撞英軍](../Page/九廣鐵路.md "wikilink")[彗星坦克車的](../Page/彗星坦克.md "wikilink")[罕見意外](../Page/九龍坑火車坦克相撞事故.md "wikilink")，4死20傷。<REF>香港年鑑
    第九回 (1955年) 火車坦克相撞慘劇

新界九坑地方。十一月十二日發生撞車慘劇，當時有火車一列經過鐵路公路交叉點，時有英兵駕彗星坦克亦至其地，不知如何，竟爾相撞，結果死四傷二十，車守亦於是役殉職！圖為肇事後情形，彗星坦克緊貼火車之旁，而火車之客廂則互相套住。</REF>

  - [12月17日](../Page/12月17日.md "wikilink")，[筲箕灣木樓](../Page/筲箕灣.md "wikilink")[發生大火](../Page/筲箕灣木樓大火.md "wikilink")，8死3傷。\[4\]

## 出生人物

  - 3月18日—[陳茂波](../Page/陳茂波.md "wikilink")，香港執業會計師，香港特別行政區立法會議員及第5任[財政司司長](../Page/香港特別行政區財政司司長.md "wikilink")。
  - 8月17日—李志源神父
  - 11月24日—[林瑞麟](../Page/林瑞麟.md "wikilink")，香港高級公務員，香港特別行政區第5任[政務司司長](../Page/香港特別行政區政務司司長.md "wikilink")。

## 逝世人物

  - 3月22日—[劉德譜](../Page/劉德譜.md "wikilink")，[油麻地小輪創辦人之一](../Page/油麻地小輪.md "wikilink")。
  - 4月13日—[高可寧](../Page/高可寧.md "wikilink")，港澳著名巨商。
  - 8月27日—[余程萬](../Page/余程萬.md "wikilink")，抗日國軍名將。

## 参考文献

  - 陳昕、郭志坤：《香港全紀錄》，卷二，香港中華書店，1998。 ISBN 978-962-231-891-5
  - 湯開建、蕭國健、陳佳榮：《香港6000年》，麒麟書業有限公司，1998。 ISBN 978-962-232-126-6

[Category:1955年亚洲](../Category/1955年亚洲.md "wikilink")
[1955年香港](../Category/1955年香港.md "wikilink")
[Category:20世紀各年香港](../Category/20世紀各年香港.md "wikilink")
[Category:1955年英国](../Category/1955年英国.md "wikilink")

1.
2.  香港工商日報, 1955-08-02 第5頁
3.  香港工商日報, 1955-10-12 第5頁
4.