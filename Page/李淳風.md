[张邱建算经.jpg](https://zh.wikipedia.org/wiki/File:张邱建算经.jpg "fig:张邱建算经.jpg")》\]\]

**李淳風**（），[中国](../Page/中国.md "wikilink")[唐朝初年](../Page/唐朝.md "wikilink")[岐州](../Page/岐州.md "wikilink")[雍人](../Page/雍县.md "wikilink")（今[陝西省](../Page/陝西省.md "wikilink")[凤翔县](../Page/凤翔县.md "wikilink")），唐朝[政治人物](../Page/政治人物.md "wikilink")、[天文學家](../Page/天文學家.md "wikilink")、和[數學家](../Page/數學家.md "wikilink")\[1\]。大六壬法，文王卦

## 生平

父親李播是[隋時的](../Page/隋朝.md "wikilink")[高唐](../Page/高唐.md "wikilink")[縣尉](../Page/縣尉.md "wikilink")，後放棄官職為[道士](../Page/道士.md "wikilink")，[道号黃冠子](../Page/道号.md "wikilink")\[2\]，著有《[天文大象賦](../Page/天文大象賦.md "wikilink")》。

李淳風早年學道於[天台山](../Page/天台山_\(浙江\).md "wikilink")，通曉天文星象，最早是[隋煬帝的司監官](../Page/隋煬帝.md "wikilink")。

[劉餗的](../Page/劉餗.md "wikilink")《[隋唐嘉話](../Page/隋唐嘉話.md "wikilink")》記載淳风能預測[日蝕](../Page/日蝕.md "wikilink")，唐太宗頗不信其言，李淳風说：“有如不蝕，則臣請死之。”一日唐太宗等不至日蝕，於是對李淳風說：“我放你回家，和老婆孩子告別。”李淳风在墙上-{zh:划;
zh-hans:划;
zh-hant:劃}-了一条标记说：“尚早一刻。等到日光照到这里时，日食就出现了。”後來果“如言而蝕，不差毫髮”。一日李淳風與[张文收同皇帝出遊](../Page/张文收.md "wikilink")，有狂風從南面迎來，李淳風認為在南面五-{里}-處一定有人在哭，張文收則認為那裡有音樂聲。後來果然有一送葬儀隊經過。\[3\]

[貞觀十五年](../Page/貞觀_\(唐太宗\).md "wikilink")（641年）任太史丞，撰《[法象志](../Page/法象志.md "wikilink")》\[4\]，貞觀二十二年（648年）任[太史令](../Page/太史令.md "wikilink")，奉詔註釋《[算經十書](../Page/算經十書.md "wikilink")》。一日，唐太宗得到一本秘讖，上面說：「唐中弱，有女武代王。」，李淳風預知[武后將稱帝且改變之會造成不能預計的後果](../Page/武則天.md "wikilink")\[5\]。[唐高宗時](../Page/唐高宗.md "wikilink")，李淳風以[劉焯的](../Page/劉焯.md "wikilink")《[皇極曆](../Page/皇極曆.md "wikilink")》為據，編成《[麟德曆](../Page/麟德曆.md "wikilink")》。李淳風还编撰有《[推背圖](../Page/推背圖.md "wikilink")》、《[典章文物志](../Page/典章文物志.md "wikilink")》、《[秘阁录](../Page/秘阁录.md "wikilink")》、《[乙巳占](../Page/乙巳占.md "wikilink")》等书。《舊唐書》、《新唐書》皆記載李淳風曾研制「三重環」[渾天儀](../Page/渾天儀.md "wikilink")。

指出《[戊寅元曆](../Page/戊寅元曆.md "wikilink")》的錯誤，麟德二年（665年），改用李淳风的《[麟德曆](../Page/麟德曆.md "wikilink")》\[6\]。

傳說李淳風就是六壬仙師的化身， 在唐朝輔助李世民成為真龍天子， 貞觀盛世讓大唐昌盛。 更與袁天罡一起 ，寫下推背圖
，預言了幾千年後所發生的國家大事現在所有歷史學家根玄學家還在研究中。

## 参考文献

## 參考書目

  - 《太平廣記》卷第七十六方士一

唐朝议大夫行太史令上轻车都尉

[Category:隋朝中央官员](../Category/隋朝中央官员.md "wikilink")
[Category:唐朝朝议大夫](../Category/唐朝朝议大夫.md "wikilink")
[Category:唐朝太史令](../Category/唐朝太史令.md "wikilink")
[Category:唐朝天文学家](../Category/唐朝天文学家.md "wikilink")
[Category:唐朝數學家](../Category/唐朝數學家.md "wikilink")
[Category:鳳翔人](../Category/鳳翔人.md "wikilink")
[C](../Category/李姓.md "wikilink")
[Category:命理師](../Category/命理師.md "wikilink")

1.  請參照[周髀算經](../Page/周髀算經.md "wikilink")，該人作注釋於此書
2.  《[新唐書](../Page/新唐書.md "wikilink")·列傳第一百二十九方技》：“李淳風，[岐州雍人](../Page/岐州.md "wikilink")。父播，仕隋高唐尉，棄官為道士，號黃冠子，以論撰自見”
3.  《[太平廣記](../Page/太平廣記.md "wikilink")》卷第七十六方士一：“李以为南五里当有哭者，张以为有音乐。左右驰马观之，则遇送葬者。”[刘餗](../Page/刘餗.md "wikilink")《隋唐嘉话》卷中：“李太史与张文收率更坐，有暴风自南而至，李以南五里当有哭者，张以为有音乐。左右驰马观之，则遇送葬者，有鼓吹焉。”
4.  《新唐书.天文志》载：“贞观中，淳风撰《法象志》，因《汉书》十二次度数，始以唐之州县配焉。而一行以为天下山河之象存乎两戒。”
5.  《[新唐書·列傳第一百九十五·方技](../Page/:s:新唐書/卷204.md "wikilink")》
6.  《[舊唐書·曆志一](../Page/:s:舊唐書/卷32.md "wikilink")》：“
    高宗时，太史奏旧历加时寖差，宜有改定。乃詔李淳风造《麟德历》。”[谢肇淛](../Page/谢肇淛.md "wikilink")《[五杂俎](../Page/五杂俎.md "wikilink")·天部二》：“
    李淳风最精占候，其造《麟德历》，自谓应洛下閎后八百年之语，似极精且密矣，然至开元二年，仅四十年，而纬晷渐差，不亦近儿戏乎？”