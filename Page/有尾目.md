**有尾目**（[學名](../Page/學名.md "wikilink")：**）是终身有尾的[兩棲動物](../Page/兩棲綱.md "wikilink")，一共有9[科](../Page/科_\(生物\).md "wikilink")60[屬约](../Page/屬_\(生物\).md "wikilink")358[種](../Page/種_\(生物\).md "wikilink")，幼體與成體形態上差別不大，主要包括[蠑螈](../Page/蠑螈科.md "wikilink")、[小鯢和](../Page/小鯢.md "wikilink")[大鯢](../Page/大鯢.md "wikilink")。

有尾目动物有發展完全的[前肢和](../Page/前肢.md "wikilink")[後肢](../Page/後肢.md "wikilink")，大小大約一致，但四肢细弱，少数种类仅有前肢（[鳗螈](../Page/鳗螈.md "wikilink")）。眼小，或隐于皮下（[洞螈](../Page/洞螈.md "wikilink")），沒有[鼓膜或](../Page/鼓膜.md "wikilink")[外耳開口](../Page/外耳.md "wikilink")。牙齒位於[下頜](../Page/下頜.md "wikilink")。身體有黏膜[皮膚](../Page/皮膚.md "wikilink")，沒有[鱗片或尖銳的](../Page/鱗片.md "wikilink")[爪子](../Page/爪子.md "wikilink")。通常行[體內受精](../Page/體內受精.md "wikilink")。

## 分類

有尾目包括：

  - [隱鰓鯢亞目](../Page/隱鰓鯢亞目.md "wikilink") Cryptobranchoidea
      - [隐鰓鯢科](../Page/隐鰓鯢科.md "wikilink") Cryptobranchidae
      - [小鯢科](../Page/小鯢科.md "wikilink") Hynobiidae
  - [蠑螈亞目](../Page/蠑螈亞目.md "wikilink") Salamandroidea
      - [鈍口螈科](../Page/鈍口螈科.md "wikilink") Ambystomatidae
      - [兩棲鯢科](../Page/兩棲鯢科.md "wikilink") Amphiumidae
      - [陸巨螈科](../Page/陸巨螈科.md "wikilink") Dicamptodontidae
      - [無肺螈科](../Page/無肺螈科.md "wikilink") Plethodontidae
      - [洞螈科](../Page/洞螈科.md "wikilink") Proteidae
      - [急流螈科](../Page/急流螈科.md "wikilink") Rhyacotritonidae
      - [蠑螈科](../Page/蠑螈科.md "wikilink") Salamandridae
  - [鰻螈亞目](../Page/鰻螈亞目.md "wikilink") Sirenoidea
      - [鰻螈科](../Page/鰻螈科.md "wikilink") Sirenidae

## 參見

[\*](../Category/有尾目.md "wikilink")