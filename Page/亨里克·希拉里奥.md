**亨里克·希拉里奥**（****，一般被稱為**希拉里奧**，）乃一名已經退役的葡萄牙足球守門員。他之前曾效力多家葡萄牙球會，包括班霸級的[波圖](../Page/波圖足球俱樂部.md "wikilink")。2006年7月1日他加盟[車路士](../Page/車路士.md "wikilink")，為球會第三號門將，並效力至2014年退役，現時在[車路士擔任助理守門員教練](../Page/車路士.md "wikilink")。

2009年11月，希拉里奥被[葡萄牙國家隊徵召入伍](../Page/葡萄牙國家足球隊.md "wikilink")，並入選[2010年世界盃外圍賽對](../Page/2010年世界盃外圍賽.md "wikilink")[波斯尼亞的陣容中](../Page/波斯尼亞黑塞哥維那國家足球隊.md "wikilink")，但未有上陣。翌年3月，他在對[中國的友誼場中下半場後備上陣](../Page/中國國家足球隊.md "wikilink")，是為他於國家隊中的首發賽事。

## 榮譽

### 車路士

  - [英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")：2007年、2009年
  - [英格蘭聯賽杯](../Page/英格蘭聯賽杯.md "wikilink")：2007年

[Category:葡萄牙足球运动员](../Category/葡萄牙足球运动员.md "wikilink")
[Category:波圖球員](../Category/波圖球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:拿華球員](../Category/拿華球員.md "wikilink")
[Category:科英布拉大學球員](../Category/科英布拉大學球員.md "wikilink")
[Category:艾馬多拉球員](../Category/艾馬多拉球員.md "wikilink")
[Category:國民隊球員](../Category/國民隊球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")