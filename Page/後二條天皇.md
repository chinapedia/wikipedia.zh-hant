**後二条天皇**（；[弘安](../Page/弘安.md "wikilink")8年[二月初二](../Page/二月初二.md "wikilink")（1285年3月9日）
-
[德治](../Page/德治.md "wikilink")3年[八月二十五日](../Page/八月二十五日.md "wikilink")（1308年9月10日）），為日本[鎌倉時代的第](../Page/鎌倉時代.md "wikilink")94代[天皇](../Page/天皇.md "wikilink")（在位：[正安](../Page/正安.md "wikilink")3年[一月二十二日](../Page/一月二十二日.md "wikilink")（1301年3月3日）
-
[德治](../Page/德治.md "wikilink")3年[八月二十五日](../Page/八月二十五日.md "wikilink")（1308年9月10日））。諱**邦治**（）。

## 系譜

[大覺寺統的](../Page/大覺寺統.md "wikilink")[後宇多天皇第一皇子](../Page/後宇多天皇.md "wikilink")。母[源基子（西華門院）](../Page/堀川基子.md "wikilink")。
1286年親王宣下，1296年被[持明院統的](../Page/持明院統.md "wikilink")[後伏見天皇冊立為](../Page/後伏見天皇.md "wikilink")[皇太子](../Page/皇太子.md "wikilink")。

  -
    主要的后妃與皇子女：

<!-- end list -->

  - 中宮：德大寺（藤原）忻子（1283-1352）
  - 宮人：五辻（藤原）宗子
      - 第一皇子：[邦良親王](../Page/邦良親王.md "wikilink")（1300-1326）（[後醍醐天皇](../Page/後醍醐天皇.md "wikilink")[皇太子](../Page/皇太子.md "wikilink")）
      - 第二皇子：邦省親王（1302-1375）
  - 宮人：三条（藤原）公泰之女
      - 第三皇子：祐助法親王（1302-1359）
      - 第四皇子：聖尊法親王（1303-1370）
  - 宮人：三条（藤原）公親之女
      - 第五皇子：尊濟法親王（1304-1329）
  - 宮人：平棟俊之女
      - 第一皇女：女便（女＋便）子內親王（1302-1362）

## 略歴

1301年[一月二十二日](../Page/一月二十二日.md "wikilink")，因[後伏見天皇的讓位而登基為天皇](../Page/後伏見天皇.md "wikilink")。

1308年[八月二十五日](../Page/八月二十五日.md "wikilink")，因急病而崩御。得年24歲。

在位期間由其父[後宇多上皇實行](../Page/後宇多天皇.md "wikilink")[院政](../Page/院政.md "wikilink")。

[Category:1301年出生](../Category/1301年出生.md "wikilink")
[Category:1308年逝世](../Category/1308年逝世.md "wikilink")
[Category:鎌倉時代天皇](../Category/鎌倉時代天皇.md "wikilink")