**虞姓**是[漢姓之一](../Page/漢姓.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》排名第161位。

## 來源

虞姓主要來源有二：

  - 源于[妫姓](../Page/妫姓.md "wikilink")，[虞舜把天下](../Page/虞舜.md "wikilink")[禅让給](../Page/禅让.md "wikilink")[夏禹](../Page/夏禹.md "wikilink")，夏禹將[虞城這一片地區分封給虞舜的儿子](../Page/虞城.md "wikilink")[商均](../Page/商均.md "wikilink")，成立[虞国](../Page/虞国_\(夏\).md "wikilink")。商均後代不久後以“以国为氏”而姓虞。
  - 源于[姬姓](../Page/姬姓.md "wikilink")，[周太王次子](../Page/周太王.md "wikilink")[仲雍的后裔](../Page/仲雍.md "wikilink")[虞仲被封于现](../Page/虞仲.md "wikilink")[山西省](../Page/山西省.md "wikilink")[平陆县的东北](../Page/平陆县.md "wikilink")，成立[虞国](../Page/虞国.md "wikilink")，其後代也“以国为氏”。

## 郡望

  - 濟陽郡：晉惠帝的時候將陳留郡的一部分劃出來，設置了濟陽郡。在今天的河南省蘭考縣一帶。
  - [會稽郡](../Page/會稽虞氏.md "wikilink")：秦朝的時候設置。在今天的江蘇省東南部以及浙江省活上西部一帶。治所在吳縣，也就是今天的江蘇省蘇州市。
  - 陳留郡：秦代設置陳留縣，漢代改設陳留郡。在今天的河南省開封地區。

## 堂號

  - 五絕堂：源自唐太宗曾經誇讚虞世南：「德行絕好，忠直絕好，博學絕好，文詞絕好，書翰絕好。」一時傳為美談。

## 虞姓名人

  - [虞娟之](../Page/虞姬_\(齊國\).md "wikilink")：楚王妃妾，善辭令
  - [虞卿](../Page/虞卿.md "wikilink")：戰國時說客，曾遊說趙孝成王成功，成為趙國上卿，後与魏齐一同去逃往魏国大梁。
  - [虞姬](../Page/虞姬.md "wikilink")：[秦朝末年西楚霸王项羽宠妃](../Page/秦朝.md "wikilink")，自刎于垓下，有《垓下歌》证。
  - [虞經](../Page/虞經.md "wikilink")：東漢史官，武平人。
  - [虞詡](../Page/虞詡.md "wikilink")：漢代軍事家，擊退西羌。
  - [虞松](../Page/虞松.md "wikilink")：三國曹魏官員
  - [虞翻](../Page/虞翻.md "wikilink")：三國吳的经学家和政治家；既能著书立说，又通醫理和占卜，官至[骑都尉](../Page/骑都尉.md "wikilink")。
  - [虞溥](../Page/虞溥.md "wikilink")：晉朝學者，《江表傳》的作者
  - [虞丘進](../Page/虞丘進.md "wikilink")：南朝劉宋將領

<!-- end list -->

  - [虞世南](../Page/虞世南.md "wikilink")：[唐朝书法家](../Page/唐朝.md "wikilink")
  - [虞仲文](../Page/虞仲文.md "wikilink")：[南宋名臣](../Page/南宋.md "wikilink")，紹興二十二年（1152年）進士。
  - [虞允文](../Page/虞允文.md "wikilink")：[南宋名臣](../Page/南宋.md "wikilink")，紹興二十四年（1154年）進士。
  - [虞集](../Page/虞集.md "wikilink")：元代學者，崇仁人
  - [虞朗](../Page/虞朗.md "wikilink")：清代女畫家，金壇人。
  - [虞黃昊](../Page/虞黃昊.md "wikilink")：清代詩人。字景明，錢塘人。康熙舉人。自幼聰明，十歲的時候就能做詩
  - [虞偉亮](../Page/虞偉亮.md "wikilink")：中国职业[足球](../Page/足球.md "wikilink")[守门员](../Page/守门员.md "wikilink")，曾经入选[国家队](../Page/中国国家足球队.md "wikilink")。
  - [虞承璇](../Page/虞承璇.md "wikilink")：台灣[壹電視名主播](../Page/壹電視.md "wikilink")。

## 參考資料

  - [虞](https://web.archive.org/web/20071029102022/http://genealogy.hyweb.com.tw/surname_search_result_all.jsp?xml_id=0000026498)
  - [虞姓 -
    “以国为氏”](http://www.zaobao.com.sg/zaobao/chinese/surname/pages/yu020600.html)

[en:Yu (Chinese name)](../Page/en:Yu_\(Chinese_name\).md "wikilink")

[Y虞](../Category/漢字姓氏.md "wikilink") [\*](../Category/虞姓.md "wikilink")