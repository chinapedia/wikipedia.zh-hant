[Surat_Thani_waterfront.jpg](https://zh.wikipedia.org/wiki/File:Surat_Thani_waterfront.jpg "fig:Surat_Thani_waterfront.jpg")

**他彼河**（泰語：**แม่น้ำ
ตาปี**），是[南泰國最長的](../Page/泰國南部.md "wikilink")[河流](../Page/河流.md "wikilink")。她發源于[洛坤府境內的](../Page/洛坤府.md "wikilink")[黃山](../Page/泰國黃山.md "wikilink")，經[素叻府全境流入](../Page/素叻府.md "wikilink")[萬彤灣](../Page/萬彤灣.md "wikilink")（อ่าว
บ้านดอน，Bandon
Bay），總長230[公里](../Page/公里.md "wikilink")，[流域面積為](../Page/流域面積.md "wikilink")5460[平方公里](../Page/平方公里.md "wikilink")。

根據一份1997年的研究報告，這條河流當年的平均流量為135.4立方米/每秒，以此計算當年總流量共計為4.3立方公里。河流在[奔平區](../Page/奔平區.md "wikilink")（Phun-phin）與**Phum
Duang河**相匯合，然後經[素叻府城流入](../Page/素叻府城.md "wikilink")[泰國灣](../Page/泰國灣.md "wikilink")。

## 外部連結

  - [泰國沼地資訊](https://web.archive.org/web/20060406133846/http://www.arcbc.org.ph/wetlands/thailand/tha_taprivnontunton.htm)
  - [萬彤灣財經預算](https://web.archive.org/web/20050324104315/http://data.ecology.su.se/mnode/Asia/Thailand/bandonbay/bb.htm)
  - [萬彤灣海岸化學品污染資訊（Biogeochemical and human dimensions of coastal
    functioning and change in Southeast
    Asia）](https://web.archive.org/web/20070927081417/http://www.loicz.org/public/loicz/products/r_and_s/17_2thailandreport.pdf)

[Category:泰國河流](../Category/泰國河流.md "wikilink")