《**福星小子**》（／）\[1\]，是[日本著名漫画家](../Page/日本.md "wikilink")[高桥留美子的第一部长篇连载作品](../Page/高桥留美子.md "wikilink")，也是受欢迎的代表作之一。《福星小子》于[小學館](../Page/小學館.md "wikilink")[少年漫画杂志](../Page/少年漫画.md "wikilink")《[少年Sunday周刊](../Page/少年Sunday周刊.md "wikilink")》1978年第39号到1987年第8号上连载，起初连载断断续续，后在高桥[大学](../Page/大学.md "wikilink")[毕业并全力投身漫画创作之后才予以正式连载](../Page/毕业.md "wikilink")，1981年改編成[電視动画](../Page/電視动画.md "wikilink")。

## 概要

《福星小子》的故事主要围绕外星[美少女拉姆和男高中生诸星当展开](../Page/美少女.md "wikilink")，每一回都是独立情节的小故事，基本上是疯狂搞笑的内容。后期还推出了[OVA与](../Page/OVA.md "wikilink")[剧场版](../Page/剧场版.md "wikilink")。《福星小子》电视动画一共有218集，[OVA共](../Page/OVA.md "wikilink")11集，[剧场版则制作了](../Page/剧场版.md "wikilink")6集；其中由[押井守执导的剧场版第](../Page/押井守.md "wikilink")2集《绮丽梦中人》（Beautiful
Dreamer）非常有名，堪称《福星小子》剧场版之最。

《福星小子》漫画在《少年Sunday周刊》连载多年，并先后出版过多种[单行本](../Page/单行本.md "wikilink")，比较有名的有普通版34卷、文库版18集、ワイド版全15集。[台灣地區](../Page/台灣地區.md "wikilink")1991年由[尖端出版正式引進](../Page/尖端出版.md "wikilink")，「-{拉姆}-同好組織」翻譯並詳細注釋，俗稱「尖端版」，是尖端出版第一部取得授權的日本漫畫，全部共25集，每集附錄-{拉姆}-同好組織的評論版面〈友引町情報站〉；現在已經絕版，很難在市面上看到。[香港地區由日本](../Page/香港地區.md "wikilink")[國王唱片香港](../Page/國王唱片.md "wikilink")[子公司](../Page/子公司.md "wikilink")[皇聲洋行率先引進](../Page/皇聲洋行.md "wikilink")，全部共67集，現在已經絕版；之後[文化傳信重新出版](../Page/文化傳信.md "wikilink")，全部共34集，現在已經絕版。[中國大陸地區尚未有正式引進的版本](../Page/中國大陸地區.md "wikilink")，不過[寧夏人民出版社曾經出版過非正式引進的版本](../Page/寧夏人民出版社.md "wikilink")。日本方面，小學館在2006至2008年間推出了「《福星小子》新裝版」，新裝版一共34集，每集都會請一位日本漫畫家或插畫家畫一位拉姆；因此34集中，除了第一集是高橋留美子本人畫的拉姆外，其餘33集皆是由不同的人畫的拉姆。目前新裝版只有在日本販售。

《福星小子》可以说开了日本爆笑漫画的先河，之后的作品均无出其右者，可见其对日本漫画界的影响之深。在2000年以後，日本漫畫界推出了類似題材的漫畫《[圓盤皇女](../Page/圓盤皇女.md "wikilink")》和《[出包王女](../Page/出包王女.md "wikilink")》，都被改編成電視動畫，可見《福星小子》影響力歷久不衰；但該兩書的[露出度都大大超過了](../Page/殺必死.md "wikilink")《福星小子》。

## 故事簡介

福星小子描述的是外星鬼族女子拉姆与地球少年诸星当之间的爱情故事。拉姆是遥远的鬼星公主，随其父踏上了企图征服地球的旅程，然而卻在一场决定地球命运的捉迷藏对决中被诸星当打败。随后双方就上演了一场又一场令人捧腹大笑的爱情欢笑故事。

## 登場角色

下列各角色之[配音員](../Page/配音員.md "wikilink")（CV）姓名後方之標示如下：「JP」為日語配音員，「HK-ATV-1」為[亞洲電視](../Page/亞洲電視.md "wikilink")1984年版配音員，「HK-ATV-2」為亞視1990年版配音員，「TW」為台灣版（[衛視中文台版與](../Page/衛視中文台.md "wikilink")[台視版](../Page/台視.md "wikilink")，[富國錄音製作配音](../Page/富國錄音.md "wikilink")）配音員。

### 主角

  - **諸星當**（，Ataru
    Moroboshi）（CV：[古川登志夫](../Page/古川登志夫.md "wikilink")(JP)
    / [葉少榮](../Page/葉少榮.md "wikilink")(HK-ATV-1/2) /
    [-{于}-正昇](../Page/于正昇.md "wikilink")(TW)）

<!-- end list -->

  -
    暱稱「阿當」，港版譯成「諸事當」，本作品的男主角。友引高中2年4班学生(连载初期是1年4班)。极端喜欢女孩子，只要见到年輕或貌美的女子，不論對方是人是妖或是幽靈，就會立即上前搭訕，是個花心大蘿蔔。有超人的体力和耐力，復完能力極高，故他多次受到拉姆的電擊、櫻的飛腿、三宅忍的飛桌、面堂終太郎的劍擊和錯亂僧的臉嚇，也能安然無恙。他并被认为是宇宙之中的烦恼之主宰。诸星当这个名字取自日本著名[棒球运动员](../Page/棒球.md "wikilink")[江川卓的弟弟江川中](../Page/江川卓.md "wikilink")。诸星当最擅长的是偽装术，当然只有他一个人自认完美。诸星当出生的时候遇上了很多不吉祥的事情，所以他才被错乱僧认为是世界上最倒霉的人；不过，他对此并不在意。

<!-- end list -->

  - **拉姆**（，Lum Invader）（CV：[平野文](../Page/平野文.md "wikilink")(JP) /
    [雷碧娜](../Page/雷碧娜.md "wikilink")(HK-ATV-1) /
    [陸惠玲](../Page/陸惠玲.md "wikilink")(HK-ATV-2) /
    [呂佩玉](../Page/呂佩玉.md "wikilink")(TW)）

<!-- end list -->

  -
    港版譯成「阿-{}-琳」，台灣譯作「拉-{}-姆」，本作品的女主角。

### 同學

  - **面堂終太郎**（，Shutaro Mendou）（CV：[神谷明](../Page/神谷明.md "wikilink")(JP) /
    [黃志成](../Page/黃志成.md "wikilink")(HK-ATV-1)、[陳永信](../Page/陳永信.md "wikilink")(HK-ATV-2)
    / [魏伯勤](../Page/魏伯勤.md "wikilink")(TW)）

<!-- end list -->

  -
    在臺灣動畫版中被改名“唐泰宗”（與唐代君主[唐太宗諧音](../Page/唐太宗.md "wikilink")），港版譯成「面太郎」。诸星当同班（友引高中2年4班）同学，除了家境富有、长得帅、功课好、极受女生欢迎之外，其实性格和诸星当是一样的人，同时也是诸星当的死敌；但通常都特別倒楣。從小就有“[密室恐惧症](../Page/密室恐惧症.md "wikilink")”，只要被關在黑暗空間裏就會狂叫：「好黑！好暗！好可怕！」
    「面堂」一詞在日文中與「面倒」（「麻煩」、「trouble」之義）同音。終太郎跟他妹妹"了子"，名字在字面上都帶有「結束」、“完结”或「末代」的意思。

<!-- end list -->

  - **三宅忍**（，Shinobu Miyake）（CV：[島津冴子](../Page/島津冴子.md "wikilink")(JP) /
    ［港譯：阿秀］[林錦燕](../Page/林錦燕.md "wikilink")(HK-ATV-1)、［珠珠］[朱妙蘭](../Page/朱妙蘭.md "wikilink")(HK-ATV-2)
    ）

<!-- end list -->

  -
    和诸星当青梅竹马兼同班（友引高中2年4班）同学，拥有怪力，但总自称柔弱女子。富有善良心。而她的名字则来自於最初連載時的《少年Sunday周刊》编辑[三宅克](../Page/三宅克.md "wikilink")，兩人姓名的日語讀法相同。

<!-- end list -->

  - **藤波龍之介**（，Ryūnosuke
    Fujinami）（CV：[田中真弓](../Page/田中真弓.md "wikilink")(JP)）/
    [吳小藝](../Page/吳小藝.md "wikilink")(HK-ATV-2)

<!-- end list -->

  -
    诸星当的同班（友引高中2年4班）同学。虽然是女生，但是被其父从小作为男生培养。拥有高强的武艺，梦想成为真正的女生。

### 拉姆親衛隊

  - **眼鏡**（，Megane）（CV：[千葉繁](../Page/千葉繁.md "wikilink")(JP)）

<!-- end list -->

  -
    诸星当同班（友引高中2年4班）同学，亲卫队队长。眼镜本名叫（Satoshi），是动画特有的人物。他富有激情的演说，给无数的观众留下了深刻的印象。

<!-- end list -->

  - **金髮**（，Pama）（CV：[村山明](../Page/村山明.md "wikilink")(JP)）

<!-- end list -->

  -
    诸星当同班（友引高中2年4班）同学，亲卫队成员。个子高，动画中是一头金色的头发。本名叫（Kosuke）。

<!-- end list -->

  - **矮子**（，Chibi）（CV：[二又一成](../Page/二又一成.md "wikilink")(JP)）

<!-- end list -->

  -
    诸星当同班（友引高中2年4班）同学，亲卫队成员。个子矮小，胆子也小。喜欢骑单车。本名叫（Akira）。

<!-- end list -->

  - **胖子**（，Kakukari）（CV：[野村信次](../Page/野村信次.md "wikilink")）

<!-- end list -->

  -
    诸星当同班（友引高中2年4班）同学，亲卫队成员。本名叫（Hiroyuki）。

### 拉姆的親友

  - **拉姆的父親**（）（CV：[沢りつお](../Page/沢りつお.md "wikilink")(JP) /
    [周永坤](../Page/周永坤.md "wikilink")(HK-ATV-1)、[戴偉光](../Page/戴偉光.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    拉姆的父親。外星人，造型是日本傳說故事的鬼，虎皮裝且長角。

<!-- end list -->

  - **拉姆的母親**（）（CV：[山田禮子](../Page/山田禮子.md "wikilink")(JP) /
    [陶碧儀](../Page/陶碧儀.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    拉姆的母親，穿著虎皮[旗袍](../Page/旗袍.md "wikilink")。

<!-- end list -->

  - **小天**（）（CV：[杉山佳壽子](../Page/杉山佳壽子.md "wikilink")(JP) /
    [林錦燕](../Page/林錦燕.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    拉姆的表弟，会飞和喷火。经常被诸星当欺负。

<!-- end list -->

  - **小天的母親**（）（CV：[橫澤啟子](../Page/橫澤啟子.md "wikilink")(JP一代)、[勝生真沙子](../Page/勝生真沙子.md "wikilink")(JP二代)）/
    [龍寶鈿](../Page/龍寶鈿.md "wikilink")(HK-ATV-2)

<!-- end list -->

  -
    小天的母親，拉姆的阿姨，身穿日本江戶時代的消防裝，手持消防隊的旗印(日文稱為[纏](../Page/纏.md "wikilink"))是位消防隊長(日文的[火消](../Page/火消.md "wikilink"))。

<!-- end list -->

  - **雷易**（）（CV：[玄田哲章](../Page/玄田哲章.md "wikilink")(JP) /
    [陳永信](../Page/陳永信.md "wikilink")(HK-ATV-1)）

<!-- end list -->

  -
    臺灣動畫版譯為“小雷”。与拉姆同族，并和拉姆定有婚约，最后拉姆不能忍受雷易的性格而取消婚约。长得非常帅，但是有时会变成[牛](../Page/牛.md "wikilink")。非常非常爱吃东西，[不愛说话](../Page/無口.md "wikilink")，说出两个字以上的话就被认为非常不可思议。穿著虎皮連身衣，從腳掌一路包覆至頸部以下，这是鬼星男性特有的服装。

<!-- end list -->

  - **小蘭**（，Ran）（CV：[井上瑤](../Page/井上瑤.md "wikilink")(JP一代)、[小宮和枝](../Page/小宮和枝.md "wikilink")(JP二代)
    / [李宛鳳](../Page/李宛鳳.md "wikilink")(TW) /
    [林錦燕](../Page/林錦燕.md "wikilink")(HK-ATV-1)、[吳小藝](../Page/吳小藝.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    拉姆的童年好友兼仇人，具有[双重人格](../Page/双重人格.md "wikilink")。喜欢雷易，一直以为拉姆要和她抢雷易。她是拉姆在行星小学的同学，來到地球後入讀友引高中2年7班，企圖向拉姆報復。

<!-- end list -->

  - **弁天**（）（CV：[三田友子](../Page/三田友子.md "wikilink")(JP) /
    [陶碧儀](../Page/陶碧儀.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    拉姆的童年好友，也是拉姆在行星小学的同学，是壞女孩一名，喜歡飈車和打架。名字取自[七福神的](../Page/七福神.md "wikilink")[弁財天](../Page/弁財天.md "wikilink")。

<!-- end list -->

  - **阿雪**（，Oyuki）（CV：[小原乃梨子](../Page/小原乃梨子.md "wikilink")(JP) /
    [陶碧儀](../Page/陶碧儀.md "wikilink")(HK-ATV-1)）

<!-- end list -->

  -
    臺灣動畫版譯為“小雪”。拉姆的童年好友，也是拉姆在行星小学的同学，性格貪財。是個[雪女](../Page/雪女.md "wikilink")，能冰封對手，也是[海王星女王](../Page/海王星.md "wikilink")。

### 其他人物

  - **諸星當的父親**（）（CV：[緒方賢一](../Page/緒方賢一.md "wikilink")(JP) /
    [李學儒](../Page/李學儒.md "wikilink")(HK-ATV-1)、[陸頌愚](../Page/陸頌愚.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    諸星當的父親。

<!-- end list -->

  - **諸星當的母親**（）（CV：[佐久間夏生](../Page/佐久間夏生.md "wikilink")(JP) /
    [余秀明](../Page/余秀明.md "wikilink")(HK-ATV-1)、[曾秀清](../Page/曾秀清.md "wikilink")／[黃鳳英](../Page/黃鳳英.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    諸星當的母親。

<!-- end list -->

  - **藤波龍之介的父親**（）（CV：[安西正弘](../Page/安西正弘.md "wikilink")(JP
    TV版)、[大川透](../Page/大川透.md "wikilink")(JP OVA版)）

<!-- end list -->

  -
    藤波龍之介的父親。

<!-- end list -->

  - **終太郎與了子的父親**（）（CV：[麥人](../Page/麥人.md "wikilink")(JP)）

<!-- end list -->

  -
    面堂終太郎與面堂了子的父親，面堂終太郎與面堂了子尊稱為「父親大人」。

<!-- end list -->

  - **終太郎與了子的母親**（）（CV：[坪井章子](../Page/坪井章子.md "wikilink")(JP)）

<!-- end list -->

  -
    面堂終太郎與面堂了子的父親，面堂終太郎與面堂了子尊稱為「母親大人」。

<!-- end list -->

  - **面堂祖父**（）（CV：[北村弘一](../Page/北村弘一.md "wikilink")(JP)）

<!-- end list -->

  -
    面堂終太郎與面堂了子的祖父。

<!-- end list -->

  - **錯亂僧**〔，又名「櫻桃」（；Cherry）〕（CV：[永井一郎](../Page/永井一郎.md "wikilink")(JP)，[黃子敬](../Page/黃子敬.md "wikilink")(HK-ATV-1)、[周永坤](../Page/周永坤.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    自称「日本最强」的灵能力者。总是莫名其妙出现。特技是衝浪、瑜珈。錯乱的日語發音和櫻相近，坊除了和尚的意思外，也有另一義為小孩。所以自稱小名櫻桃。

<!-- end list -->

  - **櫻花**（，Sakura）（CV：[鷲尾真知子](../Page/鷲尾真知子.md "wikilink")(JP)、[余秀明](../Page/余秀明.md "wikilink")(HK-ATV-1)、[陶碧儀](../Page/陶碧儀.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    错乱僧的外甥女，友引高中的保健医生，同时还是一名无照[巫女](../Page/巫女.md "wikilink")。和雷易一样，都拥有恐怖的食量。

<!-- end list -->

  - **被爐貓**（）（CV：[西村朋紘](../Page/西村朋紘.md "wikilink")(JP)）

<!-- end list -->

  -
    由一只冤死的[猫的](../Page/猫.md "wikilink")[灵魂所化成](../Page/灵魂.md "wikilink")，喜歡待在被爐裡。据说是《福星小子》中最强的生物。被"超渡"後成了校長室的常客，和校長一起喝茶，巡視校園。

<!-- end list -->

  - **面堂了子**（，Ryoko
    Mendou）（CV：[小山茉美](../Page/小山茉美.md "wikilink")(JP)，[吳小藝](../Page/吳小藝.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    面堂終太郎的妹妹。經常会做出一些可怕的事情。腹黑代表。

<!-- end list -->

  - **克拉瑪**（，Kurama）（CV：[吉田理保子](../Page/吉田理保子.md "wikilink")(JP)）

<!-- end list -->

  -
    克拉瑪星的天狗女王，[源義經的女兒](../Page/源義經.md "wikilink")。為了让諸星當改掉花心的习惯，使出了各種手段，但都因阿當屢教不改而宣告失敗。

<!-- end list -->

  - **溫泉老師**（）（CV：[池水通洋](../Page/池水通洋.md "wikilink")(JP)，[周慶樑](../Page/周慶樑.md "wikilink")(HK-ATV-1)、[黃子敬](../Page/黃子敬.md "wikilink")(HK-ATV-2)）

<!-- end list -->

  -
    嚴格的老師，衣服上都是溫泉的標誌。常與阿當立場對立，進而發生無意義的對抗。

<!-- end list -->

  - **花和老師**（）（CV：[納谷六朗](../Page/納谷六朗.md "wikilink")）

<!-- end list -->

  -
    熱心的溫和派老師。

<!-- end list -->

  - **水乃小路飛麿**（）（CV：[井上和彥](../Page/井上和彥.md "wikilink")(JP一代)、[島田敏](../Page/島田敏.md "wikilink")(JP二代)）

<!-- end list -->

  -
    終太郎的敵對家族小開，運動神經遲鈍，從小被了子惡作劇。

<!-- end list -->

  - **水乃小路飛鳥**（）（CV：[島本須美](../Page/島本須美.md "wikilink")(JP)）

<!-- end list -->

  -
    水乃小路飛麿的妹妹，面堂終太郎的未婚妻。擁有巨大的力量，但有[男性恐懼症](../Page/男性恐懼症.md "wikilink")。

<!-- end list -->

  - **飛麿的祖父**（）

<!-- end list -->

  -
    水乃小路飛麿的祖父。

<!-- end list -->

  - **飛麿與飛鳥的父親**（）（CV：[二又一成](../Page/二又一成.md "wikilink")(JP)）

<!-- end list -->

  -
    水乃小路飛麿與水乃小路飛鳥的母親。

<!-- end list -->

  - **飛麿與飛鳥的母親**（）（CV：[梨羽由記子](../Page/梨羽由記子.md "wikilink")(JP)）

<!-- end list -->

  -
    水乃小路飛麿與水乃小路飛鳥的母親。

<!-- end list -->

  - **星際計程車司機**（）（CV：[肝付兼太](../Page/肝付兼太.md "wikilink")(JP)）

<!-- end list -->

  -

<!-- end list -->

  - **金太郎**（）（CV：[野澤雅子](../Page/野澤雅子.md "wikilink")(JP)）

<!-- end list -->

  -

<!-- end list -->

  - **地底王子**（）（CV：[千葉繁](../Page/千葉繁.md "wikilink")(JP)）

<!-- end list -->

  -

<!-- end list -->

  - **CAO-2**（）（CV：[永井一郎](../Page/永井一郎.md "wikilink")(JP)）

<!-- end list -->

  -
    在行星小學任教200年的教師。

<!-- end list -->

  - **大魔神**（）（CV：[飯塚昭三](../Page/飯塚昭三.md "wikilink")(JP)）

<!-- end list -->

  -

<!-- end list -->

  - **侵略者**（）（CV：[富山敬](../Page/富山敬.md "wikilink")(JP)）

<!-- end list -->

  -
    意圖侵略地球的宇宙人，以接吻將「洗腦液」注入地球人體內。

<!-- end list -->

  - **邱比特**（）（CV：[丸山裕子](../Page/丸山裕子.md "wikilink")(JP)）

<!-- end list -->

  -
    動畫版工作人員表誤標為「邱比」（）。

<!-- end list -->

  - **青鳥**（）（CV：[肝付兼太](../Page/肝付兼太.md "wikilink")(JP)）

<!-- end list -->

  -
    即是“連續幸福魔青鳥”。友引高中遭它攻擊時，宇宙捕快奇魯奇魯和米奇魯來將之逮捕歸案。

<!-- end list -->

  - **許願星**（）（CV：[田之中勇](../Page/田之中勇.md "wikilink")(JP)）

<!-- end list -->

  -
    每次允許實現3個願望，滿3個願望即離開地球。

## 漫画

### 各卷标题

下面是福星小子漫画的各卷标题，以最为流行的日文普通版为参考资料给出。

  - 第０１卷
      - 1　かけめぐる青春
      - 2　やさしい悪魔
      - 3　悲しき雨音
      - 4　あなたにあげる
      - 5　绝体绝命
      - 6　爱で杀したい
      - 7　憎みきれないろくでなし
      - 8　いい日旅立ち
      - 9　大胜负
  - 第０２卷
      - 10　お雪
      - 11　性
      - 12　系図
      - 13　あやつり人形
      - 14　いまだ浮上せず
      - 15　幸せの黄色いリボン
      - 16　女になって出直せよ
      - 17　思い过ごしも恋のうち
  - 第０３卷
      - 18　あなたは强かった
      - 19　ディスコ・インフェルノ
      - 20　さよならを言う気もない
      - 21　勇気があれば
      - 22　トラブルは舞い降りた！！
      - 23　星座はめぐる
      - 24　春のうららの落第教室
      - 25　ツノる思いが地狱をまねく
      - 26　君まてども
  - 第０4卷
      - 27　酒と泪と男と女
      - 28　涙の日记
      - 29　この子はだあれ？
      - 30　迷路でメロメロ
      - 31　目覚めたら悪梦
      - 32　四次元カメラ
      - 33　美しい旅立ち
      - 34　君去りし后
      - 35　魔のランニング
      - 36　七夕デート
  - 第０５卷
      - 37　妖花・サクラ先生
      - 38　みんな雨ン中
      - 39　怪谈・コピー人间
      - 40　白球にかけた青春
      - 41　怪人赤マント
      - 42　サーフィンＳＯＳ
      - 43　水着ドロボウ
      - 44　ビキニを夺え
      - 45　マンナンウォーズ
      - 46　ヨガで迷想
      - 47　哀愁でいと
  - 第０６卷
      - 48　个人教授
      - 49　コートの中では泣かないわ
      - 50　コートに消える恋
      - 51　戦栗の参観日
      - 52　くノ一、奈良を走る
      - 53　くノ一、京に潜む
      - 54　くノ一は永远に
      - 55　体育祭危机一髪
      - 56　文化祭危机一髪
      - 57　演剧祭危机一髪
      - 58　お别れパーティー危机一髪
  - 第０７卷
      - 59　思い出危机一髪
      - 60　正真正铭危机一髪
      - 61　あたるの引退
      - 62　おみくじこわい
      - 63　テンちゃんがきた
      - 64　テンちゃんの恋
      - 65　ふたりだけのデート
      - 66　花から花へ
      - 67　鬼に豆鉄炮
      - 68　ミス雪の女王
      - 69　バレンタインデーの惨剧
  - 第０８卷
      - 70　恋のアリ地狱
      - 71　桃の花歌合戦
      - 72　イヤーマッフルの怪
      - 73　ニャオンの恐怖にゃ気をつけろ！！
      - 74　大空に舞うハチャメチャ四人组！！
      - 75　平安编　壱の巻
      - 76　平安编　弐の巻
      - 77　平安编　参の巻
      - 78　”爱”それは校内暴力とともに
      - 79　ああ、図书馆
      - 80　ああ、子どもの日は恐怖じゃ！
  - 第０９卷
      - 81　幼児たちよ、大志を抱け！！
      - 82　ツバメさんとペンギンさん
      - 83　三つ子の魂、百までも！
      - 84　女王陛下と爱のラガーマン！！
      - 85　原生动物の逆袭
      - 86　住めば都
      - 87　见合いコワし＝その１＝
      - 88　见合いコワし＝その２＝
      - 89　见合いコワし＝その３＝
      - 90　见合いコワし＝その４＝
      - 91　生ゴミ、海へ
  - 第１０卷　
      - 92　雨よ降れ降れ、もっと降れ！＝その１＝
      - 93　雨よ降れ降れ、もっと降れ！＝その２＝
      - 94　雨よ降れ降れ、もっと降れ！＝その３＝
      - 95　パニックＩｎ幽霊民宿
      - 96　わがまま幽霊
      - 97　海辺の怪
      - 98　台风は楽し
      - 99　秘密指令”デートをのぞけ！”
      - 100　风イヤですね！
      - 101　翔んだドラキュラ
      - 102　爱の献血运动
  - 第１１卷
      - 103　薬口害
      - 104　苦しいときの神だのみ！！
      - 105　アマテラス宴会
      - 106　买い食い大戦争
      - 107　コピーｄｅデート！
      - 108　昔なつかし、ぐちれよパソコン！
      - 109　おれのツノがない！？
      - 110　自习騒动
      - 111　阶段に猫がおんねん！
      - 112　酔っぱらいブギ
      - 113　忘年会じゃあ！
  - 第１２卷
      - 114　面倒邸新年怪
      - 115　旅の雪ダルマ情话
      - 116　吃茶店の出入りを禁ず！！
      - 117　所持品検查だ！
      - 118　部外者ちん入
      - 119　惑わじのバレンタイン！！
      - 120　命かけます、授业中！！
      - 121　面堂兄弟！！＝その１＝
      - 122　面堂兄弟！！＝その２＝
      - 123　セーラー服よ、こんにちは！
      - 124　テンからの赠り物！！
  - 第１３卷
      - 125　スーパー武蔵、天までダッシュ！！
      - 126　武蔵、ただいま修行中！！
      - 127　ダメッ子武蔵、男やないか！！
      - 128　柳精翁の恐怖！？
      - 129　テン敌
      - 130　化石の僻地
      - 131　虫歯ＷＡＲＳ！！
      - 132　汤船に浮かぶ銭の花を流せ！！
      - 133　夫妇ゲンカ始末记
      - 134　タヌキが”ツルの恩返し”
      - 135　面堂家仮面ぶとう会
  - 第１４卷
      - 136　クラマ再び！！
      - 137　口づけと共に契らん！！
      - 138　掟、おさらば！！
      - 139　ビン诘めの诱惑！！
      - 140　食べれば恐怖！！
      - 141　校内赌博球技大会
      - 142　辛いキャンプに明日はない！！
      - 143　桃源郷奇谈！！
      - 144　三人サクラ浮気の构図
      - 145　デートとイルカと海辺の浮気
      - 146　みんなで海をきれいにしよう！
  - 第１５卷
      - 147　激闘、父子鹰！！
      - 148　セーラー服よ、コンにちは！！
      - 149　ブルマーを求めて！！
      - 150　兰は女の香り＝その１＝
      - 151　兰は女の香り＝その２＝
      - 152　努力、女の道！！
      - 153　女の证明！
      - 154　絵画はスポーツだ！
      - 155　花婿、それは女！！
      - 156　あたる、女へ！！
      - 157　性転のヘキレキ！！
  - 第１６卷
      - 158　晩秋、そしてまつたけ！！
      - 159　妖怪徳利！！
      - 160　母子断绝火事模様＝その１＝
      - 161　母子断绝火事模様＝その２＝
      - 162　爱◆ダーリンの危机！！
      - 163　クリスマスツリーわいわいパーティー！！
      - 164　家宝は寝て待て！！
      - 165　竜之介一家団らん！！
      - 166　怒りのラムちゃん！！
      - 167　さらば温泉マーク！！
      - 168　うちらの节分ケンカ祭りだっちゃ！！
  - 第１７卷
      - 169　父と娘の爱のバレンタイン！！
      - 170　必杀ケンカ少年・スッポンのテン！！
      - 171　あんこ悲しや、恋の味！？
      - 172　き・え・な・いルージュマジック！！
      - 173　埋没教室！！
      - 174　艶姿桜御祓！！
      - 175　花见デスマッチ！！
      - 176　思い出ボロボロ！？
      - 177　思い出のアルバム
      - 178　みじめっ子・终太郎！！
      - 179　逆上、终太郎！！
  - 第１８卷
      - 180　惑星教师ＣＡＯ－２
      - 181　无我の妙薬！！
      - 182　トンちゃんの駆け落ち！
      - 183　夜を二人で！！
      - 184　あこがれを胸に！！＝その１＝
      - 185　あこがれを胸に！！＝その２＝
      - 186　ミス友引コンテスト；予备选
      - 187　ミス友引コンテスト；本选
      - 188　ミス友引コンテスト；水着审查
      - 189　ミス友引コンテスト；戦う女たち
      - 190　ミス友引コンテスト；结果発表
  - 第１９卷
      - 191　水中ラブ三角形！！
      - 192　テンちゃん宙に浮く！？
      - 193　岩石の母；前编
      - 194　岩石の母；后编
      - 195　缶につめた小さな青春
      - 196　ああ夜店
      - 197　わたしはサ・カ・ナ
      - 198　哀愁の幼年期・サクラ
      - 199　失われたモノを求めて；前编
      - 200　失われたモノを求めて；激闘３ＶＳ３
      - 201　失われたモノを求めて；完结编
  - 第２０卷
      - 202　トＬＯＶＥル・レター；前编
      - 203　トＬＯＶＥル・レター；后编
      - 204　爱の行方
      - 205　华丽なる落叶の舞い
      - 206　爱は国境を越えて
      - 207　恋人泥棒；前编
      - 208　恋人泥棒；后编
      - 209　爱がふれあうとき
      - 210　青春ラグビー地狱
      - 211　空を飞ぶ童女
      - 212　魔境！戦栗の密林
  - 第２１卷
      - 213　密室！ケーキ屋敷の恐怖
      - 214　シェイプアップ・羽根つき
      - 215　芸道一筋いばら道
      - 216　爱と闘魂のグローブ
      - 217　苍白き炎の怒り；前编
      - 218　苍白き炎の怒り；中编
      - 219　苍白き炎の怒り；后编
      - 220　変身スプレー
      - 221　ラムちゃん、ウシになる
      - 222　月夜のキツネたち
      - 223　竜之介式男女交际
  - 第２２卷
      - 224　水乃小路家の娘＝その１＝
      - 225　水乃小路家の娘＝その２＝
      - 226　水乃小路家の娘＝その３＝
      - 227　水乃小路家の娘＝その４＝
      - 228　游び人金さん始末记
      - 229　逆袭！三人组
      - 230　悩み多き青春
      - 231　豪华绚烂正义の味方
      - 232　あな恐ろしや、ワラ人形
      - 233　秘密の花园
      - 234　テンちゃんの息子
  - 第２３卷
      - 235　勇気ある决闘；前编
      - 236　勇気ある决闘；后编
      - 237　飞鸟ふたたび＝その１＝
      - 238　飞鸟ふたたび＝その２＝
      - 239　飞鸟ふたたび＝その３＝
      - 240　必杀！ヤミナベ
      - 241　アイスクーラー・リラックス
      - 242　ザ・障害物水泳大会；前编
      - 243　ザ・障害物水泳大会；后编
      - 244　合戦！面堂家花火大会
      - 245　浜茶屋繁盛记
  - 第２４卷
      - 246　非道なる商売人
      - 247　最后のデート
      - 248　大脱走・大騒动
      - 249　スケ番グループ色気大作戦
      - 250　コタツにかけた爱
      - 251　妻は面影の中に
      - 252　岚を呼ぶデート＝その１＝
      - 253　岚を呼ぶデート＝その２＝
      - 254　岚を呼ぶデート＝その３＝
      - 255　岚を呼ぶデート＝その４＝
      - 256　岚を呼ぶデート＝その５＝
  - 第２５卷
      - 257　梦で逢えたら；现実编
      - 258　梦で逢えたら；大决戦
      - 259　执念の形
      - 260　オデンに秘めた恋
      - 261　更け行く秋のイモの悲しき
      - 262　风邪ひかば
      - 263　モチ争夺校长胸像杯
      - 264　歳末は钟鸣り
      - 265　かむかむエブリボデェ
      - 266　ハートブレイクＣｒｏｓｓｉｎ’；前编
      - 267　ハートブレイクＣｒｏｓｓｉｎ’；后编
  - 第２６卷
      - 268　节分危机一髪
      - 269　决闘！女ＶＳ女；前编
      - 270　决闘！女ＶＳ女；中编
      - 271　决闘！女ＶＳ女；后编
      - 272　大ビン小ビン
      - 273　风邪见舞い
      - 274　あやかしの面堂
      - 275　妄想フーセンガム
      - 276　宇宙からの侵略者
      - 277　爱の使者どすえ
      - 278　お祓三すくみ
  - 第２７卷
      - 279　キツネの嫁取り
      - 280　强食惑星
      - 281　かってな幸福、青い鸟
      - 282　母の心、子の心
      - 283　飞んでけ！肩こり
      - 284　スケ番三人娘、动物作戦
      - 285　活鱼つかみどり
      - 286　十年目の真実
      - 287　はんぎゃ摘み
      - 288　水着ラプソディー
      - 289　反省座禅会
  - 第２８卷
      - 290　星に愿いを
      - 291　口づけ◆速达小包
      - 292　兄弟★爱の戦い；前编
      - 293　兄弟★爱の戦い；后编
      - 294　怒りのプール
      - 295　つるつるソープ
      - 296　浜茶屋の夏
      - 297　风铃树の音色
      - 298　涙の家庭访问
      - 299　妖精のパラソル
      - 300　写真の中の女
  - 第２９卷
      - 301　电饰の魔境＝その１＝
      - 302　电饰の魔境＝その２＝
      - 303　电饰の魔境＝その３＝
      - 304　电饰の魔境＝その４＝
      - 305　マジカルハット
      - 306　情热の赤い靴
      - 307　プリマの星をつかめ
      - 308　箱宇宙に秘められた过去
      - 309　电気仕挂けの御庭番；前编
      - 310　电気仕挂けの御庭番；后编
      - 311　校长殴打事件
  - 第３０卷
      - 312　七面鸟逃げた
      - 313　おまえのひみつをしっている
      - 314　おとなの恋の物语；前编
      - 315　おとなの恋の物语；后编
      - 316　馒头こわい
      - 317　怒りの空中戦
      - 318　カマクラ伝说
      - 319　霊魂とデート
      - 320　极彩のペアルック
      - 321　别世界への招待
      - 322　ダーリンの本音
  - 第３１卷
      - 323　爱の脳天逆落とし；前编
      - 324　爱の脳天逆落とし；后编
      - 325　花より桜もち
      - 326　モグラの嫁とり
      - 327　咒われたタマゴ
      - 328　失われた记忆
      - 329　扉を开けて
      - 330　爱と哀しみの果て
      - 331　明日なき暴走
      - 332　梦の扉
      - 333　明日へもういっちょ
  - 第３２卷
      - 334　极めよ、贫乏道
      - 335　思い出は时限爆弾
      - 336　タコを呼ぶ笛
      - 337　飞べ、爱の伝书鸠
      - 338　洁癖の要塞；前编
      - 339　洁癖の要塞；中编
      - 340　洁癖の要塞；后编
      - 341　渚のフィアンセ；前编
      - 342　渚のフィアンセ；后编
      - 343　朝颜、怒りの逆袭
      - 344　怒れ、シャーベット
  - 第３３卷
      - 345　马と令嬢
      - 346　爱と勇気の花一轮；前编
      - 347　爱と勇気の花一轮；中编
      - 348　爱と勇気の花一轮；后编
      - 349　一夜の攻防戦；前编
      - 350　一夜の攻防戦；后编
      - 351　乙女バシカの恐怖
      - 352　ヤギさんとチーズ
      - 353　月に吠える
      - 354　ハートをつかめ
      - 355　秘汤を求めて
  - 第３４卷
      - 356　ボーイ　ミーツ　ガール　　まっくろけ
      - 357　ボーイ　ミーツ　ガール　　嫁に来ないか
      - 358　ボーイ　ミーツ　ガール　　别れの朝
      - 359　ボーイ　ミーツ　ガール　　ハートのＩｇｎｉｔｉｏｎ
      - 360　ボーイ　ミーツ　ガール　　再会のラビリンス
      - 361　ボーイ　ミーツ　ガール　　结婚するって本当ですか
      - 362　ボーイ　ミーツ　ガール　　ねじれたハートで
      - 363　ボーイ　ミーツ　ガール　　こまっちゃうな
      - 364　ボーイ　ミーツ　ガール　　どうにもとまらない
      - 365　ボーイ　ミーツ　ガール　　ないものねだりのＩ　Ｗａｎｔ　Ｙｏｕ
      - 366　ボーイ　ミーツ　ガール　　Ｆｉｎ

## 影视

### 动画

《福星小子》TV版于1981年10月14日至1986年3月19日在日本[富士电视台播出](../Page/富士电视台.md "wikilink")，共計218集。《福星小子》在[商业上的巨大成功](../Page/商业.md "wikilink")，很大程度要归功于动画的功绩。《福星小子》TV版由[高田明美擔任](../Page/高田明美.md "wikilink")[人物設計](../Page/人物設計.md "wikilink")，率先将拉姆的头发塑造成绿色，而将拉姆的眼睛设定为金色；这个设定非常成功，使《福星小子》迅速成为当时最受观众喜爱的动画之一。1986年，《福星小子》播毕，由高橋留美子另一漫畫改編的動畫《[相聚一刻](../Page/相聚一刻.md "wikilink")》接挡，[收视率大为降低](../Page/收视率.md "wikilink")，反映出《福星小子》的影响力。

[香港早期由](../Page/香港.md "wikilink")[亞洲電視取得播映權](../Page/亞洲電視.md "wikilink")，並曾經以《我係山T-{}-女福星》名義在1983年8月18日至10月21日於當時仍稱作中文台的[本港台首播](../Page/本港台.md "wikilink")，並於1984年5月7日至6月22日期間作第二次播映；在1986年至1991年間也曾經多次重播，而[陳永信](../Page/陳永信.md "wikilink")、[雷碧娜和](../Page/雷碧娜.md "wikilink")[陸惠玲皆已轉往](../Page/陸惠玲.md "wikilink")[無綫電視工作](../Page/無綫電視.md "wikilink")。

在[臺灣地區](../Page/臺灣.md "wikilink")，曾於[衛視中文台](../Page/衛視中文台.md "wikilink")、[台視及](../Page/台視.md "wikilink")[Animax播放](../Page/Animax.md "wikilink")。

### OVA

《福星小子》一共有十一部OVA（以2015年發行的[日版OVA Blu-ray
BOX](http://www.amazon.co.jp/exec/obidos/ASIN/B00PBZBPMG/nataliecomic-22)之收錄清單為準）。分别如下表示：

|            |                              |                     |
| ---------- | ---------------------------- | ------------------- |
| **发售日期**   | **日文名**                      | **中文名**             |
| 1985/??/?? | 了子の９月のお茶会                    | 了子的九月茶会             |
| 1987/09/21 | 夢の仕掛人、因幡くん登場！ラムの未来はどうなるっちゃ！？ | 夢想製造者因幡登場！拉姆的未來會是！？ |
| 1988/12/02 | 怒れシャーベット                     | 生氣吧！冰砂              |
| 1988/12/08 | 渚のフィアンセ                      | 小渚的婚約對象             |
| 1989/08/21 | 電気仕掛けの御庭番                    | 電力裝置園丁              |
| 1989/09/01 | 月に吠える                        | 對月亮吼叫               |
| 1989/12/21 | ヤギさんとチーズ                     | 與山羊笑一個              |
| 1989/12/27 | ハートをつかめ                      | 捉住那顆心               |
| 1991/06/21 | 乙女ばしかの恐怖                     | 少女麻疹的恐怖             |
| 1991/06/21 | 霊魂とデート                       | 與靈魂約會               |
| 1992/09/21 | メモリアルアルバム アイムTHE終ちゃん         | 我是小終【紀念相簿】          |

### 电影

福星小子同样被拍摄成多部电影（又名剧场版）。下面是所有这些福星电影的一个列表。

|            |               |                                            |                   |
| ---------- | ------------- | ------------------------------------------ | ----------------- |
| **发售日期**   | **日文原名**      | **中文译名**                                   | **英文名**           |
| 1983/02/21 | オンリー·ユー       | 爱星球之恋                                      | Only You          |
| 1984/02/11 | ビューティフル·ドリーマー | [綺麗夢中人](../Page/福星小子2_綺麗夢中人.md "wikilink") | Beautiful Dreamer |
| 1985/01/26 | リメンバー·マイ·ラヴ   | 情侣乐天派                                      | Remember My Love  |
| 1986/02/22 | ラム·ザ·フォーエバー   | 鬼姬传说                                       | Lum The Forever   |
| 1988/02/06 | うる星やつら　完結篇    | 求你说声我爱你                                    | The Final Chapter |
| 1991/08/18 | いつだって·マイ·ダーリン | 圣水奇缘                                       | Always My Darling |

在所有的电影版中，最著名当属1984年发售的Beautiful
Dreamer。臺灣普威爾發行採英文名稱，香港鐳射娛樂僅發行前4部，大陆并没有引进。[美国地区的发行商是Viz](../Page/美国.md "wikilink")。

### 主要声优

下表包括了福星小子中的主要人物的[声优](../Page/声优.md "wikilink")，以及所配音的具体剧目。目前列出日文原版与英文版的声优。香港配音的中文版资料待收集。部分资料翻译自[Wikipedia英文版福星条目](http://en.wikipedia.org/wiki/Urusei_Yatsura)。

|                                                                      |          |                    |
| -------------------------------------------------------------------- | -------- | ------------------ |
| **声优**                                                               | **所配人物** | **配音剧目**           |
| [平野文](../Page/平野文.md "wikilink")（Fumi Hirano）                        | 拉姆       | 原版福星小子全集           |
| [Martha Ellen Senseney](../Page/Martha_Ellen_Senseney.md "wikilink") | 拉姆       | 福星小子英文TV版          |
| [Shannon Settlemyre](../Page/Shannon_Settlemyre.md "wikilink")       | 拉姆       | 福星小子英文电影版1，3，4，5，6 |
| [Ann Ulrey](../Page/Ann_Ulrey.md "wikilink")                         | 拉姆       | 福星小子英文电影版2         |
| [古川登志夫](../Page/古川登志夫.md "wikilink")（Toshio Furukawa）                | 诸星当      | 原版福星小子全集           |
| [Michael Sinterniklaas](../Page/Michael_Sinterniklaas.md "wikilink") | 诸星当      | 福星小子英文TV版          |
| [Steve Rassin](../Page/Steve_Rassin.md "wikilink")                   | 诸星当      | 英文电影版1，3，4，5，6     |
| [Vinnie Penna](../Page/Vinnie_Penna.md "wikilink")                   | 诸星当      | 英文电影版2             |

## 音乐

福星小子在漫画界的巨大成功，以及随后迅速到来的动画，使得福星小子的相关音乐CD或者磁带也大量的推出。并且数量之多，在日本动漫史上也是罕见的。下面先介绍动画片中的主题音乐，接下来是单独发行的音乐CD的介绍（这一部分会按照年代排序），最后单独列出目前收集到的单曲福星CD。需要说明的是，虽然标题是CD，但是这里同样收录了以唱片和磁带为存储介质的福星音乐。

### 动画中的主题音乐

|        |                                          |         |
| ------ | ---------------------------------------- | ------- |
| **类型** | **歌曲名**                                  | **演唱者** |
| OP     | [ラムのラブソング](../Page/拉姆的情歌.md "wikilink")  | 松谷祐子    |
| OP     | Dancing Star                             | 小林泉美    |
| OP     | パジャマ・じゃまだ\!                              | 成清加奈子   |
| OP     | Chance on Love                           | CINDY   |
| OP     | ロック・ザ・プラネット                              | ステファニー  |
| OP     | 殿方ごめん遊ばせ                                 | 南翔子     |
| ED     | [宇宙は大ヘンだ\!](../Page/拉姆的情歌.md "wikilink") | 松谷祐子    |
| ED     | 心細いな                                     | ヘレン笹野   |
| ED     | [星空サイクリング](../Page/星空騎行.md "wikilink")   | ヴァージンVS |
| ED     | I,I,You&愛                                | 小林泉美    |
| ED     | 夢はLove me more                           | 小林泉美    |
| ED     | 恋のメビウス                                   | リッツ     |
| ED     | Open Invitaion                           | CINDY   |
| ED     | エヴリデイ                                    | ステファニー  |
| ED     | Good Luck\~永遠より愛をこめて                     | 南翔子     |

### 单独发行的音乐CD

#### 82年到84年

|                             |                 |             |            |             |
| --------------------------- | --------------- | ----------- | ---------- | ----------- |
| **CD名**                     | **发行日期（月/日/年）** | **歌曲数量**    | **CD编号**   | **存储介质**    |
| Music Capsule               | 4/21/1982       | 38          | PCCG-00146 | Vinyl、磁带、CD |
| Drama Special               | 10/21/1982      | 6           | PCCG-00145 | Vinyl、磁带、CD |
| Only You BGM                | 03/1982         | 39（包含所有BGM） | H30K-20023 | Vinyl、磁带、CD |
| Only You Drama-hen          | 03/1982         | 未知          | 未知         | Vinyl、磁带、CD |
| The Hit Parade              | 7/1983          | 10          | KTCR-1087  | Vinyl、磁带、CD |
| Music Capsule 2             | 21/09/1983      | 18          | PCCG-00147 | Vinyl、磁带、CD |
| Beautiful Dreamer BGM       | 2/10/1984       | 9（包含３首BGM ） | H30K-20024 | Vinyl、磁带、CD |
| Beautiful Dreamer Drama-hen | 2/10/1984       | 未知          | 未知         | Vinyl、磁带、CD |
| Yuko Matsutani Songbook     | 5/21/1984       | 11          | PCCG-00144 | Vinyl、磁带、CD |
| Music Tour                  | 11/10/1984      | 14          | KTCR-1088  | Vinyl、磁带、CD |

#### 85年到86年

|                            |                 |                       |                  |             |
| -------------------------- | --------------- | --------------------- | ---------------- | ----------- |
| **CD名**                    | **发行日期（月/日/年）** | **歌曲数量**              | **CD编号**         | **存储介质**    |
| Remember My Love BGM       | 2/10/1985       | 24（包含11首BGM）          | KTCR-1092        | Vinyl、磁带、CD |
| Remember My Love Drama-hen | 3/5/1985        | 未知                    | 未知               | Vinyl、磁带、CD |
| Juke Box                   | 4/10/1985       | 15                    | H33K-20005       | CD          |
| The Hit Parade 2           | 5/25/1985       | 10                    | KTCR-1089        | Vinyl、磁带、CD |
| Fumi no Lum Song           | 9/21/1985       | 10                    | D32G0011         | Vinyl、磁带、CD |
| Music File                 | 9/25/1985       | 11（Disk 1）、10（Disk 2） | H55K-20010/20011 | Vinyl、磁带、CD |
| Lum the Forever BGM        | 2/21/1986       | 25                    | H30K-20026       | Vinyl、磁带、CD |
| Final Song                 | 3/21/1986       | 未知                    | N/A              | Vinyl       |
| Lum for the World          | 3/21/1986       | 11                    | H30K-20027       | Vinyl、磁带、CD |

#### 86年到88年

|                                   |                 |          |            |             |
| --------------------------------- | --------------- | -------- | ---------- | ----------- |
| **CD名**                           | **发行日期（月/日/年）** | **歌曲数量** | **CD编号**   | **存储介质**    |
| Symphony Urusei-Yatsura           | 4/25/1986       | 6        | H33K-20029 | Vinyl、磁带、CD |
| Fumi no Lum Song 2                | 5/21/1986       | 10       | D32G0031   | Vinyl、磁带、CD |
| Juke Box 2                        | 12/5/1986       | 15       | H33K-20063 | CD          |
| Super Best 20                     | 12/5/1986       | 未知       | N/A        | 磁带          |
| Non-Stop                          | 3/5/1987        | 未知       | N/A        | 磁带          |
| Urusei Yatsura '87 natsu event-yo | 1/7/1987        | 未知       | N/A        | Vinyl       |
| Telephone Card Special            | 7/21/1987       | 未知       | N/A        | 磁带          |
| Forever Best Fumi Hirano          | 2/21/1988       | 16       | D32P-6132  | 磁带、CD       |
| Kanketsu-hen BGM                  | 2/21/1988       | 21       | H30K-20115 | Vinyl、磁带、CD |
| Kanketsu-hen Drama-hen            | 3/21/1988       | 未知       | 未知         | Vinyl、磁带、CD |
| Headphone Stereo Select           | 5/21/1988       | 未知       | 未知         | 磁带          |

#### 86年到91年

|                             |                 |                       |              |          |
| --------------------------- | --------------- | --------------------- | ------------ | -------- |
| **CD名**                     | **发行日期（月/日/年）** | **歌曲数量**              | **CD编号**     | **存储介质** |
| CD Single Memorial File     | 7/11/1988       | 50                    | KACD-0001/24 | 磁带、CD    |
| Music Calendar '89          | 12/1988         | 12                    | KACD-0025    | CD       |
| Music Calendar '90          | 11/1989         | 未知                    | N/A          | 磁带       |
| Music Calendar '91          | 11/1990         | 未知                    | N/A          | 磁带       |
| Juunen Kinen Album Datcha\! | 8/18/1991       | 2                     | PCCG-00143   | 磁带、CD    |
| Gold Disc                   | 8/1991          | 15（Disk 1）、15（Disk 1） | ACD-0028\~29 | 磁带、CD    |
| Itsudatte My Darling BGM    | 10/25/1991      | 48                    | KTTR-1128    | 磁带、CD    |
| Itsudatte my Darling Drama  | 12/21/1991      | 未知                    | 未知           | CD       |
| Shudai-ka Best 10 Remix     | 12/25/1991      | 12                    | KTCR-1131    | CD       |
| Music Calendar '92          | 12/25/1991      | 12                    | KACD-0030    | CD       |

#### 92年到99年

|                             |                 |          |              |          |
| --------------------------- | --------------- | -------- | ------------ | -------- |
| **CD名**                     | **发行日期（月/日/年）** | **歌曲数量** | **CD编号**     | **存储介质** |
| Music Calendar '93          | 12/21/1992      | 5        | KACD-0031    | CD       |
| Juunen Kinen Album Datcha\! | 12/12/1993      | 21       | 未知           | CD       |
| Complete Music Box          | 未知              | 15 CDs   | KTCR-9018/30 | CD       |
| Dear My Friends             | 未知              | 9        | 未知           | CD       |
| Lum's Best Selection 2      | 1993            | 11       | KTCR-1297    | CD       |
| Music Calendar '95          | 10/01/1994      | 10       | KACD-0032    | CD       |
| TV Theme Song Best          | 3/17/1999       | 15       | 未知           | CD       |

#### 单曲CD

|              |                 |                                             |           |             |
| ------------ | --------------- | ------------------------------------------- | --------- | ----------- |
| **CD名**      | **发行日期（月/日/年）** | **歌曲数量**                                    | **CD编号**  | **存储介质**    |
| Stars On     | 8/8/1988        | 1                                           | KACD-0026 | Vinyl、CD    |
| BEGIN THE 綺麗 | 1993            | 1（[UL-SAYS](../Page/UL-SAYS.md "wikilink")） | ESDB 3264 | Vinyl、磁带、CD |
| 承诺／星光        | 25/04/2006（预定）  | 1（[倖田来未](../Page/倖田来未.md "wikilink")）       | ???       | CD、DVD      |

## 游戏

由于福星小子的走紅，日本的众多游戏厂商也制作了大量的福星小子游戏。这些游戏基本上都是[AVG类型](../Page/AVG.md "wikilink")，这部分的是由于福星小子所营造的氛围本身就适合被改编成AVG类型的游戏。下面是这些游戏的一个列表。
[fuxinggame.jpg](https://zh.wikipedia.org/wiki/File:fuxinggame.jpg "fig:fuxinggame.jpg")

|                           |                                                                           |                                                  |                                   |          |
| ------------------------- | ------------------------------------------------------------------------- | ------------------------------------------------ | --------------------------------- | -------- |
| **游戏名**                   | **发行商**                                                                   | **系统**                                           | **游戏类型**                          | **发售日期** |
| Ryoko's Survival Birthday | [Microcabin](../Page/Microcabin.md "wikilink")                            | [MSX2](../Page/MSX2.md "wikilink")               | [AVG](../Page/冒险游戏.md "wikilink") | 1987     |
| Lum no Wedding Bell       | [Jaleco](../Page/Jaleco.md "wikilink")                                    | [FC](../Page/FC遊戲機.md "wikilink")                | [STG](../Page/STG.md "wikilink")  | 1986     |
| Deay My Friends           | [Game Arts](../Page/Game_Arts.md "wikilink")                              | [Mega CD](../Page/Mega_CD.md "wikilink")         | AVG                               | 1993     |
| Miss Tomobiki o Sagase\!  | [Kid](../Page/Kid.md "wikilink")/[Yanoman](../Page/Yanoman.md "wikilink") | [Game Boy](../Page/Game_Boy.md "wikilink")       | AVG                               | 1992     |
| Stay With You             | [Hudson](../Page/Hudson_Soft.md "wikilink")                               | [PC Engine](../Page/PC_Engine.md "wikilink")     | AVG                               | 1992     |
| Endless Summer            | [Marvelous Interactive](../Page/Marvelous_Interactive.md "wikilink")      | [Nintendo DS](../Page/Nintendo_DS.md "wikilink") | AVG                               | 2005     |

## 註釋

## 各地播放日期

## 外部連結

  -
  - [友引町Tomobiki-Cho](http://www.furinkan.com/uy/)

  - [Rumic World](http://www.furinkan.com/)

  - [福星小子图片库 Urusei Yatsura Photo Gallary](http://urusei-yatsura.com/)

  - [福星小子维基](http://zh.uruseiyatsura.wikia.com/)

[Category:福星小子](../Category/福星小子.md "wikilink")
[Category:高橋留美子](../Category/高橋留美子.md "wikilink")
[Category:高橋留美子原作改編動畫作品](../Category/高橋留美子原作改編動畫作品.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:週刊少年Sunday](../Category/週刊少年Sunday.md "wikilink")
[Category:戀愛漫畫](../Category/戀愛漫畫.md "wikilink")
[Category:科幻動畫](../Category/科幻動畫.md "wikilink")
[Category:1981年日本電視動畫](../Category/1981年日本電視動畫.md "wikilink")
[Category:富士電視台動畫](../Category/富士電視台動畫.md "wikilink") [Category:Studio
Pierrot](../Category/Studio_Pierrot.md "wikilink")
[Category:1985年日本OVA動畫](../Category/1985年日本OVA動畫.md "wikilink")
[Category:1986年日本OVA動畫](../Category/1986年日本OVA動畫.md "wikilink")
[Category:1987年日本OVA動畫](../Category/1987年日本OVA動畫.md "wikilink")
[Category:1988年日本OVA動畫](../Category/1988年日本OVA動畫.md "wikilink")
[Category:1989年日本OVA動畫](../Category/1989年日本OVA動畫.md "wikilink")
[Category:1991年日本OVA動畫](../Category/1991年日本OVA動畫.md "wikilink")
[Category:2010年日本OVA動畫](../Category/2010年日本OVA動畫.md "wikilink")
[Category:1983年日本劇場動畫](../Category/1983年日本劇場動畫.md "wikilink")
[Category:1984年日本劇場動畫](../Category/1984年日本劇場動畫.md "wikilink")
[Category:1985年日本劇場動畫](../Category/1985年日本劇場動畫.md "wikilink")
[Category:1986年日本劇場動畫](../Category/1986年日本劇場動畫.md "wikilink")
[Category:1988年日本劇場動畫](../Category/1988年日本劇場動畫.md "wikilink")
[Category:1991年日本劇場動畫](../Category/1991年日本劇場動畫.md "wikilink")
[Category:亞洲電視外購動畫](../Category/亞洲電視外購動畫.md "wikilink")
[Category:台視外購動畫](../Category/台視外購動畫.md "wikilink")
[Category:外星生命入侵地球題材漫畫](../Category/外星生命入侵地球題材漫畫.md "wikilink")
[Category:外星生命題材漫畫](../Category/外星生命題材漫畫.md "wikilink")
[Category:外星生命題材動畫](../Category/外星生命題材動畫.md "wikilink")
[Category:練馬區背景作品](../Category/練馬區背景作品.md "wikilink")

1.  ，如同中文破口大罵「吵死了」，直譯即「吵死人的傢伙」 。