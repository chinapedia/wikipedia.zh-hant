[Openmotif_screenshot.png](https://zh.wikipedia.org/wiki/File:Openmotif_screenshot.png "fig:Openmotif_screenshot.png")

在计算机应用中，**Motif**是指一个[图形用户界面规格](../Page/图形用户界面.md "wikilink")，也指随该规格，在[Unix与其他](../Page/Unix.md "wikilink")[POSIX系统中运行于](../Page/POSIX.md "wikilink")[X
Window系统的](../Page/X_Window系统.md "wikilink")[部件工具箱](../Page/部件工具箱.md "wikilink")。1980年代，随同Unix[工作站逐渐通用](../Page/工作站.md "wikilink")，成为[OPEN
LOOK图形用户界面的对手](../Page/OPEN_LOOK.md "wikilink")。目前作为[Common Desktop
Environment的基本构件](../Page/Common_Desktop_Environment.md "wikilink")。

Motif应用程序接口由IEEE
1295[标准规定](../Page/标准.md "wikilink")。从版本2.1后，Motif有支持[Unicode](../Page/Unicode.md "wikilink")，因而在几种多语言环境，受到广泛的使用。

用户界面部件（如菜单，按钮，滑动器，文字框等）的方形，凿刻，立体外表组成Motif的特殊界面外观。由于特意设计，Motif的操作方式相当于当时常见的[微软视窗](../Page/微软视窗.md "wikilink")3.11与[Presentation
Manager界面](../Page/Presentation_Manager.md "wikilink")。而且，[微软在原本格式指南设计有起作用](../Page/微软.md "wikilink")。

Motif是Open Software Foundation创造的（有时也被称为OSF/Motif）。[Open Software
Foundation现在已纳入](../Page/Open_Software_Foundation.md "wikilink")[The
Open Group](../Page/The_Open_Group.md "wikilink")。

Motif应用程序接口的[实现不多](../Page/实现.md "wikilink")。Motif的工具箱是第一个。也有[Open
Motif](../Page/Open_Motif.md "wikilink")，此实现是原先的Motif在更为自由的许可证下发布。最后，[LessTif计划企图在](../Page/LessTif.md "wikilink")[LGPL下实现Motif的应用程序接口](../Page/LGPL.md "wikilink")。

## 参见

  - [MoOLIT](../Page/MoOLIT.md "wikilink")
  - [GTK+与](../Page/GTK+.md "wikilink")[GNOME](../Page/GNOME.md "wikilink")
  - [Qt与](../Page/Qt.md "wikilink")[KDE](../Page/KDE.md "wikilink")
  - [Integrated Computer
    Solutions](../Page/Integrated_Computer_Solutions.md "wikilink")
  - [4Dwm](../Page/4Dwm.md "wikilink")

## 外部链接

  - [Motif首页](http://www.opengroup.org/motif/)
  - [Open Motif计划网页](http://www.openmotif.org/)
  - [Motif FAQ](http://www.rahul.net/kenton/mfaq.html) (Kenton Lee)
  - [Motif: Volumes 6A and 6B](http://www.oreilly.com/openbook/motif/)
    (O'Reilly and Associates)

[Category:部件工具箱](../Category/部件工具箱.md "wikilink") [Category:X
视窗管理器](../Category/X_视窗管理器.md "wikilink")
[Category:Motif](../Category/Motif.md "wikilink")