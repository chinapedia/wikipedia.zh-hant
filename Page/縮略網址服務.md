**縮址**，又稱**短址**、**短网址**、**網址縮短**、**縮短網址**、**URL缩短**等，指的是一种[互联网上的技术与服务](../Page/互联网.md "wikilink")。此服务可以提供一个非常短小的[URL以代替原来的可能较长的URL](../Page/URL.md "wikilink")，將長的[URL位址縮短](../Page/URL.md "wikilink")。

用户访问缩短后的URL时，通常将会[重定向到原来的URL](../Page/網域名稱轉址.md "wikilink")。

大多数的URL缩短服务都提供有[API](../Page/API.md "wikilink")。URL缩短服务在[Twitter等一些每条消息有字数限制的](../Page/Twitter.md "wikilink")[微博客及其他](../Page/微博客.md "wikilink")[社交網絡中有广泛的使用](../Page/社交網絡.md "wikilink")。

## 原因

由於某些类似于[Twitter的](../Page/Twitter.md "wikilink")[微博客服务对于每条贴子或消息有字数限制](../Page/微博客.md "wikilink")（多为140字）。某些[BBS文章超過一行](../Page/BBS.md "wikilink")78個字元時，也會造成一些會自動為網址加上[超連結的](../Page/超連結.md "wikilink")[Telnet及BBS軟體無法正確執行該動作](../Page/Telnet.md "wikilink")，因此需要透過縮短網址的功能來達到網址縮短的目的。

縮址另外也有方便使用者記憶及傳送網址的功能，短址可將太長的網址轉換成15個字以內的替代網址，也有部分網站提供自訂名稱以及密碼保護的功能，可以讓使用者取得更有自己風格的短網址。

## 方法

利用[鍵值對應方式](../Page/鍵值存儲.md "wikilink")，將網址對應成被縮短的代碼來散佈使用。

使用時，系統先查詢出原本的網址，再以[網址重導向](../Page/網域名稱轉址.md "wikilink")（）来将缩短后的地址重定向到原来的URL。

## 历史

2002年1月开始的[TinyURL是最早的URL缩短服务](../Page/TinyURL.md "wikilink")。建立此服务的想法则要追溯到至少2001年\[1\]。

TinyURL影响到了后来的至少100个URL缩短服务\[2\]。其后的服务大多数提供直接的网址重定向；有些也可以提供一些额外的服务，如统计点击数等；还有些则使用网页框架，于缩短后的网址建立带-{框}-架的网页，将目标页放于-{框}-架内，而并非使用直接的网址重定向。

[托克勞](../Page/托克勞.md "wikilink")[國家及地區頂級域](../Page/國家及地區頂級域.md "wikilink")[.tk曾经可以提供较短的纪念性顶级域名生成服务](../Page/.tk.md "wikilink")。2009年5月，其开设了**Tweak.tk**\[3\]，以提供更为短小的顶级域名生成。

## 安全性問題

由於縮短網址具有隱匿原始網址的特性，使用者無法透過短網址的「外觀」得知真實網址，因此短網址便成為有心人士利用來散播[電腦病毒及](../Page/電腦病毒.md "wikilink")[有害內容的管道](../Page/有害內容.md "wikilink")，成為近年來[社交工程攻擊的慣用工具之一](../Page/社交工程.md "wikilink")。

目前已經有部分短網址服務整合資訊安全廠商提供的網址檢查機制，提供使用者在點擊短網址時預覽真實網址以進行及安全性檢查，透過即時的檢查機制降低使用者暴露在資訊安全風險的機會。

## 獲利機制

部分短網址會在目標URL之前生成一個重定向頁，並在頁面內放置廣告以獲取利潤，當訪客點入短網址後，必須要在充滿廣告的頁面內點擊特定按鈕才能抵達目的地；這類型網頁通常會造成訪客的不快，因此需要搭配分潤機制來吸引流量提供商使用這樣的縮網址。

## 網址縮短服務

| 網域                                               | 網站名稱                                                               | 簡介                                                                               | 自訂義別名 | 營運資金來源              |
| ------------------------------------------------ | ------------------------------------------------------------------ | -------------------------------------------------------------------------------- | ----- | ------------------- |
| [ouo.tw](https://ouo.tw/)                        | ouo.tw 表情短網址                                                       | 免費設置網址到期日、密碼、[QR碼以及Google](../Page/QR碼.md "wikilink")、FB像素等多種進階功能                | 是     | 廣告、捐款、專業版、大部份功能免費使用 |
| [ppt.cc](../Page/ppt.cc.md "wikilink")           | [來個PPT短網址](../Page/來個PPT短網址.md "wikilink")                         | 台灣老牌知名短網址服務                                                                      | 否     | 未知                  |
| [bit.ly](../Page/bit.ly.md "wikilink")           | [bit.ly](../Page/bit.ly.md "wikilink")                             | 知名短網址服務，在 Twitter 等微部落格網站上很流行                                                    | 是     | 廣告、企業方案、商業合作        |
| [PicSee](../Page/PicSee.md "wikilink")           | [pics.ee](https://picsee.co)                                       | 為社群而生的短網址服務，可為短網址設定在社群上出現之縮圖及標題，埋放FB Pixel及GTM等第三方追蹤及供品牌網域，是許多知名網紅(如蔡阿嘎)常用的短網址服務 | 是     | 專業版、企業合作            |
| [lihi.io](../Page/lihi.io.md "wikilink")         | [lihi.io](https://app.lihi.io/)                                    | 提供AB分流測試, QR Code 創立, 自訂網域, 網址內埋GTM跟臉書像素等進階功能                                    | 是     | 專業版、企業方案、部份功能免費提供   |
| [goo.gl](../Page/goo.gl.md "wikilink")           | [Google URL Shortener](../Page/Google_URL_Shortener.md "wikilink") | Google創立的簡便短網址服務                                                                 | 否     | 免費提供                |
| [t.co](../Page/t.co.md "wikilink")               | Twitter                                                            | 該服務僅供縮短發布到Twitter的連結                                                             | 否     | 免費提供                |
| [tinyurl.com](../Page/tinyurl.com.md "wikilink") | TinyURL                                                            | 第一個專門提供縮略網址服務的網站                                                                 | 是     | 捐款、廣告               |
| [youtu.be](../Page/youtu.be.md "wikilink")       | YouTube                                                            | YouTube分享影片用短網址                                                                  | 否     | 免費提供                |
| t.cn                                             | [新浪微博](../Page/新浪微博.md "wikilink")                                 |                                                                                  |       |                     |
| c.tb.cn                                          | [淘寶](../Page/淘寶.md "wikilink")                                     |                                                                                  |       |                     |
| dwz.cn                                           | [百度](../Page/百度.md "wikilink")                                     |                                                                                  |       |                     |
| url.cn                                           | [騰訊微博](../Page/騰訊微博.md "wikilink")\[4\]                            | w.url.cn [微信](../Page/微信.md "wikilink")                                          |       |                     |

## 参见

  - [網域名稱轉址](../Page/網域名稱轉址.md "wikilink")
  - [URL重寫](../Page/URL重寫.md "wikilink")

## 參考

[Category:Web服务](../Category/Web服务.md "wikilink")

1.  [Comment thread 8916](http://www.metafilter.com/8916/),
    *Metafilter.com*, [10 June](../Page/10_June.md "wikilink") 2001.
    Announcement of url shortening service available at
    makeashorterlink.com.
2.  [90+ URL Shortening
    Services](http://mashable.com/2008/01/08/url-shortening-services/),
    *Mashable.Com*, [8 January](../Page/8_January.md "wikilink") 2008,
    page 84
3.  <http://twitter.com/TweaKdotTK/status/1834883583>
4.  <https://www.ft12.com/tags-12.html>