[KB_United_States_Dvorak.svg](https://zh.wikipedia.org/wiki/File:KB_United_States_Dvorak.svg "fig:KB_United_States_Dvorak.svg")

**德沃夏克[鍵盤](../Page/鍵盤.md "wikilink")**（Dvorak Keyboard、Dvorak Simplified
Keyboard）是[鍵盤排列方式](../Page/鍵盤布局.md "wikilink")，由[奥古斯特·德沃夏克和](../Page/奥古斯特·德沃夏克.md "wikilink")[威廉·迪利在](../Page/威廉·迪利.md "wikilink")1930年代期間設計，是主流的鍵盤排列[快蹄鍵盤](../Page/QWERTY鍵盤.md "wikilink")（QWERTY）的對手。

## 概觀

[Apple_iicb.jpg](https://zh.wikipedia.org/wiki/File:Apple_iicb.jpg "fig:Apple_iicb.jpg")
德沃夏克和Dealey研究過[字母頻率和手的](../Page/字母頻率.md "wikilink")[生理之後](../Page/生理學.md "wikilink")，依以下的宗旨來建立這個排列：

  - 以使用英語為設計出發觀點
  - 能讓雙手交互輸入
  - 為了達至最高速度和效率，最常用的字母和[二合字母應最易輸入](../Page/二合字母.md "wikilink")——它們應該在中排，食指放在的地方。
  - 最少用的字母應在最難碰到的下排
  - 右手應打得更多字，因為大部分的人都是[右撇子](../Page/右撇子.md "wikilink")
  - 使用連着的手指打[二合字母比不相連的更難](../Page/二合字母.md "wikilink")。
  - 輸入應由邊緣循序漸進地移到中心

此排列於1932年完成，並在1936年得到[U.S. Patent
No. 2,040,248](http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=/netahtml/srchnum.htm&r=1&f=G&l=50&s1=2,040,248.WKU.&OS=PN/2,040,248&RS=PN/2,040,248)。

在1984年，德沃夏克鍵盤估計有100,000個使用者。

## 改變的阻力

使用德沃夏克鍵盤时，键入应用软件默认的快捷键（例如要[複製的Ctrl](../Page/複製.md "wikilink")+C，以及要[貼上的Ctrl](../Page/貼上.md "wikilink")+V，德沃夏克鍵盤的C鍵跟V鍵距離Ctrl鍵都太遠了），或操作键，可能会感到不舒服。因为很多应用程序都假设用户使用QWERTY键盘。例如[vi編輯器假設H](../Page/vi.md "wikilink")、J、K和L在中排。有些[電腦遊戲](../Page/電腦遊戲.md "wikilink")（例如[創世神與](../Page/創世神.md "wikilink")[鬥陣特攻](../Page/鬥陣特攻.md "wikilink")）用WSAD（上下左右，在QWERTY键盘）来控制人物的行动（[英雄聯盟則是用](../Page/英雄聯盟.md "wikilink")[滑鼠右鍵來控制行動](../Page/滑鼠.md "wikilink")，用QWERDF六個鍵來發動技能（QWER為攻擊技能，DF為防禦技能，數字鍵1到7（4鍵為插眼用）發動為主動技能（非被動技能）的道具（例如中婭沙漏），A鍵為普攻，B鍵為回城）。不过所幸大部分的電腦遊戲都允許使用者自行決定鍵位。

目前，世界上最快的英文打字速度是在Dvorak键盘上创造的。根据《[吉尼斯世界记录大全](../Page/吉尼斯世界记录大全.md "wikilink")》，Barbara
Blackburn是目前世界上最快的打字员。2005年，她在Dvorak键盘上连续打字50分钟，平均每分钟150个词，峰值速度为每分钟212个单词。

## 其他德沃夏克布局方案

[thumb使用的德沃夏克鍵盤](../Page/file:Dv3rak.png.md "wikilink")\]\]
[350px使用的](../Page/file:KB_Svorak.svg.md "wikilink")**Svorak**鍵盤\]\]

德沃夏克博士原考虑將數字鍵的數字排列改變為7-5-3-1-9-0-2-4-6-8，因為他相信這個排法更有效。这种数字排法被使用在programmer
dvorak中。

Programmer Dvorak键盘中，数字键变为特殊符号键，便于输入代码中常用的特殊符号。原本的数字需要按下Shift输入。\[1\]

另外，以德沃夏克鍵盤為根本的鍵盤排列亦為其他非英語的語言建立起來。例如給瑞典語使用的**Svorak**，將三個[瑞典語](../Page/瑞典語.md "wikilink")[元音放在左上角](../Page/元音.md "wikilink")。

## 德沃夏克鍵盤單手版

[KB_Dvorak_Left.svg](https://zh.wikipedia.org/wiki/File:KB_Dvorak_Left.svg "fig:KB_Dvorak_Left.svg")
[KB_Dvorak_Right.svg](https://zh.wikipedia.org/wiki/File:KB_Dvorak_Right.svg "fig:KB_Dvorak_Right.svg")

德沃夏克鍵盤亦有特別為左手、右手单手打字設計的布局，左右手的排列各一。美军Robert
Allen上校在[第二次世界大戰中失掉了右臂](../Page/第二次世界大戰.md "wikilink")，他便找[奥古斯特·德沃夏克協助](../Page/奥古斯特·德沃夏克.md "wikilink")，德沃夏克便建立了這兩套排法。兩部007電影出現過這種單手打字：《[明日帝國](../Page/明日帝國.md "wikilink")》中一個富商和《[黄金眼](../Page/黄金眼.md "wikilink")》中一個[俄羅斯電腦](../Page/俄羅斯.md "wikilink")[駭客身上](../Page/黑客.md "wikilink")。德沃夏克鍵盤單手版的打字設計排法，為左右手的排列法各一種；德沃夏克鍵盤左手版、德沃夏克鍵盤右手版。

## 各式德沃夏克排列型態

### 一般鍵盤排列

```
   '  ,  .  p  y  f  g  c  r  l  / =
    a  o  e  u  i  d  h  t  n  s  -
     ;  q  j  k  x  b  m  w  v  z

   "  <  >  P  Y  F  G  C  R  L  ?  +
    A  O  E  U  I  D  H  T  N  S  _
     :  Q  J  K  X  B  M  W  V  Z
```

### [世界語鍵盤排列](../Page/世界語.md "wikilink")

```
   '  ,  .  p  ĝ  f  g  c  r  l  ĵ  !
    a  o  e  u  i  d  h  t  n  s  -
     ĥ  ĉ  j  k  ŝ  b  m  ŭ  v  z

   "  (  )  P  Ĝ  F  G  C  R  L  Ĵ  ?
    A  O  E  U  I  D  H  T  N  S  -
     Ĥ  Ĉ  J  K  Ŝ  B  M  Ŭ  V  Z
```

### [德語鍵盤排列](../Page/德語.md "wikilink")

```
   ü  ,  .  p  y  f  g  c  t  z  ?  /
    a  o  e  i  u  h  d  r  n  s  l  -
  ä  ö  q  j  k  x  b  m  w  v  #

   Ü  ;  :  P  Y  F  G  C  T  Z  ß  \
    A  O  E  I  U  H  D  R  N  S  L  _
  Ä  Ö  Q  J  K  X  B  M  W  V  '
```

## 参考文献

## 參見

  - [鍵盤佈局](../Page/鍵盤佈局.md "wikilink")
  - [QWERTY鍵盤](../Page/QWERTY鍵盤.md "wikilink")

## 外部連結

  - [Dvorak與行列輸入法，Jedi](http://jedi.org/blog/archives/002990.html)
  - [DvZine.org](http://www.dvzine.org/) – A print and
    [webcomic](../Page/網絡漫畫.md "wikilink")
    [zine](../Page/zine.md "wikilink") advocating the Dvorak Keyboard
    and teaching its history.
  - [A Basic Course in Dvorak](http://www.gigliwood.com/abcd/) – by Dan
    Wood
  - [Dvorak your
    way](https://web.archive.org/web/20170901111552/https://typeyourway.com/node/1)
    with by Dan Wood and Marcus Hayward
  - [1](http://mkweb.bcgsc.ca/carpalx/?popular_alternatives) –
    Comparison of common optimal keyboard layouts, including Dvorak.
  - [2](https://scienceteaser.com/2018/12/technology-facts) - A list of
    interesting Technology Facts, one of which the Dvorak Keyboard.

[Category:鍵盤配置](../Category/鍵盤配置.md "wikilink")

1.