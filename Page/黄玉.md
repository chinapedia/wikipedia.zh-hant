  -
    <small>*黄宝石也是兩部电影的名字。参见[黄宝石
    (1945年电影)和](../Page/黄宝石_\(1945年电影\).md "wikilink")[黄宝石
    (1969年电影)](../Page/黄宝石_\(1969年电影\).md "wikilink")。*</small>

**黄玉**（英语：topaz），也称呼为**黄宝石**、**托帕石**，為含有鋁和氟的硅酸盐[矿物是](../Page/矿物.md "wikilink")11月的[诞生石](../Page/诞生石.md "wikilink")，分佈於巴西、墨西哥、薩克森、蘇格蘭、日本、烏拉山脈及其他地方。

## 性状

纯的**黄玉**透明，但常因其中的杂质引起不透明。典型的黄玉为葡萄酒色或淡黄色。但也有可能是白色，灰色，蓝色，绿色的。無色的黃玉在切割精良下，可能被誤認為是鑽石。有色的黃玉可能較不穩定或因日照而褪色\[1\]。目前珠宝业在研究使用[物理蒸汽沉淀法](../Page/物理蒸汽沉淀法.md "wikilink")（Physical
Vapor Deposition）在黄玉上形成[二氧化钛薄膜](../Page/二氧化钛.md "wikilink")，从而生产彩虹色黄玉。

## 晶体与化学结构

**黄玉**密度为3.49-3.57克/立方厘米，硬度为8，属[正交晶系](../Page/正交晶系.md "wikilink")。其[化学式为](../Page/化学式.md "wikilink")[Al](../Page/铝.md "wikilink")<sub>2</sub>[Si](../Page/硅.md "wikilink")[O](../Page/氧.md "wikilink")<sub>4</sub>([F](../Page/氟.md "wikilink"),[OH](../Page/OH.md "wikilink"))。成分中F和(OH)的比值变化不定。

<center>

TopazEZ2.jpg| Topaz-2.jpg TOPAZE9.jpg

</center>

## 参见

  - [黄龙玉](../Page/黄龙玉.md "wikilink")

## 參考

  - [黄玉 - Webmineral](http://webmineral.com/data/Topaz.shtml)
  - [黄玉 - Mindat](http://www.mindat.org/show.php?id=3996&ld=1&pho=)

[Category:含铝矿物](../Category/含铝矿物.md "wikilink")
[Category:矽酸鹽礦物](../Category/矽酸鹽礦物.md "wikilink")
[Category:寶石](../Category/寶石.md "wikilink")
[Category:岛状硅酸盐](../Category/岛状硅酸盐.md "wikilink")

1.  [藝術與建築索引典—黃玉](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300011162)
    於2010年11月4日查閱