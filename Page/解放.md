「**解放**」（），指摆脱束缚、压迫的一种状态，包括[精神与](../Page/精神.md "wikilink")[物质两方面](../Page/物质.md "wikilink")。如[奴隶解放运动](../Page/奴隶解放.md "wikilink")，使人获得[自由](../Page/自由.md "wikilink")。

## 精神方面

“精神解放”指摆脱精神的桎梏，如[政治](../Page/政治.md "wikilink")、[宗教给人的枷锁](../Page/宗教.md "wikilink")。

## 物质方面

物质解放，通过改善人民物质生活条件，使人获得更多的自由，常与生产力水平的提高相适应。如通过[工业革命之后](../Page/工业革命.md "wikilink")，随着[机械化水平的提高](../Page/机械化.md "wikilink")，使人摆脱繁重的体力劳动，同时使人有更多时间享受物质文明的成果，促进[社会](../Page/社会.md "wikilink")[文化的发展](../Page/文化.md "wikilink")。

## 詞彙爭議

「解放」亦是中國大陸常用術語，常見[中國共產黨用於形容某些事件](../Page/中國共產黨.md "wikilink")，例如[中国人民解放军對](../Page/中国人民解放军.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[政府控制地區的佔領](../Page/中華民國政府.md "wikilink")，被中共稱為「解放」，[第二次国共内战被称为](../Page/第二次国共内战.md "wikilink")“[解放战争](../Page/解放战争.md "wikilink")”。但是香港學者[陳雲根認為](../Page/陳雲_\(學者\).md "wikilink")：[中共中央宣傳部善於生產這類詞彙](../Page/中共中央宣傳部.md "wikilink")，以擄略人心。當義軍瓦解了暴政或敵外政權，古文稱為「重光、[光復](../Page/光復.md "wikilink")、收復失地、還我河山」，但是「解放」就絕對稱不上———因為在[國共內戰](../Page/國共內戰.md "wikilink")，受到[蘇聯資助和支配的中共在](../Page/蘇聯.md "wikilink")1949年推翻的[國民政府並非敵外政權](../Page/國民政府.md "wikilink")。又如[二次大戰期間](../Page/二次大戰.md "wikilink")，盟軍攻陷[納粹](../Page/納粹.md "wikilink")[德國統治之地](../Page/德國.md "wikilink")，及攻破關鎖[猶太人之](../Page/猶太人.md "wikilink")[集中營](../Page/集中營.md "wikilink")，猶太人重獲自由，才能稱之「解放」。而[盟軍](../Page/同盟國_\(第二次世界大戰\).md "wikilink")「解放」了納粹德國的佔領區，人民是紛紛回國定居，而不是像中國人般往外逃難的。\[1\]
在[中国大陆](../Page/中国大陆.md "wikilink")，以“解放”作为地名或路名的地方有成千上百处，例如[解放路](../Page/解放路.md "wikilink")、[解放街道等](../Page/解放街道.md "wikilink")。[广州的中华路在](../Page/广州.md "wikilink")[中華人民共和國建國後就被改成了](../Page/中華人民共和國.md "wikilink")“[解放路](../Page/解放路_\(广州市\).md "wikilink")”。

## 參考文獻

[Category:自由主義](../Category/自由主義.md "wikilink")

1.  [解放與建政](https://hk.news.yahoo.com/blogs/sandwich/%E8%A7%A3%E6%94%BE%E8%88%87%E5%BB%BA%E6%94%BF.html)，雅虎香港，陳雲
    三文治，2009年11月11日