[201612_Guixi_Smelter_of_Jiangxi_Copper_Corporation.jpg](https://zh.wikipedia.org/wiki/File:201612_Guixi_Smelter_of_Jiangxi_Copper_Corporation.jpg "fig:201612_Guixi_Smelter_of_Jiangxi_Copper_Corporation.jpg")
**江西銅業股份有限公司**，簡稱**江銅**、**江西銅**，是一家在[香港交易所](../Page/香港交易所.md "wikilink")
(,)和[上海证券交易所](../Page/上海证券交易所.md "wikilink")
()上市的工業公司。它是[中國最大的銅業公司](../Page/中國.md "wikilink")，主要業務是採礦、選礦、熔煉與精煉，生產陰[銅及副產品](../Page/銅.md "wikilink")，包括硫精礦、[硫酸及](../Page/硫酸.md "wikilink")[電解](../Page/電解.md "wikilink")[金和](../Page/金.md "wikilink")[銀](../Page/銀.md "wikilink")。拥有的主要矿山有[德兴铜矿](../Page/德兴铜矿.md "wikilink")、武山铜矿、永平铜矿等。公司在[中華人民共和國註冊](../Page/中華人民共和國.md "wikilink")，现任董事长為[龙子平](../Page/龙子平.md "wikilink")，總部在[江西省](../Page/江西省.md "wikilink")[南昌市](../Page/南昌市.md "wikilink")。2004年[資產淨值為RMB](../Page/資產淨值.md "wikilink")
5,956,080,000，[純利為RMB](../Page/純利.md "wikilink") 1,108,139,000。

百利大（0495）公布，於2013年12月20日與江西銅業訂立臨時買賣協議，出售灣仔會展廣場辦公大樓45樓01室的總辦事處予江西銅業，代價3.3657億元，預期出售事項變現除稅前收益約2.62億元，將於（27日）復牌。於交易完成後，百利大將繼續於山頂道8、10及12號再發展一個物業項目。

江西銅業股份有限公司的大股東為江西铜业集團。江西铜业集團是[江西省人民政府國有資產監督管理委員會全資附屬公司](../Page/江西省人民政府國有資產監督管理委員會.md "wikilink")。\[1\]

## 参考文献

## 連結

  - [江西銅業集團](http://www.jxcc.com/)

[Category:中华人民共和国国有企业](../Category/中华人民共和国国有企业.md "wikilink")
[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市工業公司](../Category/香港上市工業公司.md "wikilink")
[Category:金屬公司](../Category/金屬公司.md "wikilink")
[Category:江西公司](../Category/江西公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:香港交易所上市牛熊證](../Category/香港交易所上市牛熊證.md "wikilink")
[Category:铜业公司](../Category/铜业公司.md "wikilink")
[Category:1979年中國建立](../Category/1979年中國建立.md "wikilink")

1.  [2014年年報](http://www.hkexnews.hk/listedco/listconews/SEHK/2015/0423/LTN201504231329_C.pdf)江西銅業股份