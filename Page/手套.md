[Handschoenen-nappaleer0859.JPG](https://zh.wikipedia.org/wiki/File:Handschoenen-nappaleer0859.JPG "fig:Handschoenen-nappaleer0859.JPG")

**手套**是包裹[手的服饰或保护器材](../Page/手.md "wikilink")。作用有：手部保暖、装饰、宗教用途、保护手免遭伤害、隔离手部，特殊的手套也是体育运动的器材。

## 历史

### 中国历史

手套在中国古代并不很普遍。一个原因是[汉服的袖子宽大](../Page/汉服.md "wikilink")，可以遮挡暖手，或者双手抄在胸前的袖筒内。

但[湖北省](../Page/湖北省.md "wikilink")[江陵县出土过](../Page/江陵县.md "wikilink")[战国中期墓葬中的皮手套](../Page/戰國_\(中國\).md "wikilink")\[1\]。另外，[马王堆汉墓中的](../Page/马王堆汉墓.md "wikilink")[西汉女尸手上戴有丝绣的无指手套](../Page/西汉.md "wikilink")\[2\]。

### 西方历史

手套在古希腊、罗马偶有记载。从13世纪起，欧洲的女性开始流行戴手套为装饰。这些手套一般是[亚麻布或](../Page/亚麻布.md "wikilink")[丝绸质地](../Page/丝绸.md "wikilink")，可以长达手腕或肘部，亦多有以動物皮革製作，同時為掩蓋皮革的氣味而將皮手套浸泡於[香水的特殊手套](../Page/香水.md "wikilink")。这期间，男性贵族也流行戴有装饰的手套。直到現在，西方許多正式場合都會有以戴手套以表示莊重的傳統。

[罗马天主教的](../Page/罗马天主教.md "wikilink")[教皇](../Page/教皇.md "wikilink")、[红衣主教和](../Page/红衣主教.md "wikilink")[主教等在施行](../Page/主教.md "wikilink")[弥撒的时候戴白色手套](../Page/弥撒.md "wikilink")，有装饰和象征神圣二重用途。

西方的职业军人传统上戴白手套，向别人丢手套往往是挑起[決鬥的第一步](../Page/決鬥.md "wikilink")。目前白手套仍然是很多国家军礼服的一部分。

[Disposable_nitrile_glove.jpg](https://zh.wikipedia.org/wiki/File:Disposable_nitrile_glove.jpg "fig:Disposable_nitrile_glove.jpg")

[Gloves1.jpg](https://zh.wikipedia.org/wiki/File:Gloves1.jpg "fig:Gloves1.jpg")

## 手套的分类

### 按指部外形

  - 分指手套：每只有5个分开的长袋装手指；
  - 三指手套：[拇指和](../Page/拇指.md "wikilink")[食指分开](../Page/食指.md "wikilink")，其余3个手指连在一起。
  - 连指手套：[中国东北称手闷子](../Page/中国东北.md "wikilink")，拇指分开，其余4个手指连在一起。
  - 直型手套：5个手指连在一起。
  - 半指手套：每个手指部分不闭合，只遮到第一节。
  - 无指手套：没有手指部分，在指跟处开口。

分开的手指越少，对手指的保温效果也越好，但同时限制了手部的活动。半指和无指手套除了装饰外，比分指手套的手指灵活性也有增加。

[Glove.png](https://zh.wikipedia.org/wiki/File:Glove.png "fig:Glove.png")

### 按用途

  - 防寒手套：一般用布、布夹[棉花](../Page/棉花.md "wikilink")、毛线编织、毡、[皮革等材料制作](../Page/皮革.md "wikilink")。
  - 烹飪手套：在廚房使用，結構與防寒手套類似，做為隔熱或隔離細菌之用。
  - 純裝飾：常見於西式女性[晚礼服](../Page/晚礼服.md "wikilink")、[婚纱](../Page/婚纱.md "wikilink")、禮賓人員服裝，经常配有丝绸、纱制的长手套。
  - 棉紗手套：在劳动的时候戴手套以保护手並避免污損物品。例如[焊接](../Page/焊接.md "wikilink")、安装[铁丝网](../Page/铁丝网.md "wikilink")、使用[电锯](../Page/电锯.md "wikilink")、帶電操作、使用有害化学品和[有害生物制品的时候](../Page/生物性危害.md "wikilink")。但使用一些迴轉機械如[鑽孔機等](../Page/鑽孔機.md "wikilink")，為防止手被捲入機械內受傷，不可戴手套操作。
  - [醫用手套](../Page/醫用手套.md "wikilink")：[医生](../Page/医生.md "wikilink")、[護理師](../Page/護理師.md "wikilink")、藥劑師给病人做检查的时候，为了卫生经常戴密封的[医用手套](../Page/医用手套.md "wikilink")，用[乳胶](../Page/乳胶.md "wikilink")、[聚氯乙烯](../Page/聚氯乙烯.md "wikilink")[丁腈橡胶等合成材料制作](../Page/丁腈橡胶.md "wikilink")。
  - 駕駛手套：驾驶员有时候戴专用的手套以增加摩擦，把稳[汽车的](../Page/汽车.md "wikilink")[方向盘](../Page/方向盘.md "wikilink")。在日本，大眾運輸的從業員承襲明治維新後軍人戴白手套的傳統，工作時戴白手套。
  - 運動用途：[棒球手套](../Page/棒球手套.md "wikilink")、[拳擊手套](../Page/拳擊.md "wikilink")、[足球](../Page/足球.md "wikilink")[守門員手套等](../Page/守門員.md "wikilink")。
  - [BDSM用途](../Page/BDSM.md "wikilink")：皮革或乳膠製的手套。
  - 戰爭用途：通常為金屬製，可見於全套式或半套式的金屬鎧甲。

## 另見

  - [手套箱](../Page/手套箱.md "wikilink")

## 注释

<references />

[Category:服裝](../Category/服裝.md "wikilink")
[Category:工具](../Category/工具.md "wikilink")
[Category:手](../Category/手.md "wikilink")

1.  [湖北江陵藤店1号汉墓](http://www.jianbo.org/Mlhc/2002/biannian/1973.htm)
2.  照片见[该网页](http://210.28.216.20/cixiu/cixiu/program/qiyuan.htm)