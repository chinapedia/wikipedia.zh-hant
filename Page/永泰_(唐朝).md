**永泰**（765年正月—766年十一月）是[唐代宗的年號](../Page/唐代宗.md "wikilink")，共计2年。

[吐鲁番出土有](../Page/吐鲁番.md "wikilink")《唐永泰三年后麦粟帐》，可见当时与唐朝隔绝的[西州地区仍用永泰年号至少直至](../Page/西州.md "wikilink")“永泰三年”（767年）。

## 大事記

## 出生

## 逝世

## 紀年

| 永泰                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 765年                           | 766年                           |
| [干支](../Page/干支紀年.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 其他使用[永泰年號的政權](../Page/永泰.md "wikilink")
  - 同期存在的其他政权年号
      - [寳勝](../Page/寳勝.md "wikilink")（762年八月—763年四月）：唐朝時期領袖[袁晁之年號](../Page/袁晁.md "wikilink")
      - [天平寳字](../Page/天平寳字.md "wikilink")（757年八月十八日—765年一月七日）：[奈良時代](../Page/奈良時代.md "wikilink")[孝謙天皇](../Page/孝謙天皇.md "wikilink")、[淳仁天皇](../Page/淳仁天皇.md "wikilink")、[稱德天皇之年號](../Page/稱德天皇.md "wikilink")
      - [天平神護](../Page/天平神護.md "wikilink")（765年一月七日—767年八月十六日）：奈良時代稱德天皇之年號
      - [大興](../Page/大興_\(大欽茂\).md "wikilink")（738年—794年）：[渤海文王](../Page/渤海国.md "wikilink")[大欽茂之年號](../Page/大欽茂.md "wikilink")
      - [贊普鍾](../Page/贊普鍾.md "wikilink")（752年—768年）：[南詔領袖](../Page/南詔.md "wikilink")[閣羅鳳之年號](../Page/閣羅鳳.md "wikilink")

[Category:唐朝年号](../Category/唐朝年号.md "wikilink")
[Category:8世纪中国年号](../Category/8世纪中国年号.md "wikilink")
[Category:760年代中国政治](../Category/760年代中国政治.md "wikilink")