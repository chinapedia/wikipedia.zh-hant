**薑目**（[学名](../Page/学名.md "wikilink")：）是[單子葉植物的一目](../Page/單子葉植物.md "wikilink")，本目也叫做美人蕉目。本目常見的植物有香蕉、薑、美人蕉等。

## 分类

本目的[旁系群是](../Page/旁系群.md "wikilink")[鸭跖草目](../Page/鸭跖草目.md "wikilink")，与

### 内部分类

本目可分成八個科：

  - [薑科](../Page/薑科.md "wikilink")
    Zingiberaceae：[豆蔻](../Page/豆蔻.md "wikilink")、[蝴蝶薑](../Page/蝴蝶薑.md "wikilink")、[火炬薑](../Page/火炬薑.md "wikilink")、[薑三七](../Page/薑三七.md "wikilink")、[薑](../Page/薑.md "wikilink")。
  - [芭蕉科](../Page/芭蕉科.md "wikilink")
    Musaceae：[象腿蕉](../Page/象腿蕉.md "wikilink")、[香蕉](../Page/香蕉.md "wikilink")、[地湧金蓮](../Page/地湧金蓮.md "wikilink")、[台灣芭蕉](../Page/台灣芭蕉.md "wikilink")、[紅焰蕉](../Page/紅焰蕉.md "wikilink")、[磚紅蕉](../Page/磚紅蕉.md "wikilink")、[臘紅蕉](../Page/臘紅蕉.md "wikilink")、[橘紅蕉](../Page/橘紅蕉.md "wikilink")、[粉紅蕉](../Page/粉紅蕉.md "wikilink")。
  - [旅人蕉科](../Page/旅人蕉科.md "wikilink")
    Strelitziaceae，別名[鶴望蘭科](../Page/鶴望蘭科.md "wikilink")：[旅人蕉](../Page/旅人蕉.md "wikilink")、[天堂鳥花](../Page/天堂鳥花.md "wikilink")。
  - [蘭花蕉科](../Page/蘭花蕉科.md "wikilink")
    Lowiaceae：[蘭花蕉](../Page/蘭花蕉.md "wikilink")。
  - [赫蕉科](../Page/赫蕉科.md "wikilink")
    Heliconiaceae，別名[蠍尾蕉科](../Page/蠍尾蕉科.md "wikilink")：[赫蕉屬](../Page/赫蕉屬.md "wikilink")、[蠍尾蕉](../Page/蠍尾蕉.md "wikilink")。
  - [閉鞘薑科](../Page/閉鞘薑科.md "wikilink")
    Costaceae：[閉鞘薑](../Page/閉鞘薑.md "wikilink")。
  - [美人蕉科](../Page/美人蕉科.md "wikilink")
    Cannaceae，別名[曇華科](../Page/曇華科.md "wikilink")：[美人蕉屬](../Page/美人蕉屬.md "wikilink")。
  - [竹芋科](../Page/竹芋科.md "wikilink")
    Marantaceae：[孔雀竹芋](../Page/孔雀竹芋.md "wikilink")、[葛鬱金](../Page/葛鬱金.md "wikilink")、[紅裏蕉](../Page/紅裏蕉.md "wikilink")。

## 参考文献

## 外部链接

  -
[\*](../Category/薑目.md "wikilink")