本列表統計的是國家籃球協會（NBA）球員個人職業生涯例行賽助攻總和，NBA球員在其它聯賽（如ABA）中的數據並不包括在內。所有數據都是截至到2018年12月2日（2018-19）。

[John_Stockton_Lipofskydotcom-32245.jpg](https://zh.wikipedia.org/wiki/File:John_Stockton_Lipofskydotcom-32245.jpg "fig:John_Stockton_Lipofskydotcom-32245.jpg")\]\]
[KiddFTline2.jpg](https://zh.wikipedia.org/wiki/File:KiddFTline2.jpg "fig:KiddFTline2.jpg")\]\]
[SteveNash3.jpg](https://zh.wikipedia.org/wiki/File:SteveNash3.jpg "fig:SteveNash3.jpg")\]\]

|    |              |
| -- | ------------ |
| ^  | 现役球员         |
| \* | 入选了奈史密斯篮球名人堂 |

<table>
<thead>
<tr class="header">
<th><p>排名</p></th>
<th><p>球員</p></th>
<th><p><a href="../Page/籃球位置.md" title="wikilink">場上位置</a></p></th>
<th><p><a href="../Page/NBA.md" title="wikilink">球隊</a><a href="../Page/NBA赛季.md" title="wikilink">NBA赛季</a></p></th>
<th><p>總<br />
<a href="../Page/助攻.md" title="wikilink">助攻</a></p></th>
<th><p>平均<a href="../Page/助攻.md" title="wikilink">助攻</a><br />
</p></th>
<th><p>入選<br />
<a href="../Page/籃球名人堂.md" title="wikilink">籃球名人堂</a><br />
年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/約翰·斯托克頓.md" title="wikilink">約翰·史托克頓</a></p></td>
<td><p><a href="../Page/控球後衛.md" title="wikilink">控球後衛</a></p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>－<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>15,806</p></td>
<td><p>10.5</p></td>
<td><p><a href="../Page/2009年體育.md" title="wikilink">2009年</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/贾森·基德.md" title="wikilink">傑森·基德</a></p></td>
<td><p><a href="../Page/控球後衛.md" title="wikilink">控球後衛</a></p></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>－<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a><br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>－<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>－<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>－<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>）</p></td>
<td><p>12,091</p></td>
<td><p>8.7</p></td>
<td><p><a href="../Page/2018年體育.md" title="wikilink">2018年</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/史蒂夫·納什.md" title="wikilink">史蒂夫·納什</a></p></td>
<td><p><a href="../Page/控球後衛.md" title="wikilink">控球後衛</a></p></td>
<td><p><a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>－<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>－<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>－<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>-<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）</p></td>
<td><p>10,335</p></td>
<td><p>8.5</p></td>
<td><p><a href="../Page/2018年體育.md" title="wikilink">2018年</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/馬克·傑克遜.md" title="wikilink">馬克·傑克森</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>－<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>)<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>－<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>）<br />
<a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>－<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>）<br />
<a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a>（<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>-<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>)<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>)<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/休斯敦火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）</p></td>
<td><p>10,334</p></td>
<td><p>8.0</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/埃爾文·約翰遜.md" title="wikilink">魔術师约翰逊</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1979-80_NBA賽季.md" title="wikilink">1979-80</a>－<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>、<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）</p></td>
<td><p>10,141</p></td>
<td><p>11.2</p></td>
<td><p><a href="../Page/2002年體育.md" title="wikilink">2002年</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/奧斯卡·羅伯遜.md" title="wikilink">奧斯卡·羅伯森</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">辛辛那提皇家</a>（<a href="../Page/1960-61_NBA賽季.md" title="wikilink">1960-61</a>－<a href="../Page/1969-70_NBA賽季.md" title="wikilink">1969-70</a>）<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/1970-71_NBA賽季.md" title="wikilink">1970-71</a>－<a href="../Page/1973-74_NBA賽季.md" title="wikilink">1973-74</a>）</p></td>
<td><p>9,887</p></td>
<td><p>9.5</p></td>
<td><p><a href="../Page/1980年體育.md" title="wikilink">1980年</a></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><strong><a href="../Page/基斯·保羅.md" title="wikilink">基斯·保羅</a></strong></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/紐奧良黃蜂.md" title="wikilink">紐奧良黃蜂</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>－<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>-<a href="../Page/2016-17_NBA賽季.md" title="wikilink">2016-17</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a><a href="../Page/2017-18_NBA賽季.md" title="wikilink">2017-18</a>-）</p></td>
<td><p>9,062</p></td>
<td><p>19|</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/伊塞亞·托馬斯.md" title="wikilink">以賽亞·托馬斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>（<a href="../Page/1981-82_NBA賽季.md" title="wikilink">1981-82</a>－<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>）</p></td>
<td><p>9,061</p></td>
<td><p>9.3</p></td>
<td><p><a href="../Page/2000年體育.md" title="wikilink">2000年</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/蓋瑞·裴頓.md" title="wikilink">蓋瑞·裴頓</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>－<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a><br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>－<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a></p></td>
<td><p>8,966</p></td>
<td><p>6.7</p></td>
<td><p><a href="../Page/2013年體育.md" title="wikilink">2013年</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><strong><a href="../Page/安德烈·米勒.md" title="wikilink">安德烈·米勒</a></strong></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>－<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>－<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>）<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>－<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>－<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>-<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>）<br />
<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a>（<a href="../Page/2013-14_NBA賽季.md" title="wikilink">2013-14</a>-<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/萨克拉门托国王.md" title="wikilink">萨克拉门托国王</a>（<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/明尼苏达森林狼.md" title="wikilink">明尼苏达森林狼</a>（<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）<br />
<a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a>（<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）</p></td>
<td><p>8,524</p></td>
<td><p>6.5</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p><strong><a href="../Page/雷霸龍·詹姆士.md" title="wikilink">雷霸龍·詹姆士</a></strong></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>－<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>-<a href="../Page/2013-14_NBA赛季.md" title="wikilink">2013-14</a>）<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/2014-15_NBA賽季.md" title="wikilink">2014-15</a>－<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>)</p></td>
<td><p>8,206</p></td>
<td><p>7.2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/羅德·斯特里克兰.md" title="wikilink">羅德·斯特里克兰</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>－<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a><br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>－<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>－<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>、<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>－<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a><br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a><br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）</p></td>
<td><p>7,987</p></td>
<td><p>7.3</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p><a href="../Page/莫里斯·奇克斯.md" title="wikilink">莫里斯·奇克斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/1978-79_NBA賽季.md" title="wikilink">1978-79</a>－<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a><br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>－<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>）</p></td>
<td><p>7,392</p></td>
<td><p>8.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p><a href="../Page/蘭尼·威爾肯斯.md" title="wikilink">蘭尼·威肯斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">聖路易老鷹</a>（<a href="../Page/1960-61_NBA賽季.md" title="wikilink">1960-61</a>－<a href="../Page/1967-68_NBA賽季.md" title="wikilink">1967-68</a>）<br />
<a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1968-69_NBA賽季.md" title="wikilink">1968-69</a>－<a href="../Page/1971-72_NBA賽季.md" title="wikilink">1971-72</a>）<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/1972-73_NBA賽季.md" title="wikilink">1972-73</a>－<a href="../Page/1973-74_NBA賽季.md" title="wikilink">1973-74</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1974-75_NBA賽季.md" title="wikilink">1974-75</a>）</p></td>
<td><p>7,211</p></td>
<td><p>6.7</p></td>
<td><p><a href="../Page/1989年體育.md" title="wikilink">1989年</a></p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p><a href="../Page/泰瑞·波特.md" title="wikilink">泰瑞·波特</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1985-86_NBA賽季.md" title="wikilink">1985-86</a>－<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>）<br />
<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>－<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>－<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）</p></td>
<td><p>7,160</p></td>
<td><p>5.6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p><a href="../Page/蒂姆·哈达威.md" title="wikilink">蒂姆·哈达威</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>－<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a><br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>－<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a><br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>7,095</p></td>
<td><p>8.2</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p><strong><a href="../Page/東尼·帕克.md" title="wikilink">東尼·帕克</a></strong></p></td>
<td><p><a href="../Page/控球後衛.md" title="wikilink">控球後衛</a></p></td>
<td><p><a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>-<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>)</p></td>
<td><p>6,975</p></td>
<td><p>5.6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p><a href="../Page/鲍勃·库锡.md" title="wikilink">鮑伯·庫錫</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/1950-51_NBA賽季.md" title="wikilink">1950-51</a>－<a href="../Page/1962-63_NBA賽季.md" title="wikilink">1962-63</a>）<br />
<a href="../Page/沙加緬度國王.md" title="wikilink">辛辛那提皇家</a>（<a href="../Page/1969-70_NBA賽季.md" title="wikilink">1969-70</a>）</p></td>
<td><p>6,955</p></td>
<td><p>7.5</p></td>
<td><p><a href="../Page/1971年體育年.md" title="wikilink">1971</a></p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p><a href="../Page/盖伊·罗杰斯.md" title="wikilink">盖伊·罗杰斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">舊金山勇士</a>（<a href="../Page/1958-59_NBA賽季.md" title="wikilink">1958-59</a>、<a href="../Page/1965-66_NBA賽季.md" title="wikilink">1965-66</a>）<br />
<a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1966-67_NBA賽季.md" title="wikilink">1966-67</a>－<a href="../Page/1967-68_NBA賽季.md" title="wikilink">1967-68</a><br />
<a href="../Page/沙加緬度國王.md" title="wikilink">辛辛那提皇家</a>（<a href="../Page/1967-68_NBA賽季.md" title="wikilink">1967-68</a>）<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/1968-69_NBA賽季.md" title="wikilink">1968-69</a>－<a href="../Page/1969-70_NBA賽季.md" title="wikilink">1969-70</a>）</p></td>
<td><p>6,917</p></td>
<td><p>7.8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p><strong><a href="../Page/德隆·威廉斯.md" title="wikilink">德隆·威廉斯</a></strong></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>－<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>/<a href="../Page/布魯克林籃網.md" title="wikilink">布魯克林籃網</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>－<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/达拉斯小牛.md" title="wikilink">达拉斯小牛</a>（<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）</p></td>
<td><p>6,819</p></td>
<td><p>8.2</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p><a href="../Page/馬格西·博格斯.md" title="wikilink">馬格西·伯格斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓子彈</a>（<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>）<br />
<a href="../Page/紐奧良黃蜂.md" title="wikilink">夏洛特黃峰</a>（<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>－<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a><br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>－<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>－<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）</p></td>
<td><p>6,726</p></td>
<td><p>7.6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p><a href="../Page/凱文·约翰逊.md" title="wikilink">凱文·约翰逊</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a><br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>－<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>）</p></td>
<td><p>6,711</p></td>
<td><p>9.1</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p><strong><a href="../Page/拉简·隆多.md" title="wikilink">拉简·隆多</a></strong></p></td>
<td><p>控球后卫</p></td>
<td><p><a href="../Page/波士顿凯尔特人.md" title="wikilink">波士顿凯尔特人</a>（<a href="../Page/2006-07_NBA赛季.md" title="wikilink">2006-07</a>-<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/达拉斯小牛.md" title="wikilink">达拉斯小牛</a>（<a href="../Page/2014-15_NBA赛季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/萨克拉门托国王.md" title="wikilink">萨克拉门托国王</a>（<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）</p></td>
<td><p>6,702</p></td>
<td><p>8.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p><a href="../Page/德里克·哈珀.md" title="wikilink">德里克·哈珀</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>－<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>、<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>－<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>（<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）</p></td>
<td><p>6,577</p></td>
<td><p>5.5</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p><a href="../Page/拉塞尔·威斯布鲁克.md" title="wikilink">拉塞尔·威斯布鲁克</a></p></td>
<td><p>控球后卫</p></td>
<td><p><a href="../Page/俄克拉荷马雷霆.md" title="wikilink">俄克拉荷马雷霆</a>（<a href="../Page/2009-10_NBA赛季.md" title="wikilink">2009-10</a> - ）</p></td>
<td><p>6,484</p></td>
<td><p>8.2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p><a href="../Page/奈特·阿奇博尔德.md" title="wikilink">奈特·阿奇博尔德</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/沙加緬度國王.md" title="wikilink">堪薩斯市國王</a>（<a href="../Page/1970-71_NBA賽季.md" title="wikilink">1970-71</a>－<a href="../Page/1975-76_NBA賽季.md" title="wikilink">1975-76</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1976-77_NBA賽季.md" title="wikilink">1976-77</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/1978-79_NBA賽季.md" title="wikilink">1978-79</a>－<a href="../Page/1982-83_NBA賽季.md" title="wikilink">1982-83</a>）<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>）</p></td>
<td><p>6,476</p></td>
<td><p>7.4</p></td>
<td><p><a href="../Page/1991年體育.md" title="wikilink">1991年</a></p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p><a href="../Page/斯蒂芬·馬布里.md" title="wikilink">史蒂芬·馬布里</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>－<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a><br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>－<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>－<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a><br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>－<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）</p></td>
<td><p>6,471</p></td>
<td><p>7.6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p><a href="../Page/约翰·卢卡斯二世_(1953年出生).md" title="wikilink">约翰·卢卡斯二世</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/1976-77_NBA賽季.md" title="wikilink">1976-77</a>－<a href="../Page/1977-78_NBA賽季.md" title="wikilink">1977-78</a>、<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>－<a href="../Page/1985-86_NBA賽季.md" title="wikilink">1985-86</a>、<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1978-79_NBA賽季.md" title="wikilink">1978-79</a>－<a href="../Page/1980-81_NBA賽季.md" title="wikilink">1980-81</a>）<br />
<a href="../Page/華盛頓子彈.md" title="wikilink">華盛頓子彈</a>（<a href="../Page/1981-82_NBA賽季.md" title="wikilink">1981-82</a>－<a href="../Page/1982-83_NBA賽季.md" title="wikilink">1982-83</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>）<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/1986-87_NBA賽季.md" title="wikilink">1986-87</a>－<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>）<br />
<a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>）</p></td>
<td><p>6,454</p></td>
<td><p>7.0</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p><a href="../Page/雷吉·瑟乌斯.md" title="wikilink">雷吉·瑟乌斯</a></p></td>
<td><p>控球后卫<br />
得分後衛</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1978-79_NBA賽季.md" title="wikilink">1978-79</a>－<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>）<br />
<a href="../Page/沙加緬度國王.md" title="wikilink">堪薩斯市國王</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>－<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>）<br />
<a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>）</p></td>
<td><p>6,453</p></td>
<td><p>6.3</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p><a href="../Page/诺姆·尼克森.md" title="wikilink">诺姆·尼克森</a></p></td>
<td><p>控球后卫<br />
得分後衛</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1977-78_NBA賽季.md" title="wikilink">1977-78</a>-<a href="../Page/1982-83_NBA賽季.md" title="wikilink">1982-83</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>－<a href="../Page/1985-86_NBA賽季.md" title="wikilink">1985-86</a>、<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>）</p></td>
<td><p>6,386</p></td>
<td><p>8.3</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td><p><strong><a href="../Page/科比·布萊恩.md" title="wikilink">科比·布萊恩</a></strong></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>-<a href="../Page/2015-16_NBA赛季.md" title="wikilink">2015-16</a>）</p></td>
<td><p>6,302</p></td>
<td><p>4.7</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p><a href="../Page/傑里·韋斯特.md" title="wikilink">傑里·韦斯特</a></p></td>
<td><p><a href="../Page/得分後衛.md" title="wikilink">得分後衛</a></p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1960-61_NBA賽季.md" title="wikilink">1960-61</a>－<a href="../Page/1973-74_NBA賽季.md" title="wikilink">1973-74</a>）</p></td>
<td><p>6,238</p></td>
<td><p>6.7</p></td>
<td><p><a href="../Page/1980年體育.md" title="wikilink">1980年</a></p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td><p><a href="../Page/斯科蒂·皮蓬.md" title="wikilink">史考提·皮朋</a></p></td>
<td><p><a href="../Page/小前鋒.md" title="wikilink">小前鋒</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1987-88_NBA賽季.md" title="wikilink">1987-88</a>－<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>、<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/休斯敦火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>－<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>6,135</p></td>
<td><p>5.2</p></td>
<td><p>2010年</p></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td><p><a href="../Page/克萊德·德雷克斯勒.md" title="wikilink">克萊德·崔斯勒</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>－<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>－<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）</p></td>
<td><p>6,125</p></td>
<td><p>5.6</p></td>
<td><p><a href="../Page/2004年體育.md" title="wikilink">2004年</a></p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td><p><a href="../Page/約翰·哈夫利切克.md" title="wikilink">約翰·哈夫利切克</a></p></td>
<td><p>小前鋒</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/1962-63_NBA賽季.md" title="wikilink">1962-63</a>－<a href="../Page/1977-78_NBA賽季.md" title="wikilink">1977-78</a>）</p></td>
<td><p>6,114</p></td>
<td><p>4.8</p></td>
<td><p><a href="../Page/1984年體育.md" title="wikilink">1984年</a></p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td><p><a href="../Page/貝倫·戴維斯.md" title="wikilink">貝倫·戴維斯</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/夏洛特黃蜂.md" title="wikilink">夏洛特黃蜂</a>（<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>－<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/紐奧良黃蜂.md" title="wikilink">紐奧良黃蜂</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>－<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/2004-2005_NBA賽季.md" title="wikilink">2004-2005</a>－<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>－<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/克里夫蘭騎士.md" title="wikilink">克里夫蘭騎士</a>（<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）</p></td>
<td><p>6,025</p></td>
<td><p>7.2</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td><p><a href="../Page/穆基·布莱洛克.md" title="wikilink">穆基·布莱洛克</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>－<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>－<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a>－<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）</p></td>
<td><p>5,972</p></td>
<td><p>6.7</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td><p><a href="../Page/薩姆·卡塞爾.md" title="wikilink">山姆·卡塞爾</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>－<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>&lt;<br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a>）<br />
<a href="../Page/紐澤西籃網.md" title="wikilink">紐澤西籃網</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1997-98</a>－<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>）<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>－<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/明尼蘇達雙城.md" title="wikilink">明尼蘇達雙城</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>－<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>－<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）</p></td>
<td><p>5,939</p></td>
<td><p>6.0</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p><a href="../Page/艾弗里·约翰逊.md" title="wikilink">艾弗里·约翰逊</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>－<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/1990-91_NBA賽季.md" title="wikilink">1990-91</a>－<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>、<a href="../Page/1992-93_NBA賽季.md" title="wikilink">1992-93</a>、<a href="../Page/1994-95_NBA賽季.md" title="wikilink">1994-95</a>－<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>、<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a><br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>－<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>5,846</p></td>
<td><p>5.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p><a href="../Page/尼克·范埃克塞尔.md" title="wikilink">尼克·范埃克塞尔</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1993-94_NBA賽季.md" title="wikilink">1993-94</a>－<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>－<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a><br />
<a href="../Page/達拉斯小牛.md" title="wikilink">達拉斯小牛</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>－<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）<br />
<a href="../Page/金州勇士.md" title="wikilink">金州勇士</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/波特蘭拓荒者.md" title="wikilink">波特蘭拓荒者</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>）<br />
<a href="../Page/聖安東尼奧馬刺.md" title="wikilink">聖安東尼奧馬刺</a>（<a href="../Page/2005-06_NBA賽季.md" title="wikilink">2005-06</a>）</p></td>
<td><p>5,777</p></td>
<td><p>6.6</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td><p><a href="../Page/拉里·伯德.md" title="wikilink">賴瑞·柏德</a></p></td>
<td><p><a href="../Page/大前鋒.md" title="wikilink">大前鋒</a><br />
小前鋒</p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/1979-80_NBA賽季.md" title="wikilink">1979-80</a>－<a href="../Page/1991-92_NBA賽季.md" title="wikilink">1991-92</a>）</p></td>
<td><p>5,695</p></td>
<td><p>6.3</p></td>
<td><p><a href="../Page/1998年體育.md" title="wikilink">1998年</a></p></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td><p><a href="../Page/卡里姆·阿卜杜勒·賈巴爾.md" title="wikilink">卡里姆·阿布都·賈霸</a></p></td>
<td><p><a href="../Page/中鋒.md" title="wikilink">中鋒</a></p></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/1969-70_NBA賽季.md" title="wikilink">1969-70</a>－<a href="../Page/1974-75_NBA賽季.md" title="wikilink">1974-75</a>）<br />
<a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a>（<a href="../Page/1975-76_NBA賽季.md" title="wikilink">1975-76</a>－<a href="../Page/1988-89_NBA賽季.md" title="wikilink">1988-89</a>）</p></td>
<td><p>5,660</p></td>
<td><p>3.6</p></td>
<td><p><a href="../Page/1995年體育.md" title="wikilink">1995年</a></p></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td><p><a href="../Page/昌西·比卢普斯.md" title="wikilink">昌西·比卢普斯</a></p></td>
<td><p><a href="../Page/控球後衛.md" title="wikilink">控球後衛</a></p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a>（<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a>-<a href="../Page/1999-00_NBA賽季.md" title="wikilink">1999-00</a>）<br />
<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>-<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>）<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>（<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>-<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/丹佛金塊.md" title="wikilink">丹佛金塊</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>-<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/洛杉磯快艇.md" title="wikilink">洛杉磯快艇</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>-<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>）<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>（<a href="../Page/2013-14_NBA賽季.md" title="wikilink">2013-14</a>）</p></td>
<td><p>5,636</p></td>
<td><p>5.4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td><p><a href="../Page/麥可·喬登.md" title="wikilink">麥可·喬登</a></p></td>
<td><p>得分後衛</p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a>（<a href="../Page/1984-85_NBA賽季.md" title="wikilink">1984-85</a>－<a href="../Page/1997-98_NBA賽季.md" title="wikilink">1997-98</a>）<br />
<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>－<a href="../Page/2002-03_NBA賽季.md" title="wikilink">2002-03</a>）</p></td>
<td><p>5,633</p></td>
<td><p>5.3</p></td>
<td><p>2009年</p></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td><p><a href="../Page/阿倫·艾佛森.md" title="wikilink">阿倫·艾佛森</a></p></td>
<td><p>控球後衛</p></td>
<td><p><a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/1996-97_NBA賽季.md" title="wikilink">1996-97</a> －<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>）<br />
<a href="../Page/丹佛掘金.md" title="wikilink">丹佛掘金</a>（<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>－<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/底特律活塞.md" title="wikilink">底特律活塞</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>）<br />
<a href="../Page/孟菲斯灰熊.md" title="wikilink">孟菲斯灰熊</a>（<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>）<br />
<a href="../Page/費城76人.md" title="wikilink">費城76人</a>（<a href="../Page/2009-10_NBA賽季.md" title="wikilink">2009-10</a>）</p></td>
<td><p>5,624</p></td>
<td><p>6.2</p></td>
<td><p>2016年</p></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td><p><a href="../Page/德懷恩·韋德.md" title="wikilink">德懷恩·韋德</a></p></td>
<td><p>得分后卫</p></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a> －<a href="../Page/2018-19_NBA賽季.md" title="wikilink">2018-19</a></p></td>
<td><p>5,529</p></td>
<td><p>5.4</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td><p><a href="../Page/迈克·毕比.md" title="wikilink">迈克·毕比</a></p></td>
<td><p>控球后卫</p></td>
<td><p><a href="../Page/溫哥華灰熊.md" title="wikilink">溫哥華灰熊</a>（<a href="../Page/1998-99_NBA賽季.md" title="wikilink">1998-99</a> －<a href="../Page/2000-01_NBA賽季.md" title="wikilink">2000-01</a>）<br />
<a href="../Page/沙加緬度國王.md" title="wikilink">沙加緬度國王</a>（<a href="../Page/2001-02_NBA賽季.md" title="wikilink">2001-02</a>－<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>）<br />
<a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/2008-09_NBA賽季.md" title="wikilink">2008-09</a>－<a href="../Page/2010-11_NBA賽季.md" title="wikilink">2010-11</a>）<br />
<a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a>（<a href="../Page/2011_NBA賽季.md" title="wikilink">2011</a>）<br />
<a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a>（<a href="../Page/2011_NBA賽季.md" title="wikilink">2011</a>）<br />
<a href="../Page/紐約尼克.md" title="wikilink">紐約尼克</a>（<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）</p></td>
<td><p>5,517</p></td>
<td><p>5.5</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td><p><a href="../Page/丹尼斯·约翰逊.md" title="wikilink">丹尼斯·约翰逊</a></p></td>
<td><p>得分后卫<br />
控球后卫</p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a>（<a href="../Page/1976-77_NBA賽季.md" title="wikilink">1976-77</a> －<a href="../Page/1979-80_NBA賽季.md" title="wikilink">1979-80</a>）<br />
<a href="../Page/鳳凰城太陽.md" title="wikilink">鳳凰城太陽</a>（<a href="../Page/1980-81_NBA賽季.md" title="wikilink">1980-81</a>－<a href="../Page/1982-83_NBA賽季.md" title="wikilink">1982-83</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/1983-84_NBA賽季.md" title="wikilink">1983-84</a>－<a href="../Page/1989-90_NBA賽季.md" title="wikilink">1989-90</a>）</p></td>
<td><p>5,499</p></td>
<td><p>5.0</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td><p><strong><a href="../Page/凯文·加内特.md" title="wikilink">凯文·加内特</a></strong></p></td>
<td><p>大前锋<br />
小前锋<br />
中锋</p></td>
<td><p><a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/1995-96_NBA賽季.md" title="wikilink">1995-96</a> －<a href="../Page/2006-07_NBA賽季.md" title="wikilink">2006-07</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/2007-08_NBA賽季.md" title="wikilink">2007-08</a>－<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>）<br />
<a href="../Page/布魯克林籃網.md" title="wikilink">布魯克林籃網</a>（<a href="../Page/2013-14_NBA賽季.md" title="wikilink">2013-14</a>－<a href="../Page/2014-15_NBA賽季.md" title="wikilink">2014-15</a>）<br />
<a href="../Page/明尼蘇達灰狼.md" title="wikilink">明尼蘇達灰狼</a>（<a href="../Page/2015-16_NBA賽季.md" title="wikilink">2015-16</a>）</p></td>
<td><p>5,445</p></td>
<td><p>3.7</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>50</p></td>
<td><p><a href="../Page/傑森·特里.md" title="wikilink">傑森·特里</a></p></td>
<td><p>控球后卫<br />
得分后卫</p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a>（<a href="../Page/1999-2000_NBA賽季.md" title="wikilink">1999-2000</a> －<a href="../Page/2003-04_NBA賽季.md" title="wikilink">2003-04</a>）<br />
<a href="../Page/達拉斯獨行俠.md" title="wikilink">達拉斯獨行俠</a>（<a href="../Page/2004-05_NBA賽季.md" title="wikilink">2004-05</a>－<a href="../Page/2011-12_NBA賽季.md" title="wikilink">2011-12</a>）<br />
<a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a>（<a href="../Page/2012-13_NBA賽季.md" title="wikilink">2012-13</a>）<br />
<a href="../Page/布魯克林籃網.md" title="wikilink">布魯克林籃網</a>（<a href="../Page/2013-14_NBA賽季.md" title="wikilink">2013-14</a>）<br />
<a href="../Page/休士頓火箭.md" title="wikilink">休士頓火箭</a>（<a href="../Page/2014-15_NBA賽季.md" title="wikilink">2014-15</a>－<a href="../Page/2015-16_NBA賽季.md" title="wikilink">2015-16</a>）<br />
<a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a>（<a href="../Page/2016-17_NBA賽季.md" title="wikilink">2016-17</a>－<a href="../Page/2017-18_NBA賽季.md" title="wikilink">2017-18</a>）</p></td>
<td><p>5,415</p></td>
<td><p>3.8</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

  - [basketball-reference.com
    NBA生涯助攻](http://www.basketball-reference.com/leaders/AST_career.html)
  - [databaseBasketball.com
    NBA生涯助攻](https://web.archive.org/web/20080317042030/http://www.databasebasketball.com/leaders/leaderscareer.htm?stat=asts&lg=n)

[Category:NBA數據統計](../Category/NBA數據統計.md "wikilink")