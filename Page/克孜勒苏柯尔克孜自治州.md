**克孜勒苏柯尔克孜自治州**（；[柯尔克孜语](../Page/柯尔克孜语.md "wikilink")：），简称**克孜勒苏州**、**克州**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[新疆维吾尔自治区下辖的](../Page/新疆维吾尔自治区.md "wikilink")[自治州](../Page/自治州.md "wikilink")。位于新疆维吾尔族自治区的西部。[柯爾克孜族為克孜勒苏州的自治民族](../Page/柯爾克孜族.md "wikilink")。

## 政治

### 现任领导

<table>
<caption>克孜勒苏柯尔克孜自治州四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党克孜勒苏柯尔克孜自治州委员会.md" title="wikilink">中国共产党<br />
克孜勒苏柯尔克孜自治州<br />
委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/克孜勒苏柯尔克孜自治州人民代表大会.md" title="wikilink">克孜勒苏柯尔克孜自治州<br />
人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/克孜勒苏柯尔克孜自治州人民政府.md" title="wikilink">克孜勒苏柯尔克孜自治州<br />
人民政府</a><br />
<br />
州长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议克孜勒苏柯尔克孜自治州委员会.md" title="wikilink">中国人民政治协商会议<br />
克孜勒苏柯尔克孜自治州<br />
委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/安征宇.md" title="wikilink">安征宇</a>[1]</p></td>
<td><p><a href="../Page/帕尔哈提·吐尔地.md" title="wikilink">帕尔哈提·吐尔地</a>[2]</p></td>
<td><p><a href="../Page/迪力夏提·柯德尔汗.md" title="wikilink">迪力夏提·柯德尔汗</a>[3]</p></td>
<td><p><a href="../Page/古丽夏提·西尔艾力.md" title="wikilink">古丽夏提·西尔艾力</a>（女）[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p><a href="../Page/柯尔克孜族.md" title="wikilink">柯尔克孜族</a></p></td>
<td><p>柯尔克孜族</p></td>
<td><p><a href="../Page/维吾尔族.md" title="wikilink">维吾尔族</a></p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/保定市.md" title="wikilink">保定市</a><a href="../Page/清苑区.md" title="wikilink">清苑区</a></p></td>
<td><p><a href="../Page/新疆维吾尔自治区.md" title="wikilink">新疆维吾尔自治区</a><a href="../Page/阿克陶县.md" title="wikilink">阿克陶县</a></p></td>
<td><p>新疆维吾尔自治区<a href="../Page/特克斯县.md" title="wikilink">特克斯县</a></p></td>
<td><p>新疆维吾尔自治区<a href="../Page/和田市.md" title="wikilink">和田市</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2017年10月</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年1月</p></td>
<td><p>2017年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

下辖1个[县级市](../Page/县级市.md "wikilink")、3个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 县级市：[阿图什市](../Page/阿图什市.md "wikilink")
  - 县：[阿克陶县](../Page/阿克陶县.md "wikilink")、[阿合奇县](../Page/阿合奇县.md "wikilink")、[乌恰县](../Page/乌恰县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>区划代码[5]</p></th>
<th><p>区划名称<br />
维吾尔文<br />
柯尔克孜文</p></th>
<th><p>汉语拼音<br />
拉丁转写</p></th>
<th><p>面积面积[6]<br />
（平方公里）</p></th>
<th><p>政府驻地</p></th>
<th><p>邮政编码</p></th>
<th><p>行政区划[7]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>街道</p></td>
<td><p>镇</p></td>
<td><p>乡</p></td>
<td><p>其中：<br />
民族乡</p></td>
<td><p>社区</p></td>
<td><p>行政村</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>653000</p></td>
<td><p>克孜勒苏柯尔克孜自治州<br />
<br />
</p></td>
<td></td>
<td><p>70916.33</p></td>
<td><p><a href="../Page/阿图什市.md" title="wikilink">阿图什市</a></p></td>
<td><p>845350</p></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p>653001</p></td>
<td><p>阿图什市<br />
<br />
</p></td>
<td></td>
<td><p>15697.78</p></td>
<td><p><a href="../Page/光明路街道.md" title="wikilink">光明路街道</a></p></td>
<td><p>845350</p></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p>653022</p></td>
<td><p>阿克陶县<br />
<br />
</p></td>
<td></td>
<td><p>24555.06</p></td>
<td><p><a href="../Page/阿克陶镇.md" title="wikilink">阿克陶镇</a></p></td>
<td><p>845550</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>653023</p></td>
<td><p>阿合奇县<br />
<br />
</p></td>
<td></td>
<td><p>11545.38</p></td>
<td><p><a href="../Page/阿合奇镇.md" title="wikilink">阿合奇镇</a></p></td>
<td><p>843500</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>653024</p></td>
<td><p>乌恰县<br />
<br />
</p></td>
<td></td>
<td><p>19118.11</p></td>
<td><p><a href="../Page/乌恰镇.md" title="wikilink">乌恰镇</a></p></td>
<td><p>845450</p></td>
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>克孜勒苏柯尔克孜自治州各市（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[8]（2010年11月）</p></th>
<th><p>户籍人口[9]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="odd">
<td><p>克孜勒苏柯尔克孜自治州</p></td>
<td><p>525570</p></td>
<td><p>100</p></td>
</tr>
<tr class="even">
<td><p>阿图什市</p></td>
<td><p>240368</p></td>
<td><p>45.73</p></td>
</tr>
<tr class="odd">
<td><p>阿克陶县</p></td>
<td><p>199065</p></td>
<td><p>37.88</p></td>
</tr>
<tr class="even">
<td><p>阿合奇县</p></td>
<td><p>38876</p></td>
<td><p>7.40</p></td>
</tr>
<tr class="odd">
<td><p>乌恰县</p></td>
<td><p>47261</p></td>
<td><p>8.99</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全州[常住人口为](../Page/常住人口.md "wikilink")525570人\[10\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加85882人，增长19.53%。年平均增长率为1.80%。其中，男性人口为267660人，占50.93%
；女性人口为257910人，占49.07%。总人口性别比（以女性为100）为103.78。0－14岁人口为146343人，占27.84%
；15－64岁人口为354774人，占67.50% ；65岁及以上人口为24453人，占4.65%。

### 民族

常住人口中，[汉族人口](../Page/汉族.md "wikilink")35629人，占总人口的6.78%，各[少数民族人口](../Page/少数民族.md "wikilink")489941人，占总人口的93.22%。

{{-}}

| 民族名称         | [维吾尔族](../Page/维吾尔族.md "wikilink") | [柯尔克孜族](../Page/柯尔克孜族.md "wikilink") | [汉族](../Page/汉族.md "wikilink") | [塔吉克族](../Page/塔吉克族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [哈萨克族](../Page/哈萨克族.md "wikilink") | [藏族](../Page/藏族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | [乌孜別克族](../Page/乌孜別克族.md "wikilink") | [蒙古族](../Page/蒙古族.md "wikilink") | 其他民族 |
| ------------ | ---------------------------------- | ------------------------------------ | ------------------------------ | ---------------------------------- | ------------------------------ | ---------------------------------- | ------------------------------ | -------------------------------- | ------------------------------------ | -------------------------------- | ---- |
| 人口数          | 339926                             | 143582                               | 35629                          | 5547                               | 447                            | 88                                 | 51                             | 49                               | 44                                   | 40                               | 167  |
| 占总人口比例（%）    | 64.68                              | 27.32                                | 6.78                           | 1.06                               | 0.09                           | 0.02                               | 0.01                           | 0.01                             | 0.01                                 | 0.01                             | 0.03 |
| 占少数民族人口比例（%） | 69.38                              | 29.31                                | \---                           | 1.13                               | 0.09                           | 0.02                               | 0.01                           | 0.01                             | 0.01                                 | 0.01                             | 0.03 |

**克孜勒苏柯尔克孜自治州民族构成（2010年11月）**\[11\]

## 参考文献

## 参见

  - [柯尔克孜族](../Page/柯尔克孜族.md "wikilink")
  - [吉尔吉斯共和国](../Page/吉尔吉斯共和国.md "wikilink")

{{-}}

[克孜勒苏](../Category/克孜勒苏.md "wikilink")
[Category:新疆自治州](../Category/新疆自治州.md "wikilink")
[Category:柯尔克孜族](../Category/柯尔克孜族.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.