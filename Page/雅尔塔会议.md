[Yalta_summit_1945_with_Churchill,_Roosevelt,_Stalin.jpg](https://zh.wikipedia.org/wiki/File:Yalta_summit_1945_with_Churchill,_Roosevelt,_Stalin.jpg "fig:Yalta_summit_1945_with_Churchill,_Roosevelt,_Stalin.jpg")、[罗斯福和](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")[斯大林](../Page/斯大林.md "wikilink")\]\]

**雅尔塔会议**（；），又称**雅爾達密約**，是[美国](../Page/美国.md "wikilink")、[英国和](../Page/英国.md "wikilink")[苏联三国領袖](../Page/苏联.md "wikilink")——[美国总统](../Page/美国总统.md "wikilink")[富兰克林·德拉诺·罗斯福](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")、[英国首相](../Page/英国首相.md "wikilink")[温斯顿·丘吉尔和](../Page/温斯顿·丘吉尔.md "wikilink")[苏联人民委员会主席](../Page/苏联人民委员会主席.md "wikilink")[约瑟夫·斯大林於](../Page/约瑟夫·斯大林.md "wikilink")1945年2月4日至2月11日期间，在[苏联](../Page/苏联.md "wikilink")[克里米亚](../Page/克里米亚.md "wikilink")[雅尔塔](../Page/雅尔塔.md "wikilink")[里瓦幾亞宫内举行之一次首脑会议](../Page/里瓦幾亞宫.md "wikilink")。这次会议，制定了第二次世界大战战后的世界新秩序和列强利益分配方針，形成了「[雅尔塔体系](../Page/雅尔塔体系.md "wikilink")」，对[第二次世界大战后的世界局勢产生了深远的影响](../Page/第二次世界大战.md "wikilink")。

这次会议是继1943年[德黑兰会议后](../Page/德黑兰会议.md "wikilink")，第二次[同盟國首脑会议](../Page/同盟國.md "wikilink")，惟結論在1945年7至8月之[波茨坦会议就有争议](../Page/波茨坦会议.md "wikilink")。許多人批评此次會議，使蘇聯及各國[共产党得以控制](../Page/共产党.md "wikilink")[中欧](../Page/中欧.md "wikilink")、[东欧以及](../Page/东欧.md "wikilink")[亞洲許多](../Page/亞洲.md "wikilink")[國家](../Page/國家.md "wikilink")，主要是會中[羅斯福以及](../Page/羅斯福.md "wikilink")[邱吉爾都未尊重战时被佔領国家之期望](../Page/邱吉爾.md "wikilink")，將被苏联“解放”之国家先交由[聯合國代管](../Page/聯合國.md "wikilink")（联合国1945年10月才成立，甚至连联合国宪章于雅尔塔会议结束2个月后才签署。且战后盟军没有任何一方将位于欧、亚的曾被占领国交由联合国代管；联合国曾托管的11个地区，其中7個在[非洲](../Page/非洲.md "wikilink")、4個在[大洋洲](../Page/大洋洲.md "wikilink")）。此外，為爭取蘇聯對日本宣戰，包括帮助中国从[日本关东军手中夺取满洲国](../Page/日本.md "wikilink")，會中部份內容“侵犯”[中國權益甚大](../Page/中國.md "wikilink")。會前其他国家並不知情，故有“雅爾達密約”之稱。

1944年6月，随着[诺曼底登陆](../Page/诺曼底登陆.md "wikilink")，盟军在[德国西线展开大规模反攻](../Page/德国.md "wikilink")，第三帝国已无力回天。1945年2月，美英认为有必要和苏联商讨德国战败后对欧的权力分配问题，并商讨下一步对日作战。对欧洲大陆，希望苏军加强对德攻势，基本原则以谁攻克的地区战后由谁控制，辅以对重要地区相互交换。在东南亚，美军在[菲律宾开展一系列登陆战](../Page/菲律宾.md "wikilink")，开始把对日作战的重点从海战转向陆战；同期自1944年6月起对日本本土的B29大规模无差别轰炸效果有限，1945年2月4日[李梅刚展开烧夷弹战术试验](../Page/柯蒂斯·李梅.md "wikilink")；在东亚大陆上中国军队尚与日军僵持，日军占领大片中国土地；而[满洲地区则透過](../Page/满洲.md "wikilink")[滿洲國拥有大片土地](../Page/滿洲國.md "wikilink")，重工业设施，70万[关东军尚未直接参战](../Page/关东军.md "wikilink")。因此美英希望苏联尽快转入对[东北亚日军的进攻](../Page/东北亚.md "wikilink")，打消日本依托东亚、东北亚，与盟国长期僵持并得以谈判的念头。

## 背景

1944年7月19日，美国总统罗斯福致函苏联人民委员会主席斯大林，希望再次举行美、英、苏三国首脑会议。斯大林回信表示，他要亲自指挥苏军对德作战，不可出席。1944年10月，英国首相丘吉尔赴[莫斯科会晤斯大林](../Page/莫斯科.md "wikilink")，英苏单独讨论[欧洲和](../Page/欧洲.md "wikilink")[巴尔干问题](../Page/巴尔干.md "wikilink")。美国驻苏大使[哈里曼以观察员身份列席丘吉尔与斯大林之会谈](../Page/W·埃夫里尔·哈里曼.md "wikilink")（史称第四次莫斯科会议）。1944年12月，哈里曼奉命拜会斯大林，询问苏联有关参加对日作战及参战条件等事项。1944年底，三国一致同意三国首脑再次会晤\[1\]。

在1945年2月4日至11日，[三巨头](../Page/三巨头.md "wikilink")——[富兰克林·德拉诺·罗斯福](../Page/富兰克林·德拉诺·罗斯福.md "wikilink")、[温斯顿·丘吉尔和](../Page/温斯顿·丘吉尔.md "wikilink")[斯大林聚集在](../Page/斯大林.md "wikilink")[克里米亚半岛的](../Page/克里米亚半岛.md "wikilink")[雅尔塔](../Page/雅尔塔.md "wikilink")（前沙皇[尼古拉二世的之行宫](../Page/尼古拉二世_\(俄罗斯\).md "wikilink")-{里}-瓦幾亞宮）举行会议。這是二戰期間，繼1943年的[德黑兰会议之後](../Page/德黑兰会议.md "wikilink")，盟國領袖的第二次重要會議。罗斯福去世後，再舉行*[波茨坦会议](../Page/波茨坦会议.md "wikilink")*。

這次會議在蘇聯境內進行，是因為斯大林拒絕到[黑海雅爾塔行宮以外開會](../Page/黑海.md "wikilink")。結果，丘吉爾和羅斯福必須再度遠涉重洋。

会前未邀請盟軍中國戰區最高統帥[蒋介石参加](../Page/蒋介石.md "wikilink")。一方面，斯大林向来讨厌蒋介石；另一方面，也有学者认为罗斯福此时对蒋介石失去信心\[2\]。

## 概观

三巨頭各自带来自己之議程。羅斯福希望遊說蘇聯終止與日本不作戰的協定、對日宣戰，並且支持盟國在[太平洋作戰](../Page/太平洋.md "wikilink")；邱吉爾希望斯大林在[東歐戰後能放弃部分控制权](../Page/東歐.md "wikilink")，“自由”選出“民主”政府；斯大林則认为战争中苏联付出巨大代价，希望控制東歐，因為對蘇聯的戰略布局極重要。

還有，三方領袖尚須建立章程以管理戰後的德國。1943年，[威廉·克里斯林·布利特之論點預告](../Page/威廉·克里斯林·布利特.md "wikilink")“紅色阿米巴變形蟲”正進入歐洲。1943年12月底時，戰線仍在蘇聯境內；到了1944年8月，[蘇聯紅軍已經在波蘭及](../Page/蘇聯紅軍.md "wikilink")[羅馬尼亞境內](../Page/羅馬尼亞.md "wikilink")，且繼續向西前進\[3\]。會議進行時，[格奧爾吉·朱可夫](../Page/格奧爾吉·朱可夫.md "wikilink")[元帥之部隊已經到達](../Page/元帥.md "wikilink")[柏林](../Page/柏林.md "wikilink")40[英哩外地方](../Page/英哩.md "wikilink")，史達林自信可在會議上採取強勢以爭取條件，而羅斯福則希望史達林承諾蘇聯加入[聯合國](../Page/聯合國.md "wikilink")。

> “對於俄國人來說，波蘭問題不僅是榮譽問題，而且是安全問題。在整個[歷史上](../Page/歷史.md "wikilink")，波蘭一直是敵人入侵俄國的走廊，波蘭是關係到俄國生死存亡之問題。”

因此，斯大林清楚表達他對於波蘭的所求是沒有談判空間：蘇聯將從波蘭東部獲得領土，而波蘭將擴張西面領土作為補償。在這個要求下，波蘭西面數以百萬計的德國人將要離開。儘管建立了一個共產黨政府，斯大林答應波蘭會有自由選舉。1947年1月波蘭舉行選舉，結果在1949年，波蘭就成為[社會主義國家](../Page/社會主義.md "wikilink")。

有人認為羅斯福要求蘇聯對日宣戰之焦慮是多余；事實上，斯大林很緊張能否一雪昔年[日俄戰爭之失敗](../Page/日俄戰爭.md "wikilink")；他也想得回昔年損失之領土，並擴張蘇聯的影響力及於東亞。

不過，對於羅斯福是否有意讓紅軍登陸日本本土，就存在“爭議”。1945年，美军在2月雅尔塔会议后即在西太平洋开展2月硫磺岛战役、4月冲绳战役等一系列夺岛战，遭遇日军强烈抵抗，伤亡惨重，预计进攻日本本岛，需付出伤亡美军百万，英军五十万的代价。罗斯福本人希望苏联亦出兵日本本岛，分担一部分代价，并焦慮蘇聯是否肯對日宣戰。且罗斯福同意了由苏联进攻柏林，以避免英美军队伤亡。1945年4月12日，在苏联进攻柏林的前4天，罗斯福去世。

1945年8月6日和9日，美国在日本投下两颗原子弹，8月8日苏联全线出兵百万对日宣战；与夺岛战遇到的下层日军强烈抵抗截然相反，日本上层在双重压力下于8月10日6时通过中立国瑞典、瑞士向盟国转达，在保证天皇地位的前提下有条件投降；后在未获答复的情况下于8月15日由昭和天皇广播宣布无条件投降。投降之快出乎盟国意外。

罗斯福的幕僚和继任者杜鲁门副总统，对羅斯福是否有意讓紅軍登陸日本本土，就存在“爭議”。原本有美国将领希望能以无差别战略轰炸迫使日本投降，从而避免登陆战的巨大伤亡；繼羅斯福之位的[杜魯門](../Page/杜魯門.md "wikilink")，更认为使用[原子彈即可結束戰爭](../Page/原子彈.md "wikilink")；均希望避免紅軍攻進日本本土，使蘇聯無法如在德國般地瓜分日本的利益。

实际李梅于2月提出的烧夷弹战术，在日本则取得比在欧洲使用的高空轰炸更好的效果，造成的傷亡比後來的原子彈攻擊還要多，可用的原子弹也仅2枚。日本本土非但没有因B-29轰炸造成的伤亡退缩，甚至上下宣扬“举国玉碎”。同时各国民间对原子弹缺乏认识，没有辐射后效概念，原子弹造成的直接伤亡有限，直观上只是一个大炸弹。日本籍希望苏联中立，决心以满洲为依托，僵持、拒绝投降且最终迫使盟国与其谈判，使盟国無法如在德國般地瓜分日本的利益。
[Livadiya_Conference.JPG](https://zh.wikipedia.org/wiki/File:Livadiya_Conference.JPG "fig:Livadiya_Conference.JPG")

作为一个附带影响，俄國人並沒有因此解決當初日本南千岛群岛的主權問題至今。

> “重建歐洲的秩序和國民经济的大前提是：徹底消滅[納粹主義和](../Page/納粹主義.md "wikilink")[法西斯主義](../Page/法西斯主義.md "wikilink")，和由被解放的人民建立他們自己選擇的民主体制。”

[Livadia_Palace_Crimea.jpg](https://zh.wikipedia.org/wiki/File:Livadia_Palace_Crimea.jpg "fig:Livadia_Palace_Crimea.jpg")\]\]

## 會議重點

  - 协定最重要是[纳粹德国](../Page/纳粹德国.md "wikilink")[無條件投降](../Page/無條件投降.md "wikilink")，這樣保證盟國團結，盟國任何一方都不可能和納粹有任何談判空間。战后德国和柏林将被分成四个占领区。在德国统一前，柏林将由四国占领。
    [德国被分裂](../Page/德国.md "wikilink")，美英苏认为这是“今后和平與安全的必要条件”。
  - 斯大林同意[法国將在德国和奥地利拥有第四个占领区](../Page/法国.md "wikilink")，但必須成立於美國和英國區以外；並同意法国有资格进入同盟国对德国控制委员会。
  - 德國将会进行非军事化和[去纳粹化](../Page/去纳粹化.md "wikilink")。
  - 德国战争赔款部份以强迫劳动形式來支付。
  - 建立一個位於蘇聯之賠償理事會。
  - [波兰地位問題](../Page/波兰.md "wikilink")。盟国同意重組當時之波兰政府，加入其他組織，例如波兰[临时政府進行](../Page/临时政府.md "wikilink")[民主选举](../Page/民主选举.md "wikilink")，但排除了1939年所成立的[波兰流亡政府參與其中](../Page/波兰流亡政府.md "wikilink")。
  - 波兰东部边界将按照[寇松线](../Page/寇松线.md "wikilink")，同时波兰將从德国得到西部領土补偿，而精确的边界将晚些决定。
  - 斯大林承諾允許在波蘭自由選舉。
  - 無視苏联和[南斯拉夫](../Page/南斯拉夫.md "wikilink")[战俘意願](../Page/战俘.md "wikilink")，他們將會被送回國。
  - 斯大林同意在击败德国以后三個月内，加入对日本作战。苏联战胜日本后，将可收復[库页岛南部](../Page/库页岛.md "wikilink")、獲得[千岛群岛](../Page/千岛群岛.md "wikilink")，並保障其在[大連港](../Page/大連港.md "wikilink")、[中東鐵路](../Page/中東鐵路.md "wikilink")、[南滿鐵路的利益](../Page/南滿鐵路.md "wikilink")，以及恢復[俄羅斯海軍在](../Page/俄羅斯海軍.md "wikilink")[旅顺口的租賃作为报酬](../Page/旅顺口.md "wikilink")。
  - 羅斯福得到斯大林承諾，蘇聯將加入联合国，同意[联合国安全理事会五個](../Page/联合国安全理事会.md "wikilink")[常任理事國均將享有](../Page/聯合國安全理事會常任理事國.md "wikilink")[否決權](../Page/聯合國安全理事會否決權.md "wikilink")。
  - 納粹戰犯都會懲處，並繩之以法。
  - 將成立一個「决定分裂德国之委員會」。委員會將决定德国是否要分成六个国家，如果要分割，几个新德意志国家之间的边界和相互关系又将如何确定。
  - 成立新国际组织（[联合国](../Page/联合国.md "wikilink")），取代失敗的[国际联盟](../Page/国际联盟.md "wikilink")。

## 會議協議

  - 所有被[解放之欧洲](../Page/解放.md "wikilink")[国家内应该举行](../Page/国家.md "wikilink")[民主](../Page/民主.md "wikilink")[选举](../Page/选举.md "wikilink")。
  - 4月在[旧金山进行就联合国成立之会议](../Page/旧金山.md "wikilink")。基本确定联合国组织方式，采纳联合国安全理事会之主意。美国和英国同意当时属於苏联之[乌克兰苏维埃社会主义共和国和](../Page/乌克兰苏维埃社会主义共和国.md "wikilink")[白俄罗斯苏维埃社会主义共和国为联合国独立成员](../Page/白俄罗斯苏维埃社会主义共和国.md "wikilink")。
  - 分裂德国，解散[納粹德军](../Page/納粹德军.md "wikilink")，德国不准再拥有军队。美英苏认为这是“今后和平和安全之必要条件”。
  - 德国应该为“她对同盟国在战争中造成的之损失”负战争赔款。战争赔款可以以德国国家资源（机器、船只、企业所有等）、一段时间内应该支付的之偿款或劳动力的之方式赔偿。美国和苏联达成协议偿款总额约220亿美元。英国认为在当时偿款总额还无法估计。
  - 暂时搁置[战争罪问题](../Page/战争罪.md "wikilink")。
  - 一个“广泛民主临时政府”，应在波兰“尽快进行自由和不受他国控制、全民和秘密之选举”。
  - 应在南斯拉夫建立一个保皇党和共产党联合政府。
  - 暂时搁置关于[意大利](../Page/意大利.md "wikilink")—南斯拉夫、意大利—[奥地利](../Page/奥地利.md "wikilink")、南斯拉夫—[保加利亚](../Page/保加利亚.md "wikilink")、罗马尼亚、[伊朗以及](../Page/伊朗.md "wikilink")[土耳其管理之黑海与](../Page/土耳其.md "wikilink")[地中海间之海峡使用之问题](../Page/地中海.md "wikilink")。
  - 不论被俘苏联公民愿不愿意，一律遣返回苏联。

就未来德国问题之处理，雅尔塔协议相当含糊，用词也為後來東西德分裂詮釋造就許多想像空間，亦有人認為這次會議為戰後[冷战之濫觴](../Page/冷战.md "wikilink")。

這次會議的結果，就事後來看，永遠的改變了許多國家的命運（東歐諸國成为蘇聯[衛星国](../Page/衛星国.md "wikilink")、南北韓分裂、[外蒙古獨立和](../Page/外蒙古獨立.md "wikilink")[中國國共内戰等](../Page/中國國共内戰.md "wikilink")），也懸留許多問题至今尚未解決。

## 后果

[Yalta1945.gif](https://zh.wikipedia.org/wiki/File:Yalta1945.gif "fig:Yalta1945.gif")

雅尔塔会议是二战欧洲战事结束前及罗斯福总统去世前，最后一次的重要会议。東歐大部分地區的納粹軍隊已被紅軍消滅了，所以斯大林有條件和美英談判，結果蘇聯在東歐得到一個夢寐以求之緩衝區。過程中，一些小國利益因為要保持盟軍內部穩定而被犧牲，蘇聯繼續統治[波罗的海国家](../Page/波罗的海国家.md "wikilink")：[拉脱维亚](../Page/拉脱维亚.md "wikilink")、[立陶宛和](../Page/立陶宛.md "wikilink")[爱沙尼亚](../Page/爱沙尼亚.md "wikilink")。

雅尔塔会议促使苏联对满洲关东军发动进攻，而苏联曾承认满洲国，将满洲作为独立的敌对国看待，随后[中國与苏联签订](../Page/中華民國_\(大陸時期\).md "wikilink")《[中苏友好同盟条约](../Page/中苏友好同盟条约.md "wikilink")》，以苏联击败关东军后退出满洲为条件，允诺[外蒙古在战后进行公民投票以决定其前途](../Page/外蒙古.md "wikilink")。最后在苏联和中华民国监视下，公民投票赞成独立。1946年1月，中華民國政府通知[库伦方面](../Page/烏蘭巴托.md "wikilink")，同意其独立；1947年7月28日，中華民國駐聯合國代表[徐淑希在联合国安理會发表演說](../Page/徐淑希.md "wikilink")，指責蒙古人民共和国軍隊入侵中國新疆，反對外蒙古加入聯合國\[4\]。1955年12月13日，中华民国代表在安理會否決蒙古人民共和国加入联合国，理由是全蒙古是中国的一部分。\[5\]\[6\]\[7\]\[8\]。1961年在美国和苏联的压力下，中華民國最終不得不在[缺席的情況下讓外蒙入聯](../Page/聯合國安理會166號決議.md "wikilink")。

另一方面，**雅爾達會議更造成了今日的領土、領海問題**。邱吉爾、羅斯福為了要讓蘇聯參戰，同意了史達林的要求：**「南[庫頁半島](../Page/薩哈林島.md "wikilink")、[北方四島和部分的](../Page/北方四島.md "wikilink")[阿留申群島歸](../Page/阿留申群島.md "wikilink")[蘇聯所有](../Page/蘇聯.md "wikilink")，不得異議，[蘇聯便參戰](../Page/蘇聯.md "wikilink")。」**在苏联参战之后又有部分反悔，造成了今日的[北方四島](../Page/北方四島.md "wikilink")、[釣魚台列島和靠近](../Page/釣魚台列島.md "wikilink")[美國](../Page/美國.md "wikilink")[阿拉斯加州的幾個離島的經濟海域](../Page/阿拉斯加州.md "wikilink")、[荷蘭港外圍的海底資源探勘等地的諸多國際問題](../Page/荷蘭港.md "wikilink")。

經此會議後。蘇聯即对日宣戰促成五日后[日本投降](../Page/日本投降.md "wikilink")（一次性出动89個師包括1,500,000人、3,704輛[坦克](../Page/坦克.md "wikilink")、1,852具[自走炮](../Page/自走炮.md "wikilink")、85,819部車輛及3,721架飛機，徹底地压垮日军已打算“举国玉碎”的顽抗意志），並出兵進攻[日本](../Page/日本.md "wikilink")[關東軍侵占的中國東北的](../Page/關東軍.md "wikilink")[嫩江省](../Page/嫩江省.md "wikilink")、[黑龍江省和](../Page/黑龍江省.md "wikilink")[察哈爾省等地](../Page/察哈爾省.md "wikilink")。在戰後亦不退還給[中国](../Page/中国.md "wikilink")[国民政府](../Page/国民政府.md "wikilink")，而是交給扶植的[中國共產黨](../Page/中國共產黨.md "wikilink")。当时东北没有国民政府所属的军队，跟从[蔣介石的军队如东北军早已溃败入关](../Page/蔣介石.md "wikilink")，跟从[汪精衛的军队](../Page/汪精衛.md "wikilink")，则在[汪精衛政府承认](../Page/汪精衛政府.md "wikilink")[满洲国的背景下](../Page/满洲国.md "wikilink")，转化为滿洲國军队，只有[东北抗日联军在东北坚持抗战](../Page/东北抗日联军.md "wikilink")14年，其间在未受蔣介石援助又遭日、滿联军清剿的情况下数次几近灭绝。而後又在[中国共产党和](../Page/中国共产党.md "wikilink")[苏联的扶植下几度壮大](../Page/苏联.md "wikilink")。1946年蒋介石要求苏联暂缓从滿洲撤军，苏联大使3月22日通知[中国](../Page/中国.md "wikilink")[国民政府](../Page/国民政府.md "wikilink")，4月底以前苏军将全部撤离滿洲，但国民政府的军队仍没有足够的兵力进入滿洲。

## 参考文献

### 引用

### 来源

  - Best, Geoffrey. *Churchill: A Study in Greatness*. London: Hambledon
    and London, 2001.
  - Black, Conrad. *Franklin Delano Roosevelt: Champion of Freedom*.
    Cambridge, MA: Perseus Books Group, 2003.
  - Clemens, Diane S. "Yalta Conference." World Book. 2006 ed. vol. 21.
    2006, 549.
  - "[Cold War: Teheran
    Declaration."](https://web.archive.org/web/20060118153742/http://www.cnn.com/SPECIALS/cold.war/episodes/01/documents/yalta.html).
    CNN. 1998. 26 March 2006.
  - Meacham, John. *Franklin and Winston: An Intimate Portrait of an
    Epic Friendship*. New York: Random House Inc., 2003.
  - O’Neil, William L. *World War II: a Student Companion*. New York:
    Oxford UP, 1999.
  - Perisco, Joseph E. *Roosevelt’s Secret War*. New York: Random House,
    2001.
  - “Portraits of Presidents: Franklin D. Roosevelt.” School Arts
    Magazine February 1999: 37. Student Research Center. EBSCO Host.
    Philadelphia. 2 Apr. 2006. Keyword: FDR.
  - Snyder, Louis L. *World War II*. New York: Grolier Company, 1981.
  - Sulzberger, C L. *American Heritage New History of World War II*.
    Ed. Stephen E. Ambrose. New York: Viking Penguin, 1998.
  - Waring, J. G. *A student's experience of Yalta*
  - "Yalta Conference." Funk and Wagnalls New Encyclopedia. World
    Almanac Education Group, 2003. SIRS DISCOVER. Philadelphia. 2 April
    2006. Keyword: Yalta Conference.

## 外部链接

  - [Foreign relations of the United States. Conferences at Malta and
    Yalta, 1945](http://digital.library.wisc.edu/1711.dl/FRUS.FRUS1945)
  - [WW2DB: 雅尔塔会议](http://ww2db.com/battle_spec.php?battle_id=78)
  - [Protocol of proceedings of Crimea
    Conference](http://www.taiwandocuments.org/yalta.htm)
  - [The Yalta
    Betrayal](https://web.archive.org/web/20021018055456/http://www.geocities.com/Pentagon/6315/yaltabet.html)
  - [*Mr Bush and the Riga
    axioms*](http://svaradarajan.blogspot.com/2005/05/mr-bush-and-riga-axioms.html)
    (Positive view on Yalta)
  - [MilitaryHistoryOnline Yalta
    Conference](http://www.militaryhistoryonline.com/wwii/articles/yalta.aspx)
  - [How good was the Good
    War?](http://www.boston.com/news/globe/ideas/articles/2005/05/08/how_good_was_the_good_war?pg=full)
  - [The Division of
    Europe](http://www.cvce.eu/obj/the_division_of_europe-en-d01c006d-55ec-4c69-a3d2-f4577f0e9b4c.html)
    Online Documents Collection
  - [The Real Myths of Yalta](http://hnn.us/roundup/entries/11904.html)
  - [Special German series 2. The Committee on Dismemberment of
    Germany](http://www.fdrlibrary.marist.edu/psf/box32/t298x01.html)
    Allied discussions on the dismemberment of Germany into separate
    states, March 29, 1945. The commite was ordained at Yalta.
  - [Yalta casts its shadow 60 years
    on](http://news.bbc.co.uk/2/hi/europe/4241863.stm),
    [BBC](../Page/BBC.md "wikilink"), [February
    7](../Page/February_7.md "wikilink"), 2005

## 参见

  - [第二次世界大戰](../Page/第二次世界大戰.md "wikilink")
  - [开罗会议](../Page/开罗会议.md "wikilink")
  - [德黑蘭會議](../Page/德黑蘭會議.md "wikilink")
  - [波茨坦会议](../Page/波茨坦会议.md "wikilink")
  - [冷戰](../Page/冷戰.md "wikilink")

{{-}}

[Category:乌克兰历史](../Category/乌克兰历史.md "wikilink")
[Category:第二次世界大战重大会议](../Category/第二次世界大战重大会议.md "wikilink")
[Category:1945年2月](../Category/1945年2月.md "wikilink")
[Category:富蘭克林·德拉諾·羅斯福](../Category/富蘭克林·德拉諾·羅斯福.md "wikilink")
[Category:丘吉尔](../Category/丘吉尔.md "wikilink")
[Category:斯大林](../Category/斯大林.md "wikilink")
[Category:克里米亞歷史](../Category/克里米亞歷史.md "wikilink")

1.
2.
3.  Traktuyev, Michael Ivanovich, *The Red Army's Drive into Poland* in
    *Purnell's History of the Second World War*, editor Sir Basil
    Liddell Hart, Hatfield, UK, 1981, vol.18, p.1920-1929
4.
5.
6.
7.
8.