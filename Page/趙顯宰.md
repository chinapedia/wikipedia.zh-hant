**趙顯宰**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。已婚，長子於2018年11月20日出生。

## 經歷

曾作為歌手，以組合Guardian發行僅有一張唱片。因為一直夢想成為演員，之后轉換身份努力至今，曾因在首次擔當主演的電視劇《[情書](../Page/情書_\(電視劇\).md "wikilink")》中有出色發揮，而受到影迷們的廣泛關注與喜愛。也因為這部作品，在東南亞等地以韓流明星身份而大受歡迎。

尤其是在[日本](../Page/日本.md "wikilink")，還參與過日韓合作拍攝的劇集《[星的聲音](../Page/星的聲音.md "wikilink")》，2008年入伍前夕更是召開了多場影迷見面會證明了在日本的人气地位。

2007年7月，為了韓國著名設計師[安德烈·金的服裝表演來到](../Page/安德烈·金.md "wikilink")[中國](../Page/中國.md "wikilink")，在以往的共同演出中，也和一些明星成為朋友，影迷們較為熟知的有[池珍熙](../Page/池珍熙.md "wikilink")、[秀愛](../Page/秀愛.md "wikilink")、[韓彩英](../Page/韓彩英.md "wikilink")、[在喜等](../Page/在喜.md "wikilink")。2008年8月5日入伍服役，並發行為答謝影迷而專門錄制的CD。2010年6月19日正式退伍。隔年3月以[SBS水木劇](../Page/SBS.md "wikilink")《[49天](../Page/49天.md "wikilink")》正式回歸螢幕。

## 演出作品

### 電視劇

  - 1999年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[KAIST](../Page/KAIST_\(電視劇\).md "wikilink")》飾演
    張東和
  - 2001年：SBS《[父與子](../Page/父與子_\(韓國電視劇\).md "wikilink")》
  - 2001年：SBS《[真的很好](../Page/真的很好.md "wikilink")》
  - 2002年：SBS《[黃金池子](../Page/黃金池子.md "wikilink")》
  - 2002年：SBS《[大望](../Page/大望.md "wikilink")》飾演 世子
  - 2003年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[情書](../Page/情書_\(電視劇\).md "wikilink")》飾演
    李有政／安德烈
  - 2003年：SBS《[初戀](../Page/初戀_\(2003年電視劇\).md "wikilink")》
  - 2003年：MBC《》飾演 成宰
  - 2004年：SBS《[陽光照射](../Page/陽光照射.md "wikilink")》飾演 鄭尹憲
  - 2004年：[KBS](../Page/韓國放送公社.md "wikilink")《[九尾狐外傳](../Page/九尾狐外傳.md "wikilink")》飾演
    姜敏宇
  - 2005年：SBS《[只有你](../Page/只有你_\(2005年電視劇\).md "wikilink")》飾演 韓爾俊
  - 2005年：SBS《[薯童謠](../Page/薯童謠_\(電視劇\).md "wikilink")》飾演 薯童 /
    [百濟武王](../Page/百濟武王.md "wikilink")
  - 2008年：KBS《[三個爸爸一個媽媽](../Page/三個爸爸一個媽媽.md "wikilink")》飾演 韓秀賢
  - 2011年：SBS《[49日](../Page/49日.md "wikilink")》飾演 韓康
  - 2013年：中國《[胭脂霸王](../Page/胭脂霸王.md "wikilink")》飾演 蕭武魁
  - 2013年：KBS《[廣告天才李太白](../Page/廣告天才李太白.md "wikilink")》飾演 Addie姜
  - 2013年：MBC《[帝王之女守百香](../Page/帝王之女守百香.md "wikilink")》飾演
    [百濟聖王](../Page/百濟聖王.md "wikilink")
  - 2014年：中國《[青春風暴](../Page/青春風暴.md "wikilink")》飾演 亦子軒
  - 2015年：SBS《[龍八夷](../Page/龍八夷.md "wikilink")》飾演 韓道俊
  - 2017年：MBC《[醫療船](../Page/醫療船_\(韓國電視劇\).md "wikilink")》飾演 張盛浩（特別出演）
  - 2018年：SBS《[如果是她的話](../Page/如果是她的話.md "wikilink")》飾演 姜燦基
  - 2018年：《[熱血主婦名偵探](../Page/熱血主婦名偵探.md "wikilink")》飾演 車載勳（取消製作）

### 電影

  - 2003年：《[醜聞—朝鮮男女相悅之詞](../Page/醜聞—朝鮮男女相悅之詞.md "wikilink")》
  - 2004年：《[愛的喜悅](../Page/愛的喜悅.md "wikilink")》
  - 2008年：《[惡靈碉堡](../Page/惡靈碉堡.md "wikilink")[G.P.506](../Page/G.P.506.md "wikilink")》
  - 2014年：《[女演員太過分](../Page/女演員太過分.md "wikilink")》

### MV

  - 2007年：SeeYa《愛情的問候》（與[李枖原](../Page/李枖原.md "wikilink")）
  - 2007年：SeeYa《冰偶人》（與[李枖原](../Page/李枖原.md "wikilink")）

## 廣告代言

  - 口香糖廣告（與[金荷娜](../Page/金荷娜.md "wikilink")）
  - 咖啡飲料廣告（與[金荷娜](../Page/金荷娜.md "wikilink")）
  - 2003年：春夏秋冬季系列 Crencia服飾

## 獲獎

  - 2003年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－最佳男新人獎（初戀）
  - 2005年：SBS演技大賞－十大明星奖（只有你、薯童谣）
  - 2006年：第42届[百想艺术大赏](../Page/百想艺术大赏.md "wikilink")－TV部门人气赏（薯童谣）
  - 2007年：安德烈金最佳明星奖－明星奖
  - 2011年：第19届大韩民国文化演艺大奖－韩流明星奖

## 外部連結

  -
  -
  -
  - [daum cafe](http://cafe.daum.net/hyunjaelove)

[J](../Category/韓國電視演員.md "wikilink")
[J](../Category/檀國大學校友.md "wikilink")
[J](../Category/首爾特別市出身人物.md "wikilink")
[J](../Category/趙姓.md "wikilink")