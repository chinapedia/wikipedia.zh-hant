**奥利弗·雷金纳德·坦博国际机场**（，）是一座位于[南非](../Page/南非.md "wikilink")[約翰尼斯堡附近](../Page/約翰尼斯堡.md "wikilink")[肯普頓公園的国际](../Page/肯普頓公園_\(豪登省\).md "wikilink")[机场](../Page/机场.md "wikilink")，每年服務約1600萬乘客。坦博国际机场是[南非航空的基地](../Page/南非航空.md "wikilink")，亦是[南非以至](../Page/南非.md "wikilink")[非洲最繁忙的機場](../Page/非洲.md "wikilink")。

## 歷史

O·R·坦博国际机场於1952年啟用，最初被命名为**[扬·史末资机场](../Page/扬·史末资.md "wikilink")**。在1994年，新成立的南非政府将本机场改名为**约翰内斯堡国际机场**，但在2006年10月27日再度更名为O·R·坦博国际机场，以纪念已故的前[南非非洲人国民大会主席](../Page/南非非洲人国民大会.md "wikilink")[奥利弗·坦博](../Page/奥利弗·坦博.md "wikilink")。

1996年，本机场超越[開羅國際機場成為非洲最繁忙機場](../Page/開羅國際機場.md "wikilink")，目前是世界上最繁忙的100个機場之一。

2006年11月26日，[空中巴士A380客机在一次经](../Page/空中巴士A380.md "wikilink")[南极的测试飞行中降落于坦博国际机场](../Page/南极.md "wikilink")，使本机场成为第一个迎接A380的非洲機場。

从2012年1月31日起，南非航空公司开通了约翰内斯堡－北京的航线，每周将运营三个航班，周二，周四和周六，全程飞行时间大约为１５个小时。\[1\]

2013年1月10日，奧利弗·坦博機場的ICAO代碼由原先的**FAJS**改為**FAOR**。\[2\]

## 机场信息

由於機場位於高地，航機受到稀薄的大氣所影響，需要較長的[跑道來達到正常的起飛速度](../Page/跑道.md "wikilink")。此機場的03L/21R長度超過4400米，是目前全世界最長的跑道之一。

## 航空公司及航点

### 客運

### 貨運

  - [非洲國際航空](../Page/非洲國際航空.md "wikilink")
  - [亞特拉斯航空](../Page/亞特拉斯航空.md "wikilink")
  - [卢森堡货运航空](../Page/卢森堡货运航空.md "wikilink")
  - [DAS Air Cargo](../Page/DAS_Air_Cargo.md "wikilink")
  - [Emirates SkyCargo](../Page/Emirates_SkyCargo.md "wikilink")
  - [Fast Air](../Page/Fast_Air.md "wikilink")
  - [汉莎航空货运](../Page/汉莎航空货运.md "wikilink")
  - [馬丁航空](../Page/馬丁航空.md "wikilink")
  - [MK Airlines](../Page/MK_Airlines.md "wikilink")
  - [Ocean
    Airlines](../Page/Ocean_Airlines.md "wikilink")（開辦日期有待確定）\[3\]
  - [新加坡航空貨運](../Page/新加坡航空貨運.md "wikilink")
  - [Tramon Air](../Page/Tramon_Air.md "wikilink")
  - [WDA](../Page/Wimbi_Dira_Airways.md "wikilink")

## 地面運輸

### 軌道交通

國內和國際航站樓之間有中轉站。當中設有連接機場和[桑頓](../Page/桑頓.md "wikilink")（一個主要的商業區和旅遊區）的[豪登列車車站](../Page/豪登列車.md "wikilink")，並能夠從車站開始乘坐豪登列車其他系統。

2006年9月，[豪登省與](../Page/豪登省.md "wikilink")[龐巴迪運輸簽訂建造一條連接](../Page/龐巴迪運輸.md "wikilink")[約翰內斯堡](../Page/約翰內斯堡.md "wikilink")、[比勒陀利亞和機場的鐵路系統](../Page/比勒陀利亞.md "wikilink")，並立即開始施工\[4\]。連接機場與約翰內斯堡桑頓的路段於2010年6月8日完成，趕及世界盃。列車每天運行90次，每天運送約8,000名乘客\[5\]。

### 汽車

機場位於約翰內斯堡東北部的24號省道機場高速公路上，可通過24號省道和高速公路輕鬆抵達機場。24號省道與機場附近的21號省道和奧利弗·坦博國際機場高速公路相交。這條高速公路穿過機場航站樓，將其與停車位分開，但它分為“離境”和“入境”兩個方向，然後重新分支到交叉路口。

## 參考文獻

## 外部連結

  -
  -
[Category:南非機場](../Category/南非機場.md "wikilink")
[Category:約翰內斯堡機場](../Category/約翰內斯堡機場.md "wikilink")
[Category:约翰内斯堡](../Category/约翰内斯堡.md "wikilink")
[Category:南非之最](../Category/南非之最.md "wikilink")
[T](../Category/冠以人名的非洲機場.md "wikilink")
[Category:1952年启用的机场](../Category/1952年启用的机场.md "wikilink")

1.  <http://news.xinhuanet.com/travel/2012-02/07/c_122668738.htm>
2.  [South African Civil Aviation Authority, AIRAC AIP Supplement
    S087/12, 20
    September 2012](http://www.caa.co.za/resource%20center/AIP%27s/2012/S087-12%20FAJS-FAOR.pdf)
3.  <http://www.oceanairlines.com/pagine/destinations.htm>
4.
5.