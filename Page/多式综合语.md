**多式綜合語**（又稱**編插語**、**相抱语**、**复综语**）是[綜合語的一種](../Page/綜合語.md "wikilink")，擁有極高的綜合語特性。多式綜合語的詞，都由許多的[詞素組成](../Page/詞素.md "wikilink")。

## 定義

如果以「綜合程度」指詞素和詞之間的比例，當某種語言的詞由多於一個詞素組成，這種語言便稱為綜合語。以綜合程度而言，多式綜合語走在「每個詞都由極多的詞素組成」的極端。（在另一個極端，就是一個詞只由一個詞素組成的[孤立語](../Page/孤立語.md "wikilink")。）

<span lang=en>[併合](../Page/併合.md "wikilink")</span>
(主要是[名詞併合進句子裡](../Page/名詞.md "wikilink"))
是多式綜合語有史以來一個令人混淆的特徵,
而此亦因而用來定義多式綜合語。「併合」是指[詞素](../Page/詞素.md "wikilink")
(或稱[词位](../Page/词位.md "wikilink")) 全都結合成一個單字的現象。可是有的併合語不是多式綜合語, 反之亦然。

## 典故

「多式綜合」<span lang=en>(polysynthesis)
</span>一詞作為一個語言學的術語，最早應該是1819年，當<span lang=en>[Peter
Stephen
Duponceau](../Page/Peter_Stephen_Duponceau.md "wikilink")</span>（又名<span lang=en>Pierre
Étienne Duponceau</span>）以它來形容美洲原住民的語言。
「綜合」和「多式綜合」兩個詞的現代用法，最早是在1920年代由<span lang=en>[Edward
Sapir](../Page/Edward_Sapir.md "wikilink")</span>開始。

## 範例

多式綜合語的例子包括[因紐特語](../Page/因紐特語.md "wikilink")、[莫霍克語](../Page/莫霍克語.md "wikilink")、古[愛努語](../Page/愛努語.md "wikilink"),
[Central Siberian
Yupik](../Page/Yupik_language.md "wikilink")、[切羅基語](../Page/切羅基語.md "wikilink")、[Sora](../Page/Sora_language.md "wikilink")、[楚科奇語](../Page/楚科奇語.md "wikilink")、[科里亞克語](../Page/科里亞克語.md "wikilink")、以及北美及西伯利亞的[古西伯利亞語言等多種語言](../Page/古西伯利亞語言.md "wikilink")。

### 楚科奇語

**[楚科奇語](../Page/楚科奇語.md "wikilink")**（一種多式綜合併入[黏著語](../Page/黏著語.md "wikilink")）中的例子：

  -
    <span lang=mis>Tымэвӈылэвтпыгтыpкын.
    (T-ы-мэвӈ-ы-лэвт-пыгт-ы-pкын.)</span>
    **
    <small>[單數](../Page/單數.md "wikilink")[第一人稱](../Page/第一人稱.md "wikilink")</small>.<small>[主語](../Page/主語.md "wikilink")</small>-大(嚴重)-頭-痛-<small>第一人稱[現在式](../Page/現在式.md "wikilink")</small>
    “我有嚴重頭痛。”   <cite lang=en>(Skorik 1961: 102)</cite>

<span lang=mis>T-ы-мэвӈ-ы-лэвт-пыгт-ы-pкын.(*T-ə-meyŋ-ə-levt-pəγt-ə-rkən*)</span>
一詞的語素與詞的比例是5:1，其中有3個（**「大(嚴重)」、**「頭」、**「痛」）為併入詞素。

### 古愛努語

取自**古[愛努語](../Page/愛努語.md "wikilink")**（另一種多式綜合併入黏著語）的例子：

|                                                          |
| -------------------------------------------------------- |
| <span lang=mis>Usaopuspe aejajkotujmasiramsujpa.</span>  |
| **                                                       |
| 種種-謠言                                                    |
| “我的心在種種謠言之下不斷在遠方和自身間搖晃” <small>(即,「我對種種謠言感到疑惑」.)</small> |
| <cite lang=en>(Shibatani 1990: 72)</cite>                |

<span lang=mis>*aejajkotujmasiramsujpa*</span>一字由9個語素組成，當中兩個詞素（**「遠」、**「心」）併入到動詞之中。

### 西格陵蘭語

中[西伯利亞](../Page/西伯利亞.md "wikilink")[尤皮克語](../Page/尤皮克語.md "wikilink")(Yupik
languages)和西[格陵蘭語是高度綜合而沒有併入的語言的例子](../Page/格陵蘭語.md "wikilink")。

以下是一段西格陵蘭語 (一種多式綜合黏著語, 但非併入語) 的例子 (Fortescue 1983:97; 於 Evans and Sasse
2002 引用過), 語素-詞比例為12:1：

  -



    **

    娛樂-提供-<small>[半及物動詞](../Page/及物動詞.md "wikilink")</small>-他精於-<small>[繫詞](../Page/繫詞.md "wikilink")</small>-說-<small>重覆</small>-<small>將來式</small>-肯定的,但是-<small>[第三人稱](../Page/第三人稱.md "wikilink")[眾數主語](../Page/眾數.md "wikilink")</small>-3<small>第三人稱單數[賓語](../Page/賓語.md "wikilink")</small>-但

    「不過，他們會說他是個偉大的娛樂圈人，但……」

### 西北高加索語族

[西北高加索語族的動詞非常多式綜合](../Page/西北高加索語族.md "wikilink")。此語的動詞幾乎與句中所有名詞和[代詞呼應](../Page/代詞.md "wikilink")，亦含小量併合成分。

取自[尤比克语](../Page/尤比克语.md "wikilink")(Ubykh language)的例子：

  -

    \!

    **

    他們-<small>利益格</small>-我-之下-<small>奪格</small>-你-<small>使役</small>-取-<small>反覆</small>-全部-<small>可能語氣</small>-<small>過去式</small>-<small>未完成式</small>-<small>否定</small>-<small>條件語氣</small>-<small>願望語氣</small>

    “只望你從沒有令他又為他們把我之下的全都拿出來\!”

即使沒有像 Ubykh 很多動詞般併入了名詞，這個詞已包含16個明確的語素了。

## 多式綜合語之分布

世界上，很多地方都發展出了多式綜合語。不管有沒有併入成分，有些語系是典型的多式綜合語。它們包括非洲的[班图语](../Page/班图语.md "wikilink")、北美洲和西伯利亞的[阿薩巴斯卡語](../Page/阿薩巴斯卡語.md "wikilink")（Athabaskan）和[因紐特語](../Page/因紐特語.md "wikilink")-[阿留申語](../Page/阿留申語.md "wikilink")、高加索地區的[西北高加索語族和](../Page/西北高加索語族.md "wikilink")[東北高加索語族](../Page/東北高加索語族.md "wikilink")、歐洲的[巴斯克語和](../Page/巴斯克語.md "wikilink")[芬蘭-烏戈爾語系](../Page/芬蘭-烏戈爾語系.md "wikilink")、以及澳洲北部的[Gunwinyguan
語](../Page/Gunwinyguan_語.md "wikilink")。

## 問題

不是所有語言都可被輕易的分類為完全屬於多式綜合語。語素和語素之間、詞和詞之間的界線，有時並不明顯。有些語言可能在某部分比較綜合，但在其他部分比較不綜合。(compare
verbs and nouns in [Southern Athabaskan
languages](../Page/Southern_Athabaskan_languages.md "wikilink")).

## 參見

  - [语言系属分类](../Page/语言系属分类.md "wikilink")

## 參考文獻

  - Baker, Mark. (1988). *Incorporation: A theory of grammatical
    function changing*.

  -
  -
  -
  - Boas, Franz. (1911). *Handbook of American Indian languages* (Part
    1).

  -
  -
  -
  -
  - Comrie, Bernard. (1989). *Language universals and linguistic
    typology* (2nd ed.). Chicago: The University of Chicago Press.

  -
  -
  - Fortescue, Michael. (1983). *A comparative manual of affixes for the
    Inuit dialects of Greenland, Canada, and Alaska*. Meddelelser om
    Grønland, Man & society (No. 4). Copenhagen: Nyt Nordisk Forlag.

  - Fortescue, Michael. (1994). Morphology, polysynthetic. In R. E.
    Asher & J. M. Y. Simpson (Eds.), *The Encyclopedia of language and
    linguistics*.

  -
  - Hewitt, John N. B. (1893). Polysynthesis in the languages of the
    American Indians. *American Anthropologist*, *6*, 381–407.

  - von Humboldt, Wilhelm. (1836). *Über die Verschiedenheit des
    menschichen Sprachbaues und ihren Einfluß auf die geistige
    Entwicklung des Menschengeschlechts*. Berlin: Königliche Akadamie
    der Wissenschaften.

  - Jacobson, Steven A. (1977). *A grammatical sketch of Siberian Yupik
    Eskimo* (pp. 2–3). Fairbanks: Alaska Native Languages Center,
    University of Alaska.

  -
  -
  -
  -
  - de Reuse, Willem J. Central Siberian Yupik as a polysynthetic
    language.

  -
  - Sapir, Edward. (1911). Problem of noun incorporation in American
    Indian languages. *American Anthropologist*, *13*, 250–282.

  - Osborne, C.R., 1974. The Tiwi language. Canberra: AIAS

  -
  -
  - Shopen, Timothy. (1985). *Language typology and syntactic
    description: Grammatical categories and the lexicon* (Vol. 3).
    Cambridge: Cambridge University Press.

  -
  -
  -
[Category:語言類型學](../Category/語言類型學.md "wikilink")