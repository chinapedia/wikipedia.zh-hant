[West_China_Medical_Center_of_SCU.png](https://zh.wikipedia.org/wiki/File:West_China_Medical_Center_of_SCU.png "fig:West_China_Medical_Center_of_SCU.png")

[Scuhuaxi.jpg](https://zh.wikipedia.org/wiki/File:Scuhuaxi.jpg "fig:Scuhuaxi.jpg")[华西医学院钟楼](../Page/华西医学院.md "wikilink")\]\]

**四川大学华西医学中心**（）是[四川大学的一个介于大学和学院之间的行政机构](../Page/四川大学.md "wikilink")。下属学院包括：

  - 华西基础医学与法医学院
  - 华西公共卫生学院
  - [华西口腔医学院](../Page/四川大学华西口腔医院.md "wikilink")
  - 华西临床医学院
  - 华西药学院

还包括四所医院：华西医院、华西第二医院、华西第四医院、华西口腔医院

华西医学中心的前身是**华西医科大学**，其在2000年同[四川大学合并](../Page/四川大学.md "wikilink")。合并前，华西医科大学已发展成为[中国大陆最优秀的医学院之一](../Page/中国大陆.md "wikilink")。华西医学中心的成立是内部各方协商后的结果。

## 历史

[West_China_Union_University.jpg](https://zh.wikipedia.org/wiki/File:West_China_Union_University.jpg "fig:West_China_Union_University.jpg")
**私立[华西协合大学](../Page/华西协合大学.md "wikilink")**，又称**華西聯合大學**。[美国](../Page/美国.md "wikilink")、[英国](../Page/英国.md "wikilink")、[加拿大的](../Page/加拿大.md "wikilink")5个教会于1905年開始籌办**华西协合大学**，校址位于[四川省](../Page/四川省.md "wikilink")[成都市](../Page/成都市.md "wikilink")[府南河畔的](../Page/府南河.md "wikilink")[华西坝](../Page/华西坝.md "wikilink")。华西协合大学於1910年3月11日正式开学。第一任校长是美国人毕启。华西协合大学當時的教授大多来自英国[剑桥大學](../Page/剑桥大學.md "wikilink")、[牛津大學](../Page/牛津大學.md "wikilink")，加拿大[多伦多大學](../Page/多伦多大學.md "wikilink")，美国[哈佛大學](../Page/哈佛大學.md "wikilink")、[耶鲁大學](../Page/耶鲁大學.md "wikilink")。华西坝老建筑群以华西钟楼为代表。\[1\]

1937年，[抗日战争爆发](../Page/抗日战争.md "wikilink")，[金陵大学](../Page/金陵大学.md "wikilink")、[齐鲁大学](../Page/齐鲁大学.md "wikilink")、[金陵女子文理学院](../Page/金陵女子文理学院.md "wikilink")、[燕京大学四所學校也遷到华西坝](../Page/燕京大学.md "wikilink")，在华西协合大学校園上課。抗日战争結束后，各校陆续迁回。\[2\]

1951年被更名為「華西大學」，1953年改稱「四川醫學院」，1985年5月改名华西医科大学。2000年與四川大學合併，成為四川大學一部分\[3\]。

2007年，四川大学医学院成为[新加坡卫生部门承认的八所中国大陆医学院之一](../Page/新加坡.md "wikilink")。\[4\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [四川大学华西医学中心](https://web.archive.org/web/20110707025028/http://wcums.scu.edu.cn/Default.htm)

{{-}}

[Category:成都高等院校](../Category/成都高等院校.md "wikilink")
[川](../Category/中华人民共和国全国重点大学.md "wikilink")
[Category:四川大学历史](../Category/四川大学历史.md "wikilink")

1.
2.
3.  [历史沿革](http://wcums.scu.edu.cn/about_a.asp) . 四川大学华西医学中心门户网站

4.  [新加坡认可中国大陆医学院增至8所](http://www.mdweekly.com.cn/doc/2007/12/5162.shtml)