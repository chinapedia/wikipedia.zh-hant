**戰國無雙**是由[Omega
Force開發](../Page/Omega_Force.md "wikilink")，[日本](../Page/日本.md "wikilink")[光榮公司發行的](../Page/光榮公司.md "wikilink")[動作遊戲系列](../Page/動作遊戲.md "wikilink")。以鈴木亮浩監督的《[真·三國無雙系列](../Page/真·三國無雙系列.md "wikilink")》作為系統基礎，採不同時代與人物建構的系列。2016年全系列作出貨數突破700萬套\[1\]。

自2004年於PS2上推出的《[戰國無雙](../Page/戰國無雙.md "wikilink")》為系列首作，本傳作品已發展至《[戰國無雙4](../Page/戰國無雙4.md "wikilink")》。

## 遊戲說明

  - 沿用《真·三國無雙》系列的遊戲系統，並以[日本戰國時代做為背景舞台的動作遊戲](../Page/日本戰國時代.md "wikilink")，在限定的時間內完成指定的任務進行關卡劇情。
  - 設定與《真·三國無雙》系列之主人翁[趙雲風格相似的](../Page/趙雲.md "wikilink")[真田幸村為主人翁](../Page/真田幸村.md "wikilink")，號稱「日本第一勇士」。
  - 以相繼發生的事件做為遊戲進行的背景。
  - 1代分為野戰及攻城戰，2代開始合併在一起，但城內少了許多機關。
  - 與真·三國無雙系列同樣，歷史事件有部份是誇大或者虛構。例：墨俣城花了一天築起，實際上是用了三天的時間。
  - PSP版本真·三國無雙放進了戰國無雙猛將傳的人物，同樣地，在激·戰國無雙，滿足特定條件、真·三國無雙系列的人物也有登場。
  - 無雙演武模式是採用了[真·三國無雙2和](../Page/真·三國無雙2.md "wikilink")[4代](../Page/真·三國無雙4.md "wikilink")，即是每一個武將都是進行不同的故事模式。
  - 前田慶次和稻姬的模組在2004年發行的[決戰III使用](../Page/決戰III.md "wikilink")，是遊戲內的隱藏角色之一。
  - 1代有出現[東漢末期武將](../Page/東漢.md "wikilink")[呂布](../Page/呂布.md "wikilink")。
  - 右下角的K.O count會顯示改為「討」代表武將擊殺的人數。
  - 激·戰國無雙是戰國無雙系列中可以進行連線的遊戲。
  - 2代的人物及2代未登場的1代人物與[真·三國無雙4的人物混合成為](../Page/真·三國無雙4.md "wikilink")《[無雙OROCHI](../Page/無雙OROCHI.md "wikilink")》的題材。
  - KATANA以扮演足輕為主，無雙武將以NPC登場。
  - 3代的遊戲關卡增加擊破效果，類似支線任務的設計，可以獲得各種不同的好處。
  - 3代與[任天堂合作](../Page/任天堂.md "wikilink")，改編1986年的遊戲[謎之村雨城](../Page/謎之村雨城.md "wikilink")，3代中稱為**村雨城**模式，該遊戲主角**鷹丸**可以在遊戲中使用。
  - 3代的戰國史模式可使用自創的新武將遊玩，但需購買Wii點數使用Wii上網消費下載。
  - 3代的人物及3代未登場的1-2代人物與[真·三國無雙6的人物混合成為](../Page/真·三國無雙6.md "wikilink")《[無雙OROCHI
    2](../Page/無雙OROCHI_2.md "wikilink")》的題材。
  - 4代的[真田幸村](../Page/真田幸村.md "wikilink")、[石田三成](../Page/石田三成.md "wikilink")、[井伊直虎與](../Page/井伊直虎.md "wikilink")[真·三國無雙7的](../Page/真·三國無雙7.md "wikilink")[趙雲](../Page/趙雲.md "wikilink")、[呂布](../Page/呂布.md "wikilink")、[王元姬混合成為](../Page/王元姬.md "wikilink")《[無雙☆群星大會串](../Page/無雙☆群星大會串.md "wikilink")》的題材。
  - 4代的人物與[真·三國無雙7的人物混合成為](../Page/真·三國無雙7.md "wikilink")《[无双大蛇3](../Page/无双大蛇3.md "wikilink")》的題材。

## 角色列表

詳見「[戰國無雙角色列表](../Page/戰國無雙角色列表.md "wikilink")」。

## 系列作品

  -
    以下記述**SW版**為其歐美版名稱**Samurai Warriors**的縮寫

<table>
<thead>
<tr class="header">
<th><p>代</p></th>
<th><p>作品名稱（日文版 / 歐美版）</p></th>
<th><p>平台</p></th>
<th><p>發售日期</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>戰國無雙 / Samurai Warriors</p></td>
<td><p><a href="../Page/PlayStation_2.md" title="wikilink">PS2</a><br />
<a href="../Page/XBOX.md" title="wikilink">XBOX</a></p></td>
<td><p>2004年2月11日（PS2）<br />
2004年7月29日（XBOX）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙 猛將傳 / Samurai Warriors Xtreme Legends</p></td>
<td><p>PS2</p></td>
<td><p>2004年9月16日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>激·戰國無雙 / Samurai Warriors State of War</p></td>
<td><p><a href="../Page/PSP.md" title="wikilink">PSP</a></p></td>
<td><p>2005年12月8日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>戰國無雙2 / Samurai Warriors 2</p></td>
<td><p>PS2<br />
<a href="../Page/XBOX360.md" title="wikilink">XBOX360</a><br />
PC</p></td>
<td><p>2006年2月24日（PS2）<br />
2006年8月17日（XBOX360）<br />
2008年7月11日（PC）</p></td>
<td><p>PC版無SW版</p></td>
</tr>
<tr class="odd">
<td><p>戰國無雙2 Empires / Samurai Warrors 2 Empires</p></td>
<td><p>PS2<br />
XBOX360</p></td>
<td><p>2006年11月16日（PS2）<br />
2007年2月27日（XBOX360）</p></td>
<td><p>XBOX360版無日文版</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙2 猛將傳 / Samurai Warriors 2 Xtreme Legends</p></td>
<td><p>PS2<br />
XBOX360</p></td>
<td><p>2007年8月23日（PS2）<br />
2008年4月14日（XBOX360）</p></td>
<td><p>XBOX360版為付費<a href="../Page/追加下載內容.md" title="wikilink">DLC</a>，且無SW版</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>戰國無雙2 with 猛將傳 / -</p></td>
<td><p>XBOX360</p></td>
<td><p>2008年3月19日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙 KATANA / Samurai Warriors KATANA</p></td>
<td><p><a href="../Page/Wii.md" title="wikilink">Wii</a></p></td>
<td><p>2007年9月20日</p></td>
<td><p>第一人稱視角的體感遊戲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>戰國無雙2 with 猛將傳 &amp; Empires HD Version / -</p></td>
<td><p><a href="../Page/PS3.md" title="wikilink">PS3</a><br />
<a href="../Page/PlayStation_Vita.md" title="wikilink">PSV</a></p></td>
<td><p>2013年10月24日</p></td>
<td><p>PSV版只有下載版</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>戰國無雙3 / Samurai Warriors 3</p></td>
<td><p>Wii</p></td>
<td><p>2009年12月3日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>戰國無雙3 猛將傳 / -</p></td>
<td><p>Wii</p></td>
<td><p>2011年2月10日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙3 Z / -</p></td>
<td><p>PS3</p></td>
<td><p>2011年2月10日</p></td>
<td><p>内容：本篇+猛將傳-村雨城</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>戰國無雙 Chronicle / Samurai Warriors Chronicle</p></td>
<td><p><a href="../Page/3DS.md" title="wikilink">3DS</a></p></td>
<td><p>2011年2月26日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙3 Empires / -</p></td>
<td><p>PS3</p></td>
<td><p>2011年8月26日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>戰國無雙3 Z Special / -</p></td>
<td><p>PSP</p></td>
<td><p>2012年2月16日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙 Chronicle 2nd / -</p></td>
<td><p>3DS</p></td>
<td><p>2012年9月13日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>戰國無雙4 / Samurai Warriors 4</p></td>
<td><p>PS3<br />
PSV<br />
<a href="../Page/PS4.md" title="wikilink">PS4</a></p></td>
<td><p>2014年3月20日（PS3/PSV）<br />
2014年9月4日（PS4）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙4 DX /</p></td>
<td><p>PS4<br />
switch</p></td>
<td><p>2019年3月14日</p></td>
<td><p>收录全部ps商店上架贩卖的DLC</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>戰國無雙 Shoot / -</p></td>
<td><p><a href="../Page/IOS.md" title="wikilink">IOS</a><br />
<a href="../Page/Andriod.md" title="wikilink">Andriod</a></p></td>
<td><p>2014年4月22日</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙 Chronicle 3 / Samurai Warriors Chronicle 3</p></td>
<td><p>3DS<br />
PSV</p></td>
<td><p>2014年12月4日</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>戰國無雙 4-II / Samurai Warriors 4-II</p></td>
<td><p>PS4<br />
PS3<br />
PSV<br />
PC</p></td>
<td><p>2015年2月11日（PS4/PS3/PSV）<br />
2015年9月30日（PC）</p></td>
<td><p>PC版美版首發，通過STEAM平台售賣<br />
SW版無PS3版</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>戰國無雙 4 Empires / Samurai Warriors 4 Empires</p></td>
<td><p>PS4<br />
PS3<br />
PSV</p></td>
<td><p>2015年9月17日</p></td>
<td><p>SW版無PS3版</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>戰國無雙 真田丸 / Samurai Warriors: Spirit of Sanada</p></td>
<td><p>PS4<br />
PS3<br />
PSV<br />
PC<br />
Switch</p></td>
<td><p>2016年11月23日（PS4/PS3/PSV）<br />
2017年5月26日（PC）<br />
2017年11月9日（switch）</p></td>
<td><p>PC版只對應SW版，通過STEAM平台售賣<br />
PS3版只有日文版<br />
Switch版收录除大河剧相关外的全部DLC</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 戰國無雙2

本作有別於1代為了顯示群雄割據於色彩表現上使用混沌的黑色，2代則以安土桃山時代轉移至江戶時代初期為印象，於色彩表現上使用豪華閃亮的金色。以關原之戰為劇情主軸，德川家康與石田三成在這代中扮演相當重要的角色。

|                                        |                                            |                      |
| -------------------------------------- | ------------------------------------------ | -------------------- |
| 人物名稱                                   | 聲優                                         | 備註                   |
| [德川家康](../Page/德川家康.md "wikilink")     | [中田讓治](../Page/中田讓治.md "wikilink")         | 1代為不可使用角色，2代改為可使用角色  |
| [石田三成](../Page/石田三成.md "wikilink")     | [竹本英史](../Page/竹本英史.md "wikilink")         | 2代新增角色               |
| [淺井長政](../Page/淺井長政.md "wikilink")     | [神谷浩史](../Page/神谷浩史.md "wikilink")         | 1代為不可使用角色，2代改為可使用角色  |
| [島左近](../Page/島左近.md "wikilink")       | [山田真一](../Page/山田真一_\(配音員\).md "wikilink") | 2代新增角色               |
| [島津義弘](../Page/島津義弘.md "wikilink")     | [江川央生](../Page/江川央生.md "wikilink")         | 2代新增角色               |
| [立花誾千代](../Page/立花誾千代.md "wikilink")   | [進藤尚美](../Page/進藤尚美.md "wikilink")         | 2代新增角色               |
| [直江兼續](../Page/直江兼續.md "wikilink")     | [高塚正也](../Page/高塚正也.md "wikilink")         | 2代新增角色               |
| [寧寧](../Page/寧寧.md "wikilink")         | [山崎和佳奈](../Page/山崎和佳奈.md "wikilink")       | 2代新增角色               |
| [風魔小太郎](../Page/風魔小太郎.md "wikilink")   | [檜山修之](../Page/檜山修之.md "wikilink")         | 2代新增角色               |
| [宮本武藏](../Page/宮本武藏.md "wikilink")     | [金子英彥](../Page/金子英彥.md "wikilink")         | 2代新增角色               |
| [前田利家](../Page/前田利家.md "wikilink")     | [小西克幸](../Page/小西克幸.md "wikilink")         | 2代猛將傳新增角色            |
| [長宗我部元親](../Page/長宗我部元親.md "wikilink") | [置鮎龍太郎](../Page/置鮎龍太郎.md "wikilink")       | 2代猛將傳新增角色            |
| [加西亞](../Page/明智玉.md "wikilink")       | [鹿野潤](../Page/鹿野潤.md "wikilink")           | 2代猛將傳新增角色            |
| [佐佐木小次郎](../Page/佐佐木小次郎.md "wikilink") | [上田祐司](../Page/上田祐司.md "wikilink")         | 2代為不可使用角色，2代猛將傳改為可使用 |
| [柴田勝家](../Page/柴田勝家.md "wikilink")     | [竹本英史](../Page/竹本英史.md "wikilink")         | 2代為不可使用角色，2代猛將傳改為可使用 |
| [今川義元](../Page/今川義元.md "wikilink")     | [河內孝博](../Page/河內孝博.md "wikilink")         | 2代本傳未登場，2代猛將傳重新上陣    |

## 戰國無雙3

本作以三大故事為主軸，分別是以武田信玄等人為主的《關東三國志》，織田信長等人為主的《戰國三傑》，石田三成等人為主的《關原的年輕武者》，豐富遊戲內的劇情。

|                                      |                                        |                                 |
| ------------------------------------ | -------------------------------------- | ------------------------------- |
| 人物名稱                                 | 聲優                                     | 備註                              |
| [加藤清正](../Page/加藤清正.md "wikilink")   | [杉田智和](../Page/杉田智和.md "wikilink")     | 3代新增角色                          |
| [黑田官兵衛](../Page/黑田官兵衛.md "wikilink") | [高塚正也](../Page/高塚正也.md "wikilink")     | 3代新增角色                          |
| [立花宗茂](../Page/立花宗茂.md "wikilink")   | [東地宏樹](../Page/東地宏樹.md "wikilink")     | 3代新增角色                          |
| [甲斐姬](../Page/甲斐姬.md "wikilink")     | [鈴木真仁](../Page/鈴木真仁.md "wikilink")     | 3代新增角色                          |
| [北条氏康](../Page/北条氏康.md "wikilink")   | [石塚運昇](../Page/石塚運昇.md "wikilink")     | 3代新增角色                          |
| [竹中半兵衛](../Page/竹中半兵衛.md "wikilink") | [庄司宇芽香](../Page/庄司宇芽香.md "wikilink")   | 3代新增角色                          |
| [毛利元就](../Page/毛利元就.md "wikilink")   | [石川英郎](../Page/石川英郎.md "wikilink")     | 3代新增角色                          |
| [綾御前](../Page/仙桃院.md "wikilink")     | [庄司宇芽香](../Page/庄司宇芽香.md "wikilink")   | 3代為完成特定條件可使用外觀模組，3代猛將傳改為玩家可操作武將 |
| [福島正則](../Page/福島正則.md "wikilink")   | [藤本たかひろ](../Page/藤本たかひろ.md "wikilink") | 3代為完成特定條件可使用外觀模組，3代猛將傳改為玩家可操作武將 |
| [女忍者](../Page/女忍者.md "wikilink")     | [永島由子](../Page/永島由子.md "wikilink")     | 2代未登場，3代重新上陣                    |
| [加西亞](../Page/明智玉.md "wikilink")     | [鹿野潤](../Page/鹿野潤.md "wikilink")       | 3代未登場，3代猛將傳重新登場                 |
| [藤堂高虎](../Page/藤堂高虎.md "wikilink")   | [松風雅也](../Page/松風雅也.md "wikilink")     | 編年史2nd新增人物                      |
| [井伊直虎](../Page/井伊直虎.md "wikilink")   | [齊藤佑圭](../Page/齊藤佑圭.md "wikilink")     | 編年史2nd新增人物                      |
| [柳生宗矩](../Page/柳生宗矩.md "wikilink")   | [宮崎寬務](../Page/宮崎寬務.md "wikilink")     | 編年史2nd新增人物                      |

## [戰國無雙4](../Page/戰國無雙4.md "wikilink")

在主要模式的「無雙演武」部分，將分為描寫織田、武田、中國、關東、九州、東北、四國、德川、上杉、近畿等地方活躍武將的「地方篇」，以及描寫豐臣、真田等戰國時代終結的「天下統一篇」兩大主軸。

|                                        |                                      |           |
| -------------------------------------- | ------------------------------------ | --------- |
| 人物名稱                                   | 聲優                                   | 備註        |
| [真田信之](../Page/真田信之.md "wikilink")     | [小野大輔](../Page/小野大輔.md "wikilink")   | 4代新增角色    |
| [大谷吉繼](../Page/大谷吉繼.md "wikilink")     | [日野聰](../Page/日野聰.md "wikilink")     | 4代新增角色    |
| [松永久秀](../Page/松永久秀.md "wikilink")     | [石井康嗣](../Page/石井康嗣.md "wikilink")   | 4代新增角色    |
| [片倉小十郎](../Page/片倉小十郎.md "wikilink")   | [竹內良太](../Page/竹內良太.md "wikilink")   | 4代新增角色    |
| [島津豐久](../Page/島津豐久.md "wikilink")     | [宮坂俊藏](../Page/宮坂俊藏.md "wikilink")   | 4代新增角色    |
| [小早川隆景](../Page/小早川隆景.md "wikilink")   | [岡本寬志](../Page/岡本寬志.md "wikilink")   | 4代新增角色    |
| [小少將](../Page/細川小少將.md "wikilink")     | [白石涼子](../Page/白石涼子.md "wikilink")   | 4代新增角色    |
| [上杉景勝](../Page/上杉景勝.md "wikilink")     | [竹內良太](../Page/竹內良太.md "wikilink")   | 4代新增角色    |
| [早川殿](../Page/早川殿.md "wikilink")       | [佐藤聰美](../Page/佐藤聰美.md "wikilink")   | 4代新增角色    |
| [石川五右衛門](../Page/石川五右衛門.md "wikilink") | [江川央生](../Page/江川央生.md "wikilink")   | 4代復出角色    |
| [宮本武藏](../Page/宮本武藏.md "wikilink")     | [金子英彥](../Page/金子英彥.md "wikilink")   | 4代復出角色    |
| [佐佐木小次郎](../Page/佐佐木小次郎.md "wikilink") | [上田祐司](../Page/上田祐司.md "wikilink")   | 4代復出角色    |
| [井伊直政](../Page/井伊直政.md "wikilink")     | [小西克幸](../Page/小西克幸.md "wikilink")   | 4代-II新增角色 |
| [真田昌幸](../Page/真田昌幸.md "wikilink")     | [三宅健太](../Page/三宅健太.md "wikilink")   | 真田丸新增角色   |
| [茶茶](../Page/淀殿.md "wikilink")         | [高野麻里佳](../Page/高野麻里佳.md "wikilink") | 真田丸新增角色   |
| [佐助](../Page/猿飛佐助.md "wikilink")       | [阿座上洋平](../Page/阿座上洋平.md "wikilink") | 真田丸新增角色   |
| [武田勝賴](../Page/武田勝賴.md "wikilink")     | [岡本寬志](../Page/岡本寬志.md "wikilink")   | 真田丸新增角色   |
| [德川秀忠](../Page/德川秀忠.md "wikilink")     | [半田裕典](../Page/半田裕典.md "wikilink")   | 真田丸新增角色   |

## 關連作品

  - [真三國無雙系列](../Page/真三國無雙系列.md "wikilink")，此系列以其為藍本。
  - [無雙OROCHI系列](../Page/無雙OROCHI系列.md "wikilink")，以真三國無雙系列與本系列為主的混合作品。

## 外傳作品

### 柏青哥戰國無雙系列

本系列遊戲由[山佐承製](../Page/山佐公司.md "wikilink")，以[街機為主](../Page/街機.md "wikilink")，後來有移植到[PS3](../Page/PS3.md "wikilink")。本系列作品的背景及人物造型均還是以一代為主。本傳的PS3版本於2007年9月發售。

以下是本系列作中的主要角色：

  - [真田幸村](../Page/真田幸村.md "wikilink")
  - [前田慶次](../Page/前田慶次.md "wikilink")
  - [服部半藏](../Page/服部半藏.md "wikilink")

再於猛將傳中增加一名主要角色：

  - [本多忠勝](../Page/本多忠勝.md "wikilink")

### [精靈寶可夢](../Page/精靈寶可夢.md "wikilink")+[信長之野望](../Page/信長之野望.md "wikilink")

本遊戲由[任天堂與](../Page/任天堂.md "wikilink")[光榮特庫摩共同開發](../Page/光榮特庫摩.md "wikilink")，登場於[NDS](../Page/NDS.md "wikilink")。雖然名為**信長之野望**，但人物造型是使用戰國無雙3的，且完成發表會中光榮方的出席者之一也是由戰國無雙系列的主製作者之一[鯉沼久史先生出席](../Page/鯉沼久史.md "wikilink")。作品中只寫武將的名，而且是[片假名](../Page/片假名.md "wikilink")。地點在「亂世地方」，總共有17個國家，對應寶可夢遊戲內的17種屬性，於2012年3月17日發售。

以下是武將&配對寶可夢之名單：

|                                    |                                                                                 |           |      |        |
| ---------------------------------- | ------------------------------------------------------------------------------- | --------- | ---- | ------ |
| 人物名稱                               | 夥伴寶可夢                                                                           | 夥伴寶可夢屬性   | 所屬國度 | 所屬國度屬性 |
| 原創男女主角                             | [伊布](../Page/伊布_\(神奇寶貝\).md "wikilink")（イーブイ）                                   | 普通系       | 初始之國 | 普通系    |
| [信長](../Page/織田信長.md "wikilink")   | [捷克羅姆](../Page/捷克羅姆.md "wikilink")（ゼクロム）                                        | 龍系、雷系     | 龍之國  | 龍系     |
| [光秀](../Page/明智光秀.md "wikilink")   | [急凍鳥](../Page/急凍鳥.md "wikilink")（フリーザー）                                         | 冰系、飛行系    | 吹雪之國 | 冰系     |
| [阿市](../Page/織田市.md "wikilink")    | [胖丁](../Page/胖丁.md "wikilink")（プリン）                                             | 普通系       | 初始之國 | 普通系    |
| [謙信](../Page/上杉謙信.md "wikilink")   | [超夢](../Page/超夢.md "wikilink")（ミュウツー）                                           | 超能力系      | 幻夢之國 | 超能力系   |
| [信玄](../Page/武田信玄.md "wikilink")   | [固拉多](../Page/固拉多.md "wikilink")（グラードン）                                         | 地面系       | 大地之國 | 地面系    |
| [秀吉](../Page/豐臣秀吉.md "wikilink")   | [猛火猴](../Page/猛火猴.md "wikilink")（モウカザル）→[烈焰猴](../Page/烈焰猴.md "wikilink")（ゴウカザル） | 火系→火系、格鬥系 | 火炎之國 | 火系     |
| [元就](../Page/毛利元就.md "wikilink")   | [藤藤蛇](../Page/藤藤蛇.md "wikilink")（ツタージャ）→[青藤蛇](../Page/青藤蛇.md "wikilink")（ジャノビー） | 草系        | 青葉之國 | 草系     |
| [元親](../Page/長宗我部元親.md "wikilink") | [水水獺](../Page/水水獺.md "wikilink")（ミジュマル）→[雙刃丸](../Page/雙刃丸.md "wikilink")（フタチマル） | 水系        | 泉水之國 | 水系     |
| [加西亞](../Page/明智玉.md "wikilink")   | [食夢夢](../Page/食夢夢.md "wikilink")（ムンナ）                                           | 超能力系      |      |        |
| [三成](../Page/石田三成.md "wikilink")   | [駒刀小兵](../Page/駒刀小兵.md "wikilink")（コマタナ）                                        | 惡系、鋼系     |      |        |
| [正則](../Page/福島正則.md "wikilink")   | [混混鱷](../Page/混混鱷.md "wikilink")（ワルビル）                                          | 地面系、惡系    |      |        |
| [清正](../Page/加藤清正.md "wikilink")   | [斧牙龍](../Page/斧牙龍.md "wikilink")（オノンド）                                          | 龍系        |      |        |
| [慶次](../Page/前田慶次.md "wikilink")   | [護城龍](../Page/護城龍.md "wikilink")（トリデプス）                                         | 岩系、鋼系     |      |        |
| [綾御前](../Page/仙桃院.md "wikilink")   | [噴嚏熊](../Page/噴嚏熊.md "wikilink")（クマシュン）                                         | 冰系        | 幻夢之國 | 超能力系   |
| [兼續](../Page/直江兼續.md "wikilink")   | [沙奈朵](../Page/沙奈朵.md "wikilink")（サーナイト）                                         | 超能力系      | 幻夢之國 | 超能力系   |
| [幸村](../Page/真田幸村.md "wikilink")   | [暖暖豬](../Page/暖暖豬.md "wikilink")（ポカブ）                                           | 火系        | 大地之國 | 地面系    |
| [義元](../Page/今川義元.md "wikilink")   | [榛果球](../Page/榛果球.md "wikilink")（クヌギダマ）                                         | 蟲系        | 蛹之國  | 蟲系     |
| [義弘](../Page/島津義弘.md "wikilink")   | [鐵骨土人](../Page/鐵骨土人.md "wikilink")（ドテッコツ）                                       | 格鬥系       | 拳之國  | 格鬥系    |
| [誾千代](../Page/立花誾千代.md "wikilink") | [勒克貓](../Page/勒克貓.md "wikilink")（ルクシオ）                                          | 雷系        | 紫電之國 | 雷系     |
| [宗茂](../Page/立花宗茂.md "wikilink")   | [姆克鳥](../Page/姆克鳥.md "wikilink")（ムクバード）                                         | 普通系、飛行系   | 紫電之國 | 雷系     |
| 女忍者                                | [滑頭小子](../Page/滑頭小子.md "wikilink")（ズルッグ）                                        | 惡系、格鬥系    | 大地之國 | 地面系    |
| [政宗](../Page/伊達政宗.md "wikilink")   | [勇士鷹](../Page/勇士鷹.md "wikilink")（ウォーグル）                                         | 普通系、飛行系   | 翼之國  | 飛行系    |
| [小太郎](../Page/風魔小太郎.md "wikilink") | [索羅亞克](../Page/索羅亞克.md "wikilink")（ゾロアーク）                                       | 惡系        | 夜叉之國 | 惡系     |
| [寧寧](../Page/寧寧.md "wikilink")     | [大嘴蝠](../Page/大嘴蝠.md "wikilink")（ゴルバット）                                         | 毒系、飛行系    | 毒牙之國 | 毒系     |
| [家康](../Page/德川家康.md "wikilink")   | [波士可多拉](../Page/波士可多拉.md "wikilink")（ボスゴドラ）                                     | 鋼系、岩系     | 不屈之國 | 鋼系     |
| [蘭丸](../Page/森蘭丸.md "wikilink")    | [哈克龍](../Page/哈克龍.md "wikilink")（ハクリュー）                                         | 龍系        |      |        |
| [濃姬](../Page/濃姬.md "wikilink")     | [夢妖魔](../Page/夢妖魔.md "wikilink")（ムウマージ）                                         | 幽靈系       | 御靈之國 | 幽靈系    |
| [忠勝](../Page/本多忠勝.md "wikilink")   | [巨金怪](../Page/巨金怪.md "wikilink")（メタグロス）                                         | 鋼系、超能力系   | 不屈之國 | 鋼系     |
| [半兵衛](../Page/竹中半兵衛.md "wikilink") | [皮卡丘](../Page/皮卡丘.md "wikilink")（ピカチュウ）                                         | 雷系        |      |        |
| [官兵衛](../Page/黑田官兵衛.md "wikilink") | [燈火幽靈](../Page/燈火幽靈.md "wikilink")（ランプラー）                                       | 幽靈系       |      |        |
| [半藏](../Page/服部半藏.md "wikilink")   | [花岩怪](../Page/花岩怪.md "wikilink")（ミカルゲ）                                          | 幽靈系、惡系    |      |        |
| [阿國](../Page/阿國.md "wikilink")     | [車輪毬](../Page/車輪毬.md "wikilink")（ホイーガ）                                          | 蟲系、毒系     |      |        |
| [氏康](../Page/北條氏康.md "wikilink")   | [地幔岩](../Page/地幔岩.md "wikilink")（ガントル）                                          | 岩系        | 奇岩之國 | 岩系     |
| [孫市](../Page/雜賀孫市.md "wikilink")   | [尖牙籠](../Page/尖牙籠.md "wikilink")（マスキッパ）                                         | 草系        | 翼之國  | 飛行系    |
| [稻姬](../Page/稻姬.md "wikilink")     | [沼王](../Page/沼王.md "wikilink")（ヌオー）                                             | 水系、地面系    | 不屈之國 | 鋼系     |
| [甲斐姬](../Page/甲斐姬.md "wikilink")   | [爆香猴](../Page/爆香猴.md "wikilink")（バオップ）                                          | 火系        | 奇岩之國 | 岩系     |

## 參考資料

## 外部連結

主系列

  - [戰國無雙系列日文綜合官方網站](http://www.gamecity.ne.jp/sengoku/index.html)

外傳作品

  - [柏青哥戰國無雙日文官方網站](https://web.archive.org/web/20111016190533/http://www.yamasa.co.jp/homegame/ps3_sgm/)
  - [柏青哥戰國無雙猛將傳日文情報網站](http://betty.jp/sengokuslot/)
  - [Pokémon+信長之野望日文官方網站](http://www.pokemon.co.jp/ex/ranse/)

[Category:無雙系列](../Category/無雙系列.md "wikilink")
[戰國無雙系列](../Category/戰國無雙系列.md "wikilink")

1.