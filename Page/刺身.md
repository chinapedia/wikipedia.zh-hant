[Sashimi_-_Tokyo_-_Japan.jpg](https://zh.wikipedia.org/wiki/File:Sashimi_-_Tokyo_-_Japan.jpg "fig:Sashimi_-_Tokyo_-_Japan.jpg")

**刺身**（；或音譯**沙西米**），多指[生魚片](../Page/生魚片.md "wikilink")，也叫“**鱼生**”，但也可用[雞肉](../Page/雞肉.md "wikilink")、[馬肉或是任何可以切成片狀食用的食材製作](../Page/馬肉.md "wikilink")，是一種常见的[日本料理](../Page/日本料理.md "wikilink")。其做法是以新鮮的[魚](../Page/魚.md "wikilink")[貝類生切成片](../Page/貝.md "wikilink")，蘸[醬油](../Page/醬油.md "wikilink")、[山葵](../Page/山葵.md "wikilink")、[味噌等](../Page/味噌.md "wikilink")[調味料食用](../Page/調味料.md "wikilink")。

刺身一般都是用新鮮海魚、海貝製作，魚如果不新鮮，生吃可能會有細菌或是寄生蟲污染，因此雖然[日本人是出了名的喜歡吃生魚片](../Page/日本人.md "wikilink")，但實際上在日本內陸，20世紀初尚未發明[冰箱前並不普遍](../Page/冰箱.md "wikilink")，只是沿海比較流行。現在保鮮和運輸條件改進後，吃刺身的日本人也開始多了起來。生魚片的營養價值頗高，含有豐富的[蛋白質](../Page/蛋白質.md "wikilink")，而且是質地柔軟的優質蛋白質。它也含有豐富的[維生素與微量礦物質](../Page/維生素.md "wikilink")。脂肪含量低，卻含有不少[DHA等的](../Page/二十二碳六烯酸.md "wikilink")[ω-3脂肪酸](../Page/ω-3脂肪酸.md "wikilink")。稱得上是營養豐富且容易吸收的食物。三國時期[陳登以喜食生魚片聞名](../Page/陳登.md "wikilink")，但當時並無生食保鮮技術，故陳登多次食用後年紀輕輕便因腸胃問題身亡。

由於刺身是日本料理中最為清淡的菜式，所以在[餐中通常為最早的一道](../Page/餐.md "wikilink")[菜](../Page/食物.md "wikilink")。以免濃烈味道的菜式會把淡淡的魚鮮味蓋過。而且很多時候刺身亦會與[壽司共食](../Page/壽司.md "wikilink")，多以[蘿蔔](../Page/蘿蔔.md "wikilink")、[青菜](../Page/青菜.md "wikilink")、[海藻等作配菜](../Page/海藻.md "wikilink")。

## 刺身的食材

### 鮪魚

まぐろ（Maguro），是[北方藍鰭金槍魚](../Page/北方藍鰭金槍魚.md "wikilink")
另外Aki是[黃鰭金槍魚](../Page/黃鰭金槍魚.md "wikilink")。\[1\]

#### 瘦鮪魚肉

あかみ（Akami），指瘦鮪魚肉，一般Maguro是北方藍鰭金槍魚魚身上背部等較瘦的魚肉。\[2\]

#### 肥鮪魚

トロ（Toro）：指肥鮪魚肉。又可細分兩種：

  -
    大トロ（Ōtoro）是鮪魚身上最肥的魚肉，可說是入口即化。
    中トロ（Chūtoro）是鮪魚身上介於Akami與Ōtoro之間的肥鮪魚肉。\[3\]

### 比目魚

平目（Hirame）：[扁口魚](../Page/扁口魚.md "wikilink")

### 鮭魚（三文魚）

鮭 サーモン（Salmon）：[鮭魚](../Page/鮭魚.md "wikilink")

### 海膽

「」（Uni）：[海膽](../Page/海膽.md "wikilink")，作刺身時使用海膽的[生殖腺](../Page/生殖腺.md "wikilink")，即海膽籽。

### 鰤魚

はまち（Hamachi）：[鰤魚](../Page/鰤魚.md "wikilink")

### 河豚

ふぐ（Fugu）：[河豚](../Page/河豚.md "wikilink")、[黃鰭東方魨](../Page/黃鰭東方魨.md "wikilink")

### 魷魚

いか（Ika）：[魷魚](../Page/魷魚.md "wikilink")

### 章魚

たこ（Tako）：[章魚](../Page/章魚.md "wikilink")

### 蝦

えび（Ebi）：熟[蝦，海牛](../Page/蝦，海牛.md "wikilink")

### 鰺魚

あじ（Aji）：[鰺魚](../Page/鰺科.md "wikilink")、[竹莢魚](../Page/竹莢魚.md "wikilink")

### 扇貝

ほたて貝（Hotate-gai）：[扇貝](../Page/扇貝.md "wikilink")

### 丁香魚

キビナゴ（Kibinago）：[丁香魚](../Page/日本銀帶鯡.md "wikilink")

### 鯨肉

げいにく（Gei-niku）：[鯨肉](../Page/鯨肉.md "wikilink")

### 牛肉

ぎゅうにく（Giu-niku）：牛肉

### 馬肉

ばにく（Ba-niku）：马肉

### 豚肉

ぶたにく（Buta-niku）：猪肉

### 鶏肉

とりにく（Tori-niku）：鸡肉

## 生食規範

  - 捕獲時需及時分離易有寄生蟲的部位，並儘量以完全海水水產魚貝類為主，因能於淡水生活之水產與陸地生物較多共通寄生蟲。並維持低溫。
  - 為預防海獸胃線蟲，需經冷凍使中心溫度低於攝氏零下20度24小時，歐盟亦同。（美國規範壽司用生食水產須經冷凍使中心溫度低於攝氏零下35度15小時或零下20度7日）\[4\]
  - 東京都是要求生食用海水水產須經冷凍使中心溫度低於攝氏零下20度48小時，陸生動物肉則不建議生食。\[5\]
  - 為預防旋尾線蟲，需經冷凍使中心溫度低於攝氏零下30度4日（等同殺蟲效果：攝氏零下35度15小時或零下40度40分）。\[6\]
  - 解除冷凍後須保存於攝氏10度以下，並須標註為生食用。<ref>\[

東京都市場衛生検査所東京都福祉保健局</ref>

美國FDA列出了各種魚種的風險比如寄生蟲，汙染物等。\[7\]

## 刺身在世界各地的傳播

[日本統治台灣時](../Page/台灣日治時期.md "wikilink")，[台灣人逐漸習慣食用本地](../Page/台灣人.md "wikilink")[海產所作的刺身](../Page/海鮮.md "wikilink")。稱為**沙西米**（Sashimi），經常在夜市與傳統市場中販賣。在[日本統治南洋時](../Page/南洋群島.md "wikilink")，刺身也傳入了今日的[帛琉](../Page/帛琉.md "wikilink")、[密克羅尼西亞和](../Page/密克羅尼西亞.md "wikilink")[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")，馬紹爾人稱刺身為Chachimi。通常將鮪魚等生魚肉淋上檸檬汁，沾醬油食用。\[8\]\[9\]\[10\]。

## 刺身圖集

<File:Sea> urchin eggs.jpg|海膽籽 <File:Whale> meat on dish.jpg|日本的鯨肉刺身
[File:Basashi111.jpg|馬肉刺身](File:Basashi111.jpg%7C馬肉刺身)
[File:鮭魚生魚片.jpg|鮭魚刺身](File:鮭魚生魚片.jpg%7C鮭魚刺身) <File:Salmon>
and Shrimp Lunch 20180630.jpg|一顿鮭魚刺身家庭料理 <File:Fugu>
sashimi.jpg||[河豚刺身](../Page/河豚.md "wikilink")（）
<File:Sashimi-dish> Yellow tail amberijack01.jpg|刺身

## 參見

  - [鱠](../Page/鱠.md "wikilink")
  - [壽司](../Page/壽司.md "wikilink")
  - [生魚片](../Page/生魚片.md "wikilink")

## 參考文獻

[Category:日本傳統食品](../Category/日本傳統食品.md "wikilink")
[Category:魚生](../Category/魚生.md "wikilink")

1.

2.
3.
4.  [厚生勞動省](http://www.mhlw.go.jp/stf/seisakunitsuite/bunya/0000042953.html)

5.  [東京都](http://www.tokyo-eiken.go.jp/assets/issue/kurashi/ck43/2001430all.pdf)

6.  [農林水產省引自厚生勞動省](http://www.maff.go.jp/j/syouan/tikusui/gyokai/g_kenko/busitu/pdf/hotaru_ika.pdf)

7.  [Fish and Fishery Products Hazards and Controls Guidance - Fourth
    Edition](http://www.fda.gov/food/guidanceregulation/guidancedocumentsregulatoryinformation/seafood/ucm2018426.htm#toc)FDA

8.  [キッコーマン　世界のしょうゆクッキング](http://www.kikkoman.co.jp/homecook/world/micronesia.html)

9.  [ミクロネシア連邦 -
    愛知県国際交流協会](http://www2.aia.pref.aichi.jp/koryu/j/kyouzai/PDF/H22/micronesia.pdf)

10. [珊瑚の楽園・南国マーシャル諸島に今も残るノスタルジックな日本語7つ 70seeds](https://www.70seeds.jp/marshall_japanese/)