**沙頭角公路**（）是[香港早期興建的](../Page/香港.md "wikilink")[公路之一](../Page/公路.md "wikilink")，連接[新界](../Page/新界.md "wikilink")[粉嶺及](../Page/粉嶺.md "wikilink")[沙頭角](../Page/沙頭角.md "wikilink")，全長10.5公里，由[馬會道至](../Page/馬會道.md "wikilink")[坪輋路一段為雙程四線分隔車道](../Page/坪輋路.md "wikilink")（為[路政署於](../Page/路政署.md "wikilink")1995年至1999年間所擴建），其餘路段為雙程雙線道路。沙頭角公路主要分作4段，起始是[龍躍頭段](../Page/龍躍頭.md "wikilink")（粉嶺鐵路站至流水響）、馬尾下段（高埔至大塘湖）、[禾坑段](../Page/禾坑.md "wikilink")（萊洞至麻雀嶺）以及石涌凹段（鹽灶下至沙頭角邊境檢查站）。

沙頭角公路是繼[青山公路和](../Page/青山公路.md "wikilink")[大埔公路後](../Page/大埔公路.md "wikilink")，香港政府興建的第三條主要公路，於1927年正式通車。公路的通車，使當時[九廣鐵路的](../Page/九廣鐵路.md "wikilink")[沙頭角支線於](../Page/沙頭角支線.md "wikilink")1年後的1928年停辦。

沙頭角公路大部分路段限速為50公里，但禾坑一段限速達70公里，現因道路工程而改為50公里。

## 鄰近設施

  - [粉嶺消防局](../Page/粉嶺消防局.md "wikilink")
  - [粉嶺樓](../Page/粉嶺樓.md "wikilink")
  - [聯和墟](../Page/聯和墟.md "wikilink")
  - [逸峯](../Page/逸峯.md "wikilink")
  - [新圍軍營](../Page/新圍軍營.md "wikilink")
  - [前皇后山軍營](../Page/皇后山軍營.md "wikilink")（[皇后山邨及](../Page/皇后山邨.md "wikilink")[山麗苑](../Page/山麗苑.md "wikilink")）
  - [孔嶺](../Page/孔嶺.md "wikilink")
  - [南涌](../Page/南涌.md "wikilink")
  - [鹿頸路](../Page/鹿頸路.md "wikilink")（並連接[新娘潭路](../Page/新娘潭路.md "wikilink")）
  - [沙頭角診所](../Page/沙頭角診所.md "wikilink")
  - [沙頭角農莊](../Page/沙頭角農莊.md "wikilink")

Sha Tau Kok Road at night (Street Light rays improved).jpg|沙頭角公路西端夜景 San
Wan Road, Fanling (Hong
Kong).JPG|沙頭角公路西端與[新運路交界的迴旋處](../Page/新運路.md "wikilink")
ShaTauKokRd-ShekchungAu 2011.JPG|沙頭角公路石涌凹段

[Category:北區街道 (香港)](../Category/北區街道_\(香港\).md "wikilink")
[Category:香港公路](../Category/香港公路.md "wikilink")
[Category:粉嶺](../Category/粉嶺.md "wikilink")
[Category:沙頭角](../Category/沙頭角.md "wikilink")