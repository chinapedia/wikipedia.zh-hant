[菅公母子像.JPG](https://zh.wikipedia.org/wiki/File:菅公母子像.JPG "fig:菅公母子像.JPG")）\]\]

**菅原道真**，出生於[承和](../Page/承和_\(仁明天皇\).md "wikilink")12年陰曆6月25日（845年8月1日），卒於[延喜](../Page/延喜.md "wikilink")3年陰曆2月25日（903年3月26日）。是[日本](../Page/日本.md "wikilink")[平安時代的學者](../Page/平安時代.md "wikilink")、[詩人](../Page/詩人.md "wikilink")、[政治家](../Page/政治家.md "wikilink")。長於[漢詩](../Page/漢詩.md "wikilink")、被日本人尊為學問之神（相對地，日本傳統文化中的[武神則是](../Page/武神.md "wikilink")[坂上田村麻呂](../Page/坂上田村麻呂.md "wikilink")）。33歳時被任為[文章博士](../Page/文章博士.md "wikilink")。[醍醐天皇時晉升為](../Page/醍醐天皇.md "wikilink")[右大臣](../Page/右大臣.md "wikilink")、但受到[左大臣](../Page/左大臣.md "wikilink")[藤原時平的讒言](../Page/藤原時平.md "wikilink")、被貶到[九州](../Page/九州_\(日本\).md "wikilink")[太宰府擔任](../Page/太宰府.md "wikilink")[權帥](../Page/權帥.md "wikilink")（太宰府副統帥），後抑鬱以終，被後人尊稱為「火雷天神」。

## 家系

父親是[菅原是善](../Page/菅原是善.md "wikilink")、母出自伴氏（名不詳）。從道真的祖父[菅原清公那一代開始將姓從](../Page/菅原清公.md "wikilink")[土師改成](../Page/土師氏.md "wikilink")[菅原](../Page/菅原氏.md "wikilink")。祖父與父親都曾經是文章博士、為朝廷的名士。母方的伴氏出自古代的[大伴氏](../Page/大伴氏.md "wikilink")，因避[淳和天皇的名諱而從大伴改為伴姓](../Page/淳和天皇.md "wikilink")，族人中[大伴旅人和](../Page/大伴旅人.md "wikilink")[大伴家持皆是著名的詩人](../Page/大伴家持.md "wikilink")。

道真的妻子正室是[島田宣來子](../Page/島田宣來子.md "wikilink")（[島田忠臣的女兒](../Page/島田忠臣.md "wikilink")）。長子是[菅原高視和其他多名子女](../Page/菅原高視.md "wikilink")。子孫亦多為著名的學者。高視的曾孫、道真第五代孫[菅原孝標](../Page/菅原孝標.md "wikilink")，他的女兒是『[更級日記](../Page/更級日記.md "wikilink")』的作者。

## 生涯

道真年幼時即長於詩歌，[貞觀](../Page/貞觀_\(日本\).md "wikilink")4年（862年），18歳的他成為文章生；5年後（867年），菅原道真成為[文章得業生](../Page/文章得業生.md "wikilink")，敘任[正六位下](../Page/正六位下.md "wikilink")[下野権少掾](../Page/下野國.md "wikilink")。[貞觀](../Page/貞觀_\(日本\).md "wikilink")12年（870年）升任[正六位上](../Page/正六位上.md "wikilink")，隔年轉任[玄蕃助](../Page/玄蕃寮.md "wikilink")，不久，再遷任[少内記](../Page/内記.md "wikilink")；[貞觀](../Page/貞觀_\(日本\).md "wikilink")16年（874年）升任[從五位下的](../Page/從五位下.md "wikilink")[兵部少輔](../Page/兵部省.md "wikilink")，後遷任[民部少輔](../Page/民部省.md "wikilink")。877年擔任[式部少輔](../Page/式部省.md "wikilink")，同年10月，改元[元慶](../Page/元慶.md "wikilink")，兼任[文章博士](../Page/文章博士.md "wikilink")；元慶3年（879年），升為[從五位上](../Page/從五位上.md "wikilink")。元慶7年（883年），他兼任[加賀權守](../Page/加賀國.md "wikilink")、[治部權大輔](../Page/治部省.md "wikilink")；[仁和](../Page/仁和.md "wikilink")2年（886年）1月，菅原道真被任命為[讚岐守](../Page/讚岐國.md "wikilink")，前往[四國](../Page/四國.md "wikilink")[讚岐任官](../Page/讚岐.md "wikilink")，至到[宽平](../Page/宽平.md "wikilink")2年任滿回京。

之後他深受[宇多天皇的信任](../Page/宇多天皇.md "wikilink")，開始擔任重要職務，當時天皇為了牽制當道的外戚[藤原氏](../Page/藤原氏.md "wikilink")，而重用道真。寛平3年（891年）2月，他補任[藏人頭的官位](../Page/藏人頭.md "wikilink")，後兼任式部少輔與[左中辩](../Page/辩官.md "wikilink")。次年1月，升任[從四位下](../Page/從四位.md "wikilink")，並於同年年底兼任[左京大夫](../Page/京職.md "wikilink")；寛平5年（893年）2月，47歲的菅原道真補任[参議一職](../Page/参議.md "wikilink")，並兼任式部大輔，後因兼任左大辩、[勘解由長官](../Page/勘解由使.md "wikilink")、[春宮亮](../Page/春宮坊.md "wikilink")，而免去式部大輔的職務。寛平6年（894年）8月，他被任命為[遣唐大使](../Page/遣唐使.md "wikilink")，但在道真的建議下，當時的日本朝廷廢止了遣唐使的職務，同年12月，菅原道真兼任[侍從](../Page/侍從.md "wikilink")。

895年，他兼任[近江守一職](../Page/近江國.md "wikilink")，後升任[從三位](../Page/從三位.md "wikilink")[權中納言](../Page/中納言.md "wikilink")，兼任[春宮權大夫](../Page/春宮坊.md "wikilink")；其長女衍子成為宇多天皇的女侍。次年8月，他兼任民部卿；寛平9年（897年），他的女兒和宇多天皇之子[齊世親王結婚](../Page/真寂法親王.md "wikilink")，同年6月，道真升任[權大納言](../Page/大納言.md "wikilink")，兼任[右近衛大將](../Page/近衛大將.md "wikilink")；7月，宇多天皇讓位與[醍醐天皇](../Page/醍醐天皇.md "wikilink")，醍醐天皇繼續重用他，將他升任為[正三位](../Page/正三位.md "wikilink")，並兼任[中宮大夫](../Page/中宮職.md "wikilink")；在當時只有[藤原時平與他擁有](../Page/藤原時平.md "wikilink")“官奏執奏”（又稱為「[内覽](../Page/内覽.md "wikilink")」）的特權。

[Dazaihu_keinai.jpg](https://zh.wikipedia.org/wiki/File:Dazaihu_keinai.jpg "fig:Dazaihu_keinai.jpg")
[Japanese_Crest_Ume.svg](https://zh.wikipedia.org/wiki/File:Japanese_Crest_Ume.svg "fig:Japanese_Crest_Ume.svg")
醍醐天皇即位後，道真一直晉昇，但對於道真掌權感到威脅的[藤原氏開始有所行動](../Page/藤原氏.md "wikilink")，以及中下層貴族中對道真的政治改革感到不安者亦所在多有，於是雙方結合謀圖對付菅原道真；[昌泰](../Page/昌泰.md "wikilink")2年（899年），道真升任為[右大臣並兼任右近衛大將](../Page/右大臣.md "wikilink")。次年，文章博士[三善清行建議道真應該知足退隱享受人生之樂](../Page/三善清行.md "wikilink")，但不被道真接受。901年，他升任[從二位](../Page/從二位.md "wikilink")，但卻被誣告意圖幫助齊世親王簒奪皇位因而獲罪，被眨為[大宰權帥](../Page/大宰府.md "wikilink")，流放至[九州](../Page/九州_\(日本\).md "wikilink")[太宰府](../Page/太宰府.md "wikilink")。宇多上皇聞訊意圖阻止但未能成功。以長子[菅原高視為首的四名子女皆被處以流刑](../Page/菅原高視.md "wikilink")（是為[昌泰之變](../Page/昌泰之變.md "wikilink")）。菅原道真於903年在太宰府病逝、並葬在當地（現在之[太宰府天滿宮](../Page/太宰府天滿宮.md "wikilink")）。

## 歷任官職

※日期為陰曆

  - 862年（[貞觀](../Page/貞觀_\(日本\).md "wikilink")4年）5月17日、成為文章生。
  - 867年（貞觀9年）1月7日、成為文章得業生。　
      - 2月29日、敘任正六位下、下野權少掾。
  - 870年（貞觀12年）9月11日、昇任正六位上下野權少掾如元。
  - 871年（貞觀13年）1月29日、轉任玄蕃助。　　
      - 2月2日、遷任少内記。
  - 874年（貞觀16年）1月7日、昇任從五位下。　　
      - 1月15日、任官兵部少輔。　
      - 2月29日、轉任民部少輔。
  - 877年（[元慶元年](../Page/元慶.md "wikilink")）1月15日、遷任式部少輔。　　
      - 10月18日、兼任祖傳官位文章博士。
  - 879年（元慶3年）1月7日、昇任從五位上式部少輔・文章博士如元。
  - 883年（元慶7年）1月11日、兼任加賀權守。　　
      - 4月、兼任治部權大輔。
  - 886年（[仁和](../Page/仁和.md "wikilink")2年）1月16日、前往四國就任讚岐守。
  - 890年（仁和6年）、任滿讚岐守回京都。
  - 891年（[寬平](../Page/寬平.md "wikilink")3年）2月29日、補任藏人頭。　　
      - 3月9日、兼任式部少輔。　　
      - 4月11日、兼任左中辩。
  - 892年（寬平4年）1月7日、昇任從四位下。藏人頭・式部少輔・左中辩如元。　　
      - 12月5日、兼任左京大夫。
  - 893年（寬平5年）2月16日、補任参議。兼式部大輔。　　
      - 2月22日、兼左大辩。除式部大輔一職。　　
      - 3月15日、兼任勘解由長官。　　
      - 4月1日、兼春宮亮。
  - 894年（寬平6年）8月21日、補任遣唐大使。　　
      - 9月30日、停止遣唐使。　　
      - 12月15日、兼任侍從。
  - 895年（寬平7年）1月11日、兼任近江守。　　
      - 10月26日、昇任從三位、轉任權中納言。　　
      - 11月13日、兼任春宮權大夫。
  - 896年（寬平8年）8月28日、兼任民部卿。
  - 897年（寬平9年）6月19日、轉任權大納言、兼右近衛大將。　　
      - 7月13日、昇任正三位、權大納言・右近衛大將如元。　　
      - 7月26日、兼職中宮大夫。
  - 899年（[昌泰](../Page/昌泰.md "wikilink")2年）2月14日、轉任右大臣、右近衛大將如元。
  - 901年（[延喜元年](../Page/延喜.md "wikilink")）1月7日、昇任從二位、右大臣・右近衛大將如元。　　
      - 1月25日、眨至九州為太宰權帥。
  - 903年（延喜3年）2月25日、逝世。

[《北野天神縁起絵巻》中描绘的清涼殿落雷事件](https://zh.wikipedia.org/wiki/File:Kitano_Tenjin_Engi_Emaki_-_Jokyo_-_Thunder_God.jpg "fig:《北野天神縁起絵巻》中描绘的清涼殿落雷事件")

  - 923年（延喜22年）4月20日、回復為右大臣、追贈正二位。
  - 993年（[正曆](../Page/正曆.md "wikilink")4年）5月20日、追贈正一位左大臣。　　
      - 閏10月20日、追贈太政大臣。

## 事蹟與作品

他將自己所著的詩和散文集結成《菅家文草》全12巻（900年）[1](http://miko.org/~uraki/kuon/furu/text/waka/kanke/bunsou/bunsou.htm)、在太宰府時的作品結集成《菅家後集》（903年左右）、另編著有《[類聚國史](../Page/類聚國史.md "wikilink")》。[2](http://miko.org/~uraki/kuon/furu/text/mokuroku/ruijyu.htm)

詩歌集《菅家御集》經考證則有後世偽作的嫌疑。《新撰萬葉集》\[[http://miko.org/\~uraki/kuon/furu/text/waka/sinsen/sinsenmanyou_f.htm\]傳為道真所作，但據考應為後人託付](http://miko.org/~uraki/kuon/furu/text/waka/sinsen/sinsenmanyou_f.htm%5D傳為道真所作，但據考應為後人託付)。《古今和歌集》\[[http://miko.org/\~uraki/kuon/furu/text/waka/kokin/kokin.htm\]中有節錄他所著2首和歌](http://miko.org/~uraki/kuon/furu/text/waka/kokin/kokin.htm%5D中有節錄他所著2首和歌)。

他亦是[六國史之一](../Page/六國史.md "wikilink")《[日本三代實錄](../Page/日本三代實錄.md "wikilink")》\[[http://miko.org/\~uraki/kuon/furu/text/sandai/sandai.htm\]的編者、於左遷之後的901年](http://miko.org/~uraki/kuon/furu/text/sandai/sandai.htm%5D的編者、於左遷之後的901年)（[延喜元年](../Page/延喜.md "wikilink")）8月完成。

從其祖父時代開始經營的私塾[菅家廊下其中出了不少傑出門生](../Page/菅家廊下.md "wikilink")，往後歷任至朝廷要職者有100人。

### 主要和歌

[Hyakuninisshu_024.jpg](https://zh.wikipedia.org/wiki/File:Hyakuninisshu_024.jpg "fig:Hyakuninisshu_024.jpg")》
菅家（菅原道真）\]\]

  - 此の度は 幣も取り敢へず 手向山 紅葉の錦
    神の随に（這首歌包含在[小倉百人一首內](../Page/小倉百人一首.md "wikilink")）

譯：此去未能執幣而獻，唯手向山如錦紅葉，隨神之側。

  - 海ならず 湛へる水の 底までに 清き心は 月ぞ照らさむ

譯：水深似海亦映月，丹心終可見光輝。

  - 東風吹かば にほひおこせよ 梅の花 主なしとて
    春を忘るな（收錄於『[拾遺和歌集](../Page/拾遺和歌集.md "wikilink")』。後世寫成「春な忘れそ」）

譯：東風若吹起，務使庭香乘風來。吾梅縱失主，亦勿忘春日。

## 死後

道真死後，[京都陸續出現多種異相](../Page/京都.md "wikilink")，先是醍醐天皇的皇子一一病死。後來在皇宮的[清涼殿更遭遇雷擊](../Page/清凉殿落雷事件.md "wikilink")，造成多名死傷者。朝廷對此相當驚恐，認為是道真的怨靈在作祟，因此赦免道真的罪名並[追贈官位](../Page/追贈.md "wikilink")。道真之子也被解除[流放之刑召喚回京都](../Page/流放.md "wikilink")。923年陰曆4月20日，將其從二位太宰權帥回復為正二位右大臣之官位、993年追贈正一位左大臣、同年追贈為太政大臣。據傳當時陷害道真的[藤原時平之後斷了子嗣](../Page/藤原時平.md "wikilink")（其實[藤原時平次子藤原顕忠死時](../Page/藤原時平.md "wikilink")60多歲，有四個兒子），但相反地，對道真友好的時平之弟[藤原忠平](../Page/藤原忠平.md "wikilink")，其子孫則成了[藤原氏的大宗](../Page/藤原氏.md "wikilink")。

[清涼殿落雷事件以後](../Page/清涼殿落雷事件.md "wikilink")，[天皇因為害怕道真的怨靈化身為](../Page/天皇.md "wikilink")[雷神](../Page/雷神.md "wikilink")，為了祭拜「火雷天神」而在京都的北野興建了[北野天滿宮](../Page/北野天滿宮.md "wikilink")。之後道真成了「天神信仰」的一部份，並逐漸普及到日本全國，道真生前因是傑出的學者和詩人，因此被當成學問之神來敬拜。同時也被認為是日本三大怨靈之一。

[江戶時代時以](../Page/江戶時代.md "wikilink")[昌泰之變為題材的戲劇](../Page/昌泰之變.md "wikilink")，『[天神記](../Page/天神記.md "wikilink")』、『[菅原傳授手習鑑](../Page/菅原傳授手習鑑.md "wikilink")』、『[天滿宮菜種御供](../Page/天滿宮菜種御供.md "wikilink")』等一一上演、特別是『[菅原傳授手習鑑](../Page/菅原傳授手習鑑.md "wikilink")』在「人偶劇淨瑠璃」和[歌舞伎的表演相當賣座](../Page/歌舞伎.md "wikilink")、成為[義太夫](../Page/義太夫.md "wikilink")[狂言三大名作之一](../Page/狂言.md "wikilink")。現在仍是反復上演的節目。

近代，尤其是[第二次世界大戰之前](../Page/第二次世界大戰.md "wikilink")，道真被認為是皇室的忠臣。1928年[講談社出版的雜誌](../Page/講談社.md "wikilink")《》
（King），有一篇模仿菅原道真事蹟「**恩賜御衣**今在此，捧持每日拜餘香」\[1\]的句子被改寫成「**坊主吽子**今在此，捧持每日拜餘香」（坊主：[和尚](../Page/和尚.md "wikilink")，吽子：大便），結果當時人在[伊香保溫泉的講談社社長](../Page/伊香保溫泉.md "wikilink")[野間清治因此受到不滿人士的攻擊](../Page/野間清治.md "wikilink")。

## 其他逸事

  - 據傳道真素有胃疾，他胃痛時常以一塊加熱過的石頭放在肚子上以減輕不適。
  - 在現今[大阪市](../Page/大阪市.md "wikilink")[東淀川區有](../Page/東淀川區.md "wikilink")「淡路」、「菅原」的地名、據說是道真被貶謫至太宰府途中經過時，誤認當地為[淡路島之故](../Page/淡路島.md "wikilink")。

## 參考

「日本眾神辭典(日本の神々の事典)」　[東京](../Page/東京.md "wikilink")[學習研究社出版](../Page/學習研究社.md "wikilink")
1997年 ISBN 4056016291

## 相關事物

  - [天滿宮](../Page/天滿宮.md "wikilink")
  - 赤兵子天神

## 外部連結

  - [菅家文草](http://miko.org/~uraki/kuon/furu/text/waka/kanke/bunsou/bunsou.htm)
  - [山陰亭](http://www.michiza.net/)
  - [能面 長澤重春能面集：天神](http://nohmask.exblog.jp/i26)

[Category:平安時代歌人](../Category/平安時代歌人.md "wikilink")
[Category:菅原氏](../Category/菅原氏.md "wikilink")
[Category:日本漢詩詩人](../Category/日本漢詩詩人.md "wikilink")
[Category:日本歷史學家](../Category/日本歷史學家.md "wikilink")
[Category:智慧之神](../Category/智慧之神.md "wikilink")
[Category:日本人物神](../Category/日本人物神.md "wikilink")
[Category:雷神](../Category/雷神.md "wikilink")
[Category:天滿宮](../Category/天滿宮.md "wikilink")
[Category:日本紙幣上的人物](../Category/日本紙幣上的人物.md "wikilink")
[Category:大和國出身人物](../Category/大和國出身人物.md "wikilink")
[Category:遣唐使](../Category/遣唐使.md "wikilink")
[Category:平安時代前期貴族](../Category/平安時代前期貴族.md "wikilink")

1.  [菅家後集　卷十三　菅家後草　卷第十三](http://miko.org/~uraki/kuon/furu/text/waka/kanke/bunsou/gosou13.htm#482)