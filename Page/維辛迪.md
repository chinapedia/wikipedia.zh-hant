**维森特·罗德里格斯·吉连**（**Vicente Rodríguez
Guillén**，），通常稱**維辛迪**（**Vicente**），[西班牙足球運動員](../Page/西班牙.md "wikilink")，司職左翼。

## 生平

### 球會

維辛迪於1997/98年球季在西班牙小球會[萊萬特展開球員生涯](../Page/萊萬特體育聯盟.md "wikilink")。兩季後，西班牙著名球會[華倫西亞宣布與他簽約](../Page/華倫西亞足球俱樂部.md "wikilink")，並迅即受到同隊左翼球員[基利·干沙利斯](../Page/基利·干沙利斯.md "wikilink")（Kily
González）的首發位置挑戰。儘管他並非這場位置競爭中的大贏家，他仍能在其首季西甲聯賽中射入五球，並在[歐冠杯得到十三次出場機會](../Page/歐冠杯.md "wikilink")。令人惋惜的是，他在歐冠杯決賽對[拜仁慕尼黑一仗並未被派上陣](../Page/拜仁慕尼黑.md "wikilink")，而球隊亦未能奪得該杯賽冠軍。

[賓尼迪斯於](../Page/賓尼迪斯.md "wikilink")2001/02年球隊走馬上任後，維辛迪開始被球隊器重，而他亦未有令球迷失望，並為球隊帶來三十年來首次西甲聯賽冠軍，並憑上佳的演出，把前位置競爭者基利干沙利斯擠出球隊。

於效力球隊11年後，維辛迪於2011年夏季遭華倫西亞放棄。9月2日他出國加盟[英冠球會](../Page/英冠.md "wikilink")[白禮頓](../Page/布莱顿足球俱乐部.md "wikilink")，簽約效力一年\[1\]。

### 國家隊

維辛迪早在2001年已被國家隊徵召入伍，但常受傷患影響未能出戰正式大賽，如他沒有出現在2002年世界杯名單裡。2004年隨隊出戰[歐洲國家杯](../Page/歐洲國家杯.md "wikilink")，因不敵同組的葡萄牙而首圈出局。2006年因受傷患困擾，維辛迪又一次落選於[2006年世界杯大軍](../Page/2006年世界杯.md "wikilink")。

### 國家隊入球

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>Date</p></th>
<th><p>Venue</p></th>
<th><p>Opponent</p></th>
<th><p>Score</p></th>
<th><p>Result</p></th>
<th><p>Competition</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1.</p></td>
<td><p>19 November 2003</p></td>
<td><p><a href="../Page/Ullevaal_Stadion.md" title="wikilink">Ullevaal</a>, <a href="../Page/Oslo.md" title="wikilink">Oslo</a>, <a href="../Page/Norway.md" title="wikilink">Norway</a></p></td>
<td></td>
<td><p>0–<strong>2</strong></p></td>
<td><p>0–3</p></td>
<td><p><a href="../Page/UEFA_Euro_2004_qualifying_play-offs#Second_leg.md" title="wikilink">Euro 2004 qualifying</a></p></td>
</tr>
<tr class="even">
<td><p>2.</p></td>
<td><p>8 September 2004</p></td>
<td><p><a href="../Page/Bilino_Polje.md" title="wikilink">Bilino Polje</a>, <a href="../Page/Zenica.md" title="wikilink">Zenica</a>, <a href="../Page/Bosnia_and_Herzegovina.md" title="wikilink">Bosnia and Herzegovina</a></p></td>
<td></td>
<td><p>0–<strong>1</strong></p></td>
<td><p>1–1</p></td>
<td><p>[<a href="https://en.wikipedia.org/wiki/2006_FIFA_World_Cup_qualification_–_UEFA_Group_7">世界盃外圍賽 – UEFA Group 7|2006 World Cup qualification</a></p></td>
</tr>
<tr class="odd">
<td><p>3.</p></td>
<td><p>17 August 2005</p></td>
<td><p><a href="../Page/El_Molinón.md" title="wikilink">El Molinón</a>, <a href="../Page/Gijón.md" title="wikilink">Gijón</a>, Spain</p></td>
<td></td>
<td><p><strong>2</strong>–0</p></td>
<td><p>2–0</p></td>
<td><p><a href="../Page/Exhibition_game.md" title="wikilink">友誼賽</a></p></td>
</tr>
</tbody>
</table>

### 榮譽

2001-2002 2003-2004[西班牙甲組足球聯賽](../Page/西班牙甲組足球聯賽.md "wikilink")

2003-2004[歐洲足協盃](../Page/歐洲足協盃.md "wikilink")
2004[歐洲超級盃](../Page/歐洲超級盃.md "wikilink")

2007-2008年[西班牙國王盃](../Page/西班牙國王盃.md "wikilink")

2004年[西班牙足球先生](https://en.wikipedia.org/wiki/Don_Bal%C3%B3n_Award)

### 生涯數據統計

\[2\]

| Club                                                                        | Season                                                                | League | Cup   | Europe | Total |
| --------------------------------------------------------------------------- | --------------------------------------------------------------------- | ------ | ----- | ------ | ----- |
| Apps                                                                        | Goals                                                                 | Apps   | Goals | Apps   | Goals |
| [Levante](../Page/Levante_UD.md "wikilink")                                 | [1997–98](../Page/1997–98_Segunda_División.md "wikilink")             | 3      | 1     | 0      | 0     |
| [1998–99](../Page/1998–99_Segunda_División_B.md "wikilink")                 | 16                                                                    | 1      | 2     | 0      | —     |
| [1999–00](../Page/1999–2000_Segunda_División.md "wikilink")                 | 35                                                                    | 7      | 1     | 0      | —     |
| Total                                                                       | 54                                                                    | 9      | 3     | 0      | —     |
| [巴伦西亚](../Page/巴伦西亚足球俱乐部.md "wikilink")                                     | [2000–01](../Page/2000–01_La_Liga.md "wikilink")                      | 33     | 5     | 2      | 1     |
| [2001–02](../Page/2001–02_La_Liga.md "wikilink")                            | 31                                                                    | 1      | 1     | 0      | 7     |
| [2002–03](../Page/2002–03_La_Liga.md "wikilink")                            | 28                                                                    | 1      | 0     | 0      | 11    |
| [2003–04](../Page/2003–04_La_Liga.md "wikilink")                            | 33                                                                    | 12     | 2     | 0      | 7     |
| [2004–05](../Page/2004–05_La_Liga.md "wikilink")                            | 12                                                                    | 3      | 0     | 0      | 3     |
| [2005–06](../Page/2005–06_La_Liga.md "wikilink")                            | 21                                                                    | 3      | 1     | 0      | 0     |
| [2006–07](../Page/2006–07_La_Liga.md "wikilink")                            | 16                                                                    | 4      | 0     | 0      | 5     |
| [2007–08](../Page/2007–08_La_Liga.md "wikilink")                            | 17                                                                    | 0      | 4     | 1      | 4     |
| [2008–09](../Page/2008–09_La_Liga.md "wikilink")                            | 27                                                                    | 6      | 6     | 4      | 5     |
| [2009–10](../Page/2009–10_La_Liga.md "wikilink")                            | 11                                                                    | 0      | 1     | 0      | 1     |
| [2010–11](../Page/2010–11_La_Liga.md "wikilink")                            | 13                                                                    | 1      | 3     | 2      | 1     |
| Total                                                                       | 242                                                                   | 36     | 20    | 8      | 57    |
| [Brighton & Hove Albion](../Page/Brighton_&_Hove_Albion_F.C..md "wikilink") | [2011–12](../Page/2011–12_Football_League_Championship.md "wikilink") | 17     | 3     | 2      | 0     |
| [2012–13](../Page/2012–13_Football_League_Championship.md "wikilink")       | 11                                                                    | 2      | 1     | 0      | 0     |
| Total                                                                       | 28                                                                    | 5      | 3     | 0      | 0     |
| Career totals                                                               | 324                                                                   | 48     | 25    | 8      | 62    |

## 參考資料

## 外部連結

  - [Valencia CF
    profile](http://www.ciberche.net/histoche/jugadores/19?pid=15&show_all=1)


  - [BDFutbol profile](http://www.bdfutbol.com/en/j/j2583.html)

  - [National team
    data](https://web.archive.org/web/20120531195332/http://futbol.sportec.es/seleccion/ficha_jugador.asp?j=662&n=vicente%2Fvicente%2Frodriguez%2Fguillem)


  - [CiberChe biography and
    stats](http://www.ciberche.net/histoche/jugadores/19?pid=15)

  -
  - [Transfermarkt
    profile](http://www.transfermarkt.co.uk/en/vicente/leistungsdaten/spieler_7582.html)

  - [FootballDatabase profile and
    statistics](http://www.footballdatabase.com/index.php?page=player&Id=551&b=true&pn=Vicente_Rodr%C3%ADguez_Guillem)

[Category:西班牙足球運動員](../Category/西班牙足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:利雲特球員](../Category/利雲特球員.md "wikilink")
[Category:華倫西亞球員](../Category/華倫西亞球員.md "wikilink")
[Category:白禮頓球員](../Category/白禮頓球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")

1.
2.