[Prestosuchus_AMNH.jpg](https://zh.wikipedia.org/wiki/File:Prestosuchus_AMNH.jpg "fig:Prestosuchus_AMNH.jpg")\]\]
[Prestosuchus_chiniquensis_Expominer_07.jpg](https://zh.wikipedia.org/wiki/File:Prestosuchus_chiniquensis_Expominer_07.jpg "fig:Prestosuchus_chiniquensis_Expominer_07.jpg")
[Tecodonte.JPG](https://zh.wikipedia.org/wiki/File:Tecodonte.JPG "fig:Tecodonte.JPG")
**迅猛鱷屬**（屬名：*Prestosuchus*）又名**布里斯托鱷**，屬於[勞氏鱷目](../Page/勞氏鱷目.md "wikilink")[迅猛鱷科](../Page/迅猛鱷科.md "wikilink")。如同牠的迅猛鱷科近親，迅猛鱷擁有縱深的頭顱，以及鋸齒狀牙齒。迅猛鱷生存於晚[三疊紀的](../Page/三疊紀.md "wikilink")[巴西](../Page/巴西.md "wikilink")。迅猛鱷的身長接近5公尺。迅猛鱷的外表類似[恐龍](../Page/恐龍.md "wikilink")，因為牠們的大型身體與直立姿勢；但牠們其實是[主龍類](../Page/主龍類.md "wikilink")，是恐龍的近親。迅猛鱷是種伏擊掠食動物，以小型動物為食（例如[舟爪龍](../Page/舟爪龍.md "wikilink")）。迅猛鱷的腿部強壯，顯示牠們可以快速奔跑。迅猛鱷的近親有[蜥鱷](../Page/蜥鱷.md "wikilink")、[波斯特鱷](../Page/波斯特鱷.md "wikilink")。

迅猛鱷的化石是在1938年由[德國古生物學家](../Page/德國.md "wikilink")[弗雷德里克·馮·休尼](../Page/弗雷德里克·馮·休尼.md "wikilink")（Friedrich
von
Huene）發現，他當時在巴西[南里奧格蘭德州旅行中](../Page/南里奧格蘭德州.md "wikilink")；這個地區後來成為Paleorrota地質公園\[1\]。在2010年，[巴西路德大學的化石挖掘團隊](../Page/巴西路德大學.md "wikilink")，在南里奧格蘭德州發燕一個相當完整的身體骨骼，包含保良好的後肢\[2\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [Drawing of *Prestosuchus
    chiniquensis*](http://www.ufrgs.br/geociencias/paleo/tecodonte.jpg)
  - [Dinosaurs of Rio grande do
    Sul.](https://web.archive.org/web/20080127175240/http://www.royalsul.com.br/paleo/galeria.asp)
  - [Brazilian Society of
    Paleontology.](https://web.archive.org/web/20081202160307/http://www.sbpbrasil.org/home.htm)
  - [Palaeos.com:
    Prestosuchidae](https://web.archive.org/web/20060522234154/http://www.palaeos.com/Vertebrates/Units/270Archosauromorpha/270.650.html)

[Category:勞氏鱷目](../Category/勞氏鱷目.md "wikilink")

1.  [Picture and informations about of *Prestosuchus chiniquensis*
    (UFRGS)](http://www.ufrgs.br/geociencias/paleo/tecodontei.html)
2.  [(UK Daily Mail) "World's most complete fossil of pre-dinosaur
    predator
    discovered"](http://www.dailymail.co.uk/sciencetech/article-1278021/Prestosuchus-chiniquensis-Fearsome-predator-roamed-Earth-dinosaurs-discovered.html#):
    accessed 1113 May 2010.