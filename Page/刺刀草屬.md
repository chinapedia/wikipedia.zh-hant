**刺刀草屬**是[傘形科下的一](../Page/傘形科.md "wikilink")[屬](../Page/屬_\(生物\).md "wikilink")，包含有40個左右的[物種](../Page/物種.md "wikilink")，原生於[紐西蘭和](../Page/紐西蘭.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")。刺刀草一般長得像被堅硬、尖銳的葉叢所圍繞的長矛。

[Category:傘形科](../Category/傘形科.md "wikilink")