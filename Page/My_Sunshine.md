《****》是[香港歌唱組合](../Page/香港.md "wikilink")[Cream於](../Page/Cream_\(香港樂隊\).md "wikilink")2006年8月演唱會的同名專輯，同時也是他離開[輝皇娛樂前](../Page/輝皇娛樂.md "wikilink")，亦是她們的最後一張專輯。

## 曲目列表

《My
Sunshine》一共收錄了5首歌及5首自唱音樂。其中《幫哥哥找女友》是[Cream與](../Page/Cream_\(香港樂隊\).md "wikilink")[Chips的合唱歌](../Page/Chips.md "wikilink")，而《My
Sunshine》一曲則是國語歌。

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>曲名</p></th>
<th><p>作曲人</p></th>
<th><p>作詞人</p></th>
<th><p>編曲</p></th>
<th><p>監製</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1.</p></td>
<td><p>宮崎駿的童話</p></td>
<td><p>張佳添@宇宙大爆炸</p></td>
<td><p><a href="../Page/郭薾多.md" title="wikilink">郭薾多</a></p></td>
<td><p>BC@宇宙大爆炸</p></td>
<td><p>陳歷恆</p></td>
</tr>
<tr class="even">
<td><p>2.</p></td>
<td><p>愚人日記</p></td>
<td><p>BC@宇宙大爆炸</p></td>
<td><p>BC@宇宙大爆炸／PC</p></td>
<td><p>BC@宇宙大爆炸</p></td>
<td><p>BC@宇宙大爆炸</p></td>
</tr>
<tr class="odd">
<td><p>3.</p></td>
<td><p>幫哥哥找女友</p></td>
<td><p><a href="../Page/章霈迎.md" title="wikilink">Kenix Cheang</a></p></td>
<td><p>陳歷恆、<br />
陳炳洋</p></td>
<td><p><a href="../Page/章霈迎.md" title="wikilink">Kenix Cheang</a></p></td>
<td><p>陳歷恆</p></td>
</tr>
<tr class="even">
<td><p>4.</p></td>
<td><p>學生應否談戀愛</p></td>
<td><p><strong><a href="../Page/盧嘉寶.md" title="wikilink">盧嘉寶</a>@<a href="../Page/Cream_(香港樂隊).md" title="wikilink">Cream</a></strong></p></td>
<td><p>陳歷恆、<br />
陳炳洋</p></td>
<td><p>王兆綽</p></td>
<td><p>Hit Group</p></td>
</tr>
<tr class="odd">
<td><p>5.</p></td>
<td><p>My Sunshine</p></td>
<td><p>胡芳芳</p></td>
<td><p>胡芳芳</p></td>
<td><p>黃家昌</p></td>
<td><p>陳歷恆</p></td>
</tr>
<tr class="even">
<td><p>6.</p></td>
<td><p>宮崎駿的童話 <small>Music</small></p></td>
<td><p>張佳添@宇宙大爆炸</p></td>
<td><p>郭薾多</p></td>
<td><p>——</p></td>
<td><p>——</p></td>
</tr>
<tr class="odd">
<td><p>7.</p></td>
<td><p>愚人日記 <small>Music</small></p></td>
<td><p>BC@宇宙大爆炸</p></td>
<td><p>BC@宇宙大爆炸</p></td>
<td><p>——</p></td>
<td><p>——</p></td>
</tr>
<tr class="even">
<td><p>8.</p></td>
<td><p>幫哥哥找女友 <small>Music</small></p></td>
<td><p><a href="../Page/章霈迎.md" title="wikilink">Kenix Cheang</a></p></td>
<td><p>陳歷恆、<br />
陳炳洋</p></td>
<td><p>——</p></td>
<td><p>——</p></td>
</tr>
<tr class="odd">
<td><p>9.</p></td>
<td><p>學生應否談戀愛 <small>Music</small></p></td>
<td><p><strong>盧嘉寶@<a href="../Page/Cream_(香港樂隊).md" title="wikilink">Cream</a></strong></p></td>
<td><p>陳歷恆、<br />
陳炳洋</p></td>
<td><p>——</p></td>
<td><p>——</p></td>
</tr>
<tr class="even">
<td><p>10.</p></td>
<td><p>My Sunshine <small>Music</small></p></td>
<td><p>胡芳芳</p></td>
<td><p>胡芳芳</p></td>
<td><p>——</p></td>
<td><p>——</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [《My
    Sunshine》全碟介紹宣傳聲帶](http://music.163888.net/openmusic.aspx?id=4320509)
  - [《My
    Sunshine》於Yesasia的介紹](http://global.yesasia.com/gb/PrdDept.aspx/code-c/section-music/pid-1004477833/)

[Category:Cream音樂專輯](../Category/Cream音樂專輯.md "wikilink")
[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")