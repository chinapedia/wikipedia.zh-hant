**沃克·施隆多夫**（**Volker
Schlöndorff**，）是一位[德國](../Page/德國.md "wikilink")[導演](../Page/導演.md "wikilink")，也是[德國新浪潮的成員之一](../Page/德國新浪潮.md "wikilink")，並曾經贏得[金棕櫚獎和](../Page/金棕櫚獎.md "wikilink")[奥斯卡最佳外语片](../Page/奥斯卡最佳外语片.md "wikilink")。

## 生平

## 作品

  - 1979年：《[錫鼓](../Page/錫鼓.md "wikilink")》
  - 1984年：《[青樓紅杏](../Page/青樓紅杏_\(1984年電影\).md "wikilink")》
  - 1990年：《[使女的故事](../Page/使女的故事.md "wikilink")》

## 外部連結

  -
  - [Volker Schlöndorff Faculty Website European Graduate
    School](http://www.egs.edu/faculty/volkerschlondorff.html)

  - [Volker Schlöndorff's Cinématon - A 4 minutes online portrait by
    Gérard
    Courant](http://www.gerardcourant.com/projection.php?t=c&film=572)

[Category:德國導演](../Category/德國導演.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:黑森人](../Category/黑森人.md "wikilink")
[Category:威斯巴登人](../Category/威斯巴登人.md "wikilink")
[Category:奥斯卡最佳外语片获奖导演](../Category/奥斯卡最佳外语片获奖导演.md "wikilink")