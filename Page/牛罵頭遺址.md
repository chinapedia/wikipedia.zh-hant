[Chin-shuei_Niumatou_site_in_Taichung_County.JPG](https://zh.wikipedia.org/wiki/File:Chin-shuei_Niumatou_site_in_Taichung_County.JPG "fig:Chin-shuei_Niumatou_site_in_Taichung_County.JPG")
**牛罵頭遺址**是位於[台灣](../Page/台灣.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[清水區](../Page/清水區_\(臺灣\).md "wikilink")[鰲峰山的一個考古遺址](../Page/鰲峰山.md "wikilink")，也是台灣中部[新石器時代文化牛罵頭文化的](../Page/新石器時代.md "wikilink")[命名遺址](../Page/命名遺址.md "wikilink")。[臺中市政府文化局已將原址規劃為](../Page/臺中市政府文化局.md "wikilink")「**牛罵頭遺址文化園區**」並對外開放。

## 歷史

  - 牛罵頭遺址最早可能在1937年興建[清水神社時已顯露有部分文物出土](../Page/清水神社.md "wikilink")，[神社社主](../Page/神社.md "wikilink")[西村昌隆即收藏了部分遺址出土石器及陶器](../Page/西村昌隆.md "wikilink")。
  - 1943年日本學者[國分直一在](../Page/國分直一.md "wikilink")[大肚臺地西緣及](../Page/大肚臺地.md "wikilink")[大肚溪北岸進行考古調查時才正式發現](../Page/大肚溪.md "wikilink")「牛罵頭遺址」。
  - [戰後初期](../Page/臺灣戰後時期.md "wikilink")[劉斌雄](../Page/劉斌雄.md "wikilink")、[張光直](../Page/張光直.md "wikilink")、[宋文薰等學者均曾加以調查研究](../Page/宋文薰.md "wikilink")，對於該遺址的年代及其所代表的文化層有所了解。
  - 1975年，人類學者正以牛罵頭遺址之名來命名中部之新石器文化，但由於當地為[陸軍營區](../Page/中華民國陸軍.md "wikilink")，一直未能深入調查挖掘。
  - 2001年由[劉益昌](../Page/劉益昌.md "wikilink")、[溫振華學者進行挖掘](../Page/溫振華.md "wikilink")，出土大量器物。
  - 2002年由台中縣文化局指定為縣定古蹟，並將該地規畫為「**牛罵頭遺址文化園區**」。
  - 2005年陸軍軍營遷出，開始整理園區。
  - 2010年12月25日台中縣、市合併，逕改列直轄市定遺址。
  - 2013年2月，「牛罵頭遺址界牆剝取物」被指定為一般古物。7月，開放民眾預約入園，並將重要古物「沙鹿區南勢坑遺址文化層斷面（灰坑剝取物）」移置園區內保存並對外展示，同時將「鰲峰山營區及原清水神社遺構」登錄為歷史建築。
  - 2014年7月，改為無需預約開放入園\[1\]。

## 遺址內容

牛罵頭遺址的年代約在3500年前至4500年前，除了下層屬較早的牛罵頭文化外，也包括上層[番仔園文化](../Page/番仔園文化.md "wikilink")，中層的[營埔文化等不同時期的遺留](../Page/營埔文化.md "wikilink")。牛罵頭文化層出土的[陶器](../Page/陶器.md "wikilink")，以[紅](../Page/紅.md "wikilink")、[褐色陶為主](../Page/褐色.md "wikilink")，亦有少量[黑色陶器](../Page/黑色.md "wikilink")，手製紋飾以拍印[繩紋為主](../Page/繩紋.md "wikilink")，其陶器的特色可看出承續了[大坌坑文化](../Page/大坌坑文化.md "wikilink")，石器以為獵和農具為主，大量石製農具可看出當時人應是以農耕為主，並兼行漁撈和[狩獵](../Page/狩獵.md "wikilink")。除器物外也有墓葬出土。

## 相片集

[File:牛罵頭遺址界牆剝取物.jpg|牛罵頭遺址界牆剝取物](File:牛罵頭遺址界牆剝取物.jpg%7C牛罵頭遺址界牆剝取物)
[File:牛罵頭遺址出土文物.jpg|牛罵頭遺址出土文物](File:牛罵頭遺址出土文物.jpg%7C牛罵頭遺址出土文物)
[File:牛罵頭遺址出土文物典存.jpg|出土文物，原址典存](File:牛罵頭遺址出土文物典存.jpg%7C出土文物，原址典存)
[File:清水神社石燈.jpg|清水神社石燈](File:清水神社石燈.jpg%7C清水神社石燈)
[File:清水神社石狛.jpg|清水神社石狛](File:清水神社石狛.jpg%7C清水神社石狛)
[File:鰲峰山營區司令台.jpg|鰲峰山營區司令台](File:鰲峰山營區司令台.jpg%7C鰲峰山營區司令台)
[File:鰲峰山營區營舍.jpg|鰲峰山營區軍事營舍](File:鰲峰山營區營舍.jpg%7C鰲峰山營區軍事營舍)
<File:沙鹿區南勢坑遺址文化層斷面（灰坑剝取物>）.jpg|沙鹿區南勢坑遺址文化層斷面（灰坑剝取物），保存在園區內
[File:頂橋仔遺址出土文物.jpg|頂橋仔遺址出土文物，保存在園區內](File:頂橋仔遺址出土文物.jpg%7C頂橋仔遺址出土文物，保存在園區內)

## 周邊

  - [鰲峰山公園](../Page/鰲峰山市鎮公園.md "wikilink")
  - [社口遺址](../Page/社口遺址.md "wikilink")
  - [中社遺址](../Page/中社遺址.md "wikilink")

## 參考資料

<references/>

## 外部連結

  - [牛罵頭遺址文化園區](http://niumatou.taichung.gov.tw/)

[Category:臺中市考古遺址](../Category/臺中市考古遺址.md "wikilink")
[N](../Category/台湾新石器时代遗址.md "wikilink")
[Category:臺中市文化資產](../Category/臺中市文化資產.md "wikilink")
[Category:清水區 (臺灣)](../Category/清水區_\(臺灣\).md "wikilink")

1.  [台中地下故事
    牛罵頭遺址看透透](http://news.ltn.com.tw/news/local/paper/797458)，自由時報，2014-07-20