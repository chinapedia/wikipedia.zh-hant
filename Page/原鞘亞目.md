**原鞘亞目**（[學名](../Page/學名.md "wikilink")：**Archostemata**）是[昆蟲綱](../Page/昆蟲綱.md "wikilink")[鞘翅目下的一個](../Page/鞘翅目.md "wikilink")[亞目](../Page/亞目.md "wikilink")，也是種類最少的亞目，其下有[長扁甲總科](../Page/長扁甲總科.md "wikilink")（Cupedoidea），有數個[科](../Page/科_\(生物\).md "wikilink")，包括較為知名的[長扁蟲科](../Page/長扁蟲科.md "wikilink")（Cupedidae）、[微長扁蟲科](../Page/微長扁蟲科.md "wikilink")（Micromalthidae）、[澳洲長扁蟲科](../Page/澳洲長扁蟲科.md "wikilink")（Ommatidae）與[溝胸長扁蟲科](../Page/溝胸長扁蟲科.md "wikilink")（Tetraphaleridae）等等，共有約50種昆蟲包含於此亞目中。

## 參考文獻

  - Richard E. White, *A Field Guide to Beetles of North America*
    (Houghton Mifflin Company, 1983)

## 外部連結

  - [Tree of Life -
    Archostemata](http://tolweb.org/tree?group=Archostemata)

[Category:原鞘亞目](../Category/原鞘亞目.md "wikilink")