{{ Supernova | | name = 超新星SN 1181 | image =
[3C58_xray.jpg](https://zh.wikipedia.org/wiki/File:3C58_xray.jpg "fig:3C58_xray.jpg")
| caption= [3C
58](../Page/3C58.md "wikilink")，[波霎風星雲](../Page/波霎風星雲.md "wikilink")，被認為是超新星SN
1181的殘骸 | epoch = ? | type = 不詳 | SNRtype = 不詳 | host =
[銀河系](../Page/銀河系.md "wikilink") | constellation =
[仙后座](../Page/仙后座.md "wikilink") | gal = g.130.719+03.084 | ra =
2<sup>h</sup> 2<sup>m</sup> | dec = +64° 37′ | discovery = 1181
[世界時](../Page/世界時.md "wikilink") | iauc = | mag_v = -1? |
distance = \>8kpc | progenitor = 不詳 | progenitor_type = 不詳 | b-v = 不詳 |
notes = 在夜晚可見185天}}

**SN
1181**於1181年8月4日與6日間被觀測到，[中國和](../Page/中國.md "wikilink")[日本的](../Page/日本.md "wikilink")[天文學家在](../Page/天文學家.md "wikilink")8份不同的文書上記錄了這顆[超新星](../Page/超新星.md "wikilink")。

在歷史上，在[銀河系內只有](../Page/銀河系.md "wikilink")8顆肉眼能觀測到的超新星，這一顆位於[仙后座](../Page/仙后座.md "wikilink")，在夜晚能觀測到的天數大約是185天。

每秒旋轉15次的電波和X-射線源[脈衝星](../Page/脈衝星.md "wikilink")[J0205+6449](../Page/3C58.md "wikilink")（也稱為[3C
58](../Page/3C58.md "wikilink")）可能是這顆[超新星爆發的殘骸](../Page/超新星.md "wikilink")。如果這顆[超新星和](../Page/超新星.md "wikilink")[脈衝星是一致的](../Page/脈衝星.md "wikilink")，那麼它的轉速依然和剛形成時的速度一致\[1\]。

相較於[蟹狀星雲](../Page/蟹狀星雲.md "wikilink")，是在1054年爆炸的[超新星](../Page/超新星.md "wikilink")[SN
1054的](../Page/SN_1054.md "wikilink")[殘骸](../Page/超新星殘骸.md "wikilink")，在相同的時間內已經喪失了三分之二的轉動能量。但是，最近對[3c
58的無線電測量顯示這顆超新星殘骸可能比SN](../Page/3C58.md "wikilink")
1181更老，因此兩者可能並無關聯\[2\]。

## 相關條目

  - [明亮的超新星列表](../Page/明亮的超新星列表.md "wikilink")

## 參考資料

[Category:仙后座](../Category/仙后座.md "wikilink")
[Category:超新星遺迹](../Category/超新星遺迹.md "wikilink")

1.
2.