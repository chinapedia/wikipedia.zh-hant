**极品飞车 III：闪电追踪**（**Need for Speed III: Hot
Pursuit**，副标题也译作**热力追踪**，在日本又称作**Over
Drivin' III: Hot
Pursuit**），是[美国艺电EA](../Page/美国艺电.md "wikilink")）推出的《[极品飞车](../Page/极品飞车系列.md "wikilink")》系列赛车游戏之一，于1998年推出。

## 出场车型

本作中登场的车辆几乎都是90年代的世界名车，只有两辆例外：[兰博基尼Countach](../Page/兰博基尼.md "wikilink")
25周年版本，诞生于80年代；终极奖励跑车El Nino，为科幻虚构。

登场的车辆被分为A,B,C三个级别。其中，A级为超级跑车（Supercar），B级为高性能运动跑车（Sportscar），C级为大马力豪华轿跑车。以下是登场车辆清单：

A级：

  - [奔驰CLK](../Page/奔驰.md "wikilink")-GTR \* (1997)
  - [捷豹XJR](../Page/捷豹.md "wikilink")-15 \* （1991）
  - 兰博基尼Diablo SV \*\*（1995）
  - [法拉利](../Page/法拉利汽車.md "wikilink")550 Maranello （1996）
  - [阿尔法-罗密欧](../Page/阿尔法·罗密欧.md "wikilink") Italdesign Scighera (1997)
  - El Nino （厄尔尼诺） \* \*\*（虚构）

B级：

  - 兰博基尼Countach 25周年纪念款（1988）
  - 法拉利F355 F1 (1997)
  - [雪弗莱Corvette](../Page/雪弗兰.md "wikilink") C5 \*\* \*\*\*
    \*\*\*\*（1996）

C级：

  - 奔驰SL600 R129 (1997)
  - [阿斯顿马丁](../Page/阿斯顿·马丁.md "wikilink") DB7 (1994)
  - 捷豹XK8 (1996)

注释：

"\*" 为必须解锁（完成指定赛事或输入秘籍）获得的隐藏车 "\*\*" 为可用作警车的车辆 "\*\*\*"
一种特别涂装，即1997年Indy 500赛事涂装的Corvette
C5仅出现于早期的[沃尔玛特别版中](../Page/沃尔玛.md "wikilink")，而大陆引进的早期盗版，多属此版，因而有此型车。除了涂装不同及不能变换颜色，实际性能与标准版毫无二致。
"\*\*\*\*" C5的手动版和自动版装备了不同的变速箱，自动为4速，手动为6速，显然后者的性能更佳，这是符合事实的。

“下载新车”功能是当年EA重点宣传的一大功能，玩家可通过国际互联网，下载到[EA官方推出的增补车辆](../Page/EA.md "wikilink")，甚至包括相关的车辆介绍和影集（Showcase），这些“新车”至今能从EA的FTP上下载到，他们分别是：

  - [Lister Storm](../Page/Lister_Storm.md "wikilink") (1994, A级）
  - [法拉利 456M GT](../Page/法拉利_456M_GT.md "wikilink") （1999，B级）
  - [捷豹XKR](../Page/捷豹XKR.md "wikilink") （1998，C级）
  - [Spectre R42](../Page/Spectre_R42.md "wikilink") (1999, C级）

## 游戏音轨

### 竞赛音乐

1.  Little Sweaty Sow
2.  Hydrus 606
3.  Snorkeling Cactus Weasels
4.  Cetus 808
5.  Rear Flutterblast \#19
6.  Aquilla 303
7.  Snow Bags
8.  Knossos
9.  Flimsy
10. Warped

### 菜单音乐

1.  Whacked
2.  Romulus 3
3.  Minotaur
4.  Pi
5.  Triton
6.  Monster
7.  Whipped

## 参考资料

## 参见

  - [极品飞车系列](../Page/极品飞车系列.md "wikilink")

[3](../Category/極速快感系列.md "wikilink")
[N](../Category/1998年电子游戏.md "wikilink")
[N](../Category/Windows遊戲.md "wikilink")
[N](../Category/PlayStation_\(游戏机\)游戏.md "wikilink")