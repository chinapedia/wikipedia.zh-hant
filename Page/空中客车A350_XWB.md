**空中客车A350 XWB**（Airbus A350
XWB），是歐洲飛機製造商[空中客车所發展的長程](../Page/空中客车.md "wikilink")[雙引擎](../Page/雙發噴射機.md "wikilink")[廣體客機系列](../Page/宽体飞机.md "wikilink")。A350是最先在機身與機翼同時使用[碳纖維強化聚合物的空中巴士航空器](../Page/碳纖維強化聚合物.md "wikilink")。\[1\]在典型的三等級座艙配置裡，它可搭載280至366位旅客。\[2\]A350定位為接替[A340](../Page/空中客车A340.md "wikilink")，主要競爭對手為[波音777及](../Page/波音777.md "wikilink")[787](../Page/波音787.md "wikilink")。

A350最初構想於2004年，以[A330機身搭配新空氣動力學外形和引擎](../Page/空中巴士A330.md "wikilink")。2006年，空中巴士重新設計了這架飛機，以回應幾個主要潛在客戶的負面反饋，生產出“A350
XWB”（超廣體，eXtra Wide
Body）。開發成本估計為110億歐元（150億美元或95億英鎊）。截至2019年2月，空中巴士已收到全球47家客戶的852架A350訂單。\[3\]A350原型機於2013年6月14日首次從法國土魯斯起飛。
2014年9月收到[歐洲航空安全局的型式認證](../Page/歐洲航空安全局.md "wikilink")，兩個月後獲得[聯邦航空管理局的認證](../Page/聯邦航空管理局.md "wikilink")。
2015年1月15日，A350-900在[卡達航空啟航](../Page/卡達航空.md "wikilink")；而且2018年2月24日的A350-1000也是在同一家航空公司進行，A350-900的最大續航距離達15000公里，A350-1000的最大續航距離達15600公里，A350ULR的最大續航距離達17960公里，可以執飛超過19小時的超長距離航班，而A350ULR的首發客戶[新加坡航空已接收](../Page/新加坡航空.md "wikilink")7架A350ULR客機，用於執飛[新加坡](../Page/新加坡.md "wikilink")[樟宜國際機場直飛](../Page/樟宜國際機場.md "wikilink")[紐約](../Page/紐約.md "wikilink")[自由國際機場的超長距離航班](../Page/自由國際機場.md "wikilink")。

## 歷史

### 早期設計

當[波音於](../Page/波音.md "wikilink")2003年1月宣佈其7E7「夢幻客機」計劃時（現稱為[787](../Page/波音787.md "wikilink")），宣稱其較低的營運成本會對[空中客车A330構成嚴重威脅](../Page/空中客车A330.md "wikilink")。空中巴士最初否定此說法，認為只是對A330的攻擊，對7E7計劃不予回應。

航空公司催促[空中巴士提供一款新飛機與](../Page/空中巴士.md "wikilink")787競爭，因[波音曾承諾](../Page/波音.md "wikilink")787的耗油量比[A330低](../Page/空中巴士A330.md "wikilink")20%。起初空中巴士提議一種A330的衍生型，稱為A330-200Lite，其空氣動力設計及引擎均有所改善。然而航空公司不滿性能改善幅度太小，因此空中巴士投入40億歐元於新設計方案，並定名為A350。最初的A350十分類似A330，這是因兩款型號皆使用與A330、A340相同的機身結構，並且配上新的機翼
、引擎及水平穩定翼(horizontal stabilizer)，加上新的複合材料和製造機身方法，令A350"差不多"成為一款全新飛機。

2004年9月16日，空中巴士總裁兼首席執行官（CEO）[佛吉爾德](../Page/佛吉爾德.md "wikilink")（Noël
Forgeard）證實該公司正研究一項新計劃，但沒有給予正式名稱，亦沒有明確指出計劃中的飛機為全新設計，或是從現有型號改造而成。佛吉爾德指空中巴士會於2004年底落實有關概念，2005年初諮詢各航空公司，並於2005年底展開有關發展計劃。

2004年12月10日，空中巴士股東[歐洲航空防務太空公司](../Page/歐洲航空防務太空公司.md "wikilink")（EADS）和[英國宇航系統公司](../Page/英國宇航系統公司.md "wikilink")（BAE
Systems）批准相關計畫並正式將命名為A350。\[4\]A350將配備經改良的機翼和新的引擎，機身橫切面則與A330相同。預計A350將會有兩種型號，並於2010年投入服務，分別為A350-800及A350-900。800型的最大航程為8,800海里（16,300公里），在三級客艙編排下可搭載253人。900型的最大航程則為7,500海里（13,890公里），在三級客艙編排下可搭載300人。

為了不影響A330的市場地位（因兩者的載客量相近），A350的設計為作較長途飛行，航程由7,500至8,800海里。這正好與波音的787-9及[777-200ER客機爭奪市場](../Page/波音777.md "wikilink")。A350-900則為空中巴士首架能與[波音777-200ER](../Page/波音777#777-200ER（772）.md "wikilink")（航程及載客量方面）匹敵的雙引擎客機，外界因而對其十分感興趣。然而，空中客车的A330則須與[波音的](../Page/波音.md "wikilink")[787-3及](../Page/波音787.md "wikilink")[787-8競爭](../Page/波音787.md "wikilink")。

A350將於[A330及](../Page/空中客车A330.md "wikilink")[A340的相同設施內生產及裝嵌](../Page/空中客车A340.md "wikilink")，但亦同時加入更多[风险分擔夥伴](../Page/风险.md "wikilink")，分佈於[中国](../Page/中国.md "wikilink")、[俄罗斯及世界各地](../Page/俄罗斯.md "wikilink")。預期同類型飛機（包括貨機）在未來20年的市場需求量為3,300架，[空中客车預計能佔市場一半](../Page/空中客车.md "wikilink")。

其後[美國及](../Page/美國.md "wikilink")[歐盟就對波音公司及空中客车的政府支持發生貿易糾紛](../Page/欧洲联盟.md "wikilink")，A350計劃因而受到影響。根據一項能追溯至1992年的協議及[世界貿易組織所訂下的條例](../Page/世界貿易組織.md "wikilink")，均決定哪些政府給予飛機製造商的資助是允許的。美國強調歐盟於得到有利的情況下給予空中客车貸款違反有關條例，並向世界貿易組織提出投訴。歐盟對投訴作出回應，指波音公司將得到的可疑資助用作發展[波音787及其他飛機](../Page/波音787.md "wikilink")，以及懷疑從其軍事計劃中抽取資助。

2005年1月11日，美國及歐盟宣佈同意透過雙向會談來解決這次空中客车及波音公司的資助分歧。美國及歐盟均停止給予有關公司新的資助。然而，本來成功的談判最後失敗告終，因為[英国政府臨時批准資助空中客车](../Page/英国.md "wikilink")。\[5\]
英國政府將給予空中巴士3.79億[欧元資金](../Page/欧元.md "wikilink")，換來於英國建造A350的合成機翼的權利，因而造就近10,000工作職位。

空中巴士於2005年10月6日宣佈正式全面展開A350計劃，預算整個研發項目開支達35億歐元。A350的競爭對象為波音[787-9](../Page/波音787.md "wikilink")、[787-10](../Page/波音787.md "wikilink")、[777-200ER](../Page/波音777#777-200ER（772）.md "wikilink")。

消息公佈後，[西班牙的](../Page/西班牙.md "wikilink")[歐羅巴航空](../Page/歐羅巴航空.md "wikilink")（Air
Europa）隨即宣佈訂購12架A350-800型客機。但此後，空中巴士只是接到零星的A350訂單，遠低於空中巴士預期。

### 引來批評

空中客车的A350計劃惹來其兩大客戶的批評，即[國際租賃金融公司](../Page/國際租賃金融公司.md "wikilink")（ILFC）及[奇異金融航空服務公司](../Page/奇異金融航空服務公司.md "wikilink")（GECAS）。2006年3月28日，在數百名航空公司高層在場的情況下，國際租賃金融公司主席兼行政總裁[斯蒂文‧艾德華茲](../Page/斯蒂文‧艾德華茲.md "wikilink")（Steven
F. Udvar-Hazy）毫不留情地批評空中客车的策略，指這只是「對於波音787的補綴反應」，其後此觀點亦被奇異金融航空服務公司總裁Henry
Hubschman重申。[斯蒂文‧艾德華茲請求空中客车為業界帶來一架全新設計的飛機](../Page/斯蒂文‧艾德華茲.md "wikilink")，否則將要冒流失大量市場到波音的風險。\[6\]\[7\]
數天後，[新加坡航空](../Page/新加坡航空.md "wikilink")[行政總裁](../Page/行政總裁.md "wikilink")[周俊成亦發表相似評論](../Page/周俊成.md "wikilink")。他指既然空中客车不嫌麻煩地設計新的機翼、機尾、駕駛艙及加入先進的新材料，空中客车早應重新審視整架飛機及設計一個全新機身。\[8\]
同時，新加坡航空正檢討波音787及A350的訂購。空中巴士則回應指正考慮改善A350來迎合客戶要求。\[9\]
然後，空中巴士行政總裁[古斯塔夫·亨伯特提出並沒有任何快捷的更新安排](../Page/古斯塔夫·亨伯特.md "wikilink")，亦指出「我們的策略並非由另外一、兩個計劃帶動，而是由市場的長遠目標及我們履行諾言的能力。」"\[10\]
2006年6月14日，新加坡航空宣佈選擇訂購波音787而非A350。[阿联酋航空亦因A](../Page/阿聯酋國際航空.md "wikilink")350的設計弱點而拒絕作出訂購。
\[11\]

### 新設計：A350-XWB

[A350xwb_nose_2009B.png](https://zh.wikipedia.org/wiki/File:A350xwb_nose_2009B.png "fig:A350xwb_nose_2009B.png")
[A350_variant_sizes.svg](https://zh.wikipedia.org/wiki/File:A350_variant_sizes.svg "fig:A350_variant_sizes.svg")
[Airbus_Industrie,_F-WLXV,_Airbus_A350-1000.jpg](https://zh.wikipedia.org/wiki/File:Airbus_Industrie,_F-WLXV,_Airbus_A350-1000.jpg "fig:Airbus_Industrie,_F-WLXV,_Airbus_A350-1000.jpg")
因外界的各項批評，[空中客车於](../Page/空中客车.md "wikilink")2006年中就A350概念作了一次重大的探討研究。有推測指經修訂的飛機將名為A370。2006年7月17日，空中客车於[英国](../Page/英国.md "wikilink")[法茵堡航空展上宣佈其經重新設計的飛機將名為](../Page/范堡罗航空展.md "wikilink")**A350
XWB**（即 e**X**tra **W**ide
**B**ody，超廣體）。其機身橫切面宽度比空中巴士一直沿用的廣體客機机身（用于A300、A310、A330及A340系列）大。空中客车為了要區隔舊的A350計畫與新的A350
XWB計畫之不同，在所有的場合中都一定會稱全名A350 XWB。原本的A350計畫於2006年10月正式啟動。而A350
XWB則會將原定的時間表推遲數年，並且使研發成本增加一倍，由53億美元提升至約100億美元\[12\]。

根據空中客车發表的資料，A350
XWB雙氣泡形（[橢圓形](../Page/橢圓.md "wikilink")）的機身橫切面最大直徑為5.97公尺（A300至A340系列沿用的為5.64公尺），超越波音787的5.91米-{zh:宽度;zh-cn:宽度;zh-tw:寬度}-、接近波音777的6.19米。以座艙斷面最寬的座椅扶手高度為基準，A350
XWB的座艙內寬為5.61公尺，較主要競爭對手中的波音787（5.49公尺）多了0.12公尺。在這樣的機艙寬度下，A350
XWB的經濟艙特別設計為模組化機艙，允許兩種不同的座椅佈局，包括標準的3-3-3標準經濟艙佈局，與座椅寬度較大的2-4-2加大經濟艙佈局。

空中巴士將提供3種A350型號，其中一型號的航程將超過8,800海里，超越[波音787及原本的A](../Page/波音787.md "wikilink")350。此雙走道客機的經濟客艙能並列9個座位，而不是原本的8個座位。空中巴士打算令A350
XWB能夠與[波音787及較大型的](../Page/波音787.md "wikilink")[波音777競爭](../Page/波音777.md "wikilink")。

與原本的A350一樣，其機翼為[碳化纖維合成結構](../Page/碳纖維.md "wikilink")，機身亦會大量使用碳化纖維[合成物](../Page/合成物.md "wikilink")。以前，空中巴士曾憂慮此類結構於地面發生[碰撞時的完整性](../Page/碰撞.md "wikilink")。此外，A350
XWB將附設特大[窗戶](../Page/窗.md "wikilink")，其[駕駛艙亦會以](../Page/駕駛艙.md "wikilink")[A380為基礎](../Page/空中巴士A380.md "wikilink")。

[劳斯莱斯同意為空中巴士A](../Page/劳斯莱斯.md "wikilink")350 XWB提供一款新的特侖特（Trent）引擎（推動力達
75,000至95,000lbf）。[通用電氣已確認會提供一種適用於A](../Page/通用电气.md "wikilink")350-800的[GEnx引擎子型號](../Page/通用电气GEnx发动机.md "wikilink")，並且正與空中巴士商討為A350-900提供相同的引擎。特侖特（Trent）引擎及GEnx引擎均可互相交替。其後通用電氣曾宣佈提供由通用電氣及[普惠聯營的Engine](../Page/普惠公司.md "wikilink")
Alliance GP7200引擎，代替GEnx。\[13\] 但最終通用電器拒絕提供 GP7200
引擎與A350XWB包括原設計的GEnx均不會提供與A350XWB \[14\]
因此[劳斯莱斯](../Page/劳斯莱斯.md "wikilink") Trent XWB為A350 XWB獨家引擎。

空中客车於公佈新A350
XWB計劃後4天便收到第一張訂單，[新加坡航空宣佈訂購](../Page/新加坡航空.md "wikilink")20架A350
XWB以及20架選擇購買權。其首席執行官（CEO）[周俊成於聲明中表示](../Page/周俊成.md "wikilink")：「這是令人振奮的，空中巴士能聆聽客戶意見並得出一款經全新設計的A350」。\[15\]
新加坡航空亦同時訂購20架[波音787-10客機](../Page/波音787.md "wikilink")。\[16\]

2009年，亚洲航空订购10架A350 XWB－900和5架A350 XWB的选购权，将由AirAsia X运营。

2012年7月10日，[國泰航空與空中巴士公司](../Page/國泰航空.md "wikilink")，購買20架空中巴士A350-900型及26架A350-1000型飛機，涉及78億2000萬美元，國泰將支付訂金450萬美元。\[17\]

2013年5月13日，首架空中巴士A350 XWB客機完成塗裝，將展開一連串試飛測試\[18\]。

2013年6月14日，首架空中客车A350-900(F-WXWB)宽体飞机（序列号为MSN001）于法国当地时间6月14日（星期五）上午10时在法国城市图卢兹布拉尼亚克机场首飞，经过4小时测试安全返航完成首次飞行。

2013年6月20日，[聯合航空和空中巴士在巴黎航展上簽署協議](../Page/聯合航空.md "wikilink")，訂購35架A350-1000飛機。根據協議，這35架飛機包括：聯合航空原來訂購的25架A350-900飛機轉換為更大的A350-1000飛機，以及追加訂購的10架A350-1000飛機。

2013年10月，日本航空在宣布訂購33架A350-900、13架A350-1000和25架A350的選購權。

2014年6月12日，阿联酋航空取消空中巴士70架A350長途客機的訂單，其中包括50架A350-900和20架更大的A350-1000的訂單，總值達216億美元，但在2019年2月14日再次確認訂購40架A350-900客機

2014年12月22日，空中巴士交付了首架A350-900客機予[卡塔爾航空](../Page/卡塔爾航空.md "wikilink")，它同時亦是空中巴士A350-900的啟發客戶。

2015年6月30日， 空中巴士交付了首架A350-900予[越南航空](../Page/越南航空.md "wikilink")
2015年10月7日， 空中巴士交付了首架A350-900予[芬蘭航空](../Page/芬蘭航空.md "wikilink")。

2016年3月3日， 空中巴士交付了首架A350-900予[新加坡航空](../Page/新加坡航空.md "wikilink")

2016年5月29日， 空中巴士交付了首架A350-900予[國泰航空](../Page/國泰航空.md "wikilink")\[19\]

2016年6月28日， 空中巴士交付了首架A350-900予[埃塞俄比亞航空](../Page/埃塞俄比亞航空.md "wikilink")

2016年8月30日， 空中巴士交付了首架A350-900予[泰國國際航空](../Page/泰國國際航空.md "wikilink")

2016年9月30日，
空中巴士交付了首架A350-900予[中華航空的首架A](../Page/中華航空.md "wikilink")350民航機。

2016年11月24日，经过4小时18分的测试飞行，生产序列号为MSN059的首架A350-1000测试飞机于法国当地时间下午三点成功返回图卢兹布拉尼亚克机场，顺利完成首飞。

2016年12月21日，
空中巴士交付了首架A350-900予[漢莎航空的首架A](../Page/漢莎航空.md "wikilink")350民航機。\[20\]

2017年2月28日，空中巴士交付了首架A350-900予[加勒比海航空](../Page/加勒比海航空.md "wikilink")

2017年4月24日，空中巴士交付了首架A350-900予[韓亞航空](../Page/韓亞航空.md "wikilink")

2017年7月24日，空中巴士交付了首架A350-900予[達美航空](../Page/達美航空.md "wikilink")

2017年7月25日，空中巴士交付第100架A350-900予[中華航空](../Page/中華航空.md "wikilink")

2017年9月1日，空中巴士交付了首架A350-900予[香港航空](../Page/香港航空.md "wikilink")

2017年11月30日，空中巴士交付了首架A350-900予[馬來西亞國際航空](../Page/馬來西亞國際航空.md "wikilink")\[21\]，並在12月22日交付了第二架A350民航機（此機有著馬來西亞國旗‘輝煌條紋’的彩繪）。\[22\]

2018年2月21日，首架空中巴士A350-1000民航機交付予[卡塔尔航空](../Page/卡塔尔航空.md "wikilink")。

2018年6月19日，第二架空中巴士A350-1000民航機交付給[國泰航空](../Page/國泰航空.md "wikilink")

2018年8月，空中巴士交付了首架A350-900予[中国国际航空及](../Page/中国国际航空.md "wikilink")[四川航空](../Page/四川航空.md "wikilink")

2018年9月29日，空中巴士交付了首架A350-900予[海南航空](../Page/海南航空.md "wikilink")

2018年12月4日﹐空中巴士交付了首架A350-900予[中國東方航空](../Page/中國東方航空.md "wikilink")\[23\]

## 型號

A350WXB的細部型號於2006年啟動，計畫於2013年投入運行，但在2011年[巴黎航展上](../Page/巴黎航展.md "wikilink")，[空中巴士將A](../Page/空中巴士.md "wikilink")350-1000的投入日期延到2017年中期。2012年7月，再將A350-900的投入日期延到2014下半年，-800型的投入日期延到2016年年中。

  - A350-800（未生產）：設計規格為長，三級艙等配置276個座位，航程
    ，最大起飛重量（MTOW）。\[24\]主要竞争对手为[波音787系列](../Page/波音787.md "wikilink")，也可用于替代[A330系列](../Page/A330.md "wikilink")，原預計於2014年投入服務。由[勞斯萊斯新一代遄达](../Page/勞斯萊斯股份有限公司.md "wikilink")（Next
    Generation Trent）[Trent
    XWB-75引擎推動](../Page/罗尔斯·罗伊斯遄达XWB.md "wikilink")。2008年中期的訂單為182架，但自2010年來，隨著更大的900型推出，許多客戶紛紛將訂單轉為900型，曾經只有[韓亞航空為](../Page/韓亞航空.md "wikilink")-800型的唯一客戶（12架），但現在已全部取消，故目前A350-800處於與A330-800neo一樣的無限期停滯狀態。

<!-- end list -->

  - A350-900：三級艙等可配置314人，最大可搭載440人，航程為15,000公里（8100海里），主要競爭機型為波音777-200ER/LR和787-10，可替換[A340-300和](../Page/A340-300.md "wikilink")[A340-500](../Page/A340.md "wikilink")，於2014年投入服務。由劳斯莱斯新一代遄达Trent
    XWB-84引擎推動。
      - A350-900ULR：為超長航程（Ultra-Long
        Range）的縮寫，增加燃油容量以擴增航程，可實現達19小時的航程，並將航程擴大為17,960公里。2018年投入服務。\[25\]和A350-900相比，A350-900ULR拥有更大的最大起飞重量，供油系統也重新設計過，但執飛超長航線時需要實施載荷限制，以免燃油消耗過快\[26\]。该机型启动客户为[新加坡航空](../Page/新加坡航空.md "wikilink")，新加坡航空借此重新開通曾使用[A340-500执飞的新加坡](../Page/A340.md "wikilink")－紐瓦克的不经停航线（[新加坡航空21号班机](../Page/新加坡航空21号班机.md "wikilink")）。
      - ACJ350：廣體公務機，燃油容量從ULR再擴大，航程增至20,000公里。
      - A350
        Regional：计划中的區域短程型號，拥有較低的起飛重量以適應較密集的起降。\[27\]自2013年以來，沒有關於此型號的進一步公告。
      - A350-900F：貨機型號，尚未正式推出（[波音777F的競爭對手](../Page/波音777#777貨機.md "wikilink")）。

<!-- end list -->

  - A350-1000：為[A350系列中最大的型號](../Page/A350.md "wikilink")，三級艙等可配置350人，最大可搭載475人。航程為14,800-15,600公里。可替代[A340-600](../Page/A340.md "wikilink")，競爭機型為[波音777-300ER和](../Page/波音777-300ER.md "wikilink")[777-9X](../Page/777X.md "wikilink")。由劳斯莱斯新一代遄达（Next
    Generation Trent）Trent XWB-97引擎推動，主起落架配置接近777的三排輪式，達到12輪。

A350 XWB的巡航速度大約為0.85馬赫，與對手波音787相若。然而空中巴士號稱A350的維修成本比787低10%。A350
XWB將配備與[A380類似的駕駛艙](../Page/空中巴士A380.md "wikilink"),維持機種操作的一貫性，減少訓練成本。空中巴士亦聲稱A350
XWB-900的每座位耗油量比787-10低7%。A350
XWB將配備更大的[窗戶](../Page/窗.md "wikilink")，機艙內氣壓會被加壓至等同6,000呎或更低的高度，空氣濕度則最少為20%。

## 訂單

[QTR_A350_A7-ALA_23dec14_LFBO-2.jpg](https://zh.wikipedia.org/wiki/File:QTR_A350_A7-ALA_23dec14_LFBO-2.jpg "fig:QTR_A350_A7-ALA_23dec14_LFBO-2.jpg")
[China_Airlines_A350-941_B-18901_Landing_in_TPE_2016-10-01.jpg](https://zh.wikipedia.org/wiki/File:China_Airlines_A350-941_B-18901_Landing_in_TPE_2016-10-01.jpg "fig:China_Airlines_A350-941_B-18901_Landing_in_TPE_2016-10-01.jpg")首架A350於2016年10月交付營運\]\]
[B-LXG@HKG_(20181205101644).jpg](https://zh.wikipedia.org/wiki/File:B-LXG@HKG_\(20181205101644\).jpg "fig:B-LXG@HKG_(20181205101644).jpg")A350-1000XWB\]\]
[B-LGH@HKG_(20181201122543).jpg](https://zh.wikipedia.org/wiki/File:B-LGH@HKG_\(20181201122543\).jpg "fig:B-LGH@HKG_(20181201122543).jpg")的A350-941客機\]\]

### 機型訂單

|           |         |         |
| --------- | ------- | ------- |
| 機型        | 訂單      | 交付      |
| A350-900  | 724     | 226     |
| A350-1000 | 170     | **14**  |
| **總數**    | **894** | **240** |
|           |         |         |

空中巴士A350 XWB逐年訂單與交付情形\[28\]

|        |      |           |           |      |      |      |      |      |      |      |      |      |      |      |         |
| ------ | ---- | --------- | --------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ------- |
|        | 2006 | 2007      | 2008      | 2009 | 2010 | 2011 | 2012 | 2013 | 2014 | 2015 | 2016 | 2017 | 2018 | 2019 | 總數      |
| **訂單** | 2    | 292\[29\] | 163\[30\] | 51   | 78   | \-31 | 27   | 230  | \-32 | \-3  | 41   | 30   | 40   | 0    | **894** |
| **交付** | –    | –         | –         | –    | –    | –    | –    | –    | 1    | 14   | 49   | 78   | 93   | 5    | **240** |
|        |      |           |           |      |      |      |      |      |      |      |      |      |      |      |         |

空中巴士A350 XWB逐年訂單與交付情形

\[31\]

</noinclude>

### 客戶訂單

<table>
<thead>
<tr class="header">
<th><p>簽約日期</p></th>
<th><p>客戶名稱</p></th>
<th><p>交機時間</p></th>
<th><p>A350-800</p></th>
<th><p>A350-900</p></th>
<th><p>A350-1000</p></th>
<th><p>總計</p></th>
<th><p>交付數</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>AerCap / <a href="../Page/國際租賃財務公司.md" title="wikilink">ILFC</a></p></td>
<td><p>2015</p></td>
<td></td>
<td><p><strong>20</strong></p></td>
<td></td>
<td><p><strong>20</strong></p></td>
<td><p><strong>16</strong></p></td>
<td><p>原始訂購6架A350-800與14架A350-900[32][33] 於2014年1月全數轉為A350-900。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/愛爾蘭航空.md" title="wikilink">愛爾蘭航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p>9</p></td>
<td></td>
<td><p><strong>9</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/俄羅斯航空.md" title="wikilink">俄羅斯航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/Lei&#39;s_Airways_Co.,_Ltd.md" title="wikilink">Lei's Airways Co., Ltd</a></p></td>
<td><p>2023</p></td>
<td></td>
<td><p><strong>7</strong></p></td>
<td><p><strong>1</strong></p></td>
<td><p><strong>8</strong></p></td>
<td></td>
<td><p>A350-900四張為意向訂單</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/泛非航空.md" title="wikilink">泛非航空</a></p></td>
<td></td>
<td></td>
<td><p>10</p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td></td>
<td><p>原先訂購6架A350-800[34] ，於2012年10月1日轉為10架A350-900[35]。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/加勒比海航空.md" title="wikilink">加勒比海航空</a></p></td>
<td><p>2018</p></td>
<td></td>
<td><p><strong>4</strong></p></td>
<td><p><strong>3</strong></p></td>
<td><p><strong>7</strong></p></td>
<td><p><strong>2</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/中國國際航空.md" title="wikilink">中國國際航空</a></p></td>
<td><p>2018</p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td><p><strong>8</strong></p></td>
<td><p>其中B-1083采用“多彩世园号”彩绘。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/法國航空.md" title="wikilink">法國航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>21</strong></p></td>
<td></td>
<td><p><strong>21</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Air Lease Corporation</p></td>
<td><p>2018</p></td>
<td></td>
<td><p><strong>20</strong></p></td>
<td><p><strong>9</strong></p></td>
<td><p><strong>29</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/毛里裘斯航空.md" title="wikilink">毛里裘斯航空</a></p></td>
<td><p>2017[36]</p></td>
<td></td>
<td><p><strong>6</strong></p></td>
<td></td>
<td><p><strong>6</strong></p></td>
<td><p><strong>2</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/亞洲航空长途.md" title="wikilink">亞洲航空长途</a></p></td>
<td><p>2020</p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>align="left|Aircraft Purchase Fleet</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2014年3月取消12架A350-800[37]。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>ALAFCO</p></td>
<td></td>
<td></td>
<td><p><strong>12</strong></p></td>
<td></td>
<td><p><strong>12</strong></p></td>
<td><p><strong>5</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/美國航空.md" title="wikilink">美國航空</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2013年12月27日將18架A350-800轉換為A350-900[38][39][40]。 2016年7月22日宣布22架將延後至2018年交付[41] 。 2018年4月7日美國航空決定取消訂單，並轉購B787系列。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/韓亞航空.md" title="wikilink">韓亞航空</a></p></td>
<td><p>2017</p></td>
<td></td>
<td><p><strong>21</strong></p></td>
<td><p><strong>9</strong></p></td>
<td><p><strong>30</strong></p></td>
<td><p><strong>6</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/AWAS.md" title="wikilink">AWAS</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2014年轉移訂單至A350-900[42]，2016年5月取消兩架訂單。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/曼谷航空.md" title="wikilink">曼谷航空</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>訂購4架A350-800[43]，於2011年9月6日取消[44]。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/英國航空.md" title="wikilink">英國航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td></td>
<td><p><strong>18</strong></p></td>
<td><p><strong>18</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/國泰航空.md" title="wikilink">國泰航空</a></p></td>
<td><p>2016</p></td>
<td></td>
<td><p>'''26 '''</p></td>
<td><p><strong>20</strong></p></td>
<td><p><strong>46</strong></p></td>
<td><p>'''31 '''</p></td>
<td><p>原始訂購30架A350-900[45][46]， 2012年1月20日追加六架[47]，2012年8月8日將16架轉為A350-1000且另外加定10架[48][49]。2017年9月13日將6架A350-1000轉為A350-900</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/中華航空.md" title="wikilink">中華航空</a></p></td>
<td><p>2016</p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td><p><strong>14</strong></p></td>
<td><p>2018年10月交付完畢，並有6架選擇權</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/中國東方航空.md" title="wikilink">中國東方航空</a></p></td>
<td><p>2018</p></td>
<td></td>
<td><p><strong>20</strong></p></td>
<td></td>
<td><p><strong>20</strong></p></td>
<td><p><strong>4</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/中國南方航空.md" title="wikilink">中國南方航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>20</strong></p></td>
<td></td>
<td><p><strong>20</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/CIT集團.md" title="wikilink">CIT集團</a></p></td>
<td><p>2016</p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td><p><strong>4</strong></p></td>
<td><p>原先訂購5架A350-800及2架A350-900[50][51]，2011年7月1日取消所有A350-800並加定3架A350-900[52]，2013年1月3日再追加採購10架A350-900[53]。然而2013年11月30日取消一架A350-900[54]。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/達美航空.md" title="wikilink">達美航空</a></p></td>
<td><p>2017</p></td>
<td></td>
<td><p><strong>25</strong></p></td>
<td></td>
<td><p><strong>25</strong></p></td>
<td><p><strong>11</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿聯酋航空.md" title="wikilink">阿聯酋航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>30</strong></p></td>
<td></td>
<td><p><strong>30</strong></p></td>
<td></td>
<td><p>原本50架A350-900及20架A350-1000，之後全數取消，全改訂購A380，後來和空中巴士談判後，取消50架A380後，2019年2月14日確認訂購30架A350-900客機(隨時可能轉單A350-1000XWB)[55][56]。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/埃塞俄比亞航空.md" title="wikilink">埃塞俄比亞航空</a></p></td>
<td><p>2017</p></td>
<td></td>
<td><p><strong>24</strong></p></td>
<td></td>
<td><p><strong>24</strong></p></td>
<td><p><strong>10</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿提哈德航空.md" title="wikilink">阿提哈德航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>40</strong></p></td>
<td><p><strong>22</strong></p></td>
<td><p><strong>62</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/芬蘭航空.md" title="wikilink">芬蘭航空</a></p></td>
<td><p>2015</p></td>
<td></td>
<td><p><strong>19</strong></p></td>
<td></td>
<td><p><strong>19</strong></p></td>
<td><p><strong>11</strong></p></td>
<td><p>原始訂購11架A350-900[57]，2014年12月3日將8架選購權轉為確認訂單[58]。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>公務、私人機</p></td>
<td></td>
<td></td>
<td><p><strong>1</strong></p></td>
<td></td>
<td><p><strong>1</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Groupe Dubreuil</p></td>
<td><p>2016</p></td>
<td></td>
<td><p><strong>1</strong></p></td>
<td></td>
<td><p><strong>1</strong></p></td>
<td><p><strong>1</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/夏威夷航空.md" title="wikilink">夏威夷航空</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>原始訂購6架A350-800[59][60]，後改定6架<a href="../Page/空中客车A330neo.md" title="wikilink">A330-800neo取代</a>[61]。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/香港航空.md" title="wikilink">香港航空</a></p></td>
<td><p>2017</p></td>
<td></td>
<td><p><strong>12</strong></p></td>
<td></td>
<td><p><strong>12</strong></p></td>
<td><p><strong>7</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/西班牙國家航空.md" title="wikilink">西班牙國家航空</a></p></td>
<td><p>2018</p></td>
<td></td>
<td><p><strong>16</strong></p></td>
<td></td>
<td><p><strong>16</strong></p></td>
<td><p><strong>1</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/伊朗航空.md" title="wikilink">伊朗航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td></td>
<td><p><strong>16</strong></p></td>
<td><p><strong>16</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/日本航空.md" title="wikilink">日本航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>31</strong></p></td>
<td><p><strong>13</strong></p></td>
<td><p><strong>44</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/翠鳥航空.md" title="wikilink">翠鳥航空</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2013年12月31日取消4架A350-800[62][63]。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/荷蘭皇家航空.md" title="wikilink">荷蘭皇家航空</a></p></td>
<td><p>2021</p></td>
<td></td>
<td><p><strong>7</strong></p></td>
<td></td>
<td><p><strong>7</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/科威特航空.md" title="wikilink">科威特航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>5</strong></p></td>
<td></td>
<td><p><strong>5</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/南美航空集團.md" title="wikilink">南美航空集團</a></p></td>
<td><p>2015</p></td>
<td></td>
<td><p><strong>15</strong></p></td>
<td><p><strong>12</strong></p></td>
<td><p><strong>27</strong></p></td>
<td><p><strong>7</strong></p></td>
<td><p>2015年轉換其中6架A350-900至A350-1000[64]。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/利比亞航空.md" title="wikilink">利比亞航空</a></p></td>
<td></td>
<td></td>
<td><p><strong>6</strong></p></td>
<td></td>
<td><p><strong>6</strong></p></td>
<td></td>
<td><p>原始訂購4架A350-800[65]，於2014年1月轉為6架A350-900。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/漢莎航空.md" title="wikilink">漢莎航空</a></p></td>
<td><p>2016</p></td>
<td></td>
<td><p><strong>45</strong></p></td>
<td></td>
<td><p><strong>45</strong></p></td>
<td><p><strong>11</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/菲律賓航空.md" title="wikilink">菲律賓航空</a></p></td>
<td><p>2018</p></td>
<td></td>
<td><p><strong>6</strong></p></td>
<td></td>
<td><p><strong>6</strong></p></td>
<td><p><strong>1</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/卡達航空.md" title="wikilink">卡達航空</a></p></td>
<td><p>2014</p></td>
<td></td>
<td><p><strong>41</strong></p></td>
<td><p><strong>37</strong></p></td>
<td><p><strong>78</strong></p></td>
<td><p><strong>30</strong></p></td>
<td><p>首次訂購時包含20架A350-800[66][67]，於2012年12月3日轉換為3架A350-900及17架A350-1000[68]。卡達航空同時為A350-900及A350-1000啟動客戶，並於2015年1月15日開始首條A350-900商業航班服務。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/北歐航空.md" title="wikilink">北歐航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>8</strong></p></td>
<td></td>
<td><p><strong>8</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/新加坡航空.md" title="wikilink">新加坡航空</a></p></td>
<td><p>2016</p></td>
<td></td>
<td><p><strong>67</strong></p></td>
<td></td>
<td><p><strong>67</strong></p></td>
<td><p><strong>32</strong></p></td>
<td><p>首次訂購20架A350[69]，2012年12月13日追加20架[70]，2013年5月30日再追加30架[71][72]。2017年7月取消7架[73]。2015年10月訂購額外4架[74]。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/四川航空.md" title="wikilink">四川航空</a></p></td>
<td><p>2018</p></td>
<td></td>
<td><p><strong>16</strong></p></td>
<td></td>
<td><p><strong>16</strong></p></td>
<td><p><strong>3</strong></p></td>
<td><p>斯里兰卡航空取消全部四架订单后将其订单全部转交给四川航空；后四川航空加购10架;</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Synergy Aerospace</p></td>
<td><p>2020</p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td></td>
<td><p>給巴西<a href="../Page/哥倫比亞航空.md" title="wikilink">哥倫比亞航空</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/TAP葡萄牙航空.md" title="wikilink">TAP葡萄牙航空</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>原始訂購12架A350-900預計2017投入服務，2015年11月13日改定14架<a href="../Page/空中客车A330neo.md" title="wikilink">A330-900neo取代</a><ref>{{Cite web |url=<a href="http://www.airbus.com/newsevents/news-events-single/detail/tap-portugal-orders-14-a330-900neo-and-39-a320neo-family-aircraft/">http://www.airbus.com/newsevents/news-events-single/detail/tap-portugal-orders-14-a330-900neo-and-39-a320neo-family-aircraft/</a> |title=TAP Portugal orders 14 A330-900neo and 39 A320neo Family aircraft</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/泰國國際航空.md" title="wikilink">泰國國際航空</a></p></td>
<td><p>2016</p></td>
<td></td>
<td><p><strong>12</strong></p></td>
<td></td>
<td><p><strong>12</strong></p></td>
<td><p><strong>12</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/突尼斯航空.md" title="wikilink">突尼斯航空</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2013年5月31日取消3架A350-800[75]<ref>{{Cite web |url=<a href="http://www.flightglobal.com/news/articles/tunisair-appears-to-cancel-a350-800-order-386812/">http://www.flightglobal.com/news/articles/tunisair-appears-to-cancel-a350-800-order-386812/</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>匿名客戶</p></td>
<td></td>
<td></td>
<td></td>
<td><p><strong>1</strong></p></td>
<td><p><strong>1</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/聯合航空.md" title="wikilink">聯合航空</a></p></td>
<td><p>2022</p></td>
<td></td>
<td><p><strong>45</strong></p></td>
<td></td>
<td><p><strong>45</strong></p></td>
<td></td>
<td><p>原始訂購25架A350-900，2013年6月20日全數轉為A350-1000且另外加定10架。2017年9月7日轉為45架A350-900。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/越南航空.md" title="wikilink">越南航空</a></p></td>
<td><p>2016</p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td><p><strong>12</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/維珍航空.md" title="wikilink">維珍航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td></td>
<td><p><strong>12</strong></p></td>
<td><p><strong>12</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/也門航空.md" title="wikilink">也門航空</a></p></td>
<td><p>2019</p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td></td>
<td><p><strong>10</strong></p></td>
<td></td>
<td><p>[76][77][78]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/星宇航空.md" title="wikilink">星宇航空</a></p></td>
<td><p>2021年底</p></td>
<td></td>
<td><p><strong>5</strong></p></td>
<td><p><strong>12</strong></p></td>
<td><p><strong>17</strong></p></td>
<td></td>
<td><p>星宇航空董事長<a href="../Page/張國煒.md" title="wikilink">張國煒與空中巴士集團於英國</a>2019年3月19日在<a href="../Page/法恩堡航空展.md" title="wikilink">法恩堡航空展簽訂</a>17架包含5架A350-900、12架A350-1000，A350-900預計2021年底開始交機，而A350-1000預計在2022年第三季開始交機，17架A350預計在2024年底全數交付完畢。[79]</p></td>
</tr>
<tr class="odd">
<td><p>總計</p></td>
<td></td>
<td></td>
<td><p>—</p></td>
<td><p>686</p></td>
<td><p>188</p></td>
<td><p>854</p></td>
<td><p>240</p></td>
<td></td>
</tr>
</tbody>
</table>

\[80\]

1.  A350原始訂單已不存在，以上訂單為XWB型號。
2.  XWB型號目前只有[勞斯萊斯引擎可供選擇](../Page/勞斯萊斯股份有限公司.md "wikilink")。

## 技術

A350與[A330比較](../Page/空中巴士A330.md "wikilink")，A350配備全新機艙、[機翼](../Page/機翼.md "wikilink")、機尾、[起落架及各項新系統](../Page/起落架.md "wikilink")。一些原為[A380發展出來的技術均可在A](../Page/空中巴士A380.md "wikilink")350上找到，其中一項為大量使用複合材料。總括來說，整架A350客機約有60%會使用先進材料建造，[複合物料佔](../Page/複合材料.md "wikilink")52%，鋁—鋰[合金佔](../Page/合金.md "wikilink")20%，[鋼鐵佔](../Page/钢.md "wikilink")7%，[铝佔](../Page/铝.md "wikilink")7%，[钛佔](../Page/钛.md "wikilink")14%，其餘的為其他物料。A350亦配備新的複合物料機翼及機身，主要使用鋁-鋰合金建造。使用大量複合材料及鋁—鋰合金能有效減少飛機重量達8,000公斤（17,600磅）。空中巴士在2008年十月的內部目標是要凍結設計比競爭對手的飛機減少10%的維修成本和14%每座的净重。

空中巴士已經與[BMW簽訂有關發展飛機內部的協議](../Page/BMW.md "wikilink")。\[81\]

空中巴士原計劃使用原為[波音787開發的](../Page/波音787.md "wikilink")[渦輪發動機技術](../Page/渦輪發動機.md "wikilink")，例如-{zh:[罗尔斯·罗伊斯](../Page/罗尔斯·罗伊斯.md "wikilink");zh-cn:[罗尔斯·罗伊斯](../Page/罗尔斯·罗伊斯.md "wikilink");zh-tw:[勞斯萊斯](../Page/勞斯萊斯.md "wikilink")}-[特倫特1000及](../Page/劳斯莱斯瑞达1000.md "wikilink")[通用電气](../Page/通用電气.md "wikilink")[GEnx](../Page/通用電气GEnx发动机.md "wikilink")。GEnx引擎可按其推動力劃分為4個級別，由63,000 lbf
至 75,000 lbf（280 kN 至 334 kN）不等。但通用電气至今还没有确认为空中巴士A350 XWB提供引擎。

\-{zh:[罗尔斯·罗伊斯](../Page/罗尔斯·罗伊斯.md "wikilink");zh-cn:[罗尔斯·罗伊斯](../Page/罗尔斯·罗伊斯.md "wikilink");zh-tw:[勞斯萊斯](../Page/勞斯萊斯.md "wikilink")}-[特倫特XWB是當前空中巴士A](../Page/Trent_XWB.md "wikilink")350
XWB唯一的引擎选擇。特倫特XWB的静态推力在330－410kN之间，将采用該公司的3轴技术，2013年6月14日，首次搭载遄达XWB发动机的A350进行了飞行测试\[82\]。

更新舊款飛機來應付新推出的飛機早已有先例，如[空中巴士A320的進化版](../Page/空中巴士A320.md "wikilink")[空中巴士A320neo](../Page/空中巴士A320neo.md "wikilink")；而波音為了與之抗衡，於是更新旗下的[波音737系列](../Page/波音737.md "wikilink")，導致第四代737——[波音737MAX出現](../Page/波音737MAX.md "wikilink")。而A350
XWB不同於上述，而是為了應對另一全新機種[波音787而重新開發](../Page/波音787.md "wikilink")，並非改良現有飛機。

## 性能規格

<table>
<thead>
<tr class="header">
<th><p>機型</p></th>
<th><p>A350-800</p></th>
<th><p>A350-900</p></th>
<th><p>A350-1000</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>機師數</p></td>
<td><p>2</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>座位數</p></td>
<td><p>270 （3級）<br />
312 （2級）<br />
440 （最大）</p></td>
<td><p>314 （3級）<br />
366 （2級）<br />
440 （最大）</p></td>
<td><p>350 （3級）<br />
412 （2級）<br />
475 （最大）</p></td>
</tr>
<tr class="odd">
<td><p>長度</p></td>
<td><p>60.5 m （198 ft 6 in）</p></td>
<td><p>66.8 m （219 ft 3 in）</p></td>
<td><p>73.8 m （242 ft 3 in）</p></td>
</tr>
<tr class="even">
<td><p>翼展</p></td>
<td><p>64.75 m （212.4 ft 10 in）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>機翼面積</p></td>
<td><p>443.0 m² （4,740 ft²）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>後掠翼</p></td>
<td><p>35° 4</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高度</p></td>
<td><p>16.9 m （55 ft 5 in）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>機身直徑</p></td>
<td><p>5.94 m （19 ft 5 in） [83]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>機艙寬度</p></td>
<td><p>5.59 m （18 ft 5） in [84]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>貨運載酬</p></td>
<td><p>26 LD3</p></td>
<td><p>36 LD3</p></td>
<td><p>44 LD3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/最大起飛重量.md" title="wikilink">最大起飛重量</a></p></td>
<td><p>248 t（<a href="../Page/英噸.md" title="wikilink">噸</a>） （540,133 lb）</p></td>
<td><p>268 t （584,225 lb）</p></td>
<td><p>308 t （650,364 lb）</p></td>
</tr>
<tr class="even">
<td><p>巡航速率</p></td>
<td><p>0.85 mach（<a href="../Page/马赫.md" title="wikilink">马赫</a>）（903 km/h，561 mph（<a href="../Page/英里.md" title="wikilink">英里</a>），487 knots（<a href="../Page/節.md" title="wikilink">節</a>），於40,000英尺高度）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最大巡航速度</p></td>
<td><p>0.89 mach （945 km/h, 587 mph, 510 knots, 於40,000英尺高度）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>滿載航距</p></td>
<td><p>14,816-15,400 km （8,000-8,315 nmi）</p></td>
<td><p>15,000 km （8,099nmi）<br />
 (-900ULR)[85]</p></td>
<td><p>14,800 km （7,991 nmi)-15,600 km</p></td>
</tr>
<tr class="odd">
<td><p>最大燃油容量</p></td>
<td><p>150,000 L （39,626 US gal）</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>實用昇限</p></td>
<td><p>43,000 ft （13,100 m）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>發動機 （╳2）</p></td>
<td><p><a href="../Page/罗尔斯·罗伊斯遄达XWB.md" title="wikilink">勞斯萊斯（Rolls-Royce Trent） Trent XWB</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>發動機最大推力</p></td>
<td><p>337 kN （74,861 lbf）</p></td>
<td><p>374 kN （87,001 lbf）</p></td>
<td><p>432 kN （94,869 lbf）</p></td>
</tr>
</tbody>
</table>

## 缺點

根據歐洲民航局發出的適航指令，與A350飛行操作有關的液壓系統中，油箱中的液壓泵易過熱，溫度超過原設計值，並出現警告燈，如果在油箱油量減少時，會有點燃油箱中油氣的風險，因此只要任一組液壓泵有過熱警報就立即停飛。\[86\]

## 參考文獻

## 相關條目

### 空中客车产品线

  - [A300](../Page/空中客车A300.md "wikilink") -
    [A310](../Page/空中客车A310.md "wikilink") -
    [A318](../Page/空中客车A320#A318.md "wikilink") -
    [A319](../Page/空中客车A320#A319.md "wikilink") -
    [A320](../Page/空中客车A320#A320.md "wikilink") -
    [A321](../Page/空中客车A320#A321.md "wikilink") -
    [A330](../Page/空中客車A330.md "wikilink") -
    [A340](../Page/空中客车A340.md "wikilink") -A350 -
    [A380](../Page/空中客车A380.md "wikilink")

### 類似競爭對手

  - [波音777-200ER/-300ER](../Page/波音777.md "wikilink")
  - [波音787-8/-9/-10](../Page/波音787.md "wikilink")
  - [波音Y3](../Page/波音Y3.md "wikilink")（波音777-8X/-9X）

[Category:空中客车](../Category/空中客车.md "wikilink")
[Category:幹線客機](../Category/幹線客機.md "wikilink")
[Category:雙發噴射機](../Category/雙發噴射機.md "wikilink")

1.

2.

3.
4.  "[Airbus to launch Boeing 7E7
    rival](http://news.bbc.co.uk/2/hi/business/4085631.stm)." [BBC
    News](../Page/BBC_News.md "wikilink"). 2004年12月10日.

5.  "[Airbus claims agreement in principle for A350
    funding](http://www.flightinternational.com/Articles/2005/09/16/Navigation/177/201559/Airbus+claims+agreement+in+principle+for+A350+funding.html)."
    *[Flight
    International](../Page/Flight_International.md "wikilink")*.
    2005年9月16日.

6.  "[Airplane kingpins tell Airbus: Overhaul
    A350](http://seattletimes.nwsource.com/html/boeingaerospace/2002896362_boeing29.html)."
    Gates, D. *[Seattle
    Post-Intelligencer](../Page/Seattle_Post-Intelligencer.md "wikilink")*.
    [29 March](../Page/29_March.md "wikilink") 2006.

7.  "[Redesigning the A350: Airbus’ tough
    choice](http://www.leeham.net/filelib/ScottsColumn040406.pdf) ."
    Hamilton, S., Leeham Company.

8.  "[Singapore Airlines Says Airbus Needs to Make A350
    Improvements](http://online.wsj.com/article/SB114442937150720186.html)."
    Michaels, D. and Lunsford, J. L. *[The Wall Street
    Journal](../Page/The_Wall_Street_Journal.md "wikilink")*. [7
    April](../Page/7_April.md "wikilink") 2006.

9.  "[Criticism prompts Airbus to study options, CEO
    says](http://seattletimes.nwsource.com/html/businesstechnology/2002923790_airbus11.html)."
    Rothman, A. [Bloomberg News](../Page/Bloomberg_News.md "wikilink").
    [11 April](../Page/11_April.md "wikilink") 2006.

10. "[Airbus Considering Improvements to
    A350](http://news.moneycentral.msn.com/provider/providerarticle.asp?feed=AP&Date=20060410&ID=5603964)
    ." [Associated Press](../Page/Associated_Press.md "wikilink"). [10
    April](../Page/10_April.md "wikilink") 2006.

11. "[Pressure mounts following attack by
    Emirates](http://news.independent.co.uk/business/news/article1090115.ece)."
    Brierley, D. [The
    Independent](../Page/The_Independent.md "wikilink"). [18
    June](../Page/18_June.md "wikilink") 2006.

12. "[Airbus Unveils New A350 to Take on
    Boeing's 787](http://www.bloomberg.com/apps/news?pid=20601085&sid=aW1OpAimXpY8&refer=europe)."
    www.bloomberg.com. [17 July](../Page/17_July.md "wikilink") 2006.

13. ["GE/Pratt & Whitney Engine Alliance makes bid to power revamped
    Airbus
    A350"](http://www.flightglobal.com/Articles/2006/05/08/Navigation/177/206438/GEPratt++Whitney+Engine+Alliance+makes+bid+to+power+revamped+Airbus+A350.html)
    *[Flight International](../Page/Flight_International.md "wikilink")*
    8 May 2006

14.

15. [Singapore Airlines orders 20 Airbus A350 XWB-900s and 9 Airbus
    A380s](http://business-times.asiaone.com/mnt/html/pdf/SIA_july21.pdf)


16. [Singapore Airlines 787 Dreamliner
    Announcement](http://boeing.com/news/releases/2006/q2/060614b_nr.html)
     Press release

17. [Cathay Pacific selects
    A350-1000](http://www.airbus.com/presscentre/pressreleases/press-release-detail/detail/cathay-pacific-selects-a350-1000/)

18. [Airbus rolls out new A350
    XWB](http://edition.cnn.com/2013/05/14/travel/airbus-a350-paint)

19.

20.

21.

22.

23.

24.

25. ["Airbus launches new Ultra-Long Range version of the
    A350-900"](http://www.airbus.com/presscentre/pressreleases/press-release-detail/detail/airbus-launches-new-ultra-long-range-version-of-the-a350-900/)

26.

27. ["Singapore launches lower-weight 'regional'
    A350"](http://www.flightglobal.com/news/articles/singapore-launches-lower-weight-39regional39-a350-388540/)

28.
29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.
55. <https://www.upmedia.mg/news_info.php?SerialNo=57587>

56.

57.

58. <http://news.cision.com/finnair/r/finnair-firms-up-orders-for-eight-additional-a350-aircraft,c9690564>

59.

60.

61.

62.

63.

64.
65.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.
81. "[BMW to design parts of Airbus A350 model, reportedly aircraft
    cabins](http://www.forbes.com/markets/feeds/afx/2006/01/05/afx2430232.html)."
    *[Forbes](../Page/Forbes.md "wikilink")*. [5
    January](../Page/5_January.md "wikilink") 2006.

82. <http://www.rolls-royce.com/news/press_releases/2013/14062013_engine_powers_first_flight.jsp>

83. [A350
    Specifications](http://www.airbus.com/en/aircraftfamilies/a350/a350-800/specifications.html)

84.
85.

86.