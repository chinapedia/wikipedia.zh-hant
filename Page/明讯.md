**明讯**（[英文](../Page/英文.md "wikilink")：Maxis，），是[马来西亚的](../Page/马来西亚.md "wikilink")[移动电话服务業者](../Page/移动电话.md "wikilink")，業務範圍涵蓋固網通訊、資料通訊及行動通訊等。同時也是第一個在馬來西亞提供[4G](../Page/4G.md "wikilink")
[LTE的電信公司](../Page/LTE.md "wikilink")。

在手機號碼上，使用012、017、014-2和011-2作為開頭。明訊在2013年1月1日起推出4G LTE網路。

## 服務

明訊提供了多樣化的通訊產品和服務。同時提供月租型和預付型服務。
截至2015年12月，Maxis在人口稠密地区的2G覆盖率为96％，3G为94.5％，4G
LTE为92％（以至少-110dBm测量），\[1\] 2G和3G网络现代化占88％。 明訊声称其4G LTE网络提供室外20Mbit /
s和室内10Mbit / s的平均速度，但没有提供第三方参考。

有几位着名的明星已经签署了明訊的发言人，包括Siti Nurhaliza，马来西亚现实电视节目Akademi幻想曲的参赛者，以及Jessy
Wong The Diva和Arthur the Brave和Fluffy。
明訊是第一家于2013年1月1日在马来西亚推出LTE服务的公司，从巴生谷地区开始。

### 明讯 FastTap

明讯也是马来西亚的第一个NFC（[近场通信](../Page/近场通信.md "wikilink")）服务叫FastTap。它集成了Touch'n
Go，来自Maybank的Visa Wave \[2\]

## 外部链接

  - [明讯官方首页](http://www.maxis.com.my)

[category:行動電話運營商](../Page/category:行動電話運營商.md "wikilink")

[Category:馬來西亞通信公司](../Category/馬來西亞通信公司.md "wikilink")
[Category:1993年馬來西亞建立](../Category/1993年馬來西亞建立.md "wikilink")

1.
2.