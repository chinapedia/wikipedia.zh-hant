**馬德拉島蛋糕**（），是一種傳統的[英國飲食海綿蛋糕](../Page/英國飲食.md "wikilink")。

## 名稱

有時被誤認為是源於[馬德拉](../Page/馬德拉.md "wikilink")；然而，事實並非如此，而是後來以[马德拉酒命名](../Page/马德拉酒.md "wikilink")，來自島嶼的[葡萄酒](../Page/葡萄酒.md "wikilink")，英國在當時很受歡迎，經常搭配蛋糕\[1\]\[2\]\[3\]\[4\]。

## 參見

  - [蛋糕列表](../Page/蛋糕列表.md "wikilink")

## 參考資料

[Category:蛋糕](../Category/蛋糕.md "wikilink")
[Category:英國食品](../Category/英國食品.md "wikilink")

1.
2.
3.
4.