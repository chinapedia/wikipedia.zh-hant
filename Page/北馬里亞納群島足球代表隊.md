**北馬利亞納群島足球代表隊**是[北馬里亞納群島足球協會派出的一支認可官方](../Page/北馬里亞納群島足球協會.md "wikilink")[足球代表隊](../Page/足球.md "wikilink")，其至今仍然未加入[國際足協](../Page/國際足協.md "wikilink")。於2010年6月8日，在[南非](../Page/南非.md "wikilink")[約翰尼斯堡](../Page/約翰尼斯堡.md "wikilink")[加拉雷會議中心舉行的](../Page/加拉雷會議中心.md "wikilink")[亞洲足球聯會特別代表大會上](../Page/亞洲足球聯會.md "wikilink")，決定接受北馬里亞納群島足球協會成為亞洲足球聯會候補會員。於[2006年12月](../Page/2006年12月.md "wikilink")，北馬利亞納群島以臨時會員身份加入東亞足球協會，於[2008年9月被確認為正式會員](../Page/2008年9月.md "wikilink")\[1\]。

北馬利亞納群島足球代表隊首次參加的主要賽事是[密克羅尼西亞聯邦運動會](../Page/密克羅尼西亞聯邦運動會.md "wikilink")。

北馬里亞納群島足球協會會長為Jerry
Tan，即[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink")[陳守仁之子](../Page/陳守仁.md "wikilink")。

## 國際賽成績

<table>
<thead>
<tr class="header">
<th><p>日期</p></th>
<th><p>地點</p></th>
<th></th>
<th><p>對手</p></th>
<th><p>比數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2007年4月1日</p></td>
<td><p>關島</p></td>
<td><p>北馬利亞納群島 </p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Guam.svg" title="fig:Flag_of_Guam.svg">Flag_of_Guam.svg</a> <a href="../Page/關島國家足球隊.md" title="wikilink">關島</a></p></td>
<td><p>0 - 9</p></td>
</tr>
<tr class="even">
<td><p>2007年3月25日</p></td>
<td><p>北馬利亞納群島</p></td>
<td><p>北馬利亞納群島 </p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Guam.svg" title="fig:Flag_of_Guam.svg">Flag_of_Guam.svg</a> <a href="../Page/關島國家足球隊.md" title="wikilink">關島</a></p></td>
<td><p>3 - 3</p></td>
</tr>
<tr class="odd">
<td><p>1999年6月12日</p></td>
<td><p>密克羅尼西亞聯邦</p></td>
<td><p>北馬利亞納群島 </p></td>
<td><p><a href="../Page/密克羅尼西亞聯邦國家足球隊.md" title="wikilink">密克羅尼西亞聯邦</a></p></td>
<td><p>0 - 7</p></td>
</tr>
<tr class="even">
<td><p>1998年8月3日</p></td>
<td><p>帛琉</p></td>
<td><p>北馬利亞納群島 </p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Guam.svg" title="fig:Flag_of_Guam.svg">Flag_of_Guam.svg</a> <a href="../Page/關島國家足球隊.md" title="wikilink">關島</a></p></td>
<td><p>3 - 0</p></td>
</tr>
<tr class="odd">
<td><p>1998年8月2日</p></td>
<td><p>帛琉</p></td>
<td><p>北馬利亞納群島 </p></td>
<td><p><a href="../Page/帛琉國家足球隊.md" title="wikilink">帛琉</a></p></td>
<td><p>12 - 2</p></td>
</tr>
<tr class="even">
<td><p>1998年7月30日</p></td>
<td><p>帛琉</p></td>
<td><p>北馬利亞納群島 </p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Guam.svg" title="fig:Flag_of_Guam.svg">Flag_of_Guam.svg</a> <a href="../Page/關島國家足球隊.md" title="wikilink">關島</a></p></td>
<td><p>2 - 2</p></td>
</tr>
<tr class="odd">
<td><p>1998年7月</p></td>
<td><p>帛琉</p></td>
<td><p>北馬利亞納群島 </p></td>
<td><p><a href="../Page/波納佩島足球代表隊.md" title="wikilink">波納佩島</a></p></td>
<td><p>13 - 0</p></td>
</tr>
<tr class="even">
<td><p>1998年7月</p></td>
<td><p>帛琉</p></td>
<td><p>北馬利亞納群島 </p></td>
<td><p><a href="../Page/雅浦島足球代表隊.md" title="wikilink">雅浦島</a></p></td>
<td><p>8 - 0</p></td>
</tr>
</tbody>
</table>

## 歷任教練

2013年:[古廉權其足球代表隊教練為曾經帶領](../Page/古廉權.md "wikilink")[香港甲組足球聯賽](../Page/香港甲組足球聯賽.md "wikilink")[晨曦足球會成為](../Page/晨曦足球會.md "wikilink")「五冠王」的，其他足球教練包括同[王國斌和](../Page/王國斌.md "wikilink")[鄧志明等](../Page/鄧志明.md "wikilink")\[2\]\[3\]。

2014至今:[関口潔](../Page/関口潔.md "wikilink")

## 備註

## 外部連結

  - [加入東亞足球聯盟](https://web.archive.org/web/20070228024641/http://www.eaff.com/release/2006/061217.html)

[Category:大洋洲國家足球隊](../Category/大洋洲國家足球隊.md "wikilink")

1.
2.  古廉權統領塞班島 晨曦5冠名帥推動太平洋足運 《東方日報》 2012年12月15日
3.  古廉權助塞班島發展足運 《太陽報》 2012年12月15日