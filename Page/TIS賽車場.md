**TIS賽車場**（）位於[台灣](../Page/台灣.md "wikilink")[桃園市](../Page/桃園市.md "wikilink")[龍潭區](../Page/龍潭區_\(桃園市\).md "wikilink")（原桃園縣龍潭鄉）高原里，因此又被稱為**龍潭賽車場**或**龍潭TIS**（以下簡稱TIS）。

TIS設立於1993年，是台灣第一個正規的[賽車場地](../Page/賽車.md "wikilink")。由台灣賽必威國際股份有限公司興建營運，[中華賽車會協助規劃](../Page/中華賽車會.md "wikilink")。TIS可供[房車](../Page/汽車.md "wikilink")、[機車](../Page/機車.md "wikilink")、小型賽車（Go-Kart）比賽，賽道全長1.85[公里](../Page/公里.md "wikilink")，寬10至15[公尺](../Page/公尺.md "wikilink")，八個[右彎](../Page/右.md "wikilink")，五個[左彎](../Page/左.md "wikilink")，全是[柏油鋪面](../Page/柏油.md "wikilink")，依用途分成四個賽道。TIS除了舉行比賽之外，平時也收費讓賽車愛好者進場練車，也是國際知名的賽車手（例如[日本的](../Page/日本.md "wikilink")[土屋圭市](../Page/土屋圭市.md "wikilink")、[瑞典的Eric](../Page/瑞典.md "wikilink")
Carlsson等）到台灣表演車技的場地。

在2005年12月，由於土地租約到期，地主不再續租。在中華賽車協會與地主協商下，地主同意再續約一年，重新鋪設跑道後於2006年3月19日以房車賽慶祝重新開幕。但由於桃園縣人口大幅增加，賽車場週邊陸續建有住宅，假日舉辦比賽時，經常遭周遭居民向環保局檢舉製造[噪音](../Page/噪音.md "wikilink")，且土地使用與都市計畫分區不符規定，因而被桃園縣政府於2013年9月勒令停用，10月1日起停止營業\[1\]。

## 另見

  - [大鵬灣國際賽車場](../Page/大鵬灣國際賽車場.md "wikilink")

## 參考資料

## 外部連結

  - [中華賽車會](http://www.ctmsa.org.tw)
  - [Motoring Fans
    玩車友網站的TIS詳細資料](https://web.archive.org/web/20051124075721/http://www.motoringfans.com.tw/circuit/default.asp?category=tis)

[Category:桃園市體育場地](../Category/桃園市體育場地.md "wikilink")
[Category:賽車場](../Category/賽車場.md "wikilink") [Category:龍潭區
(臺灣)](../Category/龍潭區_\(臺灣\).md "wikilink")

1.