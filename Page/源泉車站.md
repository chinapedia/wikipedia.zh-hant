**源泉車站**位於[台灣](../Page/台灣.md "wikilink")[彰化縣](../Page/彰化縣.md "wikilink")[二水鄉](../Page/二水鄉.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[集集線的](../Page/集集線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。
[TRA_Yuanquan_Station.JPG](https://zh.wikipedia.org/wiki/File:TRA_Yuanquan_Station.JPG "fig:TRA_Yuanquan_Station.JPG")

## 車站構造

  - [側式月台一座](../Page/側式月台.md "wikilink")。

## 利用狀況

  - 目前為[招呼站](../Page/臺灣鐵路管理局車站等級#招呼站.md "wikilink")。
  - 行駛車輛為DR1000型[柴油客車](../Page/柴油.md "wikilink")，等級為[區間車](../Page/臺鐵區間車.md "wikilink")。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通及](../Page/一卡通_\(台灣\).md "wikilink")[icash
    2.0付費](../Page/icash.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 2001 | 8         |
| 2002 | 10        |
| 2003 | 12        |
| 2004 | 12        |
| 2005 | 11        |
| 2006 | 12        |
| 2007 | 11        |
| 2008 | 17        |
| 2009 | 20        |
| 2010 | 24        |
| 2011 | 27        |
| 2012 | 28        |
| 2013 | 29        |
| 2014 | 26        |
| 2015 | 24        |
| 2016 | 25        |
| 2017 | 24        |

歷年旅客人次紀錄

## 車站週邊

  - [濁水溪](../Page/濁水溪.md "wikilink")
  - [彰化縣](../Page/彰化縣.md "wikilink")[二水鄉源泉國民小學](../Page/二水鄉.md "wikilink")
  - [林先生廟](../Page/林先生廟.md "wikilink")：紀念協助[八堡圳開鑿的匿名人士](../Page/八堡圳.md "wikilink")。
  - 二水鼻仔頭鄭氏古厝

## 公車資訊

### 公路客運

  - [員林客運](../Page/員林客運.md "wikilink")：
      - **<font color="red">6701</font>** 員林－竹山
      - **<font color="red">6702</font>** 員林－水-{里}-

## 歷史

  - 1922年1月14日：開通，初名**鼻子頭**。
  - 1962年4月1日：與[龍泉](../Page/龍泉車站.md "wikilink")、[大山](../Page/大山車站.md "wikilink")、[山佳](../Page/山佳車站.md "wikilink")、[鎮安](../Page/鎮安車站.md "wikilink")、[侯硐](../Page/猴硐車站.md "wikilink")、[平和等車站同時改稱今名](../Page/平和車站.md "wikilink")。
  - 1979年5月1日：由簡易站降為招呼站，並指定由[二水站管理](../Page/二水車站.md "wikilink")。
  - 2015年6月30日：啟用多卡通刷卡機。

## 鄰近車站

## 參考文獻

## 外部連結

  - [源泉車站（臺灣鐵路管理局）](http://www.railway.gov.tw/Taichung-Transportation/cp.aspx?sn=12122)

[Category:彰化縣鐵路車站](../Category/彰化縣鐵路車站.md "wikilink")
[Category:八卦山風景區](../Category/八卦山風景區.md "wikilink")
[Category:1922年启用的铁路车站](../Category/1922年启用的铁路车站.md "wikilink")
[Category:二水鄉](../Category/二水鄉.md "wikilink")
[Category:集集線車站](../Category/集集線車站.md "wikilink")