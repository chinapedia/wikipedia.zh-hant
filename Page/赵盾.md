**赵盾**，又称**赵宣子**、**赵孟**。[春秋时期](../Page/春秋时期.md "wikilink")[晋国大夫](../Page/晋国.md "wikilink")。[赵衰之子](../Page/赵衰.md "wikilink")，[赵同](../Page/赵同.md "wikilink")、[赵括](../Page/赵括_\(春秋\).md "wikilink")、[赵婴齐之兄](../Page/赵婴齐_\(春秋\).md "wikilink")。

## 生平

公元前656年（周惠王二十一年），赵衰跟随[重耳逃亡](../Page/晋文公.md "wikilink")[狄地](../Page/狄.md "wikilink")，娶狄女[叔隗](../Page/叔隗.md "wikilink")，生赵盾。

公元前621年（周襄王三十一年）春，赵盾执掌晋国国事。八月，[晋襄公崩](../Page/晋襄公.md "wikilink")，赵盾等立晋襄公之子[夷皋](../Page/晋灵公.md "wikilink")。晋襄公遗命立[夷皋为君](../Page/晋灵公.md "wikilink")，而夷皋年幼，故晋人希望立年长者为君安定晋国。赵盾欲迎立正在[秦国作亚卿的](../Page/秦国.md "wikilink")[公子雍](../Page/公子雍.md "wikilink")（[晋文公与](../Page/晋文公.md "wikilink")[杜祁之子](../Page/杜祁.md "wikilink")），而[狐射姑则支持](../Page/狐射姑.md "wikilink")[公子乐](../Page/公子乐.md "wikilink")（[晋文公与](../Page/晋文公.md "wikilink")[辰嬴之子](../Page/辰嬴.md "wikilink")）。赵盾认为公子乐“母淫子辟”，而公子雍“母义子爱”且与秦国亲近，派[先蔑](../Page/先蔑.md "wikilink")、[士会前往秦国迎接公子雍](../Page/士会.md "wikilink")。襄公夫人[穆赢抱夷皋在朝堂大哭](../Page/穆赢.md "wikilink")，以“先君之命”责备赵盾，赵盾和各大夫被逼不过，只得背着先蔑等人立夷皋为君，是为[灵公](../Page/晋灵公.md "wikilink")。赵盾率军在令狐将护送公子雍回晋的秦军击败，但此事已徹底損害了秦晉的友好關係,使春秋時期,秦晉一直交惡。后与齐侯、宋公、卫侯、郑伯、许男、曹伯会盟于扈。

公元前619年（周襄王三十三年）冬十月壬午，赵盾与鲁国会盟于衡雍。

公元前615年（周顷王四年），[秦康公攻晋](../Page/秦康公.md "wikilink")，赵盾率晋军与秦军战于*河曲*。晋国大夫[臾骈认为秦军粮食不足](../Page/臾骈.md "wikilink")，不能持久，建议坚守待战。秦国大夫[士会看破臾骈之计](../Page/士会.md "wikilink")，让秦兵袭击晋营，诱[赵穿出击](../Page/赵穿.md "wikilink")。赵穿为晋侯女婿，狂妄却不晓军事，而且不服臾骈。赵穿中计孤军出击，赵盾唯恐赵穿被俘，全力出战接应，接战后退回。夜间秦军退走。

公元前613年（周顷王六年），赵盾赴鲁文公会盟，六月癸酉同盟于新城。

公元前608年，楚、郑攻击陈国、宋国，赵盾率军救援，与宋、陈、卫、曹等诸侯会师于**棐林**，随后进攻郑国。

公元前607年，晋灵公荒淫无道，赵盾多次直谏。灵公欲杀赵盾，派[鉏麑行刺](../Page/鉏麑.md "wikilink")。鉏麑感于赵盾忠公亲国，不忍下手，触槐而死。九月，灵公同赵盾饮酒，埋伏甲士欲杀赵盾，[提弥明以死相救](../Page/提弥明.md "wikilink")，赵盾逃出晋都。乙丑日，[赵穿杀灵公于桃园](../Page/赵穿.md "wikilink")。赵盾回到都城，迎立公子黑臀为君，是为[晋成公](../Page/晋成公.md "wikilink")。太史[董狐書曰](../Page/董狐.md "wikilink")：「趙盾弒其君夷皋。」趙盾說人不是他殺的。[史狐曰](../Page/董狐.md "wikilink")：「子為正卿，入諫不聽。出亡不遠，君弒，反不討賊，則志同。志同則書重，非子而誰？故書之曰『晉趙盾弒其君夷皋』。」

公元前601年，病逝。

## 参見

  - [趙穿](../Page/趙穿.md "wikilink")
  - [趙氏孤兒](../Page/趙氏孤兒.md "wikilink")

## 参考文献

  - [《左传·文公》](https://web.archive.org/web/20040808082009/http://www.chinakyl.com/rbbook/gb/13/7/cqzz_006.htm)
  - [《左传·宣公》](https://web.archive.org/web/20040808082512/http://www.chinakyl.com/rbbook/gb/13/7/cqzz_007.htm)

[Z赵](../Category/春秋战国政治人物.md "wikilink")
[Z赵](../Category/晋卿.md "wikilink")
[Z赵](../Category/春秋人.md "wikilink")
[Category:弒君者](../Category/弒君者.md "wikilink")