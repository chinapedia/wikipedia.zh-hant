**顾鼎臣**（），初名**仝**，字**九和**，号**未斋**，[谥](../Page/谥.md "wikilink")**文康**，[直隶](../Page/南直隸.md "wikilink")[崑山縣](../Page/崑山縣.md "wikilink")（今[江苏](../Page/江苏.md "wikilink")[昆山市](../Page/昆山市.md "wikilink")）人。[明朝](../Page/明朝.md "wikilink")[狀元](../Page/狀元.md "wikilink")、官员、書法家。

## 生平

生于[成化九年](../Page/成化.md "wikilink")（1473年），[弘治十八年](../Page/弘治_\(明朝\).md "wikilink")（1505年）成乙丑科一甲第一名進士（状元），授[翰林院](../Page/翰林院.md "wikilink")[修撰](../Page/修撰.md "wikilink")。[正德年间](../Page/正德_\(明朝\).md "wikilink")，任[左谕德](../Page/左谕德.md "wikilink")。擅长[青词](../Page/青词.md "wikilink")，撰“步虚词”七章，受到“优诏褒答”，得以入閣，歷官[礼部右侍郎](../Page/礼部右侍郎.md "wikilink")、[吏部左侍郎](../Page/吏部左侍郎.md "wikilink")、掌[詹事府](../Page/詹事府.md "wikilink")、[礼部尚书](../Page/礼部尚书.md "wikilink")，[嘉靖十七年](../Page/嘉靖.md "wikilink")（1538年）八月，兼任[文渊阁大学士](../Page/文渊阁大学士.md "wikilink")，入参机务。不久加少保、太子太傅、进武英殿大学士。時人稱“青詞宰相”\[1\]。嘉靖十九年（1540年）逝世，追赠[太保衔](../Page/太保.md "wikilink")，[谥](../Page/谥.md "wikilink")**文康**\[2\]。曾擁有《[清明上河圖](../Page/清明上河圖.md "wikilink")》。

## 著作

工书法，似[赵孟頫](../Page/赵孟頫.md "wikilink")。著有《医眼方论》一卷，《经验方》一卷。

## 参考文献

{{-}}

[Category:明朝内阁首辅](../Category/明朝内阁首辅.md "wikilink")
[Category:明朝狀元](../Category/明朝狀元.md "wikilink")
[Category:明朝翰林院编修](../Category/明朝翰林院编修.md "wikilink")
[Category:明朝礼部右侍郎](../Category/明朝礼部右侍郎.md "wikilink")
[Category:明朝吏部左侍郎](../Category/明朝吏部左侍郎.md "wikilink")
[Category:明朝詹事府官员](../Category/明朝詹事府官员.md "wikilink")
[Category:明朝礼部尚书](../Category/明朝礼部尚书.md "wikilink")
[Category:明朝文渊阁大学士](../Category/明朝文渊阁大学士.md "wikilink")
[Category:明朝少保](../Category/明朝少保.md "wikilink")
[Category:明朝太子太傅](../Category/明朝太子太傅.md "wikilink")
[Category:明朝武英殿大学士](../Category/明朝武英殿大学士.md "wikilink")
[Category:明朝追赠太保](../Category/明朝追赠太保.md "wikilink")
[Category:明朝書法家](../Category/明朝書法家.md "wikilink")
[Category:苏州状元](../Category/苏州状元.md "wikilink")
[Category:昆山人](../Category/昆山人.md "wikilink")
[D](../Category/顧姓.md "wikilink")
[Category:諡文康](../Category/諡文康.md "wikilink")

1.  [清](../Page/清.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷193）：“嘉靖初，直經筵。進講范浚心箴，敷陳剴切。帝悅，乃自為註釋，而鼎臣特受眷。累官詹事、給事中劉世揚、李仁劾鼎臣汙佞。帝下世揚等獄，以鼎臣救，得薄譴。拜禮部右侍郎。帝好長生術，內殿設齋醮。鼎臣進步虛詞七章，且列上壇中應行事。帝優詔褒答，悉
    從之。詞臣以青詞結主知，由鼎臣倡也。改吏部左侍郎，掌詹事府。請令曾子後授五經博士，比三氏子孫，從之。大同軍變，
    張孚敬主用兵，鼎臣言不可，帝嘉納。十三年孟冬，享廟，命鼎臣及侍郎霍韜捧主。二人
    有期功服，當辭。乃上言：「古禮，諸侯絕期。今公卿即古諸侯，請得毋避。」禮部尚書夏言
    極詆其非，乃已。尋進禮部尚書，仍掌府事。京師淫雨，四方多水災，鼎臣請振饑弭盜，報可。”
2.  [清](../Page/清朝.md "wikilink")·[张廷玉等](../Page/张廷玉.md "wikilink")，《[明史](../Page/明史.md "wikilink")》（卷193）：“十七年八月，以本官兼文淵閣大學士入參機務。尋加少保、太子太傅、進武英殿。初，
    李時為首輔，夏言次之，鼎臣又次之。時卒，言當國專甚，鼎臣素柔媚，不能有為，充位而已。帝將南巡，立皇太子，命言扈行，鼎臣輔太子監國。御史蕭祥曜劾吏部侍郎張潮受鼎
    臣屬，調刑部主事陸崑為吏部。潮言：「兵部主事馬承學恃鼎臣有聯，自詭必得銓曹，臣故
    抑承學而用崑。」帝下承學詔獄，鼎臣不問。十九年十月卒官，年六十八。贈太保，諡文康。”