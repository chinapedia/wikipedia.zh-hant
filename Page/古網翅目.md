**古網翅目**（[學名](../Page/學名.md "wikilink")：）是一目已[滅絕](../Page/滅絕.md "wikilink")，體型介乎中等至大型的原始[古生代](../Page/古生代.md "wikilink")[古翅類](../Page/古翅類.md "wikilink")[昆蟲](../Page/昆蟲.md "wikilink")。牠們有像鳥喙的口器，前後翅膀相似，在第一對翅膀前有一對額外的小翅。牠們的翅膀上的脈很粗，甚至在[化石上仍可見到](../Page/化石.md "wikilink")。一些種類的體型非常大，如*Mazothairos*，牠的翅膀展開約有55厘米。

古網翅目是[古網翅總目基底的](../Page/古網翅總目.md "wikilink")[側系群](../Page/側系群.md "wikilink")，而非一個分支，因為牠們[演化成其他目的昆蟲](../Page/演化.md "wikilink")。牠們生存於[石炭紀中期至](../Page/石炭紀.md "wikilink")[二疊紀晚期](../Page/二疊紀.md "wikilink")。

## 外部連結

  - [Tree of Life
    project](http://tolweb.org/tree?group=Paleodictyoptera&contgroup=Paleodictyopteroidea)

## 參考

  - Carpenter, F. M. 1992. Superclass Hexapoda. Volume 3 of Part R,
    Arthropoda 4; *Treatise on Invertebrate Paleontology*, Boulder,
    Colorado, Geological Society of America.

  -
  -
[Category:古網翅總目](../Category/古網翅總目.md "wikilink")