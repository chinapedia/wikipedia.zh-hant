**西北军**，泛指在[民國初年至](../Page/民國初年.md "wikilink")[中國抗日戰爭全面爆發](../Page/中國抗日戰爭.md "wikilink")，具有[中國西北地域关联背景的军队](../Page/中國西北.md "wikilink")，是一个泛称。其得名於1919年，由原计划参与[第一次世界大战的](../Page/第一次世界大战.md "wikilink")「参战军」改編而成的「西北邊防軍」。如[冯玉祥统帅](../Page/冯玉祥.md "wikilink")“国民军”，[中原大战后由冯玉祥体系中分裂出的](../Page/中原大战.md "wikilink")[韩复榘](../Page/韩复榘.md "wikilink")、[宋哲元](../Page/宋哲元.md "wikilink")、[石友三等](../Page/石友三.md "wikilink")。另外，[杨虎城所部源自陕军](../Page/杨虎城.md "wikilink")，也被称西北军；而杨虎城的部队在中原大战曾猛烈攻击过冯玉祥军。相比起[东北军的称呼](../Page/东北军.md "wikilink")，这些被视为西北军的队伍在形成过程中与西北地域并无稳定联系，很多将领是直隶、山东人，且也非发迹于西北（但也有一些人是起于西北的地方势力，如杨虎城、高桂滋）。即使从其队伍活动最久的势力范围看，也并非仅仅在西北，更多活动在塞北及华北地带。而还有一些地方势力，尤其是边疆民族地方的势力，虽然也在西北地域，但一般被视为西北军阀势力而不称西北军，如马家军（马鸿逵曾参与冯玉祥“护党救国西北军”又离开）、新疆不同时期的控制者。1930年[蒋介石在中原大战中获胜后](../Page/蒋介石.md "wikilink")，[南京國民政府对军队获得了优势主导权](../Page/南京國民政府.md "wikilink")，西北军这样的称呼慢慢不再被人们使用。

## 前身

清末[新軍建設計畫在陝西成立](../Page/新軍.md "wikilink")[陸軍第三十九混成協](../Page/陸軍.md "wikilink")，由陝西[督軍負責建立](../Page/督軍.md "wikilink")。1912年，[辛亥革命後](../Page/辛亥革命.md "wikilink")，民國成立，[袁世凯委任親信](../Page/袁世凯.md "wikilink")[陆建章任左路统领兼北京總統府警衛軍參謀官](../Page/陆建章.md "wikilink")（後升警衛軍統領）。1914年，陸建章以此部隊擴編為[陸軍第七師師長](../Page/陸軍第七師.md "wikilink")，由北京政府任命討伐陝西民變首領[白朗名義進軍陝西](../Page/白朗_\(民國\).md "wikilink")，並取代原陝西都督兼民政長[張鳳翽](../Page/張鳳翽.md "wikilink")，成為新的陝西都督（全名：武将軍督理陝西軍務）。後來，陆建章委任其内侄女婿[馮玉祥任第七師左翼第一营](../Page/馮玉祥.md "wikilink")[營長](../Page/營長.md "wikilink")。以後，此營擴編為第十四旅、混編第十六旅，馮玉祥亦隨著部隊擴編升任旅长，並在西北募兵建軍。軍閥史中各路軍閥大多在出生地發跡，馮玉祥以[安徽人身分在西北立足](../Page/安徽.md "wikilink")，根源由此開始。

1915年[袁世凱稱帝](../Page/袁世凱稱帝.md "wikilink")，陸建章為擁護者之一。陸建章隸下的混編第十六旅在當時雖然效忠袁世凱政府，但是並未接受北洋軍元老[王士珍呼應發電報擁立袁世凱的要求](../Page/王士珍.md "wikilink")。在北京政府的命令下，混編第十六旅開往[四川準備討伐](../Page/四川.md "wikilink")[護國軍](../Page/護國軍.md "wikilink")，途中與遭到護國軍擊潰的混編第四旅會合。當時混編第四旅第二團第二營營副為馮玉祥的舊友[鹿鍾麟](../Page/鹿鍾麟.md "wikilink")，因此第二團接受改編加入混編第十六旅，這批部隊因為之前立場未明確表態的遠因下與護國軍私下達成協議，實際上並未和護國軍直接作戰，這作法保全了馮玉祥日後興起的家底。這批部隊直到1920年後再度回到西北，這段空白時間則在北洋軍系的互鬥間西北未有長時間的根據勢力。

1916年，陸建章旗下的陝北鎮守使[陈树藩反對帝制](../Page/陈树藩.md "wikilink")，因此獨立自號陕西护国军总司令，與[胡景翼合作擊潰了陸建章之子](../Page/胡景翼.md "wikilink")[陸承武率領的陝西第一混成旅](../Page/陸承武.md "wikilink")，陸承武亦被俘虜。作為愛子安危的交換條件，陆建章主動將其權利讓渡給陳樹藩。陈树藩由此创立“陕军”，胡景翼則藉此取得陝西督軍之位，西北軍此時為雙頭領導之局。

1918年，[中国国民党元老](../Page/中国国民党.md "wikilink")、[书法家](../Page/书法家.md "wikilink")[于右任在](../Page/于右任.md "wikilink")[孙中山的指示下由胡景翼協助回到陕西组织成立](../Page/孙中山.md "wikilink")“靖国军”\[1\]，手下有[樊钟秀](../Page/樊钟秀.md "wikilink")、[杨虎城等将領](../Page/杨虎城.md "wikilink")。這導致身為安徽派卻與北京政府關係密切的陳樹藩不安，兩方多次發生衝突。為此1919年[皖系军阀控制的](../Page/皖系军阀.md "wikilink")[北洋政府派](../Page/北洋政府.md "wikilink")[徐树铮出兵西北解決問題](../Page/徐树铮.md "wikilink")，其手下的军队改称“西北边防军”。徐树铮在1918年杀死陆建章。几年之后，徐树铮被冯玉祥杀死。

## 成军

[Fyc.png](https://zh.wikipedia.org/wiki/File:Fyc.png "fig:Fyc.png")
在冯玉祥的[连襟陕西省长](../Page/连襟.md "wikilink")[阎相文给](../Page/阎相文.md "wikilink")[直系军阀的](../Page/直系军阀.md "wikilink")[曹锟](../Page/曹锟.md "wikilink")、[吴佩孚的推荐下冯玉祥所部扩编为师](../Page/吴佩孚.md "wikilink")。1921年阎相文因为工作压力过大自杀。冯玉祥趁机接掌西北兵权，任陕西督军、河南督军。1922年，冯玉祥任陆军检阅使。1923年5月11日，裁撤蒙疆善后委员会，同时成立西北边防督办公署，由大总统曹锟特任陆军检阅使冯玉祥为西北边防督办，其职责为管理内、外蒙古及新疆地方事务，对西北地区边防负有军事上、行政上的完全责任。不久，政府又发表冯玉祥兼任甘肃督办，他委派[刘郁芬代理](../Page/刘郁芬.md "wikilink")，[蒋鸿遇为](../Page/蒋鸿遇.md "wikilink")[帮办](../Page/帮办.md "wikilink")，后又保举[薛笃弼为甘肃省长](../Page/薛笃弼.md "wikilink")。

1924年，[第二次直奉戰爭中](../Page/第二次直奉戰爭.md "wikilink")，吴佩孚任命冯玉祥为第三军总司令，令其出[古北口进击](../Page/古北口.md "wikilink")[开鲁攻击](../Page/开鲁.md "wikilink")[奉系军阀](../Page/奉系军阀.md "wikilink")，但冯受奉系贿赂，和驻北京的[孙岳](../Page/孙岳.md "wikilink")、[胡景翼联合反戈占领北京](../Page/胡景翼.md "wikilink")，发动[北京政变](../Page/北京政变.md "wikilink")，改名“国民军”，造成直系兵败。冯玉祥囚禁直系首领[曹锟](../Page/曹锟.md "wikilink")，但因为自己兵力不过4万人，请出[皖系段祺瑞执政](../Page/皖系.md "wikilink")，驱逐[溥仪出宫](../Page/溥仪.md "wikilink")，邀请[孙中山北上](../Page/孙中山.md "wikilink")。孙到达北京后于1925年初病逝。1925年1月，冯玉祥就任西北边防督办，所部也就被称之为“西北军”\[2\]。在[苏联支持下](../Page/苏联.md "wikilink")，冯的部队达到15万人。

1925年底，国民军参加长江中游的吴佩孚和长江下游的[孙传芳联合发动的](../Page/孙传芳.md "wikilink")[反奉战争](../Page/反奉战争.md "wikilink")。11月22日，奉系将领[郭松龄在冯玉祥的支持下](../Page/郭松龄.md "wikilink")，在[直隶](../Page/直隶.md "wikilink")[滦州倒戈](../Page/滦州.md "wikilink")，将所部改称东北国民军，迅速回师占领[山海关](../Page/山海关.md "wikilink")、[锦州](../Page/锦州.md "wikilink")、[新民等地](../Page/新民.md "wikilink")，直逼[沈阳](../Page/沈阳.md "wikilink")。12月24日郭松龄兵败被捕，次日被杀。张作霖挥师南下入关，挺进京津。1926年1月直、奉联合，攻击国民军。在其攻势下冯玉祥下野，去[苏联考察](../Page/苏联.md "wikilink")，西北边防督办一职由[张之江代理](../Page/张之江.md "wikilink")。3月奉军在[日军的掩护下占领天津](../Page/日本.md "wikilink")。[三·一八惨案之后](../Page/三·一八惨案.md "wikilink")，1926年4月9日国民军驱逐[段祺瑞](../Page/段祺瑞.md "wikilink")。随后国民军被直、奉联合击败，撤出北京，1926年5月到8月在[南口大战失败后撤回至西北](../Page/南口大战.md "wikilink")。

## 壮大

国民军的主力是冯玉祥、[胡景翼](../Page/胡景翼.md "wikilink")、[孙岳下属的三个军](../Page/孙岳.md "wikilink")。胡景翼的手下多为靖国军的旧部。胡景翼病死后，其手下部队多投奔冯玉祥。另有奉系[郭松龄残部由](../Page/郭松龄.md "wikilink")[魏益三率领加入国民军的第四军和脱离](../Page/魏益三.md "wikilink")[张宗昌加入国民军的](../Page/张宗昌.md "wikilink")[方振武部第五军](../Page/方振武.md "wikilink")。

1926年8月冯玉祥回国。之前1月直系吴佩孚手下[刘镇华的](../Page/刘镇华.md "wikilink")“镇嵩军”进攻[西安的杨虎城](../Page/西安.md "wikilink")，[围城8个月](../Page/二虎守长安.md "wikilink")，9月被冯玉祥派[邓宝珊率](../Page/邓宝珊.md "wikilink")[孙良诚和](../Page/孙良诚.md "wikilink")[吉鸿昌击败](../Page/吉鸿昌.md "wikilink")。刘镇华投入冯玉祥门下。

冯玉祥1926年9月17日在[綏遠](../Page/綏遠.md "wikilink")[五原誓師](../Page/巴彦淖尔.md "wikilink")，任[國民革命軍聯軍](../Page/國民革命軍.md "wikilink")（後改國民革命軍第二集團軍）司令，宣佈率領所部9个方面军全軍加入中國國民黨。1927年5月出兵[潼关](../Page/潼关.md "wikilink")，参加[國民革命軍北伐](../Page/國民革命軍北伐.md "wikilink")。部队达到25万人，后达到40万，这时的西北军达到顶峰。同年响应南方的[蒋介石和](../Page/蒋介石.md "wikilink")[汪精卫进行](../Page/汪精卫.md "wikilink")[清党反共](../Page/清党.md "wikilink")。

北伐时期國民革命軍有四个集团军，分别是蒋介石的第一集团军、冯玉祥的第二集团军、阎锡山的第三集团军、和[桂系李宗仁白崇禧的第四集团军](../Page/桂系.md "wikilink")。

1927年冯玉祥所部渡过[黄河](../Page/黄河.md "wikilink")，1928年参加第二期北伐，占领[天津](../Page/天津.md "wikilink")。1928年初，二次北伐结束，西北军实行缩编，全军缩编为十二个师，共22万人。

## 瓦解

1929年北伐成功后，由于裁军问题，冯玉祥与蒋介石产生了矛盾，导致蒋冯战争，但由于[阎锡山软禁冯玉祥和韩复榘](../Page/阎锡山.md "wikilink")、石友三改投蒋介石西北军很快失败。
[Song_Zheyuan.jpg](https://zh.wikipedia.org/wiki/File:Song_Zheyuan.jpg "fig:Song_Zheyuan.jpg")
1930年冯玉祥率所部参加[中原大战](../Page/中原大战.md "wikilink")，联合阎锡山、李宗仁等对抗蒋介石，是反蒋方的主力。初期吉鸿昌等杀得[陈诚大败](../Page/陈诚.md "wikilink")，但因为阎锡山的“晋军”支援不利和[張學良入关支持蒋](../Page/張學良.md "wikilink")，最终失败。冯玉祥下野隐居。西北军被蒋介石解散收编，余部被缩编为[宋哲元的第二十九路军](../Page/宋哲元.md "wikilink")、[孙连仲的第二十六路军](../Page/孙连仲.md "wikilink")、[吉鸿昌的第二十二路军](../Page/吉鸿昌.md "wikilink")、[梁冠英的第二十五路军等](../Page/梁冠英.md "wikilink")，以及之前投蒋的韩复榘、石友三的部队。

宋哲元的部队被[张学良改编为](../Page/张学良.md "wikilink")[国民革命军第二十九军](../Page/国民革命军第二十九军.md "wikilink")，成为[东北军作战序列](../Page/东北军.md "wikilink")，是后来[长城抗战](../Page/长城抗战.md "wikilink")、[卢沟桥事变中的主力](../Page/卢沟桥事变.md "wikilink")。在[喜峰口威震日军的](../Page/喜峰口.md "wikilink")“大刀队”就是建于1916年的冯玉祥的手枪队\[3\]。在[国共内战末期](../Page/国共内战.md "wikilink")[-{A中](../Page/淮海战役.md "wikilink")，这支部队在第三[綏靖區](../Page/綏靖區.md "wikilink")，其中的一部分在[张克侠](../Page/张克侠.md "wikilink")、[何基沣的带领下投奔](../Page/何基沣.md "wikilink")[解放军](../Page/解放军.md "wikilink")。

孙连仲的部队驻扎[华东](../Page/华东.md "wikilink")，后来成为[台儿庄大捷的主力](../Page/台儿庄大捷.md "wikilink")。

韩复榘镇守[山东对日军临阵退缩被国民政府处决](../Page/山东.md "wikilink")。[石友三则在包括日本人在内各方势力之间多次反水](../Page/石友三.md "wikilink")，最终被手下杀死。

这一时期，最终各支队伍已经自寻出路，但宋哲元成为众将领尊重的“大哥”，这种声望是从中原大战时期开始的。而冯玉祥则居于泰山悉心总结经验，同时仍与过去的部下们保持着联系，并参与了察哈尔抗日同盟军的事件。做为过去的首领，冯玉祥仍受到山东的控制者韩复榘的尊重。

## 杨虎城的西北军

[Yang_Hucheng_1949.jpg](https://zh.wikipedia.org/wiki/File:Yang_Hucheng_1949.jpg "fig:Yang_Hucheng_1949.jpg")
杨虎城在1929年投蒋，在蒋唐战争中击败[唐生智](../Page/唐生智.md "wikilink")。1930年[中原大战后](../Page/中原大战.md "wikilink")，西归升格为[國民革命軍第十七軍](../Page/國民革命軍第十七軍.md "wikilink")，6万多人，被称为“西北军”。1936年[西安事变后](../Page/西安事变.md "wikilink")，缩编为[国民革命军第三十八军](../Page/国民革命军第三十八军.md "wikilink")，由[孙蔚如率领](../Page/孙蔚如.md "wikilink")。[中国抗日战争期间](../Page/中国抗日战争.md "wikilink")，第三十八军扩编为第三十一[军团](../Page/军团.md "wikilink")，由孙蔚如任军团长；下辖第三十八军（军长[赵寿山](../Page/赵寿山.md "wikilink")）和第九十六军（军长[李兴中](../Page/李兴中.md "wikilink")），在[中条山等地](../Page/中条山.md "wikilink")\[4\]。抗日战争勝胜利后，第三十八军和第九十六军合并为第三十八军。1946年，[孔从周率第三十八军投奔](../Page/孔从周.md "wikilink")[中国共产党的](../Page/中国共产党.md "wikilink")[晋冀鲁豫军区](../Page/晋冀鲁豫军区.md "wikilink")，被改编为[西北民主联军第三十八军](../Page/西北民主联军.md "wikilink")，归[陈赓](../Page/陈赓.md "wikilink")、[谢富治](../Page/谢富治.md "wikilink")[兵团指挥](../Page/兵团.md "wikilink")，成为[解放军一部分](../Page/解放军.md "wikilink")。

## 人物

出身西北军的著名将领有：

  - [冯玉祥](../Page/冯玉祥.md "wikilink")，[胡景翼](../Page/胡景翼.md "wikilink")，[孙岳](../Page/孙岳.md "wikilink")
  - [方振武](../Page/方振武.md "wikilink")，[井岳秀](../Page/井岳秀.md "wikilink")，[孙蔚如](../Page/孙蔚如.md "wikilink")
  - 冯玉祥部下：[宋哲元](../Page/宋哲元.md "wikilink")，[张自忠](../Page/张自忠.md "wikilink")，[孙连仲](../Page/孙连仲.md "wikilink")，[石敬亭](../Page/石敬亭.md "wikilink")，[赵登禹](../Page/赵登禹.md "wikilink")，[韩复榘](../Page/韩复榘.md "wikilink")，[石友三](../Page/石友三.md "wikilink")，[秦德纯](../Page/秦德纯.md "wikilink")，[佟麟阁](../Page/佟麟阁.md "wikilink")，[刘汝明](../Page/刘汝明.md "wikilink")，[张维玺](../Page/张维玺.md "wikilink")，[孙良诚](../Page/孙良诚.md "wikilink")，[吴化文](../Page/吴化文.md "wikilink")，[梁冠英](../Page/梁冠英.md "wikilink")，[韩多峰](../Page/韩多峰.md "wikilink")，[吉鸿昌](../Page/吉鸿昌.md "wikilink")，[冯安邦](../Page/冯安邦.md "wikilink")，[冯治安](../Page/冯治安.md "wikilink")，[黄樵松](../Page/黄樵松.md "wikilink")，[池峰城](../Page/池峰城.md "wikilink")
  - 胡景翼部下：[邓宝珊](../Page/邓宝珊.md "wikilink")，[李虎臣](../Page/李虎臣.md "wikilink")，[续范亭](../Page/续范亭.md "wikilink")，[岳维峻](../Page/岳维峻.md "wikilink")，[董振堂](../Page/董振堂.md "wikilink")，[高桂滋](../Page/高桂滋.md "wikilink")，[弓富魁](../Page/弓富魁.md "wikilink")
  - 孙岳部下：[杨虎城](../Page/杨虎城.md "wikilink")，[徐永昌](../Page/徐永昌.md "wikilink")，[庞炳勋](../Page/庞炳勋.md "wikilink")

<!-- end list -->

  - [中国国民党派到西北军](../Page/中国国民党.md "wikilink")：[黄郛](../Page/黄郛.md "wikilink")、[杨杰](../Page/杨杰.md "wikilink")、[杜聿明](../Page/杜聿明.md "wikilink")、[张群等](../Page/张群.md "wikilink")

<!-- end list -->

  - [中国共产党派到西北军](../Page/中国共产党.md "wikilink")：[宣侠父](../Page/宣侠父.md "wikilink")、[刘伯坚](../Page/刘伯坚.md "wikilink")、[邓小平](../Page/邓小平.md "wikilink")、[王若飞](../Page/王若飞.md "wikilink")、[徐向前等](../Page/徐向前.md "wikilink")

### 馮玉祥五虎將

「馮玉祥五虎將」是指出身於西北軍的五位將領，五位均與[馮玉祥有所關係](../Page/馮玉祥.md "wikilink")，依序為[宋哲元](../Page/宋哲元.md "wikilink")、[張之江](../Page/張之江.md "wikilink")、[鹿鍾麟](../Page/鹿鍾麟.md "wikilink")、[鄭金聲](../Page/鄭金聲.md "wikilink")、[劉郁芬五人](../Page/劉郁芬.md "wikilink")。一說包括[李鳴鐘](../Page/李鳴鐘.md "wikilink")，李雖為早期馮玉祥麾下六師長之一，但功績不及其他五人且過早脫離西北軍因此不被廣泛認同。

## 参考文献

### 引用

### 来源

  - 网页

<!-- end list -->

  - [複國會之西北軍](https://web.archive.org/web/20050523172602/http://www.l-team.idv.tw/S-Report/S-Report-105.htm)——有西北軍的军装、武器照片

## 参见

  - [民国军阀](../Page/民国军阀.md "wikilink")
      - [东北军](../Page/东北军.md "wikilink")
      - [马家军](../Page/马家军.md "wikilink")
  - [西北野战军](../Page/西北野战军.md "wikilink")
  - [中国人民解放军兰州军区](../Page/中国人民解放军兰州军区.md "wikilink")

{{-}}

[Category:中华民国大陆时期军队](../Category/中华民国大陆时期军队.md "wikilink")
[Category:民国军阀](../Category/民国军阀.md "wikilink")
[西北军](../Category/西北军.md "wikilink")
[Category:西北军事史](../Category/西北军事史.md "wikilink")
[Category:冯玉祥](../Category/冯玉祥.md "wikilink")

1.  [葉惠芬：〈胡景翼與陝西靖國軍的建立〉，[國史館學術集刊](../Page/國史館.md "wikilink")，2006年](http://www.drnh.gov.tw/ImagesPost/a413570b-20df-47ac-a2c0-9f821ebe73bd/93808bd7-6836-4f29-8cf1-fd41e7075fc6_ALLFILES.pdf)

2.  [西北军名将张自忠](http://www.zhangzizhong.com/tbcr-4.asp)
3.  [揭秘令日寇闻风丧胆的西北军“破锋八刀”刀法](http://xk.cn.yahoo.com/articles/070912/1/3l8z.html)

4.