**郭榮宗**（**Kuo
June-tsung**，），[台灣政治人物](../Page/台灣.md "wikilink")，[國立台灣海洋大學航運管理碩士](../Page/國立台灣海洋大學.md "wikilink")，[民主進步黨籍](../Page/民主進步黨.md "wikilink")，曾任[桃園市議會第一屆議員](../Page/桃園市議會.md "wikilink")（辭職）、第五、六、七（遞補）屆立委、第十二、十三屆桃園縣觀音鄉長。

## 生平

郭榮宗出生於桃園縣[大園鄉](../Page/大園區.md "wikilink")，後因[桃園國際機場興建](../Page/桃園國際機場.md "wikilink")，舉家遷移至[觀音鄉](../Page/觀音區.md "wikilink")，1994年當選觀音鄉鄉長，後順利連任；2001年成功轉戰立委，後於2008年尋求三連霸時敗給前桃園縣副縣長[廖正井](../Page/廖正井.md "wikilink")；但廖於2009年10月底因涉及賄選（後於2014年平反、宣判無罪）遭到解職，因而於2010年1月舉辦補選，郭獲民進黨徵召，擊敗[中國國民黨與](../Page/中國國民黨.md "wikilink")[新黨共推的](../Page/新黨.md "wikilink")[陳麗玲](../Page/陳麗玲.md "wikilink")，高票重返立法院；2012年1月14日卻以七百餘票之差再度敗給廖正井，連任失敗；2014年11月29日，以觀音區最高票當選桃園市議員，並參與民進黨桃園市第二選區立委初選，敗給時任桃園市議員、民進黨桃園市黨部主委[陳賴素美](../Page/陳賴素美.md "wikilink")。

2015年8月24日，郭榮宗由於無法接受民事、刑事一審分別被判當選無效、[有期徒刑](../Page/有期徒刑.md "wikilink")6個月，因此在[臺灣高等法院審理前辭去桃園市議員職務以示抗議](../Page/臺灣高等法院.md "wikilink")。由於該選區議員缺額達半，[中華民國中央選舉委員會定於](../Page/中華民國中央選舉委員會.md "wikilink")2015年11月14日舉辦議員補選，民進黨徵召郭榮宗之子[郭裕信參選](../Page/郭裕信.md "wikilink")\[1\]，最終郭裕信以兩千餘票之差敗於[2014年九合一選舉中連任失利的前桃園縣議員](../Page/2014年九合一選舉.md "wikilink")[吳宗憲](../Page/吳宗憲_\(桃園縣議員\).md "wikilink")\[2\]。

2016年4月20日，高等法院因行賄罪判決6個月定讞，可易科罰金18萬、褫奪公權兩年。

### [2008年中華民國立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")

第七屆立法委員選舉在[桃園縣立法委員第二選舉區敗給](../Page/桃園縣立法委員第二選舉區.md "wikilink")[中國國民黨籍的](../Page/中國國民黨.md "wikilink")[廖正井](../Page/廖正井.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/郭榮宗.md" title="wikilink">郭榮宗</a></p></td>
<td></td>
<td><p>58,577</p></td>
<td><p>44.91%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/廖正井.md" title="wikilink">廖正井</a></p></td>
<td></td>
<td><p>71,174</p></td>
<td><p>54.57%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/蔡楊如松.md" title="wikilink">蔡楊如松</a></p></td>
<td><p><a href="../Page/台灣農民黨.md" title="wikilink">台灣農民黨</a></p></td>
<td><p>672</p></td>
<td><p>0.52%</p></td>
<td></td>
</tr>
</tbody>
</table>

### [2010年桃園縣第二選區立法委員補選](../Page/2010年桃園縣第二選區立法委員補選.md "wikilink")

2009年10月27日，[台灣高等法院判決廖正井當選無效](../Page/台灣高等法院.md "wikilink")\[3\]。11月19日[民主進步黨宣布徵召郭榮宗回鍋參選](../Page/民主進步黨.md "wikilink")[立委補選](../Page/2010年桃園縣第二選區立法委員補選.md "wikilink")\[4\]，在[彭添富顧全大局退選後郭榮宗順利當選](../Page/彭添富.md "wikilink")，重拾立委身分\[5\]。值得一提的是，郭榮宗是2008年立委選制變革以來首位桃園縣的民進黨立委。

| 2010年[桃園縣第二選舉區](../Page/桃園市第二選舉區.md "wikilink")[立法委員補選選舉結果](../Page/立法委員.md "wikilink") |
| --------------------------------------------------------------------------------------- |
| 號次                                                                                      |
| 1                                                                                       |
| 2                                                                                       |
| 3                                                                                       |
| **選舉人數**                                                                                |
| **投票數**                                                                                 |
| **有效票**                                                                                 |
| **無效票**                                                                                 |
| **投票率**                                                                                 |

### [2012年中華民國立法委員選舉](../Page/2012年中華民國立法委員選舉.md "wikilink")

| 2012年[桃園縣第二選舉區](../Page/桃園市第二選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

### [2014年中華民國直轄市議員及縣市議員選舉](../Page/2014年中華民國直轄市議員及縣市議員選舉.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>候選人</p></th>
<th><p>性別</p></th>
<th><p>政黨</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><strong><a href="../Page/吳宗憲_(桃園縣議員).md" title="wikilink">吳宗憲</a></strong></p></td>
<td><p>男</p></td>
<td></td>
<td><p>9,161</p></td>
<td><p>27.74%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/郭榮宗.md" title="wikilink">郭榮宗</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>11,424</p></td>
<td><p>34.60%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/廖薛文.md" title="wikilink">廖薛文</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>1,087</p></td>
<td><p>3.29%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/歐炳辰.md" title="wikilink">歐炳辰</a></p></td>
<td><p>男</p></td>
<td></td>
<td><p>11,347</p></td>
<td><p>34.37%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
</tbody>
</table>

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [立法院](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00120&stage=7)

[Category:第1屆桃園市議員](../Category/第1屆桃園市議員.md "wikilink")
[Category:桃園市政治人物](../Category/桃園市政治人物.md "wikilink")
[Category:觀音鄉鄉長](../Category/觀音鄉鄉長.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:國立臺灣海洋大學校友](../Category/國立臺灣海洋大學校友.md "wikilink")
[Category:國立中央大學附屬中壢高級中學校友](../Category/國立中央大學附屬中壢高級中學校友.md "wikilink")
[Category:觀音人](../Category/觀音人.md "wikilink")
[Category:大園人](../Category/大園人.md "wikilink")
[R榮宗](../Category/郭姓.md "wikilink")

1.  [郭榮宗辭桃市議員
    斷絕吳宗憲遞補機會](http://news.ltn.com.tw/news/politics/breakingnews/1422669)，自由時報
2.  [觀音議員補選
    吳宗憲當選](http://www.chinatimes.com/newspapers/20151115000437-260107)，中國時報
3.  [國民黨立委廖正井賄選案
    當選無效定讞](http://tw.news.yahoo.com/article/url/d/a/091027/1/1tryx.html)
    2009年10月27日，中廣新聞
4.  \[<http://www.dpp.org.tw/news_content.php?links=&kw=%E8%AB%8B%E8%BC%B8%E5%85%A5%E9%97%9C%E9%8D%B5%E5%AD%97>..\&Login1%3AtxtEmail=\&menu_sn=\&sub_menu=43\&show_title=%E6%B4%BB%E5%8B%95%E8%A8%8A%E6%81%AF%E5%85%AC%E5%91%8A\&sn=4113\&ad_type=\&act=
    民主進步黨針對桃園縣第二選區立法委員補選提名事宜之說明\]，[民主進步黨](../Page/民主進步黨.md "wikilink")，2009年11月19日
5.  [《立委補選／桃園》團結戰勝空降
    綠票倉傳捷報](http://www.libertytimes.com.tw/2010/new/jan/10/today-fo5-2.htm)