**費爾干納龍屬**（[屬名](../Page/屬.md "wikilink")：，意為「[費爾干納的蜥蜴](../Page/費爾干納盆地.md "wikilink")」）是[蜥腳下目的一](../Page/蜥腳下目.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，在2003年被正式描述、命名。牠的[模式種是](../Page/模式種.md "wikilink")*F.
verzilini*。牠與[瑞拖斯龍很相似](../Page/瑞拖斯龍.md "wikilink")。牠的[化石是在](../Page/化石.md "wikilink")1966年發現於[中亞的](../Page/中亞.md "wikilink")[吉爾吉斯](../Page/吉爾吉斯.md "wikilink")，出土於[巴剌班賽組](../Page/巴剌班賽組.md "wikilink")（Balabansai
Formation），年代相當於[侏羅紀中期的](../Page/侏羅紀.md "wikilink")[卡洛維階](../Page/卡洛維階.md "wikilink")。

## 參考

  - <https://web.archive.org/web/20050830082006/http://palaeo.gly.bris.ac.uk/dinobase/Yrs2000.html>
  - <https://web.archive.org/web/20070928020945/http://www.courier.com.ru/priroda/pr1003new_31.htm>
  - <https://web.archive.org/web/20090425130208/http://www.thescelosaurus.com/eusauropoda.htm>

[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:新蜥腳類](../Category/新蜥腳類.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")