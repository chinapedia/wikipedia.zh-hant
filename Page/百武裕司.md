為[日本業餘](../Page/日本.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")，因於1996年發現著名明亮[彗星C](../Page/彗星.md "wikilink")/1996
B2而成名。

## 生平

1950年，百武裕司生於[長崎縣](../Page/長崎縣.md "wikilink")[島原市](../Page/島原市.md "wikilink")，畢業於[九州產業大學攝影系](../Page/九州產業大學.md "wikilink")；中學時代的百武先生已迷上[天文學](../Page/天文學.md "wikilink")，還加入高中的地理學會；1989年開始彗星搜索；1993年移居[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[姶良町](../Page/姶良町.md "wikilink")，1995年12月25日和1996年1月30日以大口徑雙筒[望遠鏡](../Page/望遠鏡.md "wikilink")25\*150分別獨立發現C/1995
Y1和C/1996
B2[百武彗星](../Page/百武彗星.md "wikilink")，後者因為接近地球時亮度接近0等，肉眼易見，3月底時更拖著接近100度長的[彗尾](../Page/彗尾.md "wikilink")，成爲世紀大彗星之一；亦因兩個月內接連發現兩顆彗星，百武亦因此聲名大噪。

1996年3月[美國](../Page/美國.md "wikilink")[芝加哥Adler天文館邀請百武裕司為特別貴賓](../Page/芝加哥.md "wikilink")，並成為該市的榮譽市民，9月他成為日本姶良町(Aira)天文台及博物館館長，直至逝世。同年11月，百武裕司當選鹿兒島縣榮譽縣民，其他榮譽還有「日本天文學會天體發現獎」、「東亞天文學會天體發現獎」。

2000年5月27日，日本[清里大友天文台](../Page/清里大友天文台.md "wikilink")[大友哲把他在](../Page/大友哲.md "wikilink")1991年12月13日發現的[小行星7291命名爲](../Page/小行星7291.md "wikilink")「百武」。

他於2002年4月10日因為[主動脈的](../Page/主動脈.md "wikilink")[動脈瘤內出血而於晚上](../Page/動脈瘤.md "wikilink")7點47分在[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[國分市醫院猝逝](../Page/國分市.md "wikilink")。

## 生平

百武裕司妻子為百武昭子，夫妻育有兩子。

## 外部連結

  - [百武裕司畫廊](https://web.archive.org/web/20051216042255/http://uchukan.satsumasendai.jp/data/gallery/hyakutake.html)
  - [日本姶良町Aira天文台](http://www.synapse.ne.jp/starlandaira/)

[H](../Category/1950年出生.md "wikilink")
[H](../Category/2002年逝世.md "wikilink")
[H](../Category/日本天文学家.md "wikilink")
[H](../Category/業餘天文學家.md "wikilink")
[H](../Category/長崎縣出身人物.md "wikilink")
[H](../Category/九州產業大學校友.md "wikilink")
[H](../Category/彗星發現者.md "wikilink")