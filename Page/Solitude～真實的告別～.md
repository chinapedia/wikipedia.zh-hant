**solitude～真實的告別～**（**solitude**）是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")15張[單曲](../Page/單曲.md "wikilink")。於2002年10月23日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

雖然綑上了[電視](../Page/電視.md "wikilink")[連續劇主題曲之名](../Page/連續劇.md "wikilink")，再加上推出了兩款設計不同封面的初回版，連收錄曲不同的通常版共三種款式，但結果依然阻止不了銷量下滑。據[Oricon統計所動的首批銷量只比上張單曲輕輕稍作上調](../Page/Oricon.md "wikilink")，總銷量反比上張單曲回落。

本單曲初回版與通常版收錄歌曲不同。初回版收錄A面曲、c/w曲及加送歌曲；而通常版則不收錄加送歌曲，改為A面曲的純音樂版本。這是繼上張作品『[藍色憂傷](../Page/藍色憂傷.md "wikilink")』初回版與通常版封面不同，及對上一張作品『[Hey！大家好嗎？](../Page/Hey!大家好嗎?.md "wikilink")』只在初回版收錄加送歌曲後，再一次改變兩個版本的收錄內容。這個迫使想要把所有歌曲到手的樂迷只得每個版本都必需購法的營商手法，直至現在為止亦被批評為不誠實。可是，大多數人氣偶像及大型唱片公司均已採用這種營商手法。

KinKi
Kids二人共同創作的歌曲，曾有『[越來越喜歡　越來越愛](../Page/越來越喜歡_越來越愛/KinKi充滿幹勁之歌.md "wikilink")』及『[愛的聚合物](../Page/Hey!大家好嗎?#初回版收錄歌曲.md "wikilink")』兩首，但全由一人製作的樂曲就只有這首歌曲唯一一首。另外，這也是唯一一首單曲A面曲沒有序幕部份（Intro），歌曲開始即進行歌詞部份。由於在連續劇播放的版本，把序幕版本刪掉以A段旋律開始，故在CD發售前歌詞開首部份從未公開過。

至於在初回版收錄的加送歌曲，當初並沒有公開該曲作曲作詞者的真正身份。

### K.Dino的小插曲

在單曲發售初時，均無人清楚究竟負責作曲作詞者的“K.Dino”是誰。結果在當年年末開始跨越到2002年底的跨年倒數[演唱會](../Page/演唱會.md "wikilink")『KinKi
Kids DOME F Concert ～Fun Fan
Forever～』[2003年1月1日的演出上](../Page/2003年1月1日.md "wikilink")，終於公開了其真正身份正是[堂本光一本人](../Page/堂本光一.md "wikilink")。而當天正是光一的生日。

“K.Dino”名字中的「Dino」，是取自光一沉醉的[一級方程式賽車車隊](../Page/一級方程式賽車.md "wikilink")・[法拉利的始創者](../Page/法拉利.md "wikilink")[恩佐·法拉利的兒子](../Page/恩佐·法拉利.md "wikilink")[阿法度·法拉利的暱稱](../Page/阿法度·法拉利.md "wikilink")“Dino”。據光一自己所解釋為，「若以其名字縮寫的“K.D”的話一定會被猜到是堂本光一，所以把D的部份改做法拉利兒子的暱稱Dino」。其後他更說「實在取得太隨便了」。

至於為什麼不以自己名字去記載作曲作詞者，光一的理由是「若以自己名義的話，歌迷就一定會先入為主地去聽，其他的人也有可能會持有偏見。所以就想大家能夠先以歌曲為主」。正因如此，據說每當被[雜誌等的訪問問及有關這首歌的感想時](../Page/雜誌.md "wikilink")，光一本人也甚為苦惱。

\===解說5X9=63===
收錄於初回版的加送歌曲『5X9=63』，若以真正的[數學來計算則是個錯誤的答案](../Page/數學.md "wikilink")。其實那並不是條算式，該以日語發音「5()9()63（）」這樣的組合去理解，意思是感謝別人付出的辛勞時說的一句「辛苦了」的諧音。而這首歌的歌詞是沒有記載在唱片內的歌詞本。另外，其實在作曲作詞者的名字前也有所暗示，其是正是KinKi
Kids二人。KANZAI BOYA是組成KinKi
Kids之前的組合名字，RED就是代表光一，而BLUE就是代表剛。當然，這並不是說他們在KANZAI
BOYA時代以RED和BLUE的名字活動，那是一直以來兩人的代表顏色來。一直以來由二人共同創作的歌曲，皆是由剛負責作詞，光一負責作曲，這是第一首把組合逆轉的歌曲。亦因為這已經所謂的公開秘密，所以嚴格一點來說由KinKi
Kids二人共同創作的單曲收錄歌曲，包括這首在內共有4首。

## 名稱

  - 日語原名：**solitude**
  - 中文意思：**孤獨～真實的再見～**
  - [香港](../Page/香港.md "wikilink")[正東譯名](../Page/正東唱片.md "wikilink")：**solitude～真實的再見～**
  - [台灣](../Page/台灣.md "wikilink")[艾迴譯名](../Page/艾迴唱片.md "wikilink")：**solitude～真實的告別～**

## 收錄歌曲

### 初回版收錄歌曲

1.  **solitude～真實的告別～**（**solitude**）
      - ※堂本光一參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『Remote』（台譯：遙控刑警）主題曲。
      - 作曲、作詞：[K.Dino](../Page/堂本光一.md "wikilink")
      - 編曲：[ha-j](../Page/ha-j.md "wikilink")
2.  **太陽的門扇**（****）
      - 作曲：[谷本新](../Page/谷本新.md "wikilink")
      - 作詞：[淺田信一](../Page/淺田信一.md "wikilink")
      - 編曲：[白井良明](../Page/白井良明.md "wikilink")
3.  **5X9=63**
      - 作曲：[KANZAI BOYA BLUE](../Page/堂本剛.md "wikilink")
      - 作詞：[KANZAI BOYA RED](../Page/堂本光一.md "wikilink")
      - 編曲：[知野芳彥](../Page/知野芳彥.md "wikilink")

### 通常版收錄歌曲

1.  **solitude～真實的告別～**
2.  **太陽的門扇**
3.  **solitude～真實的告別～ (Instrumental)**

[Category:近畿小子歌曲](../Category/近畿小子歌曲.md "wikilink")
[Category:日本電視台週六連續劇主題曲](../Category/日本電視台週六連續劇主題曲.md "wikilink")
[Category:2002年單曲](../Category/2002年單曲.md "wikilink")
[Category:日劇學院賞最佳主題曲](../Category/日劇學院賞最佳主題曲.md "wikilink")
[Category:2002年Oricon單曲月榜冠軍作品](../Category/2002年Oricon單曲月榜冠軍作品.md "wikilink")
[Category:2002年Oricon單曲週榜冠軍作品](../Category/2002年Oricon單曲週榜冠軍作品.md "wikilink")