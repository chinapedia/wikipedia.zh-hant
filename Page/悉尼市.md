**悉尼市（雪梨市）**（[英語](../Page/英語.md "wikilink")：**City of
Sydney**）位於[澳洲](../Page/澳洲.md "wikilink")[新南威爾士州](../Page/新南威爾士州.md "wikilink")，由[悉尼商業中心區及附近的](../Page/悉尼商業中心區.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")[都會區的](../Page/都會區.md "wikilink")[內西區的郊區組成](../Page/內西區_\(悉尼\).md "wikilink")。悉尼市是一個[地方政府區域](../Page/地方政府區域.md "wikilink")，以往的地方政府區域**南悉尼市**於2004年2月6日正式併入該市。

合併前，悉尼市內的郊區包括現時的悉尼商業中心區、東面的[烏魯木魯](../Page/烏魯木魯_\(新南烕爾斯\).md "wikilink")（Woolloomooloo）、南面的[乾草市場](../Page/乾草市場_\(新南烕爾斯\).md "wikilink")（Haymarket）、西面的[派蒙](../Page/派蒙_\(新南烕爾斯\).md "wikilink")（Pyrmont）及[歐緹莫](../Page/歐緹莫_\(新南烕爾斯\).md "wikilink")（Ultimo）等等。

悉尼商業中心區被東面的[麥格理街](../Page/麥格理街_\(悉尼\).md "wikilink")、南面的[利物浦街](../Page/利物浦街_\(悉尼\).md "wikilink")（Liverpool
Street）、西面的西部幹道（Western
Distributor）、北面的[環形碼頭及](../Page/環形碼頭.md "wikilink")[悉尼港毗鄰環繞](../Page/悉尼港.md "wikilink")。合併前，南悉尼市內的郊區包括[亞歷山大](../Page/亞歷山大_\(新南烕爾斯\).md "wikilink")（Alexandria）、[達靈頓](../Page/達靈頓_\(新南烕爾斯\).md "wikilink")（Darlington，現時主要由[悉尼大學佔用](../Page/悉尼大學.md "wikilink")）、[厄斯金內威爾](../Page/厄斯金內威爾_\(新南烕爾斯\).md "wikilink")（Erskineville）、[新城](../Page/新城_\(新南烕爾斯\).md "wikilink")（Newtown）、[紅坊](../Page/紅坊_\(新南烕爾斯\).md "wikilink")（Redfern）、[紀聶](../Page/紀聶_\(新南烕爾斯\).md "wikilink")（Glebe）、[滑鐵盧](../Page/滑鐵盧_\(新南烕爾斯\).md "wikilink")（Waterloo）及[帕丁頓](../Page/帕丁頓_\(新南烕爾斯\).md "wikilink")（Paddington）。

## 悉尼市轄區

以下是悉尼市所轄的郊區：

| 辖区                                                                      | 英文              |
| ----------------------------------------------------------------------- | --------------- |
| [亞歷山大](../Page/亞歷山大_\(新南威尔士\).md "wikilink")                            | Alexandria      |
| [比肯斯菲爾德](../Page/比肯斯菲爾德_\(新南威尔士\).md "wikilink")                        | Beaconsfield    |
| [百老匯](../Page/百老匯_\(新南威尔士\).md "wikilink")                              | Broadway        |
| [齊賓泰爾](../Page/齊賓泰爾_\(新南威尔士\).md "wikilink")                            | Chippendale     |
| [達令赫斯特](../Page/達令赫斯特_\(新南威尔士\).md "wikilink")                          | Darlinghurst    |
| [达令顿](../Page/达令顿_\(新南威尔士\).md "wikilink")                              | Darlington      |
| [多維斯角](../Page/多維斯角_\(新南威尔士\).md "wikilink")                            | Dawes Point     |
| [伊麗莎白灣](../Page/伊麗莎白灣_\(新南威尔士\).md "wikilink")                          | Elizabeth Bay   |
| [厄斯金內威爾](../Page/厄斯金內威爾_\(新南威尔士\).md "wikilink")                        | Erskineville    |
| [伊夫利](../Page/伊夫利_\(新南威尔士\).md "wikilink")                              | Eveleigh        |
| [Forest Lodge](../Page/:en:Forest_Lodge,_New_South_Wales.md "wikilink") | Forest Lodge    |
| [紀聶](../Page/紀聶_\(新南威尔士\).md "wikilink")                                | Glebe           |
| [禧市](../Page/禧市（新南威尔士）.md "wikilink")                                   | Haymarket       |
| [米勒角](../Page/米勒角（新南威尔士）.md "wikilink")                                 | Millers Point   |
| [摩亞公園](../Page/摩亞公園_\(新南威尔士\).md "wikilink")                            | Moore Park      |
| [帕茲角](../Page/帕茲角_\(新南威尔士\).md "wikilink")                              | Potts Point     |
| [派蒙](../Page/派蒙_\(新南威尔士\).md "wikilink")                                | Pyrmont         |
| [紅坊](../Page/紅坊_\(新南威尔士\).md "wikilink")                                | Redfern         |
| [羅斯伯里](../Page/羅斯伯里_\(新南威尔士\).md "wikilink")                            | Rosebery        |
| [瑞西卡特灣](../Page/瑞西卡特灣_\(新南威尔士\).md "wikilink")                          | Rushcutters Bay |
| [莎梨山](../Page/莎梨山_\(新南威尔士\).md "wikilink")                              | Surry Hills     |
| [悉尼商業中心區](../Page/悉尼商業中心區.md "wikilink")                                | Sydney CBD      |
| [岩石區](../Page/岩石區_\(新南威尔士\).md "wikilink")                              | The Rocks       |
| [歐緹莫](../Page/歐緹莫_\(新南威尔士\).md "wikilink")                              | Ultimo          |
| [滑鐵盧](../Page/滑鐵盧_\(新南威尔士\).md "wikilink")                              | Waterloo        |
| [烏魯木魯](../Page/烏魯木魯_\(新南威尔士\).md "wikilink")                            | Woolloomooloo   |
| [泽特蘭](../Page/泽特蘭_\(新南威尔士\).md "wikilink")                              | Zetland         |

## 悉尼市內的地區

以下是悉尼市的地區：

  - [唐人街](../Page/悉尼唐人街.md "wikilink")
  - [環形碼頭](../Page/環形碼頭.md "wikilink")
  - [達令港](../Page/達令港_\(新南烕爾斯\).md "wikilink")（Darling Harbour）
  - [山羊島](../Page/山羊島_\(傑克遜港\).md "wikilink")（Goat Island）
  - [花園島](../Page/花園島_\(新南烕爾斯\).md "wikilink")（Garden Island）
  - [国王十字](../Page/国王十字_\(新南烕爾斯\).md "wikilink")（Kings Cross）
  - [麥克唐納城](../Page/麥克唐納城_\(新南烕爾斯\).md "wikilink")（Macdonaldtown）

## 歷史

[Lower_George_Street_Sydney_1828.jpg](https://zh.wikipedia.org/wiki/File:Lower_George_Street_Sydney_1828.jpg "fig:Lower_George_Street_Sydney_1828.jpg")\]\]

悉尼市1842年通過市政府法案（Corporation
Act）成立，範圍包括現時的烏魯木魯、莎梨山、[齊賓泰爾](../Page/齊賓泰爾_\(新南威爾斯\).md "wikilink")（Chippendale）和派蒙，[面積合共](../Page/面積.md "wikilink")11.65平方公-{里}-。邊界郵政設置了六個區域。邊境郵政今日仍見於[悉尼廣場前方](../Page/悉尼廣場.md "wikilink")。

1900年起，悉尼市的範圍多次變更。1909年，[坎普爾頓](../Page/坎普爾頓_\(新南威爾斯\).md "wikilink")（Camperdown）自治市與悉尼市合併。1949年，亞歷山大、達令頓、厄斯金內威爾、新城、紅坊、紀聶、滑鐵盧及帕丁頓併入。1968年，不少上述郊區納入新成立的[南悉尼自治市](../Page/南悉尼市.md "wikilink")。1982年，南悉尼市併入悉尼市，於1988年透過《悉尼市法案》再度獨立，其範圍縮減了6.19平方公-{里}-。2004年2月，悉尼市再度擴大，兩個議會區域合併；現時人口約有177,000人。

悉尼市是[悉尼和平獎的主要贊助者](../Page/悉尼和平獎.md "wikilink")。

## 政治

正如上文所述，自1945年起，悉尼市的選舉範圍在至少4個時刻，被州政府大力地改變，以為當時的新南威爾斯州議會的執政黨製造優勢。其後，[澳洲工黨和](../Page/澳洲工黨.md "wikilink")[澳洲自由黨接替](../Page/澳洲自由黨.md "wikilink")，兩大黨的政府先後重新規劃選舉的範圍，將傳統上的支持他們的內郊區編入，並將傳統上不支持的撇除。

面對悉尼議會的分裂狀況，[澳洲自由黨遂於](../Page/澳洲自由黨.md "wikilink")1987年重組，將南部的郊區合組成新的[南悉尼議會](../Page/南悉尼市.md "wikilink")。據說，這是要為自由黨製造優勢，因為南部郊區一直以來是工黨的票源。

2004年，[澳洲工黨州政府把悉尼市與](../Page/澳洲工黨.md "wikilink")[南悉尼市議會合併](../Page/南悉尼市.md "wikilink")，恢復原狀。有評論家指，這意味著已控制州政府的工黨有意建立「超級議會」掌握大權。議會合併後，選舉於2004年3月27日舉行。獨立候選人[克羅夫·摩爾](../Page/克洛弗·莫尔.md "wikilink")（Clover
Moore）擊敗倍受矚目的工黨候選人、前聯邦部長[李米高](../Page/李米高_\(澳洲政治家\).md "wikilink")（Michael
Lee）當選悉尼市[市長](../Page/市長.md "wikilink")。有評論家指，這顯示選民強烈反對工黨建立「超級議會」的意圖。

## 友好城市

為了促進[文化交流](../Page/文化.md "wikilink")、[貿易與](../Page/貿易.md "wikilink")[旅遊](../Page/旅遊.md "wikilink")，悉尼市議會與下列[城市建立了](../Page/城市.md "wikilink")[友好城市關係](../Page/友好城市.md "wikilink")：\[1\]

  - [三藩市](../Page/三藩市.md "wikilink")

  - [名古屋市](../Page/名古屋市.md "wikilink")

  - [威靈頓](../Page/威靈頓.md "wikilink")

  - [朴次茅斯](../Page/朴次茅斯.md "wikilink")

  - [多倫多](../Page/多倫多.md "wikilink")

  - [佛羅倫斯](../Page/佛羅倫斯.md "wikilink")

  - [馬尼拉](../Page/馬尼拉.md "wikilink")

## 参考文獻

## 外部連結

  - [悉尼市官方網站](http://www.cityofsydney.nsw.gov.au/)
  - [悉尼市的範圍及地圖](https://web.archive.org/web/20050317115520/http://www.dlg.nsw.gov.au/dlg/dlghome/dlg_Regions.asp?regiontype=2&slacode=7210&region=SI)

  - [悉尼節](http://www.sydneyfestival.org.au/)

{{-}}

[Sydney, City of](../Category/悉尼郊区.md "wikilink")
[悉尼市](../Category/悉尼市.md "wikilink")

1.  [1](http://www.cityofsydney.nsw.gov.au/Business/AwardsPrograms/SisterCityProgram.asp)