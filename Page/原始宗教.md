**原始宗教**（，或稱**自然宗教**或**自發宗教**），是史前人類[宗教信仰和](../Page/宗教信仰.md "wikilink")[習俗的總稱](../Page/習俗.md "wikilink")。更具體的，包括[舊石器時代](../Page/舊石器時代.md "wikilink")、[中石器時代](../Page/中石器時代.md "wikilink")、[新石器時代和](../Page/新石器時代.md "wikilink")[青銅時代的](../Page/青銅時代.md "wikilink")[宗教](../Page/宗教.md "wikilink")。西方[殖民主義興起後](../Page/殖民主義.md "wikilink")，殘存於近代[原始部落的](../Page/原始部落.md "wikilink")[原住民宗教受到嚴重衝擊](../Page/原住民.md "wikilink")，宗教之原始狀態與文明社會之接觸發生許多變形。原始宗教多表現為對[大自然的](../Page/大自然.md "wikilink")[崇拜](../Page/崇拜.md "wikilink")。

## 舊石器時代

[Gabillou_Sorcier.png](https://zh.wikipedia.org/wiki/File:Gabillou_Sorcier.png "fig:Gabillou_Sorcier.png")[多爾多涅](../Page/多爾多涅省.md "wikilink")[洞穴壁畫的半獸半人畫像](../Page/洞穴壁畫.md "wikilink")。考古學家認為這可能提供了早期[薩滿信仰的證據](../Page/薩滿.md "wikilink")。\]\]

特意地[埋葬](../Page/土葬.md "wikilink")，特別是帶著[陪葬品](../Page/陪葬品.md "wikilink")，可能是最早的可檢驗形式的宗教實踐之一（埋葬本身是[行為現代性的典型指標](../Page/行為現代性.md "wikilink")）。如[認知科學家菲利普](../Page/認知科學.md "wikilink")·利伯曼(Philip
Lieberman)所說，這可能意味著“對死者的關注超越了日常生活”\[1\]。

一些[考古學家指出](../Page/考古學.md "wikilink")，諸如[尼安德特人等](../Page/尼安德特人.md "wikilink")[舊石器時代中期社會可能已經形成](../Page/舊石器時代中期.md "wikilink")[圖騰或](../Page/圖騰崇拜.md "wikilink")。埃米爾·巴克勒（EmilBächler）特別提出（根據舊石器時代中期的考古學證據），舊石器時代中期的尼安德特人存在廣泛的[熊崇拜](../Page/熊崇拜.md "wikilink")。另外一項公元前7萬年關於[舊石器時代中期的動物崇拜證據](../Page/舊石器時代中期.md "wikilink")（[非洲](../Page/非洲.md "wikilink")[卡拉哈里沙漠的](../Page/卡拉哈里沙漠.md "wikilink")[措迪洛山](../Page/措迪洛山.md "wikilink")），已經被原始研究人員否認\[2\]\[3\]。
之後[舊石器時代晚期的動物崇拜](../Page/舊石器時代晚期.md "wikilink")，如[熊崇拜](../Page/熊崇拜.md "wikilink")，推測可能緣自這些[舊石器時代中期的動物崇拜](../Page/舊石器時代中期.md "wikilink")\[4\]。

舊石器時代晚期的動物崇拜與狩獵儀式交織在一起\[5\]。例如，和熊遺蹟的考古學證據顯示有一種犧牲熊的儀式，一隻熊被箭射進肺部致死，然後儀式地埋在一隻由熊毛皮覆蓋的粘土熊雕像附近，頭骨和熊體分開埋藏\[6\]。

## 新石器時代

[新石器時代仍未發現任何的文字記錄](../Page/新石器時代.md "wikilink")，因此關於新石器時代社會可能擁有的任何信仰系統的所有陳述，都是從[考古學中推測](../Page/考古學.md "wikilink")。法國考古學家表示，[新石器革命受到一個他稱為](../Page/新石器革命.md "wikilink")“符號革命”的重要主題影響，這表明在新石器時代誕生了“宗教”。他認為新石器時代的人類受到思想變化的影響，多過於環境變化的影響，並指出了這一過程的一系列階段\[7\]。他的研究透過審視描繪最早女性作為女神和公牛作為神的陶俑和早期藝術，提出了關於知覺和二元性等人類思維演化的重要概念\[8\]。

公元前5千年期間的被解釋為用於教育功能。的部份，發現了人類犧牲的遺骸。
這些結構中的許多具有與[至點日落或日出方位對齊的開口](../Page/至點.md "wikilink")，表明它們是維持[陰曆的一種方式](../Page/陰曆.md "wikilink")。
歐洲的[巨石文化的建設也從公元前](../Page/巨石文化.md "wikilink")5千年開始，持續整個新石器時代，而一些地區更已經進入[青銅器時代早期](../Page/青銅器時代.md "wikilink")。

先驅[瑪利亞·金布塔斯提出新石器時代的歐洲以圍繞著](../Page/瑪利亞·金布塔斯.md "wikilink")“女神崇拜”的“女性中心”社會概念。
之後隨著青銅器時代的到來，新石器時代的“母權”文化才被父權制取而代之。不過金布塔斯的觀點今天沒有受到廣泛的支持\[9\]。

## 青銅器時代

青銅器時代宗教包括了、、和。

### 重建

青銅器時代早期的（重建得到），和已經證實的，被推定是新石器時代晚期某些傳統的延續。

## 鐵器時代

雖然[地中海](../Page/地中海.md "wikilink")、[近東](../Page/近東.md "wikilink")、[印度和](../Page/印度.md "wikilink")[中國的鐵器時代宗教在文字記錄中得到很好的證明](../Page/中國.md "wikilink")，鐵器時代大部分的歐洲，從公元前700年到[大遷徙期間都落在史前時期](../Page/歐洲民族大遷徙.md "wikilink")。在希臘化和羅馬時代民族志記錄中，非地中海地區宗教習俗的敘述也相當少。

在北極圈宗教（和）、、[美洲原住民宗教和](../Page/美洲原住民宗教.md "wikilink")[大洋洲宗教](../Page/大洋洲宗教.md "wikilink")，史前時代大多只能以[近代早期和歐洲](../Page/近世.md "wikilink")[殖民主義結束](../Page/殖民主義.md "wikilink")。
這些傳統往往只能在[基督教化的背景下首次被記錄](../Page/基督教化.md "wikilink")。由於這些原因，對於許多地區鐵器時代宗教的詮釋和理解，主要仍依靠考古材料。

## 參見

### 宗教人類學

### 宗教考古學

### 崇拜與神祇

## 參考資料

[原始宗教](../Category/原始宗教.md "wikilink")

1.

2.  World's Oldest Ritual Discovered -- Worshipped The Python 70,000
    Years Ago The Research Council of Norway (2006/11/30).
    [ScienceDaily](http://www.sciencedaily.com/releases/2006/11/061130081347.htm)

3.

4.

5.
6.
7.  [Aurenche, Olivier., Jacques Cauvin et la préhistoire du Levant,
    Paléorient, Volume 27, Number 27-2,
    pp. 5-11, 2001.](http://www.persee.fr/web/revues/home/prescript/article/paleo_0153-9345_2001_num_27_2_4728)

8.

9.