****是历史上第八十九次航天飞机任务，也是[哥伦比亚号航天飞机的第二十二次太空飞行](../Page/哥倫比亞號太空梭.md "wikilink")。

## 任务成员

  - **[理查德·希尔佛斯](../Page/理查德·希尔佛斯.md "wikilink")**（，曾执行、以及任务），指令长
  - **[斯科特·阿尔特曼](../Page/斯科特·阿尔特曼.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[理查德·林奈](../Page/理查德·林奈.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[达迪德·莱斯·威廉姆斯](../Page/达迪德·莱斯·威廉姆斯.md "wikilink")**（，[加拿大宇航员](../Page/加拿大.md "wikilink")，曾执行任务），任务专家
  - **[凯瑟琳·海尔](../Page/凯瑟琳·海尔.md "wikilink")**（，曾执行任务），任务专家
  - **[杰·巴克利](../Page/杰·巴克利.md "wikilink")**（，曾执行任务），有效载荷专家
  - **[詹姆斯·帕维尔齐克](../Page/詹姆斯·帕维尔齐克.md "wikilink")**（，曾执行任务），有效载荷专家

### 替补有效载荷专家

  - **[亚历山大·邓拉普](../Page/亚历山大·邓拉普.md "wikilink")**（）
  - **[向井千秋](../Page/向井千秋.md "wikilink")**

[Category:1998年佛罗里达州](../Category/1998年佛罗里达州.md "wikilink")
[Category:1998年科學](../Category/1998年科學.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1998年4月](../Category/1998年4月.md "wikilink")
[Category:1998年5月](../Category/1998年5月.md "wikilink")