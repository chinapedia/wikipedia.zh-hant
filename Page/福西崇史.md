
**福西崇史**是[日本國家足球隊的](../Page/日本.md "wikilink")[中場球員](../Page/中場.md "wikilink")，曾效力[磐田山葉](../Page/磐田山葉.md "wikilink")、[FC東京等](../Page/FC東京.md "wikilink")，現已退役。他在[2004年亚洲杯足球赛決賽中日對壘賽事中首先射入第一個入球](../Page/2004年亚洲杯足球赛.md "wikilink")。

[Category:日本足球運動員](../Category/日本足球運動員.md "wikilink")
[Category:日本國家足球隊成員](../Category/日本國家足球隊成員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:日職球員](../Category/日職球員.md "wikilink")
[Category:磐田喜悦球員](../Category/磐田喜悦球員.md "wikilink")
[Category:FC東京球員](../Category/FC東京球員.md "wikilink")
[Category:东京日视1969球员](../Category/东京日视1969球员.md "wikilink")
[Category:愛媛縣出身人物](../Category/愛媛縣出身人物.md "wikilink")
[Category:1999年美洲盃球員](../Category/1999年美洲盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2004年亞洲盃足球賽球員](../Category/2004年亞洲盃足球賽球員.md "wikilink")
[Category:2005年洲際國家盃球員](../Category/2005年洲際國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:亞洲盃足球賽冠軍隊球員](../Category/亞洲盃足球賽冠軍隊球員.md "wikilink")