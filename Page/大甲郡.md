**大甲郡**為[台灣日治時期的行政區劃之一](../Page/台灣日治時期.md "wikilink")。該郡隸屬[台中州](../Page/臺中州.md "wikilink")。

## 簡介

大甲郡役所設於[清水街](../Page/清水街.md "wikilink")\[1\]，為今日[華南銀行清水分行現址](../Page/華南銀行.md "wikilink")。大甲郡管轄[大甲街](../Page/大甲街.md "wikilink")、[清水街](../Page/清水街.md "wikilink")、[梧棲街](../Page/梧棲街.md "wikilink")、[沙鹿街](../Page/沙鹿街.md "wikilink")、[外埔-{庄}-](../Page/外埔庄.md "wikilink")、[大安-{庄}-](../Page/大安庄.md "wikilink")、[龍井-{庄}-](../Page/龍井庄.md "wikilink")、[大肚-{庄}-](../Page/大肚庄.md "wikilink")。

轄域即今[台中市](../Page/臺中市.md "wikilink")[大甲區](../Page/大甲區.md "wikilink")、[清水區](../Page/清水區_\(臺灣\).md "wikilink")、[梧棲區](../Page/梧棲區.md "wikilink")、[沙鹿區](../Page/沙鹿區.md "wikilink")、[外埔區](../Page/外埔區.md "wikilink")、[大安區](../Page/大安區_\(臺中市\).md "wikilink")、[龍井區](../Page/龍井區.md "wikilink")、[大肚區](../Page/大肚區.md "wikilink")，今日俗稱**臺中海線地區**。

## 統計

<table>
<caption>1942年本籍國籍別常住戶口[2]</caption>
<thead>
<tr class="header">
<th style="text-align: center;"><p>街-{庄}-名</p></th>
<th style="text-align: center;"><p>面積<br />
(km²)</p></th>
<th style="text-align: center;"><p>人口 (人)</p></th>
<th style="text-align: center;"><p>人口密度<br />
(人/km²)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>本籍</p></td>
<td style="text-align: center;"><p>國籍</p></td>
<td style="text-align: center;"><p>計</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>臺灣</p></td>
<td style="text-align: center;"><p>內地</p></td>
<td style="text-align: center;"><p>朝鮮</p></td>
<td style="text-align: center;"><p>中華民國</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>清水街</p></td>
<td style="text-align: center;"><p>64.1709</p></td>
<td style="text-align: center;"><p>38,418</p></td>
<td style="text-align: center;"><p>812</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>梧棲街</p></td>
<td style="text-align: center;"><p>16.6049</p></td>
<td style="text-align: center;"><p>15,321</p></td>
<td style="text-align: center;"><p>857</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>大甲街</p></td>
<td style="text-align: center;"><p>58.4534</p></td>
<td style="text-align: center;"><p>30,544</p></td>
<td style="text-align: center;"><p>461</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>沙鹿街</p></td>
<td style="text-align: center;"><p>40.4604</p></td>
<td style="text-align: center;"><p>23,708</p></td>
<td style="text-align: center;"><p>317</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>外埔-{庄}-</p></td>
<td style="text-align: center;"><p>42.4099</p></td>
<td style="text-align: center;"><p>12,405</p></td>
<td style="text-align: center;"><p>73</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>大安-{庄}-</p></td>
<td style="text-align: center;"><p>27.4045</p></td>
<td style="text-align: center;"><p>12,600</p></td>
<td style="text-align: center;"><p>50</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>龍井-{庄}-</p></td>
<td style="text-align: center;"><p>38.2487</p></td>
<td style="text-align: center;"><p>18,083</p></td>
<td style="text-align: center;"><p>69</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>大肚-{庄}-</p></td>
<td style="text-align: center;"><p>37.0024</p></td>
<td style="text-align: center;"><p>15,312</p></td>
<td style="text-align: center;"><p>489</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>大甲郡</p></td>
<td style="text-align: center;"><p>324.7551</p></td>
<td style="text-align: center;"><p>166,391</p></td>
<td style="text-align: center;"><p>3,128</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 後續發展

1938年（日治[昭和](../Page/昭和.md "wikilink")13年），[台灣總督府發布](../Page/台灣總督府.md "wikilink")「梧棲築港計劃」，計畫在梧棲街開闢「**[新高港](../Page/新高港.md "wikilink")**」。1941年公佈「新高港都市建設計劃方案」，計畫以梧棲街為中心合併鄰近街庄組成「[新高市](../Page/新高市.md "wikilink")」，至1944年因戰爭影響，此行政區劃並未實施。

1945年3月[重慶國民政府通過之](../Page/重慶國民政府.md "wikilink")[台灣接管計劃綱要地方政制中](../Page/台灣接管計劃綱要地方政制.md "wikilink")，將大甲郡改制為**大甲縣**，為該地方政制的30個[縣之一](../Page/縣.md "wikilink")，將[台灣總督府規劃中](../Page/台灣總督府.md "wikilink")**新高市**改制為**梧棲市**，為該地方政制的12個市之一。

1945年10月，因[台灣省行政長官公署認為該政制不符實際](../Page/台灣省行政長官公署.md "wikilink")，因此「台灣接管計劃地方政制」並未實施，僅將**州廳－郡－街、庄**各級行政區改稱**縣－區－鎮、鄉**，大甲郡改稱**大甲區**。

## 歷任首長

### 郡守

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>出身地</p></th>
<th><p>學歷</p></th>
<th><p>任期</p></th>
<th><p>經歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/齋藤透.md" title="wikilink">齋藤透</a></p></td>
<td><p><a href="../Page/石川縣.md" title="wikilink">石川縣</a></p></td>
<td></td>
<td><p>1922年7月－1923年7月</p></td>
<td><p>後調任<a href="../Page/屏東郡.md" title="wikilink">屏東郡守</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小笠原敬太郎.md" title="wikilink">小笠原敬太郎</a></p></td>
<td><p><a href="../Page/島根縣.md" title="wikilink">島根縣</a></p></td>
<td></td>
<td><p>1924年</p></td>
<td><p>短期代理</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/池田莊太郎.md" title="wikilink">池田莊太郎</a></p></td>
<td><p><a href="../Page/福岡縣.md" title="wikilink">福岡縣</a></p></td>
<td></td>
<td><p>1924年7月</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/增永吉次郎.md" title="wikilink">增永吉次郎</a></p></td>
<td><p><a href="../Page/鹿兒島縣.md" title="wikilink">鹿兒島縣</a></p></td>
<td><p><a href="../Page/台灣總督府國語學校.md" title="wikilink">台灣總督府國語學校</a></p></td>
<td><p>1925年7月－1926年7月</p></td>
<td><p>後升任<a href="../Page/澎湖廳長.md" title="wikilink">澎湖廳長</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/杉山靖憲.md" title="wikilink">杉山靖憲</a></p></td>
<td><p><a href="../Page/東京府.md" title="wikilink">東京府</a></p></td>
<td></td>
<td><p>1926年7月－1930年8月</p></td>
<td><p>原任<a href="../Page/澎湖郡守.md" title="wikilink">澎湖郡守</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮野為長.md" title="wikilink">宮野為長</a></p></td>
<td><p><a href="../Page/香川縣.md" title="wikilink">香川縣</a></p></td>
<td><p>香川縣五板出商業學校</p></td>
<td><p>1931年8月－1933年8月</p></td>
<td><p>原任<a href="../Page/大溪郡.md" title="wikilink">大溪郡守</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/滿富俊美.md" title="wikilink">滿富俊美</a></p></td>
<td><p><a href="../Page/宮崎縣.md" title="wikilink">宮崎縣</a></p></td>
<td><p>宮崎縣立宮崎農校畢業<br />
高文行政科及格</p></td>
<td><p>1934年9月－1936年12月</p></td>
<td><p>原任交通局副參事，後調任總督府參事官</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/橫山竹男.md" title="wikilink">橫山竹男</a></p></td>
<td><p>香川縣</p></td>
<td><p>通信傳習所</p></td>
<td><p>1936年12月26日－1939年6月</p></td>
<td><p>原任<a href="../Page/台南市_(州轄市).md" title="wikilink">台南市助役</a>，後調任<a href="../Page/基隆市長.md" title="wikilink">基隆市尹</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大井又次.md" title="wikilink">大井又次</a></p></td>
<td><p><a href="../Page/佐賀縣.md" title="wikilink">佐賀縣</a></p></td>
<td><p>佐賀縣立佐賀中學</p></td>
<td><p>1939年7月－1941年1月</p></td>
<td><p>原任<a href="../Page/蘇澳郡.md" title="wikilink">蘇澳郡守</a>，後調任<a href="../Page/彰化市.md" title="wikilink">彰化市長</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/富中市藏.md" title="wikilink">富中市藏</a></p></td>
<td><p>鹿兒島縣</p></td>
<td></td>
<td><p>1941年7月</p></td>
<td><p>原任<a href="../Page/潮州郡.md" title="wikilink">潮州郡守</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/渡部政鬼.md" title="wikilink">渡部政鬼</a></p></td>
<td><p><a href="../Page/福島縣.md" title="wikilink">福島縣</a></p></td>
<td></td>
<td><p>1942年4月－1944年1月</p></td>
<td><p>原任<a href="../Page/斗六郡.md" title="wikilink">斗六郡守</a></p></td>
</tr>
</tbody>
</table>

### 區長

## 注釋

<references/>

## 參考文獻

  - 施雅軒，《台灣的行政區變遷》，2003年，台北，遠足文化出版社。

[Category:台中州](../Category/台中州.md "wikilink")
[大甲郡](../Category/大甲郡.md "wikilink")

1.  [為什麼不是清水郡而是大甲郡？](http://blog.roodo.com/purebox/archives/20967338.html)
2.