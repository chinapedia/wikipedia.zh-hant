**[九龍巴士](../Page/九龍巴士.md "wikilink")259B線**是[香港一條由](../Page/香港.md "wikilink")[屯門碼頭單向前往](../Page/屯門碼頭.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")
(中間道)的巴士路線，只在平日上午繁忙時間行駛。

另設由新屯門中心開出的輔助線**259C線**，同樣只在平日上午繁忙時間行駛。

## 歷史

  - 259B

<!-- end list -->

  - 1991年7月15日：路線投入服務，初時以[尖沙咀碼頭巴士總站作終站](../Page/尖沙咀天星碼頭公共運輸交匯處.md "wikilink")
  - 1992年6月1日：改經[西九龍走廊](../Page/西九龍走廊.md "wikilink")，不經長沙灣道
  - 1999年2月8日：繞經龍門居巴士總站
  - 2001年10月22日：尖沙咀總站縮短至[彌敦道近](../Page/彌敦道.md "wikilink")[中間道](../Page/中間道.md "wikilink")
  - 2007年10月15日：總站由尖沙咀中間道遷往[九龍站](../Page/九龍站_\(港鐵\).md "wikilink")，不再停美孚，並增設下午回程服務，回程繞經[安定及](../Page/安定邨.md "wikilink")[友愛](../Page/友愛邨.md "wikilink")
  - 2013年9月28日：取消星期六的服務。
  - 2015年12月28日：實施以下改動\[1\]：
      - 去程班次總站縮短至尖沙咀彌敦道近中間道；
      - 原有07:33之班次改為07:35開出；
      - 繞經屯青里，並由該站起增設分段收費；
      - 取消下午繁忙時間回程服務。

<!-- end list -->

  - 259C

<!-- end list -->

  - 1991年7月15日：路線投入服務，初時以[尖沙咀碼頭作終站](../Page/尖沙咀碼頭.md "wikilink")
  - 1992年6月1日：改經[西九龍走廊](../Page/西九龍走廊.md "wikilink")，不經長沙灣道
  - 1999年2月8日：繞經龍門居巴士總站
  - 2001年10月22日：配合[尖沙咀碼頭巴士總站行人道擴建工程](../Page/尖沙咀天星碼頭公共運輸交匯處.md "wikilink")，縮短至[尖沙咀](../Page/尖沙咀.md "wikilink")[彌敦道近](../Page/彌敦道.md "wikilink")[中間道](../Page/中間道.md "wikilink")
  - 2013年9月28日：取消星期六的服務。
  - 2015年12月28日：削減至2班車。\[2\]

自2010年1月9日[261B不再停靠美孚後](../Page/九龍巴士261B線.md "wikilink")，本路線是唯一一條停靠美孚巴士站的由新界西北開往尖沙咀的早晨特別線。

## 服務時間及班次

### 259B

  - 屯門碼頭開：

<!-- end list -->

  - 星期一至五：07:20、07:35、07:45

<!-- end list -->

  -
    星期六、日及公眾假期不設服務

### 259C

  - 新屯門中心開：

<!-- end list -->

  - 星期一至五：07:20、07:55

<!-- end list -->

  -
    星期六、日及公眾假期不設服務

## 收費

### 259B

全程：$14.3

  - [新屯門中心往](../Page/新屯門中心.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")：$13.7
  - [荔枝角道往](../Page/荔枝角道.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")：$5.1

### 259C

全程：$13.7

  - [美孚往尖沙咀](../Page/美孚.md "wikilink")：$6.2
  - [荔枝角道往尖沙咀](../Page/荔枝角道.md "wikilink")：$5.1

## 使用車輛

### 259B

由[58M](../Page/九龍巴士58M線.md "wikilink")、[59M及](../Page/九龍巴士59M線.md "wikilink")[59X抽調車輛行走](../Page/九龍巴士59X線.md "wikilink")。

### 259C

由[59X和](../Page/九龍巴士59X線.md "wikilink")[259E抽調車輛行走](../Page/九龍巴士259E線.md "wikilink")。

## 行車路線

**經**：[湖翠路](../Page/湖翠路.md "wikilink")\*、[龍門路](../Page/龍門路.md "wikilink")\*、[屯青里](../Page/屯青里.md "wikilink")、龍門路、D4路、[龍門公共運輸交匯處](../Page/龍門公共運輸交匯處.md "wikilink")、D4路、[皇珠路](../Page/皇珠路.md "wikilink")、[屯門公路](../Page/屯門公路.md "wikilink")、[荃灣路](../Page/荃灣路.md "wikilink")、[葵涌道](../Page/葵涌道.md "wikilink")、[荔枝角道](../Page/荔枝角道.md "wikilink")^、[西九龍走廊](../Page/西九龍走廊.md "wikilink")、[太子道西](../Page/太子道西.md "wikilink")、荔枝角道、[彌敦道](../Page/彌敦道.md "wikilink")。

259C線不經有\*之道路，並加經有^之道路。

### 沿線車站

[KMB259B_KlnBound_RtMap.png](https://zh.wikipedia.org/wiki/File:KMB259B_KlnBound_RtMap.png "fig:KMB259B_KlnBound_RtMap.png")

| [屯門碼頭開](../Page/屯門碼頭.md "wikilink") |
| ----------------------------------- |
| **序號**                              |
| 1\*                                 |
| 2\*                                 |
| 3\*                                 |
| 4                                   |
| 5                                   |
| 6                                   |
| 7                                   |
| ^                                   |
| 8                                   |
| 9                                   |
| 10                                  |
| 11                                  |
| 12                                  |
| 13                                  |
| 14                                  |

259C線不停有\*之車站，並加停有^之車站。

## 參考資料

  - 《巴士路線發展綱要(2)──荃灣．屯門．元朗》，BSI(香港)，ISBN 9628414720003，174頁

  - [九巴259B線—i-busnet.com](http://www.i-busnet.com/busroute/kmb/kmbr259b.php)

  - [九巴259B線—681巴士總站](http://www.681busterminal.com/259b.html)

  - [九巴259C線—i-busnet.com](http://www.i-busnet.com/busroute/kmb/kmbr259c.php)

  - [九巴259C線—681巴士總站](http://www.681busterminal.com/259c.html)

  -
  -
## 外部連結

  - [九巴259B線—九龍巴士](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=259B)

[259B](../Category/九龍巴士路線.md "wikilink")
[259B](../Category/屯門區巴士路線.md "wikilink")
[259B](../Category/油尖旺區巴士路線.md "wikilink")

1.  [259B由屯門碼頭開出之班次將修改部份行車路線並改以尖沙咀為總站，同時下午由九龍鐵路站開往屯門碼頭的班次亦會取消](http://kmb.hk/loadImage.php?page=1449738052_4504_0.jpg)
    ，九巴乘客通告，2015-12-11。
2.  [修改開車時間](http://kmb.hk/loadImage.php?page=1449737559_3318_0.jpg)
    ，九巴乘客通告，2015-09-21