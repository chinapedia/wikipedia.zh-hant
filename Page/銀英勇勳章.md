**銀英勇勳章**（，縮寫MBS）是[香港授勳及嘉獎制度的其中一種勳章](../Page/香港授勳及嘉獎制度.md "wikilink")。自1999年起開始頒授。獲頒授銀英勇勳章的人士英勇非凡，堪稱典範。

## 授勳名單

### 1999年

  - [班立志先生](../Page/班立志.md "wikilink")
  - 張志明先生
  - 陳超明先生
  - [劉惠恩先生](../Page/劉惠恩.md "wikilink")
  - [譚少雄先生](../Page/譚少雄.md "wikilink")

### 2000年

  - [李誌華先生](../Page/李誌華.md "wikilink")
  - [蔡濤先生](../Page/蔡濤.md "wikilink")

### 2001年

  - [張日銘先生](../Page/張日銘.md "wikilink")
  - [陶志明先生](../Page/陶志明.md "wikilink")
  - [彭玉華先生](../Page/彭玉華.md "wikilink")
  - [蔡汝成先生](../Page/蔡汝成.md "wikilink")
  - [鄭錦華先生](../Page/鄭錦華.md "wikilink")

### 2002年

  - [梁成恩先生](../Page/梁成恩.md "wikilink")（追授）

### 2003年

  - [王庚娣女士](../Page/王庚娣.md "wikilink")（追授）
  - [劉永佳先生](../Page/劉永佳.md "wikilink")（追授）
  - [劉錦蓉女士](../Page/劉錦蓉.md "wikilink")（追授）
  - [鄭夏恩醫生](../Page/鄭夏恩.md "wikilink")（追授）
  - [鄧香美女士](../Page/鄧香美.md "wikilink")（追授）
  - [梁寶明先生](../Page/梁寶明.md "wikilink")（追授）
  - [許志樑先生](../Page/許志樑.md "wikilink")
  - [吳嘉文先生](../Page/吳嘉文.md "wikilink")

### 2004年

  - [葉子恆先生](../Page/葉子恆.md "wikilink")

### 2005年

  - [黃漢先生](../Page/黃漢.md "wikilink")
  - [譚堡明先生](../Page/譚堡明.md "wikilink")

### 2007年

  - [陳志培機長](../Page/陳志培.md "wikilink")
  - [胡偉雄機長](../Page/胡偉雄.md "wikilink")
  - [陳兆基先生](../Page/陳兆基.md "wikilink")
  - [翟海亮先生](../Page/翟海亮.md "wikilink")
  - [李旭華先生](../Page/李旭華.md "wikilink")
  - [李國明先生](../Page/李國明.md "wikilink")

### 2014年

  - [翟海亮先生](../Page/翟海亮.md "wikilink")
  - [袁家威先生](../Page/袁家威.md "wikilink")

### 2016年

  - [尹建偉先生](../Page/尹建偉.md "wikilink")

### 2018年

  - [李栢強先生](../Page/李栢強.md "wikilink")
  - [黃瑞其先生](../Page/黃瑞其.md "wikilink")
  - [蘇智榮先生](../Page/蘇智榮.md "wikilink")
  - [蘇嘉祥先生](../Page/蘇嘉祥.md "wikilink")

{{-}}

[Category:香港授勳及嘉獎制度](../Category/香港授勳及嘉獎制度.md "wikilink")
[Category:1999年建立的獎項](../Category/1999年建立的獎項.md "wikilink")
[獲頒授香港銀英勇勳章者](../Category/獲頒授香港銀英勇勳章者.md "wikilink")