**黃曉飛**，[中國作曲家](../Page/中國.md "wikilink")、指揮家，現任[北京](../Page/北京.md "wikilink")[中國音樂學院教授](../Page/中國音樂學院.md "wikilink")。

她從北京[中央音樂學院作曲系畢業後](../Page/中央音樂學院.md "wikilink")，先是留在母校任教，後曾任[湖北省歌舞團作曲](../Page/湖北省.md "wikilink")、指揮，中國電影樂團客席指揮、[東方歌舞團客席指揮](../Page/東方歌舞團.md "wikilink")。

1992年起擔任中國音樂學院正教授。

1994年到[台灣](../Page/台灣.md "wikilink")[台北](../Page/台北.md "wikilink")[中國文化大學中國音樂學系擔任客座教授](../Page/中國文化大學.md "wikilink")，6月轉任高雄市實驗國樂團（現在的高雄市國樂團）客席指揮，1995年又獲聘擔任高雄市實驗國樂團客席指揮，1997年再任高雄市實驗國樂團客席指揮。

1998年到1999年出任高雄市實驗國樂團1年期的駐團客席指揮。

2002年和2003年這2年在台灣[台南縣](../Page/台南縣.md "wikilink")[官田鄉國立台南藝術學院](../Page/官田區.md "wikilink")（2004年8月1日改[國立台南藝術大學](../Page/國立台南藝術大學.md "wikilink")）中國音樂學系擔任客座教授。

譜曲作品有《青年鋼琴協奏曲》（與[劉詩昆](../Page/劉詩昆.md "wikilink")、孫亦林、潘一鳴合作，1958年，[中華人民共和國第](../Page/中華人民共和國.md "wikilink")1部[鋼琴音樂大型作品](../Page/鋼琴.md "wikilink")，參見向乾坤〈毛泽东《同音乐工作者的谈话》与当代中国音乐发展的方向〉1文
）、雙千金板胡高音[板胡協奏曲](../Page/板胡.md "wikilink")《白毛女敘事曲》、《紅色娘子軍組曲》、[二胡協奏曲](../Page/二胡.md "wikilink")《愛河之春》（台灣[高雄市國樂團委約創作](../Page/高雄市國樂團.md "wikilink")）、《車鼓迴旋曲》（使用台灣[車鼓](../Page/車鼓.md "wikilink")、[歌仔音樂素材和特性樂器](../Page/歌仔.md "wikilink")[殼仔弦](../Page/殼仔弦.md "wikilink")、[大廣弦](../Page/大廣弦.md "wikilink")、台灣[月琴](../Page/月琴.md "wikilink")，台灣[台南市立民族管絃樂團委約創作](../Page/台南市立民族管絃樂團.md "wikilink")，2003年4月26日在台灣[台南市世界首演](../Page/台南市.md "wikilink")）、[馬頭琴協奏曲](../Page/馬頭琴.md "wikilink")《草原風情》（與[丁魯峰合作](../Page/丁魯峰.md "wikilink")）、馬頭琴協奏曲《憶》（與[丁魯峰合作](../Page/丁魯峰.md "wikilink")）等。

2002年5月9日，她指揮台南藝術學院國樂系學生樂團在[台北市台灣](../Page/台北市.md "wikilink")[國家音樂廳舉行](../Page/國家音樂廳.md "wikilink")《樣板戲音樂選粹》音樂會，親自指揮自己創作的《白毛女敘事曲》（雙千金板胡高音板胡協奏曲，取材自現代芭蕾舞劇《[白毛女](../Page/白毛女.md "wikilink")》音樂）、
《紅色娘子軍組曲》（與[王竹林合作](../Page/王竹林.md "wikilink")，取材自現代[芭蕾舞劇](../Page/芭蕾舞.md "wikilink")《[紅色娘子軍](../Page/紅色娘子軍.md "wikilink")》音樂）、《沙家浜選段》（[琵琶與樂隊](../Page/琵琶.md "wikilink")，與[曹文工合作](../Page/曹文工.md "wikilink")，取材自現代[京劇](../Page/京劇.md "wikilink")《[沙家浜](../Page/沙家浜.md "wikilink")》音樂）等作品，獨奏家由客座教授[俞遜發](../Page/俞遜發.md "wikilink")（[笛](../Page/笛.md "wikilink")）和專任教授[湯良興](../Page/湯良興.md "wikilink")（琵琶）、[丁魯峰](../Page/丁魯峰.md "wikilink")（雙千金板胡高音[板胡](../Page/板胡.md "wikilink")）擔任。（首演是2002年5月3日在[台南市](../Page/台南市.md "wikilink")[國立成功大學](../Page/國立成功大學.md "wikilink")，2002年5月13日的最後1場在[高雄市](../Page/高雄市.md "wikilink")），俞遜發演奏的《大紅棗之歌》（與[箏重奏](../Page/箏.md "wikilink")，取材自現代芭蕾舞劇《白毛女》音樂）、《打虎上山》（[箏重奏](../Page/箏.md "wikilink")，取材自現代京劇《[智取威虎山](../Page/智取威虎山.md "wikilink")》音樂）也是她的創作。

## 外部連結

  - [中國音樂學院國樂系黃曉飛網頁](http://www.ccmusic.edu.cn/jxjg/yxsz/qyx/jsjl/gyxhxf/)

[Category:中国音乐家](../Category/中国音乐家.md "wikilink")
[Category:中国指挥家](../Category/中国指挥家.md "wikilink")
[Category:中国作曲家](../Category/中国作曲家.md "wikilink")