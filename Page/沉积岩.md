[Limestoneshale7342.jpg](https://zh.wikipedia.org/wiki/File:Limestoneshale7342.jpg "fig:Limestoneshale7342.jpg")[田纳西州的石灰石性质的](../Page/田纳西州.md "wikilink")[页岩层理结构](../Page/页岩.md "wikilink")\]\]
[Triassic_Utah.JPG](https://zh.wikipedia.org/wiki/File:Triassic_Utah.JPG "fig:Triassic_Utah.JPG")西南部的沉積岩\]\]
**沉积岩**，在有[水循环的星球上又称为](../Page/水循环.md "wikilink")**水成岩**，是三种组成[地球](../Page/地球.md "wikilink")[岩石圈的主要](../Page/岩石圈.md "wikilink")[岩石之一](../Page/岩石.md "wikilink")（另外两种是[岩浆岩和](../Page/岩浆岩.md "wikilink")[变质岩](../Page/变质岩.md "wikilink")）。

沉积岩是在[地表不太深的地方](../Page/地表.md "wikilink")，将其他[岩石的](../Page/岩石.md "wikilink")[风化产物和一些](../Page/风化.md "wikilink")[火山喷发物](../Page/火山.md "wikilink")，经过水流或[冰川的搬运](../Page/冰川.md "wikilink")、沉积、成岩作用形成的岩石。在[地球地表](../Page/地球.md "wikilink")，有70%的岩石是沉积岩組成的，但如果从地球表面到16公里深的整个[岩石圈算](../Page/岩石圈.md "wikilink")，沉积岩體積只占5%，因此沉积岩是构成地壳表层的主要岩石。沉积岩主要包括有[石灰岩](../Page/石灰岩.md "wikilink")、[砂岩](../Page/砂岩.md "wikilink")、[页岩等](../Page/页岩.md "wikilink")。沉积岩中所含有的[矿产极为丰富](../Page/矿产.md "wikilink")，占全部世界[矿产蕴藏量的](../Page/矿产.md "wikilink")80%。

沉积岩特征是有层理，某些含有动植物[化石](../Page/化石.md "wikilink")，所以可以断定其[地质年代](../Page/地质年代.md "wikilink")。相較於[火成岩及](../Page/火成岩.md "wikilink")[變質岩](../Page/變質岩.md "wikilink")，沉積岩中的化石所受破壞較少，也較易完整保存，因此對[考古學來說是十分重要的研究目標](../Page/考古學.md "wikilink")。

## 成因

风化的[岩石颗粒](../Page/岩石.md "wikilink")，经[大气](../Page/大气.md "wikilink")、[水流](../Page/水.md "wikilink")，到一定地点沉积下来，受到高压的[成岩作用](../Page/成岩作用.md "wikilink")，逐渐形成岩石。沉积岩保留了许多地球的[历史信息](../Page/历史.md "wikilink")，包括有古代[动](../Page/动物.md "wikilink")[植物](../Page/植物.md "wikilink")[化石](../Page/化石.md "wikilink")，沉积岩的层理有[地球](../Page/地球.md "wikilink")[气候](../Page/气候.md "wikilink")[环境变化的信息](../Page/环境.md "wikilink")。

  - 風化侵蝕：在河流上、中游兩岸的大岩石，經年累月被侵蝕、風化或直接被河水切割後，逐漸崩解成碎塊或更小的沙、泥碎屑。
  - 搬運：崩解、被切割下來的岩石碎屑，被河水從上游往下游搬移。
  - 堆積：河流的流速向下游逐漸減緩，搬運能力也越來越小，因此，河中岩石碎屑便一路沉積下來。由上游、下游到海底，分別是「礫石層」、「砂層」、「泥層」。此外，由珊瑚、藻類等生物所堆積形成的，便是「珊瑚礁石層」。
  - 壓密：由於新的層積物不斷堆壓在舊的上面，時間一久，底下的沉積物便會被壓得緊密結實。
  - 膠結：密實的沉積物依然有許多孔隙。當地下水流經這些孔隙，帶來的礦物質將孔隙一一填滿，同時將岩石碎屑顆粒緊緊膠黏再一起。

## 构造

### 层理构造

层理构造是由沉积物的成分或粒徑大小的结构沿垂直于沉积物表面（层面）方向及側向延伸变化而显示出来的一种层状构造。层理构造是与[岩浆岩](../Page/岩浆岩.md "wikilink")、[变质岩区别的重要标志](../Page/变质岩.md "wikilink")。常见的有：

  - **水平层理**：由一系列与层面平行的细层组成的层理；一般形成于平静的或微弱流动的水环境中。
  - **斜理层理**：由一系列与层面斜交的细层组成的层理；一般是在单向水流（或风）的作用下形成的，常见于河床沉积物中。
  - **交错层理**：有些斜层理与原来生成的斜层理呈一定角度相交，相互交错（切蝕）而形成的。
  - **递变层理**：同一层内碎屑颗粒粒径向上逐渐变细；它的形成常常是因沉积作用发生在运动的水介质中，其动力由强逐渐减弱。

### 层面构造

  - **波痕**：在还没有固结的沉积层面上，由于流水、风或波浪的作用形成的波浪起伏的表面，经过成岩作用保存下来。可指示流水方向與地層上下方向。
  - **泥裂**：没有固结的沉积物露出水面干涸时，经过脱水收缩干裂而形成的裂缝。可用來指示地層上下方向。

## 分类

### 碎屑沉积岩

是从其他[岩石的碎屑沉积形成的](../Page/岩石.md "wikilink")，包括有[长石](../Page/长石.md "wikilink")，[闪石](../Page/闪石.md "wikilink")，[火山喷出物](../Page/火山.md "wikilink")，[黏土](../Page/黏土.md "wikilink")，以及[变质岩的碎屑](../Page/变质岩.md "wikilink")，碎屑的大小不同形成的[岩石同](../Page/岩石.md "wikilink")，形成[页岩的碎屑小于](../Page/页岩.md "wikilink")0.0039
mm，形成[粉砂岩的碎屑在](../Page/粉砂岩.md "wikilink")0.0039 至 0.0625 mm
之间，形成[砂岩的碎屑则有](../Page/砂岩.md "wikilink")0.0625 到
2 mm之間，形成[砾岩的碎屑则有](../Page/砾岩.md "wikilink")2 到 256 mm。

沉积岩的分类不仅根据其形成颗粒的大小，还要考虑到组成颗粒的化学成分，形成的条件等因素。颗粒形成的条件，是被[冰](../Page/冰.md "wikilink")、[水](../Page/水.md "wikilink")、[温度变化将](../Page/温度.md "wikilink")[岩石碎裂](../Page/岩石.md "wikilink")，也有是由于[化学作用](../Page/化学.md "wikilink")，如[淋溶再](../Page/淋溶.md "wikilink")[析出等](../Page/析出.md "wikilink")。在搬运过程中，颗粒体积进一步变小，最终在一个新地点沉积成岩。

### 生物沉积岩

是由[生物体的堆积造成的](../Page/生物.md "wikilink")，如[花粉](../Page/花粉.md "wikilink")、[孢子](../Page/孢子.md "wikilink")、[贝壳](../Page/贝壳.md "wikilink")、[珊瑚等大量堆积](../Page/珊瑚.md "wikilink")，经过[成岩作用形成的](../Page/成岩作用.md "wikilink")

一般认为，[地球大气中的含碳量之所以相对其他](../Page/地球.md "wikilink")[行星如](../Page/行星.md "wikilink")[金星要低](../Page/金星.md "wikilink")，就是因为被[石灰岩等沉积岩固定](../Page/石灰岩.md "wikilink")。形成[石灰岩的](../Page/石灰岩.md "wikilink")[碳和](../Page/碳.md "wikilink")[钙都能在](../Page/钙.md "wikilink")[生物系统中循环](../Page/生物.md "wikilink")。

### 化学沉积岩

由各种[溶解物质或](../Page/溶解.md "wikilink")[胶体物质沉淀而成](../Page/胶体.md "wikilink")，某种化学成分沉淀后，在一定条件下常同时结晶。

[岩盐](../Page/岩盐.md "wikilink")、[石膏等](../Page/石膏.md "wikilink")。

[Category:地质学](../Category/地质学.md "wikilink")
[\*](../Category/水成岩.md "wikilink")