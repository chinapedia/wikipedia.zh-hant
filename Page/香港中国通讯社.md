**香港中国通讯社**（），簡稱**中通社**、**CNA**，于1956年11月13日在[香港注册成立](../Page/香港.md "wikilink")，以註冊日起計是香港历史最悠久的[通讯社](../Page/通讯社.md "wikilink")，1986年4月1日起向华文傳媒同时提供文字和图片，2005年8月起發報圖片新聞稿，以報道[中國內地新聞為主](../Page/中國內地.md "wikilink")。該社屬於[國僑辦](../Page/國僑辦.md "wikilink")\[1\]。

## 參考書目

  - 《穿行歷史時空：香港中通社新聞作品集》房奕強 編，中通社2006年11月初版，ISBN
    9889914174（[上冊](http://www.hkcna.hk/books/1.shtml)）、ISBN
    9889914131（[下冊](http://www.hkcna.hk/books/7.shtml)）
  - 《穿行中國大地：[中國內地新聞報道集](http://www.hkcna.hk/books/4.shtml)》李孝諄
    編，香港中通社2006年11月初版，ISBN 9889914110
  - 《穿行海峽兩岸：[台灣及兩岸新聞報道集](http://www.hkcna.hk/books/5.shtml)》張穗峰
    編，香港中通社2006年11月初版，ISBN 9889914127

## 外部链接

  - [香港中国通讯社](http://www.hkcna.hk/index.shtml)
  - [香港新闻网](http://www.hkcna.hk/index.shtml)從一九九五年七月一日起，香港中通社新聞進入全球資訊網路INTERNET，原名為中通網，自2009年9月1日正式改名為香港新聞網。
  - [中通社舉行慶祝成立50周年酒會](https://web.archive.org/web/20120222084055/http://news.xinhuanet.com/newmedia/2006-11/18/content_5345591.htm)，中新社2006年11月18日

## 參見

  - [中國評論通訊社](../Page/中國評論通訊社.md "wikilink")
  - [中央通訊社](../Page/中央通訊社.md "wikilink")
  - [親中媒體](../Page/親中媒體.md "wikilink")

{{-}}

[Category:香港通讯社](../Category/香港通讯社.md "wikilink")

1.  <http://www.bbc.com/zhongwen/trad/china/2016/09/160901_hongkong_leung_commentary_spat>