**赵季平**（），中国作曲家，画家[赵望云之子](../Page/赵望云.md "wikilink")，[河北](../Page/河北.md "wikilink")[束鹿人](../Page/束鹿.md "wikilink")，[西安音乐学院](../Page/西安音乐学院.md "wikilink")、[中央音乐学院校友](../Page/中央音乐学院.md "wikilink")，[中国共产党党员](../Page/中国共产党.md "wikilink")。2008年至2015年任[西安音乐学院院长](../Page/西安音乐学院.md "wikilink")，2009年12月当选中国音乐家协会主席。

## 生平

趙季平的家庭堪稱藝術世家，父親[趙望雲為知名畫家](../Page/趙望雲.md "wikilink")，是[長安畫派的領袖](../Page/長安畫派.md "wikilink")，兄[趙振川](../Page/趙振川.md "wikilink")、[趙振霄分別為畫家](../Page/趙振霄.md "wikilink")、[大提琴家](../Page/大提琴.md "wikilink")\[1\]。趙季平1945年7月20日\[2\]生於[中華民國](../Page/中華民國_\(大陸時期\).md "wikilink")[河北省束鹿縣](../Page/河北省_\(中華民國\).md "wikilink")（今[中華人民共和國](../Page/中華人民共和國.md "wikilink")[河北省](../Page/河北省.md "wikilink")[辛集市](../Page/辛集市.md "wikilink")）。十二歲時報考[西安音樂學院附中](../Page/西安音乐学院.md "wikilink")，因父親被歸類於“右派”而未能通過政治審查。後於15歲時仍考入該校作曲系，畢業後分配到陝西省戲曲研究院工作，潛心研究國樂。[文革結束後](../Page/文革.md "wikilink")，趙季平進入[中央音樂學院就讀](../Page/中央音樂學院.md "wikilink")，1984年時為電影《[黄土地](../Page/黄土地_\(电影\).md "wikilink")》配樂，1987年又為《[红高粱](../Page/红高粱_\(电影\).md "wikilink")》配樂而知名。\[3\]1984年12月加入[中国共产党](../Page/中国共产党.md "wikilink")。

## 作品

### 影视配乐

他为诸多影视作品配乐作曲，许多已经成为经典之作。包括：

#### 电影

  - 《[黄土地](../Page/黄土地_\(电影\).md "wikilink")》
  - 《[红高粱](../Page/红高粱.md "wikilink")》（第八届“金鸡”奖最佳作曲奖）
  - 《[大红灯笼高高挂](../Page/大红灯笼高高挂.md "wikilink")》
  - 《[活着](../Page/活着_\(電影\).md "wikilink")》
  - 《[五个女子和一根绳子](../Page/五个女子和一根绳子.md "wikilink")》（法国“南特”国际电影节最佳音乐奖）
  - 《[孔繁森](../Page/孔繁森.md "wikilink")》获第十六届“金鸡”奖最佳作曲奖；
  - 《[大话西游](../Page/大话西游.md "wikilink")》
  - 《大阅兵》
  - 《[老井](../Page/老井.md "wikilink")》
  - 《[菊豆](../Page/菊豆.md "wikilink")》
  - 《[秋菊打官司](../Page/秋菊打官司.md "wikilink")》
  - 《[霸王别姬](../Page/霸王别姬_\(电影\).md "wikilink")》
  - 《[一九四二](../Page/一九四二.md "wikilink")》

#### 电视剧

  - 《[燕子李三](../Page/燕子李三.md "wikilink")》
  - 《[水浒传](../Page/水浒传_\(电视剧\).md "wikilink")》(第十六届“飞天”奖最佳音乐桨，其中“好汉歌”获最佳歌曲奖)
  - 《嫂娘》(第十八届“金鹰”奖最佳音乐奖)
  - 《[笑傲江湖](../Page/笑傲江湖_\(2001年电视剧\).md "wikilink")》
  - 《[大宅门](../Page/大宅门.md "wikilink")》
  - 《[乔家大院](../Page/乔家大院_\(电视剧\).md "wikilink")》
  - 《[大秦帝国之裂变](../Page/大秦帝国之裂变.md "wikilink")》
  - 《[三国](../Page/三国_\(电视剧\).md "wikilink")》
  - 《[倚天屠龙记](../Page/倚天屠龙记_\(1994年电视剧\).md "wikilink")》

### 其他作品

  - 歌曲《黄河鼓震》、《西部扬帆》（“五个一工程入选”）
  - 歌曲《祖国强大、国旗增色》（建国五十周年歌曲征集一等奖）

<!-- end list -->

  - 《第一交响乐》（首届“金钟”奖优秀作品铜奖）
  - 《第二交响乐 - 和平颂》

#### 唱片

  - [琵琶协奏曲](../Page/中國琵琶.md "wikilink")《[祝福](../Page/祝福_\(琵琶协奏曲\).md "wikilink")》
  - 管子与乐队《丝绸之路幻想曲》
  - 《黄河遥遥》（日本JVC唱片公司）

<!-- end list -->

  - 舞剧《大漠孤烟直》
  - 交响音画《太阳鸟》
  - 交响叙事诗《霸王别姬》
  - 室内乐作品《关山月——丝绸之路印象》
  - 舞剧《情天·恨海圆明园》

## 其他

  - 1985年起任陕西省戏曲研究院副院长。
  - 1991年起任陕西省歌舞剧院院长，曾兼任中国音乐家协会副主席、陕西省文联副主席、陕西省音乐家协会副主席、陕西省电影音乐学会会长和陕西省八届人大代表、[中共十五大代表](../Page/中共十五大.md "wikilink")、陕西省第十届政协委员等职务。
  - 2004年当选陕西文联主席。
  - 2008年當選[第十一屆全國人民代表大會代表](../Page/第十一屆全國人民代表大會.md "wikilink")，並擔任主席團成員

## 參考文獻

## 外部链接

  - [作曲家赵季平非官方网站](https://web.archive.org/web/20130323050820/http://www.zhaojiping.com/)

[Category:西安音樂學院校友](../Category/西安音樂學院校友.md "wikilink")
[Category:中央音樂學院校友](../Category/中央音樂學院校友.md "wikilink")
[Category:一级作曲](../Category/一级作曲.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:中國音樂家協會主席](../Category/中國音樂家協會主席.md "wikilink")
[Category:中国作曲家](../Category/中国作曲家.md "wikilink")
[Category:西安音乐学院院长](../Category/西安音乐学院院长.md "wikilink")
[Category:中国电影音乐作曲家](../Category/中国电影音乐作曲家.md "wikilink")
[Category:中国共产党党员
(1984年入党)](../Category/中国共产党党员_\(1984年入党\).md "wikilink")
[Category:辛集人](../Category/辛集人.md "wikilink")
[Category:平凉人](../Category/平凉人.md "wikilink")
[J](../Category/趙姓.md "wikilink")

1.
2.

3.