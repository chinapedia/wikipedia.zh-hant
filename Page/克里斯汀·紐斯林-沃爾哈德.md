**克里斯汀·紐斯林-沃爾哈德**（，），[德國](../Page/德國.md "wikilink")[發育](../Page/發育生物學.md "wikilink")[遺傳學家](../Page/遺傳學.md "wikilink")，出生於[馬格德堡](../Page/馬格德堡.md "wikilink")。1991年獲得[拉斯克基礎醫學研究獎](../Page/拉斯克基礎醫學研究獎.md "wikilink")，1995年与[艾瑞克·威斯喬斯和](../Page/艾瑞克·威斯喬斯.md "wikilink")[愛德華·路易斯共同獲得](../Page/愛德華·路易斯.md "wikilink")[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")，以表彰他们对[胚胎](../Page/胚胎.md "wikilink")[发育的](../Page/发育生物学.md "wikilink")[遗传控制的研究](../Page/遗传学.md "wikilink")。\[1\]

## 参见

  - [诺贝尔奖女性得主列表](../Page/诺贝尔奖女性得主列表.md "wikilink")

## 参考资料

## 外部链接

  - [Christiane Nüsslein-Volhard's
    homepage](https://web.archive.org/web/20120206024134/http://www.eb.tuebingen.mpg.de/departments/3-genetics/christiane-nusslein-volhard/?searchterm=nusslein-volhard)
  - [*Gradients That Organize Embryo Development*, (Scientific American,
    August
    1996)](https://web.archive.org/web/20120206024553/http://www.eb.tuebingen.mpg.de/departments/3-genetics/christiane-nusslein-volhard/gradients-that-organize-embryo-development)
  - [诺贝尔官方网站克里斯汀·紐斯林-沃爾哈德自传](http://nobelprize.org/medicine/laureates/1995/nusslein-volhard-lecture.html)
  - [*Smithsonian* magazine
    interview](http://www.smithsonianmag.com/issues/2006/june/cnv.php?page=1)
  - [Christiane Nüsslein-Volhard Stiftung (Christiane Nüsslein-Volhard
    Foundation)](http://www.cnv-stiftung.de/)
  - [The Official Site of Louisa Gross Horwitz
    Prize](http://www.cumc.columbia.edu/horwitz/)

[Category:德國生物化學家](../Category/德國生物化學家.md "wikilink")
[Category:德國生理學家](../Category/德國生理學家.md "wikilink")
[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:女性諾貝爾獎獲得者](../Category/女性諾貝爾獎獲得者.md "wikilink")
[Category:德国女性科学家](../Category/德国女性科学家.md "wikilink")
[Category:蒂賓根大學教師](../Category/蒂賓根大學教師.md "wikilink")
[Category:蒂賓根大學校友](../Category/蒂賓根大學校友.md "wikilink")
[Category:法蘭克福大學校友](../Category/法蘭克福大學校友.md "wikilink")
[Category:薩克森-安哈特人](../Category/薩克森-安哈特人.md "wikilink")
[Category:拉斯克基礎醫學研究獎得主](../Category/拉斯克基礎醫學研究獎得主.md "wikilink")
[Category:欧洲分子生物学组织会员](../Category/欧洲分子生物学组织会员.md "wikilink")
[Category:霍维茨奖获得者](../Category/霍维茨奖获得者.md "wikilink")
[Category:马克斯·普朗克学会人物](../Category/马克斯·普朗克学会人物.md "wikilink")

1.