[Boeing_737-300_TNT.jpg](https://zh.wikipedia.org/wiki/File:Boeing_737-300_TNT.jpg "fig:Boeing_737-300_TNT.jpg")的波音737-300\]\]

**ASL比利時航空**，前身為TNT航空，是貨物和搭載乘客的[包機航空](../Page/包機.md "wikilink")，樞紐在[比利時的](../Page/比利時.md "wikilink")[列日](../Page/列日.md "wikilink")。於1999年由[TNT集团成立](../Page/TNT集团.md "wikilink")。這也是[TNT快递的一部分](../Page/TNT快递.md "wikilink")。TNT航空每天有超過67個貨物航線到各地機場
。在2004年5月他們開始操作乘客包機服務。

## 機隊

<table>
<caption><strong>ASL比利時貨機機隊</strong></caption>
<thead>
<tr class="header">
<th><p>型號</p></th>
<th><p>總數</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/空中巴士A300.md" title="wikilink">空中巴士A300B4-200F</a></p></td>
<td><div style='text-align: center;'>
<p>6</p>
</div></td>
<td><div style='text-align: center;'>
</div></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BAe_146.md" title="wikilink">BAe 146-200QT</a></p></td>
<td><div style='text-align: center;'>
<p>9</p>
</div></td>
<td><div style='text-align: center;'>
</div></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BAe_146.md" title="wikilink">BAe 146-200QC</a></p></td>
<td><div style='text-align: center;'>
<p>2</p>
</div></td>
<td><div style='text-align: center;'>
</div></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BAe_146.md" title="wikilink">BAe 146-300QT</a></p></td>
<td><div style='text-align: center;'>
<p>8</p>
</div></td>
<td><div style='text-align: center;'>
</div></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-300</a></p></td>
<td><div style='text-align: center;'>
<p>2</p>
</div></td>
<td><div style='text-align: center;'>
</div></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-300QC</a></p></td>
<td><div style='text-align: center;'>
<p>1</p>
</div></td>
<td><div style='text-align: center;'>
</div></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音737.md" title="wikilink">波音737-300SF</a></p></td>
<td><div style='text-align: center;'>
<p>7</p>
</div></td>
<td><div style='text-align: center;'>
</div></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波音747-400.md" title="wikilink">波音747-400ERF</a></p></td>
<td><div style='text-align: center;'>
<p>4</p>
</div></td>
<td><div style='text-align: center;'>
</div></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音757.md" title="wikilink">波音757-200PF</a></p></td>
<td><div style='text-align: center;'>
<p>1</p>
</div></td>
<td><div style='text-align: left;'>
<p>從<a href="../Page/冰島航空.md" title="wikilink">冰島航空租借</a></p>
</div></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Tu-204.md" title="wikilink">圖波列夫204-120C</a></p></td>
<td><div style='text-align: center;'>
<p>2</p>
</div></td>
<td><div style='text-align: center;'>
<p>從<a href="../Page/開羅航空.md" title="wikilink">開羅航空租借</a></p>
</div></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波音777.md" title="wikilink">波音777-FHT</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<div style='text-align: center;'>

3\.

</div>

</center>

## 外部連結

[Category:比利時航空公司](../Category/比利時航空公司.md "wikilink")
[Category:1999年成立的航空公司](../Category/1999年成立的航空公司.md "wikilink")
[Category:貨運航空公司](../Category/貨運航空公司.md "wikilink")