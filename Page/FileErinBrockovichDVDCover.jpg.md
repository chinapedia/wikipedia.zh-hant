<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>A scanned, low-resolution image of the DVD cover of the film Erin Brockovich.</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p>转自英文维基百科 © Copyright 2000 Universal Studios. All Rights Reserved</p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2005年11月27日</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>The image picture-fair-use.jpg is being linked here; though the picture is originally copyright I (— EagleOne\Talk 04:06, 27 December 2005 (UTC)) feel it is covered by fair use because:</p>
<ol>
<li>it is a low resolution copy of a DVD cover;</li>
<li>it does not limit the copyright owners rights to sell the DVD in any way;</li>
<li>copies could not be used to make illegal copies of the album artwork on another DVD;</li>
<li>the image is used only to illustrate the article on the film itself.</li>
</ol>
<p>{{#switch: 档案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>