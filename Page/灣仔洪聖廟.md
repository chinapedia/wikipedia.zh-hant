[Hk_wan_chai_old_temple_1.jpg](https://zh.wikipedia.org/wiki/File:Hk_wan_chai_old_temple_1.jpg "fig:Hk_wan_chai_old_temple_1.jpg")
**灣仔洪聖廟**是[香港一座](../Page/香港.md "wikilink")[洪聖廟](../Page/洪聖廟.md "wikilink")，位於[灣仔](../Page/灣仔.md "wikilink")[皇后大道東](../Page/皇后大道東.md "wikilink")，現為[香港一級歷史建築](../Page/香港一級歷史建築.md "wikilink")。洪聖廟面對[大王東街及](../Page/大王東街.md "wikilink")[大王西街](../Page/大王西街.md "wikilink")，大王即[洪聖大王](../Page/洪聖大王.md "wikilink")。1971年[華人廟宇委員會以](../Page/華人廟宇委員會.md "wikilink")「授權管理廟宇」條件將灣仔洪聖廟交由[東華三院管理](../Page/東華三院.md "wikilink")。灣仔洪聖廟是提供每年[驚蟄](../Page/驚蟄.md "wikilink")[打小人活動的廟宇之一](../Page/打小人.md "wikilink")\[1\]。

## 歷史

[WanchaiHungShingTemple_MainTitle.jpg](https://zh.wikipedia.org/wiki/File:WanchaiHungShingTemple_MainTitle.jpg "fig:WanchaiHungShingTemple_MainTitle.jpg")
[Hk_wan_chai_old_temple_2.jpg](https://zh.wikipedia.org/wiki/File:Hk_wan_chai_old_temple_2.jpg "fig:Hk_wan_chai_old_temple_2.jpg")\]\]
灣仔洪聖廟原為海濱岩石上的小神壇，由坊眾於1847年依山岩建築，[清朝](../Page/清.md "wikilink")[咸豐十年](../Page/咸豐.md "wikilink")（1860年）重建。廟宇原座落海邊，因灣仔海傍填海，現已位處內陸。該廟最近一次修葺是於1992年，並使1867年擴建的望海觀音廟內花崗石過樑重現本來面貌。

到2015年，灣仔洪聖廟暫時封閉，並進行3個月大型復修，於2015年9月26日舉行重修揭幕及開光典禮。[東華三院稱](../Page/東華三院.md "wikilink")，廟宇4月時有七成橫樑被蟲蛀，有倒塌危險，故為公眾安全遂封廟重修。是次重修耗資逾140萬元，更換了橫樑及部分屋頂瓦片，並翻新神像等。\[2\]

## 結構

[Kwun_Yum_temple,_an_annex_to_the_Hung_Shing_Temple,_Wan_Chai_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Kwun_Yum_temple,_an_annex_to_the_Hung_Shing_Temple,_Wan_Chai_\(Hong_Kong\).jpg "fig:Kwun_Yum_temple,_an_annex_to_the_Hung_Shing_Temple,_Wan_Chai_(Hong_Kong).jpg")
廟宇為三間設計，惟內無深進，只有一間廳堂，屋頂鋪有瓦塊，石柱上刻對廟聯：「古廟街新，海晏河清歌聖德；下環\[3\]抒悃，民康物阜被天恩。」，廟內供奉多位神明，包括[洪聖大王](../Page/洪聖大王.md "wikilink")、[太歲](../Page/太歲.md "wikilink")、[包公丞相](../Page/包公.md "wikilink")、[華佗](../Page/華佗.md "wikilink")、[文昌帝君](../Page/文昌帝君.md "wikilink")、華光大帝，還有兩位女神：「金花夫人」和「花粉夫人」等。金花夫人，即「金花娘娘」，是保護母嬰的正神。而「花粉夫人」則是保佑女性青春貌美，吸引異性的民間俗神\[4\]。廟側建有「望海觀音廟」，高兩層，內奉[觀音](../Page/觀音.md "wikilink")、[城隍及灶君老張王爺的神位](../Page/城隍.md "wikilink")。

灣仔洪聖廟依山岩而建，廟後石牆生有一棵[細葉榕](../Page/細葉榕.md "wikilink")\[5\]。

## 註釋

<div class="references-small">

<references />

</div>

## 參考資料

  - [東華三院：灣仔洪聖廟](http://www.tungwahcsd.org/chi/ts/tscentre.php?id=106&steid=226&fontsize=middle)

[Category:香港洪聖廟](../Category/香港洪聖廟.md "wikilink")
[Category:東華三院](../Category/東華三院.md "wikilink")
[Category:香港一級歷史建築](../Category/香港一級歷史建築.md "wikilink")
[Category:灣仔](../Category/灣仔.md "wikilink")

1.  [香港「打小人」儀式的非物質文化遺產保護及承傳（PDF格式）](http://ihome.cuhk.edu.hk/~b109214/Final%20Report_Leungs%20gp.pdf)

2.  [蟲蛀七成樑 灣仔洪聖廟修復
    《明報》 2015年9月26日](http://news.mingpao.com/pns/%E8%9F%B2%E8%9B%80%E4%B8%83%E6%88%90%E6%A8%91%20%E7%81%A3%E4%BB%94%E6%B4%AA%E8%81%96%E5%BB%9F%E4%BF%AE%E5%BE%A9/web_tc/article/20150927/s00002/1443290957832)
3.  灣仔當時稱「下環」，是「小海灣」的意思。
4.
5.  [遊遊舊灣仔
    看看石牆樹](http://forestying.mysinablog.com/index.php?op=ViewArticle&articleId=641229)