[莫荣新.jpg](https://zh.wikipedia.org/wiki/File:莫荣新.jpg "fig:莫荣新.jpg")
**莫榮新**（）字**日初**，[廣西省](../Page/廣西省.md "wikilink")[潯州府](../Page/潯州府.md "wikilink")[桂平县](../Page/桂平县.md "wikilink")[江口鎮盤石村人](../Page/江口鎮.md "wikilink")。[清末](../Page/清末.md "wikilink")[民初军事将领](../Page/民初.md "wikilink")，[旧桂系成员](../Page/旧桂系.md "wikilink")。\[1\]\[2\]

## 生平

莫榮新自幼求学，年纪稍大便当農民及雑貨店店員。1871年（[同治](../Page/同治.md "wikilink")10年）参軍，在[广西的巡防营逐步升迁](../Page/广西省.md "wikilink")。1909年（[宣統元年](../Page/宣統.md "wikilink")），任广西巡防营帮统，驻扎[梧州](../Page/梧州市.md "wikilink")。1911年（宣統3年）6月，[陸荣廷升任广西](../Page/陸荣廷.md "wikilink")[提督](../Page/提督.md "wikilink")，莫荣新升任巡防营督带。\[3\]\[4\]
[Mo_Rongxin.jpg](https://zh.wikipedia.org/wiki/File:Mo_Rongxin.jpg "fig:Mo_Rongxin.jpg")
[辛亥革命爆发後](../Page/辛亥革命.md "wikilink")，莫榮新被广西军政府任命为[慶遠府府長](../Page/慶遠府.md "wikilink")。1912年（[民国元年](../Page/中華民国历.md "wikilink")）5月，莫荣新被陸荣廷任命为[梧州府府長兼](../Page/梧州府.md "wikilink")[梧州關監督](../Page/梧州關.md "wikilink")。不久，被任命为广西中区第1正司令。1914年（民国3年），任广西陸軍第1師第2旅旅長兼[蒼梧道道尹](../Page/蒼梧道.md "wikilink")。1915年（民国4年）9月，任[桂平鎮守使](../Page/桂平.md "wikilink")。同年12月，[袁世凱称帝](../Page/袁世凱称帝.md "wikilink")，莫榮新获封二等男爵。1916年（民国5年），随陸荣廷参加[護国战争](../Page/護国战争.md "wikilink")，5月[肇慶成立](../Page/肇慶市.md "wikilink")[護国軍](../Page/護国軍.md "wikilink")[两广都司令部](../Page/两广都司令部.md "wikilink")，莫榮新被任命为[護国軍第](../Page/護国軍.md "wikilink")3師師長兼肇慶衛戌司令。\[5\]\[6\]

袁世凱死後，1916年10月，莫荣新兼任广惠鎮守使。12月，调任[广東第](../Page/广東省_\(中華民国\).md "wikilink")2混成旅旅長。1917年（民国6年）11月，被任命为广東督軍。1918年（民国7年）8月，陸荣廷掌握主導权后，改組广州軍政府，莫荣新兼任陸軍部長。1920年（民国9年）10月，被[孫文支持的](../Page/孫文.md "wikilink")[陳炯明击败后](../Page/陳炯明.md "wikilink")，莫荣新下野并逃往[上海](../Page/上海市.md "wikilink")，化名“高崇民”潜伏。\[7\]\[8\]1922年2月25日，获北京政府授为[将军府](../Page/北洋将军府.md "wikilink")[腾威将军](../Page/北洋政府将军府将军列表.md "wikilink")。\[9\]1928年（民国17年）秋，回到家乡桂平并引退。\[10\]\[11\]

1930年（民国19年）3月30日，莫荣新在桂平病逝。享年78岁（满76歳）。\[12\]\[13\]

## 参考文献

{{-}}

[Category:桂军将领](../Category/桂军将领.md "wikilink")
[Category:护国军将领](../Category/护国军将领.md "wikilink")
[Category:北洋将军府冠字将军](../Category/北洋将军府冠字将军.md "wikilink")
[Category:中華民國慶遠府府長](../Category/中華民國慶遠府府長.md "wikilink")
[Category:中華民國梧州府府長](../Category/中華民國梧州府府長.md "wikilink")
[Category:中華民國廣東省省長](../Category/中華民國廣東省省長.md "wikilink")
[Category:中華民國蒼梧道道尹](../Category/中華民國蒼梧道道尹.md "wikilink")
[Category:桂平镇守使](../Category/桂平镇守使.md "wikilink")
[Category:广惠镇守使](../Category/广惠镇守使.md "wikilink")
[R榮](../Category/莫姓.md "wikilink")

1.

2.

3.
4.
5.
6.
7.
8.
9.  1922年2月25日第2150号. 《政府公报》第一八五册.

10.
11.
12.
13.