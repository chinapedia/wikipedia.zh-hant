**羅特·蓮娜**（**Lotte
Lenya**，）是曾拿下[托尼獎以及獲得](../Page/托尼獎.md "wikilink")[奧斯卡獎提名的](../Page/奧斯卡獎.md "wikilink")[女演員與](../Page/女演員.md "wikilink")[歌手](../Page/歌手.md "wikilink")。本名為**Karoline
Wilhelmine Charlotte Blamauer**出生於維也納。最為人所知的角色是貝爾托·布萊希特與庫特懷爾（Kurt
Weill）的歌劇《[三文錢的歌劇](../Page/三文錢的歌劇.md "wikilink")》裡頭的角色
Jenny。在1961年[田納西·威廉斯所執導的電影版](../Page/田納西·威廉斯.md "wikilink")《[斯通夫人的羅馬之春](../Page/斯通夫人的羅馬之春.md "wikilink")》中扮演[費雯·麗的好友](../Page/費雯·麗.md "wikilink")，因此片而獲得了[奧斯卡最佳女配角獎提名](../Page/奧斯卡最佳女配角獎.md "wikilink")。在《[第七號情報員續集](../Page/第七號情報員續集.md "wikilink")》(From
Russia with Love)中因飾演[羅莎·克列伯](../Page/羅莎·克列伯.md "wikilink")(Rosa
Klebb)而聲名大噪。

[分類:奧地利電影演員](../Page/分類:奧地利電影演員.md "wikilink")

[Category:奧地利女歌手](../Category/奧地利女歌手.md "wikilink")
[Category:維也納人](../Category/維也納人.md "wikilink")