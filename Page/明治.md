**明治**是[日本](../Page/日本.md "wikilink")[明治天皇在位期間使用的](../Page/明治天皇.md "wikilink")[年號](../Page/年號.md "wikilink")，時間為1868年10月23日（[旧历](../Page/天保历.md "wikilink")9月8日）至1912年7月30日。

## 年號更改

  - [慶應](../Page/慶應.md "wikilink")4年9月8日（[1868年](../Page/1868年.md "wikilink")10月28日）
    - [明治天皇即位](../Page/明治天皇.md "wikilink")，年號更改。
  - [1912年](../Page/1912年.md "wikilink")（明治45年）7月30日（[1873年開始實行西洋曆](../Page/1873年.md "wikilink")）
    -
    [明治天皇去世](../Page/明治天皇.md "wikilink")，[大正天皇即位](../Page/大正天皇.md "wikilink")，改年號為[大正](../Page/大正.md "wikilink")。

## 名稱來源

「明治」一詞出自《[易經](../Page/易經.md "wikilink")·說卦傳》之「[聖人南面而聽](../Page/聖人.md "wikilink")[天下](../Page/天下.md "wikilink")，**嚮明而治**」。

## 年表

  - 明治2年（1869年）：[戊辰戰爭結束](../Page/戊辰戰爭.md "wikilink")、[版籍奉還](../Page/版籍奉還.md "wikilink")
  - 明治4年（1871年）：[廢藩置縣](../Page/廢藩置縣.md "wikilink")、[日清修好條規](../Page/日清修好條規.md "wikilink")、[新貨條例](../Page/新貨條例.md "wikilink")、[岩倉使節團出使外國](../Page/岩倉使節團.md "wikilink")。
  - 明治5年（1872年）：[格里曆的採用](../Page/格里曆.md "wikilink")（11月9日的改暦詔書。明治5年12月2日（舊曆）的翌日作為明治6年1月1日）。
  - 明治6年（1873年）：[徵兵令](../Page/徵兵令.md "wikilink")、[地租改正](../Page/地租改正.md "wikilink")、[明治六年政變](../Page/明治六年政變.md "wikilink")（[西鄉隆盛](../Page/西鄉隆盛.md "wikilink")・[板垣退助等下野](../Page/板垣退助.md "wikilink")）
  - 明治7年（1874年）：[民撰議院設立建白書](../Page/民撰議院設立建白書.md "wikilink")、[台灣出兵](../Page/牡丹社事件.md "wikilink")
  - 明治8年（1875年）：[樺太千島交換條約](../Page/樺太千島交換條約.md "wikilink")
  - 明治9年（1876年）：[日朝修好条規](../Page/江华条约.md "wikilink")、[廢刀令](../Page/廢刀令.md "wikilink")
  - 明治10年（1877年）：[西南戰爭](../Page/西南戰爭.md "wikilink")（- 明治11年）
  - 明治11年（1878年）：[地方三新法](../Page/地方三新法.md "wikilink")、[大久保利通遇刺身亡](../Page/紀尾井坂之變.md "wikilink")、[竹橋事件](../Page/竹橋事件.md "wikilink")
  - 明治12年（1879年）：8月31日、皇太子明宮嘉仁親王（[大正天皇](../Page/大正天皇.md "wikilink")）誕生。[沖繩縣設置](../Page/沖繩縣.md "wikilink")（[琉球處分](../Page/琉球處分.md "wikilink")）
  - 明治13年（1880年）：[国会期成同盟結成](../Page/国会期成同盟.md "wikilink")
  - 明治14年（1881年）：[明治十四年政變](../Page/自由民權運動.md "wikilink")、[國會開設的詔書發佈](../Page/國會.md "wikilink")。→[大隈重信垮台後](../Page/大隈重信.md "wikilink")、大蔵卿[松方正義的](../Page/松方正義.md "wikilink")[松方通貨緊縮](../Page/松方通貨緊縮.md "wikilink")
  - 明治15年（1882年）：[福島事件](../Page/福島事件.md "wikilink")、[壬午事變](../Page/壬午事變.md "wikilink")
  - 明治17年（1884年）：[秩父事件](../Page/秩父事件.md "wikilink")、[甲申政变](../Page/甲申政变.md "wikilink")
  - 明治18年（1885年）：[天津條約](../Page/中日天津会议专条.md "wikilink")、[内閣制開始實施](../Page/内閣制.md "wikilink")
  - 明治22年（1889年）：《[大日本帝國憲法](../Page/大日本帝國憲法.md "wikilink")》發布
  - 明治23年（1890年）：[第1屆日本眾議院議員總選舉](../Page/第1屆日本眾議院議員總選舉.md "wikilink")、第1回[帝國議會召集](../Page/帝國議會.md "wikilink")、『[教育敕語](../Page/教育敕語.md "wikilink")』發布
  - 明治24年（1891年）：[大津事件](../Page/大津事件.md "wikilink")、研制出下濑火药
  - 明治26年（1893年）：长井长义自[麻黄碱合成出](../Page/麻黄碱.md "wikilink")[甲基苯丙胺](../Page/甲基苯丙胺.md "wikilink")
  - 明治27年（1894年）：[甲午農民戰爭](../Page/甲午農民戰爭.md "wikilink")→[日英通商航海條約](../Page/日英通商航海條約.md "wikilink")→[日清戰爭](../Page/日清戰爭.md "wikilink")→（明治28年）
  - 明治28年（1895年）：[馬關條約](../Page/馬關條約.md "wikilink")、[乙未戰爭開始統治臺灣](../Page/乙未戰爭.md "wikilink")
  - 明治33年（1900年）：[皇太子](../Page/皇太子.md "wikilink")[嘉仁親王](../Page/嘉仁.md "wikilink")（大正天皇）與[九條節子結婚](../Page/貞明皇后.md "wikilink")。[義和團事件](../Page/義和團事件.md "wikilink")。
  - 明治34年（1901年）：4月29日、[皇太孫迪宮裕仁親王](../Page/皇太孫.md "wikilink")（[昭和天皇](../Page/昭和天皇.md "wikilink")）誕生。[足尾銅山礦毒事件](../Page/足尾銅山礦毒事件.md "wikilink")、國營[八幡製鐵所開始作業](../Page/八幡製鐵所.md "wikilink")、[高峰让吉首次分离出](../Page/高峰让吉.md "wikilink")[肾上腺素](../Page/肾上腺素.md "wikilink")
  - 明治35年（1902年）：[日英同盟](../Page/日英同盟.md "wikilink")
  - 明治37年（1904年）：[日俄戰爭](../Page/日俄戰爭.md "wikilink")
  - 明治38年（1905年）：[樸茨茅斯條約](../Page/樸茨茅斯條約.md "wikilink")、[日比谷縱火事件](../Page/日比谷縱火事件.md "wikilink")
  - 明治39年（1906年）：[南滿州鐵道設立](../Page/南滿州鐵道.md "wikilink")。
  - 明治40年（1907年）：[海牙密使事件](../Page/海牙密使事件.md "wikilink")
  - 明治41年（1908年）：[赤旗事件](../Page/赤旗事件.md "wikilink")
  - 明治42年（1909年）：[伊藤博文遇刺身亡](../Page/伊藤博文.md "wikilink")
  - 明治43年（1910年）：[日韓併合](../Page/日韓併合.md "wikilink")、[大逆事件](../Page/大逆事件.md "wikilink")、[铃木梅太郎从米糠中提取出了抗脚气病酸](../Page/铃木梅太郎.md "wikilink")，将它命名为[硫胺](../Page/硫胺.md "wikilink")（维生素B1）
  - 明治44年（1911年）：[關稅自主權的回復](../Page/關稅自主權.md "wikilink")、幕末以来的[不平等條約完全廢除](../Page/不平等條約.md "wikilink")。
  - 明治45年（1912年）：7月30日、[明治天皇](../Page/明治天皇.md "wikilink")[駕崩](../Page/駕崩.md "wikilink")、[大正改元](../Page/大正.md "wikilink")

## 與西曆的對照表

明治n年與[公元g年的關係是n](../Page/公元.md "wikilink") = g - 1867 = g - 1900 +
33。也就是，公元1900年之後，年份最後兩個數字再加上33就是明治的年份。

| 明治                             | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             | 6年                             | 7年                             | 8年                             | 9年                             | 10年                            |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西曆                             | 1868年                          | 1869年                          | 1870年                          | 1871年                          | 1872年                          | 1873年                          | 1874年                          | 1875年                          | 1876年                          | 1877年                          |
| [干支](../Page/干支.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") |
| 明治                             | 11年                            | 12年                            | 13年                            | 14年                            | 15年                            | 16年                            | 17年                            | 18年                            | 19年                            | 20年                            |
| 西曆                             | 1878年                          | 1879年                          | 1880年                          | 1881年                          | 1882年                          | 1883年                          | 1884年                          | 1885年                          | 1886年                          | 1887年                          |
| 干支                             | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") | [甲申](../Page/甲申.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") | [丙戌](../Page/丙戌.md "wikilink") | [丁亥](../Page/丁亥.md "wikilink") |
| 明治                             | 21年                            | 22年                            | 23年                            | 24年                            | 25年                            | 26年                            | 27年                            | 28年                            | 29年                            | 30年                            |
| 西曆                             | 1888年                          | 1889年                          | 1890年                          | 1891年                          | 1892年                          | 1893年                          | 1894年                          | 1895年                          | 1896年                          | 1897年                          |
| 干支                             | [戊子](../Page/戊子.md "wikilink") | [己丑](../Page/己丑.md "wikilink") | [庚寅](../Page/庚寅.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") | [甲午](../Page/甲午.md "wikilink") | [乙未](../Page/乙未.md "wikilink") | [丙申](../Page/丙申.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink") |
| 明治                             | 31年                            | 32年                            | 33年                            | 34年                            | 35年                            | 36年                            | 37年                            | 38年                            | 39年                            | 40年                            |
| 西曆                             | 1898年                          | 1899年                          | 1900年                          | 1901年                          | 1902年                          | 1903年                          | 1904年                          | 1905年                          | 1906年                          | 1907年                          |
| 干支                             | [戊戌](../Page/戊戌.md "wikilink") | [己亥](../Page/己亥.md "wikilink") | [庚子](../Page/庚子.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") | [壬寅](../Page/壬寅.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink") | [甲辰](../Page/甲辰.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") |
| 明治                             | 41年                            | 42年                            | 43年                            | 44年                            | 45年                            |                                |                                |                                |                                |                                |
| 西曆                             | 1908年                          | 1909年                          | 1910年                          | 1911年                          | 1912年                          |                                |                                |                                |                                |                                |
| 干支                             | [戊申](../Page/戊申.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") |                                |                                |                                |                                |                                |

## 外部連結

  - [明治史研究のためのリンク集](http://1868.fc2web.com/links/index.html)

{{-}}

[Category:19世纪日本年号](../Category/19世纪日本年号.md "wikilink")
[Category:20世纪日本年号](../Category/20世纪日本年号.md "wikilink")
[明治時代](../Category/明治時代.md "wikilink")
[日](../Category/台灣年號.md "wikilink")