[Chengdu_2012-03.jpg](https://zh.wikipedia.org/wiki/File:Chengdu_2012-03.jpg "fig:Chengdu_2012-03.jpg")
[Covered_bridge_in_Humble_Administrator's_Garden.JPG](https://zh.wikipedia.org/wiki/File:Covered_bridge_in_Humble_Administrator's_Garden.JPG "fig:Covered_bridge_in_Humble_Administrator's_Garden.JPG")

**廊桥**亦称**虹桥**、**蜈蚣桥**等，為有頂的[橋](../Page/橋.md "wikilink")，可保护桥梁，同时亦可遮阳避雨、供人休憩、交流、聚会等作用。主要有木拱廊桥、石拱廊桥、木平廊桥、风雨桥、亭桥等。世界各地，譬如[欧洲](../Page/欧洲.md "wikilink")（以[中欧为多](../Page/中欧.md "wikilink")）、[北美](../Page/北美.md "wikilink")、[亚洲等地都有廊桥](../Page/亚洲.md "wikilink")，各具特色。

## 中国廊桥

廊桥在[中国已有](../Page/中国.md "wikilink")2000多年的历史，[汉朝已有关于](../Page/汉朝.md "wikilink")“廊桥”的记载。[虹桥盛行于北宋时](../Page/虹桥.md "wikilink")[中原地区](../Page/中原.md "wikilink")，以[汴水虹桥为代表](../Page/汴水虹桥.md "wikilink")。但汴水虹桥只留在了[北宋画家](../Page/北宋.md "wikilink")[张择端所画的](../Page/张择端.md "wikilink")《[清明上河图](../Page/清明上河图.md "wikilink")》中。但有专家认为这种虹桥技术目前依然存于闽浙边界的木拱廊桥，并未失传。

### 中国木拱廊桥

中国木拱廊桥分布于[闽](../Page/福建.md "wikilink")[浙边界山区](../Page/浙.md "wikilink")，尤其在[浙江](../Page/浙江.md "wikilink")[泰顺](../Page/泰顺.md "wikilink")，泰顺因此被称为“中国廊桥之乡”，古廊桥目前尚存30余座。2005年末，浙江[泰顺](../Page/泰顺.md "wikilink")、[庆元](../Page/庆元.md "wikilink")、[景宁和](../Page/景宁.md "wikilink")[福建](../Page/福建.md "wikilink")[寿宁](../Page/寿宁.md "wikilink")、[屏南](../Page/屏南.md "wikilink")、[福鼎等县市联合将廊桥向国家申报](../Page/福鼎.md "wikilink")[世界文化遗产预备名单](../Page/世界文化遗产.md "wikilink")。2009年9月30日，由福建省屏南县、寿宁县、[周宁县和浙江省泰顺县](../Page/周宁县.md "wikilink")、庆元县联合申报的“中国[木拱桥传统营造技艺](../Page/木拱桥传统营造技艺.md "wikilink")”被列入[联合国教科文组织](../Page/联合国教科文组织.md "wikilink")《[急需保护的非物质文化遗产名录](../Page/急需保护的非物质文化遗产名录.md "wikilink")》。

[Jinxi_bridge_front_view.JPG](https://zh.wikipedia.org/wiki/File:Jinxi_bridge_front_view.JPG "fig:Jinxi_bridge_front_view.JPG")
木拱廊桥现存200余座。其中泰顺县、庆元县、景宁县、寿宁县据传设于[明朝](../Page/明.md "wikilink")[景泰年间](../Page/景泰.md "wikilink")，为祝皇帝寿庆，合称“景泰寿庆”。廊桥在各地外观上存在着差异，泰顺、景宁的廊桥相桥凳窄、桥身侧板为半封闭；庆元、寿宁的板凳则为宽的，坐、卧兼可，侧板则为全封闭，上开各式小窗用以采光。

木拱廊桥亦被学者称为“木拱桥”，在泰顺境内当地人则称为**蜈蚣桥**。对于木拱廊桥的研究始于20世纪中期，1959年[罗英先生出版](../Page/罗英.md "wikilink")《中国桥梁史料》，内收[唐寰澄先生研究汴水虹桥的文章](../Page/唐寰澄.md "wikilink")，而当时的学术界普遍认为虹桥技术已失传。七十年代末人们“发现”了在浙南的木拱廊桥，因此[茅以升的](../Page/茅以升.md "wikilink")《中国古桥技术史》则举泰顺薛宅桥和庆元的竹口桥等五桥为例证明“北宁时期盛行于中原的虹桥技术并未失传”，此后又经历多了方讨论。但[南京大学建筑系的](../Page/南京大学.md "wikilink")[赵辰教授认为木拱廊桥是典型的山地人居文化遗产](../Page/赵辰.md "wikilink")。

赵辰教授的依据是浙闽边界地区山高溪深，有众多山崖。行路极不便，修路、铺桥，成为当地居民的基本需求，而且当地盛产杉木，为木拱桥提供了材料来源。同时当地处[亚热带海洋性季风气候区](../Page/亚热带海洋性季风气候.md "wikilink")，夏季多雨，这让加盖廊屋、侧板成为以保护木制桥身的必要要求。同时正因如此，廊桥成为了当地居民交流、聚会的中心。

早期，狭义的廊桥在学术上有两个名称，分别为Woven Timber Arch Bridge（编木拱桥）和Woven Timber
Arch-Beam Bridge（编木拱梁桥）。民间则习惯使用“Covered bridge”一词，但在实际中的“Covered
Bridge”多指美国的廊桥。因此，三者都不兼备“信、达、雅”的原则。2004年6月，美籍华人[叶守璋先生在一次世界木工程大会上建议](../Page/叶守璋.md "wikilink")，将中国这一特定的廊桥英文名称定为“Lounge
Bridge”，“Lounge”为“休息室”的意思，并且这一英文译名与汉语发音相似，且反映了廊屋的多种功能，是音意兼具的佳译。目前学术界开始逐步使用“Lounge
Bridge”一词。

#### 泰顺廊桥

[泰顺有](../Page/泰顺.md "wikilink")“中国廊桥之乡”的美誉，境内有各式桥梁900多座。泰顺人长期以来称廊桥为“蜈蚣桥”。目前有[明](../Page/明.md "wikilink")[清古廊桥](../Page/清.md "wikilink")30余座，其中木拱廊桥6座，包括泗溪[溪东桥](../Page/溪东桥.md "wikilink")、[北涧桥](../Page/北涧桥.md "wikilink")（合称姐妹桥）；三魁[薛宅桥](../Page/薛宅桥.md "wikilink")；仙稔[仙居桥](../Page/仙居桥.md "wikilink")；筱村[文兴桥](../Page/文兴桥.md "wikilink")；洲岭[三条桥](../Page/三条桥.md "wikilink")。

  - 北涧桥和溪东桥最著名，北涧桥被誉为“世界上最美的廊桥”。
  - 薛宅桥为拱矢斜度最大的廊桥。
  - 文兴桥为造型最为奇异，唯一两边不对称的廊桥。
  - 三条桥为史料记载最古老的廊桥（曾发现[唐](../Page/唐.md "wikilink")[贞观年号瓦](../Page/贞观.md "wikilink")），也是最有可能的实际上最早的廊桥。
  - 仙居桥为该县境内跨径最大的廊桥。

#### 庆元廊桥

[庆元的廊桥内为全封闭](../Page/庆元.md "wikilink")、宽桥凳，为使桥面更坚固、耐用，多铺设鹅卵石。现存有[如龙桥](../Page/如龙桥.md "wikilink")、[兰溪桥等](../Page/兰溪桥.md "wikilink")

#### 景宁廊桥

[景宁为畲族自治县](../Page/景宁.md "wikilink")，景宁拥有木拱、木平、石拱廊桥总量为50多座，其中木拱廊桥有6条。现存有[大地桥等](../Page/大地桥.md "wikilink")

#### 寿宁廊桥

[Xian'gong_Bridge.jpg](https://zh.wikipedia.org/wiki/File:Xian'gong_Bridge.jpg "fig:Xian'gong_Bridge.jpg")[寿宁县仙宫桥](../Page/寿宁县.md "wikilink")\]\]
据统计，[寿宁现存木拱廊桥](../Page/寿宁.md "wikilink")19座，主要分布在：鳌阳镇（县城）境内4座：飞云桥、仙宫桥、升平桥、登云桥；南阳镇境内2座：溪南桥、四洲桥；坑底乡境内5座：单桥、小东上桥、小东下桥、杨梅州桥、红军桥；犀溪乡境内2座：福寿桥、翁坑桥；芹洋乡境内4座：张坑桥、中村桥、长濑溪桥、尤溪桥；下党乡境内2座：杨溪头桥、鸾峰桥。
建桥时间从[清](../Page/清.md "wikilink")[乾隆](../Page/乾隆.md "wikilink")、[嘉庆](../Page/嘉庆.md "wikilink")、[道光](../Page/道光.md "wikilink")、[同治](../Page/同治.md "wikilink")、[光绪](../Page/光绪.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")、直至1967年。在寿宁县坑底乡，生活着当地现今唯一懂建廊桥技术的年逾70的老人[郑多金](../Page/郑多金.md "wikilink")，由郑多金1967年主持修建的杨溪头桥被认为是[中国最后建的木拱廊桥](../Page/中国.md "wikilink")。

[Chieng-seng-gio1.jpg](https://zh.wikipedia.org/wiki/File:Chieng-seng-gio1.jpg "fig:Chieng-seng-gio1.jpg")[屏南县](../Page/屏南县.md "wikilink")[千乘桥](../Page/千乘桥.md "wikilink")\]\]
[Chengyangqiao,_Guangxi,_China.jpg](https://zh.wikipedia.org/wiki/File:Chengyangqiao,_Guangxi,_China.jpg "fig:Chengyangqiao,_Guangxi,_China.jpg")（风雨桥）\]\]

#### 屏南廊桥

[屏南境内的廊桥据统计有](../Page/屏南.md "wikilink")13座。始建于[宋代的](../Page/宋.md "wikilink")[万安桥长度近百米](../Page/万安桥（廊桥）.md "wikilink")，为现在[中国最长的木拱廊桥](../Page/中国.md "wikilink")。

#### 福鼎廊桥

[福鼎市](../Page/福鼎市.md "wikilink")[管阳镇](../Page/管阳镇.md "wikilink")[西阳村境内的老人桥是福鼎市唯一的廊桥](../Page/西阳村.md "wikilink")。老人桥建于[明](../Page/明.md "wikilink")[正德年间](../Page/正德.md "wikilink")，[清](../Page/清.md "wikilink")[康熙时期重修过](../Page/康熙.md "wikilink")。

### 风雨桥

[风雨桥为](../Page/风雨桥.md "wikilink")[侗族建筑的代表](../Page/侗族.md "wikilink")，亦称**风水桥**、**花桥**、**凉桥**等。分布于[湖南](../Page/湖南.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[广西一带](../Page/广西.md "wikilink")。一般由桥、塔、亭构成。石桥墩上一般建多层的塔或亭，各层檐角翘起。顶有葫芦、千年鹤状吉祥物。

著名的风雨桥有位于[广西三江林溪河上的](../Page/广西.md "wikilink")[程阳桥](../Page/程阳永济桥.md "wikilink")。程阳桥建于1916年，为一四孔五墩伸臂木梁桥。

### 亭桥

[thumb](../Page/文件:The_Five_Pavilion_bridge.jpg.md "wikilink")[五亭桥](../Page/五亭桥.md "wikilink")\]\]
亭桥即在桥身上建亭的桥，主要分布于[中国](../Page/中国.md "wikilink")[江南地区](../Page/江南.md "wikilink")，如[杭州](../Page/杭州.md "wikilink")、[扬州](../Page/扬州.md "wikilink")、[苏州等城市](../Page/苏州.md "wikilink")。
其代表有[杭州](../Page/杭州.md "wikilink")[西湖十景之一](../Page/西湖十景.md "wikilink")[曲院风荷的](../Page/曲院风荷.md "wikilink")[玉带晴虹](../Page/玉带晴虹.md "wikilink")（玉带桥）。玉带晴虹位于苏堤之西的金沙港西面，与苏堤的望山桥相对。此外亭桥代表还有扬州[瘦西湖的](../Page/瘦西湖.md "wikilink")[五亭桥](../Page/五亭桥.md "wikilink")，苏州[拙政园的](../Page/拙政园.md "wikilink")[小飞虹等](../Page/小飞虹.md "wikilink")。

## 瑞士廊桥

[Luzern_Kapellbruecke.jpg](https://zh.wikipedia.org/wiki/File:Luzern_Kapellbruecke.jpg "fig:Luzern_Kapellbruecke.jpg")
卡贝尔桥（）位于[瑞士小城](../Page/瑞士.md "wikilink")[卢塞恩](../Page/卢塞恩.md "wikilink")，长达245米，横跨于[羅伊斯河上](../Page/羅伊斯河.md "wikilink")，建于1333年，为[欧洲历史最悠久的有顶木桥](../Page/欧洲.md "wikilink")，因其建桥时附近有一座[教堂](../Page/教堂.md "wikilink")，所以亦称教堂桥。桥中央有一八角形的水塔（Wasserturm），曾用作瞭望塔、监狱和金库，目前成为会馆。桥的横眉上有许多[宗教历史绘画](../Page/宗教.md "wikilink")。卡贝尔桥于1993年毁于一场大火，后得以逼真地重建。

## 美国廊桥

[Larrys_Creek_Covered_Bridge.JPG](https://zh.wikipedia.org/wiki/File:Larrys_Creek_Covered_Bridge.JPG "fig:Larrys_Creek_Covered_Bridge.JPG")
美国[宾夕法尼亚州有](../Page/宾夕法尼亚.md "wikilink")200余座廊桥，为全国之冠。

## 加拿大廊桥

[Hartland_Covered_Bridge.JPG](https://zh.wikipedia.org/wiki/File:Hartland_Covered_Bridge.JPG "fig:Hartland_Covered_Bridge.JPG")，
全长391米\]\]
1990年[加拿大](../Page/加拿大.md "wikilink")[魁北克省有](../Page/魁北克.md "wikilink")1000多廊桥，[新不伦瑞克省有](../Page/新不伦瑞克.md "wikilink")400座，到2006年魁北克省仍有94座廊桥，新不伦瑞克省仍有65座。
世界最长的木廊桥--[加拿大](../Page/加拿大.md "wikilink")[新不伦瑞克省](../Page/新不伦瑞克省.md "wikilink")
[哈特兰廊桥](../Page/哈特兰廊桥.md "wikilink"), 全长391米。

## 參見

  - [桥](../Page/桥.md "wikilink")

## 参考文献

## 外部链接

  - [中国廊桥网](http://www.langqiao.net/Index.asp)
  - [廊桥论坛](http://www.langqiao.net/bobbs/dispbbs.asp?boardid=3&id=11188)
  - [风雨廊桥](http://www.wh21.cn/hangerfeng/zouspecview.asp?keyno=65)

[Category:各結構型式橋梁](../Category/各結構型式橋梁.md "wikilink")