**美以美会差会**（American Methodist Episcopal
Mission）是[美以美会](../Page/美以美会.md "wikilink")（美国北方的[卫理宗教派](../Page/卫理宗.md "wikilink")）向海外传教的差会组织，曾向[中国等国家派遣为数众多的传教士](../Page/中国.md "wikilink")。

## 中国差会

[Former_amem_center_in_foochow.JPG](https://zh.wikipedia.org/wiki/File:Former_amem_center_in_foochow.JPG "fig:Former_amem_center_in_foochow.JPG")的总部，位于今天的[福建神学院内](../Page/福建神学院.md "wikilink")\]\]
[美以美会对华传教开始于](../Page/美以美会.md "wikilink")1847年。先锋传教士[柯林](../Page/柯林.md "wikilink")（Judson
Dwight
Collins）热切地恳求该会前往中国传教，当他得知没有财力可以提供时，他写道：如此的热情令人无法抗拒，于是在这一年，柯林和[懷特](../Page/懷特.md "wikilink")（Moses
Clark
White）两位传教士被派往开埠不久的通商口岸、福建省省会[福州](../Page/福州.md "wikilink")，不久又有希赫克（H.
Hickok）[麦利和](../Page/麦利和.md "wikilink")（R. S.
Maclay）等宣教士前來帮助，正式展開福音事工。他们在福州焦急地等待了10年之久，努力终于得到了成果，扩展为6个大的区，包括60个传教站。该会的印书馆相当繁忙，在1888年一年中发行了14,000页的传教作品。差会还在长江中下游沿岸长约300英里的地区的[九江](../Page/九江.md "wikilink")、镇江、南京、芜湖等城市开辟了传教站。在北方，美以美会进入[北京](../Page/北京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[泰安和](../Page/泰安.md "wikilink")[遵化](../Page/遵化.md "wikilink")，并开设了学校和医院；该会还向西扩展到距离海岸线1,400英里的[重庆](../Page/重庆.md "wikilink")。到1890年，它拥有32名传教士，17名单身女传教士，43名
本地牧师，91名未受圣职的本地助手，还有4000多名领圣餐者\[1\]。

  - 1939年美國衛理公會合併後，三會併稱為中華基督教衛理公會（The Methodist church in Republic of
    China），中央議會設在[福州天安堂](../Page/福州天安堂.md "wikilink")。

<!-- end list -->

  - 1947年11月17日，衛理公會在福州[天安堂舉行來華宣教百週年庆典](../Page/福州天安堂.md "wikilink")。全國已成立9個正式年議會；除了華東年議會（[上海](../Page/上海.md "wikilink")、[苏南](../Page/苏南.md "wikilink")、[浙北一带](../Page/浙北.md "wikilink")[吴语区](../Page/吴语区.md "wikilink")）是原[监理会开辟](../Page/监理会.md "wikilink")，其余8个都是原[美以美会开辟](../Page/美以美会.md "wikilink")。會友超過十萬，90％以上属于原[美以美会](../Page/美以美会.md "wikilink")。另外还有临时年议会，张家口年议会，原来属于[美普会](../Page/美普会.md "wikilink")

### 发源地：福州年议会（FOOCHOW CONFERENCE）

[Church_of_Heavenly_Peace_in_Foochow.jpg](https://zh.wikipedia.org/wiki/File:Church_of_Heavenly_Peace_in_Foochow.jpg "fig:Church_of_Heavenly_Peace_in_Foochow.jpg")
[Flower_lane_church.JPG](https://zh.wikipedia.org/wiki/File:Flower_lane_church.JPG "fig:Flower_lane_church.JPG")|right\]\]

  - 1847年，美以美會派遣[柯林和](../Page/柯林.md "wikilink")[懷特进入](../Page/懷特.md "wikilink")[福州](../Page/福州.md "wikilink")。
  - 1856年7月3日建成茶亭街（八一七中路）[真神堂](../Page/真神堂.md "wikilink")，10月18日建成仓山[天安堂](../Page/福州天安堂.md "wikilink")。但直到1857年7月14日，才有一位商人[陈安受洗](../Page/陈安.md "wikilink")。肯斯理（Kingsley）擔任首任會督。
  - 福州年议会下设福州、福清、龙田、[渔溪](../Page/渔溪镇_\(福建省\).md "wikilink")、平潭、闽清、古田、屏湖等八个教区，下设牧区（堂会），[天安堂](../Page/福州天安堂.md "wikilink")、[尚友堂之下又设有布道所](../Page/尚友堂.md "wikilink")。
  - 传教站：[福州](../Page/福州.md "wikilink")（1847）；[古田](../Page/古田.md "wikilink")(1889);海坛岛（[平潭](../Page/平潭.md "wikilink")，1895);[闽清](../Page/闽清.md "wikilink")(1896)；[福清](../Page/福清.md "wikilink")(1914)；
  - 著名教堂：[福州仓山](../Page/福州.md "wikilink")[福州天安堂](../Page/福州天安堂.md "wikilink")、花巷[尚友堂](../Page/尚友堂.md "wikilink")、茶亭[真神堂](../Page/真神堂.md "wikilink")、上渡[福民堂](../Page/福民堂.md "wikilink")、[小岭堂](../Page/小岭堂.md "wikilink")、伙贩街[尚仁堂](../Page/尚仁堂.md "wikilink")、义序[宣道堂](../Page/宣道堂.md "wikilink")、浦下[天道堂](../Page/天道堂.md "wikilink")、胪雷[正道堂](../Page/正道堂.md "wikilink")、城门堂等。[福清](../Page/福清.md "wikilink")[福华堂](../Page/福华堂.md "wikilink")。
  - 教育：[福建协和大学](../Page/福建协和大学.md "wikilink")、[华南女子大学](../Page/华南女子大学.md "wikilink")、福州[鹤龄英华中学](../Page/鹤龄英华中学.md "wikilink")、毓英女中
  - 医院：福清和新田医院，惠乐生女医院，平潭基督教西医院，古田怀礼医院，闽清善牧医院

### [兴化年议会](../Page/兴化.md "wikilink")（HINGHWA CONFERENCE）

  - 传教站：涵江; 兴化([莆田](../Page/莆田.md "wikilink")，1864);
    [仙游](../Page/仙游.md "wikilink")(1865)。传教范围是原兴化府（莆田,仙游两县），讲一种特别的[兴化方言](../Page/兴化方言.md "wikilink")。
  - 著名教堂：[莆田天道堂](../Page/莆田天道堂.md "wikilink")
  - 教育：兴化[哲理中学](../Page/哲理中学.md "wikilink")
  - 医院：涵江与仁医院，仙游美以美会女医院

### 延平年议会（YENPING CONFERENCE）

  - 传教站：延平([南平](../Page/南平.md "wikilink")，1902)。传教范围包括永安,[沙县一带](../Page/沙县.md "wikilink")。
  - 著名教堂：
  - 医院：延平吐吡哩医院，沙县美以美会医院

### 江西年议会（KIANGSI CONFERENCE）

  - 传教站：九江(1868); 南昌(1894)。传教范围包括湖北黄梅县。
  - 著名教堂：九江[甘南堂](../Page/甘南堂.md "wikilink")、大中路388号[化善堂](../Page/化善堂.md "wikilink")；南昌[清钟堂](../Page/清钟堂.md "wikilink")、[新民堂](../Page/新民堂.md "wikilink")、[志道堂](../Page/志道堂.md "wikilink")、[德胜堂](../Page/德胜堂.md "wikilink")
  - 教育：九江[同文中学](../Page/九江同文中学.md "wikilink")
    、[儒励女中](../Page/儒励女中.md "wikilink")；南昌[豫章中学](../Page/豫章中学.md "wikilink")、[葆灵女中](../Page/葆灵女中.md "wikilink")
  - 医院：九江[生命活水医院](../Page/生命活水医院.md "wikilink")，[但福德医院](../Page/但福德医院.md "wikilink")，南昌南昌医院，妇幼医院。

### 华北年议会（NORTH CHINA CONFERENCE）

  - 传教站：[北京](../Page/北京.md "wikilink")(1869)；[天津](../Page/天津.md "wikilink")(1870)；[昌黎](../Page/昌黎.md "wikilink")(1903)；[北戴河海滨](../Page/北戴河.md "wikilink");
    [遵化](../Page/遵化.md "wikilink").
  - 教堂：在北京城区设有崇文门[亚斯立堂](../Page/亚斯立堂.md "wikilink")、[珠市口堂](../Page/珠市口堂.md "wikilink")、宣外牧区、永定门外教堂、花市福音堂、方巾巷堂、广安门关厢福音堂、白纸坊福音堂、和平门外小沙土园堂、左安门外教堂、右安门关厢福音堂等10多座教堂，主要集中在南城；在天津设有[维斯理堂](../Page/维斯理堂.md "wikilink")（在法租界内）、南关教堂等
  - 教育：[燕京大学](../Page/燕京大学.md "wikilink")；北京[汇文中学](../Page/汇文中学.md "wikilink")、[慕贞女中](../Page/慕贞女中.md "wikilink")；[天津汇文中学](../Page/天津汇文中学.md "wikilink")、[中西女中](../Page/中西女中.md "wikilink")
  - 医院：北京[同仁医院](../Page/同仁医院.md "wikilink")、妇婴医院，天津南关[妇婴医院](../Page/妇婴医院.md "wikilink")，昌黎广济医院，遵化同仁医院，山海关普济医院

### 华西年议会（CHENGTU AND CHUNGKING CONFERNCE）

  - 传教站：[重庆](../Page/重庆.md "wikilink")(1882)；[成都](../Page/成都.md "wikilink")(1892);
    [遂宁](../Page/遂宁.md "wikilink")(1896);资州([资中](../Page/资中.md "wikilink")，1897).
  - 著名教堂：成都陕西街教堂；[重庆社交会堂](../Page/重庆社交会堂.md "wikilink")
  - 教育：[华西协和大学](../Page/华西协和大学.md "wikilink")、成都华美女中；重庆求精中学、淑德女中
  - 医院：成都存仁医院，重庆[宽仁医院](../Page/宽仁医院.md "wikilink")，宽仁女医院，资中宏仁医院，

### 山东年议会（SHANTUNG CONFERENCE）

  - 传教站：[泰安](../Page/泰安.md "wikilink")(1874); 济南.
  - 著名教堂：泰安登云街教堂
  - 医院：
  - 教育：[萃英中学](../Page/萃英中学.md "wikilink")

### 华中年议会（CENTRAL CHINA CONFERENCE）

  - 传教站：[镇江](../Page/镇江.md "wikilink")(1884);[南京](../Page/南京.md "wikilink")(1887);
    [芜湖](../Page/芜湖.md "wikilink")(1895);
  - 著名教堂：南京估衣廊城中堂、昇州路111号卫斯理堂；镇江[大西路福音堂](../Page/大西路福音堂.md "wikilink")（1889年）、小码头福音堂（1932年）；芜湖二街状元坊教堂
  - 上海办事处(1903).
  - 教育：[金陵大学](../Page/金陵大学.md "wikilink")、[金陵女子大学](../Page/金陵女子大学.md "wikilink")；南京金陵大学附中、汇文女中、镇江崇实女中
  - 医院：芜湖[弋矶山医院](../Page/弋矶山医院.md "wikilink")，镇江保黎医院

## 在东南亚

随着福建省美以美会信徒的大批移民，美以美会（今[卫理公会](../Page/卫理公会.md "wikilink")）在[马来西亚拥有大批信徒](../Page/马来西亚.md "wikilink")。

## 重要人物

  - [柯林](../Page/柯林.md "wikilink")（Judson Dwight Collins）
  - [懷特](../Page/懷特.md "wikilink")（Moses Clark White）
  - [麦利和](../Page/麦利和.md "wikilink")（Robert Samuel Maclay）
  - [刘海澜博士](../Page/刘海澜.md "wikilink") (Hiram Harrison Lowry,1843-1924)
  - [力宣德會督](../Page/力宣德.md "wikilink")
  - [黃安素會督](../Page/黃安素.md "wikilink")（Ralph A. Ward，1882－1958）
  - [高智会督](../Page/高智.md "wikilink")
  - [王治平會督](../Page/王治平.md "wikilink") ( Wang Chih Ping )
  - [陳文淵會督](../Page/陳文淵.md "wikilink")
  - [宋尚节博士](../Page/宋尚节.md "wikilink")
  - [薛承恩](../Page/薛承恩.md "wikilink")（Nathan Sites）

## 參考文献

<references />

  -
## 参见

  - [美以美会在华传教士列表](../Page/美以美会在华传教士列表.md "wikilink")

{{-}}

[Category:基督教在华差会](../Category/基督教在华差会.md "wikilink")
[Category:美以美会](../Category/美以美会.md "wikilink")

1.  Townsend (1890), 237-238