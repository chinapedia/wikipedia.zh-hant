汉诺威作为[神圣罗马帝国的一个邦国](../Page/神圣罗马帝国.md "wikilink")，前身是不伦瑞克-卡伦贝格公国，不伦瑞克-卡伦贝格是不伦瑞克-吕讷堡的幼系。该公国1636年被封给[不伦瑞克-吕讷堡公爵](../Page/不伦瑞克统治者列表.md "wikilink")[「胜利者」威廉四世的第六子](../Page/威廉四世_\(不伦瑞克-吕讷堡\).md "wikilink")[格奥尔格](../Page/格奥尔格_\(不伦瑞克-卡伦贝格\).md "wikilink")。不久，该家系将首府迁移到了[汉诺威城](../Page/汉诺威.md "wikilink")。1692年，不伦瑞克-卡伦贝格公爵[恩斯特·奧古斯特被晋升为](../Page/恩斯特·奧古斯特_\(漢諾威选侯\).md "wikilink")[选侯](../Page/选侯.md "wikilink")，他的长子[格奥尔格·路德维希更于](../Page/喬治一世_\(大不列顛\).md "wikilink")1714年继承了[英国的王位](../Page/英国.md "wikilink")。1814年10月，在[维也纳会议上](../Page/维也纳会议.md "wikilink")，汉诺威升为[王国](../Page/王国.md "wikilink")。

以下是[汉诺威历代君主的名单](../Page/汉诺威_\(地区\).md "wikilink")。

## 汉诺威选侯国

  - 1692年-1698年：[恩斯特·奧古斯特](../Page/恩斯特·奧古斯特_\(漢諾威选侯\).md "wikilink")
  - 1698年-1727年：[格奥尔格一世·路德维希](../Page/乔治一世_\(大不列颠\).md "wikilink")（Georg
    I Ludwig）
    <span style="font-size:smaller;">1714年後兼任[大不列顛和爱尔兰國王](../Page/英国君主列表.md "wikilink")</span>
  - 1727年-1760年：[格奥尔格二世·奧古斯特](../Page/乔治二世_\(英国\).md "wikilink")（Georg
    II August） <span style="font-size:smaller;">兼任大不列顛和爱尔兰國王</span>
  - 1760年-1802年：[格奥尔格三世·威廉·弗里德里希](../Page/乔治三世_\(英国\).md "wikilink")（Georg
    III Wilhelm Friedrich）
    <span style="font-size:smaller;">兼任大不列顛和爱尔兰國王，1814年成为汉诺威国王</span>

1802年，汉诺威被法国占领，1807年并入[威斯特法伦王国](../Page/威斯特法伦王国.md "wikilink")。

## 汉诺威王国

  - 1814年-1820年：[格奥尔格三世](../Page/乔治三世_\(英国\).md "wikilink")
  - 1820年-1830年：[格奥尔格四世·奧古斯特·弗里德里希](../Page/喬治四世_\(英國\).md "wikilink")（Georg
    IV August Friedrich）
    <span style="font-size:smaller;">兼任大不列顛和爱尔兰联合王国國王</span>
  - 1830年-1837年：[威廉·亨利](../Page/威廉四世_\(英國\).md "wikilink")（Wilhelm
    Heinrich） <span style="font-size:smaller;">兼任大不列顛和爱尔兰联合王国國王</span>
  - 1837年-1851年：[恩斯特·奧古斯特一世](../Page/恩斯特·奧古斯特一世_\(漢諾威\).md "wikilink")（Ernst
    August I）
    <span style="font-size:smaller;">兼任[英国](../Page/英国.md "wikilink")[坎伯蘭公爵](../Page/坎伯蘭公爵.md "wikilink")</span>
  - 1851年-1866年：[格奥尔格五世](../Page/格奥尔格五世_\(漢諾威\).md "wikilink")（Georg V）
    <span style="font-size:smaller;">兼任[英国](../Page/英国.md "wikilink")[坎伯蘭公爵](../Page/坎伯蘭公爵.md "wikilink")</span>

1866年，汉诺威王国被[普魯士吞并](../Page/普魯士.md "wikilink")。[格奥尔格五世的孙子](../Page/格奥尔格五世_\(漢諾威\).md "wikilink")[恩斯特·奧古斯特曾在](../Page/恩斯特·奧古斯特三世_\(漢諾威\).md "wikilink")1913年至1918年间任不伦瑞克公国的公爵。

## 汉诺威王位继承人

  - 1866年-1878年：[格奥尔格五世](../Page/格奥尔格五世_\(漢諾威\).md "wikilink")
  - 1878年-1923年：[恩斯特·奧古斯特二世](../Page/恩斯特·奧古斯特二世_\(漢諾威\).md "wikilink")（Ernst
    August II）
    <span style="font-size:smaller;">1884年至1885年成为挂名的[不伦瑞克公爵](../Page/不伦瑞克统治者列表.md "wikilink")，1878年至1919年为英国坎伯蘭公爵</span>
  - 1923年-1953年：[恩斯特·奧古斯特三世](../Page/恩斯特·奧古斯特三世_\(漢諾威\).md "wikilink")（Ernst
    August III）
    <span style="font-size:smaller;">1913年至1918年为不伦瑞克公爵</span>
  - 1953年-1987年：[恩斯特·奧古斯特四世](../Page/恩斯特·奧古斯特四世_\(漢諾威\).md "wikilink")（Ernst
    August IV）
  - 1987年至今：[恩斯特·奧古斯特五世](../Page/恩斯特·奧古斯特五世_\(漢諾威\).md "wikilink")（Ernst
    August V）

## 参见

  - [不伦瑞克统治者列表](../Page/不伦瑞克统治者列表.md "wikilink")
  - [汉诺威 (地区)](../Page/汉诺威_\(地区\).md "wikilink")
  - [汉诺威王朝](../Page/汉诺威王朝.md "wikilink")
  - [韦尔夫家族](../Page/韦尔夫家族.md "wikilink")

[en:Rulers of Hanover](../Page/en:Rulers_of_Hanover.md "wikilink")

[H](../Category/德国君主列表.md "wikilink")
[H](../Category/德國相關列表.md "wikilink")
[H](../Category/神圣罗马帝国.md "wikilink")
[H](../Category/韦尔夫王朝.md "wikilink")