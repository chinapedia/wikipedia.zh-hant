[JiaoDaFeng.jpg](https://zh.wikipedia.org/wiki/File:JiaoDaFeng.jpg "fig:JiaoDaFeng.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Changsha_Yuelu_Shan_Jiao_Dafeng_Mu_2014.03.04_10-14-24.jpg "fig:缩略图")\]\]
**焦达峰**（），原名**焦大鹏**，字**鞠荪**，曾加入[哥老会](../Page/哥老会.md "wikilink")，後為中国[同盟会成员](../Page/同盟会.md "wikilink")，[共进会领袖](../Page/共进会.md "wikilink")，[辛亥革命时任](../Page/辛亥革命.md "wikilink")[中华民国军政府湖南都督府都督](../Page/中华民国.md "wikilink")。

## 生平

  - 1887年1月16日
    出生于中国[湖南省](../Page/湖南省.md "wikilink")[浏阳县龙伏镇焦家桥](../Page/浏阳县.md "wikilink")。
  - 1899年 入浏阳县[南台书院小学就读](../Page/南台书院.md "wikilink")。
  - 1902年 加入[洪江会](../Page/洪江会.md "wikilink")，开始参与会党活动。
  - 1903年
    入[长沙高等学堂游学预备科学习日文](../Page/长沙高等学堂.md "wikilink")，并在[华兴会的东文讲习所学习](../Page/华兴会.md "wikilink")，后加入华兴会外围组织同仇会，与[黄兴](../Page/黄兴.md "wikilink")、[禹之谟等交往密切](../Page/禹之谟.md "wikilink")。
  - 1904年 赴日本留学，入东京铁道学校学习铁路管理。
  - 1905年 在东京加入同盟会。
  - 1906年
    任同盟会联络部长，负责联络中国地下会党，将同盟会活动范围由南方沿海推进到长脚流域。同年回中国参加[萍浏醴起义](../Page/萍浏醴起义.md "wikilink")，任李金奇参谋，起义失败后返回日本，组织四进社。
  - 1907年
    在东京东斌学堂学习军事。与[孙武](../Page/孙武_\(民国\).md "wikilink")、[张百祥等成立](../Page/张百祥.md "wikilink")[共进会](../Page/共进会.md "wikilink")，将同盟会宗旨中的“平均地权”改为“平均人权”。
  - 1908年 回到中国与孙武等谋划两湖军事暴动，创建共进会湖南总堂，任龙头大哥。
  - 1911年4月
    图谋响应广州[黄花岗起义](../Page/黄花岗起义.md "wikilink")，未果而避居[武汉](../Page/武汉.md "wikilink")。与湖北共进会孙武、居正等商议两湖起义，订下“长沙先发难，武汉立即响应；武汉先发难，长沙立即响应”之盟约。
  - 1911年10月22日
    与[陈作新率湖南新军最先响应](../Page/陈作新.md "wikilink")[武昌起义](../Page/武昌起义.md "wikilink")，攻占[长沙](../Page/长沙.md "wikilink")，次日建立湖南军政府，被推举为都督。
  - 1911年10月28日 派出援鄂军从长沙出发支援武昌。
  - 1911年10月31日
    被从邵阳赶到长沙的[新军第25混成协第](../Page/新军第25混成协.md "wikilink")50标第2营管带梅馨杀害，同一天陈作新也被杀害。随后梅馨迎立[谭延闿任湖南都督](../Page/谭延闿.md "wikilink")。
  - 1912年
    中华民国临时总统[孙中山在南京追授焦达峰大将军衔](../Page/孙中山.md "wikilink")，遗体安葬于长沙[岳麓山](../Page/岳麓山.md "wikilink")。

## 纪念地

  - 焦达峰故居 位于中国湖南省浏阳市龙伏镇达峰村，北侧有焦达峰之妻沈菁莪用孙中山恤银所建之达峰纪念堂。
  - 焦达峰墓 位于中国湖南省长沙市岳麓山，麓山寺东侧，有[刘人熙所撰之碑文](../Page/刘人熙.md "wikilink")。

[Category:辛亥革命人物](../Category/辛亥革命人物.md "wikilink")
[Category:清朝遇刺身亡者](../Category/清朝遇刺身亡者.md "wikilink")
[J焦](../Category/浏阳人.md "wikilink")
[Category:焦姓](../Category/焦姓.md "wikilink")