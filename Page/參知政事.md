**參知政事**是[中國歷史上的一個官職](../Page/中國歷史.md "wikilink")，於[唐朝首先設置](../Page/唐朝.md "wikilink")，並非正式官位，而是[三省](../Page/三省.md "wikilink")[長官以外的](../Page/長官.md "wikilink")[官員](../Page/官員.md "wikilink")，加上此頭銜（或[同中書門下三品](../Page/同中書門下三品.md "wikilink")、[同中書門下平章事等的頭銜](../Page/同中書門下平章事.md "wikilink")），即可以進入[政事堂議事](../Page/政事堂.md "wikilink")，等於成為[宰相群的一員](../Page/宰相.md "wikilink")。

[宋代](../Page/宋代.md "wikilink")[太祖時](../Page/趙匡胤.md "wikilink")，為分割宰相權力，故在[同平章事](../Page/同平章事.md "wikilink")（即宰相，簡稱[平章](../Page/平章.md "wikilink")），底下設參知政事（簡稱[參知](../Page/參知.md "wikilink")、[參政](../Page/參政.md "wikilink")），原不知印，不升政事堂，為宰相的副職，開寶六年始升都堂議政，至道元年與宰相輪班知印，後來實際權力大致等於宰相，[遼](../Page/遼.md "wikilink")、[金](../Page/金.md "wikilink")、[元等朝多沿用此制](../Page/元.md "wikilink")。**參知政事**一職，[明代洪武九年以後不存](../Page/明代.md "wikilink")，[清代的](../Page/清代.md "wikilink")[協辦大學士有時亦美稱](../Page/協辦大學士.md "wikilink")**參知**，但已為虛職而無實權。

地方亦有參政一職，洪武九年，廢行省平章政事、左右丞，改參知政事為布政使，以左、右參政為布政使之副職。[清初略略沿用](../Page/清.md "wikilink")，[乾隆時廢](../Page/乾隆.md "wikilink")。

## 历任

  - 唐朝

<!-- end list -->

  - [杜淹](../Page/杜淹.md "wikilink")（627-628參豫朝政）
  - [魏徵](../Page/魏徵.md "wikilink")（629-632參豫朝政，636-642參議得失）
  - [萧瑀](../Page/萧瑀.md "wikilink")（630參議朝政，635-636參豫朝政）
  - [戴胄](../Page/戴胄.md "wikilink")（630-633參豫朝政）
  - [侯君集](../Page/侯君集.md "wikilink")（630-632, 632-643，參豫朝政）
  - [刘洎](../Page/刘洎.md "wikilink")（639-644參知政事）
  - [岑文本](../Page/岑文本.md "wikilink")（642-644專典機密）
  - [高季辅](../Page/高季辅.md "wikilink")（645同掌机务）
  - [张行成](../Page/张行成.md "wikilink")（645同掌机务）
  - [崔仁师](../Page/崔仁师.md "wikilink")（648參知機務）
  - [李义府](../Page/李义府.md "wikilink")（655-657參知政事）
  - [卢承庆](../Page/卢承庆.md "wikilink")（659參知政事）
  - [乐彦玮](../Page/乐彦玮.md "wikilink")（665知軍國政事）
  - [孙处约](../Page/孙处约.md "wikilink")（665知軍國政事）
  - [刘仁轨](../Page/刘仁轨.md "wikilink")（665-666知政事）
  - [张文瓘](../Page/张文瓘.md "wikilink")（667-669參知政事）
  - [李旦](../Page/李旦.md "wikilink")（710參謀政事）
  - [刘幽求](../Page/刘幽求.md "wikilink")（710-711參豫機務，713知軍國重事）
  - [钟绍京](../Page/钟绍京.md "wikilink")（710參豫機務）
  - [薛稷](../Page/薛稷.md "wikilink")（710參豫機務）
  - [崔日用](../Page/崔日用.md "wikilink")（710參豫機務）
  - [李知柔](../Page/李知柔.md "wikilink")（895權知中書事）
  - [卢光启](../Page/卢光启.md "wikilink")（901權句當中書事，901-902參知機務）

<!-- end list -->

  - 宋朝

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/薛居正.md" title="wikilink">薛居正</a>（964-976）</li>
<li><a href="../Page/吕余庆.md" title="wikilink">吕余庆</a>（964-965,968-973）</li>
<li><a href="../Page/刘熙古.md" title="wikilink">刘熙古</a>（972-973）</li>
<li><a href="../Page/卢多逊.md" title="wikilink">卢多逊</a>（973-976）</li>
<li><a href="../Page/窦偁.md" title="wikilink">窦偁</a>（982）</li>
<li><a href="../Page/郭贽.md" title="wikilink">郭贽</a>（982-983）</li>
<li><a href="../Page/宋琪.md" title="wikilink">宋琪</a>（983）</li>
<li><a href="../Page/李昉.md" title="wikilink">李昉</a>（983）</li>
<li><a href="../Page/李穆_(北宋).md" title="wikilink">李穆</a>（983-984）</li>
<li><a href="../Page/吕蒙正.md" title="wikilink">吕蒙正</a>（983-988）</li>
<li><a href="../Page/李至.md" title="wikilink">李至</a>（983-986）</li>
<li><a href="../Page/辛仲甫.md" title="wikilink">辛仲甫</a>（986-991）</li>
<li><a href="../Page/王沔.md" title="wikilink">王沔</a>（988-991）</li>
<li><a href="../Page/张齐贤.md" title="wikilink">张齐贤</a>（991）</li>
<li><a href="../Page/陈恕.md" title="wikilink">陈恕</a>（991）</li>
<li><a href="../Page/贾黄中.md" title="wikilink">贾黄中</a>（991-993）</li>
<li><a href="../Page/李沆.md" title="wikilink">李沆</a>（991-993，997-998）</li>
<li><a href="../Page/吕端.md" title="wikilink">吕端</a>（993-995）</li>
<li><a href="../Page/苏易简.md" title="wikilink">苏易简</a>（993-995）</li>
<li><a href="../Page/赵昌言.md" title="wikilink">赵昌言</a>（993-995）</li>
<li><a href="../Page/寇准.md" title="wikilink">寇准</a>（994-995）</li>
<li><a href="../Page/張洎_(南唐).md" title="wikilink">张洎</a>（994-997）</li>
<li><a href="../Page/李昌龄.md" title="wikilink">李昌龄</a>（996-997）</li>
<li><a href="../Page/温仲舒.md" title="wikilink">温仲舒</a>（997-998）</li>
<li><a href="../Page/王化基.md" title="wikilink">王化基</a>（997-1001）</li>
<li><a href="../Page/李至.md" title="wikilink">李至</a>（997-998）</li>
<li><a href="../Page/向敏中.md" title="wikilink">向敏中</a>（998-1001）</li>
<li><a href="../Page/王旦.md" title="wikilink">王旦</a>（1001-1006）</li>
<li><a href="../Page/王钦若.md" title="wikilink">王钦若</a>（1001-1005）</li>
<li><a href="../Page/毕士安.md" title="wikilink">毕士安</a>（1004）</li>
<li><a href="../Page/冯拯.md" title="wikilink">冯拯</a>（1005-1011）</li>
<li><a href="../Page/赵安仁.md" title="wikilink">赵安仁</a>（1006-1012）</li>
<li><a href="../Page/丁谓.md" title="wikilink">丁谓</a>（1012-1016，1019）</li>
<li><a href="../Page/王曾.md" title="wikilink">王曾</a>（1016-1017）</li>
<li><a href="../Page/张知白.md" title="wikilink">张知白</a>（1016-1018）</li>
<li><a href="../Page/陈彭年.md" title="wikilink">陈彭年</a>（1016-1017）</li>
<li><a href="../Page/李迪.md" title="wikilink">李迪</a>（1017-1020）</li>
<li><a href="../Page/任中正.md" title="wikilink">任中正</a>（1020-1022）</li>
<li><a href="../Page/吕夷简.md" title="wikilink">吕夷简</a>（1022-1029）</li>
<li><a href="../Page/鲁宗道.md" title="wikilink">鲁宗道</a>（1022-1029）</li>
<li><a href="../Page/夏竦.md" title="wikilink">夏竦</a>（1029）</li>
<li><a href="../Page/薛奎.md" title="wikilink">薛奎</a>（1029-1033）</li>
<li><a href="../Page/陈尧佐.md" title="wikilink">陈尧佐</a>（1029-1033）</li>
<li><a href="../Page/王曙.md" title="wikilink">王曙</a>（1029-1032）</li>
<li><a href="../Page/晏殊.md" title="wikilink">晏殊</a>（1032-1033）</li>
<li><a href="../Page/王随.md" title="wikilink">王随</a>（1033-1035）</li>
<li><a href="../Page/宋绶.md" title="wikilink">宋绶</a>（1033-1037，1040）</li>
<li><a href="../Page/蔡齐.md" title="wikilink">蔡齐</a>（1035-1037）</li>
<li><a href="../Page/盛度.md" title="wikilink">盛度</a>（1035-1037，1038-1039）</li>
<li><a href="../Page/韩亿.md" title="wikilink">韩亿</a>（1037-1038）</li>
<li><a href="../Page/程琳_(北宋).md" title="wikilink">程琳</a>（1037-1039）</li>
<li><a href="../Page/石中立.md" title="wikilink">石中立</a>（1037-1038）</li>
<li><a href="../Page/王鬷.md" title="wikilink">王鬷</a>（1038-1039）</li>
<li><a href="../Page/李若谷_(北宋).md" title="wikilink">李若谷</a>（1038-1040）</li>
<li><a href="../Page/宋庠.md" title="wikilink">宋庠</a>（1039-1041，1045-1048）</li>
<li><a href="../Page/晁宗悫.md" title="wikilink">晁宗悫</a>（1040-1042）</li>
<li><a href="../Page/王举正.md" title="wikilink">王举正</a>（1041-1043）</li>
<li><a href="../Page/贾昌朝.md" title="wikilink">贾昌朝</a>（1043-1044）</li>
<li><a href="../Page/范仲淹.md" title="wikilink">范仲淹</a>（1043-1044）</li>
<li><a href="../Page/陈执中.md" title="wikilink">陈执中</a>（1044-1045）</li>
<li><a href="../Page/丁度.md" title="wikilink">丁度</a>（1046-1044）</li>
<li><a href="../Page/文彦博.md" title="wikilink">文彦博</a>（1047-1048）</li>
<li><a href="../Page/明镐.md" title="wikilink">明镐</a>（1048）</li>
<li><a href="../Page/庞籍.md" title="wikilink">庞籍</a>（1048-1049）</li>
<li><a href="../Page/高若讷.md" title="wikilink">高若讷</a>（1049-1051）</li>
<li><a href="../Page/刘沆.md" title="wikilink">刘沆</a>（1051-1054）</li>
<li><a href="../Page/梁适.md" title="wikilink">梁适</a>（1051-1053）</li>
<li><a href="../Page/程戡.md" title="wikilink">程戡</a>（1054-1056）</li>
<li><a href="../Page/王尧臣.md" title="wikilink">王尧臣</a>（1056-1058）</li>
<li><a href="../Page/曾公亮.md" title="wikilink">曾公亮</a>（1056-1060）</li>
<li><a href="../Page/孙抃.md" title="wikilink">孙抃</a>（1060-1062）</li>
<li><a href="../Page/张昪.md" title="wikilink">张昪</a>（1060-1061）</li>
<li><a href="../Page/欧阳修.md" title="wikilink">欧阳修</a>（1061-1067）</li>
<li><a href="../Page/赵槩.md" title="wikilink">赵槩</a>（1062-1068）</li>
<li><a href="../Page/吴奎.md" title="wikilink">吴奎</a>（1067）</li>
<li><a href="../Page/张方平.md" title="wikilink">张方平</a>（1067）</li>
<li><a href="../Page/赵抃.md" title="wikilink">赵抃</a>（1067-1070）</li>
<li><a href="../Page/唐介.md" title="wikilink">唐介</a>（1068-1069）</li>
<li><a href="../Page/王安石.md" title="wikilink">王安石</a>（1069-1070）</li>
<li><a href="../Page/韩绛.md" title="wikilink">韩绛</a>（1070）</li>
<li><a href="../Page/冯京.md" title="wikilink">冯京</a>（1070-1075）</li>
<li><a href="../Page/王珪.md" title="wikilink">王珪</a>（1070-1076）</li>
<li><a href="../Page/吕惠卿.md" title="wikilink">吕惠卿</a>（1074-1075）</li>
<li><a href="../Page/元绛.md" title="wikilink">元绛</a>（1075-1079）</li>
<li><a href="../Page/蔡确.md" title="wikilink">蔡确</a>（1079-1082）</li>
<li><a href="../Page/章惇.md" title="wikilink">章惇</a>（1080-1081）</li>
<li><a href="../Page/張璪_(宋朝).md" title="wikilink">张璪</a>（1081-1082）</li>
</ul>
<p>改为<a href="../Page/门下侍郎.md" title="wikilink">门下侍郎</a>、<a href="../Page/中书侍郎.md" title="wikilink">中书侍郎</a>、<a href="../Page/尚书左丞.md" title="wikilink">尚书左丞</a>、<a href="../Page/尚书右丞.md" title="wikilink">尚书右丞</a></p></td>
<td><ul>
<li><a href="../Page/李邴.md" title="wikilink">李邴</a>（1129）</li>
<li><a href="../Page/王绹.md" title="wikilink">王绹</a>（1129-1130）</li>
<li><a href="../Page/范宗尹.md" title="wikilink">范宗尹</a>（1129-1130）</li>
<li><a href="../Page/张守.md" title="wikilink">张守</a>（1130-1131，1136-1138）</li>
<li><a href="../Page/谢克家.md" title="wikilink">谢克家</a>（1130-1131）</li>
<li><a href="../Page/秦桧.md" title="wikilink">秦桧</a>（1131）</li>
<li><a href="../Page/李回.md" title="wikilink">李回</a>（1131）</li>
<li><a href="../Page/孟庾.md" title="wikilink">孟庾</a>（1131-1135）</li>
<li><a href="../Page/翟汝文.md" title="wikilink">翟汝文</a>（1132）</li>
<li><a href="../Page/席益.md" title="wikilink">席益</a>（1133-1134）</li>
<li><a href="../Page/赵鼎.md" title="wikilink">赵鼎</a>（1134）</li>
<li><a href="../Page/沈与求.md" title="wikilink">沈与求</a>（1134-1136）</li>
<li><a href="../Page/陈与义.md" title="wikilink">陈与义</a>（1137-1138）</li>
<li><a href="../Page/刘大中.md" title="wikilink">刘大中</a>（1138）</li>
<li><a href="../Page/孙近.md" title="wikilink">孙近</a>（1138-1141）</li>
<li><a href="../Page/李光.md" title="wikilink">李光</a>（1138-1139）</li>
<li><a href="../Page/王次翁.md" title="wikilink">王次翁</a>（1140-1143）</li>
<li><a href="../Page/范同.md" title="wikilink">范同</a>（1141）</li>
<li><a href="../Page/何铸.md" title="wikilink">何铸</a>（1142）</li>
<li><a href="../Page/万俟卨.md" title="wikilink">万俟卨</a>（1142-1144，1156）</li>
<li><a href="../Page/程克俊.md" title="wikilink">程克俊</a>（1142-1143）</li>
<li><a href="../Page/楼炤.md" title="wikilink">楼炤</a>（1144）</li>
<li><a href="../Page/李文会.md" title="wikilink">李文会</a>（1144）</li>
<li><a href="../Page/李若谷_(南宋).md" title="wikilink">李若谷</a>（1145-1147）</li>
<li><a href="../Page/段拂.md" title="wikilink">段拂</a>（1147-1148）</li>
<li><a href="../Page/汪勃.md" title="wikilink">汪勃</a>（1148）</li>
<li><a href="../Page/詹大方.md" title="wikilink">詹大方</a>（1148）</li>
<li><a href="../Page/余尧弼.md" title="wikilink">余尧弼</a>（1148-1151）</li>
<li><a href="../Page/巫伋.md" title="wikilink">巫伋</a>（1151-1152）</li>
<li><a href="../Page/章复.md" title="wikilink">章复</a>（1152）</li>
<li><a href="../Page/宋朴.md" title="wikilink">宋朴</a>（1152-1153）</li>
<li><a href="../Page/史才.md" title="wikilink">史才</a>（1153-1154）</li>
<li><a href="../Page/魏师逊.md" title="wikilink">魏师逊</a>（1154）</li>
<li><a href="../Page/施钜.md" title="wikilink">施钜</a>（1154-1155）</li>
<li><a href="../Page/郑仲熊.md" title="wikilink">郑仲熊</a>（1154-1155）</li>
<li><a href="../Page/董德元.md" title="wikilink">董德元</a>（1155）</li>
<li><a href="../Page/汤思退.md" title="wikilink">汤思退</a>（1155-1157）</li>
<li><a href="../Page/魏良臣_(宋朝).md" title="wikilink">魏良臣</a>（1155-1156）</li>
<li><a href="../Page/沈该.md" title="wikilink">沈该</a>（1155-1156）</li>
<li><a href="../Page/程克俊.md" title="wikilink">程克俊</a>（1156）</li>
<li><a href="../Page/张纲.md" title="wikilink">张纲</a>（1156-1157）</li>
<li><a href="../Page/汤鹏举.md" title="wikilink">汤鹏举</a>（1157）</li>
<li><a href="../Page/陈康伯.md" title="wikilink">陈康伯</a>（1157-1159）</li>
<li><a href="../Page/贺允中.md" title="wikilink">贺允中</a>（1159-1160，1164）</li>
<li><a href="../Page/朱倬.md" title="wikilink">朱倬</a>（1160-1161）</li>
<li><a href="../Page/杨椿.md" title="wikilink">杨椿</a>（1161）</li>
<li><a href="../Page/汪澈.md" title="wikilink">汪澈</a>（1162-1163）</li>
<li><a href="../Page/史浩.md" title="wikilink">史浩</a>（1162-1163）</li>
<li><a href="../Page/张焘.md" title="wikilink">张焘</a>（1163）</li>
<li><a href="../Page/辛次膺.md" title="wikilink">辛次膺</a>（1163）</li>
<li><a href="../Page/周葵.md" title="wikilink">周葵</a>（1163-1164）</li>
<li><a href="../Page/王之望.md" title="wikilink">王之望</a>（1164）</li>
<li><a href="../Page/钱端礼.md" title="wikilink">钱端礼</a>（1164-1165）</li>
<li><a href="../Page/虞允文.md" title="wikilink">虞允文</a>（1164-1165）</li>
<li><a href="../Page/洪适.md" title="wikilink">洪适</a>（1165）</li>
<li><a href="../Page/叶颙.md" title="wikilink">叶颙</a>（1165-1166）</li>
<li><a href="../Page/魏杞.md" title="wikilink">魏杞</a>（1166）</li>
<li><a href="../Page/林安宅.md" title="wikilink">林安宅</a>（1166）</li>
<li><a href="../Page/蒋芾.md" title="wikilink">蒋芾</a>（1166-1168）</li>
<li><a href="../Page/陈俊卿_(南宋).md" title="wikilink">陈俊卿</a>（1166-1168）</li>
<li><a href="../Page/刘珙.md" title="wikilink">刘珙</a>（1168）</li>
<li><a href="../Page/王炎_(宋朝).md" title="wikilink">王炎</a>（1169-1173）</li>
<li><a href="../Page/梁克家.md" title="wikilink">梁克家</a>（1169-1172）</li>
<li><a href="../Page/曾怀.md" title="wikilink">曾怀</a>（1172-1173）</li>
<li><a href="../Page/郑闻.md" title="wikilink">郑闻</a>（1173-1174）</li>
<li><a href="../Page/姚宪.md" title="wikilink">姚宪</a>（1174）</li>
<li><a href="../Page/叶衡.md" title="wikilink">叶衡</a>（1174）</li>
<li><a href="../Page/龚茂良.md" title="wikilink">龚茂良</a>（1174-1177）</li>
<li><a href="../Page/李彦颖.md" title="wikilink">李彦颖</a>（1175-1178）</li>
<li><a href="../Page/王淮.md" title="wikilink">王淮</a>（1177-1178）</li>
<li><a href="../Page/范成大.md" title="wikilink">范成大</a>（1178）</li>
<li><a href="../Page/赵雄.md" title="wikilink">赵雄</a>（1178）</li>
<li><a href="../Page/钱良臣.md" title="wikilink">钱良臣</a>（1178-1181）</li>
<li><a href="../Page/周必大.md" title="wikilink">周必大</a>（1180-1182）</li>
<li><a href="../Page/谢廓然.md" title="wikilink">谢廓然</a>（1181-1182）</li>
<li><a href="../Page/施师点.md" title="wikilink">施师点</a>（1182-1187）</li>
<li><a href="../Page/黄洽.md" title="wikilink">黄洽</a>（1183-1189）</li>
<li><a href="../Page/留正.md" title="wikilink">留正</a>（1186-1189）</li>
<li><a href="../Page/王蔺.md" title="wikilink">王蔺</a>（1189-1190）</li>
<li><a href="../Page/葛邲.md" title="wikilink">葛邲</a>（1189-1190）</li>
<li><a href="../Page/萧燧.md" title="wikilink">萧燧</a>（1189）</li>
<li><a href="../Page/胡晋臣.md" title="wikilink">胡晋臣</a>（1190-1193）</li>
<li><a href="../Page/陈骙.md" title="wikilink">陈骙</a>（1193-1194）</li>
<li><a href="../Page/赵汝愚.md" title="wikilink">赵汝愚</a>（1193-1194）</li>
<li><a href="../Page/余端礼.md" title="wikilink">余端礼</a>（1194-1195）</li>
<li><a href="../Page/京镗.md" title="wikilink">京镗</a>（1194-1195）</li>
<li><a href="../Page/郑侨.md" title="wikilink">郑侨</a>（1195-1196）</li>
<li><a href="../Page/謝深甫.md" title="wikilink">謝深甫</a>（1196-1198）</li>
<li><a href="../Page/何澹.md" title="wikilink">何澹</a>（1196-1200）</li>
</ul></td>
<td><ul>
<li><a href="../Page/陈自强.md" title="wikilink">陈自强</a>（1201-1203）</li>
<li><a href="../Page/张岩.md" title="wikilink">张岩</a>（1201-1203，1204-1207）</li>
<li><a href="../Page/许及之.md" title="wikilink">许及之</a>（1202-1203）</li>
<li><a href="../Page/袁说友.md" title="wikilink">袁说友</a>（1203）</li>
<li><a href="../Page/费士寅.md" title="wikilink">费士寅</a>（1203-1205）</li>
<li><a href="../Page/张孝伯.md" title="wikilink">张孝伯</a>（1204）</li>
<li><a href="../Page/钱象祖.md" title="wikilink">钱象祖</a>（1205-1207）</li>
<li><a href="../Page/李壁.md" title="wikilink">李壁</a>（1206-1207）</li>
<li><a href="../Page/卫泾.md" title="wikilink">卫泾</a>（1207-1208）</li>
<li><a href="../Page/雷孝友.md" title="wikilink">雷孝友</a>（1207-1208）</li>
<li><a href="../Page/史弥远.md" title="wikilink">史弥远</a>（1208）</li>
<li><a href="../Page/娄机.md" title="wikilink">娄机</a>（1208-1210）</li>
<li><a href="../Page/楼钥.md" title="wikilink">楼钥</a>（1209-1211）</li>
<li><a href="../Page/章良能.md" title="wikilink">章良能</a>（1213）</li>
<li><a href="../Page/郑昭先.md" title="wikilink">郑昭先</a>（1214-1215）</li>
<li><a href="../Page/曾从龙.md" title="wikilink">曾从龙</a>（1219-1235）</li>
<li><a href="../Page/任希夷.md" title="wikilink">任希夷</a>（1220-1221）</li>
<li><a href="../Page/宣缯.md" title="wikilink">宣缯</a>（1221-1227）</li>
<li><a href="../Page/俞应符.md" title="wikilink">俞应符</a>（1221-1222）</li>
<li><a href="../Page/薛极.md" title="wikilink">薛极</a>（1225-1234）</li>
<li><a href="../Page/葛洪_(南宋).md" title="wikilink">葛洪</a>（1228-1235）</li>
<li><a href="../Page/乔行簡.md" title="wikilink">乔行簡</a>（1233-1235）</li>
<li><a href="../Page/陈贵谊.md" title="wikilink">陈贵谊</a>（1233-1234）</li>
<li><a href="../Page/真德秀.md" title="wikilink">真德秀</a>（1235）</li>
<li><a href="../Page/崔与之.md" title="wikilink">崔与之</a>（1235-1236）</li>
<li><a href="../Page/郑性之.md" title="wikilink">郑性之</a>（1236-1255）</li>
<li><a href="../Page/李鸣复.md" title="wikilink">李鸣复</a>（1236-1244）</li>
<li><a href="../Page/邹应龙.md" title="wikilink">邹应龙</a>（1237）</li>
<li><a href="../Page/李宗勉.md" title="wikilink">李宗勉</a>（1238-1239）</li>
<li><a href="../Page/余天锡.md" title="wikilink">余天锡</a>（1239-1241）</li>
<li><a href="../Page/游似.md" title="wikilink">游似</a>（1239-1242）</li>
<li><a href="../Page/范鍾.md" title="wikilink">范鍾</a>（1240-1244）</li>
<li><a href="../Page/徐荣叟.md" title="wikilink">徐荣叟</a>（1242）</li>
<li><a href="../Page/别之杰.md" title="wikilink">别之杰</a>（1242，1247-1248）</li>
<li><a href="../Page/高定子.md" title="wikilink">高定子</a>（1243-1247）</li>
<li><a href="../Page/刘伯正.md" title="wikilink">刘伯正</a>（1244-1245）</li>
<li><a href="../Page/李性传.md" title="wikilink">李性传</a>（1245）</li>
<li><a href="../Page/赵葵.md" title="wikilink">赵葵</a>（1245-1249）</li>
<li><a href="../Page/陳韡.md" title="wikilink">陳韡</a>（1245-1247）</li>
<li><a href="../Page/王伯大.md" title="wikilink">王伯大</a>（1247-1248）</li>
<li><a href="../Page/吴潜.md" title="wikilink">吴潜</a>（1247-1251）</li>
<li><a href="../Page/应𢖟.md" title="wikilink">应𢖟</a>（1248-1249）</li>
<li><a href="../Page/谢方叔.md" title="wikilink">谢方叔</a>（1248-1251）</li>
<li><a href="../Page/徐清叟.md" title="wikilink">徐清叟</a>（1252-1255）</li>
<li><a href="../Page/董槐.md" title="wikilink">董槐</a>（1253-1255）</li>
<li><a href="../Page/李曾伯.md" title="wikilink">李曾伯</a>（1254）</li>
<li><a href="../Page/贾似道.md" title="wikilink">贾似道</a>（1256-1257）</li>
<li><a href="../Page/程元凤.md" title="wikilink">程元凤</a>（1256）</li>
<li><a href="../Page/蔡抗.md" title="wikilink">蔡抗</a>（1256）</li>
<li><a href="../Page/吴渊.md" title="wikilink">吴渊</a>（1257）</li>
<li><a href="../Page/张磻.md" title="wikilink">张磻</a>（1257）</li>
<li><a href="../Page/丁大全.md" title="wikilink">丁大全</a>（1257-1258）</li>
<li><a href="../Page/林存.md" title="wikilink">林存</a>（1258）</li>
<li><a href="../Page/朱熠.md" title="wikilink">朱熠</a>（1258-1261）</li>
<li><a href="../Page/饶虎臣.md" title="wikilink">饶虎臣</a>（1259-1260）</li>
<li><a href="../Page/戴庆炣.md" title="wikilink">戴庆炣</a>（1259-1260）</li>
<li><a href="../Page/皮龙荣.md" title="wikilink">皮龙荣</a>（1260-1261）</li>
<li><a href="../Page/沈炎.md" title="wikilink">沈炎</a>（1261）</li>
<li><a href="../Page/何梦然.md" title="wikilink">何梦然</a>（1261-1264）</li>
<li><a href="../Page/孙附凤.md" title="wikilink">孙附凤</a>（1262）</li>
<li><a href="../Page/杨栋.md" title="wikilink">杨栋</a>（1262-1265）</li>
<li><a href="../Page/叶梦鼎.md" title="wikilink">叶梦鼎</a>（1264-1267）</li>
<li><a href="../Page/姚希得.md" title="wikilink">姚希得</a>（1264-1266，1275）</li>
<li><a href="../Page/江万里.md" title="wikilink">江万里</a>（1265-1269）</li>
<li><a href="../Page/王爚.md" title="wikilink">王爚</a>（1265-1266，1267）</li>
<li><a href="../Page/留梦炎.md" title="wikilink">留梦炎</a>（1267）</li>
<li><a href="../Page/马光祖.md" title="wikilink">马光祖</a>（1267-1269）</li>
<li><a href="../Page/常挺.md" title="wikilink">常挺</a>（1267-1268）</li>
<li><a href="../Page/马廷鸾.md" title="wikilink">马廷鸾</a>（1268-1269）</li>
<li><a href="../Page/陈宗礼.md" title="wikilink">陈宗礼</a>（1270）</li>
<li><a href="../Page/赵顺孙.md" title="wikilink">赵顺孙</a>（1270-1274）</li>
<li><a href="../Page/章鉴.md" title="wikilink">章鉴</a>（1272-1274）</li>
<li><a href="../Page/陈宜中.md" title="wikilink">陈宜中</a>（1274-1275）</li>
<li><a href="../Page/高斯得.md" title="wikilink">高斯得</a>（1275）</li>
<li><a href="../Page/李庭芝.md" title="wikilink">李庭芝</a>（1275-1276）</li>
<li><a href="../Page/陈文龙.md" title="wikilink">陈文龙</a>（1275-1277）</li>
<li><a href="../Page/黄镛.md" title="wikilink">黄镛</a>（1275-1276）</li>
<li><a href="../Page/常懋.md" title="wikilink">常懋</a>（1276）</li>
<li><a href="../Page/全允坚.md" title="wikilink">全允坚</a>（1276）</li>
<li><a href="../Page/刘黻.md" title="wikilink">刘黻</a>（1276）</li>
<li><a href="../Page/曾渊子.md" title="wikilink">曾渊子</a>（1278）</li>
</ul></td>
</tr>
</tbody>
</table>

  - 明朝

<!-- end list -->

  - [杨宪](../Page/杨宪.md "wikilink")
  - [傅瓛](../Page/傅瓛.md "wikilink")
  - [汪广洋](../Page/汪广洋.md "wikilink")
  - [刘惟敬](../Page/刘惟敬.md "wikilink")
  - [蔡哲](../Page/蔡哲_\(明初官员\).md "wikilink")
  - [陈亮](../Page/陈亮_\(明朝\).md "wikilink")
  - [睢稼](../Page/睢稼.md "wikilink")
  - [侯至善](../Page/侯至善.md "wikilink")
  - [胡惟庸](../Page/胡惟庸.md "wikilink")
  - [李谦](../Page/李谦.md "wikilink")
  - [宋冕](../Page/宋冕.md "wikilink")
  - [丁玉](../Page/丁玉.md "wikilink")
  - [冯冕](../Page/冯冕.md "wikilink")
  - [侯善](../Page/侯善.md "wikilink")
  - [殷哲](../Page/殷哲.md "wikilink")
  - [方鼐](../Page/方鼐.md "wikilink")

## 参考文献

  - 书籍

<!-- end list -->

  - 黃本驥：《歷代職官表》（台北：樂天出版社，1974）

## 参见

  - [宰相](../Page/宰相.md "wikilink")
  - [同中書門下平章事](../Page/同中書門下平章事.md "wikilink")
  - [軍機處](../Page/軍機處.md "wikilink")
  - [大學士](../Page/大學士.md "wikilink")
  - [協辦大學士](../Page/協辦大學士.md "wikilink")
  - [明朝首辅列表](../Page/明朝首辅列表.md "wikilink")

{{-}}

[Category:唐朝官制](../Category/唐朝官制.md "wikilink")
[Category:宋朝官制](../Category/宋朝官制.md "wikilink")
[Category:宰相](../Category/宰相.md "wikilink")