**非洲菝葜科**（**Behniaceae**）只有1[属](../Page/属.md "wikilink")1[种](../Page/种.md "wikilink")，是单种[科](../Page/科.md "wikilink")，只生长在[南非](../Page/南非.md "wikilink")，是当地的特有种。

本科[植物是](../Page/植物.md "wikilink")[灌木或](../Page/灌木.md "wikilink")[藤本](../Page/藤本.md "wikilink")，单[叶互生](../Page/叶.md "wikilink")，有假叶；[花小](../Page/花.md "wikilink")，[花冠](../Page/花冠.md "wikilink")3，被片类似[花瓣状](../Page/花瓣.md "wikilink")，两轮，[绿色或](../Page/绿色.md "wikilink")[白色](../Page/白色.md "wikilink")；[果实为](../Page/果实.md "wikilink")[浆果](../Page/浆果.md "wikilink")。

1981年的[克朗奎斯特分类法没有这一](../Page/克朗奎斯特分类法.md "wikilink")[科](../Page/科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该单独分出一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，属于[天门冬目](../Page/天门冬目.md "wikilink")，
但2003年经过修订的[APG II
分类法将其放到](../Page/APG_II_分类法.md "wikilink")[龙舌兰科中](../Page/龙舌兰科.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[非洲菝葜科](http://delta-intkey.com/angio/www/behniace.htm)
  - [APG网站中的龙舌兰科](http://www.mobot.org/MOBOT/Research/APWeb/orders/Asparagalesweb.htm#Agavaceae)
  - [NCBI中的非洲菝葜属](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=51437)

[\*](../Category/非洲菝葜科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")