**Finale**是[美國](../Page/美國.md "wikilink")[MakeMusic公司所開發的](../Page/MakeMusic.md "wikilink")[樂譜製作軟件](../Page/樂譜製作軟件.md "wikilink")，第一個版本於1989年發售。初期只能於[Macintosh的](../Page/Macintosh.md "wikilink")[Mac
OS使用](../Page/Mac_OS.md "wikilink")，1998年對應[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")，2006年以後的版本則可以完全對應[Windows
XP和](../Page/Windows_XP.md "wikilink")[Mac OS
X](../Page/Mac_OS_X.md "wikilink")。最新版本的價格約600[美元](../Page/美元.md "wikilink")。另有較廉宜的數個版本，提供較簡便的功能，包括SongWriter及PrintMusic，而NotePad則為免費軟體，只提供基礎編輯功能。Finale
Guitar、Notepad Plus、Allegro及免費的Finale Reader則已停止開發。

## 版本歷史

  - 1989年 Ver.1 Macintosh版
  - 1991年 Ver.2
  - 1994年 Ver.3.0 支援非日語的歌詞顯示
  - 1995年 Ver.3.2 於日本發售（[漢字Talk](../Page/漢字.md "wikilink")7.1-Mac OS
    8.1兼容）
  - 1996年 Ver.3.5 修訂後令日語顯示更為穩定（漢字Talk7.1-Mac OS 9.2.2兼容）
  - 1997年 Ver.3.7 Windows版（Windows 3.1, 95,
    98英語兼容）在日本首次發售。但Macintosh版本則不會發售於日本。
  - 1998年 97 Macintosh版（日語）及Windows版（英語）於日本發售。
  - 1999年 98 首日語版的Windows版（Windows 95, 98, ME, 2000兼容）
  - 2000年 2000（支援Windows XP，但無法讀取EPS文件）（Mac OS 7.6.1-9.2.2兼容）
  - 2001年 2001（Mac OS 8.6-9.2.2兼容）
  - 2002年 2002
  - 2003年 2003
  - 2004年 2004 Mac OS X兼容（Mac OS X 10.2以上、Mac OS 9.2.2兼容）。
  - 2005年 2005 Macintosh經典環境不兼容（Mac OS X 10.2以上兼容）。
  - 2006年 2006 Windows XP SP2、Mac OS X 10.4完全兼容。
  - 2007年 2007 Windows XP SP2、Mac OS X 10.4完全兼容。[Intel
    Mac兼容](../Page/Intel_Mac.md "wikilink")。
  - 2008年 2008 Windows XP/Vista兼容、Mac OS X 10.4完全兼容。[Intel
    Mac兼容](../Page/Intel_Mac.md "wikilink")。
  - 2009年 2009
  - 2009年 2010
    支援PrintMusic的[VST儀器兼容](../Page/Virtual_Studio_Technology.md "wikilink")。

## 外部連結

  - [FinaleMusic](http://www.finalemusic.com)（官方網站）
  - [Finale Notepad下載](http://www.finalemusic.com/notepad/)
  - [用戶討論區](http://forum.makemusic.com)
  - [The Finale School](http://www.thefinaleschool.com)（教學網站）

[Category:樂譜製作軟件](../Category/樂譜製作軟件.md "wikilink")
[Category:音樂軟件](../Category/音樂軟件.md "wikilink")