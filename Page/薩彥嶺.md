[Sayan.jpg](https://zh.wikipedia.org/wiki/File:Sayan.jpg "fig:Sayan.jpg")

**薩彥嶺**（），分東西兩支，是[俄羅斯](../Page/俄羅斯.md "wikilink")[西伯利亞南部的山脈](../Page/西伯利亞.md "wikilink")，[蒙古高原的北沿](../Page/蒙古高原.md "wikilink")，為[中國与](../Page/中國.md "wikilink")[俄羅斯间的](../Page/俄羅斯.md "wikilink")[國界](../Page/國界.md "wikilink")，1944年為[俄國併吞](../Page/俄國.md "wikilink")。

## 歷史

[雍正五年](../Page/雍正.md "wikilink")（1727年），[清廷與](../Page/清廷.md "wikilink")[沙俄簽定](../Page/沙俄.md "wikilink")《[布連斯奇條約](../Page/布連斯奇條約.md "wikilink")》（《[恰克圖條約](../Page/恰克圖條約.md "wikilink")》），有[滿](../Page/滿語.md "wikilink")、[蒙](../Page/蒙古語.md "wikilink")、[俄](../Page/俄語.md "wikilink")、[拉丁四種文本](../Page/拉丁語.md "wikilink")，規定清俄兩國此薩彥嶺為界，立[鄂博為界牌](../Page/鄂博.md "wikilink")，歸[烏里雅蘇台將軍管轄](../Page/烏里雅蘇台將軍.md "wikilink")。

在[外蒙古獨立](../Page/外蒙古獨立.md "wikilink")、蘇聯兼併[唐努烏梁海](../Page/唐努烏梁海.md "wikilink")、以及中华人民共和国成立前，薩彥嶺曾經是[中華民國版圖上的最北點](../Page/中华民国疆域.md "wikilink")\[1\]。

而自1949年后退守臺灣的中華民國政府曾經未承認放棄[唐努烏梁海的主權](../Page/唐努烏梁海.md "wikilink")。但目前「為尊重國際社會的共識」，因此認同其為俄國境內下屬之83個「[聯邦主體](../Page/俄羅斯聯邦主體.md "wikilink")（federal
subject），[類似州或省](../Page/一级行政区.md "wikilink")」之一\[2\]。而[中华民国外交部網站上的俄國地圖也包含唐努烏梁海](../Page/中华民国外交部.md "wikilink")\[3\]。

。而唐努烏梁海現已成為[蒙古國西北部的一部分與](../Page/蒙古國.md "wikilink")[俄羅斯](../Page/俄羅斯.md "wikilink")[聯邦主體](../Page/聯邦主體.md "wikilink")[圖瓦共和國的一部分](../Page/圖瓦共和國.md "wikilink")。

## 地形

位於89°E至106°E之間，是[阿爾泰山的東支](../Page/阿爾泰山.md "wikilink")。

平均高度2,000至2,700公尺，當中由[花崗岩及](../Page/花崗岩.md "wikilink")[變質岩構成的個別山峰](../Page/變質岩.md "wikilink"),海拔超過3,000公尺，最高峰為Munku-Sardyk峰（3,492公尺）。主要的山口高度約1800至2300公尺（如Muztagh：2280公尺,
Mongol：1980公尺, Tenghyz：2280公尺，Obo-sarym 1860公尺）。

山脈在92°E處被[上葉尼塞河割開](../Page/上葉尼塞河.md "wikilink")，而在其最東端，即106°處遇到了[色楞格](../Page/色楞格河.md "wikilink")—[鄂爾渾流域](../Page/鄂爾渾河.md "wikilink")。山勢南緩而北陡。

山脈在[葉尼塞河和](../Page/葉尼塞河.md "wikilink")[庫蘇古爾湖之間](../Page/庫蘇古爾湖.md "wikilink")，即100°
30' E稱為Yerghik-taiga。
[Khuvsgul.jpg](https://zh.wikipedia.org/wiki/File:Khuvsgul.jpg "fig:Khuvsgul.jpg")

## 生態

整體上呈荒蕪狀態，但在高處仍有由[松](../Page/松.md "wikilink")、[落葉松](../Page/落葉松.md "wikilink")、[刺柏屬](../Page/刺柏.md "wikilink")、[杜鵑花](../Page/杜鵑花.md "wikilink")、[赤楊](../Page/赤楊.md "wikilink")、[樺樹](../Page/樺樹.md "wikilink")、[醋栗屬及](../Page/醋栗屬.md "wikilink")[小蘖屬等構成的茂盛的森林](../Page/小蘖屬.md "wikilink")。在高坡的大石上長著[苔蘚和](../Page/苔蘚.md "wikilink")[地衣](../Page/地衣.md "wikilink")。

## 參考文獻

### 引用

### 来源

  -
## 外部連結

  - [Tuva -Sayan
    Mountains](https://web.archive.org/web/20061114024659/http://www.ewpnet.com/tuvados.htm)
  - [Photo of Sayan
    Mountains](http://www.pbase.com/dimitrisokolenko/image/33960726)

## 参见

  - [沙俄和苏联割占中国领土列表](../Page/沙俄和苏联割占中国领土列表.md "wikilink")
  - [唐努烏梁海](../Page/唐努烏梁海.md "wikilink")
  - [图瓦共和国](../Page/图瓦共和国.md "wikilink")

{{-}}

[Category:中俄边界问题](../Category/中俄边界问题.md "wikilink")
[Category:蒙古国地理](../Category/蒙古国地理.md "wikilink")
[Category:中国山脈](../Category/中国山脈.md "wikilink")
[Category:俄羅斯山脈](../Category/俄羅斯山脈.md "wikilink")

1.  [1](http://hk.knowledge.yahoo.com/question/index?qid=7006041600175)
2.  [中华民国外交部回函](../Page/中华民国外交部.md "wikilink")：
3.