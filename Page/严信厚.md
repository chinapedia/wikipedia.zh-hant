[Yan_Xinhou.jpg](https://zh.wikipedia.org/wiki/File:Yan_Xinhou.jpg "fig:Yan_Xinhou.jpg")
**严信厚**（），原名**严经邦**，字**筱舫**，又字**小舫**，号**石泉居士**。

[清末著名](../Page/清.md "wikilink")[实业家](../Page/实业家.md "wikilink")、[书法家](../Page/书法家.md "wikilink")、[画家](../Page/画家.md "wikilink")，[浙江](../Page/浙江.md "wikilink")[慈溪县人](../Page/慈溪.md "wikilink")。他是[中国近代早期](../Page/中国.md "wikilink")[民族资本家的代表之一](../Page/民族资本家.md "wikilink")，也对中国[教育的近代化做出了贡献](../Page/教育.md "wikilink")。

## 生历

幼年就读于乡里[私塾](../Page/私塾.md "wikilink")。

青少年时期曾在[宁波城里当学徒](../Page/宁波.md "wikilink")，在[上海任小职员](../Page/上海.md "wikilink")，后去杭州文书。

[同治初年](../Page/同治.md "wikilink")，入[李鸿章幕](../Page/李鸿章.md "wikilink")，后历任补道、[知府](../Page/知府.md "wikilink")、[天津盐务帮办等](../Page/天津.md "wikilink")。

后致力民族工商业、金融业，开创颇多，于民族实业有诸多贡献。

## 实业业绩

  - 1887年，投资5万两白银于宁波创办通久源机器轧花厂，是中国第一家机器轧花厂。
  - 1894年，于宁波创办通久源纺织局，是中资中首家使用动力机的近代纺织企业，产品行销一时。
  - 于天津开物华楼金店。
  - 于天津开同德盐号，经营盐业。
  - 于今上海[南京东路开设老九章绸缎庄](../Page/南京东路.md "wikilink")，并在天津设分店。
  - 于上海创办后来首屈一指的源丰润票号，有资本[白银](../Page/银两.md "wikilink")100万兩，其分号遍布[北京](../Page/北京.md "wikilink")、[上海](../Page/上海.md "wikilink")、[天津及东南诸省](../Page/天津.md "wikilink")，是近代新型[银行网络的雏形](../Page/银行.md "wikilink")。
  - 1897年，就任[中国通商银行第一任总经理](../Page/中国通商银行.md "wikilink")，并参与创办上海著名的[四明银行](../Page/四明银行.md "wikilink")。
  - 1905年，参与创办华兴保险公司，是中国第一家[保险公司](../Page/保险.md "wikilink")。
  - 1902年，就任上海商业会议公所首届总理，该所时为国内首创；后又继任总经理。

其一生在上海大办实业，曾投资兴办多家工厂如[面粉厂](../Page/面粉.md "wikilink")、油厂等，并积极投资航运业。曾参与投资上海[自来水公司](../Page/自来水.md "wikilink")（其子创立）。\[1\]

## 慈善

  - 捐巨资建造[唐沽铁路](../Page/唐沽铁路.md "wikilink")
  - 捐巨资建造宁波铁路
  - 赈灾浙江、[安徽](../Page/安徽.md "wikilink")、[山东等地](../Page/山东.md "wikilink")
  - 捐建养正学堂、芝秀义塾、芝田义塾、创办富春义塾
  - 建芝生痘局等公共[医疗](../Page/医疗.md "wikilink")[卫生机构](../Page/卫生.md "wikilink")
  - 和施则敬、[庞莱臣](../Page/庞莱臣.md "wikilink")、杨廷杲、朱葆三等一起创办济急善局，为东三省红十字普济善会前身。
  - 创办储才学堂等新式学堂
  - 资助[周恩来等青年学生](../Page/周恩来.md "wikilink")

## 书画

[Yan_Xin-Hou_Calligraphy.jpg](https://zh.wikipedia.org/wiki/File:Yan_Xin-Hou_Calligraphy.jpg "fig:Yan_Xin-Hou_Calligraphy.jpg")

严信厚也是位享誉海上的[书法家和](../Page/书法家.md "wikilink")[画家](../Page/画家.md "wikilink")，他工书法并擅长绘画。其书法风格近[赵孟頫](../Page/赵孟頫.md "wikilink")；绘画以芦雁著名。有《小长芦馆集帖》12卷行世。

## 著作

《听月楼诗钞》、《严修诗集》、《蟫香馆日记》、《严修东游日记》等

## 家庭

严信厚共有两女一子，分别为女儿淑英、毓珊，儿子[子均](../Page/严子均.md "wikilink")。其中儿子[严子均子承父业](../Page/严子均.md "wikilink")，为清末民初时期的著名企业家。孙女[严彩韵](../Page/严彩韵.md "wikilink")、[严莲韵](../Page/严莲韵.md "wikilink")、[严幼韵被并称为](../Page/严幼韵.md "wikilink")“严氏三姐妹”。女子严淑英则嫁给著名实业家[吴调卿之子吴熙元](../Page/吴调卿.md "wikilink")。

另有族侄[严修](../Page/严修.md "wikilink")，为中国近代著名教育家。

## 注释

## 参见

  - [严信厚——浙江档案网](https://web.archive.org/web/20050101153742/http://www.zjda.gov.cn/show_hdr.php?xname=CP0GHU0&dname=HSVVHU0&xpos=10)
  - [英才辈出的费市严家](http://culture.zjol.com.cn/05culture/system/2005/12/27/006419611.shtml)
  - [严信厚——上海地方志办公室](http://www.shtong.gov.cn/newsite/node2/node4429/node4432/node70708/node70710/userobject1ai45530.html)

[Y严](../Category/清朝商人.md "wikilink")
[Y](../Category/宁波裔上海人.md "wikilink")
[Y](../Category/宁波商帮.md "wikilink")
[Y严](../Category/慈溪人.md "wikilink")
[X严信厚](../Category/费市严家.md "wikilink")
[X信](../Category/严姓.md "wikilink")

1.  [英才辈出的费市严家](http://culture.zjol.com.cn/05culture/system/2005/12/27/006419611.shtml)