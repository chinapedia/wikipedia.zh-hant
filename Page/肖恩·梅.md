**肖恩·格雷戈里·梅**（，），生于[伊利诺伊州的](../Page/伊利诺伊州.md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")，[美国职业篮球运动员](../Page/美国.md "wikilink")，身高6英尺9英寸，司职中鋒[大前锋](../Page/大前锋.md "wikilink")。

梅成长在[印第安那州的布鲁明顿](../Page/印第安那州.md "wikilink")（Bloomington），他在北布鲁明顿高中的时候曾3次当选过全州全明星球员，还有一次是他的队友也是现在的NBA球员[贾瑞德·杰弗里斯](../Page/贾瑞德·杰弗里斯.md "wikilink")。2002年梅当选为麦当劳全美高中最佳阵容。他和[雷蒙德·費爾頓](../Page/雷蒙德·費爾頓.md "wikilink")，[拉希德·麦克坎斯这两位之后和梅一起夺得NCAA全美大学冠军的球员在麦当劳的比赛中相遇](../Page/拉希德·麦克坎斯.md "wikilink")。

05-06賽季，排名最佳新秀第二名。

## 参考资料

## 外部链接

  - [肖恩·梅NBA官方资料](http://www.nba.com/playerfile/sean_may/)
  - [北卡大学体育官方网站](http://tarheelblue.collegesports.com/sports/m-baskbl/mtt/may_sean00.html)
    (检索于2005年4月，没有更新2004-05赛季)

|width="25%" align="center"|前任：
**[本·戈登](../Page/本·戈登.md "wikilink")** |width="50%"
align="center"|**美国NCAA大学篮球總決賽最杰出球员
MVP**
2005年 |width="25%" align="center"|继任：
**[埃爾·霍弗德](../Page/埃爾·霍弗德.md "wikilink")** |-

|width="25%" align="center"|前任：
**[埃米卡·奥卡福](../Page/埃米卡·奥卡福.md "wikilink")** |width="50%"
align="center"|**美国NCAA大学篮球四強賽最杰出球员
MOP**
2005年 |width="25%" align="center"|继任：
**[乔金·诺阿](../Page/乔金·诺阿.md "wikilink")** |-

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:非洲裔美國籃球運動員](../Category/非洲裔美國籃球運動員.md "wikilink")
[Category:北卡罗来纳大学校友](../Category/北卡罗来纳大学校友.md "wikilink")
[Category:大前锋](../Category/大前锋.md "wikilink")
[Category:夏洛特山貓隊球員](../Category/夏洛特山貓隊球員.md "wikilink")
[Category:萨克拉门托国王队球员](../Category/萨克拉门托国王队球员.md "wikilink")