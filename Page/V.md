**V**, **v**
是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")22个[字母](../Page/字母.md "wikilink")。

就像[F那样](../Page/F.md "wikilink")，在[希腊语中](../Page/希腊语.md "wikilink")，（Ypsilon）也来源于[闪族语的Waw](../Page/闪族语.md "wikilink")。[伊特鲁里亚人在某种程度上简化了字母变成了V](../Page/伊特鲁里亚人.md "wikilink")。它在伊特鲁里亚语中的发音是\[u\]；但是因为[拉丁语缺乏表达](../Page/拉丁语.md "wikilink")\[w\]的字母，[罗马人把V用来表达](../Page/罗马.md "wikilink")\[w\]和\[u\]。在[罗曼语系中](../Page/罗曼语系.md "wikilink")，V用来表达从\[w\]中发展而来的\[v\]，就像德语字母W，开始用于发像英语字母的音，但在后来在[中高地德语](../Page/中高地德语.md "wikilink")（Middle
High
German）时代后变成发\[v\]音了。与此同时，V在德语中的发音用于表达与在英语中相同的发音，但是德语的Vau很快就再次用于表达\[f\]了（相同的情况现在可能正在[荷兰发生](../Page/荷兰.md "wikilink")）。

## 字母V的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") V | 86                                   | 0056                                     | 229                                    | `···-`                             |
| [小写](../Page/小写字母.md "wikilink") v | 118                                  | 0076                                     | 165                                    |                                    |

## 其他表示方法

## [漢語拼音](../Page/漢語拼音.md "wikilink")

由於英語不存在「[ü](../Page/ü.md "wikilink")」這個字母，所以大部份[漢語拼音輸入法以](../Page/漢語拼音輸入法.md "wikilink")「v」代替「ü」進行鍵入。這也導致[翻譯人名時帶](../Page/翻譯.md "wikilink")「ü」的名字被寫成「v」，如「[呂](../Page/呂姓.md "wikilink")」寫作了「Lv」；另外也存在寫成「Lu」的情況，與「[陸](../Page/陸姓.md "wikilink")」混淆。為解決這一問題，[中華人民共和國自](../Page/中華人民共和國.md "wikilink")2012年5月15日新發的[電子護照起](../Page/電子護照.md "wikilink")，對[姓名中的](../Page/姓名.md "wikilink")「ü」改寫成「yu」，如「呂」寫成「Lyu」，「女」寫成「Nyu」。\[1\]

## 参看

### V的變體

  - [半開後不圓唇元音](../Page/半開後不圓唇元音.md "wikilink")

### 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Upsilon）

## 参考资料

## 人物

防弹少年团成员; 金泰亨

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")

1.  <http://hznews.hangzhou.com.cn/shehui/content/2012-08/21/content_4344299.htm>