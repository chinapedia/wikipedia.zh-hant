<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>南開中學肄業<br />
<span style="color: blue;">(1919年)</span></li>
<li>英國<a href="../Page/倫敦大學學院.md" title="wikilink">倫敦大學學院美術部思乃德</a>(Slade)學院畢業<br />
<span style="color: blue;">(1924年)</span></li>
<li>法國國立最高美術專科學院畢業<br />
<span style="color: blue;">(1926年)</span></li>
<li>綏遠包頭煙酒公賣分局徵收員<br />
<span style="color: blue;">(1915年-1916年)</span></li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>中國國民黨駐倫敦總支部評議部部長<br />
<span style="color: blue;">(1923年-1926年6月)</span></li>
<li>中國國民黨駐倫敦總支部評議部部長<br />
<span style="color: blue;">(1923年-1926年6月)</span></li>
<li>中國國民黨廣州省黨部黨務指導委員<br />
<span style="color: blue;">(1926年6月-1928年)</span></li>
<li>（國民政府）廣東省政府農工廳秘書<br />
<span style="color: blue;">(1926年6月-1928年)</span></li>
<li>中國國民黨貴州省黨部黨務指導委員<br />
<span style="color: blue;">(1926年11月-1928年)</span></li>
<li>中國國民黨組織部秘書<br />
<span style="color: blue;">(1928年-1931年)</span></li>
<li>（國民政府）南京市政府秘書長<br />
<span style="color: blue;">(1928年1月-1930年)</span></li>
<li>青島大學教務長<br />
<span style="color: blue;">(1930年8月-1932年)</span></li>
<li>（國民政府）浙江省政府委員<br />
<span style="color: blue;">(1930年12月-)</span></li>
<li>（國民政府）浙江省政府教育廳廳長<br />
<span style="color: blue;">(1930年12月-)</span></li>
<li>中國國民黨組織部副部長<br />
<span style="color: blue;">(1931年6月14日-1931年12月29日)</span></li>
<li>中國文藝社發起人<br />
<span style="color: blue;">(1932年)</span></li>
<li>中國國民黨組織委員會委員<br />
<span style="color: blue;">(1931年12月29日-)</span></li>
<li>（國民政府）交通部常務次長<br />
<span style="color: blue;">(1932年12月7日-1936年4月8日)</span></li>
<li>中國國民黨中央執行委員<br />
<span style="color: blue;">(1932年11月-)</span></li>
<li>中央電影事業委員會委員<br />
<span style="color: blue;">(1933年4月-)</span></li>
<li>國立戲劇學校校務委員會主任委員<br />
<span style="color: blue;">(1935年-1936年)</span></li>
<li>國立戲劇學校創辦人<br />
<span style="color: blue;">(1935年)</span></li>
<li>中國國民黨文化事業計畫委員會副主任委員<br />
<span style="color: blue;">(1935年12月7日-)</span></li>
<li>（國民政府）內政部常務次長<br />
<span style="color: blue;">(1936年2月12日-1938年1月14日)</span></li>
<li>中國國民黨社會部副部長<br />
<span style="color: blue;">(1938年4月8日-1940年)</span></li>
<li>（國民政府）教育部教科用書編輯委會主任委員<br />
<span style="color: blue;">(1938年6月-)</span></li>
<li>（國民政府）教育部常務次長<br />
<span style="color: blue;">(1938年1月14日-1939年8月18日)</span></li>
<li>中央政治學校教務主任<br />
<span style="color: blue;">(1939年9月-1940年8月)</span></li>
<li>中央政治學校教育長<br />
<span style="color: blue;">(1940年8月-1941年)</span></li>
<li>中國國民黨文化運動委員會主任委員<br />
<span style="color: blue;">(1941年1月6日-1949年7月28日)</span></li>
<li>中國國民黨宣傳部部長<br />
<span style="color: blue;">(1942年12月7日-1943年10月4日)</span></li>
<li>中國國民黨海外部部長<br />
<span style="color: blue;">(1943年10月4日-1944年11月20日)</span></li>
<li>中國國民黨組織委員會委員<br />
<span style="color: blue;">(1944年7月10日-)</span></li>
<li>中央電影企業公司董事長<br />
<span style="color: blue;">(1946年6月-)</span></li>
<li>（國民政府）制憲國民大會代表<br />
<span style="color: blue;">(1946年-1947年)</span></li>
<li>立法院（第一屆）委員<br />
<span style="color: blue;">(1948年1月-1968年6月12日)</span></li>
<li>中華文藝獎金委員會創辦人<br />
<span style="color: blue;">(1950年)</span></li>
<li>中國廣播公司董事長<br />
<span style="color: blue;">(1950年-1968年)</span></li>
<li>中國國民黨中央改造委員會委員<br />
<span style="color: blue;">(1950年7月26日-1952年10月9日)</span></li>
<li>中華日報董事長<br />
<span style="color: blue;">(1950年1月-1968年)</span></li>
<li>立法院（第四任）院長<br />
<span style="color: blue;">(1952年3月17日-1961年2月24日)</span></li>
<li>中國國民黨中央文化工作指導小組委員<br />
<span style="color: blue;">(1956年11月28日-1957年10月26日)</span></li>
<li>中國國民黨宣傳工作指導小組委員<br />
<span style="color: blue;">(1957年4月29日-1957年10月26日)</span></li>
<li>中山學術文化基金董事會副董事長<br />
<span style="color: blue;">(1965年-1968年)</span></li>
<li>文藝創作獎助審議委員會召集人<br />
<span style="color: blue;">(1965年-1968年)</span></li>
<li>中國國民黨中央委員<br />
<span style="color: blue;">(-1968年6月12日)</span></li>
<li>中國國民黨中央常務委員<br />
<span style="color: blue;">(1957年10月26日-1968年6月12日)</span></li>
</ul>
</div></td>
</tr>
</tbody>
</table>

**張道藩**（），本籍[貴州](../Page/貴州.md "wikilink")[盤縣](../Page/盤縣.md "wikilink")，美術教育者。曾於1952年3月11日－1961年2月24日擔任[中華民國](../Page/中華民國.md "wikilink")[立法院長](../Page/立法院.md "wikilink")。

## 生平

張道藩1897年生於[貴州](../Page/貴州.md "wikilink")。小学毕业後到[天津一个族叔家里](../Page/天津.md "wikilink")。1916年考入[天津](../Page/天津.md "wikilink")[南开中学](../Page/南开中学.md "wikilink")，加入[中华革命党](../Page/中华革命党.md "wikilink")。1919年11月以公費留學[歐洲](../Page/歐洲.md "wikilink")，並先後於[倫敦大學學院美術部](../Page/倫敦大學學院.md "wikilink")、[巴黎](../Page/巴黎.md "wikilink")[法國美術學院深造](../Page/法國美術學院.md "wikilink")。1926年返回[中國後](../Page/中國.md "wikilink")，加入[中國國民黨](../Page/中國國民黨.md "wikilink")。為[國民政府文化事業與政治宣傳的策劃者](../Page/國民政府.md "wikilink")。

1939年，蔣介石讓張道藩成立「中央文化運動委員會」。\[1\]

1942年，張道藩邀[趙友培任](../Page/趙友培.md "wikilink")[中國國民黨中央委員會文化運動委員會](../Page/中國國民黨中央委員會.md "wikilink")[秘書兼](../Page/秘書.md "wikilink")《文藝先鋒[月刊](../Page/月刊.md "wikilink")》主編。1950年，張道藩隨[國民政府遷徙至](../Page/國民政府.md "wikilink")[台灣](../Page/台灣.md "wikilink")，並繼續擔任中國國民黨中央委員會內中央組織部秘書等重要黨職。在文學活動的參與上，張道藩曾經受[蔣經國的指示](../Page/蔣經國.md "wikilink")，與[陳紀瀅](../Page/陳紀瀅.md "wikilink")、[王藍](../Page/王藍.md "wikilink")、[趙友培等人發起](../Page/趙友培.md "wikilink")、成立[中國文藝協會](../Page/中國文藝協會.md "wikilink")，還另行成立[中華文藝獎金委員會及其機關刊物](../Page/中華文藝獎金委員會.md "wikilink")《[文藝創作](../Page/文藝創作.md "wikilink")》，獎勵[反共抗俄的優良文藝作品](../Page/反共抗俄.md "wikilink")，生產出大批[反共文藝](../Page/反共文藝.md "wikilink")(包括[反共文學](../Page/反共文學.md "wikilink"))作品，尤其推行所謂的「[戰鬥文藝](../Page/戰鬥文藝.md "wikilink")」，要作家、藝術家創作具有「[反共抗俄](../Page/反共抗俄.md "wikilink")」意涵的作品，放棄個人的創作自由，強調文藝的功用。相對於[中華文藝獎金委員會](../Page/中華文藝獎金委員會.md "wikilink")，張道藩雖然是[中國文藝協會的發起人](../Page/中國文藝協會.md "wikilink")，不過擔任過首屆常務理事以後，他就不過問協會裡的大小事\[2\]。

創作方面，張道藩著有〈我們所需的文藝政策〉、〈[三民主義文藝論](../Page/三民主義.md "wikilink")〉這篇論述；劇本〈再相逢〉、〈密電碼〉、〈留學生之戀〉等作品。張道藩過世以後，他的劇本作品被人編輯成《張道藩戲劇集》；至於文學作品、論述則被[九歌出版社編輯成](../Page/九歌出版社.md "wikilink")《張道藩先生文集》，1999年出版。文學以外，張道藩也精通繪畫，因此他留下的畫作，後來被人編輯成《張道藩先生畫集》出版。

[台灣文學史上](../Page/台灣文學史.md "wikilink")，張道藩及其作為，具有相當大的爭議性，正、負面的評價都有。本土論者多持負面看法，認為：**張道藩**、[陳紀瀅等人趁國民政府壓制言論自由](../Page/陳紀瀅.md "wikilink")、人民集會結社自由之時，依恃他們的立法委員身份及蔣家父子的指示，成立多個半官方性質的民間文藝團體，壟斷當時台灣的文藝資源，去生產大量[反共文藝的作品](../Page/反共文藝.md "wikilink")；還遵循國民黨當局的意思，去推行極負爭議性的「[文化清潔運動](../Page/文化清潔運動.md "wikilink")」。不過，[中國文藝協會](../Page/中國文藝協會.md "wikilink")、[中國青年寫作協會](../Page/中國青年寫作協會.md "wikilink")、[中國婦女寫作協會等出身的作家](../Page/中國婦女寫作協會.md "wikilink")，則相當肯定張道藩對台灣文藝的貢獻。1991年2月作家們召開「近代學人風範研討會」，以張道藩為主題；1997年12月14日，一些作家則在台北環亞飯店2樓舉行「張道藩先生百年誕辰紀念研討會」\[3\]及「文藝運動對文壇的貢獻─從張道藩先生談起」的座談會\[4\]\[5\]。

1948年，貴州省第二區選出之立法委員
[陽明公園內涼亭上張道藩所提的匾額.jpg](https://zh.wikipedia.org/wiki/File:陽明公園內涼亭上張道藩所提的匾額.jpg "fig:陽明公園內涼亭上張道藩所提的匾額.jpg")內涼亭上張道藩所提的匾額,1957年\]\]

1952年3月，張道藩當選[立法院院長](../Page/立法院.md "wikilink")。\[6\]至1961年間，張道藩先後四度當選[立法院長](../Page/立法院長.md "wikilink")。

1956年4月19日，因當時靖國神社並未合祀甲乙丙三級[戰犯](../Page/戰犯.md "wikilink")，張道藩訪問[日本並參訪](../Page/日本.md "wikilink")[靖國神社](../Page/靖國神社.md "wikilink")，而此前不久的1955年8月，靖国神社刚刚举办过“終戦時自決者540柱慰霊祭”。

1968年6月12日，張道藩逝於[台北](../Page/台北.md "wikilink")。

## 轶事

  - 張道藩与[徐悲鸿的妻子](../Page/徐悲鸿.md "wikilink")[蒋碧微的婚外恋](../Page/蒋碧微.md "wikilink")，是民国文艺界的一大花边新闻。\[7\]

## 紀念館

[台北市立圖書館設有](../Page/台北市立圖書館.md "wikilink")[道藩紀念圖書館](../Page/道藩紀念圖書館.md "wikilink")，簡稱「道藩分館」，館舍座落於[台北市](../Page/台北市.md "wikilink")[大安區](../Page/大安區_\(台北市\).md "wikilink")[辛亥路](../Page/辛亥路.md "wikilink")3段11號3樓。

另外，[國立政治大學外語學院院館也被命名為](../Page/國立政治大學.md "wikilink")「道藩樓」以紀念之。

## 相關條目

  - [中華民國立法院](../Page/中華民國立法院.md "wikilink")
  - [中華民國立法院院長](../Page/中華民國立法院院長.md "wikilink")
  - [徐悲鸿](../Page/徐悲鸿.md "wikilink")
  - [中國文藝協會](../Page/中國文藝協會.md "wikilink")
  - [中華文藝獎金委員會](../Page/中華文藝獎金委員會.md "wikilink")
  - [反共文學](../Page/反共文學.md "wikilink")

## 參考資料

<references />

## 外部連結

[Z張](../Category/立法院院長.md "wikilink")
[Z張](../Category/第1屆正選立法委員.md "wikilink")
[Z張](../Category/中國國民黨黨員.md "wikilink")
[Z張](../Category/國立政治大學教授.md "wikilink")
[Z張](../Category/伦敦大学学院校友.md "wikilink")
[Z張](../Category/中廣董事長.md "wikilink")
[Z張](../Category/台灣戰後貴州移民.md "wikilink")
[D道](../Category/張姓.md "wikilink")
[Category:盤縣人](../Category/盤縣人.md "wikilink")
[Category:南开中学校友](../Category/南开中学校友.md "wikilink")
[Category:國立故宮博物院人物](../Category/國立故宮博物院人物.md "wikilink")
[Category:中国国民党第三届中央执行委员会候补委员](../Category/中国国民党第三届中央执行委员会候补委员.md "wikilink")

1.  程榕寧著，《文藝鬥士——張道藩傳》，近代中國出版社，第65頁
2.  古繼堂/著，《消逝的文學風華》，九歌出版社，2011年12月初版，頁39-46。
3.  該研討會由[鍾雷](../Page/鍾雷.md "wikilink")、[應未遲](../Page/應未遲.md "wikilink")、[李瑞騰](../Page/李瑞騰.md "wikilink")、[張放發表論文](../Page/張放.md "wikilink")。
4.  該座談會由[瘂弦主持](../Page/瘂弦.md "wikilink")，[周玉山](../Page/周玉山.md "wikilink")、[張雲家](../Page/張雲家.md "wikilink")、[應平書等人與會](../Page/應平書.md "wikilink")。
5.  文訊雜誌社/編，《1997 台灣文學年鑑》，台北市：文建會，1998年，頁184
6.  [陳-{布}-雷等編著](../Page/陳布雷.md "wikilink")，《蔣介石先生年表》，台北，[傳記文學出版社](../Page/傳記文學.md "wikilink")，1978年6月1日，第70頁
7.  [张道藩如何热恋上徐悲鸿妻子蒋碧薇，载
    王由青，张道藩的文宦生涯，北京：团结出版社，2008年](http://book.sina.com.cn/zhangdaofa/excerpt/eduhissz/2008-02-14/1415230004.shtml)