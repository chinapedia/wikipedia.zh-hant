[Japan_prefectures.png](https://zh.wikipedia.org/wiki/File:Japan_prefectures.png "fig:Japan_prefectures.png")訂定之[日本都道府縣地圖](../Page/日本.md "wikilink")\]\]
[Map_of_Japan_1855.jpg](https://zh.wikipedia.org/wiki/File:Map_of_Japan_1855.jpg "fig:Map_of_Japan_1855.jpg")

**日本寺院列表**是[日本的](../Page/日本.md "wikilink")[佛教](../Page/佛教.md "wikilink")[寺院之列表](../Page/寺院.md "wikilink")。

本列表之寺院列出標準為近代以前所成立的佛教各宗派之總本山、大本山級之寺院，且在日本的佛教史、文化史、美術史上佔有重要地位、及擁有國寶級等之重要的文化財，並擁有廣大信仰、與對日本文化上有重大影響之一系列寺院。

本列表以[日本都道府縣排列](../Page/都道府县.md "wikilink")。

## 日本都道府縣之寺院分布

### 北海道

  - [國泰寺](../Page/國泰寺_\(厚岸町\).md "wikilink")（[北海道](../Page/北海道.md "wikilink")[厚岸郡](../Page/厚岸郡.md "wikilink")[厚岸町](../Page/厚岸町.md "wikilink")）

### 青森縣

### 岩手縣

  - [毛越寺](../Page/毛越寺.md "wikilink")（[岩手縣](../Page/岩手縣.md "wikilink")[西磐井郡](../Page/西磐井郡.md "wikilink")[平泉町](../Page/平泉町.md "wikilink")）
  - [中尊寺](../Page/中尊寺.md "wikilink")（岩手縣西磐井郡平泉町）
  - [正法寺](../Page/正法寺_\(奧州市\).md "wikilink")（岩手縣[奧州市](../Page/奧州市.md "wikilink")）

### 宮城縣

  - [瑞巖寺](../Page/瑞巖寺.md "wikilink")（[宮城縣](../Page/宮城縣.md "wikilink")[宮城郡](../Page/宮城郡.md "wikilink")[松島町](../Page/松島町.md "wikilink")）

### 秋田縣

### 山形縣

  - [立石寺](../Page/立石寺.md "wikilink")（[山形縣](../Page/山形縣.md "wikilink")[山形市](../Page/山形市.md "wikilink")）

### 福島縣

  - 專稱寺（福島縣いわき市）

### 茨城縣

### 栃木縣

  - [輪王寺](../Page/輪王寺.md "wikilink")（[栃木縣](../Page/栃木縣.md "wikilink")[日光市](../Page/日光市.md "wikilink")）
  - 滿願寺（栃木縣栃木市）
  - 專修寺（三重縣津市、栃木縣芳賀郡二宮町）
  - [鑁阿寺](../Page/足利氏館.md "wikilink")（栃木縣[足利市](../Page/足利市.md "wikilink")）

### 群馬縣

### 埼玉縣

  - 正法寺（埼玉縣東松山市）
  - [喜多院](../Page/喜多院.md "wikilink")（埼玉縣川越市）

### 千葉縣

  - 萬滿寺（千葉縣松戶市）
  - 本土寺（千葉縣松戶市）
  - 東漸寺（千葉縣松戶市）
  - 新勝寺（千葉縣成田市）→ [成田山新勝寺](../Page/成田山新勝寺.md "wikilink")

### 東京都

  - [護國寺](../Page/護國寺_\(東京\).md "wikilink")（[東京都](../Page/東京都.md "wikilink")[文京區](../Page/文京區.md "wikilink")）
  - 藥王院有喜寺（東京都八王子市）→ 高尾山藥王院有喜寺
  - 本門寺（池上本門寺、東京都大田區）
  - 普濟寺（東京都立川市）
  - 禪林寺（東京都三鷹市）
  - 總持寺（西新井大師、東京都足立區）
  - [增上寺](../Page/增上寺.md "wikilink")（東京都[港區](../Page/港區.md "wikilink")）
  - [淺草寺](../Page/淺草寺.md "wikilink")（東京都台東區）
  - [泉岳寺](../Page/泉岳寺.md "wikilink")（東京都港區）
  - [豪德寺](../Page/豪德寺.md "wikilink")（東京都世田谷區）
  - [寬永寺](../Page/寬永寺.md "wikilink")（東京都台東區）
  - [池上本门寺](../Page/池上本门寺.md "wikilink")（東京都大田區）

### 神奈川縣

  - [總持寺](../Page/總持寺.md "wikilink")（[神奈川縣](../Page/神奈川縣.md "wikilink")[横濱市](../Page/横濱市.md "wikilink")[鶴見區](../Page/鶴見區.md "wikilink")）
  - [明月院](../Page/明月院.md "wikilink")（神奈川縣[鎌倉市](../Page/鎌倉市.md "wikilink")）
  - 妙本寺（神奈川縣鎌倉市）
  - [長谷寺](../Page/長谷寺_\(鎌倉市\).md "wikilink")（神奈川縣鎌倉市）
  - [東慶寺](../Page/東慶寺.md "wikilink")（神奈川縣鎌倉市）
  - 瑞泉寺（神奈川縣鎌倉市）
  - 青蓮寺（神奈川縣鎌倉市）
  - 稱名寺（橫濱市金澤區）
  - [清淨光寺](../Page/清淨光寺.md "wikilink")（神奈川縣藤澤市）
  - [壽福寺](../Page/壽福寺.md "wikilink")（神奈川縣鎌倉市）
  - [光明寺](../Page/光明寺_\(鎌倉市\).md "wikilink")（鎌倉市）
  - [高德院](../Page/高德院.md "wikilink")（鎌倉大佛、神奈川縣鎌倉市）
  - [建長寺](../Page/建長寺.md "wikilink")（神奈川縣鎌倉市）
  - 弘明寺（橫濱市南區）
  - 覺園寺（神奈川縣鎌倉市）
  - [圓覺寺](../Page/圓覺寺_\(鎌倉\).md "wikilink")（神奈川縣鎌倉市）

### 新潟縣

  - 蓮華峰寺（新潟縣佐渡市）
  - 淨興寺（新潟縣上越市）

### 富山縣

  - [來迎寺](../Page/來迎寺_\(富山市\).md "wikilink")（富山縣富山市）

### 石川縣

### 福井縣

  - [永平寺](../Page/永平寺.md "wikilink")（福井縣永平寺町）

### 山梨縣

  - 本遠寺（山梨縣南巨摩郡身延町）
  - [久遠寺](../Page/久遠寺.md "wikilink")（山梨縣南巨摩郡身延町）

### 長野縣

  - 放光寺（長野縣松本市）
  - 長谷寺（長野縣長野市）
  - [善光寺](../Page/善光寺.md "wikilink")（長野縣長野市）
  - 牛伏寺（長野縣松本市）

### 岐阜縣

  - 華嚴寺（岐阜縣揖斐郡揖斐川町）

### 靜岡縣

  - [大石寺](../Page/大石寺.md "wikilink")（靜岡縣富士宮市）
  - 修禪寺（靜岡縣伊豆市）

### 愛知縣

  - 長興寺（愛知縣豐田市）
  - 興正寺（尾張高野、名古屋市昭和區）

### 三重縣

  - 初馬寺（三重縣津市）→ 蓮光院初馬寺
  - 專修寺（三重縣津市、栃木縣芳賀郡二宮町）
  - 青蓮寺（地藏院、三重縣名張市）
  - 金剛證寺（三重縣伊勢市）

### 滋賀縣

  - [三井寺](../Page/園城寺.md "wikilink")（滋賀縣大津市）→ 園城寺
  - [寶嚴寺](../Page/寶嚴寺.md "wikilink")（竹生島、滋賀縣東淺井郡びわ町）
  - 百濟寺（滋賀縣東近江市）
  - 長命寺（滋賀縣近江八幡市）
  - 金剛輪寺（滋賀縣愛知郡秦莊町）
  - [延曆寺](../Page/延曆寺.md "wikilink")（滋賀縣大津市）
  - [園城寺](../Page/園城寺.md "wikilink")（三井寺、滋賀縣大津市）
  - [石山寺](../Page/石山寺.md "wikilink")（滋賀縣大津市）
  - [永源寺](../Page/永源寺.md "wikilink")（滋賀縣東近江市）

### 京都府

  - [天龍寺](../Page/天龍寺.md "wikilink")（[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[右京區](../Page/右京區.md "wikilink")）
  - [龍安寺](../Page/龍安寺.md "wikilink")（京都市右京區）
  - 了德寺（京都市右京區）
  - [三十三間堂](../Page/三十三間堂.md "wikilink")（蓮華王院本堂、京都市東山區）
  - [鹿苑寺](../Page/京都鹿苑寺.md "wikilink")（金閣寺、京都市右京區）
  - [慈照寺](../Page/慈照寺.md "wikilink")（銀閣寺、京都市左京區）
  - [善峯寺](../Page/善峯寺.md "wikilink")（京都市西京區）
  - [壬生寺](../Page/壬生寺.md "wikilink")（京都市中京區）
  - [妙心寺](../Page/妙心寺.md "wikilink")（京都市右京區）
  - [妙法院](../Page/妙法院.md "wikilink")（京都市東山區）
  - 松尾寺（京都府舞鶴市）
  - [曼殊院](../Page/曼殊院.md "wikilink")（京都市左京區）
  - [萬福寺](../Page/萬福寺_\(宇治市\).md "wikilink")（京都府宇治市）
  - [本願寺](../Page/本願寺.md "wikilink")（京都市下京區）
  - [西本願寺](../Page/西本願寺.md "wikilink")（淨土真宗本願寺派本願寺、京都市下京區）
  - [東本願寺](../Page/東本願寺.md "wikilink")（京都市下京區）
  - [本能寺](../Page/本能寺.md "wikilink")（京都市中京區）
  - [法然院](../Page/法然院.md "wikilink")（京都市左京區）
  - [方廣寺](../Page/方廣寺_\(京都市\).md "wikilink")（京都市東山區）
  - [寶嚴院](../Page/寶嚴院.md "wikilink")（京都市右京區）
  - [法金剛院](../Page/法金剛院.md "wikilink")（京都市右京區）
  - [法住寺](../Page/法住寺.md "wikilink")（京都市東山區）
  - [佛光寺](../Page/佛光寺_\(京都市\).md "wikilink")（京都市下京區）
  - 法界寺（京都市伏見區）
  - [法觀寺](../Page/法觀寺.md "wikilink")（八坂塔、京都市東山區）
  - [平等院](../Page/平等院.md "wikilink")（京都府宇治市）
  - 成相寺（京都府宮津市）
  - [南禪寺](../Page/南禪寺_\(日本\).md "wikilink")（京都市左京區）
  - [仁和寺](../Page/仁和寺.md "wikilink")（御室御所）（京都市右京區）
  - [東福寺](../Page/東福寺.md "wikilink")（京都市[東山區](../Page/東山區_\(日本\).md "wikilink")）
  - [東寺](../Page/東寺.md "wikilink")（教王護國寺、京都市南區）
  - [等持院](../Page/等持院.md "wikilink")（京都市右京區）
  - [頂法寺](../Page/頂法寺.md "wikilink")（六角堂、京都市[中京區](../Page/中京區.md "wikilink")）
  - [知恩院](../Page/知恩院.md "wikilink")（京都市東山區）
  - [智積院](../Page/智積院.md "wikilink")（京都市東山區）
  - [大報恩寺](../Page/大報恩寺_\(京都市\).md "wikilink")（京都市上京區）
  - [大德寺](../Page/大德寺.md "wikilink")（京都市北區）
  - [醍醐寺](../Page/醍醐寺.md "wikilink")（京都市伏見區）
  - [大覺寺](../Page/大覺寺_\(京都\).md "wikilink")（京都市右京區）
  - [禪林寺](../Page/禪林寺_\(京都市\).md "wikilink")（永觀堂、京都市）
  - [泉涌寺](../Page/泉涌寺.md "wikilink")（京都市東山區）
  - [清涼寺](../Page/清涼寺_\(京都市\).md "wikilink")（京都市右京區）
  - [隨心院](../Page/隨心院.md "wikilink")（京都市山科區）
  - [淨瑠璃寺](../Page/淨瑠璃寺.md "wikilink")（京都府[木津川市加茂町](../Page/木津川市.md "wikilink")）
  - [神護寺](../Page/神護寺.md "wikilink")（京都市右京區）
  - [青蓮院](../Page/青蓮院.md "wikilink")（京都市東山區）
  - [相國寺](../Page/相國寺.md "wikilink")（京都市上京區）
  - [清淨華院](../Page/清淨華院.md "wikilink")（京都市上京區）
  - [實相院](../Page/實相院.md "wikilink")（京都市左京區）
  - [寂光院](../Page/寂光院.md "wikilink")（京都市左京區）
  - [三室戶寺](../Page/三室戶寺.md "wikilink")（京都府宇治市）
  - [三十三間堂](../Page/三十三間堂.md "wikilink")（妙法院蓮華王院、京都市）
  - [三千院](../Page/三千院.md "wikilink")（京都市左京區）
  - [六波羅蜜寺](../Page/六波羅蜜寺.md "wikilink")（京都市東山區）
  - [西芳寺](../Page/西芳寺.md "wikilink")（苔寺、京都市）
  - [廣隆寺](../Page/廣隆寺.md "wikilink")（京都市右京區）
  - [金戒光明寺](../Page/金戒光明寺.md "wikilink")（京都市）
  - [高台寺](../Page/高台寺.md "wikilink")（京都市東山區）
  - [鞍馬寺](../Page/鞍馬寺.md "wikilink")（京都市左京區）
  - [建仁寺](../Page/建仁寺.md "wikilink")（京都市東山區）
  - [光悅寺](../Page/光悅寺.md "wikilink")（京都市北區）
  - [高山寺](../Page/高山寺.md "wikilink")（京都市右京區）
  - 興正寺（京都市下京區）
  - [教王護國寺](../Page/東寺.md "wikilink")（京都市南區）→
    [東寺](../Page/東寺.md "wikilink")
  - [行願寺](../Page/行願寺.md "wikilink")（京都市中京區）
  - [清水寺](../Page/清水寺.md "wikilink")（京都市東山區）
  - [海住山寺](../Page/海住山寺.md "wikilink")（京都府[木津川市加茂町](../Page/木津川市.md "wikilink")）
  - [勸修寺](../Page/勸修寺.md "wikilink")（京都市山科區）
  - 穴太寺（京都府龜岡市）
  - [廬山寺](../Page/廬山寺.md "wikilink")（京都市上京區）

### 大阪府

  - 林昌寺（大阪府泉南市）
  - 野中寺（大阪府羽曳野市）
  - 葛井寺（大阪府藤井寺市）
  - 南宗寺（大阪府堺市）
  - 道明寺（大阪府藤井寺市）
  - 長慶寺（大阪府泉南市）
  - 太融寺（大阪市北區）
  - 大聖觀音寺（大阪市住吉區）
  - 大聖勝軍寺（大阪府八尾市）
  - [大念佛寺](../Page/大念佛寺.md "wikilink")（大阪市平野區）
  - 施福寺（大阪府和泉市）
  - [四天王寺](../Page/四天王寺.md "wikilink")（大阪市）
  - 西琳寺（大阪府羽曳野市）
  - 孝恩寺（大阪府貝塚市）
  - [觀心寺](../Page/觀心寺.md "wikilink")（大阪府河內長野市）
  - 勝尾寺（大阪府箕面市）
  - 西江寺（大阪府箕面市）
  - 叡福寺（大阪府南河內郡太子町）

### 兵庫縣

  - 本興寺（兵庫縣尼崎市）
  - 斑鳩寺（兵庫縣揖保郡太子町）
  - [中山寺](../Page/中山寺_\(寶塚市\).md "wikilink")（兵庫縣寶塚市）
  - 如意寺（兵庫縣神戶市西區）
  - 能福寺（兵庫縣神戶市兵庫區）
  - 朝光寺（兵庫縣加東郡社町）
  - 大龍寺（兵庫縣神戶市中央區）
  - [太山寺](../Page/太山寺.md "wikilink")（兵庫縣神戶市西區）
  - [須磨寺](../Page/須磨寺.md "wikilink")（福祥寺、兵庫縣神戶市須磨區）
  - [淨土寺](../Page/淨土寺_\(小野市\).md "wikilink")（兵庫縣小野市）
  - 清荒神清澄寺（兵庫縣寶塚市）
  - 清水寺（兵庫縣加東郡社町）
  - 神呪寺（兵庫縣西宮市）
  - 伽耶院（兵庫縣三木市）
  - 鶴林寺（兵庫縣加古川市）
  - 溫泉寺（兵庫縣豐岡市）
  - [圓教寺](../Page/圓教寺.md "wikilink")（兵庫縣姫路市）
  - 一乘寺（兵庫縣加西市）

### 奈良縣

  - [大安寺](../Page/大安寺_\(奈良\).md "wikilink")（奈良縣奈良市）
  - [靈山寺](../Page/靈山寺_\(奈良市\).md "wikilink")（奈良市）
  - [藥師寺](../Page/藥師寺.md "wikilink")（奈良縣奈良市）
  - [室生寺](../Page/室生寺.md "wikilink")（奈良縣宇陀郡室生村）
  - 南法華寺（壺阪寺、奈良縣高市郡高取町）
  - [寶山寺](../Page/寶山寺_\(奈良縣\).md "wikilink")（奈良縣生駒市）
  - [法隆寺](../Page/法隆寺.md "wikilink")（奈良縣生駒郡斑鳩町）
  - 法輪寺（奈良縣生駒郡斑鳩町）
  - [法華寺](../Page/法華寺_\(奈良市\).md "wikilink")（奈良縣奈良市）
  - [法起寺](../Page/法起寺.md "wikilink")（奈良縣生駒郡斑鳩町）
  - 不退寺（奈良縣奈良市）
  - [長谷寺](../Page/長谷寺.md "wikilink")（奈良縣櫻井市）
  - [唐招提寺](../Page/唐招提寺.md "wikilink")（奈良縣奈良市）
  - [東大寺](../Page/東大寺.md "wikilink")（[正倉院](../Page/正仓院.md "wikilink")）（奈良縣奈良市）
  - [中宮寺](../Page/中宮寺.md "wikilink")（奈良縣生駒郡斑鳩町）
  - [當麻寺](../Page/當麻寺.md "wikilink")（奈良縣葛城市）
  - 橘寺（奈良縣高市郡明日香村）
  - 新藥師寺（奈良縣奈良市）
  - [西大寺](../Page/西大寺_\(奈良市\).md "wikilink")（奈良市）
  - [興福寺](../Page/興福寺.md "wikilink")（奈良縣奈良市）
  - [元興寺](../Page/元興寺.md "wikilink")（奈良縣奈良市）
  - 岡寺（奈良縣高市郡明日香村）→龍蓋寺
  - 帶解寺（奈良縣奈良市）
  - 秋篠寺（奈良縣奈良市）
  - [飛鳥寺](../Page/飛鳥寺.md "wikilink")（安居院、奈良縣高市郡明日香村）
  - [大峰山寺](../Page/大峯山寺.md "wikilink")（奈良県吉野郡天川村）
  - 大峯山龍泉寺（奈良県吉野郡天川村）

### 和歌山縣

  - [補陀洛山寺](../Page/補陀洛山寺.md "wikilink")（[和歌山縣東牟婁郡那智勝浦町](../Page/和歌山縣.md "wikilink")）
  - [根來寺](../Page/根來寺.md "wikilink")（和歌山縣岩出市）
  - [道成寺](../Page/道成寺.md "wikilink")（和歌山縣日高郡日高川町）
  - 長保寺（和歌山縣海南市）
  - 善福院（和歌山縣海南市）
  - [青岸渡寺](../Page/青岸渡寺.md "wikilink")（和歌山縣東牟婁郡那智勝浦町）
  - [淨妙寺](../Page/淨妙寺_\(有田市\).md "wikilink")（和歌山縣有田市）
  - 金剛三昧院（和歌山縣伊都郡高野町）
  - [金剛峰寺](../Page/金剛峯寺.md "wikilink")（和歌山縣伊都郡高野町）
  - [粉河寺](../Page/粉河寺.md "wikilink")（和歌山縣那賀郡粉河町）
  - [紀三井寺](../Page/紀三井寺.md "wikilink")（和歌山縣和歌山市）

### 鳥取縣

  - [三佛寺](../Page/三佛寺.md "wikilink")（鳥取縣東伯郡三朝町）

### 島根縣

  - [一畑寺](../Page/一畑寺.md "wikilink")（島根縣出雲市）

### 岡山縣

### 廣島縣

  - 明王院（廣島縣福山市）
  - 福禪寺（對潮樓、廣島縣福山市）

### 山口縣

  - 東光寺（山口縣萩市）
  - [功山寺](../Page/功山寺.md "wikilink")（山口縣下關市）

### 德島縣

  - [靈山寺](../Page/靈山寺_\(鳴門市\).md "wikilink")（[德島縣](../Page/德島縣.md "wikilink")[鳴門市](../Page/鳴門市.md "wikilink")）

### 香川縣

  - [善通寺](../Page/善通寺.md "wikilink")（[香川縣](../Page/香川縣.md "wikilink")[善通寺市](../Page/善通寺市.md "wikilink")）

### 愛媛縣

  - 泰平寺（愛媛縣宇和島市）

### 高知縣

### 福岡縣

  - 承天寺（福岡縣福岡市）
  - 觀世音寺（福岡縣太宰府市）

### 佐賀縣

### 長崎縣

  - [崇福寺](../Page/崇福寺_\(長崎市\).md "wikilink")（長崎縣長崎市）

### 熊本縣

### 大分縣

  - [富貴寺](../Page/富貴寺.md "wikilink")（大分縣）

### 宮崎縣

### 鹿兒島縣

  - 泰平寺（鹿兒島縣薩摩川內市）

### 沖繩縣

## 參見

  - [西國三十三箇所](../Page/西國三十三箇所.md "wikilink")
  - [四國八十八箇所](../Page/四國八十八箇所.md "wikilink")
  - [攝津國八十八箇所](../Page/攝津國八十八箇所.md "wikilink")
  - [中國三十三觀音靈場](../Page/中國三十三觀音靈場.md "wikilink")
  - [日本神社列表](../Page/日本神社列表.md "wikilink")
  - [中国寺院列表](../Page/中国寺院列表.md "wikilink")

[日本佛寺](../Category/日本佛寺.md "wikilink")
[Category:日本相關列表](../Category/日本相關列表.md "wikilink")
[Category:佛教建築物列表](../Category/佛教建築物列表.md "wikilink")