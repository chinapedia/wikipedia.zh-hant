[Goodwood_Park_Hotel,_Feb_06.JPG](https://zh.wikipedia.org/wiki/File:Goodwood_Park_Hotel,_Feb_06.JPG "fig:Goodwood_Park_Hotel,_Feb_06.JPG")
**良木園酒店**（[英文](../Page/英文.md "wikilink")：**Goodwood Park
Hotel**）位在[新加坡](../Page/新加坡.md "wikilink")[烏節商圈](../Page/烏節路.md "wikilink")，史各士路22號，位處獅城精華地段，週遭有許多的購物中心。其為[新加坡一家歷史悠久的酒店](../Page/新加坡.md "wikilink")，其地位可與[萊佛士酒店相抗衡](../Page/萊佛士酒店.md "wikilink")。酒店的母公司為良木集團為前新國首富[邱德拔所有現由其女伊麗莎白邱經營](../Page/邱德拔.md "wikilink")。良木集團還擁有新加坡怡閣（約克）飯店（同在烏節區）和英國的皇家花園飯店。良木園酒店高貴不貴，但也因此常只被評為四星級，不過其[英國殖民時期所保留的建築](../Page/英國.md "wikilink")，卻是一般五星級酒店所無法企及的。

## 歷史

[Goodwood_Park_Hotel_10,_Feb_06.JPG](https://zh.wikipedia.org/wiki/File:Goodwood_Park_Hotel_10,_Feb_06.JPG "fig:Goodwood_Park_Hotel_10,_Feb_06.JPG")

良木園酒店前身為新加坡[條頓俱樂部](../Page/條頓.md "wikilink")，完工於1900年，在1918年一對[猶太兄弟買下酒店及週邊五棟建築物](../Page/猶太.md "wikilink")，並將其改名為良木廳，但當時的功能仍是類似俱樂部。不過當[二戰來臨](../Page/第二次世界大戰.md "wikilink")，[日本佔領新加坡](../Page/日本.md "wikilink")，1942年其被改為[皇軍附屬總部](../Page/皇軍.md "wikilink")，到了1945年良木園交回那對猶太兄弟經營。1947年Vivian
Bath成為良木園的共同擁有者，他將良木園改為酒店經營，1963年其被馬來亞銀行集團收購，不過最終在1968年被今日的經營者——[邱德拔](../Page/邱德拔.md "wikilink")[爵士買下](../Page/爵士.md "wikilink")。

## 榮耀

良木園酒店招待過無數名人，包括拳王[阿里](../Page/穆罕默德·阿里.md "wikilink")、Acker Bilk、、Cliff
Richard爵士、Shirley Bassey、Shirley MacLaine、Eartha
Kitt、[成龍](../Page/成龍.md "wikilink")、[周潤發](../Page/周潤發.md "wikilink")、[義大利](../Page/義大利.md "wikilink")[男高音](../Page/男高音.md "wikilink")[安德烈·波伽利](../Page/安德烈·波伽利.md "wikilink")。[英國首相](../Page/英國首相.md "wikilink")[爱德华·希斯和](../Page/爱德华·希斯.md "wikilink")[荷蘭Beatrix皇后](../Page/荷蘭.md "wikilink")。在1978年良木園酒店被Harpers
and Queens U.K.選為世界300大最佳酒店，在1986年成為Preferred Hotels Worldwide的會員之一。

## 柏寧套房樓

柏寧套房樓同屬良木園酒店，不過這是一棟獨立的大廈，就在良木園主樓隔壁，和主樓共用停車場。柏寧套房的房間都比主樓的大，而且內有廚房和起居室，但是相較於主樓的歷史意味，此樓就和普通旅館套房無異。

## 外部連結

  - [良木園酒店](http://www.goodwoodparkhotel.com/)

  -
[Category:新加坡酒店](../Category/新加坡酒店.md "wikilink")