**奧地利國家足球隊**（[德語](../Page/德語.md "wikilink")：****）是[奧地利的官方男子足球代表隊](../Page/奧地利.md "wikilink")，由[奧地利足球協會管理](../Page/奧地利足球協會.md "wikilink")。奧地利曾七次晉級[世界盃決賽周](../Page/世界盃足球賽.md "wikilink")，最近一次是[1998年法國世界盃](../Page/1998年世界盃足球賽.md "wikilink")，奧地利於2008年與[瑞士合辦歐洲國家盃](../Page/瑞士.md "wikilink")，以主辦國身份首次晉級決賽周，但是只能以一和二負成績分組賽出局。

奧地利的球衣贊助商是[Puma](../Page/Puma.md "wikilink")。

## 歷史

[1938年法國世界盃奧地利雖然獲得晉級決賽周](../Page/1938年世界盃足球賽.md "wikilink")，但因[德奧合併](../Page/德奧合併.md "wikilink")（）而退出。[1954年瑞士世界盃奧地利再次晉級決賽周](../Page/1954年世界盃足球賽.md "wikilink")，分組初賽第二名出線，八強淘汰賽以7-5擊敗主辦國[瑞士](../Page/瑞士國家足球隊.md "wikilink")，-{zh-hans:半決賽;
zh-hk:準決賽;}-1-6負於最終的冠軍[西德](../Page/德國國家足球隊.md "wikilink")，季軍戰3-1撃敗[烏拉圭獲得歷來世界盃最佳成績](../Page/烏拉圭國家足球隊.md "wikilink")。

奧地利上世紀七十年代末至八十年代初再創佳績，在[1978年及](../Page/1978年世界盃足球賽.md "wikilink")[1982年連續兩屆晉級世界盃決賽周第二圈賽事](../Page/1982年世界盃足球賽.md "wikilink")，[1978年世界盃足球賽在初賽力壓](../Page/1978年世界盃足球賽.md "wikilink")[巴西以小組首名出線](../Page/巴西國家足球隊.md "wikilink")，在第二圈賽事更擊敗衛冕冠軍的[西德](../Page/西德國家足球隊.md "wikilink")，但敗給後來奪得亞軍的[荷蘭出局](../Page/荷蘭國家足球隊.md "wikilink")。[1982年世界盃足球賽初賽被指與](../Page/1982年世界盃足球賽.md "wikilink")[西德踢](../Page/西德國家足球隊.md "wikilink")「[默契-{zh-hans:球;zh-tw:球;zh-hk:波}-](../Page/希洪之恥.md "wikilink")」，助西德壓倒[阿爾及利亞](../Page/阿爾及利亞國家足球隊.md "wikilink")，與奧地利一同出線次圈，間接令到國際足協決定所有世界盃分組賽最後一場比賽將會同時進行。

### 1998年世界盃

[1998年法國世界盃決賽周奧地利被抽入B組](../Page/1998年世界盃足球賽.md "wikilink")，同組有[義大利](../Page/義大利國家足球隊.md "wikilink")、[喀麥隆及](../Page/喀麥隆國家足球隊.md "wikilink")[智利](../Page/智利國家足球隊.md "wikilink")。當屆的奧地利一如預料在分組賽出局，但奇怪地奧地利在每場比賽臨完場前才取得入球：首場比賽喀麥隆尼贊卡（）的美妙入球被[-{zh-hans:托尼·波尔斯特;
zh-hk:保士達;}-](../Page/托尼·波尔斯特.md "wikilink")（）追平；次場比賽華士達（）最後一分鐘彎入再次追平沙拉斯（）具爭議的先開紀錄入球；但幸運之神在王子球場（）對義大利最後一場比賽離棄奧地利，下半場義大利先後兩次射破奧地利大門，[-{zh-hans:克里斯蒂安·维耶里;
zh-hk:韋利;}-](../Page/克里斯蒂安·维耶里.md "wikilink")（）頭球先破網，[-{zh-hans:罗伯特·巴乔;zh-hk:巴治奧;zh-tw:羅貝托·巴吉歐;}-](../Page/罗伯特·巴乔.md "wikilink")（）錦上添花，雖然[-{zh-hans:安德雷斯·赫尔佐格;
zh-hk:靴索;}-](../Page/安德雷斯·赫尔佐格.md "wikilink")（）在完場前射入-{zh-hans:點球;
zh-hk:十二碼;}-保持球隊最後時刻入球的形式，但不能阻止奧地利排於義大利及智利之後在分組初賽出局。[Österreichische_Fußballnationalmannschaft_2011-09-06_(01).jpg](https://zh.wikipedia.org/wiki/File:Österreichische_Fußballnationalmannschaft_2011-09-06_\(01\).jpg "fig:Österreichische_Fußballnationalmannschaft_2011-09-06_(01).jpg")

## 世界盃成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>總排名</p></th>
<th><p>場數</p></th>
<th><p>勝</p></th>
<th><p>和*</p></th>
<th><p>負</p></th>
<th><p>入球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1930年世界盃足球賽.md" title="wikilink">1930年</a></p></td>
<td><p><em>無參賽</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1934年世界盃足球賽.md" title="wikilink">1934年</a></p></td>
<td><p><strong>殿軍</strong></p></td>
<td><p><strong>4</strong></p></td>
<td><p><strong>4</strong></p></td>
<td><p><strong>2</strong></p></td>
<td><p><strong>0</strong></p></td>
<td><p><strong>2</strong></p></td>
<td><p><strong>7</strong></p></td>
<td><p><strong>7</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1938年世界盃足球賽.md" title="wikilink">1938年</a></p></td>
<td><p><em>晉級決賽周，因<a href="../Page/德奧合併.md" title="wikilink">德奧合併而退出</a></em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1950年世界盃足球賽.md" title="wikilink">1950年</a></p></td>
<td><p><em>退出</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1954年世界盃足球賽.md" title="wikilink">1954年</a></p></td>
<td><p><strong>季軍</strong></p></td>
<td><p><strong>3</strong></p></td>
<td><p><strong>5</strong></p></td>
<td><p><strong>4</strong></p></td>
<td><p><strong>0</strong></p></td>
<td><p><strong>1</strong></p></td>
<td><p><strong>17</strong></p></td>
<td><p><strong>12</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1958年世界盃足球賽.md" title="wikilink">1958年</a></p></td>
<td><p>第一圈</p></td>
<td><p>15</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1962年世界盃足球賽.md" title="wikilink">1962年</a></p></td>
<td><p><em>退出</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1966年世界盃足球賽.md" title="wikilink">1966年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1970年世界盃足球賽.md" title="wikilink">1970年</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1974年世界盃足球賽.md" title="wikilink">1974年</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1978年世界盃足球賽.md" title="wikilink">1978年</a></p></td>
<td><p>第二圈分組賽</p></td>
<td><p>7</p></td>
<td><p>6</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>7</p></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1982年世界盃足球賽.md" title="wikilink">1982年</a></p></td>
<td><p>第二圈分組賽</p></td>
<td><p>8</p></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>5</p></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1986年世界盃足球賽.md" title="wikilink">1986年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1990年世界盃足球賽.md" title="wikilink">1990年</a></p></td>
<td><p>第一圈</p></td>
<td><p>18</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>2</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1994年世界盃足球賽.md" title="wikilink">1994年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998年</a></p></td>
<td><p>第一圈</p></td>
<td><p>23</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
<td><p>4</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006年</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年世界盃足球賽.md" title="wikilink">2010年</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年世界盃足球賽.md" title="wikilink">2014年</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2018年世界盃足球賽.md" title="wikilink">2018年</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2022年世界盃足球賽.md" title="wikilink">2022年</a></p></td>
<td><p><em>待定</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><strong>總結</strong></p></td>
<td><p><strong>季軍</strong></p></td>
<td><p><strong>7/21</strong></p></td>
<td><p>29</p></td>
<td><p>12</p></td>
<td><p>4</p></td>
<td><p>13</p></td>
<td><p>43</p></td>
<td><p>47</p></td>
</tr>
</tbody>
</table>

## 歐洲國家杯成績

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>最終成績</p></th>
<th><p>場數</p></th>
<th><p>勝</p></th>
<th><p>和*</p></th>
<th><p>負</p></th>
<th><p>入球</p></th>
<th><p>失球</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1960年歐洲國家盃.md" title="wikilink">1960年</a></p></td>
<td><p><em>未能晉級</em></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1964年歐洲國家盃.md" title="wikilink">1964年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1968年歐洲國家盃.md" title="wikilink">1968年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1972年歐洲國家盃.md" title="wikilink">1972年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1976年歐洲國家盃.md" title="wikilink">1976年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1980年歐洲國家盃.md" title="wikilink">1980年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1984年歐洲國家盃.md" title="wikilink">1984年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1988年歐洲國家盃.md" title="wikilink">1988年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1992年歐洲國家盃.md" title="wikilink">1992年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1996年歐洲國家盃.md" title="wikilink">1996年</a>'</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2000年歐洲國家盃.md" title="wikilink">2000年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2004年歐洲國家盃.md" title="wikilink">2004年</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008年歐洲國家盃.md" title="wikilink">2008年</a></p></td>
<td><p>第一圈</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>3</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012年歐洲國家盃.md" title="wikilink">2012年</a></p></td>
<td><p>''未能晉級</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2016年歐洲國家盃.md" title="wikilink">2016年</a></p></td>
<td><p>第一圈</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
</tr>
<tr class="even">
<td><p><strong>總結</strong></p></td>
<td><p>2/15</p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>7</p></td>
</tr>
</tbody>
</table>

## 近賽成績

### [2014年世界盃](../Page/2014年世界盃足球賽.md "wikilink")

  - [外圍賽C組](../Page/2014年世界盃外圍賽歐洲區C組.md "wikilink")

### [2016年歐洲國家盃](../Page/2016年歐洲國家盃外圍賽.md "wikilink")

  - G組

## 球員名單

最新一期球員名單、出賽與進球數更新至2014年6月3日對[捷克的友誼賽](../Page/捷克國家足球隊.md "wikilink")。
|----- \! colspan="9" bgcolor="\#B0D3FB" align="left" | |-----
bgcolor="\#DFEDFD"          |----- \! colspan="9" bgcolor="\#B0D3FB"
align="left" | |----- bgcolor="\#DFEDFD"            |----- \!
colspan="9" bgcolor="\#B0D3FB" align="left" | |----- bgcolor="\#DFEDFD"


## 球衣歷史

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>{{Football kit |pattern_la=_whitesmalllower|pattern_b=_aut12h|pattern_ra=_whitesmalllower|pattern_sh=_dpr10a|pattern_so=_sui12H</p></td>
<td><p>body=FF0000|rightarm=FF0000|shorts=FFFFFF|socks=FF0000|title=2012}}</p></td>
</tr>
</tbody>
</table>

## 著名球員

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/尚尼.md" title="wikilink">尚尼</a>（）</li>
<li><a href="../Page/費辛加.md" title="wikilink">費辛加</a>（）</li>
<li><a href="../Page/Hans_Krankl.md" title="wikilink">哈斯·克兰克尔</a>（）</li>
<li><a href="../Page/Dietmar_Kühbauer.md" title="wikilink">Dietmar Kühbauer</a></li>
<li><a href="../Page/漢納比.md" title="wikilink">漢納比</a>（）</li>
<li><a href="../Page/恩斯特_·_哈佩尔.md" title="wikilink">恩斯特 · 哈佩尔</a>（）</li>
<li><a href="../Page/安德雷斯·赫尔佐格.md" title="wikilink">-{zh-hans:安德雷斯·赫尔佐格; zh-hk:靴索;}-</a>（）</li>
<li><a href="../Page/Anton_Pfeffer.md" title="wikilink">Anton Pfeffer</a></li>
<li><a href="../Page/托尼·波尔斯特.md" title="wikilink">-{zh-hans:托尼·波尔斯特; zh-hk:保士達;}-</a>（）</li>
<li><a href="../Page/Erich_Probst.md" title="wikilink">Erich Probst</a></li>
<li><a href="../Page/普赫斯基.md" title="wikilink">普赫斯基</a>（）</li>
<li><a href="../Page/马蒂亚斯·辛德拉尔.md" title="wikilink">马蒂亚斯·辛德拉尔</a>（）</li>
<li><a href="../Page/伊维卡·瓦斯蒂奇.md" title="wikilink">伊维卡·瓦斯蒂奇</a></li>
<li><a href="../Page/干素爾.md" title="wikilink">干素爾</a>（）</li>
</ul></td>
<td><ul>
<li><a href="../Page/罗兰·科尔曼.md" title="wikilink">罗兰·科尔曼</a>（）</li>
<li><a href="../Page/Toni_Fritsch.md" title="wikilink">Toni Fritsch</a></li>
<li><a href="../Page/Ernst_Ocwirk.md" title="wikilink">Ernst Ocwirk</a></li>
<li><a href="../Page/Franz_Hasil.md" title="wikilink">Franz Hasil</a></li>
<li><a href="../Page/Walter_Zemann.md" title="wikilink">Walter Zemann</a></li>
<li><a href="../Page/弗兰茨·宾德.md" title="wikilink">弗兰茨·宾德</a>（）</li>
<li><a href="../Page/Fritz_Gschweidl.md" title="wikilink">Fritz Gschweidl</a></li>
<li><a href="../Page/Rudi_Hiden.md" title="wikilink">Rudi Hiden</a></li>
<li><a href="../Page/Robert_Körner.md" title="wikilink">Robert Körner</a></li>
<li><a href="../Page/Alfred_Körner.md" title="wikilink">Alfred Körner</a></li>
<li><a href="../Page/Friedl_Koncilia.md" title="wikilink">Friedl Koncilia</a></li>
<li><a href="../Page/Josef_Uridil.md" title="wikilink">Josef Uridil</a></li>
<li><a href="../Page/Johann_Horvath.md" title="wikilink">Johann Horvath</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/沙爾.md" title="wikilink">沙爾</a>（）</li>
<li><a href="../Page/Erich_Hof.md" title="wikilink">Erich Hof</a></li>
<li><a href="../Page/沃尔特·沙赫纳.md" title="wikilink">沃尔特·沙赫纳</a>（）</li>
<li><a href="../Page/Wilhelm_Kreuz.md" title="wikilink">Wilhelm Kreuz</a></li>
<li><a href="../Page/Bruno_Pezzey.md" title="wikilink">Bruno Pezzey</a></li>
<li><a href="../Page/Karl_Koller.md" title="wikilink">Karl Koller</a></li>
<li><a href="../Page/Josef_Blum.md" title="wikilink">Josef Blum</a></li>
<li><a href="../Page/Karl_Decker.md" title="wikilink">Karl Decker</a></li>
<li><a href="../Page/Josef_Smistik.md" title="wikilink">Josef Smistik</a></li>
<li><a href="../Page/Walter_Nausch.md" title="wikilink">Walter Nausch</a></li>
<li><a href="../Page/库尔特·亚拉.md" title="wikilink">库尔特·亚拉</a>（）</li>
<li><a href="../Page/Karl_Sesta.md" title="wikilink">Karl Sesta</a></li>
<li><a href="../Page/Alois_Vogel.md" title="wikilink">Alois Vogel</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 外部連結

  - [RSSSF《1902-2003年》戰績紀錄](http://www.rsssf.com/tableso/oost-intres.html)
  - [RSSSF代表次數及入球最高紀錄](http://www.rsssf.com/miscellaneous/oost-recintlp.html)
  - [RSSSF《1902-1999年》教練名單](http://www.rsssf.com/miscellaneous/oost-coach-triv.html)

[Category:歐洲國家足球隊](../Category/歐洲國家足球隊.md "wikilink")
[Category:奧地利足球](../Category/奧地利足球.md "wikilink")
[Category:奧地利體育國家隊](../Category/奧地利體育國家隊.md "wikilink")