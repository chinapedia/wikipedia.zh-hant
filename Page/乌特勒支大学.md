**乌特勒支大学**（），[荷兰最古老大学之一](../Page/荷兰.md "wikilink")，也是欧洲规模最大的大学之一。

## 历史

乌特勒支大学坐落在荷兰[乌特勒支市](../Page/乌得勒支.md "wikilink")，创办于1636年3月26日。2004年入学学生有26,787人，教职员工8,224人，其中有570正教授。乌特勒支大学是荷兰综合实力最强的大学，也是欧洲最好的研究型大学之一，大学排名荷兰第一，欧洲第六，世界第五十三\[1\]，乌特勒支大学一直以强大的研究实力，高质量的教育水准，和良好的学术声誉而享誉欧洲。乌特勒支大学除了法学院，经济学院以及下属的UCU坐落在乌特勒支城市之内外，其余学院包括图书馆全部坐落于城市东边的Uithof区。

## 院系

乌特勒支大学有7个学院：

  - [文学院](../Page/文学.md "wikilink")
      - [艺术系](../Page/艺术.md "wikilink")
      - [神学系](../Page/神学.md "wikilink")
      - [哲学系](../Page/哲学.md "wikilink")
  - [社会科学和](../Page/社会科学.md "wikilink")[心理学院](../Page/心理学.md "wikilink")
  - [法律](../Page/法律.md "wikilink")、[经济和](../Page/经济.md "wikilink")[工商管理学院](../Page/工商管理.md "wikilink")。
  - [地球物理学院](../Page/地球物理学.md "wikilink")
  - [医学院](../Page/医学.md "wikilink")
  - [兽医学院](../Page/兽医学.md "wikilink")
  - [理学院](../Page/理学.md "wikilink")
      - [生物系](../Page/生物.md "wikilink")
      - [化学系](../Page/化学.md "wikilink")
      - [通讯和](../Page/通讯.md "wikilink")[计算机系](../Page/计算机.md "wikilink")
      - [数学系](../Page/数学.md "wikilink")
      - [药物学系](../Page/药物学.md "wikilink")
      - [物理学和](../Page/物理学.md "wikilink")[天文学系](../Page/天文学.md "wikilink")。

## 著名校友

  - 1901年[伦琴获](../Page/伦琴.md "wikilink")[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")
  - 1901年[雅克布斯·范特霍夫获](../Page/雅克布斯·范特霍夫.md "wikilink")[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")
  - 1924年[威廉·埃因托芬获](../Page/威廉·埃因托芬.md "wikilink")[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")
  - 1929年[克里斯蒂安·艾克曼获](../Page/克里斯蒂安·艾克曼.md "wikilink")[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")
  - 1975年[佳林·库普曼斯获](../Page/佳林·库普曼斯.md "wikilink")[诺贝尔经济学奖](../Page/诺贝尔经济学奖.md "wikilink")
  - 1981年[布洛姆伯根获](../Page/布洛姆伯根.md "wikilink")[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")
  - 1999年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")：[杰拉德斯·图夫特](../Page/杰拉德斯·图夫特.md "wikilink")、[韦尔特曼](../Page/韦尔特曼.md "wikilink")
  - [高罗佩](../Page/高罗佩.md "wikilink")，[汉学家](../Page/汉学家.md "wikilink")、[外交家](../Page/外交家.md "wikilink")、[翻译家](../Page/翻译家.md "wikilink")、[小说家](../Page/小说家.md "wikilink")。

## 画廊

[File:Academiegebouw-utrecht-the-netherlands.png|大学本部](File:Academiegebouw-utrecht-the-netherlands.png%7C大学本部)
<File:Academiegebouw> (Rectorat de l'Université d'Utrecht).JPG|学术楼
Image:De_Uithof_(nouveau_campus_de_l'Université_d'Utrecht).JPG|现代建筑楼
Image:University_College_Utrecht_(Entrance).JPG|大学
Image:University_College_Utrecht.JPG|大学
Image:Juridische_Bibliotheek_(Bibliothèque_de_Droit_et_d'Economie).JPG|法学院图书馆
Image:Vie_étudiante_à_Utrecht.JPG|学生生活

## 参考资料

[category:乌特勒支大学](../Page/category:乌特勒支大学.md "wikilink")

[Category:1630年代創建的教育機構](../Category/1630年代創建的教育機構.md "wikilink")

1.