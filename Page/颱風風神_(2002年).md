**颱風風神**（，國際編號：**0209**，[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")：**12W**）為[2002年太平洋颱風季第九個被命名的風暴](../Page/2002年太平洋颱風季.md "wikilink")。「風神」一名由[中國提供](../Page/中國.md "wikilink")，意思是掌管風的神。

## 發展過程

7月13日，[季風槽中催生出一股規模很小的](../Page/季風槽.md "wikilink")[熱帶低氣壓](../Page/熱帶低氣壓.md "wikilink")。系統迅速強化，到7月15日已達[颱風強度](../Page/颱風.md "wikilink")。氣旋起初向北移動，然後開始轉向西北。根據[日本氣象廳的數據](../Page/日本氣象廳.md "wikilink")，颱風風神於7月18日達到[10分鐘持續風速每小時](../Page/最大持續風速.md "wikilink")185公里的最高強度，成為[年度最強風暴](../Page/2002年太平洋颱風季.md "wikilink")。[聯合颱風警報中心估計系統的](../Page/聯合颱風警報中心.md "wikilink")1分鐘持續風力時速為270公里，並在[超級颱風強度下保持了](../Page/超級颱風.md "wikilink")5天之久。\[1\]\[2\]這打破了[1997年](../Page/1997年太平洋颱風季.md "wikilink")[颱風瓊創下的超級颱風持續時長紀錄](../Page/颱風瓊_\(1997年\).md "wikilink")\[3\]，直至[2006年才由](../Page/2006年太平洋颱風季.md "wikilink")[颶風伊歐凱追平](../Page/颶風伊歐凱.md "wikilink")\[4\]。

## 影響

###

颱風風神在接近最高強度時與[颱風鳳凰發生藤原效應](../Page/颱風鳳凰_\(2002年\).md "wikilink")，令鳳凰轉動到風神的南面\[5\]。風神在接近[日本期間逐漸減弱](../Page/日本.md "wikilink")，於7月25日以[強烈熱帶風暴強度經過該國](../Page/強烈熱帶風暴.md "wikilink")[大隅群島](../Page/大隅群島.md "wikilink")\[6\]。風暴將一艘貨輪衝到岸上，船上4人溺斃，另外15人獲救\[7\]\[8\]。氣旋給日本帶去強降雨\[9\]，引發多場[泥石流](../Page/泥石流.md "wikilink")，造成價值約400萬[美元的莊稼損失](../Page/美元.md "wikilink")\[10\]{{\#tag:ref|報導中原本採用的貨幣單位是[日圓](../Page/日圓.md "wikilink")，經公司網站外匯歷史匯率頁面轉換成[美元](../Page/美元.md "wikilink")\[11\]。|group="注"}}，還有1人死亡\[12\]。

###

襲擊[日本後](../Page/日本.md "wikilink")，系統在[黃海弱化成](../Page/黃海.md "wikilink")[熱帶低氣壓](../Page/熱帶低氣壓.md "wikilink")，並在經過[中華人民共和國](../Page/中華人民共和國.md "wikilink")[山東半島後於](../Page/山東半島.md "wikilink")7月28日逐漸消散\[13\]。

## 圖片庫

Typhoon Fengshen 14 july 2002 2325Z.jpg|7月14日 Typhoon Fengshen 16 july
2002 2310Z.jpg|7月16日 Typhoon Fengshen 22 july 2002 0105Z.jpg|7月22日
Fengshen and Fung-Wong 25 july 2002
0135Z.jpg|7月25日的風神（上）、[鳳凰](../Page/颱風鳳凰_\(2002年\).md "wikilink")（下）
Fengshen and Fung-Wong 26 july 2002 0220Z.jpg|7月26日的風神（左）、鳳凰（右）

## 注釋

## 參考資料

## 參見

  - [颱風鳳凰 (2002年)](../Page/颱風鳳凰_\(2002年\).md "wikilink")
  - [藤原效應](../Page/藤原效應.md "wikilink")
  - [2002年太平洋颱風季](../Page/2002年太平洋颱風季.md "wikilink")

## 外部連結

  - <http://www.jma.go.jp/> [日本氣象廳首頁](../Page/日本氣象廳.md "wikilink")

  - <http://www.usno.navy.mil/JTWC/>
    [聯合颱風警報中心首頁](../Page/聯合颱風警報中心.md "wikilink")

  - <http://www.nmc.gov.cn/> [中央氣象台首頁](../Page/中央氣象台.md "wikilink")

  - <http://www.cwb.gov.tw/> [中央氣象局首頁](../Page/中央氣象局.md "wikilink")

  - <http://www.hko.gov.hk/> [香港天文台首頁](../Page/香港天文台.md "wikilink")

  - <http://www.smg.gov.mo/>
    [澳門地球物理暨氣象局首頁](../Page/澳門地球物理暨氣象局.md "wikilink")

[Category:2002年太平洋台风季](../Category/2002年太平洋台风季.md "wikilink")
[Category:五級熱帶氣旋](../Category/五級熱帶氣旋.md "wikilink")
[Category:影响中国大陆的热带气旋](../Category/影响中国大陆的热带气旋.md "wikilink")
[Category:影響日本的熱帶氣旋](../Category/影響日本的熱帶氣旋.md "wikilink")
[Category:2002年中国](../Category/2002年中国.md "wikilink")
[Category:2002年日本](../Category/2002年日本.md "wikilink")
[Category:台风](../Category/台风.md "wikilink")

1.

2.

3.
4.

5.
6.
7.

8.

9.

10.

11.

12.
13.