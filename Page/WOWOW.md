[Wowow_head_office.jpg](https://zh.wikipedia.org/wiki/File:Wowow_head_office.jpg "fig:Wowow_head_office.jpg")
**WOWOW**（）是[日本的一家收費民營衛星](../Page/日本.md "wikilink")[電視台](../Page/電視台.md "wikilink")，成立于1984年12月25日，開播於1991年4月1日。其節目以[電影為主](../Page/電影.md "wikilink")，此外也有大量的[動畫片](../Page/動畫片.md "wikilink")、[網球](../Page/網球.md "wikilink")、[足球和](../Page/足球.md "wikilink")[音樂節目](../Page/音樂.md "wikilink")。

由WOWOW播放的著名動畫節目包括《[星際牛仔](../Page/星際牛仔.md "wikilink")》、《[死亡代理人](../Page/死亡代理人.md "wikilink")》、《[星界的紋章](../Page/星界的纹章.md "wikilink")》及《[妄想代理人](../Page/妄想代理人.md "wikilink")》等。

於 BS 頻道為 CH191, CH192, CH193

## 外部連結

  - [WOWOW ONLINE](http://www.wowow.co.jp/index.shtml)

[\*](../Category/WOWOW.md "wikilink")
[Category:日本電視台](../Category/日本電視台.md "wikilink")
[Category:港區公司 (東京都)](../Category/港區公司_\(東京都\).md "wikilink")
[Category:日本衛星電視頻道](../Category/日本衛星電視頻道.md "wikilink")
[Category:1984年成立的公司](../Category/1984年成立的公司.md "wikilink")