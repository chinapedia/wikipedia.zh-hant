**XF5F**[戰鬥機是](../Page/戰鬥機.md "wikilink")[格鲁门公司設計的一款試驗性雙發動機艦載戰鬥機](../Page/格鲁门公司.md "wikilink")，這架飛機同時也被[美國陸軍看中](../Page/美國陸軍.md "wikilink")，改良成為[XP-50戰鬥機](../Page/XP-50.md "wikilink")，不過這兩架飛機都只處於試驗階段。官方正式名稱是**空中火箭**（Skyrocket）。

## 歷史

[美國海軍於](../Page/美國海軍.md "wikilink")1937年發出雙發動機艦載戰鬥機的設計需求，其中要求這架飛機的最大速度需要超過每小時480公里，一共有4家公司提出他們的設計方案，格魯曼公司的設計是採用兩具[艾利森公司](../Page/艾利森公司.md "wikilink")[V-1710](../Page/V-1710.md "wikilink")[液冷式發動機加上渦輪](../Page/液冷式發動機.md "wikilink")[增壓器](../Page/增壓器.md "wikilink")。經過審核之後，海軍認為沒有一款設計預估性能足以滿足需求。因此1938年初海軍提出修改方案，設計規格放寬為可以採用一具液冷式發動機加上機械增壓器，或者是兩具氣冷式發動機配合渦輪增壓器。

這一次一共有5家公司提出他們的設計案，其中[渥特公司](../Page/渥特公司.md "wikilink")（Vought）是採用一具普萊特·惠特尼公司的R-2800氣冷式發動機作為動力來源，雖然這一款發動機並未包含在海軍提出的需求裡面，稍後海軍還是發出合約給渥特公司進行研發，成為[F4U戰鬥機](../Page/F4U.md "wikilink")。格魯門公司提出的設計是採用兩具普萊特·惠特尼公司R-1535氣冷式發動機配合兩速機械增壓器。可是在設計初期格魯門被迫改用萊特公司直徑較大的R-1820發動機。由於直徑較大的發動機會影響前方與下方的視野，這對在[航空母艦上起降作業相當不便](../Page/航空母艦.md "wikilink")，使得美國海軍很不願意接受這項變更。

1940年4月第一架原型機展開試飛的工作，為了提供良好的飛行員視野，XF5F有一個非常短的機鼻，整個機身沒有超過機翼的前緣，看起來像是沒有機鼻的樣子。即使如此，飛行員下方與側方的視野仍然因為發動機的直徑關係而很有限。

從試飛開始XF5F的問題就相當多，包括發動機潤滑油冷卻不足，阻力過高，起落架艙門的關閉有困難等等。到了1942年2月遞交給海軍時，XF5F的前景已經不被看好。1944年11月在一次起落架故障中損傷過重而讓研發計畫正式畫上休止符。相關的研究資料日後協助格魯門設計第一架雙發動機艦載戰鬥機F7F。

## 規格 (XF5F Skyrocket)

## 参考文献

<div class="references-small">

<references />

  - Green, William. *War Planes of the Second World War - Fighters, Vol
    4*. London, UK: MacDonald, 1961. ISBN 0-356-01448-7.
  - Green, William and Swanborough, Gordon. *WW2 Aircraft Fact Files: US
    Navy and Marine Corps Fighters*. London, UK: Macdonald and Jane's,
    1976. ISBN 0-356-08222-9.
  - Lucabaugh, David and Martin, Bob. *Grumman XF5F-1 & XP-50 Skyrocket,
    Naval fighters number thirty-one*. Simi Valley, CA: Ginter Books,
    1995. ISBN 0-942612-31-0.
  - Morgan, Eric B. "Grumman F7F Tigercat F.7/30". *Twentyfirst Profile,
    Volume 1, No. 11*. New Milton, Hants, UK: 21st Profile Ltd. ISBN
    0-961-82100-4.
  - Morgan, Eric B. "The Grumman Twins". *Twentyfirst Profile, Volume 2,
    No. 15*. New Milton, Hants, UK: 21st Profile Ltd. ISBN
    0-961-82101-1.

</div>

[Category:美國戰鬥機](../Category/美國戰鬥機.md "wikilink")