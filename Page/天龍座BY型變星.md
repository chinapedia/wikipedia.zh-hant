**天龍座BY型變星**通常屬於[晚期的K或M型](../Page/恆星光譜.md "wikilink")[主序星變星](../Page/主序星.md "wikilink")。
這個名稱來自這種變星的原型：[天龍座BY](../Page/天龍座BY.md "wikilink")。由於表面的[星斑和](../Page/星斑.md "wikilink")[恆星的自轉](../Page/恆星.md "wikilink")，和[色球層上的其它活動](../Page/色球層.md "wikilink")，使它們展現出[光度上的變化](../Page/光度.md "wikilink")\[1\]。天龍座BY型變星的亮度波動一般小於0.5[視星等](../Page/視星等.md "wikilink")，[光變曲線週期接近恆星的平均自轉週期](../Page/光變曲線.md "wikilink")，被稱為。在週期內的[光變曲線是不規則的](../Page/光變曲線.md "wikilink")，並且從一個週期到下一個週期，在形狀上略有變化。對[天龍座BY](../Page/天龍座BY.md "wikilink")，週期大約是一個月，而形狀保持相似\[2\]。

鄰近的K和M型的天龍座BY型變星包括[巴納德星](../Page/巴納德星.md "wikilink")、[卡普坦星](../Page/卡普坦星.md "wikilink")、[天鵝座61](../Page/天鵝座61.md "wikilink")、[羅斯248](../Page/羅斯248.md "wikilink")、[拉卡伊8760](../Page/拉卡伊8760.md "wikilink")、[拉蘭德21185和](../Page/拉蘭德21185.md "wikilink")[魯坦726-8](../Page/魯坦726-8.md "wikilink")。羅斯248
是被發現的第一顆天龍座BY型變星，傑拉爾德寇倫在1950年辨識出它的變異。天龍座BY本身的變異在1966年才被發現，Sugainov在1973-1976年間對它進行了詳細的研究\[3\]。

比太陽明亮的[南河三](../Page/南河三.md "wikilink")，光譜類型為F5
IV/V，也被宣稱是天龍座BY型變星\[4\]。南河三有兩點異於平常：它正在移出[主序星進入](../Page/主序星.md "wikilink")[次巨星階段](../Page/次巨星.md "wikilink")；它的核融合反應介於[PP燃燒和](../Page/質子-質子鏈反應.md "wikilink")[CNO燃燒](../Page/碳氮氧循環.md "wikilink")，這意味著它的中間有對流層和非對流層。而任何一個理由都可以大於正常的[星斑](../Page/星斑.md "wikilink")。

有些天龍座BY型變星會出現[閃光](../Page/耀星.md "wikilink")，而成為另一型變星：[鯨魚座UV型變星](../Page/耀星.md "wikilink")\[5\]。同樣的，天龍座BY型變星的光譜（特別是[H和K線](../Page/鈣.md "wikilink")）類似於[獵犬座RS型變星](../Page/獵犬座RS型變星.md "wikilink")，這是另一種有著活躍色球層活動的[變星](../Page/變星.md "wikilink")\[6\]

## 參考資料

## 進階讀物

  - Samus N.N., Durlevich O.V., et al. *Combined General Catalog of
    Variable Stars (GCVS4.2, 2004 Ed.)*
  - Schaaf, Fred, *The Brightest Stars*, Wiley, 2008

[天龍座BY變星](../Category/天龍座BY變星.md "wikilink")
[Category:變星](../Category/變星.md "wikilink")
[Category:恆星物理學](../Category/恆星物理學.md "wikilink")

1.
2.
3.
4.
5.
6.