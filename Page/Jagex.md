Jagex
是一間[網頁遊戲開發公司](../Page/網頁遊戲.md "wikilink")，遊戲多以[Java語言開發](../Page/Java.md "wikilink")，名稱原本來自"**Ja**va
**G**ame **Ex**pert"\[1\]，在開始代理[War of
Legends之後](../Page/War_of_Legends.md "wikilink")，更名為"Just About the
Game
Experience"\[2\]。公司開發的遊戲不多，其中最大型的網頁遊戲乃[Runescape](../Page/Runescape.md "wikilink")。Runescape
廣受歡迎，在美國、英國、加拿大、荷蘭、澳大利亞、芬蘭以及瑞典等地均設有[伺服器](../Page/伺服器.md "wikilink")。

繼 [Runescape](../Page/Runescape.md "wikilink") 後，Jagex 在 27-2-2008
推出另一個以java開發的的小型網頁遊戲平台
[FunOrb](../Page/FunOrb.md "wikilink")。

另外，杰格克斯以War of Legends的名字代理中国游戏“封神无敌”。

## 參考文獻

[FunOrb](../Page/FunOrb.md "wikilink") }}

[Category:英國電子遊戲公司](../Category/英國電子遊戲公司.md "wikilink")
[Category:2001年英國建立](../Category/2001年英國建立.md "wikilink")

1.  <http://www.runescape.com/kbase/viewarticle.ws?article_id=746>
    有關Jagex
2.