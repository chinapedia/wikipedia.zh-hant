**中山藏藝所**位於[臺北市](../Page/臺北市.md "wikilink")[中山區](../Page/中山區_\(臺北市\).md "wikilink")[長安西路](../Page/長安西路.md "wikilink")15號，為1930年代[臺灣日治時期所蓋的兩層樓磚造](../Page/臺灣日治時期.md "wikilink")[建築](../Page/建築.md "wikilink")。該建築採[折衷主義](../Page/折衷主義.md "wikilink")，特色是外觀簡潔的現代建築，但木窗、橫帶裝飾仍夾雜古典意味，整體表現出1930年代的歐風氣息。另外在內部方面，該建物樑柱、樓梯等結構部分採用原木打造，讓迴廊與走道等內部[裝潢呈現不俗的另外典雅風味](../Page/裝潢.md "wikilink")。

[二戰前](../Page/二戰.md "wikilink")，該建物為[臺北州地方政府公設的](../Page/臺北州.md "wikilink")[職業介紹所](../Page/職業介紹所.md "wikilink")，一者提供介紹工作的服務，一者則訓練失業者工作技藝以助重返職場。1945年，[國民政府則將其作為臺北市](../Page/國民政府.md "wikilink")[公共衛生的執行場所](../Page/公共衛生.md "wikilink")，謂之**臺北市衛生院**。經過數度更名與升格後，1967年，該單位更名為**[臺北市政府衛生局](../Page/臺北市政府衛生局.md "wikilink")**。

1994年，因應[臺北市政府從](../Page/臺北市政府.md "wikilink")[臺北市政府舊廈遷出](../Page/臺北市政府舊廈.md "wikilink")，所留舊址，因極具保存價值，1998年5月4日臺北市政府公告為市定[古蹟](../Page/古蹟.md "wikilink")。\[1\]

2000年，臺北市政府將其建物移交[臺北市政府社會局](../Page/臺北市政府社會局.md "wikilink")，並計劃將該場所挪用[社會救助使用](../Page/社會救助.md "wikilink")。2002年，臺北市政府利用部分[公益彩券提撥的法定福利基金修繕該舊址](../Page/公益彩券.md "wikilink")。

[2003年8月](../Page/2003年8月.md "wikilink")，臺北市政府社會局於該建物成立臺灣第一所[身心障礙福利會館](../Page/身心障礙.md "wikilink")「臺北市身心障礙福利會館」，對外開放，設有餐飲、[咖啡廳](../Page/咖啡廳.md "wikilink")，不但將其收入全數投入社會救助，也提供身心障礙人士就業機會。2012年10月1日，臺北市身心障礙福利會館為因應第二期新建工程而休館。2013年1月21日，臺北市身心障礙福利會館第二期新建工程開工[動土](../Page/動土.md "wikilink")，於該建物後方興建地上6層、地下2層的大樓。2014年12月18日，臺北市身心障礙福利會館第二期大樓（臺北市中山區長安西路5巷2號）落成。2017年1月26日，臺北市身心障礙福利會館第二期大樓正式更名為「臺北市政府身心障礙服務中心」\[2\]。

[Old_Taipei_City_Health_Department_entrance_20170728.jpg](https://zh.wikipedia.org/wiki/File:Old_Taipei_City_Health_Department_entrance_20170728.jpg "fig:Old_Taipei_City_Health_Department_entrance_20170728.jpg")
2017年4月29日，臺北市身心障礙福利會館更名為「中山藏藝所」重新開放，臺北市政府社會局委託[喜憨兒基金會經營管理](../Page/喜憨兒基金會.md "wikilink")\[3\]，作為一處公益平台，可供展覽、演講、工作坊或舉辦任何非營利活動。

## 參考資料

## 外部連結

  -
[Category:台北市古蹟](../Category/台北市古蹟.md "wikilink") [Category:中山區
(臺北市)](../Category/中山區_\(臺北市\).md "wikilink")
[Category:1930年代完成的建築物](../Category/1930年代完成的建築物.md "wikilink")
[Category:台灣日治時期官署建築](../Category/台灣日治時期官署建築.md "wikilink")
[Category:臺北市衛生局](../Category/臺北市衛生局.md "wikilink")

1.
2.
3.