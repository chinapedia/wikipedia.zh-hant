**超铀元素**在[化学上指的是](../Page/化学.md "wikilink")[原子序数在](../Page/原子序数.md "wikilink")92（[铀](../Page/铀.md "wikilink")）以上的重[元素](../Page/元素.md "wikilink")。原子序数从1到92的元素中，除了[锝](../Page/锝.md "wikilink")，[钷](../Page/钷.md "wikilink")，[砹](../Page/砹.md "wikilink")，[钫](../Page/钫.md "wikilink")4种物质以外，都可以很容易在[地球上大量检测到](../Page/地球.md "wikilink")，而且比较稳定，有很长的半衰期，或者是铀的普遍[衰变物](../Page/衰变.md "wikilink")。

序数92以上的元素都是首先以[人工合成的办法发现的](../Page/人工合成.md "wikilink")。僅有少數的元素在地球上被發現自然生成，例如[钚](../Page/钚.md "wikilink")、[镎](../Page/镎.md "wikilink")、[鉲等](../Page/鉲.md "wikilink")，因为他们都有[放射性](../Page/放射性.md "wikilink")，[半衰期短](../Page/半衰期.md "wikilink")。可以在富铀的矿石中检测到[钚的痕迹](../Page/钚.md "wikilink")，在[核试验后也有少量生成](../Page/核试验.md "wikilink")。它们是铀矿石经过[中子俘获紧接着两次](../Page/中子俘获.md "wikilink")[β衰变而成的](../Page/β衰变.md "wikilink")：（[<sup>238</sup>U](../Page/铀.md "wikilink")
→ [<sup>239</sup>U](../Page/铀.md "wikilink") →
[<sup>239</sup>Np](../Page/镎.md "wikilink") →
[<sup>239</sup>Pu](../Page/钚.md "wikilink")）。

这些元素现在可以用核反应堆或者粒子加速器人工合成。这些元素的半衰期有随着序数的增加而有缩短的趋势，然而也有例外：例如[𨧀和](../Page/𨧀.md "wikilink")[锔的一些](../Page/锔.md "wikilink")[同位素](../Page/同位素.md "wikilink")。[格伦·西奥多·西博格预言了在这一系列元素中更多的反常元素](../Page/格伦·西奥多·西博格.md "wikilink")，并且把它们归类于“[稳定岛](../Page/稳定岛理论.md "wikilink")”，即[质子或](../Page/质子.md "wikilink")[中子为](../Page/中子.md "wikilink")[幻数的](../Page/幻数.md "wikilink")[原子核具有特别的稳定性](../Page/原子核.md "wikilink")。

超铀元素中未发现的元素以及发现但[未命名的元素](../Page/未发现元素.md "wikilink")，使用[IUPAC元素系统命名法](../Page/IUPAC元素系统命名法.md "wikilink")。超铀元素的命名曾引起很大的争论，104到109号元素命名的争论从二十世纪六十年代开始，一直到1997年才解决。

## 電子分佈

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/元素序號列表.md" title="wikilink">序號</a></p></th>
<th><p><a href="../Page/元素.md" title="wikilink">元素</a></p></th>
<th><p><a href="../Page/電子.md" title="wikilink">電子在每</a><a href="../Page/能級.md" title="wikilink">能級的排佈</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>93</p></td>
<td><p><a href="../Page/镎.md" title="wikilink">镎</a></p></td>
<td><p>2，8，18，32，22，9，2</p></td>
</tr>
<tr class="even">
<td><p>94</p></td>
<td><p><a href="../Page/钚.md" title="wikilink">钚</a></p></td>
<td><p>2，8，18，32，24，8，2</p></td>
</tr>
<tr class="odd">
<td><p>95</p></td>
<td><p><a href="../Page/镅.md" title="wikilink">镅</a></p></td>
<td><p>2，8，18，32，25，8，2</p></td>
</tr>
<tr class="even">
<td><p>96</p></td>
<td><p><a href="../Page/鋦.md" title="wikilink">鋦</a></p></td>
<td><p>2，8，18，32，25，9，2</p></td>
</tr>
<tr class="odd">
<td><p>97</p></td>
<td><p><a href="../Page/锫.md" title="wikilink">锫</a></p></td>
<td><p>2，8，18，32，27，8，2</p></td>
</tr>
<tr class="even">
<td><p>98</p></td>
<td><p><a href="../Page/鐦.md" title="wikilink">鐦</a></p></td>
<td><p>2，8，18，32，28，8，2</p></td>
</tr>
<tr class="odd">
<td><p>99</p></td>
<td><p><a href="../Page/鎄.md" title="wikilink">鎄</a></p></td>
<td><p>2，8，18，32，29，8，2</p></td>
</tr>
<tr class="even">
<td><p>100</p></td>
<td><p><a href="../Page/鐨.md" title="wikilink">鐨</a></p></td>
<td><p>2，8，18，32，30，8，2</p></td>
</tr>
<tr class="odd">
<td><p>101</p></td>
<td><p><a href="../Page/鍆.md" title="wikilink">鍆</a></p></td>
<td><p>2，8，18，32，31，8，2</p></td>
</tr>
<tr class="even">
<td><p>102</p></td>
<td><p><a href="../Page/鍩.md" title="wikilink">鍩</a></p></td>
<td><p>2，8，18，32，32，8，2</p></td>
</tr>
<tr class="odd">
<td><p>103</p></td>
<td><p><a href="../Page/鐒.md" title="wikilink">鐒</a></p></td>
<td><p>2，8，18，32，32，8，3</p></td>
</tr>
<tr class="even">
<td><p>104</p></td>
<td><p><a href="../Page/鑪.md" title="wikilink">鑪</a></p></td>
<td><p>2，8，18，32，32，10，2</p></td>
</tr>
<tr class="odd">
<td><p>105</p></td>
<td><p><a href="../Page/𨧀.md" title="wikilink">𨧀</a></p></td>
<td><p>2，8，18，32，32，11，2</p></td>
</tr>
<tr class="even">
<td><p>106</p></td>
<td><p><a href="../Page/𨭎.md" title="wikilink">𨭎</a></p></td>
<td><p>2，8，18，32，32，12，2</p></td>
</tr>
<tr class="odd">
<td><p>107</p></td>
<td><p><a href="../Page/𨨏.md" title="wikilink">𨨏</a></p></td>
<td><p>2、8、18、32、32、13、2</p></td>
</tr>
<tr class="even">
<td><p>108</p></td>
<td><p><a href="../Page/𨭆.md" title="wikilink">𨭆</a></p></td>
<td><p>2、8、18、32、32、14、2</p></td>
</tr>
<tr class="odd">
<td><p>109</p></td>
<td><p><a href="../Page/䥑.md" title="wikilink">䥑</a></p></td>
<td><p>2, 8, 18, 32, 32, 15, 2</p></td>
</tr>
<tr class="even">
<td><p>110</p></td>
<td><p><a href="../Page/鐽.md" title="wikilink">鐽</a></p></td>
<td><p>2, 8, 18, 32, 32, 16, 2</p></td>
</tr>
<tr class="odd">
<td><p>111</p></td>
<td><p><a href="../Page/錀.md" title="wikilink">錀</a></p></td>
<td><p>2, 8, 18, 32, 32, 18, 1</p></td>
</tr>
<tr class="even">
<td><p>112</p></td>
<td><p><a href="../Page/鎶.md" title="wikilink">鎶</a></p></td>
<td><p>2, 8, 18, 32, 32, 18, 2</p></td>
</tr>
<tr class="odd">
<td><p>113</p></td>
<td><p><a href="../Page/鉨.md" title="wikilink">鉨</a></p></td>
<td><p>2, 8, 18, 32, 32, 18, 3</p></td>
</tr>
<tr class="even">
<td><p>114</p></td>
<td><p><a href="../Page/鈇.md" title="wikilink">鈇</a></p></td>
<td><p>2, 8, 18, 32, 32, 18, 4</p></td>
</tr>
<tr class="odd">
<td><p>115</p></td>
<td><p><a href="../Page/鏌.md" title="wikilink">鏌</a></p></td>
<td><p>2, 8, 18, 32, 32, 18, 5</p></td>
</tr>
<tr class="even">
<td><p>116</p></td>
<td><p><a href="../Page/鉝.md" title="wikilink">鉝</a></p></td>
<td><p>2, 8, 18, 32, 32, 18, 6</p></td>
</tr>
<tr class="odd">
<td><p>117</p></td>
<td></td>
<td><p>2, 8, 18, 32, 32, 18, 7</p></td>
</tr>
<tr class="even">
<td><p>118</p></td>
<td><p>-{zh-hans:;zh-hant:}-</p></td>
<td><p>2, 8, 18, 32, 32, 18, 8</p></td>
</tr>
<tr class="odd">
<td><p>119</p></td>
<td><p><a href="../Page/Uue.md" title="wikilink">Uue</a></p></td>
<td><p>2, 8, 18, 32, 32, 18, 8, 1</p></td>
</tr>
<tr class="even">
<td><p>120</p></td>
<td><p><a href="../Page/Ubn.md" title="wikilink">Ubn</a></p></td>
<td><p>2, 8, 18, 32, 32, 18, 8, 2</p></td>
</tr>
</tbody>
</table>

## 参见

  - [元素周期表](../Page/元素周期表.md "wikilink")
  - [超重元素](../Page/超重元素.md "wikilink")

## 参考资料

  - [超铀元素](https://web.archive.org/web/20070503140615/http://web.fccj.org/~ethall/uranium/uranium.htm)

[Category:元素周期表](../Category/元素周期表.md "wikilink")
[C](../Category/原子核物理学.md "wikilink")