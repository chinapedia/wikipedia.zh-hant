**中尾衣里**（）是[日本](../Page/日本.md "wikilink")[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市出身的女性](../Page/神戶市.md "wikilink")[聲優](../Page/聲優.md "wikilink")。身高163.5公分。[血型B型](../Page/血型.md "wikilink")。從屬於[大澤事務所](../Page/大澤事務所.md "wikilink")。

## 特色

「[ZEGAPAIN](../Page/ZEGAPAIN.md "wikilink")」裡的酷女性、「[萌單](../Page/萌單.md "wikilink")」裡的手塚澪、「[貓願三角戀](../Page/貓願三角戀.md "wikilink")」裡的妹妹高坂鈴、「[月面兔兵器米娜](../Page/月面兔兵器米娜.md "wikilink")」的六棟愛絲卡爾汀的雙面女性、「[野良耳](../Page/野良耳.md "wikilink")」的直美一般的特徵角色，不僅如此人群中的少年還有媽媽，甚至是老奶奶的角色都可以辦到。此外，在CM裡擔任旁白的活躍也很多，年齡對應綺麗系（漂亮）的女性聲音擔任很多。全部與她的說話方式完全不同。

## 演出作品

※**粗體字**為主要角色

### 電視動畫

**2004年**

  - [這醜陋又美麗的世界](../Page/這醜陋又美麗的世界.md "wikilink")（女學生）

**2005年**

  - [混沌武士](../Page/混沌武士.md "wikilink")（小孩）
  - [SPEED GRAPHER](../Page/SPEED_GRAPHER.md "wikilink")（同學2）
  - [少年悍將](../Page/少年悍將.md "wikilink")（キトゥン）
  - [血戰](../Page/血戰.md "wikilink")（蕾米）※第4話登場

**2006年**

  - [童話槍手小紅帽](../Page/童話槍手小紅帽.md "wikilink")（村人）
  - [女生愛女生](../Page/女生愛女生.md "wikilink")（泊的母親）
  - [Kanon](../Page/Kanon.md "wikilink")（醫師）
  - [金色琴弦](../Page/金色琴弦.md "wikilink")（小林直）
  - [不平衡抽籤](../Page/不平衡抽籤.md "wikilink")（**橘伊月**）
  - [蜂蜜與四葉草](../Page/蜂蜜與四葉草.md "wikilink")（女學生B）
  - [Fate/stay night](../Page/Fate/stay_night.md "wikilink")（三枝由紀香）

**2007年**

  - [君吻](../Page/君吻.md "wikilink")（幼年光一）
  - [月面兔兵器米娜](../Page/月面兔兵器米娜.md "wikilink")（**六棟エスカルティン**）
  - [現視研](../Page/現視研.md "wikilink")（加藤、女子B、女面接官）
  - [灼眼的夏娜II](../Page/灼眼的夏娜.md "wikilink")（佐佐木）
  - [萌單](../Page/萌單.md "wikilink")（**手塚澪**）
  - [戀愛情結](../Page/戀愛情結.md "wikilink")（女學生）
  - [旋風管家](../Page/旋風管家.md "wikilink")（花菱美希）

**2008年**

  - [旋風管家](../Page/旋風管家.md "wikilink")（花菱美希、西澤一樹、女子、老奶奶）
  - [楚楚可憐超能少女組](../Page/楚楚可憐超能少女組.md "wikilink")（常盤奈津子、花菱美希、六棟エスカルティン）

**2009年**

  - [CLANNAD ～AFTER
    STORY～](../Page/CLANNAD_～AFTER_STORY～.md "wikilink")（母親）\*第19回
  - [旋风管家 第二季](../Page/旋风管家.md "wikilink")（花菱美希、西澤一樹）
  - [守護甜心\!\!心跳](../Page/守護甜心.md "wikilink")（一之宫光）
  - [猫愿三角恋](../Page/猫愿三角恋.md "wikilink")（高坂铃）
  - [肯普法](../Page/肯普法.md "wikilink")（中尾沙也香）
  - [守護甜心](../Page/守護甜心.md "wikilink") 派對（一之宫光）

**2012年**

  - [旋風管家！CAN'T TAKE MY EYES OFF
    YOU](../Page/旋風管家_\(動畫\).md "wikilink")（花菱美希）
  - [最強學生會長](../Page/最強學生會長.md "wikilink")（八代三年生）

**2013年**

  - [旋風管家！Cuties](../Page/旋風管家_\(動畫\).md "wikilink")（花菱美希）

**2014年**

  - [人生諮詢電視動畫「人生」](../Page/人生_\(輕小說\).md "wikilink")（山中君江）
  - [Fate/stay
    night（ufotable版）](../Page/Fate/stay_night.md "wikilink")（三枝由紀香）
  - [境界觸發者](../Page/境界觸發者.md "wikilink")（一之瀨、市民、宇佐美栞）

**2015年**

  - [亂步奇譚 拉普拉斯的遊戲](../Page/亂步奇譚_拉普拉斯的遊戲.md "wikilink")（現場記者）

**2017年**

  - [小魔女學園](../Page/小魔女學園.md "wikilink")（漢娜、多林大臣\[1\]）
  - [數碼暴龍宇宙-應用怪獸](../Page/數碼暴龍宇宙-應用怪獸.md "wikilink")（飛鳥珍妮）

### OVA

  - [BALDR FORCE EXE
    RESOLUTION](../Page/BALDR_FORCE_EXE_RESOLUTION.md "wikilink")（小隊長）

### 遊戲

  - [Fate/stay night](../Page/Fate/stay_night.md "wikilink")（三枝由紀香）

## 來源

## 外部連結

  - [大澤事務所公式官網的聲優簡介](http://osawa-inc.co.jp/blocks/index/talent00127.html)


[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:大澤事務所](../Category/大澤事務所.md "wikilink")
[Category:兵庫縣出身人物](../Category/兵庫縣出身人物.md "wikilink")

1.