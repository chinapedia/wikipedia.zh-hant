**真田幸隆**（、[永正](../Page/永正.md "wikilink")10年－[天正](../Page/天正.md "wikilink")2年5月19日），[日本](../Page/日本.md "wikilink")[戰國時代的武將](../Page/戰國時代_\(日本\).md "wikilink")。[甲斐国的戰國大名](../Page/甲斐国.md "wikilink")[武田氏的家臣](../Page/武田氏.md "wikilink")，也是[真田家的始祖](../Page/真田氏.md "wikilink")。有子[真田信綱](../Page/真田信綱.md "wikilink")、[真田昌輝](../Page/真田昌輝.md "wikilink")、[真田昌幸](../Page/真田昌幸.md "wikilink")、[真田信尹及](../Page/真田信尹.md "wikilink")[金井高勝](../Page/金井高勝.md "wikilink")。又名**真田幸綱**，人稱「攻彈正」。正室是[河原隆正之妹](../Page/河原隆正.md "wikilink")[恭雲院](../Page/恭雲院.md "wikilink")。

## 經歷

1513年幸隆出生，是[真田賴昌的次男](../Page/真田賴昌.md "wikilink")，幼名次郎三郎，通稱是源太左衛門，其後成為了真田氏的家督。1541年，[武田信虎聯同](../Page/武田信虎.md "wikilink")[諏訪賴重以及](../Page/諏訪賴重.md "wikilink")[村上義清攻打海野氏](../Page/村上義清.md "wikilink")，協助海野氏的幸隆被聯軍所擊敗，海野氏投降，幸隆逃到[上野國投靠](../Page/上野國.md "wikilink")[箕輪城主](../Page/箕輪城.md "wikilink")[長野業正](../Page/長野業正.md "wikilink")。

幸隆歸順武田氏（歸順時間為1543年或1544年）後不久協助武田家攻打[佐久城](../Page/佐久城.md "wikilink")，接著亦參與不同規模的戰爭。根據《[甲陽軍鑑](../Page/甲陽軍鑑.md "wikilink")》記載，幸隆亦有參與[上田原之戰](../Page/上田原之戰.md "wikilink")，當時身份是[板垣信方的脇備](../Page/板垣信方.md "wikilink")。

1550年幸隆向晴信提出攻打[砥石城](../Page/砥石城.md "wikilink")（又稱戶石城）的請求，晴信親率大軍進攻砥石城，但遭到村上義清的頑強抵抗，大敗而還（[砥石崩](../Page/戶石崩.md "wikilink")）。不過據《高白齋記》記載，翌年1551年5月26日，幸隆運用了智謀僅花一天時間便佔領了砥石城，使村上義清實力大減。1553年當武田軍陷克村上義清的[葛尾城後](../Page/葛尾城.md "wikilink")，義清逃到[越後](../Page/越後國.md "wikilink")，幸隆終於回復舊有的領土。1559年晴信出家剃髮改名信玄，幸隆亦跟隨信玄，法號一德齋。在武田信和上杉軍的五次[川中島之戰當中](../Page/川中島之戰.md "wikilink")，真田幸隆成為了前線的重要人物。第四次川中島之戰亦在陣，嘗試突襲上杉軍的本隊。

1563年9月出兵攻打上杉家的[岩櫃城](../Page/岩櫃城.md "wikilink")，家臣[齋藤憲廣死守](../Page/齋藤憲廣.md "wikilink")。幸隆則勸誘憲廣外甥彌三郎成功，10月攻克岩櫃城，憲廣逃亡。1565年在海野氏及常田氏的協助下，攻佔了[嵩山城](../Page/嵩山城.md "wikilink")。真田氏以岩櫃城作為軍事據點。1566年武田信玄親征[箕輪城](../Page/箕輪城.md "wikilink")，長野氏滅亡，而幸隆因舊主恩惠，收留了部份長野氏的遺臣。

1567年因生病的關係，將家督繼位給長男信綱。由於武田家和[今川家的關係出現斷裂](../Page/今川氏.md "wikilink")，武田信玄開始入侵今川氏統治的駿河地區，幸隆派遣三個兒子出陣。晚年留守吾妻郡，力拒上杉氏的入侵，尤其是白井城，1572年幸隆派遣軍隊成功佔領，1573年乘信玄西征未回歸時，上杉軍攻佔了白井城。1574年5月14日，在[戶石城](../Page/戶石城.md "wikilink")（另說岩櫃城）病逝，享壽62歲。

## 人物

  - 別稱「攻彈正」，擅於攻城，亦擅於說服他人謀反，幸隆亦是武田家三彈正之一。

## 家系

  - 父：[真田賴昌](../Page/真田賴昌.md "wikilink")
  - 母：[海野棟綱之女](../Page/海野棟綱.md "wikilink")
  - 正室：[河原隆正女兒](../Page/河原隆正.md "wikilink")
  - 側室：羽尾幸全女兒
  - 長男：[真田信綱](../Page/真田信綱.md "wikilink")
  - 次男：[真田昌輝](../Page/真田昌輝.md "wikilink")
  - 三男：[真田昌幸](../Page/真田昌幸.md "wikilink")
  - 四男：[真田信尹](../Page/真田信尹.md "wikilink")
  - 五男：[金井高勝](../Page/金井高勝.md "wikilink")

## 參見

  - [真田氏](../Page/真田氏.md "wikilink")

## 參考資料

  - [真田幸隆（真田幸綱）](http://www.rokumonsen.com/source-of-future/sanada-db/s-family/sf-hanshu/a-sanada-reimei-gr/1-s-yorimasa-family/c2-s-yukitaka/index.htm)
  - [武家家傳 真田氏](http://www2.harimaya.com/sengoku/html/sanada_k.html)

[Category:武田二十四將](../Category/武田二十四將.md "wikilink")
[Category:信濃國出身人物](../Category/信濃國出身人物.md "wikilink")
[Yukitaka](../Category/真田氏.md "wikilink")