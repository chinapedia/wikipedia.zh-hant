[Wah_Ming_Shopping_Centre.jpg](https://zh.wikipedia.org/wiki/File:Wah_Ming_Shopping_Centre.jpg "fig:Wah_Ming_Shopping_Centre.jpg")
[Back_of_Wah_Ming_Shopping_Centre.jpg](https://zh.wikipedia.org/wiki/File:Back_of_Wah_Ming_Shopping_Centre.jpg "fig:Back_of_Wah_Ming_Shopping_Centre.jpg")
[Wah_Ming_Market.jpg](https://zh.wikipedia.org/wiki/File:Wah_Ming_Market.jpg "fig:Wah_Ming_Market.jpg")
[Wah_Ming_Estate_Basketball_Court.jpg](https://zh.wikipedia.org/wiki/File:Wah_Ming_Estate_Basketball_Court.jpg "fig:Wah_Ming_Estate_Basketball_Court.jpg")
[Wah_Ming_Estate_football_pitch.jpg](https://zh.wikipedia.org/wiki/File:Wah_Ming_Estate_football_pitch.jpg "fig:Wah_Ming_Estate_football_pitch.jpg")
[Wah_Ming_Estate_Volleyball_Court.jpg](https://zh.wikipedia.org/wiki/File:Wah_Ming_Estate_Volleyball_Court.jpg "fig:Wah_Ming_Estate_Volleyball_Court.jpg")\]\]
[Wah_Ming_Estate_playground.jpg](https://zh.wikipedia.org/wiki/File:Wah_Ming_Estate_playground.jpg "fig:Wah_Ming_Estate_playground.jpg")
[Wah_Ming_Estate_playground_(2).jpg](https://zh.wikipedia.org/wiki/File:Wah_Ming_Estate_playground_\(2\).jpg "fig:Wah_Ming_Estate_playground_(2).jpg")
[Wah_Ming_Estate_fitness_zone.jpg](https://zh.wikipedia.org/wiki/File:Wah_Ming_Estate_fitness_zone.jpg "fig:Wah_Ming_Estate_fitness_zone.jpg")
**華明邨**（）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨](../Page/公共屋邨.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[北區](../Page/北區_\(香港\).md "wikilink")[和合石](../Page/和合石.md "wikilink")、[畫眉山附近](../Page/畫眉山.md "wikilink")，邨名從畫眉山的[諧音而來](../Page/諧音.md "wikilink")。本邨在[香港房屋署](../Page/香港房屋署.md "wikilink")[租者置其屋計劃](../Page/租者置其屋計劃.md "wikilink")（第2期）的其中一個出售的公屋之一，於1999年3月讓居民購買現時租住的單位，現在已成立業主立案法團。

## 屋邨資料

| 樓宇名稱 | 樓宇類型 | 落成年份 |
| ---- | ---- | ---- |
| 禮明樓  | Y3型  | 1990 |
| 信明樓  |      |      |
| 添明樓  |      |      |
| 耀明樓  |      |      |
| 頌明樓  | Y4型  |      |
| 富明樓  |      |      |
| 康明樓  |      |      |

## 歷史

華明邨前身為[九廣鐵路](../Page/九廣鐵路.md "wikilink")[和合石支線](../Page/和合石支線.md "wikilink")（現已拆除）以西、[畫眉山以北](../Page/畫眉山.md "wikilink")、[田心臨時房屋區](../Page/臨時房屋區.md "wikilink")（現已清拆）以南的一些寮屋和農地。1970年代末，政府決定開發[粉嶺／上水新市鎮](../Page/粉嶺／上水新市鎮.md "wikilink")，並率先發展[彩園邨和](../Page/彩園邨.md "wikilink")[祥華邨等地](../Page/祥華邨.md "wikilink")。1980年後，政府開始發展粉嶺南地段，逐步收回[和合石至](../Page/和合石.md "wikilink")[粉嶺站之間的土地](../Page/粉嶺站.md "wikilink")，並拆除[和合石支線的鐵路設施](../Page/和合石支線.md "wikilink")，而該地段第一個發展項目就是由[香港房屋委員會建造的公共屋邨華明邨](../Page/香港房屋委員會.md "wikilink")，名字取自畫眉山「畫眉」二字的諧音，屋邨於1990年開始入伙。

由於華明邨靠近[和合石墳場](../Page/和合石墳場.md "wikilink")，加上屋邨落成時附近地段尚未建設，又與粉嶺市中心的社區和交通設施距離較遠，而當時香港尚有大量其他地方的公共屋邨可供選擇，故華明邨剛落成時的出租情況並不理想，在當時只要任何申請者願意入住華明邨，都會立即獲得編配單位，政府更曾推出優惠政策來吸引租戶。隨着粉嶺南社區於1990年代中後期逐步建設完成，同時香港樓價上升導致更多基層市民申請租住公屋單位，華明邨的入住率也達到一般公共屋邨的水平。

1999年，[香港房屋委員會將華明邨加入](../Page/香港房屋委員會.md "wikilink")[租者置其屋計劃](../Page/租者置其屋計劃.md "wikilink")（第二期），部分單位被出售予當時租住該等單位的居民。

2013年，香港政府推行「[北區區域性巴士路線重組](../Page/北區區域性巴士路線重組.md "wikilink")」計劃，華明邨內的華明總站成為北區巴士交通樞紐，並被冠以「華明轉車站」之名，與上水轉車站一同作為北區區內線與對外長途線之間的轉乘站。

2010年後，香港樓市持續熾熱，中小型單位受到市場追捧，而[北區的住宅單位更因靠近](../Page/北區_\(香港\).md "wikilink")[內地之地利條件而樓價飆升](../Page/中國內地.md "wikilink")。華明邨信明樓一個小型單位於2015年初以呎價11565港元售出，創下香港公屋呎價記錄，成為當時的香港「公屋王」\[1\]；之後華明邨亦屢次刷新記錄\[2\]，甚至曾經超越部分市區大型私人屋苑\[3\]。

## 鄰近屋苑

### [公共屋邨](../Page/公共屋邨.md "wikilink")

  - [華心邨](../Page/華心邨.md "wikilink")

### [居屋](../Page/居屋.md "wikilink")

  - [欣盛苑](../Page/欣盛苑.md "wikilink")
  - [雍盛苑](../Page/雍盛苑.md "wikilink")
  - [昌盛苑](../Page/昌盛苑.md "wikilink")
  - [景盛苑](../Page/景盛苑.md "wikilink")

### [私人屋苑](../Page/私人屋苑.md "wikilink")

  - [花都廣場](../Page/花都廣場.md "wikilink")
  - [牽晴間](../Page/牽晴間.md "wikilink")
  - [碧湖花園](../Page/碧湖花園.md "wikilink")

## 周邊設施

### 幼稚園

  - [保良局莊啟程夫人幼稚園](http://www.kids-club.net/edu/plkmrsvc)（1990年創辦）（位於耀明樓地下）
  - [香海正覺蓮社佛教慧光幼稚園](http://www.bwkk.edu.hk)（1990年創辦）（位於禮明樓地下）
  - [基督徒信望愛堂華明幼稚園](http://www.fhlwmkg.edu.hk)（1990年創辦）（位於添明樓地下）
  - [明愛香港崇德社幼兒學校](http://zcns.caritas.org.hk)（1991年創辦）（位於富明樓地下）

### [小學](../Page/小學.md "wikilink")

  - [鳳溪廖潤琛紀念學校](../Page/鳳溪廖潤琛紀念學校.md "wikilink")
  - [方樹福堂基金方樹泉小學](../Page/方樹福堂基金方樹泉小學.md "wikilink")
  - [基督教粉嶺神召會小學](../Page/基督教粉嶺神召會小學.md "wikilink")
  - [五旬節于良發小學](../Page/五旬節于良發小學.md "wikilink")
  - [香海正覺蓮社佛教正覺蓮社學校](../Page/香海正覺蓮社佛教正覺蓮社學校.md "wikilink")

### [中學](../Page/中學.md "wikilink")

  - [保良局馬錦明中學](../Page/保良局馬錦明中學.md "wikilink")
  - [宣道會陳朱素華紀念中學](../Page/宣道會陳朱素華紀念中學.md "wikilink")
  - [粉嶺救恩書院](../Page/粉嶺救恩書院.md "wikilink")
  - [聖芳濟各書院](../Page/聖芳濟各書院.md "wikilink")
  - [粉嶺官立中學](../Page/粉嶺官立中學.md "wikilink")

### [商場](../Page/商場.md "wikilink")

  - [華明商場](../Page/華明商場.md "wikilink")（連街市）
  - [花都廣場購物商場](../Page/花都廣場.md "wikilink")
  - [雍盛商場（連街市）](../Page/雍盛苑.md "wikilink")
  - [華心商場（連街市）](../Page/華心邨.md "wikilink")

### [郵局](../Page/郵局.md "wikilink")

  - [華明郵政局](../Page/華明郵政局.md "wikilink")

### 康樂設施

  - [和興體育館](../Page/和興體育館.md "wikilink")
  - [百福田心遊樂場](../Page/百福田心遊樂場.md "wikilink")

## 交通

<div class="NavFrame" style="background: yellow; margin: 0 auto; padding: 0 10px">

<div class="NavHead" style="background: yellow; margin: 0 auto; padding: 0 10px">

**交通路線列表**

</div>

<div class="NavContent" style="background: yellow; margin: 0 auto; padding: 0 10px">

  - [華明巴士總站](../Page/華明巴士總站.md "wikilink")

<!-- end list -->

  - [華明路](../Page/華明路.md "wikilink")

<!-- end list -->

  - [百和路](../Page/百和路.md "wikilink")

<!-- end list -->

  - [一鳴路](../Page/一鳴路.md "wikilink")

<!-- end list -->

  - [偉明街](../Page/偉明街.md "wikilink")

<!-- end list -->

  - 雍盛苑專線小巴總站

</div>

</div>

## 外部連結

  - [房屋署華明邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2774)

[Category:粉嶺](../Category/粉嶺.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  [香港經濟日報2015年1月30日：粉嶺華明邨呎價11565
    全港公屋王](http://ps.hket.com/content/532762/)
2.  [香港經濟日報2015年5月26日：粉嶺華明呎價1.2萬
    新界公屋王](http://ps.hket.com/content/607172/)
3.  [東方日報2015年7月14日：粉嶺公屋華明邨呎租42元
    貴過太古城](http://hk.on.cc/hk/bkn/cnt/finance/20160714/bkn-20160714214711658-0714_00842_001.html)