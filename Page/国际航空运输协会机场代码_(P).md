| 代碼       | 機場                                                           | 城市                                          | 国家和地区                                                                |
| -------- | ------------------------------------------------------------ | ------------------------------------------- | -------------------------------------------------------------------- |
| PEK\[1\] | [北京首都国际机场](../Page/北京首都国际机场.md "wikilink")                   | [北京市](../Page/北京市.md "wikilink")            | [中国](../Page/中国.md "wikilink")                                       |
| PKX      | [北京大兴国际机场](../Page/北京大兴国际机场.md "wikilink")                   | 北京市                                         | 中国                                                                   |
| PIK      | [格拉斯哥普雷斯蒂克国际机场](../Page/格拉斯哥普雷斯蒂克国际机场.md "wikilink")         | [格拉斯哥](../Page/格拉斯哥.md "wikilink")          | [英国](../Page/英国.md "wikilink")                                       |
| PVG      | [上海浦东国际机场](../Page/上海浦东国际机场.md "wikilink")                   | [上海市](../Page/上海市.md "wikilink")            | [中国](../Page/中国.md "wikilink")                                       |
| PEN      | [檳城國際機場](../Page/檳城國際機場.md "wikilink")                       | 檳城                                          | [馬來西亞](../Page/馬來西亞.md "wikilink")                                   |
| PAD      | Paderborn                                                    | Paderborn                                   | [德國](../Page/德國.md "wikilink")                                       |
| PAH      |                                                              | Paducah                                     | [美國](../Page/美國.md "wikilink")                                       |
| PAO      |                                                              | Palo Alto                                   | [美國](../Page/美國.md "wikilink")                                       |
| PAP      | Mais Gate                                                    | 太子港                                         | [海地](../Page/海地.md "wikilink")                                       |
| PAS      | Paros Community                                              | Paros                                       | [希臘](../Page/希臘.md "wikilink")                                       |
| PAT      | Patna                                                        | Patna                                       | [印度](../Page/印度.md "wikilink")                                       |
| PAZ      |                                                              | Poza Rica                                   | [墨西哥](../Page/墨西哥.md "wikilink")                                     |
| PBC      |                                                              | Puebla                                      | [墨西哥](../Page/墨西哥.md "wikilink")                                     |
| PBI      | [棕櫚灘國際機場](../Page/棕櫚灘國際機場.md "wikilink")                     | [佛羅里達州](../Page/佛羅里達州.md "wikilink")        | [美國](../Page/美國.md "wikilink")                                       |
| PBM      | Zanderij Intl                                                | Paramaribo                                  | [蘇利南](../Page/蘇利南.md "wikilink")                                     |
| PBO      | Paraburdoo                                                   | Paraburdoo                                  | [澳大利亞](../Page/澳大利亞.md "wikilink")                                   |
| PCL      | Captain Rolden                                               | Pucallpa                                    | [秘魯](../Page/秘魯.md "wikilink")                                       |
| PDG      | Tabing                                                       | Padang                                      | [印度尼西亞](../Page/印度尼西亞.md "wikilink")                                 |
| PDL      | Nordela                                                      | Ponta Delgada                               | [葡萄牙](../Page/葡萄牙.md "wikilink")                                     |
| PDT      |                                                              | Pendleton                                   | [美國](../Page/美國.md "wikilink")                                       |
| PDX      | Portland International Airport                               | 波特蘭                                         | [美國](../Page/美國.md "wikilink")                                       |
| PEE      | Perm                                                         | Perm                                        | [俄羅斯](../Page/俄羅斯.md "wikilink")                                     |
| PEG      | Na                                                           | 佩魯賈                                         | [義大利](../Page/義大利.md "wikilink")                                     |
| PEI      | Matecana                                                     | Pereira                                     | [哥倫比亞](../Page/哥倫比亞.md "wikilink")                                   |
| PER      | Perth                                                        | 珀斯                                          | [澳大利亞](../Page/澳大利亞.md "wikilink")                                   |
| PES      | Petrozavodsk Airport                                         | Petrozavodsk                                | [俄羅斯](../Page/俄羅斯.md "wikilink")                                     |
| PEW      | Peshawar                                                     | Peshawar                                    | [巴基斯坦](../Page/巴基斯坦.md "wikilink")                                   |
| PFN      |                                                              | [巴拿馬市](../Page/巴拿馬市.md "wikilink")          | [美國](../Page/美國.md "wikilink")                                       |
| PFO      | International                                                | Paphos                                      | [賽普勒斯](../Page/賽普勒斯.md "wikilink")                                   |
| PGA      |                                                              | [佩吉](../Page/佩吉_\(亞利桑那州\).md "wikilink")    | [美國](../Page/美國.md "wikilink")                                       |
| PGF      | Llabanere                                                    | Perpignan                                   | [法國](../Page/法國.md "wikilink")                                       |
| PGX      |                                                              | Perigueux                                   | [法國](../Page/法國.md "wikilink")                                       |
| PHE      | Port Hedlan                                                  | Port Hedland                                | [澳大利亞](../Page/澳大利亞.md "wikilink")                                   |
| PHL      | [费城国际机场](../Page/費城國際機場.md "wikilink")                       | 費城                                          | [美國](../Page/美國.md "wikilink")                                       |
| PHO      | Point Hope                                                   | Point Hope                                  | [美國](../Page/美國.md "wikilink")                                       |
| PHS      |                                                              | Phitsanulok                                 | [泰國](../Page/泰國.md "wikilink")                                       |
| PHX      | [鳳凰城天港國際機場](../Page/鳳凰城天港國際機場.md "wikilink")                 | [鳳凰城](../Page/鳳凰城.md "wikilink")            | [美國](../Page/美國.md "wikilink")                                       |
| PIA      | Greater Peoria Airport                                       | Peoria                                      | [美國](../Page/美國.md "wikilink")                                       |
| PIH      |                                                              | Pocatello                                   | [美國](../Page/美國.md "wikilink")                                       |
| PIR      | Pierre Municipal Airport                                     | Pierre                                      | [美國](../Page/美國.md "wikilink")                                       |
| PIT      | [匹兹堡国际机场](../Page/匹兹堡国际机场.md "wikilink")                     | [匹茲堡](../Page/匹兹堡.md "wikilink")            | [美國](../Page/美國.md "wikilink")                                       |
| PIW      |                                                              | Pikwitonei                                  | [加拿大](../Page/加拿大.md "wikilink")                                     |
| PJG      |                                                              | Panjgur                                     | [巴基斯坦](../Page/巴基斯坦.md "wikilink")                                   |
| PKB      | Wood County                                                  | Parkersburg / Marietta                      | [美國](../Page/美國.md "wikilink")                                       |
| PKC      | Petropavlovsk                                                | Petropavlovsk-Kamchatsky                    | [俄羅斯](../Page/俄羅斯.md "wikilink")                                     |
| PKE      | Parkes                                                       | Parkes                                      | [澳大利亞](../Page/澳大利亞.md "wikilink")                                   |
| PKR      | Pokhara                                                      | Pokhara                                     | [尼泊爾](../Page/尼泊爾.md "wikilink")                                     |
| PKU      | Simpang Tiga                                                 | Pekanbaru                                   | [印度尼西亞](../Page/印度尼西亞.md "wikilink")                                 |
| PLB      |                                                              | Plattsburgh                                 | [美國](../Page/美國.md "wikilink")                                       |
| PLH      | Plymouth Airport                                             | [普利茅斯](../Page/普利茅斯.md "wikilink")          | [英國](../Page/英國.md "wikilink")                                       |
| PLM      | Sultan Mahmud Badaruddin Ii                                  | Palembang                                   | [印度尼西亞](../Page/印度尼西亞.md "wikilink")                                 |
| PLN      | Pellston Regional Airport                                    | Pellston                                    | [美國](../Page/美國.md "wikilink")                                       |
| PLO      | Port Lincoln                                                 | Port Lincoln                                | [澳大利亞](../Page/澳大利亞.md "wikilink")                                   |
| PLS      |                                                              | Providenciales                              | [TurksAndCaicosIslands](../Page/TurksAndCaicosIslands.md "wikilink") |
| PLW      | Mutiara                                                      | Palu                                        | [印度尼西亞](../Page/印度尼西亞.md "wikilink")                                 |
| PLZ      | Hf Verwoerd                                                  | [伊麗沙伯港](../Page/伊麗沙伯港.md "wikilink")        | [南非](../Page/南非.md "wikilink")                                       |
| PMC      | Tepual                                                       | Puerto Montt                                | [智利](../Page/智利.md "wikilink")                                       |
| PMD      | Air Force 42                                                 | Palmdale                                    | [美國](../Page/美國.md "wikilink")                                       |
| PMI      | Palma Mallorca                                               | Palma Mallorca                              | [西班牙](../Page/西班牙.md "wikilink")                                     |
| PMO      | Punta Raisi                                                  | 巴勒墨                                         | [義大利](../Page/義大利.md "wikilink")                                     |
| PMR      | Palmerstown North                                            | Palmerston North                            | [新西蘭](../Page/新西蘭.md "wikilink")                                     |
| PMV      | Gral Santiago Marino                                         | 波拉馬爾                                        | [委內瑞拉](../Page/委內瑞拉.md "wikilink")                                   |
| PNA      | Pamplona                                                     | Pamplona                                    | [西班牙](../Page/西班牙.md "wikilink")                                     |
| PNC      |                                                              | Ponca City                                  | [美國](../Page/美國.md "wikilink")                                       |
| PNH      | Pochentong                                                   | [金邊](../Page/金邊.md "wikilink")              | [柬埔寨](../Page/柬埔寨.md "wikilink")                                     |
| PNI      | Pohnpei International                                        | Pohnpei                                     | [密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")                               |
| PNK      | Supadio                                                      | Pontianak                                   | [印度尼西亞](../Page/印度尼西亞.md "wikilink")                                 |
| PNL      | Pantelleria                                                  | Pantelleria                                 | [義大利](../Page/義大利.md "wikilink")                                     |
| PNQ      | Lohegaon Poona                                               | Poona                                       | [印度](../Page/印度.md "wikilink")                                       |
| PNR      | Pointe Noire                                                 | Pointe Noire                                | [剛果](../Page/剛果.md "wikilink")                                       |
| PNS      |                                                              | 彭薩科拉                                        | [美國](../Page/美國.md "wikilink")                                       |
| POA      | Salgado Filho                                                | Porto Alegre                                | [巴西](../Page/巴西.md "wikilink")                                       |
| POG      | Port Gentil                                                  | Port Gentil                                 | [加蓬](../Page/加蓬.md "wikilink")                                       |
| POM      | Jackson                                                      | Port Moresby                                | [巴布亞新磯內亞](../Page/巴布亞新磯內亞.md "wikilink")                             |
| POP      | La Union                                                     | Puerto Plata                                | [多明尼加](../Page/多明尼加.md "wikilink")                                   |
| POR      | Pori                                                         | Pori                                        | [芬蘭](../Page/芬蘭.md "wikilink")                                       |
| POS      | Piarco International Airport                                 | 西班牙港                                        | [特立尼達和多巴哥](../Page/特立尼達和多巴哥.md "wikilink")                           |
| POU      | Dutchess County                                              | [波啟浦夕](../Page/波啟浦夕_\(紐約州\).md "wikilink")  | [美國](../Page/美國.md "wikilink")                                       |
| POZ      | Lawica                                                       | Poznan                                      | [Poland](../Page/Poland.md "wikilink")                               |
| PPG      | International                                                | Pago Pago                                   | [美屬薩摩亞](../Page/美屬薩摩亞.md "wikilink")                                 |
| PPS      | Puerto Princesa                                              | Puerto Princesa                             | [菲律賓](../Page/菲律賓.md "wikilink")                                     |
| PPT      | Intl Tahiti                                                  | Papeete                                     | [法屬玻利尼西亞](../Page/法屬玻利尼西亞.md "wikilink")                             |
| PQI      |                                                              | Presque Isle                                | [美國](../Page/美國.md "wikilink")                                       |
| PQQ      | Port Macquarie                                               | Port Macquarie                              | [澳大利亞](../Page/澳大利亞.md "wikilink")                                   |
| PRC      |                                                              | Prescott                                    | [美國](../Page/美國.md "wikilink")                                       |
| PRG      | Ruzyne                                                       | 布拉格                                         | [捷克](../Page/捷克.md "wikilink")                                       |
| PRI      |                                                              | Praslin Island                              | [Seychelles](../Page/Seychelles.md "wikilink")                       |
| PSA      | G Galilei                                                    | Pisa                                        | [義大利](../Page/義大利.md "wikilink")                                     |
| PSE      | Mercedita                                                    | Ponce                                       | [美國](../Page/美國.md "wikilink")                                       |
| PSG      | Municipal                                                    | 彼得堡                                         | [美國](../Page/美國.md "wikilink")                                       |
| PSM      | Pease Intl Tradeport                                         | 朴茨茅斯                                        | [美國](../Page/美國.md "wikilink")                                       |
| PSO      | Cano                                                         | Pasto                                       | [哥倫比亞](../Page/哥倫比亞.md "wikilink")                                   |
| PSP      | [棕榈泉国际机场](../Page/棕櫚泉國際機場.md "wikilink")                     | [棕櫚泉](../Page/棕櫚泉_\(加利福尼亚州\).md "wikilink") | [美國](../Page/美國.md "wikilink")                                       |
| PSR      | Liberi                                                       | 佩斯卡拉                                        | [義大利](../Page/義大利.md "wikilink")                                     |
| PSZ      |                                                              | Puerto Suarez                               | [玻利維亞](../Page/玻利維亞.md "wikilink")                                   |
| PTG      |                                                              | Pietersburg                                 | [南非](../Page/南非.md "wikilink")                                       |
| PTP      | Le Raizet                                                    | Pointe A Pitre                              | [Guadeloupe](../Page/Guadeloupe.md "wikilink")                       |
| PTY      | Tocumen International Airport                                | 巴拿馬市                                        | [巴拿馬](../Page/巴拿馬.md "wikilink")                                     |
| PUB      | Pueblo Memorial Airport                                      | Pueblo                                      | [美國](../Page/美國.md "wikilink")                                       |
| PUJ      |                                                              | Punta Cana                                  | [多明尼加](../Page/多明尼加.md "wikilink")                                   |
| PUQ      | Presidente Ibanez                                            | Punta Arenas                                | [智利](../Page/智利.md "wikilink")                                       |
| PUS      | [金海國際機場](../Page/金海國際機場.md "wikilink")                       | [釜山](../Page/釜山.md "wikilink")              | [南韓](../Page/南韓.md "wikilink")                                       |
| PUW      | Pullman                                                      | Pullman                                     | [美國](../Page/美國.md "wikilink")                                       |
| PUZ      | Puerto Cabezas                                               | Puerto Cabezas                              | [尼加拉瓜](../Page/尼加拉瓜.md "wikilink")                                   |
| PVC      | Provincetown Municipal Airport                               | Provincetown                                | [美國](../Page/美國.md "wikilink")                                       |
| PVD      | [西奧多·弗朗西斯·格林紀念州立機場](../Page/西奧多·弗朗西斯·格林紀念州立機場.md "wikilink") | 普羅維登斯                                       | [美國](../Page/美國.md "wikilink")                                       |
| PVR      | Gustavo Diaz Ordaz                                           | Puerto Vallarta                             | [墨西哥](../Page/墨西哥.md "wikilink")                                     |
| PVU      | Provo                                                        | Provo                                       | [美國](../Page/美國.md "wikilink")                                       |
| PWM      | Portland International Jetport                               | 波特蘭                                         | [美國](../Page/美國.md "wikilink")                                       |
| PXM      |                                                              | Puerto Escondido                            | [墨西哥](../Page/墨西哥.md "wikilink")                                     |
| PXO      | Porto Santo                                                  | Porto Santo                                 | [葡萄牙](../Page/葡萄牙.md "wikilink")                                     |
| PZE      | Penzance                                                     | Penzance                                    | [英國](../Page/英國.md "wikilink")                                       |
| PZO      | Puerto Ordaz                                                 | Puerto Ordaz                                | [委內瑞拉](../Page/委內瑞拉.md "wikilink")                                   |

## 注釋

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")

1.  IATA代碼使用北京於民國時期的[邮政式拼音Peking](../Page/郵政式拼音.md "wikilink")，而非現今[漢語拼音譯名Beijing](../Page/漢語拼音.md "wikilink")