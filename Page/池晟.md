**池晟**\[1\]（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/演員.md "wikilink")。本名**郭太根**，曾於2011年8月[SBS綜藝節目](../Page/SBS株式會社.md "wikilink")《[Healing
Camp](../Page/Healing_Camp.md "wikilink")》正名漢字為**池城**，因[五行缺土取](../Page/五行.md "wikilink")「地」字為藝名，也有「至誠感天」的意涵在內\[2\]。2015年4月經紀公司公布**池城**為錯誤譯名，以**池晟**為準\[3\]。

## 經歷

曾是運動健將且立志成為棒球選手，但父母希望他能把更多精力投入到學習中而反對他打棒球，進入高中後便不再打棒球。

偶然看了電影《[雨人](../Page/雨人.md "wikilink")》[達斯汀·霍夫曼所扮演的自閉症患者](../Page/達斯汀·霍夫曼.md "wikilink")，因為這催人淚下的表演使他萌生當演員的想法。而他的父母（父親是校長，母親是老師，在池晟20歲時離婚）反對兒子進入演藝界，後來進入了大學念哲學系。但他始終沒有丟棄對表演的夢想，決定要休學，將自己的資料寄到[SBS電視台](../Page/SBS_\(韓國\).md "wikilink")，所幸通過電視劇《[KAIST](../Page/KAIST_\(電視劇\).md "wikilink")》試鏡，並且開始了表演生涯。

進入演藝界之後，以清秀的外貌、斯文的氣質吸引了越來越多影迷，2003年的《[All
In](../Page/All_In_真愛賭注.md "wikilink")》使他的演技得到認可。因2004年末的《[最後的舞請與我一起](../Page/最後的舞請與我一起.md "wikilink")》在亞洲各地人氣高升。

2005年6月7日入伍，2007年6月6日於首爾龍山區梨泰院國防部退伍。退伍後首部作品《[New
Heart](../Page/赤子之心.md "wikilink")》扮演外科醫生。

2015年出演《[Kill Me Heal
Me](../Page/Kill_Me_Heal_Me.md "wikilink")》，藉由飾演女高中生角色安耀娜獲得高人氣，得到了第一位使女性化妝品售罄的男性演員的名號。

## 感情生活

2003年，池晟與[朴帥眉合作電視劇](../Page/朴帥眉.md "wikilink")《[All
In](../Page/All_In_真愛賭注.md "wikilink")》之後發展為戀人，2006年宣告分手。

2007年，與[李寶英交往](../Page/李寶英.md "wikilink")，是在2004年合作電視劇《[最後的舞請與我一起](../Page/最後的舞請與我一起.md "wikilink")》結識，於2013年9月27日結婚。

2014年11月24日，證實李寶英已懷胎10周。

2015年6月13日，李寶英順利產下女兒，池晟正式成為人父。

2015年9月6日，節目《[Section TV
演藝通信](../Page/Section_TV_演藝通信.md "wikilink")》中池晟公開愛女名為郭智柔（，音譯）。

2019年2月5日，李寶英順利產下一子。

## 演出作品

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
<td><p>電視台</p></td>
<td><p>劇名</p></td>
<td><p>角色</p></td>
<td><p>性質</p></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p><a href="../Page/SBS株式會社.md" title="wikilink">SBS</a></p></td>
<td><p><a href="../Page/KAIST_(電視劇).md" title="wikilink">KAIST</a></p></td>
<td><p>姜戴旭</p></td>
<td><p>閒角</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td></td>
<td><p>李治秀</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td></td>
<td><p>張錫鎮</p></td>
<td><p>第一男主角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/文化廣播_(韓國).md" title="wikilink">MBC</a></p></td>
<td></td>
<td><p>黃元修</p></td>
<td><p>第二男主角</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美味關係_(韓國電視劇).md" title="wikilink">美味關係</a></p></td>
<td><p>吳俊秀</p></td>
<td><p>男配角</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/韓國放送公社.md" title="wikilink">KBS</a></p></td>
<td></td>
<td><p>李昇俊</p></td>
<td><p>第二男主角</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p>SBS</p></td>
<td><p><a href="../Page/All_In_真愛賭注.md" title="wikilink">All In 真愛賭注</a></p></td>
<td><p>崔正元</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/光海君.md" title="wikilink">光海君</a></p></td>
<td><p>第一男主角</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>KBS</p></td>
<td><p><a href="../Page/愛情的條件.md" title="wikilink">愛情的條件</a></p></td>
<td><p>盧潤澤</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SBS</p></td>
<td><p><a href="../Page/最後的舞請與我一起.md" title="wikilink">最後的舞請與我一起</a></p></td>
<td><p>姜玄宇</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p>MBC</p></td>
<td></td>
<td><p>金友振</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/赤子之心.md" title="wikilink">New Heart 嶄新的心</a></p></td>
<td><p>李恩成</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p>SBS</p></td>
<td><p><a href="../Page/吞噬太陽.md" title="wikilink">吞噬太陽</a></p></td>
<td><p>金政宇</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p>MBC</p></td>
<td><p><a href="../Page/金首露_(電視劇).md" title="wikilink">金首露</a></p></td>
<td><p><a href="../Page/金首露.md" title="wikilink">金首露</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/Royal_Family.md" title="wikilink">Royal Family</a></p></td>
<td><p>韓志勳</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>SBS</p></td>
<td><p><a href="../Page/守護老闆.md" title="wikilink">守護老闆</a></p></td>
<td><p>車智憲</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/大風水.md" title="wikilink">大風水</a></p></td>
<td><p>睦池尚</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p>KBS</p></td>
<td><p><a href="../Page/秘密_(2013年電視劇).md" title="wikilink">秘密</a></p></td>
<td><p>趙敏赫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p>MBC</p></td>
<td><p><a href="../Page/Kill_Me_Heal_Me.md" title="wikilink">Kill Me Heal Me</a></p></td>
<td><p>車道賢</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p>SBS</p></td>
<td><p><a href="../Page/戲子_(電視劇).md" title="wikilink">戲子</a></p></td>
<td><p>申錫浩</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017</p></td>
<td><p><a href="../Page/被告人_(韓國電視劇).md" title="wikilink">被告人</a></p></td>
<td><p>朴政宇</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p><a href="../Page/TVN.md" title="wikilink">tvN</a></p></td>
<td><p><a href="../Page/認識的妻子.md" title="wikilink">認識的妻子</a></p></td>
<td><p>車柱赫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019</p></td>
<td><p>SBS</p></td>
<td><p><a href="../Page/Doctor_Room.md" title="wikilink">Doctor Room</a></p></td>
<td><p>車耀漢</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

<table>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
<td><p>名稱</p></td>
<td><p>角色</p></td>
<td><p>備註</p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td></td>
<td><p>俊浩</p></td>
<td><p>主演</p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/新暗行御史#电影动画.md" title="wikilink">新暗行御史</a></p></td>
<td></td>
<td><p>結尾旁白</p></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td></td>
<td><p>杜浩</p></td>
<td><p>第二男主角</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td></td>
<td><p>朴榮煥</p></td>
<td><p>客串</p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/我的PS搭檔.md" title="wikilink">我的PS搭檔</a></p></td>
<td><p>李賢勝</p></td>
<td><p>主演</p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td></td>
<td><p>任賢泰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p>Color of Asia Master－我的鄰居是吸血鬼</p></td>
<td><p>韓昌浩</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018</p></td>
<td><p>|</p></td>
<td><p><a href="../Page/興宣大院君.md" title="wikilink">興宣大院君</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### MV

|      |                                |                   |                                                                   |                                           |
| ---- | ------------------------------ | ----------------- | ----------------------------------------------------------------- | ----------------------------------------- |
| 年份   | 演唱者                            | 曲名                | 合作演員                                                              | 備註                                        |
| 2000 | 李剛新                            | 拋棄我               |                                                                   |                                           |
| 2002 | 李基燦                            | 感冒                | [蘇怡賢](../Page/蘇怡賢.md "wikilink")、[朴恩玭](../Page/朴恩玭.md "wikilink") | [潘瑋柏](../Page/潘瑋柏.md "wikilink")《我們都會錯》原曲 |
| 2004 | KCM                            | 黑白寫真              | [河智苑](../Page/河智苑.md "wikilink")                                  |                                           |
| 2008 | 金範洙                            | 悲傷活用法             |                                                                   |                                           |
| 2009 | [輝星](../Page/輝星.md "wikilink") | 淚嘩嘩               | [朴彩京](../Page/朴彩京.md "wikilink")                                  |                                           |
| 2009 |                                | So I'm Loving You |                                                                   | Lotte Duty Free廣告曲                        |

## 綜藝或訪談節目

  - 2005年：SBS《夜心萬萬》
  - 2009年：Estar名人專訪
  - 2011年7月31日：SBS《[Running
    Man](../Page/Running_Man.md "wikilink")》EP54（與[崔江姬](../Page/崔江姬.md "wikilink")
    宣傳《守護BOSS》）
  - 2011年8月1日：SBS《[Healing Camp](../Page/Healing_Camp.md "wikilink")》EP3
  - 2011年12月22日：tvN《[現場脫口秀 Taxi](../Page/現場脫口秀_Taxi.md "wikilink")》EP223
  - 2012年：日本紀實類日常池晟的《Real Life》
  - 2012年10月21日.28日：SBS《Running
    Man》EP116、117（與[宋昶儀](../Page/宋昶儀.md "wikilink")、[池珍熙宣傳](../Page/池珍熙.md "wikilink")《大風水》）
  - 2012年10月23.30日：SBS《[強心臟](../Page/強心臟.md "wikilink")》EP151、152（與[金素妍](../Page/金素妍.md "wikilink")、[李允芝宣傳](../Page/李允芝.md "wikilink")《大風水》）
  - 2012年12月6日：KBS《[Happy
    Together](../Page/歡樂在一起.md "wikilink")》EP276（《我的P.S夥伴》劇組）
  - 2013年9月19日：KBS《Happy Together》EP317（《秘密》劇組）
  - 2014年7月1日：tvN《現場脫口秀 Taxi》EP336（與[李光洙](../Page/李光洙.md "wikilink")）
  - 2014年6月29日.7月6日：SBS《Running Man》EP202、203（宣傳電影《好朋友們》）
  - 2015年6月5日.12日：tvN《[一日三餐](../Page/一日三餐.md "wikilink")》農村篇第二季EP4、5
  - 2015年9月6日：MBC《[Section TV
    演藝通信](../Page/Section_TV_演藝通信.md "wikilink")》
  - 2016年2月21日：MBC《Section TV 演藝通信》

## 節目主持

  - 2001-2002年：MBC《》
  - 2004年：KBS《[Music Bank](../Page/KBS音樂銀行.md "wikilink")》
  - 2011年：SBS《[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")》

## 音樂作品

  - 2004年：電視劇《[愛情的條件](../Page/愛情的條件.md "wikilink")》的插曲－《告白》
  - 2005年：電視劇《驛動的心 之 外出》的插曲－《燕子花》 　　
  - 2008年：電視劇《[New
    Heart](../Page/赤子之心.md "wikilink")》的插曲－《我比任何人都愛你》（與南惠汐通電話時）、《星星凋零》
    　　
  - 2008年：Innisfree化妝品第一季廣告曲（與 [文瑾瑩](../Page/文瑾瑩.md "wikilink")）　　
  - 2008年：日本影友會 演繹日文歌曲 　　
  - 2009年：日本影友會 與金范秀合唱《鵝之夢》 　　
  - 2009年：Lotte Duty Free廣告曲－《So I'm Loving You》
  - 2012年：電影《色即是空0204愛你呦》OST－《Sexy Jingle
    Bells》（與[金亞中](../Page/金亞中.md "wikilink")）、《Show Me
    Your Panty》
  - 2013年：電視劇《秘密》OST－《呼嘯山莊》
  - 2015年：電視劇《[Kill Me Heal
    Me](../Page/Kill_Me_Heal_Me.md "wikilink")》OST－《紫羅蘭》

## 走秀

  - 2003年：日本安德烈金時裝秀（與[金玟廷](../Page/金玟廷.md "wikilink")） 　　
  - 2005年：日本安德烈金時裝秀（與[金素妍](../Page/金素妍.md "wikilink")）

## 獲獎

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

## 其他經歷

  - 2003年：大韓看護協會宣傳大使
  - 2005-2007年：韓國軍務宣傳大使
  - 2006年：日本媒體譽為韓流"新四大天王" 　　
  - 2006年：韓國環境宣傳大使 　　
  - 2006年：國家軍隊廣播宣傳大使 　　
  - 2006年：軍務弘報大使　 　　
  - 2007年：服兵役期間獲「優秀士兵」的稱號
  - 2008年：韓國胸外科宣傳大使 　　
  - 2008年：韓國粉紅色帶宣傳大使 　
  - 2010年：韓國健康博覽會大使

## 參考資料

## 外部連結

  - [NAVER](http://people.search.naver.com/search.naver?where=nexearch&query=%EC%A7%80%EC%84%B1&sm=tab_txc&ie=utf8&key=PeopleService&os=95036)


  - [池晟me2day](http://me2day.net/actorjisung)

  - [經紀公司Namoo Actors個人網頁](http://www.namooactors.com/star/jisung/)

  -
  -
  -
[J](../Category/韓國電視演員.md "wikilink")
[J](../Category/韓國電影演員.md "wikilink")
[J](../Category/韓國男演員.md "wikilink")
[J](../Category/韓國新教徒.md "wikilink")
[J](../Category/漢陽大學校友.md "wikilink")
[J](../Category/全羅南道出身人物.md "wikilink")
[J](../Category/順天市出身人物.md "wikilink")
[J](../Category/郭姓.md "wikilink")

1.

2.

3.