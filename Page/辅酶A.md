**辅酶A**（，簡稱、或）是一種[輔酶](../Page/輔酶.md "wikilink")，值得注意的是其在合成和氧化[脂肪酸的角色](../Page/脂肪酸.md "wikilink")，和在[三羧酸循環中氧化](../Page/三羧酸循環.md "wikilink")[丙酮酸](../Page/丙酮酸.md "wikilink")。所有基因組測序日期編碼的酶，即利用輔酶A作為[底物](../Page/底物.md "wikilink")，並在4％左右的細胞酶中使用（或[硫酯](../Page/硫酯.md "wikilink")，例如[乙酰-CoA](../Page/乙酰-CoA.md "wikilink")）作為基材。在人類中，輔酶A生物合成需要[半胱氨酸](../Page/半胱氨酸.md "wikilink")、[泛酸和](../Page/泛酸.md "wikilink")[三磷酸腺苷](../Page/三磷酸腺苷.md "wikilink")（ATP）。主要參與脂肪酸以及丙酮酸的代謝。

## 參考文獻

  - Karl Miller (1998). [Beta Oxidation of Fatty
    Acids](https://web.archive.org/web/20100619135847/http://www.gwu.edu/~mpb/betaox.htm).
    Retrieved May 18, 2005.
  - Charles Ophard (2003). [Acetyl-CoA
    Crossroads](https://web.archive.org/web/20071221033717/http://www.elmhurst.edu/~chm/vchembook/623acetylCoAfate.html).
    Retrieved May 18, 2005.
  - Lehninger: Principles of Biochemistry, 4th edition, David L. Nelson,
    Michael M. Cox
  - <http://www.elmhurst.edu/~chm/vchembook/621fattyacidrx.html>

[Category:輔酶](../Category/輔酶.md "wikilink")
[Category:代谢](../Category/代谢.md "wikilink")
[Category:硫醇](../Category/硫醇.md "wikilink")