**《g小調鋼琴五重奏》**，**[作品](../Page/作品號.md "wikilink")57**，是[蘇聯作曲家](../Page/蘇聯.md "wikilink")[蕭士達高維契其中一首著名的室內樂作品](../Page/蕭士達高維契.md "wikilink")。本曲正如大多數的鋼琴五重奏，是創作給[鋼琴與](../Page/鋼琴.md "wikilink")[絃樂四重奏演出的作品](../Page/絃樂四重奏.md "wikilink")。作品是為[貝多芬弦樂四重奏所創作](../Page/貝多芬弦樂四重奏.md "wikilink")，於1940年11月23日在莫斯科音樂學院作首演，由作曲家及該團擔任。

## 背景

蕭氏自1940年夏季開始創作此曲，並於同年9月14日完成。經歷過1936年的風波後及藉[第5號交響曲翻身後](../Page/第5號交響曲_\(蕭士塔高維奇\).md "wikilink")，使他在音樂創作上以平衡為主，盡量依從過往作曲家的傳統寫作手法來作曲，而這首曲亦不例外。主題清晰，旋律亦分明，首演時作品得到了不少的好評，可是在官方層面，卻出現了兩種不同的見解。其中有個別保守而作風強硬的蘇共領導，認為蕭氏的音樂雖然特別強調簡單直接的風格，卻同時仍然出現一些矯揉造作的和聲及聲音效果，不免覺得仍然隱藏著抽像的形式主義。\[1\]而政府本身，亦對於室樂這類仍帶有[資產階級化的活動煞有介事](../Page/資產階級.md "wikilink")（但又未敢高調的禁止演奏這類音樂），不過，樂曲在1941年仍獲得了[史達林獎](../Page/史達林獎.md "wikilink")。

## 配器

  - [鍵盤樂器](../Page/鍵盤樂器.md "wikilink")：[鋼琴](../Page/鋼琴.md "wikilink")
  - [絃樂器](../Page/絃樂器.md "wikilink")：第一[小提琴](../Page/小提琴.md "wikilink")、第二小提琴、[中提琴](../Page/中提琴.md "wikilink")、[大提琴](../Page/大提琴.md "wikilink")

## 結構

全曲共分為五個樂章，一般的演奏時間為30分鐘。

  - 第1樂章：[前奏曲](../Page/前奏曲.md "wikilink")，緩板（Lento）
  - 第2樂章：[賦格曲](../Page/賦格曲.md "wikilink")，慢板（Adagio）
  - 第3樂章：[詼諧曲](../Page/詼諧曲.md "wikilink")，稍快板（Allegretto）
  - 第4樂章：[間奏曲](../Page/間奏曲.md "wikilink")，緩板（Lento）
  - 第5樂章：終曲，稍快板（Allegretto）

## 樂譜

  - Shostakovich, Dmitri (2005). Piano Quintet, op. 57. Vol. 99 from
    ''New Collected Works of Dmitri Shostakovich in 150 Volumes".
    Moscow: DSCH Society.

## 注釋

[Category:鋼琴五重奏](../Category/鋼琴五重奏.md "wikilink")
[Category:肖斯塔科維奇作品](../Category/肖斯塔科維奇作品.md "wikilink")

1.  [美國約翰·甘迺迪表演藝術中心於2008年10月5日一次演出中的樂曲介紹。](http://www.kennedy-center.org/calendar/?fuseaction=composition&composition_id=3713)