"**搏擊會**"是英國[科幻](../Page/科幻.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")*[超疑特工](../Page/超疑特工.md "wikilink")*第十一集，於2006年12月24日播出。

## 大綱

食人怪[Weevil在卡迪夫城的街頭肆虐](../Page/:en:Weevil_\(Torchwood\).md "wikilink")，夏積隊長疲於奔命，要求高軍加入追捕行動。高軍正與韋暉共晉晚餐，高軍隱瞞工作性質，韋暉蒙在鼓裡，兩人感情出現危機。高軍堅持跟隊長會合，令韋暉怒不可遏。後來炬木成員在一個貨倉發現一名叫何定欣
(Dan Hodges)的屍體，身上雖有被食人怪襲擊的痕跡，但炬木成員相信是有人利用食人怪借刀殺人。續派奧雲追查，發現一位名叫連邁克 (Mark
Lynch)的商人，相信是幕後操控食人怪的黑手。夏奧雲深入虎穴，大吃一驚，眼前是現實世界不可能的情景；惟連邁克已得悉夏奧雲的真正身份，奧雲如何脫身？

## 演員

  - [Captain Jack
    Harkness](../Page/:en:Jack_Harkness.md "wikilink")—[约翰·巴洛曼](../Page/约翰·巴洛曼.md "wikilink")
  - [Gwen Cooper](../Page/:en:Gwen_Cooper.md "wikilink")—[Eve
    Myles](../Page/:en:Eve_Myles.md "wikilink")
  - [Owen Harper](../Page/:en:Owen_Harper.md "wikilink")—[Burn
    Gorman](../Page/:en:Burn_Gorman.md "wikilink")
  - [Toshiko
    Sato](../Page/:en:Toshiko_Sato.md "wikilink")—[森尚子](../Page/森尚子.md "wikilink")
  - [Ianto Jones](../Page/:en:Ianto_Jones.md "wikilink")—[Gareth
    David-Lloyd](../Page/:en:Gareth_David-Lloyd.md "wikilink")
  - [Rhys
    Williams](../Page/:en:List_of_Torchwood_minor_characters#Rhys_Williams.md "wikilink")—[Kai
    Owen](../Page/:en:Kai_Owen.md "wikilink")
  - Mark Lynch—Alex Hassell
  - [Weevil](../Page/:en:Weevil_\(Torchwood\).md "wikilink")—[Paul
    Kasey](../Page/:en:Paul_Kasey.md "wikilink")
  - Barmaid—Alexandra Dunn
  - Boyfriend—Matthew Raymond
  - Hospital Patient—David Gyasi

## 外部連結

  - ["Combat" episode guide entry on the BBC
    website](http://www.bbc.co.uk/torchwood/epguides.shtml?ep=11)

[Category:火炬木小组](../Category/火炬木小组.md "wikilink")
[Category:火炬木小组剧情](../Category/火炬木小组剧情.md "wikilink")