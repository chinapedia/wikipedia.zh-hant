[Ms.ToSanKu(1954).jpg](https://zh.wikipedia.org/wiki/File:Ms.ToSanKu\(1954\).jpg "fig:Ms.ToSanKu(1954).jpg")及[廣州](../Page/廣州.md "wikilink")[粵劇花旦](../Page/粵劇.md "wikilink")、香港[電影明星](../Page/電影.md "wikilink")：陶三姑（銀老鼠）小姐（1954年）\]\]

**陶三姑**（To Sam
Ku，1895年－1983年3月13日），原名**陶群英**，從前有用過「陶桃艷」及「陶秀貞」作[名字](../Page/名字.md "wikilink")\[1\]，[籍貫](../Page/籍貫.md "wikilink")：[廣東省](../Page/廣東省.md "wikilink")[南海縣](../Page/南海縣.md "wikilink")，[廣州市](../Page/廣州市.md "wikilink")「超群樂」[粵劇戲班班主周錦波](../Page/粵劇.md "wikilink")（細旺）的夫人，育有一子，自20世紀50年代已守寡。早年在[上海及英屬](../Page/上海.md "wikilink")[馬來亞當首席](../Page/馬來亞.md "wikilink")[粵劇花旦](../Page/粵劇.md "wikilink")，藝名為「銀老鼠」。1935年，開始[電影生涯](../Page/電影.md "wikilink")，主要參演[粵語片](../Page/粵語片.md "wikilink")，經常飾演[媒婆](../Page/媒婆.md "wikilink")、[鴇母](../Page/鴇母.md "wikilink")、[包租婆等角色](../Page/包租婆.md "wikilink")。亦常到[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞及](../Page/馬來西亞.md "wikilink")[越南等地演出](../Page/越南.md "wikilink")[粵劇](../Page/粵劇.md "wikilink")（轉當丑角）、[歌劇](../Page/歌劇.md "wikilink")。1970年代息影。1979年8月30日的[報紙報導她會復出](../Page/報紙.md "wikilink")，並首次當[電視藝員](../Page/電視.md "wikilink")，客串[麗的電視的劇集](../Page/麗的電視.md "wikilink")\[2\]\[3\]。

## 榮譽

  - 陶三姑（銀老鼠）在清末民初時代，第一個到[上海演出的坤班](../Page/上海.md "wikilink")（全女班粵劇團）的正印花旦，當時[上海劇評人對她的評語](../Page/上海.md "wikilink")：「稱纖得中，體態輕盈，一翦雙瞳，尤足令觀者魄奪。唱工清脆，歷歷如[出谷黃鶯](../Page/出谷黃鶯.md "wikilink")，表情媚絕。」表示當年的陶三姑（銀老鼠）是位身段豐滿[窈窕](../Page/窈窕.md "wikilink")，眼角送秋波媚惑[觀眾](../Page/觀眾.md "wikilink")，擅演風騷戲的艷旦，唱腔清脆，聲域宏闊的一級[粵劇演員](../Page/粵劇.md "wikilink")\[4\]\[5\]。

<!-- end list -->

  - 陶三姑（銀老鼠）在1950年的[香港製作](../Page/香港.md "wikilink")[電影](../Page/電影.md "wikilink")《珠江淚》裡，飾演：肥婆三，使她獲得[中華人民共和國的](../Page/中華人民共和國.md "wikilink")[文化部榮譽獎金質徽章](../Page/文化部.md "wikilink")\[6\]。

<!-- end list -->

  - 陶三姑（銀老鼠）在1955年12月代表「靳永福藥行」當「工展會小姐」\[7\]。

## 逝世

  - 1983年3月13日（星期日），陶三姑（銀老鼠）因為爆血管在[香港病逝](../Page/香港.md "wikilink")。\[8\]
  - 1983年3月17日（星期四）上午11時在[九龍](../Page/九龍.md "wikilink")[紅磡](../Page/紅磡.md "wikilink")[世界殯儀館出殯](../Page/世界殯儀館.md "wikilink")，遺體葬於[和合石墳場](../Page/和合石墳場.md "wikilink")。

## 參演電影

## 電視劇([無綫電視](../Page/無綫電視.md "wikilink"))

  - 1971年《[年晚錢](../Page/年晚錢.md "wikilink")》
  - 1974年《[群星譜](../Page/群星譜.md "wikilink")》

## 電視劇([香港電台](../Page/香港電台.md "wikilink"))

  - 1972年-1974年《[獅子山下](../Page/獅子山下.md "wikilink")》

## 陶三姑（銀老鼠）的粵曲唱片

  - 《仕林祭塔》，共5張唱片：陶三姑（銀老鼠）、[風情牛合唱](../Page/風情牛.md "wikilink")\[9\]：

## 參考

## 外部連結

  - [香港電影資料館：陶三姑（銀老鼠）參演電影的記錄](http://ipac.hkfa.lcsd.gov.hk/ipac20/ipac.jsp?session=125BX3461576F.6141&profile=hkfa&uri=link=3100036@!123@!3100024@!3100036&menu=search&submenu=basic_search&source=192.168.110.61@!horizon)

  - [陶三姑（銀老鼠）的演出片斷](http://www.youtube.com/watch?v=2eW_XxCZvsE)<sup>[侵犯版權](../Page/侵犯版權.md "wikilink")</sup>

  -
  -
  -
  -
[Category:南海人](../Category/南海人.md "wikilink")
[Category:1895年出生](../Category/1895年出生.md "wikilink")
[Category:1983年逝世](../Category/1983年逝世.md "wikilink")
[Category:粵劇演員](../Category/粵劇演員.md "wikilink")
[Category:香港粵劇演員](../Category/香港粵劇演員.md "wikilink")
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:香港粵語片演員](../Category/香港粵語片演員.md "wikilink")
[Category:前麗的電視藝員](../Category/前麗的電視藝員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Sam](../Category/陶姓.md "wikilink")

1.  1939年6月24日（星期六），[香港](../Page/香港.md "wikilink")《[華字日報](../Page/華字日報.md "wikilink")》，第三張；第三頁：每周電影。
2.  1979年8月30日（星期四），[香港](../Page/香港.md "wikilink")《[工商日報](../Page/工商日報.md "wikilink")》，第十一頁。
3.  2009年8月16日（星期日）
    ，《香港電臺》第一臺的《打開天窗甘國亮》節目裡，節目主持人[甘國亮訪問著名播音員](../Page/甘國亮.md "wikilink")[李我](../Page/李我.md "wikilink")
4.  1924年12月28日（星期日），[上海](../Page/上海.md "wikilink")《[申報](../Page/申報.md "wikilink")》，光磊室主：《粵坤角短評》。
5.  1994年1月，宋鑽友：《粵劇在舊上海的演出》。
6.  1957年4月24日（星期三），[香港](../Page/香港.md "wikilink")《[大公報](../Page/大公報.md "wikilink")》，第六版。
7.  1955年12月5日（星期一），[香港](../Page/香港.md "wikilink")《[大公報](../Page/大公報.md "wikilink")》，第八版。
8.  1983年3月18日（星期五），[香港](../Page/香港.md "wikilink")《[工商日報](../Page/工商日報.md "wikilink")》，第九頁。
9.  [七十八轉粗紋老唱片](http://www.bh2000.net/special/patzak/detail.php?id=833)