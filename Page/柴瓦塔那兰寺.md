[Ayutthaya_Thailand_2004.jpg](https://zh.wikipedia.org/wiki/File:Ayutthaya_Thailand_2004.jpg "fig:Ayutthaya_Thailand_2004.jpg")
**柴瓦塔那兰寺**（）又称**贵妃寺**，是位于[泰国中部](../Page/泰国.md "wikilink")[阿瑜陀耶历史公园的一座](../Page/阿瑜陀耶历史公园.md "wikilink")[佛教](../Page/佛教.md "wikilink")[寺庙](../Page/寺庙.md "wikilink")。寺庙坐落在[昭拍耶河西畔](../Page/昭拍耶河.md "wikilink")，艾尤塔雅岛外。它是公园所在的[大城府最雄伟美丽的寺庙之一](../Page/大城府.md "wikilink")，也是一个旅游胜地。

1630年，[大城王国的](../Page/大城王国.md "wikilink")[巴萨通王](../Page/巴萨通王.md "wikilink")（Prasat
Thong）为了纪念他居住在该地区的母亲而下令开始建造寺庙。设计风格为[高棉风格](../Page/高棉.md "wikilink")，为当时流行的建筑风格。塔群的中央是一座高棉式的大塔，四周有4个小塔，再外围则有8个更小的塔及门。13座高塔的周围围绕着120尊坐佛，8个小塔中另端坐着12尊大佛。

## 参考资料

  - Elizabeth Moore u.a.，《Ancient Capitals of Thailand》，River
    Books/Thames And Hudson, Bangkok 1996, ISBN 0-500-97429-2。
  - Chaiwat Worachetwarawat，《Interesting Temples and Ruins in
    Ayutthaya》，Rajabhat Institute Phra Nakhon Si Ayutthaya,
    Ayutthaya 2001 (oh. ISBN)。
  - 《》，Museum Press, Bangkok 2546 (2003)，ISBN 974-92888-5-8。
  - 《》，(Temples and Palaces in the Old Capital City)，Plan Readers
    Publication，Bangkok 2003，ISBN 974-91126-7-9。

[Category:泰國佛寺](../Category/泰國佛寺.md "wikilink")