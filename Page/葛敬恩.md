**葛敬恩**（）\[1\]，字**湛侯**，[浙江](../Page/浙江.md "wikilink")[嘉兴人](../Page/嘉兴.md "wikilink")。中华民国、中华人民共和国政治人物。

## 生平

### 辛亥革命中

  - 1889年7月30日出生于[浙江](../Page/浙江.md "wikilink")[嘉兴柴场湾一个布商家庭](../Page/嘉兴.md "wikilink")。
  - 1904年，15岁。考入[浙江武备学堂](../Page/浙江武备学堂.md "wikilink")。
  - 1906年，17岁。[光复会](../Page/光复会.md "wikilink")、[同盟会员](../Page/同盟会.md "wikilink")[秋瑾在](../Page/秋瑾.md "wikilink")[杭州运动](../Page/杭州.md "wikilink")[新军人士加入光复会](../Page/新军.md "wikilink")、同盟会。
  - 1907年，18岁。毕业留校任教官。由同学好友[黄郛介绍加入同盟会](../Page/黄郛.md "wikilink")。
  - 1911年，22岁。参加[辛亥革命](../Page/辛亥革命.md "wikilink")。是年10月10日[武昌起义](../Page/武昌起义.md "wikilink")。11月5日，杭州新军起义，葛敬恩参加起义的领导核心，负责起义计划起草和命令的下达，并率领和指挥起义军攻克了杭州羊市街火车站，在车站建立总司令部。嗣后，参加浙二军，任援助南京起义军支队参谋，协助支队长[朱瑞攻克](../Page/朱瑞.md "wikilink")[南京天堡城](../Page/南京.md "wikilink")。攻打南京的胜利，成为辛亥革命武装斗争的典范载入军事史册。南京光复也是辛亥革命胜利的转折点。[孙中山在自传中说](../Page/孙中山.md "wikilink")：“[汉阳一失](../Page/汉阳.md "wikilink")，吾得南京以抵之。革命大局，因此一振。”葛敬恩在杭州新军起义和南京之役中，作为军事参谋，勇敢无畏，计划周密，对取得胜利发挥了重要作用，展示了他的革命精神与军事才能。在杭州一役初识前来助战的[上海敢死队队长](../Page/上海.md "wikilink")[蒋介石](../Page/蒋介石.md "wikilink")，两人彼此都留下了较深的印象。
  - 1912年，23岁。是年1月1日[中华民国临时政府在南京成立](../Page/中华民国临时政府.md "wikilink")。葛敬恩任兵站交通部长。[南北议和葛敬恩作为军界代表赴北京](../Page/南北议和.md "wikilink")。

### 辛亥革命后

  - 1913年，24岁。求学于[北京](../Page/北京.md "wikilink")[陆军大学](../Page/陆军大学.md "wikilink")，与[李济深是先后同学](../Page/李济深.md "wikilink")。学习期间参加了[二次革命及讨袁](../Page/二次革命.md "wikilink")[护国战争](../Page/护国战争.md "wikilink")。
  - 1916年，27岁。陆军大学毕业，任军事教官。
  - 1918年，29岁。被[北京政府选派到](../Page/北京政府.md "wikilink")[日本陆军大学学习](../Page/日本陆军大学.md "wikilink")。
  - 1921年，32岁。[日本陆军大学毕业](../Page/日本.md "wikilink")，回国。但日本陸軍大學名冊查無此人,
  - 1924年，35岁。应[陈仪邀请任浙军第一师参谋长](../Page/陈仪.md "wikilink")，浙军第一师把奉系军阀[张宗昌从浙江赶到陇海路以北](../Page/张宗昌.md "wikilink")。

### 北伐战争中

  - 1926年，37岁。是年[国民革命军](../Page/国民革命军.md "wikilink")[北伐](../Page/北伐.md "wikilink")。在国民革命军总司令[蒋介石坚邀之下](../Page/蒋介石.md "wikilink")，加入国民革命军总司令部任参谋处长，策划北伐战事。
  - 1928年，39岁。是年北伐完成。国民政府定都南京。政府军事机构成立了参谋本部。葛敬恩任参谋次长，陆军中将（李济深任参谋总长）。

### 北伐后

  - 1929年，40岁。任国民革命军编遣委员会总务部副主任，[李济深任主任](../Page/李济深.md "wikilink")。李济深被囚禁后，葛敬恩代理主任职。兼任第一编遣区军务局局长。
  - 1930年，41岁。任[青岛](../Page/青岛.md "wikilink")[特别市市长](../Page/特别市.md "wikilink")。
  - 1931年，42岁。回南京仍任参谋次长。1931年夏赴北方平定[石友三叛乱](../Page/石友三叛乱.md "wikilink")。后至[山西与](../Page/山西.md "wikilink")[阎锡山会谈](../Page/阎锡山.md "wikilink")，使阎锡山重归[国民政府](../Page/国民政府.md "wikilink")。
  - 1932年，43岁。葛敬恩任国民政府航空署长兼[航空学校代理校长](../Page/航空学校.md "wikilink")（校长蒋介石）。

### 赴台湾受降

1945年，56岁。[抗日战争胜利](../Page/抗日战争.md "wikilink")。葛敬恩任[台湾省行政长官公署秘书长兼前进指挥所主任](../Page/台湾省行政长官公署.md "wikilink")。9月在南京出席了国民政府举行的对日受降仪式。10月赴台湾主持接受在台日军投降的仪式。在他第一次对台湾人民演讲时说：「台湾人还没接受真正中华文化之薰陶，是『[二等公民](../Page/二等公民.md "wikilink")』。」

### 二二八事件

1947年[二二八事件爆發之際](../Page/二二八事件.md "wikilink")，日後於1949年投共的臺灣長官公署秘書長葛敬恩、民政處長[周一鶚與時任財政處長](../Page/周一鶚.md "wikilink")[嚴家淦](../Page/嚴家淦.md "wikilink")、工礦處長[包可永等](../Page/包可永.md "wikilink")[陳儀在福建時期舊部被臺灣人團體指責為陳儀周遭貪官污吏之](../Page/陳儀.md "wikilink")「四兇」，導致政治黑暗、米糧外流、人民無穀為炊，被指名要求究辦。\[2\]\[3\]其中葛敬恩其女婿李卓芝亦涉及貪污「劫收」臺灣。\[4\]\[5\]\[6\]\[7\]

根据署名“台湾青年团”所发出的传单，上面写着：“打倒葛敬恩之官僚资本！打倒葛弟之中和公司 打倒葛兄之茶叶公司”\[8\]

根据[中央社的報導](../Page/中央社.md "wikilink")，葛敬恩在台湾的公馆曾被[忠義服務隊投掷](../Page/忠義服務隊.md "wikilink")[手榴弹](../Page/手榴弹.md "wikilink")。\[9\]1947年3月14日，台灣行政長官公署秘書長葛敬恩乘專機飛南京，代表陳儀向蔣介石報告事變經過。\[10\]葛敬恩談話稱，2月28日暴動原因，係日本統治時代遺留之鷹犬與近由海外遺回之台籍浪人受奸徒煽惑。\[11\]

### 返回中國大陸

1947年，58岁。由台湾回到上海。先后任[国民参政会参政员](../Page/国民参政会.md "wikilink")、[制宪国民大会代表](../Page/制宪国民大会.md "wikilink")。民國三十七年（1948年）[第一屆立法委員選舉中被選為](../Page/第一屆立法委員選舉.md "wikilink")[浙江省第一選區](../Page/浙江省.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。

### 投奔中共

  - 1949年5月，60岁。葛敬恩会同上海、[香港等地](../Page/香港.md "wikilink")50余位立法委员通电拥护[中国共产党](../Page/中国共产党.md "wikilink")。[中华人民共和国成立以后](../Page/中华人民共和国.md "wikilink")，葛敬恩历任[民革中央团结委员](../Page/民革.md "wikilink")、上海市政协委员、第四届[全国人大代表](../Page/全国人大.md "wikilink")、第四、第五届[全国政协委员](../Page/全国政协.md "wikilink")。
  - 1979年，90岁。葛敬恩在上海逝世。

## 其他

### 台灣黃金案

台灣旅日作家[黃文雄表示](../Page/黃文雄_\(作家\).md "wikilink")，由於葛敬恩在任行政長官公署秘書長時，向欲留在台灣的日本人收賄、又在接收日本資產時中飽私囊，為了掩蓋，遂將協助善後的末代總督[安藤利吉毒殺滅口](../Page/安藤利吉.md "wikilink")。\[12\]

[蘇新於](../Page/蘇新.md "wikilink")1949年出版的著作《憤怒的台灣》一書中表示：「所謂黃金一百二十公斤，是安藤親手送給葛敬恩的...這些黃金放在原田大佐的家裡時，已被美籍軍人艾文思中校所獲悉，原田害怕事發，把情形報告安藤，安藤再向葛敬恩說知此事，葛敬恩卻泰然自若，對安藤說：『你放心，一切都在鄙人身上』」、「同年八月，葛敬恩突被召赴京...他在老蔣面前，堅定說並沒有此事...他回到了上海，在上海逗留很久，不知所幹何事，不過，奇怪的很，恰巧葛在上海期間，日本戰犯安藤利吉，忽在上海戰犯監獄服毒自殺」\[13\]\[14\]、「關於安藤的自殺，不但在台日本人，就是台灣人也都疑心與『黃金案』有關」\[15\]。蘇新並引述自[台灣總督府主計課長](../Page/台灣總督府.md "wikilink")的說法：「許多日人被拘時，這本清冊正本抄本均被查出、被拿走了，唯一的證據只剩下了安藤一人，在這種情形之下，你想安藤是怎樣死的？一句話，貪污手段之妙、消滅異己之毒，我們是不及國民黨官僚百倍！」\[16\]

另根據學者蘇瑤崇的考證，認為《憤怒的台灣》蘇新所提「侵吞黃金弊案」純係美軍聯絡組艾文思中校侵佔事件，與葛敬恩無關。\[17\]

## 参考文献

注：传略整理者王丽方为葛敬恩长女葛允怡之女，[清华大学建筑学院教授](../Page/清华大学.md "wikilink")。

<div class="references-small">

<references />

</div>

[Category:二二八事件相關人物](../Category/二二八事件相關人物.md "wikilink")
[Category:中華民國陸軍中將](../Category/中華民國陸軍中將.md "wikilink")
[Category:第五屆全國政協委員](../Category/第五屆全國政協委員.md "wikilink")
[Category:第四屆全國政協委員](../Category/第四屆全國政協委員.md "wikilink")
[Category:第四屆全國人大代表](../Category/第四屆全國人大代表.md "wikilink")
[Category:制憲國民大會代表](../Category/制憲國民大會代表.md "wikilink")
[Category:中華民國青島市市長](../Category/中華民國青島市市長.md "wikilink")
[Category:中國國民黨革命委員會黨員](../Category/中國國民黨革命委員會黨員.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:北伐人物](../Category/北伐人物.md "wikilink")
[Category:護國戰爭人物](../Category/護國戰爭人物.md "wikilink")
[Category:二次革命人物](../Category/二次革命人物.md "wikilink")
[Category:辛亥革命人物](../Category/辛亥革命人物.md "wikilink")
[Category:嘉興人](../Category/嘉興人.md "wikilink")
[Jing敬](../Category/葛姓.md "wikilink") [Category:中国国民党第六届中央监察委员会委员
(汪精卫政权)](../Category/中国国民党第六届中央监察委员会委员_\(汪精卫政权\).md "wikilink")

1.  有關《葛敬恩簡歷》的補充 葛允怡 嘉興市政協文史資料委員會編.《嘉興市文史資料通訊》第17輯,1996年04月第1版，第28頁

2.  [許雪姬](../Page/許雪姬.md "wikilink"),
    [1937-1947年在上海的臺灣人](http://www.ntl.edu.tw/public/Attachment/281415523478.pdf),
    《臺灣學研究》第13期（2012年6月），頁24，[國立中央圖書館臺灣分館](../Page/國立中央圖書館臺灣分館.md "wikilink")

3.  [藍博洲](../Page/藍博洲.md "wikilink"),
    [《消逝在二二八迷霧中的王添灯》](https://quasi-quasi.com/category/%E5%A0%B1%E5%B0%8E%E6%96%87%E5%AD%B8/)
    , 2008

4.  王秋森、[陳婉真](../Page/陳婉真.md "wikilink")、李賢群、李堅,
    《1947台灣二二八革命》，[1](http://www.books.com.tw/web/sys_serialtext/?item=0010740959&page=7)、[2](http://www.taaze.tw/sing.html?pid=11100804407),
    2017

5.  [楊碧川](../Page/楊碧川.md "wikilink"),
    [二二八真相](https://www.twreporter.org/a/testimonies-of-228),
    [報導者](../Page/報導者_\(臺灣媒體\).md "wikilink"), 2017/2/19

6.  [魔戒魔戒我愛你](http://www.peoplenews.tw/news/1e19355f-24b1-4d51-a870-494114461279),
    [民報 (2014年)](../Page/民報_\(2014年\).md "wikilink"), 2016-03-22

7.  [陳儀治理台灣
    三大敗筆](http://www.ksnews.com.tw/index.php/news/contents_page/0000965120),
    [更生日報](../Page/更生日報.md "wikilink"), 2017年03月09日

8.  [暴動海報傳單十九件](http://www.archives.gov.tw/NationArc/Detail.aspx?u=38&o=2&ui=9)
    檔案管理局

9.  [件名:武裝暴徒續騷擾　楊監使亦受攻擊](http://catalog.digitalarchives.tw/item/00/29/88/d3.html)
    數位典藏聯合目錄

10. 李新總主編，[中國社會科學院近代史研究所](../Page/中國社會科學院.md "wikilink")[中華民國史研究室編](../Page/中華民國史.md "wikilink")，韓信夫、姜克夫主編：《中華民國史大事記》（全十二卷，共十二冊），[北京](../Page/北京.md "wikilink")：[中華書局](../Page/中華書局.md "wikilink")，2011年7月

11.
12.

13. 蘇新《憤怒的台灣》，香港:智源書局，1949年3月出版，臺灣臺北:時報文化，1993年2月重刊

14. [論戰後（1945-1947）中美共同軍事佔領臺灣的事實與問題](http://www.ith.sinica.edu.tw/quarterly_download.php?name=03蘇瑤崇3校稿0905.pdf)，蘇瑤崇，《臺灣史研究》‧第23卷第3期，中央研究院臺灣史研究所，2016-09

15.
16.
17.