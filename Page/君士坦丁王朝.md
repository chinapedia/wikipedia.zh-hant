**君士坦丁王朝**是在公元四世紀時，屬於同一親族與血源的成員，連續擔任[羅馬帝國](../Page/羅馬帝國.md "wikilink")[皇帝的王朝](../Page/羅馬皇帝.md "wikilink")。該王朝肇始於公元305年，[君士坦提烏斯一世即位為羅馬帝國西方的](../Page/君士坦提烏斯一世.md "wikilink")[奧古斯都](../Page/奧古斯都_\(稱號\).md "wikilink")（君士坦提烏斯·克羅魯斯），其時實行[四帝共治制](../Page/四帝共治制.md "wikilink")，故其並非為統治全羅馬帝國之王朝，直至[君士坦丁一世統一全國](../Page/君士坦丁一世.md "wikilink")，方為惟一之王室，其終於363年，最後一任皇帝為[尤利安二世](../Page/尤利安二世.md "wikilink")。

自君士坦提烏斯一世登基為西方的奧古斯都後，帝國的皇位多為此一家族的成員。除了[李基尼烏斯屬姻親之外](../Page/李基尼烏斯.md "wikilink")，其後的皇帝皆為君士坦提烏斯一世的直系血親。

[Constantine_Dynasty_Zhongwen.png](https://zh.wikipedia.org/wiki/File:Constantine_Dynasty_Zhongwen.png "fig:Constantine_Dynasty_Zhongwen.png")

君士坦丁王朝中，曾經登基為帝（使用「奧古斯都」稱號）的有七人：

  - **[君士坦提烏斯一世](../Page/君士坦提烏斯一世.md "wikilink")**（305年-306年在位）：西方的奧古斯都。
  - **[李基尼烏斯](../Page/李基尼烏斯.md "wikilink")**（308年-324年在位）：東方的奧古斯都，君士坦提烏斯一世的女婿。
  - **[君士坦丁一世](../Page/君士坦丁一世.md "wikilink")**（307年-337年在位）：即君士坦丁大帝。君士坦提烏斯之子，強迫李基尼烏斯遜位。東西羅馬帝國統一。
  - **[君士坦丁二世](../Page/君士坦丁二世.md "wikilink")**（337年-340年在位）：君士坦丁一世之子。父親死後成為三位皇帝之一，統治[高盧](../Page/高盧.md "wikilink")、[不列顛與](../Page/不列顛.md "wikilink")[西班牙地區](../Page/西班牙.md "wikilink")。後被其弟君士坦斯擊敗而亡。

<!-- end list -->

  - **[君士坦提烏斯二世](../Page/君士坦提烏斯二世.md "wikilink")**（337年-361年在位）：君士坦丁一世之子。父親死後成為三位皇帝之一，統治羅馬帝國的東方部分。諸雄彼此征伐，最後君士坦提烏斯擊敗[馬格嫩提烏斯](../Page/馬格嫩提烏斯.md "wikilink")，成了帝國唯一的奧古斯都。

<!-- end list -->

  - **[君士坦斯](../Page/君士坦斯.md "wikilink")**（337年-350年在位）：君士坦丁一世之子。父親死後成為三位皇帝之一，統治[義大利](../Page/義大利.md "wikilink")、[阿非利加與](../Page/阿非利加.md "wikilink")[伊里利孔地區](../Page/伊里利孔.md "wikilink")。後被自立為帝的軍人馬格嫩提烏斯擊敗而亡。

<!-- end list -->

  - **[尤利安二世](../Page/尤利安.md "wikilink")**（361年-363年在位）：君士坦提烏斯一世之孫。繼任其堂兄君士坦提烏斯二世而成為帝國唯一的奧古斯都。後在征伐[波斯過程中陣亡](../Page/薩珊王朝.md "wikilink")。

尤利安二世沒有子嗣，君士坦丁王朝結束。在由[禁衛軍](../Page/禁衛軍.md "wikilink")[約維安短暫的統治之後](../Page/約維安.md "wikilink")，羅馬帝國進入了[瓦倫提尼安王朝](../Page/瓦倫提尼安王朝.md "wikilink")。

## 家族树

## 參考文献

  - [Heinrich Chantraine](../Page/Heinrich_Chantraine.md "wikilink"):
    *Die Nachfolgeordnung Constantins des Großen*. Steiner, Stuttgart
    1992, ISBN 3-515-06193-2.
  - Michael DiMaio: *Zonaras’ Account of the Neo-Flavian Emperors. A
    Commentary*. Diss., University of Missouri, Columbia 1977.
  - [Manfred Clauss](../Page/Manfred_Clauss.md "wikilink"): *Die Frauen
    der diokletianisch-konstantinischen Zeit*. In: [Hildegard
    Temporini-Gräfin
    Vitzthum](../Page/Hildegard_Temporini-Gräfin_Vitzthum.md "wikilink")
    (Hrsg.): *Die Kaiserinnen Roms. Von Livia bis Theodora*. C. H. Beck,
    München 2002, S. 340–369, ISBN 3-406-49513-3.
  - Noel Lenski (Hrsg.): *The Cambridge Companion to the Age of
    Constantine (Cambridge Companions to the Ancient World)*. Cambridge
    University Press, Cambridge 2005, ISBN 0-521-52157-2.
  - [R. Scott Moore, "The Stemmata of the Neo-Flavian Emperors", *DIR*
    (1998)](http://www.roman-emperors.org/nfstem.htm)
  - [R. Scott Moore, "The Stemmata of the Emperors of the Tetrarchy",
    *DIR* (1998)](http://www.roman-emperors.org/tetstem.htm)

{{-}}

[君士坦丁王朝](../Category/君士坦丁王朝.md "wikilink")