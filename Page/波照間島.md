[Yaeyama_map.png](https://zh.wikipedia.org/wiki/File:Yaeyama_map.png "fig:Yaeyama_map.png")
**波照間島**（，）位於[琉球列島之](../Page/琉球列島.md "wikilink")[八重山群島](../Page/八重山群島.md "wikilink")，北緯24度2分25秒，東經123度47分16秒。為[日本最南端的有人](../Page/日本.md "wikilink")[島](../Page/島.md "wikilink")，位於[台灣東北方向海域](../Page/台灣.md "wikilink")，距離台灣[宜蘭](../Page/宜蘭縣.md "wikilink")[蘇澳港僅約](../Page/蘇澳港.md "wikilink")200公里。

## 概要

全島面積12.7平方公里，最高點標高59.5公尺，人口約600人。行政劃分屬於日本[沖繩縣](../Page/沖繩縣.md "wikilink")[八重山郡](../Page/八重山郡.md "wikilink")[竹富町](../Page/竹富町.md "wikilink")，郵遞區號為
907-1751。

主要的產業為甘蔗栽培和製糖，並生產沖繩[燒酒](../Page/燒酒.md "wikilink")，以「泡盛」最為有名。

波照間島是可以自行前往的日本最南端的有人島，建有「日本最南端的碑」和「日本最南端和平碑」，同時也是能在日本國內觀測[南十字星的少數島嶼](../Page/南十字座.md "wikilink")。

## 文化財

  - 下田原城跡 - 日本國定史跡
  - 下田原貝塚 - 沖繩縣定史跡
  - コート盛 - 先島諸島火番盛（日本國定史跡）

## 氣候

</div>

  - 屬於[熱帯雨林氣候](../Page/熱帯雨林.md "wikilink")。
  - 最冷月平均氣温18.8℃（1月）
  - 最少雨月降水量106.2mm（12月）

## 交通

可經由[石垣島前往本島](../Page/石垣島.md "wikilink")。

### 海路

安榮觀光公司提供來往石垣島與西表島的船運服務。

### 空路

2007年11月30日前由[琉球空中通勤提供服務](../Page/琉球空中通勤.md "wikilink")。同年12月28日至2008年10月改由[海豚航空聯絡與石垣島之間的天空](../Page/海豚航空.md "wikilink")；隨著海豚航空而後結束經營，目前尚無法透過空路抵達本島。目前大阪第一航空計劃在2015年10月恢復空運服務。

<center>

<File:2016-02-26> Shimotabaru-jou,Hateruma 下田原城・ぶりぶち公園 波照間島
DSCF2530.JPG|下田原城
[File:Hateruma-minami.jpg|日本最南端碑](File:Hateruma-minami.jpg%7C日本最南端碑)
<File:The> southest end stele of Japan.jpg|日本最南端和平碑

</center>

## 相關連結

  - [南波照間島](../Page/南波照間島.md "wikilink")（一個傳說中位於波照間島南方的島嶼，但實際上並不存在）

## 外部連結

  - [竹富町公所網頁的波照間島介紹](https://web.archive.org/web/20061006133951/http://www.town.taketomi.okinawa.jp/island/hateruma2.htm)

  - [波照間島點滴](http://www.kt.rim.or.jp/~yami/haterumamenu.html)

  - [＠Ｙａｉｍａ【波照間島的觀光情報】](https://web.archive.org/web/20060821231032/http://yaima.cool.ne.jp/haterumainfo.html)

[分类:竹富町](../Page/分类:竹富町.md "wikilink")

[P波](../Category/八重山群島.md "wikilink")