[HirambinghamIII.jpg](https://zh.wikipedia.org/wiki/File:HirambinghamIII.jpg "fig:HirambinghamIII.jpg")

**海勒姆·宾厄姆三世**（**Hiram Bingham
III**，）生於[夏威夷](../Page/夏威夷.md "wikilink")[火奴鲁鲁](../Page/火奴鲁鲁.md "wikilink")，[美国学者](../Page/美国.md "wikilink")、探险家、政治人物，[美国共和党成员](../Page/美国共和党.md "wikilink")，曾任[康涅狄格州州长](../Page/康涅狄格州.md "wikilink")（1925年）和[美国参议员](../Page/美国参议员.md "wikilink")（1925年-1933年）。\[1\]

## 生平

宾厄姆因1911年在當地農民帶領下，重新发现[印加帝国遗迹](../Page/印加帝国.md "wikilink")[马丘比丘而闻名](../Page/马丘比丘.md "wikilink")。[马丘比丘的古蹟遺物](../Page/马丘比丘.md "wikilink")，被宾厄姆強行掠奪至[耶魯大學](../Page/耶魯大學.md "wikilink")。為了索回文物[祕魯政府不斷抗議](../Page/祕魯.md "wikilink")，與耶鲁大學追討多年，甚至對簿公堂，最後美國歐巴馬總統在祕魯總統親自遊說下，親自與耶魯大學進行道德勸說希望讓步，終於在2011年耶魯大學退讓，願意歸還第一批、總計366件文物，這些文物于2011年3月30日（星期三），運抵祕魯總統府。\[2\]

## 注释

<div class="references-small" >

<references />

</div>

## 外部链接

  - [Works by Hiram
    Bingham](http://www.archive.org/search.php?query=Hiram%20Bingham%201956%20AND%20mediatype%3Atexts)
  - [Works by Hiram
    Bingham](http://books.google.com/books?q=%22hiram+bingham%22&btnG=Search+Books&as_brr=1)
  - [Selection from Bingham's *The Lost City of the
    Incas*](http://www.labyrinthina.com/bingham.htm)
  - [Machu Picchu on the Web - The
    Discovery](https://web.archive.org/web/20041027004636/http://www.isidore-of-seville.com/machu/2.html)
  - [*Inca Land*, by Hiram
    Bingham](http://www.kellscraft.com/IncaLand/incalandscontents.html)

[Category:美国共和党联邦参议员](../Category/美国共和党联邦参议员.md "wikilink")
[Category:康乃狄克州州长](../Category/康乃狄克州州长.md "wikilink")
[Category:美國共和黨黨員](../Category/美國共和黨黨員.md "wikilink")
[Category:南美洲探險家](../Category/南美洲探險家.md "wikilink")
[Category:美國探險家](../Category/美國探險家.md "wikilink")
[Category:美國陸軍航空軍軍官](../Category/美國陸軍航空軍軍官.md "wikilink")
[Category:美國陸軍中校](../Category/美國陸軍中校.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:普林斯頓大學教師](../Category/普林斯頓大學教師.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:夏威夷人](../Category/夏威夷人.md "wikilink")
[B](../Category/共濟會會員.md "wikilink")
[Category:菲利普斯学院校友](../Category/菲利普斯学院校友.md "wikilink")

1.  [Hiram Bingham III: Machu Picchu Explorer and
    Politician](https://connecticuthistory.org/hiram-bingham-iii-machu-picchu-explorer-and-politician/)
2.  中廣新聞網