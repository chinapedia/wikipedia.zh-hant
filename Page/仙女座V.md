**仙女座
Ⅴ**是一個距離大約252萬[光年](../Page/光年.md "wikilink")，位於[仙女座的](../Page/仙女座.md "wikilink")[矮橢球星系](../Page/矮橢球星系.md "wikilink")。

## 歷史

仙女座
Ⅴ是由Armandroff等人，在1998年為了出版[帕羅馬第二次巡天](../Page/帕羅馬第二次巡天.md "wikilink")（POSS-II）的資料時，進行數值分析時發現的。\[1\]

## 相關條目

  - [仙女座星系的衛星星系](../Page/仙女座星系的衛星星系.md "wikilink")



## 外部連結

  - [**SEDS**: Dwarf Spheroidal Galaxy Andromeda
    V](https://web.archive.org/web/20060220024006/http://www.seds.org/~spider/spider/LG/and5.html)

<!-- end list -->

  - [**SIMBAD**: And V --
    Galaxy](http://simbad.u-strasbg.fr/sim-id.pl?protocol=html&Ident=Andromeda+V&NbIdent=1&Radius=10&Radius.unit=arcmin&CooFrame=FK5&CooEpoch=2000&CooEqui=2000&output.max=all&o.catall=on&output.mesdisp=N&Bibyear1=1983&Bibyear2=2006&Frame1=FK5&Frame2=FK4&Frame3=G&Equi1=2000.0&Equi2=1950.0&Equi3=2000.0&Epoch1=2000.0&Epoch2=1950.0&Epoch3=2000.0)

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:矮橢球星系](../Category/矮橢球星系.md "wikilink")
[Category:仙女座次集團](../Category/仙女座次集團.md "wikilink")
[Andromeda 05](../Category/仙女座.md "wikilink")

1.