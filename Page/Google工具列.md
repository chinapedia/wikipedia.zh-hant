**Google工具栏**（**Google Toolbar**）是一個可於[Microsoft Internet
Explorer與](../Page/Internet_Explorer.md "wikilink")[Mozilla
Firefox上使用的網絡](../Page/Mozilla_Firefox.md "wikilink")[瀏覽器工具列](../Page/瀏覽器.md "wikilink")。

2011年7月19日，Google宣布由於許多Google工具栏所提供的功能已經融入到瀏覽器中，對Firefox版本Google工具栏的開發至Firefox
4版本停止，不再提供對Firefox 5及未來版本的支持\[1\]。

## 功能

[Google_toolbar_firefox.jpg](https://zh.wikipedia.org/wiki/File:Google_toolbar_firefox.jpg "fig:Google_toolbar_firefox.jpg")

  - 可以設定不同搜尋引擊的搜尋框，預設為[Google搜尋](../Page/Google.md "wikilink")
  - [釣魚式攻擊保護](../Page/釣魚式攻擊.md "wikilink")（僅在 Firefox版本）
  - [Feed訂閱](../Page/Feed.md "wikilink")
  - 拼法檢查
  - 自動連接
  - 自動填表
  - [翻譯](../Page/Google翻译.md "wikilink")
  - [PageRank顯示](../Page/PageRank.md "wikilink")
  - 網絡實名
  - [Google書簽](../Page/Google書簽.md "wikilink")
  - [彈出式廣告阻擋](../Page/彈出式廣告.md "wikilink")

## Google計算

**Google計算**是Google
工具列一個可個別[下載的插件](../Page/下載.md "wikilink")，用於[分散式計算的計劃以幫助](../Page/分散式計算.md "wikilink")[科學](../Page/科學.md "wikilink")[硏究](../Page/硏究.md "wikilink")，於2002年3月以限制名額的方式開始\[2\]，並於2005年10月結束\[3\]
\[4\]，只可於[英文版的Google工具列上使用](../Page/英文.md "wikilink")。Google計算容許使用者的電腦在閒置時幫助科學問題的繁複計算，當一個使用者啓動Google計算後，該電腦會下載一個大型研究計畫的一小部份並開始執行[運算](../Page/運算.md "wikilink")，然後歸入其他一千台[電腦的結果](../Page/電腦.md "wikilink")\[5\]。

目前為止，相關的計算研究都是由一個非營利組織[Folding@home提供](../Page/Folding@home.md "wikilink")，他們的目標為試圖計算和模擬[蛋白質摺疊](../Page/蛋白質摺疊.md "wikilink")，以便提高理解與冶療許多不同[疾病](../Page/疾病.md "wikilink")。Google計算的首頁建議，希望能繼續貢獻該計劃的使用者下載Folding@home的客戶端軟體。

## 参考文献

## 外部連結

  - [Google Toolbar](http://toolbar.google.com/intl/en/)
      - [Google工具栏](http://toolbar.google.com/intl/zh-CN/)
      - [Google工具列](http://toolbar.google.com/intl/zh-TW/)

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")
  - [SETI@home](../Page/SETI@home.md "wikilink")

{{-}}

[ja:Googleのサービス\#Google
ツールバー](../Page/ja:Googleのサービス#Google_ツールバー.md "wikilink")

[工具列](../Category/Google服務.md "wikilink")
[Category:分布式计算](../Category/分布式计算.md "wikilink")
[Category:應用程式](../Category/應用程式.md "wikilink") [Category:Firefox
附加组件](../Category/Firefox_附加组件.md "wikilink") [Category:Internet
Explorer工具栏](../Category/Internet_Explorer工具栏.md "wikilink")

1.  An update on Google Toolbar for Firefox
    <http://googletoolbarhelp.blogspot.com/2011/07/update-on-google-toolbar-for-firefox.html>
2.
3.   "Google計算計劃已完結"
4.
5.