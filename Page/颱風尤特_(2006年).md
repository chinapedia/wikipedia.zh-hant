颱-{}-風尤特}}
**颱風尤特**（[英語](../Page/英語.md "wikilink")：****，國際編號：**0622**；[聯合颱風警報中心編號](../Page/聯合颱風警報中心.md "wikilink")：**25W**；[PAGASA命名](../Page/菲律宾大气地球物理和天文管理局.md "wikilink")：**Seniang**）是[2006年太平洋台风季中的一個](../Page/2006年太平洋台风季.md "wikilink")[热带气旋](../Page/热带气旋.md "wikilink")，风暴於12月7日形成，於12月15日消散，维持了8天。尤特是造成的元兇之一，為剛受[颱風榴槤吹襲的](../Page/颱風榴槤_\(2006年\).md "wikilink")[菲律賓再次帶來破壞](../Page/菲律賓.md "wikilink")，導致至少有30人死亡及1580萬[美元](../Page/美元.md "wikilink")（以2006年計算，下同）的經濟損失，在[馬來西亞亦有](../Page/馬來西亞.md "wikilink")8人死亡。「尤特」是[美國提供的名稱](../Page/美國.md "wikilink")，在[馬紹爾語中有](../Page/馬紹爾語.md "wikilink")[颮線之意](../Page/颮線.md "wikilink")。\[1\]

## 氣象歷史

2006年12月2日，一個對流區在[楚克岛东南被](../Page/楚克州.md "wikilink")[聯合颱風警報中心發現](../Page/聯合颱風警報中心.md "wikilink")。雖然對流在幾小時後減少，但3天後再次活躍起來，12月6日聯合颱風警報中心為它發佈熱帶氣旋形成警報。12月7日早上聯合颱風警報中心和[日本氣象廳報告有一個](../Page/氣象廳_\(日本\).md "wikilink")[熱帶低氣壓經已形成](../Page/熱帶低氣壓.md "wikilink")。\[2\]
不久，這個熱帶低氣壓進入[菲律宾大气地球物理和天文管理局的專責範圍內](../Page/菲律宾大气地球物理和天文管理局.md "wikilink")，菲律宾大气地球物理和天文管理局為它命名為Seniang。該熱帶低氣壓在這天增強，日本氣象廳在晚上將它升格為熱帶風暴，命名為尤特。\[3\]

12月8日，日本氣象廳將尤特升格為強烈熱帶風暴，隨著尤特繼續增強，日本氣象廳於翌日早上再升格為一[颱風](../Page/颱風.md "wikilink")，菲律宾大气地球物理和天文管理局和聯合颱風警報中心皆是如此。\[4\]\[5\]
增強後不久，尤特在該日下午登陸[菲律賓](../Page/菲律賓.md "wikilink")[薩馬島和](../Page/薩馬島.md "wikilink")[民都洛島](../Page/民都洛島.md "wikilink")。在橫過菲律賓後，尤特採取相近於[11月颱風飛燕的路徑](../Page/颱風飛燕_\(2006年\).md "wikilink")，先向西北移動，再移向[南海北部](../Page/南海.md "wikilink")。但受到[副熱帶高壓脊的影響](../Page/副熱帶高壓脊.md "wikilink")，尤特的移動速度開始減慢。受到[风切变上升和乾燥空氣環境的共同影響](../Page/风切变.md "wikilink")，尤特在[海南島及](../Page/海南島.md "wikilink")[西沙群島附近逐漸減弱](../Page/西沙群島.md "wikilink")。日本氣象廳於12月13日將其降格為一強烈熱帶風暴，再在同日較後時間降格為一熱帶風暴，最後於翌日降格為一熱帶低氣壓並發出最後報告。\[6\]

## 影響

###

[Typhoon_Utor_2006_PSWS_Map.png](https://zh.wikipedia.org/wiki/File:Typhoon_Utor_2006_PSWS_Map.png "fig:Typhoon_Utor_2006_PSWS_Map.png")
在約兩星期前受[颱風榴槤吹襲的](../Page/颱風榴槤_\(2006年\).md "wikilink")[菲律賓](../Page/菲律賓.md "wikilink")，有9萬人為防範是次風災而需要疏散。\[7\]
而原定於12月10日開始舉行的[东南亚国家联盟峰會和於](../Page/东南亚国家联盟.md "wikilink")12月13日開始舉行的[东亚峰会需要延遲至翌年](../Page/东亚峰会.md "wikilink")1月舉行。\[8\]\[9\]
尤特在菲律賓造成至少30人死亡、8人失蹤及44人受傷，\[10\]
亦吹毀共9,553間房屋，另有33,943間受損，56,313人無家可歸。在當地總共有880,663人受影響。\[11\]
是次風災共造成約1580萬[美元的經濟損失](../Page/美元.md "wikilink")\[12\]，當中有960萬美元是來自[農業損失](../Page/農業.md "wikilink")。\[13\]

###

尤特為[西马来西亚南部地區在](../Page/西马来西亚.md "wikilink")24小時內降下暴雨，雨量高至350毫米。消散後的殘餘雨帶在12月18日為當地，尤其是[柔佛](../Page/柔佛州.md "wikilink")、[森美兰](../Page/森美兰.md "wikilink")、[马六甲和](../Page/马六甲.md "wikilink")[彭亨造成嚴重水浸](../Page/彭亨.md "wikilink")，是為當地有史以來最嚴重的水災。\[14\]
不過，在水災之前的報告卻指出天氣將不會非常惡劣。\[15\][昔加末和](../Page/昔加末.md "wikilink")[哥打丁宜是重災區](../Page/哥打丁宜縣.md "wikilink")，洪水令兩地所有主要道路中斷。\[16\]
截至12月24日，水災共造成8人死亡。\[17\]

## 圖片

Typhoon 25W (Utor) 2006-12-08 12-56.jpg|12月8日 Typhoon Utor 09 dec 2006
0455Z.jpg|12月9日 Typhoon Utor 20061210 0330Z.jpg|12月10日 Typhoon Utor 13
dec 2006 0305Z.jpg|12月13日

## 參見

  - [2006年太平洋台风季](../Page/2006年太平洋台风季.md "wikilink")

  -
## 參考

## 外部連結

  - <https://web.archive.org/web/20090826230250/http://metocph.nmci.navy.mil/jtwc.php>
    [聯合颱風警報中心首頁](../Page/聯合颱風警報中心.md "wikilink")

  - <http://www.jma.go.jp/jma/index.html>
    [日本氣象廳首頁](../Page/日本氣象廳.md "wikilink")

  - <http://www.pagasa.dost.gov.ph>
    [菲律賓大氣地球物理和天文管理局首頁](../Page/菲律賓大氣地球物理和天文管理局.md "wikilink")

[Category:2006年太平洋颱風季](../Category/2006年太平洋颱風季.md "wikilink")
[Utor](../Category/三级热带气旋.md "wikilink")
[Category:影响菲律宾的热带气旋](../Category/影响菲律宾的热带气旋.md "wikilink")
[Category:台风](../Category/台风.md "wikilink")

1.

2.

3.
4.
5.

6.
7.

8.  [The official site of 12th ASEAN SUMMIT: Cebu,
    Philippines](http://www.12thaseansummit.org.ph/innertemplate3.asp?category=news&newsid=319)

9.

10.

11.

12.
13.
14. [Typhoon Utor to
    blame](http://thestar.com.my/news/story.asp?file=/2006/12/21/nation/16380186&sec=nation)
     The Star Online

15. [Seas too choppy for small
    vessels](http://thestar.com.my/news/story.asp?file=/2006/12/14/nation/16311944&sec=nation)


16. [Segamat and Kota Tinggi folks stranded by
    floods](http://thestar.com.my/news/story.asp?file=/2006/12/21/nation/20061221072623&sec=nation)
     The Star Online

17. [Relief all
    round](http://www.thestar.com.my/news/story.asp?file=/2006/12/24/nation/16409486&sec=nation&focus=1)
     The Star Online