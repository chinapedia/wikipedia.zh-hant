**張信剛**教授（）生於[遼寧](../Page/遼寧.md "wikilink")[瀋陽](../Page/瀋陽.md "wikilink")，成長於[台灣](../Page/台灣.md "wikilink")，是[香港城市大學第三任校長及大學講座教授](../Page/香港城市大學.md "wikilink")。曾發表過科學論文逾百篇，編撰[研究專論兩部](../Page/研究專論.md "wikilink")，並在[加拿大取得了一項專利註冊](../Page/加拿大.md "wikilink")。以中文著有五本書及多篇文章。

## 學歷

  - 1962年獲[國立台灣大學土木工程理學士學位](../Page/國立台灣大學.md "wikilink")
  - 1964年獲[史丹福大學結構工程理學碩士學位](../Page/史丹福大學.md "wikilink")
  - 1969年獲[西北大學生物醫學工程哲學博士學位](../Page/西北大學.md "wikilink")

## 履歷

  - 1969年-1975年：任[美國](../Page/美國.md "wikilink")[水牛城](../Page/水牛城.md "wikilink")[紐約州立大學工程及應用科學院土木工程學助理教授](../Page/紐約州立大學.md "wikilink")
  - 1975年-1976年：獲晉升為副教授
  - 1976年-1980年：任[加拿大](../Page/加拿大.md "wikilink")[麥吉爾大學醫學院生物醫學工程及生理學副教授](../Page/麥吉爾大學.md "wikilink")
  - 1980年：獲晉升為教授
  - 1980年-1984年：兼任工程學院化學工程副教授
  - 1981年-1982年：任[法國](../Page/法國.md "wikilink")[巴黎第十二大學](../Page/巴黎第十二大學.md "wikilink")（Université
    Paris-Val de Marne）醫學院訪問教授
  - 1984年-1990年：任美國[南加州大學工程學院生物醫學工程教授及學系主任](../Page/南加州大學.md "wikilink")（1985-1990），兼任醫學院生理學及生物物理學教授
  - 1990年-1994年，任[香港科技大學工程學院創院院長](../Page/香港科技大學.md "wikilink")
  - 1994年-1996年，任美國[匹茲堡大學工程學院院長](../Page/匹茲堡大學.md "wikilink")，兼任工程學院化學工程教授及醫學院醫學教授
  - 1996年-2007年：任香港城市大學校長

## 公職

  - 2003年-08年，任第十屆[全國政協委員](../Page/全國政協.md "wikilink")
  - 1999年起，任香港特別行政區政府司法人員推薦委員會委員
  - 2000-04年，任香港特別行政區政府創新科技顧問委員會委員
  - 2000-03年，任香港特別行政區政府文化委員會主席

## 榮譽

  - 非官守[太平紳士](../Page/太平紳士.md "wikilink") (1999年)
  - [金紫荊星章](../Page/金紫荊星章.md "wikilink") (2002年)
  - [法國政府榮譽團騎士級獎章](../Page/法國政府榮譽.md "wikilink")（Chevalier de la Légion
    d'Honneur）
  - [英國皇家工程師學院外籍會員](../Page/英國皇家工程師學院.md "wikilink")（Foreign Member of
    Royal Academy of Engineering）

## 出版作品

  - 《大中东行纪》 广西师范大学出版社
  - 《大学之修养:张信刚人文通识三十六讲》 广西师范大学出版社
  - 《尼罗河畔随想》 生活·读书·新知三联书店出版社
  - 《茶与咖啡:张信刚文化与经济讲座》北京大学出版社

## 外部連結

  - [香港城市大學：校長室](http://www.cityu.edu.hk/cityu/dpt-admin/op-tc.htm)2007年4月30日已經[退休](../Page/退休.md "wikilink")。
  - [央視國際：“文化校長”張信剛](http://big5.cctv.com/news/china/20060312/100441.shtml)
  - [中國文化中心
    張信剛教授](http://www.cciv.cityu.edu.hk/content.php?p=info&id=chang_hk)

{{-}}     　

[Category:台灣戰後東北移民](../Category/台灣戰後東北移民.md "wikilink")
[Category:獲頒授香港金紫荊星章者](../Category/獲頒授香港金紫荊星章者.md "wikilink")
[Category:太平紳士](../Category/太平紳士.md "wikilink")
[Category:台灣工程學家](../Category/台灣工程學家.md "wikilink")
[Category:香港科學家](../Category/香港科學家.md "wikilink")
[Category:香港城市大學校長](../Category/香港城市大學校長.md "wikilink")
[Category:香港城市大學教授](../Category/香港城市大學教授.md "wikilink")
[Category:美國西北大學校友](../Category/美國西北大學校友.md "wikilink")
[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:國立臺灣大學工學院校友](../Category/國立臺灣大學工學院校友.md "wikilink")
[Category:第十屆全國政協委員](../Category/第十屆全國政協委員.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:紐約州立大學教師](../Category/紐約州立大學教師.md "wikilink")
[Category:南加州大學教師](../Category/南加州大學教師.md "wikilink")
[Category:百人会会员](../Category/百人会会员.md "wikilink")
[Category:沈陽人](../Category/沈陽人.md "wikilink")
[H](../Category/張姓.md "wikilink")