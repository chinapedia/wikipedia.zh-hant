**男同性恋免疫缺乏症**（）是1982年提出，在[艾滋病拥有合理的作为](../Page/艾滋病.md "wikilink")[综合征的医学解释之前](../Page/综合征.md "wikilink")，用于描述「一系列的意料之外的病例」\[1\]也是用来代指[艾滋病的早期用语](../Page/艾滋病.md "wikilink")。这个术语源于当时公共健康方面的科学家注意到在[南加州和](../Page/南加州.md "wikilink")[纽约有](../Page/纽约.md "wikilink")[卡波西氏肉瘤和](../Page/卡波西氏肉瘤.md "wikilink")[卡氏肺囊虫肺炎的](../Page/卡氏肺囊虫肺炎.md "wikilink")[同性恋者疾病群](../Page/同性恋者.md "wikilink")。这是当时对艾滋病的一种误称。
在[艾滋病史早期](../Page/艾滋病.md "wikilink")，当[艾滋病还被考虑为一种男同性恋的疾病时](../Page/艾滋病.md "wikilink")，至少一位医师建议同性恋者严肃考虑与陌生人进行性行为。

一个叫男同性恋健康危机的[特设组织成立](../Page/Ad_hoc.md "wikilink")，来抗争那些通过劈腿行为，静脉注射毒品，[芳香剂而患上的同性恋者才会获得的疾病](../Page/Poppers.md "wikilink")。不久，在进入[美国的](../Page/美国.md "wikilink")[海地人](../Page/海地.md "wikilink")\[2\]、[血友病人](../Page/血友病.md "wikilink")、无其他风险因素的受血者中，同样的卡波西氏肉瘤和卡氏肺囊虫肺炎疾病群被发现。

[AIDS这个术语](../Page/AIDS.md "wikilink")，在1982年末被提出以精确疾病名称。\[3\]这个新名字被政治家等人群支持，因为他们意识到术语中所含的「同性恋者」不能很好的概括这个疾病。1984年4月23日，[美国卫生及公共服务部部长在记者招待会中宣布AIDS的可能诱因被发现](../Page/美国卫生及公共服务部.md "wikilink")：这种逆转录病毒随后于1986年被命名为[人類免疫缺陷病毒](../Page/人類免疫缺陷病毒.md "wikilink")。

## 參看

[Category:LGBT歷史](../Category/LGBT歷史.md "wikilink")
[Category:已淘汰醫學術語](../Category/已淘汰醫學術語.md "wikilink")
[Category:恐同](../Category/恐同.md "wikilink")
[Category:贬义词](../Category/贬义词.md "wikilink")

1.  ["The History of AIDS and
    ARC"](http://biotech.law.lsu.edu/Books/lbb/x590.htm) at the LSU Law
    Center
2.
3.