**瓣蕊花科**(Himantandraceae)也叫[假木兰科](../Page/假木兰科.md "wikilink")、[舌蕊花科](../Page/舌蕊花科.md "wikilink")、[芳香木科](../Page/芳香木科.md "wikilink")、[锥形药科或](../Page/锥形药科.md "wikilink")[刺毛树科](../Page/刺毛树科.md "wikilink")，只有1[属](../Page/属.md "wikilink")—[白木兰属](../Page/白木兰属.md "wikilink")（*Galbulimima*）2[种](../Page/种.md "wikilink")，仅生长在[澳大利亚北部和](../Page/澳大利亚.md "wikilink")[东南亚岛屿上](../Page/东南亚.md "wikilink")。

本[科](../Page/科.md "wikilink")[植物都是含芳香油的](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")，单[叶互生](../Page/叶.md "wikilink")，其[树皮和叶有麻醉作用](../Page/树.md "wikilink")，用于原始宗教的仪式。

## 参考文献

## 外部链接

:\* [瓣蕊花科](http://delta-intkey.com/angio/www/himantan.htm) in L. Watson
and M.J. Dallwitz (1992 onwards). *[The families of flowering
plants](http://delta-intkey.com/angio/): descriptions, illustrations,
identification, information retrieval.* Version: 3rd May 2006.
<http://delta-intkey.com>.

:\* [*Galbulimima
belgraveana*介绍](http://www.shaman-australis.com/~benjamin-thomas/galbu_belgra.html)

:\* [*Galbulimima
belgraveana*的宗教利用](http://www.entheology.org/edoto/anmviewer.asp?a=50&z=5)

:\*
[NCBI瓣蕊花科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=22300&lvl=3&lin=f&keep=1&srchmode=1&unlock)

:\*
[CSDL瓣蕊花科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Himantandraceae)

[\*](../Category/瓣蕊花科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")