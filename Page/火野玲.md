**火野玲**，
**火野灵**（又譯為**-{zh-hans:火野丽、火野蕾依;zh-hk:火野玲、火野蕾依;zh-tw:火野玲、火野蕾依;}-**），日本動漫《[美少女戰士](../Page/美少女戰士.md "wikilink")》本作女主角之一。動畫版的[日語配音員是](../Page/聲優.md "wikilink")[富澤美智惠](../Page/富澤美智惠.md "wikilink")（1990年代版本）／[佐藤利奈](../Page/佐藤利奈.md "wikilink")（Crystal），寫實版的演員是[北川景子](../Page/北川景子.md "wikilink")。

## 變身力量

### 動畫版

  - 火星力量，變身！（Mars Power,Make Up\!）
  - 火星星光力量，變身！（Mars Star Power,Make Up\!）
  - 火星水晶力量，變身！（Mars Crystal Power,Make Up\!）

### 真人版

  - 火星力量，變身！（Mars Power,Make Up\!）

### 漫畫版

  - 火星力量，變身！（Mars Power,Make Up\!）
  - 火星星光力量，變身！（Mars Star Power,Make Up\!）
  - 火星行星力量，變身！（Mars Planet Power,Make Up\!）
  - 火星水晶力量，變身！（Mars Crystal Power,Make Up\!）

### 新版動畫

  - 火星力量，變身！（Mars Power,Make Up\!）
  - 火星星光力量，變身！（Mars Star Power,Make Up\!）
  - 火星行星力量，變身！（Mars Planet Power,Make Up\!）

## 必殺技

### 90年代動畫版

| 初次出現集數 | 日文原版            | 英語 (Viz版本)          | 台灣翻譯    | 香港翻譯 (TVB)   | 大陆翻译   |
| ------ | --------------- | ------------------- | ------- | ------------ | ------ |
| 10     | 悪霊退散（あくりょうたいさん） | Evil Spirit, Begone | 惡靈退散    | 惡靈退散         | 消灭罪恶灵魂 |
| 10     | ファイヤー・ソウル       | Fire Soul           | 火焰聖靈    | 火焰飛散         | 火雷神    |
| 54     | ファイヤー・ソウル・バード   | Fire Soul Bird      | 火焰鳳凰聖靈  | 火焰飛散Firebird | 火雷神    |
| 63     | バーニング・マンダラー     | Burning Mandala     | 燃燒曼陀羅   | 火舞曼陀羅        | 无      |
| 152    | マーズ・フレイム・スナイパー  | Mars Flame Sniper   | 火星烈焰狙擊手 | 火星烈焰飛箭       | 无      |

### Crystal (新版動畫)

| 初次出現回數 | 日文原版               | 英語 (Viz版本)          | 中文翻譯  | 香港翻譯 (ViuTV) |
| ------ | ------------------ | ------------------- | ----- | ------------ |
| 3      | 悪霊退散（あくりょうたいさん）    | Evil Spirit, Begone | 惡靈退散  | 惡靈退散         |
| 15     | バーニング・マンダラー        | Burning Mandala     | 火舞曼陀羅 | 火舞曼陀羅        |
| 28     | 蛇火炎（マーズ・スネイク・ファイア） | Mars Snake Fire     | 蛇火炎   | 火舞烈焰吞噬^      |

^日文漫畫原版中，此絕技的漢字為「蛇火炎」，ViuTV則自創翻譯為「火舞烈焰吞噬」。

### 真人版

  - 妖魔退散（Youma Taisan）
  - 惡靈退散（Akuyou Taisan）
  - 火舞曼陀羅（Burning Mandara）

### 漫畫版

  - 恶灵退散（）
  - 燃烧曼陀罗（）
  - 火星蛇火炎（）
  - 火星烈焰攻击（）

## 參見

[fi:Sailor Moon\#Rei
Hino](../Page/fi:Sailor_Moon#Rei_Hino.md "wikilink") [sv:Lista över
rollfigurer i Sailor Moon\#Sailor Mars (Rei
Hino)](../Page/sv:Lista_över_rollfigurer_i_Sailor_Moon#Sailor_Mars_\(Rei_Hino\).md "wikilink")

[Category:美少女戰士登場人物](../Category/美少女戰士登場人物.md "wikilink")
[Category:1992年首次亮相的漫畫角色](../Category/1992年首次亮相的漫畫角色.md "wikilink")
[Category:虛構女性動漫角色](../Category/虛構女性動漫角色.md "wikilink")
[Category:女性日本人動漫角色](../Category/女性日本人動漫角色.md "wikilink")
[Category:虛構巫女](../Category/虛構巫女.md "wikilink")
[Category:虛構預知能力者](../Category/虛構預知能力者.md "wikilink")
[Category:虛構炎能力者](../Category/虛構炎能力者.md "wikilink")
[Category:虛構日本弓箭手](../Category/虛構日本弓箭手.md "wikilink")
[Category:變身英雄](../Category/變身英雄.md "wikilink")
[Category:虛構公主](../Category/虛構公主.md "wikilink")
[Category:動畫中的青少年角色](../Category/動畫中的青少年角色.md "wikilink")
[Category:漫畫中的青少年角色](../Category/漫畫中的青少年角色.md "wikilink")
[Category:虛構高中生](../Category/虛構高中生.md "wikilink")