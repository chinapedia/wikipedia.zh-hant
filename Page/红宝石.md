**红宝石**，是[剛玉的一种](../Page/剛玉.md "wikilink")，主要成分是[氧化铝](../Page/氧化铝.md "wikilink")（Al<sub>2</sub>O<sub>3</sub>），红色来自[铬](../Page/铬.md "wikilink")（Cr）。自然没有铬的宝石是蓝色的[蓝宝石](../Page/蓝宝石.md "wikilink")。

天然红宝石大多来自[亚洲](../Page/亚洲.md "wikilink")（[缅甸](../Page/缅甸.md "wikilink")、[泰国和](../Page/泰国.md "wikilink")[斯里兰卡](../Page/斯里兰卡.md "wikilink")）、[非洲和](../Page/非洲.md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")，[美国](../Page/美国.md "wikilink")[蒙大拿州和](../Page/蒙大拿州.md "wikilink")[南卡羅莱納州也有部分生产](../Page/南卡羅莱納州.md "wikilink")。天然红宝石非常少见且珍贵，但是人造并非太难，所以工业用红宝石都是人造的。

## 特徵

  - 颜色：深红色
  - 光泽：玻璃光泽
  - [晶体形态](../Page/晶体.md "wikilink")：三方晶系
  - 条痕：白色
  - [硬度](../Page/硬度.md "wikilink")：9，极硬，仅次于[莫桑石和](../Page/莫桑石.md "wikilink")[钻石](../Page/钻石.md "wikilink")（钻石硬度为10，莫桑石硬度介于钻石和红宝石之间）
  - 比重：3.97 g/cm<sup>3</sup> - 4.05 g/cm<sup>3</sup>
  - 断口：不均匀或贝壳状端口

## 名称

  - 紅寶石的英文名稱為Ruby，源於[拉丁文Ruber](../Page/拉丁文.md "wikilink")，意思是紅色。紅寶石的礦物名稱為[剛玉](../Page/剛玉.md "wikilink")。

## 图集

<center>

<File:WLA> hmns Morganite Sapphire and Diamond Necklace.jpg <File:Cut>
Ruby.jpg <File:RubisintéticoEZpg.jpg>

</center>

## 参考文献

## 外部連結

  - [紅寶石 - Mindat](http://www.mindat.org/min-3473.html)

## 参见

  - [紅寶石雷射](../Page/紅寶石雷射.md "wikilink")
  - [剛玉](../Page/剛玉.md "wikilink")
  - [氧化铝](../Page/氧化铝.md "wikilink")
  - [矿物列表](../Page/矿物列表.md "wikilink")

{{-}}

[Category:氧化物礦物](../Category/氧化物礦物.md "wikilink")
[Category:宝石](../Category/宝石.md "wikilink")
[Category:含铝矿物](../Category/含铝矿物.md "wikilink")
[Category:含铬矿物](../Category/含铬矿物.md "wikilink")
[Category:三方晶系矿物](../Category/三方晶系矿物.md "wikilink")