**-{zh-hans:常;zh-hant:宏}-量元素**，又称**-{zh-hans:宏;
zh-hant:常}-量元素**、**大量元素**，指在体内含量丰富的元素。

## 定义

宏量元素指在体内含量占[生物體總](../Page/生物.md "wikilink")[質量](../Page/質量.md "wikilink")0.01%以上的[化学元素](../Page/化学元素.md "wikilink")，計有[碳](../Page/碳.md "wikilink")、[氫](../Page/氫.md "wikilink")、[氧](../Page/氧.md "wikilink")、[氮](../Page/氮.md "wikilink")、[磷](../Page/磷.md "wikilink")、[硫](../Page/硫.md "wikilink")、[氯](../Page/氯.md "wikilink")、[鉀](../Page/鉀.md "wikilink")、[鈉](../Page/鈉.md "wikilink")、[鈣和](../Page/鈣.md "wikilink")[鎂](../Page/鎂.md "wikilink")，這些元素在人體中的含量均在0.04%－62.8%之間，這11種元素共占[人體總質量的](../Page/人.md "wikilink")99.97%。

在天然的條件下，[地球上或多或少地可以找到](../Page/地球.md "wikilink")90多種元素，根據目前掌握的情況，多數科學家比較一致的看法，認為生命必需的元素共有28種，在28種[生命元素中](../Page/生命元素.md "wikilink")，按體內含量的高低可分為宏量元素和[微量元素](../Page/微量元素.md "wikilink")。

## 各种宏量元素在人体内的作用

[Linolenic-acid-3D-vdW.png](https://zh.wikipedia.org/wiki/File:Linolenic-acid-3D-vdW.png "fig:Linolenic-acid-3D-vdW.png")[α-亞麻酸的分子含有其中三个宏量元素](../Page/α-亞麻酸.md "wikilink")：碳、氢、氧。\]\]
人体内拥有11种宏量元素，其中四种组成了身体的大部分：[碳](../Page/碳.md "wikilink")、[氫](../Page/氫.md "wikilink")、[氧和](../Page/氧.md "wikilink")[氮](../Page/氮.md "wikilink")。

### 碳

碳是在体内最丰富的四种元素之一。作为[生命的必须元素](../Page/生命.md "wikilink")，碳和其它物质（一般是其它生命主要元素）组成了所有的[有机化合物](../Page/有机化合物.md "wikilink")。碳之所以可以组成有机化合物的骨架，是因为[碳原子的](../Page/碳原子.md "wikilink")[共价键可以与其它碳原子连接成为长碳链](../Page/共价键.md "wikilink")。\[1\]

### 氫

[Ethane-A-3D-balls.png](https://zh.wikipedia.org/wiki/File:Ethane-A-3D-balls.png "fig:Ethane-A-3D-balls.png")[乙烷的分子](../Page/乙烷.md "wikilink")\]\]
[Ethanol-3D-balls.png](https://zh.wikipedia.org/wiki/File:Ethanol-3D-balls.png "fig:Ethanol-3D-balls.png")的分子比乙烷多了一个[氧原子](../Page/氧.md "wikilink")\]\]
氢是另外一中生命要素。有机化合物和[水分子都包括至少一个氢原子](../Page/水分子.md "wikilink")，其中[水在人体内占了大约](../Page/水.md "wikilink")70％。水可以帮助排毒并辅助代谢，水还可以保护器官，使其正常运作。\[2\]氢原子可以和碳原子接触形成[共价键](../Page/共价键.md "wikilink")，共价键使氢和碳原子组成[碳氢化合物](../Page/碳氢化合物.md "wikilink")，一种由氢和碳组成的有机化合物。碳氢化合物可以经过一些[化学反应衍生成为其它有机物](../Page/化学反应.md "wikilink")。

### 氧

[氧在大多数有机物中都存在](../Page/氧.md "wikilink")，它也是组成水的元素之一。在一些有机物（例如[甲酸](../Page/甲酸.md "wikilink")）内，氧原子和其它原子组合成为[含氧酸](../Page/含氧酸.md "wikilink")，其中有一些是[有机酸](../Page/有机酸.md "wikilink")。\[3\]含氧的酸性[羧基可以帮助](../Page/羧基.md "wikilink")[脂肪酸和](../Page/脂肪酸.md "wikilink")[甘油](../Page/甘油.md "wikilink")[脱水结合成为](../Page/脱水缩合.md "wikilink")[三酸甘油酯](../Page/三酸甘油酯.md "wikilink")，一种储存多余能量的方式。[人体内基本上所有有机化合物都含有氧原子](../Page/人体.md "wikilink")（碳氢化合物没有氧原子，但是[人体内只有微量](../Page/人体.md "wikilink")[肠道细菌](../Page/益生菌.md "wikilink")[代谢后的](../Page/代谢.md "wikilink")[甲烷](../Page/甲烷.md "wikilink")）。

### 氮

[Glucagon.png](https://zh.wikipedia.org/wiki/File:Glucagon.png "fig:Glucagon.png")，一种简单的[肽](../Page/肽.md "wikilink")，是由含氮的氨基酸组成的\]\]
虽说氮是[大气层中含量最丰富的](../Page/大气层.md "wikilink")[气体](../Page/气体.md "wikilink")，但是人体无法利用[氮气](../Page/氮气.md "wikilink")。人体内的有机氮来源于一些[细菌经过](../Page/固氮生物.md "wikilink")[固氮作用形成的](../Page/固氮作用.md "wikilink")[氨](../Page/氨.md "wikilink")。这些氨溶解于水后经过植物根部以[铵的形式被吸收](../Page/铵.md "wikilink")。氮和其它元素组成了[生物碱](../Page/生物鹼.md "wikilink")、[氨基酸和](../Page/氨基酸.md "wikilink")[核碱基](../Page/碱基.md "wikilink")。氨基酸可以组成[蛋白质](../Page/蛋白质.md "wikilink")，而核碱基可以于[磷酸和](../Page/磷酸.md "wikilink")[脱氧核糖或](../Page/脱氧核糖.md "wikilink")[核糖组成](../Page/核糖.md "wikilink")[DNA和](../Page/DNA.md "wikilink")[RNA](../Page/RNA.md "wikilink")。

### 磷

[Phospholipid_TvanBrussel.jpg](https://zh.wikipedia.org/wiki/File:Phospholipid_TvanBrussel.jpg "fig:Phospholipid_TvanBrussel.jpg")是组成细胞膜的主要成分。\]\]
[磷是一种以](../Page/磷.md "wikilink")[磷酸的形式由](../Page/磷酸.md "wikilink")[植物的](../Page/植物.md "wikilink")[根吸收](../Page/根.md "wikilink")。磷酸在人体内十分重要，磷酸和其它化合物组成了[生物必须的](../Page/生物.md "wikilink")[DNA](../Page/DNA.md "wikilink")（储存[基因](../Page/基因.md "wikilink")）、[RNA](../Page/RNA.md "wikilink")（使DNA的基因转化为[蛋白质](../Page/蛋白质.md "wikilink")）、[三磷酸腺苷](../Page/三磷酸腺苷.md "wikilink")（储存和制造[能量](../Page/能量.md "wikilink")）、[磷酸钙](../Page/磷酸钙.md "wikilink")（组成[骨骼和](../Page/骨骼.md "wikilink")[牙齿](../Page/牙齿.md "wikilink")）和[磷脂](../Page/磷脂.md "wikilink")（组成[细胞膜](../Page/细胞膜.md "wikilink")）。磷的存在和生活息息相关，例如围绕[神经细胞](../Page/神經細胞.md "wikilink")[轴突的](../Page/轴突.md "wikilink")[髓鞘](../Page/髓鞘.md "wikilink")（由[鞘磷脂组成](../Page/鞘磷脂.md "wikilink")）可以使[神经冲动得以传播进入](../Page/神经冲动.md "wikilink")[器官](../Page/器官.md "wikilink")（例如[肌肉和](../Page/肌肉.md "wikilink")[腺体](../Page/腺体.md "wikilink")）。\[4\]

### 硫

半胱氨酸、[甲硫氨酸和牛磺酸等一些含有氨基的](../Page/甲硫氨酸.md "wikilink")[有机酸里面有](../Page/有机酸.md "wikilink")[硫原子](../Page/硫.md "wikilink")。而牛磺酸可以抑制神经冲动，用于控制[痉挛](../Page/痉挛.md "wikilink")，而含硫的[氨基酸组成了](../Page/氨基酸.md "wikilink")[蛋白质和](../Page/蛋白质.md "wikilink")[生命重要的一部分](../Page/生命.md "wikilink")：甲硫氨酸可以增进食欲，标识蛋白质的形成，并携带人体需要的硫和[甲基](../Page/甲基.md "wikilink")；半胱氨酸可以帮助解毒和修复[放射线损害](../Page/放射线.md "wikilink")。

### 氯

血液和細胞內液電解質主要成分，也是胃酸的主要成分。

### 鉀

血液和細胞內液電解質主要成分。

### 鈉

血液和細胞外液電解質主要成分。

### 鈣

骨和牙齒的結構; 作用在細胞信號傳導，代謝，組織維持中。

### 鎂

骨頭結構中重要的角色。

## 参考文献

## 相關條目

  - [礦物質](../Page/礦物質.md "wikilink")
  - [生命元素](../Page/生命元素.md "wikilink")
  - [微量元素](../Page/微量元素.md "wikilink")
  - [有机化合物](../Page/有机化合物.md "wikilink")

[Category:生命化学元素](../Category/生命化学元素.md "wikilink")

1.
2.
3.
4.  [1](http://www.term.gov.cn/pages/homepage/result2.jsp?id=333797&subid=10001798&subject=%E8%84%82%E8%B4%A8&subsys=%E7%94%9F%E7%89%A9%E5%8C%96%E5%AD%A6%E4%B8%8E%E5%88%86%E5%AD%90%E7%94%9F%E7%89%A9%E5%AD%A6)