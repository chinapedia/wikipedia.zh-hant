**邹平市**是[中国](../Page/中国.md "wikilink")[山东省](../Page/山东省.md "wikilink")[滨州市所辖的一个](../Page/滨州市.md "wikilink")[县级市](../Page/县级市.md "wikilink")。总面积为1252平方千米，2011年人口为77万。

## 行政区划

邹平市辖11个镇和5个街道办事处。

  - 镇：长山镇、魏桥镇、临池镇、焦桥镇、韩店镇、孙镇、九户镇、青阳镇、明集镇、台子镇、码头镇
  - 街道：黛溪街道、黄山街道、高新街道、好生街道和西董街道。

## 名人

[:Category: 邹平人](../Category/_邹平人.md "wikilink")

## 外部链接

  - [滨州市邹平县政府网站](http://www.zouping.gov.cn/)
  - [邹平生活网](http://www.zouping.cc/)
  - [0543驴友联盟网](http://www.sdhbhw.com/)

## 参考资料

<references />

[邹平县](../Category/邹平县.md "wikilink") [县](../Category/滨州区县.md "wikilink")
[滨州](../Category/山东省县份.md "wikilink")