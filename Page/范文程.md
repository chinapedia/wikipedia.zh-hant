**范文程**（），字**憲斗**，为[清朝开国重臣](../Page/清朝.md "wikilink")。其先世于明初自江西贬往沈阳（清史稿，卷232，列傳十九），“居[抚顺所](../Page/抚顺.md "wikilink")”。他曾事[努尔哈赤](../Page/努尔哈赤.md "wikilink")、[皇太极](../Page/皇太极.md "wikilink")、[顺治帝](../Page/顺治帝.md "wikilink")、[康熙帝四代君主](../Page/康熙帝.md "wikilink")，是清初一代重臣，清朝開國時的規制大多出自其手，更被視為文臣之首。

## 生平

[明](../Page/明朝.md "wikilink")[萬曆二十五年](../Page/萬曆.md "wikilink")（1597年）出生于[遼東都指揮使司](../Page/遼東都指揮使司.md "wikilink")[瀋陽中衛文官屯](../Page/瀋陽中衛.md "wikilink")\[1\]。據《[清史稿](../Page/清史稿.md "wikilink")》，十八世祖是北宋名臣[范仲淹](../Page/范仲淹.md "wikilink")，十七世祖是北宋[宰相](../Page/宰相.md "wikilink")[范纯仁](../Page/范纯仁.md "wikilink")，十六世祖是[南宋朝请大夫](../Page/南宋.md "wikilink")，[范纯仁五子](../Page/范纯仁.md "wikilink")，[范正国](../Page/范正国.md "wikilink")。十五世祖是南宋朝奉郎，[范正国长子](../Page/范正国.md "wikilink")，[范直筠](../Page/范直筠.md "wikilink")。曾祖[范鏓](../Page/范鏓.md "wikilink")，嘉靖年間重臣，官至明朝兵部侍郎，《[明史](../Page/明史.md "wikilink")》有傳。

范文程少时喜好读書，聪明沉稳，「年十八補[生員](../Page/生員.md "wikilink")」，十九歲為[瀋陽中衛學](../Page/瀋陽中衛學.md "wikilink")（清代改為[奉天府](../Page/奉天府.md "wikilink")[儒學及](../Page/儒學.md "wikilink")[盛京](../Page/盛京.md "wikilink")[文廟](../Page/文廟.md "wikilink")）生員，已接受明朝功名。天命三年（1618年），[努尔哈赤攻佔](../Page/努尔哈赤.md "wikilink")[撫順](../Page/撫順.md "wikilink")，當時范文程年僅二十一歲，與其兄[-{范}-文寀](../Page/范文寀.md "wikilink")（生卒年不詳）主动迁往投靠[努尔哈赤](../Page/努尔哈赤.md "wikilink")。努尔哈赤曾對诸贝勒有言：「此名臣孫也，其善遇之！」
随努尔哈赤征讨明朝，攻取[辽阳](../Page/辽阳.md "wikilink")、[西平等地](../Page/西平.md "wikilink")。

[皇太极即位](../Page/皇太极.md "wikilink")，引为左右。范文程曾任[大學士](../Page/大學士.md "wikilink")、[議政大臣等職](../Page/議政大臣.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》有載：「崇德元年（1636），改文館為[內三院](../Page/內三院.md "wikilink")，范為內秘書院大學士，進[世職二等](../Page/世職.md "wikilink")[甲喇章京](../Page/甲喇章京.md "wikilink")」、「進一等[阿思哈尼哈番加](../Page/男爵.md "wikilink")[拖沙喇哈番](../Page/雲騎尉.md "wikilink")，賜號『[巴克先](../Page/巴克先.md "wikilink")』。復進二等[精奇尼哈番](../Page/精奇尼哈番.md "wikilink")」。康熙五年（1666年）八月范文程卒，年七十，諡號**文肅**。
康熙五十二年（1713年），即范文程死後四十七年，康熙為他親題「元輔高風」匾額。

## 家族

  - 曾祖[范鏓](../Page/范鏓.md "wikilink")
  - 子[范承谟](../Page/范承谟.md "wikilink")
  - 子[范承勳](../Page/范承勳.md "wikilink")
  - 子[范承斌](../Page/范承斌.md "wikilink")
  - 孫[范時紀](../Page/范時紀.md "wikilink")

## 家族

《清史稿·范文程传》记载：范文程六子，承廕、承谟、承勋、承斌、承烈、承祚。
[范承謨](../Page/范承謨.md "wikilink")，官至福建總督；三子[范承勳](../Page/范承勳.md "wikilink")，號眉山，自稱九松主人，累遷廣西巡撫、雲貴總督、江南江西總督。

孫[范時崇](../Page/范時崇.md "wikilink")，官至左都御史、兵部尚書；孫[范時繹](../Page/范時繹.md "wikilink")，官至都統、戶部尚書、工部尚書；孫[范時捷](../Page/范時捷.md "wikilink")，官至提督、鑲白旗漢軍都統；孫[范時綬](../Page/范時綬.md "wikilink")，官至左都御史、都統、工部尚書、刑部尚書；孫[范時紀](../Page/范時紀.md "wikilink")，官至副都統、侍郎。

## 参考文献

  -
[category:清朝大学士](../Page/category:清朝大学士.md "wikilink")
[category:沈阳人](../Page/category:沈阳人.md "wikilink")
[category:抚顺人](../Page/category:抚顺人.md "wikilink")
[category:汉军镶黄旗人](../Page/category:汉军镶黄旗人.md "wikilink")
[W文程](../Page/category:范姓.md "wikilink")

[Category:清朝子爵](../Category/清朝子爵.md "wikilink")
[Category:清朝會試正考官](../Category/清朝會試正考官.md "wikilink")
[Category:議政王大臣](../Category/議政王大臣.md "wikilink")
[Category:清朝三孤](../Category/清朝三孤.md "wikilink")
[Category:清朝太子三師](../Category/清朝太子三師.md "wikilink")
[Category:清朝三公](../Category/清朝三公.md "wikilink")
[Category:謚文肅](../Category/謚文肅.md "wikilink")

1.  國立故宮博物院圖書文獻處清史館傳稿