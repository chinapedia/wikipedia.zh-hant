**The
KMPlayer**，簡稱KMP，是[南韓作者](../Page/南韓.md "wikilink")[姜勇囍](../Page/姜勇囍.md "wikilink")（）所開發的免費影音播放軟體。早期名叫“WaSaVi”播放器，歷時五年自主開發的韓文多媒體圖形視窗工程免費項目。目前以其強大的操控功能在眾多免費播放器中逐漸顯示其開發實力。底層調用[WMP的內核](../Page/WMP.md "wikilink")[DirectShow](../Page/DirectShow.md "wikilink")，外部同時支持臨時掛接[Windows下的全部解碼器](../Page/Windows.md "wikilink")，同時也支-{zh-hans:持;
zh-hant:援;}-Winamp中的各種輸入、音效、視覺效果[插件](../Page/插件.md "wikilink")。

The KMPlayer的 *K* 是來自於開發者的姓氏 *姜*（Kang）；The KMPlayer的名稱意義是Kang's
Multimedia Player。\[1\]2007年8月作者将源代码及核心引擎技术出售给Pandora TV。\[2\]

2008年作者姜勇囍進入Daum Communications（多音通訊公司，），並在其後發表另一項作品[Daum
PotPlayer播放軟體](../Page/Daum_PotPlayer.md "wikilink")\[3\]。

## 功能

KMPlayer把網路上所有能見得到的解碼程式（[Codec](../Page/Codec.md "wikilink")）全部收集，支援大多數的影片、音樂、圖片等格式。播放影片時可以作多種調整，例如影像過濾、聲道、亮度、畫面縮放、畫面移動、跳轉到5秒\~10分鐘等等。而在播放音樂可以設定音樂模式、Bass、立體聲等等，也可載裝Winamp的外掛程式。支持自定按鍵，支持更換Skin。支持擷取影片畫面和聲音的功能，不再需要安裝其他的軟件就能把聲音或影片畫面擷取出來。

其強大的檔案支援功能深受使用者歡迎，支援的影片檔包括：
[AVI](../Page/AVI.md "wikilink")、[RealMedia](../Page/RealMedia.md "wikilink")、[MPEG](../Page/MPEG.md "wikilink")
1/2/4、[ASF](../Page/ASF.md "wikilink")、[MKV](../Page/MKV.md "wikilink")、[OGM](../Page/OGM.md "wikilink")、[FLV](../Page/FLV.md "wikilink")、[VCD](../Page/VCD.md "wikilink")、[SVCD等](../Page/SVCD.md "wikilink")；
AVI／MPEG-4支援[Xvid](../Page/Xvid.md "wikilink")／[DivX](../Page/DivX.md "wikilink")／[3vid](../Page/3vid.md "wikilink")／[H.264](../Page/H.264.md "wikilink")／[VP8](../Page/VP8.md "wikilink")（[WebM](../Page/WebM.md "wikilink")）等編碼格式。
[OGG](../Page/Ogg.md "wikilink")／[OGM](../Page/OGM.md "wikilink")／[MKV](../Page/MKV.md "wikilink")／[AC3](../Page/AC3.md "wikilink")／[DTS](../Page/DTS.md "wikilink")／[Monkey's
Audio解碼等](../Page/Monkey's_Audio.md "wikilink")。
支援的聲音檔：[APE](../Page/APE.md "wikilink")、[MP3](../Page/MP3.md "wikilink")、[WMA](../Page/WMA.md "wikilink")、[WAV](../Page/WAV.md "wikilink")、[MPC](../Page/MPC.md "wikilink")、[FLAC](../Page/FLAC.md "wikilink")、[ALAC](../Page/ALAC.md "wikilink")、[AAC](../Page/AAC.md "wikilink")、[MIDI等](../Page/MIDI.md "wikilink")。
支援的圖片檔：[BMP](../Page/BMP.md "wikilink")、[GIF](../Page/GIF.md "wikilink")、[JPEG](../Page/JPEG.md "wikilink")、[PNG](../Page/PNG.md "wikilink")···等。
支援[BIN](../Page/BIN.md "wikilink")、[ISO](../Page/ISO.md "wikilink")、[IMG](../Page/IMG.md "wikilink")、[NRG製成的音樂格式光碟映像檔](../Page/NRG.md "wikilink")。

完全支援基於[MPEG-2編碼標準的DVD影片光碟播放](../Page/MPEG-2.md "wikilink")。

## 跟其它播放器的比較

The KMPlayer的在GUI（图形界面，即前端）中包含的功能比跟官方編碼的播放器比較，例如[Windows Media
Player](../Page/Windows_Media_Player.md "wikilink")、[Real
Player等](../Page/Real_Player.md "wikilink")，进而跟一些其它萬能播放器对比，如[Media
Player
Classic](../Page/Media_Player_Classic.md "wikilink")、[Mplayer等都要多](../Page/Mplayer.md "wikilink")。

功能較接近的有[GOM
Player](../Page/GOM_Player.md "wikilink")、[SPlayer](../Page/SPlayer.md "wikilink")（射手播放器）等。

但功能數目少於[VLC以及Mplayer前端](../Page/VLC.md "wikilink")[SMPlayer](../Page/SMPlayer.md "wikilink")。

## Bug問題

雖然KMPlayer功能非常強，但是[Bug](../Page/Bug.md "wikilink")（漏洞）也很多。不過版本也更新得很快，[BETA版本有時候幾天就更新來新增功能和修正BUG](../Page/軟件版本週期#Beta.md "wikilink")。但是，在原作者離開製作團隊後更新速度已大幅減慢。中文官方論壇不少人的發文都是反映BUG問題。其中針對Full-HD（1920
\* 1080 up）及藍光光碟[Blu-ray
Disc輸出的](../Page/Blu-ray_Disc.md "wikilink")[m2ts格式檔案與](../Page/m2ts.md "wikilink")[DTS](../Page/DTS.md "wikilink")
5.1聲道支援的不完整，常導致播放時有聲無影、有影無聲、爆音等問題。主因為KMPlayer本身無法順暢支援[LPCM的數位音源輸出](../Page/LPCM.md "wikilink")。

## 移动版本

2014年，KMPlayer推出[Android](../Page/Android.md "wikilink")、[iPhone和](../Page/iPhone.md "wikilink")[iPad版本](../Page/iPad.md "wikilink")。这些移动版与其电脑版一样主推支持“万能格式”的优势，安装一次即可支持几乎所有主流的音、视频格式。此外，还针对目前手机、平板的触摸屏特点提供方便的操作支持。

## 爭議

有傳言指出The KMPlayer核心程式碼有使用到以[GPL發佈的](../Page/GPL.md "wikilink")[Media
Player
Classic程式碼](../Page/Media_Player_Classic.md "wikilink")。\[4\]然而在論壇上宣稱The
KMPlayer是以[Borland](../Page/Borland.md "wikilink")
[Delphi寫成](../Page/Delphi.md "wikilink")，未使用[GPL的程式碼](../Page/GPL.md "wikilink")。\[5\]\[6\]以[GPL](../Page/GPL.md "wikilink")（或[LGPL](../Page/LGPL.md "wikilink")）发布的[ffmpeg](../Page/ffmpeg.md "wikilink")，在2009年2月，将The
KMPlayer加入到“耻辱名单”\[7\]，因为被认为使用[ffmpeg的代码和二进制文件](../Page/ffmpeg.md "wikilink")，但没有按照规定/惯例开放相应说明/源码。\[8\]\[9\]\[10\]\[11\]

## 復活節彩蛋

在KMPlayer的主視窗中點右鍵→選項→關於，之後在「關於」主視窗左上角的黑色方框中點兩下滑鼠左鍵，會出現一個[雷電射擊遊戲](../Page/雷電_\(遊戲\).md "wikilink")（.swf,
flash
demo），預設以上下左右控制方向，Z鍵開火、X鍵轟炸，該彩蛋是以外連到KMPlayer網站的一個Flash檔案來進行，因此在沒有網路或是遭防火牆阻擋的情況下則無法使用。

## 参考资料

## 外部連結

  - [官方网站](http://www.kmpmedia.net/)
  - [官方中文論壇](http://www.kmplayer.com/forums/forumdisplay.php?f=7)

[Category:媒体播放器](../Category/媒体播放器.md "wikilink")

1.  <http://www.kmplayer.com/forums/showthread.php?t=141>
2.  <http://www.kmplayer.com/forums/showthread.php?t=8931>
3.
4.  <http://sourceforge.net/forum/forum.php?forum_id=462894>
5.  <http://www.kmplayer.com/forums/showthread.php?t=141>
6.  <http://www.kmplayer.com/forums/showthread.php?t=140>
7.
8.  [Kang Media Player violates FFmpeg's
    license](http://roundup.ffmpeg.org/roundup/ffmpeg/issue820)
9.  <http://ffmpeg.org/shame.html>
10.
11.