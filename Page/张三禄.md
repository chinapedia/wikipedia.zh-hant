**张三禄**，[清末](../Page/清末.md "wikilink")[藝術家](../Page/藝術家.md "wikilink")，[北京人](../Page/北京.md "wikilink")，目前见于[文字记载最早的](../Page/文字记载.md "wikilink")[相声](../Page/相声.md "wikilink")[艺人](../Page/艺人.md "wikilink")\[1\]，其艺术生涯下限大抵在[清朝](../Page/清朝.md "wikilink")[同治年间](../Page/同治.md "wikilink")，上限在[咸丰或](../Page/咸丰.md "wikilink")[道光年间](../Page/道光.md "wikilink")。根据相关记载并推测，张三禄本是[八角鼓丑角艺人](../Page/八角鼓.md "wikilink")，后改说相声。

张三禄是[北京天桥艺人](../Page/北京天桥.md "wikilink")“[管儿张](../Page/管儿张.md "wikilink")”的先辈，据艺人们说曾是北京东、西、北城艺人的头目。他原为八角鼓丑角艺人，以抓哏逗乐为主，亦通晓[口技](../Page/口技.md "wikilink")、[戏法等技艺](../Page/戏法.md "wikilink")，后改说相声。\[2\]
他在演出时使“暗春”（用幔帐围着演员说相声、口技，听众闻其声，不见其人）也极拿手，曾被誉为“暗春泰斗”。近世[满族艺人](../Page/满族.md "wikilink")[玉小三曾回忆说](../Page/玉小三.md "wikilink")：“张三禄说单口笑话（即单口相声）最有名望，‘活儿’非常出色，内容也净，也可以吸收妇女观众，当时12[文钱可买一斤](../Page/文钱.md "wikilink")[麵](../Page/麵粉.md "wikilink")，他一天可以赚到25、26[吊钱](../Page/貫錢.md "wikilink")\[3\]。由此可见当时群众是多么喜欢他的表演。他常说的活儿有《贼鬼夺刀》、《九头案》等。”\[4\]《贼鬼夺刀》传留了下来，仍是传统相声名篇。在清代“百本张”钞本子弟书《[随缘乐](../Page/随缘乐.md "wikilink")》中提到“学相声好似还魂张三禄”，亦可见张三禄的相声艺术在相声发展早期已有重大影响。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

`|width=30% align=center|`**`从师`**
`|width=40% align=center|`**`辈分`**
`|width=30% align=center|`**`收徒`**
`|-`

|width=30% align=center|--

`|width=40% align=center|`**`-`**
` |width=30% align=center |`[`朱绍文`](../Page/朱绍文.md "wikilink")`、`[`阿彦涛`](../Page/阿彦涛.md "wikilink")`、`[`沈春和`](../Page/沈春和.md "wikilink")

|-

[Z张](../Category/清朝藝術家.md "wikilink")
[1Z张](../Category/相声演员.md "wikilink")

1.
2.  [連闊如](../Page/連闊如.md "wikilink")《[江湖丛谈](../Page/江湖丛谈.md "wikilink")》，1936年北京《[时言报](../Page/时言报.md "wikilink")》社版：“八角鼓之有名丑角儿为张三禄，其艺术之高超，胜人一筹者，仗以当场抓哏，见景生情，随机应变，不用死套话儿，演来颇受社会各界人士欢迎。后因性情怪僻，不易搭班，受人排挤，彼愤而撂地。当其上明地时，以说、学、逗、唱四大技能作艺，游逛的人士皆愿听其玩艺儿。张三禄不愿说八角鼓，自称其艺为相声。”。

3.  1吊＝1000文錢，即25,000到26,000[文錢](../Page/文錢.md "wikilink")，當時1斤約600克。假設張賺25,000文錢，約可買2083斤麵粉，約1250公斤。2013年北京的超市1[公斤裝麵粉](../Page/公斤.md "wikilink")，約[人民幣](../Page/人民幣.md "wikilink")6元。依物價指數，約相當於每日可賺人民幣7500元。

4.  薛宝琨《中国的相声》，1985年[人民出版社](../Page/人民出版社.md "wikilink")。