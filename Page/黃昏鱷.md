**黃昏鱷**（屬名：*Hesperosuchus*）是已滅絕[爬行動物的一屬](../Page/爬行動物.md "wikilink")，只包含唯一種**敏捷黃昏鱷**（*H.
agilis*）。這種動物成為大型恐龍[腔骨龍的食物來源](../Page/腔骨龍.md "wikilink")，出現在[化石化的胃部裡](../Page/化石.md "wikilink")。這種喙頭鱷類的化石在[亞利桑那州](../Page/亞利桑那州.md "wikilink")、[新墨西哥州的晚](../Page/新墨西哥州.md "wikilink")[三疊紀](../Page/三疊紀.md "wikilink")[诺利阶地層中發現](../Page/诺利阶.md "wikilink")。

黃昏鱷與[獸腳亞目的](../Page/獸腳亞目.md "wikilink")[腔骨龍生存於相同時代](../Page/腔骨龍.md "wikilink")。腔骨龍曾經被認為會有同類相食的行為，曾經在成年腔骨龍的[胃部發現疑似同類的化石](../Page/胃.md "wikilink")，目前被認為可能是黃昏鱷的化石\[1\]。

## 參考資料

[Category:喙頭鱷亞目](../Category/喙頭鱷亞目.md "wikilink")
[Category:三疊紀鱷形類](../Category/三疊紀鱷形類.md "wikilink")

1.