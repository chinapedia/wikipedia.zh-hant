**燈頰鯛**（[学名](../Page/学名.md "wikilink")：），又名**燈眼魚**、**奇眼鲷**，是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[金眼鯛目](../Page/金眼鯛目.md "wikilink")[燧鯛亞目](../Page/燧鯛亞目.md "wikilink")[燈眼魚科](../Page/燈眼魚科.md "wikilink")[燈頰鯛屬的下的唯一](../Page/燈頰鯛屬.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於西[太平洋區](../Page/太平洋.md "wikilink")，包括[印尼](../Page/印尼.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[日本](../Page/日本.md "wikilink")、[密克羅尼西亞等海域](../Page/密克羅尼西亞.md "wikilink")。

## 深度

水深2至400公尺。

## 特徵

本魚體黑色，兩背鰭，背鰭及臀鰭有強棘，鱗片粗厚，眼下有半月形的大型有橫斑紋之發光器，白天此斑紋為白色，夜間發光。體長可達28公分。

## 生態

為岩礁區魚類，隨著成長棲息深度越深，以浮游生物為食物。[夜行性](../Page/夜行性.md "wikilink")，白天躲在洞穴或陰暗處。

## 經濟利用

非食用魚，為水族館常見的觀賞魚種。

## 参考文献

  -
  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

  -
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[katoptron](../Category/燈眼魚科.md "wikilink")
[Category:單種屬魚類](../Category/單種屬魚類.md "wikilink")