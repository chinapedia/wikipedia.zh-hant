**地蜥鱷科**（Metriorhynchidae）是群海生鱷類，生存於[侏儸紀與](../Page/侏儸紀.md "wikilink")[白堊紀的海洋](../Page/白堊紀.md "wikilink")。牠們的前肢縮小並成為槳狀，與牠們現存的近親不同，地蜥鱷類失去了牠們的[皮內成骨](../Page/皮內成骨.md "wikilink")（Osteoderms）。牠們尾巴上有類似魚的鰭，身體形狀最具流體動力學。\[1\]地蜥鱷科是[主龍類中](../Page/主龍類.md "wikilink")，唯一適應海生環境的生物群。\[2\]

地蜥鱷科是在1843年由[奧地利](../Page/奧地利.md "wikilink")[動物學家Leopold](../Page/動物學家.md "wikilink")
Fitzinger建立。\[3\]

## 地理分佈

本科是全球廣佈的動物，化石在[英格蘭](../Page/英格蘭.md "wikilink")、[法國](../Page/法國.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[德國](../Page/德國.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")、[古巴](../Page/古巴.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")、[智利等地發現](../Page/智利.md "wikilink")。\[4\]

## 屬

地蜥鱷科的[模式屬是](../Page/模式屬.md "wikilink")[地蜥鱷](../Page/地蜥鱷.md "wikilink")，生存於[侏儸紀中期到晚期](../Page/侏儸紀.md "wikilink")。\[5\]此科的其他屬包括[遠距鱷](../Page/遠距鱷.md "wikilink")（*Teleidosaurus*）、[地龍](../Page/地龍.md "wikilink")、[達寇龍](../Page/達寇龍.md "wikilink")、[安那里奧鱷](../Page/安那里奧鱷.md "wikilink")（*Enaliosuchus*）。本科的[可疑名稱包括](../Page/可疑名稱.md "wikilink")*Aggiosaurus*\[6\]與[泳鱷](../Page/泳鱷.md "wikilink")（*Neustosaurus*）。\[7\]

*Purranisaurus*與*Suchodus*在現今被認為是[地蜥鱷屬的](../Page/地蜥鱷.md "wikilink")[次同物異名](../Page/次同物異名.md "wikilink")；而[速蜥鱷](../Page/速蜥鱷.md "wikilink")（*Cricosaurus*）則被不同的古生物學家認為是地蜥鱷、地龍、達寇龍的次同物異名。\[8\]

[卡普林鱷曾經被認為屬於地蜥鱷科](../Page/卡普林鱷.md "wikilink")，類似達寇龍。\[9\]在1989年，科學家證實卡普林鱷是[滄龍的次同物異名](../Page/滄龍.md "wikilink")。\[10\]

根據近年的[親緣分支分類法研究](../Page/親緣分支分類法.md "wikilink")，許多屬被質疑是否為天然的[單系群](../Page/單系群.md "wikilink")，包含：地蜥鱷\[11\]\[12\]遠距鱷、\[13\]\[14\]地龍、\[15\]\[16\]與達寇龍。\[17\]研究有限的速蜥鱷也被質疑，而*Purranisaurus*則可能是個獨立的屬。\[18\]

## 參考資料

[Category:海鱷亞目](../Category/海鱷亞目.md "wikilink")

1.  Fraas E. 1902. Die Meer-Krocodilier (Thalattosuchia) des oberen Jura
    unter specieller Berücksichtigung von *Dacosaurus* und *Geosaurus*.
    *Paleontographica* **49**: 1-72.

2.  Steel R. 1973. *Crocodylia. Handbuch der Paläoherpetologie, Teil
    16*. Stuttgart: Gustav Fischer Verlag,116 pp.

3.
4.
5.
6.  Buffetaut E. 1982. *Aggiosaurus nicaeensis* Ambayrac, 1913, from the
    Upper Jurassic of south-eastern France: A marine crocodilian, not a
    dinosaur. *Neues Jahrbuch für Geologie und Paläontologie,
    Monatshefte* (8): 469-475.

7.  Buffetaut, E. 1982. Radiation évolutive, paléoécologie et
    biogéographie des Crocodiliens mésosuchienes. *Mémoires Societé
    Geologique de France* **142**: 1–88.

8.
9.  Simonelli V. 1896. Intoro agli avanzi di coccodrilliano scoperti a
    San Valentino (provincial di Reggio Emilia) nel 1886. *Atli della
    Reale Accademia dei Lincei, series Qunita Rendiconti* **5** (2):
    11-18.

10. Sirotti A. 1989. *Mosasaurus hoffmanni* Mantell, 1828 (Reptilia)
    nelle \<<argille scagliose>\> di S. Valentino (Reggio E.). *Atti
    della società dei naturalisti e matematici di Modena* **120**:
    135-146.

11. Gasparini Z, Pol D, Spalletti LA. 2006. An unusual marine
    crocodyliform from the Jurassic-Cretaceous boundary of Patagonia.
    *Science* **311**: 70-73.

12. Young MT. 2007. The evolution and interrelationships of
    Metriorhynchidae (Crocodyliformes, Thalattosuchia). *Journal of
    Vertebrate Paleontology* **27** (3): 170A.

13.
14. Mueller-Töwe IJ. 2005. Phylogenetic relationships of the
    Thalattosuchia. *Zitteliana* **A45**: 211–213.

15.
16.
17.
18.