[Schleswig-Hostein_map.PNG](https://zh.wikipedia.org/wiki/File:Schleswig-Hostein_map.PNG "fig:Schleswig-Hostein_map.PNG")
[Slesvig-counties.png](https://zh.wikipedia.org/wiki/File:Slesvig-counties.png "fig:Slesvig-counties.png")

**石勒苏益格**（；；
[低地德语](../Page/低地德语.md "wikilink")：*Sleswig*；[北弗里西亚语](../Page/北弗里西亚语.md "wikilink")：*Slaswik*）位於德國和丹麥的邊界以北約60公里和以南約70公里的範圍。該地區的傳統意義在於北海和波羅的海之間的貨物轉運，將通過俄羅斯的貿易路線與沿著萊茵河和大西洋海岸的貿易路線相連接（參見[基爾運河](../Page/基爾運河.md "wikilink")）。

11至12世纪时，石勒苏益格是一个丹麦的[公国](../Page/公国.md "wikilink")。到了15世纪，采邑与[姻亲令它与](../Page/姻亲.md "wikilink")[德意志公国](../Page/德意志.md "wikilink")[荷尔斯泰因关系亲近](../Page/荷尔斯泰因.md "wikilink")。前者一直隶属丹麦，后者则属[神圣罗马帝国](../Page/神圣罗马帝国.md "wikilink")。于是，到了19世纪，因为[民族国家的兴起](../Page/民族国家.md "wikilink")，德丹一直有领土争议。1460年，石勒苏益格公国与[丹麦王国建立](../Page/丹麦王国.md "wikilink")[共主邦聯](../Page/共主邦聯.md "wikilink")，在1864年前是丹麦的封地。1864年，[普鲁士王国和](../Page/普鲁士王国.md "wikilink")[奥地利帝国发动](../Page/奥地利帝国.md "wikilink")[普丹战争](../Page/普丹战争.md "wikilink")，击败丹麦，共同占领石勒蘇益格与荷爾斯泰因两地。1866年，普军在[普奥战争击败奥地利](../Page/普奥战争.md "wikilink")，夺取两地，戰爭勝利後成立[北德意志邦联](../Page/北德意志邦联.md "wikilink")。1920年，德国战败后，经过，石勒苏益格的北部归于丹麦，成為現在丹麥的[南日德蘭郡](../Page/南日德蘭郡.md "wikilink")，南部归于德国。

## 请参看

  - [什列斯威-好斯敦](../Page/什列斯威-好斯敦.md "wikilink")
  - [日德兰半岛](../Page/日德兰半岛.md "wikilink")

[Category:丹麦历史上的公国](../Category/丹麦历史上的公国.md "wikilink")
[Category:德国历史上的公国](../Category/德国历史上的公国.md "wikilink")
[Category:南丹麦大区历史](../Category/南丹麦大区历史.md "wikilink")
[Category:石勒苏益格-荷尔斯泰因州历史](../Category/石勒苏益格-荷尔斯泰因州历史.md "wikilink")