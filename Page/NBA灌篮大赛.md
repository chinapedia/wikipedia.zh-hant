**灌籃大賽**（）是一項[NBA全明星賽中的帶有娛樂性質的比賽](../Page/NBA全明星賽.md "wikilink")，通常是在全明星賽前的週六舉辦。參賽者嘗試做出各種最具觀賞性和難度的[入樽動作](../Page/入樽.md "wikilink")，評審共五人，每次的扣籃動作評分從6-10分不等，但此競賽為娛樂性質居多，並無嚴肅的比賽壓力。首次入樽大賽是[美國籃球協會](../Page/美國籃球協會.md "wikilink")（ABA）在1976所舉辦的全明星賽上，當時的「J博士」[朱利葉斯·歐文奪得了那次比賽的冠軍](../Page/朱利葉斯·歐文.md "wikilink")。

[扎克·拉文](../Page/扎克·拉文.md "wikilink")、[傑森·理察森](../Page/傑森·理察森.md "wikilink")、[哈洛德·邁納](../Page/哈洛德·邁納.md "wikilink")、[多米尼克·威爾金斯和](../Page/多米尼克·威爾金斯.md "wikilink")[麥可·喬丹都曾贏得過兩次入樽大賽冠軍頭銜](../Page/麥可·喬丹.md "wikilink")，而[內特·羅賓遜是唯一一位贏得三次入樽大賽冠軍的球員](../Page/內特·羅賓遜.md "wikilink")，同時[麥可·喬丹和](../Page/麥可·喬丹.md "wikilink")曾經在自己主場贏得冠軍。

## 2014年賽制失敗

2014年全明星周末，[扣篮大赛由之前的单人参赛改为东西部各挑选](../Page/扣篮大赛.md "wikilink")3人组队参赛，比赛分为第一轮的90秒团队自由扣篮和第二轮的一对一扣篮PK。同时赛事不再有正式的扣篮大赛冠军，而是由在第二轮中先胜3盘的队伍获胜，并且由球迷在社交网站上投票选出人气最高的球员获得本场最佳扣将称号。这是扣篮大赛1985年进入NBA全明星周末以来最大的规则改动，参赛选手单独扣篮的场面减少，第一轮时间限制使不少扣篮显得简单而仓促，第二轮一方先胜3盘即结束的规则又使某些球员精心准备的扣篮动作未来得及施展比赛就草草结束，让不少球迷和球员大呼不过瘾。此赛制实行一届后即宣告废除，2015年纽约全明星周末扣篮大赛将回归经典的单人赛评分机制。

## 歷屆灌籃大賽冠軍

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>冠軍得主</p></th>
<th><p>所屬球隊</p></th>
<th><p>舉辦地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2019</p></td>
<td><p>Hamidou Diallo</p></td>
<td><p>奧克拉荷馬雷霆</p></td>
<td><p>夏洛特</p></td>
</tr>
<tr class="even">
<td><p>2018</p></td>
<td><p><a href="../Page/多诺万·米切尔.md" title="wikilink">多诺万·米切尔</a></p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td><p><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td></td>
<td><p><a href="../Page/印第安那步行者.md" title="wikilink">印第安那步行者</a></p></td>
<td><p><a href="../Page/紐奧良.md" title="wikilink">紐奧良</a></p></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/扎克·拉文.md" title="wikilink">扎克·拉文</a></p></td>
<td><p><a href="../Page/明尼蘇達木狼.md" title="wikilink">明尼蘇達木狼</a></p></td>
<td><p><a href="../Page/多倫多.md" title="wikilink">多倫多</a></p></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/扎克·拉文.md" title="wikilink">扎克·拉文</a></p></td>
<td><p><a href="../Page/明尼蘇達木狼.md" title="wikilink">明尼蘇達木狼</a></p></td>
<td><p><a href="../Page/紐約.md" title="wikilink">紐約</a>/<a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/约翰·沃尔.md" title="wikilink">约翰·沃尔</a>[1]</p></td>
<td><p><a href="../Page/華盛頓巫師.md" title="wikilink">華盛頓巫師</a></p></td>
<td><p><a href="../Page/新奧爾良.md" title="wikilink">新奧爾良</a></p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/泰伦斯·罗斯.md" title="wikilink">泰伦斯·罗斯</a></p></td>
<td><p><a href="../Page/多伦多猛龙.md" title="wikilink">多伦多猛龙</a></p></td>
<td><p><a href="../Page/休士頓.md" title="wikilink">休士頓</a></p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/傑里米·埃文斯.md" title="wikilink">傑里米·埃文斯</a></p></td>
<td><p><a href="../Page/猶他爵士.md" title="wikilink">猶他爵士</a></p></td>
<td><p><a href="../Page/奧蘭多.md" title="wikilink">奧蘭多</a></p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/布莱克·格里芬.md" title="wikilink">布莱克·格里芬</a></p></td>
<td><p><a href="../Page/洛杉磯快船.md" title="wikilink">洛杉磯快船</a></p></td>
<td><p><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/內特·羅賓遜.md" title="wikilink">內特·羅賓遜</a></p></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a></p></td>
<td><p><a href="../Page/達拉斯.md" title="wikilink">達拉斯</a></p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/內特·羅賓遜.md" title="wikilink">內特·羅賓遜</a></p></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a></p></td>
<td><p><a href="../Page/鳳凰城.md" title="wikilink">鳳凰城</a></p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/德懷特·霍華.md" title="wikilink">德懷特·霍華</a></p></td>
<td><p><a href="../Page/奧蘭多魔術.md" title="wikilink">奧蘭多魔術</a></p></td>
<td><p><a href="../Page/新奧爾良.md" title="wikilink">新奧爾良</a></p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/傑拉德·格林.md" title="wikilink">傑拉德·格林</a></p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td><p><a href="../Page/拉斯維加斯.md" title="wikilink">拉斯維加斯</a></p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/內特·羅賓遜.md" title="wikilink">內特·羅賓遜</a></p></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a></p></td>
<td><p><a href="../Page/休斯敦.md" title="wikilink">休斯敦</a></p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/喬什·史密斯.md" title="wikilink">喬什·史密斯</a></p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></p></td>
<td><p><a href="../Page/丹佛.md" title="wikilink">丹佛</a></p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/弗雷德里克·瓊斯.md" title="wikilink">弗雷德里克·瓊斯</a></p></td>
<td><p><a href="../Page/印第安納步行者.md" title="wikilink">印第安納步行者</a></p></td>
<td><p><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯</a></p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/傑森·理察森.md" title="wikilink">傑森·理察森</a></p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td><p><a href="../Page/亞特蘭大.md" title="wikilink">亞特蘭大</a></p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/傑森·理察森.md" title="wikilink">傑森·理察森</a></p></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
<td><p><a href="../Page/費城.md" title="wikilink">費城</a></p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/戴斯蒙·梅森.md" title="wikilink">戴斯蒙·梅森</a></p></td>
<td><p><a href="../Page/西雅圖超音速.md" title="wikilink">西雅圖超音速</a></p></td>
<td><p><a href="../Page/華盛頓.md" title="wikilink">華盛頓</a></p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p><a href="../Page/文斯·卡特.md" title="wikilink">文斯·卡特</a></p></td>
<td><p><a href="../Page/多倫多暴龍.md" title="wikilink">多倫多暴龍</a></p></td>
<td><p><a href="../Page/奧克蘭.md" title="wikilink">奧克蘭</a></p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p>賽事取消</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p>賽事取消</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><a href="../Page/柯比·布萊恩.md" title="wikilink">柯比·布萊恩</a></p></td>
<td><p><a href="../Page/洛杉磯湖人.md" title="wikilink">洛杉磯湖人</a></p></td>
<td><p><a href="../Page/克里夫蘭.md" title="wikilink">克里夫蘭</a></p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/布倫特·貝瑞.md" title="wikilink">布倫特·貝瑞</a></p></td>
<td><p><a href="../Page/洛杉磯快船.md" title="wikilink">洛杉磯快船</a></p></td>
<td><p><a href="../Page/聖安東尼奧.md" title="wikilink">聖安東尼奧</a></p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/哈洛德·邁納.md" title="wikilink">哈洛德·邁納</a></p></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a></p></td>
<td><p><a href="../Page/菲尼克斯.md" title="wikilink">菲尼克斯</a></p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/伊塞亞·萊德爾.md" title="wikilink">伊塞亞·萊德爾</a></p></td>
<td><p><a href="../Page/明尼蘇達森林狼.md" title="wikilink">明尼蘇達森林狼</a></p></td>
<td><p><a href="../Page/明尼阿波利斯.md" title="wikilink">明尼阿波利斯</a></p></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/哈洛德·邁納.md" title="wikilink">哈洛德·邁納</a></p></td>
<td><p><a href="../Page/邁阿密熱火.md" title="wikilink">邁阿密熱火</a></p></td>
<td><p><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/塞德里克·塞巴洛斯.md" title="wikilink">塞德里克·塞巴洛斯</a></p></td>
<td><p><a href="../Page/菲尼克斯太陽.md" title="wikilink">菲尼克斯太陽</a></p></td>
<td><p><a href="../Page/奧蘭多.md" title="wikilink">奧蘭多</a></p></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td><p><a href="../Page/迪·布朗_(1968年出生).md" title="wikilink">迪·布朗</a></p></td>
<td><p><a href="../Page/波士頓塞爾提克.md" title="wikilink">波士頓塞爾提克</a></p></td>
<td><p><a href="../Page/夏洛特.md" title="wikilink">夏洛特</a></p></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p><a href="../Page/多米尼克·威爾金斯.md" title="wikilink">多米尼克·威爾金斯</a></p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></p></td>
<td><p><a href="../Page/邁阿密.md" title="wikilink">邁阿密</a></p></td>
</tr>
<tr class="odd">
<td><p>1989</p></td>
<td><p><a href="../Page/肯尼·沃克.md" title="wikilink">肯尼·沃克</a></p></td>
<td><p><a href="../Page/纽约尼克斯.md" title="wikilink">纽约尼克斯</a></p></td>
<td><p><a href="../Page/休斯敦.md" title="wikilink">休斯敦</a></p></td>
</tr>
<tr class="even">
<td><p>1988</p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p><a href="../Page/芝加哥.md" title="wikilink">芝加哥</a></p></td>
</tr>
<tr class="odd">
<td><p>1987</p></td>
<td><p><a href="../Page/迈克尔·乔丹.md" title="wikilink">迈克尔·乔丹</a></p></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
<td><p><a href="../Page/西雅圖.md" title="wikilink">西雅圖</a></p></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td><p><a href="../Page/安東尼·韋布.md" title="wikilink">安東尼·韋布</a></p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></p></td>
<td><p><a href="../Page/達拉斯.md" title="wikilink">達拉斯</a></p></td>
</tr>
<tr class="odd">
<td><p>1985</p></td>
<td><p><a href="../Page/多米尼克·威爾金斯.md" title="wikilink">多米尼克·威爾金斯</a></p></td>
<td><p><a href="../Page/亞特蘭大老鷹.md" title="wikilink">亞特蘭大老鷹</a></p></td>
<td><p><a href="../Page/印第安纳波利斯.md" title="wikilink">印第安纳波利斯</a></p></td>
</tr>
<tr class="even">
<td><p>1984</p></td>
<td><p><a href="../Page/拉里·南斯.md" title="wikilink">拉里·南斯</a></p></td>
<td><p><a href="../Page/菲尼克斯太陽.md" title="wikilink">菲尼克斯太陽</a></p></td>
<td><p><a href="../Page/丹佛.md" title="wikilink">丹佛</a></p></td>
</tr>
</tbody>
</table>

## 精采度問題

由於NBA灌籃大賽已舉辦多年，因此也曾被批評越來越難看。

但在2016年NBA灌籃大賽中，[扎克·拉文和](../Page/扎克·拉文.md "wikilink")[阿隆·戈登的精采對決](../Page/阿隆·戈登.md "wikilink")，讓球迷有種「灌籃大賽要復活了」的感覺。而該屆也被評為近年來灌籃大賽最精彩的一屆。

在2017年NBA灌籃大賽中，已經奪下灌籃大賽2連霸的[扎克·拉文確定不會參加該屆的灌籃大賽](../Page/扎克·拉文.md "wikilink")，因此讓2016年灌籃大賽亞軍[阿隆·戈登被看好能夠奪冠](../Page/阿隆·戈登.md "wikilink")，是該屆呼聲最高的，在首輪中，[阿隆·戈登噱頭十足](../Page/阿隆·戈登.md "wikilink")，第一灌就使出無人機灌籃，不過高登卻連續3次失敗，直到第4次才成功，只得到38分。接下來的第二灌更是連續4次失敗，只得到34分。最終總分72分，在首輪遭到淘汰。而最終雖然由[格連·羅賓森三世奪冠](../Page/格連·羅賓森三世.md "wikilink")，但由於該屆選手狀態不好，發生大量灌籃失敗的情形，因此精采度不如上屆（2016年）。

## 參考資料

## 外部連結

  - [NBA Top
    Dunks](https://archive.is/20130102231245/http://www.goppo.com/nba)
  - [Year By Year
    Results](http://www.nba.com/history/allstar/slamdunk_year_by_year.html)
  - [Slam Dunk
    Records](http://www.nba.com/history/allstar/slamdunk_records.html)
  - [Slam Dunk contest video
    history](http://www.nba.com/history/allstar/slam_dunk_video.html)

[Category:NBA全明星賽](../Category/NBA全明星賽.md "wikilink")

1.  官方冠軍是東岸，约翰·沃尔則被評爲全場最佳球員