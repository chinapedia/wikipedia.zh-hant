**Mini-DVI**接口用於[蘋果公司的電腦上](../Page/蘋果公司.md "wikilink")，它是[Mini-VGA接口的數字替代接口](../Page/Mini-VGA.md "wikilink")。它的尺寸介於正常[DVI接口與](../Page/DVI.md "wikilink")[Micro-DVI接口之間](../Page/Micro-DVI.md "wikilink")。它被使用在12英吋[PowerBook
G4](../Page/PowerBook_G4.md "wikilink")，Intel-based
[iMac](../Page/iMac.md "wikilink")，[MacBook](../Page/MacBook.md "wikilink")
Intel-based筆記本以及Intel-based
[Xserve](../Page/Xserve.md "wikilink")。[MacBook
Air並不使用這種接口](../Page/MacBook_Air.md "wikilink")。用在蘋果電腦硬件上的Mini-DVI接口能夠通過不同的適配器通過[DDC檢測](../Page/Display_Data_Channel.md "wikilink")[EDID](../Page/Extended_display_identification_data.md "wikilink")（Extended
display identification
data），傳送DVI、[VGA或者電視信號](../Page/VGA.md "wikilink")。這種接口通常是用來代替[DVI接口以減少設備的物理尺寸大小](../Page/DVI.md "wikilink")。它需要一個Mini-DVI適配器來連接其他顯示器。Mini-DVI並不支持雙通道連接因此並不支持高於1920x1200
@60Hz的分辨率。

Mini-DVI適配器有以下幾種：

  -
    Apple Mini-DVI to VGA Adapter Apple part\#
    [1](https://web.archive.org/web/20080513030526/http://store.apple.com/1-800-MY-APPLE/WebObjects/AppleStore/?productLearnMore=M9320G%2FA)（M9320G/A）
    Apple Mini-DVI to Video Adapter Apple part\#
    [2](https://web.archive.org/web/20080513030521/http://store.apple.com/1-800-MY-APPLE/WebObjects/AppleStore/?productLearnMore=M9319G%2FA)（M9319G/A）
    Apple Mini-DVI to DVI Adapter (DVI-D) Apple part\#
    [3](https://web.archive.org/web/20090723124726/http://store.apple.com/1-800-MY-APPLE/WebObjects/AppleStore?productLearnMore=M9321G%2FA)（M9321G/A）在物理外觀上Mini-DVI接口與[Mini-VGA相似](../Page/Mini-VGA.md "wikilink")，不同的是其有四行針腳而不是Mini-VGA中的兩行。

如果需要使用DVI-I接口，必須把Mini-DVI to DVI-D線連接到一個DVI-D to DVI-I適配器上。

## 批評

對於蘋果公司的Mini-DVI接口，有一些批評的聲音。

  - 蘋果公司的Mini-DVI至DVI-D轉接線不能輸送由蘋果電腦上的mini-DVI接口傳來的模擬信號。這意味著當需要VGA輸出時，用戶不得不額外購買另外一條mini-DVI轉接線，而不能用一條mini-DVI加上一個便宜的DVI至VGA適配器來解決。如果蘋果公司能夠提供mini-DVI至DVI-I轉接線，這种情況完全可以得以解決。[DVI-I的作用就是提供通用的兼容性](../Page/DVI#Connector.md "wikilink")。

<!-- end list -->

  - 蘋果公司的mini-DVI至DVI-D轉接線的包裝上的圖顯示的是DVI-I而不是DVI-D，並且沒有說明其轉至DVI-D。

## 外部連結

  - [12-inch PowerBook G4 Developer
    Note：外部顯示端口](https://web.archive.org/web/20081211045421/http://developer.apple.com/documentation/Hardware/Developer_Notes/Macintosh_CPUs-G4/12inchPowerBookG4/3_Input-Output/chapter_4_section_15.html#//apple_ref/doc/uid/TP40001763-CH207-TPXREF119)

[Category:數碼顯示接口](../Category/數碼顯示接口.md "wikilink")
[Category:蘋果公司硬體](../Category/蘋果公司硬體.md "wikilink")