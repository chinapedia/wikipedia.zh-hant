**卡羅萊納長尾鸚鵡**
（[学名](../Page/学名.md "wikilink")：）是[美國東部唯一的本土](../Page/美國.md "wikilink")[鸚鵡品種](../Page/鸚形目.md "wikilink")。此鸚鵡被發現於[俄亥俄谷至](../Page/俄亥俄河.md "wikilink")[墨西哥灣一帶](../Page/墨西哥灣.md "wikilink")，一般居住於[河流或](../Page/河流.md "wikilink")[沼澤旁的柏樹及槭樹上](../Page/沼澤.md "wikilink")。卡羅萊納長尾鸚鵡已經宣告絕種。

## 絕種

卡羅萊納長尾鸚鵡絕種是有著幾個因素：農業發展奪去了大片森林面積，亦奪去了卡羅萊納長尾鸚鵡的生境；其彩色的[羽毛被用作裝飾帽子](../Page/羽毛.md "wikilink")；亦有人飼養卡羅萊納長尾鸚鵡作寵物；被迫與引入的蜜蜂競爭。雖然人工繁殖此雀鳥非常容易，但已馴養的卡羅萊納長尾鸚鵡卻沒有被大量[繁殖](../Page/繁殖.md "wikilink")。

隨著農地擴張，卡羅萊納長尾鸚鵡開始習慣食用穀物、水果等農作物，農民視之為害鳥，紛紛把牠們射殺，儘管卡羅萊納長尾鸚鵡可以控制具侵略性及毒性的[蒼耳植物](../Page/蒼耳.md "wikilink")。

導致卡羅萊納長尾鸚鵡絕種最致命的原因是其群居的習性。當一個地區內的同類數目減少，牠們很快就會飛回來補充，換言之，農民不斷地射殺，牠們卻繼續聚居。牠們甚至會在受傷或死去的同伴旁聚集，造成更多的鸚鵡被射殺。

在重重的因素打擊下，絕大部份的卡羅萊納長尾鸚鵡於20世紀初消失。最後一隻野生卡羅萊納長尾鸚鵡的屍體於1904年在美國[佛羅里達州](../Page/佛羅里達州.md "wikilink")[歐基求碧湖](../Page/歐基求碧湖.md "wikilink")（Lake
Okeechobee）被鳥類學家法蘭克切伯曼（）發現。[辛辛那堤動物園於](../Page/辛辛那堤動物園.md "wikilink")1880年代購入了16隻卡羅萊納長尾鸚鵡企圖進行人工繁殖，但沒有取得成功，動物園最後只餘下最後一對的卡羅萊納長尾鸚鵡，雌性的（取名「Lady
Jane」）在1917年死去，雄性的（取名「Incas」）亦於翌年1918年因悲傷而死。\[1\]

聲稱卡羅萊納長尾鸚鵡重現人間的說法一直持續至1930年代。

於1920年，有聲稱指在歐基求碧湖發現一群13隻的卡羅萊納長尾鸚鵡，2位著名的鳥類學家及於1936年出發搜尋。他們報告於[南卡羅萊納州的](../Page/南卡罗来纳州.md "wikilink")[桑蒂河](../Page/桑蒂河.md "wikilink")（）沿岸發現一群卡羅萊納長尾鸚鵡，但後來[奧杜邦學會駁回此報告](../Page/奧杜邦學會.md "wikilink")。無論報告屬是屬非，那個地區後來因發電計劃的興建工程而被破壞。1937年，一部黑白的家庭錄像在[喬治亞州南部的歐基求碧沼澤](../Page/喬治亞州.md "wikilink")（Okefenokee
Swamp）拍下了幾隻可能是卡羅萊納長尾鸚鵡，但奧杜邦學會研究過影片後，認為只是其他品種的長尾鸚鵡。\[2\]

## 参考文献

### 引用

### 来源

  - 國際雀鳥聯盟（）（2004）[](https://web.archive.org/web/20060223185932/http://www.redlist.org/search/details.php?species=5268)。2006年[世界自然保護聯盟瀕危物種紅色名錄絕種物種](../Page/世界自然保護聯盟瀕危物種紅色名錄.md "wikilink")。資料解釋了為何此物種被編入絕種級別

  - Snyder, N. F. R., and K. Russell (2002): "Carolina Parakeet
    (*Conuropsis carolinensis*)". In: The Birds of North America, No.
    667 (A. Poole and F. Gill, eds.). The Birds of North America, Inc.,
    Philadelphia, PA. <doi:10.2173/bna.667>

  -
## 外部連結

  - [The Carolina Parrot from John James Audubon's Birds of
    America](https://web.archive.org/web/20080317192753/http://www.ncmoa.org/collections/highlights/american/before1850/carolina_lrg.shtml)
  - [Carolina Parakeet](http://www.audubon.org/bird/boa/F28_G1a.html)
  - [The Endangered Species
    Handbook](https://web.archive.org/web/20121202091952/http://www.endangeredspecieshandbook.org/dinos_eastern.php)

[CC](../Category/IUCN絕滅物種.md "wikilink")
[CC](../Category/已滅絕鳥類.md "wikilink")
[CC](../Category/鹦鹉科.md "wikilink")
[CC](../Category/美國鳥類.md "wikilink")

1.  [The Eastern Forests Endangered Species Hankbook - It's too
    late](http://www.endangeredspecieshandbook.org/dinos_eastern.php)

2.