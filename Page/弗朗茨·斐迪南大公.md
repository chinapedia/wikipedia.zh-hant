**弗朗茨·斐迪南大公**（，），[奥匈帝国](../Page/奥匈帝国.md "wikilink")[皇储](../Page/皇储.md "wikilink")，[弗兰茨·约瑟夫一世皇帝之弟](../Page/弗兰茨·约瑟夫一世.md "wikilink")[卡爾·路德維希大公之长子](../Page/卡爾·路德維希大公.md "wikilink")。

[弗兰茨·约瑟夫一世的獨子皇太子](../Page/弗兰茨·约瑟夫一世.md "wikilink")[魯道夫於](../Page/魯道夫_\(奧匈帝國皇太子\).md "wikilink")[1889年1月30日自殺後](../Page/梅耶林惨案.md "wikilink")，他成為皇位繼承人。他因主张藉由兼并[塞尔维亚王国将](../Page/塞尔维亚王国.md "wikilink")[奥匈帝国由](../Page/奥匈帝国.md "wikilink")[奥地利](../Page/奥地利.md "wikilink")、[匈牙利组成的二元帝国扩展为由](../Page/匈牙利.md "wikilink")[奥地利](../Page/奥地利.md "wikilink")、[匈牙利与](../Page/匈牙利.md "wikilink")[南斯拉夫组成的三元帝国](../Page/南斯拉夫.md "wikilink")，所以於1914年与妻子苏菲视察时为奥匈帝国[波斯尼亚和黑塞哥维那的首府](../Page/波斯尼亚和黑塞哥维那.md "wikilink")[萨拉热窝](../Page/萨拉热窝.md "wikilink")，在[萨拉热窝被](../Page/萨拉热窝.md "wikilink")[塞尔维亚民族主义者](../Page/塞尔维亚.md "wikilink")[普林西普刺杀身亡](../Page/普林西普.md "wikilink")。“[萨拉热窝事件](../Page/萨拉热窝事件.md "wikilink")”成为[第一次世界大战的导火线](../Page/第一次世界大战.md "wikilink")。

## 子女

妻子：

1.  霍亨伯格郡主 - 苏菲（1901-1990），享年89岁。
2.  霍亨伯格公爵 - 迈西米利安（1902-1962），终年59岁。
3.  霍亨伯格王子 - 恩斯特（1904-1954），终年49岁。

因為[贵贱通婚](../Page/贵贱通婚.md "wikilink")，他们的子女没有皇位继承权\[1\]。

## 注释

<small>

<references/>

</small>

## 参见

  - [薩拉熱窩事件](../Page/薩拉熱窩事件.md "wikilink")

[Category:奥匈帝国皇储](../Category/奥匈帝国皇储.md "wikilink")
[Category:奥地利大公](../Category/奥地利大公.md "wikilink")
[Category:奧匈帝國將軍](../Category/奧匈帝國將軍.md "wikilink")
[Category:奧匈帝國海軍將領](../Category/奧匈帝國海軍將領.md "wikilink")
[Category:奧匈帝國第一次世界大戰人物](../Category/奧匈帝國第一次世界大戰人物.md "wikilink")
[Category:波希米亞王子](../Category/波希米亞王子.md "wikilink")
[Category:匈牙利王子](../Category/匈牙利王子.md "wikilink")
[Category:匈牙利天主教徒](../Category/匈牙利天主教徒.md "wikilink")
[Category:奧地利天主教徒](../Category/奧地利天主教徒.md "wikilink")
[Category:最神聖報喜勳章爵士](../Category/最神聖報喜勳章爵士.md "wikilink")
[Category:格拉茨人](../Category/格拉茨人.md "wikilink")
[Category:奥匈帝国遇刺身亡者](../Category/奥匈帝国遇刺身亡者.md "wikilink")
[Category:遇刺身亡的欧洲政治人物](../Category/遇刺身亡的欧洲政治人物.md "wikilink")
[Category:哈布斯堡-洛林王朝](../Category/哈布斯堡-洛林王朝.md "wikilink")

1.  1900年**弗朗茨·斐迪南大公**为获得[弗兰茨·约瑟夫一世批准其](../Page/弗兰茨·约瑟夫一世.md "wikilink")[婚姻](../Page/婚姻.md "wikilink")，曾承诺两人的后代放弃[奥匈帝国的皇位继承权](../Page/奥匈帝国.md "wikilink")。