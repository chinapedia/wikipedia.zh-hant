**铁青树科**共有14[属约](../Page/属.md "wikilink")103[种](../Page/种.md "wikilink")，分布在全世界的[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，[中国有](../Page/中国.md "wikilink")3属约5种，生长在[秦岭以南各地](../Page/秦岭.md "wikilink")。

本科[植物为](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")、[灌木或木质](../Page/灌木.md "wikilink")[藤本](../Page/藤本.md "wikilink")，单[叶互生](../Page/叶.md "wikilink")；[花两性](../Page/花.md "wikilink")，辐射对称，[花瓣](../Page/花瓣.md "wikilink")3－6；[果实为](../Page/果实.md "wikilink")[核果](../Page/核果.md "wikilink")、[坚果或](../Page/坚果.md "wikilink")[浆果](../Page/浆果.md "wikilink")。

1981年的[克朗奎斯特分类法包括有](../Page/克朗奎斯特分类法.md "wikilink")25属约250种，但1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为只有其中的](../Page/APG_分类法.md "wikilink")14属属于本[科](../Page/科.md "wikilink")，分为三个族。

## 属

### 铁青树族 Olacaceae

  - *[Dulacia](../Page/Dulacia.md "wikilink")*
  - [铁青树属](../Page/铁青树属.md "wikilink") *Olax*
  - *[Ptychopetalum](../Page/Ptychopetalum.md "wikilink")*

### 油籽树族 Aptandraceae

  - *[Anacolosa](../Page/Anacolosa.md "wikilink")*
  - [油籽树属](../Page/油籽树属.md "wikilink") *Aptandra*
  - *[Cathedra](../Page/Cathedra.md "wikilink")*
  - *[Chaunochiton](../Page/Chaunochiton.md "wikilink")*
  - *[Harmandia](../Page/Harmandia.md "wikilink")*
  - *[Ongokea](../Page/Ongokea.md "wikilink")*
  - *[Phanerodiscus](../Page/Phanerodiscus.md "wikilink")*

### 海檀木族 Ximeniaceae

  - *[Curupira](../Page/Curupira.md "wikilink")*
  - *[Douradoa](../Page/Douradoa.md "wikilink")*
  - [蒜头果属](../Page/蒜头果属.md "wikilink") *Malania*
  - [海檀木属](../Page/海檀木属.md "wikilink") *Ximenia*

## 外部链接

  - [被子植物信息网中的铁青树科](http://www.ars-grin.gov/cgi-bin/npgs/html/gnlist.pl?786)
  - [铁青树科](http://www.parasiticplants.siu.edu/Olacaceae/)
  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[铁青树科](http://delta-intkey.com/angio/www/olacacea.htm)
  - [NCBI中的铁青树科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=3973&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/铁青树科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")