**XAMPP**是一个把[Apache](../Page/Apache.md "wikilink")[网页服务器与](../Page/网页服务器.md "wikilink")[PHP](../Page/PHP.md "wikilink")、[Perl及](../Page/Perl.md "wikilink")[MariaDB集合在一起的](../Page/MariaDB.md "wikilink")[安裝包](../Page/软件包.md "wikilink")，允许用戶可以在自己的电脑上轻易的建立[网页服务器](../Page/网页服务器.md "wikilink")。

## 关于命名

XAMPP的名称来自以下组合：

  - **X**（支持跨平台）
  - [**A**pache](../Page/Apache_HTTP_Server.md "wikilink")
  - [**M**ySQL或](../Page/MySQL.md "wikilink")[**M**ariaDB](../Page/MariaDB.md "wikilink")
  - [**P**HP](../Page/PHP.md "wikilink")
  - [**P**erl](../Page/Perl.md "wikilink")

## 参见

  - [LAMP](../Page/LAMP.md "wikilink")

## 組件

XAMPP 7.2.12 Windows、Linux、Mac OSX版本，其包含了：

  - Apache 2.4.37
  - MariaDB 10.1.37
  - PHP 7.2.12
  - phpMyAdmin 4.8.3
  - FileZilla FTP 服务器 0.9.41
  - Tomcat 7.0.56 (自带mod_proxy_ajp连接器)
  - Strawberry Perl 7.0.56 移动版
  - XAMPP 控制台 3.2.2 (来自 hackattack142)

XAMPP 7.2.12 Windows版本，其包含了：

  - Apache 2.4.37
  - MariaDB 10.1.37
  - PHP 7.2.12
  - phpMyAdmin 4.8.3
  - FileZilla FTP 伺服器 0.9.41
  - Tomcat 7.0.56 (with mod_proxy_ajp as connector)
  - Strawberry Perl 7.0.56 移動版
  - XAMPP 控制台 3.2.2 (來自 hackattack142)
  - OpenSSL 1.1.0i

XAMPP 7.2.12 Linux版本，包含了：

  - Apache 2.4.37
  - MariaDB 10.1.37
  - PHP 7.2.12
  - phpMyAdmin 4.8.3
  - OpenSSL 1.0.2p

## 各版本组件详情

| XAMPP  | Apache | MySQL   | PHP 5  | PHP 4       |
| ------ | ------ | ------- | ------ | ----------- |
| 1.6.0  | 2.2.3  | 5.0.33  | 5.2.1  | 4.4.5       |
| 1.6.0a | 2.2.4  | 5.0.33  | 5.2.1  | 4.4.5       |
| 1.6.1  | 2.2.4  | 5.0.37  | 5.2.1  | 4.4.6       |
| 1.6.2  | 2.2.4  | 5.0.41  | 5.2.2  | 4.4.7       |
| 1.6.3  | 2.2.4  | 5.0.54  | 5.2.3  | 4.4.7       |
| 1.6.3a | 2.2.4  | 5.0.45  | 5.2.3  | 4.4.7       |
| 1.6.4  | 2.2.6  | 5.0.45  | 5.2.4  | 4.4.7       |
| 1.6.5  | 2.2.6  | 5.0.51  | 5.2.5  | 4.4.7       |
| 1.6.6  | 2.2.8  | 5.0.51  | 5.2.5  | 4.4.8 (RC2) |
| 1.6.6a | 2.2.8  | 5.0.51a | 5.2.5  | 4.4.8       |
| 1.6.7  | 2.2.9  | 5.0.51b | 5.2.6  | 4.4.8       |
| 1.6.8  | 2.2.9  | 5.0.67  | 5.2.6  | 4.4.9       |
| 1.7.0  | 2.2.11 | 5.1.30  | 5.2.8  | \-          |
| 1.7.1  | 2.2.11 | 5.1.33  | 5.2.9  | \-          |
| 1.7.2  | 2.2.12 | 5.1.37  | 5.3.0  | \-          |
| 1.7.3  | 2.2.14 | 5.1.41  | 5.3.1  | \-          |
| 1.7.4  | 2.2.17 | 5.5.8   | 5.3.5  | \-          |
| 1.7.5  | 2.2.21 | 5.5.15  | 5.3.8  | \-          |
| 1.7.7  | 2.2.21 | 5.5.16  | 5.3.8  | \-          |
| 1.8.0  | 2.4.2  | 5.5.25a | 5.4.4  | \-          |
| 1.8.1  | 2.4.3  | 5.5.27  | 5.4.7  | \-          |
| 1.8.2  | 2.4.10 | 5.5.39  | 5.4.31 | \-          |
| 1.8.3  | 2.4.10 | 5.6.20  | 5.5.15 | \-          |
| 5.5.19 | 2.4.10 | 5.6.21  | 5.5.19 | \-          |
| 5.6.3  | 2.4.10 | 5.6.21  | 5.6.3  | 5.1.0       |
| 5.6.4  | 2.4.10 | 5.6.21  | 5.6.3  | 5.2.0       |

## 外部連結

  - [简体中文官方主页](https://www.apachefriends.org/zh_cn/index.html)

  - [繁体中文官方主页](https://www.apachefriends.org/zh_tw/index.html)

  - [免安裝版主页](http://portableapps.com/apps/development/xampp)

  - [XAMPP安装](https://web.archive.org/web/20110215192243/http://www.leapsoul.cn/?p=275)

[Category:自由软件](../Category/自由软件.md "wikilink")