**摩德纳足球俱乐部**是[意大利](../Page/意大利.md "wikilink")[艾米利亚-罗马涅大区](../Page/艾米利亚-罗马涅.md "wikilink")[摩德纳的一家](../Page/摩德纳.md "wikilink")[足球俱乐部](../Page/足球俱乐部.md "wikilink")，成立于1912年。球队的颜色是黄色和蓝色。

摩德纳队最近一次在[意大利足球甲级联赛中参赛是在](../Page/意大利足球甲级联赛.md "wikilink")2003-04赛季，而在此之前球队上一次顶级联赛的经历要追溯到1964年。

摩德纳曾在1981年和1982年赢得[英格兰-意大利杯](../Page/英格兰-意大利杯.md "wikilink")，也是唯一一个曾经两次夺得该项赛事冠军的俱乐部。

## 著名球员

*仅包括为俱乐部出场100次以上，或参加过[世界杯足球赛的球员](../Page/世界杯足球赛.md "wikilink")。*

  - [马可·巴洛塔](../Page/马可·巴洛塔.md "wikilink")（Marco Ballotta）

  - [朱塞佩·巴雷西](../Page/朱塞佩·巴雷西.md "wikilink")（Giuseppe Baresi）

  - [雷纳托·布拉利亚](../Page/雷纳托·布拉利亚.md "wikilink")（Renato Braglia）

  - [雷纳托·布里格恩蒂](../Page/雷纳托·布里格恩蒂.md "wikilink")（Renato Brighenti）

  - [恩里克·基耶萨](../Page/恩里克·基耶萨.md "wikilink")（Enrico Chiesa）

  - [卢卡·托尼](../Page/卢卡·托尼.md "wikilink")（Luca Toni）

## 纪录

  - 出场次数最多的球员：[雷纳托·布拉利亚](../Page/雷纳托·布拉利亚.md "wikilink")（484次）
  - 进球数最大的球员：[雷纳托·布里格恩蒂](../Page/雷纳托·布里格恩蒂.md "wikilink")（82球）
  - 主场最大比分胜利：6比0胜[利沃诺](../Page/利沃诺足球俱乐部.md "wikilink")（1929-30赛季意甲）
  - 主场最大比分失败：0比5负[那不勒斯](../Page/那不勒斯足球俱乐部.md "wikilink")（1929-30赛季意甲）
  - 客场最大比分胜利：4比0胜[威尼斯](../Page/威尼斯足球俱乐部.md "wikilink")（1939-40赛季意甲）
  - 客场最大比分失败：1比9负[拉齐奥](../Page/拉齐奥足球俱乐部.md "wikilink")（1931-32赛季意甲）

## 相关参考

## 外部链接

  - [官方网站](https://web.archive.org/web/20110807234633/http://www.modenafc.net/)

[M](../Category/意大利足球俱樂部.md "wikilink")
[Category:艾米利亞-羅馬涅大區體育](../Category/艾米利亞-羅馬涅大區體育.md "wikilink")
[Category:1912年建立的足球俱樂部](../Category/1912年建立的足球俱樂部.md "wikilink")