**劳尔·荷西·特林达德·梅雷莱斯**（，），[葡萄牙足球运动员](../Page/葡萄牙.md "wikilink")，出生於[波尔图](../Page/波尔图.md "wikilink")，現效力[沙田超級聯賽球會](../Page/沙田超級聯賽.md "wikilink")[PCCW](../Page/PCCW.md "wikilink")。他主要司職中場中路位置，有時也負責中路防守。

## 生平

### 球會

美利拉斯於2010年夏天轉會市場以1,400萬[歐元加盟](../Page/歐元.md "wikilink")[利物浦](../Page/利物浦足球俱乐部.md "wikilink")。他加盟的上半季表現平庸，直到利物浦名宿[杜格利殊重新執教後](../Page/杜格利殊.md "wikilink")，美利拉斯漸漸適應[英超](../Page/英超.md "wikilink")，於2011年1月16日的[默西賽德郡打吡中射入加盟利物浦的第一個入球](../Page/默西賽德郡打吡.md "wikilink")，之後連續在對陣[狼隊](../Page/伍尔弗汉普顿流浪足球俱乐部.md "wikilink")、[史篤城](../Page/斯托克城足球俱乐部.md "wikilink")、[車路士和](../Page/切尔西足球俱乐部.md "wikilink")[韋根的賽事取得入球](../Page/威根竞技足球俱乐部.md "wikilink")。

2011年8月31日，美利拉斯向利物浦提出轉會申請，最終以1,500萬[英鎊加盟](../Page/英鎊.md "wikilink")[車路士](../Page/切尔西足球俱乐部.md "wikilink")\[1\]。

2012年4月4日，在歐冠盃八強對陣[賓菲加的賽事中](../Page/賓菲加.md "wikilink")，美利拉斯在92分鐘打入致勝一球，協助球隊以2-1擊敗對手，進入四強。

2012年9月4日，車路士在其官網宣佈葡萄牙中場魯爾梅利斯以800萬鎊加盟土超班霸[費倫巴治](../Page/費倫巴治.md "wikilink")。\[2\]。

### 國家隊

他在[2008年欧锦赛第一場分組賽中取得一個入球](../Page/2008年欧锦赛.md "wikilink")，助[葡萄牙國家隊以](../Page/葡萄牙國家足球隊.md "wikilink")2-0獲勝。

### 榮譽

#### 波圖

  - [葡超冠軍](../Page/葡萄牙足球超級聯賽.md "wikilink"): 2005–06, 2006–07, 2007–08,
    2008–09
  - [葡萄牙盃冠軍](../Page/葡萄牙盃.md "wikilink"): 2006, 2009, 2010
  - [葡萄牙超級盃冠軍](../Page/葡萄牙超級盃.md "wikilink"): 2004, 2006, 2009, 2010
  - [洲際盃](../Page/洲際盃.md "wikilink")（豐田盃）冠軍﹕2004

#### 車路士

  - [英格蘭足總盃冠軍](../Page/英格蘭足總盃.md "wikilink"): 2012
  - [歐冠盃冠軍](../Page/歐洲聯賽冠軍盃.md "wikilink"): 2012

#### 費倫巴治

  - [土耳其盃冠軍](../Page/土耳其盃.md "wikilink"): 2013
  - [土超冠軍](../Page/土耳其足球超級聯賽.md "wikilink"): 2013-14

#### 國家隊

  - [U-16歐洲足球錦標賽冠軍](../Page/U-16歐洲足球錦標賽.md "wikilink"): 2000

## 參考資料

## 外部链接

  - [Stats and profile at
    Zerozero](https://web.archive.org/web/20110425003134/http://www.zerozerofootball.com/jogador.php?id=517)

  - [Stats at
    ForaDeJogo](http://www.foradejogo.net/player.php?player=198303170001)


  - [PortuGOAL
    profile](http://www.portugoal.net/index.php/player-profiles/123-players-m-o/83-player-profile-raul-meireles)

  -
  - [LFC History
    profile](http://www.lfchistory.net/redcorner_articles_view.asp?article_id=3112)

  - [ESPN
    Profile](http://soccernet.espn.go.com/players/stats?id=71704&cc=5739)

  -
  - [Premier League
    profile](http://www.premierleague.com/page/PlayerProfile/0,,12306~31151,00.html)

[Category:葡萄牙足球运动员](../Category/葡萄牙足球运动员.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:博維斯塔球員](../Category/博維斯塔球員.md "wikilink")
[Category:艾維斯球員](../Category/艾維斯球員.md "wikilink")
[Category:波圖球員](../Category/波圖球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:費倫巴治球員](../Category/費倫巴治球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會足球運動員](../Category/2004年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")

1.
2.