**凯茜·弗里曼**（Cathy Freeman），全名**凯瑟琳·阿斯特里德·莎乐美·弗里曼**（Catherine Astrid
Salome
Freeman，）是[澳大利亚](../Page/澳大利亚.md "wikilink")[土著](../Page/澳洲原住民.md "wikilink")[田径运动员](../Page/田径.md "wikilink")。弗里曼於1996年[亞特蘭大奧運奪得](../Page/亞特蘭大奧運.md "wikilink")400公尺銀牌，隨後在2000年[悉尼奧運中奪得](../Page/悉尼奧運.md "wikilink")400公尺金牌。

## 參考資料

## 外部連結

  -
  - [官方网站](http://www.cathyfreeman.com.au)

  - [Powerhouse Museum
    collection](http://www.powerhousemuseum.com/collection/database/index.php?irn=10767&search=2001%2F84%2F267&images=&c=&s=)

[Category:澳大利亚田径运动员](../Category/澳大利亚田径运动员.md "wikilink")
[Category:澳大利亚奥林匹克运动会金牌得主](../Category/澳大利亚奥林匹克运动会金牌得主.md "wikilink")
[Category:澳大利亚奥林匹克运动会银牌得主](../Category/澳大利亚奥林匹克运动会银牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:奥林匹克勋章获得者](../Category/奥林匹克勋章获得者.md "wikilink")
[Category:澳大利亚原住民人物](../Category/澳大利亚原住民人物.md "wikilink")
[Category:1992年夏季奧林匹克運動會田徑運動員](../Category/1992年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:1996年夏季奧林匹克運動會田徑運動員](../Category/1996年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2000年夏季奧林匹克運動會田徑運動員](../Category/2000年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:奥林匹克运动会田径金牌得主](../Category/奥林匹克运动会田径金牌得主.md "wikilink")
[Category:奥林匹克运动会田径银牌得主](../Category/奥林匹克运动会田径银牌得主.md "wikilink")
[Category:世界田徑錦標賽獎牌得主](../Category/世界田徑錦標賽獎牌得主.md "wikilink")
[Category:英格蘭裔澳大利亞人](../Category/英格蘭裔澳大利亞人.md "wikilink")
[Category:勞倫斯世界體育獎得主](../Category/勞倫斯世界體育獎得主.md "wikilink")
[Category:澳大利亚华人](../Category/澳大利亚华人.md "wikilink")
[Category:英联邦运动会澳大利亚金牌得主](../Category/英联邦运动会澳大利亚金牌得主.md "wikilink")
[Category:英联邦运动会澳大利亚参赛选手](../Category/英联邦运动会澳大利亚参赛选手.md "wikilink")