[Lihu_Park.jpg](https://zh.wikipedia.org/wiki/File:Lihu_Park.jpg "fig:Lihu_Park.jpg")
[thumb](../Page/image:The_light_of_Lihu_lake.jpg.md "wikilink")
**蠡湖公园**是位于中国[江苏省](../Page/江苏省.md "wikilink")[无锡市](../Page/无锡市.md "wikilink")[滨湖区](../Page/滨湖区.md "wikilink")[蠡湖北岸](../Page/蠡湖.md "wikilink")、[蠡湖大桥西北侧的一座开放式免费公园](../Page/蠡湖大桥.md "wikilink")，2005年10月1日起正式对外开放。该园占地面积300亩，由美国泛亚易道公司和无锡园林设计所合作设计。园内以植物造景为主，建有“春之媚”、“夏之秀”、“秋之韵”、“冬之凝”四季花木林带。环水而筑圆形百米长廊——水镜廊，展示古今赞美[太湖的名人](../Page/太湖.md "wikilink")[诗词](../Page/诗词.md "wikilink")、[绘画](../Page/绘画.md "wikilink")、[雕塑精品](../Page/雕塑.md "wikilink")。\[1\]\[2\]\[3\]\[4\]

## 参考来源

## 另见

  - [蠡园](../Page/蠡园.md "wikilink")
  - [蠡湖](../Page/蠡湖.md "wikilink")

[Category:无锡公园](../Category/无锡公园.md "wikilink")
[Category:滨湖区](../Category/滨湖区.md "wikilink")

1.
2.
3.
4.