**Aice<sup>5</sup>**是[日本的一個](../Page/日本.md "wikilink")[聲優組合](../Page/聲優.md "wikilink")，由[堀江由衣所發起](../Page/堀江由衣.md "wikilink")，隶属于[Starchild](../Page/Starchild.md "wikilink")（[Kingrecords旗下公司](../Page/Kingrecords.md "wikilink")）。團體於2005年尾出道，在2007年9月20日舉行最後一場演唱會並解散。

於2015年7月17日宣佈再次展開活動，並參演[淺野真澄原作的動畫](../Page/淺野真澄.md "wikilink")《[那就是聲優！](../Page/那就是聲優！.md "wikilink")》。

## 成員

  - [堀江由衣](../Page/堀江由衣.md "wikilink")（**組長**，代表色水色）
  - [高橋智秋](../Page/高橋智秋.md "wikilink")（代表色紫色）
  - [神田朱未](../Page/神田朱未.md "wikilink")（代表色[紅色](../Page/紅色.md "wikilink")）
  - [淺野真澄](../Page/淺野真澄.md "wikilink")（代表色綠色）
  - [木村圓](../Page/木村圓.md "wikilink")（代表色[黃色](../Page/黃色.md "wikilink")）

## 唱片目录

### 单曲

<table>
<thead>
<tr class="header">
<th></th>
<th><p>名稱</p></th>
<th><p>發售日</p></th>
<th><p>販売生産番号</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><strong><a href="../Page/Get_Back_(Aice5單曲).md" title="wikilink">Get Back</a></strong></p></td>
<td><p>2006年3月11日</p></td>
<td><p>SCRF-19</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><strong><a href="../Page/Believe_My_Love/友情物語.md" title="wikilink">Believe My Love/友情物語</a></strong></p></td>
<td><p>2006年5月24日</p></td>
<td><p>KICM-1165<br />
KICM-91165（初回限定盤）</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><strong><a href="../Page/Love_Power.md" title="wikilink">Love Power</a></strong></p></td>
<td><p>2006年10月25日</p></td>
<td><p>KICM-1181<br />
KICM-91181（初回限定盤）</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><strong><a href="../Page/Brand_new_day_(Aice5單曲).md" title="wikilink">Brand new day</a></strong></p></td>
<td><p>2007年4月25日</p></td>
<td><p>KICM-1201</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><strong><a href="../Page/Letter_(Aice5單曲).md" title="wikilink">Letter</a></strong></p></td>
<td><p>2007年4月29日</p></td>
<td><p>NMAX-80001</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><strong><a href="../Page/Re.MEMBER.md" title="wikilink">Re.MEMBER</a></strong></p></td>
<td><p>2007年9月5日</p></td>
<td><p>KICM-1214<br />
KICM-91215（初回限定盤）</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 專輯

|   | 名稱                                              | 發售日        | 販売生産番号    |
| - | ----------------------------------------------- | ---------- | --------- |
| 1 | '''\[\[Love_Aice5|Love Aice<sup>5\]\]'''</sup> | 2007年2月14日 | KICS-1293 |

### DVD

|   | 名稱                                                                                                                             | 發售日                                                                  | 販売生産番号       |
| - | ------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------- | ------------ |
| 1 | '''\[\[Aice5_1st_Tour_2007_"Love_Aice5"_〜Tour_Final                                                                     | 〜|Aice<sup>5</sup> 1st Tour 2007 "Love Aice<sup>5</sup>" 〜Tour Final | 〜\]\]'''     |
| 2 | **[Aice<sup>5</sup> Final Party LAST Aice<sup>5</sup> in 橫濱體育館](../Page/Aice5_Final_Party_LAST_Aice5_in_橫濱體育館.md "wikilink")** | 2008年2月14日                                                           | KIBM-161〜162 |

## 出演

### 雜誌

  - Aice<sup>5</sup> In
    Wonderland（[主婦の友社](../Page/主婦の友社.md "wikilink")「[SEIGURA](../Page/SEIGURA.md "wikilink")」）
  - Music
    Aice<sup>5</sup>（[音楽専科社](../Page/音楽専科社.md "wikilink")「hm<sup>3</sup>
    SPECIAL」）
  - Aice<sup>5</sup>物語
    〜世界Aice<sup>5</sup>が始まった日〜（[角川書店](../Page/角川書店.md "wikilink")「[Newtype](../Page/Newtype_\(雜誌\).md "wikilink")」）

### 廣播

  - [Aice<sup>5</sup> In Wonder
    RADIO](../Page/Aice5_In_Wonder_RADIO.md "wikilink")

### Live

  - Aice5 1st Tour 2007 "Love Aice5"|Aice<sup>5</sup> 1st Tour "Love
    Aice<sup>5</sup>"
  - Aice5 Final Party "Last Aice5"|Aice<sup>5</sup> Final Party "Last
    Aice<sup>5</sup>"

[Category:日本女子演唱團體](../Category/日本女子演唱團體.md "wikilink")
[Category:2005年成立的音樂團體](../Category/2005年成立的音樂團體.md "wikilink")
[Category:2007年解散的音樂團體](../Category/2007年解散的音樂團體.md "wikilink")
[Category:堀江由衣](../Category/堀江由衣.md "wikilink")
[Category:Aice5](../Category/Aice5.md "wikilink")