**乔治·伊登，第一代奥克兰伯爵**，[GCB](../Page/巴斯勋章.md "wikilink")、[PC](../Page/英国枢密院.md "wikilink")（**George
Eden, 1st Earl of Auckland**，1784年8月25日 -
1849年1月1日），英国[辉格党政治家](../Page/辉格党_\(英国\).md "wikilink")、殖民地官员。曾三次出任[第一海军大臣](../Page/第一海军大臣.md "wikilink")，并在1836年到1842年间出任[印度总督](../Page/印度总督.md "wikilink")。[紐西蘭第二任首都](../Page/紐西蘭.md "wikilink")[奧克蘭以他命名](../Page/奧克蘭.md "wikilink")。

## 生平

奥克兰生于[肯特郡](../Page/肯特郡.md "wikilink")[贝肯纳姆](../Page/贝肯纳姆.md "wikilink")，父亲是[威廉·伊登，第一代奥克兰男爵](../Page/威廉·伊登，第一代奥克兰男爵.md "wikilink")（William
Eden, 1st Baron Auckland），母亲是第三代从男爵吉尔伯特·埃利奥特爵士(Gilbert
Elliot)之女埃莉诺（Eleanor）。\[1\]奥克兰的姊妹是女旅行家、作家艾米莉·伊登（Emily
Eden），她曾经在印度游览一段时间，并记下了见闻。奥克兰起初受教于[伊顿公学](../Page/伊顿公学.md "wikilink")，然后进入[牛津基督学院](../Page/牛津基督学院.md "wikilink")，最后，在1809年入读[林肯律师学院](../Page/英國律師學院.md "wikilink")。\[2\]1810年，他的长兄威廉·伊登（William
Eden）溺毙于[泰晤士河中](../Page/泰晤士河.md "wikilink")，所以他在1814年继承父亲的[男爵爵位](../Page/男爵.md "wikilink")。

1809年，奥克兰继承了长兄在国会代表[伍德斯托克的席位](../Page/伍德斯托克_\(牛津郡\).md "wikilink")。他把持这个席位到1812年。在1813年，他再次取得这个席位，这次把持到1814年。\[3\]后来，因为他继承了父亲的爵位，所以他取得了[上议院席位](../Page/英国上议院.md "wikilink")，并在上议院中支持改革派。1830年，他出任[英国贸易委员会主席](../Page/英国贸易委员会.md "wikilink")、[造币局局长](../Page/英国造币局.md "wikilink")，时任首相为[格雷伯爵](../Page/查爾斯·格雷，第二代格雷伯爵.md "wikilink")。\[4\]

他在1834年到1835年出任[第一海军大臣](../Page/第一海军大臣.md "wikilink")，时任首相分别为格雷伯爵和[墨爾本勋爵](../Page/威廉·蘭姆，第二代墨爾本子爵.md "wikilink")。\[5\]第一海军大臣任内，他委任威廉·霍布森（William
Hobson）航往[东印度](../Page/东印度.md "wikilink")，霍布森为了纪念他，在1840年将一座新西兰城镇命名为[奥克兰](../Page/奧克蘭都會區.md "wikilink")。澳大利亚的[伊登和](../Page/新南威尔士伊登.md "wikilink")[奥克兰郡也以他为名](../Page/奥克兰郡.md "wikilink")。

1835年，奥克兰被任命为[印度总督](../Page/印度总督.md "wikilink")，次年1月到[加尔各答](../Page/加尔各答.md "wikilink")。\[6\]他的私人秘书是约翰·罗素·科尔文（John
Russell Colvin），被他提拔为印度西北省份的副总督，而科尔文的儿子名从奥克兰，即奥克兰·科尔文（Auckland
Colvin）。在总督任内，奥克兰改善当地教育，发展地方商业。

他为扩大英国在中亚的贸易和影响，谋求与[阿富汗的统治者](../Page/阿富汗.md "wikilink")[多斯特·穆罕默德·汗](../Page/多斯特·穆罕默德·汗.md "wikilink")（Dost
Mahommed
Khan）订立通商条约。由于俄国人和波斯人的阻碍，他改而起用多斯特的对手，投靠英国的[沙阿·苏查](../Page/沙阿·苏查.md "wikilink")。奥克兰牢牢掌握阿富汗，到1839年苏查已经控制了[喀布尔和](../Page/喀布尔.md "wikilink")[坎大哈](../Page/坎大哈.md "wikilink")。

但是，他在阿富汗工作时出现了病症，时为1838年。1838年10月1日，为了推翻多斯特，奥克兰在[西姆拉发表](../Page/西姆拉.md "wikilink")[西姆拉宣言](../Page/西姆拉宣言.md "wikilink")（Simla
Manifesto），向多斯特·穆罕默德汗开战。在经过成功的初期作战后，1839年他获封[奥克兰伯爵](../Page/奥克兰伯爵.md "wikilink")。\[7\]但他削减对部族首脑的补贴，引起骚乱。到1841年冬英军从喀布尔撤退时，被击毙和俘虏者达5000名。[第一次英阿战争最后以失败告终](../Page/第一次英阿战争.md "wikilink")。1842年奥克兰被召回国，他把总督职位移交给[埃伦伯勒勋爵](../Page/埃伦伯勒勋爵.md "wikilink")，然后回到了英国。他虽然在阿富汗遭遇失败，但作为印度总督，扩大农业灌溉，救济饥荒，发展教育和职业训练，不失为优秀的行政官员。

1846年，他再次出任第一海军大臣一职，这次时任首相是[罗素勋爵](../Page/约翰·罗素，第一代罗素伯爵.md "wikilink")。\[8\]在三年后，他在第一海军大臣任内去世。

## 个人生活

奥克兰死于1849年元旦，享年64。\[9\]他终身未婚，所以无人继承伯爵爵位，而男爵爵位则由较年幼兄弟罗伯特（Robert）继承。\[10\]

## 参考

## 注脚

<references/>

[Category:1784年出生](../Category/1784年出生.md "wikilink")
[Category:1849年逝世](../Category/1849年逝世.md "wikilink")
[Category:印度總督](../Category/印度總督.md "wikilink")
[Category:英國政治人物](../Category/英國政治人物.md "wikilink")
[E](../Category/伊頓公學校友.md "wikilink")
[E](../Category/牛津大學基督堂學院校友.md "wikilink")

1.  [The Peerage.com](http://thepeerage.com/p3491.htm#i34909)

2.
3.
4.
5.
6.
7.
8.
9.
10.