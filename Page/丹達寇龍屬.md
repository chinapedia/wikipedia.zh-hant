**丹達寇龍屬**（[學名](../Page/學名.md "wikilink")：*Dandakosaurus*）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，化石是在[印度](../Page/印度.md "wikilink")[安得拉邦發現](../Page/安得拉邦.md "wikilink")，年代為[早侏羅紀的](../Page/侏羅紀.md "wikilink")[托阿爾階](../Page/托阿爾階.md "wikilink")，約1億8300萬到1億7500萬年前。牠現時被分類為[新角鼻龍類的未定位屬](../Page/新角鼻龍類.md "wikilink")，但有可能是種[角鼻龍科恐龍或基礎](../Page/角鼻龍科.md "wikilink")[堅尾龍類恐龍](../Page/堅尾龍類.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")**印度丹達寇龍**（*D.
indicus*），是由Yadagiri於1982年命名。丹達寇龍的[化石只有一部份的](../Page/化石.md "wikilink")[骨盆](../Page/骨盆.md "wikilink")，故此對牠所知甚少，而有些[古生物學家認為牠其實是](../Page/古生物學家.md "wikilink")[疑名](../Page/疑名.md "wikilink")。

## 外部連結

  - [恐龍博物館——丹達寇龍](https://web.archive.org/web/20070612062812/http://www.dinosaur.net.cn/museum/Dandakosaurus.htm)

[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:堅尾龍類](../Category/堅尾龍類.md "wikilink")
[Category:印度與馬達加斯加恐龍](../Category/印度與馬達加斯加恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")