[Afghanistan_demography.svg](https://zh.wikipedia.org/wiki/File:Afghanistan_demography.svg "fig:Afghanistan_demography.svg")[**阿富汗的語言分布圖**
           
](https://zh.wikipedia.org/wiki/File:Languages_of_afghanistan-provinces.jpg "fig:阿富汗的語言分布圖             ")

阿富汗人口，混雜著形形色色的民族和語言。會導致如此複雜，皆反映在她所在的位置。處於[中亞](../Page/中亞.md "wikilink")、[南亞](../Page/南亞.md "wikilink")、[西亞的](../Page/西亞.md "wikilink")，歷史上各種商業交易與入侵的要衝和並經之地。
[普什圖人](../Page/普什圖人.md "wikilink") 是最主要的民族組成，大約佔了總人口數的四十二個百分比（42%）。
以及其他如：[塔吉克人](../Page/塔吉克.md "wikilink")(27%)，[哈扎拉族](../Page/哈扎拉族.md "wikilink")(9%)，[烏茲別克人](../Page/烏茲別克.md "wikilink")(9%)，[艾马克人](../Page/艾马克人.md "wikilink")(4%)
，[土庫曼人](../Page/土庫曼.md "wikilink")(3%)，[俾路支人](../Page/俾路支人.md "wikilink")(4%)，和其他少數民族(約4%)，共同組成了剩下人口的五十八個百分比（58%）。
[普什圖語和](../Page/普什圖語.md "wikilink")[达里波斯語都是](../Page/达里波斯語.md "wikilink")[阿富汗的官方語言](../Page/阿富汗.md "wikilink")。

## 人口

**总计:**

  - 32,738,376 (2008年)

**[年龄结构](../Page/年龄结构.md "wikilink"):**

  - 0-14岁: 44.6% (男性7,474,394; 女性7,121,145)
  - 15-64岁: 53% (男性8,901,880; 女性8,447,983)
  - 65岁以上: 2.4% (男性383,830; 女性409,144) (2008年)

**[年龄中位数](../Page/年龄中位数.md "wikilink"):**

  - 共计: 17.6岁
  - 男性: 17.6岁
  - 女性: 17.6岁(2008年)

**人口增长率:**

  - 2.626%(2008年)

**出生率:**

  - 45.82/1,000 人(2008年)

**死亡率:**

  - 19.56 /1,000 人(2008年)

**净迁移率:**

  - 21/1,000 人(2005年)

**性别比:**

  - 出生: 1.05 男性/女性
  - 15岁以下: 1.05男性/女性
  - 15-64岁: 1.05 男性/女性
  - 65岁以上: 0.94 男性/女性
  - 总计: 1.05 男性/女性 (2008年)

**婴儿死亡率:**

  - 共计: 154.67/1,000出生婴儿
  - 女性: 150.24/1,000出生婴儿(2008年)
  - 男性: 158.88/1,000出生婴儿

**平均寿命:**

  - 总计: 44.21岁
  - 男性: 44.04岁
  - 女性: 44.39岁(2008年)

**总出生率:**

  - 6.58 婴儿/妇女 (2008年)

**[艾滋病成人流行率](../Page/艾滋病.md "wikilink"):**

  - 0.01% (2001年)

**主要传染病：**

  - 危险程度：高
  - 食品和水传播疾病：[A型肝炎](../Page/A型肝炎.md "wikilink")、[伤寒](../Page/伤寒.md "wikilink")、[痢疾](../Page/痢疾.md "wikilink")
  - 细菌疾病：全国200米(公尺)以下高度，3月至11月為[疟疾高发期](../Page/疟疾.md "wikilink")
  - 动物接触疾病：[狂犬病](../Page/狂犬病.md "wikilink")

## 民族

*参看[阿富汗民族](../Page/阿富汗民族.md "wikilink")*

[普什图族占](../Page/普什图族.md "wikilink")42%，[塔吉克族占](../Page/塔吉克族.md "wikilink")27%，[哈扎拉族](../Page/哈扎拉族.md "wikilink")9%，[乌兹别克族](../Page/乌兹别克族.md "wikilink")9%,[恰拉马克族](../Page/恰拉马克族.md "wikilink")4%，[土库曼族](../Page/土库曼族.md "wikilink")3%，[俾路支族](../Page/俾路支族.md "wikilink")2%，其它4%，还有少数[阿拉伯聖战者后人](../Page/阿拉伯.md "wikilink")。

## 宗教

[逊尼派穆斯林](../Page/逊尼派.md "wikilink")80%,
[什叶派穆斯林](../Page/什叶派.md "wikilink")19%, 其它1%

## 语言

[普什图语](../Page/普什图语.md "wikilink")35%和[达里波斯语](../Page/达里波斯语.md "wikilink")50%是[官方语言](../Page/官方语言.md "wikilink")，

其他还有[土耳其语](../Page/土耳其语.md "wikilink")(主要乌兹别克族和[土库曼族](../Page/土库曼族.md "wikilink"))
11%,
还有30种较小语言(主要是[俾路支语和](../Page/俾路支语.md "wikilink")[波沙语](../Page/波沙语.md "wikilink"))
4%, 大多数人使用双语。

**识字能力:**

  - 定义: 15岁以上可以读写
  - 女性: 12.6% (2000年)
  - 男性: 43.1%
  - 总计: 28.1%

## 难民

在2001年10月估计有4百万[难民](../Page/难民.md "wikilink"), 已经有2、3百万返回。

## 參考

  - [阿富汗 -
    美国中情局《世界概况》](https://www.cia.gov/library/publications/the-world-factbook/geos/af.html)

[\*](../Category/阿富汗人口.md "wikilink")