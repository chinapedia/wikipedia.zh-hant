**cω**是一種尚在研究中的[程序设计语言](../Page/程序设计语言.md "wikilink")，專門應用於[平行運算的編程語言](../Page/平行運算.md "wikilink")。這種語言由[微軟研究院開發](../Page/微軟研究院.md "wikilink")。

根據微軟研究所，Cω是一種用作擴充[C\#的試驗性的語言](../Page/C♯.md "wikilink")，內裏包括新建的關聯性和準結構性的資料存取，與及異步並發計算。Cω主要在兩大範圍上擴充C\#：

  - 一種針對「非同步式寬域同作」（asynchronous wide-area concurrency）的控制流程延伸（如
    Polyphonic C\#）。
  - 一種針對 [XML](../Page/XML.md "wikilink") 和表格處理（table
    manipulation）的資料型別延伸（如著名的 Xen 和 X\#）。

2004年10月28日在compiler preview首度公開發表。

## 外部連結

  - [Cω在微軟研究所的簡介](http://research.microsoft.com/Comega/)

[Category:C語言](../Category/C語言.md "wikilink")
[Category:C語言家族](../Category/C語言家族.md "wikilink")