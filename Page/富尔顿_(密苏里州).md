**富尔顿**（****）位于[美国](../Page/美国.md "wikilink")[密苏里州中部](../Page/密苏里州.md "wikilink")，是[卡勒韦县的县治所在](../Page/卡勒韦县_\(密苏里州\).md "wikilink")，根据[美国人口调查局](../Page/美国人口调查局.md "wikilink")2000年统计，共有人口12,128人，其中[白人占](../Page/白人.md "wikilink")81.26%、[非裔美国人占](../Page/非裔美国人.md "wikilink")15.44%、[亚裔美国人占](../Page/亚裔美国人.md "wikilink")1.06%。

1946年3月5日，[英国前首相](../Page/英国首相.md "wikilink")[温斯顿·丘吉尔在富尔顿](../Page/温斯顿·丘吉尔.md "wikilink")[威斯敏斯特学院发表了著名的](../Page/威斯敏斯特学院_\(密苏里州\).md "wikilink")“[铁幕](../Page/铁幕.md "wikilink")”演讲。

[F](../Category/密苏里州城市.md "wikilink")