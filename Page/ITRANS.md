**印度語言轉寫**（**I**ndian languages **TRANS**literation 簡寫為
**ITRANS**）是[印度文字](../Page/婆羅米系文字.md "wikilink")，特別但不限於[天城文](../Page/天城文.md "wikilink")（用於[印度語](../Page/印度語.md "wikilink")、[馬拉地語](../Page/馬拉地語.md "wikilink")、[梵語](../Page/梵語.md "wikilink")、[尼泊爾語](../Page/尼泊爾語.md "wikilink")、[信德語和其他語言](../Page/信德語.md "wikilink")）的
[ASCII](../Page/ASCII.md "wikilink") 碼[轉寫](../Page/轉寫.md "wikilink")。它由
Avinash Chopde 開發。最新版本 ITRANS 5.30 是2001年七月的發布。ITRANS 在這個版本已經穩定了。

ITRANS 有時用來編碼印度的電子文本 - 它的範圍比
[Harvard-Kyoto](../Page/Harvard-Kyoto.md "wikilink")
的天城體轉寫方案要廣闊，在很大程度上但非完全和它保持一致。隨著
[Unicode](../Page/Unicode.md "wikilink") 的廣泛實現，傳統
[IAST](../Page/IAST.md "wikilink") 日益廣泛的用於了電子文本。

類似 [Harvard-Kyoto](../Page/Harvard-Kyoto.md "wikilink") 方案，ITRANS
[羅馬化不使用在日常英語中計算機鍵盤上找不到的變音符號](../Page/羅馬化.md "wikilink")，它非常容易讀取和錄入。

ITRANS 計算機程序包還確保可以從羅馬文字自動轉換成印度文字。

## 轉寫方案

元音（依賴和獨立）:

`a     aa / A       i      ii / I       u     uu / U `
`RRi / R^i    RRI / R^I    LLi / L^i    LLI / L^I`
`e     ai     o     au     aM    aH`

輔音：（只用於表示輔音部分。天城體字母還包括了默認的 'a' 音。如果需要它，必須明確的寫出。）

`k     kh     g     gh     ~N / N^`
`ch    Ch     j     jh     ~n / JN`
`T     Th     D     Dh     N`
`t     th     d     dh     n`
`p     ph     b     bh     m`
`y     r      l     v / w`
`sh    Sh     s     h      L / ld`
`x / kSh     GY / j~n / dny     shr`
`R (用於 marathi half-RA)`
`L / ld (marathi LLA)`
`Y (bengali)`

有 nukta（點）在左下角的輔音（主要用於 Urdu Devanāgarī）:

`k  with a dot:      q`
`kh with a dot:      K`
`g  with a dot:      G`
`j  with a dot:      z / J`
`p  with a dot:      f`
`D  with a dot:      .D`
`Dh with a dot:      .Dh`

特殊／重音：

`Anusvara:           .n / M / .m  `
`Avagraha (元音省略): .a    `
`Ardhachandra:       .c   `
`Chandra-Bindu:      .N   `
`Halant:             .h   `
`Visarga:            H     `
`Om (Om 符號):     OM, AUM`

## 參見

  - [天城文轉寫](../Page/天城文轉寫.md "wikilink")
  - [IAST](../Page/IAST.md "wikilink")
  - [Harvard-Kyoto](../Page/Harvard-Kyoto.md "wikilink")
  - [加爾各答國家圖書館羅馬化](../Page/加爾各答國家圖書館羅馬化.md "wikilink")

## 外部連結

  - [ITRANS Official site](http://www.aczoom.com/itrans/)
  - [HiTrans - Online ITRANS to Unicode converter with scheme
    extensions](http://www.giitaayan.com/x.htm)
  - [Online Interface to ITRANS - ITRANS to GIF, PS, PDF and
    HTML](http://209.6.178.5:8080/cgi-bin/webitrans.pl)
  - [site on ITRANS and religious content in
    ITRANS](http://trchari.tripod.com/itrans1.html)
  - [View Unicode Hindi through Roman transliteration (ITRNS
    scheme)](http://ccat.sas.upenn.edu/plc/tamilweb/trans/itransunicode.html)
  - [Downloadable ITRANS to Unicode
    transformer](http://ash.banerjee.googlepages.com/dev2uni.html) A
    simple Java applet demo, with source code. Uses a simple table based
    extendable algorithm.
  - [Google Indic
    Transliteration](http://www.google.com/transliterate/indic/) Online
    Indic Transliteration by Google
  - [1](https://archive.is/20051217201509/http://devendraparakh.port5.com/)
    Devendra Parakh's Hindi word processor site
  - [Itranslator 2003 as a freeware from Onkarananda Ashram
    Himalayas](http://www.omkarananda-ashram.org/Sanskrit/Itranslt.html)

[Category:转写系统](../Category/转写系统.md "wikilink")
[Category:梵語](../Category/梵語.md "wikilink")