**湯·芬尼**[爵士](../Page/爵士.md "wikilink")（，1922年4月5日－2014年2月14日），生於[英國](../Page/英國.md "wikilink")[普雷斯頓](../Page/普雷斯頓.md "wikilink")，已故退休[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。

湯·芬尼自小便以職業足球員為目標，因此在14歲時加入了[普雷斯頓成為學徒球員](../Page/普雷斯顿足球俱乐部.md "wikilink")。不過，因於家庭環境關係，他除出席球會的訓練之餘，平時又要在一間鉛工廠當學徒幫補家計。湯·芬尼和馬菲士可稱得上是同一類型的球員，兩人同樣擁有純熟的盤扭技巧和極高速度，而湯·芬尼更是「雙槍將」一名，再加上他那炮彈式射門，雖然未曾協助[英格蘭國家隊和普雷斯顿贏得任何獎項](../Page/英格蘭足球代表隊.md "wikilink")，但也配稱為英格蘭史上最出色的球員之一。他也被身邊的人稱他為「完美的鉛工」（*The
Perfect Plumber*），表是他無論是工作和足球場上同樣是實而不華。

## 1940年代

1940年，[第二次世界大戰爆發](../Page/第二次世界大戰.md "wikilink")，令原本有機會首次在當時的英格蘭頂級聯賽上陣的湯·芬尼等了6年。大戰期間，他和很多英國年青人一樣踏上無情的戰場。當時他在[北非第八中隊服兵役](../Page/北非.md "wikilink")，期間他不時為軍部的足球學校上陣比賽，由於表現出色，隨即成為軍部的足球明星。1946年，[第二次世界大戰終於結束](../Page/第二次世界大戰.md "wikilink")，湯·芬尼亦可為普雷斯頓上陣。這時，英格蘭國家隊亦開始重上軌道，湯·芬尼自然入選其中。不過，他和馬菲士同樣屬於右翼球員，令多名英格蘭領隊應以誰人當正選而大傷腦筋。後來，由於湯·芬尼的踢法較配合球隊，因此他為國家隊效力的機會比馬菲士多。雖然湯·芬尼和馬菲士經常爭奪正選位置，但兩人也試過在同一場賽事上陣。1947年，英格蘭作客[葡萄牙](../Page/葡萄牙國家足球隊.md "wikilink")，當時馬菲士出任右翼，而湯·芬尼則擔任左翼。結果英格蘭以10:0大破對手，兩人除各自取得一個入球外，更包辦所有助攻。

## 1950年代

雖然湯·芬尼受到國家隊重用，但未能為他帶來甚麼成就。在1950年的[世界盃](../Page/世界盃.md "wikilink")，英格蘭意外地被[美國淘汰出局](../Page/美國.md "wikilink")，之後一屆亦同樣在決賽週初段被淘汰。和國家隊一樣，湯·芬尼在球會生涯過得並不如意。當時的普雷斯頓雖擁有湯·芬尼如此出色的球員，但整體實力依然有限，因而始終未能贏得任何獎項。不過湯·芬尼在個人獎項上總算有不俗的成績，他在1954年和1957年獲選為[英格蘭足球先生](../Page/英格蘭足球先生.md "wikilink")。另外，他在球壇另一傑出成就是在球員生涯中，從未被黃牌警告或被罰出場。

## 退休後的生活

湯·芬尼在1960年結束球員生涯，其後當上普雷斯頓主席。1961年，[英女皇頒受OBE勳銜給他](../Page/英女皇.md "wikilink")，1998年再被封為爵士。

## 外部連結

  - [Full list of Finney's international
    goals](http://www.rsssf.com/miscellaneous/finney-intlg.html)
  - [Tom Finney at the International Football Hall of
    Fame](http://www.ifhof.com/hof/finney.asp)
  - [Tom Finney at the English Football Hall of
    Fame](https://web.archive.org/web/20070930181548/http://www.nationalfootballmuseum.com/pages/fame/2002.htm)
  - [Football fans are asked to raise the standard for North End's
    greatest
    player](https://web.archive.org/web/20070108080837/http://www.prestontoday.net/ViewArticle2.aspx?sectionid=73)
  - [Tom Finney Profile at Football
    England](http://www.football-england.com/tom_finney.html)

[Category:1922年出生](../Category/1922年出生.md "wikilink")
[Category:2014年逝世](../Category/2014年逝世.md "wikilink")
[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:普雷斯頓球員](../Category/普雷斯頓球員.md "wikilink")
[Category:OBE勳銜](../Category/OBE勳銜.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:英格蘭足球名人堂男球員](../Category/英格蘭足球名人堂男球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:1950年世界盃足球賽球員](../Category/1950年世界盃足球賽球員.md "wikilink")
[Category:1954年世界盃足球賽球員](../Category/1954年世界盃足球賽球員.md "wikilink")
[Category:1958年世界盃足球賽球員](../Category/1958年世界盃足球賽球員.md "wikilink")
[Category:愛爾蘭裔英國人](../Category/愛爾蘭裔英國人.md "wikilink")