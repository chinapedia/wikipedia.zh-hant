**阿什兰县**（[英语](../Page/英语.md "wikilink")：****）是[美國](../Page/美國.md "wikilink")[威斯康星州北部的一個縣](../Page/威斯康星州.md "wikilink")，北傍[蘇必利爾湖](../Page/蘇必利爾湖.md "wikilink")，並轄[阿波斯特尔群岛](../Page/阿波斯特尔群岛.md "wikilink")（Apostle
Islands）的大部分。面積5,941平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口16,866。縣治[阿什兰](../Page/阿什兰_\(威斯康星州\).md "wikilink")。

成立於1860年。縣名來自[參議員](../Page/美國參議院.md "wikilink")[亨利·克萊在](../Page/亨利·克萊.md "wikilink")[肯塔基州](../Page/肯塔基州.md "wikilink")[勒星頓附近的家](../Page/勒星頓_\(肯塔基州\).md "wikilink")。

[A](../Category/威斯康辛州行政区划.md "wikilink")