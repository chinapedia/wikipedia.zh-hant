<noinclude></noinclude> [編組站](編組站.md "wikilink"){{.w}}
（[到达场](到达场.md "wikilink"){{.w}}
[編組場](編組場.md "wikilink"){{.w}}
[出发场](出发场.md "wikilink")）{{.w}}[区段站](区段站.md "wikilink"){{.w}}[技术站](技术站.md "wikilink"){{.w}}[中间站](中间站.md "wikilink"){{.w}}
[越行站](越行站.md "wikilink"){{.w}} [会让站](会让站.md "wikilink"){{.w}}
[轉乘站](轉乘站.md "wikilink"){{.w}}
[轉車站／換乘站](换乘站.md "wikilink"){{.w}}
[线路所](线路所.md "wikilink"){{.w}}
[乘降所](乘降所.md "wikilink"){{.w}}
[共構車站](共構與共站.md "wikilink"){{.w}}
[车辆段](车辆段.md "wikilink"){{.w}}
[机务段](机务段.md "wikilink"){{.w}}
[扇形車庫](扇形車庫.md "wikilink"){{.w}}
[轉車盤](轉車盤.md "wikilink"){{.w}}
[客技站](客技站.md "wikilink"){{.w}}
[铁路废站](铁路废站.md "wikilink"){{.w}} {{.w}}
[幽灵车站](幽灵车站.md "wikilink"){{.w}}
[秘境車站](秘境車站.md "wikilink"){{.w}}
[車站月台](車站月台.md "wikilink"){{.w}}
[月台幕門](月台幕門.md "wikilink"){{.w}}
[月台閘門](月台閘門.md "wikilink") {{.w}}
[驼峰](驼峰_\(铁路\).md "wikilink"){{.w}}
[停車場](停車場_\(鐵路\).md "wikilink"){{.w}}[總站](總站.md "wikilink"){{.w}}[中央车站](中央车站.md "wikilink")

|group2 = [轨道](鐵路軌道.md "wikilink") |list2 =
[道岔](道岔.md "wikilink"){{.w}} [轉轍器](轉轍器.md "wikilink"){{.w}}
[道碴](道碴.md "wikilink"){{.w}} [枕木](枕木.md "wikilink") {{.w}}
[无砟轨道](无砟轨道.md "wikilink")
{{.w}}[最小曲线半径](最小曲线半径.md "wikilink"){{.w}}[最大坡度](最大坡度.md "wikilink"){{.w}}[坡度](坡度.md "wikilink"){{.w}}
[折返式鐵路](折返式鐵路.md "wikilink")

|group2=形式 |list2=[單線鐵路](單線鐵路.md "wikilink"){{.w}}
[雙線鐵路](复线铁路.md "wikilink"){{.w}}
[三線鐵路](三線鐵路.md "wikilink"){{.w}}
[四線鐵路](四線鐵路.md "wikilink"){{\!w}}
[正线](正线.md "wikilink"){{.w}} [侧线](侧线_\(铁路\).md "wikilink"){{.w}}
[站线](站线.md "wikilink")（[橫渡線](橫渡線.md "wikilink"){{.w}}
[袋狀軌](袋狀軌.md "wikilink"){{.w}}
[到发线](到发线.md "wikilink"){{.w}}
[机走线](机走线.md "wikilink"){{.w}}
[安全線](安全线_\(鐵路\).md "wikilink"){{.w}}
[避难线](避难线.md "wikilink"){{.w}}
[调车线](调车线.md "wikilink"){{.w}}
[牵出线](牵出线.md "wikilink"){{.w}}
[存车线](存车线.md "wikilink"){{.w}}
[货物线](货物线.md "wikilink"){{.w}}
[换装线](换装线.md "wikilink"){{.w}}
[加冰线](加冰线.md "wikilink"){{.w}}
[轨道衡线](轨道衡线.md "wikilink"){{.w}}
[机械保温车加油线](机械保温车加油线.md "wikilink"){{.w}}
[特殊危险货物车辆停留线](特殊危险货物车辆停留线.md "wikilink"){{.w}}
[机车走行线](机车走行线.md "wikilink"){{.w}}
[机待线](机待线.md "wikilink"){{.w}}
[驼峰迂回线](驼峰迂回线.md "wikilink"){{.w}}
[驼峰推送线](驼峰推送线.md "wikilink"){{.w}}
[驼峰禁溜线](驼峰禁溜线.md "wikilink")）
[段管线](段管线.md "wikilink")（[三角線](三角線.md "wikilink"){{.w}}
[车辆站修线](车辆站修线.md "wikilink"){{.w}} [環形迴車道](環形迴車道.md "wikilink")）
[展线](展线.md "wikilink"){{.w}} [套线](套线.md "wikilink"){{.w}}
[平交道](平交道.md "wikilink"){{.w}}
[疏解线](疏解线.md "wikilink"){{.w}}
[联络线](联络线.md "wikilink"){{.w}}
[铁路立交](立體交匯.md "wikilink"){{.w}}
[鐵路地下化{{.w}}鐵路高架化](交通立體化.md "wikilink"){{.w}}
[火车渡轮](火车渡轮.md "wikilink"){{\!w}}
[城际铁路](城际铁路.md "wikilink"){{.w}}[區域鐵路](區域鐵路.md "wikilink"){{.w}}[通勤铁路](通勤铁路.md "wikilink")／[快軌](快軌.md "wikilink")
}}

|group3 = [列车](鐵路列車.md "wikilink") |list3 =
[旅客列车](旅客列车.md "wikilink"){{.w}}
[货物列车](货物列车.md "wikilink"){{.w}}（[直达列车](直达列车.md "wikilink"){{.w}}
[直通列车](直通列车.md "wikilink"){{.w}} [小运转列车](小运转列车.md "wikilink"){{.w}}
[区段列车](区段列车.md "wikilink"){{.w}}
[摘挂列车](摘挂列车.md "wikilink")）{{.w}}
[路用列车](路用列车.md "wikilink")（[救援列车](救援列车.md "wikilink"){{.w}}
[单机列车](单机列车.md "wikilink"){{.w}} [补机列车](补机列车.md "wikilink"){{.w}}
[试运转列车](试运转列车.md "wikilink"){{.w}}
[轨道车列车](轨道车列车.md "wikilink"){{.w}}
[回送列车](回送列车.md "wikilink")）{{\!w}}
[動力分散式](動力分散式列車.md "wikilink"){{.w}}
[動力集中式](動力集中式列車.md "wikilink"){{\!w}}
[城际列车](城际列车.md "wikilink"){{.w}}
[城軌車](城軌車.md "wikilink"){{.w}}
[擺式列車](擺式列車.md "wikilink"){{\!w}}

|group4 = [鐵道機車車輛](鐵道機車車輛.md "wikilink") |list4 =
[柴油機車](柴油機車.md "wikilink"){{.w}}[混合動力車輛](混合動力車輛.md "wikilink"){{.w}}[双动力源铁路车辆](双动力源铁路车辆.md "wikilink"){{.w}}[燃氣渦輪機車](燃氣渦輪機車.md "wikilink"){{.w}}
[電力機車](電力機車.md "wikilink"){{\!w}} [機車重聯](機車重聯.md "wikilink"){{.w}}
[调车机车](调车机车.md "wikilink")

`|group2 = `[`鐵路車輛`](鐵路車輛.md "wikilink")
`|list2 = `[`鐵路客車`](鐵路客車.md "wikilink")`{{.w}} `[`控制客車`](控制客車.md "wikilink")`{{.w}} `[`鐵路貨車`](鐵路貨車.md "wikilink")`（`[`敞车`](敞车.md "wikilink")`{{.w}} `[`棚车`](棚车.md "wikilink")`{{.w}} `[`平车`](平车.md "wikilink")`{{.w}} `[`罐车`](罐车.md "wikilink")`{{.w}} `[`守车`](守车.md "wikilink")`{{.w}} `[`保温车`](保温车.md "wikilink")`{{.w}} `[`家畜车`](家畜车.md "wikilink")`{{.w}} `[`长大货物车`](长大货物车.md "wikilink")`{{.w}} `[`毒品车`](毒品车.md "wikilink")`{{.w}} `[`粮食车`](粮食车.md "wikilink")`{{.w}} `[`集装箱车`](集装箱车.md "wikilink")`{{.w}} `[`特种车`](特种车_\(铁路\).md "wikilink")`{{.w}} `[`矿石车`](矿石车.md "wikilink")`）`

`|group3 = `[`動車組`](動車組.md "wikilink")
`|list3 = `[`柴聯車`](柴聯車.md "wikilink")`{{.w}} `[`電聯車`](電聯車.md "wikilink")`{{.w}} ``{{.w}} `[`燃氣渦輪動車組`](燃氣渦輪動車組.md "wikilink")`{{!}} `[`动车`](动车组动车.md "wikilink")`{{.w}} `[`拖車`](动车组拖车.md "wikilink")

}} |group5 = 通訊及安全 |list5 =
[铁路信号机](铁路信号机.md "wikilink"){{.w}}[臂板信号机](臂板信号机.md "wikilink"){{.w}}
[閉塞](閉塞_\(鐵路\).md "wikilink"){{.w}} [鐵路安全裝置](鐵路安全裝置.md "wikilink"){{.w}}
[止衝擋](止衝擋.md "wikilink"){{.w}} [放沙裝置](放沙裝置.md "wikilink"){{.w}}
[调度集中系统](调度集中系统.md "wikilink"){{.w}}
[駕駛失知制動裝置](失能开关.md "wikilink"){{.w}}
[歐洲列車控制系統](歐洲列車控制系統.md "wikilink"){{.w}}
[中国列车控制系统](中国列车控制系统.md "wikilink")
[自動列車控制系統](列车自动控制系统.md "wikilink"){{.w}}
[列车自动保护系统](列车自动保护系统.md "wikilink"){{.w}}
[自動列車停止裝置](自動列車停止裝置.md "wikilink"){{.w}}
[自動列車警報裝置](自動列車警報裝置.md "wikilink") }}

|group6 = [电气化](電氣化鐵路.md "wikilink") |list6 =
[高架電車線](高架電車線.md "wikilink"){{.w}}
[軌道供電](軌道供電.md "wikilink"){{.w}}
[分相区](分相区.md "wikilink"){{.w}}[集電弓](集電弓.md "wikilink"){{.w}}[受电杆](受电杆.md "wikilink")

|group7 = 系統標準 |list7 = [國際鐵路聯盟](國際鐵路聯盟.md "wikilink"){{.w}}
[铁路合作组织](铁路合作组织.md "wikilink"){{.w}} {{.w}}
[UIC分類法](UIC铁路机车轴式分类.md "wikilink"){{.w}}
[華氏式別](華氏式別.md "wikilink") }}

|list2 =
[地鐵](地鐵.md "wikilink")／[捷運](捷運.md "wikilink")（[重型鐵路系統](重型鐵路系統.md "wikilink"){{.w}}[中型鐵路系統](中型鐵路系統.md "wikilink")／[輕型鐵路系統](輕型鐵路系統.md "wikilink")／[輕軌捷運系統](輕軌捷運系統.md "wikilink")）{{.w}}[地铁及轻轨车型](城市轨道交通列车车型.md "wikilink")）{{\!w}}[准地铁](准地铁.md "wikilink"){{\!w}}[輕軌](輕軌運輸系統.md "wikilink"){{.w}}[有軌電車](有軌電車.md "wikilink"){{.w}}[電車-火車](電車-列車系統.md "wikilink"){{\!w}}[無軌電車](無軌電車.md "wikilink"){{\!w}}[導向巴士](導向巴士.md "wikilink"){{\!w}}[國際地鐵聯盟](國際地鐵聯盟.md "wikilink"){{.w}}[軌道運輸標竿聯盟](軌道運輸標竿聯盟.md "wikilink")
}} |list3 =
[磁浮列車](磁懸浮列車.md "wikilink"){{.w}}[單軌鐵路](單軌鐵路.md "wikilink"){{.w}}
[纜索鐵路](纜索鐵路.md "wikilink"){{.w}}
[輕便鐵路](輕便鐵路.md "wikilink"){{.w}}[齒軌鐵路](齒軌鐵路.md "wikilink"){{.w}}
[膠輪路軌系統](膠輪路軌系統.md "wikilink"){{.w}}
[個人快速運輸系統](個人快速運輸系統.md "wikilink"){{.w}}
[旅客捷運系統](旅客捷運系統.md "wikilink"){{.w}}[機場聯絡軌道系統](機場聯絡軌道系統.md "wikilink"){{.w}}[兒童鐵路](兒童鐵路.md "wikilink")
}} }}<noinclude></noinclude>

[Category:技术模板](../Category/技术模板.md "wikilink")
[Category:鐵路模板](../Category/鐵路模板.md "wikilink")