**廣達電腦股份有限公司**（Quanta Computer
Incorporated，），簡稱**廣達電腦**，是[台灣的](../Page/台灣.md "wikilink")[筆記型電腦](../Page/筆記型電腦.md "wikilink")、[平板電腦](../Page/平板電腦.md "wikilink")、[伺服器](../Page/伺服器.md "wikilink")、[相機](../Page/相機.md "wikilink")、[工業電腦](../Page/工業電腦.md "wikilink")、[手機](../Page/手機.md "wikilink")[代工及](../Page/代工.md "wikilink")[雲端軟硬體整合大廠](../Page/雲端.md "wikilink")，歷年皆為美國[財星全球](../Page/財星.md "wikilink")500大及美國[富比士世界](../Page/富比士.md "wikilink")2000大企業之一，也是美国《[财富](../Page/财富.md "wikilink")》杂志2016年评选的[全球最大500家公司](http://www.fortunechina.com/fortune500/c/2015-07/22/content_244435.htm)的排行榜中的第326名；歷年亦同時在台灣千大製造業調查中名列前茅，2012年列名第2。

## 概要

[Quanta_ILI_Mini-Note,_Computex_Taipei_20080606.jpg](https://zh.wikipedia.org/wiki/File:Quanta_ILI_Mini-Note,_Computex_Taipei_20080606.jpg "fig:Quanta_ILI_Mini-Note,_Computex_Taipei_20080606.jpg")
[Quanta_Computer_Hwa_Ya_Technology_Park.JPG](https://zh.wikipedia.org/wiki/File:Quanta_Computer_Hwa_Ya_Technology_Park.JPG "fig:Quanta_Computer_Hwa_Ya_Technology_Park.JPG")
廣達電腦成立於1988年，最初設址於[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[士林區](../Page/士林區.md "wikilink")，1999年遷至[桃園市](../Page/桃園市.md "wikilink")[龜山區](../Page/龜山區.md "wikilink")。2000年起生產線逐漸遷至[中國大陸](../Page/中國大陸.md "wikilink")，目前有[上海市](../Page/上海市.md "wikilink")[松江區製造城](../Page/松江區.md "wikilink")（QSMC）、[常熟製造城](../Page/常熟.md "wikilink")（CSMC）、[重庆](../Page/重庆.md "wikilink")[沙坪坝区制造城](../Page/沙坪坝区.md "wikilink")（QCMC），目前在台灣僅留行政與研發。2006年，廣達研發中心於[桃園市](../Page/桃園市.md "wikilink")[龜山區](../Page/龜山區.md "wikilink")[華亞科技園區落成](../Page/華亞科技園區.md "wikilink")，並設總部於此，可容納數千位研發工程師。

廣達電腦為專門的[ODM](../Page/ODM.md "wikilink")、[OEM代工廠](../Page/OEM.md "wikilink")，代工客戶有[Acer](../Page/Acer.md "wikilink")、[IBM](../Page/IBM.md "wikilink")、[HP](../Page/惠普公司.md "wikilink")、[戴爾](../Page/戴爾.md "wikilink")、[蘋果電腦](../Page/蘋果電腦.md "wikilink")、[Toshiba](../Page/Toshiba.md "wikilink")、[Sony](../Page/Sony.md "wikilink")、[聯想集團](../Page/聯想集團.md "wikilink")（Lenovo）等；而在雲端軟硬體整合產品上，有雲達QCT(完整伺服器、儲存、網通設備整合)等[品牌](../Page/品牌.md "wikilink")，目前QCT在全球大型雲端資料中心(前50大)市占率超過五成。此外，廣達電腦2005年營業額突破[新台幣](../Page/新台幣.md "wikilink")4000億元，2010年營業額突破[新台幣](../Page/新台幣.md "wikilink")1兆元。

廣達電腦的關係企業有[廣輝電子](../Page/廣輝電子.md "wikilink")、[廣明光電](../Page/廣明光電.md "wikilink")、[鼎天國際](../Page/鼎天國際.md "wikilink")、[達威電子](../Page/達威電子.md "wikilink")、[控創科技等](../Page/控創科技.md "wikilink")。2006年4月，林百里宣布廣輝電子與[友達光電合併](../Page/友達光電.md "wikilink")，廣輝成為消滅公司；同年2月，廣達成立[達威電子](../Page/達威電子.md "wikilink")，負責廣達集團旗下[無線網路通訊產品](../Page/無線網路.md "wikilink")；同年7月，廣達宣佈以1比1.25換股方式將鼎天國際加入[廣達集團](../Page/廣達集團.md "wikilink")，負責[GPS產品](../Page/GPS.md "wikilink")，董事長由廣達電腦總經理[梁次震出任](../Page/梁次震.md "wikilink")。2008年，廣達入股德商肯創（Kontron
AG）之台灣分公司[控創科技](../Page/控創科技.md "wikilink")，成為控創科技第一大股東，並與肯創結盟推出工業電腦品牌「Quanmax」。

## 全年產品出貨量

2014年, 筆電總出貨量則為 4,850 萬台。\[1\]

## 參考

  - \[台灣50成份股-廣達2382
    <https://web.archive.org/web/20100208100742/http://www.twse.com.tw/ch/trading/indices/twco/tai50i.php>
    \]

<div class="references-small">

<references />

</div>

## 外部連結

  - [廣達電腦](http://www.quantatw.com)

  - [台灣5間企業入選2014"財星"世界500大](http://www.chinatimes.com/newspapers/20140709001074-260202)

  - [台灣47間企業入選 2014年富比世2000大](http://www.appledaily.com.tw/appledaily/article/headline/20140509/35820559/)

  - [2013年《財富》全球500強排名](http://wiki.mbalib.com/zh-tw/2013%E5%B9%B4%E3%80%8A%E8%B4%A2%E5%AF%8C%E3%80%8B%E5%85%A8%E7%90%83500%E5%BC%BA%E6%8E%92%E5%90%8D)

  - [2014年《財富》全球500強排名](http://wiki.mbalib.com/zh-tw/2014%E5%B9%B4%E3%80%8A%E8%B4%A2%E5%AF%8C%E3%80%8B%E5%85%A8%E7%90%83500%E5%BC%BA%E6%8E%92%E5%90%8D_%281-100%29)

  - [2012年《天下雜誌》一千大製造業排名](https://web.archive.org/web/20160304095721/http://media.cw.com.tw/cw/cwdata/pdf/2012-cw-1000.pdf)

[Q](../Category/1988年成立的公司.md "wikilink")
[Q](../Category/1988年台灣建立.md "wikilink")
[Q](../Category/台灣電子公司.md "wikilink")
[2](../Category/臺灣證券交易所上市公司.md "wikilink")
[Q](../Category/總部位於桃園市的工商業機構.md "wikilink")
[Q](../Category/龜山區.md "wikilink")

1.  [廣達 12 月營收攀 2014 年高峰；本季 NB
    出貨估季減 15%](http://technews.tw/2015/01/13/quanta-2014-12-revenue/)