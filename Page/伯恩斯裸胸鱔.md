**伯恩斯裸胸鱔**，又名**伯恩斯裸胸鯙**、**錢鰻**、**薯鰻**、**虎鰻**，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鰻鱺目](../Page/鰻鱺目.md "wikilink")[鯙亞目](../Page/鯙亞目.md "wikilink")[鯙科的其中一個](../Page/鯙科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚廣泛分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[東非](../Page/東非.md "wikilink")、[日本](../Page/日本.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[哥斯大黎加等海域](../Page/哥斯大黎加.md "wikilink")。

## 深度

水深0至40公尺。

## 特徵

本魚吻尖銳，體為[暗褐色](../Page/暗褐色.md "wikilink")，有多數不甚明顯而不規則的黑斑，大小如眼徑，多少成行列，在尾部者成不規則之橫條紋，且有粒狀突起，鰭邊緣為黃綠色。體長可達31公分。牙齒尖銳，需小心咬傷。

## 生態

本魚為[鯙科中較小型者](../Page/鯙科.md "wikilink")，棲息環境為礁岩之洞穴或礁隙間，晝伏夜出，以節肢動物和其他[魚類為食](../Page/魚類.md "wikilink")。

## 經濟利用

[食用魚](../Page/食用魚.md "wikilink")，煮清湯，味道鮮美，另外也可作為[觀賞魚](../Page/觀賞魚.md "wikilink")。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

  -
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[buroensis](../Category/裸胸鱔屬.md "wikilink")