**江东区**是[中國](../Page/中國.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[宁波市曾经存在的一个](../Page/宁波.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。辖区面积33.75平方千米，是宁波中心城区的一部分，也曾是宁波的行政中心所在地。2016年与[鄞州区东部合并成立新的鄞州区](../Page/鄞州区.md "wikilink")。

## 地理

江东区位于宁波市域中北部，东、南与鄞州区相连，北与江北区隔[甬江相望](../Page/甬江.md "wikilink")，东与[海曙区隔](../Page/海曙区.md "wikilink")[奉化江相望](../Page/奉化江.md "wikilink")。全境为平原，河网密布，水资源丰富。

## 沿革

江东区历史上属[鄮县](../Page/鄮县.md "wikilink")，后属[鄞县](../Page/鄞县.md "wikilink")。1951年5月，析鄞县成立宁波市，江东区在此时正式建立。2016年并入鄞州区东部成立新的[鄞州区](../Page/鄞州区.md "wikilink")，江东区建制始废。

## 行政区划

下辖8个街道，72个社区：

  - 街道办事处：百丈街道、福明街道、东柳街道、东胜街道、明楼街道、东郊街道、白鹤街道、新明街道。

## 经济

近代宁波开埠以后，江东区所辖地域成为宁波的工业重镇。

[Category:鄞州区](../Category/鄞州区.md "wikilink")
[Category:宁波行政区划史](../Category/宁波行政区划史.md "wikilink")
[Category:浙江已撤消的市辖区](../Category/浙江已撤消的市辖区.md "wikilink")
[Category:2016年废除的行政区划](../Category/2016年废除的行政区划.md "wikilink")