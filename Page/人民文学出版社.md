**人民文学出版社**是[中华人民共和国国有的一个注重于文学作品出版的](../Page/中华人民共和国.md "wikilink")[出版社](../Page/出版社.md "wikilink")。现为[中国出版集团下属出版社](../Page/中国出版集团.md "wikilink")。[ISBN代码為](../Page/ISBN.md "wikilink")978-7-02。现任社长是[臧永清](../Page/臧永清.md "wikilink")。

人民文学出版社至今为止出版了13000多种图书，包括3000多种翻译的文学作品。除当今文学作品外该出版社也从事出版经典著作，包括中外著名作家的文集和全集图书。

它还出版五种文学报刊：
《[中华文学选刊](../Page/中华文学选刊.md "wikilink")》、《[新文学史料](../Page/新文学史料.md "wikilink")》、《[当代](../Page/当代.md "wikilink")》、《[帅作文](../Page/帅作文.md "wikilink")》和《[中华散文](../Page/中华散文.md "wikilink")》。

近年与[99读书人合作](../Page/99读书人.md "wikilink")，开始出版[日本漫画和](../Page/日本漫画.md "wikilink")[轻小说](../Page/轻小说.md "wikilink")。

2009年被[新闻出版署评选为全国百佳图书出版单位](../Page/新闻出版署.md "wikilink")（文艺类）\[1\]。

## 著名出版書籍

  - 《[魯迅全集](../Page/魯迅全集.md "wikilink")》
  - 《[郭沫若全集](../Page/郭沫若.md "wikilink")·文學編》
  - 《[茅盾全集](../Page/茅盾.md "wikilink")》
  - 《[巴金全集](../Page/巴金.md "wikilink")》
  - 《[老舍文集](../Page/老舍.md "wikilink")》
  - 《[莎士比亞全集](../Page/莎士比亞.md "wikilink")》
  - 《[歌德文集](../Page/歌德.md "wikilink")》
  - 《[高尔基文集](../Page/马克西姆·高尔基.md "wikilink")》
  - 《[塞萬提斯文集](../Page/塞萬提斯.md "wikilink")》
  - 《[肖洛霍夫文集](../Page/肖洛霍夫.md "wikilink")》（[金人](../Page/金人.md "wikilink")、[草嬰](../Page/草嬰.md "wikilink")、孫美玲等譯）
  - 《[歐·亨利小說全集](../Page/歐·亨利.md "wikilink")》（[王永年譯](../Page/王永年.md "wikilink")）
  - 《[哈利·波特](../Page/哈利·波特.md "wikilink")》
    ([苏农](../Page/苏农.md "wikilink")、[马爱农](../Page/马爱农.md "wikilink")、[马爱新譯](../Page/马爱新.md "wikilink"))
  - 《[神曲](../Page/神曲.md "wikilink")》（[田德望譯](../Page/田德望.md "wikilink")）
  - 《[中国农民调查](../Page/中国农民调查.md "wikilink")》（遭禁,后解禁）
  - 《[伊利亚特](../Page/伊利亚特.md "wikilink")》（[罗念生](../Page/罗念生.md "wikilink")、[王煥生译](../Page/王煥生.md "wikilink")）
  - 《[圍城](../Page/圍城.md "wikilink")》([钱锺书](../Page/钱锺书.md "wikilink"))
  - 《[宋诗选注](../Page/宋诗选注.md "wikilink")》
    ([钱锺书註](../Page/钱锺书.md "wikilink"))
  - 《[四大名著](../Page/四大名著.md "wikilink")》
  - 《[卡夫卡小說全集](../Page/卡夫卡小說全集.md "wikilink")》
  - 《[白鹿原](../Page/白鹿原.md "wikilink")》
    ([陳忠實著](../Page/陳忠實.md "wikilink"))
  - 《[我与地坛](../Page/我与地坛.md "wikilink")》([史鐵生著](../Page/史鐵生.md "wikilink"))
  - 《[名著名譯插圖本](../Page/名著名譯插圖本.md "wikilink")》

## 出版漫画

## 出版轻小说

## 参考文献

{{-}}

[Category:中华人民共和国出版社](../Category/中华人民共和国出版社.md "wikilink")
[R人](../Category/漫画出版社.md "wikilink")
[R人](../Category/1951年成立的公司.md "wikilink")

1.