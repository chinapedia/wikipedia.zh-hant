**逾越節**是[猶太教節日](../Page/猶太教.md "wikilink")，紀念[上帝在殺死](../Page/上帝.md "wikilink")[埃及一切頭胎生物](../Page/埃及.md "wikilink")，並殺死[埃及人的長子時](../Page/埃及人.md "wikilink")，越過以色列人的長子而去。

## 第一個逾越節

《[旧约圣经](../Page/旧约圣经.md "wikilink")》中《[出埃及记](../Page/出埃及记.md "wikilink")》的**第12章25-27节**中写道，逾越節行的禮是獻給上帝逾越節的祭：當以色列人在[埃及的時候](../Page/埃及.md "wikilink")，上帝擊殺埃及人，越過以色列人的房屋，救了以色列人各家。

以色列人在埃及時，受到埃及人的苦役；上主選召[摩西](../Page/摩西.md "wikilink")，帶領以色列人離開埃及，脫離奴役的身分，前往上主應許的[迦南美地](../Page/迦南.md "wikilink")。《舊約聖經》中《出埃及記》的第12章記載著：

## 現代的逾越節

[A_Seder_table_setting.jpg](https://zh.wikipedia.org/wiki/File:A_Seder_table_setting.jpg "fig:A_Seder_table_setting.jpg")
目前逾越節的慶祝方式，是由[聖殿第二度被毀開始的](../Page/聖殿.md "wikilink")。由於聖殿被毀（到公元[135年](../Page/135年.md "wikilink")，猶太人甚至被逐出[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")），到[耶路撒冷過節變成了一句流於形式的話](../Page/耶路撒冷.md "wikilink")，直到[1967年](../Page/1967年.md "wikilink")，而羊肉亦變成了羊骨。

### seder用餐步骤

1.  點燃火烛，示意晚餐開始
2.  祝禱，喝第一杯酒
3.  洗手
4.  頭盤（青菜浸入鹽水，象徵在埃及被奴役的辛酸，昔日用חרוסת）
5.  掰開一塊無酵餅，把其中一半藏起
6.  打開覆蓋餅的布，邀請窮人進餐
7.  重述逾越故事
8.  第二杯酒，象徵救贖
9.  洗手
10. 祝福無酵餅，然後夾苦菜食用
11. 儀式暫停，享用事前烹調好的晚餐
12. 尋找程序五被藏起來的餅
13. 飯後禱告
14. 第三杯酒
15. [以利亞之杯](../Page/以利亞.md "wikilink")（邀請[彌賽亞來臨](../Page/彌賽亞.md "wikilink")）
16. 讚美詩
17. 歌曲
18. 第四杯酒，並祝福：「明年在耶路撒冷過逾越節！」

### 晚餐盤

在晚餐桌中間有一專用的盤，放上幾種食物：

  - 象徵奴役的苦菜
  - 象徵磚土的חרוסת，一種由果仁、酒、蘋果和肉桂煮成的醬
  - 象徵淚水的青菜
  - 象徵在節日犧牲的羊骨（不食）
  - 象徵對不能在聖殿舉行節日哀悼的蛋（不食）

鹽水和餅分開盛裝。

## 现代犹太教节期的变化

现代犹太教逾越节庆祝的时间与古代的不尽相同。现代犹太人的历法是按照天文上的新月日子而定，而古代犹太人的历法则是按照耶路撒冷出现新月的那一天日落后，作为一个月的第一天。

这样古代的以色列人在春分前后，观测到耶路撒冷出现新月的那天日落后，就是尼散月一日。从这天开始，第14日就是古代的逾越节。而现代犹太人根据天文学的新月定义，就会产生18至30小时的误差。

除此之外，今天的大部分犹太人在尼散月十五日庆祝逾越节，而不是像耶穌一样在十四日守逾越节。

## 补过逾越节

以珥月十四日（宗教曆的二月，即西曆4月至5月期間）是**補過逾越節**。是特別為那些在上月因不潔或遠行不能參加逾越節的人而設的。

補過逾越節的起因記載於民數記
9:1-14。當時有些人因接觸了死屍而不潔，不能守逾越節。他們來到摩西和亞倫面前，提出他們因為不潔而不能在所定的日期守節，要求給他們另一次機會。上帝因此讓他們在一個月後補過逾越節。

## 基督教與逾越節

[耶穌受難前](../Page/耶穌受難.md "wikilink")，[最后的晚餐就是與](../Page/最后的晚餐.md "wikilink")[門徒共進逾越節晚餐](../Page/耶穌十二門徒.md "wikilink")，根据犹太人对一天的定义起于日落，终于第二天日落的方法，耶穌在当年逾越节的这天（星期四日落至星期五日落）死去的。

一般教会的做法与犹太教还有不同，大部分教会认为耶穌是在星期五（安息日前一天）被杀害的，所以他们的“逾越节”纪念，又称耶穌受难纪念一定是在[聖週中的星期五举行](../Page/聖週.md "wikilink")，稱為[聖週五](../Page/聖週五.md "wikilink")，而耶穌是在周日（上帝創造天地的第一日）复活，[復活節與逾越節同期舉行](../Page/復活節.md "wikilink")。但是由于不可能每年逾越节正好是星期五，所以大部分教会的庆祝节日很少和犹太人的节日吻合。

在有些語言中（如[拉丁語](../Page/拉丁語.md "wikilink")），復活節和逾越節的名稱是相同的（拉丁語：Pascha）。

## 参见

  - [出埃及记](../Page/出埃及记.md "wikilink")
  - [塔纳赫](../Page/塔纳赫.md "wikilink")

## 外部链接

  - [探索猶太世界:逾越節](https://web.archive.org/web/20080317071805/http://www.explorejewish.net/Feast/pesach.htm)

  - [About.com:关于逾越节](https://web.archive.org/web/20080328200045/http://judaism.about.com/od/passover/Passover.htm)

  - [逾越节购物指南和完全向导](http://www.kosher4passover.com/)

  - [逾越节](http://www.angelfire.com/pa2/passover/)，Eli Ha-Levi, BA,
    M.L.I.S.

  - [逾越节完全指南](http://www.chabad.org/holidays/passover/default.asp)
    ，[Chabad.org](../Page/Chabad.org.md "wikilink")

  - [逾越节FAQ](http://www.askmoses.com/qa_list.html?h=107)，[Askmoses.com](../Page/Askmoses.com.md "wikilink")

  - [逾越节完全指南](http://www.aish.com/holidays/passover/default.asp)，[Aish
    HaTorah](../Page/Aish_HaTorah.md "wikilink")

  - [International Seder
    Directory](http://www.chabad.org/holidays/passover/seders.asp)

  - [Hilchot Pesach on **Kitzur Shulchan
    Aruch**](http://www.kitzur.net/main.php?nk=1&siman=107)
    ([希伯来语](../Page/希伯来语.md "wikilink"))

  - [Passover](http://reference.aol.com/fast-facts/holidays/_a/fast-facts-about-passover/20060411105609990001)
    美国在线研究和学习板块

  - [Extensive collection of ceramic, fused glass, wooden, pewter &
    silver plated Seder & Matzah plates and beautifully hand-painted &
    embroidered Matzah & Affikoman
    covers](http://www.judaica-mall.com/passover-gifts.htm)

  - [Pniney Halakha-פניני הלכה in
    Hebrew](http://www.yeshiva.org.il/midrash/Hmidrash.asp?cat=132)

  -
{{-}}

[Category:犹太教节日](../Category/犹太教节日.md "wikilink")