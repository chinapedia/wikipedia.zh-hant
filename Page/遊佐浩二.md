**遊佐浩二**（，），[日本男性](../Page/日本.md "wikilink")[配音員](../Page/配音員.md "wikilink")、[旁白](../Page/旁白.md "wikilink")。[自由身](../Page/自由職業.md "wikilink")。出身於[京都府](../Page/京都府.md "wikilink")。[身高](../Page/身高.md "wikilink")173cm。B型[血](../Page/血型.md "wikilink")。前妻為同行[三橋加奈子](../Page/三橋加奈子.md "wikilink")（經由朋友的介紹認識發展）。

## 概要

### 經歷

小時候曾經住在[京都競馬場附近](../Page/京都競馬場.md "wikilink")（[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[伏見區淀](../Page/伏見區.md "wikilink")）。現在的老家在[宇治市附近](../Page/宇治市.md "wikilink")。[大學時期專攻](../Page/大學.md "wikilink")[日本語](../Page/日本語.md "wikilink")\[1\]。大學畢業後，進入成為第7期研修生。1995年，於電視動畫《[黃金勇者](../Page/黃金勇者.md "wikilink")》正式在聲優業界出道。出道之後進入經紀公司，但不久就離所，從此以後一直以[自由聲優的身份持續參與](../Page/自由職業.md "wikilink")。

2007年，在他自己參演的《[假面騎士電王](../Page/假面騎士電王.md "wikilink")》中，因擔任後期所演唱的主題曲『[Climax
Jump DEN-LINER
form](../Page/Climax_Jump.md "wikilink")』登上了[Oricon第二名](../Page/Oricon.md "wikilink")，所以和其他共演者一起榮獲第二屆[聲優獎與特別獎](../Page/聲優獎.md "wikilink")。

### 聲音特色

代表作為《[假面騎士電王](../Page/假面騎士電王.md "wikilink")》的浦塔羅斯／電王Rod
Form、《[音速小子](../Page/音速小子系列.md "wikilink")》系列的黑影（Shadow The
HedgeHog）、《[BLEACH](../Page/BLEACH_\(動畫\).md "wikilink")》中的市丸銀等。

進入聲優業界之後主要從事動畫、廣播劇CD、海外洋片日語配音的工作，不僅如此廣泛擔任過許多齣綜藝節目旁白。除了聲優本業的配音工作之外，還擔任過活動和廣播節目主持人在一部分中廣為人知。

配音的角色類型方面，以偏高而充滿色氣的聲音是其特徵。除了飾演好青年和美男子之外，也演過許多冷酷、狂妄、輕薄、陰險的反派。在動畫《楚楚可憐超能少女組》中飾演反派兵部京介就使他為人熟知，另外，在一起出演《[假面騎士電王](../Page/假面騎士電王.md "wikilink")》的[佐藤健的](../Page/佐藤健.md "wikilink")[部落格裡也曾出現這樣的證言](../Page/部落格.md "wikilink")，這已經成為一般對他聲音的認知。據他本人說，他扮演的角色大多眼睛都不睁開的（如《黑執事》的劉、《銀魂》的東城步、《BLEACH》的市丸銀等）\[2\]。另外，從喉嚨發出的笑聲為其特色。

[鹽澤兼人逝去之後](../Page/鹽澤兼人.md "wikilink")，遊戲版《[海底兩萬哩](../Page/冒險少女娜汀亞.md "wikilink")》登場的尼奧皇帝由遊佐擔任。

### 人物

  - 除了從事配音工作之外，曾在2008年8月上映的假面騎士系列電影《[劇場版
    假面騎士Kiva：魔界城之王](../Page/魔界城之王.md "wikilink")》中友情客串，飾演的角色是高中棋藝社社員。
  - 曾養著一隻公的長毛[吉娃娃犬](../Page/吉娃娃.md "wikilink")，名字叫銀牙（[平假名](../Page/平假名.md "wikilink")：，羅馬字：Ginga，）。取名來自遊佐他在動畫《》的配音角色鳥飼銀牙。
  - 上面有一個大四歲的哥哥，目前似乎都定居於京都。從小時候就跟自家兄弟處不太來。
  - 配偶方面，2007年與同行[三橋加奈子結婚](../Page/三橋加奈子.md "wikilink")\[3\]。2012年離婚\[4\]。
  - 不管別人還是自己都認為自身是個S。自己也說過自己是個S的話（）。主張自己是[腹黑型的角色](../Page/腹黑.md "wikilink")。
  - 家就住在同行[鳥海浩輔家附近](../Page/鳥海浩輔.md "wikilink")，是會一起喝酒的朋友。只要有共同出演的作品，就會一起去喝酒。平常都稱呼鳥海為「**鳥**」，在廣播劇CD（像是《》等）中可以常常聽見這樣的稱呼。
  - 《The New York
    Times》的記事中有寫著，「有點像年輕時的大衛·卡西迪（英文：）」的聲音。不過也有人說完全不像。另一方面，也有人說他像[權相佑](../Page/權相佑.md "wikilink")。曾經替[權相佑進行日語配音的](../Page/權相佑.md "wikilink")[真殿光昭卻這樣說了](../Page/真殿光昭.md "wikilink")『（與其說是臉還不如說）髮型倒是挺像的。』
  - 隔天宿醉的話，一定會把蔬菜三明治跟可樂配在一起吃。
  - 不太能吃甜的食物。比起咖啡，屬於紅茶派的\[5\]。咖啡的話，在學生時代相當常喝。

<!-- end list -->

  - 交友關係

好友為漫畫家[柴田亞美](../Page/柴田亞美.md "wikilink")，曾在她的漫畫作品進行方言教導，在那之後以電子信箱相互聯絡。

## 演出作品

※**粗體**為主角、主要角色

### 電視動畫

  - [我們這一家](../Page/我們這一家.md "wikilink")（數學老師、女兒的男朋友、公車司機、營業員等）
  - [伊索寓言](../Page/伊索寓言.md "wikilink")（チーターキッド）
  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（上班族、ショウの父親 他）
  - 激動\!歴史を変える男たち ～アニメ静岡県史～（江川担庵、プチャーチン提督）
  - [麵包超人](../Page/麵包超人.md "wikilink")（空とぶベッドくん）
  - [神奇寶貝](../Page/神奇寶貝.md "wikilink")（ユウジ、ミツジ）
  - [名侦探柯南](../Page/名侦探柯南_\(动画\).md "wikilink")（飛田銀二、田所俊也、川崎哲也、勝呂久志、椎名涼介、ダムの係員、消防員(第385-387話)、星河童吾(第440-442話)）

**1995年**

  - 飛べ\!イサミ（落書魔）
  - 暖暖日記（ナンナン）

**1996年**

  - 黄金勇者ゴルドラン（舎弟）
  - [機動新世紀鋼彈X](../Page/機動新世紀鋼彈X.md "wikilink")（デマー・グライフ）
  - [秀逗魔導士](../Page/秀逗魔導士.md "wikilink")（見張り）
  - [超者雷丁](../Page/超者雷丁.md "wikilink")（**鳥飼銀牙**、ライディーンアウル）
  - 勇者指令ダグオン（アナウンサー、男子生徒、不良）

**1997年**

  - [CLAMP學園偵探團](../Page/CLAMP學園偵探團.md "wikilink")（黒服D）
  - [勇者王GaoGaiGar](../Page/勇者王.md "wikilink")（營運委員）

**1998年**

  - [青澀之戀](../Page/青澀之戀.md "wikilink")（Sentimental Journey）（男學生）
  - [DT戰士](../Page/DT戰士.md "wikilink")（DT EIGHTRON）（ベルク）
  - [失落的宇宙](../Page/失落的宇宙.md "wikilink")（LOST UNIVERSE）（壞小孩）
  - ヨシモトムチッ子物語（シンスケクワガタ）

**1999年**

  - [頭文字D](../Page/頭文字D.md "wikilink") Second Stage（THUNDERS 他）
  - 宇宙海賊ミトの大冒険（海賊）
  - [可愛巧虎島](../Page/可愛巧虎島.md "wikilink")（キャスター）
  - 週刊ストーリーランド（従業員、警官、男）
  - [星界的紋章](../Page/星界的紋章.md "wikilink")（レーリア）
  - 星方天使エンジェルリンクス（ニコラ）
  - ∀ガンダム（作業員A、農民B、ミリシャ兵B）
  - 地球防衛企業ダイ・ガード（牧瀨）

#### 2000年代

**2000年**

  - [星界的戰旗](../Page/星界的戰旗.md "wikilink")（レーリア）
  - [第一神拳](../Page/第一神拳.md "wikilink")（テスト生）
  - ブギーポップは笑わない Boogiepop Phantom（隆）

**2001年**

  - グラップラー刃牙（ガイア・野村）
  - The Soul Taker ～魂狩～（オペレーター）
  - [光劍星傳EX](../Page/光劍星傳EX.md "wikilink")（奇斯）
  - [地球少女Arjuna](../Page/地球少女Arjuna.md "wikilink")（副官C）
  - パワーパフガールズ（グローリー、ディック）
  - [棋魂](../Page/棋魂.md "wikilink")（白川道夫、片桐恭平、大君、佐和良中學三將、金 他）
  - [魔法戰士李維](../Page/魔法戰士李維.md "wikilink")（勇者、冒險者B）

**2002年**

  - [足球小將](../Page/足球小將.md "wikilink") WILD STRIKER篇（大村ハルキ、藤森稔、古木 他）

**2003年**

  - [原子小金剛](../Page/原子小金剛.md "wikilink")（布魯斯、職員、パイロット）
  - ガンパレード・マーチ ～新たなる行軍歌～（運転手）
  - [奇諾之旅](../Page/奇諾之旅.md "wikilink")（上班族）
  - ソニックX（シャドウ・ザ・ヘッジホッグ）
  - 超ロボット生命体トランスフォーマー マイクロン伝説（アイアンハイド、スラスト）
  - [成惠的世界](../Page/成惠的世界.md "wikilink")（島田老闆）
  - PAPUWA（アラシヤマ、キムラ、長良川の鵜）

**2004年**

  - Get Ride\! アムドライバー（**シーン・ピアース**）
  - それいけ\!活寶三人組（茜的父親）
  - トランスフォーマー スーパーリンク（アイアンハイド / アイアントレッド、ウイングダガー / ウイングセイバー）
  - B-伝説\! バトルビーダマン 炎魂（用心棒）
  - ブラック・ジャック（医師）
  - [BLEACH](../Page/BLEACH.md "wikilink")（**市丸銀**）
  - 陸奥圓明流外伝 修羅の刻（**陸奥天斗**、朝之助、藤吉）
  - 流星戦隊ムスメット（西翔沙、プロデューサー）
  - [烘焙王](../Page/烘焙王.md "wikilink")（愛德華・凱薩）
  - [遊戲王GX](../Page/遊戲王GX.md "wikilink")（天上院吹雪 / ダークネス）
  - 勇午～交渉人～（中尉）

**2005年**

  - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink")（葉柱纇、三宅成、賽門）
  - [銀牙傳説](../Page/銀牙傳説.md "wikilink")（ブルゲ）
  - [血戰BLOOD](../Page/血戰.md "wikilink")+（グドリフ、アーチャー推進調査補佐官 他）
  - [驚爆危機](../Page/驚爆危機.md "wikilink")（トニー）
  - まじめにふまじめ かいけつゾロリ（黒服C）

**2006年**

  - [Ergo Proxy](../Page/Ergo_Proxy.md "wikilink")（**ビンセント・ロウ**）
  - [彩雲國物語](../Page/彩雲國物語.md "wikilink")（華眞/ 千夜）
  - [史上最強弟子兼一](../Page/史上最強弟子兼一.md "wikilink")（池下）
  - NIGHT HEAD GENESIS（神谷司）
  - [NANA](../Page/NANA.md "wikilink")（西本）
  - [妖逆門](../Page/妖逆門.md "wikilink")（雷信）
  - [Marginal Prince
    —月桂樹的王子們—](../Page/Marginal_Prince_—月桂樹的王子們—.md "wikilink")（**エンジュ**）
  - ワンワンセレプー それゆけ\!徹之進（ヨル・ゲイツ）

**2007年**

  - [一騎當千](../Page/一騎當千.md "wikilink") Dragon Destiny（左慈元放、徐晃公明）
  - [Yes\! 光之美少女5](../Page/Yes!_光之美少女5.md "wikilink")（ローゼット伯爵）
  - [鋼鐵三國志](../Page/鋼鐵三國志.md "wikilink")（**諸葛瑾子瑜**、伝令、ゴロツキ）
  - [悲慘世界 少女珂賽特](../Page/悲慘世界_少女珂賽特.md "wikilink")（蒙帕納斯）
  - [鐵子之旅](../Page/鐵子之旅.md "wikilink")（群衆）
  - [電腦線圈](../Page/電腦線圈.md "wikilink")（猫目）
  - [魔人偵探腦嚙涅羅](../Page/魔人偵探腦嚙涅羅.md "wikilink")（**笹塚衛士**）
  - [银魂](../Page/银魂_\(動畫\).md "wikilink") （**东城步**）
  - [獵魔戰記](../Page/獵魔戰記.md "wikilink") （**伊士利**）
  - [節哀唷♥二之宮同學](../Page/節哀唷♥二之宮同學.md "wikilink") （**奧城 翼**）

**2008年**

  - [雨夜之月](../Page/雨夜之月.md "wikilink") （**篠之女绀**）
  - [楚楚可憐超能少女組](../Page/楚楚可憐超能少女組.md "wikilink") （**兵部京介**）
  - 名偵探柯南 （永作司朗）
  - [S·A特優生](../Page/S·A特優生.md "wikilink") （緒方蒼）
  - [黑執事](../Page/黑執事.md "wikilink") （**劉**）

**2009年**

  - [蒼天航路](../Page/蒼天航路.md "wikilink") （周瑜）
  - [東之伊甸](../Page/東之伊甸.md "wikilink") （辻仁太郎）
  - [海猫鳴泣之時](../Page/海猫悲鳴時.md "wikilink")（天草十三）
  - [貓願三角戀](../Page/貓願三角戀.md "wikilink") （約瑟芬）
  - [哆啦A夢](../Page/哆啦A夢動畫集數列表_\(2005年至今\).md "wikilink") （學生）

#### 2010年代

**2010年**

  - [神隱之狼](../Page/神隱之狼.md "wikilink")（賢木儁一郎）
  - [薄櫻鬼\~新選組奇譚\~](../Page/薄櫻鬼.md "wikilink")（**原田左之助**）
  - [薄櫻鬼 碧血錄](../Page/薄櫻鬼.md "wikilink")（**原田左之助**）
  - [花丸幼稚園](../Page/花丸幼稚園.md "wikilink")（花丸）
  - [黑执事2](../Page/黑执事.md "wikilink")（**劉**）
  - [變形金剛進化版](../Page/變形金剛進化版.md "wikilink")（**[巡弋者](../Page/巡弋者.md "wikilink")**）
  - [妖怪少爺](../Page/妖怪少爺.md "wikilink")/[滑頭鬼之孫](../Page/滑頭鬼之孫.md "wikilink")（奴良滑瓢（年輕時期））
  - [心靈偵探八雲](../Page/心靈偵探八雲.md "wikilink")（木下英一）
  - [Starry☆Sky ～in Autumn～](../Page/Starry☆Sky.md "wikilink")（**水嶋郁**）

**2011年**

  - [青之驅魔師](../Page/青之驅魔師.md "wikilink")（**志摩廉造**）
  - [歌之王子殿下](../Page/歌之王子殿下.md "wikilink")（日向龍也）
  - [賭博默示錄-破戒篇](../Page/賭博默示錄.md "wikilink")（三好智廣）
  - [GOSICK](../Page/GOSICK.md "wikilink")（亞朗）
  - [這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")（夜之王）
  - [世界第一初戀](../Page/世界第一初戀.md "wikilink")（男）
  - [TIGER & BUNNY](../Page/TIGER_&_BUNNY.md "wikilink")（Lunatic /
    尤利・佩特洛夫）
  - [妖怪少爺第二期～千年魔京～](../Page/妖怪少爺.md "wikilink")（奴良滑瓢（年輕時期））
  - 名偵探柯南（梨田輝之 \#640-641）
  - [銀魂'](../Page/銀魂_\(動畫\).md "wikilink")（东城步）
  - [請認真的和我戀愛！！](../Page/請認真的和我戀愛！！.md "wikilink")（葵冬馬）
  - [歌之王子殿下♪LOVE1000%](../Page/歌之王子殿下.md "wikilink")（日向龍也）

**2012年**

  - [少年同盟2](../Page/少年同盟.md "wikilink")（淺羽父）
  - [ZETMAN](../Page/ZETMAN.md "wikilink")（灰谷政次）
  - [這樣算是殭屍嗎？OF THE DEAD](../Page/這樣算是殭屍嗎？.md "wikilink")（夜之王）
  - [王者天下](../Page/王者天下.md "wikilink")（壁）
  - [聖靈家族](../Page/聖靈家族.md "wikilink")（**喬利(Jolly)**）
  - [窮神](../Page/窮神_\(漫畫\).md "wikilink")（伊吹）
  - [愛、選舉與巧克力](../Page/愛、選舉與巧克力.md "wikilink")（佐賀玲二）
  - [刀劍神域](../Page/刀劍神域.md "wikilink")（克拉帝爾）
  - [薄櫻鬼 黎明錄](../Page/薄櫻鬼.md "wikilink")（**原田左之助**）

**2013年**

  - [THE UNLIMITED 兵部京介](../Page/楚楚可憐超能少女組.md "wikilink")（**兵部京介**）
  - [黑色嘉年華](../Page/黑色嘉年華.md "wikilink")（**朔**）
  - [革命機Valvrave](../Page/革命機Valvrave.md "wikilink")（費加洛）
  - [宇宙戰艦大和號2199](../Page/宇宙戰艦大和號2199.md "wikilink")（远山清）※2012年起于电影院先行上映
  - [王者天下2](../Page/王者天下.md "wikilink")（壁）
  - [歌之王子殿下♪LOVE2000%](../Page/歌之王子殿下.md "wikilink")（日向龍也）
  - [斷裁分離的罪惡之剪](../Page/斷裁分離的罪惡之剪.md "wikilink")（皇鼎〈教授〉）
  - [GATCHAMAN Crowds](../Page/GATCHAMAN_Crowds.md "wikilink")（梅田光一）
  - [Little Busters\!
    ～Refrain～](../Page/Little_Busters!.md "wikilink")（導師）
  - [飆速宅男](../Page/飆速宅男.md "wikilink")（御堂筋翔）
  - [蒼藍鋼鐵戰艦](../Page/蒼藍鋼鐵戰艦.md "wikilink")（刑部藤十郎）
  - [東京闇鴉](../Page/東京闇鴉.md "wikilink")（**大友陣**）
  - 名偵探柯南（羽川条平 ※第712～715話）
  - [BLOOD LAD 血意少年](../Page/BLOOD_LAD_血意少年.md "wikilink")（帕普拉頓‧亞基姆）

**2014年**

  - [信長之槍](../Page/信長之槍.md "wikilink")（聖傑爾曼）
  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（**白澤**）
  - [pupa](../Page/pupa.md "wikilink")（**鬼島四朗**）
  - [上課小動作](../Page/上課小動作.md "wikilink")（谷老師）
  - [金田一少年之事件簿R（第二季）](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（小金井守
    ※第15～16話）
  - [魔法科高中的劣等生](../Page/魔法科高中的劣等生.md "wikilink")（周 / 周公瑾）
  - [東京喰種](../Page/東京喰種.md "wikilink")（多田良）
  - [黑執事 Book of Circus](../Page/黑執事.md "wikilink")（劉）
  - [火星異種](../Page/火星異種.md "wikilink")「阿聶克斯1號篇」（阿德魯夫·萊因哈魯特）
  - [笑傲曇天](../Page/笑傲曇天.md "wikilink")（風魔小太郎/假面男子）
  - [飆速宅男 GRANDE ROAD](../Page/飆速宅男.md "wikilink")（御堂筋翔）
  - [GUNDAM創戰者TRY](../Page/GUNDAM創戰者.md "wikilink")（宮賀大樹）

**2015年**

  - [美男高校地球防衛部LOVE！](../Page/美男高校地球防衛部LOVE！.md "wikilink")（黑鳥持手男）
  - [聖劍使的禁咒詠唱](../Page/聖劍使的禁咒詠唱.md "wikilink")（漆原賢典）
  - [歌之王子殿下♪ 真愛革命](../Page/歌之王子殿下.md "wikilink")（日向龍也）
  - [银魂°](../Page/銀魂_\(動畫\).md "wikilink") （東城步）
  - [GANGSTA.](../Page/GANGSTA..md "wikilink")（多梅尼科·阿爾坎傑羅）
  - [GATE 奇幻自衛隊 在彼方的土地，如斯的戰鬥](../Page/奇幻自衛隊.md "wikilink")（柳田明）
  - [迷糊餐廳 第三期](../Page/迷糊餐廳.md "wikilink")（峰岸透）
  - [OVERLORD](../Page/OVERLORD_\(小說\).md "wikilink")（布萊恩·安格勞斯）
  - [青年黑傑克](../Page/青年怪醫黑傑克.md "wikilink")（**藪**）
  - [對魔導學園35試驗小隊](../Page/對魔導學園35試驗小隊.md "wikilink")（兇煞）

**2016年**

  - [NORN9 命運九重奏](../Page/NORN9_命運九重奏.md "wikilink")（**加賀見一月**）

  - [Divine Gate](../Page/Divine_Gate.md "wikilink")（洛基）

  - GATE 奇幻自衛隊 在彼方的土地，如斯的戰鬥 第2期（柳田明）

  - [昭和元祿落語心中](../Page/昭和元祿落語心中.md "wikilink")（萬月）

  - [魔法使 光之美少女！](../Page/魔法使_光之美少女！.md "wikilink")（巴提）

  - [火星異種 復仇](../Page/火星異種.md "wikilink")（阿德魯夫·萊因哈魯特）

  -
  - [薄櫻鬼 〜御伽草子〜](../Page/薄櫻鬼.md "wikilink")（**原田左之助**）

  - [KUROMUKURO](../Page/KUROMUKURO.md "wikilink")（弗斯納尼）

  - [線上遊戲的老婆不可能是女生？](../Page/線上遊戲的老婆不可能是女生？.md "wikilink")（†黑之魔法師†）

  - [在下坂本，有何貴幹？](../Page/在下坂本，有何貴幹？.md "wikilink")（8823前輩）

  - [龍族拼圖X](../Page/龍族拼圖X.md "wikilink")（傑斯特）

  - [吸血鬼僕人](../Page/吸血鬼僕人.md "wikilink")（約漢內斯·米密爾·佛斯特斯）

  - [Scared Rider
    Xechs](../Page/Scared_Rider_Xechs.md "wikilink")（甘粕ソーイチロウ）

  - [時間旅行少女～真理、和花與8名科學家～](../Page/時間旅行少女～真理、和花與8名科學家～.md "wikilink")（薩繆爾·摩斯）

  - [路人超能100](../Page/靈能百分百.md "wikilink")（神室真司）

  - [終未的伊澤塔](../Page/終未的伊澤塔.md "wikilink")（蓋爾茲）

  - 歌之王子殿下 真愛傳奇之星（日向龍也）

**2017年**

  - [青之驅魔師](../Page/青之驅魔師.md "wikilink") 京都不淨王篇（**志摩廉造**）
  - [昭和元祿落語心中](../Page/昭和元祿落語心中.md "wikilink")－助六再現篇－（萬月）
  - 飆速宅男 NEW GENERATION（御堂筋翔）
  - [ACCA13區監察課](../Page/ACCA13區監察課.md "wikilink")（利利烏姆）
  - 名侦探柯南（江舟论介 ）
  - [不正經的魔術講師與禁忌教典](../Page/不正經的魔術講師與禁忌教典.md "wikilink")（修伊·ロスターム）
  - [Fate/Apocrypha](../Page/Fate/Apocrypha.md "wikilink")（**紅之Lancer**）
  - [妖怪公寓的優雅日常](../Page/妖怪公寓的優雅日常.md "wikilink")（佐藤、寇庫馬）
  - 鬼燈的冷徹 第貳期（**白澤**）
  - [梵諦岡奇蹟調查官](../Page/梵諦岡奇蹟調查官.md "wikilink") (**朱利亞·米迦勒·伯格**)
  - 戀愛與謊言(仁坂遙一)

**2018年**

  - 名偵探柯南（沖田總司）
  - [七大罪 戒之復活](../Page/七大罪_\(漫畫\).md "wikilink")（古雷羅德）
  - [OVERLORD II](../Page/OVERLORD_\(小說\).md "wikilink")（布萊恩·安格勞斯）
  - [魔法少女 我](../Page/魔法少女_我.md "wikilink")（兵衛）
  - 東京喰種:re（多田良）
  - 鬼燈的冷徹 第貳期 其之貳（**白澤**）
  - [輕羽飛揚](../Page/輕羽飛揚.md "wikilink")（倉石監督）
  - [百鍊霸王與聖約女武神](../Page/百鍊霸王與聖約女武神.md "wikilink")（洛普特／弗貝茲倫古）
  - [邪神與廚二病少女](../Page/邪神與廚二病少女.md "wikilink")（惡魔A）
  - [京都寺町三條商店街的福爾摩斯](../Page/京都寺町三條商店街的福爾摩斯.md "wikilink")（**圓生**\[6\]）
  - OVERLORD III（布萊恩·安格勞斯）
  - 東京喰種:re 第2期（多田良）

**2019年**

  - 路人超能100 II（神室真司）
  - [不愉快的妖怪庵 续](../Page/不愉快的妖怪庵_续.md "wikilink")（行政）

### OVA

  - アーケードゲーマーふぶき（ナレーション、ザッコーA、ザッコーLv.1）
  - 頭文字D Extra Stage インパクトブルーの彼方に…（野上）
  - [腐敗教師方程式](../Page/腐敗教師方程式.md "wikilink")2（子分）
  - SHADOW SKILL グルダ流交殺法の秘密（デスウィンド）
  - SHADOW SKILL グルダ流交殺法の秘密 FULL COMPLETE（デスウィンド）
  - てなもんやボイジャーズ（地回り）
  - [網球王子](../Page/網球王子.md "wikilink")（渡邊オサム）
  - [獵人HUNTER](../Page/獵人.md "wikilink")×HUNTER GREED ISLAND（ウイング）
  - [獵人HUNTER](../Page/獵人.md "wikilink")×HUNTER G・I final（バラ、モントール、ゼツク）
  - B'T X NEO（科学者、通信兵）
  - ふたりのジョー（結城譲）
  - 絶対可憐チルドレン～愛多憎生！ 奪われた未來？～（**兵部京介**）
  - [聖鬥士星矢 THE LOST CANVAS
    冥王神話](../Page/THE_LOST_CANVAS_冥王神話_\(動畫\).md "wikilink")（天蠍座
    卡路狄亞）
  - [型男戀愛王國](../Page/型男戀愛王國.md "wikilink")（**谷口**）

**2014年**

  - [進擊的巨人 無悔的選擇 前篇](../Page/進擊的巨人.md "wikilink")（法兰）※單行本第15巻附DVD限定版

**2015年**

  - [黑執事 Book of Murder](../Page/黑執事.md "wikilink")（劉）
  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（**白澤**）※第17卷附贈原創動畫DVD限定版
  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（**白澤**）※第18卷附贈原創動畫DVD限定版
  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（**白澤**）※第19卷附贈原創動畫DVD限定版

**2017年**

  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（**白澤**）※第24卷附贈原創動畫DVD限定版

### 動畫電影

  - [銀魂劇場版:新譯紅櫻篇](../Page/銀魂劇場版:新譯紅櫻篇.md "wikilink")（東城步）（客串）
  - [空之境界](../Page/空之境界.md "wikilink")（科爾奈利烏斯·阿魯巴）
  - [FAIRY TAIL魔導少年 鳳凰的巫女](../Page/FAIRY_TAIL魔導少年.md "wikilink")（切斯）
  - [銀魂完結篇 永遠的萬事屋](../Page/銀魂劇場版_完結篇_永遠的萬事屋.md "wikilink")（東城步）
  - [名偵探柯南 迷宮的十字路](../Page/名偵探柯南_迷宮的十字路.md "wikilink")（水尾春太郎）

**2002年**

  - [千年女優](../Page/千年女優.md "wikilink")

### 網路動畫

**2016年**

  - [哨聲響起 重配版](../Page/哨聲響起.md "wikilink")（雨宮東吾） \[7\]

### 遊戲

  - [魔塔大陸](../Page/魔塔大陸.md "wikilink")2～少女們於世界中迴響的創造詩～ （**クロア**）
  - [超音鼠系列](../Page/超音鼠.md "wikilink")（）
  - [花宵ロマネスク](../Page/花宵ロマネスク.md "wikilink")（宝生 葵）
  - [神隱之狼](../Page/神隱之狼.md "wikilink")（**賢木儁一郎**）
  - [Starry☆Sky ～in Autumn～](../Page/Starry☆Sky.md "wikilink") （**水嶋郁**）
  - [薄櫻鬼系列](../Page/薄櫻鬼.md "wikilink")（**原田左之助**）
      - 薄桜鬼 〜新選組奇譚〜
      - 薄桜鬼 ポータブル
      - 薄桜鬼 DS
      - 薄桜鬼 随想録
      - 薄桜鬼 遊戯録
      - 薄桜鬼 巡想録
      - 薄桜鬼 黎明録
  - [BLEACH系列](../Page/BLEACH.md "wikilink")（**市丸銀**）
      - \[PSP\]BLEACH ヒート・ザ・ソウル（1 - 6）
      - \[PSP\]BLEACH ソウル・カーニバル
      - \[PS2\]BLEACH 選ばれし魂
      - \[PS2\]BLEACH 放たれし野望
      - \[PS2\]BLEACH ブレイド・バトラーズ（1 - 2nd）
      - \[GBA\]BLEACH 紅に染まる尸魂界
      - \[GC\]BLEACH 黄昏にまみえる死神
      - \[DS\]BLEACH 蒼天に駆ける運命
      - \[DS\]BLEACH 黒衣ひらめく鎮魂歌
      - \[DS\]BLEACH The 3rd Phantom
      - \[Wii\]BLEACH 白刃きらめく輪舞曲
      - \[Wii\]BLEACH バーサス・クルセイド
  - [Under The Moon](../Page/Under_The_Moon.md "wikilink")（**賽玖**）
  - [Princess
    Nightmare](../Page/Princess_Nightmare.md "wikilink")（**ラドウ・ドラクレア**）
  - うたの☆プリンスさまっ♪（日向龍也）
  - \[PS3\][尼爾 人工生命](../Page/尼爾_\(遊戲\).md "wikilink")（**尼爾**〈青年時期〉）
  - \[PC\]（**白鷳**(ハッカン)）
  - \[PC\]籠中的艾莉希絲（**達倫**）
  - \[PSP\]十三支演義 〜偃月三國伝〜（**[張遼](../Page/張遼.md "wikilink")**）
  - \[PSP\][Fate/EXTRA CCC](../Page/Fate/EXTRA.md "wikilink")（フェイト／エクストラ
    CCC） - 「**迦爾納（カルナ）**」
  - \[PC\]假面后的真实(乙女向)（石川 类）
  - \[PSP\]ロミオVSジュリエット（ **マキューシオ＝エテルナ** ）
  - \[PSP\]NORN9 命運九重奏（ **加賀見一月** ）
  - \[PSV\]NORN9 命運九重奏 Last Era（ **加賀見一月** ）
  - \[手機遊戲\][Fate/Grand Order](../Page/Fate/Grand_Order.md "wikilink") -
    「**坂田金時（バーサーカー）**」

### 廣播劇CD

  - [黑執事系列](../Page/黑執事.md "wikilink")（劉）
      - 黑執事
      - 黒執事 華麗なるドラマCD
  - [新娘16歲](../Page/新娘16歲.md "wikilink")（尾白一馬）
  - [薄櫻鬼系列](../Page/薄櫻鬼.md "wikilink")（原田左之助）
      - 薄桜鬼 ドラマCD 〜新選組捕物控〜 前編・後編
      - 薄桜鬼 ドラマCD 〜若殿道中記〜
      - 薄桜鬼 ドラマCD 〜千鶴誘拐事件帳〜
      - 薄桜鬼 ドラマCD 〜島原騒動記〜
      - アニメ「薄桜鬼」ドラマCD vol.1
  - [黑色嘉年華系列](../Page/黑色嘉年華.md "wikilink")（朔）
      - 飛べない翼/人魚の瓶
      - リノル
      - ヴィント
  - [Starry☆Sky系列](../Page/Starry☆Sky.md "wikilink")（水嶋郁）
      - 星座彼氏シリーズ starry☆sky 〜Gemini〜（雙子座）
      - Starry☆Sky 〜in Autumn〜
      - Starry☆Sky 〜in Autumn〜 星的収穫浪漫譚
      - 星座旦那シリーズ Starry☆sky 〜Taurus\&Gemini〜
      - Starry☆Sky 〜in bitter season〜
      - Starry☆Sky 〜13constellation〜
      - Starry☆Sky 〜in Autumn〜Portable
      - Starry☆Sky 〜After Autumn〜
  - [葬儀屋リドル](../Page/葬儀屋リドル.md "wikilink")（riddleリドル）
  - [好想惡作劇系列](../Page/好想惡作劇.md "wikilink")（白羽七王）
      - 無慈悲的男人
  - 殺愛～宋良博

### 特攝影集

  - [-{zh:假面騎士;zh-hans:假面骑士;zh-hk:幪面超人;zh-tw:假面騎士;}-電王](../Page/幪面超人電王.md "wikilink")
    - 異魔神 浦塔羅斯/ 電王杖型態 Rod Form
  - [劇場版 假面騎士KIVA
    魔界城之王](../Page/劇場版_假面騎士KIVA_魔界城之王.md "wikilink")（棋藝社社員）
  - [假面騎士DECADE](../Page/假面騎士DECADE.md "wikilink")-浦塔羅斯

## 編劇

  - COLLECTION DVD  2 第12話

## 參考來源

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:旁白](../Category/旁白.md "wikilink")

1.  廣播Mobile文化放送《週刊·游佐浩二 第61回》。

2.  網路廣播『黑執事 Phantom Midnight Raido』「Radio Black Side」第2回。

3.  2007年2月7日更新的廣播Mobile文化放送《週刊·遊佐浩二》第38回，及3月9日更新的廣播《》第33回

4.  2012年6月23日更新的Mobile文化放送《週刊·遊佐浩二》第318回。

5.
6.

7.