**兴宁**（363年二月-365年）是[東晋皇帝](../Page/東晋.md "wikilink")[晉哀帝司馬丕的第二个](../Page/晉哀帝.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。

兴宁元年[二月](../Page/二月.md "wikilink")，[改元興寧](../Page/改元.md "wikilink")。二年[三月庚戌朔](../Page/三月.md "wikilink")、[庚戌](../Page/庚戌土斷.md "wikilink")[土斷頒佈](../Page/土斷.md "wikilink")。[八月](../Page/八月.md "wikilink")，[前燕奪取](../Page/前燕.md "wikilink")[洛陽](../Page/洛陽.md "wikilink")。三年二月，哀帝死；[廢帝司馬奕即位](../Page/晉廢帝.md "wikilink")，沿用至明年改元太和。

## 纪年

| 兴宁                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 363年                           | 364年                           | 365年                           |
| [干支](../Page/干支纪年.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [升平](../Page/升平.md "wikilink")：[前凉政权年号](../Page/前凉.md "wikilink")
      - [建熙](../Page/建熙.md "wikilink")（360年-370年十一月）：[前燕政权](../Page/前燕.md "wikilink")[慕容暐年号](../Page/慕容暐.md "wikilink")
      - [甘露](../Page/甘露.md "wikilink")（359年六月-364年）：[前秦政权](../Page/前秦.md "wikilink")[苻坚年号](../Page/苻坚.md "wikilink")
      - [建元](../Page/建元.md "wikilink")（365年-385年七月）：[前秦政权](../Page/前秦.md "wikilink")[苻坚年号](../Page/苻坚.md "wikilink")
      - [建国](../Page/建国.md "wikilink")（338年十一月-376年）：[代政权](../Page/代国.md "wikilink")[拓跋什翼犍年号](../Page/拓跋什翼犍.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:东晋年号](../Category/东晋年号.md "wikilink")
[Category:4世纪年号](../Category/4世纪年号.md "wikilink")
[Category:360年代中国政治](../Category/360年代中国政治.md "wikilink")