**中船海洋與防務裝備股份有限公司**，(,)，簡稱**中船防務**，前身為**广州广船国际股份有限公司**（简称**广船国际**），是[中國船舶工業集團公司屬下的](../Page/中國船舶工業集團公司.md "wikilink")[華南地區最大的現代化](../Page/華南地區.md "wikilink")[造船綜合企業](../Page/造船.md "wikilink")，也是目前在[香港上市的造船企業之一](../Page/香港.md "wikilink")。公司在1954年成立，並在1993年經改組在[香港交易所和](../Page/香港交易所.md "wikilink")[上海證券交易所分別發行](../Page/上海證券交易所.md "wikilink")[H股和](../Page/H股.md "wikilink")[A股](../Page/A股.md "wikilink")。现任公司[董事长兼党委书记是](../Page/董事长.md "wikilink")[陈景奇](../Page/陈景奇.md "wikilink")，副董事长兼[总经理是](../Page/总经理.md "wikilink")[韩广德](../Page/韩广德.md "wikilink")。
中船防務主要業務為建造及銷售[船舶](../Page/船舶.md "wikilink")；[製造及](../Page/製造.md "wikilink")[銷售](../Page/銷售.md "wikilink")[鋼結構及](../Page/鋼.md "wikilink")[機電設備](../Page/機電.md "wikilink")；[電腦銷售](../Page/電腦.md "wikilink")，[集裝箱](../Page/集裝箱.md "wikilink")[運輸及](../Page/運輸.md "wikilink")[修理船隻服務](../Page/修理.md "wikilink")，並在[廣東省](../Page/廣東省.md "wikilink")[廣州市和](../Page/廣州市.md "wikilink")[佛山市均設有](../Page/佛山市.md "wikilink")[造船廠](../Page/造船廠.md "wikilink")。\[1\]

中船防務受惠於[中國內地](../Page/中國內地.md "wikilink")[十一五規劃和國際造船業快速增長](../Page/中国五年计划.md "wikilink")，造船[訂單充裕](../Page/訂單.md "wikilink")，[純利自](../Page/純利.md "wikilink")2006年起以倍數增長。\[2\]A股和H股股價自2006年初的10元[人民幣及港元以下](../Page/人民幣.md "wikilink")，曾分別升至2007年10月最高的102.89元人民幣和68.5港元\[3\]，但其後因[美國](../Page/美國.md "wikilink")[次級按揭危機引發的大跌市而大幅回落](../Page/次級按揭危機.md "wikilink")。

廣船國際（317）2013年10月1日公佈，將向母公司[中船集團](../Page/中船集團.md "wikilink")、[寶鋼與](../Page/寶鋼.md "wikilink")[中國海運集團配股集資約](../Page/中國海運集團.md "wikilink")28.25億元，其中其中9.56億人民幣約12.05億元將用於向中船收購造船廠資產，變相獲母公司「半賣半送」華南地區最大造船廠「龍穴造船」，涉及資產總值107.88億元人民幣，不過亦同時背負約104.09億元人民幣的負債。

2014年2月12日廣船國際（0317）公告宣布，完成H股配售，向關聯人中國船舶（香港）航運租賃公司、寶鋼資源（國際）和中國海運（香港）三家機構共配售3.87億股H股，每股發行價7.29元，較昨日收市價18.78元折讓61.2%，籌資額達28.25億元。其中中船航運租賃公司斥資25.2億元，認購了3.45億股股份，佔擴大後股本的33.57%

广船国际最近几年的军船业绩主要包括大型综合补给舰 886 号“千岛湖舰”、889 号“太湖舰”，医院船 866 号“和平方舟”，潜艇支援舰
867 号“长岛舰”，航母训练支援舰 88 号“徐霞客舰”等。2014 年 5 月 30 日和 12月 20 日，广船制造的 903A
型综合补给舰 5 号舰和 6 号舰分别下水，两舰服役后将形成三大舰队各自拥有至少两艘综合补给舰。\[4\]

## 参考文献

## 外部链接

  - [廣州廣船國際股份有限公司](https://web.archive.org/web/20080905200328/http://www.chinagsi.com/)
  - [廣州造船廠](https://web.archive.org/web/20080315061144/http://www.cssc-gzs.com/)
  - [中国船舶工业集团公司企业列表](http://www.cssc.net.cn/component_general_situation/jtqy.php)

[Category:中国船舶工业集团](../Category/中国船舶工业集团.md "wikilink")
[Category:总部位于广州的中华人民共和国国有企业](../Category/总部位于广州的中华人民共和国国有企业.md "wikilink")
[Category:中华人民共和国军工企业](../Category/中华人民共和国军工企业.md "wikilink")
[Category:中國H股](../Category/中國H股.md "wikilink")
[Category:中华人民共和国造船企业](../Category/中华人民共和国造船企业.md "wikilink")
[Category:物流公司](../Category/物流公司.md "wikilink")
[Category:广州上市公司](../Category/广州上市公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")

1.  [廣州廣船國際公司簡介](http://www.chinagsi.com/cn/main/company.asp)
2.  [廣船國際首季利潤預計大增300%](http://indexchi.mysinablog.com/index.php?op=ViewArticle&articleId=542654)
3.  [广船国际跻身百元股
    成中国船舶第二](http://finance.sina.com.cn/stock/t/20071015/10071719399.shtml)
4.