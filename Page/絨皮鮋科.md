**絨皮鮋科**是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鮋形目的其中一科](../Page/鮋形目.md "wikilink")。

## 分類

**絨皮鮋科**下分17個屬，如下：

### 單棘鮋屬(*Acanthosphex*)

  - [印度單棘鮋](../Page/印度單棘鮋.md "wikilink")(*Acanthosphex leurynnis*)

### 脊首絨皮鮋屬(*Adventor*)

  - [長體脊首絨皮鮋](../Page/長體脊首絨皮鮋.md "wikilink")(*Adventor elongatus*)

### 疣鮋屬(絨皮鮋屬)(*Aploactis*)

  - [絨皮鮋](../Page/絨皮鮋.md "wikilink")(*Aploactis
    aspera*)：又稱[疣鮋](../Page/疣鮋.md "wikilink")。

### 擬絨皮鮋(擬疣鮋)屬(*Aploactisoma*)

  - [米氏擬絨皮鮋](../Page/米氏擬絨皮鮋.md "wikilink")(*Aploactisoma
    milesii*)：又稱米氏擬疣鮋。

### 深水絨皮鮋(深海疣鮋)屬(*Bathyaploactis*)

  - [深水絨皮鮋](../Page/深水絨皮鮋.md "wikilink")(*Bathyaploactis
    curtisensis*)：又稱深水疣鮋。
  - [飾妝深水絨皮鮋](../Page/飾妝深水絨皮鮋.md "wikilink")（*Bathyaploactis
    ornatissima*）：又稱飾妝深水疣鮋。

### 可哥鮋屬(*Cocotropus*)

  - [高鰭可哥鮋](../Page/高鰭可哥鮋.md "wikilink")(*Cocotropus altipinnis*)
  - [阿斯氏可哥鮋](../Page/阿斯氏可哥鮋.md "wikilink")（*Cocotropus astakhovi*）
  - [皮棘可哥鮋](../Page/皮棘可哥鮋.md "wikilink")(*Cocotropus dermacanthus*)
  - [多刺可哥鮋](../Page/多刺可哥鮋.md "wikilink")(*Cocotropus echinatus*)
  - [埃克斯可哥鮋](../Page/埃克斯可哥鮋.md "wikilink")（*Cocotropus eksae*）
  - [伊勢可哥鮋](../Page/伊勢可哥鮋.md "wikilink")（*Cocotropus izuensis*）
  - [慶良間島可哥鮋](../Page/慶良間島可哥鮋.md "wikilink")（*Cocotropus keramaensis*）
  - [魅可哥鮋](../Page/魅可哥鮋.md "wikilink")（*Cocotropus larvatus*）
  - [開田可可鮋](../Page/開田可可鮋.md "wikilink")(*Cocotropus masudai*)
  - [小眼可哥鮋](../Page/小眼可哥鮋.md "wikilink")（*Cocotropus microps*）
  - [單棘可哥鮋](../Page/單棘可哥鮋.md "wikilink")(*Cocotropus monacanthus*)
  - [波氏可哥鮋](../Page/波氏可哥鮋.md "wikilink")（*Cocotropus possi*）
  - [里氏可哥鮋](../Page/里氏可哥鮋.md "wikilink")（*Cocotropus richeri*）
  - [玫斑可哥鮋](../Page/玫斑可哥鮋.md "wikilink")（*Cocotropus roseomaculatus*）
  - [玫瑰可哥鮋](../Page/玫瑰可哥鮋.md "wikilink")(*Cocotropus roseus*)
  - [斯氏可哥鮋](../Page/斯氏可哥鮋.md "wikilink")(*Cocotropus steinitzi*)

### 絨鮋(虻鮋)屬(*Erisphex*)

  - [黑鰭絨鮋](../Page/黑鰭絨鮋.md "wikilink")（*Erisphex aniarus*）：又稱黑鰭虻鮋。
  - [菲律賓絨鮋](../Page/菲律賓絨鮋.md "wikilink")(*Erisphex
    philippinus*)：又稱菲律賓虻鮋。
  - [絨鮋](../Page/絨鮋.md "wikilink")(*Erisphex
    pottii*)：又稱蜂鮋、[虻鮋](../Page/虻鮋.md "wikilink")。
  - [平滑絨鮋](../Page/平滑絨鮋.md "wikilink")(*Erisphex simplex*)：又稱平滑虻鮋。

### 無鱗鮋屬(*Kanekonia*)

  - [佛羅里達無鱗鮋](../Page/佛羅里達無鱗鮋.md "wikilink")(*Kanekonia florida*)
  - [盾無鱗鮋](../Page/盾無鱗鮋.md "wikilink")(*Kanekonia pelta*)
  - [昆士蘭無鱗鮋](../Page/昆士蘭無鱗鮋.md "wikilink")(*Kanekonia queenslandica*)

### 松原絨皮鮋(松原疣鮋)屬(*Matsubarichthys*)

  - [松原絨皮鮋](../Page/松原絨皮鮋.md "wikilink")(*Matsubarichthys
    inusitatus*)：又稱松原疣鮋。

### 新絨皮鮋(新疣鮋)屬(*Neoaploactis*)

  - [三鰭新絨皮鮋](../Page/三鰭新絨皮鮋.md "wikilink")(*Neoaploactis
    tridorsalis*)：又稱三鰭新疣鮋。

### 副絨皮鮋屬(*Paraploactis*)

  - [香港副絨皮鮋](../Page/香港副絨皮鮋.md "wikilink")（*Paraploactis
    hongkongiensis*）
  - [西澳副絨皮鮋](../Page/西澳副絨皮鮋.md "wikilink")(*Paraploactis intonsa*)
  - [鹿兒島副絨皮鮋](../Page/鹿兒島副絨皮鮋.md "wikilink")(*Paraploactis
    kagoshimensis*)：又稱斑鰭絨棘鮋。
  - [奧氏副絨皮鮋](../Page/奧氏副絨皮鮋.md "wikilink")（*Paraploactis obbesi*）
  - [小枕副絨皮鮋](../Page/小枕副絨皮鮋.md "wikilink")(*Paraploactis pulvinus*)
  - [斯里蘭卡副絨皮鮋](../Page/斯里蘭卡副絨皮鮋.md "wikilink")（*Paraploactis
    taprobanensis*）
  - [粗皮副絨皮鮋](../Page/粗皮副絨皮鮋.md "wikilink")(*Paraploactis trachyderma*)

### 狡絨皮鮋(狡疣鮋)屬(*Peristrominous*)

  - [澳洲狡絨皮鮋](../Page/澳洲狡絨皮鮋.md "wikilink")(*Peristrominous
    dolosus*)：又稱澳洲狡疣鮋。

### 前肛鮋屬(*Prosoproctus*)

  - [前肛鮋](../Page/前肛鮋.md "wikilink")(*Prosoproctus pataecus*)

### 擬奇矮鮋屬(*Pseudopataecus*)

  - [肉鬚擬奇矮鮋](../Page/肉鬚擬奇矮鮋.md "wikilink")(*Pseudopataecus
    carnatobarbatus*)
  - [長鰭擬奇矮鮋](../Page/長鰭擬奇矮鮋.md "wikilink")（*Pseudopataecus taenianotus*）

### 毒疣鮋屬(*Ptarmus*)

  - [野毒疣鮋](../Page/野毒疣鮋.md "wikilink")(*Ptarmus gallus*)
  - [毒疣鮋](../Page/毒疣鮋.md "wikilink")(*Ptarmus jubatus*)

### 發鮋屬(*Sthenopus*)

  - [發鮋](../Page/發鮋.md "wikilink")(*Sthenopus mollis*)

### 奇絨鮋屬(*Xenaploactis*)

  - [呂宋奇絨鮋](../Page/呂宋奇絨鮋.md "wikilink")(*Xenaploactis anopta*)
  - [糙奇絨鮋](../Page/糙奇絨鮋.md "wikilink")(*Xenaploactis asperrima*)
  - [暹羅灣奇絨鮋](../Page/暹羅灣奇絨鮋.md "wikilink")（*Xenaploactis cautes*）

## 參考資料

1.  [台灣魚類資料庫](http://fishdb.sinica.edu.tw/AjaxTree/tree.php)

[\*](../Category/絨皮鮋科.md "wikilink")