是一部[香港](../Page/香港.md "wikilink")[電影](../Page/電影.md "wikilink")，2006年1月26日在香港上映。電影由[動畫及真人演出組合而成](../Page/動畫.md "wikilink")，由[趙良駿任](../Page/趙良駿.md "wikilink")[導演](../Page/導演.md "wikilink")。故事講述「春田花花幼稚園」裡一班小朋友，在長大後在社會工作的情況。電影中的歌曲《齋鹵味》由指揮[葉詠詩及](../Page/葉詠詩.md "wikilink")[香港小交響樂團一同演出](../Page/香港小交響樂團.md "wikilink")。

## 情节

在送雞迎狗的[大除夕](../Page/大除夕.md "wikilink")，[中環](../Page/中環.md "wikilink")[國際金融中心發生](../Page/國際金融中心_\(香港\).md "wikilink")[脅持人質事件](../Page/脅持人質.md "wikilink")，[特別任務連在門外準備](../Page/特別任務連.md "wikilink")[拯救人質](../Page/拯救人質.md "wikilink")，而賊人提出的第一個要求是「囉仔記」的窩蛋牛肉飯等外賣飯盒。這要求令警方掌握了重要的線索，原來事件與即將來臨的「春田花花幼稚園50週年」慶典有關。

當年「春田花花幼稚園」的小朋友，畢業後各自在各行各業發展，現在成為了「春田花花同學會」的一份子，但成年人的工作世界竟然同小時的世界一樣，目標不清的，大家最終期望其實只是「搵兩餐」。（「搵兩餐」在[粵語中字面意思是](../Page/粵語.md "wikilink")「找兩餐」，意思是勉强维持生活过日子，普通話是「討兩餐（或『討口飯』，更為可悲）吃」）。

## 演員表

  - [葉詠詩](../Page/葉詠詩.md "wikilink")
  - [尹子維](../Page/尹子維.md "wikilink")
  - [方力申](../Page/方力申.md "wikilink")
  - [池珍熙](../Page/池珍熙.md "wikilink")
  - [何超儀](../Page/何超儀.md "wikilink")
  - [余文樂](../Page/余文樂.md "wikilink")
  - [吳君如](../Page/吳君如.md "wikilink")
  - [吳彥祖](../Page/吳彥祖.md "wikilink")
  - [吳雨霏](../Page/吳雨霏.md "wikilink")
  - [岑建勳](../Page/岑建勳.md "wikilink")
  - [杜可風](../Page/杜可風.md "wikilink")
  - [李蘢怡](../Page/李蘢怡.md "wikilink")
  - [周筆暢](../Page/周筆暢.md "wikilink")
  - [房祖名](../Page/房祖名.md "wikilink")
  - [苗僑偉](../Page/苗僑偉.md "wikilink")
  - [林一峰](../Page/林一峰.md "wikilink")
  - [林海峰](../Page/林海峰.md "wikilink")
  - [夏春秋](../Page/夏春秋.md "wikilink")
  - [徐天佑](../Page/徐天佑.md "wikilink")
  - [陳子聰](../Page/陳子聰.md "wikilink")
  - [陳柏霖](../Page/陳柏霖.md "wikilink")
  - [陳慧琳](../Page/陳慧琳.md "wikilink")（配音 OL）
  - [連凱](../Page/連凱.md "wikilink")
  - [張兆輝](../Page/張兆輝.md "wikilink")
  - [張致恆](../Page/張致恆.md "wikilink")
  - [張達明](../Page/張達明.md "wikilink")
  - [張靚穎](../Page/張靚穎.md "wikilink")
  - [許紹雄](../Page/許紹雄.md "wikilink")（配音 黃啟順督察）
  - [梁洛施](../Page/梁洛施.md "wikilink")（配音 斬雞詠詩）
  - [梁詠琪](../Page/梁詠琪.md "wikilink")
  - [梁漢文](../Page/梁漢文.md "wikilink")
  - [傅穎](../Page/傅穎.md "wikilink")
  - [喬寶寶](../Page/喬寶寶.md "wikilink")
  - [曾志偉](../Page/曾志偉.md "wikilink")（配音
    [白雪公主](../Page/白雪公主.md "wikilink")）
  - [黃又南](../Page/黃又南.md "wikilink")
  - [黃秋生](../Page/黃秋生.md "wikilink")（配音 [船长](../Page/船长.md "wikilink")）
  - [吳鎮宇](../Page/吳鎮宇.md "wikilink")
  - [楊愛瑾](../Page/楊愛瑾.md "wikilink")
  - [詹瑞文](../Page/詹瑞文.md "wikilink")
  - [蔣志光](../Page/蔣志光.md "wikilink")
  - [鄧兆尊](../Page/鄧兆尊.md "wikilink")
  - [鄭中基](../Page/鄭中基.md "wikilink")
  - [區瑞強](../Page/區瑞強.md "wikilink")
  - [黎耀祥](../Page/黎耀祥.md "wikilink")
  - [劉以達](../Page/劉以達.md "wikilink")
  - [謝霆鋒](../Page/謝霆鋒.md "wikilink")
  - [梁慧恩](../Page/梁慧恩.md "wikilink")
  - [關智斌](../Page/關智斌.md "wikilink")
  - [Vitas](../Page/Vitas.md "wikilink")

## 參見

  - [麥嘜](../Page/麥嘜.md "wikilink")
  - [麥兜](../Page/麥兜.md "wikilink")
  - [麥兜故事](../Page/麥兜故事.md "wikilink")
  - [麥兜菠蘿油王子](../Page/麥兜菠蘿油王子.md "wikilink")
  - [春田花花中華博物館](../Page/春田花花中華博物館.md "wikilink")

## 外部連結

  - [**春田花花中華博物館**](http://www.rthk.org.hk/press/chi/20060419_66_120921.html)
    香港電台電視部 外判節目

  -
  - {{@movies|fmhk90499525|麥兜的春田花花同學會}}

  -
  -
  -
  -
  -
  -
[Category:香港动画电影](../Category/香港动画电影.md "wikilink")
[Category:真人動畫电影](../Category/真人動畫电影.md "wikilink")
[6](../Category/2000年代香港電影作品.md "wikilink")
[Category:麥嘜麥兜系列](../Category/麥嘜麥兜系列.md "wikilink")
[Category:银都机构电影](../Category/银都机构电影.md "wikilink")
[Category:赵良骏电影](../Category/赵良骏电影.md "wikilink")