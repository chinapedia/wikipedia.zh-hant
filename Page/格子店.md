[日本](https://zh.wikipedia.org/wiki/File:HK_Wan_Chai_Emperor_Group_Centre_Box-blog_1a_==_歷史_==_格仔鋪起源於[[日本 "fig:日本")，當時在日本的格仔鋪是寄賣[二手](../Page/二手.md "wikilink")[玩具](../Page/玩具.md "wikilink")，懷舊玩具為主，店內[裝修物料均以](../Page/裝修.md "wikilink")[透明](../Page/透明.md "wikilink")[膠櫃為主](../Page/膠.md "wikilink")，從而做出五方格一條或六方格一條的膠櫃，而每一格的呎價約為35x35x35cm。收費約400港幣到1000港幣不等，另收取5-10%佣金，收費則視乎位置及高度而定，而不同格仔店的服務條款也大有不同。

這一個新興行業慢慢地由日本傳至[台灣](../Page/台灣.md "wikilink")，早在約2001年時，在[台北市](../Page/台北市.md "wikilink")[西門町的](../Page/西門町.md "wikilink")[萬年商場內](../Page/萬年商場.md "wikilink")，已經開始有這類店鋪營業，而當時台灣也是用作寄賣玩具為主。另一方面，傳聞在香港的第一間格仔店約在2000年[中環開業](../Page/中環.md "wikilink")，因為那個時候的香港還沒有接受這種模式，所以很快[結業](../Page/清盤.md "wikilink")，繼而在打後幾年間，在香港的[旺角區開始漸漸出現](../Page/旺角區.md "wikilink")。在2007年，這類格仔店已經傳遍[深圳及](../Page/深圳.md "wikilink")[廣州等地](../Page/廣州.md "wikilink")，是[時尚的特色](../Page/時尚.md "wikilink")。

### 格仔店在港興起

在2006年，在旺角的格仔店大多數以[上樓鋪型式經營](../Page/上樓鋪.md "wikilink")，而比較為人熟悉的格仔店是位於[信和中心的Toyzone](../Page/信和中心.md "wikilink")，Toyzone除了在[旺角](../Page/旺角.md "wikilink")[信和中心外](../Page/信和中心.md "wikilink")，在[銅鑼灣區](../Page/銅鑼灣.md "wikilink")[糖街的](../Page/糖街.md "wikilink")[銅鑼灣中心亦有分店](../Page/銅鑼灣中心.md "wikilink")。而知名度較高的原因是始終他也是寄賣玩具為主，所以在玩具行業及客人很早已經習慣這消費模式。

在2006年以後，很多這類店鋪逐漸開幕，而早在[信和中心開業的Consignment](../Page/信和中心.md "wikilink")，[兆萬中心的multiland及始創中心的自家箱場最為人熟悉](../Page/兆萬中心.md "wikilink")，而與Toyzone不同之處是除了[寄賣玩具外](../Page/寄賣.md "wikilink")，更寄賣[首飾](../Page/首飾.md "wikilink")，[精品](../Page/精品.md "wikilink")，[鐘錶](../Page/鐘錶.md "wikilink")，[化粧品](../Page/化粧品.md "wikilink")，應有盡有，為小本经营人士提供多一個銷售機會。藝人[周星馳先生更在](../Page/周星馳.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")[加連威老道創立以格仔店概念為本的GI商場](../Page/加連威老道.md "wikilink")，給人一個嶄新的購物新體驗。

### 現今

時至今日，在香港或大或小的商場內，也可以看同不同型式的格仔店，現時的格仔店，除了寄賣玩具及精品外，更設有衣服寄賣，亦有不同的配套，服務等。

## 全香港有格仔店的商場

  - 香港島：[銅鑼灣地帶](../Page/銅鑼灣地帶.md "wikilink")、天后[蘋果商場](../Page/蘋果商場.md "wikilink")、[天悅廣場](../Page/天悅廣場.md "wikilink")、[柴灣](../Page/柴灣.md "wikilink")[永利購物中心](../Page/永利購物中心.md "wikilink")
  - 九龍半島：[信和中心](../Page/信和中心.md "wikilink")、[兆萬中心](../Page/兆萬中心.md "wikilink")、[始創中心](../Page/始創中心.md "wikilink")、[西九龍中心](../Page/西九龍中心.md "wikilink")、[藍田](../Page/藍田_\(香港\).md "wikilink")[滙景商場](../Page/滙景花園.md "wikilink")、[黃大仙中心](../Page/黃大仙中心.md "wikilink")、[旺角中心](../Page/旺角中心.md "wikilink")、[栢麗購物大道](../Page/栢麗購物大道.md "wikilink")
  - 新界：[葵涌廣場](../Page/葵涌廣場.md "wikilink")、[新都會廣場](../Page/新都會廣場.md "wikilink")、[荃灣城市中心](../Page/荃灣城市中心.md "wikilink")、[綠楊坊](../Page/綠楊坊.md "wikilink")、[荃新天地](../Page/荃新天地.md "wikilink")、天水圍[新天地廣場](../Page/新天地廣場.md "wikilink")、天水圍[頌富廣場](../Page/頌富廣場.md "wikilink")、元朗[嘉城廣場](../Page/嘉城廣場.md "wikilink")、屯門[海趣坊](../Page/海趣坊.md "wikilink")、[華都商場](../Page/華都花園.md "wikilink")、[雅都商場](../Page/雅都商場.md "wikilink")、[都會駅](../Page/都會駅.md "wikilink")、[新都城中心](../Page/新都城中心.md "wikilink")、[禾輋商場](../Page/禾輋邨.md "wikilink")、[好運中心](../Page/好運中心.md "wikilink")、[連城廣場](../Page/連城廣場.md "wikilink")、[大埔廣場](../Page/大埔廣場.md "wikilink")、粉嶺[花都購物廣場](../Page/花都廣場#購物商場.md "wikilink")、上水[龍豐花園商場](../Page/龍豐花園.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [香港格仔店聯盟 -
    首個香港格仔店/格仔舖的資料庫](https://web.archive.org/web/20140413144930/http://www.hkcubeshop.org/)（互聯網檔案館，存檔於2014年4月13日）

[Category:零售商](../Category/零售商.md "wikilink")