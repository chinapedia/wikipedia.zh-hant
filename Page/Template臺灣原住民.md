[賽德克](../Page/賽德克族.md "wikilink") |group2 =
[排灣語群](../Page/排灣語群.md "wikilink") |list2 =
[阿美](../Page/阿美族.md "wikilink"){{.w}}[排灣](../Page/排灣族.md "wikilink"){{.w}}[布農](../Page/布農族.md "wikilink"){{.w}}[卑南](../Page/卑南族.md "wikilink"){{.w}}[賽夏](../Page/賽夏族.md "wikilink"){{.w}}[邵](../Page/邵族.md "wikilink"){{.w}}[噶瑪蘭](../Page/噶瑪蘭族.md "wikilink"){{.w}}[撒奇萊雅](../Page/撒奇萊雅族.md "wikilink")
|group3 = [鄒語群](../Page/鄒語群.md "wikilink") |list3 =
[鄒](../Page/鄒族.md "wikilink"){{.w}}[卡那卡那富](../Page/卡那卡那富族.md "wikilink"){{.w}}[拉阿魯哇](../Page/拉阿魯哇族.md "wikilink")
|group4 = [魯凱語群](../Page/魯凱語.md "wikilink") |list4 =
[魯凱](../Page/魯凱族.md "wikilink") |group5 =
[巴丹語群](../Page/巴丹語群.md "wikilink") |list5 =
[達悟<small>／雅美</small>](../Page/達悟族.md "wikilink") }} |group2 =
[地方政府認定族群](../Page/地方政府.md "wikilink") |list2 =
[大武壠<small>／大滿</small>](../Page/大武壠族.md "wikilink")<small>（[花蓮富里](../Page/富里鄉.md "wikilink")）</small>{{.w}}[馬卡道](../Page/馬卡道族.md "wikilink")<small>（[屏東](../Page/屏東縣政府.md "wikilink")、[花蓮富里](../Page/富里鄉.md "wikilink")）</small>
}} |group3 = [非官方認定族群](../Page/台灣未識別民族.md "wikilink") |list3 =
[雷朗](../Page/雷朗族.md "wikilink"){{·w}}[哆囉美遠](../Page/哆囉美遠族.md "wikilink")）{{.w}}[龜崙](../Page/龜崙族.md "wikilink"){{.w}}[巴宰<small>／巴則海</small>](../Page/巴宰族.md "wikilink"){{.w}}[噶哈巫](../Page/噶哈巫族.md "wikilink"){{.w}}[巴布薩](../Page/巴布薩族.md "wikilink"){{.w}}[洪雅<small>／和安雅</small>](../Page/洪雅族.md "wikilink")（[阿立昆](../Page/阿立昆族.md "wikilink"){{·w}}[羅亞](../Page/羅亞族.md "wikilink")）{{.w}}[拍瀑拉<small>／巴布拉</small>](../Page/拍瀑拉族.md "wikilink"){{.w}}[道卡斯](../Page/道卡斯族.md "wikilink"){{.w}}[猴猴](../Page/猴猴族.md "wikilink"){{·w}}[達谷布亞努](../Page/達谷布亞努族.md "wikilink"){{·w}}[箕模](../Page/箕模族.md "wikilink"){{·w}}[斯卡羅](../Page/斯卡羅族.md "wikilink")
|group2 = [魯凱語群](../Page/魯凱語.md "wikilink") |list2 =
[歐佈諾伙](../Page/魯凱族#語言群別.md "wikilink"){{·w}}[德樂日卡](../Page/魯凱族#語言群別.md "wikilink"){{·w}}[古納達望](../Page/魯凱族#語言群別.md "wikilink")
}} |group4 = 統治管理區分 |list4 =
[高山族](../Page/高山族.md "wikilink"){{.w}}[平埔族群](../Page/平埔族群.md "wikilink")
|group5 = 相關條目 |list5 = [福爾摩沙人](../Page/福爾摩沙人.md "wikilink")
}}<noinclude>

</noinclude>

[\*](../Category/台灣原住民.md "wikilink")
[Category:民族模板](../Category/民族模板.md "wikilink")