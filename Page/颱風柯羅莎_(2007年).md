颱-{}-風羅莎}}    **颱風柯羅莎**
（[英語](../Page/英語.md "wikilink")：****，菲律賓大氣地理天文部門：**Ineng**，國際編號：**0715**，[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")：**17W**）是[2007年太平洋颱風季的一個](../Page/2007年太平洋颱風季.md "wikilink")[熱帶氣旋](../Page/熱帶氣旋.md "wikilink")。「柯羅莎」這個名稱是由[柬埔寨所命名](../Page/柬埔寨.md "wikilink")，指的是「[鶴](../Page/鶴.md "wikilink")」這種鳥類。

## 發展過程

柯羅莎是由2007年第91號[熱帶擾動生成](../Page/熱帶擾動.md "wikilink")，[美國](../Page/美國.md "wikilink")[聯合颱風警報中心於](../Page/聯合颱風警報中心.md "wikilink")10月1日0時([UTC](../Page/協調世界時.md "wikilink"))發布2007年第17號[熱帶低氣壓生成](../Page/熱帶低氣壓.md "wikilink")。[日本氣象廳於當地時間](../Page/日本氣象廳.md "wikilink")10月1日14時發布颱風羅莎形成，是最早發布颱風羅莎形成的單位，當時柯羅莎位於[菲律賓東方海面上](../Page/菲律賓.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[中央氣象局也於台北時間](../Page/中央氣象局.md "wikilink")（[UTC+8](../Page/UTC+8.md "wikilink")）上午10時左右，發布第15號颱風形成。

柯羅莎路徑类似於1996年的[颱風賀伯](../Page/颱風賀伯.md "wikilink")\[1\]。台灣中央氣象局於10月4日17時30分，對台灣東部海域發布[海上颱風警報第](../Page/海上颱風警報.md "wikilink")1報，在10月5日5時30分的第5報中，對台灣海上陸上皆發出警報。10月6日22時30分，柯羅莎在[台灣](../Page/台灣.md "wikilink")[宜蘭縣](../Page/宜蘭縣.md "wikilink")[頭城鎮進入北部陸地](../Page/頭城鎮.md "wikilink")。10月7日17點30分，台灣中央氣象局解除台灣本島的[陸上颱風警報](../Page/陸上颱風警報.md "wikilink")，23時30分解除海上颱風警報，整個過程共發了26個警報單。聯合颱風警報中心則是於10月8日18時(UTC)解除柯羅莎的颱風警報。

根據台灣中央氣象局所測得的資料，柯羅莎威力最強時的中心最低氣壓為925[百帕斯卡](../Page/百帕斯卡.md "wikilink")，7級暴風半徑300[公里](../Page/公里.md "wikilink")，近中心最大風速有16級風，瞬間最大陣風達17級風。

羅莎在橫過中國福建至浙江一帶後，減弱為熱帶風暴並改向偏東北移動，移出東海並轉化為溫帶氣旋。柯羅莎在西風帶下加速移動，以偏東路徑掠過日本，10月12日更穿越國際換日線，13日才正式消散。

## 影響

###

  -
    當地發佈最高颱風警報：[海上陸上颱風警報](../Page/海上陸上颱風警報.md "wikilink")

#### 背景

台灣常將於秋季生成的颱風歸類秋颱，在柯羅莎之前10年，有4個較典型的秋颱影響台灣，分別是[颱風謝柏](../Page/颱風謝柏_\(1998年\).md "wikilink")([1998年](../Page/1998年太平洋颱風季.md "wikilink"))、[颱風寶絲](../Page/颱風寶絲_\(1998年\).md "wikilink")([1998年](../Page/1998年太平洋颱風季.md "wikilink"))、[颱風象神](../Page/颱風象神_\(2000年\).md "wikilink")([2000年](../Page/2000年太平洋颱風季.md "wikilink"))、[颱風納坦](../Page/2004年太平洋颱風季#颱風納坦_\(Nock-ten\).md "wikilink")([2004年](../Page/2004年太平洋颱風季.md "wikilink"))\[2\]。對台灣而言，典型的秋颱生成的位置主要在台灣以南的海面上，並不一定會直接登陸台灣，但是漸強的[東北季風所形成的](../Page/東北季風.md "wikilink")[共伴效應](../Page/共伴效應.md "wikilink")，會對[北台灣帶來較多的雨勢](../Page/北台灣.md "wikilink")，但是柯羅莎雖然在10月形成，但是形成的位置是在台灣東向的海面上，行徑路線也和夏季生成的颱風相似。在羅莎接近台灣的過程中，中央氣象局也一再上修北台灣雨量預測，至10月6日23時止之統計，台灣南部的[嘉義縣](../Page/嘉義縣.md "wikilink")[奮起湖測得累積](../Page/奮起湖.md "wikilink")1117.5毫米的雨量，次高的觀測點為[宜蘭縣太平山](../Page/宜蘭縣.md "wikilink")，有1072毫米的累積雨量\[3\]。

#### 災情

[宜蘭高中因颱風造成活動中心坍塌.jpg](https://zh.wikipedia.org/wiki/File:宜蘭高中因颱風造成活動中心坍塌.jpg "fig:宜蘭高中因颱風造成活動中心坍塌.jpg")

  - 全台曾有233萬戶[停電](../Page/停電.md "wikilink")\[4\]，僅次於1996年[颱風賀伯的](../Page/颱風賀伯.md "wikilink")279萬戶停電，累計共有130萬戶受到影響，主要集中於[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[臺北縣](../Page/新北市.md "wikilink")、[台北市](../Page/台北市.md "wikilink")、[基隆市等北部縣市](../Page/基隆市.md "wikilink")。\[5\]
  - [台9線蘇花公路](../Page/台9線.md "wikilink")、[台7線北橫公路](../Page/台7線.md "wikilink")、[台7甲線中橫宜蘭支線](../Page/台7線#甲線.md "wikilink")、[台18線阿里山公路等交通要道曾一度中斷](../Page/台18線.md "wikilink")，其中[阿里山森林鐵路受損嚴重](../Page/阿里山森林鐵路.md "wikilink")，預估需花費15個工作天才能修復。\[6\]
  - 主要[蔬菜產區產量受到影響](../Page/蔬菜.md "wikilink")，加上[梨山等高山產區對外交通中斷](../Page/梨山.md "wikilink")，秋季菜價創下10年前颱風賀伯以來的新高價位。\[7\]
  - 根據[行政院農業委員會的統計](../Page/行政院農業委員會.md "wikilink")，截至10月9日下午為止，台灣各地農業損失總金額約[新台幣](../Page/新台幣.md "wikilink")42.7億元，共計有苗栗縣、南投縣、嘉義縣、台南縣等13個縣達到[災害管理救助標準](../Page/災害管理.md "wikilink")，損失總額已超過2006年全年損失總額。主要受害作物為[香蕉](../Page/香蕉.md "wikilink")、[二期稻作](../Page/水稻.md "wikilink")、[柿子](../Page/柿子.md "wikilink")、[椪柑](../Page/椪柑.md "wikilink")、[巨峰葡萄](../Page/葡萄.md "wikilink")、[芭樂](../Page/番石榴.md "wikilink")、[木瓜](../Page/木瓜属.md "wikilink")、[蓮霧等](../Page/蓮霧.md "wikilink")。\[8\]

<!-- end list -->

  - [台7甲線](../Page/台7線#甲線.md "wikilink")[宜蘭境內路段落石嚴重](../Page/宜蘭縣.md "wikilink")，單線通車。\[9\]
  - [蘇花公路](../Page/蘇花公路.md "wikilink")141.9公里處坍塌。\[10\]
  - [台6線](../Page/台6線.md "wikilink")[苗栗市附近路面因](../Page/苗栗市.md "wikilink")[後龍溪溪水暴漲](../Page/後龍溪.md "wikilink")，路基流失約20[公尺](../Page/公尺.md "wikilink")。\[11\]
  - [台7線](../Page/台7線.md "wikilink")（[北橫公路](../Page/北橫公路.md "wikilink")）在[桃園縣](../Page/桃園市.md "wikilink")[復興鄉榮華及四稜交通雙向阻斷](../Page/復興區_\(桃園市\).md "wikilink")。\[12\]
  - [台20線](../Page/台20線.md "wikilink")（[南橫公路](../Page/南橫公路.md "wikilink")）[台東縣](../Page/台東縣.md "wikilink")[海端鄉](../Page/海端鄉.md "wikilink")[大關山隧道附近邊坡土石坍方](../Page/大關山隧道.md "wikilink")。\[13\]
  - [台27線](../Page/台27線.md "wikilink")[高雄縣](../Page/高雄縣.md "wikilink")[六龜鄉葫蘆谷附近道路坍方](../Page/六龜區.md "wikilink")。\[14\]
  - [台北市](../Page/台北市.md "wikilink")[陽明山新安路一處民宅發生土石崩落](../Page/陽明山.md "wikilink")，有6人獲救送醫，有2人遭活埋。\[15\]
  - [台北市](../Page/台北市.md "wikilink")[北投區中央南路一帶大淹水](../Page/北投區.md "wikilink")，水深一度及腰。\[16\]

## 熱帶氣旋警告使用紀錄

### 台灣

－{{\#time:Y.m.d H:i|2007-10-07 23:30}}
|上一熱帶氣旋=[中度颱風韋帕](../Page/颱風韋帕_\(2007年\).md "wikilink")
|下一熱帶氣旋=[中度颱風米塔](../Page/颱風米塔_\(2007年\).md "wikilink")
|2熱帶氣旋警告=[陸上颱風警報](../Page/陸上颱風警報.md "wikilink")
|2使用時間={{\#time:Y.m.d H:i|2007-10-05 05:30}}－{{\#time:Y.m.d
H:i|2007-10-07 20:30}}
|2上一熱帶氣旋=[中度颱風韋帕](../Page/颱風韋帕_\(2007年\).md "wikilink")
|2下一熱帶氣旋=[中度颱風卡玫基](../Page/颱風卡玫基_\(2008年\).md "wikilink") }}

## 圖片庫

Krosa 04 oct 2007 0440Z.jpg|10月4日 Krosa 05 oct 2007 0215Z.jpg|10月5日

## 參看

  - [2007年太平洋颱風季](../Page/2007年太平洋颱風季.md "wikilink")

## 参考來源

## 外部連結

  - <https://web.archive.org/web/20090826230250/http://metocph.nmci.navy.mil/jtwc.php>
    [聯合颱風警報中心首頁](../Page/聯合颱風警報中心.md "wikilink")

  - <http://www.jma.go.jp/jma/index.html>
    [日本氣象廳首頁](../Page/日本氣象廳.md "wikilink")

  - <http://www.cwb.gov.tw> [中央氣象局首頁](../Page/中央氣象局.md "wikilink")

  - <http://www.pagasa.dost.gov.ph/>
    [菲律賓大氣地球物理和天文管理局首頁](../Page/菲律賓大氣地球物理和天文管理局.md "wikilink")

  - [TDB防災颱風資料庫網頁系統](http://rdc28.cwb.gov.tw/data.php?num=2007151002&year=2007&c_name=%AC_%C3%B9%B2%EF&e_name=KROSA)

[category:2007年太平洋颱風季](../Page/category:2007年太平洋颱風季.md "wikilink")

[颱風](../Category/2007年台灣.md "wikilink")
[Category:影響臺灣的熱帶氣旋](../Category/影響臺灣的熱帶氣旋.md "wikilink")
[Category:四级热带气旋](../Category/四级热带气旋.md "wikilink")

1.  [賀伯路徑圖](http://rdc28.cwb.gov.tw/data.php?num=1996080724&year=1996&c_name=%B6P%A7B&e_name=HERB)

2.  [TDB防災颱風資料庫網頁系統](http://rdc28.cwb.gov.tw/data.php)

3.  [中央氣象局](http://www.cwb.gov.tw/)

4.  <http://rdc28.cwb.gov.tw/data.php?num=2007151002&year=2007&c_name=柯羅莎&e_name=KROSA>

5.  <http://times.hinet.net/news/20071006/headline/71d5d3644959.htm>

6.

7.  <http://times.hinet.net/news/20071007/headline/31ffeb6b1b5b.htm>

8.  [農損37億
    超過去年總額](http://www.libertytimes.com.tw/2007/new/oct/10/today-life6.htm)，《自由時報》，2007年10月10日。

9.  <http://udn.com/NEWS/NATIONAL/NAT3/4043414.shtml>

10. <http://udn.com/NEWS/NATIONAL/NAT3/4043416.shtml>

11. <http://udn.com/NEWS/NATIONAL/NAT3/4043425.shtml>

12.
13.
14.
15. <http://udn.com/NEWS/NATIONAL/NAT3/4043406.shtml>

16. <http://udn.com/NEWS/NATIONAL/NAT3/4043780.shtml>