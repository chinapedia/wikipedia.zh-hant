**Art. Lebedev
Studio**是[俄羅斯最大的](../Page/俄羅斯.md "wikilink")[設計公司](../Page/設計.md "wikilink")，於1995年由[Artemy
Lebedev創立](../Page/Artemy_Lebedev.md "wikilink")。他們同時創作[工業與](../Page/工業設計.md "wikilink")[平面設計](../Page/平面設計.md "wikilink")；他們的[訓辭為](../Page/訓辭.md "wikilink")"*設計將拯救世界*"。2005年為止他們擁有六位首席[藝術總監與超過](../Page/藝術總監.md "wikilink")100名員工。

## 經歷與工程

該工作室以平面藝術起家，之後擴展到包括[用戶界面與](../Page/用戶界面.md "wikilink")[網頁設計](../Page/網頁設計.md "wikilink")，而最近亦包括了工業設計。它擁有自己的教育中心、出版社、媒體部與數支[軟體隊伍](../Page/軟體.md "wikilink")。

該工作室的政策為只為商業個體工作，而不會接受私人平民與政黨、政府或宗教組織的工作。

### 工作室網站

該工作室的官方網站提供[俄語](../Page/俄語.md "wikilink")、[法語與](../Page/法語.md "wikilink")[英語](../Page/英語.md "wikilink")。特別的是該網站像印刷本的排版；例如標點符號與及字元Ф、О、C、Т、Л、Я、G、Q、W、V、t、f、т、1與0，當它們在每行開始時會提供數個像素予左側（使用[CSS時](../Page/CSS.md "wikilink")）因為他們有一部份會延伸到左側並移動以增加流動與可讀性。

該網站亦有提供其設計概念的遠見，案例中的工作例子經常會詳細解釋 – 他們要表達的東西與他們如何令該主題更易被大眾理解。

在俄羅斯版的網頁上會免費提供該設計室設計師累積經歷所得的[HTML](../Page/HTML.md "wikilink")、[XML](../Page/XML.md "wikilink")、[XSLT](../Page/XSLT.md "wikilink")、[JavaScript](../Page/JavaScript.md "wikilink")、CSS及其他網頁技術建議。

### 招聘競爭

該設計室會定期於網站上進行公開招聘，應徵者會被要求提供以自己的方式證明自己的能力。例如平面設計師要將一張已撞毀不動汽車的圖片修飾到完全修理完畢並在高速公路上行駛。網頁設計師通常會以隱藏在網頁的[復活節彩蛋或HTML](../Page/彩蛋_\(視覺\).md "wikilink")[註解的方式進行招聘](../Page/註解.md "wikilink")。

### 網站設計

在俄羅斯該設計室最為人熟悉就是其網站設計計劃。因他們的創立人，在該領域貢獻最大的人，發明了[可擴放性](../Page/可擴放性.md "wikilink")[網站](../Page/網站.md "wikilink")，用以將圖片縮小或放大以放置到適當的空間。

### 工業設計

2005年5月三星電子發售由該設計室設計的Sweetheart[微波爐](../Page/微波爐.md "wikilink")，如此大型的企業雇用一家俄羅斯設計公司在當時非常罕見。該設計室最近因其可自訂每個按鍵顯示圖案的[Optimus鍵盤](../Page/Optimus鍵盤.md "wikilink")[原型而被媒體廣泛報道](../Page/原型.md "wikilink")。

## 參考

  - [Parser](../Page/Parser.md "wikilink") –
    該設計室為其網絡計劃而開發的網頁腳本語言，以[免費軟體方式發佈](../Page/免費軟體.md "wikilink")。

## 外部連結

  - [官方網站](http://www.artlebedev.com/)
  - [官方全球商店](http://store.artlebedev.com/)
  - 使用他們獨有的 [Artemius字體家族](http://www.artlebedev.com/company/artemius/)
    的多種語言 [公司口號](http://www.artlebedev.com/company/slogan/)。

[Category:俄羅斯公司](../Category/俄羅斯公司.md "wikilink")
[Category:工業設計公司](../Category/工業設計公司.md "wikilink")