[thumb遷台之前的台灣南島語言分布圖](../Page/文件:Formosan_languages.png.md "wikilink")(按
Blust,
1999)\[1\].東台灣"[蘭嶼島](../Page/蘭嶼.md "wikilink")(深紅色)表示為使用[馬來-玻里尼西亞語族](../Page/馬來-玻里尼西亞語族.md "wikilink")[巴丹語群](../Page/巴丹語群.md "wikilink")[達悟語的區域](../Page/達悟語.md "wikilink").\]\]

**阿美語**（[阿美文](../Page/原住民族語羅馬字.md "wikilink")：、）是[南島語系的一種](../Page/南島語系.md "wikilink")，用[拉丁字母書寫](../Page/拉丁字母.md "wikilink")，是[台灣原住民](../Page/台灣原住民.md "wikilink")[阿美族的民族語言](../Page/阿美族.md "wikilink")，也是目前[臺灣南島語言當中使用人數最多的語言之一](../Page/臺灣南島語言.md "wikilink")。依據2000年[立法院](../Page/立法院.md "wikilink")[三讀通過的](../Page/三讀通過.md "wikilink")《大眾運輸工具播音語言平等保障法》，原本在阿美族人較多數的地區或公共場所，包括[花蓮車站](../Page/花蓮車站.md "wikilink")、[臺東車站](../Page/臺東車站.md "wikilink")、[知本車站等地](../Page/知本車站.md "wikilink")，以及[花東線的列車](../Page/花東線.md "wikilink")，會播放阿美語。

## 語名由來

**Amis**這個字為他族稱阿美族之用法，原在[阿美語](../Page/阿美語.md "wikilink")、[卑南語](../Page/卑南語.md "wikilink")、[巴賽語等語中](../Page/巴賽語.md "wikilink")，amis
均為「北方」之意。\[2\] 若是阿美族自稱，則一般會用
**Pangcah**（邦查、意思為「人」或「同族」，也是「平坦台地」和「居住在平坦台地之人」的意思），如花蓮地區以及長濱鄉等地阿美族大都以此自稱。\[3\]\[4\]

## 方言群

現代阿美語因早期地域上的隔離，有了以下幾個方言群：**南勢阿美語**、**秀姑巒阿美語**、**海岸阿美語**、**馬蘭阿美語**和**恆春阿美語**等。

台灣原住民阿美語認證考試也因此分為5類語群: \[5\]

  - 南勢阿美語 ('Amisay a Pangcah)
  - 秀姑巒阿美語 (Siwkolan 'Amis)
  - 海岸阿美語 (Pasawalian Pangcah)
  - 馬蘭阿美語 (Farangaw Amis)
  - 恆春阿美語 (Palidaw 'Amis)

## 語音系統

阿美語音系之[音位大都使用適當的](../Page/音位.md "wikilink")[Unicode符號來標示](../Page/Unicode.md "wikilink")。在[台灣南島語的](../Page/台灣南島語.md "wikilink")[書寫系統的訂定上](../Page/書寫系統.md "wikilink")，元輔音之音系表原則上都是先「[發音部位](../Page/發音部位.md "wikilink")」(橫列)、後「[發音方法](../Page/發音方法.md "wikilink")」(縱列)、再考量「[清濁音](../Page/清濁音.md "wikilink")」，來訂定其音系之音位架構。\[6\]

## 語法架構

在語法的分類上，[台灣南島語並不同於一般的](../Page/台灣南島語.md "wikilink")[分析語或其它](../Page/分析語.md "wikilink")[综合语裡的動詞](../Page/综合语.md "wikilink")、名詞、形容詞、介詞和副詞等之基本[詞類分類](../Page/詞類.md "wikilink")。比如台灣南島語裡普遍沒有副詞，而副詞的概念，一般以動詞方式呈現，可稱之為「副動詞」，類之於[俄语裡的副動詞](../Page/俄语.md "wikilink")。\[7\]
阿美語語法分類是將基礎的語法之詞類、詞綴、字詞結構及分類法，對比[分析語等之詞類分類法加以條析判別](../Page/分析語.md "wikilink")。\[8\]
在語詞的表達上，是以其它地區的阿美語作為基礎參考，而以[中部阿美語](../Page/中部阿美語.md "wikilink")（秀姑巒阿美語/Siwkolan
'Amis）作為一般通行參考。

## 例句

  - Kita salikaka mangta'ay to ko
    Kalingko：我們即將抵達花蓮（[臺鐵所使用的阿美語廣播](../Page/臺灣鐵路管理局.md "wikilink")）

## 参考文献

### 引用

### 来源

  - Namoh Rata (吳明義)：*O pidafo'an to Sowal Misanopangcah*
    (《阿美族語詞典》),南天書局,台北,2013年12月. <small>ISBN
    978-957-43-1030-2</small>

  - 王文娟："原住民族語言書寫系統符號教學研究─以Akiyo教師阿美族語教學為例",國立東華大學民族發展研究所,博碩士論文,2007/06/01-2008/06/01.

  - Ci Tasang
    (林登仙)：《台灣Amis語語料的解讀與分析》,[中央民族大學出版社](../Page/中央民族大學出版社.md "wikilink"),北京,2012年6月.
    <small>ISBN 978-7-5660-0201-3</small>

  - 張裕龍（新竹教育大學台灣語言與語文教育研究所碩士）："中部阿美語否定詞研究－祈使句的語法結構",2007全國原住民族研究論文集
    學生組佳作,2007.

  - Sing 'Olam (星·歐拉姆)、曾思奇：''O Sakafana' to Rayray to Sowal no 'Amis
    ''(《阿美族語實用語法》),台北/使徒出版有限公司,2007年4月. <small>ISBN
    978-986-82986-0-6</small>

  - 曾思奇/楊梅：《台灣阿美語基礎教程》,中央民族大學出版社,北京,2006年10月. <small>ISBN
    7-81108-294-2</small>

  - 吳美蘭：《阿美語參考語法》(Amis<Pangcah>
    Grammar),臺北,[遠流出版公司](../Page/遠流出版公司.md "wikilink"),台北,2005年1月1日.
    <small>ISBN 957-32-3897-7</small>

  - 魏廷冀："阿美語疑問詞研究"[7](http://www.ling.sinica.edu.tw/files/publication/j2009_2_05_9065.pdf),Language
    and Linguistics,10.2:315-374,2009.

  - Tiway Saion (帝瓦伊·撒耘)：*O Lailay no toas ni Pangcah ato Sakizaya
    Cikasoan* (《阿美語族群諺語》第一冊),台北/德英國際有限公司出版,2005年9月. <small>ISBN
    986-81472-0-4</small>

  - 曾思奇、蔡中涵,1997：《阿美族母語會話句型》,臺北/台灣原住民基金會.<small>ISBN
    957-98515-1-4</small>

  - 曾思奇、蔡中涵,1997：《阿美族母語語法結構分析》,臺北/台灣原住民基金會.<small>ISBN
    957-98515-0-6</small>

  -
  - Gils, Rémy, 2010."Parlons amis: Une langue aborigène de Taïwan",
    Paris, L'Harmattan. <small>ISBN 2296114652</small>

  -
## 外部連結

  - [政大原民語教文中心(含阿美語認證題庫)](http://www.alcd.nccu.edu.tw/)
  - [國立臺灣師範大學-進修推廣學院 NTNU-SCE](http://www.sce.ntnu.edu.tw/)
  - [葉美利課程網頁-台灣語言之旅](https://web.archive.org/web/20100330020545/http://taiwanlg.web.nhcue.edu.tw/front/bin/home.phtml)
  - [澎湖縣教育影音網 -
    本土語言課程-阿美語2](http://video.phc.edu.tw/mediadetails.php?key=e89b0e4aabacf14f1b39&title=本土語言課程-阿美語2)

## 參見

  - [斯瓦迪士核心詞列表](../Page/斯瓦迪士核心詞列表.md "wikilink")
  - [原住民族語能力認證](../Page/原住民族語能力認證.md "wikilink")
  - [原住民族語言書寫系統 (中華民國)](../Page/原住民族語言書寫系統_\(中華民國\).md "wikilink")
  - [族語新聞](../Page/族語新聞.md "wikilink")
  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")
  - [原住民部落社區大學](../Page/原住民部落社區大學.md "wikilink")
  - [臺灣原住民命名文化](../Page/臺灣原住民命名文化.md "wikilink")
  - [原始人類語言](../Page/原始人類語言.md "wikilink")

{{-}}

[阿美語](../Category/阿美語.md "wikilink")
[Category:謂主賓語序語言](../Category/謂主賓語序語言.md "wikilink")

1.  Blust, R. (1999). "Subgrouping, circularity and extinction: some
    issues in Austronesian comparative linguistics" in E. Zeitoun &
    P.J.K Li (Ed.) Selected papers from the Eighth International
    Conference on Austronesian Linguistics (pp. 31-94). Taipei: Academia
    Sinica.
2.  李壬癸/中央研究院,"巴賽語的地位"[1](http://www.ling.sinica.edu.tw/eip/FILES/journal/2007.3.9.15528505.8852881.pdf),LANGUAGE
    AND LINGUISTICS 2.2:155-171, 2001.
3.  主編 Sing‘Olam,"O Citing no
    Pangcah(阿美語簡明詞典)",台灣族群母語推行委員會,2011/06/20.<small>ISBN
    978-986-86936-3-0</small>
4.  李文成(阿美族),"對於阿美族名的商議",原教界-原住民族教育報誌(Aboriginal Education
    World),第12期/2006年12月號,pp.91-93.<small>ISBN
    978-986-86936-3-0</small>
5.  國立台灣師範大學進修推廣學院,"100年度原住民族語言能力認證考試"：[2](http://ipt.sce.ntnu.edu.tw/100ipt/viewpage.php?page_id=11),臺北市,2011.
6.  行政院原住民族委員會,"原住民族語言書寫系統"[3](http://www.edu.tw/files/list/M0001/aboriginal.pdf),台語字第0940163297號,原民教字第09400355912號公告,中華民國94年12月15日.
7.  張永利,"台灣南島語言語法：語言類型與理論的啟示(Kavalan)"[4](http://vietnam.nsc.gov.tw/public/Data/012714292271.pdf),語言學門熱門前瞻研究,2010年12月/12卷1期,pp.112-127.
8.  Barbara B.H. Partee, A.G. ter Meulen, R. Wall,"Mathematical Methods
    in Linguistics (Studies in Linguistics and
    Philosophy)(語言研究的數學方法)"[5](http://acl.ldc.upenn.edu/J/J92/J92-1009.pdf)[6](http://www.amazon.com/Mathematical-Methods-Linguistics-Studies-Philosophy/dp/9027722455),Springer,1/e
    1993 edition(April 30, 1990).