《**思春期誘惑**》（すんドめ）是由[日本漫畫家](../Page/日本漫畫家.md "wikilink")[冈田和人所創作的](../Page/冈田和人.md "wikilink")[日本漫画作品](../Page/日本漫画.md "wikilink")。於[秋田書店发行的](../Page/秋田書店.md "wikilink")[青年漫画杂志](../Page/青年漫画.md "wikilink")《[Young
Champion](../Page/Young_Champion.md "wikilink")》上進行連載\[1\]。在2007年時改编为真人[电影](../Page/电影.md "wikilink")。

## 劇情簡介

有生以来从来没有过一见钟情的经验的主人公相羽英男，在看到转校生早华胡桃的第一眼就被她迷住了。而早华胡桃似乎也对他有些兴趣，不过是在另一个方面。

## 登场人物

  -
    主人公。对轉校而来的胡桃[一见钟情](../Page/一见钟情.md "wikilink")。有[被虐癖與潛在的變態](../Page/被虐癖.md "wikilink")。因前社長犯了守則而被指定為第38代社長。對性非常感興趣，受刺激就會馬上[勃起](../Page/勃起.md "wikilink")，不[打手槍臉上會出疹子](../Page/打手槍.md "wikilink")，在結局的時候，成了醫師
  -
    女主角。美少女，擁有美腿，但[貧乳](../Page/貧乳.md "wikilink")，身材纖瘦。因為發現了俱樂部的秘本《自家發電秘傳之書》而對俱樂部感到有興趣而加入。一開始對英男的態度忽冷忽熱，但其實早已鍾情於英男，時常讓他做一些难度极高的任务。患有[血癌](../Page/血癌.md "wikilink")，發病時月經來潮的血會過量，不時會貧血，隨故事發展愈趨嚴重，最後，在結局的時候過世了。
  -
    虽然不是社員，不过常在浪漫俱樂部出现的[巨乳女](../Page/巨乳.md "wikilink")。說話容易錯字或亂用成語。興趣是尋找靈異地點與賺錢，一千圓似乎是她的基本單位。
  -
    浪漫俱樂部社員。着迷于若隐若现的东西。原本也是貧乳迷，後來迷上京子。
  -
    浪漫俱樂部社員。[对脚着迷](../Page/戀足.md "wikilink")。[人偶狂人](../Page/人偶.md "wikilink")，有一個愛人娃娃：愛夢。遇到危險時會以最快速度逃離現場。京子稱他「小白豬」。
  - 原部長（演：[高田健一](../Page/高田健一.md "wikilink")）
    因为掉入刺客的圈套，而把社长的位子让给英男。興趣是摳他人的肛門，也是[口頭禪](../Page/口頭禪.md "wikilink")。

## 浪漫俱樂部

學校社團，但似乎不是純粹只有學生活動，還有一批畢業的校友也在暗中參與。社團的活動內容是研究各種超自然現象和尋找戶外的靈異地點，此外男子社員們都有自己的性癖好，所以性的方面也是研究之一。加入社團的守則就是必須保持處子之身，但可以談戀愛。校友們會派出刺客，目的是誘惑社員做出超友誼的行為，如此一來就必須退社，畢業後也得不到校友們的庇護。

## 出版書籍

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p><a href="../Page/秋田書店.md" title="wikilink">秋田書店</a></p></th>
<th><p><a href="../Page/長鴻出版社.md" title="wikilink">長鴻出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2006年11月20日</p></td>
<td><p>ISBN 978-4-253-15001-9</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2007年4月20日</p></td>
<td><p>ISBN 978-4-253-15002-6</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2007年9月20日</p></td>
<td><p>ISBN 978-4-253-15003-3</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2008年1月18日</p></td>
<td><p>ISBN 978-4-253-15004-0</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>2008年7月18日</p></td>
<td><p>ISBN 978-4-253-15005-7</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>2008年12月19日</p></td>
<td><p>ISBN 978-4-253-15006-4</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>2009年6月19日</p></td>
<td><p>ISBN 978-4-253-15007-1</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>2009年11月20日</p></td>
<td><p>ISBN 978-4-253-15008-8</p></td>
</tr>
</tbody>
</table>

## 電影

2007年至2009年期間發行4部電影，2009年11月6日發售盒裝版1到4集DVD。

2017年7月4日翻拍新版電影《思春期誘惑New》（すんドめNew），直接發行2卷DVD，並未公開上映。

### 劇情

相宇英男是一个处于[青春期](../Page/青春期.md "wikilink")[荷尔蒙多发的高中生](../Page/荷尔蒙.md "wikilink")，他对异性充满了向往和疑惑，经常性的[自慰](../Page/自慰.md "wikilink")。直到期末考试之前，早华胡桃作为转校生来到了他所在的班级，也正好是他的同桌，初次见面，相宇英男就[勃起了](../Page/勃起.md "wikilink")，恰好被早华胡桃看到，早华胡桃也萌生了对相宇英男的好感。相宇英男无意间被好友拉入了浪漫俱乐部，早华胡桃也自愿加入了。

### 主演

|                                    |        |                  |
| ---------------------------------- | ------ | ---------------- |
| **演员**                             | **角色** | **备注**           |
| [铃木茜](../Page/铃木茜.md "wikilink")   | 早华胡桃   | 转学生，相宇英男爱慕的对象。   |
| [二宫敦](../Page/二宫敦.md "wikilink")   | 相宇英男   | 16岁高中生，对异性充满好奇。  |
| [次原香奈](../Page/次原香奈.md "wikilink") | 叶子     | 浪漫俱乐部成员，相宇英男的同学。 |

### 製作團隊

  - 監督、脚本：[宇田川大吾](../Page/宇田川大吾.md "wikilink")
  - 製作人：[純春人](../Page/純春人.md "wikilink")
  - 攝影：[ふじもと光明](../Page/ふじもと光明.md "wikilink")
  - 錄音：[植田中](../Page/植田中.md "wikilink")
  - 美術：[佐々木健一](../Page/佐々木健一.md "wikilink")
  - 角色分配：[小林良二](../Page/小林良二.md "wikilink")
  - 音樂：Taruitakayoshi（タルイタカヨシ）

## 註解

## 外部链接

  - [思春期誘惑官方網站（第1集）](https://web.archive.org/web/20120213154045/http://www.sundome.jp/sundome1/index.html)
  - [思春期诱惑官方網站（第2集）](https://web.archive.org/web/20120223143724/http://www.sundome.jp/sundome2/index.html)
  - [思春期誘惑官方網站（第3集）](https://web.archive.org/web/20120223143445/http://www.sundome.jp/sundome3/index.html)
  - [思春期誘惑官方網站（第4集）](http://www.sundome.jp/sundome4/index.html)
  - [電影《思春期誘惑New》官方網站](http://sundome-movie.com/)

[Category:改编成电影的日本漫画](../Category/改编成电影的日本漫画.md "wikilink")
[S](../Category/青年漫畫.md "wikilink")
[Category:日语电影](../Category/日语电影.md "wikilink")
[Category:日本青春電影](../Category/日本青春電影.md "wikilink")
[Category:2007年电影](../Category/2007年电影.md "wikilink")
[Category:2008年电影](../Category/2008年电影.md "wikilink")
[Category:2009年电影](../Category/2009年电影.md "wikilink")

1.  《Young Champion》在每月第二个和第四个星期二发售。