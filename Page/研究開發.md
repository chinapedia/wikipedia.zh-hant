**研究開發**簡稱**研發** (；[縮寫](../Page/縮寫.md "wikilink")：**R\&D**)
是隸屬於[企業](../Page/企業.md "wikilink")、[大學及](../Page/大學.md "wikilink")[國家的](../Page/國家.md "wikilink")[機構所開展的項目](../Page/機構.md "wikilink")[研究與](../Page/研究.md "wikilink")[發展活動](../Page/發展.md "wikilink")。

於2006年，在研究開發領域上投入資金最多的3個國家是：

  - [美國](../Page/美國.md "wikilink")：3,300億美元
  - [中國](../Page/中國.md "wikilink")：1,360億美元
  - [日本](../Page/日本.md "wikilink")：1,300億美元

另外：[歐盟](../Page/歐盟.md "wikilink")：2,300亿美元

於2004年，各國家研究開發的強度 (研究開發於[國民生產總值中的百分比](../Page/國民生產總值.md "wikilink"))：

  - [瑞典](../Page/瑞典.md "wikilink")：4.3%
  - [日本](../Page/日本.md "wikilink")：3.1%
  - [美國](../Page/美國.md "wikilink")：2.7%
  - [中國](../Page/中國.md "wikilink")：1.8%

另外：[歐盟](../Page/歐盟.md "wikilink")：1.8%

於2006年，各國研究開發人员數目：

  - [美国](../Page/美国.md "wikilink")：130万人
  - [中國](../Page/中國.md "wikilink")：92.6万人
  - [日本](../Page/日本.md "wikilink")：64.8万人
  - [俄罗斯](../Page/俄罗斯.md "wikilink")：50.5万人

中國的研究開發投资總量及強度於1990年代末期以来增长迅速\[1\]，除了[中国经济继续高速扩张等一些必然因素外](../Page/中国经济.md "wikilink")，中国决策层在发展中重视[科技投资及企业加大投资是造成这一现象的主要原因](../Page/科技.md "wikilink")。

## 参考资料

## 外部連結

  - [BBC:中国研发即将超日赶美](http://news.bbc.co.uk/chinese/simp/hi/newsid_6200000/newsid_6207000/6207066.stm)
  - [China to beat Japan in R\&D
    spend](http://news.bbc.co.uk/2/hi/business/6205782.stm)

{{\#ifexpr:}}

[研究开发](../Category/研究开发.md "wikilink")
[Category:經濟](../Category/經濟.md "wikilink")
[Category:技術總論](../Category/技術總論.md "wikilink")
[Category:教育](../Category/教育.md "wikilink")
[Category:研究](../Category/研究.md "wikilink")

1.