****是历史上第四十六次航天飞机任务，也是[亚特兰蒂斯号航天飞机的第十一次太空飞行](../Page/亞特蘭提斯號太空梭.md "wikilink")。

## 任务成员

  - **[查尔斯·伯尔登](../Page/查尔斯·伯尔登.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[布莱恩·达菲](../Page/布莱恩·达菲.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[凯瑟琳·萨利文](../Page/凯瑟琳·萨利文.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[大卫·里茨马](../Page/大卫·里茨马.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[迈克尔·福奥勒](../Page/迈克尔·福奥勒.md "wikilink")**（，曾执行、、、、以及[远征8号任务](../Page/远征8号.md "wikilink")），任务专家
  - **[拜伦·利希滕贝格](../Page/拜伦·利希滕贝格.md "wikilink")**（，曾执行以及任务），有效载荷专家
  - **[德克·福里莫特](../Page/德克·福里莫特.md "wikilink")**（，[比利时宇航员](../Page/比利时.md "wikilink")，曾执行任务），有效载荷专家

[Category:航天飞机任务](../Category/航天飞机任务.md "wikilink")
[Category:1992年佛罗里达州](../Category/1992年佛罗里达州.md "wikilink")
[Category:1992年3月](../Category/1992年3月.md "wikilink")
[Category:1992年4月](../Category/1992年4月.md "wikilink")