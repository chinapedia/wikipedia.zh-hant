**奇克索县**（**Chickasaw County,
Mississippi**），[美國](../Page/美國.md "wikilink")[密西西比州東北部的一個縣](../Page/密西西比州.md "wikilink")。面積1,306平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口19,440人。縣治[休斯敦](../Page/休斯敦_\(密西西比州\).md "wikilink")
（Houston）和[奧科洛納](../Page/奧科洛納_\(密西西比州\).md "wikilink") （Okolona）。

奇克索縣成立於1836年2月9日。縣名是為紀念[奇克索人](../Page/奇克索人.md "wikilink")\[1\]而命名。

## 参考文献

<div class="references-small">

<references />

</div>

[C](../Category/密西西比州行政區劃.md "wikilink")
[Category:阿巴拉契亚地区的县](../Category/阿巴拉契亚地区的县.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.