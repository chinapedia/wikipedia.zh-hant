[NTT_DoCoMo_FOMA_N702iD.jpg](https://zh.wikipedia.org/wiki/File:NTT_DoCoMo_FOMA_N702iD.jpg "fig:NTT_DoCoMo_FOMA_N702iD.jpg")
**FOMA N702iD**是一部由[NEC開發](../Page/NEC.md "wikilink")、[NTT
DoCoMo的](../Page/NTT_DoCoMo.md "wikilink")[第三代流動電話](../Page/第三代流動電話.md "wikilink")（[FOMA](../Page/FOMA.md "wikilink")）[通訊終端產品](../Page/通訊終端.md "wikilink")。

## 概要

N702iD是[流動電話服務供應商](../Page/流動電話服務供應商.md "wikilink")[NTT
DoCoMo聯同](../Page/NTT_DoCoMo.md "wikilink")[通訊終端產品生產商](../Page/通訊終端產品生產商.md "wikilink")[NEC及](../Page/NEC.md "wikilink")[設計師](../Page/設計師.md "wikilink")[佐藤可士和](../Page/佐藤可士和.md "wikilink")[合作研發而成的產品](../Page/合作.md "wikilink")。

N702iD沿襲了[FOMA N701i的功能](../Page/N701i.md "wikilink")。

佐藤可士和以銳利的[直線構成整部N](../Page/直線.md "wikilink")702iD。機身顏色有紅、白、銀、黑四種，其中對前兩種進行表面光澤處理、對後兩種的表面則進行幼細粗糙度的無光表面處理。

此手機採用了2.3吋的**Mobile Shine
View液晶體**（[QVGA+](../Page/QVGA.md "wikilink")）顯示屏為主顯示屏。在近聽筒那一邊配備了用來進行[視像通訊的](../Page/視像電話.md "wikilink")11萬像素[CMOS](../Page/CMOS圖像感應器.md "wikilink")[相機](../Page/數碼相機.md "wikilink")。在背面則有一個幼長的副顯示屏，採用了被名為**Illumination
Window**的[有機電子冷光](../Page/有機電子冷光.md "wikilink")，並將在其中出現的文字顯示成綠色。支援[i-channel](../Page/i-channel.md "wikilink")（），橫向流動顯示的訊息就如[電子告示板一般](../Page/電子告示板.md "wikilink")。

在副顯示屏那一面的相機採用了有125萬像素的[νMaicovicon圖像感應器](../Page/νMaicovicon.md "wikilink")，能[讀取如](../Page/光學標記識別.md "wikilink")[QR碼等的](../Page/QR碼.md "wikilink")[條碼](../Page/條碼.md "wikilink")。另外，也配備了在拍照時照明用的高亮度白色[LED燈](../Page/發光二極管.md "wikilink")。此LED燈也兼作如有訊息到來時顏色鮮明的閃爍[照明功能](../Page/照明.md "wikilink")。在顯示屏頂端有一個[紅外線通訊連接埠](../Page/IrDA.md "wikilink")。

在電話的側面設有[miniSD記憶卡插卡槽](../Page/miniSD.md "wikilink")，支援最大容量為512MB的miniSD卡。可用記憶卡儲存拍攝得來的[圖像和](../Page/圖像.md "wikilink")[動態影像](../Page/動態影像.md "wikilink")。

此外，N702iD有播放音樂文件的功能，文件可透過安裝了由NEC免費分發的軟件的[個人電腦傳送到記憶卡中](../Page/個人電腦.md "wikilink")，然後讓手機播放卡中的文件，使之成為一部[手提音樂播放器](../Page/手提音樂播放器.md "wikilink")。在電話側面的兩個按鈕，令使用者即使電話在閉合的狀態下，仍然能作出跳到曲目的開端和調整音量這兩種操作。支援能透過包括[iTunes在內的](../Page/iTunes.md "wikilink")[媒體播放軟件製成的](../Page/媒體播放器.md "wikilink")[AAC格式的音樂文件](../Page/AAC.md "wikilink")。

按鈕的形狀與機身統一，成[四邊形](../Page/四邊形.md "wikilink")。內藏[無機EL](../Page/電致發光.md "wikilink")[光源](../Page/光源.md "wikilink")。

佐藤可士和除了負責N702iD機身的設計外，還設計了充電座及宣傳小冊子。此外，在N702iD的按鈕上和顯示屏中出現的文字，也使用了一款比較獨特的[字體](../Page/字體.md "wikilink")。

## 規格

  - 硬件規格
      - 形式：摺疊式
      - 外部記憶體：[miniSD](../Page/miniSD.md "wikilink")（支援的最大容量：512MB）
      - 顏色：紅、白、銀、黑
      - 大小：闊48×高98×厚18.7毫米
      - 重量：112g
      - 持續通話時間：140分鐘
      - 持續[視像電話通話時間](../Page/視像電話.md "wikilink")：90分鐘
      - 持續待命時間
          - 靜止時：560小時
          - 移動時：400小時
      - 主液晶體顯示屏：2.2[吋](../Page/吋.md "wikilink")、[QVGA+](../Page/QVGA.md "wikilink")240×345像素、65,536色的[TFT液晶體顯示屏](../Page/TFT液晶體.md "wikilink")
      - 副液晶體顯示屏：2.1吋、300×30像素、1色的[有機EL顯示屏](../Page/有機電子冷光.md "wikilink")
      - 相機
          - 外側：有效125萬像素、記錄123萬像素的[νMaicovicon](../Page/νMaicovicon.md "wikilink")
          - 內側：有效11萬像素、記錄10萬像素的[CMOS](../Page/CMOS.md "wikilink")

## 歷史

  - 2005年11月30日：通過[電氣通信端末機器審查協會](../Page/電氣通信端末機器審查協會.md "wikilink")（JATE）的審查（認定號碼：*A05-0492001*）。
  - 2005年12月5日：通過[技術基準適合証明](../Page/技術基準適合証明.md "wikilink")（TELEC）的審查（証明記號：*001XYAA1189*）。
  - 2006年1月17日：[F702iD](../Page/F702iD.md "wikilink")、N702iD、[SH702iD](../Page/SH702iD.md "wikilink")、[D702i](../Page/D702i.md "wikilink")、[P702i](../Page/P702i.md "wikilink")
    開發的發佈。
  - 2006年2月24日：N702iD開始發售。

## 關連項目

  - [N701i](../Page/N701i.md "wikilink")
  - [N702iS](../Page/N702iS.md "wikilink")
  - [佐藤可士和](../Page/佐藤可士和.md "wikilink")

## 外部連結

  - [N702iD Special
    Site](https://web.archive.org/web/20080531124040/http://n702id.jp/)
  - [NTT DoCoMo N702iD
    產品資料](https://web.archive.org/web/20061216005953/http://www.nttdocomo.co.jp/product/foma/702i/n702id/)
  - [NEC N702iD
    產品資料](https://web.archive.org/web/20061220053815/http://www.n-keitai.com/n702id/index.html)

[Category:行動電話](../Category/行動電話.md "wikilink")