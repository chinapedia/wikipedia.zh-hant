**单子叶植物**\[1\]（Monocotyledons，簡稱monocots），舊名**單子葉植物綱**（Monocotyledoneae）或**百合綱**（Liliopsida），單子葉植物有約59,300個物種。當中最大的科是[蘭科](../Page/蘭科.md "wikilink")，有超過20,000個物種。

## 特征

单子叶[植物](../Page/植物.md "wikilink")[种子内的](../Page/种子.md "wikilink")[胚只有一片顶生](../Page/胚.md "wikilink")[子叶](../Page/子叶.md "wikilink")，主[根不发达](../Page/根.md "wikilink")，因此多[鬚根](../Page/鬚根.md "wikilink")；[茎内](../Page/茎.md "wikilink")[维管束散生](../Page/维管束.md "wikilink")，没有排列成[形成层](../Page/形成层.md "wikilink")，因此不能依靠形成层发育逐渐加粗；[叶脉都是平行脉或弧形脉](../Page/叶.md "wikilink")，因此叶子较长；[花的蕊和瓣數一般是](../Page/花.md "wikilink")3的倍數（如3瓣、6瓣等），也有极少数為4的倍数，但絕不會是5的倍數。[花粉具有单个的萌发孔](../Page/花粉.md "wikilink")。

## 分类

依[被子植物种系发生学组](../Page/被子植物种系发生学组.md "wikilink")（APG）的分类，现有的单子叶植物分类如下\[2\]：

  - *' [单子叶植物分支](../Page/单子叶植物分支.md "wikilink") monocots*'
      -

          -
            [无叶莲目](../Page/无叶莲目.md "wikilink") Petrosaviales
            [菖蒲目](../Page/菖蒲目.md "wikilink") Acorales
            [泽泻目](../Page/泽泻目.md "wikilink") Alismatales
            [天門冬目](../Page/天門冬目.md "wikilink") Asparagales
            [薯蕷目](../Page/薯蕷目.md "wikilink") Dioscoreales
            [百合目](../Page/百合目.md "wikilink") Liliales
            [露兜树目](../Page/露兜树目.md "wikilink") Pandanales

        *' [鸭跖草类植物](../Page/鸭跖草类植物.md "wikilink") commelinoids*'

          -

              -
                [棕榈目](../Page/棕榈目.md "wikilink") Arecales
                [鸭跖草目](../Page/鸭跖草目.md "wikilink") Commelinales
                [禾本目](../Page/禾本目.md "wikilink") Poales
                [薑目](../Page/薑目.md "wikilink") Zingiberales

### 沿革

在传统的[克朗奎斯特分类法中](../Page/克朗奎斯特分类法.md "wikilink")，单子叶植物被称做“百合纲”，和[双子叶植物的](../Page/双子叶植物.md "wikilink")[木兰纲并立](../Page/木兰纲.md "wikilink")。其中包括了[兰目这个大目](../Page/兰目.md "wikilink")。

但后来的研究逐渐让植物学家认知，单子叶植物实际是由古代的[双子叶植物演化而来](../Page/双子叶植物.md "wikilink")，是双子叶植物的其中一个特化分支，这使传统的[双子叶植物分类成为一个](../Page/双子叶植物.md "wikilink")[并系群而不再被视为有效分类](../Page/并系群.md "wikilink")。

[APG分类法对被子植物的基因研究确定单子叶植物是](../Page/APG分类法.md "wikilink")[被子植物分支之下的一个](../Page/被子植物分支.md "wikilink")[单系群分支](../Page/单系群.md "wikilink")，分类有效，并设立了**单子叶植物分支**，内部分类则有许多变动，包括否定了[兰目这个](../Page/兰目.md "wikilink")[多系群分类](../Page/多系群.md "wikilink")，并成立了[天門冬目](../Page/天門冬目.md "wikilink")、[薯蓣目等新目](../Page/薯蓣目.md "wikilink")。

2003年经过修订的[APG II
分类法也使用这种分类方法](../Page/APG_II_分类法.md "wikilink")，包括传统分类中所有[单子叶植物纲中的](../Page/单子叶植物纲.md "wikilink")[植物种类](../Page/植物.md "wikilink")。

[APG II
分类法将单子叶植物分支中的植物分为](../Page/APG_II_分类法.md "wikilink")10个[目和](../Page/目.md "wikilink")2个单独的[科](../Page/科.md "wikilink")，
其中部分目列入[鸭跖草分支](../Page/鸭跖草分支.md "wikilink")（一
个[基因亲缘群体](../Page/基因.md "wikilink")）。

2009年修订的[APG
III分类法又将单独的](../Page/APG_III分类法.md "wikilink")[无叶莲科](../Page/无叶莲科.md "wikilink")（）提升为[无叶莲目](../Page/无叶莲目.md "wikilink")（）\[3\]。

2016年修订的[APG IV
分类法将](../Page/APG_IV_分类法.md "wikilink")[多须草科归于](../Page/多须草科.md "wikilink")[棕榈目](../Page/棕榈目.md "wikilink")。\[4\]

### 系统发生学

单子叶植物在[被子植物中的演化位置以及其下各分支的亲缘关系如下](../Page/被子植物.md "wikilink")\[5\]：

## 参考文献

<references />

## 外部链接

[\*](../Category/单子叶植物分支.md "wikilink")
[\*](../Category/單子葉植物綱.md "wikilink")

1.

2.
3.  Angiosperm Phylogeny Group (2009). [An update of the Angiosperm
    Phylogeny Group classification for the orders and families of
    flowering plants: APG
    III](http://www3.interscience.wiley.com/journal/122630309/abstract)
    . *Botanical Journal of the Linnean Society* **161**(2): 105-121.

4.

5.