**内斯托尔·卡洛斯·基什内尔**（，），已故[阿根廷政治人物](../Page/阿根廷.md "wikilink")，前[阿根廷总统](../Page/阿根廷总统.md "wikilink")，首任[南美洲國家聯盟秘書長](../Page/南美洲國家聯盟.md "wikilink")。其妻[克里斯蒂娜·費爾南德斯为第](../Page/克里斯蒂娜·費爾南德斯.md "wikilink")52任阿根廷总统，也是阿根廷史上第二位女总统。

## 生平

基什内尔生于[阿根廷](../Page/阿根廷.md "wikilink")[圣克鲁斯省](../Page/圣克鲁斯省.md "wikilink")[里奥加耶戈斯](../Page/里奥加耶戈斯.md "wikilink")，他父亲是一个邮局的官员，是[瑞士裔](../Page/瑞士.md "wikilink")。母亲Marija
Ostoić出生在[智利南部](../Page/智利.md "wikilink")，是[克罗地亚移民](../Page/克罗地亚.md "wikilink")。他中小学在当地公立学校受教育，他的高中文凭是危地马拉国立大学。1976年毕业于国立[拉普拉塔大学](../Page/拉普拉塔大学.md "wikilink")，获[法律学位](../Page/法律.md "wikilink")。他和妻子[克里斯蒂娜·费尔南德斯后来回到里奥加耶戈斯市做律师](../Page/克里斯蒂娜·費爾南德斯·德基什內爾.md "wikilink")。在軍人獨裁者[魏地拉执政期间](../Page/豪尔赫·拉斐尔·魏地拉.md "wikilink")，他曾被关押。

1983年军政府下台后，恢复了民主政治。基什内尔成为省政府公职人员。1983－1984年任圣克鲁斯省社会福利银行行长，1987年至1991年，他任圣克鲁斯省省会里奥加耶戈斯市市长。1991年当选圣克鲁斯省省长，1995年和1999年两次连任。

## 總統

2003年5月14日，在阿根廷大选，正義党無法推举统一的候選人。他代表左派勝利阵綫參選，成功進入第二轮选举。在第二轮选举中，因前总统[梅内姆支持度太低退出竞选](../Page/卡洛斯·萨乌尔·梅内姆.md "wikilink")，而自动当选总统。

基什內爾是[阿根廷](../Page/阿根廷.md "wikilink")[正義党第五位總統](../Page/正義党.md "wikilink")，但他執政后把正義党转向左翼[庇隆主義路線](../Page/庇隆主義.md "wikilink")（儘管[正義黨有不少右翼黨員](../Page/正義黨.md "wikilink")，這些党內右翼其后与反对党[激進公民联盟等结盟](../Page/激進公民联盟.md "wikilink")），採取政府有限干預市場经濟方式，经过多年后，成功穩定阿根廷经濟。

他的夫人[克里斯蒂娜·费尔南德斯也是正义党人](../Page/克里斯蒂娜·费尔南德斯·德基什内尔.md "wikilink")，拉普拉塔大学法律系毕业，曾任阿根廷参议员。2007年總統大選取得勝利，接替他的職務，他以「第一先生」身份繼續掌權。

2010年5月4日，基什內爾獲選為[南美洲國家聯盟秘書長](../Page/南美洲國家聯盟.md "wikilink")。

## 死亡

2010年10月27日，基什內爾在[阿根廷](../Page/阿根廷.md "wikilink")[聖克魯斯省](../Page/聖克魯斯省.md "wikilink")[埃尔卡拉法特的Jose](../Page/埃尔卡拉法特.md "wikilink")
Formenti醫院過世，死於心臟病。

## 外部链接

  - [阿根廷总统基什内尔](http://news.xinhuanet.com/ziliao/2003-05/26/content_886968.htm)，新華網

[Category:阿根廷總統](../Category/阿根廷總統.md "wikilink")
[Category:曾入獄的領袖](../Category/曾入獄的領袖.md "wikilink")
[Category:阿根廷正義黨黨員](../Category/阿根廷正義黨黨員.md "wikilink")
[Category:阿根廷律师](../Category/阿根廷律师.md "wikilink")
[Category:拉普拉塔大學校友](../Category/拉普拉塔大學校友.md "wikilink")
[Category:阿根廷天主教徒](../Category/阿根廷天主教徒.md "wikilink")
[Category:克罗地亚裔阿根廷人](../Category/克罗地亚裔阿根廷人.md "wikilink")
[Category:瑞士裔阿根廷人](../Category/瑞士裔阿根廷人.md "wikilink")
[Category:聖克魯斯省(阿根廷)人](../Category/聖克魯斯省\(阿根廷\)人.md "wikilink")