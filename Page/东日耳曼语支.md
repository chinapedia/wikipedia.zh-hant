[ The expansion of the Germanic tribes 750 BC – 1 AD:
](https://zh.wikipedia.org/wiki/File:Germanic_tribes_\(750BC-1AD\).png "fig: The expansion of the Germanic tribes 750 BC – 1 AD:     ")
**东日耳曼语支**是[印欧语系](../Page/印欧语系.md "wikilink")[日耳曼语族之下一个已经灭绝的语支](../Page/日耳曼语族.md "wikilink")，唯一已知文字的东日耳曼语支语言是[哥德語](../Page/哥德語.md "wikilink")。其他被认定同是东日耳曼语支的语言有[汪达尔语](../Page/汪达尔语.md "wikilink")、[勃艮第日耳曼语](../Page/勃艮第日耳曼语.md "wikilink")、[伦巴底日耳曼语和](../Page/伦巴底日耳曼语.md "wikilink")[克里米亚哥德语](../Page/克里米亚哥德语.md "wikilink")。克里米亚哥德语使用者相信在18世纪之前它仍然被使用。現時，我們一般只能從帶有東日耳曼語痕跡的地名來瞭解這種語言在當年的流通範圍。

## 分支

東日耳曼語支

  - [哥德語](../Page/哥德語.md "wikilink")（已消亡）()
      - [克里米亞哥特語](../Page/克里米亞哥特語.md "wikilink")（1800年代滅亡）
  - [汪達爾語](../Page/汪達爾語.md "wikilink")（滅亡）()
  - [勃艮第日耳曼語](../Page/勃艮第日耳曼語.md "wikilink")（Burgundian，滅亡）

## 参考文献

## 参见

  - [日耳曼语族](../Page/日耳曼语族.md "wikilink")
      - [北日耳曼语支](../Page/北日耳曼语支.md "wikilink")
      - [西日耳曼语支](../Page/西日耳曼语支.md "wikilink")

{{-}}

[东日耳曼语支](../Category/东日耳曼语支.md "wikilink")
[Category:日耳曼語族](../Category/日耳曼語族.md "wikilink")