[Boutique_Takenoko.jpg](https://zh.wikipedia.org/wiki/File:Boutique_Takenoko.jpg "fig:Boutique_Takenoko.jpg")
**竹之子族**（），亦可稱**竹筍族**，是一種在1979年崛起，於1980年代初期廣為流行過的[日本](../Page/日本.md "wikilink")[街頭表演文化](../Page/街頭表演.md "wikilink")。

## 簡介

「竹之子」在[日文裡面其實就是](../Page/日文.md "wikilink")[竹筍的意思](../Page/竹筍.md "wikilink")，但這族群的起源卻與真正的[竹子或竹筍無關](../Page/竹子.md "wikilink")，而是源自一家名為「竹之子服飾店」（<span lang="ja">ブティック·竹の子</span>）的流行服飾專賣店。這家1978年在[東京](../Page/東京.md "wikilink")[原宿](../Page/原宿.md "wikilink")[竹下通開幕的服飾店專門銷售一些顏色非常誇張鮮豔](../Page/竹下通.md "wikilink")、帶有[阿拉伯或](../Page/阿拉伯.md "wikilink")[中國功夫風味的衣服](../Page/中國功夫.md "wikilink")，尤其是寬大的褲身與褲腳的綁腿，稱為「[後宮服](../Page/後宮_\(伊斯蘭\).md "wikilink")」（<span lang="ja">ハーレムスーツ</span>）。許多喜歡這種服飾的年輕人在原宿的行人徒步區（原宿步行者天国，每週日時將[表參道封鎖](../Page/表參道.md "wikilink")、禁止汽車通行後所圍出的行人徒步區）與[代代木公園一帶聚集](../Page/代代木公園.md "wikilink")，穿著著這種奇裝異服配合著手提[卡式放音機放出的音樂](../Page/卡式錄放音機.md "wikilink")，進行團體風格的舞蹈。這種風潮經過[大眾媒體的報導後](../Page/大眾媒體.md "wikilink")，在短短一年內迅速擴散到日本全國各地，蔚為風潮。

在組成竹之子族的許多年輕人之中，有很多人原本曾參與過[暴走族活動](../Page/暴走族.md "wikilink")，這可能可以解釋為何後宮服的服飾風格與暴走族所穿的「特攻服」有所雷同。如同暴走族的活動特性，竹之子族的基本組成單位是由幾個愛好者組成的「小隊」（Team），而替自己的小隊賦予一個用日文[漢字以](../Page/漢字.md "wikilink")[音讀方式發音來拼成的隊名](../Page/音讀.md "wikilink")，則是另一個延續自暴走族的特色，例如「呪浬悦賭」（<span lang="ja">ジュリエット</span>，西洋女子名Juliette的日文化拼法），「獅利亜巣」（<span lang="ja">シリアス</span>，[天狼星Sirius的日文化拼法](../Page/天狼星.md "wikilink")）都是類似風格的隊伍名稱。竹之子族講究的是許多人同時做出一致舞步的表演方式，規模龐大時往往會給旁觀者一種類似宗教活動般的錯覺，因此當時社會上也有不少人對這種年輕人聚集進行的活動感到憂心。

如同竹之子族的竄起，它的熱潮衰退也非常迅速，在1980年至1981年時發展到最高峰後，1981年起竹之子族逐漸被喜歡1950年代復古風格與[搖滾樂的](../Page/搖滾樂.md "wikilink")[50族](../Page/50族.md "wikilink")（50's，或稱為「搖滾族」，<span lang="ja">ローラー</span>）所取代。1980年代後半起[樂團形式的街頭表演風格竄起](../Page/樂團.md "wikilink")，而使得所謂的玩團文化徹底壓過這些曾曇花一現街頭表演流行方式。

在許多曾參與竹之子族活動的年輕人中，因參加知名[電視連續劇](../Page/電視連續劇.md "wikilink")《[3年B組金八先生](../Page/3年B組金八先生.md "wikilink")》而走紅的已故日本偶像男星[沖田浩之是少數以竹之子族活躍參與者身份而出道的人物之一](../Page/沖田浩之.md "wikilink")。

不過，竹之子族的流行雖然短暫，它的精神卻沒有真的完全消失。一些更晚期的日本流行文化，例如1990年代末期的[Para
Para舞就被認為是繼承了竹之子族的大規模團體舞蹈精神](../Page/Para_Para.md "wikilink")。至於模仿[動畫或](../Page/動畫.md "wikilink")[漫畫中人物角色的造型進行聚會的](../Page/漫畫.md "wikilink")[角色扮演活動](../Page/Cosplay.md "wikilink")（<span lang="ja">コスプレ</span>，Cosplay），也被認為多少繼承了一些竹之子族利用大膽獨特的穿著方式彰顯個人主張的基本精神。

## 相關條目

  - [竹筍](../Page/竹筍.md "wikilink")
  - [原宿](../Page/原宿.md "wikilink")
  - [Para Para](../Page/Para_Para.md "wikilink")

## 外部連結

  - [竹之子族的歷史（日本流行史網站）](http://www.fashion-rekishi.com/takenoko.html)（日文）
  - [一些竹之子族的活動照片集錦與回顧](http://www.geocities.co.jp/AnimalPark-Shiro/3093/gallerytop_001.htm)（日文）

[Category:日本次文化](../Category/日本次文化.md "wikilink")
[Category:街頭藝術](../Category/街頭藝術.md "wikilink")
[T](../Category/舞蹈.md "wikilink")
[Category:日語詞彙](../Category/日語詞彙.md "wikilink")
[Category:戰後昭和時代文化](../Category/戰後昭和時代文化.md "wikilink")
[Category:竹筍族](../Category/竹筍族.md "wikilink")