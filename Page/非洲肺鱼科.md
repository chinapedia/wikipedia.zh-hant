**非洲肺鱼科**是[美洲肺鱼目下的一科](../Page/美洲肺鱼目.md "wikilink")，形状和生活习性都类似于[美洲肺鱼](../Page/美洲肺鱼.md "wikilink")，唯一不同的是非洲肺鱼只具有6个腮裂5个腮弓。非洲肺鱼科下僅有一屬，為非洲肺魚屬，其下有數種：

  - [維多利亞肺魚](../Page/維多利亞肺魚.md "wikilink") (*Protopterus aethiopicus*)
  - [东非肺鱼](../Page/东非肺鱼.md "wikilink") (*Protopterus amphibius*)
  - [原鳍鱼](../Page/原鳍鱼.md "wikilink") (''Protopterus annectens '')
  - [长肺鱼](../Page/长肺鱼.md "wikilink") (*Protopterus dolloi*)

## 参考文献

## 外部链接

  - [长肺鱼照片](http://www.whozoo.org/Anlife99/lashawn/africanlungfishindex4.htm)
  - [Protopterus annectens (African Lungfish, Lungfish, Mudfish, Tana
    Lungfish, West African
    Lungfish)](http://www.iucnredlist.org/apps/redlist/details/169408/0)

[\*](../Category/非洲肺鱼科.md "wikilink")