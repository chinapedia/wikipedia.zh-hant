**宋霭齡**，又写作**宋蔼齡**（[英語](../Page/英語.md "wikilink")：**Nancy Ai-Ling
Soong**，），祖籍[海南](../Page/海南.md "wikilink")[文昌县](../Page/文昌县.md "wikilink")，生于[江苏省](../Page/江苏省.md "wikilink")[苏州府](../Page/苏州府.md "wikilink")[昆山县](../Page/昆山县.md "wikilink")（今江苏苏州昆山）\[1\]。[孔祥熙之妻](../Page/孔祥熙.md "wikilink")，[宋庆龄](../Page/宋庆龄.md "wikilink")、[宋美龄的姐姐](../Page/宋美龄.md "wikilink")，是[宋氏家族的核心人物](../Page/宋氏家族.md "wikilink")。

## 生平

宋霭齡出生在江苏昆山，父亲[宋嘉澍原是](../Page/宋嘉澍.md "wikilink")[监理会的](../Page/监理会.md "wikilink")[传教士](../Page/传教士.md "wikilink")，后来放弃传教工作经商；母亲為[倪桂珍](../Page/倪桂珍.md "wikilink")。1904年5月28日，即14岁时抵达[美国](../Page/美国.md "wikilink")，进入[佐治亚州梅肯市威斯里安女子学院就讀](../Page/佐治亚州.md "wikilink")。1910年毕业后回国，任[孙中山的秘书](../Page/孙中山.md "wikilink")，同孫中山到全國各地勘察，參與制訂營建20萬里鐵路的計劃。“[二次革命](../Page/二次革命.md "wikilink")”失败后隨父去[日本](../Page/日本.md "wikilink")，仍任孙中山[秘书](../Page/秘书.md "wikilink")。1914年和[孔祥熙在日本](../Page/孔祥熙.md "wikilink")[横滨结婚](../Page/横滨.md "wikilink")。次年回国，在[山西经营家业](../Page/山西.md "wikilink")，主持铭贤学校事务。

1937年7月22日，在[何香凝](../Page/何香凝.md "wikilink")、宋慶齡等倡議下，聯絡宋靄齡、[于鳳至等](../Page/于鳳至.md "wikilink")，在上海成立中國婦女抗敵後援會，呼籲婦女同胞共赴國難\[2\]。[抗日战争时期与妹庆龄](../Page/抗日战争.md "wikilink")、美龄共同参加抗日活动，参与支持中国工业合作协会，组织妇女指导委员会，创办全国儿童福利会，担任[香港](../Page/香港.md "wikilink")「伤兵之友协会」会长。1947年移往美国定居。1973年10月20日在[纽约长老会医院因](../Page/纽约.md "wikilink")[癌症病故](../Page/癌症.md "wikilink")，享壽84岁，安葬於紐約郊外[風可立夫高級室內墓園](../Page/芬克里夫墓園.md "wikilink")（Ferncliff
Cemetery）。

## 評價

  - [徐家涵說](../Page/徐家涵.md "wikilink")：「[蔣介石](../Page/蔣介石.md "wikilink")、[宋子文](../Page/宋子文.md "wikilink")、[孔祥熙三個家族發生內部摩擦](../Page/孔祥熙.md "wikilink")，鬧得不可開交時，只有她這個大姊姊可以出面仲裁解決。她平日深居簡出，不像宋美齡那樣喜歡抛頭露面。可是她的勢力，直接可以影響國家大事，連蔣介石遇事也讓她三分。」
  - 蔣介石的侄孫[蔣孝鎮曾對軍統頭子](../Page/蔣孝鎮.md "wikilink")[戴笠說](../Page/戴笠.md "wikilink")：「委座之病，唯宋（美齡）可醫；夫人之病，唯孔（靄齡）可醫；孔之病（孔家貪腐）則無人可治。」
  - 《[紐約時報](../Page/紐約時報.md "wikilink")》形容她：“這個世界上一個令人感興趣的、掠奪成性的居民昨天在一片緘默的氣氛中辭世了。這是一位在金融上取得巨大成就的婦女，是世界上少有的靠自己的精明手段斂財的最有錢的婦女，是介紹[宋美齡和](../Page/宋美齡.md "wikilink")[蔣介石結婚的媒人](../Page/蔣介石.md "wikilink")，是宋家神話的創造者，是使宋家王朝掌權的設計者。”

## 子女

  - [孔令儀](../Page/孔令儀.md "wikilink")
  - [孔令侃](../Page/孔令侃.md "wikilink")
  - [孔令俊](../Page/孔令俊.md "wikilink")（又名[孔令偉](../Page/孔令偉.md "wikilink")）
  - [孔令傑](../Page/孔令傑.md "wikilink")

## 影視形象

### 電影

  - 1997年，[宋家皇朝](../Page/宋家皇朝.md "wikilink")

## 注釋

<div class="references-small">

<references />

</div>

[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:文昌人](../Category/文昌人.md "wikilink")
[Category:昆山人](../Category/昆山人.md "wikilink")
[Category:孔宋家族](../Category/孔宋家族.md "wikilink")
[A](../Category/宋姓.md "wikilink")
[Category:在日本的中國人](../Category/在日本的中國人.md "wikilink")
[Category:葬于芬克里夫墓园](../Category/葬于芬克里夫墓园.md "wikilink")

1.  [海派文化孕育了宋氏家族](http://news.ifeng.com/gundong/detail_2013_11/03/30914057_0.shtml)
2.