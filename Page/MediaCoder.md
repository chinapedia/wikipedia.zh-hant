**MediaCoder**是由[黄轶纯开发的一个通用音频](../Page/黄轶纯.md "wikilink")／视频批量转码工具，它将众多来自[开源社区的优秀音频视频编解码器和工具整合为一个通用的解决方案](../Page/开源社区.md "wikilink")，它可以将音频、视频文件在各种格式之间进行转换。该软件直到2008年还是一款基于[GPL协议的](../Page/GPL.md "wikilink")[自由软件](../Page/自由软件.md "wikilink")，后来作者封闭了源代码，不再开源，以[MediaCoder
EULA](http://blog.mediacoderhq.com/docs/EULA/)协议发布，并从[Sourceforge中移出](../Page/Sourceforge.md "wikilink")。

## 特点

  - 直接、批量地在众多音频视频压缩格式和容器格式之间进行转换
  - 无需安装任何媒体播放器或编码器
  - 纯绿色软件，不在系统中注册任何组件（仅限于官方发布的绿色便携版）
  - 完全使用[Win32](../Page/Win32.md "wikilink")
    [SDK开发](../Page/Windows_SDK.md "wikilink")，高效、紧凑，不依赖于任何中间层（如[.NET](../Page/.NET.md "wikilink")，[JAVA等](../Page/JAVA.md "wikilink")）

## 典型应用

  - 提高压缩率／减小文件尺寸
  - 将无损音频或者高码率的有损音频转换转换为较低码率的有损音频，以便用便携式数字音乐播放设备欣赏
  - 转换至可在各种设备（如手机、PDA、MP4播放器、VCD/DVD播放机）上播放的格式
  - 提取视频文件中的音轨
  - 保存[CD](../Page/CD.md "wikilink")、[VCD和](../Page/VCD.md "wikilink")[DVD](../Page/DVD.md "wikilink")

## 支援的影音格式

  - [MP3](../Page/MP3.md "wikilink")，[Vorbis](../Page/Vorbis.md "wikilink")，[AAC](../Page/AAC.md "wikilink")，[AAC+](../Page/AAC+.md "wikilink")，[AMR](../Page/自适应多速率音频压缩.md "wikilink")
    NB/WB, [Musepack](../Page/Musepack.md "wikilink")，[Windows Media
    Audio](../Page/Windows_Media_Audio.md "wikilink")，[RealAudio](../Page/RealAudio.md "wikilink")
  - [FLAC](../Page/FLAC.md "wikilink")，[WavPack](../Page/WavPack.md "wikilink")，[Monkey's
    Audio](../Page/Monkey's_Audio.md "wikilink") (APE, APL),
    [OptimFROG](../Page/OptimFROG.md "wikilink")，[AAC
    Lossless](../Page/AAC_Lossless.md "wikilink")，[WMA
    Lossless](../Page/WMA_Lossless.md "wikilink")，[WAV](../Page/WAV.md "wikilink")
  - [H.265](../Page/H.265.md "wikilink")，[H.264](../Page/H.264.md "wikilink")，[Xvid](../Page/Xvid.md "wikilink")，[DivX](../Page/DivX.md "wikilink")
    4/5, [MPEG](../Page/MPEG.md "wikilink") 1/2/4,
    [H.263](../Page/H.263.md "wikilink")，[3ivx](../Page/3ivx.md "wikilink")，[RealVideo](../Page/RealVideo.md "wikilink")，[Windows
    Media Video](../Page/Windows_Media_Video.md "wikilink") 7/8/9, DV
  - [AVI](../Page/AVI.md "wikilink")，[MPEG](../Page/MPEG.md "wikilink")／[VOB](../Page/VOB.md "wikilink")，[Matroska](../Page/Matroska.md "wikilink")，[MP4](../Page/MP4.md "wikilink")，[RealMedia](../Page/RealMedia.md "wikilink")，[ASF](../Page/ASF.md "wikilink")／WMV,
    [Quicktime](../Page/Quicktime.md "wikilink") MOV,
    [OGM](../Page/OGM.md "wikilink")
  - [CD](../Page/CD.md "wikilink")，[VCD](../Page/VCD.md "wikilink")，[DVD](../Page/DVD.md "wikilink")，[Cue
    sheet](../Page/Cue_sheet.md "wikilink")

## 外部链接

  - [官方网站](http://www.mediacoderhq.com)
  - [开发博客](http://blog.mediacoderhq.com)

## 参考

<div class="references-small">

<references />

</div>

[Category:媒体播放器](../Category/媒体播放器.md "wikilink")