[Sandy_sex_doll_or_poseable_mannequin.jpeg](https://zh.wikipedia.org/wiki/File:Sandy_sex_doll_or_poseable_mannequin.jpeg "fig:Sandy_sex_doll_or_poseable_mannequin.jpeg")
**性愛娃娃**（英语：，日语：、和製外来语：），是一種真人大小、人體形狀的[性玩具](../Page/性玩具.md "wikilink")，讓使用者在與之[自行性刺激行為中獲得快感](../Page/自慰.md "wikilink")。過去的性愛娃娃多採用充氣式設計，故又稱為**充氣娃娃**（）。从21世紀開始，性愛娃娃在工業製作上愈來愈接近真人形象的外觀與質感，尤以[日本為甚](../Page/日本.md "wikilink")，现成為[性產業中新崛起的勢力之一](../Page/性產業.md "wikilink")。

## 歷史

[清朝](../Page/清朝.md "wikilink")[梁章鉅著书](../Page/梁章鉅.md "wikilink")《[浪跡叢談](../Page/:s:浪跡叢談.md "wikilink")》卷五，引[吳德芝文章](../Page/吳德芝.md "wikilink")〈天主教書事〉說：「西洋國[天主教](../Page/天主教.md "wikilink")，前未之有也。[明季](../Page/明朝.md "wikilink")，其國人[利瑪竇](../Page/利瑪竇.md "wikilink")、[湯若望](../Page/湯若望.md "wikilink")、[南懷仁先後來中國](../Page/南懷仁.md "wikilink")，人多信之。……又能制物為倮婦人，肌膚、骸骨、耳目、齒舌、陰竅無一不具，初折疊如衣物；以氣吹之，則柔軟溫暖如美人，可擁以交接如人道。其巧而喪心如此。」清朝[夏燮著书](../Page/夏燮.md "wikilink")《[中西纪事](../Page/中西纪事.md "wikilink")》卷二，谈到：“洋人又能制物为裸妇人，肌肤、骸骨、耳目、齿舌、阴窍无一不具，初折迭如衣物；以气吹之，则柔软温暖如美人，可拥以交接如人道。其巧而丧心如此。”由此推断，其历史已有百年。當時的性愛娃娃，出現於只有[男人的環境中使人解決性需要](../Page/男人.md "wikilink")。至1950年代，性愛娃娃作為[性玩具於](../Page/性玩具.md "wikilink")[德國市場上售賣](../Page/德國.md "wikilink")，其設計甚至啟發了創造[芭比娃娃](../Page/芭比娃娃.md "wikilink")\[1\]。

20世紀中期，日本首次派探險隊往[南極](../Page/南極.md "wikilink")。探險隊隊員長期[禁慾會影響健康](../Page/禁慾.md "wikilink")，因此日本人動用公費研製出高質素性愛吹氣娃娃「南極1號」，解決南極探險隊的生理需要\[2\]。因而在1990年代，日本[成人视频制造商](../Page/成人视频制造商.md "wikilink")[KUKI甚至推出以仿性愛娃娃為主題的AV系列](../Page/KUKI.md "wikilink")《[南極2號](../Page/南極2號.md "wikilink")》\[3\]。

1970年代，典型的充氣性愛娃娃首次出現於日本；1990年代，性愛娃娃的質感已經比較接近真人的皮膚，但摸起來還是硬綁綁。同期亦有廠家試圖製造有骨骼的娃娃，產品效果並不理想。2000年代，日本出產的娃娃造型以假亂真，觸感亦接近真人[皮膚](../Page/皮膚.md "wikilink")。

## 設計

[Brigitte_sex_doll_2011-04-02_18-09-35_116.jpg](https://zh.wikipedia.org/wiki/File:Brigitte_sex_doll_2011-04-02_18-09-35_116.jpg "fig:Brigitte_sex_doll_2011-04-02_18-09-35_116.jpg")性愛娃娃\]\]

少女性愛娃娃一般以仿少女為形狀，設有[陰道及能夠張開的嘴巴](../Page/陰道.md "wikilink")，使男性能够把[陽具放進洞內摩擦並產生快感](../Page/陽具.md "wikilink")；壯男性愛娃娃則以壯男為形狀，售賣對象主要為女性及[男同性戀者](../Page/男同性戀.md "wikilink")。踏入二十一世紀，性愛娃娃採用具有[形狀記憶力的](../Page/形状记忆合金.md "wikilink")[凝胶体製造](../Page/凝胶体.md "wikilink")（近年亦有採用[樹脂](../Page/樹脂.md "wikilink")\[4\]），觸感已跟真人[皮膚與肌肉無異](../Page/皮膚.md "wikilink")，並有球狀關節，可做出不同動作。現在機器人方面發展讓人相信[性愛機械人終有一日會被製造並發售](../Page/性愛機械人.md "wikilink")\[5\]，目前已有製造商引入此概念，生產了一些懂得轉變[體溫與](../Page/體溫.md "wikilink")[心跳的性愛機械娃娃雛形](../Page/心跳.md "wikilink")。

常見材質還包括高级医用无毒软体硅胶或PVC材料等，近年來有更多仿真膚質材料發展中

## 與觀賞

現在一些製作精美、接近真人的性愛娃娃，甚至被人（包括女性）視為[藝術品](../Page/藝術品.md "wikilink")[收藏和觀賞](../Page/收藏.md "wikilink")；亦有人以把玩[球狀關節人形的方式玩賞這類娃娃](../Page/球狀關節人形.md "wikilink")，為它們穿上不同的[服裝](../Page/服裝.md "wikilink")，或对它们进行[化妝](../Page/化妝.md "wikilink")、更換[髮型等](../Page/髮型.md "wikilink")。

## 衛生問題

性愛娃娃也可能成為性病傳染的途徑。1996年[搞笑諾貝爾獎公共衛生獎頒給格陵蘭努克的愛倫](../Page/搞笑諾貝爾獎.md "wikilink")·克萊斯特（Ellen
Kleist）及挪威奧斯陸的哈洛得·摩伊（Harald Moi），他們因為《經由充氣娃娃造成的淋病傳染途徑》（Transmission of
Gonorrhea Through an Inflatable
Doll）獲獎，內容描述一名船員因為借用其他船員的性愛娃娃，而使用前沒有妥善清潔，因此傳染到前一名船員的淋病\[6\]\[7\]。

## 参考资料

## 相關條目

  -
  - [人工陰道](../Page/人工陰道.md "wikilink")

  -
  -
  - [戀玩偶癖](../Page/戀玩偶癖.md "wikilink")

  -
  -
  -
  - [恐怖谷理论](../Page/恐怖谷理论.md "wikilink")

  - [性爱机械](../Page/性爱机械.md "wikilink")

  - [物戀](../Page/物戀.md "wikilink")

## 外部連結

  - [凤凰卫视节目](../Page/凤凰卫视.md "wikilink")[李敖有话说](../Page/李敖有话说.md "wikilink")2006-01-02/03（视频）

  - [性愛娃娃的發展史及未來展望](http://www.haooye.com/article.php?id=66)

[R](../Category/性玩具.md "wikilink") [X](../Category/充气制造品.md "wikilink")
[X](../Category/假人與人體模型.md "wikilink")

1.  [日本充氣娃娃發展史(18P) @ osaki's
    Blog](http://blog.xuite.net/osaki99/blog/22276263)
2.  [太陽報2006年8月20日 -
    南極2號](http://the-sun.on.cc/channels/adult/20060820/20060819230252_0000.html)
3.  [AVNO1.COM -
    南極2號](http://avno1.com/?action-channel-name-viewblogitem-itemid-203)
4.  [Fashion 真人娃娃](http://www.realmodel.com.tw/index1.asp)
5.  [Ananova - Robot sex
    dolls](http://www.ananova.com/news/story/sm_1361247.html?menu=news.quirkies.sexlife)
6.
7.  [充氣娃娃成了淋病傳染途徑—《搞笑諾貝爾獎》](http://pansci.asia/archives/105198)