**Device
Bay**是一種讓[電腦用的硬體設備能夠熱插拔](../Page/電腦.md "wikilink")、熱置換的一項規格[標準及規範技術](../Page/標準.md "wikilink")，這項標準是在1997年由當時的[康柏電腦](../Page/康柏電腦.md "wikilink")、[英特爾](../Page/英特爾.md "wikilink")、[微軟三家業者所共同研究開發](../Page/微軟.md "wikilink")，並打算在1998年下半年完成制訂並加以推行。不過Device
Bay這項標準始終未能完成制訂，最後並且被放棄，而該規格技術的[官方網站也在](../Page/官方網站.md "wikilink")2001年中左右關閉、消失。

## 概述

Device
Bay原先的構想是運用當時興起的新介面來實現碟機的熱插拔，例如使用[USB或](../Page/USB.md "wikilink")[IEEE
1394等介面](../Page/IEEE_1394.md "wikilink")，如此可以讓[桌上型電腦](../Page/桌上型電腦.md "wikilink")、[筆記型電腦的](../Page/筆記型電腦.md "wikilink")[光碟機](../Page/光碟機.md "wikilink")、[硬碟機等碟機類裝置也能有隨插即用的便利性](../Page/硬碟機.md "wikilink")。從使用機制與功效上來看，等於是將[PCMCIA](../Page/PCMCIA.md "wikilink")、[CardBus的熱插拔記憶卡技術複製延伸到碟機上](../Page/CardBus.md "wikilink")，如此即便抽拔出碟機或裝入新碟機、置換碟機位置等，都不需要將電腦關閉，而是在電腦持續運用的情況下完成這一切程序。更重要的是，整個插拔過程也不需要對電腦進行機身拆解或用上任何手工具。

此外，Device
Bay的其他應用方式也能為電腦的使用帶來更多便利，例如將一顆帶有[作業系統的系統主硬碟推入電腦的磁碟槽位置後](../Page/作業系統.md "wikilink")，電腦就會自動開機，如同[VHS磁帶錄放影機](../Page/VHS.md "wikilink")，在電源關閉的情況下，一旦有錄影帶推入，錄放影機就會自動開啟。

或者，當硬碟被拔出時電腦也會立即停止，一直到重新被接入後才會恢復。或者也可以實現便利性接替，例如即將出門，可以將桌上型電腦的硬碟拔出，然後改裝到筆記型電腦上再出門，或者直接將硬碟帶到他處再裝入使用；相同的，回到家後可以將筆記型電腦的硬碟取出並重新裝回桌上型電腦，因為桌上型電腦的效能與功能皆比筆記型電腦理想，同時也可運用桌上型電腦來進行光碟片燒錄、或連接掃描器後進行圖片掃瞄，或各種其他可能的週邊應用。

雖然Device
Bay的標準未被實現，但今日[伺服器也多具備碟機熱插拔的機制](../Page/伺服器.md "wikilink")，只是機制未更低廉平價的普及到個人電腦上，而光碟機方面今日也都能透過USB介面來實現熱插拔。

不過，近年來也有電腦業者推出略具Device
Bay功效概念的產品，如[惠普科技公司發表過一系列標有](../Page/惠普.md "wikilink")「HP
Personal Media
Drives」字樣的個人電腦，此系列個人電腦即允許使用者將個人的資料硬碟進行快速簡易的插拔拆裝。此外相同的功效技術也有用在惠普科技的媒體中心電腦（Media
Center PC）上。

## 參考引據

  - [英特爾新聞發佈（關於Device
    Bay）](http://www.intel.com/pressroom/archive/releases/cn33197b.htm)

  - [Device
    Bay（Webopedia.com）](http://www.webopedia.com/TERM/D/Device_Bay.html)

## 外部連結

  - [HP Personal Media
    Drive安裝示意圖](https://web.archive.org/web/20070313093858/http://h10025.www1.hp.com/ewfrf-JAVA/Doc/images/c00258887.gif)

[Category:電腦週邊設備](../Category/電腦週邊設備.md "wikilink")