**矮螺旋星系**是[縮小版本的](../Page/矮星系.md "wikilink")[螺旋星系](../Page/螺旋星系.md "wikilink")。[矮星系的特徵是低](../Page/矮星系.md "wikilink")[光度](../Page/光度.md "wikilink")、小[直徑](../Page/直徑.md "wikilink")（距离高於50000[秒差距难以被观测到](../Page/秒差距.md "wikilink")）、低表面亮度和缺乏[氫的集團](../Page/氫.md "wikilink")。\[1\]這類星系也會被分類為[低表面亮度星系](../Page/低表面亮度星系.md "wikilink")。

矮螺旋星系，特別是在[Sa-Sc的類型中](../Page/星系分類.md "wikilink")，是非常罕見的。相反的，[矮橢圓星系](../Page/矮橢圓星系.md "wikilink")、[矮不規則星系和](../Page/矮不規則星系.md "wikilink")[Sm](../Page/星系分類.md "wikilink")（被認為是螺旋星系和不規則星系之間的類型）等其他類型的則很常見。\[2\]

## 位置

絕大部分的矮螺旋星系都位於[星系團的外緣](../Page/星系團.md "wikilink")，處在星系、星系與[星系團氣體之間有著強烈交互作用](../Page/星系團物質.md "wikilink")，預料會毀壞大多數矮螺旋星系盤的場所。
\[3\]\[4\]但是，矮星系和類似螺旋的結構在[室女座星系團和](../Page/室女座星系團.md "wikilink")[后髮座星系團內都已經被確認了](../Page/后髮座星系團.md "wikilink")。\[5\]\[6\]\[7\]\[8\]

## 參考資料

<div class="references-small">

<references/>

</div>

[矮星系](../Category/矮星系.md "wikilink")
[\*](../Category/矮螺旋星系.md "wikilink")
[\*](../Category/螺旋星系.md "wikilink")
[Category:低表面亮度星系](../Category/低表面亮度星系.md "wikilink")

1.

2.
3.
4.

5.

6.

7.
8.