**D線第六大道快車**（）是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的一條[地鐵路線](../Page/地鐵.md "wikilink")。由於該線使用[IND第六大道線通過](../Page/IND第六大道線.md "wikilink")[曼哈頓主要地區](../Page/曼哈頓.md "wikilink")，因此其路線徽號為橙色\[1\]。

D線任何時候都營運，來往的[205街與](../Page/諾伍德-205街車站_\(IND匯集線\).md "wikilink")[康尼島的](../Page/康尼島.md "wikilink")[斯提威爾大道](../Page/康尼島-斯提威爾大道車站.md "wikilink")。日間列車在布朗克斯以慢車營運，在曼哈頓和布魯克林以快車營運（[大西洋大道-巴克萊中心與](../Page/大西洋大道-巴克萊中心車站_\(BMT第四大道線\).md "wikilink")[36街之間](../Page/36街車站_\(BMT第四大道線\).md "wikilink")，通過[德卡爾布大道](../Page/德卡爾布大道車站_\(BMT第四大道線\).md "wikilink")）。繁忙時段的尖峰方向，列車以快車來往布朗克斯的[福德漢姆路與曼哈頓](../Page/福德漢姆路車站_\(IND匯集線\).md "wikilink")[145街](../Page/145街車站_\(IND第八大道線\).md "wikilink")。深夜列車布布朗克斯和布魯克林以慢車營運（停靠德卡爾布大道），以快車在曼哈頓營運。

早期，D線列車曾經途經[IND第八大道線前往](../Page/IND第八大道線.md "wikilink")[曼哈頓下城的](../Page/曼哈頓下城.md "wikilink")[世界貿易中心](../Page/世界貿易中心車站_\(IND第八大道線\).md "wikilink")。1954年至1967年間，D線列車使用[IND卡爾弗線](../Page/IND卡爾弗線.md "wikilink")，而1967年至2001年之間則使用[BMT布萊頓線](../Page/BMT布萊頓線.md "wikilink")。黃色D線列車一度在曼哈頓途經[BMT百老匯線前往布魯克林的布萊頓線](../Page/BMT百老匯線.md "wikilink")，而原初橙色D線列車則在曼哈頓和布朗克斯使用第六大道、[中央公園西和匯集線](../Page/IND第八大道線.md "wikilink")。

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p><a href="../Page/R1_(紐約地鐵車輛).md" title="wikilink">R1</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a></p>
</div></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</div>

</div>

## 歷史

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p>1967-1979年使用（環狀）</p>
</div></td>
</tr>
</tbody>
</table>

</div>

1940年12月15日，D線列車開始營運於第六大道的慢車軌道上，自205街車站起，行經西四街車站時切換到第八大道線至[曼哈頓哈得孫總站](../Page/曼哈頓.md "wikilink")（今[世貿中心車站](../Page/世界貿易中心.md "wikilink")）。

根據1948年的路線圖，D線列車曾行駛[IND克佛線至教堂大道車站](../Page/IND克佛線.md "wikilink")，但一段時間後又改回哈得孫總站。

1954年與的克佛線互通後，D線於當年12月30日首次延駛斯提威爾大道車站。

1962年12月主水管破裂，在此期間使用DD標誌行駛205街車站至斯提威爾大道車站的慢車。

1967年11月26日[IND克莉絲蒂街連接道通車](../Page/IND克莉絲蒂街連接道.md "wikilink")，D線加開平常日快車，經由該連接道延駛[BMT布來頓線線至布來頓海灘車站](../Page/BMT布來頓線.md "wikilink")，其他時間行慢車至斯提威爾大道車站，於曼哈頓行駛西四街至34街的尖峰快車（[B線於其他時段使用快車軌道](../Page/紐約地鐵B線.md "wikilink")）。當B線延駛57街-第六大道車站時，D線調整為全天的第六大道快車。

1986年[曼哈頓橋北邊軌道關閉時](../Page/曼哈頓橋.md "wikilink")，D線變成兩部份，一部分自[布朗克斯駛至](../Page/布朗克斯.md "wikilink")34街，另一部分自57街起，沿[BMT百老匯線至運河街車站](../Page/BMT百老匯線.md "wikilink")，經曼哈頓橋南邊軌道進入[BMT布來頓線](../Page/BMT布來頓線.md "wikilink")，至斯提威爾大道車站，同時間於布魯克林內實施平常日的B∕[Q線跳站停措施](../Page/紐約地鐵Q線.md "wikilink")。

1988年北邊軌道開放，D線調整為全天慢車。

1995年5月，曼哈頓橋北邊軌道於中午及週末關閉，D線自34街以南停駛，2001年7月22日北邊軌道全天停駛，布魯克林以[Q線代替](../Page/紐約地鐵Q線.md "wikilink")。

[911事件後](../Page/911事件.md "wikilink")[C線停駛期間](../Page/紐約地鐵C線.md "wikilink")，D線代其於第八大道線59街車站以北行駛慢車。

2004年2月22日，曼哈頓橋北邊軌道再度開放，D線經其駛至斯提威爾大道車站，取代[W線第四大道快車](../Page/紐約地鐵W線.md "wikilink")。同年3月24日起[IND匯集線閉線維修](../Page/IND匯集線.md "wikilink")，D線停駛[布朗克斯的快車直到秋季](../Page/布朗克斯.md "wikilink")。

2005年1月的第八大道線火災後，D線代替C線於第八大道線59街車站以北行駛慢車，直到2月2日C線復駛。

## 路線

### 服務形式

以下表格顯示D線所使用路線，特定時段在有陰影的格的路段內營運：\[2\]

<table>
<thead>
<tr class="header">
<th><p>路線</p></th>
<th><p>起點</p></th>
<th><p>終點</p></th>
<th><p>軌道</p></th>
<th><p>時段</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>非繁忙時段</p></td>
<td><p>繁忙時段</p></td>
<td><p>深夜</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/IND匯集線.md" title="wikilink">IND匯集線</a>（全線）</p></td>
<td><p><a href="../Page/諾伍德-205街車站_(IND匯集線).md" title="wikilink">諾伍德-205街</a></p></td>
<td><p>全部</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/貝德福德公園林蔭路車站_(IND匯集線).md" title="wikilink">貝德福德公園林蔭路</a></p></td>
<td><p><a href="../Page/145街車站_(IND匯集線).md" title="wikilink">145街</a></p></td>
<td><p>快車</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>慢車</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a></p></td>
<td><p><a href="../Page/135街車站_(IND第八大道線).md" title="wikilink">135街</a></p></td>
<td><p><a href="../Page/59街-哥倫布圓環車站_(IND第八大道線).md" title="wikilink">59街-哥倫布圓環</a></p></td>
<td><p>快車</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a></p></td>
<td><p><a href="../Page/第七大道車站_(IND第六大道線).md" title="wikilink">第七大道</a></p></td>
<td><p><a href="../Page/百老匯-拉法葉街車站_(IND第六大道線).md" title="wikilink">百老匯-拉法葉街</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/格蘭街車站_(IND第六大道線).md" title="wikilink">格蘭街</a></p></td>
<td><p>全部</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曼哈頓大橋.md" title="wikilink">曼哈頓大橋</a></p></td>
<td><p>北</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BMT第四大道線.md" title="wikilink">BMT第四大道線</a></p></td>
<td><p><a href="../Page/德卡爾布大道車站_(BMT第四大道線).md" title="wikilink">德卡爾布大道</a></p></td>
<td><p>通過</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>橋</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大西洋大道-巴克萊中心車站_(BMT第四大道線).md" title="wikilink">大西洋大道-巴克萊中心</a></p></td>
<td><p><a href="../Page/36街車站_(BMT第四大道線).md" title="wikilink">36街</a></p></td>
<td><p>快車</p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p>慢車</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BMT西城線.md" title="wikilink">BMT西城線</a>（全線）</p></td>
<td><p><a href="../Page/第九大道車站_(BMT西城線).md" title="wikilink">第九大道</a></p></td>
<td><p><a href="../Page/康尼島-斯提威爾大道車站_(BMT西城線).md" title="wikilink">康尼島-斯提威爾大道</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

### 車站

更詳細的車站列表參見上方列出的路線。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-D.svg" title="fig:NYCS-bull-trans-D.svg">NYCS-bull-trans-D.svg</a></p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/布朗克斯.md" title="wikilink">布朗克斯</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND匯集線.md" title="wikilink">匯集線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/諾伍德-205街車站_(IND匯集線).md" title="wikilink">諾伍德-205街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/貝德福德公園林蔭路車站_(IND匯集線).md" title="wikilink">貝德福德公園林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>部分早上繁忙時段來往布魯克林的列車以此站為起點為終點</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/王橋路車站_(IND匯集線).md" title="wikilink">王橋路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/福德漢姆路車站_(IND匯集線).md" title="wikilink">福德漢姆路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/182-183街車站_(IND匯集線).md" title="wikilink">182-183街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/特雷蒙特大道車站_(IND匯集線).md" title="wikilink">特雷蒙特大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/174-175街車站_(IND匯集線).md" title="wikilink">174-175街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/170街車站_(IND匯集線).md" title="wikilink">170街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/167街車站_(IND匯集線).md" title="wikilink">167街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/161街-洋基體育場車站_(IND匯集線).md" title="wikilink">161街-洋基體育場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT傑羅姆大道線.md" title="wikilink">IRT傑羅姆大道線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/曼哈頓.md" title="wikilink">曼哈頓</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/155街車站_(IND匯集線).md" title="wikilink">155街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/145街車站_(IND匯集線).md" title="wikilink">145街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND第八大道線.md" title="wikilink">第八大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/125街車站_(IND第八大道線).md" title="wikilink">125街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/59街-哥倫布圓環車站_(IND第八大道線).md" title="wikilink">59街-哥倫布圓環</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/IND第六大道線.md" title="wikilink">第六大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第七大道車站_(IND第六大道線).md" title="wikilink">第七大道</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND皇后林蔭路線.md" title="wikilink">IND皇后林蔭路線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/47-50街-洛克斐勒中心車站_(IND第六大道線).md" title="wikilink">47-50街-洛克斐勒中心</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/42街-布萊恩特公園車站_(IND第六大道線).md" title="wikilink">42街-布萊恩特公園</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>，<a href="../Page/第五大道車站_(IRT法拉盛線).md" title="wikilink">第五大道</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/34街-先驅廣場車站_(IND第六大道線).md" title="wikilink">34街-先驅廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>）</p></td>
<td><p><br />
<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/33街車站_(PATH).md" title="wikilink">33街</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/西四街-華盛頓廣場車站_(IND第六大道線).md" title="wikilink">西四街-華盛頓廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/第9街車站_(PATH).md" title="wikilink">第9街</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/百老匯-拉法葉街車站_(IND第六大道線).md" title="wikilink">百老匯-拉法葉街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>，<a href="../Page/布利克街車站_(IRT萊辛頓大道線).md" title="wikilink">布利克街</a>）</p></td>
<td><p>平日黃昏有1班北行列車自此站開出</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND第六大道線.md" title="wikilink">克里斯蒂街支線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/格蘭街車站_(IND第六大道線).md" title="wikilink">格蘭街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT第四大道線.md" title="wikilink">第四大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/德卡爾布大道車站_(BMT布萊頓線).md" title="wikilink">德卡爾布大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大西洋大道-巴克萊中心車站_(BMT布萊頓線).md" title="wikilink">大西洋大道-巴克萊中心</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT布萊頓線.md" title="wikilink">BMT布萊頓線</a>）<br />
（<a href="../Page/IRT東公園道線.md" title="wikilink">IRT東公園道線</a>）</p></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/聯合街車站_(BMT第四大道線).md" title="wikilink">聯合街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/第九街車站_(BMT第四大道線).md" title="wikilink">第九街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND卡爾弗線.md" title="wikilink">IND卡爾弗線</a>，<a href="../Page/第四大道車站_(IND卡爾弗線).md" title="wikilink">第四大道</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/展望大道車站_(BMT第四大道線).md" title="wikilink">展望大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/25街車站_(BMT第四大道線).md" title="wikilink">25街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/36街車站_(BMT第四大道線).md" title="wikilink">36街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/BMT西城線.md" title="wikilink">西城線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/第九大道車站_(BMT西城線).md" title="wikilink">第九大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/漢密爾頓堡公園道車站_(BMT西城線).md" title="wikilink">漢密爾頓堡公園道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/50街車站_(BMT西城線).md" title="wikilink">50街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/55街車站_(BMT西城線).md" title="wikilink">55街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/62街車站_(BMT西城線).md" title="wikilink">62街</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/BMT海灘線.md" title="wikilink">BMT海灘線</a>，<a href="../Page/新烏德勒支大道車站_(BMT海灘線).md" title="wikilink">新烏德勒支大道</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/71街車站_(BMT西城線).md" title="wikilink">71街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/79街車站_(BMT西城線).md" title="wikilink">79街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/18大道車站_(BMT西城線).md" title="wikilink">18大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/20大道車站_(BMT西城線).md" title="wikilink">20大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/海灣公園道車站_(BMT西城線).md" title="wikilink">海灣公園道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/25大道車站_(BMT西城線).md" title="wikilink">25大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/海灣50街車站_(BMT西城線).md" title="wikilink">海灣50街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/康尼島-斯提威爾大道車站_(BMT西城線).md" title="wikilink">康尼島-斯提威爾大道</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IND卡爾弗線.md" title="wikilink">IND卡爾弗線</a>）<br />
（<a href="../Page/BMT海灘線.md" title="wikilink">BMT海灘線</a>）<br />
（<a href="../Page/BMT布萊頓線.md" title="wikilink">BMT布萊頓線</a>）</p></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20131112111622/http://www.mta.info/nyct/service/dline.htm)
  - [D線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit -
    D線時刻表（PDF）](https://web.archive.org/web/20060921231137/http://www.mta.info/nyct/service/pdf/tdcur.pdf)

[D](../Category/紐約地鐵服務路線.md "wikilink")

1.
2.