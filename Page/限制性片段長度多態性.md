在[分子生物學中](../Page/分子生物學.md "wikilink")，**限制性片段長度多態性**（）具有兩種涵義：一是[DNA分子由於](../Page/DNA.md "wikilink")[核苷酸序列的不同而產生的一種可以用來相互區別的性質](../Page/核苷酸.md "wikilink")；二是一種實驗技術，利用這種性質來比較不同的DNA分子。這種技術可以用於[遺傳指紋和](../Page/遺傳指紋.md "wikilink")[親子鑑定](../Page/親子鑑定.md "wikilink")。\[1\]

## 方法

通常，一個獨立樣本的[DNA首先被提取和純化](../Page/DNA.md "wikilink")。純化後的DNA可以用[PCR反應擴增](../Page/PCR.md "wikilink")。隨後，用[限制性核酸内切酶切成](../Page/限制性核酸内切酶.md "wikilink")“限制性片段”，每種内切酶只能切除可被它識別的特定序列。隨後，限制性片段通過[瓊脂糖凝膠電泳將不同長度的片段分開](../Page/瓊脂糖凝膠電泳.md "wikilink")。得到的凝胶可以通過[南方墨點法](../Page/南方墨點法.md "wikilink")(Southern
blotting)进行强化。

RFLP是第一种被用于作图研究的DNA标记，它们一般有如下特征：1.出于染色体上的位置相对固定；2.同一亲本及其子代相同微点上的多态性片段特征不变；3.同一凝胶电泳可显示不同多态性片段，具有共显性特点。

## 結果

每個個體的酶切位點之間的距離會有差距，這樣限制性片段的長度有區別，不同個體的某個條帶的位置也會不同（也就是“多態性”）。這樣就能從遺傳水平上區分不同個體。RFLP也可以揭示不同個體之間的遺傳關係，因爲孩子從父母處繼承了染色體。這種技術也可以用來判斷各個[種的生物之間的關係](../Page/種_\(生物\).md "wikilink")。

## T-RFLP

T爲terminal縮寫，即**端點限制性片段長度多態性**。如在16S
rRNA分析中，利用一個或一對熒光標記的[引物進行](../Page/引物.md "wikilink")[PCR擴增](../Page/PCR.md "wikilink")。隨後用高頻率的内切酶進行酶切。通過[毛細管電泳將切開的片段分離](../Page/毛細管電泳.md "wikilink")，可檢測端部的限制性片段長度。這是快速鑒別菌株或者簡單[群落的方法](../Page/群落.md "wikilink")。

## 參見

  - [擴增片段長度多態性](../Page/擴增片段長度多態性.md "wikilink")
  - [遺傳學](../Page/遺傳學.md "wikilink")
  - [擴增核糖體DNA限制性分析](../Page/擴增核糖體DNA限制性分析.md "wikilink")(ARDRA)

## 参考

<references/>

[Category:分子生物学](../Category/分子生物学.md "wikilink")
[Category:實驗室技術](../Category/實驗室技術.md "wikilink")
[Category:電泳](../Category/電泳.md "wikilink")

1.