**中華人民共和國城市運動會**（National Intercity Games of the People's Republic of
China）簡稱**城運會**，是[中华人民共和国經已停辦的一個全国綜合性運動會](../Page/中华人民共和国.md "wikilink")，規模僅次於[全國運動會](../Page/中華人民共和國全國運動會.md "wikilink")。城運會每四年舉辦一次，各代表團以城市為單位，以培養新進運動員參與運動競賽。由于[国际奥委会设立了专门面向](../Page/国际奥委会.md "wikilink")14～18岁年轻人的[青年奥林匹克运动会](../Page/青年奥林匹克运动会.md "wikilink")，为适应国内及国际体育形势发展需要，更好地与青奥会接轨，经中共中央、国务院批准，[国家体育总局于](../Page/国家体育总局.md "wikilink")2013年11月将全国城市运动会更名为[全国青年运动会](../Page/中华人民共和国青年运动会.md "wikilink")。原第八届全国城运会更名为**第一届全国青年运动会**，于2015年10月在[福建举行](../Page/福建.md "wikilink")。

## 比賽項目

除[武術外](../Page/中國武術.md "wikilink")，目前城運會的比賽項目設置基本與奧運會的相同。以下是中華人民共和國城市運動會正式比賽項目：

  - [游泳](../Page/游泳.md "wikilink")（包括[跳水](../Page/跳水.md "wikilink")）
  - [射箭](../Page/射箭.md "wikilink")
  - [田徑](../Page/田徑.md "wikilink")
  - [羽毛球](../Page/羽毛球.md "wikilink")
  - [籃球](../Page/籃球.md "wikilink")
  - [皮划艇](../Page/皮划艇.md "wikilink")
  - [自行車](../Page/自行車.md "wikilink")
  - [擊劍](../Page/擊劍.md "wikilink")
  - [足球](../Page/足球.md "wikilink")
  - [體操](../Page/競技體操.md "wikilink")（包括[蹦床](../Page/蹦床.md "wikilink")）
  - [手球](../Page/手球.md "wikilink")
  - [曲棍球](../Page/曲棍球.md "wikilink")
  - [柔道](../Page/柔道.md "wikilink")
  - [賽艇](../Page/賽艇.md "wikilink")
  - [帆板](../Page/帆板.md "wikilink")
  - [射擊](../Page/射擊.md "wikilink")
  - [壘球](../Page/壘球.md "wikilink")
  - [乒乓球](../Page/乒乓球.md "wikilink")
  - [跆拳道](../Page/跆拳道.md "wikilink")
  - [網球](../Page/網球.md "wikilink")
  - [排球](../Page/排球.md "wikilink")
  - [舉重](../Page/舉重.md "wikilink")
  - [摔跤](../Page/摔跤.md "wikilink")
  - [武術](../Page/中國武術.md "wikilink")（包括[套路](../Page/套路.md "wikilink")、[散打](../Page/散手.md "wikilink")）

## 歷屆概況

| 屆次                                                                | 舉辦時間              | 舉辦地                                                                                                                                                                                                                     | 代表團數 | 比賽項目 | 運動員人數 |
| ----------------------------------------------------------------- | ----------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---- | ---- | ----- |
| [第一届](../Page/中华人民共和国第一届城市运动会.md "wikilink")                      | 1988年10月23日至11月2日 | [山東](../Page/山東.md "wikilink")[濟南](../Page/濟南.md "wikilink")、[淄博](../Page/淄博.md "wikilink")                                                                                                                             | 42   | 12   | 2332  |
| [第二届](../Page/中华人民共和国第二届城市运动会.md "wikilink")                      | 1991年9月20日至28日    | [河北](../Page/河北.md "wikilink")[唐山](../Page/唐山市.md "wikilink")、[石家莊](../Page/石家莊.md "wikilink")                                                                                                                          | 96   | 16   | 2928  |
| [第三届](../Page/中华人民共和国第三届城市运动会.md "wikilink")                      | 1995年10月22日至30日   | [江蘇](../Page/江蘇.md "wikilink")[南京](../Page/南京.md "wikilink")、[常州](../Page/常州.md "wikilink")、[無錫](../Page/無錫.md "wikilink")、[蘇州](../Page/蘇州.md "wikilink")、[揚州](../Page/揚州.md "wikilink")、[鎮江](../Page/鎮江.md "wikilink") | 50   | 16   | 3352  |
| [第四届](../Page/中华人民共和国第四届城市运动会.md "wikilink")                      | 1999年9月11日至20日    | [陝西](../Page/陝西.md "wikilink")[西安](../Page/西安.md "wikilink")                                                                                                                                                            | 57   | 16   | 3861  |
| [第五届](../Page/中华人民共和国第五届城市运动会.md "wikilink")                      | 2003年10月18日至23日   | [湖南](../Page/湖南.md "wikilink")[長沙](../Page/長沙.md "wikilink")                                                                                                                                                            | 78   | 25   | 6648  |
| [第六届](../Page/中华人民共和国第六届城市运动会.md "wikilink")                      | 2007年10月25日至11月3日 | [湖北](../Page/湖北.md "wikilink")[武漢](../Page/武漢.md "wikilink")                                                                                                                                                            | 74   | 27   | 6351  |
| [第七届](../Page/中华人民共和国第七届城市运动会.md "wikilink")（停辦前最後一屆）             | 2011年10月16日至25日   | [江西](../Page/江西.md "wikilink")[南昌](../Page/南昌.md "wikilink")                                                                                                                                                            | 57   | 25   | 6000  |
| [第八届](../Page/中华人民共和国第一届青年运动会.md "wikilink")（及後轉型為**第一届全国青年运动会**） | 2015年             | [福建](../Page/福建.md "wikilink")[福州](../Page/福州.md "wikilink")                                                                                                                                                            |      |      |       |

## 参见

  - [中华人民共和国全国运动会](../Page/中华人民共和国全国运动会.md "wikilink")
  - [中华人民共和国全国冬季运动会](../Page/中华人民共和国全国冬季运动会.md "wikilink")
  - [中华人民共和国全国残疾人运动会](../Page/中华人民共和国全国残疾人运动会.md "wikilink")
  - [中华人民共和国农民运动会](../Page/中华人民共和国农民运动会.md "wikilink")
  - [全国体育大会](../Page/全国体育大会.md "wikilink")
  - [中华人民共和国少数民族传统体育运动会](../Page/中华人民共和国少数民族传统体育运动会.md "wikilink")
  - [中华人民共和国青年运动会](../Page/中华人民共和国青年运动会.md "wikilink")

[中华人民共和国城市运动会](../Category/中华人民共和国城市运动会.md "wikilink")
[Category:1988年建立](../Category/1988年建立.md "wikilink")