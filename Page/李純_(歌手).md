**李純**（，）现名为**李沁谣**，生於[中華人民共和國](../Page/中華人民共和國.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[岳陽市](../Page/岳陽市.md "wikilink")，是[日本女子組合](../Page/日本.md "wikilink")「[早安少女組。](../Page/早安少女組。.md "wikilink")」第八期成員（留學生），加入早安後取日本名（純純）。

早安的監製[淳君於](../Page/淳君.md "wikilink")2007年3月15日宣佈她將會在2007年5月6日（即隊長[吉澤瞳卒業之日](../Page/吉澤瞳.md "wikilink")）成為早安第八期成員。她和[錢琳是早安成立十年以來首兩位成為隊員的外國](../Page/錢琳.md "wikilink")（即非日籍）人。
2010年8月早安家族演唱會最終場，早安的監製[淳君宣佈中國兩名成員](../Page/淳君.md "wikilink")（純純）和[錢琳將於](../Page/錢琳.md "wikilink")「[早安少女組。](../Page/早安少女組。.md "wikilink")」秋季巡迴演唱會最終場，從「[早安少女組。](../Page/早安少女組。.md "wikilink")」和早安家族畢業，並返回中國為在中國的歌手和演藝活動做準備。2011年1月回國，以靚麗、清純的形象被稱為“氧氣少女”，2012年開始活躍於國內各大綜藝，亦與多位國內著名演員共同參與了許多電影、電視劇的拍攝。
2014年11月3日，李純與中國音樂人[鄭楠結婚](../Page/鄭楠.md "wikilink")。

## 個人背景

李純的個人興趣為唱歌，鋼琴及演戲。李純於2005年參加「洞庭佳麗」競賽獲季度冠軍和總決賽季軍\[1\]，其後再於2006年參加中國舉行的[超級女聲](../Page/2006年超級女聲.md "wikilink")，並入圍長沙賽區頭50名。監製[淳君在比賽結束後親自到北京並邀請落選的參賽者進行試音](../Page/淳君.md "wikilink")，最後在一眾參賽者中選中李純。此事當時乃商業秘密，就連早安其他隊員也毫不知情。

淳君於2007年3月15日在早安的官方電視節目[Hello\!
Morning内宣佈](../Page/Hello!_Morning.md "wikilink")，來自中國的李純和錢琳將會成為早安的第八期交換生。淳君並說此安排將會對早安在[亞洲市場的發展有重要作用](../Page/亞洲.md "wikilink")。

三日後，李純與錢琳首次在[Hello\!
Morning中露面](../Page/Hello!_Morning.md "wikilink")，並於同一週內移居[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")。李純初加入的時候因為不懂日語，所以和其他成員用[英語交流](../Page/英語.md "wikilink")，和道重沙由美亦偶有寫[電子郵件給對方](../Page/電子郵件.md "wikilink")。李喜歡的藝人有林俊傑，[王心凌等等](../Page/王心凌.md "wikilink")。

李純加入早安的消息傳出後，她的blog遭大量日本及台湾網民，以日文、中文、及英文為主，寫下很多侮辱字句惡意破壞。其後李純把她的blog內所有自己的文章刪掉，只留下那些惡意的字句，並加上一篇回覆。其中一句內容是：

於2007年12月17日出版的「TIME
Asia」（[時代雜誌亞洲版](../Page/時代雜誌.md "wikilink")，Vol.170，No.24）刊有一篇撰寫於2007年12月6日，題為《Chinese
Immigrants Chase the Japanese Dream》的COVER文章，內容提及早安的李純，並引有她說過的兩句話：

### 藝歷

  - 於2005年參加「洞庭佳麗」競賽獲季度冠軍和總決賽季軍。
  - 於2006年參加[湖南衛視舉辦的](../Page/湖南衛視.md "wikilink")[超級女聲](../Page/超級女聲.md "wikilink")，長沙賽區五十強。
  - 於2011年參加[樂視網舉辦的選秀節目](../Page/樂視網.md "wikilink")「[我為校花狂](https://web.archive.org/web/20120326133247/http://best.letv.com/zt/xhjg2012/index.shtml)」獲全國總決賽冠軍。

## 演出

### 參加節目

  - (2007年7月22日 - 2008年9月28日)

  - (2008年10月6日 - 2009年3月27日)

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p>首播年份</p></td>
<td><p>電視台</p></td>
<td><p>劇名</p></td>
<td><p>角色</p></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/2010年上海世博會.md" title="wikilink">2010年上海世博會日本館後援娛樂網站劇集</a></p></td>
<td><p>《<a href="../Page/夢歸所夢.md" title="wikilink">夢歸所夢</a>》</p></td>
<td><p>小蓮</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>《<a href="../Page/半超能力少女.md" title="wikilink">半超能力少女</a>》</p></td>
<td><p>純純</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/奇藝網.md" title="wikilink">奇藝網</a></p></td>
<td><p>《<a href="../Page/非常宅.md" title="wikilink">非常宅</a>》</p></td>
<td><p>蘇雨晴</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/樂視網.md" title="wikilink">樂視網</a></p></td>
<td><p>《<a href="../Page/女人幫•妞兒.md" title="wikilink">女人幫•妞兒</a>》</p></td>
<td><p>應聘職員</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/淘梦网.md" title="wikilink">淘梦网</a></p></td>
<td><p>《<a href="../Page/深夜的士.md" title="wikilink">深夜的士</a>》</p></td>
<td><p>于成靖</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/CCTV-1.md" title="wikilink">CCTV-1</a></p></td>
<td><p>《<a href="../Page/鐵血紅安.md" title="wikilink">鐵血紅安</a>》</p></td>
<td><p>藤井寬子, 方曉蕾</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/優酷網.md" title="wikilink">優酷網</a><br />
<a href="../Page/土豆網.md" title="wikilink">土豆網</a></p></td>
<td><p>《<a href="../Page/小時代之摺紙時代.md" title="wikilink">小時代之摺紙時代</a>》</p></td>
<td><p>林泉, 林汀</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/深圳卫视.md" title="wikilink">深圳卫视</a></p></td>
<td><p>《<a href="../Page/擁抱星星的月亮.md" title="wikilink">擁抱星星的月亮</a>》</p></td>
<td><p>夏明星</p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><a href="../Page/江苏卫视.md" title="wikilink">江苏卫视</a><br />
<a href="../Page/湖北卫视.md" title="wikilink">湖北卫视</a></p></td>
<td><p>《<a href="../Page/東方戰場.md" title="wikilink">東方戰場</a>》</p></td>
<td><p>蘇婭</p></td>
</tr>
<tr class="odd">
<td><p>未播</p></td>
<td></td>
<td><p>《<a href="../Page/诚中堂.md" title="wikilink">诚中堂</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/爱奇艺.md" title="wikilink">爱奇艺</a></p></td>
<td><p>《<a href="../Page/动物管理局.md" title="wikilink">动物管理局</a>》</p></td>
<td><p>姜辛</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

|       |                                                                  |      |
| ----- | ---------------------------------------------------------------- | ---- |
| 上映日期  | 電影名稱                                                             | 角色名稱 |
| 2010年 | 《[手機刑警電影版3 營救早安少女大作戰](../Page/手機刑警電影版3_營救早安少女大作戰.md "wikilink")》 | 純純   |
| 2011年 | 《[愛一秒](../Page/愛一秒.md "wikilink")》                               | 女學生  |
| 2012年 | 《微電影[夢寶穀](../Page/夢寶穀.md "wikilink")》                            | 小夢   |
| 2013年 | 《[全民目擊](../Page/全民目擊.md "wikilink")》                             | 萌萌同學 |
| 2014年 | 《[詭婚](../Page/詭婚.md "wikilink")》                                 | 小優   |
| 2014年 | 《[嘿澀往事](../Page/嘿澀往事.md "wikilink")》                             | 陳璐   |
| 2014年 | 《[醜男大翻身](../Page/醜男大翻身.md "wikilink")》                           | 莎莎   |
| 2014年 | 《[伊黎河穀](../Page/伊黎河穀.md "wikilink")》                             | 路華   |
| 2017年 | 《[愿无岁月](../Page/愿无岁月.md "wikilink")》                             |      |
| 2017年 | 《[菜鸟变形计](../Page/菜鸟变形计.md "wikilink")》                           |      |
|       |                                                                  |      |

## 作品

### 早安时期单曲

1.  [**女に 幸あれ**](../Page/:ja:女に_幸あれ.md "wikilink") （2007年7月25日）
2.  [**みかん**](../Page/:ja:みかん.md "wikilink") （2007年11月21日）
3.  [**リゾナント ブルー**](../Page/:ja:リゾナント_ブルー.md "wikilink") （2008年4月16日）
4.  [**ペッパー警部**](../Page/:ja:ペッパー警部.md "wikilink") （2008年9月24日）
5.  [**泣いちゃうかも**](../Page/:ja:泣いちゃうかも.md "wikilink") （2009年2月18日）
6.  [**しょうがない 夢追い人**](../Page/:ja:しょうがない_夢追い人.md "wikilink")
    （2009年5月13日）
7.  [**なんちゃって恋爱**](../Page/:ja:なんちゃって恋爱.md "wikilink") （2009年8月12日）
8.  [**気まぐれプリンセス**](../Page/:ja:気まぐれプリンセス.md "wikilink") （2009年10月28日）
9.  [**女が目立って なぜイケナイ**](../Page/:ja:女が目立って_なぜイケナイ.md "wikilink")
    （2010年2月10日）
10. [**青春コレクション**](../Page/:ja:青春コレクション.md "wikilink") （2010年6月9日）
11. [**女と男のララバイゲーム**](../Page/:ja:女と男のララバイゲーム.md "wikilink")
    （2010年11月17日）

### 早安时期专辑

1.  [**モーニング娘。 ALL SINGLES COMPLETE 〜10th
    ANNIVERSARY〜**](../Page/:ja:モーニング娘。_ALL_SINGLES_COMPLETE_〜10th_ANNIVERSARY〜.md "wikilink")
    （2007年10月24日）
2.  [**COVER YOU**](../Page/:ja:_COVER_YOU.md "wikilink") （2008年11月26日）
3.  [**プラチナ 9 DISC**](../Page/:ja:プラチナ_9_DISC.md "wikilink")
    （2009年3月18日）
4.  [**モーニング娘。全シングル
    カップリングコレクション**](../Page/:ja:モーニング娘。全シングル_カップリングコレクション.md "wikilink")
    （2009年10月7日）
5.  [**10 MY ME**](../Page/:ja:_10_MY_ME.md "wikilink") （2010年3月17日）
6.  [**Fantasy\! 拾壱**](../Page/:ja:_Fantasy!_拾壱.md "wikilink")
    （2010年12月1日）

### DVD

  - 2007年9月26日 Morning Days Vol.01

## 外部連結

  -
  -
  -
## 參考資料

[Category:早安少女組。成員](../Category/早安少女組。成員.md "wikilink")
[Category:Hello\! Project](../Category/Hello!_Project.md "wikilink")
[Category:超級女聲參賽選手](../Category/超級女聲參賽選手.md "wikilink")
[Category:中國女歌手](../Category/中國女歌手.md "wikilink")
[Category:日語歌手](../Category/日語歌手.md "wikilink")
[Category:在日本的中华人民共和国人](../Category/在日本的中华人民共和国人.md "wikilink")
[Category:日本外籍藝人](../Category/日本外籍藝人.md "wikilink")
[Category:岳阳人](../Category/岳阳人.md "wikilink")
[Chun](../Category/李姓.md "wikilink")
[Category:日本女性偶像](../Category/日本女性偶像.md "wikilink")

1.  [美麗賽事第一李純“琴”少女傲群芳](http://yytv.rednet.cn/Article/yymy/dtjlkx/200505/597.html)