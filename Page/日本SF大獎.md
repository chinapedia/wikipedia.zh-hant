**日本SF大獎**（）是由[日本SF作家俱樂部於](../Page/日本SF作家俱樂部.md "wikilink")1980年設立的獎項，主辦者為日本SF作家俱樂部，[德間書店支助](../Page/德間書店.md "wikilink")。不論媒體或藝術的種類，只要是有關[科幻的作品](../Page/科幻.md "wikilink")，都是本獎的受獎對象，是本獎的一大特色。有許多小說、電影等專屬的大獎，但像此獎一樣將不同種類的作品放在一起評價的，卻相當罕見。

## 受賞作品

### 第1回至第10回

  - 第1回(1980年) - [堀晃](../Page/堀晃.md "wikilink") 『太陽風交点』
  - 第2回(1981年) - [井上廈](../Page/井上廈.md "wikilink")
    『[吉里吉里人](../Page/吉里吉里人.md "wikilink")』
  - 第3回(1982年) - [山田正紀](../Page/山田正紀.md "wikilink") 『最後の敵』
  - 第4回(1983年) - [大友克洋](../Page/大友克洋.md "wikilink")
    『[童夢](../Page/童夢_\(漫畫\).md "wikilink")』
  - 第5回(1984年) - [川又千秋](../Page/川又千秋.md "wikilink") 『幻詩狩り』
  - 第6回(1985年) - [小松左京](../Page/小松左京.md "wikilink")
    『[首都消失](../Page/首都消失.md "wikilink")』
  - 第7回(1986年) - [かんべむさし](../Page/かんべむさし.md "wikilink") 『笑い宇宙の旅芸人』
  - 第8回(1987年) - [荒俣宏](../Page/荒俣宏.md "wikilink")
    『[帝都物語](../Page/帝都物語.md "wikilink")』
  - 第9回(1988年) - [半村良](../Page/半村良.md "wikilink")
    『岬一郎の抵抗』、[横田順彌](../Page/横田順彌.md "wikilink")・[會津信吾](../Page/會津信吾.md "wikilink")
    『快男児・押川春浪』
  - 第10回(1989年) - [夢枕獏](../Page/夢枕獏.md "wikilink") 『吞食上弦月的獅子』、特別賞
    [手塚治虫](../Page/手塚治虫.md "wikilink")

### 第11回至第20回

<table>
<thead>
<tr class="header">
<th><p>届数</p></th>
<th><p>年度</p></th>
<th><p>得獎作品</p></th>
<th><p>特別獎</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第11回</p></td>
<td><p>1990年</p></td>
<td><p><a href="../Page/椎名誠.md" title="wikilink">椎名誠</a> 『アド・バード』</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第12回</p></td>
<td><p>1991年</p></td>
<td><p><a href="../Page/梶尾真治.md" title="wikilink">梶尾真治</a> 『サラマンダー殲滅』</p></td>
<td><p><a href="../Page/石原藤夫.md" title="wikilink">石原藤夫</a></p></td>
</tr>
<tr class="odd">
<td><p>第13回</p></td>
<td><p>1992年</p></td>
<td><p><a href="../Page/筒井康隆.md" title="wikilink">筒井康隆</a>『清晨的加斯巴』</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第14回</p></td>
<td><p>1993年</p></td>
<td><p><a href="../Page/柾悟郎.md" title="wikilink">柾悟郎</a> 『ヴィーナス・シティ』</p></td>
<td><p><a href="../Page/黑丸尚.md" title="wikilink">黑丸尚</a></p></td>
</tr>
<tr class="odd">
<td><p>第15回</p></td>
<td><p>1994年</p></td>
<td><p><a href="../Page/大原まり子.md" title="wikilink">大原まり子</a> 『戦争を演じた神々たち』<br />
<a href="../Page/小谷真理.md" title="wikilink">小谷真理</a> 『女性狀無意識』</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第16回</p></td>
<td><p>1995年</p></td>
<td><p><a href="../Page/神林長平.md" title="wikilink">神林長平</a> 『言壷』</p></td>
<td><p><a href="../Page/野田昌宏.md" title="wikilink">野田昌宏</a></p></td>
</tr>
<tr class="odd">
<td><p>第17回</p></td>
<td><p>1996年</p></td>
<td><p><a href="../Page/金子修介.md" title="wikilink">金子修介</a> 『<a href="../Page/卡美拉2_雷吉翁襲來.md" title="wikilink">卡美拉2 雷吉翁襲來</a>』</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第18回</p></td>
<td><p>1997年</p></td>
<td><p><a href="../Page/宮部美幸.md" title="wikilink">宮部美幸</a> 『蒲生邸事件』<br />
<a href="../Page/庵野秀明.md" title="wikilink">庵野秀明</a> 『<a href="../Page/新世紀福音戰士.md" title="wikilink">新世紀福音戰士</a>』</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第19回</p></td>
<td><p>1998年</p></td>
<td><p><a href="../Page/瀬名秀明.md" title="wikilink">瀬名秀明</a> 『BRAIN VALLEY(上・下)』</p></td>
<td><p><a href="../Page/星新一.md" title="wikilink">星新一</a><br />
<a href="../Page/NHK人間大学.md" title="wikilink">NHK人間大学</a> 『宇宙を空想してきた人々』<br />
<a href="../Page/井上雅彦.md" title="wikilink">井上雅彦</a>『異形コレクション1～6』</p></td>
</tr>
<tr class="even">
<td><p>第20回</p></td>
<td><p>1999年</p></td>
<td><p><a href="../Page/新井素子.md" title="wikilink">新井素子</a> 『チグリスとユーフラテス』</p></td>
<td><p><a href="../Page/光瀬龍.md" title="wikilink">光瀬龍</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 第21回至第30回

<table>
<thead>
<tr class="header">
<th><p>届数</p></th>
<th><p>年度</p></th>
<th><p>得獎作品</p></th>
<th><p>特別獎</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第21回</p></td>
<td><p>2000年</p></td>
<td><p><a href="../Page/巽孝之.md" title="wikilink">巽孝之</a>『日本SF論争史』</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第22回</p></td>
<td><p>2001年</p></td>
<td><p><a href="../Page/北野勇作.md" title="wikilink">北野勇作</a>『かめくん』</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第23回</p></td>
<td><p>2002年</p></td>
<td><p><a href="../Page/古川日出男.md" title="wikilink">古川日出男</a>『アラビアの夜の種族』<br />
<a href="../Page/牧野修.md" title="wikilink">牧野修</a>『傀儡后』</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第24回</p></td>
<td><p>2003年</p></td>
<td><p><a href="../Page/冲方丁.md" title="wikilink">冲方丁</a>『<a href="../Page/壳中少女.md" title="wikilink">壳中少女</a>』</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第25回</p></td>
<td><p>2004年</p></td>
<td><p><a href="../Page/押井守.md" title="wikilink">押井守</a>『<a href="../Page/攻殼機動隊2_INNOCENCE.md" title="wikilink">攻殼機動隊2 INNOCENCE</a>』</p></td>
<td><p><a href="../Page/矢野徹.md" title="wikilink">矢野徹</a></p></td>
</tr>
<tr class="even">
<td><p>第26回</p></td>
<td><p>2005年</p></td>
<td><p><a href="../Page/飛浩隆.md" title="wikilink">飛浩隆</a>『象られた力』</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第27回</p></td>
<td><p>2006年</p></td>
<td><p><a href="../Page/萩尾望都.md" title="wikilink">萩尾望都</a>『<a href="../Page/沉睡的祕境.md" title="wikilink">沉睡的祕境</a>』</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第28回</p></td>
<td><p>2007年</p></td>
<td><p><a href="../Page/最相葉月.md" title="wikilink">最相葉月</a>『星新一　一〇〇一話をつくった人』</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第29回</p></td>
<td><p>2008年</p></td>
<td><p><a href="../Page/貴志祐介.md" title="wikilink">貴志祐介</a>『<a href="../Page/來自新世界.md" title="wikilink">來自新世界</a>』<br />
<a href="../Page/磯光雄.md" title="wikilink">磯光雄</a>『<a href="../Page/電腦線圈.md" title="wikilink">電腦線圈</a>』</p></td>
<td><p><a href="../Page/野田昌宏.md" title="wikilink">野田昌宏</a></p></td>
</tr>
<tr class="even">
<td><p>第30回</p></td>
<td><p>2009年</p></td>
<td><p><a href="../Page/伊藤計劃.md" title="wikilink">伊藤計劃</a> 『<a href="../Page/和諧.md" title="wikilink">和諧</a>』</p></td>
<td><p><a href="../Page/栗本薫.md" title="wikilink">栗本薫</a> 『<a href="../Page/豹頭王傳說.md" title="wikilink">豹頭王傳說</a>』</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 第31回至第40回

  - 第31回(2010年) -
    [長山靖生](../Page/長山靖生.md "wikilink")『日本SF精神史』、[森見登美彦](../Page/森見登美彦.md "wikilink")『企鵝高速公路』、特別賞
    [柴野拓美](../Page/柴野拓美.md "wikilink")、[浅倉久志](../Page/浅倉久志.md "wikilink")
  - 第32回(2011年) - [上田早夕里](../Page/上田早夕里.md "wikilink")『華竜の宮』、特別賞
    [横田順彌](../Page/横田順彌.md "wikilink")『近代日本奇想小説史 明治篇』、 特別功劳賞
    [小松左京](../Page/小松左京.md "wikilink")
  - 第33回(2012年) - [月村了衛](../Page/月村了衛.md "wikilink")『機龍警察
    自爆条項』、[宮内悠介](../Page/宮内悠介.md "wikilink")『盤上の夜』、特別賞
    [伊藤計劃](../Page/伊藤計劃.md "wikilink")・[円城塔](../Page/円城塔.md "wikilink")『屍者の帝国』
  - 第34回（2013年） - [酉島伝法](../Page/酉島伝法.md "wikilink")『皆勤の徒』、特別賞
    [大森望責任編集](../Page/大森望.md "wikilink")『[NOVA
    書き下ろし日本SFコレクション](../Page/NOVA_書き下ろし日本SFコレクション.md "wikilink")』
    全10巻、[宮内悠介](../Page/宮内悠介.md "wikilink")『ヨハネスブルグの天使たち』
  - 第35回（2014年） -
    [藤井太洋](../Page/藤井太洋.md "wikilink")『オービタル・クラウド』、[長谷敏司](../Page/長谷敏司.md "wikilink")『My
    Humanity』、功績賞 [平井和正](../Page/平井和正.md "wikilink")
  - 第36回（2015年） - 谷甲州『コロンビア・ゼロ 新・航空宇宙軍史』、森岡浩之『突変』、特別賞 牧野修『月世界小説』、功績賞
    生賴範義
  - 第37回（2016年）- [白井弓子](../Page/白井弓子.md "wikilink")『轉孕奇兵』、特別賞
    庵野秀明（脚本・総監督)・樋口真嗣（監督・特技監督）・尾上克郎（准監督・特技統括）『シン・ゴジラ』
  - 第38回（2017年）-
    [小川哲](../Page/小川哲.md "wikilink")『ゲームの王国』、[飛浩隆](../Page/飛浩隆.md "wikilink")『自生の夢』、功績賞
    [山野浩一](../Page/山野浩一.md "wikilink")

## 外部連結

  - [關於日本SF大賞](http://www.sfwj.or.jp/list.html)

[Category:日本文學獎](../Category/日本文學獎.md "wikilink")
[Category:日本漫畫獎](../Category/日本漫畫獎.md "wikilink")
[Category:日本電影獎項](../Category/日本電影獎項.md "wikilink")
[Category:日本科幻小說](../Category/日本科幻小說.md "wikilink")
[Category:1980年建立的獎項](../Category/1980年建立的獎項.md "wikilink")
[Category:1980年日本建立](../Category/1980年日本建立.md "wikilink")