[Wanchai.jpg](https://zh.wikipedia.org/wiki/File:Wanchai.jpg "fig:Wanchai.jpg")[莊士敦道的唐樓](../Page/莊士敦道.md "wikilink")\]\]**唐樓**是[中國](../Page/中國.md "wikilink")[華南地區](../Page/華南.md "wikilink")、[香港及](../Page/香港.md "wikilink")[澳門](../Page/澳門.md "wikilink")，甚至[東南亞一帶於](../Page/东南亚.md "wikilink")19世紀中後期至1960年代的[建築風格](../Page/建築.md "wikilink")。唐樓不少混合了[中式及](../Page/中国传统建筑.md "wikilink")[西式建築風格](../Category/西式建築.md "wikilink")。

每一個地方所建的唐樓都有當地的本土特色，故此有「新加坡[店屋](../Page/店屋.md "wikilink")」、「檳城店屋」、「澳門唐樓」、「廣州[騎樓](../Page/骑楼.md "wikilink")」等稱號以作分辨。

## 香港

### 歷史

[缩略图](https://zh.wikipedia.org/wiki/File:Shanghaistreet1930s.jpg "fig:缩略图")\]\]
香港政府未大量興建[公共房屋以前](../Page/香港公共房屋.md "wikilink")，除[寮屋居民外](../Page/寮屋.md "wikilink")，幾乎所有港人都是舊式「唐樓」的住戶。唐樓早於19世紀中後期開始在香港出現。當時的唐樓樓高2至3層，闊15呎（約4.5米），以青磚砌成，而屋頂是以[木結構及](../Page/木.md "wikilink")[瓦片組成的斜頂](../Page/瓦片.md "wikilink")，唐樓旁則設有木[樓梯連接各層](../Page/樓梯.md "wikilink")。當中部份唐樓更有2呎闊的鐵製騎樓。
19世紀末期起，香港的唐樓普遍有3至4層，每層高4米，闊5米。設計方面，騎樓以磚砌支柱支撐，並跨出唐樓前的行人路。由於這種設計始於[廣州](../Page/廣州.md "wikilink")，所以當時俗稱為「[廣州式騎樓](../Page/廣州式騎樓.md "wikilink")」。到了1930年代，[混凝土取代了磚成為了唐樓的主要建築材料](../Page/混凝土.md "wikilink")。現時仍然存在的唐樓，尤其是於[九龍區的](../Page/九龍.md "wikilink")，都是混凝土建的唐樓。[Sales_brochure_of_Choi_Hung_Building,_Hong_Kong_(1960s).jpg](https://zh.wikipedia.org/wiki/File:Sales_brochure_of_Choi_Hung_Building,_Hong_Kong_\(1960s\).jpg "fig:Sales_brochure_of_Choi_Hung_Building,_Hong_Kong_(1960s).jpg")藏品）\]\]

[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，[香港人口急速增加](../Page/香港人口.md "wikilink")，房屋短缺。不少唐樓都被用作[分租](../Page/板間房.md "wikilink")。通常分租的唐樓會由其中一位住客「包租」，稱「包租公」或「包租婆」。包租者向業主每月繳交固定租金，同時亦交付負責支付水電。唐樓單位再被劃分為房間分租，通常最少分為“騎樓房”（亦稱頭房，面積較大及光線充足）、中間房、尾房；很多時更會在廚房或廁所上設置“閣仔（[閣樓](../Page/閣樓.md "wikilink")）”出租。亦有將單位細分為“床位”。正因如此，根據[香港法例第](../Page/香港法例.md "wikilink")123F章《建築物（規劃）規例》的第46條，唐樓被定義為「任何建築物，而在其住用部分有任何起居室擬供或改裝以供多於一名租客或分租客使用」，而這條法例至今仍然生效。

### 不同時期的唐樓

香港的唐樓可分為四代，主要是由不同殖民地時代所採用的建造物料和技術來界定\[1\]。

  - 第一代（約1841－1900年）：[維多利亞時代](../Page/维多利亚时代.md "wikilink")
  - 第二代（約1901－1930年）：[愛德華時代](../Page/爱德华时代.md "wikilink")
  - 第三代（約1930－1941年）：[二戰前現代時期](../Page/第二次世界大战.md "wikilink")
  - 第四代（約1945－1970年）：二戰後現代時期

<File:20161217>
威靈頓街120號航拍.jpg|第一代：建於1884年的[永和號](../Page/永和號.md "wikilink")
<File:HK> Wan Chai Stone Nullah Lane Blue House
SR.JPG|第二代：建於1925年的[藍屋](../Page/藍屋.md "wikilink")
<File:雷生春> - panoramio.jpg|第三代：建於1931年的[雷生春](../Page/雷生春.md "wikilink")
<File:為群公寓> 201704.jpg|第四代：建於1964年的[為群公寓](../Page/為群公寓.md "wikilink")

### 唐樓類型

#### 結構方式

  - 分為**背靠側** (現僅存1幢\[2\]) 和**背靠背** (現僅存1幢磚牆遺址\[3\])
      - 背靠側唐樓：[威靈頓街](../Page/威靈頓街.md "wikilink")120號「永和號」
      - 背靠背唐樓：[吉士笠街](../Page/吉士笠街.md "wikilink")2號至10號及[閣麟街](../Page/閣麟街.md "wikilink")2號至33號，共10間背靠背唐樓牆壁的遺跡（屬1903年香港頒佈《[公共衛生及建築物條例](../Page/公共衛生及建築物條例.md "wikilink")》前之建築方式\[4\]）

<File:Wellington> Street No120 Night
View.jpg|香港唯一僅存的**背靠側唐樓**[永和號](../Page/永和號.md "wikilink")
<File:Cochrane> Street in
1870s.jpg|1870年[閣麟街](../Page/閣麟街.md "wikilink")，現只剩**背靠背**遺跡

#### 轉角唐樓

  - 分為**直角轉角** (現僅存6幢\[5\]) 和**弧形轉角**
    (現僅存3幢\[6\])（不包括實為[洋樓的太子道西](../Page/洋樓.md "wikilink")177至179號及太子道西190至220號\[7\]）
      - 直角轉角唐樓：[史釗域道6號](../Page/史釗域道6號.md "wikilink")、[德輔道西207號](../Page/德輔道西207號.md "wikilink")、皇后大道西1號、彌敦道190號、廣東道578號、北河街58號
      - 弧形轉角唐樓：[雷生春](../Page/雷生春.md "wikilink")、[汝州街269及271號](../Page/汝州街269及271號.md "wikilink")、[青山道301及303號](../Page/青山道301及303號.md "wikilink")、[同德押](../Page/同德押.md "wikilink")
        (已拆卸)

<File:Stewart> Road No.6
201701.jpg|屬**直角轉角唐樓**的[史釗域道6號](../Page/史釗域道6號.md "wikilink")
<File:HK> LSC Lui Seng Chun Wai Yuen Tong
Medicine.JPG|屬**弧型轉角唐樓**的[雷生春](../Page/雷生春.md "wikilink")

### 分佈

隨著市區的急速發展，香港部分唐樓經已被拆卸[重建](../Page/市區重建.md "wikilink")，但大部分仍分佈在以下各地，而且許多都經過翻新來延長使用期：

#### [香港島](../Page/香港島.md "wikilink")

| [中西區](../Page/中西區_\(香港\).md "wikilink") | [灣仔區](../Page/灣仔區.md "wikilink")            | [東區](../Page/東區_\(香港\).md "wikilink")  | [南區](../Page/南區_\(香港\).md "wikilink")  |
| --------------------------------------- | ------------------------------------------- | -------------------------------------- | -------------------------------------- |
| [堅尼地城](../Page/堅尼地城.md "wikilink")\[8\] | [灣仔](../Page/灣仔.md "wikilink")\[9\]         | [北角](../Page/北角.md "wikilink")\[10\]   | [香港仔](../Page/香港仔.md "wikilink")\[11\] |
| [石塘咀](../Page/石塘咀.md "wikilink")\[12\]  | [銅鑼灣](../Page/銅鑼灣.md "wikilink")\[13\]      | [鰂魚涌](../Page/鰂魚涌.md "wikilink")\[14\] | [田灣](../Page/田灣.md "wikilink")\[15\]   |
| [西營盤](../Page/西營盤.md "wikilink")\[16\]  | [跑馬地](../Page/跑馬地.md "wikilink")\[17\]      | [西灣河](../Page/西灣河.md "wikilink")\[18\] | [鴨脷洲](../Page/鴨脷洲.md "wikilink")\[19\] |
| [上環](../Page/上環.md "wikilink")\[20\]    | [大坑](../Page/大坑_\(香港\).md "wikilink")\[21\] | [筲箕灣](../Page/筲箕灣.md "wikilink")\[22\] | [赤柱](../Page/赤柱.md "wikilink")\[23\]   |
| [中環](../Page/中環.md "wikilink")\[24\]    |                                             | [柴灣](../Page/柴灣.md "wikilink")\[25\]   |                                        |

#### [九龍](../Page/九龍.md "wikilink")

| [油尖旺區](../Page/油尖旺區.md "wikilink")     | [深水埗區](../Page/深水埗區.md "wikilink")     | [九龍城區](../Page/九龍城區.md "wikilink")     | [黃大仙區](../Page/黃大仙區.md "wikilink")     | [觀塘區](../Page/觀塘區.md "wikilink")       |
| -------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- | -------------------------------------- |
| [尖沙咀](../Page/尖沙咀.md "wikilink")\[26\] | [深水埗](../Page/深水埗.md "wikilink")\[27\] | [紅磡](../Page/紅磡.md "wikilink")\[28\]   | [黃大仙](../Page/黃大仙.md "wikilink")\[29\] | [觀塘](../Page/觀塘.md "wikilink")\[30\]   |
| [油麻地](../Page/油麻地.md "wikilink")\[31\] | [長沙灣](../Page/長沙灣.md "wikilink")\[32\] | [土瓜灣](../Page/土瓜灣.md "wikilink")\[33\] | [新蒲崗](../Page/新蒲崗.md "wikilink")\[34\] | [牛頭角](../Page/牛頭角.md "wikilink")\[35\] |
| [旺角](../Page/旺角.md "wikilink")\[36\]   | [石硤尾](../Page/石硤尾.md "wikilink")\[37\] | [馬頭圍](../Page/馬頭圍.md "wikilink")\[38\] | [樂富](../Page/樂富.md "wikilink")\[39\]   | [茶果嶺](../Page/茶果嶺.md "wikilink")\[40\] |
| [大角咀](../Page/大角咀.md "wikilink")\[41\] |                                        | [何文田](../Page/何文田.md "wikilink")\[42\] | [慈雲山](../Page/慈雲山.md "wikilink")\[43\] |                                        |
|                                        |                                        | [九龍城](../Page/九龍城.md "wikilink")\[44\] | [牛池灣](../Page/牛池灣.md "wikilink")\[45\] |                                        |

#### [新界西](../Page/新界西.md "wikilink")

| [離島區](../Page/離島區.md "wikilink")            | [葵青區](../Page/葵青區.md "wikilink")     | [荃灣區](../Page/荃灣區.md "wikilink")           | [元朗區](../Page/元朗區.md "wikilink")       | [屯門區](../Page/屯門區.md "wikilink")     |
| ------------------------------------------- | ------------------------------------ | ------------------------------------------ | -------------------------------------- | ------------------------------------ |
| [大澳](../Page/大澳.md "wikilink")\[46\]        | [葵涌](../Page/葵涌.md "wikilink")\[47\] | [荃灣市中心](../Page/荃灣市中心.md "wikilink")\[48\] | [元朗市](../Page/元朗市.md "wikilink")\[49\] | [屯門](../Page/屯門.md "wikilink")\[50\] |
| [長洲](../Page/長洲_\(香港\).md "wikilink")\[51\] |                                      |                                            |                                        |                                      |
| [梅窩](../Page/梅窩.md "wikilink")\[52\]        |                                      |                                            |                                        |                                      |

#### [新界東](../Page/新界東.md "wikilink")

| [北區](../Page/北區_\(香港\).md "wikilink")  | [大埔區](../Page/大埔區.md "wikilink")         | [沙田區](../Page/沙田區.md "wikilink")     | [西貢區](../Page/西貢區.md "wikilink")       |
| -------------------------------------- | ---------------------------------------- | ------------------------------------ | -------------------------------------- |
| [粉嶺](../Page/粉嶺.md "wikilink")\[53\]   | [大埔舊墟](../Page/大埔舊墟.md "wikilink")\[54\] | [大圍](../Page/大圍.md "wikilink")\[55\] | [西貢市](../Page/西貢市.md "wikilink")\[56\] |
| [上水](../Page/上水.md "wikilink")\[57\]   |                                          |                                      |                                        |
| [沙頭角](../Page/沙頭角.md "wikilink")\[58\] |                                          |                                      |                                        |

### 清拆爭議

[缩略图](https://zh.wikipedia.org/wiki/File:HK_Wan_Chai_Hennessy_Road_Pawn_Shop_building.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Demolished_Tung_Tak_Pawn_Shop.JPG "fig:缩略图")
香港發展需求高，市區的土地難能可貴，而香港唐樓大部份建於19世紀至20世紀華人聚居的港島和九龍地區。因此，地產商會以只有三、四層的唐樓為目標，希望可收地以重建更高層的住宅和商業大廈。

2015年，位於[灣仔](../Page/灣仔.md "wikilink")[軒尼詩道的](../Page/軒尼詩道.md "wikilink")[同德押被清拆](../Page/同德押.md "wikilink")，令全港只剩11幢轉角唐樓。同德大押落成逾80年、在現址經營逾60年，屬[三級歷史建築](../Page/香港三級歷史建築列表.md "wikilink")。因為只有[法定古蹟被法例保護](../Page/香港法定古蹟列表.md "wikilink")，其他的歷史評級只具參考價值，2013年同德押已獲[屋宇署批准改建為](../Page/屋宇署.md "wikilink")23層高商廈。是次事件引起大家對歷史文物評級的準確性，以及如何在保育和發展之間取得平衡。

### 活化及保育

[市區重建局近年保留了灣仔](../Page/市區重建局.md "wikilink")[莊士敦道](../Page/莊士敦道.md "wikilink")、茂蘿街及巴路士街多幢唐樓，並計劃在修葺後作為文化用途。位於灣仔[石水渠街](../Page/石水渠街.md "wikilink")72至74號，建於1922年的幾幢唐樓，因被塗上藍色而有[藍屋之稱](../Page/藍屋.md "wikilink")，亦被政府列為歷史建築。位於[旺角](../Page/旺角.md "wikilink")[荔枝角道](../Page/荔枝角道.md "wikilink")，由[九巴創辦人之一雷亮於](../Page/九巴.md "wikilink")1931年所建的唐樓[雷生春](../Page/雷生春.md "wikilink")，則在2003年10月7日由九巴家族後人捐贈予香港政府，並計劃改建為[博物館](../Page/博物館.md "wikilink")。

於2010年1月29日下午1時43分左右，發生[馬頭圍道唐樓倒塌事故](../Page/馬頭圍道唐樓倒塌事故.md "wikilink")，在馬頭圍道45號J一座5層唐樓全座塌下，造成4人死亡。是香港[第二次世界大戰後的少有的同類事件](../Page/第二次世界大戰.md "wikilink")，使政府及社會都關注全港同類約50年樓齡的唐樓的安全問題。

<File:HK> Sai Ying Pun Centre Street Chinese Building
01.jpg|[西營盤](../Page/西營盤.md "wikilink")[正街唐樓](../Page/正街.md "wikilink")
<File:Nathan> Road, perto do MTR em YMT 7060068.JPG|油麻地的唐樓 <File:Blue>
House HK.jpg|香港灣仔[藍屋](../Page/藍屋.md "wikilink")
[File:2012年4月25日的雷生春建築正面.jpg|2012年活化後的](File:2012年4月25日的雷生春建築正面.jpg%7C2012年活化後的)[雷生春](../Page/雷生春.md "wikilink")
<File:Ma> Tau Wai Road 45J Building 200811.jpg|塌樓前的馬頭圍道45J號唐樓（2008年11月）

## 澳門

[澳門市區內仍然保存有不少唐樓](../Page/澳門.md "wikilink")，在[議事亭前地及](../Page/議事亭前地.md "wikilink")[新馬路一帶的唐樓](../Page/新馬路.md "wikilink")，仍然保留了20世紀初期的風格，部份唐樓的上層則經改裝後作為商業用途。

## 参见

  - [洋樓](../Page/洋樓.md "wikilink")
  - [骑楼](../Page/骑楼.md "wikilink")
  - [排屋](../Page/排屋.md "wikilink")
  - [馬頭圍道唐樓倒塌事故](../Page/馬頭圍道唐樓倒塌事故.md "wikilink")

## 參考來源

## 外部参考

  - [香港地方: 唐樓特色及地址列表(九龍區)](http://www.hk-place.com/view.php?id=263)
  - [香港地方: 唐樓特色及地址列表(香港島)](http://www.hk-place.com/view.php?id=262)

{{-}}

[唐樓](../Category/唐樓.md "wikilink")
[香港唐樓](../Category/香港唐樓.md "wikilink")
[Category:建築物](../Category/建築物.md "wikilink")
[Category:香港房屋](../Category/香港房屋.md "wikilink")
[Category:集合住宅](../Category/集合住宅.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.  [堅尼地城唐樓集結](http://kennedytownhongkong.blogspot.hk/2012/04/1.html)
9.  [灣仔87年轉角唐樓面臨清拆
    街坊感可惜：呢類唐樓買少見少](http://hk.apple.nextmedia.com/realtime/news/20161125/55964881)
10. [回流香港客815萬購北角唐樓](http://www2.hkej.com/property/article/id/62852/%E5%9B%9E%E6%B5%81%E9%A6%99%E6%B8%AF%E5%AE%A2815%E8%90%AC%E8%B3%BC%E5%8C%97%E8%A7%92%E5%94%90%E6%A8%93)
11. [港島唐樓價
    跌破200萬元](https://hk.finance.yahoo.com/news/%E6%B8%AF%E5%B3%B6%E5%94%90%E6%A8%93%E5%83%B9-%E8%B7%8C%E7%A0%B4200%E8%90%AC%E5%85%83-225525558--sector.html)

12. [400萬以下有貨西環細價樓回報逾3.5厘
    西港島綫年底竣工](http://drc.century21-hk.com/zh_hk/news-inner/?id=16021)

13. [銅鑼灣唐樓劏房 呎租58逼新盤價 170呎月租9800
    外籍客承租](http://ps.hket.com/article/1574904/%E9%8A%85%E9%91%BC%E7%81%A3%E5%94%90%E6%A8%93%E5%8A%8F%E6%88%BF%20%E5%91%8E%E7%A7%9F58%E9%80%BC%E6%96%B0%E7%9B%A4%E5%83%B9)
14. [太古洽購鰂魚涌三唐樓](http://hk.apple.nextmedia.com/financeestate/art/20100910/14435572)
15. [田灣街唐樓劏房失火母抱子危坐外牆獲救](http://topick.hket.com/article/1447170/%E7%94%B0%E7%81%A3%E8%A1%97%E5%94%90%E6%A8%93%E5%8A%8F%E6%88%BF%E5%A4%B1%E7%81%AB%20%E6%AF%8D%E6%8A%B1%E5%AD%90%E5%8D%B1%E5%9D%90%E5%A4%96%E7%89%86%E7%8D%B2%E6%95%91)
16. [西營盤唐樓變太空艙　月租4000元包家電](http://topick.hket.com/article/1653618/%E8%A5%BF%E7%87%9F%E7%9B%A4%E5%94%90%E6%A8%93%E8%AE%8A%E5%A4%AA%E7%A9%BA%E8%89%99%E3%80%80%E6%9C%88%E7%A7%9F4000%E5%85%83%E5%8C%85%E5%AE%B6%E9%9B%BB)
17. [跑馬地唐樓高價落釘](http://hk.apple.nextmedia.com/financeestate/art/20091103/13379212)
18. [長綫放租博收購 西灣河唐樓300多萬有貨
    重建掀尋寶熱](http://utp.century21-hk.com/zh_hk/news-inner/index.php?id=20329)

19. [鴨脷洲大街單幢樓執平貨](http://hk.apple.nextmedia.com/financeestate/art/20091120/13440149)
20. [唐樓癡公開投資秘笈 翻新吸豪客
    回報17厘](http://www.yinfat32.com/tenementhouse/story/1.htm)
21. [戰前唐樓拒重建　大坑兩家人](http://hk.apple.nextmedia.com/supplement/special/art/20150428/19127496)
22. [筲箕灣唐樓升值百五倍](http://eastweek.my-magazine.me/main/15387)
23. [離島去到悶 轉場赤柱獨享天台BBQ嘆海景](http://www.hkoasis.com/articles/stanleyroof)
24. [百年唐樓遺蹟隱身中環鬧市
    團體斥古蹟辦忽視歷史價值](http://hk.apple.nextmedia.com/realtime/news/20160414/54986569)
25. [柴灣滿華樓 2房百萬上車](http://paper.hket.com/article/987440/%E6%9F%B4%E7%81%A3%E6%BB%BF%E8%8F%AF%E6%A8%93%202%E6%88%BF%E7%99%BE%E8%90%AC%E4%B8%8A%E8%BB%8A)
26. [尖沙咀唐樓叫價6億](http://paper.wenweipo.com/2011/05/24/ME1105240007.htm)
27. [250呎深水埗唐樓大改造屏風加強空間感](http://topick.hket.com/article/1544424/250%E5%91%8E%E6%B7%B1%E6%B0%B4%E5%9F%97%E5%94%90%E6%A8%93%E5%A4%A7%E6%94%B9%E9%80%A0%E3%80%80%E5%B1%8F%E9%A2%A8%E5%8A%A0%E5%BC%B7%E7%A9%BA%E9%96%93%E6%84%9F)
28. [收購值四百億紅磡唐樓
    離奇火燭兼漏水](http://hk.apple.nextmedia.com/nextplus/%E8%B2%A1%E7%B6%93/article/20161214/2_459758_0/%E5%9B%9B%E5%8F%94%E5%A4%A7%E8%8C%B6%E9%A3%AF-%E6%94%B6%E8%B3%BC%E5%80%BC%E5%9B%9B%E7%99%BE%E5%84%84%E7%B4%85%E7%A3%A1%E5%94%90%E6%A8%93-%E9%9B%A2%E5%A5%87%E7%81%AB%E7%87%AD%E5%85%BC%E6%BC%8F%E6%B0%B4)
29. [黃大仙唐樓賣200萬](http://orientaldaily.on.cc/cnt/finance/20120316/00204_002.html)
30. [一樓一古：觀塘月華街 180級富貴樓梯](http://hk.apple.nextmedia.com/supplement/culture/art/20130312/18191879)
31. [區內多戰前唐樓
    議員料研究有助保育](http://m.mingpao.com/pns/dailynews/web_tc/article/20161226/s00001/1482687769451)
32. [舊商號消失　頂層圍欄被拆　轉角唐樓　翻新失特色](http://hk.apple.nextmedia.com/news/art/20150914/19294666)
33. [重建土瓜灣唐樓戶主歡迎冀102歲母不用行樓梯](http://topick.hket.com/article/1711716/%E9%87%8D%E5%BB%BA%E5%9C%9F%E7%93%9C%E7%81%A3%E5%94%90%E6%A8%93%E6%88%B6%E4%B8%BB%E6%AD%A1%E8%BF%8E%E3%80%80%E5%86%80102%E6%AD%B2%E6%AF%8D%E4%B8%8D%E7%94%A8%E8%A1%8C%E6%A8%93%E6%A2%AF)
34. [炒家掃新蒲崗唐樓](http://orientaldaily.on.cc/cnt/finance/20101021/00204_002.html)
35. [唐樓遇竊　失平板電腦及現金](http://hk.on.cc/hk/bkn/cnt/news/20170206/bkn-20170206060458584-0206_00822_001.html)
36. [旺角唐樓水表離奇被盜　警拘39歲無業漢](https://hk.news.yahoo.com/%E6%97%BA%E8%A7%92%E5%94%90%E6%A8%93%E6%B0%B4%E8%A1%A8%E9%9B%A2%E5%A5%87%E8%A2%AB%E7%9B%9C-%E8%AD%A6%E6%8B%9839%E6%AD%B2%E7%84%A1%E6%A5%AD%E6%BC%A2-130500769.html)
37. [石硤尾唐樓起火
    男住客吸入濃煙不適](http://hk.apple.nextmedia.com/realtime/breaking/20170209/56279961)
38. [市建局重建馬頭圍唐樓
    供逾400單位](http://hk.apple.nextmedia.com/news/art/20170318/19962124)
39. [樂富唐樓回報38厘](http://the-sun.on.cc/cnt/finance/20121005/00436_008.html)
40. [茶果嶺
    淳樸樂土](http://hd.stheadline.com/culture/culture_content.asp?contid=158256&srctype=g)
41. [市建局收大角咀唐樓　呎價$13,614史上最高](http://hk.on.cc/hk/bkn/cnt/news/20150805/bkn-20150805170036226-0805_00822_001.html)
42. [何文田唐樓 強拍底價4.13億](http://www1.hkej.com/dailynews/article/id/1479599)
43. [慈雲山唐樓售215萬](http://hk.apple.nextmedia.com/financeestate/art/20150808/19247772)
44. [九龍城400萬上車盤絕少](http://hk.apple.nextmedia.com/news/art/20130106/18124839)
45. [牛池灣村](http://ohmyking.com/market/hk-market-045/)
46. [大澳被評級的歷史建築物](http://www.ilovetaio.com/?q=node/94)
47. [葵涌唐樓劏房疑遭縱火　住客抱B疏散](http://hk.on.cc/hk/bkn/cnt/news/20160113/bkn-20160113013554503-0113_00822_001.html)
48. [荃灣市區（唐樓）環境研究及展望](http://www.afhc2014.org.hk/database/video/1031/1400/1031-1400-19.pdf)
49. [元朗盤大賤賣33萬平過深圳樓](http://hk.apple.nextmedia.com/financeestate/art/20060916/6319191)
50. [焦點故事：屯門私樓　170萬上車](http://hk.apple.nextmedia.com/financeestate/art/20140208/18618405)
51. [長洲舊樓石屎剝落
    飛墮行人路](http://hk.apple.nextmedia.com/realtime/news/20130619/51492496)
52. [明報周刊
    更換水管的煩惱 2386期封面故事](http://www.waterpipeplus.com/index.php?c=msg&id=438&)

53. [粉嶺
    聯和墟唐樓180萬起](http://hd.stheadline.com/arts/arts_content.asp?contid=151836&srctype=g)
54. [阿婆也劏房　收租2.8萬](http://hk.apple.nextmedia.com/financeestate/art/20130619/18303530)
55. [大圍唐樓套房租七千五](http://orientaldaily.on.cc/cnt/finance/20120905/00204_017.html)
56. [西貢唐樓
    賣本地好土產](http://hk.apple.nextmedia.com/realtime/magazine/20150128/53378255)
57. [上水唐樓兩年冧價30%](http://orientaldaily.on.cc/cnt/finance/20140410/00204_004.html)
58. [禁區沙頭角墟](http://hd.stheadline.com/culture/culture_content.asp?contid=207010&srctype=g)