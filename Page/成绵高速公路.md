**成绵高速公路**是[成都至](../Page/成都.md "wikilink")[绵阳的](../Page/绵阳.md "wikilink")[高速公路](../Page/高速公路.md "wikilink")，于1998年建成，全长92.27公里，设计时速为100公里。成绵高速公路穿越[成都平原上的成都](../Page/成都平原.md "wikilink")、[德阳](../Page/德阳.md "wikilink")、绵阳三市，是中国国家高速公路网中以及的一部分，全线双向四车道，起于成都白鹤林，止于绵阳磨家，南接，北接[绵广高速公路](../Page/绵广高速公路.md "wikilink")。

## 沿线出入口及服务区

## 外部链接

  -
[G](../Category/四川省高速公路.md "wikilink")
[高](../Category/成都交通.md "wikilink")
[Category:绵阳交通](../Category/绵阳交通.md "wikilink")
[Category:绵阳地理](../Category/绵阳地理.md "wikilink")