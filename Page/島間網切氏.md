**島間網切氏**（），韩国的一个[朝鲜族姓氏本贯](../Page/朝鲜族姓氏.md "wikilink")，源自[归化的](../Page/归化.md "wikilink")[日本姓氏](../Page/日本姓氏.md "wikilink")**網切**（），是網切氏的唯一一個本貫。

该本貫的始祖是，是[第二次世界大戰結束時在韩日本人後裔遗留的孤兒](../Page/第二次世界大戰.md "wikilink")，後來歸化成為韓國人。本貫地「島間」取自於網切一郎祖父的出生地——[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[南種子町島間](../Page/南種子町.md "wikilink")（）村。

根據[韓國在](../Page/韓國.md "wikilink")1985年的[人口普查](../Page/人口普查.md "wikilink")，當時韓國全國以「網切」為姓的人只有五人\[1\]，而到了[2000年人口調查](../Page/2000年韓國人口普查.md "wikilink")，這個家族的總人口已增至10人\[2\]。

## 參考

<references />

## 參看

  - [日系人](../Page/日系人.md "wikilink")

## 外部連結

  - 日語版[朝鮮日報](../Page/朝鮮日報.md "wikilink")：[已歸化日本人農夫　網切一郞氏](https://web.archive.org/web/20050307102537/http://japanese.chosun.com/site/data/html_dir/2003/02/03/20030203000031.html)


[網切](../Category/朝鮮語姓氏.md "wikilink")
[Category:日本裔](../Category/日本裔.md "wikilink")

1.  <http://www.burimhong.pe.kr/jaryo%20room/name%20tonggye-1985.htm>
2.