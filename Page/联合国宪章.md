《**联合国宪章**》是[联合国建立的基礎](../Page/联合国.md "wikilink")[條約](../Page/條約.md "wikilink")，它既确立了联合国的宗旨、原则和组织机构设置，又规定了成员国的责任、权利和义务，以及处理[国际关系](../Page/国际关系.md "wikilink")、维护世界和平与安全的基本原则和方法。遵守《联合国宪章》、维护联合国威信，是每个成员国不可推脱的责任。

## 签字与生效

联合国宪章是1945年6月26日[联合国国际组织会议结束时在美国](../Page/联合国国际组织会议.md "wikilink")[旧金山的](../Page/旧金山.md "wikilink")签字，于1945年10月24日生效。為了表揚中華民國長期抗戰的功勞，中華民國代表在联合国宪章上第一個簽字，主辦國美國代表最後一個簽字\[1\]。

## 历次修正案

### 宪章第23、27、61条修正案

該次修正案于1963年12月17日通过，并于1965年8月31日生效。

第23条修正案规定安全理事会成员自十一国增至十五国。

第27条修正案规定安全理事会关于程序事项的决定及关于一切其他事项的决定的可决票门限，由7国增至9国，其中包括安全理事会五个[常任理事国的同意票作出](../Page/常任理事国.md "wikilink")。

第61条修正案将经济及社会理事会的成员自18国增至27国。

### 宪章第109条修正案

第109条修正案于1965年12月20日通过，并于1968年6月12日生效。

第109条修正案修正该条第1项，规定“会员国为审查本宪章，得以大会会员国三分之二表决，经安全理事会任何九个理事国（原先为七个）之表决，确定日期及地点举行全体会议”。

该条第3项涉及大会第十届常会期间可能举行审查会议的问题，仍保留原行文：“安全理事会任何七个理事国之表决”，

### 宪章第61条的进一步修正案

第61条的进一步修正案于1971年12月20日通过，并于1973年9月24日生效。该条的本次修正案将经社理事会的成员再增至54国。

## 联合国宪章序言

[UNITED_NATIONS_-_PREAMBLE_TO_THE_CHARTER_OF_THE_UNITED_NATIONS_-_NARA_-_515901.jpg](https://zh.wikipedia.org/wiki/File:UNITED_NATIONS_-_PREAMBLE_TO_THE_CHARTER_OF_THE_UNITED_NATIONS_-_NARA_-_515901.jpg "fig:UNITED_NATIONS_-_PREAMBLE_TO_THE_CHARTER_OF_THE_UNITED_NATIONS_-_NARA_-_515901.jpg")
**我联合国人民同兹决心，**

  -
    欲免后世再遭今代人类两度身历惨不堪言之战祸，
    重申[基本人权](../Page/基本人权.md "wikilink")、人格尊严与价值，以及男女与大小各国平等权利之信念，
    创造适当环境，俾克维持正义，尊重由条约与国际法其他渊源而起之义务，久而弗懈，
    促成大自由中之社会进步及较善之民生；

**并为达此目的，**

  -
    力行容恕，彼此以善邻之道，和睦相处，
    集中力量，以维持国际和平及安全，
    接受原则，确立方法，以保证非为[公共利益](../Page/公共利益.md "wikilink")，不得使用武力，
    运用[国际机构](../Page/国际机构.md "wikilink")，以促成全球人民经济及社会之进展，

**用是发愤立志，务当同心协力，以竟厥功。**

  -
    爰由我各本国政府，经齐集[金山市之代表](../Page/旧金山市.md "wikilink")，各将所奉全权证书互相校阅，均属妥善；议定本联合国宪章，并设立[国际组织](../Page/国际组织.md "wikilink")，定名联合国。

## 舊金山會議中國代表團

此次會議[中國派出的代表和高等顧問共十一位](../Page/中華民國_\(大陸時期\).md "wikilink")，正式排定的簽署順序為：

  - [國民政府](../Page/國民政府.md "wikilink")[行政院代理院長](../Page/行政院.md "wikilink")[宋子文](../Page/宋子文.md "wikilink")（首席代表）
  - [駐英國大使](../Page/中華民國駐英國大使.md "wikilink")[顧維鈞](../Page/顧維鈞.md "wikilink")
  - [國民參政會主席](../Page/國民參政會.md "wikilink")[王寵惠](../Page/王寵惠.md "wikilink")
  - 駐美國大使[魏道明](../Page/魏道明.md "wikilink")
  - 前駐美國大使[胡適](../Page/胡適.md "wikilink")
  - [金陵女子大学校長](../Page/金陵女子大学.md "wikilink")[吳貽芳](../Page/吳貽芳.md "wikilink")
  - [中国青年党代表](../Page/中国青年党.md "wikilink")[李璜](../Page/李璜.md "wikilink")
  - [中国民主社会党代表](../Page/中国民主社会党.md "wikilink")[張君勱](../Page/張君勱.md "wikilink")（[中華民國憲法起草人](../Page/中華民國憲法.md "wikilink")）
  - [中國共產黨代表](../Page/中國共產黨.md "wikilink")[董必武](../Page/董必武.md "wikilink")
  - [大公報總編輯](../Page/大公報.md "wikilink")[胡霖](../Page/胡霖.md "wikilink")
  - 高等顧問[施肇基](../Page/施肇基.md "wikilink")（資深外交官）

會議最後的簽字儀式，除宋子文、胡適因故不克參加外，其餘八位中華民國代表皆在憲章上簽字，以昭信守。

## 參考文獻

## 外部链接

  - [《联合国宪章》全文](http://www.un.org/chinese/aboutun/charter/charter.htm)

## 参见

  - [联合国制宪会议](../Page/联合国制宪会议.md "wikilink")
  - [聯合國憲章日](../Page/聯合國憲章日.md "wikilink")
  - [国际法](../Page/国际法.md "wikilink")
  - [日内瓦公约](../Page/日内瓦公约.md "wikilink")

{{-}}

[Category:聯合國憲章](../Category/聯合國憲章.md "wikilink")
[Category:宪制性法律](../Category/宪制性法律.md "wikilink")
[Category:國際法](../Category/國際法.md "wikilink")
[Category:1945年法律](../Category/1945年法律.md "wikilink")
[Category:20世紀條約](../Category/20世紀條約.md "wikilink")
[Category:國際公約](../Category/國際公約.md "wikilink")

1.