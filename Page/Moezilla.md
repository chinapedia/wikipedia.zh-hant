[Don't_touch_the_red_panda\!.jpg](https://zh.wikipedia.org/wiki/File:Don't_touch_the_red_panda!.jpg "fig:Don't_touch_the_red_panda!.jpg")
**Moezilla**（）是[日本的一個創作出](../Page/日本.md "wikilink")[擬人化美少女的](../Page/擬人化美少女.md "wikilink")[同人誌團體](../Page/同人誌.md "wikilink")「」的名稱，於2005年的[冬季](../Page/冬季.md "wikilink")[Comic
Market所發表的](../Page/Comic_Market.md "wikilink")[十八禁同人漫畫](../Page/十八禁.md "wikilink")《*Don't
touch the red panda\!*》中出現。

## 簡介

Moezilla，一如[OS娘一樣](../Page/OS娘.md "wikilink")，係以[電腦](../Page/電腦.md "wikilink")[軟體作為擬人化的對象](../Page/軟體.md "wikilink")，他們針對的是[Mozilla基金會所開發的軟體](../Page/Mozilla.md "wikilink")，將日文的「」與Mozilla合成為Moezilla，以表示[萌化的Mozilla軟體](../Page/萌.md "wikilink")。與OS娘一樣，這些人物也都畫成[女性](../Page/女性.md "wikilink")，以日本女性常取的XX子（XX-Ko）為[名字](../Page/名字.md "wikilink")（Nvu及Opera為例外）。不過Moezilla與[雙葉頻道](../Page/雙葉頻道.md "wikilink")（）並沒有任何關係。而「」之名又是從日本的Mozilla社群「」來的。

## 人物

### Firefox子

Firefox子（，「火狐娘」）是針對[Mozilla
Firefox瀏覽器所創作的擬人化](../Page/Mozilla_Firefox.md "wikilink")[角色](../Page/角色.md "wikilink")，人物造型頗類似日本廟宇的[赤狐石像](../Page/赤狐.md "wikilink")，她有一對日本漫畫常見的[獸耳](../Page/獸耳.md "wikilink")。顏色的搭配也與Firefox標誌類似。

### Thunderbird子

Thunderbird子（，「雷鳥娘」）是針對[Mozilla
Thunderbird這套](../Page/Mozilla_Thunderbird.md "wikilink")[電子郵件軟體所做的擬人化](../Page/電子郵件.md "wikilink")[角色](../Page/角色.md "wikilink")，她是Firefox子的好朋友。她的造型類似[電話](../Page/電話.md "wikilink")[接線生](../Page/接線生.md "wikilink")，總是帶著[鍵盤和](../Page/鍵盤.md "wikilink")[螢幕](../Page/螢幕.md "wikilink")。手裡常拿著[信件](../Page/信件.md "wikilink")。與Firefox子一樣，顏色的搭配Thunderbird標誌類似。

### SeaMonkey子

[SeaMonkey是一套源自Mozilla的瀏覽器](../Page/SeaMonkey.md "wikilink")（包含網頁瀏覽、電子郵件、新聞群組），而SeaMonkey子則是它的擬人化[角色](../Page/角色.md "wikilink")。

### Nvu大姐

[Nvu是Mozilla基金會出品的網頁編輯程式](../Page/Nvu.md "wikilink")，而Nvu大姐（）則是該程式的擬人化[角色](../Page/角色.md "wikilink")。

### Øpera娘

Øpera娘是根據Lawrence
Eng所建議而創造出來的[Opera瀏覽器擬人化](../Page/Opera.md "wikilink")[角色](../Page/角色.md "wikilink")，但Opera瀏覽器並非Mozilla基金會的軟體。Øpera娘被塑造成一個高雅但平易近人的[淑女](../Page/淑女.md "wikilink")，有著一頭銀灰色的頭髮，戴著紫色的[帽子](../Page/帽子.md "wikilink")，帽子上面有Opera的紅色[Ø標誌](../Page/Ø.md "wikilink")。

## 外部連結

  - [Moezilla 官方網站](http://piro.sakura.ne.jp/moezilla/)
  - 動漫畫家筆下的Firefox子、Thunderbird子或Opera娘的吉祥物：
      - [CornerValley](http://cornervalley.net) — karokaro
      - [inugamix](http://inu.imagines.jp)
  - [Firefox子與Thunderbird子的漫畫](http://piro.sakura.ne.jp/pics/_cartoon.html)
  - Nvu大姐
      - [HappyDeath-Nvu姐さん／擬人化](https://web.archive.org/web/20060622001705/http://happideath.com/archives/illust_other/2005/0711-1841.php)
  - Opera娘
      - <http://www.cjas.org/~leng/opera.htm>
      - <http://www.pandora.nu/tempo-depot/notes/The_other_side/Graffiti/d050113.htm>
      - <http://orera.g.hatena.ne.jp/keyword/%E3%81%8A%E3%81%BA%E3%82%89%E3%81%9F%E3%82%93>
      - <https://web.archive.org/web/20060603220116/http://haruka.saiin.net/~ryukaku/img/jhg350_2.jpg>
      - <https://web.archive.org/web/20060613082918/http://cornervalley.net/illust/pbbs/20050424.htm>
      - <http://www.pandora.nu/tempo-depot/notes/The_other_side/Graffiti/d050422.htm>

[en:Moezilla](../Page/en:Moezilla.md "wikilink")

[Category:電腦吉祥物](../Category/電腦吉祥物.md "wikilink")
[Category:萌擬人化](../Category/萌擬人化.md "wikilink")