**元朗公立中學**（；簡稱YLPSS、元中）是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗區的一所](../Page/元朗區.md "wikilink")[官立男女](../Page/官立學校.md "wikilink")[英文中學](../Page/英文中學.md "wikilink")，也是新界歷史最悠久的中學。

## 歷史

早於1930年代，[新界區並未有官立或資助中學成立](../Page/新界區.md "wikilink")，只有數間私立中學，大部份學生升讀中學必須遠赴[九龍區或](../Page/九龍區.md "wikilink")[香港島](../Page/香港島.md "wikilink")，故此元朗的鄉紳於1936年開始集資興建中學，元朗公立中學遂於1946年落成。因為元朗公立中學最初是由元朗的鄉紳集資興建，後集得約十萬資金，當時教育司署有見鄉紳之熱心，亦注資約十萬，鄉紳則見政府支持，乃將管理權悉於政府，政府亦感謝鄉紳回饋社會，故將校名立為「元朗公立中學」而非「元朗官立中學」，成為唯一名為「公立」的「官立中學」，其校歌歌詞亦云：「主理憑政府，公立表初衷」。起初元中建校於[凹頭](../Page/凹頭.md "wikilink")，後於1989年遷至[水牛嶺](../Page/水牛嶺.md "wikilink")[元朗公園旁現址](../Page/元朗公園.md "wikilink")，並於2003年完成擴展工程。\[1\][凹頭原址則現為](../Page/凹頭.md "wikilink")[東華三院馬振玉紀念中學](../Page/東華三院馬振玉紀念中學.md "wikilink")。

## 結構

中一至中六各設四班，其中中四至中六三班為兩個選修科班，一班為三個選修科班，全校共有24個班別。

## 設施

元朗公立中學校舍座落於[元朗公園旁](../Page/元朗公園.md "wikilink")，學校擁有的設施包括：\[2\]

  -
    33 個資訊科技設備課室
    1 個語言學習室
    5 個資訊科技特別室
    1 個電腦室
    2 個操場
    1 個禮堂
    1 個多媒體學習室
    1 個醫療室
    1 個電腦輔助學習室
    1 個家長及教師聯誼會資源室
    1 個學生會室

<!-- end list -->

  -
    1 個多功能室
    1 個文娛中心
    1 個會議室
    1 個學習室
    1 個聚會室
    1 個輔導室
    1 個社工室
    4 個教員室
    3 個高級管理室
    4 個實驗室
    1 個圖書館
    1 個音樂室

## 歷任校長

  - 鄧友山先生
  - 龍壽堂先生
  - 何國權先生
  - 鄧欽文先生
  - 倫賴小琴女士
  - 鄧容偉明女士（2006-2012）
  - 郭兆輝先生（2012-2017）
  - 余國健先生（2017-）

## 學生會

元朗公立中學第一屆學生會在2007至2008年度正式成立，學生會致力於為學生爭取福利，成為學生與學校之間的橋樑。

## 香港傑出學生選舉

直至2018年（第33屆），在[香港傑出學生選舉共出產](../Page/香港傑出學生選舉.md "wikilink")1名傑出學生。

## 傑出校友

  - [張志承](../Page/張志承.md "wikilink")：前[無綫新聞記者](../Page/無綫新聞.md "wikilink")
  - [汪正平](../Page/汪正平.md "wikilink")：[香港中文大學工程學院院長](../Page/香港中文大學.md "wikilink")
  - [鄧兆棠](../Page/鄧兆棠.md "wikilink")：註冊西醫，全國政協委員，曾任[元朗區議會主席](../Page/元朗區議會.md "wikilink")，臨時立法會議員，立法會議員，立法局議員，臨時區域市政局議員，區域市政局議員。以其冠名的學校為[元朗公立中學校友會鄧兆棠中學](../Page/元朗公立中學校友會鄧兆棠中學.md "wikilink")
  - [香灼璣](../Page/香灼璣.md "wikilink")：[公民教育委員會前主席](../Page/公民教育委員會.md "wikilink")
  - [鄧英敏](../Page/鄧英敏.md "wikilink")：[電視廣播藝員](../Page/電視廣播有限公司.md "wikilink")
  - [姜渭榮](../Page/姜渭榮.md "wikilink")：曾任[元朗公立中學校友會小學校長](../Page/元朗公立中學校友會小學.md "wikilink")
  - [梁科慶](../Page/梁科慶.md "wikilink")：[作家](../Page/作家.md "wikilink")
  - [黃陳小萍](../Page/黃陳小萍.md "wikilink")：現任[加拿大國會下議院](../Page/加拿大國會下議院.md "wikilink")[列治文選區國會議員](../Page/列治文選區.md "wikilink")
  - [甄澤權](../Page/甄澤權.md "wikilink")：[魔術師](../Page/魔術師.md "wikilink")
  - [趙俊霆](../Page/趙俊霆.md "wikilink")：[無綫新聞](../Page/無綫新聞.md "wikilink")[新聞透視高級記者及編輯](../Page/新聞透視.md "wikilink")
  - [王百羽](../Page/王百羽.md "wikilink")：[2015年香港區議會選舉元朗天恆選區候選人](../Page/2015年香港區議會選舉.md "wikilink")。
  - [鄧青雲](../Page/鄧青雲.md "wikilink")：[沃爾夫化學獎得主](../Page/沃爾夫化學獎.md "wikilink")
  - 劉永康：香港城市大學電子工程系副教授

### 校友會

元朗公立中學校友會於1988年註冊成為香港的辦學團體\[3\]，其辦學之學校包括：

  - [元朗公立中學校友會小學](../Page/元朗公立中學校友會小學.md "wikilink")（1989年創辦）
  - 元朗公立中學校友會劉良驤紀念幼稚園（1993年創辦）
  - [元朗公立中學校友會鄧兆棠中學](../Page/元朗公立中學校友會鄧兆棠中學.md "wikilink")（1999年創辦）
  - [元朗公立中學校友會鄧英業小學](../Page/元朗公立中學校友會英業小學.md "wikilink")（2003年創辦）
  - 廣東韶關元中校友會韶關新聯小學（2007年創辦）

其屬下之學校均以「勤孝友誠」作為校訓。

## 參考資料

## 外部連結

  - [元朗公立中學](http://www.ylpss.edu.hk)
  - [2017年十八區STEM學校巡禮](https://media.openschool.hk/images/temp/stem02_bookB.pdf)
  - [OpenSchool元朗公立中學介紹](https://www.openschool.hk/school/details/%E4%B8%AD%E5%AD%B8/%E5%85%83%E6%9C%97%E5%85%AC%E7%AB%8B%E4%B8%AD%E5%AD%B8)

[Category:元朗](../Category/元朗.md "wikilink")
[Y](../Category/元朗區中學.md "wikilink")
[Category:1946年創建的教育機構](../Category/1946年創建的教育機構.md "wikilink")
[Y](../Category/香港官立中學.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")

1.  [元朗公立中學：學校背景](http://www.ylpss.edu.hk/schoolnet2/about%20us/school%20background/school_background.htm)
2.  [元朗公立中學：學校設施](http://www.ylpss.edu.hk/schoolnet2/about%20us/school%20facilities/school_facilities.htm)
3.  [校友會出錢出力山區建校
    元朗公立中學師兄弟滿辦學熱誠](http://hk.news.yahoo.com/061222/12/1ysix.html)，《[明報](../Page/明報.md "wikilink")》2006年12月22日