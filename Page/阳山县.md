**阳山县**位于[中国](../Page/中国.md "wikilink")[广东省西北部](../Page/广东省.md "wikilink")，[南岭山脉的南面](../Page/南岭.md "wikilink")，[连江中游](../Page/连江.md "wikilink")，属于[清远市管辖](../Page/清远市.md "wikilink")。东边与广东省[英德市](../Page/英德市.md "wikilink")、[乳源瑶族自治县交界](../Page/乳源瑶族自治县.md "wikilink")，南边与广东省[怀集县](../Page/怀集县.md "wikilink")、[广宁县](../Page/广宁县.md "wikilink")、[清新县毗邻](../Page/清新县.md "wikilink")，西边与广东省[连南瑶族自治县](../Page/连南瑶族自治县.md "wikilink")、[连州市接壤](../Page/连州市.md "wikilink")，北边与湖南省[宜章县相连](../Page/宜章县.md "wikilink")，总面积为3372平方公里。2001年总人口为52万。县政府驻地在阳城镇。

## 行政区划

2005年5月下辖12个镇：阳城镇、岭背镇、青莲镇、七拱镇、太平镇、黎埠镇、小江镇、黄坌镇、江英镇、杜步镇、杨梅镇、大崀镇，1个民族乡：秤架瑶族乡。

## 历史

阳山县因[秦朝末年在县境设](../Page/秦.md "wikilink")[阳山关而得名](../Page/阳山关.md "wikilink")，[西汉](../Page/西汉.md "wikilink")[武帝](../Page/汉武帝.md "wikilink")[元鼎](../Page/元鼎.md "wikilink")6年（公元前111年）置县，迄今已有2100多年的历史。公元803年，[唐代大文豪](../Page/唐.md "wikilink")[韩愈被贬任阳山县令时](../Page/韩愈.md "wikilink")，曾发出“吾州之山水名天下”（见《燕喜亭记》，燕喜亭，位于连州中学）等赞美阳山的名句。

## 气候

阳山位于[珠江三角洲与内陆的结合部](../Page/珠江三角洲.md "wikilink")，属于中[亚热带季风气候](../Page/亚热带.md "wikilink")，全年季节分明，温度适宜，光照充足，雨量充沛。

## 民族和语言

阳山县居民以[汉族人为主](../Page/汉族.md "wikilink")，另有一部分[瑶族人](../Page/瑶族.md "wikilink")。

阳山县主要[语言是阳山本地话](../Page/语言.md "wikilink")，是[粤语的一个分支](../Page/粤语.md "wikilink")；此外，还有一部分居民习惯使用[客家话和](../Page/客家话.md "wikilink")[瑶语](../Page/瑶语.md "wikilink")。

## 经济

阳山县的经济以[农业为主](../Page/农业.md "wikilink")，2001年农业总产值占工农业总产值的65.3%。阳山的地势差别很大，形成了差异巨大的气候特征，适宜种植不同气候带的作物及[反季节蔬菜](../Page/反季节蔬菜.md "wikilink")。阳山富有特色的农业产品有阳山鸡、竹八鲤、同冠[梨](../Page/梨.md "wikilink")、[淮山](../Page/淮山.md "wikilink")、[板栗](../Page/板栗.md "wikilink")、[沙田柚等](../Page/沙田柚.md "wikilink")。

在[工业方面](../Page/工业.md "wikilink")，阳山的[煤](../Page/煤.md "wikilink")、[铁](../Page/铁.md "wikilink")、[铅](../Page/铅.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[大理石等矿产资源丰富](../Page/大理石.md "wikilink")，水力资源蕴藏量巨大，吸引了不少厂商前来投资，已先后开发了石螺、城南、光明、杜步等四个工业园区。

在[旅游业方面](../Page/旅游业.md "wikilink")，阳山拥有丰富的旅游资源，旅游业发展迅速，每年举行的“中国·阳山四驱越野车节”已成为拉动阳山经济发展的一个新动力。

## 旅游

阳山的人文景观和自然景观丰富，主要的旅游景点有广东第一峰旅游风景区、神笔洞、七拱学发公祠、水口小桂林风光、石螺森林温泉、县城贤令山、[韩愈遗迹和墨迹](../Page/韩愈.md "wikilink")、摩崖石刻等。在广东第一峰旅游风景区内拥有广东第一高峰，海拔1902米的南岭主峰“石坑崆”，景区内原始森林和高山峡谷密布，[喀斯特地形明显](../Page/喀斯特地形.md "wikilink")，野生动植物众多。

## 交通

阳山自古就是交通要道，其北面的阳山关是历代名人进入岭南的必经之道之一。

现在，阳山的水陆交通也很便利，[107国道和](../Page/107国道.md "wikilink")[323国道贯穿阳山全境](../Page/323国道.md "wikilink")，水路运输直通[清远](../Page/清远.md "wikilink")、[广州等地](../Page/广州.md "wikilink")，可通航150吨的[货轮](../Page/货轮.md "wikilink")。

## 外部链接

  - [阳山县政府公众网](https://web.archive.org/web/20160410140641/http://www.yangshan.gov.cn/)

[阳山县](../Category/阳山县.md "wikilink")
[县](../Category/清远区县市.md "wikilink")
[清远](../Category/广东省县份.md "wikilink")