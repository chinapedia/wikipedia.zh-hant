[Памятник_М._И._Калинину_в_Твери.JPG](https://zh.wikipedia.org/wiki/File:Памятник_М._И._Калинину_в_Твери.JPG "fig:Памятник_М._И._Калинину_в_Твери.JPG")[特維爾](../Page/特維爾.md "wikilink")\]\]
**米哈伊尔·伊万诺维奇·加里宁**（，），[蘇聯政治家](../Page/蘇聯.md "wikilink")、革命家、早期的國家領導人。自从[十月革命后到去世为止](../Page/十月革命.md "wikilink")，一直担任苏俄和苏联的[国家元首](../Page/苏联国家元首列表.md "wikilink")。

## 生平

加里寧出生於[俄羅斯帝國的](../Page/俄羅斯帝國.md "wikilink")[特維爾省](../Page/特維爾省.md "wikilink")，為農家子弟，年輕時曾受僱於地主。後來前往[聖彼得堡的炮兵工廠工作](../Page/聖彼得堡.md "wikilink")，並且於1891年參與[革命運動](../Page/革命.md "wikilink")；1898年加入[俄羅斯社會民主工黨](../Page/俄羅斯社會民主工黨.md "wikilink")。後來加里寧加入[列寧的](../Page/列寧.md "wikilink")[布爾什維克](../Page/布爾什維克.md "wikilink")，並且之後有好幾次被[俄羅斯帝國政府逮捕與脫逃的紀錄](../Page/俄羅斯帝國.md "wikilink")。

十月革命之後，1919年加里寧成為[俄国共產黨](../Page/俄国共產黨.md "wikilink")（[布尔什维克](../Page/布尔什维克.md "wikilink")）中央委員；同年三月，加里寧成為俄羅斯最高苏维埃中央執行委員會委員長，等同於當時的[國家元首](../Page/苏联国家元首列表.md "wikilink")；1922年[蘇維埃社會主義共和國聯盟正式成立之後](../Page/蘇維埃社會主義共和國聯盟.md "wikilink")，加里寧成為[苏联中央執行委員會委員長](../Page/苏联中央執行委員會.md "wikilink")。最高苏维埃改組之後，加里寧成為[苏联最高苏维埃主席团主席](../Page/苏联最高苏维埃主席团.md "wikilink")，到1946年過世為止都保有這個地位。1925年開始為[黨中央政治局委員](../Page/蘇聯共產黨中央政治局.md "wikilink")。

加里寧與列寧一樣都是相當關心勞工與農民的領導人，常常以國家領導人身份與一般市民接觸並且了解市民的希望，並且展現一個「好爺爺」的風範；在[大清洗期間也有不少人寫信給加里寧](../Page/大清洗.md "wikilink")，而加里寧也常常介入審判，因此救了不少無辜的人。很多人也稱呼加里寧為「親切的祖父加里寧」。但是在对波兰战俘的[卡廷大屠殺事件中](../Page/卡廷大屠殺.md "wikilink")，加里寧卻也在公文上簽名批准。

[第二次世界大戰後的](../Page/第二次世界大戰.md "wikilink")1946年，加里寧於[莫斯科過世](../Page/莫斯科.md "wikilink")；而蘇聯也將過去屬於[德國](../Page/德國.md "wikilink")[東普魯士的](../Page/東普魯士.md "wikilink")[哥尼斯堡改名為](../Page/哥尼斯堡.md "wikilink")[加里寧格勒](../Page/加里寧格勒.md "wikilink")。

## 参考文献

{{-}}

[Category:蘇聯國家元首](../Category/蘇聯國家元首.md "wikilink")
[Category:蘇聯政治人物](../Category/蘇聯政治人物.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:第二次世界大戰領袖](../Category/第二次世界大戰領袖.md "wikilink")
[Category:蘇聯第二次世界大戰人物](../Category/蘇聯第二次世界大戰人物.md "wikilink")
[Category:俄國革命家](../Category/俄國革命家.md "wikilink")
[Category:俄羅斯共產主義者](../Category/俄羅斯共產主義者.md "wikilink")
[Category:俄羅斯馬克思主義者](../Category/俄羅斯馬克思主義者.md "wikilink")
[Category:特維爾州人](../Category/特維爾州人.md "wikilink")
[Category:安葬於克里姆林宮紅場墓園者](../Category/安葬於克里姆林宮紅場墓園者.md "wikilink")
[Category:老布尔什维克](../Category/老布尔什维克.md "wikilink")