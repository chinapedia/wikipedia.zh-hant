**侏儒蚺屬**（[學名](../Page/學名.md "wikilink")：*Exiliboa*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[林蚺科下的一個](../Page/林蚺科.md "wikilink")[單型](../Page/單型.md "wikilink")[屬](../Page/屬.md "wikilink")，專為一種主要分布於[墨西哥的無毒林蚺](../Page/墨西哥.md "wikilink")**侏儒蚺**（亦稱**瓦哈卡侏儒蚺**，*E.
placata*）而設。\[1\]目前未有任何亞種被確認。\[2\]

## 特徵

侏儒蚺具備洞棲性，擅長穿越於地洞之間。身體顏色呈深[黑色](../Page/黑色.md "wikilink")，暗泛光亮。\[3\]

## 地理分布

侏儒蚺主要分布於[墨西哥的](../Page/墨西哥.md "wikilink")[瓦哈卡州](../Page/瓦哈卡州.md "wikilink")。\[4\]

## 保育狀態

在[世界自然保護聯盟中](../Page/世界自然保護聯盟.md "wikilink")，侏儒蚺被列入[紅色名錄中的](../Page/世界自然保護聯盟瀕危物種紅色名錄.md "wikilink")「易危」（VU）級別。

## 備註

## 外部連結

  - [侏儒蚺屬](http://reptile-database.reptarium.cz/species.php?genus=Exiliboa&species=placata)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:林蚺科](../Category/林蚺科.md "wikilink")

1.

2.

3.  Mehrtens JM：《Living Snakes of the World in
    Color》頁480，[紐約](../Page/紐約.md "wikilink")：Sterling
    Publishers，1987年。ISBN 0-8069-6460-X

4.