**科布伦茨**，依粵語又譯**高本斯**（Koblenz，源於[拉丁語](../Page/拉丁語.md "wikilink")*Confluentes*，匯流的意思）。臺灣議會之父[林獻堂於二十世紀初造訪此地時](../Page/林獻堂.md "wikilink")，曾在所著《環球遊記》一書中，將此城名譯為**科不林士**，是德國重要的觀光都市。位於[德國](../Page/德國.md "wikilink")[莱茵兰-普法尔茨州](../Page/莱茵兰-普法尔茨州.md "wikilink")，[摩泽尔河與](../Page/摩泽尔河.md "wikilink")[萊茵河交匯處](../Page/萊茵河.md "wikilink")。兩河匯流之處，稱[德意志角](../Page/德意志角.md "wikilink")，為當地名勝。人口
113.000
(2017)，是莱茵兰-普法尔茨州第三大城，儘次於[美茵茲](../Page/美茵茲.md "wikilink")、[路德維希港](../Page/路德維希港.md "wikilink")，並與[特里爾](../Page/特里爾.md "wikilink")、[凱撒斯勞滕共同構成該邦五大中心都市](../Page/凱撒斯勞滕.md "wikilink")。科布伦茨所在的[萊茵峽谷於](../Page/萊茵峽谷.md "wikilink")2002年被列入[聯合國世界文化遺產](../Page/聯合國世界文化遺產.md "wikilink")。

## 歷史

公元前55年，[尤利烏斯·凱撒指揮羅馬軍隊抵達](../Page/尤利烏斯·凱撒.md "wikilink")[萊茵河](../Page/萊茵河.md "wikilink")，並建成大橋連接[安德納赫](../Page/安德納赫.md "wikilink")。前8年[羅馬帝國在此建立軍事基地](../Page/羅馬帝國.md "wikilink")。

[中世紀時代](../Page/中世紀.md "wikilink")，曾被[法蘭克人佔據](../Page/法蘭克人.md "wikilink")。882年又被[諾曼人摧毀](../Page/諾曼人.md "wikilink")。925年，成為[神聖羅馬帝國一部分](../Page/神聖羅馬帝國.md "wikilink")。

在十八世纪末[法国大革命时期](../Page/法国大革命.md "wikilink")，是贵族保皇党人流亡和准备对革命的法国进行干涉的中心。

本城亦是這個時期保皇派領袖，奧國首相[梅特涅的出生地](../Page/梅特涅.md "wikilink")。

## 教育

1990年起，原有的**萊茵邦立高等師範學院**改制為，設有科布倫茲與[蘭道兩個校區](../Page/蘭道.md "wikilink")，校長辦公室及中央辦公區設在萊茵邦首府[美茵茲](../Page/美茵茲.md "wikilink")。截至2015/2016學年，兩校區共有15.757
名學生，是邦內僅次於[美茵茲大學的第二大校](../Page/美茵茲大學.md "wikilink")。

## 經濟

傳統上科布林茲是釀酒的中心。它位處[摩泽尔葡萄酒产区內](../Page/摩泽尔葡萄酒产区.md "wikilink")，同時啤酒生產亦很有名。其他工業有汽車配件生產、鋼琴、紙張、機械、鋁材等等。

於2012年9月12日，[阿馬遜在科布倫茲建立物流中心](../Page/亞馬遜公司.md "wikilink").\[1\] 。

## 姐妹城市

  - [諾里奇](../Page/諾里奇.md "wikilink")

  - Haringey

  - [馬斯特里赫特](../Page/馬斯特里赫特.md "wikilink")

  - [訥韋爾](../Page/訥韋爾.md "wikilink")

  - [諾瓦拉](../Page/諾瓦拉.md "wikilink")

  - Petah Tikva

  - [德克薩斯州](../Page/德克薩斯州.md "wikilink")[奧斯汀](../Page/奧斯汀.md "wikilink")

## 參考

[K](../Category/莱茵兰-普法尔茨州市镇.md "wikilink")
[Category:前0年代建立](../Category/前0年代建立.md "wikilink")

1.  <http://www.rhein-zeitung.de/regionales_artikel,-Bei-Amazon-in-Koblenz-arbeiten-bald-3000-Leute-_arid,494182.html>
    (Rhein-Zeitung newspaper, in German language)