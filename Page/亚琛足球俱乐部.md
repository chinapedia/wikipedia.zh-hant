**阿勒曼尼亞·亞琛體育會**（[德語](../Page/德語.md "wikilink")：****）是位於[德國西南部](../Page/德國.md "wikilink")[北莱茵-威斯特法伦的](../Page/北莱茵-威斯特法伦.md "wikilink")[-{zh-hans:亚琛;
zh-hk:阿亨;}-市的](../Page/阿亨.md "wikilink")-{zh-hans:体育俱乐部;
zh-hk:體育會;}-。亞琛長時期在[德國乙組聯賽中比賽](../Page/德乙.md "wikilink")，只曾在60年代末期升上[德國甲組聯賽三個年頭](../Page/德甲.md "wikilink")，但2005–06年球季亞琛成功獲得德乙的亞軍席位，可以升級德甲再嘗頂級聯賽的滋味。

升入德甲后俱乐部却连年成绩糟糕，2007–08赛季降入德乙、2012–13赛季滑入德丙，2012年下半年俱乐部遭遇破产。按规定，球队2013–14赛季将降入第四级别的[德国足球地区联赛](../Page/德国足球地区联赛.md "wikilink")。\[1\]

## 球會歷史

### 成立到大戰時期

足球俱乐部在1900年12月16日由 18
名高中學生建立。当时已经有一支名為「亚琛第一足球俱乐部」（**）的球队，所以这支新足球俱乐部命名为「阿勒曼尼亚足球俱乐部」（**），是古德国的[拉丁语名字](../Page/拉丁语.md "wikilink")。在[第一次世界大战中遭受嚴重的打擊](../Page/第一次世界大战.md "wikilink")，戰前有
200 多名會员，到戰後只剩下 37 人。1919年年初，「阿勒曼尼亚」与「亚琛人 1847 體操俱樂部」（**）合并，成为「阿勒曼尼亚·亚琛
1900
體操俱樂部」（**）。但是新伙伴的主要兴趣是[体操](../Page/体操.md "wikilink")，没有多少共同处，在1924年再次分开。

[-{zh-hans:亚琛;
zh-hk:阿亨;}-位於德国](../Page/阿亨.md "wikilink")、[比利时和](../Page/比利时.md "wikilink")[荷兰交界处](../Page/荷兰.md "wikilink")，「阿勒曼尼亚」经常碰到这两国国家的球队，其首場比賽對手正正是比利時的「皇家杜哈林」（）。球隊在莱茵-威斯特法伦（）足總屬下的聯賽作賽，更於1907年獲得首個冠軍，其後於1909年加入新成立的「德國西部聯賽」（）。

由於群眾對足球的興趣增加而令球隊穩步擴展，於1921年獲得「莱茵省際聯賽」（）參賽資格，1928年興建主場球場，更於翌年升級「上級聯賽」（）。球隊在30年代初期取得一定的成就，曾經晉級「德國西部錦標賽」（Westdeutsche
championship）的附加賽四強。稍後[第三帝国重組德國聯賽](../Page/第三帝国.md "wikilink")，建立由 16
隊角逐的頂級「省際聯賽」（Gauliga）。於30年代末及40年代初，球隊有數個球季在「中萊茵省際聯賽」（Gauliga
Mittelrhein）中角逐，並在1938年獲得冠軍晉級「全國錦標賽」決賽週。但同組的「波易爾06」（Beuel
06）提出抗議，最後更獲承認為真正冠軍得主，但時間已經太遲，未能取代「阿勒曼尼亚」參賽「全國錦標賽」附加賽。

### 戰後到加入德甲

[第二次世界大戰後的](../Page/第二次世界大戰.md "wikilink")1946年，「盟國管制理事會」（Allied Control
Council）對[佔領地區大部分組織解禁](../Page/盟軍佔領下的德國.md "wikilink")，「阿勒曼尼亚」重組並參加「萊茵河地區」（Rheinbezirk）的次級聯賽。翌年返回「西部上級聯賽」（Oberliga
West），但遇上財政困難，只能維持穩定但不特出的中游位置。

亚琛首項重大成就是於1953年晉級[德國杯的決賽](../Page/德国足协杯.md "wikilink")，以 1-2
僅負於[红白埃森獲得亞軍](../Page/红白埃森足球俱乐部.md "wikilink")。當德國新的職業聯賽[德甲在](../Page/德甲.md "wikilink")1963年組成時，亚琛只能棲身「西部地區聯賽（第二組）」（）。
1965年亚琛再次晉級德國杯決賽，但今次以 0-2
負於[-{zh-hans:多特蒙德;zh-hk:多蒙特;zh-tw:多特蒙德;}-](../Page/多特蒙德足球俱乐部.md "wikilink")。

1967年亚琛獲得「西部地區聯賽」冠軍，升級[德甲聯賽](../Page/德甲.md "wikilink")。在德甲的第二季（1968/69年），積分榜只落後於[拜仁慕尼黑屈居第二](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")。不過下一個球季卻是一個災難：球隊全季作客的比賽只能獲得
1 分，以第 18 名結束及降級回到「西部地區聯賽」。於1991年更降到第三級的「北萊茵上級聯賽」（Am.Oberliga
Nordrhein）。

### 復甦之路

[Aachen_ASEAG_Alemannia.jpg](https://zh.wikipedia.org/wiki/File:Aachen_ASEAG_Alemannia.jpg "fig:Aachen_ASEAG_Alemannia.jpg")
在1990年代下半期經歷數個平凡的球季後，主教練（）重整旗鼓，改打 4-4-2 陣式，取消[-{zh-hans:自由人;
zh-hk:清道夫;}-](../Page/後衛_\(足球\).md "wikilink")，建造成一隊進攻流暢而富吸引力的球隊。1999年亚琛表現出色，下半季更為強勁，獲得破球隊紀錄的
11
連勝，在球季結束前數週進佔榜首，但富克斯不幸在這時身故，全城為之震驚，球隊決心克服難關，將升級獻給已故的教練<small>\[2\]</small>，奪得「西部及西南部地區聯賽」（Regionalliga
West/Südwest）冠軍，升級[德乙](../Page/德乙.md "wikilink")。

亚琛在[德乙的首年無論在球場上或財政上皆遇上困難](../Page/德乙.md "wikilink")，此後數年球隊艱苦經營，當違規的財務被揭露，顯示球隊接近破產邊緣而令情況更加惡劣。當由主席霍斯特·海因里克斯（）、主教練迪特·黑金（）及經理约尔格·施马特克（）組成新的執行董事會接手管理後亚琛獲得轉機，透過改善的財務管理、精明的球員收購及聰明的比賽策略，亚琛在
2003-04
年球季重震雄風，在[德國盃決賽之路先後淘汰](../Page/德國盃.md "wikilink")[慕尼黑1860](../Page/慕尼黑1860.md "wikilink")、[拜仁慕尼黑及](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")[-{zh-hans:门兴格拉德巴赫;
zh-hk:慕遜加柏;}-](../Page/门兴格拉德巴赫足球俱乐部.md "wikilink")，才在決賽 2-3
僅負當年[德甲盟主](../Page/德甲.md "wikilink")[-{zh-hans:云达不来梅;
zh-hk:雲達不萊梅;}-](../Page/云达不来梅足球俱乐部.md "wikilink")。由於不来梅已取得[歐聯參賽資格](../Page/歐聯.md "wikilink")，亚琛得以參賽[-{zh-hans:欧洲联盟杯;
zh-hk:歐洲足協盃;}-](../Page/欧洲联盟杯足球赛.md "wikilink")，表現合乎期望，晉級 16
強階段才被[阿爾克馬爾淘汰](../Page/AZ阿爾克馬爾.md "wikilink")。亚琛在德國盃及歐洲的比賽理想表現大大改善球隊的財務狀況。

### 破产跌入德丁

2006年4月16日亚琛成為首支確定來季升級[德甲的球隊](../Page/德甲.md "wikilink")，結束亚琛長達 36
年缺席頂級聯賽的等待。2006/07球季回升德甲後，在最后八轮联赛仅獲得1分，僅一季即回降乙組。2012-13赛季，球队又以德乙第17名的成绩滑入德丙。

2012年11月16日，由于缺少400万欧元以上的流动资金，处于德丙降级区的俱乐部宣布破产。按规定，球队2012-13赛季将降入第四级别的[德国足球地区联赛](../Page/德国足球地区联赛.md "wikilink")。\[3\]\[4\]

## 主場球場

[Aachen_Tivoli_under_construction.JPG](https://zh.wikipedia.org/wiki/File:Aachen_Tivoli_under_construction.JPG "fig:Aachen_Tivoli_under_construction.JPG")
**蒂沃利体育场**（）是亞琛的主場球場，建於1908年，球場曾數度進行擴建，最近一次大規模擴建，在1957年，增建坐席及夜間比賽燈光設備，於8月28日完成舉行揭幕，首戰[-{zh-hans:西班牙人;
zh-hk:愛斯賓奴;}-](../Page/西班牙人足球俱樂部.md "wikilink")。現時以可容 21,632 人（其中 3,632
為坐席）。

亞琛在2004年的[-{zh-hans:欧洲联盟杯;
zh-hk:歐洲足協盃;}-比賽](../Page/欧洲联盟杯足球赛.md "wikilink")，因球場容量不符規定而需借用[科隆的](../Page/科隆.md "wikilink")[萊茵能源球場進行主場比賽](../Page/萊茵能源球場.md "wikilink")。

2006年[亚琛市及球隊準備興建新的球場](../Page/亚琛.md "wikilink")，初時選址市外，鄰近當地的 Merzbruck
機場，但球迷普遍希望球場仍然留在市內。經過不斷的商討，於2007年2月公告新球場計劃，將在蒂沃利体育场現址的 Sportpark
Soers 內興建。

新球場於進行國內聯賽可容32,900名觀眾，其中19,655各為坐席及11,496名為立席，餘下包括[輪椅席](../Page/輪椅.md "wikilink")、酒廊及[記者席等](../Page/記者.md "wikilink")，當比賽不容許立席時，容量縮減到只有25,587人。

## 球會榮譽

  - **[德國杯](../Page/德国足协杯.md "wikilink")**亞軍（3次）：1953年、1965年、2004年

## 著名球星

  - [约普·德尔沃尔](../Page/约普·德尔沃尔.md "wikilink")（），曾任[西德國家隊教練](../Page/德國國家足球隊.md "wikilink")
  - [-{zh-hans:托尔斯滕·弗林斯; zh-hk:芬寧斯;}-](../Page/托尔斯滕·弗林斯.md "wikilink")（）
  - [Willi Landgraf](../Page/Willi_Landgraf.md "wikilink")，德乙出場次數紀錄保持者
  - [Erik Meijer](../Page/Erik_Meijer_\(soccer\).md "wikilink")
  - [Karlheinz Pflipsen](../Page/Karlheinz_Pflipsen.md "wikilink")
  - [Erik van der Luer](../Page/Erik_van_der_Luer.md "wikilink")
  - [雷因霍尔特·蒙岑伯格](../Page/雷因霍尔特·蒙岑伯格.md "wikilink")（Reinhold
    Münzenberg），球隊首名國家隊球員
  - [谢晖](../Page/谢晖.md "wikilink")

## 球會軼事

[Colorado_potato_beetle.jpg](https://zh.wikipedia.org/wiki/File:Colorado_potato_beetle.jpg "fig:Colorado_potato_beetle.jpg")

  - 亚琛渾名[马铃薯](../Page/马铃薯.md "wikilink")[甲蟲](../Page/鞘翅目.md "wikilink")（）是因為其黃黑直間球衣與該類昆蟲十分相似。
  - 亚琛及波易爾06（Beuel
    06）同時自稱奪得1938年的中莱茵省際聯賽（）錦標賽冠軍，德國足球聯會（，簡稱）很遲才決定將分數判給波易爾06使其成為合法冠軍，但其時亚琛已參加全國錦標賽決賽週。

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [亚琛官方網站](http://www.alemannia-aachen.de)
  - [Abseits Guide to German
    Soccer](http://www.abseits-soccer.com/clubs/aachen.html)

[A](../Category/德國足球俱樂部.md "wikilink")
[Category:北莱茵-威斯特法伦州体育组织](../Category/北莱茵-威斯特法伦州体育组织.md "wikilink")
[Category:1900年建立的足球俱樂部](../Category/1900年建立的足球俱樂部.md "wikilink")

1.
2.  [In Memoriam Werner
    Fuchs](http://www.ochehoppaz.de/Danke_Werner.htm)
3.
4.