**惡搞文化**，指的是对[現實主题加以](../Page/現實.md "wikilink")[虛偽](../Page/虛偽.md "wikilink")。

从而[建构出](../Page/建构.md "wikilink")[喜剧或](../Page/喜剧.md "wikilink")[諷刺效果的胡鬧娱乐文化](../Page/諷刺.md "wikilink")。常見形式是將一些既成話題，節目等改編後再次發布，屬於[二次創作的其中一手法](../Page/二次創作.md "wikilink")。惡搞在當代流行文化中很常見。

香港樂壇有史以來第一首、也是目前最成功的一首[改編詞](../Page/改編詞.md "wikilink")︰[黃霑的填詞作品](../Page/黃霑.md "wikilink")《誰是大英雄》被[黎彼得惡搞成為](../Page/黎彼得.md "wikilink")[許冠傑的](../Page/許冠傑.md "wikilink")《打雀英雄傳》。

## 詞源

「惡搞」一詞的最初使用情況並不清楚。有人認為，「惡搞」一詞來自日語的**Kuso**，是一種經典的[網上次文化](../Page/網上次文化.md "wikilink")，由[日本的遊戲界傳入](../Page/日本.md "wikilink")[台灣及](../Page/台灣.md "wikilink")[中國大陸](../Page/中國大陸.md "wikilink")，成為了台灣[BBS網路上一種特殊的文化](../Page/BBS.md "wikilink")。這種新文化然後再經由網路傳到[香港至世界各地](../Page/香港.md "wikilink")。

不過，關於Kuso一詞在華文領域中最早出現的可能性倒是有一種說法。Kuso一詞應源於[kuso
game](../Page/kuso_game.md "wikilink")《[死亡火槍](../Page/死亡火槍.md "wikilink")》中的主角[越前康介遭受敵人攻擊時的慘叫聲](../Page/越前康介.md "wikilink")「くそ（kuso）」。在1996年時，台灣有一群遊戲玩家，因為日本雜誌[Saturn
Magazine](../Page/Saturn_Magazine.md "wikilink")（現名ドリマガ）最爛遊戲專欄的持續介紹，開始對死亡火槍感興趣，進而將此作品的主角越前康介的慘叫聲「」掛在嘴邊成為口頭禪，久而久之，kuso慢慢成為台灣人世界中惡搞的代名詞之一。

Kuso這詞語常使人摸不清楚其意思，主要的爭議點在於最早使用這個口頭禪的年輕玩家們，本身不熟悉這個日本詞彙的詳細意義，也無太大企圖心，因此對這詞的詳細用法並無嚴格定義規範，純粹只是當做一種心情感嘆上的口頭禪，早期的意思與原本[kuso
game的用法比較相似](../Page/kuso_game.md "wikilink")，指“很爛，爛到讓人發笑”的意思。

但其實這個字是[日語的](../Page/日語.md "wikilink")，也就是[糞](../Page/糞便.md "wikilink")。在日文中引申的用法還有很多，例如口語中的粗話（類似英語語系中的粗話——[shit](../Page/shit.md "wikilink")）；「很爛，爛到讓人發笑」也是其中之一，與台灣人文化圈中的用法不同，日本人使用時貶低意味較濃，因此kuso等於惡搞的用法在日本並不適用。甚至誤用的話還可能造成誤會。另外一個解釋是日語的（kūsō），也就是空想。這個說法在文意上較說得通。

> KUSO如果沒有創意，KUSO就不過是坨KUSO而已。
> 　　此名言的意義是：創意是惡搞的神髓。若果惡搞缺乏創意，惡搞將會有如糞土一般毫無價值可言。

但是，惡搞的這種行為方式，早在「Kuso」一詞出現之前在世界各國就已存在，惡搞和[無厘頭就難以區分](../Page/無厘頭.md "wikilink")，可以認為，惡搞只是對古已有之的某種滑稽幽默的行為的另一種說法。下面例子中很多被認為是惡搞的行為，也並不能證明和日本的「Kuso」有什麼關係。

在英文中，與惡搞意思相近的詞語是
"parody"。在西方流行文化中，惡搞同樣常見。只是parody這個詞本身就是正式用語，可被視為藝術手段的一種。不像「惡搞」一詞，是由民間流傳形成的非正式語言。

## 惡搞目的

惡搞（détournement, kuso）運用已存在的形式、概念，將之加以改造後,
對原先的意義和作用產生反噬和顛覆力，並以此傳達不同的、甚至是相反的訊息。惡搞是一種[快樂抗爭](../Page/快樂抗爭.md "wikilink")。

## 惡搞慣例

1.  禁止[原創研究](../Page/原創研究.md "wikilink")。因為沒有被惡搞的對象，就無惡搞可言。因此[二次創作是惡搞的](../Page/二次創作.md "wikilink")[必要條件](../Page/必要條件.md "wikilink")。
2.  惡搞仍被[版權保護的作品](../Page/版權.md "wikilink")，應匿名發表與禁止牟利行為。不然創作者會有被提告侵權的危險。
3.  重點在於[幽默](../Page/幽默.md "wikilink")，「好的惡搞」必須要能讓人發出會心的一笑。
4.  惡搞應該拿捏分寸，過於詆毀原作或特定人物者，就變成[毀謗](../Page/毀謗.md "wikilink")，而非惡搞了。
5.  惡搞不是炒冷飯或[老梗的集結](../Page/老梗.md "wikilink")，創意是惡搞的主要神髓。
6.  為避免引起公眾恐慌，不得使人誤以為是真實事件。詳見：[:Category:虚假新闻事件](../Category/虚假新闻事件.md "wikilink")

## 惡搞範例

下列例子中，有些事物本身創作或創始的目的即在於惡搞，并無其他目的。有些影視節目或人物是以惡搞為其賣點，著重考慮商業效益。如：

### 娛樂

  - [偽基百科](../Page/偽基百科.md "wikilink")
  - [毛記電視](../Page/毛記電視.md "wikilink")
  - [亞洲電視節目](../Page/亞洲電視.md "wikilink")《[香港亂噏](../Page/香港亂噏.md "wikilink")》
  - [無綫電視節目](../Page/無綫電視.md "wikilink")《[荃加福祿壽](../Page/荃加福祿壽.md "wikilink")》
  - [香港網絡大典系列](../Page/香港網絡大典.md "wikilink")
  - 2010年香港[申訴專員公署廣告](../Page/申訴專員公署.md "wikilink")
  - 《萬眾同心公益金》中的公益廣告慈善家：改編自其他廣告
  - [沒良心百貨公司](https://www.youtube.com/watch?v=4q6sGfu5jl4)：因為過於[無厘頭的旁白配音而在](../Page/無厘頭.md "wikilink")[Youtube爆紅](../Page/Youtube.md "wikilink")。
  - [金草莓獎](../Page/金酸莓奖.md "wikilink")：一個模仿[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")，為[垃圾電影有](../Page/垃圾電影.md "wikilink")「出頭」一天的獎項。
  - [搞笑諾貝爾獎](../Page/搞笑諾貝爾獎.md "wikilink")：模仿[諾貝爾獎](../Page/諾貝爾獎.md "wikilink")，頒獎給**無法重做亦不應被重現**的[科學研究](../Page/科學.md "wikilink")，例如：為什麼[肚臍垢大多是藍色的](../Page/肚臍.md "wikilink")？（2002年得獎研究之一）
  - [達爾文獎](../Page/達爾文獎.md "wikilink")：主要頒給對人類進化「有貢獻」的人；得獎者全因為幹了蠢事死掉或失去生育能力——意味他們的愚蠢基因不致流傳後代
    、禍及人類進化 。
  - 在[Youtube上出現](../Page/Youtube.md "wikilink")《[Call Me
    Maybe](../Page/Call_Me_Maybe.md "wikilink")》的改詞翻拍的惡搞版MV，片中男主角變成美國前總統[歐巴馬](../Page/歐巴馬.md "wikilink")。\[1\]
  - 搞笑電影《[這不是斯巴達](../Page/這不是斯巴達.md "wikilink")》惡搞電影《[300壯士：斯巴達的逆襲](../Page/300壯士：斯巴達的逆襲.md "wikilink")》，將《[變形金剛](../Page/變形金剛.md "wikilink")》、《[史瑞克](../Page/史瑞克.md "wikilink")》等電影元素融入其中。
  - 美國電影《[史詩大帝國](../Page/史詩大帝國.md "wikilink")》惡搞諸多賣座電影，包括《[神鬼奇航](../Page/神鬼奇航.md "wikilink")》，《[哈利波特](../Page/哈利波特.md "wikilink")》以及《[巧克力冒險工廠](../Page/巧克力冒險工廠.md "wikilink")》等等。
  - 美國電影《[雞餓遊戲](../Page/雞餓遊戲.md "wikilink")》用幽默詼諧的方式惡搞知名電影《[飢餓遊戲](../Page/飢餓遊戲.md "wikilink")》，比如說融入《[柳丁砸來亂](../Page/柳丁砸來亂.md "wikilink")》的元素。
  - 電影《[葉問](../Page/叶问_\(电影\).md "wikilink")》在[Youtube出現惡搞配音KUSO版](../Page/Youtube.md "wikilink")。\[2\]
  - 電影《[赤壁](../Page/赤壁_\(电影\).md "wikilink")》亦在[Youtube出現惡搞配音版的](../Page/Youtube.md "wikilink")《[三國志ㄤ賴](https://www.youtube.com/watch?v=A79MYk2lwCc)》，頗具台灣國語意味。
  - 《[仲夏夜之淫夢](../Page/仲夏夜之淫夢.md "wikilink")》
  - 德國電影《[帝國毀滅](../Page/帝國毀滅.md "wikilink")》中[希特勒痛斥下屬的橋段被網友加上惡搞版字幕](../Page/希特勒.md "wikilink")，這類改字幕影片多用於諷刺時事；而在[BiliBili等網站也有大陸網友將片中希特勒的聲音合成](../Page/BiliBili.md "wikilink")[Rap及歌曲](../Page/Rap.md "wikilink")，製作[音MAD惡搞](../Page/音MAD.md "wikilink")。\[3\]\[4\]\[5\]\[6\]
  - [打爆螢幕](https://www.youtube.com/watch?v=NR27mX7e-Zk)：一名男子看到螢幕中的鬼畫面而驚慌地用手打穿螢幕的影片，被網友加工處理成看到各種人物而作此動作。\[7\]\[8\]
  - [萬千師奶賀台慶](../Page/萬千師奶賀台慶.md "wikilink")
  - [眉精眼企宣傳片](../Page/眉精眼企.md "wikilink")
  - 亞視55周年台慶宣傳片
  - [Youtube影片](../Page/Youtube.md "wikilink")《[大便代表我的心](https://www.youtube.com/watch?v=yjup8dD9urc)》惡搞[鄧麗君的](../Page/鄧麗君.md "wikilink")《[月亮代表我的心](../Page/月亮代表我的心.md "wikilink")》。
  - [搜神傳宣傳片](../Page/搜神傳.md "wikilink")：模仿[無綫電視](../Page/無綫電視.md "wikilink")08北京奧運宣傳口號「我哋就係奧運」。[1](http://v.youku.com/v_show/id_XMzg0Mjc2MjQ=.html)
  - Scary
    movie系列，美國惡搞喜劇片。（譯為《[恐怖電影](../Page/恐怖電影.md "wikilink")》或《[驚聲尖笑](../Page/驚聲尖笑.md "wikilink")》）
  - 美國一系列電視卡通片往往以惡搞與諷刺為賣點，代表作有The
    Simpson's等（譯為[辛普森一家等](../Page/辛普森一家.md "wikilink")）。
  - 美國"Weird Al" Yankovic的歌曲。
  - [赤拉尼維的](../Page/赤拉尼維.md "wikilink")[Indian
    Triller被網友惡搞變成](../Page/赤拉尼維.md "wikilink")「幹你媽之歌」
  - 《[千本幼女](https://www.youtube.com/watch?v=K7E3Aw4brMM)》惡搞改編自《[千本櫻](../Page/千本櫻.md "wikilink")》
  - [周星馳電影](../Page/周星馳.md "wikilink")
      - 《[國產凌凌漆](../Page/國產凌凌漆.md "wikilink")》
      - 《[大內密探零零發](../Page/大內密探零零發.md "wikilink")》
      - 《[西遊記第一百零一回之月光寶盒](../Page/西遊記第一百零一回之月光寶盒.md "wikilink")》
      - 《[西遊記大結局之仙履奇緣](../Page/西遊記大結局之仙履奇緣.md "wikilink")》。
  - [改編詞](../Page/改編詞.md "wikilink")
      - [麥浚龍的歌曲](../Page/麥浚龍.md "wikilink")《羅生門》被惡搞成為[葉蘊儀的](../Page/葉蘊儀.md "wikilink")《中女羅生門》。
      - [周杰倫的歌曲](../Page/周杰倫.md "wikilink")《七里香》被惡搞成為[18禁的](../Page/18禁.md "wikilink")《[騎李湘](../Page/騎李湘.md "wikilink")》。
      - [蔡依林的歌曲](../Page/蔡依林.md "wikilink")《[舞孃](../Page/舞孃_\(歌曲\).md "wikilink")》被惡搞成為[救您-菜伊淋版的](../Page/救您-菜伊淋版.md "wikilink")《[舞釀](../Page/舞釀.md "wikilink")》。
      - [賀年歌曲](../Page/春节.md "wikilink")《財神到》被惡搞成諷刺香港[藝人](../Page/藝人.md "wikilink")[陳冠希的](../Page/陳冠希.md "wikilink")《奇拿到》。
      - 歌曲《沉默是金》被惡搞成諷刺[鄧光榮的](../Page/鄧光榮.md "wikilink")《明白是金》。
      - 《[U8柒](../Page/wikia:evchk:賭撚大碟.md "wikilink")》（又稱賭撚大碟），改自[陳奕迅的唱片](../Page/陳奕迅.md "wikilink")《U87》；《14歲》改自[許冠傑歌曲](../Page/許冠傑.md "wikilink")《有酒今朝醉》，藉以諷刺現時有關風化案的法例；《花生水起》則改自[農夫的](../Page/農夫.md "wikilink")《風生水起》。
      - [鄧麗欣的歌曲](../Page/鄧麗欣.md "wikilink")《[電燈膽](../Page/電燈膽.md "wikilink")》被網民惡搞成諷刺其[唱技的](../Page/歌唱#技巧.md "wikilink")《[杏加橙](../Page/wikia:evchk:杏加橙.md "wikilink")》。
          - 同一首歌又被改成《認真慘》及《杏加橙大平反》。
      - [傅穎的歌曲](../Page/傅穎.md "wikilink")《[花吃了這女孩](../Page/花吃了這女孩.md "wikilink")》被網民惡搞成諷刺其走音的[唱技的](../Page/歌唱#技巧.md "wikilink")《吃了屎的女孩》。
          - 同一首歌又被改成《吃了泥的女孩》及《我走了幾個音》。
      - [古巨基的一首歌](../Page/古巨基.md "wikilink")《錢錢錢錢》被網民惡搞成諷刺[雷頌德抄襲中外作曲家作品的](../Page/雷頌德.md "wikilink")《[抄抄抄抄](../Page/wikia:evchk:抄抄抄抄.md "wikilink")》。
          - 同一首歌又被改成諷刺[馬力言論的](../Page/馬力_\(立法會議員\).md "wikilink")《[扁扁扁扁](../Page/wikia:evchk:扁扁扁扁.md "wikilink")》。
      - [北京奧運主題曲](../Page/2008年夏季奥林匹克运动会.md "wikilink")《[北京歡迎你](../Page/北京歡迎你.md "wikilink")》被網民惡搞成諷刺[中國國家足球隊的](../Page/中國國家足球隊.md "wikilink")《[国足欢迎你](../Page/国足欢迎你.md "wikilink")》。
      - 《[崖上的波兒](../Page/崖上的波兒.md "wikilink")》主題曲被香港網民改編，被填入[成龍拍攝的霸王洗髮液廣告的台詞](../Page/wikia:evchk:成龍洗頭水潮文.md "wikilink")。[2](http://www.youtube.com/watch?v=bhMPkS8_EWM)
      - 香港[無綫電視翡翠台劇集](../Page/無綫電視翡翠台.md "wikilink")《[西遊記（貳）](../Page/西遊記（貳）.md "wikilink")》之中，曾經惡搞日本電視動畫《[櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")》的主題曲，改編成歌詞不同的粵語歌。
      - 香港藝人[許志安於](../Page/許志安.md "wikilink")2010年，在《[獅王適齒美牙刷廣告](../Page/wikia:evchk:獅王適齒美牙刷廣告惡搞熱潮.md "wikilink")》中的歌詞上不斷重覆「尖……尖尖尖……\[9\]」，但由於廣告極其[洗腦](../Page/洗腦.md "wikilink")，故慘被惡搞。\[10\]
  - [菲律賓人創意的](../Page/菲律賓.md "wikilink")[電影](../Page/電影.md "wikilink")《[蜘蛛俠百戰曱甴精](../Page/蜘蛛俠百戰曱甴精.md "wikilink")》
  - [中天電視台綜藝節目](../Page/中天電視台.md "wikilink")《[全民大悶鍋](../Page/全民大悶鍋.md "wikilink")》（現已改由第三代節目「[全民最大黨](../Page/全民最大黨.md "wikilink")」接班）的角色扮演與其小單元內容（包括：國台辦對記者粉瞭、張國志人市分析、[爺爺您回來了](../Page/爺爺您回來了.md "wikilink")、說文解字名人傳真、芒果亂報等等）
  - 中国经典历史[电视剧](../Page/电视剧.md "wikilink")《[亮剑](../Page/亮剑_\(2006年电视剧\).md "wikilink")》中，一支由[李云龙等人所擒獲的](../Page/李云龙_\(亮剑\).md "wikilink")[国军](../Page/中華民國國軍.md "wikilink")[暂七师](../Page/暂七师.md "wikilink")[军乐队演奏乐器的片段](../Page/军乐队.md "wikilink")，被[bilibili网友音效剪辑成演奏各种流行曲](../Page/bilibili.md "wikilink")，十分趣味，因而爆红。该军乐队也被戏称为「暂七师吹奏部」。\[11\]
  - [后舍男生](../Page/后舍男生.md "wikilink")，以夸张搞怪的动作假唱流行歌曲的组合。
  - 在[無綫電視節目](../Page/無綫電視.md "wikilink")《[殘酷一叮](../Page/殘酷一叮.md "wikilink")》表演過的爆炸猛男PHD。
  - 《[香港97](../Page/香港97_\(遊戲\).md "wikilink")》：一個[日本出品](../Page/日本.md "wikilink")，以[香港主權移交為主題的](../Page/香港主權移交.md "wikilink")[超級任天堂](../Page/超級任天堂.md "wikilink")[電視遊戲](../Page/電視遊戲.md "wikilink")。其拙劣的中文對白及超爛的畫面是其賣點。
  - 一套參考[樱花大战](../Page/樱花大战.md "wikilink")，諷刺[香港政治的](../Page/香港政治.md "wikilink")《[膠花大戰](../Page/膠花大戰.md "wikilink")》。
  - 《[無間道之CD-pro2](../Page/無間道之CD-pro2.md "wikilink")》：由[劉裕銘創作](../Page/劉裕銘.md "wikilink")，因買不到CD-pro2而惱羞成怒的作品，後來第二季由台灣連續劇改編。
  - 網民為抗議[中國](../Page/中國.md "wikilink")[江蘇省](../Page/江蘇省.md "wikilink")[宿遷市委書記](../Page/宿遷.md "wikilink")[仇和的](../Page/仇和.md "wikilink")《[禁桌令](../Page/禁桌令.md "wikilink")》而把它改寫成《[禁床令](../Page/禁床令.md "wikilink")》。
  - [胡戈的](../Page/胡戈.md "wikilink")《[一個饅頭引發的血案](../Page/一個饅頭引發的血案.md "wikilink")》：惡搞[陳凱歌的電影](../Page/陳凱歌.md "wikilink")《[無極](../Page/無極_\(電影\).md "wikilink")》。
  - [River的漫畫](../Page/River.md "wikilink")《543》 經常使用現實生活中的事件來作惡搞，發揮
  - 《[日本以外全部沉没](../Page/日本以外全部沉没.md "wikilink")》，《[日本沉没](../Page/日本沉没.md "wikilink")》的戏仿作品，讲述世界除日本外全部沉没，大量外国人涌入日本的故事。
  - [笑傲江湖二之東方不敗被網民惡搞成](../Page/笑傲江湖二之東方不敗.md "wikilink")[笑傲江湖之普羅米修斯復仇記](../Page/笑傲江湖之普羅米修斯復仇記.md "wikilink")。
  - 中国的旧版《[西游记](../Page/西游记_\(1982年电视剧\).md "wikilink")》电视剧中[狮驼岭的片段被](../Page/狮驼岭.md "wikilink")[BiliBili网民改编成唱歌型的](../Page/BiliBili.md "wikilink")[音MAD](../Page/音MAD.md "wikilink")，包括[小钻风巡山的一段](../Page/小钻风.md "wikilink")，恶搞《[宝可梦](../Page/宝可梦.md "wikilink")》片头曲以及《[恋愛サーキュレーション](../Page/恋愛サーキュレーション.md "wikilink")》等[二次元音乐](../Page/二次元.md "wikilink")，使人听了难以忘怀。\[12\]
  - [新世紀福音戰士中的](../Page/新世紀福音戰士.md "wikilink")[AT力場被惡搞成](../Page/新世紀福音戰士名詞列表_#AT力場.md "wikilink")「[李氏力場](../Page/李氏力場.md "wikilink")」(「李氏」指[香港首富](../Page/香港.md "wikilink")[李嘉誠](../Page/李嘉誠.md "wikilink")）以諷刺[香港天文台在發出風球信號時偏幫商界](../Page/香港天文台.md "wikilink")，罔顧市民安全[3](http://evchk.wikia.com/wiki/%E6%9D%8E%EF%BC%8B%EF%BC%B8#8.E8.99.9F.E9.A2.A8.E7.90.83.E7.B5.82.E7.B5.90.E8.80.85)，而[新世紀福音戰士本身也被惡搞成新世紀福佳戰士](../Page/新世紀福音戰士.md "wikilink")[4](http://forum2.hkgolden.com/view.asp?message=982568)、新世紀[高登戰士](../Page/高登討論區.md "wikilink")[5](http://forum2.hkgolden.com/view.asp?message=1020095)、新世紀思覺福音戰士[6](http://forum2.hkgolden.com/view.asp?message=908201)等。
  - [陳冠希於](../Page/陳冠希.md "wikilink")2008年2月4日發送各電視傳媒的英語道歉影片被大陸網民惡搞成諷刺[鍾欣桐對淫照的回應及](../Page/鍾欣桐.md "wikilink")[鄧竟成的處理淫照手法的](../Page/鄧竟成.md "wikilink")《陳冠希[切JJ以謝天下](../Page/去勢.md "wikilink")》。[7](https://web.archive.org/web/20080213203623/http://hk.news.yahoo.com/080210/60/2oll9.html)
  - [夏金城夏金城為港台拍攝著名MV](../Page/夏金城.md "wikilink")《雞與龜》，表達出型男的矛盾。[8](http://www.youtube.com/watch?v=X1jg2lFWluo)
  - 「[中国队勇夺世界杯](../Page/中国队勇夺世界杯.md "wikilink")」，恶搞[中國國家足球隊夺得](../Page/中國國家足球隊.md "wikilink")[德国世界杯冠军](../Page/2006年世界杯足球赛.md "wikilink")
  - [李毅大帝](../Page/李毅大帝.md "wikilink")
  - [超女冠军](../Page/2005年超级女声.md "wikilink")[李宇春被恶搞为](../Page/李宇春.md "wikilink")“[春哥](../Page/春哥.md "wikilink")”，并配有“春哥纯爷们，铁血真汉子”的调侃。其一广告代言被恶搞为“霸气爷们波”，翻唱[小红莓歌曲](../Page/小红莓.md "wikilink")《zombie》的视频被恶搞创作为《硬又黑》被网民观看超过一百万次，形象还被[石柱县龙沙镇计生办恶搞修改为](../Page/石柱县.md "wikilink")[计划生育宣传画](../Page/计划生育.md "wikilink")。\[13\]，另有“信春哥，死后原地满全状态复活”等段子的恶搞，某网络游戏公司更是提出“灭春鸽得永生”的口号。\[14\]此外网络中还有大量[Photoshop恶搞创作将李宇春头像和健美男性剪接的](../Page/Photoshop.md "wikilink")，对此李宇春所在公司[天娱传媒发声明](../Page/天娱传媒.md "wikilink")，“谴责恶意修改和制作公司艺人李宇春肖像的网站及个人。”\[15\]，而不少[李宇春的歌迷认为这些恶搞作品是由](../Page/李宇春.md "wikilink")[张靓颖的歌迷](../Page/张靓颖.md "wikilink")（[凉粉](../Page/凉粉.md "wikilink")）所作，是因为[张靓颖嫉妒](../Page/张靓颖.md "wikilink")[李宇春的人气和超女得票数所引起](../Page/李宇春.md "wikilink")，[李宇春的歌迷还制作了一些人身攻击](../Page/李宇春.md "wikilink")[张靓颖的虚假新闻在网上流传来反击对李宇春的恶搞](../Page/张靓颖.md "wikilink")。而实际上，对李宇春的恶搞是全体网民行为，玉米只是难以接受现实，只好自欺欺人，以攻击张靓颖来麻痹自己和对其他李宇春粉丝群体洗脑。李宇春对此表示“不排斥被叫“春哥”，因为这是一种娱乐”。\[16\]
  - [快女十强](../Page/2009快乐女声.md "wikilink")[曾轶可由于形象酷似](../Page/曾轶可.md "wikilink")[史泰龙](../Page/西尔维斯特·史泰龙.md "wikilink")、[胡歌和](../Page/胡歌.md "wikilink")[魂斗罗游戏中的人物](../Page/魂斗罗.md "wikilink")，被很多网民称为“曾哥”，对其恶搞的ps作品亦是在网络上广为流传。曾轶可对此表示“叫我曾哥？无所谓”。\[17\]
  - [十大神兽](../Page/十大神兽.md "wikilink")：当中有[草泥马等神兽](../Page/草泥马.md "wikilink")，全部为地球上不存在，虚构、恶搞出的动物。
  - 阿迪王：是[中国一个](../Page/中华人民共和国.md "wikilink")[运动用品的注册商标](../Page/运动用品.md "wikilink")，因其名称和[阿迪达斯相似](../Page/阿迪达斯.md "wikilink")，并且广告较为“[雷人](../Page/雷_\(网络用语\).md "wikilink")”\[18\]，广告代言人因有一句“阿迪王，I'm
    coming”的广告台词，所以该代言人被网友称为“艾抗米大神”。阿迪王的广告词“一切皆可改变”与[李宁的](../Page/李宁有限公司.md "wikilink")“一切皆有可能”也很像。后来由于网友讨厌阿迪王，不喜欢这个牌子，所以恶搞它。有人在网上发帖，正话反说，说世界上只有7双正品阿迪王，其他皆为仿制品（其中分高仿和低仿），低仿足以刺瞎人眼，很多人终身都在寻找阿迪王。还有网友给阿迪王和艾抗米在百度贴吧建了贴吧，在里面膜拜，还有著书立传，其实都是恶搞寻开心的。其中也有可能商家看到网友的恶搞，在推波助澜，提高自身知名度。
  - [金坷垃](../Page/金坷垃.md "wikilink")：是一个农业产品，其[广告](http://v.youku.com/v_show/id_XMjAyMDY4Mjg==.html?full=true)[雷人而受到各地网民恶搞相關](../Page/雷_\(网络用语\).md "wikilink")[音MAD影片](../Page/音MAD.md "wikilink")。
  - 以線上遊戲[魔獸世界：浩劫與重生為角色的](../Page/魔獸世界：浩劫與重生.md "wikilink")「魔獸世界：浩劫與富翁
    Cataclysm Millionaire」\[19\]
  - [伦敦奥运开幕式歌曲](../Page/2012年夏季奧林匹克運動會.md "wikilink")[Hey,
    Jude被网民曲解为](../Page/Hey,_Jude.md "wikilink")“嘿，[朱德](../Page/朱德.md "wikilink")”并加以恶搞，\[20\]网民戏称此歌“是1928年[毛泽东在](../Page/毛泽东.md "wikilink")[井冈山建立根据地后](../Page/井冈山.md "wikilink")，思念远在他乡的朱德所作”。
  - [金正恩的肖像被网民以PS恶搞成](../Page/金正恩.md "wikilink")《[七龙珠](../Page/七龙珠.md "wikilink")》中的人造人19号。
  - [金正恩被腾讯视频网民利用筷子兄弟歌曲](../Page/金正恩.md "wikilink")《[小苹果](../Page/小苹果_\(歌曲\).md "wikilink")》恶搞成《[金正恩版小苹果](../Page/金正恩版小苹果.md "wikilink")》。

### 政治

  - [北京奧運倒计时一周年主題曲](../Page/2008年夏季奥林匹克运动会.md "wikilink") [We Are
    Ready先後被惡搞成](../Page/We_Are_Ready.md "wikilink")《[2012 We Are
    Ready](../Page/We_Are_Ready#改編歌曲.md "wikilink")》和《[福佳，We Have
    Money！](../Page/福佳歌曲系列#.E7.AC.AC.E4.B8.89.E7.89.88.EF.BC.88.E7.A6.8F.E4.BD.B3.EF.BC.8CWe_have_money.EF.BC.81.EF.BC.89.md "wikilink")》，諷刺香港邁向普選舉步維艱。[9](http://plastichk.blogspot.com)
  - [中國的革命歌曲](../Page/中國.md "wikilink")《[社會主義好](../Page/社會主義好.md "wikilink")》被[死死團成員惡搞成為](../Page/死死團.md "wikilink")[死死主義好](../Page/死死主義好.md "wikilink")，但其實在電影《[榴槤飄飄](../Page/榴槤飄飄.md "wikilink")》裡，有一幕大-{伙}-兒高唱《社會主義好》的另一個版本《原始社會好》。
  - [香港主權移交十週年主題曲](../Page/香港主權移交十週年.md "wikilink")《[始終有你](../Page/始終有你.md "wikilink")》被網民惡搞成諷刺[馬力的歌曲](../Page/馬力_\(立法會議員\).md "wikilink")《[福佳](../Page/福佳歌曲系列.md "wikilink")》系列中的《[福佳始終有你](../Page/福佳歌曲系列#.E7.AC.AC.E4.B8.80.E7.89.88.EF.BC.88.E7.A6.8F.E4.BD.B3.E5.A7.8B.E7.B5.82.E6.9C.89.E4.BD.A0.EF.BC.89.md "wikilink")》和《[煲呔搵工靠你](../Page/福佳歌曲系列#.E7.AC.AC.E4.BA.8C.E7.89.88.EF.BC.88.E7.85.B2.E5.91.94.E6.90.B5.E5.B7.A5.E9.9D.A0.E4.BD.A0.EF.BC.89.md "wikilink")》，然後又再被親政府陣營二度惡搞成為《[整死香港有你](../Page/整死香港有你.md "wikilink")》。
      - 後來，同一首歌又先後被惡搞成抗議九巴大幅加價的《迫死九巴有你》[10](http://evchk.wikia.com/wiki/%E8%BF%AB%E6%AD%BB%E4%B9%9D%E5%B7%B4%E6%9C%89%E4%BD%A0)、諷刺[朱培慶的](../Page/朱培慶.md "wikilink")《出街始終有你》及諷刺[考評局的](../Page/香港考試及評核局.md "wikilink")[次文化創作](../Page/次文化.md "wikilink")《[屈機O嘴靠你](../Page/屈機.md "wikilink")》。
  - [2007年香港特別行政區行政長官選舉曾蔭權以](../Page/2007年香港特別行政區行政長官選舉.md "wikilink")「我會做好呢份工」為口號角逐連任，當時在網上及各媒體被廣泛流傳惡搞，並變化出數十種不同的口號，適用於各行各業。
      - [陳克勤](../Page/陳克勤.md "wikilink")：「我會試好呢個胸！」
      - 地盤佬：「我會鑽好呢個窿！」
      - 氣功師：「我會發好呢次功！」
      - 八達通公司：「我會整好八達通！」
      - 天文台：「我會測好呢個風！」
      - 整容醫生：「我會隆好呢個胸！」
      - 鐘錶師：「我會整好呢個鐘！」
      - 廚師：「我會爆好呢碟蔥！」
      - 麻將腳：「我會碰好呢隻東！」
      - 海關︰「我會睇實呢個zone」
      - 妓女︰「我會走多轉私鐘」
      - [陳曉東](../Page/陳曉東.md "wikilink")︰「我會跟翻[戴思聰](../Page/戴思聰.md "wikilink")」
      - 士兵︰「我會好好地進攻」
      - 食客︰「我會食埋呢碗檬」
      - [長毛](../Page/梁国雄.md "wikilink")︰「我會競選新界東」
      - 學生︰「我會讀好呢份書」
      - 足球員︰「我會打好d前鋒」
      - 打工仔辭職︰「我會辭左呢份工」
      - 窮鬼︰「我會去搵二叔公」

<!-- end list -->

  - [中华人民共和国](../Page/中华人民共和国.md "wikilink")[国歌](../Page/国歌.md "wikilink")《[义勇军进行曲](../Page/义勇军进行曲.md "wikilink")》被香港的[BT愛好者改為](../Page/BitTorrent.md "wikilink")《BT國歌》[11](http://www.kungfuboard.com/forums/showthread.php?t=14169)來抗議[香港海關拘捕](../Page/香港海關.md "wikilink")[蠱惑天王](../Page/陳乃明.md "wikilink")。
      - 同一首歌後來亦被中國網民改編成《買股暴富國際歌》，諷刺中國人瘋狂炒股的現象。
  - 在香港有一個以支持香港獨立為綱領的組織「[我是香港人連線](../Page/我是香港人連線.md "wikilink")」，普遍被認為是為了抗議北京當局抨擊「香港有人想搞港獨」而刻意惡搞出來的組織。
  - 《[鐵拳無敵孫中山](../Page/鐵拳無敵孫中山.md "wikilink")》：將民國創立血淚史幻化成為港漫式打鬥故事。
      - 《[我愛北京天安門](../Page/我愛北京天安門.md "wikilink")》：中國大陸[文革時期](../Page/文革.md "wikilink")[愛國歌曲](../Page/愛國.md "wikilink")，這首歌成為了《HK97》遊戲的主題曲兼背景音樂。
  - 一種為抗議[美國](../Page/美國.md "wikilink")[聖經地帶保守學院設立與](../Page/聖經地帶.md "wikilink")[進化論等同的](../Page/進化論.md "wikilink")[創造論課程而設立的虛構宗教](../Page/創造論.md "wikilink")[飞行面条怪](../Page/飞行面条怪.md "wikilink")。
  - 飞灾的《红星照耀府南河上》：05年度堪称经典的惡搞。
  - 面世不足一天的[2008年夏季奧林匹克運動會吉祥物：福娃](../Page/福娃.md "wikilink")，已經被香港、中國大陸和日本的網民惡搞成為[北奧戰隊](../Page/北奧戰隊.md "wikilink")（或稱[福娃戰隊](../Page/福娃戰隊.md "wikilink")）。
  - 香港流行組合[農夫的](../Page/農夫_\(樂隊\).md "wikilink")[舉高隻手](../Page/舉高隻手.md "wikilink")，被惡搞成《舉高隻手(民建聯版)》，為該政黨[2008年立法會選舉造勢](../Page/2008年立法會選舉.md "wikilink")。
  - 惡搞變相公投，全民起義\[21\]
  - 惡搞火影忍者—功能組別篇 \[22\]
  - 陳19，[大家樂集團董事局主席](../Page/大家樂.md "wikilink")[陳裕光](../Page/陳裕光.md "wikilink")。
  - 2014年，日本[兵庫縣前](../Page/兵庫縣.md "wikilink")[議員](../Page/議員.md "wikilink")[野野村龍太郎挪用公款被人發現因而嚎啕大哭的影片](../Page/野野村龍太郎.md "wikilink")\[23\]，被[NICONICO動畫網友剪輯並用其淒厲的哭聲製作成](../Page/NICONICO動畫.md "wikilink")[音MAD洗腦影片](../Page/音MAD.md "wikilink")\[24\]，引發迴響。
  - 香港2010年政治運動——「[起錨](../Page/起錨_\(口號\).md "wikilink")」，被惡搞成「[超錯](../Page/超錯.md "wikilink")」。其後[曾蔭權認為](../Page/曾蔭權.md "wikilink")「起錨」與「超錯」有九成相似，並且以「零五超錯，一二起錨」來回應\[25\]\[26\]
    。
  - 有中国大陆网民批评[中华人民共和国总理](../Page/中华人民共和国.md "wikilink")[温家宝言行不一](../Page/温家宝.md "wikilink")，并将其称之为“温影帝”和“宝宝影帝”。2010年，一本名叫《[中國影帝溫家寶](../Page/中国影帝温家宝.md "wikilink")》的书籍在香港出版发行。\[27\]\[28\]
  - [小兔子哐哐](../Page/小兔子哐哐.md "wikilink") 影片表現了中國人對社會問題的焦慮
    (影片已經被官方刪除，在中國境內的大部分網站已經無法看見)
  - 《[和谐中国](../Page/和谐中国.md "wikilink")》\[29\]
    是由中国大陆[异议人士](../Page/异议人士.md "wikilink")[尤里复制人X制作的](../Page/尤里复制人X.md "wikilink")[尤里的复仇](../Page/尤里的复仇.md "wikilink")[MOD](../Page/MOD.md "wikilink")，其中以中华[河蟹共和国的各类单位](../Page/河蟹.md "wikilink")（如[范跑跑](../Page/范跑跑.md "wikilink")、[城管](../Page/城管.md "wikilink")）来讥讽中国大陆社会现状。
  - 有網友取《[聯合報](../Page/聯合報.md "wikilink")》頭版改為「連核爆」，諷刺有爭議的臺灣[龍門核能發電廠](../Page/龍門核能發電廠.md "wikilink")（核四）。
  - 2013年，因為[洪仲丘事件](../Page/洪仲丘事件.md "wikilink")，而戲稱[中華民國國防部為](../Page/中華民國國防部.md "wikilink")「國防布」。\[30\]
  - 2014年，因為馬英九「鹿茸是鹿耳朵裡面的毛」的言論，而戲稱[馬英九為](../Page/馬英九.md "wikilink")「馬卡茸」。
  - 動畫[Code Geass
    反叛的魯路修將其實只有](../Page/Code_Geass_反叛的魯路修.md "wikilink")[化學學歷根本沒有任何經濟專業的](../Page/化學.md "wikilink")[日本](../Page/日本.md "wikilink")[經濟學家](../Page/經濟學家.md "wikilink")[大前研一提出的](../Page/大前研一.md "wikilink")[中華聯邦論惡搞成裡面的一個滅亡的莫名其妙的](../Page/中華聯邦論.md "wikilink")[紙老虎](../Page/紙老虎.md "wikilink")[超級大國](../Page/超級大國.md "wikilink")，也叫中華聯邦而且雖然名為聯邦其實卻是個[閹黨統治的國家](../Page/閹黨.md "wikilink")。
  - **「被OO」**：2015年發生的「[銅鑼灣書局事件](../Page/銅鑼灣書局事件.md "wikilink")」，負責人和幾個關係人忽然從[香港](../Page/香港.md "wikilink")、[泰國失蹤](../Page/泰國.md "wikilink")，數日後卻在中國媒體出現，當眾「認罪」，但香港人和一部分的大陸網友卻難以接受，就連國際輿論也不相信。於是「被OO」就被這樣被大陸媒體創造出來，OO可以是：失蹤、認罪、和諧(河蟹)、傳真、和平等。

### 其他

  - [袁绍](../Page/袁绍.md "wikilink")，字隆平，[江户时代官渡之战中](../Page/江户时代.md "wikilink")，被麦哲伦火烧秦皇陵，幸而研究出了[金坷垃](../Page/金坷垃.md "wikilink")，被[大木博士誉为拯救了](../Page/大木博士.md "wikilink")[木叶村的四皇之一](../Page/木叶村.md "wikilink")。
  - 城市論台：模仿自[城市論壇的情景](../Page/城市論壇.md "wikilink")，出現於以[維園阿伯為題材](../Page/維園阿伯.md "wikilink")[維他](../Page/維他奶.md "wikilink")[檸檬茶電視廣告中](../Page/檸檬茶.md "wikilink")。
  - [韓劇](../Page/韓國電視劇.md "wikilink")[總有艷陽天其中一集內容部分模仿自](../Page/總有艷陽天.md "wikilink")[胃仙U廣告的情節](../Page/胃仙U.md "wikilink")：多勞多得。
  - 中華王[先行者](../Page/先行者.md "wikilink")：將中國製造的機器人變成如同[新世紀福音戰士或](../Page/新世紀福音戰士.md "wikilink")[機動戰士GUNDAM的作品](../Page/機動戰士GUNDAM.md "wikilink")。
  - [臺鐵一號](../Page/臺鐵一號.md "wikilink")：將[臺鐵員工製造的](../Page/臺鐵.md "wikilink")[機器人形先行者化的產物](../Page/台鐵機器人.md "wikilink")。
  - [一騎當千](../Page/一騎當千.md "wikilink")：將[三國時代人物惡搞成](../Page/三國時代.md "wikilink")[萌化](../Page/萌.md "wikilink")[巨乳女高中生格鬥士的漫畫作品](../Page/巨乳.md "wikilink")。
  - [小泉麻將傳說](../Page/小泉麻將傳說.md "wikilink")：用麻將遊戲為背景，以日本內閣總理大臣[小泉純一郎](../Page/小泉純一郎.md "wikilink")、中國共產黨主席[毛澤東](../Page/毛澤東.md "wikilink")，中國共產黨總書記[胡錦濤](../Page/胡錦濤.md "wikilink")、中國給國務院總理[溫家寶以及](../Page/溫家寶.md "wikilink")[納粹德國的](../Page/納粹德國.md "wikilink")[希特勒元首和俄羅斯總統](../Page/希特勒.md "wikilink")[普丁等國家領導人為主要角色](../Page/普丁.md "wikilink")。
  - 用来恶搞创作的软件：[Photoshop在](../Page/Photoshop.md "wikilink")[网络语言中被简称为](../Page/网络语言.md "wikilink")**PS**，[高登討論區的](../Page/高登討論區.md "wikilink")[全職賭撚二世](../Page/wikia:evchk:全職賭撚二世.md "wikilink")、[-{曹宏威}-见证了PS改图之威力](../Page/wikia:evchk:曹宏威.md "wikilink")。
  - [改編港鐵海報](../Page/Wikia:evchk:改編港鐵海報.md "wikilink")
  - [中華民國國軍於](../Page/中華民國國軍.md "wikilink")[華視的](../Page/華視.md "wikilink")[莒光日教學節目](../Page/莒光日.md "wikilink")《[莒光園地](../Page/莒光園地.md "wikilink")》主題曲《青春不留白》改為「時薪八塊半」(義務役二兵月薪5890
    一兵6490 下士一萬)
  - [羅省/LA](../Page/洛杉磯.md "wikilink")：[羅湖鐵路站](../Page/羅湖站_\(香港\).md "wikilink")/[羅湖口岸](../Page/羅湖口岸.md "wikilink")，取名該處第一個字/羅省現稱的英文簡寫。
  - [玫瑰園](../Page/玫瑰園.md "wikilink")：[香港](../Page/香港.md "wikilink")[沙田馬場](../Page/沙田馬場.md "wikilink")
  - [本島不歡迎未達](../Page/台灣.md "wikilink")[放假標準之貧弱](../Page/颱風假.md "wikilink")[颱風](../Page/颱風.md "wikilink")\[31\]
  - [使命召唤恶搞版](../Page/使命召唤.md "wikilink")
  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")：經常出現惡整其他動漫或藝人的情節。(尤其是特殊刑事組登場之後)
  - [烘焙王](../Page/烘焙王.md "wikilink")：有惡搞[鼻毛拳王和](../Page/鼻毛拳王.md "wikilink")[摺紙戰士的橋段](../Page/摺紙戰士.md "wikilink")。
  - 千呼萬喚｢[屎](../Page/屎.md "wikilink")｣出來：形容[便秘](../Page/便秘.md "wikilink")。惡搞自[白居易的](../Page/白居易.md "wikilink")〈[琵琶行](../Page/琵琶行.md "wikilink")〉。
  - [銀魂](../Page/銀魂.md "wikilink")：幾乎所有的角色都是在惡搞日本[幕府末期的知名人物](../Page/幕府.md "wikilink")。

### 被動

有些事物本身原本有其他目的，但是被人拿來惡搞。如：

  - 美國怪人導演[艾德·伍德所導的](../Page/艾德·伍德.md "wikilink")《[外太空九號計劃](../Page/外太空九號計劃.md "wikilink")》（Plan
    9 from Outer
    Space）被評為影史上最爛的(科幻)電影，但卻擁有大量以戏谑觀點觀看的影迷而聞名於世，好萊塢名導演[提姆·波頓甚至因此為伍德](../Page/提姆·波頓.md "wikilink")‧艾德拍了以他為名的電影作為尊敬。
  - 日本大型公共網路討論板[2ch網友將觸犯風化罪的諧星](../Page/2ch.md "wikilink")[田代政灌票成為](../Page/田代政.md "wikilink")2001年[時代雜誌年度風雲人物](../Page/時代雜誌.md "wikilink")。
  - 在災難電影《[世界末日](../Page/世界末日.md "wikilink")》中，俄國太空站的老太空人在[NASA太空站幫助成員時說了](../Page/NASA.md "wikilink")「到處都是中華民國製造(MIT,
    Made In
    Taiwan，不是麻省理工學院(簡寫亦是MIT))的東西！」用一扳手敲打數下，機台恢復正常運作。(※「[MIT](../Page/MIT.md "wikilink")(Made
    In
    Taiwan)」係指在1970－1990[中華民國在當時的行政院官員](../Page/中華民國.md "wikilink")[孫運銓先生](../Page/孫運銓.md "wikilink")、[李國鼎先生等人的政策推動下](../Page/李國鼎.md "wikilink")，將台灣境內的產品由內需過剩轉為對外貿易，令中華民國的經濟逐漸好轉。從一般的農產品如[香蕉](../Page/香蕉.md "wikilink")、[鳳梨](../Page/鳳梨.md "wikilink")，到製[罐的](../Page/罐.md "wikilink")[蘆筍](../Page/蘆筍.md "wikilink")、[蘑菇](../Page/蘑菇.md "wikilink")、[筍子等](../Page/筍子.md "wikilink")、手工藝產品如[雨衣](../Page/雨衣.md "wikilink")、[傘](../Page/傘.md "wikilink")、[家具](../Page/家具.md "wikilink")、裝飾物到高科技產品如3C[零件](../Page/零件.md "wikilink")、各式[零組件](../Page/零組件.md "wikilink")、電腦產品等都有。)

## 反响

2006年8月10日，中國[光明日报社举办防止网上](../Page/光明日报.md "wikilink")「恶搞」成风专家座谈会，呼吁“旗帜鲜明地反对网上‘恶搞’红色经典，反对‘恶搞’优秀的民族传统文化”\[32\]。

叱咤903商業二台為了對抗被網民稱為「網絡23條」的《2011年版權（修訂）條例草案》，2012年5月6日下午2時至4時，網絡創作單位、填詞人周博賢、Ming仔、Eminleo、鍵盤戰線，聯同商業電台，於銅鑼灣時代廣場舉辦「903二次創作人音樂會」。邀請了多位香港著名歌手，唱出多首改編歌曲。

## 参考文献

## 外部連結

  -
  - [Can Dialectics Break Bricks? (1973) (主演 : 陳鴻烈) Situationist
    惡搞整部電影](http://www.ubu.com/film/vienet.html)

  - [林欣怡 :
    日常生活展演戰術：以萬德男孩為例](http://www.nhu.edu.tw/~society/e-j/81/81-08.htm)

  - [Peking Duck
    Soup](http://www.freakingnews.com/Peking-Duck-Soup-Pictures-71474.asp)

  - [陳雲--開卷看世界﹕快樂抗爭的靈感來源 ——
    弗雷勒的解放教育法](http://hk.news.yahoo.com/article/100710/4/j3kk.html)

  - [愛酷搜](http://i-kuso.com)

## 参见

  - [二次創作](../Page/二次創作.md "wikilink")、[戲仿](../Page/戲仿.md "wikilink")、[異軌](../Page/異軌.md "wikilink")
  - [港式幽默](../Page/港式幽默.md "wikilink")、[無厘頭文化](../Page/無厘頭文化.md "wikilink")
  - [愚人節](../Page/愚人節.md "wikilink")、[惡作劇](../Page/惡作劇.md "wikilink")
  - [綠壩娘](../Page/綠壩娘.md "wikilink")、[十大神獸](../Page/十大神獸.md "wikilink")
  - [偽基百科](../Page/偽基百科.md "wikilink")、[論文產生器](../Page/論文產生器.md "wikilink")

{{-}}

[Category:次文化](../Category/次文化.md "wikilink")
[Category:恶搞文化](../Category/恶搞文化.md "wikilink")
[Category:网络文化](../Category/网络文化.md "wikilink")
[Category:虛假傳播](../Category/虛假傳播.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.
32.