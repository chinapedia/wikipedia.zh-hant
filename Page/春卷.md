[Spring_rolls_on_sale.jpg](https://zh.wikipedia.org/wiki/File:Spring_rolls_on_sale.jpg "fig:Spring_rolls_on_sale.jpg")
[HK_Sai_Ying_Pun_308_Queen's_Road_West_寶蓮苑素食_Po_Lin_Yuen_Vegetarian_Restaurant_food_Mar-2014_spring_rolls.JPG](https://zh.wikipedia.org/wiki/File:HK_Sai_Ying_Pun_308_Queen's_Road_West_寶蓮苑素食_Po_Lin_Yuen_Vegetarian_Restaurant_food_Mar-2014_spring_rolls.JPG "fig:HK_Sai_Ying_Pun_308_Queen's_Road_West_寶蓮苑素食_Po_Lin_Yuen_Vegetarian_Restaurant_food_Mar-2014_spring_rolls.JPG")春卷\]\]
**春卷**（[臺灣常寫作](../Page/臺灣.md "wikilink")**春-{捲}-**）是[中国民间流行的传统小吃](../Page/中国.md "wikilink")，用薄麵皮，捲上餡料，經[油](../Page/油.md "wikilink")[炸的食品](../Page/炸.md "wikilink")。各地的餡料和大小不同，但基本是扁圓筒狀。以[中國為例](../Page/中國.md "wikilink")，一般人多用[蔬菜及](../Page/蔬菜.md "wikilink")[肉作餡料](../Page/肉.md "wikilink")，而北方部分地區則會改用[紅豆沙](../Page/豆沙.md "wikilink")。春卷亦是[滿漢全席中第二席中的一道菜式](../Page/滿漢全席.md "wikilink")。

## 來歷

於[立春當天](../Page/立春.md "wikilink")，人們用餅皮包裹[生菜](../Page/生菜.md "wikilink")，做成[春餅或春卷](../Page/春餅.md "wikilink")，大家分食，祈求身體健康，稱之為「咬春」，逐漸變成日常[食物](../Page/食物.md "wikilink")。

春卷是用干面皮包馅心，经煎、炸而成。它由立春之日食用春盘的习俗演变而来。

据晋周处《[风土记](../Page/风土记.md "wikilink")》载：“元旦造五辛盘”，就是将五种辛荤的蔬菜，供人们在春日食用，故又称为“春盘”。唐时，其内容有了发展变化，《四时宝镜》称：“立春日，食芦菔、春饼、生菜，号春盘。”后春卷样式越来越精致，在元代已有关于包馅油炸的春卷记载。

元代无名氏编撰的《居家必用事类全集》记载有“卷煎饼”：“摊薄煎饼，以胡桃仁、松仁、桃仁、榛仁、嫩莲肉、干柿、熟藕、银杏、熟栗、芭榄仁，以上除栗黄片切外皆细切，用蜜、糖霜和加碎羊肉、姜末、盐、葱调和作馅，卷入煎饼，油焯过。”这就是早期的春卷制法。明代食谱《易牙遗意》中也有类似的记载。

## 各式特色

### 閩台春卷

流行於[福建](../Page/福建.md "wikilink")、[潮汕](../Page/潮汕.md "wikilink")、[臺灣的春卷](../Page/臺灣.md "wikilink")，俗稱「[潤餅](../Page/潤餅.md "wikilink")」，又稱「[嫩餅菜](../Page/嫩餅菜.md "wikilink")」、「[冷春捲](../Page/冷春捲.md "wikilink")」（相對於炸春捲而言），是一種比春捲更為古老的吃法。「閩台春捲」並不像越式春捲那樣「油炸」才能食用，[福建](../Page/福建.md "wikilink")、[潮汕和](../Page/潮汕.md "wikilink")[臺灣一帶的家庭在](../Page/臺灣.md "wikilink")[新春](../Page/新春.md "wikilink")、[尾牙](../Page/尾牙.md "wikilink")（或以炸春捲、[掛包替代](../Page/割包.md "wikilink")）以及[清明節](../Page/清明節.md "wikilink")（[寒食節](../Page/寒食節.md "wikilink")）時會以潤餅皮（熟）[祭祖](../Page/祭祖.md "wikilink")，之後家族成員圍據一桌，桌上擺上各式煮熟青菜（包含[五辛在內](../Page/五辛.md "wikilink")）以及蛋皮、白肉、等等菜色，食用者挑選自己喜愛菜色加上[花生粉和](../Page/花生.md "wikilink")[糖粉以潤餅皮包裹後食用](../Page/糖.md "wikilink")，是屬於福建、潮汕、臺灣一帶家族聚會的重要飲食。

### 上海春卷

上海春卷是一款美味食谱，馅的主要原料是猪肉，香菇，白菜，酒，酱油，盐等，这道菜制作简单、营养丰富，鲜香可口。

### 四川春卷

四川[成都人口中的](../Page/成都.md "wikilink")“春卷”并非油炸的。它是用摊的薄面皮，在饭桌上卷入馅料（蔬菜为主，也有人吃肉的）后直接入口食用。吃法类似于[烤鸭卷饼](../Page/烤鸭.md "wikilink")。貴州[贵阳也有类似的菜品](../Page/贵阳.md "wikilink")，当地叫“丝娃娃”，所用面饼较小。

### 香港春卷

[香港春卷有時會寫作](../Page/香港.md "wikilink")「-{春捲}-」，是[茶樓](../Page/茶樓.md "wikilink")「[飲茶](../Page/飲茶.md "wikilink")」十分常見的[點心之一](../Page/點心.md "wikilink")，約四[吋長](../Page/英吋.md "wikilink")，[直徑一吋左右](../Page/直徑.md "wikilink")，以油炸方式烹調，外為[麵粉皮](../Page/麵粉.md "wikilink")，炸至金黃色，內裡餡料豐富，如[冬菇絲](../Page/冬菇.md "wikilink")、[豬肉絲](../Page/豬肉.md "wikilink")、[甘筍絲](../Page/甘筍.md "wikilink")、[竹筍絲](../Page/竹筍.md "wikilink")、[粉絲和](../Page/粉絲.md "wikilink")[木耳等](../Page/木耳.md "wikilink")，每碟數量大多為三件，吃時或會用[剪刀從中間剪成兩半](../Page/剪刀.md "wikilink")，然後蘸[喼汁食用](../Page/喼汁.md "wikilink")。香港亦有變種版本，[小販攤檔所售賣的春卷直徑長兩吋甚至更粗](../Page/小販.md "wikilink")，外皮比傳統春卷鬆脆，內裡的饀料更多，作為街頭小吃很受歡迎。

### 法式春卷

是[茶餐廳的產物](../Page/茶餐廳.md "wikilink")，於1992年在本土的一名水吧師傅創作。為了加強茶餐廳小食文化創作，這名師傅在2011年香港金茶王及金鴛鴦獲得雙優異獎。
製作法式春卷所需的材料有[麵包](../Page/麵包.md "wikilink")、[雞肉腸](../Page/雞肉腸.md "wikilink")、[雞蛋](../Page/雞蛋.md "wikilink")、[火腿](../Page/火腿.md "wikilink")、[沙律醬](../Page/沙律醬.md "wikilink")。

### 越南春卷

[越南春卷與中國春卷最大的不同在於餅皮並不用薄麵皮而是用稻米磨漿製成的米皮包裹](../Page/越南.md "wikilink")，餡料以[蝦肉](../Page/蝦.md "wikilink")、[豬肉和當地](../Page/豬肉.md "wikilink")[蔬菜為主](../Page/蔬菜.md "wikilink")，以米皮包裹成約十公分的小長筒形狀油炸，吃時用手以[生菜葉包裹春捲後沾](../Page/生菜.md "wikilink")[魚露後食用](../Page/魚露.md "wikilink")，是[越南相當盛行的一道料理](../Page/越南.md "wikilink")。

### 菲律賓春捲

菲律賓語發音是lumpia，是福建閩南話「潤餅」的音譯。由於菲律賓的華人多為福建閩南移民，華人移民影響菲律賓飲食文化，菲律賓春捲為與閩南一樣的潤餅捲。

### 荷蘭煎餅卷

荷語發音是loempia，極可能是閩南話「潤餅」的音譯。
相傳是明末時，西葡荷諸夷曾經造訪中國沿海嘗試貿易並引進數量不少的華工，將當地的食材作法帶回家試吃。
現今在荷蘭當地超市仍能看到loempia的冷凍速食包裝。

### 新馬薄餅

在[馬來西亞和](../Page/馬來西亞.md "wikilink")[新加坡春卷則稱為薄餅](../Page/新加坡.md "wikilink")（Popiah；po̍h-piáⁿ），普遍上分為炸薄餅和没有油炸的兩種,裡面餡料多是沙葛（[凉薯](../Page/凉薯.md "wikilink")）。

### 北美蛋捲

在[北美洲和部份](../Page/北美洲.md "wikilink")[歐洲的](../Page/歐洲.md "wikilink")[中餐館](../Page/中餐.md "wikilink")，“春捲”一詞專指素春捲，而肉餡春捲被稱為蛋捲（Egg
Roll）。雖然各家餐廳的春捲形制不一，一般來說同一家餐廳的蛋捲要大於春捲。蛋捲使用較厚的麵皮，內陷以豬絞肉為主，摻雜蔬菜、蝦肉、粉絲等普通春捲材料。下油鍋前，蛋捲外刷蛋漿，炸后呈現凹凸不平的外皮。和其他類型的春捲一樣，蛋捲佐紅色酸甜醬或杏醬進食。

## 外部連結

[Category:中國小吃](../Category/中國小吃.md "wikilink")
[Category:福建小吃](../Category/福建小吃.md "wikilink")
[Category:台灣小吃](../Category/台灣小吃.md "wikilink")
[Category:香港小吃](../Category/香港小吃.md "wikilink")
[Category:點心](../Category/點心.md "wikilink")
[Category:贺年食品](../Category/贺年食品.md "wikilink")
[Category:油炸食品](../Category/油炸食品.md "wikilink")