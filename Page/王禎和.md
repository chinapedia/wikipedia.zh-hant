**王禎和**（），是一位生於臺灣[花蓮的](../Page/花蓮縣.md "wikilink")[作家](../Page/作家.md "wikilink")。

## 生平概略

[民國](../Page/民國紀年.md "wikilink")39年
(1950年)，王禎和首次離開花蓮，民國47年前往[國立臺灣大學](../Page/國立臺灣大學.md "wikilink")[外文系就讀](../Page/外文系.md "wikilink")。不久，他首次發表作品《鬼。北風。人》於[白先勇所創辦的](../Page/白先勇.md "wikilink")《現代文學》[雜誌上](../Page/雜誌.md "wikilink")。

1961年，[張愛玲前往臺灣訪問](../Page/張愛玲.md "wikilink")，並與白先勇、[王文興](../Page/王文興.md "wikilink")、[歐陽子等作家對談](../Page/歐陽子.md "wikilink")；《鬼。北風。人》一文吸引張愛玲注意，她更隨王禎和前往[花蓮遊覽](../Page/花蓮縣.md "wikilink")，並在隨後鼓勵王禎和繼續創作；此後，王禎和更加專注於於小說創作，並致力於追求純粹的文學藝術。\[1\]

自1961年到1980年，他總共創作二百篇長篇及短篇小說，並另外寫作戲劇、著譯書目、影評與雜文。

1966年，任[亞洲航空職員](../Page/亞洲航空_\(臺灣\).md "wikilink")。1967年，轉任[國泰航空職員](../Page/國泰航空.md "wikilink")。1969年，任職[臺灣電視公司編審組](../Page/臺灣電視公司.md "wikilink")。1972年，前往[美國參加](../Page/美國.md "wikilink")[国际写作计划](../Page/国际写作计划.md "wikilink")。1973年，返回臺灣，任職臺視企劃組。1976年，於臺視官方期刊《[電視周刊](../Page/電視周刊.md "wikilink")》撰寫專欄〈走訪追問錄〉。1977年，任職臺視影片組。1979年，罹患[鼻咽癌](../Page/鼻咽癌.md "wikilink")，返回花蓮休養。

1990年9月3日，他因罹患[鼻咽癌逝世](../Page/鼻咽癌.md "wikilink")。\[2\]\[3\]\[4\]

## 代表作品

  - 《[嫁妝一牛車](../Page/嫁妝一牛車.md "wikilink")》。臺北市：金字塔，民國58年
  - 《寂寞紅》。臺北市 : 晨鐘, 民59
  - 《香格里拉》 : 王禎和自選集。臺北市 : 洪範, 民69
  - 《從簡愛出發》。臺北市 : 洪範, 民74
  - 《美人圖》。臺北市 : 洪範, 民74
  - 《人生歌王》。臺北市：聯合文學，民國76年
  - 《嫁粧一牛車》。臺北市 : 洪範,民82
  - 《玫瑰玫瑰我愛你》。臺北市 : 洪範, 民83
  - 《兩地相思》。臺北市 : 聯合文學出版 民87

## 參見

  - [20世紀中文小說100強](../Page/20世紀中文小說100強.md "wikilink")

## 注釋及參考

## 外部連結

  - [王禎和先生手稿資料展 - 國立臺灣大學圖書館 - National Taiwan
    University](http://www.lib.ntu.edu.tw/events/manuscript/wch/)
  - [臺灣大學圖書館專題書單 - 當代中文作家系列 -
    王禎和](http://tulips.ntu.edu.tw/screens/c_wch.html)

[Category:臺灣男性小說家](../Category/臺灣男性小說家.md "wikilink")
[Category:國立臺灣大學文學院校友](../Category/國立臺灣大學文學院校友.md "wikilink")
[Category:花蓮人](../Category/花蓮人.md "wikilink")
[Z禎](../Category/王姓.md "wikilink")
[Category:罹患鼻咽癌逝世者](../Category/罹患鼻咽癌逝世者.md "wikilink")
[Category:国际写作计划校友](../Category/国际写作计划校友.md "wikilink")
[\*](../Category/王禎和.md "wikilink")

1.
2.
3.
4.