**谷山－志村定理**（）建立了[椭圆曲线](../Page/椭圆曲线.md "wikilink")（[代数几何的对象](../Page/代数几何.md "wikilink")）和[模形式](../Page/模形式.md "wikilink")（[数论中用到的某种周期性](../Page/数论.md "wikilink")[全纯函数](../Page/全纯函数.md "wikilink")）之间的重要联系。

定理的证明由英國數學家[安德鲁·怀尔斯](../Page/安德鲁·怀尔斯.md "wikilink")（Andrew John
Wiles）、[理查·泰勒](../Page/理查·泰勒_\(數學家\).md "wikilink")（）、法國數學家、美國數學家和所完成。

若*p*是一个[质数而](../Page/质数.md "wikilink")*E*是一个**Q**（[有理数](../Page/有理数.md "wikilink")[域](../Page/域.md "wikilink")）上的一个椭圆曲线，我们可以简化定义*E*的方程[模](../Page/模.md "wikilink")*p*；除了有限个*p*值，我们会得到有*n*<sub>*p*</sub>个元素的[有限域](../Page/有限域.md "wikilink")**F**<sub>*p*</sub>上的一个椭圆曲线。然后考虑如下序列

  -
    *a*<sub>*p*</sub> = *n*<sub>*p*</sub> − *p*,

这是椭圆曲线*E*的重要的不变量。从[傅里叶变换](../Page/傅里叶变换.md "wikilink")，每个模形式也会产生一个数列。一个其序列和从模形式得到的序列相同的椭圆曲线叫做*模的*。谷山-志村定理说：

  -
    “所有**Q**上的椭圆曲线是模的。”

通俗而言，椭圆方程与模形式是一一对应的，每个椭圆方程都可以用模形式表达出来，而费马大定理和谷山－志村猜想是共存关系。如果费马大定理成立则谷山-志村猜想也成立，反之亦然。

## 歷史

1955年9月，日本數學家[谷山丰提出](../Page/谷山丰.md "wikilink")[猜想](../Page/猜想.md "wikilink")。到1957年为止，他和[志村五郎一起改进了](../Page/志村五郎.md "wikilink")[严格性](../Page/严格性.md "wikilink")。谷山于1958年[自杀身亡](../Page/自杀.md "wikilink")。在1960年代，它和统一数学中的猜想[朗兰兹纲领联系了起来](../Page/朗兰兹纲领.md "wikilink")，并是关键的组成部分。猜想由[安德烈·韦伊于](../Page/安德烈·韦伊.md "wikilink")1970年代重新提起并得到推广，韦伊的名字有一段时间和它联系在一起。尽管有明显的用处，这个问题的深度在后来的发展之前并未被人们所感觉到。

在1980年代当德國數學家提出**谷山－志村猜想**（那时还是猜想）應該蕴含[费马最后定理](../Page/费马最后定理.md "wikilink")（即费马大定理）的时候，它吸引到了不少注意力。他试图通过表明费马大定理的任何反例会导致一个非模的椭圆曲线来做到这一点。[肯尼斯·阿蘭·黎貝后来证明了这一结果](../Page/肯尼斯·阿蘭·黎貝.md "wikilink")（）。在1995年，安德鲁·怀尔斯和理查·泰勒证明了谷山－志村定理的一个特殊情况（[半稳定椭圆曲线的情况](../Page/半稳定椭圆曲线.md "wikilink")），这个特殊情况足以证明费马大定理。

完整的证明最后于1999年由布勒伊、康萊德、戴蒙德和泰勒作出，他们在怀尔斯的基础上，一块一块的逐步证明剩下的情况直到全部完成。

数论中类似于费马最后定理的几个定理可以从谷山－志村定理得到。例如：没有立方可以写成两个[互质n次幂的和](../Page/互质.md "wikilink")，*n* ≥ 3。（*n* = 3的情况已为[欧拉所知](../Page/欧拉.md "wikilink")）

在1996年3月，怀尔斯和加拿大數學家[罗伯特·朗兰兹](../Page/罗伯特·朗兰兹.md "wikilink")（）分享了[沃尔夫数学奖](../Page/沃尔夫数学奖.md "wikilink")。虽然他们都没有完成给予他们这个成就的定理的完整形式，他们还是被认为对最终完成的证明有着决定性影响。

## 参考

  - Henri Darmon: *[A Proof of the Full Shimura-Taniyama-Weil Conjecture
    Is Announced](http://www.ams.org/notices/199911/comm-darmon.pdf)*,
    Notices of the American Mathematical Society, Vol. 46 (1999), No.
    11. Contains a gentle introduction to the theorem and an outline of
    the proof.
  - Brian Conrad, Fred Diamond, Richard Taylor: [*Modularity of certain
    potentially Barsotti-Tate Galois
    representations*](https://web.archive.org/web/20060318075859/http://abel.math.harvard.edu/~rtaylor/cdt.dvi),
    Journal of the American Mathematical Society 12 (1999), pp. 521–567.
    Contains the proof.
  - [A Bluffer's Guide to Fermat's Last
    Theorem](http://math.stanford.edu/~lekheng/flt/)--原始文獻集

[Category:代数曲线](../Category/代数曲线.md "wikilink")
[Category:黎曼曲面](../Category/黎曼曲面.md "wikilink")
[Category:模形式](../Category/模形式.md "wikilink")
[G](../Category/数学定理.md "wikilink")