**大脑皮質**（），又称为**[大脑灰質](../Page/大脑灰質.md "wikilink")**，或简称为**皮质**或**皮层**，是[大脑的一个解剖结构](../Page/大脑.md "wikilink")。大脑皮层是[端脑的一部分](../Page/端脑.md "wikilink")，属于[脑和整个](../Page/脑.md "wikilink")[神经系统演化史上最为晚出现](../Page/神经系统.md "wikilink")、功能上最为高階的一部分。

## 大脑皮层的结构

[thumb](../Page/image:Gray754.png.md "wikilink")
高级动物的大脑皮层是包裹在大脑外侧的连通皮状结构。**沟**和**回**是皮层最为显著的两个解剖特征。皮层有些区域向内凹陷，形成称为“沟”的解剖结构。沟之外向外凸出的区域称为“腦回”，越高等的動物腦回皺褶就越多，大腦皮質面積也越大。成年人类的皮质如果全部展平，面积大约有两张报纸展开这么大。大脑皮质的厚度约为2-4毫米。

皮层是由神经细胞组成，包括[神经元和](../Page/神经元.md "wikilink")[星形膠質細胞等其它支持细胞](../Page/星形膠質細胞.md "wikilink")。大多数神经元属于锥体细胞形态，其余形态有篮状细胞等。成年人类大脑皮层所含的神经元的数量大约在10<sup>10</sup>量级。皮层神经元之间形成大量的[突触连接](../Page/突触.md "wikilink")。这些突触连接包括分区内的连接、分区之间的侧向连接和半球之间通过[胼胝体的连接](../Page/胼胝体.md "wikilink")、以及和[脑的其它部分](../Page/脑.md "wikilink")（例如[丘脑](../Page/丘脑.md "wikilink")、[基底核等](../Page/基底核.md "wikilink")）形成的连接。

在高级哺乳类动物中，大脑皮层存在分层结构。在新皮层，由外向内依次为1到6层。不同的层的细胞类型和分布，以及投射模式不同。

## 大脑皮层的分区

\[\[image:Brain diagram ja.svg|thumb|255px|外側大脳皮层的解剖的区分

|                                |                                |                                |                                |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [额叶](../Page/额叶.md "wikilink") | [顶叶](../Page/顶叶.md "wikilink") | [颞叶](../Page/颞叶.md "wikilink") | [枕叶](../Page/枕叶.md "wikilink") |

\]\] 根据空间位置，大脑皮层被分为几个叶。每个叶是空间上连通的一部分皮层。以下列出的是这些分区的名称及目前学术界所认为的主要功能

  - **[额叶](../Page/额叶.md "wikilink")**：高级认知功能，比如[学习](../Page/学习.md "wikilink")、[语言](../Page/语言.md "wikilink")、[决策](../Page/决策.md "wikilink")、[抽象思维](../Page/抽象.md "wikilink")、[情绪等](../Page/情绪.md "wikilink")，自主运动的控制（参见[运动皮层](../Page/运动皮层.md "wikilink")）。
  - **[顶叶](../Page/顶叶.md "wikilink")**：躯体感觉（参见[体感皮层](../Page/体感皮层.md "wikilink")），空间信息处理，视觉信息和体感信息的整合。
  - **[颞叶](../Page/颞叶.md "wikilink")**：[听觉](../Page/听觉.md "wikilink")（参见[听觉皮层](../Page/听觉皮层.md "wikilink")），嗅覺，高级视觉功能（例如物体识别），分辨左右，[长期记忆](../Page/长期记忆.md "wikilink")。
  - **[枕叶](../Page/枕叶.md "wikilink")**：[视觉处理](../Page/视觉.md "wikilink")（参见[视觉皮层](../Page/视觉皮层.md "wikilink")）
  - **[边缘系统](../Page/边缘系统.md "wikilink")**：[奖励学习和](../Page/奖励学习.md "wikilink")[情感处理](../Page/情感.md "wikilink")。

高階[哺乳类动物大脑皮层的另一个重要特征是功能分区](../Page/哺乳类.md "wikilink")。[布羅德曼依据大脑皮层的细胞架构特征将其分区并编号](../Page/布羅德曼.md "wikilink")，以他名字命名的分区系统今日仍被广泛使用。

根据进化史，大脑皮层分为[新皮层](../Page/新皮质.md "wikilink")（Neocortex）和[古皮层](../Page/古皮質.md "wikilink")。古皮层主要包括[嗅觉皮层和](../Page/嗅觉皮层.md "wikilink")[边缘系统](../Page/边缘系统.md "wikilink")。

### 優勢大腦

大脑皮层分为左右两个半球。两侧半球在功能上有分化。在大多数人类中，[语言](../Page/语言.md "wikilink")、意念、邏輯、理性主要由[左脑掌管](../Page/左脑.md "wikilink")，而[右脑负责](../Page/右脑.md "wikilink")[形象思维和](../Page/形象思维.md "wikilink")[情感等](../Page/情感.md "wikilink")。例如：科學家比較理性、有邏輯，所以左腦發達；藝術家重感性、具創造力、擅長空間和物體形狀認知，故右腦發達（以上說法並不被當代神經科學、心理學界支持。多數功能實際上需要左右腦同時參與）。连接两侧半球之间的大量[轴突形成一个称为](../Page/轴突.md "wikilink")[胼胝体的解剖结构](../Page/胼胝体.md "wikilink")。在身體下方相對應腦的主管區域越上方、越精密的動作器官佔的區域越大。

## 参考文献

## 参见

  - [Brodmann分区系统](../Page/Brodmann分区系统.md "wikilink")

{{-}}

[Category:大脑](../Category/大脑.md "wikilink")
[大脑皮层](../Category/大脑皮层.md "wikilink")
[C](../Category/神經科學.md "wikilink")
[C](../Category/神经解剖学.md "wikilink")
[Category:心理学](../Category/心理学.md "wikilink")