**奥利佛·克倫威爾**（Oliver
Cromwell，），[英國](../Page/英國.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")、[國會](../Page/國會.md "wikilink")[議員](../Page/議員.md "wikilink")、[獨裁者](../Page/獨裁者.md "wikilink")，在[英国内战中击败了](../Page/英国内战.md "wikilink")，1649年[斬殺了](../Page/斬殺.md "wikilink")[查理一世後](../Page/查理一世_\(英格蘭\).md "wikilink")，克伦威尔廢除英格蘭的[君主制](../Page/君主制.md "wikilink")，並征服[苏格兰](../Page/苏格兰.md "wikilink")、[爱尔兰](../Page/爱尔兰.md "wikilink")，在1653年至1658年期間出任[英格蘭-蘇格蘭-愛爾蘭聯邦之](../Page/英格蘭聯邦-護國公時期.md "wikilink")[护国公](../Page/护国公.md "wikilink")。

克倫威爾是[英国历史中最具争议性的人物之一](../Page/英国历史.md "wikilink")。一些历史学家，如[大卫·休谟](../Page/大卫·休谟.md "wikilink")、指责他是“[大逆不道](../Page/大逆不道.md "wikilink")”的人物。\[1\]\[2\]他在[苏格兰](../Page/苏格兰.md "wikilink")、[爱尔兰残害](../Page/爱尔兰.md "wikilink")[天主教信徒的行为](../Page/天主教.md "wikilink")，被人批评是与[种族灭绝无异的行为](../Page/种族灭绝.md "wikilink")。\[3\]\[4\]另一方面，一些学者视他为英雄，如[托馬斯·卡萊爾](../Page/托馬斯·卡萊爾.md "wikilink")、[塞繆爾·羅森·加德納](../Page/塞繆爾·羅森·加德納.md "wikilink")。在2002年由[英國廣播公司](../Page/英國廣播公司.md "wikilink")（BBC）发起的《[最伟大的100名英国人](../Page/最伟大的100名英国人.md "wikilink")》票選中，克伦威尔名列第十。\[5\]

## 生平

1599年4月25日克倫威爾生於[亨廷登](../Page/亨廷登.md "wikilink")，\[6\]
父母分別是羅伯特·克倫威爾（Robert
Cromwell）和伊麗莎白·斯圖亞特（Elizabeth
Steward）。他是[托馬斯·克倫威爾的姐姐凱瑟琳](../Page/托馬斯·克倫威爾.md "wikilink")·克倫威爾（Katherine
Cromwell）的後裔，[宗教改革期間克倫威爾家族攫取了大量的財富](../Page/宗教改革.md "wikilink")。凱瑟琳與威爾士軍人[理查德·威廉姆斯](../Page/理查德·威廉姆斯_\(克倫威爾\).md "wikilink")（Richard
Williams）結婚，從這一代向下是[理查德和](../Page/理查德·威廉姆斯_\(克倫威爾\).md "wikilink")[亨利·威廉姆斯](../Page/亨利·威廉姆斯_\(克倫威爾\).md "wikilink")，\[7\]
然後就是奧利弗的父親羅伯特（約1560–1617），其父和其母伊麗莎白（約1564–1654）很可能是在1591年結婚。他們十個孩子，奧立佛·克倫威爾是老五，也是唯一一個未夭折的男孩。\[8\]

克伦威尔出身中层[士绅](../Page/士绅.md "wikilink")，祖父亨利是亨廷登郡內最富有的兩個地主之一，其父自己身分雖然還不差，卻只繼承了亨廷登的一座房子和少量土地。其地產年收益為300[英鎊](../Page/英鎊.md "wikilink")，逼近當時貴族收入的底線。\[9\]
1654年9月4日克倫威爾說“我生為[紳士](../Page/紳士.md "wikilink")，如今卻身分低微”。\[10\]

1599年4月29日在[聖約翰教堂](../Page/聖約翰教堂.md "wikilink")[受洗](../Page/受洗.md "wikilink")，\[11\]
後在[亨廷登語法學校讀書](../Page/亨廷登語法學校.md "wikilink")。之後進入[劍橋大學雪梨學院學習](../Page/劍橋大學雪梨學院.md "wikilink")。1617年其父去世後，克倫威爾沒有畢業，就中輟離校而去\[12\]
，克倫威爾也有可能回過家，因為當時他母親新寡，而七個姐妹尚未結婚，需要克倫威爾的幫助\[13\]，有一些早期傳記作家宣稱克倫威爾之後進入了[林肯律師學院](../Page/林肯律師學院.md "wikilink")，雖然他的祖父、父親及兒子都曾在此學院讀書，但該學院的檔案中並無克倫威爾的入學記錄。他可能確實是去過倫敦的一所[律師學院讀書](../Page/律師學院.md "wikilink")，但未必是林肯律師學院。\[14\]

在继承叔父遗产之前，他的生活都和普通农民一样。当时[宗教改革运动正盛](../Page/宗教改革.md "wikilink")，克伦威尔也受此影响，成为了[清教徒](../Page/清教徒.md "wikilink")。他是一个虔诚的独立清教徒，深信[上帝指引他走向胜利](../Page/上帝.md "wikilink")。雖然他从未表明自己的宗教立场，但他强烈赞成容忍各种[新教教派](../Page/新教.md "wikilink")。\[15\]

他曾在1628年的国会担任[亨廷登议员](../Page/亨廷登_\(英国国会选区\).md "wikilink")，在1640年的[短期国会和](../Page/短期国会.md "wikilink")[长期国会](../Page/长期国会.md "wikilink")（Long
Parliament）担任[剑桥郡议员](../Page/剑桥郡_\(英国国会选区\).md "wikilink")，才开始声名鹊起。在[英国内战中](../Page/英国内战.md "wikilink")，他支持[圆颅党](../Page/圆颅党.md "wikilink")，并成为了党内重要军事领袖。外号「老铁骑军」的他，很快地由一支骑兵部队的指挥官，晋升为总司令。1649年，他与其他圆颅党领袖一起，做出[处死](../Page/处死.md "wikilink")[查理一世的决定](../Page/查理一世.md "wikilink")，他大權在握，罷黜了反對派的議員，使國會變成[残缺议会](../Page/残缺议会.md "wikilink")。在1649年至1650年间，他征服爱尔兰，在1650年至1651年间，他又率领军队入侵苏格兰，1653年4月20日，他用武力解散残缺议会，建立了成员完全由他选择的新议会——[小议会](../Page/小议会.md "wikilink")。他推行了积极有效的内外政策，在相當程度上塑造了英国的未来。

1653年12月16日起，克倫威爾自命為[大不列顛的](../Page/大不列顛.md "wikilink")[护国公](../Page/护国公.md "wikilink")，1658年病死，葬于[西敏寺](../Page/西敏寺.md "wikilink")，並把护国公的[寶座傳位給他的兒子](../Page/寶座.md "wikilink")[理查·克倫威爾](../Page/理查·克倫威爾.md "wikilink")。但是[理查·克倫威爾並無其父的政治與軍事實力](../Page/理查·克倫威爾.md "wikilink")，立刻被迫流亡，[斯图亚特王朝](../Page/斯图亚特王朝.md "wikilink")[查理二世](../Page/歡樂王查理.md "wikilink")[复辟](../Page/复辟.md "wikilink")。

## 身后

1658年克伦威尔去世，由其子[理查·克倫威爾繼位為](../Page/理查·克倫威爾.md "wikilink")[護國公](../Page/護國公.md "wikilink")。理查無力鎮壓反叛的[貴族與](../Page/貴族.md "wikilink")[軍官](../Page/軍官.md "wikilink")，英国政坛混乱，國會聲明由[君主制](../Page/君主制.md "wikilink")[復辟](../Page/復辟.md "wikilink")，[理查](../Page/理查.md "wikilink")[流亡](../Page/流亡.md "wikilink")[法國](../Page/法國.md "wikilink")，查理二世因此得以返回英国。

[斯圖亞特王朝复辟](../Page/斯圖亞特王朝.md "wikilink")，[查理二世即位](../Page/查理二世.md "wikilink")，下令把克伦威尔的遗体从[西敏寺的墓地掘出以](../Page/西敏寺.md "wikilink")[英式車裂之刑](../Page/英式車裂.md "wikilink")[戮屍](../Page/戮屍.md "wikilink")：首先命人拖着它穿过[伦敦城](../Page/伦敦城.md "wikilink")，然后送到了日常处决普通犯人的[泰伯恩法场](../Page/泰伯恩法场.md "wikilink")，在那里被吊上绞刑架，与其他两具尸体一同示众一整天。执刑者把吊尸体的绳子砍断后，就把克伦威尔的头颅砍了下来，并把那颗头颅挑在长矛上游街示众，而尸身则被扔进了坑里草草掩埋。伟大的护国公自此和前国王一样身首分离，再难团聚。克伦威尔的头颅被挑在长矛上四处游街。最后，克伦威尔的脑袋被一根长钉子钉在了[西敏寺的屋顶上](../Page/西敏寺.md "wikilink")，在上面一待就是25年。后来一场风暴把它刮了下来，这时它已经完全干燥，胡子仍然长在下颌上。后来克伦威尔的[首級不知怎么流落民间](../Page/首級.md "wikilink")，竟成了私人收藏品，并且一度被颇懂生意经的英国人拿去當作古董，辗转贩卖。18世纪后期，[罗塞尔家族开办了一个小小的博物馆](../Page/罗塞尔家族.md "wikilink")，克伦威尔的头颅成了这个小小博物馆的中心展品。14年后，一个名叫[约西亚·亨利·威尔金森的人花](../Page/约西亚·亨利·威尔金森.md "wikilink")230[英镑买下了这颗头颅](../Page/英镑.md "wikilink")，并开始抱着它到处炫耀。当时看过这个人头的玛利亚·[艾吉沃斯写道](../Page/艾吉沃斯.md "wikilink")：“[威尔金森先生是这个人头现在的持有人](../Page/威尔金森.md "wikilink")，他一直以此为荣。那是个极可怕的人头，头上面盖着乾透的黄皮，就跟别的[木乃伊一样](../Page/木乃伊.md "wikilink")，上面还有栗色的头发，眉毛和胡须都保存得很好，那颗头颅依然插在一个铁杆上。”[艾吉沃斯还描述说](../Page/艾吉沃斯.md "wikilink")，客人们可以排着队轮流到窗边抱一抱那个东西。人们可以在它的脑后部看到笨手笨脚的[刽子手留下的](../Page/刽子手.md "wikilink")“斧砍的痕迹”，“有一只耳朵已经按要求砍掉了，他头颅左眼上面还有克伦威尔的独特标志“一个硕大[肉疣的痕迹](../Page/肉疣.md "wikilink")”。”直到多年后的1960年，克伦威尔的头颅才离开了巡回展览，由克伦威尔的母校[剑桥大学悉尼学院收回](../Page/剑桥大学悉尼学院.md "wikilink")，葬在[牛津的一座小教堂旁](../Page/牛津.md "wikilink")，這流浪的灵魂总算有了栖身之地。

## 參考文獻

## 外部链接

  - [The Perfect Politician: Or, a Full View of the Life and Actions
    (Military and Civil) of O.
    Cromwell, 1660](http://archive.org/details/ThePerfectPoliticianOrAFullViewOfTheLifeAndActionsmilitaryAnd)
    A digitized copy by John Geraghty

  - [Well established informational website about Oliver
    Cromwell.](http://www.olivercromwell.com/)

  - [The Oliver Cromwell Project at the University of
    Cambridge](https://web.archive.org/web/20140414223411/http://www.cromwell.hist.cam.ac.uk/)

  - [Oliver Cromwell World History
    Database](http://www.malc.eu/history/Cromwell-Oliver-England.biog.html)

  - [Oliver Cromwell and the English Revolution—In Honor of Christopher
    Hill 1912–2003](http://www.icl-fi.org/english/wv/918/cromwell.html)

  - [The Cromwell Association](http://www.olivercromwell.org/)

  - [The Cromwell Museum in
    Huntingdon](https://web.archive.org/web/20061005001747/http://www.cambridgeshire.gov.uk/leisure/museums/cromwell/)

  - [Chronology of Oliver Cromwell World History
    Database](http://www.badley.info/history/Cromwell-Oliver-England.biog.html)

  - [Biography at the British Civil Wars & Commonwealth
    website](http://www.british-civil-wars.co.uk/biog/oliver-cromwell.htm)

  -
  -
  -
  - Vallely, Paul. [The Big Question: Was Cromwell a revolutionary hero
    or a genocidal war
    criminal?](http://www.independent.co.uk/news/uk/this-britain/the-big-question-was-cromwell-a-revolutionary-hero-or-a-genocidal-war-criminal-917996.html),
    [The Independent](../Page/The_Independent.md "wikilink") 4 September
    2008.

  - [The Cromwellian Catastrophe in Ireland:an Historiographical
    Analysis (an overview of writings/writers on the subject by Jameel
    Hampton pub. Gateway An Academic Journal on the Web: Spring 2003
    PDF)](https://web.archive.org/web/20071128180651/http://grad.usask.ca/gateway/art_Hampton_spr_03.pdf)

  - [An Interview with a conservator from the Library of Congress who
    conserved a document that bears the signature of Oliver
    Cromwell](http://blogs.loc.gov/law/2011/12/an-interview-with-yasmeen-khan-senior-rare-book-conservator-at-the-library-of-congress/)

  - [Cromwell (1970)](http://www.imdb.com/title/tt0065593/) at
    [IMDb](../Page/Internet_Movie_Database.md "wikilink")

{{-}}

[Category:英国国家元首](../Category/英国国家元首.md "wikilink")
[Category:英国国会议员](../Category/英国国会议员.md "wikilink")
[C](../Category/英格蘭加爾文主義者.md "wikilink")
[C](../Category/英國軍事人物.md "wikilink")
[C](../Category/剑桥大学悉尼·萨塞克斯学院校友.md "wikilink")
[Category:墓穴遭搗毀者](../Category/墓穴遭搗毀者.md "wikilink")
[Category:劍橋郡人](../Category/劍橋郡人.md "wikilink")

1.
2.
3.  Brendam O'Leary and John McGarry, *Regulating nations and ethnic
    communities*, p. 248, Breton Albert, 1995. *Nationalism and
    Rationality*, Cambridge University Press.
4.
5.
6.
7.
8.
9.  Gaunt, p.31.
10. 原文：I was by birth a gentleman, living neither in considerable
    height, nor yet in obscurity；.
11. British Civil Wars, Commonwealth and Proctectorate 1638–1660
12.
13. John Morrill, (1990). "The Making of Oliver Cromwell", in Morrill,
    ed., *Oliver Cromwell and the English Revolution* (Longman), ISBN
    978-0-582-01675-0, p.24.
14. Antonia Fraser, *Cromwell: Our Chief of Men* (1973), ISBN
    978-0-297-76556-1, p. 24.
15. Oliver Cromwell, 2003, p. 68.