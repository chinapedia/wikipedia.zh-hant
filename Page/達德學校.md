[TakTakSchool01.jpg](https://zh.wikipedia.org/wiki/File:TakTakSchool01.jpg "fig:TakTakSchool01.jpg")的達德學校\]\]
[TakTakSchool02.jpg](https://zh.wikipedia.org/wiki/File:TakTakSchool02.jpg "fig:TakTakSchool02.jpg")
[YuKiuAncestralHall02.jpg](https://zh.wikipedia.org/wiki/File:YuKiuAncestralHall02.jpg "fig:YuKiuAncestralHall02.jpg")

**達德學校**為[香港](../Page/香港.md "wikilink")[新界一所已經停辦的](../Page/新界.md "wikilink")[小學](../Page/香港小學教育.md "wikilink")，由[屏山鄧族出資興建](../Page/屏山鄧族.md "wikilink")\[1\]，於1931年創立，直至1961年，達德學校早期一直以[元朗](../Page/元朗區.md "wikilink")[屏山的](../Page/屏山_\(香港\).md "wikilink")[愈喬二公祠作為辦學場所](../Page/愈喬二公祠.md "wikilink")，供予屏山各村子弟修讀，而愈喬二公祠正門的對聯「達期兼善，德修於身」，就是為紀念達德學校而立。1964年，達德學校搬遷到在[屏山南北路新落成的校舍](../Page/屏山南北路.md "wikilink")，該校舍呈「U」字形，樓高兩層，禮堂設於中央，兩翼是課室，中間空地為籃球場，周邊長滿樹木。學校於1998年停辦，校舍存留至今，後來[地政總署將達德學校的大閘鎖上](../Page/地政總署.md "wikilink")，並且聘請保安員看守。

## 鬧鬼傳聞

達德學校新校舍自啟用到停辦期間，不斷傳出鬧鬼事件。可能源於1941年[日本攻佔香港期間](../Page/日本.md "wikilink")，大批抵抗[日本皇軍的屏山村民被殺害而致](../Page/日本皇軍.md "wikilink")，另外一個說法是指於1899年[英國強迫](../Page/英國.md "wikilink")，大批屏山村民為了抵抗[英國軍隊而傷亡](../Page/英國軍隊.md "wikilink")，屍骸被亂葬在該處山邊，成為[亂葬崗](../Page/萬人塚.md "wikilink")，導致達德學校附近陰魂不散，頻頻[鬧鬼](../Page/鬧鬼.md "wikilink")。此外，有指達德學校其中一任校長（另外一個說法是指校長之妻\[2\]）曾經在校內的女廁穿著紅衣自縊，自此廁格經常出現紅衣女鬼，為校內鬧鬼傳言最盛傳之地方\[3\]。可是因為沒有警方報案記錄，當年報章也沒有報導記錄，所以故事是否只是以訛傳訛，無法證實。

2011年9月20日，[香港傳媒廣泛報道一群少年在達德學校](../Page/香港傳媒.md "wikilink")「探險」，其間因為有人聲稱「見鬼」或者聽到「異聲」後過於驚慌，3名少女相繼不適暈倒、情緒失控咬人及雙手自揑頸項，需要友人合力制止及請求[警察到場幫助](../Page/警察.md "wikilink")。而大學學者則表示，案中聲稱「見鬼」的青少年不排除因為[急性反應性精神錯亂而導致產生](../Page/急性反應性精神錯亂.md "wikilink")[幻覺](../Page/幻覺.md "wikilink")。\[4\]

雖然達德學校於2013年被《[國家地理頻道](../Page/國家地理頻道.md "wikilink")》選舉為亞洲十大[恐怖地點之一](../Page/恐怖.md "wikilink")，可惜片中也無法交代鬼故事的事實真相，而當地原居民亦沒有聽聞鬧鬼傳聞。\[5\]。

## 相關條目

  - [愈喬二公祠](../Page/愈喬二公祠.md "wikilink")
  - [鬼](../Page/鬼.md "wikilink")

## 參考

<references/>

## 外部連結

  - [電影服務統籌科 — 拍攝場地資料庫 —
    前達德學校](https://web.archive.org/web/20131103122814/http://www.fso-createhk.gov.hk/main/locations_search_details.php?File_No=01059&search_type=3)
    — 含學校相片
  - \[60停辦村校恐成罪惡溫床\] — [大公網](../Page/大公報.md "wikilink") 2005年10月20日
  - \[鬼上身三成真有其事 呂國佑：勿嘗試接觸邪靈\] [成報](../Page/成報.md "wikilink") 2006年8月10日
  - [廢墟中虛構鬼故與傳聞. 荒凝止息](http://www.inmediahk.net/node/1037031)
    [香港獨立媒體](../Page/香港獨立媒體.md "wikilink") 2015年8月28日
  - [今晚鬼鬼哋 元朗達德
    亞洲十大恐怖地點](http://hk.on.cc/hk/bkn/cnt/lifestyle/20171021/mobile/bkn-20171021000003692-1021_00982_001.html)
    [東網](../Page/東方日報_\(香港\).md "wikilink") 2017年10月21日

[Category:屏山 (香港)](../Category/屏山_\(香港\).md "wikilink")
[Category:香港已關閉小學](../Category/香港已關閉小學.md "wikilink")

1.

2.
3.  [元朗靈異宗祠　達德鬧鬼女廁](http://the-sun.on.cc/channels/adult/20051224/20051223233323_0000.html)
    [太陽報](../Page/太陽報_\(香港\).md "wikilink") 2005年12月24日

4.  [12人探鬼屋
    少女暈倒咬人](http://news.sina.com.hk/news/1474/3/2/2440302/1.html)
     明報專訊 2011年9月20日

5.  [達德學校亞洲十大恐怖地](http://orientaldaily.on.cc/cnt/news/20131102/00176_045.html)
    《東方日報》 2013年11月2日