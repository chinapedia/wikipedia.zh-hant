**环己酮**是六个碳的[环酮](../Page/脂环族化合物.md "wikilink")，室温下为无色油状液体，有类似薄荷油和[丙酮的气味](../Page/丙酮.md "wikilink")，久置颜色变黄。它微溶于水（5-10
g/100 ml），可与大多数有机溶剂混溶。

环己酮在工业上被用作溶剂以及一些氧化反应的触发剂，也用于制取[己二酸](../Page/己二酸.md "wikilink")、环己酮[树脂](../Page/树脂.md "wikilink")、[己内酰胺以及](../Page/己内酰胺.md "wikilink")[尼龙6](../Page/尼龙6.md "wikilink")。\[1\]

## 参考资料

<references/>

[Category:酮](../Category/酮.md "wikilink")
[Category:IARC第3类致癌物质](../Category/IARC第3类致癌物质.md "wikilink")
[Category:六元环](../Category/六元环.md "wikilink")

1.  [环己酮](http://chemicalland21.com/industrialchem/solalc/CYCLOHEXANONE.htm)
    - Chemicalland21.com