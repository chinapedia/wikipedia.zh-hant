[HK_Golden_Beach_Dolphin_Plaza.jpg](https://zh.wikipedia.org/wiki/File:HK_Golden_Beach_Dolphin_Plaza.jpg "fig:HK_Golden_Beach_Dolphin_Plaza.jpg")
[Hong_Kong_Gold_Coast_Dolphin_Square_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Gold_Coast_Dolphin_Square_\(Hong_Kong\).jpg "fig:Hong_Kong_Gold_Coast_Dolphin_Square_(Hong_Kong).jpg")
**屯門黃金海岸海豚廣場**是[香港的一個](../Page/香港.md "wikilink")[廣場](../Page/廣場.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[屯門區](../Page/屯門區.md "wikilink")[黃金海岸](../Page/黃金海岸_\(香港\).md "wikilink")。

為了吸引更多中外旅客到訪，[信和集團與](../Page/信和集團.md "wikilink")[屯門區議會於](../Page/香港區議會.md "wikilink")2005年合作，斥資250萬港元於[康樂及文化事務署轄下的](../Page/康樂及文化事務署.md "wikilink")[黃金泳灘興建](../Page/黃金泳灘.md "wikilink")[中華白海豚雕像](../Page/中華白海豚.md "wikilink")。雕像位於泳灘的末端，跟[龍珠島對望](../Page/龍珠島.md "wikilink")，所在地則原是[直升機坪](../Page/直升機坪.md "wikilink")。整個構思參考了[新加坡著名的](../Page/新加坡.md "wikilink")[魚尾獅像](../Page/魚尾獅像.md "wikilink")，雕塑由三條[海豚組成](../Page/海豚.md "wikilink")，分別象徵[和平](../Page/和平.md "wikilink")、[自由](../Page/自由.md "wikilink")、[快樂](../Page/快樂.md "wikilink")，寓意屯門區吉祥連連，歡樂綿綿。

海豚像高9米、闊6.45米、長11米\[1\]，由兩大一小的海豚組成，形態彷如由海中躍出，動感十足，配以特別燈光效果，使金光閃閃的海豚像，於日夜間產生不同景緻，各具特色。

<File:Hong> Kong Gold Coast Dolphin Square, board for taking photos
(Hong Kong).jpg|海豚造型立板 <File:Hong> Kong Gold Coast Dolphin Square,
planter (Hong Kong).jpg|黃金海岸海豚廣場有海豚設計的花槽

## 資料來源

## 參看

  - [中港城海豚池平台花園](../Page/中港城.md "wikilink")

[Category:香港廣場](../Category/香港廣場.md "wikilink") [Category:黃金海岸
(香港)](../Category/黃金海岸_\(香港\).md "wikilink")

1.