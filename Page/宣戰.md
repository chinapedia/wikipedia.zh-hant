[Franklin_Roosevelt_signing_declaration_of_war_against_Japan.jpg](https://zh.wikipedia.org/wiki/File:Franklin_Roosevelt_signing_declaration_of_war_against_Japan.jpg "fig:Franklin_Roosevelt_signing_declaration_of_war_against_Japan.jpg")於1941年12月8日的[珍珠港事變後簽署對日宣戰聲明](../Page/珍珠港事變.md "wikilink")\]\]
**宣戰**是指一個國家宣告該國与其他（可多於一個）國家處於[戰爭狀態](../Page/戰爭.md "wikilink")。

## 背景

[文藝復興時](../Page/文藝復興.md "wikilink")，已有國家作出正式的宣戰聲明，自此，[民主國家已接受宣戰作為一個](../Page/民主國家.md "wikilink")[外交手段](../Page/外交.md "wikilink")。

在[國際法中](../Page/國際法.md "wikilink")，宣戰聲明代表該等國家互相承認處於敵對狀態，並規範各自的[軍隊的行為](../Page/軍隊.md "wikilink")。[海牙公約是影響這些聲明的主要多邊條約](../Page/海牙公約.md "wikilink")。

[第一次世界大戰後](../Page/第一次世界大戰.md "wikilink")，1919年成立的[國際聯盟](../Page/國際聯盟.md "wikilink")，和1928年在巴黎簽訂的[非戰公約](../Page/非戰公約.md "wikilink")，皆顯示世界各國都致力避免大戰造成的浩劫。然而，[第二次世界大戰仍然爆發了](../Page/第二次世界大戰.md "wikilink")，於是各國於戰後成立了[聯合國機構以防止國家透過宣戰來發動侵略](../Page/聯合國機構.md "wikilink")。

## 聯合國與戰爭

為迫使國家放棄以[戰爭解決紛爭](../Page/戰爭.md "wikilink")，[聯合國憲章的起草者嘗試限制會員國只能在特定環境下方能發動戰爭](../Page/聯合國憲章.md "wikilink")，尤其只是為了[自衞](../Page/自衞.md "wikilink")。

但是，1950年6月25日，[北韓进攻](../Page/北韓.md "wikilink")[南韓後](../Page/南韓.md "wikilink")，聯合國卻參與了[朝鮮戰爭](../Page/朝鮮戰爭.md "wikilink")。[聯合國安全理事會在](../Page/聯合國安全理事會.md "wikilink")[蘇聯代表因抗議](../Page/蘇聯.md "wikilink")[聯合國拒絕接納](../Page/聯合國.md "wikilink")[中華人民共和國為新成員國而自](../Page/中華人民共和國.md "wikilink")1950年1月起缺席的情況下，動議以13對1（[南斯拉夫投了反對票](../Page/南斯拉夫.md "wikilink")）的表決結果得到通過。於是[美國和](../Page/美國.md "wikilink")15個國家組成了「聯合國軍」。美國[總統](../Page/總統.md "wikilink")[哈利·S·杜魯門在](../Page/哈利·S·杜魯門.md "wikilink")6月29日的[記者會上](../Page/記者會.md "wikilink")，稱這種軍事行為為「」，而非「戰爭」。\[1\]

聯合國通過了一些[安全理事會決議](../Page/聯合國安全理事會決議.md "wikilink")，以宣告一些戰爭為合乎國際法的。其中一個決議為關於1991年[波斯灣戰爭的](../Page/波斯灣戰爭.md "wikilink")[第687號決議](../Page/聯合國安理會687號決議.md "wikilink")。

## 不宣而戰

在大部分的[民主國家](../Page/民主.md "wikilink")，須[立法部門通過後方可進行宣戰](../Page/立法部門.md "wikilink")。[美國憲法只說明](../Page/美國憲法.md "wikilink")「[國會有權力](../Page/美國國會.md "wikilink")……宣告戰爭……」，而沒有提及宣戰聲明的形式，故一些人認為經國會通過並授權的軍事行為是「宣戰聲明」。這個概念並無在美國法律制度中證實。[榮·保羅和一些人則爭論明確的宣戰聲明實際上是憲法要求](../Page/榮·保羅.md "wikilink")。\[2\]

聯合國參與韓戰後，很多民主國家把他們的戰爭行為標籤成其他東西，如「軍事行動」或「武裝回應」，例如在美國參與[越南戰爭時](../Page/越南戰爭.md "wikilink")。[法國和一些國家的軍事制度包括很多](../Page/法國.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")，他們以警衛行動來干涉其前殖民地的事務，因為它們不能被視為「國際紛爭」。

不宣而戰是一種歸避憲法對宣戰者的保護，同時避免受到[戰爭法的規限](../Page/戰爭法.md "wikilink")。不使用「戰爭」字眼似乎更符合[公共關係](../Page/公共關係.md "wikilink")。故此，政府多數不再發表宣戰聲明，改為委婉地稱其行動為「警衛行動」和「獲授權使用軍隊」。

## 獲授權使用軍隊

「獲授權使用軍隊」可避免傳統對發動戰爭的限制，故常被用作「宣戰」的代替品。一般來說，必須獲眾多立法機構通過正式聲明，但「獲授權使用軍隊」可容許選舉出來的國家元首直接調動軍隊，而無須太多諮詢。此外，國際間對宣戰越來越多規限，「獲授權使用軍隊」可避免宣戰聲明的負面影響。

獲授權使用軍隊在民主國家中很常見。例如：美國在20世紀後半部直接參與了很多軍事事件，但從未正式宣戰。越戰和[伊拉克戰爭中](../Page/伊拉克戰爭.md "wikilink")，美國國會授權使用軍隊，而不發表宣戰聲明。如上述，一些人質疑這些立法程序是否合乎憲法。

## 二戰期間的宣戰

### 1939年

9月3日：[英國](../Page/英國.md "wikilink")、[法國](../Page/法國.md "wikilink")、[澳洲和](../Page/澳洲.md "wikilink")[新西蘭對](../Page/新西蘭.md "wikilink")[德國宣戰](../Page/納粹德國.md "wikilink")。
9月6日：[南非對德國宣戰](../Page/南非.md "wikilink")。
9月10日：[加拿大對德國宣戰](../Page/加拿大.md "wikilink")。

### 1940年

4月9日：[挪威對德國宣戰](../Page/挪威.md "wikilink")。
5月10日：[荷蘭對德國宣戰](../Page/荷蘭.md "wikilink")。
6月10日：[意大利對法國和英國宣戰](../Page/意大利.md "wikilink")。
6月11日：法國對意大利宣戰。英國、澳洲、加拿大和南非與意大利爆發戰爭。
10月28日：意大利對[希臘宣戰](../Page/希臘.md "wikilink")。
11月23日：[比利時對意大利宣戰](../Page/比利時.md "wikilink")。

### 1941年

4月6日：意大利對[南斯拉夫宣戰](../Page/南斯拉夫.md "wikilink")。
4月24日：[保加利亞對希臘和南斯拉夫宣戰](../Page/保加利亞.md "wikilink")。
6月22日：德國、意大利和[羅馬尼亞對](../Page/羅馬尼亞.md "wikilink")[蘇聯宣戰](../Page/蘇聯.md "wikilink")。
6月25日：[芬蘭官方正式宣布與蘇聯處於戰爭狀態](../Page/芬蘭.md "wikilink")。（沒有通過國會決議）
6月27日：[匈牙利對蘇聯宣戰](../Page/匈牙利.md "wikilink")。
12月6日：英國對芬蘭和羅馬尼亞宣戰。
12月7日([珍珠港事件](../Page/珍珠港事件.md "wikilink"))：日本對美國、英國、澳洲、加拿大、南非宣戰。英國、澳洲、新西蘭對芬蘭、匈牙利和羅馬尼亞宣戰。加拿大對芬蘭、匈牙利、日本和羅馬尼亞宣戰。[巴拿馬對日本宣戰](../Page/巴拿馬.md "wikilink")。南斯拉夫與日本爆發戰爭。
12月8日：美國、英國、澳洲、[哥斯達黎加](../Page/哥斯達黎加.md "wikilink")、[多明尼加共和國](../Page/多明尼加共和國.md "wikilink")、[薩爾瓦多](../Page/薩爾瓦多.md "wikilink")、[海地](../Page/海地.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")、荷蘭、新西蘭和尼加拉瓜對日本宣戰。
12月11日：德國和意大利對美國宣戰。美國和對德國和意大利宣戰，中華民國[南京國民政府正式對日本及德義兩國宣战](../Page/南京國民政府.md "wikilink")。
12月12日：保加利亞對美國和英國宣戰。羅馬尼亞對美國宣戰。 12月13日：匈牙利對美國宣戰。

### 1942年

1月10日：日本對荷蘭宣戰。 1月25日：英國、新西蘭和南非對泰國宣戰。泰國對美國和英國宣戰。
3月2日：[澳大利亚對泰國宣戰](../Page/澳大利亚.md "wikilink")。
5月22日：墨西哥對[軸心國宣戰](../Page/軸心國.md "wikilink")。

### 1943年

10月13日：意大利對德國宣戰。

### 1945年

7月13日：意大利對日本宣戰。 8月8日：蘇聯對日本宣戰。

1945年，[第二次世界大戰已進入尾聲](../Page/第二次世界大戰.md "wikilink")，很多國家雖之前從未參戰，都向德日宣戰。以下是這些國家的列表，括號內為宣戰日期。

[厄瓜多爾](../Page/厄瓜多爾.md "wikilink")（2月2日）、[巴拉圭](../Page/巴拉圭.md "wikilink")（2月8日）、[秘魯](../Page/秘魯.md "wikilink")（2月13日）、[智利](../Page/智利.md "wikilink")（2月14日）、[委內瑞拉](../Page/委內瑞拉.md "wikilink")（2月16日）、[土耳其](../Page/土耳其.md "wikilink")（2月23日）、[烏拉圭](../Page/烏拉圭.md "wikilink")（2月23日）、[埃及](../Page/埃及.md "wikilink")（2月24日）、[敘利亞](../Page/敘利亞.md "wikilink")（2月26日）、[黎巴嫩](../Page/黎巴嫩.md "wikilink")（2月27日）、[沙地阿拉伯](../Page/沙地阿拉伯.md "wikilink")（3月1日）、[芬蘭](../Page/芬蘭.md "wikilink")（3月2日）、[阿根廷](../Page/阿根廷.md "wikilink")（3月27日）

## 仍生效的宣戰

一些宣戰聲明仍然生效，但它們並不代表敵對狀態，而是沒有簽訂和平條約去終止聲明。

  - [第二次國共內戰](../Page/第二次國共內戰.md "wikilink")1945-1949年10月1日[中國共產黨在](../Page/中國共產黨.md "wikilink")[北京建立](../Page/北京.md "wikilink")[中華人民共和國](../Page/中華人民共和國.md "wikilink")。[中華民國政府則退往](../Page/中華民國.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，[兩岸分治](../Page/兩岸分治.md "wikilink")，法律上仍在戰。
  - [敘利亞和](../Page/敘利亞.md "wikilink")[以色列在](../Page/以色列.md "wikilink")[贖罪日戰爭後](../Page/贖罪日戰爭.md "wikilink")，仍然在戰。
  - 伊拉克拒絕簽署[第一次中东战争的停戰協定](../Page/第一次中东战争.md "wikilink")，與以色列仍然處於戰爭狀態；後因[伊拉克戰爭使伊拉克受到美國武力保護](../Page/伊拉克戰爭.md "wikilink")，並跟以色列一樣納入美軍保護傘下，減緩了對立態勢，但因為宗教民族等層面的意識形態差異而未完全結束實質的對立。
  - 由於[納戈爾諾-卡拉巴赫事件的死結](../Page/納戈爾諾-卡拉巴赫.md "wikilink")，[亞美尼亞和](../Page/亞美尼亞.md "wikilink")[阿塞拜疆未能和平解決](../Page/阿塞拜疆.md "wikilink")。
  - 第二次世界大戰後，雖然[蘇聯](../Page/蘇聯.md "wikilink")（[俄羅斯](../Page/俄羅斯.md "wikilink")）和[日本并未缔结和约](../Page/日本.md "wikilink")。不過1956年10月《[日苏联合宣言](../Page/日苏联合宣言.md "wikilink")》声明：自宣言生效之日起，结束两国战争状态。因此，日苏（俄）战争状态已结束。
  - 1982年英國和阿根廷間爆發的[-{zh-cn:马岛战争;zh-sg:福克兰战争;zh-hk福克蘭戰爭;zh-tw:福克蘭戰爭;}-](../Page/福克蘭戰爭.md "wikilink")，英軍在戰爭中獲勝並保全對-{zh-cn:马尔维纳斯群岛;zh-sg:福克兰群岛;zh-hk:福克蘭群島;zh-tw:福克蘭群島;}-的實質主權，但阿根廷方面雖戰敗卻未向英軍正式投降，並繼續堅稱-{zh-tw:福島;zh-hk:福島;zh-cn:马岛;zh-sg:福岛;}-為其領土，且雙方並未簽署停戰協定。
  - 2012年4月18日，[苏丹总统](../Page/苏丹.md "wikilink")[巴希尔宣布向](../Page/奧馬爾·巴希爾.md "wikilink")[南苏丹正式宣战](../Page/南苏丹.md "wikilink")，这是目前世界上最新的宣战声明。

## 参考文献

### 引用

### 来源

  -
## 外部連結

  - [二戰期間的宣戰](http://worldatwar.net/timeline/other/diplomacy39-45.html)

[Category:军事术语](../Category/军事术语.md "wikilink")
[Category:战争法](../Category/战争法.md "wikilink")

1.  <http://teachingamericanhistory.org/library/index.asp?document=594>
2.