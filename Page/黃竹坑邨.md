**黃竹坑邨**（[英語](../Page/英語.md "wikilink")：****）是[香港](../Page/香港.md "wikilink")[房委會轄下的](../Page/香港房屋委員會.md "wikilink")[公共屋邨之一](../Page/公共屋邨.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區_\(香港\).md "wikilink")[黃竹坑](../Page/黃竹坑.md "wikilink")，於1968年開始入伙。該邨是香港最後一個前[政府廉租屋邨](../Page/政府廉租屋邨.md "wikilink")，已於2009年完成清拆。

## 歷史

黃竹坑邨原是政府於1962年推出「政府廉租屋計劃」的16個廉租屋邨之一，也是港島區唯一的政府廉租屋邨。全邨分三期興建，使用[舊長型大廈設計](../Page/舊長型大廈.md "wikilink")，第1及2座於1968年11月落成；第3至6座於1972年10月落成；第7至10座於1973年8月落成。

1983年至1984年間，房屋署全面勘查全港所有樓齡超過5年的公共屋邨，發現不少樓宇的[混凝土強度未及標準](../Page/混凝土.md "wikilink")，其中更有26座因石屎強度太低，有倒塌危險，這批「[問題公屋](../Page/26座問題公屋醜聞.md "wikilink")」必須拆卸，黃竹坑邨第9座是其中之一，樓齡只有15年，於1988年中根據「擴展重建計劃」拆卸，原址改建為一梯級形花圃。

根據房屋委員會的「整體重建計劃」，清拆重建黃竹坑邨，並於2007年9月28日關閉。大部分住戶被安排遷往重建完成的[石排灣邨](../Page/石排灣邨.md "wikilink")。

在屋邨清拆前的空置期間，香港警察於2007年12月借用此邨進行反恐演習。[無綫電視於](../Page/無綫電視.md "wikilink")2008年6月底借用此邨拍攝劇集《[烈火雄心3](../Page/烈火雄心3.md "wikilink")》中的火警及爆炸場面。[1](http://news.sina.com/sinacn/505-104-103-107/2008-06-23/0128554798.html)

黃竹坑邨（1-8座）的原址是港鐵[南港島綫的](../Page/南港島綫.md "wikilink")[車廠及車廠上蓋項目](../Page/港鐵車廠#黃竹坑車廠.md "wikilink")；而旁邊的10座原址則建成[港鐵](../Page/港鐵.md "wikilink")[南港島綫](../Page/南港島綫.md "wikilink")[黃竹坑站](../Page/黃竹坑站.md "wikilink")，規劃中的[南港島綫（西段）的轉綫站亦將在此處](../Page/南港島綫（西段）.md "wikilink")。

<File:WCH> Estate BLK05 and 06.jpg|第5及6座 <File:WCH> Estate BLK10
20071125.jpg|黃竹坑邨第10座 <File:WCH> Catholic Primary School.jpg|黃竹坑天主教小學校舍
<File:The> Map of Wong Chuk Hang Estate.JPG|屋邨地圖

## 屋邨資料

### 樓宇

[WCH_Estate_Overview(Clear_while_dismantling).jpg](https://zh.wikipedia.org/wiki/File:WCH_Estate_Overview\(Clear_while_dismantling\).jpg "fig:WCH_Estate_Overview(Clear_while_dismantling).jpg")
[Former_Site_of_Wong_Chuk_Hang_Estate.jpg](https://zh.wikipedia.org/wiki/File:Former_Site_of_Wong_Chuk_Hang_Estate.jpg "fig:Former_Site_of_Wong_Chuk_Hang_Estate.jpg")

| 樓宇名稱/座號 | 樓宇類型                                                                  | 落成年份                                       | 拆卸年份 | 備註                                                | 層數 | 戶數 | 升降機數 |
| ------- | --------------------------------------------------------------------- | ------------------------------------------ | ---- | ------------------------------------------------- | -- | -- | ---- |
| 第1座     | [新型廉租大廈](../Page/舊長型.md "wikilink")（[舊長型](../Page/舊長型.md "wikilink")） | 1968                                       | 2009 | 重建為[黃竹坑車廠及會興建上蓋物業](../Page/港鐵黃竹坑車廠.md "wikilink") | 11 | 16 | 0    |
| 第2座     | | 19                                                                  | 40                                         | 6    |                                                   |    |    |      |
| 第3座     | 1972                                                                  | | 11                                       | 16   | 0                                                 |    |    |      |
| 第4座     | | 19                                                                  | 60                                         | 10   |                                                   |    |    |      |
| 第5座     | | 19                                                                  | 40                                         | 6    |                                                   |    |    |      |
| 第6座     | | 13                                                                  | 20                                         | 0    |                                                   |    |    |      |
| 第7座     | 1973                                                                  | | 11                                       | 16   | 0                                                 |    |    |      |
| 第8座     | | 19                                                                  | 40                                         | 5    |                                                   |    |    |      |
| 第9座     | 1988                                                                  | [26座問題公屋之一](../Page/26座問題公屋.md "wikilink") | 16   | 16                                                | 0  |    |      |
| 第10座    | 2009                                                                  | 重建為[黃竹坑站](../Page/黃竹坑站.md "wikilink")      | 19   | 64                                                | 10 |    |      |

### 設施

#### 幼稚園

  - [貞德幼稚園](../Page/貞德幼稚園.md "wikilink")（位於第一座地下）
  - [黃竹坑幼稚園](../Page/黃竹坑幼稚園.md "wikilink")（位於第六座地下）
  - [聖本幼稚園](../Page/聖本幼稚園.md "wikilink")（位於第十座地下）

#### 小學

  - [中華基督教會基恆小學](../Page/中華基督教會基恆小學.md "wikilink")
  - [香港西區婦女福利會鄧肇堅小學](../Page/香港西區婦女福利會鄧肇堅小學.md "wikilink")
  - [黃竹坑天主教小學](../Page/黃竹坑天主教小學.md "wikilink")

#### 街市

  - [舊街市](../Page/舊街市.md "wikilink")（位於第二、三及四座之間）
  - [第四座新街市](../Page/第四座新街市.md "wikilink")（位於第四座近警校道）

## 外部連結

  - [房委會黃竹坑邨資料](http://www.housingauthority.gov.hk/b5/interactivemap/estate/0,,3-347-18_4891,00.html)
  - [香港地方: 政府廉租屋](http://www.hk-place.com/view.php?id=204)
  - [香港地方: 黃竹坑邨樓宇平面圖](http://www.hk-place.com/database/i005023c.jpg)
  - [黃竹坑邨圖集](http://www.pbase.com/jack_hui/2252006)
  - [香港地方:未來鐵路](http://www.hk-place.com/view.php?id=335)第十庲
  - [房屋署收回黃竹坑邨](http://www.info.gov.hk/gia/general/200709/28/P200709280207.htm)

[Category:黃竹坑](../Category/黃竹坑.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")
[Category:1968年香港建立](../Category/1968年香港建立.md "wikilink")
[Category:1968年完工的居住建築物](../Category/1968年完工的居住建築物.md "wikilink")