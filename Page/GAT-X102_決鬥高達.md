**GAT-X102
Duel**，**決鬥鋼彈**為日本科幻[動畫作品](../Page/動畫.md "wikilink")[機動戰士鋼彈SEED中的人型兵器](../Page/機動戰士鋼彈SEED.md "wikilink")（MS），為[伊薩克·焦耳所駕駛的機體](../Page/伊薩克·焦耳.md "wikilink")。
[Gat-x102.jpg](https://zh.wikipedia.org/wiki/File:Gat-x102.jpg "fig:Gat-x102.jpg")

## 機體介紹

**GAT-X102
Duel**出廠時，是[X100系中最為標準](../Page/X系列#X100系列.md "wikilink")、同時也是最早完成的一機，只有CIWS、步槍、光束軍刀及盾牌。作為X系列原型機，設計以應對聯合軍各種武器為前提，故此掌心設有供電插槽，武器透過插槽供電，因此使發射時間較內置電源武器更長。（所以有同樣設計的[-{zh-hans:異端高达;zh-hk:迷惘高達;zh-tw:異端鋼彈;}-金色機](../Page/異端鋼彈#金色機.md "wikilink")，能夠使用本機專用的火箭炮；在本機與[-{zh-hans:侵略高达;zh-hk:獵殺高達;zh-tw:侵略鋼彈;}-作戰時](../Page/GAT-X370_Raider.md "wikilink")，能夠啟動[暴風鋼彈的超高衝擊長射程狙擊步槍](../Page/GAT-X103_暴風高達.md "wikilink")，將其擊毀。）

此機以[MS戰為目的而製造](../Page/机动战士.md "wikilink")，因此也有[PS裝甲](../Page/機動戰士技術#PS裝甲（Phase_Shift_Armour）.md "wikilink")。在PS裝甲啟動後以藍、灰白兩色為主。基本武裝和[-{zh-hans:强袭高达;zh-hk:突擊高達;zh-tw:攻擊鋼彈;}-相似](../Page/GAT-X105_突擊高達.md "wikilink")。為[地球聯合軍五台作為其初代MS開發的](../Page/地球聯合軍.md "wikilink")[G兵器之一](../Page/G兵器.md "wikilink")，但遭[ZAFT所強奪](../Page/ZAFT.md "wikilink")。
。
[地球聯合軍出於其為](../Page/地球聯合#地球連合軍.md "wikilink")[G兵器的最初試作機](../Page/G兵器.md "wikilink")，欠缺特殊性能而給予很低的評價。[ZAFT強奪本機後](../Page/ZAFT.md "wikilink")，特別為其設計一套[突擊裝甲](../Page/突擊裝甲.md "wikilink")，一種原本就在ZAFT機體間流行的裝甲系統並予以安裝，使其戰力需要被重新評估繼（但突擊護甲本身並非是使用PS裝甲的材質，因此遇到實彈攻擊還是會破損）。而於X系列量產化時，先行將突擊裝甲的概念，沿用於決鬥鋼彈的量產機決鬥刃（Duel
Dagger）以及供少數強化人使用的[長刃](../Page/長刃.md "wikilink")（Long Dagger）。

而第一次血色情人節戰爭後，[地球聯合軍利用機體資料再次製作本機](../Page/地球聯合#地球連合軍.md "wikilink")，並於投入於測試及實戰，再收集足夠資料後由阿克泰昂企業開發出後繼機[GAT-X1022
Blu Duel蔚藍決鬥鋼彈](../Page/GAT-X1022_蔚藍決鬥高達.md "wikilink")。\[1\]\[2\]

## 戰鬥史

  - 在[歐普的資源衛星海利歐波里斯製造](../Page/歐普.md "wikilink")，然後被[扎夫特軍的特殊任務部隊中的伊薩克](../Page/ZAFT.md "wikilink")·焦耳搶奪，在故事中期敗給[攻擊鋼彈後追加了額外的裝甲](../Page/GAT-X105_攻擊鋼彈.md "wikilink")\[3\]。在此之後連同其他三架X系列機持續追擊大天使號和攻擊鋼彈。
  - 在[巴拿馬和第二次](../Page/巴拿馬.md "wikilink")[雅金·杜威攻防戰中大大活躍](../Page/雅金·杜威攻防戰.md "wikilink")，尤其在雅金·杜威更一口氣擊落[地球聯合軍三部新型](../Page/地球聯合軍.md "wikilink")[X系列中的其中兩部](../Page/X系列.md "wikilink")。是在最後仍存在的早期[X系列中兩部之一](../Page/X系列.md "wikilink")。

## 機體型式

  - 型號：GAT-X102 Duel Gundam
  - 建造：[大西洋聯邦](../Page/大西洋聯邦.md "wikilink")
  - 隸屬：[地球聯合軍](../Page/地球聯合#地球連合軍.md "wikilink") →
    [Z.A.F.T.](../Page/ZAFT.md "wikilink")
  - 駕駛：[伊薩克·焦耳](../Page/伊薩克·焦耳.md "wikilink")
  - 分類：X100系泛用型試作機
  - 用途：基本型接近戰戰用[MS](../Page/MS.md "wikilink")
  - 生產形態：試作機
  - 作業系統：MOBILE SUIT OPERATION SYSTEM 「**G**eneral **U**nilateral
    **N**euro-Link **D**ispersive **A**utonomic **M**aneuver Synthesis
    System」
  - 全高：17.50米
  - 重量：61.9噸 → 103.47噸（「突擊護甲」裝備時）
  - 裝甲材質：[PS裝甲](../Page/機動戰士技術#PS裝甲（Phase_Shift_Armour）.md "wikilink")（本體）
  - 塗裝：深藍、灰白、灰 → 深藍、灰白、灰、橙（「突擊護甲」裝備時）

## 機體武裝

### 基本武裝

  - 75mm 對空自動火神砲砲塔系統「」（）× 2
  - 57mm 高能量光束步槍 × 1
      - 175mm 榴彈砲擊發管 × 1
  - 光束軍刀 × 2
  - 對光束防盾
  - 350mm線性火箭炮「魔槍」（Gae Bolg） × 1

<!-- end list -->

  -
    該武裝實際上是磁軌砲，但也能使用一般火箭炮的火藥式彈頭。
    動畫中因為被[-{zh-hans:異端高达;zh-hk:迷惘高達;zh-tw:異端鋼彈;}-金色機拿走而沒有在劇中使用過](../Page/異端鋼彈#金色機.md "wikilink")，但在漫畫「SEED
    Re：」中則存在由本機使用的紀錄。

### 突擊護甲（Assault Shroud）

  - 115mm [電磁砲](../Page/電磁砲.md "wikilink")「破壞神」（Shiva）
  - 220 mm 5連裝飛彈匣

## 參考文獻

<div class="references-small">

<references />

</div>

## 參見

  - [GAT-X105 Strike](../Page/GAT-X105_突擊高達.md "wikilink")

[Category:C.E.兵器](../Category/C.E.兵器.md "wikilink")

1.  [1](http://www.seed-stargazer.net/mechanics/phantompain.html)"，「GAT-X1022
    Blu Duel」\]
2.  GAT-X105E ストライクノワールガンダム (MG) 模型內附資料
3.  機動戰士-{鋼彈}-SEED{{〈}}1{{〉}}交會而過的羽翼 ISBN 986-7427-27-0