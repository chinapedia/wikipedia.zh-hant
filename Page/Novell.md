**網威**（）是一家專門從事網絡作業系統如[Novell
NetWare與](../Page/Novell_NetWare.md "wikilink")[Linux](../Page/Linux.md "wikilink")、安全身份管理工具及應用整合與合作方案的[美國高科技企業](../Page/美國.md "wikilink")。Novell與[WordPerfect幫助猶他山谷成為專注於高科技軟體開發的區域](../Page/WordPerfect.md "wikilink")。現今該地區很多小公司的員工都曾為Novell工作。。

## 歷史

1979年該公司以「Novell Data Systems
Inc.」之名於[猶他州](../Page/猶他州.md "wikilink")[普若佛創立](../Page/普若佛.md "wikilink")，當時為一家生產[CP/M系統的](../Page/CP/M.md "wikilink")[硬體](../Page/硬體.md "wikilink")[製造商](../Page/製造商.md "wikilink")。由[喬治·坎諾瓦](../Page/喬治·坎諾瓦.md "wikilink")（George
Canova）、[達林·菲爾德](../Page/達林·菲爾德.md "wikilink")（Darin
Field）與[傑克·戴維斯](../Page/傑克·戴維斯.md "wikilink")（Jack
Davis）合作創辦。[維特·維普萊特](../Page/維特·維普萊特.md "wikilink")（Victor V.
Vurpillat）促成與[Safeguard Scientifics,
Inc.董事長](../Page/Safeguard_Scientifics,_Inc..md "wikilink")[彼特·莫佘](../Page/彼特·莫佘.md "wikilink")（Pete
Musser）提供種子資金的交易。這間公司最初發展不太好，戴維斯與坎諾瓦離開了該公司。

### 語源

該公司名字「Novell」由喬治·坎諾瓦的妻子所提議，她錯認「Novell」為法語中「新」的意思（事實上「新」的法語為「」）。

### NetWare

1983年1月該公司的名字縮短為「Novell
Inc.」，而[雷蒙·諾爾達](../Page/雷蒙·諾爾達.md "wikilink")（Raymond
Noorda）成為該公司的領導人，並同時在1983年該公司引進其最重要的產品，多[平臺](../Page/系統平臺.md "wikilink")[網絡作業系統](../Page/網絡作業系統.md "wikilink")[Novell
NetWare](../Page/Novell_NetWare.md "wikilink")。

Novell將它的[網絡傳輸協定基於](../Page/網絡傳輸協定.md "wikilink")[XNS](../Page/Xerox_network_services.md "wikilink")，並從IDP及SPP中創建自己的標準，名為[IPX](../Page/IPX.md "wikilink")（Internetwork
Packet eXchange）與[SPX](../Page/SPX.md "wikilink")（Sequenced Packet
eXchange）。檔案與列印服務透過IPX執行[NetWare核心協定](../Page/NetWare核心協定.md "wikilink")（NCP），IPX同時包括Routing
Information Protocol（RIP）與Service Advertising
Protocol（SAP）。NetWare使用Novell
DOS（前身為[DR-DOS](../Page/DR-DOS.md "wikilink")）作為開機載入器。Novell
DOS類似[MS-DOS與](../Page/MS-DOS.md "wikilink")[IBM
PC-DOS](../Page/IBM_PC-DOS.md "wikilink")，但不需要額外的DOS許可證；這於1991年收購[Digital
Research中獲得](../Page/Digital_Research.md "wikilink")。

Novell在整個80年代都非常成功，侵略性地按成本銷售昂貴的[乙太網卡來增加初期市場](../Page/乙太網.md "wikilink")；90年代Novell幾乎[壟斷了使用網路公司的網絡作業系統](../Page/壟斷.md "wikilink")。

Novell以市場領導者的地位開始在其頂級NetWare作業平台上建設服務。那些服務擴展了NetWare在某些產品上的能力，如SAA用NetWare、Novell多協定路由器、[GroupWise與BorderManager](../Page/GroupWise.md "wikilink")。

### NetWare之外

不過Novell不明智地從事多種經營，放棄一般用戶並以大型企業為主要客群，硏究投資不足及不改進產品的管理工具（這時唯一的可取之處為他們的產品普遍都需要一點「調整」——他們只能啓動）。

1993年該公司自[AT\&T收購](../Page/AT&T.md "wikilink")[Unix System
Laboratories](../Page/Unix_System_Laboratories.md "wikilink")，讓他們有權利使用[Unix作業系統](../Page/Unix.md "wikilink")，明顯企圖打擊[微軟](../Page/微軟.md "wikilink")。1994年Novell買下[WordPerfect以及](../Page/WordPerfect.md "wikilink")[Borland的](../Page/Borland.md "wikilink")[Quattro
Pro產品](../Page/Quattro_Pro.md "wikilink")。那麼收購不能持久：1995年Novell分派部份他們的Unix業務到[聖克魯茲作業](../Page/聖克魯茲作業.md "wikilink")（直至現在Novell與Santa
Cruz的股權繼承人[The SCO
Group仍在爭論資產的分配範圍](../Page/The_SCO_Group.md "wikilink")，參考[SCO
v.
Novell](../Page/SCO_v._Novell.md "wikilink")），1996年WordPerfect與Quattro
Pro一同賣給[Corel](../Page/Corel.md "wikilink")。DR亦於1996年賣給[Caldera
Systems](../Page/Caldera_Systems.md "wikilink")。

Novell在面對新競爭中表現不濟，1994年Noorda被迫下台，數個像臨時雇員一樣不起作用的CEO也跟隨他的步伐下台。該時期Novell的唯一革新為Novell
Directory
Services（NDS），現在稱為[eDirectory](../Page/eDirectory.md "wikilink")，與[NetWare](../Page/NetWare.md "wikilink")
v4.0一起引入。eDirectory取代了NetWare v3.x與更早版本所使用的舊Bindery伺服器與用戶管理工具技術。

1996年該公司在互聯網能力的產品上展開一個為時已晚的行動，放棄依賴專有的「IPX協定」而改為支持TCP/IP。1997年[艾瑞克·史格米德](../Page/艾瑞克·史格米德.md "wikilink")（Eric
Schmidt）成為CEO時加速了這個行動。結果為1998年10月發佈的「NetWare
v5.0」，影響及建設eDirectory，並引進新功能如「Novell Cluster
Services」（NCS，用以取代SFT-III）與「Novell Storage
Services」（NSS），取代NetWare早期版本使用的「Traditional/FAT」檔案系統。當NetWare
v5.0引入原生的支援TCP/IP到NOS，IPX仍然受到支援，容許平穏的過度不同環境與避免競爭環境下經常需要的「鏟車升級」。同樣地Traditional/FAT檔案系統保留為一個可選擇的支援檔案系統。

不過現在Novell已耗盡其市場領導者的地位，並不斷被微軟剝奪市場，微軟直接銷售存取公司資料中心的權限給公司主管而不用透過技術人員。收入減少讓該公司更加努力耕耘網路服務與增加平台的互動性，如eDirectory與GroupWise等皆為多平台。

2001年7月Novell收購顧問公司[Cambridge Technology
Partners](../Page/Cambridge_Technology_Partners.md "wikilink")，以擴展提供的服務。Novell感到有能力提供解決方案（結合軟件與服務）是滿足顧客要求的關鍵。這個改變受到來自公司軟件開發文化以及建議反對合併金融組織的強烈反對。CTP的CEO[傑克·麥斯曼](../Page/傑克·麥斯曼.md "wikilink")（Jack
Messman）從開始時以他Novell理事會成員的地位策劃合併。很快地他亦成為Novell的CEO。

2002年7月Novell收購「SilverStream
Software」，一家看重網絡服務應用開發的領導公司，但在市場中落後。名為「[Novell
exteNd](../Page/Novell_exteNd.md "wikilink")」的業務領域包括了[XML與基於](../Page/XML.md "wikilink")[J2EE的](../Page/J2EE.md "wikilink")[網絡服務](../Page/網絡服務.md "wikilink")。

### 業務用Linux

2003年8月Novell收購了[Ximian](../Page/Ximian.md "wikilink")，一家[Linux](../Page/Linux.md "wikilink")[開放源代碼應用程式的開發公司](../Page/開放源代碼.md "wikilink")。（[Evolution](../Page/Novell_Evolution.md "wikilink")、[Red
Carpet與](../Page/Red_Carpet.md "wikilink")[Mono](../Page/Mono.md "wikilink")）。這次收購表明了Novell計劃轉移其集體產品套裝到[Linux核心](../Page/Linux核心.md "wikilink")。

2003年11月Novell收購[SUSE](../Page/SUSE_Linux.md "wikilink")，一個領導[Linux發行套件的開發者](../Page/Linux發行套件.md "wikilink")，導致主要的力量轉移到Linux發行套件。[IBM亦開始發行Linux套件](../Page/IBM.md "wikilink")，並投資$5000萬以表示對收購SUSE的支持。Novell繼續以SUSE
Linux為基礎發展其商業產品。SUSE Linux 10.1以開放源代碼版本（OSS
Edition）提供，可自由下載的同時並非有限制的評估產品。亦有提供包括正式支援的盒裝零售版本。

2003年夏季Novell發表了「Novell eNterprise Linux
Services」（NNLS），將一些傳統[NetWare的相關服務移植到結合](../Page/NetWare.md "wikilink")[SUSE
LINUX Enterprise
Server](../Page/SUSE_LINUX_Enterprise_Server.md "wikilink")（SLES）第8版。

2004年11月Novell發表基於Linux的企業桌面[Novell Linux
Desktop](../Page/Novell_Linux_Desktop.md "wikilink") v9。這產品基於Ximian
Desktop與SUSE Linux Professional 9.1。這為Novell次嘗試進入企業桌面市場。

[NetWare的繼任產品](../Page/NetWare.md "wikilink")*[Open Enterprise
Server](../Page/Novell_Open_Enterprise_Server.md "wikilink")*於2005年3月發表。OES提供所有之前[NetWare](../Page/NetWare.md "wikilink")
v6.5主持的服務，並增加了***選擇***使用[NetWare](../Page/NetWare.md "wikilink")
v6.5或[SUSE Linux Enterprise
Server](../Page/SUSE_Linux_Enterprise_Server.md "wikilink")
v9核心任一個傳送那些服務。這次發表瞄準說服[NetWare使用者轉移到Linux](../Page/NetWare.md "wikilink")。

### 停滯

2003年至2005年Novell發表一個橫越其業務量的轉折產品，目的為阻止市場份額的下跌以及離開其他附屬Novell的產品，但開始並不如Novell預期那麼成功。為了盡力減低損失Novell在2005年後期進行一輪臨時解雇。同時其Linux業務的收入不斷增加，成長不夠快以容納[NetWare損失的收入](../Page/NetWare.md "wikilink")。這意味着公司的收入不是急速下跌，也不成長。缺乏清晰的方向與不適當的管理意味Novell完成改組需要比預期更多時間。

2006年6月首席執行長[Jack
Messman與財務長](../Page/Jack_Messman.md "wikilink")[Joseph
Tibbetts被解雇](../Page/Joseph_Tibbetts.md "wikilink")，Novell的總裁與首席營運長[Ronald
Hovsepian被任命為首席執行長](../Page/Ronald_Hovsepian.md "wikilink")，而財務副總裁與公司主計員[Dana
Russell被任命為過渡的CFO](../Page/Dana_Russell.md "wikilink")。

2006年Novell出售其顧問子公司[Cambridge Technology
Partners](../Page/Cambridge_Technology_Partners.md "wikilink")，名義上集中其四個核心產品業務量——平台服務、安全識別管理、資源管理與工作群組方案。2006年4月它收購了e-Security,
Inc.，一家領導安全訊息與事件管理（SIEM）空間的提供者。

### Your Linux is Ready

2006年8月Novell發表SUSE Linux Enterprise 10（SLE 10）系列產品。SUSE Linux
Enterprise伺服器是第一個提供基於[Xen監管程式虛擬化的企業級Linux伺服器](../Page/Xen.md "wikilink")。SUSE
Linux Enterprise
Desktop提拱一個新的更加友善的GUI與基於[XGL的](../Page/XGL.md "wikilink")3D顯示能力。SLE
10的發表使用短句「Your Linux is
Ready」（您的Linux已經準備好了）用作市場銷售，意為傳達Novell提供的Linux已為企業準備好。2006年9月後期Novell宣佈一個基於Concurrent
Computer Corporation技術的實時版SLES名為SUSE Linux Enterprise Real Time（SLERT）。

Novell未來的成長主要取決於SLE 10產品在市場的成功與否。

### 與微軟合作

微軟和網威於2006年11月2日達成協議，允許[開放源代碼軟件Linux與微軟的Windows軟體協同作業](../Page/開放源代碼.md "wikilink")。內部著名的原始碼程式設計師傑洛米·艾力森（Jeremy
Allison）也隨即請辭，投向[Google的懷抱](../Page/Google.md "wikilink")。

在多年來試圖擠垮開放原始代碼的競爭對手未果後，微軟表示，將向網威提供技術和相關支持，讓Linux與視窗軟件協同作業，合約中規定微軟負責銷售35萬份Novell的Suse
Linux Enterprise Server，並不控告Suse的使用者侵犯專利。

艾力森認為就算在技術上符合[Samba和](../Page/Samba.md "wikilink")[Linux的](../Page/Linux.md "wikilink")[GPL](../Page/GPL.md "wikilink")，仍有違所有使用者權利均等的開放原始碼原則。（GPL程式的演繹作品也要在GPL之下，Suse
Linux是[GPL之下的軟體](../Page/GPL.md "wikilink")，微軟此舉違反了[GPL的原則](../Page/GPL.md "wikilink")。）

兩家公司的協議主要涉及三個技術領域：允許Linux和視窗能在裝有對方操作系統的機器上運行的[虛擬化技術](../Page/虛擬化技術.md "wikilink")；為使用同時包括Linux和視窗的混合產品的客戶提供網絡服務；以及允許用戶共享文件的兼容文件格式。

## 收購

  - Digital Research－1991年
  - [Unix System
    Laboratories](../Page/Unix_System_Laboratories.md "wikilink")－1993年
  - [WordPerfect](../Page/WordPerfect.md "wikilink") & [Quattro
    Pro](../Page/Quattro_Pro.md "wikilink")（[Borland](../Page/Borland.md "wikilink")）－1994年
  - [Cambridge Technology
    Partners](../Page/Cambridge_Technology_Partners.md "wikilink")－2001年
  - SilverStream Software－2002年
  - [Ximian](../Page/Ximian.md "wikilink")－2003年
  - [SUSE](../Page/SUSE_Linux.md "wikilink")－2003年
  - Salmon－2004年
  - Tally Systems－2005年
  - Immunix－2005年
  - e-Security, Inc－2006年

## 被收購

2010年11月23日，Novell接受以22億美元的收購要求。

## 產品

  - [NetWare](../Page/NetWare.md "wikilink")
  - [Open Enterprise
    Server](../Page/Open_Enterprise_Server.md "wikilink")
  - [GroupWise](../Page/GroupWise.md "wikilink")
  - [NetMail](../Page/NetMail.md "wikilink")
  - [Novell eDirectory](../Page/Novell_eDirectory.md "wikilink")
  - [SUSE Linux Enterprise
    Desktop](../Page/SUSE_Linux_Enterprise_Desktop.md "wikilink")
  - [SUSE Linux Enterprise
    Server](../Page/SUSE_Linux_Enterprise_Server.md "wikilink")
  - [ZENworks Configuration
    Management](../Page/ZENworks_Configuration_Management.md "wikilink")

## 參考文獻

## 外部連結

  - [官方網站](http://www.novell.com/)
  - [Yahoo\! – Novell, Inc.公司介紹](http://biz.yahoo.com/ic/14/14287.html)
  - [Novell Forge](http://forge.novell.com/) –
    Novell主辦贊助的[開放源代碼計劃](../Page/開放源代碼.md "wikilink")
  - [Novell Open Audio](http://www.novell.com/openaudio) –
    Novell技術社區的官方播客
  - [Cool Solutions](http://www.novell.com/coolsolutions/)
    –提示與技巧、指南、工具與其他由Novell社區提交的資源
  - [Cool Blogs](http://www.novell.com/coolblogs/) - official blog of
    Novell employees
  - [NovellBlog.com](https://web.archive.org/web/20070604154918/http://www.novellblog.com/)
  - [Novell Users
    International](https://web.archive.org/web/20060718103450/http://www.nuinet.com/)
  - [abeNd.org](https://web.archive.org/web/20050324091448/http://www.abend.org/)
    – IT專業人士的Novell新聞
  - [猶他州普若佛](http://www.provo.org/)–城市官方網站
  - [Salmon Ltd.](http://www.salmon.com/)
  - [Hong Kong & Macau Novell User Group](http://www.novellughk.org/)
    –非官方網站。供Novell使用者分享、討論的平台。亦有技術文章及花邊新聞

## 参见

  - [SUSE Linux](../Page/SUSE_Linux.md "wikilink")
  - [OpenSUSE](../Page/OpenSUSE.md "wikilink")
  - [SCO v. Novell](../Page/SCO_v._Novell.md "wikilink")
  - [Novell Open Audio](../Page/Novell_Open_Audio.md "wikilink")

{{-}}

[Novell](../Category/Novell.md "wikilink")
[Category:美国软体公司](../Category/美国软体公司.md "wikilink")
[Category:猶他州公司](../Category/猶他州公司.md "wikilink")
[Category:資訊技術顧問公司](../Category/資訊技術顧問公司.md "wikilink")
[Category:Linux公司](../Category/Linux公司.md "wikilink")
[Category:1983年成立的公司](../Category/1983年成立的公司.md "wikilink")