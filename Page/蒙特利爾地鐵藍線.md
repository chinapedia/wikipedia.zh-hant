**蒙特利爾地鐵 5
號線（藍線）**（、）是[蒙特利爾地鐵的](../Page/蒙特利爾地鐵.md "wikilink")[路線之一](../Page/路線.md "wikilink")，來往[諾東站及](../Page/諾東站.md "wikilink")[聖米歇爾站間](../Page/聖米歇爾站.md "wikilink")。其代表色為<span style="color: #0202FE">藍色</span>。

## 車站

<table>
<thead>
<tr class="header">
<th><p>車站名稱</p></th>
<th><p>站體型式</p></th>
<th><p>車站間距</p></th>
<th><p>車站接駁路線</p></th>
<th><p>啟用日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>法語名稱</p></td>
<td><p>中文譯名（非官方）</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>藍線　Blue Line</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/諾東站.md" title="wikilink">諾東站</a></p></td>
<td><p>地下</p></td>
<td><p>959.60 米</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:MtlMetro2.svg" title="fig:MtlMetro2.svg">MtlMetro2.svg</a> <a href="../Page/蒙特利爾地鐵2號線.md" title="wikilink">2號線</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/雪嶺站.md" title="wikilink">雪嶺站</a></p></td>
<td><p>764.60 米</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/蒙特利爾大學站.md" title="wikilink">蒙特利爾大學站</a></p></td>
<td><p>667.60 米</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/奕德華·蒙比提站.md" title="wikilink">奕德華·蒙比提站</a></p></td>
<td><p>1090.60 米</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/外山站.md" title="wikilink">外山站</a></p></td>
<td><p>728.60 米</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/阿卡迪亞站.md" title="wikilink">阿卡迪亞站</a></p></td>
<td><p>727.60 米</p></td>
<td></td>
<td><p>1988年3月28日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/公園站.md" title="wikilink">公園站</a></p></td>
<td><p>490.60 米</p></td>
<td></td>
<td><p>1987年6月15日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/卡斯特諾站.md" title="wikilink">卡斯特諾站</a></p></td>
<td><p>471.60 米</p></td>
<td></td>
<td><p>1986年6月16日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/莊塔隆站.md" title="wikilink">莊塔隆站</a></p></td>
<td><p>839.60 米</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:MtlMetro2.svg" title="fig:MtlMetro2.svg">MtlMetro2.svg</a> <a href="../Page/蒙特利爾地鐵2號線.md" title="wikilink">2號線</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/法比站.md" title="wikilink">法比站</a></p></td>
<td><p>644.50 米</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/迪巴域站.md" title="wikilink">迪巴域站</a></p></td>
<td><p>607.60 米</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/聖米歇爾站_(蒙特利爾).md" title="wikilink">聖米歇爾站</a></p></td>
<td><p>總站</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:蒙特利爾地鐵路線](../Category/蒙特利爾地鐵路線.md "wikilink")
[Category:1986年啟用的鐵路線](../Category/1986年啟用的鐵路線.md "wikilink")