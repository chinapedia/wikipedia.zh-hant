**武汉大学口腔医院**，又名**湖北省口腔医院**（英文：**Wuhan University Stomatological
Hospital**，或**Hubei Stomatological
Hospital**），是一家集医疗、教学、科研于一体的口腔专科医疗机构，是[武汉大学的三所附属](../Page/武汉大学.md "wikilink")[医院之一](../Page/医院.md "wikilink")，[湖北省首批三级甲等医院之一](../Page/湖北省.md "wikilink")。

## 历史

由于当时[中国](../Page/中国.md "wikilink")[中南地区](../Page/中南地区.md "wikilink")[口腔医学教学](../Page/口腔医学.md "wikilink")、科研、医疗水平比较落后，在[中国卫生部](../Page/中华人民共和国卫生部.md "wikilink")、四川医学院（后更名为[华西医科大学](../Page/华西医科大学.md "wikilink")）和上海第二医学院（后更名为[上海第二医科大学](../Page/上海第二医科大学.md "wikilink")）的帮助和支持下，[湖北医学院于](../Page/湖北医学院.md "wikilink")1960年建立口腔医学系。1962年，湖北医学院附属口腔医院在湖北省[武汉市](../Page/武汉市.md "wikilink")[武昌大东门正式开诊](../Page/武昌.md "wikilink")，并同时使用湖北省口腔医院名称。1993年1月，湖北医学院更名为[湖北医科大学](../Page/湖北医科大学.md "wikilink")。同年，口腔医院被中国卫生部评定为湖北省首批三级甲等医院之一。

2000年8月2日，湖北医科大学与[武汉大学](../Page/老武汉大学.md "wikilink")、[武汉水利电力大学](../Page/武汉水利电力大学.md "wikilink")、[武汉测绘科技大学合并为新的](../Page/武汉测绘科技大学.md "wikilink")[武汉大学](../Page/武汉大学.md "wikilink")。医院则更名为武汉大学口腔医院，并继续沿用湖北省口腔医院名称。

## 地理位置

武汉大学口腔医院位于[湖北省](../Page/湖北省.md "wikilink")[武汉市](../Page/武汉市.md "wikilink")[洪山区珞喻路](../Page/洪山区.md "wikilink")237号，附近有[武汉大学](../Page/武汉大学.md "wikilink")、[华中师范大学](../Page/华中师范大学.md "wikilink")、[武汉理工大学](../Page/武汉理工大学.md "wikilink")、[武汉体育学院等高校](../Page/武汉体育学院.md "wikilink")，和[武汉东湖风景区](../Page/武汉东湖.md "wikilink")。

## 现状

目前，武汉大学口腔医院是中国大陆地区口腔医院公认五强之一。\[1\]口腔医院有临床及医技科室17个，在湖北省内有10家门诊部。医院有完备的先进医疗仪器设备，承担[武汉大学医学部](../Page/武汉大学医学部.md "wikilink")[口腔医学院的临床教学任务](../Page/武汉大学口腔医学院.md "wikilink")，并设有口腔医学[博士后科研流动站](../Page/博士后.md "wikilink")。

医院实力较强的学科有：口腔基础医学、口腔临床医学。

## 注释

<div class="references-small">

<references/>

</div>

## 参看

  - [武汉大学](../Page/武汉大学.md "wikilink")
  - [武汉大学医学部](../Page/武汉大学医学部.md "wikilink")
  - [湖北医科大学](../Page/湖北医科大学.md "wikilink")
  - [武漢大學口腔醫學院](../Page/武漢大學口腔醫學院.md "wikilink")

## 外部链接

  - [武汉大学口腔医院网站](https://web.archive.org/web/20060903185934/http://www.whuss.com/)

[Category:武汉大学附属医院](../Category/武汉大学附属医院.md "wikilink")
[鄂](../Category/武汉三级甲等医院.md "wikilink")

1.  中国大陆地区实力最强的五所口腔医院是位于[华北地区的](../Page/华北地区.md "wikilink")[北京大学医学部](../Page/北京大学医学部.md "wikilink")、位于[华东地区的](../Page/华东地区.md "wikilink")[上海交通大学医学院附属第九人民医院](../Page/上海交通大学医学院附属第九人民医院.md "wikilink")、位于[中南地区的武汉大学口腔医院](../Page/中南地区.md "wikilink")、位于[西南地区的](../Page/中国西南地区.md "wikilink")[四川大学华西口腔医院](../Page/四川大学华西口腔医院.md "wikilink")、以及位于[西北地区的](../Page/中国西北地区.md "wikilink")[中国人民解放军空军军医大学口腔医学院](../Page/中国人民解放军空军军医大学口腔医学院.md "wikilink")。它们所依靠的[北京大学](../Page/北京大学.md "wikilink")（原[北京医科大学并入](../Page/北京医科大学.md "wikilink")）、[上海交通大学](../Page/上海交通大学.md "wikilink")（原[上海第二医科大学并入](../Page/上海第二医科大学.md "wikilink")）、[武汉大学](../Page/武汉大学.md "wikilink")（原[湖北医科大学并入](../Page/湖北医科大学.md "wikilink")）、[四川大学](../Page/四川大学.md "wikilink")（原[华西医科大学并入](../Page/华西医科大学.md "wikilink")）、[第四军医大学各有一个口腔医学全国重点学科](../Page/第四军医大学.md "wikilink")，而中国大陆地区其余高校都无口腔医学全国重点学科。