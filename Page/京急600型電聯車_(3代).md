**京急600型電聯車**（）是[京濱急行電鐵的](../Page/京濱急行電鐵.md "wikilink")[通勤型電車](../Page/通勤型電車.md "wikilink")，於1994年投入服務。

## 概要

600型的製造目的是作為[1500型的後繼車](../Page/京急1500型電車.md "wikilink")，並取替[舊1000型](../Page/京急1000型電車_\(首代\).md "wikilink")。在[地鐵對應車中極為罕有使用並列式座位](../Page/地鐵對應車輛.md "wikilink")。

其他的[大手私鐵及](../Page/大手私鐵.md "wikilink")[JR在繁忙時間也紛紛使用多門數的通勤電車作疏導乘客](../Page/JR.md "wikilink")，不過京急為乘客舒適，不惜使用並列式座椅。因為上述的原因，令列車的載客量下降，所以京急避免在繁忙時間使用600型。但是列車的運用是不能脫離現實的，所以京急最後也要在2005年把600型改回[對向式座椅](../Page/日本鐵路車輛座位.md "wikilink")。

## 外観

  - 600型的塗裝與早期的[2000型塗装相同](../Page/京急2000型電車.md "wikilink")。
  - 車頭燈移往車頭較上的部分，日後[2100型](../Page/京急2100型形電車.md "wikilink")，[新1000型也是參照這型式](../Page/京急1000型電車_\(二代\).md "wikilink")。
  - 三次車前的版本仍是雙臂式菱形電弓，但是四次車開始使用單臂式電弓。
  - 600型的車尾燈，車門識別燈為[發光二極管](../Page/發光二極管.md "wikilink")。

## 内裝

  - 早期座位為電動可接式座位，但是在其他鐵路公司運行時將會回復正常。
  - 因為座位損耗嚴重，座位的顏色由藍色轉為紅色（優先席是深藍色）。
  - 因並列式座位載客量低，列車於翻新時改回對向式座位設計。

## 性能

  - 最高速度：130km/h
  - 起動加速度：3.5km/h/s（601編成-607編成）；3.3km/h/s（608編成，651-656編成）

## 保安裝置

  - [ATS及C](../Page/ATS.md "wikilink")-ATS

## 制御裝置

控制裝置使用[東洋電機製造及](../Page/東洋電機製造.md "wikilink")[三菱電機製的](../Page/三菱電機.md "wikilink")[GTO牽引逆變器](../Page/可關斷晶閘管.md "wikilink")。

## 運用

  - 8-{zh-tw:節;zh-cn:节;zh-hk:卡}-編成常見在[快特](../Page/快速特急.md "wikilink")、機場快特及[特急](../Page/特別急行列車.md "wikilink")，對[都營淺草線](../Page/都營淺草線.md "wikilink")、[京成線](../Page/京成電鐵.md "wikilink")、[北總線乘入的班次](../Page/北總鐵道.md "wikilink")。此外，在京成線會以[ACCESS特急經](../Page/特別急行列車.md "wikilink")[北總線和](../Page/北總線.md "wikilink")[成田機場線駛往](../Page/成田機場線.md "wikilink")[成田機場](../Page/成田機場.md "wikilink")。由於京成電鐵要求行經
    [成田機場線](../Page/成田機場線.md "wikilink")
    列車必須有[停車預告裝置](../Page/停車預告裝置.md "wikilink")，京急目
    前只有600形和新1000形10次車配有此裝置，亦只有此兩款京急車能作為常規班次駛入京成路段。
  - 4-{zh-tw:節;zh-cn:节;zh-hk:卡}-編成的多數在京急自社線內的[普通班次行走](../Page/普通列車.md "wikilink")。現在全編成的[方向幕更改為LED顯示屏](../Page/方向幕.md "wikilink")。

## 編成

<div style="float:right; margin:0 0 0 10px;text-align:center;">

[Keikyu-607-since-1994.jpg](https://zh.wikipedia.org/wiki/File:Keikyu-607-since-1994.jpg "fig:Keikyu-607-since-1994.jpg")
[Keikyu-606-since-1994.jpg](https://zh.wikipedia.org/wiki/File:Keikyu-606-since-1994.jpg "fig:Keikyu-606-since-1994.jpg")
[Keikyu_600_series_606_formation.jpg](https://zh.wikipedia.org/wiki/File:Keikyu_600_series_606_formation.jpg "fig:Keikyu_600_series_606_formation.jpg")

</div>

**粗體**為對向式座椅改造車

  - 第1次～3次車　8-{zh-tw:節;zh-cn:节;zh-hk:卡}-編成

| M1c       | M2        | Tu        | Ts        | M1'       | M2'       | M1        | M2c       | 製造廠商                                 |
| --------- | --------- | --------- | --------- | --------- | --------- | --------- | --------- | ------------------------------------ |
| *601-1*   | *601-2*   | *601-3*   | *601-4*   | *601-5*   | *601-6*   | *601-7*   | *601-8*   | [東急車輛](../Page/東急車輛製造.md "wikilink") |
| *602-1*   | *602-2*   | *602-3*   | *602-4*   | *602-5*   | *602-6*   | *602-7*   | *602-8*   | [川崎重工](../Page/川崎重工.md "wikilink")   |
| 603-1     | 603-2     | 603-3     | 603-4     | 603-5     | 603-6     | 603-7     | 603-8     | 東急車輛                                 |
| 604-1     | 604-2     | 604-3     | 604-4     | 604-5     | 604-6     | 604-7     | 604-8     | 川崎重工                                 |
| 605-1     | 605-2     | 605-3     | 605-4     | 605-5     | 605-6     | 605-7     | 605-8     | 東急車輛・川崎重工                            |
| **606-1** | **606-2** | **606-3** | **606-4** | **606-5** | **606-6** | **606-7** | **606-8** | 東急車輛                                 |
| 607-1     | 607-2     | 607-3     | 607-4     | 607-5     | 607-6     | 607-7     | 607-8     | 川崎重工                                 |

  - 4次車　8-{zh-tw:節;zh-cn:节;zh-hk:卡}-編成

| Muc       | T         | Tp1       | Mu        | Ms        | T         | Tp1       | Msc       | 製造廠商 |
| --------- | --------- | --------- | --------- | --------- | --------- | --------- | --------- | ---- |
| **608-1** | **608-2** | **608-3** | **608-4** | **608-5** | **608-6** | **608-7** | **608-8** | 東急車輛 |

  - 第4次車　4-{zh-tw:節;zh-cn:节;zh-hk:卡}-編成

<table>
<thead>
<tr class="header">
<th><p>Muc</p></th>
<th><p>T</p></th>
<th><p>Tp2</p></th>
<th><p>Msc</p></th>
<th><p>製造廠商</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>651-1</p></td>
<td><p>651-2</p></td>
<td><p>651-3</p></td>
<td><p>651-4</p></td>
<td><p><br />
</p></td>
</tr>
<tr class="even">
<td><p>652-1</p></td>
<td><p>652-2</p></td>
<td><p>652-3</p></td>
<td><p>652-4</p></td>
<td><p><br />
</p></td>
</tr>
<tr class="odd">
<td><p>653-1</p></td>
<td><p>653-2</p></td>
<td><p>653-3</p></td>
<td><p>653-4</p></td>
<td><p>川崎重工</p></td>
</tr>
<tr class="even">
<td><p>654-1</p></td>
<td><p>654-2</p></td>
<td><p>654-3</p></td>
<td><p>654-4</p></td>
<td><p><br />
</p></td>
</tr>
<tr class="odd">
<td><p>655-1</p></td>
<td><p>655-2</p></td>
<td><p>655-3</p></td>
<td><p>655-4</p></td>
<td><p><br />
</p></td>
</tr>
<tr class="even">
<td><p>656-1</p></td>
<td><p>656-2</p></td>
<td><p>656-3</p></td>
<td><p>656-4</p></td>
<td><p><br />
</p></td>
</tr>
</tbody>
</table>

## 其他

  - 一部分的四次車在試車時裝設雙臂式菱形集電弓，營業時改裝設單臂式集電弓。
  - 編成605裝設了電纜觀測裝置（在試車時）。而這裝置之後移到1500型1601號車上。
  - 編成605於車內裝設了LCD屏幕以顯示行車資訊及廣告。

[Category:京濱急行電鐵](../Category/京濱急行電鐵.md "wikilink")
[Category:日本電力動車組](../Category/日本電力動車組.md "wikilink")
[Category:京濱急行電鐵車輛](../Category/京濱急行電鐵車輛.md "wikilink")
[Category:川崎製鐵路車輛](../Category/川崎製鐵路車輛.md "wikilink")
[Category:東急製鐵路車輛](../Category/東急製鐵路車輛.md "wikilink") [Keikyuu Class 600
III](../Category/1500伏直流电力动车组.md "wikilink")