<table>
<thead>
<tr class="header">
<th><p>Socket 754</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Elitegroup_761GX-M754_-_Socket_754-5485.jpg" title="fig:240x240像素">240x240像素</a></p>
</center></td>
</tr>
<tr class="even">
<td><p>插座規格</p></td>
</tr>
<tr class="odd">
<td><p>類型：</p></td>
</tr>
<tr class="even">
<td><p>針腳數目：</p></td>
</tr>
<tr class="odd">
<td><p>FSB:</p></td>
</tr>
<tr class="even">
<td><p>電壓：</p></td>
</tr>
<tr class="odd">
<td><p>支援處理器：</p></td>
</tr>
</tbody>
</table>

**Socket 754**是[AMD處理器專用插座的一種](../Page/AMD.md "wikilink")，用作取代[Athlon
XP所使用的](../Page/Athlon_XP.md "wikilink")[Socket
A](../Page/Socket_A.md "wikilink")，也是首款支援自家的64位平台AMD64（[x86-64](../Page/x86-64.md "wikilink")）的插座。它採用HyperTransport技術與晶片組連接。

這款插座屬PGA-ZIF形式，使用時-{只}-要把槓桿拉起，將處理器依正確方向插入，再把槓桿推落鎖緊即可。

Socket 754支援K8微架構的[Athlon
64](../Page/Athlon_64.md "wikilink")、[Sempron](../Page/Sempron.md "wikilink")、[Turion
64及](../Page/Turion_64.md "wikilink")[Athlon
XP-M處理器](../Page/Athlon.md "wikilink")，與[Socket
939主要不同的地方計有](../Page/Socket_939.md "wikilink")：

  - 支援包含最多3條DIMM插槽的單通道記憶體控制器（64位）
  - [HyperTransport傳輸速度較低](../Page/HyperTransport.md "wikilink")（800MHz的16位雙向資料傳輸）
  - 有效資料頻寬較少 (9.6GB/s)
  - 主機版製作成本較低

此外，Socket 754並不如Socket 939般支援雙通道記憶體，主要針對低價處理器Sempron及單通道記憶體版的Athlon
64，而AMD方面也宣佈即將停止生產Socket 754版的Athlon 64。

## 使用Socket 754的處理器核心

| 核心               | 品牌                                           | 型號                       | 備註                                     |
| ---------------- | -------------------------------------------- | ------------------------ | -------------------------------------- |
| Clawhammer       | [Athlon 64](../Page/Athlon_64.md "wikilink") | 2800+\~3700+             |                                        |
| Newcastle        | [Athlon 64](../Page/Athlon_64.md "wikilink") | 2800+\~3400+             |                                        |
| Venice           | [Athlon 64](../Page/Athlon_64.md "wikilink") | 3000+\~3200+             |                                        |
| Palermo          | [Sempron](../Page/Sempron.md "wikilink")     | 2500+\~3000+             |                                        |
| Winchester 128KB | [Sempron](../Page/Sempron.md "wikilink")     | 2600+                    |                                        |
| Paris            | [Sempron](../Page/Sempron.md "wikilink")     | 3000+,3100+              |                                        |
| Lancaster        | [Turion 64](../Page/Turion_64.md "wikilink") | MT-28\~MT40，ML-28\~ML-44 | 僅供[流動平台使用](../Page/流動平台.md "wikilink") |

## 支援Socket 754的晶片組

### AMD

  - AMD 8000

### ATI

  - ATI xPress 200, xPress 200P

### nVidia

  - [nForce 3](../Page/nForce_3.md "wikilink")

<!-- end list -->

  -
    nForce 3 150/Pro 150/Pro 250/Pro 250 Gb

<!-- end list -->

  - [nForce 4](../Page/nForce_4.md "wikilink")

<!-- end list -->

  -
    nForce 4/Ultra/SLi/SLi X16

### SiS

[Sis_760gxlv.jpg](https://zh.wikipedia.org/wiki/File:Sis_760gxlv.jpg "fig:Sis_760gxlv.jpg")

  - SiS 755/755FX/756
  - SiS 760/761

### ULi

  - M1687/M1689/M1695/M1695 TGi/M1697

### VIA

  - VIA K8T800/Pro, K8M800
  - VIA K8T890
  - VIA K8T935

[Category:CPU插座](../Category/CPU插座.md "wikilink")