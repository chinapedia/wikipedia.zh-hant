**奇雲·路蘭**（，），是一名[英格蘭職業](../Page/英格蘭.md "wikilink")[足球員](../Page/足球.md "wikilink")，曾擔任[英格蘭乙組足球聯賽球隊](../Page/英格蘭乙組足球聯賽.md "wikilink")[諾士郡的球員兼領隊](../Page/諾士郡足球會.md "wikilink")。司職[中場](../Page/中場.md "wikilink")。

路蘭於[英格蘭](../Page/英格蘭.md "wikilink")[利物浦的](../Page/利物浦.md "wikilink")[托克斯泰斯長大](../Page/托克斯泰斯.md "wikilink")，於16歲時，他成為[博尔顿的球員](../Page/博尔顿足球俱乐部.md "wikilink")。路蘭跟隨球隊在2001年的[英格蘭甲組聯賽升班附加賽中擊敗](../Page/英格蘭甲組聯賽.md "wikilink")[普雷斯顿](../Page/普雷斯顿足球俱乐部.md "wikilink")，成功升班至[英格蘭超級足球聯賽](../Page/英格蘭超級足球聯賽.md "wikilink")，他亦成為球隊的正選球員。路蘭於頂級聯賽的首兩季皆在作客對陣[曼聯時入球](../Page/曼聯.md "wikilink")，令球隊連續兩屆在[奧脫福球場勝利而回](../Page/奧脫福球場.md "wikilink")。他亦時常為球隊攻入重要的入球，協助球隊間中以聯賽頭十名的成績完成賽季。

2005-06賽季，保頓歷史性首次獲得參加[歐洲足協盃的資格](../Page/歐洲足協盃.md "wikilink")，路蘭亦是其中一位正選球員，可是球隊於淘汰賽三十二強階段出局。2006年，[杰伊·杰伊·奥科查離隊後](../Page/杰伊·杰伊·奥科查.md "wikilink")，路蘭成為保頓的隊長，他帶領球隊於獲得參加2007-08賽季歐洲足協盃的資格，球隊於淘汰賽十六強階段被淘汰。

成為保頓的領隊後，球隊和路蘭的表現受一批球迷批評，因此路蘭於2009年1月的[轉會窗轉投](../Page/轉會窗.md "wikilink")[纽卡斯尔联](../Page/纽卡斯尔联足球俱乐部.md "wikilink")，轉會費為約400萬英鎊。紐卡素於該季完結時排名第18，次季需降班至[英格蘭冠軍足球聯賽作賽](../Page/英格蘭冠軍足球聯賽.md "wikilink")。路蘭於英冠的表現受到廣泛讚揚，因他在各項賽事中攻入18球，包括上演職業生涯首次的[帽子戲法](../Page/帽子戲法.md "wikilink")，協助球隊以英冠冠軍的身份重返英超。升班後，路蘭獲任命為球隊隊長。

2011年夏天，路蘭轉會至英冠球隊[西汉姆联](../Page/西汉姆联足球俱乐部.md "wikilink")，簽約5年，與前保頓領隊[-{zh-hans:山姆·阿勒代斯;zh-hk:森姆·艾拿戴斯;zh-tw:山姆·阿勒代斯;}-再度合作](../Page/萨姆·阿勒代斯.md "wikilink")。他轉會後立刻成為球隊的隊長，並於該季帶領球隊返回英超。2015年8月，路蘭與球會雙方同意下解約，並於2016年1月成為[奧連特的](../Page/莱顿东方足球俱乐部.md "wikilink")[球員兼領隊](../Page/球員兼領隊.md "wikilink")，但在同年4月除去領隊的職務。

## 早期生涯

路蘭在1982年6月24日於[默西賽德郡](../Page/默西賽德郡.md "wikilink")[利物浦出生](../Page/利物浦.md "wikilink")\[1\]，並成長於[托克斯泰斯的一個足球家庭](../Page/托克斯泰斯.md "wikilink")\[2\]。他自小已立志當一名足球員，年幼時曾入讀。路蘭於14歲時，曾代表利物浦市學童隊參賽\[3\]。路蘭年少時為[些路迪和](../Page/些路迪足球會.md "wikilink")[利物浦的球迷](../Page/利物浦足球會.md "wikilink")\[4\]\[5\]，但相反地，他所喜愛的球員為[曼聯的](../Page/曼聯.md "wikilink")[-{zh-hans:埃里克·坎通纳;zh-hk:艾力·簡東拿;zh-tw:埃里克·坎通納;}-和](../Page/埃里克·坎通纳.md "wikilink")[-{zh-hans:李·夏普;zh-hk:李·沙柏;zh-tw:李·夏普;}-](../Page/李·夏普.md "wikilink")\[6\]。

## 球會生涯

### 保頓

路蘭出生於英國[利物浦市](../Page/利物浦.md "wikilink")，出道時就已加盟[保頓青年隊](../Page/博尔顿足球俱乐部.md "wikilink")，17歲成為一隊成員。2000/01年[英甲聯賽中開始獲得器重](../Page/英格蘭足球甲級聯賽.md "wikilink")，於對[克魯的聯賽中取得個人首個聯賽入球](../Page/克鲁足球俱乐部.md "wikilink")。次季，已升上[英超的保頓險而降班](../Page/英格蘭足球超級聯賽.md "wikilink")，幸路蘭的出色表現使球會依能停留於英超之中。

2003/04年球季，路蘭全季上陣35場，並貢獻12球，又協助球會晉身[英格蘭聯賽盃決賽](../Page/英格蘭聯賽盃.md "wikilink")，可惜不敵另一支英超球會[米杜士堡](../Page/米杜士堡足球會.md "wikilink")，與[冠軍無緣](../Page/冠軍.md "wikilink")。後來，路蘭又助球會在聯賽中得到第6名，從而得到參與[歐洲足協盃的資格](../Page/歐洲足協盃.md "wikilink")，因此，路蘭與球會續約至2009年。2005/06年球季，路蘭表現更上一層樓，他於多場，如對[樸茨茅夫](../Page/朴茨茅斯足球俱乐部.md "wikilink")、[查爾頓以及對](../Page/查尔顿竞技足球俱乐部.md "wikilink")[熱刺都](../Page/托特纳姆足球俱乐部.md "wikilink")「一箭定江山」，為球會得勝，功不可沒。

### 紐卡素

2007年保頓教練[艾拿戴斯投效英超球會](../Page/萨姆·阿勒代斯.md "wikilink")[紐卡素](../Page/紐卡素足球會.md "wikilink")，曾一度邀請路蘭過檔，但遭路蘭拒絕。2009年1月，路蘭以400萬[英鎊轉會紐卡素](../Page/英鎊.md "wikilink")，簽約四年半\[7\]。

[Kevin_Nolan_with_Championship_trophy.jpg](https://zh.wikipedia.org/wiki/File:Kevin_Nolan_with_Championship_trophy.jpg "fig:Kevin_Nolan_with_Championship_trophy.jpg")冠軍獎盃\]\]
雖然紐卡素降班[英冠](../Page/英冠.md "wikilink")，但路蘭表現出色，帶領球隊在聯賽領放，9月26日作客4-0擊敗[葉士域治一役取得個人職業生涯首次演出](../Page/伊普斯维奇城足球俱乐部.md "wikilink")[帽子戲法](../Page/帽子戲法.md "wikilink")，直到2010年3月仍以13球成為球隊首席射手，更在[畢特及](../Page/尼基·巴特.md "wikilink")[阿倫·史密夫雙雙缺陣時](../Page/阿兰·史密斯.md "wikilink")，擔任代[隊長帶領球隊](../Page/隊長_\(足球\).md "wikilink")5-1大勝[卡迪夫城](../Page/卡迪夫城足球俱乐部.md "wikilink")。路蘭於3月14日「英格蘭聯賽頒獎禮」獲選為「英格蘭冠軍聯賽年度最佳球員」\[8\]。4月5日主場對[錫菲聯](../Page/谢菲尔德联足球俱乐部.md "wikilink")，路蘭射入致勝球協助球隊2-1獲勝兼確保升班[英超資格](../Page/英超.md "wikilink")\[9\]。

[2010/11年球季路蘭取代退役的](../Page/2010年至2011年英格蘭超級聯賽.md "wikilink")[畢特獲正式委任為](../Page/尼基·巴特.md "wikilink")[隊長](../Page/隊長_\(足球\).md "wikilink")\[10\]。他於球隊回升英超後首場主場6-0大勝[阿士東維拉](../Page/阿士東維拉足球會.md "wikilink")[梅開二度射入季內首兩記入球](../Page/梅開二度.md "wikilink")\[11\]；於2010年10月31日路蘭取得生平首次[帽子戲法](../Page/帽子戲法.md "wikilink")，協助紐卡素在「泰恩-威爾打吡」（Tyne–Wear
derby）主場5-1大破[新特蘭](../Page/桑德兰足球俱乐部.md "wikilink")\[12\]。2010年12月11日於新任領隊[-{zh-hans:阿兰·帕德鲁;
zh-hk:阿倫·柏祖;}-](../Page/阿兰·帕德鲁.md "wikilink")（Alan
Pardew）執教首場事對[利物浦](../Page/利物浦足球俱乐部.md "wikilink")，路蘭先拔頭籌，紐卡素最終以3-1取勝\[13\]，他於2011年1月16日次場打吡戰的作客賽事再次首開記錄，雙方1-1平手完場\[14\]。2月26日路蘭在[聖占士公園球場對舊東家](../Page/聖占士公園球場.md "wikilink")[保頓射入個人第](../Page/博尔顿足球俱乐部.md "wikilink")50個英超入球\[15\]。季末尚餘兩輪賽事，路蘭接受[手術治理整季困擾他的](../Page/手術.md "wikilink")[腳踝傷患](../Page/腳踝.md "wikilink")\[16\]，他整季射入12球成為球會首席射手。

### 韋斯咸

路蘭與紐卡素商討續約，但雙方未能就合約年期達成協議，剛降班[英冠的](../Page/英冠.md "wikilink")[韋斯咸隨即出價](../Page/西汉姆联足球俱乐部.md "wikilink")150萬[英鎊求購被拒](../Page/英鎊.md "wikilink")\[17\]。2011年6月15日韋斯咸提高出價獲得接納，紐卡素准許路蘭與韋斯咸商討加盟條件及接受體測\[18\]。6月16日落實加盟，轉會費沒有透露，簽約五年，重投老上司[艾拿戴斯麾下](../Page/萨姆·阿勒代斯.md "wikilink")\[19\]。由於[-{zh-hans:厄普森;
zh-hk:厄遜;}-約滿離隊轉投](../Page/马修·厄普森.md "wikilink")[史篤城](../Page/斯托克城足球俱乐部.md "wikilink")，曾獲艾拿戴斯在[保頓及](../Page/博尔顿足球俱乐部.md "wikilink")[紐卡素委派擔任](../Page/纽卡斯尔联足球俱乐部.md "wikilink")[隊長的路蘭再次戴上隊長臂章](../Page/隊長_\(足球\).md "wikilink")\[20\]。2011年8月7日於揭幕戰主場對[卡迪夫城首次披甲上陣](../Page/卡迪夫城足球俱乐部.md "wikilink")，但落筆打三更，以0-1不敵未能開門紅\[21\]。但次輪賽事韋斯咸隨即憑路蘭的遠柱「窩利」1-0作客小勝[唐卡士打](../Page/唐卡斯特流浪足球俱乐部.md "wikilink")\[22\]。因為路蘭出色的表現及全季射入13球，帶領韋斯咸升上英超。

路蘭於2012-13年的英超球季繼續了他出色的表現，首先於首場聯賽對[阿士東維拉時射入唯一的入球](../Page/阿士東維拉.md "wikilink")，協助球隊於重返英超的首戰全取三分\[23\]。2012年9月1日，在對[富咸的比賽中](../Page/富咸.md "wikilink")，路蘭於開賽不足一分鐘便為球隊打開紀錄，最後韋斯咸以3比0獲勝\[24\]。路蘭與前隊友[卡路爾再次拍擋](../Page/安德魯·卡路爾.md "wikilink")，為韋斯咸進攻重心，路蘭亦於2013年5月19日對[雷丁的最後一場聯賽](../Page/雷丁.md "wikilink")，射入個人第二次的帽子戲法，以4比2擊敗對手\[25\]，得以第十名的佳績完成聯賽。

### 奧連特

2016年1月21日，路蘭以球員兼領隊的方式，加入英乙的[奧連特](../Page/莱顿东方足球俱乐部.md "wikilink")，雙方簽約2年半\[26\]。2日後，他以教練的身份，贏得對陣[-{zh-hans:韦康比流浪者;
zh-hant:韋甘比;}-的聯賽](../Page/韦康比流浪者足球俱乐部.md "wikilink")，最終比分為2比0。他亦把自己列入後備的其中一員，但並沒有派遣自己上陣\[27\]。1月26日，他於作客[-{zh-hans:纽波特郡;
zh-hk:新港郡;}-的比賽中](../Page/纽波特郡足球俱乐部.md "wikilink")，於第79分鐘後備入替。於比賽中，他的傳中球協助球隊贏得1個[十二碼](../Page/十二碼.md "wikilink")，並由前鋒[-{zh-hans:杰伊·辛普森;zh-hk:積·森遜;zh-tw:杰伊·辛普森;}-射入全場唯一的入球](../Page/積·森遜.md "wikilink")，協助球隊以1比0勝出\[28\]。2月6日，他在對陣[朴茨茅斯的比賽中首次正選上陣](../Page/朴茨茅斯足球俱乐部.md "wikilink")，於第70分鐘退下火線，最終球隊以1比0勝出\[29\]。

### 諾士郡

2017年1月12日，路蘭成為諾士郡的領隊\[30\]，並於30日注冊兼任球員\[31\]。

## 國家隊

路蘭曾被[英格蘭 U18](../Page/英格蘭18歲以下足球代表隊.md "wikilink")\[32\]和[英格蘭
U21](../Page/英格蘭21歲以下足球代表隊.md "wikilink")\[33\]徵召進入大軍名單，並曾為後者上陣。部份[體育評述員認為他將來能成為其中一位候選加入](../Page/體育評述員.md "wikilink")[英格蘭國家足球隊球員](../Page/英格蘭國家足球隊.md "wikilink")\[34\]。有報導指，因路蘭有着[愛爾蘭和](../Page/愛爾蘭.md "wikilink")[荷蘭的血統](../Page/荷蘭.md "wikilink")，[愛爾蘭國家足球隊](../Page/愛爾蘭國家足球隊.md "wikilink")\[35\]和[荷蘭國家足球隊皆能徵召路蘭](../Page/荷蘭國家足球隊.md "wikilink")\[36\]。

2012年11月12日，路蘭於一次訪問提到，他對自己經常被英格蘭隊的領隊忽略而感到傷心\[37\]。路蘭成為於英超上陣次數最多但未能為英格蘭隊上陣的球員\[38\]。

## 個人生活

路蘭於2005年訂婚，並於2008年夏季正式迎娶女友海莉（Hayley）。兩人育有兩名子女，長女潔絲明·伊麗莎白（Jasmine
Elizabeth）於2006年11月出生，而幼子則在2010年11月出生\[39\]\[40\]。

## 生涯統計

<table>
<thead>
<tr class="header">
<th><p>球會</p></th>
<th><p>賽季</p></th>
<th><p>聯賽</p></th>
<th><p><a href="../Page/英格蘭足總盃.md" title="wikilink">英格蘭足總盃</a></p></th>
<th><p><a href="../Page/英格蘭聯賽盃.md" title="wikilink">英格蘭聯賽盃</a></p></th>
<th><p>歐洲賽事</p></th>
<th><p>其他</p></th>
<th><p>總計</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>聯賽</p></td>
<td><p>出場</p></td>
<td><p>入球</p></td>
<td><p>出場</p></td>
<td><p>入球</p></td>
<td><p>出場</p></td>
<td><p>入球</p></td>
<td><p>出場</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/博尔顿足球俱乐部.md" title="wikilink">博尔顿</a></p></td>
<td><p>[41]</p></td>
<td><p><a href="../Page/英格蘭甲組聯賽.md" title="wikilink">英格蘭甲組聯賽</a></p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p>[42]</p></td>
<td><p>31</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2001–02賽季英格蘭超級聯賽.md" title="wikilink">2001-02賽季</a>[43]</p></td>
<td><p><a href="../Page/英格蘭超級足球聯賽.md" title="wikilink">英格蘭超級足球聯賽</a></p></td>
<td><p>35</p></td>
<td><p>8</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002–03賽季英格蘭超級聯賽.md" title="wikilink">2002-03賽季</a>[44]</p></td>
<td><p>33</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2003–04賽季英格蘭超級聯賽.md" title="wikilink">2003-04賽季</a>[45]</p></td>
<td><p>37</p></td>
<td><p>9</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004–05賽季英格蘭超級聯賽.md" title="wikilink">2004-05賽季</a>[46]</p></td>
<td><p>36</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2005–06賽季英格蘭超級聯賽.md" title="wikilink">2005-06賽季</a>[47]</p></td>
<td><p>36</p></td>
<td><p>9</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>7</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006–07賽季英格蘭超級聯賽.md" title="wikilink">2006-07賽季</a>[48]</p></td>
<td><p>31</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007–08賽季英格蘭超級聯賽.md" title="wikilink">2007-08賽季</a>[49]</p></td>
<td><p>33</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>5</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008–09賽季英格蘭超級聯賽.md" title="wikilink">2008-09賽季</a>[50]</p></td>
<td><p>20</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>總計</p></td>
<td><p>296</p></td>
<td><p>40</p></td>
<td><p>20</p></td>
<td><p>4</p></td>
<td><p>15</p></td>
<td><p>4</p></td>
<td><p>12</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/纽卡斯尔联足球俱乐部.md" title="wikilink">纽卡斯尔联</a></p></td>
<td><p><a href="../Page/2008–09賽季英格蘭超級聯賽.md" title="wikilink">2008-09賽季</a>[51]</p></td>
<td><p>英格蘭超級足球聯賽</p></td>
<td><p>11</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年至2010年英格蘭足球冠軍聯賽.md" title="wikilink">2009-10賽季</a>[52]</p></td>
<td><p><a href="../Page/英格蘭冠軍足球聯賽.md" title="wikilink">英格蘭冠軍足球聯賽</a></p></td>
<td><p>44</p></td>
<td><p>17</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010–11賽季英格蘭超級聯賽.md" title="wikilink">2010-11賽季</a>[53]</p></td>
<td><p>英格蘭超級足球聯賽</p></td>
<td><p>30</p></td>
<td><p>12</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
</tr>
<tr class="even">
<td><p>總計</p></td>
<td><p>85</p></td>
<td><p>29</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西汉姆联足球俱乐部.md" title="wikilink">西汉姆联</a></p></td>
<td><p><a href="../Page/2011年至2012年英格蘭足球冠軍聯賽.md" title="wikilink">2011-12賽季</a>[54]</p></td>
<td><p>英格蘭冠軍足球聯賽</p></td>
<td><p>42</p></td>
<td><p>12</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012–13賽季英格蘭超級聯賽.md" title="wikilink">2012-13賽季</a>[55]</p></td>
<td><p>英格蘭超級足球聯賽</p></td>
<td><p>35</p></td>
<td><p>10</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013–14賽季英格蘭超級聯賽.md" title="wikilink">2013-14賽季</a>[56]</p></td>
<td><p>33</p></td>
<td><p>7</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014–15賽季英格蘭超級聯賽.md" title="wikilink">2014-15賽季</a>[57]</p></td>
<td><p>29</p></td>
<td><p>1</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015–16賽季英格蘭超級聯賽.md" title="wikilink">2015-16賽季</a>[58]</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p>總計</p></td>
<td><p>141</p></td>
<td><p>30</p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莱顿东方足球俱乐部.md" title="wikilink">奧連特</a></p></td>
<td><p><a href="../Page/2015年至2016年英格蘭足球乙級聯賽.md" title="wikilink">2015-16賽季</a>[59]</p></td>
<td><p><a href="../Page/英格蘭乙組足球聯賽.md" title="wikilink">英格蘭乙組足球聯賽</a></p></td>
<td><p>14</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/諾士郡足球會.md" title="wikilink">諾士郡</a></p></td>
<td><p>[60]</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td><p>生涯總計</p></td>
<td><p>536</p></td>
<td><p>99</p></td>
<td><p>29</p></td>
<td><p>4</p></td>
<td><p>22</p></td>
<td><p>5</p></td>
<td><p>15</p></td>
</tr>
</tbody>
</table>

## 執教生涯數據

| 球隊                                     | 從          | 至          | 統計                 | 參考資料 |
| -------------------------------------- | ---------- | ---------- | ------------------ | ---- |
| 场次                                     | 胜          | 和          | 負                  | 得勝率％ |
| [奧連特](../Page/莱顿东方足球俱乐部.md "wikilink") | 2016年1月21日 | 2016年4月12日 | \[61\]\[62\]\[63\] |      |
| [諾士郡](../Page/諾士郡足球會.md "wikilink")    | 2017年1月12日 | 現在         | \[64\]             |      |
| 總計                                     | —          |            |                    |      |

## 榮譽

[Kevin_Nolan_Wembley_May_2012.jpg](https://zh.wikipedia.org/wiki/File:Kevin_Nolan_Wembley_May_2012.jpg "fig:Kevin_Nolan_Wembley_May_2012.jpg")

### 球會

  -  保頓

<!-- end list -->

  - [英格蘭甲組聯賽附加賽決賽冠軍](../Page/英格蘭甲組聯賽.md "wikilink")：\[65\]

<!-- end list -->

  -  紐卡素

<!-- end list -->

  - [英格蘭足球冠軍聯賽冠軍](../Page/英格蘭足球冠軍聯賽.md "wikilink")：[2009-10賽季](../Page/2009年至2010年英格蘭足球冠軍聯賽.md "wikilink")\[66\]

<!-- end list -->

  -  韋斯咸

<!-- end list -->

  - [英格蘭足球冠軍聯賽附加賽決賽冠軍](../Page/英格蘭足球冠軍聯賽.md "wikilink")：\[67\]

### 個人

  - [英格蘭超級足球聯賽每月最佳球員](../Page/英格蘭超級足球聯賽.md "wikilink")：2006年2月\[68\]
  - [英格蘭足球冠軍聯賽年度最佳球員](../Page/英格蘭足球聯賽頒獎禮.md "wikilink")：[2009-10賽季](../Page/2009年至2010年英格蘭足球冠軍聯賽.md "wikilink")\[69\]
  - [英格蘭足球冠軍聯賽球迷公選每月最佳球員](../Page/英格蘭足球冠軍聯賽.md "wikilink")：2009年2月\[70\]
  - [PFA年度最佳陣容](../Page/PFA年度最佳陣容.md "wikilink")（英冠）：[2009-10賽季](../Page/2009年至2010年英格蘭足球冠軍聯賽.md "wikilink")\[71\]
  - [英格蘭足球冠軍聯賽每月最佳球員](../Page/英格蘭足球冠軍聯賽.md "wikilink")：2010年4月\[72\]
  - [英格蘭足球冠軍聯賽年度最佳球員](../Page/英格蘭足球聯賽頒獎禮.md "wikilink")（候選）：[2011-12賽季](../Page/2011年至2012年英格蘭足球冠軍聯賽.md "wikilink")\[73\]
  - [英格蘭足球冠軍聯賽年度最佳陣容](../Page/英格蘭足球冠軍聯賽.md "wikilink")：[2011-12賽季](../Page/2011年至2012年英格蘭足球冠軍聯賽.md "wikilink")\[74\]

## 參考資料

## 外部連結

  -
  - [奇雲·路蘭](https://web.archive.org/web/20130127121048/http://www.whufc.com/articles/kevin-nolan-west-ham_2228487_8217)於韋斯咸官方網站上的資料

  - [奇雲·路蘭](https://web.archive.org/web/20100912220306/http://www.nufc.co.uk/page/Teams/PlayerProfile/0%2C%2C10278~8217%2C00.html)於紐卡素官方網站上的資料

  - [奇雲·路蘭](http://www.premierleague.com/page/PlayerProfile/0,,12306~8217,00.html)於英超官方網站上的資料

  - [奇雲·路蘭](http://news.bbc.co.uk/sport1/hi/football/5271162.stm)於[BBC上首個專欄](../Page/BBC.md "wikilink")

[Category:荷蘭裔英格蘭人](../Category/荷蘭裔英格蘭人.md "wikilink")
[Category:愛爾蘭裔英格蘭人](../Category/愛爾蘭裔英格蘭人.md "wikilink")
[Category:英格蘭足球運動員](../Category/英格蘭足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:保頓球員](../Category/保頓球員.md "wikilink")
[Category:紐卡素球員](../Category/紐卡素球員.md "wikilink")
[Category:韋斯咸球員](../Category/韋斯咸球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:奧連特球員](../Category/奧連特球員.md "wikilink")
[Category:諾士郡球員](../Category/諾士郡球員.md "wikilink")
[Category:英格兰足球领队](../Category/英格兰足球领队.md "wikilink")
[Category:奧連特領隊](../Category/奧連特領隊.md "wikilink")
[Category:諾士郡領隊](../Category/諾士郡領隊.md "wikilink")
[Category:英格蘭足球聯賽領隊](../Category/英格蘭足球聯賽領隊.md "wikilink")
[Category:利物浦運動員](../Category/利物浦運動員.md "wikilink")

1.
2.

3.

4.

5.

6.
7.

8.

9.

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.
30.

31.

32.

33.
34.

35.
36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.
52.

53.

54.

55.

56.

57.

58.

59.
60.

61.
62.
63.

64.

65.

66.

67.
68.

69.
70.

71.

72.

73.

74.