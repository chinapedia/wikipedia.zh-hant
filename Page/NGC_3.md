**NGC
3**，是[雙魚座的一個](../Page/雙魚座.md "wikilink")[透镜状星系](../Page/透镜状星系.md "wikilink")。它位於[赤經為](../Page/赤經.md "wikilink")7分33秒，[赤緯為](../Page/赤緯.md "wikilink")8°20′03″。
[紅位移約](../Page/紅位移.md "wikilink") 3900[km](../Page/公里.md "wikilink")／s

## 参见

  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 參考文獻

  -
[NGC 0003](../Category/透鏡狀星系.md "wikilink") [NGC
0003](../Category/雙魚座.md "wikilink")
[0003](../Category/NGC天体.md "wikilink")
[00565](../Category/PGC天体.md "wikilink")
[00058](../Category/UGC天体.md "wikilink")
[Category:双鱼座NGC天体](../Category/双鱼座NGC天体.md "wikilink")