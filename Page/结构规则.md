在[证明论中](../Page/证明论.md "wikilink")，**结构规则**是不提及任何[逻辑连结词的](../Page/逻辑连结词.md "wikilink")[推理规则](../Page/推理规则.md "wikilink")，它直接操作于判断或[相继式](../Page/相继式.md "wikilink")。结构规则通常模仿逻辑的元理论性质。拒绝一个或多个结构规则的逻辑被归类为[亚结构逻辑](../Page/亚结构逻辑.md "wikilink")。

## 常见结构规则

  - **弱化**，这里的[相继式的假设或结论可以扩展到额外的数目](../Page/相继式.md "wikilink")。在符号形式中弱化规则可以写为

\[\frac{\Gamma \vdash \Sigma}{\Gamma, A \vdash \Sigma}\]
在[十字转门的左侧](../Page/十字转门.md "wikilink")，和

\[\frac{\Gamma \vdash \Sigma}{\Gamma \vdash A, \Sigma}\] 在右侧。

  - **紧缩**，这里的在相继式同一侧两个相等的(或可合一的)成员可以替代为单一的一个成员(或公共实例)。符号化为:

\[\frac{\Gamma, A, A \vdash \Sigma}{\Gamma, A \vdash \Sigma}\] 和

\[\frac{\Gamma \vdash A, A, \Sigma}{\Gamma \vdash A, \Sigma}\]。在使用[归结原理的](../Page/归结原理.md "wikilink")[自动定理证明中也叫做](../Page/自动定理证明.md "wikilink")**因子化**。

  - **交换**，这里的在相继式同一侧的两个成员可以对换。符号化为:

\[\frac{\Gamma_1, A, \Gamma_2, B, \Gamma_3 \vdash \Sigma}{\Gamma_1, B, \Gamma_2, A, \Gamma_3 \vdash \Sigma}\]
和

\[\frac{\Gamma \vdash \Sigma_1, A, \Sigma_2, B, \Sigma_3}{\Gamma \vdash \Sigma_1, B, \Sigma_2, A, \Sigma_3}\]。(这也叫做*排列规则*)。

没有任何上述结构规则的逻辑将把相继式解释为纯粹的[序列](../Page/序列.md "wikilink")；带有交换规则它们就是[多重集](../Page/多重集.md "wikilink")；带有紧缩和交换规则二者它们就是[集合](../Page/集合.md "wikilink")。

最著名的结构规则叫做**[切](../Page/切消定理.md "wikilink")**。证明论理论家花了相当的努力来证实切规则在各种逻辑中是多余的。更严格的说，证实了切只是(某种意义上)简化证明的工具，不能增加可以证明的定理。成功消除了切规则叫做**[切消定理](../Page/切消定理.md "wikilink")**，直接有关于规范化[计算](../Page/计算.md "wikilink")(参见[lambda
演算](../Page/lambda_演算.md "wikilink"))的哲学；它经常对给定逻辑的[判定的](../Page/判定.md "wikilink")[复杂性给出好的指示](../Page/复杂性.md "wikilink")。

## 参见

  - [相继式演算](../Page/相继式演算.md "wikilink")
  - [亚结构逻辑](../Page/亚结构逻辑.md "wikilink")
  - [线性逻辑](../Page/线性逻辑.md "wikilink")
  - [仿射逻辑](../Page/仿射逻辑.md "wikilink")
  - [严格逻辑](../Page/严格逻辑.md "wikilink")
  - [有序逻辑](../Page/有序逻辑.md "wikilink")

[Category:证明论](../Category/证明论.md "wikilink")
[Category:推理规则](../Category/推理规则.md "wikilink")