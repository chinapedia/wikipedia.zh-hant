****是历史上第二十三次航天飞机任务，也是[亚特兰蒂斯号航天飞机的第二次太空飞行](../Page/亞特蘭提斯號太空梭.md "wikilink")。

## 任务成员

  - **[布鲁斯特·肖](../Page/布鲁斯特·肖.md "wikilink")**（，曾执行、以及任务），指令长
  - **[布莱恩·欧康诺尔](../Page/布莱恩·欧康诺尔.md "wikilink")**（，曾执行以及任务），飞行员
  - **[玛丽·克利夫](../Page/玛丽·克利夫.md "wikilink")**（，曾执行以及任务），任务专家
  - **[谢伍德·斯普林](../Page/谢伍德·斯普林.md "wikilink")**（，曾执行任务），任务专家
  - **[杰瑞·罗斯](../Page/杰瑞·罗斯.md "wikilink")**（，曾执行、、、、、以及任务），任务专家
  - **[罗道弗·内里·维拉](../Page/罗道弗·内里·维拉.md "wikilink")**（，[墨西哥宇航员](../Page/墨西哥.md "wikilink")，曾执行任务），有效载荷专家
  - **[查尔斯·沃克](../Page/查尔斯·沃克.md "wikilink")**（，曾执行、以及任务），有效载荷专家

[Category:1985年科學](../Category/1985年科學.md "wikilink")
[Category:1985年佛罗里达州](../Category/1985年佛罗里达州.md "wikilink")
[Category:亚特兰蒂斯号航天飞机任务](../Category/亚特兰蒂斯号航天飞机任务.md "wikilink")
[Category:1985年11月](../Category/1985年11月.md "wikilink")
[Category:1985年12月](../Category/1985年12月.md "wikilink")