[Water_drop_animation_enhanced_small.gif](https://zh.wikipedia.org/wiki/File:Water_drop_animation_enhanced_small.gif "fig:Water_drop_animation_enhanced_small.gif")

**水滴**，是小量的[水體](../Page/水.md "wikilink")，能以任何[形態出現](../Page/形態.md "wikilink")。

一支簡單的垂直管狀物體，慢慢流動液體到末端，便形成最容易出現的水滴。當[氣態水份遇上冰冷的物體表面](../Page/氣態.md "wikilink")，[冷凝作用便會發生](../Page/冷凝作用.md "wikilink")，[過度冷卻](../Page/過度冷卻.md "wikilink")[水蒸氣](../Page/水蒸氣.md "wikilink")，霧化成水滴，結[露的現象也是由於冷凝作用的關係](../Page/露.md "wikilink")。

[數學上](../Page/數學.md "wikilink")，垂直管狀物體末端水滴的最大可承受[重量可以此計算](../Page/重量.md "wikilink")：

\(mg = 3 \pi a \lambda \cos \alpha\)

mg ，又即 W ，為水滴的[重量](../Page/重量.md "wikilink")； π
為[圓周率](../Page/圓周率.md "wikilink")； a
為管口[半徑](../Page/半徑.md "wikilink")； λ
為[液體的](../Page/液體.md "wikilink")[表面張力](../Page/表面張力.md "wikilink")；
α
為[接觸角度](../Page/接觸角度.md "wikilink")（液面與剛體接觸點的延伸切線與剛體平面之間的角度）。這個關係是簡單地量度表面張力的基礎，常用於[石油工業](../Page/石油.md "wikilink")。

此外，由於水和[空氣的](../Page/空氣.md "wikilink")[折射率](../Page/折射率.md "wikilink")，[光線](../Page/光線.md "wikilink")[折射和](../Page/折射.md "wikilink")[反射在](../Page/反射.md "wikilink")[雨珠表面](../Page/雨.md "wikilink")，導致[彩虹的形成](../Page/彩虹.md "wikilink")。

### 水滴的連橫圖

<File:Dripping> faucet 1.jpg| <File:Dripping> faucet 2.jpg|
<File:2006-02-13> Drop before impact.jpg| <File:2006-02-13>
Drop-impact.jpg| <File:Strahltropfen.jpg>|

## 參見

  - [冷凝作用](../Page/冷凝作用.md "wikilink")
  - [露水](../Page/露水.md "wikilink")

[ps:څاڅکې](../Page/ps:څاڅکې.md "wikilink")

[Category:水](../Category/水.md "wikilink")