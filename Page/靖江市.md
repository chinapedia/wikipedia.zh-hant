**靖江市**位于[中国](../Page/中国.md "wikilink")[江苏省中南部](../Page/江苏省.md "wikilink")，是[泰州市下辖的一个](../Page/泰州市.md "wikilink")[县级市](../Page/县级市.md "wikilink")。

## 历史

靖江原是[长江中的一片](../Page/长江.md "wikilink")[沙洲](../Page/沙洲.md "wikilink")，[宋代移民垦殖](../Page/宋.md "wikilink")，以后逐渐与北岸连接。[明代由](../Page/明.md "wikilink")[江阴县析置](../Page/江阴县.md "wikilink")，隶属[南直隶](../Page/南直隶.md "wikilink")[常州府](../Page/常州府.md "wikilink")，为常州五邑之一。[清代属于江苏省常州府](../Page/清.md "wikilink")，为常州八邑之一。[民国初年属于江苏省苏常道](../Page/民国.md "wikilink")。

1949年属于苏北行政公署。1952年以后属于扬州地区及[扬州市](../Page/扬州市.md "wikilink")，1993年撤县建市，1996年改由[泰州市代管](../Page/泰州市.md "wikilink")。

## 地理

靖江市位于[长江北岸](../Page/长江.md "wikilink")，隔[长江与](../Page/长江.md "wikilink")[江阴市](../Page/江阴市.md "wikilink")、[张家港市相对](../Page/张家港市.md "wikilink")，北侧毗邻[泰兴市及](../Page/泰兴市.md "wikilink")[如皋市](../Page/如皋市.md "wikilink")。

靖江市地处[长江三角洲](../Page/长江三角洲.md "wikilink")，地势低平，气候四季分明。冬天最冷可达零下10度。

## 行政区划

现辖1个街道办事处，8个镇，2个开发区，3办事处。

  - 街道办事处：靖城街道办事处。
  - 镇：新桥镇、东兴镇、斜桥镇、西来镇、季市镇、孤山镇、生祠镇、马桥镇。
  - 开发区：靖江经济开发区（包括：新港园区、城北园区、城南园区、新桥园区）和[江阴园区](../Page/江阴园区.md "wikilink")
  - 办事处：城南办事处，滨江新区办事处，江阴—靖江工业园区办事处

## 文化

### 方言

靖江大部分地区所说的[靖江话](../Page/靖江话.md "wikilink")（[老岸话](../Page/老岸话.md "wikilink")）属于[吴语](../Page/吴语.md "wikilink")，是长江以北使用吴语的少数几个县市之一。老岸话的使用人口约占七成。西南部沿江的新桥镇、东兴镇、八圩镇说[沙上话](../Page/沙上话.md "wikilink")（与[扬中话基本相同](../Page/扬中话.md "wikilink")）属于[江淮方言](../Page/江淮方言.md "wikilink")，使用人口约占总人口二成。其他一成的人口操[夹沙话](../Page/夹沙话.md "wikilink")、[崇明话](../Page/崇明话.md "wikilink")、[泰兴话](../Page/泰兴话.md "wikilink")、[如皋话等](../Page/如皋话.md "wikilink")。

## 经济

靖江市为[中国百强县之一](../Page/中国百强县.md "wikilink")。

2013年靖江市完成工业开票销售697亿元，同比增长61%；国税入库24.26亿元，同比增长42.8%；工业减、免、退税22亿元，同比增长100.8%；城乡居民人均储蓄24850元，同比增长31.8%；完成GDP364亿元，财政总收入54.14亿元，同比增长30.6%；一般预算收入20.84亿元，同比增长30.1%。主要经济指标五年增长五倍。

靖江市各项经济和社会发展指标目前在江苏省处于第二方阵前列，与江阴、[昆山](../Page/昆山.md "wikilink")、[张家港](../Page/张家港.md "wikilink")、[常熟等第一方阵成员尚有差距](../Page/常熟.md "wikilink")。

## 交通

（[广靖高速公路](../Page/广靖高速公路.md "wikilink")）贯穿靖江市境，经[江阴长江大桥到达](../Page/江阴长江大桥.md "wikilink")[江阴市](../Page/江阴市.md "wikilink")，靖江市境内还有
、等高速公路。主要干道还包括公新公路（城南公所桥-新桥镇）、江平公路（江都-平潮）、姜八公路（姜堰-靖江八圩）、沿江公路（新桥镇-西来镇）等。

[15px](../Page/file:China_Railways.svg.md "wikilink")
[新长铁路](../Page/新长铁路.md "wikilink")（海安-长兴段）经过靖江，通过[火车轮渡过江至江阴](../Page/火车轮渡.md "wikilink")，现阶段只开行货运列车。周边的[火车站包括](../Page/火车站.md "wikilink")[无锡](../Page/无锡.md "wikilink")、[常州](../Page/常州.md "wikilink")、[泰州](../Page/泰州.md "wikilink")、[南通等站](../Page/南通.md "wikilink")。

[Airplane_silhouette.svg](https://zh.wikipedia.org/wiki/File:Airplane_silhouette.svg "fig:Airplane_silhouette.svg")
周边[机场包括上海](../Page/机场.md "wikilink")、南京、无锡、常州、南通、扬州等地的机场。

港口[靖江港](../Page/靖江港.md "wikilink")。

## 特产

  - [猪肉脯](../Page/猪肉脯.md "wikilink")
  - [蟹黄汤包](../Page/蟹黄汤包.md "wikilink")
  - [长江三鲜](../Page/长江三鲜.md "wikilink")
  - [香沙芋](../Page/香沙芋.md "wikilink")
  - [季市老汁鸡](../Page/季市老汁鸡.md "wikilink")
  - [季市荷叶茵糕](../Page/季市荷叶茵糕.md "wikilink")
  - [季市肉圆](../Page/季市肉圆.md "wikilink")
  - [季市大炉饼](../Page/季市大炉饼.md "wikilink")
  - [四墩子羊肉](../Page/四墩子羊肉.md "wikilink")
  - [泥狗子](../Page/泥狗子.md "wikilink")

## 名人

  - [常裕](../Page/常裕.md "wikilink")
  - [戴学江](../Page/戴学江.md "wikilink")
  - [鄂维南](../Page/鄂维南.md "wikilink")
  - [方祖岐](../Page/方祖岐.md "wikilink")
  - [高津](../Page/高津.md "wikilink")
  - [顾功耘](../Page/顾功耘.md "wikilink")
  - [吉星](../Page/吉星.md "wikilink")
  - [刘守仁](../Page/刘守仁.md "wikilink")
  - [刘秀梵](../Page/刘秀梵.md "wikilink")
  - [刘宜良](../Page/刘宜良.md "wikilink")
  - [刘国钧](../Page/刘国钧.md "wikilink")
  - [刘云圻](../Page/刘云圻.md "wikilink")
  - [刘季幸](../Page/刘季幸.md "wikilink")
  - [卢锡城](../Page/卢锡城.md "wikilink")

<!-- end list -->

  - [毛林坤](../Page/毛林坤.md "wikilink")
  - [倪同芳](../Page/倪同芳.md "wikilink")
  - [盛金章](../Page/盛金章.md "wikilink")
  - [宋祖德](../Page/宋祖德.md "wikilink")
  - [陶驷驹](../Page/陶驷驹.md "wikilink")
  - [汪海粟](../Page/汪海粟.md "wikilink")
  - [张启先](../Page/张启先.md "wikilink")
  - [翟婉明](../Page/翟婉明.md "wikilink")
  - [朱杰 (演员)](../Page/朱杰_\(演员\).md "wikilink")

## 名胜

  - 孤山
  - 岳庙
  - 四眼井
  - 魁星阁
  - 刘国均故居

## 外部链接

  - [靖江市政府门户网站](http://www.jingjiang.gov.cn/)
  - [中国靖江网](https://web.archive.org/web/20100106112025/http://www.jj.js.cn/)
  - [靖江人才网](http://www.0523zp.com)

[市](../Page/category:泰州区市.md "wikilink")
[靖江市](../Page/category:靖江市.md "wikilink")

[泰州](../Category/江苏县级市.md "wikilink")
[苏](../Category/中国中等城市.md "wikilink")