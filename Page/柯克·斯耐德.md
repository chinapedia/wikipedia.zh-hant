**柯克·帕特里克·斯耐德**（，）生于[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[洛杉矶](../Page/洛杉矶.md "wikilink")，美国职业篮球运动员，司职[小前锋](../Page/小前锋.md "wikilink")。

## 大学时期

斯耐德在他的大学篮球时期是在[内华达大学度过的](../Page/内华达大学.md "wikilink")。他带领大学群狼篮球队打入了NCAA一级联赛的16强
(Sweet
16)。在这一系列赛中，内华达大学先后击败了2号种子[冈再伽大学和](../Page/冈再伽大学.md "wikilink")7号种子球队[密歇根州立大学](../Page/密歇根州立大学.md "wikilink")，但是在第三轮中被[乔治亚理工大学所淘汰](../Page/乔治亚理工大学.md "wikilink")。2005年球队在击败[德克薩斯州大學奧斯汀分校后再次晋级锦标赛第二轮](../Page/德克薩斯州大學奧斯汀分校.md "wikilink")，但这一次球队输给了头号顺位的伊利诺伊大学。

## NBA时期

他在[2004年NBA选秀中](../Page/2004年NBA选秀.md "wikilink")，第一轮第16顺位被[犹他爵士选中](../Page/犹他爵士.md "wikilink")，一个赛季后斯耐德在NBA历史上最大规模的交易中被交易去了[新奥尔良黄蜂](../Page/新奥尔良黄蜂.md "wikilink")。在黄蜂的斯耐德成为了球队的主力球员，在和[明尼苏达森林狼队的比赛中拿下了个人职业生涯最高的](../Page/明尼苏达森林狼队.md "wikilink")28分。在黄蜂度过了一个出色的赛季后，他再次被交易，黄蜂用他和[休斯敦火箭交换来了](../Page/休斯敦火箭.md "wikilink")2008赛季的第二轮选秀权以及现金补贴。\[1\]

## 参考资料

## 外部链接

  - [NBA官方网站资料](http://www.nba.com/playerfile/kirk_snyder/) at NBA.com

  - [斯耐德资料统计](http://www.basketball-reference.com/players/s/snydeki01.html)
    at Basketball-Reference.com

[Category:洛杉矶人](../Category/洛杉矶人.md "wikilink")
[Category:非洲裔美国篮球运动员](../Category/非洲裔美国篮球运动员.md "wikilink")
[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:得分后卫](../Category/得分后卫.md "wikilink")
[Category:小前锋](../Category/小前锋.md "wikilink")
[Category:休斯頓火箭隊球員](../Category/休斯頓火箭隊球員.md "wikilink")
[Category:犹他爵士队球员](../Category/犹他爵士队球员.md "wikilink")
[Category:新奥尔良黄蜂队球员](../Category/新奥尔良黄蜂队球员.md "wikilink")
[Category:明尼苏达森林狼队球员](../Category/明尼苏达森林狼队球员.md "wikilink")
[Category:中国篮球联赛外援](../Category/中国篮球联赛外援.md "wikilink")

1.  <http://www.clutchfans.net/news/1346/kirk_snyder_acquired_from_hornets/>