## [9月29日](../Page/9月29日.md "wikilink")

  - [台灣](../Page/台灣.md "wikilink")[台北捷運綠線的](../Page/松山新店線.md "wikilink")[七張](../Page/七張站.md "wikilink")－[小碧潭路段已於本日完工通車](../Page/小碧潭站.md "wikilink")。

## [9月28日](../Page/9月28日.md "wikilink")

  - [Google新闻中国版对新闻来源进行自我审查](../Page/Google.md "wikilink")，Google称此举出于无奈。[CNN（英文）](https://web.archive.org/web/20041012083719/http://www.cnn.com/2004/TECH/internet/09/27/google.china.ap/)

## [9月27日](../Page/9月27日.md "wikilink")

  - 在[印度旅游胜地](../Page/印度.md "wikilink")[泰姬陵前的](../Page/泰姬陵.md "wikilink")[亚穆纳河畔](../Page/亚穆纳河.md "wikilink")，两名演员扮演成印度[莫卧儿王朝第五代皇帝](../Page/莫卧儿王朝.md "wikilink")[沙贾汗与其爱妻](../Page/沙贾汗.md "wikilink")[泰姬·玛哈尔](../Page/泰姬·玛哈尔.md "wikilink")。当天，印度举行活动庆祝泰姬陵350岁生日。[华夏经纬网](http://www.huaxia.com/xw/ksj/00247254.html)
  - [日本首相](../Page/日本.md "wikilink")[小泉纯一郎改组内阁](../Page/小泉纯一郎.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2004-09-28/11073790580s.shtml)
  - [泰国泰国东北部一名](../Page/泰国.md "wikilink")36岁的妇女感染了[禽流感病毒](../Page/禽流感.md "wikilink")。这是自今年7月禽流感复发以来泰国确认的第二起人类感染病例。[新华网](http://news.xinhuanet.com/world/2004-09/27/content_2029464.htm)

## [9月26日](../Page/9月26日.md "wikilink")

  - [统一圣战组织发表声明称](../Page/统一圣战组织.md "wikilink")，他们已将被绑架的[英国工程师](../Page/英国.md "wikilink")[肯尼思·比格利斩首](../Page/肯尼思·比格利.md "wikilink")，同时新抓获了7名英军士兵作为人质。但英军表示未获士兵失踪的报告，也称未收到人质下落的准确报道。[大公报](http://202.83.203.133:8080/news/2004-9-26/YM-310590.htm)
  - [中共中央军委会举行晋升上将仪式](../Page/中共中央军委会.md "wikilink")，[张定发](../Page/张定发.md "wikilink")、[靖志远两人晋升](../Page/靖志远.md "wikilink")[上将军衔](../Page/上将.md "wikilink")。[新浪网](http://jczs.news.sina.com.cn/2004-09-25/2150230379.html)
  - [台湾](../Page/台湾.md "wikilink")“[反6108军购联盟](../Page/反6108军购联盟.md "wikilink")”和“[民主行动联盟](../Page/民主行动联盟.md "wikilink")”发动万人反军购大游行活动，[许信良](../Page/许信良.md "wikilink")、[陈文茜](../Page/陈文茜.md "wikilink")、[宋楚瑜](../Page/宋楚瑜.md "wikilink")、[侯孝贤](../Page/侯孝贤.md "wikilink")、[李敖等人参加并发表演说](../Page/李敖.md "wikilink")，反对[中华民国政府即将推动的价值](../Page/中华民国.md "wikilink")6108亿台币的军购案。[台湾苹果日报](https://web.archive.org/web/20041024062652/http://appledaily.com.tw/template/twapple/art_main.cfm?loc=TP)[联合报](http://udn.com/NEWS/TOPIC/TOP3/2261853.shtml)[自立晚报](http://www.idn.com.tw/article_content.php?catid=1&catsid=1&catdid=2&artid=20040925andy001)[东森](http://www.ettoday.com/2004/09/25/301-1690948.htm)[新华社](http://news.xinhuanet.com/taiwan/2004-09/25/content_2022530.htm)

## [9月25日](../Page/9月25日.md "wikilink")

  - 2004中国曲阜国际孔子文化节在孔子故里[山东](../Page/山东.md "wikilink")[曲阜开幕](../Page/曲阜.md "wikilink")，纪念[孔子诞辰](../Page/孔子.md "wikilink")2555年。[央视国际](http://www.cctv.com/news/entertainment/20040927/100519.shtml)

## [9月24日](../Page/9月24日.md "wikilink")

  - [美国国务卿](../Page/美国.md "wikilink")[鲍威尔表示关注有关](../Page/鲍威尔.md "wikilink")[朝鲜试射导弹的传闻](../Page/朝鲜.md "wikilink")，但重申不会改变美国以外交手段解决[朝鲜问题的立场](../Page/朝鲜.md "wikilink")。[中新社](http://www.chinanews.com.cn/news/2004/2004-09-25/26/487958.shtml)
  - [中国国务院总理](../Page/中国国务院.md "wikilink")[温家宝抵达](../Page/温家宝.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")，开始其上任后首次对[俄罗斯的访问](../Page/俄罗斯.md "wikilink")。在与俄罗斯联邦总理[弗拉德科夫举行的联合记者招待会上](../Page/弗拉德科夫.md "wikilink")，两国总理强调将加强两国在[能源领域的合作](../Page/能源.md "wikilink")，俄罗斯将在未来增加对华的[石油出口](../Page/石油.md "wikilink")，并合作进行[天然气的开发](../Page/天然气.md "wikilink")。两国总理也草签了俄罗斯加入[世界贸易组织的双边协议](../Page/世界贸易组织.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2004-09-25/09233767375s.shtml)[扬子晚报](https://web.archive.org/web/20041015034054/http://www.yangtse.com/gb/content/2004-09/25/content_711096.htm)[香港文汇报](https://web.archive.org/web/20040925161754/http://www.wenweipo.com/news.phtml?news_id=IN0409250015&cat=000IN)
  - [一级方程式赛车中国上海赛道正式启用](../Page/一级方程式赛车.md "wikilink")，各车队已经完成了两轮的练习赛。这是F1首次在中国设赛站。[文新传媒](https://web.archive.org/web/20040928102145/http://www.news365.com.cn/xwzx/jrgz/t20040925_230593.htm)
  - [英国又有一名闯入者进入女王](../Page/英国.md "wikilink")[伊丽莎白二世在](../Page/伊丽莎白二世.md "wikilink")[苏格兰首府](../Page/苏格兰.md "wikilink")[爱丁堡的王宫](../Page/爱丁堡.md "wikilink")，并在事后成功逃逸。[大公报](http://202.83.203.133:8080/news/2004-9-25/YM-309992.htm)
  - [埃及外长在](../Page/埃及.md "wikilink")[联合国大会发言](../Page/联合国大会.md "wikilink")，要求安理会应该增加一个代表[非洲的常任理事国名额](../Page/非洲.md "wikilink")，并认为埃及能够担任此任务。此前就[德国外长要求成为安理会常任理事国的发言](../Page/德国.md "wikilink")，[意大利外长表示强烈反对](../Page/意大利.md "wikilink")。[新华社](http://news.xinhuanet.com/world/2004-09/25/content_2019845.htm)[联合早报](http://www.zaobao.com/gj/gj504_250904.html)

## [9月23日](../Page/9月23日.md "wikilink")

  - 第五届[中国金鹰电视艺术节在](../Page/中国金鹰电视艺术节.md "wikilink")[长沙召开](../Page/长沙.md "wikilink")，[中国全国人大](../Page/中华人民共和国全国人民代表大会.md "wikilink")[常委会副](../Page/中华人民共和国全国人民代表大会常务委员会.md "wikilink")[委员长](../Page/中华人民共和国全国人民代表大会常务委员会委员长.md "wikilink")[司马仪·艾买提宣布开幕](../Page/司马仪·艾买提.md "wikilink")。
  - [上海合作组织成员国总理第三次会议发表了联合公报](../Page/上海合作组织.md "wikilink")，强调应对安全威胁重要性。[1](http://www.livingchina.cn住在中国房产网)
  - 名列台湾“十大通缉要犯”的黑帮首领[薛球和](../Page/薛球.md "wikilink")[陈益华昨天从中国大陆遣返回台湾](../Page/陈益华.md "wikilink")，准备接受审讯。[东森](http://www.ettoday.com/2004/09/24/138-1690348.htm)
  - [英国政府提出](../Page/英国.md "wikilink")[联合国安理会改革的具体方案](../Page/联合国安理会.md "wikilink")，建议安理会成员国从现有的15国扩大到24国，并吸纳[日本](../Page/日本.md "wikilink")、[德国](../Page/德国.md "wikilink")、[巴西和](../Page/巴西.md "wikilink")[印度为新的常任理事国](../Page/印度.md "wikilink")。[中央电视台](https://web.archive.org/web/20041127063731/http://202.108.249.200/news/world/20040924/101244.shtml)
  - [伊拉克过渡政府总理](../Page/伊拉克.md "wikilink")[阿拉维在](../Page/阿拉维.md "wikilink")[美国国会发表演说](../Page/美国.md "wikilink")，感谢美国政府推翻[萨达姆政权](../Page/萨达姆.md "wikilink")，并再次重申将在明年1月举行大选。[凤凰网](http://www.phoenixtv.com/home/news/world/200409/24/331853.html)
  - [西班牙极右翼政党再次发动游行](../Page/西班牙.md "wikilink")，抗议华商对西班牙本地鞋业造成的激烈竞争。西班牙之前部分地区已经发生了排华暴动事件。[2](http://www.livingchina.cn住在中国网)
  - [美國](../Page/美國.md "wikilink")[圣海伦斯火山喷发](../Page/圣海伦斯火山.md "wikilink")。

## [9月22日](../Page/9月22日.md "wikilink")

  - [中国与](../Page/中国.md "wikilink")[东盟](../Page/东盟.md "wikilink")10国专家在[苏州召开反恐研讨会](../Page/苏州.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2004-09/23/content_2012668.htm)
  - [中央电视台新台址建设工程开工](../Page/中央电视台.md "wikilink")。

<https://web.archive.org/web/20040827045910/http://www.livingchina.cn/>

## [9月21日](../Page/9月21日.md "wikilink")

  - [聯合國秘書長](../Page/聯合國.md "wikilink")[科菲·安南批評美用武力手段向](../Page/科菲·安南.md "wikilink")[伊拉克輸送](../Page/伊拉克.md "wikilink")[民主](../Page/民主.md "wikilink")。[解放軍報](https://web.archive.org/web/20040925163050/http://www.pladaily.com.cn/big5/pladaily/2004/09/22/20040922001243_zhxw.html)
  - [海地水災死亡人數超過](../Page/海地.md "wikilink")700人。[大公報](https://web.archive.org/web/20040923040630/http://www.takungpao.com/news/2004-9-22/YM-308889.htm)
  - [中国开始向符合要求的外国人发放](../Page/中国.md "wikilink")《[外国人永久居留证](../Page/外国人永久居留证.md "wikilink")》。[国际在线](https://web.archive.org/web/20040924001720/http://gb.chinabroadcast.cn/3821/2004/09/22/146@307450.htm)
  - [2008年奥运会的](../Page/2008年夏季奥林匹克运动会.md "wikilink")“倒计时钟”在位于[北京](../Page/北京.md "wikilink")[天安门广场东的](../Page/天安门广场.md "wikilink")[中国国家博物馆前正式启动](../Page/中国国家博物馆.md "wikilink")。[湖北日报](http://www.cnhubei.com/200409/ca569147.htm)
  - [中国首都各界隆重庆祝](../Page/中国.md "wikilink")[中国人民政治协商会议成立](../Page/中国人民政治协商会议.md "wikilink")55周年。[新华网](http://news.xinhuanet.com/newscenter/2004-09/21/content_2001751.htm)
  - [日本首相](../Page/日本.md "wikilink")[小泉纯一郎在](../Page/小泉纯一郎.md "wikilink")[联合国大会发言时提出](../Page/联合国大会.md "wikilink")，希望联合国能够让日本成为[联合国安理会](../Page/联合国安理会.md "wikilink")[常任理事国](../Page/常任理事国.md "wikilink")；此外日本还与[德国](../Page/德国.md "wikilink")、[印度和](../Page/印度.md "wikilink")[巴西组成了一个](../Page/巴西.md "wikilink")4国游说团，在争取成为安理会常任理事国方面互相合作。[新华社](http://news.xinhuanet.com/world/2004-09/22/content_2006417.htm)
  - 美国[哥伦比亚广播公司](../Page/哥伦比亚广播公司.md "wikilink")（CBS）就美国总统[布什服役记录的失实报道公开道歉](../Page/乔治·沃克·布什.md "wikilink")。CBS声称，受到了文件来源的“误导”。[新华社](http://news.xinhuanet.com/newmedia/2004-09/22/content_2005562.htm)
  - 台湾奥运跆拳道银牌得主[黃志雄获得](../Page/黃志雄.md "wikilink")[国民党提名](../Page/国民党.md "wikilink")，被列入其不分区立委安全名单，此外一名[新党提名的候选人也被国民党列为不分区立委安全名单之列](../Page/台湾新党.md "wikilink")，体现泛蓝团结。[台湾日报](https://web.archive.org/web/20040923094130/http://www.taiwandaily.com.tw/news.php?news_id=28849)
  - 一家伊斯蘭網站發佈的消息說，上周在伊拉克被綁架的3名西方人中的另一名美國人質已經被斬首，但未提及人质姓名。[大公网](https://web.archive.org/web/20050210123744/http://www.takungpao.com/news/2004-9-22/YM-308862.htm)

## [9月20日](../Page/9月20日.md "wikilink")

  - 《[濒危野生动植物种国际贸易公约](../Page/濒危野生动植物种国际贸易公约.md "wikilink")》（CITES）下属的两家机构最新公布的调查结果表明，每年四千[大象死于](../Page/大象.md "wikilink")[象牙交易](../Page/象牙.md "wikilink")。[新浪网](http://news.sina.com.cn/w/2004-09-21/09263725567s.shtml)
  - [印尼大选第二轮投票开始](../Page/2004年印度尼西亚总统选举.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-09/20/content_1996396.htm)
  - 俄罗斯[尤科斯石油公司因财务问题宣布把部分循陆路输往中国的](../Page/尤科斯石油公司.md "wikilink")[石油暂停付运](../Page/石油.md "wikilink")，导致国际石油价格重回每桶46美元的高位。[3](http://www.livingchina.cn住在中国)
  - 美国总统[布什与民主党总统候选人](../Page/乔治·沃克·布什.md "wikilink")[克里的竞选班子宣布](../Page/克里.md "wikilink")，两人将在11月大选前举行3场电视辩论会，第一场将在9月30日举行。此外，两党的副总统候选人也将在10月5日举行一场辩论会。[新华社](http://news.xinhuanet.com/world/2004-09/21/content_2001829.htm)[BBC](http://news.bbc.co.uk/chinese/simp/hi/newsid_3670000/newsid_3675000/3675010.stm)
  - 11名台湾[中央研究院院士连署一封公开信](../Page/中央研究院.md "wikilink")，反对政府计划的6108亿军购案，并呼吁民众参加于周六举行的反军购游行。[东森](http://www.ettoday.com/2004/09/20/301-1688356.htm)
  - 第56届美国[艾美奖揭晓](../Page/艾美奖.md "wikilink")，《[黑道家族](../Page/黑道家族.md "wikilink")》、《[欲望城市](../Page/欲望城市.md "wikilink")》等获奖。[台湾苹果日报](https://web.archive.org/web/20050424195226/http://www.appledaily.com.tw/template/twapple/art_main.cfm?loc=TP&showdate=20040921&sec_id=9&art_id=1248318)
  - [维基百科完成第](../Page/维基百科.md "wikilink")
    \[<http://meta.wikimedia.org/wiki/Translation_requests/PR-1mil/Zh>:
    100万\] 篇文章。

## [9月19日](../Page/9月19日.md "wikilink")

  - [十六届四中全会同意](../Page/中国共产党第十六届中央委员会第四次全体会议.md "wikilink")[江泽民辞去中共](../Page/江泽民.md "wikilink")[中央军事委员会主席职务](../Page/中央军事委员会.md "wikilink")，决定[胡锦涛任中共中央军事委员会主席](../Page/胡锦涛.md "wikilink")。[联合早报](http://www.zaobao.com/special/newspapers/2004/09/others200904z.html)

## [9月17日](../Page/9月17日.md "wikilink")

  - 一[中國漁船遭中共公安開火兩人落海失蹤](../Page/中國.md "wikilink")，四漁民逃[金門被捕兩傷者送醫](../Page/金門.md "wikilink")
    [中央社](https://web.archive.org/web/20041126003233/http://www.cna.com.tw/perl/cipread_mp.cgi?id=200409180088)
  - [第12屆残奥会在](../Page/2004年残疾人奥林匹克运动会.md "wikilink")[希臘](../Page/希臘.md "wikilink")[雅典开幕](../Page/雅典.md "wikilink")。

## [9月16日](../Page/9月16日.md "wikilink")

  - 第22届[中国电视金鹰奖揭晓](../Page/中国电视金鹰奖.md "wikilink")。[新华网](http://news.xinhuanet.com/ent/2004-09/17/content_1992369.htm)
  - [欧盟国防部长会议决定](../Page/欧盟.md "wikilink")[2004年内成立](../Page/2004年.md "wikilink")[欧盟宪兵部队](../Page/欧盟宪兵部队.md "wikilink")。[新华网](http://news.xinhuanet.com/mil/2004-09/17/content_1992065.htm)
  - [十六届四中全会开幕](../Page/中国共产党第十六届中央委员会第四次全体会议.md "wikilink")。
  - [丹麦王子](../Page/丹麦.md "wikilink")[约阿希姆和](../Page/约阿希姆.md "wikilink")[香港裔的王妃](../Page/香港.md "wikilink")[文雅丽宣布分居](../Page/文雅丽.md "wikilink")。[丹麦不少人民都指責婚姻破裂是王子過份任性做成](../Page/丹麦.md "wikilink")。
  - [飓风伊万席卷](../Page/飓风伊万.md "wikilink")[加勒比海和](../Page/加勒比海.md "wikilink")[美国多个地区](../Page/美国.md "wikilink")，造成至少103人死亡。

## [9月15日](../Page/9月15日.md "wikilink")

  - [台灣知名性文化研究者](../Page/台灣.md "wikilink")[何春蕤所主持的一個](../Page/何春蕤.md "wikilink")[動物戀學術研究網站被起訴散佈猥褻物品及公然猥褻案](../Page/兽交.md "wikilink")，第一審原獲判無罪，經檢方提起[上訴](../Page/上訴.md "wikilink")，[台灣高等法院今日作出第二審判決](../Page/台灣高等法院.md "wikilink")，維持無罪的決定，全案[定讞不得上訴第三審](../Page/定讞.md "wikilink")。[聯合新聞網](http://udn.com/NEWS/NATIONAL/NAT2/2244078.shtml)
  - [Activision宣布旗下](../Page/Activision.md "wikilink")[Gray
    Matter制作的二战](../Page/Gray_Matter.md "wikilink")[动作射击大作](../Page/动作射击.md "wikilink")《[使命召唤：联合进攻](../Page/使命召唤：联合进攻.md "wikilink")》今日于北美上市。[太平洋游戏网](http://games.sina.com.cn/newgames/2004/09/091547878.shtml)
  - [中国首都各界隆重纪念](../Page/中国.md "wikilink")[全国人民代表大会成立五十周年](../Page/全国人民代表大会.md "wikilink")。[新华网](http://news.xinhuanet.com/newscenter/2004-09/15/content_1984577.htm)

## [9月14日](../Page/9月14日.md "wikilink")

  - 由[澳大利亞和](../Page/澳大利亞.md "wikilink")[美國](../Page/美國.md "wikilink")[疆獨組織合組的](../Page/疆獨組織.md "wikilink")[東突厥斯坦流亡政府在美國](../Page/東突厥斯坦流亡政府.md "wikilink")[華盛頓成立](../Page/华盛顿哥伦比亚特区.md "wikilink")。[聯合新聞網](http://udn.com/NEWS/WORLD/WOR1/2248169.shtml)
  - [英国首相](../Page/英国.md "wikilink")[布莱尔发表讲话](../Page/布莱尔.md "wikilink")，指出[全球变暖已经成为迫切需要解决的国际问题](../Page/全球变暖.md "wikilink")。他呼吁[八国集团对此采取统一立场](../Page/八国集团.md "wikilink")，保证全球进步与发展。
    [新华网](http://news.xinhuanet.com/st/2004-09/15/content_1984756.htm)
  - [乌克兰建议](../Page/乌克兰.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")、[白俄罗斯](../Page/白俄罗斯.md "wikilink")、[哈萨克斯坦同乌克兰开展航天合作](../Page/哈萨克斯坦.md "wikilink")，共同开发多次往返式载人飞船——“[快速帆船](../Page/快速帆船.md "wikilink")”。
    [新华网](http://news.xinhuanet.com/st/2004-09/15/content_1985097.htm)
  - [第59届联合国大会在](../Page/第59届联合国大会.md "wikilink")[纽约](../Page/纽约.md "wikilink")[联合国总部开幕](../Page/联合国.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-09/15/content_1983201.htm)
  - [東突厥斯坦流亡政府在美國](../Page/東突厥斯坦流亡政府.md "wikilink")[華盛頓成立](../Page/华盛顿哥伦比亚特区.md "wikilink")。

## [9月13日](../Page/9月13日.md "wikilink")

  - [普京公布了](../Page/普京.md "wikilink")[俄罗斯国家权力体系的新一轮改革计划](../Page/俄罗斯.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-09/15/content_1984203.htm)
  - [越南北部](../Page/越南.md "wikilink")[老街省发生的](../Page/老街省.md "wikilink")[山体滑坡至少造成](../Page/山体滑坡.md "wikilink")12人死亡，14人失踪。[新华网](http://news.xinhuanet.com/st/2004-09/15/content_1985132.htm)
  - [英国](../Page/英国.md "wikilink")[伦敦一名示威者打扮成](../Page/伦敦.md "wikilink")[蝙蝠侠闯入](../Page/蝙蝠侠.md "wikilink")[白金汉宫示威](../Page/白金汉宫.md "wikilink")。[解放军报](https://web.archive.org/web/20040916020045/http://www.pladaily.com.cn/gb/pladaily/2004/09/14/20040914001352_zhxw.html)
  - [中国在](../Page/中国.md "wikilink")[量子化霍尔电阻基准研究方面获重大突破](../Page/量子化霍尔电阻基准.md "wikilink")。[人民日报](https://web.archive.org/web/20040916033400/http://www.people.com.cn/GB/paper464/12937/1162713.html)
  - [美國國務卿](../Page/美國.md "wikilink")[鮑威爾說](../Page/鮑威爾.md "wikilink")，美國不大可能在[伊拉克境內找到任何](../Page/伊拉克.md "wikilink")[大規模殺傷性武器](../Page/大規模殺傷性武器.md "wikilink")。[多維新聞網](https://web.archive.org/web/20050410200005/http://www5.chinesenewsnet.com/MainNews/Topics/xhw_2004_09_13_20_55_40_439.html)
  - [巴勒斯坦民族解放运动](../Page/巴勒斯坦民族解放运动.md "wikilink")（[法塔赫](../Page/法塔赫.md "wikilink")）下属军事派别“[阿克萨烈士旅](../Page/阿克萨烈士旅.md "wikilink")”命令[约旦河西岸城市](../Page/约旦河.md "wikilink")[杰宁一所私立](../Page/杰宁.md "wikilink")[大学关闭](../Page/大学.md "wikilink")。该组织声称，此举是为抗议[巴勒斯坦安全部门的干涉](../Page/巴勒斯坦.md "wikilink")。
    [新华网](http://news.xinhuanet.com/world/2004-09/14/content_1979242.htm)
  - [一塌糊涂BBS被关闭](../Page/一塌糊涂BBS.md "wikilink")，并且[中国大陆的各大](../Page/中国大陆.md "wikilink")[BBS相继发布公告](../Page/BBS.md "wikilink")，禁止讨论此事。

## [9月12日](../Page/9月12日.md "wikilink")

  - [達賴喇嘛四名特使訪問](../Page/達賴喇嘛.md "wikilink")[北京](../Page/北京.md "wikilink")。[多維新聞網](https://web.archive.org/web/20050410200503/http://www5.chinesenewsnet.com/MainNews/SinoNews/Mainland/cna_2004_09_13_22_28_07_680.html)
  - [泰国新增](../Page/泰国.md "wikilink")4名[禽流感疑似患者](../Page/禽流感.md "wikilink")，其中3名是儿童。[央视国际](http://www.cctv.com/news/world/20040913/100079.shtml)
  - [朝鲜国庆](../Page/朝鲜.md "wikilink")56周年近[中国边界发生巨大爆炸](../Page/中国.md "wikilink")
    [MSNBC（英文）](http://msnbc.msn.com/id/5973861/)
  - [2004年香港立法會選舉剛剛結束](../Page/2004年香港立法會選舉.md "wikilink")。投票人數為歷界最高，而投票率亦超過55%。[BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_3640000/newsid_3648700/3648752.stm)

## [9月11日](../Page/9月11日.md "wikilink")

  - [威尼斯国际电影节在](../Page/威尼斯国际电影节.md "wikilink")[意大利著名水城](../Page/意大利.md "wikilink")[威尼斯落下帷幕](../Page/威尼斯.md "wikilink")，[英国导演](../Page/英国.md "wikilink")[迈克·雷凭借](../Page/迈克·雷.md "wikilink")《[维拉－德雷克](../Page/维拉－德雷克.md "wikilink")》捧走了本届电影节的最高奖项金狮奖。[新华网](http://news.xinhuanet.com/ent/2004-09/01/content_1934642.htm)
  - [美国纪念](../Page/美国.md "wikilink")[九一一事件三周年](../Page/九一一事件.md "wikilink")。[ABC（英文）](http://abcnews.go.com/Sept11_2004/index.html)

## [9月10日](../Page/9月10日.md "wikilink")

  - [中国网球公开赛开打](../Page/中国网球公开赛.md "wikilink")。[重庆商报](http://www.chinacqsb.com/show_article.php?art_id=55987&art_cate_id=85&today=2004-09-10&menu_date=2004-09-10)

## [9月9日](../Page/9月9日.md "wikilink")

  - [俄罗斯](../Page/俄罗斯.md "wikilink")[北极-33浮冰漂流站正式开始运转](../Page/北极-33浮冰漂流站.md "wikilink")。[新华网](http://news.xinhuanet.com/st/2004-09/10/content_1965768.htm)
  - [韩国科技部承认](../Page/韩国科技部.md "wikilink")，几名韩国科学家曾在[1982年](../Page/1982年.md "wikilink")4月至5月间成功进行了提取[钚的实验](../Page/钚.md "wikilink")，但韩国政府当时并未向[国际原子能机构报告此事](../Page/国际原子能机构.md "wikilink")。[汉网](https://web.archive.org/web/20070929093034/http://www.cnhan.com/gb/content/2004-09/10/content_383174.htm)
  - [文莱王儲迎娶](../Page/文莱.md "wikilink")17歲平民少女大典隆重舉行。[中國經濟網](https://web.archive.org/web/20041114043506/http://big5.ce.cn/xwzx/gjss/gdxw/200409/10/t20040910_1727398.shtml)
  - [中国同时将两颗](../Page/中国.md "wikilink")[实践六号空间环境探测卫星成功送入](../Page/实践六号空间环境探测卫星.md "wikilink")[太空](../Page/太空.md "wikilink")。[光明日报](http://www.gmw.cn/01gmrb/2004-09/10/content_97365.htm)
  - 位於[印尼](../Page/印尼.md "wikilink")[雅加達的](../Page/雅加達.md "wikilink")[澳大利亞大使館遭汽車炸彈攻擊](../Page/澳大利亞.md "wikilink")，至少一百人以上傷亡，這起事件被懷疑是[恐怖攻擊事件](../Page/恐怖攻擊.md "wikilink")。[聯合新聞網](http://udn.com/NEWS/WORLD/WOR3/2232382.shtml)
  - [Google推出中文版的新闻服务](../Page/Google.md "wikilink")。

## [9月8日](../Page/9月8日.md "wikilink")

  - [英国首相](../Page/英国.md "wikilink")[布莱尔宣布改组内阁](../Page/布莱尔.md "wikilink")。[大公报](http://202.83.203.133:8080/news/2004-9-9/YM-303761.htm)
  - [俄罗斯联邦安全局宣布](../Page/俄罗斯联邦安全局.md "wikilink")，悬赏3亿[卢布](../Page/卢布.md "wikilink")（约合1000万[美元](../Page/美元.md "wikilink")）捉拿[别斯兰人质危机幕后黑手](../Page/别斯兰人质危机.md "wikilink")——[车臣非法武装头目](../Page/车臣非法武装.md "wikilink")[马斯哈多夫和](../Page/马斯哈多夫.md "wikilink")[巴萨耶夫](../Page/巴萨耶夫.md "wikilink")。[南方日报](http://www.nanfangdaily.com.cn/southnews/dd/nfrb/A09/200409090133.asp)
  - [美国未能成功在空中拦截](../Page/美国.md "wikilink")[起源号太空船](../Page/起源号太空船.md "wikilink")，坠落在[犹他州的沙漠上](../Page/犹他州.md "wikilink")。[上海热线](https://web.archive.org/web/20040911014045/http://news.online.sh.cn/news/gb/content/2004-09/09/content_957066.htm)
  - 世界最大的[太阳能发电站在](../Page/太阳能发电站.md "wikilink")[德国](../Page/德国.md "wikilink")[莱比锡附近的](../Page/莱比锡.md "wikilink")[埃斯彭海因投入使用](../Page/埃斯彭海因.md "wikilink")。[新华网](http://news.xinhuanet.com/st/2004-09/09/content_1960457.htm)
  - [俄罗斯](../Page/俄罗斯.md "wikilink")[北奥塞梯共和国总统宣布](../Page/北奥塞梯.md "wikilink")，北奥塞梯政府将在两天内集体辞职，而未能防范[别斯兰人质危机的相关责任人也将受到惩处](../Page/别斯兰人质危机.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-09/09/content_1960187.htm)

## [9月7日](../Page/9月7日.md "wikilink")

  - [法国计划](../Page/法国.md "wikilink")[2007年前成为](../Page/2007年.md "wikilink")[欧洲最大的](../Page/欧洲.md "wikilink")[生物燃料生产国](../Page/生物燃料.md "wikilink")。[新华网](http://news.xinhuanet.com/st/2004-09/08/content_1954208.htm)
  - [加蓬共和国总统](../Page/加蓬.md "wikilink")[哈吉·奥马尔·邦戈·翁丁巴抵达](../Page/哈吉·奥马尔·邦戈·翁丁巴.md "wikilink")[北京](../Page/北京.md "wikilink")，开始对[中国进行](../Page/中国.md "wikilink")[国事访问](../Page/国事访问.md "wikilink")。[新华网](https://web.archive.org/web/20041026191508/http://world.ynet.com/view.jsp?oid=3740175)
  - [2004年阿富汗总统选举拉开帷幕](../Page/2004年阿富汗总统选举.md "wikilink")。[新华网](https://web.archive.org/web/20041020013245/http://news.tom.com/1003/20040907-1289260.html)
  - [美國最新](../Page/美國.md "wikilink")[考古研究發現](../Page/考古.md "wikilink")，數個新近從[加州和](../Page/加州.md "wikilink")[墨西哥出土的人頭骨](../Page/墨西哥.md "wikilink")，可以追遡至約13000年前，比現時已知來自[西伯利亞的移民早約數千年到達](../Page/西伯利亞.md "wikilink")[美洲](../Page/美洲.md "wikilink")。研究員根據頭骨的[DNA推斷](../Page/DNA.md "wikilink")，這一批移民乃是[澳洲原住民](../Page/澳洲原住民.md "wikilink")，並經過[日本或](../Page/日本.md "wikilink")[波里尼西亞群島](../Page/波里尼西亞群島.md "wikilink")，以海路到達美洲，推翻了西伯利亞是美洲最早期居民的論據。[MSNBC](http://www.msnbc.msn.com/id/5927028/)

## [9月6日](../Page/9月6日.md "wikilink")

  - [香港国际机场突起](../Page/香港國際機場.md "wikilink")[龙卷风](../Page/龙卷风.md "wikilink")。[人民网](http://news.sohu.com/20040907/n221918414.shtml)
  - [叠湖短期形成后迅速消失](../Page/叠湖.md "wikilink")，[罗布泊中又见水波荡漾](../Page/罗布泊.md "wikilink")。[新疆天山网](http://www.tianshannet.com/GB/channel3/17/200409/06/109430.html)
  - [新疆全力抢救](../Page/新疆.md "wikilink")[哈萨克族民间古典音乐](../Page/哈萨克族.md "wikilink")[六十二阔恩尔](../Page/六十二阔恩尔.md "wikilink")。[新华网](http://news.xinhuanet.com/ent/2004-09/06/content_1950091.htm)
  - 中国[南京军区](../Page/南京军区.md "wikilink")[福州总医院在世界上首次发现](../Page/福州总医院.md "wikilink")4个[人类](../Page/人类.md "wikilink")[肾上腺](../Page/肾上腺.md "wikilink")[脑白质营养不良相关](../Page/肾上腺脑白质.md "wikilink")[基因突变](../Page/基因突变.md "wikilink")[新华网](http://news.xinhuanet.com/st/2004-09/06/content_1950887.htm)
  - [9月2日以来](../Page/9月2日.md "wikilink")，[四川和](../Page/四川.md "wikilink")[重庆遭遇罕见](../Page/重庆.md "wikilink")[洪灾](../Page/洪灾.md "wikilink")。[四川因](../Page/四川.md "wikilink")[暴雨](../Page/暴雨.md "wikilink")[死亡人数升至](../Page/死亡.md "wikilink")55人，失踪人数增至52人。[4](http://www.chinanews.com/news/2004/2004-09-06/26/480784.shtml)，[重庆](../Page/重庆.md "wikilink")21人丧生，27人失踪[5](http://news.xinhuanet.com/newscenter/2004-09/06/content_1949398.htm)[中国新闻网](http://www.chinanews.com/news/2004/2004-09-06/26/481062.shtml)
  - [凤凰卫视前副主席](../Page/凤凰卫视.md "wikilink")[周一男灭门案今日于深圳开审](../Page/周一男.md "wikilink")[中国新闻网](http://www.chinanews.com/news/2004/2004-09-06/26/480802.shtml)
  - [中国奥运金牌代表团访问](../Page/2004年夏季奥林匹克运动会中国代表团.md "wikilink")[香港](../Page/香港.md "wikilink")[中国新闻网](http://www.chinanews.com/news/2004/2004-09-06/26/480927.shtml)
  - [俄罗斯](../Page/俄罗斯.md "wikilink")[北奥塞梯别斯兰人质事件劫持人质统计上升至](../Page/别斯兰人质危机.md "wikilink")1181人、人质[死亡上升至](../Page/死亡.md "wikilink")335人，俄联邦全国下半旗致哀[6](http://www.chinanews.com.cn/news/2004/2004-09-05/26/480643.shtml)[中国新闻网](http://www.chinanews.com.cn/news/2004/2004-09-06/26/481030.shtml)

## [9月5日](../Page/9月5日.md "wikilink")

  - [中国](../Page/中国.md "wikilink")[国家食品药品监督管理局公布](../Page/中华人民共和国食品药品监督管理局.md "wikilink")19种劣质[药品](../Page/药品.md "wikilink")[新华网](http://news.xinhuanet.com/zhengfu/2004-09/06/content_1950495.htm)

## [9月4日](../Page/9月4日.md "wikilink")

  - [俄罗斯](../Page/俄罗斯.md "wikilink")[北奥赛梯人质危机人质死亡人数上升到](../Page/别斯兰人质危机.md "wikilink")322人，[绑匪全部被击毙](../Page/绑匪.md "wikilink")
    [新华网](http://news.xinhuanet.com/world/2004-09/04/content_1944548.htm)
  - [飓风佛朗西斯袭击美国](../Page/飓风佛朗西斯.md "wikilink")[佛罗里达州](../Page/佛罗里达州.md "wikilink")、[乔治亚州](../Page/乔治亚州.md "wikilink")、[南卡罗莱纳州以及](../Page/南卡罗莱纳州.md "wikilink")[巴哈马](../Page/巴哈马.md "wikilink")，造成15人死亡。

## [9月3日](../Page/9月3日.md "wikilink")

  - [俄罗斯特种部队展开强攻解救人质](../Page/俄罗斯.md "wikilink")，尸体令人惨不忍睹
    [sina](http://news.sina.com.cn/w/2004-09-03/20363577164s.shtml)
  - [美国前总统](../Page/美国.md "wikilink")[比尔·克林顿将在](../Page/比尔·克林顿.md "wikilink")[纽约做](../Page/纽约.md "wikilink")[心脏](../Page/心脏.md "wikilink")[手术](../Page/手术.md "wikilink")。[ABC](http://abcnews.go.com/wire/US/ap20040903_1605.html)
  - 第三届[亚洲政党国际会议在](../Page/亚洲政党国际会议.md "wikilink")[北京开幕](../Page/北京.md "wikilink")。[7](http://news.xinhuanet.com/newscenter/2004-09/03/content_1940582.htm)

## [9月2日](../Page/9月2日.md "wikilink")

  - [联合国难民署计划今年年底前从](../Page/联合国难民署.md "wikilink")[科特迪瓦](../Page/科特迪瓦.md "wikilink")、[几内亚等国遣返](../Page/几内亚.md "wikilink")５万名[利比里亚难民](../Page/利比里亚难民.md "wikilink")。[新华网](http://news.xinhuanet.com/world/2004-09/03/content_1940461.htm)
  - [联合国安理会要求](../Page/联合国安理会.md "wikilink")[叙利亚尊重](../Page/叙利亚.md "wikilink")[黎巴嫩的政治](../Page/黎巴嫩.md "wikilink")[独立](../Page/独立.md "wikilink")，并立即从该国撤出所有军队。[中国政府认为此事不影响国际和平](../Page/中国.md "wikilink")，是两国之间的事，投了弃权票。
    [新华网](http://news.xinhuanet.com/world/2004-09/03/content_1940305.htm)
  - [葡萄牙总理表示](../Page/葡萄牙.md "wikilink")，政府不打算修改[堕胎法](../Page/堕胎法.md "wikilink")，也不准备就是否取消[堕胎法进行](../Page/堕胎.md "wikilink")[全民公决](../Page/全民公决.md "wikilink")，禁止堕胎的法令将一直实施到2006年本届政府结束。[新华网](http://news.xinhuanet.com/world/2004-09/03/content_1940701.htm)
  - [美国共和党全国代表大会开幕](../Page/美国共和党全国代表大会.md "wikilink")，正式提名[小布什为共和党总统候选人](../Page/小布什.md "wikilink")。

## [9月1日](../Page/9月1日.md "wikilink")

  - 第61届[威尼斯电影节开幕](../Page/威尼斯电影节.md "wikilink")。[8](http://news.xinhuanet.com/ent/2004-09/01/content_1934642.htm)
  - [伊拉克临时国民议会宣誓就职](../Page/伊拉克临时国民议会.md "wikilink")。[京华时报](https://web.archive.org/web/20040902123839/http://www.bjt.net.cn/news.asp?newsid=76136)
  - [中国的](../Page/中国.md "wikilink")[藏羚羊种群数量呈增长态势](../Page/藏羚.md "wikilink")
    至少达到10万只。[光明日报](http://news.xinhuanet.com/newscenter/2004-09/01/content_1934066.htm)
  - [2004年中国互联网大会幕](../Page/2004年中国互联网大会.md "wikilink")。
  - [俄罗斯南部](../Page/俄罗斯.md "wikilink")[北奥塞梯共和国别斯兰市第一中学发生](../Page/北奥塞梯.md "wikilink")[劫持人质事件](../Page/别斯兰人质危机.md "wikilink")，被劫持人数超过1000人[新华网](http://news.xinhuanet.com/world/2004-09/03/content_1943059.htm)
    [中国新闻网](http://www.chinanews.com/special/2004-09-01/351.shtml)

[Category:2004年9月](../Category/2004年9月.md "wikilink")