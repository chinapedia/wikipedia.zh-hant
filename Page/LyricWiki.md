***LyricWiki***是一個純[歌詞](../Page/歌詞.md "wikilink")、[資料庫導向的](../Page/資料庫.md "wikilink")[網站](../Page/網站.md "wikilink")，使用[MediaWiki](../Page/MediaWiki.md "wikilink")[維基伺服器](../Page/維基.md "wikilink")。2008年6月13日的資料指出，它是全世界第七大的維基。擁有超過
670,000
個頁面。（資料來源[Wikimedia](../Page/Wikimedia.md "wikilink")）\[1\]2009年8月，該站併入[wikia](../Page/wikia.md "wikilink")。

這個網站允許外部程式，經由[SOAP](../Page/SOAP.md "wikilink") [Web
Service](../Page/Web_Service.md "wikilink")\[2\]存取他們資料庫的資料。這個[API已經被許多](../Page/API.md "wikilink")[影音](../Page/影音.md "wikilink")[媒體播放器的](../Page/媒體播放器.md "wikilink")[外掛廣泛使用](../Page/外掛.md "wikilink")，包括
[Winamp](../Page/Winamp.md "wikilink")、[Amarok](../Page/Amarok_\(audio\).md "wikilink")、[Windows
Media
Player](../Page/Windows_Media_Player.md "wikilink")、[musikCube](../Page/musikCube.md "wikilink")、[foobar2000等等](../Page/foobar2000.md "wikilink")。\[3\]

LyricWiki
最近也釋出了[Facebook的應用程式](../Page/Facebook.md "wikilink")，叫做「LyricWiki
Challenge」，是一個社交、競技的遊戲，可以用來猜測、辨認各式各樣曲風，熱門歌曲的歌詞。\[4\]

## 參考資料

<references/>

## 外部連結

  -
  - (网站的新地址)

[Category:線上音樂](../Category/線上音樂.md "wikilink")
[Category:歌詞資料庫](../Category/歌詞資料庫.md "wikilink")
[Category:自由網站](../Category/自由網站.md "wikilink")
[Category:Wiki社群](../Category/Wiki社群.md "wikilink")
[Category:Wikia](../Category/Wikia.md "wikilink")

1.
2.  [LyricWiki API](http://www.programmableweb.com/api/lyricwiki)
3.  [LyricWiki:Plugins](http://www.lyricwiki.org/LyricWiki:Plugins)
4.