**盘盘国**是3世纪-7世纪时[马来半岛的一个古代国家](../Page/马来半岛.md "wikilink")，其地理位置在马来半岛东岸，暹罗湾附近，與[狼牙脩国接壤](../Page/狼牙脩.md "wikilink")，北接[占城国](../Page/占城.md "wikilink")、堕和罗国，东南接[哥罗国](../Page/屈都昆.md "wikilink")。自[交州海行四十日可以到达](../Page/交州.md "wikilink")。\[1\]

盘盘国王名叫杨粟圬，也称崑崙（kurung），大王之意。大臣称为勃郎索滥。国中有佛寺、有印度教庙，人皆学婆罗门书。

[梁武帝](../Page/梁武帝.md "wikilink")[中大通元年](../Page/中大通.md "wikilink")（529年）12月、四年（533年）4月、五年（534年）秋，六年（535年）8月，盘盘国四度遣使献方物。\[2\]

[南朝陈](../Page/南朝陈.md "wikilink")[太建三年](../Page/太建.md "wikilink")（571年）5月，盘盘国遣使献方物。\[3\]

[隋朝](../Page/隋朝.md "wikilink")[大业十二年](../Page/大业.md "wikilink")（616年），[丹丹](../Page/丹丹.md "wikilink")、盘盘二国，来贡方物。\[4\]

[唐朝](../Page/唐朝.md "wikilink")[贞观九年](../Page/贞观.md "wikilink")（637年），遣使来朝，贡方物。\[5\]

## 参考文献

<div class="references-small">

<references />

</div>

  - 陈贵荣 谢方 《古代南海地名汇释》 “盘盘” 703 页 中华书局 1986 统一书号 11018 1326
  - [许云樵著](../Page/许云樵.md "wikilink")
    《[南洋史](../Page/南洋史.md "wikilink")》上卷
    盘盘、个罗、与[狼牙修](../Page/狼牙修.md "wikilink") 第158页
    星洲世界书局 1961

[category:马来半岛](../Page/category:马来半岛.md "wikilink")

[Category:东南亚古国](../Category/东南亚古国.md "wikilink")
[Category:中外交通史地名](../Category/中外交通史地名.md "wikilink")
[Category:马来西亚历史政权](../Category/马来西亚历史政权.md "wikilink")
[Category:已不存在的亞洲國家](../Category/已不存在的亞洲國家.md "wikilink")
[Category:已不存在的亞洲君主國](../Category/已不存在的亞洲君主國.md "wikilink")

1.  《[旧唐书](../Page/旧唐书.md "wikilink")·盘盘》
2.  《[梁书](../Page/梁书.md "wikilink")·武帝下》
3.  《[陈书](../Page/陈书.md "wikilink")·宣帝》
4.  《[隋书](../Page/隋书.md "wikilink")·婆利》
5.  《[旧唐书](../Page/旧唐书.md "wikilink")·盘盘》