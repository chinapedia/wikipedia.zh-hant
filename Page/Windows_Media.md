**Windows Media**是[Microsoft
Windows下媒体创建和分发所用的](../Page/Microsoft_Windows.md "wikilink")[多媒体框架](../Page/多媒体框架.md "wikilink")。它包含一个带有[应用程序编程接口的](../Page/应用程序编程接口.md "wikilink")[软件开发工具包以及一些预构建技术](../Page/软件开发工具包.md "wikilink")。以下是Windows
Media的组成部分：

## 应用程序

  - [Windows Media Encoder](../Page/Windows_Media_Encoder.md "wikilink")
  - [Windows Media Player](../Page/Windows_Media_Player.md "wikilink")
  - [Windows Media
    Services](../Page/Windows_Media_Services.md "wikilink")

## 格式

  - [Advanced Systems
    Format](../Page/Advanced_Systems_Format.md "wikilink")（ASF）
  - [Windows Media Audio](../Page/WMA.md "wikilink")（WMA）
  - [HD Photo](../Page/HD_Photo.md "wikilink")（WDP/HDP）
  - [Windows Media Video](../Page/WMV.md "wikilink")（WMV）

## 其他

  - [Microsoft Media
    Services](../Page/MMS（协议）.md "wikilink")（MMS），流媒体传输协议
  - [Windows Media
    DRM](../Page/Windows_Media_DRM.md "wikilink")，[数字版权管理的一种实现](../Page/数字版权管理.md "wikilink")
  - [WMVHD](../Page/WMVHD.md "wikilink")，Windows Media Video High
    Definition
  - [Windows XP Media Center
    Edition](../Page/Windows_XP_Media_Center_Edition.md "wikilink")，内含[Media
    Center软件的](../Page/Media_Center.md "wikilink")[Windows
    XP或](../Page/Windows_XP.md "wikilink")[Windows
    Vista](../Page/Windows_Vista.md "wikilink")。

## 外部链接

  - [微软官方Windows Media站点](http://www.microsoft.com/windowsmedia)
  - [Windows Media Intelligent
    Streaming](http://www.microsoft.com/windows/windowsmedia/howto/articles/intstreaming.aspx)（or
    Intellistream）
  - [Windows Media Lite](http://finalbuilds.edskes.com/#wmlite)
  - [Windows Media
    Community](http://forums.techarena.in/forumdisplay.php?f=150)
  - [Description of the algorithm used for WMA
    encryption](https://web.archive.org/web/20040305101021/http://www.spinnaker.com/crypt/drm/freeme/Technical)

[Category:Windows多媒体](../Category/Windows多媒体.md "wikilink")
[Category:多媒体框架](../Category/多媒体框架.md "wikilink")