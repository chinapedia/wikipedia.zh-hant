[HK_Sai_Ying_Pun_Chiu_Kwong_Street_Cross-border_Bus_a.jpg](https://zh.wikipedia.org/wiki/File:HK_Sai_Ying_Pun_Chiu_Kwong_Street_Cross-border_Bus_a.jpg "fig:HK_Sai_Ying_Pun_Chiu_Kwong_Street_Cross-border_Bus_a.jpg")的一個[香港与](../Page/香港.md "wikilink")[内地间跨境巴士的](../Page/内地.md "wikilink")[車站](../Page/車站.md "wikilink")\]\]
[Greyhound_Lines_86025b.JPG](https://zh.wikipedia.org/wiki/File:Greyhound_Lines_86025b.JPG "fig:Greyhound_Lines_86025b.JPG")\]\]
**跨境巴士**，是指往來兩個不同[國家或地區的](../Page/國家.md "wikilink")[巴士服務](../Page/巴士.md "wikilink")，它們運輸乘客跨越[邊境出入兩地](../Page/邊境.md "wikilink")，是站對站客流服務，所以又名**過境巴士**、**直通巴**、**跨境快線**、**跨境公交车**等。而在香港及澳門的跨境巴士，由於港澳和大陸的車行方向不同，故跨境巴士需適應兩邊不同的行車方向，且須考取特許駕照方可營運。

## 粤港跨境巴士

粤港跨境巴士主要往来[香港和](../Page/香港.md "wikilink")[广东省各地](../Page/广东省.md "wikilink")。[香港的跨境巴士使用](../Page/香港.md "wikilink")[皇崗口岸](../Page/皇崗口岸.md "wikilink")、[文錦渡口岸](../Page/文錦渡口岸.md "wikilink")、[沙頭角口岸](../Page/沙頭角口岸.md "wikilink")、[深圳灣口岸及](../Page/深圳灣口岸.md "wikilink")[港珠澳大橋往返香港](../Page/港珠澳大橋.md "wikilink")、[澳門及](../Page/澳門.md "wikilink")[中國内地](../Page/中國内地.md "wikilink")。在[皇崗口岸除了有往來](../Page/皇崗口岸.md "wikilink")[落馬洲與皇崗的](../Page/落馬洲.md "wikilink")[皇巴士外](../Page/皇巴士.md "wikilink")，還有直通跨境巴士往來皇崗口岸與[元朗](../Page/元朗.md "wikilink")[錦上路站](../Page/錦上路站.md "wikilink")、[灣仔碼頭](../Page/灣仔碼頭.md "wikilink")、[旺角](../Page/旺角.md "wikilink")（[跨境全日通](../Page/跨境全日通.md "wikilink")）、[荃灣站](../Page/荃灣站.md "wikilink")（[中港直通快線](../Page/中港直通快線.md "wikilink")）、[觀塘](../Page/觀塘.md "wikilink")[藍田站及](../Page/藍田站.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")[柯士甸道](../Page/柯士甸道.md "wikilink")（[24小時跨境快線](../Page/24小時跨境快線.md "wikilink")）。除此之外，還有一些由香港出發經皇崗、[深圳灣口岸](../Page/深圳灣口岸.md "wikilink")、[文錦渡及](../Page/文錦渡.md "wikilink")[沙頭角前往](../Page/沙頭角.md "wikilink")[廣州](../Page/廣州.md "wikilink")、[中山](../Page/中山市.md "wikilink")、[佛山](../Page/佛山.md "wikilink")、[新會](../Page/新會.md "wikilink")、[順德等地的長途巴士](../Page/順德.md "wikilink")。另外，乘搭跨境巴士到海关後，乘客需要携带行李全部下车并步行通过海关和移民检查，过关后再重新登车前往目的地。

而香港主要的跨境旅遊客車服務經營商包括：

  - [香港中旅汽車服務](../Page/香港中旅汽車服務.md "wikilink")
  - [環島旅運](../Page/環島旅運.md "wikilink")
  - [永東直通巴士](../Page/永東直通巴士.md "wikilink")
  - [粵港汽車](../Page/粵港汽車.md "wikilink")
  - [中港通](../Page/中港通.md "wikilink")
  - [廣通巴士](../Page/廣通巴士.md "wikilink")
  - [華通巴士](../Page/華通巴士.md "wikilink")
  - [港澳一號](../Page/港澳一號.md "wikilink")
  - [港通汽車](../Page/港通汽車.md "wikilink")（左軚限定）
  - [通榮達](../Page/通榮達.md "wikilink")
  - [中信國際](../Page/中信國際.md "wikilink")
  - [深華粵海](../Page/深華粵海.md "wikilink")
  - [東僑客運](../Page/東僑客運.md "wikilink")
  - [深港榮利](../Page/深港榮利.md "wikilink")
  - [威盛運輸](../Page/威盛運輸.md "wikilink")
  - [穗港客車](../Page/穗港客車.md "wikilink")
  - [沙港巴士](../Page/沙港巴士.md "wikilink")
  - [金旅客運](../Page/金旅客運.md "wikilink")
  - [湛港旅運](../Page/湛港旅運.md "wikilink")
  - [香港國旅](../Page/香港國旅.md "wikilink")（左軚限定）
  - [華閩汽車](../Page/華閩汽車.md "wikilink")

## 澳門的跨境巴士

在[澳門](../Page/澳門.md "wikilink")，亦有提供跨境巴士往來[澳門](../Page/澳門.md "wikilink")、[香港與](../Page/香港.md "wikilink")[中國內地](../Page/中國內地.md "wikilink")，其中[澳巴與](../Page/澳巴.md "wikilink")[岐關車路聯合開辦](../Page/岐關車路.md "wikilink")[蓮花口岸專線巴士](../Page/蓮花口岸.md "wikilink")，由[路氹邊檢大樓前往](../Page/路氹邊檢大樓.md "wikilink")[珠海](../Page/珠海.md "wikilink")[橫琴口岸](../Page/橫琴口岸.md "wikilink")，收費澳門幣5元，每日24小時提供服務。另外，中旅也有經營關閘邊檢大樓至[富豪酒店的路線](../Page/澳門富豪酒店.md "wikilink")。另外，乘搭跨境巴士到海关後，乘客需要携带行李全部下车并步行通过海关和移民检查，过关后再重新登车前往目的地。

而澳門主要的跨境旅遊客車服務經營商包括：

  - [香港中旅汽車服務](../Page/香港中旅汽車服務.md "wikilink")
  - [岐關車路](../Page/岐關車路.md "wikilink")
  - [信德國旅](../Page/信德國旅.md "wikilink")

## 新加坡跨境巴士

[新加坡的](../Page/新加坡.md "wikilink")[新捷运營辦](../Page/新捷运.md "wikilink")160及170路巴士線，往來新加坡與[馬來西亞](../Page/馬來西亞.md "wikilink")[新山](../Page/新山.md "wikilink")，每日清晨至晚間服務。乘客可以使用[坡幣及](../Page/新加坡元.md "wikilink")[馬幣繳費](../Page/令吉.md "wikilink")。另有跨境的[星柔快車服務](../Page/星柔快車.md "wikilink")。

除此之外，新加坡亦有跨境長途巴士前往[吉隆坡](../Page/吉隆坡.md "wikilink")。

## 中朝跨境巴士

[中國](../Page/中國.md "wikilink")[辽宁省](../Page/辽宁省.md "wikilink")[丹東的](../Page/丹東.md "wikilink")301路公交車，往來[丹东口岸与](../Page/丹东.md "wikilink")[新义州火车站](../Page/新义州.md "wikilink")，每天8：30及13：30发两班车。

## 中越跨境巴士

[中國](../Page/中國.md "wikilink")[廣西有多條往返](../Page/廣西.md "wikilink")[越南的客運班車](../Page/越南.md "wikilink")，途經[友誼關](../Page/友誼關.md "wikilink")、[龍邦口岸前](../Page/龍邦.md "wikilink")[越南](../Page/越南.md "wikilink")。路線：[南寧至](../Page/南寧.md "wikilink")[越南](../Page/越南.md "wikilink")[河內](../Page/河內.md "wikilink")，[南宁至](../Page/南宁.md "wikilink")[越南](../Page/越南.md "wikilink")[海防](../Page/海防.md "wikilink")、[桂林至](../Page/桂林.md "wikilink")[越南](../Page/越南.md "wikilink")[河内](../Page/河内.md "wikilink")、[崇左至](../Page/崇左.md "wikilink")[越南](../Page/越南.md "wikilink")[下龙](../Page/下龙.md "wikilink")、[百色至](../Page/百色.md "wikilink")[越南](../Page/越南.md "wikilink")[高平](../Page/高平.md "wikilink")。
除此之外，[中國](../Page/中國.md "wikilink")[雲南省](../Page/雲南省.md "wikilink")[昆明往返](../Page/昆明.md "wikilink")[越南](../Page/越南.md "wikilink")[沙巴的客運已有計劃開行](../Page/沙巴.md "wikilink")，途經[天保口岸出境](../Page/天保口岸.md "wikilink")。

## 中俄跨境巴士

[中國](../Page/中國.md "wikilink")[黑龍江省](../Page/黑龍江省.md "wikilink")[哈爾濱有長途大巴往返](../Page/哈爾濱.md "wikilink")[俄羅斯远东](../Page/俄羅斯远东.md "wikilink")[海参崴](../Page/海参崴.md "wikilink")，全程約12小時。

## 中哈跨境巴士

[中國](../Page/中國.md "wikilink")[新疆](../Page/新疆.md "wikilink")[烏魯木齊有長途大巴往返](../Page/烏魯木齊.md "wikilink")[哈薩克斯坦](../Page/哈薩克斯坦.md "wikilink")[阿拉木圖](../Page/阿拉木圖.md "wikilink")，途經[霍尔果斯口岸](../Page/霍尔果斯口岸.md "wikilink")。

## 北美洲和墨西哥跨境巴士

[灰狗巴士](../Page/灰狗巴士.md "wikilink")，Tufesa，TapRoyal提供跨越美墨两国的跨境巴士。另外，乘搭跨境巴士到海关後，乘客需要携带行李全部下车并步行通过海关和移民检查，过关后再重新登车前往目的地。

## 歐洲跨境巴士

由於歐洲大陸大部分地區皆為[申根區範圍](../Page/申根區.md "wikilink")，跨境巴士可在申根區各國內暢行無阻，且乘客與司機無須接受護照檢查。跨欧洲主要是[Eurolines和](../Page/欧洲之线巴士.md "wikilink")[FlixBus](../Page/弗利克斯巴士.md "wikilink")。

巴士車資在[歐元區內通常以](../Page/歐元區.md "wikilink")[歐元計價](../Page/歐元.md "wikilink")，另外在非歐元區可使用[瑞士法郎](../Page/瑞士法郎.md "wikilink")、[丹麥克朗](../Page/丹麥克朗.md "wikilink")、[捷克克朗](../Page/捷克克朗.md "wikilink")、[瑞典克朗](../Page/瑞典克朗.md "wikilink")、和[英镑等歐元以外貨幣](../Page/英镑.md "wikilink")，视巴士駛經的國家而定。

## 外部連結

  - [中港跨境巴士查詢及票務（Go巴出行提供）](https://www.GoByBus.hk)

[Category:巴士](../Category/巴士.md "wikilink")