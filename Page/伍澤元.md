<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>臺灣省立屏東師範學校畢業</li>
<li>中央警官學校刑事警察學系畢業<br />
<span style="color: blue;">(1971年)</span></li>
<li>國立交通大學運輸工程研究所工學碩士<br />
<span style="color: blue;">(1979年)</span></li>
<li>中國文化大學實業計畫研究所工學博士<br />
<span style="color: blue;">(1986年)</span></li>
<li>英國倫敦大學住宅建設專題研究班結業</li>
<li>國立臺灣大學土木工程研究所研究</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>臺北市政府警察局刑警大隊刑事組組長<br />
<span style="color: blue;">(1980年)</span></li>
<li>內政部營建署國宅組組長<br />
<span style="color: blue;">(1981年-1984年)</span></li>
<li>臺灣省政府副秘書長<br />
<span style="color: blue;">(1984年-1988年)</span></li>
<li>臺灣省政府住宅及都市發展局局長<br />
<span style="color: blue;">(1988年-1993年1月1日)</span></li>
<li>國民大會（第三屆）代表<br />
<span style="color: blue;">(1992年-1996年)</span></li>
<li>臺灣省政府交通處處長<br />
<span style="color: blue;">(1993年1月1日-1993年12月20日)</span></li>
<li>屏東縣政府（第十二屆）縣長<br />
<span style="color: blue;">(1993年12月20日-1996年10月14日)</span></li>
<li>立法院（第四屆）委員<br />
<span style="color: blue;">(1999年-2002年)</span></li>
<li>內政部警政署研究員</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

**伍澤元**（），[台灣政治人物](../Page/台灣.md "wikilink")，前[屏東縣縣長](../Page/屏東縣縣長.md "wikilink")。胞弟[伍錦霖為](../Page/伍錦霖.md "wikilink")[考試院院長](../Page/考試院院長.md "wikilink")。妹夫徐安旋為[臺灣菸酒公司董事長](../Page/臺灣菸酒公司.md "wikilink")。

[日治時期生於](../Page/台灣日治時期.md "wikilink")[屏東縣](../Page/屏東縣.md "wikilink")[萬丹鄉](../Page/萬丹鄉.md "wikilink")。為[中國文化大學實業計畫研究所](../Page/中國文化大學.md "wikilink")[工學博士](../Page/工學博士.md "wikilink")，曾任[中](../Page/中學.md "wikilink")[小學](../Page/小學.md "wikilink")[教師](../Page/教師.md "wikilink")、內政部營建署國宅組組長、[台灣省政府副秘書長](../Page/台灣省政府.md "wikilink")、[台灣省政府住都局局長](../Page/台灣省政府.md "wikilink")、[屏東縣](../Page/屏東縣.md "wikilink")[縣長](../Page/屏東縣縣長.md "wikilink")、立法委員等職。1980年當選[中華民國十大傑出青年](../Page/中華民國十大傑出青年.md "wikilink")，2001年因涉「四汴頭弊案」與「八里污水廠弊案」棄保潛逃至中國大陸，2003年被[行政院列為十大通緝要犯](../Page/行政院.md "wikilink")\[1\]。2008年9月22日，因[糖尿病](../Page/糖尿病.md "wikilink")[併發症病死於](../Page/併發症.md "wikilink")[上海](../Page/上海.md "wikilink")。

## 生平

伍澤元於[臺灣省立屏東師範學校畢業](../Page/國立屏東教育大學.md "wikilink")，1971年於[中央警官學校刑事警察學系畢業](../Page/中央警官學校.md "wikilink")，1979年於[國立交通大學運輸工程研究所取得工學碩士學位](../Page/國立交通大學.md "wikilink")，1986年於[中國文化大學實業計畫研究所取得工學博士](../Page/中國文化大學.md "wikilink")。

1993年，時任[台灣省政府交通處處長的伍澤元被](../Page/台灣省政府.md "wikilink")[中國國民黨推舉與爭取連任的](../Page/中國國民黨.md "wikilink")[民主進步黨籍](../Page/民主進步黨.md "wikilink")[屏東縣長](../Page/屏東縣.md "wikilink")[蘇貞昌競選](../Page/蘇貞昌.md "wikilink")，最終伍澤元以一萬兩千票的差距當選。成為民主化後首位國民黨籍縣長。

1997年，爆發台北縣板橋市[四汴頭抽水站弊案](../Page/四汴頭抽水站弊案.md "wikilink")，伍澤元因涉嫌於台灣省政府住都局長期間，圖利特定廠商浮編工程預算三億餘元,收受賄款[新臺幣](../Page/新臺幣.md "wikilink")600萬元\[2\]，依[貪污罪起訴](../Page/貪污罪.md "wikilink")，一審判[無期徒刑](../Page/無期徒刑.md "wikilink")，二審改判十五年，不久以「病重須保外就醫」為由，獲以新臺幣360萬元保釋就醫。1998年，伍於保外期間，積極投入選戰，當選[第四屆立法委員](../Page/1998年中華民國立法委員選舉.md "wikilink")\[3\]，因而取得任內的「司法免訴權」。最高法院後將原案發回高院更一審\[4\]。

1998年4月，[監察院調查發現時任](../Page/監察院.md "wikilink")[副總統](../Page/中華民國副總統.md "wikilink")[連戰借款新臺幣](../Page/連戰.md "wikilink")3,628萬元供伍澤元用作競選經費，卻未依法在財產申報中登記，連戰因此被裁定須罰款30萬元\[5\]。

有鑒於伍澤元利用保外就醫當選立委，亂及司法制度，2000年1月15日[立法院三讀通過俗稱](../Page/立法院.md "wikilink")「保外就醫回籠條款」（又稱作「伍澤元條款」）的《刑事訴訟法》第116條之2與第117條第1項第4、5款，以限制[受刑人保外就醫期間](../Page/受刑人.md "wikilink")，不得從事醫療以外其他不當活動，違者將遭取消保外\[6\]。

2001年3月，台灣[板橋地檢署偵查](../Page/板橋地檢署.md "wikilink")[八里污水廠蛋形消化槽工程弊案終結](../Page/八里污水廠蛋形消化槽工程弊案.md "wikilink")，伍澤元被控於任台灣省政府住都局長時涉嫌以浮編工程預算方式圖利特定廠商達新臺幣十四億元，被依「經辦公用工程共同舞弊罪」、「違背職務收受賄賂罪」起訴，求處十五年徒刑、褫奪公權七年\[7\]。同年12月，伍澤元競選立委連任失敗，隨即以立委出國考察為由潛逃至[中國大陸](../Page/中國大陸.md "wikilink")\[8\]。2002年被[台灣高等法院發布通緝](../Page/台灣高等法院.md "wikilink")，2003年起被[行政院列為台灣十大通緝要犯](../Page/中華民國行政院.md "wikilink")\[9\]。

## 参考文献

{{-}}   |- |colspan="3"
style="text-align:center;"|**[Emblem_of_Pingtung_County.svg](https://zh.wikipedia.org/wiki/File:Emblem_of_Pingtung_County.svg "fig:Emblem_of_Pingtung_County.svg")[屏東縣政府](../Page/屏東縣政府.md "wikilink")**
|-

[分類:任內離職的臺灣地方首長](../Page/分類:任內離職的臺灣地方首長.md "wikilink")

[W伍](../Category/潛逃出境的台灣通緝犯.md "wikilink")
[W伍](../Category/臺灣貪污犯.md "wikilink")
[Category:屏東縣縣長](../Category/屏東縣縣長.md "wikilink")
[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[W伍](../Category/國立交通大學校友.md "wikilink")
[W伍](../Category/中國文化大學校友.md "wikilink")
[W伍](../Category/中央警察大學校友.md "wikilink")
[Category:萬丹人](../Category/萬丹人.md "wikilink")
[T澤](../Category/伍姓.md "wikilink")
[Category:國立屏東教育大學校友](../Category/國立屏東教育大學校友.md "wikilink")
[Category:被開除中國國民黨黨籍者](../Category/被開除中國國民黨黨籍者.md "wikilink")

1.  [內政部警政署刑事警察局-重要(緊急)查緝專案 - 重大案件通緝犯 -
    伍澤元](http://www.cib.gov.tw/Run/run_1_2.aspx?no=134)

2.

3.

4.

5.

6.

7.
8.
9.