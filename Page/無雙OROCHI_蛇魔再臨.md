是《[無雙OROCHI](../Page/無雙蛇魔.md "wikilink")》的續集，此作品在2008年4月3日在日本開始發售PS2版，而[北美版則在](../Page/北美.md "wikilink")2008年9月23日發售，[歐洲版則在](../Page/歐洲.md "wikilink")2008年9月19日發售。此作品也正式宣布在2008年9月4日在日本發售[Xbox
360版](../Page/Xbox_360.md "wikilink")\[1\]，而歐洲版和北美版則隨同PS2版一起發售。[PSP版則宣布在](../Page/Playstation_Portable.md "wikilink")2008年11月27日正式發售\[2\]，之後又推出了含有[無雙OROCHI
Z部分內容的增值中文版](../Page/無雙OROCHI_Z.md "wikilink")。 2011年，推出續作《[無雙OROCHI
2](../Page/無雙OROCHI_2.md "wikilink")》。

## 角色

### 新角色

由於此遊戲是《[戰國無雙2：猛將傳](../Page/戰國無雙.md "wikilink")》發售後的作品，[今川義元的新造型版](../Page/今川義元.md "wikilink")、[佐佐木小次郎](../Page/佐佐木小次郎.md "wikilink")、[柴田勝家](../Page/柴田勝家.md "wikilink")、[前田利家](../Page/前田利家.md "wikilink")、[長宗我部元親和](../Page/長宗我部元親.md "wikilink")[加西亞也正式加入遊戲內](../Page/細川玉.md "wikilink")。而且除了自《[真·三國無雙2](../Page/真·三國無雙.md "wikilink")》登場後就沒再出現的[伏羲和](../Page/伏羲.md "wikilink")[女媧再次回歸之外](../Page/女媧.md "wikilink")，遊戲也為這2個角色重新設計新造型，並採用另一聲優為他們配音。

以下無雙系列在本作品新增的角色：

  - [太公望](../Page/姜子牙.md "wikilink")，源自[封神榜的](../Page/封神榜.md "wikilink")[周朝軍師](../Page/周朝.md "wikilink")
  - [平清盛](../Page/平清盛.md "wikilink")，平安時代最有權勢的平家大名
  - [源義經](../Page/源義經.md "wikilink")，平安時代與平家對敵的源氏民族英雄
  - [孫悟空](../Page/孫悟空.md "wikilink")，源自[中國文學](../Page/中國文學.md "wikilink")[四大奇書之一](../Page/四大奇書.md "wikilink")[西遊記](../Page/西遊記.md "wikilink")
  - [卑彌呼](../Page/卑彌呼.md "wikilink")，古代日本邪馬台國的女王

此外還新增真‧遠呂智，為遠呂智的終極形態，達成特定條件之後即可使用之。

以及2位新敵人（只適用於對戰模式及生存模式）

  - [牛鬼](../Page/牛鬼.md "wikilink")
  - [百百目鬼](../Page/百百目鬼.md "wikilink")

## 故事

### 主線故事

此續作延續前作一個月後所發生的事情。

由於平清盛欣賞[遠呂智之實力](../Page/八岐大蛇.md "wikilink")，故打算要把其復活，並召集其舊部下歸隊以整頓遠呂智軍實力。
在發現只有十七歲的邪馬台國女王卑彌呼體內擁有令遠呂智復活的神秘力量後，平清盛便千方百計要將她『拿』到手。

以太公望為首領的仙界軍和源義經為了阻止平清盛的野心，便與人類各勢力合作，並和遠呂智軍不斷交戰，以圖打擊遠呂智軍勢力。不過平清盛最終成功使遠呂智復活，復活後的遠呂智更因卑彌呼的神祕力量而進化成「真‧遠呂智」。

眾勢力為了阻止異世界繼續崩壞，唯有賭上自己的力量，跟魔王決一死戰...

### 新增故事

在本作中還新增了遠呂智傳，主要講述前作中遠呂智及其爪牙與人類各勢力交戰，亦交代遠呂智軍如何擊潰蜀國及部份戰國大名勢力，並迫使魏國、吳國歸順。

## 系統改變

1.  每個人的熟練度最大值由10變成50。
2.  每個人3個造型（真遠呂智、百百目鬼、牛鬼除外）。
3.  增加武器的15种煉成技能，每個武器最多可同時裝3個，且收藏要素變成收集煉成技能的30個素材。
4.  增加援護技。
5.  增加合體技，指定的三個人在一起有特殊效果。
6.  帶武器屬性的Charge攻擊改變（最明顯的是三國人物的C1、C2不帶屬性，以及関平的C4只有第4擊帶武器屬性、曹操的C6只有第1和第5擊帶武器屬性等）。

## 新增模式

故事模式增加遠呂智勢力的關卡，以遠呂智的角度描述《[無雙OROCHI](../Page/無雙OROCHI.md "wikilink")》前的故事，加上承接前作的蜀、魏、吳、戰國，共有5個章節。除此之外也新增了劇情模式（Dramatic
Mode）、對戰模式（VS Mode）、生存模式（Survival Mode）。

## 新增關卡

以下列表為此續作新增之關卡，包括本作原創關卡、在真三國無雙4猛將傳或戰國無雙2猛將傳出現的關卡。括號內列出使用該關卡的章節：

### 原創關卡

  - [嚴島](../Page/嚴島.md "wikilink")（吳傳4章「嚴島之戰」、劇情模式No.4「嚴島突破戰」）
  - [五行山](../Page/五行山.md "wikilink")（蜀傳4章「五行山之戰」）
  - [邪馬台](../Page/邪馬台.md "wikilink")（魏傳6章「邪馬台之戰」、劇情模式No.8「邪馬台決戰」）
  - 火河（遠呂智傳終章「火河之戰」、劇情模式No.19「火河決戰」）

### 真三國無雙4猛將傳關卡

  - [漢水](../Page/漢水.md "wikilink")（劇情模式No.15「漢水之戰」）
  - [冀城](../Page/冀城.md "wikilink")（魏傳4章「冀城之戰」）
  - [兗州](../Page/兗州之戰.md "wikilink")（劇情模式No.27「兗州之戰」）
  - [祁山](../Page/祁山.md "wikilink")（劇情模式No.5「祁山之戰」）
  - [司州](../Page/司州.md "wikilink")（魏傳1章「司州之戰」）
  - [石亭](../Page/石亭之戰.md "wikilink")（蜀傳1章「石亭之戰」）
  - [長沙](../Page/長沙.md "wikilink")（劇情模式No.25「長沙之戰」）
  - [南中](../Page/南中.md "wikilink")（劇情模式No.26「南中攻防戰」）
  - [濡須口](../Page/濡須口之戰.md "wikilink")（吳傳1章「濡須口之戰」）
  - [麥城](../Page/麥城.md "wikilink")（劇情模式No.20「麥城之戰」）
  - [陽平關](../Page/陽平關.md "wikilink")（吳傳6章「陽平關之戰」）
  - [樓桑村](../Page/樓桑村.md "wikilink")（戰國傳1章「樓桑村之戰」）
  - [葭萌關](../Page/葭萌關.md "wikilink")（蜀傳5章「葭萌關之戰」）
  - [崑崙山](../Page/崑崙山.md "wikilink")（劇情模式No.9「荊州之戰」）

### 戰國無雙2猛將傳關卡

  - [四國](../Page/四國.md "wikilink")（劇情模式No.21「四國之戰」）
  - [桶狹間](../Page/桶狹間之戰.md "wikilink")（吳傳5章「桶狹間之戰」）

## 新角色資料

### 他國2

| 人物名稱                             | 配音（CV）                               | 武器（Lv4武器）                                                            | 首次登場作品  |
| -------------------------------- | ------------------------------------ | -------------------------------------------------------------------- | ------- |
| [太公望](../Page/姜子牙.md "wikilink") | [岸尾大輔](../Page/岸尾大輔.md "wikilink")   | [細鞭](../Page/細鞭.md "wikilink")（雷公鞭）                                  | 本作      |
| [孫悟空](../Page/孫悟空.md "wikilink") | [小山力也](../Page/小山力也.md "wikilink")   | [天凜棒](../Page/天凜棒.md "wikilink")（[齊天大聖](../Page/齊天大聖.md "wikilink")） | 本作      |
| [伏羲](../Page/伏羲.md "wikilink")   | [安元洋貴](../Page/安元洋貴.md "wikilink")   | [巨劍](../Page/劍.md "wikilink")（伏羲の大剣）                                 | 真·三國無雙2 |
| [女媧](../Page/女媧.md "wikilink")   | [牧島有希](../Page/牧島有希.md "wikilink")   | [細劍](../Page/劍.md "wikilink")（女媧の細剣）                                 | 真·三國無雙2 |
| [源義經](../Page/源義經.md "wikilink") | [織田優成](../Page/織田優成.md "wikilink")   | [閃光の小手](../Page/閃光の小手.md "wikilink")（武雷の小手）                          | 本作      |
| [平清盛](../Page/平清盛.md "wikilink") | [大友龍三郎](../Page/大友龍三郎.md "wikilink") | [念珠](../Page/念珠.md "wikilink")（[重盛](../Page/平重盛.md "wikilink")）      | 本作      |
| [卑彌呼](../Page/卑彌呼.md "wikilink") | [前田沙耶香](../Page/前田沙耶香.md "wikilink") | [銅鐸](../Page/銅鐸.md "wikilink")（[天照](../Page/天照大神.md "wikilink")）     | 本作      |

### 戰國2猛將傳

| 人物名稱                                   | 配音（CV）                               | 武器（Lv4武器）                                                            | 首次登場作品                    |
| -------------------------------------- | ------------------------------------ | -------------------------------------------------------------------- | ------------------------- |
| [佐佐木小次郎](../Page/佐佐木小次郎.md "wikilink") | [上田祐司](../Page/上田祐司.md "wikilink")   | [日本刀](../Page/日本刀.md "wikilink")、從異空間召喚的兩把大刀（田道闇守刀）                  | 戰國無雙2（NPC狀態）、戰國無雙2猛將傳     |
| [柴田勝家](../Page/柴田勝家.md "wikilink")     | [竹本英史](../Page/竹本英史.md "wikilink")   | 兩把大[斧](../Page/斧.md "wikilink")（金剛武斧）                                | 戰國無雙2（NPC狀態；使用槍）、戰國無雙2猛將傳 |
| [前田利家](../Page/前田利家.md "wikilink")     | [小西克幸](../Page/小西克幸.md "wikilink")   | [刀](../Page/刀.md "wikilink")、兩把大[槍](../Page/槍.md "wikilink")（葦原火遠理命） | 戰國無雙2猛將傳                  |
| [長宗我部元親](../Page/長宗我部元親.md "wikilink") | [置鮎龍太郎](../Page/置鮎龍太郎.md "wikilink") | [三味線](../Page/三味線.md "wikilink")（天津甕三味星）                             | 戰國無雙2猛將傳                  |
| [加西亞](../Page/細川玉.md "wikilink")       | [鹿野潤](../Page/鹿野潤.md "wikilink")     | [腕輪](../Page/腕輪.md "wikilink")（神直毘釧）                                 | 戰國無雙2猛將傳                  |

## PSP版變化

  - 增加趙云、夏侯惇和孫尚香的《[真三國無雙：連袂出擊](../Page/真·三國無雙5.md "wikilink")》的無雙覺醒的造型

<!-- end list -->

  - 倍受惡評的劇情模式的兗州之戰的出現條件簡化

<!-- end list -->

  - 馬上攻擊也可發揮“天舞”的效果

<!-- end list -->

  - 戰場上與主綫劇情無關的對話去除語音

## PSP版增值中文版變化

  - 追加[三藏法師](../Page/三藏法師.md "wikilink")、[辨慶](../Page/辨慶.md "wikilink")

<!-- end list -->

  - 戲劇模式增加12個劇本

<!-- end list -->

  - 增加14個事件

<!-- end list -->

  - 牛鬼、百百目鬼可以在故事模式中使用

<!-- end list -->

  - VS模式可與CPU對戰

<!-- end list -->

  - 取消連袂出擊（真三MR）造型

（以上全部是以蛇魔Z為藍本）

## 參考資料

## 外部連結

  - [無雙OROCHI 魔王再臨 官方網站](http://www.gamecity.ne.jp/orochi-sairin/)
  - [無雙OROCHI 蛇魔再臨 官方網站](http://www.gamecity.com.tw/orochi-sairin/)

[Category:无双大蛇系列](../Category/无双大蛇系列.md "wikilink")
[Category:2008年电子游戏](../Category/2008年电子游戏.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:Xbox 360遊戲](../Category/Xbox_360遊戲.md "wikilink")
[Category:光荣特库摩电子游戏系列](../Category/光荣特库摩电子游戏系列.md "wikilink")
[Category:砍殺遊戲](../Category/砍殺遊戲.md "wikilink")

1.
2.