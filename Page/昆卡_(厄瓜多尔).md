**昆卡**（，全稱：**昆卡·四河之地·聖安娜**、）是[厄瓜多尔的第三大城市](../Page/厄瓜多尔.md "wikilink")，[阿苏艾省首府](../Page/阿苏艾省.md "wikilink")，人口约40万。该城位于海拔约2,500米的高原地区，其历史城区被联合国教科文组织列为世界文化遗产。

## 气候

昆卡属[柯本气候分类中的](../Page/柯本气候分类.md "wikilink")[亚热带高原气候](../Page/亚热带高原气候.md "wikilink")（Cfb）。与[厄瓜多尔](../Page/厄瓜多尔.md "wikilink")[安第斯山脉其他地区一样](../Page/安第斯山脉.md "wikilink")，昆卡全年气候温和。白天通常是温暖的，夜晚足够凉爽，通常需要毛衣或夹克。平均每日温度为14.7°C（58.5°F）。有两个季节：雨季和旱季。旱季在6月到12月之间，有一些变化。雨季在1月到5月之间，其特点是早晨晴朗，下午有阵雨。3月、4月和5月是热带美洲雨季（雨季）降雨量最大的季节。

## 图册

Municipio de Cuenca (Ecuador).jpg|波尔瓦街昆卡市政厅 Tranvía de Cuenca
01.jpg|昆卡电车 Rio-tomebamba-centro-cuenca.png|托姆班巴河

## 友好城市

  - [智利](../Page/智利.md "wikilink")[康塞普西翁](../Page/康塞普西翁_\(智利\).md "wikilink")

  - [西班牙](../Page/西班牙.md "wikilink")[昆卡](../Page/昆卡_\(西班牙\).md "wikilink")

  - [秘鲁](../Page/秘鲁.md "wikilink")[库斯科](../Page/库斯科.md "wikilink")

  - [古巴](../Page/古巴.md "wikilink")[哈瓦那](../Page/哈瓦那.md "wikilink")

## 参考资料

## 外部链接

  - [市政府官方网站](https://web.archive.org/web/20060615030156/http://www.municipalidadcuenca.gov.ec/)

[Category:厄瓜多城市](../Category/厄瓜多城市.md "wikilink")
[Category:厄瓜多世界遺產](../Category/厄瓜多世界遺產.md "wikilink")