**马里奥·梅尔奇奥特**（**Mario
Melchiot**，）[荷蘭足球運動員](../Page/荷蘭.md "wikilink")，擔任[後衛](../Page/後衛_\(足球\).md "wikilink")，可擔任[右閘或](../Page/右閘.md "wikilink")[中堅](../Page/中堅.md "wikilink")。

## 生平

### 球會

#### 阿積士

米茲洛是於荷蘭首都[阿姆斯特丹出生和成長](../Page/阿姆斯特丹.md "wikilink")，屬蘇里南裔人。其後加入阿積士青年軍，為[阿積士青訓產品](../Page/阿積士.md "wikilink")。其第一次出場是於1996-97年球季，曾參與過[歐冠盃](../Page/歐冠盃.md "wikilink")。在效力阿積士三季間，米茲洛助球隊於1998年贏得[荷甲冠軍](../Page/荷蘭甲組聯賽.md "wikilink")，同時又在1998年、1999年贏得[荷蘭盃](../Page/荷蘭盃.md "wikilink")。

#### 車路士

1999年，米茲洛以[博斯曼法案](../Page/博斯曼法案.md "wikilink")，自由身轉會[英超球隊](../Page/英超.md "wikilink")[車路士](../Page/車路士.md "wikilink")。在加盟第一個球季，米茲洛便協助車路士，於2000年贏得[英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")。其時他司職右閘，與[泰利](../Page/泰利.md "wikilink")、[迪西里](../Page/迪西里.md "wikilink")、[加拿斯等球員組成一條穩健防線](../Page/加拿斯.md "wikilink")。然而，自2003年6月俄羅斯富商[阿巴莫域治入主車路士後](../Page/阿巴莫域治.md "wikilink")，運用大量資金羅致球員，不斷壓縮米茲洛的上陣機會，包括2003年引入後衛[格連·莊臣](../Page/格連·莊臣.md "wikilink")，和2004年6月引入的後衛[保羅·費拉拿](../Page/保羅·費拉拿.md "wikilink")。

#### 伯明翰

在上陣機日漸減少後，米茲洛於2004年7月轉投[伯明翰城](../Page/伯明翰城足球俱樂部.md "wikilink")，簽訂4年合約。米茲洛在伯明翰城對[樸茨茅夫的比賽首次上陣](../Page/樸茨茅夫足球會.md "wikilink")。但整體上因伯明翰實力有限，米茲洛基本都是協助球會打護級戰。在2005-06年球季，伯明翰成績急轉直下，並於2006年4月宣告護級失敗。結果在2006年5月7日上陣對[保頓後](../Page/保頓足球會.md "wikilink")，米茲洛選擇離開伯明翰城。

#### 雷恩

2006年8月，米茲洛與[法甲的](../Page/法甲.md "wikilink")[雷恩簽訂一年合約](../Page/雷恩足球俱樂部.md "wikilink")，在對[摩納哥的比賽首次代表雷恩](../Page/摩納哥足球俱樂部.md "wikilink")。在效力一季間，米茲洛上陣了三十場聯賽，取得兩個入球。

#### 韋根

2007年夏季，米茲洛與雷恩的合約結束，米茲洛決定重回[英超](../Page/英超.md "wikilink")，於6月15日以自由轉會形式加盟[韋根](../Page/韋根足球會.md "wikilink")\[1\]。在三季內，米茲洛為韋根核心主力，上陣了近一百場聯賽。而自2007年7月24日起，米茲洛更被選為隊長\[2\]。

#### 韋根

2010年6月4日，年屆33歲的米茲洛，由於未能與韋根談妥新約，離開英超，加盟卡塔爾球會[乌姆锡拉勒](../Page/乌姆沙拉尔体育俱乐部.md "wikilink")<small>\[3\]</small>，但只簽約一年。季末離隊，回復自由身。

## 國家隊

米茲洛共21次代表[荷蘭國家隊上陣](../Page/荷蘭國家足球隊.md "wikilink")，他於2000年10月11日首次代表國家隊上陣[2002年世界盃外圍賽對](../Page/2002年世界盃.md "wikilink")[葡萄牙的比賽](../Page/葡萄牙國家足球隊.md "wikilink")。自離開車路士後，米茲洛只效力小球會，一直未受國家隊注意。

2008年6月，米茲洛入選了荷蘭國家隊參加[2008年歐洲國家盃的](../Page/2008年歐洲國家盃.md "wikilink")23人名單\[4\]，但只後備上陣一次。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [米茲洛資料數據](http://www.stade-rennais-online.com/Mario-Melchiot.html)
  - [soccerbase
    米茲洛數據](http://www.soccerbase.com/players_details.sd?playerid=16250)
  - [Icons.com
    米茲洛官方網頁](https://web.archive.org/web/20080511180933/http://www.icons.com/melchiot/)
  - [Wereld van oranje.nl
    米茲洛資料](http://www.wereldvanoranje.nl/profielen/profiel.php?id=1124)

[Category:荷蘭足球運動員](../Category/荷蘭足球運動員.md "wikilink")
[Category:阿積士球員](../Category/阿積士球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:伯明翰城球員](../Category/伯明翰城球員.md "wikilink")
[Category:雷恩球員](../Category/雷恩球員.md "wikilink")
[Category:韋根球員](../Category/韋根球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")

1.
2.
3.
4.  [cn.uefa.com](http://cn.euro2008.uefa.com/tournament/players/player=26400/index.html)2008年歐洲國家盃官方網頁
    米茲洛資料