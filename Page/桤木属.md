**桤木属**（也称赤杨属，[学名](../Page/学名.md "wikilink")：）是[桦木科的一个](../Page/桦木科.md "wikilink")[属](../Page/属_\(生物\).md "wikilink")。在中欧有三种本土赤杨：普通赤杨，灰赤杨和绿赤杨。

和其他桦木科植物一样，赤杨的花是单性雌雄同株的，葇荑花序。每一花序仅含有雌性或雄性花。赤杨是阔叶树木中唯一雌性花序会木质化的。木质化后的雌性花序被称作[球果](../Page/球果.md "wikilink")。

全世界有赤杨30余种。[安第斯赤杨只在](../Page/安第斯赤杨.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")[安第斯山脉分布](../Page/安第斯山脉.md "wikilink")，未见其在[欧洲](../Page/欧洲.md "wikilink")，[亚洲和](../Page/亚洲.md "wikilink")[北美洲有分布](../Page/北美洲.md "wikilink")。

## 生态

赤杨是为夏绿落叶[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")。

赤杨的根部与固氮细菌[共生](../Page/共生.md "wikilink")，形成根瘤,这和结节细菌与豆类植物共生所形成的[菌根一样](../Page/菌根.md "wikilink")。因此赤杨即使在树叶脱落后也有足够的氮源。这种共生关系使得赤杨能在低氮环境中生存。所以在那些经自然作用或者人类活动后失去植被的地区，赤杨常常作为一种先锋植物在这些地方繁殖，比如雪崩后，绿赤杨会作为先锋，首先在该地区生长，为其他植物进驻做准备。

灰赤杨和普通赤杨(*A. incana* 和 *A. glutinosa*)
常见于水域边缘或者潮湿地区。在非常潮湿的地区养分经常不充足，具固氮作用的根瘤起着决定性作用，这就是有些地区只有赤杨生长，而不见其他树种的原因。

灰赤杨和普通赤杨能防止河水对堤岸的侵蚀，起到很大的生态作用。

## 用途

赤杨木轻，在湿度经常变化的环境中很容易变坏。但在水中它却可以长久保存。赤杨木在新鲜砍伐后或者在潮湿环境中可以燃烧。特别适用于土方作业，制作刷子杆柄，扫帚杆，玩具还有乐器。

## 赤杨种类 (部分)

  - [意大利赤杨](../Page/意大利赤杨.md "wikilink") (*Alnus cordata*)
  - [普通赤杨](../Page/普通赤杨.md "wikilink") (*Alnus glutinosa*)
  - [灰赤杨](../Page/灰赤杨.md "wikilink"), 也作白赤杨(*Alnus incana*)
  - [安第斯赤杨](../Page/安第斯赤杨.md "wikilink") (*Alnus jorullensis*)
  - [美国赤杨](../Page/美国赤杨.md "wikilink") (*Alnus rubra*)
  - [绿赤杨](../Page/绿赤杨.md "wikilink") (*Alnus viridis*)
  - [臺灣赤楊](../Page/臺灣赤楊.md "wikilink")(*Alnus formosana*) (Burkill ex
    Forbes & Hemsl.) Makino

[Category:桤木属](../Category/桤木属.md "wikilink")
[Category:树](../Category/树.md "wikilink")