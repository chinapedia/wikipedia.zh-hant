《**Day &
Night**》為[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[衛蘭的首張個人音樂專輯](../Page/衛蘭.md "wikilink")，於2005年4月13日發行\[1\]。

該專輯由名監製[雷頌德及當時仍在幕後工作的](../Page/雷頌德.md "wikilink")[側田擔任唱片監製](../Page/側田.md "wikilink")，全碟均是翻唱或改編自歌手[黎明的經典歌曲](../Page/黎明.md "wikilink")。[衛蘭憑其優美的西化聲線](../Page/衛蘭.md "wikilink")，及上佳的歌唱技巧令此碟大賣，並於同年五月再推出第二版，[衛蘭亦因此而一炮而紅](../Page/衛蘭.md "wikilink")。第二版的推出日期為2005年5月26日，除了換上新封面外，亦加插了《大哥》一曲，該歌曲於[香港成為高流行度及高播放率的流行曲](../Page/香港.md "wikilink")\[2\]\[3\]。

## 曲目

## 派台歌曲成績

|           年份           |              歌曲              |        最高排名         |
| :--------------------: | :--------------------------: | :-----------------: |
|   <small>903</small>   |      <small>週數</small>       | <small>RTHK</small> |
|  <small>2005</small>   | <small>In Love Again</small> |         \-          |
|  <small>一夜傾情</small>   |              \-              |         \-          |
|  <small>不可一世</small>   |              10              |          2          |
| <small>情深說話未曾講</small> |              9               |          3          |
|   <small>大哥</small>    |              \-              |         \-          |

## 参考

## 外部連結

  - [Day &
    Night](https://web.archive.org/web/20111014221837/http://music.soso.com/portal/albumn/10/albumn_2528322810.html)

[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:衛蘭音樂專輯](../Category/衛蘭音樂專輯.md "wikilink")
[Category:2005年音樂專輯](../Category/2005年音樂專輯.md "wikilink")

1.
2.
3.