**康瓦爾郡**（，[國際音標](../Page/國際音標.md "wikilink")：/'/；-{zh-hk:[康和語](../Page/康和語.md "wikilink");zh-cn:[康沃尔语](../Page/康沃尔语.md "wikilink");zh-tw:[康瓦爾語](../Page/康瓦爾語.md "wikilink")}-
：Kernow；香港旧译**歌和老**），[大不列顛島西南端的](../Page/大不列顛島.md "wikilink")[半島](../Page/半島.md "wikilink")，[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[西南端的](../Page/英格蘭西南.md "wikilink")[區](../Page/英格蘭的郡.md "wikilink")。康瓦爾東與[德文郡相鄰](../Page/德文郡.md "wikilink")，南臨[英倫海峽](../Page/英倫海峽.md "wikilink")，西、北臨[大西洋](../Page/大西洋.md "wikilink")。以人口計算，[特魯羅是最大](../Page/特魯羅.md "wikilink")[城市](../Page/英國的城市地位.md "wikilink")（亦是行政總部），[聖奧斯特爾是第一大鎮](../Page/聖奧斯特爾.md "wikilink")（Town）。

2009年之前，康瓦爾是34個[非都市郡之一](../Page/非都市郡.md "wikilink")，實際管轄6個[非都市區](../Page/非都市區.md "wikilink")，[佔地](../Page/面積.md "wikilink")3547[平方公里](../Page/平方公里.md "wikilink")（[第9](../Page/英格蘭的非都市郡列表_\(以面積排列\).md "wikilink")），人口为524200（[第23](../Page/英格蘭的非都市郡列表_\(以人口排列\).md "wikilink")）；如看待成48個[名譽郡之一](../Page/名譽郡.md "wikilink")，它名義上包含多1個[單一管理區](../Page/單一管理區.md "wikilink")——[錫利群島](../Page/錫利群島.md "wikilink")，[佔地增至](../Page/面積.md "wikilink")3563[平方公里](../Page/平方公里.md "wikilink")（[第12](../Page/英格蘭的名譽郡列表_\(以面積排列\).md "wikilink")），[人口增至](../Page/人口.md "wikilink")526,300（[第39](../Page/英格蘭的名譽郡列表_\(以人口排列\).md "wikilink")）。

2009年4月，康瓦爾全郡改制為[單一管理區](../Page/單一管理區.md "wikilink")。

## 歷史與政治

### 古代

据[古罗马統治不列颠地区時期的歷史](../Page/古罗马.md "wikilink")，-{A|zh-hans:康沃尔;zh-hant:康瓦爾;zh-cn:康沃尔;zh-tw:康瓦爾;zh-hk:康和;zh-mo:康和;}-人應是古[凱爾特人之一支](../Page/凱爾特人.md "wikilink")。“corn”一字在[拉丁语與](../Page/拉丁语.md "wikilink")[蓋爾語皆指](../Page/蓋爾語言.md "wikilink")「半島」、「角狀」之意。紀元700年前後當地人建立政權，被稱為「康奴比雅」（Cornubia），亦即角地人之地。而後因與[薩克遜人和其語言接觸](../Page/薩克遜人.md "wikilink")，[薩克遜人將當地人與](../Page/薩克遜人.md "wikilink")[威尔士人並指混用](../Page/威尔士人.md "wikilink")。於是出現康瓦爾一詞（薩克遜人指其為西威爾斯人）。在紀元900年前後，經歷多年與[西薩克斯王國](../Page/西薩克斯王國.md "wikilink")（Kingdom
of
Wessex）和後來的英王的戰爭，兩方一度議和，互不隸屬。但到了英國最後一任[薩克遜王朝](../Page/薩克遜王朝.md "wikilink")[愛德華統治時期](../Page/懺悔者愛德華.md "wikilink")（約1066年前若干年），康瓦爾已被[英格蘭併吞](../Page/英格蘭.md "wikilink")，成為[西薩克斯](../Page/西薩克斯.md "wikilink")（Wessex）伯爵的領地，本地貴族的土地皆被薩克遜貴族接受。在[諾曼征服之後](../Page/諾曼征服英格蘭.md "wikilink")，來自[法國的](../Page/法國.md "wikilink")[諾曼貴族又接收了薩克遜地主的土地](../Page/諾曼貴族.md "wikilink")。由於諾曼貴族後來時有缺位，其特權遂又回到說[法語](../Page/法語.md "wikilink")、[康沃尔语](../Page/康沃尔语.md "wikilink")、[拉丁語的康瓦爾菁英階層](../Page/拉丁語.md "wikilink")。這些人後來成為所謂的「康瓦爾貴族」。

由於康和的特殊地位，[英國王室依慣例將國王的長子封為](../Page/英國王室.md "wikilink")「康和公爵」（兼[威爾斯親王](../Page/威爾斯親王.md "wikilink")），其領地「康和公國」橫跨[德雲郡與康和郡兩處](../Page/德雲郡.md "wikilink")，佔地七百餘方公里。

### 分離主義與自治要求

由於康和的古代經歷，向英格蘭要求更高自治地位的呼聲始終不斷。最激烈的意見是要求成為與威爾斯、[蘇格蘭地位一致的政治實體](../Page/蘇格蘭.md "wikilink")，具有分立的國會與政府--成立「康和國民大會」（Cornish
Assembly）。此要求若能落實，[聯合王國的組成單位將由四個增為五個](../Page/聯合王國.md "wikilink")（[英格蘭](../Page/英格蘭.md "wikilink")、[蘇格蘭](../Page/蘇格蘭.md "wikilink")、[威爾斯](../Page/威爾斯.md "wikilink")、[北愛爾蘭](../Page/北愛爾蘭.md "wikilink")、[康瓦爾](../Page/康瓦爾.md "wikilink")）。另外，康瓦爾人也積極推動英格蘭承認康瓦爾應享有「少數民族」的待遇。

### 政黨

目前康和地區在[英國國會下議院有五席](../Page/英國國會.md "wikilink")，皆為[自由民主黨勝選](../Page/自由民主黨.md "wikilink")、在任。康瓦爾地區另一主要政治勢力是[康和民族黨](../Page/康和之子.md "wikilink")（Mebyon
Kernow）。五席自由民主黨議員和康和民族黨都聲援康瓦爾自治運動。到2009年國會改選時，康和區將有六席國會議員。為了爭取此區民意支持，英國[保守党已在](../Page/英国保守党.md "wikilink")[影子內閣增設](../Page/影子內閣.md "wikilink")「康和事務大臣」一職。

### 經濟史

康和乃世界上最具歷史的產[錫區](../Page/錫.md "wikilink")。自[石器時代以來就已經開始採礦了](../Page/石器時代.md "wikilink")，歷史記錄表明在11世紀與12世紀就已創辦了錫礦業。由於錫能製造古代最重要金屬──[青銅](../Page/青銅.md "wikilink")，錫在古代是一種需求相當龐大的礦產。[中世紀時康瓦爾礦山數目約有](../Page/中世紀.md "wikilink")2000座，至十九世紀[工業革命猶未衰微](../Page/工業革命.md "wikilink")。直至二十世紀末年國外的競爭使錫的價格下跌，現存的錫礦大多是作為古代遺址對遊客開放。

## 行政區劃

### 1972-2009

[缩略图](https://zh.wikipedia.org/wiki/File:Wheatfield_behind_a_modern_gate_-_geograph.org.uk_-_198315.jpg "fig:缩略图")
[ 1. [彭威斯](../Page/彭威斯.md "wikilink")
2\. [凱里爾](../Page/凱里爾.md "wikilink")
3\. [-{卡里克}-](../Page/卡里克區.md "wikilink")
4\. [里斯托默爾](../Page/里斯托默爾.md "wikilink")
5\. [卡拉登](../Page/卡拉登.md "wikilink")
6\. [北康瓦爾](../Page/北康瓦爾.md "wikilink")
7\.
[錫利群島](../Page/錫利群島.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
](https://zh.wikipedia.org/wiki/File:CornwallScillyNumbered.png "fig: 1. 彭威斯 2. 凱里爾 3. -{卡里克}- 4. 里斯托默爾 5. 卡拉登 6. 北康瓦爾 7. 錫利群島（單一管理區） ")

[錫利群島](../Page/錫利群島.md "wikilink")（Isles of
Scilly）東北偏東44公里達[大不列顛島西南端](../Page/大不列顛島.md "wikilink")，也就是康瓦爾彭威斯區的森南（Sennen）。錫利群島曾與康瓦爾共組為單一郡縣，但《1888年地方政府法案》定明錫利群島脫離作為行政郡的康瓦爾，成為單一管理區。該法案在在1889年4月1日生效後，錫利群島於1890年正式成為單一管理區。雖然如此，錫利群島的某些事務在行政或名義上，仍與康瓦爾組成同一單位，例如警察部門\[1\]。

康瓦爾是[非都市郡](../Page/非都市郡.md "wikilink")，實際管轄6個[非都市區](../Page/非都市區.md "wikilink")：[彭威斯](../Page/彭威斯.md "wikilink")（Penwith）、[凱里爾](../Page/凱里爾.md "wikilink")（Kerrier）、[卡-{里}-克](../Page/卡里克區.md "wikilink")（Carrick）、[里斯托默爾](../Page/里斯托默爾.md "wikilink")（Restormel）、[卡拉登](../Page/卡拉登.md "wikilink")（Caradon）、[北康瓦爾](../Page/北康瓦爾.md "wikilink")（North
Cornwall）；如看待成[名譽郡](../Page/名譽郡.md "wikilink")，它下名義上包含多一個*'特殊地位*的[單一管理區](../Page/單一管理區.md "wikilink")：[錫利群島](../Page/錫利群島.md "wikilink")。

### 2009-

2009年4月1日，英格蘭地方政府結構變化正式實施，7個[英格蘭的郡改革行政區劃](../Page/英格蘭的郡.md "wikilink")，產生9個新的[單一管理區](../Page/單一管理區.md "wikilink")。康瓦爾由[非都市郡改制為](../Page/非都市郡.md "wikilink")[單一管理區](../Page/單一管理區.md "wikilink")。

## 地理

下圖顯示英格蘭48個名譽郡的分佈情況。康瓦爾東與[德文郡相鄰](../Page/德文郡.md "wikilink")，南臨[英倫海峽](../Page/英倫海峽.md "wikilink")，西、北臨[大西洋](../Page/大西洋.md "wikilink")。康瓦爾位處[英格蘭的西南端](../Page/英格蘭.md "wikilink")，鄰近大西洋。地理上康瓦爾南北兩邊有明顯差異。北面海岸以懸崖峭壁為主，而南面則有較多海灘。

### 氣候

由於康瓦爾是在英國南部，相對下氣候較為和暖，下雪次數也不頻密,康瓦爾全年平均氣溫為9.8-12 °C。同時因為近海，雨量也較充足，平均計為每年1051-1290毫米。

[Truro_stmarysst.jpg](https://zh.wikipedia.org/wiki/File:Truro_stmarysst.jpg "fig:Truro_stmarysst.jpg")\]\]

## 注釋

## 參考來源

## 外部連結

## 参见

  - [康沃尔肉馅饼](../Page/康沃尔肉馅饼.md "wikilink")
  - [康沃尔人](../Page/康沃尔人.md "wikilink")
  - [康沃尔语](../Page/康沃尔语.md "wikilink")

{{-}}

[Category:英格蘭的郡](../Category/英格蘭的郡.md "wikilink")
[康沃爾郡](../Category/康沃爾郡.md "wikilink")
[Category:單一管理區](../Category/單一管理區.md "wikilink")

1.  [Devon-cornwall](http://webarchive.nationalarchives.gov.uk/20081105155104/http://www.devon-cornwall.police.uk/v3/about/index.htm)