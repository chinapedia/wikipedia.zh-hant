**萊辛頓大道線**（），又稱**東城線**或**萊辛頓-第四大道線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")[IRT分部其中一條路線](../Page/跨區捷運公司.md "wikilink")，由或[曼哈頓下城北上至](../Page/曼哈頓下城.md "wikilink")[東哈萊姆的](../Page/東哈萊姆_\(曼哈頓\).md "wikilink")[125街](../Page/125街車站_\(IRT萊辛頓大道線\).md "wikilink")。[曼哈頓下城和](../Page/曼哈頓下城.md "wikilink")[中城的部分是紐約全市](../Page/曼哈頓中城.md "wikilink")的一部分。此線有等服務路線途經。

幾十年間，萊辛頓大道線是[曼哈頓唯一一條服務](../Page/曼哈頓.md "wikilink")[上東城和東](../Page/上東城.md "wikilink")[中城的路線](../Page/曼哈頓中城.md "wikilink")，因此這條四線鐵路也是全美國最多乘客使用的地鐵路線。該路線每日平均有130萬人次使用，比[舊金山](../Page/舊金山灣區捷運系統.md "wikilink")（每日452,600人次）、[芝加哥](../Page/芝加哥地鐵.md "wikilink")（每日772,900人次）、[波士頓](../Page/馬薩諸塞灣交通局.md "wikilink")（每日569,200人次）的整個還要多。2007年，萊辛頓大道線的戴客量已經超越了整個[華盛頓地鐵](../Page/華盛頓地鐵.md "wikilink")\[1\]，以至於同年開始興建[第二大道線以減輕其負擔](../Page/第二大道線.md "wikilink")\[2\]。

沿線有四個車站被廢棄。當各月台都延長至容納10節車箱的列車時一併關閉了一些相鄰的站。車站因為太接近[14街-聯合廣場與](../Page/14街-聯合廣場車站_\(IRT萊辛頓大道線\).md "wikilink")[23街車站而廢棄](../Page/23街車站_\(IRT萊辛頓大道線\).md "wikilink")\[3\]。另外，與車站都與[布魯克林大橋-市政府車站的布魯克林大橋和杜安街出口太接近而廢棄](../Page/布魯克林大橋-市政府車站_\(IRT萊辛頓大道線\).md "wikilink")\[4\]\[5\]。最後，[南碼頭位於](../Page/南碼頭迴圈.md "wikilink")[布林格陵車站的步行距離內](../Page/布林格陵車站_\(IRT萊辛頓大道線\).md "wikilink")，而且正正位於[IRT百老匯-第七大道線對應車站旁邊](../Page/IRT百老匯-第七大道線.md "wikilink")\[6\]。

## 服務

以下路使使用全部或部分IRT萊辛頓大道線，服務路線顏色為綠色\[7\]：\[8\]

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>時段</p></th>
<th><p>路段</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>繁忙時段及日間</p></td>
<td><p>晚上及週末</p></td>
<td><p>深夜</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>快車</p></td>
<td><p>慢車</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>無服務</p></td>
<td><p>全線（平日）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布林格陵車站_(IRT萊辛頓大道線).md" title="wikilink">布林格陵以北</a>（晚上及週末）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>慢車</p></td>
<td><p><a href="../Page/布魯克林大橋-市政府車站_(IRT萊辛頓大道線).md" title="wikilink">布魯克林大橋-市政府以北</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>慢車</p></td>
<td><p>無服務</p></td>
</tr>
</tbody>
</table>

## 車站

<table>
<thead>
<tr class="header">
<th><p>鄰里<br />
（大約）</p></th>
<th></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th><p>軌道</p></th>
<th><p>服務</p></th>
<th><p>啟用</p></th>
<th><p>轉乘和備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/IRT傑羅姆大道線.md" title="wikilink">IRT傑羅姆大道線</a>（）及<a href="../Page/IRT佩勒姆線.md" title="wikilink">IRT佩勒姆線</a>（）匯合，軌道開始</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>萊辛頓大道隧道</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東哈萊姆_(曼哈頓).md" title="wikilink">東哈萊姆</a></p></td>
<td></td>
<td><p><a href="../Page/125街車站_(IRT萊辛頓大道線).md" title="wikilink">125街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td><p>nowrap|</p></td>
<td><p>1918年7月17日[9]</p></td>
<td><p>可連接<a href="../Page/大都會北方鐵路.md" title="wikilink">大都會北方鐵路</a>，<br />
，<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/116街車站_(IRT萊辛頓大道線).md" title="wikilink">116街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月17日[10]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/110街車站_(IRT萊辛頓大道線).md" title="wikilink">110街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月17日[11]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/103街車站_(IRT萊辛頓大道線).md" title="wikilink">103街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月17日[12]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上東城.md" title="wikilink">上東城</a></p></td>
<td></td>
<td><p><a href="../Page/96街車站_(IRT萊辛頓大道線).md" title="wikilink">96街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月17日[13]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/86街車站_(IRT萊辛頓大道線).md" title="wikilink">86街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1918年7月17日[14]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/77街車站_(IRT萊辛頓大道線).md" title="wikilink">77街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月17日[15]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/68街-亨特學院車站_(IRT萊辛頓大道線).md" title="wikilink">68街-亨特學院</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月17日[16]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曼哈頓中城.md" title="wikilink">曼哈頓中城</a></p></td>
<td></td>
<td><p><a href="../Page/59街車站_(IRT萊辛頓大道線).md" title="wikilink">59街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1918年7月17日[17]<br />
(1962, express)</p></td>
<td><p>（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>，<a href="../Page/萊辛頓大道／59街車站_(BMT百老匯線).md" title="wikilink">萊辛頓大道／59街</a>）<br />
使用出站轉乘：（<a href="../Page/63街線.md" title="wikilink">63街線</a>，<a href="../Page/萊辛頓大道-63街車站.md" title="wikilink">萊辛頓大道-63街</a>）<br />
<a href="../Page/羅斯福島纜車.md" title="wikilink">羅斯福島纜車</a><br />
此站起初為慢車站。供快車使用的下層月台在1962年啟用。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/51街車站_(IRT萊辛頓大道線).md" title="wikilink">51街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1918年7月17日[18]</p></td>
<td><p>（<a href="../Page/IND皇后林蔭路線.md" title="wikilink">IND皇后林蔭路線</a>，<a href="../Page/萊辛頓大道-53街車站_(IND皇后林蔭路線).md" title="wikilink">萊辛頓大道-53街</a>）<br />
往下城月台升降機關閉至2017年10月以便進行長期維修[19]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/大中央-42街車站_(IRT萊辛頓大道線).md" title="wikilink">大中央-42街</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1918年7月17日[20]</p></td>
<td><p>（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）<br />
（<a href="../Page/42街接駁線.md" title="wikilink">42街接駁線</a>）<br />
可連接<a href="../Page/大都會北方鐵路.md" title="wikilink">大都會北方鐵路</a>，<a href="../Page/大中央總站.md" title="wikilink">大中央總站</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>與<a href="../Page/42街接駁線.md" title="wikilink">IRT 42街接駁線南行慢車軌道匯合</a>（無定期服務）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/33街車站_(IRT萊辛頓大道線).md" title="wikilink">33街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[21]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/玫瑰山_(曼哈頓).md" title="wikilink">玫瑰山</a></p></td>
<td><p> ↓</p></td>
<td><p><a href="../Page/28街車站_(IRT萊辛頓大道線).md" title="wikilink">28街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[22]</p></td>
<td><p>車站只限南行可<a href="../Page/美國殘疾人法案.md" title="wikilink">無障礙通行</a>，升降機暫停使用</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/格拉梅西公園.md" title="wikilink">格拉梅西</a></p></td>
<td></td>
<td><p><a href="../Page/23街車站_(IRT萊辛頓大道線).md" title="wikilink">23街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[23]</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[24]</p></td>
<td><p>1948年11月7日關閉[25]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聯合廣場_(紐約市).md" title="wikilink">聯合廣場</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><a href="../Page/14街-聯合廣場車站_(IRT萊辛頓大道線).md" title="wikilink">14街-聯合廣場</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1904年10月27日[26]</p></td>
<td><p>（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>）<br />
原名為14街</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東村_(紐約市).md" title="wikilink">東村</a></p></td>
<td></td>
<td><p><a href="../Page/阿斯托廣場車站_(IRT萊辛頓大道線).md" title="wikilink">阿斯托廣場</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[27]</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/布利克街車站_(IRT萊辛頓大道線).md" title="wikilink">布利克街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[28]</p></td>
<td><p>（<a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a>，<a href="../Page/百老匯-拉法葉街車站_(IND第六大道線).md" title="wikilink">百老匯-拉法葉街</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紐約蘇豪區.md" title="wikilink">蘇豪區</a>/<a href="../Page/小意大利_(曼哈頓).md" title="wikilink">小意大利</a></p></td>
<td></td>
<td><p><a href="../Page/泉街車站_(IRT萊辛頓大道線).md" title="wikilink">泉街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[29]</p></td>
<td><p>快車軌道之間有廢棄軌道存在，上方設有信號室</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曼哈頓華埠.md" title="wikilink">華埠</a></p></td>
<td></td>
<td><p><a href="../Page/堅尼街車站_(IRT萊辛頓大道線).md" title="wikilink">堅尼街</a></p></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[30]</p></td>
<td><p>（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線-主線</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線-曼哈頓大橋線</a>）<br />
（<a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/市政中心_(曼哈頓).md" title="wikilink">市政中心</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>慢車</p></td>
<td></td>
<td><p>1904年10月27日[31]</p></td>
<td><p>1962年9月1日關閉[32]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/布魯克林大橋-市政府車站_(IRT萊辛頓大道線).md" title="wikilink">布魯克林大橋-市政府</a></p></td>
<td></td>
<td><p>全部</p></td>
<td></td>
<td><p>1904年10月27日[33]</p></td>
<td><p>（<a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a>，<a href="../Page/錢伯斯街車站_(BMT納蘇街線).md" title="wikilink">錢伯斯街</a>）<br />
原名為布魯克林大橋，後來改名為布魯克林大橋-沃斯街。設站有兩個側式月台，但只能容納5節慢車。同時南面有已關閉月台的延伸。</p></td>
<td></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>慢車軌道不再與快車軌道並排，慢車軌道使用迴圈作（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>迴圈</p></td>
<td></td>
<td><p>1904年10月27日[34]</p></td>
<td><p>1945年12月31日關閉，現時用作慢車而不停靠。萊辛頓大道線慢車在1904至1945年間除深夜外停靠此站，列車繼續前往南碼頭。[35]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>快車軌道繼續（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曼哈頓金融區.md" title="wikilink">金融區</a></p></td>
<td></td>
<td><p><a href="../Page/福爾頓街車站_(IRT萊辛頓大道線).md" title="wikilink">福爾頓街</a></p></td>
<td></td>
<td><p>快車</p></td>
<td></td>
<td><p>1905年1月16日[36]</p></td>
<td><p>（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>）<br />
（<a href="../Page/BMT納蘇街線.md" title="wikilink">BMT納蘇街線</a>）<br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）<br />
可途經連接，<a href="../Page/科特蘭街車站_(BMT百老匯線).md" title="wikilink">科特蘭街</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/華爾街車站_(IRT萊辛頓大道線).md" title="wikilink">華爾街</a></p></td>
<td></td>
<td><p>快車</p></td>
<td></td>
<td><p>1905年6月12日[37]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/布林格陵車站_(IRT萊辛頓大道線).md" title="wikilink">布林格陵</a></p></td>
<td></td>
<td><p>快車</p></td>
<td></td>
<td><p>1905年7月10日[38]</p></td>
<td><p><br />
<a href="../Page/史泰登島渡輪.md" title="wikilink">史泰登島渡輪</a>，</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>途經（）分岔往布魯克林，成為<a href="../Page/IRT東公園道線.md" title="wikilink">IRT東公園道線快車軌道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曼哈頓金融區.md" title="wikilink">金融區</a></p></td>
<td><p>快車途經兩個迴圈（）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/南碼頭迴圈.md" title="wikilink">南碼頭</a></p></td>
<td></td>
<td><p>兩個迴圈</p></td>
<td></td>
<td><p>1905年7月10日[39]</p></td>
<td><p>內側月台在1977年2月12日關閉，現時用作快車而不停靠。萊辛頓大道線列車在1905年7月10日至1918年7月1日及1950年至1977年2月12日期間使用<a href="../Page/南碼頭迴圈.md" title="wikilink">外側月台</a>。外側月台在2009年3月16日關閉，再次容許萊辛頓大道線使用兩個迴圈軌道。</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考文獻

[萊](../Category/紐約地鐵路線.md "wikilink")
[Category:1904年啟用的鐵路線](../Category/1904年啟用的鐵路線.md "wikilink")

1.

2.

3.
4.
5.
6.

7.

8.

9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.

20.
21.
22.
23.
24.
25.

26.
27.
28.
29.
30.
31.
32.

33.
34.
35.
36.
37.

38.
39.