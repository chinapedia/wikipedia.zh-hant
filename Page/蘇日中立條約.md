[Soviet_Japanese_Neutrality_Pact_13_April_1941.jpg](https://zh.wikipedia.org/wiki/File:Soviet_Japanese_Neutrality_Pact_13_April_1941.jpg "fig:Soviet_Japanese_Neutrality_Pact_13_April_1941.jpg")
[Matsuoka_signs_the_Soviet–Japanese_Neutrality_Pact-1.jpg](https://zh.wikipedia.org/wiki/File:Matsuoka_signs_the_Soviet–Japanese_Neutrality_Pact-1.jpg "fig:Matsuoka_signs_the_Soviet–Japanese_Neutrality_Pact-1.jpg")簽署《蘇日中立條約》\]\]
[Molotov_signs_the_Soviet–Japanese_Neutrality_Pact.jpg](https://zh.wikipedia.org/wiki/File:Molotov_signs_the_Soviet–Japanese_Neutrality_Pact.jpg "fig:Molotov_signs_the_Soviet–Japanese_Neutrality_Pact.jpg")签署《苏日中立条约》\]\]
《**蘇日中立條約**》，又稱**日蘇中立條約**、**蘇日互不侵犯條約**、**日蘇互不侵犯條約**（，），指[蘇聯與](../Page/蘇聯.md "wikilink")[大日本帝國於](../Page/大日本帝國.md "wikilink")[第二次世界大戰期間為了互相保證戰事維持中立而於](../Page/第二次世界大戰.md "wikilink")1941年4月13日所簽訂的條約，由[蘇聯人民委員會主席兼外交人民委員](../Page/蘇聯人民委員會.md "wikilink")[莫洛托夫與日本外相](../Page/莫洛托夫.md "wikilink")[松岡洋右在](../Page/松岡洋右.md "wikilink")[莫斯科簽署](../Page/莫斯科.md "wikilink")，條約有效期爲五年的條約，於同年4月25日起生效。

條約共有四款，主要內容包括蘇日雙方保持和平友好關係，相互尊重對方之領土完整，不予侵犯；如果締約一方成為第三者的戰爭對象，另一方應在整個衝突過程中保持中立。但，条约正文内容并没有涉及签约双方的具体领土的范围，也没有提及日方在中國東北所建立的[滿洲國的事宜](../Page/滿洲國.md "wikilink")。总体来说，条约在形式上，避免让苏军和日军发生直接军事对垒。对于苏联，避免了在东西两面同时受到1940年形成的[轴心国对其可能的军事进攻的威胁](../Page/轴心国.md "wikilink")；而日本也让苏联减缓了自1937年以来，对中国[抗日战争所提供的巨额军事援助](../Page/抗日战争.md "wikilink")。

当年，中共对《苏日中立条约》表态：“保证了外蒙不受侵犯，这不但对外蒙有利，即对全中国争取解放也是有利的。说到东四省的收复，原是我们自己的事”，并为此发表社论。雙方簽署的原因與當時的形勢有關。蘇聯方面，為了避免一方面要抵抗德軍又要抵抗日軍而陷入兩線作戰的困境，而決定以减缓对中國提供抗日所需的军事援助，来避免日本北上侵蘇。而日本方面自1937年全面侵[華後](../Page/中華民國.md "wikilink")，雖佔領了中國東部大片土地，但未能迅速擊敗中國軍隊，戰爭陷入膠著狀態。為了打破局面，當日軍分為[北進派與](../Page/北進派.md "wikilink")[南進派](../Page/南進派.md "wikilink")。北進是指北上與德國共同攻打蘇聯；南進則是揮軍南下侵襲[東南亞](../Page/東南亞.md "wikilink")。後來南進派佔優，故放棄北侵蘇聯。

1945年4月5日，莫洛托夫在莫斯科召見日本駐蘇大使佐藤，宣佈蘇聯在《蘇日中立條約》截止后將不再謀求續約，8月日本投降前夕，蘇聯對日宣戰，並派兵進入被关东军占领的中國东北與日本北方殖民的島嶼。

## 签约过程

1941年元旦，日本驻苏大使[建川美次提议](../Page/建川美次.md "wikilink")：两国可以订立互不侵犯条约。因苏联所提出的各项条件均不能使日本同意，谈判结束。1941年3月12日外相[松冈洋右赴苏访问](../Page/松冈洋右.md "wikilink")。3月22日，松冈一行抵达莫斯科，与苏联多位领导人会晤。　

## 中國共產黨表态

4月15日，重庆中共《新华日报》社论「论苏日中立条约」\[1\]：

4月16日，由毛澤東起草的「中國共產黨對蘇日中立條約發表意見」：

4月27日，延安中共《解放》期刊「苏日条约之伟大意义」：

## 各方影响

### 中国

《苏日中立条约》的签订，使得中国失去了苏联的官方援助。抗战爆发四年以来，中国至此失去了仅有的外界官方援助，陷入了孤立无援、最艰苦卓绝的阶段\[2\]。直到同年底[珍珠港事件导致](../Page/珍珠港事件.md "wikilink")[太平洋战争爆发](../Page/太平洋战争.md "wikilink")，美国停止了自1931年以来长期地对日本侵华的战略物资援助，而改为对日宣战、并转而开始援华，中国抗战的外部国际形势才得以缓解，走出最低谷。一種觀點廣泛傳播，西方社論作者、評論家和一般知識階層幾乎普遍接受：中國共產黨根本不是真正的共產主義者，他們只是一些土地改革家而已；被歪曲有關共產黨之觀點成為美國國務院和白宮接受之教條，面對蔣介石本人所作之宣傳卻沒有什麼效果\[3\]。

### 德国

日本不顾纳粹德国的反对而独自与苏联媾和，极大地破坏了轴心三国同盟。《苏日中立条约》签订仅仅两个月后，6月22日纳粹德国就发动突袭苏联的[巴巴羅薩行動](../Page/巴巴羅薩行動.md "wikilink")，二战苏德战场爆发。

### 苏联

[波茨坦会议时](../Page/波茨坦会议.md "wikilink")，出于苏日中立条约还在有效期的理由，要求美国政府出具正式的请求苏联出兵攻击日本的文件\[4\]。

## 后续

1945年4月5日，蘇聯單方面宣佈不再謀求續約，并更于8月出兵攻击日本关东军。

## 参考文献

## 外部链接

  - [中野文库中收录的条约全文](http://www.geocities.jp/nakanolib/joyaku/js16-6.htm)
  - [日本驻俄罗斯大使馆网站的条约全文](http://www.ru.emb-japan.go.jp/RELATIONSHIP/MAINDOCS/1941.html)
  - [远东国际军事法庭的审判记录，1948.11.4
    - 1948.11.12（判决）](http://www.jacar.go.jp/DAS/meta/image_A08071311400?IS_STYLE=default&IS_KEY_S1=F2008081909480862878&IS_KIND=MetaFolder&IS_TAG_S1=FolderId&)
    - 日本国立公文书馆

[Category:蘇聯條約](../Category/蘇聯條約.md "wikilink")
[Category:日本條約](../Category/日本條約.md "wikilink")
[Category:1941年建立](../Category/1941年建立.md "wikilink")
[Category:1945年廢除](../Category/1945年廢除.md "wikilink")
[Category:第二次世界大战](../Category/第二次世界大战.md "wikilink")
[Category:戰前昭和時代外交](../Category/戰前昭和時代外交.md "wikilink")

1.
2.  李嘉谷，[苏日中立条约签订的国际背景及其对中苏关系的影响](http://cc.sjtu.edu.cn/G2S/eWebEditor/uploadfile/20110510211947_662202224636.pdf)，世界历史，2002年第4期.
3.
4.