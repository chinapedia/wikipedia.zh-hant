**林玉鳳**（
，\[1\]），[澳門立法會直選](../Page/澳門立法會.md "wikilink")[議員](../Page/議員.md "wikilink")、[新聞學學者](../Page/新聞學.md "wikilink")、[作家和](../Page/作家.md "wikilink")[教授](../Page/教授.md "wikilink")，她亦是[河北省政協以及澳門行政長官選舉委員會委員](../Page/河北省政協.md "wikilink")。

## 經歷

[林玉鳳議員辦事處.jpg](https://zh.wikipedia.org/wiki/File:林玉鳳議員辦事處.jpg "fig:林玉鳳議員辦事處.jpg")
林玉鳳祖籍[廣東](../Page/廣東.md "wikilink")[潮陽](../Page/潮陽.md "wikilink")，出生於澳門，成長於東[花地瑪堂區](../Page/花地瑪堂區.md "wikilink")（[黑沙灣馬場](../Page/黑沙灣馬場_\(澳門\).md "wikilink")、[台山和](../Page/台山\(澳門\).md "wikilink")[關閘](../Page/關閘\(澳門\).md "wikilink")）一帶。在[澳門大學主修中文傳意](../Page/澳門大學.md "wikilink")，取得文學士學位，並後於[中國人民大學取得法學碩士](../Page/中國人民大學.md "wikilink")（主修[新聞學](../Page/新聞學.md "wikilink")）學位，以及在[中國人民大學取得哲學博士](../Page/中國人民大學.md "wikilink")（主修新聞學）學位。

林玉鳳1992年底開始任職[澳門廣播電視有限公司節目部主持](../Page/澳門廣播電視有限公司.md "wikilink")，1994年7月至1997年8月，於[澳門廣播電視有限公司](../Page/澳門廣播電視有限公司.md "wikilink")(TDM)任職新聞記者及新聞節目主持，1997年9月起在[澳門大學任教](../Page/澳門大學.md "wikilink")，曾長期出任新聞與公共傳播課程主任，曾獲選為[中國王寬誠基金會青年學者](../Page/中國王寬誠基金會.md "wikilink")，[英國劍橋大學](../Page/英國劍橋大學.md "wikilink")[克萊爾學堂](../Page/克萊爾學堂.md "wikilink")([Clare
Hall](../Page/Clare_Hall.md "wikilink"))訪問學者。現職澳門大學社會科學及人文學院助理院長、傳播系助理教授兼本科學士學位課程主任。自[九十年代起曾為多份報紙雜誌的](../Page/九十年代.md "wikilink")[專欄作家](../Page/專欄作家.md "wikilink")，《[新生代](../Page/新生代.md "wikilink")》月刊社長。曾擔任[澳門廣播電視有限公司時事評論節目](../Page/澳門廣播電視有限公司.md "wikilink")《[風火台](../Page/風火台.md "wikilink")》、[澳門蓮花衛視Call](../Page/澳門蓮花衛視.md "wikilink")
in節目[澳門開講以及](../Page/澳門開講.md "wikilink")[澳門電台時事節目](../Page/澳門電台.md "wikilink")《縱橫天下》嘉賓主持。

2008年，[澳門公民力量成立](../Page/澳門公民力量.md "wikilink")，她任職理事長。2009年宣布以「[公民監察](../Page/公民監察.md "wikilink")」名義參選[立法會選舉](../Page/2009年澳門立法會選舉.md "wikilink")，得5329票，未有取得議席。2013年起出任澳門公民力量會長至今。2017年9月當選立法會議員。

### 學術成就

在歷史定位上，早期的學術界普遍認為1822年創刊的《[蜜蜂華報](../Page/蜜蜂華報.md "wikilink")》擁有「三個第一」的定論，分別為「[中國領土上出版的第一份近代](../Page/中國.md "wikilink")[報紙](../Page/報紙.md "wikilink")」、「[澳門歷史上第一份報紙](../Page/澳門歷史.md "wikilink")」和「外國人在中國領土上創辦的第一份報紙」\[2\]，然而有學者如澳門[歷史學家](../Page/歷史學家.md "wikilink")[湯開建和林玉鳳對相關說法表示懷疑](../Page/湯開建.md "wikilink")。2015年，林玉鳳提出新理據，指出目前可證的澳門第一份報章應是比《蜜蜂華報》早15年於1807創刊的《[消息日報](../Page/消息日報.md "wikilink")》（），認為《蜜蜂華報》的歷史定位應該修正為「目前可找到原件的在澳門出版的第一份報章」和「1820年葡萄牙立憲派革命以後在澳門出版的首份報刊」\[3\]\[4\]。

### 爭議事件

2010年6月3日，澳廣視編輯曹永賢在由澳廣視策略發展工作小組召集的第三場公聽會上點名指控林玉鳳於去年選舉期間干預新聞自由。曹永賢指林玉鳳不滿澳廣視記者高蓮敏在選舉期間問及她有關官委議員的傳言，並立即致電澳廣視總監投訴此事，涉嫌干預新聞自由\[5\]。同年6月6日，林玉鳳發表聲明\[6\]反駁曹永賢的指控，強調她爭取的，只是公平報導。林玉鳳認為，該名記者問及她是否因為求官不遂而參選議員本質等同散佈謠言，因為該「傳言」本身就不符事實，亦沒有「沒有確切可信的『爆料』來源（包括見諸文字的網上言論），在公開場合提問本身已等同在散佈傳言」\[7\]。曹永賢翌日亦於網上發表聲明，指傳言有多個消息來源，並非片面之詞，他質疑林玉鳳為何不把握機會在鏡頭前回應事件而訴諸澳廣視高層。

## 社會公職

  - 澳門特別行政區行政長官選舉委員會委員（2014年至今）
  - 中國河北省政協
  - 中華全國青年聯合會澳門區特邀常任委員（1996年至今）\[8\]
  - 澳門筆會副理事長（2001年至今）
  - 澳門青年聯合會副會長\[9\]
  - 澳門特別行政區政府婦女事務委員會委員 （2005-）\[10\]\[11\]
  - 澳門特別行政區政府舊區重整諮詢委員會委員 （2005-）\[12\]\[13\]
  - 澳門中華文化交流協會副理事長 （2005年至今）\[14\]

## 著作

  - 《中國近代報業的起點──澳門新聞出版史（1557-1840）》\[15\]
  - 文集《澳門，一覺醒來又四年》（2013，澳門日報出版社出版）
  - 散文集《一個人影，一把聲音》（2004，澳門日報出版社出版）
  - 詩集《詩·想》（2007）
  - 詩集《忘了》（1999，中國文聯出版社）
  - 詩集《假如你愛上了我》（1997，澳門五月詩社出版）
  - 詩合集《鏡海妙思》（1994，澳門五月詩社出版）

## 媒體節目

### 曾主持節目

  - [澳廣視晚間新聞](../Page/澳廣視新聞.md "wikilink")
  - 《[澳視新聞檔案](../Page/澳視新聞檔案.md "wikilink")》
  - [一二三事件](../Page/一二三事件.md "wikilink")30周年特輯

### 嘉賓參與

  - 《[澳視新聞檔案](../Page/澳視新聞檔案.md "wikilink")》(2011，[澳門廣播電視有限公司](../Page/澳門廣播電視有限公司.md "wikilink"))
  - 《[人在澳門](../Page/人在澳門.md "wikilink")》(2011，[澳門蓮花衛視](../Page/澳門蓮花衛視.md "wikilink"))
  - 《[風火台](../Page/風火台.md "wikilink")》(2011-2013，[澳門廣播電視有限公司](../Page/澳門廣播電視有限公司.md "wikilink"))
  - 《[澳門論壇](../Page/澳門論壇.md "wikilink")》(2011，[澳門廣播電視有限公司](../Page/澳門廣播電視有限公司.md "wikilink"))
  - 《[公民圓桌](../Page/公民圓桌.md "wikilink")》座談會(2010-2013，[澳門公民力量](../Page/澳門公民力量.md "wikilink"))

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部链接

  - [林玉鳳的博客／舞盡天涯](http://mypaper.pchome.com.tw/news/agnesmacau/)
  - [澳門大學網頁內連結](https://web.archive.org/web/20130520164330/http://www.umac.mo/fsh/comm/commlam.html)
  - [林玉鳳Facebook專頁](http://www.facebook.com/pages/%E6%9E%97%E7%8E%89%E9%B3%B3Agnes-Lam/149528495161334)
  - [學術研究活動](https://web.archive.org/web/20130520164330/http://www.umac.mo/fsh/comm/commlam.html)

[Category:澳門女性作家](../Category/澳門女性作家.md "wikilink")
[Category:澳門文化界人士](../Category/澳門文化界人士.md "wikilink")
[Category:澳門大學教授](../Category/澳門大學教授.md "wikilink")
[Category:澳門廣播主持人](../Category/澳門廣播主持人.md "wikilink")
[Category:澳門電視主持人](../Category/澳門電視主持人.md "wikilink")
[Category:澳門記者](../Category/澳門記者.md "wikilink")
[Category:河北省政協委員](../Category/河北省政協委員.md "wikilink")
[Category:澳門女性政治人物](../Category/澳門女性政治人物.md "wikilink")
[Category:中國人民大學校友](../Category/中國人民大學校友.md "wikilink")
[Category:澳門大學校友](../Category/澳門大學校友.md "wikilink")
[Category:澳門潮汕人](../Category/澳門潮汕人.md "wikilink")
[Y](../Category/林姓.md "wikilink")

1.
2.
3.
4.
5.  [澳視編輯指控林玉鳳曾致電高層干預
    (澳亞版)](http://www.youtube.com/watch?v=Ct8T5g5BbAI&feature=related)
6.  [我爭取的，只是公平報導](http://mypaper.pchome.com.tw/agnesmacau/post/1321041750)
7.  [我爭取的，只是公平報導](http://mypaper.pchome.com.tw/agnesmacau/post/1321041750)
8.
9.
10.
11.
12.
13.
14.
15.