**李凡秀**\[1\]（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。

[大學時修讀戲劇電影專業](../Page/大學.md "wikilink")，他初出道時是一名[電影演員](../Page/電影演員.md "wikilink")，由綠葉演員發展至到今天成為男主演，他擔當主角的[電影如](../Page/電影.md "wikilink")《超級明星甘先生》、《我的老婆是大佬3》中的精堪演出，令他成為[韓國](../Page/韓國.md "wikilink")[二十世紀末最受歡迎的](../Page/二十世紀.md "wikilink")[演員之一](../Page/演員.md "wikilink")。他是韓國不可多得的又出色又多產的影人，在韓國更被稱為「國民演員」。近年他的作品有增無減，後來更走向[電視界發展](../Page/電視劇.md "wikilink")，在2007年憑《[外科醫生奉達熙](../Page/外科醫生奉達熙.md "wikilink")》中的安中根[醫生一角走紅](../Page/醫生.md "wikilink")。

## 生日

  - [戶籍上](../Page/戶籍.md "wikilink")[出生日期](../Page/出生日期.md "wikilink")：1970年1月3日
  - 正確出生日期：1969年10月16日（[農曆](../Page/農曆.md "wikilink")，本人皆過此生日）/
    1969年11月25日（[國曆](../Page/國曆.md "wikilink")）

## 個人生活

2003年11月30日，與舊同窗朴曉雲舉行[婚禮](../Page/婚禮.md "wikilink")，但僅過了五個月，便因感情不和[離婚](../Page/離婚.md "wikilink")。

2009年11月3日，公開承認與小自己14歲的英文老師李允真熱戀，於2010年5月22日[結婚](../Page/結婚.md "wikilink")。2011年3月1日，女兒出生。2014年2月21日，兒子出生。

## 演出作品

### 電視劇

|                                          |                                          |                                                    |        |    |
| ---------------------------------------- | ---------------------------------------- | -------------------------------------------------- | ------ | -- |
| 年份                                       | 電視台                                      | 劇名                                                 | 角色     | 備註 |
| 1999年                                    | [SBS](../Page/SBS_\(韓國\).md "wikilink")  | [2002的愛戀之訊息](../Page/2002的愛戀之訊息.md "wikilink")     | 宋承憲的同事 |    |
| 2007年                                    | [MBC](../Page/文化廣播_\(韓國\).md "wikilink") | [外科醫生奉達熙](../Page/外科醫生奉達熙.md "wikilink")           | 安中根    |    |
| 2008年                                    | SBS                                      | [On Air](../Page/On_Air.md "wikilink")             | 張基俊    |    |
| 2010年                                    | SBS                                      | [Giant](../Page/巨人_\(韓國電視劇\).md "wikilink")        | 李江慕    |    |
| 2012年                                    | SBS                                      | [工薪族楚漢誌](../Page/工薪族楚漢誌.md "wikilink")             | 劉邦     |    |
| [MBC](../Page/文化廣播_\(韓國\).md "wikilink") | [Dr. JIN](../Page/Dr._JIN.md "wikilink") | [李昰應](../Page/興宣大院君.md "wikilink")                 |        |    |
| 2013年                                    | [KBS](../Page/韓國放送公社.md "wikilink")      | [IRIS 2](../Page/IRIS_2.md "wikilink")             | 劉鍾元    |    |
| KBS                                      | [總理與我](../Page/總理與我.md "wikilink")       | 權律                                                 |        |    |
| 2014年                                    | MBC                                      | [Triangle](../Page/Triangle_\(電視劇\).md "wikilink") | 張東洙    |    |
| 2015年                                    | [JTBC](../Page/JTBC.md "wikilink")       | [LAST](../Page/LAST_\(電視劇\).md "wikilink")         | 郭興三    |    |
| 2016年                                    | KBS                                      | [武林學校](../Page/武林學校.md "wikilink")                 | 王豪     |    |

### 電影

<table>
<tbody>
<tr class="odd">
<td><p>年份</p></td>
<td><p>電影名稱</p></td>
<td><p>角色</p></td>
<td><p>備註</p></td>
</tr>
<tr class="even">
<td><p>1990年</p></td>
<td><p><a href="../Page/偶爾看一下天空.md" title="wikilink">偶爾看一下天空</a></p></td>
<td></td>
<td><p>（又譯：那麼，偶爾看看星空吧）</p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p><a href="../Page/17歲的政變.md" title="wikilink">17歲的政變</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p><a href="../Page/狗一樣的午后.md" title="wikilink">狗一樣的午后</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td><p><a href="../Page/銀杏床.md" title="wikilink">銀杏床</a></p></td>
<td></td>
<td><p>（又譯：銀杏樹床）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/幽靈媽媽.md" title="wikilink">幽靈媽媽</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p><a href="../Page/地上滿歌.md" title="wikilink">地上滿歌</a></p></td>
<td></td>
<td><p>（又譯：Elegy of the Earth／地上滿歌）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/傷心街角戀人.md" title="wikilink">傷心街角戀人</a></p></td>
<td><p>運送員</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p><a href="../Page/退魔錄_(電影).md" title="wikilink">退魔錄</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/男人的香氣.md" title="wikilink">男人的香氣</a></p></td>
<td></td>
<td><p>（又譯：哥哥的愛人／戀上不歸路）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/如果太陽從西邊出來.md" title="wikilink">如果太陽從西邊出來</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日出城市.md" title="wikilink">日出城市</a></p></td>
<td></td>
<td><p>（又譯：沒有太陽）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LOVE.md" title="wikilink">LOVE</a></p></td>
<td></td>
<td><p>（又譯：韓城直擊）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/新張開業.md" title="wikilink">新張開業</a></p></td>
<td></td>
<td><p>（又譯：新裝開店）</p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td><p><a href="../Page/天地男兒之激進黨員.md" title="wikilink">天地男兒之激進黨員</a></p></td>
<td></td>
<td><p>（又譯：無政府主義）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/發達不用本錢.md" title="wikilink">發達不用本錢</a></p></td>
<td><p>沅光泰</p></td>
<td><p>（又譯：發達不用本錢／賤色發達）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/情約笨跳豬.md" title="wikilink">情約笨跳豬</a></p></td>
<td><p>李大根</p></td>
<td><p>（又譯：發達唔使本／賤色發達）</p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/彈鋼琴的總統.md" title="wikilink">彈鋼琴的總統</a></p></td>
<td><p>流浪漢</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/叢林果汁.md" title="wikilink">叢林果汁</a></p></td>
<td></td>
<td><p>（又譯：逃亡二人組）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Very_Good_(電影).md" title="wikilink">Very Good</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夢精記之金老師.md" title="wikilink">夢精記之金老師</a></p></td>
<td></td>
<td><p>（又譯：夢精記／女仙）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天降橫財.md" title="wikilink">天降橫財</a></p></td>
<td></td>
<td><p>（又譯：我的大舊橫財／把他搞大／走為上策）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/單身貴族_(電影).md" title="wikilink">單身貴族</a></p></td>
<td><p>鄭俊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/OH!兄弟.md" title="wikilink">OH!兄弟</a></p></td>
<td><p>姜奉九</p></td>
<td><p>（又譯：憨豆兄弟）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英語完全征服.md" title="wikilink">英語完全征服</a></p></td>
<td></td>
<td><p>（客串）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/哈囉！UFO.md" title="wikilink">哈囉！UFO</a></p></td>
<td><p>尚賢</p></td>
<td><p>（又譯：你好！UFO／緣自UFO）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/超級明星_甘先生.md" title="wikilink">超級明星 甘先生</a></p></td>
<td><p>感思容</p></td>
<td><p>（又譯：超級明星 甘四用）</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/校園臥底.md" title="wikilink">校園臥底</a></p></td>
<td></td>
<td><p>（客串）（又譯：插班師姐／凸搥俏女警／逃學威風／臥底任務）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿達刑警.md" title="wikilink">阿達刑警</a></p></td>
<td><p>李大羅</p></td>
<td><p>（又譯：不能就這樣死）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/淫亂書生.md" title="wikilink">淫亂書生</a></p></td>
<td><p>廣憲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/暴力城市.md" title="wikilink">暴力城市</a></p></td>
<td><p>張畢鎬</p></td>
<td><p>（又譯：迷失罪惡城／同夥／夥伴）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/好好過日子.md" title="wikilink">好好過日子</a></p></td>
<td><p>卞石九</p></td>
<td><p>（又譯：Mission Sex Control／計劃生育／好好活著）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/醜女大翻身.md" title="wikilink">醜女大翻身</a></p></td>
<td><p>計程車司機</p></td>
<td><p>（特別出演）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我的老婆是老大3.md" title="wikilink">我的老婆是老大3</a></p></td>
<td><p>韓基哲</p></td>
<td><p>（又譯：我老婆係大佬3）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/姐姐走了.md" title="wikilink">姐姐走了</a></p></td>
<td><p>吳泰勛</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/血之期中考.md" title="wikilink">血之期中考－死題</a></p></td>
<td><p>黃昌旭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/他們來了.md" title="wikilink">他們來了</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/比悲傷更悲傷的故事.md" title="wikilink">比悲傷更悲傷的故事</a></p></td>
<td><p>車柱煥</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/舉起金剛.md" title="wikilink">舉起金剛</a></p></td>
<td><p>李志鋒</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭勝必失蹤事件.md" title="wikilink">鄭勝必失蹤事件</a></p></td>
<td><p>鄭承畢</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洪吉童的後裔.md" title="wikilink">洪吉童的後裔</a></p></td>
<td><p>洪茂赫</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/借屍還魂_(電影).md" title="wikilink">借屍還魂</a></p></td>
<td><p>白賢哲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/神之一手.md" title="wikilink">神之一手</a></p></td>
<td><p>殺手</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p><a href="../Page/愛上變身情人.md" title="wikilink">愛上變身情人</a></p></td>
<td><p>金禹鎮</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p><a href="../Page/純情_(電影).md" title="wikilink">純情</a></p></td>
<td><p>容哲</p></td>
<td><p>特別出演</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/代號：鐵鉻行動.md" title="wikilink">-{zh-hant:;zh-tw:代號：鐵鉻行動;zh-hk:仁川登陸戰;}-</a></p></td>
<td><p>林桂鎮</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/作錢.md" title="wikilink">作錢</a></p></td>
<td><p>車基範</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td></td>
<td><p>黃載浩</p></td>
<td></td>
</tr>
</tbody>
</table>

### 綜藝節目

  - 2005年8月27日：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[無謀挑戰](../Page/無限挑戰.md "wikilink")》（推土機
    vs 人工推車）
  - 2012年1月22日：[SBS](../Page/SBS株式會社.md "wikilink")《[Running
    Man](../Page/Running_Man.md "wikilink")》（Ep.78 楚漢志）
  - 2016年2月14日－2017年5月14日：[KBS](../Page/韓國放送公社.md "wikilink")《[超人回來了](../Page/超人歸來_\(韓國綜藝\).md "wikilink")》（自第117期出演至第182期）\*2019年
    : 《[认识的哥哥](../Page/认识的哥哥.md "wikilink")》 (Ep.167)

### MV

  - 2001年：Brown
    Eyes《已過一年》\[2\]（與[金賢珠](../Page/金賢珠.md "wikilink")、[張震](../Page/張震_\(演員\).md "wikilink")、[鄭殷杓](../Page/鄭殷杓.md "wikilink")）
  - 2002年：Brown Eyes《漸漸》（與金賢珠、張震）
  - 2004年：M.C The Max《你強忍住淚水》（電影《哈囉！UFO》插曲）
  - 2005年：金章勳《幸福嗎》（與[崔貞媛](../Page/崔貞媛.md "wikilink")、[柳承龍](../Page/柳承龍.md "wikilink")）
  - 2006年：[SG
    Wannabe](../Page/SG_Wannabe.md "wikilink")《愛情歌》（與[南奎里](../Page/南奎里.md "wikilink")）
  - 2007年：SG
    Wannabe《阿里郎》（與[玉珠鉉](../Page/玉珠鉉.md "wikilink")、[李善均](../Page/李善均.md "wikilink")）
  - 2008年：金鍾旭《壞男人》（與[韓銀貞](../Page/韓銀貞.md "wikilink")、[孔賢珠](../Page/孔賢珠.md "wikilink")）
  - 2009年：[M TO M](../Page/M_TO_M.md "wikilink")《Good Bye》（與韓銀貞）
  - 2009年：李承哲《再也沒有這樣的人》（與[權相佑](../Page/權相佑.md "wikilink")、[李寶英](../Page/李寶英.md "wikilink")，《[比悲傷更悲傷的故事](../Page/比悲傷更悲傷的故事.md "wikilink")》OST）

## 導演作品

  - 2013年：《傀儡》（禁煙社會電影）

## 音樂作品

  - 2008年：《[On Air](../Page/On_Air.md "wikilink")》OST － 島
  - 2012年：《[Dr. JIN](../Page/Dr._JIN.md "wikilink")》OST － 隨風而行

## 獎項

  - 2006年：第14届[春史電影賞](../Page/春史電影賞.md "wikilink")－男优助演赏
  - 2006年：第5届大韩民国电影大赏－男优助演赏
  - 2007年：第43届[百想艺术大赏](../Page/百想艺术大赏.md "wikilink")－电视部门 男子人气奖
  - 2007年：第44届[大鐘獎](../Page/大鐘獎.md "wikilink")－国内人气奖
  - 2007年：[SBS演技大赏](../Page/SBS演技大赏.md "wikilink")－最佳情侣赏、导演赏、十大明星赏
  - 2007年：第八屆韩国电影演技大赏－特别演技奖
  - 2009年：第29届 韩国电影评论家协会颁奖典礼－男主角奖
  - 2009年：第32届 黄金摄影影片奖－人气男演员奖
  - 2010年：第18屆韩国文化演艺大奖－藝人界大賞
  - 2010年：SBS演技大賞－特别企划部门 最优秀演员奖、十大明星奖
  - 2011年：第二届大韩民国首尔文化艺术大奖－电视演员大赏
  - 2013年：[KBS演技大赏](../Page/KBS演技大赏.md "wikilink")－最佳情侣奖《总理和我》
  - 2016年：[KBS演藝大賞](../Page/KBS演藝大賞.md "wikilink")－藝能部門優秀賞《[超人回來了](../Page/超人歸來_\(韓國綜藝\).md "wikilink")》

## 腳註

## 外部連結

  -
  - [韓國官方影迷CAFE](http://cafe.daum.net/bumsoolove)

  - [EPG](https://web.archive.org/web/20070826223738/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=5033)


[L](../Category/韓國電視演員.md "wikilink")
[L](../Category/韓國電影演員.md "wikilink")
[L](../Category/韓國中央大學校友.md "wikilink")
[L](../Category/高麗大學校友.md "wikilink")
[L](../Category/忠清北道出身人物.md "wikilink")
[L](../Category/清州市出身人物.md "wikilink")
[L](../Category/李姓.md "wikilink")

1.  [韓國電影數據庫李凡秀個人頁面](http://www.kmdb.or.kr/vod/mm_basic.asp?person_id=00000979)KMDb
2.  [許慧欣](../Page/許慧欣.md "wikilink")《愛情抗體》之原曲