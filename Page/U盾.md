**U盾**（**USBKey**）是[中国工商银行推出的一种用于在网络上以客户端的形式识别客户身份的](../Page/中国工商银行.md "wikilink")[数字证书](../Page/数字证书.md "wikilink")。它结合[IC卡与](../Page/IC卡.md "wikilink")[数字证书](../Page/数字证书.md "wikilink")、[数字签名等技术](../Page/数字签名.md "wikilink")。

U盾采用1024位的非对称[RSA加密算法](../Page/RSA加密算法.md "wikilink")。U盾的私钥（唯一序列号）直接在IC卡中产生，将[CA中心颁发的安全数字证书下载到卡中与私钥相对应](../Page/CA中心.md "wikilink")。

用户在使用[网上银行系统时](../Page/网上银行.md "wikilink")，通过U盾的加密通道确认客户身份。在进行网上交易时，数据先送入插入电脑[USB接口的U盾](../Page/USB.md "wikilink")，并使用私钥进行数字签名，然后再传送到网上银行进行验证。

[USBKey.png](https://zh.wikipedia.org/wiki/File:USBKey.png "fig:USBKey.png")

## 外部链接

  -
  -
[Category:网络安全](../Category/网络安全.md "wikilink")
[Category:支付系统](../Category/支付系统.md "wikilink")
[Category:銀行術語](../Category/銀行術語.md "wikilink")
[Category:中国工商银行](../Category/中国工商银行.md "wikilink")