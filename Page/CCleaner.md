**CCleaner**是由英國[軟體公司](../Page/軟體公司.md "wikilink")[Piriform開發的一款](../Page/Piriform.md "wikilink")[免費軟體](../Page/免費軟體.md "wikilink")，可從[Windows及其他軟件](../Page/Microsoft_Windows.md "wikilink")（如[Firefox](../Page/Firefox.md "wikilink")、[Opera](../Page/Opera.md "wikilink")、[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")、[QuickTime](../Page/QuickTime.md "wikilink")、[Windows
Media
Player等](../Page/Windows_Media_Player.md "wikilink")）中清理不必要的垃圾檔案。該軟件亦可找出及修正[Windows登錄中的問題包括不再使用的](../Page/登錄.md "wikilink")[副檔名及程式路徑的問題等](../Page/副檔名.md "wikilink")。另外它亦可幫助使用者移除軟件、管理系統還原點及修改系統啟動時開啟的程式。

## 沿革

2017年7月，安全软件厂商 [avast](../Page/avast.md "wikilink") 收购 Piriform。\[1\]

## 後門事件

2017年9月12日，Piriform的母公司[Avast發現同年](../Page/Avast.md "wikilink")8月釋出的CCleaner版本v5.33.6162和CCleaner
Cloud
版本v1.07.3191被植入[惡意程式](../Page/惡意程式.md "wikilink")，约200万用户计算机已遭感染，[駭客可能以此遠端控制](../Page/駭客.md "wikilink")32位系统的计算机，該公司已緊急更新版本並呼籲使用者盡速更新。\[2\]\[3\]\[4\]

## 參看

  - [Recuva](../Page/Recuva.md "wikilink")──Piriform開發的檔案修復軟體（[網站](http://www.piriform.com/recuva/)）
  - [Defraggler](../Page/Defraggler.md "wikilink")──Piriform開發的磁碟重組軟體（[網站](http://www.piriform.com/defraggler/)）
  - [Speccy](../Page/Speccy.md "wikilink")──Piriform開發的系統硬件資訊偵測軟體（[網站](http://www.piriform.com/speccy/)）

## 參考資料

## 外部連結

  - [CCleaner Official Site](https://www.ccleaner.com)

[Category:Windows卸载程序](../Category/Windows卸载程序.md "wikilink")
[Category:数据擦除](../Category/数据擦除.md "wikilink")
[Category:Windows实用程序](../Category/Windows实用程序.md "wikilink")
[Category:免费软件](../Category/免费软件.md "wikilink")
[Category:2003年软件](../Category/2003年软件.md "wikilink")

1.
2.  [系統清理軟體CCleaner被駭
    可能控制你的電腦](https://www.inside.com.tw/article/10552-avast-ccleaner-gets-hit-by-a-nasty-malware-infection)
3.
4.