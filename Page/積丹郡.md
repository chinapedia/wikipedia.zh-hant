**積丹郡**（）為[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[後志綜合振興局的郡](../Page/後志綜合振興局.md "wikilink")，位於後志綜合振興局西北的[積丹半島最北端](../Page/積丹半島.md "wikilink")。

## 轄區

轄區包含1町：

  - [積丹町](../Page/積丹町.md "wikilink")

## 沿革

[Shiribeshi-shicho.png](https://zh.wikipedia.org/wiki/File:Shiribeshi-shicho.png "fig:Shiribeshi-shicho.png")

  - 1869年8月15日：北海道設置11國86郡，[後志國積丹郡成立](../Page/後志國.md "wikilink")。
  - 1897年11月：北海道實施支廳制，此時石狩郡下轄入舸村、野塚村、日司村、出岬村、余別村、來岸村、神岬村、西河村。\[1\]（8村）
  - 1906年4月1日：[余別村](../Page/余別村.md "wikilink")（余別村、來岸村、神岬村、西河村合併而成）和[入舸村](../Page/入舸村.md "wikilink")（入舸村、野塚村、日司村、出岬村合併而成）成為北海道二級村。（2村）
  - 1924年4月1日：余別村成為北海道一級村。
  - 1943年6月1日：北海道一級二級町村制廢止，入舸村改為內務省指定村。
  - 1946年10月5日：指定町村制廢止。
  - 1956年9月30日：余別村、入舸村和[美國郡](../Page/美國郡.md "wikilink")[美國町](../Page/美國町.md "wikilink")[合併為積丹町](../Page/市町村合併.md "wikilink")。（1町）

## 參考資料

[Category:後志國](../Category/後志國.md "wikilink")
[Category:北海道的郡](../Category/北海道的郡.md "wikilink")

1.