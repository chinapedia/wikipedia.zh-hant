**郭芝苑**（），[台灣](../Page/台灣.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")[苑裡鎮人](../Page/苑裡鎮.md "wikilink")。台灣音樂作曲家，擁有多元而廣泛的創作類型及風格，包括[管弦樂](../Page/管弦樂.md "wikilink")、[歌劇](../Page/歌劇.md "wikilink")、[室內樂](../Page/室內樂.md "wikilink")、[協奏曲](../Page/協奏曲.md "wikilink")、[鋼琴曲](../Page/鋼琴曲.md "wikilink")、[合唱曲](../Page/合唱曲.md "wikilink")、藝術歌曲等，甚至連兒童歌曲及流行歌曲都有作品產生。\[1\]郭氏可說是台灣現代民族音樂的先驅，在台灣接觸西洋古典音樂之初，扮演著相當重要的角色。

## 前輩影響

郭氏曾多次在公開場合表示他對前輩江文也的景仰，江文也早年就以歌劇演唱在日本取得傑出成績，後來又在日本、德國的作曲競賽中屢獲佳績，江的音樂創作當中獨特的民族風格，為年少的郭氏留下深刻印象。直至他赴日留學時，終得與江見面，討論尼采哲學、波特萊爾詩作與馬拉美象徵主義等題，逐漸建立情誼，郭氏遂決定以全方位的作品致力於台灣民族音樂創作。兩人之間的關係猶如西方樂史上布魯克納對馬勒的精神影響。

## 台灣第一

  - 1955年，台灣省立交響樂團首次演出台灣作曲家的管弦樂作品：《台灣土風交響變奏曲》在中山堂首演，由王錫奇指揮。
  - 1956年1月，美國人首次聽到來自台灣的聲音：台北市與美國印地安納州波里斯市締盟，《台灣土風交響變奏曲》在交換演奏會上演出。
  - 1974年，李泰祥指揮、簡若芬彈奏演出《小協奏曲──為鋼琴與弦樂隊》，這是台灣音樂史上第一首鋼琴協奏曲。
  - 1977年，韓國人第一次聽到台灣作品：陳秋盛指揮省交訪韓，與釜山交響樂團合演《民俗組曲》。
  - 1985年，台灣歌劇首征法國：郭聯昌指揮國際管弦樂團，在夏隆棟劇院演出全本《牛郎織女》。

## 年表

  - 1921年，出生於台灣[新竹州](../Page/新竹州.md "wikilink")[苗栗郡](../Page/苗栗郡.md "wikilink")[苑裡庄](../Page/苑裡街.md "wikilink")。
  - 1934年，自公學校畢業後，1934年考進臺南長榮中學，在此開始接觸宗教詩歌，也參加學校的口琴隊。
  - 1935年，就讀東京[錦成中學校](../Page/錦成中學校.md "wikilink")，在音樂老師指導下，首度以[口琴嶄露音樂頭角](../Page/口琴.md "wikilink")，畢業該年獲得當時全東京中學生口琴比賽第一名。
  - 1941年，進入東京[東洋音樂學院](../Page/東洋音樂學院.md "wikilink")，原想一圓小時的夢想專攻小提琴。但因為左手小指先天向內彎曲，在按弦上有困難，也無法演奏鋼琴，於是改學作曲與樂理，從[古典樂開始](../Page/古典樂.md "wikilink")，到[國民樂派](../Page/國民樂派.md "wikilink")，再到[民族樂派](../Page/民族樂派.md "wikilink")。\[2\]
  - 1943年，東京東洋音樂學院畢業。
  - 1946年，回台。
  - 1950年，台灣省音樂比賽作曲第一名。
  - 1955年，發表第一首由台灣人創作的管弦樂曲《台灣土風交響變奏曲》。
  - 1959年，為台灣第一部台語音樂劇「阿三哥出馬」配樂。
  - 1966年，為學習理論作曲，再度就讀日本[國立東京藝術大學音樂部作曲科](../Page/國立東京藝術大學.md "wikilink")。
  - 1969年，於東京藝大結業時，仿[艾爾加](../Page/艾爾加.md "wikilink")「威風堂堂進行曲」的形式完成大型管絃樂進行曲「大台北」。
  - 1986年，青少年歌劇「牛郎織女」首次在巴黎演出。
  - 1987年，「小協奏曲──為鋼琴與弦樂器」獲金鼎獎作曲獎。自省立台灣交響樂團退休。
  - 1992年，管弦樂組曲〈回憶〉在國家音樂廳演出。
  - 1993年，獲頒[吳三連獎音樂類藝術獎](../Page/吳三連獎.md "wikilink")。
  - 1994年，獲頒舊制[第19屆國家文藝獎](../Page/國家文藝獎#第十九屆.md "wikilink")，得獎作品:交響組曲「天人師」獲國家文藝獎。
  - 1999年，獲頒「台灣文化榮譽博士」學位，歌劇「許仙與白娘娘」於[台北市立社會教育館首演](../Page/台北市立社會教育館.md "wikilink")。
  - 2000年，於台北市立社會教育館發表＜台灣吉慶序曲＞。
  - 2001年12月5日，[靜宜大學授予](../Page/靜宜大學.md "wikilink")「名譽博士學位」表彰對台灣民族音樂的貢獻。
  - 2002年，獲頒[第13屆金曲獎終身特別貢獻獎](../Page/第13屆金曲獎.md "wikilink")。
  - 2006年，獲頒[第十屆國家文藝獎](../Page/第十屆國家文藝獎.md "wikilink")。
  - 2006年，獲頒[行政院文化獎](../Page/行政院文化獎.md "wikilink")。郭芝苑因為不諳[中華民國國語](../Page/中華民國國語.md "wikilink")，因此從未在大學任教，但驕傲地說：「我的作曲就是我的地位。」\[3\]
  - 2013年4月12日，因大腸癌病逝於苗栗縣苑裡鎮為公路老家，享壽92歲。\[4\]\[5\]
  - 2013年4月28日，郭芝苑家屬舉行告別式，[文化部政務次長代表總統](../Page/中華民國文化部.md "wikilink")[馬英九頒發郭芝苑](../Page/馬英九.md "wikilink")[褒揚令](../Page/褒揚令.md "wikilink")，鎮長[杜文卿稱郭芝苑為](../Page/杜文卿.md "wikilink")「苑裡之光」、「苗栗音樂之父」\[6\]。

## 主要作品一覽

### 歌劇

  - 青少年歌劇《牛郎織女》
  - 輕歌劇《許仙與白娘娘》，1984年

### 管弦樂

  - 《交響變奏曲─臺灣土風為主題》，1955年\[7\]

<!-- end list -->

  - 《狂想曲「原住民的幻想」─為鋼琴與管絃樂》
  - 《臺灣旋律二樂章》
  - 《民俗組曲（回憶）》
  - 《大合唱與管弦樂─偉人的誕生》
  - 《大進行曲─大台北》
  - 《小協奏曲─為鋼琴與絃樂隊》1974年發表
      - 1973年完成，於1974年10月由當時的[省立交響樂團發表於](../Page/國立臺灣交響樂團.md "wikilink")[台中中興堂](../Page/台中中興堂.md "wikilink")，指揮是[李泰祥](../Page/李泰祥.md "wikilink")
  - 《三首臺灣民間音樂》【〈劍舞〉、〈南管〉、〈鬧廳〉】
  - 《民歌與採茶舞》
  - 《三首交響曲練習曲─由湖北民謠而來》
  - 《交響曲A調─唐山過臺灣》
  - 《天人師─釋迦傳上、下集》管弦樂組曲
  - 《臺灣吉慶序曲》
  - 《臺灣頌》

### 室內樂

  - 《豎笛與鋼琴小奏鳴曲》
  - 《為小號與鋼琴的三個樂章》

### 藝術歌曲

  - 《涼州詞》
  - 《紅薔薇》
  - 《楓橋夜泊》
  - 《前進！臺灣人》
  - …等等數十首

### 校歌

  - [國立苑裡高級中學](../Page/國立苑裡高級中學.md "wikilink")
  - [苗栗縣立苑裡高級中學](../Page/苗栗縣立苑裡高級中學.md "wikilink")
  - [新竹縣立新埔國民中學](../Page/新竹縣立新埔國民中學.md "wikilink")

### 流行歌

  - 《心內事無人知》由鳳飛飛演唱
  - 《初見的一日》由鳳飛飛演唱
  - 《歹命子》由劉福助演唱

## 參考文獻

  - 內文

<!-- end list -->

  - 全文

<!-- end list -->

  - 《郭芝苑：野地的紅薔薇》，吳玲宜撰文，時報出版。
  - 《Musik》No. 76, June 2013

## 相關詞條

  - [古典音樂作曲家列表](../Page/古典音樂作曲家列表.md "wikilink")
  - 台灣日治時期重要的台語流行音樂人（包含作詞人、作曲人、歌手），有：[蘇桐](../Page/蘇桐.md "wikilink")、[江中清](../Page/江中清.md "wikilink")、[陳秋霖](../Page/陳秋霖.md "wikilink")、[陳水柳](../Page/陳水柳.md "wikilink")、[陳達儒](../Page/陳達儒.md "wikilink")、[鄧雨賢](../Page/鄧雨賢.md "wikilink")、[王雲峰](../Page/王雲峰.md "wikilink")、[姚讚福](../Page/姚讚福.md "wikilink")、[林氏好](../Page/林氏好.md "wikilink")（女歌手）、[純純](../Page/純純_\(歌手\).md "wikilink")（女歌手）、[愛愛](../Page/愛愛.md "wikilink")（女歌手）、[葉俊麟](../Page/葉俊麟.md "wikilink")、[陳君玉](../Page/陳君玉.md "wikilink")、[許丙丁](../Page/許丙丁.md "wikilink")、[呂泉生](../Page/呂泉生.md "wikilink")、[吳成家](../Page/吳成家.md "wikilink")、[林清月](../Page/林清月.md "wikilink")、[李臨秋](../Page/李臨秋.md "wikilink")、[吳晉淮](../Page/吳晉淮.md "wikilink")、[楊三郎](../Page/楊三郎.md "wikilink")、[周添旺](../Page/周添旺.md "wikilink")、[那卡諾](../Page/那卡諾.md "wikilink")、郭芝苑、[許石](../Page/許石.md "wikilink")

## 外部連結

  - [郭芝苑 - 臺灣音樂群像資料庫 -
    國立傳統藝術中心](http://musiciantw.ncfta.gov.tw/list.aspx?p=M012&c=&t=1)
  - [郭芝苑 - 文化部國家文化資料庫](http://nrch.culture.tw/twpedia.aspx?id=21339)
  - [郭芝苑創作音樂| 臺灣戲曲中心（The Xiqu Center of
    Taiwan）](http://xiqucenter.culture.tw/Xiqu/ContemporaryCreation_16052416030724157.aspx)

### 閱讀

  - [顏綠芬](../Page/顏綠芬.md "wikilink")，[〈郭芝苑音樂作品中的台灣意識與人文關懷〉](http://www.tisanet.org/quarterly/7-3-3.pdf)pdf格式，2011，臺灣國際研究季刊。[摘要](http://www.airitilibrary.com/Publication/alDetailedMesh?DocID=18162622-201109-201206200012-201206200012-69-90)。[論文及評論](http://www.youtube.com/watch?v=KeVOSAqt3fY)(線上影音)
  - 郭芝苑，〈[我寫台語愛國歌曲](https://archive.is/20130706141537/http://www.libertytimes.com.tw/2002/new/jul/22/today-o1.htm%23o2#selection-391.1-391.4)〉，民國91年7月22日，自由廣場。

[Category:國家文藝獎得主](../Category/國家文藝獎得主.md "wikilink")
[Category:行政院文化獎得主](../Category/行政院文化獎得主.md "wikilink")
[Category:吳三連獎得主](../Category/吳三連獎得主.md "wikilink")
[Category:金曲獎特別貢獻獎獲得者](../Category/金曲獎特別貢獻獎獲得者.md "wikilink")
[Category:台灣作曲家](../Category/台灣作曲家.md "wikilink")
[Category:東京藝術大學校友](../Category/東京藝術大學校友.md "wikilink")
[Category:苑裡人](../Category/苑裡人.md "wikilink")
[Zhi芝](../Category/郭姓.md "wikilink")
[Category:罹患大腸癌逝世者](../Category/罹患大腸癌逝世者.md "wikilink")

1.  〈國寶級作曲家郭芝苑〉，阮文池，靜宜大學音樂系講師。
2.  民視新聞網·台灣筆記-2007.05.14
3.  2006年5月19日東森新聞
4.
5.
6.
7.