*关于佛经中的波罗奈河，请参见[瓦拉纳西河](../Page/瓦拉纳西河.md "wikilink")*

**波罗奈河**（），[日本称](../Page/日本.md "wikilink")**幌内川**，是[俄罗斯](../Page/俄罗斯.md "wikilink")[库页岛中北部主要河流](../Page/库页岛.md "wikilink")，向南流入[捷尔佩尼耶湾](../Page/捷尔佩尼耶湾.md "wikilink")，全长320公里。

[P](../Category/俄罗斯河流.md "wikilink")
[Category:庫頁島](../Category/庫頁島.md "wikilink")