**鄭景益**（），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[誠泰COBRAS隊](../Page/誠泰COBRAS.md "wikilink")，守備位置為[一壘手](../Page/一壘手.md "wikilink")。

## 經歷

  - [臺北縣板橋國小少棒隊](../Page/新北市.md "wikilink")（榮工）
  - [臺北縣板橋國中青少棒隊](../Page/新北市.md "wikilink")（榮工）
  - [臺北縣中華中學青棒隊](../Page/新北市.md "wikilink")（榮工）
  - [文化大學棒球隊](../Page/文化大學.md "wikilink")（聲寶巨人）
  - 聲寶巨人棒球隊
  - [台灣大聯盟](../Page/台灣大聯盟.md "wikilink")[高屏雷公隊](../Page/高屏雷公.md "wikilink")（1997年－2002年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰太陽隊](../Page/誠泰太陽.md "wikilink")（2003年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰COBRAS隊](../Page/誠泰COBRAS.md "wikilink")（2004年－2006年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[誠泰COBRAS隊二軍總教練](../Page/誠泰COBRAS.md "wikilink")（2007年－2008年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[米迪亞暴龍隊二軍總教練](../Page/米迪亞暴龍.md "wikilink")（2008年）
  - [緯來體育台棒球球評](../Page/緯來體育台.md "wikilink")
  - [愛爾達體育台棒球球評](../Page/愛爾達電視.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                                         | 出賽  | 打數  | 安打  | 全壘打 | 打點 | 四死 | 三振  | 盜壘 | 壘打數 | 雙殺打 | 打擊率   |
| ----- | ------------------------------------------ | --- | --- | --- | --- | -- | -- | --- | -- | --- | --- | ----- |
| 2003年 | [誠泰太陽](../Page/誠泰太陽.md "wikilink")         | 96  | 282 | 62  | 8   | 37 | 0  | 30  | 83 | 92  | 2   | 0.220 |
| 2004年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 67  | 143 | 31  | 5   | 24 | 0  | 27  | 39 | 56  | 2   | 0.217 |
| 2005年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 34  | 49  | 10  | 2   | 9  | 0  | 7   | 16 | 19  | 2   | 0.204 |
| 2006年 | [誠泰COBRAS](../Page/誠泰COBRAS.md "wikilink") | 16  | 25  | 3   | 0   | 3  | 0  | 7   | 12 | 4   | 0   | 0.120 |
| 合計    | 4年                                         | 213 | 499 | 106 | 15  | 73 | 0  | 150 | 27 | 171 | 6   | 0.212 |

## 特殊事蹟

電影《條子阿布拉》中 飾演 體育老師 （王老師）

## 外部連結

[Category:台灣棒球選手](../Category/台灣棒球選手.md "wikilink")
[Category:高屏雷公隊球員](../Category/高屏雷公隊球員.md "wikilink")
[Category:米迪亞暴龍隊教練](../Category/米迪亞暴龍隊教練.md "wikilink")
[Category:中國文化大學校友](../Category/中國文化大學校友.md "wikilink")
[Category:誠泰COBRAS隊球員](../Category/誠泰COBRAS隊球員.md "wikilink")
[Jing景益](../Category/鄭姓.md "wikilink")