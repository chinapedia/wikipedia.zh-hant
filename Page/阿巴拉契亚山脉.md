[AppalachianLocatorMap2.png](https://zh.wikipedia.org/wiki/File:AppalachianLocatorMap2.png "fig:AppalachianLocatorMap2.png")
[Wild_Turkey-27527-1.jpg](https://zh.wikipedia.org/wiki/File:Wild_Turkey-27527-1.jpg "fig:Wild_Turkey-27527-1.jpg")、[白尾鹿](../Page/白尾鹿.md "wikilink")、[駝鹿](../Page/駝鹿.md "wikilink")、[東加拿大狼](../Page/東加拿大狼.md "wikilink")、[北美山獅](../Page/北美山獅.md "wikilink")、[美洲黑熊及圖中的](../Page/美洲黑熊.md "wikilink")[野火雞等](../Page/野生火雞.md "wikilink")\]\]
**-{zh-hans:阿巴拉契亚山脉;zh-tw:阿帕拉契山脈}-**（），又譯**-{zh-hans:阿帕拉契山脉;zh-tw:阿巴拉契亞山脈}-**，是[北美洲东部的一座山系](../Page/北美洲.md "wikilink")。南起[美国的](../Page/美国.md "wikilink")[阿拉巴马州](../Page/阿拉巴马州.md "wikilink")，北至[加拿大的纽芬兰和拉布拉多省](../Page/加拿大.md "wikilink")。最北部-{zh:余;zh-hans:余;zh-hant:餘;}-脉则延伸到[魁北克的加斯佩地区](../Page/魁北克.md "wikilink")。最高峰在[北卡罗莱纳州的](../Page/北卡罗莱纳州.md "wikilink")[米切尔峰](../Page/米切尔峰.md "wikilink")（2037[米](../Page/米.md "wikilink")）。

构成阿巴拉契亚山脉的有[纽芬兰省的长岭山](../Page/纽芬兰省.md "wikilink")、[魁北克的圣母山](../Page/魁北克.md "wikilink")、[缅因州的](../Page/缅因州.md "wikilink")[朗费罗山](../Page/朗费罗山.md "wikilink")、[新罕布夏州的](../Page/新罕布夏州.md "wikilink")[怀特山](../Page/白山山脈_\(新罕布夏州\).md "wikilink")、[佛蒙特州的](../Page/佛蒙特州.md "wikilink")[格林山脈](../Page/格林山脈.md "wikilink")、[塔库尼克山](../Page/塔库尼克山.md "wikilink")；[马萨诸塞州的](../Page/马萨诸塞州.md "wikilink")[勃克夏山](../Page/勃克夏山.md "wikilink")；跨[宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink")、[马里兰州和](../Page/马里兰州.md "wikilink")[西佛吉尼亚州三州的](../Page/西佛吉尼亚州.md "wikilink")[阿莱干尼山](../Page/阿莱干尼山.md "wikilink")；跨宾夕法尼亚州、马里兰州、西佛吉尼亚州以及[佛吉尼亚州四州的](../Page/佛吉尼亚州.md "wikilink")[阿巴拉契亚岭谷](../Page/阿巴拉契亚岭谷.md "wikilink")。还有从宾夕法尼亚州南部到[佐治亚州北部的](../Page/佐治亚州.md "wikilink")[蓝岭山脈](../Page/蓝岭山脈.md "wikilink")。

实际上*阿巴拉契高地*
严格的边界範圍存有争议，[阿第倫達克山脈一般被认为是属于](../Page/阿第倫達克山脈.md "wikilink")[加拿大地盾](../Page/加拿大地盾.md "wikilink")，而非阿帕拉契高地。

## 相關條目

  - [阿巴拉契亞](../Page/阿巴拉契亞.md "wikilink")

## 参考资料

[\*](../Category/阿帕拉契山脈.md "wikilink")
[Category:美國山脈](../Category/美國山脈.md "wikilink")
[Category:加拿大山脈](../Category/加拿大山脈.md "wikilink")
[Category:北美洲山脈](../Category/北美洲山脈.md "wikilink")
[Category:緬因州地理](../Category/緬因州地理.md "wikilink")
[Category:佛蒙特州地理](../Category/佛蒙特州地理.md "wikilink")
[Category:新罕布夏州地理](../Category/新罕布夏州地理.md "wikilink")
[Category:麻薩諸塞州地理](../Category/麻薩諸塞州地理.md "wikilink")
[Category:康乃狄克州地理](../Category/康乃狄克州地理.md "wikilink")
[Category:紐約州地理](../Category/紐約州地理.md "wikilink")
[Category:新澤西州地理](../Category/新澤西州地理.md "wikilink")
[Category:賓夕法尼亞州地理](../Category/賓夕法尼亞州地理.md "wikilink")
[Category:馬里蘭州地理](../Category/馬里蘭州地理.md "wikilink")
[Category:維吉尼亞州地理](../Category/維吉尼亞州地理.md "wikilink")
[Category:西維吉尼亞州地理](../Category/西維吉尼亞州地理.md "wikilink")
[Category:肯塔基州地理](../Category/肯塔基州地理.md "wikilink")
[Category:田納西州地理](../Category/田納西州地理.md "wikilink")
[Category:北卡羅來納州地理](../Category/北卡羅來納州地理.md "wikilink")
[Category:南卡羅來納州地理](../Category/南卡羅來納州地理.md "wikilink")
[Category:喬治亞州地理](../Category/喬治亞州地理.md "wikilink")
[Category:阿拉巴馬州地理](../Category/阿拉巴馬州地理.md "wikilink")
[Category:魁北克省地理](../Category/魁北克省地理.md "wikilink")
[Category:新斯科細亞省地理](../Category/新斯科細亞省地理.md "wikilink")
[Category:新不倫瑞克省地理](../Category/新不倫瑞克省地理.md "wikilink")
[Category:加拿大地形区](../Category/加拿大地形区.md "wikilink")
[Category:美国地形区](../Category/美国地形区.md "wikilink")
[Category:阿巴拉契亚](../Category/阿巴拉契亚.md "wikilink")
[Category:阿巴拉契亞文化](../Category/阿巴拉契亞文化.md "wikilink")