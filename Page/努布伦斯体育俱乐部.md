**努布伦斯体育俱乐部**（**Club de Deportes
Ñublense**）是[智利足球俱乐部](../Page/智利.md "wikilink")，位于智利小城[奇康](../Page/奇康.md "wikilink")。球队现为[智利足球甲级联赛球队](../Page/智利足球甲级联赛.md "wikilink")。

## 球队荣誉

  - **[智利足球乙级联赛](../Page/智利足球乙级联赛.md "wikilink")**冠军：1976年
  - **智利足球乙级联赛**春季冠军：1971年
  - **[智利足球丙级联赛](../Page/智利足球丙级联赛.md "wikilink")**冠军：1986年, 1992年, 2004年

## 外部链接

  - [Fans Website](http://www.losdiablosrojos.cl/)

[Category:智利足球俱乐部](../Category/智利足球俱乐部.md "wikilink")