**[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")**[網頁瀏覽器最初是由](../Page/網頁瀏覽器.md "wikilink")[Mozilla
Application
Suite所分支的的實驗性專案](../Page/Mozilla_Application_Suite.md "wikilink")，由[戴夫·海厄特及](../Page/戴夫·海厄特.md "wikilink")[布雷克·罗斯所](../Page/布雷克·罗斯.md "wikilink")-{zh-hans:创建;
zh-hant:建立}-。Firefox 1.0於2004年11月9日發行。

## 命名過程

### Phoenix

[Phoenix_0.1.png](https://zh.wikipedia.org/wiki/File:Phoenix_0.1.png "fig:Phoenix_0.1.png")
Firefox起源於叫做「m／b」（或稱mozilla／browser）的[Mozilla
Suite實驗性分支](../Page/Mozilla_Suite.md "wikilink")，2002年9月，以Phoenix為名，釋出測試用執行檔而正式問世。Phoenix朝著和其上游的Mozilla
Suite完全不同的開發方向進行。相較於Mozilla
Suite是以資深工程師為首，組織大型開發團隊的大型應用軟體，Phoenix的開發成員，則是特別專注於網頁瀏覽功能的小型核心式團隊。這種小型志願專案的形式源自於更早期，[戴夫·海厄特與](../Page/戴夫·海厄特.md "wikilink")[本·古德傑以](../Page/本·古德傑.md "wikilink")[Netscape和](../Page/網景_\(瀏覽器\).md "wikilink")[Internet
Explorer為基礎](../Page/Internet_Explorer.md "wikilink")，使用[C♯和](../Page/C♯.md "wikilink")[.NET語言撰寫的Manticore瀏覽器](../Page/.NET.md "wikilink")。Manticore意在以精幹的軟體型態，提供基本瀏覽功能；而[布雷克·羅斯和戴夫](../Page/布雷克·羅斯.md "wikilink")·海厄特的Phoenix計畫，則著眼於瀏覽體驗的創新。致力於將安全及效能作為支柱，且不被Netscape的商業心態的約束，才能開發出一個「完全替終端使用者量身訂造」的瀏覽器。Phoenix這個名字一直使用至2003年4月14日，因為與[BIOS製造商Phoenix](../Page/BIOS.md "wikilink")
Technologies的商標糾紛而更名\[1\]\[2\]。

### Firebird

在2003年4月，Mozilla宣佈他們的新瀏覽器將命名為Firebird，以避開與Phoenix的爭議。Firebird是個通過自焚重生的神話生物[不死鳥](../Page/不死鳥.md "wikilink")，有時也被認為是[鳳凰的別稱](../Page/鳳凰.md "wikilink")。這個新名字引起了許多不同的反應，特別是激起了早以[Firebird為名的自由軟體資料庫專案強烈反彈](../Page/Firebird_\(資料庫\).md "wikilink")。因應於此，[Mozilla基金會亦表示他們將會永遠使用](../Page/Mozilla基金會.md "wikilink")「Mozilla
Firebird」的名稱，以避免和Firebird資料庫混淆\[3\]\[4\]。

### Firefox

來自Firebird社群的持續壓力，迫使2004年2月9日的另一次改變，Mozilla Phoenix計劃再次更名成Mozilla
Firefox（簡稱Firefox）。「Firefox」是「小熊貓」的暱稱，因為它和「Firebird」的相似性、以及它在資訊產業中的獨特性而雀屏中選。

為了確保不會再有變更名稱的問題，Mozilla基金會在2003年12月向[美國專利商標局申請註冊Firefox商標](../Page/美國專利商標局.md "wikilink")\[5\]。不幸的是此時發現，在英國Firefox已是Charlton
Company軟體公司的註冊商標。這個問題導致Firefox 0.8延遲了幾個月釋出，在Mozilla基金會取得Charlton
Company的歐洲商標使用權後順利解決\[6\]\[7\]。

Mozilla官方建議的Firefox縮寫为[Fx或fx](../Page/Mozilla_Firefox.md "wikilink")\[8\]，但一般大眾有時寫成而并非想当然的[FF](../Page/FF.md "wikilink")
\[9\]。雖然firefox在英文俗語中指的是[小熊貓](../Page/小熊貓.md "wikilink")，但[吉祥物及官方圖示都是](../Page/吉祥物.md "wikilink")[狐狸](../Page/狐狸.md "wikilink")。

## 早期版本

Firefox保留Mozilla Application
Suite原有的跨平台特性，使用了[XUL使用者介面語言](../Page/XUL.md "wikilink")。XUL的運用使得能利用[佈景主題與擴充套件來延伸瀏覽器功能](../Page/皮肤_\(电脑\).md "wikilink")。然而附加元件的開發與安裝衍生了一些安全性問題，因此在Firefox
0.9釋出時，Mozilla基金會同時公開Mozilla Update網站（即後來的[Mozilla
Add-ons網站](../Page/Mozilla_Add-ons.md "wikilink")）來提供「合法的」佈景主題與擴充套件。XUL的運用將Firefox從其它使用Gecko排版引擎開發的瀏覽器（如[Galeon](../Page/Galeon.md "wikilink")、[Epiphany使用](../Page/Epiphany.md "wikilink")[GTK+](../Page/GTK+.md "wikilink")、[K-Meleon使用](../Page/K-Meleon.md "wikilink")[MFC與](../Page/MFC.md "wikilink")[Camino使用](../Page/Camino.md "wikilink")[Cocoa](../Page/Cocoa.md "wikilink")）及大多數其它使用原生作業系統介面的瀏覽器中區別開來。

2004年2月5日，商業和IT顧問公司AMS將Mozilla Firefox（當時稱為Firebird）歸類為「Tier
1」（最佳的）等級的開放原始碼產品，AMS認為Firebird擁有優秀的技術和幾乎沒有風險\[10\]。

### Firefox 1.0

[Mozilla_Firefox_1.0_front_page_screenshot.png](https://zh.wikipedia.org/wiki/File:Mozilla_Firefox_1.0_front_page_screenshot.png "fig:Mozilla_Firefox_1.0_front_page_screenshot.png")
Firefox
1.0發表於2004年11月9日\[11\]。当时Mozilla基金会藉由募捐的方式，在[纽约时报上面刊登了整版的广告](../Page/纽约时报.md "wikilink")。儘管Mozilla基金會一直希望能讓Mozilla
Suite走入歷史，用Firefox來取代它，但由於還有許多企業用戶使用，也被一些第三方軟體包裹在內，因此Mozilla基金會仍然持續維持更新，直至2006年4月12號\[12\]。在那之後，Mozilla社群改以[SeaMonkey為產品名稱](../Page/SeaMonkey.md "wikilink")，繼續發行新版的Mozilla
Suite\[13\]。

### Firefox 1.5

[Delicious_delicacies-Firefox0.9.3-Knoppix671.png](https://zh.wikipedia.org/wiki/File:Delicious_delicacies-Firefox0.9.3-Knoppix671.png "fig:Delicious_delicacies-Firefox0.9.3-Knoppix671.png")早期敘述。這個敘述很快的在之後的版本被刪除了。\]\]
Firefox
1.5發表於2005年11月30日。最初計劃在1.0之後推出1.1版本的新版本Firefox，更高版本（1.5）在獨立的分支中進行開發，但是在2005年兩個分支和功能都被合併（Mozilla基金會在前兩個Alpha版本之後放棄了1.1版本發布計劃）。

早期的Firefox使用者一定記得，Firefox
0.9的選項對話方塊中，在設定[Cookie的地方是這樣解釋Cookie的](../Page/Cookie.md "wikilink")：「Cookie是精緻的美味。」（"Cookies
are delicious
delicacies."）。這個解釋如傳奇一般流傳開來，它讓開發人員感覺到幽默和傳神，但是卻令普通使用者如墜雲霧。實際上，布雷克·羅斯當時只是一時找不到一句簡短的描述來解釋Cookie，便隨手寫下了這句話放在這裡占位置。他後來解釋道：「在這麼小的空間裡面解釋這麼複雜的東西，坦白的說，我想最後我可能要重寫整個Cookie管理器」。這個對普通使用者不友好的特性後來作為一個[Bug被提出到](../Page/Bug.md "wikilink")[Bugzilla資料庫](../Page/Bugzilla.md "wikilink")，並在2004年7月被「修正」\[14\]\[15\]。現在，這個文字被取代成了索然無味的：「Cookie是網站在您的電腦中儲存的訊息『片段』。一般用來記住登入訊息或其它資料。」。不過故事並沒有結束，「精緻美味」傳說的終結引起了一些人的不滿，於是Jesse
Ruderman專門開發了一個Delicious
Delicacies擴充套件用來將那個描述取消復原成原來的文字\[16\]。在中文版本中，一直到1.5版本，這個著名的句子才被取代掉。

### Firefox 2.0

[Wikipedia_Main_Page_in_Firefox_2.0.0.12.png](https://zh.wikipedia.org/wiki/File:Wikipedia_Main_Page_in_Firefox_2.0.0.12.png "fig:Wikipedia_Main_Page_in_Firefox_2.0.0.12.png")

Firefox 2.0發表於2006年10月24日。Firefox 2.0.0.20是該系列的最後一個版本\[17\]。Firefox
2.0發布後，微軟Internet Explorer團隊送上蛋糕，祝賀Firefox 2發佈\[18\]。

### Firefox 3.0

[Mozilla_Firefox_3_in_MS_XP.png](https://zh.wikipedia.org/wiki/File:Mozilla_Firefox_3_in_MS_XP.png "fig:Mozilla_Firefox_3_in_MS_XP.png")

Firefox 3.0發表於2008年6月17日\[19\]。該版本修正了非常多錯誤，也增加對網頁標準的相容性，還有許多新的Web
APIs提供開發者使用。其他新功能包括重新設計的下載管理員、新的“Place”系統來儲存書籤和瀏覽記錄以及在各種作業系統上有一致性的佈景主題。Firefox
3發布創下一天內超過800萬下載次數的[金氏世界紀錄](../Page/金氏世界紀錄.md "wikilink")\[20\]。當天，微軟Internet
Explorer團隊再次送上蛋糕，祝賀Firefox 3發佈\[21\]。

### Firefox 3.5

Firefox
3.5的開發代號為Shiretoko。初期的版本號為3.1，但為了反應出大幅度的改善以及增加的新功能，正式版本命名為3.5。其中最大的革新是加入了全新的JavaScript引擎[TraceMonkey](../Page/TraceMonkey.md "wikilink")，大幅提高頁面渲染的速度。並且支援了-{zh-hans:隐私;
zh-tw:私密}-瀏覽功能，Firefox圖示也重新繪製\[22\]\[23\]\[24\]。

### Firefox 3.6

Firefox 3.6發表於2010年1月21日，開發代號為Namoroka\[25\]。後來發佈了Firefox
3.6.4的一個小更新，修正一些安全和穩定性的問題，並且加入了外掛獨立運行（out-of-proccess-plugin）架構，將[Adobe
Flash
Player這類的外掛程式將隔離到另一個獨立的](../Page/Adobe_Flash_Player.md "wikilink")-{程序}-，增加瀏覽器的穩定性\[26\]。

### Firefox 4.0

[Firefox_4.png](https://zh.wikipedia.org/wiki/File:Firefox_4.png "fig:Firefox_4.png")

Mozilla技術長[布蘭登·艾克提出了](../Page/布蘭登·艾克.md "wikilink")「Mozilla
2」的計劃，指的是對目前Firefox和其他產品（自推出以來）的一次最全面的更替\[27\]。這個計劃大多數特性已從Firefox
3.0、3.5及3.6版本開始納入，不過當中最大的改變被延遲到Firefox 4.0。Firefox
4.0的新特性包括全新的使用者介面\[28\]\[29\]\[30\]、新的JavaScript引擎[JägerMonkey](../Page/JägerMonkey.md "wikilink")\[31\]、分頁群組、應用程式分頁、重新設計的附加元件管理員、整合[Firefox同步以及支援螢幕多點觸控等](../Page/Firefox同步.md "wikilink")\[32\]\[33\]\[34\]\[35\]。發布後當天，微軟Internet
Explorer團隊再次獻上蛋糕，祝賀Firefox 4.0發佈\[36\]。

## 快速釋出週期

2011年4月，開發過程分成幾個“頻道”，每個頻道在不同的[開發階段進行建構](../Page/軟件版本週期.md "wikilink")：

  - 每夜版（Nightly）：汇集每天源代码的改动，提供最新的尚未被Mozilla[品質保证團隊測試的新功能](../Page/品質保证.md "wikilink")，不保证稳定性。
  - 曙光版（Aurora）：亦每天均有更新，每六周合并一次来自Nightly的最新代码，它将修复在Nightly中出现的关键问题，能在基本稳定的前提下提供新功能，自35版起，被更名為*Developer
    Edition*，54版推出*Developer Edition*的最後一個版本。
  - 測試版（Beta）：会不定期快速更新，它经过廣泛的品質測試，预计可提供给一千万用户所使用\[37\]以进一步测试其稳定性。从Beta开始，Mozilla会提供包含数字签名的安装包及可执行程序，并使用正式的Firefox名称及标志。
  - 正式版（Release）：已經過完整品质保证測試，提供全球數億用户所使用的最終穩定發行版本，每六星期发布一版。

新版本計劃每隔六週發布一次，這項快速節奏過程的目標是讓使用者更快地獲得新功能\[38\]\[39\]。這項快速釋出週期遭到了使用者的批評，因為它經常破壞附加元件的相容性\[40\]，以及有人認為Mozilla只是想試圖增加其版本數量以與Google
Chrome等其他瀏覽器進行較量\[41\]。

### Firefox 5–9

#### Firefox 5.0

Firefox 5.0正式版发布于2011年6月21日，除了保留了Firefox
4的全新界面，还在前版本的基础上增加了“禁止网站追踪功能”（[请勿跟踪](../Page/请勿跟踪.md "wikilink")）
\[42\]。

發布後，微軟IE团队送上一个小杯子蛋糕\[43\]。

#### Firefox 6.0

Firefox 6.0正式版发布于2011年8月16日。

Firefox 6带来了以下新功能、特性：\[44\]
[FF6Addr.png](https://zh.wikipedia.org/wiki/File:FF6Addr.png "fig:FF6Addr.png")

  - 更新了地址栏，采用了新的网站通知、站点身份识别样式。\[45\]
  - 在开发者工具中提供“Scratchpad”，开发者可以通过它进行JavaScript的调试。
  - 增加了检查插件（plugin）版本的链接
  - 增加了网站权限管理器（permission manager）
  - WebSockets回归、API改进、新的“progress”元素等\[46\]

Firefox 6正式发布后，IE团队再次赠送蛋糕\[47\]。

#### Firefox 7.0

Firefox 7.0正式版发布于2011年9月27日。

该版本对内存控制进行了优化与改善，得益于MemShrink项目，Firefox 7.0相对于Firefox
4.0内存使用量减少了50%，\[48\]\[49\]\[50\]并支持了新的硬件加速技术，除此之外还加入了性能报告扩展Telemetry。

微軟IE團隊依慣例送上小蛋糕。\[51\]

#### Firefox 8.0

Firefox
8.0正式版发布于2011年11月8日。当恢复上次浏览的标签页时，用户可以指定只恢复一部分，以加快启动速度。用户拖拽排列标签页时，可以看到一定的动画效果。而和前版本相比，在该版本中用户可以一次性决定是否保留、禁用或移除扩展。此外Firefox还为网络开发者更新了编辑工具\[52\]。

2011年11月10日，微軟IE團隊聲明不再贈送蛋糕，微軟發言人表示：「我們不會再延續這個傳統，因為太具殺傷力了」「基本上每六星期我們就要提供好多個紙杯蛋糕。」\[53\]。

#### Firefox 9.0

Firefox 9.0正式版发布于2011年12月20日。Firefox
9加入了类型推断技术，从而在JavaScript的性能上有了30%的提升。对[Mac
OS
X有了更佳的支援](../Page/Mac_OS_X.md "wikilink")，使用者可通過雙指滑動手勢更加方便的瀏覽網頁。此外，新版本对网页开发者的工作也提供了更好之支持，在处理大量数据或是使用
AJAX 技术的网站时加载速度更快，不用等待所有下载完成就可以显示相关内容。\[54\]

### Firefox 10–16

#### Firefox 10.0

[Page_inspector_firefox_10.png](https://zh.wikipedia.org/wiki/File:Page_inspector_firefox_10.png "fig:Page_inspector_firefox_10.png")
Firefox 10.0正式版发布于2012年1月31日。 特性\[55\]\[56\]

  - 預設隱藏至下一頁按鈕，除非執行了回到上一頁操作
  - 支援[WebGL反鋸齒](../Page/WebGL.md "wikilink")
  - 全螢幕APIs支援創建可全螢幕的web應用
  - 支援[CSS3](../Page/CSS3.md "wikilink") 3D-Transforms
  - 為開發增加了高亮顯示內容的檢測工具，博阿凱CSS Style Inspector，新增了IndexedDB APIs
  - 修復了部分用戶移動標籤時遭遇的崩潰的問題
  - 首個“長期支持版本”（Extended Support
    Release，簡稱“ESR”），專門為那些無法或不願每隔六周升級一次的企業打造，升級周期為42周。
  - 為默認兼容擴展版本升級，簡化了Firefox升級流程。

#### Firefox 11.0

[Firefox_3D_tilt.png](https://zh.wikipedia.org/wiki/File:Firefox_3D_tilt.png "fig:Firefox_3D_tilt.png")
Firefox 11.0正式版发布于2012年3月13日。 特性\[57\]

  - 支援從Google Chrome載入瀏覽紀錄、我的最愛及Cookies
  - 開啟同步功能後，附加元件（Add-ons）可從不同電腦中進行同步
  - 支援CSS的text-size-adjust元素
  - 重新設計HTML5短片的媒體控制外觀
  - 支援HTML5的outerHTML 元素
  - 使用的HTML5分析器以查看網頁代碼的語法
  - 網頁開發者可使用CSS樣式編輯器
  - 網頁開發者可以使用Page Inspector 3D View去瀏覽3D網頁
  - 支援更快頁面加載的SPDY協議已可作測試
  - XMLHttpRequest現在支援HTML parsing
  - 檔案可以儲存在IndexedDB
  - 可使用無前綴的網頁代碼Websockets（毋須moz-前綴）
  - 修正MFSA 2012-12至MFSA 2012-19的保安漏洞。
  - 修正Firefox通知信息與Growl 1.3之後版本不相容的問題

#### Firefox 12.0

Firefox 12.0正式版发布于2012年4月24日。 特性\[58\]

  - 在[微軟](../Page/微軟.md "wikilink")[Windows](../Page/Windows.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")：Firefox更新時，現在可以減少一次使用者帳戶控制設定（[User
    Account Control](../Page/User_Account_Control.md "wikilink")）
  - 查看網頁源碼時加入行數
  - 標題（Title attribute）現在支援換行（Line breaks）
  - 改善「網頁搜尋」功能，搜尋結果現會置中
  - 在下載視窗時貼上網址將會自動進行下載
  - 支援CSS的column-fill元素（須要 -moz- 前綴）
  - 支援CSS的text-align-last元素
  - 實施ECMAScript 6 Map and Set的測試性支援
  - 在[Mac OS X作業系統](../Page/Mac_OS_X.md "wikilink")：修正在某些硬件上WebGL表現下降的問題

#### Firefox 13.0

Firefox 13.0正式版发布于2012年6月5日。 特性\[59\]

  - 開啟新分頁時，列出最常造訪的網頁
  - 預設開始頁提供了前往書籤、歷史、設定以及更多捷徑
  - 預設啟用 SPDY 通訊協定，可以更快速地瀏覽支援的網站
  - 為了加快啟動速度，預設不先載入背景分頁
  - 預設啟用平滑捲動功能
  - 網頁檢測器、HTML 面板、樣式檢測器、程式碼速記本和樣式編輯器的總共 72 項改進\[60\]
  - 實作了 column-fill\[61\] CSS 屬性
  - 實作了 ECMAScript 6 Map\[62\] 和 Set\[63\] 物件的實驗性支援
  - 新增對 CSS3 background-position\[64\] 擴充語法的支援
  - :invalid\[65\] 虛擬類別可以套用在
    <form>
    元素上
  - 支援 CSS turn <angle>\[66\] 單位

#### Firefox 14.0

Firefox安卓版 14.0发布于2012年6月26日。\[67\]
之后为了保持移动版和桌面版版本号一致，Mozilla于2012年7月17日同时发布了移动版和桌面版的Firefox
14.0.1。

特性\[68\]

  - 採用 [HTTPS](../Page/HTTPS.md "wikilink") 進行 Google 搜尋
  - Mac OS X Lion 的全螢幕支援\[69\]
  - 可以設定外掛程式，改為點選一下才載入（需調整一個 <about:config> 設定\[70\]）
  - 智慧網址列輸入時的 自動完成 URL\[71\] 功能
  - 改善網站身分識別設計\[72\]，防止配合 favicon 的 SSL 連線詐騙
  - 實作了 Pointer Lock API\[73\]
  - 新增可以防止顯示裝置進入休眠\[74\]的 API
  - 對突厥、希臘文字的 text-transform 和 font-variant CSS 改良\[75\]
  - 各項安全性修正\[76\]
  - 修正GIF 動畫於 src 和尺寸變更時可能會卡住的問題（bug 743598\[77\]）
  - 修正OS X：nsCocoaWindow::ConstrainPosition 在多重顯示裝置上誤用了不正確的螢幕的問題（bug
    752149\[78\]）
  - CSS：修正以 JavaScript 設定元素 class name 時，發生 :hover regression 的問題（bug
    758885\[79\]）

#### Firefox 15.0

Firefox 15.0正式版发布于2012年8月28日。 特性\[80\]

  - 安靜地背景更新

  - 支援 SPDY 網路通訊協定 v3

  - WebGL 改善，包括壓縮材質以增進效能等

  - 馬蒂科里（Maithili ）語系支援（見所有支援的語系\[81\]）

  - 最佳化附加元件相關的記憶體使用率

  - 將 JavaScript「除錯器」整合進開發者工具

  - 檢測工具新增了布局檢視

  - 實作了高精準度事件計時器

  - 實作了 CSS word-break\[82\] 屬性

  - 新增「適應性設計檢視模式」讓開發者快速切換 桌面／行動版 網站檢視

  - 新增原生 Opus 音訊編碼支援

  - \< source \>\[83\] 元素已支援 media 屬性

  -

    <audio>

    \[84\] 和

    <video>

    \[85\] 元素已支援 played 屬性

  - 修正重覆以 tab 在元素間切換時，顯示焦點的外框可能會逐漸變大的問題 （bug 720987\[86\]）

#### Firefox 16.0

Firefox 16.0正式版发布于2012年10月9日。 特性\[87\]

  - Mac OS X 版本的 Firefox 已初步支援 VoiceOver，且預設\[88\]即啟用
  - 開始支援 web app\[89\]
    ([Windows](../Page/Windows.md "wikilink")/[Mac](../Page/Mac.md "wikilink")/[Linux](../Page/Linux.md "wikilink"))。
  - 新增 Acholi 及 Kazakh 語系支援。
  - 藉由增量垃圾回收（incremental garbage collection\[90\]）增進
    [JavaScript](../Page/JavaScript.md "wikilink") 回應速度。
  - 新增「開發者工具列」提供快速存取「網頁主控台」錯誤計數等工具的按鈕，以及便於鍵盤存取的命令列介面
  - 使用 CSS3 Animations、Transitions、Transforms 和 Gradient 時需要多寫的 prefix
    已於 Firefox 16 移除\[91\]
  - 程式碼速記本「開啟最近檔案」功能
  - 重新載入頁面時，除錯器設置的中斷點無法被正常捕捉（bug 783393\[92\]）
  - 不再於數位簽章中支援 MD5 雜湊演算法（bug 650355\[93\]）
  - 預設即支援 Opus（772341\[94\]）
  - 實作了動畫方向反轉（655920\[95\]）
  - <about:memory> 可產生個別分頁的報告（687724\[96\]）
  - 未發行版本 Firefox 的使用者代理字串，改為只顯示主要版本號（728831\[97\]）

### Firefox 17–23

#### Firefox 17.0

Firefox 17.0正式版发布于2012年11月20日。 特性\[98\]

  - Click-to-play 阻擋機制，避免在使用者未授權的情況下啟用可能有危險的插件版本\[99\]
  - 增大圖示以改進智慧位址列使用經驗。
  - 不再支援\[100\] Mac OS X 10.5。
  - JavaScript Maps 和 Sets 現在是可列舉（iterable）的了。
  - 實作了 SVG FillPaint 和 StrokePaint。
  - 網頁主控台、除錯器、開發者工具列的各種改進，可以更快速、容易地使用。
  - 新的網頁檢測器「標記面板」讓 DOM 編輯更加容易。
  - 實作 iframe 的 sandbox \[101\] 屬性，提供更佳的安全性。
  - 超過二十個效能改進，包括開啟新分頁的相關修正。
  - 修正Pointer lock 在 web app 中無法使用的問題。（ bug 769150\[102\]）
  - 修正在有固定置頂元素網頁的頁面的捲動問題。（bug 780345\[103\]）
  - Mozilla已在2012年11月釋出的Firefox
    17版修補該漏洞。起因美國國安局（[NSA](../Page/NSA.md "wikilink")）及英國政府通訊總部（Government
    Communications Headquarter，GCHQ）於2012年6月發佈名為Tor
    Stinks的簡報檔，檔案中兩國政府單位提及監聽[Tor網路困難重重](../Page/Tor.md "wikilink")，文中表示「無法辨識出所有Tor用戶的身份。而以人工分析後，我們得以辨識非常小部份的Tor用戶。」然而對特定呼叫有回應的用戶，國安單位未成功辨識出任何身份。
    之後利用代號為「EgotisticalGiraffe」的技術（靠入侵[Tor](../Page/Tor.md "wikilink")
    Browser
    Bundle軟體漏洞，特別是Firefox），NSA在用戶可能造訪的網站或[論壇植入](../Page/論壇.md "wikilink")[惡意程式](../Page/惡意程式.md "wikilink")，然後趁用戶造訪時植入其電腦，並藉此蒐集資料，監聽[Tor網路](../Page/Tor.md "wikilink")（The
    Onion
    Router）。雖然NSA表示這是為了瞄準恐怖份子或有組織的罪犯，但《[衛報](../Page/衛報.md "wikilink")》表示，這類攻擊也可能傷害到新聞記者、研究人員，或是不小心造訪這些網站的人。
    \[104\]

#### Firefox 18.0

Firefox 18.0正式版发布于2013年1月8日。 特性\[105\]

  - 藉由 IonMonkey 引擎帶來更快的 JavaScript 效能。
  - 支援 OS X 10.7 以上的 Retina 顯示器。
  - 初步 WebRTC 支援。
  - 新的 HTML 縮放演算法帶來更佳的圖片品質。
  - 改善分頁切換效能。
  - 支援新的 window.devicePixelRatio DOM 屬性。
  - 藉由對已簽署附加元件憑證的智慧處理，達到更快啟動速度。
  - 實作 W3C touch 事件，並取代原有的 MozTouch 事件。
  - 禁止 HTTPS 頁面中不安全內容的載入（bug 62178\[106\]）。
  - 改善 proxy 環境中對使用者的回應速度（bug 769764\[107\]）。

#### Firefox 19.0

Firefox 19.0正式版发布于2013年2月19日。 特性\[108\]

  - 內建 PDF 檢視器。
  - Canvas 元素可使用 canvas.toBlob()\[109\] 將內容輸出為影像 blob 資料。
  - 改善啟動效能（bug 715402、756313）\[110\]。
  - 除錯器開始支援「在發生例外時暫停」和「顯示隱藏（non-enumerable）屬性」。
  - 可藉由遠端網頁主控台（Remote Web Console）連線到 Firefox OS 或 Android 上的 Firefox。
    （此為實驗性功能，需將 devtools.debugger.remote-enabled 設定改為 true）
  - 提供瀏覽器除錯器（Browser Debugger）予附加元件、瀏覽器開發者使用。 （此為實驗性功能，需將
    devtools.chrome.enabled 設定改為 true）
  - 網頁主控台的 CSS 連結現在會於樣式編輯器開啟。
  - CSS @page \[111\]支援。
  - CSS viewport-percentage 長度單位\[112\]實作（vh、vw、vmin 和 vmax）。
  - CSS text-transform 的 full-width\[113\] 支援。
  - 修正部分正確 WebGL 繪圖操作被錯誤地拒絕，造成頁面顯示不完整的問題（bug 825205\[114\]）。
  - 修正帶著 -private flag 啟動 Firefox 時，錯誤地表示使用者不在隱私瀏覽模式中的問題（bug
    802274\[115\]）。
  - 修正於 HiDPI 模式中，捲動頁面使插件上半部超出畫面時，插件會停止描繪的問題（bug 825734\[116\]）。

#### Firefox 20.0

Firefox 20.0正式版发布于2013年4月2日。 特性\[117\]

  - 安全性更新，詳見相關安全性公告\[118\]。

  - 個別視窗的隱私瀏覽功能，見詳細資訊\[119\]。

  - 新的下載體驗，見詳細資訊\[120\]。

  - 單獨關閉停止回應的外掛程式，而不是整個瀏覽器停止回應。

  - 持續改善一般瀏覽工作的效能（網頁載入\[121\]、下載\[122\]、關閉\[123\]等）。

  - 持續實作 ECMAScript 6 草案—— clear() \[124\]和 Math.imul\[125\]。

  - 新的 JavaScript Profiler 工具。

  - 實作 getUserMedia 讓網路存取使用者的攝影機和麥克風（需使用者授權）。

  -

    <canvas>

    的 blend mode\[126\] 支援。

  - 各種

    <audio>

    和

    <video>

    改善\[127\]。

  - 修正當機回報員的「細節」按鈕的問題（bug 793972\[128\]）。

  - 修正Unity 外掛程式在 HiDPI 模式中無法顯示的問題（bug 829284\[129\]）。

#### Firefox 21.0

Firefox 21.0正式版发布于2013年5月14日。 特性\[130\]

  - Social API 新增了多個提供者支援\[131\]。
  - 將「不要追蹤我」（Do Not Track, DNT）功能改進為具有三種狀態的使用介面\[132\]。
  - 有需要時，Firefox 會提供如何改善啟動速度的建議。
  - 初步實作了 Firefox 健康檢查報告\[133\]。
  - 可在「新分頁」頁面復原已移除的縮圖。
  - 變更 CSS -moz-user-select:none 的行為，以增進與 -webkit-user-select:none
    的相容性（bug 816298\[134\]）。
  - 與繪圖相關的效能改善（bug 809821\[135\]）。
  - 移除 SpiderMonkey 的 E4X\[136\] 支援。
  - 實作遠端效能分析\[137\]。
  - 將 Add-on SDK loader 與 API 元件庫整合進 Firefox。
  - 新增
    <main>
    \[138\]元素支援。
  - 實作 scoped 樣式表\[139\]。
  - 修正部分功能鍵可能沒有效用的問題（bug 833719\[140\]）。
  - 需統整瀏覽記錄和下載記錄的清除，以免清除下載記錄時產生混淆（bug 847627\[141\]）。
  - 安全性更新\[142\]。

#### Firefox 22.0

Firefox 22.0正式版发布于2013年6月25日。 特性\[143\]

  - [WebRTC](../Page/WebRTC.md "wikilink") 現在預設開啟\[144\]。
  - Windows: 在高解析度顯示裝置上，Firefox 會隨系統縮放設定顯示較大的文字。
  - Mac OS X: 下載進度會顯示於 Dock 應用程式圖示上。
  - 可以變更 [HTML5](../Page/HTML5.md "wikilink") audio/video 的播放速度。
  - 附加元件管理員中實作了社交服務的管理功能。
  - 啟用 asm.js
    最佳化功能（[OdinMonkey](../Page/SpiderMonkey#OdinMonkey.md "wikilink")\[145\]）而達到重大效能改善。\[146\]
  - 藉由非同步 canvas 更新\[147\]改善 WebGL 描繪效能。
  - 在 Firefox 中顯示的純文字檔案，現在會 word-wrap（折行）顯示。
  - 安全性考量，web 內容不再能夠存取 |Components| 物件。
  - 即使不在全螢幕中，仍可使用 Pointer Lock API。
  - 改善描繪圖片時的記憶體用量與顯示時間\[148\]。
  - 實作 [CSS3](../Page/CSS3.md "wikilink") Flexbox\[149\]，且預設就是啟用的
  - 實作了新的 Web Notifications API。
  - 新增 clipboardData API，讓 JavaScript 得以存取使用者剪貼簿。
  - 內建新的字型檢測器。
  - 支援新的 HTML5 \<data\> \[150\]和 \<time\> \[151\]元素。
  - 修正部分支援高解析度捲動的觸控板，捲動時會感到遲鈍的問題（bug 829952\[152\]）。
  - 安全性更新\[153\]。

#### Firefox 23.0

Firefox 23.0正式版发布于2013年8月6日。
[Mozilla_Firefox_logo_2013.svg](https://zh.wikipedia.org/wiki/File:Mozilla_Firefox_logo_2013.svg "fig:Mozilla_Firefox_logo_2013.svg")
特性\[154\]

  - 阻擋可能不安全的混合內容，保護使用者免於中間人攻擊或利用 HTTPS 的頁面監聽\[155\]。
  - 開發者工具箱新增選項面板。
  - 移除「啟用 JavaScript」選項，並將既存的設定重設為預設值。
  - 移除「自動載入圖片」和「總是顯示分頁工具列」選項，並將設定重設為預設值。
  - 更新 Firefox 圖示。
  - 改善 <about:memory> 使用者介面。
  - 簡化安裝外掛程式的通知介面。
  - 於 Windows Vista 以上啟用 DXVA2 以加速 H.264 影片解碼。
  - 切換到新的搜尋引擎時，整個瀏覽器都會使用該搜尋引擎。
  - 強制使用符合標準文法及語義的 CSP (Content Security Policy) 政策。
  - 強化 \<input type='file'\> 顯示（bug 838675\[156\]）。
  - 實作 HTML5 \<input type="range"\> 表單控制。
  - 可在按鈕上使用新的 ARIA role，撰寫對觸控介面更具親和力的網頁。
  - 新增了社交分享功能。
  - 增加不需前綴縮寫的 requestAnimationFrame。
  - 實作了全域的瀏覽器主控台。
  - 取消閃爍效果，含 text-decoration: blink，並完全移除 \<blink\> 元素。
  - 工具箱新功能：網路監測器\[157\]。
  - 安全性更新\[158\]。

### Firefox 24–30

#### Firefox 24.0

Firefox 24.0正式版发布于2013年9月17日。 特性\[159\]

  - 支援 Mac OS X 10.7 以後的新捲軸外觀。
  - 實作關閉右方分頁\[160\]功能。
  - 社交功能：可以將聊天功能拖曳出來，以單獨視窗顯示。
  - 有關釘選分頁的親和力改進（bug 577727\[161\]）。
  - 移除對 Revocation Lists 功能的支援（bug 867465\[162\]）。
  - 改善新分頁頁面的載入效能（bug 791670\[163\]）。
  - SVG 影像 tiling 和 scaling 描繪的重大改進（bug 600207\[164\]）。
  - 以進化、統整過的瀏覽器主控台\[165\]取代舊有錯誤主控台，提供更佳除錯體驗。
  - 不再支援從應用程式目錄或設定檔目錄載入 sherlock 檔案。
  - 改用 Speex 重新取樣器取代 webrtc.org 的固定比率聲音取樣器，並淘汰虛擬 44000Hz 頻率（bug
    886886\[166\]）。
  - 安全性更性\[167\]。

#### Firefox 25.0

Firefox 25.0正式版发布于2013年10月29日。 特性\[168\]

  - 支援 Web Audio。
  - 各分頁間不再共用同一個尋找工具列。
  - 如果數個月未使用 Firefox，可選擇匯入其它瀏覽器的歷史和設定。
  - 重設 Firefox 現在不會清除瀏覽狀態了。
  - 支援 CSS 3 控制背景捲動的 background-attachment: local\[169\]。
  - 實作多項 ES 6 的新功能\[170\]。
  - 可以用 srcdoc \[171\]屬性直接指定 iframe 的 document 內容。
  - 修正開新分頁時，預覽縮圖是空白的或有遺漏的問題。
  - 安全性更新\[172\]。

#### Firefox 26.0

Firefox 26.0正式版发布于2013年12月10日。 特性\[173\]

  - 所有 Java 外掛程式都預設為需點擊才會啟用\[174\]。
  - 密碼管理員能支援指令碼產生的密碼欄位了。
  - 即使 Windows 使用者對 Firefox 安裝目錄沒有寫入權限，也能順利更新程式（使用 Mozilla 維護服務）。
  - 安裝可用的 gstreamer 外掛程式後，就能在 Linux 上支援 H.264 編碼。
  - 在 Windows XP 也支援 MP3 解碼，完成了對 Windows 作業系統的跨版本 MP3 支援。
  - CSP 實作現在可支援複數政策，例如標準 enforced 和 Report-Only 政策共存的情形。
  - Social API 可藉由 SocialMarks 支援各社交提供者的書籤功能\[175\]。
  - 可行的話，Math.ToFloat32 會採取 JS 的數值並轉換為 Float32。
  - 網站使用 appcache 時不再出現確認訊息。
  - 支援 CSS image orientation 屬性。
  - 新增應用程式管理員，可用於 Firefox OS 手機和模擬器上的 HTML5 webapp 部署與除錯。
  - IndexedDB 使用時不再出現確認訊息。另資料現在會儲存於採取 LRU 政策的暫存 pool 之中。
  - 單獨顯示圖片時，Firefox 能正確使用 JPEG 所含 EXIF 中的方向資訊（bug 298619\[176\]）。
  - Windows 7 更新 KB2670838 後（MSIE 10 需要此更新）或 Windows 8.1 上的文字顯示問題（bug
    812695\[177\]）。
  - 不再對看不見的圖片進行解碼，縮短頁面載入時間（bug 847223\[178\]）。
  - Mac OS 的 AudioToolbox MP3 後端（bug 914479\[179\]）。
  - 安全性更新\[180\]。

#### Firefox 27.0

Firefox 27.0正式版发布于2014年2月4日。 特性\[181\]

  - 可同時執行多個 Social API 服務，從不同社交來源接收通知、進行交談以及更多功能。
  - 預設即啟用 TLS 1.1（RFC 4346\[182\]）和 TLS 1.2（RFC 5246\[183\]）支援。
  - 支援 SPDY 3.1\[184\] 通訊協定。
  - 可使用 'all:unset'\[185\] 重設樣式。
  - 可在除錯器中，將混淆器處理後的 JavaScript 轉為易讀格式顯示（bug 762761\[186\]）。
  - 支援以捲軸顯示 fieldset 內容（bug 261037\[187\]）。
  - 實作 iframe sandbox 屬性的 allow-popups 用法，允許更高的安全性（bug 766282\[188\]）。
  - 移除 CSS cursor 屬性 -moz-grab 和 -moz-grabbing 的製造商前綴（bug
    880672\[189\]）。
  - SpiderMonkey 可支援 ES6 的 generator 機制\[190\]。
  - 支援 ES6 的 Math.hypot() 數學函數（bug 896264\[191\]）。
  - 支援在 Canvas 中描繪虛線（bug 768067\[192\]）。
  - 支援 Linux Azure/Skia 的內容描繪（bug 740200\[193\]）。
  - 安全性更新\[194\]。

#### Firefox 28.0

Firefox 28.0正式版发布于2014年3月18日。 特性\[195\]

  - 支援 [VP9](../Page/VP9.md "wikilink") 影像解碼。
  - Mac OS X: 在通知中心顯示 Web Notification。
  - HTML5 audio/video 音量控制。
  - WebM 的 [Opus](../Page/Opus.md "wikilink") 支援。
  - 支援 [spdy](../Page/SPDY.md "wikilink")/3，並移除 spdy/2 支援。不使用 spdy/3
    的伺服器仍將順暢地以 http/1 進行通訊。
  - 支援 [MathML](../Page/MathML.md "wikilink") 2.0 'mathvariant' 屬性。
  - 背景[-{A停滯報告](../Page/线程.md "wikilink")。
  - 支援多行 flexbox 配置。
  - 支援使用[-{A](../Page/手柄.md "wikilink")（Gamepad）來操控網頁遊戲：在瀏覽列輸入「about:config」進入控制台，然後在「dom.gamepad.enabled」欄位修改設定為
    true，即可順利啟動。\[196\]
  - 安全性修正。\[197\]。

#### Firefox 29.0

Firefox 29.0正式版发布于2014年4月29日。
[Firefox_29.0_on_Windows_8.png](https://zh.wikipedia.org/wiki/File:Firefox_29.0_on_Windows_8.png "fig:Firefox_29.0_on_Windows_8.png")\]\]
特性\[198\]

  - 新的Australis介面，並在首次使用時，以互動式導覽幫助使用者認識介面變更\[199\] 。
  - 能夠以註冊 Firefox 帳號的方式設定 Firefox Sync\[200\] 。
  - 預設啟用已完成的 Gamepad API\[201\] 。
  - 新增馬來語（ma）語系支援。
  - 點選 W3C Web Notification 時，將切換至原來產生通知的分頁。
  - 實作 'box-sizing'（移除了 -moz 前綴）屬性\[202\] 。
  - 在 Web Worker 中也能取用 Console 物件\[203\] 。
  - 預設啟用 Promise\[204\] 。
  - 預設啟用 SharedWorker。
  - 實作並啟用 <input type="number">。
  - 實作並啟用 <input type="color">。
  - 啟用 [ECMAScript](../Page/ECMAScript.md "wikilink")
    Internationalization API。
  - 移除附加元件列，將其內容移動到瀏覽工具列。

#### Firefox 30.0

Firefox 30.0正式版发布于2014年6月10日。 特性\[205\]

  - 新增「側邊欄」按鈕，存取社交、書籤、歷史等側邊欄更加方便。
  - 在 Mac OS X，按下 command-E 將目前選取的文字複製到尋找列。
  - 支援 [GStreamer](../Page/GStreamer.md "wikilink") 1.0。
  - 不允許 web 網頁將 WebIDL 定義的建構子當作函數呼叫。
  - 除了擴充套件內建以及白名單\[206\]項目外，外掛程式將不再預設為啟用\[207\]。
  - 修正 outline 被描繪到 box-shadow 和其它視覺 overflow 效果之外的問題 [`(Bug``
     ``480888)`](https://bugzilla.mozilla.org/show_bug.cgi?id=480888)。
  - 使用 WebAudio 時，可針對個別視窗調整音量。
  - 預設啟用 background-blend-mode 屬性。
  - 允許在 <input type="reset|button|submit"> 使用 line-height 屬性。
  - 實作 ES6 Array 和 Generator 的 comprehension 語法\[208\]。
  - 錯誤堆疊的顯示會包含列號。
  - Canvas 的 context options 可支援 alpha 選項\[209\]。
  - 修正密碼管理員在 autocomplete="off" 時，仍可提供儲存密碼的問題 [`(Bug``
     ``956906)`](https://bugzilla.mozilla.org/show_bug.cgi?id=956906)。
  - TypedArray 無法新增有名字的屬性 [`(Bug``
     ``695438)`](https://bugzilla.mozilla.org/show_bug.cgi?id=695438)。
  - 安全性修正\[210\]。

### Firefox 31–37

#### Firefox 31.0

Firefox 31.0正式版发布于2014年7月22日。 特性\[211\]

  - 「新分頁」頁面新增搜尋欄。
  - 根據「家長監護」設定送出「Prefer:Safe」http 標頭\[212\]。
  - 改用 mozilla:pkix 驗證安全憑證\[213\]。
  - 阻擋下載檔案中的惡意軟體\[214\]。
  - 實作部分的 OpenType MATH table (section 6.3.6)\[215\]\[216\]\[217\]。
  - 若未指定影音 .ogg 和 .pdf 檔案的預設應用程式，Firefox 就會自動開啟它們（限 Windows）。
  - 新增上索布語（Upper Sorbian, hsb）語系支援。
  - 移除用來指定網站權限（透過 capability.policy.\* 設定值）的 CAPS
    基礎程式。需注意將無法再藉此功能允許剪貼簿的存取，唯一的例外是
    checkloaduri 權限，仍可用來讓網站載入 <file://> URI。
  - 實作並啟用 WebVTT\[218\]。
  - 實作 CSS3 變數\[219\]。
  - 開發者工具：附加元件除錯器\[220\]。
  - 開發者工具：Canvas 除錯器。
  - 新的 Array 內建方法：Array.prototype.fill()\[221\]。
  - 新的 Object 內建方法：Object.setPrototypeOf()\[222\]。
  - 預設啟用 CSP 1.1 nonce-source 和 hash-source。
  - 開發者工具：滴管工具\[223\]。
  - 開發者工具：Box Model 即時編輯功能\[224\]。
  - 開發者工具：原始碼編輯器的改進\[225\]。
  - 開發者工具：主控台錯誤記錄可顯示堆疊追溯\[226\]。
  - 開發者工具：以 cURL 指令複製\[227\]。
  - 開發者工具：主控台記錄訊息支援格式化輸出\[228\]。
  - 預設啟用 navigator.sendBeacon\[229\]。
  - onbeforeunload 產生的對話框不會阻擋對瀏覽器的存取。
  - 修復選取連結文字的一部分時，右鍵選單中的搜尋功能無法正確使用的問題（bug 985824\[230\]）。

#### Firefox 32.0

Firefox 32.0正式版发布于2014年9月2日。 特性\[231\]

  - 採用新版 HTTP 快取\[232\]，提供各種效能改善並加強當機復原能力。
  - 整合根據世代計算的垃圾回收機制。
  - 支援 Public Key Pinning\[233\]。
  - 尋找工具列會顯示符合的筆數。
  - 從右鍵選單\[234\]就能使用上一頁、下一頁、重新載入、加入書籤功能。
  - 密碼管理員會顯示登入次數等使用資訊。
  - 新增下索布語（Lower Sorbian, dsb）語系支援。
  - 移除部分 1024-bit 根憑證\[235\]。
  - 「密碼管理員」和「附加元件管理員」效能改善。
  - 預設啟用 drawFocusIfNeeded。
  - 預設啟用 CSS position: sticky\[236\]。
  - 預設啟用 mix-blend-mode。
  - 更新 Vibration API 至最新的 W3C 標準。
  - 預設啟用 box-decoration-break\[237\]。
  - 實作 ECMAScript 6 內建方法 Array\#copyWithin\[238\]。
  - 新的 Array 內建方法：Array.from()\[239\]。
  - 實作 navigator.languages\[240\] 屬性及 languagechange\[241\] 事件。
  - 以 CSS box-decoration-break\[242\] 屬性取代
    -moz-background-inline-policy。
  - 支援 HiDPI 的開發者工具 UI。
  - 將「檢測器」按鈕移至左上角。
  - 隱藏的節點在標記檢視中會以不同方式呈現。
  - 新增 Web Audio 編輯器\[243\]。
  - 程式碼速記本自動完成\[244\]與說明提示\[245\]功能。
  - 修復在Mac OS X上關閉所有視窗後，cmd-L 不會再開啟新視窗的問題\[246\]。
  - 修復在Windows 7 更新 KB2670838 後（MSIE 10 需要此更新）或 Windows 8.1 上的文字顯示問題。
  - 各種安全性修正\[247\]。

#### Firefox 33.0

Firefox 33.0正式版发布于2014年10月14日。 特性\[248\]

  - 支援
    [OpenH264](../Page/OpenH264.md "wikilink")\[249\]（[沙箱模式](../Page/沙盒_\(電腦安全\).md "wikilink")）。
  - 改善網址列的搜尋使用體驗\[250\]。
  - 更加輕巧快速的 JavaScript 字串\[251\]。
  - Firefox 開始頁（about:home）和新分頁（about:newtab）開始支援搜尋建議功能。
  - Windows版本預設啟用 OMTC\[252\]。
  - 新的 CSP（）後端。
  - 支援以 HTTPS 連線至 HTTP proxy。
  - 改善還原瀏覽分頁\[253\]的可靠性。
  - 新增[亞塞拜然](../Page/亞塞拜然.md "wikilink")（Azerbaijani, az）語系支援。
  - 移除 window.crypto 屬性與函數。
  - 移除 JSD（JavaScript Debugger Service）\[254\]，以 Debugger 介面取代。
  - 實作 [CSS3 Counter Styles 標準的](../Page/瀏覽器引擎CSS支援比較.md "wikilink")
    `@counter-style` 規則\[255\]。
  - 實作 DOMMatrix\[256\] 介面。
  - [Cubic-bezier 曲線](../Page/貝茲曲線.md "wikilink")\[257\]編輯器。
  - 顯示元素上已綁定的事件監聽器\[258\]。
  - 樣式編輯器新增`@media` 側邊欄\[259\]，顯示前往 `@media` 規則的捷徑。
  - 強調繪圖區\[260\]功能，顯示瀏覽器重新繪圖的情形。
  - 可以即時編輯檢測器中的 `@keyframes` 規則\[261\]。
  - 改善樣式檢測器對 CSS transform\[262\] 的上色標示。
  - 修正 HTTP 1.1 傳輸損壞，造成不完整的下載被視為已完成的問題\[263\]。
  - 各種安全性修正\[264\]。

#### Firefox 34.0

Firefox 34.0正式版发布于2014年12月1日。 特性\[265\]

  - 將白俄羅斯、哈薩克和俄羅斯語系的預設搜尋引擎改為[Yandex](../Page/Yandex.md "wikilink")。
  - 改善搜尋列功能（限 en-US 語系）。
  - Firefox Hello\[266\]\[267\]\[268\]即時通訊客戶端。
  - 在自訂模式中輕鬆切換佈景主題／[Persona](../Page/附加组件_\(Mozilla\)#主题.md "wikilink")\[269\]。
  - [英文維基百科搜尋改用](../Page/英文維基百科.md "wikilink")[HTTPS進行安全搜尋](../Page/HTTPS.md "wikilink")（限
    en-US 語系）。
  - [HTTP/2](../Page/HTTP/2.md "wikilink")（draft 14）和
    [ALPN](../Page/ALPN.md "wikilink") 實作。
  - 在[Windows版](../Page/Windows.md "wikilink")「Firefox
    已在執行中」對話框內，可選擇關閉鎖定的 Firefox
    程序並重新啟動。
  - 停用[SSL](../Page/SSL.md "wikilink") v3。
  - 重新啟用 window.crypto 屬性與函數（將於 Firefox 35 移除）。
  - 取得 Apple OS X version 2 簽署。
  - 實作 [ECMAScript](../Page/ECMAScript.md "wikilink") 6 WeakSet\[270\]。
  - 實作 JavaScript Template Strings\[271\]。
  - CSS3 Font variant 和字型特徵控制（例如 font-kerning，字距）的實作。
  - WebCrypto：支援 RSA-OAEP、PBKDF2 和 AES-KW、實作 wrapKey 和 unwrapKey、匯入／匯出
    JWK 格式的金鑰、支援 ECDH。
  - 實作 `matches()`\[272\] DOM API。
  - 實作wroker用的`Performance.now()`。
  - WebIDE\[273\]：在瀏覽器中建立、編輯、測試新的 web 應用程式。
  - 在「樣式編輯器」和檢測器的「規則」面板中，強調顯示符合指定選擇器的節點。
  - 改進「效能分析器」的使用者介面。
  - Web console新增console.table\[274\]函數。
  - CSS transition 在同時改變 display、position、overflow 與相似屬性時也能正確開始。
  - 各種安全性修正\[275\]。

#### Firefox 35.0

Firefox 35.0正式版发布于2015年1月13日。
[Firefox-hello-logo.svg](https://zh.wikipedia.org/wiki/File:Firefox-hello-logo.svg "fig:Firefox-hello-logo.svg")
特性\[276\]

  - 聊天室型式的Firefox Hello交談。
  - 在更多語系中啟用新的搜尋介面。
  - 從工具選單「應用程式」或工具列按鈕存取Firefox Marketplace\[277\]。
  - 在[Mac OS X Snow
    Leopard](../Page/Mac_OS_X_Snow_Leopard.md "wikilink")（10.6）以上，藉由原生API提供內建的H264（MP4）支援。
  - 在[OS X使用tiled](../Page/OS_X.md "wikilink") rendering。
  - 改善高品質圖片縮放的效能。
  - 改善對動態樣式變更的處理，以增進反應速度。
  - 實作HTTP Public Key Pinning Extension\[278\]，為加密連線提供更好的認證。
  - 支援CSS Font Loading API。
  - 實作Resource Timing API。
  - 預設啟用CSS filter\[279\]屬性。
  - 調整[JavaScript](../Page/JavaScript.md "wikilink") 'let'語義\[280\]以符合
    ES6 標準。
  - 支援檢測`::before`和`::after`虛擬元素。
  - 「計算樣式」面板：標示符合目前滑鼠所在選擇器的節點。
  - 網路監測器：新增請求／回應檔頭檢視\[281\]。
  - 支援`EXT_blend_minmax` WebGL擴充功能。
  - 修復檢測器右鍵選單中的「顯示 DOM 屬性」項目。
  - 減少處理已縮放圖片所使用的資源。
  - 更新PDF.js至1.0.907版本。
  - 正確回應非HTTP(S)的XHR狀態碼。
  - 各種安全性修正\[282\]。

#### Firefox 36.0

Firefox 36.0正式版发布于2015年2月24日。 特性\[283\]

  - 完整支援[HTTP/2](../Page/HTTP/2.md "wikilink")
  - 各種安全性修正\[284\]。

#### Firefox 37.0

Firefox 37.0正式版发布于2015年3月31日。 特性\[285\]

  - 新的使用者回饋系統。
  - 各種安全性修正\[286\]。

### Firefox 38–44

#### Firefox 38.0

Firefox 38.0正式版发布于2015年5月12日。 特性\[287\]

  - 將選項視窗變更為選項分頁。
  - 各種安全性修正\[288\]。

#### Firefox 39.0

Firefox 39.0正式版发布于2015年7月2日。 特性\[289\]

  - 在對話框中直接將Firefox Hello連結分享至社群網路的連結。
  - 在[Mac OS
    X及](../Page/Mac_OS_X.md "wikilink")[Linux的版本中啟用惡意軟體的偵測功能](../Page/Linux.md "wikilink")。
  - 支援Unicode 8.0的emoji。
  - 各種安全性修正\[290\]。

#### Firefox 40.0

Firefox 40.0正式版发布于2015年8月11日。 特性\[291\]

  - 支持Windows10。
  - 对于潜在附加软件(PUP)的下载提供保护。

#### Firefox 41.0

Firefox 41.0正式版发布于2015年9月22日。 特性\[292\]

  - 可以为你的火狐账号设置头像了。

#### Firefox 42.0

Firefox 42.0正式版发布于2015年11月3日。 特性\[293\]

  - 隐私保护中可以设置浏览跟踪保护。

#### Firefox 43.0

Firefox 43.0正式版发布于2015年12月15日。 特性\[294\]

  - 提供Windows 64位的Firefox。

#### Firefox 44.0

Firefox 44.0正式版发布于2016年1月26日。 特性\[295\]

  - 可以在不支持MP4/H.264的系统中开启H.264和WebM/VP9。

### Firefox 45–51

#### Firefox 45.0

Firefox 45.0正式版发布于2016年3月8日。 特性\[296\]

  - 通过Hello共享即时浏览。

#### Firefox 46.0

Firefox 46.0正式版发布于2016年4月26日。 特性\[297\]

  - 改进的JavaScript JIT编译技术。
  - GTK3集成。
  - 移除对Android 3.0的支持。

#### Firefox 47.0

Firefox 47.0正式版发布于2016年6月7日。 特性\[298\]

  - 支持Google Widevine CDM，例如Amazon Video可以切换为Silverlight来加密HTML5视频。

#### Firefox 48.0

Firefox 48.0正式版发布于2016年8月2日。 特性\[299\]

  - 增强的下载保护。
  - 移除Windows Remote Access Service modem Autodial。

#### Firefox 49.0

Firefox 49.0正式版发布于2016年9月20日。 特性\[300\]

  - 更新登录管理器。
  - 改善视频性能。
  - HTML5音频视频增加右键菜单可以以1.25倍速播放。
  - 移除Firefox Hello。

#### Firefox 50.0

Firefox 50.0正式版发布于2016年11月15日。 特性\[301\]

  - 98%的Windows7或更高系统的用户都可以使用[WebGL了](../Page/WebGL.md "wikilink")。

#### Firefox 51.0

Firefox 51.0正式版发布于2017年1月24日。 特性\[302\]

  - 支持[FLAC](../Page/FLAC.md "wikilink")。
  - 支持WebGL 2。

### Firefox 52–57

#### Firefox 52.0

Firefox 52.0正式版發布於2017年3月7日。 特性\[303\]

  - 支持[WebAssembly](../Page/WebAssembly.md "wikilink")。

#### Firefox 53.0

Firefox 53.0正式版發布於2017年4月19日。 特性\[304\]

  - 改善图形稳定性。

#### Firefox 54.0

Firefox 54.0正式版發布於2017年6月13日。 特性\[305\]

  - 下载按钮和面板的精简。

#### Firefox 55.0

Firefox 55.0正式版發布於2017年8月8日。 特性\[306\]

  - 支持[WebVR](../Page/WebVR.md "wikilink")。

#### Firefox 56.0

Firefox 56.0正式版發布於2017年9月28日。 特性\[307\]

  - 设置面板有了新布局。

[Firefox_Logo,_2017.svg](https://zh.wikipedia.org/wiki/File:Firefox_Logo,_2017.svg "fig:Firefox_Logo,_2017.svg")

#### Firefox 57.0

Firefox 57.0正式版發布於2017年11月14日。新版本命名为Firefox Quantum。\[308\] 特性\[309\]

  - 速度提升1倍，内存使用减少30%。

#### Firefox 58.0

Firefox 58.0正式版發布於2018年1月23日。特性\[310\]

  - 加入串流編譯以及分層編譯技術，讓網頁程式執行速度更快。

### Firefox 59–65

#### Firefox 59.0

Firefox 58.0正式版發布於2018年3月13日。特性\[311\]

  - 這是一個相對變動較小的版本，除了效能上的改善外，它允許使用者封鎖來自所造訪網站的擾人通知，也移除隱私瀏覽模式中的路徑資訊。

#### Firefox 60.0

Firefox 60.0正式版發布於2018年5月9日。特性\[312\]

  - 它還是全球第一個支援Web Authentication（WebAuthn）的瀏覽器。
  - 也是首個支援大型企業的Firefox Quantum版本。

#### Firefox 61.0

Firefox 61.0正式版發布於2018年6月26日。特性\[313\]

  - 預設啟用最新 TLS 1.3 標準。

#### Firefox 62.0

Firefox 62.0正式版發布於2018年6月26日。特性\[314\]

  - Firefox 首頁可選擇呈現最多 4 列的熱門網站、Pocket 頁面或精選網站。

#### Firefox 63.0

Firefox 63.0正式版發布於2018年10月23日。特性\[315\]

  - 啟用了增強版追蹤保護（Enhanced Tracking
    Protection）機制，可封鎖第三方的追蹤，以改善用戶的隱私及改善網頁載入速度。

#### Firefox 64.0

Firefox 64.0正式版發布於2018年12月11日。特性\[316\]

  - 使用多標籤頁選取功能，一次移動多個分頁標籤，方便使用者管理開啟許多頁面的視窗
  - 終止支援賽門鐵克憑證。

#### Firefox 65.0

Firefox 65.0正式版發布於2019年1月29日。特性\[317\]

  - 簡化內容封鎖（Content Blocking）設定
  - 改善對多國語言的支援
  - 支援開放的串流格式AV1
  - 相容於macOS的Handoff功能

### Firefox 66–72

#### Firefox 66.0

Firefox 66.0正式版發布於2019年3月19日。特性\[318\]

  - 新增自動播放聲音的網頁影片消音
  - 減少廣告下載導致網頁內容跳動
  - 允許跨頁籤搜尋與隱私搜尋
  - 支援Windows Hello

## 延長支持版本

Firefox延長支持版本（Extended Support
Release，简称“ESR”）是向不能或不愿每6周升级一次的用户提供的版本，其每42周更新一次。Firefox
10是继Firefox 3.6后第一个发布的延長支持版本。而Firefox 17則是Firefox 3.6後的第二個延長支持版本。

| 浏览器名   | 版本号  | 发布状态 | 发布日期        |
| ------ | ---- | ---- | ----------- |
| ESR 10 | 10.0 | 发布   | 2012年1月31日  |
| ESR 17 | 17.0 | 发布   | 2012年11月20日 |
| ESR 24 | 24.0 | 发布   | 2013年9月17日  |
| ESR 31 | 31.0 | 发布   | 2014年7月22日  |
| ESR 38 | 38.0 | 发布   | 2015年5月12日  |
| ESR 45 | 45.0 | 发布   | 2016年3月8日   |
| ESR 52 | 52.0 | 发布   | 2017年3月7日   |
| ESR 60 | 60.0 | 发布   | 2018年5月9日   |
| ESR 68 | 68.0 | 未发布  | 2019年7月9日   |
|        |      |      |             |

ESR版本列表

## 参考文献

## 外部連結

  - [Firefox Releases](https://www.mozilla.org/en-US/firefox/releases/)
    官方更新日誌
  - [Squarefree.com](http://www.squarefree.com/burningedge/releases/)
    非官方更新日誌
  - [RapidRelease -
    MozillaWiki](../Page/:mozillawiki:RapidRelease/Calendar.md "wikilink")
    快速釋出週期日程表
  - [MozillaZine
    Weblogs](https://web.archive.org/web/20060208030104/http://weblogs.mozillazine.org/ben/archives/009698.html)
    Firefox從哪裡來？
  - [BBC News](http://news.bbc.co.uk/2/hi/technology/6078016.stm)
    Firefox瀏覽器的Web 2.0時代

## 相關條目

  - [Mozilla Firefox版本歷史](../Page/Mozilla_Firefox版本歷史.md "wikilink")

[Category:Mozilla Firefox](../Category/Mozilla_Firefox.md "wikilink")
[Category:互联网的歷史](../Category/互联网的歷史.md "wikilink")
[Category:軟體版本列表](../Category/軟體版本列表.md "wikilink")

1.

2.

3.
4.
5.

6.
7.
8.

9.

10.

11. <http://website-archive.mozilla.org/www.mozilla.org/firefox_releasenotes/en-US/firefox/releases/1.0.html>

12. <https://developer.mozilla.org/devnews/index.php/2006/04/12/sunset-announcement-for-fxtb-10x-and-mozilla-suite-17x/>

13. <http://mozlinks.moztw.org/2013/06/mozillafirefox.html>

14.

15.

16.

17. www.mozilla.org/en-US/firefox/2.0.0.20/releasenotes/

18. [The Internet Explorer team sent us a cake at Mozilla HQ for our
    release of
    Firefox 2.0\!](http://www.flickr.com/photos/thunder/278483422/)

19. <https://developer.mozilla.org/devnews/index.php/2008/06/17/firefox-3-available-today-at-1700-utc-10am-pdt/>

20. [Mozilla要讓Firefox 3首日下載留名金氏世界紀錄](http://www.ithome.com.tw/itadm/article.php?c=49376)


21. [al with cake](http://www.flickr.com/photos/robceemoz/2587912633/)

22. [Firefox 3.5
    最後的更新：全新的-{圖示}-](http://mozlinks-zh.blogspot.com/2009/05/firefox-35.html)

23. [Firefox Icon History and creative brieffor shiretoko
    refresh](http://people.mozilla.com/~faaborg/files/20090515-creativeBrief/creativeBrief-i1-wm.png_large.png)


24. [Firefox
    Logos](http://www.mozilla.org/en-US/firefox/brand/identity/)Firefox
    brand toolkit

25. [Firefox/Namoroka](../Page/:mozillawiki:Firefox/Namoroka.md "wikilink")

26. news.cnet.com/8301-30685_3-10433844-264.html

27.

28. <https://wiki.mozilla.org/Firefox/4.0_Windows_Theme_Mockups>

29. <https://wiki.mozilla.org/Firefox/4.0_Mac_Theme_Mockups>

30. <https://wiki.mozilla.org/Firefox/4.0_Linux_Theme_Mockups>

31. hacks.mozilla.org/2010/03/a-quick-note-on-javascript-engine-components/

32. <https://support.mozilla.org/en-US/kb/tab-groups-organize-tabs?redirectlocale=en-US&redirectslug=what-are-tab-groups>

33. <http://www.azarask.in/blog/post/designing-tab-candy/>

34. <http://www.cnet.com/news/firefox-4-release-plan-the-need-for-speed/>

35. <http://www.cnet.com/news/mozilla-prepares-coders-for-firefox-4-features/>

36. [Another version of Firefox, another
    cake](http://www.openbuddha.com/2011/03/22/another-version-of-firefox-another-cake/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+InPursuitOfMysteries%2FMozilla+%28OpenBuddha+-+Mozilla%29)

37.

38.

39.

40.

41.

42.

43. [cupcake for Firefox 5: Microsoft has some fun with Mozilla's new
    rapid
    release](http://www.geekwire.com/2011/cupcake-firefox-5-microsoft-fun-mozillas-rapid-release)

44. [1](https://archive.is/20120714150809/reviewsmyfavourite.blogspot.com/2011/05/what-is-new-in-firefox-6.html),
    What is new in Firefox 6

45. [2](https://bugzilla.mozilla.org/show_bug.cgi?id=634065), Bug 634065
    - Implement design for identity block and persistent indicators

46. [3](http://hacks.mozilla.org/2011/05/aurora-6-is-here/),
    hacks.mozilla.org -- Aurora 6 is here

47. <http://www.cnet.com/news/ie-team-pokes-fun-at-rapid-release-firefox-6/>

48.

49.

50.

51.

52. [Firefox 8 for
    developers](https://developer.mozilla.org/en/Firefox_8_for_developers)


53. [Microsoft breaks with tradition, no cake for
    Firefox 8](http://www.winrumors.com/microsoft-breaks-with-tradition-no-cake-for-firefox-8/)
     | WinRumors

54. [Firefox 9 for
    developers](https://developer.mozilla.org/en/Firefox_9_for_developers)


55. [Firefox 10 for
    developers](https://developer.mozilla.org/en/Firefox_10_for_developers)


56. [Firefox Release Notes
    (10.0)](http://www.mozilla.org/en-US/firefox/10.0/releasenotes/)

57. [Firefox Release Notes
    (11.0)](http://www.mozilla.org/en-US/firefox/11.0/releasenotes/)

58. [Firefox Release Notes
    (12.0)](http://www.mozilla.org/en-US/firefox/12.0/releasenotes/)

59. [Firefox Release Notes
    (13.0)](http://www.mozilla.org/en-US/firefox/13.0/releasenotes/)

60. [4](http://hacks.mozilla.org/2012/03/firefox-aurora-13-developer-tools-updates/)

61. [5](https://developer.mozilla.org/en-US/docs/CSS/column-fill)

62. [6](https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Map)

63. [7](https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Set)

64. [8](https://developer.mozilla.org/en-US/docs/CSS/background-position)

65. [9](https://developer.mozilla.org/en-US/docs/CSS/:invalid)

66. [10](https://developer.mozilla.org/en-US/docs/CSS/angle)

67.

68. [Firefox Release Notes
    (14.0.1)](http://www.mozilla.org/en-US/firefox/14.0.1/releasenotes/)

69. [11](https://bugzilla.mozilla.org/show_bug.cgi?id=639705)

70. [Opt-in activation for
    plugins](../Page/:mozillawiki:Opt-in_activation_for_plugins.md "wikilink")

71. [12](https://bugzilla.mozilla.org/show_bug.cgi?id=566489)

72. [13](http://blog.mozilla.org/ux/2012/06/site-identity-ui-updates/)

73. [14](https://developer.mozilla.org/en-US/docs/API/Pointer_Lock_API)

74. [15](https://bugzilla.mozilla.org/show_bug.cgi?id=697132)

75. [16](https://developer.mozilla.org/en-US/docs/Firefox_14_for_developers)

76. [17](https://www.mozilla.org/security/known-vulnerabilities/firefox.html)

77. [18](https://bugzilla.mozilla.org/show_bug.cgi?id=743598)

78. [19](https://bugzilla.mozilla.org/show_bug.cgi?id=752149)

79. [20](https://bugzilla.mozilla.org/show_bug.cgi?id=758885)

80. [Firefox Release Notes
    (15.0)](http://www.mozilla.org/en-US/firefox/15.0/releasenotes/)

81. [21](http://www.mozilla.org/en-US/firefox/all.html)

82. [22](https://developer.mozilla.org/en-US/docs/CSS/word-break)

83. [23](https://developer.mozilla.org/en-US/docs/HTML/Element/source)

84. [24](https://developer.mozilla.org/en-US/docs/HTML/Element/audio)

85. [25](https://developer.mozilla.org/en-US/docs/HTML/Element/video)

86. [26](https://bugzilla.mozilla.org/show_bug.cgi?id=720987)

87. [Firefox Release Notes
    (16.0)](http://www.mozilla.org/en-US/firefox/16.0/releasenotes/)

88. [27](http://www.marcozehe.de/2012/04/30/initial-voiceover-support-now-in-firefox-nightly-builds-for-mac-os-x/)

89. [28](https://developer.mozilla.org/en-US/docs/Apps/Getting_Started)

90. [29](https://blog.mozilla.org/javascript/2012/08/28/incremental-gc-in-firefox-16/)

91. [30](https://hacks.mozilla.org/2012/07/aurora-16-is-out/)

92. [31](https://bugzilla.mozilla.org/show_bug.cgi?id=783393)

93. [32](https://bugzilla.mozilla.org/show_bug.cgi?id=650355)

94. [33](https://bugzilla.mozilla.org/show_bug.cgi?id=772341)

95. [34](https://bugzilla.mozilla.org/show_bug.cgi?id=655920)

96. [35](https://bugzilla.mozilla.org/show_bug.cgi?id=687724)

97. [36](https://bugzilla.mozilla.org/show_bug.cgi?id=728831)

98. [Firefox Release Notes
    (17.0)](http://www.mozilla.org/en-US/firefox/17.0/releasenotes/)

99. [37](http://blog.mozilla.org/addons/2012/10/11/click-to-play-coming-firefox-17/)

100. [38](https://blog.mozilla.org/futurereleases/2012/10/04/we-bid-you-adieu-spotted-cat/)

101. [39](https://developer.mozilla.org/en-US/docs/HTML/Element/iframe#attr-sandbox)

102. [40](https://bugzilla.mozilla.org/show_bug.cgi?id=769150)

103. [41](https://bugzilla.mozilla.org/show_bug.cgi?id=780345)

104. [美、英政府利用Firefox漏洞監聽反追蹤軟體Tor](http://www.ithome.com.tw/itadm/article.php?c=83018)
     ，即時新聞，[iThome](../Page/iThome.md "wikilink")
     online，文：林妍溱，2013-10-07

105. [Firefox Release Notes
     (18.0)](http://www.mozilla.org/en-US/firefox/18.0/releasenotes/)

106. <https://bugzilla.mozilla.org/show_bug.cgi?id=62178>

107. <https://bugzilla.mozilla.org/show_bug.cgi?id=769764>

108. [Firefox Release Notes
     (19.0)](http://www.mozilla.org/en-US/firefox/19.0/releasenotes/)

109. <https://hacks.mozilla.org/2012/10/firefox-development-highlights-viewport-percentage-canvas-toblob-and-webrtc/>

110. <https://bugzilla.mozilla.org/buglist.cgi?quicksearch=715402%2C756313>

111. <https://developer.mozilla.org/en-US/docs/CSS/@page>

112. <https://developer.mozilla.org/en-US/docs/CSS/length#Viewport-percentage_lengths>

113. <https://developer.mozilla.org/en-US/docs/CSS/text-transform>

114. <https://bugzilla.mozilla.org/show_bug.cgi?id=825205>

115. <https://bugzilla.mozilla.org/show_bug.cgi?id=802274>

116. <https://bugzilla.mozilla.org/show_bug.cgi?id=825734>

117. [Firefox Release Notes
     (20.0)](http://www.mozilla.org/en-US/firefox/20.0/releasenotes/)

118. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

119. <https://support.mozilla.org/en-US/kb/private-browsing-browse-web-without-saving-info#os=win7&browser=fx20>

120. <https://support.mozilla.org/en-US/kb/find-and-manage-downloaded-files?esab=a&s=download+manager&r=0&as=s#os=mac&browser=fx20>

121. <https://bugzilla.mozilla.org/show_bug.cgi?id=792438>

122. <https://bugzilla.mozilla.org/show_bug.cgi?id=789932>

123. <https://bugzilla.mozilla.org/buglist.cgi?quicksearch=818296%2C818739>

124. <https://bugzilla.mozilla.org/show_bug.cgi?id=814562>

125. <https://bugzilla.mozilla.org/show_bug.cgi?id=808148>

126. <https://hacks.mozilla.org/2012/12/firefox-development-highlights-per-window-private-browsing-canvas-globalcompositeoperation-new-values/>

127. <http://blog.pearce.org.nz/2012/12/html5-video-playbackrate-and-ogg.html>

128. <https://bugzilla.mozilla.org/show_bug.cgi?id=793972>

129. <https://bugzilla.mozilla.org/show_bug.cgi?id=829284>

130. [Firefox Release Notes
     (21.0)](http://www.mozilla.org/en-US/firefox/21.0/releasenotes/)

131. <https://blog.mozilla.org/blog/2013/05/14/stay-social-with-firefox/>

132. <https://blog.mozilla.org/privacy/2013/01/28/newdntui/>

133. <https://blog.mozilla.org/futurereleases/2013/05/14/firefox-heal-thyself/>

134. <https://bugzilla.mozilla.org/show_bug.cgi?id=816298>

135. <https://bugzilla.mozilla.org/show_bug.cgi?id=809821>

136. <https://developer.mozilla.org/en-US/docs/E4X>

137.

138. <https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main?redirectlocale=en-US&redirectslug=HTML%2FElement%2Fmain>

139. <http://updates.html5rocks.com/2012/03/A-New-Experimental-Feature-style-scoped>

140. <https://bugzilla.mozilla.org/show_bug.cgi?id=833719>

141. <https://bugzilla.mozilla.org/show_bug.cgi?id=847627>

142. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

143. [Firefox Release Notes
     (22.0)](http://www.mozilla.org/en-US/firefox/22.0/releasenotes/)

144. <https://blog.mozilla.org/futurereleases/2013/05/16/firefox-beta-now-includes-webrtc-on-by-default/>

145. <https://blog.mozilla.org/luke/2013/03/21/asm-js-in-firefox-nightly/>

146. [Firefox最新29版效能激增的秘密Asm.js](http://www.ithome.com.tw/news/87540)，李建興，ithome，2014-05-09

147. <https://bugzilla.mozilla.org/buglist.cgi?quicksearch=829747>

148. <https://bugzilla.mozilla.org/buglist.cgi?quicksearch=716140%2C689623%2C661304>

149. <https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Flexible_boxes?redirectlocale=en-US&redirectslug=CSS%2FTutorials%2FUsing_CSS_flexible_boxes>

150. <https://bugzilla.mozilla.org/show_bug.cgi?id=839371>

151. <https://bugzilla.mozilla.org/show_bug.cgi?id=629801>

152. <https://bugzilla.mozilla.org/show_bug.cgi?id=829952>

153. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

154. [Firefox Release Notes
     (23.0)](http://www.mozilla.org/en-US/firefox/23.0/releasenotes/)

155. <https://blog.mozilla.org/security/2013/05/16/mixed-content-blocking-in-firefox-aurora/>

156. <https://bugzilla.mozilla.org/show_bug.cgi?id=838675>

157. <https://hacks.mozilla.org/2013/05/firefox-developer-tool-features-for-firefox-23/>

158. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

159. [Firefox Release Notes
     (24.0)](http://www.mozilla.org/en-US/firefox/24.0/releasenotes/)

160. <http://msujaws.wordpress.com/2013/06/17/picking-up-the-crumbs/>

161. <https://bugzilla.mozilla.org/show_bug.cgi?id=577727>

162. <https://bugzilla.mozilla.org/show_bug.cgi?id=867465>

163. <https://bugzilla.mozilla.org/show_bug.cgi?id=791670>

164. <https://bugzilla.mozilla.org/show_bug.cgi?id=600207>

165. <http://www.robodesign.ro/mihai/blog/the-browser-console-is-replacing-the-error-console>

166. <https://bugzilla.mozilla.org/show_bug.cgi?id=886886>

167. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

168. [Firefox Release Notes
     (25.0)](http://www.mozilla.org/en-US/firefox/25.0/releasenotes/)

169. <https://bugzilla.mozilla.org/show_bug.cgi?id=483446>

170. <https://bugzilla.mozilla.org/buglist.cgi?quicksearch=717379%2C866847%2C866849%2C886949>

171. <https://bugzilla.mozilla.org/show_bug.cgi?id=802895>

172. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

173. [Firefox Release Notes
     (26.0)](http://www.mozilla.org/en-US/firefox/26.0/releasenotes/)

174. <https://blog.mozilla.org/futurereleases/2013/09/24/plugin-activation-in-firefox/>

175. <https://developer.mozilla.org/en-US/docs/Social_API>

176. <https://bugzilla.mozilla.org/show_bug.cgi?id=298619>

177. <https://bugzilla.mozilla.org/show_bug.cgi?id=812695>

178. <https://bugzilla.mozilla.org/show_bug.cgi?id=847223>

179. <https://bugzilla.mozilla.org/show_bug.cgi?id=914479>

180. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

181. [Firefox Release Notes
     (27.0)](http://www.mozilla.org/en-US/firefox/27.0/releasenotes/)

182.

183. <http://www.ietf.org/rfc/rfc5246.txt>

184. <http://dev.chromium.org/spdy/spdy-protocol/spdy-protocol-draft3-1>

185. <http://mcc.id.au/blog/2013/10/all-unset>

186. <https://bugzilla.mozilla.org/show_bug.cgi?id=762761>

187. <https://bugzilla.mozilla.org/show_bug.cgi?id=261037>

188. <https://bugzilla.mozilla.org/show_bug.cgi?id=766282>

189. <https://bugzilla.mozilla.org/show_bug.cgi?id=880672>

190. <http://wingolog.org/archives/2013/10/07/es6-generators-and-iteration-in-spidermonkey>

191. <https://bugzilla.mozilla.org/show_bug.cgi?id=896264>

192. <https://bugzilla.mozilla.org/show_bug.cgi?id=768067>

193. <https://bugzilla.mozilla.org/show_bug.cgi?id=740200>

194. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

195. [Firefox Release Notes
     (28.0)](http://www.mozilla.org/en-US/firefox/28.0/releasenotes/)

196. [「Firefox 28」正式版登場、ブラウザをゲームパッドで操作することが可能に |
     GIGAZINE](http://gigazine.net/news/20140318-firefox-28/)，2014年3月25日最後查閱。

197. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

198. [Firefox Release Notes
     (29.0)](http://www.mozilla.org/en-US/firefox/29.0/releasenotes/)

199.

200.

201.

202.

203.

204.

205. [Firefox Release Notes
     (30.0)](http://www.mozilla.org/en-US/firefox/30.0/releasenotes/)

206.

207.

208.

209.

210. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

211. [Firefox Release Notes
     (31.0)](http://www.mozilla.org/en-US/firefox/31.0/releasenotes/)

212.

213.

214.

215.

216.

217.

218.

219.

220.

221.

222.

223.

224.

225.

226.

227.

228.

229.

230.

231. [Firefox Release Notes
     (32.0)](http://www.mozilla.org/en-US/firefox/32.0/releasenotes/)

232.

233.

234.

235.

236.

237.

238.

239.

240.

241.

242.

243.

244.

245.

246.

247. <https://www.mozilla.org/security/known-vulnerabilities/firefox.html>

248. [Firefox Release Notes
     (33.0)](http://www.mozilla.org/en-US/firefox/33.0/releasenotes/)

249.

250.

251.

252.

253.

254.

255.

256.

257.

258.

259.

260.

261.

262.

263.

264.

265. [Firefox Release Notes
     (34.0)](http://www.mozilla.org/en-US/firefox/34.0/releasenotes/)

266.

267. [Firefox 免費通訊服務「Firefox
     Hello」開放使用者測試](http://technews.tw/2014/10/17/firefox-hello/)，TechNews
     科技新報，20114-10-17

268. [Firefox Hello
     即時影音通訊功能測試版上線，瀏覽器直接通話不需外掛](http://www.ithome.com.tw/news/91655)，iThome臺灣，20114-10-17

269.

270.

271.

272.

273.

274.

275.

276. [Firefox Release Notes
     (35.0)](https://www.mozilla.org/en-US/firefox/35.0/releasenotes/)

277.

278.

279.

280.

281.

282.

283. [Firefox Release Notes
     (36.0)](http://www.mozilla.org/en-US/firefox/36.0/releasenotes/)

284.

285. [Firefox Release Notes
     (37.0)](http://www.mozilla.org/en-US/firefox/37.0/releasenotes/)

286.

287. [Firefox Release Notes
     (38.0)](http://www.mozilla.org/en-US/firefox/38.0/releasenotes/)

288.

289. [Firefox Release Notes
     (39.0)](http://www.mozilla.org/en-US/firefox/39.0/releasenotes/)

290.

291. [42](https://www.mozilla.org/en-US/firefox/40.0/releasenotes/)

292. [43](https://www.mozilla.org/en-US/firefox/41.0/releasenotes/)

293. [44](https://www.mozilla.org/en-US/firefox/42.0/releasenotes/)

294. [45](https://www.mozilla.org/en-US/firefox/43.0/releasenotes/)

295. [46](https://www.mozilla.org/en-US/firefox/44.0/releasenotes/)

296. [47](https://www.mozilla.org/en-US/firefox/45.0/releasenotes/)

297. [48](https://www.mozilla.org/en-US/firefox/46.0/releasenotes/)

298. [49](https://www.mozilla.org/en-US/firefox/47.0/releasenotes/)

299. [50](https://www.mozilla.org/en-US/firefox/48.0/releasenotes/)

300. [51](https://www.mozilla.org/en-US/firefox/49.0/releasenotes/)

301. [52](https://www.mozilla.org/en-US/firefox/50.0/releasenotes/)

302. [53](https://www.mozilla.org/en-US/firefox/51.0/releasenotes/)

303. [54](https://www.mozilla.org/en-US/firefox/52.0/releasenotes/)

304. [55](https://www.mozilla.org/en-US/firefox/53.0/releasenotes/)

305. [56](https://www.mozilla.org/en-US/firefox/54.0/releasenotes/)

306. [57](https://www.mozilla.org/en-US/firefox/55.0/releasenotes/)

307. [58](https://www.mozilla.org/en-US/firefox/56.0/releasenotes/)

308.  Firefox Quantum
     Browser|website=Mozilla|language=en-US|accessdate=2017-10-09}}

309. [59](https://www.mozilla.org/en-US/firefox/57.0/releasenotes/)

310.

311.

312.

313.

314.

315.

316.

317.

318.