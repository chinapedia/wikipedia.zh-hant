</div>

</div>

**1988冬季奥林匹克运动会**，即**第十五届冬季奥林匹克运动会**（，），于2月13日至2月28日在[加拿大](../Page/加拿大.md "wikilink")[阿尔伯塔省的](../Page/阿尔伯塔.md "wikilink")[卡尔加里举办](../Page/卡尔加里.md "wikilink")，由[加拿大总督](../Page/加拿大总督.md "wikilink")[让娜·索韦揭开序幕](../Page/加拿大总督.md "wikilink")。这届奥运会在经济上非常成功，它带来了几百万加元的盈利。这次奥运会也为长时间影响了卡尔加里，为这座城市展开了新的篇章。共有57个国家、地区参加了这届运动会。本届冬奥会的吉祥物为两只小熊。
1988年的冬季[残奥会也是最后一次在不同城市举行的一届](../Page/残奥会.md "wikilink")，之后的各届的残奥会都在相同或临近城市举行。但直到2010年由同一國家的溫哥華冬季帕拉林匹克運動會起才確定奧運會與帕運會會相同城市。

## 背景

## 焦點

  - 由加拿大总督[让娜·索韦以英国女皇](../Page/加拿大总督.md "wikilink")[伊丽莎白二世之名义揭开帷幕](../Page/伊丽莎白二世.md "wikilink")。
  - [冰壶](../Page/冰壶.md "wikilink")、[自由滑雪](../Page/自由滑雪.md "wikilink")、[短道速滑和残奥滑雪作为表演项目](../Page/短道速滑.md "wikilink")。
  - 由于暖风影响，冬奥会历史上第一次延长至16日。速滑项目在室内冰场进行，高山滑雪在人工雪上进行等。
  - 奥运会第一次闭幕式与开幕式在同一体育场进行。

## 比赛场馆

  - [加拿大奥林匹克公园(卡尔加里)](../Page/加拿大奥林匹克公园\(卡尔加里\).md "wikilink")
  - [坎莫尔诺蒂克中心](../Page/坎莫尔诺蒂克中心省立公园.md "wikilink")
  - Max Bell Centre
  - [麦克马安体育场](../Page/麦克马安体育场.md "wikilink")
  - Nakiska
  - 奥林匹克椭圆体育馆
  - 奥林匹克马鞍体育馆
  - Stampede Corral

## 比赛项目

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/1988年冬季奥林匹克运动会雪上项目.md" title="wikilink">高山滑雪</a></li>
<li><a href="../Page/1988年冬季奥林匹克运动会雪上项目.md" title="wikilink">冬季两项</a></li>
<li><a href="../Page/1988年冬季奥林匹克运动会雪上项目.md" title="wikilink">有舵雪橇</a></li>
<li><a href="../Page/1988年冬季奥林匹克运动会雪上项目.md" title="wikilink">越野滑雪</a></li>
<li><a href="../Page/1988年冬季奥林匹克运动会冰上项目.md" title="wikilink">花样滑冰</a></li>
</ul></td>
<td></td>
<td><ul>
<li><a href="../Page/1988年冬季奥林匹克运动会冰上项目.md" title="wikilink">冰球</a></li>
<li><a href="../Page/1988年冬季奥林匹克运动会雪上项目.md" title="wikilink">无舵雪橇</a></li>
<li><a href="../Page/1988年冬季奥林匹克运动会雪上项目.md" title="wikilink">北欧两项</a></li>
<li><a href="../Page/1988年冬季奥林匹克运动会雪上项目.md" title="wikilink">跳台滑雪</a></li>
<li><a href="../Page/1988年冬季奥林匹克运动会冰上项目.md" title="wikilink">速度滑冰</a></li>
</ul></td>
<td></td>
<td><ul>
<li><strong>表演项目</strong>
<ul>
<li><a href="../Page/冰壶.md" title="wikilink">冰壶</a></li>
<li><a href="../Page/短道速滑.md" title="wikilink">短道速滑</a></li>
<li><a href="../Page/自由式滑雪.md" title="wikilink">自由式滑雪</a></li>
<li>残疾人滑雪</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 参赛国家及地区

共有57个国家及地区的代表团参加了本届冬奥会。

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

## 奖牌榜

<span style="font-size:smaller;">此为奖牌榜前10位，详见</span>
*[1988年冬季奥林匹克运动会奖牌榜](../Page/1988年冬季奥林匹克运动会奖牌榜.md "wikilink")*

<table>
<thead>
<tr class="header">
<th><p>colspan=5 style="border-right:0px;";| <strong>1988年冬季奥林匹克运动会奖牌榜</strong></p></th>
<th><p>style="border-left:0px"; | <a href="https://zh.wikipedia.org/wiki/File:Olympic_rings.svg" title="fig:Olympic_rings.svg">Olympic_rings.svg</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>排名</strong></p></td>
<td><p><strong>国家／地区</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>总 计</strong></p></td>
<td><p><strong>46</strong></p></td>
</tr>
</tbody>
</table>

## 奥运会的相关影响

  - 1988年时奥林匹克运动会第二次在加拿大举办。第一次是1976年举办的[蒙特利尔夏季奥运会](../Page/1976年夏季奥林匹克运动会.md "wikilink")。本届冬奥会与蒙特利尔奥运一样，加拿大队没有取得金牌。之后，加拿大的[温哥华赢得了](../Page/温哥华.md "wikilink")[2010年冬奥会的举办权](../Page/2010年冬季奥林匹克运动会.md "wikilink")，加拿大奥委会声称希望在2010温哥华冬奥会上会改变这个状况。
  - 22年之後，在[温哥华冬季奥运会](../Page/2010年冬季奥林匹克运动会.md "wikilink")，加拿大一口氣囊括了14面冬奧金牌，打破之前兩次舉辦奧運會零金的記錄，同時位在獎牌榜首位。
  - 卡尔加里在这次奥运会上得到了国际上的认知，留下的奥运场馆也成为了国际重要比赛、会议、训练的场地。
  - 與造成巨額虧損的[蒙特利尔夏季奥运会不同](../Page/1976年夏季奥林匹克运动会.md "wikilink")，卡尔加里和之後的[温哥华冬季奥运会被認為是加拿大比較成功的兩次奧運會](../Page/2010年冬季奥林匹克运动会.md "wikilink")。

## 外部链接

  - [國際奧委會關於卡爾加里冬季奧運會的資料](http://www.olympic.org/uk/games/past/index_uk.asp?OLGT=2&OLGY=1988)

{{-}}

[Category:冬季奥林匹克运动会](../Category/冬季奥林匹克运动会.md "wikilink")
[Category:1988年加拿大體育](../Category/1988年加拿大體育.md "wikilink")
[Category:1988年冬季奧林匹克運動會](../Category/1988年冬季奧林匹克運動會.md "wikilink")
[Category:1988年綜合運動會](../Category/1988年綜合運動會.md "wikilink")
[Category:加拿大綜合運動會](../Category/加拿大綜合運動會.md "wikilink")
[Category:加拿大主辦的國際體育賽事](../Category/加拿大主辦的國際體育賽事.md "wikilink")