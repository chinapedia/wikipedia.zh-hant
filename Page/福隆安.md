[Fulunga.jpg](https://zh.wikipedia.org/wiki/File:Fulunga.jpg "fig:Fulunga.jpg")

**福隆安**（），[富察氏](../Page/富察.md "wikilink")，字**珊林**，[乾隆十一年](../Page/乾隆.md "wikilink")（1746年）生。父[傅恒](../Page/傅恒.md "wikilink")，官至[大学士](../Page/大学士.md "wikilink")，封[一等忠勇公](../Page/一等忠勇公.md "wikilink")，为[孝贤纯皇后侄](../Page/孝贤纯皇后.md "wikilink")。

乾隆二十三年（1758年）授福隆安和硕[额驸](../Page/额驸.md "wikilink")，二十五年（1760年）三月尚[乾隆帝第四女](../Page/乾隆帝.md "wikilink")[和硕和嘉公主](../Page/和硕和嘉公主.md "wikilink")，三十五年七月袭父爵，封一等忠勇公，官至[兵部尚书](../Page/兵部尚书.md "wikilink")，兼[军机大臣](../Page/军机大臣.md "wikilink")。乾隆三十八年（1773年）四月加[太子太保](../Page/太子太保.md "wikilink")。乾隆四十九年（1784年）三月二十四卒，年三十九。谥**勤恪**。有二子，乾隆帝分別命名為[豐紳濟倫](../Page/豐紳濟倫.md "wikilink")、豐紳果勒敏。

[琼瑶的](../Page/琼瑶.md "wikilink")《[还珠格格](../Page/还珠格格.md "wikilink")》系列中主角**福尔康**的原型即为福隆安或其弟福康安。主角紫薇有[乾隆帝第四女](../Page/乾隆帝.md "wikilink")[和硕和嘉公主的影子](../Page/和硕和嘉公主.md "wikilink")。

## 參考文獻

{{-}}

[Category:清朝大学士](../Category/清朝大学士.md "wikilink")
[F富](../Category/清朝额驸.md "wikilink")
[F富](../Category/军机大臣.md "wikilink")
[F富](../Category/满洲镶黄旗人.md "wikilink")
[F福](../Category/沙濟富察氏.md "wikilink")
[Category:清朝兵部尚書](../Category/清朝兵部尚書.md "wikilink")
[Category:清朝太子三師](../Category/清朝太子三師.md "wikilink")
[Category:清朝工部尚書](../Category/清朝工部尚書.md "wikilink")
[Category:鑲黃旗滿洲都統](../Category/鑲黃旗滿洲都統.md "wikilink")
[Category:正白旗滿洲都統](../Category/正白旗滿洲都統.md "wikilink")