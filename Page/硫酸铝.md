\-{H|zh-cn:污; zh-tw:汙}-
**硫酸铝**（[化学式](../Page/化学式.md "wikilink")：Al<sub>2</sub>(SO<sub>4</sub>)<sub>3</sub>）是一个被广泛运用的工业试剂，通常会与[明矾混淆](../Page/明矾.md "wikilink")。硫酸铝通常被作为[絮凝剂](../Page/絮凝.md "wikilink")，用于提纯[饮用水及](../Page/饮用水.md "wikilink")[污水处理设备当中](../Page/污水处理.md "wikilink")，也用于[造纸工业](../Page/造纸.md "wikilink")。

自然状况下，硫酸铝几乎不以无水盐形式存在。它会形成一系列的水合物，其中十六水硫酸铝是最常见的。

硫酸铝也是一种很有效的[软体动物](../Page/软体动物.md "wikilink")[杀虫剂](../Page/杀虫剂.md "wikilink")，能杀灭西班牙[鼻涕虫](../Page/鼻涕虫.md "wikilink")（Spanish
slugs）。

## 实验室制备

硫酸铝可以通过将[氢氧化铝](../Page/氢氧化铝.md "wikilink")（Al(OH)<sub>3</sub>）投入[硫酸](../Page/硫酸.md "wikilink")（H<sub>2</sub>SO<sub>4</sub>）中制备。

  -
    2Al(OH)<sub>3</sub> + 3H<sub>2</sub>SO<sub>4</sub> +
    10H<sub>2</sub>O →
    Al<sub>2</sub>(SO<sub>4</sub>)<sub>3</sub>·16H<sub>2</sub>O

## 用途

  - 在[纺织品的印染中作为媒介](../Page/纺织品.md "wikilink")。由於Al<sup>3+</sup>會和SO<sub>4</sub><sup>2-</sup>水解出[氫氧化鋁](../Page/氫氧化鋁.md "wikilink")[膠體](../Page/膠體.md "wikilink")，因此硫酸铝常用于[净水](../Page/净水.md "wikilink")，在水的净化中，它使杂质凝结，更容易[沉淀和](../Page/沉淀.md "wikilink")[过滤](../Page/过滤.md "wikilink")。
  - 当溶解于大量[中性或微](../Page/中性.md "wikilink")[碱性的水中时](../Page/碱性.md "wikilink")，产生[胶体沉淀](../Page/胶体.md "wikilink")[氢氧化铝](../Page/氢氧化铝.md "wikilink")，Al(OH)<sub>3</sub>。在印染布料时，氢氧化铝胶体使得染料更容易附着于[植物](../Page/植物.md "wikilink")[纤维之上](../Page/纤维.md "wikilink")。
  - 硫酸铝也被用来调节[土壤](../Page/土壤.md "wikilink")[pH值](../Page/pH.md "wikilink")，因为它[水解生成氢氧化铝的同时产生少量的](../Page/水解.md "wikilink")[硫酸稀溶液](../Page/硫酸.md "wikilink")。

## 参见

  - [硫酸](../Page/硫酸.md "wikilink")
  - [铝](../Page/铝.md "wikilink")
  - [明矾](../Page/明矾.md "wikilink")
  - [胆矾](../Page/胆矾.md "wikilink")

## 注释

  -
## 外部链接

  - [国际化学安全卡1191](http://www.ilo.org/public/english/protection/safework/cis/products/icsc/dtasht/_icsc11/icsc1191.htm)
  - [美国国立职业安全与健康研究所化学危险手册](http://www.cdc.gov/niosh/npg/npgd0024.html)
  - [硫酸铝字典-Guidechem.com](http://www.guidechem.com/dictionary/10043-01-3.html)

[Category:硫酸盐](../Category/硫酸盐.md "wikilink")
[Category:铝化合物](../Category/铝化合物.md "wikilink")