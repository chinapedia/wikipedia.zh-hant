[Demis_-_Shikotan.png](https://zh.wikipedia.org/wiki/File:Demis_-_Shikotan.png "fig:Demis_-_Shikotan.png")
[North_4_islands.png](https://zh.wikipedia.org/wiki/File:North_4_islands.png "fig:North_4_islands.png")
[Малокурильское,_o.Шикотан.png](https://zh.wikipedia.org/wiki/File:Малокурильское,_o.Шикотан.png "fig:Малокурильское,_o.Шикотан.png")
**色丹島**（），俄语名为**希科坦島**（）\[1\]，面積為255.12[平方千米](../Page/平方千米.md "wikilink")，2006年人口為925人，2018年人口為3070人，在[南千岛群岛中面積僅大於](../Page/南千岛群岛.md "wikilink")[齒舞群島](../Page/齒舞群島.md "wikilink")。目前為[俄羅斯实际管理](../Page/俄羅斯.md "wikilink")，划归[远东联邦管区](../Page/远东联邦管区.md "wikilink")[萨哈林州](../Page/萨哈林州.md "wikilink")。[日本主张该岛的主权](../Page/日本.md "wikilink")，島名的由來是阿伊努語中的「大村」（si-kotan）。地質上為[根室半島於海中延伸露出水面的部份](../Page/根室半島.md "wikilink")。

色丹岛俄語名曾稱**什潘贝格岛**，以其发现者[马丁·什潘贝格命名](../Page/马丁·什潘贝格.md "wikilink")。

## 概要

色丹岛是南千岛群岛中较小的岛。最高峰为[斜古丹山](../Page/斜古丹山.md "wikilink")（标高413公尺），中心集落为[斜古丹](../Page/斜古丹.md "wikilink")。

第二次世界大战结束后的1945年8月底9月初，苏联按照[雅尔塔协定占领含该岛在内的](../Page/雅尔塔协定.md "wikilink")[南千岛群岛](../Page/南千岛群岛.md "wikilink")。2004年俄羅斯曾打算歸還色丹島和[齒舞群島的行政权](../Page/齒舞群島.md "wikilink")，但因日本堅持四岛全部歸還而未達成一致。

2017年12月，日本政府「地震調查委員會」發布地震預測\[2\]，預測該區可能會發生8.8規模強震\[3\]。

## 图册

[File:Шикотан3.jpg|色丹岛，摄于1980年](File:Шикотан3.jpg%7C色丹岛，摄于1980年)
[File:Шикотан.jpg|色丹岛的灯塔](File:Шикотан.jpg%7C色丹岛的灯塔)
[File:Шикотан5.jpg|色丹岛景观](File:Шикотан5.jpg%7C色丹岛景观)

## 相关条目

  - [色丹郡](../Page/色丹郡.md "wikilink") [色丹村](../Page/色丹村.md "wikilink")

## 参考资料

## 外部連接

  - [Kuril Island Network - A volunteer group dedicated to raising
    awareness of the habitat on the
    Kurils](https://web.archive.org/web/20050409181730/http://www.kurilnature.org/)
  - [1](https://web.archive.org/web/20101223015139/http://www.oceandots.com/pacific/kuril/shikotan.php)
    - Satellite image of Shikotan
  - [Ainu speaker and activist explains the meaning of the South Kuril
    island
    names](http://search.japantimes.co.jp/cgi-bin/nn20061008f1.html)

[Category:南千岛群岛](../Category/南千岛群岛.md "wikilink")

1.
2.
3.