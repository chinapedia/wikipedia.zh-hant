**埃爾·格雷考**（，），[西班牙](../Page/西班牙.md "wikilink")[文藝復興時期](../Page/文藝復興時期.md "wikilink")[畫家](../Page/畫家.md "wikilink")、[雕塑家與](../Page/雕塑家.md "wikilink")[建築家](../Page/建築師.md "wikilink")。「埃爾·格雷考」在西班牙文中意為「希臘人」，是依格雷考的希臘血統而取的別名；格雷考在畫作上通常署名以[希臘文全名](../Page/希臘字母.md "wikilink")
**Δομήνικος Θεοτοκόπουλος**（多米尼克·提托克波洛斯，西班牙文：Doménikos Theotokópoulos）。

格雷考出生於[後拜占庭藝術時期的](../Page/後拜占庭藝術.md "wikilink")[克里特](../Page/克里特.md "wikilink")，當時為[威尼斯共和國所治理](../Page/威尼斯共和國.md "wikilink")。他原受傳統繪畫教育，直到26歲時，和許多希臘藝術家一樣，動身前往[威尼斯旅行後畫風不變](../Page/威尼斯.md "wikilink")\[1\]。1570年，格雷考遷居至羅馬，在當地經營一間工作坊、創作了一系列的作品。在此期間，格雷考受到[威尼斯文藝復興的薰染](../Page/義大利文藝復興.md "wikilink")，在畫風中融入了[矯飾主義](../Page/矯飾主義.md "wikilink")。1577年，他轉往[西班牙](../Page/西班牙.md "wikilink")[托雷多發展](../Page/托雷多_\(西班牙\).md "wikilink")，直到逝世。在托雷多，格雷考接受了些許重要的委託，並創作出他最著名的畫作。

格雷考兼具戲劇性與表現主義的畫風在當代並不受寵，但在20世紀獲肯。格雷考被公認是[表現主義及](../Page/表現主義.md "wikilink")[立體主義先驅](../Page/立體主義.md "wikilink")。格雷考被現代學者視為一極與眾不同、具高度個人色彩的藝術家，不屬任何傳統流派\[2\]。他的畫作以彎曲瘦長的身形為特色，[用色怪誕而變幻無常](../Page/色素.md "wikilink")，融合了[拜占庭傳統與](../Page/拜占庭藝術.md "wikilink")[西方繪畫風格](../Page/西方繪畫.md "wikilink")\[3\]。

## 引用

## 參考文獻

### 出版資料 (書與文章)

<div class="references-small">

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -

</div>

### 線上資料

<div class="references-small">

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -

</div>

## 延伸閱讀

<div class="references-small">

  -
  -
  -
  -
  -
  -

</div>

## 外部連結

<div class="references-small">

  - [埃爾·格雷考基金會 - 虛擬藝廊](http://www.el-greco-foundation.org)

  -
  -
  - [埃爾·格雷考](http://www.moreeuw.com/histoire-art/biographie-el-greco.htm)

  - [埃爾·格雷考](http://dominikostheotokopoulos.webnode.gr/)

</div>

[Category:西班牙画家](../Category/西班牙画家.md "wikilink")
[Category:希腊画家](../Category/希腊画家.md "wikilink")
[Category:文藝復興畫家](../Category/文藝復興畫家.md "wikilink")
[Category:克里特大区人](../Category/克里特大区人.md "wikilink")

1.  J. Brown, *El Greco of Toledo*, 75-77
2.
3.  M. Lambraki-Plaka, *El Greco—The Greek*, 60