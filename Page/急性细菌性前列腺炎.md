**急性细菌性前列腺炎**()是由非特异性细菌引起的[前列腺组织的急性](../Page/前列腺.md "wikilink")[炎症称之为急性细菌性前列腺炎](../Page/炎症.md "wikilink")。它是[前列腺炎中的罕见类型](../Page/前列腺炎.md "wikilink")。

## 临床表现

**急性细菌性前列腺炎**发病急，[高热](../Page/高热.md "wikilink")，[尿频、尿急、尿痛](../Page/尿频、尿急、尿痛.md "wikilink")。[肛指检查發現前列腺肿大](../Page/肛指检查.md "wikilink")、变软，明显触痛。此时严禁**前列腺按摩**。取终末尿，细菌培养找致病菌；做药敏实验，制定最佳抗菌治疗方案。

## 治疗

一般，急性细菌性前列腺炎患者的前列腺包膜可以透过治疗用抗菌素，大多数可以达到有效治疗浓度，因此急性细菌性前列腺炎多能治愈，不转变为[慢性前列腺炎](../Page/慢性前列腺炎.md "wikilink")；治疗不及时或治疗不当，急性细菌性前列腺炎发展为[前列腺脓肿](../Page/前列腺脓肿.md "wikilink")。

## 预后

如果得到及时诊断及正确治疗，预后良好。

## 附加圖片

Image:Gray1160.png|前列腺、尿道，及精囊。 Image:Gray539.png|骨盆動脈。

`Image:Gray1136.png|從右側所視男性盆腔器官。`

## 外部連結

  -
[category:细菌性疾病](../Page/category:细菌性疾病.md "wikilink")

[Category:性病](../Category/性病.md "wikilink")
[Category:醫療急症](../Category/醫療急症.md "wikilink")
[Category:前列腺疾病](../Category/前列腺疾病.md "wikilink")