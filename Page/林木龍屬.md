**林木龍屬**（屬名：*Silvisaurus*）又名**釘肩龍**、**叢林甲龍**，意為「林木蜥蜴」，是[甲龍下目](../Page/甲龍下目.md "wikilink")[結節龍科的一屬](../Page/結節龍科.md "wikilink")，生存於中[白堊紀的](../Page/白堊紀.md "wikilink")[堪薩斯州](../Page/堪薩斯州.md "wikilink")。

## 發現與種

[正模標本是由](../Page/正模標本.md "wikilink")[堪薩斯大學的T](../Page/堪薩斯大學.md "wikilink").
H.
Eaton發現於[堪薩斯州的](../Page/堪薩斯州.md "wikilink")[達科他組](../Page/達科他組.md "wikilink")，年代為晚[阿普第階到早](../Page/阿普第階.md "wikilink")[森諾曼階](../Page/森諾曼階.md "wikilink")，化石由頭顱骨與[薦骨構成](../Page/薦骨.md "wikilink")。林木龍目前只有一個種，[模式種](../Page/模式種.md "wikilink")*S.
condrayi*。

## 古生物學

根據這些化石，林木龍被估計身長接近4公尺。林木龍的頭顱骨長度為33公分，寬度為25公分。林木龍的骨質[次生顎保存並不完整](../Page/次生顎.md "wikilink")，[齒骨包含至少](../Page/齒骨.md "wikilink")25顆牙齒，[基枕骨的結節基部呈球根狀](../Page/基枕骨.md "wikilink")，[前上頜骨有](../Page/前上頜骨.md "wikilink")8到9顆牙齒。除了常見的圓形、多角形[皮內成骨](../Page/皮內成骨.md "wikilink")，林木龍的肩膀與尾巴可能也有骨刺。

林木龍的口鼻部前段具有牙齒，顯示牠們是種原始的結節龍科；後期的結節龍科已演化出無齒的喙嘴。林木龍的身體可能覆蓋者圓形、多角形的皮內成骨，肩膀可能具有長刺。頭部內部具有複雜的鼻管，可能具有發聲功能\[1\]。

## 分類

林木龍為一種相當原始的[結節龍科](../Page/結節龍科.md "wikilink")，在2004年，Maryanska、Vickaryous等人宣稱[蜥結龍](../Page/蜥結龍.md "wikilink")、林木龍、[爪爪龍形成一個](../Page/爪爪龍.md "wikilink")[演化支](../Page/演化支.md "wikilink")，原始程度僅次於[雪松甲龍](../Page/雪松甲龍.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

  - Eaton, T. H., Jr. 1960. A new armored dinosaur from the Cretaceous
    of Kansas. The University of Kansas Paleontological Contributions:
    Vertebrata 8:1-24.
  - Vickaryous, Maryanska, and Weishampel 2004. Chapter Seventeen:
    Ankylosauria. in The Dinosauria (2nd edition), Weishampel, D. B.,
    Dodson, P., and Osmólska, H., editors. University of California
    Press.

</div>

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [森林龍](http://www.dinodata.org/index.php?option=com_content&task=view&id=7440&Itemid=67)
    Dinodata.org

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:結節龍科](../Category/結節龍科.md "wikilink")

1.