[TBS_headquarters_2013.JPG](https://zh.wikipedia.org/wiki/File:TBS_headquarters_2013.JPG "fig:TBS_headquarters_2013.JPG")

**日本新聞網**（，），簡稱****，是以[TBS電視台為核心的](../Page/TBS電視台.md "wikilink")[電視聯播網](../Page/電視聯播網.md "wikilink")，成立於1959年8月1日，為日本歷史最悠久的電視聯播網。

JNN成立之時，核心成員是[東京放送](../Page/東京放送控股.md "wikilink")（TBS電視台前身）和[每日放送](../Page/每日放送.md "wikilink")，有16個[電視台加盟](../Page/電視台.md "wikilink")。目前JNN共有28個成員，以聯播[新聞節目為主](../Page/新聞節目.md "wikilink")。在大多數的情況下，JNN也等同於**TBS電視網**（，又稱為「TBS系列」），後者是指TBS電視台其他類型節目的聯播服務。

## 聯播網成員

### 現在的聯播網成員

JNN成員全部都是節目表數據放送業務的放送機構，在沒有JNN成員的[秋田縣](../Page/秋田縣.md "wikilink")、[福井縣](../Page/福井縣.md "wikilink")、[德岛县](../Page/德岛县.md "wikilink")、[佐賀縣則由當地民營電視台負責](../Page/佐賀縣.md "wikilink")。

  - ● - 兼營中波電台
  - ○ - 以前兼營中波電台，現在由關聯公司或子公司經營 \[1\]
  - ■ - 實施提供地域新聞視頻的成員
  - ◆ - [第三部門局](../Page/第三部門.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p>播放地區</p></th>
<th><p>簡稱/頻道號</p></th>
<th><p>公司名</p></th>
<th><p>開播日期</p></th>
<th><p>JNN加盟日</p></th>
<th><p>備注</p></th>
<th><p>標記</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a></p></td>
<td><p><strong>HBC 1</strong></p></td>
<td><p><a href="../Page/北海道放送.md" title="wikilink">北海道放送</a></p></td>
<td><p>1957年4月1日</p></td>
<td><p>1959年8月1日成立時[2]</p></td>
<td><p>基幹局。1seg不顯示水印。</p></td>
<td><p>●■</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/青森縣.md" title="wikilink">青森縣</a></p></td>
<td><p><strong>ATV 6</strong></p></td>
<td><p><a href="../Page/青森電視台.md" title="wikilink">青森電視台</a></p></td>
<td><p>1969年12月1日</p></td>
<td><p>1975年3月31日[3]</p></td>
<td><p>1seg不顯示水印。</p></td>
<td><p>■</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/岩手縣.md" title="wikilink">岩手縣</a></p></td>
<td><p><strong>IBC 6</strong></p></td>
<td><p><a href="../Page/IBC岩手放送.md" title="wikilink">IBC岩手放送</a></p></td>
<td><p>1959年9月1日</p></td>
<td><p>1995年6月22日之前的公司名是岩手放送。</p></td>
<td><p>●■</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮城縣.md" title="wikilink">宮城縣</a></p></td>
<td><p><strong>TBC 1</strong></p></td>
<td><p><a href="../Page/東北放送.md" title="wikilink">東北放送</a></p></td>
<td><p>1959年4月1日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td><p>基幹局。但沒有加入五社連盟。</p></td>
<td><p>●■</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/秋田縣.md" title="wikilink">秋田縣</a></p></td>
<td><p>無成員台</p></td>
<td><p>新聞採訪由IBC岩手放送負責。[4][5]<br />
G Guide節目數據放送業務由<a href="../Page/秋田電視台.md" title="wikilink">秋田電視台</a>（<a href="../Page/富士新聞網.md" title="wikilink">FNN</a>/<a href="../Page/富士電視網.md" title="wikilink">FNS系列</a>）負責。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/山形縣.md" title="wikilink">山形縣</a></p></td>
<td><p><strong>TUY 6</strong></p></td>
<td><p><a href="../Page/TVU山形.md" title="wikilink">TVU山形</a></p></td>
<td><p>1989年10月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福島縣.md" title="wikilink">福島縣</a></p></td>
<td><p><strong>TUF 6</strong></p></td>
<td><p><a href="../Page/TVU福島.md" title="wikilink">TVU福島</a></p></td>
<td><p>1983年12月4日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關東廣域圈.md" title="wikilink">關東廣域圈</a></p></td>
<td><p><strong>TBS 6</strong></p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台</a></p></td>
<td><p>1955年4月1日</p></td>
<td><p>2009年4月1日</p></td>
<td><p>基幹局、<a href="../Page/核心局.md" title="wikilink">核心局</a>。2009年3月31日以前東京放送[6]是牌照持有人。[7]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新潟縣.md" title="wikilink">新潟縣</a></p></td>
<td><p><strong>BSN 6</strong></p></td>
<td><p><a href="../Page/新潟放送.md" title="wikilink">新潟放送</a></p></td>
<td><p>1958年12月24日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td><p>1959年8月1日正式加盟。1961年2月28日之前名稱為新潟電台（RNK）。</p></td>
<td><p>●■</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/長野县.md" title="wikilink">長野县</a></p></td>
<td><p><strong>SBC 6</strong></p></td>
<td><p><a href="../Page/信越放送.md" title="wikilink">信越放送</a></p></td>
<td><p>1958年10月25日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td><p>1952年3月7日之前的名稱為信濃放送（簡稱依然是SBC）。</p></td>
<td><p>●■</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山梨縣.md" title="wikilink">山梨縣</a></p></td>
<td><p><strong>UTY 6</strong></p></td>
<td><p><a href="../Page/山梨電視台.md" title="wikilink">山梨電視台</a></p></td>
<td><p>1970年4月1日</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/静岡縣.md" title="wikilink">静岡縣</a></p></td>
<td><p><strong>SBS 6</strong></p></td>
<td><p><a href="../Page/静岡放送.md" title="wikilink">静岡放送</a></p></td>
<td><p>1958年11月1日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td></td>
<td><p>●■</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/富山縣.md" title="wikilink">富山縣</a></p></td>
<td><p><strong>TUT 6</strong></p></td>
<td><p><a href="../Page/鬱金香電視台.md" title="wikilink">鬱金香電視台</a></p></td>
<td><p>1990年10月1日</p></td>
<td><p>1992年9月30日之前的名稱為TVU富山。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/石川縣.md" title="wikilink">石川縣</a></p></td>
<td><p><strong>MRO 6</strong></p></td>
<td><p><a href="../Page/北陸放送.md" title="wikilink">北陸放送</a></p></td>
<td><p>1958年12月1日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td><p>鬱金香電視台開播前，負責富山県的新聞採訪。<br />
因未播放聯播網的廣告，1997年一整年停止其除新聞採訪以外其他會員活動的處分。</p></td>
<td><p>●</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福井縣.md" title="wikilink">福井縣</a></p></td>
<td><p>無成員台</p></td>
<td><p>新聞採訪方面，由北陸放送負責、<a href="../Page/岭南_(日本).md" title="wikilink">嶺南地方由毎日放送負責</a>。<br />
G Guide節目數據放送業務由<a href="../Page/福井電視台.md" title="wikilink">福井電視台</a>（FNN/FNS系列）負責。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中京廣域圈.md" title="wikilink">中京廣域圈</a></p></td>
<td><p><strong>CBC 5</strong></p></td>
<td><p><a href="../Page/CBC电视台_(日本).md" title="wikilink">CBC電視台</a></p></td>
<td><p>1956年12月1日</p></td>
<td><p>2014年4月1日</p></td>
<td><p>基幹局。2014年3月31日為止中部日本放送是牌照持有人。[8]1seg不顯示水印。</p></td>
<td><p>○■</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/近畿廣域圈.md" title="wikilink">近畿廣域圈</a></p></td>
<td><p><strong>MBS 4</strong></p></td>
<td><p><a href="../Page/毎日放送.md" title="wikilink">毎日放送</a></p></td>
<td><p>1959年3月1日</p></td>
<td><p>1975年3月31日[9]</p></td>
<td><p>基幹局、<a href="../Page/核心局.md" title="wikilink">準核心局</a>。1seg不顯示水印。<br />
曾與德岛县唯一的一家民營電視台<a href="../Page/四國放送.md" title="wikilink">四國放送</a>(<a href="../Page/日本新聞網_(日本電視台).md" title="wikilink">NNN</a>/<a href="../Page/日本電視網協議會.md" title="wikilink">NNS系列局一起負責德岛县的G</a> Guide節目表數據放送業務，後隨著停止模擬電視播放而停止。</p></td>
<td><p>●■</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鳥取縣.md" title="wikilink">鳥取縣</a></p></td>
<td><p><strong>BSS 6</strong></p></td>
<td><p><a href="../Page/山陰放送.md" title="wikilink">山陰放送</a></p></td>
<td><p>1959年12月15日</p></td>
<td><p>1959年12月15日開播時 - 1972年9月30日期間，電視頻道只在島根縣播放。</p></td>
<td><p>●■◆</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/島根縣.md" title="wikilink">島根縣</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岡山縣.md" title="wikilink">岡山縣</a></p></td>
<td><p><strong>RSK 6</strong></p></td>
<td><p><a href="../Page/山陽放送.md" title="wikilink">山陽放送</a></p></td>
<td><p>1958年6月1日</p></td>
<td><p>1959年8月1日成立時[10]</p></td>
<td><p>基幹局。不過不是五社聯盟成員。1958年6月1日開播時 - 1979年3月31日間、電視頻道只在岡山縣播放。</p></td>
<td><p>●◆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/香川縣.md" title="wikilink">香川縣</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廣島縣.md" title="wikilink">廣島縣</a></p></td>
<td><p><strong>RCC 3</strong></p></td>
<td><p><a href="../Page/中国放送.md" title="wikilink">中国放送</a></p></td>
<td><p>1959年4月1日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td><p>1967年3月31日之前名稱為中國電台（簡稱依然是RCC）。</p></td>
<td><p>●■◆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山口縣.md" title="wikilink">山口縣</a></p></td>
<td><p><strong>tys 3</strong></p></td>
<td><p><a href="../Page/山口電視台.md" title="wikilink">山口電視台</a></p></td>
<td><p>1970年4月1日</p></td>
<td><p>[11]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德岛县.md" title="wikilink">德岛县</a></p></td>
<td><p>無成員台</p></td>
<td><p>新聞採訪由每日放送負責。[12]<br />
數碼版的G Guide節目表數據放送業務由四国放送（NNN/NNS系列）負責，模擬版的由每日放送負責。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛媛縣.md" title="wikilink">愛媛縣</a></p></td>
<td><p><strong>ITV 6</strong></p></td>
<td><p><a href="../Page/愛電視台.md" title="wikilink">愛電視台</a></p></td>
<td><p>1992年10月1日</p></td>
<td><p>2002年9月30日之前的名稱是伊予電視台。[13]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高知縣.md" title="wikilink">高知縣</a></p></td>
<td><p><strong>KUTV 6</strong></p></td>
<td><p><a href="../Page/高知電視台.md" title="wikilink">高知電視台</a></p></td>
<td><p>1970年4月1日</p></td>
<td><p>[14]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福岡縣.md" title="wikilink">福岡縣</a></p></td>
<td><p><strong>RKB 4</strong></p></td>
<td><p><a href="../Page/RKB毎日放送.md" title="wikilink">RKB毎日放送</a></p></td>
<td><p>1958年3月1日</p></td>
<td><p>1959年8月1日成立時[15]</p></td>
<td><p>基幹局。1958年8月17日之前的名稱是九州電台（簡稱依然是RKB）。<br />
在模擬電視時代，亦在佐賀縣提供模擬電視信號的G Guide節目表數據放送業務。</p></td>
<td><p>●■</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐賀県.md" title="wikilink">佐賀県</a></p></td>
<td><p>無成員台</p></td>
<td><p>新聞採訪由RKB每日放送負責。<br />
中波電台業務由<a href="../Page/長崎放送.md" title="wikilink">NBC佐賀電台</a>[16]運營。<br />
數碼版G Guide節目表數據放送業務由<a href="../Page/佐賀電視台.md" title="wikilink">佐賀電視台</a>（FNN/FNS系列）運行，模擬版由RKB每日放送運行。</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長崎縣.md" title="wikilink">長崎縣</a></p></td>
<td><p><strong>NBC 3</strong></p></td>
<td><p><a href="../Page/長崎放送.md" title="wikilink">長崎放送</a></p></td>
<td><p>1959年1月1日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td></td>
<td><p>●■</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熊本縣.md" title="wikilink">熊本縣</a></p></td>
<td><p><strong>RKK 3</strong></p></td>
<td><p><a href="../Page/熊本放送.md" title="wikilink">熊本放送</a></p></td>
<td><p>1959年4月1日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td><p>1961年5月31日之前的名稱是熊本電台（簡稱依然是RKK）。</p></td>
<td><p>●■</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大分縣.md" title="wikilink">大分縣</a></p></td>
<td><p><strong>OBS 3</strong></p></td>
<td><p><a href="../Page/大分放送.md" title="wikilink">大分放送</a></p></td>
<td><p>1959年10月1日</p></td>
<td><p>1961年3月31日之前的名稱是大分電台。</p></td>
<td><p>●■◆</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宮崎縣.md" title="wikilink">宮崎縣</a></p></td>
<td><p><strong>MRT 6</strong></p></td>
<td><p><a href="../Page/宮崎放送.md" title="wikilink">宮崎放送</a></p></td>
<td><p>1960年10月1日</p></td>
<td><p>1961年6月30日之前的名稱是宮崎電台（RMK）。</p></td>
<td><p>●■</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鹿兒島縣.md" title="wikilink">鹿兒島縣</a></p></td>
<td><p><strong>MBC 1</strong></p></td>
<td><p><a href="../Page/南日本放送.md" title="wikilink">南日本放送</a></p></td>
<td><p>1959年4月1日</p></td>
<td><p>1959年8月1日成立時</p></td>
<td><p>1961年9月30日之前的名稱是南日本電台。</p></td>
<td><p>●■</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沖縄県.md" title="wikilink">沖縄県</a></p></td>
<td><p><strong>RBC 3</strong></p></td>
<td><p><a href="../Page/琉球放送.md" title="wikilink">琉球放送</a></p></td>
<td><p>1960年6月1日</p></td>
<td><p>1972年5月15日[17]</p></td>
<td><p>[18]</p></td>
<td><p>●</p></td>
</tr>
</tbody>
</table>

### 非可收視地區收視聯播網節目之辦法

目前在秋田縣、福井縣、德島縣和佐賀縣並無JNN之加盟台，除佐賀縣外三縣的部分地區可通過接收臨近縣的JNN加盟台信號收看，而佐賀縣全境均可接收福岡縣JNN加盟台RKB每日放送。除接收無線電視訊號外，亦可通過安裝有線電視來收看JNN節目。各家有線電視系統業者可供播送的JNN加盟台不同，亦有可能不播送。
以下各電視台，是目前有購買JNN聯播網一般節目播放的電視台。

<table>
<thead>
<tr class="header">
<th><p>地域</p></th>
<th><p>简称</p></th>
<th><p>名稱</p></th>
<th><p>所属联播网</p></th>
<th><p>縣內有線電視轉播之JNN成員台</p></th>
<th><p>無線電視接收JNN成員台</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/秋田縣.md" title="wikilink">秋田縣</a></p></td>
<td><p><strong>ABS</strong></p></td>
<td><p><a href="../Page/秋田放送.md" title="wikilink">秋田放送</a></p></td>
<td><p>NNN/NNS</p></td>
<td><p>青森電視台（<a href="../Page/大馆市.md" title="wikilink">大馆市</a>）<br />
TVU山形（<a href="../Page/由利本莊市.md" title="wikilink">由利本莊市</a>）<br />
IBC岩手放送（<a href="../Page/秋田市.md" title="wikilink">秋田市</a>）</p></td>
<td><p>TVU山形（秋田市以南的沿岸）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>AFK</strong></p></td>
<td><p><a href="../Page/秋田電視台.md" title="wikilink">秋田電視台</a></p></td>
<td><p>FNN/FNS</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/福井縣.md" title="wikilink">福井縣</a></p></td>
<td><p><strong>FTB</strong></p></td>
<td><p><a href="../Page/福井電視台.md" title="wikilink">福井電視台</a></p></td>
<td><p>FNN/FNS</p></td>
<td><p>每日放送（領南地方與南越前町）<br />
北陸放送（其餘地區）</p></td>
<td><p>CBC電視台（大野市部分地區）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>FBC</strong></p></td>
<td><p><a href="../Page/福井放送.md" title="wikilink">福井放送</a></p></td>
<td><p>NNN/NNS/ANN</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德岛县.md" title="wikilink">德岛县</a></p></td>
<td><p><strong>JRT</strong></p></td>
<td><p><a href="../Page/四國放送.md" title="wikilink">四國放送</a></p></td>
<td><p>NNN/NNS</p></td>
<td><p>每日放送・山陽放送</p></td>
<td><p>每日放送・山陽放送</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐賀縣.md" title="wikilink">佐賀縣</a></p></td>
<td></td>
<td><p>無</p></td>
<td></td>
<td><p>RKB每日放送</p></td>
<td><p>RKB每日放送・长崎放送<br />
熊本放送</p></td>
<td><p>全縣皆可收到RKB每日放送<br />
的信號</p></td>
</tr>
</tbody>
</table>

## 海外支局

  - [亞洲](../Page/亞洲.md "wikilink")

<!-- end list -->

  - 東京放送（TBS）[首爾支局](../Page/首爾特別市.md "wikilink")
    ※最早RKB每日放送開設。[漢城奧運后](../Page/1988年夏季奧林匹克運動會.md "wikilink")，由TBS直接運營。
  - 北海道放送（HBC）[北京支局](../Page/北京市.md "wikilink")→TBS也有記者派遣。
  - 每日放送（MBS）[上海支局](../Page/上海市.md "wikilink")
  - RKB每日放送（RKB）[曼谷支局](../Page/曼谷.md "wikilink")→TBS也有記者派遣。

<!-- end list -->

  - [歐洲](../Page/歐洲.md "wikilink")・前蘇聯

<!-- end list -->

  - TBS[莫斯科支局](../Page/莫斯科.md "wikilink")　※HBC現在也有特派員派遣。
  - 每日放送[柏林支局](../Page/柏林.md "wikilink")
  - 中部日本放送（CBC）[維也納支局](../Page/維也納.md "wikilink")
  - TBS[巴黎支局](../Page/巴黎.md "wikilink")
  - TBS[倫敦支局](../Page/倫敦.md "wikilink")

<!-- end list -->

  - [非洲](../Page/非洲.md "wikilink")

<!-- end list -->

  - 山陽放送（RSK）[開羅支局](../Page/開羅.md "wikilink")

<!-- end list -->

  - [美國](../Page/美國.md "wikilink")

<!-- end list -->

  - TBS[華盛頓支局](../Page/華盛頓.md "wikilink")
  - TBS[紐約支局](../Page/紐約.md "wikilink")
  - TBS[洛杉磯支局](../Page/洛杉磯.md "wikilink")

## 各節新聞

### 随時

  - [JNN News](../Page/JNN_News.md "wikilink")（1959年8月\~2009年3月）
  - [THE NEWS](../Page/THE_NEWS.md "wikilink")（2009年3月至今）

### 晨間

  - 周一～周五

<!-- end list -->

  - JNN新聞（1959年8月－1971年3月）
  - [モーニングジャンボ](../Page/モーニングジャンボ.md "wikilink")（1971年4月－1972年3月）
  - [モーニングジャンボ・JNNニュースショー](../Page/モーニングジャンボJNNニュースショー.md "wikilink")（1972年4月－1974年12月）
  - [JNN NEWS
    CALL](../Page/JNN_NEWS_CALL.md "wikilink")（第一期）（1975年1月－1976年9月）
  - JNN新聞（1976年10月－1983年3月）
  - [JNNおはようニュース&スポーツ](../Page/JNNおはようニュース&スポーツ.md "wikilink")（1983年4月－1991年3月）
  - JNN八点鐘新聞（1983年4月 - 1987年3月）
  - JNN NEWS CALL（第二期）（1991年4月－1993年9月）
  - [JNNニュース
    あなたにオンタイム](../Page/あなたにオンタイム.md "wikilink")（1993年10月－1995年3月）
  - JNN News オンタイム（1995年4月－1996年5月）
  - [おはようクジラ](../Page/おはようクジラ.md "wikilink")（1996年6月－1999年3月）
  - [エクスプレス](../Page/エクスプレス_\(テレビ番組\).md "wikilink")（1999年4月－2002年3月）
  - [おはよう\!グッデイ](../Page/おはよう!グッデイ.md "wikilink")（2002年4月－2003年3月）
  - [ウォッチ\!](../Page/ウォッチ!.md "wikilink")（2003年4月－2005年3月）
  - [みのもんたの朝ズバッ\!](../Page/みのもんたの朝ズバッ!.md "wikilink")（2005年4月－）

<!-- end list -->

  - 周六

<!-- end list -->

  - JNN新聞（1959年8月－1972年3月）
  - モーニングジャンボJNNニュースショー（1972年4月－1974年12月）
  - JNN新聞（1975年1月－1984年3月）
  - JNNおはようニュース&スポーツ（1984年4月－1991年3月）
  - JNN NEWS CALL（1991年4月－1993年9月）
  - JNN News あなたにオンタイム（1993年10月－1995年3月）
  - JNN News オンタイム（1995年4月－1996年5月）
  - JNN新聞（1996年6月－2009年3月）
  - JNN THE NEWS（2009年4月4日 - 至今）

<!-- end list -->

  - 周日

<!-- end list -->

  - JNN新聞（1959年8月－1985年3月）
  - JNNおはようニュース&スポーツ（1985年4月－1991年3月）
  - JNN NEWS CALL（1991年4月－1993年9月）
  - JNN News あなたにオンタイム（1993年10月－1995年3月）
  - JNN News オンタイム（1995年4月－1996年5月）
  - JNN新聞（1996年6月－2009年3月）
  - JNN THE NEWS（2009年4月5日 - 至今）

### 午間

  - 周一～周五

<!-- end list -->

  - JNN News（1959年8月－1992年3月）
  - [JNN News
    1130](../Page/JNN_News_1130.md "wikilink")（1992年4月－2000年3月）
  - [Best Time](../Page/Best_Time.md "wikilink")（2000年4月－2004年3月）
  - [ニュースフロント](../Page/ニュースフロント.md "wikilink")（2004年4月－2005年3月）
  - [きょう発プラス\!](../Page/きょう発プラス!.md "wikilink")（2005年4月－2006年9月）
  - [ピンポン\!](../Page/ピンポン!.md "wikilink")（2006年10月－2009年3月27日）
  - [ひるおび\!](../Page/ひるおび!.md "wikilink")・JNN THE NEWS（2009年3月30日 - ）

<!-- end list -->

  - 周六&周日

<!-- end list -->

  - JNN News（1959年8月－）
  - JNN THE NEWS（2009年4月4日 - ）

### 傍晚

  - 周一～周五

<!-- end list -->

  - [JNN電視夕刊](../Page/JNN電視夕刊.md "wikilink")（1959年8月－1962年9月）
  - [JNNニュースコープ](../Page/JNNニュースコープ.md "wikilink")（1962年10月－1990年3月）
  - [JNN新聞之森](../Page/JNN新聞之森.md "wikilink")（1990年4月－2005年3月）
  - [JNN晚間新聞](../Page/JNN晚間新聞.md "wikilink")（2005年4月－）

<!-- end list -->

  - 周六

<!-- end list -->

  - JNN電視夕刊（1959年8月－1962年9月）
  - JNNニュースコープ（1962年10月－1990年3月）
  - JNN新聞之森（1990年4月－2005年3月）
  - JNN Evening・News（2005年4月－2008年3月）
  - [報道特集NEXT](../Page/報道特集NEXT.md "wikilink")（2008年4月－）

<!-- end list -->

  - 周日

<!-- end list -->

  - JNN電視夕刊（1959年8月－1965年3月）
  - JNNニュースコープ（1965年4月－1990年3月）
  - JNN新聞之森（1990年4月－2005年3月）
  - JNN Evening・News（2005年4月－）

### 20:54～21:00

  - [JNN Flash News](../Page/JNN_Flash_News.md "wikilink")

### 夜間

  - 周一～周五

<!-- end list -->

  - JNN News（1959年8月－1969年3月）
  - [JNN News
    Desk](../Page/JNN_News_Desk.md "wikilink")（1969年4月－1986年9月）
  - [Network
    JNN](../Page/Network_\(新聞節目\).md "wikilink")（1986年10月－1987年9月）
  - [News22PrimeTimeJNN](../Page/News22PrimeTimeJNN.md "wikilink")（1987年10月－1988年9月）
  - [JNN News Desk
    '88](../Page/JNN_News_Desk'88・'89.md "wikilink")（1988年10月－1988年12月）
  - JNN News Desk '89（1989年1月－1989年9月）
  - [筑紫哲也 NEWS23→NEWS23](../Page/NEWS23.md "wikilink")（1989年10月－）

<!-- end list -->

  - 周六

<!-- end list -->

  - JNN News（1959年8月－1978年3月）
  - JNN News Desk（1978年4月－1988年9月）
  - JNN News（1988年10月－1990年9月）
  - [JNN SPORTS and
    NEWS](../Page/JNN_SPORTS_and_NEWS.md "wikilink")（1990年10月－2002年3月）
  - JNN News（2002年4月－）

<!-- end list -->

  - 周日

<!-- end list -->

  - JNN News（1959年8月－1981年3月）
  - JNN News Desk（1981年4月－1988年9月）
  - JNN News（1988年10月－1990年9月）
  - [JNN SPORTS and
    NEWS](../Page/JNN_SPORTS_and_NEWS.md "wikilink")（1990年10月－2002年3月）
  - JNN News（2002年4月－）

## 參考資料

## 外部鏈結

  - [JNN web](http://www.tbs.co.jp/jnn/)
  - [TBS News-i](http://news.tbs.co.jp/)
  - [TBS News-i](http://www.youtube.com/user/tbsnewsi)（YouTube）

[日本新聞網_(TBS)](../Category/日本新聞網_\(TBS\).md "wikilink")
[Category:電視網](../Category/電視網.md "wikilink")
[\*](../Category/東京放送控股.md "wikilink")
[JNN](../Category/日本地面電視.md "wikilink")
[Category:1959年建立](../Category/1959年建立.md "wikilink")
[Category:日本公司列表](../Category/日本公司列表.md "wikilink")

1.  TBS的電台部門於2001年10月1日開始由子公司[TBS廣播與信息公司](../Page/TBS廣播與信息公司.md "wikilink")(2016年4月1日改名為TBS廣播電台)繼承。中部日本放送的電台部門於2013年4月1日由子公司CBC廣播電台继承。
2.  在JNN成立前的1957年4月1日即與TBS組成建立聯播關係，。
3.  1969年12月1日開播時 -
    1975年3月30日期間只參與聯播網的新聞採訪活動，亦會向其他聯播網提供新聞採訪（這種事情僅此一例）。当時的節目編排日本教育電視台（NET電視台〈NET〉、現在的朝日電視台）、全国新聞是JNN新聞網與NET新聞（後來的ANN新聞）並存的狀態，因為排他協定的關係，只能以這樣的形式參與JNN。
4.  當有重大事件時東北放送會加入採訪。
5.  1960年4月1日～1992年9月30日期間有聯播JNN的新聞與一般節目。
6.  現在的公司名稱是東京放送控股。1960年11月29日東京電台（KRT）更名為東京放送。
7.  2001年10月1日廣播電台部門子公司化。子公司化（子公司「TBS廣播與信息公司」繼承電台牌照）後電視頻道與電台頻道的分開。電台的呼號和以前一樣是JOKR、電視頻道的則改為JORX-TV與JORX-DTV。另外，電台主要發射站的維護業務是各委託給子公司化的TBS電視台（在東京放送變為控股公司之前是委託給東京放送）進行的。
8.  電視台的開播時間比TBS要晚一些。但是電台開播的時候卻很早，是日本第一家民營廣播電台（1951年9月1日，日本第一家民營廣播電台開播日）。2013年4月1日電台部分子公司化。子公司化（子公司「CBC電台」繼承）後電視與電台的播出呼號分開。電台和以前一樣是JOAR，電視則改為JOGX-DTV。
9.  JNN成立之前的1959年3月1日就有開始建立合作連接，但只包括一般節目。1960年2月1日 -
    1975年3月30日（腸扭轉解除前一日）因相同廣播區域（近畿広域圏）的ABC朝日放送是JNN成員導致毎日放送無法參加JNN。順便說一下，每日放送當初開播時，是作為準教育電視台開始電視業務的，其他核心局也與其他在大阪的電視台締結了關係，每日放送當時的核心局是NET日本教育電視台（即現在的朝日電視台），東部的報紙資本與西部的電視台之間的關係發生了逆轉。詳情見日文維基。
10. これよりJNN成立前的1958年6月1日締結網絡關係。
11. 1970年4月1日開局 -
    1978年9月30日期間是JNN/朝日電視台（只參加一般節目，新聞網〔ANN〕因JNN協定的關係，沒有參加。）/FNS的交叉網絡、1978年10月1日
    - 1987年9月30日期間是JNN・FNS的交叉網絡。
12. 最終沒有開播的，预订参加JNN。
13. 1958年12月1日 - 1992年9月30日期间RNB 南海放送提供新聞服務。
14. 1959年4月1日 - 1970年3月31日期間RKC 高知放送提供新聞服務。
15. 在JNN成立前的1958年3月1日就有締結網絡關係。
16. 長崎放送在佐賀縣運營的子公司的愛称。
17. 在開播時即與JNN有保持關係。
18. 接受在同一處辦公的ANN成員台[琉球朝日放送的委託辦理其部分業務](../Page/琉球朝日放送.md "wikilink")。但是，營業關係、新聞採訪報道取材除外。