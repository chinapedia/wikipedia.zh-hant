**圣路易斯**（），是[美國](../Page/美國.md "wikilink")[密蘇里州唯一的](../Page/密蘇里州.md "wikilink")[獨立市](../Page/獨立市_\(美国\).md "wikilink")，也是該州第二大城市（第一大城市為[堪薩斯城](../Page/堪薩斯城_\(密蘇里州\).md "wikilink")）。市內人口為308,626人，為美國按人口排名第58大城市。

聖路易斯位于[密苏里河和](../Page/密苏里河.md "wikilink")[密西西比河汇合处](../Page/密西西比河.md "wikilink")，是美国中西部交通枢纽。曾於1904年舉辦過[第三屆夏季奧林匹克運動會](../Page/1904年夏季奧林匹克運動會.md "wikilink")。

聖路易斯市與1764年由法國探險家兼皮草商人皮埃爾·拉克利德（Pierre Laclède）和奧古斯特·舒托（Auguste
Chouteau）建立，並以[中世紀是](../Page/中世紀.md "wikilink")[法王路易九世的封號](../Page/路易九世_\(法兰西\).md "wikilink")**聖路易**命名。早年居民為[法國殖民者](../Page/法國.md "wikilink")，18世紀中葉[七年战争後被](../Page/七年战争.md "wikilink")[西班牙人開始增多](../Page/西班牙.md "wikilink")，由於法國在戰爭中失敗[密西西比河以東的殖民地由](../Page/密西西比河.md "wikilink")[大英帝國佔領](../Page/大英帝國.md "wikilink")。如今聖路易斯市所在的區被稱為[西班牙路易斯安那仍然保留在法國勢力範圍內](../Page/西班牙路易斯安那.md "wikilink")。

## 歷史

聖路易斯早於[大航海時代以前](../Page/大航海時代.md "wikilink")，已經有人聚居，當時主要是些被稱為[築丘人](../Page/築丘人.md "wikilink")（Mound
Builder）的美洲原住民。[歐洲人最早於](../Page/歐洲.md "wikilink")1673年踏足聖路易斯，其時[法國探險家Louis](../Page/法國.md "wikilink")
Jolliet乘船穿越[密西西比河時](../Page/密西西比河.md "wikilink")，正好途經此地。而在1700年代，就有一些歐洲移民來此定居。

至於正式建城則是1764年的事：兩位法國籍皮草商人皮埃爾·拉克利德（Pierre Laclède）和奧古斯特·舒托（Auguste
Chouteau），就在聖路易斯建城，專門與美國中西部原住民進行皮草貿易。同時以[中世紀是](../Page/中世紀.md "wikilink")[法王路易九世的封號](../Page/路易九世_\(法兰西\).md "wikilink")**聖路易**命名這座城市，故該城的末尾s是否發音仍有爭議（相比之下，美國另一座以[波旁王朝法王路易命名的城市](../Page/波旁王朝.md "wikilink")[路易維爾公認s不發音](../Page/路易維爾_\(肯塔基州\).md "wikilink")）。

美國宣佈獨立期間，聖路易斯仍是法國屬土。至1803年[路易斯安那購地](../Page/路易斯安那購地.md "wikilink")，聖路易斯與其他中西部地區一樣，被賣給了[美國](../Page/美國.md "wikilink")。

### 十九世紀

1804年，受美國總統[托馬斯·傑斐遜派遣](../Page/托馬斯·傑斐遜.md "wikilink")，[美國陸軍軍官](../Page/美國陸軍.md "wikilink")[梅里韦瑟·刘易斯和](../Page/梅里韦瑟·刘易斯.md "wikilink")[威廉·克拉克率隊從聖路易斯出發](../Page/威廉·克拉克_\(探险家\).md "wikilink")，[遠征西部地區](../Page/刘易斯与克拉克远征.md "wikilink")。探險隊沿密蘇里河逆流而上向西，並於此年5月抵達太平洋海岸。1806年9月23日探險隊勝利返回聖路易斯，探險隊兩位領導劉易斯和克拉克均居住於聖路易斯。此後聖路易斯成為各種探險家，西進墾荒者和毛皮生意人的集散地。

1808年，聖路易斯居民首次進行公投選舉市政府。1818年，[蒸汽船正式通航](../Page/蒸汽船.md "wikilink")，大宗貨物可直接通航至[新奧爾良並海運至東海岸和歐洲國家](../Page/新奧爾良.md "wikilink")，商業開始蓬勃發展。

1821年[密蘇里州成立](../Page/密蘇里州.md "wikilink")，聖路易斯也於1822年被劃做美國城市之一。這一時期主要支柱產業為[港口](../Page/港口.md "wikilink")、貿易和運輸業。上述行業大量使用[黑奴作為苦力](../Page/黑奴.md "wikilink")。由於城市緊鄰[伊利諾伊州](../Page/伊利諾伊州.md "wikilink")（[自由州](../Page/自由州.md "wikilink")），每年有大量[黑奴逃亡](../Page/黑奴.md "wikilink")。某些逃亡失敗的黑奴（多數為婦女兒童）則被法院起訴。聖路易斯深受自由州影響，每年都有許多正義律師義務為黑人爭取自由。自[美國內戰前](../Page/美國內戰.md "wikilink")，有數百位黑人通過法庭合法的爭取到自由。1840年起，[德國](../Page/德國.md "wikilink")、[愛爾蘭裔移民大量湧入](../Page/愛爾蘭.md "wikilink")，人口從1840年的20,000，升至1850年的77,860；1860年則高達16萬。從18世紀中葉，聖路易斯人口超過新奧爾良，為密西西比河沿岸最大城市。

[St_Louis_Birdseye_Map_1896.jpg](https://zh.wikipedia.org/wiki/File:St_Louis_Birdseye_Map_1896.jpg "fig:St_Louis_Birdseye_Map_1896.jpg")
[St._Louis,_Mo._tornado_May_27,_1896_south_broadway.JPG](https://zh.wikipedia.org/wiki/File:St._Louis,_Mo._tornado_May_27,_1896_south_broadway.JPG "fig:St._Louis,_Mo._tornado_May_27,_1896_south_broadway.JPG")袭击的南百老汇大街\]\]

密蘇里州雖然為[蓄奴州](../Page/蓄奴州.md "wikilink")，在內戰前是否脫離聯邦的問題上舉棋不定，加上聖路易斯身處奴隸州和自由州的邊界，因此內戰前各種政治勢力在聖路易斯激烈衝突。內戰中聖路易斯由忠於聯邦的勢力控制，但不乏極端份子製造騷亂。1861年5月10日，邦聯的支持者發生[騷亂](../Page/聖路易斯大屠殺.md "wikilink")，聯邦軍隊向騷亂開槍造成28名平民死亡，被稱為[聖路易斯大屠殺](../Page/聖路易斯大屠殺.md "wikilink")。內戰中[聯邦海軍對](../Page/美國海軍.md "wikilink")[邦聯進行經濟封鎖](../Page/邦聯.md "wikilink")，造成密西西比河航運中斷，聖路易斯經濟也遭受沈重打擊。期間，聖路易斯亦給聯邦海軍生產[鐵甲艦](../Page/鐵甲艦.md "wikilink")。內戰後聖路易斯經濟很快復蘇，並成為西部開發的窗口。1874年[伊茲橋竣工](../Page/伊茲橋.md "wikilink")，為第一座橫跨密西西比河中下游的公鐵兩用大橋。伊茲橋溝通聖路易斯和[東聖路易斯](../Page/東聖路易斯_\(伊利諾伊州\).md "wikilink")，並取代了[輪渡](../Page/輪渡.md "wikilink")，將兩岸的鐵路網連結成一體。伊茲橋一直使用至今並成為聖路易斯市的重要標誌之一。得益於交通的改善，各種[製造業工廠沿密西西比河兩岸展開](../Page/製造業.md "wikilink")。

1876年8月22日，經公民投票，聖路易斯市正式從[聖路易斯縣中獨立出來](../Page/聖路易斯縣_\(密蘇里州\).md "wikilink")，成為一個[獨立市](../Page/獨立市.md "wikilink")，原縣治遷往[克雷頓](../Page/克雷頓_\(密蘇里州\).md "wikilink")。19世紀後半葉，聖路易斯市成為了工業重地，較著名的產業有輕工業啤酒廠“[安海斯-布希](../Page/安海斯-布希.md "wikilink")”（Anheuser-Busch），動物食品廠“[羅爾斯頓-普瑞納](../Page/羅爾斯頓-普瑞納.md "wikilink")”（Ralston-Purina）；重工業有冶金業“[德洛熱聯合鉛公司](../Page/德洛熱聯合鉛公司.md "wikilink")”（Desloge
Consolidated Lead
Company）。聖路易斯亦是當時交通設備的生產基地，有生產鐵路和電車的企業“[聖路易斯車輛廠](../Page/聖路易斯車輛廠.md "wikilink")”（Saint
Louis Car
Company），多家中小型汽車製造廠，其中較有名的有“[成功汽車製造廠](../Page/成功汽車製造廠.md "wikilink")”（Success
Automobile Manufacturing
Company）。但汽車業不久就被革命性的[密歇根州汽車工業取代](../Page/密歇根州.md "wikilink")。1892年聖路易斯出現了第一幢[摩天大樓](../Page/摩天大樓.md "wikilink")[溫萊特大廈](../Page/溫萊特大廈_\(聖路易斯\).md "wikilink")，設計者為著名摩天大樓設計師[路易斯·沙利文](../Page/路易斯·沙利文.md "wikilink")。

### 二十世紀

1904年，[路易斯安那購地100週年萬國博覽會](../Page/路易斯安那購地100週年萬國博覽會.md "wikilink")、[1904年夏季奥林匹克运动会在聖路易斯召開](../Page/1904年夏季奥林匹克运动会.md "wikilink")，聖路易斯也成為第一個舉辦[奧林匹克運動會的北美洲城市](../Page/奧林匹克運動會.md "wikilink")。世博會和奧運會的會場在[森林公園以及與之毗鄰的](../Page/森林公園_\(聖路易斯\).md "wikilink")[聖路易斯華盛頓大學體育場](../Page/聖路易斯華盛頓大學.md "wikilink")。世博會的許多建築被保留至今作為歷史博物館，藝術博物館，動物園和都市景觀。

雖然美國內戰已經結束逾40年，二十世紀初的聖路易斯依然面臨嚴重的種族衝突問題。住房歧視，就業歧視和社會地位不平等在第一個十年尤為突出。許多地區對不同種族或信仰的購房者或租房客進行限制。20世紀前半葉，隨著工業的進一步發展，大批[非裔美國人從南方州份](../Page/非裔美國人.md "wikilink")[遷居至聖路易斯尋求工作](../Page/大遷徙_\(非裔美國人\).md "wikilink")。[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")[美国全国有色人种协进会遊說政府](../Page/美国全国有色人种协进会.md "wikilink")，在軍工廠內消除了種族歧視。戰後，[地役權種族歧視通過](../Page/地役權.md "wikilink")“雪莉訴克雷默案”由[最高法院裁決違憲](../Page/美國最高法院.md "wikilink")。1964年，民權活動者在[大拱門舉行示威抗議絲綢貿易工會對黑人歧視](../Page/聖路易斯拱門.md "wikilink")。[1964年民權法案批准後](../Page/1964年民權法案.md "wikilink")，絲綢貿易工會被最高法院起訴。

1950年代，聖路易斯公立教育[原則上是種族隔離的](../Page/De_jure.md "wikilink")，並且種族隔離[事實上一直持續到](../Page/De_facto.md "wikilink")1970年代。為此，法院開始強制推行種族融合制度。部分居住在黑人區的學齡兒童被強制送到白人公立學校以推進種族融合（被稱為“bus”，因為這些學童需乘坐校巴前往距家較遠的學校）。不少公立學校亦成立附屬學校和特色學校以提高入學率。

1965年[聖路易斯拱門在密西西比河畔竣工](../Page/聖路易斯拱門.md "wikilink")，成為城市新的地標。

二十世紀前半葉，聖路易斯工業發展至頂峰，吸納大量移民使其人口在1950年達到頂峰856,796。隨後由於1950年代開始的郊區化，以及美國產業結構調整，導致大量工廠倒閉，聖路易斯人口開始銳減。郊區化對聖路易斯市影響相較其他城市尤為嚴重。1976年市縣分離後，聖路易斯市僅剩下市區，大量人口遷居相鄰縣市，造成了稅收銳減，市內亦無剩餘土地用郊區居民區或衛星城。2011年，聖路易斯市人口僅為[大聖路易斯地區的](../Page/大聖路易斯地區.md "wikilink")11%。目前大聖路易斯仍為美國前20大都會區之一，24%的總人口居住在城市內。

聖路易斯的新移民主要由[越南人](../Page/京族.md "wikilink")，拉美裔（墨西哥人為主）和[波士尼亞人為主](../Page/波士尼亞人.md "wikilink")。其中波士尼亞人為[波黑以外最大的一支](../Page/波斯尼亚和黑塞哥维那.md "wikilink")。政府亦有數個[都市復興計劃](../Page/市區更新.md "wikilink")，拆除舊屋以提供條件改善的新屋，但效果不彰。[普魯伊特-伊戈公寓即為一個典型的失敗例子](../Page/普魯伊特-伊戈公寓.md "wikilink")。

## 地理

聖路易斯是[大聖路易斯都會圈的核心](../Page/大聖路易斯都會圈.md "wikilink")。整個都會圈包括了密蘇里州境內的[聖路易斯縣](../Page/聖路易斯縣_\(密蘇里州\).md "wikilink")，[聖查爾斯縣等並包括了位於](../Page/聖查爾斯縣_\(密蘇里州\).md "wikilink")[伊利諾伊州的](../Page/伊利諾伊州.md "wikilink")[都會東區](../Page/都會東區.md "wikilink")，總人口為2,803,707，为按人口統計為美国第十八大城市区。按佔地面積算為美國面積最大的都會圈之一。其密蘇里州部分亦是該州最大的都會圈。

### 城市地理

[St_Louis_Rivers.png](https://zh.wikipedia.org/wiki/File:St_Louis_Rivers.png "fig:St_Louis_Rivers.png")
根據[美國人口調查局統計數據](../Page/美國人口調查局.md "wikilink")，聖路易斯市面積為170平方公里（66平方英里），水域面積為11平方公里，佔11%。[蘭伯特-聖路易斯國際機場亦屬於聖路易斯市領土](../Page/蘭伯特-聖路易斯國際機場.md "wikilink")，但其位於聖路易斯縣，故在地理上屬於[飛地](../Page/飛地.md "wikilink")。城市位於[密蘇里河和](../Page/密蘇里河.md "wikilink")[密西西比河交界處西南的丘陵區域](../Page/密西西比河.md "wikilink")，平均海拔高度高於河面100-200英尺。地形主要為坡度較緩的丘陵，山谷和兩河沖積平原，土壤肥沃適合作物生長。

### 城市气候

圣路易斯气候代表美国中西部温带，属[溫帶大陸性濕潤氣候](../Page/溫帶大陸性濕潤氣候.md "wikilink")，四季分明。冬季寒冷，潮湿，但日照相对充足，日最高气温低于的平均日数为41天，日最低气温低于的平均日数为14天，低于的有4.8天；夏季炎热潮湿，日最高气温超过的日数年均有75天，以上的有14天。\[1\]最冷月（1月）均温，极端最低气温（1884年1月5日）。\[2\]最热月（7月）均温，极端最高气温（1954年7月14日）。\[3\]无霜期平均为212天（4月4日至10月31日）；可测量降雪平均期为12月3日至3月14日。\[4\]年均降水量约，年极端最少降水量为（1953年），最多为（2008年）。\[5\]年均降雪量为；1953–54年的降雪量最少，积累降雪量只有，1911–12年的降雪量最多，积累降雪量为。\[6\]

### 城市天際線

## 人口

[Race_and_ethnicity_2010_St._Louis.png](https://zh.wikipedia.org/wiki/File:Race_and_ethnicity_2010_St._Louis.png "fig:Race_and_ethnicity_2010_St._Louis.png")
聖路易斯的人口在1950年曾達到高峰，有856,796人。此後人口逐年遞減，至2000年，有348,189人；到2010年，則有319,294人。

種族方面，1940年聖路易斯86.6%的人口為白人，黑人佔13.3%，拉丁裔則僅0.2%。至2010年，白人人口比例下滑，僅佔全體人口43.9%，黑人則大幅上升，為49.2%，而拉丁裔則有3.5%。

## 经济

聖路易斯曾是美國重要的工業城市和中部水陸運輸的樞紐。隨後由於美國產業結構調整，導致聖路易斯一帶大量工廠倒閉，製造業的佔比在聖路易斯經濟中不斷下降。現今仍盤踞在聖路易斯的產業，包括美國大型航空製造業公司[麦克唐纳-道格拉斯公司](../Page/麦克唐纳-道格拉斯公司.md "wikilink")，目前為[波音的一部分](../Page/波音.md "wikilink")；以及[艾默生电气公司](../Page/艾默生电气公司.md "wikilink")（Emerson
Electric Company）。

目前聖路易斯最重要的產業為服務業，主要包括公營事業、高等教育、醫療和生物技術等。以聖路易斯為總部的大型企業有：[皮博迪能源](../Page/皮博迪能源.md "wikilink")（Peabody
Energy）、[阿莫林公司](../Page/阿莫林公司.md "wikilink")（Ameren）、[快捷藥方](../Page/快捷藥方.md "wikilink")（Express
Scripts）、[西格瑪-艾爾德里奇公司](../Page/西格瑪-艾爾德里奇公司.md "wikilink")（Sigma-Aldrich）。

根據2013年聖路易斯綜合年度財務報告\[7\]\[8\]，2012年全市首十大僱主為：

  - [聖路易斯華盛頓大學](../Page/聖路易斯華盛頓大學.md "wikilink")：僱用14,705人
  - BJC醫療護理：僱用13,241人
  - [聖路易斯大學](../Page/聖路易斯大學.md "wikilink")：僱用10,096人
  - 聖路易斯市政府：僱用8,098人
  - 美國國防部財政組：僱用6,379人
  - [富國銀行集團](../Page/富國銀行集團.md "wikilink")：僱用5,653人
  - 聖路易斯教育局：僱用4,992人
  - 密蘇里州政府：僱用4,240人
  - AT\&T：僱用4,016人
  - [美國郵政署](../Page/美國郵政署.md "wikilink")：僱用3,973人

## 文化

由於在歷史上，最先在聖路易斯駐腳的是法國人，聖路易斯傳統上受[羅馬天主教影響甚大](../Page/羅馬天主教.md "wikilink")。其中最具代表性建築物，為[聖路易聖殿主教座堂](../Page/聖路易聖殿主教座堂.md "wikilink")。

## 教育

[Slu_dubourg_1888.jpg](https://zh.wikipedia.org/wiki/File:Slu_dubourg_1888.jpg "fig:Slu_dubourg_1888.jpg")行政樓，該校為密西西比河以西最早的高等教育機構之一\]\]

[聖路易斯公立學校系統是聖路易斯市最大的教育機構](../Page/聖路易斯公立學校系統.md "wikilink")，負責管理全市75所公立中小學和附屬學校，擁有約2萬5千名學生。聖路易斯公立學校系統由州政府授權成立，並且受州政府機構聖路易斯教育委員會節制。此外市內亦有教育委員會但並無法律上的權力。至2000年起，聖路易斯市亦開設了數家特許學校（charter
schools）。這些學校由州政府許可成立，多由當地或企業贊助，提供從幼稚園到高中的系統性教育。[天主教圣路易斯总教区開設數家](../Page/天主教圣路易斯总教区.md "wikilink")[教区学校](../Page/教区学校.md "wikilink")，部分能提供高中階段的教育。其他私立高中有[天主教學校](../Page/天主教.md "wikilink")，[信義宗學校和幾所非宗教私立高中](../Page/信義宗.md "wikilink")。

聖路易斯市內亦有兩所大型研究性大學：[聖路易斯華盛頓大學和](../Page/聖路易斯華盛頓大學.md "wikilink")[聖路易斯大學](../Page/聖路易斯大學.md "wikilink")。兩所學校均為多學科綜合性私立大學。聖路易斯華盛頓大學醫學院久負盛名，為[美国新闻与世界报道評出前十名的醫學院](../Page/美国新闻与世界报道.md "wikilink")。聖路易斯大學為天主教背景的私立大學，是聖路易斯，也是整個密西西比河西岸第一所高等教育機構。市內唯一的一所公立大學為[哈里斯-史杜威州立大學](../Page/哈里斯-史杜威州立大學.md "wikilink")，該校曾經為[傳統黑人大學](../Page/傳統黑人大學.md "wikilink")。史蒂芬商業和藝術學院為市內一所提供2年制大專和4年制本科學位的學校。此外，大聖路易斯地區有約數十所公私立學校提供不同程度的高等教育。

## 體育

聖路易斯傳統體育運動隊有[聖路易斯紅雀隊](../Page/聖路易紅雀.md "wikilink")（職業[棒球](../Page/棒球.md "wikilink")）,[聖路易斯藍調隊](../Page/聖路易斯藍調.md "wikilink")（職業冰球）。

| 球隊                                     | 联盟                                            | 运动 | 主场                                                           | 成立年份  | 锦标赛       |
| -------------------------------------- | --------------------------------------------- | -- | ------------------------------------------------------------ | ----- | --------- |
| [聖路易紅雀](../Page/聖路易紅雀.md "wikilink")   | [美国职棒大联盟](../Page/美國職棒大聯盟.md "wikilink")（MLB） | 棒球 | [布希球場](../Page/布希球場.md "wikilink")                           | 1882年 | 11次世界大賽冠军 |
| [圣路易斯藍調](../Page/圣路易斯藍調.md "wikilink") | [國家冰球聯盟](../Page/國家冰球聯盟.md "wikilink")（NHL）   | 冰球 | [Enterprise Center](../Page/Enterprise_Center.md "wikilink") | 1967年 | 0次史丹利盃    |

## 交通

[I-70_in_downtown_St._Louis.jpg](https://zh.wikipedia.org/wiki/File:I-70_in_downtown_St._Louis.jpg "fig:I-70_in_downtown_St._Louis.jpg")南北貫穿聖路易斯市區\]\]
聖路易斯交通發達，主要交通方式有[公路](../Page/公路.md "wikilink")，[鐵路](../Page/鐵路.md "wikilink")，[河運和](../Page/河運.md "wikilink")[空運](../Page/空運.md "wikilink")。聖路易斯市亦有較為發達的公共交通系統，[公共汽車和](../Page/公共汽車.md "wikilink")[輕軌列車服務於聖路易斯市和相鄰縣市](../Page/輕軌.md "wikilink")。

### 公路運輸

聖路易斯共有四條[洲際高速公路通過](../Page/州際公路系統.md "wikilink")，其中[70號州際公路](../Page/70號州際公路.md "wikilink")（I-70）從西北進入城市，[55號州際公路](../Page/55號州際公路.md "wikilink")（I-55）則從西南進入，兩條高速公路分別在[斯坦·穆休老兵纪念大桥和](../Page/斯坦·穆休老兵纪念大桥.md "wikilink")[波普勒街大橋穿過密西西比河向東進入](../Page/波普勒街大橋.md "wikilink")[伊利諾伊州](../Page/伊利諾伊州.md "wikilink")。[44号高速公路](../Page/44号高速公路.md "wikilink")（I-44）和[64号高速公路](../Page/64号高速公路.md "wikilink")（I-64）则从市西侧进入城市并和其他高速公路在市中心汇合。圣路易斯市亦是[圣路易斯-圣保罗大道的起点](../Page/圣路易斯-圣保罗大道.md "wikilink")（Avenue
of the
Saints），该公路包含了一部分州际公路和国道系统，全长563英里，是沟通[明尼苏达州](../Page/明尼苏达州.md "wikilink")[聖保羅和聖路易斯重要的交通要道](../Page/聖保羅_\(明尼蘇達州\).md "wikilink")。城市道路市中心為棋盤狀，西邊為西北，西南兩個走向放射。重要的道路有南北走向有紀念大道（Memorial
Drive，和I-70市區段平行的普通道路），格蘭德大道（Grand Boulevard），傑斐遜大道（Jefferson
Avenue），西南－東北走向的格拉維斯路（Gravois
Road，舊[66號美國國道](../Page/66號美國國道.md "wikilink")），以及西北－東南走向的[馬丁·路德·金大道](../Page/馬丁·路德·金.md "wikilink")（Martin
Luther King, Jr. Drive）。

### 輕軌運輸

[St_Louis_Metrolink_train.jpg](https://zh.wikipedia.org/wiki/File:St_Louis_Metrolink_train.jpg "fig:St_Louis_Metrolink_train.jpg")列車從[聖路易斯聯合車站開出](../Page/聖路易斯聯合車站.md "wikilink")\]\]
[MetroLink_map_(article_version).svg](https://zh.wikipedia.org/wiki/File:MetroLink_map_\(article_version\).svg "fig:MetroLink_map_(article_version).svg")
聖路易斯市擁有美國中西部最大的都市輕軌系統，由[雙州發展署運營](../Page/雙州發展署.md "wikilink")，資金來自聖路易斯市和鄰近縣市的[消費稅提成](../Page/消費稅.md "wikilink")，服務於聖路易斯市和周圍其他縣市以及伊利諾伊州部分地區。雖然名為輕軌（Light
Rail）但線路標準按照高運量[捷運系統設計建造](../Page/捷運.md "wikilink")，全程擁有獨立路權，[復線行駛](../Page/復線鐵路.md "wikilink")。目前共有紅，藍兩條線，在森林公園站會合。樞紐站為[聖路易斯交通中心](../Page/聖路易斯交通中心.md "wikilink")，乘客在該站亦可轉乘長途火車，長途巴士和公共汽車前往市內外各地。

### 机场

[Lambert-terminal1.jpg](https://zh.wikipedia.org/wiki/File:Lambert-terminal1.jpg "fig:Lambert-terminal1.jpg")

[聖路易蘭伯特國際機場位於聖路易斯縣](../Page/聖路易蘭伯特國際機場.md "wikilink")，毗鄰70號高速公路，據聖路易斯市11英里。但歸屬於聖路易斯市管理運營，是美國最大最繁忙航空港之一。2011年平均每天有255架次起降，客貨航班目的地達90個地區（含國際航班），全年共運輸乘客1300萬人次。聖路易斯機場曾是[環球航空的總部所在地](../Page/環球航空.md "wikilink")，現在為[美國航空](../Page/美國航空.md "wikilink")，[美國航空聯運和](../Page/美國航空聯運.md "wikilink")[西南航空的主要樞紐機場](../Page/西南航空.md "wikilink")。機場共有2個航站樓和5個進站大廳（其中一個目前處於關閉狀態）。有通勤巴士連結兩個航站樓，亦有輕軌，出租車和市郊巴士連結市區和周邊市縣。

蘭伯特國際機場亦是重要貨運機場和重要的飛機生產維護中心。[麦克唐纳-道格拉斯公司總部曾設在蘭伯特機場並生產裝配](../Page/麦克唐纳-道格拉斯公司.md "wikilink")[美國空軍使用的](../Page/美國空軍.md "wikilink")[戰鬥機](../Page/戰鬥機.md "wikilink")。麥道被波音收購後，繼續在機場附近的麥道裝配廠生產[F-15鷹式戰鬥機和](../Page/F-15鷹式戰鬥機.md "wikilink")[美國海軍使用的](../Page/美國海軍.md "wikilink")[F/A-18E/F超級大黃蜂式打擊戰鬥機](../Page/F/A-18E/F超級大黃蜂式打擊戰鬥機.md "wikilink")。除蘭伯特機場外，聖路易斯地區還擁有數個貨運和通用航空機場，其中較大的有中美國聖路易斯機場（MidAmerica
St. Louis Airport），“聖路易斯精神號”機場（Spirit of St. Louis
Airport）以及（東）聖路易斯市區機場（St. Louis Downtown
Airport）。

### 铁路運輸

[Terminal_Railroad_Association_of_St._Louis_Freight_Train.jpg](https://zh.wikipedia.org/wiki/File:Terminal_Railroad_Association_of_St._Louis_Freight_Train.jpg "fig:Terminal_Railroad_Association_of_St._Louis_Freight_Train.jpg")的貨運列車正在漢普頓大街附近的鐵路上向東行駛\]\]
聖路易斯曾經是美國重要的鐵路交通樞紐，[聖路易斯聯合車站曾經是美國也是世界上最大](../Page/聖路易斯聯合車站.md "wikilink")，最繁忙的鐵路車站之一。目前鐵路長途客運已經衰退，但鐵路貨運仍然十分繁忙。

[美國國鐵為聖路易斯](../Page/美國國鐵.md "wikilink")[長途鐵路客運唯一運營商](../Page/城際鐵路.md "wikilink")，車站為位於市區的[聖路易斯交通中心](../Page/聖路易斯交通中心.md "wikilink")。始發終到的列車有往返於[芝加哥的](../Page/芝加哥.md "wikilink")[林肯服務號準高速試驗列車](../Page/林肯服務號.md "wikilink")，以及往返[堪薩斯城的](../Page/堪薩斯城_\(密蘇里州\).md "wikilink")[密蘇里河跑手號](../Page/密蘇里河跑手號.md "wikilink")。上述兩班列車每日均開行多對。經停的列車有從芝加哥開往[德克薩斯州](../Page/德克薩斯州.md "wikilink")[聖安東尼奧的](../Page/聖安東尼奧_\(德克薩斯州\).md "wikilink")[德州之鷹號](../Page/德州之鷹號.md "wikilink")。

貨運方面，聖路易斯目前仍為全美第三大[鐵路貨運樞紐](../Page/鐵路貨運.md "wikilink")。密蘇里州出產的大量工農業原料和產品通過鐵路運輸並經過聖路易斯。常見貨運列車裝載物品有[化肥](../Page/化肥.md "wikilink")，碎石，石油，動物產品，食品原料，非金屬礦石，穀物，酒精，菸草製品，汽車和汽車零配件。市內主要鐵路運營商有[聯合太平洋鐵路](../Page/聯合太平洋鐵路.md "wikilink")，[諾福克南方鐵路](../Page/諾福克南方鐵路.md "wikilink")，福斯特·湯生鐵路（舊名“啤酒廠鐵路”），[終端鐵路公司](../Page/終端鐵路公司.md "wikilink")，阿弗頓鐵路，以及[伯靈頓，北方和聖塔菲鐵路](../Page/伯靈頓，北方和聖塔菲鐵路.md "wikilink")（BNSF）。

終端鐵路公司（Terminal Railroad Association of St.
Louis，TRRA）為聖路易斯市負責管理車站，提供調車和裝載業務，以及溝通各鐵路公司的線路。該公司由聖路易斯主要鐵路公司協商成立，目前擁有約30台[柴油機車](../Page/柴油機車.md "wikilink")。聖路易斯各主要工廠自備線亦由終端鐵路公司負責運送。TRAA也負責市內數個鐵路隧道，[鐵路地下化](../Page/鐵路地下化.md "wikilink")，鐵路橋樑（[商業橋和](../Page/商業橋.md "wikilink")[麥克阿瑟大橋](../Page/麥克阿瑟大橋.md "wikilink")）的經營和保養。[美國國鐵和](../Page/美國國鐵.md "wikilink")[聖路易斯輕軌亦會使用TRAA的設施](../Page/聖路易斯輕軌.md "wikilink")。

[MetroBus_at_St_Louis_Science_Center.jpg](https://zh.wikipedia.org/wiki/File:MetroBus_at_St_Louis_Science_Center.jpg "fig:MetroBus_at_St_Louis_Science_Center.jpg")

### 巴士和出租車

聖路易斯的公交車亦有[雙州發展署運營](../Page/雙州發展署.md "wikilink")，目前共有75條巴士線路。除服務聖路易斯市外還延伸至周邊縣市。此外伊利諾伊州營運的曼迪遜縣巴士亦有數條線路延伸至聖路易斯，搭乘改線路可以抵達大聖路易斯地區伊利諾伊州的縣市。長途巴士有[灰狗巴士](../Page/灰狗巴士.md "wikilink")，[美鐵巴士](../Page/美鐵巴士.md "wikilink")（停站[聖路易斯交通中心](../Page/聖路易斯交通中心.md "wikilink")）和[麥格巴士](../Page/麥格巴士_\(北美\).md "wikilink")（停站[聖路易斯聯合車站](../Page/聖路易斯聯合車站.md "wikilink")）。聖路易斯有多家[出租車公司經營招手的士](../Page/出租車.md "wikilink")，資費和線路，地區，所用車輛大小有關。所有出租車受到“城市出租車委員會”管理，統一安裝計價器，可用現金或信用卡付款。

### 內河運輸

[水運曾是聖路易斯最重要的交通運輸方式並且促成城市早期的繁榮](../Page/水運.md "wikilink")。目前已經退居二線，但仍然是貨物運輸的重要補充。聖路易斯河港由“聖路易斯港口管理局”（St.
Louis Port
Authority）負責管理維護，密蘇里河，密西西比河均可通航，但主要的運輸集中在密西西比河。聖路易斯的密西西比河岸全長19.3英里，港務局年吞吐量達3200萬公噸，按噸公里計算是美國第二繁忙的內河港口，也是全美吞吐量第三大的內河港口。全市沿河共設有大小100多個停靠點可用於[駁船裝卸](../Page/駁船.md "wikilink")，並且有16個大型公共裝卸場。此外，港務局亦裝備數艘小艇用於救援任務。

## 姊妹城市

  - [博洛尼亚](../Page/博洛尼亚.md "wikilink")

  - [戈尔韦](../Page/戈尔韦.md "wikilink")

  - [茂物](../Page/茂物.md "wikilink")

  - [乔治敦](../Page/乔治敦_\(圭亚那\).md "wikilink")

  - [里昂](../Page/里昂.md "wikilink")

  - [南京](../Page/南京.md "wikilink")

  - [圣路易斯](../Page/圣路易斯_\(塞内加尔\).md "wikilink")

  - [萨马拉](../Page/萨马拉.md "wikilink")

  - [斯图加特](../Page/斯图加特.md "wikilink")

  - [诹访市](../Page/诹访市.md "wikilink")

  - [什切青](../Page/什切青.md "wikilink")

  - [悉尼](../Page/悉尼.md "wikilink")

  - [武汉](../Page/武汉.md "wikilink")\[9\]\[10\]

## 參考文獻

## 外部链接

  - [City of St. Louis Community Information
    Network](http://stlouis.missouri.org/)

  - [St. Louis Convention & Visitors
    Bureau](http://www.explorestlouis.com/)

  - [St. Louis Regional Chamber and Growth
    Association](https://web.archive.org/web/20120606084615/http://www.stlrcga.org/)

  - [Saint Louis Attractions
    Association](http://www.stlattractions.org/)

  - [Google地图](http://maps.google.com/maps?ll=38.627718,-90.242806&spn=0.11,0.18)

[br:Saint-Louis](../Page/br:Saint-Louis.md "wikilink")

[Category:密苏里州城市](../Category/密苏里州城市.md "wikilink")
[Category:聖路易斯](../Category/聖路易斯.md "wikilink")
[Category:密苏里州行政区划](../Category/密苏里州行政区划.md "wikilink")
[Category:夏季奥林匹克运动会主办城市](../Category/夏季奥林匹克运动会主办城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.

8.

9.  [St. Louis Sister Cities](http://www.slcir.org/sistercities.asp)
    ，St. Louis Center for International Relations，2004年9月

10. [武汉与美国圣路易斯市签署友城关系备忘录](http://www.whfao.gov.cn/data/2006/0701/article_519.htm)
    ，武汉市人民政府外事办公室，，2004年9月