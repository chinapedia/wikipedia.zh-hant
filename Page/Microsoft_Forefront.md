[MicrosoftForefrontClientSecurity-ScreenCapture.png](https://zh.wikipedia.org/wiki/File:MicrosoftForefrontClientSecurity-ScreenCapture.png "fig:MicrosoftForefrontClientSecurity-ScreenCapture.png")
**Microsoft ForeFront**是[微軟的](../Page/微軟.md "wikilink")[Microsoft
Windows用戶端及伺服器端的一系列保安產品名稱](../Page/Microsoft_Windows.md "wikilink")，和[Microsoft
Security
Essentials為姊妹軟件](../Page/Microsoft_Security_Essentials.md "wikilink")。

這一系列產品包括了以下各項產品：

  - Microsoft Forefront Client Security（舊稱*Microsoft Client Protection*）
  - Microsoft Forefront Security for [Exchange
    Server](../Page/Microsoft_Exchange_Server.md "wikilink")（舊稱*Microsoft
    Antigen for Exchange*）
  - Microsoft Forefront Security for
    [SharePoint](../Page/Microsoft_Office_SharePoint_Portal_Server.md "wikilink")（舊稱*Antigen
    for SharePoint*）
  - Microsoft Forefront Security for [Microsoft Office Communications
    Server](../Page/Microsoft_Office_Communications_Server.md "wikilink")（舊稱*Antigen
    for Instant Messaging*）
  - Microsoft [Internet Security and Acceleration (ISA) Server
    2006](../Page/Microsoft_Internet_Security_and_Acceleration_Server.md "wikilink")

## 外部連結

  - [Microsoft
    ForeFront官方網址](https://web.archive.org/web/20061230194830/http://www.microsoft.com/forefront/default.mspx)

<references/>

[Category:Windows Server
System](../Category/Windows_Server_System.md "wikilink")
[Category:微軟伺服器技術](../Category/微軟伺服器技術.md "wikilink")
[Category:微軟軟體](../Category/微軟軟體.md "wikilink")
[Category:安全軟體](../Category/安全軟體.md "wikilink")