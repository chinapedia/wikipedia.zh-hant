**無粒隕石**\[1\]是沒有[球粒的一種石](../Page/球粒隕石.md "wikilink")[隕石](../Page/隕石.md "wikilink")
\[2\]\[3\]。它包含的成分與地球上的[玄武岩或](../Page/玄武岩.md "wikilink")[火成岩相似](../Page/火成岩.md "wikilink")，並且曾在流星體母體內或本身經過不同程度的熔化和再結晶的[地質分異作用](../Page/行星分異.md "wikilink")\[4\]\[5\]。因此，無粒隕石有不同的紋理和火成岩過程的礦物學特徵
\[6\]。但非專業人士很難分辨無粒隕石與地球岩石，使它們被發現的機會大為減少。

無粒隕石佔所有[隕石約](../Page/隕石.md "wikilink")8%，大部分（佔2/3）相信為源自[灶神星的](../Page/灶神星.md "wikilink")[HED隕石](../Page/HED隕石.md "wikilink")，其它種類的無粒隕石包括[火星隕石](../Page/火星隕石.md "wikilink")、[月球隕石](../Page/月球隕石.md "wikilink")、以及幾種相信源自[灶神星之外](../Page/灶神星.md "wikilink")，其它未被辨認出的[小行星隕石](../Page/小行星.md "wikilink")。無粒隕石按其[鐵](../Page/鐵.md "wikilink")/[錳比例和](../Page/錳.md "wikilink")<sup>17</sup>[氧](../Page/氧.md "wikilink")/<sup>18</sup>[氧](../Page/氧.md "wikilink")[同位素的比例來區分](../Page/同位素.md "wikilink")。這兩個比例仿如指紋，每個母天體都是不同的
\[7\]。

## 分類

無粒隕石可分為以下幾種\[8\]：

  - [原始無粒隕石](../Page/原始無粒隕石.md "wikilink")
  - [小行星的無粒隕石](../Page/小行星的無粒隕石.md "wikilink")
  - [月球隕石](../Page/月球隕石.md "wikilink")
  - [火星隕石](../Page/火星隕石.md "wikilink")

### 原始無粒隕石

原始無粒隕石也稱為PAC群，所以這樣稱呼，是因為它們的化學成分是*原始*的，在意義上感覺它是相似的[隕石](../Page/隕石.md "wikilink")，但其紋理是火成岩，是熔化過程的指標。屬於這群的有\[9\]：

  - [阿卡普爾科群](../Page/阿卡普爾科群.md "wikilink")：以1976年墜落在墨西哥[阿卡普爾科的隕石命名](../Page/阿卡普爾科.md "wikilink")。
  - [洛德蘭群](../Page/洛德蘭群.md "wikilink")：以1868年墜落在[巴基斯坦洛德蘭的隕石命名](../Page/巴基斯坦.md "wikilink")。
  - [文諾納群](../Page/文諾納隕石.md "wikilink")：以美國亞利桑那州[文諾納發現的隕石命名](../Page/文諾納.md "wikilink")。

### 小行星的無粒隕石

小行星的無球粒隕石也稱為進化的無粒隕石，之所以這樣稱呼是因為它們的[母體是源自不同的小行星](../Page/母體.md "wikilink")，這意味著它們的礦物和化學成分經過熔化和再結晶的過程。它們可以分成幾個群\[10\]：

  - [HED隕石](../Page/HED隕石.md "wikilink")：它們最有可能源自小行星的[灶神星](../Page/灶神星.md "wikilink")，因為它們的反射光譜非常相似\[11\]。它們依據英文字首的不同分為三組：
      - [古銅鈣無粒隕石](../Page/古銅鈣無粒隕石.md "wikilink")(Howardites)
      - [鈣長輝長無粒隕石](../Page/鈣長輝長無粒隕石.md "wikilink")(Eucrites)
      - [古銅無球隕石](../Page/古銅無球隕石.md "wikilink")(Diogenites)
  - [鈦輝無粒隕石](../Page/鈦輝無粒隕石.md "wikilink")(Angrites)
  - [頑火無粒隕石](../Page/頑火無粒隕石.md "wikilink")(Aubrites)
  - [橄輝無球粒隕石](../Page/橄輝無球粒隕石.md "wikilink")（Ureilite，已在蘇聯的發現地Novy
    Ureii命名的隕石)
  - [布莱奇那群](../Page/布莱奇那群.md "wikilink")：以[布萊奇那隕石命名](../Page/布萊奇那隕石.md "wikilink")

### 月球隕石

[月球隕石也稱為Lunanites](../Page/月球隕石.md "wikilink")，是源自於[月球的隕石](../Page/月球.md "wikilink")。

### 火星隕石

[火星隕石](../Page/火星隕石.md "wikilink")\[12\]是源自於[火星的隕石](../Page/火星.md "wikilink")。它們被分成4個子群：

  - [輝玻無粒隕石](../Page/火星隕石#輝玻無粒隕石.md "wikilink")(Shergottites)
  - [輝橄無粒隕石](../Page/火星隕石#輝橄無粒隕石.md "wikilink")(Nakhlites)
  - [純橄無粒隕石](../Page/火星隕石#純橄無粒隕石.md "wikilink")(Chassignites)
  - [直輝石岩隕石](../Page/Allan_Hills_84001.md "wikilink") (ALH 84001)
  - [表岩屑](../Page/表岩屑.md "wikilink")/土壤樣本（[NWA
    7034](../Page/NWA_7034.md "wikilink")）

## 相關條目

  - [隕石學辭彙](../Page/隕石學辭彙.md "wikilink")

## 參考資料

## 外部連結

  - [Achondrite
    Images](http://www.meteorites.com.au/collection/achondrites.html)
    from Meteorites Australia
  - [www.meteorite.fr](http://www.meteorite.fr/en/classification/stonymain.htm#ACHON)
  - [英國自然歷史博物館的隕石目錄](http://internt.nhm.ac.uk/jdsml/research-curation/projects/metcat/index.dsml)
  - [www.planetbrey.com](http://www.planetbrey.com/PBMeteoriteInformation.htm)
  - [中文隕石分類網頁](http://astro.com.tw/new_page_65.htm)

[A](../Category/行星科學.md "wikilink") [A](../Category/隕石類型.md "wikilink")
[\*](../Category/無粒隕石.md "wikilink")

1.  Etymology: from the prefix a- ([privative
    a](../Page/privative_a.md "wikilink")) and the word
    [chondrite](../Page/chondrite.md "wikilink").

2.  [Recommended classifications:
    Eucrite-pmict](http://www.lpi.usra.edu/meteor/metbullclass.php?sea=Eucrite-pmict)

3.  [Achondrite](http://www.britannica.com/EBchecked/topic/3630/achondrite),
    Encyclopedia Britannica

4.

5.

6.

7.

8.  O. Richard Norton. The Cambridge encyclopedia of meteorites. UK,
    Cambridge University Press, 2002. ISBN 978-0-521-62143-4.

9.
10.
11.

12.