**富國島**（），又名**玉島**
（），[柬埔寨稱爲](../Page/柬埔寨.md "wikilink")**国多岛**（），是[越南實際控制的最大島嶼](../Page/越南.md "wikilink")，目前行政區劃上屬於越南[堅江省](../Page/堅江省.md "wikilink")[富國島縣管轄](../Page/富國島縣.md "wikilink")。

越南與柬埔寨就富國島主權歸屬問題有糾紛。1982年，[越南入侵柬埔寨之后](../Page/越柬战争.md "wikilink")，[柬埔寨人民共和国曾表面上与越南达成解决争端的协议](../Page/柬埔寨人民共和国.md "wikilink")，但后来并未正式签署。柬埔寨至今仍宣称对富国岛的拥有主权。

## 地理

富國島位於[暹羅灣](../Page/暹羅灣.md "wikilink")，与[柬埔寨](../Page/柬埔寨.md "wikilink")[贡布的西海岸隔海相望](../Page/贡布_\(城市\).md "wikilink")，距离越南[河仙市的西海岸](../Page/河仙市.md "wikilink")40公里。该岛在地图上形状像个倒三角形，其南北两端长约50公里，东西两端最长处长约25公里。自该岛距越南[迪石市约有](../Page/迪石市.md "wikilink")115公里，距离[泰国的](../Page/泰国.md "wikilink")[林查班约有](../Page/林查班.md "wikilink")540公里。

富國島在越南行政区划上隸屬坚江省富国岛县，该县辖境由22個不同大小島嶼组成，其中富国岛面积最大。越南富国岛县的县人民委员会所在地位于富国岛北端的。

## 经济

富國島天然資源豐富，有優質的海灘，因此有很高的觀光潛力。该岛盛产[珍珠](../Page/珍珠.md "wikilink")，又被称为“珍珠岛”。该岛也以出产高品质的[鱼露著称](../Page/鱼露.md "wikilink")。

富國島以2种特产闻名于世：[鱼露和](../Page/鱼露.md "wikilink")[胡椒粉](../Page/胡椒粉.md "wikilink")。富國島的鱼露用一种富含蛋白质的小鱼[白飯魚](../Page/七星飛刀.md "wikilink")（，即七星飛刀）制作，特别美味。该岛有数以百计的鱼露工厂，每年的产量达到600万升；还有无数的胡椒种植园，总面积达到5平方公里。富国岛附近海域富含[鯷科的鱼类](../Page/鯷科.md "wikilink")。

目前旅游业是富国岛主要的经济来源。岛上建有[富国机场](../Page/富国机场.md "wikilink")，该机场有飞往[胡志明市](../Page/胡志明市.md "wikilink")[新山一国际机场以及迪石市](../Page/新山一国际机场.md "wikilink")[迪石机场的航班](../Page/迪石机场.md "wikilink")。2012年12月2日，富国机场被关闭，取而代之的是[富国国际机场](../Page/富国国际机场.md "wikilink")。富国岛也有驶往迪石市和河仙镇的[水翼船](../Page/水翼船.md "wikilink")。

[湄公航空的总部位于富国岛县的](../Page/湄公航空.md "wikilink")。

2006年，包括富国岛在内的被[联合国教科文组织认定为世界](../Page/联合国教科文组织.md "wikilink")[生物圈保护区](../Page/生物圈保护区.md "wikilink")。

2003年，越南計劃把富国岛建成行政特区。

2015年，越南計劃把富国岛模範[上海自由貿易區籌建](../Page/上海自由貿易區.md "wikilink")[自由貿易區與領海經濟](../Page/自由貿易區.md "wikilink")[工業](../Page/工業.md "wikilink")[特區](../Page/特區.md "wikilink")，完成之後，富國島將成為東南亞地區的主要經濟戰略之一，

## 历史

[Ở_khu_vực_chợ_Dương_Đông.jpg](https://zh.wikipedia.org/wiki/File:Ở_khu_vực_chợ_Dương_Đông.jpg "fig:Ở_khu_vực_chợ_Dương_Đông.jpg")
1671年，華人[鄚玖因不願臣服於](../Page/鄚玖.md "wikilink")[清朝](../Page/清朝.md "wikilink")，率部眾投奔[柬埔寨](../Page/柬埔寨.md "wikilink")。柬王[巴龙列谢八世將鄚玖安置在今日的河仙市一帶](../Page/巴龙列谢八世.md "wikilink")。鄚玖派遣四百名福建人前去開發富國島。又派人前去開拓[隴棋](../Page/隴棋.md "wikilink")（[柬埔寨白馬](../Page/柬埔寨.md "wikilink")）、[芹渤](../Page/芹渤.md "wikilink")（柬埔寨[嗊吥一帶](../Page/嗊吥.md "wikilink")）、[淎㵅](../Page/淎㵅.md "wikilink")（柬埔寨[雲壤港](../Page/雲壤港.md "wikilink")）、[瀝架](../Page/瀝架.md "wikilink")（越南[堅江省](../Page/堅江省.md "wikilink")[迪石市](../Page/迪石市.md "wikilink")）、[哥毛](../Page/哥毛.md "wikilink")（越南[金甌市](../Page/金甌市.md "wikilink")）等地，成立「七村社」，這是富國島第一次在越南史料中出現。

1708年，鄚玖棄柬埔寨，向[廣南阮主](../Page/阮主.md "wikilink")（舊阮）臣服，使得富國島成為越南阮主的領地。河仙鎮鄚氏在舊阮復辟中扮演著重要角色，富國島和[土珠島也成為舊阮早期流亡政權的重要根據地](../Page/土珠島.md "wikilink")。據《[越南史略](../Page/越南史略.md "wikilink")》記載，[阮福映](../Page/阮福映.md "wikilink")（後為[阮朝](../Page/阮朝.md "wikilink")[嘉隆帝](../Page/嘉隆帝.md "wikilink")）於1782年被[西山朝擊敗之時](../Page/西山朝.md "wikilink")，曾數度流亡至此。

雖然富國島被越南佔領，但柬埔寨歷代國王都認為是柬埔寨領土。1856年，柬埔寨國王[昂東令](../Page/哈里勒·列密·埃萨瓦拉提巴迭.md "wikilink")[敏體尼通過](../Page/敏體尼.md "wikilink")[暹羅向法國轉交國書](../Page/暹羅.md "wikilink")，\[1\]表示願意富國島割讓給法國，希望建立軍事同盟，但法方未做任何回覆。\[2\]法國、西班牙聯軍[遠征交趾支那的時候](../Page/南圻遠征.md "wikilink")，昂東向法皇拿破崙三世遞交國書，重申對[下交趾支那的領土主張](../Page/下柬埔寨.md "wikilink")。

柬埔寨脫離法國獨立之後，對於富國島等島嶼的主權爭議出現了。早在1939年，[法屬印度支那總督](../Page/法屬印度支那.md "wikilink")[朱尔斯·布雷维耶劃分了布雷维耶线](../Page/朱尔斯·布雷维耶.md "wikilink")，規定了該線以北歸[柬埔寨保護國管轄](../Page/柬埔寨保護國.md "wikilink")，以南歸[法属交趾支那殖民政府管轄](../Page/法属交趾支那.md "wikilink")。雖然富國島位於柬埔寨保護國的管轄區域內，但該柬埔寨保護國沒有任何管轄該島的實權，事實上富國島是由交趾支那殖民政府管理。越南獨立之後富國島屬於越南管轄範圍之內。

1949年，[中國共產黨得到](../Page/中國共產黨.md "wikilink")[中國大陸的执政权](../Page/中國大陸.md "wikilink")，時任[中華民國陸軍的](../Page/中華民國陸軍.md "wikilink")[黃杰領](../Page/黃杰_\(將軍\).md "wikilink")3.3萬軍隊，從[湖南省撤退到富國島](../Page/湖南省.md "wikilink")（其部隊後稱為[富台部隊](../Page/富台部隊.md "wikilink")）駐守，人稱「海上蘇武」；直到1953年6月此部隊方撤至台灣，並整編為[海軍陸戰隊](../Page/中華民國海軍陸戰隊.md "wikilink")\[3\]。1955年11月，一座人工小島於[臺灣](../Page/臺灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[澄清湖建立](../Page/澄清湖.md "wikilink")，並被命名為[富國](../Page/富國島_\(台灣\).md "wikilink")，以紀念國軍在[中南半島的孤軍](../Page/中南半島.md "wikilink")。此外，著名的[台湾豫剧团亦与此岛有关键因素](../Page/台湾豫剧团.md "wikilink")。

[越南戰爭期間](../Page/越南戰爭.md "wikilink")，富國島是當時[越南共和國](../Page/越南共和國.md "wikilink")（即南越）政府最大的[集中營](../Page/集中營.md "wikilink")「[富國監獄](../Page/富國監獄.md "wikilink")」的所在地\[4\]。在1973年，集中營囚禁了約4萬人。

1975年5月1日，[红色高棉的一個](../Page/红色高棉.md "wikilink")[分隊入侵占領富国岛](../Page/班.md "wikilink")，但很快為越南重新佔有。事件其後導致一連串的入侵與反入侵行動，被認為是1979年[越柬戰爭的導火線](../Page/越柬戰爭.md "wikilink")。

## 气候

富國島屬於副赤道季风气候。每年只有2个季节：雨季和旱季。年平均降雨量为2,879毫米，年平均气温为27摄氏度。终年适于旅游，但是最佳季节是旱季，因为天气晴朗。

## 參考資料

## 相关条目

  - [富國狗](../Page/富國狗.md "wikilink")
  - [富台部隊](../Page/富台部隊.md "wikilink")
  - [土珠岛](../Page/土珠岛.md "wikilink")
  - [柬越戰爭](../Page/柬越戰爭.md "wikilink")

## 外部連結

  - [富國島在線網站](https://web.archive.org/web/20151113191814/http://www.daongoc.com/)
  - [富國島虛擬之旅](http://www.virtourist.com/asia/vietnam/phu-quoc)

[Category:有爭議的島嶼](../Category/有爭議的島嶼.md "wikilink")
[Category:柬埔寨島嶼](../Category/柬埔寨島嶼.md "wikilink")
[Category:越南島嶼](../Category/越南島嶼.md "wikilink")
[Category:泰國灣](../Category/泰國灣.md "wikilink")

1.  "Le Second Empire en Indo-Chine (Siam-Cambodge-Annam): l'ouverture
    de Siam au commerce et la convention du Cambodge”, Charles Meyniard,
    1891, Bibliothèque générale de géographie
2.  "La Politique coloniale de la France au début du second Empire
    (Indo-Chine, 1852-1858)", Henri Cordier, 1911, Ed. E.J. Brill
3.
4.  Ngo Cong Duc, deputy of the Vinh Binh province, quoted in "Le régime
    de Nguyen Van Thieu à travers l'épreuve", Etude Vietnamienne, 1974,
    pp. 99–131