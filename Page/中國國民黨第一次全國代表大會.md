[1st_National_Congress_of_Kuomintang_of_China.jpg](https://zh.wikipedia.org/wiki/File:1st_National_Congress_of_Kuomintang_of_China.jpg "fig:1st_National_Congress_of_Kuomintang_of_China.jpg")\]\]
**中國國民黨第一次全國代表大會**於1924年1月20日至30日期間，在當時[广州的](../Page/广州.md "wikilink")[國立廣東高等師範學校禮堂舉行](../Page/國立廣東高等師範學校.md "wikilink")。大會出席代表共196人，[孫中山任大會主席](../Page/孫中山.md "wikilink")，[胡漢民](../Page/胡漢民.md "wikilink")、[汪精衛](../Page/汪精衛.md "wikilink")、[林森](../Page/林森.md "wikilink")、[李大釗及](../Page/李大釗.md "wikilink")[謝持任主席團成員](../Page/謝持.md "wikilink")。苏联顾问[鲍罗廷也出席此次会议](../Page/鲍罗廷.md "wikilink")。

## 背景

第二次[護法運動後](../Page/護法運動.md "wikilink")，孫中山開始考慮與共產黨合作的可能。1923年1月，孫中山與[蘇聯政府全權代表](../Page/蘇聯.md "wikilink")[越飛在](../Page/越飛.md "wikilink")[上海會面](../Page/上海.md "wikilink")，正式討論與共產黨合作。孫、越會面曾後發表《[孫文越飛聯合宣言](../Page/孙文越飞宣言.md "wikilink")》。

同年年初，[陳炯明部被拥护孙中山的](../Page/陳炯明.md "wikilink")[滇](../Page/滇系.md "wikilink")、[桂](../Page/桂系.md "wikilink")、和倒戈[粤军联合组成的西路讨贼军击败](../Page/粤系.md "wikilink")，退守[东江](../Page/东江.md "wikilink")。孫中山得以在3月回廣州。12月29日，孫中山落實接受[列寧和](../Page/列寧.md "wikilink")[共產國際的協助重建](../Page/共產國際.md "wikilink")[大元帥府](../Page/大元帥府.md "wikilink")，共產國際派出[鮑羅廷到廣州為孫中山顧問](../Page/鮑羅廷.md "wikilink")，以[蘇共為模式重組](../Page/蘇共.md "wikilink")[中國國民黨](../Page/中國國民黨.md "wikilink")。1924年1月，孫中山在中國國民黨第一次全國代表大會上宣佈實行[聯俄容共政策](../Page/聯俄容共.md "wikilink")，並發表[《中國國民黨第一次全國代表大會宣言》](../Page/s:中國國民黨第一次全國代表大會宣言.md "wikilink")。在蘇聯援助下，於3月組建[黃埔軍校](../Page/黃埔軍校.md "wikilink")，並以[蔣中正為校長](../Page/蔣中正.md "wikilink")。

## 會議

[The-Principle-of-Minsheng_notice_in_Guangzhou_1924.jpg](https://zh.wikipedia.org/wiki/File:The-Principle-of-Minsheng_notice_in_Guangzhou_1924.jpg "fig:The-Principle-of-Minsheng_notice_in_Guangzhou_1924.jpg")
本次大會選出中央執行委員24人、以總理為主席、候補中央執行委員17人；中央監察委員5人、候補中央監察委員5人。中央執行委員會為全國代表大會閉會期間之最高權力機關。

重要決議：

1.  通過「組織國民政府之必要案」。
2.  制定「中國國民黨總章」、「中國國民黨政綱」。
3.  通過「紀律問題」及「海關問題」案。
4.  通過第1次全國代表大會宣言。

## 组织机构

中央执行委员（共24人）：胡汉民、汪精卫、张静江、廖仲恺、李烈钧、居正、戴季陶、林森、柏文蔚、丁惟汾、石瑛、邹鲁、谭延闿、覃振、谭平山、石青阳、熊克武、李大钊、恩克巴图、王法勤、于右任、杨希闵、叶楚伧、于树德

中央候补执行委员（共17人）：邵元冲、邓家彦、沈定一、茅祖权、韩麟符、李宗黄、白云梯、张知本、彭素民、毛泽东、张国焘、傅汝霖、于方舟、张苇村、瞿秋白、张秋白

中央监察委员（共5人）：邓泽如、吴稚晖、李石曾、张继、谢持

中央候补监察委员（共5人）：蔡元培、许崇智、刘震寰、樊钟秀、杨庶堪

1月31日，孙中山主持第一次会议，选出中央领导机构。2月4日，中央执行委员会决定辖下各部部长任命：

  - 总理：孙 文
  - 中央执行委员会常委：廖仲恺、戴季陶、谭平山（共产党员）
  - 组织部长[谭平山](../Page/谭平山.md "wikilink")；秘书 杨匏安（共产党员）
  - 宣传部长[戴季陶](../Page/戴季陶.md "wikilink")（汪兆铭继任）；秘书 刘芦隐
  - 工人部长[廖仲恺](../Page/廖仲恺.md "wikilink")；秘书 冯菊坡（共产党员）
  - 农民部长[林祖涵](../Page/林祖涵.md "wikilink")（共产党员）（黄居素、廖仲恺继任）；秘书 彭湃（共产党员）
  - 军事部长[许崇智](../Page/许崇智.md "wikilink")
  - 青年部长[邹鲁](../Page/邹鲁.md "wikilink")；秘书 孙甄陶。

随后，中央党部会议又推定：

  - 妇女部长[曾醒](../Page/曾醒.md "wikilink")（何香凝继任）；秘书 唐允恭
  - 海外部长[林森](../Page/林森.md "wikilink")

## 參見

  - [中國國民黨第一次全國代表大會舊址](../Page/中國國民黨第一次全國代表大會舊址.md "wikilink")

## 外部連結

  - [歷屆全代會](https://web.archive.org/web/20140806053554/http://www.kmt.org.tw/page.aspx?id=146)
    - 中國國民黨

[Category:1924年中国](../Category/1924年中国.md "wikilink")
[Category:中华民国大陆时期中国国民党会议](../Category/中华民国大陆时期中国国民党会议.md "wikilink")
[Category:中國國民黨全國代表大會](../Category/中國國民黨全國代表大會.md "wikilink")
[Category:广州政治会议](../Category/广州政治会议.md "wikilink")
[Category:1924年1月](../Category/1924年1月.md "wikilink")