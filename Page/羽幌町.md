**羽幌町**（）是[日本](../Page/日本.md "wikilink")[北海道北部西海岸](../Page/北海道.md "wikilink")[留萌振興局中部的一個半農半商的町](../Page/留萌振興局.md "wikilink")。[暑寒別天賣燒尻國定公園的](../Page/暑寒別天賣燒尻國定公園.md "wikilink")[天賣島和](../Page/天賣島.md "wikilink")[燒尻島正是屬于羽幌町](../Page/燒尻島.md "wikilink")，每年夏季有很多旅客前來。

名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「hapur」（柔軟）\[1\]或「ha-poro-pet」（有廣大流域的河川）\[2\]。

## 歷史

地區的開發較鄰近的的苫前町、初山別村晚，直到羽幌煤礦被發現後，才開始有顯著的開發，但在煤礦停止開採之後，人口開始減少。

  - 1885年：開始有和人前來居住。\[3\]
  - 1887年：[石川縣的齊藤知一等約](../Page/石川縣.md "wikilink")20人移民至此，使得漁業開始興盛。
  - 1894年2月3日：從苫前村分村，設置羽幌村。\[4\]
  - 1897年：從苫前村（現在的[苫前町](../Page/苫前町.md "wikilink")）戶長役場分割獨立設置羽幌村戶長役場。
  - 1900年6月7日：成立[初山別村](../Page/初山別村.md "wikilink")。
  - 1901年：初山別村戶長役場分割獨立設置。
  - 1902年4月1日：成為北海道二級村。
  - 1906年4月1日：[燒尻村和](../Page/燒尻村.md "wikilink")[天賣村成立為北海道二級村](../Page/天賣村.md "wikilink")。
  - 1909年4月1日：成為北海道一級村。
  - 1921年7月1日：改制為羽幌町。
  - 1955年4月1日：併入天賣村。
  - 1959年4月1日：併入燒尻村。

## 產業

主要産業是漁業、農業。

## 交通

### 機場

  - [旭川機場](../Page/旭川機場.md "wikilink")（位於[旭川市](../Page/旭川市.md "wikilink")）

### 鐵路

  - 羽幌線（已廢線）
  - 羽幌煤礦鐵道上羽幌線（已廢線）
  - 築別森林鐵道（已廢線）

### 道路

<table style="width:70%;">
<colgroup>
<col style="width: 35%" />
<col style="width: 35%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道232號</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道255號燒尻島線</li>
<li>北海道道356號築別煤礦築別停車場線</li>
<li>北海道道437號羽幌原野古丹別停車場線</li>
<li>北海道道547號羽幌港線</li>
<li>北海道道548號天賣島線</li>
<li>北海道道612號築別天鹽有明停車場線</li>
<li>北海道道741號上遠別霧立線</li>
<li>北海道道747號上羽幌羽幌停車場線</li>
<li>北海道道762號平羽幌線</li>
<li>北道道道866號東濱燒尻港線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

[Haboro_rose-garden.jpg](https://zh.wikipedia.org/wiki/File:Haboro_rose-garden.jpg "fig:Haboro_rose-garden.jpg")\]\]
[Hokkaido_Yagishiri_Isle.jpg](https://zh.wikipedia.org/wiki/File:Hokkaido_Yagishiri_Isle.jpg "fig:Hokkaido_Yagishiri_Isle.jpg")
[Hokkaido_Yagishiri_Ichii.jpg](https://zh.wikipedia.org/wiki/File:Hokkaido_Yagishiri_Ichii.jpg "fig:Hokkaido_Yagishiri_Ichii.jpg")）\]\]

### 觀光

  - [暑寒別天賣燒尻國定公園](../Page/暑寒別天賣燒尻國定公園.md "wikilink")
  - 羽幌日落沙灘
  - 羽幌溫泉
  - 電視劇\[幸福黃手帕\]的場景

### 文化財

  - 燒尻的自然林（國指定天然記念物）
  - 天賣島海鳥棲息地（國指定天然記念物）
  - 舊小納家住宅（北海道指定有形文化財）

## 教育

### 高等學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="http://www.haboro.hokkaido-c.ed.jp/">道立北海道羽幌高等學校</a></li>
<li>町立北海道天賣高等學校</li>
</ul></td>
<td><ul>
<li>羽幌太陽高等學校（於19749年廢校）</li>
<li>燒尻高等學校（於1977年廢校）</li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>羽幌町立羽幌中學校</li>
<li><a href="https://web.archive.org/web/20070928012940/http://x.iwa.hokkyodai.ac.jp/~teuri/">羽幌町立天賣中學校</a></li>
</ul></td>
<td><ul>
<li>羽幌町立燒尻中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小中學校

  - [羽幌町立天賣小中學校](https://web.archive.org/web/20070901100650/http://www.geocities.jp/teuri_es/)

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>羽幌町立羽幌小學校</li>
</ul></td>
<td><ul>
<li>羽幌町立燒尻小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市、友好都市

### 日本

  - [平村](../Page/平村.md "wikilink")（[富山縣](../Page/富山縣.md "wikilink")[東礪波郡](../Page/東礪波郡.md "wikilink")）：於1979年9月11日締結友好都市，已於2004年11月1日合併為[南礪市](../Page/南礪市.md "wikilink")。
  - [內灘町](../Page/內灘町.md "wikilink")（[石川縣](../Page/石川縣.md "wikilink")[河北郡](../Page/河北郡.md "wikilink")）：於1980年10月1日締結姉妹都市。

## 本地出身的名人

  - [松樹路人](../Page/松樹路人.md "wikilink")：[西洋畫家](../Page/西洋畫家.md "wikilink")
  - [蓑谷雅彦](../Page/蓑谷雅彦.md "wikilink")：[歌手](../Page/歌手.md "wikilink")
  - [林朗](../Page/林朗.md "wikilink")：前[劍道世界冠軍](../Page/劍道.md "wikilink")
  - [平尾泰](../Page/平尾泰.md "wikilink")：前劍道日本第二
  - [吉田亘](../Page/吉田亘.md "wikilink")：[指揮家](../Page/指揮家.md "wikilink")
  - [太田慶文](../Page/太田慶文.md "wikilink")：插圖畫家
  - [寒川光太郎](../Page/寒川光太郎.md "wikilink")：[作家](../Page/作家.md "wikilink")

## 相關條目

  - [天賣島](../Page/天賣島.md "wikilink")
  - [燒尻島](../Page/燒尻島.md "wikilink")

## 參考資料

## 外部連結

  - [羽幌町觀光協會](http://www.haboro.tv/)

  - [北海道天賣島官方網頁](http://www.teuri.jp/)

  - [北海道燒尻島官方網頁](https://web.archive.org/web/20070704072249/http://www.linkclub.or.jp/~willow/)

  - [北海道海鳥中心](https://web.archive.org/web/20070709145246/http://www.seabird.go.jp/)

  - [羽幌煤礦遺跡](http://www.asahi-net.or.jp/~re4m-idgc/HABOROTANKOU.htm)

  - [羽幌日落廣場飯店](http://sunset-plaza.com/)

  - [北留萌漁會](https://web.archive.org/web/20070612202416/http://www.gyokyo.net/syoukai/index.html)

  - [羽幌警察署](https://web.archive.org/web/20070810010655/http://www.haboro-syo.police.pref.hokkaido.jp/index.html)

  - [羽幌郵局](https://web.archive.org/web/20040815044728/http://www2.wagamachi-guide.com/japanpost/search/syousai.asp?ID=893)

  - [羽幌青年會議所](http://www8.plala.or.jp/haboro/jc/)

  - [羽幌鄉村俱樂部](http://www.ororon.com/)

<!-- end list -->

1.

2.

3.
4.