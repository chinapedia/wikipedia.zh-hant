**天竺葵属**（[学名](../Page/学名.md "wikilink")：*[Pelargonium](../Page/:en:Pelargonium.md "wikilink")*），别名石腊红、入腊红、日烂红、洋葵，屬牻牛兒苗科，是[开花植物的一个](../Page/开花植物.md "wikilink")[属](../Page/属.md "wikilink")，包括约230[种](../Page/种.md "wikilink")[多年生](../Page/多年生.md "wikilink")[肉质](../Page/肉质.md "wikilink")、[亚灌木或](../Page/亚灌木.md "wikilink")[灌木](../Page/灌木.md "wikilink")[植物](../Page/植物.md "wikilink")。天竺葵原产非洲南部，世界各地普遍栽培。

天竺葵幼株为肉质草本，老株半木质化；其叶对生，叶片为圆形，肾形或扇形；花色有白、紅、紫、粉紅、橙等多种，为伞形花序，四季都能开花，以春季最盛。天竺葵是一種帶有香味、長年生的多毛植物，帶有鋸齒狀的葉子與花，在西方是很好的装饰窗台的花卉，在欧洲大陆，如德奥等国，尤为常见。

天竺葵的[叶子通常是互生的](../Page/叶子.md "wikilink"),
[掌状浅裂或](../Page/掌状.md "wikilink")[羽状签裂](../Page/羽状.md "wikilink"),
一般具有长叶柄，有的种类[叶上有深浅纹路](../Page/叶.md "wikilink")，每朵[花具五瓣](../Page/花.md "wikilink")，花聚集成伞状，称为“假伞状花序”，花的形状从星状到漏斗状有各种形状，[颜色有](../Page/颜色.md "wikilink")[白色](../Page/白色.md "wikilink")、[粉红色](../Page/粉红色.md "wikilink")、[红色](../Page/红色.md "wikilink")、[橘红色](../Page/橘红色.md "wikilink")、[紫色直到甚至近似](../Page/紫色.md "wikilink")[黑色](../Page/黑色.md "wikilink")，各种都有。

人工培育的天竺葵可以分为六大组，其中带状花又可进一步区分。

  - 天使花
  - 蔓藤型
  - 帝王花（法国种）
  - 灌木叶
  - 独花型
  - 带状花-直立型
      - 仙人掌花型
      - 重瓣
      - 观叶型
      - Formosum hybrid
      - 月季花型
      - 单瓣
      - 星状花型

第一个天竺葵的种植物种为*[齐斯特洋葵](../Page/齐斯特洋葵.md "wikilink")*，为[南非原产](../Page/南非.md "wikilink")。在1600年之前被一艘停靠过[好望角的船只带回](../Page/好望角.md "wikilink")[荷兰](../Page/荷兰.md "wikilink")[莱顿的](../Page/莱顿.md "wikilink")[植物园](../Page/植物园.md "wikilink")。
1631年，英国园艺家[John
Tradescant从](../Page/John_Tradescant.md "wikilink")[巴黎的](../Page/巴黎的.md "wikilink")[Rene
Morin购买种子并引进这种植物到](../Page/Rene_Morin.md "wikilink")[英格兰](../Page/英格兰.md "wikilink")。
天竺葵的英文名字*Pelargonium*是于[1738年由](../Page/1738.md "wikilink")[Johannes
Burman命名的](../Page/Johannes_Burman.md "wikilink")。

## 天竺葵精油

天竺葵精油由花、葉子與枝幹中以蒸氣萃取出來的，這種精油很漂亮，淡綠色，氣味也可以用淡綠色形容。有時天竺葵精油被形容成平民的玫瑰。萃取成精油後，能抗憂鬱、抗糖尿、殺菌、抗感染、驅蟲、止痛。具肌膚收斂性，也是一種優質的平衡物質，能平衡賀爾蒙，對經前症候群、神經緊張、皮膚問題、神經痛等皆有止痛、溫和鎮定的功效，可當作沐浴添加劑。天竺葵精油也能帶來安全與舒適感，可提振精神並鼓勵人們勇於表達自我，當做熏香或是香水使用能化解悲傷的情緒。能與佛手柑、羅勒、迷迭香、玫瑰、野橘、檀香、檸檬、廣藿香和薰衣草等精油相互調和。

天竺葵對於皮膚再生有良好的效果，可消緩炎症、濕疹、面皰、曬傷、傷口感染與水泡。天竺葵精油也有不錯的保濕效果

<center>

<File:Rose> Geranium.jpg|香叶天竺葵
[File:Tzkyz.jpg|left|thumb|天竺葵的叶子](File:Tzkyz.jpg%7Cleft%7Cthumb%7C天竺葵的叶子)
<File:R10> kamera 059be.jpg|天竺葵 文件:天竺葵.JPG|天竺葵

</center>

## 参考文献

  - Maria Lis-Balchin, ed., *Geranium and Pelargonium: History of
    Nomenclature, Usage and Cultivation*. ([Taylor and
    Francis](../Page/Taylor_and_Francis.md "wikilink"), 2002) ISBN
    0-415-28487-2
  - [天竺葵
    Tianzhukui](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00642)
    藥用植物圖像資料庫 (香港浸會大學中醫藥學院)

[Category:花卉](../Category/花卉.md "wikilink")
[\*](../Category/天竺葵属.md "wikilink")