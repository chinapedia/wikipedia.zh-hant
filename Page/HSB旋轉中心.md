**HSB旋轉中心**（）於2005年8月27日開幕；建築物位於[瑞典](../Page/瑞典.md "wikilink")[馬爾默樓高](../Page/馬爾默.md "wikilink")、54層的一幢[摩天大廈](../Page/摩天大廈.md "wikilink")，有「扭毛巾大樓」之稱\[1\]；為當地著名地標\[2\]；它是除了是[瑞典及](../Page/瑞典.md "wikilink")[北歐最高的建築物外](../Page/斯堪的納維亞.md "wikilink")，也是[歐洲第二高的住宅大廈](../Page/歐洲.md "wikilink")。

## 設計

HSB旋轉中心的設計源於一個名為「[扭軀幹](../Page/扭軀幹.md "wikilink")」（Twisting
Torso）的[白色](../Page/白色.md "wikilink")[大理石片雕塑](../Page/大理石.md "wikilink")，該雕塑1999年由[西班牙設計師](../Page/西班牙.md "wikilink")[聖地牙哥·卡洛特拉瓦模仿扭曲的人形製作](../Page/聖地牙哥·卡洛特拉瓦.md "wikilink")。\[3\]\[4\]
大廈的建築模型是由歐洲住房展覽會的主辦單位Bo01邀請該設計師在2001年於[瑞典](../Page/瑞典.md "wikilink")[馬爾默舉辦展覽時的一個臨時展館內展出](../Page/馬爾默.md "wikilink")\[5\]。

## 興建

HSB旋轉中心於2001年2月14日動工，施工為期四年半。2002年3月和8月，該大樓分別完成[地基及](../Page/地基.md "wikilink")[混凝土澆注工程](../Page/混凝土.md "wikilink")\[6\]。2005年8月27日，中心正式開幕\[7\]。用戶於同年11月1日起入伙\[8\]。

## 建築特色

整幢大廈高，合共54層\[9\]。HSB旋轉中心的大廈核心是一個為[直徑](../Page/直徑.md "wikilink")的巨大的混凝土管；而HSB旋轉中心的大廈外牆厚度由地面的逐漸收到大廈頂層的\[10\]。

HSB旋轉中心共分九個區層，每個區層有五層\[11\]。每個區層的方向都跟下面的區層不同，而2800板外牆及2250塊玻璃幕牆均以1.6度「旋轉」\[12\]；當中最高及最底的區層成[直角](../Page/直角.md "wikilink")，看起來整座大廈猶如扭了[毛巾一圈](../Page/毛巾.md "wikilink")，因而有「扭毛巾大樓」之稱\[13\]。

## 用途

第一、二個區層的一至十樓用作商用辦公室用途，每層佔地，全部總佔地；當中第一區層的5個樓層用作HSB建築公司在[馬爾默的總部](../Page/馬爾默.md "wikilink")\[14\]。其餘區層的樓層均用作住宅用途，每層有三至五個單位，合共147個單位\[15\]\[16\]，總佔地為；而屬於住戶樓層的43樓是為客房樓層，這層設有[健身房和](../Page/健身.md "wikilink")[桑拿設備](../Page/桑拿.md "wikilink")；除此之外，43樓及49樓同為觀光樓層\[17\]；而最高的第53、54層是用作會議室及演講廳\[18\]。建築物分別設有2部及3部[升降機通往商用辦公室及住宅的樓層](../Page/升降機.md "wikilink")\[19\]，而住宅升降機只需38秒就可到達最高的一層\[20\]。

## 獎項

2005年3月，HSB旋轉中心除了於[法國](../Page/法國.md "wikilink")[坎城的世界房地產市場頒獎典禮奪得](../Page/坎城.md "wikilink")「最佳住宅類大獎」\[21\]
外，也獲最大的摩天大廈網站──安普爾斯（Emporis）授予「最佳新摩天大廈（）」榮譽。翌年，該建築又獲位於[瑞士](../Page/瑞士.md "wikilink")[洛桑的國際混凝土聯合會](../Page/洛桑.md "wikilink")（fib）頒發「國際混凝土聯合會2006年傑出結構獎（）」\[22\]。

## 軼事

2006年，著名[奧地利定點](../Page/奧地利.md "wikilink")[跳傘好手](../Page/跳傘.md "wikilink")[菲利克斯·保加拿成功在HSB旋轉中心進行空中定點跳傘](../Page/菲利克斯·保加拿.md "wikilink")。\[23\]
在兴建过程中，[探索频道制作了一集有关HSB旋轉中心建设的](../Page/探索频道.md "wikilink")[工程大突破节目](../Page/工程大突破.md "wikilink")。

## 图集

<File:Turning> torso by night1.jpg|夜景 <File:The> Turning Torso,
Malmo.JPG|从底部看
[File:Turning_Torso_from_aiplane_2.jpg|鸟瞰](File:Turning_Torso_from_aiplane_2.jpg%7C鸟瞰)

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [HSB旋轉中心官方網頁](https://web.archive.org/web/20100623024903/http://www.hsb.se/omhsb/regionforeningar/malmo/hsb-turning-torso)

  - [HSB旋轉中心圖片及新聞](http://www.bizzbook.com/map/turningtorso.html)

  - [HSB旋轉中心360度全景圖](http://virtualsweden.se/panorama/turning-torso)

[Category:瑞典摩天大楼](../Category/瑞典摩天大楼.md "wikilink")
[Category:瑞典建築物](../Category/瑞典建築物.md "wikilink")
[Category:后现代主义建筑](../Category/后现代主义建筑.md "wikilink")
[Category:2006年完工建築物](../Category/2006年完工建築物.md "wikilink")
[Category:150米至199米高的摩天大樓](../Category/150米至199米高的摩天大樓.md "wikilink")
[Category:扭曲建築物](../Category/扭曲建築物.md "wikilink")

1.
2.
3.

4.

5.

6.
7.

8.

9.
10.
11.

12.

13.

14.

15.
16.

17.

18.

19.

20.
21.
22.

23.