[P1080792.JPG](https://zh.wikipedia.org/wiki/File:P1080792.JPG "fig:P1080792.JPG")
[人民英雄纪念碑侧面.jpg](https://zh.wikipedia.org/wiki/File:人民英雄纪念碑侧面.jpg "fig:人民英雄纪念碑侧面.jpg")

**人民英雄纪念碑**位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[首都](../Page/中国首都.md "wikilink")[北京市](../Page/北京市.md "wikilink")[天安门广场的中央](../Page/天安门广场.md "wikilink")，在[天安门城楼](../Page/天安门.md "wikilink")、[长安街](../Page/长安街.md "wikilink")、国旗旗杆以南，[毛主席纪念堂以北](../Page/毛主席纪念堂.md "wikilink")，是1949年[中国人民政治协商会议第一届全体会议为纪念](../Page/中国人民政治协商会议第一届全体会议.md "wikilink")[中国近现代史上的](../Page/中国近现代史.md "wikilink")[牺牲的人民英雄而建的](../Page/烈士_\(中华人民共和国\).md "wikilink")[纪念碑](../Page/纪念碑.md "wikilink")。1949年9月30日奠基，1952年8月1日开工，1958年5月1日揭幕，1961年成为第一批[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。该纪念碑是[中华人民共和国成立后第一个由国家兴建的大型纪念碑](../Page/中华人民共和国.md "wikilink")，也是第一个大型公共艺术项目。\[1\]

## 纪念碑

人民英雄纪念碑通高37.94米，占地约3100平方米，由碑身、须弥座和台基三部分组成。

碑体平面呈长方形，顶为[盝顶](../Page/盝顶.md "wikilink")。碑身用413块[花岗石分](../Page/花岗石.md "wikilink")32层垒砌而成。正面（北面）碑心是一整块石材，长14.7米、宽2.9米、厚1米、重103吨，镌刻着[毛泽东题写的](../Page/毛泽东.md "wikilink")“**人民英雄永垂不朽**”八个鎏金大字。背面碑心由7块石材构成，内容為[毛泽东起草](../Page/毛泽东.md "wikilink")、[周恩来书写的碑文](../Page/周恩来.md "wikilink")，共150字，其文如下：（以下採用為碑文實際字體）

碑文中的“三年以來”是指[解放戰爭時期](../Page/第二次国共内战.md "wikilink")（1946—1949）；“三十年以來”是指​​以[五四運動起點的](../Page/五四運動.md "wikilink")[新民主主義革命時期](../Page/新民主主義.md "wikilink")（1919—1949），標誌著新民主義革命開始；“上溯到一千八百四十年”是指以[鴉片戰爭為起點的整個民主革命時期](../Page/鴉片戰爭.md "wikilink")（1840—1949）；而1840年則是中國受侵略的開始，1840年鴉片戰爭，中國從此瀰漫著滾滾硝煙​​，成為了[半殖民地半](../Page/半殖民地.md "wikilink")[封建國家](../Page/封建國家.md "wikilink")，標誌著[中國近代史開端](../Page/中國近代史.md "wikilink")。內部敵人：反動派，清政府；外部敵人：侵略者。

在碑身的左右两侧刻有五角星、松柏和旗帜组成的花纹图案。

碑身由[林徽因设计的两层](../Page/林徽因.md "wikilink")[须弥座承托](../Page/须弥座.md "wikilink")。上层小须弥座的四面刻[牡丹](../Page/牡丹.md "wikilink")、[荷花](../Page/荷花.md "wikilink")、[菊花](../Page/菊花.md "wikilink")、[垂幔等拼成的八个花环](../Page/垂幔.md "wikilink")，以示对烈士的崇敬之情。下层大[须弥座的束腰部镶嵌着十幅巨大的](../Page/须弥座.md "wikilink")[汉白玉](../Page/汉白玉.md "wikilink")[浮雕](../Page/浮雕.md "wikilink")，其中八幅作品反映了中国近现代史上的革命事件，按东南西北的顺序依次为“[虎门销烟](../Page/虎门销烟.md "wikilink")”、“[金田起义](../Page/金田起义.md "wikilink")”、“[武昌起义](../Page/武昌起义.md "wikilink")”、“[五四运动](../Page/五四运动.md "wikilink")”、“[五卅运动](../Page/五卅运动.md "wikilink")”、“[南昌起义](../Page/南昌起义.md "wikilink")”、“抗日游击战”和“[胜利渡长江](../Page/渡江战役.md "wikilink")·解放全中国”。此外，在北面正中“胜利渡长江”的两侧还有两幅装饰性作品“支援前线”和“欢迎人民解放军”。这十座浮雕的高度均为2米，宽2至6.4米，总长40.68米，一共雕刻了180个人物，是由[刘开渠等人设计创作的](../Page/刘开渠.md "wikilink")。

纪念碑的台基也分为两层，上层呈方形，下层呈[海棠形](../Page/海棠.md "wikilink")，东西宽50.44米，南北长61.5米。两层台基的四面均为栏杆环绕，并设有台阶。整座人民英雄纪念碑共用1.7万块花岗石和汉白玉石砌成，面对[天安门](../Page/天安门.md "wikilink")，肃穆庄严，雄伟壮观。它整合了明清以来的汉民族建筑与希腊罗马建筑的风格，并与北京这座城市的中轴线呼应。

## 兴建

### 简述

1949年9月30日，[中国人民政治协商会议第一届全体会议通过决议](../Page/中国人民政治协商会议第一届全体会议.md "wikilink")，提出“为纪念在[人民解放战争和](../Page/第二次国共内战.md "wikilink")[人民革命](../Page/人民革命.md "wikilink")、[民族解放](../Page/民族解放.md "wikilink")、[民主运动中牺牲的人民英雄](../Page/民主运动.md "wikilink")”，在北京建立“为国牺牲的人民英雄纪念碑”。当天下午在[天安门广场上](../Page/天安门广场.md "wikilink")，[毛泽东和全体政协代表为纪念碑奠基](../Page/毛泽东.md "wikilink")。此后开始了广泛的设计方案的征集活动。在建筑学家[梁思成和雕塑家](../Page/梁思成.md "wikilink")[刘开渠的主持下](../Page/刘开渠.md "wikilink")，确定了最终的建设方案。1952年8月1日起工程正式开工，到1958年4月落成，同年5月1日举行了隆重的揭幕典礼。

### 奠基

1949年9月30日，中国人民政治协商会议第一届全体会议闭幕。当天下午6时，參加[中国人民政治协商会议第一届全体会议代表及首都各界群众代表共](../Page/中国人民政治协商会议第一届全体会议代表列表.md "wikilink")3000人在天安门广场南端举行了人民英雄纪念碑奠基典礼。

典礼在《[义勇军进行曲](../Page/义勇军进行曲.md "wikilink")》中开始。[周恩来代表中国人民政治协商会议第一届全体会议主席团致词](../Page/周恩来.md "wikilink")\[2\]：

周恩来致词结束后，全体人员脱帽默哀。此后，[中央人民政府主席](../Page/中央人民政府主席.md "wikilink")[毛泽东宣读了亲自撰写的碑文](../Page/毛泽东.md "wikilink")，并第一个为纪念碑的奠基石执锹铲土。

奠基石高1米7，其座高65厘米、宽100厘米、厚35厘米，质地为[艾青石](../Page/艾青石.md "wikilink")。石上有铭文155字，[隶书](../Page/隶书.md "wikilink")，由[毛泽东起草](../Page/毛泽东.md "wikilink")，[叶恭绰书写](../Page/叶恭绰.md "wikilink")，[陈志敬镌刻](../Page/陈志敬.md "wikilink")。\[3\]初稿为[彭真所拟](../Page/彭真.md "wikilink")，原题为“中国人民解放战争和中国人民革命烈士纪念碑奠基典礼”。[毛泽东对其加以修改](../Page/毛泽东.md "wikilink")，形成了终稿：\[4\]

### 首都人民英雄纪念碑兴建委员会

1951年2月27日，[北京市人民政府向](../Page/北京市人民政府.md "wikilink")[中央人民政府政务院提交了由北京市市长](../Page/中央人民政府政务院.md "wikilink")[聂荣臻](../Page/聂荣臻.md "wikilink")、副市长[张友渔](../Page/張友漁_\(法學家\).md "wikilink")、[吴晗签署的报告](../Page/吴晗.md "wikilink")，称拟于1951年春开始施工，并提出了建立组织机构和工程预算等情况。

北京市人民政府1952年5月22日关于成立首都人民英雄纪念碑兴建委员会致政务院周总理的报告稿记载，北京市人民政府于1952年4月29日邀请中央部委、[中央人民政府人民革命军事委员会总政治部](../Page/中央人民政府人民革命军事委员会.md "wikilink")、[中国人民政治协商会议全国委员会等](../Page/中国人民政治协商会议全国委员会.md "wikilink")9个单位举行了人民英雄纪念碑筹建座谈会，决定成立“首都人民英雄纪念碑兴建委员会”，由17个单位各推派代表一名为委员。\[5\]

首都人民英雄纪念碑兴建委员会的17个组成单位及其代表如下：\[6\]

1.  [中国人民政治协商会议全国委员会](../Page/中国人民政治协商会议全国委员会.md "wikilink")（[郑振铎](../Page/郑振铎.md "wikilink")）
2.  [中华全国总工会](../Page/中华全国总工会.md "wikilink")（[史占春](../Page/史占春.md "wikilink")）
3.  [中共中央宣传部](../Page/中共中央宣传部.md "wikilink")
4.  [中央人民政府人民革命军事委员会总政治部](../Page/中央人民政府人民革命军事委员会.md "wikilink")（[戴夫](../Page/戴夫_\(中国人民解放军\).md "wikilink")）
5.  [华北军区政治部](../Page/华北军区.md "wikilink")（[丁里](../Page/丁里.md "wikilink")）
6.  [中央人民政府文化部](../Page/中央人民政府文化部.md "wikilink")（[王冶秋](../Page/王冶秋.md "wikilink")）
7.  [政务院机关事务管理局](../Page/政务院机关事务管理局.md "wikilink")（[张效曾](../Page/张效曾.md "wikilink")）
8.  [政务院财政经济委员会](../Page/政务院财政经济委员会.md "wikilink")（[冯昌伯](../Page/冯昌伯.md "wikilink")→[曹言行](../Page/曹言行.md "wikilink")）
9.  [中央人民政府民族事务委员会](../Page/中央人民政府民族事务委员会.md "wikilink")（[金民](../Page/金民.md "wikilink")）
10. [中央人民政府华侨事务委员会](../Page/中央人民政府华侨事务委员会.md "wikilink")（[王纪元](../Page/王纪元.md "wikilink")）
11. [全国美术工作者协会](../Page/全国美术工作者协会.md "wikilink")（[王朝闻](../Page/王朝闻.md "wikilink")）
12. [中国建筑工程学会](../Page/中国建筑工程学会.md "wikilink")（[庄俊](../Page/庄俊.md "wikilink")）
13. [北京市人民政府](../Page/北京市人民政府.md "wikilink")（[彭真](../Page/彭真.md "wikilink")）
14. [中国共产党北京市委员会](../Page/中国共产党北京市委员会.md "wikilink")（[薛子正](../Page/薛子正.md "wikilink")）
15. [北京市总工会](../Page/北京市总工会.md "wikilink")
16. [北京市协商委员会](../Page/中国人民政治协商会议北京市委员会.md "wikilink")（[李健生](../Page/李健生.md "wikilink")）
17. [北京市都市计划委员会](../Page/北京市都市计划委员会.md "wikilink")（[梁思成](../Page/梁思成.md "wikilink")）

1952年5月10日，纪念碑兴建委员会成立会在[北京市人民政府第一会议室召开](../Page/北京市人民政府.md "wikilink")。会议由[梁思成主持](../Page/梁思成.md "wikilink")，[彭真](../Page/彭真.md "wikilink")、[王冶秋](../Page/王冶秋.md "wikilink")、[张效曾因事请假](../Page/张效曾.md "wikilink")，北京市建设局[王明之](../Page/王明之.md "wikilink")、[赵慎之列席](../Page/赵慎之.md "wikilink")。会议通过了《首都人民英雄纪念碑兴建委员会组织规程》，并推选出以下纪念碑兴建委员会的重要工作人员：\[7\]

  - 主任委员：[彭真](../Page/彭真.md "wikilink")
  - 副主任委员：[郑振铎](../Page/郑振铎.md "wikilink")、[梁思成](../Page/梁思成.md "wikilink")
  - 秘书长：[薛子正](../Page/薛子正.md "wikilink")
  - 工程事务处：处长[王明之](../Page/王明之.md "wikilink")；副处长[吴华庆](../Page/吴华庆.md "wikilink")

此后，委员会又设立了其他机构。委员会成立初期的基本构架如下：
**专门委员会**（4个）：\[8\]

  - （1）施工委员会
      - 委员：[郑孝燮](../Page/郑孝燮.md "wikilink")（[中央人民政府重工业部基本建设处副处长](../Page/中央人民政府重工业部.md "wikilink")）、[刘导楠](../Page/刘导楠.md "wikilink")（[中财委总建筑处直属工程公司](../Page/政务院财政经济委员会.md "wikilink")）、[钟森](../Page/钟森.md "wikilink")（北京市建筑公司设计部）、[张象昶](../Page/张象昶.md "wikilink")（北京市企业公司）、[吴柳生](../Page/吴柳生.md "wikilink")（[清华大学](../Page/清华大学.md "wikilink")）。
  - （2）建筑设计专门委员会
      - 委员：[庄俊](../Page/庄俊.md "wikilink")（[中国建筑公司](../Page/中国建筑公司.md "wikilink")）、[杨廷宝](../Page/杨廷宝.md "wikilink")（[南京大学建筑系](../Page/南京大学建筑系.md "wikilink")）、[郑振铎](../Page/郑振铎.md "wikilink")（[政务院文物局](../Page/国家文物局.md "wikilink")）、[张镈](../Page/张镈.md "wikilink")、[朱兆雪](../Page/朱兆雪.md "wikilink")（以上二人为北京市建筑公司）、[赵政之](../Page/赵政之.md "wikilink")、[林徽因](../Page/林徽因.md "wikilink")、[莫宗江](../Page/莫宗江.md "wikilink")、[吴良镛](../Page/吴良镛.md "wikilink")（以上四人为[清华大学](../Page/清华大学.md "wikilink")）、[王朝闻](../Page/王朝闻.md "wikilink")（[中共中央宣传部](../Page/中共中央宣传部.md "wikilink")）、[陈占祥](../Page/陈占祥.md "wikilink")（北京市都市计划委员会）。
      - 召集人：[梁思成](../Page/梁思成.md "wikilink")
      - 列席：[薛子正](../Page/薛子正.md "wikilink")、[吴华庆](../Page/吴华庆.md "wikilink")、[梁思敬](../Page/梁思敬.md "wikilink")。
  - （3）结构设计专门委员会
      - 委员：[杨宽麟](../Page/杨宽麟.md "wikilink")、[陈致中](../Page/陈致中_\(结构工程师\).md "wikilink")、[陈梁生](../Page/陈梁生.md "wikilink")、[茅以升](../Page/茅以升.md "wikilink")、[蔡方阴](../Page/蔡方阴.md "wikilink")、[林诗伯](../Page/林诗伯.md "wikilink")、[陈志德](../Page/陈志德.md "wikilink")、[卞维德](../Page/卞维德.md "wikilink")、[王明之](../Page/王明之.md "wikilink")。
      - 召集人：[朱兆雪](../Page/朱兆雪.md "wikilink")
  - （4）雕画史料编审委员会
      - 委员：[范文澜](../Page/范文澜.md "wikilink")、[刘大年](../Page/刘大年.md "wikilink")、[荣孟源](../Page/荣孟源.md "wikilink")（以上三人为[中国科学院现代史研究所](../Page/中国科学院现代史研究所.md "wikilink")）、[郑振铎](../Page/郑振铎.md "wikilink")、[王冶秋](../Page/王冶秋.md "wikilink")（以上二人为政务院文物局）、[江丰](../Page/江丰.md "wikilink")、[王朝闻](../Page/王朝闻.md "wikilink")（以上二人为[中央美术学院](../Page/中央美术学院.md "wikilink")）、[陈沂](../Page/陈沂.md "wikilink")（中央人民政府人民革命军事委员会总政治部）、[中共中央宣传部党史资料室](../Page/中共中央宣传部.md "wikilink")（[缪楚黄代表](../Page/缪楚黄.md "wikilink")）、[中共中央办公厅](../Page/中共中央办公厅.md "wikilink")（[裴桐代表](../Page/裴桐.md "wikilink")）。
      - 召集人：[范文澜](../Page/范文澜.md "wikilink")

**处**（3个）：

  - 工程事务处：处长[王明之](../Page/王明之.md "wikilink")；副处长[吴华庆](../Page/吴华庆.md "wikilink")
      - 电气装置组（[中央人民广播电台及北京市电业局负责](../Page/中央人民广播电台.md "wikilink")）
      - 土木施工组：组长[王明之](../Page/王明之.md "wikilink")
      - 石料供应组
      - 财务核算组（由[中财委负责](../Page/政务院财政经济委员会.md "wikilink")）
      - 摄影纪录组（[北京电影制片厂及](../Page/北京电影制片厂.md "wikilink")[新闻摄影局负责](../Page/新闻摄影局.md "wikilink")）
  - 设计处：处长[梁思成](../Page/梁思成.md "wikilink")、[刘开渠](../Page/刘开渠.md "wikilink")
      - 建筑设计组：组长[梁思成](../Page/梁思成.md "wikilink")；副组长[莫宗江](../Page/莫宗江.md "wikilink")
      - 美术工作组
  - 办事处：秘书主任[贾国卿](../Page/贾国卿.md "wikilink")
      - 文书组
      - 会计组
      - 总务组

设计处下辖的美术工作组是[滑田友于](../Page/滑田友.md "wikilink")1952年底初步组建，1953年2月[刘开渠到京后重新组定](../Page/刘开渠.md "wikilink")。重新组定后的美术工作组人员如下：\[9\]

  - 组长：[刘开渠](../Page/刘开渠.md "wikilink")；副组长：[滑田友](../Page/滑田友.md "wikilink")、[彦涵](../Page/彦涵.md "wikilink")、[吴作人](../Page/吴作人.md "wikilink")、[张松鹤](../Page/张松鹤.md "wikilink")
      - 研究组：组长：[刘开渠](../Page/刘开渠.md "wikilink")；副组长：[王式廓](../Page/王式廓.md "wikilink")；组员：[滑田友](../Page/滑田友.md "wikilink")、[吴作人](../Page/吴作人.md "wikilink")、[江丰](../Page/江丰.md "wikilink")、[彦涵](../Page/彦涵.md "wikilink")、[王朝闻](../Page/王朝闻.md "wikilink")、[蔡若虹](../Page/蔡若虹.md "wikilink")、[张松鹤](../Page/张松鹤.md "wikilink")、[罗工柳](../Page/罗工柳.md "wikilink")、[郑可](../Page/郑可.md "wikilink")
      - 东面组：组长：[萧传玖](../Page/萧传玖.md "wikilink")、[王丙召](../Page/王丙召.md "wikilink")
          - 南昌组：组长：萧传玖、[王式廓](../Page/王式廓.md "wikilink")；组员：[王万景](../Page/王万景.md "wikilink")、[王卓予](../Page/王卓予.md "wikilink")
          - 延安组：组长：王丙召、[古元](../Page/古元.md "wikilink")；组员：[王鸿文](../Page/王鸿文.md "wikilink")
      - 南面组：组长：王丙召、[辛莽](../Page/辛莽.md "wikilink")
          - 游击组：组长：张松鹤、辛莽；组员：[陈淑光](../Page/陈淑光.md "wikilink")
          - 解放组：组长：刘开渠、彦涵；组员：[夏肖敏](../Page/夏肖敏.md "wikilink")、[凌春德](../Page/凌春德.md "wikilink")、[吴汝钊](../Page/吴汝钊.md "wikilink")
          - 禁烟组：组长：王琦；组员：[沈海驹](../Page/沈海驹.md "wikilink")
      - 西面组：组长：[曾竹韶](../Page/曾竹韶.md "wikilink")、[李宗津](../Page/李宗津.md "wikilink")
          - 太平组：组长：[廖新学](../Page/廖新学.md "wikilink")、李宗津；组员：[谢家声](../Page/谢家声.md "wikilink")、[刘士铭](../Page/刘士铭.md "wikilink")
          - 甲午组：组长：曾竹韶、[艾中信](../Page/艾中信.md "wikilink")；组员：[李祯祥](../Page/李祯祥.md "wikilink")
      - 北面组：组长：曾竹韶、[董希文](../Page/董希文.md "wikilink")
          - 辛亥组：组长：[司徒杰](../Page/司徒杰.md "wikilink")、董希文；组员：[邹佩珠](../Page/邹佩珠.md "wikilink")、[张文新](../Page/张文新.md "wikilink")
          - 五四组：组长：滑田友、[冯法祀](../Page/冯法祀.md "wikilink")；组员：[谷浩](../Page/谷浩.md "wikilink")、[陈天](../Page/陈天.md "wikilink")
          - 五卅组：组长：[王临乙](../Page/王临乙.md "wikilink")、吴作人；组员：[于津源](../Page/于津源.md "wikilink")
      - 秘书组：组长：陈天、[沈海驹](../Page/沈海驹.md "wikilink")
          - 资料：[谢家声](../Page/谢家声.md "wikilink")、[夏肖敏](../Page/夏肖敏.md "wikilink")、[吴汝钊](../Page/吴汝钊.md "wikilink")
          - 总务：[信英华](../Page/信英华.md "wikilink")、[刘培林](../Page/刘培林.md "wikilink")、[李祯祥](../Page/李祯祥.md "wikilink")
          - 会计：[傅筑岩](../Page/傅筑岩.md "wikilink")、[韩云鹏](../Page/韩云鹏.md "wikilink")
          - 秘书助理：[卢厂](../Page/卢厂.md "wikilink")
          - 石工管理：[王万景](../Page/王万景.md "wikilink")、[王鸿文](../Page/王鸿文.md "wikilink")

### 浮雕

1952年7月中旬，史料委员会初步提出了浮雕的主题方案，共有九幅。1953年1月19日，秘书长[薛子正传达了](../Page/薛子正.md "wikilink")[毛泽东主席关于浮雕主题的指示](../Page/毛泽东.md "wikilink")：\[10\]

史料委员会经过多次讨论后，又对原先提出的浮雕主题进行了多次改变，形成了现在的八幅。\[11\]

1.  [林则徐](../Page/林则徐.md "wikilink")《[虎门销烟](../Page/虎门销烟.md "wikilink")》画稿[艾中信](../Page/艾中信.md "wikilink")，雕刻[曾竹韶](../Page/曾竹韶.md "wikilink")\[12\]
2.  [太平天国](../Page/太平天国.md "wikilink")[洪秀全](../Page/洪秀全.md "wikilink")《[金田起义](../Page/金田起义.md "wikilink")》画稿[李宗津](../Page/李宗津.md "wikilink")，雕刻[王丙召](../Page/王丙召.md "wikilink")，助手[刘士铭](../Page/刘士铭.md "wikilink")、[谢家声](../Page/谢家声.md "wikilink")
3.  《武昌起义》画稿[董希文](../Page/董希文.md "wikilink")，雕刻[傅天仇](../Page/傅天仇.md "wikilink")，助手[祖文轩](../Page/祖文轩.md "wikilink")
4.  《五四运动》画稿[冯法祀](../Page/冯法祀.md "wikilink")，雕刻[滑田友](../Page/滑田友.md "wikilink")，助手[陈天](../Page/陈天.md "wikilink")、[夏肖敏](../Page/夏肖敏.md "wikilink")、[吴汝钊](../Page/吴汝钊.md "wikilink")
5.  《五卅运动》画稿[吴作人](../Page/吴作人.md "wikilink")，雕刻[王临乙](../Page/王临乙.md "wikilink")，助手[李祯祥](../Page/李祯祥.md "wikilink")、[王澎（王鸿文）](../Page/王鸿文.md "wikilink")
6.  《南昌起义》画稿[王式廓](../Page/王式廓.md "wikilink")，雕刻[萧传玖](../Page/萧传玖.md "wikilink")，助手[王卓予](../Page/王卓予.md "wikilink")、[王万景](../Page/王万景.md "wikilink")
7.  《抗日游击战》画稿[辛莽](../Page/辛莽.md "wikilink")，雕刻[张松鹤](../Page/张松鹤.md "wikilink")
8.  《胜利渡长江·解放全中国》画稿[彦涵](../Page/彦涵.md "wikilink")、雕刻[刘开渠](../Page/刘开渠.md "wikilink")

<!-- end list -->

  - 《支援前线》雕刻[刘开渠](../Page/刘开渠.md "wikilink")
  - 《欢迎人民解放军》雕刻[刘开渠](../Page/刘开渠.md "wikilink")
  - 放大稿助手：[王殿臣](../Page/王殿臣.md "wikilink")、[祖文轩](../Page/祖文轩.md "wikilink")、[李唐寿等](../Page/李唐寿.md "wikilink")\[13\]

## 修缮记录

  - 1971年，纪念碑北面原为[鎏金的](../Page/鎏金.md "wikilink")“人民英雄永垂不朽”题字被改成[红色](../Page/红色.md "wikilink")[玻璃钢字](../Page/玻璃钢.md "wikilink")。
  - 1980年[国庆节前](../Page/中华人民共和国国庆节.md "wikilink")，纪念碑拆除了红色玻璃钢字，更换了损坏断裂的石料，恢复了原有的鎏金字。
  - 1999年6月，在天安门广场进行大规模改造时又对其进行了彻底的清洗和维护。
  - 2006年，修复纪念碑存在的漏水、有裂缝、部分地方错位、风化严重等问题，是47年来首次大规模的修缮。\[14\]

## 历史事件

  - 1976年4月5日，天安门广场上发生以悼念已故总理[周恩来为目的的](../Page/周恩来.md "wikilink")[四五运动](../Page/四五运动.md "wikilink")，大量的花圈和诗歌布满了人民英雄纪念碑及广场。这一运动后被[四人帮镇压](../Page/四人帮.md "wikilink")，但运动中的诗歌后来被搜集出版，称为“[天安门诗抄](../Page/天安门诗抄.md "wikilink")”。\[15\]
  - 1989年4月16日，北京高校的学生把已故前中共中央总书记[胡耀邦的遗照放在纪念碑上](../Page/胡耀邦.md "wikilink")，也有人把[大字报等贴在这上面](../Page/大字报.md "wikilink")。在随后整个[學生運動的过程中](../Page/六四事件.md "wikilink")，纪念碑都是抗议者的指挥部和活动中心。当[中国人民解放军戒嚴部隊在](../Page/中国人民解放军.md "wikilink")6月4日凌晨对天安门广场实施[清场时](../Page/六四清场.md "wikilink")，聚集在纪念碑周围的数千学生是最后离开的。在此之前纪念碑的基座是开放的，任何人都可以上去，近距离参观纪念碑须弥座上的浮雕。而此后纪念碑基座则被护栏围起，不开放予公众。内有武警站岗。此外还有[少先队员的](../Page/中国少年先锋队.md "wikilink")“少年先锋岗”，该岗自1986年起设立，2003年因[SARS而中断](../Page/SARS.md "wikilink")，2006年起恢复。\[16\]

## 紀念活動

### 國慶節和烈士纪念日

[党和国家领导人](../Page/党和国家领导人.md "wikilink")，和各界代表，以往在10月1日[中華人民共和國國慶節向人民英雄纪念碑敬献花篮](../Page/中華人民共和國國慶節.md "wikilink")。該儀式於2008年\[17\]、2010年\[18\]、2011年\[19\]、2012年、2013年\[20\]举行（2009年則是在[天安門廣場舉行了](../Page/天安門廣場.md "wikilink")[中华人民共和国成立60周年庆典](../Page/中华人民共和国成立60周年庆典.md "wikilink")）。2014年8月31日，第十二届全国人大常委会第十次会议通过关于设立[烈士纪念日的决定](../Page/烈士纪念日.md "wikilink")，规定每年9月30日，即人民英雄纪念碑奠基日为烈士纪念日，并规定每年9月30日国家举行纪念烈士活动。

### 抗日戰爭勝利紀念日

[党和国家领导人和各界代表在](../Page/党和国家领导人.md "wikilink")9月3日[抗日戰爭勝利紀念日向人民英雄纪念碑敬献花篮](../Page/抗日戰爭勝利紀念日.md "wikilink")。该仪式曾于1995年、2005年\[21\][抗日战争胜利](../Page/抗日战争.md "wikilink")50、60周年之際举行。

### 外国领导人敬献花篮

人民英雄纪念碑也是外国领导人来京访问时敬献花篮的场所。

  - 1991年8月，日本首相[海部俊樹在訪華期間](../Page/海部俊樹.md "wikilink")，向人民英雄紀念碑獻了花圈，挽聯寫有“人民英雄永垂不朽”幾個字。\[22\]

## 邮票

为纪念人民英雄纪念碑落成，[中华人民共和国邮电部于纪念碑揭幕之日暨](../Page/中华人民共和国邮电部.md "wikilink")1958年5月1日发行了纪念邮票，全套1枚，主图为人民英雄纪念碑，画面为红色。1958年5月30日发行该邮票的同名[小全张](../Page/小全张.md "wikilink")（M）1枚，其图案除纪念碑邮票外，还印有纪念碑正面和背面的碑文。该小全张是[中国人民邮政发行的第一枚小全张](../Page/中国人民邮政.md "wikilink")。

  - 邮票：
      - 发行日期：1958年5月1日
      - 编号：纪47
      - 1-1（165）人民英雄纪念碑
      - 面值：8分
      - 邮票规格：35×27.5mm
      - 齿孔度数：14度
      - 整张枚数：90枚
      - 版别：雕刻版
      - 设计者：[孙传哲](../Page/孙传哲.md "wikilink")
      - 雕刻者：[孔绍惠](../Page/孔绍惠.md "wikilink")
      - 印刷厂：[中国近代印刷公司](../Page/中国近代印刷公司.md "wikilink")

<!-- end list -->

  - 小全张
      - 发行日期：1958年5月30日
      - 编号：纪47M
      - 面值：8分（售价0.12元）
      - 小全张规格：87X137mm、其中邮票尺寸：35×27.5mm
      - 齿孔度数：无齿
      - 版别：雕刻版
      - 设计者：[孙传哲](../Page/孙传哲.md "wikilink")（邮票）、[刘硕仁](../Page/刘硕仁.md "wikilink")（M）
      - 雕刻者：[孔绍惠](../Page/孔绍惠.md "wikilink")
      - 印刷厂：[中国近代印刷公司](../Page/中国近代印刷公司.md "wikilink")

## 参考文献

## 外部链接

  - [华夏文化网－人民英雄纪念碑建设始末](http://gb.cri.cn/3601/2004/09/28/1266@313552.htm)

## 参见

  - [忠烈祠](../Page/忠烈祠.md "wikilink")
      - [國民革命忠烈祠](../Page/國民革命忠烈祠.md "wikilink")
  - [烈士 (中华人民共和国)](../Page/烈士_\(中华人民共和国\).md "wikilink")
      - [烈士纪念日](../Page/烈士纪念日.md "wikilink")
      - [烈士陵园](../Page/烈士陵园.md "wikilink")
      - [全国重点烈士纪念建筑物保护单位](../Page/全国重点烈士纪念建筑物保护单位.md "wikilink")
  - [八宝山革命公墓](../Page/八宝山革命公墓.md "wikilink")
  - [毛主席纪念堂](../Page/毛主席纪念堂.md "wikilink")
  - [北京太庙](../Page/北京太庙.md "wikilink")

{{-}}

[Category:中华人民共和国中央政府建筑物
(北京)](../Category/中华人民共和国中央政府建筑物_\(北京\).md "wikilink")
[Category:中华人民共和国纪念碑](../Category/中华人民共和国纪念碑.md "wikilink")
[Category:北京市纪念碑](../Category/北京市纪念碑.md "wikilink")
[Category:天安门广场](../Category/天安门广场.md "wikilink")
[Category:北京中轴线](../Category/北京中轴线.md "wikilink")
[Category:中国人民政治协商会议第一届全体会议](../Category/中国人民政治协商会议第一届全体会议.md "wikilink")

1.
2.  [人民英雄纪念碑奠基前后，环球视野，于2011-10-1查阅](http://www.globalview.cn/ReadNews.asp?NewsID=207)


3.  [人民英雄纪念碑奠基碑失踪50年
    碑文与现在不同，新浪网，2004年09月26日](http://news.sina.com.cn/c/2004-09-26/04544425148.shtml)

4.  [詹晓，人民英雄纪念碑的诞生，光明网，2004年12月26日](http://www.gmw.cn/content/2004-12/26/content_153578.htm)

5.  [刘开渠与人民英雄纪念碑的组织工作，中央美术学院，于2011-10-1查阅](http://www.cafa.com.cn/comments/?N=1222)

6.
7.
8.
9.
10. 梁思成全集·第五卷，北京：中国建筑工业出版社，2001年，第463页

11.

12. 孟兰英，人民英雄纪念碑浮雕创作者之一曾竹韶，党史文汇 2011年第9期

13.
14. [人民英雄纪念碑开始全面修缮](http://www.singtaonet.com/city/center/t20060209_135833.html)，星岛环球网引自《新京报》，2006年2月9日

15. [四五运动期间广场上抗议的民众，中央为读诗者](http://cimg.163.com/catchpic/1/1E/1E175714A7DFB61782202984E5736938.jpg)，[网易](../Page/网易.md "wikilink")

16. [人民英雄纪念碑将恢复少先队员站岗，新华网，2006年09月29日](http://news.xinhuanet.com/edu/2006-09/29/content_5151029.htm)

17. [胡锦涛等党和国家领导人向人民英雄纪念碑敬献花篮](http://news.xinhuanet.com/newscenter/2008-10/01/content_10138286.htm)，新华网，2008年10月1日

18. [胡锦涛等党和国家领导人向人民英雄纪念碑敬献花篮](http://politics.people.com.cn/GB/1024/12870526.html)
    ，人民网，2010年10月1日

19. [胡锦涛等到天安门广场向人民英雄纪念碑敬献花篮](http://www.gov.cn/ldhd/2011-10/01/content_1961467.htm)，中国政府网，2011年10月1日

20. [2013年10月1日](http://news.sina.com.cn/c/2013-10-01/103128346440.shtml)

21. [为了和平的纪念
    向人民英雄纪念碑敬献花篮仪式侧记，人民网，2005年09月04日](http://politics.people.com.cn/GB/1026/3666029.html)

22. [“对日中关系要有大局观和决断力”，京報网，2012年04月25日](http://roll.sohu.com/20120425/n341535971.shtml)