<noinclude></noinclude>  **名字**（，），正式的名字是**真名、正式或者全名**，字、号等也在此说明。

## 生平

本节内容应按年份编排，例如“*婚姻家庭*”可能排在“补充说明”的前面或后面，余类推。

### 早年生活

<includeonly>![有关图片](有关图片 "有关图片")</includeonly>

用新闻报道的风格描述传主的早期生活史。

### 婚姻家庭

如果传主结过婚而且有后代，描述他的婚姻，并且列出其后代。

  - [传主的儿子](传主的儿子.md "wikilink")（[生日](生日.md "wikilink")－[死期](死期.md "wikilink")）如果有的话，用一句话概括其值得注意的事迹。
  - [传主的女儿](传主的女儿.md "wikilink")（[生日](生日.md "wikilink")－[死期](死期.md "wikilink")）如果有的话，用一句话概括其值得注意的事迹。

### 补充说明

如果传主的生活中发生了一些要补充说明的事情，在此详细解释。

### 逝世及以后

（**如果有的话**）描述一下**遗产**（包括精神的遗产） 。参见[:en:Charles Darwin\#Death and
legacy](:en:Charles_Darwin#Death_and_legacy.md "wikilink")。

## 宗教观点

如果有的话描述一下。

## 著作

如果有的话，按面世日期列出其作品。参见[:en:Charles Darwin
bibliography或](:en:Charles_Darwin_bibliography.md "wikilink")[达尔文](查尔斯·达尔文#著作與信件.md "wikilink")。

### 已出版的作品

### 信件

## 参见

有一个[傳記列表](傳記.md "wikilink")，是按[MLA格式编写的](MLA.md "wikilink")。参考[EasyBib.com](http://www.easybib.com/.md)了解如何简单地编写MLA格式的传记条目。

  - [一定要标明原始文献！](Wikipedia:文献的引用.md "wikilink")
  - [不要写入原创研究！](Wikipedia:非原创研究.md "wikilink")

## 相关条目

按照字母序/笔划序/拼音序列出所有的相关条目，通用名词放在前面，专有名词放后面。

  - [自传](自传.md "wikilink")
  - [傳記](傳記.md "wikilink")
  - [维基传记](Wikipedia:维基传记.md "wikilink")
  - [维基调查](Wikipedia:维基调查.md "wikilink")

## 外部链接

列出其官方网站，头衔放在传主名称后面，而其他有用的信息也可以写。