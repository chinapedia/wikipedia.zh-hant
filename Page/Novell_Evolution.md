**Evolution**或**Novell
Evolution**（在[Novell於](../Page/Novell.md "wikilink")2003年收購[Ximian前稱為](../Page/Ximian.md "wikilink")**Ximian
Evolution**）是[GNOME官方的個人資訊管理員](../Page/GNOME.md "wikilink")，亦是工作群組資訊管理工具。它集合[電郵](../Page/電郵.md "wikilink")、[行事曆](../Page/行事曆.md "wikilink")、[地址簿與](../Page/地址簿.md "wikilink")[工作排程等功能於一身](../Page/工作排程.md "wikilink")。自[GNOME](../Page/GNOME.md "wikilink")2.8以後，它是[GNOME的官方套件](../Page/GNOME.md "wikilink")。現在Evolution的開發由Novell所贊助。

它的用戶介面及功能非常類似於[Microsoft
Outlook](../Page/Microsoft_Outlook.md "wikilink")。但它亦有一些獨特的特色，例如：對於所接收的郵件有全面的文字索引，以及一個「虛擬資料夾」功能。

Evolution能夠經由它的網路介面及一個名叫[Connector的插件連接至](../Page/Ximian_Connector.md "wikilink")[Microsoft
Exchange
Server](../Page/Microsoft_Exchange_Server.md "wikilink")。而使用*gnome-pilot*可使其與[Palm
Pilot裝置進行同步化](../Page/Palm_Pilot.md "wikilink")，而[Multisync則容許它與](../Page/Multisync_\(software\).md "wikilink")[移动电话及其他](../Page/移动电话.md "wikilink")[PDA進行同步](../Page/个人数码助理.md "wikilink")。

## Evolution 2

Evolution 2，這個於2004年9月推出的新版，具有以下的新功能：

  - 整合連線至[Novell GroupWise](../Page/Novell_GroupWise.md "wikilink")
  - 整合連線至[Microsoft Exchange](../Page/Microsoft_Exchange.md "wikilink")
  - 增強[IMAP帳戶的離線支援](../Page/IMAP.md "wikilink")
  - 無數的[行事曆功能改進](../Page/行事曆.md "wikilink")
  - 支持[S/MIME](../Page/S/MIME.md "wikilink")，增強聯絡人管理
  - 整合[Gaim整時通訊工具](../Page/Gaim.md "wikilink")
  - 增進桌面整合
  - 更加遵守GNOME的[人性化介面指引](../Page/人性化介面指引.md "wikilink")
  - 整合[SpamAssassin的功能](../Page/SpamAssassin.md "wikilink")（with
    user-defined spam score and whitelist rules in `~/.spamassassin/`）

## Windows版的Evolution

在2005年1月，Novell的[Nat Friedman](../Page/Nat_Friedman.md "wikilink")
[在他的博客上](http://nat.org/2005/january/#17-January-2005)
指出該公司正在招募將[GIMP移植至微軟視窗的程式設計師來為Evolution進行同樣的工作](../Page/GIMP.md "wikilink")，最近更公佈
[測試版](http://ftp.gnome.org/pub/gnome/platform/2.14/2.14.2/win32/)。

在這個通告公佈之前，有少數有著相同的目標的計劃已經開始進行（[Evolution for
Windows](http://evolution-win32.sourceforge.net/) being the most
notable），但卻沒有任何一個進入測試階段。

## 參見條目

  - [電郵客戶端列表](../Page/電郵客戶端列表.md "wikilink")
  - [個人資訊管理軟體列表](../Page/個人資訊管理軟體列表.md "wikilink")
  - [電郵客戶端比較](../Page/電郵客戶端比較.md "wikilink")
  - [個人資訊管理軟體比較](../Page/個人資訊管理軟體比較.md "wikilink")

## 外部連結

  - [Evolution計劃官方網址](https://wiki.gnome.org/Apps/Evolution)
  - [Novell的Evolution主頁](http://www.novell.com/zh-cn/products/desktop/features/evolution.html)
  - [Connector](http://www.novell.com/products/connector/)
  - [在視窗上使用Evolution](https://web.archive.org/web/20130703124406/http://www.go-evolution.org/Building_Evolution_on_Windows)
  - [Hong Kong & Macau Novell User Group](http://www.novellughk.org/)
    –非官方網站。供Novell使用者分享、討論的平台。亦有SUSE Linux技術文章及其他花邊新聞

[Category:自由软件](../Category/自由软件.md "wikilink")