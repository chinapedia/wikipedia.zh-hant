**七股黑面琵鷺保護區**，位於[臺南市](../Page/臺南市.md "wikilink")[七股區](../Page/七股區.md "wikilink")。每年10月至翌年4、5月季節性候鳥[黑面琵鷺便會從北方飛到臺南市七股區附近避寒過冬](../Page/黑面琵鷺.md "wikilink")。今幾年，來七股度冬的黑面琵鷺已經有超過千隻的數量。2014年1月18日至19日，全球普查黑面琵鷺總數為2725隻，臺南市即占近半數的1246隻，是全世界黑面琵鷺最重要的度冬地之一，而在10月至翌年4、5月也是遊客欣賞黑面琵鷺的最佳時期。

## 沿革

由於[七股工業區](https://web.archive.org/web/20111009054900/http://web1.tainan.gov.tw/investintainan/CP/12294/cigu.aspx)及[濱南工業區等開發案可能導致黑面琵鷺棲息環境的破壞](../Page/濱南工業區.md "wikilink")，以及遊客的干擾，使得[臺灣催生](../Page/臺灣.md "wikilink")**七股黑面琵鷺保護區**，於2002年10月15日經[農委會公告劃定](../Page/農委會.md "wikilink")634公頃的「**臺南縣曾文溪口黑面琵鷺野生動物重要棲息環境**{{\#tag:ref|其名稱是依行政院農業委員會、臺南縣政府公告為準。為配合2010年12月25日升格[直轄市](../Page/直轄市.md "wikilink")，原臺南縣改制成臺南市，故名稱應更正為「臺南市曾文溪口黑面琵鷺野生動物重要棲息環境」、「臺南市曾文溪口北岸黑面琵鷺動物保護區」。|group=注}}」，其中300公頃為「**臺南縣曾文溪口北岸黑面琵鷺動物保護區**{{\#tag:ref|同-{注}-釋1|group=注}}」」，以保護其生態資源。而在2009年12月25日時，七股黑面琵鷺保護區被併入為[台江國家公園中](../Page/台江國家公園.md "wikilink")，為臺灣的第八座[國家公園](../Page/中華民國國家公園.md "wikilink")。

## 相關書籍

《飛吧！ 七股的黑面仔》（ISBN 9789574349265）：由台灣繪本漫畫家 巫昱宏 繪製

## 註釋

## 參考資料

## 外部連結

  - [台南縣曾文溪口北岸黑面琵鷺動物保護區](https://web.archive.org/web/20100808000130/http://conservation.forest.gov.tw/ct.asp?xItem=7644&ctNode=178&mp=10)
  - [黑面琵鷺生態展示館](https://web.archive.org/web/20120130030345/http://cec.tesri.gov.tw/blackfaced/)
  - [台灣黑面琵鷺保育學會](http://www.bfsa.org.tw//)
  - [全球黑面琵鷺回報系統](http://bfsn.bfsa.org.tw/index.php)

[Category:台南市旅遊景點](../Category/台南市旅遊景點.md "wikilink")
[Category:台南市地理](../Category/台南市地理.md "wikilink")
[Category:台灣自然保留區](../Category/台灣自然保留區.md "wikilink")
[Category:台江國家公園](../Category/台江國家公園.md "wikilink")
[Category:台灣濕地](../Category/台灣濕地.md "wikilink")