**精密國際AWP**（**Accuracy International Arctic Warfare
Police**）為英國精密國際開發的北極作戰（Arctic
Warfare）系列的[狙擊步槍](../Page/狙击步枪.md "wikilink")，為[精密國際AW狙擊步槍針對警察和執法機構用戶生產的改良版](../Page/精密國際AW狙擊步槍.md "wikilink")。

## 设计

精密國際AW（AW為英語Arctic
Warfare的縮寫）的開發主要是為了參與瑞典軍方新型狙擊步槍的競爭，之所以會稱作北極作戰也是因為能夠在極冷的天氣當中發揮最極致的效能。這個槍枝系列的成功也使的產生出許多衍生版本，AWP即為供給警察使用的[槍械](../Page/槍械.md "wikilink")。最有特色的即為基座為黑色，而非亮綠色，而且槍支比其他版本有較短的24吋槍管（610毫米），也移除了槍上的後備[機械瞄具](../Page/機械瞄具.md "wikilink")，只有[7.62×51毫米NATO和](../Page/7.62×51mm_NATO.md "wikilink")[.243溫徹斯特兩種口徑](../Page/.243溫徹斯特.md "wikilink")，但是也有可能與原版一樣裝填[.300
Winchester Magnum](../Page/.300_Winchester_Magnum.md "wikilink")、7mm
Remington Magnum、[.338 Lapua
Magnum子彈](../Page/.338拉普麥格農.md "wikilink")。不過AWP不應該與[AW
AE搞混](../Page/精密國際AW狙擊步槍#AE（Accuracy_Enforcement）.md "wikilink")，而且AE的價格便宜了很多。

## 使用國家和地區

  -   - [溫哥華特警隊](../Page/溫哥華.md "wikilink")\[1\]

  -   - 德國邦警察[特別行動突擊隊](../Page/特別行動突擊隊.md "wikilink")

  -   - [香港警務處](../Page/香港警務處.md "wikilink")[特別任務連](../Page/特別任務連.md "wikilink")

  -   - [特種部隊單位](../Page/特種部隊.md "wikilink")

  -   - [卡賓槍騎兵](../Page/卡賓槍騎兵.md "wikilink")[特別干預組](../Page/特別干預組_\(義大利\).md "wikilink")

  -   - C連

  -   - [維安特勤隊](../Page/維安特勤隊.md "wikilink")
      - [霹靂小組單位](../Page/中華民國特種警察.md "wikilink")

  -
  -   - 警察單位（.243溫徹斯特口徑型）

  -   - [西班牙國民警衛隊](../Page/西班牙國民警衛隊.md "wikilink")

  -   - 警察單位

  -   - 武裝特警單位

## 流行文化

该武器在游戏《[-{zh-hans:反恐精英;zh-hant:絕對武力}-](../Page/絕對武力.md "wikilink")》中作为威力最大的狙击步枪出现，因而为全球的游戏玩家所知，更是一度被很多玩家认为是全球最优秀的狙击步枪之一。

然而，縱使遊戲中該武器被稱作AWP，但現實世界的AWP只有使用7.62子彈或其他同口徑子彈，並不存在使用.338
Magnum的AWP。在《[-{zh-hans:反恐精英;zh-hant:絕對武力}-](../Page/絕對武力.md "wikilink")》中的AWP能夠以一發击殺敵人，但使用7.62子弹的[斯泰爾斥候步槍需要](../Page/斯泰爾斥候步槍.md "wikilink")2發，因此造成了.338
Magnum子弹的性能远比7.62×51毫米NATO子弹性能优秀的错觉。如果無視遊戲中該武器裝填10發彈藥的設定，針對外型和子彈類型而言，其實遊戲中的該武器更接近現實中的[AWM多於AWP](../Page/精密國際AWM狙擊步槍.md "wikilink")。

### 電子遊戲

  - 2007年—《[-{zh-hans:军团要塞2;zh-hant:絕地要塞2}-](../Page/絕地要塞2.md "wikilink")》（Team
    Fortress 2）：命名為「AWP」（AWPer
    Hand），被狙擊手作為更新後可使用的解鎖武器之一所使用，原[-{zh-hans:狙击步枪;
    zh-hant:狙擊槍;}-的](../Page/雷明登700步槍.md "wikilink")《[-{zh-hans:反恐精英：全球攻势;
    zh-hant:絕對武力：全球攻勢;}-](../Page/反恐精英：全球攻势.md "wikilink")》促銷版。

## 參見

  - [AW](../Page/精密國際AW狙擊步槍.md "wikilink")
  - [AWM](../Page/精密國際AWM狙擊步槍.md "wikilink")

## 外部链接

  - [D Boy Gun
    World-AWP](http://firearmsworld.net/britain/ai/aw/awp.htm)
  - [精密國際](http://www.ketmer.com/ai/index.htm)

[es:AWP](../Page/es:AWP.md "wikilink")

[Category:狙擊步槍](../Category/狙擊步槍.md "wikilink")
[Category:栓動式步槍](../Category/栓動式步槍.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:英國槍械](../Category/英國槍械.md "wikilink")

1.  <https://sites.google.com/site/worldinventory/wiw_na_canada>