**李傑**（），[中華民國海軍](../Page/中華民國海軍.md "wikilink")[一級上將退役](../Page/一級上將.md "wikilink")，生於[天津市](../Page/天津市.md "wikilink")，成長於[臺灣省](../Page/臺灣省.md "wikilink")[屏東市](../Page/屏東市.md "wikilink")[空軍](../Page/中華民國空軍.md "wikilink")[眷村](../Page/眷村.md "wikilink")，被視為海軍鷹派將領的第一領導人物與精神領袖，許多海軍高階將領皆是李傑提報，為[中華民國國軍留美將領之一](../Page/中華民國國軍.md "wikilink")，畢業於[海軍官校](../Page/海軍官校.md "wikilink")1963年（52年）班、1994年班。是[中華民國海軍第一位](../Page/中華民國海軍.md "wikilink")[潛艦出身的](../Page/潛艦.md "wikilink")[二級上將](../Page/二級上將.md "wikilink")[總司令](../Page/中華民國海軍司令.md "wikilink")。

曾任[海軍反潛指揮部指揮官](../Page/中華民國潛艦.md "wikilink")、[海軍總司令部參謀長](../Page/國防部海軍司令部.md "wikilink")。在任[海軍艦隊司令部司令期間](../Page/海軍艦隊司令部.md "wikilink")，因应对[1996年臺海危機表現優異](../Page/1996年臺海危機.md "wikilink")，後來高升[海軍總司令](../Page/中華民國海軍司令.md "wikilink")、[參謀總長](../Page/中華民國參謀總長.md "wikilink")、[國防部長](../Page/中華民國國防部部長.md "wikilink")，他在任內對於21世紀期間的[中華民國海軍建設發展](../Page/中華民國海軍.md "wikilink")，無論是在[人事](../Page/人事.md "wikilink")、[戰略或](../Page/戰略.md "wikilink")[軍備](../Page/軍備.md "wikilink")[科技上](../Page/科技.md "wikilink")，皆有著深遠的影響力。今天許多海軍現役將領都是李傑所提拔，上將部分包括[林鎮夷](../Page/林鎮夷.md "wikilink")、[高廣圻](../Page/高廣圻.md "wikilink")、[董翔龍](../Page/董翔龍.md "wikilink")、[陳永康](../Page/陳永康.md "wikilink")、[李喜明等皆是](../Page/李喜明.md "wikilink")。

## 生平

1963年，李傑自[中華民國海軍軍官學校航輪科系畢業後](../Page/中華民國海軍軍官學校.md "wikilink")，即進入海軍服役。1981年進入[海軍參謀大學](../Page/海軍參謀大學.md "wikilink")，1990年再前往[美國海軍](../Page/美國海軍.md "wikilink")[戰爭學院深造](../Page/戰爭學院.md "wikilink")。

1963年－1990年這期間，李傑任[中華民國海軍海獅潛艦兵器長](../Page/中華民國海軍.md "wikilink")、副長。1980年代再任海獅潛鑑與太原、洛陽兩[驅逐艦艦長](../Page/驅逐艦.md "wikilink")。之後歷練226及256艦隊艦隊長，對潛艦發展影響深遠，並獲登入[中華民國潛艦名人榜](../Page/中華民國潛艦名人榜.md "wikilink")。

[美國海軍](../Page/美國海軍.md "wikilink")[戰爭學院進修結業後](../Page/戰爭學院.md "wikilink")，李傑回任海軍，並先後擔任海軍124艦隊艦隊長、反潛部指揮官、總司令部副參謀長、參謀長、艦令部司令、副總司令及[國防部參謀本部副參謀總長等重要隊職主官](../Page/國防部參謀本部.md "wikilink")，並於1997年12月16日晉任[海軍](../Page/海軍.md "wikilink")[二級上將](../Page/二級上將.md "wikilink")。

1999年2月1日李傑接任[中華民國海軍總司令](../Page/中華民國海軍司令.md "wikilink")，任內致力於[中華民國海軍第二代軍艦成軍](../Page/中華民國海軍.md "wikilink")。因表現稱職，2002年2月1日晉陞[海軍](../Page/海軍.md "wikilink")[一級上將並接任](../Page/一級上將.md "wikilink")[中華民國參謀總長](../Page/中華民國參謀總長.md "wikilink")。

2004年5月20日李傑接任[中華民國國防部部長](../Page/中華民國國防部部長.md "wikilink")，並同時獲得[中華民國國軍的次高榮譽](../Page/中華民國國軍.md "wikilink")——[青天白日勳章](../Page/青天白日勳章.md "wikilink")。

2005年3月1日，李傑廢除國軍晚點名，禁唱《我愛中華》。

2007年3月，李傑遭[中國國民黨永久開除黨籍](../Page/中國國民黨.md "wikilink")，原因是執政黨[民主進步黨](../Page/民主進步黨.md "wikilink")「[去蔣化](../Page/去蔣化.md "wikilink")」運動，配合移除軍中[蔣介石銅像一事](../Page/蔣介石.md "wikilink")，且表示「哪一黨執政，我就聽哪一黨的」、「反正國民黨執政想搬回來就搬回來」，而國民黨方面認為蔣介石乃是[國民革命軍之父](../Page/國民革命軍.md "wikilink")，李傑此舉可謂「破壞軍中倫理，趨炎附勢」，因而撤銷李傑黨籍。[中國國民黨](../Page/中國國民黨.md "wikilink")[黃復興黨部主委](../Page/黃復興黨部.md "wikilink")[王文燮](../Page/王文燮.md "wikilink")[二級上將痛批李傑沒有軍人應有的風骨](../Page/二級上將.md "wikilink")，「不尊重蔣公就是影響軍中倫理，若是把過去李傑所犯的錯如柔性政變等都加進來，老早就該開除他了，這次會開鍘是因為基層黨員反彈壓力太大，不得不處理。」因[拉法葉軍購案遭判刑](../Page/拉法葉軍購案.md "wikilink")5年定讞的前[海軍副總司令](../Page/中華民國海軍.md "wikilink")[鄭立中中將也說](../Page/拉法葉軍購案.md "wikilink")：「李傑的作法很不妥，行為很不好，更破壞軍中傳統，他無法認同這個學弟。」但李傑面對以上批評則反嗆：「現在的工作不涉黨派立場，被撤銷黨籍正好。」「沒有政黨色彩其實很輕鬆。」\[1\]

2007年5月21日，隨著內閣的更動，李傑也卸下[國防部長的職務](../Page/國防部長.md "wikilink")，改由[總統府戰略顧問](../Page/總統府戰略顧問.md "wikilink")[李天羽](../Page/李天羽.md "wikilink")[上將接任](../Page/上將.md "wikilink")。

## 注释

## 參見

  - [柔性政變](../Page/柔性政變.md "wikilink")
  - 關振清，《中華民國海軍潛艦部隊之創建》
  - 高智陽，《海獅、海豹潛艇接艦秘辛(原載全球防衛雜誌245期》
  - 高智陽，《潛進荷蘭－劍龍專案秘辛Inside the Taiwan－Netherland Submarine Sale》
  - [中華民國潛艦之友會](../Page/中華民國潛艦之友會.md "wikilink")

|- |colspan="3"
style="text-align:center;"|**[ROC_Ministry_of_National_Defense_Seal.svg](https://zh.wikipedia.org/wiki/File:ROC_Ministry_of_National_Defense_Seal.svg "fig:ROC_Ministry_of_National_Defense_Seal.svg")[中華民國](../Page/中華民國.md "wikilink")[國防部](../Page/中華民國國防部.md "wikilink")**
|-    |-     |- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")[行政院](../Page/行政院.md "wikilink")**
|-

[J傑](../Category/李姓.md "wikilink")
[李](../Category/中華民國海軍一級上將.md "wikilink")
[Category:中華民國參謀總長](../Category/中華民國參謀總長.md "wikilink")
[Category:中華民國國防部部長](../Category/中華民國國防部部長.md "wikilink")
[L李](../Category/中華民國海軍總司令.md "wikilink")
[L李](../Category/中華民國潛艦學校校友.md "wikilink")
[L李](../Category/中華民國海軍軍官學校校友.md "wikilink")
[Category:美國海軍戰爭學院校友](../Category/美國海軍戰爭學院校友.md "wikilink")
[Category:屏東中學校友](../Category/屏東中學校友.md "wikilink")
[Category:台灣戰後天津移民](../Category/台灣戰後天津移民.md "wikilink")
[L](../Category/獲頒授景星勳章者.md "wikilink")
[Category:青天白日勳章獲得者](../Category/青天白日勳章獲得者.md "wikilink")
[L](../Category/被开除中国国民党党籍者.md "wikilink")
[Category:天津人](../Category/天津人.md "wikilink")
[Category:中華民國潛艦出身人物](../Category/中華民國潛艦出身人物.md "wikilink")

1.  [蔣中正銅像爭議 李傑被撤國民黨籍](http://www.epochtimes.com/b5/7/3/10/n1641292.htm)