**日本武道館**（）是[日本的一个为鼓勵傳統的](../Page/日本.md "wikilink")[武道及身心磨練的](../Page/武道.md "wikilink")[大道場而興建的設施](../Page/道場.md "wikilink")，坐落於[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")[北之丸公園](../Page/北之丸公園.md "wikilink")。日本武道館亦是其業主[財團法人的名稱](../Page/財團法人.md "wikilink")。通稱為「**武道館**」。

## 概略

日本武道館是為1964年舉行的[東京奧運會場之一而建設](../Page/1964年夏季奥林匹克運動會.md "wikilink")，因而得名。同年10月3日落成。設計師為[山田守](../Page/山田守.md "wikilink")，採用八角形的設計，屋頂樣貌則仿自[富士山](../Page/富士山.md "wikilink")。門口的「武道舘」匾額，為財團法人日本武道館首任會長[正力松太郎之手筆](../Page/正力松太郎.md "wikilink")。

1964年東京奧運期間，至10月20日共4日間在此舉行[柔道比賽](../Page/柔道.md "wikilink")。之後主要做為柔道、[劍道](../Page/劍道.md "wikilink")、[空手道等](../Page/空手道.md "wikilink")[武道活動場地使用](../Page/武道.md "wikilink")，同時成為[舞蹈](../Page/舞蹈.md "wikilink")、[樂團](../Page/樂團.md "wikilink")、演武會等表演，以及[演唱會與](../Page/演唱會.md "wikilink")[格闘技](../Page/格闘技.md "wikilink")（[職業拳擊](../Page/職業拳擊.md "wikilink")、[職業摔跤](../Page/職業摔跤.md "wikilink")、[K-1](../Page/K-1.md "wikilink")、[PRIDE](../Page/PRIDE.md "wikilink")）的舉行會場、[大學等的大規模](../Page/大學.md "wikilink")[入學禮](../Page/入學禮.md "wikilink")、[畢業禮會場等廣泛使用](../Page/畢業禮.md "wikilink")。

毎年4月29日舉行的[全日本柔道選手權大會](../Page/全日本柔道選手權大會.md "wikilink")（無差別級競賽，日本全國冠軍決定戰）、11月3日[全日本劍道選手權](../Page/全日本劍道選手權.md "wikilink")、11月下旬的[自衛隊音樂節皆在此舉行](../Page/自衛隊音樂節.md "wikilink")。[日本天皇與](../Page/日本天皇.md "wikilink")[首相固定出席的](../Page/日本內閣總理大臣.md "wikilink")[全國戰歿者追悼式也於每年的](../Page/全國戰歿者追悼式.md "wikilink")8月15日（日本[二戰投降日](../Page/日本投降.md "wikilink")）在此地舉行。

## 館内設備等

  - 大道場（[競技場](../Page/競技場.md "wikilink")）上方天井常態掛置[日本國旗](../Page/日本國旗.md "wikilink")。
  - 大體育館舖設木板，作為柔道競技場使用時，會舖設[疊](../Page/疊.md "wikilink")，在演唱會舉行時則放置椅子。在天井的日本國旗在演唱會或活動舉行時會降下。柔道場使用的數百枚疊，收納於體育館的地底下。（建築物的B3F）
  - 一階固定席3199席，2、3階固定席7846席，立見席（3階）480席，競技場區最大2946席，最大共14471席。\[1\]
  - 大體育館亦作其他，小規模的柔道場與劍道場。
  - 各武道的學生連盟事務所與[國際武道大學事務所設於此](../Page/國際武道大學.md "wikilink")。
  - 2020年將作為第32屆夏季奧林匹克運動會柔道與空手道比賽場館，在此之前日本武道館將首次進行改建。

## 音樂活動

由於悠久的歷史（1964年建成。而最早的巨蛋[東京巨蛋於](../Page/東京巨蛋.md "wikilink")1988年才建成），武道館是眾多世界頂尖樂團及歌手演出過，並錄製下「Live
In
Budokan」現場專輯的場地，也因此為全世界所知。故此地有著一種「聖地」及「最高殿堂」的象徵意義，而不單單只是一個體育館，能夠在此地演出是眾多歌手的目標。

  - [披頭四樂隊於](../Page/披頭四樂隊.md "wikilink")1966年6、7月間第一次踏上日本即是在此舉行公演，是第一個在此演出的流行樂樂團。
  - [老虎樂隊在](../Page/老虎樂隊.md "wikilink")1968年3月10日在此公演，是首個在武道館演出的日本樂隊。後1971年1月24日《The
    Tigers Beautiful Concert》告別演唱會亦在此舉行。
  - [齊柏林飛船於](../Page/齊柏林飛船.md "wikilink")1971年9月在此首次來日公演。
  - [深紫於](../Page/深紫.md "wikilink")1972年，1973年在此舉行日本公演。1972年之演出錄製為『Made
    in Japan』現場專輯。
  - [木匠兄妹](../Page/木匠兄妹.md "wikilink")1972年6月2日進行了首次的武道館公演。
  - [Eric
    Clapton](../Page/Eric_Clapton.md "wikilink")1974年10月31日首次來日公演。自此開始至今(2016年)，除1985年，1991年以外（因與披頭四成員[George
    Harrison合作音樂會](../Page/George_Harrison.md "wikilink")），42年來每次巡迴演唱都會來到武道館，總場數破百場。1979年12月3日之公演錄製為『Just
    One Night』現場專輯發表。
  - [皇后樂團](../Page/皇后樂團.md "wikilink")1975年4月19日首次來日公演。
  - [西城秀樹於](../Page/西城秀樹.md "wikilink")1975年11月3日，舉行了首個日本人在武道館的個人演唱會。[尾崎豐原定在](../Page/尾崎豐.md "wikilink")1992年6月16、17日舉行其首個武道館演唱會，但因意外離世而取消。
  - [聲優](../Page/聲優.md "wikilink")[椎名碧流於](../Page/椎名碧流.md "wikilink")1997年2月22及23日在此舉行個人公演，是第一位在此演出的聲優。
  - [法蘭克·辛納屈](../Page/法蘭克·辛納屈.md "wikilink")1985年在此舉行演唱會。
  - [鄧麗君曾在](../Page/鄧麗君.md "wikilink")1985于日本東京武道館開唱，是首位在此場地演出的華人歌手。
  - [楊麗花曾在](../Page/楊麗花.md "wikilink")1983年和2001年于日本東京武道館演出[唐伯虎點秋香和](../Page/唐伯虎點秋香.md "wikilink")[梁山伯與祝英台](../Page/梁山伯與祝英台.md "wikilink")，是首位在此場地演出[歌仔戲的藝人](../Page/歌仔戲.md "wikilink")。
  - [王菲也曾在日本東京武道館開唱](../Page/王菲.md "wikilink")。1999年，在日本武道館舉辦了兩場演唱會，2001年又在同場館舉辦了兩場演唱會。
  - [女子十二樂坊曾在](../Page/女子十二樂坊.md "wikilink")2004年於日本武道館舉行專場音樂會。
  - [周杰倫](../Page/周杰倫.md "wikilink")2008年世界巡迴演唱會日本站，2月16、17日在東京武道館展開連唱兩天的演出，是首位在此場地演出的華人男歌手。
  - [神話在](../Page/神话_\(组合\).md "wikilink")2006年9月24日在日本東京武道館舉行《SHINHWA
    2006 JAPAN TOUR INSPIRATION》演唱會，是首位在此場地演出的韓國團體。
  - [谷山紀章於](../Page/谷山紀章.md "wikilink")2010年5月3日以歌手KISHOW名義和吉他手[飯塚昌明所組成的樂團](../Page/飯塚昌明.md "wikilink")[GRANRODEO在此舉辦成立五周年紀念的武道館演唱會](../Page/GRANRODEO.md "wikilink")，也是第一位在武道館開唱的男性聲優（聲優界內為第六位）。
  - [泰勒·史薇芙特於](../Page/泰勒·史薇芙特.md "wikilink")2011年2月17日舉行[愛的告白世界巡迴演唱會東京站第二場](../Page/愛的告白世界巡迴演唱會.md "wikilink")。
  - [宮野真守於](../Page/宮野真守.md "wikilink")2013年音樂活動屆滿五周年之際，以首位男性聲優SOLO歌手身份成功登上武道館舉辦個人公演。
  - [AKB48姐妹團](../Page/AKB48.md "wikilink")[HKT48於](../Page/HKT48.md "wikilink")2013年4月27日，主流出道第39天初次登上武道館，為[女子偶像團體之紀錄](../Page/女子偶像團體.md "wikilink")（《[AKB48集團臨時總會～黑白分明！～](../Page/AKB48集團臨時總會～黑白分明！～.md "wikilink")》）。
  - [SILENT
    SIREN於](../Page/SILENT_SIREN.md "wikilink")2015年1月17日，以主流出道2年2個月的時間打破了[SCANDAL以](../Page/SCANDAL.md "wikilink")5年7個月創下的紀錄,
    成為女子樂團首次登上武道館的最快紀錄(截至2017年)。
  - [张根硕](../Page/张根硕.md "wikilink") 2015年6月2日-3日 举办 《The Cri Show Ⅲ》
  - [五月天](../Page/五月天.md "wikilink") 2015年8月28、29日舉辦《[Just Rock
    It！就是世界巡迴演唱會](../Page/五月天_Just_Rock_It！就是世界巡迴演唱會.md "wikilink")》，是首位在此場地演出的華人樂團。2017年2月3日、4日举办《Re：DNA》演唱会。2018年5月19日、20日舉辦《[LIFE人生無限公司巡迴演唱會](../Page/人生無限公司巡迴演唱會.md "wikilink")》\[2\]
  - [初音未来](../Page/初音未来.md "wikilink")
    2015年9月4及5日举办《魔法未来2015》演唱会，是首位在日本武道馆举办演唱会的虚拟偶像。
  - [iKON](../Page/iKON.md "wikilink") 2016年2月15及16日舉辦《iKON Japan Tour
    2016》，在日本出道後僅33天便在日本武道館舉辦演唱會，打破史上海外藝人用時最短的記錄
  - ClariS 2017年2月10日舉辦 《ClariS 1st 武道館コンサート～2つの仮面と失われた太陽～》
  - [Versailles](../Page/Versailles.md "wikilink")
    2017年2月14日情人節當天舉辦該樂團首度於日本武道館的演唱會，許多外國人慕名而來，日本內閣總理大臣夫人安倍昭恵也到場支持。
  - [M.S.S Project](../Page/M.S.S_Project.md "wikilink")
    2017年2月18日舉辦《M.S.S Project ～光と闇のファンタジア～
    FINAL》，是第一組以遊戲實況者的身分在武道館表演。
  - [BLACKPINK](../Page/BLACKPINK.md "wikilink") 2017年7月20日舉辦《BLACKPINK
    Premium Debut Showcase》，成為首組尚未在日本出道便於此地舉行公演的海外藝人。
  - [After the Rain](../Page/After_the_Rain.md "wikilink")
    2017年8月9、10日連續兩日舉辦《After the Rain 日本武道館2017
    -Clockwise/Anti-Clockwise-》，是首組以Nico唱見出身在此演出的音樂組合。
  - [Aikatsu\!](../Page/Aikatsu!.md "wikilink")2018年2月27、28日舉辦《Aikatsu
    Music Festa 2018》，亦是Aikatsu歌唱擔當活動卒業前最後專場演唱會
  - [i☆Ris](../Page/i☆Ris.md "wikilink")2017年4月5日舉辦的《i☆Ris 4th
    Anniversary Live\~418\~》是4週年演唱會

### 演出關聯紀錄

  - 史上最多公演歌手 -
    [矢澤永吉](../Page/矢澤永吉.md "wikilink")（總合1位・132回・2014年12月16日現在）
  - 史上最多公演女性歌手 -
    [松田聖子](../Page/松田聖子.md "wikilink")（總合2位・102回・2014年7月7日現在）
  - 史上最多公演樂團 - [THE
    ALFEE](../Page/THE_ALFEE.md "wikilink")（總合3位・86回・2014年12月24日現在）
  - 史上最多公演海外歌手 -
    [埃里克·克萊普頓](../Page/埃里克·克萊普頓.md "wikilink")（總合3位・86回・2014年2月28日現在）
  - 史上最年長公演歌手 - [加山雄三](../Page/加山雄三.md "wikilink")（73歲・2010年）
  - 史上最年長公演海外歌手 - [法蘭克·辛納屈](../Page/法蘭克·辛納屈.md "wikilink")（69歲・1985年）
  - 史上最多日数公演 - [HOUND DOG](../Page/HOUND_DOG.md "wikilink")（15日間・1989年）
  - 史上1日最多公演 - [SMAP](../Page/SMAP.md "wikilink")（6回）
  - 史上最快登上武道館公演女子樂團 - [Poppin'Party](../Page/BanG_Dream!.md "wikilink")
    （一年零六個月）
  - 武道館最多動員数 - [SIAM
    SHADE解散公演](../Page/SIAM_SHADE.md "wikilink")（2002年3月10日）・福山雅治（2009年5月28日・29日・6月4日・5日）（15,000人）
  - 第一个举办见面会的广播节目 -
    [神谷浩史、小野大輔的DearGirl～Stories～](../Page/神谷浩史、小野大輔的DearGirl～Stories～.md "wikilink")
  - 尚未在日本出道的韓國女團即登上武道館舞台 - [BLACKPINK](../Page/BLACKPINK.md "wikilink")
    (2017年8月9日在日出道，出道前的7月20日在武道館舉辦《BLACKPINK Premium Debut Showcase》。）

## 交通方式

### 東日本旅客鐵道

  - [JR_logo_(east).svg](https://zh.wikipedia.org/wiki/File:JR_logo_\(east\).svg "fig:JR_logo_(east).svg")[東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）[JR_JB_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JB_line_symbol.svg "fig:JR_JB_line_symbol.svg")[中央線](../Page/中央·總武緩行線.md "wikilink")[飯田橋車站](../Page/飯田橋車站.md "wikilink")

### 東京地下鐵

  - [Tokyo_Metro_logo_(full).svg](https://zh.wikipedia.org/wiki/File:Tokyo_Metro_logo_\(full\).svg "fig:Tokyo_Metro_logo_(full).svg")[東京地下鐵](../Page/東京地下鐵.md "wikilink")（東京Metro地鐵）[Subway_TokyoTozai.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoTozai.png "fig:Subway_TokyoTozai.png")[東西線](../Page/東西線_\(東京地下鐵\).md "wikilink")[竹橋車站](../Page/竹橋車站.md "wikilink")
  - [Tokyo_Metro_logo_(full).svg](https://zh.wikipedia.org/wiki/File:Tokyo_Metro_logo_\(full\).svg "fig:Tokyo_Metro_logo_(full).svg")[東京地下鐵](../Page/東京地下鐵.md "wikilink")（東京Metro地鐵）[Subway_TokyoTozai.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoTozai.png "fig:Subway_TokyoTozai.png")[東西線](../Page/東西線_\(東京地下鐵\).md "wikilink")[飯田橋車站](../Page/飯田橋車站.md "wikilink")
  - [Tokyo_Metro_logo_(full).svg](https://zh.wikipedia.org/wiki/File:Tokyo_Metro_logo_\(full\).svg "fig:Tokyo_Metro_logo_(full).svg")[東京地下鐵](../Page/東京地下鐵.md "wikilink")（東京Metro地鐵）[Subway_TokyoYurakucho.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoYurakucho.png "fig:Subway_TokyoYurakucho.png")[有樂町線](../Page/有樂町線.md "wikilink")[飯田橋車站](../Page/飯田橋車站.md "wikilink")
  - [Tokyo_Metro_logo_(full).svg](https://zh.wikipedia.org/wiki/File:Tokyo_Metro_logo_\(full\).svg "fig:Tokyo_Metro_logo_(full).svg")[東京地下鐵](../Page/東京地下鐵.md "wikilink")（東京Metro地鐵）[Subway_TokyoNamboku.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoNamboku.png "fig:Subway_TokyoNamboku.png")[南北線](../Page/南北線_\(東京地下鐵\).md "wikilink")[飯田橋車站](../Page/飯田橋車站.md "wikilink")
  - [Tokyo_Metro_logo_(full).svg](https://zh.wikipedia.org/wiki/File:Tokyo_Metro_logo_\(full\).svg "fig:Tokyo_Metro_logo_(full).svg")[東京地下鐵](../Page/東京地下鐵.md "wikilink")（東京Metro地鐵）[Subway_TokyoTozai.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoTozai.png "fig:Subway_TokyoTozai.png")[東西線](../Page/東西線_\(東京地下鐵\).md "wikilink")[九段下車站](../Page/九段下車站.md "wikilink")
  - [Tokyo_Metro_logo_(full).svg](https://zh.wikipedia.org/wiki/File:Tokyo_Metro_logo_\(full\).svg "fig:Tokyo_Metro_logo_(full).svg")[東京地下鐵](../Page/東京地下鐵.md "wikilink")（東京Metro地鐵）[Subway_TokyoHanzomon.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoHanzomon.png "fig:Subway_TokyoHanzomon.png")[半藏門線](../Page/東西線_\(東京地下鐵\).md "wikilink")[九段下車站](../Page/九段下車站.md "wikilink")

### 東京都交通局

  - [PrefSymbol-Tokyo.svg](https://zh.wikipedia.org/wiki/File:PrefSymbol-Tokyo.svg "fig:PrefSymbol-Tokyo.svg")[都營地下鐵](../Page/都營地下鐵.md "wikilink")（都營地鐵）[Subway_TokyoOedo.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoOedo.png "fig:Subway_TokyoOedo.png")[大江戶線](../Page/大江戶線.md "wikilink")[飯田橋車站](../Page/飯田橋車站.md "wikilink")
  - [PrefSymbol-Tokyo.svg](https://zh.wikipedia.org/wiki/File:PrefSymbol-Tokyo.svg "fig:PrefSymbol-Tokyo.svg")[都營地下鐵](../Page/都營地下鐵.md "wikilink")（都營地鐵）[Subway_TokyoShinjuku.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoShinjuku.png "fig:Subway_TokyoShinjuku.png")[新宿線](../Page/新宿線_\(都營地下鐵\).md "wikilink")[九段下車站](../Page/九段下車站.md "wikilink")

## 圖片

<File:Nippon> Budokan 1 Kitanomaru Chiyoda Tokyo.jpg|武道館・九段下側入口
<File:Nippon> Budokan dsc05079.jpg|武道館・正面入口 <File:Nippon> Budokan 3
Kitanomaru Chiyoda Tokyo.jpg|擬寶珠 <File:Nippon> Budoukan AGE Ibamoto
3.png|內部樣貌（演唱會籌備之景） <File:Budokan> sakura.JPG|春景

## 参考文献

## 外部連結

  - [財團法人日本武道館](http://www.nipponbudokan.or.jp/)
  - [築土神社（日本武道館的氏神）](http://www.tsukudo.jp/)

{{-}}

[N](../Category/1964年日本建立.md "wikilink")
[N](../Category/1964年完工的體育場館.md "wikilink")
[N](../Category/1964年夏季奧林匹克運動會運動場.md "wikilink")
[N](../Category/2020年夏季奧林匹克運動會運動場.md "wikilink")
[N](../Category/武道.md "wikilink") [N](../Category/東京體育場地.md "wikilink")
[N](../Category/東京都觀光地.md "wikilink")
[N](../Category/千代田區.md "wikilink")
[N](../Category/日本室內體育場.md "wikilink")
[N](../Category/奧運柔道場館.md "wikilink")
[N](../Category/日本職業摔角場館.md "wikilink")
[N](../Category/日本拳擊場館.md "wikilink")
[N](../Category/演唱會場地.md "wikilink")

1.  [日本武道館建物概要（アリーナ面積・座席表）](http://www.nipponbudokan.or.jp/about/gaiyou.html)
2.