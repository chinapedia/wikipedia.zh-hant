**組曲《NICONICO動畫》**最早是指由[影片分享網站](../Page/影片分享網站.md "wikilink")[NICONICO動畫的使用者](../Page/NICONICO動畫.md "wikilink")（Shimo），選取出NICONICO動畫中受到使用者注目、在站內流行的33首歌曲與配樂，重新編成[歐陸舞曲風的](../Page/歐陸舞曲.md "wikilink")[混合曲](../Page/混合曲.md "wikilink")，之後便出有一系列相同名稱的系列作品，並在NICONICO動畫有其專屬的標題分類。

## 簡介

組曲《NICONICO動畫》系列第一首是在2007年6月5日上載的影片「送給NICONICO動畫中毒者的一首曲子」（），並繼續這項整理工作，續作「組曲《NICONICO動畫》」在2007年6月23日上載至網站。由於將各首樂曲連接得十分巧妙，在同年7、8月間獲得廣大的迴響，有超過1,800,000次的觀賞紀錄、超過3,300,000次的相關意見發表以及超過100,000個用戶將其納入「我的動畫清單」中。

2007年9月9日，原作者再次發表了《NICONICO動畫故事.wav》（）。2008年4月11日，原作者再發表《NICONICO動畫流星群》(「ニコニコ動画流星群」)。2009年6月3日，原作者的新作《七色之NICONICO動畫》（「七色のニコニコ動画」）發表。

  - 2007年6月5日：第1作《**送給NICONICO動畫中毒者的一首曲子**》上載
  - 2007年6月23日：第2作《**組曲“NICONICO動畫”**》上載
  - 2007年9月9日：第3作《**NICONICO動畫故事.wav**》上載（9月18日修正版上載）
  - 2008年4月11日：第4作《**NICONICO動畫流星群**》上載
  - 2008年7月5日：《**NICONICO動畫流星娘**》上載
  - 2009年6月3日：第5作《**七色之NICONICO動畫**》上載
  - 2010年3月19日：《**七色之NICONICO動畫（mobile rainbow mix）**》開始販售
  - 2011年4月23日：《**組曲“NICONICO動畫”改**》上載
  - 2012年4月15日：《**超組曲“NICONICO動畫”**》上載

## 風靡程度

組曲《NICONICO動畫》上載後五日，有另一位使用者將自己唱的《組曲〈NICONICO動畫〉》上傳至網站，讓NICONICO動畫中的「試著唱看看」分類為之沸騰；許多使用者開始上傳各式各樣版本（如演奏版、跳舞版）的組曲《NICONICO動畫》。而原組曲中沒有歌詞的部份，大多使用者便即興將歌詞填入（大多是NICONICO動畫網站中有名的[幻聽歌詞](../Page/幻聽歌詞.md "wikilink")）。

除了由單獨用戶自行演唱外，亦有一些用戶上傳由許多人分別歌唱各段落，並加以編輯的版本，形成一龐大的合唱的影片。其他版本中也有由使用者自行以刻录机、Hatewa、古琴、直笛、鋼琴、小提琴、各種鼓，甚至長笛等樂器演奏，或者是由整個樂團共同表演的樂器版本。這也讓NICONICO動畫「組曲」這分類選項並非只單純是指音樂的分類，而是專門於「組曲《NICONICO動畫》」系列中使用。

《NICONICO動畫》組曲也有被以英語、希伯來語、法語、韓語、中文、泰語、馬來語和菲律賓語等語言翻唱。其中最眾所皆知的版本分別有由C_Chat提出的台灣PTT版本和[國立中央大學學生的中文版本](../Page/國立中央大學.md "wikilink")。特別是台灣PTT版本，為2007年8月2日由C_Chat在其[台灣](../Page/台灣.md "wikilink")[網路論壇](../Page/網路論壇.md "wikilink")[批踢踢的版面中提出計劃](../Page/批踢踢.md "wikilink")，以一個星期的時間來整理眾人合唱《NICONICO動畫》組曲，並上傳到[NICONICO動畫中](../Page/NICONICO動畫.md "wikilink")。這是第一個《NICONICO動畫》組曲並非由日本NICO使用戶創作的影片，也讓許多用戶認識到日本NICO有許多海外用戶瀏覽。

## 使用歌曲

下面為《NICONICO動畫》組曲系列的使用歌曲列表：

### 送給NICONICO動畫中毒者的一首曲子

<table>
<caption>原名：</caption>
<tbody>
<tr class="odd">
<td><p>曲目</p></td>
<td><p>樂曲名稱</p></td>
<td><p>出處</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/:jp:豪血寺一族#新・豪血寺一族_煩悩解放.md" title="wikilink">新・豪血寺一族 -煩惱解放-</a>》插入歌</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS同人專輯</a>「東方乙女囃子」，遊戲《<a href="../Page/東方妖妖夢.md" title="wikilink">東方妖妖夢</a>》<a href="../Page/背景音樂.md" title="wikilink">背景音樂</a>「」的重新編曲</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>[1]</p></td>
<td><p>遊戲《<a href="../Page/:en:Mega_Man_2.md" title="wikilink">洛克人2</a>》背景音樂「Dr.WILY STAGE 1」的重新編曲，在NICONICO上的正式名稱即為此名。（二次創作歌詞「」）</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》插入歌</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td></td>
<td><p>動畫《<a href="../Page/幸運☆星.md" title="wikilink">幸運☆星</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td></td>
<td><p>動畫《<a href="../Page/創聖機械天使.md" title="wikilink">創聖機械天使</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/:jp:ことばのパズル_もじぴったん.md" title="wikilink">语言解谜文字拼词</a>》主题曲</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p><a href="../Page/true_My_Heart.md" title="wikilink">true my heart</a></p></td>
<td><p>遊戲《<a href="../Page/Nursery_Rhyme.md" title="wikilink">Nursery Rhyme</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>kiss my lips</p></td>
<td><p><a href="../Page/佐倉紗織.md" title="wikilink">佐倉紗織所唱歌曲</a></p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>RODEO MACHINE</p></td>
<td><p><a href="../Page/:jp:HALFBY.md" title="wikilink">HALFBY作曲而成的</a><a href="../Page/音樂錄影帶.md" title="wikilink">音樂錄影帶</a></p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>序曲（<a href="../Page/:en:Dragon_Quest.md" title="wikilink">DRAGON QUEST</a>）</p></td>
<td><p>遊戲《<a href="../Page/勇者鬥惡龍.md" title="wikilink">勇者鬥惡龍</a>》主題曲（填入的歌詞是日本國歌<a href="../Page/君之代.md" title="wikilink">君之代の前半部</a>）[2]</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>FINAL FANTASY</p></td>
<td><p>遊戲《<a href="../Page/最終幻想系列.md" title="wikilink">FINAL FANTASY</a>》主題曲（填入的歌詞是日本國歌君之代後半部）</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td></td>
<td><p>遊戲《》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td></td>
<td><p>音樂劇《<a href="../Page/網球王子舞台劇.md" title="wikilink">網球王子</a>》插入歌</p></td>
</tr>
</tbody>
</table>

### 組曲《NICONICO動畫》

<table>
<caption>原名：</caption>
<tbody>
<tr class="odd">
<td><p>曲目</p></td>
<td><p>樂曲名稱</p></td>
<td><p>出處</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>エージェント夜を往く</p></td>
<td><p>遊戲《<a href="../Page/偶像大师.md" title="wikilink">偶像大师</a>》插入歌</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><a href="../Page/晴天愉快.md" title="wikilink">ハレ晴レユカイ</a></p></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="../Page/:jp:イオシスの東方Project音楽アレンジCDの一覧.md" title="wikilink">患部で止まってすぐ溶ける～狂気の優曇華院</a></p></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS同人專輯</a>《東方月燈籠》，遊戲《<a href="../Page/東方永夜抄.md" title="wikilink">東方永夜抄</a>》背景音樂「」的編曲</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>Help me, ERINNNNNN</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="../Page/:jp:瞳の欠片.md" title="wikilink">nowhere</a></p></td>
<td><p>動畫《<a href="../Page/异域天使.md" title="wikilink">异域天使</a>》插入歌</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>クリティウスの牙</p></td>
<td><p>動畫《<a href="../Page/遊戲王怪獸之決鬥.md" title="wikilink">遊戲王怪獸之決鬥</a>》162話插入歌</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>GONG</p></td>
<td><p>由<a href="../Page/JAM_Project.md" title="wikilink">JAM Project所唱的遊戲</a>《<a href="../Page/第3次超級機器人大戰α_終焉之銀河.md" title="wikilink">第3次超級機器人大戰α</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/:jp:スーパーマリオRPG.md" title="wikilink">超級馬利歐RPG</a>》背景音樂、使用者另外自行填入歌詞）</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p><a href="../Page/Butter-Fly.md" title="wikilink">Butter-Fly</a></p></td>
<td><p>動畫《<a href="../Page/數碼寶貝大冒險.md" title="wikilink">數碼寶貝大冒險</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td></td>
<td><p>動畫《<a href="../Page/武裝鍊金.md" title="wikilink">武裝鍊金</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td></td>
<td><p>《<a href="../Page/:en:Mega_Man_2.md" title="wikilink">洛克人2</a>》背景音樂的二次創作樂曲</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td></td>
<td><p>音樂劇《<a href="../Page/網球王子舞台劇.md" title="wikilink">網球王子</a>》插入歌</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p><a href="../Page/:jp:アンインストール_(石川智晶の曲).md" title="wikilink">アンインストール</a></p></td>
<td><p>動畫《<a href="../Page/地球防衛少年.md" title="wikilink">地球防衛少年</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p><a href="../Page/鳥の詩.md" title="wikilink">鳥の詩</a></p></td>
<td><p>遊戲《<a href="../Page/AIR.md" title="wikilink">AIR</a>》主題曲</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p><a href="../Page/you_-Visionen_im_Spiegel.md" title="wikilink">you -Visionen im Spiegel</a></p></td>
<td><p>動畫《<a href="../Page/暮蟬悲鳴時.md" title="wikilink">暮蟬悲鳴時</a>》第二巻・目明し編主題曲</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS同人專輯</a>《東方乙女囃子》，遊戲《<a href="../Page/東方妖妖夢.md" title="wikilink">東方妖妖夢</a>》背景音樂「」的重新編曲</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p><a href="../Page/:jp:ロックマン2_Dr.ワイリーの謎.md" title="wikilink">ロックマン2</a> <a href="../Page/:en:Omoide_wa_Okkusenman!.md" title="wikilink">ワイリーステージ</a>1[3]</p></td>
<td><p>遊戲《<a href="../Page/:en:Mega_Man_2.md" title="wikilink">洛克人2</a>》背景音樂「Dr.WILY STAGE 1」的重新編曲，在NICONICO上的正式名稱即為此名。（二次創作歌詞「」）</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td></td>
<td><p>動畫《涼宮春日的憂鬱》插入歌</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p><a href="../Page/拿去吧！水手服.md" title="wikilink">もってけ!セーラーふく</a></p></td>
<td><p>動畫《<a href="../Page/幸運☆星.md" title="wikilink">幸運☆星</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td></td>
<td><p>遊戲《》主題曲</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p><a href="../Page/創聖大天使_(單曲).md" title="wikilink">創聖のアクエリオン</a></p></td>
<td><p>動畫《<a href="../Page/創聖機械天使.md" title="wikilink">創聖機械天使</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/:jp:ことばのパズル_もじぴったん.md" title="wikilink">语言解谜文字拼词</a>》主题曲</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p>つるぺったん</p></td>
<td><p>收錄於<a href="../Page/Silver_Forest.md" title="wikilink">Silver Forest的同人專輯</a>《<a href="../Page/東方萃奏樂.md" title="wikilink">東方萃奏樂</a>》，遊戲《<a href="../Page/東方永夜抄.md" title="wikilink">東方永夜抄</a>》的背景音樂《竹取飛翔 ～ Lunatic Princess》加上《<a href="../Page/:jp:豪血寺一族#レッツゴー!_陰陽師.md" title="wikilink">レッツゴー！陰陽師</a>》、《<a href="../Page/:Jp:ことばのパズル_もじぴったん.md" title="wikilink">ふたりのもじぴったん</a>》的編曲</p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>Here we go!</p></td>
<td><p>遊戲《<a href="../Page/超級馬利歐.md" title="wikilink">超級馬利歐</a>》背景音樂</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p><a href="../Page/true_My_Heart.md" title="wikilink">true my heart</a></p></td>
<td><p>遊戲《<a href="../Page/Nursery_Rhyme.md" title="wikilink">Nursery Rhyme</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>26</p></td>
<td><p>kiss my lips</p></td>
<td><p><a href="../Page/ave;new.md" title="wikilink">ave;new的</a><a href="../Page/佐倉紗織.md" title="wikilink">佐倉紗織主唱歌曲</a></p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td><p>RODEO MACHINE</p></td>
<td><p><a href="../Page/HALFBY.md" title="wikilink">HALFBY作曲而成的</a><a href="../Page/音樂錄影帶.md" title="wikilink">音樂錄影帶</a></p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p>序曲（DRAGON QUEST）</p></td>
<td><p>遊戲《<a href="../Page/勇者鬥惡龍.md" title="wikilink">勇者鬥惡龍</a>》主題曲（填入的歌詞是日本國歌<a href="../Page/君之代.md" title="wikilink">君之代前半部</a>）[4]</p></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p>FINAL FANTASY</p></td>
<td><p>遊戲《<a href="../Page/最終幻想.md" title="wikilink">FINAL FANTASY</a>》主題曲（填入的歌詞是日本國歌君之代後半部）</p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td></td>
<td><p>遊戲《》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td></td>
<td><p>音樂劇《網球王子》插入歌</p></td>
</tr>
<tr class="odd">
<td><p>32</p></td>
<td><p><a href="../Page/豪血寺一族#レッツゴー!_陰陽師.md" title="wikilink">レッツゴー! 陰陽師</a></p></td>
<td><p>遊戲《<a href="../Page/新・豪血寺一族_-煩惱解放-.md" title="wikilink">新・豪血寺一族 -煩惱解放-</a>》插入歌</p></td>
</tr>
<tr class="even">
<td><p>33</p></td>
<td></td>
<td><p>日本古謠。NICONICO動畫在影片被刪除時所替代播放的歌曲</p></td>
</tr>
</tbody>
</table>

  -
    另外，本首組曲使用的曲子中有著稱為「彈幕曲」、「（日本ACG界）國歌」和「NICONICO動畫代表曲」的曲子。

### NICONICO動畫故事.wav

<table>
<caption>原名：</caption>
<tbody>
<tr class="odd">
<td><p>曲目</p></td>
<td><p>樂曲名稱</p></td>
<td><p>出處</p></td>
</tr>
<tr class="even">
<td><p>0</p></td>
<td><p>イントロ</p></td>
<td><p>將最後面結尾的《》、《》、《》、《》等四首曲子副歌段結合進來的前奏。</p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>ENDLESS RAIN</p></td>
<td><p>J-POP (<a href="../Page/X_JAPAN.md" title="wikilink">X JAPAN</a>)</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/粉雪_(Remioromen單曲).md" title="wikilink">粉雪</a></p></td>
<td><p>日劇《<a href="../Page/一公升的眼淚_(電視劇).md" title="wikilink">一公升的眼淚</a>》插入曲</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>GREEN GREENS</p></td>
<td><p>遊戲《<a href="../Page/星之卡比系列.md" title="wikilink">星之卡比系列</a>》關卡背景音乐（歌詞為二次創作）</p></td>
</tr>
<tr class="even">
<td><p>4-1</p></td>
<td><p>Here we go!</p></td>
<td><p>遊戲《<a href="../Page/超級瑪利歐.md" title="wikilink">超級瑪利歐</a>》背景音乐（歌詞為二次創作）</p></td>
</tr>
<tr class="odd">
<td><p>4-2(3)</p></td>
<td><p>GREEN GREENS</p></td>
<td><p>遊戲《星之卡比系列》關卡背景音乐（歌詞為二次創作）</p></td>
</tr>
<tr class="even">
<td><p>5-1</p></td>
<td><p>SMILES&amp;TEARS</p></td>
<td><p>遊戲《MOTHER2》片尾曲</p></td>
</tr>
<tr class="odd">
<td><p>5-2【6】</p></td>
<td><p>Eight Melodies</p></td>
<td><p>遊戲《MOTHER》主旋律</p></td>
</tr>
<tr class="even">
<td><p>5-3【7】</p></td>
<td></td>
<td><p>遊戲《MOTHER3》主旋律</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>CANDY POP</p></td>
<td><p>J-POP (<a href="../Page/Heartsdales.md" title="wikilink">Heartsdales</a>) NICONICO上有名的《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》之<a href="../Page/MAD.md" title="wikilink">MAD使用曲</a></p></td>
</tr>
<tr class="even">
<td><p>9-1</p></td>
<td><p>GO MY WAY</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9-2(5)</p></td>
<td><p>SMILES&amp;TEARS</p></td>
<td><p>遊戲《MOTHER2》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>10-1</p></td>
<td><p><a href="../Page/拿去吧！水手服.md" title="wikilink">もってけ!セーラーふく</a></p></td>
<td><p>動畫《<a href="../Page/幸運☆星.md" title="wikilink">幸運☆星</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>10-2【11】</p></td>
<td><p>God knows...</p></td>
<td><p>動畫《涼宮春日的憂鬱》插入歌</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/偶像大师.md" title="wikilink">偶像大师</a>》插入歌</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>14-1</p></td>
<td></td>
<td><p>動畫《<a href="../Page/絕望先生.md" title="wikilink">絕望先生</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>14-2(13)</p></td>
<td></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>PRIDE</p></td>
<td><p>《總合格鬥技PRIDE》主題曲 在NICONICO動畫上是以『SUMOU』稱呼的MAD使用曲聞名，此MAD最初其實是<a href="../Page/YouTube.md" title="wikilink">YouTube的投稿動畫</a></p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td></td>
<td><p>ザ・ドリフターズ オチ背景音乐 ニコニコ動画では、『イチローのレーザービームで人類滅亡』と呼ばれるMADの背景音乐</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td></td>
<td><p>特攝《<a href="../Page/蜘蛛人_(特攝).md" title="wikilink">蜘蛛人</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p><a href="../Page/Ievan_Polkka.md" title="wikilink">Ievan Polkka</a></p></td>
<td><p>波蘭民謠（初音未来《甩葱歌》）</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td></td>
<td><p>游戏《<a href="../Page/巫女みこナース.md" title="wikilink">巫女みこナース</a>》主题曲</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td></td>
<td><p>動畫《<a href="../Page/地上最強新娘.md" title="wikilink">地上最強新娘</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS編曲</a>，是遊戲《<a href="../Page/東方花映塚.md" title="wikilink">東方花映塚</a>》背景音乐《お宇佐さまの素い幡》的改編版</p></td>
</tr>
<tr class="odd">
<td><p>22-1</p></td>
<td></td>
<td><p>遊戲《》的片頭曲</p></td>
</tr>
<tr class="even">
<td><p>22-2【23】</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS同人專輯</a>《東方月燈籠》，遊戲《<a href="../Page/東方永夜抄.md" title="wikilink">東方永夜抄</a>》背景音樂《》的編曲）</p></td>
</tr>
<tr class="odd">
<td><p>24-1</p></td>
<td><p>Help me, ERINNNNNN</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>24-2【25】</p></td>
<td></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》古泉一樹角色曲</p></td>
</tr>
<tr class="odd">
<td><p>26-1</p></td>
<td></td>
<td><p>J-POP（<a href="../Page/大塚愛.md" title="wikilink">大塚愛</a>）NICONICO動畫上有名的《<a href="../Page/Fate/stay_night.md" title="wikilink">Fate/stay night</a>》之MAD使用曲</p></td>
</tr>
<tr class="even">
<td><p>26-2【27】</p></td>
<td></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》第0話（放映第1話）片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>26-3</p></td>
<td></td>
<td><p>游戏《<a href="../Page/最终幻想.md" title="wikilink">最终幻想</a>》背景音乐（歌詞是NICONICO動畫上的二次創作）</p></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td></td>
<td><p>音樂劇《<a href="../Page/網球王子.md" title="wikilink">網球王子</a>》劇中歌</p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td></td>
<td><p>Web音樂素材(NICONICO動畫上以「貓鍋」聞名的影片背景音乐，亦为游戏《いりす症候群！》背景音乐之一)</p></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td></td>
<td><p>動畫《<a href="../Page/暮蟬悲鳴時.md" title="wikilink">暮蟬悲鳴時解</a> 》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td></td>
<td><p>動畫《<a href="../Page/暮蟬悲鳴時.md" title="wikilink">暮蟬悲鳴時解</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td></td>
<td><p><a href="../Page/NHK.md" title="wikilink">NHK</a>《<a href="../Page/みんなのうた.md" title="wikilink">みんなのうた</a>》2004年10 - 11月放送曲</p></td>
</tr>
<tr class="even">
<td><p>33-1</p></td>
<td><p>GOLD RUSH</p></td>
<td><p>遊戲《beatmania IIDX14 GOLD》收錄曲</p></td>
</tr>
<tr class="odd">
<td><p>34</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>35</p></td>
<td></td>
<td><p>動畫《<a href="../Page/王牌投手_振臂高揮.md" title="wikilink">王牌投手 振臂高揮</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>36</p></td>
<td><p><a href="../Page/殘酷天使的行動綱領.md" title="wikilink">殘酷天使的行動綱領</a></p></td>
<td><p>動畫《<a href="../Page/新世紀福音戰士.md" title="wikilink">新世紀福音戰士</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>37-1</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/東方紅魔鄉.md" title="wikilink">東方紅魔鄉</a>》背景音乐</p></td>
</tr>
<tr class="odd">
<td><p>37-2(36)</p></td>
<td></td>
<td><p>動畫《<a href="../Page/新世紀福音戰士.md" title="wikilink">新世紀福音戰士</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/東方妖妖夢.md" title="wikilink">東方妖妖夢</a>》背景音乐</p></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p>情熱大陸</p></td>
<td><p><a href="../Page/每日放送.md" title="wikilink">每日放送</a>《<a href="../Page/情熱大陸.md" title="wikilink">情熱大陸</a>》主題音樂</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p>Princess Bride!</p></td>
<td><p>游戏《<a href="../Page/公主新娘.md" title="wikilink">公主新娘</a>》主题曲</p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td><p>Love Cheat!</p></td>
<td><p>游戏《<a href="../Page/いただきじゃんがりあんR.md" title="wikilink">いただきじゃんがりあんR</a>》主题曲</p></td>
</tr>
<tr class="even">
<td><p>42</p></td>
<td><p>Butter-Fly</p></td>
<td><p>動畫《<a href="../Page/數碼寶貝大冒險.md" title="wikilink">數碼寶貝大冒險</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>43</p></td>
<td></td>
<td><p>動畫《<a href="../Page/武裝鍊金.md" title="wikilink">武裝鍊金</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td><p>SKILL</p></td>
<td><p>遊戲《<a href="../Page/超級機器人大戰.md" title="wikilink">第2次超級機器人大戰α</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td></td>
<td><p>動畫《<a href="../Page/创圣的大天使.md" title="wikilink">创圣的大天使</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/语言解谜文字拼词.md" title="wikilink">语言解谜文字拼词</a>》主題曲</p></td>
</tr>
<tr class="odd">
<td><p>47</p></td>
<td></td>
<td><p><a href="../Page/Silver_Forest.md" title="wikilink">Silver Forest將</a>《》、《》、《》組合起來的改編曲</p></td>
</tr>
<tr class="even">
<td><p>48</p></td>
<td><p><a href="../Page/true_My_Heart.md" title="wikilink">true my heart</a></p></td>
<td><p>遊戲《<a href="../Page/Nursery_Rhyme.md" title="wikilink">Nursery Rhyme</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>49</p></td>
<td><p>relations</p></td>
<td><p>遊戲《<a href="../Page/偶像大师.md" title="wikilink">偶像大师</a>》插入曲</p></td>
</tr>
<tr class="even">
<td><p>50</p></td>
<td></td>
<td><p>動畫《<a href="../Page/地球防衛少年.md" title="wikilink">地球防衛少年</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>51</p></td>
<td><p><a href="../Page/鸟之诗.md" title="wikilink">鸟之诗</a></p></td>
<td><p>遊戲《<a href="../Page/AIR.md" title="wikilink">AIR</a>》主題曲</p></td>
</tr>
<tr class="even">
<td><p>52-1</p></td>
<td><p>you</p></td>
<td><p>遊戲《<a href="../Page/暮蟬悲鳴時.md" title="wikilink">暮蟬悲鳴時</a> 目明篇》片尾曲</p></td>
</tr>
<tr class="odd">
<td><p>52-2(50)</p></td>
<td></td>
<td><p>動畫《<a href="../Page/地球防衛少年.md" title="wikilink">地球防衛少年</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>53</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS同人專輯</a>《東方乙女囃子》，遊戲《<a href="../Page/東方妖妖夢.md" title="wikilink">東方妖妖夢</a>》背景音樂「」的重新編曲</p></td>
</tr>
<tr class="odd">
<td><p>53-2(51)</p></td>
<td><p>you</p></td>
<td><p>遊戲《<a href="../Page/暮蟬悲鳴時_目明篇.md" title="wikilink">暮蟬悲鳴時 目明篇</a>》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>54-1</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/洛克人2_威利博士之谜.md" title="wikilink">洛克人2 威利博士之谜</a>》背景音乐「Dr.WILY STAGE 1」（歌詞為二次創作「」）</p></td>
</tr>
<tr class="odd">
<td><p>54-2(53)</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS同人專輯</a>《東方乙女囃子》，遊戲《<a href="../Page/東方妖妖夢.md" title="wikilink">東方妖妖夢</a>》背景音樂「」的重新編曲</p></td>
</tr>
<tr class="even">
<td><p>55</p></td>
<td></td>
<td><p>音樂劇《<a href="../Page/網球王子.md" title="wikilink">網球王子</a>》插入曲</p></td>
</tr>
<tr class="odd">
<td><p>56</p></td>
<td></td>
<td><p>遊戲《新・豪血寺一族 -煩惱解放-》插入歌</p></td>
</tr>
<tr class="even">
<td><p>ED1-1(54)</p></td>
<td><p>ロックマン2 ワイリーステージ1</p></td>
<td><p>遊戲《<a href="../Page/洛克人2_威利博士之谜.md" title="wikilink">洛克人2 威利博士之谜</a>》背景音乐「Dr.WILY STAGE 1」（歌詞為二次創作「」）</p></td>
</tr>
<tr class="odd">
<td><p>ED1-2(48)</p></td>
<td><p><a href="../Page/true_My_Heart.md" title="wikilink">true my heart</a></p></td>
<td><p>遊戲《<a href="../Page/Nursery_Rhyme.md" title="wikilink">Nursery Rhyme</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>ED2-1(53)</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS同人專輯</a>《東方乙女囃子》，遊戲《<a href="../Page/東方妖妖夢.md" title="wikilink">東方妖妖夢</a>》背景音樂《》的重新編曲</p></td>
</tr>
<tr class="odd">
<td><p>ED2-2(22)</p></td>
<td></td>
<td><p>遊戲《》主题曲</p></td>
</tr>
<tr class="even">
<td><p>ED3-1(56)</p></td>
<td></td>
<td><p>遊戲《新・豪血寺一族 -煩惱解放-》插入歌</p></td>
</tr>
<tr class="odd">
<td><p>ED3-2(4)</p></td>
<td><p>Here we go!</p></td>
<td><p>遊戲《<a href="../Page/超級瑪利歐.md" title="wikilink">超級瑪利歐</a>》背景音乐（歌詞為二次創作）</p></td>
</tr>
<tr class="even">
<td><p>ED4-1(55)</p></td>
<td></td>
<td><p>音樂劇《<a href="../Page/網球王子.md" title="wikilink">網球王子</a>》挿入歌</p></td>
</tr>
<tr class="odd">
<td><p>ED4-2(10)</p></td>
<td><p><a href="../Page/拿去吧！水手服.md" title="wikilink">もってけ!セーラーふく</a></p></td>
<td><p>動畫《<a href="../Page/幸運☆星.md" title="wikilink">幸運☆星</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>ED5(56)</p></td>
<td></td>
<td><p>遊戲《新・豪血寺一族 -煩惱解放-》插入歌</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### NICONICO動畫流星群

<table>
<caption>原名：</caption>
<tbody>
<tr class="odd">
<td><p>順序</p></td>
<td><p>歌曲名稱</p></td>
<td><p>出處</p></td>
</tr>
<tr class="even">
<td><p>0</p></td>
<td></td>
<td><p><a href="../Page/NICONICO動畫.md" title="wikilink">NICONICO動畫使用的報時音樂</a>。</p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>STAR RISE</p></td>
<td><p>動畫《<a href="../Page/竹刀少女.md" title="wikilink">竹刀少女</a>》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>動畫《<a href="../Page/幸運☆星.md" title="wikilink">幸運☆星</a>》柊司的角色歌曲</p></td>
</tr>
<tr class="odd">
<td><p>3-1</p></td>
<td></td>
<td><p>動畫《<a href="../Page/萌少女的戀愛時光.md" title="wikilink">萌少女的戀愛時光</a>》的片尾曲</p></td>
</tr>
<tr class="even">
<td><p>3-2(4)</p></td>
<td><p>caramelldansen</p></td>
<td><p>Caramell樂團的樂曲</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/星之卡比系列.md" title="wikilink">星之卡比：超級之星</a>》的背景音乐《》</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>nowhere</p></td>
<td><p>動畫《<a href="../Page/异域天使.md" title="wikilink">异域天使</a>》插入曲</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/獵豹人.md" title="wikilink">猎豹人2</a>》背景音乐</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/Ievan_Polkka.md" title="wikilink">Ievan Polkka</a></p></td>
<td><p>芬蘭民謠（<a href="../Page/初音未来.md" title="wikilink">初音未来</a>《甩葱歌》）</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>動畫《<a href="../Page/偶像宣言.md" title="wikilink">偶像宣言</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>男女</p></td>
<td><p>歌手太郎的樂曲</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>芭蕾舞劇《羅密歐與茱麗葉》第二場‧第十三首</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p><a href="../Page/I&#39;m_lovin&#39;_it.md" title="wikilink">I'm lovin' it</a></p></td>
<td><p>日本<a href="../Page/麥當勞.md" title="wikilink">麥當勞的廣告歌曲</a></p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>動畫《<a href="../Page/神劍闖江湖.md" title="wikilink">神劍闖江湖</a>》背景音乐</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>《》的續作</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/Muv-Luv_Alternative.md" title="wikilink">Muv-Luv Alternative</a>》導入主題曲</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>せら的原創歌曲（遊戲《洛克人2 威利博士之謎》主題背景音乐的二次創作）</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>ryo使用《VOCALOID2 初音未來》製作的原創歌曲</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td></td>
<td><p>動畫《<a href="../Page/CLANNAD.md" title="wikilink">CLANNAD</a>》片尾曲</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/時空之輪.md" title="wikilink">時空之輪</a>》背景音乐</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》長門有希角色歌曲</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>you</p></td>
<td><p>遊戲《<a href="../Page/暮蟬悲鳴時.md" title="wikilink">暮蟬悲鳴時解 目明編</a>》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS的</a><a href="../Page/同人遊戲.md" title="wikilink">同人遊戲</a>《<a href="../Page/東方妖妖夢.md" title="wikilink">東方妖妖夢</a>》背景音乐《》的二次創作版本</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/東方紅魔鄉.md" title="wikilink">東方紅魔鄉</a>》背景音乐</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td></td>
<td><p>IOSYS所製作，在東方Project遊戲系列中多次使用的背景音乐之重新編曲版</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>[5]</p></td>
<td><p>遊戲《洛克人2 威利博士之謎》背景音乐《Dr.WILY STAGE 1》（歌詞是二次創作「」）</p></td>
</tr>
<tr class="even">
<td><p>26-1</p></td>
<td><p>(åh) När ni tar saken i egna händer</p></td>
<td><p>AfterDark的歌曲</p></td>
</tr>
<tr class="odd">
<td><p>26-2(27)</p></td>
<td></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》片尾曲</p></td>
</tr>
<tr class="even">
<td><p>28-1</p></td>
<td><p>God knows...</p></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》插曲</p></td>
</tr>
<tr class="odd">
<td><p>28-2(29)</p></td>
<td><p>JOINT</p></td>
<td><p>動畫《<a href="../Page/灼眼的夏娜.md" title="wikilink">灼眼的夏娜II</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>30-1</p></td>
<td></td>
<td><p>動畫《<a href="../Page/代號基亞斯_反叛的魯路修.md" title="wikilink">Code Geass 反叛的魯路修</a>》第二片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>30-2(31)</p></td>
<td></td>
<td><p>Saka-ROW組的原創歌曲</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td></td>
<td><p><a href="../Page/Perfume.md" title="wikilink">Perfume的歌曲</a></p></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td></td>
<td><p>IOSYS製作的遊戲《東方永夜抄》背景音乐《》重新編曲版</p></td>
</tr>
<tr class="even">
<td><p>34-1</p></td>
<td><p>Help me, ERINNNNNN</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>34-2(35)</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/東方永夜抄.md" title="wikilink">東方永夜抄</a>》背景音乐</p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td><p>Little Busters!</p></td>
<td><p>遊戲《<a href="../Page/Little_Busters!.md" title="wikilink">Little Busters!</a>》片頭曲</p></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td><p>1000%SPARKING!</p></td>
<td><p>動畫《<a href="../Page/魔法老師_(電視)#2006.E5.B9.B4.E5.8B.95.E7.95.AB.md" title="wikilink">涅吉！？(魔法老師第二季)</a>》片頭曲</p></td>
</tr>
<tr class="even">
<td><p>37-2(38)</p></td>
<td></td>
<td><p>音樂劇《<a href="../Page/網球王子.md" title="wikilink">網球王子</a>》劇中歌</p></td>
</tr>
<tr class="odd">
<td><p>37-3(39)</p></td>
<td></td>
<td><p>音樂劇《<a href="../Page/網球王子.md" title="wikilink">網球王子</a>》劇中歌</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/偶像大师.md" title="wikilink">偶像大师</a>》挿入歌</p></td>
</tr>
<tr class="odd">
<td><p>41</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/東方風神錄.md" title="wikilink">東方風神錄</a>》背景音乐</p></td>
</tr>
<tr class="even">
<td><p>42-1</p></td>
<td><p><a href="../Page/true_My_Heart.md" title="wikilink">true my heart</a></p></td>
<td><p>遊戲《》主题曲</p></td>
</tr>
<tr class="odd">
<td><p>42-2(43)</p></td>
<td></td>
<td><p><a href="../Page/Silver_Forest.md" title="wikilink">Silver Forest將遊戲</a>《東方風神錄》的背景音乐《》之重新編曲版，收錄於同人專輯《<a href="../Page/東方蒼天歌.md" title="wikilink">東方蒼天歌</a>》。</p></td>
</tr>
<tr class="even">
<td><p>44</p></td>
<td><p><a href="../Page/YATTA!.md" title="wikilink">YATTA!</a></p></td>
<td><p>隊的歌曲（電視節目『』）</p></td>
</tr>
<tr class="odd">
<td><p>45</p></td>
<td></td>
<td><p>ika使用《VOCALOID2 初音未來》製作的原創歌曲</p></td>
</tr>
<tr class="even">
<td><p>46</p></td>
<td></td>
<td><p>遊戲《》插曲</p></td>
</tr>
<tr class="odd">
<td><p>ED1-0</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>ED1-1</p></td>
<td></td>
<td><p><a href="../Page/IOSYS.md" title="wikilink">IOSYS的</a><a href="../Page/同人遊戲.md" title="wikilink">同人遊戲</a>《<a href="../Page/東方妖妖夢.md" title="wikilink">東方妖妖夢</a>》背景音乐《》的二次創作版</p></td>
</tr>
<tr class="odd">
<td><p>ED1-2</p></td>
<td><p>[6]</p></td>
<td><p>遊戲《洛克人2 威利博士之謎》背景音乐《Dr.WILY STAGE 1》</p></td>
</tr>
<tr class="even">
<td><p>ED1-3</p></td>
<td></td>
<td><p>動畫《<a href="../Page/涼宮春日的憂鬱.md" title="wikilink">涼宮春日的憂鬱</a>》片尾曲</p></td>
</tr>
<tr class="odd">
<td><p>ED1-4</p></td>
<td></td>
<td><p>動畫《<a href="../Page/幸運☆星.md" title="wikilink">幸運☆星</a>》柊司的角色歌曲</p></td>
</tr>
<tr class="even">
<td><p>ED1-5</p></td>
<td></td>
<td><p>ika使用《VOCALOID2 初音未來》製作的原創歌曲</p></td>
</tr>
<tr class="odd">
<td><p>ED1-6</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/星之卡比系列.md" title="wikilink">星之卡比：超級之星</a>》的背景音乐《》</p></td>
</tr>
<tr class="even">
<td><p>ED1-7</p></td>
<td></td>
<td><p>遊戲《<a href="../Page/東方紅魔鄉.md" title="wikilink">東方紅魔鄉</a>》背景音乐</p></td>
</tr>
<tr class="odd">
<td><p>ED1-8</p></td>
<td></td>
<td><p>Silver Forest將遊戲《東方風神錄》的背景音乐《》之重新編曲版。</p></td>
</tr>
<tr class="even">
<td><p>ED2-1</p></td>
<td></td>
<td><p>ika使用《VOCALOID2 初音未來》製作的原創歌曲</p></td>
</tr>
<tr class="odd">
<td><p>ED2-2</p></td>
<td></td>
<td><p>遊戲《》插曲</p></td>
</tr>
<tr class="even">
<td><p>47</p></td>
<td><p><a href="../Page/G弦上的詠嘆調.md" title="wikilink">G弦上的詠嘆調</a></p></td>
<td><p><a href="../Page/約翰·塞巴斯蒂安·巴赫.md" title="wikilink">巴哈的管絃樂組曲第三首</a></p></td>
</tr>
</tbody>
</table>

### 七色之NICONICO動畫

|          |                                                                |                                                                                                                               |
| -------- | -------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| 順序       | 歌曲名稱                                                           | 出處                                                                                                                            |
| 1        | ブラック★ロックシューター                                                  | supercell使用《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [初音未來](../Page/初音未來.md "wikilink")》製作的原創歌曲                           |
| 2        | Heavenly Star                                                  | [元気ロケッツ](../Page/元気ロケッツ.md "wikilink")                                                                                        |
| 3        | Do-Dai                                                         | 遊戲《偶像大師》歌曲                                                                                                                    |
| 4-1      | みｗなｗぎｗっｗてｗきｗたｗｗｗ（篠笛禁断症状L5）                                     | しましまP的原創歌曲                                                                                                                    |
| 4-2【5】   | Under My Skin                                                  | Paffendorf feat. Leyla de Vaar的曲子                                                                                             |
| 6-1      | ナイト・オブ・ナイツ                                                     | 由ビートまりお重新编配的，由「十六夜咲夜」BGM，『東方紅魔鄉』的「月時計　〜 ルナ·ダイアル」與『東方花映塚』BGM「フラワリングナイト」的Remix                                                  |
| 6-2【7】   | ダンシング☆サムライ                                                     | カニミソP使用《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [GACKPOID](../Page/GACKPOID.md "wikilink")》製作的原創歌曲                       |
| 8        | サンドキャニオン                                                       | 遊戲《星之卡比3》BGM                                                                                                                  |
| 9        | スカイハイ                                                          | 遊戲《星之卡比SDX》BGM                                                                                                                |
| 10       | ポップスター                                                         | 遊戲《星之卡比64》BGM                                                                                                                 |
| 11       | Got The Groove                                                 | SM-Trax                                                                                                                       |
| 12       | 亡き王女の為のセプテット                                                   | 《[東方紅魔鄉](../Page/東方紅魔鄉.md "wikilink")》「蕾米莉亞·斯卡雷特」的BGM                                                                         |
| 13       | Bad Apple                                                      |                                                                                                                               |
| 14       | 俺ら東京さ行ぐだ                                                       | [吉幾三](../Page/吉幾三.md "wikilink")                                                                                              |
| 15       | RAINBOW GIRL                                                   | 来自[2ちゃんねる掲示板](../Page/2ちゃんねる.md "wikilink")[ニュース速報(VIP)板的讨论串](../Page/ニュース速報\(VIP\)板.md "wikilink")「作曲できる奴ちょっとこい」(能作曲的家伙赶紧进来) |
| 16       | Starry Sky                                                     | [capsule](../Page/capsule.md "wikilink")                                                                                      |
| 17       | Hello Windows                                                  | ヒゲドライバー的原创歌曲（由[Windows XP的效果音编配而成](../Page/Windows_XP.md "wikilink")）                                                         |
| 18       | [最強パレパレード](../Page/最強パレパレード.md "wikilink")                     | 广播剧『涼宮ハルヒの憂鬱 SOS団ラジオ支部』片头曲                                                                                                    |
| 19       | 空                                                              | CD『THE IDOLM@STER MASTER ARTIST FINALE』收錄曲目                                                                                   |
| 20       | celluloid                                                      | baker使用《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [初音未來](../Page/初音未來.md "wikilink")》製作的原創歌曲                               |
| 21       | 初音ミクの消失                                                        | cosMo(暴走P)使用《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [初音未來](../Page/初音未來.md "wikilink")》製作的原創歌曲                          |
| 22       | [ライオン](../Page/ライオン_\(May'n/中島愛の曲\).md "wikilink")             | 動畫「[超時空要塞 Frontier](../Page/超時空要塞_Frontier.md "wikilink")」主題曲                                                                 |
| 23       | [星間飛行](../Page/星間飛行.md "wikilink")                             | 動畫「[超時空要塞 Frontier](../Page/超時空要塞_Frontier.md "wikilink")」插曲                                                                  |
| 24       | [ニホンノミカタ -ネバダカラキマシタ](../Page/ニホンノミカタ_-ネバダカラキマシタ.md "wikilink") | [矢島美容室](../Page/矢島美容室.md "wikilink")                                                                                          |
| 25       | promise                                                        | [広瀬香美](../Page/広瀬香美.md "wikilink")                                                                                            |
| 26       | [魂のルフラン](../Page/魂のルフラン.md "wikilink")                         | 劇場版動畫『[新世紀福音戰士劇場版：死與新生](../Page/新世紀福音戰士劇場版：死與新生.md "wikilink")』主題曲                                                            |
| 27       | ワールドイズマイン                                                      | supercell使用《VOCALOID2 初音未來》製作的原創歌曲                                                                                            |
| 28       | おてんば恋娘                                                         | 《[東方紅魔鄉](../Page/東方紅魔鄉.md "wikilink")》「琪露諾」的BGM                                                                               |
| 29-1     | TOWN                                                           | 遊戲《偶像大師》背景音樂                                                                                                                  |
| 29-2【30】 | ぽっぴっぽー                                                         | ラマーズP使用《VOCALOID2 初音未來》製作的原創歌曲                                                                                                |
| 31       | 溝ノ口太陽族                                                         | 動畫『[天體戰士](../Page/天體戰士.md "wikilink")』主題曲                                                                                     |
| 32       | [崖の上のポニョ](../Page/崖の上のポニョ_\(シングル\).md "wikilink")              | 劇場版動畫『[崖上的波兒](../Page/崖上的波兒.md "wikilink")』主題曲                                                                                |
| 33       | 伯方の塩                                                           | [伯方塩業TVCM](../Page/伯方塩業.md "wikilink")『伯方の塩』サウンドロゴ /?高城靖雄                                                                     |
| 34       | smooooch・∀・                                                    | ゲーム『[beatmaniaIIDX 16 EMPRESS](../Page/beatmaniaIIDX.md "wikilink")』收錄曲目                                                      |
| 35       | ダブルラリアット                                                       | アゴアニキP使用《VOCALOID2 巡音流香/歌》製作的原創歌曲                                                                                             |
| 36       | ってゐ\! ～えいえんてゐver～                                              | 由秀三(石鹸屋)重新编配的、『[東方永夜抄](../Page/東方永夜抄.md "wikilink")』BGM「シンデレラケージ 〜 Kagome-Kagome」的Remix                                       |
| 37       | 炉心融解                                                           | iroha（sasaki）使用《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [鏡音鈴、連](../Page/鏡音鈴、連.md "wikilink")》製作的原創歌曲                     |
| 38       | おジャ魔女カーニバル                                                     |                                                                                                                               |
| 39       | 青く燃える炎                                                         | 音乐剧『网球王子』插入歌曲                                                                                                                 |
| 40       | ザ・レギュラー                                                        | 音乐剧『网球王子』插入歌曲                                                                                                                 |
| 41       | ハンマー状態                                                         | 游戏『[Donkey kong](../Page/Donkey_kong.md "wikilink")』BGM                                                                       |
| 42       | RED ZONE                                                       | 游戏『beatmaniaIIDX 11 IIDXRED』收錄曲目                                                                                              |
| ED1      | [粉雪](../Page/粉雪_\(レミオロメン\).md "wikilink")                      | J-POP （[レミオロメン](../Page/レミオロメン.md "wikilink")）                                                                                |
| ED2      | Endless Rain                                                   | ENDLESS RAIN （[X JAPAN](../Page/X_JAPAN.md "wikilink")）                                                                       |
| ED3      | [アンインストール](../Page/アンインストール.md "wikilink")                     | 动画片『[地球防卫少年](../Page/地球防卫少年.md "wikilink")』片头曲                                                                                |
| ED4      | [true my heart](../Page/true_My_Heart.md "wikilink")           | 游戏『[Nursery Rhyme](../Page/Nursery_Rhyme.md "wikilink")』片头曲                                                                   |
| ED5      | [レッツゴー\! 陰陽師](../Page/レッツゴー!_陰陽師.md "wikilink")                | 遊戲《[新・豪血寺一族 -煩惱解放-](../Page/新・豪血寺一族_-煩惱解放-.md "wikilink")》插曲                                                                  |
| ED6      | [魔理沙は大変なものを盗んでいきました](../Page/魔理沙は大変なものを盗んでいきました.md "wikilink") | [IOSYS的](../Page/IOSYS.md "wikilink")[同人遊戲](../Page/同人遊戲.md "wikilink")《[東方妖妖夢](../Page/東方妖妖夢.md "wikilink")》背景音乐《》的二次創作版本    |
| ED7      | ふたりのもじぴったん                                                     | 游戏『[ことばのパズル もじぴったん](../Page/ことばのパズル_もじぴったん.md "wikilink")』主题曲 |-\[                                                            |
| ED9      | ロックマン2 ワイリーステージ1                                               | 遊戲《洛克人2 威利博士之謎》背景音乐《Dr.WILY STAGE 1》（歌詞是二次創作「」）                                                                               |
| ED10     | つるぺったん                                                         | 由Silver Forest重新编配的「竹取飛翔」「Let's Go！陰陽師」「ふたりのもじぴったん」的Remix                                                                     |
| ED11     | ハレ晴レユカイ                                                        | 動畫『[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")』片尾曲                                                                               |
| ED12     | ガチャガチャきゅ?と・ふぃぎゅ@メイト                                            | 游戏『[ふぃぎゅ@メイト](../Page/ふぃぎゅ@メイト.md "wikilink")』片头曲                                                                             |
| ED13     | エージェント夜を往く                                                     | 遊戲《偶像大師》插曲                                                                                                                    |
| ED14     | あいつこそがテニスの王子様                                                  | 音乐剧『网球王子』插入歌曲                                                                                                                 |
| ED15     | you                                                            | 遊戲『[暮蟬鳴泣時解 目明編](../Page/暮蟬鳴泣時解_目明編.md "wikilink")』片尾曲                                                                         |
| ED16     | [もってけ\!セーラーふく](../Page/拿去吧！水手服.md "wikilink")                  | 动画片『[幸运☆星](../Page/幸运☆星.md "wikilink")』片头曲                                                                                    |
| ED17     | Help me, ERINNNNNN                                             |                                                                                                                               |
| ED18     | \[\[GO_MY_WAY                                                | |GO MY WAY                                                                                                                    |
| ED19     | [エアーマンが倒せない](../Page/エアーマンが倒せない.md "wikilink")                 | せら的原创歌曲（以游戏『[洛克人2 威利博士之謎](../Page/洛克人2_威利博士之謎.md "wikilink")』为主题的二次創作）                                                        |
| ED20     | メルト                                                            | supercell使用《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [初音未來](../Page/初音未來.md "wikilink")》製作的原創歌曲                           |
| ED21     | Little Busters\!                                               | 遊戲『[Little Busters\!](../Page/Little_Busters!.md "wikilink")』主題曲                                                              |
| ED22     | [God knows...](../Page/God_knows....md "wikilink")             | 動畫『涼宮春日的憂鬱』插曲                                                                                                                 |
| ED23     | チーターマン2 BGM                                                    | 游戏『[CHEETAHMEN 2](../Page/CHEETAHMEN_2.md "wikilink")』 BGM                                                                    |
| ED24     | [創聖のアクエリオン](../Page/創聖のアクエリオン.md "wikilink")                   | 動畫《[創聖的大天使](../Page/創聖的大天使.md "wikilink")》主題曲                                                                                 |
| ED25     | [みくみくにしてあげる♪【してやんよ】](../Page/みくみくにしてあげる♪【してやんよ】.md "wikilink") | ika使用《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [初音未來](../Page/初音未來.md "wikilink")》製作的原創歌曲                                 |
| ED26     | [caramelldansen](../Page/caramelldansen.md "wikilink")         | Caramell                                                                                                                      |
| ED27     | U.N.オーエンは彼女なのか?                                                | 『[東方紅魔鄉](../Page/東方紅魔鄉.md "wikilink")』「芙蘭朵露·斯卡雷特」的BGM                                                                         |
| ED28     | [真赤な誓い](../Page/真赤な誓い.md "wikilink")                           | 動畫『[武裝鍊金](../Page/武裝鍊金.md "wikilink")』主題曲                                                                                     |
| 43       | [Don't say "lazy"](../Page/Don't_say_"lazy".md "wikilink")     | 動畫『[K-ON！輕音部](../Page/K-ON！輕音部.md "wikilink")』片尾曲                                                                             |
| \*       | 時報                                                             | [NICONICO動畫使用的報時音樂](../Page/NICONICO動畫.md "wikilink")。                                                                        |
| 44       | Reach Out To The Truth                                         | 游戏『[Persona 4](../Page/Persona_4.md "wikilink")』BGM                                                                           |

原名：七色のニコニコ動画

最后的部分（ED1～ED28）称作「HEROES」，皆為前四作使用過的曲子。

### 超組曲《NICONICO動畫》

|    |                                                                                                                        |                                                                                                                            |
| -- | ---------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| 曲目 | 樂曲名稱                                                                                                                   | 出處                                                                                                                         |
| 1  | ブラック★ロックシューター                                                                                                          | supercell使用《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [初音未來](../Page/初音未來.md "wikilink")》製作的原創歌曲                        |
| 2  | Heavenly Star                                                                                                          | [元気ロケッツ單曲](../Page/元気ロケッツ.md "wikilink")                                                                                   |
| 3  | STAR RISE                                                                                                              | 動畫《[竹刀少女](../Page/竹刀少女.md "wikilink")》片尾曲                                                                                  |
| 4  | [caramelldansen](../Page/caramelldansen.md "wikilink")                                                                 | Caramell樂曲                                                                                                                 |
| 5  | [nowhere](../Page/:jp:瞳の欠片.md "wikilink")                                                                              | 動畫《[异域天使](../Page/异域天使.md "wikilink")》插入歌                                                                                  |
| 6  | [Ievan Polkka](../Page/Ievan_Polkka.md "wikilink")                                                                     | 芬蘭民謠（[初音未来](../Page/初音未来.md "wikilink")《甩葱歌》）                                                                              |
| 7  | Bad Apple                                                                                                              |                                                                                                                            |
| 8  | 俺ら東京さ行ぐだ                                                                                                               | 歌手[吉幾三第](../Page/吉幾三.md "wikilink")10首單曲                                                                                   |
| 9  | 男女                                                                                                                     | 歌手太郎的樂曲                                                                                                                    |
| 10 | [真赤な誓い](../Page/真赤な誓い.md "wikilink")                                                                                   | 動畫『[武裝鍊金](../Page/武裝鍊金.md "wikilink")』主題曲                                                                                  |
| 11 | エアーマンが倒せない                                                                                                             | 《[洛克人2](../Page/:en:Mega_Man_2.md "wikilink")》背景音樂的二次創作樂曲                                                                  |
| 12 | メルト                                                                                                                    | [初音未来樂曲](../Page/初音未来.md "wikilink")                                                                                       |
| 13 | [創聖のアクエリオン](../Page/創聖のアクエリオン.md "wikilink")                                                                           | 動畫《[創聖的大天使](../Page/創聖的大天使.md "wikilink")》主題曲                                                                              |
| 15 | 炉心融解                                                                                                                   | 《[VOCALOID2](../Page/VOCALOID2.md "wikilink") [鏡音鈴、連](../Page/鏡音鈴、連.md "wikilink")》樂曲                                      |
| 16 | U.N.オーエンは彼女なのか?                                                                                                        | 『[東方紅魔鄉](../Page/東方紅魔鄉.md "wikilink")』「芙蘭朵露·斯卡雷特」的BGM                                                                      |
| 17 | 魂のルフラン                                                                                                                 | 動畫《[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")》片頭曲                                                                            |
| 18 | Promise                                                                                                                | 歌手『広瀬香美』第11首單曲                                                                                                             |
| 19 | [アンインストール](../Page/:jp:アンインストール_\(石川智晶の曲\).md "wikilink")                                                              | 動畫《[地球防衛少年](../Page/地球防衛少年.md "wikilink")》片頭曲                                                                              |
| 20 | you                                                                                                                    | 遊戲『[暮蟬鳴泣時解 目明編](../Page/暮蟬鳴泣時解_目明編.md "wikilink")』片尾曲                                                                      |
| 21 | [魔理沙は大変なものを盗んでいきました](../Page/魔理沙は大変なものを盗んでいきました.md "wikilink")                                                         | [IOSYS的](../Page/IOSYS.md "wikilink")[同人遊戲](../Page/同人遊戲.md "wikilink")《[東方妖妖夢](../Page/東方妖妖夢.md "wikilink")》背景音乐《》的二次創作版本 |
| 22 | [星間飛行](../Page/星間飛行.md "wikilink")                                                                                     | 動畫「[超時空要塞 Frontier](../Page/超時空要塞_Frontier.md "wikilink")」插曲                                                               |
| 23 | [ロックマン2](../Page/:jp:ロックマン2_Dr.ワイリーの謎.md "wikilink") [ワイリーステージ](../Page/:en:Omoide_wa_Okkusenman!.md "wikilink")1\[7\] | 遊戲《[洛克人2](../Page/:en:Mega_Man_2.md "wikilink")》背景音樂「Dr.WILY STAGE 1」的重新編曲，在NICONICO上的正式名稱即為此名。（二次創作歌詞「」）                  |
| 24 | [God knows...](../Page/God_knows....md "wikilink")                                                                     | 動畫《[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")》插曲                                                                             |
| 25 | [ハレ晴レユカイ](../Page/晴天愉快.md "wikilink")                                                                                  | 動畫《[涼宮春日的憂鬱](../Page/涼宮春日的憂鬱.md "wikilink")》片尾曲                                                                            |
| 26 | みくみくにしてあげる♪                                                                                                            | 《VOCALOID2 初音未來》歌曲                                                                                                         |
| 27 | [レッツゴー\! 陰陽師](../Page/豪血寺一族#レッツゴー!_陰陽師.md "wikilink")                                                                  | 遊戲《》插曲                                                                                                                     |

原名：超組曲『ニコニコ動画』

## 其他

2007年12月26日由[Lantis發售的動畫](../Page/Lantis.md "wikilink")《[幸運☆星](../Page/幸運☆星.md "wikilink")》混音曲集「」其中一首「」是為了向組曲《NICONICO動畫》致敬而創作\[8\]。2008年3月5日，同公司再出版同類型歌曲CD「Lantis組曲」。

## 參見

  - [NICONICO動畫](../Page/NICONICO動畫.md "wikilink")
  - [Lantis組曲](../Page/:ja:ランティス組曲_feat.Nico_Nico_Artists.md "wikilink")

## 註釋

## 參考資料

  -  ISBN 978-4883806638

## 外部連結

  - [組曲《NICONICO動畫》](https://www.nicovideo.jp/watch/sm500873) -
    NICONICO動畫（需要註冊帳號）
  - [組曲《NICONICO動畫》、台灣人大合唱（PTT）](https://www.nicovideo.jp/watch/sm746146)
    - NICONICO動畫（需要註冊帳號）
  - [中央大學歌謠祭「組曲《NICONICO動畫》」](https://www.nicovideo.jp/watch/sm1323730)
    - NICONICO動畫（需要註冊帳號）
  - [NICONICO動畫『裏組曲』(衍生作品之一)](https://www.nicovideo.jp/watch/sm2529813)
    - NICONICO動畫（需要註冊帳號）
  - [ニコニコ組曲　原曲ver](https://www.youtube.com/watch?v=aR-kTGD5VZc)-組曲《NICONICO動畫》(YouTube)

[Category:NICONICO動畫](../Category/NICONICO動畫.md "wikilink")
[Category:2007年音樂](../Category/2007年音樂.md "wikilink")

1.  動画投稿者在NICONICO上的正式名稱即為此名。

2.  演唱版組曲填入的歌詞是日本國歌[君之代](../Page/君之代.md "wikilink")，台灣PTT版本為[中華民國國旗歌](../Page/中華民國國旗歌.md "wikilink")，中文版本為[中華民國國歌](../Page/中華民國國歌.md "wikilink")，希伯來文版本為[希望曲](../Page/希望曲.md "wikilink")。

3.
4.
5.  動畫最後的歌曲列表內是以「CAST "MUSIC LIST"」標記

6.
7.
8.  [](http://www.lantis.jp/new-release/data.php?id=57feeeb0de0f18e45a7033e21c6d2e8c)