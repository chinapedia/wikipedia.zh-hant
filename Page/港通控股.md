**港通控股有限公司**（簡稱**港通**，前稱**香港隧道有限公司**）()，為一間作[交通](../Page/交通.md "wikilink")[基建項目投資的控股公司](../Page/基建.md "wikilink")。

港通於1965年成立，擁有興建及經營[香港](../Page/香港.md "wikilink")[紅磡海底隧道之](../Page/紅磡海底隧道.md "wikilink")[專營權](../Page/專營權.md "wikilink")。該[專營權至](../Page/專營權.md "wikilink")1999年終止。及後，港通透過一間聯營公司[香港運輸物流及管理有限公司](../Page/香港運輸物流及管理有限公司.md "wikilink")（前稱[香港隧道及高速公路管理有限公司](../Page/香港隧道及高速公路管理有限公司.md "wikilink")），根據與[香港政府續簽之](../Page/香港政府.md "wikilink")「管理、營運和維修保養」合約，為[紅磡海底隧道提供管理服務](../Page/紅磡海底隧道.md "wikilink")。

港通連同其全資附屬公司持有[香港](../Page/香港.md "wikilink")[西區隧道有限公司](../Page/西區隧道有限公司.md "wikilink")50%股權。其公司擁有由1993年起至2023年止興建及經營[西區海底隧道之](../Page/西區海底隧道.md "wikilink")[專營權](../Page/專營權.md "wikilink")。

於2008年，港通收購[大老山隧道有限公司](../Page/大老山隧道有限公司.md "wikilink")39.5%實際權益。其公司擁有由1988年至2018年興建及經營[大老山隧道之](../Page/大老山隧道.md "wikilink")[專營權](../Page/專營權.md "wikilink")，目前已經終止。

另外，港通持有[香港駕駛學院有限公司](../Page/香港駕駛學院.md "wikilink")70%股權。[香港駕駛學院在香港經營三間特定之](../Page/香港駕駛學院.md "wikilink")[駕駛學校](../Page/駕駛學校.md "wikilink")。港通並在一間擁有[快易通有限公司半數權益之公司持有](../Page/快易通.md "wikilink")70%股權。快易通提供之電子收費設施覆蓋[香港](../Page/香港.md "wikilink")11條不同收費[道路及](../Page/道路.md "wikilink")[隧道](../Page/隧道.md "wikilink")。

而港通之控股股東為[渝太地產集團有限公司](../Page/渝太地產集團有限公司.md "wikilink")()。

## 參考資料

## 外部連結

  - [港通控股](http://www.crossharbour.com.hk)

[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市綜合企業公司](../Category/香港上市綜合企業公司.md "wikilink")
[Category:香港公司](../Category/香港公司.md "wikilink")
[Category:1965年成立的公司](../Category/1965年成立的公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")