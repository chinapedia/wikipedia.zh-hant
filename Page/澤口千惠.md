**澤口千惠**是日本的女性[聲優](../Page/聲優.md "wikilink")，隸屬於[Across
Entertainment事務所](../Page/Across_Entertainment.md "wikilink")，日本東京都出身，[血型A型](../Page/血型.md "wikilink")。

丈夫是同為聲優的[野島健兒](../Page/野島健兒.md "wikilink")，兩人間有兩個小孩（頭胎：女兒；第二胎：男兒）。

## 經歷

  - 過去是[青二Production旗下的聲優](../Page/青二Production.md "wikilink")，使用的藝名是「**石橋千惠**」，後因結婚與生產讓活動暫停一段時間，並把事務所換成[Production
    baobab](../Page/Production_baobab.md "wikilink")，同時也把藝名改為「****」，幾個月後才又把藝名改成現在的澤口千惠；目前的事務所為Across
    Entertainment。

<!-- end list -->

  - 代表作為[動畫](../Page/動畫.md "wikilink")《[新IQ博士](../Page/IQ博士.md "wikilink")》的丁可美、《[羅德斯島戰記-英雄騎士傳-](../Page/羅德斯島戰記-英雄騎士傳-.md "wikilink")》的妮絲、《[開花天使](../Page/開花天使.md "wikilink")》的Q比、《[雙胞胎公主](../Page/不可思議星球的雙胞胎公主.md "wikilink")》第二季的丹芭·林；[遊戲](../Page/遊戲.md "wikilink")《[命運傳奇](../Page/命運傳奇.md "wikilink")》系列的莉莉斯·艾爾隆（Lilith
    Aileron）、《純愛物語2》的、《[闇影之心](../Page/闇影之心.md "wikilink")》系列的愛麗斯·艾略特（Alice
    Elliot）等。

<!-- end list -->

  - 其妹是音樂人[石橋優子](../Page/石橋優子.md "wikilink")，其弟石橋哲也是日本年輕[搞笑團體](../Page/搞笑.md "wikilink")「」的成員。

<!-- end list -->

  - 2004年和野島健兒、石橋優子、音樂人朋友鈴木利宗四人組成朗讀與音樂團體「**BELOVED**」（目前活動停止中）。

<!-- end list -->

  - 2007年和石橋優子、音樂人朋友野上朝生三人組成團體「**CHARMS**」。

<!-- end list -->

  - 除了聲優工作外，也在聲優雜誌《hm3》撰寫文章，或是擔任其他Live演出的製作人員等。

## 演出作品

### 電視動畫

  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（女大學生）
  - [新IQ博士](../Page/怪博士與機器娃娃.md "wikilink")（丁可美）<span style="font-size:smaller;">（，1997\~1999年）</span>
  - [羅德斯島戰記-英雄騎士傳-](../Page/羅德斯島戰記-英雄騎士傳-.md "wikilink")（妮絲）<span style="font-size:smaller;">（1998年）</span>
  - 聖光明女子學院（學生）<span style="font-size:smaller;">（，1998年）</span>
  - [南海奇皇](../Page/南海奇皇.md "wikilink")（近松友子）<span style="font-size:smaller;">（1998\~1999年）</span>
  - [開花天使](../Page/開花天使.md "wikilink")（Q比）<span style="font-size:smaller;">（，1998\~1999年）</span>
  - [魅影巨神](../Page/魅影巨神.md "wikilink")（）<span style="font-size:smaller;">（，1999\~2000年</span>）
  - [格鬥小霸王](../Page/格鬥小霸王.md "wikilink")（女性）<span style="font-size:smaller;">（，1999\~2000年）</span>
  - [爆裂戰士戰藍寶](../Page/爆裂戰士戰藍寶.md "wikilink")（）<span style="font-size:smaller;">（，2000年）</span>
  - [法布爾老師是名偵探](../Page/法布爾老師是名偵探.md "wikilink")（女支持者、麗莎、小裁縫）<span style="font-size:smaller;">（2000年）</span>
  - [新戀愛白書](../Page/新戀愛白書.md "wikilink")（新田菜緒）<span style="font-size:smaller;">（2000年）</span>
  - [勝負師傳說](../Page/勝負師傳說.md "wikilink")（莉莎）<span style="font-size:smaller;">（2000\~2001年）</span>
  - [十二國記](../Page/十二國記.md "wikilink")（里家的少女）<span style="font-size:smaller;">（2002\~2003年）</span>
  - [聖槍修女](../Page/聖槍修女.md "wikilink")（老太婆）<span style="font-size:smaller;">（2003\~2004年）</span>
  - [花右京女侍隊La
    Verite](../Page/花右京女侍隊.md "wikilink")（母親）<span style="font-size:smaller;">（2004年）</span>
  - （）（媽媽）<span style="font-size:smaller;">（2006年）</span>
  - [夢使者](../Page/夢使者.md "wikilink")（[办公室女职员](../Page/办公室女职员.md "wikilink")、少年）<span style="font-size:smaller;">（2006年）</span>
  - [雙胞胎公主第二季](../Page/不可思議星球的雙胞胎公主.md "wikilink")（丹芭·林老師）<span style="font-size:smaller;">（，2006\~2007年）</span>
  - [完美小姐進化論](../Page/完美小姐進化論.md "wikilink")（校醫、井澤）<span style="font-size:smaller;">（2006\~2007年）</span>
  - [獸神演武](../Page/獸神演武.md "wikilink")（林鈴）<span style="font-size:smaller;">（2007\~）</span>
  - [古代王者恐龍王第二季
    翼龍傳說](../Page/古代王者恐龍王.md "wikilink")（蘇菲亞）<span style="font-size:smaller;">（2008年）</span>

### 遊戲

  - [未来零点](../Page/未来零点.md "wikilink")（Future zero）
  - 闇影之心系列（Shadow Hearts）
      - [闇影之心](../Page/闇影之心.md "wikilink")（愛麗斯·艾略特）（2001年）
      - [闇影之心2](../Page/闇影之心2.md "wikilink")（愛麗斯·艾略特）（2004年）
  - [傳奇系列](../Page/傳奇系列.md "wikilink")
      - [命運傳奇](../Page/命運傳奇.md "wikilink")（莉莉斯·艾爾隆，僅PS2版）（1997年）
      - [命運傳奇2](../Page/命運傳奇2.md "wikilink")（莉莉斯·艾爾隆）（2002年）
      - [經典傳奇 Vol.1](../Page/經典傳奇_Vol.1.md "wikilink")（莉莉斯·艾爾隆）（2002年）
      - [幻想傳奇PSP版](../Page/幻想傳奇.md "wikilink")（莉莉斯·艾爾隆）（2006年）
  - 純愛物語2（True Love Story 2）（）（1999年）
  - [聖騎士之戰2：前奏曲](../Page/聖騎士之戰.md "wikilink")（Guilty Gear 2:
    Overture）（Valentine）（2007年）

### 發行CD

  - 單曲「」（1998年4月22日）

### 旁白

  - 日本電視台節目「」週二檔「」

### 配音

  - [失蹤現場](../Page/失蹤現場.md "wikilink")（Amber）

## 外部連結

  - [Production
    baobab事務所時期的官方檔案](https://web.archive.org/web/20071006001106/http://pro-baobab.jp/ladies/sawaguchi_r/)

  - [CHARMS](http://chie-sawaguchi.com/)

  - [BELOVED
    CLUB](https://web.archive.org/web/20080314005611/http://www.kotokana.com/)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:旁白](../Category/旁白.md "wikilink")