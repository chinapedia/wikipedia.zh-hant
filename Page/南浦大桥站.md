[NanPuBridgeStation.JPG](https://zh.wikipedia.org/wiki/File:NanPuBridgeStation.JPG "fig:NanPuBridgeStation.JPG")
**南浦大桥站**位于[上海](../Page/上海.md "wikilink")[黃浦區](../Page/黃浦區.md "wikilink")[中山南路](../Page/中山南路_\(上海\).md "wikilink")[陆家浜路](../Page/陆家浜路.md "wikilink")，[南浦大桥附近](../Page/南浦大桥.md "wikilink")，为[上海轨道交通4号线的地下车站](../Page/上海轨道交通4号线.md "wikilink")，于2007年12月29日启用。由于穿越[南浦大桥](../Page/南浦大桥.md "wikilink")[桩基](../Page/桩基础.md "wikilink")，本站是上海轨道交通网络中采用[双层侧式站台结构的车站](../Page/侧式叠式站台.md "wikilink")，地下一层为站厅层，地下二层为内圈列车站台层，地下三层为外圈列车站台层。

由于2003年7月1日在[浦西](../Page/浦西.md "wikilink")[董家渡路附近发生的](../Page/董家渡路.md "wikilink")[严重事故](../Page/2003年上海地铁4号线董家渡段建设事故.md "wikilink")，令工程延误，本站未与4号线其它车站一并启用。

## 公交换乘

18、43、43区间、45、64、65、66、66区间、89、109、144、251、303、305、306、318、324、327、801、802、868、869、910、915、928、931

## 车站出口

  - 1号口：陆家浜路东侧，中山南路北
  - 2号口：中山南路北侧，海潮路西
  - 3号口：陆家浜路西侧，中山南路北

[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以建築物命名的鐵路車站](../Category/以建築物命名的鐵路車站.md "wikilink")