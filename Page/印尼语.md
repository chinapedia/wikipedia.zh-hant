**印尼语**（），又稱為**印尼文**，即印尼化的[马来语](../Page/马来语.md "wikilink")[廖内方言](../Page/廖内群岛省.md "wikilink")，是[印度尼西亚的官方语言](../Page/印度尼西亚.md "wikilink")。属[马来-波利尼西亚语族](../Page/马来-波利尼西亚语族.md "wikilink")。全世界大约有4280万人使用这种语言，还有1.55亿人将印尼语作为第二语言\[1\]。印尼語和馬來語非常相似。這兩種語言的祖先本來是東南亞的非官方通用語。因此，有關地區的人在加以學習之下，都很容易掌握這種語言。此外，隨着印尼移民四散，以及到國外工作的傭工，不少說印尼語的人都散播到其他地區去。

大多數印尼人能夠流利地把印尼語當第二語言使用。這些人在家裡或是當地社區使用當地語言（例如[米南加保語](../Page/米南加保語.md "wikilink")、[爪哇語等等](../Page/爪哇語.md "wikilink")），而在正式的教育機構、全國性媒體及其他種類的溝通傳播上使用印尼語。一些[東帝汶人也會說印尼語](../Page/東帝汶.md "wikilink")。

## 歷史

印尼語是一種被規範後的[馬來語](../Page/馬來語.md "wikilink")（Malay
language）。在規範化以前，馬來語已經在印尼群島以通用語的身分流通了數世紀。受1928年的Sumpah
Pemuda（青年誓言）影響，[印尼獨立時定印尼語為官方語言](../Page/印尼獨立.md "wikilink")。儘管印尼語與[馬來西亞官方語言](../Page/馬來西亞.md "wikilink")[馬來西亞語](../Page/馬來西亞語.md "wikilink")（Bahasa
Malaysia）非常類似，然而前者在發音及詞彙上受[荷蘭語影響](../Page/荷蘭語.md "wikilink")，所以與马来语還是有些差異。

印尼的人口中僅有7%以印尼語當作母語（主要在[雅加達附近](../Page/雅加達.md "wikilink")），但其使用人口卻超過二億人，通用於全國，但他們印尼語的流利程度也不一。這是因為對於一個擁有超過300個地區語言的國家來說，印尼語是聯繫印尼不同地區的重要溝通工具，主要用於印尼商業、行政、各級教育以及各大眾傳播媒體上。

然而，大多數以印尼語當母語的印尼人都承認，他們在日常生活中很少使用標準印尼語：人們可以在報紙、書本上讀到，也可以在電視新聞上聽到標準印尼語，然而在日常會話中人們卻很少使用這種正式的印尼語。雖然這個現象在世界上大多數語言中都存在（例如[英語口語就不一定與標準書面語相符](../Page/英語.md "wikilink")），然而以語法及詞彙而言，印尼語口語的「正確度」比較起正式書面印尼語而言相當的低。這可能是因為大多印尼人都傾向於在使用印尼語時混入當地語言的詞彙（如[爪哇語](../Page/爪哇語.md "wikilink")、[巽他語](../Page/巽他語.md "wikilink")、[漢語](../Page/漢語.md "wikilink")）。結果，大多數外國人造訪印尼時都常常聽到各式各樣口音的印尼語。這現象因為俚語的使用而更明顯，而且在城市裡尤為明顯。現實中一個典型的例子便是前[印尼總統](../Page/印尼總統.md "wikilink")[蘇哈托](../Page/蘇哈托.md "wikilink")——他演講時總會混入一些[爪哇方言](../Page/爪哇.md "wikilink")。

## 語音系統

### 語音體系

下列為現代印尼語的[元音及](../Page/元音.md "wikilink")[輔音之](../Page/輔音.md "wikilink")[音位架構](../Page/音位.md "wikilink")。

<table>
<caption><strong>元音</strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/前元音.md" title="wikilink">前元音</a></p></th>
<th><p><a href="../Page/央元音.md" title="wikilink">央元音</a></p></th>
<th><p><a href="../Page/後元音.md" title="wikilink">後元音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/閉元音.md" title="wikilink">閉元音</a>(高)</p></td>
<td></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/半閉元音.md" title="wikilink">半閉元音</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/半開元音.md" title="wikilink">半開元音</a></p></td>
<td><p>()</p></td>
<td><p> </p></td>
<td><p>()</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/開元音.md" title="wikilink">開元音</a>(低)</p></td>
<td></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

<table>
<caption><strong>輔音</strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/唇音.md" title="wikilink">唇</a></p></th>
<th><p><a href="../Page/舌尖音.md" title="wikilink">舌尖</a></p></th>
<th><p><a href="../Page/齒齦後音.md" title="wikilink">齒齦後</a></p></th>
<th><p><a href="../Page/硬腭音.md" title="wikilink">硬腭</a></p></th>
<th><p><a href="../Page/軟腭音.md" title="wikilink">軟腭</a></p></th>
<th><p><a href="../Page/喉音.md" title="wikilink">喉</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></td>
<td></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/塞音.md" title="wikilink">塞音</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></td>
<td><p>()</p></td>
<td><p> ()</p></td>
<td><p>()</p></td>
<td><p> </p></td>
<td><p>()</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/滑音.md" title="wikilink">滑音</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/近音.md" title="wikilink">近音</a></p></td>
<td></td>
<td><p> </p></td>
<td><p> </p></td>
<td></td>
<td><p> </p></td>
<td><p> </p></td>
</tr>
</tbody>
</table>

## 印尼語語法

  -

### 字序

基本語序：[主詞─動詞─受詞](../Page/SVO.md "wikilink")

[形容詞](../Page/形容詞.md "wikilink")、[指示代名詞及](../Page/指示代名詞.md "wikilink")[所有格代名詞接在名詞之後](../Page/所有格代名詞.md "wikilink")，採用[後位修飾法](../Page/中心語前置.md "wikilink")，類如[法語](../Page/法語.md "wikilink")。

[介詞為](../Page/介詞.md "wikilink")[前置詞](../Page/前置詞.md "wikilink")，置於其所修飾之名詞前。

名詞一般不依性、[數](../Page/數_\(語法\).md "wikilink")、[格等進行變化](../Page/格_\(語法\).md "wikilink")，但有時名詞以[疊詞的方式構造](../Page/疊詞.md "wikilink")[眾數形](../Page/眾數.md "wikilink")。

### 代名詞省略

#### 礼貌用语

在敘述句和疑問句中省略代名词，表示禮貌或尊敬，比如店里的禮貌店員，她會避免將代名詞和「請問」這兩種表達法夾在一塊使用：

| 代名詞的省略 (主詞 及 賓語) | 對應中文字詞       | 中文套句            |
| ---------------- | ------------ | --------------- |
| Bisa *dibantu?*  | 可以 + *被服務*嗎? | (我)可以為(你)*服務*嗎? |

#### 省略主语

当主语未知或並不重要时，或在文句中有隱含主语时的簡略表達，比如回答关于房屋购买的詢問：

| 代名詞的省略 (隱含主詞)                           | 對應中文字詞            | 中文套句          |
| --------------------------------------- | ----------------- | ------------- |
| Rumah ini *dibeli* lima tahun yang lalu | 房子 這間 + *被買走* 五年前 | 這間房子在五年前*被買走* |

關於主動語氣和被動語氣(也是和字序有所關聯)表達上的選擇使用來看，這個選擇到底是要從主動者及被動者的關係來出發，且要相當底依靠語言的內在風格及內文敘述，如此總的結合來表達出適當的語句。

## 雙言現象

雙言現象(diglossia，另譯[雙層語言或文白分離](../Page/雙層語言.md "wikilink"))是一社會語言學術語，意指在一個語言社群裡同時存在兩種不同語言變體的狀態。這兩種語體的正式性與社會地位有所不同；擁有較高威望者稱為H形式，威望較低者稱為L形式。H形式用於正式場合，例如學校教育、政府文書、傳播媒體；L形式則用於日常生活。\[2\]

印尼語對印尼語的稱呼是bahasa Indonesia (印尼的語言)，而印尼語的H形式常被稱為bahasa resmi
(官方語言)或bahasa baku (標準語)，L形式則稱為 bahasa sehari-hari
(日常語)或bahasa non-baku (非標準語)。需要注意的是，印尼語的H和L形式之間沒有清楚的界線。

## 拼寫改革

印尼語在1972年正式頒布了[Ejaan Yang
Disempurnakan](../Page/:id:Article_Name.md "wikilink")拼寫規範，這個規範是為了統一印尼與馬來西亞兩國的文字而設計的。雖然新制已經廣為使用，但不少人仍然慣用舊制來拼寫自己的名字。下表列出印尼語舊制與新制比較\[3\]：

| 舊拼寫法 | 新拼寫法 |
| ---- | ---- |
| tj   | c    |
| dj   | j    |
| j    | y    |
| nj   | ny   |
| sj   | sy   |
| ch   | kh   |

## 成語與諺語

  -
    *Ada gula, Ada semut.*

為印尼語文學用語：“哪兒有[糖](../Page/糖.md "wikilink")，哪兒就有[螞蟻](../Page/螞蟻.md "wikilink")”，字義為「當那兒有些好事（糖），那兒就有些人（螞蟻）來搶拿好處」。這句話相似於中文的成語：“不是蒼蠅不沾糞”、“有钱能使鬼推磨”(广东话为有钱使得鬼推磨)。

## 關連項目

  -
  - [明古魯語](../Page/明古魯語.md "wikilink")

## 参考文献

### 引用

### 来源

  - Bellwood, Peter. *The Austronesians*, 1995.
  - Blagden, C. O. *Two Malay letters from Ternate in the Moluccas,
    written in 1521 and 1522*, 1930.
  - Ricklefs, M. C. « Banten and the Dutch in 1619 : six early "Pasar
    Malay" letters », dans *Classical Civilisations of South East Asia*,
    2002
  - Аракин, Владимир Дмитриевич. "Индонезийский язык", М. — 1965
  - "Грамматика индонезийского языка", ред. Аракин В. Д., М. — 1972

## 外部链接

  - [使用Google自动将印尼语翻译成简体中文](http://arquivo.pt/wayback/20080218151543/http://translate.google.com/translate_t#auto%7Czh-CN)
  - [使用Google自动将印尼语翻译成繁体中文](http://arquivo.pt/wayback/20080218151543/http://translate.google.com/translate_t#auto%7Czh-TW)

[\*](../Category/印度尼西亚语.md "wikilink")
[Category:主謂賓語序語言](../Category/主謂賓語序語言.md "wikilink")

1.
2.
3.