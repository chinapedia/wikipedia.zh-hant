**元仁**（1224年十一月二十日至1225年四月二十日）是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")。這個時代的[天皇是](../Page/天皇.md "wikilink")[後堀河天皇](../Page/後堀河天皇.md "wikilink")、[鎌倉幕府之](../Page/鎌倉幕府.md "wikilink")[執權為](../Page/執權.md "wikilink")[北條泰時](../Page/北條泰時.md "wikilink")。

## 改元

  - 貞應三年十一月二十日（1224年12月31日）改元元仁
  - 元仁二年四月二十日（1225年5月28日）改元嘉祿

## 出處

《[周易](../Page/周易.md "wikilink")》

## 出生

## 逝世

## 大事記

## 紀年、西曆、干支對照表

|                                    |                                |                                |
| ---------------------------------- | ------------------------------ | ------------------------------ |
| 元仁                                 | 元年                             | 二年                             |
| [儒略曆日期](../Page/儒略曆.md "wikilink") | 1224年                          | 1225年                          |
| [干支](../Page/干支.md "wikilink")     | [甲申](../Page/甲申.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [嘉定](../Page/嘉定_\(宋寧宗\).md "wikilink")（1208年正月至1224年十二月）：[宋](../Page/南宋.md "wikilink")—[宋寧宗趙擴之年號](../Page/宋寧宗.md "wikilink")
      - [寳慶](../Page/寳慶.md "wikilink")（1225年正月至1227年十二月）：宋—[宋理宗趙昀之年號](../Page/宋理宗.md "wikilink")
      - [天開](../Page/天開.md "wikilink")（1205年至1225年）：[大理](../Page/大理國.md "wikilink")—段智祥之年號
      - [建嘉](../Page/建嘉.md "wikilink")（1211年至1224年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李旵之年號](../Page/李旵.md "wikilink")
      - [天彰有道](../Page/天彰有道.md "wikilink")（1224年至1225年）：李朝—[李天馨之年號](../Page/李天馨.md "wikilink")
      - [正大](../Page/正大.md "wikilink")（1224年正月至1231年十二月）：[金](../Page/金朝.md "wikilink")—[金哀宗完顏守緒之年號](../Page/金哀宗.md "wikilink")
      - [大同](../Page/大同_\(蒲鮮萬奴\).md "wikilink")（1224年正月至1233年九月）：金時期—[蒲鮮萬奴之年號](../Page/蒲鮮萬奴.md "wikilink")
      - [乾定](../Page/乾定.md "wikilink")（1223年十二月至1226年七月）：[西夏](../Page/西夏.md "wikilink")—夏獻宗[李德旺之年號](../Page/李德旺.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129

[Category:13世纪日本年号](../Category/13世纪日本年号.md "wikilink")
[Category:1220年代日本](../Category/1220年代日本.md "wikilink")