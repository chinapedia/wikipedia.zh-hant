**希伯·道斯特·柯蒂斯**（，），美国天文学家。

## 生平

柯蒂斯1872年出生于[美国](../Page/美国.md "wikilink")[密执安州的马斯基根](../Page/密执安州.md "wikilink")，1893年在[密执安大学获得](../Page/密执安大学.md "wikilink")[硕士学位](../Page/硕士.md "wikilink")，后在[加利福尼亚州教授](../Page/加利福尼亚州.md "wikilink")[拉丁语和](../Page/拉丁语.md "wikilink")[希腊语](../Page/希腊语.md "wikilink")。1896年他成为[天文学教授](../Page/天文学.md "wikilink")，于1898年进入[利克天文台工作](../Page/利克天文台.md "wikilink")，1902年获得[弗吉尼亚大学天文学的](../Page/弗吉尼亚大学.md "wikilink")[博士学位](../Page/博士.md "wikilink")。

根据范·马南（Adriaan van
Maanen,1884—1946）的观测结果，柯蒂斯认为观测到的旋涡星云是远在[银河系以外](../Page/银河系.md "wikilink")，与银河系相似的恒星系统，这一点与美国天文学家[哈罗·沙普利坚持的银河系是宇宙中的主要天体的观点不同](../Page/哈罗·沙普利.md "wikilink")。1920年4月26日，美国国家科学院在[华盛顿举办了一场著名的辩论](../Page/华盛顿.md "wikilink")，史称“[沙普利-柯蒂斯之争](../Page/沙普利-柯蒂斯之争.md "wikilink")”。双方分别就各自的观点进行了时间为半个小时的报告。由于柯蒂斯具有良好的口才，多数人认为他在这场争论中略微占了上风。后来的观测表明柯蒂斯的观点是基本正确的。

柯蒂斯发现，在[M31的附近观测到大量的新星](../Page/M31.md "wikilink")，显示它们与M31有物理上的联系，并且这些新星的亮度比其他新星暗很多。柯蒂斯由此计算出M31的距离约为100k秒差距，并从角大小估算出M31的尺度约为3k秒差距，与当时认为银河系的大小相近。后来的研究表明，柯蒂斯错误地将[新星与M](../Page/新星.md "wikilink")31中的[超新星相混淆](../Page/超新星.md "wikilink")，使得M31的距离被低估了5倍。

## 参见

  - [沙普利-柯蒂斯之争](../Page/沙普利-柯蒂斯之争.md "wikilink")
  - [哈罗·沙普利](../Page/哈罗·沙普利.md "wikilink")

## 外部链接

  - [](http://antwrp.gsfc.nasa.gov/diamond_jubilee/debate20.html)

[C](../Category/美国天文学家.md "wikilink")
[Category:密西根大學校友](../Category/密西根大學校友.md "wikilink")
[Category:維吉尼亞大學校友](../Category/維吉尼亞大學校友.md "wikilink")