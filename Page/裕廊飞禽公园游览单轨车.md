**裕廊飞禽公园游览单轨车**是[新加坡](../Page/新加坡.md "wikilink")[裕廊飞禽公园内的一条](../Page/裕廊飞禽公园.md "wikilink")[单轨](../Page/单轨.md "wikilink")，也是世界上唯一的一条贯通了[大鸟笼的轨道系统](../Page/:en:aviary.md "wikilink")。整个游览线设有三个车站，分别是主站(Main
Station)，鹦鹉站(Lory Station)和瀑布站(Waterfall
Station)。系统有三辆列车，每辆车头有一名驾驶员，全车设座位，不设站位。

游览单轨车系統已於2012年4月1日停止運作，並由與[新加坡動物園](../Page/新加坡動物園.md "wikilink")（Singapore
Zoo）相同的電車系統取代。

## 车票

标价为每人4[新加坡元](../Page/新加坡元.md "wikilink")，一票限乘一次，乘客可在主站搭乘。

[Category:新加坡地铁](../Category/新加坡地铁.md "wikilink")
[Category:新加坡公园](../Category/新加坡公园.md "wikilink")
[Category:1991年啟用的鐵路線](../Category/1991年啟用的鐵路線.md "wikilink")
[J](../Category/铁路环线.md "wikilink")