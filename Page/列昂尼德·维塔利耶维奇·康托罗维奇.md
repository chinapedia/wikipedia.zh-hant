**列昂尼德·维塔利耶维奇·坎托罗维奇**（[俄语](../Page/俄语.md "wikilink")：****，），出生於聖彼得堡，[苏联经济学家](../Page/苏联.md "wikilink")、数学家，1920年代末创立[线性规划](../Page/线性规划.md "wikilink")，1975年获[诺贝尔经济学奖](../Page/诺贝尔经济学奖.md "wikilink")。

[K](../Category/诺贝尔经济学奖获得者.md "wikilink")
[K](../Category/俄羅斯/蘇聯諾貝爾獎獲得者.md "wikilink")
[K](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:苏联经济学家](../Category/苏联经济学家.md "wikilink")
[Category:苏联数学家](../Category/苏联数学家.md "wikilink")
[Category:聖彼得堡國立大學校友](../Category/聖彼得堡國立大學校友.md "wikilink")
[Category:聖彼得堡人](../Category/聖彼得堡人.md "wikilink")
[Category:安葬於新聖女公墓者](../Category/安葬於新聖女公墓者.md "wikilink")
[Category:苏联诺贝尔奖得主](../Category/苏联诺贝尔奖得主.md "wikilink")