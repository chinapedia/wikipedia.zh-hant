[Ensign_Kiyoshi_Ogawa_hit_Bunker_Hill_(new).png](https://zh.wikipedia.org/wiki/File:Ensign_Kiyoshi_Ogawa_hit_Bunker_Hill_\(new\).png "fig:Ensign_Kiyoshi_Ogawa_hit_Bunker_Hill_(new).png")少尉，1945年5月11日驾机击中美军碉堡山号航空母艦（见下图）\]\]
[USS_Bunker_Hill_hit_by_two_Kamikazes.jpg](https://zh.wikipedia.org/wiki/File:USS_Bunker_Hill_hit_by_two_Kamikazes.jpg "fig:USS_Bunker_Hill_hit_by_two_Kamikazes.jpg")在1945年沖繩之役艦橋遭到自殺攻擊，共有389人喪生，造成該艦退出戰鬥回珍珠港整修。\]\]
**特別攻擊隊**（），最有名的一支为特别攻击队通称为「**神風特攻隊**（）」\[1\]、「**神风突击队**」、「**神風敢死隊**」，由[日本海軍中將](../Page/日本海軍.md "wikilink")[大西瀧治郎首倡](../Page/大西瀧治郎.md "wikilink")，是在[第二次世界大战末期](../Page/第二次世界大战.md "wikilink")[日軍在](../Page/日軍.md "wikilink")[中途島海戰失败后](../Page/中途島海戰.md "wikilink")，为了对抗[美国海軍强大的优势](../Page/美国海軍.md "wikilink")，挽救其战败的局面，利用日本人的[武士道精神](../Page/武士道.md "wikilink")，按照「一人、一机、一弹换一舰」的要求，对美国海軍舰艇编队、登陆部队及固定的集群目标实施的[自杀式攻擊的特別攻擊隊](../Page/自杀式恐怖袭击.md "wikilink")。這種行為廣泛地用於二戰後期的戰場上，皆因日本的兵力、武器裝備、補給物資均遜於盟軍，日軍於是利用自殺式襲擊，以最少資源獲取最高破壞力。

神风敢死队的战果不同方面的数据不同，但至少47艘盟军舰船被击沉，300多艘被击伤。近4,000名神风敢死队员丧生，其中有24%的飞机击中目标。神风自杀机本质上是飞行员制导航空炸弹，飞机上满载[炸药和航空燃油](../Page/炸药.md "wikilink")，飞行员的使命是驾驶飞机撞击敌方舰船，因此命中率远高于传统炸弹，破坏力也更大。即使在飞机损坏的情况下仍然能够命中目标。攻击对象主要是盟军舰船，尤其是[航空母舰](../Page/航空母舰.md "wikilink")。

在几次重大失利后，1944年10月神风自杀机投入使用。此时日军丧失了[制空权](../Page/制空权.md "wikilink")，有经验的飞行员也伤亡殆尽。国内的工业生产也无法满足战争的需要，但日本军政府不愿意投降。神风自杀机的目的是破坏美军[航空母舰的飞行甲板使之作战能力瘫痪](../Page/航空母舰.md "wikilink")。

除了神风自杀机，神风攻击还包括[自杀潜艇](../Page/自杀潜艇.md "wikilink")、[自杀鱼雷](../Page/自杀鱼雷.md "wikilink")、[自杀快艇](../Page/震洋特攻隊.md "wikilink")、[自杀潜水员](../Page/自杀潜水员.md "wikilink")。纳粹德国也建立了，没有像神风攻击队这样大规模使用。大规模的神风攻击得益于日军的[武士道精神](../Page/武士道.md "wikilink")：为了忠诚和荣誉，直到战死，絕不投降。

2014年2月4日日本[鹿儿岛县](../Page/鹿儿岛县.md "wikilink")[南九州市的](../Page/南九州市.md "wikilink")“[知览特攻和平会馆](../Page/知览特攻和平会馆.md "wikilink")”向[联合国教科文组织总部递交申请书](../Page/联合国教科文组织.md "wikilink")，要求将“神风特攻队”的队员[遗书](../Page/遗书.md "wikilink")、[信件等](../Page/信件.md "wikilink")333件物品，列入“[世界记忆遗产名录](../Page/世界记忆遗产.md "wikilink")”\[2\]。而在6月12日[联合国教科文组织日本国内委员会公布了](../Page/联合国教科文组织.md "wikilink")2015年世界记忆遗产的申报名单，其中不包括“神风特攻队”队员的遗书。但申遗方表示，他们原本计划兩年后继续申请\[3\]，后来在2015年4月日本方面宣布，再次为神风特攻队队员的遗书和信件等资料，申报世界记忆遗产。日本方面最晚将在6月提交申请书\[4\]。

## 名稱由來

[KairyuuSuicideboat.jpg](https://zh.wikipedia.org/wiki/File:KairyuuSuicideboat.jpg "fig:KairyuuSuicideboat.jpg")

「[神風](../Page/神風_\(颱風\).md "wikilink")」的起名来源于[元朝皇帝](../Page/元朝.md "wikilink")[元世祖](../Page/元世祖.md "wikilink")[忽必烈的](../Page/忽必烈.md "wikilink")[元军侵日战争](../Page/元军侵日战争.md "wikilink")。元朝军队1274年和1281年两次对日本东征，都因为海上突如其来的[台风](../Page/台风.md "wikilink")，导致元朝的舰队损失，使得东征告吹。日本人從此將此風稱為「神風」。二戰中「神風敢死隊」即出自這個典故。日本人认为是[應神天皇](../Page/應神天皇.md "wikilink")（[八幡大菩薩](../Page/八幡大菩薩.md "wikilink")）的[靈魂掀起的](../Page/靈魂.md "wikilink")“神风”击退了元军。日本也逃脱了有可能被元王朝灭国的命运。

由於「神風」為大西中將所發起的首次特攻任務的隊名（由其參謀[豬口力平提出](../Page/豬口力平.md "wikilink")），並在1944年[雷伊泰灣海戰中一舉成名](../Page/雷伊泰灣海戰.md "wikilink")，後世遂將所有特攻隊皆冠以「神風」之名；但實際上，每支特攻隊皆有自己的隊名，「神風」並不是其總稱。事实上陆军的富岳特别攻击队和万朵特别攻击队是同样于1944年10月21日组建的。

## 起源

自殺攻擊敵軍目標的戰術據說起源於[日俄戰爭](../Page/日俄戰爭.md "wikilink")\[5\]，當時日軍採取這種戰術攻擊俄軍的效果不錯，遂被記載下來。

最早有組織的空中神風特攻隊出自1944年的[雷伊泰灣海戰](../Page/雷伊泰灣海戰.md "wikilink")。美軍打算在雷伊泰灣登陸[菲律賓](../Page/菲律賓.md "wikilink")，這恐會切斷日本從南洋輸送的燃料與物資的供應鏈，因此日軍認為必須擊敗盟軍在此之行動。但駐菲的日軍第一航空隊受命支援海軍時僅有40架軍機，實力薄弱，根本無可能完成任務。司令長官[大西瀧治郎中將在](../Page/大西瀧治郎.md "wikilink")10月19日的軍事會議上指出，除了把軍機裝填炸藥撞擊美軍航母，讓對方癱瘓幾週，拖延時間外，根本沒有其他辦法。於是他在10月底从軍事学校中招募一批23名由他親自訓練，成绩優秀並自願加入的飛行員學徒。

[Youngest_kamikaze_only_17_years_old.jpg](https://zh.wikipedia.org/wiki/File:Youngest_kamikaze_only_17_years_old.jpg "fig:Youngest_kamikaze_only_17_years_old.jpg")的隊員，平均年齡17歲。\]\]

大西瀧治郎曾詢問這些當時大約17歲的隊員是否願意為國犧牲，他們回應表示十分願意加入。之後，大西瀧治郎詢問上尉[關行男](../Page/關行男.md "wikilink")，是否願意帶領此史無前例的神風特攻隊。據聞當時23歲，剛剛新婚四個月的關行男閉起雙眼，低頭沉思了十多秒後才說出「請讓我去帶領他們」（另有一说是他在考虑一晚之後才接受的）。世界上第一個神風特攻隊小組就是這樣誕生的，而關行男則成為了特攻隊中「敷島隊」的隊長，並在自殺式攻擊中戰死，無法跟自己的妻子再見一面。

1944年10月21日，總人數約24人的隊員組成海軍**神風特別攻擊隊**，下設大和、敷島、朝日、山櫻四個小隊，各駕一機參與有史以來首次的特攻作戰。由於此次行動成功擊沉兩艘美國[航空母艦](../Page/航空母艦.md "wikilink")，「神風特攻隊」遂一舉成名，並成為往後日軍常用的戰術之一。

## 戰果

由於自殺式飛机能高速飛行，在飛行員的操控下，它相当于一颗[精确制导炸弹](../Page/精确制导武器.md "wikilink")，爆炸效果非常不错，一架自殺式飛机如果击中目标有可能炸沉美军一艘[驱逐舰甚至](../Page/驱逐舰.md "wikilink")[航空母舰](../Page/航空母舰.md "wikilink")。这也就成了日后日本军部为何大规模运用自杀飞机进行攻击的原因之所在。

然而因為僅能破壞船艦上層結構；無法像魚雷般容易擊沉船艦，因此僅能擊沉商船改裝航艦。而較大的正規航艦，如[約克鎮級](../Page/約克鎮級航空母艦.md "wikilink")、[企業級](../Page/企業號航空母艦_\(CV-6\).md "wikilink")、[艾賽克斯級僅被重創](../Page/艾賽克斯級航空母艦.md "wikilink")，沒有沉沒。

在这种攻击方式的出现初期，给美军军舰造成了一定损失。但在美军加强防空火力后，这些攻击就鲜有成功的战例。到戰爭末期，由于日本缺乏汽油，燃料仅能以酒精混合代替，其战斗机性能已经远远落后于美军，难以在空战中取胜。

[雷伊泰灣海戰](../Page/雷伊泰灣海戰.md "wikilink")「神風特攻隊」擊沉美軍兩艘[航空母艦](../Page/航空母艦.md "wikilink")，協助日本軍艦擊沉四艘美軍[驅逐艦](../Page/驅逐艦.md "wikilink")，似乎收到成效。後來此戰術又用於次年[沖繩島戰役](../Page/沖繩島戰役.md "wikilink")，以及本土防衛戰。但由於美軍數量已經居絕對優勢，後來短期速成的特攻人員難以接近美軍軍艦，效果也逐漸不彰。

## 相關爭議

神風特攻隊的成員包含一些不知生死為何物，但具有狂热军国主义思想的日本以及其殖民地的青年。神風特攻隊在军中、民间招募军民青年，当时甚至包括日本的殖民地如[臺灣以及](../Page/臺灣.md "wikilink")[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")，皆曾有年輕人参加。在进行简单的飞行训练后，让其驾驶只有单程燃料的飞机满载炸藥起飞，抵達美海军军舰上空后，以自杀攻击的方式撞向军舰。內中或者有像關行男那樣，在軍國主義與自己的生命之間作過一番掙扎的較年長軍人。但也有报道说部份神風特攻隊队员是被迫的，尤其是在战争后期，指挥官甚至命令优秀的飞行员一并充当[敢死队](../Page/敢死队.md "wikilink")。

[鳥飼行博研究室的网页中指出](../Page/鳥飼行博.md "wikilink")，至今仍有人認為特攻隊是特攻少年兵及學徒兵志願發生的，如海军的航空特攻战死者军官级别（少尉候补生以上）战死769名中648名是飞行预备学生。但其實特攻隊乃1944年秋[莱特湾海战前的](../Page/莱特湾海战.md "wikilink")[日軍高層的方針](../Page/日軍.md "wikilink")。他們強求志願送死，年紀輕輕便要接受為祖國、家人，甚至天皇與神的榮耀和家人的榮誉去送死的命運，因此即使心中有不願意也必須默默接受。

有不少日本指挥官及部分飛行員，包括王牌[岩本彻三在内](../Page/岩本彻三.md "wikilink")，表示了对特攻的质疑。

[MXY7_Ohka_Cherry_Blossom_Baka_Ohka-10.jpg](https://zh.wikipedia.org/wiki/File:MXY7_Ohka_Cherry_Blossom_Baka_Ohka-10.jpg "fig:MXY7_Ohka_Cherry_Blossom_Baka_Ohka-10.jpg")\]\]
關於部分神風隊員參加任務並非完全本於自由意願的事實，可以從参加者基本都是很年轻的新召集来航校的并非形成足够强的自我意志的预备士官上看出来。需要说明的是根据日本资料，有很多回忆称特攻队员携带伞具而且因不可抗力取消任务是能返航的。冲绳战役时一半的特攻队员都在出击后因故取消任务返航。上級對因故取消任務（如敵蹤消失）的特攻員們，會動員同儕加強心理壓力，務使堅定執行下一次必死任務的意志。但即管如此，也仍然有神风队员拒绝了出击的少数例子\[6\]。

## 设备

[Ohka_carried_under_the_belly_of_a_Betty_of_721st_Naval_Air_Group.jpg](https://zh.wikipedia.org/wiki/File:Ohka_carried_under_the_belly_of_a_Betty_of_721st_Naval_Air_Group.jpg "fig:Ohka_carried_under_the_belly_of_a_Betty_of_721st_Naval_Air_Group.jpg")\]\]

神風特攻隊所用的飞机多由轻型轰炸机或战斗机改装，设备简陋，攻擊力弱，但如以自殺式的方法撞向敵方軍艦，卻有非同小可的破壞力。在每次進攻中，大部份的神風特攻隊飛行員為國捐躯，務求為日軍帶來最大的利益。

從1945年起，由於有經驗的飛行員大量戰死短缺，且戰機減少、油料極為-{困}-乏。日軍開始採用了多種新式特攻戰術。主要是开始製造多款專為神風特攻隊而設計的自殺式武器，其中最出名的是“[樱花导弹](../Page/MXY-7櫻花特別攻擊機.md "wikilink")”和“[回天魚雷](../Page/回天.md "wikilink")”。
[Kamikaze_pilots_personal_equipment.jpg](https://zh.wikipedia.org/wiki/File:Kamikaze_pilots_personal_equipment.jpg "fig:Kamikaze_pilots_personal_equipment.jpg")中的裝備\]\]

## 命運

根據電視頻道[Discovery的專題](../Page/Discovery.md "wikilink")，[大西瀧治郎等人將特攻任務常規化的企圖](../Page/大西瀧治郎.md "wikilink")，並非僅在於「一人、一機、一艦」的犧牲與交換，因為即使能維持[萊特灣海戰的效率](../Page/萊特灣海戰.md "wikilink")，日軍高層也能預知飛行員銳減的後果。大西瀧治郎的設想是，依據日軍觀察美軍作戰程序，總是以官兵生命安全為重要顧慮。如果特攻能造成美方大量傷亡，並顯示日人必死抵抗之決心，經由美國媒體揭露，在美國本土或許會引起厭戰情緒，不願貿然攻擊日本本土。則處於不利戰局的日本，或許有謀求談判，停止戰鬥，不需面對投降與毀滅的一線希望。

但該專題報導，美軍太平洋戰區也掌握日方此一謀劃，而把日本特攻的消息盡量壓縮，所以戰時美國社會對特攻的具體情況所知不多。加上特攻成效在幾個月內就銳減，特攻戰術的失敗遂無可避免。更有甚者，在有限的報導與口耳相傳下，神風特攻隊的行為與日軍不願投降也不善待俘虜的思想混合，在美國民間塑造出日本人野蠻、殘酷又瘋狂的形象，反而更加強化了「對日作戰應毫不留情」的輿論。一般相信，因此產生的道德[除罪化氣氛](../Page/除罪化.md "wikilink")，在一定程度上促成了用[B-29轟炸日本城市和投擲](../Page/B-29.md "wikilink")[原子彈等行動](../Page/原子彈.md "wikilink")。

[Takijiro_Onishi.jpg](https://zh.wikipedia.org/wiki/File:Takijiro_Onishi.jpg "fig:Takijiro_Onishi.jpg")
大西瀧治郎在天皇[裕仁宣布投降後](../Page/裕仁.md "wikilink")，為對他推出的戰術造成的約4,000名日本青年的死難及其家屬致歉，刻意不請人[介錯的情況下](../Page/介錯.md "wikilink")[切腹自殺身亡](../Page/切腹.md "wikilink")。联合舰队参谋长[宇垣纏在](../Page/宇垣纏.md "wikilink")8月15日[玉音放送后](../Page/玉音放送.md "wikilink")，由於不會駕駛飛機，搭乘[彗星四三型特攻机要做日本最後一次神風攻擊](../Page/彗星俯衝轟炸機.md "wikilink")，在冲绳美海军舰队附近被美軍擊落墜海身亡。

## 死亡人数

2010年8月为止确认的特攻隊員死亡数为

  - 海軍

<!-- end list -->

  - 海軍航空特攻隊員：2,531名
  - 特殊潜艇（甲標的・海龙）隊員：440名
  - 回天特攻隊員：104名
  - 震洋特攻隊員：1,081名
  - 合計：4,156名

<!-- end list -->

  - 陸軍

<!-- end list -->

  - 陸軍航空特攻隊員：1,417名
  - 丹羽戰車特攻隊員：9名
  - 陸軍海上挺身隊員（马来）：263名
  - 合計：1,689名

还有第二艦隊战没者、搭载回天进行出击战死的潜水艦乘員、运输时乘船沉没而参加陆上战斗的战没者、[義烈空挺隊等特攻作战相关战没者如以下](../Page/義号作戦.md "wikilink")。

  - 第二艦隊战没者：3,751名
  - 回天部隊关連战没者：1,083名
  - 震洋部隊关連战没者：1,446名
  - 陸軍航空关連战没者：177名
  - 海上挺身隊关連战没者：1,573名
  - 空挺部隊关連战没者：100名
  - 其他（終战時自杀、[神州不滅特別攻撃隊](../Page/神州不滅特別攻撃隊.md "wikilink")、大分701空组成的所谓「**宇垣軍团私兵特攻**」等）战没者：34名
  - 合計：8,164名

一共合計14,009名\[7\]。

## 原特攻队员名人

  - [園田直](../Page/園田直.md "wikilink")（原内閣官房長官、原外務大臣、原厚生大臣）
  - [島尾敏雄](../Page/島尾敏雄.md "wikilink")（作家）
  - [田中六助](../Page/田中六助.md "wikilink")（原眾議院議員）
  - [西村晃](../Page/西村晃.md "wikilink")（演员・2代目水戸黄門）
  - [千玄室](../Page/千玄室.md "wikilink")
  - [田英夫](../Page/田英夫.md "wikilink")（原参議院議員）
  - [黑尾重明](../Page/黑尾重明.md "wikilink")（原東急FLYERS投手）
  - [信太正道](../Page/信太正道.md "wikilink")（原航空自衛隊教官、全日本空运機長、反战团体代表）
  - [蓼沼謙一](../Page/蓼沼謙一.md "wikilink")（勞動法学者、原一橋大学学長、原日本勞動法学会代表理事）
  - [蔦文也](../Page/蔦文也.md "wikilink")（原池田高校野球部監督）
  - [井出定治](../Page/井出定治.md "wikilink")（牧師）
  - [佐野浅夫](../Page/佐野浅夫.md "wikilink")（演员・3代目水戸黄門）

## 特別攻擊隊列表

特別攻擊隊為[大日本帝國](../Page/大日本帝國.md "wikilink")[本土決戰防衛](../Page/本土決戰.md "wikilink")，及二戰特殊攻擊之敢死玉碎的軍事戰術。主要分為：海上特別攻擊隊、空中特別攻擊隊，及其他之特別攻擊隊等類別。

### 海上特別攻擊隊

#### 本土決戰防衛

  - [震洋特攻隊](../Page/震洋特攻隊.md "wikilink")（[震洋](../Page/震洋.md "wikilink")）：海上敢死隊\[8\]
  - [回天特攻隊](../Page/回天.md "wikilink")：海上敢死隊
  - [海龍特攻隊](../Page/海龍_\(潜水艇\).md "wikilink")（）：海上敢死隊
  - [マルニ特工隊](../Page/四式肉薄攻撃艇.md "wikilink")（）：海上敢死隊

#### 二戰特別攻擊潛艦

  - [蛟龍特攻隊](../Page/蛟龍_\(潛水艇\).md "wikilink")（）：海面下突擊艦隊
  - [第10特攻戰隊](../Page/第10特攻戰隊.md "wikilink")（）：以蛟龍潛水艇編成之潛水艇艦隊

#### 空中特別攻擊隊

  - [金鵄隊](../Page/金鵄隊.md "wikilink")：221空在1944年12月于[吕宋島](../Page/吕宋島.md "wikilink")[组织的对](../Page/组织.md "wikilink")[B-24轰炸机特攻队](../Page/B-24轰炸机.md "wikilink")
  - [天雷特別攻撃隊](../Page/天雷特別攻撃隊.md "wikilink")：未实战。

#### 本土決戰防衛

  - [神風特攻隊](../Page/神風特攻隊.md "wikilink")：空中敢死隊
  - [神雷特攻隊（櫻花特攻隊）](../Page/MXY-7櫻花特別攻擊機.md "wikilink")：空中敢死隊、有人誘導爆擊戰
  - [震天制空隊](../Page/震天制空隊.md "wikilink")（）：空對空特別攻撃隊，1944年（昭和19年）11月7日組成

#### 空降特攻

莱特湾之战中，有[高砂义勇队组织的](../Page/高砂义勇队.md "wikilink")“薰空挺队”。

#### 陆上特攻

1945年4月12日，[山下奉文命令坦克第](../Page/山下奉文.md "wikilink")10連隊第5中隊组织对M4坦克的特攻，17日在丹羽治一准尉指挥下用九七式和九五式各一辆装上炸药攻击M4，炸毁的4辆残骸挡住了道路使司令部能够撤离。另外冲绳战役有鉄血勤皇隊的出现。

## 有关作品

### 电影

  - 《[愛與夢飛翔](../Page/愛與夢飛翔.md "wikilink")》（1995）
  - 《[吾为君亡](../Page/吾为君亡.md "wikilink")》（2007）
  - 《[永遠的零](../Page/永遠的零.md "wikilink")》（2013）

### 動畫

  - 《》（1978）

## 相關

  - [回天](../Page/回天.md "wikilink")
  - [自殺攻擊](../Page/自殺攻擊.md "wikilink")
  - [元軍侵日戰爭](../Page/元軍侵日戰爭.md "wikilink")
  - [特攻兵器](../Page/特攻兵器.md "wikilink")（）
  - [特殊攻撃機](../Page/特殊攻撃機.md "wikilink")（）
  - [上原良司](../Page/上原良司.md "wikilink")
  - [菊水作戰](../Page/菊水作戰.md "wikilink")
  - [坊之岬海戰](../Page/坊之岬海戰.md "wikilink")
  - [萬歲衝鋒](../Page/萬歲衝鋒.md "wikilink")
  - [玉碎](../Page/玉碎.md "wikilink")

## 註釋

## 外部連結

  - [互联网的收集二次世界大战信函](http://ww2.war-letters.com)
  - [神風特攻隊戰術](http://ww2db.com/other.php?other_id=18)
  - [配有熱門音樂的神風特攻隊攻擊航母紀錄片](http://www.youtube.com/watch?v=U7Bej28UhFc)
  - [美方民間關於神風特攻隊資料的網站](http://wgordon.web.wesleyan.edu/kamikaze/index.htm)
  - [Kamikaze Images](http://wgordon.web.wesleyan.edu/kamikaze/) -
    西方與日本對特攻員形象的記述
  - [神風特攻隊紀錄照片](http://ww2db.com/photo.php?source=all&color=all&list=search&foreigntype=O&foreigntype_id=18)
  - [Kamikaze
    Footage](http://www.ww2incolor.com/gallery/movies/kamikaze) -
    二戰特攻紀錄片
  - [Dayofthekamikaze.com](http://www.dayofthekamikaze.com/d4y.html)
  - [關於一位前特攻隊員](http://www.japanvisitor.com/index.php?cID=359&pID=1089)

[Chiran_high_school_girls_wave_kamikaze_pilot.jpg](https://zh.wikipedia.org/wiki/File:Chiran_high_school_girls_wave_kamikaze_pilot.jpg "fig:Chiran_high_school_girls_wave_kamikaze_pilot.jpg")執行神風任務的隊員\]\]

  - ["Gyokusai"](http://www.antithesiscommon.com/Issue1/pg4.htm)
  - ["誰成為特攻隊員？"](https://web.archive.org/web/20090306053547/http://www.tcr.org/tcr/essays/EPrize_Kamikaze.pdf)
  - [www.tokkotai.or.jp](https://web.archive.org/web/20070210072415/http://www.tokkotai.or.jp/kikanshi/kaihou_No49.htm)
  - [「一個選擇活下去的特攻隊員」](http://www.timesonline.co.uk/article/0,,3-1640211,00.html#cid=rssfeed&attr=World)
  - [www.geocities.jp (Book
    collection)](http://www.geocities.jp/kamikazes_site_e/sankou.html)
  - [特攻隊員日記摘要](http://www.press.uchicago.edu/Misc/Chicago/619508.html)
  - [Kaiten](http://www.williammaloney.com/Dad/WWII/SubmarineLing/KaitenSuicideSubmarine/index.htm)
    紐澤西州博物館的日軍自殺潛艇資料
  - [Okinawa Suicide
    Boat](http://www.williammaloney.com/Aviation/BattleshipCoveNavalMuseum/JapaneseSuicideBoat/index.htm)
    麻州博物館中關於日軍自殺小艇的照片
  - [財團法人日本神風特攻隊陣亡者追思與和平祈禱會](http://www.tokkotai.or.jp)
  - [](http://www.geocities.jp/torikai007/1945/tokkou.html)（）

[Category:日军](../Category/日军.md "wikilink")
[Category:太平洋战争](../Category/太平洋战争.md "wikilink")
[Category:第二次世界大战](../Category/第二次世界大战.md "wikilink")
[\*](../Category/特攻作戰.md "wikilink")

1.  [所谓“神风特攻队”](http://www.apdnews.com/news/67622.html) ，亚太日报，2014年2月11日
2.  [日本为“神风特攻队”遗物申遗](http://world.people.com.cn/n/2014/0204/c1002-24281042.html)
3.  [日本神风特攻队遗书申遗被拒 申遗方将继续申请](http://news.qq.com/a/20140614/003741.htm)
4.
5.  大贯美惠子：《神风特攻队、樱花与民族主义》，商务印书馆，2016，191页
6.  日本帝国的衰亡
7.  『特別攻撃隊全史』（特攻隊慰霊顕彰会）より
8.  鄭閔聲／台北報導，「震洋特攻隊」遺跡 高屏澎現蹤
    日軍二戰海上敢死隊」，[中國時報](../Page/中國時報.md "wikilink")，2011-01-14