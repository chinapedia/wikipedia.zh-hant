[Elaphrosaurus.jpg](https://zh.wikipedia.org/wiki/File:Elaphrosaurus.jpg "fig:Elaphrosaurus.jpg")而建立\]\]
[Elaphosaurus_bambergiiP1060058.jpg](https://zh.wikipedia.org/wiki/File:Elaphosaurus_bambergiiP1060058.jpg "fig:Elaphosaurus_bambergiiP1060058.jpg")自然科學博物館。\]\]
**輕巧龍屬**（[學名](../Page/學名.md "wikilink")：*Elaphrosaurus*）又名**輕龍**或**伊拉夫羅龍**，意為「體重很輕的蜥蜴」，是種[肉食性](../Page/肉食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於[侏羅紀晚期的](../Page/侏羅紀.md "wikilink")[坦桑尼亞](../Page/坦桑尼亞.md "wikilink")，約1億4500萬年前。輕巧龍可能是屬於[角鼻龍下目](../Page/角鼻龍下目.md "wikilink")，身長約6公尺。有科學家提出輕巧龍是最晚期的[腔骨龍超科](../Page/腔骨龍超科.md "wikilink")，但此說法已被否定。在1920年代，輕巧龍最初被歸類於[虛骨龍科](../Page/虛骨龍科.md "wikilink")\[1\]。在恐龍分類的早期歷史，有許多無法歸類的小型獸腳類，被歸類於虛骨龍科。在1970年代，輕巧龍被歸類於[似鳥龍科](../Page/似鳥龍科.md "wikilink")\[2\]。目前較普遍的分類，把輕巧龍歸類於[角鼻龍類](../Page/角鼻龍類.md "wikilink")\[3\]。[泥潭龍被認為是其最近親](../Page/泥潭龍.md "wikilink")\[4\]。

## 古生物學

輕巧龍的化石是一副接近完整的骨骼，是於坦桑尼亞[湯達鳩組地層被發現](../Page/湯達鳩組.md "wikilink")，當地發現的化石亦有[腕龍](../Page/腕龍.md "wikilink")、[異特龍及](../Page/異特龍.md "wikilink")[釘狀龍等](../Page/釘狀龍.md "wikilink")。[科學家並不肯定輕巧龍的樣貌](../Page/科學家.md "wikilink")，因為從未發現牠的[頭顱骨](../Page/頭顱骨.md "wikilink")。在湯達鳩組，只有少量的[獸腳亞目化石被發現](../Page/獸腳亞目.md "wikilink")，大部分是一些骨頭碎片，輕巧龍如此完整的骨骼則是很稀少的。在1960年，一個發現於[尼日的化石被建立為戈氏輕巧龍](../Page/尼日.md "wikilink")（*E.
gautieri*），年代屬於[白堊紀早期](../Page/白堊紀.md "wikilink")；在2004年，由[保羅·賽里諾](../Page/保羅·賽里諾.md "wikilink")（Paul
Sereno）將其重新命名為[棘椎龍](../Page/棘椎龍.md "wikilink")。在[莫里遜組發現的一個化石](../Page/莫里遜組.md "wikilink")，可能屬於輕巧龍\[5\]。目前還沒有發現輕巧龍的頭顱骨化石。

## 發現歷史

牠是體型修長的恐龍，長頸可能是用作挖掘腐肉。輕巧龍約6.2公尺長，臀部高1.46公尺，重約210公斤\[6\]。輕巧龍的[脛骨長於](../Page/脛骨.md "wikilink")[股骨](../Page/股骨.md "wikilink")，顯示牠們很適合奔跑，牠們可能在廣大的平原追捕小型的獵物\[7\]。輕巧龍的化石發現於[莫里遜組的第](../Page/莫里遜組.md "wikilink")2到第4地層帶\[8\]。

## 參考資料

<div class="references-small">

<references />

  - <span style="font-size:smaller;"></span>

  -

</div>

[Category:角鼻龍下目](../Category/角鼻龍下目.md "wikilink")
[Category:非洲恐龍](../Category/非洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")

1.

2.

3.

4.  Diego Pol & Oliver W. M. Rauhut (2012). "A Middle Jurassic
    abelisaurid from Patagonia and the early diversification of theropod
    dinosaurs". Proceedings of the Royal Society B: Biological Sciences
    (in press).
    <http://rspb.royalsocietypublishing.org/content/early/2012/05/17/rspb.2012.0660.short?rss=1>.


5.

6.
7.

8.  Foster, J. (2007). "Appendix." *Jurassic West: The Dinosaurs of the
    Morrison Formation and Their World*. Indiana University Press. pp.
    327-329.