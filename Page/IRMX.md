**iRMX**是[Intel公司为](../Page/Intel.md "wikilink")8080和8086[微处理器设计的一款非常典型的](../Page/微处理器.md "wikilink")[实时多任务](../Page/实时操作系统.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，iRMX是Intel
Real-time Multitasking
eXecutive的缩写，该操作系统由[INTEL公司在](../Page/Intel.md "wikilink")70年代后期开发，并于1980年面世，以满足市场对8080/8086处理器日益增长的应用需求。

80年代时期，iRMX比较广泛地应用在以8080/8086/80286/80486为[中央处理器的](../Page/中央处理器.md "wikilink")[嵌入式系统上作为系统的开发](../Page/嵌入式系统.md "wikilink")[编译平台](../Page/编译.md "wikilink")，特别是在当时的[程控交换机通信系统开发领域](../Page/程控交换机.md "wikilink")，iRMX几乎成为一个事实上的标准开发平台。

iRMX系统采用INTEL公司的PL/M高级语言及相应的系列[编译器](../Page/编译器.md "wikilink")，调试器作为开发[编译调试环境](../Page/编译.md "wikilink")。

[Category:实时操作系统](../Category/实时操作系统.md "wikilink")
[Category:嵌入式系统](../Category/嵌入式系统.md "wikilink")