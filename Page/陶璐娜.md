**陶璐娜**，[上海人](../Page/上海.md "wikilink")。前[中國](../Page/中國.md "wikilink")[射擊女子運動員](../Page/射擊.md "wikilink")，奥运冠军，现为上海市人大代表，\[1\]上海击剑自行车运动管理中心副主任。北京[首都体育学院](../Page/首都体育学院.md "wikilink")[运动心理学硕士](../Page/运动心理学.md "wikilink")。

## 生涯

陶璐娜的啓蒙教練是翁紹興，陶璐娜還是13歲時就被翁紹興物色，入讀了[上海](../Page/上海.md "wikilink")[南市區少體校](../Page/南市區少體校.md "wikilink")，在翁教練的教導之下，陶璐娜的成績顯赫。

幾年後，陶璐娜成為了上海射擊隊的隊員之一，其教練是為謝前喬。隨著時間過去，陶璐娜的天才橫溢吸引了當時的[中國射擊隊總教練](../Page/中國.md "wikilink")、為[中國在](../Page/中國.md "wikilink")[奧運中奪得首面](../Page/奧運.md "wikilink")[金牌的](../Page/金牌.md "wikilink")[許海峰](../Page/許海峰.md "wikilink")，陶璐娜入選國家隊，成績繼續進步。在同[年陶璐娜奪得射犛世界盃的冠軍](../Page/年.md "wikilink")，次年成功衛冕。1999年，全國射擊賽中，陶璐娜成為女子10米[空氣手槍的冠軍](../Page/空氣手槍.md "wikilink")、一年後的世界盃不但奪得了冠軍，更打破了[世界紀錄](../Page/世界紀錄.md "wikilink")。

同年的[悉尼奧運](../Page/悉尼奧運.md "wikilink")，陶璐娜憑著出眾的表現替[中國摘下該屆](../Page/中國.md "wikilink")[奧運中的首面](../Page/奧運.md "wikilink")[金牌](../Page/金牌.md "wikilink")。其後一年，陶璐娜獲得大大小小的冠軍。於[釜山亞運中](../Page/2002年亞洲運動會.md "wikilink")，陶璐娜為[中國增添了三面金牌](../Page/中國.md "wikilink")，並再次打破了世界紀錄。2004年再度參與10米氣手槍項目，但在預賽中僅排38名，未能晉身決賽。

在2008年北京奥运会射击选拔赛中，陶璐娜再最后阶段“功亏一篑”败给了[郭文珺](../Page/郭文珺.md "wikilink")，未能获得奥运门票。\[2\]2008年后，陶璐娜暂退并生子
，后成为了上海击剑自行车运动管理中心的副主任。

2011年，陶璐娜复出参加选拔赛，由于成绩优异而再次回到了国家队，但在2012年3月的奥运会组队选拔赛中陶璐娜在三场比赛中夺得一次第五，两个第三，最后的奥运积分不敌[郭文珺和](../Page/郭文珺.md "wikilink")[苏玉玲](../Page/苏玉玲.md "wikilink")，无缘伦敦奥运会。\[3\]

## 参考资料

[Category:上海籍运动员](../Category/上海籍运动员.md "wikilink")
[Category:中国射击运动员](../Category/中国射击运动员.md "wikilink")
[Category:中国奥运射击运动员](../Category/中国奥运射击运动员.md "wikilink")
[Category:中國奧林匹克運動會金牌得主](../Category/中國奧林匹克運動會金牌得主.md "wikilink")
[Luna](../Category/陶姓.md "wikilink")
[Category:中国奥林匹克运动会银牌得主](../Category/中国奥林匹克运动会银牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會射擊運動員](../Category/2000年夏季奧林匹克運動會射擊運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會射擊運動員](../Category/2004年夏季奧林匹克運動會射擊運動員.md "wikilink")
[Category:亞洲運動會射擊獎牌得主](../Category/亞洲運動會射擊獎牌得主.md "wikilink")
[Category:1998年亞洲運動會射擊運動員](../Category/1998年亞洲運動會射擊運動員.md "wikilink")
[Category:2002年亞洲運動會射擊運動員](../Category/2002年亞洲運動會射擊運動員.md "wikilink")
[Category:2006年亞洲運動會射擊運動員](../Category/2006年亞洲運動會射擊運動員.md "wikilink")
[Category:1998年亞洲運動會金牌得主](../Category/1998年亞洲運動會金牌得主.md "wikilink")
[Category:2002年亞洲運動會金牌得主](../Category/2002年亞洲運動會金牌得主.md "wikilink")
[Category:2002年亞洲運動會銀牌得主](../Category/2002年亞洲運動會銀牌得主.md "wikilink")
[Category:2006年亚洲运动会金牌得主](../Category/2006年亚洲运动会金牌得主.md "wikilink")
[Category:2006年亞洲運動會銀牌得主](../Category/2006年亞洲運動會銀牌得主.md "wikilink")
[Category:奧林匹克運動會射擊獎牌得主](../Category/奧林匹克運動會射擊獎牌得主.md "wikilink")

1.  [上海市第十四届人民代表大会代表名单](http://sh.eastday.com/m/20121226/u1a7089714.html)
    东方网
2.  [陶璐娜无缘北京奥运会](http://epaper.syd.com.cn/syrb/html/2008-03/12/content_341174.htm)
    沈阳日报
3.  [不后悔复出](http://sports.people.com.cn/GB/22155/17380292.悉尼奥运会冠军陶璐娜无缘伦敦奥运会)
    北京青年报