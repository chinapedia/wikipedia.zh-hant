**斯蒂芬妮·野野下·托帕里恩**（，，），藝名為**Stephanie**（），[日本女歌手](../Page/日本.md "wikilink")，[美國](../Page/美國.md "wikilink")[加州出生](../Page/加州.md "wikilink")，[美日](../Page/美國.md "wikilink")[混血](../Page/混血.md "wikilink")，現居[東京](../Page/東京.md "wikilink")。

## 人物

父親是美籍[亚美尼亚人](../Page/亚美尼亚人.md "wikilink")，母親是[山口县出身的日本人](../Page/山口县.md "wikilink")。因為练得五个高八度的音域\[1\]，受到注目而被發掘。同[模特兒](../Page/模特兒.md "wikilink")[太田莉菜及歌手](../Page/太田莉菜.md "wikilink")[矢住夏菜關係友好](../Page/矢住夏菜.md "wikilink")。当Stephanie在[日本电视台的](../Page/日本电视台.md "wikilink")[音乐战士
MUSIC
FIGHTER节目中的](../Page/音乐战士_MUSIC_FIGHTER.md "wikilink")「新入战士」单元进行演出时，太田作为推荐人出现并且送上VTR预祝演出成功。

受到歌手[克里斯蒂娜·阿奎莱拉的影响](../Page/克里斯蒂娜·阿奎莱拉.md "wikilink")，立志成为歌手，并尝试为自己的单曲填词（在[JASRAC登记的填词人姓名是](../Page/日本音乐著作权协会.md "wikilink")**STEPHANIE**），最初是和矢住夏菜一起为自己的单曲填词。从第三张单曲〈Winter
Gold〉开始，即独力负责作词。

## 略历

  - 13歲の頃に音楽プロデューサーの[Joe
    Rinoieにデモビデオテープ](../Page/ジョー・リノイエ.md "wikilink")（[shelaの](../Page/shela.md "wikilink")「sepia」を歌唱する内容が『[Music
    Station](../Page/Music_Station.md "wikilink")』に出演した際に公開された）を送付する。同時期に独学でボイストレーニングを始め、声域を拡大することに成功する。
  - 14歲時從[加利福尼亚州搬遷至](../Page/加利福尼亚州.md "wikilink")[東京都](../Page/東京都.md "wikilink")。
  - 2007年、首張單曲「」發行。
  - 2007年12月30日，凭借“”，获得[第49回日本唱片大賞的最佳新人奖](../Page/日本唱片大賞.md "wikilink")。
  - 2008年1月30日，单曲「[朋友](../Page/朋友_\(Stephanie专辑\).md "wikilink")」首次入围[Oricon週間单曲销售TOP](../Page/Oricon.md "wikilink")10。
  - 2008年3月5日，首张个人大碟、同名专辑发售。
  - 2008年3月10日，[一条由香莉原作的漫画](../Page/一条由香莉.md "wikilink")[Pride -
    邁向榮耀之路](../Page/Pride_迈向荣耀之路.md "wikilink")（2009年1月17日上映）的真人电影版启用Stephanie出演女主角麻見史緒。
  - 2015年上半年，與其他五名歌手組成[Genealogy代表](../Page/Genealogy.md "wikilink")[亞美尼亞出席](../Page/亞美尼亞.md "wikilink")[2015年歐洲歌唱大賽](../Page/2015年歐洲歌唱大賽.md "wikilink"),
    並且成功進入決賽。

## 音樂作品

### 單曲

1.  ****（2007年5月30日發售／SECL-505）
      - [東京電視台系動畫](../Page/東京電視台.md "wikilink")「[星界死者之書](../Page/星界死者之書.md "wikilink")」片尾曲（ED1）
2.  ****（2007年8月29日發售／SECL-528）
      - [東京電視台系動畫](../Page/東京電視台.md "wikilink")「[星界死者之書](../Page/星界死者之書.md "wikilink")」片尾曲（ED2）
3.  ****（2007年12月12日發售／SECL-548）
      - 初回限定盤購入者対象ライブ招待IDナンバー入りフライヤー封入
4.  ****（2008年1月30日發售／SECL-594）
      - 初回限定盤“機動戰士GUNDAM 00”』オリジナル書き下ろしイラスト付キャップ仕様・キャラクターID卡（）封入

      - ：[MBS](../Page/毎日放送.md "wikilink")・[TBS系動畫](../Page/東京放送.md "wikilink")“[機動戰士GUNDAM
        00](../Page/機動戰士GUNDAM_00.md "wikilink")”片尾曲2(ED2)
5.  ****（2008年7月23日發售／SECL-668）
6.  **[Pride〜A Part of Me〜
    feat.SRM](../Page/Pride〜A_Part_of_Me〜_feat.SRM.md "wikilink")**（2009年1月14日发售／SECL-740（DVD付初回限定盘）、742（通常盘））
7.  **[FUTURE](../Page/FUTURE.md "wikilink")**（2009年4月29日发售/SECL-718）

### 专辑

1.  同名专辑**Stephanie**（2008年3月5日发售／SECL-621（CD+DVD 初回限定盘）、SCEL-623（CD盘））
      - 初回限定盘：四首单曲的PV DVD收录。
2.  **[Colors of my
    Voice](../Page/Colors_of_my_Voice.md "wikilink")**（2009年6月17日发售/SECL-752\~753（CD+DVD
    初回限定盘）、SCEL-754（通常盘））

## 活动

  - 记录Stephanie所参加的各种活动
      - 初ライブ（オフィシャルサイト応募での完全招待制）
      - 2007年6月13日：シェラトン・グランデ・トーキョーベイ・ホテル クリスタルチャペル
      - 2007年6月14日：リーガロイヤルホテル大阪 ザクリスタルチャペル
  - [mora](../Page/mora.md "wikilink") presentsスペシャルライブ
    9月活动「中村中×Stephanie」
      - 初の一般招待制ライブ、與[中村中共演](../Page/中村中.md "wikilink")。
      - 2007年9月4日：東京都内
  - Stephanie Winter Gold发售纪念特别演出
      - 仅限单曲Winter Gold初回限定盘购入者参加
      - 2008年1月26日：東京都内
      - 2008年1月27日：大阪府内
  - MUV RINOIE PRESENTS Stephanie & 矢住夏菜 バレンタインライヴ
      - 与[矢住夏菜一同演出](../Page/矢住夏菜.md "wikilink")
      - 2008年2月14日：恵比寿LIVE GATE TOKYO
  - 个人Live@渋谷DUO
      - 首次个人Live
      - 2009年2月15日：Shibuya DUO -Music Exchange-
  - [机器人之魂](../Page/机器人动画.md "wikilink")2009“春之陣”
      - 以机器人动画片尾曲「朋友」参与。
      - 2009年4月29日：Zepp Tokyo

## 演出

### 電影

  - 《[Pride 邁向榮耀之路](../Page/Pride_邁向榮耀之路.md "wikilink")》飾演 麻見史緒（主演）

### 電視節目

  - [MUSIC
    JAPAN](../Page/MUSIC_JAPAN_\(NHK\).md "wikilink")（[NHK](../Page/NHK.md "wikilink")，2007年6月15日・9月28日）
  - [月刊MelodiX\!](../Page/月刊MelodiX!.md "wikilink")（[東京電視台](../Page/東京電視台.md "wikilink")，2007年8月25日・2008年1月26日）
  - 音時間（東京電視台，2007年9月25日）
  - [HEY\!HEY\!HEY\!MUSIC
    CHAMP](../Page/HEY!HEY!HEY!MUSIC_CHAMP.md "wikilink")（富士電視台，2007年12月3日）
  - [ショーパン](../Page/ショーパン_\(电视节目\).md "wikilink")（富士電視台，2007年12月11日）
  - [第49回
    日本唱片大賞](../Page/日本唱片大賞.md "wikilink")（[TBS](../Page/東京放送.md "wikilink")，2007年12月30日）
  - [音乐战士 MUSIC
    FIGHTER](../Page/音乐战士_MUSIC_FIGHTER.md "wikilink")（[日本电视台](../Page/日本电视台.md "wikilink")、2008年2月1日）

### 電台節目

  - [HotStuff extra
    feat.ステファニー](../Page/e-radio_HotStuff.md "wikilink")（[e-radio](../Page/滋賀電台.md "wikilink")、2007年9月29日）

## 相關條目

  - [Joe Rinoie](../Page/ジョー・リノイエ.md "wikilink")

## 参考文献

## 外部連結

  - [SME
    RECORDS官方網頁](https://web.archive.org/web/20070906102822/http://www.steph.jp/)

  - [所属事务所官方网页](http://www.muvrinoie.com/)

  - [ステファニーのFUNNYな一日](http://ameblo.jp/stepha-nie/) 2009年4月29日～

  - [Stephanie's blog](http://www.playlog.jp/steph/blog/)
    2007年5月29日～2009年4月28日

[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:亞美尼亞裔混血兒](../Category/亞美尼亞裔混血兒.md "wikilink")
[Category:日裔混血儿](../Category/日裔混血儿.md "wikilink")
[Category:報知金酸莓獎獲獎者](../Category/報知金酸莓獎獲獎者.md "wikilink")
[Category:加州人](../Category/加州人.md "wikilink")
[Category:青山學院大學校友](../Category/青山學院大學校友.md "wikilink")

1.