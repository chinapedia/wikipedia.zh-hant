**喀土穆**（
）是[蘇丹共和國的](../Page/蘇丹共和國.md "wikilink")[首都](../Page/首都.md "wikilink")。來自[烏干達的](../Page/烏干達.md "wikilink")[白尼羅河與來自](../Page/白尼羅河.md "wikilink")[埃塞俄比亞的](../Page/埃塞俄比亞.md "wikilink")[青尼羅河在此交匯](../Page/青尼羅河.md "wikilink")，向北奔向[埃及流入](../Page/埃及.md "wikilink")[地中海](../Page/地中海.md "wikilink")。「喀土穆」一詞的意思是大象鼻子。

喀土穆市區由100萬常住人口，由臨近的北喀土穆和[恩圖曼所構成的大都會的人口可能超過](../Page/恩圖曼.md "wikilink")400萬。

## 歷史

喀土穆於1821年由[埃及的](../Page/埃及.md "wikilink")[穆罕默德·阿里帕夏建城](../Page/穆罕默德·阿里帕夏.md "wikilink")，作為埃及軍隊的據點。不過後來成長為包含[奴隸輸出的區域交易中心](../Page/奴隸.md "wikilink")。1884年3月13日，效忠[馬赫迪](../Page/馬赫迪.md "wikilink")[穆罕默德·艾哈邁德的軍隊包圍了由](../Page/穆罕默德·艾哈邁德.md "wikilink")[英國將領](../Page/英國.md "wikilink")[查理·喬治·戈登帶領的守軍](../Page/查理·喬治·戈登.md "wikilink")，之後對於本城的英埃聯軍大屠殺。

1885年1月26日，殘破的喀土穆被攻下。後來在1898年9月2日，由[赫伯特·基奇勒帶領的英軍擊敗了守城的馬赫迪派部隊](../Page/赫伯特·基奇勒.md "wikilink")。

1899年，喀土穆成了[英埃蘇丹國的首都](../Page/英埃蘇丹國.md "wikilink")，並於1956年獨立為蘇丹後成為該國首都。

## 气候

[Khartoum.jpg](https://zh.wikipedia.org/wiki/File:Khartoum.jpg "fig:Khartoum.jpg")
喀土穆的气候是[热带沙漠气候](../Page/热带沙漠气候.md "wikilink")，七月和八月可看到显著的降水量。喀土穆平均每年降水量超过155毫米。基于年平均气温，喀土穆是最热的大城市之一。盛夏的温度可能会超过53°C（27°F）的。年平均最高气温37.1℃（99°F）。此外，没有任何月平均温度低于30°C（86°F）。这一点没见过其他主要的热带沙漠气候的城市[利雅得](../Page/利雅得.md "wikilink")，[巴格达和](../Page/巴格达.md "wikilink")[凤凰城这样的](../Page/凤凰城.md "wikilink")。温度在夜间降温明显，喀土穆的平均最低气温较低，今年刚刚超过15°C（59°F）。

## 交通

### 空運

[Sudan_Khartoum_View_with_Traffic_2003.jpg](https://zh.wikipedia.org/wiki/File:Sudan_Khartoum_View_with_Traffic_2003.jpg "fig:Sudan_Khartoum_View_with_Traffic_2003.jpg")
喀土穆有蘇丹最大的空港：[喀土穆國際機場](../Page/喀土穆國際機場.md "wikilink")。也是[蘇丹航空主要的集散地](../Page/蘇丹航空.md "wikilink")。原本建在喀土穆市的南方，但隨著城市的快速成長與附近地區的都市化，機場反而變成在市中心。

新的國際機場目前在[恩圖曼建造中](../Page/恩圖曼.md "wikilink")。它將取代目前的[喀土穆國際機場成為蘇丹最大的機場](../Page/喀土穆國際機場.md "wikilink")。蘇丹第2大機場是[蘇丹港機場](../Page/蘇丹港機場.md "wikilink")。

## 姐妹市

  - —[安曼](../Page/安曼.md "wikilink")，[約旦](../Page/約旦.md "wikilink")

  - —[開羅](../Page/開羅.md "wikilink")，[埃及](../Page/埃及.md "wikilink")

  - —[伊斯坦堡](../Page/伊斯坦堡.md "wikilink")，[土耳其](../Page/土耳其.md "wikilink")

  - —[安卡拉](../Page/安卡拉.md "wikilink")，[土耳其](../Page/土耳其.md "wikilink")

  - —[聖彼得堡](../Page/聖彼得堡.md "wikilink")，[俄羅斯](../Page/俄羅斯.md "wikilink")

  - —[武漢](../Page/武漢.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")

  - —[阿斯馬拉](../Page/阿斯馬拉.md "wikilink")，[厄利垂亞](../Page/厄利垂亞.md "wikilink")

## 參見

  - [喀土穆省](../Page/喀土穆省.md "wikilink")

## 外部連結

[Category:蘇丹城市](../Category/蘇丹城市.md "wikilink")
[Category:非洲首都](../Category/非洲首都.md "wikilink")
[Category:喀土木](../Category/喀土木.md "wikilink")