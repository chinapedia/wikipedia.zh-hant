[Bookcover-Generation-X.jpg](https://zh.wikipedia.org/wiki/File:Bookcover-Generation-X.jpg "fig:Bookcover-Generation-X.jpg")
**X世代**專指**1966年～1980年**出生的人。

這詞彙最先出現在傳奇戰地記者[羅伯特·卡帕](../Page/羅伯特·卡帕.md "wikilink")（[Robert
Capa](http://en.wikipedia.org/wiki/Robert_Capa))的相片特輯([Photo-essay](http://en.wikipedia.org/wiki/Photo-essay))，他用這來形容第二次世界大戰後出生的一代。這特輯1953年於英國的picture
post及美國的holiday刊出。羅伯特·卡帕自己的解釋:「我們稱這個未知的世代為x世代，這世代中，我們意識到，縱使我們滿腔熱誠，有些事情還是憑我們的財力及能力都做不來的。」(We
named this unknown generation, The Generation X, and even in our first
enthusiasm we realised that we had something far bigger than our talents
and pockets could cope with)。作家John
Ulrich曾定義，x世代通常是指欠缺身份認同、面對着前景不明朗、甚至惡劣的未來的一群青年。這個詞彙在1960年代中到1970年代中期都被使用，但定義就漸漸由當初的一整代人，轉為狹義的形容對搖滾及punk十分熱衷的英國白人青年。

這是[1991年之後大為流行的一個](../Page/1991年.md "wikilink")[生活型態名詞](../Page/生活型態.md "wikilink")，源於1991年[道格拉斯·柯普蘭](../Page/道格拉斯·柯普蘭.md "wikilink")（Douglas
Coupland）出版的同名小說《X世代：速成文化的故事》。在小說當中，柯普蘭將[1950年代後期和](../Page/1950年代.md "wikilink")[1960年代之間出生的世代](../Page/1960年代.md "wikilink")，定義為X世代。也就是過去稱為[嬰兒潮世代的下一世代](../Page/嬰兒潮世代.md "wikilink")。有很大一部份X世代的討論與解釋，都是針對在[美國與](../Page/美國.md "wikilink")[加拿大地區在這個時間出生的世代](../Page/加拿大.md "wikilink")。

根據以往研究，購房需求受諸多因素驅動，例如：利率，移民，房屋市場之投資價值。Y世代認為負擔能力為最重要因素，X世代則認為價格才是首要因素。Y世代較着重政府相關政策幫助，X世代更着重生活質素。\[1\]

## 簡介

X世代中的X是由英文字Excluding的字母X而來，一般寫做e**X**cluding。有著「被排擠的世代」隱喻，有一說出生時間範圍為1965年1月至1976年12月間；正確的出生年份為1966年1月至1980年12月間。

X世代目前廣泛的運用在[人口學](../Page/人口學.md "wikilink")、[社會學](../Page/社會學.md "wikilink")、[行銷學和](../Page/行銷學.md "wikilink")[大眾文化研究上](../Page/大眾文化.md "wikilink")。在1980年代、1990年代、2000年代都具有相當的影響力。

目前已知最早的X世代說法可能是在[1960年代出現的](../Page/1960年代.md "wikilink")，但1991年小說出版後，才開始大為流行。某些媒體報導中，X世代的定義可能不是以出生日為說法，而改成「1980年代的少年」、「1990年代的青年」這種說法。僅管後者較為模糊（因青少年的定義較不準確）、但基本上兩種說法並不衝突。

## 原著小說中的X世代

X世代的定義在今日有相當多的說法，在原著小說中的X世代，很明顯的定義為中高學歷、相對低薪、低福利、從事服務業、常感到沒有未來的人們；喜歡在購物商場，消費低價但是跟得上流行的商品。其最大特色在於這個世代相當容易受到[媒體影響](../Page/媒體.md "wikilink")，舉凡新聞、時尚、運動等重大消息，都有可能決定性的影響了他們的人生觀。

## 其他X世代的轉變與解釋

另外關於X世代的一些描述還有他們的父母親，與戰後[嬰兒潮明顯不同的是](../Page/嬰兒潮.md "wikilink")，X世代的母親多半是職業婦女，因此X世代青年受到父母親在家規範的時間是較少的，這也發展出X世代著名的裝扮，舉凡穿眉環、[刺青的風潮](../Page/刺青.md "wikilink")，都是在X世代中第一次發展出來的。

在這一批X世代出生的人物當中，具有代表性的明星或是名人，可能有相當多。然而將這些明星或是名人一一列出並不見得是妥當的，原因是他們的行為舉動並不會永遠與最初的X世代定義相同，某名人此刻也許具有相當的X世代特質，會被媒體或是理論家拿來當作範例說明。

另一個更廣義的X世代描述是指：1945年到1990年間出生的人。也就是說在[第二次世界大戰之後](../Page/第二次世界大戰.md "wikilink")，[殖民主義消退之後](../Page/殖民主義.md "wikilink")，柏林圍牆倒下之前、冷戰結束之前的世代。一般來說，在殖民主義和全球化之間出生的這一轉折世代，有學者稱其為「」（），也就是使嬰兒潮世代被切割分裂的族群。法國社會學家[路易·肖韋爾](../Page/路易·肖韋爾.md "wikilink")（Louis
Chauvel）分析世代間骨折[歐洲](../Page/歐洲.md "wikilink")[嬰兒潮世代](../Page/嬰兒潮.md "wikilink")（）和X世代。

## 流行文化

[香港著名歌手](../Page/香港.md "wikilink")[郭富城於](../Page/郭富城.md "wikilink")1998年發行的粵語唱片《[唱這歌](../Page/唱這歌.md "wikilink")》內的同名歌曲，歌詞就有提及Generation
X一詞，配合其招牌舞姿，已成為不少香港人的集體回憶。

## X世代出生名人榜

1962年7月3日 – [湯告魯斯](../Page/湯告魯斯.md "wikilink")，美國電影演員及電影製片人。

1966年1月1日 – [Anna
Burke](../Page/Anna_Burke.md "wikilink")，安娜·伊麗莎白·伯克，是[澳大利亞工黨成員和前](../Page/澳大利亞工黨.md "wikilink")[澳大利亞眾議院議長](../Page/澳大利亞.md "wikilink")。

1966年6月7日 – [張雨生](../Page/張雨生.md "wikilink")，台灣歌手、製作人(逝於1997年)。

1967年1月12日 — [井上雄彥](../Page/井上雄彥.md "wikilink")，日本漫畫家。

1967年6月20日 — Nicole Kidman，[妮可·基嫚](../Page/妮可·基嫚.md "wikilink")，澳洲演員。

1968年1月30日 — [費利佩六世](../Page/費利佩六世.md "wikilink")，西班牙現任國王。

1968年2月4日 — [魏綺珊](../Page/魏綺珊.md "wikilink")，香港電視新聞主播、戲劇劇團創辦人及作家。

1968年8月13日 — [蕭薔](../Page/蕭薔.md "wikilink")，臺灣知名模特兒、女演員。

1968年11月17日 —
[李彥宏](../Page/李彥宏.md "wikilink")，中國[百度創始人兼CEO](../Page/百度.md "wikilink")。

1969年9月25日 —
[凱薩琳·麗塔-瓊絲](../Page/凱薩琳·麗塔-瓊絲.md "wikilink")，英國女演員，曾獲得[奧斯卡獎](../Page/奧斯卡.md "wikilink")。

## 相關條目

  - [嬰兒潮](../Page/嬰兒潮.md "wikilink")
  - [Y世代](../Page/Y世代.md "wikilink")
  - [Z世代](../Page/Z世代.md "wikilink")

## 注释

## 參考文献

  - 书籍

<!-- end list -->

  - X世代：速成文化的故事 Generation X ─Tales for an accelerated culture
    道格拉斯·柯普蘭／著 ISBN 986-7252-52-7
  - X世代的價值觀 Managing Generation X: How to bring out the best in young
    talent 布魯斯·塔爾根／著 ISBN 957-621-384-3
  - X世代的未來 Future Tense Ian Morrison／著 ISBN 957-9553-61-0

<!-- end list -->

  - 文章

<!-- end list -->

  - [須文蔚](../Page/須文蔚.md "wikilink") [世代差異
    斷層的記憶](http://blog.chinatimes.com/winway/archive/2005/08/13/10751.html)
  - 夏瑞紅
    [殘忍住在奢侈隔壁](http://blog.chinatimes.com/xletter/archive/2005/08/26/12920.html)
  - GenXegesis: essays on alternative youth (sub)culture By John
    McAllister Ulrich, Andrea L. Harris p. 5
    [1](http://books.google.co.uk/books?id=v10ZUR_Ca3EC&pg=PA32&dq=generation+x+holiday&cd=1&redir_esc=y#v=onepage&q=generation%20x%20holiday&f=false)
  - Rita Yi Man Li Generation X and Y’s demand for homeownership in Hong
    Kong，Pacific Rim Property Research Journal, Volume 21, Issue 1,
    2015， pp.15-36
    [2](http://www.tandfonline.com/eprint/4yGfwGRmkdqjXUMI5yFb/full)

[Category:世代](../Category/世代.md "wikilink")
[Category:以出生年代劃分的特定人群稱謂](../Category/以出生年代劃分的特定人群稱謂.md "wikilink")

1.  <http://www.tandfonline.com/eprint/4yGfwGRmkdqjXUMI5yFb/full>