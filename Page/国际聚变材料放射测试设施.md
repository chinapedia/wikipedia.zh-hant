[IFMIF_Target_Area.svg](https://zh.wikipedia.org/wiki/File:IFMIF_Target_Area.svg "fig:IFMIF_Target_Area.svg")
**国际聚变材料放射测试设施**（International Fusion Material Irradiation
Facility，缩写：**IFMIF**），是一项国际科学研究项目，目的是测试[核聚变反应堆所用墙壁材料的可用性](../Page/聚变能.md "wikilink")。IFMIF将使用基于[粒子加速器的](../Page/粒子加速器.md "wikilink")[中子源在适当的时间周期内产生大的但合适的中子流](../Page/中子源.md "wikilink")，以测试在极端情况下材料的长期行为，这些极端情况类似于反应堆内壁处的情况。

IFMIF将由两个平行的加速器构成，每个长约50米，用来产生[氘核粒子束](../Page/氘.md "wikilink")。用这些粒子束撞击[锂元素组成的标靶后](../Page/锂.md "wikilink")，可得到高能中子，进而照射材料样本和被测试成分。
[Deuterium-tritium_fusion.svg](https://zh.wikipedia.org/wiki/File:Deuterium-tritium_fusion.svg "fig:Deuterium-tritium_fusion.svg")-[tritium](../Page/tritium.md "wikilink")
(D-T) fusion reaction, showing the waste [free
neutron](../Page/free_neutron.md "wikilink") emitted.\]\]

## 参与者

IFMIF的最初策划者是[日本](../Page/日本.md "wikilink")、[欧盟](../Page/欧盟.md "wikilink")、[美国和](../Page/美国.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")，管理者是[国际能源署](../Page/国际能源署.md "wikilink")（International
Energy Agency）。自从2007年以来，它被日本和欧盟一直在聚变能研究领域通过更广泛的方法说服，通过IFMIF /
EVEDA项目为IFMIF进行工程验证和工程设计活动\[1\]\[2\]。在欧洲研究基础设施战略论坛（ESFRI）发表的欧洲研究基础设施路线图报告中推荐了IFMIF的建设\[3\]。

## 建设

IFMIF的建设准备工作按预期已经在2006年开始，尽管发挥其实际的测试功能至少被排在2017年之后。

## 参见

  - [国际热核聚变实验反应堆](../Page/国际热核聚变实验反应堆.md "wikilink")（ITER）

## 参考资料

## 外部链接

  - [IFMIF 主页](http://www.frascati.enea.it/ifmif/)
  - [ITER
    时间表](http://www.iter.org/a/n1/downloads/construction_schedule.pdf)

[Category:核能技術](../Category/核能技術.md "wikilink")
[Category:中子](../Category/中子.md "wikilink")
[Category:核聚变](../Category/核聚变.md "wikilink")

1.
2.
3.