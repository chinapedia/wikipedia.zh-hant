**OPAC**（全称*Online Public Access Catalogue*，图书馆联机目录），又称为“WebPAC（全称Web
Public Access
Catalogue，图书馆自动化系统线上公用目录）”，最新一代的产品往往也被称为“前端发现平台”，是一个电脑化的在线[图书馆资源](../Page/图书馆.md "wikilink")[目录](../Page/图书馆目录.md "wikilink")。读者可以通过万维网实现图书的查找和借阅。原来的电子书目用[TELNET作为技术支持](../Page/TELNET.md "wikilink")，但是已不能满足现今[图书馆联盟间的书目进行汇总](../Page/图书馆联盟.md "wikilink")。现在的网上书目多以地区性图书馆的书目加以汇总，能使读者的查询结果覆盖更大范围的图书馆。

## 查找方式

对于传统的图书馆联机目录来说，最常见的查找方式包括：书名检索、作者检索、[ISBN检索](../Page/ISBN.md "wikilink")、年份检索、[出版社检索](../Page/出版社.md "wikilink")。还有一些不常用，但十分重要的检索方法：分类法检索、导出词检索、丛书检索、套书检索等，以及上述检索条件组合进行的复杂检索。

受到[图书馆2.0运动的影响](../Page/图书馆2.0.md "wikilink")，2006年以后新一代的OPAC纷纷出现。其中，这新一代的“OPAC
2.0”为了更好的适应用户在[互联网时代的新习惯](../Page/互联网.md "wikilink")，仿照[搜索引擎的搜索方式](../Page/搜索引擎.md "wikilink")，以包含全部书目数据内容“关键词”作为默认的搜索条件。

## 正确使用

OPAC的正确使用是减少书目检索时间的一条重要途径。

1.  如果读者在查找单书的同时能够给出相对多的检索项目，那么出现的检索项就越少，找到所需书的概率也就越大。

2.
3.  如果读者要查找一大类的书，比如有读者想了解中国的历史。这种情况就好在OPAC的自由查找栏中键入“中国”和（and）“历史”，这样所需的书目才能以最小的范围量出现。如果读者只在一栏中键入“中国历史”，那么有关“中国”和“历史”项目都会出现，比如，中国[经济](../Page/经济.md "wikilink")、中国[文化](../Page/文化.md "wikilink")、[美国](../Page/美国.md "wikilink")[历史等和读者期待不相关的内容也会出现](../Page/历史.md "wikilink")。或者还有一种简便的检索方法：分类法检索。读者可以通过所在图书馆的分类法直接找到“中国历史”这一项，再用相应的图书馆书籍编号去查阅具体的书籍。

4.
5.  利用OPAC进行检索，最常利用的检索途径是「主题词」。因为「主题词」是经过规范了的自然语言，一般能够揭示文献信息的中心含义，在不知道所要查找书目的篇名和著者的情况下，利用主题词检索，可以提高查全率。

## 举例

世界上最大的图书馆联盟[OCLC的](../Page/OCLC.md "wikilink")[Worldcat目录](../Page/Worldcat.md "wikilink")\[1\]，包含了其收录的全世界范围内170个国家，72000家图书馆的2亿6千余条书目数据。\[2\]
目录采用了“OPAC
2.0”的许多功能，包括关键词的检索方式、在搜索结果页的分面浏览、通过提供书目信息和内容提要，对资源信息的更好的解释等。同时，这个目录还采用了[FRBR的数据模型进行呈现](../Page/FRBR.md "wikilink")，方便对于资源之间的关系进行更好的揭示和导航。

## OPAC系統

  - [Evergreen](../Page/Evergreen_\(軟件\).md "wikilink")－
    一個[開放原始碼](../Page/開放原始碼.md "wikilink")（[GNU通用公共許可證](../Page/GNU通用公共許可證.md "wikilink")）的集成圖書館系統

  - [Koha](../Page/Koha.md "wikilink")－
    第一個以[自由軟體](../Page/自由軟體.md "wikilink")（[GNU通用公共許可證](../Page/GNU通用公共許可證.md "wikilink")）的整合式圖書館管理系統

  -
  -
  - [PCLIS](../Page/PCLIS.md "wikilink")

## 参考

[Category:圖書資訊科學](../Category/圖書資訊科學.md "wikilink")
[Category:文献检索数据库](../Category/文献检索数据库.md "wikilink")

1.  [<OCLC Worldcat数据库>](http://www.worldcat.org/)
2.  [<WorldCat facts and statistics: 截至2012-5-3>](http://www.oclc.org/worldcat/statistics/default.htm)