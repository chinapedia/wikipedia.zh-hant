[三多三路.JPG](https://zh.wikipedia.org/wiki/File:三多三路.JPG "fig:三多三路.JPG")

**三多路（Sanduo
Rd.）**是[臺灣](../Page/臺灣.md "wikilink")[高雄市的東西向重要幹道](../Page/高雄市.md "wikilink")。東於[澄清路](../Page/澄清路.md "wikilink")、[國泰路口接](../Page/國泰路.md "wikilink")[鳳山區自由路](../Page/鳳山區.md "wikilink")，西止於[成功路口](../Page/成功路_\(高雄市\).md "wikilink")。原在六十期重劃區開發後延伸至[海邊路](../Page/海邊路_\(高雄市\).md "wikilink")，此延伸段初定名為三多五路；惟維持一段時間後，該延伸段更改為海邊路之延伸段，海邊路與三多路銜接於成功路。另外三多四路和[一心路](../Page/一心路.md "wikilink")、[中山路原有一個圓環](../Page/中山路_\(高雄市\).md "wikilink")，現為統整交通、蓋捷運，已改建，此原圓環地即為著名的[三多商圈](../Page/三多商圈.md "wikilink")。

## 行經行政區域

  - [苓雅區](../Page/苓雅區.md "wikilink")（部份路段為[苓雅區與](../Page/苓雅區.md "wikilink")[前鎮區界](../Page/前鎮區.md "wikilink")）

## 分段

三多路共分為四個部份：

  - 三多一路：東起於[澄清路](../Page/澄清路.md "wikilink")、[國泰路二段口接](../Page/國泰路.md "wikilink")[鳳山區](../Page/鳳山區.md "wikilink")[自由路](../Page/自由路_\(鳳山區\).md "wikilink")，西於[福德三路口接三多二路](../Page/福德路_\(高雄市\).md "wikilink")。
  - 三多二路：東於福德三路口接三多一路，西於[民權二路口接三多三路](../Page/民權路_\(高雄市\).md "wikilink")。
  - 三多三路：東於民權二路口接三多二路，西於[中山二路及](../Page/中山路_\(高雄市\).md "wikilink")[一心二路口接三多四路](../Page/一心路.md "wikilink")。
  - 三多四路：東於中山二路及一心二路口接三多三路，西抵[成功路口接](../Page/成功路_\(高雄市\).md "wikilink")[海邊路](../Page/海邊路_\(高雄市\).md "wikilink")。

## 歷史

語出《[莊子](../Page/莊子.md "wikilink")‧天地》：「使聖人富，使聖人壽，使聖人多男子」，取多福（「多富」因為音似轉稱「多福」）、多壽、多男子（俗稱[財子壽](../Page/財子壽.md "wikilink")）的意思，是祝頌的話，沿用至今。

2014年7月31日深夜因發生[高雄氣爆事故的影響](../Page/2014年高雄氣爆事故.md "wikilink")，造成三多一路從[武營路口](../Page/武營路.md "wikilink")（[國道一號](../Page/中山高速公路.md "wikilink")[高雄交流道三多路出口](../Page/高雄交流道.md "wikilink")）至[凱旋路口無法通行](../Page/凱旋路_\(高雄市\).md "wikilink")。歷經3個月道路重建的日夜趕工之後，於同年11月20日午夜12時完工同時開放通車\[1\]。

## 沿線設施

（由東至西）

  - 三多一路
      - [高雄市公車建軍站](../Page/高雄市公車.md "wikilink")
      - [高雄捷運](../Page/高雄捷運.md "wikilink")[衛武營站](../Page/衛武營站.md "wikilink")
      - [衛武營藝術文化中心](../Page/衛武營藝術文化中心.md "wikilink")
      - [衛武營都會公園北側入口](../Page/衛武營都會公園.md "wikilink")
      - 中正公園
      - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[中山高速公路](../Page/中山高速公路.md "wikilink")[高雄交流道三多路出口](../Page/高雄交流道.md "wikilink")
      - 中正體育場
      - [全家便利商店高雄武慶店](../Page/全家便利商店.md "wikilink")（因受[高雄氣爆事故影響](../Page/2014年臺灣高雄氣爆事故.md "wikilink")，已停業）
      - [高雄市政府消防局第一大隊](../Page/高雄市政府消防局.md "wikilink")、苓雅分隊
      - [高雄市政府警察局苓雅分局三多路派出所](../Page/高雄市政府警察局.md "wikilink")
      - 五塊厝郵局
      - [苓雅區戶政事務所第二辦公處](../Page/苓雅區.md "wikilink")
      - [高雄市農會苓雅辦事處](../Page/高雄市農會.md "wikilink")
      - [三信家商](../Page/三信家商.md "wikilink")
      - 三多時尚球館
      - [三商百貨三多店](../Page/三商百貨.md "wikilink")
      - 小北百貨三多店
      - [鞋全家福苓雅店](../Page/鞋全家福.md "wikilink")

<!-- end list -->

  - 三多二路
      - [台灣中油加油站三多二路站](../Page/台灣中油.md "wikilink")
      - [國際商工](../Page/國際商工.md "wikilink")
      - 丹丹漢堡國際店
      - [高雄環狀輕軌](../Page/高雄環狀輕軌.md "wikilink")[五權國小站預定地](../Page/五權國小站.md "wikilink")
      - [高雄市苓雅區五權國小](../Page/高雄市苓雅區五權國民小學.md "wikilink")
      - 22號綠地公園（英明路口）
      - [高雄銀行三多分行](../Page/高雄銀行.md "wikilink")
      - 全國加油站三多路站
      - 林華郵局
      - 乃榮醫院
      - [高雄市第三信用合作社三多分社](../Page/高雄市第三信用合作社.md "wikilink")
      - [全國電子光華門市](../Page/全國電子.md "wikilink")
      - [台灣中油光華加油站](../Page/台灣中油.md "wikilink")

<!-- end list -->

  - 三多三路
      - 生力美食三多店
      - [合作金庫銀行南高雄分行](../Page/合作金庫銀行.md "wikilink")
      - 廣澤郵局
      - [華南銀行南高雄分行](../Page/華南銀行.md "wikilink")
      - [彰化銀行前鎮分行](../Page/彰化銀行.md "wikilink")
      - [第一銀行](../Page/第一銀行.md "wikilink") 前鎮分行
      - [元大銀行三多分行](../Page/元大銀行.md "wikilink")
      - [鞋全家福三多店](../Page/鞋全家福.md "wikilink")
      - [三多商圈](../Page/三多商圈.md "wikilink")
      - [新光三越高雄三多店](../Page/新光三越.md "wikilink")
      - [光南大批發高雄三多店](../Page/光南大批發.md "wikilink")
      - [星巴克三多門市](../Page/星巴克.md "wikilink")
      - [太平洋崇光百貨高雄店](../Page/太平洋崇光百貨.md "wikilink")
      - [台灣中油加油站三多三路站](../Page/台灣中油.md "wikilink")
      - [高雄市公共腳踏車租賃系統](../Page/高雄市公共腳踏車租賃系統.md "wikilink")
      - 捷運[三多商圈站](../Page/三多商圈站.md "wikilink")
          - 三多站：[苓雅區](../Page/苓雅區.md "wikilink")[中山二路與三多三路圓環處](../Page/中山路_\(高雄市\).md "wikilink")
  - 三多四路
      - 捷運[三多商圈站](../Page/三多商圈站.md "wikilink")
      - [遠東百貨](../Page/遠東百貨.md "wikilink")
      - [高雄意誠堂](../Page/高雄意誠堂.md "wikilink")
      - [威秀影城](../Page/威秀影城.md "wikilink")
      - [85大樓](../Page/85大樓.md "wikilink")
      - [三多影城](http://0982884976.blogspot.tw/)

## 公車資訊

  - 三多一路
      - [統聯客運](../Page/統聯客運.md "wikilink")（市區公車）
          - 11 [瑞豐站](../Page/瑞豐站.md "wikilink")－鹽埕埔
          - 53
            [建軍站](../Page/建軍站.md "wikilink")－[高雄應用科技大學](../Page/高雄應用科技大學.md "wikilink")－[高醫](../Page/高醫.md "wikilink")－[高雄火車站](../Page/高雄火車站.md "wikilink")
          - 88 建國幹線 鳳山轉運站（ 捷運[大東站](../Page/大東站.md "wikilink")）－建軍站（
            捷運[衛武營站](../Page/衛武營站.md "wikilink")）－鹽埕埔
          - 100 瑞豐站－高雄火車站
          - 紅21 建軍站（ 捷運衛武營站）－ 捷運[三多商圈站](../Page/三多商圈站.md "wikilink")
          - 橘8 建軍站（
            捷運衛武營站）－[ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")[鳳山車站](../Page/鳳山車站.md "wikilink")－鳳山轉運站（
            捷運大東站）－過埤派出所
          - 橘10 建軍站（ 捷運衛武營站）－鳳山轉運站（ 捷運大東站）
      - [東南客運](../Page/東南客運.md "wikilink")
          - 248 建軍站－[鼓山輪渡站](../Page/鼓山輪渡站.md "wikilink")
          - 橘7A 建軍站（ 捷運[衛武營站](../Page/衛武營站.md "wikilink")）－舊鐵橋濕地－大樹區公所
          - 橘7B 建軍站（ 捷運衛武營站）－大樹區公所－[佛陀紀念館](../Page/佛光山.md "wikilink")
      - [港都客運](../Page/港都客運.md "wikilink")
          - 70 三多幹線 [前鎮站](../Page/前鎮站.md "wikilink")－長庚醫院
          - 50 五福幹線
            [建軍站](../Page/建軍站.md "wikilink")－[鼓山輪渡站](../Page/鼓山輪渡站.md "wikilink")
          - 52 建軍站－[高雄火車站](../Page/高雄火車站.md "wikilink")
          - 73 建軍站－左營區公所
          - 81
            [瑞豐站](../Page/瑞豐站.md "wikilink")－[育英醫專](../Page/育英醫護管理專科學校.md "wikilink")
      - [高雄客運](../Page/高雄客運.md "wikilink")
          - E11 鳳山高鐵城市快線 中崙國中－鳳山轉運站－台鐵新左營站
          - 87 誠義里－中崙－鳳山－[建軍站](../Page/建軍站.md "wikilink")
          - 8001
            高雄－鳳山－林園（經[高雄市政府四維行政中心](../Page/高雄市政府.md "wikilink")、[雄商](../Page/高雄市立高雄高級商業職業學校.md "wikilink")）
          - 橘11 林園站－鳳山－建軍站

<!-- end list -->

  - 三多二路
      - [統聯客運](../Page/統聯客運.md "wikilink")（市區公車）
          - 11 [瑞豐站](../Page/瑞豐站.md "wikilink")－鹽埕埔
          - 100 瑞豐站－[高雄火車站](../Page/高雄火車站.md "wikilink")
      - [港都客運](../Page/港都客運.md "wikilink")
          - 52
            [建軍站](../Page/建軍站.md "wikilink")－[高雄火車站](../Page/高雄火車站.md "wikilink")
          - 70 三多幹線 [前鎮站](../Page/前鎮站.md "wikilink")－長庚醫院
          - 81
            [瑞豐站](../Page/瑞豐站.md "wikilink")－[育英醫專](../Page/育英醫護管理專科學校.md "wikilink")
      - [東南客運](../Page/東南客運.md "wikilink")
          - 紅16 [高雄軟科園區](../Page/高雄軟體科技園區.md "wikilink")－
            捷運[獅甲站](../Page/獅甲站.md "wikilink")－
            捷運[三多商圈站](../Page/三多商圈站.md "wikilink")
      - [高雄客運](../Page/高雄客運.md "wikilink")
          - 8001
            高雄－鳳山－林園（經[高雄市政府四維行政中心](../Page/高雄市政府.md "wikilink")、[雄商](../Page/高雄市立高雄高級商業職業學校.md "wikilink")）

<!-- end list -->

  - 三多三路
      - [港都客運](../Page/港都客運.md "wikilink")
          - 70 三多幹線 [前鎮站](../Page/前鎮站.md "wikilink")－長庚醫院
      - [統聯客運](../Page/統聯客運.md "wikilink")（市區公車）
          - 83
            [瑞豐站](../Page/瑞豐站.md "wikilink")－[高雄火車站](../Page/高雄火車站.md "wikilink")
          - 100 瑞豐站－高雄火車站
          - 紅21
            捷運[三多商圈站](../Page/三多商圈站.md "wikilink")－[建軍站](../Page/建軍站.md "wikilink")
      - [東南客運](../Page/東南客運.md "wikilink")
          - 紅16 [高雄軟科園區](../Page/高雄軟體科技園區.md "wikilink")－
            捷運[獅甲站](../Page/獅甲站.md "wikilink")－
            捷運[三多商圈站](../Page/三多商圈站.md "wikilink")
          - 紅18 一心幹線 中崙社區－
            捷運[三多商圈站](../Page/三多商圈站.md "wikilink")－[實踐大學高雄城區中心](../Page/實踐大學.md "wikilink")
      - [南台灣客運](../Page/南台灣客運.md "wikilink")
          - 民族幹線
            捷運[三多商圈站](../Page/三多商圈站.md "wikilink")－[ROC_Taiwan_Railways_Administration_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Taiwan_Railways_Administration_Logo.svg "fig:ROC_Taiwan_Railways_Administration_Logo.svg")
            [高鐵左營站](../Page/新左營車站.md "wikilink")

<!-- end list -->

  - 三多四路
      - [港都客運](../Page/港都客運.md "wikilink")
          - 70 三多幹線 [前鎮站](../Page/前鎮站.md "wikilink")－長庚醫院
      - [統聯客運](../Page/統聯客運.md "wikilink")（市區公車）
          - 83
            [瑞豐站](../Page/瑞豐站.md "wikilink")－[高雄火車站](../Page/高雄火車站.md "wikilink")
          - 100 瑞豐站－高雄火車站
      - [東南客運](../Page/東南客運.md "wikilink")
          - 紅16 [高雄軟科園區](../Page/高雄軟體科技園區.md "wikilink")－
            捷運[獅甲站](../Page/獅甲站.md "wikilink")－
            捷運[三多商圈站](../Page/三多商圈站.md "wikilink")
          - 紅18 一心幹線 中崙社區－
            捷運[三多商圈站](../Page/三多商圈站.md "wikilink")－[實踐大學高雄城區中心](../Page/實踐大學.md "wikilink")

## 來源

[Category:苓雅區](../Category/苓雅區.md "wikilink")
[Category:前鎮區](../Category/前鎮區.md "wikilink")
[Category:高雄市街道](../Category/高雄市街道.md "wikilink")

1.  [沒有慶祝！高雄氣爆道路修復
    凌晨開放通車](http://news.ltn.com.tw/index.php/news/life/breakingnews/1161793).
    [自由時報](../Page/自由時報.md "wikilink"). \[2014-11-21\].