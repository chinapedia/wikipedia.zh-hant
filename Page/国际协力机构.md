[Jica_hyogo01s3200.jpg](https://zh.wikipedia.org/wiki/File:Jica_hyogo01s3200.jpg "fig:Jica_hyogo01s3200.jpg")

**獨立行政法人国际协力机构**（），成立于2003年10月，是日本对外实施[政府开发援助](../Page/政府开发援助.md "wikilink")（ODA）的主要执行机构之一，隶属于[日本](../Page/日本.md "wikilink")[外务省](../Page/外務省_\(日本\).md "wikilink")，其前身是成立于1974年日本国际协力事业团。

JICA的资金全部来源于政府财政预算。2004年度，JICA承担政府开发援助资金总额为1612亿[日元](../Page/日元.md "wikilink")，占当年全部ODA[预算](../Page/预算.md "wikilink")（8169亿日元）的19.7%。

JICA在全球54个国家设有事务所，中国事务所设立于1982年，但自1979年JICA即与中国开始进行合作，合作领域涵盖[环境保护](../Page/环境保护.md "wikilink")、农林水产业、[医疗](../Page/医疗.md "wikilink")[保健](../Page/保健.md "wikilink")、[教育](../Page/教育.md "wikilink")、工矿业、[能源](../Page/能源.md "wikilink")、[运输](../Page/运输.md "wikilink")、[交通](../Page/交通.md "wikilink")、[通讯等诸多方面](../Page/通讯.md "wikilink")。

## 参见

  - [国际协力银行](../Page/国际协力银行.md "wikilink")
  - [国际交流基金](../Page/国际交流基金.md "wikilink")
  - [贸易振兴机构](../Page/贸易振兴机构.md "wikilink")
  - [海外渔业协力事业团](../Page/海外渔业协力事业团.md "wikilink")

## 外部链接

  - [JICA网站](http://www.jica.go.jp/)

[Category:戰後日本外交](../Category/戰後日本外交.md "wikilink")
[Category:外務省](../Category/外務省.md "wikilink")
[Category:人道援助组织](../Category/人道援助组织.md "wikilink")
[Category:千代田區](../Category/千代田區.md "wikilink")
[Category:2003年建立的组织](../Category/2003年建立的组织.md "wikilink")
[Category:国际协力机构](../Category/国际协力机构.md "wikilink")
[Category:外援](../Category/外援.md "wikilink")