《**秋瑾**》（Qiu Jin: A Woman To
Remember）是香港無綫電視1984年拍攝的電視連續劇，全劇共20集，監製[蕭笙](../Page/蕭笙.md "wikilink")，主要演員有[汪明荃](../Page/汪明荃.md "wikilink")、[謝賢](../Page/謝賢.md "wikilink")、[劉江](../Page/劉江_\(香港\).md "wikilink")、[任達華](../Page/任達華.md "wikilink")、[楊澤霖等](../Page/楊澤霖.md "wikilink")。

## 故事大綱

1905年，秋氏東渡日本；後於翌年回國，至1907年，秋瑾在華東發生起義，並於7月15日處死，而她的死亡間接促成辛亥革命。

## 演員表

  - [汪明荃](../Page/汪明荃.md "wikilink") 飾 [秋　瑾](../Page/秋瑾.md "wikilink")
  - [謝　賢](../Page/謝賢.md "wikilink") 飾 [王廷鈞](../Page/王廷鈞.md "wikilink")
  - [區艷蓮](../Page/區艷蓮.md "wikilink")
  - [潘宏彬](../Page/潘宏彬.md "wikilink")
  - [梁潔華](../Page/梁潔華_\(演員\).md "wikilink") 飾 小　玉
  - [歐陽震華](../Page/歐陽震華.md "wikilink") 飾 林　寬
  - [黃素娟](../Page/黃素娟.md "wikilink")
  - [霍潔貞](../Page/霍潔貞.md "wikilink")
  - [陳嘉碧](../Page/陳嘉碧.md "wikilink")
  - [甘永禧](../Page/甘永禧.md "wikilink")
  - [陳　東](../Page/陳東_\(香港演員\).md "wikilink")
  - [梅智清](../Page/梅智清.md "wikilink")
  - [張英才](../Page/張英才.md "wikilink") 飾 秋壽南（秋瑾父）
  - [蘇杏璇](../Page/蘇杏璇.md "wikilink") 飾 秋　母
  - [陳安瑩](../Page/陳安瑩.md "wikilink") 飾 香　蓮（秋瑾婢女）
  - [梁鴻華](../Page/梁鴻華.md "wikilink") 蝕 啟　生（金逸之會計）
  - [池佩芬](../Page/池佩芬.md "wikilink")
  - [白　蘭](../Page/白蘭.md "wikilink")
  - [余慕蓮](../Page/余慕蓮.md "wikilink")
  - [何鳳儀](../Page/何鳳儀.md "wikilink")
  - [麥飛霏](../Page/麥飛霏.md "wikilink")
  - [文秀言](../Page/文秀言.md "wikilink")
  - [吳博君](../Page/吳博君.md "wikilink")
  - [呂靜紅](../Page/呂靜紅.md "wikilink")
  - [梁靜文](../Page/梁靜文.md "wikilink")
  - [許逸華](../Page/許逸華.md "wikilink") 飾 芝　玲（王廷鈞情人）
  - [鄧汝超](../Page/鄧汝超.md "wikilink")
  - [孫季卿](../Page/孫季卿.md "wikilink")
  - [鄭少萍](../Page/鄭少萍.md "wikilink")
  - [陳志雄](../Page/陳志雄.md "wikilink")
  - [俞　明](../Page/俞明.md "wikilink")
  - [石中玉](../Page/石中玉.md "wikilink")
  - [石毅魂](../Page/石毅魂.md "wikilink")
  - [陳玉麟](../Page/陳玉麟.md "wikilink")
  - [王靜儀](../Page/王靜儀.md "wikilink")
  - [劉　江](../Page/劉江.md "wikilink") 飾
    [金逸之](../Page/金逸之.md "wikilink")（[清末](../Page/清朝.md "wikilink")[成親王之子](../Page/成親王.md "wikilink")，[愛新覺羅恩榮](../Page/愛新覺羅恩榮.md "wikilink")）
  - [馬宗德](../Page/馬宗德.md "wikilink")
  - [黃素媚](../Page/黃素媚.md "wikilink")
  - [譚一清](../Page/譚一清.md "wikilink")
  - [張志強](../Page/張志強_\(演員\).md "wikilink")
  - [虞天偉](../Page/虞天偉.md "wikilink")
  - [秦覺廉](../Page/秦覺廉.md "wikilink")
  - [徐新義](../Page/徐新義.md "wikilink")
  - [羅國維](../Page/羅國維.md "wikilink") 飾 [貴　福](../Page/貴福.md "wikilink")
  - [黃振文](../Page/黃振文.md "wikilink")
  - [胡芳齡](../Page/胡芳齡.md "wikilink")
  - [陳栢燊](../Page/陳栢燊.md "wikilink")
  - [白　蘭](../Page/白蘭.md "wikilink")
  - [何璧堅](../Page/何璧堅.md "wikilink")
  - [劉國誠](../Page/劉國誠.md "wikilink") 飾 [本　明](../Page/本明和尚.md "wikilink")
  - [郭　鋒](../Page/郭鋒.md "wikilink") 飾 [朱紅燈](../Page/朱紅燈.md "wikilink")
  - [凌禮文](../Page/凌禮文.md "wikilink")
  - [白文彪](../Page/白文彪.md "wikilink") 飾 威爾遜
  - [佩　雲](../Page/佩雲.md "wikilink")
  - [何　雲](../Page/何雲.md "wikilink")
  - [龍天生](../Page/龍天生.md "wikilink") 飾 王廷鈞[書僮](../Page/書僮.md "wikilink")
  - [源子才](../Page/源子才.md "wikilink")
  - [謝子揚](../Page/謝子揚.md "wikilink")
  - [陳榮輝](../Page/陳榮輝.md "wikilink")
  - [梁　雄](../Page/梁雄.md "wikilink")
  - [黃宗賜](../Page/黃宗賜.md "wikilink")
  - [劉偉海](../Page/劉偉海.md "wikilink")
  - [王家駒](../Page/王家駒.md "wikilink")
  - [謝　雄](../Page/謝雄.md "wikilink")
  - [李青薇](../Page/李青薇.md "wikilink")
  - [任達華](../Page/任達華.md "wikilink") 飾 [陳少白](../Page/陳少白.md "wikilink")
  - [何廣倫](../Page/何廣倫.md "wikilink") 飾 汪烈
  - [林文偉](../Page/林文偉.md "wikilink")
  - [高　雄](../Page/高雄_\(演員\).md "wikilink")
  - [麥子雲](../Page/麥子雲.md "wikilink")
  - [江志深](../Page/江志深.md "wikilink")
  - [麥皓為](../Page/麥皓為.md "wikilink")
  - [劉青雲](../Page/劉青雲.md "wikilink") 飾 [楊鶴齡](../Page/楊鶴齡.md "wikilink")
  - [吳華山](../Page/吳華山.md "wikilink")
  - [高妙思](../Page/高妙思.md "wikilink") 飾 翠　鳳（金逸之前女友）
  - [葉天行](../Page/葉天行.md "wikilink") 飾 [湯壽潛](../Page/湯壽潛.md "wikilink")
  - [連偉健](../Page/連偉健.md "wikilink")
  - [關灼元](../Page/關灼元.md "wikilink")
  - [馬慶生](../Page/馬慶生.md "wikilink")
  - [曾楚霖](../Page/曾楚霖.md "wikilink")
  - [何貴林](../Page/何貴林.md "wikilink")
  - [張志強](../Page/張志強_\(演員\).md "wikilink")
  - [梁潔芳](../Page/梁潔芳.md "wikilink") 飾 貴夫人
  - [談泉慶](../Page/談泉慶.md "wikilink")
  - [傅玉蘭](../Page/傅玉蘭.md "wikilink") 飾 廉夫人
  - [李建川](../Page/李建川.md "wikilink")
  - [羅麗娟](../Page/羅麗娟.md "wikilink")
  - [楊澤霖](../Page/楊澤霖.md "wikilink") 飾 [徐錫麟](../Page/徐錫麟.md "wikilink")
  - 張文光
  - [吳業光](../Page/吳業光.md "wikilink")
  - [曹　濟](../Page/曹濟.md "wikilink")
  - \-{[陳有后](../Page/陳有后.md "wikilink")}- 飾
    [恩　銘](../Page/恩銘.md "wikilink")
  - [劉妙玲](../Page/劉妙玲.md "wikilink")
  - [蔡麗薇](../Page/蔡麗薇.md "wikilink")
  - [葉麗霞](../Page/葉麗霞.md "wikilink")
  - [梁志芳](../Page/梁志芳.md "wikilink")
  - [龔慈恩](../Page/龔慈恩.md "wikilink") 飾 服部繁子（鈴木先生姪女）
  - [李樹佳](../Page/李樹佳.md "wikilink") 飾 [胡道南](../Page/胡道南.md "wikilink")
  - [朱　剛](../Page/朱剛.md "wikilink") 飾 鈴木先生
  - [李國平](../Page/李國平_\(演員\).md "wikilink")
  - [徐　僖](../Page/徐僖.md "wikilink") 飾
    [吳　樾](../Page/吳樾_\(革命家\).md "wikilink")
  - [梁少狄](../Page/梁少狄.md "wikilink") 飾 [馮自由](../Page/馮自由.md "wikilink")
  - [虞天偉](../Page/虞天偉.md "wikilink")
  - [黃思恩](../Page/黃思恩.md "wikilink")
  - [關　鍵](../Page/關鍵.md "wikilink") 飾 市　長
  - [林民偉](../Page/林民偉.md "wikilink") 飾 公　使
  - [謝子揚](../Page/謝子揚.md "wikilink")
  - [吳啟華](../Page/吳啟華.md "wikilink")
  - [黎壁光](../Page/黎壁光.md "wikilink")
  - [梁　珊](../Page/梁珊.md "wikilink") 飾
    徐振漢（[徐錫麟妻](../Page/徐錫麟.md "wikilink")）
  - [吳博君](../Page/吳博君.md "wikilink")
  - [陶大宇](../Page/陶大宇.md "wikilink") 飾
    革命黨[團練成員](../Page/團練.md "wikilink")（龍套）
  - [鮑偉亮](../Page/鮑偉亮.md "wikilink")
  - [黃宗賜](../Page/黃宗賜.md "wikilink") 飾 王沅德
  - [馬宗德](../Page/馬宗德.md "wikilink")

[Category:1984年無綫電視劇集](../Category/1984年無綫電視劇集.md "wikilink")
[Category:無綫電視光緒時期背景劇集](../Category/無綫電視光緒時期背景劇集.md "wikilink")
[Category:傳記電視劇](../Category/傳記電視劇.md "wikilink")
[Category:女性主義電視連續劇](../Category/女性主義電視連續劇.md "wikilink")