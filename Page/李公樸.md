[Li_Gongpu.jpg](https://zh.wikipedia.org/wiki/File:Li_Gongpu.jpg "fig:Li_Gongpu.jpg")
[Li_Gongpu1.jpg](https://zh.wikipedia.org/wiki/File:Li_Gongpu1.jpg "fig:Li_Gongpu1.jpg")
**李公樸**（），原名**李永祥**、號**晉祥**，後改名**公樸**，號**什如**，筆名**長嘯**。原籍[江蘇](../Page/江蘇.md "wikilink")[武進](../Page/武進.md "wikilink")，生於江蘇[淮安](../Page/淮安.md "wikilink")。[中国社會教育家](../Page/中国.md "wikilink")，[中国民主同盟的发起人和领导人之一](../Page/中国民主同盟.md "wikilink")。

## 生平

李公樸生於江蘇[淮安](../Page/淮安.md "wikilink")，家境貧困，曾在[鎮江當過學徒](../Page/鎮江.md "wikilink")。先後就讀於鎮江[潤州中學](../Page/潤州中學.md "wikilink")、[武昌](../Page/武昌.md "wikilink")[文華大學附中](../Page/文華大學附中.md "wikilink")，1925年入[滬江大學學習](../Page/滬江大學.md "wikilink")，受[孫中山革命民主主義和](../Page/孫中山.md "wikilink")[新文化運動的影響](../Page/新文化運動.md "wikilink")，于同年加入[中國國民黨](../Page/中國國民黨.md "wikilink")。[五卅运动中](../Page/五卅运动.md "wikilink")，代表[沪江大学学生团体参加](../Page/沪江大学.md "wikilink")[上海学联](../Page/上海学联.md "wikilink")，任工人科长。1926年初，李公樸离开学校，赴[广州任职于](../Page/广州.md "wikilink")[国民革命军东路前敌总指挥部政治部](../Page/国民革命军.md "wikilink")。1927年5月，任国民革命军东路前敌总政治部沪宁路属党政特派员。后来，因不满国民党“[清党](../Page/清党.md "wikilink")”，李公樸离开军队。1928年8月赴[美国](../Page/美国.md "wikilink")[俄勒岡州](../Page/俄勒岡州.md "wikilink")[雷德大學（现译作里德学院）政治系學習](../Page/里德学院.md "wikilink")。1930年夏，李公樸毕业后，自费赴[纽约和](../Page/纽约.md "wikilink")[欧洲考察](../Page/欧洲.md "wikilink")。同年冬，经欧洲归国。\[1\]

归國后，李公樸任[環球新聞社社長](../Page/環球新聞社.md "wikilink")。1932年出版《申報月刊》、《申報年鑑》，並創辦《申報》業餘婦女補習學校與《申報》流通圖書館，1933年創辦《申報》業餘補習學校。1934年11月10日與[柳湜](../Page/柳湜.md "wikilink")、[艾思奇在上海共同創辦](../Page/艾思奇.md "wikilink")《[讀書生活](../Page/讀書生活.md "wikilink")》半月刊。1936年，与[邹韬奋等人共同創辦](../Page/邹韬奋.md "wikilink")[讀書生活出版社](../Page/讀書生活出版社.md "wikilink")，當選為[全國各界救國會聯合會執行委員](../Page/全國各界救國會聯合會.md "wikilink")、常務委員。他發表宣言，提出“停止一切內戰”，“釋放一切政治犯”。1936年11月23日，國民政府以“危害民國”的罪名，逮捕了救國會領導人[沈钧儒](../Page/沈钧儒.md "wikilink")、李公樸、[章乃器](../Page/章乃器.md "wikilink")、[鄒韜奮](../Page/鄒韜奮.md "wikilink")、[史良](../Page/史良.md "wikilink")、[王造時](../Page/王造時.md "wikilink")、[沙千里等七人](../Page/沙千里.md "wikilink")，史稱“[七君子事件](../Page/七君子事件.md "wikilink")”。[抗日战争爆發後](../Page/抗日战争.md "wikilink")，1937年7月他们被釋放出獄。\[2\]

1937年12月，李公樸在[武漢與鄒韜奮](../Page/武漢.md "wikilink")、[沈鈞儒](../Page/沈鈞儒.md "wikilink")、[陶行知一起創辦](../Page/陶行知.md "wikilink")《全民抗戰》三日刊，建立[全民通讯社](../Page/全民通讯社.md "wikilink")。同年，任[第二战区民族革命战争战地总动员委员会委员和宣传部长](../Page/第二战区民族革命战争战地总动员委员会.md "wikilink")。1937年底，應[閻錫山邀請](../Page/閻錫山.md "wikilink")，任[山西民族革命大學副校長](../Page/山西民族革命大學.md "wikilink")。1938年11月，赴[延安参观](../Page/延安.md "wikilink")，会见了[毛泽东及其他](../Page/毛泽东.md "wikilink")[中共中央领导人](../Page/中共中央.md "wikilink")。在[中国共产党支持下](../Page/中国共产党.md "wikilink")，李公樸组建“抗战建国教学团”，在[晋察冀边区](../Page/晋察冀边区.md "wikilink")、[晋冀鲁豫边区培训抗日宣传人员](../Page/晋冀鲁豫边区.md "wikilink")。\[3\]

1941年，李公樸到达[雲南](../Page/雲南.md "wikilink")[昆明](../Page/昆明.md "wikilink")，在昆明組織“[青年讀書會](../Page/青年讀書會.md "wikilink")”，出版《青年周刊》。1942年，在昆明创办創辦[北門書屋](../Page/北門書屋.md "wikilink")，傳播[馬列主义思想](../Page/馬列主义.md "wikilink")；1943年創辦[北門出版社](../Page/北門出版社.md "wikilink")。1944年10月，[中国民主同盟云南省支部在昆明成立](../Page/中国民主同盟.md "wikilink")，李公朴当选支部执行委员。1945年10月1日當選[中国民主同盟中央執行委員](../Page/中国民主同盟.md "wikilink")，还兼任中国民主同盟民主教育委员会副主任委员。他还担任[中国人民救国会中央委员](../Page/中国人民救国会.md "wikilink")。\[4\]

1946年1月與[陶行知在重慶創辦](../Page/陶行知.md "wikilink")[社會大學](../Page/社會大學.md "wikilink")，宗旨是“人民創造大社會，社會變成大學堂”，李公樸任副校長兼教務長。1946年2月10日發生“[校場口事件](../Page/校場口事件.md "wikilink")”，大会主席团成员、大会总指挥李公樸當場被打得頭破血流。同年5月，李公樸在伤愈后自重庆返回昆明。1946年7月11日，李公樸在[昆明](../Page/昆明.md "wikilink")[大興街學院坡被开枪行刺](../Page/大興街.md "wikilink")，于7月12日凌晨在云南大学医院因抢救无效逝世，年僅44歲。\[5\]

1946年7月15日[聞一多在悼念李公樸先生大會上](../Page/聞一多.md "wikilink")，怒斥暗殺罪行，發表了著名的《[最後一次的講演](../Page/最後一次的講演.md "wikilink")》，當天下午被暗杀。

## 著作

著有《華北敵後－晉察冀》、《全民動員論》、《青年之路》、《抗戰教育的理論與實踐》、《走上勝利之路的山西》等。

## 参考文献

## 外部链接

  - [暗杀李闻两公的主谋](https://archive.is/20130427001703/http://www.luobinghui.com/ld/zx/wyd/jn/200607/14413.html)

## 参见

  - [聞一多](../Page/聞一多.md "wikilink")
  - [史量才](../Page/史量才.md "wikilink")

{{-}}

[Category:常州人](../Category/常州人.md "wikilink")
[Category:淮安人](../Category/淮安人.md "wikilink")
[Category:滬江大學校友](../Category/滬江大學校友.md "wikilink")
[Category:中華民國大陸時期人物](../Category/中華民國大陸時期人物.md "wikilink")
[Category:中華民國教育家](../Category/中華民國教育家.md "wikilink")
[Category:中华民国大陆时期遇刺身亡者](../Category/中华民国大陆时期遇刺身亡者.md "wikilink")
[Gong公樸](../Category/李姓.md "wikilink")
[Category:中華民國持不同政見者](../Category/中華民國持不同政見者.md "wikilink")

1.  [李公朴，国际在线，2008-03-27](http://gb.cri.cn/18824/2008/03/27/1545@1997449.htm)

2.
3.
4.
5.