**星捧月科**也被称为**无叶花科**，只包含一[属](../Page/属.md "wikilink")（*Aphyllanthes*）一[种](../Page/种.md "wikilink")，是分布在[地中海沿岸的](../Page/地中海.md "wikilink")**星捧月**（[学名](../Page/学名.md "wikilink")：**），也叫**无叶花**。

## 分類

在[克朗奎斯特分类法中](../Page/克朗奎斯特分类法.md "wikilink")**无叶花属**被分在[百合科下](../Page/百合科.md "wikilink")。1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其单独分为一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，2003年经过修订的[APG
II
分类法认为这个科也可以和](../Page/APG_II_分类法.md "wikilink")[天门冬科选择合并](../Page/天门冬科.md "wikilink")。

《[被子植物APG III分类法](../Page/被子植物APG_III分类法.md "wikilink")》(2009年)
則傾向將**星捧月科**併入**廣義的[天門冬科](../Page/天門冬科.md "wikilink")**，而將本科以[亞科方式表示](../Page/亞科.md "wikilink")\[1\]：**Aphyllanthoideae**
Lindley\[2\]。

## 注釋

<references />

## 參考文獻

  - Angiosperm phylogeny group. 2003. [An update of the Angiosperm
    Phylogeny Group classification for the orders and families of
    flowering plants: APG
    II](http://onlinelibrary.wiley.com/doi/10.1046/j.1095-8339.2003.t01-1-00158.x/pdf).
    Botanical Journal of the Linnean Society 141: 399–436.
  - Angiosperm phylogeny group. 2009. [An update of the Angiosperm
    Phylogeny Group classification for the orders and families of
    flowering plants: APG
    III](http://onlinelibrary.wiley.com/doi/10.1111/j.1095-8339.2009.00996.x/pdf).
    Botanical Journal of the Linnean Society 161: 105–121.
  - Stevens, P. F. (2001 onwards). Angiosperm Phylogeny Website. Version
    9, June 2008 \[and more or less continuously updated since\].
    [Angiosperm Phylogeny
    Website](http://www.mobot.org/MOBOT/research/APweb) cached
    2011.05.26.

## 外部链接

  - [NCBI分类中的星捧月科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=59002&lvl=3&lin=f&keep=1&srchmode=1&unlock)
  - [links at
    CSDL分类中的星捧月科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Aphyllanthaceae)

[\*](../Category/星捧月科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.  [APG
    III](http://onlinelibrary.wiley.com/doi/10.1046/j.1095-8339.2003.t01-1-00158.x/pdf)，Botanical
    Journal of the Linnean Society 161: 109.
2.  [Angiosperm Phylogeny Website:
    **Asparagaceae**](http://www.mobot.org/MOBOT/Research/APweb/welcome.html)，retrieved
    2011-06-28。