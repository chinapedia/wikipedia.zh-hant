[CWB_Wing_Lung_Bank_Building.jpg](https://zh.wikipedia.org/wiki/File:CWB_Wing_Lung_Bank_Building.jpg "fig:CWB_Wing_Lung_Bank_Building.jpg")
[Wing_Lung_Bank,_Tsuen_Wan_Branch_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Wing_Lung_Bank,_Tsuen_Wan_Branch_\(Hong_Kong\).jpg "fig:Wing_Lung_Bank,_Tsuen_Wan_Branch_(Hong_Kong).jpg")[荃灣](../Page/荃灣.md "wikilink")[沙咀道](../Page/沙咀道.md "wikilink")251號的永隆銀行荃灣分行（2013年未改名）\]\]

**招商永隆銀行**創立於1933年，是香港具悠久歷史的[華資銀行之一](../Page/華資.md "wikilink")，素持「進展不忘穩健、服務必盡忠誠」之宗旨，為客戶提供全面銀行服務，包括[存款](../Page/存款.md "wikilink")、[貸款](../Page/貸款.md "wikilink")、私人銀行及財富管理、投資理財、證券、信用卡、網上銀行、銀團貸款、企業貸款、押匯、租購貸款、匯兌、保險代理、強制性公積金等。永隆銀行更透過全資附屬公司提供期貨經紀、保險經紀及一般保險承保、物業管理、信託、受託代管，以及資產管理等服務。

2018年10月1日，永隆銀行改名為招商永隆銀行。\[1\]

## 歷史

永隆於[1933年](../Page/1933年.md "wikilink")[2月25日在香港文咸東街](../Page/2月25日.md "wikilink")37號開業，定名**永隆銀號**，資本港幣44,500[港元](../Page/港元.md "wikilink")，經營找換、匯兌、存款、各埠來往、代客買賣股票、黃金及國內公債等業務，成績按年遞增。

[1937年](../Page/1937年.md "wikilink")﹐為擴充營業，遷至香港[皇后大道中](../Page/皇后大道中.md "wikilink")112號。

[1941年](../Page/1941年.md "wikilink")，[太平洋戰爭爆發](../Page/太平洋戰爭.md "wikilink")，[香港淪陷](../Page/香港淪陷.md "wikilink")。永隆轉往[澳門繼續經營銀業](../Page/澳門.md "wikilink")，及在[廣西](../Page/廣西.md "wikilink")[柳州設立](../Page/柳州.md "wikilink")「永隆金號」。

[1945年](../Page/1945年.md "wikilink")，戰事結束，回港復業。

[1952年](../Page/1952年.md "wikilink")，經營[押匯業務](../Page/押匯.md "wikilink")；同年舊總行行址重建落成。

[1956年](../Page/1956年.md "wikilink")，註冊為有限公司。由於業務發展，職員增多，原有行址已不敷用，乃將預置的右鄰和店後舖位重建，擴大營業。

[1960年](../Page/1960年.md "wikilink")，中文名稱由「永隆銀號有限公司」改稱「永隆銀行有限公司」。

[1965年](../Page/1965年.md "wikilink")-為配合業務發展，將原總行行址作第3次重建。

[1973年](../Page/1973年.md "wikilink")-位於[德輔道中連貫](../Page/德輔道中.md "wikilink")[干諾道中之總行新大廈落成](../Page/干諾道中.md "wikilink")，適值本行成立40周年。總行大廈樓高300呎，共25層，總面積145,000餘呎。是年改為公共有限公司。[渣打銀行加入為本行股東](../Page/渣打銀行.md "wikilink")。

[1975年](../Page/1975年.md "wikilink")，另一物業－[旺角](../Page/旺角.md "wikilink")[彌敦道之](../Page/彌敦道.md "wikilink")[銀行中心亦告落成](../Page/銀行中心.md "wikilink")，大廈樓高300呎，共25層，總面積245,000餘呎，同時設「旺角分行」於該廈。本行亦於是年開始使用電腦處理存款業務，初期主機附搭電腦公司，至[1981年自行設立電腦中心](../Page/1981年.md "wikilink")，陸續將各項業務電腦化。

[1980年年初](../Page/1980年.md "wikilink")，本行股份上市（上市編號096），將25%股份公開發售與社會人士，並在證券交易所掛牌買賣。

[1982年](../Page/1982年.md "wikilink")，聯同另外4間華資銀行組成銀聯通寶有限公司，向客戶提供自動櫃員機服務。

[1984年](../Page/1984年.md "wikilink")，在[美國](../Page/美國.md "wikilink")[羅省開設首家海外分行](../Page/羅省.md "wikilink")。

[1986年](../Page/1986年.md "wikilink")，全資附屬之永隆保險有限公司獲政府授權經營保險業務。

[1987年](../Page/1987年.md "wikilink")，[渣打銀行因改變投資政策](../Page/渣打銀行.md "wikilink")，退出為本行股東，將所持10%股權轉讓與伍氏家族控制之公司。同年成立康令有限公司（其後改名為永隆證券有限公司），向客戶提供證券投資服務。

[1992年](../Page/1992年.md "wikilink")，[新加坡發展銀行從伍氏家族控制之公司承讓百分之十本行股權](../Page/新加坡發展銀行.md "wikilink")，加入為股東。

[1994年](../Page/1994年.md "wikilink")，在[廣州市設立代表處](../Page/廣州市.md "wikilink")，為本行在國內首個業務據點。

[1995年](../Page/1995年.md "wikilink")-開始經營[信用卡](../Page/信用卡.md "wikilink")。

[1996年](../Page/1996年.md "wikilink")-成立服務後勤中心，將各部及各分行多項業務工序集中在該中心處理。同年增設[開曼群島分行](../Page/開曼群島.md "wikilink")，向客戶提供離岸銀行服務。

[1998年](../Page/1998年.md "wikilink")-透過全資附屬之永隆財務有限公司經營租購貸款業務。同年推出「永隆網上銀行」，利用互聯網向客戶提供自動化服務。

[1999年](../Page/1999年.md "wikilink")-在[上海市設立代表處](../Page/上海市.md "wikilink")。同年本行聯同幾家同業組成銀聯信託有限公司，開拓強制性公積金服務。並推出電子證券交易系統「永隆電話買賣證券」及「永隆網上買賣證券」服務。

[2000年](../Page/2000年.md "wikilink")-與同業組成銀和再保險有限公司及香港人壽保險有限公司，進一步拓展保險業務。

[2004年](../Page/2004年.md "wikilink")-本行是內地與香港更緊密經貿關係安排落實後，首家獲中國銀行業監督管理委員會批准在內地設立分行的香港銀行。同年首家內地分行設於[深圳](../Page/深圳.md "wikilink")，向客戶提供外匯存款、放款、匯款及押匯服務，開辦人民幣業務。主要股東伍絜宜有限公司購回星展銀行所持10%本行股權。

2008年，大股東[伍宜孫家族向](../Page/伍宜孫家族.md "wikilink")[招商銀行以](../Page/招商銀行.md "wikilink")193億港元出售所持有的53.12%永隆銀行權益。\[2\]伍氏家族[伍步高](../Page/伍步高.md "wikilink")、伍步剛及伍步謙分別辭任公司[董事長](../Page/董事長.md "wikilink")、副董事長及[行政總裁職務](../Page/行政總裁.md "wikilink")，但續留任為[非執行董事](../Page/非執行董事.md "wikilink")。\[3\]永隆委任朱琦、[馬蔚華及張光華為新行政總裁](../Page/馬蔚華.md "wikilink")、董事長及副董事長。

2009年，招商銀行完成強制性收購永隆銀行餘下股份後，永隆銀行撤銷上市地位。

2019年10月1日，永隆銀行正式更新名字為招商永隆銀行。

## 營業點

招商永隆銀行現於香港、[中國內地](../Page/中國.md "wikilink")、[澳門及海外共有機構網點](../Page/澳門.md "wikilink")45家，員工逾1,900人。

## 財務狀況

截至2016年6月30日，綜合資產總額為港幣2,379億元。永隆銀行於2008年成為[招商銀行集團成員](../Page/招商銀行.md "wikilink")，並由民營上市公司轉為[國有企業](../Page/國有企業.md "wikilink")。按資產總額計，招商銀行為中國第六大商業銀行，現已躋身全球百大銀行之列。

## 香港人壽

2017年3月20日，[香港人壽原本股東](../Page/香港人壽.md "wikilink")[亞洲保險有限公司](../Page/亞洲保險有限公司.md "wikilink")、[創興銀行有限公司](../Page/創興銀行有限公司.md "wikilink")、[華僑永亨銀行有限公司](../Page/華僑永亨銀行有限公司.md "wikilink")、[上海商業銀行有限公司和永隆銀行有限公司以](../Page/上海商業銀行有限公司.md "wikilink")71億港元，出售予神祕買家[首元國際有限公司](../Page/首元國際有限公司.md "wikilink")（中國先鋒集團持有），其主席[陳智文](../Page/陳智文.md "wikilink")（[陳有慶兒子](../Page/陳有慶.md "wikilink")，也是[陳智思親屬](../Page/陳智思.md "wikilink")）及總經理張立輝表示，出售後維持正常運作。而[高盛（亞洲）有限責任公司和](../Page/高盛（亞洲）有限責任公司.md "wikilink")[野村國際（香港）有限公司作為上述](../Page/野村國際（香港）有限公司.md "wikilink")5名股東聯席財務顧問\[4\]\[5\]\[6\]\[7\]\[8\]。

## 参考文献

## 外部链接

  - [招商永隆銀行官網](http://www.cmbwinglungbank.com/)

{{-}}

[category:招商局](../Page/category:招商局.md "wikilink")

[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:1933年成立的公司](../Category/1933年成立的公司.md "wikilink")
[Category:華資銀行](../Category/華資銀行.md "wikilink")

1.  [公司網站 2018-10-02](https://www.cmbwinglungbank.com/wlb_corporate/hk/about-us/corporate-information/company-history.html)
2.  [招行193億購永隆53%](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20080531&sec_id=15307&subsec_id=15320&art_id=11173608)
3.  [1](http://hk.news.yahoo.com/article/080930/9/8hff.html)
4.  [亞洲保險有限公司、創興銀行有限公司、華僑永亨銀行有限公司、上海商業銀行有限公司和永隆銀行有限公司聯合宣佈就出售香港人壽保險有限公司並建立新的銀保合作關係達成協議](http://www.hklife.com.hk/hklifeweb/web/about/about_script_detail.jsp?lang=chi&id=166)2017年3月20日，香港人壽保險有限公司
5.  [亞洲金融：料出售香港人壽最快明年完成](http://hk.apple.nextmedia.com/realtime/finance/20170323/56469764)2017年3月23日，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")
6.  [香港人壽買家為神秘富豪](http://hk.apple.nextmedia.com/financeestate/art/20170322/19966185)2017年3月22日，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")
7.  [香港人壽賣盤予首元國際
    作價達71億元](http://hk.apple.nextmedia.com/realtime/finance/20170320/56456504)2017年3月20日，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")
8.  [亞洲金融指出售香港人壽股權仍待保監會審批](http://www.metroradio.com.hk/news/default.aspx?NewsID=20170323180710)[新城廣播有限公司](../Page/新城廣播有限公司.md "wikilink")，2017年3月23日