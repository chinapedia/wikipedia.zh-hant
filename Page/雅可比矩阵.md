在[向量分析中](../Page/向量分析.md "wikilink")，**雅可比矩阵**是函數的一阶[偏导数以一定方式排列成的矩阵](../Page/偏导数.md "wikilink")，其[行列式称为](../Page/行列式.md "wikilink")**雅可比行列式**。

在[代数几何中](../Page/代数几何.md "wikilink")，[代数曲线的](../Page/代数曲线.md "wikilink")**雅可比行列式**表示[雅可比簇](../Page/雅可比簇.md "wikilink")：伴随该曲线的一个[代數群](../Page/代數群.md "wikilink")，曲线可以嵌入其中。

它们全部都以[普魯士](../Page/普魯士.md "wikilink")[数学家](../Page/数学家.md "wikilink")[卡爾·雅可比命名](../Page/卡爾·雅可比.md "wikilink")。

## 雅可比矩阵

假設某函數從 \(\mathbb{R}^n\) 映到 \(\mathbb{R}^m\)， 其雅可比矩阵是從 \(\mathbb{R}^n\) 到
\(\mathbb{R}^m\) 的線性映射，其重要意義在于它表現了一个多變數向量函數的最佳线性逼近。因此，雅可比矩阵类似于單變數函数的导数。
假设\(F: \mathbb{R}_n\rightarrow \mathbb{R}_m\) 是一个从 \(n\) 维欧氏空间映射到到
\(m\) 维欧氏空间的函数。这个函数由 \(m\) 个实函数组成:
\(y_1(x_1, \cdots, x_n), \cdots, y_m(x_1, \cdots,x_n)\)。这些函数的偏导数(如果存在)可以组成一个
\(m\)行 \(n\)列的矩阵，這個矩陣就是所谓的雅可比矩阵：

\[\begin{bmatrix} \frac{\partial y_1}{\partial x_1} & \cdots & \frac{\partial y_1}{\partial x_n} \\ \vdots & \ddots & \vdots \\ \frac{\partial y_m}{\partial x_1} & \cdots & \frac{\partial y_m}{\partial x_n}  \end{bmatrix}.\]

此矩阵用符號表示为：

\[J_F(x_1,\ldots,x_n)\]
，或者\(\frac{\partial(y_1,\ldots,y_m)}{\partial(x_1,\ldots,x_n)}.\)

这个矩阵的第 \(i\)行是由梯度函数的[转置](../Page/转置.md "wikilink")
\(y_i (i=1,\cdots,m)\)表示的

如果 \(p\)是\(\mathbb{R}^n\) 中的一点，\(F\)在
\(p\)点可微分，根據[數學分析](../Page/數學分析.md "wikilink")，
\(J_F(p)\)是在这点的[导数](../Page/导数.md "wikilink")。在此情况下，\(J_F(p)\)這個线性映射即
\(F\) 在点 \(p\)附近的最优线性逼近，也就是說當 \(x\)足夠靠近點 \(p\)時，我們有

\[F(x) \approx F(p) + J_F(p)\cdot (x-p)\]

### 例子

由[球坐标系到直角坐标系的转化由](../Page/球坐标系.md "wikilink")
\(F\)函数给出︰\(\mathbb{R}\times [0,\pi]\times [0,2\pi]\rightarrow\mathbb{R}^3\)

\[x_1 = r \sin\theta \cos\phi \,\]

\[x_2 = r \sin\theta \sin\phi \,\]

\[x_3 = r \cos\theta \,\]

此坐标变换的雅可比矩阵是

\[J_F(r,\theta,\phi) =\begin{bmatrix}
\frac{\partial x_1}{\partial r} & \frac{\partial x_1}{\partial \theta} & \frac{\partial x_1}{\partial \phi} \\[3pt]
\frac{\partial x_2}{\partial r} & \frac{\partial x_2}{\partial \theta} & \frac{\partial x_2}{\partial \phi} \\[3pt]
\frac{\partial x_3}{\partial r} & \frac{\partial x_3}{\partial \theta} & \frac{\partial x_3}{\partial \phi} \\
\end{bmatrix}=\begin{bmatrix}
    \sin\theta \cos\phi &  r \cos\theta \cos\phi &  -r \sin\theta \sin\phi \\
    \sin\theta \sin\phi &  r \cos\theta \sin\phi &  r \sin\theta \cos\phi \\
    \cos\theta          &  -r \sin\theta         &  0
\end{bmatrix}.\]

\(\mathbb{R}^4\)的 \(F\)函数:

\[y_1 = x_1 \,\]

\[y_2 = 5x_3 \,\]

\[y_3 = 4x_2^2 - 2x_3 \,\]

\[y_4 = x_3 \sin x_1 \,\]

其雅可比矩阵为:

\[J_F(x_1,x_2,x_3) =\begin{bmatrix}
\frac{\partial y_1}{\partial x_1} & \frac{\partial y_1}{\partial x_2} & \frac{\partial y_1}{\partial x_3} \\[3pt]
\frac{\partial y_2}{\partial x_1} & \frac{\partial y_2}{\partial x_2} & \frac{\partial y_2}{\partial x_3} \\[3pt]
\frac{\partial y_3}{\partial x_1} & \frac{\partial y_3}{\partial x_2} & \frac{\partial y_3}{\partial x_3} \\[3pt]
\frac{\partial y_4}{\partial x_1} & \frac{\partial y_4}{\partial x_2} & \frac{\partial y_4}{\partial x_3} \\
\end{bmatrix}=\begin{bmatrix} 1 & 0 & 0 \\ 0 & 0 & 5 \\ 0 & 8x_2 & -2 \\ x_3\cos x_1 & 0 & \sin x_1 \end{bmatrix}.\]
此例子说明雅可比矩阵不一定为方阵。

### 在动力系统中

考虑形为
\(x^\prime=F(x)\)的[动力系统](../Page/动力系统.md "wikilink")，\(F: \mathbb{R}^n\rightarrow\mathbb{R}^n\)。如果
\(F(x_0)=0\)，那么 \(x_0\)是一个驻点（又稱臨界點）。系统接近驻点时的行為跟
\(J_F(x_0)\)的[特征值相關](../Page/特征值.md "wikilink")。

## 雅可比行列式

如果 \(m=n\)，那么 \(F\)是从 \(n\)维空间到
\(n\)维空间的函数，且它的雅可比矩阵是一个[方块矩阵](../Page/方块矩阵.md "wikilink")。于是我们可以取它的行列式，称为**雅可比行列式**。

在某个给定点的雅可比行列式提供了
\(F\)在接近该点时的表现的重要信息。例如，如果[连续可微函数](../Page/连续可微函数.md "wikilink")
\(F\)在
\(p\)点的雅可比行列式不是零，那么它在该点附近具有[反函数](../Page/反函数.md "wikilink")。这称为[反函数定理](../Page/反函数定理.md "wikilink")。更进一步，如果
\(p\)点的雅可比行列式是[正数](../Page/正数.md "wikilink")，则 \(F\)在
\(p\)点的取向不变；如果是负数，则
\(F\)的取向相反。而从雅可比行列式的[绝对值](../Page/绝对值.md "wikilink")，就可以知道函数
\(F\)在 \(p\)点的缩放因子；这就是它出现在[换元积分法中的原因](../Page/换元积分法.md "wikilink")。

### 例子

设有函数\(F: \mathbb{R}^3\rightarrow\mathbb{R}^3\)，其分量为：

\[y_1 = 5x_2 \,\]

\[y_2 = 4x_1^2 - 2 \sin (x_2x_3) \,\]

\[y_3 = x_2 x_3 \,\]

则它的雅可比行列式为：

\[\begin{vmatrix} 0 & 5 & 0 \\ 8x_1 & -2x_3\cos(x_2 x_3) & -2x_2\cos(x_2 x_3) \\ 0 & x_3 & x_2 \end{vmatrix}=-8x_1\cdot\begin{vmatrix} 5 & 0\\ x_3&x_2\end{vmatrix}=-40x_1 x_2.\]

从中我们可以看到，当\(x_1\)和\(x_2\)同号时，\(F\)的取向相反；该函数处处具有反函数，除了在 \(x_1=0\)和
\(x_2=0\)时以外。

## 逆矩陣

根據[反函數定理](../Page/反函數定理.md "wikilink")，一個*可逆函數*（存在[反函數的函數](../Page/反函數.md "wikilink")）的[雅可比矩陣的](../Page/雅可比矩陣.md "wikilink")[逆矩陣即為該函數的](../Page/逆矩陣.md "wikilink")*[反函數](../Page/反函數.md "wikilink")*的[雅可比矩陣](../Page/雅可比矩陣.md "wikilink")。即，若函數
\(F: \mathbb{R}^n\rightarrow\mathbb{R}^n\)在點
\(p\in\mathbb{R}^n\)的雅可比矩陣是連續且可逆的，則 \(F\)在點
\(p\)的某一[鄰域內也是可逆的](../Page/鄰域.md "wikilink")，且有

\[J_{F^{-1}}\circ f=J^{-1}_{F}\]
成立。相反，倘若雅可比行列式在某一個點**不為**零，那麽該函數在這個點的某一鄰域內*可逆*（存在[反函數](../Page/反函數.md "wikilink")）。

一個[多項式函數的可逆性與非經證明的](../Page/多項式函數.md "wikilink")[雅可比猜想有關](../Page/雅可比猜想.md "wikilink")。其斷言，如果函數的雅可比行列式為一個非零實數（相當於其不存在**[複零點](../Page/零點.md "wikilink")**），則該函數可逆且其反函數也為一個多項式。

## 参看

  - [前推](../Page/前推_\(微分\).md "wikilink")
  - [海森矩阵](../Page/海森矩阵.md "wikilink")

## 外部链接

  - [Ian
    Craw的本科教学网页](https://web.archive.org/web/20060421002832/http://www.maths.abdn.ac.uk/~igc/tch/ma2001/notes/node77.html)
    雅可比行列式的通俗解释
  - [Mathworld](http://mathworld.wolfram.com/Jacobian.html)
    更技术型的雅可比行列式的解释

[Y](../Category/多变量微积分.md "wikilink")
[Category:行列式](../Category/行列式.md "wikilink")
[Category:矩阵](../Category/矩阵.md "wikilink")
[Category:导数的推广](../Category/导数的推广.md "wikilink")