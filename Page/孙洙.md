**孫洙**（），字**臨西**，一字**苓西**，別號**蘅塘退士**，[清朝文人](../Page/清朝.md "wikilink")、政治人物。[江蘇](../Page/江蘇.md "wikilink")[無錫縣](../Page/無錫縣.md "wikilink")（今[無錫市](../Page/無錫市.md "wikilink")）人。[乾隆辛未進士](../Page/乾隆.md "wikilink")，官[直隸](../Page/直隸.md "wikilink")、[山東](../Page/山東.md "wikilink")[知縣](../Page/知縣.md "wikilink")。著名的《[唐詩三百首](../Page/唐詩三百首.md "wikilink")》一書是其選編。

## 生平

家貧，隆冬讀書，手中握有一塊木頭，謂“木生火”可禦寒。乾隆九年（1744年）中舉人，擔任過[上元縣儒學](../Page/上元縣.md "wikilink")[教諭](../Page/教諭.md "wikilink")。乾隆十六年（1751年）辛未科進士，任[順天府](../Page/順天府.md "wikilink")[大城縣知縣](../Page/大城縣.md "wikilink")，歷改[直隸](../Page/直隸.md "wikilink")[盧龍縣](../Page/盧龍縣.md "wikilink")、[山東](../Page/山東.md "wikilink")[鄒平縣知縣](../Page/鄒平縣.md "wikilink")\[1\]。後改[江寧府儒學教授](../Page/江寧府.md "wikilink")，退職歸里。乾隆四十三年（1778年）卒於無錫，葬城南陈湾里。

## 著作

乾隆二十八年（1763年）春天，孫洙不滿於當時流傳的《[千家詩](../Page/千家詩.md "wikilink")》選詩不精，與妻子徐蘭英商議編選[唐詩](../Page/唐詩.md "wikilink")。他依據[沈德潛](../Page/沈德潛.md "wikilink")（1673年－1769年）的《[唐詩別裁](../Page/唐詩別裁.md "wikilink")》及[王士禎](../Page/王士禎.md "wikilink")（1634年－1711年）的《[古詩選](../Page/古詩選.md "wikilink")》、《[唐賢三昧集](../Page/唐賢三昧集.md "wikilink")》、《[唐人萬首絕句選](../Page/唐人萬首絕句選.md "wikilink")》為主，雜以其他唐詩選本，“專就唐詩中膾炙人口之作，擇其尤要者”，模仿《[詩經](../Page/詩經.md "wikilink")》的規模，編成《唐詩三百首》，共三百一十首。其題材廣泛，反映[唐代的政治矛盾](../Page/唐代.md "wikilink")、邊塞軍事、宮閨婦怨、酬酢應制、宦海浮沉、隱逸生活等。但《唐詩三百首》也有一些遺珠之憾，如[杜甫](../Page/杜甫.md "wikilink")《[自京赴奉先縣詠懷五百字](../Page/自京赴奉先縣詠懷五百字.md "wikilink")》、《[北征](../Page/北征.md "wikilink")》，[白居易](../Page/白居易.md "wikilink")《[新樂府](../Page/新樂府.md "wikilink")》以及[皮日休等人的作品](../Page/皮日休.md "wikilink")，均未被選入。

孫洙還有《[蘅塘漫稿](../Page/蘅塘漫稿.md "wikilink")》、《[排悶錄](../Page/排悶錄.md "wikilink")》、《[異聞錄](../Page/異聞錄.md "wikilink")》等著作。

## 參考文獻

  - [顧光旭](../Page/顧光旭.md "wikilink")，《梁溪詩鈔·卷四十二》
  - [竇鎮](../Page/竇鎮.md "wikilink")，《名儒言行錄·卷下》

## 外部链接

  - [孙洙](http://www.jyst.cn/ReadNews.asp?NewsID=2055)

[Category:乾隆九年甲子科舉人](../Category/乾隆九年甲子科舉人.md "wikilink")
[Category:清朝縣儒學教諭](../Category/清朝縣儒學教諭.md "wikilink")
[Category:清朝大城縣知縣](../Category/清朝大城縣知縣.md "wikilink")
[Category:清朝盧龍縣知縣](../Category/清朝盧龍縣知縣.md "wikilink")
[Category:清朝鄒平縣知縣](../Category/清朝鄒平縣知縣.md "wikilink")
[Category:清朝府儒學教授](../Category/清朝府儒學教授.md "wikilink")
[Category:無錫人](../Category/無錫人.md "wikilink")
[Z](../Category/孫姓.md "wikilink")

1.  光緒《無錫金匱縣志》