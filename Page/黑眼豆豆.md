**黑眼豆豆**（）是一支来自[美国](../Page/美国.md "wikilink")[洛杉矶的](../Page/洛杉矶.md "wikilink")[嘻哈](../Page/嘻哈.md "wikilink")（Hip-hop）流行音乐团体，在全球都广受欢迎。该团体现在由[will.i.am](../Page/will.i.am.md "wikilink")、[apl.de.ap](../Page/:en:apl.de.ap.md "wikilink")、[Taboo以及](../Page/Taboo_\(歌手\).md "wikilink")[菲姬四位成员组成](../Page/菲姬.md "wikilink")，而該組合於2007年首季時取得[葛萊美獎](../Page/葛萊美獎.md "wikilink")「最佳流行組合大奬」。

## 發展時期

時間回溯到1989年，will.i.am（William Adams）和Apl.de.ap（Allen
Pineda）在[洛杉矶相遇且開始RAP及演出](../Page/洛杉矶.md "wikilink")。在得到[Jerry
Heller](../Page/Jerry_Heller.md "wikilink")（[Eazy-E的老板](../Page/Eazy-E.md "wikilink")）的外甥赏识之后，这对组合与Ruthless唱片签约（该公司由[Eazy-E管理](../Page/Eazy-E.md "wikilink")）。同时签约的还有他们的一个朋友，Dante
Santiago。他们将这三人的组合称作**Atban
Klann**。他们的首张专辑从未出版，因为公司认为该张专辑的正面主题不会受到市场及大众的欢迎。

在[Eazy-E于](../Page/Eazy-E.md "wikilink")1995年因[爱滋病死去之后](../Page/爱滋病.md "wikilink")，Atban
Klann重组成黑眼豆豆。will.i.am曾在专辑《Monkey
Business》的封面上解释取这个名字的原因：“黑眼豆豆是灵魂的食物”。这个组合新加进了成员[Taboo](../Page/Taboo_\(歌手\).md "wikilink")（原名Jaime
Gomez），并邀请[Kim Hill作乐队的固定背景和声](../Page/Kim_Hill.md "wikilink")。

2017年6月，一篇Billboard的文章指Fergie將退出樂團，後來被Will.i.am否認，並指Fergie只是因為要參與Double
Duchess的創作而暫時休息。

## 专辑列表

### 大碟

  - [Behind the Front](../Page/Behind_the_Front.md "wikilink")（1998年）

<!-- end list -->

  -
    美国排行榜第129位。

<!-- end list -->

  - [Bridging the Gap](../Page/Bridging_the_Gap.md "wikilink")（2000年）

<!-- end list -->

  -
    美国排行榜第67位，澳大利亚排行榜第37位。

<!-- end list -->

  - [Elephunk](../Page/Elephunk.md "wikilink")（2003年）

<!-- end list -->

  -
    美国排行榜第14位，英国第3位，澳大利亚第1位。[美国唱片工业协会](../Page/美国唱片工业协会.md "wikilink")（[RIAA](../Page/RIAA.md "wikilink")）：双[白金唱片](../Page/白金唱片.md "wikilink")。[加拿大唱片工业协会](../Page/加拿大唱片工业协会.md "wikilink")（[CRIA](../Page/CRIA.md "wikilink")）：七重[白金唱片](../Page/白金唱片.md "wikilink")。全球销量超过七百万张。

<!-- end list -->

  - [Monkey Business](../Page/Monkey_Business.md "wikilink")（2005年）

<!-- end list -->

  -
    美国排行榜第2位，英国第4位，加拿大第1位，澳大利亚第1位。[美国唱片工业协会](../Page/美国唱片工业协会.md "wikilink")（[RIAA](../Page/RIAA.md "wikilink")）：三重[白金唱片](../Page/白金唱片.md "wikilink")。[加拿大唱片工业协会](../Page/加拿大唱片工业协会.md "wikilink")（[CRIA](../Page/CRIA.md "wikilink")）：六重[白金唱片](../Page/白金唱片.md "wikilink")。全球销量超过八百八十万张。

<!-- end list -->

  - [The E.N.D.](../Page/The_E.N.D..md "wikilink")（2009年）

<!-- end list -->

  -
    美国排行榜第1位，英国第3位，加拿大第1位，澳大利亚第1位。專輯兩張單曲更霸佔告示牌單曲榜冠軍26週，〈Boom Boom
    Pow〉12週冠軍和〈I Gotta Feeling〉14週冠軍。

<!-- end list -->

  - [The Beginning](../Page/The_Beginning.md "wikilink")（2010年）

### EP

  - [Renegotiations: The
    Remixes](../Page/Renegotiations:_The_Remixes.md "wikilink")（2006年）

### 葛萊美獎获奖记录

  - 最佳饶舌双人组或团体演出

### 知名單曲

  - 2003年 Where Is The Love?（no.8）
  - 2005年 Don't Phunk With My Heart（no.3）
  - 2005年 My Humps（no.3）
  - 2006年 Pump It（no.18）
  - 2009年 Boom Boom Pow（12週冠軍）
  - 2009年 I Gotta Feeling（14週冠軍）
  - 2010年 Imma Be（2週冠軍）
  - 2010年 Rock That Body（no.9）
  - 2010年 The Time (Dirty Bit)（no.9）
  - 2011年 Just Can't Get Enough（no.3）

## 全美音樂大獎

  - 最受歡迎流行/搖滾組合
  - 最受歡迎Soul/R\&B組合

## 参考资料

## 外部链接

  - [官方网站](http://www.blackeyedpeas.com/)

  -
[Category:美國演唱團體](../Category/美國演唱團體.md "wikilink")
[Category:美国嘻哈歌手](../Category/美国嘻哈歌手.md "wikilink")
[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")
[H](../Category/美國嘻哈團體.md "wikilink")
[H](../Category/朱諾獎獲得者.md "wikilink")
[H](../Category/1995年成立的音樂團體.md "wikilink")
[Category:Mnet亞洲音樂大獎獲得者](../Category/Mnet亞洲音樂大獎獲得者.md "wikilink")
[Category:1995年加利福尼亞州建立](../Category/1995年加利福尼亞州建立.md "wikilink")