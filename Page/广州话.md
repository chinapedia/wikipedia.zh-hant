**廣州話**，又稱為**廣東話**，四鄉則有**廣府話**、**粵語**、**廣東省城話**、**白話**之謂，相傳起源於[廣信](../Page/廣信_\(漢朝\).md "wikilink")\[1\]（即[梧州及](../Page/梧州.md "wikilink")[封開一隅](../Page/封開.md "wikilink")），而今多奉作廣府話之圭臬。學界所指廣州話，乃現時[香港及](../Page/香港.md "wikilink")[广州所講之粵語](../Page/广州.md "wikilink")，主要通行於[珠江三角洲地区](../Page/珠江三角洲.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、粵東、粵西（局部地方現今闢為廣西之屬）、[南洋與及外洋](../Page/東南亞.md "wikilink")[唐人街](../Page/華人社區.md "wikilink")。

## 名稱

[廣州大觀第十編_廣州話指南.pdf](https://zh.wikipedia.org/wiki/File:廣州大觀第十編_廣州話指南.pdf "fig:廣州大觀第十編_廣州話指南.pdf")支持編撰的《廣州大觀》中的廣州話指南部分，該書在1948年10月出版。\]\]

「[廣州](../Page/古代廣州.md "wikilink")」一名於[三國時期已經存在](../Page/三國時期.md "wikilink")，范围比现在的廣州大得多，覆盖大部分岭南地区。「廣州」區域至[唐朝才有所縮減](../Page/唐朝.md "wikilink")，但是仍然覆蓋整个廣東珠三角地區。清代廣東學者[陳澧認為廣州音密合於隋唐](../Page/陳澧.md "wikilink")[韻書切語](../Page/韻書.md "wikilink")，為他方所不及：「廣州人聲音之所以善者，蓋千餘年來中原之人徙居廣中，今之廣音實隋唐時中原之音。」

## 使用範圍

[Canton_Maps.djvu](https://zh.wikipedia.org/wiki/File:Canton_Maps.djvu "fig:Canton_Maps.djvu")），說廣州話的市民集中居住在圖中的區域。中共1949年建政後實行封閉政策及大搞政治運動，導致廣州城市基建停滯不前，市民的活動範圍在1980年代仍然與圖中相差不大。\]\]
[廣州市馬路圖.jpg](https://zh.wikipedia.org/wiki/File:廣州市馬路圖.jpg "fig:廣州市馬路圖.jpg")
通常稱自己的口音為廣州話及以之為[母語的人](../Page/母語.md "wikilink")，基本上都是廣州舊城區（2005年行政區劃調整前的：[東山區](../Page/東山區_\(廣州市\).md "wikilink")、[越秀區](../Page/越秀區.md "wikilink")、[荔灣區](../Page/荔灣區.md "wikilink")、[海珠區舊河南區域](../Page/海珠區.md "wikilink")）的長期居民。除此之外，廣州市其餘各區亦有部分人能說一口地道廣州話。不過近年來，由於舊城拆遷、[城市規劃以及城市](../Page/城市規劃.md "wikilink")[公共交通發展等問題](../Page/公共交通.md "wikilink")，已經有大量廣州市區居民從舊城區遷出至新城區（主要是白雲區、芳村區、天河區），但基於社會福利（包括就業、學位、養老等）的考慮，有相當一部分廣州人的戶口實際仍處於舊四區當中，並未進行遷移。

近年來，廣州當局喜歡興建公租房、廉租房、限價房（即香港的[公屋](../Page/公屋.md "wikilink")、[居屋](../Page/居屋.md "wikilink")）在城鄉結合部，為了賣地賺錢而幾乎不會選擇市區地段，其目的是「抽疏老城區」，例如白雲區的[金沙洲](../Page/金沙洲.md "wikilink")[新社區和](../Page/金沙洲新社區.md "wikilink")[同德圍](../Page/同德圍.md "wikilink")、芳村地區的芳和花園、海珠區的聚德花園（非舊城所在的河南區域）、天河區廣氮花園等地，使廣州市民被迫搬離舊區。而在[西關](../Page/西關.md "wikilink")[恩寧路的](../Page/恩寧路.md "wikilink")「危房改造工程」中，當局只為有產權的居民提供回遷方案，而且是回遷到距離這地塊約一公里外的待興建小區，其餘公私房（樓）租客部分要遷到靠近[佛山市](../Page/佛山市.md "wikilink")[南海區](../Page/南海區.md "wikilink")[大瀝鎮黃岐的金沙洲新社區](../Page/大瀝鎮.md "wikilink")，部份遷到天河區的[珠江新城](../Page/珠江新城.md "wikilink")，部分自謀出路。而持普通話、各地語言或「唔鹹唔淡」粵語的外地租客開始湧入，他們大部分在西關附近的批發市場打工，例如黃沙水產批發市場、十三行附近的服裝批發市場，因這些人中有部分受教育水平較低，不良的生活習慣使舊區環境更加惡劣。隨著舊城變得空心，傳統民俗文化也加速消亡。有恩寧路舊街坊認為，舊城改造不僅是興建新樓宇，還需要讓土生土長的西關人安住於西關，「如果西關街坊都搬到很遠的地方，誰來講述西關歷史？重塑西關歷史文化又從何而談？讓本土居住的人留在本地才是最重要的，根的傳承還得依靠原住民」。
\[2\]

此外，廣府地區（粵語廣府片）、[韶關](../Page/韶關.md "wikilink")、[清遠](../Page/清遠.md "wikilink")、[粵西等部分年輕人除了當地方言外](../Page/粵西.md "wikilink")，一般也能說廣州話。[廣東電視台自啟播起](../Page/廣東電視台.md "wikilink")，就使用廣州話作為廣播語言，對廣東省各地居民的語言影響深遠。而在廣州的學校讀書的其他粵語方言學生及非粵語的外地學生，因社交需要，普遍能使用部分廣州話作交流。而[香港粵語基本是從廣州話分化而來](../Page/香港粵語.md "wikilink")，因此與廣州音非常接近，區別在於香港粵語在詞彙方面有更多外來語，如來自[英語的外來語](../Page/英語.md "wikilink")。

## 聲韻調

以下列出的發音以廣州老派城裏話為準。

### 聲母

廣州話有15个[聲母](../Page/聲母.md "wikilink")（不包括[零聲母](../Page/零聲母.md "wikilink")）\[3\]
：

  -
    {| class="wikitable" style="text-align:center; width:45%"

\!colspan=3|
\!\![双唇音](../Page/双唇音.md "wikilink")\!\![唇齒音](../Page/唇齒音.md "wikilink")\!\![齿齦音](../Page/齿齦音.md "wikilink")\!\![软腭音](../Page/软腭音.md "wikilink")\!\![聲門音](../Page/聲門音.md "wikilink")
|- \!colspan=3|[鼻音](../Page/鼻音_\(辅音\).md "wikilink") ||||||||| |-
\!rowspan=2|[塞音](../Page/塞音.md "wikilink")
\!rowspan=2|[清音](../Page/清音.md "wikilink")
\!<small>[送氣](../Page/送氣.md "wikilink")</small> ||||||| || |-
\!<small>[不送氣](../Page/不送氣.md "wikilink")</small> ||||||||| |-
\!rowspan=2|[塞擦音](../Page/塞擦音.md "wikilink")
\!rowspan=2|[清音](../Page/清音.md "wikilink")
\!<small>[送氣](../Page/送氣.md "wikilink")</small> |||||||| |rowspan=2|
|- \!<small>[不送氣](../Page/不送氣.md "wikilink")</small> |||||||| |-
\!rowspan=1|[擦音](../Page/擦音.md "wikilink")
\!colspan=2|[清音](../Page/清音.md "wikilink") |||||||||| |-
\!colspan=3|[边音](../Page/边音.md "wikilink") |||||||||| |}

  - 註1：有些學者認爲在、或元音前方有[聲門塞音](../Page/聲門塞音.md "wikilink")，但由於它不構成對立，一般都視之爲「[零聲母](../Page/零聲母.md "wikilink")」。
  - 註2：有些學者視下列韻母裏的介音爲聲母，視零聲母的介音爲聲母，視聲母與介音爲聲母，視聲母與介音爲聲母。若如此做，聲母則有19個。

### 韻母

廣州話有94個[韻母](../Page/韻母.md "wikilink")（包括自成音節的\[\]和\[\]）\[4\]：

  -
    {| class="wikitable" style="width:80%; text-align:center"

\!||colspan=5|開尾韻||colspan=10|[元音](../Page/元音.md "wikilink")[尾韻](../Page/韻尾.md "wikilink")
|- \![開口呼](../Page/開口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![齊齒呼](../Page/齊齒呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![合口呼](../Page/合口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![撮口呼](../Page/撮口呼.md "wikilink") ||||||||||||||||||||||||||||| |}

  -
    {| class="wikitable" style="width:80%; text-align:center"

\!||colspan=15|[鼻](../Page/鼻音_\(辅音\).md "wikilink")[尾韻](../Page/韻尾.md "wikilink")
|- \![開口呼](../Page/開口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![齊齒呼](../Page/齊齒呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![合口呼](../Page/合口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![撮口呼](../Page/撮口呼.md "wikilink") ||||||||||||||||||||||||||||| |}

  -
    {| class="wikitable" style="width:80%; text-align:center"

\!||colspan=15|[塞音](../Page/塞音.md "wikilink")[尾韻](../Page/韻尾.md "wikilink")
|- \![開口呼](../Page/開口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![齊齒呼](../Page/齊齒呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![合口呼](../Page/合口呼.md "wikilink") ||||||||||||||||||||||||||||| |-
\![撮口呼](../Page/撮口呼.md "wikilink") ||||||||||||||||||||||||||||| |}

  -
    {| class="wikitable" style="width:15%; text-align:center"

\!其他 ||| |}

  - 由於介音開首的韻只跟零聲母相拼，介音開首的韻只跟、、零聲母相拼，有些學者會簡化韻母系統，去除介音，視韻母裏的介音爲聲母，視零聲母的介音爲聲母，視聲母與介音爲聲母，視聲母與介音爲聲母。若如此做，韻母則有53個，比上表的計算方法減少了41個。

### 聲調

廣州話有9個聲調（不包括變音）\[5\]：

  -
    {| class="wikitable" style="width:20%; text-align:center"

|- \!調類\!\!聲調記號\!\![調值](../Page/調值.md "wikilink") |- \!陰平 |˥˧、˥˥ |53、55
|- \!陽平 |˨˩、˩˩ |21、11 |- \!陰上 |˧˥ |35 |- \!陽上 |˨˧ |23 |- \!陰去 |˧˧ |33 |-
\!陽去 |˨˨ |22 |- \!陰入 |˥˥ |55 |- \!中入 |˧˧ |33 |- \!陽入 |˨˨ |22 |}

## 詞彙

廣州話中除了粵語中所含的詞語之外，還吸收不少[潮州話及](../Page/潮州話.md "wikilink")[客家話的詞語](../Page/客家話.md "wikilink")，例如「口渴」讀成「口涸」、「喉乾」或「口乾」。另外廣州也是外國人的集中地，有不少外來語，如[士多](../Page/士多.md "wikilink")、-{[士多啤梨](../Page/士多啤梨.md "wikilink")}-、摩登等等，與香港相同。廣州本地也有一些原創的詞語，如黃犬（[蚯蚓](../Page/蚯蚓.md "wikilink")）、塘尾（[蜻蜓](../Page/蜻蜓.md "wikilink")）。廣州話經常在形容某事情誇張時，喜好用「好」字，如：「好大」、「好開心」、「好鍾意」等。

### 古漢語詞

粵語使用了不少現代[標準官話不再常用的單字或者](../Page/標準官話.md "wikilink")[詞彙](../Page/詞彙.md "wikilink")，例如：

  - 渠：他之意，今作「佢」，「渠所謂小令，蓋市井所唱小曲也。」[明](../Page/明.md "wikilink")[王驥德](../Page/王驥德.md "wikilink")《[曲律](../Page/曲律.md "wikilink")》
  - 尋日：昨日之意，「尋日尋花花不語。」（[程垓](../Page/程垓.md "wikilink")，宋詞）

<!-- end list -->

  - 擸雜：可以隨手取得的物品，被隨處放置的物品等，例句「收埋你啲擸雜嘢。」

粵語保留了不少可能與[古漢語相關的](../Page/古漢語.md "wikilink")[詞彙](../Page/詞彙.md "wikilink")，例如：

  - 崖：危險之意，原為「懸崖上的小屋」，引申為很危險之意，今常用近音字「牙煙」表示。\[6\]

### 英語等外來詞彙

由於[廣州歷來是外貿港口](../Page/廣州.md "wikilink")，很多詞彙受到英語影響，如稱：

| 廣州話                                                                             | 普通話                                | 英文         |
| ------------------------------------------------------------------------------- | ---------------------------------- | ---------- |
| 波                                                                               | 球                                  | Ball       |
| 士多                                                                              | 小商店                                | Store      |
| 的士                                                                              | [出租車](../Page/出租車.md "wikilink")   | Taxi       |
| 巴士                                                                              | [公共汽車](../Page/公共汽車.md "wikilink") | Bus        |
| 迷你                                                                              | 小                                  | Mini       |
| 摩登                                                                              | 現代                                 | Modern     |
| \-{zh-hans:士多啤梨; zh-hant:士多啤梨; zh-cn:士多啤梨; zh-tw:士多啤梨; zh-hk:士多啤梨; zh-sg:士多啤梨}- | [草莓](../Page/草莓.md "wikilink")     | Strawberry |
| 啤梨                                                                              | [梨](../Page/梨.md "wikilink")       | Pear       |
| 肥佬                                                                              | 不合格                                | Fail       |

但這些詞語在中國內陸卻很少見到，故它們成為珠三角地區特有的詞彙，但「的士」一词，已几乎蔓延全大陆，由过去说「截出租车」到现在流行说「打的」。

## 標準口音

[Canton1890.jpg](https://zh.wikipedia.org/wiki/File:Canton1890.jpg "fig:Canton1890.jpg")包圍的為廣州城內，城牆以西為[西關一帶](../Page/西關.md "wikilink")，即地圖的左邊區域，北至長庚里（後成為長庚路，現人民北路與東風西路交界）、南至太平路（打銅街）珠江海皮。\]\]

### 廣州正音

粵語學者以廣州城內音作為正宗廣州音，但廣州話其實包含城內音、西關音、番禺音，[廣州音字典也有少量收錄](../Page/廣州音字典.md "wikilink")[佛山](../Page/佛山.md "wikilink")、[中山口音](../Page/中山市.md "wikilink")，視為「俗音」。粵語代表音的制定都取自粵語廣州音，粵語及粵音字典多從廣州音，廣州音被認定為粵語的代表口音。

#### 廣州子音

廣州除西關與東山音外，尚有多個子口音，為舊時仍是[農村郊區的語音](../Page/農村.md "wikilink")，現已大部分失落。深圳本地的白話發音比較短。

### 書面語

廣州話相當的詞語來自古漢語，因此習慣用詞精簡，例如「房子」稱為「屋」，亦有些老式詞語「過往」、「舊時」，現在叫「以前」。

隨著在兩廣（廣東、廣西）大力[推廣普通話](../Page/推廣普通話.md "wikilink")，[普通話成為強制性教學語言及生活語言](../Page/普通話.md "wikilink")，學生很難在學校裡學習到傳統的粵語發音、詞彙，而大量改用普通話詞彙。例如「淘貨」（掃貨）、「老廣州」（街坊、街-{里}-）、「倒是」（反而係）、「[老鄉](../Page/老鄉.md "wikilink")」（鄉里）等，這類明顯的普通話詞彙時常混入廣州話中，個別已經成為廣州話詞語，如「落班」（放工,收工）、落課（落堂）、單位（廣州指任職機關,香港指度量衡單位或樓宇住宅）、機關（廣州指組織機構,香港指一種機械裝置）、戶口（廣州指家庭狀況/家境,香港多指銀行賬戶）。

### 香港粵語

[香港粵語是香港地區使用的廣州話](../Page/香港粵語.md "wikilink")。長久以來，香港對當地的廣州話口語並沒有正式的名稱，民眾最常稱之廣東話，而香港官方則稱之為粵語。1970年代之前，香港地區的廣州話混合了多種地區的粵語口音，如[媽姐按順德音叫成馬姐](../Page/媽姐.md "wikilink")，但傾向以廣州話為準。1980年代開始，香港廣州話口語的標準稱謂，廣東話比廣州話及白話等名稱較普及。以前習慣稱廣州話做廣東話的人主要是外省移民（包括臺灣），他們稱粵人的廣州話為廣東話\[7\]。

香港粵語口音標準主要源自廣州話，加入了[香港本地文化](../Page/香港文化.md "wikilink")，慢慢地形成為具有香港特色、以口語為主的語言。由於香港為英國統治150多年，英國將大量英語詞彙帶入香港，香港人亦早已習慣中英混合使用。粵語是香港的法定語言之一，政府內部溝通以及發佈消息也以廣州話與[英語進行](../Page/英語.md "wikilink")，廣播媒體大部份設有廣州話頻道。雖然港式廣州話使用者眾多及覆蓋面很廣，不過港式廣州話只當作一種廣州話口語變體，即使香港有所謂[粵語正音運動](../Page/粵語正音運動.md "wikilink")，但仍未作為口音標準。另外，由[何文匯博士所發起的](../Page/何文匯.md "wikilink")[粵語正音運動](../Page/粵語正音運動.md "wikilink")，提倡根據宋朝《[廣韻](../Page/廣韻.md "wikilink")》一書中的切韻方法，恢復當時的讀音，由於該運動得到各大電子傳播媒介及香港政府部門的支持，致使香港社會普遍對某些詞彙的發音與廣泛的珠江三角洲粵語系地區產生脫節。

### 四邑土白話

四邑白話（不是指[四邑方言](../Page/四邑方言.md "wikilink")）有一些不同於廣州音及香港音的特點，如落雨讀作落水，好大雨讀作好大水，並且沒有雨這一概念。開**窗**讀作後元音開**倉**。不如廣州話香港話般有豐富的口語變調，如口語條**件**、事**件**、部**隊**、排**隊**、開**會**皆讀作第6調陽去，而不是像廣州音及香港音變調為第2調陰上，釣**魚**讀作第4調陽平而非第2調陰上。作業**簿**讀作捕部而非寶保（並母，可參考**薄**）。**掉垃圾**讀作**調立濕**。**腸**粉、大**腸**讀作**牆詳**而非**搶**

### 馬來西亞粵語

[怡保市和](../Page/怡保市.md "wikilink")[吉隆坡內念英文學校的](../Page/吉隆坡.md "wikilink")[華人雖然會說](../Page/華人.md "wikilink")[华语](../Page/华语.md "wikilink")（[標準官話](../Page/標準官話.md "wikilink")），但是依然能說廣東話。部分念華語學校的華人說的華語也帶有廣東話音。由於部分當地華人長期受[香港電影及](../Page/香港電影.md "wikilink")[香港電視的影響](../Page/香港電視.md "wikilink")，因此怡保廣東話與[港式廣州話十分接近](../Page/港式廣州話.md "wikilink")。

在馬來西亞，[馬來語是必修的語言](../Page/馬來語.md "wikilink")，當地人的廣州話也受到[馬來文的影響](../Page/馬來文.md "wikilink")，如[咖啡](../Page/咖啡.md "wikilink")，當地人會唸成「過B」（「KOPI」是馬來文）；也会受到福建移民使用的闽南语影响，如[奶茶會唸成](../Page/奶茶.md "wikilink")「茶」（「TEH」是闽南話的发音），[鴛鴦會唸成](../Page/鴛鴦.md "wikilink")「摻」（「CAM」是闽南話的发音）。不過現在的家長們都重視普通話多於廣州話，在马来西亚华人圈通用的[馬來西亞華語亦以北京話为基准](../Page/馬來西亞華語.md "wikilink")。

## 廣州話與港式粵語

兩地口音的區別最主要在於對部分特定事物的稱謂，例如「摩托車」與「-{zh-hans:电单车;
zh-hant:電單車;}-」、「冰箱」與「雪櫃」、「膠擦」 與
「擦膠」、「麵包車」與「VAN仔」等等。在1990年代之前，有些事物在廣州的慣稱與今日香港一樣，如「雪櫃」和「冷氣」等（參見[Wikipedia:唔同粵語地區用字習慣](../Page/:zh-yue:Wikipedia:唔同粵語地區用字習慣.md "wikilink")），這些變化與當局的政策及外省廠商有直接關係。

儘管香港教育界向來承認香港使用的是廣州語音，但近年香港推行的「粵語正音運動」卻使得兩地某些字音有分裂的趨勢，但這基本不會影響雙方交流。另外，廣州人說話平穩，近年口語常受到普通話發音及詞語影響；香港人說話急促，容易造成[懶音](../Page/懶音.md "wikilink")，常直接混用英語單詞。廣州電視節目上常出現普通話與廣州話直接對話，在現實中不太真實，廣州大多外地人根本聽不懂廣州話\[8\]。

隨著兩地人員交流越趨頻密，以及廣播傳媒的影響下，除「廣州話」與「廣東話」的爭議外，上述兩地差異已逐漸縮小。在外界看來，這種大同小異的差別顯得微不足道，因此一般不會為了口音或常用詞而作出正式的分辨。

## 影響力

廣東官方一向以廣州話作為省內的[官方語言](../Page/官方語言.md "wikilink")，因此廣東民間習慣以廣州話在省內溝通，大部分人以廣州話為母語。直至北京政府在廣東[推普後](../Page/推普.md "wikilink")，廣東省各級政府改用[北京話作為官方語言](../Page/北京話.md "wikilink")，廣州話在廣東省內的影響力迅速萎縮。在廣西也遭受同樣的困境，公共媒體大幅減少廣州話節目，相反地大量增加普通話節目，使廣州話在廣西的影響力也正逐漸減弱。

香港、澳門官方、媒體以廣州話為通用語言，部分書面媒體也常用廣州話白話文，廣州話成為香港和澳門民眾主要的溝通語言，當中以香港影響力最大。因為香港的廣州話流行曲在東亞及東南亞地區受歡迎，因此廣州話在這些地區也較有流行文化和影響力。因廣東籍和香港的海外移民數量龐大，使廣州話成為大多數海外廣東籍移民的通用語言。

有些廣州話詞彙被[北京話吸收](../Page/北京話.md "wikilink")，可是大多數的[外省人](../Page/外省人.md "wikilink")，因為不甚明白原本的出處，本來的語意，說話時的環境（[語境](../Page/語境.md "wikilink")），令到後來使用上語義和用字很多都有改變。

例子：

  - 買單：源自「埋單」，來自「埋尾結單」（結帳），還有去到最後的意思。[官話用諧音](../Page/官話.md "wikilink")「買單」，只取結帳之意。
  - 打的：源自「搭的士」，即「坐出租車」的意思，现谓扬手示意的士停驻，道地说法应是“截的士”。
  - 沒卵用：源自「無撚用」，即沒用，在中間加入粗口字撚（本字為𡳞），以增強語氣。

## 參見

  - [粵語](../Page/粵語.md "wikilink")
      - [粵語方言](../Page/粵語方言.md "wikilink")
      - [香港粵語](../Page/香港粵語.md "wikilink")
  - [西關口音](../Page/西關口音.md "wikilink")
  - [母語教學](../Page/母語教學.md "wikilink")
  - [慣用語](../Page/慣用語.md "wikilink")
  - [2010年廣州撐粵語行動](../Page/2010年廣州撐粵語行動.md "wikilink")
  - [速打粵語拼音輸入法](../Page/速打粵語拼音輸入法.md "wikilink")
  - [廣東話](../Page/廣東話.md "wikilink")

## 參考文獻

## 外部連結

  - [粵語審音配詞字庫](http://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/)
  - [香港貿易發展局
    文字發聲工具](https://web.archive.org/web/20131230233221/http://tdc.putonghuaonline.com/tools1.html)
    － 可即時轉化為普通話或廣州話發音
  - [Google粵語輸入法](https://play.google.com/store/apps/details?id=com.google.android.apps.inputmethod.cantonese)
  - [廣州話語音翻译朗讀](https://web.archive.org/web/20111017185104/http://l2china.com/yueyu/)

[Category:粵語方言](../Category/粵語方言.md "wikilink")
[Category:粵語](../Category/粵語.md "wikilink")
[Category:广州文化](../Category/广州文化.md "wikilink")

1.  羅康寧, 葉國泉:《粵語源流考》

2.

3.

4.
5.
6.  出自[唐朝诗人](../Page/唐朝.md "wikilink")[韩愈的](../Page/韩愈.md "wikilink")《陪杜侍御游湘西两寺献杨常侍》：「剖竹走泉源，开廊架崖」。

7.  香港人對粵語、廣東話、廣州話的概念混淆不清，所以前兩者一般特指廣州話

8.  [廣州招考公務員要求會講廣州話引發激烈爭論](http://news.xinhuanet.com/newscenter/2002-10/23/content_605012.htm)