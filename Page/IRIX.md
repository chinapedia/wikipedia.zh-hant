**IRIX**是由[硅谷图形公司](../Page/硅谷图形公司.md "wikilink")（[Silicon Graphics
Inc.](../Page/硅谷图形公司.md "wikilink")，一般用簡稱：SGI）以[System
V與](../Page/UNIX_System_V.md "wikilink")[BSD延伸程式為基礎所發展成的](../Page/Berkeley_Software_Distribution.md "wikilink")[UNIX](../Page/UNIX.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")，IRIX可以在SGI公司的[RISC型電腦上執行](../Page/RISC.md "wikilink")，即是採行32位元、64位元[MIPS架構的SGI工作站](../Page/MIPS架構.md "wikilink")、伺服器。

2006年9月6日，SGI宣佈於同年12月起終止MIPS IRIX作業系統產品線。之後SGI電腦的作業系統改為[Red Hat
Enterprise Linux或](../Page/Red_Hat_Enterprise_Linux.md "wikilink")[SuSE
Linux Enterprise
Server](../Page/SuSE.md "wikilink")。IRIX的支援服務持續到2013年12月為止。

## 相關參見

  - [IRIX software](../Category/IRIX_software.md "wikilink")

## 外部連結

  - [Overview of information related to the IRIX operating
    system](http://www.sgi.com/products/software/irix/)
  - [Technical Publications
    Library](https://web.archive.org/web/20040910055918/http://techpubs.sgi.com/library/tpl/cgi-bin/init.cgi)
  - [IRIX
    Datasheet](http://www.sgi.com/products/software/irix/datasheet.pdf)
  - [IRIX Administration
    Guide](http://software.majix.org/irix/index.shtml)
  - [Nekochan
    Net](https://web.archive.org/web/20070707031832/http://www.nekochan.net/)
  - [Silicon Bunny - IRIX software and
    information](http://www.siliconbunny.com/)
  - [IRIX Versions and
    History](https://web.archive.org/web/20070516002703/http://www.tliquest.net/ryan/sgi/irix_versions.html)
  - [<cite>IRIX® Admin: Backup, Security, and
    Accounting</cite>](https://web.archive.org/web/20060818192713/http://www.cepba.upc.es/docs/sgi_doc/SGI_Admin/books/IA_BakSecAcc/sgi_html/index.html)
    Document Number: 007-2862-004 February 1999
  - [What could have been
    IEEE 1003.1e/2c](https://web.archive.org/web/20060705205324/http://wt.xpilot.org/publications/posix.1e/download.html)

[Category:SGI](../Category/SGI.md "wikilink") [Category:System
V](../Category/System_V.md "wikilink")