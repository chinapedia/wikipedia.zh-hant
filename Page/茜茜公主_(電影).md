{{ Otheruses|subject=奥地利电影《茜茜公主》|other=电影主角的原型巴伐利亚公主伊丽莎白（茜茜公主）|伊丽莎白皇后}}
[Romy_Schneider_1955.jpg](https://zh.wikipedia.org/wiki/File:Romy_Schneider_1955.jpg "fig:Romy_Schneider_1955.jpg")
《**茜茜公主**》（**）是[奥地利拍摄的](../Page/奥地利.md "wikilink")[历史题材的](../Page/历史.md "wikilink")[德语](../Page/德语.md "wikilink")[三部曲](../Page/三部曲.md "wikilink")[电影](../Page/电影.md "wikilink")。第一部公映于1955年，电影讲述了奥地利皇后[伊丽莎白](../Page/伊莉莎白_\(巴伐利亞公主\).md "wikilink")（1837年—1898年）的早年生活，故事改编自1952年出版的玛丽·布兰克-艾斯曼（）的同名[小说](../Page/小说.md "wikilink")。由于第一部《茜茜公主》在[票房收入上获得了巨大成功](../Page/票房.md "wikilink")，1956年和1957年又相继拍摄并公映了续集《**茜茜公主2：年轻的皇后**》（**）和《**茜茜公主3：皇后的命运**》（**）。

《茜茜公主》三部曲是历史上最成功的德语电影之一，也是在[德国获得最高票房收入的电影之一](../Page/德国.md "wikilink")，第一部观众约6,538,000人，第二部观众约6,385,000人\[1\]。

这部电影的[编剧和](../Page/编剧.md "wikilink")[导演是奥地利](../Page/导演.md "wikilink")[剧作家恩斯特](../Page/剧作家.md "wikilink")·马利施卡（，1893年1月2日—1963年5月12日），饰演[奥匈帝国](../Page/奥匈帝国.md "wikilink")[皇后伊丽莎白](../Page/皇后.md "wikilink")（茜茜）的是德国籍和[法国籍](../Page/法国.md "wikilink")[维也纳女演员](../Page/维也纳.md "wikilink")[罗密·施奈德](../Page/罗密·施奈德.md "wikilink")，饰演奥匈帝国[皇帝](../Page/皇帝.md "wikilink")[弗朗茨·约瑟夫一世的是奥地利演员](../Page/弗朗茨·约瑟夫一世.md "wikilink")[卡尔海因茨·伯姆](../Page/卡尔海因茨·伯姆.md "wikilink")。

## 第一部

伊丽莎白（昵称“茜茜”，罗密·施奈德饰）是[巴伐利亚王国](../Page/巴伐利亚王国.md "wikilink")[马克思公爵和](../Page/马科西米利安·约瑟夫.md "wikilink")[卢多维卡公主的次女](../Page/卢多维卡·威廉明娜.md "wikilink")，生性活泼，热爱自然。而父亲和母亲对子女有着截然相反的教育观念。故事开始于她同她的母亲卢多维卡和姐姐海伦（昵称“内那”）在从[巴伐利亚去奥地利以舍尔的旅行途中](../Page/巴伐利亚.md "wikilink")。她们此行的目的是为了让内那与卢多维卡的[外甥](../Page/外甥.md "wikilink")——23岁的年轻奥地利[皇帝](../Page/皇帝.md "wikilink")[弗朗茨·约瑟夫](../Page/弗朗茨·约瑟夫一世.md "wikilink")（[卡尔海因茨·伯姆饰](../Page/卡尔海因茨·伯姆.md "wikilink")）[订婚](../Page/订婚.md "wikilink")。这份婚事是由卢多维卡的姐姐、弗朗茨母亲——专制的[索菲钦定的](../Page/苏菲公主_\(巴伐利亚\).md "wikilink")。为了向马克思公爵，隐瞒此次行程的目地，母女两人决定以“旅行”的名义带上茜茜，故茜茜对这份[婚约一无所知](../Page/婚约.md "wikilink")。到达以舍尔后，她在独自钓鱼的途中偶遇出行的弗朗茨，两人[一见钟情](../Page/一见钟情.md "wikilink")。

弗朗茨向茜茜表白时，茜茜才得知弗朗茨内定的订婚对象是姐姐内那。茜茜未能告之弗朗茨自己的真实身份，匆忙与弗朗茨分别。当晚将为弗朗茨举行生日晚宴。晚宴后的[舞会上](../Page/舞会.md "wikilink")，弗朗茨和内那订婚的消息即将公开。茜茜未能拒绝参加晚宴。晚宴上，再次见到茜茜的弗朗茨，终于得知了她真实的身份。无可救药地爱上了茜茜的弗朗茨，违背母亲索菲的旨意，宣布和茜茜订婚，茜茜将要成为[奥地利的皇后](../Page/奥地利皇后.md "wikilink")。

此后，姨母索菲对这位来自巴伐利亚带着豪放狂野作风的外甥女茜茜十分不满，但却又无能为力，只能努力地教她宫廷礼仪。茜茜乘船凯旋般地回到[维也纳](../Page/维也纳.md "wikilink")，同弗朗茨在施特凡大教堂举行了隆重的[皇家婚礼](../Page/皇家婚礼.md "wikilink")，成为了奥地利的皇后。

## 第二部

[Wappen_Kaiserin_Elisabeth.png](https://zh.wikipedia.org/wiki/File:Wappen_Kaiserin_Elisabeth.png "fig:Wappen_Kaiserin_Elisabeth.png")
茜茜开始了她皇后的生活，但倔强的茜茜和专制的婆婆索菲之间的矛盾却始终不断，在茜茜的女儿出生后，索菲不愿意让来自巴伐利亚的狂野的茜茜抚养小公主，而是要亲自抚养并把小公主从茜茜身边夺走。弗朗茨也顺从了母亲的旨意，茜茜一气之下逃回了巴伐利亚，弗朗茨亲自带着小公主才把她又接回了维也纳，他们和好如初。

此时的奥地利同[匈牙利之间发生摩擦](../Page/匈牙利.md "wikilink")，将要发生[战争](../Page/战争.md "wikilink")，尤其是野心勃勃的革命党领袖、匈牙利伯爵[安多西不服奥地利对匈牙利的统治](../Page/久洛·安德拉什.md "wikilink")，鼓动匈牙利不甘于俯首称臣的贵族们起来反抗。茜茜以她的个人魅力征服了安多西伯爵，同时也征服了匈牙利的人民，最终，茜茜和弗朗茨加冕成为[奥匈帝国的皇后和皇帝](../Page/奥匈帝国.md "wikilink")。

## 第三部

[Franz_joseph1.jpg](https://zh.wikipedia.org/wiki/File:Franz_joseph1.jpg "fig:Franz_joseph1.jpg")

奥匈帝国成立后，茜茜居住在匈牙利，她同弗朗茨在一起的时间越来越少，[安多西伯爵也爱上了茜茜并向她示爱](../Page/久洛·安德拉什.md "wikilink")，遭到了茜茜的婉言拒绝。婆婆索菲却在背后散布茜茜与伯爵关系不寻常的谣言。

与此同时，茜茜染上了严重的[肺结核](../Page/肺结核.md "wikilink")，只得去[大西洋上的](../Page/大西洋.md "wikilink")[马德拉岛修养](../Page/马德拉岛.md "wikilink")，茜茜和弗朗茨再次分离。这个消息令十分爱戴茜茜的奥匈人民非常不安，所幸茜茜在母亲卢多维卡的悉心照料下最终痊愈。

病愈后，茜茜与弗朗茨一同访问奥地利统治下的[意大利](../Page/意大利.md "wikilink")，却遭到了意大利人民的冷遇，在[威尼斯](../Page/威尼斯.md "wikilink")，意大利人紧闭窗门，用沉默来表达自己对外族统治的憎恶。此时出现了感人的一幕，等候在[圣马可广场上小公主跑向她的妈妈](../Page/圣马可广场.md "wikilink")，茜茜紧紧地抱住女儿，这一举动感动了意大利人民，他们高呼“Viva
la
Mama”（意大利语的“母亲万岁”），茜茜为奥匈帝国赢得了意大利人民的心，电影背景响起了奥匈帝国的[国歌](../Page/国歌.md "wikilink")。

## 背景资料

### 电影

  - 小说和电影《茜茜公主》中“Sissi”的名字其实是个误写，伊丽莎白皇后在家中的昵称其实是“Sisi”，她自己在签名时有时候也使用“Lisi”。
  - 电影《茜茜公主》的编剧和导演恩斯特·马利施卡曾计划拍摄第四部，并为第四部定下茜茜同[法国皇后](../Page/法国皇后.md "wikilink")[欧根妮](../Page/欧根妮.md "wikilink")（Eugènie）之间争风吃醋的故事。但是由于在前三部饰演茜茜的罗密·施奈德拒绝再出演茜茜而作罢\[2\]。
  - 电影《茜茜公主》在讲述这个闻名遐迩的爱情故事的同时，也向人们展现了奥地利的迷人风光，至今仍对奥地利的[旅游业起着积极作用](../Page/旅游业.md "wikilink")。
  - 半个世纪过去了，人们对电影《茜茜公主》的热情不减，它依旧是爱情故事作品的模范。

### 演员

  - 由于在电影《茜茜公主》中塑造人物形象的成功，反而使得主演罗密·施奈德和卡尔海因茨·伯姆都无法走出茜茜和弗朗茨的影子，他们都表示不愿意一生的演艺被框在这两个形象中，也因此拒绝再次出演《茜茜公主》\[3\]。
  - 扮演茜茜的德籍和法籍维也纳女演员罗密·施奈德当年年仅16岁，《茜茜公主》使她一下子出了名。她那迷人的微笑驱散了[二战后奥地利人民心中的阴霾](../Page/二战.md "wikilink")，给人们带来了重建家园的信心。她也迅速征服了欧洲人的心，赢得了“战后欧洲第一美人”的赞誉\[4\]。但无论是现实中的茜茜还是饰演茜茜的罗密·施奈德，最后都十分不幸地结束了自己的一生。历史上的茜茜公主，即奥匈帝国的皇后于1898年在[日内瓦养病时被一名意大利](../Page/日内瓦.md "wikilink")[无政府主义者谋杀](../Page/无政府主义.md "wikilink")。而在电影中饰演茜茜的罗密·施奈德在1982年遭遇了独生子不幸夭折的打击，44岁的她被发现死于[巴黎的家中](../Page/巴黎.md "wikilink")，[媒体认为她是](../Page/媒体.md "wikilink")[自杀](../Page/自杀.md "wikilink")，但验尸报告表明她死于心力衰竭，属于正常死亡\[5\]。她留下一个女儿莎拉·比阿西尼也是演员，同样拒绝扮演茜茜公主\[6\]。
  - 扮演弗朗茨的奥地利演员卡尔海因茨·伯姆于2014年5月29日逝世，从事为[埃塞俄比亚募捐的事业](../Page/埃塞俄比亚.md "wikilink")，2005年他出席在德国[斯图加特举行的纪念](../Page/斯图加特.md "wikilink")《茜茜公主》上演五十周年、名为“你好，茜茜！”的活动。参加活动的还有弗朗茨与茜茜的重孙——[哈布斯堡王族的大公爵马尔库斯](../Page/哈布斯堡王朝.md "wikilink")·萨尔瓦托\[7\]。卡尔海因茨·伯姆给他1955年出生的女儿也取名为“Sissi”\[8\]。

### 真实历史

[Kaiser_Franz_Joseph_tomb_-_Vienna.jpg](https://zh.wikipedia.org/wiki/File:Kaiser_Franz_Joseph_tomb_-_Vienna.jpg "fig:Kaiser_Franz_Joseph_tomb_-_Vienna.jpg")在维也纳的棺墓\]\]
历史上真实的茜茜公主并非完全像电影《茜茜公主》中那样。

  - 茜茜与弗朗茨的爱情，完全没有电影中描述的那么完美。弗朗茨受过严格的宫廷教育，而茜茜是在[巴伐利亚的湖光山色中自由自在地成长的](../Page/巴伐利亚.md "wikilink")。两种不同的气质最初相互吸引，逐渐地却显得格格不入。成为了伊丽莎白皇后的茜茜虽然享尽荣耀富贵，却总是郁郁寡欢。
  - 茜茜从内心里一直拒绝扮演传统的妻子、母亲、皇后乃至一个大帝国形象代表的角色。晚年的茜茜心灰意冷，周游欧亚非列国。1898年，她在日内瓦被一名无政府主义者杀害。尽管如此，这位皇后仍以美貌、魅力和浪漫的忧郁气质而受到臣民的爱戴\[9\]。

## 相关条目

  - [茜茜公主](../Page/茜茜公主.md "wikilink")，伊莉莎白 (巴伐利亞公主)

## 参考文献

<div class="references-small">

  - Marie Blank-Eismann: *Sissi. Roman*. Deutscher Literatur-Verlag
    Melchert, Hamburg 1991, 318 S., ISBN 3-87152-275-9
  - Ernst Marischka: *Sissi. Ein Roman nach den Filmen Sissi; Sissi, die
    junge Kaiserin und Schicksalsjahre einer Kaiserin*. Blüchert,
    Hamburg 1960, 271 S.
  - Karin Petra Rudolph: *Sissi. Das Leben einer Kaiserin. Der Bildband
    zu den Originalfilmen*. Burgschmiet-Verlag, Nürnberg 1998, 95 S.,
    ISBN 3-932234-26-X
  - Renate Seydel: *Ich Romy, mein Leben.* (Autobiographie), Piper 2005
  - Michael Jürgs: "Der Fall Romy Schneider" (Biographie nach
    persönlichen Aufzeichnungen und Erfahrungen)
  - Alice Schwarzer: "Romy Schneider. Mythos und Leben" (Portrait)

</div>

## 外部链接

  - [电影《茜茜公主》的网站](http://www.sissi.de/)
  - [电影《茜茜公主》](http://www.imdb.com/title/tt0048624/)
    在[互联网电影数据库](../Page/互联网电影数据库.md "wikilink")
  - [电影《茜茜公主2：年轻的皇后》](http://www.imdb.com/title/tt0049762/) 在互联网电影数据库
  - [电影《茜茜公主3：皇后的命运》](http://www.imdb.com/title/tt0050974/) 在互联网电影数据库

## 资料来源

<div class="references-small">

<references />

</div>

[Category:1955年电影](../Category/1955年电影.md "wikilink")
[Category:1956年电影](../Category/1956年电影.md "wikilink")
[Category:1957年电影](../Category/1957年电影.md "wikilink")
[Category:德语电影](../Category/德语电影.md "wikilink")
[Category:1950年代劇情片](../Category/1950年代劇情片.md "wikilink")
[Category:奧地利劇情片](../Category/奧地利劇情片.md "wikilink")
[Category:19世紀背景電影](../Category/19世紀背景電影.md "wikilink")
[Category:公主主角題材電影](../Category/公主主角題材電影.md "wikilink")
[Category:电影三部曲](../Category/电影三部曲.md "wikilink")

1.

2.

3.
4.  <http://www.people.com.cn/GB/paper68/6243/617365.html>

5.

6.  <http://www.dw-world.de/dw/article/0,2144,1449646,00.html>

7.  <http://www.dw-world.de/dw/article/0,2144,1830587,00.html>

8.

9.