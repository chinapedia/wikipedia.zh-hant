**瓦尔特GSP手枪**
是[卡尔·瓦尔特运动枪有限公司一款為射击比賽而生產的手枪](../Page/卡尔·瓦尔特运动枪有限公司.md "wikilink")，可發射[.22
LR或](../Page/.22_LR.md "wikilink")[.32 S\&W
Long子弹](../Page/.32_S&W_Long.md "wikilink")，主要用於[射击运动标准手枪比賽项目中](../Page/射击.md "wikilink")。瓦尔特
GSP Expert是更高精度的衍生版本。
[2006_0915Waffe10001.JPG](https://zh.wikipedia.org/wiki/File:2006_0915Waffe10001.JPG "fig:2006_0915Waffe10001.JPG")口徑的瓦尔特
GSP\]\]

## 外部链接

  - [Carl Walther GSP
    官方頁面](https://web.archive.org/web/20070927202752/http://www.carl-walther.de/dev2/index.php?company=walther&lang=DE&content=products&hid=1&uid=3&product=163)

[Category:半自動手槍](../Category/半自動手槍.md "wikilink")
[Category:德國半自動手槍](../Category/德國半自動手槍.md "wikilink")
[Category:瓦尔特半自动手枪](../Category/瓦尔特半自动手枪.md "wikilink") [Category:.22
LR口徑槍械](../Category/.22_LR口徑槍械.md "wikilink")