**AC尼尔森公司**（****），又名：AGB尼爾森，是一家总部位于[美国](../Page/美国.md "wikilink")[纽约市国际](../Page/纽约市.md "wikilink")[市場調查研究公司](../Page/市場調查研究.md "wikilink")，[北美地区总部坐落于](../Page/北美.md "wikilink")[伊利诺斯州](../Page/伊利诺斯州.md "wikilink")[绍姆堡](../Page/绍姆堡_\(伊利诺伊州\).md "wikilink")。主要研究包括[消费品市场的情况和动态](../Page/消费品.md "wikilink")、解决市场和销售问题，以及确定市场发展机会。在该行业是全球最大和最有名气的公司。\[1\]

各地名稱：
韓國：K.AGB 美國：AC 台灣：Taiwan AGB
中國 / 蒙古 - 福建省：妙爾森 英國：AGB 澳大利亞 / 紐西蘭 / 日本：AC·韋迪 智利：韋迪思

## 歷史

1923年[亚瑟·尼尔森在伊利诺斯州](../Page/亚瑟·尼尔森.md "wikilink")[埃文斯顿创办了AC尼尔森公司](../Page/埃文斯顿_\(伊利诺斯州\).md "wikilink")，公司的业务是为顾客提供可靠及客观的[广告效应的情报以及提供销售顾问](../Page/广告.md "wikilink")。1939年AC尼尔森开始国际性发展，今天它的业务遍及上百個國家。

AC尼尔森最著名的一个产品是调查电视、广播和报纸在[媒体市场上的顾客数目的](../Page/媒体市场.md "wikilink")[尼尔森收视率](../Page/尼尔森收视率.md "wikilink")。1952年AC尼尔森开始在美国约1200台消费者电视机上装相应的纪录仪器来做统计。这些仪器使用照片和邮盒来记录观众观看的频道来确定每个频道的观众数目。后来AC尼尔森又研制了使用电子技术收集和传递数据的方法。1996年AC尼尔森将这个部门划分为一个名为[尼尔森媒体研究公司的独立公司](../Page/尼尔森媒体研究公司.md "wikilink")。1999年尼尔森媒体研究公司被[荷兰的](../Page/荷兰.md "wikilink")[VNU集团收购](../Page/VNU集团.md "wikilink")，2001年AC尼尔森也被VNU收购，成为VNU市场信息集团的一部分。尼尔森媒体研究公司的总部在[纽约](../Page/纽约.md "wikilink")，AC尼尔森的总部依然在绍姆堡。

调查互联网和电子媒体市场的[尼尔森互联网市场研究机构和调查消费者产生的媒体的](../Page/尼尔森互联网市场研究机构.md "wikilink")公司也是AC尼尔森的姐妹公司。

2016年12月21日AC尼尔森将以5.6亿美元现金收购媒体元数据提供商Gracenote，

## 参考文献

## 外部链接

  - AC尼尔森官方网站
      - [全球](http://www.nielsen.com)
      - [中国](http://cn.nielsen.com/)
      - [台灣](http://tw.cn.nielsen.com/site/index.shtml)
      - [香港](http://hk.nielsen.com)

[Category:市场营销](../Category/市场营销.md "wikilink")
[Category:美国公司](../Category/美国公司.md "wikilink")
[Category:1923年成立的公司](../Category/1923年成立的公司.md "wikilink")
[Category:纽约市公司](../Category/纽约市公司.md "wikilink")

1.  [Nielsen - Contact](http://www2.acnielsen.com/site/contact.php)