**學院貴公子**\[1\]（**abingdon boys
school**）是[日本四人男子](../Page/日本.md "wikilink")[搖滾](../Page/搖滾.md "wikilink")[-{zh-hans:乐団;zh-hk:樂隊;zh-tw:樂團;}-](../Page/樂隊.md "wikilink")，于2005年組成，簡稱a.b.s.。所屬事務所是Parfit
Production，唱片公司是[日本Epic
Records](../Page/日本Epic_Records.md "wikilink")。2005年凭借《Stay
Away》一曲成功出道。2006年12月發行首张单曲《INNOCENT
SORROW》。2007年10月发行首张专辑《Abingdon boys School》。

## 成員

  - [西川貴教](../Page/西川貴教.md "wikilink")（主唱，灵魂人物）、[T.M.Revolution主唱](../Page/T.M.Revolution.md "wikilink")。
  - [柴崎浩](../Page/柴崎浩.md "wikilink")（吉他）、[WANDS及](../Page/WANDS.md "wikilink")[al.ni.co吉他手](../Page/al.ni.co.md "wikilink")。
  - [SUNAO](../Page/SUNAO.md "wikilink")（吉他）
  - [岸利至](../Page/岸利至.md "wikilink")（合成器、程序）

### 支援成員

  - [Ikuo](../Page/Ikuo.md "wikilink")（貝斯）、[Cube-ray成員](../Page/Cube-ray.md "wikilink")
  - [長谷川浩二](../Page/長谷川浩二.md "wikilink")（鼓手）、Cube-ray成員。

## 組成

樂隊的名是來自英國樂隊[電台司令成員在](../Page/電台司令.md "wikilink")1980年代就讀[Abingdon
School影響而命名](../Page/Abingdon_School.md "wikilink")，以西川貴教為活跃中心（但是隊長是岸利至）。2005年3月15日正式出道，為漫畫[NANA所發行專輯](../Page/NANA.md "wikilink")《LOVE
for NANA 〜Only 1 Tribute〜》收錄了歌曲《stay away》。2006年12月6日，發行第一張單曲[INNOCENT
SORROW](../Page/INNOCENT_SORROW.md "wikilink")，為動畫版[驅魔少年的主題曲](../Page/驅魔少年.md "wikilink")。2007年5月16日發行第二張單曲[HOWLING](../Page/HOWLING.md "wikilink")，为动画[黑之契约者的主题曲](../Page/黑之契约者.md "wikilink")。

2007年10月17日發行首張專輯「abingdon boys
school」，在Oricon專輯榜最高排行第二\[2\]。12月5日，發行第四張單曲[BLADE
CHORD](../Page/BLADE_CHORD.md "wikilink")，是[戰國BASARA](../Page/戰國BASARA.md "wikilink")2
英雄外傳主題曲。

2009年2月25日，abingdon boys school的单曲《STRENGTH.》开始发售。此单曲为动画版[SOUL
EATER的片尾曲](../Page/SOUL_EATER.md "wikilink")。

2009年5月20日，abingdon boys
school的單曲《JAP》發行，此單曲為動畫[戰國BASARA的主題曲](../Page/戰國BASARA.md "wikilink")。

## 作品

### 單曲

1.  [INNOCENT
    SORROW](../Page/INNOCENT_SORROW.md "wikilink")（2006年12月6日）動畫[驅魔少年片頭曲](../Page/驅魔少年.md "wikilink")。
2.  [HOWLING](../Page/HOWLING.md "wikilink")（2007年5月16日）動畫[黑之契約者片頭曲](../Page/黑之契約者.md "wikilink")
3.  [Nephilim](../Page/Nephilim.md "wikilink")（2007年7月4日）遊戲[Folklore
    -異魂傳承-主題曲](../Page/Folklore_-異魂傳承-.md "wikilink")。
4.  [BLADE
    CHORD](../Page/BLADE_CHORD.md "wikilink")（2007年12月5日）遊戲[戰國BASARA](../Page/戰國BASARA.md "wikilink")2
    英雄外傳主題曲。
5.  [STRENGTH.](../Page/STRENGTH..md "wikilink")(2009年2月25日)動畫[SOUL
    EATER片尾曲](../Page/SOUL_EATER.md "wikilink")
6.  [JAP](../Page/JAP_\(音樂\).md "wikilink")(2009年5月20日)TV動畫[戰國BASARA主题曲](../Page/戰國BASARA.md "wikilink")/遊戲[戰國BASARA
    BATTLE HEROES主題曲](../Page/戰國BASARA_BATTLE_HEROES.md "wikilink")
7.  [你的歌聲](../Page/你的歌聲.md "wikilink")(2009年8月26日)動畫[東京震級8.0主题曲](../Page/東京震級8.0.md "wikilink")
8.  [From Dusk Till
    Dawn](../Page/From_Dusk_Till_Dawn.md "wikilink")(2009年12月16日)動畫[黑之契約者
    -流星の双子-片尾曲](../Page/黑之契約者_-流星の双子-.md "wikilink")
9.  [WE
    aRE](../Page/WE_aRE.md "wikilink")(2012年9月5日)遊戲[戰國BASARA](../Page/戰國BASARA.md "wikilink")
    HD Collection合輯主題曲

### 專輯

1.  [學院貴公子](../Page/學院貴公子_\(專輯\).md "wikilink")（2007年10月17日）
2.  [ABINGDON
    ROAD](../Page/ABINGDON_ROAD_\(專輯\).md "wikilink")（2010年1月27日）

### 參加專輯

  - LOVE for NANA ～Only 1 Tribute～

<!-- end list -->

  -
    M-5 Stay Away

<!-- end list -->

  - PARADE〜RESPECTIVE TRACKS OF BUCK-TICK〜

<!-- end list -->

  -
    M-13「ドレス」

<!-- end list -->

  - The songs for DEATH NOTE the movie 〜the Last name TRIBUTE〜

<!-- end list -->

  -
    M-5「Fre@K $HoW」

<!-- end list -->

  - D.Gray-man Original Soundtrack 1

<!-- end list -->

  -
    M-1「INNOCENT SORROW (TV size)」

<!-- end list -->

  - DARKER THAN BLACK －黑之契約者－ 劇伴

<!-- end list -->

  -
    M-2「HOWLING -TV size ver.-」

<!-- end list -->

  - LUNA SEA MEMORIAL COVER ALBUM -Re:birth-

<!-- end list -->

  -
    M-2「Sweetest Coma Again」

### 藍光

  - Abingdon Boys School-Japan Tour

### DVD

  - ABINGDON ROAD MOVIES

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [官方網站](http://www.aabbss.com/)

[Category:日本另类摇滚乐团](../Category/日本另类摇滚乐团.md "wikilink")
[Category:日本索尼音樂娛樂旗下藝人](../Category/日本索尼音樂娛樂旗下藝人.md "wikilink")

1.  [學院貴公子專輯介紹](http://www.sonybmg.com.tw/album/album_info.php?al_prodno=88697171142)

2.  [傑尼斯獨霸公信榜
    西川貴教率「學院貴公子」唱出頭](http://tw.news.yahoo.com/article/url/d/a/071025/35/mzui.html)