| [國際單位制](../Page/國際單位制.md "wikilink")[电学單位](../Page/電.md "wikilink") |
| ------------------------------------------------------------------- |
| 基本單位                                                                |
| 單位                                                                  |
| [安培](../Page/安培.md "wikilink")                                      |
| 導出單位                                                                |
| 單位                                                                  |
| [伏特](../Page/伏特.md "wikilink")                                      |
| [歐姆](../Page/歐姆.md "wikilink")                                      |
| [法拉](../Page/法拉.md "wikilink")                                      |
| [亨利](../Page/亨利_\(单位\).md "wikilink")                               |
| [西門子](../Page/西门子_\(单位\).md "wikilink")                             |
| [庫侖](../Page/库仑.md "wikilink")                                      |
| 歐姆·米                                                                |
| 西門子/每米                                                              |
| 法拉/每米                                                               |
| 反法拉                                                                 |
| 伏安                                                                  |
| 無功伏安                                                                |
| [瓦特](../Page/瓦特.md "wikilink")                                      |
| [千瓦·时](../Page/千瓦·时.md "wikilink")                                  |
|                                                                     |

<noinclude> </noinclude>

[Category:物理学模板](../Category/物理学模板.md "wikilink")