**THE CHANGING
SAME**（中譯：平井堅的歲月）是[日本男歌手](../Page/日本.md "wikilink")[平井堅的第三張原創專輯](../Page/平井堅.md "wikilink")，日本地區於2000年6月21日發行。發行首周空降[Oricon日本公信榜冠軍](../Page/Oricon.md "wikilink")，加上再發版（2000年10月1日[Sony
Music](../Page/Sony_Music.md "wikilink") [DefSTAR
Records發行](../Page/DefSTAR_Records.md "wikilink")）登場回數68周，總銷量超過126萬張，獲得[日本唱片協會百萬唱片認證](../Page/日本唱片協會.md "wikilink")。

## 解説

  - 距離上一張專輯『[Stare At](../Page/Stare_At.md "wikilink")』約四年的第三張原創專輯。
  - 収録以R\&B樂風走紅的熱門單曲「[楽園](../Page/樂園_\(平井堅單曲\).md "wikilink")」、「[why](../Page/Why_\(平井堅單曲\).md "wikilink")」，以及兩首c/w曲「affair」、「wonderful
    world」。
  - 1998年単曲「[Love Love
    Love](../Page/Love_Love_Love_\(平井堅單曲\).md "wikilink")」當中含有濃厚對歌唱的信念，而収録於專輯裡。\[1\]
  - 平井堅個人的第一張百萬專輯。

## 單曲銷量

  - [楽園](../Page/樂園_\(平井堅單曲\).md "wikilink")：ORICON最高第7名。銷量約53.8萬張
  - [why](../Page/Why_\(平井堅單曲\).md "wikilink")：ORICON最高第8名。銷量約21.1萬張

## 収録曲

1.  **Introduction -HERE COMES KH-**
2.  **[Love Love Love](../Page/Love_Love_Love_\(平井堅單曲\).md "wikilink")**
      - 作詞・作曲：平井堅／編曲：中西康晴
      - [TBS電視台](../Page/TBS電視台.md "wikilink")「愛のヒナ壇」片頭曲
3.  **[why](../Page/Why_\(平井堅單曲\).md "wikilink")**
      - 作詞：平井堅／作曲：松原憲／編曲：Maestro-T
4.  **[affair](../Page/樂園_\(平井堅單曲\).md "wikilink")**
      - 作詞：松井五郎／作曲: 243／編曲: URU
5.  **the flower is you**
      - 作詞：平井堅／作曲：割田康彥／編曲：松原憲
6.  **Interlude -BRIGHTEN UP-**
      - 作曲・編曲：中野雅仁
7.  **K.O.L**
      - 作詞：平井堅／作曲・編曲：村山晋一郎
8.  **LADYNAPPER**
      - 作詞・作曲：平井堅／編曲：中野雅仁
9.  **Unfit In Love**
      - 作詞：井邊清、平井堅／作曲：平井堅／編曲：[久保田利伸](../Page/久保田利伸.md "wikilink")、柿崎洋一郎
10. **wonderful world**
      - 作詞：藤林聖子／作曲・編曲：中野雅仁
11. **Interlude -TURN OFF THE LIGHTS-**
      - 作曲・編曲：中野雅仁
12. **[楽園](../Page/樂園_\(平井堅單曲\).md "wikilink")**
      - 作詞：阿閉真琴／作曲・編曲：中野雅仁
13. **アオイトリ**
      - 作詞：平井堅／作曲：西賢二／編曲：中野雅仁
14. **The Changing Same -変わりゆく変わらないもの-**
      - 作詞：平井堅／作曲・編曲：中野雅仁

## 参考資料

[Sony Music的作品紹介](../Page/索尼音樂娛樂.md "wikilink")

  - [通常盤](http://www.sonymusic.co.jp/Music/Arch/DF/KenHirai/DFCZ-1020/index.html)

[Category:平井堅音樂專輯](../Category/平井堅音樂專輯.md "wikilink")
[Category:2000年音樂專輯](../Category/2000年音樂專輯.md "wikilink")
[Category:RIAJ百萬認證專輯](../Category/RIAJ百萬認證專輯.md "wikilink")
[Category:Oricon百萬銷量達成專輯](../Category/Oricon百萬銷量達成專輯.md "wikilink")
[Category:2000年Oricon專輯週榜冠軍作品](../Category/2000年Oricon專輯週榜冠軍作品.md "wikilink")

1.  日文音樂雜誌『[ROCKIN'ON
    JAPAN](../Page/ROCKIN'ON_JAPAN.md "wikilink")』（ロッキング・オン刊、25
    JUNE 2002 VOL.223）