**朱鳳芝**（），生於台灣桃園縣，籍貫[河南省](../Page/河南省.md "wikilink")[汝南縣](../Page/汝南縣.md "wikilink")，台灣政治人物，丈夫[唐江濤任職於](../Page/唐江濤.md "wikilink")[中科院](../Page/中科院.md "wikilink")，兩人育有1子1女，女兒[唐先慧為](../Page/唐先慧.md "wikilink")[中國國民黨第](../Page/中國國民黨.md "wikilink")18屆[中央委員](../Page/中央委員.md "wikilink")。1989年12月當選第1屆增額立法委員之後，每屆立法委員選舉皆當選。因前任七屆立委選舉皆成功當選，故有人戲稱若第8屆再度連任就成了「朱八屆」，但在立法委員初選中，朱鳳芝民調不及時任平鎮市長[陳萬得](../Page/陳萬得.md "wikilink")，不過陳後因恐嚇罪遭起訴而宣布退選，但朱只願意接受徵召、不再參與初選，因此在後來舉辦的二次初選中並未領表，立委連任紀錄也因此中止。

## 第七屆立法委員任期出席紀錄

2008年2/1\~3/26院會應出席15次，委員會應出席19次，總計34次，缺席20次。
資料來源：[公民監督國會聯盟監督國會周報第五期](../Page/公民監督國會聯盟.md "wikilink")。www.ccw.org.tw

2008年4/1\~4/30院會應出席4次，委員會應出席13次，總計17次，缺席1次。
資料來源：公民監督國會聯盟監督國會週報第十五期。www.ccw.org.tw

[Category:第1屆第6次增額立法委員](../Category/第1屆第6次增額立法委員.md "wikilink")
[Category:第2屆中華民國立法委員](../Category/第2屆中華民國立法委員.md "wikilink")
[Category:第3屆中華民國立法委員](../Category/第3屆中華民國立法委員.md "wikilink")
[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:第7屆中華民國立法委員](../Category/第7屆中華民國立法委員.md "wikilink")
[Category:國立成功大學校友](../Category/國立成功大學校友.md "wikilink")
[Category:淡江大學校友](../Category/淡江大學校友.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:河南裔台灣人](../Category/河南裔台灣人.md "wikilink")
[F鳳](../Category/朱姓.md "wikilink")
[Category:臺灣女性立法委員](../Category/臺灣女性立法委員.md "wikilink")
[Category:桃園市人](../Category/桃園市人.md "wikilink")
[Category:桃園縣議員](../Category/桃園縣議員.md "wikilink")