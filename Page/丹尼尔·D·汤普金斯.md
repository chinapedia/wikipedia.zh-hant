**丹尼尔·D·汤普金斯**（**Daniel D.
Tompkins**，），[美国实业家](../Page/美国.md "wikilink")、记者、政治家，[民主共和党成员](../Page/民主共和党.md "wikilink")，曾任[纽约州州长](../Page/纽约州州长.md "wikilink")（1807年－1817年）和[美国副总统](../Page/美国副总统.md "wikilink")（1817年－1825年）。

## 外部链接

  - [Find-A-Grave profile for Daniel D.
    Tompkins](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=1039)

[T](../Category/哥倫比亞大學校友.md "wikilink")
[Category:美国民主党共和党州长](../Category/美国民主党共和党州长.md "wikilink")