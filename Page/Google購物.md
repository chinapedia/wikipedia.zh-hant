**Google购物**（），前称“Google Product Search”、Google
Products和Froogle，是由[Google公司開辦的](../Page/Google.md "wikilink")[價格比較服務](../Page/價格比較服務.md "wikilink")，其發明者是Craig
Nevill-Manning。其界面提供了一個[HTML](../Page/HTML.md "wikilink")[表單欄位讓用戶可鍵入](../Page/表單.md "wikilink")[產品查詢以獲得一個有販賣該項產品的](../Page/產品.md "wikilink")[銷售商及其價格的列表](../Page/銷售商.md "wikilink")。

## 历史

Google Product
Search原名為*Froogle*（發音*frugal*），是意為*節儉*的*[frugal](../Page/wiktionary:_frugal.md "wikilink")*以及該公司名稱*Google*的[雙關語](../Page/雙關語.md "wikilink")，但在2007年4月18日更名為Google
Product Search。

Google Product
Search與大部份其他的價格比較服務不同，並不收取任何列表費用以及不接受提高產品排名的[報酬](../Page/報酬.md "wikilink")；另外亦不在銷售上收取[佣金](../Page/佣金.md "wikilink")，任何公司均可提交產品資訊（透過“*[數據](../Page/數據.md "wikilink")[饋送](../Page/網路饋送.md "wikilink")*”）以包含到Froogle引擎中。值得注意的是亦有以[AdWords的形式在Product](../Page/AdWords.md "wikilink")
Search顯示的[廣告位置可供購買](../Page/廣告.md "wikilink")，與Google網路其他部份相似，其位置明顯與搜尋結果主體分開。

搜尋結果可按[關聯性](../Page/關聯性.md "wikilink")（“最匹配”）或價錢（[遞增或](../Page/wiktionary:_ascend.md "wikilink")[遞減](../Page/wiktionary:_descend.md "wikilink")）[排序](../Page/排序.md "wikilink")，亦可在指定的線上商店搜尋物品（如他們有提供Product
Search數據饋送）。Product Search現時只有特定國家可以使用。

Google在Froogle以beta運作數年後於2002年12月正式[對外宣佈](../Page/wiktionary:_announce.md "wikilink")。現有提供[WML格式](../Page/WML.md "wikilink")，可經由支援WML的[手提電話或其他無線裝置存取](../Page/手提電話.md "wikilink")。

Google的說明中心表示：「Froogle的搜尋結果將保持獨立自[Google
Base](../Page/Google_Base.md "wikilink")，但Froogle Merchant
Center正逐漸被Google Base控制面板取代，提交到Google
Base的物品如適合Froogle將一併在Froogle與Google
Base搜尋結果中顯示（Froogle's search results will remain
separate from [Google Base](../Page/Google_Base.md "wikilink")。However,
the Froogle Merchant Center is being replaced by the Google Base
dashboard. Items submitted to Google Base which are appropriate for
Froogle will be displayed both in Froogle and in Google Base search
results.）。」

2017年6月，欧盟法院称Google
Shopping滥用搜索引擎，操纵搜索结果，违反了反垄断法，判罚24.2亿[欧元](../Page/欧元.md "wikilink")\[1\]。

## 對手

  - [Kelkoo](../Page/Kelkoo.md "wikilink")
  - [NexTag](../Page/NexTag.md "wikilink")
  - [PriceGrabber.com](../Page/PriceGrabber.com.md "wikilink")
  - [Shopping.com](../Page/Shopping.com.md "wikilink")
  - [ShopWiki](../Page/ShopWiki.md "wikilink")
  - [Shopzilla](../Page/Shopzilla.md "wikilink")
  - [AuctionSHARK](../Page/AuctionSHARK.md "wikilink")
  - [SmartShopper](../Page/SmartShopper.md "wikilink")
  - [Dealio](../Page/Dealio.md "wikilink")
  - [thefind.com](../Page/thefind.com.md "wikilink")

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")

## 外部連結

  - [Google Product Search](http://www.google.com/products)
  - [Hudson Horizons - Automated Product Submission to
    Froogle](https://web.archive.org/web/20070427002231/http://froogle.hudsonhorizons.com/)
    - 自動化提交產品到Froogle

## 参考来源

<references />

[Category:Google搜尋](../Category/Google搜尋.md "wikilink")

1.