**开放声音系统**（Open Sound System，OSS）是一個在 Unix 操作系統上用於發出和取得聲音的介面。它是基於標準的
Unix 設備（即 POSIX 的讀、寫、ioctl 等）。

OSS 是在 1992年由 [Hannu Savolainen](../Page/Hannu_Savolainen.md "wikilink")
創造的，目前可用於11個主流的類Unix操作系統。OSS 可以在4種授權選擇下發布，其中3種是自由軟體授權，因此 OSS 是自由軟體。\[1\]

## API

這個 API 設計成使用傳統 Unix 的 open()、read()、write() 和 ioctl()
架構，存取特定的設備。例如，聲音輸入和輸出的預設設備是
**/dev/dsp**。使用Shell 的例子：

`cat `[`Open_Sound_System//dev/urandom`](../Page/Open_Sound_System/dev/urandom.md "wikilink")` >/dev/dsp # plays `[`white``
 ``noise`](../Page/white_noise.md "wikilink")` through the speaker`
`cat /dev/dsp >a.a # reads data from the microphone and copies it to file a.a`

## 免費，專有，自由

2007年7月，4Front Technologies 發布用於 OpenSolaris 的
[CDDL](../Page/CDDL.md "wikilink") 和用於
[Linux](../Page/Linux.md "wikilink") 的 GPL 授權的 OSS 原始碼。\[2\]
2008年1月4Front Technologies發布基於
[FreeBSD](../Page/FreeBSD.md "wikilink") （和其它
[BSD](../Page/BSD.md "wikilink")
系統）下[BSD許可證的](../Page/BSD許可證.md "wikilink")
OSS。\[3\]

## 参见

  - [ALSA](../Page/ALSA.md "wikilink")

## 參考文獻

## 外部链接

  - [Open Sound System - 首頁](http://www.opensound.com/)
  - [Building the Open Sound System From
    Source](http://www.4front-tech.com/wiki/index.php/Building_OSSv4_from_source)
  - [Sorry State of Sound in
    Linux](http://insanecoding.blogspot.com/2007/05/sorry-state-of-sound-in-linux.html)
  - [State of sound in Linux not so sorry after
    all](http://insanecoding.blogspot.com/2009/06/state-of-sound-in-linux-not-so-sorry.html)

[Category:Linux](../Category/Linux.md "wikilink")
[Category:Unix](../Category/Unix.md "wikilink")
[Category:自由音訊軟體](../Category/自由音訊軟體.md "wikilink")
[Category:應用程序介面](../Category/應用程序介面.md "wikilink")
[Category:Linux内核功能](../Category/Linux内核功能.md "wikilink")

1.

2.
3.