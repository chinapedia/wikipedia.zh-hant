是一款基於著名[第一人稱射擊遊戲](../Page/第一人稱射擊遊戲.md "wikilink")《[反恐精英](../Page/絕對武力.md "wikilink")》的改編作，以[反恐精英：零點行動的](../Page/絕對武力：一觸即發.md "wikilink")[GoldSrc引擎為基礎製作而成的FPS類](../Page/GoldSrc引擎.md "wikilink")[網絡遊戲](../Page/網路遊戲.md "wikilink")，主要目的是為了進一步拓展亞洲的[FPS市場](../Page/第一人稱射擊遊戲.md "wikilink")，由原反恐精英研發商[Valve與](../Page/維爾福.md "wikilink")[韓國的](../Page/大韓民國.md "wikilink")[Nexon公司合作](../Page/Nexon.md "wikilink")，在2007年12月於[韓國首度發行](../Page/大韓民國.md "wikilink")。

2012年4月5日NEXON宣布將和Valve再次合作，採用新版本的Source引擎開發續作《[反恐精英Online
2](../Page/絕對武力_Online_2.md "wikilink")》\[1\]。

## 伺服器訊息(依照代理時間)

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>国家／地区</p></th>
<th style="text-align: left;"><p>官方译名</p></th>
<th style="text-align: left;"><p>代理商</p></th>
<th style="text-align: left;"><p>时间</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td style="text-align: left;"><p>營運日期：2007年12月20日~</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/大陆.md" title="wikilink">大陆</a></p></td>
<td style="text-align: left;"><p>-{反恐精英Online}-</p></td>
<td style="text-align: left;"><p><a href="../Page/世纪天成.md" title="wikilink">世纪天成</a></p></td>
<td style="text-align: left;"><p>營運日期：2008年11月18日~</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>／</p></td>
<td style="text-align: left;"><p>-{絕對武力Online}-</p></td>
<td style="text-align: left;"><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td style="text-align: left;"><p>營運日期：2008年7月24日~</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td style="text-align: left;"><p>營運日期：2009年8月12日<br />
結束營運：2019年3月6日</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>／</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>首次代理：IAHGames<br />
二次代理：Megaxus</p></td>
<td style="text-align: left;"><p>營運日期：2010年5月<br />
結束營運：2015年6月30日<br />
二次營運：2018年8月23日~</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>Megaxus</p></td>
<td style="text-align: left;"><p>營運日期：2011年7月22日~</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>PlayFPS</p></td>
<td style="text-align: left;"><p>營運日期：2012年8月<br />
結束營運：2013年12月19日</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/Nexon.md" title="wikilink">Nexon Europe</a></p></td>
<td style="text-align: left;"><p>營運日期：2013年5月<br />
結束營運：2014年12月30日</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>GoPlay</p></td>
<td style="text-align: left;"><p>營運日期：2015年3月26日<br />
結束營運：2016年8月15日</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>／／</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/Nexon.md" title="wikilink">Nexon Europe</a></p></td>
<td style="text-align: left;"><p>營運日期：2014年9月23日~</p></td>
</tr>
</tbody>
</table>

本遊戲在[台灣及](../Page/臺灣.md "wikilink")[香港地區由](../Page/香港.md "wikilink")[gamania遊戲橘子代理發行](../Page/遊戲橘子.md "wikilink")\[2\]，[中國大陸地區則由](../Page/中國大陸.md "wikilink")[世紀天成代理](../Page/世紀天成.md "wikilink")\[3\]。

另外，本游戏在歐美地區以《Counter-Strike
Nexon:Zombies》的名称加入[Steam](../Page/Steam.md "wikilink")，但只對歐美地區開放，並由[Nexon
Europe經營](../Page/Nexon#歐洲.md "wikilink")\[4\]

## 遊戲命名

絕對武力 Online 的英文全名為「Counter Strike
Online（CSO）」，常錯拼為「Counter-Strike:Online（CS:O）」。

一般不將 CSO 視作CS系列中的遊戲，但大多數人容易將 CSO 簡稱為 CS 而將兩者混淆。

另外，CS系列版本的命名皆為「Counter-Strike:版本名」。例如：

「Counter-Strike: Condition Zero（CS:CZ）」 「Counter-Strike: Source（CS:S）」
「Counter-Strike: Global Offensive（CS:GO）」

## 發行

該款遊戲可能因其本質與 Valve 的原生遊戲 Counter-Strike 不同，所以 Counter-Strike Online 並不屬於
Counter-Strike 系列中的遊戲，而是一款獨立的網絡遊戲。

原生遊戲皆為產品賣斷的形式，且玩家可自行架設伺服器與他人同樂；而 Counter-Strike Online
雖然主程序免費，但遊戲中的武器和道具大部分需要付費取得。

另外，遊戲中的文件和原生遊戲不同，無法隨便更改，全部內容都被封裝在NAR檔案中，一旦發現惡意更改，玩家可能受到遊戲官方的懲罰。

## 參見

  - [絕對武力](../Page/絕對武力.md "wikilink")
  - [絕對武力：一觸即發](../Page/絕對武力：一觸即發.md "wikilink")
  - [維爾福](../Page/維爾福.md "wikilink")

## 參考文獻

<references/>

## 外部連結

  - [韓國官方網站](http://csonline.nexon.com)<small>
  - [台灣官方網站](http://tw.beanfun.com/cso/)
  - [香港官方網站](http://hk.beanfun.com/cso/)
  - [中國官方網站](http://csol.tiancity.com/)
  - [日本官方網站](http://cso.nexon.co.jp/)
  - [印尼官方網站](http://www.csonline.co.id/)
  - [Counter-Strike Nexon: Zombies
    官方網站](http://store.steampowered.com/app/273110/)（CSO分支，遊戲內容相同)<small>（附属于[Steam](../Page/Steam.md "wikilink")，仅限欧美用户使用）</small>

<!-- end list -->

  -
  -
[C](../Category/Windows遊戲.md "wikilink")
[C](../Category/恐怖活動題材作品.md "wikilink")
[Category:反恐題材電子遊戲](../Category/反恐題材電子遊戲.md "wikilink")
[C](../Category/2007年電子遊戲.md "wikilink")
[C](../Category/第一人稱射擊遊戲.md "wikilink")
[C](../Category/大型多人線上第一人稱射擊遊戲.md "wikilink")
[Category:架空歷史電子遊戲](../Category/架空歷史電子遊戲.md "wikilink")
[Category:喪屍遊戲](../Category/喪屍遊戲.md "wikilink")
[Category:美國背景電子遊戲](../Category/美國背景電子遊戲.md "wikilink")
[Category:意大利背景電子遊戲](../Category/意大利背景電子遊戲.md "wikilink")
[Category:墨西哥背景電子遊戲](../Category/墨西哥背景電子遊戲.md "wikilink")
[Category:古巴背景電子遊戲](../Category/古巴背景電子遊戲.md "wikilink")
[Category:英國背景電子遊戲](../Category/英國背景電子遊戲.md "wikilink")
[Category:日本背景電子遊戲](../Category/日本背景電子遊戲.md "wikilink")
[Category:香港背景電子遊戲](../Category/香港背景電子遊戲.md "wikilink")
[Category:俄羅斯背景電子遊戲](../Category/俄羅斯背景電子遊戲.md "wikilink")
[Category:中國背景電子遊戲](../Category/中國背景電子遊戲.md "wikilink")
[Category:伊拉克背景電子遊戲](../Category/伊拉克背景電子遊戲.md "wikilink")
[Category:土耳其背景電子遊戲](../Category/土耳其背景電子遊戲.md "wikilink")
[Category:泰國背景電子遊戲](../Category/泰國背景電子遊戲.md "wikilink")
[Category:哥倫比亞背景電子遊戲](../Category/哥倫比亞背景電子遊戲.md "wikilink")
[Category:希臘背景電子遊戲](../Category/希臘背景電子遊戲.md "wikilink")
[Category:中東背景電子遊戲](../Category/中東背景電子遊戲.md "wikilink")
[Category:威尼斯背景電子遊戲](../Category/威尼斯背景電子遊戲.md "wikilink")
[Category:邁阿密背景遊戲](../Category/邁阿密背景遊戲.md "wikilink")
[Category:東京背景電子遊戲](../Category/東京背景電子遊戲.md "wikilink")

1.

2.

3.
4.