**广宗县**是[河北省](../Page/河北省.md "wikilink")[邢台市下辖的一个县](../Page/邢台.md "wikilink")。县人民政府驻广宗镇。

## 历史

在广宗县[大平台乡平台村南](../Page/大平台乡.md "wikilink")，有一个长150米，宽70米的沙丘。附近的群众曾经从这里拣过一些古代的陶、铜饰件残片和绳纹砖瓦片。据考古研究，这里是[沙丘宫平台遗址](../Page/沙丘宫.md "wikilink")。

[趙武靈王在此地被圍困](../Page/趙武靈王.md "wikilink")，是稱[沙丘之亂](../Page/沙丘之亂.md "wikilink")。[秦始皇病逝于此](../Page/秦始皇.md "wikilink")，權臣[趙高](../Page/趙高.md "wikilink")、[李斯乘機作亂](../Page/李斯.md "wikilink")，殺太子[扶蘇](../Page/扶蘇.md "wikilink")，是為[沙丘之變](../Page/沙丘之變.md "wikilink")。

東漢名將[皇甫嵩曾在此與黃巾頭領之一的張梁作戰](../Page/皇甫嵩.md "wikilink")。

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、4个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [广宗县人民政府](https://web.archive.org/web/20090423184958/http://gzxzf.gov.cn/)

[广宗县](../Page/category:广宗县.md "wikilink")
[县](../Page/category:邢台区县市.md "wikilink")
[邢台](../Page/category:河北省县份.md "wikilink")
[冀](../Page/分类:国家级贫困县.md "wikilink")