**四平腔**，中國地方[戲曲之一](../Page/戲曲.md "wikilink")，屬於「北管音樂系統」。自[明代](../Page/明代.md "wikilink")[嘉靖年間](../Page/嘉靖.md "wikilink")，流行於[安徽](../Page/安徽.md "wikilink")[歙縣](../Page/歙縣.md "wikilink")，被稱為「庶民戲」。後來在江南數省廣為流傳，並與當地劇種結合。\[1\]\[2\]清中葉以後，傳入[潮州的四平腔吸收了海陸豐](../Page/潮州.md "wikilink")[正字戲及](../Page/正字戲.md "wikilink")[亂彈戲](../Page/亂彈戲.md "wikilink")，表演變得更豐富緊湊。

四平腔重武輕文，多演歷史戰袍戲。由於場面熱鬧，深受大眾歡迎。四平腔在曲牌**聯綴體**加入了**皮黃腔**的板腔體，句末常以「幫腔」和唱，字多而腔少，使它更顯古樸粗獷。樂器則以鑼、鼓、[鈸](../Page/鈸.md "wikilink")、板為主，以鼓為指揮，控制節奏，演唱時只用[嗩吶及](../Page/嗩吶.md "wikilink")[鑼](../Page/鑼.md "wikilink")[鼓伴奏](../Page/鼓.md "wikilink")，很有特色。\[3\]

四平腔的著名劇目有《[出獵回獵](../Page/出獵回獵.md "wikilink")》，《[磨房會](../Page/磨房會.md "wikilink")》和《借靴》等。\[4\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [四平戲﹝一﹞：七十四年民間劇場演出](http://publish.cca.gov.tw/ccapb/web/search/video_detail.jsp?dtd_id=000012&sysid=0000009540)
  - [四平戲﹝二﹞：七十四年民間劇場演出](http://publish.cca.gov.tw/ccapb/web/search/video_detail.jsp?dtd_id=000012&sysid=0000010216)

[Category:戏曲唱腔](../Category/戏曲唱腔.md "wikilink")

1.  [新華網蕉城線上（連結失效）](http://nd.ndjczx.com/9/ndnews/wtpd/wtzx/2006-11-01/5569.html)
2.  [中國文化報](http://www.ccnt.gov.cn/xwzx/whbdfdt/t20061026_31278.htm)
3.  [客家戲劇音樂資料庫（連結失效）](http://www.hakka-lib.taipei.gov.tw/kh_web/theater_intro2.htm)

4.  中國大百科智慧藏