**食蟻獸**，在[生物分類學上屬於](../Page/生物分類學.md "wikilink")[哺乳綱](../Page/哺乳綱.md "wikilink")[貧齒總目](../Page/貧齒總目.md "wikilink")[披毛目下的](../Page/披毛目.md "wikilink")**蠕舌亞目**\[1\]，共有兩科四種。除了吃[蟻之外](../Page/蟻.md "wikilink")，它們也吃[白蟻](../Page/白蟻.md "wikilink")。
本亞目最大的品種包括[大食蟻獸](../Page/大食蟻獸.md "wikilink")（*Myrmecophaga
tridactyla*），連同尾巴體長可達；中型的如[小食蟻獸](../Page/小食蟻獸.md "wikilink")（*Tamandua
tetradactyla*），體長約；最小的則是[侏食蟻獸](../Page/侏食蟻獸.md "wikilink")（*Cyclopes
didactylus*），總體長只有，體溫只有攝氏33度。由於它們的體溫保持33攝氏度左右，所以在平時能量消耗方面比一般動物要小，故平時只需進食數百只蟻足以滿足未來幾天的能量。

常稱的**刺食蟻獸**並不是蠕舌亞目的品種，而是[針鼴](../Page/針鼴.md "wikilink")，屬於[原獸亞綱下](../Page/原獸亞綱.md "wikilink")[單孔目的](../Page/單孔目.md "wikilink")[針鼴科](../Page/針鼴科.md "wikilink")。[土豚](../Page/土豚.md "wikilink")、[袋食蟻獸](../Page/袋食蟻獸.md "wikilink")、[穿山甲也常被誤認為是食蟻獸](../Page/穿山甲.md "wikilink")，但其實相似的外型只是[趨同演化後的結果](../Page/趨同演化.md "wikilink")。

## 種

**蠕舌亞目 Vermilingua**

  - [侏食蟻獸科](../Page/侏食蟻獸科.md "wikilink") Cyclopedidae
      - [侏食蟻獸屬](../Page/侏食蟻獸屬.md "wikilink") *Cyclopes*
          - [侏食蟻獸](../Page/侏食蟻獸.md "wikilink") *Cyclopes didactylus*
  - [食蟻獸科](../Page/食蟻獸科.md "wikilink") Myrmecophagidae
      - [大食蟻獸屬](../Page/大食蟻獸屬.md "wikilink") *Myrmecophaga*
          - [大食蟻獸](../Page/大食蟻獸.md "wikilink") *Myrmecophaga tridactyla*
      - [小食蟻獸屬](../Page/小食蟻獸屬.md "wikilink") *Tamandua*
          - [中美小食蟻獸](../Page/中美小食蟻獸.md "wikilink") *Tamandua mexicana*
          - [小食蟻獸](../Page/小食蟻獸.md "wikilink") *Tamandua tetradactyla*

## 分布

朱食蟻獸與中美小食蟻獸的棲息地最北可以延伸至[墨西哥東南部](../Page/墨西哥.md "wikilink")，而大食蟻獸最北只到中美洲。小食蟻獸的棲息地最南可至[烏拉圭](../Page/烏拉圭.md "wikilink")，[巴西東部為除了中美小食蟻獸以外三種食蟻獸重疊的棲息地](../Page/巴西.md "wikilink")。食蟻獸在南美洲獨立演化與發展，並在[巴拿馬地峽形成後透過](../Page/巴拿馬地峽.md "wikilink")[南北美洲生物大遷徙入侵至中美洲](../Page/南北美洲生物大遷徙.md "wikilink")。

食蟻獸棲息的環境包括、[雨林](../Page/雨林.md "wikilink")、[草原及](../Page/草原.md "wikilink")[疏林草原](../Page/疏林草原.md "wikilink")。侏食蟻獸為完全樹棲，小食蟻獸會在靠近溪流和湖附近的地面與樹上尋找食物，大食蟻獸則為完全陸生，生活於疏林草原上\[2\]。

## 參考資料

<div class="references-small">

<references />

</div>

## 圖庫

Image:Silky Anteater.jpg|侏食蟻獸 Image:Myrmecophaga tridactyla - Phoenix
Zoo.jpg|大食蟻獸 Image:Tamandua_mexicana.jpg|中美小食蟻獸 Image:Lesser
Anteater.jpg|小食蟻獸

[hr:Mravojed](../Page/hr:Mravojed.md "wikilink")
[ru:Муравьеды](../Page/ru:Муравьеды.md "wikilink")

[Category:異關節總目](../Category/異關節總目.md "wikilink")
[Category:披毛目](../Category/披毛目.md "wikilink")
[Category:南美洲動物](../Category/南美洲動物.md "wikilink")

1.
2.