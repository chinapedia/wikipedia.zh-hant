} |list1name = 儒學理論 |list1title = 儒學理論 |list1 = [仁](仁.md "wikilink")
[義](義.md "wikilink") [礼](礼乐制度.md "wikilink") [智](智.md "wikilink")
[信](誠信.md "wikilink")
[天命](天命.md "wikilink") [良知](良知.md "wikilink")
[天理](天理_\(儒家\).md "wikilink") [中庸](中庸.md "wikilink")
[三綱五常](三綱五常.md "wikilink") [四端](四端.md "wikilink") [五倫](五倫.md "wikilink")
[四維八德](四維八德.md "wikilink")
[仁政](仁政.md "wikilink") [名教](名教.md "wikilink") [宗法](宗法.md "wikilink")
[井田](井田.md "wikilink")
[格物致知](格物致知.md "wikilink") [内圣外王](内圣外王.md "wikilink") [大同
(思想)](大同_\(思想\).md "wikilink")
[道統](道統.md "wikilink") [聖賢](聖賢.md "wikilink") [君子](君子.md "wikilink") [王道
(儒家思想)](王道_\(儒家思想\).md "wikilink")

|list2name = 儒門人物 |list2title = 儒門人物 |list2 = [堯](堯.md "wikilink")
[舜](舜.md "wikilink") [禹](禹.md "wikilink") [商湯](商湯.md "wikilink")
[周文王](周文王.md "wikilink") [周武王](周武王.md "wikilink")
[周公](周公.md "wikilink")
[孔子](孔子.md "wikilink") [孟子](孟子.md "wikilink") [荀子](荀子.md "wikilink")


**四配**
[顏回](顏回.md "wikilink") [孟子](孟子.md "wikilink")
[曾参](曾参.md "wikilink") [孔伋](孔伋.md "wikilink")


**十二哲**
[闵损](闵损.md "wikilink") [冉雍](冉雍.md "wikilink") [端木赐](端木赐.md "wikilink")
[仲由](仲由.md "wikilink") [卜商](卜商.md "wikilink") [有若](有若.md "wikilink")
[宰予](宰予.md "wikilink") [冉耕](冉耕.md "wikilink") [冉求](冉求.md "wikilink")
[言偃](言偃.md "wikilink") [颛孙师](颛孙师.md "wikilink")
[朱熹](朱熹.md "wikilink")

|list3name = 古代儒者 |list3title = 古代儒者 |list3 = **中國**
[董仲舒](董仲舒.md "wikilink") [何休](何休.md "wikilink") [鄭玄](鄭玄.md "wikilink")
[王肅](王肅.md "wikilink")
[王通](王通_\(隋朝\).md "wikilink") [韓愈](韓愈.md "wikilink")
[程颢](程颢.md "wikilink") [程颐](程颐.md "wikilink")
[邵雍](邵雍.md "wikilink") [周敦頤](周敦頤.md "wikilink")
[張載](張載_\(北宋\).md "wikilink") [朱熹](朱熹.md "wikilink")
[陸九淵](陸九淵.md "wikilink") [吳澄](吳澄.md "wikilink") [陳獻章](陳獻章.md "wikilink")
[王守仁](王守仁.md "wikilink")
[薛瑄](薛瑄.md "wikilink") [吕坤](吕坤.md "wikilink") [罗钦顺](罗钦顺.md "wikilink")
[顾炎武](顾炎武.md "wikilink")
[王夫之](王夫之.md "wikilink") [黄宗羲](黄宗羲.md "wikilink") [颜元](颜元.md "wikilink")
[戴震](戴震.md "wikilink")

**日本**
[藤原惺窩](藤原惺窩.md "wikilink") [林羅山](林羅山.md "wikilink")
[室鳩巢](室鳩巢.md "wikilink")
[新井白石](新井白石.md "wikilink") [雨森芳洲](雨森芳洲.md "wikilink")

**朝鮮**
[薛聰](薛聰_\(新羅\).md "wikilink") [權近](權近.md "wikilink")
[吉再](吉再.md "wikilink") [安珦](安珦.md "wikilink")
[李穡](李穡.md "wikilink")
[李滉](李滉.md "wikilink") [王仁](王仁.md "wikilink") [李齊賢](李齊賢.md "wikilink")
[鄭夢周](鄭夢周.md "wikilink") [鄭道傳](鄭道傳.md "wikilink")
[崔致遠](崔致遠.md "wikilink") [徐敬德](徐敬德.md "wikilink")
[趙光祖](趙光祖.md "wikilink") [李彥迪](李彥迪.md "wikilink")
[李退溪](李退溪.md "wikilink") [李栗谷](李栗谷.md "wikilink")


**越南**
[朱文安](朱文安.md "wikilink") [阮秉谦](阮秉谦.md "wikilink")
[阮廌](阮廌.md "wikilink")
[黎贵惇](黎贵惇.md "wikilink") [阮文超](阮文超.md "wikilink")
[吳時任](吳時任.md "wikilink")


**琉球**
[程順則](程順則.md "wikilink") [向象賢](向象賢.md "wikilink") [蔡溫](蔡溫.md "wikilink")

|list4name = 儒家經書 |list4title = 儒家經書 |list4 =
[六经](六经.md "wikilink")　[五经](五经.md "wikilink")　[九經](九經.md "wikilink")　[四书](四书.md "wikilink")　[十三经](十三经.md "wikilink")
</br>[周易](周易.md "wikilink")　[尚書](尚書_\(書籍\).md "wikilink")　[詩經](詩經.md "wikilink")</br>[周禮](周禮.md "wikilink")　[儀禮](儀禮.md "wikilink")　[禮記](禮記.md "wikilink")</br>[春秋](春秋_\(史書\).md "wikilink")　[左傳](左傳.md "wikilink")　[公羊傳](公羊傳.md "wikilink")　[穀梁傳](穀梁傳.md "wikilink")</br>[孝經](孝經.md "wikilink")　[論語](論語.md "wikilink")　[爾雅](爾雅.md "wikilink")　[孟子](孟子_\(著作\).md "wikilink")</br>[大學](大學_\(經傳\).md "wikilink")　[中庸](中庸.md "wikilink")
</br>[十三經注疏](十三經注疏.md "wikilink")　[四書章句集注](四書章句集注.md "wikilink")

|list5name = 古典儒學 |list5title = 古典儒學 |list5 = **中国儒学**
[经学](经学.md "wikilink") [今文經學](今文經學.md "wikilink")
[古文經學](古文經學.md "wikilink")
[宋明理學](宋明理學.md "wikilink") [程朱理学](程朱理学.md "wikilink")
[陆王心学](陆王心学.md "wikilink")
[陽明學](陽明學.md "wikilink") [朴学](朴学.md "wikilink")
[經世之學](經世之學.md "wikilink") [实学](实学.md "wikilink")


**日本儒学**
[水戶學](水戶學.md "wikilink") [石門心學](石門心學.md "wikilink")


**朝鮮儒學**

**越南儒學**


**琉球儒學**

|list6name = 當代儒學 |list6title = 當代儒學 |list6 = **學者**
[熊十力](熊十力.md "wikilink") [梁漱溟](梁漱溟.md "wikilink")
[马一浮](马一浮.md "wikilink") [唐君毅](唐君毅.md "wikilink")
[牟宗三](牟宗三.md "wikilink") [方东美](方东美.md "wikilink")
[徐复观](徐复观.md "wikilink") [张君劢](张君劢.md "wikilink")
[蔣慶](蔣慶.md "wikilink") [杜維明](杜維明.md "wikilink") [成中英](成中英.md "wikilink")
[刘述先](刘述先.md "wikilink")


**學派**
[新儒家](新儒家.md "wikilink") [孔教](儒教_\(宗教\).md "wikilink")
[學衡派](學衡派.md "wikilink") [波士頓儒家](波士頓儒家.md "wikilink")

|list7name = 相关事项 |list7title = 相关事项 |list7 = [六艺](六艺.md "wikilink")
[孔子弟子](孔子弟子列表.md "wikilink") [三纲五常](三纲五常.md "wikilink")
[五经博士](五经博士.md "wikilink") [书院](书院.md "wikilink")
[孔庙](孔庙.md "wikilink")
[衍聖公](衍聖公.md "wikilink") [科举](科举.md "wikilink") [国子监](国子监.md "wikilink")
[批林批孔](批林批孔.md "wikilink")[孔子和平獎](孔子和平獎.md "wikilink")

|below = [儒家文化圈](Portal:漢字文化圈.md "wikilink") }}<noinclude> </noinclude>

[Category:儒學](../Category/儒學.md "wikilink")
[Category:哲學與思想模板](../Category/哲學與思想模板.md "wikilink")