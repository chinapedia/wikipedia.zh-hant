**Lia**（）是[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[唱作歌手](../Page/唱作歌手.md "wikilink")，1st
Place所屬。為[IA -ARIA ON THE
PLANETES-的聲源](../Page/IA_-ARIA_ON_THE_PLANETES-.md "wikilink")。擁有治癒系的歌聲，其歌唱力亦極受肯定，並參加**[I've](../Page/I've.md "wikilink")**。她亦在[快樂硬核的領域中活躍](../Page/快樂硬核.md "wikilink")（以大寫「**LIA**」為名義），現居香港\[1\]。

## 作品列表

### Lia名義

#### 單曲

<table>
<thead>
<tr class="header">
<th></th>
<th><p>發售日</p></th>
<th><p>標題</p></th>
<th><p><a href="../Page/規格編號.md" title="wikilink">規格編號</a></p></th>
<th><p>最高名次</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2001年12月29日</p></td>
<td><p><strong>SHIFT〜世代の向こう〜</strong></p></td>
<td><p>IVCD-0001</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2001年12月24日</p></td>
<td><p><strong>Natukage／nostalgia</strong></p></td>
<td><p>KSLA-0002[2]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2004年6月25日</p></td>
<td><p><strong>Birthday Song,Requiem</strong></p></td>
<td><p>KSLA-0007[3]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2004年6月25日</p></td>
<td><p><strong>Spica／Hanabi／Moon</strong></p></td>
<td><p>KSLA-0008[4]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>2005年3月24日</p></td>
<td><p><strong>君の余韻 〜遠い空の下で〜</strong></p></td>
<td><p>KICM-1130</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p>2006年6月21日</p></td>
<td><p><strong>PRIDE 〜try to fight〜</strong></p></td>
<td><p>MJCD-23020</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p>2006年11月22日</p></td>
<td><p><strong>Over the Future</strong></p></td>
<td><p>MJCD-23027</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p>2008年1月30日</p></td>
<td><p><strong>doll／human</strong></p></td>
<td><p>MJCD-230387</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9th</p></td>
<td><p>2008年11月14日</p></td>
<td><p><strong><a href="../Page/時を刻む唄/TORCH.md" title="wikilink">時を刻む唄／TORCH</a></strong></p></td>
<td><p>KSLA-0044</p></td>
<td><p>13名</p></td>
</tr>
<tr class="even">
<td><p>10th</p></td>
<td><p>2010年5月26日</p></td>
<td><p><strong><a href="../Page/My_Soul,_Your_Beats!/Brave_Song.md" title="wikilink">My Soul,Your Beats!</a></strong></p></td>
<td><p>KSLA-0053</p></td>
<td><p>3名</p></td>
</tr>
<tr class="odd">
<td><p>11th</p></td>
<td><p>2010年7月21日</p></td>
<td><p><strong><a href="../Page/あした天気になぁれ.md" title="wikilink">あした天気になぁれ</a></strong></p></td>
<td><p>NECM-12165</p></td>
<td><p>189名</p></td>
</tr>
<tr class="even">
<td><p>12th</p></td>
<td><p>2010年10月27日</p></td>
<td><p><strong><a href="../Page/絆-kizunairo-色.md" title="wikilink">絆-kizunairo-色</a></strong></p></td>
<td><p>AMG-7018（初回盤）<br />
AMG-7019（通常盤）</p></td>
<td><p>6名</p></td>
</tr>
<tr class="odd">
<td><p>13th</p></td>
<td><p>2010年12月15日</p></td>
<td><p><strong><a href="../Page/心に届く詩.md" title="wikilink">心に届く詩</a></strong></p></td>
<td><p>PCCG-90058</p></td>
<td><p>46名</p></td>
</tr>
<tr class="even">
<td><p>※</p></td>
<td><p>2011年5月18日</p></td>
<td><p><strong><a href="../Page/A_Whole_New_World_God_Only_Knows.md" title="wikilink">A Whole New World God Only Knows</a></strong></p></td>
<td><p>GNCA-0192</p></td>
<td><p>19名</p></td>
</tr>
<tr class="odd">
<td><p>14th</p></td>
<td><p>2011年7月27日（amazon限定盤）<br />
2011年8月17日（通常盤）</p></td>
<td><p><strong>きみはひとりなんかじゃないよ／Song of Life</strong></p></td>
<td><p>MJMCD-03（amazon限定盤）<br />
MJMCD-04（通常盤）</p></td>
<td><p>177名</p></td>
</tr>
<tr class="even">
<td><p>15th</p></td>
<td><p>2012年11月21日</p></td>
<td><p><strong>ローレライの詩</strong></p></td>
<td><p>PCCG-90087</p></td>
<td><p>56名</p></td>
</tr>
<tr class="odd">
<td><p>16th</p></td>
<td><p>2014年3月5日</p></td>
<td><p><strong>JUSTITIA</strong></p></td>
<td><p>PCCG-70209</p></td>
<td><p>29名</p></td>
</tr>
<tr class="even">
<td><p>17th</p></td>
<td><p>2015年4月1日</p></td>
<td><p><strong>Heartily Song</strong></p></td>
<td><p>KSLM-99</p></td>
<td><p>17名</p></td>
</tr>
<tr class="odd">
<td><p>18th</p></td>
<td><p>2015年8月26日</p></td>
<td><p>"""Bravely You/灼け落ちない翼'''</p></td>
<td><p>KSLA-0104</p></td>
<td><p>4名</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 專輯

##### 原創專輯

<table>
<thead>
<tr class="header">
<th></th>
<th><p>發售日</p></th>
<th><p>標題</p></th>
<th><p><a href="../Page/規格編號.md" title="wikilink">規格編號</a></p></th>
<th><p>最高名次</p></th>
<th><p>收錄曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2004年6月25日</p></td>
<td><p><strong>prismatic</strong></p></td>
<td><p>QLCD-0001</p></td>
<td><p>190名</p></td>
<td><p>～HIKARI～／NEW WORLD／Farewell Song／Goin'on!／getting started／FAIRY LAND／／Starting Over／You Are...／SAYONARA／I'm wondering／enigmatic／Birthday Song,Requiem／Light In the Air</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2005年5月25日</p></td>
<td><p><strong>Colors of Life</strong></p></td>
<td><p>EUCA-1</p></td>
<td><p>92名</p></td>
<td><p>will／桜風（another dream mix）／夏の蝶／Star Light Night／君の余韻～遠い空の下で～（summer lights mix）／観覧車／彩～Colors of Life ～／大切な君へ…／Mother Rhythm／君の余韻～遠い空の下で～／桜風</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2005年12月29日</p></td>
<td><p><strong>gift</strong></p></td>
<td><p>QLCD-0003（QLVD-0003）</p></td>
<td><p>未上榜</p></td>
<td><p>荒城の月／Love Sick Maze／I dream／White Sky／射光の丘／gift（Duo Version）</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2006年11月1日</p></td>
<td><p><strong>dearly</strong></p></td>
<td><p>QLCD-0004</p></td>
<td><p>145名</p></td>
<td><p>Life／Sky high／Last regrets（feat.彩菜 duo version）／karma／Beautiful Dreamer～Interlude～／appear／君の翔ぶ空／THE FORCE OF LOVE～Interlude～／River of Time／虹イロ三日月／時間の風／射光の丘／dearly</p></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>2008年9月2日（queens label流通）</p></td>
<td><p><strong><a href="../Page/new_moon.md" title="wikilink">new moon</a></strong></p></td>
<td><p>PCCR-90031</p></td>
<td><p>107名</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年9月3日（ポニーキャニオン流通）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 精選輯

|     | 發售日        | 標題                                                                                                                             | [規格編號](../Page/規格編號.md "wikilink") | 最高名次 |
| --- | ---------- | ------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------- | ---- |
| 1st | 2008年9月2日  | **[Lia & LIA COLLECTION ALBUM -Special Limited BOX-](../Page/Lia_&_LIA_COLLECTION_ALBUM_-Special_Limited_BOX-.md "wikilink")** | QLCD-0008/9                        | 53名  |
| 2nd | 2011年6月24日 | **[key+Lia Best 2001-2010](../Page/key+Lia_Best_2001-2010.md "wikilink")**                                                     | KSLA-0071                          | 11名  |

#### 收藏集(Collection album)

|     | 發售日         | 標題                                             | [規格編號](../Page/規格編號.md "wikilink") | 最高名次 |
| --- | ----------- | ---------------------------------------------- | ---------------------------------- | ---- |
| 1st | 2007年9月19日  | **Lia Collection Album Vol.1 -Diamond Days-**  | PCCA-90029                         | 192名 |
| 2nd | 2007年10月17日 | **Lia Collection Album Vol.2 -Crystal Voice-** | PCCA-90030                         | 213名 |

#### 黑膠唱片

  - **I'm feeling**（2003年7月1日発売　発売元：MasterWax）
    収録曲：I'm Feeling （Scott Brown & DJ Evil Remix）remix by DJ Evil, remix
    by Scott Brown／I'm Feeling （Phosphor Remix）remix by DJ Phosphor

### LIA名義

  - Lia另一個名義，是以happy hardcore為中心的Club music scene。以大文字「LIA」發表的album。

<table>
<thead>
<tr class="header">
<th><p>發售日</p></th>
<th><p>標題</p></th>
<th><p><a href="../Page/規格編號.md" title="wikilink">規格編號</a></p></th>
<th><p>最高名次</p></th>
<th><p>收錄曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2005年9月22日</p></td>
<td><p><strong>enigmatic LIA</strong></p></td>
<td><p>QLCD0002<br />
QLVD0001</p></td>
<td></td>
<td><p>輝～HIKARI～／NEW WORLD／Farewell Song／Goin'on!／getting started／FAIRY LAND／あなたがいるだけで／Starting Over／You Are...／SAYONARA／I'm wondering／enigmatic／Birthday Song,Requiem／Light In the Air</p></td>
</tr>
<tr class="even">
<td><p>2007年2月16日</p></td>
<td><p><strong>enigmatic LIA2</strong></p></td>
<td><p>QLCD0005〜0006</p></td>
<td></td>
<td><dl>
<dt></dt>
<dd>[DISC1 HARDCORE SIDE DISC]
</dd>
<dd>HARMONICS／Love Sick Maze／SPECTRUM／I REALIZE...／White Sky／／Ana／／karma／Never be alone／STARDUST／YOU&amp;I／Last regrets／FILIATION
</dd>
<dd>[DISC2 ACOUSTIC SIDE DISC]
</dd>
<dd>Goin'on／YOU&amp;I／Light In the Air／／nostalgia／starting Over／SAKURA
</dd>
<dd>2007年2月16日（queens label）
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p>2007年6月20日</p></td>
<td><p><strong><a href="../Page/Lia_&amp;_LIA_COLLECTION_ALBUM_-Special_Limited_BOX-.md" title="wikilink">Lia &amp; LIA COLLECTION ALBUM -Special Limited BOX-</a></strong></p></td>
<td><p>PCCA-90028</p></td>
<td><p>53位</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/Lia_&amp;_LIA_COLLECTION_ALBUM_-Special_Limited_BOX-#LIA*COLLECTION_ALBUM_SPECTRUM_RAYS.md" title="wikilink">LIA*COLLECTION ALBUM SPECTRUM RAYS</a></strong></p></td>
<td><p>QLCD-0012</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年4月1日</p></td>
<td><p><strong>enigmatic LIA3 -worldwide collection-</strong></p></td>
<td><p>PCCG-90031</p></td>
<td><p>72位</p></td>
<td><dl>
<dt></dt>
<dd>≪DISC1≫
</dd>
<dd>Upsurge presents -worldwide collection side-
</dd>
<dd>01 sky-high [Inverse &amp; Orbit1 Remix]
</dd>
<dd>02 The World [ORIGINAL MIX]
</dd>
<dd>03 時を刻む唄 [JAKAZiD Remix]
</dd>
<dd>04 Crystal World [teranoid Remix]
</dd>
<dd>05 new moon [Al Storm Remix]
</dd>
<dd>06 Believe ～from Vivaldi's "Winter"～ [JAKAZiD Remix]
</dd>
<dd>07 TORCH [Spy47 Remix]
</dd>
<dd>08 pure rain [ORIGINAL MIX]
</dd>
<dd>09 SANDSTORM [Fracus Remix]
</dd>
<dd>10 The Never Ending Love [ORIGINAL MIX]
</dd>
<dd>11 月童 [Reese Remix]
</dd>
<dd>12 Saya's Song [Nu Foundation Remix]
</dd>
<dd>13 Fly high again... [ORIGINAL MIX]
</dd>
<dd><hr />
</dd>
<dd>≪DISC2≫
</dd>
<dd>REDALiCE presents -HARDCORE TANO*C side-
</dd>
<dd>01 君をのせて ～from 天空の城ラピュタ～ [REDALiCE Remix]
</dd>
<dd>02 I'm feeling [源屋 Remix]
</dd>
<dd>03 FILIATION [DJ-Technetium Remix]
</dd>
<dd>04 You Are... [Betwixt &amp; Between Remix]
</dd>
<dd>05 I'm wondering [源屋 Remix]
</dd>
<dd>06 Light In the Air [Thanatos Remix]
</dd>
<dd>07 SAYONARA [REDALiCE Remix]
</dd>
<dd>08 Saya's Song [REDALiCE Remix]
</dd>
<dd>-Special Bonus Trax-
</dd>
<dd>09 DJ Sharpnel - 鳥の詩・torino-uta [the speed freak's noise rave remix]
</dd>
<dd>10 LIA's アカペラ Track
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p>2011年6月22日</p></td>
<td><p><strong>enigmaticLIA4-Anthemical Keyworlds-</strong></p></td>
<td><p>QLC-00001</p></td>
<td><p>53位</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>enigmaticLIA4-Anthemnia L's core-</strong></p></td>
<td><p>QLC-00002</p></td>
<td><p>80位</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

q流通＝queens label流通／p流通＝ポニーキャニオン流通

### 主題曲、印象曲、商業搭配

<table>
<thead>
<tr class="header">
<th><p>出處</p></th>
<th><p>曲目 - 註解</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/AIR.md" title="wikilink">AIR</a></p></td>
<td><dl>
<dt></dt>
<dd><a href="../Page/鳥の詩.md" title="wikilink">鳥の詩</a> - PC遊戲・TV動畫OP主題曲
</dd>
<dd>夏影 - 印象曲
</dd>
<dd>月童 - 同上
</dd>
<dd>青空 - PC遊戲、TV動畫插入曲
</dd>
<dd>Farewell song - PC遊戲・TV動畫ED主題歌
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/CLANNAD.md" title="wikilink">CLANNAD</a></p></td>
<td><dl>
<dt></dt>
<dd>Ana - PC遊戲、TV動畫插入曲
</dd>
<dd>願いが叶う場所 - 印象曲
</dd>
<dd>約束 - 劇場版動畫特別印象曲
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/CLANNAD.md" title="wikilink">CLANNAD AFTER STORY</a></p></td>
<td><dl>
<dt></dt>
<dd><a href="../Page/時を刻む唄/TORCH.md" title="wikilink">時を刻む唄</a> - TV動畫OP主題曲
</dd>
<dd><a href="../Page/時を刻む唄/TORCH.md" title="wikilink">TORCH</a> - TV動畫ED主題曲
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/智代after.md" title="wikilink">智代after 〜It's a Wonderful Life〜</a></p></td>
<td><dl>
<dt></dt>
<dd>Light colors - OP主題曲
</dd>
<dd>Life is like a Melody - ED主題曲
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Little_Busters!.md" title="wikilink">Little Busters! Ecstasy</a></p></td>
<td><dl>
<dt></dt>
<dd>Saya's Song - ED主題曲
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/爆球Hit!_轟烈彈珠人.md" title="wikilink">爆球Hit! クラッシュビーダマン</a></p></td>
<td><dl>
<dt></dt>
<dd>明日に向かって Get Dream! - ED主題曲[5]
</dd>
<dd>PRIDE 〜try to fight!〜 - 同上
</dd>
<dd>Over the Future - 同上
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/校園迷糊大王.md" title="wikilink">校園迷糊大王～二學期～</a></p></td>
<td><dl>
<dt></dt>
<dd>Girls Can Rock - 挿入曲
</dd>
<dd>Feel Like A Girl - 同上
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/beatmaniaIIDX.md" title="wikilink">beatmaniaIIDX</a>11 - IIDXRED</p></td>
<td><dl>
<dt></dt>
<dd>HORIZON - 收錄曲、LIA名義、由 korsk（斎藤広祐）作曲
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/頭文字D.md" title="wikilink">頭文字D</a></p></td>
<td><dl>
<dt></dt>
<dd>SKY HIGH - 第4期動畫(頭文字D Fourth Stage)挿入曲
</dd>
<dd>ALL AROUND - 同上並びにアーケードゲーム(頭文字D Arcade Stage4)挿入歌
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/RF_online.md" title="wikilink">RF online</a></p></td>
<td><dl>
<dt></dt>
<dd>War for Peace
</dd>
<dd>somewhere
</dd>
<dd>HAIL THE NATION
</dd>
<dd>solitude
</dd>
<dd>GIVE PRAISE
</dd>
<dd>THE FORCE OF LOVE
</dd>
<dd>harmonia
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/GUNSLINGER_GIRL.md" title="wikilink">GUNSLINGER GIRL -IL TEATRINO-</a></p></td>
<td><dl>
<dt></dt>
<dd>doll - ED主題曲、另有<a href="../Page/多田葵.md" title="wikilink">多田葵主唱的版本</a>
</dd>
<dd>human - 第13話ED主題曲
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/FORTUNE_ARTERIAL.md" title="wikilink">FORTUNE ARTERIAL</a></p></td>
<td><dl>
<dt></dt>
<dd>赤い約束 - 挿入曲、Veil ∞ Lia名義
</dd>
<dd><a href="../Page/絆-kizunairo-色.md" title="wikilink">絆-kizunairo-色</a> - TV動畫OP主題曲
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星之歌.md" title="wikilink">ほしうた</a></p></td>
<td><dl>
<dt></dt>
<dd>尋ねビト - OP主題歌、Veil ∞ Lia名義
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星之歌.md" title="wikilink">ほしうた 〜Starlight Serenade〜</a></p></td>
<td><dl>
<dt></dt>
<dd>星屑のキズナ - OP主題歌、Veil ∞ Lia名義
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Angel_Beats!.md" title="wikilink">Angel Beats!</a></p></td>
<td><dl>
<dt></dt>
<dd><a href="../Page/My_Soul,_Your_Beats!/Brave_Song.md" title="wikilink">My Soul,Your Beats!</a> - OP主題曲
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/天翼之鍊.md" title="wikilink">天翼之鍊</a></p></td>
<td><dl>
<dt></dt>
<dd>u plus me - 新生章節用OP動畫主題歌(2013/07/25日本官網公開之影片)
</dd>
</dl></td>
</tr>
<tr class="odd">
<td></td>
<td><dl>
<dt></dt>
<dd>あした天気になぁれ - 前期片頭曲
</dd>
<dd>歩いて帰ろう - 後期片頭曲
</dd>
</dl></td>
</tr>
<tr class="even">
<td></td>
<td><dl>
<dt></dt>
<dd>心に届く詩 - 遊戲片頭曲
</dd>
<dd>WHITE and SHADOW 〜夢幻の輪舞〜 - 遊戲印象曲
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/只有神知道的世界.md" title="wikilink">只有神知道的世界</a>Ⅱ</p></td>
<td><dl>
<dt></dt>
<dd>A Whole New World God Only Knows - OP主題曲
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/光明之刃.md" title="wikilink">シャイニング・ブレイド</a></p></td>
<td><dl>
<dt></dt>
<dd>ローレライの詩 - 遊戲片頭曲
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/WIZARD_BARRISTERS～弁魔士賽希爾.md" title="wikilink">WIZARD BARRISTERS～弁魔士賽希爾</a></p></td>
<td><dl>
<dt></dt>
<dd>JUSTITIA - TV動畫OP主題曲
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/目隱城市的演繹者.md" title="wikilink">目隱城市的演繹者</a></p></td>
<td><dl>
<dt></dt>
<dd>days - TV動畫ED主題曲
</dd>
</dl></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Charlotte_(動畫).md" title="wikilink">Charlotte (動畫)</a></p></td>
<td><dl>
<dt></dt>
<dd>Bravely You - TV動畫OP主題曲
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星之夢.md" title="wikilink">星之夢</a></p></td>
<td><dl>
<dt></dt>
<dd>星の舟 - 劇場版主題曲
</dd>
</dl></td>
</tr>
</tbody>
</table>

## 注释

## 外部連結

  - [queens label official site](http://queenslabel.product.co.jp/)

[Category:日本女性創作歌手](../Category/日本女性創作歌手.md "wikilink")
[Category:I've](../Category/I've.md "wikilink") [Category:Key Sounds
Label](../Category/Key_Sounds_Label.md "wikilink")
[Category:愛貝克思集團藝人](../Category/愛貝克思集團藝人.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:Oricon動畫單曲年榜冠軍獲得者](../Category/Oricon動畫單曲年榜冠軍獲得者.md "wikilink")
[Category:電子遊戲音樂家](../Category/電子遊戲音樂家.md "wikilink")
[Category:伯克利音樂學院校友](../Category/伯克利音樂學院校友.md "wikilink")

1.  【Lia & IA】“A Special Night” Lia & IA Live in Hong Kong 2016
2.  2001年8月10日Comic Market先行發售
3.  2002年12月28日Comic Market先行發售
4.  2003年8月15日Comic Market先行發售
5.  収録シングルは同アニメのオープニング主題歌を担当した[松原剛志との合同名義](../Page/松原剛志.md "wikilink")。サウンドプロデュースに[浜崎あゆみ](../Page/浜崎あゆみ.md "wikilink")、[右手愛美などのj](../Page/右手愛美.md "wikilink")-popシンガーに曲を提供してきた[HΛLが手がけている](../Page/HΛL.md "wikilink")。