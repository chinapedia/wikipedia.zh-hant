**葛立恆數**由[葛立恆提出](../Page/葛立恆.md "wikilink")，曾經被視為在正式數學[證明中出現過最大的數](../Page/證明.md "wikilink")，後來則被取代。它大得連[高德納箭號表示法也難以簡單表示](../Page/高德納箭號表示法.md "wikilink")，而必須使用64層高德納箭號表示法才表示得出來。

[馬丁·加德納於](../Page/馬丁·加德納.md "wikilink")1977年11月在美國[科學人雜誌的](../Page/科學人.md "wikilink")「數學遊戲」專欄將此數刊登出來，1980年被金氏世界紀錄訂為在正式數學[證明中出現過最大的數](../Page/證明.md "wikilink")。

## 問題背景

[GrahamCube.svg](https://zh.wikipedia.org/wiki/File:GrahamCube.svg "fig:GrahamCube.svg")
葛立恆數與[拉姆齊理論有關](../Page/拉姆齊理論.md "wikilink")：考慮一個*n*維的[超立方体](../Page/超立方体.md "wikilink")，在連結所有頂點後，將形成一個2<sup>*n*</sup>個頂點的[完全圖](../Page/完全圖.md "wikilink")。將這個圖的每條邊填上紅色或藍色。求*n*的最小值，才使得所有填法中都必定存在一個在同一平面上有四個頂點的單色完全子圖。

在1971年Graham與Rothschild證明了此題之解*N\**的上下界為6 ≤ *N\** ≤
*N*，其中*N*是一個極大但明確定義的數字\(F^7(12) = F(F(F(F(F(F(F(12)))))))\)，用[高德納箭號表示法](../Page/高德納箭號表示法.md "wikilink")，F可表為\(F(n) = 2\uparrow^n 3\)；[康威鏈式箭號表示法也可以表示此數的大略範圍](../Page/康威鏈式箭號表示法.md "wikilink")，此數介於4
→ 2 → 8 → 2與2 → 3 → 9 → 2之間。\[1\]
，*N\**的上界在2014年藉由Hales–Jewett數的上界而降為\(N' = 2\uparrow\uparrow\uparrow6\)；\[2\]下界則在2003年由Geoffrey
Exoo提高為11。\[3\]，並由Jerome
Barkley在2008年進一步的提高為13。\[4\]因此目前所知最佳的*N\**上下界為13
≤ *N\** ≤ *N*'。

葛立恆數*G*比*N*大得多，*G*可表為\(f^{64}(4)\)，其中\(f(n) = 3 \uparrow^n 3\)。葛立恆數為此問題的較弱上界，是葛立恆未出版的工作，最後由馬丁·加德納刊登在1977年11月的美國[科學人雜誌](../Page/科學人雜誌.md "wikilink")。\[5\]

## 定義

定義函數\(f(n) = 3 \uparrow^n 3 = 3[n + 2]3 = 3 \rightarrow 3 \rightarrow n\)（參見[高德納箭號表示法](../Page/高德納箭號表示法.md "wikilink")、[超運算](../Page/超運算.md "wikilink")、[康威鏈式箭號表示法](../Page/康威鏈式箭號表示法.md "wikilink")），則葛立恆數可使用[疊代函數表示為](../Page/疊代函數.md "wikilink")\(f^{64}(4)\)。

雖然葛立恆數不可以用康威鏈式箭號表示法很方便地表達，但康威鏈式箭號表示法能為它簡單地定上下界：

\[3 \rightarrow 3 \rightarrow 64 \rightarrow 2 <\]葛立恆數\(< 3 \rightarrow 3 \rightarrow 65 \rightarrow 2\)

使用高德納箭號表示法來表示葛立恆數：

\[G = \underbrace{3^{3^{3^{\cdot^{\cdot^{\cdot^{3 \uparrow \uparrow \uparrow \uparrow 3}\cdot}\cdot}\cdot}3}3}3}_{64\text{ layers}} = \left. 3 \underbrace{\uparrow \uparrow \cdots \cdots \cdots \cdots \uparrow}_{\displaystyle 3 \underbrace{\uparrow \uparrow \cdots \cdots \cdots \uparrow}_{\displaystyle \underbrace{\qquad \vdots \qquad}_{\displaystyle 3 \underbrace{\uparrow \uparrow \cdots \uparrow}_{\displaystyle 3 \uparrow \uparrow \uparrow \uparrow3}3}}3}3 \right \} 64 \text{ layers}\]

## 巨大的葛立恆數

利用[超運算](../Page/超運算.md "wikilink")，葛立恆數*G*可以表示為：

\[\underbrace{3 [3 [3 [ \cdots 3 [ 3[ 3}_{64} [6] 3+2] 3+2] 3 \cdots ] 3+2] 3+2] 3\]

其中，\(64\)表示共有64層超運算。從內至外，每一層中的超運算級數由方括號內的那一層所表示的數值決定。
計算*G*值需要經過64步，首先從最內層開始計算：

\[g_1
= 3 [6] 3
= 3 [5] (3 [5] 3)
= \underbrace{3 [4] (3 [4] (3 [4] \cdots (3 [4] (3 [4] 3}_{3 [5] 3})) \cdots ))\]

讓：\(X
= 3 [5] 3
= 3 [4] (3 [4] 3)
= 3 [4] (3 [3] (3 [3] 3))
= 3 [4] (3 [3] 27)\)
\[= 3 [4] 7625597484987
= \underbrace{3 [3] 3 [3] \cdots [3] 3}_{7625597484987}
= \underbrace{3_{}^{3^{{}^{.\,^{.\,^{.\,^3}}}}}}_{7625597484987}\]（[迭代冪次](../Page/迭代冪次.md "wikilink")）

\[g_1
= 3 [6] 3
= 3 [5] (3 [5] 3)
= 3 [5] X
= \underbrace{3 [4] 3 [4] \cdots [4] 3}_X
= \left. \underbrace{3^{3^{.^{.^{.{3}}}}}}_{ \underbrace{3^{3^{.^{.^{.{3}}}}}}_{ \underbrace{\vdots}_{3} }} \right\} X\]

給定函數：

\[f(n) = 3 [n+2] 3\]

例如：\(f(1) = 3 [3] 3,\ f(2) = 3 [4] 3\)

\[g_1=f(4)
= 3 [6] 3
= 3 [5] X
= \underbrace{3 [4] 3 [4] \cdots [4] 3}_X
= \left. \underbrace{3^{3^{.^{.^{.{3}}}}}}_{ \underbrace{3^{3^{.^{.^{.{3}}}}}}_{ \underbrace{\vdots}_{3} }} \right\} X\]

然後計算g<sub>2</sub>：

\[g_2 = f^{2}(4) = f(f(4)) = 3 [g_1+2] 3\]

接著計算：

\[g_3 = f(f^{2}(4)) = 3 [g_2+2] 3\]

\[\vdots \vdots\]

\[g_{63} = f(f^{62}(4)) = 3 [g_{62}+2] 3\] 最後：

\[G = g_{64} = f^{64}(4)= \underbrace{f(f(\cdots f}_{64}(4) \cdots )) = 3 [g_{63}+2] 3\]

一般來說：

\[g_n = 3 [g_{n-1}+2] 3\]

其中：

\[f^{2}(n) = f(f(n)),\ f^{3}(n) = f(f(f(n))),\] 以此類推。

## 葛立恆數最尾端的500位數字

...

02425 95069 50647 38395 65747 91365 19351 79833 45353 62521

43003 54012 60267 71622 67216 04198 10652 26316 93551 88780

38814 48314 06525 26168 78509 55526 46051 07117 20009 97092

91249 54437 88874 96062 88291 17250 63001 30362 29349 16080

25459 46149 45788 71427 83235 08292 42102 09182 58967 53560

43086 99380 16892 49889 26809 95101 69055 91995 11950 27887

17830 83701 83402 36474 54888 22221 61573 22801 01329 74509

27344 59450 43433 00901 09692 80253 52751 83328 98844 61508

94042 48265 01819 38515 62535 79639 96189 93967 90549 66380

03222 34872 39670 18485 18643 90591 04575 62726 24641 95387.

事實上，對於所有正整數\(n > 500\)，\(3 [4] n\)的末500位數也與葛立恆數相同。

## 参考文献

## 外部連結

[pl:Notacja strzałkowa\#Liczba
Grahama](../Page/pl:Notacja_strzałkowa#Liczba_Grahama.md "wikilink")

[Category:离散数学](../Category/离散数学.md "wikilink")
[Category:大数](../Category/大数.md "wikilink")

1.
2.
3.  Exoo 將Graham與Rothschild提出的上界*N*稱為「葛立恆數」，但這不是馬丁·加德納所說的「葛立恆數」*G*。
4.
5.  [馬丁·加德納](../Page/馬丁·加德納.md "wikilink") (1977) ["In which joining
    sets of points leads into diverse (and diverting)
    paths"](http://iteror.org/big/Source/Graham-Gardner/GrahamsNumber.html).
    Scientific American, November 1977