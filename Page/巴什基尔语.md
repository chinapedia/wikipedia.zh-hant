**巴什基尔语**是[突厥语族语言之一](../Page/突厥语族.md "wikilink")，流行在[俄罗斯的](../Page/俄罗斯.md "wikilink")[巴什科尔托斯坦共和国](../Page/巴什科尔托斯坦共和国.md "wikilink")（[巴什基尔](../Page/巴什基尔.md "wikilink")）。

## 使用者

1989年普查显示[苏联有超过](../Page/苏联.md "wikilink")1,047,000巴什基尔语母语使用者，还有26,737人称巴什基尔语为他们的第二语言。大约300,000[巴什基尔人认为](../Page/巴什基尔人.md "wikilink")[鞑靼语是他们的母语](../Page/鞑靼语.md "wikilink")。

巴什基尔语使用者多在俄罗斯的[巴什科尔托斯坦共和国](../Page/巴什科尔托斯坦共和国.md "wikilink")（巴什基尔），以及邻近的[鞑靼斯坦共和国和](../Page/鞑靼斯坦共和国.md "wikilink")[乌德穆尔特共和国](../Page/乌德穆尔特共和国.md "wikilink")。也有不少使用者在[彼尔姆州和](../Page/彼尔姆州.md "wikilink")[车里雅宾斯克州](../Page/车里雅宾斯克州.md "wikilink")、[奥伦堡州](../Page/奥伦堡州.md "wikilink")、[斯维尔德洛夫斯克州](../Page/斯维尔德洛夫斯克州.md "wikilink")、[库尔干州](../Page/库尔干州.md "wikilink")，以及在[哈萨克斯坦和](../Page/哈萨克斯坦.md "wikilink")[乌兹别克斯坦的巴什基尔少数民族](../Page/乌兹别克斯坦.md "wikilink")。

## 字母和方言

尽管巴什基尔人源自乌拉尔或[芬蘭-烏戈爾部族](../Page/芬蘭-烏戈爾.md "wikilink")，他们最初采用了伏尔加语言。在13世纪[蒙古入侵之后](../Page/蒙古.md "wikilink")，因大多[金帳部族说欽察语言](../Page/金帳汗國.md "wikilink")，[欽察語变成了当时最流行的语言](../Page/欽察語.md "wikilink")。

现代巴什基尔语，与鞑靼语相似，词根都是来自欽察語支的语言。现在巴什基尔语有很多[方言](../Page/方言.md "wikilink")，这点也与鞑靼语相似。在过去，巴什基尔语使用鞑靼语作为书面语。在15世纪，书面语被[察合台語取代](../Page/察合台語.md "wikilink")（但根据一些学者指出，巴什基尔语采用察合台语的旧鞑靼语变体），直至1923年。鞑靼语和察合台语都使用[阿拉伯字母](../Page/阿拉伯字母.md "wikilink")。

在1923年，巴什基尔语建立了自己的书写体系，还创建了自己的书面语言，使用最不同于bourgeous的鞑靼语方言。首先，它使用经修改的阿拉伯字母。在1930年，它转为基于[拉丁字母的字母](../Page/拉丁字母.md "wikilink")，而在1938年冬天改用[西里尔字母](../Page/西里尔字母.md "wikilink")。

巴什基尔语所采用的西里尔字母，在[俄语字母的基础上加上以下的字母](../Page/俄语字母.md "wikilink")：
[{{IPA](../Page/Ә.md "wikilink")、[{{IPA](../Page/Ө.md "wikilink")、[{{IPA](../Page/Ү.md "wikilink")、[{{IPA](../Page/Ғ.md "wikilink")、[{{IPA](../Page/Ҡ.md "wikilink")、[{{IPA](../Page/Ң.md "wikilink")、[{{IPA](../Page/Ҙ.md "wikilink")、[{{IPA](../Page/Ҫ.md "wikilink")、[{{IPA](../Page/Һ.md "wikilink").

## 参看

  - [突厥語族斯瓦迪士核心詞列表](../Page/突厥語族斯瓦迪士核心詞列表.md "wikilink")

## 外部链接

  - [Swadesh list in Bashkir](../Page/斯瓦迪士核心詞列表.md "wikilink")
  - ["Bashinform" news
    agency](https://web.archive.org/web/20060403185029/http://bash.bashinform.ru/)
  - ["Bashkortostan"
    newspaper](https://web.archive.org/web/20060316093558/http://bashgazet.ru/bash/bnews/)
  - [Site on the 450th anniversary of Bashkortostan's joining
    Russia](http://www.bashinform.ru/450/index.php?lg=bash&section=0)
  - [Ufa city administration's
    site](https://web.archive.org/web/20060221130656/http://www.ufacity.info/bashkir/index.shtml)
  - ["Ural batyr" epos](http://uralbatir.narod.ru)
  - [Bashkir folk songs' texts](http://yir.atspace.com)
  - [Толковый русско-башкирский словарь лингвистических
    терминов](https://web.archive.org/web/20060511182842/http://www.bashedu.ru/konkurs/zainullin/index_win.htm)
  - [Статья "Башкирский язык" в Краткой энциклопедии
    Башкортостана](https://web.archive.org/web/20060615114703/http://www.bashedu.ru/encikl/bbb/b_yazyk.htm)
  - [Статья "Алфавит" в Краткой энциклопедии
    Башкортостана](https://web.archive.org/web/20060619224029/http://www.bashedu.ru/encikl/aaa/alfavit.htm)

[Category:突厥語族](../Category/突厥語族.md "wikilink")
[Category:俄罗斯语言](../Category/俄罗斯语言.md "wikilink")
[Category:巴什基爾語](../Category/巴什基爾語.md "wikilink")