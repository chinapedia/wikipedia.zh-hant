**中国棋院**，是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[国务院直属机构](../Page/国务院直属机构.md "wikilink")[国家体育总局下属的一个](../Page/国家体育总局.md "wikilink")[事业单位](../Page/事业单位.md "wikilink")，与**国家体育总局棋牌运动管理中心**[一个机构、两块牌子](../Page/一个机构、两块牌子.md "wikilink")。

中国棋院成立于1991年10月24日，1992年3月15日正式建院，总部位于[北京](../Page/北京.md "wikilink")[东城区](../Page/东城区_\(北京市\).md "wikilink")[玉蜓桥东北部](../Page/玉蜓桥.md "wikilink")，[天坛公园东侧](../Page/天坛公园.md "wikilink")\[1\]。

与[日本棋院](../Page/日本棋院.md "wikilink")、[韩国棋院等民间组织不同](../Page/韩国棋院.md "wikilink")，中国棋院具有官方地位，其管理的范围也并不仅限于[围棋](../Page/围棋.md "wikilink")，而是包括所有的[棋类](../Page/棋类.md "wikilink")、[牌类竞技项目](../Page/牌类.md "wikilink")，尤其是围棋、[中国象棋](../Page/中国象棋.md "wikilink")、[国际象棋和](../Page/国际象棋.md "wikilink")[桥牌四项竞赛](../Page/桥牌.md "wikilink")。

## 历任院长

|   |                                             |                    |
| - | ------------------------------------------- | ------------------ |
| 任 | 院长                                          | 任期                 |
| 1 | [陳祖德](../Page/陳祖德.md "wikilink")            | 1991.10－2003.6     |
| 2 | [王汝南](../Page/王汝南.md "wikilink")            | 2003.6－2007.1      |
| 3 | [华以刚](../Page/华以刚.md "wikilink")            | 2007.1－2009.6      |
| 4 | [刘思明](../Page/刘思明.md "wikilink")            | 2009.6－2015.1\[2\] |
| 5 | [朱国平](../Page/朱国平_\(中国棋院院长\).md "wikilink") | 2018.9 - 现在\[3\]   |

## 参考文献

### 引用

### 网页

  - [中国棋院简介](https://web.archive.org/web/20130218141323/http://www.qipai.org.cn/intro)

## 外部链接

  - [中国棋院](http://www.qipai.org.cn/index.php)（[国家体育总局棋牌运动管理中心](http://www.qipai.org.cn/index.php)）
  - [中国棋院 - 中国围棋协会](http://weiqi.qipai.org.cn/)
  - [中国棋院 - 中国象棋协会](http://xiangqi.qipai.org.cn/)
  - [中国棋院 - 中国国际-{}-象棋协会](http://chess.qipai.org.cn/)
  - [中国棋院 - 中国桥牌](http://bridge.qipai.org.cn/)
  - [中国棋院 - 中国围棋协会五子棋分会](http://wuziqi.qipai.org.cn/)
  - [中国棋院 - 中国国际跳棋协会](http://draughts.qipai.org.cn/)

## 参见

  - [中国围棋协会](../Page/中国围棋协会.md "wikilink")
  - [中国象棋协会](../Page/中国象棋协会.md "wikilink")
  - [中国国际象棋协会](../Page/中国国际象棋协会.md "wikilink")
  - [中国桥牌协会](../Page/中国桥牌协会.md "wikilink")

{{-}}

[Category:国家体育总局](../Category/国家体育总局.md "wikilink")
[Category:中华人民共和国体育组织](../Category/中华人民共和国体育组织.md "wikilink")
[Category:1991年建立政府機構](../Category/1991年建立政府機構.md "wikilink")
[Category:北京市东城区](../Category/北京市东城区.md "wikilink")
[Category:中国围棋](../Category/中国围棋.md "wikilink")
[Category:象棋](../Category/象棋.md "wikilink")
[Category:一个机构多块牌子](../Category/一个机构多块牌子.md "wikilink")
[Category:中國棋院](../Category/中國棋院.md "wikilink")

1.  [中国象棋网·棋院成立盛况空前](http://www.cchess.com/qirenqishi/123.html)
2.
3.