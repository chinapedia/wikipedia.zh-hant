**PukiWiki**是一套[Wiki軟體](../Page/Wiki軟體.md "wikilink")，以[PHP寫成](../Page/PHP.md "wikilink")，為[日文的Wiki網站所普遍使用](../Page/日文.md "wikilink")。此軟體是從日本人[結城浩所寫的](../Page/結城浩.md "wikilink")[YukiWiki所分支發展的](../Page/YukiWiki.md "wikilink")，從1.4版起由PukiWiki
Developers Team接續開發維護工作。

## 特色

由於PukiWiki原自於日本，因此對於[雙位元字](../Page/雙位元字.md "wikilink")（DBCS）的支援較許多由英語系所開發的Wiki軟體為佳。另外日本國內使用手機上網十分普遍，因此PukiWiki也支援了手機網頁功能。

PukiWiki支援PHP
4或5。與[MediaWiki一樣支援](../Page/MediaWiki.md "wikilink")[跨Wiki連結](../Page/跨Wiki連結.md "wikilink")（interwiki）與擴充套件。

PukiWiki
基于[PHP](../Page/PHP.md "wikilink")+TXT的方式实现，不需要任何数据库程序支援，原生程序也未支援任何数据库系统，但是可以借助名为[PukiWiki+DB的修改补丁来得到对](../Page/PukiWiki+DB.md "wikilink")[MySQL](../Page/MySQL.md "wikilink")、[SQLite](../Page/SQLite.md "wikilink")、[ORACLE
Oci8](../Page/ORACLE_Oci8.md "wikilink")、[PostgreSQL的支援](../Page/PostgreSQL.md "wikilink")。

## 衍生版本

  - [xpWiki](../Page/xpWiki.md "wikilink")：由nao-pon所開發，衍生自PukiWiki
    1.4.7版，以與[XOOPS結合為目的的Wiki軟體](../Page/XOOPS.md "wikilink")。\[1\]
  - [PukiWiki
    Mod](../Page/PukiWiki_Mod.md "wikilink")：與xpWiki同一開發者及開發目的，是衍生自PukiWiki
    1.3.x版。\[2\]
  - [PukiWiki
    Plus\!](../Page/PukiWiki_Plus!.md "wikilink")：加強[國際化等功能的衍生版本](../Page/國際化.md "wikilink")。\[3\]
      - PukiWiki Advance - PukiWiki Plus\!的一个变种\[4\]
  - [PyukiWiki](../Page/PyukiWiki.md "wikilink")：改以[Perl撰寫的衍生版本](../Page/Perl.md "wikilink")。\[5\]

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [PukiWiki 公式サイト](http://pukiwiki.sourceforge.jp/) 官方網站

  - [PukiWiki+DB](https://archive.is/20121221162058/http://blue.ribbon.to/~nekyo/pukiwiki_db)

  - [噗叽唯姬中文站](https://web.archive.org/web/20130310201455/http://weiji.meyito.com/)
    PukiWiki Advance中文

[Category:Wiki](../Category/Wiki.md "wikilink")
[Category:Wiki軟件](../Category/Wiki軟件.md "wikilink")

1.
2.
3.
4.
5.