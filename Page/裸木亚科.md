**裸木亚科**（[学名](../Page/学名.md "wikilink")：）又名**亮皮树亚科**，是[桃金娘目](../Page/桃金娘目.md "wikilink")[桃金娘科的一个](../Page/桃金娘科.md "wikilink")[演化支](../Page/演化支.md "wikilink")，包含2[属](../Page/属.md "wikilink")4[种](../Page/种.md "wikilink")。

## 特征

本亚科都是[乔木或小乔木](../Page/乔木.md "wikilink")，单[叶互生](../Page/叶.md "wikilink")，有腺点；[花瓣](../Page/花瓣.md "wikilink")5。

[裸木属是常绿乔木](../Page/裸木属.md "wikilink")，树皮光亮[白色](../Page/白色.md "wikilink")，含芳香油，无托叶；[花相当大](../Page/花.md "wikilink")；[果实为](../Page/果实.md "wikilink")[浆果](../Page/浆果.md "wikilink")。

[异裂果属](../Page/异裂果属.md "wikilink")5-7米高；有托叶；花有香味，淡黄色或粉红色，花期为12月至3月；果实为蒴果。

## 分布

裸木属只有一种——[裸木](../Page/裸木.md "wikilink")（**），只生长在[印度洋中的](../Page/印度洋.md "wikilink")[马斯克林群岛](../Page/马斯克林群岛.md "wikilink")（法属[留尼旺岛和](../Page/留尼旺岛.md "wikilink")[毛里求斯](../Page/毛里求斯.md "wikilink")），是当地的特有种。

[异裂果属有三种](../Page/异裂果属.md "wikilink")，都分布于[非洲南部](../Page/非洲.md "wikilink")。

## 分类

1981年的[克朗奎斯特分类法将裸木属列在](../Page/克朗奎斯特分类法.md "wikilink")[桃金娘科中](../Page/桃金娘科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该单独分出一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")——**裸木科**（），仍然列在[桃金娘目中](../Page/桃金娘目.md "wikilink")，只有1[属](../Page/属.md "wikilink")1[种](../Page/种.md "wikilink")——[裸木属](../Page/裸木属.md "wikilink")[裸木](../Page/裸木.md "wikilink")，2003年经过修订的[APG
II 分类法维持原分类](../Page/APG_II_分类法.md "wikilink")。

后来发现异裂果科的[异裂果属是裸木属的](../Page/异裂果属.md "wikilink")[旁系群](../Page/旁系群.md "wikilink")，两者又是其它桃金娘科的旁系群，2009年修订的[APG
III分类法便将裸木科与异裂果科重新并入桃金娘科](../Page/APG_III分类法.md "wikilink")，成立包含这两个属的裸木亚科。

## 参考文献

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[裸木属](http://delta-intkey.com/angio/www/psiloxyl.htm)
  - [APweb网站中的裸木亚科](http://www.mobot.org/MOBOT/Research/APweb/orders/myrtalesweb2.htm#Myrtaceae)

[裸木亚科](../Category/裸木亚科.md "wikilink")