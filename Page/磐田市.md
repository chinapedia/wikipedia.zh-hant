**磐田市**（）為[日本](../Page/日本.md "wikilink")[靜岡縣西部地区的](../Page/靜岡縣.md "wikilink")[市](../Page/市.md "wikilink")。過去為[磐田郡的一部份](../Page/磐田郡.md "wikilink")。

## 地理

位于靜岡縣西部，[天龍川東岸的](../Page/天龍川.md "wikilink")[磐田原台地上](../Page/磐田原台地.md "wikilink")。

  - [沼](../Page/沼.md "wikilink")：[桶谷沼](../Page/桶谷沼.md "wikilink")
  - [海岸](../Page/海岸.md "wikilink")：[遠州灘](../Page/遠州灘.md "wikilink")

### 相鄰的自治体

  - [濱松市](../Page/濱松市.md "wikilink")[南区](../Page/南區_\(濱松市\).md "wikilink")、[東区](../Page/東區_\(濱松市\).md "wikilink")、[濱北区](../Page/濱北区.md "wikilink")、[天龍区](../Page/天龍区.md "wikilink")
  - [袋井市](../Page/袋井市.md "wikilink")
  - [周智郡](../Page/周智郡.md "wikilink")[森町](../Page/森町_\(靜岡縣\).md "wikilink")

## 友好城市

### 國外

  - 姐妹市

<!-- end list -->

  - [邦阿西楠省](../Page/邦阿西楠省.md "wikilink")[達古潘市](../Page/達古潘市.md "wikilink")（Dagupan）

      -
        1975年（昭和50年）2月19日 締結

  - [加州](../Page/加州.md "wikilink")[山景城](../Page/山景城_\(加利福尼亞州\).md "wikilink")（Mountain
    View）

      -
        1976年（昭和51年）6月4日 締結

### 國內

  - 友好都市

<!-- end list -->

  - [長野縣](../Page/長野縣.md "wikilink")[駒根市](../Page/駒根市.md "wikilink")
      - 1967年（昭和42年）1月12日締結。
  - 長野縣[下伊那郡](../Page/下伊那郡.md "wikilink")[喬木村](../Page/喬木村.md "wikilink")
      - 1983年（昭和58年）11月21日與[龍洋町締結](../Page/龍洋町.md "wikilink")
  - 長野縣[伊那市](../Page/伊那市.md "wikilink")
      - 原為[上伊那郡](../Page/上伊那郡.md "wikilink")[長谷村](../Page/長谷村_\(長野縣\).md "wikilink")，1984年（昭和59年）8月1日與[福田町締結](../Page/福田町.md "wikilink")
  - 長野縣[中野市](../Page/中野市.md "wikilink")
      - 原為[下水內郡](../Page/下水內郡.md "wikilink")[豐田村](../Page/豐田村_\(長野縣\).md "wikilink")，2003年（平成15年）5月3日與[豐田町締結](../Page/豐田町.md "wikilink")