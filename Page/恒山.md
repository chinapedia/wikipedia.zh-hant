**恒山**又名**元岳**、**常山**、**北岳**，明代之前指河北[大茂山](../Page/大茂山.md "wikilink")，清代顺治时因改祭北岳于[山西省](../Page/山西省.md "wikilink")[浑源县](../Page/浑源县.md "wikilink")**玄岳山**，遂称浑源玄岳山为恒山。恒山山脉为东北-西南走向，绵延150公里。恒山主峰天峰岭海拔2016.1米\[1\]，主庙北岳庙，供奉着[恒山神](../Page/恒山神.md "wikilink")，即[北岳大帝](../Page/北岳大帝.md "wikilink")。

恒山[悬空寺位于浑源县城与恒山之间](../Page/悬空寺.md "wikilink")，距恒山山门大约三公里，始建于1400多年前的[北魏王朝后期](../Page/北魏.md "wikilink")，其建筑特色可以概括为“奇、悬、巧”三个字。

## 参考文献

## 參見

  - [五嶽](../Page/五嶽.md "wikilink")
  - [三山五嶽](../Page/三山五嶽.md "wikilink")

[Category:山西山脉](../Category/山西山脉.md "wikilink")
[Category:大同地理](../Category/大同地理.md "wikilink")
[Category:浑源县](../Category/浑源县.md "wikilink")
[Category:中国国家重点风景名胜区](../Category/中国国家重点风景名胜区.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")
[Category:中国山峰](../Category/中国山峰.md "wikilink")

1.  [1](http://news.sina.com.cn/c/2007-05-09/112712946888.shtml)