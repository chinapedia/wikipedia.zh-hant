在[數學裡](../Page/數學.md "wikilink")，若兩個[集合沒有共同的](../Page/集合.md "wikilink")[元素](../Page/元素_\(數學\).md "wikilink")，稱為**不交**（disjoint）。例如\(\{1, 2, 3\}\)和\(\{4, 5, 6\}\)為**不交集**（disjoint
sets）。

[TwoDisjointSets.png](https://zh.wikipedia.org/wiki/File:TwoDisjointSets.png "fig:TwoDisjointSets.png")

## 解釋

從定義說，兩個集合\(A\)和\(B\)為不交，若其[交集為](../Page/交集.md "wikilink")[空集](../Page/空集.md "wikilink")，即\[1\]

\[A\cap B = \varnothing\]

此一定義可推廣至[集族上](../Page/集合族.md "wikilink")。若然一個集族裡的任意兩個相異集合均為不交，則稱之為**兩兩不交**。

形式上，設\(I\)為[索引集](../Page/索引集.md "wikilink")，且對\(I\)內的任一元素\(i\)，設\(A_i\)為一集合。然後\(\{A_i : i \in I\}\)為兩兩不交，當對任何於\(I\)內的\(i\)和\(j\)且\(i \ne j\)，有

\[A_i \cap A_j = \varnothing\]

舉例來說，\(\{\{1\}, \{2\}, \{3\}, \dots\}\}\)便為兩兩不交。若\(\{A_i\}\)為兩兩不交，則\(\{A_i\}\)中各集合的交集為空集：

\[\bigcap_{i \in I} A_i = \varnothing\]

相反則不必為真：\(\{\{1, 2\}, \{2, 3\}, \{3, 1\}\}\)內各集合的交集為空集，但非兩兩不交。事實上，其內的集合甚至沒有兩個是不交集。

[集合划分](../Page/集合划分.md "wikilink")\(X\)是由一群兩兩不交的非空集合\(\{A_i : i \in I\}\)組成的集族。

\[\bigcup_{i \in I} A_i = X\]

## 参考文献

## 另見

  - [幾乎不交集](../Page/幾乎不交集.md "wikilink")
  - [不交並](../Page/不交並.md "wikilink")
  - [不交集資料結構](../Page/並查集.md "wikilink")

[B](../Category/集合論基本概念.md "wikilink")
[B](../Category/集合族.md "wikilink")

1.  .