[Yaoi_Books_by_miyagawa.jpg](https://zh.wikipedia.org/wiki/File:Yaoi_Books_by_miyagawa.jpg "fig:Yaoi_Books_by_miyagawa.jpg")[紀伊國屋的Yaoi書櫃一角](../Page/紀伊國屋.md "wikilink")\]\]
**Yaoi**（****）是以[男男色情為題材的](../Page/男男色情.md "wikilink")[漫畫與](../Page/漫畫.md "wikilink")[小說的俗稱](../Page/小說.md "wikilink")，通常指有描寫性愛的[色情作品](../Page/色情.md "wikilink")、[二次創作作品](../Page/二次創作.md "wikilink")。用中文表示一般写作。

## 概要

[JackXArik.png](https://zh.wikipedia.org/wiki/File:JackXArik.png "fig:JackXArik.png")
使用[漫畫](../Page/漫畫.md "wikilink")、[動畫等原作的男性](../Page/動畫.md "wikilink")[角色](../Page/角色.md "wikilink")，描寫男性之間[性愛關係的](../Page/性愛.md "wikilink")[二次創作作品](../Page/二次創作.md "wikilink")；以男性同性愛情為題材的女性向漫畫或[小說](../Page/小說.md "wikilink")；或者是其性愛關係，稱之為「Yaoi」。有時候即使沒有男性之間的性愛關係也會稱作「Yaoi」（此情況時和「[BL](../Page/BL.md "wikilink")」同義）。

的語源一般認為是因為作品很多時都只是由性描寫構成，從「」（沒有高潮、沒有結尾、沒有意義）轉變而來。亦有「801」（日語轉音）與「」（蔬菜）（「」常掩飾寫成「」，在[Fanroad雜誌上讀者興起在其中填入](../Page/Fanroad.md "wikilink")「」而廣為流傳）等的黑話。類似概念有「[BL](../Page/BL.md "wikilink")」的詞語，但在Yaoi領域當中多特別指一次創作。在歐美等地Yaoi也稱為「Slash」。Yaoi愛好者的女性被稱為[腐女](../Page/腐女.md "wikilink")（成人女性也稱為貴腐人或污超腐人）、男性愛好者被稱為[腐男](../Page/腐男.md "wikilink")（腐兄）。這些稱呼在愛好者之間也作自貶的用途。現無正式譯名。

Yaoi作品主要採行[漫畫](../Page/漫畫.md "wikilink")、[繪畫](../Page/繪畫.md "wikilink")、[小說等形態](../Page/小說.md "wikilink")。由於角色喜好與人際關係的差異，一部作品會產生出五花八門的[配對組合](../Page/配對_\(同人\).md "wikilink")。配對的表示方式可參考[配對和](../Page/配對_\(同人\).md "wikilink")[BL條目](../Page/BL.md "wikilink")。

雖然Yaoi大部分描寫的是[虛構角色之間的戀愛](../Page/虛構角色.md "wikilink")，不過也有人以真實人物（男性[偶像](../Page/偶像.md "wikilink")、[運動員](../Page/運動員.md "wikilink")、[音樂家](../Page/音樂家.md "wikilink")、[搞笑藝人等](../Page/搞笑藝人.md "wikilink")）作為角色來使用，通稱為「真人同人」。但因為被當事人與關係者見到可能會引發糾紛，這種作法通常是敬謝不敏。

此外，Yaoi也被批評是揶揄與侮辱[Gay與同性戀](../Page/男同性戀.md "wikilink")，侵害到[LGBT的](../Page/LGBT.md "wikilink")[人權](../Page/人權.md "wikilink")。另外採用實際人物的虛構性愛關係，以及包含強暴等以未成年者為對象的性描寫，在法律和道德方面上可能產生問題。在尊重[言論自由的同時](../Page/言論自由.md "wikilink")，[性騷擾](../Page/性騷擾.md "wikilink")、[差别用语](../Page/差别用语.md "wikilink")、[仇恨言論等問題也被提出作為抨擊的議題](../Page/仇恨言論.md "wikilink")。

## 由來

一般認為Yaoi的[語源出自女性向的動漫迷](../Page/語源學.md "wikilink")[同人誌](../Page/同人誌.md "wikilink")。受到1970年代登場，描繪少年同志戀愛的少女漫畫家[花之24年組](../Page/24年組.md "wikilink")（成員有[竹宮惠子](../Page/竹宮惠子.md "wikilink")、[萩尾望都](../Page/萩尾望都.md "wikilink")、[大島弓子](../Page/大島弓子.md "wikilink")、[山岸-{凉}-子等人](../Page/山岸凉子.md "wikilink")），以及《[宇宙戰艦大和號](../Page/宇宙戰艦大和號.md "wikilink")》等動畫風潮的影響，在少女之間也掀起了同人誌的風潮。這類作品多半只有四頁上下，內容粗製濫造寫完就扔，因此人們叫它「沒有高潮」、「沒有結尾」、「沒有意義」\[1\]。又因為內容涉及[少年愛](../Page/少年愛.md "wikilink")（當時稱之為「ホモ落ち」），「Yaoi」一詞成為1970年代少年愛的進化。

在此同時，也不得不談到早期建立思想體系的提倡者，小說家[森茉莉和](../Page/森茉莉.md "wikilink")[栗本薰](../Page/栗本薰.md "wikilink")（[中島梓](../Page/中島梓.md "wikilink")）等人。

在「Yaoi」這一名稱還沒登場以前，這類作品通常被冠上「美少年作品」、「同志漫畫」、「耽美」之類的稱呼。自從少年愛雜誌代表刊物《[JUNE](../Page/JUNE.md "wikilink")》發行以來，也曾經有過被叫做「JUNE系」的時期。不論是哪種稱呼，其內容都捨棄了現實男同性愛性慾露骨的一面，成為幻想下的產物。

由1979年12月20日[坂田靖子主編發行的漫畫同人社刊](../Page/坂田靖子.md "wikilink")《らっぽり》上的「Yaoi特集」開始，\[2\]，以坂田靖子和波津彬子她們提出的三大特徵，也就是前述沒有高潮、沒有結尾、沒有意義，描繪男同志的情感互動。然而作品的內容並不火辣刺激，主角兩人在相擁之後鏡頭一轉，就切換到隔天早上的[香煙畫面](../Page/菸草.md "wikilink")，諸如此類的橋段，以今日眼光來看僅屬於軟調情色之作。

最先被稱為「Yaoi」的動漫同人誌類別是《[六神合體](../Page/六神合體.md "wikilink")》和《[足球小將翼](../Page/足球小將翼.md "wikilink")》。「Yaoi」一詞主要用在動漫同人誌上，原創或小說同人誌的稱呼通常以「JUNE」為主，但不知從何時開始亦可使用「Yaoi」。原本「Yaoi」是屬於委婉的圈內用語，然而近年來也有人懷疑該詞的語感趨於露骨，而改以「Boy's
Love」或「BL」稱之。

近年來，人們意識到Yaoi成為商業戰略的一環，動畫主題歌在[Oricon排行榜上相繼名列前茅](../Page/Oricon.md "wikilink")，市場也正在擴大新的客層。隨著「[腐女子](../Page/腐女子.md "wikilink")」的存在為人知曉，即使是在圈外，BL及Yaoi等詞彙也廣為人知。不過若是翻閱[少女漫畫中涉及BL及Yaoi題材的作品](../Page/少女漫畫.md "wikilink")，可以發現就算作者本人沒有惡意，Yaoi愛好者在漫畫裡也成了被指責的對象（《[鹹蛋丫頭](../Page/鹹蛋丫頭.md "wikilink")》和《[紳士同盟](../Page/紳士同盟.md "wikilink")》中都出現了當時的圈內用語）。
[Lesson_1_Private_Tutor.jpg](https://zh.wikipedia.org/wiki/File:Lesson_1_Private_Tutor.jpg "fig:Lesson_1_Private_Tutor.jpg")

## Yaoi的爭論

### 為什麼會演變成Yaoi？

一般的Yaoi愛好者會抱持如下理由:

  - 對角色的愛。
  - 無法容忍喜歡的角色和（自己以外的）女性相戀。
  - 不想讓喜歡角色之外的第三者介入故事情節中。
  - 不論Yaoi的起源為何，現在就只單純將它視為小說（[故事類型](../Page/故事類型.md "wikilink")）的一種。

另一方面，作家和專家則持有下述論點：

  - 對女性特質的厭惡（[厭女症](../Page/厭女症.md "wikilink")）：針對以往的少年愛作品，有不少人指出其發生背景在於青春期少女對（既有的）女性特質懷抱的自我厭惡，想成為男性愛上另一個男人。比如[唐澤俊一](../Page/唐澤俊一.md "wikilink")，就充分解釋Yaoi和BL是女性對自己女性特徵厭惡而產生的結果。本身也創作Yaoi小說的[中島梓](../Page/中島梓.md "wikilink")，在《タナトスの子供たち》當中亦做了同樣的說明。而社會學家[上野千鶴子和榮格派心理學權威](../Page/上野千鶴子.md "wikilink")[河合隼雄等人也論及同樣的宗旨](../Page/河合隼雄.md "wikilink")，是至今以來最普遍的說法。總而言之，這樣的嫌惡感是少女本身內化了社會貶抑女性的後果。
  - [異性戀的安全擬仿](../Page/異性戀.md "wikilink")：[藤本由香里認為](../Page/藤本由香里.md "wikilink")，現在的Yaoi仍然是水火不容的男女關係之中，一塊安心體驗情感的領域。透過這個領域，女性在情慾上得以從被凝視，居於被動的一方中解放。從性愛中被動的立場求得解脫，亦可從光臨[Gay吧的女性客人上看到其他的例子](../Page/Gay吧.md "wikilink")。
  - [性別認同障礙的可能性](../Page/性別認同障礙.md "wikilink")：[神原史保美在其著作](../Page/神原史保美.md "wikilink")《やおい幻論》中提倡的理論是，喜歡上Yaoi的作者和讀者既有FtM（性別認同障礙的一種，指一個人生理上是女性，精神上卻是男性的狀態），同時又是[Gay](../Page/Gay.md "wikilink")（同性戀或年輕時的際遇型雙性戀），而她亦認為自己有可能是FtM-Gay。換言之，或許喜歡Yaoi的女性在精神上是男性兼男同志，才有辦法愛上男性（也可以說由於雙重性逆轉，乍看之下會誤以為是普通的異性戀）。不過就算可以愛上男性，也無法以「女性身分」為對方所愛。由於精神上是男性，想以「男性身份」愛人和被愛，但這一點卻難以在現實世界中實現，因此她們就透過幻想的方式，將這份希望投射在Yaoi作品上。

### Yaoi與男性同性愛

在傳統上Yaoi作品經常可看到下列特徵：

1.  即使登場的男性角色陷入同志戀情，也會堅稱自己是異性戀者。
2.  登場人物在性愛以外也會分攻受，扮演著擬似男女的固定性別角色。
3.  總是採用肛交。
4.  強暴情節不少。

在Yaoi作品出現以來，經過很長一段時間。雖現在社會上獲得某程度的容許，創作者和讀者也多半能享受這些「虛構作品」的樂趣，但對於明白男同志實際面貌的人而言會覺得很怪異，這也是不爭的事實。現實中的男同志對Yaoi作品和Yaoi愛好者感到彆扭和排斥。這類作品中還有不合原本男同志實況的奇特描述，像是典型的[Yaoi穴或](../Page/Yaoi穴.md "wikilink")[Yaoi汁等等](../Page/Yaoi汁.md "wikilink")。

[溝口彰子指出Yaoi作品很明顯地對男同志認同避而不談](../Page/溝口彰子.md "wikilink")，男同志的[配對不過是把男女之間的](../Page/配對_\(同人\).md "wikilink")[浪漫愛置換為男性間的關係](../Page/浪漫愛.md "wikilink")，劇情充斥著強烈的[異性戀霸權和](../Page/異性戀霸權.md "wikilink")[恐同症](../Page/同性戀恐懼症.md "wikilink")\[3\]。不過Yaoi的型態也開始多樣化，近年的作品不一定符合上列的弊端。溝口本人也在2007年表示，沒有說出「我不是同志」這種台詞的主角，同性情侶其中一方，以及雙方有同志自覺的角色也正在增加當中\[4\]，而Yaoi作品裡的恐同要素也逐漸減少。

近年的Yaoi作品逐漸稀釋以往背德的印象，和男性向作品一樣，有加強[萌要素的消費傾向](../Page/萌.md "wikilink")。然則相較於男性向作品明顯的[萌屬性組合](../Page/萌.md "wikilink")，Yaoi和BL作品則更執著於[配對的要素](../Page/配對_\(同人\).md "wikilink")。在此同時，這類作品也產生了貼近同志真實面貌，描寫現實男性的走向趨勢。

### 議論Yaoi的文獻

  - [中島梓](../Page/栗本薰.md "wikilink") (栗本薫) 『コミュニケーション不全症候群』、『タナトスの子供たち ―
    過剰適応の生態学』 1998/10／文庫版 2005/05（筑摩書房）
  - [上野千鶴子](../Page/上野千鶴子.md "wikilink")　『発情装置』1998/01（[筑摩書房](../Page/筑摩書房.md "wikilink")）
  - 榊原史保美 『やおい幻論 ―「やおい」から見えたもの』
    1998/06（[夏目書房](../Page/夏目書房.md "wikilink")）
  - 溝口彰子「ホモフォビックなホモ、愛ゆえのレイプ、そしてクィアなレズビアン－最近のやおいテキストを分析する」『クィア・ジャパン』Vol.2　2000/04（[勁草書房](../Page/勁草書房.md "wikilink")）
  - 西村マリ 『アニパロとヤオイ（オタク学叢書）』 2001/12（[太田出版](../Page/太田出版.md "wikilink")）
  - 永久保陽子 『やおい小説論 ― 女性のための[エロス表現](../Page/エロス.md "wikilink")』
    2005/03（[専修大学出版局](../Page/専修大学.md "wikilink")）
  - 水間碧　『隠喩としての少年愛―女性の少年愛嗜好という現象』
    2005/02（[創元社](../Page/創元社.md "wikilink")）
  - [熊田一雄](../Page/熊田一雄.md "wikilink")　『“男らしさ”という病？ーポップ・カルチャーの新・男性学』2005/09（[風媒社](../Page/風媒社.md "wikilink")）

## 註釋

<references/>

## 相關項目

  - [同人誌](../Page/同人誌.md "wikilink")
  - [Bara](../Page/Bara.md "wikilink")
  - [Comic Market](../Page/Comic_Market.md "wikilink")
  - [正太控](../Page/正太控.md "wikilink")
  - [GL](../Page/百合_\(同人\).md "wikilink")
  - [BL](../Page/BL.md "wikilink")
  - [同人](../Page/同人.md "wikilink")
  - [同人女](../Page/同人女.md "wikilink")
  - [腐女](../Page/腐女.md "wikilink")
  - [女體化](../Page/女體化.md "wikilink")
  - [日本動漫迷使用術語列表](../Page/日本動漫迷使用術語列表.md "wikilink")

[\*](../Category/BL.md "wikilink")
[Category:男色](../Category/男色.md "wikilink")

1.
2.  該特集《らっぽり：やおい特集号》由[坂田靖子](../Page/坂田靖子.md "wikilink")、[波津彬子](../Page/波津彬子.md "wikilink")、[花郁悠紀子](../Page/花郁悠紀子.md "wikilink")、[橋本多佳子](../Page/橋本多佳子.md "wikilink")、[磨留美樹子執筆](../Page/磨留美樹子.md "wikilink")。其內容亦再次收錄於《[小說
    JUNE](../Page/JUNE.md "wikilink")》的129期（2001年3月1日發行）。
3.
4.