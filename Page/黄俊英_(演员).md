**黄俊英**（），[中國大陸](../Page/中國大陸.md "wikilink")[一級演員](../Page/一級演員.md "wikilink")，原籍[广东](../Page/广东.md "wikilink")[罗定](../Page/罗定.md "wikilink")，著名曲艺表演家，在[广州表演工作直至](../Page/广州.md "wikilink")[退休](../Page/退休.md "wikilink")，至今仍不时参与[电视剧与](../Page/电视剧.md "wikilink")[舞台表演](../Page/舞台.md "wikilink")。

## 生平

  - 16岁开始从事表演，并师从著名[粤剧表演艺术家](../Page/粤剧.md "wikilink")[罗品超学习粤剧表演艺术](../Page/罗品超.md "wikilink")6年；1958年黄俊英到[北京参加中国第一届全国曲艺汇演](../Page/北京.md "wikilink")，并接触到北方的相声艺术，因被相声艺术深深吸引便立志把北方的相声南粤化。1983年5月，黄俊英偕同[杨达](../Page/杨达_\(广州\).md "wikilink")、[林运洪和](../Page/林运洪.md "wikilink")[崔凌霄等牵头成立了](../Page/崔凌霄.md "wikilink")“广州相声艺术团”，黄俊英任团长。

## 作品

《冒牌医生》、《[语言趣谈](http://bulo.hjenglish.com/group/topic/248598/)》《关公大战方世玉》、《[三六九查户口](https://web.archive.org/web/20070927203543/http://music4.163888.net/209448864/2005/10/02/10/MUSIC/28835245305920.mp3)》、《[盲公问米](https://web.archive.org/web/20070927203604/http://music1.163888.net/209448864/2005/10/07/08/MUSIC/28835245300860.mp3)》、《呆佬贺中秋》、《新潮打金枝》、《[肥仔米](https://web.archive.org/web/20070927203611/http://music4.163888.net/209448864/2005/12/12/23/MUSIC/34059791204390.wma)》、《[一对一](https://web.archive.org/web/20070927203640/http://music2.163888.net/209448864/2005/09/03/14/MUSIC/27257480033650.mp3)》、《老张师傅》、《哈，手机！》、《省港澳大比拼》、《无牌医生》、《真假难分》、《广州话趣》

## 資料來源

  - [香港](../Page/香港.md "wikilink")[亞洲電視新聞報導](../Page/亞洲電視.md "wikilink")，2007年5月20日

## 外部链接

[黄俊英](http://www.020vips.com/mingrentang/1039.html) 广州名人网

[Category:羅定人](../Category/羅定人.md "wikilink")
[Category:一级演员](../Category/一级演员.md "wikilink")
[Category:相声演员](../Category/相声演员.md "wikilink")
[Jun俊英](../Category/黄姓.md "wikilink")