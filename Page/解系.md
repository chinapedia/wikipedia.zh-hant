**解系**\[1\]，（），字**少連**，[濟南](../Page/濟南.md "wikilink")[著縣人](../Page/著縣.md "wikilink")。[西晉時期官員](../Page/西晉.md "wikilink")，為人清身潔己，亦不奉承當時極得寵的權貴[荀勖等](../Page/荀勖.md "wikilink")，於是當時很有聲譽。但最終在八王之亂期間被趙王司馬倫殺害。

## 生平

解系在西晉建立時獲封**梁鄒侯**。解系先被辟為公府[掾](../Page/掾.md "wikilink")，及後先後被任命為中書[黃門侍郎](../Page/黃門侍郎.md "wikilink")，散騎常侍，[豫州](../Page/豫州.md "wikilink")[刺史](../Page/刺史.md "wikilink")，[尚書](../Page/尚書.md "wikilink")。後出任[雍州刺史](../Page/雍州.md "wikilink")、揚烈將軍、西戎校尉、假節。

[元康元年](../Page/元康.md "wikilink")（291年），趙王[司馬倫初鎮](../Page/司馬倫.md "wikilink")[關中](../Page/關中.md "wikilink")，對[氐](../Page/氐.md "wikilink")[羌兩族刑賞不公](../Page/羌.md "wikilink")，管理不善之下激起其反抗，並於元康六年（296年）爆發了[馬蘭羌和](../Page/馬蘭羌.md "wikilink")[盧水胡的叛亂](../Page/盧水胡.md "wikilink")。當時司馬倫和解系爭掌軍事，並各自表奏。[馮翊太守](../Page/馮翊.md "wikilink")[歐陽建亦上表揭發司馬倫的罪行](../Page/歐陽建.md "wikilink")，朝廷於是召回司馬倫，改以梁王[司馬肜為征西將軍](../Page/司馬肜.md "wikilink")，與解系一同伐叛胡。同時解系亦主張殺死[孫秀安撫羌氐族人](../Page/孫秀_\(西晉\).md "wikilink")，初時司馬肜亦同意，但孫秀因友人[辛冉勸阻才得以免死](../Page/辛冉.md "wikilink")。但解系及後被胡人擊敗，引發[秦州和雍州的羌氐胡人叛變](../Page/秦州.md "wikilink")，並立氐人[齊萬年為帝](../Page/齊萬年.md "wikilink")。在后来对齐万年的战斗中，解系与征西长史[卢播不救战至弓箭已用完的建威将军](../Page/卢播.md "wikilink")[周处](../Page/周处.md "wikilink")，周处阵亡。解系連番兵敗後更遭司馬倫和孫秀屢次向朝廷進讒言，最終被免官。

[永康元年](../Page/永康_\(西晉\).md "wikilink")（300年），趙王司馬倫推翻了以賈后[賈南風為首的集團](../Page/賈南風.md "wikilink")，並且專掌朝政。司馬倫更意圖篡位，為了先除去朝中有名望的大臣和一報宿怨，於是殺害解系、[張華](../Page/張華.md "wikilink")、[裴頠等人](../Page/裴頠.md "wikilink")。期間司馬肜曾意圖營救，被司馬倫堅決地拒絕，解系和弟弟解結以及其家眷都一同被殺害。

次年齊王[司馬冏等起兵討伐篡位稱帝的司馬倫](../Page/司馬冏.md "wikilink")，並以裴頠和解系為冤首。司馬冏獲勝後即於次年追贈光祿大夫給解系，並且為他改葬和追加弔祭。

## 家庭

### 父親

  - [解脩](../Page/解脩.md "wikilink")，曹魏[琅邪太守](../Page/琅邪.md "wikilink")、[梁州刺史](../Page/梁州.md "wikilink")，考績在當時是全國第一。

### 兄弟

  - [解結](../Page/解結.md "wikilink")，解系弟，官至[御史中丞](../Page/御史中丞.md "wikilink")，曾與兄長一同上請誅除孫秀，因而在後來與兄長一同被殺。
  - [解育](../Page/解育.md "wikilink")，解結弟，名氣比不上兩位兄長，曾任尚書郎、弘農太守等職，亦與兩位兄長一同被殺。

## 參考資料

  - 《晉書·解系傳》（房玄齡 等　1974：1631-1632）
  - 《資治通鑑·卷八十二至八十四》

## 參考

[Category:晉朝政治人物](../Category/晉朝政治人物.md "wikilink")
[Category:晉朝軍事人物](../Category/晉朝軍事人物.md "wikilink")
[Category:晉朝被處決者](../Category/晉朝被處決者.md "wikilink")
[Category:濟南人](../Category/濟南人.md "wikilink")
[X系](../Category/解姓.md "wikilink")

1.  解，音懈，粵音haai6，國音xie4。