**糖尿病视网膜病变**（英語：Diabetic
retinopathy）是[糖尿病的](../Page/糖尿病.md "wikilink")[并发症](../Page/并发症.md "wikilink")。长期的高血糖环境会损伤[视网膜血管的内皮](../Page/视网膜.md "wikilink")，引起一系列的眼底病变，如微血管瘤、硬性渗出、棉絮斑、新生血管、玻璃体增殖甚至视网膜脱离。一般糖尿病出现十年以上的病人开始出现眼底病变，但如果血糖控制差，或者是胰岛素依赖型糖尿病的患者则可能更早出现眼底病变，故糖尿病患者需要定期到[眼科检查眼底](../Page/眼科学.md "wikilink")。

## 分级

[Eye_disease_simulation,_normal_vision.jpg](https://zh.wikipedia.org/wiki/File:Eye_disease_simulation,_normal_vision.jpg "fig:Eye_disease_simulation,_normal_vision.jpg")
[Eye_disease_simulation,_diabetic_retinopathy.jpg](https://zh.wikipedia.org/wiki/File:Eye_disease_simulation,_diabetic_retinopathy.jpg "fig:Eye_disease_simulation,_diabetic_retinopathy.jpg")
[Retinal_branch_occlusion_ratkaj.jpg](https://zh.wikipedia.org/wiki/File:Retinal_branch_occlusion_ratkaj.jpg "fig:Retinal_branch_occlusion_ratkaj.jpg")

### 分级

  - NPDR：非增殖性糖尿病视网膜病变
  - PDR：增殖性糖尿病视网膜病变

### 单纯性

  - I级：有[微小动脉瘤](../Page/微小动脉瘤.md "wikilink")，小出血点。
      - （+）：容易数；（++）：太多，数不过来
  - Ⅱ级：[硬性渗出](../Page/硬性渗出.md "wikilink")，出血斑
      - （+）：容易数；（++）：太多，数不过来
  - Ⅲ级：[软性渗出](../Page/软性渗出.md "wikilink")，出血斑
      - （+）：容易数；（++）：太多，数不过来

### 增殖性

  - Ⅳ级：新生血管，[玻璃体出血](../Page/玻璃体出血.md "wikilink")
  - Ⅴ级：新生血管，纤维增生
  - Ⅵ级：新生血管，纤维增生，[视网膜脱离](../Page/视网膜脱离.md "wikilink")

## 延伸閱讀

  -
## 外部連結

  - [Diabetic
    Retinopathy](https://web.archive.org/web/20140512194443/http://www.nei.nih.gov/health/diabetic/retinopathy.asp)
    Resource Guide from the National Eye Institute (NEI).
  - [National Diabetes Information
    Clearinghouse](http://diabetes.niddk.nih.gov/)
  - [NHS Diabetic Eye Screening
    Programme](http://diabeticeye.screening.nhs.uk/)
  - [English National Screening Programme for Diabetic
    Retinopathy](http://www.retinalscreening.nhs.uk/)

{{-}}

[Category:糖尿病](../Category/糖尿病.md "wikilink")
[Category:視覺障礙](../Category/視覺障礙.md "wikilink")
[Category:视网膜疾病](../Category/视网膜疾病.md "wikilink")