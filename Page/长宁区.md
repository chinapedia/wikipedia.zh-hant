**长宁区**是[中國](../Page/中國.md "wikilink")[上海市的一个](../Page/上海市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，位于上海中心城区西部，东连[静安区](../Page/静安区.md "wikilink")，西南邻[闵行区](../Page/闵行区.md "wikilink")，东南接[徐汇区](../Page/徐汇区.md "wikilink")，北与[普陀区以](../Page/普陀区.md "wikilink")[吴淞江](../Page/吴淞江.md "wikilink")（[苏州河](../Page/苏州河.md "wikilink")）为界，区域[面积](../Page/面积.md "wikilink")37.19[平方公里](../Page/平方公里.md "wikilink")，2010年第六次全国人口普查结果户籍人口623,041人，常住人口690,571人。邮政编码200050。区人民政府驻[长宁路](../Page/长宁路.md "wikilink")。

长宁区是上海市的一个交通枢纽。[上海轨道交通的](../Page/上海轨道交通.md "wikilink")[2](../Page/上海轨道交通2号线.md "wikilink")、[3](../Page/上海轨道交通3号线.md "wikilink")/[4号线的](../Page/上海轨道交通4号线.md "wikilink")[换乘站](../Page/换乘站.md "wikilink")[中山公园站就是在长宁区内](../Page/中山公园站.md "wikilink")；同时，区内有四条高架路：内环高架路\\延安高架路\\中环路\\外环线，而延安高架路的起发点，上海两大机场之一[虹桥机场再扩建之后也有一半](../Page/虹桥机场.md "wikilink")（一号航站楼）是位于长宁区内。区内主要的街道包括了[长宁路](../Page/长宁路.md "wikilink")、[虹桥路](../Page/虹桥路.md "wikilink")、[江苏路等](../Page/江苏路_\(上海\).md "wikilink")。

位于长宁的[虹桥开发区是上海最早的和最成熟的现代化商务区之一](../Page/虹桥开发区.md "wikilink")。最近几年，长宁区的发展迅速，区内建成了很多新的商业大厦，如多媒体生活广场、龙之梦购物中心。[中国联通上海分公司的总部](../Page/中国联通.md "wikilink")——联通大厦也处于长宁区。另外区内也有[上海动物园和](../Page/上海动物园.md "wikilink")[上海中山公园等休闲设施](../Page/上海中山公园.md "wikilink")。

## 名称由来

“长宁”的地名得名于“长宁路”：[清](../Page/清.md "wikilink")[光绪二十七年](../Page/光绪.md "wikilink")（1901年）[公共租界在](../Page/公共租界.md "wikilink")[曹家渡至北新泾间越界筑路](../Page/曹家渡.md "wikilink")，以[英国驻沪总领事之名命名为](../Page/英国驻沪总领事.md "wikilink")“白利南路”。1943年以[四川省县名改称](../Page/四川省.md "wikilink")“长宁路”。1945年置区时，取境内路名命名为“长宁区”。

## 行政区划

长宁区下辖9个街道、1个镇：\[1\]

。

## 教育

### 高等教育

长宁区的教育资源在上海属比较丰富，[交通大学](../Page/上海交通大学.md "wikilink")、[东华大学](../Page/东华大学.md "wikilink")、[华东政法大学等高等学府都有校区位于本区](../Page/华东政法大学.md "wikilink")。

  - [交通大学法华校区](../Page/上海交通大学法华校区.md "wikilink")（[交大安泰经济与管理学院研究生院所在地](../Page/上海交通大学安泰经济与管理学院.md "wikilink")）
  - [东华大学长宁校区](../Page/东华大学.md "wikilink")
  - [华东政法大学长宁校区](../Page/华东政法大学.md "wikilink")

### 基础教育

  - [上海市第三女子中学](../Page/上海市第三女子中学.md "wikilink")
  - [上海市建青实验学校](../Page/上海市建青实验学校.md "wikilink")
  - [上海市虹桥中学](../Page/上海市虹桥中学.md "wikilink")
  - [上海市延安中学](../Page/上海市延安中学.md "wikilink")
  - [華東政法大學附屬中學](../Page/華東政法大學附屬中學.md "wikilink")
  - [上海市復旦中學](../Page/上海市復旦中學.md "wikilink")
  - [上海市天山中學](../Page/上海市天山中學.md "wikilink")
  - [上海市娄山中學](../Page/上海市娄山中學.md "wikilink")
  - [上海市泸定中學](../Page/上海市泸定中學.md "wikilink")
  - [上海市新古北中學](../Page/上海市新古北中學.md "wikilink")（与娄山中学合并）

## 参考文献

## 外部链接

  - [上海市长宁区人民政府](http://www.changning.sh.cn/)
  - [上海市长宁区政协](http://cnzx.changning.sh.cn/)

{{-}}

[长宁区](../Category/长宁区.md "wikilink")
[Category:上海市辖区](../Category/上海市辖区.md "wikilink")
[3](../Category/全国文明城市.md "wikilink")
[沪](../Category/国家卫生城市.md "wikilink")

1.