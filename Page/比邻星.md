**比鄰星**或**毗鄰星**（Proxima
Centauri）位於[半人馬座](../Page/半人馬座.md "wikilink")，是[半人馬座α](../Page/半人馬座α.md "wikilink")[三合星的第三顆星](../Page/三合星.md "wikilink")，依[拜耳命名法也稱為](../Page/拜耳命名法.md "wikilink")**半人馬座α星C**，是距離[太陽最近的一顆](../Page/太陽.md "wikilink")[恆星](../Page/恆星.md "wikilink")（4.22[光年](../Page/光年.md "wikilink")），恆星分類屬於[紅矮星](../Page/紅矮星.md "wikilink")。

它是由天文學家[羅伯特·因尼斯于](../Page/羅伯特·因尼斯.md "wikilink")1915年在[南非發現的](../Page/南非.md "wikilink")，當時他是擔任[約翰尼斯堡](../Page/約翰尼斯堡.md "wikilink")[聯合天文台的主管](../Page/聯合天文台.md "wikilink")。

[Centaurus_constellation_map.png](https://zh.wikipedia.org/wiki/File:Centaurus_constellation_map.png "fig:Centaurus_constellation_map.png")

## 亮度

通常紅矮星的[亮度都很弱](../Page/亮度.md "wikilink")，以肉眼觀測是看不見的，比鄰星也不例外。它的[視星等是](../Page/視星等.md "wikilink")11等，[絕對星等是非常弱的](../Page/絕對星等.md "wikilink")15.5等。如果從半人馬座α[三合星的其他兩個星觀測](../Page/三合星.md "wikilink")，將是4.5等星。

## 距離、直徑與質量

[Alpha_Centauri_relative_sizes.svg](https://zh.wikipedia.org/wiki/File:Alpha_Centauri_relative_sizes.svg "fig:Alpha_Centauri_relative_sizes.svg")
由歐洲天文衛星Hipparcos量測到的[視差](../Page/視差.md "wikilink")772.33 ± 2.42
[毫角秒推算](../Page/角秒.md "wikilink")，比鄰星離地球大約是4.22光年遠，或者地球到太陽距離的270,000倍（AU，天文單位）。離它最近的鄰居依序為：半人馬座α三合星的其他兩顆星（0.21光年）、[太陽](../Page/太陽.md "wikilink")（4.22光年）和[巴納德星](../Page/巴納德星.md "wikilink")（6.55光年）。從地球觀測，比鄰星離半人馬座α星A視角約2°，或相當於[滿月直徑的](../Page/滿月.md "wikilink")4倍。由於比鄰星離半人馬座α星AB双星與太陽距離的比率僅僅是20分之1，天文學家猜測它可能是以500,000至2,000,000年或更長的週期在繞半人馬座α星AB双星的一個軌道上運轉。因此，比鄰星也稱為**半人馬座α星C**。

用[歐洲南天天文台](../Page/歐洲南天天文台.md "wikilink")（ESO）位在智利的[甚大望遠鏡在](../Page/甚大望遠鏡.md "wikilink")2002年以光學干涉測量得到比鄰星的[角直徑為](../Page/角直徑.md "wikilink")1.02
±
0.08毫角秒。由已知它的距離，推算實際直徑大約是太陽的7分之1，或者[木星的](../Page/木星.md "wikilink")1.5倍。它的質量約爲太陽的8分之1，或者木星的150倍。

## 形成年代與壽命

[Proxima_Centauri_2MASS_Atlas.jpg](https://zh.wikipedia.org/wiki/File:Proxima_Centauri_2MASS_Atlas.jpg "fig:Proxima_Centauri_2MASS_Atlas.jpg")
它的形成年代，與半人馬座α星A和B雙星相同，約48.5億年前，比太陽的46億年略早些。但由於大小僅比產生[氫核融合反應所需的](../Page/氫.md "wikilink")[恆星臨界質量稍大](../Page/恆星臨界質量.md "wikilink")，[融合反應的速率很慢且不穩定](../Page/融合反應.md "wikilink")，因此天文學家推算它的壽命可達數千億年以上。

## 表面活動觀測

比鄰星有很活躍的[色球層活動](../Page/色球層.md "wikilink")，在X-光波段可觀測到它色球層的噴發，是屬於典型的[耀星](../Page/耀星.md "wikilink")（[變星命名](../Page/變星命名.md "wikilink")：半人馬座V645）；在[紫外線波長觀測它的色球層的變化](../Page/紫外線.md "wikilink")，得知它的自轉週期大約31天。由90年代[哈伯太空望遠鏡所量測的資料顯示](../Page/哈伯太空望遠鏡.md "wikilink")，比鄰星應存在有一顆尚未觀測到的暗淡伴星。2016年8月24日[歐洲南天天文臺正式宣布發現了比鄰星的一顆行星](../Page/歐洲南天天文臺.md "wikilink")，命名為[比鄰星b](../Page/比鄰星b.md "wikilink")\[1\]，它位於比鄰星的[適居帶內](../Page/適居帶.md "wikilink")。即与恒星距离适中、可能有液态水存在的区域。但也有人提出，比邻星是一颗M型矮星，这类恒星比较容易爆发耀斑，破坏行星的大气。

2017年3月24日，位于智利的[阿塔卡马望远镜观测到比邻星亮度在](../Page/阿塔卡马大型毫米波／亚毫米波天线阵.md "wikilink")10秒内上升了1000倍，随后迅速回落。研究分析观测数据后发现，亮度上升集中发生在极短时间内，应该是一次比最强烈的[太阳耀斑还强](../Page/太阳耀斑.md "wikilink")10倍的恒星[耀斑](../Page/耀斑.md "wikilink")。行星“[比鄰星b](../Page/比鄰星b.md "wikilink")”在这次事件中受到的辐射，比通常太阳耀斑爆发时地球受到的辐射高出约4000倍。研究人员认为，比邻星b在它近50亿年的生涯中，可能曾多次遭受强烈耀斑袭击，即使表面曾有过液态水和大气，也早就被摧毁殆尽，环境不适合生命存在。\[2\]

## 相關条目

  - [比鄰星b](../Page/比鄰星b.md "wikilink")
  - [半人馬座](../Page/半人馬座.md "wikilink")
  - [半人馬座α星](../Page/半人馬座α星.md "wikilink")
  - [半人馬座α星A](../Page/半人馬座α星A.md "wikilink")
  - [半人馬座α星B](../Page/半人馬座α星B.md "wikilink")
  - [離太陽系最近的恆星一覽表](../Page/恆星距離列表.md "wikilink")
  - [潘朵拉星 (阿凡达)](../Page/潘朵拉星_\(阿凡达\).md "wikilink")

## 参考来源

## 外部連結

  -
  -
  -
  -
  -
  - Wikisky
    [image](http://www.wikisky.org/?ra=14.495264&de=-62.67948000000001&zoom=8&show_grid=1&show_constellation_lines=1&show_constellation_boundaries=1&show_const_names=0&show_galaxies=1&show_box=1&box_ra=14.495264&box_de=-62.67948&box_width=50&box_height=50&img_source=DSS2)
    of Proxima Centauri

  - [Proxima Centauri at Constellation
    Guide](http://www.constellation-guide.com/proxima-centauri/)

  - [比鄰星究竟是不是南門二系統的成員？](https://web.archive.org/web/20071205024057/http://www.tam.gov.tw/news/2006/200607/06072102.htm)

[Category:紅矮星](../Category/紅矮星.md "wikilink")
[Category:耀星](../Category/耀星.md "wikilink")
[Category:半人马座](../Category/半人马座.md "wikilink")
[P](../Category/格利泽和GJ天体.md "wikilink")

1.
2.  \[<http://www.sohu.com/a/225222361_119562>　[新华社北京](../Page/新华社.md "wikilink")2018年3月7日电
    《天文研究人员发现离我们最近的恒星——比邻星于2017年3月爆发了一次强烈耀斑，导致它的行星被高能射线“烘烤”了一遍》，美国卡内基科学学会研究人员的论文发表在《天体物理学杂志通讯》上\]