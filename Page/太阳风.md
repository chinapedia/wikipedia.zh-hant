[Voyagers_in_the_heliosheath_region.jpg](https://zh.wikipedia.org/wiki/File:Voyagers_in_the_heliosheath_region.jpg "fig:Voyagers_in_the_heliosheath_region.jpg")”附近的“航海家”号空间探测器\]\]

**太陽風**（）特指由[太阳上層](../Page/太阳.md "wikilink")[大氣射出的超高速](../Page/大氣.md "wikilink")[等离子体](../Page/等离子体.md "wikilink")（带电粒子）流。非出自太陽的类似带电粒子流也常稱爲“[恆星風](../Page/恆星風.md "wikilink")”。

[Solar_Wind_and_Earth's_magnetic_field.png](https://zh.wikipedia.org/wiki/File:Solar_Wind_and_Earth's_magnetic_field.png "fig:Solar_Wind_and_Earth's_magnetic_field.png")

在[太陽](../Page/太陽.md "wikilink")[日冕层的高温](../Page/日冕层.md "wikilink")（几百万[開氏度](../Page/開氏溫標.md "wikilink")）下，[氢](../Page/氢.md "wikilink")、[氦等](../Page/氦.md "wikilink")[原子已经被](../Page/原子.md "wikilink")[電離成帶正電的](../Page/電離.md "wikilink")[质子](../Page/质子.md "wikilink")、氦原子核和带负电的自由[电子等](../Page/电子.md "wikilink")。这些带电粒子运动速度极快，以致不断有带电的粒子挣脱太阳的[引力束缚](../Page/引力.md "wikilink")，射向太陽的外围，形成太陽風。
太陽風的速度一般在200-800km/s。
一般認為在[太阳极小期](../Page/太陽極小期.md "wikilink")，從太陽的磁場极地附近吹出的是高速太陽風，從太陽的磁场[赤道附近吹出的是低速太陽風](../Page/赤道.md "wikilink")。太陽的磁場的活动是會變化的，週期大約為11年。

太陽風一词是在1950年代被[尤金·派克提出](../Page/尤金·派克.md "wikilink")。但是直到1960年代才證實了它的存在。長期觀測發現，當太陽存在[冕洞時](../Page/冕洞.md "wikilink")，[地球附近就能觀測到高速的太陽風](../Page/地球.md "wikilink")。因此[天文学家認為高速太陽風的產生與冕洞有密切的關係](../Page/天文学家.md "wikilink")。太阳表面的磁场及等离子体活动对地球有很重要的影响。当太阳发生强烈的活动时，大量的带电粒子随着太阳风吹向地球的两极，就会在两极的[电离层引发美丽的](../Page/电离层.md "wikilink")[极光](../Page/极光.md "wikilink")。

## 性質

[Magnetosphere_rendition.jpg](https://zh.wikipedia.org/wiki/File:Magnetosphere_rendition.jpg "fig:Magnetosphere_rendition.jpg")

在太阳系中，太阳风的组成和太阳的日冕组成完全相同。73%的是[氢](../Page/氢.md "wikilink")，25%的是[氦](../Page/氦.md "wikilink")，还有其他一些[微蹤杂质](../Page/微蹤.md "wikilink")。可是目前還没有精確的測量結果。2004年Genesis的取樣分析失敗，因為在它返回地球時的緊急降落沒有打開降落傘。
在地球附近，太阳风速为200-889 km/s，平均值为450 km/s。大约800
kg/s的物质被以太阳风的形式从太阳逃逸。这同太阳光线的等价质量相比是很小的。如果把太阳光线的能量换算成质量，大约每秒钟太阳损失4.5Tg(\(4.5\times10^{9}\)
kg)的质量。

太阳风中的电子、质子的平均能量是1.5至10[电子伏特](../Page/电子伏特.md "wikilink")，太阳喷射出的粒子数目为1.3
每秒。因此太阳风的功率为1.95至13
电子伏特每秒，即3.1239至20.826瓦特。这与太阳的辐射通量3.846瓦特相比，太阳风的能量是太阳的电磁辐射的0.812至5.41，即十亿分之一量级。

因为太阳风是[等离子体](../Page/等离子体.md "wikilink")，所以太阳磁场被它承载。由于太阳的转动，太阳磁场被太阳风拉扯成螺线形状。通常太阳风的能量爆发来自于[太阳耀斑或其他被称为](../Page/太阳耀斑.md "wikilink")“太阳风暴”的气候现象。这些太阳活动可以被[太空探测器和](../Page/太空探测器.md "wikilink")[卫星测到](../Page/卫星.md "wikilink")，主要标志是强烈的[辐射](../Page/辐射.md "wikilink")。被[地球磁场俘获的太阳风粒子储存在](../Page/地磁場.md "wikilink")[范艾倫辐射带中](../Page/范艾倫辐射带.md "wikilink")，当这些粒子在[磁极附近与](../Page/地磁場#磁極.md "wikilink")[地球大气层作用引起](../Page/地球大气层.md "wikilink")[极光现象](../Page/极光.md "wikilink")。
具有和地球类似的磁场的其他[行星也有极光现象](../Page/行星.md "wikilink")。

在星际媒质（主要是稀薄的氢和氦）中，太阳风就像是吹出了一个“大气泡”。在太阳风不能继续推动星际媒质的地方称之为[日球层顶](../Page/日球层顶.md "wikilink")（heliopause），这也通常被认为是太阳系的外边界。
这个边界距离太阳到底多远还没有精确的结果，可能根据太阳风的强弱和当地星际媒质的密度而变化。一般认为它远远超过了[冥王星的轨道](../Page/冥王星.md "wikilink")。

## 历史

在1930年代，科学家已经知道太阳的[日冕层有几百万摄氏度的高温](../Page/日冕.md "wikilink")，这是通过在[日全食时观察到的日冕的突出形状推算的](../Page/日全食.md "wikilink")。一些相关的[光谱分析工作也证实了这个高温](../Page/光谱分析.md "wikilink")。
在五十年代，英国数学家Sydney
Chapman计算分析了在如此高温及如此好的导热条件下气体的性质。他发现日冕一定会延伸到[地球轨道以外的空间中](../Page/地球轨道.md "wikilink")。同样在五十年代，德国科学家Ludwig
Biermann对彗尾的逆太阳方向现象（即无论彗星运动方向是朝向太阳还是远离太阳，其尾部总是指向远离太阳的方向）开展了相关研究，
他推测是太阳吹出来的稳定的风压迫彗尾产生了这个现象。

1958年，Parker预言应该有一股强劲的风从太阳不间断的吹出来，使等离子体充斥行星际的空间。 在此之前，科学家认为这个空间是一个真空。
Parker
意识到在Chapman的模型中太阳向外发散热量与Biermann得用来解释彗尾的假设应该是同一种现象。Parker证明即使日冕被强烈的太阳引力束缚，由于它是热的良导体。日冕仍然会在离太阳很远的距离处保持高温。这是因为引力的大小随着距离的增大而减小,
所以在日冕外层的太阳大气会逃逸到空间中去。

当时对Parker太阳风假说的反对意见是很强的。
他给[天文物理期刊投递的论文被两个评审员拒绝了](../Page/天文物理期刊.md "wikilink")。但是当时的编辑[蘇布拉馬尼揚·錢德拉塞卡](../Page/蘇布拉馬尼揚·錢德拉塞卡.md "wikilink")（他后来获得了1983年诺贝尔物理学奖）保存了这篇论文。

在1960年代，这个太阳风假说被直接的卫星观测证实了。此发现永远的改变了科学家对行星际空间的看法，并得以解释很多现象，像“[磁暴](../Page/磁暴.md "wikilink")”（可以使地球上的供电网络瘫痪）、[极光还有其他一些太阳地球现象甚至遥远的恒星形成等](../Page/极光.md "wikilink")。

## 参考文献

### 引用

。。

### 来源

  - 刊物文章

<!-- end list -->

  -
<!-- end list -->

  - 网页

<!-- end list -->

  - <http://news.nationalgeographic.com/news/2003/08/0827_030827_kyotoprizeparker.html>

## 參見

  - [磁顶](../Page/磁顶.md "wikilink")
  - [磁层](../Page/磁层.md "wikilink")
  - [电离层](../Page/电离层.md "wikilink")
  - [激波](../Page/激波.md "wikilink")

{{-}}

[Category:太阳活动](../Category/太阳活动.md "wikilink")
[Category:電漿體天體物理學](../Category/電漿體天體物理學.md "wikilink")
[Category:恆星現象](../Category/恆星現象.md "wikilink")