**艾兰县**（）是[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[普吉特灣上的一個縣](../Page/普吉特灣.md "wikilink")，成立於1853年1月6日。面積1,340平方公里。根据[美國2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，共有人口78,506人。\[1\]縣治[庫珀維爾](../Page/庫珀維爾_\(華盛頓州\).md "wikilink")，最大城市[橡港市](../Page/橡港市_\(華盛頓州\).md "wikilink")。

該郡由兩座大島，[卡馬諾島和](../Page/卡馬諾島.md "wikilink")[惠德比島](../Page/惠德比島.md "wikilink")，和七座小島所組成，縣名也由此而來。於1852年12月22日從[瑟斯顿县
(华盛顿州)分出](../Page/瑟斯顿县_\(华盛顿州\).md "wikilink")，由[奧勒岡領地的立法機關頒定](../Page/奧勒岡領地.md "wikilink")\[2\]，是為華盛頓州第八老的郡。原本該郡包含現在的[斯诺霍米什县](../Page/斯诺霍米什县_\(华盛顿州\).md "wikilink")、[斯卡吉特县](../Page/斯卡吉特县_\(华盛顿州\).md "wikilink")、[霍特科姆县和](../Page/霍特科姆县_\(华盛顿州\).md "wikilink")[圣胡安县](../Page/圣胡安县_\(华盛顿州\).md "wikilink")。

艾兰县包含在[西雅圖都會區內](../Page/西雅圖都會區.md "wikilink")。

## 地理

根據[美国人口调查局](../Page/美国人口调查局.md "wikilink")，本郡的總面積為，其中陸地面積占、水域面積占
（59.7%）。該郡是華盛頓州面積第一小的郡。

### 地理特色

  - [普吉特海湾](../Page/普吉特海湾.md "wikilink")

  - [胡安·德·富卡海峡](../Page/胡安·德·富卡海峡.md "wikilink")

  - [惠德比島](../Page/惠德比島.md "wikilink")

  -
  -
### 毗鄰郡

  - 北－[斯卡吉特县](../Page/斯卡吉特县_\(华盛顿州\).md "wikilink")
  - 東－[斯诺霍米什县](../Page/斯诺霍米什县_\(华盛顿州\).md "wikilink")
  - 西南－[基沙普縣](../Page/基沙普縣.md "wikilink")
  - 西－[傑佛遜縣](../Page/傑佛遜縣_\(華盛頓州\).md "wikilink")
  - 西北－[圣胡安县](../Page/圣胡安县_\(华盛顿州\).md "wikilink")

### 國家保護區

  - （部分）

  - （部分）

## 資料來源

## 外部連結

  - [官方網站](https://web.archive.org/web/20070119085950/http://www.islandcounty.net/)

  - [艾兰县交通局](http://www.islandtransit.org/)

  - [艾兰县觀光網站](http://www.WhidbeyCamanoIslands.com/)

[Island County](../Category/华盛顿州行政区划.md "wikilink")

1.
2.