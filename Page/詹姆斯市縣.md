**詹姆斯市縣**位於[維吉尼亞半島](../Page/維吉尼亞半島.md "wikilink")，在[美國的](../Page/美國.md "wikilink")[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")。人口是48,102（2000年），它常使人聯想到[威廉斯堡](../Page/威廉斯堡.md "wikilink")。

## 歷史

1634年詹姆斯市郡被是[英國在](../Page/英國.md "wikilink")[弗吉尼亞的](../Page/弗吉尼亞.md "wikilink")[殖民地](../Page/殖民地.md "wikilink")。在17世紀期間，殖民地[詹姆斯鎮](../Page/詹姆斯鎮.md "wikilink")(1607年)被創立後，英國人開始在漢普頓錨地附近安定。1634年，奉[英國](../Page/英國.md "wikilink")[國王](../Page/國王.md "wikilink")[查理一世
(英國)的命令](../Page/查理一世_\(英國\).md "wikilink")，當地的八個郡或縣和當地的約5,000個居民要成為英國在弗吉尼亞的殖民地，包括一部分的[查爾斯市縣](../Page/查爾斯市縣.md "wikilink")。

[詹姆斯市郡](../Page/詹姆斯市郡.md "wikilink")，[詹姆斯河和](../Page/詹姆斯河.md "wikilink")[詹姆斯鎮都是從他們的國王](../Page/詹姆斯鎮.md "wikilink")[詹姆士一世
(英格兰)的名字而命名](../Page/詹姆士一世_\(英格兰\).md "wikilink")。1642-43年，[詹姆斯市郡](../Page/詹姆斯市郡.md "wikilink")(James
Town Shire)被改為**詹姆斯市縣**(James City County)。

1693年中央種植園是[威廉與瑪麗學院的選址](../Page/威廉與瑪麗學院.md "wikilink")。1699年中央種植園成為了弗吉尼亞([英國殖民地](../Page/英國殖民地.md "wikilink"))的郡首府和於1699年，為了紀念英皇[威廉三世](../Page/威廉三世.md "wikilink")，該村莊被重新命名為[威廉斯堡](../Page/威廉斯堡.md "wikilink")。

中央種植園是一個在高原的邊緣的天然堡壘，在弗吉尼亞半島的末端它的土地向東對海傾斜。在早期與當地的[印第安人的衝突期間](../Page/印第安人.md "wikilink")，中央種植園是十分的重要。

1693年[威廉與瑪麗學院建立今日的威廉斯堡附近](../Page/威廉與瑪麗學院.md "wikilink")。1698年10月20日[詹姆斯鎮的國會大廈第四次被燒](../Page/詹姆斯鎮.md "wikilink")。1699年，
[威廉與瑪麗學院的一個學生小組遞交提案](../Page/威廉與瑪麗學院.md "wikilink")，希望郡首府可以移至向中央種植園。

同時間，為了紀念英皇[威廉三世](../Page/威廉三世.md "wikilink")，該村莊被重新命名為[威廉斯堡](../Page/威廉斯堡.md "wikilink")。

1884年，[威廉斯堡從詹姆斯市縣獨立出去](../Page/威廉斯堡.md "wikilink")。但威廉斯堡仍然是詹姆斯市縣的县治，而且他們分享一樣的大學制度、法院和一些官員。

## 地理

根據[美國人口調查局](../Page/美國人口調查局.md "wikilink")，縣的總面積是465 km²，其中370
km²是[土地](../Page/土地.md "wikilink")，95
km²，或20.47%是[水](../Page/水.md "wikilink")。

[J](../Category/維吉尼亞州縣.md "wikilink")
[Category:維吉尼亞州歷史](../Category/維吉尼亞州歷史.md "wikilink")