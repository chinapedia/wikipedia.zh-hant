[Yau_Oi_Estate.jpg](https://zh.wikipedia.org/wiki/File:Yau_Oi_Estate.jpg "fig:Yau_Oi_Estate.jpg")
[Yau_Oi_Estate_Garden_201411.jpg](https://zh.wikipedia.org/wiki/File:Yau_Oi_Estate_Garden_201411.jpg "fig:Yau_Oi_Estate_Garden_201411.jpg")
[Yau_Oi_Estate_Shopping_Arcade_Level_2_Access_2009.jpg](https://zh.wikipedia.org/wiki/File:Yau_Oi_Estate_Shopping_Arcade_Level_2_Access_2009.jpg "fig:Yau_Oi_Estate_Shopping_Arcade_Level_2_Access_2009.jpg")
[H.A.N.D.S_Zone_D_Cooked_Food_Stalls_201501.jpg](https://zh.wikipedia.org/wiki/File:H.A.N.D.S_Zone_D_Cooked_Food_Stalls_201501.jpg "fig:H.A.N.D.S_Zone_D_Cooked_Food_Stalls_201501.jpg")\]\]
[Yau_Oi_Estate_Car_Park.jpg](https://zh.wikipedia.org/wiki/File:Yau_Oi_Estate_Car_Park.jpg "fig:Yau_Oi_Estate_Car_Park.jpg")

**友愛邨**（）是香港的一個[公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[屯門新市鎮中部](../Page/屯門.md "wikilink")，鄰近屯門市中心，於1980年落成。現由[香港房屋委員會負責屋邨管理](../Page/香港房屋委員會.md "wikilink")。由於友愛邨落成後的屋宇數量相對當時其他公共屋邨為多，所以曾成為全港最多居民的公共屋邨。

另外由於友愛邨建於青山灣填海區上，所以曾經有嚴重土地沉降問題，其中於[宣道會陳瑞芝紀念中學操場旁的建築物亦因此而加建多級樓梯](../Page/宣道會陳瑞芝紀念中學.md "wikilink")。現時土地沉降的情況已經較為穩定，但在邨內亦有不少地方可觀察到沉降的痕跡\[1\]。

## 屋邨資料

### 樓宇

| 樓宇名稱 | 樓宇類型                                     | 落成年份 |
| ---- | ---------------------------------------- | ---- |
| 愛明樓  | [雙工字型](../Page/雙工字型.md "wikilink")       | 1979 |
| 愛禮樓  | 1980                                     |      |
| 愛曦樓  |                                          |      |
| 愛智樓  | 1981                                     |      |
| 愛信樓  | [雙塔式](../Page/雙塔式.md "wikilink")         |      |
| 愛義樓  |                                          |      |
| 愛廉樓  | 1982                                     |      |
| 愛德樓  | [三座相連工字型](../Page/三座相連工字型.md "wikilink") | 1980 |
| 愛暉樓  |                                          |      |
| 愛勇樓  | [舊長型](../Page/舊長型.md "wikilink")         |      |
| 愛樂樓  | 1981                                     |      |
|      |                                          |      |

  - [H.A.N.D.S](../Page/H.A.N.D.S.md "wikilink")
    D1停車場（時租及月租，地下是H.A.N.D.S街市）
  - H.A.N.D.S D2停車場（月租）

## 教育

### 幼稚園

  - [東華三院高德根紀念幼稚園](http://www.twghktkkg.edu.hk)（1980年創辦）（位於友愛邨愛明樓地下）
  - [仁濟醫院友愛幼稚園](http://www.ychyo.org.hk)（1982年創辦）（位於友愛邨愛暉樓地下）
  - [仁愛堂顏寶鈴幼稚園](http://ppe.yot.org.hk/kg01/index.php)（1982年創辦）（位於友愛邨愛義樓地下）
  - [路德會呂祥光幼稚園](http://www.lckk.edu.hk)（1982年創辦）（位於友愛邨愛廉樓高座地下）

### 小學

  - [順德聯誼總會何日東小學](../Page/順德聯誼總會何日東小學.md "wikilink")
  - [伊斯蘭學校](../Page/伊斯蘭學校_\(友愛邨\).md "wikilink")

Islamic Primary School.jpg

### 中學

  - [順德聯誼總會譚伯羽中學](../Page/順德聯誼總會譚伯羽中學.md "wikilink")
  - [香港九龍塘基督教中華宣道會陳瑞芝紀念中學](../Page/香港九龍塘基督教中華宣道會陳瑞芝紀念中學.md "wikilink")
  - [可藝中學（嗇色園主辦）](../Page/可藝中學（嗇色園主辦）.md "wikilink")

## 居民設施

[8336.jpg](https://zh.wikipedia.org/wiki/File:8336.jpg "fig:8336.jpg")
[Yau_Oi_Estate_Football_Field.jpg](https://zh.wikipedia.org/wiki/File:Yau_Oi_Estate_Football_Field.jpg "fig:Yau_Oi_Estate_Football_Field.jpg")
[Yau_Oi_Estate_Table_Tennis_Zone.jpg](https://zh.wikipedia.org/wiki/File:Yau_Oi_Estate_Table_Tennis_Zone.jpg "fig:Yau_Oi_Estate_Table_Tennis_Zone.jpg")
[Yau_Oi_Estate_Pebble_Walking_Trail.jpg](https://zh.wikipedia.org/wiki/File:Yau_Oi_Estate_Pebble_Walking_Trail.jpg "fig:Yau_Oi_Estate_Pebble_Walking_Trail.jpg")
[Yau_Oi_Estate_Sitting_Area.jpg](https://zh.wikipedia.org/wiki/File:Yau_Oi_Estate_Sitting_Area.jpg "fig:Yau_Oi_Estate_Sitting_Area.jpg")

友愛邨擁有不同設施給居民使用，包括：

### 商業

  - [H.A.N.D.S](../Page/H.A.N.D.S.md "wikilink")

### 康體

  - 籃球場3個
  - 足球場1個
  - 遊樂場6個
  - [友愛體育館](../Page/友愛體育館.md "wikilink")（位於[興安里](../Page/興安里.md "wikilink")，與友愛邨隔著[皇珠路](../Page/皇珠路.md "wikilink")）

Yau Oi Estate Basketball Court.jpg|籃球場 Yau Oi Estate Badminton
Court.jpg|羽毛球場

<table>
<thead>
<tr class="header">
<th><p>兒童遊樂場及健體區</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yau Oi Estate Gym Zone.jpg|健體區 Yau Oi Estate Playground.jpg|兒童遊樂場 Yau Oi Estate Playground (3) and Gym Zone (3).jpg|健體區（3）及兒童遊樂場（3） Yau Oi Estate Playground (7) and Gym Zone (4).jpg|健體區（4）及兒童遊樂場（7） Yau Oi Estate Playground (4).jpg|兒童遊樂場（4）</p>
<p>Yau Oi Estate Gym Zone (5).jpg|健體區（5） Yau Oi Estate Playground (5) and Gym Zone (7).jpg|兒童遊樂場（5）健體區（7） Yau Oi Estate Playground (6) and Gym Zone (6).jpg|健體區（6）及兒童遊樂場（6） Yau Oi Estate Gym Zone (8).jpg|健體區（8） Yau Oi Estate Playground (8).jpg|兒童遊樂場（8）</p>
<p>Yau Oi Estate Playground (9).jpg|兒童遊樂場（9） Yau Oi Estate Playground (10).jpg|兒童遊樂場（10） Yau Oi Estate Playground (11).jpg|兒童遊樂場（11）</p></td>
</tr>
</tbody>
</table>

### 公共空間

  - 友愛廣場

## 邨內街道

  - [友愛路](../Page/友愛路_\(香港\).md "wikilink")
  - 愛勇街
  - 愛明里
  - 愛德里
  - 愛禮里

## 選區

友愛邨於[區議會選區分界中分為友愛南](../Page/區議會.md "wikilink")（L05）及友愛北（L06）兩個選區。\[2\]
其中愛曦樓、愛明樓、愛德樓及愛禮樓為友愛北選區，而愛暉樓、愛勇樓、愛樂樓、愛智樓、愛信樓、愛義樓及愛廉樓為友愛南選區。現時友愛南區議員為[民建聯曾憲康](../Page/民建聯.md "wikilink")，友愛北區議員為[工黨譚駿賢](../Page/工黨.md "wikilink")。立法會地區直選屬於[新界西](../Page/新界西.md "wikilink")（LC4）選區。\[3\]

## 重大事件

1981年7月，有居民發現邨內每個單位的牆上出現神秘符號，其後更先後發生多宗爆竊案。到7月13日晚上8時，居民發現7名形跡可疑的青年後便隨即通知警方，要求警方拘捕。警方在查問後認為他們身上沒有攻擊性武器，亦能出示身份證明文件，所以釋放青年。不過此舉引起邨民認為警方處理手法不當，百多人聚集邨內警署門外鼓譟。到凌晨約1時，警方在愛明樓帶走兩名青年調查。警方避免發生事故，用車將兩名青年送去青山警署，不過有百多名居民發現後，在路口攔截包圍警車。事件擾攘到大約凌晨3時，友愛邨和安定邨的居民仍然不肯離去，而且越來越多人聚集。到7月14日早上8時，仍然有很多居民在警署門外聚集，居民代表和警方及房屋署人員開會，商討改善屋邨治安和鐵閘問題。

警方人員在早上將另外五個青年帶返警署調查。警方發言人表示，由於沒有足夠證據證明該7個青年與記號或爆竊案有關，查問後全部釋放。\[4\]

7月15日晚上，逾千名友愛邨居民，持刀、棒追捕6名可疑男女，包圍並攻擊愛德樓一已上鎖的垃圾房，群情洶湧。警方派出6部巡邏車、50名軍裝及大批便衣警察到場鎮壓，警員地氈式搜索至7月16日凌晨2時無果而結束。\[5\]

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [輕鐵](../Page/香港輕鐵.md "wikilink"):[友愛站](../Page/友愛站.md "wikilink")、[安定站](../Page/安定站.md "wikilink")

<!-- end list -->

  - [友愛（南）巴士總站](../Page/友愛（南）巴士總站.md "wikilink")

<!-- end list -->

  - [豐安街](../Page/豐安街.md "wikilink")

<!-- end list -->

  - [海珠路](../Page/海珠路.md "wikilink")

<!-- end list -->

  - [屯門鄉事會路](../Page/屯門鄉事會路.md "wikilink")

<!-- end list -->

  - 安定邨居民巴士總站

<!-- end list -->

  - [愛勇街](../Page/愛勇街.md "wikilink")

<!-- end list -->

  - [海榮路](../Page/海榮路.md "wikilink")

<!-- end list -->

  - [青山公路](../Page/青山公路.md "wikilink")[青山灣段](../Page/青山灣.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 荃灣至元朗線 (通宵服務)\[6\]
  - 旺角至元朗線 (下午至通宵服務)\[7\]
  - 元朗至佐敦道線 (24小時服務)\[8\]
  - 銅鑼灣至元朗線 (下午至通宵服務)\[9\]

<!-- end list -->

  - [兆麟街](../Page/兆麟街.md "wikilink")

<!-- end list -->

  - [兆興里](../Page/兆興里.md "wikilink")

<!-- end list -->

  - [海皇路](../Page/海皇路.md "wikilink")

</div>

</div>

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [房屋署友愛邨資料](https://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=2726)

[Category:屯門市中心](../Category/屯門市中心.md "wikilink")
[Category:建在填海/填塘地的香港公營房屋](../Category/建在填海/填塘地的香港公營房屋.md "wikilink")
[Category:香港使用中央石油氣的屋苑](../Category/香港使用中央石油氣的屋苑.md "wikilink")

1.  [1](http://hk.apple.nextmedia.com/news/art/20070906/10118063)
2.  [區議會選區分界圖—屯門區](http://www.eac.gov.hk/pdf/distco/maps/dc2015l.pdf)
3.  [立法會地方選區分界圖—新界西選區](http://www.eac.gov.hk/pdf/legco/2016/2016boundaries/lc2016ntw.pdf)
4.
5.
6.  [荃灣荃灣千色匯　＞　屯門及元朗](http://www.16seats.net/chi/rmb/r_n53.html)
7.  [旺角新填地街　＞　屯門及元朗](http://www.16seats.net/chi/rmb/r_kn60.html)
8.  [佐敦道白加士街　—　屯門及元朗](http://www.16seats.net/chi/rmb/r_kn68.html)
9.  [銅鑼灣波斯富街　＞　屯門及元朗](http://www.16seats.net/chi/rmb/r_hn68.html)