<table>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: center;"><p><strong><a href="../Page/香港地鐵.md" title="wikilink">香港的地鐵在</a>2007年<a href="../Page/兩鐵合併.md" title="wikilink">兩鐵合併前路線及計劃</a></strong></p></td>
<td style="text-align: right;"></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td><p>市區綫：<small> <font color={{觀塘綫色彩}}>█</font> </small><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a> • <small> <font color={{荃灣綫色彩}}>█</font> </small><a href="../Page/荃灣綫.md" title="wikilink">荃灣綫</a> • <small> <font color={{港島綫色彩}}>█</font> </small><a href="../Page/港島綫.md" title="wikilink">港島綫</a> • <small> <font color={{將軍澳綫色彩}}>█</font> </small><a href="../Page/將軍澳綫.md" title="wikilink">將軍澳綫</a> • <small> <font color={{東涌綫色彩}}>█</font> </small><a href="../Page/東涌綫.md" title="wikilink">東涌綫</a> • <small> <font color={{迪士尼綫色彩}}>█</font> </small><a href="../Page/迪士尼綫.md" title="wikilink">迪士尼綫</a></p></td>
</tr>
<tr class="odd">
<td><p>機場聯絡軌道系統：<small> <font color={{機場快綫色彩}}>█</font> </small><a href="../Page/機場快綫.md" title="wikilink">機場快綫</a></p></td>
</tr>
<tr class="even">
<td><p>建造中路線︰<small> <font color={{南港島綫東段色彩}}>█</font> </small><a href="../Page/南港島綫東段.md" title="wikilink">南港島綫東段</a></p></td>
</tr>
<tr class="odd">
<td><p>計劃中路線︰<small> <font color={{東涌綫色彩}}>█</font> </small><a href="../Page/北港島綫.md" title="wikilink">北港島綫</a> • <small> <font color={{環保連接系統色彩}}>█</font> </small><a href="../Page/環保連接系統.md" title="wikilink">環保連接系統</a> • <small> <font color={{南港島綫西段色彩}}>█</font> </small><a href="../Page/南港島綫西段.md" title="wikilink">南港島綫西段</a> • <small> <font color={{東九龍綫色彩}}>█</font> </small><a href="../Page/東九龍綫_(2014年方案).md" title="wikilink">東九龍綫</a></p></td>
</tr>
<tr class="even">
<td><p>旅遊纜車項目：<small> <font color={{昂坪360色彩}}>█</font> </small><a href="../Page/昂坪360.md" title="wikilink">昂坪360</a></p></td>
</tr>
<tr class="odd">
<td><p><span style="font-size:smaller;">地鐵營運路線已經於<a href="../Page/2007年.md" title="wikilink">2007年</a><a href="../Page/12月2日.md" title="wikilink">12月2日合併至</a><a href="../Page/港鐵.md" title="wikilink">港鐵營運路線</a>。</span></p></td>
</tr>
</tbody>
</table>

<noinclude>
[zh-yue:Template:香港地鐵](../Page/zh-yue:Template:香港地鐵.md "wikilink")
<noinclude> </noinclude>

[Category:香港地鐵](../Category/香港地鐵.md "wikilink")
[地鐵](../Category/香港鐵路模板.md "wikilink")