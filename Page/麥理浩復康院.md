[MacLehose_Medical_Rehabilitation_Centre.jpg](https://zh.wikipedia.org/wiki/File:MacLehose_Medical_Rehabilitation_Centre.jpg "fig:MacLehose_Medical_Rehabilitation_Centre.jpg")
[MacLehoseMedicalRehabilitationCentre.jpg](https://zh.wikipedia.org/wiki/File:MacLehoseMedicalRehabilitationCentre.jpg "fig:MacLehoseMedicalRehabilitationCentre.jpg")

**麥理浩復康院**（）是一所[香港公營醫院](../Page/香港.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區.md "wikilink")[薄扶林](../Page/薄扶林.md "wikilink")[沙灣](../Page/沙灣.md "wikilink")[沙灣徑](../Page/沙灣徑.md "wikilink")7號，由[醫院管理局管理](../Page/醫院管理局.md "wikilink")，隸屬港島西醫院聯網。

## 歷史

麥理浩復康院以第25任[香港總督](../Page/香港總督.md "wikilink")[麥理浩命名](../Page/麥理浩.md "wikilink")，1984年12月7日正式啟用，現為一所專科醫院，專注為受傷或疾病而導致身體傷殘的病人服務，提供住院、長期護理、治療及康復服務。自1991年12月1日起由醫院管理局接管。由2001年2月5日開始麥理浩復康院和[根德公爵夫人兒童醫院及](../Page/根德公爵夫人兒童醫院.md "wikilink")[東華三院馮堯敬醫院合併](../Page/東華三院馮堯敬醫院.md "wikilink")。

## 服務

  - 復康醫療
  - 復康護理
  - [物理治療](../Page/物理治療.md "wikilink")
  - [職業治療](../Page/職業治療.md "wikilink")：職業治療部每年都為[香港理工大學的職業治療學生提供臨床教學](../Page/香港理工大學.md "wikilink")。
  - [義肢矯形](../Page/義肢.md "wikilink")：於1985年成立，為麥理浩復康院及其他港島醫院提供義肢及矯形服務。
  - 醫務社會工作
  - [臨床心理學](../Page/臨床心理學.md "wikilink")
  - [言語治療](../Page/言語治療.md "wikilink")

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/沙灣徑.md" title="wikilink">沙灣徑</a></dt>

</dl>
<dl>
<dt><a href="../Page/域多利道.md" title="wikilink">域多利道</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參見

  - [香港醫院列表](../Page/香港醫院列表.md "wikilink")

## 外部連結

  - [麥理浩復康院](http://www3.ha.org.hk/mmrc/c_home.htm)

[Category:南區醫院 (香港)](../Category/南區醫院_\(香港\).md "wikilink") [Category:沙灣
(香港)](../Category/沙灣_\(香港\).md "wikilink")
[Category:1984年完工醫院](../Category/1984年完工醫院.md "wikilink")