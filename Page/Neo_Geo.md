**Neo Geo
AES**是[SNK公司发布的](../Page/SNK.md "wikilink")[家用游戏机](../Page/家用游戏机.md "wikilink")，在[16位元遊樂器中性能较強](../Page/16位元.md "wikilink")。發售時的宣傳標語為「將很棒的遊戲帶回家吧！」。

因使用了與[NEO GEO
MVS街機相同的機板和CPU](../Page/NEO_GEO_MVS.md "wikilink")，軟體部份僅稍做些許變更，即可維持相同的品質移植到家用機使用。但也因此造成了机器极高的价格，影响了普及率。

遊戲中的存檔資料記錄於[PCMCIA規格的](../Page/PCMCIA.md "wikilink")[PC
CARD記憶體中](../Page/PC_CARD.md "wikilink")，容量為8[KB](../Page/KB.md "wikilink")。

## 数据

|       |                                                                                                   |                                    |                                                                                                                         |      |                                                                                                                                                                                                                                  |        |                                 |         |     |
| ----- | ------------------------------------------------------------------------------------------------- | ---------------------------------- | ----------------------------------------------------------------------------------------------------------------------- | ---- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------ | ------------------------------- | ------- | --- |
| *CPU* | [68000](../Page/MC68000.md "wikilink") (12MHz)、聲音：[Z80](../Page/Z80.md "wikilink") (4MHz) |- ---- | *[記憶體](../Page/記憶體.md "wikilink")* | [RAM](../Page/Random_Access_Memory.md "wikilink")：【68000】64KB、【Z80】2KB、【[VRAM](../Page/VRAM.md "wikilink")】68KB |- ---- | *聲音* | [Yamaha](../Page/Yamaha.md "wikilink") [YM2610](../Page/YM2610.md "wikilink")（[PCM](../Page/PCM.md "wikilink")7音 [FM](../Page/FM声源.md "wikilink")4音 [PSG](../Page/PSG.md "wikilink")3音 [雜音](../Page/雜音.md "wikilink")1音） |- ---- | *顯示色數* | 65,536色（同時顯示顏色 4,096 色） |- ---- | *圖塊顯示數* | 380 |

*NEOGEO SPEC* |- ----

## 外部链接

[Category:街机主板](../Category/街机主板.md "wikilink")
[Category:SNK](../Category/SNK.md "wikilink")
[Category:第四世代遊戲機](../Category/第四世代遊戲機.md "wikilink")
[Category:1990年面世的產品](../Category/1990年面世的產品.md "wikilink")
[Category:1991年面世的產品](../Category/1991年面世的產品.md "wikilink")