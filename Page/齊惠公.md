**齊惠公**，**姜**姓，名**元**，[齊桓公之子](../Page/齊桓公.md "wikilink")、母親為衛國女子少衛姬。因躲避齊國內亂，所以逃往衛國。[齊懿公即位後](../Page/齊懿公.md "wikilink")，非常驕橫，人心不歸附。所以[齊懿公被邴歜和閻職殺死後](../Page/齊懿公.md "wikilink")，齊國人廢黜懿公之子而從衛國迎接公子元回齊，是爲齊惠公。在位十年至公元前599年，齊惠公死，其子[齊頃公無野繼位](../Page/齊頃公.md "wikilink")。當初，[崔杼曾得到惠公寵幸](../Page/崔杼.md "wikilink")，等到惠公死後，高氏、國氏怕受他脅迫，把崔杼驅逐出國，崔杼便逃到衛國。後來崔杼扶立[齊莊公即位](../Page/齊後莊公.md "wikilink")。

在位期間執政為[國佐](../Page/國佐.md "wikilink")、[高固](../Page/高固.md "wikilink")。

## 參見

  -
[Category:姜齐君主](../Category/姜齐君主.md "wikilink")
[Category:前599年逝世](../Category/前599年逝世.md "wikilink")
[Y](../Category/姜姓.md "wikilink")
[Category:齊國君主](../Category/齊國君主.md "wikilink")