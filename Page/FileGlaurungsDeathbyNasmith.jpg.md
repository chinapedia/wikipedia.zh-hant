<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>圖林·圖倫拔在自殺前擊殺格勞龍</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://en.wikipedia.org/wiki/Image:GlaurungsDeathbyNasmith.jpg">http://en.wikipedia.org/wiki/Image:GlaurungsDeathbyNasmith.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>9/2/2007</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>Tommy9281</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>

## 合理使用

  - 本圖是由托爾金的著名插畫家繪製。
  - 2006年八月，[泰德·内史密斯](../Page/泰德·内史密斯.md "wikilink")（Ted
    Nasmith）允許他的藝術作品用於維基百科，詳見[1](http://en.wikipedia.org/wiki/Wikipedia_talk:WikiProject_Middle-earth/archive5#New_Images)
  - 本圖為低解像圖。
  - 本圖僅用作解說條目。
  - 本圖並不妨礙版權擁有者銷售原作。