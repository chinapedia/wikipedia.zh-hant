[Alc-f5es.jpg](https://zh.wikipedia.org/wiki/File:Alc-f5es.jpg "fig:Alc-f5es.jpg")是冷戰時代的諾斯洛普公司經典之作，幫助西方陣營的小國迅速擁有空軍\]\]
[B-2_Spirit_050413-F-1740G-001a.jpg](https://zh.wikipedia.org/wiki/File:B-2_Spirit_050413-F-1740G-001a.jpg "fig:B-2_Spirit_050413-F-1740G-001a.jpg")到了21世紀依然是美軍主力\]\]
**诺斯洛普公司**（Northrop
Corporation），簡稱**諾普**，是美国主要飞机制造商之一。由[约翰·诺斯洛普创建](../Page/约翰·诺斯洛普.md "wikilink")。

## 歷史

诺斯洛普于1916年在洛克西德航空器制造公司（Loughead）得到其在航空界的第一个工作。1923年诺斯洛普加入了[道格拉斯飞行器公司并成为首席工程师](../Page/道格拉斯飞行器公司.md "wikilink")。1927年诺斯洛普回到了重新组建的[洛克西德公司](../Page/洛克西德公司.md "wikilink")（Lockheed）。并设计了**洛克西德织女星**飞机。

1928年诺斯洛普创建了其自己的Avion公司，1930年被迫将公司卖给了联合飞行器和运输公司，成为诺斯洛普航空技术公司。1932年在唐纳德·道格拉斯的帮助下创办了[诺斯洛普公司](../Page/诺斯洛普公司.md "wikilink")。该公司制造了两种非常成功的单翼机，**伽马**和**德尔他**。

1937年由于劳工问题原诺斯洛普公司成为道格拉斯公司的一部分。1939年诺斯洛普在加利福尼亚Hawthorne重新创建了新的[诺斯洛普公司](../Page/诺斯洛普公司.md "wikilink")。在新的公司里，诺斯洛普把精力集中在[飞翼的设计方面](../Page/飞翼.md "wikilink")。诺斯洛普相信飞翼是飞行器设计的下一个重要阶段。其设计的飞翼包括**N-1M**、**N-9M**、[YB-35](../Page/YB-35.md "wikilink")、[YB-49等](../Page/YB-49.md "wikilink")。但是只有到了在20世纪80年代使用飞翼设计的[B-2隐形轰炸机才真正把飞翼的设计概念引入实用阶段](../Page/B-2.md "wikilink")。

1994年由于在[先进战斗机和](../Page/先進戰術戰鬥機.md "wikilink")[联合攻击机两个项目的竞争上输给了](../Page/聯合攻擊戰鬥機計劃.md "wikilink")[洛克西德·马丁公司](../Page/洛克西德·马丁公司.md "wikilink")，诺斯洛普公司同[格鲁门公司合併成为](../Page/格鲁门公司.md "wikilink")[诺斯洛普·格鲁门公司](../Page/诺斯洛普·格鲁门公司.md "wikilink")。

诺斯洛普公司的著名产品包括[P-61黑寡妇](../Page/P-61.md "wikilink")[夜间战斗机](../Page/夜间战斗机.md "wikilink")，[F-5自由战士](../Page/F-5戰鬥機.md "wikilink")，[F-20虎鲨](../Page/F-20虎鲨战斗机.md "wikilink")，[B-2](../Page/B-2.md "wikilink")，[T-38教練機](../Page/T-38教練機.md "wikilink")。以及在竞争中输给[F-22的](../Page/F-22.md "wikilink")[YF-23黑寡婦二式戰鬥機](../Page/YF-23黑寡婦二式戰鬥機.md "wikilink")（同[麦道合作](../Page/麦道.md "wikilink")）。

## 外部链接

  - [诺斯洛普·格鲁门](http://www.northropgrumman.com/)

[Category:美国飞机公司](../Category/美国飞机公司.md "wikilink")
[Category:美國已結業飛機製造商](../Category/美國已結業飛機製造商.md "wikilink")
[Category:1927年成立的公司](../Category/1927年成立的公司.md "wikilink")
[Category:1927年加利福尼亞州建立](../Category/1927年加利福尼亞州建立.md "wikilink")