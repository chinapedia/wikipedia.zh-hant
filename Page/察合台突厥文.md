**察合台突厥文**，也在与其他同是[突厥语的语言相提并论时省略](../Page/突厥语族.md "wikilink")『突厥』二字，作**察哈台文**、**查加泰文**（
- ; ，;
，），或『[纳瓦依的语言](../Page/纳瓦依.md "wikilink")』是在15世纪至19世纪间[中亞](../Page/中亞.md "wikilink")[突厥斯坦地區的](../Page/突厥斯坦.md "wikilink")[突厥民族的共同书面语](../Page/突厥.md "wikilink")，与其相对应的是通行于[突厥斯坦以西至](../Page/突厥斯坦.md "wikilink")[奥斯曼帝国境内的](../Page/奥斯曼帝国.md "wikilink")[奥斯曼突厥语](../Page/奥斯曼土耳其语.md "wikilink")。察合台語是[哈卡尼亚语的延续](../Page/哈卡尼亚语.md "wikilink")，文字系统亦如前。在19世纪末期，由于政治上的不统一，逐渐被地方化的各现代突厥语书面语所取代。其后裔包括[维吾尔语和](../Page/维吾尔语.md "wikilink")[乌兹别克语](../Page/乌兹别克语.md "wikilink")。

## 歷史

察合台語是一種突厥語，屬突厥語東南部方言，普遍通行於中亞前[蒙古帝國的](../Page/蒙古帝國.md "wikilink")[察合台汗國領土](../Page/察合台汗國.md "wikilink")，曾經與[欽察語一樣是中亞商旅之間的通用語言](../Page/欽察語.md "wikilink")。隨著地區勢力的分化，後來發展成各種方言。當中，[維吾爾語及](../Page/維吾爾語.md "wikilink")[烏茲別克語都脫胎自察合台語](../Page/烏茲別克語.md "wikilink")。後來當俄羅斯控制了中亞地區之後，由於大量俄語人口遷入，使察合台語人口愈來愈少，並在前[蘇聯時間消亡](../Page/蘇聯.md "wikilink")。

察合台文一般指15世紀─20世紀初中亞等地[突厥語諸民族使用的書面語](../Page/突厥語.md "wikilink")，古察合台文指15世紀─16世紀[維吾爾](../Page/維吾爾.md "wikilink")、[烏茲別克等族文人使用的突厥伊斯蘭文學語言](../Page/烏茲別克.md "wikilink")。最著名的是[阿里希爾·納沃伊](../Page/阿里希爾·納沃伊.md "wikilink")，他是使用察合台語的大詩人。各國學者對其使用時期與範圍尚無一致看法。中國維吾爾、[哈薩克](../Page/哈薩克.md "wikilink")、[柯爾克孜](../Page/柯爾克孜.md "wikilink")、烏茲別克、[塔塔爾等族到](../Page/塔塔爾.md "wikilink")20世紀初仍用為書面語。察合台文以[阿拉伯字母為基礎](../Page/阿拉伯字母.md "wikilink")，有[輔助符號](../Page/輔助符號.md "wikilink")。

## 代表性文献

### [纳瓦依的著作](../Page/纳瓦依.md "wikilink")

#### 《五卷书》

### 《[巴布尔纪](../Page/巴布尔纪.md "wikilink")》

### 《[突厥世系](../Page/突厥世系.md "wikilink")》

### 《[先知传](../Page/先知传.md "wikilink")》

## 参见

  - [突厥语](../Page/突厥语.md "wikilink")
  - [纳沃伊](../Page/纳沃伊.md "wikilink")
  - [巴布尔](../Page/巴布尔.md "wikilink")
  - [察合台汗國](../Page/察合台汗國.md "wikilink")
  - [奥斯曼突厥语](../Page/奥斯曼土耳其语.md "wikilink")

<!-- end list -->

  - [乌兹别克语](../Page/乌兹别克语.md "wikilink")
  - [维吾尔语](../Page/维吾尔语.md "wikilink")

## 參考文獻

  - Eckmann, János, *Chagatay Manual*. (Indiana University publications:
    Uralic and Altaic series ; 60). Bloomington, Ind.: Indiana
    University, 1966. Reprinted edition, Richmond: Curzon Press, 1997,
    ISBN 0-7007-0860-X, or ISBN 978-0-7007-0860-4.
  - Bodrogligeti, András J. E., *A Grammar of Chagatay*. (Languages of
    the World: Materials ; 155). München: LINCOM Europa, 2001. (Repr.
    2007), ISBN 3-89586-563-X.
  - Pavet de Courteille, Abel, *Dictionnaire Turk-Oriental: Destinée
    principalement à faciliter la lecture des ouvrages de Bâber,
    d'Aboul-Gâzi, de Mir Ali-Chir Nevâï, et d'autres ouvrages en langues
    touraniennes (Eastern Turkish Dictionary: Intended Primarily to
    Facilitate the Reading of the Works of Babur, Abu'l Ghazi, Mir ʿAli
    Shir Navaʾi, and Other Works in Turanian Languages)*. Paris, 1870.
    Reprinted edition, Amsterdam: Philo Press, 1972, ISBN 90-6022113-3.
  - Erkinov A. “Persian-Chaghatay Bilingualism in the Intellectual
    Circles of Central Asia during the 15th-18th Centuries (the case of
    poetical anthologies, bayāz)”. *International Journal of Central
    Asian Studies*. C.H.Woo (ed.). vol.12, 2008, pp. 57–82
    [1](http://www.iacd.or.kr/pdf/journal/12/3.PDF).

## 外部链接

  - [Russian imperial policies in Central
    Asia](http://vlib.iue.it/carrie/texts/carrie_books/paksoy-1/)
  - [Chagatai
    language](http://www.iranicaonline.org/articles/turkic-iranian-contacts-ii-chaghatay)
    at [Encyclopædia
    Iranica](../Page/Encyclopædia_Iranica.md "wikilink")

{{-}}

[Category:突厥語族](../Category/突厥語族.md "wikilink")
[Category:亞洲絕跡語言](../Category/亞洲絕跡語言.md "wikilink")
[Category:突厥文字](../Category/突厥文字.md "wikilink")
[Category:阿拉伯字母系統](../Category/阿拉伯字母系統.md "wikilink")