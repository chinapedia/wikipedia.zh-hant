[HK_Shatin_Po_Fook_Ancestral_Hill_01.JPG](https://zh.wikipedia.org/wiki/File:HK_Shatin_Po_Fook_Ancestral_Hill_01.JPG "fig:HK_Shatin_Po_Fook_Ancestral_Hill_01.JPG")
[HK_Shatin_Po_Fook_Ancestral_Hill_02.JPG](https://zh.wikipedia.org/wiki/File:HK_Shatin_Po_Fook_Ancestral_Hill_02.JPG "fig:HK_Shatin_Po_Fook_Ancestral_Hill_02.JPG")\]\]
[HK_Shatin_Po_Fook_Ancestral_Hill_10.JPG](https://zh.wikipedia.org/wiki/File:HK_Shatin_Po_Fook_Ancestral_Hill_10.JPG "fig:HK_Shatin_Po_Fook_Ancestral_Hill_10.JPG")
[HK_Shatin_Po_Fook_Ancestral_Hill_03.JPG](https://zh.wikipedia.org/wiki/File:HK_Shatin_Po_Fook_Ancestral_Hill_03.JPG "fig:HK_Shatin_Po_Fook_Ancestral_Hill_03.JPG")
**寶福山**是[香港一個](../Page/香港.md "wikilink")[骨灰龕場](../Page/骨灰龕.md "wikilink")，位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[沙田排頭街](../Page/沙田.md "wikilink")（[沙田政府合署後方](../Page/沙田政府合署.md "wikilink")），主要服務為提供私立的[骨灰龕位及殮葬服務](../Page/骨灰龕.md "wikilink")。由香港殯儀業人士馮成創立，1991年4月26日進行開光禮\[1\]\[2\]。

根據1990年無綫電視播出的《[星期一檔案](../Page/星期一檔案.md "wikilink")》的報道，寶福山發展商的大股東是[中國建築](../Page/中國建築工程總公司.md "wikilink")\[3\]。

## 特色

寶福山佔地20萬平方尺，採用現代化企業管理，有營業部及顧客服務部。寶福山亦設有直達山頂的登山[扶手電梯](../Page/扶手電梯.md "wikilink")、[纜車及](../Page/纜車.md "wikilink")[升降機](../Page/升降機.md "wikilink")，主要提供[骨灰龕和](../Page/骨灰龕.md "wikilink")[神主牌兩種供奉形式](../Page/神主牌.md "wikilink")，亦根據[靈位的](../Page/靈位.md "wikilink")[風水位](../Page/風水.md "wikilink")、高低、行數等多方面設定價錢，由數千元至數十萬元不等。另外，寶福山提供24小時管理服務，並定時為先人上香和清潔[石碑](../Page/石碑.md "wikilink")，在[香港的](../Page/香港.md "wikilink")[墓園而言亦屬罕見](../Page/墓園.md "wikilink")。

## 圖片

<File:HK> Shatin Po Fook Ancestral Hill 04.JPG|寶福山的許願池 <File:HK> Shatin
Po Fook Ancestral Hill 07.JPG|寶福山的登山[扶手電梯](../Page/扶手電梯.md "wikilink")
<File:Po> Fook Hill (Hong Kong).jpg|從寶福山停車場上望，登山電梯在右方 <File:HK> Shatin
Po Fook Ancestral Hill 06.JPG|寶福山的登山[升降機](../Page/升降機.md "wikilink")
<File:HK> Shatin Po Fook Ancestral Hill
08.JPG|寶福山的[大雄寶殿一角](../Page/大雄寶殿.md "wikilink")
<File:HK> Shatin Po Fook Ancestral Hill 09.JPG|寶福山的骨灰龕香爐

## 下葬名人

  - [张國荣](../Page/张國荣.md "wikilink") （哥哥）\[4\]
  - [董驃](../Page/董驃.md "wikilink")\[5\]
  - [羅文](../Page/羅文.md "wikilink")（譚百先）（[靈位](../Page/靈位.md "wikilink")）
  - [沈殿霞](../Page/沈殿霞.md "wikilink")（肥姐）（[靈位](../Page/靈位.md "wikilink")）
  - [朱牧](../Page/朱牧.md "wikilink")
  - [向前](../Page/向前.md "wikilink")
  - [關德興](../Page/關德興.md "wikilink")\[6\]
  - [陳毓祥](../Page/陳毓祥.md "wikilink")
  - [陳僖儀](../Page/陳僖儀.md "wikilink")\[7\]
  - [張家萬](../Page/張家萬.md "wikilink")
  - [劉家良](../Page/劉家良.md "wikilink")\[8\]
  - [馮克安](../Page/馮克安.md "wikilink")
  - [鄭昌達](../Page/鄭昌達.md "wikilink")
  - [伊雷](../Page/伊雷.md "wikilink") \[9\]

## 爭議

### 投訴

1990年無綫新聞節目《[星期一檔案](../Page/星期一檔案.md "wikilink")》報道，

### 猴子為患

寶福山靠近山邊，距離[猴子聚居的](../Page/猴子.md "wikilink")[金山郊野公園不遠](../Page/金山郊野公園.md "wikilink")，加上有人喜歡餵飼猴子，所以近年經常有猴子出現。其中[清明節](../Page/清明節.md "wikilink")、[農曆七月](../Page/農曆.md "wikilink")[盂蘭節及](../Page/盂蘭節.md "wikilink")[重陽節前後](../Page/重陽節.md "wikilink")，有人在地上遺下水果等祭品無人清理，惹來猴群，經常發生擾人事件，管理處每次接到受猴子滋擾報告，保安員只是驅趕猴子了事，可謂無補於事。

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [東鐵綫](../Page/東鐵綫.md "wikilink")[沙田站](../Page/沙田站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參見

  - [寶福紀念館](../Page/寶福紀念館.md "wikilink")──寶福山同系殯儀館

## 参考文献

## 外部連結

  - [寶福山官網](http://www.pofookhill.com/)

[Category:香港墳場](../Category/香港墳場.md "wikilink")
[Category:香港旅遊景點](../Category/香港旅遊景點.md "wikilink")
[Category:沙田](../Category/沙田.md "wikilink")

1.  華僑日報, 1991-04-27 第5頁
2.  大公報, 1991-04-27 第7頁
3.  星期一檔案《寶福山》 1990-02-26 [電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")
    (香港)
4.  [墳場研究 Cemetery Study
    在沙田寶福山](https://www.facebook.com/hongkongcemetery/photos/a.350432878853422/411598046070238/?type=3&theater)
    www.facebook.com
5.  [墳場研究 Cemetery Study
    在沙田寶福山](https://www.facebook.com/hongkongcemetery/photos/a.350432878853422/394421651121211/?type=3&notif_id=1550796749427265&notif_t=page_post_reaction&ref=notif/)
    www.facebook.com
6.  [墳場研究 Cemetery Study
    在沙田寶福山](https://www.facebook.com/hongkongcemetery/photos/pcb.400578730505503/400578550505521/?type=3&theater/)
    www.facebook.com
7.  [墳場研究 Cemetery Study
    在沙田寶福山](https://www.facebook.com/hongkongcemetery/photos/pcb.401483303748379/401480893748620/?type=3&theater/)
    www.facebook.com
8.  [墳場研究 Cemetery Study
    在沙田寶福山](https://www.facebook.com/hongkongcemetery/photos/pcb.400578730505503/400578437172199/?type=3&theater/)
    www.facebook.com
9.  [墳場研究 Cemetery Study
    在沙田寶福山](https://www.facebook.com/hongkongcemetery/photos/a.247397519156959/328437581052952/?type=3&theater/)
    www.facebook.com