[Mount_erebus_hg.jpg](https://zh.wikipedia.org/wiki/File:Mount_erebus_hg.jpg "fig:Mount_erebus_hg.jpg")

**埃里伯斯火山**（）位於[南極洲](../Page/南極洲.md "wikilink")[羅斯島](../Page/羅斯島.md "wikilink")，是南極洲上第二高的[火山](../Page/火山.md "wikilink")（僅次於[西德利火山](../Page/西德利火山.md "wikilink")），海拔高度3795米，終年和冰雪相伴。它也是地球上己知區域最南端的一座[活火山](../Page/活火山.md "wikilink")。<ref name="gvp">

` `</ref>`埃里伯斯火山自130萬年前活躍至今。`\[1\]`現時，埃里伯斯火山觀察站由`[`新墨西哥礦業及科技學院營運`](../Page/新墨西哥礦業及科技學院.md "wikilink")`。`<ref>
` `</ref>

## 地質

[Anorthoclase-219058.jpg](https://zh.wikipedia.org/wiki/File:Anorthoclase-219058.jpg "fig:Anorthoclase-219058.jpg")晶體\]\]
現時，埃里伯斯火山是全南極洲最活躍的火山。該山山體内有永久對流的[響岩岩漿](../Page/響岩.md "wikilink")[熔岩湖](../Page/熔岩湖.md "wikilink")，是世上僅有的五個永久熔岩湖之一。

火山有一个活动的喷火口，广约800米，深275米；还有两个熄灭的喷火口。1900年和1902年有过火山活动，熄灭的喷火口存有大量硫磺。
這座火山曾在1947年和1995年又各噴發了一次，在噴發出來的蒸氣中，發現了極少量的純金。
埃里伯斯火山上有好幾個噴氣孔，[蒸氣噴出不久就被冷凝](../Page/蒸氣.md "wikilink")，凍成形庇各異的蒸氣柱，這個火山噴出的煙霧含有[硫](../Page/硫.md "wikilink")。

## 历史

### 发现和命名

1841年，[詹姆斯·克拉克·羅斯和他的探險隊](../Page/詹姆斯·克拉克·羅斯.md "wikilink")（包括[達爾文之友](../Page/達爾文.md "wikilink")[約瑟夫·道爾頓·胡克](../Page/約瑟夫·道爾頓·胡克.md "wikilink")）發現了此火山。\[2\]羅斯以其[英國皇家海軍艦隊旗下其中一艘船隻](../Page/英國皇家海軍.md "wikilink")“厄瑞玻斯號”為此火山命名。該船由得名自希臘神祇[厄瑞玻斯](../Page/厄瑞玻斯.md "wikilink")，他在希臘神話中是黑暗的化身，[卡俄斯的兒子](../Page/卡俄斯.md "wikilink")。\[3\]

### 古蹟

據[南極條約體系](../Page/南極條約體系.md "wikilink")，共有三座古蹟和此火山有關，其中有兩座隸屬於此山範圍之内。其中，位於此山附近的第73號古蹟為一不鏽鋼十字架，建於1987年，用以紀念1979年的空難遇害者。另外的兩處古蹟則為第89號和第90號古蹟，它們都是1912年間[羅拔·史葛所帶領的](../Page/羅拔·史葛.md "wikilink")[新地探險隊所建的營地](../Page/新地探險.md "wikilink")。他們在當地編製地圖，以及采集了一些地質樣本。\[4\]

### 紐西蘭航空901號班機空難

[紐西蘭航空901號班機是一班定期的南極洲觀光航班](../Page/紐西蘭航空901號班機.md "wikilink")，每班航機均會由[紐西蘭](../Page/紐西蘭.md "wikilink")[奧克蘭機場出發](../Page/奧克蘭機場.md "wikilink")，在南極洲觀光回到[基督城國際機場加油](../Page/基督城國際機場.md "wikilink")，最後回到起點奧克蘭機場。\[5\]
1979年11月28日，[紐西蘭航空901號班機在飛行途中撞向埃里伯斯火山](../Page/紐西蘭航空901號班機.md "wikilink")，機上237名乘客和20位機組人員全部罹難，是紐西蘭航空歷來最嚴重的空難。至今，大部分航機殘骸仍留在埃里伯斯火山上。平常飛機殘骸被埋在冰雪之下，但當天氣回暖，冰雪融化的時候，殘骸會露出來，在空中清晰可見。\[6\]因此，紐西蘭航空1980年2月17號後取消了所有來往南極洲的航班。

## 參考

[Category:南極洲火山](../Category/南極洲火山.md "wikilink")

1.

2.  Ross, *Voyage to the Southern Seas*, vol. i, pp. 216–8.

3.  Hesiod, *Theogony*
    [116–124](http://www.perseus.tufts.edu/hopper/text?doc=Hes.+Th.+116).

4.

5.  Holmes P., Daughters of Erebus, Hachette New Zealand Ltd (2011), p.
    31

6.