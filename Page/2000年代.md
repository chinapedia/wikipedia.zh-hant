[2000s_decade_montage3.png](https://zh.wikipedia.org/wiki/File:2000s_decade_montage3.png "fig:2000s_decade_montage3.png")
**2000年代**是一個[二十世紀與](../Page/二十世紀.md "wikilink")[廿一世紀的](../Page/廿一世紀.md "wikilink")[年代](../Page/年代.md "wikilink")，從[2000年](../Page/2000年.md "wikilink")[1月1日開始](../Page/1月1日.md "wikilink")，於[2009年](../Page/2009年.md "wikilink")12月31日結束。[2000年也是](../Page/2000年.md "wikilink")[第2千年與](../Page/第2千年.md "wikilink")[二十世紀的最後一年](../Page/二十世紀.md "wikilink")，因為[公元是從](../Page/公元.md "wikilink")[1年開始的](../Page/1年.md "wikilink")，並沒有零年。

2000年代是自[1850年有现代测量数据以来最热的](../Page/1850年.md "wikilink")10年，其间全球经历了诸多气候极端事件，包括在欧洲和俄罗斯出现的[热浪](../Page/热浪.md "wikilink")，美国的[卡特里娜飓风](../Page/卡特里娜飓风.md "wikilink")，亚马孙平原、澳大利亚和东非的干旱等，所有极端事件共导致约37万人死亡\[1\]
。

## 大事記

[20090427_5475_Shanghai.jpg](https://zh.wikipedia.org/wiki/File:20090427_5475_Shanghai.jpg "fig:20090427_5475_Shanghai.jpg")20年後中國經濟崛起成為第二大經濟體\]\]
[HTC_One_X_running_Android_4_Ice_Cream_Sandwich_20120427.JPG](https://zh.wikipedia.org/wiki/File:HTC_One_X_running_Android_4_Ice_Cream_Sandwich_20120427.JPG "fig:HTC_One_X_running_Android_4_Ice_Cream_Sandwich_20120427.JPG")
[IraqWarHeader.jpg](https://zh.wikipedia.org/wiki/File:IraqWarHeader.jpg "fig:IraqWarHeader.jpg")
[Meulaboh_Hovercraft_050110-N-7586B-120.jpg](https://zh.wikipedia.org/wiki/File:Meulaboh_Hovercraft_050110-N-7586B-120.jpg "fig:Meulaboh_Hovercraft_050110-N-7586B-120.jpg")
[North_Korea_nuclear.svg](https://zh.wikipedia.org/wiki/File:North_Korea_nuclear.svg "fig:North_Korea_nuclear.svg")
[Barack_Obama_speaks_on_education_to_Hispanic_Chamber_of_Commerce_3-10-09_2.jpg](https://zh.wikipedia.org/wiki/File:Barack_Obama_speaks_on_education_to_Hispanic_Chamber_of_Commerce_3-10-09_2.jpg "fig:Barack_Obama_speaks_on_education_to_Hispanic_Chamber_of_Commerce_3-10-09_2.jpg")成為歷史上第一位[非裔美國人](../Page/非裔美國人.md "wikilink")[美國總統](../Page/美國總統.md "wikilink")\]\]

### 科學和技術

  - 電腦軟件出現多番革新，[社群網站和分享網站興起](../Page/社群網站.md "wikilink")。
  - 觸控手機開始普及。

### 戰爭與政治

  - [2000年](../Page/2000年.md "wikilink")——[第十屆中華民國總統大選](../Page/2000年中華民國總統選舉.md "wikilink")，出現首次[政黨輪替](../Page/政黨輪替.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")——[香港立法會選舉](../Page/2000年香港立法會選舉.md "wikilink")。
  - [2000年](../Page/2000年.md "wikilink")——[美國總統選舉](../Page/2000年美國總統選舉.md "wikilink")，因[佛羅里達州的計票爭議導致選舉結果延緩三十五天後由](../Page/佛羅里達州.md "wikilink")[美國最高法院作出裁決](../Page/美國最高法院.md "wikilink")。
  - [2001年](../Page/2001年.md "wikilink")——[反恐戰爭](../Page/反恐戰爭.md "wikilink")。
  - [2001年](../Page/2001年.md "wikilink")——[菲律賓](../Page/菲律賓.md "wikilink")[第二次人民力量革命](../Page/人民力量革命_\(2001年\).md "wikilink")。
  - [2001年](../Page/2001年.md "wikilink")——[阿富汗戰爭](../Page/阿富汗戰爭_\(2001年\).md "wikilink")。
  - [2002年](../Page/2002年.md "wikilink")——[法國總統選舉](../Page/2002年法國總統選舉.md "wikilink")，[極右派](../Page/極右派.md "wikilink")[民族陣線首次進入第二輪投票](../Page/民族陣線_\(法國\).md "wikilink")。
  - [2002年](../Page/2002年.md "wikilink")——[東帝汶正式獨立](../Page/東帝汶.md "wikilink")。
  - [2002年](../Page/2002年.md "wikilink")——[中國共產黨第十六次全國代表大會](../Page/中國共產黨第十六次全國代表大會.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")——[伊拉克戰爭](../Page/伊拉克戰爭.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")——[喬治亞](../Page/喬治亞.md "wikilink")[鬱金香革命](../Page/鬱金香革命.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")——[香港立法會選舉](../Page/2004年香港立法會選舉.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")——[歐洲聯盟會員國由](../Page/歐洲聯盟.md "wikilink")15國增加為25國。
  - [2004年](../Page/2004年.md "wikilink")——[烏克蘭](../Page/烏克蘭.md "wikilink")[橙色革命](../Page/橙色革命.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")——[第十一屆中華民國總統大選](../Page/2004年中華民國總統選舉.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")——[美國總統選舉](../Page/2004年美國總統選舉.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")——[中華民國立法委員選舉](../Page/2004年中華民國立法委員選舉.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")——[中華人民共和國頒佈](../Page/中華人民共和國.md "wikilink")《[反分裂國家法](../Page/反分裂國家法.md "wikilink")》。
  - [2005年](../Page/2005年.md "wikilink")——[吉爾吉斯](../Page/吉爾吉斯.md "wikilink")[檸檬革命](../Page/檸檬革命.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")——[蒙特內哥羅正式獨立](../Page/蒙特內哥羅.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")——[夏雨行動](../Page/夏雨行動.md "wikilink")、[以黎衝突](../Page/2006年以黎衝突.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")——[泰國軍事政變](../Page/2006年泰國軍事政變.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")——[-{zh-hans:朝鲜;
    zh-hant:北韓;}-核試驗](../Page/2006年北韓核試驗.md "wikilink")、[六方會談](../Page/六方會談.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")——[法國總統選舉](../Page/2007年法國總統選舉.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")——[中國共產黨第十七次全國代表大會](../Page/中國共產黨第十七次全國代表大會.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")——[香港行政長官選舉](../Page/2007年香港行政長官選舉.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")——[中華民國立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")
  - [2008年](../Page/2008年.md "wikilink")——[科索沃獨立](../Page/科索沃獨立.md "wikilink")，並未獲國際普遍承認。
  - [2008年](../Page/2008年.md "wikilink")——[第十二屆中華民國總統大選](../Page/2008年中華民國總統選舉.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")——[香港立法會選舉](../Page/2008年香港立法會選舉.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")——[美國總統選舉](../Page/2008年美國總統選舉.md "wikilink")，[巴拉克·奧巴馬成為歷史上第一位](../Page/巴拉克·奧巴馬.md "wikilink")[非裔美國人總統](../Page/非裔美國人.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")——[伊朗綠色革命](../Page/伊朗綠色革命.md "wikilink")。

### 經濟

  - [2002年](../Page/2002年.md "wikilink")——[歐盟](../Page/歐盟.md "wikilink")12個會員國正式採用[歐元為流通貨幣](../Page/歐元.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")——[中國解除](../Page/中國.md "wikilink")[人民幣與](../Page/人民幣.md "wikilink")[美金的聯繫匯率](../Page/美金.md "wikilink")，[人民币升值进程开始](../Page/人民币汇率.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")——[美國爆发](../Page/美國.md "wikilink")[次級房貸風暴](../Page/次貸危機.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")－[2009年](../Page/2009年.md "wikilink")——[全球金融海嘯](../Page/2007年－2012年環球金融危機.md "wikilink")。

### 文化及娛樂

  - [2000年](../Page/2000年.md "wikilink")——[澳洲](../Page/澳洲.md "wikilink")[雪梨奧運](../Page/2000年夏季奧林匹克運動會.md "wikilink")。
  - [2002年](../Page/2002年.md "wikilink")——[韓](../Page/南韓.md "wikilink")[日](../Page/日本.md "wikilink")[世界盃足球賽](../Page/2002年世界盃足球賽.md "wikilink")。
  - [2004年](../Page/2004年.md "wikilink")——[希臘](../Page/希臘.md "wikilink")[雅典奧運](../Page/2004年夏季奧林匹克運動會.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")——[世界棒球經典賽](../Page/2006年世界棒球經典賽.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")——[德國](../Page/德國.md "wikilink")[世界盃足球賽](../Page/2006年世界盃足球賽.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")——[中國](../Page/中華人民共和國.md "wikilink")[北京奧運](../Page/2008年夏季奧林匹克運動會.md "wikilink")

### 灾难

  - [2000年](../Page/2000年.md "wikilink")——[俄国](../Page/俄国.md "wikilink")[库斯克号爆炸事件](../Page/库尔斯克号.md "wikilink")
  - [2001年](../Page/2001年.md "wikilink")——[美国](../Page/美国.md "wikilink")[九一一事件](../Page/九一一事件.md "wikilink")
  - [2002年](../Page/2002年.md "wikilink")——[中华航空611号班机空难](../Page/中华航空611号班机空难.md "wikilink")
  - 2002年——[印尼](../Page/印尼.md "wikilink")[峇里島爆炸案](../Page/峇里島爆炸案.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")——[美国](../Page/美国.md "wikilink")[哥伦比亚号太空梭爆炸](../Page/哥伦比亚号太空梭灾难.md "wikilink")
  - 2003年——[-{zh-hans:韩国;zh-hant:南韓}-](../Page/韩国.md "wikilink")[大邱地铁纵火案](../Page/2003年大邱地铁纵火案.md "wikilink")
  - 2003年——[SARS全球大流行](../Page/SARS.md "wikilink")
  - 2003年——[伊朗](../Page/伊朗.md "wikilink")[巴姆古城大地震](../Page/巴姆古城.md "wikilink")
  - 2003年至今——[达尔富尔冲突](../Page/达尔富尔冲突.md "wikilink")
  - [2004年](../Page/2004年.md "wikilink")——[香港地鐵金鐘站縱火案](../Page/2004年香港地鐵縱火案.md "wikilink")
  - 2004年——[西班牙](../Page/西班牙.md "wikilink")[马德里三一一爆炸案](../Page/马德里三一一爆炸案.md "wikilink")
  - 2004年——[印度洋大地震引發南亞海嘯](../Page/2004年印度洋大地震.md "wikilink")
  - [2005年](../Page/2005年.md "wikilink")——[日本](../Page/日本.md "wikilink")[JR福知山线出轨事故](../Page/JR福知山线出轨事故.md "wikilink")
  - 2005年——[英国](../Page/英国.md "wikilink")[伦敦连续爆炸案](../Page/2005年7月伦敦连串爆炸事件.md "wikilink")
  - 2005年——[卡翠娜飓风侵袭](../Page/卡翠娜飓风.md "wikilink")[美国南部](../Page/美国南部.md "wikilink")，重创大城[纽奥良](../Page/纽奥良.md "wikilink")。
  - 2005年——[克什米尔大地震](../Page/2005年克什米尔大地震.md "wikilink")
  - 2005年——[H5N1](../Page/H5N1.md "wikilink")[禽流感](../Page/禽流感.md "wikilink")
  - [2006年](../Page/2006年.md "wikilink")——[孟买火车连续爆炸案](../Page/2006年孟买火车连环爆炸案.md "wikilink")
  - [2007年](../Page/2007年.md "wikilink")——[巴西天马航空3054号班机空难](../Page/巴西天马航空3054号班机空难.md "wikilink")
  - [2008年](../Page/2008年.md "wikilink")——[缅甸风灾](../Page/特强气旋风暴纳尔吉斯.md "wikilink")
  - 2008年——[中国](../Page/中华人民共和国.md "wikilink")[四川](../Page/四川.md "wikilink")[汶川大地震](../Page/汶川大地震.md "wikilink")
  - 2008年——[孟买连环恐怖攻击](../Page/2008年孟买连环恐怖袭击.md "wikilink")
  - [2009年](../Page/2009年.md "wikilink")——[H1N1流感大流行](../Page/2009年H1N1流感大流行.md "wikilink")
  - 2009年——[法国航空447号班机空难](../Page/法国航空447号班机空难.md "wikilink")
  - 2009年——[台湾八八水灾](../Page/八八水灾.md "wikilink")

## 流行文化

### 音樂

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/陳慧琳.md" title="wikilink">陳慧琳</a>—<a href="../Page/花花宇宙.md" title="wikilink">花花宇宙</a>(2000年)</li>
<li><a href="../Page/吳宗憲.md" title="wikilink">吳宗憲</a>＆<a href="../Page/溫嵐.md" title="wikilink">溫嵐</a>－<a href="../Page/屋頂.md" title="wikilink">屋頂</a>（2000年）</li>
<li><a href="../Page/王菲.md" title="wikilink">王菲</a>-<a href="../Page/笑忘書.md" title="wikilink">笑忘書</a>(2000年)</li>
<li><a href="../Page/林憶蓮.md" title="wikilink">林憶蓮</a>－<a href="../Page/至少還有你.md" title="wikilink">至少還有你</a>（2000年）</li>
<li><a href="../Page/孫燕姿.md" title="wikilink">孫燕姿</a>－<a href="../Page/天黑黑.md" title="wikilink">天黑黑</a>（2000年）</li>
<li><a href="../Page/谢霆锋.md" title="wikilink">谢霆锋</a>－<a href="../Page/活着Viva.md" title="wikilink">活着Viva</a>（2000年）</li>
<li><a href="../Page/Twins.md" title="wikilink">Twins</a>—<a href="../Page/Twins_(EP).md" title="wikilink">明愛愛戀補習社</a>(2001年)</li>
<li><a href="../Page/Twins.md" title="wikilink">Twins</a>—<a href="../Page/Twins_(EP).md" title="wikilink">女校男生</a>(2001年)</li>
<li><a href="../Page/F4.md" title="wikilink">F4</a>－<a href="../Page/流星雨.md" title="wikilink">流星雨</a>（2001年）</li>
<li><a href="../Page/Twins.md" title="wikilink">Twins</a>—<a href="../Page/愛情當入樽.md" title="wikilink">戀愛大過天</a>(2001年)</li>
<li><a href="../Page/S.H.E.md" title="wikilink">S.H.E</a>－<a href="../Page/熱帶雨林_(歌曲).md" title="wikilink">熱帶雨林</a>（2002年）</li>
<li><a href="../Page/梁靜茹.md" title="wikilink">梁靜茹</a>－<a href="../Page/分手快樂.md" title="wikilink">分手快樂</a>（2002年）</li>
<li><a href="../Page/范逸臣.md" title="wikilink">范逸臣</a>－<a href="../Page/I_Believe.md" title="wikilink">I Believe</a>（2002年）</li>
<li><a href="../Page/李聖傑.md" title="wikilink">李聖傑</a>－<a href="../Page/癡心絕對.md" title="wikilink">癡心絕對</a>（2002年）</li>
<li><a href="../Page/Twins.md" title="wikilink">Twins</a>—<a href="../Page/Happy_Together.md" title="wikilink">風箏與風</a>(2002年)</li>
<li><a href="../Page/5566.md" title="wikilink">5566</a>－<a href="../Page/我難過.md" title="wikilink">我難過</a>（2002年）</li>
<li><a href="../Page/蔡依林.md" title="wikilink">蔡依林</a>－<a href="../Page/看我72變.md" title="wikilink">看我72變</a>（2003年）</li>
<li><a href="../Page/王菲.md" title="wikilink">王菲</a>-<a href="../Page/不留.md" title="wikilink">不留</a>（2003年）</li>
<li><a href="../Page/S.H.E.md" title="wikilink">S.H.E</a>－<a href="../Page/Super_Star_(歌曲).md" title="wikilink">Super Star</a>（2003年）</li>
<li><a href="../Page/孫燕姿.md" title="wikilink">孫燕姿</a>－<a href="../Page/我不難過.md" title="wikilink">我不難過</a>（2003年）</li>
<li>手牽手抗SARS歌（2003年）</li>
<li><a href="../Page/Twins.md" title="wikilink">Twins</a>－<a href="../Page/下一站天后.md" title="wikilink">下一站天后</a>（2003年）</li>
<li><a href="../Page/戴愛玲.md" title="wikilink">戴愛玲</a>－<a href="../Page/對的人.md" title="wikilink">對的人</a>（2003年）</li>
<li><a href="../Page/杜德偉.md" title="wikilink">杜德偉</a>－<a href="../Page/脫掉.md" title="wikilink">脫掉</a>（2004年）</li>
<li><a href="../Page/蔡依林.md" title="wikilink">蔡依林</a>－<a href="../Page/倒帶.md" title="wikilink">倒帶</a>（2004年）</li>
<li><a href="../Page/王心凌.md" title="wikilink">王心凌</a>－<a href="../Page/愛你.md" title="wikilink">愛你</a>（2004年）</li>
<li><a href="../Page/林依晨.md" title="wikilink">林依晨</a>－<a href="../Page/孤單北半球.md" title="wikilink">孤單北半球</a>（2004年）</li>
<li><a href="../Page/陳綺貞.md" title="wikilink">陳綺貞</a>－<a href="../Page/旅行的意義.md" title="wikilink">旅行的意義</a>（2004年）</li>
<li><a href="../Page/林俊傑.md" title="wikilink">林俊傑</a>－<a href="../Page/第二天堂.md" title="wikilink">江南</a>（2004年）</li>
<li><a href="../Page/羅志祥.md" title="wikilink">羅志祥</a>＆<a href="../Page/徐熙娣.md" title="wikilink">徐熙娣</a>－<a href="../Page/戀愛達人.md" title="wikilink">戀愛達人</a>（2004年）</li>
<li><a href="../Page/信樂團.md" title="wikilink">信樂團</a>＆<a href="../Page/戴愛玲.md" title="wikilink">戴愛玲</a>－<a href="../Page/千年之戀.md" title="wikilink">千年之戀</a>（2004年）</li>
</ul></td>
<td><ul>
<li><a href="../Page/楊丞琳.md" title="wikilink">楊丞琳</a>－<a href="../Page/曖昧.md" title="wikilink">曖昧</a>（2005年）</li>
<li><a href="../Page/五月天.md" title="wikilink">五月天</a>－<a href="../Page/知足.md" title="wikilink">知足</a>（2005年）</li>
<li><a href="../Page/王啟文.md" title="wikilink">王啟文</a>－<a href="../Page/老鼠愛大米.md" title="wikilink">老鼠愛大米</a>（2005年）</li>
<li><a href="../Page/光良.md" title="wikilink">光良</a>－<a href="../Page/童話_(專輯).md" title="wikilink">童話</a>（2005年）</li>
<li><a href="../Page/范瑋琪.md" title="wikilink">范瑋琪</a>＆<a href="../Page/張韶涵.md" title="wikilink">張韶涵</a>－<a href="../Page/如果的事.md" title="wikilink">如果的事</a>（2005年）</li>
<li><a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>＆<a href="../Page/費玉清.md" title="wikilink">費玉清</a>－<a href="../Page/千里之外.md" title="wikilink">千里之外</a>（2006年）</li>
<li><a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>－<a href="../Page/聽媽媽的話.md" title="wikilink">聽媽媽的話</a>（2006年）</li>
<li><a href="../Page/蔡依林.md" title="wikilink">蔡依林</a>－<a href="../Page/舞孃_(歌曲).md" title="wikilink">舞孃</a>（2006年）</li>
<li><a href="../Page/張懸.md" title="wikilink">張懸</a>－<a href="../Page/寶貝.md" title="wikilink">寶貝</a>（2006年）</li>
<li><a href="../Page/蘇打綠.md" title="wikilink">蘇打綠</a>－<a href="../Page/小情歌.md" title="wikilink">小情歌</a>（2006年）</li>
<li><a href="../Page/曹格.md" title="wikilink">曹格</a>－<a href="../Page/背叛.md" title="wikilink">背叛</a>（2006年）</li>
<li><a href="../Page/曹格.md" title="wikilink">曹格</a>＆<a href="../Page/卓文萱.md" title="wikilink">卓文萱</a>－<a href="../Page/梁山伯與茱麗葉.md" title="wikilink">梁山伯與茱麗葉</a>（2006年）</li>
<li><a href="../Page/張惠妹.md" title="wikilink">張惠妹</a>－<a href="../Page/我要快樂.md" title="wikilink">我要快樂</a>（2006年）</li>
<li><a href="../Page/陶喆.md" title="wikilink">陶喆</a>＆<a href="../Page/蔡依林.md" title="wikilink">蔡依林</a>－<a href="../Page/今天妳要嫁給我.md" title="wikilink">今天妳要嫁給我</a>（2006年）</li>
<li><a href="../Page/A-Lin.md" title="wikilink">A-Lin</a>－<a href="../Page/失戀無罪.md" title="wikilink">失戀無罪</a>（2006年）</li>
<li><a href="../Page/伍佰.md" title="wikilink">伍佰</a>－<a href="../Page/你是我的花朵.md" title="wikilink">你是我的花朵</a>（2006年）</li>
<li><a href="../Page/五月天.md" title="wikilink">五月天</a>＆<a href="../Page/陳綺貞.md" title="wikilink">陳綺貞</a>－<a href="../Page/私奔到月球.md" title="wikilink">私奔到月球</a>（2007年）</li>
<li><a href="../Page/五月天.md" title="wikilink">五月天</a>－<a href="../Page/你不是真正的快樂.md" title="wikilink">你不是真正的快樂</a>（2008年）</li>
<li><a href="../Page/范逸臣.md" title="wikilink">范逸臣</a>－<a href="../Page/無樂不作.md" title="wikilink">無樂不作</a>（2008年）</li>
<li><a href="../Page/大嘴巴.md" title="wikilink">大嘴巴</a>－<a href="../Page/永遠在身邊.md" title="wikilink">永遠在身邊</a>（2008年）</li>
<li><a href="../Page/方炯鑌.md" title="wikilink">方炯鑌</a>－<a href="../Page/壞人.md" title="wikilink">壞人</a>（2008年）</li>
<li><a href="../Page/梁靜茹.md" title="wikilink">梁靜茹</a>－<a href="../Page/靜茹＆情歌-別再為他流淚.md" title="wikilink">沒有如果</a>（2009年）</li>
<li><a href="../Page/徐佳瑩.md" title="wikilink">徐佳瑩</a>－<a href="../Page/身騎白馬.md" title="wikilink">身騎白馬</a>（2009年）</li>
</ul></td>
</tr>
</tbody>
</table>

### 電影

  - [勝者為王](../Page/勝者為王_\(2000年電影\).md "wikilink")(2000年)
  - [臥虎藏龍](../Page/臥虎藏龍_\(電影\).md "wikilink")（2000年）
  - [麥兜故事](../Page/麥兜故事.md "wikilink")（2001年）
  - [少林足球](../Page/少林足球.md "wikilink")（2001年）
  - [藍宇](../Page/藍宇.md "wikilink")（2001年）
  - [無間道](../Page/無間道.md "wikilink")（2002年）
  - [下一站…天后](../Page/下一站…天后.md "wikilink")(2003年)
  - [向左走·向右走](../Page/向左走·向右走_\(電影\).md "wikilink")（2003年）
  - [新警察故事](../Page/新警察故事.md "wikilink")（2004年）
  - [功夫](../Page/功夫_\(電影\).md "wikilink")（2004年）
  - [天邊一朵雲](../Page/天邊一朵雲.md "wikilink")（2005年）
  - [父子](../Page/父子_\(電影\).md "wikilink")（2006年）
  - [盛夏光年](../Page/盛夏光年.md "wikilink")（2006年）
  - [色，戒](../Page/色，戒.md "wikilink")（2007年）
  - [投名狀](../Page/投名狀.md "wikilink")（2007年）
  - [長江七號](../Page/長江七號.md "wikilink")（2008年）
  - [海角七號](../Page/海角七號.md "wikilink")（2008年）
  - [葉問](../Page/葉問_\(電影\).md "wikilink")（2008年）
  - [風聲](../Page/風聲_\(電影\).md "wikilink")（2009年）
  - [十月圍城](../Page/十月圍城.md "wikilink")（2009年）

### 電視

#### 劇集

  - [男親女愛](../Page/男親女愛.md "wikilink")(2000年)
  - [麻辣鮮師](../Page/麻辣鮮師.md "wikilink")（2000－2004年）
  - [流星花園](../Page/流星花園_\(台灣電視劇\).md "wikilink")（2001年）
  - [貧窮貴公子](../Page/貧窮貴公子_\(台灣電視劇\).md "wikilink")（2001年）
  - [薰衣草](../Page/薰衣草_\(台灣偶像劇\).md "wikilink")（2001年）
  - [MVP情人](../Page/MVP情人.md "wikilink")（2002年）
  - [薔薇之戀](../Page/薔薇之戀.md "wikilink")（2003年）
  - [西街少年](../Page/西街少年.md "wikilink")（2003年）
  - [紫禁之巔](../Page/紫禁之巔.md "wikilink")（2004年）
  - [天國的嫁衣](../Page/天國的嫁衣.md "wikilink")（2004年）
  - [格鬥天王](../Page/格鬥天王_\(電視劇\).md "wikilink")（2005年）
  - [惡魔在身邊](../Page/惡魔在身邊.md "wikilink")（2005年）
  - [惡作劇之吻](../Page/惡作劇之吻.md "wikilink")（2005年）
  - [終極一班](../Page/終極一班.md "wikilink")（2005年）
  - [王子變青蛙](../Page/王子變青蛙.md "wikilink")（2005年）
  - [花樣少年少女](../Page/花樣少年少女.md "wikilink")（2006年）
  - [轉角\*遇到愛](../Page/轉角*遇到愛.md "wikilink")（2007年）
  - [公主小妹](../Page/公主小妹.md "wikilink")（2007年）
  - [鬥牛，要不要](../Page/鬥牛，要不要.md "wikilink")（2007年）
  - [命中注定我愛你](../Page/命中注定我愛你.md "wikilink")（2008年）
  - [敗犬女王](../Page/敗犬女王.md "wikilink")（2009年）
  - [痞子英雄](../Page/痞子英雄.md "wikilink")（2009年）
  - [下一站，幸福](../Page/下一站，幸福.md "wikilink")（2009年）
  - [神秘博士](../Page/神秘博士.md "wikilink")（2005年－至今）

#### 卡通動畫

  - [小魔女Doremi](../Page/小魔女Doremi.md "wikilink")
  - [數碼寶貝](../Page/數碼寶貝.md "wikilink")
  - [遊戲王](../Page/遊戲王.md "wikilink")
  - [網球王子](../Page/網球王子.md "wikilink")
  - [航海王](../Page/航海王.md "wikilink")
  - [我們這一家](../Page/我們這一家.md "wikilink")
  - [戰鬥陀螺](../Page/戰鬥陀螺.md "wikilink")
  - [火影忍者](../Page/火影忍者.md "wikilink")
  - [神奇寶貝](../Page/神奇寶貝.md "wikilink")
  - [魔法咪路咪路](../Page/魔法咪路咪路.md "wikilink")
  - [Keroro軍曹](../Page/Keroro軍曹_\(電視動畫\).md "wikilink")
  - [家庭教師HITMAN REBORN\!](../Page/家庭教師HITMAN_REBORN!.md "wikilink")
  - [監獄兔](../Page/監獄兔.md "wikilink")
  - [黑執事](../Page/黑執事.md "wikilink")
  - [海綿寶寶](../Page/海綿寶寶.md "wikilink")

#### 綜藝節目

  - [歡喜玉玲龍](../Page/歡喜玉玲龍.md "wikilink")
  - [少年特攻隊](../Page/少年特攻隊.md "wikilink")
  - [超級星光大道](../Page/超級星光大道.md "wikilink")
  - [超級偶像](../Page/超級偶像.md "wikilink")
  - [食尚玩家](../Page/食尚玩家.md "wikilink")

## 名人

### 世界領導者

  - [亞洲](../Page/亞洲.md "wikilink")、[大洋洲](../Page/大洋洲.md "wikilink")
      - [李登輝](../Page/李登輝.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [萧万长](../Page/萧万长.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [陳水扁](../Page/陳水扁.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [唐飞](../Page/唐飞.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [张俊雄](../Page/张俊雄.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [游锡堃](../Page/游锡堃.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [谢长廷](../Page/谢长廷.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [苏贞昌](../Page/苏贞昌.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [馬英九](../Page/馬英九.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [刘兆玄](../Page/刘兆玄.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [吴敦义](../Page/吴敦义.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [王金平](../Page/王金平.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")
      - [明仁天皇](../Page/明仁天皇.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [森喜朗](../Page/森喜朗.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [小泉純一郎](../Page/小泉純一郎.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [安倍晋三](../Page/安倍晋三.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [福田康夫](../Page/福田康夫.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [麻生太郎](../Page/麻生太郎.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [鳩山由紀夫](../Page/鳩山由紀夫.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [盧武鉉](../Page/盧武鉉.md "wikilink")，[韓國](../Page/韓國.md "wikilink")
      - [李明博](../Page/李明博.md "wikilink")，[韓國](../Page/韓國.md "wikilink")
      - [金正日](../Page/金正日.md "wikilink")，[朝鮮](../Page/朝鮮.md "wikilink")
      - [阿羅約夫人](../Page/阿羅約夫人.md "wikilink")，[菲律賓](../Page/菲律賓.md "wikilink")
      - [李顯龍](../Page/李顯龍.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")
      - [梅嘉娃蒂](../Page/梅嘉娃蒂.md "wikilink")，[印尼](../Page/印尼.md "wikilink")
      - [蘇西洛·班邦·尤多約諾](../Page/蘇西洛·班邦·尤多約諾.md "wikilink")，[印尼](../Page/印尼.md "wikilink")
      - [阿都拉·巴達威](../Page/阿都拉·巴達威.md "wikilink")，[馬來西亞](../Page/馬來西亞.md "wikilink")
      - [纳吉·阿都拉萨](../Page/纳吉·阿都拉萨.md "wikilink")，[馬來西亞](../Page/馬來西亞.md "wikilink")
      - [塔信·欽那瓦](../Page/塔信·欽那瓦.md "wikilink")，[泰國](../Page/泰國.md "wikilink")
      - [曼莫漢·辛格](../Page/曼莫漢·辛格.md "wikilink")，[印度](../Page/印度.md "wikilink")
      - [佩爾韋茲·穆沙拉夫](../Page/佩爾韋茲·穆沙拉夫.md "wikilink")，[巴基斯坦](../Page/巴基斯坦.md "wikilink")
      - [沙納納·古斯芒](../Page/沙納納·古斯芒.md "wikilink")，[東帝汶](../Page/東帝汶.md "wikilink")
      - [約翰·霍華德](../Page/約翰·霍華德.md "wikilink")，[澳大利亞](../Page/澳大利亞.md "wikilink")
      - [陸克文](../Page/陸克文.md "wikilink")，[澳大利亞](../Page/澳大利亞.md "wikilink")
      - [海倫·克拉克](../Page/海倫·克拉克.md "wikilink")，[紐西蘭](../Page/紐西蘭.md "wikilink")
      - [約翰·凱伊](../Page/約翰·凱伊.md "wikilink")，[紐西蘭](../Page/紐西蘭.md "wikilink")
      - [江澤民](../Page/江澤民.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")
      - [李鹏](../Page/李鹏.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")
      - [朱镕基](../Page/朱镕基.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")
      - [胡錦濤](../Page/胡錦濤.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")
      - [吴邦国](../Page/吴邦国.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")
      - [温家宝](../Page/温家宝.md "wikilink")，[中華人民共和國](../Page/中華人民共和國.md "wikilink")
  - [中東](../Page/中東.md "wikilink")
      - [卡爾札伊](../Page/卡爾札伊.md "wikilink")，[阿富汗](../Page/阿富汗.md "wikilink")
      - [夏隆](../Page/夏隆.md "wikilink")，[以色列](../Page/以色列.md "wikilink")
      - [艾胡德·奧爾默特](../Page/艾胡德·奧爾默特.md "wikilink")，[以色列](../Page/以色列.md "wikilink")
      - [阿拉法特](../Page/阿拉法特.md "wikilink")，[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")
      - [阿巴斯](../Page/阿巴斯.md "wikilink")，[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")
      - [海珊](../Page/海珊.md "wikilink")，[伊拉克](../Page/伊拉克.md "wikilink")
      - [艾哈邁迪-內賈德](../Page/艾哈邁迪-內賈德.md "wikilink")，[伊朗](../Page/伊朗.md "wikilink")
      - [穆巴拉克](../Page/穆巴拉克.md "wikilink")，[埃及](../Page/埃及.md "wikilink")

<!-- end list -->

  - [美洲](../Page/美洲.md "wikilink")
      - [喬治·W·布希](../Page/喬治·W·布希.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [巴拉克·歐巴馬](../Page/巴拉克·歐巴馬.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [保羅·馬丁](../Page/保羅·馬丁.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")
      - [史蒂芬·哈珀](../Page/史蒂芬·哈珀.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")
      - [比森特·福克斯](../Page/比森特·福克斯.md "wikilink")，[墨西哥](../Page/墨西哥.md "wikilink")
      - [費利佩·卡爾德龍](../Page/費利佩·卡爾德龍.md "wikilink")，[墨西哥](../Page/墨西哥.md "wikilink")
      - [馬丁·托里霍斯](../Page/馬丁·托里霍斯.md "wikilink")，[巴拿馬](../Page/巴拿馬.md "wikilink")
      - [菲德爾·卡斯楚](../Page/菲德爾·卡斯楚.md "wikilink")，[古巴](../Page/古巴.md "wikilink")
      - [勞爾·卡斯楚](../Page/勞爾·卡斯楚.md "wikilink")，[古巴](../Page/古巴.md "wikilink")
      - [盧拉·達·席爾瓦](../Page/盧拉·達·席爾瓦.md "wikilink")，[巴西](../Page/巴西.md "wikilink")
      - [內斯托爾·基什內爾](../Page/內斯托爾·基什內爾.md "wikilink")，[阿根廷](../Page/阿根廷.md "wikilink")
      - [克里斯蒂娜·费尔南德斯](../Page/克里斯蒂娜·费尔南德斯.md "wikilink")，[阿根廷](../Page/阿根廷.md "wikilink")
      - [烏戈·查維茲](../Page/烏戈·查維茲.md "wikilink")，[委內瑞拉](../Page/委內瑞拉.md "wikilink")
  - [歐洲](../Page/歐洲.md "wikilink")
      - [伊莉莎白二世](../Page/伊莉莎白二世.md "wikilink")，[英國](../Page/英國.md "wikilink")
      - [托尼·布萊爾](../Page/托尼·布萊爾.md "wikilink")，[英國](../Page/英國.md "wikilink")
      - [戈登·布朗](../Page/戈登·布朗.md "wikilink")，[英國](../Page/英國.md "wikilink")
      - [雅克·希拉克](../Page/雅克·希拉克.md "wikilink")，[法國](../Page/法國.md "wikilink")
      - [尼古拉·薩科齊](../Page/尼古拉·薩科齊.md "wikilink")，[法國](../Page/法國.md "wikilink")
      - [格哈特·施羅德](../Page/格哈特·施羅德.md "wikilink")，[德國](../Page/德國.md "wikilink")
      - [安格拉·梅克爾](../Page/安格拉·梅克爾.md "wikilink")，[德國](../Page/德國.md "wikilink")
      - [若望保祿二世](../Page/若望保祿二世.md "wikilink")，[梵蒂岡](../Page/梵蒂岡.md "wikilink")
      - [本篤十六世](../Page/本篤十六世.md "wikilink")，[梵蒂岡](../Page/梵蒂岡.md "wikilink")
      - [何塞·玛丽亚·阿斯纳尔](../Page/何塞·玛丽亚·阿斯纳尔.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")
      - [薩帕特羅](../Page/薩帕特羅.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")
      - [西爾維奧·貝盧斯科尼](../Page/西爾維奧·貝盧斯科尼.md "wikilink")，[義大利](../Page/義大利.md "wikilink")
      - [羅馬諾·普羅迪](../Page/羅馬諾·普羅迪.md "wikilink")，[義大利](../Page/義大利.md "wikilink")
      - [若澤·曼努埃爾·巴羅佐](../Page/若澤·曼努埃爾·巴羅佐.md "wikilink")，[葡萄牙](../Page/葡萄牙.md "wikilink")
      - [列昂尼德·库奇马](../Page/列昂尼德·库奇马.md "wikilink")，[烏克蘭](../Page/烏克蘭.md "wikilink")
      - [維克托·尤先科](../Page/維克托·尤先科.md "wikilink")，[烏克蘭](../Page/烏克蘭.md "wikilink")
      - [普京](../Page/普京.md "wikilink")，[俄羅斯](../Page/俄羅斯.md "wikilink")
      - [德米特里·阿纳托利耶维奇·梅德韦杰夫](../Page/德米特里·阿纳托利耶维奇·梅德韦杰夫.md "wikilink")，[俄羅斯](../Page/俄羅斯.md "wikilink")

<!-- end list -->

  - [非洲](../Page/非洲.md "wikilink")
      - [穆阿邁爾·卡達菲](../Page/穆阿邁爾·卡達菲.md "wikilink")，[利比亞](../Page/利比亞.md "wikilink")

### 科學家

### 運動員

  - [足球](../Page/足球.md "wikilink")
      - [貝克漢](../Page/大卫·贝克汉姆.md "wikilink")，[英國](../Page/英國.md "wikilink")
      - [罗纳尔迪尼奥](../Page/罗纳尔迪尼奥.md "wikilink"),
        [巴西](../Page/巴西.md "wikilink")
      - [羅納度](../Page/罗纳尔多.md "wikilink")，[巴西](../Page/巴西.md "wikilink")
      - [-{zh-hans:齐内丁·齐达内;zh-hk:施丹;zh-tw:席丹;}-](../Page/齐内丁·齐达内.md "wikilink")，[法國](../Page/法國.md "wikilink")
      - [-{zh-hans:蒂耶里·亨利;
        zh-hk:亨利;}-](../Page/蒂耶里·亨利.md "wikilink")，[法國](../Page/法國.md "wikilink")
      - [卡恩](../Page/奧利弗·卡恩.md "wikilink")，[德國](../Page/德國.md "wikilink")
      - [-{zh-hans:莱曼;zh-hk:列文;zh-tw:萊曼;}-](../Page/延斯·莱曼.md "wikilink")，[德國](../Page/德國.md "wikilink")
      - [-{zh-hans:米夏埃尔·巴拉克;zh-hk:波歷克;zh-tw:巴拉克;}-](../Page/米夏埃尔·巴拉克.md "wikilink")，[德國](../Page/德國.md "wikilink")
      - [菲利普·拉姆](../Page/菲利普·拉姆.md "wikilink"),
        [德国](../Page/德国.md "wikilink")
      - [巴斯蒂安·施魏因斯泰格](../Page/巴斯蒂安·施魏因斯泰格.md "wikilink"),
        [德国](../Page/德国.md "wikilink")
      - [-{zh-hans:米罗斯拉夫·克洛泽;zh-hk:高路斯;zh-tw:克洛斯;}-](../Page/米罗斯拉夫·克洛泽.md "wikilink")，[德國](../Page/德國.md "wikilink")
      - [-{zh-hans:詹路易吉·布冯;zh-hk:保方;zh-tw:布馮;}-](../Page/詹路易吉·布冯.md "wikilink")，[義大利](../Page/義大利.md "wikilink")
      - [法比奥·卡纳瓦罗](../Page/法比奥·卡纳瓦罗.md "wikilink"),
        [意大利](../Page/意大利.md "wikilink")
      - [保罗·马尔蒂尼](../Page/保罗·马尔蒂尼.md "wikilink"),
        [意大利](../Page/意大利.md "wikilink")
      - [达尼埃莱·德罗西](../Page/达尼埃莱·德罗西.md "wikilink"),
        [意大利](../Page/意大利.md "wikilink")
      - [-{zh-hans:安德烈亚·皮尔洛;zh-hk:派路;zh-tw:皮爾洛;}-](../Page/安德烈亚·皮尔洛.md "wikilink")，[義大利](../Page/義大利.md "wikilink")
      - [亚历山德罗·德尔·皮耶罗](../Page/亚历山德罗·德尔·皮耶罗.md "wikilink"),
        [義大利](../Page/義大利.md "wikilink")
      - [迈克尔·欧文](../Page/迈克尔·欧文.md "wikilink"),
        [英格兰](../Page/英格兰.md "wikilink")
      - [韦恩·鲁尼](../Page/韦恩·鲁尼.md "wikilink"),
        [英格兰](../Page/英格兰.md "wikilink")
      - [史提芬·謝拉特](../Page/史提芬·謝拉特.md "wikilink"),
        [英格兰](../Page/英格兰.md "wikilink")
      - [法蘭·林柏特](../Page/法蘭·林柏特.md "wikilink"),
        [英格兰](../Page/英格兰.md "wikilink")
      - [路德·范尼斯特鲁伊](../Page/路德·范尼斯特鲁伊.md "wikilink"),
        [荷兰](../Page/荷兰.md "wikilink")
      - [埃德加·戴维斯](../Page/埃德加·戴维斯.md "wikilink"),
        [荷兰](../Page/荷兰.md "wikilink")
      - [阿尔扬·罗本](../Page/阿尔扬·罗本.md "wikilink"),
        [荷兰](../Page/荷兰.md "wikilink")
      - [-{zh-hans:劳尔·冈萨雷斯;zh-hk:魯爾;zh-tw:勞爾;}-](../Page/魯爾·干沙利斯·白蘭高.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")
      - [菲戈](../Page/路易斯·菲戈.md "wikilink")，[葡萄牙](../Page/葡萄牙.md "wikilink")
      - [卡西利亚斯](../Page/伊克尔·卡西利亚斯.md "wikilink"),
        [西班牙](../Page/西班牙.md "wikilink")
      - [伊涅斯塔](../Page/安德烈斯·伊涅斯塔.md "wikilink"),
        [西班牙](../Page/西班牙.md "wikilink")
      - [哈维·埃尔南德斯](../Page/哈维·埃尔南德斯.md "wikilink"),
        [西班牙](../Page/西班牙.md "wikilink")
      - [卡莱斯·普约尔](../Page/卡莱斯·普约尔.md "wikilink"),
        [西班牙](../Page/西班牙.md "wikilink")
      - [基斯坦奴·朗拿度](../Page/基斯坦奴·朗拿度.md "wikilink"),
        [葡萄牙](../Page/葡萄牙.md "wikilink")
      - [-{zh-hans:安德烈·舍甫琴科;zh-hk:舒夫真高;zh-tw:謝夫成科;}-](../Page/安德烈·舍甫琴科.md "wikilink")，[烏克蘭](../Page/烏克蘭.md "wikilink")
      - [兹拉坦·伊布拉希莫维奇](../Page/兹拉坦·伊布拉希莫维奇.md "wikilink"),
        [瑞典](../Page/瑞典.md "wikilink")
      - [利昂内尔·梅西](../Page/利昂内尔·梅西.md "wikilink"),
        [阿根廷](../Page/阿根廷.md "wikilink")
      - [-{zh-hans:埃尔南·克雷斯波;zh-hk:基斯普;zh-tw:克雷斯波;}-](../Page/基斯普.md "wikilink")，[阿根廷](../Page/阿根廷.md "wikilink")
      - [萨缪埃尔·埃托奥](../Page/萨缪埃尔·埃托奥.md "wikilink"),
        [喀麦隆](../Page/喀麦隆.md "wikilink")
      - [迪迪埃·德罗巴](../Page/迪迪埃·德罗巴.md "wikilink"),
        [科特迪瓦](../Page/科特迪瓦.md "wikilink")
      - [中田英壽](../Page/中田英壽.md "wikilink")，[日本](../Page/日本.md "wikilink")

<!-- end list -->

  - [籃球](../Page/籃球.md "wikilink")
      - [艾倫·艾佛森](../Page/艾倫·艾佛森.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [奇雲·迦納特](../Page/奇雲·迦納特.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [勒布朗·詹姆斯](../Page/勒布朗·詹姆斯.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [科比·布萊恩](../Page/科比·布萊恩特.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [蒂姆·鄧肯](../Page/蒂姆·鄧肯.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [-{zh:沙奎爾·奧尼爾;zh-hans:沙奎尔·奥尼尔;
        zh-hant:俠客·歐尼爾;}-](../Page/沙奎爾·奧尼爾.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [保羅·皮爾斯](../Page/保羅·皮爾斯.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [姚明](../Page/姚明.md "wikilink")，[中國](../Page/中華人民共和國.md "wikilink")
      - [史蒂夫·納什](../Page/史蒂夫·納什.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")
      - [德克·諾維茨基](../Page/德克·諾維茨基.md "wikilink")，[德國](../Page/德國.md "wikilink")

<!-- end list -->

  - [棒球](../Page/棒球.md "wikilink")
      - [蘭帝·強森](../Page/蘭帝·強森.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [貝瑞·邦茲](../Page/貝瑞·邦茲.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [艾力士·羅德里奎茲](../Page/艾力士·羅德里奎茲.md "wikilink")，[美國](../Page/美國.md "wikilink")
      - [鈴木一朗](../Page/鈴木一朗.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [松坂大輔](../Page/松坂大輔.md "wikilink")，[日本](../Page/日本.md "wikilink")
      - [王建民](../Page/王建民_\(棒球選手\).md "wikilink")，[台灣](../Page/台灣.md "wikilink")
      - [-{朴贊浩}-](../Page/朴贊浩.md "wikilink")，[南韓](../Page/南韓.md "wikilink")

<!-- end list -->

  - [網球](../Page/網球.md "wikilink")
      - [瑪麗亞·莎拉波娃](../Page/瑪麗亞·莎拉波娃.md "wikilink")，[俄羅斯](../Page/俄羅斯.md "wikilink")
      - [羅傑·費德勒](../Page/羅傑·費德勒.md "wikilink")，[瑞士](../Page/瑞士.md "wikilink")

<!-- end list -->

  - [賽車](../Page/一級方程式賽車.md "wikilink")
      - [麥可·舒馬赫](../Page/迈克尔·舒马赫.md "wikilink")，[德國](../Page/德國.md "wikilink")
      - [胡安·巴布羅·蒙托亞](../Page/胡安·巴布羅·蒙托亞.md "wikilink")，[哥倫比亞](../Page/哥倫比亞.md "wikilink")
      - [魯本·巴瑞切羅](../Page/魯賓斯·巴里切羅.md "wikilink")，[巴西](../Page/巴西.md "wikilink")
      - [費爾南多·阿隆索](../Page/費爾南多·阿隆索.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")
      - [奇米·雷克南](../Page/奇米·雷克南.md "wikilink")，[芬蘭](../Page/芬蘭.md "wikilink")
      - [刘易斯·汉密尔顿](../Page/刘易斯·汉密尔顿.md "wikilink")，[英国](../Page/英国.md "wikilink")

### 演藝人員

#### 台灣

##### 歌手

  - [周杰倫](../Page/周杰倫.md "wikilink")
  - [張韶涵](../Page/張韶涵.md "wikilink")
  - [S.H.E](../Page/S.H.E.md "wikilink")
  - [蕭亞軒](../Page/蕭亞軒.md "wikilink")
  - [羅志祥](../Page/羅志祥.md "wikilink")
  - [蔡依林](../Page/蔡依林.md "wikilink")
  - [5566](../Page/5566.md "wikilink")
  - [王心凌](../Page/王心凌.md "wikilink")
  - [飛輪海](../Page/飛輪海.md "wikilink")
  - [楊丞琳](../Page/楊丞琳.md "wikilink")
  - [潘瑋柏](../Page/潘瑋柏.md "wikilink")
  - [卓文萱](../Page/卓文萱.md "wikilink")
  - [F.I.R](../Page/F.I.R.md "wikilink")
  - [八三夭](../Page/八三夭.md "wikilink")
  - [MP魔幻力量](../Page/MP魔幻力量.md "wikilink")

##### 演員

  - [F4 (男子團體)](../Page/F4_\(男子團體\).md "wikilink")
  - [藍正龍](../Page/藍正龍.md "wikilink")
  - [陳喬恩](../Page/陳喬恩.md "wikilink")
  - [陳意涵](../Page/陳意涵.md "wikilink")
  - [明道](../Page/明道_\(藝人\).md "wikilink")
  - [林依晨](../Page/林依晨.md "wikilink")
  - [許瑋倫](../Page/許瑋倫.md "wikilink")(已故)
  - [阮經天](../Page/阮經天.md "wikilink")
  - [鄭元暢](../Page/鄭元暢.md "wikilink")
  - [賀軍翔](../Page/賀軍翔.md "wikilink")
  - [安以軒](../Page/安以軒.md "wikilink")
  - [張孝全](../Page/張孝全.md "wikilink")
  - [林韋君](../Page/林韋君.md "wikilink")
  - [張睿家](../Page/張睿家.md "wikilink")
  - [彭于晏](../Page/彭于晏.md "wikilink")
  - [李威](../Page/李威.md "wikilink")
  - [林佑威](../Page/林佑威.md "wikilink")
  - [霍建華](../Page/霍建華.md "wikilink")

##### 綜藝

  - [陳漢典](../Page/陳漢典.md "wikilink")
  - [黃慧慈](../Page/黃慧慈.md "wikilink")

#### 香港

  - [陳慧琳](../Page/陳慧琳.md "wikilink")
  - [王菲](../Page/王菲.md "wikilink")
  - [谢霆锋](../Page/谢霆锋.md "wikilink")
  - [鄭秀文](../Page/鄭秀文.md "wikilink")
  - [陳奕迅](../Page/陳奕迅.md "wikilink")
  - [容祖兒](../Page/容祖兒.md "wikilink")
  - [古巨基](../Page/古巨基.md "wikilink")
  - [Twins](../Page/Twins.md "wikilink")
  - [Cookies](../Page/Cookies.md "wikilink")
  - [謝安琪](../Page/謝安琪.md "wikilink")
  - [張敬軒](../Page/張敬軒.md "wikilink")

#### 中國大陸

  - [楊冪](../Page/楊冪.md "wikilink")
  - [周迅](../Page/周迅.md "wikilink")
  - [趙薇](../Page/趙薇.md "wikilink")
  - [Angelababy](../Page/Angelababy.md "wikilink")
    [楊穎](../Page/楊穎.md "wikilink")
  - [唐嫣](../Page/唐嫣.md "wikilink")
  - [劉詩詩](../Page/劉詩詩.md "wikilink")
  - [劉亦菲](../Page/劉亦菲.md "wikilink")
  - [趙麗穎](../Page/趙麗穎.md "wikilink")
  - [袁姍姍](../Page/袁姍姍.md "wikilink")
  - [李宇春](../Page/李宇春.md "wikilink")
  - [黃曉明](../Page/黃曉明.md "wikilink")
  - [胡彥斌](../Page/胡彥斌.md "wikilink")

#### 新馬

  - [孫燕姿](../Page/孫燕姿.md "wikilink")
  - [林俊傑](../Page/林俊傑.md "wikilink")
  - [梁靜茹](../Page/梁靜茹.md "wikilink")

#### 日本

  - [濱崎步](../Page/濱崎步.md "wikilink")
  - [倉木麻衣](../Page/倉木麻衣.md "wikilink")
  - [上戶彩](../Page/上戶彩.md "wikilink")
  - [山下智久](../Page/山下智久.md "wikilink")
  - [倖田來未](../Page/倖田來未.md "wikilink")
  - [宇多田光](../Page/宇多田光.md "wikilink")
  - [堀北真希](../Page/堀北真希.md "wikilink")
  - [小栗旬](../Page/小栗旬.md "wikilink")
  - [大塚愛](../Page/大塚愛.md "wikilink")
  - [中島美嘉](../Page/中島美嘉.md "wikilink")
  - [嵐](../Page/嵐.md "wikilink")
  - [KAT-TUN](../Page/KAT-TUN.md "wikilink")
  - [NEWS](../Page/NEWS.md "wikilink")
  - [Hey\! Say\! JUMP](../Page/Hey!_Say!_JUMP.md "wikilink")
  - [AKB48](../Page/AKB48.md "wikilink")

#### 韓國

  - [Rain](../Page/Rain.md "wikilink")
  - [裴勇俊](../Page/裴勇俊.md "wikilink")
  - [東方神起](../Page/東方神起.md "wikilink")
  - [寶兒](../Page/寶兒.md "wikilink")
  - [宋慧喬](../Page/宋慧喬.md "wikilink")
  - [金秀賢](../Page/金秀賢.md "wikilink")
  - [全智賢](../Page/全智賢.md "wikilink")
  - [李孝利](../Page/李孝利.md "wikilink")
  - [李準基](../Page/李準基.md "wikilink")
  - [Super Junior](../Page/Super_Junior.md "wikilink")
  - [少女時代](../Page/少女時代.md "wikilink")
  - [BIGBANG](../Page/BIGBANG.md "wikilink")
  - [SHINee](../Page/SHINee.md "wikilink")
  - [2AM](../Page/2AM.md "wikilink")

#### 歐美

  - [瑪丹娜](../Page/瑪丹娜.md "wikilink")
  - [女神卡卡](../Page/Lady_GaGa.md "wikilink")
  - [布蘭妮·斯皮爾斯](../Page/布蘭妮·斯皮爾斯.md "wikilink")
  - [珍妮弗·洛佩兹](../Page/珍妮弗·洛佩兹.md "wikilink")
  - [克莉絲汀·阿奎萊拉](../Page/克莉絲汀·阿奎萊拉.md "wikilink")
  - [夏奇拉](../Page/夏奇拉.md "wikilink")
  - [姬蒂·佩芮](../Page/姬蒂·佩芮.md "wikilink")
  - [愛黛兒](../Page/愛黛兒.md "wikilink")
  - [粉红佳人](../Page/粉红佳人.md "wikilink")
  - [提雅斯多](../Page/提雅斯多.md "wikilink")
  - [大衛·庫塔](../Page/大衛·庫塔_\(DJ\).md "wikilink")
  - [強尼·戴普](../Page/強尼·戴普.md "wikilink")
  - [麥莉·希拉](../Page/麥莉·希拉.md "wikilink")
  - [琳賽·蘿涵](../Page/琳賽·蘿涵.md "wikilink")
  - [艾薇兒·拉維尼](../Page/艾薇兒·拉維尼.md "wikilink")
  - [蕾哈娜](../Page/蕾哈娜.md "wikilink")
  - [史努比狗狗](../Page/史努比狗狗.md "wikilink")
  - [50 Cent](../Page/50_Cent.md "wikilink")
  - [賈斯汀·提姆布萊克](../Page/賈斯汀·提姆布萊克.md "wikilink")
  - [Eminem](../Page/Eminem.md "wikilink")
  - [法瑞尔·威廉姆斯](../Page/法瑞尔·威廉姆斯.md "wikilink")
  - [亚瑟小子](../Page/亚瑟小子.md "wikilink")
  - [妮莉·費塔朵](../Page/妮莉·費塔朵.md "wikilink")
  - [艾莉西亚·凯斯](../Page/艾莉西亚·凯斯.md "wikilink")
  - [碧昂絲](../Page/碧昂絲.md "wikilink")
  - [小野貓](../Page/小野貓.md "wikilink")
  - [尼歐](../Page/尼歐_\(歌手\).md "wikilink")
  - [黑眼豆豆](../Page/黑眼豆豆.md "wikilink")
  - [丹尼爾·雷德克里夫](../Page/丹尼爾·雷德克里夫.md "wikilink")
  - [艾瑪·華森](../Page/艾瑪·華森.md "wikilink")
  - [魯伯特·葛林](../Page/魯伯特·葛林.md "wikilink")

## 其他

  - [2000年代臺灣](../Page/2000年代臺灣.md "wikilink")
  - [2000年代香港](../Page/2000年代香港.md "wikilink")

## 参考资料

[\*](../Category/2000年代.md "wikilink")
[終](../Category/20世纪各年代.md "wikilink")
[0](../Category/21世纪各年代.md "wikilink")

1.