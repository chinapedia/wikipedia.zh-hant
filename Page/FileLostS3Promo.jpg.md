## 摘要

[迷失 (第三季)](../Page/迷失_\(第三季\).md "wikilink")

  - 來源：http://picasaweb.google.com/jcr2970/Perdidos
  - 版權持有者：[美國廣播公司](../Page/美國廣播公司.md "wikilink")

## [迷失的合理使用](../Page/迷失.md "wikilink")

This image, Lost Season 3 Promotional Photo, is being linked here;
though the picture is subject to copyright I (Synflame) feel it is
covered by the U.S. fair use laws because:

  - The promo has been released for reasons of advertising and awareness
    of the 3rd season of Lost;
  - It does not limit the copyright owner's rights to sell, produce, or
    advertise the show in any way;
  - The use of the image is, it itself fulfilling much of the intent of
    the promo;

This image, LostS3Promo.jpg, is being linked here; though the picture is
subject to copyright I
([Alexisfan07](../Page/User:Alexisfan07.md "wikilink")) feel it is
covered by the U.S. fair use laws because:

  - As above
  - It is used to identify the subject of the article
  - It features the starring cast
  - It is accompanied by commentary on the cast, the season it
    advertises and the show in general

\--[Alexisfan07](../Page/User:Alexisfan07.md "wikilink") 11 February
2008

## Licensing

[Category:迷失圖片](../Category/迷失圖片.md "wikilink")