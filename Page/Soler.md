**Soler**是一對來自[澳門並於](../Page/澳門.md "wikilink")[香港發展的男子](../Page/香港.md "wikilink")[樂隊](../Page/樂隊.md "wikilink")，在[台灣的官方](../Page/台灣.md "wikilink")[中文團名為](../Page/中文.md "wikilink")**太陽系**，但大部分台灣人還是直稱Soler。兩位成員是一對[歐](../Page/歐洲.md "wikilink")[亞混血](../Page/亞洲.md "wikilink")[雙胞胎](../Page/雙胞胎.md "wikilink")，哥哥名Julio
Acconci（[中文名](../Page/漢名.md "wikilink")：[夏利奧](../Page/夏利奧.md "wikilink")），弟弟名Dino
Acconci（中文名：[夏健龍](../Page/夏健龍.md "wikilink")）。

## 成員

Soler主要由一對[歐](../Page/歐洲.md "wikilink")[亞混血](../Page/亞洲.md "wikilink")[雙胞胎組成](../Page/雙胞胎.md "wikilink")。2兄弟在訪問中表示想發展成5人樂隊，但由於觀眾對Soler的印象是2兄弟印象太強烈，所以對其他成員印象不深。

<table>
<thead>
<tr class="header">
<th><p>主要成員列表 |- style="color:#aaaaa ;" |</p></th>
<th><p>姓名</p></th>
<th><p>出生日期</p></th>
<th><p>崗位</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/夏利奧.md" title="wikilink">Julio</a></p></td>
<td></td>
<td><p>主唱、貝斯</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏健龍.md" title="wikilink">Dino</a></p></td>
<td></td>
<td><p>主唱、結他</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

| 其餘成員列表 |- style="color:\#aaaaa ;" |                | 姓名 | 出生日期 | 崗位 |
| -------------------------------------------------- | -- | ---- | -- |
| Andrew，參見[秋紅](../Page/秋紅.md "wikilink")            | ?  | 結他   |    |
| [Siu Ming](../Page/Siu_Ming.md "wikilink")         | ?  | 貝斯   |    |
| [Dave Sampson](../Page/Dave_Sampson.md "wikilink") | ?  | 鼓    |    |
|                                                    |    |      |    |

## 組合歷史

1972年9月9日出生。二人畢業於澳門[粵華中學](../Page/粵華中學.md "wikilink")。隊名Soler取自母親姓氏。其父[夏剛志是巳故著名旅澳](../Page/夏剛志.md "wikilink")[意大利](../Page/意大利.md "wikilink")[建築師](../Page/建築師.md "wikilink")，而媽媽則是[緬甸](../Page/緬甸.md "wikilink")[克倫族人](../Page/克倫族.md "wikilink")，他們自己則在澳門出生長大。兄弟二人於17歲時離開澳門到意大利生活，先就讀當地的語言學校。其後弟弟Dino就讀於意大利的高中及大學，哥哥Julio則就讀於意大利的設計學院，二人亦曾在[英國工作](../Page/英國.md "wikilink")，至2000年後先後回澳門發展。他們通曉[粵語](../Page/粵語.md "wikilink")、[普通話](../Page/普通話.md "wikilink")、[英語](../Page/英語.md "wikilink")、[意大利語](../Page/意大利語.md "wikilink")、[西班牙語和](../Page/西班牙語.md "wikilink")[葡萄牙語](../Page/葡萄牙語.md "wikilink")，弟弟Dino還通曉[法語](../Page/法語.md "wikilink")。而創作的歌曲則以粵語、普通話及英語為主。

Soler中的弟弟Dino早在在2000年至2001年間透過作曲踏入香港樂壇，作品包括[陳奕迅的](../Page/陳奕迅.md "wikilink")《送院途中》、《和平飯店》，[梁漢文的](../Page/梁漢文.md "wikilink")《超級英雄》、《快歌》等。同時亦為[Beyond前主力成員](../Page/Beyond.md "wikilink")[黃貫中的另一支樂隊](../Page/黃貫中.md "wikilink")「[汗](../Page/汗_\(樂隊\).md "wikilink")」中擔任吉他手，因此Soler一直得到黃貫中的支持，例如為他們作詞等。另外亦曾於2003年至2004年間，與[恭碩良等樂手於](../Page/恭碩良.md "wikilink")[澳門](../Page/澳門.md "wikilink")[荷蘭園二馬路的Friday酒吧作多次演出](../Page/荷蘭園.md "wikilink")。

Soler於進軍[香港之前](../Page/香港.md "wikilink")，他們曾由[意大利的](../Page/意大利.md "wikilink")[EMI公司發行第一張EP](../Page/EMI.md "wikilink")，其後他們簽約[蜂鳥音樂](../Page/蜂鳥音樂.md "wikilink")（但此公司的發行事宜則由[東亞唱片負責](../Page/東亞唱片.md "wikilink")），於2005年發行第一張粵語專輯《[雙聲道](../Page/雙聲道.md "wikilink")》，派台作品包括《[失魂](../Page/失魂.md "wikilink")》、《[海嘯](../Page/海嘯.md "wikilink")》。於澳門舉行的[2005年東亞運動會會歌亦由他們所唱](../Page/2005年東亞運動會.md "wikilink")，名為《[We
will
shine](../Page/We_will_shine.md "wikilink")》。其後於2006年4月25日於香港[紅磡體育館舉行演唱會](../Page/紅磡體育館.md "wikilink")。

2006年底Soler樂隊欲於合約完結前離開公司遂要求解約，[蜂鳥音樂的創辦人張丹告知他們須付](../Page/蜂鳥音樂.md "wikilink")2,000萬元方可解約，當時兩兄弟聽罷即拂袖而去，故之後遭前經理人公司「蜂鳥音樂」入稟告毀約索償2,400萬元。案件於2007年8月[香港區域法院開審](../Page/香港區域法院.md "wikilink")。於同時一群樂隊支持者架設了
SaveSolerMusic網站 及於[YouTube短片網站上放上](../Page/YouTube.md "wikilink") \[1\]
為其樂隊及香港音樂人爭取創作自由。2009年1月22日高等法院暫委法官[郭靄誠判Soler毀約](../Page/郭靄誠.md "wikilink")，要向蜂鳥賠償506萬元。2012年11月21日，由於Soler沒有還款，被蜂鳥音樂申請破產。蜂鳥音樂的代表向聆案官表示，已存入所有文件，對方卻沒有回應遂申請2人即時破產，聆案官最終亦頒令2人破產。在2014年1月，張丹表示兩人即使有工作，但是一元也沒有清還。\[2\]

2008年9月8日，Soler舉行加盟[博美娛樂唱片公司的簽約儀式](../Page/博美娛樂.md "wikilink")，適逢9月9日是Soler兩兄弟生日，博美娛樂老闆[羅傑承及一眾歌手](../Page/羅傑承.md "wikilink")[關心妍](../Page/關心妍.md "wikilink")、[周麗淇都有到場祝賀](../Page/周麗淇.md "wikilink")。\[3\]

## 個人生活

  - 弟弟Dino
    Acconci於2009年11月15日，與拍拖四年的女友黃麗施（Nancy）結婚\[4\]。育有兩名兒子（長子夏恩智Damiano及小兒子夏恩澤Orlando）

<!-- end list -->

  - 哥哥Julio
    Acconci於2013年1月15日，與同是藝人的[胡婷婷](../Page/胡婷婷.md "wikilink")，即前[台中](../Page/台中.md "wikilink")[市長](../Page/市長.md "wikilink")、前[中華民國外交部部長](../Page/中華民國外交部.md "wikilink")[胡志強的女兒舉行婚禮](../Page/胡志強.md "wikilink")\[5\]，被台中人稱之為「第一女婿」。2014年1月27日，兩人宣布分手，結束歷時一年的婚姻。

<!-- end list -->

  - 哥哥Julio於2017年，遇上被遺棄的黑色唐狗 Knight，決定收養，數月後，再領養白色的
    Spirit，希望牠們有個伴接受訪問時表示狗是很有靈性的動物，有幾次他情緒低落，愛犬都會走過來安慰，這份窩心感覺，實在難以形容\[6\]。

## 唱片列表

  - 1998年：在[意大利由意大利](../Page/意大利.md "wikilink")[EMI唱片公司發行](../Page/EMI.md "wikilink")《[Tears
    In Your Hands](../Page/Tears_In_Your_Hands.md "wikilink")》
  - 2004年：在[大陸發行](../Page/大陸.md "wikilink")《[北回歸線](../Page/北回歸線.md "wikilink")》（盗版）
  - 2005年：在[香港](../Page/香港.md "wikilink")《[雙聲道](../Page/雙聲道.md "wikilink")》、《[直覺](../Page/直覺_\(Soler專輯\).md "wikilink")》
  - 2006年：在[臺灣發行](../Page/臺灣.md "wikilink")《直覺》
  - 2006年：在[新加坡發行](../Page/新加坡.md "wikilink")《直覺》（跟香港版封面有別，前後發行了2個版本）
  - 2006年：在香港發行《[龍虎門](../Page/龍虎門_\(EP\).md "wikilink")》（EP）
  - 2006年：在大陸發行《龍虎門》（EP，跟香港版封面及包裝有別）
  - 2007年：在大陸、香港及臺灣發行《[X2](../Page/X2.md "wikilink")》（大碟）
  - 2008年：在臺灣發行《[海邊的卡夫卡](../Page/海邊的卡夫卡.md "wikilink")》（現場演出大碟）
  - 2009年：在香港發行《[Canto](../Page/Canto.md "wikilink")》（首張全粵語大碟）
  - 2009年：在[馬來西亞發行](../Page/馬來西亞.md "wikilink")《Canto》（馬來西亞版，跟香港版封面不同，附含3首MV的DVD）
  - 2010年：在香港及臺灣發行《[Vivo](../Page/Vivo.md "wikilink")》
  - 2011年：在大陸發行《Vivo》
  - 2018年：《Evolution 演化》

## 派台歌曲成績

| **派台歌曲成績**                                 |
| ------------------------------------------ |
| 唱片                                         |
| **2005年**                                  |
| [雙聲道](../Page/雙聲道_\(專輯\).md "wikilink")    |
| 雙聲道                                        |
| 雙聲道                                        |
| 雙聲道                                        |
| [直覺](../Page/直覺_\(Soler專輯\).md "wikilink") |
| **2006年**                                  |
| 直覺                                         |
| [龍虎門](../Page/龍虎門_\(EP\).md "wikilink")    |
| **2007年**                                  |
| [X2](../Page/X2.md "wikilink")             |
| **2008年**                                  |
| [CANTO](../Page/CANTO.md "wikilink")       |
| **2009年**                                  |
| CANTO                                      |
| CANTO                                      |
| **2014年**                                  |
| [獨家試唱 2](../Page/獨家試唱_2.md "wikilink")     |
| **2015年**                                  |
|                                            |
| **2018年**                                  |
| Evolution 演化                               |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

（\*）表示仍在榜上

**粗體**表示冠軍歌

## 曾獲獎項

  - 2005年

<!-- end list -->

  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink") —— I-Tech勁爆新人王（組合）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [叱吒樂壇流行榜頒獎典禮](../Page/叱吒樂壇流行榜頒獎典禮.md "wikilink") ——叱吒樂壇生力軍組合（金獎）
  - 叱吒樂壇流行榜頒獎典禮 ——叱吒樂壇組合（銅獎）
  - [十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink") ——最受歡迎組合獎（銅獎）
  - [十大中文金曲頒獎音樂會](../Page/十大中文金曲頒獎音樂會.md "wikilink") ——最有前途新人獎（男歌手）（銀獎）
  - [RoadShow至尊音樂頒獎禮](../Page/RoadShow至尊音樂頒獎禮.md "wikilink") ——至尊新組合

<!-- end list -->

  - 2007年

<!-- end list -->

  - [RoadShow至尊音樂頒獎禮](../Page/RoadShow至尊音樂頒獎禮.md "wikilink") ——至尊組合樂隊
  - [9+2 音樂頒獎禮](../Page/9+2_音樂頒獎禮.md "wikilink") ——最佳組合樂隊
  - 9+2 音樂頒獎禮 ——十大金曲獎：媽媽再見

<!-- end list -->

  - 2008年

<!-- end list -->

  - [2007/2008 CASH 金帆音樂獎](../Page/2007/2008_CASH_金帆音樂獎.md "wikilink")
    ——最佳樂隊演繹：惹我
  - 2007/2008 CASH 金帆音樂獎 ——最佳另類作品：堅持

<!-- end list -->

  - 2009年

<!-- end list -->

  - [澳門特別行政區](../Page/澳門特別行政區.md "wikilink") ——功績獎狀

<!-- end list -->

  - 2010年

<!-- end list -->

  - [2010 華語金曲奬](../Page/2010_華語金曲奬.md "wikilink") ——十大最佳粵語大碟：Canto
  - [9+2 音樂頒獎禮](../Page/9+2_音樂頒獎禮.md "wikilink") ——最佳粵語大碟：Canto
  - 9+2 音樂頒獎禮 ——最佳組合樂隊

<!-- end list -->

  - 2014年

<!-- end list -->

  - [新城勁爆頒獎禮](../Page/新城勁爆頒獎禮.md "wikilink") —— 新城劲爆亚洲组合大奖

## 電影

  - 2011年

<!-- end list -->

  - [潮性辦公室：電影版](../Page/潮性辦公室：電影版.md "wikilink")
  - [堂口故事2](../Page/堂口故事2.md "wikilink")
  - [東成西就2011](../Page/東成西就2011.md "wikilink")
  - [痞子英雄首部曲：全面開戰](../Page/痞子英雄首部曲：全面開戰.md "wikilink")

<!-- end list -->

  - 2015年

<!-- end list -->

  - [賭城風雲II](../Page/賭城風雲II.md "wikilink")

## 廣告

  - [Wall Street Institute](../Page/Wall_Street_Institute.md "wikilink")
  - [Absolut Vodka](../Page/Absolut_Vodka.md "wikilink")
  - [必勝客](../Page/必勝客.md "wikilink")
  - [澳門咀香園餅家](../Page/咀香園餅家.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  -
  -
  - [Soler 在台灣的日誌 - 堅持搖滾 SOLER:x2](http://blog.roodo.com/solerx2)

[Category:1972年出生](../Category/1972年出生.md "wikilink")
[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")
[Category:澳門男歌手](../Category/澳門男歌手.md "wikilink")
[Category:雙胞胎人物](../Category/雙胞胎人物.md "wikilink")
[Category:意大利裔混血兒](../Category/意大利裔混血兒.md "wikilink")
[Category:亞裔混血兒](../Category/亞裔混血兒.md "wikilink")
[Category:克倫族人](../Category/克倫族人.md "wikilink")

1.
2.
3.
4.  [Dino@Soler娶妻兩度落淚](http://the-sun.on.cc/cnt/entertainment/20091116/00470_009.html)
5.  [胡婷婷嫁Soler哥Jilio　七期豪宅低調完婚](http://www.ettoday.net/news/20130116/153250.htm)
6.  [【專訪】Soler大孖Julio領養唐狗學習相處：點解棄養主人咁狠心](https://bka.mpweekly.com/focus/local/%E5%90%8D%E4%BA%BA%E5%B0%88%E7%B7%9A/%E5%BE%97%E5%AF%B5/20180103-96113)