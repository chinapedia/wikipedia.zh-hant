**科學革命**（），指[近世歷史上](../Page/近世.md "wikilink")，[現代科學在歐洲萌芽的這段時期](../Page/現代科學.md "wikilink")。在那段時期中，[數學](../Page/數學.md "wikilink")、[物理學](../Page/物理學.md "wikilink")、[天文學](../Page/天文學.md "wikilink")、[生物學](../Page/生物學.md "wikilink")（包括[人體解剖學](../Page/人體解剖學.md "wikilink")）與[化學等學科皆出現突破性的進步](../Page/化學.md "wikilink")，這些知識改變了人類對於[自然的眼界及心態](../Page/自然.md "wikilink")\[1\]\[2\]\[3\]\[4\]\[5\]。科學革命發源於[歐洲](../Page/歐洲.md "wikilink")，[文藝復興時代結尾之時](../Page/文藝復興.md "wikilink")，開始了科學進步的歷程，這個歷程一直持續到18世紀與19世紀。科學革命的衝擊，造成[啟蒙運動的出現](../Page/啟蒙運動.md "wikilink")，影響了歐洲社會。

科學革命的開始日期，史學家仍有爭議。在1543年，[尼古拉斯·哥白尼出版著作](../Page/尼古拉·哥白尼.md "wikilink")《[天体运行论](../Page/天体运行论.md "wikilink")》，這經常被當成是科學革命的起點。從1543年，一直到1632年[伽利略出版](../Page/伽利略.md "wikilink")《[关于托勒密和哥白尼两大世界体系的对话](../Page/关于托勒密和哥白尼两大世界体系的对话.md "wikilink")》，這段時間，常被認為是科學革命的第一階段。在這個階段中，復興了[古希臘與羅馬時期的舊有科學知識](../Page/古希臘.md "wikilink")，被稱為是**科學復興**（）。在伽利略之後，則是現代科學的興起，[艾薩克·牛頓在](../Page/艾薩克·牛頓.md "wikilink")1687年發表《[自然哲学的数学原理](../Page/自然哲学的数学原理.md "wikilink")》後，經常被認為是科學革命的完成期。

科學革命這個概念的建立，可以追溯到18世紀，[让·西尔万·巴伊的著作](../Page/让·西尔万·巴伊.md "wikilink")，他同時建立了兩階段分期的看法。這個名詞的提出，則是來自20世紀史學家[亞歷山大·夸黑](../Page/亞歷山大·夸黑.md "wikilink")。

## 概論

將科學發展稱為[革命](../Page/革命.md "wikilink")，這類用語起源於18世紀。如[亚历克西斯·克劳德·克莱罗在](../Page/亚历克西斯·克劳德·克莱罗.md "wikilink")1747年，稱[艾薩克·牛頓的理論](../Page/艾薩克·牛頓.md "wikilink")，造成了一個革命。這個形容詞也曾被使用於描敍[安托万-洛朗·德·拉瓦锡發現](../Page/安托万-洛朗·德·拉瓦锡.md "wikilink")[氧氣的成就上](../Page/氧氣.md "wikilink")。

19世紀時，[威廉·休厄爾首次提出](../Page/威廉·休厄爾.md "wikilink")，歐洲在15世紀及16世紀的變化，主要來自於[科學本身](../Page/科學.md "wikilink")（或是[科學方法](../Page/科學方法.md "wikilink")）的變革。

在20世紀時，史學家[亞歷山大·夸黑首次提出科學革命這個名詞](../Page/亞歷山大·夸黑.md "wikilink")，並建立了完整的論述。

尽管科学革命的具体时间仍有争议，科学史的萌芽可能开始於14世纪，也有歷史學者认为[化学和](../Page/化学.md "wikilink")[生物学的革命开始於](../Page/生物学.md "wikilink")18、19世纪。\[6\]
但公认的是，在16至17世纪之间，[物理学](../Page/物理学.md "wikilink")、[天文学](../Page/天文学.md "wikilink")、生物学、[数学](../Page/数学.md "wikilink")、[醫學以及化學的思想都经历了根本性的变化](../Page/醫學.md "wikilink")，由中世紀的觀點轉變為現代科學的基礎，不论是在各个独立的学科内，更是在对整个宇宙的认知中。

## 新科学的发展

十六至十九世纪时的科学思想和科学家包括：

  - [尼古拉斯·哥白尼](../Page/尼古拉·哥白尼.md "wikilink")，1543年出版了《天体运行论》，提出了[日心说理论](../Page/日心说.md "wikilink")，質疑天主教的「地球中心說」，史稱「哥白尼革命」。
  - [安德烈·维赛留斯](../Page/安德烈·维赛留斯.md "wikilink")，1543年出版了《人体构造》，解释了血液在人体内循环的过程，还从解剖尸体组装了第一副人类骨架。
  - [威廉·吉尔伯特](../Page/威廉·吉尔伯特.md "wikilink")，1600年出版了《[论磁石](../Page/论磁石.md "wikilink")》是[物理学史上第一部系统阐述](../Page/物理学史.md "wikilink")[磁学的科学专著](../Page/磁学.md "wikilink")。
  - [第谷·布拉赫](../Page/第谷·布拉赫.md "wikilink")，对16世纪末期所认知的星体进行了详细并且准确的观测，为开普勒的研究提供了基本数据。
  - [弗兰西斯·培根](../Page/弗兰西斯·培根.md "wikilink")，企图通过分析和确定[科学的一般方法和表明其应用方式](../Page/科学.md "wikilink")，给予新科学运动以发展的动力和方向。，發明「[歸納法](../Page/歸納法.md "wikilink")」，出版《新工具》一書
  - [伽利略·伽利莱](../Page/伽利略·伽利莱.md "wikilink")，改进了[望远镜](../Page/望远镜.md "wikilink")，并对[金星和](../Page/金星.md "wikilink")[木星的](../Page/木星.md "wikilink")[卫星进行了准确的观测](../Page/卫星.md "wikilink")，於1610年发表观测结果。通过理论分析与实验推翻了被奉为圭臬的亚里斯多德的力学体系并建立了近代力学。
  - [约翰内斯·开普勒](../Page/约翰内斯·开普勒.md "wikilink")，1609年发表了关于行星运动的两条定律，1618年发现了第三条定律，就是后来被称为“[开普勒定律](../Page/开普勒定律.md "wikilink")”的行星三大定律，说明了[行星围绕](../Page/行星.md "wikilink")[太阳旋转的理论](../Page/太阳.md "wikilink")。
  - [威廉·哈维](../Page/威廉·哈维.md "wikilink")，通过[解剖等手段展示了血液的循环](../Page/解剖.md "wikilink")。
  - [勒奈·笛卡尔](../Page/勒奈·笛卡尔.md "wikilink")，是演绎推理的先驱，1637年出版了《[方法论](../Page/方法论.md "wikilink")》。
  - [安东·范·列文霍克](../Page/安东·范·列文霍克.md "wikilink")，建造了高清晰度的单显微镜，研究了毛细管循环和肌肉纤维。他观察了[血球](../Page/血球.md "wikilink")、[精子与](../Page/精子.md "wikilink")[细菌](../Page/细菌.md "wikilink")，并绘出了它们的形象。於1683年发现了细菌。
  - [艾萨克·牛顿](../Page/艾萨克·牛顿.md "wikilink")，1687年7月5日发表的《[自然哲学的数学原理](../Page/自然哲学的数学原理.md "wikilink")》裡提出的[万有引力定律以及他的](../Page/牛顿万有引力定律.md "wikilink")[牛顿运动定律是](../Page/牛顿运动定律.md "wikilink")[经典力学的基石](../Page/经典力学.md "wikilink")。牛顿还和[莱布尼茨各自独立地发明了](../Page/戈特弗里德·威廉·莱布尼茨.md "wikilink")[微积分](../Page/微积分学.md "wikilink")。
  - [查爾斯·羅伯特·達爾文](../Page/查爾斯·羅伯特·達爾文.md "wikilink")，1859年11月24日发表了《[物種起源](../Page/物種起源.md "wikilink")》，提出了[演化論的觀點](../Page/演化論.md "wikilink")。

## 參考文獻

[Category:科学史](../Category/科学史.md "wikilink")

1.  Galileo Galilei, *Two New Sciences*, trans. [Stillman
    Drake](../Page/Stillman_Drake.md "wikilink"), (Madison: Univ. of
    Wisconsin Pr., 1974), pp 217, 225, 296–7.
2.
3.
4.  Marshall Clagett, *The Science of Mechanics in the Middle Ages,*
    (Madison, Univ. of Wisconsin Pr., 1961), pp. 218–19, 252–5, 346,
    409–16, 547, 576–8, 673–82; Anneliese Maier, "Galileo and the
    Scholastic Theory of Impetus," pp. 103–123 in *On the Threshold of
    Exact Science: Selected Writings of Anneliese Maier on Late Medieval
    Natural Philosophy,* (Philadelphia: Univ. of Pennsylvania Pr.,
    1982).
5.  "Scientific Revolution" in
    *[Encarta](../Page/Encarta.md "wikilink")*. 2007.
    \[<http://encarta.msn.com/encyclopedia_701509067/Scientific_Revolution.html>.\]
6.  Herbert Butterfield, *The Origins of Modern Science, 1300-1800*.