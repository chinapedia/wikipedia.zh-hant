**危地馬拉鱷**（學名**''Crocodylus
moreletii**''），又名**墨西哥鱷**、**莫瑞雷鱷**，是一種小型[鱷魚](../Page/鱷魚.md "wikilink")，身長大約3米。這種鱷魚是由一位[法國](../Page/法國.md "wikilink")[自然學家](../Page/自然學.md "wikilink")[莫瑞雷](../Page/莫瑞雷.md "wikilink")（1809年—1892年）於1850年發現之後命名的\[1\]。牠們主要棲身於[中美洲](../Page/中美洲.md "wikilink")[墨西哥及](../Page/墨西哥.md "wikilink")[危地馬拉一帶的](../Page/危地馬拉.md "wikilink")[濕地](../Page/濕地.md "wikilink")。\[2\]\[3\]

## 参考来源

[CM](../Category/IUCN無危物種.md "wikilink")
[moreletii](../Category/鱷科.md "wikilink")

1.
2.
3.