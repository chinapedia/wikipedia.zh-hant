**广深高速公路**北起於[中国](../Page/中国.md "wikilink")[广州市黄村立交](../Page/广州市.md "wikilink")，与[广州环城高速公路相接](../Page/广州环城高速公路.md "wikilink")，南止於[深圳市](../Page/深圳市.md "wikilink")[皇岗口岸附近](../Page/皇岗口岸.md "wikilink")，与深圳皇岗路相接。屬于[G4和](../Page/京港澳高速公路.md "wikilink")[G15的一部分](../Page/瀋海高速公路.md "wikilink")，從鶴州出入口至火村出入口的路段為**G4**和**G15**並線之路段。

行車線設計為為双向六線行车，全线设有路灯照明，全长120[公里](../Page/公里.md "wikilink")，限速为[時速](../Page/時速.md "wikilink")120公里，沿线设有[广州](../Page/广州市.md "wikilink")、火村、[萝岗](../Page/萝岗.md "wikilink")、[新塘](../Page/新塘鎮_\(增城区\).md "wikilink")、[麻涌](../Page/麻涌镇.md "wikilink")、[望牛墩](../Page/望牛墩镇.md "wikilink")、[道滘](../Page/道滘镇.md "wikilink")、石鼓、[厚街](../Page/厚街镇.md "wikilink")、新联、太平、五点梅、[长安](../Page/长安镇_\(东莞市\).md "wikilink")、新桥、黄田、鹤洲、[宝安](../Page/宝安.md "wikilink")、[南头](../Page/南头_\(深圳\).md "wikilink")、[福田及](../Page/福田.md "wikilink")[皇岗](../Page/皇岗.md "wikilink")20个[收费站及互通式立交桥](../Page/收费站.md "wikilink")；於南头收费站附近设有同乐检查站；广深高速公路又是[香港](../Page/香港.md "wikilink")[直通巴士来往广州](../Page/跨境巴士.md "wikilink")、[珠江三角洲及广州以北城市必经之路](../Page/珠江三角洲.md "wikilink")。

广深高速公路途径广州和深圳两个千万人口的城市，以及被称为“世界工厂”的东莞，车流量极大，被称为“中国最赚钱的高速公路”，交通堵塞经常发生。\[1\]因此在廣東省「十一五」規劃中另建公路來舒緩交通堵塞问题。还曾因原来的服务区被拆除，百余公里的高速公路没有服务区而惹官非。\[2\]

廣深高速公路由香港[合和實業子公司](../Page/合和實業.md "wikilink")[合和公路基建有限公司以](../Page/合和公路基建有限公司.md "wikilink")[民間興建營運後轉移模式投資興建](../Page/民間興建營運後轉移模式.md "wikilink")，於1997年7月1日通車，有三十年[專營權直至](../Page/專營權.md "wikilink")2027年。

由深圳至廣州外圍、連同延伸至[珠海的路段](../Page/珠海.md "wikilink")（原屬[京珠高速公路](../Page/京珠高速公路.md "wikilink")）通稱「广深珠高速公路」\[3\]（現[广澳高速公路一部份](../Page/广澳高速公路.md "wikilink")），是連接[珠江口東西岸](../Page/珠江口.md "wikilink")（包括香港及[澳門](../Page/澳門.md "wikilink")）最快速陸路通道；[虎門大橋通車將深圳至珠海車程距縮短了大約一半](../Page/虎門大橋.md "wikilink")。

## 互通枢纽及服务设施

[Guangdong_G4.svg](https://zh.wikipedia.org/wiki/File:Guangdong_G4.svg "fig:Guangdong_G4.svg")
[Guangdong_G15.svg](https://zh.wikipedia.org/wiki/File:Guangdong_G15.svg "fig:Guangdong_G15.svg")

<small>注：出口序号一栏里，带有方括号的是以为基准的编号（以火村立交为起点）
其余均为以为基准的编号（以北京为起点） </small>

## 圖片集

[File:9c98.JPG|广深高速公路](File:9c98.JPG%7C广深高速公路) 深圳南頭 - 皇崗段
<File:Guangshen> Expressway Baoan Section.jpg|广深高速公路深圳宝安段 <File:G4-G15>
interchange 02.jpg|更換新路標後鶴州出入口的路標（廣州方向） <File:G4-G15> interchange
01.jpg|更換新路標後鶴州出入口的路標（深圳方向）

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [合和公路基建
    廣深高速公路](http://www.hopewellhighway.com/WebSite_cht/bo/gs_l.htm)

[Category:广东省高速公路](../Category/广东省高速公路.md "wikilink")
[Category:深圳市高速公路](../Category/深圳市高速公路.md "wikilink")
[Category:广州市高速公路](../Category/广州市高速公路.md "wikilink")

1.  <http://news.sina.com.cn/c/2011-06-22/132122686349.shtml>
2.  <http://www.law-star.com/cac/case_1450002354.htm>
3.