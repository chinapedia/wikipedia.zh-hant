**博尔扎诺－南蒂罗尔自治省**（；；[拉登语](../Page/拉登语.md "wikilink")：），又稱**上阿迪杰**（）或**南蒂罗尔**（****
或
****），是[意大利](../Page/意大利.md "wikilink")[特伦蒂诺-上阿迪杰的一个省](../Page/特伦蒂诺-上阿迪杰.md "wikilink")。面积7,400[平方公里](../Page/平方公里.md "wikilink")，2005年人口487,673人。首府[博尔扎诺](../Page/博尔扎诺.md "wikilink")。

此自治省的主體民族是日耳曼族，通行[德語](../Page/德語.md "wikilink")。

下分116[市镇](../Page/市镇_\(意大利\).md "wikilink")。

## 友好城市

  - [河北省](../Page/河北省.md "wikilink")[张家口市](../Page/张家口市.md "wikilink")（2007年11月23日缔结）

[博尔扎诺自治省](../Category/博尔扎诺自治省.md "wikilink")
[Category:意大利省份](../Category/意大利省份.md "wikilink")
[Category:特倫蒂諾-上阿迪傑大區](../Category/特倫蒂諾-上阿迪傑大區.md "wikilink")