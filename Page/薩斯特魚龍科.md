**薩斯特魚龍科**（Shastasauridae）是群早期[魚龍類](../Page/魚龍.md "wikilink")，生活於2億3000萬至2億1000萬年前[三疊紀](../Page/三疊紀.md "wikilink")，擁有比較大的頭骨與修長的嘴巴，沒有背鰭，或是背鰭很小。

薩斯特魚龍科也是所有魚龍類中最大的，在[北美洲西部發現的](../Page/北美洲.md "wikilink")[通俗秀尼魚龍](../Page/秀尼魚龍.md "wikilink")，可以長到23公尺長。薩斯特魚龍科動物以[魚類與其他小型水生動物為食](../Page/魚類.md "wikilink")，例如[菊石類](../Page/菊石類.md "wikilink")。薩斯特魚龍科包含：[薩斯特魚龍](../Page/薩斯特魚龍.md "wikilink")、秀尼魚龍。

## 外部連結

  - ["Shastasaurus altispinus (Ichthyosauria, Shastasauridae) from the
    Upper Triassic of the El Antimonio District, Northwestern Sonora,
    Mexico"](http://links.jstor.org/sici?sici=0022-3360\(198911\)63%3A6%3C930%3ASA\(SFT%3E2.0.CO%3B2-6)

[\*](../Category/魚龍目.md "wikilink")