**MIM-23鷹式飛彈**是[美國](../Page/美國.md "wikilink")[雷神公司研製的中距離](../Page/雷神公司.md "wikilink")[地對空飛彈](../Page/地對空飛彈.md "wikilink")，主要是用來對抗中低空飛行的飛機，填補的防禦空缺。其名稱「鷹式」（HAWK）是「全程歸向殺手」（Homing
All the Way
Killer）的[縮寫](../Page/縮寫.md "wikilink")。中国音译**霍克导弹**。1994年美軍的鷹式飛彈以[MIM-104愛國者飛彈替換](../Page/MIM-104愛國者飛彈.md "wikilink")，[美國海軍陸戰隊的鷹式飛彈則在](../Page/美國海軍陸戰隊.md "wikilink")2002年以[刺針飛彈替換](../Page/FIM-92刺針便攜式防空飛彈.md "wikilink")，目前鷹式飛彈仍在美國以外的盟邦服役，總生產量至少有4萬枚。

## 型號

MIM-23A

MIM-23B

## 中華民國服役現況

由於鷹式飛彈服役多年，生產線早已經關閉，零配件取得不易，因此[中華民國國防部計劃在](../Page/中華民國國防部.md "wikilink")2020年前讓所有的鷹式飛彈退役。將由[天弓三型防空飛彈取代](../Page/天弓三型防空飛彈.md "wikilink")。

### 評價

根據雷神公司的統計，[中華民國空軍為全球使用鷹式飛彈部隊中](../Page/中華民國空軍.md "wikilink")，使用效率最好的部隊。</br>
現任駐丹麥代表、前陸軍司令李翔宙先生擔任鷹式飛彈發射排排長期間，曾創下美軍鷹式飛彈測考零扣分紀錄，目前仍無人可破。

[German_Hawk_missile_unit,_1996.jpg](https://zh.wikipedia.org/wiki/File:German_Hawk_missile_unit,_1996.jpg "fig:German_Hawk_missile_unit,_1996.jpg")（Luftwaffe）服務的鷹式飛彈系統\]\]
[M727-Hawk-hatzerim-2.jpg](https://zh.wikipedia.org/wiki/File:M727-Hawk-hatzerim-2.jpg "fig:M727-Hawk-hatzerim-2.jpg")
[Hawk_Missile_Launcher_in_Chengkungling_Oct2011a.jpg](https://zh.wikipedia.org/wiki/File:Hawk_Missile_Launcher_in_Chengkungling_Oct2011a.jpg "fig:Hawk_Missile_Launcher_in_Chengkungling_Oct2011a.jpg")飛彈指揮部之鷹式飛彈\]\]

{{-}}

## 外部連結

  - [Raytheon's HAWK system
    page](https://web.archive.org/web/20080208135351/http://www.raytheon.com/products/hawk/)

  - [Designation-Systems.net Article on the
    hawk](http://www.designation-systems.net/dusrm/m-23.html)

  - <http://www.madracki.com/usarmyhawk/generalorders.html>

  - [FAS.org page on the HAWK
    system.](http://www.fas.org/spp/starwars/program/hawk.htm)

  - [HAWK page in
    Russian.](https://web.archive.org/web/20030510161725/http://www.new-factoria.ru/missile/wobb/ihawk/ihawk.shtml)

  - [Israeli use of the HAWK
    system.](https://web.archive.org/web/20051216021746/http://www.waronline.org/IDF/Articles/PVO/hawk.htm)

  - [Causes of death among Belgian professional military radar
    operators: A 37-year retrospective cohort
    study](http://onlinelibrary.wiley.com/doi/10.1002/ijc.23988/pdf)
    (Englisch)

  - [Gesundheitsschäden durch militärische
    Radaranlagen](https://de.wikipedia.org/wiki/Gesundheitssch%C3%A4den_durch_milit%C3%A4rische_Radaranlagen)
    (German)

[Category:區域防空飛彈](../Category/區域防空飛彈.md "wikilink")
[Category:美國飛彈](../Category/美國飛彈.md "wikilink")