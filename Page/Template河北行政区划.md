[新华区](新华区_\(石家庄市\).md "wikilink"){{.w}}[裕华区](裕华区.md "wikilink"){{.w}}[井陉矿区](井陉矿区.md "wikilink"){{.w}}[藁城区](藁城区.md "wikilink"){{.w}}[鹿泉区](鹿泉区.md "wikilink"){{.w}}[栾城区](栾城区.md "wikilink"){{.w}}[辛集市](辛集市.md "wikilink"){{.w}}[晋州市](晋州市.md "wikilink"){{.w}}[新乐市](新乐市.md "wikilink"){{.w}}[井陉县](井陉县.md "wikilink"){{.w}}[正定县](正定县.md "wikilink"){{.w}}[行唐县](行唐县.md "wikilink"){{.w}}[灵寿县](灵寿县.md "wikilink"){{.w}}[高邑县](高邑县.md "wikilink"){{.w}}[深泽县](深泽县.md "wikilink"){{.w}}[赞皇县](赞皇县.md "wikilink"){{.w}}[无极县](无极县.md "wikilink"){{.w}}[平山县](平山县.md "wikilink"){{.w}}[元氏县](元氏县.md "wikilink"){{.w}}[赵县](赵县.md "wikilink")

|group3 = [唐山市](唐山市.md "wikilink") |list3 =
[路北区](路北区.md "wikilink"){{.w}}[路南区](路南区.md "wikilink"){{.w}}[古冶区](古冶区.md "wikilink"){{.w}}[开平区](开平区.md "wikilink"){{.w}}[丰南区](丰南区.md "wikilink"){{.w}}[丰润区](丰润区.md "wikilink"){{.w}}[曹妃甸区](曹妃甸区.md "wikilink")
{{.w}}[遵化市](遵化市.md "wikilink"){{.w}}[迁安市](迁安市.md "wikilink"){{.w}}[滦州市](滦州市.md "wikilink"){{.w}}[滦南县](滦南县.md "wikilink"){{.w}}[乐亭县](乐亭县.md "wikilink"){{.w}}[迁西县](迁西县.md "wikilink"){{.w}}[玉田县](玉田县.md "wikilink")

|group4 = [秦皇岛市](秦皇岛市.md "wikilink") |list4 =
[海港区](海港区_\(秦皇岛市\).md "wikilink"){{.w}}[山海关区](山海关区.md "wikilink"){{.w}}[北戴河区](北戴河区.md "wikilink"){{.w}}[抚宁区](抚宁区.md "wikilink"){{.w}}[昌黎县](昌黎县.md "wikilink"){{.w}}[卢龙县](卢龙县.md "wikilink"){{.w}}[青龙满族自治县](青龙满族自治县.md "wikilink")

|group5 = [邯郸市](邯郸市.md "wikilink") |list5 =
[丛台区](丛台区.md "wikilink"){{.w}}
[邯山区](邯山区.md "wikilink"){{.w}}
[复兴区](复兴区_\(邯郸市\).md "wikilink"){{.w}}[峰峰矿区](峰峰矿区.md "wikilink"){{.w}}[永年区](永年区.md "wikilink"){{.w}}[肥乡区](肥乡区.md "wikilink"){{.w}}[武安市](武安市.md "wikilink"){{.w}}[临漳县](临漳县.md "wikilink")
{{.w}}[成安县](成安县.md "wikilink"){{.w}}[大名县](大名县.md "wikilink")
{{.w}}[涉县](涉县.md "wikilink"){{.w}}[磁县](磁县.md "wikilink"){{.w}}[邱县](邱县.md "wikilink"){{.w}}[鸡泽县](鸡泽县.md "wikilink"){{.w}}[广平县](广平县.md "wikilink"){{.w}}[馆陶县](馆陶县.md "wikilink"){{.w}}[魏县](魏县.md "wikilink"){{.w}}[曲周县](曲周县.md "wikilink")

|group6 = [邢台市](邢台市.md "wikilink") |list6 =
[桥东区](桥东区_\(邢台市\).md "wikilink"){{.w}}[桥西区](桥西区_\(邢台市\).md "wikilink"){{.w}}[南宫市](南宫市.md "wikilink"){{.w}}[沙河市](沙河市.md "wikilink"){{.w}}[邢台县](邢台县.md "wikilink"){{.w}}[临城县](临城县.md "wikilink"){{.w}}[内丘县](内丘县.md "wikilink"){{.w}}[柏乡县](柏乡县.md "wikilink"){{.w}}[隆尧县](隆尧县.md "wikilink"){{.w}}[任县](任县.md "wikilink"){{.w}}[南和县](南和县.md "wikilink"){{.w}}[宁晋县](宁晋县.md "wikilink"){{.w}}[巨鹿县](巨鹿县.md "wikilink"){{.w}}[新河县](新河县.md "wikilink"){{.w}}[广宗县](广宗县.md "wikilink"){{.w}}[平乡县](平乡县.md "wikilink"){{.w}}[威县](威县.md "wikilink"){{.w}}[清河县](清河县.md "wikilink"){{.w}}[临西县](临西县.md "wikilink")

|group7 = [保定市](保定市.md "wikilink") |list7 =
[竞秀区](竞秀区.md "wikilink"){{.w}}[莲池区](莲池区.md "wikilink"){{.w}}[满城区](满城区.md "wikilink"){{.w}}[清苑区](清苑区.md "wikilink"){{.w}}[徐水区](徐水区.md "wikilink"){{.w}}[涿州市](涿州市.md "wikilink"){{.w}}[定州市](定州市.md "wikilink"){{.w}}[安国市](安国市.md "wikilink"){{.w}}[高碑店市](高碑店市.md "wikilink"){{.w}}[涞水县](涞水县.md "wikilink"){{.w}}[阜平县](阜平县.md "wikilink")
{{.w}}[定兴县](定兴县.md "wikilink"){{.w}}[唐县](唐县.md "wikilink"){{.w}}[高阳县](高阳县.md "wikilink"){{.w}}[容城县](容城县.md "wikilink"){{.w}}[涞源县](涞源县.md "wikilink"){{.w}}[望都县](望都县.md "wikilink"){{.w}}[安新县](安新县.md "wikilink"){{.w}}[易县](易县.md "wikilink"){{.w}}[曲阳县](曲阳县.md "wikilink"){{.w}}[蠡县](蠡县.md "wikilink"){{.w}}[顺平县](顺平县.md "wikilink"){{.w}}[博野县](博野县.md "wikilink"){{.w}}[雄县](雄县.md "wikilink")

|group8 = [张家口市](张家口市.md "wikilink") |list8 =
[桥东区](桥东区_\(张家口市\).md "wikilink"){{.w}}[桥西区](桥西区_\(张家口市\).md "wikilink"){{.w}}[宣化区](宣化区.md "wikilink"){{.w}}[下花园区](下花园区.md "wikilink"){{.w}}[万全区](万全区.md "wikilink"){{.w}}[崇礼区](崇礼区.md "wikilink"){{.w}}[张北县](张北县.md "wikilink"){{.w}}[康保县](康保县.md "wikilink"){{.w}}[沽源县](沽源县.md "wikilink"){{.w}}[尚义县](尚义县.md "wikilink"){{.w}}[蔚县](蔚县.md "wikilink"){{.w}}[阳原县](阳原县.md "wikilink"){{.w}}[怀安县](怀安县.md "wikilink"){{.w}}[怀来县](怀来县.md "wikilink"){{.w}}[涿鹿县](涿鹿县.md "wikilink"){{.w}}[赤城县](赤城县.md "wikilink")

|group9 = [承德市](承德市.md "wikilink") |list9 =
[双桥区](双桥区_\(承德市\).md "wikilink"){{.w}}[双滦区](双滦区.md "wikilink"){{.w}}[鹰手营子矿区](鹰手营子矿区.md "wikilink"){{.w}}[平泉市](平泉市.md "wikilink"){{.w}}[兴隆县](兴隆县.md "wikilink"){{.w}}[滦平县](滦平县.md "wikilink"){{.w}}[隆化县](隆化县.md "wikilink"){{.w}}[承德县](承德县.md "wikilink"){{.w}}[丰宁满族自治县](丰宁满族自治县.md "wikilink"){{.w}}[宽城满族自治县](宽城满族自治县.md "wikilink"){{.w}}[围场满族蒙古族自治县](围场满族蒙古族自治县.md "wikilink")

|group10 = [沧州市](沧州市.md "wikilink") |list10 =
[运河区](运河区.md "wikilink"){{.w}}[新华区](新华区_\(沧州市\).md "wikilink"){{.w}}[泊头市](泊头市.md "wikilink"){{.w}}[任丘市](任丘市.md "wikilink"){{.w}}[黄骅市](黄骅市.md "wikilink"){{.w}}[河间市](河间市.md "wikilink")
{{.w}}[沧县](沧县.md "wikilink")
{{.w}}[青县](青县.md "wikilink"){{.w}}[东光县](东光县.md "wikilink"){{.w}}[海兴县](海兴县.md "wikilink"){{.w}}[盐山县](盐山县.md "wikilink"){{.w}}[肃宁县](肃宁县.md "wikilink"){{.w}}[南皮县](南皮县.md "wikilink"){{.w}}[吴桥县](吴桥县.md "wikilink"){{.w}}[献县](献县.md "wikilink"){{.w}}[孟村回族自治县](孟村回族自治县.md "wikilink")

|group11 = [廊坊市](廊坊市.md "wikilink") |list11 =
[广阳区](广阳区.md "wikilink"){{.w}}[安次区](安次区.md "wikilink"){{.w}}[霸州市](霸州市.md "wikilink"){{.w}}[三河市](三河市.md "wikilink"){{.w}}[固安县](固安县.md "wikilink"){{.w}}[永清县](永清县.md "wikilink"){{.w}}[香河县](香河县.md "wikilink")
{{.w}}[大城县](大城县.md "wikilink"){{.w}}[文安县](文安县.md "wikilink"){{.w}}[大厂回族自治县](大厂回族自治县.md "wikilink")

|group12 = [衡水市](衡水市.md "wikilink") |list12 =
[桃城区](桃城区.md "wikilink"){{.w}}[冀州区](冀州区.md "wikilink"){{.w}}[深州市](深州市.md "wikilink"){{.w}}[枣强县](枣强县.md "wikilink"){{.w}}[武邑县](武邑县.md "wikilink"){{.w}}[武强县](武强县.md "wikilink"){{.w}}[饶阳县](饶阳县.md "wikilink"){{.w}}[安平县](安平县.md "wikilink"){{.w}}[故城县](故城县.md "wikilink"){{.w}}[景县](景县.md "wikilink"){{.w}}[阜城县](阜城县.md "wikilink")
}}

|group3style = text-align: center; |group3 = 1 单列管辖区 |list3 =

|belowstyle = text-align: left; font-size: 80%; |below =
注：标注“\*”的行政区为河北省单列管辖区（[正地级](地级行政区.md "wikilink")），由[河北省人民政府与](河北省人民政府.md "wikilink")[国务院国有资产监督管理委员会及其](国务院国有资产监督管理委员会.md "wikilink")[中国石油天然气集团公司共同管理](中国石油天然气集团.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](中华人民共和国县级以上行政区列表.md "wikilink")、[河北省乡级以上行政区列表](河北省乡级以上行政区列表.md "wikilink")
}}<noinclude>

</noinclude>

[河](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/河北行政区划.md "wikilink")
[河北模板](../Category/河北模板.md "wikilink")