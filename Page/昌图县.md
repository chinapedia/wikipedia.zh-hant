**昌图县**位于中国[辽宁省东北部](../Page/辽宁省.md "wikilink")，是辽宁省省管县制度单位（市级县，厅级县）。原辖属铁岭市，辽宁省第二个省管县单位。

昌图县为[辽宁肉类生产第一大、中国第八大县市](../Page/中国肉类生产百强县列表.md "wikilink")，面积4321.7平方千米，县政府驻昌图镇政府路。

## 历史

[清](../Page/清.md "wikilink")[嘉庆十一年](../Page/嘉庆.md "wikilink")（1806年）置昌图厅；[光绪三年](../Page/光绪.md "wikilink")（1877年）升厅为府；1913年改为昌图县，1946年4月析置昌北县，1954年昌北县并入後維持至今。原辖属铁岭市，2011年成为辽宁省第二个省管县单位

## 地理

### 名称

“昌图”系[蒙古语](../Page/蒙古语.md "wikilink")“常突额尔克（）”前两字的转音，意为“挂霜的悬崖”。

### 位置

昌图县位于[东经](../Page/东经.md "wikilink")123°32′-124°26′[北纬](../Page/北纬.md "wikilink")42°23′-43°29′之间，于辽宁省最北部，[辽河中游东岸](../Page/辽河.md "wikilink")，[松辽平原南缘](../Page/松辽平原.md "wikilink")，隶属[铁岭市](../Page/铁岭市.md "wikilink")，北与[吉林省](../Page/吉林省.md "wikilink")[四平市接壤](../Page/四平市.md "wikilink")，西与[内蒙古](../Page/内蒙古.md "wikilink")[科尔沁左翼后旗隔河相望](../Page/科尔沁左翼后旗.md "wikilink")。

### 气候

年降水量655毫米，属于温带季风[大陆性气候](../Page/溫帶大陸性氣候.md "wikilink")，四季分明，日照丰富。

## 交通

昌图县位于辽、吉、内蒙古三省（区）交界处，公路、铁路纵横交错，交通十分方便。昌图县城距[沈阳](../Page/沈阳.md "wikilink")[桃仙机场](../Page/桃仙机场.md "wikilink")120公里，距[大连港](../Page/大连港.md "wikilink")450公里；鐵路有[哈大铁路](../Page/哈大铁路.md "wikilink")、[平齐铁路](../Page/平齐铁路.md "wikilink")，总运行里程127公里，有车站12个；公路有[京哈高速公路](../Page/京哈高速公路.md "wikilink")、[102国道](../Page/102国道.md "wikilink")、[303国道](../Page/303国道.md "wikilink")，京哈高速公路在昌图设有三个出入口；鄉村基本都已經通柏油路。

## 区划人口

总人口102.15万，有[汉](../Page/汉族.md "wikilink")、[满](../Page/滿族.md "wikilink")、[回](../Page/回族.md "wikilink")、[朝等](../Page/朝鮮族.md "wikilink")21个民族，以汉族为主。全县有工业劳动力5万人，农业劳动力20万人。

下辖10个乡，23个镇，425个村民委员会。

  - 镇：昌图镇、老城镇、八面城镇、三江口镇、金家镇、宝力镇、泉头镇、双庙子镇、亮中桥镇、马仲河镇、毛家店镇、老四平镇、大洼镇、头道镇、鴜鹭树镇、傅家镇、四合镇、朝阳镇、古榆树镇、七家子镇、东嘎镇、四面城镇、前双井镇
  - 乡：大兴乡、十八家子乡、通江口乡、大四家子乡、后窑乡、长发乡、太平乡、下二台乡、平安堡乡、曲家店乡
  - 农场：三江口农场、宝力农场、新乡农场、付家林场、 牤牛马场、两家子农场

## 名人

  - [吴俊升](../Page/吳俊升_\(民國軍政人物\).md "wikilink")：民国时期东北保安副总司令、黑龙江省督军。
  - [梁中甲](../Page/梁中甲.md "wikilink")：中将旅长。
  - [赵晋阳](../Page/赵晋阳.md "wikilink")、[金田三](../Page/金田三.md "wikilink")、[孙西村](../Page/孙西村.md "wikilink")、[许芝](../Page/许芝.md "wikilink")、[刘勇新](../Page/刘勇新.md "wikilink")：為[抗戰烈士及革命](../Page/抗戰.md "wikilink")[烈士](../Page/烈士.md "wikilink")。
  - [王永志](../Page/王永志.md "wikilink")：载人航天工程总设计师，著名航天科学家，「[神舟五號](../Page/神舟五號.md "wikilink")」總設計師。
  - [刘树勋](../Page/刘树勋.md "wikilink")：著名桥梁专家、中国农工民主党副主席。
  - [何松亭](../Page/何松亭.md "wikilink")：金融专家、轻工部顾问。
  - [刘瑞森](../Page/刘瑞森.md "wikilink")：江西省委书记。
  - [张昭](../Page/张昭.md "wikilink")：工业部常务副部长。
  - [孙国封](../Page/孙国封.md "wikilink")：留美著名科学家。
  - [黄河](../Page/黄河.md "wikilink")、[艾连友](../Page/艾连友.md "wikilink")：核工业专家。
  - [任景文](../Page/任景文.md "wikilink")：化工专家。
  - [翟玉林](../Page/翟玉林.md "wikilink")：能源专家。
  - [刘涌泉和](../Page/刘涌泉.md "wikilink")[刘万](../Page/刘万.md "wikilink")：宝石鉴定专家。
  - [端木蕻良](../Page/端木蕻良.md "wikilink")：著名作家、[红学家](../Page/红学.md "wikilink")。
  - [舒凡](../Page/舒凡.md "wikilink")：剧作家。
  - [佟韦](../Page/佟韦.md "wikilink")：原中国书法家协会副主席。
  - [孙奇](../Page/孙奇.md "wikilink")：原辽宁省政协主席。
  - [梁肃戎](../Page/梁肃戎.md "wikilink")：海峡两岸和平统一促进会会长、原中華民國立法院院长。
  - [辛曉琪](../Page/辛曉琪.md "wikilink")：臺灣歌手，昌圖縣是其祖籍。
  - [李松涛](../Page/李松涛.md "wikilink")：著名诗人。
  - [吕刚](../Page/吕刚.md "wikilink")：世界举重冠军。
  - [李卓](../Page/李卓.md "wikilink")：亚洲举重冠军。

## 著名学府

昌图高中：昌图高中创建于1948年11月，前身是1906年昌图府创办的昌图府学堂。1981年3月18日，辽宁省对昌图高中按重点高中标准进行检查验收，由辽宁省人民政府下达档，确定昌图高中为辽宁省首批办好的十七所重点中学之一。昔日，昌图高中也被昌图县百姓称作“老城高中”。1965届昌图高中毕业生升学率达88%，成为辽宁省第四名（当时全省平均升学率只有50%）。\[1\]

## 资源

  - 矿藏主要有[铁](../Page/铁.md "wikilink")、[铜](../Page/铜.md "wikilink")、[锌](../Page/锌.md "wikilink")、[铅](../Page/铅.md "wikilink")、[煤](../Page/煤.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[萤石](../Page/萤石.md "wikilink")、黄土、[大理石](../Page/大理石.md "wikilink")、河沙、[石油](../Page/石油.md "wikilink")、[天然气](../Page/天然气.md "wikilink")。
  - 中草药有[五味子](../Page/五味子.md "wikilink")、[桔梗](../Page/桔梗.md "wikilink")、[柴胡](../Page/柴胡.md "wikilink")、[威灵仙](../Page/威灵仙.md "wikilink")、[杏仁](../Page/杏仁.md "wikilink")、[车前子](../Page/车前子.md "wikilink")、[茵陈](../Page/茵陈.md "wikilink")、[地丁](../Page/地丁.md "wikilink")、[防风](../Page/防风.md "wikilink")、[知母](../Page/知母.md "wikilink")、[苍术](../Page/苍术.md "wikilink")、[玉竹](../Page/玉竹.md "wikilink")、[苦参](../Page/苦参.md "wikilink")、[天南星等](../Page/天南星.md "wikilink")。
  - 野生动物有[蛇](../Page/蛇.md "wikilink")、[蜥蜴](../Page/蜥蜴.md "wikilink")、[蟾蜍](../Page/蟾蜍.md "wikilink")、[山鸡](../Page/山鸡.md "wikilink")、[野鸭](../Page/野鸭.md "wikilink")、[鹌鹑等](../Page/鹌鹑.md "wikilink")。

[昌图县](../Category/昌图县.md "wikilink")
[县](../Category/铁岭区县市.md "wikilink")
[铁岭](../Category/辽宁省县份.md "wikilink")

1.  [昌圖高中-新加坡國立大學博客](http://blog.nus.edu.sg/xuping/2017/02/27/changtuhighschool/)