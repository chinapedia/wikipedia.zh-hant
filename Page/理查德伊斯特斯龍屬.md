**理查德伊斯特斯龍屬**（[學名](../Page/學名.md "wikilink")：*Richardoestesia*），又名**里約龍**，是[獸腳亞目的一](../Page/獸腳亞目.md "wikilink")[屬中等](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於[上白堊紀的](../Page/上白堊紀.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。

## 化石及分類

牠的[化石包括了一對](../Page/化石.md "wikilink")[齒骨及大量的個別](../Page/齒骨.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")，都是發現於[省立恐龍公園](../Page/省立恐龍公園.md "wikilink")。齒骨修長；齒骨的側面有明顯的凹溝，類似[始祖鳥](../Page/始祖鳥.md "wikilink")、[傷齒龍科及某些](../Page/傷齒龍科.md "wikilink")[馳龍科](../Page/馳龍科.md "wikilink")。牙齒小，有細微的鋸齒邊緣。在[馬蹄峽谷組及Scollard地層都有發現牠的牙齒](../Page/馬蹄峽谷組.md "wikilink")，而在[蘭斯組亦極為常見](../Page/蘭斯組.md "wikilink")，上述地層都是[白堊紀晚期的地層](../Page/白堊紀.md "wikilink")。[近爪牙龍的牙齒有可能其實是屬於理查德伊斯特斯龍的](../Page/近爪牙龍.md "wikilink")。

有理論認為理查德伊斯特斯龍是以[魚類為食的](../Page/魚類.md "wikilink")，就像是[鷺一樣](../Page/鷺.md "wikilink")。由於只有少量的資料，牠的演化與分類關係並不清楚。而牠的屬名是為紀念學者[理查德·伊斯特斯](../Page/理查德·伊斯特斯.md "wikilink")（Richard
Estes）而取的，他曾對上白堊紀的小型[脊椎動物研究有極大貢獻](../Page/脊椎動物.md "wikilink")。

## 命名混淆

[科學家在描述理查德伊斯特斯龍時](../Page/科學家.md "wikilink")，有時會使用「*Ricardoestesia*」這個拼法。[喬治·奧利舍夫斯基](../Page/喬治·奧利舍夫斯基.md "wikilink")（George
Olshevsky）曾於1991年一度想修正現有有「h」的拼法，但不幸的是他本人也不其然的使用了現有的拼法。而根據[國際動物命名法規](../Page/國際動物命名法規.md "wikilink")，他作為第一校訂者而令現有的拼法成為官方學名。後來，原有的學者亦使用了現今的拼法。

## 參考資料

  - Baszio, S. 1997. Investigations on Canadian dinosaurs: systematic
    palaeontology of isolated dinosaur teeth from the Latest Cretaceous
    of Dinosaur Provincial Park, Alberta, Canada. Courier
    Forschunginstitut Senckenberg 196:33-77.
  - Currie, P. J., K. J. Rigby, and R. E. Sloan. 1990. Theropod teeth
    from the Judith River Formation of southern Alberta, Canada. Pp.
    107-125. In P. J. Currie, and K. Carpenter, eds. Dinosaur
    Systematics: Perspectives and Approaches. Cambridge University
    Press, Cambridge.
  - Sankey, J. T., D. B. Brinkman, M. Guenther, and P. J. Currie. 2002.
    Small theropod and bird teeth from the Late Cretaceous (Late
    Campanian) Judith River Group, Alberta. Journal of Paleontology
    76(4):751-763.

## 外部連結

  - [提及理查德伊斯特斯龍的誤拼](http://dml.cmnh.org/1997Nov/msg00416.html)
  - [*Ricardoestesia*對*Richardoestesia*的討論](http://dml.cmnh.org/2002Jul/msg00530.html)

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:馳龍科](../Category/馳龍科.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")