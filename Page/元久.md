**元久**（1204年二月二十日至1206年二月二十日）是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")。[後鳥羽上皇實施](../Page/後鳥羽上皇.md "wikilink")[院政](../Page/院政.md "wikilink")。這個時代的[天皇是](../Page/天皇.md "wikilink")[土御門天皇](../Page/土御門天皇.md "wikilink")。[鎌倉幕府之](../Page/鎌倉幕府.md "wikilink")[征夷大將軍為](../Page/征夷大將軍.md "wikilink")[源實朝](../Page/源實朝.md "wikilink")；[執權為](../Page/執權.md "wikilink")[北條義時](../Page/北條義時.md "wikilink")。

## 改元

  - 建仁四年二月二十日（1204年3月23日）改元元久
  - 元久三年四月二十七日（1206年6月5日）改元建永

## 出處

《[毛詩正義](../Page/毛詩.md "wikilink")·大雅·文王》：「但文王自於國內**建元**久矣，無故更復改元」。

## 出生

## 逝世

  - 元久元年：[源賴家](../Page/源賴家.md "wikilink")，鎌倉幕府之第二代將軍。

## 大事記

  - 元久二年：《[新古今和歌集](../Page/新古今和歌集.md "wikilink")》成書。
  - 元久二年：北條義時就任鎌倉幕府之第二代執權。

## 紀年、西曆、干支對照表

|                                    |                                |                                |                                |
| ---------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| 元久                                 | 元年                             | 二年                             | 三年                             |
| [儒略曆日期](../Page/儒略曆.md "wikilink") | 1204年                          | 1205年                          | 1206年                          |
| [干支](../Page/干支.md "wikilink")     | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [嘉泰](../Page/嘉泰.md "wikilink")（1201年正月至1204年十二月）：[宋](../Page/南宋.md "wikilink")—[宋寧宗趙擴之年號](../Page/宋寧宗.md "wikilink")
      - [開禧](../Page/開禧.md "wikilink")（1205年正月至1207年十二月）：宋—宋寧宗趙擴之年號
      - [鳳曆](../Page/鳳曆_\(大理\).md "wikilink")：[大理](../Page/大理國.md "wikilink")—[段智廉之年號](../Page/段智廉.md "wikilink")
      - [元壽](../Page/元壽_\(段智廉\).md "wikilink")：大理—段智廉之年號
      - [天開](../Page/天開.md "wikilink")（1205年至1225年）：大理—段智祥之年號
      - [天嘉寳祐](../Page/天嘉寳祐.md "wikilink")（1202年至1204年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李龍翰之年號](../Page/李龍翰.md "wikilink")
      - [治平龍應](../Page/治平龍應.md "wikilink")（1205年至1210年）：李朝—李龍翰之年號
      - [天禧](../Page/天禧_\(耶律直魯古\).md "wikilink")（1178年至1211年）：[西遼](../Page/西遼.md "wikilink")—末主[耶律直魯古之年號](../Page/耶律直魯古.md "wikilink")
      - [泰和](../Page/泰和_\(金章宗\).md "wikilink")（1201年正月至1208年十二月）：[金](../Page/金朝.md "wikilink")—[金章宗完顏璟之年號](../Page/金章宗.md "wikilink")
      - [天慶](../Page/天慶_\(李純祐\).md "wikilink")（1194年正月至1206年正月）：[西夏](../Page/西夏.md "wikilink")—夏桓宗[李純祐之年號](../Page/李純祐.md "wikilink")
      - [應天](../Page/應天_\(李安全\).md "wikilink")（1206年正月至1209年十二月）：西夏—夏襄宗[李安全之年號](../Page/李安全.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129
  - 所功，《年号の歴史―元号制度の史的研究》，雄山閣，1988年3月，ISBN 4639007116

[Category:13世纪日本年号](../Category/13世纪日本年号.md "wikilink")
[Category:1200年代日本](../Category/1200年代日本.md "wikilink")