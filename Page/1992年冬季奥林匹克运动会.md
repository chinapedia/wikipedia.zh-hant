</div>

</div>

**第十六届冬季奥林匹克运动会**（，），于1992年2月8日至2月23日在[法国的](../Page/法国.md "wikilink")[阿尔贝维尔举行](../Page/阿尔贝维尔.md "wikilink")，該屆也是最後一次跟夏季奧運同一年舉辦的冬季奧運會。

## 申办

除[阿尔贝维尔外](../Page/阿尔贝维尔.md "wikilink")，本届冬奥会的其它六个候选城市分别是[美国的](../Page/美国.md "wikilink")[安克雷奇](../Page/安克雷奇.md "wikilink")、[德国的](../Page/德国.md "wikilink")[贝希特斯加登](../Page/贝希特斯加登.md "wikilink")、[意大利的](../Page/意大利.md "wikilink")[科尔蒂纳丹佩佐](../Page/科尔蒂纳丹佩佐.md "wikilink")、[挪威的](../Page/挪威.md "wikilink")[利勒哈默尔](../Page/利勒哈默尔.md "wikilink")、[瑞典的](../Page/瑞典.md "wikilink")[法伦和](../Page/法伦.md "wikilink")[保加利亚首都](../Page/保加利亚.md "wikilink")[索非亚](../Page/索非亚.md "wikilink")。

## 焦点

## 比赛项目

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/1992年冬季奥林匹克运动会雪上项目.md" title="wikilink">高山滑雪</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会雪上项目.md" title="wikilink">冬季两项</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会雪上项目.md" title="wikilink">有舵雪橇</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会雪上项目.md" title="wikilink">越野滑雪</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会冰上项目.md" title="wikilink">花样滑冰</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会雪上项目.md" title="wikilink">无舵雪橇</a></li>
</ul></td>
<td></td>
<td><ul>
<li><a href="../Page/1992年冬季奥林匹克运动会雪上项目.md" title="wikilink">北欧两项</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会雪上项目.md" title="wikilink">自由式滑雪</a>（技巧）</li>
<li><a href="../Page/1992年冬季奥林匹克运动会冰上项目.md" title="wikilink">冰球</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会雪上项目.md" title="wikilink">跳台滑雪</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会冰上项目.md" title="wikilink">速度滑冰</a></li>
<li><a href="../Page/1992年冬季奥林匹克运动会冰上项目.md" title="wikilink">短道速滑</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 表演项目

  - [冰壶](../Page/冰壶.md "wikilink")
  - 自由式滑雪（空中技巧、雪上芭蕾）
  - 速度滑雪

## 参赛国家和地区

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

## 奖牌榜

<span style="font-size:smaller;">此为奖牌榜前10位，详见</span>
*[1992年冬季奥林匹克运动会奖牌榜](../Page/1992年冬季奥林匹克运动会奖牌榜.md "wikilink")*

<table>
<thead>
<tr class="header">
<th><p>colspan=5 style="border-right:0px;";| <strong>1992年冬季奥林匹克运动会奖牌榜</strong></p></th>
<th><p>style="border-left:0px"; | <a href="https://zh.wikipedia.org/wiki/File:Olympic_rings.svg" title="fig:Olympic_rings.svg">Olympic_rings.svg</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>排名</strong></p></td>
<td><p><strong>国家</strong></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>总 计</strong></p></td>
<td><p>57</p></td>
</tr>
</tbody>
</table>

## 参考

  - [中国奥委会官方网站](http://www.olympic.cn)

## 外部链接

  - [国际奥委会有关1992年冬奥会资料](http://www.olympic.org/uk/games/past/index_uk.asp?OLGT=2&OLGY=1992)

{{-}}

[Category:冬季奥林匹克运动会](../Category/冬季奥林匹克运动会.md "wikilink")
[Category:1992年體育](../Category/1992年體育.md "wikilink")
[Category:法国体育史](../Category/法国体育史.md "wikilink")
[Category:1992年2月](../Category/1992年2月.md "wikilink")