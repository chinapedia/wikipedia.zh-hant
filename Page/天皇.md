[Flag_of_the_Japanese_Emperor.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Japanese_Emperor.svg "fig:Flag_of_the_Japanese_Emperor.svg")

**天皇**（）是[日本的](../Page/日本.md "wikilink")[君主](../Page/君主.md "wikilink")，為當今世界唯一使用[皇帝名號的](../Page/皇帝.md "wikilink")[國家元首](../Page/國家元首.md "wikilink")，以其為首的[日本皇室則是世界上现存最古老的](../Page/日本皇室.md "wikilink")[皇室](../Page/皇室.md "wikilink")。在[明治維新至](../Page/明治維新.md "wikilink")1947年期間施行的《[大日本帝國憲法](../Page/大日本帝國憲法.md "wikilink")》明文規定天皇為[日本的國家元首](../Page/日本.md "wikilink")\[1\]；之後接續施行至今的《[日本國憲法](../Page/日本國憲法.md "wikilink")》則定義其為「日本國以及[日本國民整體的](../Page/日本.md "wikilink")[象徵](../Page/國家象徵.md "wikilink")」，為[有實無名的](../Page/象徵天皇制.md "wikilink")[虛位元首](../Page/虛位元首.md "wikilink")。主要職責是任命[首相](../Page/日本內閣總理大臣.md "wikilink")、批准[國務大臣的任免](../Page/國務大臣_\(日本\).md "wikilink")、簽署[法律和](../Page/法律.md "wikilink")[命令](../Page/命令.md "wikilink")、以及進行禮儀性的外交事務等。

天皇族系號稱「萬世一系」，從首任的[神武天皇以來一脈相傳](../Page/神武天皇.md "wikilink")，並不像[中国](../Page/中国.md "wikilink")、[朝鮮](../Page/朝鮮_\(稱謂\).md "wikilink")、[越南等](../Page/越南.md "wikilink")[周邊國家经历過改朝换代](../Page/汉字文化圈.md "wikilink")，這是由於天皇在[歷史上能掌握實權的時間很短](../Page/日本歷史.md "wikilink")，反而能避過戰亂，使皇室更加長壽。由於天皇與整個皇室在日本古代被認為是超乎普通人的存在，因此時至今日都沒有[姓氏](../Page/姓氏.md "wikilink")（历史学研究上称其为[天皇氏或](../Page/天皇氏.md "wikilink")[天皇家](../Page/天皇家.md "wikilink")）。而在[神道教傳統中](../Page/神道.md "wikilink")，天皇被認為是[天照大神的後裔](../Page/天照大神.md "wikilink")，故具有「[神性](../Page/神_\(神道\).md "wikilink")」；但[二戰後](../Page/二戰.md "wikilink")，[昭和天皇發表](../Page/昭和天皇.md "wikilink")《[人間宣言](../Page/人間宣言.md "wikilink")》，放棄天皇被賦予的神性。

通常，現任天皇被稱為「[今上天皇](../Page/今上天皇.md "wikilink")」，敬稱「[陛下](../Page/陛下.md "wikilink")」。[退位後被稱為](../Page/退位.md "wikilink")「[太上天皇](../Page/太上天皇.md "wikilink")」（簡稱上皇）；若其退位後[出家則被稱為](../Page/出家.md "wikilink")「[太上法皇](../Page/太上法皇.md "wikilink")」（簡稱法皇）；天皇[駕崩時](../Page/駕崩.md "wikilink")，將先被稱為「[大行天皇](../Page/大行天皇.md "wikilink")」，之後再正式諱稱為「［在位時年號］天皇」（例如[明治天皇](../Page/明治天皇.md "wikilink")）。

現任天皇（第125任）為[明仁](../Page/明仁.md "wikilink")
，是[昭和天皇](../Page/昭和天皇.md "wikilink")
的長子，年号[平成](../Page/平成.md "wikilink") ，於1989年1月7日 即位。

## 名稱由來

「天皇」又可稱[天帝](../Page/天帝.md "wikilink")，語源源自中國[道教的](../Page/道教.md "wikilink")[天帝即](../Page/天帝.md "wikilink")[天皇大帝](../Page/天皇大帝.md "wikilink")。日本方面最早的文字记载是《》，由第40代[天武天皇制定](../Page/天武天皇.md "wikilink")，689年頒佈。

早期[日本历史於西元第一至三世纪](../Page/日本历史.md "wikilink")，小邦林立，各小邦的[君主称](../Page/君主.md "wikilink")“[王](../Page/王.md "wikilink")”或“[君](../Page/君.md "wikilink")”，约到西元第四、五世纪诸邦形成[联盟](../Page/联盟.md "wikilink")，[首领改称为](../Page/首领.md "wikilink")「[大王](../Page/大王.md "wikilink")」或「[大君](../Page/大君.md "wikilink")」，以及稱「[治天下大王](../Page/治天下大王.md "wikilink")」。到了天武天皇時代，受到同時期自稱天皇的[唐高宗所影響](../Page/唐高宗.md "wikilink")，又因崇尚中國道教的北斗信仰，選用天皇成為稱謂。其後还曾有天皇和「[皇帝](../Page/皇帝.md "wikilink")」称号并用的情况，如43代元明、45代圣武（追谥）、46代孝谦、50代桓武。民間過往亦稱天皇為「君上」或「大君」。

外交场合使用首次“天皇”一词据称是推古天皇时期，[日本書紀記載](../Page/日本書紀.md "wikilink")，[推古天皇时第二次出使](../Page/推古天皇.md "wikilink")[隋朝的](../Page/隋朝.md "wikilink")[国书中有](../Page/国书.md "wikilink")“东天皇敬白西皇帝”之句\[2\]，但是中國對此並無任何記載、可能是天武天皇欲加強其名銜正當性而杜撰的\[3\]。到了[明治元年](../Page/明治.md "wikilink")（1868年）后，日本致外国首脑信件、国际条约批准书、宣战诏书使用的都还是[皇帝称号](../Page/皇帝.md "wikilink")。1936年推行[大東亞共榮圈的口号後](../Page/大東亞共榮圈.md "wikilink")，对外就完全使用天皇称号。中国称日本元首为日本天皇大约是在清末的[同治时期](../Page/同治.md "wikilink")。

## 歷史

[缩略图](https://zh.wikipedia.org/wiki/File:Politics_Under_Constitution_of_Japan_04.svg "fig:缩略图")

### 古代

[日本神話書籍](../Page/日本神話.md "wikilink")《[日本書紀](../Page/日本書紀.md "wikilink")》称，天皇乃是[太陽神](../Page/太陽神.md "wikilink")[天照大神之後裔](../Page/天照大神.md "wikilink")，此理論成为日本[君权神授的依据](../Page/君权神授.md "wikilink")。旧时日本史书一直宣称天皇是「」，即所有天皇都来自同一家族，[日本历史上](../Page/日本历史.md "wikilink")，[皇室从来没有出现过家族更迭](../Page/皇室.md "wikilink")。事实上，中古时期的日本小国林立，最后由位于[本州岛中部](../Page/本州岛.md "wikilink")（[奈良縣](../Page/奈良縣.md "wikilink")）的[倭朝廷实现统一](../Page/倭朝廷.md "wikilink")，[倭](../Page/大和國.md "wikilink")（）也就成了日本的别称。倭朝廷成立前出现过多次[王位的争夺](../Page/國王.md "wikilink")，而當時歷史紀錄靠史官口頭背誦，沒有留下可供佐證的文字史料，是東亞國家中除中國之外，存留有最早文字史料的國家即是日本。韓國的《百濟記》、《百濟新撰》、《百濟本記》據傳比日本書紀早，卻除了《日本書紀》所引用的部分之外完全失傳。今存的《三國史記》整整比《日本書紀》晚了數個[世紀](../Page/世紀.md "wikilink")，僅有[中國歷史學家](../Page/中國歷史.md "wikilink")[陳壽](../Page/陳壽.md "wikilink")《[三國志](../Page/三國志.md "wikilink")》的東夷傳[倭人條可做參考](../Page/倭人.md "wikilink")。日本是在[律令制开始后才有](../Page/律令制.md "wikilink")「天皇」一名。在《日本國史略》中有「自天照皇太神創業垂統，而[神武天皇初都中國](../Page/神武天皇.md "wikilink")、一統天下，歷正天皇正統一系，亘萬世而不革。天下即一人之天下。」一辭，不過萬世一系四字則是到19世纪末[明治维新左右才形成並寫入](../Page/明治维新.md "wikilink")《[大日本帝国宪法](../Page/大日本帝国宪法.md "wikilink")》之中。

自公元6世纪[倭国征服](../Page/倭王權.md "wikilink")[本州及](../Page/本州.md "wikilink")[九州大部分地区后](../Page/九州_\(日本\).md "wikilink")，天皇的权力达到顶峰。但天皇作为日本实际统治者的时间并不长。从10世纪开始，日本经历了[摄关政治](../Page/摄关政治.md "wikilink")、[源](../Page/源氏.md "wikilink")[平相争](../Page/平氏.md "wikilink")，政權[被權貴門閥把持](../Page/攝關政治.md "wikilink")；有些天皇甚至負氣[禪讓](../Page/禪讓.md "wikilink")，自稱[上皇](../Page/上皇.md "wikilink")，在[江湖之中實施](../Page/江湖.md "wikilink")[院政](../Page/院政.md "wikilink")；或者[剃髮](../Page/剃髮.md "wikilink")[出家](../Page/出家.md "wikilink")，自稱[法皇](../Page/法皇.md "wikilink")，在[伽藍之中掌控大權](../Page/伽藍.md "wikilink")。12世紀以後，日本進入數世紀的[武家政權](../Page/武家政權.md "wikilink")（[軍事政權](../Page/軍政.md "wikilink")）時代，政府被[幕府將軍或者僭稱](../Page/征夷大將軍.md "wikilink")[公卿的](../Page/公卿.md "wikilink")[武家階層掌控](../Page/武家.md "wikilink")，包括了[平氏政權](../Page/平氏政權.md "wikilink")、[镰仓幕府](../Page/镰仓幕府.md "wikilink")、[室町幕府](../Page/室町幕府.md "wikilink")、[織豐政權與](../Page/安土桃山时代.md "wikilink")[江户幕府](../Page/江户幕府.md "wikilink")。天皇的[政治權力及存在作用被架空長達千年](../Page/政治權力.md "wikilink")。

至1867年[明治天皇登基後](../Page/明治天皇.md "wikilink")，江戶幕府迫於時局而行[將政權交還朝廷](../Page/大政奉還.md "wikilink")，武家統治時代結束，天皇重新掌握政治权力。在明治天皇與[伊藤博文等](../Page/伊藤博文.md "wikilink")[元老的主導下](../Page/元老_\(日本\).md "wikilink")，日本引進歐陸國家的[二元制君主立憲制](../Page/二元制君主立憲制.md "wikilink")、並在1890年施行《[大日本帝國憲法](../Page/大日本帝國憲法.md "wikilink")》，天皇成為「神聖不可侵犯」的統治者，擁有絕對的政治權力。另一方面，當時的日本政府將[神道信仰發展成近乎](../Page/神道.md "wikilink")[國教地位的](../Page/國教.md "wikilink")「[國家神道](../Page/國家神道.md "wikilink")」，由於天皇在傳統上被認為是[天照大神後裔](../Page/天照大神.md "wikilink")，因此天皇被[神格化尊為](../Page/神格化.md "wikilink")「」，意為以人的型態存在於現世的神。

### 近代

[昭和天皇登基後不久](../Page/昭和天皇.md "wikilink")，日本在[東條英機等軍部帶領下投入](../Page/東條英機.md "wikilink")[侵略中國與](../Page/中國抗日戰爭.md "wikilink")[第二次世界大戰戰事中](../Page/第二次世界大戰.md "wikilink")。二戰後，日本被以[美國為首的](../Page/美國.md "wikilink")[同盟國佔領](../Page/同盟國_\(第二次世界大戰\).md "wikilink")，並在[軍政統治下建立](../Page/同盟國軍事佔領日本.md "wikilink")[议会民主制](../Page/议会民主制.md "wikilink")；同盟國原計畫廢除天皇制度，但為穩定局勢，允许保留天皇作为象征性的国家元首，即「[象徵天皇制](../Page/象徵天皇制.md "wikilink")」。1946年，昭和天皇在[同盟國佔領當局的主導下发表了](../Page/盟軍最高司令官總司令部.md "wikilink")《[人间宣言](../Page/人间宣言.md "wikilink")》，承认天皇是人而非神。

依照《[日本國憲法](../Page/日本國憲法.md "wikilink")》，現今日本天皇的主要职责是任命[内阁总理大臣](../Page/日本內閣總理大臣.md "wikilink")（首相），批准法律、政令及条约，召集国会，批准[国务大臣的任免](../Page/国务大臣.md "wikilink")，出席礼仪性的外交事務活動和国家儀典等。

## 日本以外的称呼

### 中華民國

[中華民國政府與民間](../Page/中華民國.md "wikilink")，一般直接稱呼**天皇**。

### 中華人民共和國

[中華人民共和国政府一般直接使用](../Page/中華人民共和国政府.md "wikilink")[日文汉字称其](../Page/日文.md "wikilink")“天皇”\[4\]。

### 朝鮮半島

朝鲜半岛受传统[华夷思想影响](../Page/华夷.md "wikilink")，认为只有过去中国的君主才可以称为“皇帝”。因此除了[日本殖民统治时期外](../Page/朝鲜日治时期.md "wikilink")，朝鲜半岛一般称日本天皇为“日王”（，“日本国王”的简称）\[5\]。

### 英语

[英語的称呼是](../Page/英語.md "wikilink")「Emperor」、「Emperor of
Japan」，也有音译为「Tenno」者。

## 三神器

天皇的[三神器包括](../Page/三神器.md "wikilink")：

  - [八咫鏡](../Page/八咫鏡.md "wikilink")：一柄镜子，现在陈列于[三重县](../Page/三重县.md "wikilink")[伊势市的](../Page/伊势市.md "wikilink")[伊势神宫](../Page/伊势神宫.md "wikilink")\[6\]。
  - [天叢雲劍](../Page/天叢雲劍.md "wikilink")：又名「草薙劍」，源于創世神话中[素戔嗚尊的寶剑](../Page/素戔嗚尊.md "wikilink")，他斬殺[八岐大蛇後從其尾部取得](../Page/八岐大蛇.md "wikilink")。现在供奉于[名古屋市](../Page/名古屋.md "wikilink")[热田区的](../Page/热田区.md "wikilink")[热田神宫](../Page/热田神宫.md "wikilink")\[7\]。
  - [八尺瓊勾玉](../Page/八尺瓊勾玉.md "wikilink")：又稱「八阪瓊曲玉」，一枚勾玉。现在供奉于[东京的](../Page/东京.md "wikilink")[皇居](../Page/皇居.md "wikilink")。

日本神話中，天叢雲劍是素戔嗚尊斬殺八岐大蛇而得。關於天叢雲劍後來的下落，有一說指出，1185年日本两大武士集团平氏和源氏在[坛之浦海域决战](../Page/坛之浦.md "wikilink")，源氏获胜，而平氏所立的[安德天皇带着此剑葬身海底](../Page/安德天皇.md "wikilink")；另有一說指出，[日本武尊带着此劍在海底長眠](../Page/日本武尊.md "wikilink")。今日熱田神宮所供之天叢雲劍是否為傳說中的寶劍，實已不得而知。至於八咫鏡以及八尺瓊勾玉都在日本「記紀神話」（即《[古事記](../Page/古事記.md "wikilink")》和《[日本書紀](../Page/日本書紀.md "wikilink")》）裡和[天照大神有關故事的部分中被提及](../Page/天照大神.md "wikilink")；名稱中的「八咫」與「八尺」原先只是在形容這兩者的尺寸相當大（有人说八尺是周朝尺寸），後來就變為這兩種神器的專屬名稱\[8\]\[9\]。

三神器在[日本历史上通常是上任天皇传给下任天皇](../Page/日本历史.md "wikilink")。偶尔也有通过各種手段争夺的时候，如[南北朝时期](../Page/南北朝_\(日本\).md "wikilink")，北朝[後小松天皇用下任天皇繼承權做交換](../Page/後小松天皇.md "wikilink")，誘騙南朝[後龜山天皇交出所保有的三神器](../Page/後龜山天皇.md "wikilink")，南朝天皇在交出三神器後退位，但皇統最後由後小松天皇的兒子繼承，到此日本南北朝的分裂合而為一。

## 圖集

<File:Nijuubashi2.jpg>|[皇居內的二重桥](../Page/皇居.md "wikilink")。
<File:Emperor> Jimmu.jpg|[神武天皇](../Page/神武天皇.md "wikilink")，傳說中初代的天皇。
<File:Meiji>
Kenpo03.jpg|[大日本帝国憲法第](../Page/大日本帝国憲法.md "wikilink")3頁，[明治天皇除了以睦仁之名署名外](../Page/明治天皇.md "wikilink")、亦蓋上了「天皇御璽」的印章（）。
[File:三神器.png|三神器（劍、鏡、玉）的想像外觀](File:三神器.png%7C三神器（劍、鏡、玉）的想像外觀)

## 注釋

## 參考文獻

### 引用

### 来源

  - 小森洋一：《天皇的玉音放送》，三联书店，2004年8月 ISBN 7-108-02146-3
  - 小森洋一：《日本近代国语批判》，吉林人民出版社，2003年12月 ISBN 720604400X
  - [天皇的稱號 - 重新解讀日本歷史 -
    全球書選](http://mag.udn.com/mag/world/storypage.jsp?f_ART_ID=455325)
    - udn全球觀察,
    2013年6月7日（[網野善彥](../Page/網野善彥.md "wikilink")：《重新解讀日本歷史》，[聯經出版](../Page/聯經出版.md "wikilink")）

## 参见

  - [今上天皇](../Page/今上天皇.md "wikilink")：即当今在位的天皇，现指[明仁](../Page/明仁.md "wikilink")。
  - [太上天皇](../Page/太上天皇.md "wikilink")：退位的天皇，相当于[中国的](../Page/中国.md "wikilink")[太上皇](../Page/太上皇.md "wikilink")。
  - [太上法皇](../Page/太上法皇.md "wikilink")：[出家的太上天皇](../Page/出家.md "wikilink")。
  - [日本皇室](../Page/日本皇室.md "wikilink")
      - [日本皇室世系图](../Page/日本皇室世系图.md "wikilink")
  - [象徵天皇制](../Page/象徵天皇制.md "wikilink")
  - [天皇特例會見](../Page/天皇特例會見.md "wikilink")
  - [天皇誕生日](../Page/天皇誕生日.md "wikilink")
  - [日本天皇列表](../Page/日本天皇列表.md "wikilink")
  - [日本皇后列表](../Page/日本皇后列表.md "wikilink")
  - [日本內閣總理大臣](../Page/日本內閣總理大臣.md "wikilink")

{{-}}

[日本天皇](../Category/日本天皇.md "wikilink")
[Category:日本君主称谓](../Category/日本君主称谓.md "wikilink")
[Category:皇帝](../Category/皇帝.md "wikilink")
[Category:日本國家象徵](../Category/日本國家象徵.md "wikilink")
[Category:世界之最](../Category/世界之最.md "wikilink")

1.  新村出『広辞苑 第六版』（岩波書店、2011年）1952頁；松村明『大辞林 第三版』（三省堂、2006年) 1758頁
2.  《日本書紀・推古天皇》：「九月辛未朔乙亥、饗客等於難波大郡。辛巳、唐客裴世淸罷歸。則復以小野妹子臣爲大使、吉士雄成爲小使、福利爲通事、副于唐客而遺之。爰天皇聘唐帝、其辭曰「東天皇敬白西皇帝。使人鴻臚寺掌客裴世淸等至、久憶方解。季秋薄冷、尊何如、想淸悆。此卽如常。今遣大禮蘇因高・大禮乎那利等往。謹白不具。」。」
3.  古代東亞政治環境中天皇與日本國的產生 呂玉新
4.
5.
6.
7.
8.  新訂 古事記
9.  新編 日本書紀