****是历史上第十八次航天飞机任务，也是[发现号航天飞机的第五次太空飞行](../Page/發現號太空梭.md "wikilink")。

## 任务成员

  - **[丹尼尔·布兰登斯坦](../Page/丹尼尔·布兰登斯坦.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[约翰·克雷顿](../Page/约翰·克雷顿.md "wikilink")**（，曾执行、以及任务），飞行员
  - **[珊农·露茜德](../Page/珊农·露茜德.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[约翰·法比安](../Page/约翰·法比安.md "wikilink")**（，曾执行以及任务），任务专家
  - **[斯蒂芬·纳格尔](../Page/斯蒂芬·纳格尔.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[帕特里克·鲍德雷](../Page/帕特里克·鲍德雷.md "wikilink")**（，[法国宇航员](../Page/法国.md "wikilink")，曾执行任务），有效载荷专家
  - **[苏尔坦·萨尔曼·阿尔-萨德](../Page/苏尔坦·萨尔曼·阿尔-萨德.md "wikilink")**（，曾执行任务），有效载荷专家

[Category:1985年科學](../Category/1985年科學.md "wikilink")
[Category:1985年佛罗里达州](../Category/1985年佛罗里达州.md "wikilink")
[Category:发现号航天飞机任务](../Category/发现号航天飞机任务.md "wikilink")
[Category:1985年6月](../Category/1985年6月.md "wikilink")