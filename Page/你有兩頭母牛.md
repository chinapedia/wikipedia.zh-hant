[Two_cows_grazing.jpg](https://zh.wikipedia.org/wiki/File:Two_cows_grazing.jpg "fig:Two_cows_grazing.jpg")
「**你有兩頭母牛**」（[英文](../Page/英文.md "wikilink"):You have two
cows）是[歐美地區一連串](../Page/歐美.md "wikilink")[政治](../Page/政治.md "wikilink")[笑話的開首文句](../Page/笑話.md "wikilink")。

## 原句及戲仿

這個笑話原句見於典型的[經濟學入門教材當中](../Page/經濟學.md "wikilink")，敘述[農夫在沒有](../Page/農夫.md "wikilink")[通貨的社會裡](../Page/通貨.md "wikilink")，要與鄰居交換畜牧產品。原文是這樣的：

  - 你有兩頭母牛，但你需要[雞](../Page/雞.md "wikilink")，你開始尋找有雞而想要母牛的農夫。

在這個比喻中，登場的人發現到[以物易物的界限](../Page/以物易物.md "wikilink")，最後引入[貨幣](../Page/貨幣.md "wikilink")[制度](../Page/制度.md "wikilink")。但在這[戲仿的過程中](../Page/戲仿.md "wikilink")，表現了母牛的主人在不同的[經濟制度下](../Page/經濟.md "wikilink")，[產物和](../Page/產物.md "wikilink")[資產的遭遇](../Page/資產.md "wikilink")。這個笑話意圖指出不同制度的缺點與不合理之處。例如：

  - **[資本主義](../Page/資本主義.md "wikilink")**
    你有兩頭母牛。你賣了其中一頭母牛，加上貸款買來一隻公牛。爾後你的牛以倍數增加，經濟不斷成長從未衰退。最後你把牛都賣了，拿著那些錢還債退休。

<!-- end list -->

  - **[社會主義](../Page/社會主義.md "wikilink")**
    你有兩頭母牛。[公家徵收你一頭母牛](../Page/政府.md "wikilink")，轉送給其他人。

<!-- end list -->

  - **[俄羅斯式](../Page/俄羅斯.md "wikilink")[社會主義](../Page/社會主義.md "wikilink")**
    你有兩頭母牛。[公家全部拿走](../Page/政府.md "wikilink")，賣給你牛奶。

<!-- end list -->

  - **[共產主義](../Page/共產主義.md "wikilink")**
    你有兩頭母牛。[公家徵收你兩頭母牛](../Page/政府.md "wikilink")，只給你一部分[牛奶](../Page/牛奶.md "wikilink")。

<!-- end list -->

  - **[中國式](../Page/中國.md "wikilink")[共產主義](../Page/共產主義.md "wikilink")**
    你有兩頭母牛。[中國共產黨沒收你兩頭母牛](../Page/中國共產黨.md "wikilink")，並給你一些[河蟹](../Page/中華絨螯蟹.md "wikilink")。

<!-- end list -->

  - **[無政府主義](../Page/無政府主義.md "wikilink")**
    你有兩頭母牛。你偷鄰居的公牛，鄰居偷你的母牛，[公家完全不管](../Page/政府.md "wikilink")。

<!-- end list -->

  - **[極權主義](../Page/極權主義.md "wikilink")**
    你有兩頭母牛。公家沒收你兩頭母牛，並否認牠們曾經存在，也禁止你擁有[牛奶](../Page/牛奶.md "wikilink")。

<!-- end list -->

  - **[法西斯主義](../Page/法西斯主義.md "wikilink")**
    你有兩頭母牛。公家射殺你其中一頭母牛，又賣給你另一頭母牛。

<!-- end list -->

  - **[納粹主義](../Page/納粹主義.md "wikilink")**
    你有兩頭母牛。公家[槍斃你](../Page/槍斃.md "wikilink")，沒收兩頭母牛。

<!-- end list -->

  - **[封建主義](../Page/封建主義.md "wikilink")**
    你有兩頭母牛。[領主要求你納貢](../Page/領主.md "wikilink")[牛奶](../Page/牛奶.md "wikilink")。

<!-- end list -->

  - **[集體主義](../Page/集體主義.md "wikilink")**
    你有兩頭母牛。你得自己照顧母牛，但牛奶全屬於公家。

<!-- end list -->

  - **[軍國主義](../Page/軍國主義.md "wikilink")**
    你有兩頭母牛。公家因[戰爭需要](../Page/戰爭.md "wikilink")，全部無條件徵用。

<!-- end list -->

  - **[民主主義](../Page/民主主義.md "wikilink")**
    你有兩頭母牛。牠們與你舉辦公民[投票](../Page/投票.md "wikilink")，二對一勝出，部分禁止了[肉類與](../Page/肉類.md "wikilink")[奶類製品](../Page/奶製品.md "wikilink")。

<!-- end list -->

  - **[自由主義](../Page/自由主義.md "wikilink")**
    你有兩頭母牛。讓牠們分別和喜歡的公牛遠走吧。

<!-- end list -->

  - **[浪漫主義](../Page/浪漫主義.md "wikilink")**
    你有兩頭母牛。請選擇你愛的那頭。

<!-- end list -->

  - **[達達主義](../Page/達達主義.md "wikilink")**
    你有兩頭母牛。還是由你親自操刀，以顯示萬物平等吧。

## 兩頭母牛與經濟制度

初期「兩頭母牛」笑話的目的在於說明[政府與](../Page/政府.md "wikilink")[官僚等怎樣妨礙到乳牛的主人的](../Page/官僚.md "wikilink")[所有權](../Page/所有權.md "wikilink")，對照[資本主義與](../Page/資本主義.md "wikilink")[共產主義互相相反的](../Page/共產主義.md "wikilink")[經濟制度](../Page/經濟.md "wikilink")。

後來這個笑話發展成為諷刺各種各樣關於政治、[文化](../Page/文化.md "wikilink")、[社會](../Page/社會.md "wikilink")、[哲學等制度與理論](../Page/哲學.md "wikilink")。例如，在1990年代後期末世思想風行香港之時，在當時互聯網上流傳一個有關各地經濟的版本講到在香港的兩頭母牛是：「你有兩條母牛，然後聲稱擁有十條母牛，並利用這十條母牛的資產成立一家控股公司上市，市值立即升至二十條母牛。不過，這兩條母牛其實早就已被你殺了，因為你覺得兩條母牛的存在使公司的風水變差。」到最後，甚麼事物都套得上「兩頭母牛」。就連跟真牛有關的[新聞](../Page/新聞.md "wikilink")，如[瘋牛症](../Page/瘋牛症.md "wikilink")，也曾經被套用上「兩頭母牛」。

## 互聯網初期

「兩頭母牛」是[互聯網最初流行的網上笑話](../Page/互聯網.md "wikilink")。但這個笑話的初期變種可以上遡自互聯網普及以前的1960年代前半，當時早已經透過[打字機廣為流傳](../Page/打字機.md "wikilink")。「兩頭母牛」笑話已經被容納為很多文化圈當中的固定幽默項目，成為[全球資訊網國際性開展的一環](../Page/全球資訊網.md "wikilink")。

「兩頭母牛」直到今日依然相當流行，每過幾年又會追加新的定義，變化成無數版本，很多網頁會翻譯與引用它們。著名的下載網站「」的正式名稱雖為「」（终极[Windows软件的集合](../Page/Windows.md "wikilink")），但從標誌暗示所得，名字是出自「兩頭母牛」，故前面所示的正式名稱有被認為是之後加上去的。

## 不同文化之間的幽默

「兩頭母牛」笑話因為題材的自由度與普遍性，經常被視為不同文化之間的[幽默的好例子](../Page/幽默.md "wikilink")。這個笑話把「同一個政治理念會否因不同文化而有不相同的角度」的問題，用反諷、誇張、尖酸刻薄，甚至是有些不真實的方式簡潔地回答（但部分帶有學術性）。實際上全部「兩頭母牛」笑話都是反映該諷刺的制度的局外人之觀點與角度。

## 相關條目

  - [球体奶牛](../Page/球体奶牛.md "wikilink")

## 外部連結

  - [TheCapitol.Net You Have Two Cows... at
    YouHaveTwoCows.com](http://www.thecapitol.net/Recommended/twocows.htm)
    （英文）
  - [Two
    Cows](https://web.archive.org/web/20120911134910/http://www.aeriagloris.com/Resources/humor/TwoCows.htm)
    - 其他笑話的列表（英文）

[Category:經濟學](../Category/經濟學.md "wikilink")
[Category:幽默](../Category/幽默.md "wikilink")
[Category:政治諷刺](../Category/政治諷刺.md "wikilink")
[Category:政治笑话](../Category/政治笑话.md "wikilink")
[Category:以動物作比喻](../Category/以動物作比喻.md "wikilink")