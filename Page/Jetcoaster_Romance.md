**雲霄飛車羅曼史**（**Jetcoaster
Romance；**）是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")3張[單曲](../Page/單曲.md "wikilink")。於1998年4月22日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

這是一張和出道單曲『[玻璃少年](../Page/玻璃少年.md "wikilink")』一樣由[松本隆及](../Page/松本隆.md "wikilink")[山下達郎組合製作的單曲作品](../Page/山下達郎.md "wikilink")。A面歌曲『雲霄飛車羅曼史』成為了令當時一眾十多歲的年青人會想起“[夏天](../Page/夏天.md "wikilink")”的歌曲第一位。雖然首批銷售量與上一張單曲相近，但可惜累積銷售量只達93萬張（[Oricon統計所得](../Page/Oricon.md "wikilink")），沒有長期佔據銷量榜上，未能達至連續三張單曲均取得百萬銷量榮譽。至於發行數量方面，[日本唱片協會則認定超過一百萬張](../Page/日本唱片協會.md "wikilink")。銷售方式方面，初回限定版設有紅、藍、綠三種顏色的封套包裝，內裡的CD則與通常版的一樣。

這張單曲的A面歌曲『雲霄飛車羅曼史』同時為日本[全日空](../Page/全日本空輸.md "wikilink")[航空公司廣告](../Page/航空公司.md "wikilink")『'98
Paradise
[沖繩](../Page/沖繩.md "wikilink")』的主題曲。[電視廣告與](../Page/電視廣告.md "wikilink")[音樂錄影帶一同在沖繩進行拍攝](../Page/音樂錄影帶.md "wikilink")。這首歌曲在編曲時發生了一段小插曲。一般來說，由山下達郎創作的歌曲，基本上都會由他自己進行編曲。但因為山下先生在創作這首歌曲當時比較忙碌，結果拜託了船山基紀負責編曲工作。但原來山下先生為了向船山先生交代自己想要的編曲概念，他曾把一段示範錄音帶交予船山先生。但後來船山先生完成編後的作品卻與山下先生的示範錄音帶完全不同，山下先生自己也嚇了一跳。後來這段示範錄音在山下先生自己的[電台節目](../Page/電台.md "wikilink")『山下達郎的JACCS
CARD星期日歌曲本』中播放。

## 名稱

  - 日語原名：****
  - 中文意思：**雲霄飛車羅曼史**
  - [香港](../Page/香港.md "wikilink")[正東譯名](../Page/正東唱片.md "wikilink")：**Jetcoaster
    Romance**
  - [台灣](../Page/台灣.md "wikilink")[豐華譯名](../Page/豐華唱片.md "wikilink")：**雲霄飛車羅曼史**
  - [台灣](../Page/台灣.md "wikilink")[艾迴譯名](../Page/艾迴唱片.md "wikilink")：**雲霄飛車羅曼史**

## 收錄歌曲

1.  **雲霄飛車羅曼史**（**Jetcoaster Romance；**）
      - ※KinKi
        Kids參與演出的日本[全日空](../Page/全日本空輸.md "wikilink")[航空公司廣告](../Page/航空公司.md "wikilink")『'98
        Paradise [沖繩](../Page/沖繩.md "wikilink")』主題曲。
      - 作曲：[山下達郎](../Page/山下達郎.md "wikilink")
      - 作詞：[松本隆](../Page/松本隆.md "wikilink")
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
2.  **Hello**
      - 作曲：[寺田一郎](../Page/寺田一郎.md "wikilink")
      - 作詞：[山本英美](../Page/山本英美.md "wikilink")
      - 編曲：船山基紀
3.  **雲霄飛車羅曼史 (Instrumental)**
      - 作曲：山下達郎
      - 編曲：船山基紀
4.  **Hello (Instrumental)**
      - 作曲：寺田一郎
      - 編曲：船山基紀

[Category:近畿小子歌曲](../Category/近畿小子歌曲.md "wikilink")
[Category:1998年單曲](../Category/1998年單曲.md "wikilink")
[Category:RIAJ百萬認證單曲](../Category/RIAJ百萬認證單曲.md "wikilink")
[Category:1998年Oricon單曲週榜冠軍作品](../Category/1998年Oricon單曲週榜冠軍作品.md "wikilink")
[Category:海題材歌曲](../Category/海題材歌曲.md "wikilink")