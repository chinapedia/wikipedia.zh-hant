**Ghostscript**是一套建基於[Adobe](../Page/Adobe_Systems.md "wikilink")、[PostScript及](../Page/PostScript.md "wikilink")[可移植文檔格式](../Page/PDF.md "wikilink")（PDF）的[頁面描述語言等而編譯成的](../Page/頁面描述語言.md "wikilink")[自由軟件](../Page/自由軟件.md "wikilink")。

现在已经从Linux版本移植到其他操作系统，如其他[Unix](../Page/Unix.md "wikilink")、[Mac OS
X](../Page/Mac_OS_X.md "wikilink")、[VMS](../Page/VMS.md "wikilink")、[Windows](../Page/Microsoft_Windows.md "wikilink")、[OS/2和](../Page/OS/2.md "wikilink")[Mac
OS classic](../Page/Mac_OS.md "wikilink")。

## 歷史

Ghostview最早是由[L Peter
Deutsch和阿拉丁企業开发的](../Page/L_Peter_Deutsch.md "wikilink")，以[Aladdin
Free Public
License](../Page/Aladdin_Free_Public_License.md "wikilink")（AFPL）发布，目前由[artofcode
LLC拥有并维护](../Page/artofcode_LLC.md "wikilink")。推出了两个版本：一是在原来的AFPL许可下进行商业使用的**AFPL
Ghostscript**，一是[GNU General Public
License下使用的](../Page/GNU_General_Public_License.md "wikilink")**GPL
Ghostscript**。

GPL版本也是**Display Ghostscript**的基础，其增加了所需的功能，以便对[Display
PostScript形成完全的支持](../Page/Display_PostScript.md "wikilink")。

## 特色

这个软件可用作：

  - 计算机打印机使用的[栅格化图像处理器](../Page/栅格化图像处理器.md "wikilink")（RIP），例如，[行打印机](../Page/行打印机.md "wikilink")[lpd的输入过滤器](../Page/lpd.md "wikilink")。
  - 以[PostScript和](../Page/PostScript.md "wikilink")[PDF阅览器使用的](../Page/可移植文件格式.md "wikilink")[栅格化图像处理器](../Page/栅格化图像处理器.md "wikilink")（RIP）引擎。
  - 文件格式转换器，如[PostScript和](../Page/PostScript.md "wikilink")[PDF转换器](../Page/可移植文件格式.md "wikilink")。
  - 一般用途的编程环境。

## 前端

已经写出几个[图形用户界面](../Page/图形用户界面.md "wikilink")（GUI）用于让用户在屏幕上观看PostScript或PDF文件，可以上下卷动、向前翻页、向后翻页、缩放文字，而且还能打印一页或多个页面。

  - **Ghostview**，在Unix/X11下运行。
  - **GSView**，在Windows和OS/2下运行。
  - **gv**，在Linux下运行。

## 参考文献

## 外部連結

  - [Ghostscript社區、mailing
    lists、Bugzilla、CVS](http://www.ghostscript.com/)
  - [Ghostscript的現任版權持有人](https://web.archive.org/web/20040727104940/http://www.artofcode.com/)
  - [Ghostscript的版權與商業支援](http://www.artifex.com/)
  - [下載Ghostscript, Ghostview and
    GSview](http://www.cs.wisc.edu/~ghost/)
  - [SourceForge的Ghostscript專案](http://sourceforge.net/project/showfiles.php?group_id=1897)
  - [SourceForge的Ghostscript字型專案](http://sourceforge.net/project/showfiles.php?group_id=3498)
  - [顯示Ghostscript](https://web.archive.org/web/20040805152435/http://www.gyve.org/dgs/)

[Category:自由PDF软件](../Category/自由PDF软件.md "wikilink")
[Category:跨平台软件](../Category/跨平台软件.md "wikilink")
[Category:数码字体排印](../Category/数码字体排印.md "wikilink")