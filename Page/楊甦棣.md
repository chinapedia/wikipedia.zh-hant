<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>衛斯理安大學學士</li>
<li>芝加哥大學碩士及博士</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>美國在台協會處長</li>
<li>美國駐港總領事</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

**楊甦棣**（，）\[1\]，[美國](../Page/美國.md "wikilink")[外交官](../Page/外交官.md "wikilink")，生於[華盛頓特區](../Page/華盛頓特區.md "wikilink")。就讀[衛斯理安大學](../Page/衛斯理安大學.md "wikilink")，獲得學士學位；接著進入[芝加哥大學](../Page/芝加哥大學.md "wikilink")，獲得碩士及博士學位。曾任[美國在台協會](../Page/美國在台協會.md "wikilink")（American
Institute in Taiwan，AIT）臺北處處長和[美國駐港總領事](../Page/美國駐港總領事.md "wikilink")。

## 經歷

  - 楊甦棣於1963年（12歲）至1965年間與家人一起在[高雄的](../Page/高雄.md "wikilink")[駐台美軍社區居住過兩年](../Page/駐台美軍.md "wikilink")\[2\]。
  - 求學時，於美國东北部康乃狄克州的米德尔敦的一所私立四年制文理學院[衛斯理安大學畢業](../Page/衛斯理安大學.md "wikilink")，之後於入芝加哥大學，取得碩士及博士學位。

### 外交官生涯

  - 1981年進入[美國國務院](../Page/美國國務院.md "wikilink")，同年至1982年派駐美國在台協會台北辦事處，後歷經（1985年－1987年、1994年－1995年）、[駐華大使館](../Page/美國駐華大使館.md "wikilink")（1990年－1993年）勤務，於1995年至1998年間擔任新獨立國家高加索暨中亞事務處（）主任，1998年8月起重回美國在台協會擔任副處長\[3\]\[4\]，至2001年止\[5\]。
  - 之後結婚，夫人为費雯俐（Barbara Finamore），育有子女三人。
  - 2003年至2005年，任美國駐[吉爾吉斯大使](../Page/吉爾吉斯.md "wikilink")，曾表示華盛頓將吉爾吉斯基層的政治運動視為中亞國家的重要先例，為瓦解中亞獨裁政權的一個方法，吉爾吉斯的反獨裁運動更能為中亞社會提供一個希望。\[6\]
  - 2006年3月18日，56歲時，就任[美國在台協會辦事處處長](../Page/美國在台協會.md "wikilink")。他曾於2006年10月26日召開記者會強烈表達敦促[立法院應在本會期通過軍購案的立場](../Page/立法院.md "wikilink")\[7\]，2009年7月卸任。
  - 2010年2月25日，他轉任[美國駐港澳總領事](../Page/美國駐港澳總領事.md "wikilink")，但與中國方面關係惡劣，2013年5月，中國[外交部駐港特派員公署點名批評楊甦棣干涉香港內部事務](../Page/外交部駐港特派員公署.md "wikilink")。特派員公署表示，政制發展問題屬於香港內部事務，任何外國政府或官員都不應干預，妄加評論。在此之前，楊甦棣曾表示，香港的普選應符合國際標準。近半年來，香港親中國的報章包括大公報和文匯報對楊甦棣的攻擊尤為激烈，甚至指他「策動香港自治運動」，有意把[中亞的](../Page/中亞.md "wikilink")[花朵革命帶到香港](../Page/花朵革命.md "wikilink")\[8\]。所以，在2013年7月卸任\[9\]\[10\]。

## 爭議事項

### 評價[宋楚瑜](../Page/宋楚瑜.md "wikilink")

被[維基解密揭露](../Page/維基解密.md "wikilink")，暗示[宋楚瑜曾在](../Page/宋楚瑜.md "wikilink")2006年北高市長選舉前，告訴楊甦棣，國民黨願以台北市長一職，交換宋不加入[2008年中華民國總統選舉](../Page/2008年中華民國總統選舉.md "wikilink")。宋楚瑜得知後怒稱，楊甦棣是「醜陋的美國人」、「醜陋的外交官」，維基解密內容不符事實，但宋楚瑜的重砲抨擊造成報章媒體的激烈討論，後宋楚瑜解釋，他說的不是「醜陋」，而是[臺語](../Page/臺語.md "wikilink")「粗魯」。

### 評價[馬英九](../Page/馬英九.md "wikilink")

被[維基解密揭露](../Page/維基解密.md "wikilink")，他曾評價[馬英九根本是把台北市長當作總統](../Page/馬英九.md "wikilink")[寶座的墊腳石](../Page/寶座.md "wikilink")，在馬兼任臺北市長與國民黨主席的一年半時間中，馬主要的時間與精力放在處理黨務上，市政工作交給他的副市長[金溥聰](../Page/金溥聰.md "wikilink")。楊甦棣也說，馬英九在與美國在臺協會打交道時，總是要形塑自己是未來總統候選人，與他見面時，也要在正式的大會議中見面。

### 批評香港特區政府

曾任美國駐港總領事的楊甦棣表示，[香港及](../Page/香港.md "wikilink")[中國政府處理美國](../Page/中國政府.md "wikilink")[中情局前僱員](../Page/中情局.md "wikilink")[愛德華·斯諾登事件的手法](../Page/愛德華·斯諾登.md "wikilink")，令華府失望，動搖華府對香港的信心及合作。認為[香港政府是受到](../Page/香港政府.md "wikilink")[中華人民共和國政府影響](../Page/中華人民共和國政府.md "wikilink")。

### 認為美方不需要道歉

他又指，對於美國是否曾入侵香港電腦網絡時表示，美方沒有需要向任何人道歉。他說，美方會在適當時間有交代，但不認為有需要道歉。但至今沒有表示。

### 支持占領中環行動

楊甦棣又稱，支持香港在2017年普選[行政長官](../Page/行政長官.md "wikilink")（[特首](../Page/特首.md "wikilink")）。對於全國人大法工委主任委員[喬曉陽表示](../Page/喬曉陽.md "wikilink")，特首人選要愛國愛港，楊甦棣指喬曉陽言論引起爭論，認為喬曉陽應再解釋。楊甦棣希望[佔領中環可依法進行](../Page/讓愛與和平佔領中環.md "wikilink")。

## 家庭

  - 家族

<!-- end list -->

  - 父親：楊恩上校（Mason J.
    Young，Jr），1923年11月出生在美國[路易斯安那州](../Page/路易斯安那州.md "wikilink")[紐奧良](../Page/紐奧良.md "wikilink")。1944年於[西點軍校畢業](../Page/西點軍校.md "wikilink")，曾就讀[美國海軍戰爭學院](../Page/美國海軍戰爭學院.md "wikilink")，也曾在砲兵、飛彈與相關基地接受過數年訓練。營長下任後，1963年―1965年以中校階級派往駐在[臺灣](../Page/臺灣.md "wikilink")[高雄的](../Page/高雄.md "wikilink")[美軍顧問團服務](../Page/美軍顧問團_\(中華民國\).md "wikilink")\[11\]。
  - 母親：海倫（Helen）\[12\]
  - 長兄：麥森（Maxon）\[13\]
  - 妹妹：瓊安（Joanne）\[14\]

<!-- end list -->

  - 配偶

<!-- end list -->

  - 妻：Barbara A. Finamore，一名律師，負責在自然資源保護委員會有關中國的法律項目。

<!-- end list -->

  - 子女

育有三名子女

  - 長子：Michael，在美國[首都](../Page/首都.md "wikilink")[華府工作](../Page/華府.md "wikilink")。
  - 長女：Rebecca，在美國[西雅圖工作](../Page/西雅圖.md "wikilink")。
  - 次子：Patrick，學生，在加州[橘郡的Chapman大學就讀中](../Page/橘郡.md "wikilink")，目前三年級。

## 注釋

[Category:華盛頓哥倫比亞特區人](../Category/華盛頓哥倫比亞特區人.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[Category:衛斯理大學校友](../Category/衛斯理大學校友.md "wikilink")
[Category:美国外交官](../Category/美国外交官.md "wikilink")
[Category:美國駐港澳總領事](../Category/美國駐港澳總領事.md "wikilink")
[Category:美國在台協會台北辦事處處長](../Category/美國在台協會台北辦事處處長.md "wikilink")
[Category:美國駐吉爾吉斯斯坦大使](../Category/美國駐吉爾吉斯斯坦大使.md "wikilink")

1.

2.
3.

4.

5.
6.  Philip Shishkin 《Restless Valley：Revolution，Murder，and Intrigue in
    the Heart of Central Asia》

7.  [楊蘇棣：今秋應通過軍購案](http://www.libertytimes.com.tw/2006/new/oct/27/today-t1.htm)

8.  [美駐港代表換人
    楊甦棣離港](http://news.rti.org.tw/index_newsContent.aspx?nid=440145)
     中央廣播電臺

9.  [夏千福接替杨苏棣出任美驻港澳总领事](http://www.apdnews.com/news/30882.html)
    ，亚太日报，2013年8月1日

10.

11.

12.
13.
14.