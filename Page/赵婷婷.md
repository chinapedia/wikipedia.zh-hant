**赵婷婷**（），[江苏](../Page/江苏.md "wikilink")[南通人](../Page/南通.md "wikilink")，[中國退役](../Page/中國.md "wikilink")[羽毛球運動員](../Page/羽毛球.md "wikilink")。与著名羽毛球双打选手[葛菲是同乡](../Page/葛菲.md "wikilink")，葛菲也是她近十年的偶像。

赵婷婷1990年进入南通市业余体校，1994年进入江苏省体校，1996年进入南京军区体工队，1999年跨进国家队的大门，2000年悉尼奥运会后，从国家二队升上一队并与[张亚雯搭档](../Page/张亚雯.md "wikilink")。合作两年间，两人曾夺过一次中国公开赛第三名，一次[法国羽毛球公开赛冠军](../Page/法国羽毛球公开赛.md "wikilink")。2002年底在女双项目上和[魏轶力配对](../Page/魏轶力.md "wikilink")，在混双项目和名将[陈其遒配对](../Page/陈其遒.md "wikilink")。

与魏轶力搭档女双夺取了2003年[世界羽毛球锦标赛亚军](../Page/世界羽毛球锦标赛.md "wikilink")，甚至一度取得世界排名第一。[雅典奥运会](../Page/雅典奥运会.md "wikilink")，她俩在三四名决赛中1：2惜败于[罗景民](../Page/罗景民.md "wikilink")/[李敬元](../Page/李敬元.md "wikilink")，十分遗憾地与奥运奖牌擦肩而过。当年底在湖南益阳举行的[羽毛球世界杯](../Page/羽毛球世界杯.md "wikilink")，赵婷婷与改与[張丹合作](../Page/張丹_\(羽毛球運動員\).md "wikilink")。一年后赵婷婷频频更换搭档，她先后与[杜婧](../Page/杜婧.md "wikilink")、[杨维](../Page/杨维.md "wikilink")、[高凌合作](../Page/高凌.md "wikilink")。

再次与[张亚雯搭档后连续三次夺得国际公开赛女子双打冠军](../Page/张亚雯.md "wikilink")：2008年底的[中国羽毛球公开赛和](../Page/中国羽毛球公开赛.md "wikilink")[香港羽毛球公开赛](../Page/香港羽毛球公开赛.md "wikilink")、2008年初的[全英羽毛球公开赛](../Page/全英羽毛球公开赛.md "wikilink")。

2009年11月，赵婷婷於[上海跟隊友](../Page/上海.md "wikilink")[谢杏芳舉行退役儀式](../Page/谢杏芳.md "wikilink")；期間，中國羽毛球隊總教練[李永波](../Page/李永波.md "wikilink")，向她們頒發「突出貢獻獎」獎盃\[1\]。2010年1月正式從[羽毛球世界聯會註冊退役](../Page/羽毛球世界聯會.md "wikilink")\[2\]。

## 主要战绩

  - 2003年泰国公开赛女双、混双冠军。
  - [2004年](../Page/2004年優霸盃.md "wikilink")、[2008年優霸盃冠军成员](../Page/2008年優霸盃.md "wikilink")。

## 主要比賽成績

只列出曾進入準決賽的國際賽事成績：

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>賽事</p></th>
<th><p>公開賽級別</p></th>
<th><p>項目</p></th>
<th><p>拍檔</p></th>
<th><p>成績</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/2001年亞洲羽毛球錦標賽.md" title="wikilink">亞洲羽毛球錦標賽</a></p></td>
<td><p>其他</p></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/张亚雯.md" title="wikilink">张亚雯</a></p></td>
<td><p>季軍</p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/2002年亞洲羽毛球錦標賽.md" title="wikilink">亞洲羽毛球錦標賽</a></p></td>
<td><p>其他</p></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/魏轶力.md" title="wikilink">魏轶力</a></p></td>
<td><p>季軍</p></td>
</tr>
<tr class="odd">
<td><p>混合雙打</p></td>
<td><p><a href="../Page/王伟_(羽毛球运动员).md" title="wikilink">王伟</a></p></td>
<td><p>季軍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/2004年全英羽毛球公開賽.md" title="wikilink">全英羽毛球公開賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/魏轶力.md" title="wikilink">魏轶力</a></p></td>
<td><p>準決賽</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年中國羽毛球公開賽.md" title="wikilink">中國羽毛球公開賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/魏轶力.md" title="wikilink">魏轶力</a></p></td>
<td><p>亞軍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>混合雙打</p></td>
<td><p><a href="../Page/陈其遒.md" title="wikilink">陈其遒</a></p></td>
<td><p>亞軍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/2006年香港羽毛球公開賽.md" title="wikilink">香港羽毛球公開賽</a></p></td>
<td></td>
<td><p>混合雙打</p></td>
<td><p><a href="../Page/郑波.md" title="wikilink">郑波</a></p></td>
<td><p>'''冠軍</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/2006年羽毛球世界盃.md" title="wikilink">羽毛球世界盃</a></strong></p></td>
<td><p>其他</p></td>
<td><p>混合雙打</p></td>
<td><p><a href="../Page/郑波.md" title="wikilink">郑波</a></p></td>
<td><p>準決賽</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/2007年德國羽毛球大獎賽.md" title="wikilink">德國羽毛球大獎賽</a></p></td>
<td></td>
<td><p>混合雙打</p></td>
<td><p><a href="../Page/徐晨.md" title="wikilink">徐晨</a></p></td>
<td><p>亞軍</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年瑞士羽毛球超級賽.md" title="wikilink">瑞士羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/楊維.md" title="wikilink">楊維</a></p></td>
<td><p>'''冠軍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年亞洲羽毛球錦標賽.md" title="wikilink">亞洲羽毛球錦標賽</a></p></td>
<td><p>其他</p></td>
<td><p>混合雙打</p></td>
<td><p><a href="../Page/徐晨.md" title="wikilink">徐晨</a></p></td>
<td><p>亞軍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年新加坡羽毛球超級賽.md" title="wikilink">新加坡羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/楊維.md" title="wikilink">楊維</a></p></td>
<td><p>亞軍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年印度尼西亞羽毛球超級賽.md" title="wikilink">印尼羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/楊維.md" title="wikilink">楊維</a></p></td>
<td><p>亞軍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年中國羽毛球大師賽.md" title="wikilink">中國羽毛球大師賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/楊維.md" title="wikilink">楊維</a></p></td>
<td><p>亞軍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年日本羽毛球超級賽.md" title="wikilink">日本羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/于洋_(羽毛球运动员).md" title="wikilink">于洋</a></p></td>
<td><p>亞軍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年丹麥羽毛球超級賽.md" title="wikilink">丹麥羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/于洋_(羽毛球运动员).md" title="wikilink">于洋</a></p></td>
<td><p>準決賽</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年法國羽毛球超級賽.md" title="wikilink">法國羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/于洋_(羽毛球运动员).md" title="wikilink">于洋</a></p></td>
<td><p>亞軍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年中國羽毛球超級賽.md" title="wikilink">中國羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/高崚.md" title="wikilink">高崚</a></p></td>
<td><p>'''冠軍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年香港羽毛球超級賽.md" title="wikilink">香港羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/高崚.md" title="wikilink">高崚</a></p></td>
<td><p>準決賽</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><a href="../Page/2008年馬來西亞羽毛球超級賽.md" title="wikilink">馬來西亞羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/高崚.md" title="wikilink">高崚</a></p></td>
<td><p>亞軍</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008年韓國羽毛球超級賽.md" title="wikilink">韓國羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/高崚.md" title="wikilink">高崚</a></p></td>
<td><p>亞軍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年德國羽毛球大獎賽.md" title="wikilink">德國羽毛球大獎賽</a></p></td>
<td><p>rowspan="2" </p></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/高崚.md" title="wikilink">高崚</a></p></td>
<td><p>準決賽</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>混合雙打</p></td>
<td><p><a href="../Page/徐晨.md" title="wikilink">徐晨</a></p></td>
<td><p>準決賽</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年法國羽毛球超級賽.md" title="wikilink">法國羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/张亚雯.md" title="wikilink">张亚雯</a></p></td>
<td><p>準決賽</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008年中國羽毛球超級賽.md" title="wikilink">中國羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/张亚雯.md" title="wikilink">张亚雯</a></p></td>
<td><p>'''冠軍</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年香港羽毛球超級賽.md" title="wikilink">香港羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/张亚雯.md" title="wikilink">张亚雯</a></p></td>
<td><p>'''冠軍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/2009年全英羽毛球超級賽.md" title="wikilink">全英羽毛球超級賽</a></p></td>
<td></td>
<td><p>女子雙打</p></td>
<td><p><a href="../Page/张亚雯.md" title="wikilink">张亚雯</a></p></td>
<td><p>'''冠軍</p></td>
</tr>
</tbody>
</table>

### 奧運會和世錦賽參賽紀錄

<table>
<thead>
<tr class="header">
<th></th>
<th><p>搭檔</p></th>
<th><p><a href="../Page/2005年世界羽毛球錦標賽.md" title="wikilink">2005</a></p></th>
<th><p><a href="../Page/2006年世界羽毛球錦標賽.md" title="wikilink">2006</a></p></th>
<th><p><a href="../Page/2007年世界羽毛球錦標賽.md" title="wikilink">2007</a></p></th>
<th><p><a href="../Page/2008年夏季奧林匹克運動會羽毛球比賽.md" title="wikilink">2008</a> <a href="https://zh.wikipedia.org/wiki/File:Olympic_rings_without_rims.svg" title="fig:Olympic_rings_without_rims.svg">Olympic_rings_without_rims.svg</a></p></th>
<th><p><a href="../Page/2009年世界羽毛球錦標賽.md" title="wikilink">2009</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>女子雙打</p></td>
<td></td>
<td><p><a href="../Page/2005年世界羽毛球錦標賽女子雙打項目.md" title="wikilink">8強</a></p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/张亚雯.md" title="wikilink">张亚雯</a></p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p><strong><a href="../Page/2009年世界羽毛球錦標賽女子雙打項目.md" title="wikilink">冠軍</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>混合雙打</p></td>
<td><p><a href="../Page/陈其遒.md" title="wikilink">陈其遒</a></p></td>
<td><p><a href="../Page/2005年世界羽毛球錦標賽混合雙打項目.md" title="wikilink">8強</a></p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郑波.md" title="wikilink">郑波</a></p></td>
<td><p>－</p></td>
<td><p><a href="../Page/2006年世界羽毛球錦標賽混合雙打項目.md" title="wikilink">16強</a></p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/徐晨.md" title="wikilink">徐晨</a></p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td><p><a href="../Page/2007年世界羽毛球錦標賽混合雙打項目.md" title="wikilink">16強</a></p></td>
<td><p>－</p></td>
<td><p>－</p></td>
<td></td>
</tr>
</tbody>
</table>

## 資料來源

## 外部链接

  - [张亚雯赵婷婷六年后二次握手
    传奇经历羽坛难复制_综合体育_NIKE新浪竞技风暴_新浪网](http://sports.sina.com.cn/o/2009-03-09/02334259026.shtml)

[Category:中国退役羽毛球运动员](../Category/中国退役羽毛球运动员.md "wikilink")
[Category:中国奥运羽毛球运动员](../Category/中国奥运羽毛球运动员.md "wikilink")
[T](../Category/赵姓.md "wikilink")
[Category:南通籍运动员](../Category/南通籍运动员.md "wikilink")
[Category:2004年夏季奧林匹克運動會羽球運動員](../Category/2004年夏季奧林匹克運動會羽球運動員.md "wikilink")

1.  [羽毛球名將謝杏芳宣佈退役](http://www.881903.com/Page/ZH-TW/newsdetail.aspx?ItemId=177812)《[商業電台新聞資訊](../Page/商業電台.md "wikilink")》2009-11-23
2.  [BWF Players Retired from competitive badminton
    in 2009/2010](http://www.bwfbadminton.org/file_download.aspx?id=23613)