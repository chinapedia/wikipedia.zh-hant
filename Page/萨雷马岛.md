**萨雷马岛**(；；[瑞典语](../Page/瑞典语.md "wikilink")、[德语](../Page/德语.md "wikilink")：Ösel；[拉丁语](../Page/拉丁语.md "wikilink")：Osilia)，[爱沙尼亚第一大岛](../Page/爱沙尼亚.md "wikilink")，面积2673平方公里。萨雷马岛位于[波罗的海](../Page/波罗的海.md "wikilink")[里加湾西口处](../Page/里加湾.md "wikilink")，北隔[索埃尔海峡与](../Page/索埃尔海峡.md "wikilink")[希乌马岛相邻](../Page/希乌马岛.md "wikilink")，南隔[伊尔别海峡与](../Page/伊尔别海峡.md "wikilink")[拉脱维亚相望](../Page/拉脱维亚.md "wikilink")。全岛属[薩雷縣管辖](../Page/薩雷縣.md "wikilink")，人口4万人，首府[库雷萨雷](../Page/库雷萨雷.md "wikilink")，人口1.6万人。

[category:愛沙尼亞島嶼](../Page/category:愛沙尼亞島嶼.md "wikilink")

[Category:大西洋島嶼](../Category/大西洋島嶼.md "wikilink")