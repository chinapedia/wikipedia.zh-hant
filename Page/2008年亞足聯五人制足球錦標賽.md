**2008年亞足聯五人制足球錦標賽**會內賽於2008年5月11日至18日在[泰國](../Page/泰國.md "wikilink")[曼谷舉行](../Page/曼谷.md "wikilink")。前四名將代表亞洲參加2008年10月在[巴西舉行的](../Page/巴西.md "wikilink")[2008年世界盃五人制足球賽](../Page/2008年世界盃五人制足球賽.md "wikilink")。

## 資格賽

## 小組賽

### A組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>15</p></td>
<td><p>1</p></td>
<td><p>+14</p></td>
<td><p><strong>9</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>14</p></td>
<td><p>4</p></td>
<td><p>+10</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>6</p></td>
<td><p>20</p></td>
<td><p>−14</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>4</p></td>
<td><p>14</p></td>
<td><p>−10</p></td>
<td><p><strong>0</strong></p></td>
</tr>
</tbody>
</table>

-----

-----

-----

-----

-----

### B組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>16</p></td>
<td><p>5</p></td>
<td><p>+11</p></td>
<td><p><strong>9</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>11</p></td>
<td><p>5</p></td>
<td><p>+6</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>8</p></td>
<td><p>9</p></td>
<td><p>−1</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>4</p></td>
<td><p>20</p></td>
<td><p>−16</p></td>
<td><p><strong>0</strong></p></td>
</tr>
</tbody>
</table>

-----

-----

-----

-----

-----

### C組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>19</p></td>
<td><p>4</p></td>
<td><p>+15</p></td>
<td><p><strong>9</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>12</p></td>
<td><p>14</p></td>
<td><p>−2</p></td>
<td><p><strong>4</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>8</p></td>
<td><p>19</p></td>
<td><p>−11</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>2</p></td>
<td><p>11</p></td>
<td><p>13</p></td>
<td><p>−2</p></td>
<td><p><strong>1</strong></p></td>
</tr>
</tbody>
</table>

-----

-----

-----

-----

-----

### D組

<table>
<thead>
<tr class="header">
<th><p>隊伍</p></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>34</p></td>
<td><p>1</p></td>
<td><p>+33</p></td>
<td><p><strong>9</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>19</p></td>
<td><p>8</p></td>
<td><p>+11</p></td>
<td><p><strong>6</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>3</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>6</p></td>
<td><p>27</p></td>
<td><p>−21</p></td>
<td><p><strong>3</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>3</p></td>
<td><p>4</p></td>
<td><p>27</p></td>
<td><p>−23</p></td>
<td><p><strong>0</strong></p></td>
</tr>
</tbody>
</table>

-----

-----

-----

-----

-----

## 淘汰賽階段

### 半準決賽

-----

-----

-----

### 準決賽

-----

### 季軍戰

### 冠軍戰

## 獎項

<table>
<thead>
<tr class="header">
<th><p>2008年亞足聯五人制足球錦標賽冠軍</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Iran.svg" title="fig:Flag_of_Iran.svg">Flag_of_Iran.svg</a><br />
<strong><a href="../Page/伊朗國家五人制足球隊.md" title="wikilink">伊朗</a></strong><br />
<strong>第9次奪冠</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Asghar_Ghahremani.md" title="wikilink">Asghar Ghahremani</a>, <a href="../Page/Mohammad_Taheri.md" title="wikilink">Mohammad Taheri</a>, <a href="../Page/Mohammad_Keshavarz.md" title="wikilink">Mohammad Keshavarz</a>, <a href="../Page/Mohammad_Hashemzadeh.md" title="wikilink">Mohammad Hashemzadeh</a>, <a href="../Page/Javad_Asghari_Moghaddam.md" title="wikilink">Javad Asghari Moghaddam</a>, <a href="../Page/Kazem_Mohammadi.md" title="wikilink">Kazem Mohammadi</a>, <a href="../Page/Vahid_Shamsaei.md" title="wikilink">Vahid Shamsaei</a>, <a href="../Page/Mohammad_Reza_Heidarian.md" title="wikilink">Mohammad Reza Heidarian</a>, <a href="../Page/Majid_Latifi.md" title="wikilink">Majid Latifi</a>, <a href="../Page/Mostafa_Nazari.md" title="wikilink">Mostafa Nazari</a>, <a href="../Page/Majid_Raeisi.md" title="wikilink">Majid Raeisi</a>, <a href="../Page/Mostafa_Tayyebi.md" title="wikilink">Mostafa Tayyebi</a>, <a href="../Page/Ali_Asghar_Hassanzadeh.md" title="wikilink">Ali Asghar Hassanzadeh</a>, <a href="../Page/Hamid_Reza_Abrarinia.md" title="wikilink">Hamid Reza Abrarinia</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>總教練</strong>:  <a href="../Page/Hossein_Shams.md" title="wikilink">Hossein Shams</a></p></td>
</tr>
</tbody>
</table>

  - **最有價值球員**
      - [Vahid Shamsaei](../Page/Vahid_Shamsaei.md "wikilink")

<!-- end list -->

  - **金靴獎**
      - [Vahid Shamsaei](../Page/Vahid_Shamsaei.md "wikilink") (13球)

<!-- end list -->

  - **公平競爭獎**
      -
<!-- end list -->

  - **全明星隊**
      - [Mostafa Nazari](../Page/Mostafa_Nazari.md "wikilink") (GK)

      - [Mohammad Taheri](../Page/Mohammad_Taheri.md "wikilink")

      - [Daisuke Ono](../Page/Daisuke_Ono_\(futsal\).md "wikilink")

      - [Panuwat Janta](../Page/Panuwat_Janta.md "wikilink")

      - [Vahid Shamsaei](../Page/Vahid_Shamsaei.md "wikilink")

      - **總教練**:  [Hossein Shams](../Page/Hossein_Shams.md "wikilink")
        (伊朗)

## 進球者

  - 13球

<!-- end list -->

  - [Vahid Shamsaei](../Page/Vahid_Shamsaei.md "wikilink")

<!-- end list -->

  - 6球

<!-- end list -->

  - [Ali Asghar
    Hassanzadeh](../Page/Ali_Asghar_Hassanzadeh.md "wikilink")

  - [Anucha Munjarern](../Page/Anucha_Munjarern.md "wikilink")

<!-- end list -->

  - 5球

<!-- end list -->

  - [Li Xin](../Page/Li_Xin_\(futsal\).md "wikilink")

  - [Majid Latifi](../Page/Majid_Latifi.md "wikilink")

  - [Mohammad Taheri](../Page/Mohammad_Taheri.md "wikilink")

  - [Yuki Kanayama](../Page/Yuki_Kanayama.md "wikilink")

  - [Hurshid Tajibaev](../Page/Hurshid_Tajibaev.md "wikilink")

<!-- end list -->

  - 4球

<!-- end list -->

  - [Liang Shuang](../Page/Liang_Shuang.md "wikilink")

  - [Liu Xinyi](../Page/Liu_Xinyi.md "wikilink")

  - [Wang Wei](../Page/Wang_Wei_\(futsal\).md "wikilink")

  - [Mohammad Reza
    Heidarian](../Page/Mohammad_Reza_Heidarian.md "wikilink")

  - [Yusuke Inada](../Page/Yusuke_Inada.md "wikilink")

  - [Yusuke Komiyama](../Page/Yusuke_Komiyama.md "wikilink")

  - [Daisuke Ono](../Page/Daisuke_Ono_\(futsal\).md "wikilink")

  - [Nurjan Djetybaev](../Page/Nurjan_Djetybaev.md "wikilink")

  - [Khaled Takaji](../Page/Khaled_Takaji.md "wikilink")

  - [Panuwat Janta](../Page/Panuwat_Janta.md "wikilink")

  - [Panomkorn Saisorn](../Page/Panomkorn_Saisorn.md "wikilink")

  - [Tanakorn
    Santanaprasit](../Page/Tanakorn_Santanaprasit.md "wikilink")

  - [Farruh Farhutdinov](../Page/Farruh_Farhutdinov.md "wikilink")

<!-- end list -->

  - 3球

<!-- end list -->

  - [Lachlan Wright](../Page/Lachlan_Wright.md "wikilink")

  - [Christopher Zeballos](../Page/Christopher_Zeballos.md "wikilink")

  - [Wu Zhuoxi](../Page/Wu_Zhuoxi.md "wikilink")

  - [Zhang Xi](../Page/Zhang_Xi_\(futsal\).md "wikilink")

  - [Zhang Xiao](../Page/Zhang_Xiao_\(futsal\).md "wikilink")

  - [Majid Raeisi](../Page/Majid_Raeisi.md "wikilink")

  - [Mostafa Tayyebi](../Page/Mostafa_Tayyebi.md "wikilink")

  - [Kenichiro Kogure](../Page/Kenichiro_Kogure.md "wikilink")

  - [Hayssam Atwi](../Page/Hayssam_Atwi.md "wikilink")

  - [Nikolay Odushev](../Page/Nikolay_Odushev.md "wikilink")

  - [Ilhom Yusupdjanov](../Page/Ilhom_Yusupdjanov.md "wikilink")

<!-- end list -->

  - 2球

<!-- end list -->

  - [Fernando](../Page/Fernando_de_Moraes.md "wikilink")

  - [Adrian Vizzari](../Page/Adrian_Vizzari.md "wikilink")

<!-- end list -->

  - 2球(續)

<!-- end list -->

  - [張福祥](../Page/張福祥.md "wikilink")

  - [Jaelani Ladjanibi](../Page/Jaelani_Ladjanibi.md "wikilink")

  - [Javad Asghari
    Moghaddam](../Page/Javad_Asghari_Moghaddam.md "wikilink")

  - [Mohammad Hashemzadeh](../Page/Mohammad_Hashemzadeh.md "wikilink")

  - [Kazem Mohammadi](../Page/Kazem_Mohammadi.md "wikilink")

  - [Abdul-Karim Ghazi](../Page/Abdul-Karim_Ghazi.md "wikilink")

  - [Talaibek Djumataev](../Page/Talaibek_Djumataev.md "wikilink")

  - [Mahmoud Itani](../Page/Mahmoud_Itani.md "wikilink")

  - [Serge Said](../Page/Serge_Said.md "wikilink")

  - [Muizzudin Mohd Haris](../Page/Muizzudin_Mohd_Haris.md "wikilink")

  - [Safar Mohamad](../Page/Safar_Mohamad.md "wikilink")

  - [Kang Hyun-Uk](../Page/Kang_Hyun-Uk.md "wikilink")

  - [Lee Jong-Yun](../Page/Lee_Jong-Yun.md "wikilink")

  - [Shin Han-Kook](../Page/Shin_Han-Kook.md "wikilink")

  - [Firdavs Faizullaev](../Page/Firdavs_Faizullaev.md "wikilink")

  - [Alisher Oulmassov](../Page/Alisher_Oulmassov.md "wikilink")

  - [Prasert Innui](../Page/Prasert_Innui.md "wikilink")

  - [Ekkapong Suratsawang](../Page/Ekkapong_Suratsawang.md "wikilink")

  - [Mekan Muhamedmuradov](../Page/Mekan_Muhamedmuradov.md "wikilink")

  - [Agajan Resulov](../Page/Agajan_Resulov.md "wikilink")

  - [Dilshod Irsaliev](../Page/Dilshod_Irsaliev.md "wikilink")

<!-- end list -->

  - 1球

<!-- end list -->

  - [Greg Giovenali](../Page/Greg_Giovenali.md "wikilink")

  - [Luke Haydon](../Page/Luke_Haydon.md "wikilink")

  - [Hu Jie](../Page/Hu_Jie.md "wikilink")

  - [Li Jian](../Page/Li_Jian_\(futsal\).md "wikilink")

  - [Zhang Jiong](../Page/Zhang_Jiong.md "wikilink")

  - [陳俊介](../Page/陳俊介.md "wikilink")

  - [黃秋慶](../Page/黃秋慶.md "wikilink")

  - [Fachry Assegaff](../Page/Fachry_Assegaff.md "wikilink")

  - [Deny Handoyo](../Page/Deny_Handoyo.md "wikilink")

  - [Sayan Karmadi](../Page/Sayan_Karmadi.md "wikilink")

  - [Topas Wiyantoro](../Page/Topas_Wiyantoro.md "wikilink")

  - [Mohammad Keshavarz](../Page/Mohammad_Keshavarz.md "wikilink")

  - [Hashim Khalid](../Page/Hashim_Khalid.md "wikilink")

  - [Ali Saad](../Page/Ali_Saad_Abdul-Hameed.md "wikilink")

  - [Kenta Fujii](../Page/Kenta_Fujii.md "wikilink")

  - [Kotaro Inaba](../Page/Kotaro_Inaba.md "wikilink")

  - [Takeshi Kishimoto](../Page/Takeshi_Kishimoto.md "wikilink")

  - [Meshari Al-Nakkas](../Page/Meshari_Al-Nakkas.md "wikilink")

  - [Mohammad Al-Nagi](../Page/Mohammad_Al-Nagi.md "wikilink")

<!-- end list -->

  - 1球(續)

<!-- end list -->

  - [Hamed Al-Otaibi](../Page/Hamed_Al-Otaibi.md "wikilink")

  - [Hamad Al-Othman](../Page/Hamad_Al-Othman.md "wikilink")

  - [Daniar Abdyraimov](../Page/Daniar_Abdyraimov.md "wikilink")

  - [Marat Duvanaev](../Page/Marat_Duvanaev.md "wikilink")

  - [Dilshat Kadyrov](../Page/Dilshat_Kadyrov.md "wikilink")

  - [Emil Kanetov](../Page/Emil_Kanetov.md "wikilink")

  - [Evgeniy Malinin](../Page/Evgeniy_Malinin.md "wikilink")

  - [Azamat Mendibaev](../Page/Azamat_Mendibaev.md "wikilink")

  - [Mihail Sundeev](../Page/Mihail_Sundeev.md "wikilink")

  - [Rabih Abou Chaaya](../Page/Rabih_Abou_Chaaya.md "wikilink")

  - [Abbas Fadlallah](../Page/Abbas_Fadlallah.md "wikilink")

  - [Ruzaley Abdul Aziz](../Page/Ruzaley_Abdul_Aziz.md "wikilink")

  - [Ahmad Hanif Sarmin](../Page/Ahmad_Hanif_Sarmin.md "wikilink")

  - [Addie Azwan Zainal](../Page/Addie_Azwan_Zainal.md "wikilink")

  - [Zaidi Zulkhapri](../Page/Zaidi_Zulkhapri.md "wikilink")

  - [Jung Hae-Hyuck](../Page/Jung_Hae-Hyuck.md "wikilink")

  - [Jung Hyuk](../Page/Jung_Hyuk.md "wikilink")

  - [Kim In-Woo](../Page/Kim_In-Woo.md "wikilink")

  - [Obidjon Ismoilov](../Page/Obidjon_Ismoilov.md "wikilink")

  - [Sherzod Jumaev](../Page/Sherzod_Jumaev.md "wikilink")

  - [Sermphan Khumthinkaew](../Page/Sermphan_Khumthinkaew.md "wikilink")

  - [Ekkapan Suratsawang](../Page/Ekkapan_Suratsawang.md "wikilink")

  - [Natthapon Suttiroj](../Page/Natthapon_Suttiroj.md "wikilink")

  - [Ukrit Tangtung](../Page/Ukrit_Tangtung.md "wikilink")

  - [Ahmed Allaberdiyev](../Page/Ahmed_Allaberdiyev.md "wikilink")

  - [Bayrammurat
    Esenmamedov](../Page/Bayrammurat_Esenmamedov.md "wikilink")

  - [Mergen Orazov](../Page/Mergen_Orazov.md "wikilink")

  - [Ildar Tashliyev](../Page/Ildar_Tashliyev.md "wikilink")

  - [Umid Holmatov](../Page/Umid_Holmatov.md "wikilink")

  - [Gulomjon Mamdjonov](../Page/Gulomjon_Mamdjonov.md "wikilink")

  - [Shuhrat Tojiboev](../Page/Shuhrat_Tojiboev.md "wikilink")

<!-- end list -->

  - 烏龍球

<!-- end list -->

  - [方靖仁](../Page/方靖仁.md "wikilink") (*對澳洲*)

  - [李盟乾](../Page/李盟乾.md "wikilink") (*對日本*)

  - [Maulana Ihsan](../Page/Maulana_Ihsan.md "wikilink") (*對泰國*)

  - [Ali Saad](../Page/Ali_Saad_Abdul-Hameed.md "wikilink") (*對吉爾吉斯*)

  - [Marat Duvanaev](../Page/Marat_Duvanaev.md "wikilink") (*對日本*)

  - [Rabih Abou Chaaya](../Page/Rabih_Abou_Chaaya.md "wikilink") (*對韓國*)

  - [Zaidi Zulkhapri](../Page/Zaidi_Zulkhapri.md "wikilink") (*對韓國*)

  - [Obidjon Ismoilov](../Page/Obidjon_Ismoilov.md "wikilink") (*對伊朗*)

  - [Bakhodur Sufiev](../Page/Bakhodur_Sufiev.md "wikilink") (*對伊朗*)

## 外部連結

  - [亞足聯官方網頁](http://www.the-afc.com/chi/competitions/fixtures/index.jsp_futsal-2008.html)

[Category:亞足聯五人制足球錦標賽](../Category/亞足聯五人制足球錦標賽.md "wikilink")
[亞](../Category/2008年足球.md "wikilink")