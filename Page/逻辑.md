**邏輯**（；；；；意大利语、西班牙语、葡萄牙语:
logica），又稱**理則**、**論理**、**推理**、**推論**，是对有效[推論的](../Page/推論.md "wikilink")[哲學研究](../Page/哲學.md "wikilink")\[1\]。邏輯被使用在大部份的智能活動中，但主要在[心理](../Page/心理.md "wikilink")、[学习](../Page/学习.md "wikilink")、[哲學](../Page/哲學.md "wikilink")、[語義學](../Page/語義學.md "wikilink")、[數學](../Page/數學.md "wikilink")、[推论统计学](../Page/推论统计学.md "wikilink")、[脑科学](../Page/脑科学.md "wikilink")、[法律和](../Page/法律.md "wikilink")[電腦科學等領域內被視為一門學科](../Page/電腦科學.md "wikilink")。邏輯討論[邏輯論證會呈現的](../Page/邏輯論證.md "wikilink")[一般形式](../Page/一般形式.md "wikilink")，哪種形式是有效的，以及其中的[謬論](../Page/謬論.md "wikilink")。

邏輯通常可分為三個部份：[歸納推理](../Page/歸納推理.md "wikilink")、[溯因推理和](../Page/溯因推理.md "wikilink")[演繹推理](../Page/演繹推理.md "wikilink")。

在[哲學裡](../Page/哲學.md "wikilink")，邏輯被應用在大多數的主要領域之中：[形上學](../Page/形上學.md "wikilink")/[宇宙論](../Page/宇宙論.md "wikilink")、[本體論](../Page/本體論_\(哲學\).md "wikilink")、[知識論及](../Page/知識論.md "wikilink")[倫理學](../Page/倫理學.md "wikilink")。

在[數學裡](../Page/數學.md "wikilink")，邏輯是指形式逻辑和数理邏輯，形式逻辑是研究某個[形式語言的有效](../Page/形式語言.md "wikilink")[推論](../Page/推論.md "wikilink")\[2\]。主要是演繹推理。
在[辯證法中也會學習到邏輯](../Page/辯證法.md "wikilink")\[3\]。数理邏輯是研究抽象邏輯关系和数学基本的问题。

在[心理](../Page/心理.md "wikilink")、[脑科学](../Page/脑科学.md "wikilink")、[語义学](../Page/語义学.md "wikilink")、[法律裡](../Page/法律.md "wikilink")，是研究人类思想推理的处理。

在[学习](../Page/学习.md "wikilink")、[推论统计学裡](../Page/推论统计学.md "wikilink")，是研究最大可能的结论。主要是[歸納推理](../Page/歸納推理.md "wikilink")、[溯因推理](../Page/溯因推理.md "wikilink")。

在[電腦科學裡](../Page/電腦科學.md "wikilink")，
是研究各种方法的性质，可能性，和实现在机器上。主要是[歸納推理](../Page/歸納推理.md "wikilink")、[溯因推理](../Page/溯因推理.md "wikilink")，也有在[歸納推理的研究](../Page/歸納推理.md "wikilink")。

从[古文明开始](../Page/古文明.md "wikilink")（如[古印度](../Page/古印度.md "wikilink")、[中國](../Page/中國.md "wikilink")和[古希臘](../Page/古希臘.md "wikilink")）都有對邏輯進行研究。在西方，[亞里斯多德將邏輯建立成一門正式的學科](../Page/亞里斯多德.md "wikilink")，並在哲學中給予它一個基本的位置。

## 概論

邏輯（）的[字根源起於](../Page/字根.md "wikilink")[希臘語](../Page/古希臘.md "wikilink")[邏各斯](../Page/邏各斯.md "wikilink")（），最初的意思有[詞語](../Page/詞語.md "wikilink")、[思想](../Page/思想.md "wikilink")、[概念](../Page/概念.md "wikilink")、[推理](../Page/推理.md "wikilink")、論點之意。後譯為（），最後發展為英文中的邏輯（）。

1902年[嚴復譯](../Page/嚴復.md "wikilink")《穆勒名學》時，將其意譯為「名學」，但這不合[名家或者](../Page/名家.md "wikilink")[名教之名學中](../Page/名教.md "wikilink")「名」的本意。[和製漢語採用漢字](../Page/和製漢語.md "wikilink")「論理」，意譯為「論理學」。[孫文於其文](../Page/孫文.md "wikilink")《治國方略·以作文為證》意譯為「理則」，當代中文一般採取音譯方式，將其譯為邏輯。

邏輯本身是指是推論和[證明的思想過程](../Page/證明.md "wikilink")，而邏輯學是研究「有效[推論和證明的](../Page/推論.md "wikilink")[原則與](../Page/原則.md "wikilink")[標準](../Page/標準.md "wikilink")」的一門學科。作為一個[形式科學](../Page/形式科學.md "wikilink")，邏輯透過對[推論的](../Page/推論.md "wikilink")[形式系統與](../Page/形式系統.md "wikilink")[自然語言中的](../Page/自然語言.md "wikilink")[論證等來研究並分類命題與論證的結構](../Page/論證.md "wikilink")。\[4\]

逻辑的范围是非常广阔的，從對[謬論與](../Page/謬論.md "wikilink")[悖論的研究之類的核心議題](../Page/悖論.md "wikilink")，到利用[機率來推論及包含](../Page/機率.md "wikilink")[因果論的論證等專業的推理分析](../Page/因果.md "wikilink")。邏輯在今日亦常被使用在論辯理論之中。\[5\]

传统上，逻辑被作为[哲学的一个分支来研究](../Page/哲学.md "wikilink")，和[文法與](../Page/文法.md "wikilink")[修辭一同被稱為](../Page/修辭.md "wikilink")**古典三學科**。自十九世紀中葉，「形式邏輯」已被作為[數學基礎而被研究](../Page/數學基礎.md "wikilink")，當中經常被稱之為[符號邏輯](../Page/符號邏輯.md "wikilink")。1903年，[阿弗烈·諾夫·懷海德與](../Page/阿弗烈·諾夫·懷海德.md "wikilink")[伯特蘭·羅素寫成了](../Page/伯特蘭·羅素.md "wikilink")《Principia
Mathematica》，試圖將邏輯形式地建立成數學的基石。\[6\]不過，除了些基本的以外，當時的系統已不再被使用，大部份都被[集合論所取代掉了](../Page/集合論.md "wikilink")。當對形式邏輯的研究漸漸地擴張了之後，研究也不再只侷限於基礎的議題，之後的各個[數學領域被合稱為](../Page/數學.md "wikilink")[數理邏輯](../Page/數理邏輯.md "wikilink")。形式邏輯的發展和其在電腦上的應用是[電腦科學的基礎](../Page/電腦科學.md "wikilink")。\[7\][戈特弗里德·莱布尼茨](../Page/戈特弗里德·莱布尼茨.md "wikilink")、[乔治·布尔](../Page/乔治·布尔.md "wikilink")、[戈特洛布·弗雷格](../Page/戈特洛布·弗雷格.md "wikilink")、
[大卫·希尔伯特](../Page/大卫·希尔伯特.md "wikilink")、[库尔特·哥德尔](../Page/库尔特·哥德尔.md "wikilink")，等等，都在這個過程中非常重要。
\[8\]

## 本质

[形式是邏輯的核心](../Page/形式_\(哲学\).md "wikilink")，但在「形式邏輯」中對「形式」使用時常不很明確，因而使其闡述變得很費解。其中，符號邏輯僅為形式邏輯的一種類型，而和形式邏輯的另一種類型－只處理[直言命題的](../Page/直言命題.md "wikilink")[三段論不同](../Page/三段論.md "wikilink")。\[9\]

  - **[非形式邏輯](../Page/非形式邏輯.md "wikilink")**是研究[自然語言](../Page/自然語言.md "wikilink")[論證的一門學科](../Page/邏輯論證.md "wikilink")。對[謬論的研究是非形式邏輯中尤其重要的一個分支](../Page/謬論.md "wikilink")。[柏拉圖的作品](../Page/柏拉圖.md "wikilink")\[10\]是非形式邏輯的一重要例子。

<!-- end list -->

  - **形式邏輯**是研究純形式內容的[推論的一門學科](../Page/推論.md "wikilink")，這種內容是很明確的。若一个推论可以被表達成一個完全抽象的規則（即不只是和任一特定事物或性質有關的規則）的一個特定應用，则这个推論擁有**純形式內容**。形式邏輯的規則由[亞里斯多德最先寫成](../Page/亞里斯多德.md "wikilink")\[11\]。在許多邏輯的定義中，邏輯推論與帶有純形式內容的推論會是同一種概念。但這不表示非形式邏輯的概念是空洞的，因為沒有任何一種形式語言可以捕捉到自然語言語義間所有的微細差別。

<!-- end list -->

  - **[符號邏輯](../Page/符號邏輯.md "wikilink")**捕獲了邏輯推論的形式特徵，並將其抽象化為符號的研究\[12\]\[13\]。符號邏輯通常分為兩個分支：[命題邏輯和](../Page/命題邏輯.md "wikilink")[謂詞邏輯](../Page/謂詞邏輯.md "wikilink")。

<!-- end list -->

  - **[數理邏輯](../Page/數理邏輯.md "wikilink")**是符號邏輯在其他領域中的延伸，特別是对[模型論](../Page/模型論.md "wikilink")、[證明論](../Page/證明論.md "wikilink")、[集合論和](../Page/集合論.md "wikilink")[遞歸論的研究](../Page/遞歸論.md "wikilink")。

「形式邏輯」通常作为符號邏輯的同義詞，而非形式邏輯則是被理解為不包含符號抽象化的任何一種邏輯推論；這是由「[形式語言](../Page/形式語言.md "wikilink")」和「[形式理論](../Page/形式理論.md "wikilink")」中類推而來的用法。但廣義地來說，形式邏輯是古老的，可追溯至兩千年以前，而符號邏輯則相對較新，只有一個世紀左右的歷史而已。

### 邏輯學基本原理

  - [同一律](../Page/同一律.md "wikilink")（the law of identity）
    事物跟其自身相等同，「自己」不能「不是自己」。
  - [無矛盾律](../Page/無矛盾律.md "wikilink")（the law of non-contradiction）
    事物不能同時「是」跟「不是」。是就是，不是就不是。
  - [排中律](../Page/排中律.md "wikilink")（the law of excluded middle）
    事物只能有「是」或「不是」兩種狀態，不存在其他中間狀態。
  - [充足理由律](../Page/充足理由律.md "wikilink")（the law of sufficient reason）
    任何事物都有其存在的充足理由。

### 邏輯系統的性質

[邏輯系統可具有下列性質](../Page/形式系統.md "wikilink")：

  - [有效性](../Page/有效性.md "wikilink")（validity）
    依系統的[推理規則](../Page/推理規則.md "wikilink")，若所有[前提皆為](../Page/前提.md "wikilink")[真則結論必為真](../Page/真理.md "wikilink")（保真）。所有[命題之前提皆](../Page/命題.md "wikilink")**[語義蘊涵](../Page/語義蘊涵.md "wikilink")**（semantic
    consequence）結論。
  - [自洽性](../Page/一致性_\(邏輯\).md "wikilink")（consistency）
    系統中任一定理都不與其他定理相[矛盾](../Page/矛盾.md "wikilink")。不存在命題P，P和[非P皆可在系統中](../Page/邏輯非.md "wikilink")[證明](../Page/證明.md "wikilink")。
  - [可靠性](../Page/可靠性定理.md "wikilink")（soundness）
    系統中所有定理（有效且可證明的命題）皆為真。可靠性與完備性互為[逆命題](../Page/逆命題.md "wikilink")。
  - [完備性](../Page/完備性.md "wikilink")（completeness）
    系統中不存在無法證明或證否的有效命題。系統中真命題皆可證明（真命題皆為定理）且假命題皆可證否。

一些[邏輯系統不擁有上述所有性質](../Page/邏輯系統.md "wikilink")，比如[庫爾特·哥德爾的](../Page/庫爾特·哥德爾.md "wikilink")[哥德爾不完備定理證明了](../Page/哥德爾不完備定理.md "wikilink")，沒有任何一個蘊涵[皮亞諾公理的算術形式系統可以同時滿足自洽性和完備性](../Page/皮亞諾公理.md "wikilink")。\[14\]同時他的針對沒有通過特定公理擴展為帶有等式的算術形式系統的一階謂詞邏輯的[定理](../Page/哥德爾完備性定理.md "wikilink")，證實了它們可以同時滿足自洽性和完備性。\[15\]

### 对於逻辑的不同理解

邏輯產生於對論證正確性的關注。邏輯是對論證的研究，這個概念在歷史上是很基本的，而這也是不同邏輯傳統的創立者如[柏拉圖和](../Page/柏拉圖.md "wikilink")[亞里斯多德所設想的](../Page/亞里斯多德.md "wikilink")。現代的[邏輯學家通常會希望確保對邏輯的研究只侷限於由適度一般化了的推論中所產生出來的論證](../Page/邏輯學家.md "wikilink")；所以如《[斯坦福哲學百科](../Page/史丹佛哲學百科全書.md "wikilink")》所稱，「邏輯……沒有涵蓋有效推理的整個課題，那是理性[理論的工作](../Page/理論.md "wikilink")。更明確地說，邏輯處理一種推論，其有效性可追溯至推論中的表述的形式特徵，這可以是語言的，心理的，或其他的表述。」(Hofweber
2004).\[16\]

相對地，[伊曼努爾·康德引入了另一種概念來闡述什麼是邏輯](../Page/伊曼努爾·康德.md "wikilink")。他主張邏輯應當被設想為判斷的科學，這種想法被[戈特洛布·弗雷格採納](../Page/戈特洛布·弗雷格.md "wikilink")，寫入他的邏輯與哲學著作之中，其中，思維（）這一詞取代了康德的判斷（）。在此觀點下，有效的邏輯推論是源於判斷或思維的結構特徵。

### 演绎和归纳

[演繹推理關注於從給定的前提下有什麼是可得出的](../Page/演繹推理.md "wikilink")。而[歸納推理](../Page/歸納推理.md "wikilink")（從觀察中推論出可靠廣義化的過程）有時也被包含在對邏輯的研究中。相對應地，必須要區分出演繹有效性和[歸納有效性](../Page/確認性.md "wikilink")。一個推論是演繹有效的，若且唯若不可能存在所有前提皆為真但結論為假的狀況。對於形式邏輯的系統，演繹有效性的概念可以用[語義學中已明確理解的概念嚴格地陳述出来](../Page/語義學.md "wikilink")。另一方面，歸納的有效性則要求必須定義對某一觀察集合的「可靠廣義化」。此定義可以用各種不同的方式來達成，有的方式會比其他的方式不那麼形式化；有些定義也許會用到機率的[數學模型](../Page/數學模型.md "wikilink")。\[17\]

## 發展历史

许多文化都采用复杂的推理系统，最初僅有三个地方把逻辑学作为對推理方法的明确分析，並且有持续的发展，那就是前6世纪的[印度](../Page/印度.md "wikilink")、前5世纪的[中国和前](../Page/中国.md "wikilink")4世纪与前1世纪间的[希腊](../Page/希腊.md "wikilink")。

现代逻辑的形式复杂处理明显源自希腊传统，但是有人提出[布尔逻辑的先驱可能知道印度逻辑](../Page/布尔逻辑.md "wikilink")（Ganeri
2001）。希腊传统自身来自[亚里士多德逻辑的传播](../Page/亚里士多德逻辑.md "wikilink")，[伊斯兰哲学家和](../Page/伊斯兰哲学家.md "wikilink")[中世纪逻辑学家对它的评论](../Page/中世纪逻辑学家.md "wikilink")。欧洲以外的传统没有存活到现代时期：在中国，对逻辑的学术研究传统在[韩非的法家哲学之后就被](../Page/韩非.md "wikilink")[秦朝压制](../Page/秦朝.md "wikilink")；在伊斯兰世界，[艾什爾里派](../Page/艾什爾里派.md "wikilink")（Ash'ari）的崛起压制了逻辑的原始工作。

但是在印度，经院学派[正理派的创新持续到](../Page/正理经.md "wikilink")18世纪早期。它没有存活到。在20世纪，西方哲学家如Stanislaw
Schayer和Klaus Glashoff探究了[印度传统逻辑学的某些方面](../Page/印度邏輯.md "wikilink")。

中世纪时期，在[亚里士多德的想法显示与信仰大量兼容之后](../Page/亚里士多德.md "wikilink")，他的逻辑被给予更大强调。在中世纪的后期，逻辑成为一部分哲学家的關注焦点，他们專注於對哲学论证的逻辑分析。

## 逻辑学学科体系

## 注释

## 参考文献

### 引用

### 来源

  - G. Birkhoff and J. von Neumann, 1936. 'The Logic of Quantum
    Mechanics'. *[Annals of
    Mathematics](../Page/Annals_of_Mathematics.md "wikilink")*,
    37:823-843.
  - D. Finkelstein, 1969. 'Matter, Space and Logic'. In R. S. Cohen and
    M. W. Wartofsky, (eds.), *Proceedings of the Boston Colloquium for
    the Philosophy of Science*, Boston Studies in the Philosophy of
    Science, vol 13. ISBN 978-90-277-0377-4.
  - D. M. Gabbay and F. Guenthner (eds.) 2001-2005. *Handbook of
    philosophical logic* (2nd ed.). 13 volumes. Dordrecht, Kluwer.
  - D. Hilbert and W. Ackermann, 1928. *Grundzüge der theoretischen
    Logik* ([Principles of Theoretical
    Logic](../Page/Principles_of_Theoretical_Logic.md "wikilink")).
    Springer-Verlag, ISBN 978-0-8218-2024-7.
  - W. Hodges, 2001. *Logic. An introduction to elementary logic*.
    Penguin Books.
  - T. Hofweber, 2004. [Logic and
    Ontology](http://plato.stanford.edu/entries/logic-ontology/). In the
    [Stanford Encyclopedia of
    Philosophy](../Page/Stanford_Encyclopedia_of_Philosophy.md "wikilink").
  - R. I. G. Hughes (editor), 1993. *A Philosophical Companion to
    First-Order Logic*. Hackett.
  - W. Kneale and M. Kneale, 1962/1988. *The Development of Logic*.
    Oxford University Press, ISBN 978-0-19-824773-9.
  - G. Priest, 2004.
    [Dialetheism](http://plato.stanford.edu/entries/dialetheism/). In
    the [Stanford Encyclopedia of
    Philosophy](../Page/Stanford_Encyclopedia_of_Philosophy.md "wikilink").
  - H. Putnam, 1969. *Is Logic Empirical?*. Boston Studies in the
    Philosophy of Science, vol V.
  - B. Smith, 1989. 'Logic and the Sachverhalt', *The Monist*,
    72(1):52-69.
  - D. Vernant, 2018. 'Questions de Logique et de Philosophie'. Mimesis,
    ISBN 978-8-8697-6102-7

## 外部連結

  - 倪梁康：[〈現象學與邏輯學〉](http://www.aisixiang.com/data/26989.html)（2004年）
  - 倪梁康：[〈《邏輯研究》中的觀念對象和觀念直觀〉](http://www.aisixiang.com/data/26775.html)（2009年）
  - [History of Logic in Relationship to
    Ontology](http://www.ontology.co/history-of-logic.htm) Annotated
    bibliography on the history of logic

{{-}}

[邏輯](../Category/邏輯.md "wikilink")

1.

2.

3.

4.  J. Bruno Leclercq et Laurence Bouquiaux, *Logique formelle et
    argumentation*, Édition 3, De Boeck Université, 2017 ISBN
    978-2-8073-1446-7, ISBN 978-2807314467

5.  J. Robert Cox and Charles Arthur Willard, eds. *Advances in
    Argumentation Theory and Research*, Southern Illinois University
    Press, 1983 ISBN 978-0-8093-1050-0, ISBN 978-0809310500

6.  Alfred North Whitehead and Bertrand Russell, *Principia Mathematical
    to \*56*, Cambridge University Press, 1967, ISBN 978-0-521-62606-4

7.  J. Dirk W. Hoffmann, *Grenzen der Mathematik: Eine Reise durch die
    Kerngebiete der mathematischen Logik*, Auflage: 3, Springer
    Spektrum, 2018 ISBN 978-3-6625-6616-9, ISBN 978-3662566169

8.  J. Martin Davis, *The Universal Computer. The Road from Leibniz to
    Turing*, A K Peters and CRC Press, 2011 ISBN 978-1-4665-0520-9, ISBN
    978-1466505209

9.  J. Antonio Joaquín Roldán Marco, *Lógica: 1º Bachillerato*,
    publicado de forma independiente, 2018 ISBN 978-1-9806-4558-0, ISBN
    978-1980645580

10. Plato, *The Portable Plato*, edited by Scott Buchanan, Penguin,
    1976, ISBN 978-0-14-015040-7

11. Aristotle, *The Basic Works*, [Richard
    Mckeon](../Page/Richard_Mckeon.md "wikilink"), editor, Modern
    Library, 2001, ISBN 978-0-375-75799-0, see especially, *[Posterior
    Analytics](../Page/Posterior_Analytics.md "wikilink")*.

12.
13. For a more modern treatment, see A. G. Hamilton, *Logic for
    Mathematicians*, Cambridge, 1980, ISBN 978-0-521-29291-7

14.
15.

16.
17. J. Jörg Hardy und Christoph Schamberger, *Logik der Philosophie:
    Einführung in die Logik und Argumentationstheorie*, UTB GmbH, 2017
    ISBN 978-3-8252-4897-0, ISBN 978-3825248970