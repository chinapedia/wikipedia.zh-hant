《**座頭市**》（**ざとういち**，***Zatoichi***）是一部2003年的[日本歷史武士電影](../Page/日本.md "wikilink")，即[武俠小說](../Page/武俠小說.md "wikilink")[盲劍客改編的影像作品](../Page/盲劍客.md "wikilink")，描述一位武功高強的失明劍客座頭市遊歷日本各地，懲奸除惡的故事。[勝新太郎曾多次擔綱演出](../Page/勝新太郎.md "wikilink")[電影](../Page/電影.md "wikilink")、[連續劇](../Page/電視劇.md "wikilink")。勝新太郎死後，[北野武於](../Page/北野武.md "wikilink")2003年重新翻拍，並自導自演，獲得了2003年的[威尼斯電影節銀獅獎](../Page/威尼斯電影節.md "wikilink")。[山本耀司和黑泽明的女儿负责服装设计](../Page/山本耀司.md "wikilink")。这部表现暴力美的电影给观众以强烈的感官刺激，可谓日本武侠动作片的杰出代表。

## 情节

[盲剑客座](../Page/盲剑客.md "wikilink")-{}-头市来到一座被黑帮赌场控制的小镇，他遇到了两名艺妓，她们的父母被黑帮残忍杀害，他帮助她们进行复仇的故事。最終座頭市與黑幫聘請的落泊劍客進行決鬥。

## 角色

  - [北野武](../Page/北野武.md "wikilink") 饰 座头市 　　
  - [浅野忠信](../Page/浅野忠信.md "wikilink") 饰 服部源之助
  - [夏川结衣](../Page/夏川结衣.md "wikilink")（Yui Natsukawa）饰 服部源之助之妻
  - [橘大五郎](../Page/橘大五郎.md "wikilink") 饰 歌舞伎 　　
  - [大家由佑子](../Page/大家由佑子.md "wikilink") 饰 艺妓 　　
  - [岸部一徳](../Page/岸部一徳.md "wikilink") 饰 银藏

## 奖项

  - 第27届[日本电影学院奖](../Page/日本电影学院奖.md "wikilink")（五个技术奖，本届获奖最多的影片）
      - 最佳摄影：[柳岛克己](../Page/柳岛克己.md "wikilink")
      - 最佳照明：[高屋齐](../Page/高屋齐.md "wikilink")
      - 最佳剪辑：[北野武](../Page/北野武.md "wikilink")、[太田义则](../Page/太田义则.md "wikilink")
      - 最佳音乐：[铃木庆一](../Page/铃木庆一.md "wikilink")
      - 最佳录音
  - 第28届[多伦多国际电影节](../Page/多伦多国际电影节.md "wikilink")[观众票选奖](../Page/观众票选奖.md "wikilink")\[1\]
  - 第60届[威尼斯影展開幕獎](../Page/威尼斯影展.md "wikilink")／特別導演獎／未來電影節數位獎／觀眾票選最佳影片

## 参考资料

## 外部链接

  - [官方网站](http://www.office-kitano.co.jp/zatoichi)
  - [网易娱乐电影资料](http://ent.163.com/ent_2003/editor/movie/film/030905/030905_190100.html)

[Category:日本电影作品](../Category/日本电影作品.md "wikilink")
[Category:北野武電影](../Category/北野武電影.md "wikilink")
[Category:2003年電影](../Category/2003年電影.md "wikilink")
[Category:动作片](../Category/动作片.md "wikilink")
[Category:朝日電視台製作的電影](../Category/朝日電視台製作的電影.md "wikilink")

1.  <http://www.kitanotakeshi.com/pdf/1104143218_zatoichi_pk_uk.pdf>