[Grigol_Ordzhonikidze_at_the_10th_Party_Congress_(3).jpg](https://zh.wikipedia.org/wiki/File:Grigol_Ordzhonikidze_at_the_10th_Party_Congress_\(3\).jpg "fig:Grigol_Ordzhonikidze_at_the_10th_Party_Congress_(3).jpg")
**格里戈里·康斯坦丁诺维奇·奥尔忠尼启则**（[喬治亞語](../Page/喬治亞語.md "wikilink")：****
*转写：*；；），昵称“**谢尔戈**”（[喬治亞語](../Page/喬治亞語.md "wikilink")：****
*转写：*；），[格鲁吉亚著名革命家](../Page/格鲁吉亚.md "wikilink")，[斯大林的亲信之一](../Page/斯大林.md "wikilink")。在[苏共政治局](../Page/苏共政治局.md "wikilink")，他与[斯大林](../Page/斯大林.md "wikilink")、[米高扬被称为高加索小集团](../Page/米高扬.md "wikilink")，尽管这是个带玩笑性质的称谓。

## 早年生涯

奥尔忠尼启则出生在[格鲁吉亚西部的](../Page/格鲁吉亚.md "wikilink")[哈拉加烏利的一个村庄](../Page/哈拉加烏利市.md "wikilink")，1903年17岁的时候开始热衷政治，在他毕业于[第比利斯的米哈伊洛夫医院医学校后就因为武器交易而被捕](../Page/第比利斯.md "wikilink")，1907年他在[巴库活动](../Page/巴库.md "wikilink")，并与斯大林等人一起共事，其后，他依[布尔什维克党的指派](../Page/布尔什维克党.md "wikilink")，前往伊朗参与了[波斯宪政革命](../Page/波斯宪政革命.md "wikilink")，并在[德黑兰活动了一年多](../Page/德黑兰.md "wikilink")。他因为[社会民主党的身份被捕](../Page/社会民主党.md "wikilink")，被驱逐到[西伯利亚](../Page/西伯利亚.md "wikilink")。但3年之后又逃了回来，1911年夏季，奉列宁指示，他和斯大林一起回到[圣彼得堡](../Page/圣彼得堡.md "wikilink")。1912年1月在布拉格召开的社会民主党（布）六大上当选为中央委员、[俄罗斯中央局成员](../Page/俄罗斯中央局.md "wikilink")。1912年4月14日被捕，判罚去[雅库茨克服](../Page/雅库茨克.md "wikilink")3年苦役，他在流放地做医生。苦役期满后回到圣彼得堡。[十月革命前](../Page/十月革命.md "wikilink")，是社会民主党（布尔什维克）彼得格勒市委成员、彼得格勒苏维埃执委，积极参加了十月革命。

十月革命后，奥尔忠尼启则成为乌克兰与高加索地区的红军政委，1921年，他领导了布尔什维克对[格鲁吉亚民主共和国的进攻并建立了](../Page/格鲁吉亚民主共和国.md "wikilink")[格鲁吉亚社会主义共和国](../Page/格鲁吉亚社会主义共和国.md "wikilink")，之后，他努力削减格鲁吉亚的自治权力，并在1922年的[格鲁吉亚事件中成为一个十分关键的角色](../Page/格鲁吉亚事件.md "wikilink")。1922年2月起为外高加索共产党书记。1926年9月起任北高加索边疆区委书记。

## 高峰突坠

1921年至1927年，1934年至1937年为苏共（布）中央委员。

在[布哈林的漫画中](../Page/布哈林.md "wikilink")，奥尔忠尼启则被描绘成一个年少的俄国卫兵。1926年，[斯大林完全掌权后](../Page/斯大林.md "wikilink")，奥成为[政治局委员](../Page/政治局.md "wikilink")，然而到了1936年，[斯大林开始怀疑奥尔忠尼启则对其的忠诚](../Page/斯大林.md "wikilink")，特别是当他发现奥尔忠尼启则开始利用其影响力保护一些被苏联秘密警察和内务人民委员会（NKVD）调查的人物，同时，有传言奥尔忠尼启则准备在4月的中央全会上作公开抨击斯大林的发言。2月18日，奥尔忠尼启则在与斯大林激烈争吵后用剃刀自杀。依据[赫鲁晓夫的回忆录](../Page/赫鲁晓夫.md "wikilink")，奥尔忠尼启则曾经向[阿纳斯塔斯·米高扬吐露](../Page/阿纳斯塔斯·伊万诺维奇·米高扬.md "wikilink")，他再没办法应对党内同志无端和武断的谋杀。

在他身后，[苏联有几个城镇以其的名字命名](../Page/苏联.md "wikilink")，比如[弗拉季高加索](../Page/弗拉季高加索.md "wikilink")。

[О](../Category/蘇聯政治人物.md "wikilink")
[О](../Category/喬治亞政治人物.md "wikilink")
[О](../Category/蘇聯自殺者.md "wikilink")
[О](../Category/蘇聯共產黨中央政治局委員.md "wikilink")
[Category:老布尔什维克](../Category/老布尔什维克.md "wikilink")