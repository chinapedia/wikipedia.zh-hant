**Corel Painter**是一套专业的自然媒介（natural media）电脑绘画软件，原由[Fractal
Design开发](../Page/Fractal_Design.md "wikilink")，后经[MetaCreations最终转给了](../Page/MetaCreations.md "wikilink")[Corel](../Page/Corel.md "wikilink")。和[Dabbler一样](../Page/Dabbler.md "wikilink")，其可配合带压感笔的[数位板使用](../Page/数位绘图板.md "wikilink")，而功能则较为专业。自8.0版始，其界面由画室风格变为类似[Photoshop的风格](../Page/Photoshop.md "wikilink")。迄今为止，最新版本为2017。其面向儿童版本[Dabbler在变为](../Page/Dabbler.md "wikilink")[Art
Dabbler后停止开发](../Page/Art_Dabbler.md "wikilink")；简化版原为[Painter
Classic](../Page/Painter_Classic.md "wikilink")，现改名为[Painter
Essentials](../Page/Painter_Essentials.md "wikilink")。

## 历史版本

  - Fractal Design Painter Version 1.2 (1991-92)
  - Fractal Design Painter Version 2.0 (1991-93)
  - Fractal Design Painter Version 3.1 (1991-94)
  - Fractal Design Painter Version 4.0 (1991-95)
  - Fractal Design Painter Version 5.0 (1991-97)
  - MetaCreations Painter 5.5 (1990-98)
  - MetaCreations Painter Classic (1990-98)
  - MetaCreations Painter 6.0 (1990-99)
  - Corel Painter Classic (2000)
  - Corel Painter 6.1 (2000)
  - Corel Painter 7 (2001)
  - Corel Painter 8 (2003)
  - Corel Painter IX (2004)
  - Corel Painter X (2006)
  - Corel Painter 11 (2009)
  - Corel Painter 12 (2011)
  - Corel Painter X3 (2013)
  - Corel Painter 2015 (2014)
  - Corel Painter 2016 (2015)
  - Corel Painter 2017 (2016)
  - Corel Painter 2018 (2017)

## 外部連結

  - [Corel Website](http://www.corel.com)
  - [Painter](https://web.archive.org/web/20071109050518/http://www.dmoz.org/Computers/Software/Graphics/Image_Editing/Painter/)
    at the [Open Directory
    Project](../Page/Open_Directory_Project.md "wikilink")
  - [Official Corel Painter Magazine](http://www.paintermagazine.co.uk)
  - [Corel Painter X Master's
    Gallery](http://apps.corel.com/painterx/us/gallery_artwork.html)
  - [Discussion of Apple's resolution-independent interface
    patent](http://www.cabel.name/2007/01/apples-next-generation-themes.html)
    from Cabel Sasser of Panic Software (PDF copy of patent filing
    included)

[Category:位圖編輯軟體](../Category/位圖編輯軟體.md "wikilink")
[Category:美商藝電](../Category/美商藝電.md "wikilink")
[Category:繪圖軟體](../Category/繪圖軟體.md "wikilink")