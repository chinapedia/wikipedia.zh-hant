**舞鶴號誌站**位於[臺灣](../Page/臺灣.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")[瑞穗鄉](../Page/瑞穗鄉.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[臺東線已廢止的鐵路](../Page/臺東線.md "wikilink")[號誌站](../Page/臺灣鐵路管理局車站等級#號誌站.md "wikilink")。2017年9月26日，路線切換至[新自強隧道新線後正式裁撤](../Page/自強隧道_\(花蓮縣\)_#自強隧道（第二代）.md "wikilink")\[1\]。

## 車站構造

  - 站內兩股交會線。
  - 北端設有[安全側線](../Page/安全側線.md "wikilink")，防止列車對撞。
  - 曾設有[島式月台一座](../Page/島式月台.md "wikilink")，大理石鋪面，已經拆除。

## 利用狀況

  - 無人[號誌站](../Page/臺灣鐵路管理局車站等級#號誌站.md "wikilink")，由[瑞穗車站管理](../Page/瑞穗車站_\(台灣\).md "wikilink")。
  - 僅供列車交會待避使用，不辦理客運。

## 歷史

  - 1956年：設立舞鶴車站，招呼站。
  - 1957年12月1日：改為三等站。
  - 1971年6月1日：降為簡易站。
  - 1972年7月1日：改為招呼站。
  - 1982年：東線鐵路拓寬，車站重建完成但並未設站營業。
  - 2005年9月30日：設立號誌站。拆除原本廢棄的月臺，另鋪上一股道。
  - 2017年9月26日：切換至新自強隧道新線，本站正式裁撤。\[2\]

## 車站週邊

  - [自強隧道](../Page/自強隧道_\(花蓮縣\).md "wikilink")
  - [掃叭隧道](../Page/掃叭隧道.md "wikilink")
  - [舞鶴石柱](../Page/舞鶴石柱.md "wikilink")
  - [北回歸線紀念碑](../Page/北回歸線.md "wikilink")
  - [瑞穗牧場](../Page/瑞穗牧場.md "wikilink")

## 鄰近車站

## 參考資料

[花](../Page/分類:台灣鐵路廢站.md "wikilink")

[Category:台東線車站](../Category/台東線車站.md "wikilink")
[Category:台灣鐵路號誌站](../Category/台灣鐵路號誌站.md "wikilink")
[Category:花蓮縣鐵路車站](../Category/花蓮縣鐵路車站.md "wikilink")
[Category:1957年啟用的鐵路車站](../Category/1957年啟用的鐵路車站.md "wikilink")
[Category:2017年关闭的铁路车站](../Category/2017年关闭的铁路车站.md "wikilink")
[Category:1957年台灣建立](../Category/1957年台灣建立.md "wikilink")
[Category:2017年台灣廢除](../Category/2017年台灣廢除.md "wikilink")

1.

2.