**石墨**（Graphite），又稱**黑鉛**（Black
Lead），是[碳的一種](../Page/碳.md "wikilink")[同素異形體](../Page/同素異形體.md "wikilink")（碳的其他同素異形體有很多，為人熟悉的例如[鑽石](../Page/鑽石.md "wikilink")）。作为最軟的[礦物之一](../Page/礦物.md "wikilink")，石墨不透明且觸感油膩，顏色由鐵黑到鋼鐵灰不等，形狀可呈晶體狀、薄片狀、鱗狀、條紋狀、層狀體，或散佈在[變質岩](../Page/變質岩.md "wikilink")（由[煤](../Page/煤.md "wikilink")、碳質岩石或碳質沉積物，受到區域變質作用或是[岩漿侵入作用形成](../Page/岩漿.md "wikilink")）之中\[1\]。化学性质不活泼，具有[耐腐蚀性](../Page/腐蚀#抗腐蚀性.md "wikilink")。

## 结构

[graphite.gif](https://zh.wikipedia.org/wiki/File:graphite.gif "fig:graphite.gif")
[Graphite-layers-side-3D-balls.png](https://zh.wikipedia.org/wiki/File:Graphite-layers-side-3D-balls.png "fig:Graphite-layers-side-3D-balls.png")
[Graphite-layers-top-3D-balls.png](https://zh.wikipedia.org/wiki/File:Graphite-layers-top-3D-balls.png "fig:Graphite-layers-top-3D-balls.png")
石墨具有层状的平面结构，结构如左图所示。每层中碳原子都排列成蜂窝状晶体结构，层内原子间距0.142nm，层间距0.335nm。层内每個碳[原子的週邊以](../Page/原子.md "wikilink")[共价键連結著另外三個碳原子](../Page/共价键.md "wikilink")，排列方式呈蜂巢式的多個六邊形，每層間有微弱的[范德華力](../Page/范德華力.md "wikilink")。由於每個碳原子均會放出一個[電子](../Page/電子.md "wikilink")，那些電子能夠自由移動，因此石墨屬於導電體。

## 用途

[Graphite_write.jpg](https://zh.wikipedia.org/wiki/File:Graphite_write.jpg "fig:Graphite_write.jpg")
它的用途包括製造[鉛筆筆芯和潤滑劑或](../Page/鉛筆.md "wikilink")[集電弓上的碳刷條](../Page/集電弓.md "wikilink")，也可作為[壓力管式石墨慢化沸水反應爐的中子減速劑](../Page/壓力管式石墨慢化沸水反應爐.md "wikilink")。

目前主要用途是耐火材料的原材料，尤其是[镁碳砖](../Page/镁碳砖.md "wikilink")。自然界自然形成的石墨可分为鳞片石墨和土状石墨。

因晶体结构中存在大量[离域电子](../Page/离域电子.md "wikilink")，石墨可以导电，其与晶体层平行的方向[电阻率为](../Page/电阻率.md "wikilink")(2.5～5.0)×10<sup>-6</sup>Ω·m，与层垂直的方向电阻率为3×10<sup>-3</sup>Ω·m。\[2\]

[電解中的惰性電極可以由](../Page/電解.md "wikilink")[鉑製作](../Page/鉑.md "wikilink")，但由於會受[鹵素侵蝕](../Page/鹵素.md "wikilink")，所以在會產生鹵素(例如:電解[食鹽鹽水](../Page/食鹽.md "wikilink"))的電解時使用石墨替代鉑，作為惰性電極。

## 存在形式

碳的存在形式是多种多样的，有晶态单质碳如[金刚石](../Page/金刚石.md "wikilink")、石墨；有无定形碳如[煤](../Page/煤.md "wikilink")；有复杂的有机化合物如动植物等；[碳酸盐如](../Page/碳酸盐.md "wikilink")[大理石等](../Page/大理石.md "wikilink")。单质碳的物理和化学性质取决于它的晶体结构。高硬度的金刚石和柔软滑腻的石墨晶体结构不同，各有各的外观、密度、熔点等。

## 參看

  - [碳纖維](../Page/碳纖維.md "wikilink")
  - [-{zh:奈米碳;zh-cn:碳纳米;zh-hans:奈米碳;zh-tw:奈米碳}-管](../Page/奈米碳管.md "wikilink")
  - [鑽石](../Page/鑽石.md "wikilink")
  - [富勒烯](../Page/富勒烯.md "wikilink")
  - [石墨烯](../Page/石墨烯.md "wikilink")
  - [石墨层间化合物](../Page/石墨层间化合物.md "wikilink")
  - [藍絲黛爾石](../Page/藍絲黛爾石.md "wikilink")

## 參考

## 外部連結

  - [石墨 - Webmineral](http://www.webmineral.com/data/Graphite.shtml)

  - [石墨 - Mindat](http://www.mindat.org/show.php?id=1740&ld=1&pho=)

  - [電池級石墨](http://www.northerngraphite.com/wp-content/uploads/2010/08/Concern-over-battery-grade-graphite-supplies.pdf)

  - [Graphite at
    Minerals.net](http://www.mineral.net/mineral/graphite.aspx)

  - [Mineral
    galleries](https://web.archive.org/web/20050522074754/http://mineral.galleries.com/minerals/elements/graphite/graphite.htm)

  - [Mineral &
    Exploration](http://www.mineral-exploration.de/maps/worldgraphitemapen.gif)
    – 2012年石墨礦和生產者的世界地圖

  - [Mindat w/
    locations](http://www.mindat.org/show.php?id=1740&ld=1&pho=)

  - [giant covalent
    structures](http://www.chemguide.co.uk/atoms/structures/giantcov.html)

  - [The Graphite Page](http://www.phy.mtu.edu/~jaszczak/graphite.html)

  - [Video lecture on the properties of graphite by Prof. M.
    Heggie](http://www.vega.org.uk/video/programme/316), [University of
    Sussex](../Page/University_of_Sussex.md "wikilink")

  - [CDC – NIOSH Pocket Guide to Chemical
    Hazards](http://www.cdc.gov/niosh/npg/npgd0306.html)

[Category:碳的同素异形体](../Category/碳的同素异形体.md "wikilink")
[Category:元素礦物](../Category/元素礦物.md "wikilink")
[Category:电导体](../Category/电导体.md "wikilink")
[Category:耐火材料](../Category/耐火材料.md "wikilink")
[Category:非石油基质润滑剂](../Category/非石油基质润滑剂.md "wikilink")
[Category:美術材料](../Category/美術材料.md "wikilink")
[Category:六方晶系矿物](../Category/六方晶系矿物.md "wikilink")

1.
2.  Hugh O. Pierson, *Handbook of carbon, graphite, diamond, and
    fullerenes: properties, processing, and applications*, p. 61,
    William Andrew, 1993 ISBN 0-8155-1339-9.