[Airbus_A319-132,_Bangkok_Airways_JP7072266.jpg](https://zh.wikipedia.org/wiki/File:Airbus_A319-132,_Bangkok_Airways_JP7072266.jpg "fig:Airbus_A319-132,_Bangkok_Airways_JP7072266.jpg")\]\]

**曼谷航空**（；）是[泰國的全服務航空公司](../Page/泰國.md "wikilink")，總部設在該國首都[曼谷](../Page/曼谷.md "wikilink")，以[素万那普机场作為基地及轉運中心](../Page/素万那普机场.md "wikilink")，[蘇梅國際機場是它的重點機場](../Page/蘇梅國際機場.md "wikilink")。該公司營運國內及國際航線，往來泰國以及[柬埔寨](../Page/柬埔寨.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[印度](../Page/印度.md "wikilink")、[香港](../Page/香港.md "wikilink")、[老挝](../Page/老挝.md "wikilink")、[馬爾地夫等地之間](../Page/馬爾地夫.md "wikilink")，也利用共用班號擴大飛航版圖。[暹粒國際航空是曼谷航空的全資子公司](../Page/暹粒航空公司.md "wikilink")\[1\]。曼谷航空的收入來源包括客運、貨運、郵件、機場經營及代理其他航空公司在[蘇梅機場和](../Page/蘇梅國際機場.md "wikilink")[素万那普机场內之地勤業務](../Page/素万那普机场.md "wikilink")、機上免稅商品販售及提供旅客飯店機場間交通運送服務等。

曼谷航空公司飛行的航線以[曼谷及](../Page/曼谷.md "wikilink")[蘇梅島為中心點作放射狀遍及全球與泰國境內各主要城市](../Page/蘇梅島.md "wikilink")，包含泰國國內線、區域航線及即將開航的洲際航線等。曼谷航空為加速其營運成長並積極開拓其飛行版圖，今日曼谷航空之航線已遍及全世界8個國家、24個主要城市。

目前曼谷航空採用ATR－72客機飛行國內航線，A320則是東南亞旅遊航點，在商務客較多的香港、新加坡、吉隆坡、仰光則是採用擁有「藍帶商務艙」的A319客機提供服務。

## 歷史

曼谷航空的前身--Sahakol
Air於1968年成立時，是提供空中的士的服務。1986年，它更換高階管理人員並更名為今日的名稱—曼谷航空，曼谷航空也同時開始經營定期航班服務，是泰國首家經營定期國內航線的非國家航空公司。曼谷航空在1989年於蘇梅島建造了一座屬於自己的[機場](../Page/蘇梅機場.md "wikilink")，該機場於成立不久後擴建，提供前往[香港](../Page/香港.md "wikilink")、[新加坡和](../Page/新加坡.md "wikilink")[普吉的航班](../Page/普吉.md "wikilink")。1996年曼谷航空在[素可泰府的機場正式啟用](../Page/素可泰府.md "wikilink")，在[桐艾府的機場亦於](../Page/桐艾府.md "wikilink")2003年3月啟用，主要為前往[象島的旅客服務](../Page/象岛_\(泰国\).md "wikilink")。

2000年曼谷航空也引入噴射客機[波音717](../Page/波音717.md "wikilink")，以擴充其業務，曼谷航空未引入波音717之前，其機隊是由[ATR-72組成](../Page/ATR-72.md "wikilink")，早期亦有[Dash
8和](../Page/Dash_8.md "wikilink")[肖特330](../Page/肖特330.md "wikilink")，亦曾短期營運噴射客機[福克F100](../Page/福克F100.md "wikilink")。在2003年曼谷航空決定引入航程更遠的[A320](../Page/A320.md "wikilink")，2004年接受首架[A320](../Page/A320.md "wikilink")，以取代即將到期歸還的波音717\[2\]。

曼谷航空計劃引入雙走道廣體客機以擴充其長程機隊和業務，欲在2006年引入[A330](../Page/A330.md "wikilink")、[A340或](../Page/A340.md "wikilink")[B787開辦長途航線](../Page/B787.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")、[印度和](../Page/印度.md "wikilink")[日本](../Page/日本.md "wikilink")。在2005年12月，曼谷航空宣佈訂購4架258座位的[A350](../Page/A350.md "wikilink")-800，再加兩架選擇權，原定在2013年交付\[3\]，但因空中巴士需延誤交機，曼谷航空亦對外宣布延後交機時間，訂單最我終訴2011年取消。未來曼谷航空新購置的飛機將以大中小型客機平均訂購，也透過與不同洲際航空公司合作，以代碼共享方式快速拓展曼谷及蘇梅兩大基地的對外航線。

在2006年[Skytrax评估中曼谷航空被評為東南亞最佳地區性航空公司](../Page/Skytrax.md "wikilink")，2007年的评估中被評為四星級航空公司、東南亞最佳地區性航空公司和亞洲最佳地區性航空公司。

## 航點

以下是曼谷航空截至2016年10月的航點[1](http://www.bangkokair.com/pages/view/route-map)：

  - [泰国](../Page/泰国.md "wikilink")

<!-- end list -->

  - [曼谷](../Page/曼谷.md "wikilink") -
    [蘇凡納布機場](../Page/素万那普机场.md "wikilink") -
    **樞紐**

  - [清邁](../Page/清邁.md "wikilink") -
    [清邁國際機場](../Page/清邁國際機場.md "wikilink")

  - [清萊](../Page/清萊.md "wikilink") -
    [清萊國際機場](../Page/清萊國際機場.md "wikilink")

  - [蘇梅](../Page/苏梅岛.md "wikilink") - [蘇梅機場](../Page/蘇梅機場.md "wikilink")
    - **重點城市**

  - [甲米](../Page/甲米.md "wikilink") - [甲米機場](../Page/甲米機場.md "wikilink")

  - \-

  - [芭堤雅](../Page/芭達亞.md "wikilink") -
    [烏達堡國際機場](../Page/乌塔保国际机场.md "wikilink")

  - [布吉](../Page/普吉府.md "wikilink") -
    [布吉國際機場](../Page/布吉國際機場.md "wikilink")

  - [泰可素](../Page/素可泰府.md "wikilink") -

  - [桐艾](../Page/桐艾府.md "wikilink") - [桐艾機場](../Page/桐艾機場.md "wikilink")

  - [烏隆他尼](../Page/烏隆他尼.md "wikilink") -
    [烏隆他尼國際機場](../Page/乌隆他尼国际机场.md "wikilink")

<!-- end list -->

  - [香港](../Page/香港.md "wikilink")

<!-- end list -->

  - [香港國際機場](../Page/香港國際機場.md "wikilink")

<!-- end list -->

  - [新加坡](../Page/新加坡.md "wikilink")

<!-- end list -->

  - [樟宜機場](../Page/樟宜機場.md "wikilink")

<!-- end list -->

  - [馬來西亞](../Page/馬來西亞.md "wikilink")

<!-- end list -->

  - [吉隆坡](../Page/吉隆坡.md "wikilink") -
    [吉隆坡國際機場](../Page/吉隆坡國際機場.md "wikilink")

<!-- end list -->

  - [缅甸](../Page/缅甸.md "wikilink")

<!-- end list -->

  - [內比都](../Page/內比都.md "wikilink") -
    [內比都国际机场](../Page/內比都国际机场.md "wikilink")
  - [仰光](../Page/仰光.md "wikilink") -
    [仰光国际机场](../Page/仰光国际机场.md "wikilink")
  - [曼德勒](../Page/曼德勒.md "wikilink") -
    [曼德勒国际机场](../Page/曼德勒国际机场.md "wikilink")

<!-- end list -->

  - [柬埔寨](../Page/柬埔寨.md "wikilink")

<!-- end list -->

  - [金边](../Page/金边.md "wikilink") -
    [金邊國際機場](../Page/金邊國際機場.md "wikilink")
  - [暹粒市](../Page/暹粒市.md "wikilink") -
    [吳哥國際機場](../Page/暹粒－吴哥国际机场.md "wikilink")

<!-- end list -->

  - [老挝](../Page/老挝.md "wikilink")

<!-- end list -->

  - [琅勃拉邦](../Page/琅勃拉邦.md "wikilink") -
    [琅勃拉邦國際機場](../Page/琅勃拉邦國際機場.md "wikilink")
  - [永珍](../Page/永珍.md "wikilink") -
    [瓦岱国际机场](../Page/瓦岱国际机场.md "wikilink")

<!-- end list -->

  - [马尔代夫](../Page/马尔代夫.md "wikilink")

<!-- end list -->

  - [馬累](../Page/馬累.md "wikilink") -
    [馬累國際機場](../Page/馬累國際機場.md "wikilink")

<!-- end list -->

  - [印度](../Page/印度.md "wikilink")

<!-- end list -->

  - [孟买](../Page/孟买.md "wikilink") -
    [贾特拉帕蒂·希瓦吉国际机场](../Page/贾特拉帕蒂·希瓦吉国际机场.md "wikilink")

<!-- end list -->

  - [孟加拉国](../Page/孟加拉国.md "wikilink")

<!-- end list -->

  - [达卡](../Page/达卡.md "wikilink") -
    [沙阿贾拉勒国际机场](../Page/沙阿贾拉勒国际机场.md "wikilink")

### 代碼共享

曼谷航空與以下航空公司簽訂代碼共享協議：

  - [國泰航空](../Page/國泰航空.md "wikilink")
  - [中華航空](../Page/中華航空.md "wikilink")
  - [以色列航空](../Page/以色列航空.md "wikilink") \[4\]</ref>
  - [阿聯酋航空](../Page/阿聯酋航空.md "wikilink")
  - [阿提哈德航空](../Page/阿提哈德航空.md "wikilink")
  - [長榮航空](../Page/長榮航空.md "wikilink")
  - [嘉魯達印尼航空](../Page/嘉魯達印尼航空.md "wikilink")
  - [日本航空](../Page/日本航空.md "wikilink")
  - [捷特航空](../Page/捷特航空.md "wikilink")
  - [馬來西亞航空](../Page/馬來西亞航空.md "wikilink")
  - [澳洲航空](../Page/澳洲航空.md "wikilink")
  - [卡塔爾航空](../Page/卡塔爾航空.md "wikilink")
  - [勝安航空](../Page/勝安航空.md "wikilink")
  - [泰國國際航空](../Page/泰國國際航空.md "wikilink")
  - [廈門航空](../Page/廈門航空.md "wikilink") \[5\]

## 機隊

[Bangkok_Airways.JPG](https://zh.wikipedia.org/wiki/File:Bangkok_Airways.JPG "fig:Bangkok_Airways.JPG")\]\]
[Bangkok_Airways_ATR72.jpg](https://zh.wikipedia.org/wiki/File:Bangkok_Airways_ATR72.jpg "fig:Bangkok_Airways_ATR72.jpg")\]\]
截至2017年7月，曼谷航空的機隊如下：

<center>

<table>
<caption><strong>曼谷航空機隊</strong></caption>
<thead>
<tr class="header">
<th><p>機型</p></th>
<th><p>數量</p></th>
<th><p>載客量<br />
<small>（藍絲帶／經濟）</small></p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/空中客车A320.md" title="wikilink">空中巴士A319-132</a></p></td>
<td><p>14</p></td>
<td><p>120（12／108）<br />
144（0／144）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>空中巴士A320-232</p></td>
<td><p>9</p></td>
<td><p>162（全經濟）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/ATR_72.md" title="wikilink">ATR 72-500</a></p></td>
<td><p>6</p></td>
<td><p>70（全經濟）</p></td>
<td><p>一架由<a href="../Page/暹粒國際航空.md" title="wikilink">暹粒國際航空營運</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ATR_72.md" title="wikilink">ATR 72-600</a></p></td>
<td><p>9</p></td>
<td><p>70（全經濟）</p></td>
<td></td>
</tr>
</tbody>
</table>

</center>

## 意外事件

1990年11月21日，一架曼谷航空的[Dash
8型客機在降落](../Page/Dash_8.md "wikilink")[蘇梅島時因惡劣天氣失控墜毀](../Page/蘇梅島.md "wikilink")，機上38人全部死亡。\[6\]。

2009年8月4日，一架曼谷航空ATR72-500型客機在[蘇梅島機場着陸時滑出跑道並起火](../Page/蘇梅島.md "wikilink")，造成至少1人死亡、數十人受傷。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [曼谷航空官方網站](http://www.bangkokair.com/)
  - [曼谷航空网志](http://www.bangkokairblog.com/)

[category:泰國航空公司](../Page/category:泰國航空公司.md "wikilink")
[分類:1968年成立的航空公司](../Page/分類:1968年成立的航空公司.md "wikilink")

1.

2.  [Bangkok Airways takes first Airbus-14/09/2004-Flight
    International](http://www.flightglobal.com/articles/2004/09/14/187363/bangkok-airways-takes-first-airbus.html)

3.  \[<http://www.eads.com/1024/en/investor/News_and_Events/news_ir/2005/2005/20051230_bangkok_a350.html>|
    Bangkok Airways selects A350 for new long range services\]30
    December 2005

4.
5.

6.  [Koh Samui crash](http://www.planecrashinfo.com/1990/1990-67.htm)