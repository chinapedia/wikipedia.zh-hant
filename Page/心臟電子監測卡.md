**心臟電子監測卡**，簡稱**安心卡**或**護心卡**，是一種可隨身攜帶自行檢查的簡便[心電圖檢查裝置](../Page/心電圖.md "wikilink")。[心血管疾病的潛在發病者可使用此裝置隨時或於突發狀況時檢查後與提供服務的](../Page/心血管疾病.md "wikilink")[醫院連線上傳心電圖](../Page/醫院.md "wikilink")，經由[醫師評估即時監控身體狀況](../Page/醫師.md "wikilink")，以便及時診治。

## 功能

心臟電子監測卡是一種大小如[隨身聽但較為輕薄的電子檢測卡](../Page/隨身聽.md "wikilink")，內含短時間心電圖記錄功能，並能類似傳統[數據機以電話系統傳輸心電圖至遠距醫療服務醫院](../Page/數據機.md "wikilink")，使醫師及時判讀及時就醫或減少突發狀況和非必要就醫情形的發生。

與一般[心電圖比較](../Page/心電圖.md "wikilink")，心臟電子監測卡雖然不像在醫院由專業醫療人員及使用完整12導程心電圖儀器檢查要來得詳盡，但[心臟發生輕微異常時](../Page/心臟.md "wikilink")，往往到醫院作詳細檢查時已恢復正常而查不出病因。若以[24小時心電圖檢查比較](../Page/动态心电图.md "wikilink")，因操作此項檢查，身上必須裝置電極線路以及不能洗澡較不舒服，且不一定能在一天的時間內監測到異常較為可惜。而又以[運動心電圖來說](../Page/運動心電圖.md "wikilink")，對行動不便者無法透過步行運動昇高血壓來檢查心臟異常的情形。無疑的，小型隨身的檢查設備雖然功能有限但卻是比較切合實用。

唯判讀對一般人來說有困難，更無法診治自理，故必須要設立有[遠距醫療服務中心由](../Page/遠距醫療.md "wikilink")[心臟專科醫師親自判讀並指導就醫診治事宜並配合心臟專科](../Page/心臟科.md "wikilink")[門診或](../Page/門診.md "wikilink")[緊急醫療網系統後援方能發揮功效](../Page/緊急醫療網.md "wikilink")。

## 適用者

  - 有[暈眩或](../Page/暈眩.md "wikilink")[昏厥病史者](../Page/昏厥.md "wikilink")
  - [心悸不舒服](../Page/心悸.md "wikilink")、[心律不整或疑似心律不整患者](../Page/心律不整.md "wikilink")
  - [心臟手術](../Page/心臟手術.md "wikilink")、[心導管氣球擴張和裝置支架患者](../Page/心導管氣球擴張.md "wikilink")
  - 裝有[心律調節器患者](../Page/心律調節器.md "wikilink")
  - 高壓力狀態者（如：[大眾運輸系統](../Page/大眾運輸系統.md "wikilink")[駕駛](../Page/駕駛.md "wikilink")、[飛行員等](../Page/飛行員.md "wikilink")）
  - 高體能[運動員](../Page/運動員.md "wikilink")（如：[登山者](../Page/登山.md "wikilink")、[田徑選手](../Page/田徑.md "wikilink")、[礦工等](../Page/礦工.md "wikilink")）
  - [孕婦或須](../Page/孕婦.md "wikilink")24小時專人照顧者

## 種類

  - 單一導程：直接於胸口檢查，使用容易，適用一般人。
  - 十二導程：具有完整心電圖12導程檢查功能，須加貼[電極貼片](../Page/電極.md "wikilink")，適用病情複雜患者。

## 使用方法

1.  記錄：將卡片背面的金屬板直接貼在[心臟上方的左](../Page/心臟.md "wikilink")[胸記錄心電圖](../Page/胸.md "wikilink")。
2.  打電話：使用[室內電話或](../Page/室內電話.md "wikilink")[行動電話打電話至服務醫院專線電話](../Page/行動電話.md "wikilink")。
3.  傳訊：依服務人員指示，將卡片小圓孔對準話筒收音孔傳輸心電圖資訊\[1\]。

## 優點

  - 隨時監控及自我[疾病管理](../Page/疾病管理.md "wikilink")。
  - 避免不必要的奔波醫院。
  - 縮短[緊急救護時間](../Page/緊急救護.md "wikilink")。
  - 建立完整個人電子[病歷](../Page/病歷.md "wikilink")。

## 缺點

  - 費用昂貴，[臺灣的月租費為全日](../Page/臺灣.md "wikilink")[新台幣](../Page/新台幣.md "wikilink")7,000元，半日（上午8點至晚上9點）則為新台幣4,000元\[2\]。

## 實際應用

目前在[臺灣的全日使用者不到百名](../Page/臺灣.md "wikilink")，半日使用者不到600人，其中包括在[中國大陸工作的](../Page/中國大陸.md "wikilink")[台商以及在](../Page/台商.md "wikilink")[美國參加活動的](../Page/美國.md "wikilink")[重型機車](../Page/重型機車.md "wikilink")[騎士](../Page/騎士.md "wikilink")。實際上的遠距服務有近3成發生在晚上9點至清晨8點之間，傳輸資料中每3筆有1筆為異常並能及時獲得協助\[3\]。

## 參考資料

<div class="references-small">

<references />

</div>

## 相關條目

  - [遠距醫療照護](../Page/遠距醫療照護.md "wikilink")
  - [心血管疾病](../Page/心血管疾病.md "wikilink")
      - [心肌梗塞](../Page/心肌梗死.md "wikilink")
      - [心律不整](../Page/心律不整.md "wikilink")
  - 心血管疾病檢查
      - [生理檢查](../Page/生理學.md "wikilink")
          - [心電圖](../Page/心電圖.md "wikilink")
          - [24小時心電圖](../Page/动态心电图.md "wikilink")（Holter monitor）
          - [運動心電圖](../Page/運動心電圖.md "wikilink")
          - [心臟超音波檢查](../Page/心臟超音波檢查.md "wikilink")
      - [放射線檢查](../Page/放射線學.md "wikilink")
          - [心導管檢查](../Page/心導管檢查.md "wikilink")
      - [生化檢查](../Page/生化學.md "wikilink")（驗血）
          - [肌鈣蛋白](../Page/肌鈣蛋白.md "wikilink")（Troponin-I）
          - [肌酸肌酶](../Page/肌酸肌酶.md "wikilink")（CK及CK-MB）
          - [天門冬胺酸轉胺酶](../Page/天冬氨酸氨基转移酶.md "wikilink")（AST或GOT）
          - [乳酸去氫酶](../Page/乳酸脱氢酶.md "wikilink")（LDH）
          - [C反應蛋白](../Page/C反應蛋白.md "wikilink")（CRP）

## 外部連結

  - [遠距心電圖安心照護中心](https://web.archive.org/web/20070225072310/http://depart.femh.org.tw/cv/)
    - [亞東醫院](../Page/亞東醫院.md "wikilink")[1](http://www.femh.org.tw/)
  - [遠距心臟照護中心](https://web.archive.org/web/20070302091504/http://www.chgh.org.tw/icare/default.asp)
    - [振興醫院](../Page/振興醫院.md "wikilink")[2](http://www.chgh.org.tw/)
  - [Personal ECG
    Monitor](https://web.archive.org/web/20070731142236/http://www.cardguard.com/newsite/inner.asp?cat=104&type=2&lang=1)
    - [Card
    Guard](https://web.archive.org/web/20070712232635/http://www.cardguard.com/)

[Category:醫療設備](../Category/醫療設備.md "wikilink")
[Category:心臟病](../Category/心臟病.md "wikilink")

1.  網頁：遠距心臟照護中心，《[護心卡說明](http://www.chgh.org.tw/icare/guide.asp)
    》，[振興醫院](../Page/振興醫院.md "wikilink")，《[遠距心臟照護中心](http://www.chgh.org.tw/icare/default.asp)
    》，2006.11
2.  新聞：魏怡嘉，《[護心卡遠距監測
    病患免奔波](http://www.libertytimes.com.tw/2007/new/feb/2/today-life11.htm)
    》，[自由時報](../Page/自由時報.md "wikilink")，生活版，2007.2.2
3.  新聞：黃庭郁，《[護心卡 掌握心情
    救命一瞬間](http://tech.chinatimes.com/2007Cti/2007Cti-News/Inc/2007cti-news-Tech-inc/Tech-Content/0,4703,171704+112007030300352,00.html)》，[中國時報](../Page/中國時報.md "wikilink")，網路新知版，2007.3.3