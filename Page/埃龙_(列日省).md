[Héron_JPG00.jpg](https://zh.wikipedia.org/wiki/File:Héron_JPG00.jpg "fig:Héron_JPG00.jpg")

**埃龙**（）是位於[比利时](../Page/比利时.md "wikilink")[列日省](../Page/列日省.md "wikilink")[默兹河](../Page/默兹河.md "wikilink")[河谷的一座](../Page/河谷.md "wikilink")[城市](../Page/城市.md "wikilink")，[人口](../Page/人口.md "wikilink")4,534人（2006年）。

## 地理

埃龙位于河上。

该市由以下次级市镇组成：Héron proper、Lavoir、、.

## 人口

截至2006年1月1日，埃龙的总人口为4,534人。总面积38.32平方公里，人口密度为每平方公里118人。

## 公共设施

位於埃龍。

## 畫廊

<File:L'église> Saint-Hubert de Lavoir.JPG|聖胡貝圖斯教堂 （**）。 <File:Envoz>
(Chateau de Potesta).jpg|位於Envoz的Potesta城堡。 <File:00> Héron 051017
(1).jpg|17世紀的舊農場。

## 外部链接

  -
  -
## 参考文献

[Category:列日省市镇](../Category/列日省市镇.md "wikilink")