[China_Steel_Corporation_Headquarters_view_from_Chenggong_2nd_Road_and_Singuang_Road_intersection_20160706.jpg](https://zh.wikipedia.org/wiki/File:China_Steel_Corporation_Headquarters_view_from_Chenggong_2nd_Road_and_Singuang_Road_intersection_20160706.jpg "fig:China_Steel_Corporation_Headquarters_view_from_Chenggong_2nd_Road_and_Singuang_Road_intersection_20160706.jpg")
**成功路（Cheng-gong
Rd.）**為連結[高雄市](../Page/高雄市.md "wikilink")[前金區](../Page/前金區.md "wikilink")、[苓雅區](../Page/苓雅區.md "wikilink")、[前鎮區的南北向重要道路](../Page/前鎮區.md "wikilink")，北起[河南二路口接中庸橋往中庸街](../Page/河南路_\(高雄市\).md "wikilink")，南至[凱旋四路和](../Page/凱旋路_\(高雄市\).md "wikilink")[擴建路口](../Page/擴建路.md "wikilink")。成功路是往來高雄市區以及[高雄加工出口區](../Page/高雄加工出口區.md "wikilink")、[前鎮漁港](../Page/前鎮漁港.md "wikilink")、[高雄港區的主要道路](../Page/高雄港.md "wikilink")，知名的[漢神百貨也在成功路上](../Page/漢神百貨.md "wikilink")。

## 行經行政區域

（由北至南）

  - [前金區](../Page/前金區.md "wikilink")
  - [苓雅區](../Page/苓雅區.md "wikilink")
  - [前鎮區](../Page/前鎮區.md "wikilink")

## 分段

成功路共分為二個部份：

  - 成功一路；北起[河南二路口接中庸橋往中庸街](../Page/河南路_\(高雄市\).md "wikilink")，南於[三多四路和](../Page/三多路_\(高雄市\).md "wikilink")[海邊路口接成功二路](../Page/海邊路_\(高雄市\).md "wikilink")。
  - 成功二路：北於三多四路、海邊路口接成功一路，南止於[凱旋四路和](../Page/凱旋路_\(高雄市\).md "wikilink")[擴建路口](../Page/擴建路.md "wikilink")。

## 歷史

## 沿線設施

（由北至南）

  - 成功一路
      - [高雄市立前金國民中學](../Page/高雄市立前金國民中學.md "wikilink")
      - 東南文化大樓
          - [行政院南部聯合服務中心](../Page/行政院南部聯合服務中心.md "wikilink")
      - [上海商業儲蓄銀行前金分行](../Page/上海商業儲蓄銀行.md "wikilink")
      - 高雄旅遊中心
      - [聯合報系高雄分社](../Page/聯合報系.md "wikilink")
      - [高雄市前金區前金國民小學](../Page/高雄市前金區前金國民小學.md "wikilink")
      - [前金萬興宮](../Page/前金萬興宮.md "wikilink")
      - [台灣中油成功一路站](../Page/台灣中油.md "wikilink")
      - [高雄市立高雄女子高級中學](../Page/高雄市立高雄女子高級中學.md "wikilink")
      - [台灣銀行成功分行](../Page/台灣銀行.md "wikilink")
      - [漢神百貨](../Page/漢神百貨.md "wikilink")
      - [星巴克青年門市](../Page/星巴克.md "wikilink")
      - [阮綜合醫院](../Page/阮綜合醫院.md "wikilink")
      - [高雄市第三信用合作社苓雅分社](../Page/高雄市第三信用合作社.md "wikilink")
      - 苓雅郵局
      - 邱綜合醫院
      - [高雄市苓雅區成功國民小學](../Page/高雄市苓雅區成功國民小學.md "wikilink")

<!-- end list -->

  - 成功二路
      - [海洋委員會](../Page/海洋委員會.md "wikilink")
      - [台灣銀行三多分行](../Page/台灣銀行.md "wikilink")
      - [高雄展覽館](../Page/高雄展覽館.md "wikilink")
      - 海洋之星（[新光碼頭](../Page/新光碼頭.md "wikilink")）
      - [高雄展覽館站](../Page/新光路車站.md "wikilink")（[高雄捷運環狀輕軌](../Page/高雄捷運環狀輕軌.md "wikilink")）
      - [高雄軟體科技園區](../Page/高雄軟體科技園區.md "wikilink")
      - [軟體園區站](../Page/軟體園區車站.md "wikilink")（[高雄捷運環狀輕軌](../Page/高雄捷運環狀輕軌.md "wikilink")）
      - [正勤國宅](../Page/正勤國宅.md "wikilink")
      - [經貿園區站](../Page/正勤國宅車站.md "wikilink")（[高雄捷運環狀輕軌](../Page/高雄捷運環狀輕軌.md "wikilink")）
      - 成功橋
      - [中鋼集團總部大樓](../Page/中鋼集團總部大樓.md "wikilink")
      - [台糖高雄物流中心](../Page/台糖.md "wikilink")
      - [統一夢時代購物中心](../Page/統一夢時代購物中心.md "wikilink")
      - [夢時代站](../Page/夢時代車站.md "wikilink")（[高雄捷運環狀輕軌](../Page/高雄捷運環狀輕軌.md "wikilink")）
      - [高雄市公共腳踏車租賃系統](../Page/高雄市公共腳踏車租賃系統.md "wikilink")
          - 新光成功站：[前鎮區成功二路距](../Page/前鎮區.md "wikilink")[新光路](../Page/新光路_\(高雄市\).md "wikilink")18米處
          - 夢時代站：前鎮區成功二路與[時代大道口東北側](../Page/時代大道.md "wikilink")

## 公車資訊

  - [漢程客運](../Page/漢程客運.md "wikilink")
      - 環狀168東 [金獅湖站](../Page/金獅湖站.md "wikilink")－金獅湖站
      - 環狀168西 金獅湖站－金獅湖站
      - 0南 金獅湖站－金獅湖站
      - 0北 金獅湖站－金獅湖站
      - 76 金獅湖站－[歷史博物館](../Page/歷史博物館.md "wikilink")
      - 77 金獅湖站－歷史博物館
  - [港都客運](../Page/港都客運.md "wikilink")
      - 15
        [小港站](../Page/小港站_\(公車\).md "wikilink")－[前鎮站](../Page/前鎮站.md "wikilink")
      - 36 復興幹線 前鎮站－[高雄火車站](../Page/高雄火車站.md "wikilink")
      - 70 三多幹線 前鎮站－長庚醫院
      - 205 中華幹線
        [加昌站](../Page/加昌站_\(公車\).md "wikilink")－高雄火車站－[夢時代](../Page/夢時代.md "wikilink")
      - 214 小港站－[歷史博物館](../Page/歷史博物館.md "wikilink")
  - [統聯客運](../Page/統聯客運.md "wikilink")（市區公車）
      - 25
        [瑞豐站](../Page/瑞豐站.md "wikilink")－[歷史博物館](../Page/歷史博物館.md "wikilink")
      - 83 瑞豐站－[高雄火車站](../Page/高雄火車站.md "wikilink")
      - 100 瑞豐站－高雄火車站
  - [東南客運](../Page/東南客運.md "wikilink")
      - 紅16 [高雄軟科園區](../Page/高雄軟體科技園區.md "wikilink")－
        捷運[獅甲站](../Page/獅甲站.md "wikilink")－
        捷運[三多商圈站](../Page/三多商圈站.md "wikilink")
  - [高雄客運](../Page/高雄客運.md "wikilink")
      - 8039 鳳山－五甲－高雄－岡山－茄萣

## 內部連結

  - [高雄市主要道路列表](../Page/高雄市主要道路列表.md "wikilink")

[Category:高雄市街道](../Category/高雄市街道.md "wikilink")
[高](../Category/冠以人名的台灣道路.md "wikilink")