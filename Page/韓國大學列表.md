本列表包括[韓國各个](../Page/韓國.md "wikilink")[大學的信息](../Page/大學.md "wikilink")。需要注意的是，韓國的“大學校”（）是指[現代漢語所指的](../Page/現代漢語.md "wikilink")[綜合大學](../Page/綜合大學.md "wikilink")，而“大學”（）是指独立的[學院](../Page/學院_\(學校\).md "wikilink")、、[專科學校](../Page/專科學校.md "wikilink")、[技術學院或](../Page/技術學院.md "wikilink")[科技大學](../Page/科技大學_\(學制\).md "wikilink")。總數約400多家大學校院。

## 国立旗舰大学

  - [首爾國立大學](../Page/首爾國立大學.md "wikilink")，成立于1946年，位于[首爾特别市](../Page/首爾特别市.md "wikilink")
  - [釜山國立大學](../Page/釜山大學.md "wikilink")，成立于1946年，位于[釜山廣域市](../Page/釜山廣域市.md "wikilink")
  - [慶北國立大學](../Page/慶北大學.md "wikilink")，成立于1946年，位于[大邱廣域市](../Page/大邱廣域市.md "wikilink")
  - [江原國立大學](../Page/江原大學.md "wikilink")，成立于1947年，位于[江原道](../Page/江原道.md "wikilink")
  - [全北國立大學](../Page/全北大學.md "wikilink")，成立于1947年，位于[全羅北道](../Page/全羅北道.md "wikilink")
  - [忠北國立大學](../Page/忠北大學.md "wikilink")，成立于1951年，位于[忠清北道](../Page/忠清北道.md "wikilink")
  - [全南國立大學](../Page/全南大學.md "wikilink")，成立于1952年，位于[光州廣域市](../Page/光州廣域市.md "wikilink")
  - [忠南國立大學](../Page/忠南大學.md "wikilink")，成立于1952年，位于[大田廣域市](../Page/大田廣域市.md "wikilink")
  - [濟州國立大學](../Page/濟州大學.md "wikilink")，成立于1952年，位于[濟州特別自治道](../Page/濟州特別自治道.md "wikilink")
  - [慶尚國立大學](../Page/慶尚大學.md "wikilink")，成立于1968年，位于[慶尚南道](../Page/慶尚南道.md "wikilink")

## 地区列表

### 首爾特别市

  - [首爾大學](../Page/首爾大學.md "wikilink")（） <http://www.snu.ac.kr/>

  - [延世大學](../Page/延世大學.md "wikilink")（） <http://www.yonsei.ac.kr/>

  - [高麗大學](../Page/高麗大學.md "wikilink")（） <http://www.korea.ac.kr/>

  - [漢陽大學](../Page/漢陽大學.md "wikilink")（） <http://www.hanyang.ac.kr/>

  - [西江大學](../Page/西江大學.md "wikilink")（）http://www.sogang.ac.kr/

  - [成均館大學](../Page/成均館大學.md "wikilink")（） <http://skku.ac.kr/>

  - [慶熙大學](../Page/慶熙大學.md "wikilink")（） <http://www.khu.ac.kr/>

  - [弘益大學](../Page/弘益大學.md "wikilink")（） <http://www.hongik.ac.kr/>

  - [中央大學](../Page/中央大學_\(韓國\).md "wikilink")（）http://www.cau.ac.kr/

  - [建國大學](../Page/建國大學.md "wikilink")（） <http://www.konkuk.ac.kr/>

  - [韓國外國語大學](../Page/韓國外國語大學.md "wikilink")（）http://www.hufs.ac.kr

  - [首爾市立大學](../Page/首爾市立大學.md "wikilink")（）http://www.uos.ac.kr/

  - [東國大學](../Page/東國大學.md "wikilink")（） <http://www.dongguk.ac.kr>

  - [淑明女子大學](../Page/淑明女子大學.md "wikilink")（）http://www.sookmyung.ac.kr/

  - [韩国天主教大學](../Page/韩国天主教大學.md "wikilink")（） <http://www.cuk.ac.kr/>

  - [梨花女子大學](../Page/梨花女子大學.md "wikilink")（）http://www.ewha.ac.kr/

  - [崇實大學](../Page/崇實大學.md "wikilink")（） <http://www.ssu.ac.kr/>

  - [國民大學](../Page/國民大學.md "wikilink")（） <http://www.kookmin.ac.kr/>

  - [明知大學](../Page/明知大學.md "wikilink")（） <http://www.mju.ac.kr/>

  - [光云大學](../Page/光云大學.md "wikilink")（） <http://www.kw.ac.kr/>

  - [世宗大學](../Page/世宗大學.md "wikilink")（） <http://www.sejong.ac.kr/>

  - （） <http://www.skuniv.ac.kr/main>

  - （）http://www.sungshin.ac.kr/

  - （）http://www.induk.ac.kr/

  - [白石藝術大學](../Page/白石藝術大學.md "wikilink")（백석예술대학교）
    <http://www.bau.ac.kr/>

  - [漢城大學](../Page/漢城大學.md "wikilink")（） <http://www.hansung.ac.kr/>

  - [祥明大學](../Page/祥明大學.md "wikilink")（）http://www.smu.ac.kr

  - [德成女子大學](../Page/德成女子大學.md "wikilink")（）http://www.duksung.ac.kr/

  - [首爾女子大學](../Page/首爾女子大學.md "wikilink")（） <http://www.swu.ac.kr/>

  - [同德女子大學](../Page/同德女子大學.md "wikilink")（）http://www.dongduk.ac.kr

  - [韓國藝術綜合大學](../Page/韓國藝術綜合大學.md "wikilink")（）http://www.karts.ac.kr/

  - [韓國放送通訊大學](../Page/韓國放送通訊大學.md "wikilink")（）http://www.knou.ac.kr/

  - [大韓民國陸軍官校](../Page/大韓民國陸軍士官學校.md "wikilink")（）http://www.kma.ac.kr/

  - [首爾國立科技大學](../Page/首爾國立科技大學.md "wikilink")

### 釜山廣域市

  - [釜山國立大學](../Page/釜山大學.md "wikilink")（부산대학교） <http://www.pnu.edu/>

  - [釜慶大學](../Page/釜慶大學.md "wikilink")（부경대학교）http://www.pknu.ac.kr/

  - [韓國海洋大學](../Page/韓國海洋大學.md "wikilink")（국립한국해양대학교）
    <http://www.kmaritime.ac.kr/>

  - [東亞大學](../Page/東亞大學.md "wikilink")（동아대학교） <http://www.donga.ac.kr/>

  - [釜山外國語大學](../Page/釜山外國語大學.md "wikilink")（부산외국어대학교）
    <https://www.bufs.ac.kr/>

  - [東西大學](../Page/東西大學.md "wikilink")（동서대학교）http://www.dongseo.ac.kr

  - [慶星大學](../Page/慶星大學.md "wikilink")（경성대학교）http://www.ks.ac.kr/

  - （동의대학교） <http://www.dongeui.ac.kr/>

  - （신라대학교） <http://www.silla.ac.kr/>

  - （동명대학교） <http://www.tu.ac.kr/>

  - [高神大學](../Page/高神大學.md "wikilink")（고신대학교）http://www.kosin.ac.kr/

### 大邱廣域市

  - [慶北國立大學](../Page/慶北大學.md "wikilink")（경북대학교） <http://www.knu.ac.kr/>
  - [啟明大學](../Page/啟明大學.md "wikilink")（계명대학교） <http://www.kmu.ac.kr/>
  - [大邱大學](../Page/大邱大學.md "wikilink")（대구대학교） <http://www.daegu.ac.kr/>
  - [大邱教育大學](../Page/大邱教育大學.md "wikilink")（대구교육대학교）
    <http://www.dnue.ac.kr/>
  - [大邱天主教大学](../Page/大邱天主教大学.md "wikilink")（대구가톨릭대학교）
    <http://www.cu.ac.kr/>

### 仁川廣域市

  - [仁荷大學](../Page/仁荷大學.md "wikilink")（인하대학교）
    <https://web.archive.org/web/20060410145254/http://www.inha.ac.kr/>
  - [仁川大學](../Page/仁川大學.md "wikilink")（인천대학교）
    <http://www.incheon.ac.kr//>
  - [京仁教育大學](../Page/京仁教育大學.md "wikilink")（경인교육대학교）http://www.ginue.ac.kr/

### 大田廣域市

  - [韓國科學技術院](../Page/韓國科學技術院.md "wikilink")（한국과학기술원）
    <http://www.kaist.edu>
  - [忠南國立大學](../Page/忠南大學.md "wikilink")（충남대학교） <http://www.cnu.ac.kr>
  - [大田國立大學](../Page/大田國立大學.md "wikilink")（한밭대학교）
    <http://www.hanbat.ac.kr/>
  - [培材大学](../Page/培材大学.md "wikilink")（배재대학교） <http://www.pcu.ac.kr/>
  - [韓南大學](../Page/韓南大學.md "wikilink")（한남대학교） <http://www.hannam.ac.kr/>

### 光州廣域市

  - [全南國立大學](../Page/全南国立大学.md "wikilink")（전남대학교）
    <http://www.chonnam.ac.kr/>
  - [光州教育大學](../Page/光州教育大學.md "wikilink")（광주교육대학교）
    <https://web.archive.org/web/20090218235504/http://kwangju-e.ac.kr/>
  - [光州科學技術院](../Page/光州科學技術院.md "wikilink")
  - [朝鮮大學](../Page/朝鮮大學.md "wikilink")（조선대학교） <http://www.chosun.ac.kr/>
  - [韓國湖南大學](../Page/韓國湖南大學.md "wikilink")（호남대학교）
    <http://www.honam.ac.kr/>

### 蔚山廣域市

  - [蔚山大學](../Page/蔚山大學.md "wikilink")（울산대학교） <http://www.ulsan.ac.kr/>
  - [蔚山科技大學](../Page/蔚山科技大學.md "wikilink")（울산과학기술대학교）
    <http://www.unist.ac.kr/>

### 京畿道

  - [亞洲大學](../Page/亞洲大學_\(韓國\).md "wikilink")（아주대학교）
    <http://www.ajou.ac.kr>

  - （한국항공대학교） <http://www.hau.ac.kr>

  - [檀國大學](../Page/檀國大學.md "wikilink")（단국대학교） <http://www.dankook.ac.kr>

  - （한세대학교）http://www.hansei.ac.kr

  - （안양대학교） <http://www.anyang.ac.kr>

  - [國立韓京大學](../Page/國立韓京大學.md "wikilink")（국립한경대학교）
    <http://www.hankyong.ac.kr>

  - [加爾文大學](../Page/加爾文大學.md "wikilink")（칼빈대학교）（另譯：Calvin大學）http://www.calvin.ac.kr

  - [京畿大學](../Page/京畿大學.md "wikilink")（경기대학교）https://web.archive.org/web/20071011010859/http://web.kyonggi.ac.kr/

  - [龍仁大學](../Page/龍仁大學.md "wikilink")（용인대학교）http://www.yongin.ac.kr

  - [嘉泉大學](../Page/嘉泉大學.md "wikilink")（가천대학교）http://www.gachon.ac.kr/main.jsp

  - [水原大學](../Page/水原大學.md "wikilink")（수원대학교）http://www.suwon.ac.kr

  - （대진대학교）http://www.daejin.ac.kr

  - [乙支大學](../Page/乙支大學.md "wikilink")（을지대학교）（前稱：乙支醫科大學）http://www.eu.ac.kr

  - （협성대학교）http://www.hyupsung.ac.kr

### 江原道

  - [江原國立大學](../Page/江原大學.md "wikilink")（강원대학교）http://www.kangwon.ac.kr
  - [翰林大學](../Page/翰林大學.md "wikilink")（한림대학교） <http://www.hallym.ac.kr/>
  - [江陵原州大學](../Page/江陵原州大學.md "wikilink")（강릉원주대학교）http://www.gwnu.ac.kr/
  - [尚志大學](../Page/尚志大學.md "wikilink")（상지대학교）http://www.sangji.ac.kr/

### 忠清北道

  - [忠北國立大學](../Page/忠北大學.md "wikilink")（충북대학교） <http://www.cbu.ac.kr/>

  - [清州大學](../Page/清州大學.md "wikilink")（청주대학교） <http://www.cju.ac.kr/>

  - （）http://www.afa.ac.kr

### 忠清南道

  - [白石大學](../Page/白石大學.md "wikilink")（백석대학교）
  - [韓國技術敎育大學](../Page/韓國技術敎育大學.md "wikilink")（한국기술교대）
    <http://www.kut.ac.kr/>
  - [順天鄉大學](../Page/順天鄉大學.md "wikilink")（순천향대학교） <http://www.sch.ac.kr/>
  - [鮮文大學](../Page/鮮文大學.md "wikilink")（선문대학교）
    <http://www.sunmoon.ac.kr/>
  - [湖西大學](../Page/湖西大學.md "wikilink")（호서대학교） <http://www.hoseo.ac.kr/>
  - [韓國中部大學](../Page/韓國中部大學.md "wikilink")（중부대학교）
    <http://www.joongbu.ac.kr/>
  - [公州大學](../Page/公州大學.md "wikilink")（공주대학교）http://www.kongju.ac.kr/
  - [公州教育大學](../Page/公州教育大學.md "wikilink")（공주교육대학교）http://www.kongju-e.ac.kr
  - [韓瑞大學](../Page/韓瑞大學.md "wikilink")（한서대학교） <http://www.hanseo.ac.kr/>
  - [建陽大學](../Page/建陽大學.md "wikilink")（건양대학교）
    <http://www.konyang.ac.kr/>

### 全羅北道

  - [全北國立大學](../Page/全北大學.md "wikilink")（전북대학교） <http://www.cbnu.edu/>
  - [国立群山大学](../Page/群山大学.md "wikilink")（군산대학교）https://www.kunsan.ac.kr
  - [圓光大學](../Page/圓光大學.md "wikilink")（원광대학교）http://www.wku.ac.kr/
  - [全州大學](../Page/全州大學.md "wikilink")（전주대학교） <http://www.jeonju.ac.kr>

### 全羅南道

  - [木浦海洋大學](../Page/木浦海洋大學.md "wikilink")（목포해양대학교）
    <http://www.mmu.ac.kr/>
  - [木浦國立大學](../Page/木浦國立大學.md "wikilink")（목포대학교）
    <http://www.mokpo.ac.kr/>
  - [大佛大學](../Page/大佛大學.md "wikilink")（대불대학교）
    <https://web.archive.org/web/20070717094944/http://www.daebul.ac.kr/>
  - [順天大學](../Page/順天大學.md "wikilink")（순천대학교）
    <http://www.sunchon.ac.kr/>
  - [東新大學](../Page/東新大學.md "wikilink")（동신대학교）
    <http://www.dongshinu.ac.kr/>
  - [草堂大學](../Page/草堂大學.md "wikilink")（초당대학교）
    <http://www.chodang.ac.kr/>

### 慶尚北道

  - [浦項工科大學](../Page/浦項工科大學.md "wikilink")（포항공과대학교）
    <http://www.postech.edu>
  - [嶺南大學](../Page/嶺南大學_\(韓國\).md "wikilink")（영남대학교）
    <http://www.yeungnam.ac.kr/>
  - [韓東大學](../Page/韓東國際大學.md "wikilink")（한동대학교）
    <http://www.handong.edu/>
  - [安東大學](../Page/安東大學.md "wikilink")（안동대학교）http://www.andong.ac.kr/
  - [慶州大學](../Page/慶州大學.md "wikilink")（경주대학교）
    <https://web.archive.org/web/20040923044241/http://www.kyongju.ac.kr/>
  - [慶一大學](../Page/慶一大學.md "wikilink")（경일대학교） <http://www.kiu.ac.kr/>
  - [尚州大學](../Page/尚州大學.md "wikilink")（상주대학교）
    <https://web.archive.org/web/20050923192033/http://sangju.ac.kr/>

### 慶尚南道

  - [慶尚國立大學](../Page/慶尚大學.md "wikilink")（경상대학교）http://www.gnu.ac.kr/

  - [仁濟大學](../Page/仁濟大學.md "wikilink")（인제대학교）http://www.inje.ac.kr

  - （）http://www.navy.ac.kr

  - [慶南大學](../Page/慶南大學.md "wikilink")（경남대학교）http://www.kyungnam.ac.kr/main/

  - [昌原大學校](../Page/昌原大學校.md "wikilink")（창원대학교）http://www.changwon.ac.kr/

### 濟州特別自治道

  - [濟州國立大學](../Page/濟州大學.md "wikilink") （제주대학교）
    <http://www.jejunu.ac.kr/>

## 大學的別稱

  - [東國大學](../Page/東國大學.md "wikilink")、[檀國大學](../Page/檀國大學.md "wikilink")、[建国大學并称为](../Page/建国大學.md "wikilink")「三国」大学。
  - [首爾大學](../Page/首爾大學.md "wikilink")（**S**eoul National
    University）、[高麗大學](../Page/高麗大學.md "wikilink")（**K**orea
    University）及[延世大學](../Page/延世大學.md "wikilink")（**Y**onsei
    University）合稱[SKY](../Page/SKY.md "wikilink")。

## 參見

  - [韓國教育](../Page/韓國教育.md "wikilink")
      - [韩国国立大学列表](../Page/韩国国立大学列表.md "wikilink")
  - [朝鮮大學列表](../Page/朝鮮大學列表.md "wikilink")

{{-}}

[\*](../Category/韩国大学.md "wikilink")
[\*](../Category/韓國各地大學.md "wikilink")
[Category:亞洲大學列表](../Category/亞洲大學列表.md "wikilink")
[Category:韓國相關列表](../Category/韓國相關列表.md "wikilink")
[Category:各国高校列表](../Category/各国高校列表.md "wikilink")