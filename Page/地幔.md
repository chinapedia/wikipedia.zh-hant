[Earth-crust-cutaway-chinese.png](https://zh.wikipedia.org/wiki/File:Earth-crust-cutaway-chinese.png "fig:Earth-crust-cutaway-chinese.png")分层示意图\]\]

**地幔**（；；；原於，意為**斗篷**），-{zh-cn:台湾称作**地函**;zh-tw:中國大陸和香港稱作**地幔**;zh-hk:臺灣稱作**地函**;}-，位於[地殼之下](../Page/地殼.md "wikilink")，[地核之上](../Page/地核.md "wikilink")，和地殼以[莫氏不連續面為界](../Page/莫氏不連續面.md "wikilink")，和地核間則以[古氏不連續面為界](../Page/古氏不連續面.md "wikilink")。厚度约2900公里。化学成分主要是含[铁](../Page/铁.md "wikilink")、[镁的](../Page/镁.md "wikilink")[矽酸鹽](../Page/矽酸鹽.md "wikilink")，平均密度是3.3–5.5
g/cm<sup>3</sup>。地函含[石榴子石](../Page/石榴石.md "wikilink")、[輝石](../Page/輝石.md "wikilink")、[橄欖石及其他類型的岩石](../Page/橄欖石.md "wikilink")。\[1\]占地球體積的83%，總質量的68%。\[2\]由於[P波及](../Page/P波.md "wikilink")[S波皆可通過地函](../Page/S波.md "wikilink")，故推測地函主要為固體構成。地函可分成**上部地函**、**過渡帶**及**下部地函**。

## 結構

[Earthquake_wave_paths.png](https://zh.wikipedia.org/wiki/File:Earthquake_wave_paths.png "fig:Earthquake_wave_paths.png")

### 上部地幔

上部地函約為地殼以下至深度400公里處，包含部分[岩石圈及](../Page/岩石圈.md "wikilink")[軟流圈](../Page/軟流圈.md "wikilink")，岩石圈部分厚約100公里。

[古登堡認為上部地函有一震波低速帶](../Page/賓諾·古登堡.md "wikilink")（），此帶的P波及S波的波速皆越深越慢，其頂端約在地面以下70至100公里，底部則約200至250公里深處，震波低速帶的上方為[岩石圈](../Page/岩石圈.md "wikilink")，此帶相當於[軟流圈](../Page/軟流圈.md "wikilink")。P波及S波的波速減慢表示岩石的剛性降低，岩石部分融熔為較具有塑性的岩石，可能是岩漿出現或是有極熱的岩石存在。於低速帶以下的波速又會開始增加。

上部地函的組成方式有兩種說法，一說為由[雙輝橄欖岩所組成](../Page/雙輝橄欖岩.md "wikilink")，礦物以[輝石及](../Page/輝石.md "wikilink")[橄欖石為主](../Page/橄欖石.md "wikilink")，並含有少量[尖晶石及](../Page/尖晶石.md "wikilink")[石榴子石](../Page/石榴子石.md "wikilink")，相當於[澳洲地球內部學家](../Page/澳洲.md "wikilink")所創立的[玄橄岩成分](../Page/玄橄岩.md "wikilink")，為[玄武岩及橄欖岩以](../Page/玄武岩.md "wikilink")1:3所組成的結合體。另一說為由榴輝石所組成，含有約等量的石榴子石及輝石。

### 過渡帶

過渡帶頂部約地表以下360至400公里，底部約深650至700公里處，P波及S波的波速在此帶突然增加，此帶也是最深震源所存在之處。此帶的形成和主要化學成分的變化無關，而是和結晶構造或相的變化有關。於過渡帶的下部因壓力增加使橄欖石分解為密度較大的簡單氧化物，如[氧化鐵](../Page/氧化鐵.md "wikilink")、[氧化矽](../Page/氧化矽.md "wikilink")、[氧化鎂等](../Page/氧化鎂.md "wikilink")，但詳細的礦物組成仍在研究中。此處的礦物因高壓而變得較有彈性，密度加大，進而增加了波速。

### 下部地函

下部地函為地表下700至2900公里深處，其下方即為地核。越往深處波速緩慢增加，是因壓力增大所造成，岩石的化學成分及岩相則少有變化。主要組成成分可能為密度高的[矽酸鹽或矽](../Page/矽酸鹽.md "wikilink")、鎂的氧化物（氧化鎂、氧化矽），氧化鐵占約10至12%，另含有少量的[氧化鈣](../Page/氧化鈣.md "wikilink")、[氧化鋁及](../Page/氧化鋁.md "wikilink")[氧化鈉等](../Page/氧化鈉.md "wikilink")。

## 地函組成元素

| 元素                           | 百分比  |                                    | 氧化物  | 百分比 |
| ---------------------------- | ---- | ---------------------------------- | ---- | --- |
| [氧](../Page/氧.md "wikilink") | 44.8 |                                    |      |     |
| [矽](../Page/矽.md "wikilink") | 21.5 | [氧化矽](../Page/二氧化硅.md "wikilink")  | 46   |     |
| [鎂](../Page/鎂.md "wikilink") | 22.8 | [氧化鎂](../Page/氧化鎂.md "wikilink")   | 37.8 |     |
| [鐵](../Page/鐵.md "wikilink") | 5.8  | [氧化鐵](../Page/鐵氧化物.md "wikilink")  | 7.5  |     |
| [鋁](../Page/鋁.md "wikilink") | 2.2  | [氧化鋁](../Page/氧化鋁.md "wikilink")   | 4.2  |     |
| [鈣](../Page/鈣.md "wikilink") | 2.3  | [氧化鈣](../Page/氧化鈣.md "wikilink")   | 3.2  |     |
| [鈉](../Page/鈉.md "wikilink") | 0.3  | [鈉氧化物](../Page/鈉氧化物.md "wikilink") | 0.4  |     |
| [鉀](../Page/鉀.md "wikilink") | 0.03 | [鉀氧化物](../Page/鉀氧化物.md "wikilink") | 0.04 |     |
| 總計                           | 99.7 | 總計                                 | 99.1 |     |

**地函組成元素與其氧化物組成重量百分比**\[3\]

## 參考資料

## 参看

  - [地球構造](../Page/地球構造.md "wikilink")
  - [地核](../Page/地核.md "wikilink")
  - [地壳](../Page/地壳.md "wikilink")
  - [大地電流](../Page/大地電流.md "wikilink")
  - [地函熱柱](../Page/地函熱柱.md "wikilink")

[Category:地震學](../Category/地震學.md "wikilink")
[Category:地球的结构](../Category/地球的结构.md "wikilink")

1.  Markl, Gregor. *Minerale und Gesteine: Mineralogie - Petrologie -
    Geochemie*, 3. Auflage. Springer Spektrum, Berlin, 2014, Seiten
    435-438
2.  何春蓀，普通地質學，國立編譯馆主編，五南圖書出版公司印行，1981，頁373
3.  [mantle@Everything2.com](http://everything2.com/index.pl?node=Mantle).
    Retrieved [2008-09-06](../Page/2008-09-06.md "wikilink").