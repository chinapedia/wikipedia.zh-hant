**軟食中獸科**（Hapalodectidae）是已[滅絕的](../Page/滅絕.md "wikilink")[中爪獸目](../Page/中爪獸目.md "wikilink")，生存於[古新世至](../Page/古新世.md "wikilink")[始新世的](../Page/始新世.md "wikilink")[北美洲及](../Page/北美洲.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")\[1\]，體型相對較細小，只有1-8公斤重。\[2\]軟食中獸科與著名的[中爪獸科稍為不同](../Page/中爪獸科.md "wikilink")，牠們的[牙齒適合切開食物](../Page/牙齒.md "wikilink")，尤其是肉類，而其他的中爪獸目（如[中爪獸或](../Page/中爪獸.md "wikilink")[中國中獸](../Page/中國中獸.md "wikilink")）的牙齒則適合咬碎骨頭。\[3\]軟食中獸科曾被認為是中爪獸科的[亞科](../Page/亞科.md "wikilink")\[4\]，但從[河塘軟食中獸的](../Page/河塘軟食中獸.md "wikilink")[頭顱骨得知一些不同之處而足以成立自己的](../Page/頭顱骨.md "wikilink")[科](../Page/科.md "wikilink")。\[5\]河塘軟食中獸的眶後棒在眼窩後閉合\[6\]，但其他的中爪獸目則沒有這個特徵。對於軟食中獸科的[骨骼所知甚少](../Page/骨骼.md "wikilink")，而顱後骨骼則只有[肱骨被描述](../Page/肱骨.md "wikilink")。這根骨頭的形態顯示牠們較中爪獸目不怎麼適合陸上運動。\[7\]

## 參考

[Category:中爪獸目](../Category/中爪獸目.md "wikilink")
[Category:軟食中獸科](../Category/軟食中獸科.md "wikilink")

1.

2.

3.
4.

5.

6.
7.