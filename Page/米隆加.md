**米隆加** ([西班牙文](../Page/西班牙文.md "wikilink")
Milonga)，是一个[音乐及](../Page/音乐.md "wikilink")[舞蹈](../Page/舞蹈.md "wikilink")[术语](../Page/术语.md "wikilink")，指的是[南美洲](../Page/南美洲.md "wikilink")，尤其是[阿根廷](../Page/阿根廷.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[乌拉圭一带的一种风格近似于](../Page/乌拉圭.md "wikilink")[探戈的流行舞曲的音乐形式](../Page/探戈.md "wikilink")。

作为舞曲的米隆加，可以填入不同的旋律，表达不同的乐思，但通常用来表现较粗犷不羁的内容。米隆加根植于非洲音乐，起源于[拉普拉塔河流域](../Page/拉普拉塔河.md "wikilink")，其全盛时期在19世纪70年代，但至今仍经久不衰。

## 语源

米隆加(Milonga)源自[非洲的](../Page/非洲.md "wikilink")[班图语](../Page/班图语.md "wikilink")，本义是“唱词”，传入西班牙文后，又西班牙文传入其他语言。

汉语的“米隆加”一词是西班牙文的音译。

## 与其他舞曲种类的关系

一般认为，米隆加是[探戈的先行者](../Page/探戈.md "wikilink")。米隆加舞曲对日后探戈风格的定型起了很大的影响。甚至可以认为，探戈是放慢节奏的米隆加。因此，米隆加舞较探戈比起来，速度快、舞步更粗犷不羁，而相应音乐表现的意境也比较直接而活泼。

米隆加在节奏型上与[Candombe](../Page/Candombe.md "wikilink")（冈东贝）舞也较为相似，但其导源于一种称为**payada
de contrapunto**的[声乐形式](../Page/声乐.md "wikilink")。

## 节奏型与乐谱记法

米隆加多采用2/4或4/4拍的乐谱记法，与探戈类似。但速度活泼，因此往往将[四分音符作更细致的划分](../Page/四分音符.md "wikilink")。

以2/4拍为例，如果将每[小节的四分音符划分为](../Page/小节.md "wikilink")8个[16分音符的话](../Page/16分音符.md "wikilink")，普通的2/4拍的重音只有两个重音，分别在第1拍和第5拍：

**1** 2 3 4 **5** 6 7 8

而米隆加的重音则较为多样化：

**1** 2 3 **4** **5** 6 **7** 8

或

**1** **2** 3 **4 5** 6 **7** 8

## 舞姿

米隆加舞蹈的姿势比较独特，采用男女舞伴互动的**握持**法，取面对面闭式舞姿。\[1\]

## 为米隆加风格作曲的音乐家

为米隆加风格作曲的音乐家甚多，但超出单纯舞曲范围，将米隆加提升至一种较有思想内涵的音乐形式，并能在古典音乐厅等场合以纯音乐艺术的形式进行展演的音乐家是阿根廷的阿斯托尔·[皮亚佐拉](../Page/皮亚佐拉.md "wikilink")(Astor
Piazzola)。

他以米隆加为节奏基底，创作过一系列脍炙人口的著名作品，其中包括《天使米隆加》(西班牙文 Milonga del Angel)

## 外部链接

  - [皮亚佐拉《天使米隆加》](http://bbs.cnstrad.com/thread-11829-1-1.html)

  -
## 参见

[阿根廷](../Page/阿根廷.md "wikilink") [皮亚佐拉](../Page/皮亚佐拉.md "wikilink")
[探戈](../Page/探戈.md "wikilink")

## 参考文献

[Category:南美洲艺术](../Category/南美洲艺术.md "wikilink")
[Category:舞曲](../Category/舞曲.md "wikilink")
[Category:歌曲形式](../Category/歌曲形式.md "wikilink")
[Category:西班牙语词汇](../Category/西班牙语词汇.md "wikilink")
[Category:汉语外来词](../Category/汉语外来词.md "wikilink")

1.