**陳秋霞**（英文名：，），[香港](../Page/香港.md "wikilink")1970至1980年代唱片金曲[女歌手](../Page/女歌手.md "wikilink")，以及紅極[東南亞](../Page/東南亞.md "wikilink")[中](../Page/中國.md "wikilink")[港](../Page/香港.md "wikilink")[台](../Page/台灣.md "wikilink")[韓的](../Page/韓國.md "wikilink")[電影](../Page/電影.md "wikilink")[演員](../Page/演員.md "wikilink")。出生於[香港](../Page/香港.md "wikilink")。

## 經歷

陳秋霞八歲開始學習[鋼琴](../Page/鋼琴.md "wikilink")，15歲具職業水準，並通過[英國皇家音樂學院](../Page/:en:Royal_Academy_of_Music.md "wikilink")8級鋼琴演奏優選，1975年處女作「Dark
Side Your Mind」獲得流行歌曲創作比賽作曲與演唱優勝，歌曲合寫人[Pato
Leung就是現今的金牌經理人](../Page/Pato_Leung.md "wikilink")[梁柏濤](../Page/梁柏濤.md "wikilink")，之後又代表香港赴[日本參加世界流行歌曲創作比賽](../Page/日本.md "wikilink")，從此成為著名歌星兼[作曲人](../Page/作曲人.md "wikilink")。

台灣[導演](../Page/導演.md "wikilink")[宋存壽發掘了她的表演天賦](../Page/宋存壽.md "wikilink")，1976年演出根據自己的故事改編拍攝的《[秋霞](../Page/秋霞.md "wikilink")》，是出道的第一部電影，使她獲得第14屆[金馬獎的最佳女主角](../Page/金馬獎.md "wikilink")，創下了影齡最短的影后紀錄。從1976年到1980年間，她一共在銀幕上演出了10多部電影，影片類型單一，除了客串演出《古寧頭大戰》外，其餘均屬一般時裝[文藝片](../Page/文藝片.md "wikilink")。

近年，陳秋霞學習[書法及參展](../Page/書法.md "wikilink")[香港的](../Page/香港.md "wikilink")「中國畫·畫中國」並參與多項社會公益活動。2006年正式復出，分別於七月與十一月發行韓國版"Flying
our dreams"與台灣版「放飛夢想」創作專輯。

其娛樂圈中好友名人包括[成龍](../Page/成龍.md "wikilink")、[溫拿五虎](../Page/溫拿樂隊.md "wikilink")、[黃霑](../Page/黃霑.md "wikilink")。

陳秋霞的近作是[夢幻之都影城](../Page/夢幻之都.md "wikilink")[投資](../Page/投資.md "wikilink")。\[1\]

陳秋霞的母校為香港知名名校—[民生書院](../Page/民生書院.md "wikilink")。

## 家庭

1981年陳秋霞嫁給了商人[鍾廷森為妻](../Page/鍾廷森.md "wikilink")，鍾廷森現為[馬來西亞](../Page/馬來西亞.md "wikilink")[金獅機構的董事主席](../Page/金獅集團.md "wikilink")。在結婚之後，事業如日中天的陳秋霞選擇退出演藝圈，並移居馬來西亞，家庭生活平靜。

## 個人生活

其姨甥[鄺祖德是歌手](../Page/鄺祖德.md "wikilink")，曾推出過2張唱片；姐夫[鄺君能於](../Page/鄺君能.md "wikilink")70至80年代曾加入[無綫電視及](../Page/無綫電視.md "wikilink")[麗的電視當藝員](../Page/麗的電視.md "wikilink")。

## 唱片

### 大碟

  - 1975年 《Dark side of your mind》（英語）【寶麗多2427006】\[2\] \[3\]
  - 1976年 《秋霞 Chelsia my love》（國語/英語）【寶麗多 2427009】
  - 1976年 《秋霞 Chelsia my love》（國語/英語）【歌林 KL1098】
  - 1977年 《溫馨在我心》（國語/英語）【歌林 KL1115】
  - 1977年 《蝴蝶夢》（國語/英語）【歌林 KL1122】
  - 1977年 《Because of you》（粵語/英語）【寶麗多2427011】
  - 1978年 《Queen of hearts》（英語）【寶麗多2427018】
  - 1978年 《第二道彩虹》（國語/粵語）【寶麗多 2427313】
  - 1978年 《第二道彩虹》（國語/英語）【歌林 KL1131】
  - 1978年 《第二道彩虹》（華語/粵語）(星馬版)【寶麗多 2427313】
  - 1978年 《Mind wave》（日語）【Philips】
  - 1979年 《煙波江上‧你不要走》（國語/英語）【歌林 KL1154】
  - 1979年 《一個女工的故事》（國語/英語）【歌林 KL1167】
  - 1979年 《一個女工的故事》（華語）(星馬版)【寶麗多 2427570】
  - 1979年 《心聲》（粵語）【寶麗多 2427325】
  - 1979年 《秋霞創作輯》（國語）【歌林 KL1172】
  - 1979年 《秋霞創作輯》（華語）(星馬版)【寶麗多 2427575】
  - 1980年 《陳秋霞之歌》（粵語）【寶麗多 2427326】
  - 1980年 《珍惜好年華》（粵語）【寶麗多 2427339】
  - 1980年 《秋霞創作輯 2》（國語）【歌林 KL1192】
  - 1980年 《一聲祝福》（華語）(星馬版)【寶麗多 2427592】
  - 1981年 《寒梅‧謠言》（國語/粵語）【歌林 KL1212】
  - 1981年 《山花朵朵》（華語）(星馬版)【寶麗多 2427346】
  - 1981年 《我的歌集》（國語）【寶麗金 2918107M】 　　
  - 2006年 《Fly our dreams》（粵語/國語/英語/韓語）【EMI】
  - 2006年 《放飛夢想》（粵語/國語/英語）【EMI】

### 細碟

  - 1975年《Our Last Song Together》b/w 《Where Are You》【Philips 6002 006】
  - 1975年 《Little Bird》b/w 《Are You Still Mad At Me》【Polydor 2076 014】
  - 1975年 《Dark Side of Your Mind》b/w 《I will Follow Him》Chelsia Chan
    【Polydor 2076 015】
  - 1975年 《Pocketful of Music》b/w 《I Only Wanna be with You》 Chelsia
    Chan 【Polydor 2076 016】
  - 1976年 《秋霞》電影《秋霞》主題曲（國語）【寶麗多 2076 018】
  - 1975年 《蝴蝶夢》電影《殺手、神槍、蝴蝶夢》主題曲（粵語）【Phlips 6002 018】
  - 1977年 《インスピレーション(Inspiration)》（日語）【Phlips】
  - 1977(8?)年 《ロンロングッバイ(Long Long Goodbye)》（日語）【Phlips】
  - 1978年 《チャイニーズ・ドール(Chinese Doll)》（日語）【Phlips】

### 雜錦大碟

  - 1975年 《點解手牽手》Let's Rock 大家樂【飛利浦 6380004】

## 演出作品

### 電影

|        |                                        |        |        |                                                                                                          |
| ------ | -------------------------------------- | ------ | ------ | -------------------------------------------------------------------------------------------------------- |
| **年份** | **電影名**                                | **地區** | **角色** | **合作演員**                                                                                                 |
| 1976年  | 《秋霞》Chelsia My Love                    | 香港     |        |                                                                                                          |
| 1978年  | 《煙波江上》Love on a Foggy River            | 台灣     |        |                                                                                                          |
| 1978年  | 《你不要走》                                 | 台灣     |        |                                                                                                          |
| 1979年  | 《第二道彩虹》Rainbow in My Heart             | 香港     |        |                                                                                                          |
| 1979年  | 《結婚三級跳》                                | 台灣     |        |                                                                                                          |
| 1979年  | 《一個女工的故事》Fly with Love                 | 台灣     |        |                                                                                                          |
| 1979年  | 《悲之秋》A Sorrowful Wedding               | 台灣     |        |                                                                                                          |
| 1980年  | 《雁兒歸》Flying Home                       | 台灣     |        |                                                                                                          |
| 1980年  | 《一對傻鳥》(港名:戀愛反斗星) Poor Chasers          | 台灣     |        | [秦漢](../Page/秦漢.md "wikilink")、[譚詠麟](../Page/譚詠麟.md "wikilink")、**陳秋霞**、[林青霞](../Page/林青霞.md "wikilink") |
| 1980年  | 《[古寧頭大戰](../Page/古寧頭大戰.md "wikilink")》 | 台灣     |        |                                                                                                          |
|        |                                        |        |        |                                                                                                          |

## 獎項

<table style="width:143%;">
<colgroup>
<col style="width: 75%" />
<col style="width: 21%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>作品</p></th>
<th><p>角色</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1977年</p></td>
<td><p><a href="../Page/第14屆金馬獎.md" title="wikilink">第14屆金馬獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《秋霞》</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考文獻

## 外部連結

  - [Chelsia Chan Hong Kong Web Site
    陳秋霞香港網站](http://www.chelsia-chan.com)

  -
  -
  -
  -
  -
  -
[Category:金馬獎最佳女主角獲得者](../Category/金馬獎最佳女主角獲得者.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:香港創作歌手](../Category/香港創作歌手.md "wikilink")
[C陳](../Category/蘇州人.md "wikilink")
[C](../Category/民生書院校友.md "wikilink") [Chau
Ha](../Category/陳姓.md "wikilink")

1.
2.  [Music - Dark Side of Your Mind (by 曲:陳秋霞
    詞:梁柏濤 1975)](http://www.youtube.com/watch?v=_Mm8etMBVSA)
3.  [陳秋霞香港網站 Chelsia Chan Hong Kong Web
    Site](http://www.chelsia-chan.com/music_hk_darkside_single.htm)