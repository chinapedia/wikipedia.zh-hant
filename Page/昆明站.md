**昆明站**位于[中国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[昆明市](../Page/昆明市.md "wikilink")[官渡区北京路南端](../Page/官渡区.md "wikilink")，于1966年启用，为[昆明铁路局管辖的](../Page/昆明铁路局.md "wikilink")[一等站](../Page/一等站_\(中铁\).md "wikilink")，是[沪昆铁路](../Page/沪昆铁路.md "wikilink")、[成昆铁路](../Page/成昆铁路.md "wikilink")、[南昆铁路以及](../Page/南昆铁路.md "wikilink")[南昆客运专线的终点](../Page/南昆客运专线.md "wikilink")。昆明站北距[成都站](../Page/成都站.md "wikilink")1100公里，东距[上海站](../Page/上海站.md "wikilink")2690公里，东南距[南宁站](../Page/南宁站.md "wikilink")828公里。

## 历史

国民政府规划的[米轨叙昆铁路](../Page/米轨.md "wikilink")（叙府/宜宾至昆明）于1939年开工建设，但工程受到战争影响于1942年停工，仅完成从昆明到沾益156公里的铺轨\[1\]。1949年后，滇黔线和内昆线的建设被纳入[国民经济建设第二个五年计划](../Page/第二个五年计划_\(中国\).md "wikilink")，1958年开工。其中，昆明至沾益段利用原叙昆铁路路基，但在施工过程中改为[准轨](../Page/准轨.md "wikilink")。1962年原滇黔线和内昆线的贵阳-树舍-昆明段合并为[贵昆铁路](../Page/贵昆铁路.md "wikilink")。贵昆铁路全线于1966年3月建成\[2\]，昆明火车站随即投入使用\[3\]，为通过式车站。车站设有一座面积6420平方米的砖混结构站房，内设3个旅客候车室共1415平方米。车站站场三台八线，其中客车到发线5股、货车到发线及正线共2股、机走线1股\[4\]。

1971年7月1日，[成昆铁路开通运营](../Page/成昆铁路.md "wikilink")。当日从昆明站开行始发列车，在[西昌站与成都发来的列车相遇](../Page/西昌站.md "wikilink")\[5\]。至1980年代初，昆明站每日开行6对列车，主要发往北京、上海、贵阳、攀枝花、中谊村、金马村方向\[6\]。1997年11月30日[南昆铁路建成通车](../Page/南昆铁路.md "wikilink")。

2003年7月，昆明铁路客站开始改扩建工程，工程包括新建站房和站台改造。在车站进行改扩建工程期间，旅客使用临时候车室进出站\[7\]。工程于2004年12月28日竣工，并于2005年春节前投入使用\[8\]\[9\]。2007年初：昆明站南站房建成，成為中国西南部首個雙面火車站\[10\]。但由于配套问题，南站房从未启用\[11\]。

2017年9月4日[南昆客运专线昆明南至昆明段开通运营](../Page/南昆客运专线.md "wikilink")，昆明站首次开行动车组列车\[12\]。同时站内开启快速换乘通道，站内换乘不需再安检\[13\]。昆明站在2018年[昆楚大铁路开通前夕对候车室进行了重新装修](../Page/昆楚大铁路.md "wikilink")\[14\]。

## 车站设施

### 站房

昆明站由主站房、站前广场和高架平台、跨线式高架候车室、地下停车场等组成。昆明站北站房（主站房）于2005年投入使用，总面积二万七千多平方米。进站大厅屋檐为“人”字形的玻璃墙，为傣族建筑风格\[15\]，并获得2005年[鲁班奖](../Page/鲁班奖.md "wikilink")\[16\]。昆明站南站房于2006年完工，高4层，建筑面积11502平方米\[17\]。但截至2018年南站房从未被使用。

站房设有4个普通候车室、3个贵宾候车室、1个母婴候车室和1个军人候车室\[18\]，中央为候车通道。站房二层设有4个候车厅入口，通道东侧为第二候车厅，设有八九百个座位，为茶室；西侧可到残疾人、母子候车厅。高架候车区横跨站场，设有第三候车厅与第五候车厅（原第四候车室），每个设有1000多个座位。第五候车室原为“农民工候车室”，后因该名称含有歧视意味，遂恢复原名\[19\]。2017年9月昆明站开行动车组列车后，第五候车室内的部分区域被用作动车组列车专用候车区\[20\]。第三与第五候车厅间的中央通道两侧共植有6棵高大的棕榈树，棕榈树下还种植多种绿色植物和鲜花\[21\]，后改为民族风情购物长廊。昆明站南站房竣工后新增第四候车室（原第五、第六候车室），位于长廊末端。候车室最高聚集人数为七千人，日乘降旅客可达十一万人。\[22\]\[23\]

昆明站候车室每天23:45关闭，不允许进站候车、购票取票，次日早晨才会开放。昆明站原在站房进站口设有一道安检\[24\]，[2014年昆明火车站暴力恐怖袭击事件后在站前广场增设一道安检](../Page/2014年昆明火车站暴力恐怖袭击事件.md "wikilink")\[25\]。2017年位于站房候车厅的安检被撤除\[26\]。站前广场设有第二售票区\[27\]。

<File:Kunming> Railway Station Entrance auto ticket
gates.jpg|昆明站进站口已翻新並增設自助验证机 <File:Kunming> Railway
Station Shopping Arcade.jpg|位於候車室中央的民族风情购物长廊（2017年） <File:Kunming>
Railway Station Exit Concourse.jpg|昆明站出站大厅

### 站场

昆明站建有6座候车站台，共有13条股道\[28\]。站房上方采用弧形无柱风雨棚\[29\]\[30\]，该雨棚工程荣获中国建筑钢结构金奖\[31\]。

## 旅客列车目的地

[CRH380A-2868_EMU_at_Kunming_Railway_Station.jpg](https://zh.wikipedia.org/wiki/File:CRH380A-2868_EMU_at_Kunming_Railway_Station.jpg "fig:CRH380A-2868_EMU_at_Kunming_Railway_Station.jpg")电力动车组停靠在昆明站2站台。\]\]
[K434_25G_train_at_Kunming_Railway_Station.jpg](https://zh.wikipedia.org/wiki/File:K434_25G_train_at_Kunming_Railway_Station.jpg "fig:K434_25G_train_at_Kunming_Railway_Station.jpg")的[K434次列车停靠在昆明站](../Page/K433/432、K434/431次列车.md "wikilink")2站台。\]\]
[ChinaRail_HXD3D0135.JPG](https://zh.wikipedia.org/wiki/File:ChinaRail_HXD3D0135.JPG "fig:ChinaRail_HXD3D0135.JPG")位于昆明车辆段附近\]\]

<table>
<thead>
<tr class="header">
<th><p>列車所屬路局</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、、、、、、、、、、、、、、、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td></td>
</tr>
<tr class="odd">
<td><center></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>&lt;!--|-</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>&lt;!--|-</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>--&gt;</p></td>
</tr>
<tr class="even">
<td><center></td>
<td></td>
</tr>
</tbody>
</table>

### 相关列车

  - [三进列车](../Page/三进列车.md "wikilink")

<!-- end list -->

  - 进京：[Z53/54次列车](../Page/Z53/54次列车.md "wikilink")、[Z161/162次列车](../Page/Z161/162次列车.md "wikilink")、[K473/474次列车](../Page/K473/474次列车.md "wikilink")
  - 进沪：[T381/382次列车](../Page/T381/382次列车.md "wikilink")、[K79/80次列车](../Page/K79/80次列车.md "wikilink")、[K739/740次列车](../Page/K739/740次列车.md "wikilink")
  - 进穗：[K365/364、K366/363次列车](../Page/K365/364、K366/363次列车.md "wikilink")、[K1206/1207、K1208/1205次列车](../Page/K1206/1207、K1208/1205次列车.md "wikilink")（终到深圳东）

<!-- end list -->

  - 其他列车

<!-- end list -->

  - [Z288/289、Z290/287次列车](../Page/Z288/289、Z290/287次列车.md "wikilink")（昆明↔宁波）
  - [K155/156次列车](../Page/K155/156次列车.md "wikilink")（昆明↔南京）
  - [K230/231、K232/229次列车](../Page/K230/231、K232/229次列车.md "wikilink")（昆明↔厦门）
  - [K491/492次列车](../Page/K491/492次列车.md "wikilink")（昆明↔济南）
  - [K722/723、K724/721次列车](../Page/K722/723、K724/721次列车.md "wikilink")（昆明↔南通）
  - [K726/727、K728/725次列车](../Page/K726/727、K728/725次列车.md "wikilink")（昆明↔哈爾濱西）
  - [K878/875、K876/877次列车](../Page/K878/875、K876/877次列车.md "wikilink")（昆明↔烟台）
  - [K1502/1503、K1504/1501次列车](../Page/K1502/1503、K1504/1501次列车.md "wikilink")（昆明↔乌鲁木齐）
  - [K2288/2285、K2286/2287次列车](../Page/K2288/2285、K2286/2287次列车.md "wikilink")（昆明↔长春）

## 接驳交通

### 地铁

[昆明地铁](../Page/昆明地铁.md "wikilink")[昆明火车站站位于昆明站南广场](../Page/昆明火车站站.md "wikilink")，目前有[昆明地铁1号线经过](../Page/昆明地铁1号线.md "wikilink")，[2号线虽然经过本站但暂缓开通](../Page/昆明地铁2号线.md "wikilink")。由于昆明站南广场从未实际启用，因此地铁出站后需经过城市道路步行且步行时间超过25分钟，导致地铁出口有不少黑车司机拉客。[环城南路站至昆明站北广场步行约有](../Page/环城南路站.md "wikilink")850米的距离\[32\]，步行时间仅需13分钟\[33\]。

### 公交

## 事件

### 知青卧轨事件

1978年12月28日，昆明的知青们为[争取返城](../Page/知青大返城.md "wikilink")，在昆明站以东的羊方凹集体卧轨示威，导致数十对客运和货运列车受阻。请愿团与政府僵持三天之后，党中央国务院于12月31日同意知青请愿团赴京反映情况，但请愿团人数需在30人以内\[34\]\[35\]。

### 袭击事件

2014年3月1日晚21时12分，五名[新疆籍](../Page/新疆.md "wikilink")[恐怖分子持刀冲入昆明站砍杀路人](../Page/恐怖分子.md "wikilink")。随后大批民警赶至现场，火车站内传出枪声，当场4名暴徒被击毙，而其餘一名暴徒被擊傷，事件发生于21时12分，但直到23时还有人求救，警方起初没有估计到事件的严重性，未能及时安排警力以反恐处理事件。这次事件发生后已造成31人死亡，142人受伤。“3.01”事件定性为严重暴力恐怖事件。\[36\]\[37\]\[38\]

## 相邻车站

|-   |-   |-

## 外部链接

  -
## 参考文献

## 参见

  - [昆明东站](../Page/昆明东站.md "wikilink")、[昆明南站](../Page/昆明南站.md "wikilink")、[昆明西站](../Page/昆明西站.md "wikilink")、[昆明北站](../Page/昆明北站.md "wikilink")
  - [昆明铁路局](../Page/昆明铁路局.md "wikilink")
  - [孽债](../Page/孽债.md "wikilink")

{{-}}

[Category:昆明市铁路车站](../Category/昆明市铁路车站.md "wikilink")
[Category:官渡区](../Category/官渡区.md "wikilink")
[Category:昆明铁路局](../Category/昆明铁路局.md "wikilink")
[Category:中国铁路一等站](../Category/中国铁路一等站.md "wikilink")
[Category:成昆铁路车站](../Category/成昆铁路车站.md "wikilink")
[Category:南昆铁路车站](../Category/南昆铁路车站.md "wikilink")
[Category:1966年启用的铁路车站](../Category/1966年启用的铁路车站.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.

9.

10. [西南首個雙面火車站春運啟用](https://web.archive.org/web/20070220060053/http://www.yn.xinhuanet.com/newscenter/2007-01/26/content_9147978.htm)

11.

12.

13.

14.

15.
16.

17.

18.
19.

20.
21.
22.
23.

24.
25.

26.

27.

28.

29.
30.
31.

32.

33.

34.

35.

36.

37.

38.