[美國](../Page/美國.md "wikilink")**总统國家安全事務助理**（Assistant to the President
for National Security Affairs），又称**國家安全顧問**（National Security
Advisor）是[美國總統在](../Page/美國總統.md "wikilink")[國家安全相關事項的主要參謀](../Page/國家安全.md "wikilink")。國家安全顧問隸屬總統辦公室，屬[國家安全會議](../Page/國家安全會議_\(美國\).md "wikilink")。

這個職位自1953年設立。總統無須經由[參議院同意即可任命國家安全顧問](../Page/美國參議院.md "wikilink")。因此他們與官僚體系的[國防部無關](../Page/美國國防部.md "wikilink")，可以向總統直接提供超然獨立之意見，為總統出謀獻策，享有超然地位，但各任國家安全顧問的權利與角色卻不盡相同。在緊急時刻，國家安全顧問操控[白宮戰情室](../Page/白宮戰情室.md "wikilink")，提供總統危機事件的最新發展。

## 歷任總統與其國安顧問

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>肖像</p></th>
<th><p>名字</p></th>
<th><p>任期</p></th>
<th><p><a href="../Page/美國總統.md" title="wikilink">總統</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>上任</p></td>
<td><p>卸任</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
<td><p>(Robert Cutler)<br />
(1895-1974)</p></td>
<td><p>1953年3月23日</p></td>
<td><p>1955年4月2日</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:No_image.svg" title="fig:No_image.svg">No_image.svg</a></p></td>
<td><p>(Dillon Anderson)<br />
(1906-1974)</p></td>
<td><p>1955年4月2日</p></td>
<td><p>1956年9月1日</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:William_Harding_Jackson.jpg" title="fig:William_Harding_Jackson.jpg">William_Harding_Jackson.jpg</a></p></td>
<td><p>(William.H.Jackson)<br />
(1901-1971)</p></td>
<td><p>1956年9月1日</p></td>
<td><p>1957年1月7日</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td></td>
<td><p>(Robert Cutler.第二任)<br />
(1895-1974)</p></td>
<td><p>1957年1月7日</p></td>
<td><p>1958年6月24日</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gordon_Gray_-_Project_Gutenberg_etext_20587.jpg" title="fig:Gordon_Gray_-_Project_Gutenberg_etext_20587.jpg">Gordon_Gray_-_Project_Gutenberg_etext_20587.jpg</a></p></td>
<td><p>(Gordon Gray)<br />
(1909-1982)</p></td>
<td><p>1958年6月24日</p></td>
<td><p>1961年1月13日</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:McGeorge_Bundy.jpg" title="fig:McGeorge_Bundy.jpg">McGeorge_Bundy.jpg</a></p></td>
<td><p>(McGeorge Bundy)<br />
(1919-1996)</p></td>
<td><p>1961年1月20日</p></td>
<td><p>1966年2月28日</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Walt_Rostow_1968.jpg" title="fig:Walt_Rostow_1968.jpg">Walt_Rostow_1968.jpg</a></p></td>
<td><p>(Walt W. Rostow)<br />
(1916-2003)</p></td>
<td><p>1966年4月1日</p></td>
<td><p>1969年1月20日</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Henry_Kissinger.jpg" title="fig:Henry_Kissinger.jpg">Henry_Kissinger.jpg</a></p></td>
<td><p><a href="../Page/亨利·基辛格.md" title="wikilink">亨利·基辛格</a>(Henry Kissinger)<br />
(1923-)</p></td>
<td><p>1969年1月20日</p></td>
<td><p>1975年11月3日</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Brent_Scowcroft.jpg" title="fig:Brent_Scowcroft.jpg">Brent_Scowcroft.jpg</a></p></td>
<td><p>(Brent Scowcroft)<br />
(1925-)</p></td>
<td><p>1975年11月3日</p></td>
<td><p>1977年1月20日</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Zbigniew_Brzezinski,_1977.jpg" title="fig:Zbigniew_Brzezinski,_1977.jpg">Zbigniew_Brzezinski,_1977.jpg</a></p></td>
<td><p><a href="../Page/兹比格涅夫·布热津斯基.md" title="wikilink">兹比格涅夫·布热津斯基</a>(Zbigniew Brzezinski)<br />
(1928-2017)</p></td>
<td><p>1977年1月20日</p></td>
<td><p>1981年1月21日</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Richard_V._Allen_1981.jpg" title="fig:Richard_V._Allen_1981.jpg">Richard_V._Allen_1981.jpg</a></p></td>
<td><p>(Richard V. Allen)<br />
(1936-)</p></td>
<td><p>1981年1月21日</p></td>
<td><p>1982年1月4日</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:William_patrick_clark.png" title="fig:William_patrick_clark.png">William_patrick_clark.png</a></p></td>
<td><p>(William Clark)<br />
(1931-2013)</p></td>
<td><p>1982年1月4日</p></td>
<td><p>1983年10月17日</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Robert_Mcfarlane_IAGS.jpg" title="fig:Robert_Mcfarlane_IAGS.jpg">Robert_Mcfarlane_IAGS.jpg</a></p></td>
<td><p>(Robert C. McFarlane)<br />
(1937-)</p></td>
<td><p>1983年10月17日</p></td>
<td><p>1985年12月4日</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Admiral_John_Poindexter,_official_Navy_photo,_1985.JPEG" title="fig:Admiral_John_Poindexter,_official_Navy_photo,_1985.JPEG">Admiral_John_Poindexter,_official_Navy_photo,_1985.JPEG</a></p></td>
<td><p>(John Poindexter)<br />
(1936-)</p></td>
<td><p>1985年12月4日</p></td>
<td><p>1986年11月25日</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Frank_Carlucci_official_portrait.JPEG" title="fig:Frank_Carlucci_official_portrait.JPEG">Frank_Carlucci_official_portrait.JPEG</a></p></td>
<td><p>(Frank Carlucci)<br />
(1930-)</p></td>
<td><p>1986年12月2日</p></td>
<td><p>1987年11月23日</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Colin_Powell_2005.jpg" title="fig:Colin_Powell_2005.jpg">Colin_Powell_2005.jpg</a></p></td>
<td><p><a href="../Page/科林·鲍威尔.md" title="wikilink">科林·鲍威尔</a>(Colin Powell)<br />
(1937-)</p></td>
<td><p>1987年11月23日</p></td>
<td><p>1989年1月20日</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Brent_Scowcroft.jpg" title="fig:Brent_Scowcroft.jpg">Brent_Scowcroft.jpg</a></p></td>
<td><p>(Brent Scowcroft.第二任)<br />
(1925-)</p></td>
<td><p>1989年1月20日</p></td>
<td><p>1993年1月20日</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Anthony_Lake_0c175_7733.jpg" title="fig:Anthony_Lake_0c175_7733.jpg">Anthony_Lake_0c175_7733.jpg</a></p></td>
<td><p><a href="../Page/安東尼·雷克.md" title="wikilink">安東尼·雷克</a>(Anthony Lake)<br />
(1939-)</p></td>
<td><p>1993年1月20日</p></td>
<td><p>1997年3月14日</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:SandyBerger.jpg" title="fig:SandyBerger.jpg">SandyBerger.jpg</a></p></td>
<td><p><a href="../Page/桑迪·伯格.md" title="wikilink">桑迪·伯格</a>(Samuel Berger)<br />
(1945-2015)</p></td>
<td><p>1997年3月14日</p></td>
<td><p>2001年1月20日</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Condoleezza_Rice_cropped.jpg" title="fig:Condoleezza_Rice_cropped.jpg">Condoleezza_Rice_cropped.jpg</a></p></td>
<td><p><a href="../Page/康多莉扎·賴斯.md" title="wikilink">康多莉扎·賴斯</a>(Condoleezza Rice)<br />
(1954-)</p></td>
<td><p>2001年1月22日</p></td>
<td><p>2005年1月25日</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Stephen_Hadley.jpg" title="fig:Stephen_Hadley.jpg">Stephen_Hadley.jpg</a></p></td>
<td><p><a href="../Page/斯蒂芬·哈德利.md" title="wikilink">斯蒂芬·哈德利</a>(Stephen Hadley)<br />
(1947-)</p></td>
<td><p>2005年1月26日</p></td>
<td><p>2009年1月20日</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:James_L._Jones.jpg" title="fig:James_L._Jones.jpg">James_L._Jones.jpg</a></p></td>
<td><p>(James L. Jones)<br />
(1943-)</p></td>
<td><p>2009年1月20日</p></td>
<td><p>2010年10月8日</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Thomas_Donilon.jpg" title="fig:Thomas_Donilon.jpg">Thomas_Donilon.jpg</a></p></td>
<td><p>(Thomas E. Donilon)<br />
(1955-)</p></td>
<td><p>2010年10月8日</p></td>
<td><p>2013年7月1日</p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Susan_Rice,_official_State_Dept_photo_portrait,_2009.jpg" title="fig:Susan_Rice,_official_State_Dept_photo_portrait,_2009.jpg">Susan_Rice,_official_State_Dept_photo_portrait,_2009.jpg</a></p></td>
<td><p><a href="../Page/蘇珊·賴斯.md" title="wikilink">蘇珊·賴斯</a> (Susan E. Rice)<br />
(1964-)</p></td>
<td><p>2013年7月1日</p></td>
<td><p>2017年1月20日</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Michael_T_Flynn.jpg" title="fig:Michael_T_Flynn.jpg">Michael_T_Flynn.jpg</a></p></td>
<td><p><a href="../Page/迈克尔·T·弗林.md" title="wikilink">迈克尔·T·弗林</a> (Michael T. Flynn)<br />
(1958-)</p></td>
<td><p>2017年1月20日</p></td>
<td><p>2017年2月13日</p></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Keith_Kellogg_2000.jpg" title="fig:Keith_Kellogg_2000.jpg">Keith_Kellogg_2000.jpg</a></p></td>
<td><p><em><a href="../Page/基思·凱洛格.md" title="wikilink">基思·凱洛格</a></em>(代理) (Keith Kellogg)<br />
(1944-)</p></td>
<td><p>2017年2月13日</p></td>
<td><p>2017年2月20日</p></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:H.R._McMaster_ARCIC_2014.jpg" title="fig:H.R._McMaster_ARCIC_2014.jpg">H.R._McMaster_ARCIC_2014.jpg</a></p></td>
<td><p><a href="../Page/H·R·麥克馬斯特.md" title="wikilink">H·R·麥克馬斯特</a> (H.R. McMaster)<br />
(1962-)</p></td>
<td><p>2017年2月20日</p></td>
<td><p>2018年4月9日</p></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:John_R._Bolton_official_photo.jpg" title="fig:John_R._Bolton_official_photo.jpg">John_R._Bolton_official_photo.jpg</a></p></td>
<td><p><a href="../Page/約翰·波頓.md" title="wikilink">約翰·波頓</a> (John Robert Bolton)<br />
(1948-)</p></td>
<td><p>2018年4月9日</p></td>
<td></td>
</tr>
</tbody>
</table>

[category:美國政府](../Page/category:美國政府.md "wikilink")

[\*](../Category/美国国家安全顾问.md "wikilink")