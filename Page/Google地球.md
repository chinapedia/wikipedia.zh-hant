**Google地球**（）是一款[Google公司開發的](../Page/Google公司.md "wikilink")[軟件](../Page/軟件.md "wikilink")，
它把[衛星照片](../Page/衛星照片.md "wikilink")、[航空照相和](../Page/航空照相.md "wikilink")[GIS布置在一個](../Page/地理信息系統.md "wikilink")[地球的](../Page/地球.md "wikilink")[三維](../Page/三維.md "wikilink")[模型上](../Page/模型.md "wikilink")，支援多種作業系統平台。

## 縱覽

Google地球使用了[公共領域的圖片](../Page/公共領域.md "wikilink")、受許可的航空照相圖片、[KeyHole](../Page/KeyHole.md "wikilink")[間諜衛星的圖片和很多其他衛星所拍攝的城鎮照片](../Page/間諜衛星.md "wikilink")。甚至連[Google地圖沒有提供的圖片都有](../Page/Google地圖.md "wikilink")。分為免費版與專業版兩種。

### 语言

<table style="width:99%;">
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/阿拉伯语.md" title="wikilink">阿拉伯语</a></li>
<li><a href="../Page/保加利亚语.md" title="wikilink">保加利亚语</a></li>
<li><a href="../Page/孟加拉语.md" title="wikilink">孟加拉语</a></li>
<li><a href="../Page/加泰罗尼亚语.md" title="wikilink">加泰罗尼亚语</a></li>
<li><a href="../Page/繁体中文.md" title="wikilink">繁体中文</a></li>
<li><a href="../Page/简体中文.md" title="wikilink">简体中文</a></li>
<li><a href="../Page/克罗地亚语.md" title="wikilink">克罗地亚语</a></li>
<li><a href="../Page/捷克语.md" title="wikilink">捷克语</a></li>
<li><a href="../Page/丹麦语.md" title="wikilink">丹麦语</a></li>
<li><a href="../Page/荷兰语.md" title="wikilink">荷兰语</a></li>
<li><a href="../Page/美式英语.md" title="wikilink">美式英语</a></li>
<li><a href="../Page/英式英语.md" title="wikilink">英式英语</a></li>
<li><a href="../Page/菲律宾语.md" title="wikilink">菲律宾语</a></li>
<li><a href="../Page/芬兰语.md" title="wikilink">芬兰语</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/希腊语.md" title="wikilink">希腊语</a></li>
<li><a href="../Page/希伯来语.md" title="wikilink">希伯来语</a></li>
<li><a href="../Page/印地语.md" title="wikilink">印地语</a></li>
<li><a href="../Page/匈牙利语.md" title="wikilink">匈牙利语</a></li>
<li><a href="../Page/印度尼西亚语.md" title="wikilink">印度尼西亚语</a></li>
<li><a href="../Page/意大利语.md" title="wikilink">意大利语</a></li>
<li><a href="../Page/日语.md" title="wikilink">日语</a></li>
<li><a href="../Page/朝鲜语.md" title="wikilink">朝鲜语</a></li>
<li><a href="../Page/拉脱维亚语.md" title="wikilink">拉脱维亚语</a></li>
<li><a href="../Page/立陶宛语.md" title="wikilink">立陶宛语</a></li>
<li><a href="../Page/挪威语.md" title="wikilink">挪威语</a></li>
<li><a href="../Page/波兰语.md" title="wikilink">波兰语</a></li>
<li><a href="../Page/葡萄牙语.md" title="wikilink">葡萄牙语</a></li>
<li><a href="../Page/巴西葡萄牙语.md" title="wikilink">巴西葡萄牙语</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/俄语.md" title="wikilink">俄语</a></li>
<li><a href="../Page/塞尔维亚语.md" title="wikilink">塞尔维亚语</a></li>
<li><a href="../Page/斯洛伐克语.md" title="wikilink">斯洛伐克语</a></li>
<li><a href="../Page/斯洛文尼亚语.md" title="wikilink">斯洛文尼亚语</a></li>
<li><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></li>
<li>拉美<a href="../Page/西班牙语.md" title="wikilink">西班牙语</a></li>
<li><a href="../Page/瑞典语.md" title="wikilink">瑞典语</a></li>
<li><a href="../Page/泰语.md" title="wikilink">泰语</a></li>
<li><a href="../Page/土耳其语.md" title="wikilink">土耳其语</a></li>
<li><a href="../Page/乌克兰语.md" title="wikilink">乌克兰语</a></li>
<li><a href="../Page/越南语.md" title="wikilink">越南语</a></li>
<li><a href="../Page/法语.md" title="wikilink">法语</a></li>
<li><a href="../Page/德语.md" title="wikilink">德语</a></li>
<li><a href="../Page/罗马尼亚语.md" title="wikilink">罗马尼亚语</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 功用

### 维基百科和Panoramio

2006年12月Google地球增加了一个新的图层，名称为“地理网络”，内容包括整合[维基百科的相关条目和](../Page/维基百科.md "wikilink")[Panoramio的相关照片](../Page/Panoramio.md "wikilink")。在维基百科，用户可以通过模板在条目中添加地理坐标。2007年7月Google收购Panoramio。\[1\]

Panoramio网站上经过定位的照片符合Google地球收录条件并经管理人员审查通过之后，通常在一星期后便会在Google
地球上相应的位置展示， 而等待接受审查的时间最多需要三星期。

### 模拟飞行器

在Google地球启动成功后，使用[Windows的用戶](../Page/Windows.md "wikilink")，按下Ctrl+Alt+A会弹出Google模拟飞行器；而使用[Mac
OS X的](../Page/Mac_OS_X.md "wikilink")
用户，需按Command+Option+A。當成功启动模拟飞行器後，下一次启动Google
Earth，就可以直接选择Tools → Enter Flight Simulator启动模拟器。退出模拟器，就要按Exit Flight
Simulator，或者上面提及的快捷鍵。目前有兩款飞机，[F16和SR](../Page/F16.md "wikilink")22可供模拟。前者是戰鬥機，後者是螺旋桨飞机。飞行航线方面，可以從世界各地（含空中）開出，或者是軟體提供的預設地點。

这项功能在4.2版以[彩蛋形式推出](../Page/彩蛋_\(視覺\).md "wikilink")，5.0版以后作为正式功能。

### 星空

2007年8月22日Google地球推出4.2版，此版本正式加入Google星空功能。该功能提供用户查看[恒星和其他](../Page/恒星.md "wikilink")[天体的功能](../Page/天体.md "wikilink")。\[2\]Google星空汇集了[加州理工学院Palomar天文台](../Page/加州理工学院.md "wikilink")、[空间望远镜研究所](../Page/空间望远镜研究所.md "wikilink")、数字星空调查联盟、斯隆数字星空调查、英国天文技术中心以及英澳天文台的星空图片，使用[哈勃空间望远镜拍摄](../Page/哈勃空间望远镜.md "wikilink")。覆盖的宇宙空间包括1亿颗星球以及2亿星系。\[3\]

2008年3月13日，Google推出了基于浏览器的[Google星空](http://www.google.com/sky/)。

### 街景视图

2008年4月15日Google地球推出4.3版，此版本正式整合街景视图功能。Google街景视图提供水平方向360°及垂直方向290°的街道全景，让使用者能检视所选城市地面上街道不同位置及其两旁的景物。這項服務最初啟用時，僅涵蓋[美國的五大城市](../Page/美國.md "wikilink")。如今已擴展至114個國家和地区的各大城市。

Google街景视图會根據使用者的要求，將裝設於旗下車隊車頂上的攝影機所拍下的照片以球狀影像（imageorbs）在Google地图上定位，並以Google地图的衛星影像為背景展示。在某些限定行人通行的区域、狭窄的街道以及公园小巷等车不能进入的地方，则以Google[三輪車替代](../Page/三輪車.md "wikilink")。\[4\]此球狀影像可使用[滑鼠點擊移動](../Page/滑鼠.md "wikilink")。利用以上操作，街景的圖片即可從不同大小、不同方向及不同角度觀看。街景視圖中沿著街道展示的路線（在Google地球上以多個相機圖案標示），即為街景視圖拍攝時汽車行走的路線。

### 历史图片

历史图片在Google地球5.0版本推出，它提供用户查看历史卫星图片。此功能可用于各地区历史的分析，以及随时间推移的发展状况。\[5\]

### 水體和海洋

Google海洋提供世界各地海洋的水下全景图，附带世界各地的航海影像、图片、适合冲浪处、适合潜水处、鱼群出没地、有过真实海洋考察的地点等信息。利用此功能，用户可以潜到海平面以下，以三维的形式探索海底的各个角落。\[6\]

### 火星

Google火星通过三维图像来观察火星的功能，这也是Google与[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")（NASA）合作的成果。在Google地球的工具栏中选择“Mars（火星）”后，用户就可以通过最新的高分辨率[三维图像来实现火星之旅](../Page/三维.md "wikilink")，寻找人类发射的探测器在这颗红色星球上的着陆地点等。

### 月球

2009年7月20日正值庆祝[阿波罗11号成功发射](../Page/阿波罗11号.md "wikilink")40周年之际，Google推出基于Google地球的Google月球版本，它允许用户查看[月球的卫星图像](../Page/月球.md "wikilink")。\[7\]\[8\]\[9\]

### 網頁瀏覽器

[Google_earth_acid3.png](https://zh.wikipedia.org/wiki/File:Google_earth_acid3.png "fig:Google_earth_acid3.png")更新後，Google
Earth 6.1.0.5001 只有99分。\]\]

  - Google
    Earth內有自家[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")，雖然速度並不快，但也達[Acid3標準](../Page/Acid3.md "wikilink")。\[10\]\[11\]（於[Acid3更新後只有](../Page/Acid3.md "wikilink")99分。）

### 其他用途

除了查看卫星图片和上文介绍的基本用途外，Google地球在日常生活中还有许多其他用途。

  - 台灣[高雄警方為追查非法地下油行偵辦案件](../Page/高雄.md "wikilink")，避免偵查行動曝光及打草驚蛇而錯失機會，使用Google地球辦案，依據Google地球衛星照片及線民所提供地理資料，從照片拍攝找到借由藏身在無門牌號工地裡的油槽、油罐車所進行非法販油位置，因而循線到偵破地下油行。\[12\]
  - 可用于查看受灾地区，例如2010年1月12日海地发生[海地地震](../Page/2010年海地地震.md "wikilink")，Google于1月17日提供了最新的图像。

## 详细介绍

### 图像清晰度

[270
px](https://zh.wikipedia.org/wiki/File:Google_earth_default_interface_earth_and_stars.jpg "fig:270 px")
这裡的「[清晰度](../Page/清晰度.md "wikilink")」衡量的标准是观察到离地面最近且图像最清晰时所显示的“Eye
alt”（視覺海拔高度）数值。

  - 大多数地区的图像清晰度都在5[英里左右](../Page/英里.md "wikilink")。
  - 几乎每个国家的[首都和主要大城市都提供了较为清晰的图像](../Page/首都.md "wikilink")，基本拥有0.15英里（241.4016米）的清晰度。
  - [西方國家的](../Page/西方國家.md "wikilink")-{zh-hans:高清晰;zh-hant:高清}-图像比较多，其他地区則较少。

### 地形

  - 地表：在开启了「Terrain」效果的情况下可以观察到以[3D方式显示的](../Page/3D.md "wikilink")[高原](../Page/高原.md "wikilink")、[山地等地形](../Page/山地.md "wikilink")。3D地形功能預設為開啟。
  - 海床：2009年2月2日，Google公司宣布开始海底地形服务，用户可观察到海面下的地形。但在实际应用中海底地形细节尚经不起推敲，经常出现海岛海拔为负值，被“淹没”在海平面下的情况

### 影像年份

大部份國家的首都和大城市影像是在2004年後拍攝。部分大都市可以有20世纪末的历史影像（例如[上海](../Page/上海.md "wikilink")），极少数地区（例如美国一些地区、中国[深圳与](../Page/深圳.md "wikilink")[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")）的历史影像可以追溯到1970年代，一些歐洲城市的衛星影像可追溯至二戰時期。
[Google_Earth.png](https://zh.wikipedia.org/wiki/File:Google_Earth.png "fig:Google_Earth.png")

### 系统配置

Google
Earth不能在配備較低的-{zh-hans:计算机;zh-hk:電腦;zh-sg:計算機;zh-tw:電腦;}-上使用。下面的數據是軟件所需的最低配置：

  - [操作系统](../Page/操作系统.md "wikilink")：[Windows
    2000](../Page/Windows_2000.md "wikilink")，[XP](../Page/Windows_XP.md "wikilink")，[Vista](../Page/Windows_Vista.md "wikilink")，[7](../Page/Windows_7.md "wikilink")，[8](../Page/Windows_8.md "wikilink")，[10](../Page/Windows_10.md "wikilink")，[Mac
    OS X](../Page/Mac_OS_X.md "wikilink")
    (10.6)，[Linux](../Page/Linux.md "wikilink") 2.4
  - [CPU](../Page/CPU.md "wikilink")：500 MHz
  - [硬盘剩余空间](../Page/硬盘.md "wikilink")：500 MB
  - [主記憶體](../Page/主記憶體.md "wikilink")：512 MB
  - [顯示卡](../Page/顯示卡.md "wikilink")[記憶體](../Page/記憶體.md "wikilink")：64
    MB
  - [螢幕](../Page/螢幕.md "wikilink")[分辨率](../Page/分辨率.md "wikilink")：1024x768,
    16位颜色质量
  - [网络](../Page/网络.md "wikilink")：128
    Kbps（[宽带](../Page/宽带.md "wikilink")）

## 图层

Google地球有许多图层，包含商业网点、有趣的地点和网络社区等内容，如[维基百科](../Page/维基百科.md "wikilink")，[Panoramio和](../Page/Panoramio.md "wikilink")[YouTube](../Page/YouTube.md "wikilink")。

### 地理資訊

  - 照片：在地圖中相應位置显示[Panoramio网页上的照片](../Page/Panoramio.md "wikilink")
  - 位置：显示世界各地一些著名之处的概述，内容一般来源于Google地球社区和维基百科\[13\]
  - 预览：显示浏览地方的一些内容简短的摘要

### 道路

显示道路网。[道路显示的](../Page/道路.md "wikilink")[颜色与道路等级有关](../Page/颜色.md "wikilink")。

  - 一般情况下，各国[高速公路标为淡橙色线](../Page/高速公路.md "wikilink")。
  - 日本的一些道路显示为靛蓝。
  - 其他重要的道路也以橙色标记。
  - 一般道路标为白色。
  - 部分小路标为[透明的白线](../Page/透明.md "wikilink")。

### 交通

显示交通状况实时监控道路的车流量。绿色表示交通状况较好，黄色表示车速较慢，红色表示交通状况较差。

### 公共交通

标示地铁站和路线。

### 社區設施

標示社區設施位置，包括學校、圖書館、郵政局、商場、公園、運動場、醫院、政府建築物，甚至便利商店和餐廳等。

### 3D建筑

[3D_locations_in_Google_Earth.png](https://zh.wikipedia.org/wiki/File:3D_locations_in_Google_Earth.png "fig:3D_locations_in_Google_Earth.png")

显示3D建筑物，在[北美](../Page/北美洲.md "wikilink")、[歐洲和](../Page/歐洲.md "wikilink")[日本有較多](../Page/日本.md "wikilink")3D建築物；在其他國家則有少量集中於大城市裡。3D建筑有兩種表现类型：

  - 仿真：显示彩色、逼真的建筑物模型
  - 3D灰色简图：不具有逼真的效果，只是外形与实际建筑相同，適合在配備較低或運算速度不高的電腦上使用

### 街景视图

显示由Google街景車所拍攝之街道360度全景照片。

### 边界和地名

显示各国的边界和地名（包括省、市、镇等）。

  - [边界](../Page/边界.md "wikilink")：國界和海岸线用黄色标记，一级行政区界线用紫色标记，二級行政區界線用綠色標記，爭議中界線用紅色標記
  - 人口聚集地：显示[城市](../Page/城市.md "wikilink")、[城镇](../Page/城镇.md "wikilink")、[村庄等](../Page/村庄.md "wikilink")，以紅色圓點表示
  - 地点别名：标记当地语言的地名
  - 地名：显示地名、[海洋](../Page/海洋.md "wikilink")、[海湾](../Page/海湾.md "wikilink")、[河流等](../Page/河流.md "wikilink")

### 气象

  - 云层：显示位于[近地轨道和](../Page/近地轨道.md "wikilink")[地球静止轨道](../Page/地球静止轨道.md "wikilink")[人造卫星拍摄的云层信息](../Page/人造卫星.md "wikilink")\[14\]
  - 雷达：显示weather网站上的气象雷达\[15\]
  - 状况及预报：显示天气预报\[16\]

### 图片库

  - 探索（Discovery）网络：显示[探索频道提供的地理信息](../Page/探索频道.md "wikilink")
  - [欧洲空间局](../Page/欧洲空间局.md "wikilink")：显示欧洲空间局的许多卫星图像
  - Gigapan Photos
  - Gigapxl Photos
  - Google地球 社区
  - [美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")：显示美国国家航空航天局的许多卫星图像
  - [国家地理杂志](../Page/国家地理杂志.md "wikilink")
  - Rumsey历史地图：显示Rumsey收集的历史地图
  - 旅行观光
      - 100%纯净的[新西兰](../Page/新西兰.md "wikilink")
      - [埃及观光](../Page/埃及.md "wikilink")
      - 日本旅游
      - [京都之旅](../Page/京都.md "wikilink")
      - [南非之旅](../Page/南非.md "wikilink")
      - 韩国旅游
      - [阿尔卑斯滑雪场](../Page/阿尔卑斯.md "wikilink")
  - Trimble户外运动
  - [火山](../Page/火山.md "wikilink")
  - Webcams.travel：整合Webcams实时[摄像头](../Page/摄像头.md "wikilink")
  - [YouTube](../Page/YouTube.md "wikilink")：整合YouTube上的对应视频
  - Wikiloc

### 海洋

  - 海洋探索
  - [国家地理](../Page/国家地理.md "wikilink")
      - 杂志小测试
      - 海洋地图集
  - [BBC地球](../Page/BBC.md "wikilink")
  - 库斯托海洋世界
  - 海洋运动
      - 冲浪点
      - 潜水点
      - 风筝冲浪点
  - [海滩](../Page/海滩.md "wikilink")
  - 海洋考察
  - [海洋保护区](../Page/海洋保护区.md "wikilink")
  - [ARKive](../Page/ARKive.md "wikilink")：濒危海洋生物
  - 海洋状况
  - 沉船
  - 动物追踪系统
  - 国际海洋生物普查计划
  - Marie Tharp历史地图

### 更多

  - 全球意识
  - 景点

## 版本歷史

### 版本及发布时间

[KmlHistoryTimeline.png](https://zh.wikipedia.org/wiki/File:KmlHistoryTimeline.png "fig:KmlHistoryTimeline.png")

  - Keyhole Earthviewer 1.0 - 2001年6月11日
  - Keyhole Earthviewer 1.4 - 2002年
  - Keyhole Earthviewer 1.6 - 2003年2月
  - Keyhole LT 1.7.1 - 2003年8月26日
  - Keyhole NV 1.7.2 - 2003年10月16日
  - Keyhole 2.2 - 2004年8月19日
  - Google地球3.0 - 2005年6月28日
  - Google地球4.0 - 2006年6月11日
  - Google地球4.1 - 2007年5月9日
  - Google地球4.2 - 2007年8月23日
  - Google地球4.3 - 2008年4月15日
  - Google地球5.0 - 2009年5月5日
  - Google地球5.1 - 2009年9月17日
  - Google地球5.2 - 2010年5月2日
  - Google地球6.0 - 2010年12月1日
  - Google地球7.0 - 2012年6月27日（iOS與Android版）
  - Google地球7.0 - 2012年12月13日（桌面版）
  - Google地球7.1 - 2013年6月6日（Android版）
  - Google地球7.1 - 2013年6月21日（iOS與桌面版）
  - Google地球8.0 - 2014年10月24日（Android版）
  - Google地球9.0 - 2017年4月26日（Android版與Chrome版）

### Mac OS X版

### Linux版

Google Earth [Linux版的第一个版本是](../Page/Linux.md "wikilink")4.0
beta，使用[Qt原生移植到Linux](../Page/Qt.md "wikilink")。为遵循[数字版权管理](../Page/数字版权管理.md "wikilink")，授权为[专有软件](../Page/专有软件.md "wikilink")。

### iOS版

Google Earth [iOS版于](../Page/iOS.md "wikilink")2008年10月27日在[App
Store中上架供免费下载](../Page/App_Store.md "wikilink")，同时支持[iPhone和](../Page/iPhone.md "wikilink")[iPod
Touch](../Page/iPod_Touch.md "wikilink")。\[17\]\[18\]支持[iPad](../Page/iPad.md "wikilink")（非兼容模式）的版本则于2010年6月15日开放下载。\[19\]他们都使用了[多点触摸界面来移动虚拟地球](../Page/多点触摸.md "wikilink")，缩放或旋转视图，也允许用iPhone内置的[A-GPS来选定当前位置](../Page/A-GPS.md "wikilink")。然而目前的版本都没有桌面版的完整图层。像Google
Maps一样，他们只集成了维基百科和Panoramio图层。\[20\]

### Google Earth Plus

Google Earth Plus是一個付費升級的增强版（2008年12月起已停售），包含以下額外的功能：

  - [GPS綜合](../Page/GPS.md "wikilink")–通過[GPS驅動查看道路和建築](../Page/GPS.md "wikilink")。
  - 更高的打印清晰度。
  - 通過E-mail的技術支持。
  - 注釋–添加了能提供更詳盡注釋的工具。
  - 數據導入–從[CSV文件讀取地址信息](../Page/CSV.md "wikilink")。

### Google Earth Pro

Google Earth Pro針對商用的付費专业版，功能強於Google Earth
Plus。該版本支持[插件功能](../Page/插件.md "wikilink")，2015年6月起已免费向公众开放。

### Google Earth Enterprise

Google Earth Enterprise是提供大量购买授权使用的企业版。

## 争议

### 国家安全问题

  - [印度前](../Page/印度.md "wikilink")[总统](../Page/印度总统.md "wikilink")[阿卜杜尔·卡拉姆表示对印度敏感地区高分辨率照片的关注](../Page/阿卜杜尔·卡拉姆.md "wikilink")，\[21\]Google后来表示同意审查印度敏感地区。
  - [印度空间研究组织发表声明称](../Page/印度空间研究组织.md "wikilink")，Google地球构成了对印度安全的威胁，并寻求与Google公司对话。\[22\]
  - 中国政府称，Google地球存在泄密。\[23\]Google公司解释称，Google地球使用的是任何公司都可以购买的商业卫星图片，不存在“机密”一说。同时，除非有特殊缘由，使用的图片一般为数月到数年之前的历史图片，而非实时观测。\[24\]中国當局相关部门将会推出“影像中国”进行[反制](../Page/反制.md "wikilink")，同时推出国产“Google
    Earth”──中华人民共和国国家地理信息公共服务平台建设工程。\[25\]
  - [韩国政府对该软件提供了](../Page/韩国.md "wikilink")[总统府和军事设施的图像表示关注](../Page/总统府.md "wikilink")，称[朝鲜政府可能加以利用](../Page/朝鲜.md "wikilink")。\[26\]
  - [摩洛哥的主要互联网服务提供商摩洛哥电信公司长期屏蔽谷歌地球信息的接入](../Page/摩洛哥.md "wikilink")，\[27\]自2006年8月以来未公开表示原因。
  - 2007年7月，被拍摄的[中国海军](../Page/中国人民解放军海军.md "wikilink")[094型核潜艇在](../Page/094型核潜艇.md "wikilink")[大连附近海域被用户发现](../Page/大连.md "wikilink")。\[28\]
  - [2008年孟买连环恐怖袭击事件参与攻击的唯一一名幸存枪手承认](../Page/2008年孟买连环恐怖袭击.md "wikilink")，他们在事件前曾使用Google地球了解被攻击建筑物附近的情况。\[29\]

### 其他問題

  - 安裝Google地球的新版本時，會自動下載[Google公司提供的](../Page/Google公司.md "wikilink")[軟件升級程序](../Page/Google_Updater.md "wikilink")。\[30\]
  - Google地球linux版本對於對於中文顯示的部份會變成方塊字或模糊。但在最近的版本中，問題已解決。
  - Google地球的地标有时被部分网友用作政治用具，例如有来自[中国大陆的網友在台灣](../Page/中国大陆.md "wikilink")[玉山山頂插上](../Page/玉山.md "wikilink")[五星紅旗](../Page/中華人民共和國國旗.md "wikilink")；\[31\]也有一些日本網友在中國東北標示[滿洲國地名等](../Page/滿洲國.md "wikilink")。\[32\]

## 中国大陆封锁

2012年3月29日起，Google地球海外服务器的多个IP地址被[防火长城封锁](../Page/防火长城.md "wikilink")，在[中国大陆境内的用户开始经常无法直接访问](../Page/中国大陆.md "wikilink")，但由於Google的IP地址众多，用户可变更[Hosts文件来突破封锁问题](../Page/Hosts文件.md "wikilink")。\[33\]Google在更換IP後中國可重新直接使用。

## 参考文献

## 外部連結

  - [Google地球](https://www.google.com/earth/)
      - [简体](https://www.google.com/intl/zh-CN/earth/)
      - [繁體](https://www.google.com/intl/zh-TW/earth/)

## 参见

  - [Google产品列表](../Page/Google产品列表.md "wikilink")
  - [Google地圖](../Page/Google地圖.md "wikilink")
  - [NASA世界風](../Page/世界風.md "wikilink")
  - [Google月球](../Page/Google月球.md "wikilink")
  - [Google火星](../Page/Google火星.md "wikilink")
  - [Google星空](../Page/Google星空.md "wikilink")
  - [Google街景视图](../Page/Google街景视图.md "wikilink")
  - [电子地图服务](../Page/电子地图服务.md "wikilink")
  - [Marble (KDE)](../Page/Marble_\(KDE\).md "wikilink")

{{-}}

[Google地球](../Category/Google地球.md "wikilink")
[Category:虛擬地球](../Category/虛擬地球.md "wikilink")
[Category:电子地图](../Category/电子地图.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")
[Category:Android软件](../Category/Android软件.md "wikilink")

1.

2.

3.

4.  <http://www.flickr.com/photos/kateshanley/2811857695/in>
    /pool-googlestreetviewcar

5.

6.

7.

8.

9.

10.

11. [Result of Acid3 rendering on Google
    Earth](../Page/Acid3.md "wikilink")

12.

13.

14. Google Earth: Weather layer, information link -- accessed: 03 March
    2009 v5.0.11337.1968 (beta)

15.
16.
17.

18.

19.

20.

21.

22.

23.

24. [谷歌地球全球搜寻卫星图片全力支持政府赈灾部署──全部图片已向网友开放](http://www.google.com.hk/ggblog/googlechinablog/2008/05/blog-post_5331.html)


25.

26.

27. <https://web.archive.org/web/20070601153059/http://motic.blogspot.com/2007/05/message-au-monde-message-to-world.html>
    Message au monde - Message to the world

28.

29. <http://www.telegraph.co.uk/news/worldnews/asia/india/3691723/Mumbai-attacks-Indian-suit-against-Google-Earth-over-image-use-by-terrorists.html>
    "Mumbai attacks: Indian suit against Google Earth over image use by
    terrorists", The Daily Telegraph, December 9, 2008.

30.

31.

32. [Google Earth瀋陽 現偽滿洲國時期日文標記](http://blog.myspace.cn/e/403237123.htm)

33.