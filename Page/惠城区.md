**惠城区**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[惠州市的一个市辖区](../Page/惠州市.md "wikilink")，位于[广东省东南部](../Page/广东省.md "wikilink")，是惠州市政府所在地，是惠州市的政治、经济、文化和交通中心。东部与[河源市](../Page/河源市.md "wikilink")[紫金县毗邻](../Page/紫金县.md "wikilink")，南部与[惠阳区](../Page/惠阳区.md "wikilink")、[惠东县交界](../Page/惠东县.md "wikilink")，西部与[东莞市相连](../Page/东莞市.md "wikilink")，北部与[博罗县相邻接壤](../Page/博罗县.md "wikilink")。总面积为1410平方公里。2002年人口为92万。

## 行政区划

  - 辖

<!-- end list -->

  - 10个街道：[龙丰街道](../Page/龙丰街道.md "wikilink")、[桥东街道](../Page/桥东街道.md "wikilink")、[桥西街道](../Page/桥西街道.md "wikilink")、[江南街道](../Page/江南街道.md "wikilink")、[江北街道](../Page/江北街道.md "wikilink")、[惠环街道](../Page/惠环街道.md "wikilink")、[河南岸街道](../Page/河南岸街道.md "wikilink")、[陈江街道](../Page/陈江街道.md "wikilink")、[水口街道](../Page/水口街道.md "wikilink")、[小金口街道](../Page/小金口街道.md "wikilink")
  - 8个镇：[汝湖鎮](../Page/汝湖鎮.md "wikilink")、[三栋镇](../Page/三栋镇.md "wikilink")、[潼湖镇](../Page/潼湖镇.md "wikilink")、[潼侨镇](../Page/潼侨镇.md "wikilink")、[沥林镇](../Page/沥林镇.md "wikilink")、[马安镇](../Page/马安镇.md "wikilink")、[横沥镇](../Page/横沥镇.md "wikilink")、[芦洲镇](../Page/芦洲镇.md "wikilink")。

## 历史

[宋朝](../Page/宋朝.md "wikilink")[天禧四年](../Page/天禧.md "wikilink")(1020年)由祯州改为惠州，始有惠州。

1963年始设立惠阳专区、惠阳地区。1988年撤销惠阳地区，分拆为四个地级市，原惠州市改称惠城区，为新惠州市的政府所在地。　　

## 民族

惠城本地居民都是[汉人](../Page/汉人.md "wikilink")。從前秦時期就有越人生活在東江兩岸，之後有秦人，珠璣巷移民，客家移民，官話移民先後遷入。虽然处在客家地区的包围之中，原城区居民并不认为自己是客家人，周围的客家人也不认为他们是客家。但郊区居民和后来的新移民，有普遍的客家认同。

## 语言

在[改革开放前](../Page/改革开放.md "wikilink")，惠城区的老城区的原居民的通用语言是[惠州话](../Page/惠州话.md "wikilink")，郊区的语言有[鶴佬話](../Page/鶴佬話.md "wikilink")、[客家话](../Page/客家话.md "wikilink")、[畲话等](../Page/畲话.md "wikilink")。1980年代后，由于大量的外地人迁入，而且以周边的客家人为主，客家话也得以通行。虽然受广州、香港的一些影响，但是[粤语一直没有占主导地位](../Page/粤语.md "wikilink")。由于惠州话、客家话、粤语互不相同，[推普工作比较容易](../Page/推普.md "wikilink")，普通话成为了惠城区的通用语言，这点和其他广东老城有所不同。

## 经济

惠城区各项基础设施包括电力供应、淡水资源、通讯交通等都非常完善和丰富。惠城区的[工业以](../Page/工业.md "wikilink")[外向型经济为主](../Page/外向型经济.md "wikilink")，涉外企业的比重超过70%，服装纺织、鞋帽、箱包玩具等[制造业极具规模](../Page/制造业.md "wikilink")。惠城区的市区重点发展[第三产业](../Page/第三产业.md "wikilink")，市郊重点发展[工业和](../Page/工业.md "wikilink")[农业](../Page/农业.md "wikilink")，形成一个成熟的“双环”经济格局。
[huizhou004.jpg](https://zh.wikipedia.org/wiki/File:huizhou004.jpg "fig:huizhou004.jpg")

## 旅游

[中山纪念堂_6072.jpg](https://zh.wikipedia.org/wiki/File:中山纪念堂_6072.jpg "fig:中山纪念堂_6072.jpg")惠城区酒店林立，區内有[惠州西湖](../Page/惠州西湖.md "wikilink")，是[国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")，是一個地處鬧市的天然湖。歷史上，惠州西湖曾和[杭州西湖](../Page/杭州西湖.md "wikilink")、[潁州西湖合稱為中國的三大西湖](../Page/潁州西湖.md "wikilink")。

[歸善學宮](../Page/歸善學宮.md "wikilink")，建於[元朝泰定元年](../Page/元朝.md "wikilink")(1324年)，明清兩代曾先後修建26次，是省級文明保護單位。

明城牆，[明太祖洪武十八年](../Page/明太祖.md "wikilink")(1385年)，因沿海盜賊猖獗，遂下令在惠州境內的平海建造城池。此城牆一直保留到現在，目前為廣東省歷史文化名城。

中山紀念堂，是廣東省三座中山紀念雀之一，始建於1937年，2007年進行過大規模復修。

[汝湖鎮政府附近上廟](../Page/汝湖鎮.md "wikilink")，是1907年，[同盟會成員](../Page/同盟會.md "wikilink")[鄧子瑜發動](../Page/鄧子瑜.md "wikilink")「[七女湖起義](../Page/七女湖起義.md "wikilink")」舊址，惠州市政府於樹立了紀念碑，碑石背面刻有七女湖起義經過\[1\]。

東征遺址，黄埔军官学校东征阵亡烈士纪念碑，1930年建於主要战场拱北桥西侧，后被拆，1992年惠州市人民政府按原样重建。碑座上刻有第二次东征241名阵亡烈士芳名。

鄧演達紀念園，在[鄧演達故居基礎上興建](../Page/鄧演達.md "wikilink")，2010年9月正式開放。

## 交通

惠城区交通便利，[惠深高速公路](../Page/惠深高速公路.md "wikilink")、[广惠高速公路](../Page/广惠高速公路.md "wikilink")、[惠河高速公路](../Page/惠河高速公路.md "wikilink")、[潮莞高速公路](../Page/潮莞高速公路.md "wikilink")、[惠澳高速公路](../Page/惠澳高速公路.md "wikilink")、[广梅汕铁路](../Page/广梅汕铁路.md "wikilink")、[京九铁路和](../Page/京九铁路.md "wikilink")[莞惠城际轨道均贯穿全区](../Page/莞惠城际轨道.md "wikilink")。区内还建有[惠州机场和惠州港](../Page/惠州机场.md "wikilink")，陆、水、空交通都非常方便。

## 外部链接

  - [惠城之窗](http://www.hcq.gov.cn/)

## 参考文献

[惠城区](../Page/category:惠城区.md "wikilink")
[区](../Page/category:惠州区县.md "wikilink")

[惠州市](../Category/广东市辖区.md "wikilink")

1.  [500年古榕樹見證惠州七女湖起義](http://www.hkcna.hk/content/2011/0904/111914.shtml)中新社