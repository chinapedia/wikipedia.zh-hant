**女滿別町**（）為過去位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[網走支廳東部的行政區劃](../Page/網走支廳.md "wikilink")，轄區有三分之一的區域為[網走湖](../Page/網走湖.md "wikilink")，受到[鄂霍次克海的影響](../Page/鄂霍次克海.md "wikilink")，氣溫低，雨量少。已於2006年3月31日與[東藻琴村](../Page/東藻琴村.md "wikilink")[合併為新設立的](../Page/市町村合併.md "wikilink")[大空町](../Page/大空町.md "wikilink")。

名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「mem-an-pet」，意思為有泉池的河川。

## 歷史

  - 1890年：因伐木業在此建立工作用小屋，開始有人居住。\[1\]
  - 1898年：開始有農業開墾為目的的居民。
  - 1912年：[北見到](../Page/北見.md "wikilink")[網走之間的鐵路開通](../Page/網走.md "wikilink")，並設有[女滿別車站](../Page/女滿別車站.md "wikilink")，使得開墾者增加，並形成聚落。
  - 1921年4月1日：從網走町（現在的[網走市](../Page/網走市.md "wikilink")）分村設置為女滿別村。\[2\]
  - 1951年4月1日：改制為女滿別町。
  - 2006年3月31日：與東藻琴村合併為新設立的「大空町」。

## 產業

以農業和湖泊漁業為主。

轄區面積的50%為農業用地，主要農作包括[麥](../Page/麥.md "wikilink")、[馬鈴薯](../Page/馬鈴薯.md "wikilink")、[甜菜](../Page/甜菜.md "wikilink")，近年來在向[花卉](../Page/花卉.md "wikilink")、健康蔬菜和肉牛飼料等高收益性的農作發展。

由於轄區內有[網走湖](../Page/網走湖.md "wikilink")，因此有以網走湖為漁場的漁業，漁獲包括[蜆貝](../Page/蜆貝.md "wikilink")、[銀魚](../Page/銀魚.md "wikilink")、[鯉魚](../Page/鯉魚.md "wikilink")，每年九月有舉辦銀魚節。

## 交通

### 機場

  - [女滿別機場](../Page/女滿別機場.md "wikilink")

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [石北本線](../Page/石北本線.md "wikilink")：[西女滿別車站](../Page/西女滿別車站.md "wikilink")
        - [女滿別車站](../Page/女滿別車站.md "wikilink")

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<ul>
<li>北見網走自動車道：女滿別機場交流道</li>
</ul>
<dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道39號</li>
<li>國道334號</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a>
<ul>
<li>北海道道64號女滿別空港線</li>
<li>北海道道104號網走端野線</li>
</ul></li>
<li>一般道道
<ul>
<li>北海道道246號小清水女滿別線</li>
<li>北海道道248號嘉多山美幌線</li>
<li>北海道道249號福住女滿別線</li>
<li>北海道道714號住吉女滿別停車場線</li>
<li>北海道道1168號女滿別空港內部線</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

  - [網走國定公園](../Page/網走國定公園.md "wikilink")
  - 女滿別町濕生植物群落：國家天然記念物『水芭蕉大群落』
  - [朝日丘公園](../Page/朝日丘公園_\(大空町\).md "wikilink")：[黑澤明導演的電影](../Page/黑澤明.md "wikilink")「夢」拍攝場景之一。

## 教育

### 高等學校

  - 道立北海道女滿別高等學校

### 中學校

  - 女滿別町立女滿別中學校

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>女滿別町立女滿別小學校</li>
</ul></td>
<td><ul>
<li>女滿別町立豐住小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [稻城市](../Page/稻城市.md "wikilink")（[東京都](../Page/東京都.md "wikilink")）

## 本地出身的名人

  - [松樹路人](../Page/松樹路人.md "wikilink")：[西洋畫家](../Page/西洋畫家.md "wikilink")、[武藏野美術大學](../Page/武藏野美術大學.md "wikilink")[名譽教授](../Page/名譽教授.md "wikilink")、獨立美術協會會員。
  - [神田山陽](../Page/神田山陽.md "wikilink")：日本說書人、落語藝術協會會員。

## 外部連結

  - [女滿別町・東藻琴村合併協議會](http://www.town.ozora.hokkaido.jp/gappeimh/index.html)

  - [女滿別町・東藻琴村任意合併協議會](http://www.town.ozora.hokkaido.jp/gappeimh/nini/index.html)

  - [女滿別町農會](https://web.archive.org/web/20070209010231/http://www.memanbetsu.hotcn.ne.jp/)

  - [女滿別青少年育成事業協會](http://www.town.ozora.hokkaido.jp/ikusei/index.html)

  - [社會福祉法人女滿別福祉會](http://business4.plala.or.jp/dream/)

## 參考資料

<div style="font-size: 85%">

<references />

</div>

[Category:日本已廢除的町](../Category/日本已廢除的町.md "wikilink")
[Category:鄂霍次克管內](../Category/鄂霍次克管內.md "wikilink")
[Category:大空町](../Category/大空町.md "wikilink")

1.
2.