**吳俊才**（），[台灣](../Page/台灣.md "wikilink")[歷史學家](../Page/歷史學.md "wikilink")、[印度史專家](../Page/印度史.md "wikilink")。

吳俊才曾任[國立臺灣大學教授](../Page/國立臺灣大學.md "wikilink")，主持[國立政治大學國際關係研究中心](../Page/國立政治大學國際關係研究中心.md "wikilink")，曾任[中国国民党中央委员会副秘书长](../Page/中国国民党中央委员会.md "wikilink")，尤精于[印度史](../Page/印度史.md "wikilink")，一生兩袖清風、住在普通[公寓裡](../Page/公寓.md "wikilink")\[1\]，晚年任[中國電視公司](../Page/中國電視公司.md "wikilink")[董事長](../Page/董事長.md "wikilink")、[中國國民黨中央評議委員會委員](../Page/中國國民黨中央評議委員會.md "wikilink")、[中華民國總統府國策顧問等職](../Page/中華民國總統府國策顧問.md "wikilink")，[李敖的作品與](../Page/李敖.md "wikilink")[回憶錄中曾多次提及](../Page/回憶錄.md "wikilink")。著有《印度史》、《甘地与现代印度》等書，參修《雲五社會科學大辭典》。而《印度史》一書更是[香港高級程度會考歷史科課本](../Page/香港高級程度會考.md "wikilink")。

1996年6月10日，中國電視公司召開臨時[董事會](../Page/董事會.md "wikilink")，推選[鄭淑敏與](../Page/鄭淑敏.md "wikilink")[江奉琪分別出任董事長與](../Page/江奉琪.md "wikilink")[總經理](../Page/總經理.md "wikilink")，原董事長吳俊才與原總經理[石永貴卸任](../Page/石永貴.md "wikilink")；隔日，鄭淑敏與江奉琪正式上任。

吳俊才之妻[馬均權是](../Page/馬均權.md "wikilink")[馬星野之妹](../Page/馬星野.md "wikilink")、[吳涵碧之母](../Page/吳涵碧.md "wikilink")、[王壽南之岳母](../Page/王壽南.md "wikilink")，早年曾在[老三台主持](../Page/老三台.md "wikilink")[烹飪節目](../Page/烹飪節目.md "wikilink")。吳俊才旗下亦不少[學生投向影視界](../Page/學生.md "wikilink")。吳俊才之子[吳玉山是](../Page/吳玉山.md "wikilink")[政治學學者](../Page/政治學.md "wikilink")。

## 注釋

[Category:台灣歷史學家](../Category/台灣歷史學家.md "wikilink")
[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:中視董事長](../Category/中視董事長.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")

1.