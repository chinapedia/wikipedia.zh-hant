**Kayo Dot**是美國[前衛音樂團體](../Page/前衛音樂.md "wikilink")，由Toby
River於2003年成立。同年，他們在[John
Zorn的廠牌Tzadik下發行首張專輯](../Page/John_Zorn.md "wikilink")*Choirs
of the Eye*

## 歷年專輯

**maudlin of the Well Demos/Singles:**

  - *Through Languid Veins* - 1996 (Demo)
  - *Begat of the Haunted Oak: An Acorn* - 1997 (Demo)
  - *Odes to Darksome Spring* - 1997
  - *For My Wife* - 1998 (Demo)
  - *untitled secret song* - 2001 (released as a bonus track on the 2006
    version of LYBM)

**maudlin of the Well Full-length Albums:**

  - *My Fruit Psychobells...A Seed Combustible* (1999)
  - *Bath* (2001) (reissued with bonus tracks - 2006)
  - *Leaving Your Body Map* (2001) (reissued with bonus tracks - 2006)

**Kayo Dot Full-length Albums:**

  - *Choirs of the Eye* (2003)
  - *Dowsing Anemone With Copper Tongue* (2006)

**Kayo Dot Splits:**

  - *Kayo Dot / Bloody Panda Split 12" Vinyl* (2006)

**Toby Driver Solo Albums:**

  - *In The L..L..Library Loft* (2006)

## 外部連結

  - [Kayo Dot的官方網站](http://www.kayodot.net)
  - [Kayo
    Dot的官方論壇](http://www.ultimatemetal.com/forum/forumdisplay.php?f=170t)
  - [Kayo Dot myspace](http://www.myspace.com/kayodot)
  - [Toby
    Driver專訪](https://web.archive.org/web/20031205140646/http://www.indieworkshop.com/interviews/39/)
  - [Toby Driver專訪
    progarchives.com](http://www.progarchives.com/forum/forum_posts.asp?TID=19803&PN=1)

[Category:前衛搖滾](../Category/前衛搖滾.md "wikilink")
[Category:美國樂團](../Category/美國樂團.md "wikilink")
[Category:實驗音樂樂團](../Category/實驗音樂樂團.md "wikilink")