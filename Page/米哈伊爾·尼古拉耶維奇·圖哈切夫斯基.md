[5marshals_01.jpg](https://zh.wikipedia.org/wiki/File:5marshals_01.jpg "fig:5marshals_01.jpg")

**米哈伊爾·尼古拉耶維奇·图哈切夫斯基**（'''，），[苏联红军](../Page/苏联红军.md "wikilink")[总参谋长](../Page/总参谋长.md "wikilink")（1925年11月13日－1928年5月5日）、[苏联元帅](../Page/苏联元帅.md "wikilink")，为苏联军事理论[纵深作战作出重大贡献](../Page/纵深作战.md "wikilink")。在1937年的[图哈切夫斯基案件中](../Page/图哈切夫斯基案件.md "wikilink")，他被指控参与“反苏阴谋”被秘密逮捕判处死刑，并立即枪决。

## 生平

1893年2月16日，图哈切夫斯基出生于[斯摩棱斯克一个](../Page/斯摩棱斯克.md "wikilink")[沙俄贵族家庭](../Page/沙俄.md "wikilink")。1909年迁居[莫斯科](../Page/莫斯科.md "wikilink")，后进入亚历山大军官学校；1914年7月毕业，获[少尉军衔](../Page/少尉.md "wikilink")，入军队服役。

[第一次世界大戰中](../Page/第一次世界大戰.md "wikilink")，图哈切夫斯基随军进入东线作战，曾六次獲奖。1915年被德军俘虏。在监狱中认识同为俘虏的[戴高乐](../Page/戴高乐.md "wikilink")。1917年返回苏联。

1918年，由[列夫·达维多维奇·托洛茨基介绍加入](../Page/列夫·达维多维奇·托洛茨基.md "wikilink")[俄国共产党（布尔什维克）](../Page/俄国共产党（布尔什维克）.md "wikilink")，任职于全俄苏维埃中央执行委员会军事部。6月，他受命筹建第一集团军，後率军在[俄国内战中立下战功](../Page/俄国内战.md "wikilink")。25岁时已任方面军司令。1920年初，图哈切夫斯基接任高加索战线司令，打赢叶戈尔雷克战役和北高加索战役。4月，图哈切夫斯基升任西部战线司令，与波兰军队作战；因斯大林及叶戈罗夫的推诿不援，失利。1921年，图哈切夫斯基担任第七集团军司令，镇压了国内的[喀琅施塔得起义和](../Page/喀琅施塔得起义.md "wikilink")[坦波夫起义](../Page/坦波夫起义.md "wikilink")。

32岁时，图哈切夫斯基就任[苏联红军](../Page/苏联红军.md "wikilink")[总参谋长](../Page/总参谋长.md "wikilink")，1935年获元帅军衔时仅42岁，其军事才华广为人所称道：[斯大林暱称他](../Page/斯大林.md "wikilink")“**小[拿破仑](../Page/拿破仑.md "wikilink")**”。\[1\]

[蔣經國在莫斯科時曾經向图哈切夫斯基學習軍事作戰及](../Page/蔣經國.md "wikilink")[游擊戰等戰術](../Page/游擊戰.md "wikilink")。

但不久後，图哈切夫斯基被卷入[苏共的](../Page/苏共.md "wikilink")[大清洗](../Page/大清洗.md "wikilink")。1937年的[图哈切夫斯基案件中](../Page/图哈切夫斯基案件.md "wikilink")，他以[间谍罪被捕](../Page/间谍罪.md "wikilink")；6月11日，以[伏罗希洛夫](../Page/伏罗希洛夫.md "wikilink")、[布琼尼](../Page/布琼尼.md "wikilink")、[布留赫尔](../Page/布留赫尔.md "wikilink")、[维辛斯基等人组成的特别军事法庭判处其死刑并即时枪决](../Page/维辛斯基.md "wikilink")。与他一并被枪决的还有前基辅特别军区司令、前白俄罗斯军区司令[乌博列维奇等七人](../Page/乌博列维奇.md "wikilink")。当他的判决书出来时，[斯大林正和](../Page/斯大林.md "wikilink")[莫洛托夫](../Page/莫洛托夫.md "wikilink")、[卡冈诺维奇及](../Page/卡冈诺维奇.md "wikilink")[叶若夫等待](../Page/叶若夫.md "wikilink")，根本不看内容就签字同意。\[2\]

直到1956年[苏共二十大](../Page/苏共二十大.md "wikilink")，才得以[平反](../Page/平反.md "wikilink")。

## 參考文獻

### 引用

### 来源

  - Иванов В. М. Маршал М. Н. Тухачевский. — 2. — М.: Воениздат, 1990. —
    320 с. — (Советские полководцы и военачальники). — ISBN
    978-5-203-00571-7
  - Грызун, В Как Виктор Суворов сочинял историю. — М.: Olma Media
    Group, 2003. — 606 с. — ISBN 978-5-224-04373-6
  - Помогайбо, А. А. Вырванный меч империи 1925-1940 гг.. — М.: Вече,
    2006. — 574 с. — ISBN 978-5-9533-1336-0

## 外部連結

  - [馬克思主義百科全書](http://www.marxists.org/glossary/people/t/u.htm)
  - [The links in the chain of
    death](https://web.archive.org/web/20071231192827/http://english.mn.ru/english/issue.php?2002-44-12)
    (Polish historian Professor Pawel Wieczorkiewicz discusses the Red
    Army purges)
  - [The Tukhachevsky trial and the anti-Communist
    conspiracy](https://web.archive.org/web/20060602033230/http://www.plp.org/books/Stalin/node114.html)
    (Pro-Stalin)
  - [Tukhachevsky at Spartacus
    Schoolnet](https://web.archive.org/web/20060614161656/http://www.spartacus.schoolnet.co.uk/RUStukhachevsky.htm)
    (Trotskyist)
  - [紅軍的拿破崙](http://www.pwhce.org/rus/tukhachevsky.html)
  - [图哈切夫斯基在列寧格勒](http://www.findarticles.com/p/articles/mi_m3955/is_n8_v48/ai_19298461)

## 參見

  - [纵深作战](../Page/纵深作战.md "wikilink")
  - [大清洗](../Page/大清洗.md "wikilink")

{{-}}

[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:紅旗勳章獲得者](../Category/紅旗勳章獲得者.md "wikilink")
[Category:蘇聯元帥](../Category/蘇聯元帥.md "wikilink")
[Category:軍事學家](../Category/軍事學家.md "wikilink")
[Category:俄國內戰人物](../Category/俄國內戰人物.md "wikilink")
[Category:俄羅斯第一次世界大戰人物](../Category/俄羅斯第一次世界大戰人物.md "wikilink")
[Category:俄罗斯共产主义者](../Category/俄罗斯共产主义者.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:被處決的蘇聯人](../Category/被處決的蘇聯人.md "wikilink")
[Category:酷刑受害者](../Category/酷刑受害者.md "wikilink")
[Category:大整肅受難者](../Category/大整肅受難者.md "wikilink")
[Category:斯摩棱斯克州人](../Category/斯摩棱斯克州人.md "wikilink")
[Category:苏联的政治迫害](../Category/苏联的政治迫害.md "wikilink")
[Category:二级圣安娜勋章得主](../Category/二级圣安娜勋章得主.md "wikilink")

1.

2.