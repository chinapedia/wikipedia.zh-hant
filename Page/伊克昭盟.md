**伊克昭盟**（簡稱**伊盟**，），舊[盟名](../Page/盟.md "wikilink")，后成为[行政区名](../Page/行政区.md "wikilink")。

## 历史

[會盟七旗](../Page/會盟.md "wikilink")，均屬[鄂爾多斯部](../Page/鄂爾多斯部.md "wikilink")。此部原附於[察哈爾](../Page/察哈尔部.md "wikilink")，[林丹汗敗後降清](../Page/林丹汗.md "wikilink")。[順治時設為六旗](../Page/順治.md "wikilink")，左、右翼各分前、中、後。[乾隆時又增設一旗](../Page/乾隆.md "wikilink")，稱鄂爾多斯左翼前末旗，合共七旗。七旗各有自己的名稱，左翼前旗稱準噶爾旗，左翼中旗稱郡王旗，左翼後旗稱達拉特旗，左翼前末旗又名札薩克旗。右翼前旗為烏審旗，右翼中旗為鄂托克旗，右翼後旗為杭錦旗。

## 民国以后

1928年置。在今[内蒙古自治区西南部](../Page/内蒙古自治区.md "wikilink")。辖[东胜县及](../Page/东胜县.md "wikilink")[达拉特旗](../Page/达拉特旗.md "wikilink")、[郡王旗](../Page/郡王旗.md "wikilink")、[扎萨克旗](../Page/扎萨克旗.md "wikilink")、[准格尔旗](../Page/准格尔旗.md "wikilink")、[乌审旗](../Page/乌审旗.md "wikilink")、[鄂托克旗](../Page/鄂托克旗.md "wikilink")、[杭锦旗等旗](../Page/杭锦旗.md "wikilink")。行政公署驻东胜县（今[鄂尔多斯市](../Page/鄂尔多斯市.md "wikilink")[东胜区](../Page/东胜区.md "wikilink")）。[绥远省政府因抗日战争需要](../Page/绥远省政府.md "wikilink")，曾在伊克昭盟的[汉族民众聚居地设置](../Page/汉族.md "wikilink")[桃力民办事处](../Page/桃力民办事处.md "wikilink")、[达拉特旗组训处两个县级机构](../Page/达拉特旗组训处.md "wikilink")，形成两个县级行政区，但因伊克昭盟各旗抵制，至绥远省政府灭亡仍未能正式设[县](../Page/縣_\(中華民國\).md "wikilink")。

中华人民共和国建立初期仍属[绥远省](../Page/綏遠省_\(中華人民共和國\).md "wikilink")。1954年改属[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")。1958年郡王、扎萨克二旗合置[伊金霍洛旗](../Page/伊金霍洛旗.md "wikilink")；1961年分别设置[海勃湾市和](../Page/海勃湾市.md "wikilink")[乌达市](../Page/乌达市.md "wikilink")，1975年二市合并为[乌海市](../Page/乌海市.md "wikilink")；1980年鄂托克旗析置[鄂托克前旗](../Page/鄂托克前旗.md "wikilink")；1988年东胜撤县设[市](../Page/东胜市.md "wikilink")。2001年撤销伊克昭盟，改设[鄂尔多斯市](../Page/鄂尔多斯市.md "wikilink")（地级），东胜市改东胜区；原伊克昭盟所属各旗划归鄂尔多斯市。

## 参考文献

## 参见

  - [伊盟事变](../Page/伊盟事变.md "wikilink")

[Category:清朝的盟](../Category/清朝的盟.md "wikilink")
[Category:中华民国绥远省行政区划](../Category/中华民国绥远省行政区划.md "wikilink")
[Category:内蒙古自治区已撤消的盟](../Category/内蒙古自治区已撤消的盟.md "wikilink")
[Category:鄂尔多斯行政区划史](../Category/鄂尔多斯行政区划史.md "wikilink")
[Category:2001年废除的行政区划](../Category/2001年废除的行政区划.md "wikilink")