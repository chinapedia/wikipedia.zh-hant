**王敏超**（**Ronnie Wong Man
Chiu**，），原籍[廣東](../Page/廣東.md "wikilink")[東莞](../Page/東莞.md "wikilink")[石排鎮](../Page/石排鎮.md "wikilink")[橫山上寶潭村](../Page/橫山.md "wikilink")，前[香港](../Page/香港.md "wikilink")[游泳代表](../Page/游泳.md "wikilink")，[中華造船廠王氏家族成員之一](../Page/中華造船廠有限公司.md "wikilink")，為創辦人[王華生的二兒子](../Page/王華生.md "wikilink")，其兄為第10屆港區[全國人大代表](../Page/全國人大代表.md "wikilink")[王敏剛](../Page/王敏剛.md "wikilink")。

## 學歷

王敏超曾就讀[聖若瑟英文中學畢業於](../Page/聖若瑟英文中學.md "wikilink")[喇沙書院](../Page/喇沙書院.md "wikilink")，於[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[岩士唐大學](../Page/岩士唐大學.md "wikilink")（Armstrong
University）[工商管理系](../Page/工商管理.md "wikilink")（B.Sc）取得榮譽學士學位。其後又獲[佛羅里達州理工學院](../Page/佛羅里達州理工學院.md "wikilink")（Florida
Institute of Technology）工學院[工商管理碩士學位](../Page/工商管理碩士.md "wikilink")。

## 職務/榮譽

### 主要職務

  - 王氏投資(集團)有限公司董事
  - [中華造船廠有限公司執行董事](../Page/中華造船廠有限公司.md "wikilink")
  - 雅高企業(集團)有限公司執行董事、雅高巴士有限公司執行董事、南京公交雅高巴士有限公司董事長

### 歷任公職

  - [保良局](../Page/保良局.md "wikilink")：辛酉及壬戌年總理、癸亥及甲子年副主席、乙丑年主席及丙寅年顧問成員
  - [市政局](../Page/市政局.md "wikilink")：1986年（委任議員）、1995年（鴨脷洲區分區直選）
  - [臨時市政局](../Page/臨時市政局.md "wikilink")：1997年（委任議員）
  - [南區](../Page/南區_\(香港\).md "wikilink")[區議會](../Page/香港區議會.md "wikilink")：2000年（委任議員）
  - [賽馬會體藝中學](../Page/賽馬會體藝中學.md "wikilink")、[香港外展訓練學校校董](../Page/香港外展訓練學校.md "wikilink")
  - [香港學界體育聯會顧問](../Page/香港學界體育聯會.md "wikilink")
  - [鴨脷洲街坊福利會理事](../Page/鴨脷洲街坊福利會.md "wikilink")
  - [觀塘社區發展協會執行委員](../Page/觀塘社區發展協會.md "wikilink")
  - [香港漁業學會董事](../Page/香港漁業學會.md "wikilink")
  - [博愛醫院名譽顧問](../Page/博愛醫院.md "wikilink")
  - [中國香港體育協會暨奧林匹克委員會副秘書長](../Page/中國香港體育協會暨奧林匹克委員會.md "wikilink")
  - [國際泳協](../Page/國際業餘游泳聯合總會.md "wikilink")[公開水域游泳技術委員會](../Page/公開水域游泳.md "wikilink")[副主席](http://www.fina.org/directory/technical_Committees/TOWSC.php)
  - [香港業餘游泳總會秘書長](../Page/香港業餘游泳總會.md "wikilink")
  - [亞洲泳協副秘書長](../Page/亞洲泳協.md "wikilink")
  - [香港中華業餘游泳聯會執行委員](../Page/香港中華業餘游泳聯會.md "wikilink")
  - [香港浸會大學體育系顧問委員會主席](../Page/香港浸會大學.md "wikilink")
  - [淫褻物品審裁處審裁員](../Page/淫褻物品審裁處.md "wikilink")
  - [環境保護運動委員會主席](../Page/環境保護運動委員會.md "wikilink")：1996-2004年
  - [香港康體發展局成員](../Page/香港康體發展局.md "wikilink")：2003-04年度
  - [觀塘](../Page/觀塘區.md "wikilink")[區議員](../Page/區議員.md "wikilink")
  - [長刑期囚犯審查委員會委員](../Page/長刑期囚犯審查委員會.md "wikilink")：1981-1992年
  - [職業訓練局船舶建築業訓練委員會委員](../Page/職業訓練局.md "wikilink")
  - [香港社會服務聯會基金委員](../Page/香港社會服務聯會.md "wikilink")：1989-1991年
  - [青年事務委員會委員](../Page/青年事務委員會.md "wikilink")
  - [華人廟宇委員會委員](../Page/華人廟宇委員會.md "wikilink")
  - [銀禧體育中心管理局委員](../Page/香港體育學院.md "wikilink")
  - [度量衡十進制委員會機械工程小組主席](../Page/度量衡十進制委員會.md "wikilink")：1981-1989年
  - [香港青年聯會會董](../Page/香港青年聯會.md "wikilink")
  - [醫院管理局](../Page/醫院管理局.md "wikilink")[廣華醫院管治委員會委員](../Page/廣華醫院.md "wikilink")

### 歷任政職

  - [新香港聯盟第二任主席](../Page/新香港聯盟.md "wikilink")
  - [基本法諮詢委員會委員](../Page/基本法諮詢委員會.md "wikilink")：1981-1992年
  - [區事顧問](../Page/區事顧問.md "wikilink")：1994年

## 禁賽風波

王敏超曾於2003年擔任[香港業餘游泳總會游泳技術委員會主席期間](../Page/香港業餘游泳總會.md "wikilink")，下令禁止會員泳總參加[香港拯溺總會於](../Page/香港拯溺總會.md "wikilink")2004年舉辦的第廿八屆國際元旦冬泳錦標大賽，違規的會員將遭罰停賽一至兩年，掀起禁賽風波\[1\]。有溺總會員不滿，指泳總霸道，及以禁賽「壟斷香港游泳市場」，直斥王敏超扼殺泳手體育生涯。受到輿論壓力，王敏超才草草辯稱事件當中有誤會。

## 違反誠信

王敏超曾於2005年，被已故富商張祝珊的孫女張慧燊申請破產\[2\]。[張慧燊及其丈夫](../Page/張慧燊.md "wikilink")[阮北耀](../Page/阮北耀.md "wikilink")，於2005年以公司名義入稟高院，指[中華造船廠創辦人](../Page/中華造船廠有限公司.md "wikilink")[王華生兩名兒子](../Page/王華生.md "wikilink")[王敏智及王敏超兩兄弟](../Page/王敏智.md "wikilink")，於2001年擅自將張慧燊夫婦與王氏兄弟合作發展之東莞住宅物業的部分權益出售予第三者，違反誠信，使夫婦損失超過100萬元，因此要求王氏兄弟及其名下公司賠償及交代帳目。而王敏超本人，曾在[1991年香港立法局選舉中](../Page/1991年香港立法局選舉.md "wikilink")，與張慧燊以[新香港聯盟名義](../Page/新香港聯盟.md "wikilink")，搭檔出戰港島西選區落敗。

## 游泳運動員生涯

王敏超擅長[自由泳](../Page/自由泳.md "wikilink")，自[1966年亞運會開始](../Page/1966年亚洲运动会.md "wikilink")，出席過2屆[奧運會和](../Page/奧運會.md "wikilink")3屆[亞運會](../Page/亞運會.md "wikilink")\[3\]，於[1970年亞運會在](../Page/1970年亚洲运动会.md "wikilink")4X100米自由泳接力中為香港隊奪得第四名\[4\]。1972年後王敏超到海外升學，回港後轉投[水球運動](../Page/水球.md "wikilink")，更帶領香港水球隊出席[1978年亞運會](../Page/1978年亚洲运动会.md "wikilink")。王敏超曾獲得三屆[香港渡海泳的冠軍](../Page/香港渡海泳.md "wikilink")\[5\]。

代表香港出席的主要運動會：

  - [1966年第五屆亞洲運動會](../Page/1966年亚洲运动会.md "wikilink")（[泰國](../Page/泰國.md "wikilink")[曼谷](../Page/曼谷.md "wikilink")）
  - [1968年第十九屆奧林匹克運動會](../Page/1968年夏季奥林匹克运动会.md "wikilink")（[墨西哥](../Page/墨西哥.md "wikilink")[墨西哥城](../Page/墨西哥城.md "wikilink")）
  - 1970年[英聯邦運動會](../Page/英聯邦運動會.md "wikilink")（[英國](../Page/英國.md "wikilink")[愛丁堡](../Page/愛丁堡.md "wikilink")）
  - [1970年第六屆亞洲運動會](../Page/1970年亚洲运动会.md "wikilink")（泰國曼谷）
  - [1972年第二十屆奧林匹克運動會](../Page/1972年夏季奥林匹克运动会.md "wikilink")（[西德](../Page/西德.md "wikilink")[慕尼黑](../Page/慕尼黑.md "wikilink")）
  - [1978年第八屆亞洲運動會](../Page/1978年亚洲运动会.md "wikilink")（泰國曼谷）

### 慕尼黑惨案

1972年9月5日[巴解分子](../Page/巴勒斯坦解放组织.md "wikilink")「[黑色九月](../Page/黑色九月.md "wikilink")」（Black
September）潛入當時仍為[西德](../Page/西德.md "wikilink")[慕尼克的](../Page/慕尼克.md "wikilink")[奧運選手村](../Page/奥运村.md "wikilink")，先殺死2名[以色列運動員](../Page/以色列.md "wikilink")，再挾持餘下9名以色列運動員作為[人質](../Page/人質.md "wikilink")，要求以色列釋放234名[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")[政治犯](../Page/政治犯.md "wikilink")。

當時香港隊住在以色列隊駐紮的「I座」隔鄰的「H座」，因港隊只有不足30人，住不滿整座宿舍，而以色列隊不夠地方，就與港隊商量，借用「H座」底層的房間居住，港隊住樓上。在巴解挾持人質期間，當時年僅20歲的游泳代表王敏超與[柔道代表](../Page/柔道.md "wikilink")[莫卓榮及另一名](../Page/莫卓榮.md "wikilink")[劍擊運動員一起](../Page/劍擊.md "wikilink")，與門外的槍手說自己是[中國人](../Page/中國人.md "wikilink")，要求離開，對方通知樓下的首領，就批准三人帶同行李從正門離開\[6\]。

## 馬主生涯

王敏超曾於1998-1999馬季擁有名下馬匹「龍城之威」，並交由[呂健威訓練](../Page/呂健威.md "wikilink")，但該駒上陣六次皆未曾取得三甲，亦未能贏得獎金，最後宣布退役。

## 参考資料

<div class="references-small">

<references />

</div>

[分類:商人出身的政治人物](../Page/分類:商人出身的政治人物.md "wikilink")

[Category:香港游泳運動員](../Category/香港游泳運動員.md "wikilink")
[Category:香港奧林匹克運動會游泳選手](../Category/香港奧林匹克運動會游泳選手.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[W](../Category/香港馬主.md "wikilink")
[Category:前香港區議員](../Category/前香港區議員.md "wikilink")
[Category:前市政局議員](../Category/前市政局議員.md "wikilink")
[Category:香港十大傑出青年](../Category/香港十大傑出青年.md "wikilink")
[W](../Category/喇沙書院校友.md "wikilink")
[Category:岩士唐大學校友](../Category/岩士唐大學校友.md "wikilink")
[Category:佛羅里達州理工學院校友](../Category/佛羅里達州理工學院校友.md "wikilink")
[Category:香港東莞人](../Category/香港東莞人.md "wikilink")
[Category:東莞人](../Category/東莞人.md "wikilink")
[M](../Category/王姓.md "wikilink")

1.  [元旦冬泳鬧禁賽風波](http://orientaldaily.on.cc/cgi-bin/nsrch.cgi?seq=364164)
2.  [王敏超兄弟再遭張慧燊追債](http://life.mingpao.com/cfm/dailynews3b.cfm?File=20050507/nalge/gec1.txt)
3.  [海天體育會游泳部比賽名單及成績](http://www.hoitin.net/result19.html)
4.  [昔日泳手介紹](http://www.hkswim.com/exswimmer.html)
5.  [集體回憶: 渡海泳](http://www.hksca.org.hk/newsletter/may07newsletter.pdf)
6.  [慕尼黑惨案](http://ido.3mt.com.cn/pc/200603/20060319402484.shtm)