**仙人掌**是[石竹目](../Page/石竹目.md "wikilink")**仙人掌科**（[學名](../Page/學名.md "wikilink")：）的植物總稱，別名為**仙巴掌、仙人扇、霸王樹**。仙人掌具有相當豐富的形状和大小，並擁有優良的保水适应力，多數生長於[沙漠及半沙漠等乾燥少雨環境](../Page/沙漠.md "wikilink")，在地球上所有沙漠也可以發現。仙人掌為[多肉植物的一類](../Page/多肉植物.md "wikilink")，目前仙人掌科的植物有174属，多於2000種物種[Cactaceae
— The Plant List](http://www.theplantlist.org/browse/A/Cactaceae/)。

仙人掌用途廣泛，可作观赏植物、饲用或饲料，以及其他食物來源（特别是它们的果实​​）。
[胭脂紅是來自於生活在某些仙人掌的](../Page/胭脂紅.md "wikilink")[胭脂虫之产物](../Page/胭脂虫.md "wikilink")。

## 構造及特徵

面對沙漠缺[水和氣候的](../Page/水.md "wikilink")[適應](../Page/適應.md "wikilink")，仙人掌的[葉子退化成短短的小刺](../Page/葉子.md "wikilink")，以減少水份流失，亦能作為阻止動物吞食的武器。它們具有的肉質是「[多肉植物](../Page/多肉植物.md "wikilink")[莖](../Page/莖.md "wikilink")」,是[光合作用的主要器官](../Page/光合作用.md "wikilink")。其[莖演化為肥厚含水的形狀](../Page/莖.md "wikilink")，具有(刺座areole)代謝活性而且可長出刺狀葉，並可生出另一器官如莖（cladode）或果實。莖部表面有蠟質，以減少水分流失。15℃以下時，顏色黯淡，屬於休眠期，須保持乾燥。

仙人掌的[根與莖不同](../Page/根.md "wikilink")，是非肉質的，根羣分布淺（15\~30
cm），即使是樹狀仙人掌其根群主要分佈在地面下3cm以內，而某些生長於惡地的仙人掌反常地具有肥厚的儲藏根。仙人掌根系的覆蓋面積範圍非常之大，用以在下大雨時能吸收最多的雨水。當土壤乾燥時，細側根通常死亡，而較大的根轉為被[軟木栓層](../Page/軟木栓層.md "wikilink")（[皮層](../Page/皮層.md "wikilink")）覆蓋。主根皮層下的根原始體在土壤回濕後迅速生長，在數天之內即增加水及礦物之吸收能力。扁平仙人掌在其葉狀莖上之網孔接觸到地面即容易長根而繁殖。

多數仙人掌科植物都耐旱，但也有例外，例如生長在[巴西熱帶雨林中的](../Page/巴西.md "wikilink")[蟹爪蘭則需要蔭蔽](../Page/蟹爪蘭.md "wikilink")、潮濕的環境才能生長。

仙人掌為了適應乾燥的環境，因此營養方式和[凤梨](../Page/凤梨.md "wikilink")、[落地生根及](../Page/落地生根.md "wikilink")[长寿花一樣](../Page/长寿花.md "wikilink")，為行[景天酸代謝作用的碳固定方式](../Page/景天酸代謝.md "wikilink")。

**葉**：葉已退化為刺，無真正的葉（為了減少水分流失而退化為刺）仙人掌很大。

## 繁殖方法

仙人掌的繁殖方法有三種，分別為種子、子球、扦插：

  - 種子繁殖法：由開花結果而來，可以自然落入地面、動物(包含鳥類)吃食排泄等方式傳播。
  - 子球繁殖法：在母株上直接產生子球，子球入地面即可生根繼續生存。
  - 扦插繁殖法：一般人工繁殖的方式，可確保品質的一致。

## 用途

其肉質莖葉可其汁作飲料或煮食。仙人掌的果實可食，[火龍果就是一種仙人掌的果實](../Page/火龍果.md "wikilink")。

仙人掌也是觀景植物的一種。全世界培育仙人掌最有成績的國家是[中南美洲的](../Page/中南美洲.md "wikilink")[墨西哥](../Page/墨西哥.md "wikilink")，該國的[國徽上就有仙人掌的圖樣](../Page/墨西哥國徽.md "wikilink")。而[亞洲地區則以](../Page/亞洲.md "wikilink")[日本為代表](../Page/日本.md "wikilink")。

## 危害

在[美國及](../Page/美國.md "wikilink")[澳洲等地方](../Page/澳洲.md "wikilink")，仙人掌生長速度非常快速，表皮堅硬，無堅不摧的仙人掌往往会破壞牧場的圍欄，在一、兩年內就遍佈極其廣泛的地帶，同時搶奪其他可供放牧的植物的營養。所以部分地區會指明某些物種需要清除的[入侵植物](../Page/入侵物种.md "wikilink")。

<File:Notocactus> minimus.jpg|[阳炎城](../Page/阳炎城.md "wikilink") *Parodia
tenuicylindrica* <File:Cactus> in Mexico.jpg|一種墨西哥野生仙人掌，形狀與我們常見的品種差異很大
[File:Prickly_pears.jpg|产自墨西哥的一种可食用的仙人掌果实](File:Prickly_pears.jpg%7C产自墨西哥的一种可食用的仙人掌果实)
<File:Dragonfruit> Chiayi
market.jpg|在台湾嘉義市场销售的[火龙果](../Page/火龙果.md "wikilink")
[File:Echinopsis.ogv|花的开放过程](File:Echinopsis.ogv%7C花的开放过程)

## 分類

  - 南美球形仙人掌屬 *Acanthocalycium*
  - 雪晃玉屬 *Acanthocephala*
  - 刺萼柱屬 *Acanthocereus*
  - *Acantholobivia*
  - *Acanthorhipsalis*
  - *Acharagma*
  - *Akersia*
  - *Ancistrocactus*
  - *Anisocereus*
  - *Aporocactus*
  - *Aporophyllum*
  - *Arequipa*
  - *Arequipiopsis*
  - [岩牡丹屬](../Page/岩牡丹屬.md "wikilink")(牡丹類仙人掌) *Ariocarpus*
  - *Armatocereus*
  - *Arrojadoa*
  - *Arrojadoopsis*
  - *Arthrocereus*
  - [星球屬](../Page/星球屬.md "wikilink")(有星類仙人掌) *Astrophytum*
  - *Austrocactus*
  - *Austrocephalocereus*
  - *Austrocylindropuntia*
  - *Aylostera*
  - [皺棱球屬](../Page/皺棱球屬.md "wikilink") *Aztekium*
  - *Azureocereus*
  - *Bartschella*
  - *Bergerocactus*
  - *Bisnaga*
  - *Blossfeldia*
  - *Bolivicactus*
  - *Bolivicereus*
  - *Bonifazia*
  - *Borzicactella*
  - *Borzicactus*
  - *Brachycalycium*
  - *Brachycereus*
  - 雪晃屬 *Brasilicactus*
  - *Brasilicereus*
  - *Brasiliopuntia*
  - *Brasiliparodia*
  - *Bravocactus*
  - *Browningia*
  - *Buiningia*
  - *Calymmanthium*
  - *Carnegiea*
  - *Castellanosia*
  - *Cephalocereus*
  - *Cephalocleistocactus*
  - *Cephalomamillaria*
  - 仙人柱屬 *Cereus*
  - *Chamaecereus*
  - *Chiapasia*
  - *Chiapasophyllum*
  - *Chileniopsis*
  - *Chileorebutia*
  - *Chilita*
  - *Cinnabarinea*
  - *Cintia*
  - *Cipocereus*
  - *Cleistocactus*
  - *Clistanthocereus*
  - *Cochemiea*
  - *Cochiseia*
  - *Coleocephalocereus*
  - *Coloradoa*
  - *Consolea*
  - [龍爪球屬](../Page/龍爪球屬.md "wikilink") *Copiapoa*
  - *Corryocactus*
  - *Corynopuntia*
  - *Coryphantha*
  - *Cryptocereus*
  - *Cullmannia*
  - *Cumarinia*
  - *Cumulopuntia*
  - *Cylindropuntia*
  - *Deamia*
  - *Delaetia*
  - *Dendrocereus*
  - *Denmoza*
  - *Digitorebutia*
  - *Digitostigma*
  - *Disberocereus*
  - [圓盤玉屬](../Page/圓盤玉屬.md "wikilink") *Discocactus*
  - *Disisorhipsalis*
  - *Disocactus*
  - *Disophyllum*
  - *Dolichothele*
  - *Ebnerella*
  - *Eccremocactus*
  - 仙人球屬 *Echinocactus*
  - [鹿角柱屬](../Page/鹿角柱屬.md "wikilink") *Echinocereus*
  - *Echinofossulocactus*
  - *Echinomastus*
  - 短毛丸屬 *Echinopsis*
  - *Emorycactus*
  - *Encephalocarpus*
  - *Eomatucana*
  - *Epiphyllanthus*
  - *Epiphyllopsis*
  - *Epiphyllum*
  - [月世界屬](../Page/月世界屬.md "wikilink") *Epithelantha*
  - *Epixochia*
  - *Erdisia*
  - *Eriocactus*
  - *Eriocephala*
  - *Eriocereus*
  - *Eriosyce*
  - *Erythrorhipsalis*
  - 松球丸屬 *Escobaria*
  - *Escobariopsis*
  - *Escobesseya*
  - *Escobrittonia*
  - *Escocoryphantha*
  - *Escontria*
  - 老樂柱屬 *Espostoa*
  - *Espostoopsis*
  - *Estevesia*
  - *Eulychnia*
  - *Facheiroa*
  - [強刺球屬](../Page/強刺球屬.md "wikilink") *Ferocactus*
  - *Floribunda*
  - *Furiolobivia*
  - 帝王冠屬 *Geohintonia*
  - *Glandulicactus*
  - *Grusonia*
  - *Gymnanthocereus*
  - *Gymnocactus*
  - 裸萼球屬 *Gymnocalycium*
  - *Gymnocereus*
  - *Gymnorebutia*
  - *Haageocereus*
  - *Hamatocactus*
  - *Hariota*
  - *Harrisia*
  - *Haseltonia*
  - *Hatiora*
  - *Heliabravoa*
  - *Helianthocereus*
  - *Heliocereus*
  - *Hertrichocereus*
  - 綾波屬 *Homalocephala*
  - *Horridocactus*
  - *Hylocereus*
  - *Hylorhipsalis*
  - *Hymenorebutia*
  - *Islaya*
  - *Isolatocereus*
  - *Jasminocereus*
  - *Kadenicarpus*
  - 月光殿屬 *Krainzia*
  - *Lagenosocereus*
  - *Lasiocereus*
  - *Lemaireocereus*
  - *Leocereus*
  - *Lepidocoryphantha*
  - *Lepismium*
  - *Leptocereus*
  - *Leptocladodia*
  - [光山屬](../Page/晃山.md "wikilink") *Leuchtenbergia*
  - *Leucostele*
  - *Lobeira*
  - 麗花丸屬 *Lobivia*
  - *Lodia*
  - *Lophocereus*
  - [烏羽玉屬](../Page/烏羽玉屬.md "wikilink") *Lophophora*
  - *Loxanthocereus*
  - *Lymanbensonia*
  - *Machaerocereus*
  - *Maihuenia*
  - *Maihueniopsis*
  - *Mamillopsis*
  - 疣仙人掌屬 *Mammillaria*
  - *Mammilloydia*
  - *Marenopuntia*
  - *Marginatocereus*
  - *Maritimocereus*
  - *Marniera*
  - *Marshallocereus*
  - *Matucana*
  - *Mediocactus*
  - *Mediolobivia*
  - *Melocactus*
  - *Mesechinopsis*
  - *Meyerocactus*
  - *Micranthocereus*
  - *Micropuntia*
  - *Mila*
  - *Miqueliopuntia*
  - *Mirabella*
  - *Mitrocereus*
  - *Monvillea*
  - *Morangaya*
  - *Morawetzia*
  - *Myrtgerocactus*
  - 龍神木屬 *Myrtillocactus*
  - *Navajoa*
  - *Neoabbottia*
  - *Neobesseya*
  - *Neobinghamia*
  - *Neobuxbaumia*
  - *Neocardenasia*
  - *Neochilenia*
  - *Neodawsonia*
  - *Neoevansia*
  - *Neogomesia*
  - *Neolloydia*
  - *Neolobivia*
  - 銀翁玉屬 *Neoporteria*
  - *Neoraimondia*
  - *Neowerdermannia*
  - *Nopalea*
  - *Nopalxochia*
  - *Normanbokea*
  - *Nothorhipsalis*
  - 金晃丸屬 *Notocactus*
  - *Nyctocereus*
  - [帝冠屬](../Page/帝冠.md "wikilink") *Obregonia*
  - *Oehmea*
  - *Ophiorhipsalis*
  - 仙人掌屬(團扇仙人掌) *Opuntia*
  - 獅子錦屬 *Oreocereus*
  - *Oroya*
  - [帝王龍屬](../Page/帝王丸屬.md "wikilink") *Ortegocactus*
  - *Pacherocactus*
  - *Pachycereus*
  - *Parodia*
  - *Parrycactus*
  - *Pediocactus*
  - 精巧丸屬 *Pelechyphora*
  - *Peniocereus*
  - *Pereskia*
  - *Pereskiopsis*
  - *Peronocactus*
  - *Peruvocereus*
  - *Pfeiffera*
  - *Phellosperma*
  - *Philippicereus*
  - *Pierrebraunia*
  - *Pilocanthus*
  - *Pilocopiapoa*
  - *Pilosocereus*
  - *Piptanthocereus*
  - *Platyopuntia*
  - *Polaskia*
  - *Porfiria*
  - *Praecereus*
  - *Pseudoacanthocereus*
  - *Pseudoespostoa*
  - *Pseudolobivia*
  - *Pseudomammillaria*
  - *Pseudomitrocereus*
  - *Pseudonopalxochia*
  - *Pseudopilocereus*
  - *Pseudorhipsalis*
  - *Pseudotephrocactus*
  - *Pseudozygocactus*
  - *Pterocactus*
  - *Pterocereus*
  - *Puebloa*
  - *Puna*
  - *Pygmaeocereus*
  - *Pyrrhocactus*
  - *Quiabentia*
  - *Rapicactus*
  - *Rathbunia*
  - *Rauhocereus*
  - *Rebutia*
  - *Reicheocactus*
  - *Rhipsalidopsis*
  - *Rhipsalis*
  - *Rhodocactus*
  - 絲葦屬 *Rhypsalis*
  - *Rimacactus*
  - *Ritterocactus*
  - *Ritterocereus*
  - *Rodentiophila*
  - *Rooksbya*
  - *Roseocactus*
  - *Roseocereus*
  - *Salpingolobivia*
  - *Samaipaticereus*
  - *Schlumbergera*
  - *Sclerocactus*
  - *Selenicereus*
  - *Sericocactus*
  - *Seticereus*
  - *Seticleistocactus*
  - *Setiechinopsis*
  - *Siccobaccatus*
  - *Soehrensia*
  - *Solisia*
  - 縮玉屬 *Stenocactus*
  - *Stenocereus*
  - *Stephanocereus*
  - *Stetsonia*
  - [菊水屬](../Page/菊水.md "wikilink") *Strombocactus*
  - *Strophocactus*
  - *Submatucana*
  - *Subpilocereus*
  - 溝寶山屬 *Sulcorebutia*
  - *Tacinga*
  - *Tephrocactus*
  - 緋冠龍屬 *Thelocactus*
  - *Thelocephala*
  - *Thrixanthocereus*
  - *Torreycactus*
  - *Toumeya*
  - *Trichocereus*
  - *Tunas*
  - *Tunilla*
  - [姣麗玉屬](../Page/姣麗球屬.md "wikilink") *Turbinicarpus*
  - [尤伯球屬](../Page/尤伯球屬.md "wikilink") *Uebelmannia*
  - *Utahia*
  - *Vatricania*
  - *Weberbauerocereus*
  - *Weberocereus*
  - *Weingartia*
  - *Werckleocereus*
  - *Wigginsia*
  - *Wilcoxia*
  - *Wilmattea*
  - *Winterocereus*
  - *Wittiocactus*
  - *Yavia*
  - *Yungasocereus*
  - *Zehntnerella*
  - *Zygocactus*

## 仙人掌公園

美國的[亞利桑那州因沙漠氣候的關係](../Page/亞利桑那州.md "wikilink")，有相當多的仙人掌，特別是巨大的[巨人柱](../Page/巨人柱.md "wikilink")，因此在1994年成立了[巨人柱國家公園](../Page/巨人柱國家公園.md "wikilink")，園中有多達1000多種來自世界各地不同的仙人掌。此外在日本的[冲绳县](../Page/冲绳县.md "wikilink")，也有仙人掌植物公園。

## 外部連結

  -
  - [马来西亚“金马仑高原仙人掌中心”](https://web.archive.org/web/20070928003730/http://www.visitcameronhighlands.com/index.php?option=com_content&task=view&id=59&Itemid=91)

  - [马来西亚“金马仑高原仙人掌谷公园”](https://web.archive.org/web/20070928003708/http://www.visitcameronhighlands.com/index.php?option=com_content&task=view&id=91&Itemid=121)

  - [琉球姬百合仙人掌植物公園Himeyuri
    Park](https://web.archive.org/web/20041206004848/http://www.ryucom.ne.jp/users/himeyuri/)
    - 中文

  - [美國亞利桑那州仙人掌國家公園](http://www.nps.gov/sagu/)

  - [仙人掌
    Xianrenzhang](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00769)
    藥用植物圖像資料庫 (香港浸會大學中醫藥學院)

[Category:观赏植物](../Category/观赏植物.md "wikilink")
[仙人掌科](../Category/仙人掌科.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")