**爱德华·切赫**（**Eduard Čech**
，1893年6月29日在[波希米亞](../Page/波希米亞.md "wikilink")[斯特拉乔夫](../Page/斯特拉乔夫.md "wikilink")(Stračov)—1960年3月15日在[捷克](../Page/捷克.md "wikilink")[布拉格](../Page/布拉格.md "wikilink")）是捷克[數學家](../Page/數學家.md "wikilink")，生於当時属[奧匈帝国](../Page/奧匈帝国.md "wikilink")，现于[捷克共和国境內的波希米亞斯特拉乔夫](../Page/捷克共和国.md "wikilink")。

## 生平

切赫在1912年進[布拉格查理大學學習數學](../Page/布拉格查理大學.md "wikilink")。因為[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")，1915年他被徵召參戰而中斷了學業。1918年回校完成學位，並任教中學。1920年取得博士。

切赫的興趣在[射影微分幾何](../Page/射影微分幾何.md "wikilink")，在1921年至1922年他往[意大利向](../Page/意大利.md "wikilink")[傅比尼學習](../Page/傅比尼.md "wikilink")，還合著一部這方面的著作。他回國後完成任職論文，在布拉格查理大學任[私人講師](../Page/私人講師.md "wikilink")。1923年他被馬薩里克大學任命為教授。接替一年前逝世的[马加什·莱尔希](../Page/马加什·莱尔希.md "wikilink")(Matyáš
Lerch)。

切赫對[組合拓撲感興趣](../Page/代數拓撲.md "wikilink")，成為這方面的先驅。他最初的興趣在[同調論](../Page/同調論.md "wikilink")，並證明了[流形的對偶定理](../Page/流形.md "wikilink")。他在1932年的論文是要把[點集拓撲和組合拓撲連結起來](../Page/點集拓撲.md "wikilink")，其中他創造了切赫同調。1936年他在[布爾諾開了一個](../Page/布爾諾.md "wikilink")[拓撲研討班](../Page/拓撲.md "wikilink")，直到3年後因[第二次世界大戰而終止](../Page/第二次世界大戰.md "wikilink")。他在大戰前也領導了中學數學課程改革，以改進中學數學教育。在戰後他繼續這項工作，並和學校教師編輯教科書。

1947年他被任命為[捷克科學院的數學研究學院院長](../Page/捷克科學院.md "wikilink")。1950年任命為中央數學院院長。1952年任命為捷克科學院院長。1950年代他的興趣轉到[微分幾何](../Page/微分幾何.md "wikilink")。1956年他擔任布拉格查理大學數學研究院第一任院長。他的健康漸差，但仍然籌備了一份捷克數學期刊和布拉格的拓撲學研討會。

## 参见

  - [切赫上同调](../Page/切赫上同调.md "wikilink")
  - [切赫同调](../Page/切赫同调.md "wikilink")

## 外部链接

  - [MacTutor上切赫的生平](http://www-history.mcs.st-andrews.ac.uk/history/Mathematicians/Cech.html)

[C](../Category/1893年出生.md "wikilink")
[C](../Category/1960年逝世.md "wikilink")
[C](../Category/捷克数学家.md "wikilink")
[C](../Category/20世纪数学家.md "wikilink")
[Category:布拉格查理大學校友](../Category/布拉格查理大學校友.md "wikilink")
[Category:布拉格查理大學教師](../Category/布拉格查理大學教師.md "wikilink")