**傑夫·卡斯坦斯**（）出生地為[聖地牙哥
(加利福尼亞州)](../Page/聖地牙哥_\(加利福尼亞州\).md "wikilink")，為[美國的](../Page/美國.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，目前效力於[美國職棒大聯盟](../Page/美國職棒大聯盟.md "wikilink")[匹茲堡海盜](../Page/匹茲堡海盜.md "wikilink")。

## 經歷

  - 卡斯坦斯是[紐約洋基在](../Page/紐約洋基.md "wikilink")2003年大聯盟選秀第19輪選中的選手。
  - 卡斯坦斯在2006年8月19日升上大聯盟，在8月22日先發對[西雅圖水手](../Page/西雅圖水手.md "wikilink")，投5.2局責失3分，無關勝敗，而球隊6：5輸球\[1\]。8月27日先發對[洛杉磯天使](../Page/洛杉磯天使.md "wikilink")，投6局責失3分，拿下大聯盟的第1勝。這一年出賽9九場比賽，拿下2勝1敗的成績。
  - 2007年4月28日先發面對[波士頓紅襪時](../Page/波士頓紅襪.md "wikilink")，第一局尚未解決任何一位打者就因受傷而退場，而由[井川慶接替投球](../Page/井川慶.md "wikilink")，井川慶投6局無失分，拿下勝投\[2\]，之後卡斯坦斯進入傷兵名單，到9月才再度回到大聯盟。
  - 2008年球季卡斯坦斯在小聯盟開始。7月26日洋基以卡斯坦斯等4名球員和[匹茲堡海盜交易](../Page/匹茲堡海盜.md "wikilink")[賽維爾·奈迪及](../Page/賽維爾·奈迪.md "wikilink")[達馬索·馬提](../Page/達馬索·馬提.md "wikilink")\[3\]。
  - 2008年8月1日卡斯坦斯先發面對[芝加哥小熊](../Page/芝加哥小熊.md "wikilink")，投6局無失分拿下勝投，下一場先發面對[亞利桑那響尾蛇完投](../Page/亞利桑那響尾蛇.md "wikilink")9局0失分，擊敗響尾蛇的投手[藍迪·強森](../Page/藍迪·強森.md "wikilink")，拿下大聯盟生涯第一場完投勝。
  - 2009年球季結束以後，卡斯坦斯因表現不理想被被[指定派遣](../Page/指定派遣.md "wikilink")。11月30日，他選擇到回到小聯盟。
  - 2010年4月27日，他被叫上大聯盟。6月8日，他於客場先發面對[華盛頓國民](../Page/華盛頓國民.md "wikilink")，對方投手為[史蒂芬·史特拉斯堡的第](../Page/史蒂芬·史特拉斯堡.md "wikilink")1次先發，主投5局被打9支安打失4分，最後吞下個人今年球季的第2敗，而史特拉斯堡順利拿下個人生涯在大聯盟的第1勝。

## 參考資料

## 外部連結

  -
  - [Bio from Texas
    Tech](https://web.archive.org/web/20080820163646/http://texastech.cstv.com/sports/m-basebl/mtt/karstens_jeff00.html)

[Category:美國棒球選手](../Category/美國棒球選手.md "wikilink")
[Category:紐約洋基隊球員](../Category/紐約洋基隊球員.md "wikilink")
[Category:匹茲堡海盜隊球員](../Category/匹茲堡海盜隊球員.md "wikilink")
[Category:加州人](../Category/加州人.md "wikilink")

1.  [Yahoo Box Score for the 08/22/2006
    Game](http://sports.yahoo.com/mlb/boxscore?gid=260822112)
2.
3.