**安德雷·尼古拉耶維奇·柯爾莫哥洛夫**（[俄语](../Page/俄语.md "wikilink")：，英语：Andrey
Nikolaevich
Kolmogorov，），[俄国](../Page/俄国.md "wikilink")[數學家](../Page/數學家.md "wikilink")，主要研究[概率論](../Page/概率論.md "wikilink")、[算法信息論](../Page/算法信息論.md "wikilink")、[拓撲學](../Page/拓撲學.md "wikilink")、[直觉主义逻辑](../Page/直觉主义逻辑.md "wikilink")、[紊流](../Page/紊流.md "wikilink")、[经典力学和](../Page/经典力学.md "wikilink")[計算複雜性理論](../Page/計算複雜性理論.md "wikilink")，最為人所道的是對概率論[公理化所作出的貢獻](../Page/公理化.md "wikilink")。他曾說："概率論作為數學學科，可以而且應該從[公理開始建設](../Page/公理.md "wikilink")，和[幾何](../Page/幾何.md "wikilink")、[代數的路一樣](../Page/代數.md "wikilink")"。

## 生平

柯爾莫哥洛夫于1903年出生在[坦波夫](../Page/坦波夫.md "wikilink")。他的未婚母亲在分娩时死亡。他的阿姨们在靠近[雅罗斯拉夫尔的](../Page/雅罗斯拉夫尔.md "wikilink")[图诺什纳爷爷家把他养大](../Page/图诺什纳.md "wikilink")。关于他父亲所知甚少。据说曾是农学家，在参加反对沙皇的革命运动后被从[圣彼得堡驱逐到外省](../Page/圣彼得堡.md "wikilink")，于1919年失踪，被认为死于[俄国内战](../Page/俄国内战.md "wikilink")。柯爾莫哥洛夫在阿姨的乡村学校上学，他最初的文学成就和数学文章都发表于校报。青少年时设计了[永动机](../Page/永动机.md "wikilink")，连老师也看不出哪里出了必要的错误。1910年，阿姨收养他然后移居[莫斯科](../Page/莫斯科.md "wikilink")，他在那里上了[文科中学](../Page/文科中学.md "wikilink")，于1920年毕业。

## 參見

  - [柯爾莫哥洛夫公理](../Page/柯爾莫哥洛夫公理.md "wikilink")
  - [零一律](../Page/零一律.md "wikilink")
  - [柯爾莫哥洛夫空間](../Page/柯爾莫哥洛夫空間.md "wikilink")
  - [柯氏复杂性](../Page/柯氏复杂性.md "wikilink")

## 外部連結

  - [Kolmogorov的數學觀與業績／伊藤清](http://episte.math.ntu.edu.tw/articles/mc/mc_39_01/)

  -
[Category:20世紀數學家](../Category/20世紀數學家.md "wikilink")
[Category:沃尔夫数学奖得主](../Category/沃尔夫数学奖得主.md "wikilink")
[Category:列寧獎獲得者](../Category/列寧獎獲得者.md "wikilink")
[Category:斯大林獎獲得者](../Category/斯大林獎獲得者.md "wikilink")
[Category:社會主義勞動英雄](../Category/社會主義勞動英雄.md "wikilink")
[Category:俄羅斯科學院院士](../Category/俄羅斯科學院院士.md "wikilink")
[K](../Category/英國皇家學會院士.md "wikilink")
[Category:蘇聯數學家](../Category/蘇聯數學家.md "wikilink")
[Category:俄國數學家](../Category/俄國數學家.md "wikilink")
[Category:俄羅斯統計學家](../Category/俄羅斯統計學家.md "wikilink")
[Category:莫斯科國立大學教師](../Category/莫斯科國立大學教師.md "wikilink")
[Category:莫斯科国立大学校友](../Category/莫斯科国立大学校友.md "wikilink")
[Category:坦波夫州人](../Category/坦波夫州人.md "wikilink")
[Category:动力系统理论家](../Category/动力系统理论家.md "wikilink")