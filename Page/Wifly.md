[Wifly-logo.gif](https://zh.wikipedia.org/wiki/File:Wifly-logo.gif "fig:Wifly-logo.gif")
**Wifly**是由[安源資訊公司建置](../Page/安源資訊.md "wikilink")，於[台北市大部份區域及連鎖餐廳提供的付費式](../Page/台北市.md "wikilink")[無線網路服務](../Page/無線網路.md "wikilink")。其服務是採用[802.11系列標準之](../Page/802.11.md "wikilink")[無線區域網路](../Page/無線區域網路.md "wikilink")（Wireless
Local Area Network / Wireless LAN;
WLAN）。目前總共在台北市佈建4000顆[AP](../Page/AP.md "wikilink")（無線接取器），人口覆蓋率達90％，區域覆蓋率也有48％。

## 佈建範圍

### 第一階段

在[捷運站內](../Page/台北捷運.md "wikilink")，共有數百顆[AP](../Page/無線接取器.md "wikilink")。

### 第二階段

建置涵蓋範圍包括全[台北市四十二條](../Page/台北市.md "wikilink")[主要幹道](../Page/臺北市主要道路列表.md "wikilink")，從[民權東西路以南](../Page/民權東西路.md "wikilink")、[環河南北路以東](../Page/環河南北路.md "wikilink")、[和平西路及](../Page/和平西路.md "wikilink")[羅斯福路以北](../Page/羅斯福路_\(台北市\).md "wikilink")、[基隆路以西的住商區域](../Page/基隆路.md "wikilink")，重點商圈包括：[西門町商圈](../Page/西門町商圈.md "wikilink")、[忠孝信義商圈及](../Page/忠孝信義商圈.md "wikilink")[敦南商圈等精華地區](../Page/敦南商圈.md "wikilink")，以及[臺北市政府週邊](../Page/臺北市政府.md "wikilink")、[台北市各捷運站週邊](../Page/台北捷運.md "wikilink")、全台灣134家[星巴克](../Page/星巴克.md "wikilink")、[IS
COFFEE等](../Page/IS_COFFEE.md "wikilink")，一共建置了2,000多個AP。

### 第三階段

新增[臺北市9大市立醫院院區](../Page/臺北市立聯合醫院.md "wikilink")、53個[台北市立圖書館分館](../Page/台北市立圖書館.md "wikilink")、12個台北市行政區大樓…等地，以及臺北市內超過六百家的[7-Eleven](../Page/7-Eleven.md "wikilink")（周邊100公尺內），已在全市共計佈建超過4千顆AP。

[File:Wifly-ap4.JPG|捷運站內的Wifly](File:Wifly-ap4.JPG%7C捷運站內的Wifly) AP
[File:Wifly-ap1.jpg|敦化南路路燈上架設的Wifly](File:Wifly-ap1.jpg%7C敦化南路路燈上架設的Wifly)
AP
[File:Wifly-ap2.JPG|加掛無線發射器的Wifly](File:Wifly-ap2.JPG%7C加掛無線發射器的Wifly)
AP
[File:Wifly_AP_in_Taipei_1.jpg|在電線桿上的Wifly](File:Wifly_AP_in_Taipei_1.jpg%7C在電線桿上的Wifly)

### 第四階段

逐步將室內AP置換為WIFLY-Indoor，但許多用戶抱怨無法連上WiFly無線網路，卻持續扣除使用時數，遭致部分用戶抱怨。

## 使用設備

WIFLY 無線網路設備主要為Nortel Wireless Mesh Network。以Wireless Gateway
7250為主要路由收容設備並且搭配Wireless Access Point
7220。該設備被稱之為Mesh主要原因為各Access Point中間溝通使用
802.11a並且將802.11b/g留給用戶端使用。此設計可節省Access Point連接至Gateway的實體線路佈線成本。

## 費率

### 免費使用

點擊提供的廣告後可以免費使用30分鐘，每日可無限次使用。

### 計時制

大約在每分鐘0.8元∼0.9元之間，計時制共有360分鐘300元與110分鐘99元兩種。

### 定時制

有二種：24小時100元與31天500元。

### 固定會員

月付399元與年繳4200元。學生方案月付299元或年繳3000元。

[File:Wifly-card-freetrial.jpg|慶祝建置完成，免費贈送的30分鐘試用卡](File:Wifly-card-freetrial.jpg%7C慶祝建置完成，免費贈送的30分鐘試用卡)
[File:Wifly-card-24.jpg|24小時預付卡](File:Wifly-card-24.jpg%7C24小時預付卡)
[File:Wifly-card-time110.jpg|110分鐘計時制預付卡](File:Wifly-card-time110.jpg%7C110分鐘計時制預付卡)
[File:Wifly-card-time300.JPG|300分鐘計時制預付卡](File:Wifly-card-time300.JPG%7C300分鐘計時制預付卡)

## 其他服務

## 參考資料

## 參閱

  - [Wifi](../Page/Wifi.md "wikilink")
  - [FON](../Page/FON.md "wikilink")

## 外部連結

  - [WIFLY](https://web.archive.org/web/20080516121207/http://www.wifly.com.tw/)
  - [Qon無線網路熱點搜尋](http://www.qon.com.tw/hotsearch/)

[Category:无线网络](../Category/无线网络.md "wikilink")