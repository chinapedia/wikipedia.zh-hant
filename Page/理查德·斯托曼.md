[Richard_Matthew_Stallman.jpeg](https://zh.wikipedia.org/wiki/File:Richard_Matthew_Stallman.jpeg "fig:Richard_Matthew_Stallman.jpeg")出版\]\]
[Wikimania_stallman_keynote2.jpg](https://zh.wikipedia.org/wiki/File:Wikimania_stallman_keynote2.jpg "fig:Wikimania_stallman_keynote2.jpg")演講，題目為「版權與社群」\]\]
[FreeAsInFreedom_cover.png](https://zh.wikipedia.org/wiki/File:FreeAsInFreedom_cover.png "fig:FreeAsInFreedom_cover.png")
**理查德·马修·斯托曼**（，簡稱\[1\]，），美国[程序员](../Page/程序员.md "wikilink")，[自由软件活动家](../Page/自由软件.md "wikilink")。他发起自由软件运动，倡导软件使用者能够对软件自由进行使用、学习、共享和修改，确保了这些软件被称作自由软件。斯托曼发起了[GNU项目](../Page/GNU.md "wikilink")，并成立了[自由软件基金会](../Page/自由软件基金会.md "wikilink")。他开发了**[GCC](../Page/GCC.md "wikilink")、[GDB](../Page/GNU侦错器.md "wikilink")、[GNU
Emacs](../Page/Emacs.md "wikilink")，**同时编写了[GNU通用公共许可协议](../Page/GNU通用公共许可证.md "wikilink")。

Stallman为了创建一个完全由免费软件组成的[类Unix](../Page/类Unix系统.md "wikilink")[计算机操作系统在](../Page/操作系统.md "wikilink")1983年9月推出了GNU项目。凭借这个，他又发起了[自由软件运动](../Page/自由软件运动.md "wikilink")。他迄今为止一直是GNU项目的组织者，作为主要开发者的他开发了一些被广泛使用的GNU软件，其中包括**GCC**
**GDB** **GNU Emacs**。在1985年10月他创立了自由软件基金会。

斯托曼开创了[Copyleft的概念](../Page/Copyleft.md "wikilink")，它使用版权法的原则来保护使用、修改和分发自由软件的权利，并且是描述这些术语的自由软件许可证的主要作者。最为人所称道的是GPL（最广泛使用的自由软件协议）。

1989年，他和别人一起创立了[League for Programming
Freedom](../Page/League_for_Programming_Freedom.md "wikilink")。自20世纪90年代中期以来，斯托曼花他大部分时间组织参与宣传自由软件，反对[软件专利和](../Page/软件专利.md "wikilink")[数字版权管理的以及他认为剥夺用户自由的其他法律和技术系统运动](../Page/数字版权管理.md "wikilink")。这包括[最终用户许可协议](../Page/最终用户许可协议.md "wikilink")、[保密协议](../Page/保密协议.md "wikilink")、 [产品激活](../Page/产品激活.md "wikilink")、加密狗、软件复制保护、专有格式、[二进制软件包](../Page/二进制文件.md "wikilink")（没有[源代码的可执行文件](../Page/源代码.md "wikilink")）。

截至2016年，他获得了十五个[荣誉博士及教授称号](../Page/荣誉博士学位.md "wikilink")。

## 早年生活

斯托曼1953年出生于美国纽约一个犹太人家庭,他的母亲爱丽丝·利普曼是一名老师,父亲丹尼尔·斯托曼是一名印刷机商人.由于父亲酗酒并口头虐待他的继母,斯托曼与父母之间的关系很糟糕.他后来用暴君来描述他的父母\[2\].早年他就对计算机有很深的兴趣；在斯托曼作为一个青少年参加一个夏令营时,他阅读了一本[IBM7090的手册](../Page/IBM_7090.md "wikilink").从1967年到1969年,斯塔曼参加了哥伦比亚大学的高中生周六编程课程。\[3\]同时他也是[洛克菲勒大学](../Page/洛克菲勒大學.md "wikilink")[生物系的志愿者实验室助理](../Page/生物系.md "wikilink")。虽然他对[数学和](../Page/数学.md "wikilink")[物理学感兴趣](../Page/物理学.md "wikilink")，但洛克菲勒大学的教授认为他有成为生物学家的希望.\[4\]

他第一次实际的使用电脑是高中年代在[IBM纽约科学中心](../Page/IBM纽约科学中心.md "wikilink").他在1970年的夏天高中毕业后被雇用在[Fortran写一个数值分析程序](../Page/Fortran.md "wikilink").\[5\]他在几周后完成了这项任务,然后他用这个夏天剩余的休息时间在[APL上写了一个文本编辑器以及](../Page/APL語言.md "wikilink")[IBM
System/360上](../Page/IBM_System/360.md "wikilink")[PL/I编程语言的](../Page/PL/I.md "wikilink")[预处理器](../Page/预处理器.md "wikilink")。\[6\]

### 哈佛大学和麻省理工学院

作为哈佛大学1970年秋季的一年级学生,斯塔曼以[Math
55的表现而闻名](../Page/Math_55.md "wikilink"),\[7\]他很高兴的回忆到:“我一生中第一次觉得我在哈佛找到了一个家。”\[8\]

1971年,斯托曼在哈佛大学第一年快结束的时候,他成为了[麻省理工学院人工智能实验室的一名程序员](../Page/麻省理工学院人工智能实验室.md "wikilink")，同时也成为[黑客社区的常客](../Page/黑客.md "wikilink"),并以他的名字缩写RMS而闻名,\[9\]1974年斯塔曼从哈佛大学毕业并取得了物理学学士学位。\[10\]

斯托曼考虑过留在哈佛大学,但是他却转而决定去麻省理工学院去读研究生.他攻读物理学博士学位一年，但随后选择了放弃，专注于他在麻省理工学院AI实验室的编程.\[11\]\[12\]

1975年他开始在麻省理工学院担任[杰拉德·杰伊·萨斯曼的研究室助理](../Page/傑拉德·傑伊·薩斯曼.md "wikilink")\[13\],在1977年与苏斯曼发表了一篇*名为dependency-directed
backtracking*\[14\]*的*关于 AI [truth maintenance
system的论文](../Page/wikipedia:Truth_maintenance_system.md "wikilink").这篇论文是对[约束补偿问题智能回溯的早期研究](../Page/约束补偿问题.md "wikilink")。截至2009年，斯托曼和苏斯曼介绍的技术仍然是智能回溯中最通用和最强大的解释.\[15\]有关[Constraint_learning](../Page/Constraint_learning.md "wikilink")
的技术也在该论文中被提及.

作为[MIT计算机科学与人工智能实验室的黑客](../Page/MIT計算機科學與人工智慧實驗室.md "wikilink")，Stallman从事软件项目，如[文本編輯器](../Page/文本编辑器.md "wikilink")，不兼容的分时系统上的Emacs，还有[lisp机器的操作系统项目](../Page/Lisp机器.md "wikilink").在这期间,他成为了实验室电脑限制访问技术的热心评论家,当时这个计划是由[国防高级研究计划局资助的](../Page/國防高等研究計劃署.md "wikilink")。当MIT计算机科学与人工智能实验室安装了一套密码控制系统,斯托曼很快就找到了一个解密密码的方式并发送给用户包含其已解码密码的消息,建议他们使用空字符串作为密码(事实上就是没有密码),这样就可以重新启用用户对系统的匿名访问.当时约有20％的用户遵循他的建议，尽管使用密码的行为最终占上风。对此,多年后斯托曼还一直向别人吹嘘着他当年的成功.\[16\]

## GNU项目的发起

在二十世纪而七十年代末和八十年代初，由斯托曼所发展的[黑客文化开始分裂开来](../Page/黑客文化.md "wikilink")．为了防止自己的软件被竞争对手所使用，大多数厂家停止分发其软件[源代码并开始使用版权和限制性软件许可证来限制或者禁止软件源代码的复制或再分配](../Page/源代码.md "wikilink")，这类[专有软件以前就存在过](../Page/专有软件.md "wikilink")，很明显这将会成为一种规范性的做法．Stallman的麻省理工学院的同事布鲁斯特·卡勒（Brewster
Kahle）所说，这种软件法律特征的转变可以被认为是1976年[“美国版权法](../Page/美国版权法.md "wikilink")”所引发的后果。

## 相关

### GNU与Linux

1991年芬兰大学生[林納斯·托瓦茲在GNU通用公共許可證下发布了最初是为自己创作的](../Page/林納斯·托瓦茲.md "wikilink")[Linux操作系统内核](../Page/Linux内核.md "wikilink")，暂时替代了GNU计划的Hurd内核。至此，GNU计划基本完成，此操作系统被命名为[GNU/Linux](../Page/GNU/Linux.md "wikilink")（这类操作系统常常被称为Linux，斯托曼坚持认为应该被叫做GNU/Linux以体现GNU在其中的重大贡献。参见[GNU/Linux命名争议](../Page/GNU/Linux命名争议.md "wikilink")）。而因为Hurd内核是率先被计划的，因而GNU仍然进行着它的开发。

### 自由軟體与开源软件

斯托曼是一名坚定的自由软件运动倡导者，与提倡[开放源代码开发模型的人不同](../Page/开放源代码.md "wikilink")，斯托曼并不是从软件的-{zh-hans:质量;
zh-hant:品質;}-的角度而是从道德的角度来看待自由软件。他认为软件封闭是非常不道德的事，只有尊重用户自由的程序才是符合其道德标准的。对此许多人表示异议，并也因此有了[自由软件与](../Page/自由软件.md "wikilink")[开源软件之分](../Page/开源软件.md "wikilink")。而事实上，1998年“开源”一词最初从自由软件社区中分化出来仅仅是因为在英文中“自由”（）一词有歧义，而其支持者认为“开源”（）一词更好。\[17\]

## 荣誉

[GNU_and_Stallman_2012.JPG](https://zh.wikipedia.org/wiki/File:GNU_and_Stallman_2012.JPG "fig:GNU_and_Stallman_2012.JPG")晶片的江蘇龍夢电脑，該電腦甚至于在BIOS層級完全是自由軟體\]\]

  - 1990年度[麦克阿瑟奖](../Page/麦克阿瑟奖.md "wikilink")
  - 1991年度[美国计算机协会颁发的Grace](../Page/美国计算机协会.md "wikilink") Hopper
    Award以表彰他所开发的的Emacs文字编辑器
  - 1996年获颁[瑞典](../Page/瑞典.md "wikilink")[皇家理工学院荣誉](../Page/皇家理工学院.md "wikilink")[博士学位](../Page/博士.md "wikilink")
  - 1998年度[电子前线基金会先锋奖](../Page/电子前线基金会.md "wikilink")
  - 1999年Yuri Rubinsky纪念奖
  - 2001年在[蘇格蘭获颁](../Page/蘇格蘭.md "wikilink")[格拉斯哥大学荣誉博士学位](../Page/格拉斯哥大学.md "wikilink")
  - 2001年武田研究奖励赏（武田研究奨励賞）
  - 2002年成为[美国国家工程院院士](../Page/美国国家工程院.md "wikilink")
  - 2003年在[比利時获颁](../Page/比利時.md "wikilink")[布鲁塞尔大学荣誉博士学位](../Page/布鲁塞尔大学.md "wikilink")
  - 2004年在[阿根廷獲頒](../Page/阿根廷.md "wikilink")[國立沙爾塔大學榮譽博士學位](../Page/國立沙爾塔大學.md "wikilink")
  - 2004年獲得[祕魯國立](../Page/祕魯.md "wikilink")大學榮譽教授
  - 2007年获颁秘魯印加大學荣誉教授
  - 2007年获颁Universidad de Los Angeles de Chimbote荣誉博士学位
  - 2007年获颁帕維亞大學荣誉博士学位
  - 2008年获颁[祕魯National](../Page/祕魯.md "wikilink") University of
    Trujillo荣誉博士学位
  - 2011年在[阿根廷获颁](../Page/阿根廷.md "wikilink")[国立科尔多瓦大学荣誉博士学位](../Page/国立科尔多瓦大学.md "wikilink")\[18\]

## 参考文献

## 外部链接

  - [斯托曼个人主页](http://www.stallman.org)

  - [自由软件基金会宗旨](http://www.gnu.org/philosophy)

  - [自由软件基金会](http://www.fsf.org)

  - [Free as in Freedom--Biography of Richard
    Stallman（斯托曼传记）](http://www.oreilly.com/openbook/freedom/)

  - [Free as in Freedom--Biography of Richard
    Stallman斯托曼传记中文書評](http://littleblackrabit.mysinablog.com/index.php?op=ViewArticle&articleId=835739)

## 参见

  - [GNU](../Page/GNU.md "wikilink")
  - [自由软件基金会](../Page/自由软件基金会.md "wikilink")
  - [自由软件运动](../Page/自由软件运动.md "wikilink")
  - [Emacs](../Page/Emacs.md "wikilink")
  - [GCC](../Page/GCC.md "wikilink")
  - [Linux](../Page/Linux.md "wikilink")

{{-}}

[Category:美国程序员](../Category/美国程序员.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:自由软件程序员](../Category/自由软件程序员.md "wikilink")
[Category:美国国家工程院院士](../Category/美国国家工程院院士.md "wikilink")
[Category:GNU人物](../Category/GNU人物.md "wikilink")
[Category:Unix人物](../Category/Unix人物.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")

1.
2.
3.
4.
5.   Michael
    Gross|accessdate=2017-05-26|work=mgross.com|language=en-US}}
6.
7.
8.   Michael
    Gross|accessdate=2017-05-26|work=mgross.com|language=en-US}}
9.
10.
11.  Michael
    Gross|accessdate=2017-05-27|work=mgross.com|language=en-US}}
12.
13.
14.
15.
16.
17. [GNU工程的哲学](http://www.gnu.org/philosophy/open-source-misses-the-point.zh-cn.html)
18.