《**太玄**》，又称《**太玄经**》、《**扬子太玄经**》或《**玄经**》，是[西汉学者](../Page/西汉.md "wikilink")[扬雄的一部著作](../Page/扬雄.md "wikilink")，用来阐述他的哲学体系和[宇宙论](../Page/宇宙论.md "wikilink")。《[四库全书](../Page/四库全书.md "wikilink")》中为避[康熙皇帝的](../Page/康熙.md "wikilink")[名讳](../Page/避諱.md "wikilink")，改称其为《**太元经**》。《[新唐书](../Page/新唐书.md "wikilink")·艺文志》作十二卷，《[文献通考](../Page/文献通考.md "wikilink")》作十卷。

《太玄》糅合了[儒家](../Page/儒家.md "wikilink")、[道家和](../Page/道家.md "wikilink")[阴阳家的学说](../Page/阴阳家.md "wikilink")。其首先从《[老子](../Page/老子.md "wikilink")》“玄之又玄”中概括出“玄”（玄奥）的概念，以玄为中心，按[天](../Page/天.md "wikilink")[地](../Page/地.md "wikilink")[人三道的分类建立了一个](../Page/人.md "wikilink")[形而上学体系](../Page/形而上学.md "wikilink")。《太玄》认为一切事物从发展到旺盛到消亡都可分成九个阶段。

唐朝诗人[李白的](../Page/李白.md "wikilink")《侠客行》最后一句“白首太玄经”，即指此书。

## 与周易的比较

《太玄》的内容和体裁上都和《[周易](../Page/周易.md "wikilink")》有类似之处，所以在西方社會亦被稱為「另類易經」（）。周易以阴阳双方的关系为主题，分为了两仪、四象、八卦、六十四重卦、三百八十四爻。太玄以天地人三方的关系为主题，分成一玄、三方、九州、二十七部、八十一家、七百二十九赞；另有玄首、玄测、玄冲、玄攡、玄错、玄莹、玄数、玄文、玄倪、玄图、玄告11篇说明。

### Unicode編碼

《太玄經》使用了傳統的「-{乾}-/坤」二元以外的「天/地/人」三方，其象徵符號亦被收錄於[Unicode編碼之內](../Page/Unicode.md "wikilink")。當中，各個演化單位的代表符號的編碼如下：

  - 單爻：
      - 以一條完整的直線（[TXJ_1.svg](https://zh.wikipedia.org/wiki/File:TXJ_1.svg "fig:TXJ_1.svg")；U+268A
        ⚊）來代表「天」；
      - 以一條中間斷開一次的直線（[TXJ_2.svg](https://zh.wikipedia.org/wiki/File:TXJ_2.svg "fig:TXJ_2.svg")；U+268B
        ⚋）來代表「地」；
      - 以一條中間斷開兩次的直線（[TXJ_3.svg](https://zh.wikipedia.org/wiki/File:TXJ_3.svg "fig:TXJ_3.svg")；U+1D300
        𝌀）來代表「人」（但在Unicode的文件裡被錯譯為「地」*earth*，因為「天」和「地」的符號在西方普遍被叫作「陽」和「陰」，見Unicode編碼文件\[1\]）。
  - 二爻：U+1D301 到 U+1D305 (另加四個與易經相同的[四象符號](../Page/四象.md "wikilink"))
  - 四爻：U+1D306 到 U+1D356

## 口诀

《推玄筭》：

## 注释版本

  - 《太玄经解》，[晋朝范望著](../Page/晋朝.md "wikilink")
  - 《太玄经集注》，北宋[司马光著](../Page/司马光.md "wikilink")
  - 《太玄阐秘》，清朝陈本礼著
  - 《太玄校释》，现代郑万耕著

## 參看

  - [八卦](../Page/八卦.md "wikilink") - [易經](../Page/易經.md "wikilink")

## 參考資料

## 外部連結

  - 有關《太玄經》Unicode編碼的相關文件及討論：
      - <http://std.dkuug.dk/JTC1/SC2/WG2/docs/n2416.pdf>
      - <http://www.unicode.org/charts/PDF/U1D300.pdf>
  - [《太玄經》全文](http://ctext.org/taixuanjing/zh)

[Category:子部術數類](../Category/子部術數類.md "wikilink")
[Category:哲学书籍](../Category/哲学书籍.md "wikilink")
[Category:漢朝典籍](../Category/漢朝典籍.md "wikilink")

1.  [Unicode編碼文件](http://www.unicode.org/charts/PDF/U1D300.pdf)