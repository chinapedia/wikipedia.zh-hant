{{ \#invoke:Unsubst||date=__DATE__ |$B=
**圖像**或**照片**以[提高其-{zh-hans:质量;zh-hant:品質;}-](Wikipedia:改進條目.md "wikilink")。
}}請盡可能將此模板替換為更具體的。 {{\#if:  | |yes|no}}}}} | yes | collapsed}}" {{\!}}-
\! style="font-weight:normal;" colspan="3" {{\!}} 以下區域的維基人也許可以幫忙：
{{\!}}- style="font-size:90%; text-align:left; vertical-align:top;"
{{\!}} {{\#if:| \*  }}{{\#if:| \*  }}{{\#if:| \*  }}{{\#if:| \*
}}{{\#if:| \*  }}{{\#if:| \*  }}{{\#if:| \* }} {{\!}} {{\#if:| \*
}}{{\#if:| \*  }}{{\#if:| \*  }}{{\#if:| \*  }}{{\#if:| \*  }}{{\#if:|
\*  }}{{\#if:| \* }} {{\!}} {{\#if:| \*  }}{{\#if:| \*  }}{{\#if:| \*
}}{{\#if:| \*  }}{{\#if:| \*  }}{{\#if:| \*  }}{{\#if:| \* }} {{\!}}}|
{{\#if:|{{\#ifexist:Category:維基人|維基人\]\]'''也許可以幫忙！}} }} }}
<small>\[
自由圖像搜索工具\]也許能夠在[Flickr和其他網站上找到合適的圖像](Flickr.md "wikilink")。</small>
|imageright=|class=mw-ui-progressive}} }} }} }}<noinclude></noinclude>