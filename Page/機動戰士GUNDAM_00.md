《**機動戰士GUNDAM
00**》（）是[GUNDAM系列的](../Page/GUNDAM系列作品.md "wikilink")[電視動畫第](../Page/電視動畫.md "wikilink")12部作品。本動畫共分兩季，第一季共25話於2007年10月至2008年3月間在[每日放送](../Page/每日放送.md "wikilink")（MBS）、[東京放送](../Page/TBS電視.md "wikilink")（TBS）系列播放，第二季於2008年10月5日起播出。本動畫亦是GUNDAM系列首次以[HDTV的方式播放](../Page/HDTV.md "wikilink")。

故事起始時間為公元2307年，描寫4位操控人型機動兵器「GUNDAM」（「[-{GUNDAM}-](../Page/GUNDAM.md "wikilink")」）的GUNDAM機師，為了把戰爭從世界上根絕而戰鬥。標題的「00」是2個[數字](../Page/阿拉伯數字.md "wikilink")0，讀作「」，這是由於英語中的「Zero」，用作數字時可發音為「“oh”（）」\[1\]。

2009年3月29日在動畫最後一集播畢後，製作單位發表《[劇場版 -A wakening of the
Trailblazer-](../Page/劇場版_機動戰士GUNDAM00_-A_wakening_of_the_Trailblazer-.md "wikilink")》的消息，這部劇場版已於2010年9月18日在[日本上映](../Page/日本.md "wikilink")。

## 故事簡介

### 第一季

由於地球上的[化石燃料枯竭](../Page/化石燃料.md "wikilink")，[人類開始依靠](../Page/人類.md "wikilink")[太阳能發電作為新的能源](../Page/太阳能.md "wikilink")。西元2307年，以三座長度約五萬[公里的](../Page/公里.md "wikilink")[軌道升降機為中心](../Page/太空電梯.md "wikilink")，建構完成巨大的[太陽能發電衛星](../Page/太陽能發電衛星.md "wikilink")，但是能夠使用此系統的，只有共同參與其興建的同盟國家而已。

為了建造這幾座能產生半永久能源的軌道升降機，世界各區域經過統合，形成三個超大國家群。分別是以[美國為中心的](../Page/美國.md "wikilink")「[世界經濟聯合](../Page/機動戰士GUNDAM_00登場國家及組織列表#太陽能與自由國家聯合.md "wikilink")」（UNION）；以[中國](../Page/中國.md "wikilink")、[俄羅斯與](../Page/俄羅斯.md "wikilink")[印度為中心的](../Page/印度.md "wikilink")「[人類革新聯盟](../Page/機動戰士GUNDAM_00登場國家及組織列表#人類革新聯盟.md "wikilink")」（人革聯）；以及歐洲的「[新歐洲共同體](../Page/機動戰士GUNDAM_00登場國家及組織列表#新歐洲共同體.md "wikilink")」（AEU）。因軌道升降機體積巨大而難以防禦，構造上也極其脆弱，在此緊張狀態下，三大國家群為了各自的威信和繁榮，逐漸擴大彼此間的[零和博弈](../Page/零和博弈.md "wikilink")。

在這無法結束紛爭的世界，出現了擁有超規格性能[機動戰士](../Page/機動戰士.md "wikilink")（[Mobile
Suit](../Page/Mobile_Suit.md "wikilink")）「[GUNDAM](../Page/GUNDAM.md "wikilink")」的私設武裝組織「天上人」（Celestial
Being）。為了根絕[戰爭](../Page/戰爭.md "wikilink")，他們進行着超越了[民族](../Page/民族.md "wikilink")、[国家](../Page/国家.md "wikilink")、[宗教的武力介入行動](../Page/宗教.md "wikilink")。

於是，以GUNDAM對全戰爭行為的武力介入開始了。

### 第二季

西元2312年，天上人與聯合國軍在2307年進行大決戰的四年後，人類已建立地球聯邦政府。為了追求更進一步的國家間之統合，以及人類意識的統一，該政府在地球聯邦正規軍之外，設立獨立治安維持部隊「A-Laws」，目的為對所有危害地球聯邦的勢力與主義、思想等，進行非人道的壓迫，對象包括反地球聯邦的國家、純源（Katharon）反抗組織，以及再次出現的天上人。

為了遵守與露意絲·哈利維的約定，朝着成為太空工程師之路前進的沙慈·克洛斯羅德，也身不由己地，捲進了地球聯邦政府的變革中。

另一方面，在四年前的最終決戰中，存活下來的剎那·F·塞耶，默默注視着因為受天上人影響而變革的世界，所產生出來的變化。打倒監視者亞歷漢卓·科納後，他夢想着世界從此變得和平，再沒有任何紛爭。可是，他所看到的卻是一個由「A-Laws」所製造出來的、扭曲的和平，世界維持在「歪斜」的狀態，於是他決定與能夠改變世界的力量——「GUNDAM」再次一同戰鬥。

## 角色介紹

## 登場機體

## 世界观

## 科技、名詞

## 主題曲

### 第一季

  - 片頭曲

<!-- end list -->

  - 「[DAYBREAK'S BELL](../Page/DAYBREAK'S_BELL.md "wikilink")」（1～13話）
      - 作詞：[hyde](../Page/hyde.md "wikilink")
      - 作曲：[ken](../Page/ken.md "wikilink")
      - 編曲：[L'Arc\~en\~Ciel](../Page/L'Arc~en~Ciel.md "wikilink")
      - 主唱：L'Arc\~en\~Ciel（Ki/oon Records）
  - 「[Ash Like Snow](../Page/Ash_Like_Snow.md "wikilink")」（14～25話）
      - 作詞：[川瀬智子](../Page/川瀬智子.md "wikilink")
      - 作曲：[奥田俊作](../Page/奥田俊作.md "wikilink")
      - 主唱：[the brilliant
        green](../Page/the_brilliant_green.md "wikilink")（DefSTAR
        Records）

<!-- end list -->

  - 片尾曲

<!-- end list -->

  - 「」（1～13話）
      - 作詞：[菅波榮純](../Page/菅波榮純.md "wikilink")
      - 作曲：[THE BACK HORN](../Page/THE_BACK_HORN.md "wikilink")
      - 編曲：THE BACK HORN
      - 主唱：THE BACK HORN（SPEEDSTAR RECORDS）
  - 「」（14～24話）
      - 作詞：[Stephanie](../Page/Stephanie.md "wikilink")、[矢住夏菜](../Page/矢住夏菜.md "wikilink")
      - 作曲：[Joe
        Rinoie](../Page/Joe_Rinoie.md "wikilink")、[MASAKI](../Page/MASAKI.md "wikilink")
      - 編曲：Joe Rinoie、[峰正典](../Page/峰正典.md "wikilink")
      - 主唱：[Stephanie](../Page/Stephanie.md "wikilink")（[日本新力音樂](../Page/日本新力音樂.md "wikilink")）
  - 「DAYBREAK'S BELL」（25話）

<!-- end list -->

  - 插曲

<!-- end list -->

  - 「[LOVE TODAY](../Page/LOVE_TODAY.md "wikilink")」（第19話、第24話）
      - 作詞：[菜穂](../Page/菜穂.md "wikilink")
      - 作曲：菜穂、[佑次](../Page/佑次.md "wikilink")
      - 編曲：[Taja](../Page/Taja.md "wikilink")
      - 主唱：Taja

### 第二季

  - 片頭曲

<!-- end list -->

  - 「」（1\[2\]～13話）
      - 作詞：[TAKUYA](../Page/TAKUYA.md "wikilink")
      - 作曲：[克哉](../Page/克哉.md "wikilink")、TAKUYA
      - 編曲：[UVERworld](../Page/UVERworld.md "wikilink")、[平出悟](../Page/平出悟.md "wikilink")
      - 主唱：UVERworld
  - 「」（14～25話）
      - 作詞：[AIMI](../Page/Stereopony.md "wikilink")
      - 作曲：AIMI
      - 編曲：[stereopony](../Page/stereopony.md "wikilink")、[northa+](../Page/northa+.md "wikilink")
      - 主唱：

<!-- end list -->

  - 片尾曲

<!-- end list -->

  - 「[Prototype](../Page/Prototype_\(單曲\).md "wikilink")」（2～13話）
      - 作詞：[石川智晶](../Page/石川智晶.md "wikilink")
      - 作曲：石川智晶
      - 編曲：
      - 主唱：石川智晶
  - 「[TOMORROW](../Page/TOMORROW.md "wikilink")」（第14話）（第14話、第15話、第18話、第20話、第21話、第25話插曲使用）
      - 作詞：[黑田洋介](../Page/黑田洋介.md "wikilink")、
      - 作曲：[淺見昴生](../Page/淺見昴生.md "wikilink")、[川井憲次](../Page/川井憲次.md "wikilink")
      - 編曲：
      - 主唱：[恒松步](../Page/恒松步.md "wikilink")
  - 「[trust you](../Page/trust_you.md "wikilink")」（15～24話）（第20話插曲使用）
      - 作詞：[MARKIE](../Page/MARKIE.md "wikilink")
      - 作曲：MARKIE
      - 編曲：[JIN NAKAMURA](../Page/JIN_NAKAMURA.md "wikilink")
      - 主唱：[伊藤由奈](../Page/伊藤由奈.md "wikilink")
  - 「[DAYBREAK'S BELL](../Page/DAYBREAK'S_BELL.md "wikilink")」（第25話）

<!-- end list -->

  - 插曲

<!-- end list -->

  - 「[Unlimited
    Sky](../Page/Unlimited_Sky.md "wikilink")」（第7話、第18話、第22話、第25話）
      - 作詞：[Tommy heavenly<sup>6</sup>](../Page/川瀬智子.md "wikilink")
      - 作曲：[Chillion Brownle](../Page/Chillion_Brownle.md "wikilink")
      - 編曲：Chillion Brownle
      - 主唱：[Tommy
        heavenly<sup>6</sup>](../Page/Tommy_heavenly6.md "wikilink")

## 製作人員

  - 监督：[水岛精二](../Page/水岛精二.md "wikilink")
  - 脚本：[黑田洋介](../Page/黑田洋介.md "wikilink")
  - 角色原案：[高河弓](../Page/高河优.md "wikilink")
  - 角色设定：[千叶道德](../Page/千叶道德.md "wikilink")
  - 机械设计：[大河原邦男](../Page/大河原邦男.md "wikilink")、[海老川兼武](../Page/海老川兼武.md "wikilink")、[柳濑敬之](../Page/柳濑敬之.md "wikilink")
  - 机械概念设计：[福地仁](../Page/福地仁.md "wikilink")、[寺冈贤司](../Page/寺冈贤司.md "wikilink")
  - 动画机械设定：[中谷诚一](../Page/中谷诚一.md "wikilink")
  - 音響監督：[三間雅文](../Page/三間雅文.md "wikilink")
  - 音乐：[川井憲次](../Page/川井憲次.md "wikilink")
  - 美术设计：[须江信人](../Page/须江信人.md "wikilink")
  - 色彩设定：[手嶋明美](../Page/手嶋明美.md "wikilink")
  - 美术监督：[佐藤豪志](../Page/佐藤豪志.md "wikilink")
  - 机械考证：[千叶智宏](../Page/千叶智宏.md "wikilink")、[寺冈贤司](../Page/寺冈贤司.md "wikilink")
  - 设定协力：
  - 监制：[竹田青滋](../Page/竹田青滋.md "wikilink")（毎日放送）、[宮河恭夫](../Page/宮河恭夫.md "wikilink")（Sunrise）
  - 制片人：[丸山和雄](../Page/丸山和雄.md "wikilink")（毎日放送）、[池谷浩臣](../Page/池谷浩臣.md "wikilink")（Sunrise）、[佐佐木新](../Page/佐佐木新.md "wikilink")（Sunrise）
  - 制作：[創通](../Page/創通.md "wikilink")、[日昇動畫](../Page/日昇動畫.md "wikilink")、[毎日放送](../Page/毎日放送.md "wikilink")
  - 解说旁白：[古谷徹](../Page/古谷徹.md "wikilink")（日本）；[招世亮](../Page/招世亮.md "wikilink")（香港）；[王希華](../Page/王希華.md "wikilink")（台灣）

## 各集標題

### 第一季

  -
    下表列出第一季共25集及總集篇播放列表，放送日為日本首播時間。
    全劇劇本皆由[黑田洋介撰寫](../Page/黑田洋介.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>話數</p></th>
<th><p>放送日</p></th>
<th><p>日文標題</p></th>
<th><p>大陸標題</p></th>
<th><p>台灣標題</p></th>
<th><p>香港標題</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>角色</p></td>
<td><p>機械</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2007年10月6日</p></td>
<td></td>
<td><p>天-{}-人</p></td>
<td><p>天-{}-上人</p></td>
<td><p>天-{}-人部隊</p></td>
<td><p>大塚健<br />
水島精二</p></td>
<td><p>北村真咲</p></td>
<td><p>千葉道德</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2007年10月13日</p></td>
<td></td>
<td><p>Gundam-{}- Meister</p></td>
<td><p>鋼-{}-彈尖兵</p></td>
<td><p>高-{}-達使者</p></td>
<td><p>寺岡巖</p></td>
<td><p>大和直道</p></td>
<td><p>高村和宏</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2007年10月20日</p></td>
<td></td>
<td><p>改变的世界</p></td>
<td><p>改變的世界</p></td>
<td><p>世界在變</p></td>
<td><p>大原實<br />
水島精二|水島精二</p></td>
<td><p>吉村章</p></td>
<td><p>大貫健一</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2007年10月27日</p></td>
<td></td>
<td><p>对外交涉</p></td>
<td><p>對外折衝</p></td>
<td><p>對外交涉</p></td>
<td><p>松尾衡</p></td>
<td><p><a href="../Page/上田茂.md" title="wikilink">上田茂</a></p></td>
<td><p>新保卓郎</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>2007年11月3日</p></td>
<td></td>
<td><p>脫离临界领域</p></td>
<td><p>脫離臨界領域</p></td>
<td><p>墜毀邊緣</p></td>
<td><p>榎本明廣</p></td>
<td><p>森下博光</p></td>
<td><p>松田寛</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>2007年11月10日</p></td>
<td></td>
<td><p>七剑</p></td>
<td><p>七劍</p></td>
<td><p>七劍</p></td>
<td><p>北村真咲</p></td>
<td><p>松川哲也</p></td>
<td><p>佐村義一<br />
有澤寛</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>2007年11月17日</p></td>
<td></td>
<td><p>无法平复的灵魂</p></td>
<td><p>未瞑之魂</p></td>
<td><p>夢魘難平</p></td>
<td><p>寺岡巖</p></td>
<td><p>大和直道</p></td>
<td><p>千葉道德</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>2007年11月24日</p></td>
<td></td>
<td><p>无差別报复</p></td>
<td><p>無限制報復</p></td>
<td><p>隨-{}-機報復</p></td>
<td><p>木村真一郎</p></td>
<td><p>吉村章</p></td>
<td><p>大貫健一</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>2007年12月1日</p></td>
<td></td>
<td><p>大国的威信</p></td>
<td><p>大國的威信</p></td>
<td><p>大國威信</p></td>
<td><p>上田茂</p></td>
<td><p>新保卓郎</p></td>
<td><p>高瀨健一</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>2007年12月8日</p></td>
<td></td>
<td><p>高-{}-达捕获作战</p></td>
<td><p>鋼-{}-彈捕獲作戰</p></td>
<td><p>俘擄高-{}-達作戰</p></td>
<td><p>榎本明廣</p></td>
<td><p>森下博光</p></td>
<td><p>松田寛</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>2007年12月15日</p></td>
<td></td>
<td><p>阿雷-{}-路亚</p></td>
<td><p>阿雷-{}-路亞</p></td>
<td><p>阿-{}-路耶</p></td>
<td><p>北村真咲</p></td>
<td><p>今泉良一</p></td>
<td><p>佐村義一</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>2007年12月22日</p></td>
<td></td>
<td><p>朝向教义的尽头</p></td>
<td><p>教義的結果</p></td>
<td><p>教義之終</p></td>
<td><p>角田一樹</p></td>
<td><p>松川哲也</p></td>
<td><p>有澤寛</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>2008年1月5日</p></td>
<td></td>
<td><p>圣者的归来</p></td>
<td><p>聖者的歸還</p></td>
<td><p>聖者回歸</p></td>
<td><p>長崎健司</p></td>
<td><p>大貫健一</p></td>
<td><p>西井正典</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>2008年1月12日</p></td>
<td></td>
<td><p>决意的清晨</p></td>
<td><p>決意的早晨</p></td>
<td><p>決意之朝</p></td>
<td><p>大和直道<br />
水島精二</p></td>
<td><p>大和直道</p></td>
<td><p>新保卓郎</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>2008年1月19日</p></td>
<td></td>
<td><p>折断之翼</p></td>
<td><p>折斷的翅膀</p></td>
<td><p>斷折之翼</p></td>
<td><p>西本由紀夫</p></td>
<td><p>上田茂</p></td>
<td><p>森下博光</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>2008年1月26日</p></td>
<td></td>
<td><p>三位一体</p></td>
<td><p>崔-{}-尼提</p></td>
<td><p>三位一體</p></td>
<td><p>北村真咲</p></td>
<td><p>今泉良一</p></td>
<td><p>松田寛</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>2008年2月2日</p></td>
<td></td>
<td><p>座天使的强袭</p></td>
<td><p>座天使來襲</p></td>
<td><p>斯洛尼強襲</p></td>
<td><p>榎本明廣</p></td>
<td><p>松川哲也</p></td>
<td><p>有澤寛</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p>2008年2月9日</p></td>
<td></td>
<td><p>恶意之矛</p></td>
<td><p>惡意所向</p></td>
<td><p>惡意之矛</p></td>
<td><p>長崎健司</p></td>
<td><p>大貫健一</p></td>
<td><p>西井正典</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p>2008年2月16日</p></td>
<td></td>
<td><p>羁绊</p></td>
<td><p>羈絆</p></td>
<td><p>羈絆</p></td>
<td><p>木村真一郎<br />
水島精二</p></td>
<td><p>上田茂</p></td>
<td><p>新保卓郎</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>2008年2月23日</p></td>
<td></td>
<td><p>变革之刃</p></td>
<td><p>變革之刃</p></td>
<td><p>變革之刃</p></td>
<td><p>角田一樹</p></td>
<td><p>森下博光</p></td>
<td><p>佐村義一</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>2008年3月1日</p></td>
<td></td>
<td><p>灭亡的道路</p></td>
<td><p>毀滅之道</p></td>
<td><p>滅亡之路</p></td>
<td><p>大和直道<br />
角田一樹<br />
水島精二</p></td>
<td><p>大和直道</p></td>
<td><p>今泉良一</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p>2008年3月8日</p></td>
<td></td>
<td><p>Trans-AM</p></td>
<td><p>Trans-AM</p></td>
<td><p>Trans-AM</p></td>
<td><p>北村真咲</p></td>
<td><p>松川哲也</p></td>
<td><p>有澤寛</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p>2008年3月15日</p></td>
<td></td>
<td><p>制止全世界</p></td>
<td><p>阻止世界</p></td>
<td><p>制止世界</p></td>
<td><p>長崎健司</p></td>
<td><p>大貫健一</p></td>
<td><p>西井正典</p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>2008年3月22日</p></td>
<td></td>
<td><p>无尽的诗篇</p></td>
<td><p>無盡的詩歌</p></td>
<td><p>無盡哀詩</p></td>
<td><p>榎本明廣<br />
水島精二<br />
長崎健司<br />
北村真咲</p></td>
<td><p>上田茂</p></td>
<td><p>新保卓郎</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p>2008年3月29日</p></td>
<td></td>
<td><p>剎那</p></td>
<td><p>剎那</p></td>
<td><p>剎那</p></td>
<td><p>寺岡巖<br />
角田一樹<br />
水島精二</p></td>
<td><p>角田一樹<br />
水島精二</p></td>
<td><p>千葉道德</p></td>
</tr>
<tr class="odd">
<td><p>總集篇</p></td>
<td><p>2008年10月3日</p></td>
<td></td>
<td><p>天使們的軌跡</p></td>
<td><p>天使們的軌跡</p></td>
<td><p>眾天使的軌跡[3]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 第二季

  -
    下表列出第二季共25集播放列表，放送日為日本首播時間。
    全劇劇本皆由[黑田洋介撰寫](../Page/黑田洋介.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>話數</p></th>
<th><p>放送日</p></th>
<th><p>日文標題</p></th>
<th><p>台灣標題</p></th>
<th><p>香港標題</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>角色</p></td>
<td><p>機械</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2008年10月5日</p></td>
<td></td>
<td><p>天使再臨</p></td>
<td><p>天使再臨</p></td>
<td><p>水島精二<br />
角田一樹</p></td>
<td><p>角田一樹</p></td>
<td><p>千葉道德</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2008年10月12日</p></td>
<td></td>
<td><p>雙重動力裝置</p></td>
<td><p>-{Twin Drive}-</p></td>
<td><p>北村真咲</p></td>
<td><p>今泉良一</p></td>
<td><p>有澤寬</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2008年10月19日</p></td>
<td></td>
<td><p>阿雷-{}-路亞奪還作戰</p></td>
<td><p>營救阿-{}-路耶行動</p></td>
<td><p>長崎健司</p></td>
<td><p>大貫健一</p></td>
<td><p>西井正典</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2008年10月26日</p></td>
<td></td>
<td><p>戰鬥的理由</p></td>
<td><p>戰鬥理由</p></td>
<td><p>寺岡巖</p></td>
<td><p>宅野誠起</p></td>
<td><p>新保卓郎</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>2008年11月2日</p></td>
<td></td>
<td><p>故國燃燒</p></td>
<td><p>故國燃燒</p></td>
<td><p>上田茂</p></td>
<td><p>森下博光</p></td>
<td><p>松田寛</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>2008年11月9日</p></td>
<td></td>
<td><p>傷痕</p></td>
<td><p>傷痕</p></td>
<td><p>北村真咲</p></td>
<td><p>名取孝浩</p></td>
<td><p>松川哲也</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>2008年11月16日</p></td>
<td></td>
<td><p>再會與離別</p></td>
<td><p>再會、離別</p></td>
<td><p>角田一樹</p></td>
<td><p>角田一樹<br />
水島精二</p></td>
<td><p>千葉道德</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>2008年11月23日</p></td>
<td></td>
<td><p>純潔的扭曲</p></td>
<td><p>純潔蒙塵</p></td>
<td><p>長崎健司</p></td>
<td><p>高田正典</p></td>
<td><p>大貫健一</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>2008年11月30日</p></td>
<td></td>
<td><p>無法拭去的過去</p></td>
<td><p>往昔難拭</p></td>
<td><p>寺岡巖</p></td>
<td><p>宅野誠起</p></td>
<td><p>新保卓郎</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>2008年12月7日</p></td>
<td></td>
<td><p>天之光</p></td>
<td><p>天上之光</p></td>
<td><p>上田茂</p></td>
<td><p>森下博光</p></td>
<td><p>松田寛</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>2008年12月14日</p></td>
<td></td>
<td><p>00之聲</p></td>
<td><p>00之聲</p></td>
<td><p>北村真咲</p></td>
<td><p>松川哲也</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>2008年12月21日</p></td>
<td></td>
<td><p>在宇宙等待</p></td>
<td><p>在宇宙中等你</p></td>
<td><p>長崎健司</p></td>
<td><p>池田佳代</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>2008年12月28日</p></td>
<td></td>
<td><p>死兆-{}-砲攻略戰</p></td>
<td><p>-{Memento mori}-攻略戰</p></td>
<td><p>寺岡巖</p></td>
<td><p>角田一樹<br />
高橋正典</p></td>
<td><p>新保卓郎</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>2009年1月11日</p></td>
<td></td>
<td><p>聽見歌聲</p></td>
<td><p>歌聲蕩耳</p></td>
<td><p>宅野誠起</p></td>
<td><p>大貫健一</p></td>
<td><p>西井正典</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>2009年1月18日</p></td>
<td></td>
<td><p>反抗的凱歌</p></td>
<td><p>反抗凱歌</p></td>
<td><p>上田茂</p></td>
<td><p>森下博光</p></td>
<td><p>松田寛</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>2009年1月25日</p></td>
<td></td>
<td><p>悲劇的序章</p></td>
<td><p>悲劇序篇</p></td>
<td><p>寺岡巖</p></td>
<td><p>北村真咲</p></td>
<td><p>松川哲也</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>2009年2月1日</p></td>
<td></td>
<td><p>在消散的光芒中</p></td>
<td><p>消散光芒之中</p></td>
<td><p>長崎健司</p></td>
<td><p>池田佳代</p></td>
<td><p>中谷誠一<br />
有澤寬</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p>2009年2月8日</p></td>
<td></td>
<td><p>交錯的思念</p></td>
<td><p>思念交錯</p></td>
<td><p>角田一樹<br />
水島精二</p></td>
<td><p>角田一樹</p></td>
<td><p>新保卓郎</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p>2009年2月15日</p></td>
<td></td>
<td><p>變革者之影</p></td>
<td><p>變革者之影</p></td>
<td><p>北村真咲</p></td>
<td><p>宅野誠起</p></td>
<td><p>大貫健一</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>2009年2月22日</p></td>
<td></td>
<td><p>艾-{}-紐回歸</p></td>
<td><p>艾-{}-燎回歸</p></td>
<td><p>寺岡巖</p></td>
<td><p>綿田慎也</p></td>
<td><p>森下博光</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>2009年3月1日</p></td>
<td></td>
<td><p>革新之扉</p></td>
<td><p>革新之扉</p></td>
<td><p>上田茂</p></td>
<td><p>松川哲也</p></td>
<td><p>阿部邦博</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p>2009年3月8日</p></td>
<td></td>
<td><p>為了未來</p></td>
<td><p>為了未來</p></td>
<td><p>寺岡巖<br />
角田一樹<br />
水島精二</p></td>
<td><p>名取孝浩</p></td>
<td><p>池田佳代</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p>2009年3月15日</p></td>
<td></td>
<td><p>生命之華</p></td>
<td><p>生命之華</p></td>
<td><p>北村真咲</p></td>
<td><p>宅野誠起</p></td>
<td><p>上田茂</p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>2009年3月22日</p></td>
<td></td>
<td><p>BEYOND</p></td>
<td><p>BEYOND</p></td>
<td><p>長崎健司</p></td>
<td><p>大貫健一</p></td>
<td><p>西井正典</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p>2009年3月29日</p></td>
<td></td>
<td><p>再生</p></td>
<td><p>再生</p></td>
<td><p>寺岡巖<br />
角田一樹<br />
水島精二</p></td>
<td><p>角田一樹<br />
水島精二</p></td>
<td><p>千葉道德<br />
森下博光</p></td>
</tr>
</tbody>
</table>

## 播放電視台

  - 日本

<!-- end list -->

  - 香港

<!-- end list -->

  - 第一季：[無綫電視於](../Page/無綫電視.md "wikilink")2008年9月6日起逢星期六11:30－12:00播放，[翡翠台與](../Page/翡翠台.md "wikilink")[高清翡翠台同步播放](../Page/高清翡翠台.md "wikilink")，並於2009年6月10日起逢星期一至五22:00－22:30在[J2重播](../Page/J2.md "wikilink")（於2009年6月29日改至21:30－22:00播出）。而總集篇則在2009年12月26日10:30－10:55於[翡翠台及](../Page/翡翠台.md "wikilink")[高清翡翠台同步播放](../Page/高清翡翠台.md "wikilink")，並於2011年6月25日08:15－08:40[高清翡翠台重播](../Page/高清翡翠台.md "wikilink")。
  - 第二季：[無綫電視及後於](../Page/無綫電視.md "wikilink")2010年1月2日起逢星期六10:30－11:00於[翡翠台繼續播放](../Page/翡翠台.md "wikilink")，並於2011年6月25日08:40－09:05在2011年7月3日起逢星期日08:10－09:05在[高清翡翠台重播](../Page/高清翡翠台.md "wikilink")（每次連播兩集）。

<!-- end list -->

  - 台灣

<!-- end list -->

  - 美国

<!-- end list -->

  - 第一季：英文配音版於2008年11月24日在[Sci-Fi频道的](../Page/Sci-Fi.md "wikilink")「Ani-Monday」中首播，一次播出两话。
  - 第二季：英文配音版於2009年6月29日在[Sci-Fi频道的](../Page/Sci-Fi.md "wikilink")「Ani-Monday」中首播，一次播出两话。

## 劇場版

## 相關產品

**日期未標註地區者皆為日本之發售日期。**

### DVD

  - 第一季

<!-- end list -->

  - VOL.1 收录第1～2话（日本2008年1月25日／台灣2008年8月13日發售）
  - VOL.2 收录第3～6话（日本2008年2月2日／台灣2008年9月25日發售）
  - VOL.3 收录第7～10话（日本2008年3月25日／台灣2008年10月25日發售）
  - VOL.4 收录第11～14话（日本2008年4月25日／台灣2009年1月22日發售）
  - VOL.5 收录第15～18话（日本2008年5月23日／台灣2009年1月22日發售）
  - VOL.6 收录第19～22话（日本2008年6月25日／台灣2009年2月6日發售）
  - VOL.7 收录第23～25话（日本2008年7月25日／台灣2009年2月6日發售）

<!-- end list -->

  - 第二季

<!-- end list -->

  - VOL.1 收錄第1～2话（日本2009年2月20日／台灣2009年4月28日發售）
  - VOL.2 收錄第3～6话（日本2009年3月27日／台灣2009年5月26日發售）
  - VOL.3 收錄第7～10话（日本2009年4月24日／台灣2009年6月26日發售）
  - VOL.4 收錄第11～14话（日本2009年5月26日／台灣2009年7月24日發售）
  - VOL.5 收錄第15～18话（日本2009年6月26日／台灣2009年8月12日發售）
  - VOL.6 收錄第19～22话（日本2009年7月24日／台灣2009年9月25日發售）
  - VOL.7 收錄第23～25话（日本2009年8月25日／台灣2009年10月26日發售）

<!-- end list -->

  - 特別篇

<!-- end list -->

  - VOL.1 CELESTIAL BEING （日本2009年10月27日發售）
  - VOL.2 END OF WORLD （日本2009年12月22日發售）
  - VOL.3 RETURN THE WORLD （日本2010年2月23日發售）

### 藍光光碟

  - 第一季

<!-- end list -->

  - VOL.1 收錄第1～2話（2008年8月22日發售）
  - VOL.2 收錄第3～6話（2008年8月22日發售）
  - VOL.3 收錄第7～10話（2008年9月26日發售）
  - VOL.4 收錄第11～14話（2008年10月24日發售）
  - VOL.5 收錄第15～18話（2008年11月21日發售）
  - VOL.6 收錄第19～22話（2008年12月19日發售）
  - VOL.7 收錄第23～25話（2009年1月23日發售）

<!-- end list -->

  - 第二季

<!-- end list -->

  - VOL.1 收錄第1～2話（2009年2月20日發售）
  - VOL.2 收錄第3～6話（2009年3月27日發售）
  - VOL.3 收錄第7～10話（2009年4月24日發售）
  - VOL.4 收錄第11～14話（2009年5月26日發售）
  - VOL.5 收錄第15～18話（2009年6月26日發售）
  - VOL.6 收錄第19～22話（2009年7月24日發售）
  - VOL.7 收錄第23～25話（2009年8月25日發售）

<!-- end list -->

  - 特別篇

<!-- end list -->

  - VOL.1 CELESTIAL BEING （2009年10月27日發售）
  - VOL.2 END OF WORLD （2009年12月22日發售）
  - VOL.3 RETURN THE WORLD （2010年2月23日發售）

### UMD

  - 特別篇

<!-- end list -->

  - VOL.1 CELESTIAL BEING （2009年10月27日發售）
  - VOL.2 END OF WORLD （2009年12月22日發售）
  - VOL.3 RETURN THE WORLD （2010年2月23日發售）

### CD

  - 角色單曲

<!-- end list -->

  - （2008年8月13日發售）

  - （2008年9月24日發售）

  - （2008年10月22日發售）

  - （2008年11月19日發售）

  - （2009年8月26日發售）

  - （2009年10月21日發售）

  - （2010年2月24日發售）

<!-- end list -->

  - 廣播劇

<!-- end list -->

  - （2008年7月23日發售）

  - （2008年9月24日發售）

  - （2009年6月10日發售）

  - （2009年7月8日發售）

<!-- end list -->

  - 電台廣播CD

<!-- end list -->

  - （2008年8月30日發售）

  - （2008年11月19日發售）

  - （2009年3月31日發售）

### 遊戲

  - NDS

<!-- end list -->

  - 「機動戰士GUNDAM 00」（2008年3月27日發售）

<!-- end list -->

  - PlayStation 2

<!-- end list -->

  - 「機動戰士GUNDAM 00 -{zh-hans:Gundam Meisters; zh-tw:鋼彈尖兵;
    zh-hk:高達使者;}-」（2008年10月16日發售）

<!-- end list -->

  - PlayStation Portable

<!-- end list -->

  - 2011年4月11日[南夢宮萬代發售](../Page/南夢宮萬代.md "wikilink")[PlayStation
    Portable用遊戲軟體](../Page/PlayStation_Portable.md "wikilink")『[第2次超級機器人大戰Z](../Page/第2次超級機器人大戰Z.md "wikilink")』。

## 外傳

### 機動戰士GUNDAM 00P

  -
    於[電擊
    Hobby連載](../Page/電擊_Hobby.md "wikilink")，第一季故事時間點設定在西元2292年，第二季則是西元2302年。
    連載完結，小說2本，共有25話。

### 機動戰士GUNDAM 00F

  -
    於[GUNDAM
    ACE連載](../Page/GUNDAM_ACE.md "wikilink")，故事時間點設定在西元2307年（與本傳第一季相同）至本傳第二季前，主角是隸屬於天使(天上人的支援團體)，第五位鋼彈尖兵－馮恩·史帕克(Fon
    Spaak)。
    連載完結，單行本4卷，共有20話。

### 機動戰士GUNDAM 00V

  -
    於[Hobby
    Japan連載](../Page/Hobby_Japan.md "wikilink")，利用單元式的方法為本傳的內容做補充，故事時間點設定在本傳第一季至第二季。
    連載完結，解說書1本，共有23話。

### 機動戰士GUNDAM 00V戰記

  -
    於Hobby Japan連載，故事時間點設定在本傳第二季之後時間。
    連載結束，解說書1本，共有18話。

### 機動戰士GUNDAM 00I

  -
    於GUNDAM ACE連載，故事時間點設定在西元2312年（與本傳第二季相同）。
    連載完結，單行本3卷，共有14話。

### 機動戰士GUNDAM 00I 2314

  -
    於GUNDAM ACE連載，第一季00I兩年後開始（與本傳劇場版相同），00外傳的最後作品。
    連載完結，單行本1卷，共有4話。

### 機動戰士GUNDAM 00N

  -
    於電擊Hobby連載，故事時間點設定在機動戰士GUNDAM 00P之後時間，以報導的觀點看00世界。
    連載結束，解說書1本，共有16話。

## 參考文獻及註釋

### 註釋

### 參考文獻

## 外部連結

  - [日本GUNDAM 00
    劇場網站](https://web.archive.org/web/20070610085515/http://www.gundam00.net/index.html)

  - [日本GUNDAM 00 官方網站](http://www.gundam00.net/tv/index.html)

  - [日本GUNDAM 00 外傳網站](http://www.gundam00.net/SS/index.html)

  - [台灣GUNDAM 00 官方網站](http://www.my-cartoon.com.tw/gundam00/)

  - [機動戰士鋼彈00劇場版(A wakening of the Trailblazer) -
    Yahoo\!奇摩電影](http://tw.movie.yahoo.com/movieinfo_main.html/id=3608)

  - [优酷独播专区](http://www.youku.com/show_page/id_zcc0043b4962411de83b1.html)

  - [土豆网独播专区](http://www.tudou.com/albumcover/UQN-WBh1flU.html)

  - [爱奇艺独播专区（第一季）](https://web.archive.org/web/20140222051024/http://www.iqiyi.com/dongman/jdzsgd001.html)

  - [爱奇艺独播专区（第二季）](http://www.iqiyi.com/a_19rriftre6.html)

  - [敢达00剧场版 2010:觉醒的尖兵-优酷独播专区](http://www.youku.com/show_page/id_z76a56f8cb33911e0a046.html)

[\*](../Category/GUNDAM_00.md "wikilink")
[Category:未來題材作品](../Category/未來題材作品.md "wikilink")
[Category:2007年TBS電視網動畫](../Category/2007年TBS電視網動畫.md "wikilink")
[Category:2008年TBS電視網動畫](../Category/2008年TBS電視網動畫.md "wikilink")
[Category:日昇動畫](../Category/日昇動畫.md "wikilink")
[Category:機器人動畫](../Category/機器人動畫.md "wikilink")
[Category:中視外購動畫](../Category/中視外購動畫.md "wikilink")
[Category:2300年代背景作品](../Category/2300年代背景作品.md "wikilink")
[Category:2310年代背景作品](../Category/2310年代背景作品.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:KEROKERO ACE](../Category/KEROKERO_ACE.md "wikilink")
[Category:GUNDAM ACE](../Category/GUNDAM_ACE.md "wikilink")
[Category:月刊Newtype連載作品](../Category/月刊Newtype連載作品.md "wikilink")
[Category:月刊少年Magazine](../Category/月刊少年Magazine.md "wikilink")
[Category:月刊Magazine Z](../Category/月刊Magazine_Z.md "wikilink")
[Category:日本科幻小說](../Category/日本科幻小說.md "wikilink")
[Category:無綫電視外購動畫](../Category/無綫電視外購動畫.md "wikilink")
[Category:高河弓](../Category/高河弓.md "wikilink") [Double
O](../Category/GUNDAM系列.md "wikilink")

1.
2.  此曲在第一話片尾播放時字幕上官方寫為主題曲，意自第二季將揭開序幕，所以不是片尾曲
3.  依據電視廣播有限公司翡翠台及高清翡翠台，於2009年12月26日上午10時播放時的實際內容。