<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

**亞爾-薩卡索爾**（**Ar-Sakalthôr**），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·瑞爾·托爾金](../Page/約翰·羅納德·瑞爾·托爾金.md "wikilink")（J.R.R.
Tolkien）的史詩式奇幻作品《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》中的虛構人物。

他是[努曼諾爾](../Page/努曼諾爾.md "wikilink")（Númenor）第二十二任皇帝。

## 名字

**薩卡索爾**（Sakalthôr）的名字來自阿督納克語，其[昆雅語相應版本為](../Page/昆雅語.md "wikilink")**法拉斯昂**（Falassion），兩者意思應該一樣，解作「海岸之王」*King
of
Coasts*。\[1\]「亞爾」（Ar-）是[努曼諾爾皇帝加在自己封號前的](../Page/努曼諾爾皇帝.md "wikilink")[阿督納克語前綴](../Page/阿督納克語.md "wikilink")，有着「高的、高貴的」的意思。

## 總覽

亞爾-薩卡索爾是[努曼諾爾人](../Page/努曼諾爾人.md "wikilink")（Númenórean），是皇帝[亞爾-辛拉松](../Page/亞爾-印卡松.md "wikilink")（Ar-Zimrathôn）的兒子和繼承人。\[2\]不清楚他有沒有任何兄弟姊妹。他的兒子是[金密索爾](../Page/亞爾-金密索爾.md "wikilink")（Gimilzôr）。\[3\]

薩卡索爾是第二十二任努曼諾爾皇帝，進一步反對維拉和精靈，追求長生不死，並以阿督納克語自封帝號。他任內努曼諾爾繼續享受長期昇平和富裕，並繼續向[中土大陸擴張勢力](../Page/中土大陸.md "wikilink")，但國民都已反叛[維拉](../Page/維拉.md "wikilink")。他從父親繼承了寶劍[阿蘭路斯](../Page/阿蘭路斯.md "wikilink")（Aranrúth），這把劍只傳承於歷代皇帝之間，是努曼諾爾皇帝的權力象徵。\[4\]

他生於第二紀元2867年，在3102年去世，享年226歲。\[5\]

## 生平

薩卡索爾生於[第二紀元](../Page/第二紀元.md "wikilink")2867年的努曼諾爾島上。\[6\]3033年，他的父親亞爾-辛拉松去世，\[7\]薩卡索爾接過皇權，繼續執行父祖反對維拉的政策。

他統治的同期，[安督奈伊領主是](../Page/安督奈伊領主.md "wikilink")[伊雅仁督爾](../Page/安督奈伊領主#安督奈伊領主列表.md "wikilink")（Eärendur）。皇帝強逼了伊雅仁督爾的妹妹[林朵瑞依](../Page/林朵瑞依.md "wikilink")（Lindórië）之女[印希爾貝絲](../Page/印希爾貝絲.md "wikilink")（Inzilbêth）嫁給他的兒子金密索爾。\[8\]

第二紀元3102年，\[9\]亞爾-薩卡索爾去世，結束了69年的統治，由他的兒子金密索爾繼位。\[10\]

## 半精靈家系

## 參考

  - 《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》
      - 《[努曼諾爾淪亡史](../Page/努曼諾爾淪亡史.md "wikilink")》
  - 《[未完成的故事](../Page/未完成的故事.md "wikilink")》

## 資料來源

[category:中土大陸的角色](../Page/category:中土大陸的角色.md "wikilink")
[category:中土大陸的登丹人](../Page/category:中土大陸的登丹人.md "wikilink")
[category:精靈寶鑽中的人物](../Page/category:精靈寶鑽中的人物.md "wikilink")

[en:Kings of
Númenor\#Ar-Sakalthôr](../Page/en:Kings_of_Númenor#Ar-Sakalthôr.md "wikilink")
[no:Númenoreanske
herskere\#Ar-Sakalthôr](../Page/no:Númenoreanske_herskere#Ar-Sakalthôr.md "wikilink")
[pl:Królowie
Númenoru\#Ar-Sakalthôr](../Page/pl:Królowie_Númenoru#Ar-Sakalthôr.md "wikilink")

1.  [Encyclopedia of Arda:
    Tar-Falassion](http://www.glyphweb.com/arda/t/tarfalassion.html)

2.
3.
4.
5.
6.
7.
8.  《精靈寶鑽》 2002年 聯經初版翻譯 《[努曼諾爾淪亡史](../Page/努曼諾爾淪亡史.md "wikilink")》

9.
10.