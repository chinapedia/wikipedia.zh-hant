**二七纪念塔**，全称**郑州二七大罢工纪念塔**，是[河南省](../Page/河南省.md "wikilink")[郑州市的标志性建筑](../Page/郑州市.md "wikilink")，建成于1971年9月29日，为纪念发生于1923年2月7日的[二七大罢工而修建](../Page/二七大罢工.md "wikilink")。

二七纪念塔位于郑州市中心的[二七广场](../Page/二七广场.md "wikilink")，为钢筋混凝土结构，塔高63米，共14层。\[1\]其中塔基座为3层，塔身为11层。每层顶角为仿古挑角飞檐，绿色琉璃瓦覆顶。塔顶建有钟楼，装有六面直径2.7米的大钟，整点报时演奏乐曲，悠扬悦耳。塔平面为东西相连的两个五边形，从东西方向看为单塔，从南北方向看则为双塔。
塔内一边为旋梯，一边为展室，游人可登至塔顶，远眺市容。入夜后，多种彩灯内透外照，使双塔更加绚丽多彩。\[2\]塔顶有印有“[中国共产党万岁](../Page/中国共产党万岁.md "wikilink")”口号的霓虹灯。

二七纪念塔曾是郑州的最高建筑。在八十年代以前，晴天登临塔顶，可以远眺一线[黄河从](../Page/黄河.md "wikilink")[邙山流过的景色](../Page/邙山.md "wikilink")。\[3\]但在今天，地处商业区的二七塔已被周围的高楼大厦所包围，不能再观赏此景。

## 参看

  - [河南广播电视塔](../Page/河南广播电视塔.md "wikilink")

## 注释

<div class="references-small">

<references />

</div>

[Category:郑州文物保护单位](../Category/郑州文物保护单位.md "wikilink")
[Category:郑州地标](../Category/郑州地标.md "wikilink")
[Category:二七区](../Category/二七区.md "wikilink")
[豫](../Category/中华人民共和国纪念塔.md "wikilink")
[Category:国家三级博物馆](../Category/国家三级博物馆.md "wikilink")
[Category:1971年完工建築物](../Category/1971年完工建築物.md "wikilink")
[Category:二七大罢工](../Category/二七大罢工.md "wikilink")

1.  [河南“二七”纪念塔](http://www.china.org.cn/chinese/TR-c/134361.htm)
    ，[中国网](../Page/中国网.md "wikilink")2002年4月18日。
2.
3.  《话说河南》，王乃平著。 河南出版社, 1996