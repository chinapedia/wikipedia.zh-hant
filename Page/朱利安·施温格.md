**朱利安·西摩·施温格**（，），[犹太裔](../Page/犹太.md "wikilink")[美国](../Page/美国.md "wikilink")[理论物理学家](../Page/理论物理学.md "wikilink")，[量子电动力学的创始人之一](../Page/量子电动力学.md "wikilink")，与[理查德·费曼](../Page/理查德·费曼.md "wikilink")、[朝永振一郎共获](../Page/朝永振一郎.md "wikilink")1965年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

施温格出生于[纽约](../Page/纽约.md "wikilink")，16岁就发表了第一篇物理论文。他起先在[纽约市立学院求学](../Page/纽约市立学院.md "wikilink")，后转至[哥伦比亚大学学习](../Page/哥伦比亚大学.md "wikilink")，师从著名物理学家[伊西多·拉比](../Page/伊西多·拉比.md "wikilink")，并于1939年获得博士学位，毕业后在[伯克利加州大学和](../Page/伯克利加州大学.md "wikilink")[普渡大学任教](../Page/普渡大学.md "wikilink")，并曾一度担任[奥本海默的助手](../Page/罗伯特·奥本海默.md "wikilink")；二战期间，施温格从事了有关[雷达和](../Page/雷达.md "wikilink")[加速器的研究](../Page/加速器.md "wikilink")；1945年出任[哈佛大学](../Page/哈佛大学.md "wikilink")[教授](../Page/教授.md "wikilink")，但在70年代因[有效场论的论点与同事不和](../Page/有效场论.md "wikilink")，离开哈佛至[加州大学洛杉矶分校任教直至退休](../Page/加州大学洛杉矶分校.md "wikilink")。\[1\]
[缩略图](https://zh.wikipedia.org/wiki/File:Julian_Schwinger,_1965.jpg "fig:缩略图")

## 参考资料

<references/>

[S](../Category/美国物理学家.md "wikilink")
[S](../Category/美國猶太人.md "wikilink")
[S](../Category/诺贝尔物理学奖获得者.md "wikilink")
[S](../Category/美國諾貝爾獎獲得者.md "wikilink")
[S](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[S](../Category/紐約市立學院校友.md "wikilink")
[S](../Category/哥倫比亞大學校友.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:阿尔伯特·爱因斯坦奖获得者](../Category/阿尔伯特·爱因斯坦奖获得者.md "wikilink")

1.  [Julian Schwinger -
    Biographical](http://www.nobelprize.org/nobel_prizes/physics/laureates/1965/schwinger-bio.html)
    The Nobel Prize in Physics 1965, Nobelprize.org