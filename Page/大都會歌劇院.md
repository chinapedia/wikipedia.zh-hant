[Metropolitan_Opera_House_At_Lincoln_Center.jpg](https://zh.wikipedia.org/wiki/File:Metropolitan_Opera_House_At_Lincoln_Center.jpg "fig:Metropolitan_Opera_House_At_Lincoln_Center.jpg")的大都會歌劇院，攝於林肯中心廣場\]\]
[Metropolitan_Opera_House,_a_concert_by_pianist_Josef_Hofmann_-_NARA_541890_-_Edit.jpg](https://zh.wikipedia.org/wiki/File:Metropolitan_Opera_House,_a_concert_by_pianist_Josef_Hofmann_-_NARA_541890_-_Edit.jpg "fig:Metropolitan_Opera_House,_a_concert_by_pianist_Josef_Hofmann_-_NARA_541890_-_Edit.jpg")的音樂會，1937年11月28日\]\]

**大都會歌劇院**（）是位於[美國](../Page/美國.md "wikilink")[紐約的](../Page/紐約.md "wikilink")[林肯中心內的世界知名的](../Page/林肯中心.md "wikilink")[歌劇院](../Page/歌劇院.md "wikilink")，由「大都會歌劇院協會」（）負責營運。大都會歌劇院協會於1880年4月成立，是纽约主要的歌劇公司，也是美國最大的古典音樂組織，每年上演約240部歌劇。大都會歌劇院是林肯中心的十二個常駐組織之一。

## 公司

大都會歌劇院成立於1880年，作為音樂學院的替代品。音樂學院代表著紐約社會的最上流階級，學院委員會總監不願意讓後起的富裕家庭加入成為會員。最初加入大都會劇院的成員包括摩根（Morgan）、羅斯福（Roosevelt）、阿斯托（Astor）及范德堡（Vanderbilt）家族。他們成立的大都會歌劇院比音樂學院更加悠久。[Henry
Abbey出任開幕季的經理](../Page/w:en:Henry_Eugene_Abbey.md "wikilink")。

後來，歌劇由酬金較為便宜的德國歌唱家並混以部份享譽德國的歌唱家一同演出一些國際劇目，但演出的語言卻為德文。

## 历任首席指揮

  - （Yannick Nézet-Séguin）（2017年－2018年）待任音乐总监；（2018年－至今）音樂總監

  - （Fabio Luisi）（2010年－2011年）首席客座指挥；（2011年－2017年）首席指挥

  - [瓦列里·格吉耶夫](../Page/瓦列里·格吉耶夫.md "wikilink")（Valery
    Gergiev）（1997年－2008年）首席客座指挥

  - [詹姆士·列文](../Page/詹姆士·列文.md "wikilink")（James
    Levine）（1976年－2016年）音樂總監；（2016年－2017年）荣誉音乐总监

  - [拉法埃尔·库贝里克](../Page/拉法埃尔·库贝里克.md "wikilink")（Rafael
    Kubelík）（1973年－1974年）音乐总监

  - （Kurt Adler）（1943年－1973年）

  - [埃里希·萊因斯朵夫](../Page/埃里希·萊因斯朵夫.md "wikilink")（Erich
    Leinsdorf）（1956年－1962年）

  - [季米特里斯·米特罗普洛斯](../Page/季米特里斯·米特罗普洛斯.md "wikilink")（Dimitri
    Mitropoulos）（1954年－1960年）

  - [弗里兹·莱纳](../Page/弗里兹·莱纳.md "wikilink")（Fritz Reiner）（1949年－1953年）

  - [弗里兹·布什](../Page/弗里兹·布什.md "wikilink")（Fritz Busch）（1945年－1949年）

  - （Cesare Sodero）（1942年－1947年）

  - [乔治·塞尔](../Page/乔治·塞尔.md "wikilink")（George Szell）（1942年－1946年）

  - [埃里希·萊因斯朵夫](../Page/埃里希·萊因斯朵夫.md "wikilink")（Erich
    Leinsdorf）（1938年－1942年）德國劇目首席指挥

  - （Ettore Panizza）（1934年－1942年）意大利剧目首席指挥

  - [布鲁诺·瓦尔特](../Page/布鲁诺·瓦尔特.md "wikilink")（Bruno
    Walter）（1941－1951年，1956年，1959年）

  - （Fausto Cleva）（1931年－1971年）

  - （Tullio Serafin）（1924年－1934年）

  - [阿图尔·博丹茨基](../Page/阿图尔·博丹茨基.md "wikilink")（Artur
    Bodanzky）（1915年－1939年）德國劇目首席指挥

  - [阿图罗·托斯卡尼尼](../Page/阿图罗·托斯卡尼尼.md "wikilink")（Arturo
    Toscanini）（1908年－1915年）

  - [古斯塔夫·馬勒](../Page/古斯塔夫·馬勒.md "wikilink")（Gustav Mahler）（1908年－1910年）

  - （Alfred Hertz）（1902年－1915年）德國劇目首席指挥

  - （Walter Damrosch）（1884年－1902年）

  - （Anton Seidl）（1885年－1897年）

## 相關條目

  - [林肯中心](../Page/林肯中心.md "wikilink")

## 外部連結

  - [The Metropolitan Opera](http://www.metoperafamily.org/metopera/)
  - [History of Metropolitan
    Opera](https://web.archive.org/web/20030401101609/http://www.metopera.org/history/)
  - [Metropolitan Opera Broadcast
    Information](https://web.archive.org/web/20021212192049/http://www.operainfo.org/)
  - [Metropolitan Opera Broadcast via streaming
    audio](http://jrabold.net/radio/2met.shtml)
  - [MetOpera
    Database](https://web.archive.org/web/20070329065327/http://66.187.153.86/archives/frame.htm)
  - Full text of *[Chapters of
    Opera](http://www.gutenberg.net/etext/5995)* by [Henry Edward
    Krehbiel](../Page/Henry_Edward_Krehbiel.md "wikilink") from [Project
    Gutenberg](../Page/Project_Gutenberg.md "wikilink")

[Category:紐約市建築物](../Category/紐約市建築物.md "wikilink")
[Category:美國歌劇院](../Category/美國歌劇院.md "wikilink")