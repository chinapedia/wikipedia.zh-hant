**後漢**（）是[五代十國的第四個朝代](../Page/五代十國.md "wikilink")，同时也是最后一个由[沙陀人建立的](../Page/沙陀人.md "wikilink")[中原王朝](../Page/中原.md "wikilink")。亦称刘汉。後漢承自[後晉](../Page/后晋.md "wikilink")，根據[五行相生的順序](../Page/五行.md "wikilink")，後晉的「金」德之後是「水」德，因此後漢以「水」為王朝德運。\[1\]

## 政權歷時

後漢政權存在只有兩朝共四年，是[五代十國中历时最短的政權](../Page/五代十國.md "wikilink")，也被一些学者（例如[钱文忠](../Page/钱文忠.md "wikilink")）认为是中国历史上最短命的“[中央政權](../Page/中央政權.md "wikilink")”。主要原因是[劉知遠不懂治國](../Page/劉知遠.md "wikilink")，朝中爭鬥激烈、動則[族誅](../Page/族誅.md "wikilink")，最後其子隱帝懷疑大臣[郭威想要造反](../Page/郭威.md "wikilink")，派[郭崇前往魏州殺死郭威](../Page/郭崇.md "wikilink")，導致[郭威起兵討伐隱帝](../Page/郭威.md "wikilink")，隱帝為部屬[郭允明所殺](../Page/郭允明.md "wikilink")。郭威進入都城開封后，本想立劉知遠侄子劉赟為帝，後來反悔，殺死劉赟，\[2\]自己稱帝，建立[後周](../Page/後周.md "wikilink")。后汉被后周郭威所篡后，後漢高祖劉知遠的弟弟、鎮守[晉陽的河东](../Page/晉陽.md "wikilink")[节度使](../Page/节度使.md "wikilink")[刘崇在](../Page/刘崇.md "wikilink")[太原继位称帝](../Page/太原.md "wikilink")，自称延续汉祚，然政权之疆域及地位均已改变，史家一般作为新政权或[殘餘政權定位](../Page/殘存國家.md "wikilink")，列為「十國」之一，称为[北汉](../Page/北汉.md "wikilink")。

### 殺戮四年

[五代史記述](../Page/五代史.md "wikilink")：「[五代亂世](../Page/五代十国.md "wikilink")，本無刑章，視人命如草芥，動以[族誅為事](../Page/族誅.md "wikilink")。是族誅之法，凡罪人之父兄妻妾子孫並女之出嫁者，無一得免，非法之刑，於茲極矣！而尤莫如漢代之濫。然不問罪之輕重，理之是非，但云有犯，即處極刑。枉濫之家，莫敢上訴。軍吏因之為奸，嫁禍脅人，不可勝數。而此毒痛四海，殃及萬方。後漢劉氏父子二帝，享國不及四年。[杨邠](../Page/杨邠.md "wikilink")、[史弘肇](../Page/史弘肇.md "wikilink")、[蘇逢吉](../Page/蘇逢吉.md "wikilink")、[劉銖等諸人亦皆被橫禍](../Page/劉銖.md "wikilink")，無一善終者。此固天道之報施昭然，而民之生於是時，不知如何措手足也。\[3\]」

## 統治範圍

统治范围包括今[河南](../Page/河南.md "wikilink")、[山东](../Page/山东.md "wikilink")、[山西](../Page/山西.md "wikilink")、[河北南部](../Page/河北.md "wikilink")、[湖北北部](../Page/湖北.md "wikilink")、[陕西北部](../Page/陕西.md "wikilink")、[安徽北部](../Page/安徽.md "wikilink")。

## 君主

開國君主[劉知遠為世居](../Page/劉知遠.md "wikilink")[太原的](../Page/太原.md "wikilink")[沙陀人](../Page/沙陀.md "wikilink")，原為五代[後晉河東節度使](../Page/後晉.md "wikilink")。947年，乘[契丹陷開封而於太原稱帝](../Page/契丹.md "wikilink")，国号**漢**，史家稱“後漢”，以别于[汉朝](../Page/汉朝.md "wikilink")\[4\]。自称为[东汉显宗八子淮阳王](../Page/汉明帝.md "wikilink")[刘昞之后](../Page/刘昞.md "wikilink")，继承[汉朝](../Page/汉朝.md "wikilink")，宗庙内祭祀[刘邦和](../Page/刘邦.md "wikilink")[刘秀](../Page/刘秀.md "wikilink")\[5\]\[6\]\[7\]。后攻克中原，定都[汴京](../Page/汴京.md "wikilink")（今[开封](../Page/开封.md "wikilink")）。948年刘知远二子[劉承祐嗣位](../Page/劉承祐.md "wikilink")，即后汉隐帝。950年[李守贞等藩镇发生叛乱](../Page/李守贞.md "wikilink")，汉隐帝命郭威平之，但汉隐帝猜忌郭威，欲杀之，郭威进而反叛。同年十一月二十一日（951年1月1日）刘承祐被杀，后汉亡。

### 君主列表

### 君主世系图

<center>

</center>

## 参考文献

## 外部連結

  - [劉知遠](https://web.archive.org/web/20060214002053/http://www.tynews.com.cn/longchengsanjin/2004-12/10/content_536840.htm)

{{-}}

[\*](../Page/category:后汉.md "wikilink")

[五代4](../Category/五代十国.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")

1.
2.  《新五代史》卷11：庚寅，威率百官诣明德门，请立武宁军节度使赟为嗣。遣太师冯道迎赟于徐州。辛卯，请太后临朝听政，以王峻为枢密使，翰林学士、尚书兵部侍郎范质为副使....王峻遣郭崇以骑七百逆刘赟于宋州，杀之。
3.  （[清](../Page/清朝.md "wikilink")）[趙翼](../Page/趙翼.md "wikilink"),"〈五代濫刑〉卷22/五代史",[二十二史劄記](../Page/二十二史劄記.md "wikilink"),[清](../Page/清朝.md "wikilink")[乾隆](../Page/乾隆.md "wikilink")60年3月（1795年）.
4.
5.  《五代会要》卷1：汉文祖明元皇帝讳湍，东汉显宗第八子淮阳王昞之后。
6.  《舊五代史》卷100:庚辰，追尊六庙，以太祖高皇帝、世祖光武皇帝为不祧之庙，高曾已下四朝，追尊谥号，已载于前矣。
7.  《新五代史》卷10：以汉高皇帝为高祖，光武皇帝为世祖，皆不祧。