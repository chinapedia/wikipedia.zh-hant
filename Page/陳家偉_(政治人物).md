**陳家偉**（，），現任[新思維總幹事](../Page/新思維_\(香港\).md "wikilink")，[香港民主黨前總幹事](../Page/香港民主黨.md "wikilink")，於2000年至2007年間任[九龍城區議會](../Page/九龍城區議會.md "wikilink")（[黃埔東](../Page/黃埔花園.md "wikilink")）[區議員](../Page/區議員.md "wikilink")，並於2004至2007年間任九龍城區議會副主席。於[2007年香港區議會選舉中](../Page/2007年香港區議會選舉.md "wikilink")，以500多票之差敗於法律學者[梁美芬](../Page/梁美芬.md "wikilink")，未能連任。2007年至2011年任民主黨總幹事。\[1\]

陳家偉已婚，是基督徒，擁有[社會學碩士](../Page/社會學.md "wikilink")\[2\]及[香港中文大學崇基神學院神道學碩士學位](../Page/香港中文大學.md "wikilink")。\[3\]

## 爭議事件

2011年7月12日，陳家偉被傳媒揭發在[尖沙咀](../Page/尖沙咀.md "wikilink")[香檳大廈涉嫌嫖妓](../Page/香檳大廈.md "wikilink")，《[東方日報](../Page/東方日報_\(香港\).md "wikilink")》記者目擊陳在香檳大廈B座四樓[鳳姐](../Page/一樓一鳳.md "wikilink")「婷婷」的房間逗留四十分鐘。之後陳否認，強調自己是付錢給鳳姐做「訪談」，並叫全裸鳳姐在他面前示範各種性愛姿勢，他在旁提點如何才是安全性行為。當晚，陳家偉以令黨聲譽受損為由，辭任[民主黨總幹事及申請退黨](../Page/民主黨_\(香港\).md "wikilink")，獲即時接納。\[4\]\[5\]\[6\]

## 参考文献

<div class="references-small">

<references/>

</div>

[Category:前香港區議員](../Category/前香港區議員.md "wikilink")
[Category:香港民主黨前成員](../Category/香港民主黨前成員.md "wikilink")
[Category:香港政治人物](../Category/香港政治人物.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[K](../Category/陳姓.md "wikilink")

1.  [選戰連番受挫退居幕後](http://orientaldaily.on.cc/cnt/news/20110713/00176_003.html)
2.  [第二梯隊核心
    曾涉匯標醜聞](http://news.sina.com.hk/news/20110714/-2-2380788/1.html)

3.  [神學碩士犯淫戒 基督徒怒轟](http://202.55.1.84/news/11/07/15/GW-1388164.htm)
4.
5.
6.