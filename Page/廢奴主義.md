**廢奴主義**（），又稱**廢除主義**、**奴隸廢除論**，是一場以廢除[奴隸制度及其](../Page/奴隸制度.md "wikilink")[奴隸貿易而展开的政治運動](../Page/奴隸貿易.md "wikilink")，其運動開始於[啟蒙時代](../Page/啟蒙時代.md "wikilink")，並在19世紀推至高峰，也在很多地方得到了大致上的成功。

## 源由

奴隸制在人类社会中有很着悠久的歷史，世界各地包括[古羅馬](../Page/古羅馬.md "wikilink")、[古希臘](../Page/古希臘.md "wikilink")、[古埃及](../Page/古埃及.md "wikilink")、[古巴比倫](../Page/古巴比倫.md "wikilink")、[中國](../Page/中國.md "wikilink")[周代等都是施行奴隸制的](../Page/周代.md "wikilink")。直至16世紀，[歐洲的](../Page/歐洲.md "wikilink")[殖民者走遍世界](../Page/殖民主義.md "wikilink")，並將[西非的](../Page/西非.md "wikilink")[黑人販賣到](../Page/黑人.md "wikilink")[歐洲及](../Page/歐洲.md "wikilink")[新世界去](../Page/美洲.md "wikilink")，形成了奴隸制度的高峰。18世紀晚期，歐洲開始出現新思潮，[啟蒙運動帶給當時人們](../Page/啟蒙運動.md "wikilink")[人權](../Page/人權.md "wikilink")、[自由](../Page/自由.md "wikilink")、[平等的觀念](../Page/平等.md "wikilink")，他們漸漸覺得奴隸制是一種不道德的行為，廢奴主義開始在各國萌芽。

## 英國廢奴主義

### 國內奴隸制

[英格蘭的奴隸制早已於](../Page/英格蘭.md "wikilink")1102年定為犯法，最後一名[農奴則在](../Page/農奴.md "wikilink")17世紀初消失。但18世紀開始，[黑奴開始輸入](../Page/黑奴.md "wikilink")[倫敦和](../Page/倫敦.md "wikilink")[愛丁堡作](../Page/愛丁堡.md "wikilink")[英國人的僕役](../Page/英國.md "wikilink")，但當時的黑奴並不是貿易而得，他們的法律地位一直都是含糊不清。直至1772年，一個名[James
Somerset的黑奴逃跑了](../Page/James_Somerset.md "wikilink")，然後被他的主人Charles
Steuart捉回，再把他送去[牙買加種](../Page/牙買加.md "wikilink")[甘蔗](../Page/甘蔗.md "wikilink")。由於Somerset在倫敦時已經受洗，他的[神父便以](../Page/神父.md "wikilink")「[人身保護令](../Page/人身保護令.md "wikilink")」向法院提出訴訟。當時的英格蘭及[威爾士](../Page/威爾士.md "wikilink")[高等法院](../Page/高等法院.md "wikilink")[王座法庭院長](../Page/王座法庭.md "wikilink")[William
Murray, Lord
Mansfield於](../Page/William_Murray,_1st_Earl_of_Mansfield.md "wikilink")1772年6月22日宣判：「無論有多麼不便都應該有個決定，我不認為英格蘭法律准許或認同這個案子，所以黑人應該釋放。」此即宣布了奴隸制不存在於[英格蘭法律之下](../Page/英格蘭法律.md "wikilink")，那麼[蓄奴等同非法行為](../Page/蓄奴.md "wikilink")。這一[判例使得英格蘭境內的一萬至一萬四千名奴隸得到解放](../Page/判例.md "wikilink")，亦表明了其他轄區實行的奴隸制，在英格蘭不適用。\[1\]

在「Somerset案」後，[蘇格蘭的黑奴](../Page/蘇格蘭.md "wikilink")[Joseph
Knight也好像Somerset一樣逃跑了](../Page/Joseph_Knight.md "wikilink")。這件案在1776年[Wedderburn審訊](../Page/Wedderburn.md "wikilink")，結果與前案一樣：奴隸制不存在於[蘇格蘭法律之下](../Page/蘇格蘭法律.md "wikilink")。蘇格蘭最後的本土奴隸於1799年在煤礦主底下獲得自由。

### 大英帝國奴隸制

縱使英國國內已廢除奴隸制，但在[大英帝國的](../Page/大英帝國.md "wikilink")[殖民地如](../Page/殖民地.md "wikilink")[東印度群島和](../Page/東印度群島.md "wikilink")[美洲中依然存在](../Page/美洲.md "wikilink")。

1783年，反奴隸制運動在英國社會內開始。當[Society for the Propagation of the Gospel in
Foreign
Parts](../Page/Society_for_the_Propagation_of_the_Gospel_in_Foreign_Parts.md "wikilink")（SPG）在[倫敦](../Page/倫敦.md "wikilink")[戚普賽街](../Page/戚普賽街.md "wikilink")[聖瑪莉勒布教堂舉行](../Page/聖瑪莉勒布教堂.md "wikilink")1783週年佈道會時，當時的[切斯特主教](../Page/切斯特主教.md "wikilink")，後來的[倫敦主教](../Page/倫敦主教.md "wikilink")[Dr
Beilby
Porteus呼籲](../Page/Bishop_Beilby_Porteus.md "wikilink")[英格蘭聖公會停止參與販奴並改善教會在](../Page/英格蘭聖公會.md "wikilink")[巴貝多農莊工作的奴隸生活環境](../Page/巴貝多.md "wikilink")。同年，[貴格會成立了首個英國的廢奴組織](../Page/貴格會.md "wikilink")。此後貴格會在英國的廢奴運動中一直擔任著重要的角色。1783年6月17日，議員Sir
Cecil Wray將貴格會的請願交給政府。

1787年5月，為打擊英國商人的[大西洋奴隶贸易活動](../Page/大西洋奴隶贸易.md "wikilink")，「廢除奴隸販賣委員會」（Committee
for the Abolition of the Slave
Trade）成立，[格伦维尔·夏普](../Page/格伦维尔·夏普.md "wikilink")（Granville
Sharp）、[Thomas
Clarkson](../Page/Thomas_Clarkson.md "wikilink")、[貴格會及其他](../Page/貴格會.md "wikilink")[福音改革運動中的](../Page/福音改革運動.md "wikilink")[克拉珀姆教派都是十二個成員的其中之一](../Page/克拉珀姆教派.md "wikilink")，雖然十八世紀的信念只停留在對於窮人給予正直、尊重的生活，但不需改善窮人的社會及經濟地位的信念裡，但包括[威廉·威爾伯福斯在內的英格蘭教會的信徒仍為這些人努力服務](../Page/威廉·威爾伯福斯.md "wikilink")，並對抗大英帝國的奴隸制度。\[2\][威廉·威爾伯福斯](../Page/威廉·威爾伯福斯.md "wikilink")（William
Wilberforce）成為他們在議會的代表，Clarkson則負責研究及收集關於奴隸販賣的資料。威廉·威爾伯福斯一生中大部分的時間都在為废除奴隸制而努力，雖然進展很慢但是隨著廢奴運動的發展，地區的組織紛紛成立，他們通過會面去進行運動，包括派發小冊子和請願。他們其中一件個別的計劃便是在逼使政府於1787年在[獅子山的現時首都](../Page/獅子山.md "wikilink")[自由城建市](../Page/自由城.md "wikilink")，以供原來準備販賣往倫敦工作的奴隸生活。

廢奴主義者的運動不僅受到貴格會支持，其他宗教團體例如[浸信會](../Page/浸信會.md "wikilink")、[衛理會等](../Page/衛理會.md "wikilink")，甚至工人、婦女、兒童、非政治團體都爭相支持。1796年[John
Gabriel
Stedman出版了一本關於他在](../Page/John_Gabriel_Stedman.md "wikilink")[蘇利南鎮壓當地前奴隸五年的回憶錄](../Page/蘇利南.md "wikilink")，譴責奴隸的殘忍待遇。這本書更加上了很多由[William
Blake和](../Page/William_Blake.md "wikilink")[Francesco
Bartolozzi描述的影像](../Page/Francesco_Bartolozzi.md "wikilink")，成為了廢奴主義文學的重要一部份。

#### 1807年奴隸販賣法案

1807年3月25日，[英國國會通過了](../Page/英國國會.md "wikilink")[廢除奴隸販賣法案](../Page/奴隸販賣法案.md "wikilink")（Slave
Trade
Act），將販奴在大英帝國境內定為非法，並實施向英國奴隸船徵收每一個奴隸£100的罰金。但這法案的阻嚇性不大，一些奴販仍然繼續貿易，在此時大英帝國的境內仍有七十五萬個奴隸遭受非人的待遇，\[3\]當那些從事奴隸買賣的商人被[英國皇家海軍發現時](../Page/英國皇家海軍.md "wikilink")，他們總會將一些奴隸推下海以減少罰款。有見及此，1827年英國便宣佈參與販奴者將會秘密地被施以[死刑](../Page/死刑.md "wikilink")。

#### 1833年廢奴法案

1807年的法案雖然杜絕了販奴問題，但奴隸制依然存在。1820年代，廢奴運動又開始變得活躍。1827年，[反奴隸制協會](../Page/反奴隸制協會.md "wikilink")（Anti-Slavery
Society）成立，當中有不少便是當年參與過反奴隸販賣的廢奴主義者。這次運動則以全面廢除奴隸制為目標。

1833年8月23日，[廢奴法案](../Page/廢奴法案.md "wikilink")（Slavery Abolition
Act）定英國殖民地的奴隸制為不合法。1834年8月1日，大英帝國下的所有奴隸全部解放，但仍然要為前主人工作至1838年作為適應期。[加勒比的農莊主則獲償二千萬英镑](../Page/加勒比海.md "wikilink")。

### 廢奴後的運動

由1839年開始，英國及其他地方的反奴隸制協會一起合作去廢奴，其中較具影響力便是在[美國的廢奴運動上](../Page/美國.md "wikilink")。這些協會亦向政府施壓，要求遏制一些非法販奴。現今亦一直有一些組織如[反奴隸制國際](http://www.antislavery.org/)等。

## 法國廢奴主義

[大西洋奴隶贸易給](../Page/大西洋奴隶贸易.md "wikilink")[法國在](../Page/法國.md "wikilink")[西印度群島的](../Page/西印度群島.md "wikilink")[甘蔗種植業提供了豐富的人力資源](../Page/甘蔗.md "wikilink")。1689年，[路易十四的](../Page/路易十四.md "wikilink")《[黑法](../Page/黑法.md "wikilink")》正式允許在法國所有殖民地販賣奴隸。到了1789年[法國大革命發生](../Page/法國大革命.md "wikilink")，法國[殖民地](../Page/殖民地.md "wikilink")[海地亦乘機在](../Page/海地.md "wikilink")1791年發動[革命](../Page/海地革命.md "wikilink")，宣布廢除奴隸制。法國本土的廢奴運動本土則是由[Henri
Grégoire和](../Page/Henri_Grégoire.md "wikilink")[Jacques Pierre
Brissot的](../Page/Jacques_Pierre_Brissot.md "wikilink")[黑人之友協會](../Page/黑人之友協會.md "wikilink")（Société
des Amis des
Noirs）所領導，其工作為在城市內宣傳反奴隸制。1794年2月4日，[第一共和正式宣布廢奴](../Page/法蘭西第一共和國.md "wikilink")。法國殖民地法律的第一條便寫道：「奴隸制被廢除」，第二條則是「奴隸主會獲（金錢）賠償」。不久[拿破崙執政](../Page/拿破崙.md "wikilink")，並重新設回奴隸制，1802年5月10日[colonel
Delgrès呼籲發起暴動去抵抗拿破崙](../Page/Louis_Delgrès.md "wikilink")，然而結果暴動被鎮壓。直至1848年4月27日[第二共和才再一次廢除奴隸制](../Page/法蘭西第二共和國.md "wikilink")。

## 俄國廢奴主義

[沙俄的](../Page/沙俄.md "wikilink")[農奴制雖然嚴格來說不算是奴隸](../Page/農奴.md "wikilink")，但他們沒有選擇工作地點或不工作的自由。1861年3月3日，[沙皇](../Page/沙皇.md "wikilink")[亞歷山大二世](../Page/亞歷山大二世_\(俄國\).md "wikilink")[解放農奴](../Page/1861年俄國農奴制度改革.md "wikilink")，因此被稱為「解放者沙皇」。

1930年代，[史太林開始实行](../Page/史太林.md "wikilink")[社會主義的](../Page/社會主義.md "wikilink")[計劃經濟](../Page/計劃經濟.md "wikilink")，不准許農民不工作和離開他們的[集體農莊](../Page/集體農莊.md "wikilink")，又沒收了私有財產，被一些人認為是農奴制的復辟。

## 美國廢奴主義

[美國的廢奴運動歷史明顯地比前述的國家較為激烈及複雜](../Page/美國.md "wikilink")。它不僅為美國18世紀末至19世紀初的歷史主軸，而且更爆發了[美國歷史上唯一一次的內戰](../Page/美國歷史.md "wikilink")——[南北戰爭](../Page/南北戰爭.md "wikilink")。

美國從殖民時代開始已經實行奴隸制。1775年4月14日，第一個美國的廢奴主義協會[Society for the Relief of Free
Negroes Unlawfully Held in
Bondage由貴格會於](../Page/Society_for_the_Relief_of_Free_Negroes_Unlawfully_Held_in_Bondage.md "wikilink")[費城成立](../Page/費城.md "wikilink")。後因[美國革命時英軍佔領費城而停止運作](../Page/美國革命.md "wikilink")，在1784年重新運作，由[班傑明·富蘭克林出任第一任會長](../Page/班傑明·富蘭克林.md "wikilink")。\[4\]而第一篇呼籲廢奴在《賓夕法尼亞雜誌》發表的文章《[在美洲的非洲奴隸](../Page/在美洲的非洲奴隸.md "wikilink")》則相傳為[托馬斯·潘恩所寫](../Page/托馬斯·潘恩.md "wikilink")。1776年，[托马斯·杰斐逊為美國獨立起草了](../Page/托马斯·杰斐逊.md "wikilink")《[美國獨立宣言](../Page/美國獨立宣言.md "wikilink")》其中一個篇章涉及廢除奴隸制度，後來卻在[刑事法庭上被重寫時移除](../Page/刑事法庭.md "wikilink")。

在1804年以前每個北方州份都已廢除奴隸制，1808年全國禁止進口奴隸。而南方州份則繼續實行奴隸制，直至1865年，南北戰爭後才被廢除。

### 利比里亚殖民

1821年[美国殖民协会在](../Page/美国殖民协会.md "wikilink")[非洲](../Page/非洲.md "wikilink")[利比里亚沿岸建立黑人](../Page/利比里亚.md "wikilink")“移民区”，主要定居者是获得自由的美国奴隶。利比里亚于1847年7月26日宣布独立（也曾经招募自愿移民[海地的黑人](../Page/海地.md "wikilink")）。

## 國際廢奴日

各國廢奴的日子：

  - ：1335年（殖民地[St
    Barthélemy於](../Page/Saint-Barthélemy.md "wikilink")1847年廢奴）

  - ：1761年

  - 及  ：1772年「Somerset案」後

  - ：1776年「Wedderburne案」後

  - ：1791年近十萬奴隸起義後

  - [安大略區域](../Page/安大略.md "wikilink")：1793年《[反蓄奴法案](../Page/反蓄奴法案.md "wikilink")》（Act
    Against Slavery）

  - （首次）：1794年至1802年包括所有殖民地（一些被英國佔領殖民地則未能實行）

  - ：1813年

  - [大哥倫比亞](../Page/大哥倫比亞共和國.md "wikilink")（[厄瓜多爾](../Page/厄瓜多爾.md "wikilink")、[哥倫比亞](../Page/哥倫比亞.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[委內瑞拉](../Page/委內瑞拉.md "wikilink")）：1821年的漸進解放計劃（1852年
     及1854年  ）

  - ：1823年

  - ：1829年

  - ：1833年包括所有殖民地（真正實行於1834年8月1日，東印度則於1838年8月1日）

  - ：1835年2月1日於英國政府執行下，並為公眾假期。

  - ：1848年，包括所有殖民地

  - （二次）：1848年，包括所有殖民地

  - ：1851年

  - ：1861年

  - ：1863年，包括所有殖民地

  - ：1865年，[美國內戰後](../Page/美國內戰.md "wikilink")（某些州份在內戰前經已廢奴）

  - ：1872年11月2日，日本太政官公佈

  - ：1873年及  ：1880年（當時皆為 殖民地）

  - ：1888年

  - ：1894年（世襲奴隸制終於1886年）

  - ：1897年（奴隸貿易廢止於1873年）

  - ：1905年4月1日（由泰王[拉玛五世下令](../Page/拉玛五世.md "wikilink")）

  - ：1910年

  - ：1929年

  - ：1936年，在[意大利軍隊佔領之下廢除](../Page/意大利.md "wikilink")（詳見[第二次意大利埃塞俄比亞戰爭](../Page/第二次意大利埃塞俄比亞戰爭.md "wikilink")）。當衣索比亞於1942年[二戰期間重新獲得獨立時](../Page/二戰.md "wikilink")，其皇帝[海爾·塞拉西一世亦沒有重新恢復奴隸制](../Page/海爾·塞拉西一世.md "wikilink")。

  - ：1962年

  - ：1980年（由[法國於](../Page/法國.md "wikilink")1905年名義上廢除，後在1961年的新[憲法中暗示](../Page/憲法.md "wikilink")，後在同年的10月加入[聯合國時變為明顯](../Page/聯合國.md "wikilink")），但依然在實行。

## [奴隸制度](../Page/奴隸制度.md "wikilink")

目前奴隸制在大部份的國家（區域）被禁止，但仍然在世界各地秘密地實行； 奴隸制一般認為有三種類型：薪水奴隸，合約奴隸，傳統奴隸。

  - 薪水奴隸在社會保障不完全地區最為普遍。由於那裡的弱勢者不能承受失業的風險，所以僱主能夠輕易以低薪僱用勞工，或要求勞工從事非法勞動（如自願無償加班等常見現象），而童工多數被認為是薪水奴隸。
  - 合約奴隸通常是一些窮人或文盲受騙而簽下了一些不平等合約而成為奴隸。
  - 傳統奴隸在全球黑市交易中依然十分活躍，與古時的的手法類似：奴隸大多數是婦孺，被誘拐賣到海外（或監禁）。男的通常會以勞工賣出，女的則會賣給當地男子為妻妾或成為[妓女](../Page/妓女.md "wikilink")。這些誘拐行為通常發生在相對（於購買方）較落後、法治較不健全國家（區域），而賣去高收入、高消費國家（區域）。
  - 根據[基進女性主義的觀點](../Page/激進女性主義.md "wikilink")，[娼妓制度便是](../Page/性交易.md "wikilink")[嫖客與](../Page/嫖客.md "wikilink")[仲介對](../Page/仲介.md "wikilink")[賣性者的](../Page/賣性者.md "wikilink")[奴役](../Page/奴役.md "wikilink")。

## 現今廢奴主義

現今的廢奴組織有[反奴隸制國際](../Page/反奴隸制國際.md "wikilink")（Anti-Slavery
International）、[解放奴隸等](../Page/解放奴隸.md "wikilink")（Free the Slaves）。

1948年12月10日，[聯合國安理會宣佈了](../Page/聯合國安理會.md "wikilink")[世界人權宣言](../Page/世界人權宣言.md "wikilink")，其中第四條便提出「任何人不容使為奴役；奴隸制度及奴隸販賣，不論出於何種方式，悉應予以禁止。」

自1997年起，[美國司法部便與](../Page/美國司法部.md "wikilink")[Coalition of Immokalee
Workers合作起訴六人在農業中使用奴隸](../Page/Coalition_of_Immokalee_Workers.md "wikilink")，此案令超過1000個工作在[佛羅里達州南部的](../Page/佛羅里達州.md "wikilink")[番茄及](../Page/番茄.md "wikilink")[橙園的奴隸得到解放](../Page/橙.md "wikilink")。不過這只是全球反農業及性服務奴隸制行動其中一個例子。

在[美國](../Page/美國.md "wikilink")，一些要求廢除[死刑及爭取](../Page/死刑.md "wikilink")[移民權的人士亦被認為是繼承了廢奴運動的衣缽](../Page/移民權.md "wikilink")。

在[瑞典](../Page/瑞典.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[北愛爾蘭以及](../Page/北愛爾蘭.md "wikilink")[加拿大等國](../Page/加拿大.md "wikilink")，將[娼妓制度視作一種](../Page/性交易.md "wikilink")[奴隸制度](../Page/奴隸制度.md "wikilink")，因此採用[北歐模式
(罰嫖不罰娼)](../Page/北歐模式_\(罰嫖不罰娼\).md "wikilink")。

## 紀念

現今世界各地皆有不同的方法去紀念廢奴運動。[聯合國安理會亦將](../Page/聯合國安理會.md "wikilink")2004年定為[International
Year to Commemorate the Struggle against Slavery and its
Abolition](../Page/International_Year_to_Commemorate_the_Struggle_against_Slavery_and_its_Abolition.md "wikilink")，以紀念第一個黑人獨立國家——海地的成立，同時亦舉辦了一些展覽、節目、研究計劃等。

## 著名反奴隸制人士

<span style="font-size:smaller;">註：並非所有人皆是廢奴主義者</span>

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/約翰·昆西·亞當斯.md" title="wikilink">約翰·昆西·亞當斯</a></li>
<li><a href="../Page/William_Allen_(Quaker).md" title="wikilink">William Allen</a></li>
<li><a href="../Page/史蒂芬·皮爾·安德魯斯.md" title="wikilink">史蒂芬·皮爾·安德魯斯</a></li>
<li><a href="../Page/蘇珊·安東尼.md" title="wikilink">蘇珊·安東尼</a></li>
<li><a href="../Page/Gamaliel_Bailey.md" title="wikilink">Gamaliel Bailey</a></li>
<li><a href="../Page/Henry_Ward_Beecher.md" title="wikilink">Henry Ward Beecher</a></li>
<li><a href="../Page/Anthony_Benezet.md" title="wikilink">Anthony Benezet</a></li>
<li><a href="../Page/Ramón_Emeterio_Betances.md" title="wikilink">Ramón Emeterio Betances</a></li>
<li><a href="../Page/西蒙·玻利瓦爾.md" title="wikilink">西蒙·玻利瓦爾</a></li>
<li><a href="../Page/William_Henry_Brisbane.md" title="wikilink">William Henry Brisbane</a></li>
<li><a href="../Page/約翰·布朗.md" title="wikilink">約翰·布朗</a></li>
<li><a href="../Page/Aaron_Burr.md" title="wikilink">Aaron Burr</a></li>
<li><a href="../Page/Thomas_Fowell_Buxton.md" title="wikilink">Thomas Fowell Buxton</a></li>
<li><a href="../Page/Antônio_de_Castro_Alves.md" title="wikilink">Antônio de Castro Alves</a></li>
<li><a href="../Page/Thomas_Clarkson.md" title="wikilink">Thomas Clarkson</a></li>
<li><a href="../Page/Levi_Coffin.md" title="wikilink">Levi Coffin</a></li>
<li><a href="../Page/Thomas_Day.md" title="wikilink">Thomas Day</a></li>
<li><a href="../Page/查爾斯·狄更斯.md" title="wikilink">查爾斯·狄更斯</a></li>
<li><a href="../Page/Richard_Dillingham.md" title="wikilink">Richard Dillingham</a></li>
<li><a href="../Page/弗雷德裏克·道格拉斯.md" title="wikilink">弗雷德裏克·道格拉斯</a></li>
<li><a href="../Page/愛默生.md" title="wikilink">愛默生</a></li>
<li><a href="../Page/Olaudah_Equiano.md" title="wikilink">Olaudah Equiano</a></li>
<li><a href="../Page/Calvin_Fairbank.md" title="wikilink">Calvin Fairbank</a></li>
<li><a href="../Page/班傑明·富蘭克林.md" title="wikilink">班傑明·富蘭克林</a></li>
<li><a href="../Page/Amos_Noë_Freeman.md" title="wikilink">Amos Noë Freeman</a></li>
<li><a href="../Page/Thomas_Garret.md" title="wikilink">Thomas Garret</a></li>
<li><a href="../Page/威廉·羅伊·葛里森.md" title="wikilink">威廉·羅伊·葛里森</a></li>
<li><a href="../Page/Henri_Grégoire.md" title="wikilink">Henri Grégoire</a></li>
<li><a href="../Page/Angelina_Grimké.md" title="wikilink">Angelina Grimké</a></li>
<li><a href="../Page/Vicente_Guerrero.md" title="wikilink">Vicente Guerrero</a></li>
<li><a href="../Page/亞歷山大·漢密彌頓.md" title="wikilink">亞歷山大·漢密彌頓</a></li>
<li><a href="../Page/Laura_Smith_Haviland.md" title="wikilink">Laura Smith Haviland</a></li>
<li><a href="../Page/Lewis_Hayden.md" title="wikilink">Lewis Hayden</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/Hinton_Rowan_Helper.md" title="wikilink">Hinton Rowan Helper</a>（蓄奴主之敵）</li>
<li><a href="../Page/Elias_Hicks.md" title="wikilink">Elias Hicks</a></li>
<li><a href="../Page/Miguel_Hidalgo.md" title="wikilink">Miguel Hidalgo</a></li>
<li><a href="../Page/Isaac_Hopper.md" title="wikilink">Isaac Hopper</a></li>
<li><a href="../Page/Julia_Ward_Howe.md" title="wikilink">Julia Ward Howe</a></li>
<li><a href="../Page/Samuel_Gridley_Howe.md" title="wikilink">Samuel Gridley Howe</a></li>
<li><a href="../Page/約翰·傑伊.md" title="wikilink">約翰·傑伊</a></li>
<li><a href="../Page/塞繆爾·詹森.md" title="wikilink">塞繆爾·詹森</a></li>
<li><a href="../Page/Abby_Kelley.md" title="wikilink">Abby Kelley</a></li>
<li><a href="../Page/Benjamin_Lay.md" title="wikilink">Benjamin Lay</a></li>
<li><a href="../Page/亞伯拉罕·林肯.md" title="wikilink">亞伯拉罕·林肯</a></li>
<li><a href="../Page/Toussaint_L&#39;Ouverture.md" title="wikilink">Toussaint L'Ouverture</a></li>
<li><a href="../Page/Jermain_Loguen.md" title="wikilink">Jermain Loguen</a></li>
<li><a href="../Page/Elijah_Lovejoy.md" title="wikilink">Elijah Lovejoy</a></li>
<li><a href="../Page/James_Russell_Lowell.md" title="wikilink">James Russell Lowell</a></li>
<li><a href="../Page/Maria_White_Lowell.md" title="wikilink">Maria White Lowell</a></li>
<li><a href="../Page/Henry_G._Ludlow.md" title="wikilink">Henry G. Ludlow</a></li>
<li><a href="../Page/Benjamin_Lundy.md" title="wikilink">Benjamin Lundy</a></li>
<li><a href="../Page/Zachary_Macaulay.md" title="wikilink">Zachary Macaulay</a></li>
<li><a href="../Page/Philip_Mazzei.md" title="wikilink">Philip Mazzei</a></li>
<li><a href="../Page/José_Gregorio_Monagas.md" title="wikilink">José Gregorio Monagas</a></li>
<li><a href="../Page/Hannah_More.md" title="wikilink">Hannah More</a></li>
<li><a href="../Page/José_María_Morelos.md" title="wikilink">José María Morelos</a></li>
<li><a href="../Page/Lucretia_Mott.md" title="wikilink">Lucretia Mott</a></li>
<li><a href="../Page/William_Murray,_1st_Earl_of_Mansfield.md" title="wikilink">Lord William Murray</a></li>
<li><a href="../Page/Joaquim_Nabuco.md" title="wikilink">Joaquim Nabuco</a></li>
<li><a href="../Page/Daniel_O&#39;Connell.md" title="wikilink">Daniel O'Connell</a></li>
<li><a href="../Page/托馬斯·潘恩.md" title="wikilink">托馬斯·潘恩</a></li>
<li><a href="../Page/Theodore_Parker.md" title="wikilink">Theodore Parker</a></li>
<li><a href="../Page/John_Parker_(abolitionist).md" title="wikilink">John Parker</a></li>
<li><a href="../Page/Francis_Daniel_Pastorius.md" title="wikilink">Francis Daniel Pastorius</a></li>
<li><a href="../Page/José_do_Patrocínio.md" title="wikilink">José do Patrocínio</a></li>
<li><a href="../Page/Wendell_Phillips.md" title="wikilink">Wendell Phillips</a></li>
<li><a href="../Page/Mary_Ellen_Pleasant.md" title="wikilink">Mary Ellen Pleasant</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/Bishop_Beilby_Porteus.md" title="wikilink">Bishop Beilby Porteus</a></li>
<li><a href="../Page/John_Wesley_Posey.md" title="wikilink">John Wesley Posey</a></li>
<li><a href="../Page/Robert_Purvis.md" title="wikilink">Robert Purvis</a></li>
<li><a href="../Page/Isabel,_Princess_Imperial_of_Brazil.md" title="wikilink">Isabel, Princess Imperial of Brazil</a></li>
<li><a href="../Page/James_Ramsay.md" title="wikilink">James Ramsay</a></li>
<li><a href="../Page/John_Rankin_(abolitionist).md" title="wikilink">John Rankin</a></li>
<li><a href="../Page/André_Rebouças.md" title="wikilink">André Rebouças</a></li>
<li><a href="../Page/Charles_Lenox_Remond.md" title="wikilink">Charles Lenox Remond</a></li>
<li><a href="../Page/Ernestine_Rose.md" title="wikilink">Ernestine Rose</a></li>
<li><a href="../Page/Benjamin_Rush.md" title="wikilink">Benjamin Rush</a></li>
<li><a href="../Page/Victor_Schœlcher.md" title="wikilink">Victor Schœlcher</a></li>
<li><a href="../Page/Granville_Sharp.md" title="wikilink">Granville Sharp</a></li>
<li><a href="../Page/Gerrit_Smith.md" title="wikilink">Gerrit Smith</a></li>
<li><a href="../Page/William_Smith_(abolitionist).md" title="wikilink">William Smith</a></li>
<li><a href="../Page/Silas_Soule.md" title="wikilink">Silas Soule</a></li>
<li><a href="../Page/萊桑德·斯波納.md" title="wikilink">萊桑德·斯波納</a></li>
<li><a href="../Page/Elizabeth_Cady_Stanton.md" title="wikilink">Elizabeth Cady Stanton</a></li>
<li><a href="../Page/Henry_Stanton.md" title="wikilink">Henry Stanton</a></li>
<li><a href="../Page/William_Still.md" title="wikilink">William Still</a></li>
<li><a href="../Page/哈里特·伊莉莎白·比徹·斯托.md" title="wikilink">哈里特·伊莉莎白·比徹·斯托</a></li>
<li><a href="../Page/Charles_Sumner.md" title="wikilink">Charles Sumner</a></li>
<li><a href="../Page/Arthur_Tappan.md" title="wikilink">Arthur Tappan</a></li>
<li><a href="../Page/亨利·戴維·梭羅.md" title="wikilink">亨利·戴維·梭羅</a></li>
<li><a href="../Page/Henry_Thornton_(abolitionist).md" title="wikilink">Henry Thornton</a></li>
<li><a href="../Page/索茹爾內·特魯斯.md" title="wikilink">索茹爾內·特魯斯</a></li>
<li><a href="../Page/哈莉特·塔布曼.md" title="wikilink">哈莉特·塔布曼</a></li>
<li><a href="../Page/Nat_Turner.md" title="wikilink">Nat Turner</a> insurrectionist</li>
<li><a href="../Page/Delia_Webster.md" title="wikilink">Delia Webster</a></li>
<li><a href="../Page/Theodore_Dwight_Weld.md" title="wikilink">Theodore Dwight Weld</a></li>
<li><a href="../Page/約翰·衛斯理.md" title="wikilink">約翰·衛斯理</a></li>
<li><a href="../Page/John_Greenleaf_Whittier.md" title="wikilink">John Greenleaf Whittier</a></li>
<li><a href="../Page/威廉·威伯福士.md" title="wikilink">威廉·威伯福士</a></li>
<li><a href="../Page/John_Woolman.md" title="wikilink">John Woolman</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 參考文獻

### 引用

### 来源

  - 英國及世界

<!-- end list -->

  - Brown, Christopher Leslie. *Moral Capital: Foundations of British
    Abolitionism*（2006）
  - Davis, David Brion, *The Problem of Slavery in the Age of
    Revolution, 1770-1823*（1999）; *The Problem of Slavery in Western
    Culture*（1988）
  - Gould, Philip. *Barbaric Traffic: Commerce and Antislavery in the
    Eighteenth-Century Atlantic World* Harvard U. Press, 2003. 258 pp.
  - Thistlethwaite, Frank. *Anglo-American Connection in the Early
    Nineteenth Century*. 1971. ISBN 0-8462-1540-3.

<!-- end list -->

  - 美國

<!-- end list -->

  - Abzug, Robert H. *Cosmos Crumbling: American Reform and the
    Religious Imagination*. Oxford, 1994. ISBN 0-19-503752-9.
  - Bacon, Jacqueline. *The Humblest May Stand Forth: Rhetoric,
    Empowerment, and Abolition* U. of South Carolina Press, 2002. 352
    pp.
  - Barnes, Gilbert H. *The Anti-Slavery Impulse 1830-1844*. reprint
    1964. ISBN 0-7812-5307-1.
  - Blue, Frederick J. *No Taint of Compromise: Crusaders in Antislavery
    Politics.* Louisiana State U. Press 2005. 301 pp.
  - Bordewich, Fergus M. *Bound for Canaan: The Underground Railroad and
    the War for the Soul of America.* HarperCollins, 2005. 510 pp.
  - Davis, David Brion, *Inhuman Bondage : The Rise and Fall of Slavery
    in the New World*（2006）
  - Filler, Louis. *The Crusade Against Slavery 1830-1860*. 1960. ISBN
    0-917256-29-8. survey of movement in U.S.
  - Griffin, Clifford S. *Their Brothers' Keepers: Moral Stewardship in
    the United States 1800-1865*. Rutgers UP, 1967. ISBN 0-313-24059-0.
  - Harrold, Stanley. *The Abolitionists and the South, 1831-1861*.
    University Press of Kentucky, 1995. ISBN 0-8131-0968-X.
  - Harrold, Stanley. *The American Abolitionists*. Longman, 2000. ISBN
    0-582-35738-1, short survey
  - Harrold, Stanley. *The Rise of Aggressive Abolitionism: Addresses to
    the Slaves*. University Press of Kentucky, 2004. ISBN 0-8131-2290-2.
  - Huston, James L. "The Experiential Basis of the Northern Antislavery
    Impulse." *Journal of Southern History* 56:4（November 1990）:
    609-640.
  - John R. McKivigan *The War Against Proslavery Religion: Abolitionism
    and the Northern Churches, 1830-1865*（1984）
  - McPherson, James M. *The Abolitionist Legacy: From Reconstruction to
    the NAACP* Princeton U. Press 1975. 438 pp.
  - Newman, Richard S. *The Transformation of American Abolitionism:
    Fighting Slavery in the Early Republic.* U. of North Carolina Pr.,
    2002. 256 pp.
  - Osofsky, Gilbert. "Abolitionists, Irish Immigrants, and the Dilemmas
    of Romantic Nationalism" *American Historical Review* 1975 80 (4):
    889-912. Issn: 0002-8762
  - Perry, Lewis and Michael Fellman, eds. *Antislavery Reconsidered:
    New Perspectives on the Abolitionists*. Louisiana State Univ Press,
    1979. ISBN 0-8071-0889-8.
  - Peterson, Merrill D. *John Brown: The Legend Revisited* U. Press of
    Virginia, 2002. 196 pp.
  - Pierson, Michael D. *Free Hearts and Free Homes: Gender and American
    Antislavery Politics.* U. of North Carolina Press, 2003. 250 pp.
  - Beth A. Salerno. *Sister Societies: Women's Antislavery
    Organizations in Antebellum America* Northern Illinois University
    Press, 2005. ISBN =0-87580-338-5.
  - Speicher, Anna M. *The Religious World of Antislavery Women:
    Spirituality in the Lives of Five Abolitionist Lecturers*. Syracuse
    Univ Press, 2000. ISBN 0-8156-2850-1.
  - Harrold, Stanley. *The Rise of Aggressive Abolitionism: Addresses to
    the Slaves* U. Press of Kentucky, 2004. 246 pp.
  - Stauffer, John. *The Black Hearts of Men: Radical Abolitionists and
    the Transformation of Race.* Harvard U. Press, 2002. 367 pp.
  - Vorenberg, Michael. *Final Freedom: The Civil War, the Abolition of
    Slavery, and the Thirteenth Amendment.* Cambridge U. Press, 2001.
    305 pp.
  - Zilversmit, Arthur. *The First Emancipation: The Abolition of
    Slavery in the North*. University of Chicago Press, 1967. ISBN
    0-226-98332-3.

## 外部連結

  - [The Antislavery Literature
    Project](https://web.archive.org/web/20110220083446/http://antislavery.eserver.org/)
    major academic center for primary sources
  - *[Elijah Parish Lovejoy: A Martyr on the Altar of American
    Liberty](http://www.altonweb.com/history/lovejoy/)*
  - [Brycchan Carey's pages listing British
    abolitionists](http://www.brycchancarey.com/abolition/)
  - [*Slavery in Massachusetts*, Henry David
    Thoreau](https://web.archive.org/web/20081108152931/http://eserver.org/thoreau/slavery.html)
  - [The National Archives (UK): The Abolition of the Slave
    Trade](http://www.nationalarchives.gov.uk/pathways/blackhistory/rights/abolition.htm)
  - [John Brown
    Museum](https://web.archive.org/web/20060618001421/http://www.kshs.org/places/johnbrown/index.htm)
  - [American
    Abolitionism](http://americanabolitionist.liberalarts.iupui.edu/)

[廢奴主義](../Category/廢奴主義.md "wikilink")

1.  S.M.Wise, Though the Heavens May Fall, Pimlico (2005)
2.  黎東方，（西洋全史⑭自由屬意與保守主義），（台北市：燕京文化事業股份有限公司，1979），185。
3.  林訓民，（世界歷史博覽-⑦十九世紀），（台北市：青林國際出版股份有限公司，2001），34。
4.  Newman, p. 18,21