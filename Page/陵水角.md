**陵水角**，行政隶属[陵水县](../Page/陵水县.md "wikilink")，地理坐标为[北纬](../Page/北纬.md "wikilink")
18°23'0",[东经](../Page/东经.md "wikilink")110°3'0"，岬角山顶有“陵水角灯桩”。

  - 1996年，中国政府发布关于领海范围的声明，陵水角是中国[领海基点之一](../Page/领海基点.md "wikilink")。[](http://www.mfa.gov.cn/chn/gxh/zlb/tyfg/t556673.htm)

## 参考

<div class="references-small">

<references/>

</div>

## 另见

  - [大洲岛](../Page/大洲岛.md "wikilink")
  - [东洲岛](../Page/东洲岛_\(三亚\).md "wikilink")
  - [锦母角](../Page/锦母角.md "wikilink")

## 外部链接

  - [中国东海１０座领海基点石碑建成](http://news.xinhuanet.com/politics/2006-09/14/content_5090042.htm)

  - [中华人民共和国政府关于中华人民共和国领海基线的声明（1996年5月15日）](http://www.mfa.gov.cn/chn/gxh/zlb/tyfg/t556673.htm)

  - [Declaration of the Government of the People's Republic of China on
    the baselines of the territorial
    sea（May15th, 1996)](http://www.un.org/Depts/los/LEGISLATIONANDTREATIES/PDFFILES/CHN_1996_Declaration.pdf)

  - [在GOOGLEMAP查看陵水角位置](http://www.google.com/maps?f=q&hl=en&q=+18%C2%B022%2760.00%22N,110%C2%B0+2%2760.00%22E&ie=UTF8&z=12&ll=18.383199,110.050049&spn=0.145959,0.43396&t=k&om=1&iwloc=addr)

  - [海南省民政厅受权公告500㎡以上海岛（礁）及海南岛沿岸岬角共108条地名标准化](https://web.archive.org/web/20070310185604/http://www.hainan.gov.cn/data/news/2006/08/16869/)

[Category:中国灯塔](../Category/中国灯塔.md "wikilink")
[琼](../Category/中国大陆领海基点.md "wikilink")