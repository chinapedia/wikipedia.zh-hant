**陳永陸**（），人稱「陸叔」，[香港](../Page/香港.md "wikilink")[股市著名評論人及投資策略顧問](../Page/股市.md "wikilink")。陸叔在金融界擁有超過三十年經驗，是香港受歡迎及具權威的投資專家之一。

## 簡歷

陳永陸中學就讀於[英皇書院](../Page/英皇書院.md "wikilink")，1973年[香港大學](../Page/香港大學.md "wikilink")[化學系畢業](../Page/化學.md "wikilink")，先後在[匯豐銀行](../Page/匯豐銀行.md "wikilink")（陳永陸1976年在[匯豐銀行擔任見習行政人員時](../Page/匯豐銀行.md "wikilink")，曾在[瓦努阿圖見習半年](../Page/瓦努阿圖.md "wikilink")）\[1\]、[道亨銀行](../Page/道亨銀行.md "wikilink")、[德意志銀行及](../Page/德意志銀行.md "wikilink")[大福證券工作](../Page/大福證券.md "wikilink")。1990年，陳永陸在其中學同學的弟弟[魯連城幫助下](../Page/魯連城.md "wikilink")，加入[大福證券出任副董事總經理](../Page/大福證券.md "wikilink")，其後認識[商業電台的](../Page/商業電台.md "wikilink")[顏聯武](../Page/顏聯武.md "wikilink")，並令陳永陸在商業電台主持財經節目。2000年科網熱潮期間，陳永陸離開大福證券，開設網站hk6及公關公司。

陳永陸的節目會接聽電話，聽眾多會詢問投資意見。陳氏有眾多擁躉，是大眾媒體上主持個人財經節目的代表人物。曾在《[信報](../Page/信報.md "wikilink")》撰寫專欄的[蔡東豪](../Page/蔡東豪.md "wikilink")，在專欄裡曾指這種任何股票都可以表達意見的主持人是“財經演員”\[2\]\[3\]。

陳永陸亦推出過一些財經書籍，例如《陸陸無窮系列》。陳永陸女兒曾就讀美國[耶魯大學](../Page/耶魯大學.md "wikilink")。

現時陳永陸亦為擔任[無綫電視](../Page/無綫電視.md "wikilink")[無綫財經·資訊台](../Page/無綫財經·資訊台.md "wikilink")《商·對論》主持。

## 資料來源

[Category:香港股評人](../Category/香港股評人.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港作家](../Category/香港作家.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:英皇書院校友](../Category/英皇書院校友.md "wikilink")
[W](../Category/陳姓.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")

1.
2.  [財經演員](http://hk.apple.nextmedia.com/template/apple_sub/art_main.php?iss_id=20050829&sec_id=12187389&art_id=5176042)
    左丁山，蘋果日報，2005年08月29日
3.  [陸東現象
    第壹流](http://hk.next.nextmedia.com/template/next/art_main.php?iss_id=1011&sec_id=1000848&art_id=13016551)
    壹周刊，2009年07月23日