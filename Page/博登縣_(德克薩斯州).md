**博登縣**（）位[美國](../Page/美國.md "wikilink")[德克薩斯州](../Page/德克薩斯州.md "wikilink")[埃斯塔卡多平原邊緣的一個縣](../Page/埃斯塔卡多平原.md "wikilink")。面積2,347平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口729人。縣治[蓋爾](../Page/蓋爾_\(德克薩斯州\).md "wikilink")（Gail）。

成立於1876年8月21日，縣政府成立於1891年3月17日。縣名紀念[煉奶的發明者](../Page/煉奶.md "wikilink")[蓋爾·博登](../Page/蓋爾·博登.md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[B](../Category/得克萨斯州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.