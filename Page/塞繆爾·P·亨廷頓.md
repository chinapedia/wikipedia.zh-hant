**塞缪尔·菲利普斯·亨廷顿**（，），当代颇有争议的[美国](../Page/美国.md "wikilink")[保守派](../Page/保守派.md "wikilink")[政治学家](../Page/政治学.md "wikilink")。他以《[文明冲突论](../Page/文明冲突论.md "wikilink")》闻名于世，认为21世纪[國際政治的核心政治角力是在不同文明之间而非国家之间](../Page/國際政治.md "wikilink")。\[1\]亨廷頓与[莫里斯·詹诺维茨是](../Page/莫里斯·詹诺维茨.md "wikilink")20世纪晚期[政军关系研究的先驱者](../Page/政军关系.md "wikilink")。\[2\]。他个人的政治定位是[民主党右翼](../Page/美国民主党.md "wikilink")\[3\]。近来，他对美国[移民问题的看法亦广受关注](../Page/移民问题.md "wikilink")。

## 人物生平

亨廷頓于1927年在[紐約市出生](../Page/紐約市.md "wikilink")。他高中畢業後進入[耶鲁大学](../Page/耶鲁大学.md "wikilink")，曾在[美國陸軍中短暫服役](../Page/美國陸軍.md "wikilink")。退役後在[芝加哥大學获](../Page/芝加哥大學.md "wikilink")[碩士學位](../Page/碩士.md "wikilink")，在[哈佛大学获](../Page/哈佛大学.md "wikilink")[哲學博士学位](../Page/哲學博士.md "wikilink")。1950年起，亨廷頓在哈佛大學政治學系获得教職，直到他去世。1959年至1962年期間他曾出任[哥倫比亞大學政治學系助理教授](../Page/哥倫比亞大學.md "wikilink")。2008年12月24日，亨廷頓逝世於[麻薩諸塞州](../Page/麻薩諸塞州.md "wikilink")[瑪莎葡萄園島](../Page/瑪莎葡萄園島.md "wikilink")。

## 主要作品

  - 《文明冲突论》
  - 《第三波：20世纪后期民主化浪潮》：1991年出版，指出，从1974年葡萄牙革命开始，就出现了第三波民主化潮流，描绘了一个包括欧洲60多个国家，
    拉丁美洲，亚洲和非洲的全球趋势。
    亨廷顿因为这本书赢得了1992年[路易斯维尔大学的格文美尔大奖](../Page/路易斯维尔大学.md "wikilink")。
  - 《我们是谁：对美国国家认同的挑战》：亨廷顿的最后一本书，出版于2004年。该书主要讨论了美国的国家认同问题，以及大规模拉丁裔移民对美国可能构成的文化威胁。亨廷顿警告说：“（拉丁裔移民）会将美国割裂为两种人、两种文化、两种语言”。
  - 《士兵与国家：军民关系的理论与政治》
  - 《变化社会中的政治秩序》
  - 《文明的冲突与世界秩序的重建》

## 参考文献

## 参阅相关

  - [文明](../Page/文明.md "wikilink")
  - [文明冲突论](../Page/文明冲突论.md "wikilink")

## 外部链接

  - [哈佛大學為亨廷顿教授做的紀念網頁](https://news.harvard.edu/gazette/story/2009/02/samuel-huntington-81-political-scientist-scholar/)
  - [亨廷顿教授在哈佛大学的主页](https://web.archive.org/web/20060409232404/http://www.gov.harvard.edu/faculty/shuntington/)
  - [南方周末：从亨廷顿新书看布什连任](https://web.archive.org/web/20070226120938/http://www.southcn.com/weekend/culture/200405280026.htm)。

[Category:革命理論家](../Category/革命理論家.md "wikilink")
[Category:美國政治學家](../Category/美國政治學家.md "wikilink")
[Category:地缘政治学家](../Category/地缘政治学家.md "wikilink")
[Category:美國作家](../Category/美國作家.md "wikilink")
[Category:國際關係學者](../Category/國際關係學者.md "wikilink")
[Category:哥倫比亞大學教師](../Category/哥倫比亞大學教師.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:美国战略家](../Category/美国战略家.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:美国国家安全会议成员](../Category/美国国家安全会议成员.md "wikilink")
[Category:美国陆军人物](../Category/美国陆军人物.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")

1.
2.
3.