**謝夫人**()，[會稽](../Page/會稽.md "wikilink")[山陰](../Page/山陰.md "wikilink")（今[浙江省](../Page/浙江省.md "wikilink")[紹興](../Page/紹興.md "wikilink")）人，為吳大帝[孫權元配妻子](../Page/孫權.md "wikilink")，又称谢妃。

## 生平

在[孫權母](../Page/孫權.md "wikilink")[吳夫人的主持下](../Page/孫破虜吳夫人.md "wikilink")，謝氏嫁給孫權為妻，孫權對她也非常寵愛。\[1\]建安五年（200年）至十二年（207年）間，孫權迎娶[徐氏](../Page/徐夫人_\(孙权\).md "wikilink")，\[2\]要求謝夫人讓出正室的位置，謝夫人不情愿，從此感到失意，很早便過逝了。\[3\]

## 親族

<center>

  - 父輩
      - 謝煚，謝夫人之父，東漢末年，曾任尚書郎、徐令。
      - 謝貞，叔父，曾任建昌長。

<!-- end list -->

  - 弟及侄
      - [謝承](../Page/謝承.md "wikilink")，字偉平，同父謝煚之弟，歷任五官郎中、長沙東部都尉、武陵太守，著有《[後漢書](../Page/後漢書.md "wikilink")》百餘卷，《[會稽先賢傳](../Page/會稽先賢傳.md "wikilink")》七卷、《謝承集》四卷。
          - 謝崇，官至揚威將軍。
          - 謝勖，官至吳郡太守。

<!-- end list -->

  - 侄孫女
      - 謝仙女，謝承孫女，曾差點被[孫皓收入后宮](../Page/孫皓.md "wikilink")。\[4\]

## 注释

## 参考资料

  - 《[三國志](../Page/三國志.md "wikilink")•[吳書•吳主權謝夫人傳](../Page/s:三國志/卷50.md "wikilink")》
    [陳壽著](../Page/陳壽.md "wikilink")、[裴松之注](../Page/裴松之.md "wikilink")
  - 《[三國志](../Page/三國志.md "wikilink")•[吳書•吳主權徐夫人傳](../Page/s:三國志/卷50.md "wikilink")》
    [陳壽著](../Page/陳壽.md "wikilink")、[裴松之注](../Page/裴松之.md "wikilink")

## 外部連結

  - 《[三國志•吳書•吳主權謝夫人傳](http://www.guoxue.com/shibu/24shi/sangzz/sgzz_050.htm)》

[Category:东汉女性人物](../Category/东汉女性人物.md "wikilink")
[Category:东吴皇帝嫡妻](../Category/东吴皇帝嫡妻.md "wikilink")
[Category:中国皇帝嫡妻未得封后者](../Category/中国皇帝嫡妻未得封后者.md "wikilink")
[\~](../Category/谢姓.md "wikilink")
[Category:绍兴人](../Category/绍兴人.md "wikilink")

1.  《三國志·吳書·吳主權謝夫人傳》：權母吳為權聘以為妃，愛幸有寵。
2.  《三國志·吳書·吳主權徐夫人傳》：權為討虜將軍在吳，聘以為妃。
3.  《三國志·吳書·吳主權謝夫人傳》：後權納姑孫徐氏，欲令謝下之，謝不肯，由是失志，早卒。
4.  《[太平御覽](../Page/太平御覽.md "wikilink")·[卷365◎人事部六○面](../Page/s:太平御覽/0365.md "wikilink")》引《會稽後賢記》：貞女謝仙女者，謝承孫也。吳歸命侯采仙女充後宮，仙女乃炙面服醇醯以取黃瘦，竟得免。