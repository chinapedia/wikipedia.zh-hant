**秀沿路站**位于[上海市](../Page/上海市.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[康桥镇](../Page/康桥镇_\(上海市\).md "wikilink")，初步选址为[罗南大道](../Page/罗南大道.md "wikilink")[川周公路附近](../Page/川周公路.md "wikilink")。该站原设计为[上海轨道交通11号线南段的高架车站](../Page/上海轨道交通16号线.md "wikilink")，于2012年底启用，现调整为[11号线三期的车站](../Page/上海轨道交通11号线.md "wikilink")，2015年12月19日通車試運營\[1\]。

2014年12月10日站名由康桥东站改为秀沿路站\[2\]。

[Waiting_room_of_Xiuyan_Road_Station_(20170909142127).jpg](https://zh.wikipedia.org/wiki/File:Waiting_room_of_Xiuyan_Road_Station_\(20170909142127\).jpg "fig:Waiting_room_of_Xiuyan_Road_Station_(20170909142127).jpg")

## 參考文獻

<div class="references-small">

<references />

</div>

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:2015年启用的铁路车站](../Category/2015年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")

1.
2.  [11号线迪士尼段三车站命名确定
    更简洁](http://news.hexun.com/2014-12-10/171287623.html)