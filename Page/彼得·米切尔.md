**彼得·丹尼斯·米切尔**（[英语](../Page/英语.md "wikilink")：****，[皇家学会会员（FRS）](../Page/皇家学会.md "wikilink")\[1\]，\[2\]），[英国](../Page/英国.md "wikilink")[生物化学家](../Page/生物化学.md "wikilink")，1978年因为[化学渗透理论建立了公式而获得](../Page/化学渗透.md "wikilink")[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。

## 参考资料

## 外部链接

  - [彼得·米切尔的传记](http://www.life.uiuc.edu/crofts/bioph354/mitchell.html)

  -
[M](../Category/英国生物化学家.md "wikilink")
[M](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")

1.
2.