[Camellia_granthamiana.JPG](https://zh.wikipedia.org/wiki/File:Camellia_granthamiana.JPG "fig:Camellia_granthamiana.JPG")

**葛量洪茶**，原名**大苞山茶**，[杜鵑花目](../Page/杜鵑花目.md "wikilink")[山茶科](../Page/山茶科.md "wikilink")[山茶屬小型常綠](../Page/山茶屬.md "wikilink")[喬木植物](../Page/喬木.md "wikilink")，。首次於1955年被任職農林部的劉松彬在香港[大帽山發現](../Page/大帽山.md "wikilink")，並以當時[港督](../Page/港督.md "wikilink")[葛量洪的名字命名](../Page/葛量洪.md "wikilink")。現在受到香港《林務規例》所保護，並且已載入《[中國植物紅皮書](../Page/中國植物紅皮書.md "wikilink")》和《[廣東省珍稀瀕危植物圖譜](../Page/廣東省珍稀瀕危植物圖譜.md "wikilink")》。

葛量洪茶一般高8米。葉革質，橢圓形，端尖底圓，無毛，邊有細鋸齒；葉柄長8-12毫米。花又大又白而無柄；苞片及萼片圓型；花瓣8片，基部連生；雄蕊無輪；子房有毛，花柱長2厘米。蒴果球形，有3到4個室。花期從每年3月到4月。主要分布在香港的[大帽山](../Page/大帽山.md "wikilink")、[馬鞍山](../Page/馬鞍山_\(香港山峰\).md "wikilink")，還有[廣東等地方](../Page/廣東.md "wikilink")。

## 參見

  - [香港茶](../Page/香港茶.md "wikilink")
  - [山茶花](../Page/山茶花.md "wikilink")

## 參考文獻

<references />

  - [張宏達](../Page/張宏達.md "wikilink")，1991：[廣東植物誌](../Page/廣東植物誌.md "wikilink")2：127。廣東科技出版社，廣州。
  - 張宏達，1998：[中國植物誌](../Page/中國植物誌.md "wikilink")49(3):
    6，圖版1：1-2。科學出版社，北京。
  - [閔天祿](../Page/閔天祿.md "wikilink")，2000：[世界山茶屬的研究](../Page/世界山茶屬的研究.md "wikilink")
    223-224，圖版44：4-6。雲南科技出版社，昆明。
  - Sealy, J.R., 1956: Journal of the Royal Horticultural Society 81:
    182.

## 外部連結

  - [大苞山茶](http://www.hkherbarium.net/Herbarium/html%20text/26Camellia%20granthamiana.htm)

[category:山茶屬](../Page/category:山茶屬.md "wikilink")

[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")