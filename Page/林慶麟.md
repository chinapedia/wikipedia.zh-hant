**林慶麟**（**Lam Hing
Lun**，），生於[香港](../Page/香港.md "wikilink")，已退役[香港足球運動員](../Page/香港.md "wikilink")，司職[左翼](../Page/左翼.md "wikilink")，持有[亞洲足協A級足球教練牌照](../Page/亞洲足球協會.md "wikilink")，退役後曾在[傑志兼任教練](../Page/傑志體育會.md "wikilink")。曾擔任[香港超級聯賽球隊](../Page/香港超級聯賽.md "wikilink")[理文流浪總教練](../Page/香港流浪足球會.md "wikilink")，同時於[香港](../Page/香港.md "wikilink")[now寬頻電視擔任足球評述員](../Page/now寬頻電視.md "wikilink")，並從事青少年足球訓練工作。

## 球會生涯

林慶麟生於[香港住](../Page/香港.md "wikilink")[石蔭東邨蔭興樓](../Page/石蔭東邨.md "wikilink")，球員時代初期司職左翼，後期轉任左閘，由於出道初期留有一把長髮，故有「**飛仔麟**」外號。林慶麟於1985－86年度球季，在「升班馬」[麗新花花初次在甲組亮相](../Page/花花足球會.md "wikilink")，憑個人速度及爆發力很快便打出名堂，林慶麟與[李健和同留了一把長髮](../Page/李健和.md "wikilink")，兩個「長毛飛」同期效力[麗新花花時分踢左右翼](../Page/花花足球會.md "wikilink")，成了當年球壇佳話。但因球隊人滿之患被外借到[東方](../Page/東方足球隊.md "wikilink")，其後重返[花花](../Page/花花足球會.md "wikilink")，繼而轉投[快譯通](../Page/花花足球會.md "wikilink")，可是在[快譯通多年](../Page/花花足球會.md "wikilink")，球隊始終與冠軍無緣，在1992年代表[快譯通到](../Page/花花足球會.md "wikilink")[加拿大出戰](../Page/加拿大.md "wikilink")，1993年林慶麟便將長髮剪掉，到1997年還因沒有班落而要踢乙組的[中福](../Page/中福足球隊.md "wikilink")。2001年球季，林慶麟因累積五面黃牌，結果因停賽而無法助[快譯通在聯賽總決賽上陣](../Page/花花足球會.md "wikilink")，而[快譯通最終也不敵](../Page/花花足球會.md "wikilink")[愉園](../Page/愉園.md "wikilink")。\[1\]

在2001年球季，[快譯通退出本地聯賽後](../Page/花花足球會.md "wikilink")，便到足總擔任青訓教練，當時外界以為他會正式由球員轉做教練；不過之後他離開足總，在待業時巧獲[傑志招手](../Page/傑志.md "wikilink")，結果便以教練兼球員身份，加盟重返甲組的[傑志](../Page/傑志.md "wikilink")。由於隊內未有球員能勝任[左閘](../Page/左閘.md "wikilink")，便身兼兩職踢下去。2005年6月4日，[傑志對](../Page/傑志體育會.md "wikilink")[祖雲達斯的表演賽](../Page/祖雲達斯.md "wikilink")，是林慶麟的球員時代告別戰，[傑志在落後兩球的劣勢下擊敗](../Page/傑志體育會.md "wikilink")[祖雲達斯](../Page/祖雲達斯.md "wikilink")，林慶麟便在這麼多球迷面前以贏波告別。\[2\]
掛靴後沒有離開球迷視線，每周固定出鏡作外國足球評述，又有報章專欄分享心得。2015年10月15日，林慶麟代表「519香港傳奇足球隊」，在[小西灣運動場舉行在的](../Page/小西灣運動場.md "wikilink")「英超足球大師邀請賽」迎戰「英超足球大師隊」，最終以4–5不敵對手。\[3\]

## 國際賽生涯

林慶麟於1988年便開始入選[香港代表隊](../Page/香港足球代表隊.md "wikilink")，其間有踢過[省港盃](../Page/省港盃.md "wikilink")、[滬港盃及](../Page/滬港盃.md "wikilink")[亞運](../Page/亞運.md "wikilink")，但就從未踢過[世界盃外圍賽](../Page/世界盃外圍賽.md "wikilink")。在1994年的[廣島亞運](../Page/1994年亞洲運動會.md "wikilink")，[香港隊憑左右翼林慶麟及](../Page/香港足球代表隊.md "wikilink")[李健和各建一功](../Page/李健和.md "wikilink")，以2–1擊敗[泰國](../Page/泰國國家足球隊.md "wikilink")。\[4\]

## 退役生涯

林慶麟退役後考取教練牌，擁有[亞洲足協A級足球教練牌照](../Page/亞洲足球協會.md "wikilink")，及後轉任足球評述員，現時在[now寬頻電視工作](../Page/now寬頻電視.md "wikilink")。此外，亦從事青少年足球訓練工作，現時是「阿仙奴香港足球學校」教練之一。2016年7月19日，林慶麟重返球圈擔任[理文流浪總教練](../Page/理文流浪.md "wikilink")，因為經常戴太陽眼鏡亮相教波，令其有「**黑超教頭**」的外號。\[5\]。

## 参考資料

## 外部連結

  - [不再「飛仔」的林慶麟](http://www.kitchee.com/index.php?option=com_content&task=view&id=140&Itemid=97)

[分類:裘錦秋中學校友](../Page/分類:裘錦秋中學校友.md "wikilink")

[Category:林姓](../Category/林姓.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港足球主教練](../Category/香港足球主教練.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")
[Category:香港體院足球運動員](../Category/香港體院足球運動員.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:花花球員](../Category/花花球員.md "wikilink")
[Category:麗新花花球員](../Category/麗新花花球員.md "wikilink")
[Category:東方球員](../Category/東方球員.md "wikilink")
[Category:快譯通球員](../Category/快譯通球員.md "wikilink")
[Category:好易通球員](../Category/好易通球員.md "wikilink")
[Category:星島球員](../Category/星島球員.md "wikilink")
[Category:傑志球員](../Category/傑志球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:Now寬頻電視足球評述員](../Category/Now寬頻電視足球評述員.md "wikilink")

1.  [【第12人】贏咗半程！黑超教頭繼續快放](http://hk.on.cc/hk/bkn/cnt/sport/20170330/bkn-20170330000548730-0330_00882_001.html)
    【on.cc東網】2017年03月30日
2.  [挫祖記完美告別　林慶麟今生無悔](http://hk.apple.nextmedia.com/sports/art/20050610/4958226)
    [蘋果日報](../Page/蘋果日報.md "wikilink") 2005年06月10日
3.  [英超大師賽　史高斯科拿入世界波
    粉絲癲晒](http://hk.on.cc/hk/bkn/cnt/sport/20151015/bkn-20151015213854683-1015_00882_001.html)
    【on.cc東網】2015年10月15日
4.  [傑志誓盯死 祖雲三箭頭](http://hk.sports.yahoo.com/050603/42/1d5xh.html)
5.  [理文流浪現身！馮奇：會接收東方兵
    東網即時新聞－體育，2016年7月19日](http://hk.on.cc/hk/bkn/cnt/sport/20160719/bkn-20160719140947621-0719_00882_001.html)