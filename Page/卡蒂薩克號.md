**卡蒂薩克號**（****，又名
**短襯衫號**、**克里斯蒂克號**等），是一艘現存在世的最古老[帆船](../Page/帆船.md "wikilink")，款式是英國3桅快速機帆船，長64.7公尺。卡蒂薩克號現位於[英國](../Page/英國.md "wikilink")[泰晤士河旁的](../Page/泰晤士河.md "wikilink")[格林尼治](../Page/格林尼治.md "wikilink")[港口上](../Page/港口.md "wikilink")，作為[博物館](../Page/博物館.md "wikilink")。[管理组织為](../Page/管理.md "wikilink")[卡蒂薩克號信託](../Page/卡蒂薩克號信託.md "wikilink")。

卡蒂薩克號這個名字來自蘇格蘭著名詩人[羅伯特·伯恩斯](../Page/羅伯特·伯恩斯.md "wikilink")（Robert
Burns）的「Tam O'Shanter」一詩，Cutty Sark意指漂亮女巫的白色短襯衣。

## 歷史

  - 此帆船建於1869年的英國[蘇格蘭](../Page/蘇格蘭.md "wikilink")，花費16,150[英鎊](../Page/英鎊.md "wikilink")，是[十九世紀著名](../Page/十九世紀.md "wikilink")[運輸](../Page/運輸.md "wikilink")[茶葉的](../Page/茶葉.md "wikilink")[船](../Page/船.md "wikilink")，在1870年至1878年期間，往來[中國與](../Page/中國.md "wikilink")[英國兩地](../Page/英國.md "wikilink")，作為[茶葉貿易的](../Page/茶葉貿易.md "wikilink")[遠洋運輸工具](../Page/遠洋運輸.md "wikilink")。
  - 之後，遠渡[澳大利亞](../Page/澳大利亞.md "wikilink")，改作[羊毛](../Page/羊毛.md "wikilink")[貿易](../Page/貿易.md "wikilink")。
  - 2007年5月21日，在英國[裝修期間](../Page/裝修.md "wikilink")，卡蒂薩克號發生[火災](../Page/火災.md "wikilink")，及後進行修復至今。

## 外部参考

  - [倫敦國寶級帆船「卡蒂薩克號」火災。](https://web.archive.org/web/20070927224726/http://www.mingpaosf.com/htm/News/20070522/sna1.htm)
    [明報](../Page/明報.md "wikilink")

[Category:帆船](../Category/帆船.md "wikilink")
[Category:英國船艦](../Category/英國船艦.md "wikilink")