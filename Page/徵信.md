**[徵信](https://www.credits-search.com/)**（Credit Checking或Credit
Investigation）或稱「授信」，可以簡單視作為「對公司或個人的信用驗證工作」。但是由於中文使用的意義延伸，在[台灣提到徵信](../Page/台灣.md "wikilink")，一般人都會想到[徵信社](https://www.credits-search.com/)。

在工商社會的環境中，正確使用**徵信**這個字眼，主要是銀行或企業對於另一家公司或個人，由於要提供有擔保或無擔保的貸款或[受信額度](../Page/受信額度.md "wikilink")，因此，必須要先針對貸款申請者或是申請受信的公司進行信用調查，也就是所謂的**徵信**。

一般人常見的徵信工作包含申請[信用卡或](../Page/信用卡.md "wikilink")[貸款時的銀行審核](../Page/貸款.md "wikilink")（[聯徵中心](../Page/聯徵中心.md "wikilink")，就是專門提供銀行相關的個人資料，以便於銀行評估個人的信用卡額度或貸款額度）、公司和公司交易過程中，由A公司先提供B公司一定的[信用額度前的審核工作](../Page/信用額度.md "wikilink")（[中華徵信所及](../Page/中華徵信所.md "wikilink")[中華信評主要是提供這個部份的服務](../Page/中華信評.md "wikilink")，但前者提供的是短期信用狀況評估，後者提供的是中長期信用狀況評估，有些許差異），都是徵信工作的一種。

## 徵信的審查內容

徵信究竟是在徵信什麼？最常被徵信提出來的架構就是5C\[1\]、3F或5P。

  - 5C指的是：

<!-- end list -->

1.  品格（Character）
2.  能力（Capacity）
3.  資本（Capital）
4.  擔保品（Collateral）
5.  經營條件（Condition）

<!-- end list -->

  - 3F指的是：

<!-- end list -->

1.  個人因素（Personal Factor)
2.  財務因素 (Financial Factor)
3.  經濟因素 (Economy Factor)

<!-- end list -->

  - 5P指的是：

<!-- end list -->

1.  借款戶因素（People）
2.  資金用途因素（Purpose）
3.  還款財源因素（Payment）
4.  債權保障因素（Protection）
5.  授信展望因素（Perspective）

<!-- end list -->

  - 5原則是：

<!-- end list -->

1.  安全性
2.  流動性
3.  收益性
4.  成長性
5.  公益性

## 參考資料

<div class="references-small">

<references />

</div>

## 相关条目

  - [信用风险](../Page/信用风险.md "wikilink")
  - [信用额度](../Page/信用额度.md "wikilink")
  - [信用评分](../Page/信用评分.md "wikilink")

[Z](../Category/信用.md "wikilink")
[Category:金融服務](../Category/金融服務.md "wikilink")

1.  簡安泰，授信評估五原則，銀行對企業授信規範，第10版，台北：金融人員研究訓練中心，1990年