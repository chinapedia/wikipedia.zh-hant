**辜嚴倬雲**（英語：Cecilia Yen
Koo，），[福建省](../Page/福建省.md "wikilink")[福州倉山區陽岐鄉人](../Page/福州.md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[河南](../Page/河南.md "wikilink")[固始](../Page/固始.md "wikilink")，北京大學首任校長[嚴復之孙女](../Page/嚴復.md "wikilink")，[板橋林家](../Page/板橋林家.md "wikilink")[林爾康之外孙女](../Page/林爾康.md "wikilink")，[鹿港辜家](../Page/鹿港辜家.md "wikilink")[辜振甫之妻](../Page/辜振甫.md "wikilink")。辜嚴女士也是名作家[華嚴](../Page/華嚴_\(作家\).md "wikilink")（本名嚴停雲）的三姊、[嚴僑之妹](../Page/嚴僑.md "wikilink")，[馬英九總統任內聘其為](../Page/馬英九.md "wikilink")[總統府資政](../Page/總統府資政.md "wikilink")。

## 簡歷

出生於[福州](../Page/福州.md "wikilink")[鼓樓區的郎官巷](../Page/鼓樓區.md "wikilink")，就讀進德小學、[南洋模範中學](../Page/南洋模範中學.md "wikilink")、[上海聖約翰大學](../Page/上海聖約翰大學.md "wikilink")（肄業），後隨母和舅媽赴台。执教于[建国中学](../Page/臺北市立建國高級中學.md "wikilink")，1949年與[辜振甫結婚](../Page/辜振甫.md "wikilink")，婚禮在[台北中山堂舉行](../Page/台北中山堂.md "wikilink")，證婚人是[連震東](../Page/連震東.md "wikilink")。婚後育有二子三女。長女辜懷群，二女辜懷箴，三女辜懷如，長子[辜啟允](../Page/辜啟允.md "wikilink")，二子[辜成允](../Page/辜成允.md "wikilink")。2006年表態支持[施明德倒扁運動](../Page/施明德.md "wikilink")，並捐出新台幣100元。積極參與社會公益，原任[婦聯會主任委員](../Page/婦聯會.md "wikilink")，但由於婦聯會並未針對「捐出九成資產、由政府任命1/3董事至婦聯會贊助成立的四個基金會」等節與內政部達成協議，拒絕簽署行政契約，於2017年12月22日被內政部撤換婦聯會主委一職\[1\]。

## 相關組織

  - [中華民國婦女聯合會](../Page/中華民國婦女聯合會.md "wikilink")
  - 財團法人辜嚴倬雲植物保種暨環境保護發展基金會（下設[辜嚴倬雲植物保種中心](../Page/辜嚴倬雲植物保種中心.md "wikilink")）

## 參考來源

[D](../Category/嚴姓.md "wikilink") [Y](../Category/鹿港辜家.md "wikilink")
[Category:板橋林家](../Category/板橋林家.md "wikilink")
[Y](../Category/仓山人.md "wikilink")
[Y](../Category/台灣外省人.md "wikilink")
[Y](../Category/台灣戰後福建移民.md "wikilink")
[Y](../Category/南洋公學校友.md "wikilink")
[Y](../Category/聖約翰科技大學校友.md "wikilink")
[Y](../Category/上海市南洋模范中学校友.md "wikilink")
[Y](../Category/中華民國總統府資政.md "wikilink")
[Category:國立故宮博物院人物](../Category/國立故宮博物院人物.md "wikilink")

1.