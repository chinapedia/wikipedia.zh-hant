[Southern_Leyte_mudslide_2006_pic01.jpg](https://zh.wikipedia.org/wiki/File:Southern_Leyte_mudslide_2006_pic01.jpg "fig:Southern_Leyte_mudslide_2006_pic01.jpg")中部[莱特岛經過兩週暴雨後發生山崩](../Page/莱特岛.md "wikilink")，至少300人死亡，1,500人失蹤。\]\]
[Earthslide_in_Taroko.jpg](https://zh.wikipedia.org/wiki/File:Earthslide_in_Taroko.jpg "fig:Earthslide_in_Taroko.jpg")[花蓮](../Page/花蓮.md "wikilink")[太魯閣峽谷山崩](../Page/太魯閣峽谷.md "wikilink")，交通受阻。\]\]
[ElSalvadorslide.jpg](https://zh.wikipedia.org/wiki/File:ElSalvadorslide.jpg "fig:ElSalvadorslide.jpg")
**山崩**又稱**山体滑坡**、**山泥傾瀉**、**土溜**，是指在[重力的影響下](../Page/重力.md "wikilink")[塊體沿着一段山坡下滑的現象](../Page/塊體.md "wikilink")，又稱作**坍方**。若是土體坍方時，混和[雨水或](../Page/雨水.md "wikilink")[河水則演變成](../Page/河川.md "wikilink")[土石流](../Page/土石流.md "wikilink")。

**地滑**又稱**走山**，是融合岩屑和土壤的**岩體**沿層面滑動或移動，與山崩不同。

## 原因

山崩最主要的原因是山坡上的岩石或土壤吸收了大量的[水](../Page/水.md "wikilink")（比如由于暴雨或者融雪），导致岩石或土壤内部的摩擦力降低，土壤或岩石丧失其稳固性下滑。\[1\]
當斜坡上的引力較抗力強,斜坡便不穩固,有倒塌的危險 其它原因有

  - [地震](../Page/地震.md "wikilink")
  - 其它[地壳运动](../Page/地壳运动.md "wikilink")
  - [风和](../Page/风.md "wikilink")[霜冻造成的](../Page/霜冻.md "wikilink")[风化](../Page/风化.md "wikilink")
  - 由于[垦荒和强烈的](../Page/垦荒.md "wikilink")[采矿造成的土壤和植被的破坏](../Page/采矿.md "wikilink")
  - [火山爆发](../Page/火山.md "wikilink")
  - 違規[堆放沙泥](../Page/堆放沙泥.md "wikilink")

[St_Helens_before_1980_eruption.jpg](https://zh.wikipedia.org/wiki/File:St_Helens_before_1980_eruption.jpg "fig:St_Helens_before_1980_eruption.jpg")\]\]
[MSH82_st_helens_spirit_lake_reflection_05-19-82.jpg](https://zh.wikipedia.org/wiki/File:MSH82_st_helens_spirit_lake_reflection_05-19-82.jpg "fig:MSH82_st_helens_spirit_lake_reflection_05-19-82.jpg")\]\]

山崩发生的可能性由以下因素决定：

  - [崩壞作用](../Page/崩壞作用.md "wikilink")
  - 地表的吸水性和透水性
  - 山坡的坡度
  - 是否有加固土壤稳定性的[植被](../Page/植被.md "wikilink")
  - 是否有易滑动（比如[粘土](../Page/粘土.md "wikilink")）的土壤或岩石层〈順向坡

## 形式

山崩是一个非常复杂的过程。参加山崩的物质沿山坡向下滚动，在这个过程中它还可能携带坡上其它的物质如树木、冰雪和建筑的部分结构。

## 导致全球性影响的山崩

山崩是一种常见的[自然灾害](../Page/自然灾害.md "wikilink")。

由于[板块运动造成的山崩却可能造成具有全球性影响的危害](../Page/板块构造论.md "wikilink")。比如造成大规模的[海啸](../Page/海啸.md "wikilink")。

## 参考文献

## 參見

  - [小林部落滅村](../Page/小林里#獻肚山走山.md "wikilink")
  - [觀龍樓山泥傾瀉事件](../Page/觀龍樓山泥傾瀉事件.md "wikilink")
  - [2010年福爾摩沙高速公路山崩事件](../Page/2010年福爾摩沙高速公路山崩事件.md "wikilink")
  - [2010年沪昆铁路列车脱轨事故](../Page/2010年沪昆铁路列车脱轨事故.md "wikilink")
  - [2014年廣島土石流](../Page/2014年廣島土石流.md "wikilink")
  - [2015年深圳光明新区山体滑坡事故](../Page/2015年深圳光明新区山体滑坡事故.md "wikilink")

[山崩](../Category/山崩.md "wikilink")
[Category:地质学](../Category/地质学.md "wikilink")

1.  [國土資訊系統自然環境分組](http://ngis.moea.gov.tw/moeaweb/Rinfo/KMSubItem.aspx?ID=32)