**野茉莉科**也叫**安息香科**，共有11-12[属约](../Page/属.md "wikilink")150-170[种](../Page/种.md "wikilink")，分布在北半球的暖[温带和](../Page/温带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，[中国有](../Page/中国.md "wikilink")9属约60余种，主要分布于[长江以南各地](../Page/长江.md "wikilink")。

本科[植物基本为](../Page/植物.md "wikilink")[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，单[叶互生](../Page/叶.md "wikilink")，全缘或有齿缺，无托叶；[花两性](../Page/花.md "wikilink")，辐射对称，[花冠](../Page/花冠.md "wikilink")2-5裂，覆瓦状或镊合状排列；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，有的[种子有翅](../Page/种子.md "wikilink")。

有的种类可以作为[木材使用](../Page/木材.md "wikilink")，大部分是观赏[树种](../Page/树.md "wikilink")，其中[安息香属一些种类的树皮可以提取树脂制造苏合香](../Page/安息香属.md "wikilink")，作为香料或药材。

1981年的[克朗奎斯特分类法将其列入](../Page/克朗奎斯特分类法.md "wikilink")[柿树目](../Page/柿树目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其合并到](../Page/APG_分类法.md "wikilink")[杜鹃花目](../Page/杜鹃花目.md "wikilink")

[Pterostyrax_hispida2.jpg](https://zh.wikipedia.org/wiki/File:Pterostyrax_hispida2.jpg "fig:Pterostyrax_hispida2.jpg")

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[野茉莉科](http://delta-intkey.com/angio/www/styracac.htm)

[安息香科](../Category/安息香科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")