**何文田邨**（）是[香港的一個](../Page/香港.md "wikilink")[公共屋邨及](../Page/公共屋邨.md "wikilink")[香港房屋委員會總部](../Page/香港房屋委員會.md "wikilink")，位於[九龍城區](../Page/九龍城區.md "wikilink")[採石山](../Page/採石山.md "wikilink")[常富街旁](../Page/常富街.md "wikilink")，[佛光街與](../Page/佛光街.md "wikilink")[常樂街交界](../Page/常樂街.md "wikilink")，面對香港房屋委員會總部1及2座，並與香港房屋委員會總部第3及4座和居屋**冠暉苑**、**冠熹苑**及**冠德苑**座落同一地段。由房屋署總建築師（3）設計，並由中國海外物業服務有限公司負責屋邨管理。

**冠暉苑**（）、**冠熹苑**（）和**冠德苑**（）是何文田邨旁的三個[居者有其屋屋苑](../Page/居者有其屋.md "wikilink")。

## 歷史及概要

何文田邨前身為[政府廉租屋邨](../Page/政府廉租屋邨.md "wikilink")，始建於1970年代初，於1973-75年分期落成，共建有8座新型廉租大廈（1-8座），以及[基浩學校和](../Page/基浩學校.md "wikilink")[陳瑞祺（喇沙）小學兩座](../Page/陳瑞祺（喇沙）小學.md "wikilink")[小學](../Page/小學.md "wikilink")（前者於2001年屋邨清拆時一同拆卸；後者於2007年遷至[德蘭中學](../Page/德蘭中學.md "wikilink")／[旅港開平商會中學對面](../Page/旅港開平商會中學.md "wikilink")）。

1997年，政府在舊何文田邨的其中一個休憩球場公園，改建成一座40層的[和諧一型大廈](../Page/和諧一型.md "wikilink")，名為「景文樓」，成為何文田邨內首座和諧式大廈。2000年，餘下的8座大廈在[採石山建成](../Page/採石山.md "wikilink")，舊廈於2001年清拆。

### 舊何文田邨地皮

舊何文田邨的土地地皮原擬重建為多幢[新和諧一型大廈](../Page/新和諧一型.md "wikilink")，並預計在2005-07年入伙，後因2002年[房屋政策改變而交回](../Page/孫九招.md "wikilink")[香港政府並重新拍賣](../Page/香港政府.md "wikilink")，出售予私人發展商興建[豪宅](../Page/豪宅.md "wikilink")，並且將何文田邨地皮一分為四，以[何文田巴士總站為中心](../Page/何文田巴士總站.md "wikilink")。

位於常樂街與常盛街交界的九龍內地段第11227號，地皮由[嘉里建設成功以](../Page/嘉里建設.md "wikilink")116.8788億港元投得，現為[私人屋苑](../Page/私人屋苑.md "wikilink")[皓畋](../Page/皓畋.md "wikilink")。面積達259,165平方呎，可建樓面面積超過114萬平方呎。因該地區鄰近九龍傳統豪宅區[窩打老道山](../Page/窩打老道山.md "wikilink")，重新發展以實用面積500尺以上的私人樓宇住宅群。

位於[佛光街與常富街交界的九龍內地段第](../Page/佛光街.md "wikilink")11228號的住宅用地，有13大財團入標。最後以38.29億元批予[會德豐地產](../Page/會德豐地產.md "wikilink")，現為[私人屋苑](../Page/私人屋苑.md "wikilink")[One
Homantin](../Page/One_Homantin.md "wikilink")。地盤面積約為7,714平方米，指定作私人住宅用途，最低及最高的樓面面積分別為21,613平方米及36,022平方米。每呎樓面地價為9875.15元。市場對該地皮估值為38億元至42.7億元，呎價介乎9,800元至11,000元。

鄰近[公主道的一塊地皮何文田車輛扣留中心](../Page/公主道.md "wikilink")。鄰近[香港公開大學及](../Page/香港公開大學.md "wikilink")[香港足球總會地皮再分為兩塊地皮](../Page/香港足球總會.md "wikilink")。

### 新何文田邨

新興建的何文田邨8座樓宇屬[和諧式大廈](../Page/和諧式大廈.md "wikilink")，於1999年起相繼落成。由於位於舊何文田邨南面，因此項目名稱是「何文田南發展計劃」，並標示在地盤範圍街板上，故落成初期，街坊曾俗稱為「何文田南邨」，惟隨著舊何文田邨清拆後，已再沒有南北邨之分。新何文田邨附設有商場[何文田廣場與街市](../Page/何文田廣場.md "wikilink")，由[信和物業服務有限公司負責兩者的物業管理](../Page/信和物業服務有限公司.md "wikilink")。廣場佔地9,000平方米，而上方為香港房屋委員會總部第3及4座。

原計劃整個何文田邨重建後共有十多座出租公屋及居屋樓宇，每幢12至40層不等，由於設計時預計落成時[啟德國際機場仍可能在運作](../Page/啟德機場.md "wikilink")，故前排房署總部第3及4座、恬文、靜文、雅文、欣文及冠暉苑只有十多層高，而後排4幢40層高和諧一型大廈及冠熹苑則待[香港國際機場於](../Page/香港國際機場.md "wikilink")1998年7月6日正式遷往[赤鱲角後才繼續高度限制線之上的樓宇建造工程](../Page/赤鱲角.md "wikilink")，以符合機場高度限制之規定，故2000年才能入伙。此邨共提供約9,000多個單位。

2016年10月23日\[1\]，港鐵[觀塘綫延綫](../Page/觀塘綫延綫.md "wikilink")[何文田站落成啟用](../Page/何文田站.md "wikilink")，A3出入口行人天橋連接常樂街；同站之[屯馬綫亦將於](../Page/屯馬綫.md "wikilink")2019年通車。

  - 何文田廣場

Ho Man Tin Plaza Ground Level.jpg|地下 Ho Man Tin Plaza Level 1 under
renovation in July 2018.jpg|1樓 Ho Man Tin Plaza Level 2.jpg|2樓 Ho Man
Tin Plaza Level 3 (2).jpg|3樓（2） Ho Man Tin
Plaza.jpg|3樓（2015年，當時的超級市場為[華潤萬家](../Page/華潤萬家.md "wikilink")）

  - 何文田街市

Homantin Market (2).jpg|魚檔 Homantin Market (3).jpg|肉檔及燒味檔 Homantin
Market (4).jpg|香燭祭品及水電工程店 Homantin Market (5).jpg|報檔 Ho Man Tin
Market.jpg|翻新前（2015年）

## 冠暉苑

**冠暉苑**是[重建置業計劃下出售的居者有其屋屋苑](../Page/重建置業計劃.md "wikilink")，並不屬任何一期居屋發售計劃，其申請表是黃色及藍色，亦有別於傳統居屋的綠表和白表。屬於何文田南發展計劃第一期的一部份，由房屋署總建築師（3）設計，而利安建築師顧問有限公司亦也參與設計，有利建築有限公司承建，前身為何文田邨公屋及後轉作居屋出售，苑內共有兩座樓宇，共300個單位，鳴暉閣為和諧三C型的第三款設計,
樓高12層，提供148個和諧式單位，備有一房，兩房及三房式間格。而高暉閣則坐落5層高的平台上，提供9層住宅合共152個小型單位，單位實用面積約27平方米，為全港唯一全幢小型單位設計的居屋大廈。

房委會於1998年，接受[山谷道邨第一期重建戶申請認購冠暉苑](../Page/山谷道邨.md "wikilink")，申請費為港幣135元正。受山谷道邨一期重建影響的居民有第一優先資格選購冠暉苑。若有剩餘單位就讓山谷道邨二期、何文田邨及紅磡邨重建戶選購，再有剩餘則撥入20期甲居屋中出售。而由於該次居屋發售錄得足額認購，所以冠暉苑是未曾正式公開發售過的居屋屋苑，冠暉苑在1999年入伙。\[2\]\[3\]

## 冠熹苑

**冠熹苑**也是重建置業計劃下出售的居者有其屋屋苑，由房屋署總建築師（3）設計，供區內何文田邨及山谷道邨租戶優先選購，其次是紅磡邨租戶，餘下單位則售予同期其他申請人。冠熹苑並連同21期甲居屋發售計劃一併發售，為何文田南發展計劃第三期的一部份，由瑞安承建有限公司承建，全苑只有一座樓宇，共796個單位，備有三房，兩房，一房及小型單位，在2000年入伙。由於屋苑位於九龍中部的山崗之上，所以高層及中層單位更可遠眺[香港島北岸及飽覽整個](../Page/香港島.md "wikilink")[維多利亞港](../Page/維多利亞港.md "wikilink")。

## 冠德苑

[HK_HoManTinEstate_YanManHse.JPG](https://zh.wikipedia.org/wiki/File:HK_HoManTinEstate_YanManHse.JPG "fig:HK_HoManTinEstate_YanManHse.JPG")
[HK_HoManTinEstate_NgaManHse.JPG](https://zh.wikipedia.org/wiki/File:HK_HoManTinEstate_NgaManHse.JPG "fig:HK_HoManTinEstate_NgaManHse.JPG")
[Kwun_Fai_Court.JPG](https://zh.wikipedia.org/wiki/File:Kwun_Fai_Court.JPG "fig:Kwun_Fai_Court.JPG")
[Exterior_of_Ho_Man_Tin_Plaza.jpg](https://zh.wikipedia.org/wiki/File:Exterior_of_Ho_Man_Tin_Plaza.jpg "fig:Exterior_of_Ho_Man_Tin_Plaza.jpg")
[Ho_Man_Tin_Plaza_Level_3.jpg](https://zh.wikipedia.org/wiki/File:Ho_Man_Tin_Plaza_Level_3.jpg "fig:Ho_Man_Tin_Plaza_Level_3.jpg")
[Homantin_Market.jpg](https://zh.wikipedia.org/wiki/File:Homantin_Market.jpg "fig:Homantin_Market.jpg")
[Ho_Man_Tin_Estate_Covered_Walkway.jpg](https://zh.wikipedia.org/wiki/File:Ho_Man_Tin_Estate_Covered_Walkway.jpg "fig:Ho_Man_Tin_Estate_Covered_Walkway.jpg")
[Ho_Man_Tin_Estate_Amphitheater.jpg](https://zh.wikipedia.org/wiki/File:Ho_Man_Tin_Estate_Amphitheater.jpg "fig:Ho_Man_Tin_Estate_Amphitheater.jpg")
在冠熹苑及何文田邨適文樓後面的何文田邨範圍，原本計劃是2座40層高[康和一型雙子式設計居屋](../Page/康和型大廈.md "wikilink")，並設升降機塔連接何文田邨，後來因政府停建居屋而擱置興建。隨政府復建居屋，該地皮近年已經重新劃作新居屋項目用地，即「常樂街居屋發展計劃」（英文名稱：Home
Ownership Scheme Development at Sheung Lok Street,
Homantin）。目前，該新居屋項目已命名為**冠德苑**\[4\]\[5\]，由房屋署總建築師（2）設計，並由[新福港集團承建](../Page/新福港集團.md "wikilink")，共有3幢26至27層高的非標準型長型設計的居屋大廈，每層有7至9伙，主要提供可間3房連套房及可間2房的無固定間隔居屋單位。冠德苑於2019年9月完工，並將於居屋2019發售計劃中以現樓方式出售\[6\]。

## 屋邨資料

### 何文田邨

| 樓宇名稱（座號）  | 樓宇類型                                 | 落成年份 | 層數 |
| --------- | ------------------------------------ | ---- | -- |
| 景文樓（第10座） | [和諧一型](../Page/和諧一型.md "wikilink")   | 1998 | 40 |
| 逸文樓（第8座）  | 2000                                 |      |    |
| 采文樓（第7座）  |                                      |      |    |
| 適文樓（第5座）  |                                      |      |    |
| 綺文樓（第6座）  |                                      |      |    |
| 靜文樓（第2座）  | [和諧3B型](../Page/和諧3B型.md "wikilink") | 12   |    |
| 恬文樓 （第1座） | 15                                   |      |    |
| 欣文樓（第13座） | [小型單位大廈](../Page/和諧一型.md "wikilink") | 14   |    |
| 雅文樓（第12座） |                                      |      |    |
|           |                                      |      |    |

（註：因發展計劃內的第3，4及14座已經轉作居者有其屋計劃屋苑及因發展計劃內的部份地段已經售予私人發人發展商興建私人樓宇的關係，第9及11座已被刪除，致使部分座號被略去。上述座號是以房屋署Estate
Property 的紀錄為依歸。）

### 冠暉苑

| 樓宇名稱（座號）     | 樓宇類型                                 | 落成年份 | 層數 |
| ------------ | ------------------------------------ | ---- | -- |
| 鳴暉閣（A座/第3座）  | [和諧3C型](../Page/和諧一型.md "wikilink")  | 1999 | 11 |
| 高暉閣（B座/第14座） | [小型單位大廈](../Page/和諧一型.md "wikilink") | 9    |    |
|              |                                      |      |    |

### 冠熹苑

| 樓宇名稱（座號）    | 樓宇類型                               | 落成年份 |
| ----------- | ---------------------------------- | ---- |
| 冠熹苑（A座/第4座） | [和諧一型](../Page/和諧一型.md "wikilink") | 2000 |
|             |                                    |      |

因何文田邨發展計劃被縮小，部分土地已經用作興建豪宅，故部分座號會被略去。

### 冠德苑

| 樓宇名稱（座號） | 樓宇類型                                         | 落成年份 |
| -------- | -------------------------------------------- | ---- |
| 冠昌閣（A座）  | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink")（長型） | 2019 |
| 冠盛閣（B座）  |                                              |      |
| 冠榮閣（C座）  |                                              |      |
|          |                                              |      |

升降機方面，何文田邨景文樓採用[OTIS產品](../Page/奧的斯電梯公司.md "wikilink")，恬文、靜文、雅文及欣文樓連同冠輝苑採用[三菱電機產品](../Page/三菱電機.md "wikilink")，逸文、采文、適文、綺文樓及冠熹苑則採用[富士達升降機](../Page/富士達.md "wikilink")。至於新建的冠德苑，則採用同期居屋中廣泛應用的[東芝公司製升降機](../Page/東芝公司.md "wikilink")。

### 歷代樓宇

| 樓宇名稱 | 樓宇類型                                     | 落成年份      | 拆卸年份 |
| ---- | ---------------------------------------- | --------- | ---- |
| 第1座  | [新型政府廉租屋大廈](../Page/政府廉租屋.md "wikilink") | 1972-1975 | 2001 |
| 第2座  |                                          |           |      |
| 第3座  |                                          |           |      |
| 第4座  |                                          |           |      |
| 第5座  |                                          |           |      |
| 第6座  |                                          |           |      |
| 第7座  |                                          |           |      |
| 第8座  |                                          |           |      |
|      |                                          |           |      |

## 教育

### 幼稚園

  - [何文田浸信會幼稚園](http://www.hmtbck.edu.hk)（2000年創辦）（位於適文樓地下）
  - [何文田循道衛理楊震幼兒學校](http://www.hmtps.org)（2000年創辦）（位於靜文樓地下）
  - [聖文嘉幼稚園](http://www.stmonica.edu.hk)（2000年創辦）（位於冠熹苑地下）

### 小學

  - [陳瑞祺（喇沙）小學](../Page/陳瑞祺（喇沙）小學.md "wikilink")（1973年創辦）

## 康樂設施及冠德苑興建圖像

邨內的康樂設施包括5個兒童遊樂場、2個長者康樂天地、露天舞台、戶外羽毛球場、戶外5人籃球場及戶外3人籃球場。

Ho Man Tin Estate Basketball Court.jpg|籃球場 Ho Man Tin Estate Badminton
Court.jpg|羽毛球場 Ho Man Tin Estate Volleyball Court.jpg|排球場 Ho Man Tin
Estate children's playground.jpg|兒童遊樂場 Ho Man Tin Estate Playground
(2).jpg|兒童遊樂場（2） Ho Man Tin Estate Gym Zone (1) and Playground
(3).jpg|兒童遊樂場（3）及健體區

Kwun Tak Court under foundation works in October
2015.jpg|冠德苑地基工程（2015年10月） Kwun Tak Court under
foundation works in February 2017.jpg|冠德苑地基工程（2017年2月） Kwun Tak Court
under construction in December 2017.jpg|興建中冠德苑（2017年12月） Kwun Tak Court
under construction in July 2018.jpg|興建中冠德苑（2018年7月）

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/何文田站.md" title="wikilink">何文田站</a> (步行20至25分鐘)</li>
</ul>
<dl>
<dt><a href="../Page/何文田邨公共運輸交匯處.md" title="wikilink">何文田邨公共運輸交匯處</a>/<a href="../Page/常富街.md" title="wikilink">常富街</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>何文田邨至旺角線[7]</li>
<li>何文田邨至紅磡線 (平日上午服務)[8]</li>
</ul>
<dl>
<dt><a href="../Page/佛光街.md" title="wikilink">佛光街</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>何文田邨至<a href="../Page/旺角.md" title="wikilink">旺角線</a>[9]</li>
<li><a href="../Page/愛民邨.md" title="wikilink">愛民邨至旺角線</a>[10]</li>
<li>何文田邨至<a href="../Page/紅磡.md" title="wikilink">紅磡線</a> (平日上午服務)[11]</li>
<li>愛民邨至<a href="../Page/土瓜灣.md" title="wikilink">土瓜灣線</a>[12]</li>
<li>紅磡至旺角線[13]</li>
<li><a href="../Page/荃灣.md" title="wikilink">荃灣至何文田線</a> (平日上午及下午服務)[14]</li>
</ul>
<dl>
<dt><a href="../Page/常盛街.md" title="wikilink">常盛街</a></dt>

</dl>
<dl>
<dt><a href="../Page/紅色小巴.md" title="wikilink">紅色小巴</a></dt>

</dl>
<ul>
<li>愛民邨至土瓜灣線[15]</li>
</ul>
<dl>
<dt><a href="../Page/常樂街.md" title="wikilink">常樂街</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 資料來源

## 外部連結

  - [房屋署：何文田邨](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=3189)
  - [房屋署：何文田廣場](http://www.housingauthority.gov.hk/hkhd/hab/cpd/shop_c/chinese/homantin/homantin_menu.html)

[Category:何文田](../Category/何文田.md "wikilink")
[Category:採石山](../Category/採石山.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")
[Category:2000年完工建築物](../Category/2000年完工建築物.md "wikilink")
[Category:1999年完工建築物](../Category/1999年完工建築物.md "wikilink")

1.
2.  [何文田](http://www.homantinsports.com/about/homantin-our-root/)
3.  [冠暉苑](http://www.hk-place.com/vp.php?board=1&id=2068-1)
4.  [居屋陸續有來
    何文田最吸睛](http://cyclub.happyhongkong.com/viewthread.php?tid=176007&page=4#pid4437380)
5.  [何文田新居屋新圖（已安裝樓宇名稱）](http://cyclub.happyhongkong.com/viewthread.php?tid=228307)
6.  [1](http://cyclub.happyhongkong.com/viewthread.php?tid=214631「居屋2019」預計推售項目)
7.  [何文田邨　—　旺角花園街](http://www.16seats.net/chi/rmb/r_k72.html)
8.  [何文田邨　＞　紅磡寶其利街](http://www.16seats.net/chi/rmb/r_k74.html)
9.  [何文田邨　—　旺角花園街](http://www.16seats.net/chi/rmb/r_k72.html)
10. [愛民邨　—　旺角花園街](http://www.16seats.net/chi/rmb/r_k73.html)
11. [何文田邨　＞　紅磡寶其利街](http://www.16seats.net/chi/rmb/r_k74.html)
12. [土瓜灣譚公道　—　愛民邨](http://www.16seats.net/chi/rmb/r_k18.html)
13. [紅磡寶其利街　—　旺角始創中心](http://www.16seats.net/chi/rmb/r_k71.html)
14. [荃灣荃灣街市街　—　何文田](http://www.16seats.net/chi/rmb/r_kn41.html)
15. [土瓜灣譚公道　—　愛民邨](http://www.16seats.net/chi/rmb/r_k18.html)