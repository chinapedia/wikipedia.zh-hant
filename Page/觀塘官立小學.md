**觀塘官立小學**（英語：**Kwun Tong Government Primary
School**），為一所[香港官立小學](../Page/香港.md "wikilink")，有50年歷史，分為上下午校，並於2008年9月轉為全日制小學。

North of Kwun Tong Government Primary School.jpg|校舍北面 Kwun Tong
Government Primary School.JPG|翻油前的校舍

## 辦學宗旨

致力提供多元化的教育服務，讓學生在一個完善的環境中學習、成長；培養兒童有高尚的品格及正確的人生觀，以達到德、智、體、群、美五育並重的理想，使能成為良好的公民；並且率先履行教育統籌局所推行的政策，盡量提供優質學校教育，服務人群。

## 著名/傑出校友

  - [陳嘉嘉](../Page/陳嘉嘉.md "wikilink")：[無綫電視女藝人](../Page/無綫電視.md "wikilink")
  - [羅家英](../Page/羅家英.md "wikilink")：香港著名[粵劇老倌及演員](../Page/粵劇.md "wikilink")（1960年上午校畢業）\[1\]
  - [王虹茵](../Page/王虹茵.md "wikilink")：前香港女子藝術體操運動員

## 參考來源

## 外部連結

  - [觀塘官立小學](http://www.ktgps.edu.hk/)

[K](../Category/香港官立小學.md "wikilink")
[Category:觀塘區小學](../Category/觀塘區小學.md "wikilink")
[Category:牛頭角](../Category/牛頭角.md "wikilink")
[Category:1959年創建的教育機構](../Category/1959年創建的教育機構.md "wikilink")

1.  [荃話題《香江梨園：粵劇文武生羅家英》](http://www.lizawang.com/talk/talk20_hk_chi_opera/learning.shtml)