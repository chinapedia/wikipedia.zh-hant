**埃德娜·费伯**（，），[美国](../Page/美国.md "wikilink")[作家](../Page/作家.md "wikilink")，曾获1925年的[普利策小说奖](../Page/普利策奖.md "wikilink")。

## 生平

## 作品

由菲伯的小说《[演艺船](../Page/演艺船.md "wikilink")》改编的同名音乐剧被称为是第一部真正意义上的美国音乐剧。剧中的歌曲《[老人河](../Page/老人河.md "wikilink")》也广为传唱。

### 長篇小說

  - *Dawn O'Hara, The Girl Who Laughed* (1911)
  - *Fanny Herself* (1917)
  - *The Girls* (1921)
  - *Gigolo* (1922)
  - *So Big* (1924)
  - *Show Boat* (1926)
  - *Cimarron* (1929)
  - *American Beauty* (1931)
  - *Come and Get It* (1935)
  - *Saratoga Trunk* (1941)
  - *Great Son* (1945)
  - *Giant* (1952)
  - *Ice Palace* (1958)

## 外部連結

  -
  -
  -
[Category:美國小說家](../Category/美國小說家.md "wikilink")
[Category:美國作家](../Category/美國作家.md "wikilink")
[Category:普立茲小說獎得主](../Category/普立茲小說獎得主.md "wikilink")