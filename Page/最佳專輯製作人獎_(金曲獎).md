**金曲獎最佳專輯製作人獎**為[金曲獎現行頒發獎項](../Page/金曲獎.md "wikilink")，自1990年第二屆[金曲獎開始設立](../Page/金曲獎.md "wikilink")，現有「流行音樂類」演唱類、「流行音樂類」演奏類以及「傳統暨藝術音樂類」等三項專輯製作人獎，流行音樂演唱類另有自2007年第十八屆起增設的[最佳單曲製作人獎](../Page/最佳單曲製作人獎_\(金曲獎\).md "wikilink")。

  - 1990年第二屆：設立「最佳專輯製作人獎」
  - 1991年第三屆－1996年第七屆：分為「最佳演唱專輯製作人獎」及「最佳演奏專輯製作人獎」兩獎項。
  - 1997年第八屆－1999年第十屆：原兩獎項合併為「流行音樂作品類」最佳唱片製作人獎，並在增設的「傳統暨藝術音樂作品類」（時稱「非流行音樂類」）獎項中，另頒一最佳唱片製作人獎。
  - 2000年第十一屆：兩製作人獎項改稱為「最佳專輯製作人獎」。
  - 2007年第十八屆至今：流行音樂作品類「最佳專輯製作人獎」分演唱及演奏兩類頒發，演唱類另增設[最佳單曲製作人獎](../Page/最佳單曲製作人獎_\(金曲獎\).md "wikilink")。

## 歷屆流行音樂類入圍暨得獎名單

### 流行音樂作品類－演唱類　最佳專輯製作人獎（第十八屆～至今）

<table style="width:24%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 2%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>得獎者</p></th>
<th><p>專輯名稱</p></th>
<th><p>其他入圍者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>29<br />
<small>（<a href="../Page/第29屆金曲獎.md" title="wikilink">2018</a>）</p></td>
<td><p><a href="../Page/李宗盛.md" title="wikilink">李宗盛</a></p></td>
<td><p>《仍是異鄉人》<br />
<small>相信音樂國際股份有限公司</p></td>
<td><p><small> <a href="../Page/戴佩妮.md" title="wikilink">戴佩妮</a>／《不特別得很特別》／種子音樂有限公司<br />
<a href="../Page/陳哲廬.md" title="wikilink">陳哲廬</a>(Jerald Chan)／《C'mon In~》／環球國際唱片股份有限公司<br />
<a href="../Page/林俊傑.md" title="wikilink">林俊傑</a>／《偉大的渺小》／華納國際音樂股份有限公司<br />
</p></td>
</tr>
<tr class="even">
<td><p>28<br />
<small>（<a href="../Page/第28屆金曲獎.md" title="wikilink">2017</a>）</p></td>
<td><p>荒井十一</p></td>
<td><p>《vavayan.女人》<br />
<small>十一音像有限公司 (十一音樂)</p></td>
<td><p><small> 陳主惠／《椏幹》／卡達德邦文化工作室<br />
<a href="../Page/方大同.md" title="wikilink">方大同</a>、Derrick Sepnio／《<a href="../Page/JTW_西遊記.md" title="wikilink">JTW 西遊記</a>》／香港商賦音樂有限公司<br />
杜凱、戈非／《先生小姐》／風花雪月音樂文化事業股份有限公司<br />
<a href="../Page/五月天.md" title="wikilink">五月天</a>／《<a href="../Page/自傳.md" title="wikilink">自傳</a>》／相信音樂國際股份有限公司<br />
<a href="../Page/陳建騏.md" title="wikilink">陳建騏</a>／《<a href="../Page/末路狂花_(魏如萱專輯).md" title="wikilink">末路狂花</a>》 ／<a href="../Page/台灣索尼音樂娛樂股份有限公司.md" title="wikilink">台灣索尼音樂娛樂股份有限公司</a><br />
</p></td>
</tr>
<tr class="odd">
<td><p>27<br />
<small>（<a href="../Page/第27屆金曲獎.md" title="wikilink">2016</a>）</p></td>
<td><p><a href="../Page/林暐哲.md" title="wikilink">林暐哲</a></p></td>
<td><p>《冬 未了》<br />
<small>環球國際唱片股份有限公司</p></td>
<td><p><small> 許哲珮、王希文／《搖擺電力公司》／彎的音樂有限公司<br />
阿弟仔、AMIT／《AMIT2》／百代唱片股份有限公司<br />
安棟、蔡健雅／《失語者》／亞神音樂娛樂股份有限公司<br />
林俊傑／《實驗專輯 和自己對話》／華納國際音樂股份有限公司<br />
</p></td>
</tr>
<tr class="even">
<td><p>26<br />
<small>（<a href="../Page/第26屆金曲獎.md" title="wikilink">2015</a>）</p></td>
<td><p>荒井壯一郎</p></td>
<td><p>《不散，不見》<br />
<small>環球國際唱片股份有限公司</p></td>
<td><p><small> 黃連煜／《山歌一條路》／貳樓音樂工作室<br />
Joanna Wang、Pessi Levanto／《午夜劇院》／台灣索尼音樂娛樂股份有限公司<br />
<a href="../Page/陳建騏.md" title="wikilink">陳建騏</a>／《<a href="../Page/還是要相信愛情啊混蛋們.md" title="wikilink">還是要相信愛情啊混蛋們</a>》／<a href="../Page/添翼創越工作室.md" title="wikilink">添翼創越工作室</a><br />
<a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>／《<a href="../Page/哎呦，不錯哦.md" title="wikilink">哎呦，不錯哦</a>》／杰威爾音樂有限公司<br />
</p></td>
</tr>
<tr class="odd">
<td><p>25<br />
<small>（<a href="../Page/第25屆金曲獎.md" title="wikilink">2014</a>）</p></td>
<td><p><a href="../Page/戴佩妮.md" title="wikilink">戴佩妮</a></p></td>
<td><p>《擁抱你》<br />
<small><a href="../Page/種子音樂有限公司.md" title="wikilink">種子音樂有限公司</a></p></td>
<td><p><small> <a href="../Page/李榮浩.md" title="wikilink">李榮浩</a>／《模特》／紅柿子音樂工作室<br />
<a href="../Page/李健_(歌手).md" title="wikilink">李健</a>／《拾光》／美妙音樂股份有限公司<br />
賈敏恕／《我是海雅谷慕》／<a href="../Page/滾石唱片.md" title="wikilink">滾石國際音樂股份有限公司</a><br />
林暐哲／《<a href="../Page/秋：故事.md" title="wikilink">秋：故事</a>》／<a href="../Page/環球唱片_(台灣).md" title="wikilink">環球國際唱片股份有限公司</a><br />
<a href="../Page/杜振熙.md" title="wikilink">杜振熙</a>（蛋堡）／《你所不知道的杜振熙之內部整修》／顏社企業有限公司<br />
</p></td>
</tr>
<tr class="even">
<td><p>24<br />
<small>（<a href="../Page/第24屆金曲獎.md" title="wikilink">2013</a>）</p></td>
<td><p><a href="../Page/林憶蓮.md" title="wikilink">林憶蓮</a><br />
<a href="../Page/常石磊.md" title="wikilink">常石磊</a></p></td>
<td><p>《<a href="../Page/蓋亞_(專輯).md" title="wikilink">蓋亞</a>》<br />
<small> <a href="../Page/環球唱片_(台灣).md" title="wikilink">環球國際唱片股份有限公司</a></p></td>
<td><p><small> 陳主惠／《dalan》／卡達德邦文化工作室<br />
<a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>／《<a href="../Page/12新作.md" title="wikilink">12新作</a>》／杰威爾音樂有限公司<br />
鍾興民／《永恆。承諾》／果核有限公司<br />
賴家慶、王溪泉／《O-Kai A Cappella》／角頭文化事業股份有限公司</p></td>
</tr>
<tr class="odd">
<td><p>23<br />
<small>（<a href="../Page/第23屆金曲獎.md" title="wikilink">2012</a>）</p></td>
<td><p><a href="../Page/五月天.md" title="wikilink">五月天</a></p></td>
<td><p>《<a href="../Page/第二人生_(專輯).md" title="wikilink">第二人生</a>》（no where-末日版）<br />
<small><a href="../Page/相信音樂.md" title="wikilink">相信音樂國際股份有限公司</a></p></td>
<td><p><small></p>
<p><a href="../Page/許哲珮.md" title="wikilink">許哲珮</a>／《奇幻精品店》／彎的音樂有限公司<br />
<a href="../Page/陳建騏.md" title="wikilink">陳建騏</a>／《<a href="../Page/不允許哭泣的場合.md" title="wikilink">不允許哭泣的場合</a>》／<a href="../Page/亞神音樂.md" title="wikilink">亞神音樂娛樂股份有限公司</a><br />
<a href="../Page/蔡健雅.md" title="wikilink">蔡健雅</a>／《<a href="../Page/說到愛_(蔡健雅專輯).md" title="wikilink">說到愛</a>》／<a href="../Page/亞神音樂.md" title="wikilink">亞神音樂娛樂股份有限公司</a><br />
<a href="../Page/王若琳.md" title="wikilink">Joanna Wang</a>／《博尼的大冒險》／<a href="../Page/台灣索尼音樂娛樂.md" title="wikilink">台灣索尼音樂娛樂股份有限公司</a></p></td>
</tr>
<tr class="even">
<td><p>22<br />
<small>（<a href="../Page/第22屆金曲獎.md" title="wikilink">2011</a>）</p></td>
<td><p><a href="../Page/洪敬堯.md" title="wikilink">洪敬堯</a></p></td>
<td><p>《戀花》<br />
<small><a href="../Page/台灣索尼音樂娛樂.md" title="wikilink">台灣索尼音樂娛樂股份有限公司</a></p></td>
<td><p><small> 林揮斌、<a href="../Page/舒米恩.md" title="wikilink">Suming</a>／《<a href="../Page/Suming_(專輯).md" title="wikilink">Suming舒米恩首張個人創作專輯</a>》／彎的音樂有限公司<br />
<a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>／《<a href="../Page/跨時代.md" title="wikilink">跨時代</a>》／<a href="../Page/杰威爾音樂.md" title="wikilink">杰威爾音樂有限公司</a><br />
<a href="../Page/張亞東.md" title="wikilink">張亞東</a>／《寶貝》／<a href="../Page/環球唱片_(台灣).md" title="wikilink">環球國際唱片股份有限公司</a><br />
<a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a>、鍾興民／《Glory Days美好歲月》／果核有限公司<br />
陳子鴻、<a href="../Page/江蕙.md" title="wikilink">江蕙</a>／《當時欲嫁》／<a href="../Page/阿爾發音樂股份有限公司.md" title="wikilink">阿爾發音樂股份有限公司</a><br />
<a href="../Page/齊秦.md" title="wikilink">齊秦</a>、史大維／《美麗境界》／虹之美夢成真藝人經紀股份有限公司</p></td>
</tr>
<tr class="odd">
<td><p>21<br />
<small>（<a href="../Page/第21屆金曲獎.md" title="wikilink">2010</a>）</p></td>
<td><p><a href="../Page/阿弟仔.md" title="wikilink">阿弟仔</a><br />
<a href="../Page/張惠妹.md" title="wikilink">張惠妹</a></p></td>
<td><p>《<a href="../Page/阿密特.md" title="wikilink">A-MIT 阿密特 張惠妹意識專輯</a>》<br />
<small><a href="../Page/金牌大風_(台灣).md" title="wikilink">金牌大風音樂文化股份有限公司</a></p></td>
<td><p><small> <a href="../Page/蕭煌奇.md" title="wikilink">蕭煌奇</a>／《愛作夢的人》／黑色吉他工作室<br />
<a href="../Page/蔡健雅.md" title="wikilink">蔡健雅</a>／《若你碰到他》／<a href="../Page/亞神音樂.md" title="wikilink">亞神音樂娛樂股份有限公司</a><br />
<a href="../Page/林暐哲.md" title="wikilink">林暐哲</a>／《<a href="../Page/春·日光.md" title="wikilink">春·日光</a>》／<a href="../Page/環球唱片_(台灣).md" title="wikilink">環球國際唱片股份有限公司</a><br />
<a href="../Page/張亞東.md" title="wikilink">張亞東</a>／《回蔚》／<a href="../Page/環球唱片_(台灣).md" title="wikilink">環球國際唱片股份有限公司</a><br />
<a href="../Page/鍾成虎.md" title="wikilink">鍾成虎</a>／《<a href="../Page/太陽_(陳綺貞專輯).md" title="wikilink">太陽</a>》／添翼創越工作室<br />
李昀陵、石家豪、<a href="../Page/曾雅君.md" title="wikilink">曾雅君</a>／《Yachun Asta Tzeng 曾雅君》／威德文化有限公司</p></td>
</tr>
<tr class="even">
<td><p>20<br />
<small>（<a href="../Page/第20屆金曲獎.md" title="wikilink">2009</a>）</p></td>
<td><p><a href="../Page/陳建年_(歌手).md" title="wikilink">陳建年</a></p></td>
<td><p>《南王姐妹花同名專輯》<br />
<small>角頭文化事業股份有限公司</p></td>
<td><p><small> 陳子鴻、<a href="../Page/江蕙.md" title="wikilink">江蕙</a>／《甲你攬牢牢》／<a href="../Page/阿爾發音樂.md" title="wikilink">阿爾發音樂股份有限公司</a><br />
<a href="../Page/陳珊妮.md" title="wikilink">陳珊妮</a>／《如果有一件事是重要的》／大樂音樂企業社<br />
<a href="../Page/黃立行.md" title="wikilink">黃立行</a>、Jae Chong／《最後只好躺下來》／<a href="../Page/華納唱片_(台灣).md" title="wikilink">華納國際音樂股份有限公司</a><br />
<a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>／《<a href="../Page/魔杰座.md" title="wikilink">魔杰座</a>》／<a href="../Page/杰威爾音樂.md" title="wikilink">杰威爾音樂有限公司</a></p></td>
</tr>
<tr class="odd">
<td><p>19<br />
<small>（<a href="../Page/第19屆金曲獎.md" title="wikilink">2008</a>）</p></td>
<td><p><a href="../Page/蔡健雅.md" title="wikilink">蔡健雅</a></p></td>
<td><p>《Goodbye &amp; Hello》<br />
<small><a href="../Page/亞神音樂.md" title="wikilink">亞神音樂娛樂股份有限公司</a></p></td>
<td><p><small> <a href="../Page/黃連煜.md" title="wikilink">黃連煜</a>／《2007 BANANA》／貳樓音樂工作室<br />
<a href="../Page/曹登昌.md" title="wikilink">曹登昌</a>／《依拜維吉》／詮釋音樂文化事業有限公司<br />
Terry Chan／《拉活…》／<a href="../Page/新力博德曼_(台灣).md" title="wikilink">新力博德曼音樂娛樂股份有限公司</a><br />
<a href="../Page/曹格.md" title="wikilink">曹格</a>、凃惠源／《Super Sunshine》／<a href="../Page/滾石唱片.md" title="wikilink">滾石國際音樂股份有限公司</a></p></td>
</tr>
<tr class="even">
<td><p>18<br />
<small>（<a href="../Page/第18屆金曲獎.md" title="wikilink">2007</a>）</p></td>
<td><p><a href="../Page/Tizzy_Bac.md" title="wikilink">陳惠婷</a><br />
<a href="../Page/Tizzy_Bac.md" title="wikilink">許哲毓</a><br />
<a href="../Page/Tizzy_Bac.md" title="wikilink">林前源</a><br />
林揮斌<br />
<a href="../Page/許哲珮.md" title="wikilink">許哲珮</a></p></td>
<td><p>《<a href="../Page/我想你會變成這樣都是我害的.md" title="wikilink">我想你會變成這樣都是我害的</a>》<br />
<small>彎的音樂有限公司</p></td>
<td><p><small> <a href="../Page/林生祥.md" title="wikilink">林生祥</a>、<a href="../Page/鍾永豐.md" title="wikilink">鍾永豐</a>、鍾適芳／《種樹》／大大樹音樂圖像<br />
昊恩／《Blue in love》／角頭文化事業股份有限公司<br />
林暐哲／《<a href="../Page/小宇宙.md" title="wikilink">小宇宙</a>》／林暐哲音樂社<br />
同志／《砰砰樂隊檳榔交出來》／星火製作有限公司</small></p></td>
</tr>
</tbody>
</table>

### 流行音樂作品類　最佳專輯製作人獎（第十一屆～第十七屆）

<table style="width:24%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 2%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>得獎者</p></th>
<th><p>專輯名稱</p></th>
<th><p>其他入圍者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>17<br />
<small>（<a href="../Page/第17屆金曲獎.md" title="wikilink">2006</a>）</p></td>
<td><p><a href="../Page/鍾成虎.md" title="wikilink">鍾成虎</a></p></td>
<td><p>《<a href="../Page/華麗的冒險.md" title="wikilink">華麗的冒險</a>》<br />
<small>Cheerego.com 好小氣音樂工作室</small></p></td>
<td><p><small> 鄭捷任、王繼三／《匆匆》／參拾柒度製作有限公司<br />
<a href="../Page/蔡健雅.md" title="wikilink">蔡健雅</a>／《雙棲動物》／<a href="../Page/華納唱片_(台灣).md" title="wikilink">華納國際音樂股份有限公司</a><br />
<a href="../Page/戴佩妮.md" title="wikilink">戴佩妮</a>、陳子鴻、黃怡／《愛瘋了》／<a href="../Page/科藝百代.md" title="wikilink">科藝百代股份有限公司</a><br />
<a href="../Page/陶喆.md" title="wikilink">陶喆</a>／《<a href="../Page/太平盛世.md" title="wikilink">太平盛世</a>》／伊世代娛樂股份有限公司</small></p></td>
</tr>
<tr class="even">
<td><p>16<br />
<small>（<a href="../Page/第16屆金曲獎.md" title="wikilink">2005</a>）</p></td>
<td><p><a href="../Page/陳珊妮.md" title="wikilink">陳珊妮</a></p></td>
<td><p>《後來　我們都哭了》<br />
<small>荒島網路科技股份有限公司</small></p></td>
<td><p><small> <a href="../Page/羅大佑.md" title="wikilink">羅大佑</a>／《美麗島》／大右音樂事業有限公司<br />
<a href="../Page/林生祥.md" title="wikilink">林生祥</a>、<a href="../Page/鍾永豐.md" title="wikilink">鍾永豐</a>、鍾適芳／《臨暗》／大大樹音樂圖像<br />
丘旺蒼、<a href="../Page/王宏恩.md" title="wikilink">王宏恩</a>／《走風的人》／破鑼嗓子音樂有限公司<br />
<a href="../Page/黃立行.md" title="wikilink">黃立行</a>、Jae Chong／《黑的意念》／香港商維京百代音樂事業股份有限公司台灣分公司</p></td>
</tr>
<tr class="odd">
<td><p>15<br />
<small>（<a href="../Page/第15屆金曲獎.md" title="wikilink">2004</a>）</p></td>
<td><p><a href="../Page/王力宏.md" title="wikilink">王力宏</a></p></td>
<td><p>《<a href="../Page/不可思議_(专辑).md" title="wikilink">不可思議</a>》<br />
<small><a href="../Page/新力音樂_(台灣).md" title="wikilink">新力哥倫比亞音樂股份有限公司</a></small></p></td>
<td><p><small> <a href="../Page/陳建騏.md" title="wikilink">陳建騏</a>／《幾米地下鐵音樂劇原聲帶》／銀翼文化事業工作室<br />
陳子鴻／《風吹的願望》／亞律音樂股份有限公司<br />
<a href="../Page/王菲.md" title="wikilink">王菲</a>、<a href="../Page/張亞東.md" title="wikilink">張亞東</a>／《<a href="../Page/將愛.md" title="wikilink">將愛</a>》／銀魚音樂有限公司<br />
<a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>／《<a href="../Page/葉惠美.md" title="wikilink">葉惠美</a>》／<a href="../Page/阿爾發音樂.md" title="wikilink">阿爾發音樂股份有限公司</a><br />
<a href="../Page/謝宇威.md" title="wikilink">謝宇威</a>／《一儕·花樹下》／威德文化有限公司</small></p></td>
</tr>
<tr class="even">
<td><p>14<br />
<small>（<a href="../Page/第14屆金曲獎.md" title="wikilink">2003</a>）</p></td>
<td><p><a href="../Page/劉劭希.md" title="wikilink">劉劭希</a></p></td>
<td><p>《<a href="../Page/野放客.md" title="wikilink">野放客</a>》<br />
<small>傳響樂坊</small></p></td>
<td><p><small> <a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>／《<a href="../Page/八度空間_(專輯).md" title="wikilink">八度空間</a>》／<a href="../Page/阿爾發音樂.md" title="wikilink">阿爾發音樂股份有限公司</a><br />
<a href="../Page/順子.md" title="wikilink">順子</a>、林哲民／《Dear Shunza》／<a href="../Page/科藝百代.md" title="wikilink">科藝百代股份有限公司</a><br />
董運昌／《第33個街角轉彎》／<a href="../Page/風潮音樂.md" title="wikilink">風潮有聲出版有限公司</a><br />
李振權／《<a href="../Page/Special_Thanks_To....md" title="wikilink">special thanks to...</a>》／<a href="../Page/艾迴.md" title="wikilink">艾迴股份有限公司</a></small></p></td>
</tr>
<tr class="odd">
<td><p>13<br />
<small>（<a href="../Page/第13屆金曲獎.md" title="wikilink">2002</a>）</p></td>
<td><p><a href="../Page/周杰倫.md" title="wikilink">周杰倫</a></p></td>
<td><p>《<a href="../Page/范特西.md" title="wikilink">范特西</a>》<br />
<small><a href="../Page/阿爾發音樂.md" title="wikilink">阿爾發音樂事業有限公司</a></small></p></td>
<td><p><small> <a href="../Page/順子.md" title="wikilink">順子</a>／《And music's there》／<a href="../Page/科藝百代.md" title="wikilink">科藝百代股份有限公司</a><br />
<a href="../Page/林憶蓮.md" title="wikilink">林憶蓮</a>、Jim Lee／《<a href="../Page/原來….md" title="wikilink">原來…</a>》／<a href="../Page/維京唱片_(港台).md" title="wikilink">香港商維京百代音樂事業（股）台灣分公司</a><br />
李壽全／《忘不了2001鄧麗君最後錄音》／普金傳播有限公司<br />
<a href="../Page/訐譙龍.md" title="wikilink">訐譙龍</a>／《訐譙龍網路歌神》／<a href="../Page/豐華唱片.md" title="wikilink">豐華唱片股份有限公司</a> </small></p></td>
</tr>
<tr class="even">
<td><p>12<br />
<small>（<a href="../Page/第12屆金曲獎.md" title="wikilink">2001</a>）</p></td>
<td><p><a href="../Page/李宗盛.md" title="wikilink">李宗盛</a></p></td>
<td><p>《十二樓的莫文蔚》<br />
<small><a href="../Page/滾石唱片.md" title="wikilink">滾石國際音樂股份有限公司</a></small></p></td>
<td><p><small> <a href="../Page/阿弟仔.md" title="wikilink">阿弟仔</a>／《我是人》／勇士物流股份有限公司（常喜音樂股份有限公司製作）<br />
<a href="../Page/李偲菘.md" title="wikilink">李偲菘</a>、<a href="../Page/李偉菘.md" title="wikilink">李偉菘</a>／《心酸的浪漫》／<a href="../Page/科藝百代.md" title="wikilink">科藝百代股份有限公司</a><br />
<a href="../Page/張亞東.md" title="wikilink">張亞東</a>、<a href="../Page/王菲.md" title="wikilink">王菲</a>、Alvin Leong／《<a href="../Page/寓言_(專輯).md" title="wikilink">寓言</a>》／<a href="../Page/科藝百代.md" title="wikilink">科藝百代股份有限公司</a><br />
<a href="../Page/周杰倫.md" title="wikilink">周杰倫</a>／《<a href="../Page/杰倫_(專輯).md" title="wikilink">-{杰倫}-</a>》／<a href="../Page/博德曼音樂_(台灣).md" title="wikilink">博德曼股份有限公司</a> </small></p></td>
</tr>
<tr class="odd">
<td><p>11<br />
<small>（<a href="../Page/第11屆金曲獎.md" title="wikilink">2000</a>）</p></td>
<td><p><a href="../Page/陶喆.md" title="wikilink">陶喆</a></p></td>
<td><p>《<a href="../Page/I&#39;m_OK.md" title="wikilink">I'm ok</a>》<br />
<small>俠客唱片股份有限公司</small></p></td>
<td><p><small> <a href="../Page/李欣芸.md" title="wikilink">李欣芸</a>／《何欣穗─完美小姐》／喜樂音音樂股份有限公司<br />
黃怡、<a href="../Page/黃國倫.md" title="wikilink">黃國倫</a>、<a href="../Page/范曉萱.md" title="wikilink">范曉萱</a>／《<a href="../Page/我要我們在一起.md" title="wikilink">我要我們在一起</a>》／<a href="../Page/福茂唱片.md" title="wikilink">福茂唱片音樂股份有限公司</a><br />
鄭捷任／《太陽 風 草原的聲音》／<a href="../Page/魔岩唱片.md" title="wikilink">魔岩唱片股份有限公司</a><br />
林暐哲／《silence》／<a href="../Page/魔岩唱片.md" title="wikilink">魔岩唱片股份有限公司</a></small></p></td>
</tr>
</tbody>
</table>

### 流行音樂作品類　最佳唱片製作人獎（第八屆～第十屆）

<table style="width:24%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 2%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>得獎者</p></th>
<th><p>專輯名稱</p></th>
<th><p>其他入圍者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>10<br />
<small>（<a href="../Page/第10屆金曲獎.md" title="wikilink">1999</a>）</p></td>
<td><p>李振權<br />
<a href="../Page/王力宏.md" title="wikilink">王力宏</a></p></td>
<td><p>《<a href="../Page/公轉自轉.md" title="wikilink">公轉自轉</a>》<br />
<small><a href="../Page/新力音樂.md" title="wikilink">新力哥倫比亞音樂股份有限公司</a></small></p></td>
<td><p><small> <a href="../Page/黃韻玲.md" title="wikilink">黃韻玲</a>、黃建昌／《肉餅飯糰》／<a href="../Page/新力音樂_(台灣).md" title="wikilink">新力哥倫比亞音樂股份有限公司</a><br />
<a href="../Page/王菲.md" title="wikilink">王菲</a>、梁榮駿／《<a href="../Page/唱遊.md" title="wikilink">唱遊</a>》／<a href="../Page/科藝百代.md" title="wikilink">科藝百代股份有限公司</a><br />
賈敏恕／《I AM NOT A STAR》／<a href="../Page/魔岩唱片.md" title="wikilink">魔岩唱片股份有限公司</a><br />
李士先／《SHINO林曉培》／友善的狗有限公司</small></p></td>
</tr>
<tr class="even">
<td><p>9<br />
<small>（<a href="../Page/第9屆金曲獎.md" title="wikilink">1998</a>）</p></td>
<td><p><a href="../Page/陶喆.md" title="wikilink">陶喆</a></p></td>
<td><p>《<a href="../Page/陶喆_(專輯).md" title="wikilink">陶喆</a>》<br />
<small><a href="../Page/金點唱片.md" title="wikilink">金點唱片國際股份有限公司</a></small></p></td>
<td><p><small> <a href="../Page/陳明章.md" title="wikilink">陳明章</a>／《流浪到淡水》／<a href="../Page/魔岩唱片.md" title="wikilink">魔岩唱片股份有限公司</a><br />
<a href="../Page/齊豫.md" title="wikilink">齊豫</a>／《駱駝、飛鳥、魚》／<a href="../Page/滾石唱片.md" title="wikilink">滾石國際音樂股份有限公司</a><br />
<a href="../Page/蔡振南.md" title="wikilink">蔡振南</a>／《可愛可恨》／<a href="../Page/飛碟唱片.md" title="wikilink">飛碟企業股份有限公司</a><br />
<a href="../Page/蔡振南.md" title="wikilink">蔡振南</a>／《花若離枝》／<a href="../Page/豐華唱片.md" title="wikilink">豐華唱片股份有限公司</a></small></p></td>
</tr>
<tr class="odd">
<td><p>8<br />
<small>（<a href="../Page/第8屆金曲獎.md" title="wikilink">1997</a>）</p></td>
<td><p><a href="../Page/蔡振南.md" title="wikilink">蔡振南</a></p></td>
<td><p>《南歌》<br />
<small><a href="../Page/飛碟唱片.md" title="wikilink">飛碟企業股份有限公司</a></small></p></td>
<td><p><small> 徐德昌／《純情歌》／<a href="../Page/上華唱片.md" title="wikilink">上華國際股份有限公司</a><br />
<a href="../Page/黃國倫.md" title="wikilink">黃國倫</a>／《天使》／<a href="../Page/科藝百代.md" title="wikilink">科藝百代股份有限公司</a><br />
陳進興、劉亞文／《天頂的月娘啊》／<a href="../Page/滾石唱片.md" title="wikilink">台灣滾石唱片股份有限公司</a><br />
<a href="../Page/羅大佑.md" title="wikilink">羅大佑</a>／《寶島鹹酸甜》／<a href="../Page/科藝百代.md" title="wikilink">科藝百代股份有限公司</a></small></p></td>
</tr>
</tbody>
</table>

### 最佳演唱專輯製作人獎（第三屆～第七屆）

<table style="width:24%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 2%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>得獎者</p></th>
<th><p>專輯名稱</p></th>
<th><p>其他入圍者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>7<br />
<small>（<a href="../Page/第7屆金曲獎.md" title="wikilink">1996</a>）</p></td>
<td><p><a href="../Page/洪榮宏.md" title="wikilink">洪榮宏</a><br />
<a href="../Page/洪敬堯.md" title="wikilink">洪敬堯</a></p></td>
<td><p>《愛的一生》<br />
<small>東達唱片股份有限公司</small></p></td>
<td><p><small> <a href="../Page/周子寒.md" title="wikilink">周子寒</a>／《圈兒詞》／藍與白唱片有限公司<br />
楊明煌／《<a href="../Page/天空_(專輯).md" title="wikilink">天空</a>》／<a href="../Page/福茂唱片.md" title="wikilink">福茂唱片音樂股份有限公司</a><br />
<a href="../Page/周華健.md" title="wikilink">周華健</a>、蔡朝華／《天下有情人》／<a href="../Page/滾石唱片.md" title="wikilink">台灣滾石唱片股份有限公司</a><br />
李安修、陳耀川／《真永遠》／<a href="../Page/藝能動音.md" title="wikilink">臺灣藝能動音有限公司</a></small></p></td>
</tr>
<tr class="even">
<td><p>6<br />
<small>（<a href="../Page/第6屆金曲獎.md" title="wikilink">1994</a>）</p></td>
<td><p>蔡宗政</p></td>
<td><p>《<a href="../Page/用心良苦.md" title="wikilink">用心良苦</a>》<br />
<small><a href="../Page/歌林公司.md" title="wikilink">歌林股份有限公司</a></small></p></td>
<td><p><small> <a href="../Page/伍思凱.md" title="wikilink">伍思凱</a>／《最愛是你》／點將股份有限公司<br />
<a href="../Page/李宗盛.md" title="wikilink">李宗盛</a>／《不捨》／<a href="../Page/滾石唱片.md" title="wikilink">滾石國際股份有限公司</a><br />
吳大衛／《擁抱》／<a href="../Page/華納唱片_(台灣).md" title="wikilink">飛碟企業股份有限公司</a><br />
曹俊鴻／《左右》／點將股份有限公司</small></p></td>
</tr>
<tr class="odd">
<td><p>5<br />
<small>（<a href="../Page/第5屆金曲獎.md" title="wikilink">1993</a>）</p></td>
<td><p>蔡宗政</p></td>
<td><p>《凡人之大夥兒聽我唱支歌專輯》<br />
<small><a href="../Page/歌林公司.md" title="wikilink">歌林股份有限公司</a></small></p></td>
<td><p><small> <a href="../Page/王治平.md" title="wikilink">王治平</a>／《你不要那樣看著我的眼睛》／<a href="../Page/波麗佳音.md" title="wikilink">波麗佳音股份有限公司</a><br />
<a href="../Page/李宗盛.md" title="wikilink">李宗盛</a>／《心事》／<a href="../Page/巨石音樂.md" title="wikilink">巨石音樂有限公司</a><br />
<a href="../Page/陳昇.md" title="wikilink">陳昇</a>／《別讓我哭》／<a href="../Page/滾石唱片.md" title="wikilink">滾石國際有限公司</a><br />
黃慶元／《<a href="../Page/吻別.md" title="wikilink">吻別</a>》／<a href="../Page/寶麗金.md" title="wikilink">寶麗金唱片股份有限公司</a> </small></p></td>
</tr>
<tr class="even">
<td><p>4<br />
<small>（<a href="../Page/第4屆金曲獎.md" title="wikilink">1992</a>）</p></td>
<td><p>蔡宗政</p></td>
<td><p>《凡人二重唱國語專輯（二）》<br />
<small><a href="../Page/歌林公司.md" title="wikilink">歌林股份有限公司</a></small></p></td>
<td><p><small> <a href="../Page/周華健.md" title="wikilink">周華健</a>、劉志宏／《<a href="../Page/讓我歡喜讓我憂.md" title="wikilink">讓我歡喜讓我憂</a>》／<a href="../Page/滾石唱片.md" title="wikilink">滾石有聲出版社有限公司</a><br />
陳灝／《細說往事》／瑞星唱片有限公司<br />
<a href="../Page/庾澄慶.md" title="wikilink">庾澄慶</a>／《快樂頌》／<a href="../Page/福茂唱片.md" title="wikilink">福茂唱片出版社</a></small></p></td>
</tr>
<tr class="odd">
<td><p>3<br />
<small>（<a href="../Page/第3屆金曲獎.md" title="wikilink">1991</a>）</p></td>
<td><p><a href="../Page/陳昇.md" title="wikilink">陳昇</a><br />
<a href="../Page/李宗盛.md" title="wikilink">李宗盛</a><br />
周世暉<br />
羅紘武</p></td>
<td><p>《<a href="../Page/向前走.md" title="wikilink">向前走</a>》<br />
<small><a href="../Page/滾石唱片.md" title="wikilink">滾石有聲出版社有限公司</a></small></p></td>
<td><p><small> <a href="../Page/伍思凱.md" title="wikilink">伍思凱</a>／《特別的愛給特別的你》／可登有聲出版社有限公司<br />
<a href="../Page/梁弘志.md" title="wikilink">梁弘志</a>／《永遠的情人》／派森企業有限公司<br />
 <a href="../Page/陳揚_(作曲家).md" title="wikilink">陳揚</a>／《盆地邊緣》／點將股份有限公司<br />
<a href="../Page/薛岳_(歌手).md" title="wikilink">薛岳</a>／《<a href="../Page/生老病死.md" title="wikilink">生老病死</a>》／新笛唱片有限公司</small></p></td>
</tr>
</tbody>
</table>

### 最佳專輯製作人獎（第二屆）

<table style="width:24%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 8%" />
<col style="width: 2%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th><p>屆次</p></th>
<th><p>得獎者</p></th>
<th><p>專輯名稱</p></th>
<th><p>其他入圍者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2<br />
<small>（<a href="../Page/第2屆金曲獎.md" title="wikilink">1990</a>）</p></td>
<td><p><a href="../Page/陳揚_(作曲家).md" title="wikilink">陳揚</a></p></td>
<td><p>《一個人遊遊盪盪》<br />
<small>點將股份有限公司</small></p></td>
<td><p><small> <a href="../Page/李宗盛.md" title="wikilink">李宗盛</a>／《跟你說聽你說專輯》／<a href="../Page/滾石唱片.md" title="wikilink">滾石有聲出版社有限公司</a><br />
孫建平／《不只是朋友》／可登有聲出版社有限公司<br />
<a href="../Page/陳煥昌.md" title="wikilink">小蟲</a>／《我是不是你最疼愛的人專輯》／<a href="../Page/滾石唱片.md" title="wikilink">滾石有聲出版社有限公司</a><br />
楊明煌／《生命是一首澎湃的歌》／上揚有聲出版有限公司</small></p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [歷屆金曲獎得獎入圍名單](http://www.bamid.gov.tw/files/11-1000-138-1.php)，文化部影視及流行音樂產業局

[Category:台灣相關列表](../Category/台灣相關列表.md "wikilink")
[Category:金曲獎](../Category/金曲獎.md "wikilink")