**Nexus文件**是一种[生物信息学中常用的](../Page/生物信息学.md "wikilink")[文件格式](../Page/文件格式.md "wikilink")。很多[软件使用或者识别这种格式](../Page/软件.md "wikilink")，例如[PAUP\*和MacClade](../Page/PAUP*.md "wikilink")。

## 语法

注解：使用“\[\]”。位于“\[”和“\]”之间的文字将被软件忽略，作为注解用。

文件采用“块”的方式来组织信息。每一个块的语法结构为：

`BEGIN 块的名称；`
`END;`

## 基本块结构

### DATA

例如：
`begin data;`
`dimensions ntax=5 nchar=600;`
`format interleave datatype=DNA missing=N gap=-;`
`end;`

ntax表示[分类单位的数量](../Page/分类单位.md "wikilink")，nchar表示数据的长度，上面这个例子里的含义是DNA有600个[碱基长度](../Page/碱基.md "wikilink")。数据类型，datatype是[DNA](../Page/DNA.md "wikilink")。另外可以定义缺失信息和缺口用什么字符表示。

### PAUP或软件自定义块

有些软件可以识别特定的块，供自己特殊使用。例如PAUP
block可以用来控制[PAUP\*](../Page/PAUP*.md "wikilink")。[MrBayes也可以有自己的块](../Page/MrBayes.md "wikilink")。常用这些软件自定义块来进行[批处理](../Page/批处理.md "wikilink")。

### 树

Nexus文件里面可以包含[种系发生树信息](../Page/种系发生树.md "wikilink")。例如：
`tree PAUP_1 =
[&U]（((1:0,2:0.001674,3:0.001670):0.001658,4:0.003371):0.012438,(((5:0.003348,6:0.005043):0.001668,7:0.005031):0.003351,8:0):0.009545,9:0.564416);`
`End;`
阅读NEXUS格式的系统进化树文件，可以使用[TreeView软件](../Page/TreeView.md "wikilink")。也可以使用TreeGraph\[1\]软件将其转换成[SVG格式](../Page/SVG.md "wikilink")。

## 外部链接

  - [描述NEXUS文件格式的论文（英文）](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&list_uids=11975335&dopt=Citation)

## 注释

<references />

[Category:生物学](../Category/生物学.md "wikilink")
[Category:生物信息學](../Category/生物信息學.md "wikilink")

1.