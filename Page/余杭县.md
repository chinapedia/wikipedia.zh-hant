**余杭县**，[中国旧](../Page/中国.md "wikilink")[县名](../Page/县.md "wikilink")，始建制于[唐朝](../Page/唐朝.md "wikilink")，为今天[浙江省](../Page/浙江省.md "wikilink")[杭州市](../Page/杭州市.md "wikilink")[余杭区的主体](../Page/余杭区.md "wikilink")。

## 历史

“杭”为地名，“餘”一说为古越语“盐”之意，一说为古越语发语词，无实际含义，（余杭为“禹航”是现代传说，虽美好但并不属实）。余杭之名，[春秋时已见诸史籍](../Page/春秋.md "wikilink")，彼时属[吴](../Page/吳國.md "wikilink")、[越领地](../Page/越.md "wikilink")，[战国中期属](../Page/战国_\(中国\).md "wikilink")[楚](../Page/楚.md "wikilink")。[秦王政二十五年](../Page/秦.md "wikilink")(公元前222年)，秦灭楚，在今境内置钱唐(含杭州城区)、余杭两县，属[会稽郡](../Page/会稽郡.md "wikilink")(钱唐几经变更，到民国为杭县，今余杭区系由原余杭县和杭县大部地域合并而成)。西汉时，余杭、钱唐两县仍属会稽郡，钱唐为会稽郡西部都尉治。平帝元始四年(4年)，改钱唐县为泉亭县。新[王莽始建国元年](../Page/王莽.md "wikilink")(9年)，改余杭县为进睦县。[东汉建武元年](../Page/东汉.md "wikilink")(25年)，复名余杭、钱唐。[三国时](../Page/三国.md "wikilink")，余杭、钱唐均入[东吴版图](../Page/东吴.md "wikilink")，属[吴郡](../Page/吴郡.md "wikilink")，隶[扬州](../Page/扬州_\(古代\).md "wikilink")，钱唐县并为吴郡都尉治。

[隋开皇九年](../Page/隋.md "wikilink")(589年)，废钱唐郡，改置杭州，州治初设余杭，次年移钱唐。大业三年(607年)，又改杭州为余杭郡，钱唐、余杭仍为所属。[唐朝武德四年](../Page/唐朝.md "wikilink")（621年），复余杭郡为杭州，为避国号讳，改钱唐为钱塘。五代后梁龙德二年(922年)，划钱塘、盐官两县地各半及富春县之两乡地置钱江县，与钱塘县[同城而治](../Page/两县同城而治.md "wikilink")。[北宋太平兴国四年](../Page/北宋.md "wikilink")(979年)，改钱江县为仁和县。[南宋建炎三年](../Page/南宋.md "wikilink")(1129年)，升杭州为[临安府](../Page/临安府.md "wikilink")，属两浙西路。钱塘、仁和升赤，余杭升畿。[元朝至元十五年](../Page/元朝.md "wikilink")(1278年)，改临安府为杭州路，钱塘、仁和、余杭属之。至正二十六年(1366年)，朱元璋攻下杭州，改杭州路为杭州府。明清延之。

[中华民国元年](../Page/中华民国.md "wikilink")(1912年)2月，废杭州府，以原钱塘、仁和县地并置杭县，与余杭县同属钱塘道。1949年4月，[解放军发动](../Page/中国人民解放军.md "wikilink")[渡江战役](../Page/渡江战役.md "wikilink")。随后，逐步攻占[浙江省](../Page/浙江省_\(中華民國\).md "wikilink")。5月2日，[解放军第21军军部抵达余杭县城](../Page/中国人民解放军第21军.md "wikilink")，次日，攻占杭州\[1\]。其后余杭、杭县建制未变。中华人民共和国建立后，历属[临安专区](../Page/临安专区.md "wikilink")、[嘉興專區](../Page/嘉興專區.md "wikilink")、[建德专区](../Page/建德专区.md "wikilink")。1958年4月，杭县被撤销，属[杭州市郊区](../Page/郊区_\(杭州市\).md "wikilink")。1960年1月建立[钱塘联社](../Page/钱塘联社.md "wikilink")(县级)。余杭县亦于1958年10月撤销，并入[临安县](../Page/临安县.md "wikilink")。1961年3月，从临安县析出原余杭县境与钱塘联社合并，4月，钱塘联社撤销，恢复县建制，定名余杭县，县治设临平镇。1994年4月，撤销余杭县，设立余杭市。2001年3月，撤销余杭市，设立杭州市余杭区。

2011年余杭区9个镇乡建制改为余杭区政府直辖，改为街道办事处\[2\]。

## 参考文献

{{-}}

[Category:唐朝县份](../Category/唐朝县份.md "wikilink")
[Category:五代十国县份](../Category/五代十国县份.md "wikilink")
[Category:宋朝县份](../Category/宋朝县份.md "wikilink")
[Category:元朝县份](../Category/元朝县份.md "wikilink")
[Category:明朝县份](../Category/明朝县份.md "wikilink")
[Category:清朝县份](../Category/清朝县份.md "wikilink")
[Category:中华民国浙江省县份](../Category/中华民国浙江省县份.md "wikilink")
[Category:已撤消的中华人民共和国浙江省县份](../Category/已撤消的中华人民共和国浙江省县份.md "wikilink")
[Category:杭州行政区划史](../Category/杭州行政区划史.md "wikilink")
[Category:1958年废除的行政区划](../Category/1958年废除的行政区划.md "wikilink")

1.
2.  [杭州市人民政府
    关于余杭区部分行政区划调整的批复](http://www.hangzhou.gov.cn/main/wjgg/zxwj/zxwj/T357882.shtml)