**天涯海角**（普通話拼音：Tiānyá Hǎijiǎo；英文意譯：Edges of the heaven, corners of the
sea）是[中華人民共和國](../Page/中華人民共和國.md "wikilink")[海南省](../Page/海南省.md "wikilink")[三亚市的一处著名海滨风景旅游区](../Page/三亚市.md "wikilink")，相傳一對[情侶只要一起走到天涯海角就可一生一世擁有對方](../Page/情侶.md "wikilink")，位于三亚市区西面24千米处。

## 历史

[日月石位于风景区入口附近的海面上](../Page/日月石.md "wikilink")，为两块尖形岩石交叉的形状，其上分别刻有“日”、“月”二字。因两块岩石相互依偎，有传说到过此处情侣会日月相辉、白头到老，很多情侣都为此慕名而来。

天涯海角石指的是[日月石往西数公里的一组岩石中的两块](../Page/日月石.md "wikilink")。岩石正面（向东）是[雍正十一年](../Page/雍正.md "wikilink")[崖州](../Page/崖州.md "wikilink")[太守](../Page/太守.md "wikilink")[程哲题的](../Page/程哲.md "wikilink")“天涯”，反面（向西）是[清末文人题的](../Page/清.md "wikilink")“海角”。

天涯海角石东侧有[南天一柱石](../Page/南天一柱.md "wikilink")，上面的字系宣统元年永安[范云梯题](../Page/范云梯.md "wikilink")。\[1\]

整个天涯海角风景区内怪石嶙峋，且其上有许多文人墨客的遗迹，多是历代被流放到此的官员触景生情所作。整个风景区海滩绵延数公里，沙滩上多有冲上来的[贝壳类生物](../Page/贝壳.md "wikilink")。此处是海南岛的最南端，由此向南便是一望无垠的[南海](../Page/南海_\(海域\).md "wikilink")。

## 景观

<File:Tianya.jpg>|[天涯](../Page/天涯.md "wikilink")
<File:海角.JPG>|[海角](../Page/海角.md "wikilink")
\[<File:Riyue.jpg>|[日月石](../Page/日月石.md "wikilink")
[File:Tianyahaijiao.jpg|文人墨客的遗迹](File:Tianyahaijiao.jpg%7C文人墨客的遗迹)
<File:Nantianyizhu.jpg>|[南天一柱](../Page/南天一柱.md "wikilink")

<File:Landscape> near the Tian ya hai jiao.jpg|远望天涯海角

## 参考文献

## 外部連結

  - [天涯海角官网](http://www.aitianya.cn/)
  - [海南省政府旅游網](https://web.archive.org/web/20070630083843/http://www.hi.gov.cn/V3/exchange/lyz1.php)

  - [天涯海角旅游景點介紹](http://www.51766.com/img/tyhj/)
  - [天涯海角旅游景點介紹2](https://web.archive.org/web/20081225163423/http://d.lotour.com/sight/tianyahaijiao/)

  - [海南省政府旅游資訊](http://www.hainan.gov.cn/V3/tour/more.php?BigClass=12&SmallClass=15)

  - [海南特色旅游資訊](http://www.hainan.gov.cn/V3/tour/jd.list.php)
  - [中國海南島三亞旅游資訊](https://www.sunnyhainan.com/)

[Category:三亚潜水地点](../Category/三亚潜水地点.md "wikilink")
[Category:三亚地理](../Category/三亚地理.md "wikilink")
[三亚](../Category/海南旅游.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")
[Category:三亚旅游](../Category/三亚旅游.md "wikilink")
[Category:中国海角](../Category/中国海角.md "wikilink")

1.  [陆荣廷与范云梯的宦海之缘](http://www.renwu.com.cn/UserFiles/magazine/article/RW0124_199907231621001334.asp)