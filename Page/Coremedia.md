**CoreMedia
AG**成立于1996年，是一家提供[数字版权管理和](../Page/数字版权管理.md "wikilink")的国际化本地[软件公司](../Page/软件.md "wikilink")。

## 公司

CoreMedia
AG坐落于[德国](../Page/德国.md "wikilink")[汉堡](../Page/汉堡.md "wikilink")，有近130位员工；并在[伦敦](../Page/伦敦.md "wikilink")、[纽约](../Page/纽约.md "wikilink")、[奥斯陆和](../Page/奥斯陆.md "wikilink")[新加坡设有分公司](../Page/新加坡.md "wikilink")。CoreMedia
AG于1996年由Sören Stamer（现公司董事主席）、Jochachim W Schmidt教授（现监事会主席）、Florian
Matthes教授和 Andreas Gawecki以 Higher
Order为名，作为[汉堡大学的分立机构而建立](../Page/汉堡大学.md "wikilink")。这家[股份公司没有上市](../Page/股份公司.md "wikilink")。
公司2004年到2005年的销售额达到近一千五百万[欧元](../Page/欧元.md "wikilink")。主要[投资者为Equitrust](../Page/投资.md "wikilink")、Setubal（M.M.Warburg\&CO）、tbg和
T-Venture。

## 产品

CoreMedia
AG股份公司首先开始从事[内容管理系统](../Page/内容管理系统.md "wikilink")。近几年来，公司也開始发展[数字版权管理業務](../Page/数字版权管理.md "wikilink")。CoreMediaAG公司的内容管理系统在[欧洲处于领先地位](../Page/欧洲.md "wikilink")，在[移动数字管理方面已成为全球先锋](../Page/移动数字管理.md "wikilink")[企业](../Page/企业.md "wikilink")。

### CoreMedia CMS

CoreMedia
CMS的产品是企业内容管理系统，此系统主要运用在高频率的線上登錄。软件是利用[Java](../Page/Java.md "wikilink")[计算机语言开发出来的](../Page/计算机语言.md "wikilink")。根據开发商的介绍，此软件可以在技术上（不同的[服务器](../Page/服务器.md "wikilink")）和内容上（各种语言或不同内容范围）彈性調整，也符合多通路整合（Multi-Channel-Delivery）。服务周到、软件的经济性是公司最好的宣传[广告](../Page/广告.md "wikilink")。公司主要的客户为Bertelsmann（贝塔斯曼），bild.de，Continental，Deutsche
Telekom（[德国电讯](../Page/德国电讯.md "wikilink")），包括其[网站](../Page/网站.md "wikilink")\[[http://www.t-online.de\]T-Online，dpa和德国的o2公司。除此之外](http://www.t-online.de%5DT-Online，dpa和德国的o2公司。除此之外)，[德国联邦政府也将CoreMedia](../Page/德国联邦政府.md "wikilink")
CMS作为政府的标准内容管理系统，它同时也是建立联邦和各州政府的政府网站的广泛应用的主要组件。

### CoreMedia DRM

CoreMedia
DRM包括一系列的完整组件，这些组件对受保护的[数码内容传送的整个运行起着非常重要的作用](../Page/数码.md "wikilink")。CoreMedia
DRM是以工业标准OMA DRM 1.0，2.0和Windows
DRM为基准。除了DRM-平台，CoreMedia还提供单一的组件，例如：CoreMedia
DRM
Packaging服务器（[版权](../Page/版权.md "wikilink")-独立数据设计及其[密码](../Page/密码.md "wikilink")），CoreMedia
DRM ROAP服务器（设计，包括超级分配特性）和CoreMedia DRM
Client（将各种最终技术设备数据分配到[手机和](../Page/手机.md "wikilink")[计算机上](../Page/计算机.md "wikilink")）。CoreMedia
AG
的DRM業務主要客户为Musicload、[Nokia](../Page/Nokia.md "wikilink")、Turkcell、VIVO和Vodafone。

## 外部链接

  - [官方网站](http://www.coremedia.com)

[Category:德国企业](../Category/德国企业.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")