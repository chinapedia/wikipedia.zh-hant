[Japanese_troops_mopping_up_in_Kuala_Lumpur.jpg](https://zh.wikipedia.org/wiki/File:Japanese_troops_mopping_up_in_Kuala_Lumpur.jpg "fig:Japanese_troops_mopping_up_in_Kuala_Lumpur.jpg")\]\]
**馬來亞戰役**是於1941年12月8日至1942年1月31日期間，[同盟國與](../Page/同盟國.md "wikilink")[軸心國成員](../Page/軸心國.md "wikilink")[大日本帝國及](../Page/大日本帝國.md "wikilink")[泰國在](../Page/泰國.md "wikilink")[英屬馬來亞上的戰事](../Page/馬來亞半島.md "wikilink")。以日本的快速勝利結束。

在這場戰役因為日本的士兵擁有曾在中國作戰的豐富戰鬥經驗、加上對[叢林戰有相當完善的準備以及機動作戰](../Page/叢林.md "wikilink")，比較起來[英聯邦軍隊的士兵訓練不足](../Page/英聯邦.md "wikilink")，飞机和坦克数量上处于劣势且指揮官指揮不力。但是双方裝備並沒有明顯差異，且英軍兵力遠大於日軍。戰役中[山下奉文以難以置信的速度攻破英軍防線](../Page/山下奉文.md "wikilink")，使得英軍只能一退再退最終撤退到新加坡，戰役最後以日軍的勝利告終，以少許代價佔領了整個[馬來半島以及](../Page/馬來半島.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")，遠比[大本營預計佔領時間還要短上許多](../Page/大本營_\(大日本帝國\).md "wikilink")，而[英國首相](../Page/英國首相.md "wikilink")[溫斯頓·邱吉爾則稱新加坡的陷落為](../Page/溫斯頓·邱吉爾.md "wikilink")「[英國史上最大的災難](../Page/英國.md "wikilink")」，而日本也稱此次戰役為日本版的[閃擊戰](../Page/閃擊戰.md "wikilink")。

## 背景

兩次大戰其間，[大英帝國在](../Page/大英帝國.md "wikilink")[遠東地區的軍事策略](../Page/遠東.md "wikilink")，因不被重視及缺乏資金支持而日漸敗壞。英國政府原定的計劃是在[新加坡](../Page/新加坡.md "wikilink")[海軍基地](../Page/海軍基地.md "wikilink")，停泊強大艦隊。萬一任何戰爭爆發時，可保衛英國的遠東領土以及通往[澳洲的航線](../Page/澳洲.md "wikilink")。但事實上，當[馬來半島及](../Page/馬來半島.md "wikilink")[新加坡受到威脅時](../Page/新加坡.md "wikilink")，原定可以及時出現的[皇家海軍艦隊卻一直沒有出現](../Page/皇家海軍.md "wikilink")。直至1939年戰爭在歐洲爆發，仍然沒有跡象顯示艦隊會前來部署。

第二次世界大戰開始，[中東及](../Page/中東.md "wikilink")[蘇聯地區更受英國重視](../Page/蘇聯.md "wikilink")，在人力和物資上都獲優先分配。馬來人所希望擁有300至500架戰機實力的[空軍部隊](../Page/空軍.md "wikilink")，但未曾實現。而駐守在[馬來半島上的英軍](../Page/馬來半島.md "wikilink")，在面對配備超過200台[坦克的入侵日軍時](../Page/坦克.md "wikilink")，卻發現守軍連一台[坦克也沒有](../Page/坦克.md "wikilink")，而遭到日軍痛擊並潰敗。

在[法國被德國擊敗後](../Page/法國戰役.md "wikilink")，英國不再能依賴法國海軍阻止義大利海軍從西地中海進入大西洋，而抽調英國地中海艦隊增援馬來亞。英國對馬來亞的防禦戰略要成功，取決於兩個基本假設：第一，英軍對於日軍攻擊馬來亞有足夠的早期預警，而能即時增援。其次，在戰役爆發後美國能協助英國。到1941年底這兩個假設已經不能成立。\[1\]

英軍曾制定計劃先發制人地入侵[暹邏](../Page/暹邏.md "wikilink")（今[泰國](../Page/泰國.md "wikilink")）南部，搶先破壞日軍的著陸點，並命名為[鬥牛士行動](../Page/鬥牛士行動.md "wikilink")，但此行動最終並沒有被執行。

战役开始时，英军有88,600名官兵，辖31个步兵营、7个野战炮兵团和1个山炮团（176门山野榴炮）、2个反坦克炮团（84门反坦克炮）。后来还得到了7个步兵营、1个反坦克炮团和其他一些支援，总兵力达到13万以上。空军力量有158架作战飞机，都是“[F2A“水牛”戰鬥機](../Page/F2A戰鬥機.md "wikilink")”、“[布伦海姆](../Page/布伦海姆轰炸机.md "wikilink")”以及双翼轰炸机。

日军第15军有92,403名官兵，编为27个大队（步兵营），3个师属野炮兵联队、2个野战重炮兵联队和1个山炮兵联队（约180门山野榴炮）。战役开始时15军未到齐，例如18师团开始只有佗美支队。空中力量有354架陆军作战飞机和180架海军作战飞机。

## 日本進軍

日本的目标是要攻占整個[東南亞](../Page/東南亞.md "wikilink")，取得當地的天然資源，用以應付自身的戰爭需要，而[馬來半島是塊富產資源的](../Page/馬來半島.md "wikilink")[土地](../Page/土地.md "wikilink")（盛產[錫](../Page/錫.md "wikilink")、[橡膠](../Page/橡膠.md "wikilink")）\[2\]。
日軍負責佔領[馬來半島為陸軍中將](../Page/馬來半島.md "wikilink")[山下奉文](../Page/山下奉文.md "wikilink")，他選擇採用參謀[辻政信的建言在雨季中進行攻擊行動](../Page/辻政信.md "wikilink")，因為[辻政信認為西方英國人那些士兵不願意在下雨](../Page/辻政信.md "wikilink")、夜晚作戰，[大日本帝國陸軍軍部原本凝派給](../Page/大日本帝國陸軍.md "wikilink")[山下奉文](../Page/山下奉文.md "wikilink")4個師進行侵攻[任務](../Page/任務.md "wikilink")，但是[山下奉文卻只要](../Page/山下奉文.md "wikilink")2個[師團](../Page/師團.md "wikilink")（約5萬餘人）就足夠了，因為他認為[馬來半島的](../Page/馬來半島.md "wikilink")[叢林](../Page/叢林.md "wikilink")[地形會使得](../Page/地形.md "wikilink")[後勤補給非常不易](../Page/後勤.md "wikilink")，[軍隊人數太多會對](../Page/軍隊.md "wikilink")[後勤補給造成極大負擔](../Page/後勤.md "wikilink")。
太平洋戰爭以馬來亞戰役為始。馬來亞戰役開始於[日本第25軍在](../Page/第25軍_\(日本陸軍\).md "wikilink")1941年12月8日進攻[馬來半島](../Page/馬來半島.md "wikilink")。12月8日凌晨00:30首先在[馬來半島北端](../Page/馬來半島.md "wikilink")[吉蘭丹](../Page/吉蘭丹.md "wikilink")[哥打峇鲁海岸開始炮轟岸面登陸作戰](../Page/哥打峇鲁.md "wikilink")，當時離[珍珠港事件發生前還有約](../Page/珍珠港事件.md "wikilink")48分鐘。\[3\]
之後陸續在[泰國領地](../Page/暹羅.md "wikilink")[北大年府及](../Page/北大年府.md "wikilink")[宋卡港登陸](../Page/宋卡港.md "wikilink")，並且擊退駐守該地的[泰國](../Page/暹羅.md "wikilink")[憲兵](../Page/憲兵.md "wikilink")，目標是向西南越過[馬來—暹羅邊境進攻](../Page/馬來西亞-泰國邊境.md "wikilink")[馬來半島西部地區](../Page/馬來半島.md "wikilink")，同年12月11日另有一支日軍在[馬來半島北部的哥打峇鲁登陸](../Page/馬來半島.md "wikilink")，這與日軍在[泰國的](../Page/泰國.md "wikilink")[北大年府及](../Page/北大年府.md "wikilink")[宋卡港所實施的登陸行動相配合](../Page/宋卡港.md "wikilink")。
經過當日早上與泰國軍隊交火8小時後，日軍已經佔領泰國的軍事基地支援進攻馬來半島。

早上4時正，17架[日本帝國海軍](../Page/日本帝國海軍.md "wikilink")[轟炸機空襲新加坡](../Page/首次轟炸新加坡.md "wikilink")，約61人死亡及有超過700人受傷，這是首次針對[新加坡的空襲行動](../Page/新加坡.md "wikilink")，雖然空襲警報已經嚮起，但街上仍然燈火通明，而雲層亦阻礙盟軍[高射炮尋找日軍](../Page/高射炮.md "wikilink")[轟炸機](../Page/轟炸機.md "wikilink")，沒有任何一架日軍飛機被擊落，它們全部安全返回在[西貢的航空隊基地](../Page/胡志明市.md "wikilink")\[4\]。

[Bristol_Blenheims_62_Squadron_Singapore_Feb_1941.jpg](https://zh.wikipedia.org/wiki/File:Bristol_Blenheims_62_Squadron_Singapore_Feb_1941.jpg "fig:Bristol_Blenheims_62_Squadron_Singapore_Feb_1941.jpg")的[皇家空軍第](../Page/皇家空軍.md "wikilink")62中隊的[布倫亨轟炸機](../Page/布倫亨.md "wikilink")，它們正準備起飛往[吉打](../Page/吉打.md "wikilink")[亞羅士打的空軍基地駐防](../Page/亞羅士打.md "wikilink")，中隊長從1941年6月駐防亞羅士打，後因向日本透露英軍的情報於1942年2月被處決。\]\]

[山下奉文進攻](../Page/山下奉文.md "wikilink")[馬來半島時只有](../Page/馬來半島.md "wikilink")2個師團（約5萬餘人）但卻帶了高達1萬2千餘輛的[腳踏車](../Page/腳踏車.md "wikilink")，為此日軍的士兵負重能力大幅提高，每個日軍士兵能帶10公斤乾糧及6公斤[白米](../Page/白米.md "wikilink")，再加上軍需[槍支等裝備](../Page/槍支.md "wikilink")，每個日軍士兵總負重為34公斤，為當時只能靠步行[移動的英軍士兵](../Page/移動.md "wikilink")，其總負重16公斤的兩倍以上，這些物品大部分都被日軍士兵放到[腳踏車上載運](../Page/腳踏車.md "wikilink")，騎在[腳踏車上的日軍士兵也能在](../Page/腳踏車.md "wikilink")[叢林小徑上快速](../Page/叢林.md "wikilink")[移動](../Page/移動.md "wikilink")，進行[腳踏車](../Page/腳踏車.md "wikilink")[閃擊戰](../Page/閃擊戰.md "wikilink")。
日軍首先遭到[印度第3軍及數個英軍旅的抵抗](../Page/印度第3軍.md "wikilink")，日軍很快就清除了印度軍在海岸線的抵抗之後更集中力量包圍及迫使印度軍投降。

日軍利用快報廢老舊輕型[坦克快速地突破了駐守在](../Page/坦克.md "wikilink")[叢林裡面的英軍散兵坑防線](../Page/叢林.md "wikilink")，因為在[馬來半島上英軍連一台](../Page/馬來半島.md "wikilink")[坦克都沒有](../Page/坦克.md "wikilink")，坦克被英軍參謀認為不適合在[叢林](../Page/叢林.md "wikilink")[地形作戰](../Page/地形.md "wikilink")，故英軍在[馬來半島上沒有配置](../Page/馬來半島.md "wikilink")[坦克](../Page/坦克.md "wikilink")，[山下奉文並且使用蠍子的](../Page/山下奉文.md "wikilink")[鉗擊戰術](../Page/鉗擊.md "wikilink")，先派一部隊正面攻擊英軍，再派另一部隊潛入[叢林繞到英軍背後](../Page/叢林.md "wikilink")[夾擊](../Page/夾擊.md "wikilink")，由於[馬來半島上的英軍從未碰過這種](../Page/馬來半島.md "wikilink")[戰術](../Page/戰術.md "wikilink")，他們不是被日軍擊殺、被俘、或是奔逃到[叢林深處](../Page/叢林.md "wikilink")，英軍雖然往後持續撤退但仍然使用[炸藥炸毀](../Page/炸藥.md "wikilink")[馬來半島上的](../Page/馬來半島.md "wikilink")[橋樑](../Page/橋樑.md "wikilink")，試圖延緩日軍進攻速度，但是日軍卻利用人力[搭橋](../Page/搭橋.md "wikilink")，讓士兵一個一個下水並把木頭放在士兵[肩膀上作出一座簡易浮橋](../Page/肩膀.md "wikilink")，讓其餘日軍能夠快速通過。

日軍在馬來半島北部地面佔有很大優勢，因為他們有強大的空中支援，加上在[裝甲部隊](../Page/坦克.md "wikilink")、互相合作、戰術及經驗均比盟軍優勝，尤其是日軍在[中日戰爭中獲得很多經驗](../Page/中日戰爭.md "wikilink")，英軍方面則沒有坦克，而日軍則使用[腳踏車及輕型坦克](../Page/腳踏車.md "wikilink")，這令他們能輕易穿越馬來半島上的[熱帶雨林](../Page/熱帶雨林.md "wikilink")。

作為鬥牛士行動的代替，一個名叫[Krohcol行動於](../Page/Krohcol行動.md "wikilink")12月8日展開，但印度傭兵組成的軍隊很快便被已在暹羅北大年府登陸的日軍[第5師團擊敗](../Page/第5師團.md "wikilink")。

[Arthur_Percival.jpg](https://zh.wikipedia.org/wiki/File:Arthur_Percival.jpg "fig:Arthur_Percival.jpg")

日軍進攻[哥打峇鲁準備登陸途中](../Page/哥打峇鲁.md "wikilink")，[英國皇家海軍派來了](../Page/英國皇家海軍.md "wikilink")[Z艦隊包括](../Page/Z艦隊.md "wikilink")[航空母艦](../Page/航空母艦.md "wikilink")[不撓號](../Page/不撓號.md "wikilink")、[戰列艦](../Page/戰列艦.md "wikilink")[威爾斯親王號](../Page/威爾斯親王號戰艦.md "wikilink")、[反擊號](../Page/反擊號戰鬥巡洋艦.md "wikilink")、及4艘[驅逐艦](../Page/驅逐艦.md "wikilink")，該艦隊在戰爭爆發前已到達該區並由[海軍上將](../Page/海軍上將.md "wikilink")[湯馬鄞·菲利浦斯指揮](../Page/湯馬鄞·菲利浦斯.md "wikilink")，目的是攔截準備登陸[哥打峇鲁的日軍](../Page/哥打峇鲁.md "wikilink")，但是不幸地該艦隊[航空母艦](../Page/航空母艦.md "wikilink")[不撓號在途中擱淺](../Page/不撓號.md "wikilink")，不得不回航，由於日軍擁有空中優勢，導致[英國皇家海軍於](../Page/英國皇家海軍.md "wikilink")1941年12月10日[威爾斯親王號及](../Page/威爾斯親王號戰艦.md "wikilink")[卻敵號被](../Page/卻敵號戰列巡洋艦.md "wikilink")[日本海軍航空隊](../Page/日本海軍航空隊.md "wikilink")[轟炸機在](../Page/轟炸機.md "wikilink")[彭亨](../Page/彭亨.md "wikilink")、[關丹外海擊沉](../Page/關丹.md "wikilink")，這使得[馬來半島的東海岸門戶大開](../Page/馬來半島.md "wikilink")，[英國皇家海軍遠東艦隊經此役後更無力對抗日軍的登陸行動](../Page/英國皇家海軍.md "wikilink")。

## 空戰

[RAAF21SquadronBrewsterBuffalosMalaya1941.jpg](https://zh.wikipedia.org/wiki/File:RAAF21SquadronBrewsterBuffalosMalaya1941.jpg "fig:RAAF21SquadronBrewsterBuffalosMalaya1941.jpg")最初很成功的擊落日軍的[九七式戰鬥機](../Page/九七式戰鬥機.md "wikilink")，但卻遠遠落後於日軍的[零式艦上戰鬥機](../Page/零式艦上戰鬥機.md "wikilink")\]\]

盟軍在馬來半島的空軍戰鬥機中隊所配備的[水牛戰鬥機存在一些問題](../Page/F2A戰鬥機.md "wikilink")，包括：體型過大及裝備不佳\[5\]\[6\]；零件供應不足\[7\]；支援人員不足\[8\]；飛機場面對空中攻擊防守困難\[9\]；缺乏清楚及協調一致的指揮結構\[10\]；皇家空軍及[皇家澳大利亞空軍中隊及人員間的敵對主義](../Page/皇家澳大利亞空軍.md "wikilink")\[11\]及；沒有經驗的飛行員缺乏適當訓練\[12\]，他們在戰事爆發的頭一個星期付出慘重代價，導致一些飛行中隊需要合併及撤退到[荷屬東印度](../Page/荷屬東印度.md "wikilink")。

剩下的攻擊戰機都是過時的種類 —
[布倫亨式轟炸機](../Page/布倫亨式轟炸機.md "wikilink")、[洛克希德](../Page/洛克希德.md "wikilink")[哈德遜式轟炸機及](../Page/哈德遜式轟炸機.md "wikilink")[維克斯](../Page/維克斯.md "wikilink")
—
這些飛機大部份在地面及空中很快已被日軍戰鬥機擊落，不能發揮有效的作用，相反，一名布倫亨式飛行員隊長，中隊長[阿瑟·史夫卡因](../Page/阿瑟·史夫卡.md "wikilink")12月9日的攻擊獲頒授[維多利亞獎章](../Page/維多利亞獎章.md "wikilink")。其實原本有兩隊布倫亨式轟炸机接到命令要飛往泰國，轟炸日軍飛機及机場，但除了中隊長的飛機成功起飛。其餘的轟炸機在地面时已被日軍炸掉。所以中隊長和他的飛行員乘着一架布倫亨式飛機轟炸日軍机場。最後，他們缶毀了五架零式戰机和炸掉了机場，但被日軍飛機缶中，所以他們要迫降在馬來亞。

而且，最近的調查顯示出日本軍事情報部門收買了一名英國軍官上校，這名與印度軍接觸的空軍聯絡官為間諜\[13\]，當他開始提供情報後，日軍能在3天內摧毀所有在馬來半島北部的盟國空軍基地，漢南於12月10日被拘留及帶到[新加坡](../Page/新加坡.md "wikilink")，但日本人已獲得制空權。

1月下旬，佰翠·漢南被認為勇敢的戰士及被認為已經陣亡\[14\]，2月13日，[新加坡戰役開始](../Page/抗日战争_\(新加坡\).md "wikilink")5天後，他被英軍憲兵帶入市中心，再被帶到海邊處決，其屍體被拋入海中\[15\]。

## 從馬來半島南下

[Pacific_War_-_Malaya_1941-42_-_Map.jpg](https://zh.wikipedia.org/wiki/File:Pacific_War_-_Malaya_1941-42_-_Map.jpg "fig:Pacific_War_-_Malaya_1941-42_-_Map.jpg")
12月11日，日軍在坦克支援下從暹羅南下，在[日得拉打敗了英國及印度軍隊及快速從東北部的](../Page/日得拉戰役.md "wikilink")[哥打峇鲁灘頭向內陸推進](../Page/哥打峇鲁.md "wikilink")，以瓦解北部的防守，由於沒有真正的海軍力量存在，英軍不能阻止日本海軍在馬來半島海岸的行動，雖然這些行動對入侵部隊幫助不大，由於沒有剩下任何盟軍飛機，日本取得了制空權，不斷從空中攻擊地面上的大英國協軍隊及平民。

馬來半島的島嶼[檳榔嶼從](../Page/檳榔嶼.md "wikilink")12月8日起每天都遭到日軍轟炸，英军在12月17日棄守，武器、船隻、物資及一個仍可運作的電台落入日本的手中。歐洲人從槟城逃离，本地居民則被遗弃在日本人手里，这给英國造成了難堪的局面，英國人與本地居民的關係也从此疏离。历史学者称：“英国对东南亚的殖民统治在道德上的丧失，并不始于新加坡，而是始于槟城。”\[16\]

12月23日，由[大衛·梅菲-里昂指揮的](../Page/大衛·梅菲-里昂.md "wikilink")[印度第11步兵師被命令阻擊日軍](../Page/印度第11步兵師.md "wikilink")，但效用不大，到1月的第1個星期結束時，整個馬來半島北部已完全落入日本人手中，同時，泰國加入[轴心國](../Page/轴心國.md "wikilink")，，泰軍被容許恢復對馬來半島北部的[玻璃市](../Page/玻璃市.md "wikilink")、[吉打](../Page/吉打.md "wikilink")、[吉兰丹及](../Page/吉兰丹.md "wikilink")[登嘉楼四个传统马来藩属行使](../Page/登嘉楼.md "wikilink")[宗主权](../Page/宗主权.md "wikilink")，因此統一其佔領的區域，而日軍很快到達下一個目標，[吉隆坡市](../Page/吉隆坡.md "wikilink")，日本人於1942年1月11日沒有遇到抵抗下進入及佔領該城，不足200英哩外的新加坡島即將面對日軍的入侵。

在[仕林河戰役的災難後](../Page/仕林河戰役.md "wikilink")，印度第11步兵師奉命[在吉隆坡阻擊日軍數天](../Page/吉隆坡戰役.md "wikilink")，在仕林河戰役中，2個印度步兵旅實際上被消滅，而另一個印度步兵旅亦在麻坡被消滅。

## 防守柔彿海峽

<File:Royal> Engineers prepare to blow up a bridge in
Malaya.jpg|<small>皇家陸軍工兵在[吉隆坡正準備引爆橋樑](../Page/吉隆坡.md "wikilink")</small>
<File:UmpCABKYXM0.jpg>|<small>澳大利亞軍的[反坦克炮在麻坡正攻擊日軍坦克](../Page/反坦克炮.md "wikilink")</small>
<File:JapaneseBicycle001.jpg>|<small>在[馬來西亞的國家歷史博物館正展出日本士兵的軍服及其使用的腳踏車](../Page/馬來西亞.md "wikilink")</small>

1月14日，日軍已經到達馬來半島南部州属[柔彿州](../Page/柔彿州.md "wikilink")，他們在這裡遇到由[亨利·哥頓·本尼特指揮的](../Page/亨利·哥頓·本尼特.md "wikilink")[澳大利亞第8師](../Page/澳大利亞第8師.md "wikilink")，日軍在這裡遇到在戰術上的首次挫折，因為澳大利亞軍在[金馬士市一帶建立了堅強防線](../Page/金馬士.md "wikilink")，戰役圍繞金馬士橋進行，日軍在此地付出了死傷1000人的代價，但大橋卻在戰鬥中被毀，日軍耗費6個小時的時間修復橋樑。

當日軍在金馬士西面攻擊澳大利亞軍的側翼時，一場在這次戰事中最血腥的戰役於1月15日在半島西海岸的[麻坡河一帶爆發](../Page/麻坡河.md "wikilink")，本尼特派出已被削弱的印度第45旅(只有不半兵員已受訓練)去防衛麻坡河南岸，但被從海上登陸的日軍攻擊其側翼，整個旅被澈底擊潰，其旅長及屬下3個團的團長全部陣亡。

在澳大利亞中校[查理士·安達臣的領導下](../Page/查理士·安達臣.md "wikilink")，撤退的印度軍隊在澳大利亞軍的支援下組成麻坡軍，孤注一擲的進行了4天的戰鬥，令其它大英國協的殘餘部隊可以從馬來半島北部撤退以避免被包圍，當麻坡軍到達了在[巴里梳龍的橋樑及發現橋樑已被日軍佔領時](../Page/巴里梳龍.md "wikilink")，安達臣在死傷數字不斷上升下，命令所有人衝過去，一些人逃入附近的叢林、沼澤地及橡膠林地以尋找其步兵團在[永平的司令部](../Page/永平_\(馬來西亞\).md "wikilink")，135名受傷的-{}-士兵則落入日軍的圍剿，除了兩名士兵以外其他傷兵都陣亡。

1月20日，雖然面對皇家空軍威格士式轟炸機的轟炸，日軍在[興樓再作進一步登陸成功](../Page/興樓.md "wikilink")，大英國協軍隊的在柔彿最後防線的[峇株巴轄](../Page/峇株巴轄.md "wikilink")-[居鑾](../Page/居鑾.md "wikilink")-[豐盛港一帶全線面對日軍的進攻](../Page/豐盛港.md "wikilink")，不幸地，白思华拒絕在新加坡北面的柔彿建立固定防禦。

1月27日，白思华接到[美國-英國-荷蘭-澳大利亞聯合司令部司令](../Page/美國-英國-荷蘭-澳大利亞聯合司令部.md "wikilink")[阿奇博爾德·珀西瓦爾·韋維爾的批准](../Page/阿奇博爾德·珀西瓦爾·韋維爾.md "wikilink")，命令部隊越過[柔彿海峽撤退到](../Page/柔彿海峽.md "wikilink")[新加坡島](../Page/新加坡.md "wikilink")。

## 撤退到新加坡

[Singapore_causeway_blown_up.jpg](https://zh.wikipedia.org/wiki/File:Singapore_causeway_blown_up.jpg "fig:Singapore_causeway_blown_up.jpg")

1月31日，最後一批盟軍軍隊離開[馬來半島](../Page/馬來半島.md "wikilink")，英軍工兵在連接[柔彿及新加坡之間的](../Page/柔彿.md "wikilink")[新柔長堤炸開一個闊](../Page/新柔長堤.md "wikilink")70呎(20米)的大洞（少數落後隊伍的-{}-士兵在之後數天仍通過此長堤）。

[新加坡為英國在](../Page/新加坡.md "wikilink")[東南亞的](../Page/東南亞.md "wikilink")[政治](../Page/政治.md "wikilink")[經濟中心](../Page/經濟.md "wikilink")，日軍必須要拿下[新加坡才可以徹底把英國趕出](../Page/新加坡.md "wikilink")[東南亞](../Page/東南亞.md "wikilink")。
1942年2月1日[新加坡攻防戰開始後](../Page/新加坡.md "wikilink")，[山下奉文採用聲東擊西戰術先派一部隊](../Page/山下奉文.md "wikilink")[新加坡島東北部假裝進攻](../Page/新加坡.md "wikilink")，成功吸引了駐守在該島上英軍的注意力，然後在[新加坡島西北部的日軍趁機用充氣橡皮艇橫渡](../Page/新加坡.md "wikilink")[柔佛海峡](../Page/柔佛海峡.md "wikilink")，並在[裕廊登陸攻進](../Page/裕廊.md "wikilink")[新加坡島](../Page/新加坡.md "wikilink")。
1942年2月15日（時值[農曆新年](../Page/農曆新年.md "wikilink")），[英國](../Page/英國.md "wikilink")[白思华中将](../Page/白思华.md "wikilink")（）向[大日本帝国陸軍的](../Page/大日本帝国陸軍.md "wikilink")[山下奉文中將](../Page/山下奉文.md "wikilink")[無條件投降](../Page/無條件投降.md "wikilink")。[新加坡島上總數約六萬餘人的英軍頓時成為](../Page/新加坡.md "wikilink")[俘虜](../Page/俘虜.md "wikilink")，負責進攻的3萬人日軍卻只有9千6百餘人戰死或失蹤，日軍以大約8個星期的時間成功佔領了[馬來半島跟](../Page/馬來半島.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")，負責指揮日軍的陸軍中將[山下奉文還因此出色戰績被暱稱為](../Page/山下奉文.md "wikilink")「馬來之虎」。
馬來亞戰役以[大英國協軍隊被徹底擊敗作為結束](../Page/大英國協.md "wikilink")，至此[大英國協軍隊完全撤出](../Page/大英國協.md "wikilink")[英屬馬來亞無力與](../Page/馬來亞半島.md "wikilink")[大日本帝國軍隊相抗衡](../Page/大日本帝國.md "wikilink")。

## 相關條目

  - [馬來亞游擊隊](../Page/馬來亞游擊隊.md "wikilink")
  - [日本入侵马来亚战争](../Page/日本入侵马来亚战争.md "wikilink")

## 注释

## 參考文献

  - Bayly, Christopher / Harper, Tim: *Forgotten Armies. Britain's Asian
    Empire and the War with Japan*. Penguin Books, London, 2005
  - Dixon, Norman F, *On the Psychology of Military Incompetence*,
    London, 1976
  - Bose, Romen, "SECRETS OF THE BATTLEBOX:The Role and history of
    Britain's Command HQ during the Malayan Campaign", MArshall
    Cavendish, Singapore, 2005
  - Seki, Eiji, *Sinking of the SS Automedon And the Role of the
    Japanese Navy: A New Interpretation*, University of Hawaii Press,
    2007
  - Smyth, John George Smyth, *Percival and the Tragedy of Singapore*,
    MacDonald and Company, 1971
  - Thompson, Peter, *The Battle for Singapore*, London, 2005, ISBN
    978-0-7499-5068-2 (HB)

## 外部連結

  - [Campaign in Malaya on The Children (& Families) of the Far East
    Prisoners of
    War](http://www.cofepow.org.uk/pages/armedforces_m_campaign.html)
  - [Royal Engineers
    Museum](https://web.archive.org/web/20090503114041/http://www.remuseum.org.uk/corpshistory/rem_corps_part16.htm#far)
    Royal Engineers and the Second World War - the Far East
  - [Australia's War 1939-1945: Battle of
    Malaya](http://www.ww2australia.gov.au/japadvance/malaya.html)

[Category:第二次世界大战太平洋战场战役](../Category/第二次世界大战太平洋战场战役.md "wikilink")
[Category:英國战役](../Category/英國战役.md "wikilink")
[Category:日本战役](../Category/日本战役.md "wikilink")
[Category:澳大利亚战役](../Category/澳大利亚战役.md "wikilink")
[Category:马来西亚历史](../Category/马来西亚历史.md "wikilink")

1.

2.

3.

4.  [First air raid on
    Singapore](http://infopedia.nlb.gov.sg/articles/SIP_7_2005-01-25.html)
     Access date: August 12, 2007

5.  [Squadron Leader W.J. Harper, 1946, "REPORT ON NO. 21 AND NO. 453
    RAAF SQUADRONS" (UK Air
    Ministry)](http://www.warbirdforum.com/secret.htm), p.1 (Source: UK
    Public Records Office, ref. AIR 20/5578; transcribed by Dan Ford for
    *Warbird's Forum*.) Access date: September 8, 2007

6.  [*Harper report*, p.2](http://www.warbirdforum.com/secret2.htm)

7.
8.  *Harper report*, p.1-2

9.
10.
11.
12.
13. [Peter Elphick, 2001, "Cover-ups and the Singapore Traitor
    Affair"](http://www.abc.net.au/4corners/specials/noprisoners/viewpoints/elphick.htm)
    Access date: March 5, 2007.

14.
15. [Lynette Silver, 1997, "Scapegoats for the Bloody
    Empire"](http://www.abc.net.au/4corners/specials/noprisoners/viewpoints/silver.htm)
    (from Edward Docker & Lynette Silver \[eds\], *Fabulous Furphies —
    10 Great Myths from Australia's Past*, Sally Milner Publishing,
    \[ISBN 978-1-86351-184-1\]; 由*[Four
    Corners](../Page/:en:Four_Corners_\(TV_program\).md "wikilink")*於網路上出版,
    [澳洲廣播公司](../Page/澳洲廣播公司.md "wikilink"), 2002. 2014-2-1查閱

16. Bayly/Harper, p.119