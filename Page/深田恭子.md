**深田恭子**（，），[日本](../Page/日本.md "wikilink")[女藝人](../Page/女藝人.md "wikilink")，出生於[東京都](../Page/東京都.md "wikilink")，暱稱「（深恭）」。曾主演多部著名電視劇和電影，也曾以歌手身份發行多張唱片，目前所屬經紀公司為[Horipro](../Page/Horipro.md "wikilink")。

## 演藝生涯

深田恭子初中二年級生時，以14歲之齡參加了[經紀公司Horipro舉辦的](../Page/經紀公司.md "wikilink")1996年《第21回》，在「PURE
GIRL審查」中得獎，被發掘成為新人加入演藝圈。翌年，已獲首個演出機會，在電視劇《》演出一角，但尚未引起注目。

1998年，深田恭子在話題電視劇《[神啊！請多給我一點時間](../Page/神啊！請多給我一點時間.md "wikilink")》和[金城武共演](../Page/金城武.md "wikilink")。劇中飾演感情率直奔放、但因[援交染上](../Page/援交.md "wikilink")[HIV病毒的少女](../Page/HIV.md "wikilink")「葉野真生」。深田恭子以此劇的演技造成話題，並創下最終回28.3%的高收視率。深田恭子也憑此劇獲1998年[第18回日劇學院賞助演女優賞](../Page/第18回日劇學院賞.md "wikilink")。翌年，首次主演電視劇《[鬼之棲家](../Page/鬼之棲家.md "wikilink")》，並在同年5月發行個人首張單曲〈〉。

2004年，以主演的電影《[下妻物語](../Page/下妻物語.md "wikilink")》獲多座最佳女主角獎，演技受到肯定。\[1\]\[2\]

## 人物

  - 深田恭子有一個妹妹。
  - 與163公分的身高相較。
  - 自2歲起喜歡[游泳](../Page/游泳.md "wikilink")，曾以為自己前世是[海豚](../Page/海豚.md "wikilink")，曾經以[奧運為目標](../Page/奧運.md "wikilink")，官方影迷會命名為「pool」。在多部電視劇和廣告的游泳鏡頭都是親自上場。4歳開始學[鋼琴](../Page/鋼琴.md "wikilink")，曾在《[新堂本兄弟](../Page/新堂本兄弟.md "wikilink")》節目中擔任固定鋼琴伴奏\[3\]。
  - [書法為特一段](../Page/書法.md "wikilink")。在電視劇《[鬼之棲家](../Page/鬼之棲家.md "wikilink")》，劇名斗大的「鬼」字為深田本人所寫，劇中出現的信、紙條皆為本人字跡。

## 主要作品

### 舞台劇

  - 2015年8月 《活了一百萬次的貓》(原名:100万回生きたねこ)佐野陽子繪本改編，8月15日 - 30日於東京藝術劇場 - 主角 白貓

### 電視劇

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播年份</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p>海峽</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>這就是答案！</p></td>
<td><p>水野和音</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p>新聞女郎</p></td>
<td><p>內田亞矢</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/神啊！請多給我一點時間.md" title="wikilink">神啊！請多給我一點時間</a></strong></p></td>
<td><p><strong>葉野真生</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><strong><a href="../Page/鬼之棲家.md" title="wikilink">鬼之棲家</a></strong></p></td>
<td><p><strong>加藤あゆみ</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/To_Heart_(電視劇).md" title="wikilink">To Heart</a></strong></p></td>
<td><p><strong>三浦透子</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p>續寫友誼</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>愛情夢幻</p></td>
<td><p><strong>飯島有羽</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>大胃王</p></td>
<td><p><strong>田村麻由美</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p>第17年的爸爸</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>S.O.S.蛋糕上的草莓</p></td>
<td><p>三沢唯</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>鬥陣美眉</p></td>
<td><p><strong>吉田小夜子</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><strong><a href="../Page/命运之恋.md" title="wikilink">命运之恋</a></strong></p></td>
<td><p><strong>淺井智子</strong></p></td>
</tr>
<tr class="odd">
<td><p>First Love</p></td>
<td><p>江澤夏澄</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>遥控刑警</p></td>
<td><p><strong>彩木久琉美</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>父亲大人</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>兩個人～我們選擇的路～</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>戀愛中姊妹</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p>她已不在人世</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>农情芳心</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>小南的迷你情人</p></td>
<td><p><strong>堀切千嘉</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>圣诞节，最讨厌了</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>德川綱吉 被稱作狗的男人</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><strong><a href="../Page/富豪刑事.md" title="wikilink">富豪刑事</a></strong></p></td>
<td><p><strong>神戶美和子</strong></p></td>
</tr>
<tr class="even">
<td><p>想要幸福</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>梅子</p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><strong><a href="../Page/富豪刑事.md" title="wikilink">富豪刑事 2</a></strong></p></td>
<td><p><strong>神戶美和子</strong></p></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/山女壁女.md" title="wikilink">山女壁女</a></p></td>
<td><p><strong>毬谷真理惠</strong></p></td>
</tr>
<tr class="even">
<td><p>你給我的夏天</p></td>
<td><p>木崎トキコ</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>生之慾</p></td>
<td><p>小田切サチ役</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>藍眸與云</p></td>
<td><p>瀬恵梨香</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/神探伽利略.md" title="wikilink">神探伽利略</a></p></td>
<td><p>菅原靜子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008年</p></td>
<td><p><strong><a href="../Page/未來講師.md" title="wikilink">未來講師</a></strong></p></td>
<td><p><strong>吉田巡</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/學校學不到的事.md" title="wikilink">學校學不到的事</a></strong></p></td>
<td><p><strong>相田舞</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p><a href="../Page/天地人.md" title="wikilink">天地人</a></p></td>
<td><p><a href="../Page/淀殿.md" title="wikilink">淀殿</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/華麗的間諜.md" title="wikilink">華麗的間諜</a></strong></p></td>
<td><p><strong>桃樂菲</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><strong><a href="../Page/率直男人.md" title="wikilink">率直男人</a></strong></p></td>
<td><p><strong>栗田鳴海</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/彩虹閃耀夏之戀.md" title="wikilink">彩虹閃耀夏之戀</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第二處女</p></td>
<td><p>鈴木万理江</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/幻夜.md" title="wikilink">幻夜</a></strong></p></td>
<td><p><strong>新海美冬</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><strong><a href="../Page/我是影子.md" title="wikilink">專業主婦偵探～我是影子</a></strong></p></td>
<td><p><strong>淺葱芹菜</strong></p></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/平清盛_(大河劇).md" title="wikilink">平清盛</a></p></td>
<td><p><a href="../Page/平時子.md" title="wikilink">平時子</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/TOKYO机场～东京机场管制保安部～.md" title="wikilink">TOKYO机场～东京机场管制保安部～</a></strong></p></td>
<td><p><strong>篠田香織</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><strong><a href="../Page/無名毒.md" title="wikilink">無名毒</a></strong></p></td>
<td><p><strong>梶田聰美</strong></p></td>
</tr>
<tr class="even">
<td><p><strong></strong></p></td>
<td><p><strong>大久保由里子</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>里見涼</strong></p></td>
</tr>
<tr class="even">
<td><p><strong></strong></p></td>
<td><p><strong>岩崎麗</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><strong><a href="../Page/Second_Love.md" title="wikilink">Second Love</a></strong></p></td>
<td><p><strong>西原結唯</strong></p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p><strong><a href="../Page/請和這個沒用的我談戀愛.md" title="wikilink">請和這個沒用的我談戀愛</a></strong></p></td>
<td><p><strong>柴田倫子</strong></p></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><strong><a href="../Page/下剋上應考.md" title="wikilink">下剋上應考</a></strong></p></td>
<td><p><strong>櫻井香夏子</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/偵探物語.md" title="wikilink">偵探物語</a></strong></p></td>
<td><p><strong>四俵蘭子</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><strong></strong></p></td>
<td><p><strong>五十嵐奈奈</strong></p></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td><p><strong><a href="../Page/初次戀愛那一天所讀的故事.md" title="wikilink">初次戀愛那一天所讀的故事</a></strong></p></td>
<td><p><strong>春見順子</strong></p></td>
</tr>
</tbody>
</table>

### 電影

  - 1998年「新宿少年探偵團」（1998年4月、[松竹](../Page/松竹.md "wikilink")、導演：淵井正文），飾演夢野美香
  - 1999年「午夜凶鈴2」（1999年1月、[東寶](../Page/東寶.md "wikilink")、導演：中田秀夫），飾演澤口香苗
  - 2000年「死者的學園祭」（2000年8月、[東映](../Page/東映.md "wikilink")、導演：篠原哲雄），飾演結城真知子
  - 2001年「東京瘋」（2001年2月、東映） 的一個短篇《約束》（導演[陳慧琳](../Page/陳慧琳.md "wikilink")）
  - 2002年「[淨琉璃](../Page/淨琉璃_\(電影\).md "wikilink")」（2002年10月、[松竹](../Page/松竹.md "wikilink")、導演：[北野武](../Page/北野武.md "wikilink")），飾演山口春菜
  - 2003年「陰陽師II」（2003年10月、東寶、導演：[瀧田洋二郎](../Page/瀧田洋二郎.md "wikilink")），飾演藤原日美子
  - 2003年「宛如阿修羅」（2003年11月、東寶、導演：[森田芳光](../Page/森田芳光.md "wikilink")），飾演陣內咲子
  - 2004年「[下妻物語](../Page/下妻物語.md "wikilink")」（2004年5月、東寶、導演：[中島哲也](../Page/中島哲也.md "wikilink")），飾演龍崎桃子
  - 2006年「天使」（2006年1月、[松竹](../Page/松竹.md "wikilink")、導演：宮坂真弓），飾演天使
  - 2006年「犬神家一族」（2006年12月、東寶、導演：[市川崑](../Page/市川崑.md "wikilink")）
    ，飾演女中・はる
  - 2008年「小雙俠」（2009年3月、[日活](../Page/日活.md "wikilink")、導演：[三池崇史](../Page/三池崇史.md "wikilink")），飾演多龍芝
  - 2009年「心動奇蹟：烏魯魯的森林物語」（2009年12月、東寶、導演：長沼誠） ，飾演生野千惠
  - 2010年「戀愛戲曲：請和我戀愛吧\!」（2010年9月、SHOWGATE、導演：鴻上尚史） ，主演，飾演谷山真由美
  - 2011年「豆富小僧」（2011年4月、[華納](../Page/華納.md "wikilink")、導演：杉井儀三郎）
    ，飾演豆富小僧（配音）
  - 2011年「[烏龍派出所](../Page/烏龍派出所.md "wikilink") THE
    MOVIE」（2011年8月、[松竹](../Page/松竹.md "wikilink")、導演：川村泰祐）
    ，飾演沢村桃子
  - 2011年「Second Virgin」（2011年9月、[松竹](../Page/松竹.md "wikilink")、導演：黒崎博）
    ，飾演鈴木万理江
  - 2011年「[黎明破曉的街道](../Page/黎明破曉的街道.md "wikilink")」（2011年10月、[角川映画](../Page/角川映画.md "wikilink")、導演：[若松節朗](../Page/若松節朗.md "wikilink")）
    ，飾演仲西秋葉
  - 2011年「[鬼壓床了沒](../Page/鬼壓床了沒.md "wikilink")」（2011年10月、東寶、導演：[三谷幸喜](../Page/三谷幸喜.md "wikilink")）
    ，飾演前田熊
  - 2011年「Wild 7」（2011年12月、華納、導演：羽住英一郎） ，飾演本間雪
  - 2013年「[我的恐怖室友](../Page/我的恐怖室友.md "wikilink")」（2013年11月、東映、導演：古澤健）
    ，飾演西村麗子
  - 2014年「[偉大的咻啦啦砰](../Page/偉大的咻啦啦砰.md "wikilink")」（2014年3月、東映、導演：水落豐）
    ，飾演日出清子
  - 2014年「[超高速！參勤交代](../Page/超高速！參勤交代.md "wikilink")」（2014年6月、[松竹](../Page/松竹.md "wikilink")、導演：本木克英）
    ，飾演阿咲
  - 2015年「D機關」 (2015 年 3 月 27 日)(台灣)
  - 2016年 「超高速\!参勤交代
    リターンズ」（2016年9月、[松竹](../Page/松竹.md "wikilink")、導演：本木克英）
    ，飾演阿咲

### 唱片

  - **單曲**
      - 1999年「最後の果実」（初回限定盤）
      - 1999年「最後の果実」（一般盤）
      - 1999年「イージーライダー」
      - 2000年「煌めきの瞬間」
      - 2001年「How？」（初回盤）
      - 2001年「How？」（通常盤）
      - 2001年「スイミング」
      - 2001年「キミノヒトミニコイシテル」
      - 2002年「ルート246」（初回限定盤）
      - 2002年「ルート246」（通常盤）

<!-- end list -->

  - **專輯**
      - 1999年「Dear…」
      - 2000年「moon」
      - 2001年「Universe」
      - 2002年「Flow～Kyoko Fukada Remixes～」

### Video & DVD

  - for me（1999年1月、ポニーキャニオン）Video:PCVE-10883 / DVD:PCBE-00001

  - to me（1999年2月、ポニーキャニオン）Video:PCVE-10884 / DVD:PCBE-00003

  - for you（1999年12月、ポニーキャニオン）Video:PCVG-10640 / DVD:PCBG-00086

  - to you（2001年6月、ポニーキャニオン）Video:PCVG-10756 / DVD:PCBG-50202

  - （2002年11月、[パイオニアLDC](../Page/パイオニアLDC.md "wikilink")）Video:PIVS-7416
    / DVD:PIBW-7153

  - （2002年11月、パイオニアLDC）Video:PIVS-7417 / DVD:PIBW-7154

  - （2003年12月、ポニーキャニオン）DVD:PCBE-50799

  - （2004年5月、アミューズソフトエンタテインメント）ASBY-2490

  - （2006年1月、ハピネット・ピクチャーズ）BIBJ-5884

## 得獎紀錄

| 年份 | 大賞                                               | 獎項                        | 作品                                                   |
| -- | ------------------------------------------------ | ------------------------- | ---------------------------------------------------- |
|    | [第18回日劇學院賞](../Page/第18回日劇學院賞.md "wikilink")     | 最佳女配角                     | 《[神啊！請多給我一點時間](../Page/神啊！請多給我一點時間.md "wikilink")》   |
|    | 第2回 日劇大賞（1999-05-04）                             | 最佳女配角                     | 《[神啊！請多給我一點時間](../Page/神啊！請多給我一點時間.md "wikilink")》   |
|    | [第22回日劇學院賞](../Page/第22回日劇學院賞.md "wikilink")     | 最佳女配角                     | 《[To Heart](../Page/To_Heart_\(電視劇\).md "wikilink")》 |
|    | 第21回タレント・スカウト・キャラバン グランプリ（1996年）                 |                           |                                                      |
|    | [第24回日本電影金像獎](../Page/第24回日本電影金像獎.md "wikilink") | 新人賞                       |                                                      |
|    | 第59回 [每日電影獎](../Page/每日電影獎.md "wikilink")        | 女優主演賞                     | 《[下妻物語](../Page/下妻物語.md "wikilink")》                 |
|    | 第26回 [橫濱電影節](../Page/橫濱電影節.md "wikilink")        | 主演女優賞                     | 《[下妻物語](../Page/下妻物語.md "wikilink")》                 |
|    | [第28回日本電影金像獎](../Page/第28回日本電影金像獎.md "wikilink") | 優秀主演女優賞                   | 《[下妻物語](../Page/下妻物語.md "wikilink")》                 |
|    | 第14回 東京體育映畫大賞                                    | 主演女優賞                     | 《[下妻物語](../Page/下妻物語.md "wikilink")》                 |
|    | ゆうばり國際ファンタスティック映畫祭                               | 2005マックスファクタービューティースピリット賞 |                                                      |
|    | 第18回DVD＆錄影帶大獎                                    | 最佳藝人獎                     |                                                      |

## 写真集

  - プール（1998年7月、リトル・モア、ISBN 4-947648-76-7）撮影：徳永彩
  - COLORS（カラーズ）（1998年11月、學習研究社、ISBN 4-05-401014-8）撮影：木村晴
  - AVENIR（アヴニール）（2001年5月、學習研究社、ISBN 4-05-401407-0）撮影：木村晴
  - friends（2002年1月、集英社、ISBN 4-08-780345-7）撮影：細野晉司
  - まるごと深田恭子BOOK KYOKO 8203（2003年4月、集英社、ISBN 4-08-780374-0）
  - 深田恭子in下妻物語（2004年5月、ぴあ、ISBN 4-8356-0937-9）
  - 深田恭子meets天使 映畫『天使』Photo Making Book（2006年1月、祥伝社、ISBN 4-396-46009-0）
  - 25才 （2008年11月、角川ザテレビジョン、ISBN 978-4-04-895034-3）
  - 深田恭子寫真集『KYOKO TOKYO PIN-UP GIRL'（2009年2月 ワニブックスISBN
    978-4-8470-4161-7）
  - EXOTIQUE（2010年11月2日、ワニブックス、撮影：レスリー・キー）ISBN 978-4847043260
  - 月刊 NEO 深田 恭子（2011年2月8日、イーネット・フロンティア、撮影：鶴田直樹）ISBN 978-4862058263
  - Blue Moon（2012年11月24日、ワニブックス）ISBN 9784847045059
  - (un)touch（2014年3月13日、講談社、撮影：アンディ・チャオ）\[31\]ISBN 978-4062189163
  - Down to earth（2014年12月10日、ワニブックス、撮影：アンディ・チャオ）\[32\] ISBN
    978-4-8470-4693-3
  - This Is Me（2016年7月23日、集英社、編集：MAQUIA）ISBN 978-4087807899\[33\]
  - AKUA（2016年7月23日、集英社、撮影：ND CHOW）ISBN 978-4087807943\[33\]
  - Reflection（2016年11月12日、集英社）ISBN 978-4087808032

## 參考資料

## 相关条目

  - [堀越高等学校人物列表](../Page/堀越高等学校人物列表.md "wikilink")

## 外部連結

  -
  - [深田恭子個人官方網站](http://www.horipro.co.jp/fukadakyoko//)（Kyoko Channel）

[Category:日本女演員](../Category/日本女演員.md "wikilink")
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:日本流行音樂歌手](../Category/日本流行音樂歌手.md "wikilink")
[Category:日本寫真偶像](../Category/日本寫真偶像.md "wikilink")
[Category:Horipro](../Category/Horipro.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:每日電影獎最佳女主角得主](../Category/每日電影獎最佳女主角得主.md "wikilink")
[Category:藍絲帶獎最佳女配角得主](../Category/藍絲帶獎最佳女配角得主.md "wikilink")
[Category:日劇學院賞最佳女配角得主](../Category/日劇學院賞最佳女配角得主.md "wikilink")
[Category:橫濱電影節最佳女主角得主](../Category/橫濱電影節最佳女主角得主.md "wikilink")
[Category:日本電影學院獎新人獎得主](../Category/日本電影學院獎新人獎得主.md "wikilink")
[Category:報知金酸莓獎獲獎者](../Category/報知金酸莓獎獲獎者.md "wikilink")
[Category:日刊體育電影大獎新人獎得主](../Category/日刊體育電影大獎新人獎得主.md "wikilink")

1.
2.
3.