**全面品質管理**（****，**TQM**）\[1\]是一種針對所有組織過程中深入[品質](../Page/品質.md "wikilink")[意識的](../Page/意識.md "wikilink")[管理策略](../Page/管理學.md "wikilink")。

## 簡介

全面品質管理已經廣泛使用在[製造](../Page/製造.md "wikilink")、[教育](../Page/教育.md "wikilink")、[政府與](../Page/政府.md "wikilink")[第三產業](../Page/第三產業.md "wikilink")，以及[美國國家航空暨太空總署的太空科學計畫等領域](../Page/美國國家航空暨太空總署.md "wikilink")。全面品質提供了一個保護傘，使得在組織之中的所有人都能夠競爭，並持續降低成本及提升[客戶滿意度](../Page/客戶滿意度.md "wikilink")（Customer
satisfaction）。

而TQM（Total Quality Management）與TQC（Total Quality
Control）的分別則在於使用國家的區別，如日本與美國，就是其一使用TQM，其二使用TQC。

## 概念及範疇

全面品質管理可用三個Q（Quality）來表示：

1.  [人的品質](../Page/人.md "wikilink")
2.  [系統及](../Page/系統.md "wikilink")[流程](../Page/流程.md "wikilink")（Process）的品質
3.  [產品及](../Page/產品.md "wikilink")[服務的品質](../Page/服務.md "wikilink")

TQM的架構主體是[品質管理發展的主流之一](../Page/品質管理.md "wikilink")。

全面品質管制學說的前身為品質管制。該理論的核心為把管理完全交給品質控制工程師和技術人員，通過品質檢驗與統計方法，從而降低企業生產成本因。但由於該方式的管理具有局限性，即公司管理不能只靠工程師和技術人員，因此該管理方式逐漸被視為企業管理的一個分支，而非整體的方法論。

這之後，從20世紀50年代開始，企業逐漸將全面品質管制學取代了品質管制學說。並強調公司員工要對品質文化有一個統一的認同，並通過共同實踐，才可以實現公司管理品質的提高。

這場由[美國通用電氣公司的費根堡姆和](../Page/美國通用電氣公司.md "wikilink")[品質管制專家](../Page/品質管制.md "wikilink")[Juran提出的](../Page/約瑟夫•朱蘭.md "wikilink")“全面品質管制”運動，在日本也有重大影響。1950年，美國博士W.E戴明先生在東京的日本醫藥協會大禮堂進行了為期8天的講品質管制講座。\[2\]而為了紀念戴明博士為品質運動做出的貢獻，人們成立了戴明運用獎。[戴明獎也成為了日本品質管制的最高獎](../Page/戴明獎.md "wikilink")。

其他著名的世界品質管制獎還有EFQM[歐洲品質獎](../Page/歐洲品質獎.md "wikilink")，波多裡奇品質獎等。針對三個獎項的區別，北京市品質技術監督局做了以下表格進行分析\[3\]：

<table>
<tbody>
<tr class="odd">
<td><p><strong>差異點</strong></p></td>
<td><p><strong>波多裡奇品質獎（美國）</strong></p></td>
<td><p><strong>戴明獎（日本）</strong></p></td>
<td><p><strong>歐洲品質獎</strong></p></td>
</tr>
<tr class="even">
<td><p>成立時間</p></td>
<td><p>1987年</p></td>
<td><p>1951年</p></td>
<td><p>1992年</p></td>
</tr>
<tr class="odd">
<td><p>特點</p></td>
<td><p>運用範圍最廣</p></td>
<td><p>成立最早</p></td>
<td><p>參評國家最多</p></td>
</tr>
<tr class="even">
<td><p>評審組織</p></td>
<td><p>美國國家標準和技術研究院</p></td>
<td><p>日本科學技術聯盟</p></td>
<td><p>歐洲品質管制基金會</p></td>
</tr>
<tr class="odd">
<td><p>評審著重點</p></td>
<td><p>組織績效，經營結果</p></td>
<td><p>強調統計品質控制技術應用</p></td>
<td><p>顧客、員工滿意度，對社會的影響和績效</p></td>
</tr>
<tr class="even">
<td><p>獎項設置</p></td>
<td><p>六大行業</p></td>
<td><p>四大獎項</p></td>
<td><p>三大獎項</p></td>
</tr>
<tr class="odd">
<td><p>評獎範圍</p></td>
<td><p>海外企業不可申請</p></td>
<td><p>向海外企業開放</p></td>
<td><p>品質管制活動必須在歐洲發生</p></td>
</tr>
<tr class="even">
<td><p>核心理念</p></td>
<td><p>11條核心理念</p></td>
<td><p>沒有統一理念</p></td>
<td><p>8條基本理念</p></td>
</tr>
<tr class="odd">
<td><p>評價結構</p></td>
<td><p>波多裡奇框架結構（六過程和一結果）</p></td>
<td><p>沒有建立任何聯繫概念、行動、過程和結果的框架（六個基本要求）</p></td>
<td><p>EFQM卓越模式的框架結構（五手段和四結果）</p></td>
</tr>
<tr class="even">
<td><p>評價標準</p></td>
<td><p>波多裡奇卓越績效準則</p></td>
<td><p>戴明獎申請指南</p></td>
<td><p>EFQM卓越模式</p></td>
</tr>
<tr class="odd">
<td><p>評價方法</p></td>
<td><p>過程採用ADLI</p>
<p>結果採用LeTCI</p></td>
<td><p>對“基本要求”、“卓越的TQM活動”和“高層領導的作用”獨立評價</p></td>
<td><p>RADAR邏輯</p></td>
</tr>
</tbody>
</table>

隨著全面品質管制理論的發展，如今越來越多的品質管制優化理論出現，越來越多的高校也開展了品質管制課程。如[哈佛商學院](../Page/哈佛商學院.md "wikilink")，杜克大學富卡商學院，哥倫比亞大學商學院，[倫敦政經學院](../Page/倫敦政治經濟學院.md "wikilink")，[墨爾本大學](../Page/墨爾本大學.md "wikilink")，[昆士蘭大學](../Page/昆士蘭大學.md "wikilink")，[馬德里理工大學UPM](../Page/馬德里理工大學.md "wikilink")，[華中科技大學](../Page/華中科技大學.md "wikilink")\[4\]，[四川大學](../Page/四川大學.md "wikilink")\[5\]等。

而與此同時，越來越多的全面品質理論教學工作也被教學工作者從各個容易理解的角度詮釋給企業。如Joseph Juran的《品質三部曲》，Guy
Kawasaki在非營利組織[Ted上總結的創新的藝術](../Page/TED大會.md "wikilink")，[馬德里理工大學教授Alfonso](../Page/馬德里理工大學.md "wikilink")
Casa\[6\]l提出的QC100品質管制理論，[上海汽車工業總公司的](../Page/上汽集團.md "wikilink")[胡茂元提出的](../Page/胡茂元.md "wikilink")“零缺陷”生產管理模式、“一體化管理”非核心業務管理模式\[7\]等。

## 註腳

## 外部來源

  - [TQM-全面品質管理發展中心](https://web.archive.org/web/20110829055227/http://www.tqc.com.hk/big5/consulting/tqm.htm)

[Category:品質](../Category/品質.md "wikilink")
[Category:商業術語](../Category/商業術語.md "wikilink")
[Category:組織](../Category/組織.md "wikilink")

1.
2.
3.
4.
5.
6.
7.