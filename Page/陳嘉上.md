**陳嘉上**（，），[香港](../Page/香港.md "wikilink")[電影](../Page/電影.md "wikilink")[導演及](../Page/導演.md "wikilink")[編劇](../Page/編劇.md "wikilink")。[廣東人](../Page/廣東.md "wikilink")。1980年加入[邵氏電影公司工作](../Page/邵氏電影公司.md "wikilink")，1983年首次擔任編劇，1988年首次執導自己編劇的[香港電影](../Page/香港電影.md "wikilink")《[三人世界](../Page/三人世界.md "wikilink")》\[1\]。憑《[野獸刑警](../Page/野獸刑警.md "wikilink")》奪得[第18屆香港電影金像獎最佳導演及最佳編劇](../Page/1999年香港電影金像獎.md "wikilink")。2007年接替[許鞍華任](../Page/許鞍華.md "wikilink")[香港電影導演協會會長](../Page/香港電影導演協會.md "wikilink")，前任香港電影金像獎董事局主席。

陳嘉上前妻為前演員[陳慧儀](../Page/陳慧儀.md "wikilink")，兩人於拍攝電影[小男人週記時認識](../Page/小男人週記.md "wikilink")。

## 簡歷

**陳嘉上**畢業於[聖若瑟英文中學](../Page/聖若瑟英文中學.md "wikilink")，於[加拿大修讀城市地理](../Page/加拿大.md "wikilink")，回港後，看了[章國明導演的](../Page/章國明.md "wikilink")《[邊緣人](../Page/邊緣人.md "wikilink")》，深受該電影的創意及深度感染，應徵[邵氏電影公司的助理製片](../Page/邵氏電影公司.md "wikilink")，但發現所負責的是管理工作而非拍攝工作，於是在一年後轉職負責特技道具。後來邵氏電影公司精簡架構，陳嘉上不忍裁走下屬，於是自己請辭。輾轉之下跟[黃泰來導演擔任場記](../Page/黃泰來.md "wikilink")，後來被黃導演臨時拉夫去寫劇本，從此踏上編劇之路。

後來，陳嘉上所寫的《[三人世界](../Page/三人世界.md "wikilink")》劇本沒有導演肯接拍，結果著名製作人[冼杞然](../Page/冼杞然.md "wikilink")、[香港著名媒體人](../Page/香港.md "wikilink")[甘國亮及該劇主角](../Page/甘國亮.md "wikilink")[林子祥建議由](../Page/林子祥.md "wikilink")**陳嘉上**親自執導，因此開始了導演生涯。《[三人世界](../Page/三人世界.md "wikilink")》由[香港電影金像獎和](../Page/香港電影金像獎.md "wikilink")[台灣](../Page/台灣.md "wikilink")[金馬獎雙料影后](../Page/金馬獎.md "wikilink")[鄭裕玲主演](../Page/鄭裕玲.md "wikilink")，同劇還有[林子祥和](../Page/林子祥.md "wikilink")[周慧敏演出](../Page/周慧敏.md "wikilink")。\[2\]

## 電影作品

### 執導作品

  - [神秘寶藏](../Page/神秘寶藏.md "wikilink")
    (未上映)（與[鄭中基](../Page/鄭中基.md "wikilink")[聯合導演](../Page/聯合導演.md "wikilink"))
  - [蕩寇風雲](../Page/蕩寇風雲.md "wikilink") (2017)
  - [四大名捕大结局](../Page/四大名捕大结局.md "wikilink") (2014)
  - [四大名捕II](../Page/四大名捕II.md "wikilink") (2013)
  - [奇幻夜](../Page/奇幻夜.md "wikilink")
    (2013)（與[劉國昌](../Page/劉國昌.md "wikilink")、[泰迪羅賓](../Page/泰迪羅賓.md "wikilink")[聯合導演](../Page/聯合導演.md "wikilink"))
  - [四大名捕](../Page/四大名捕_\(电影\).md "wikilink")
    (2012)（与[秦小珍](../Page/秦小珍.md "wikilink")[聯合導演](../Page/聯合導演.md "wikilink")）
  - [画壁](../Page/画壁.md "wikilink") (2011)
  - [格鬥天王](../Page/:en:The_King_of_Fighters_\(film\).md "wikilink")
    (2010)
  - [畫皮](../Page/畫皮_\(2008年電影\).md "wikilink") (2008)
  - [三分鐘先生](../Page/三分鐘先生.md "wikilink") (2006)
  - [至尊無賴](../Page/至尊無賴.md "wikilink") (2006)
  - [A-1頭條](../Page/A-1頭條.md "wikilink") (2004)
  - [飛龍再生](../Page/飛龍再生.md "wikilink") (2003)
  - [老鼠愛上貓](../Page/老鼠愛上貓.md "wikilink") (2003)
  - [戀戰沖繩](../Page/戀戰沖繩.md "wikilink") (2000)
  - [公元2000](../Page/公元2000.md "wikilink") (2000)
  - [野獸刑警](../Page/野獸刑警.md "wikilink") (1998)
  - [天地雄心](../Page/天地雄心.md "wikilink") (1997)
  - [飛虎](../Page/飛虎_\(電影\).md "wikilink") (1996)
    (獲[香港電影評論學會大獎最佳導演](../Page/香港電影評論學會大獎.md "wikilink"))
  - [霹靂火](../Page/霹靂火_\(1995年電影\).md "wikilink") (1995)
  - [錦繡前程](../Page/錦繡前程.md "wikilink") (1994)
  - [精武英雄](../Page/精武英雄.md "wikilink") (1994)
  - [飛虎雄心](../Page/飛虎雄心.md "wikilink") (1994)
  - [武狀元蘇乞兒](../Page/武狀元蘇乞兒.md "wikilink") (1992)
  - [機Boy小子之真假威龍](../Page/機Boy小子之真假威龍.md "wikilink") (1992)
  - [逃學威龍2](../Page/逃學威龍2.md "wikilink") (1992)
  - [逃學威龍](../Page/逃學威龍.md "wikilink") (1991)
  - [神探馬如龍](../Page/神探馬如龍.md "wikilink") (1991)
  - [小男人週記 II 錯在新宿](../Page/小男人週記_II_錯在新宿.md "wikilink") (1990)
  - [小男人周記](../Page/小男人周記.md "wikilink") (1989)
  - [三人世界](../Page/三人世界.md "wikilink") (1987)

### 監製作品

  - [臥虎潛龍](../Page/臥虎潛龍.md "wikilink") (待上映)
  - [三分鐘先生](../Page/三分鐘先生.md "wikilink") (2006)
  - [飛虎雄師](../Page/飛虎雄師.md "wikilink") (2002)
  - [二人三足](../Page/二人三足.md "wikilink") (2002)
  - [常在我心](../Page/常在我心.md "wikilink") (2001)
  - [完美情人](../Page/完美情人.md "wikilink") (2001)
  - [天旋地戀](../Page/天旋地戀.md "wikilink") (1999)
  - [殺手之王](../Page/殺手之王.md "wikilink") (1998)
  - [天地雄心](../Page/天地雄心.md "wikilink") (1997)
  - [G4特工](../Page/G4特工.md "wikilink") (1997)
  - [一千零一夜之夢中人](../Page/一千零一夜之夢中人.md "wikilink") (1995)
  - [風塵三俠](../Page/風塵三俠.md "wikilink") (1992)
  - [京城81號2](../Page/京城81號2.md "wikilink") (2017)
  - [鮫珠傳](../Page/鮫珠傳.md "wikilink") (2017)

### 編劇作品

  - [四大名捕](../Page/四大名捕.md "wikilink") (2012)
  - [畫壁](../Page/畫壁.md "wikilink") (2011)
  - [精武風雲：陳真](../Page/精武風雲：陳真.md "wikilink") (2010)
  - [畫皮](../Page/畫皮.md "wikilink") (2008)
  - [三分鐘先生](../Page/三分鐘先生.md "wikilink") (2006)
  - [至尊無賴](../Page/至尊無賴.md "wikilink") (2006)
  - [A-1頭條](../Page/A-1頭條.md "wikilink") (2004)
    (獲[香港電影評論學會大獎最佳編劇](../Page/香港電影評論學會大獎.md "wikilink"))
  - [飛龍再生](../Page/飛龍再生.md "wikilink") (2003)
  - [老鼠愛上貓](../Page/老鼠愛上貓.md "wikilink") (2002)
  - [戀戰沖繩](../Page/戀戰沖繩.md "wikilink")(2000)
  - [野獸刑警](../Page/野獸刑警.md "wikilink") (1998)
  - [飛虎](../Page/飛虎_\(電影\).md "wikilink") (1996)
  - [錦繡前程](../Page/錦繡前程.md "wikilink") (1994)
  - [飛虎雄心](../Page/飛虎雄心.md "wikilink") (1994)
  - [中南海保鏢](../Page/中南海保鏢.md "wikilink") (1994)
  - [逃學威龍2](../Page/逃學威龍2.md "wikilink") (1992)
  - [92神鵰俠侶之痴心情長劍](../Page/92神鵰俠侶之痴心情長劍.md "wikilink") (1992)
  - [衛斯理之老貓](../Page/衛斯理之老貓.md "wikilink") (1992)
  - [逃學威龍](../Page/逃學威龍.md "wikilink") (1991)
  - [豪門夜宴](../Page/豪門夜宴_\(1991年電影\).md "wikilink") (1991)
  - [城市特警](../Page/城市特警.md "wikilink") (1988)
  - [靚妹正傳](../Page/靚妹正傳.md "wikilink") (1987)

### 演出

  - [我要成名](../Page/我要成名.md "wikilink") (2006) \[客串\]
  - [雙龍會](../Page/雙龍會.md "wikilink") (1992) \[客串\]
  - [城市特警](../Page/城市特警.md "wikilink") (1988)
  - [雙肥臨門](../Page/雙肥臨門.md "wikilink") (1988) 飾搶手袋賊

## 參考來源

<div class="references-small">

<references />

</div>

## 外部連結

  - [中文電影資料庫 -
    陳嘉上](http://www.dianying.com/ft/person.php?personid=ChenJiashang/)

  - [2打 6地盤 - 陳嘉上網誌](http://gordonchanks.blogspot.com/)

  -
[Category:香港电影導演](../Category/香港电影導演.md "wikilink")
[Category:香港编剧](../Category/香港编剧.md "wikilink")
[Category:香港电影监制](../Category/香港电影监制.md "wikilink")
[Category:聖若瑟英文中學校友](../Category/聖若瑟英文中學校友.md "wikilink")
[Category:多伦多大学校友](../Category/多伦多大学校友.md "wikilink")
[J嘉](../Category/陳姓.md "wikilink")
[Category:香港電影金像獎最佳編劇得主](../Category/香港電影金像獎最佳編劇得主.md "wikilink")
[Category:香港電影評論學會大獎最佳導演得主](../Category/香港電影評論學會大獎最佳導演得主.md "wikilink")
[Category:香港電影金紫荊獎最佳導演得主](../Category/香港電影金紫荊獎最佳導演得主.md "wikilink")
[Category:香港電影金像獎協會主席](../Category/香港電影金像獎協會主席.md "wikilink")

1.
2.  香港樹仁大學每週名人講座，2007年4月17日