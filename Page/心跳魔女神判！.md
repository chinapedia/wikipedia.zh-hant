《**心跳魔女神判！**》（原題為「心跳[魔女裁判](../Page/魔女裁判.md "wikilink")」\[1\]）是[SNK
Playmore於](../Page/SNK_Playmore.md "wikilink")2007年7月5日在[任天堂DS平台推出的](../Page/任天堂DS.md "wikilink")[冒險遊戲](../Page/冒險遊戲.md "wikilink")。以「色色的玩法」為賣點。續篇《**心跳魔女神判！2**》於2008年7月31日發售。第三部作品《**どき魔女ぷらす**》则於2009年7月30日發售。

## 遊戲簡介

故事主要為不良少年西村惡兒\[2\]被[天使露露拜託](../Page/天使.md "wikilink")，要找出在隱藏在星嶺鷹守學園（）的[魔女](../Page/魔女.md "wikilink")。然而，魔女和普通人類在外貌上沒有甚麼分別，但當魔女在臉紅心跳的時候身體上就會浮現出紋章。主角要想盡辦法接近目標，並在她臉紅心跳時觸摸身體而分辨是否魔女。

## 遊戲系統

平時是處於冒險模式，發現有魔女嫌疑的目標則會轉移到「魔女檢查模式」，邊用NDS的觸控筆碰觸身體邊對目標質問。但當目標有反抗的話會轉為「魔法戰鬥模式」，勝利後轉為「魔女檢查模式」，另外，可命令曾打敗的魔女嫌疑者協助戰鬥。

## 「色色的遊戲」

正因為「碰觸身體」這特別的系統頗有[十八禁遊戲的影子](../Page/十八禁遊戲.md "wikilink")，不論日本國內國外，這遊戲也被評為「色色的遊戲」\[3\]。而且[第一版遊戲官方網站](http://www.dokimajo.com/dkmj01/index.html)的首頁亦貌像含有不良資訊網站的年齡確認頁面（但實際上只是問瀏覽者「是否壞人」）。另外，此作的廣告亦有十分誇張的[乳搖](../Page/乳搖.md "wikilink")，然而實際進行遊戲中想享受乳搖功能是一項高難度作業，好感度不達至最高是沒法啟動此功能的。

## 人物介紹

  -
    遊戲主角（玩家）。星嶺鷹守學園中等部學生，志願是做個「超壞」的人。但在某一天，被天使露露威脅要幫忙尋找魔女，否則會被她變為「善人」。在迫不得已的情況下服從她。
  -
    性格[腹黑](../Page/腹黑.md "wikilink")（笑裡藏刀）的見習天使。某一天被[大天使命令到人間尋找魔女](../Page/天使長.md "wikilink")，從而威脅要惡事幫忙。
    主角西村 惡兒（玩家）初期拍檔，能使出天使劍攻擊，可以作出物理性傷害或把魔法彈打回頭，另外蓄力後也可以施展遠距離攻擊。

### 魔女嫌疑者

  -
    14歲的[雙馬尾女孩](../Page/雙馬尾.md "wikilink")。[啦啦隊成員](../Page/競技啦啦隊.md "wikilink")，喜歡幫助有困難的人。有二段變身能力，平時為[蘿莉外表](../Page/蘿莉.md "wikilink")，可以變成[巨乳啦啦隊少女](../Page/巨乳.md "wikilink")。第一話的攻略對象，以魔術球作出攻擊。
    以露露直覺判斷魔女的可能性為50%。
    魔女印記在右大腿，成為玩家一方後可以放出魔術球作遠距離攻擊。
  -
    14歲，[戴眼鏡](../Page/眼鏡娘.md "wikilink")。喜歡研究[神秘現象和閱讀月刊](../Page/神秘現象.md "wikilink")《NU-》，是神秘現象研究俱樂部的成員。第二話的攻略目標，是主角同級的委員長，相信世上存在著惡兒所說的魔女，但是並非魔女，所以沒有魔女印記。
    以露露直覺判斷魔女的可能性為80%。
    主要使用盛有[化學物質的藥瓶子作出攻擊](../Page/化學物質.md "wikilink")，以及用人牌掩護。成為玩家一方後可以扔出阻礙對手行動的化學藥瓶。
  -
    喜歡玩[懷舊遊戲的](../Page/懷舊遊戲.md "wikilink")13歲女孩。寶物是[FC遊戲機的](../Page/FC遊戲機.md "wikilink")《[Athena](../Page/Athena.md "wikilink")》，經常帶著一部NGP的舊式遊戲機。在露露的直覺中嫌疑最低。第三話攻略對象，不過只會瞬間轉移，戰鬥能力低下。
    以露露直覺判斷魔女的可能性為30%。
    魔女印記在額頭，成為玩家一方後會提供防禦牆，能把魔術球之類的攻擊絕對反彈。
  -
    [神社中](../Page/神社.md "wikilink")15歲的[巫女](../Page/巫女.md "wikilink")，也是[獨生女](../Page/獨生子女.md "wikilink")，為了保持自身的靈力所以不太多與他人交談。第四話攻略目標，與主角同級，放學後會在[女僕咖啡店工作](../Page/女僕咖啡店.md "wikilink")。戰鬥力略高，會以封印符防衛及召喚[式神吐火球攻擊](../Page/式神.md "wikilink")。
    以露露直覺判斷魔女的可能性為45%。
    魔女印記在左肩，成為玩家一方後可召喚式神施放連鎖火球。
  -
    13歲的病弱少年。雖然外表看似女生而被懷疑是魔女，但其實是個男孩。非常直率，凡事思索得太深太認真的男孩子，身子虛弱所以經常受傷到醫院去。第五話攻略目標，具有變成[狼男的能力](../Page/狼男.md "wikilink")，戰鬥時也是以爪攻。
    以露露直覺判斷魔女的可能性為40%。
    印記於左腰，加入玩家一方後可使出狼爪攻擊。
  -
    總是穿著兔子[布偶衣的](../Page/布偶衣.md "wikilink")12歲怪女孩。布偶衣研究會成員。經常穿著兔布偶衣的女生，平時性格悠閒自在，但是她發怒的時候會發生可怕的事情。個性腹黑，很難相處，所以周圍的人都格外小心地對待她。
    以露露直覺判斷魔女的可能性為70%。
    印記於腹部，加入玩家一方後可使出隕石攻擊及分身兔子防禦。
  -
    23歲。雖然是校醫，但也可以隨時代替其他老師授課。身材惹火，但毫無自覺。雖然在校任職時間不長，但在校內威望很高，在學生中的人氣也很高。真實身份為[熾天使聖夜諾艾爾之姐](../Page/熾天使.md "wikilink")。
    以露露直覺判斷魔女的可能性為99%。
    印記在左胸，加入玩家一方後可使出閃電攻擊。
  -
    年齡不詳，遊戲最後Boss，平常偽裝成圖書館管理員潛伏在學校，真實身份為最高級的熾天使。
    與姐姐聖夜伊芙個性完全相反，非常傲慢，對於違反圖書館守則的學生絕不手軟。
    似乎對姐姐抱有特殊感情。

### 其他

  -
    身體上黑白的地方[熊貓相反的怪熊貓](../Page/熊貓.md "wikilink")，與魔穗相熟。
  -
    啦啦隊成員。

## 漫畫

[秋田書店](../Page/秋田書店.md "wikilink")《[CHAMPION
RED](../Page/CHAMPION_RED.md "wikilink")》2007年9月號（2007年7月19日）發售）開始連載。[八神健繪畫](../Page/八神健.md "wikilink")。

  - どきどき魔女神判\!（チャンピオンREDコミックス）

<!-- end list -->

1.  ISBN 9784253232128
2.  ISBN 9784253232135

<!-- end list -->

  - どきどき魔女神判2 DUO（チャンピオンREDコミックス）

<!-- end list -->

1.  ISBN 9784253234412
2.  ISBN 9784253234429

## 相關產品

[DokidokiMajoShinpanFanbook.jpg](https://zh.wikipedia.org/wiki/File:DokidokiMajoShinpanFanbook.jpg "fig:DokidokiMajoShinpanFanbook.jpg")

  - （48頁，2007年7月5日發售，ISBN 4797343478、ISBN 978-4797343472）

  - 心跳魔女神判！[原聲帶](../Page/原聲帶.md "wikilink")（2007年7月25日發售）

## 註釋

<div class="references-small">

<references />

</div>

## 參考資料

<div class="references-small">

1.  [遊戲官方網站](http://www.dokimajo.com/dkmj01/index.html)
2.  [触摸女孩的身体！《心跳魔女神判》画面](https://web.archive.org/web/20070611195458/http://digi.intozgc.com/125/125850.html)
3.  [Amazon -
    心跳魔女神判！原聲帶](http://www.amazon.co.jp/exec/obidos/ASIN/B000RO53S8)

</div>

## 外部連結

  - [遊戲官方網站](http://www.dokimajo.com/) -
    日文以外的語言版本的翻譯接近[機器翻譯](../Page/機器翻譯.md "wikilink")，例如英文版的「Are
    you bad person?」，正確是「Are you **a** bad person?」。

  - [Fanbook資料](https://web.archive.org/web/20070811020919/http://game.snkplaymore.co.jp/goods/goods34.php)

  - [心跳魔女神判！Wiki](https://archive.is/20130430142925/http://hyaa.moero.info/index.php?FrontPage)

[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[Category:SNK游戏](../Category/SNK游戏.md "wikilink")
[Category:任天堂DS遊戲](../Category/任天堂DS遊戲.md "wikilink")
[Category:任天堂DS独占游戏](../Category/任天堂DS独占游戏.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:魔法少女題材遊戲](../Category/魔法少女題材遊戲.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink") [Category:Champion
RED](../Category/Champion_RED.md "wikilink")

1.  [「心跳魔女裁判（暫定）」等DS原創作品陸續登場](http://www.watch.impress.co.jp/game/docs/20060924/snk.htm)

2.  中文版官網的「惡倪」是錯譯。
3.  [工口无限！NDS《魔女裁判》摸遍全身](http://www.pcgames.com.cn/tvgames/bao/games/0610/826146.html)