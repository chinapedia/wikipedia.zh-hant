《**FNN Super
Time**》（）是[富士電視台系](../Page/富士電視台.md "wikilink")（[FNN](../Page/FNN.md "wikilink")）自1984年10月1日開始到1997年3月28日期間（周末為1985年4月6日到1997年3月30日）播出的傍晚[新聞節目](../Page/新聞節目.md "wikilink")。該節目是日本第一個主持人采用站立方式播報新聞的節目。

[Category:富士電視台新聞節目](../Category/富士電視台新聞節目.md "wikilink")
[Category:1984年日本電視節目](../Category/1984年日本電視節目.md "wikilink")