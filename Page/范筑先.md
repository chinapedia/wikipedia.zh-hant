**范筑先**（），原名-{范}-金标，[字夺魁](../Page/表字.md "wikilink")，[中国](../Page/中国.md "wikilink")[山东省](../Page/山东省.md "wikilink")[馆陶县](../Page/馆陶县.md "wikilink")（现属[河北省](../Page/河北省.md "wikilink")）人，著名[抗日将领](../Page/抗日战争_\(中国\).md "wikilink")。

范1904年因家乡[卫河水灾而被迫投军](../Page/卫河.md "wikilink")，加入[北洋军](../Page/北洋军.md "wikilink")[第四镇](../Page/北洋陆军第四师.md "wikilink")，后被选拔入[天津北洋陆军讲武堂](../Page/天津北洋陆军讲武堂.md "wikilink")[炮兵科学习](../Page/炮兵.md "wikilink")，毕业后任炮兵连长。[中华民国建国后](../Page/中华民国.md "wikilink")，范加入[国民党](../Page/国民党.md "wikilink")，但仍服役于北洋军中，被逐级提拔为第四师参谋长，属于[皖系浙江都督](../Page/皖系.md "wikilink")[卢永祥和第四师师长](../Page/卢永祥.md "wikilink")[陈乐山麾下](../Page/陈乐山.md "wikilink")。

1924年9月，[江浙战争爆发后](../Page/江浙战争.md "wikilink")，[孙传芳乘机自](../Page/孙传芳.md "wikilink")[福建率军夺取浙江](../Page/福建.md "wikilink")，收编第四师，任命范为第八旅旅长。1925年孙传芳对第四师进行改编，范被撤去军职，隐居[上海](../Page/上海.md "wikilink")，后回乡并改名“竹仙”。

1926年，范在[张维玺的游说下投入](../Page/张维玺.md "wikilink")[冯玉祥麾下](../Page/冯玉祥.md "wikilink")，再度改名为“筑先”，参与[北伐战争](../Page/北伐战争.md "wikilink")，任高级参议。[中原大战时](../Page/中原大战.md "wikilink")，范任冯玉祥讨蒋南路军（即张维玺部）总司令部参议。战败后，范决意脱离军界，不同张维玺退兵[陕西](../Page/陕西.md "wikilink")，而是投奔[韩复榘](../Page/韩复榘.md "wikilink")，任参议。

1933年，范被任命为[沂水县县长](../Page/沂水县.md "wikilink")，同年又被调任[临沂县长](../Page/临沂.md "wikilink")，为政其间多有清名。1936年冬，范升任山东省第六区行政督察专员、保安司令兼[聊城县长](../Page/聊城.md "wikilink")。

1937年抗战爆发前夕，[中国共产党特派](../Page/中国共产党.md "wikilink")[彭雪枫对范进行游说](../Page/彭雪枫.md "wikilink")，争取其秘密支持。抗战爆发后，范在中共支持下，拒绝韩复榘的撤退令，于11月19日发出“皓电”，通电全国，誓与日军在[黄河以北抗战到底](../Page/黄河.md "wikilink")。很快，范在中共的协助下掌握了鲁西北二十多个县的政权，并对各路武装力量进行收编，其兵力达到2万余人，被[国民政府委任为山东省六区游击司令](../Page/国民政府.md "wikilink")。

1938年3月，为策应[徐州会战](../Page/徐州会战.md "wikilink")，范率部在[濮县](../Page/濮县.md "wikilink")、[范县等地对](../Page/范县.md "wikilink")[日军](../Page/日军.md "wikilink")[土肥原贤二部进行了阻击](../Page/土肥原贤二.md "wikilink")，迟滞了其南渡黄河。在[武汉会战期间](../Page/武汉会战.md "wikilink")，范更是全军而出，对被日军占领的[济南展开攻势](../Page/济南.md "wikilink")，一度攻占济南市郊机场。但是在日军的反扑下，范部也遭到很大损失，其次子阵亡。

为报复范发动的济南战斗，日军在1938年11月对范的军事中心聊城发动攻势。范亲自布置放弃聊城，希望能在[八路军](../Page/八路军.md "wikilink")[129师支援下对日军实施反包围](../Page/129师.md "wikilink")。但是11月14日时，由于需要接待[鲁西行署主任](../Page/鲁西行署.md "wikilink")[李树春来访](../Page/李树春.md "wikilink")，范比预定计划推迟两小时撤离聊城，结果其所部700余人被日军千叶联队1000余人包围在聊城，经过一昼夜苦战，范全军覆没，本人亦阵亡。

[Category:中華民國大陸時期軍事人物](../Category/中華民國大陸時期軍事人物.md "wikilink")
[Category:中国抗日战争牺牲者](../Category/中国抗日战争牺牲者.md "wikilink")
[Category:馆陶人](../Category/馆陶人.md "wikilink")
[C](../Category/范姓.md "wikilink")