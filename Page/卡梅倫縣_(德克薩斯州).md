**卡梅倫縣**（英語：Cameron
County）是[美國](../Page/美國.md "wikilink")[德克薩斯州最南部的一個縣](../Page/德克薩斯州.md "wikilink")，[格蘭德河在南界](../Page/格蘭德河.md "wikilink")（同時是[美國和墨西哥的邊界](../Page/美墨邊界.md "wikilink")）注入[墨西哥灣](../Page/墨西哥灣.md "wikilink")。面積3,306平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口335,227人。縣治[布朗斯維爾](../Page/布朗斯維爾_\(德克薩斯州\).md "wikilink")（Brownsville）。

成立於1848年2月12日，縣政府成立於9月11日。縣名紀念[美墨戰爭時期的軍人Ewen](../Page/美墨戰爭.md "wikilink")
Cameron[上校](../Page/上校.md "wikilink")\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[C](../Category/得克萨斯州行政区划.md "wikilink")

1.  [DeWitt Colony Militia
    Captains](http://www.tamu.edu/ccbn/dewitt/captains.htm#cameron)