**李師師**，生卒不详，[北宋角妓](../Page/北宋.md "wikilink")（才貌出眾的名藝妓、[交際花](../Page/交際花.md "wikilink")，與賣淫為主的色妓有別）\[1\]，[汴京](../Page/汴京.md "wikilink")（今[中国](../Page/中国.md "wikilink")[河南省](../Page/河南省.md "wikilink")[开封市](../Page/开封市.md "wikilink")）人。事迹多见于野史、小说。小說《[水滸傳](../Page/水滸傳.md "wikilink")》对她有过描写。

## 生平

汴京东二厢永庆坊柒\[2\]局匠[王寅之女](../Page/王寅.md "wikilink")\[3\]。四岁時，王寅犯罪下狱死，师师被[倡籍李姥收养](../Page/倡籍.md "wikilink")。據[張邦基](../Page/張邦基.md "wikilink")《[墨庄漫錄](../Page/墨庄漫錄.md "wikilink")》說：“[政和間](../Page/政和.md "wikilink")，李師師、[崔念奴二妓](../Page/崔念奴.md "wikilink")，名著一時”。可見政和年間（1111年-1118年），李師師已經走紅。當時，詩人[晁衝之正值年少](../Page/晁衝之.md "wikilink")，每有會飲，經常招她侑席。其後十餘年，衝之再來京師，李、崔兩人“聲名溢於中國”，只得寫了兩首詩“追往昔”。詩中描述李師師居所環境是“門侵楊柳垂珠箔，窗對櫻桃卷碧紗”，“係馬柳低當戶葉，迎人桃出隔墻花”。其詩以“看舞霓裳羽衣曲，聽歌玉樹後庭花”來形容師師的歌舞技藝。

靖康元年（1126年），宋钦宗下令籍没师师家。未久，汴京沦陷，北宋灭亡，师师下落不明。[张邦基](../Page/张邦基.md "wikilink")《墨庄漫录》称师师流落于江浙，已“憔悴无复向来之态矣”\[4\]，又一說流落於湖湘间\[5\]，或[臨安之說](../Page/临安府_\(浙江\).md "wikilink")\[6\]。

## 轶事

一說宋徽宗与词人[周邦彦](../Page/周邦彦.md "wikilink")，曾在李師師家相遇\[7\]。徽宗曾将其召入宫。

## 评价

  - [张先](../Page/张先.md "wikilink")《師師令》：“不须回扇障清歌，唇一点，小于花蕊。”
  - [晏几道](../Page/晏几道.md "wikilink")：“看遍颍川花，不及師師好。”
  - [南宋初年](../Page/南宋.md "wikilink")[朱敦儒有詩云](../Page/朱敦儒.md "wikilink")：“解唱《陽關》別調聲，前朝惟有李夫人”，說的就是李師師。

## 影視作品

### 電視劇

  - 1986年《盗日英雄传》新加坡製作30集電視劇（李秾飾）
  - 1986《[林沖](../Page/林冲_\(电视剧\).md "wikilink")》（[戚美珍飾](../Page/戚美珍.md "wikilink")）
  - 1989《[李师师](../Page/李师师_\(电视剧\).md "wikilink")》（[何晴飾](../Page/何晴.md "wikilink")）
  - 1990《[飛越官場](../Page/飛越官場.md "wikilink")》([謝寧飾](../Page/謝寧_\(香港演員\).md "wikilink"))
  - 1991《[一代名伎李師師](../Page/一代名伎李師師.md "wikilink")》（[張瑜飾](../Page/張瑜_\(演員\).md "wikilink")）
  - 1996《水浒传》（[何晴飾](../Page/何晴.md "wikilink")）
  - 2000《千禧金瓶梅》（飾）
  - 2004《[浪子燕青](../Page/浪子燕青.md "wikilink")》（[于娜飾](../Page/于娜.md "wikilink")）
  - 2011《[水浒传](../Page/水浒传_\(2011年电视剧\).md "wikilink")》（[安以軒飾](../Page/安以軒.md "wikilink")）
  - 2014《[神医安道全](../Page/神医安道全.md "wikilink")》（[刘妍希飾](../Page/刘妍希.md "wikilink")）
  - 未播《[清风明月佳人](../Page/清风明月佳人.md "wikilink")》（[马苏飾](../Page/马苏.md "wikilink")）

## 注釋

[Category:生年不詳](../Category/生年不詳.md "wikilink")
[Category:卒年不詳](../Category/卒年不詳.md "wikilink")
[S师师](../Category/李姓.md "wikilink")
[Category:北宋人](../Category/北宋人.md "wikilink")
[Category:开封人](../Category/开封人.md "wikilink")
[Category:中國下落不明者](../Category/中國下落不明者.md "wikilink")
[Category:中國傳統娼妓女樂](../Category/中國傳統娼妓女樂.md "wikilink")
[Category:水浒传人物](../Category/水浒传人物.md "wikilink")

1.  [孟元老](../Page/孟元老.md "wikilink")《[東京夢華錄](../Page/東京夢華錄.md "wikilink")》
2.  同「漆」
3.  无名氏《李师师传》
4.  张邦基《墨庄漫录》卷八
5.  宋代评话《宣和遗事》，亦有类似记载。但说其“后流落湖湘间（今[湘南一带](../Page/湘南.md "wikilink")），为商人所得”。
6.  陈忱《水浒后传》称其流落于临安（今[杭州](../Page/杭州.md "wikilink")）。
7.  [张端义](../Page/张端义.md "wikilink")《贵耳集》：“道君幸李师师家，偶周邦彦先在焉，知道君至，遂匿于床下。道君自携新橙一颗云：‘江南初进来。’遂与师师谑语，邦彦悉闻之，隐括成《少年游》云：‘并刀如水，吴盐胜雪，纤手破新橙。锦幄初温，兽香不断，相对坐调笙。低声问，向谁行宿？城上已三更。马滑霜浓，不如休去，直是少人行。’师师因歌此词。’”