**下花园区**是[中国](../Page/中华人民共和国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[张家口市下辖的一个](../Page/张家口市.md "wikilink")[区](../Page/市辖区.md "wikilink")。

## 概况

下花园区位于张家口市东南50公里处。地理坐标介于北纬40°29’、东经115°16’之间,东西横距23.2公里,南北纵距16.1公里,地处燕山山系之西端的黄羊山界,在宣化、怀来之间，涿鹿之北端，总面积315平方公里,境内“山、丘、川、滩”四大地貌共存，鸡鸣山、玉带山、燕洞山三山鼎力，遥相呼应；戴家营河、洋河、东河三河并流，汇流后入永定河。

## 名称由来

公元971年辽圣宗耶律隆绪时期，距今一千多年，中国著名少数民族女政治家萧绰（萧太后）见此地风景优美，建造了上、中、下三座皇家御花园，用来休闲和处理军国大事，下花园由此得名。

## 行政区划

下辖2个[街道办事处](../Page/街道办事处.md "wikilink")、4个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 参见

  - [孟家坟民宅](../Page/孟家坟民宅.md "wikilink")

## 外部链接

  - [下花园区人民政府](http://www.zjkxhy.gov.cn/)
  - [新华网_河北频道_政府群网_下花园](http://www.he.xinhuanet.com/zfwq/xiahuayuan/)

[下花园区](../Page/category:下花园区.md "wikilink")
[区](../Page/category:张家口区县.md "wikilink")
[张家口](../Page/category:河北市辖区.md "wikilink")

[冀](../Category/中华人民共和国第二批资源枯竭型城市.md "wikilink")