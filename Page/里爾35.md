**里爾35**（）及**里爾36**（）噴射機，為[里爾噴射機所出廠的小型](../Page/里爾噴射機.md "wikilink")[商務噴射機](../Page/商務噴射機.md "wikilink")，在1983年，[美國空軍訂購](../Page/美國空軍.md "wikilink")80架里爾35式噴射機A型，作為空軍的作業支援專機，軍方編號為**C-21**。[LR36-at-Macau.jpg](https://zh.wikipedia.org/wiki/File:LR36-at-Macau.jpg "fig:LR36-at-Macau.jpg")
[U-36a_03l.jpg](https://zh.wikipedia.org/wiki/File:U-36a_03l.jpg "fig:U-36a_03l.jpg")U-36A\]\]
里爾35/36型噴射機裝置兩具格萊特TFE731-2(Garrett
TFE731-2)噴射扇葉發動機，**里爾35型**及**里爾36型**幾乎是完全相同，里爾35型可搭載7名乘客，外加一名在[駕駛艙後面的觀察席次](../Page/駕駛艙.md "wikilink")，可達8名乘客。但為了達到最遠的航程，里爾36型，減少2個乘客座位，以裝置最大的機身油箱(Fuselage
Tank)。

TFE731-2是中[旁通比的](../Page/旁通比.md "wikilink")[噴射發動機](../Page/噴射發動機.md "wikilink")，每具可產3500磅的推力。里爾35/36型噴射機的飛行控制面，是由傳統的鋼纜所控制，而[襟翼及](../Page/襟翼.md "wikilink")[起落架則為液壓所收放](../Page/起落架.md "wikilink")。鼻輪轉向則為電動的[伺服制動器](../Page/伺服.md "wikilink")。[LR-36-TIP-TANK.jpg](https://zh.wikipedia.org/wiki/File:LR-36-TIP-TANK.jpg "fig:LR-36-TIP-TANK.jpg")

**里爾35**及**里爾36**的配備包括[自動駕駛](../Page/自動駕駛.md "wikilink")、[航空氣象雷達](../Page/航空氣象雷達.md "wikilink")，可加裝[電子飛行儀表系統](../Page/電子飛行儀表系統.md "wikilink")
(**EFIS**)。

## 歷史

原有[里爾25為裝置](../Page/里爾25.md "wikilink")[奇異電氣](../Page/奇異電氣.md "wikilink")
CJ
610[噴射發動機](../Page/噴射發動機.md "wikilink")，里爾35是由[里爾25改裝格萊特TFE](../Page/里爾25.md "wikilink")731-2(Garrett
TFE731-2)噴射扇葉發動機，原有里爾35噴射機的機型編號為Learjet
25BGF(GF為格萊特噴射扇葉發動機之意)，原型機由1971年5月份試飛。除了裝置噴射扇葉發動機所提升的效率及噪音的減少。里爾噴射機公司持續改進其它系統，最後則更改機種為里爾35。

**里爾35**及**里爾36**的生產，到1994年結束。

## 性能

## 参考文献

## 外部链接

  - [Detailed NTSB slide presentation of results of the Payne Stewart
    crash](http://www.ntsb.gov/Events/2000/aberdeen/ppt_presentations.htm)

[category:美國軍用飛機](../Page/category:美國軍用飛機.md "wikilink")
[category:公務機](../Page/category:公務機.md "wikilink")

[Category:軍用飛機](../Category/軍用飛機.md "wikilink")
[Category:雙發噴射機](../Category/雙發噴射機.md "wikilink")