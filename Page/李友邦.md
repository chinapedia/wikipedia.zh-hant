[李氏古厝.JPG](https://zh.wikipedia.org/wiki/File:李氏古厝.JPG "fig:李氏古厝.JPG")[李氏古宅](../Page/蘆洲李宅.md "wikilink")（李友邦紀念館）\]\]
**李友邦**（），本名**李肇基**，[祖籍](../Page/祖籍.md "wikilink")[福建](../Page/福建.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[同安縣](../Page/同安縣.md "wikilink")，生於[臺北廳](../Page/臺北廳.md "wikilink")[芝蘭二堡和尚洲於](../Page/芝蘭二堡.md "wikilink")[蘆洲李宅](../Page/蘆洲李宅.md "wikilink")（今[新北市](../Page/新北市.md "wikilink")[蘆洲區](../Page/蘆洲區.md "wikilink")），[日治時代](../Page/台灣日治時期.md "wikilink")[抗日事發](../Page/抗日.md "wikilink")，到[中國大陸](../Page/中國大陸.md "wikilink")，就讀於[黃埔軍校](../Page/黃埔軍校.md "wikilink")，1945年[日本投降](../Page/日本投降.md "wikilink")[二次大戰結束後](../Page/二次大戰.md "wikilink")，李友邦以台灣義勇軍中將司令頭銜率領隊員光榮回台。在1952年，被以涉及[朱諶之](../Page/朱諶之.md "wikilink")[匪諜案的罪名](../Page/中國共產黨台灣省工作委員會.md "wikilink")，遭[中華民國政府](../Page/蔣中正政府.md "wikilink")[處死](../Page/處死.md "wikilink")。\[1\]

## 生平

[李友邦.jpg](https://zh.wikipedia.org/wiki/File:李友邦.jpg "fig:李友邦.jpg")

李友邦[祖籍](../Page/祖籍.md "wikilink")[福建](../Page/福建.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[同安縣](../Page/同安縣.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[台灣](../Page/台灣.md "wikilink")[臺北](../Page/臺北.md "wikilink")，生於[蘆洲李宅](../Page/蘆洲李宅.md "wikilink")，於1921年（[大正十年](../Page/大正.md "wikilink")）參加[台灣文化協會](../Page/台灣文化協會.md "wikilink")，大正十三年（1924年）在[台北師範學校](../Page/台北師範學校.md "wikilink")（現[國立台北教育大學](../Page/國立台北教育大學.md "wikilink")）就讀時曾組織夜襲[日本警察](../Page/日本警察.md "wikilink")[派出所](../Page/派出所.md "wikilink")，遭日本警察通緝，因此潛逃中國大陸[廣東](../Page/廣東.md "wikilink")[廣州](../Page/廣州.md "wikilink")，進入[黃埔軍校第二期](../Page/黃埔軍校.md "wikilink")，加入國民黨，並且受當時國民黨負責[僑務的](../Page/僑務.md "wikilink")[廖仲愷栽培](../Page/廖仲愷.md "wikilink")，李氏從此左傾並與中共關係密切\[2\]。在校期間（1924年，一說1926年）成立「[台灣獨立革命黨](../Page/台灣獨立革命黨.md "wikilink")」，後來又參加「[廣東台灣學生聯合會](../Page/廣東台灣學生聯合會.md "wikilink")」、[廣東臺灣革命青年團等組織](../Page/廣東臺灣革命青年團.md "wikilink")，並與[中國共產黨有所往來](../Page/中國共產黨.md "wikilink")。

民國十六年（1927年），因[蔣介石進行](../Page/蔣介石.md "wikilink")[清黨](../Page/清黨.md "wikilink")，李友邦潛赴[杭州](../Page/杭州.md "wikilink")，祕密從事[革命活動](../Page/革命.md "wikilink")。因結交許多[左翼人士](../Page/左翼.md "wikilink")，於民國二十一年（1932年）被國民黨逮捕入獄，至[西安事變第二次](../Page/西安事變.md "wikilink")[國共合作後才正式被釋放](../Page/國共合作.md "wikilink")。民國二十六年（1937年）[中日戰爭爆發後](../Page/中日戰爭.md "wikilink")，李友邦在[金華縣著手恢復](../Page/金華縣.md "wikilink")[台灣獨立革命黨](../Page/台灣獨立革命黨.md "wikilink")，黨綱宗旨為：「驅除日本帝國主義在台灣一切勢力；在國家關係上，脫離其統治，而返歸祖國，以共同建立三民主義之新國家。」\[3\]，並前往[福建](../Page/福建.md "wikilink")[崇安縣招募台灣同鄉組織抗日武裝力量](../Page/崇安縣.md "wikilink")，後經國民政府軍事委員會政治部批准，民國二十八年（1939年）2月「[台灣義勇隊](../Page/台灣義勇隊.md "wikilink")」在[浙江省金華正式成立](../Page/浙江省.md "wikilink")，6名兒童也組成「[台灣義勇隊少年團](../Page/台灣義勇隊少年團.md "wikilink")」，李友邦為義勇隊隊長，

1938年9月李友邦對[台灣獨立革命黨發表](../Page/台灣獨立革命黨.md "wikilink")〈中國抗戰與台灣革命〉演說；

由上可知，對李友邦來說，中國是飽受帝國主義統治之被壓迫民族的革命柱石，尤其對台灣而言更是祖國。\[4\]

李友邦利用機會宣傳；「台灣人要自由獨立，也正如中國爭取民族解放一樣。」又說：「只要是對中國抗戰、台灣獨立有利，我可以犧牲一切去苦幹到底。」\[5\]\[6\]1940年，李友邦復刊[機關報](../Page/機關報.md "wikilink")《[臺灣先鋒](../Page/臺灣先鋒.md "wikilink")》，又出版《[臺灣革命叢書](../Page/臺灣革命叢書.md "wikilink")》。後台灣獨立革命黨加入[臺灣革命團體聯合會](../Page/臺灣革命團體聯合會.md "wikilink")，後聯合會更名為[臺灣革命同盟會](../Page/臺灣革命同盟會.md "wikilink")。此時李友邦與[嚴秀峰結爲夫婦](../Page/嚴秀峰.md "wikilink")。後李友邦授銜為[少將](../Page/少將.md "wikilink")[隊長](../Page/隊長.md "wikilink")，戰後升至[中將](../Page/中將.md "wikilink")。李友邦長期在國民黨[左派營壘中從事對台工作](../Page/左派.md "wikilink")、從事抗日運動而奮鬥一生。

民國三十四年（1945年）12月8日，李友邦率台灣義勇隊回臺，兼任[三民主義青年團中央直屬台灣區團部籌備處主任](../Page/三民主義青年團.md "wikilink")，受國民黨監視。民國三十五年（1946年），台灣義勇隊遭國民政府強制解散，李友邦擔任三民主義青年團臺灣分團主任。

民國三十六年（1947年）[二二八事件爆發後](../Page/二二八事件.md "wikilink")，李友邦被[臺灣省行政長官](../Page/臺灣省行政長官.md "wikilink")[陳儀以](../Page/陳儀.md "wikilink")[通匪與幕後鼓動暴動罪名逮捕](../Page/通匪.md "wikilink")，並解送[南京監禁](../Page/南京.md "wikilink")3個月，經過其夫人[嚴秀峰至南京向](../Page/嚴秀峰.md "wikilink")[蔣經國說明原委](../Page/蔣經國.md "wikilink")，李友邦才被釋放，返台在[基隆港碼頭受到熱烈歡迎](../Page/基隆港.md "wikilink")。李友邦回台灣之後，擔任[中國國民黨臺灣省黨部副主任委員兼](../Page/中國國民黨臺灣省黨部.md "wikilink")[中國國民黨改造委員會委員](../Page/中國國民黨改造委員會.md "wikilink")。

民國三十八年（1949年），[臺灣省保安司令部根據](../Page/臺灣省保安司令部.md "wikilink")「省工委匪諜案」逮捕[朱諶之](../Page/朱諶之.md "wikilink")（化名「朱楓」）。由於[朱諶之從](../Page/朱諶之.md "wikilink")[吳石及](../Page/吳石.md "wikilink")[蔡孝乾等處取得重要機密文件及膠卷後並未直接離台](../Page/蔡孝乾.md "wikilink")，又拜訪了幾位朋友，其中即包含其妻[嚴秀峰及新竹縣長](../Page/嚴秀峰.md "wikilink")[劉啟光之妻屠劍虹](../Page/劉啟光.md "wikilink")，而牽扯出[張志忠及](../Page/張志忠.md "wikilink")[季澐](../Page/季澐.md "wikilink")\[7\]，隨後嚴秀峰因通匪資匪案被判十五年徒刑。然而因嚴秀峰及朱諶之口供中皆說明李友邦未涉入共諜案，加上[毛人鳳及](../Page/毛人鳳.md "wikilink")[谷正文向](../Page/谷正文.md "wikilink")[蔣經國保證李友邦的清白](../Page/蔣經國.md "wikilink")，因此嚴秀峰案暫時未影響李友邦的黨務工作。

之後，李友邦在三青團中任職的[潘華係共產黨員](../Page/潘華.md "wikilink")（按其於二二八事件之前即離台，應無涉及省工委案），使其被國民黨政府以「包庇窩藏中共間諜」、「早已加入中共組織」等罪名判處死刑。\[8\]而蔣經國與蔣介石間也出現溝通不良，蔣經國不敢向蔣介石提出保證李友邦與共諜案無關。以至於在嚴秀峰入獄兩年後，蔣介石仍在[陽明山實踐研究院國民黨省黨部新舊任要員交接典禮上](../Page/陽明山.md "wikilink")，親自下令將李友邦下獄處死。

在處理此案的國民黨特務[谷正文的回憶錄中](../Page/谷正文.md "wikilink")，對李友邦被[蔣介石親口下令逮捕的過程有生動描述](../Page/蔣介石.md "wikilink")：

1952年4月22日，李友邦已在獄中重病，但仍由[三軍總醫院以](../Page/三軍總醫院.md "wikilink")[擔架將之抬押至](../Page/擔架.md "wikilink")[台北](../Page/台北.md "wikilink")[馬場町刑場](../Page/馬場町紀念公園.md "wikilink")[槍斃](../Page/槍斃.md "wikilink")，其後被葬於八里（今新北市[八里區龍米路一段旁](../Page/八里區.md "wikilink")）\[9\]。

李友邦將軍終其一生，都在反抗日本帝國主義的壓迫，其妻嚴秀峰曾兩次赴北京參加紀念中國人民抗日戰爭勝利50周年與60周年的座談會，追思丈夫及其他抗日先烈。老人在座談會上曾說：『盼有生之年，在兩岸同胞共同努力下，早日實現祖國的和平統一。這是我的心願，也是包括李友邦將軍在內的台灣鄉親的共同心願。』\[10\]

## 家庭

李友邦[妻子為](../Page/妻子.md "wikilink")[嚴秀峰](../Page/嚴秀峰.md "wikilink")，[杭州](../Page/杭州.md "wikilink")[望族出身](../Page/望族.md "wikilink")，1941年與李友邦結婚，2015年6月14日逝世，享壽95歲\[11\]。
[李友邦夫妻浮雕.jpg](https://zh.wikipedia.org/wiki/File:李友邦夫妻浮雕.jpg "fig:李友邦夫妻浮雕.jpg")

## 著作

李友邦生前寫了幾本書，這些書在1990年代以後，再度面世。目前（2015/3/31）已知的書，有《日本在台灣之殖民政策》與《台灣革命運動》這兩本。《日本在台灣之殖民政策》在1941年1月出版，由當時位在浙江省金華的台灣義勇隊發行；《台灣革命運動》則在1943年4月出版，由當時位於[福建](../Page/福建.md "wikilink")[龍岩的台灣義勇隊發行](../Page/龍岩.md "wikilink")。這兩本書被埋沒多年，1991年台北的世界翻譯社才再出版。除了寫書，李友邦也曾校訂[林海濤編輯的](../Page/林海濤.md "wikilink")《日本軍政界人物評論》，該書在1943年11月由福建龍岩的台灣義勇隊發行。此外，李友邦生前的文章，多年後由台北的海峽學術出版社集結成《李友邦文粹》，2004年5月出版。

## 身後相關事件

  - 紀念

2005年10月25日，李友邦將軍大型遺像被高掛在[國民黨中央黨部外牆](../Page/國民黨中央黨部.md "wikilink")，象徵對歷史錯誤[平反](../Page/平反.md "wikilink")。\[12\]

  - 課綱爭議

2014年，在[臺灣高中歷史課綱微調案的](../Page/臺灣高中歷史課綱微調案.md "wikilink")104課綱中，將李友邦的「抗日」事蹟寫入課綱，卻未提及他被國民政府「槍決」的原由。\[13\]

  - 評論

2015年8月29日，李友邦之子[李力群說](../Page/李力群.md "wikilink")，國民黨執政錯誤讓二二八事件第二次殘殺台灣菁英，不過李友邦不會恨國民黨，因為國民黨是[孫中山創造而非蔣介石創造的黨](../Page/孫中山.md "wikilink")\[14\]。2015年9月2日，[中華民國總統](../Page/中華民國總統.md "wikilink")[馬英九在](../Page/馬英九.md "wikilink")「紀念抗戰勝利七十週年暨民國104年[軍人節慶祝大會](../Page/軍人節.md "wikilink")」致詞時說，抗戰時期，李友邦組編台灣義勇隊在[閩浙沿海](../Page/閩浙.md "wikilink")[游擊戰抗日](../Page/游擊戰.md "wikilink")，[翁俊明領導臺灣革命同盟會](../Page/翁俊明.md "wikilink")，令人感動\[15\]。

## 相關條目

  - [蘆洲李宅](../Page/蘆洲李宅.md "wikilink")
  - [廖仲愷](../Page/廖仲愷.md "wikilink")
  - [半山仔](../Page/半山仔.md "wikilink")
  - [台灣義勇隊](../Page/台灣義勇隊.md "wikilink")
  - [台灣義勇隊舊址](../Page/台灣義勇隊舊址.md "wikilink")

## 参考資料

  - 書目

<!-- end list -->

  - [李筱峰教授](../Page/李筱峰.md "wikilink")，[《李友邦的悲劇與歷史教育》](https://www.jimlee.org.tw/article_detail.php?SN=8722&currentPage=9&AtricleCategory=1)
  - \[許雪姬教授，中央研究院《「保密局臺灣站二二八史料」的解讀與研究》，台灣史研究第21卷第4期，2014年\]
  - \[林正慧，中央研究院《二二八事件中的保密局》，台灣史研究第21卷第3期，2014年：張秉承致南京言普誠 - 報李友邦包庇奸黨高兩貴\]
  - 嚴秀峰發行，《李友邦先生紀念文集》（ 世界綜合出版社，臺北市， 2003年1月1日 ）

<!-- end list -->

  - 引用

## 外部連結

  - [台灣公論報](http://www.taiwantribune.com/living/2007/06/29/tt2163-5leeyeoban-2/)

[Category:中華民國陸軍中將](../Category/中華民國陸軍中將.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:台灣白色恐怖受難者](../Category/台灣白色恐怖受難者.md "wikilink")
[Category:被台灣槍決者](../Category/被台灣槍決者.md "wikilink")
[Category:臺灣獨立運動者](../Category/臺灣獨立運動者.md "wikilink")
[Category:台灣日治時期抗日人物](../Category/台灣日治時期抗日人物.md "wikilink")
[Category:黄埔军校第二期](../Category/黄埔军校第二期.md "wikilink")
[Category:半山](../Category/半山.md "wikilink")
[Category:蘆洲人](../Category/蘆洲人.md "wikilink")
[Y友](../Category/李姓.md "wikilink")
[Category:二二八事件受難者](../Category/二二八事件受難者.md "wikilink")
[Category:制憲國民大會候補代表](../Category/制憲國民大會候補代表.md "wikilink")

1.

2.
3.

4.
5.  台灣義勇隊: 台灣抗日團體在大陸的活動, 1937-1945 王政文 五南圖書出版股份有限公司, 2007 ISBN
    9789867332790

6.

7.

8.

9.

10.

11.

12. [馬英九憶抗日英雄李友邦事蹟邊講邊落淚](http://www.chinareviewnews.com/doc/9_850_100111572_1_0911123624.html)，中國評論新聞，2006-03-17

13.

14.

15.