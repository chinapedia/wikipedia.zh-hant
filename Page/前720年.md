<table>
<tbody>
<tr class="odd">
<td><p><small><strong><a href="../Page/世纪.md" title="wikilink">世纪</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small> <a href="../Page/前9世纪.md" title="wikilink">前9世纪</a> | <strong><a href="../Page/前8世纪.md" title="wikilink">前8世纪</a></strong> | <a href="../Page/前7世纪.md" title="wikilink">前7世纪</a></small><br />
</p></td>
</tr>
<tr class="even">
<td><p><small><strong><a href="../Page/年代.md" title="wikilink">年代</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small> <a href="../Page/前740年代.md" title="wikilink">前740年代</a> <a href="../Page/前730年代.md" title="wikilink">前730年代</a> | <strong><a href="../Page/前720年代.md" title="wikilink">前720年代</a></strong> | <a href="../Page/前710年代.md" title="wikilink">前710年代</a> <a href="../Page/前700年代.md" title="wikilink">前700年代</a></small><br />
</p></td>
</tr>
<tr class="odd">
<td><p><small><strong><a href="../Page/年.md" title="wikilink">年份</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small><a href="../Page/前725年.md" title="wikilink">前725年</a> <a href="../Page/前724年.md" title="wikilink">前724年</a> <a href="../Page/前723年.md" title="wikilink">前723年</a> <a href="../Page/前722年.md" title="wikilink">前722年</a> <a href="../Page/前721年.md" title="wikilink">前721年</a> | <strong>前720年</strong> | <a href="../Page/前719年.md" title="wikilink">前719年</a> <a href="../Page/前718年.md" title="wikilink">前718年</a> <a href="../Page/前717年.md" title="wikilink">前717年</a> <a href="../Page/前715年.md" title="wikilink">前715年</a> <a href="../Page/前715年.md" title="wikilink">前715年</a> </small><br />
</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td style="text-align: center;"><p> </p></td>
</tr>
<tr class="odd">
<td><p><strong>各種紀年：</strong></p></td>
<td style="text-align: center;"><p><small>'''<a href="../Page/周平王.md" title="wikilink">周平王宜臼五十一年</a>；<a href="../Page/鲁隐公.md" title="wikilink">鲁隐公息姑三年</a>；<a href="../Page/齐僖公.md" title="wikilink">齐僖公禄父十一年</a>；<a href="../Page/晋鄂侯.md" title="wikilink">晋鄂侯郗四年</a>；<a href="../Page/曲沃庄伯.md" title="wikilink">曲沃庄伯鲜十三年</a>；<a href="../Page/卫桓公.md" title="wikilink">卫桓公完十五年</a>；<a href="../Page/蔡宣公.md" title="wikilink">蔡宣公考父三十年</a>；<a href="../Page/郑庄公.md" title="wikilink">郑庄公寤生二十四年</a>；<a href="../Page/曹桓公.md" title="wikilink">曹桓公终生三十七年</a>；<a href="../Page/燕穆侯.md" title="wikilink">燕穆侯九年</a>；<a href="../Page/陈桓公.md" title="wikilink">陈桓公鲍二十五年</a>；<a href="../Page/杞武公.md" title="wikilink">杞武公三十一年</a>；<a href="../Page/宋穆公.md" title="wikilink">宋穆公和九年</a>；<a href="../Page/秦文公.md" title="wikilink">秦文公四十六年</a>；<a href="../Page/楚武王.md" title="wikilink">楚武王熊通二十一年</a></small><br />
</p></td>
</tr>
</tbody>
</table>

-----

  - [周鄭交質](../Page/周鄭交質.md "wikilink")。
  - [周平王崩](../Page/周平王.md "wikilink")，孙周桓王林立
  - 周桓王計劃專任[虢國君主为卿士](../Page/虢國.md "wikilink")，[鄭莊公怒](../Page/鄭莊公.md "wikilink")，命大夫[祭足率軍割取溫邑](../Page/祭足.md "wikilink")（河南溫縣）麥田。秋，再率軍割取成周（河南洛陽）附近高粱。[周朝與](../Page/周朝.md "wikilink")[鄭國互相怨惡](../Page/鄭國.md "wikilink")。
  - [宋穆公卒](../Page/宋穆公.md "wikilink")，公子馮出奔鄭國，侄[宋殤公與夷嗣位](../Page/宋殤公.md "wikilink")。

## 出生

  -
## 逝世

  - [周平王宜臼](../Page/周平王.md "wikilink")，东周第一位国王（出生于[前771年](../Page/前771年.md "wikilink")）

[前720年](../Category/前720年.md "wikilink")
[年0](../Category/前720年代.md "wikilink")
[Category:前8世纪各年](../Category/前8世纪各年.md "wikilink")