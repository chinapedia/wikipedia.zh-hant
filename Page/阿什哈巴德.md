**阿什哈巴德**（[土庫曼語](../Page/土庫曼語.md "wikilink")：；[俄語](../Page/俄語.md "wikilink")：），[土庫曼首都](../Page/土庫曼.md "wikilink")，人口909,000（2009年），面积约300km²。

## 歷史

阿什哈巴德位於古代[安息王國最初的首都](../Page/安息王國.md "wikilink")[尼薩東南方](../Page/尼薩_\(土庫曼斯坦\).md "wikilink")18公里處。位處於當時[絲綢之路的路線上](../Page/絲綢之路.md "wikilink")，當時被稱為[科奇卡拉](../Page/科奇卡拉.md "wikilink")，在公元前2世紀是一個產酒的村落。後來在公元前1世紀的一次大地震，整條村被地震所毀滅，但其優越的地理位置，使村落其後又再重建，直到13世紀[蒙古帝國的興起時](../Page/蒙古帝國.md "wikilink")，連同[花剌子模一起被毀滅](../Page/花剌子模.md "wikilink")。\[1\]\[2\]

而「阿什哈巴德」這個名字早在19世纪中叶的历史文献出現，意思为“爱之城”，当时为土库曼人的一支[捷詹人的城堡](../Page/捷詹.md "wikilink")\[3\]。1881年，基於《[阿克哈條約](../Page/阿克哈條約.md "wikilink")》，阿什哈巴德連同花剌子模其他地方被[波斯割讓與](../Page/波斯.md "wikilink")[沙俄](../Page/沙俄.md "wikilink")，沙俄立即组建[後裡海軍區](../Page/後裡海軍區.md "wikilink")，並在阿什哈巴德建立要塞，作為軍區的行政中心\[4\]。1885年～1889年建成通向[裡海和](../Page/裡海.md "wikilink")[塔什干的铁路](../Page/塔什干.md "wikilink")，发展迅速，成为俄国和伊朗的贸易中心。1925年成为[土库曼苏维埃社会主义共和国的首都](../Page/土库曼苏维埃社会主义共和国.md "wikilink")\[5\]。1948年10月，阿什哈巴德遭受大地震破坏\[6\]，16萬人於地震中死亡\[7\]，之後重建。1991年，土庫曼斯坦獨立，作為新國家的首都及政治、經濟、文化的新中心，阿什哈巴德大興土木，以迎合將來的位置與機遇\[8\]。

## 地理

阿什哈巴德是[卡拉库姆沙漠中的一个](../Page/卡拉库姆沙漠.md "wikilink")[绿洲城市](../Page/绿洲.md "wikilink")，位於土庫曼斯坦南部，卡拉库姆沙漠和[科佩特山交界处](../Page/科佩特山.md "wikilink")\[9\]。

阿什哈巴德距[伊朗边境](../Page/伊朗.md "wikilink")30多公里。在[第一次世界大戰爆發後](../Page/第一次世界大戰.md "wikilink")，成为了沙俄与伊朗之間的贸易重镇\[10\]。

### 氣候

由於阿什哈巴德位處於卡拉库姆沙漠和科佩特山交界处\[11\]，阿什哈巴德的北部與沙漠接壤，距離科佩特山也只有25公里，因此當地的氣候為典型[大陆性气候](../Page/大陆性气候.md "wikilink")\[12\]的[沙漠气候](../Page/沙漠气候.md "wikilink")，夏季漫长，炎热干燥；冬季短暂，温和而少雪。昼夜温差较大，日照极为充分\[13\]。全年的總降雨量也只是227毫米，當中以三月和四月最潮濕\[14\]。

### 人口

阿什哈巴德人口結構主要為[土庫曼族](../Page/土庫曼族.md "wikilink")，其次則為[俄羅斯人](../Page/俄羅斯人.md "wikilink")、[亞美尼亞人與](../Page/亞美尼亞人.md "wikilink")[亞塞拜然族](../Page/亞塞拜然族.md "wikilink")。
2000年10月的人口有63.7万\[15\]，2009年統計有90.9萬人。每年人均人口增長約為5%\[16\]。

## 交通

[外裡海鐵路在本市設站](../Page/外裡海鐵路.md "wikilink")。[薩帕爾穆拉特·尼亞佐夫國際機場位在本市約](../Page/薩帕爾穆拉特·尼亞佐夫國際機場.md "wikilink")10公里處。[土庫曼航空的總部也設於本市](../Page/土庫曼航空.md "wikilink")。

## 歷任市長

  - [杜尔德雷耶夫](../Page/杜尔德雷耶夫.md "wikilink")（）
  - [奥拉佐夫](../Page/奥拉佐夫.md "wikilink")\[17\]
  - Azat Bilishov\[18\]

## 參考資料

## 外部連結

  - [1](http://ashgabat.gov.tm/)
  - [阿什哈巴德照片圖庫](http://www.ashgabat.us/gallery)
  - [阿什哈巴德現代照片圖庫](http://www.travel-images.com/turkmenistan.html)
  - [impressions of
    Ashgabat](https://web.archive.org/web/20080509041819/http://goetz.burggraf.de/Bildergalerien/Turkmenistan/)

  - [Page on modern
    Ashgabat](http://www.vokrugsveta.ru/publishing/vs/archives/?item_id=399)

  - [1948年以前在 阿什哈巴德
    的巴哈伊信仰活動照片圖庫](http://www.neweurasia.net/culture-and-history/a-forgotten-dawn-in-ashgabad/)

[阿什哈巴特](../Category/阿什哈巴特.md "wikilink")
[Category:土庫曼斯坦城市](../Category/土庫曼斯坦城市.md "wikilink")
[Category:絲綢之路聚居地](../Category/絲綢之路聚居地.md "wikilink")

1.

2.

3.
4.
5.
6.
7.

8.
9.
10.
11.
12.
13.
14.

15.
16.

17.

18.