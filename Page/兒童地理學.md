**兒童地理學**屬於[人文地理學研究的一部分](../Page/人文地理學.md "wikilink")，研究的是[兒童生活的地方與空間](../Page/兒童.md "wikilink")。

自[地理學的文化轉向以來](../Page/地理學.md "wikilink")，[社會多被認為是](../Page/社會.md "wikilink")[異質而非均質的](../Page/異質.md "wikilink")，且以[多元](../Page/多元.md "wikilink")、差異與主觀性為特質。在理論化孩童和他們的地理學時，研究的方法和本體論的假設常框限「兒童」與「成人」以強加一個兩極、階層和發展的模式，如此再生產和強化了兒童知識生產時，以成人為中心的言談之領導權。孩童的地理學自1990年代早期在人文地理學中發展，早期的工作可追溯至[威廉·班吉對](../Page/威廉·班吉.md "wikilink")[底特律和](../Page/底特律.md "wikilink")[多倫多的孩子面對成人社會](../Page/多倫多.md "wikilink")、文化、政治環境結構之空間壓迫的研究。

此發展呈現了早期人文地理研究大大地忽略了兒童的日常生活，疏忽了他們明顯地形塑一個[社會區塊](../Page/社會.md "wikilink")，有特別的需求和能力，可能以非常不同的方式經驗世界。

兒童地理學倚靠在兒童是一分享某些經驗、政治和道德特徵的社會群體之概念上，且值得研究。其英語標題（Children's
geographies）為複數，暗指孩童的生活在不同的時間和地點，以及不同的情況下，如：[性別](../Page/性別.md "wikilink")、[家庭和](../Page/家庭.md "wikilink")[階級](../Page/階級.md "wikilink")，將明顯不同。當前孩童的地理學之發展，試圖連接分析孩童的地理學之框架到一個多元觀點與自願性，以承認其地理學的多樣性。

兒童地理學有時被結合以與童年地理學(the geographies of
childhood)做區分。前者的興趣在孩童的日常生活；後者的興趣在成人社會如何設想孩童的想法，及其如何衝擊孩童生活中的許多方面，此包括關於孩童的本質和相關(空間)涵義之想像。

兒童的地理學有完整的焦點研究範圍，包含孩童與[城市](../Page/城市.md "wikilink")、孩童與[鄉村](../Page/鄉村.md "wikilink")、孩童與[科技](../Page/科技.md "wikilink")、孩童與[自然](../Page/自然.md "wikilink")、孩童與[全球化](../Page/全球化.md "wikilink")…等。而《[兒童地理學報](../Page/兒童地理學報.md "wikilink")》是其重要刊物，能提供讀者對其（次）學科活躍的議題、理論與方法論發展之了解。

## 參見

  -
  -
  - [文化地理學](../Page/文化地理學.md "wikilink")

  -
## 參考資料

1.  [Journal of Children's
    Geographies(英文)](../Page/:en:Journal_of_Children's_Geographies.md "wikilink")

[Category:人文地理學](../Category/人文地理學.md "wikilink")
[Category:兒童](../Category/兒童.md "wikilink")