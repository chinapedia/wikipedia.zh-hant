《**怵目驚魂28天**》（***Donnie
Darko***）是一部[2001年推出的](../Page/2001年電影.md "wikilink")[美國](../Page/美國.md "wikilink")[心理](../Page/心理.md "wikilink")[驚悚](../Page/驚悚.md "wikilink")、[科幻](../Page/科幻.md "wikilink")[電影](../Page/電影.md "wikilink")，由[理查·凱利](../Page/理查·凱利.md "wikilink")（Richard
Kelly）編劇和執導。故事敘述一位名叫東尼·達克（Donnie
Darko，與電影原文標題相同）的青少年，在一個人型[兔子的慫恿下](../Page/兔子.md "wikilink")，做出一些為了要終結世界的破壞行為。本片在美國上映時票房不盡理想，但在DVD租售市場卻表現相當優異，目前被認為是一部[邪典經典](../Page/邪典電影.md "wikilink")。

## 故事劇情

故事發生在美國[維吉尼亞州一個名為](../Page/維吉尼亞州.md "wikilink") Middlesex
的小鎮上，時值[1988年總統大選](../Page/1988年美國總統選舉.md "wikilink")。東尼·達克（Donnie
Darko，[傑克·葛倫霍](../Page/傑克·葛倫霍.md "wikilink")
飾）是一名受到心理問題困擾的青少年，他會[夢遊](../Page/夢遊.md "wikilink")，也會去向心理醫生求診。某天深夜，一個不明的噴射引擎墜毀在東尼的房間。他正好受到腦海的某種聲音指引，夢遊離開了房間而倖免於難。那個聲音的主人名叫法蘭克（Frank），是一個身著全套[兔子裝扮的人](../Page/兔子.md "wikilink")。法蘭克向東尼宣告，這個世界還有28天6小時42分12秒就要毀滅了。

法蘭克慫恿東尼做出許多行動，例如讓學校陷入一片混亂，也給他了機會認識新同學葛芮琴·羅絲（Gretchen
Ross）並進而交往；他也詢問科學老師[時光旅行的問題](../Page/時光旅行.md "wikilink")，而老師給了他一本書，名為《時光旅行的哲學》（*The
Philosophy of Time Travel*），這本書是由「死亡奶奶」（Grandma
Death）撰寫，她是個住在附近，平常看起來非常虛弱衰老的鄰居；法蘭克還被慫恿在一名當時很火紅的行動演說家的房子縱火，而火災讓屋內一間堆滿[兒童色情片的祕密房間曝光](../Page/兒童色情.md "wikilink")。隨著書中章節的推進，東尼漸漸的知道了一些重要的事，東尼開始看見許多「入口」（portals），那是一種在人類胸前出現的[蟲洞](../Page/蟲洞.md "wikilink")。其中一個蟲洞讓他發現了藏在父母衣櫥裡的[手槍](../Page/手槍.md "wikilink")，之後東尼私下藏起了那把槍。在幫姊姊慶祝考上哈佛的化妝舞會那天，東尼帶著葛芮琴和她的兩個朋友去向「死亡奶奶」求助，卻在「死亡奶奶」的家中被學校裏的惡霸學生埋伏襲擊。在打鬥中葛芮琴被一輛為了閃避不巧站在路中央的路過老奶奶而失控的車碾過去，雖然發現那名駕駛者就是穿著萬聖節兔子服裝的法蘭克，還是在氣憤之下，拿出手槍擊斃了法蘭克。已經知自己的使命的東尼，回家開車載著着葛芮琴冰冷的屍體，到了郊外看著風雲變色即將到來的世界末日，他用超能力（telekinesis），把东尼妈妈和妹妹乘坐的飞机上面的引擎扯掉，經由蟲洞，送回主要的宇宙（Primary
Universe)，修正了因不明原因造成東尼所屬的平行宇宙存在而會使得世界崩塌的可怕災難發生
。一切倒轉，回到原始宇宙事件發生的那天晚上，東尼任務成功心滿意足的醒來又沈沈的睡下，而幾分鐘後飛機引擎則穿越虫洞刚好墜落到28天前東尼的房间，將東尼砸死了。一起經歷過平行宇宙中的人們，只剩下似乎像夢境般的殘破記憶。電影結束在伤心的父母看着东尼的尸体运出家里，根本沒能跟東尼認識的
葛芮琴跟東尼媽媽，卻像是似曾相識般的互相安慰般的揮了揮手 ...。

## 演員與角色

  - [傑克·葛倫霍](../Page/傑克·葛倫霍.md "wikilink") 飾 東尼·達克（Donald J. "Donnie"
    Darko）
  - [吉娜·瑪隆](../Page/吉娜·瑪隆.md "wikilink") 飾 葛芮琴·羅絲（Gretchen Ross）
  - Mary McDonnell 飾 羅絲·達克（Rose Darko）
  - Holmes Osborne 飾 艾迪·達克（Eddie Darko）
  - [瑪姬·葛倫霍](../Page/瑪姬·葛倫霍.md "wikilink") 飾 伊莉莎白·達克（Elizabeth Darko）
  - Daveigh Chase 飾 莎曼珊·達克（Samantha Darko）
  - [派屈克·史威茲](../Page/派屈克·史威茲.md "wikilink") 飾 吉姆·库宁汉姆（Jim Cunningham）
  - [茱兒·芭莉摩](../Page/茱兒·芭莉摩.md "wikilink") 飾 凱倫（Karen Pomeroy）
  - [-{zh-hans:諾亞·懷勒;zh-hk:諾亞·懷利;zh-tw:諾亞·懷利;}-](../Page/诺亚·怀勒.md "wikilink")
    飾 肯尼斯教授（Kenneth Monnitoff）
  - Beth Grant 飾 Kitty Farmer
  - Katharine Ross 飾 莉莉安博士（Dr. Lillian Thurman）
  - Patience Cleveland 飾 羅柏塔·史派洛（Roberta Sparrow，又稱「瀕死的老奶奶」（Grandma
    Death））
  - James Duval 飾 法蘭克（Frank）
  - [艾希莉·提斯代爾](../Page/艾希莉·提斯代爾.md "wikilink") 飾 琴（Kim）
  - [賽斯·羅根](../Page/賽斯·羅根.md "wikilink") 飾 瑞奇（Ricky Danforth）
  - Jolene Purdy 飾 契瑞塔（Cherita Chen）

## 提名與獲獎

  - 2001年：[理查·凱利以](../Page/理查·凱利.md "wikilink")《怵目驚魂28天》獲得加泰隆國際影展（Catalonian
    International Film Festival）和聖地牙哥影評人協會（San Diego Film Critics
    Society）的「最佳劇本」。《怵目驚魂28天》也[日舞影展獲得](../Page/日舞影展.md "wikilink")「觀眾票選獎」（Audience
    Award）的最佳影片。本片在加泰隆國際影展獲「最佳影片」提名，在日舞影展獲得 「金評審團大獎」（Grand Jury
    Prize）提名。

## 外部連結

  - [DonnieDarkoFilm.com](http://www.donniedarkofilm.com) - 電影官方網站

  -
  -
  -
  - \[<http://www.stainlesssteelrat.net/ddfaq.htm> 怵目驚魂28天常見問答集\]

  - [科幻作家勞倫斯·波森（Lawrence
    Person）對本片的分析](http://www.locusmag.com/2003/Reviews/Person04_Darko.html)

  - [怵目驚魂28天電腦桌布](http://www.donnie-darko.de/wallpaper.htm)

[Category:2001年電影](../Category/2001年電影.md "wikilink")
[Category:美國奇幻電影](../Category/美國奇幻電影.md "wikilink")
[Category:美國驚悚片](../Category/美國驚悚片.md "wikilink")
[Category:心理驚悚片](../Category/心理驚悚片.md "wikilink")
[Category:邪典電影](../Category/邪典電影.md "wikilink")
[Category:時間旅行電影](../Category/時間旅行電影.md "wikilink")
[Category:黑色電影](../Category/黑色電影.md "wikilink")
[Category:存在主義作品](../Category/存在主義作品.md "wikilink")
[Category:美國悲劇電影](../Category/美國悲劇電影.md "wikilink")
[Category:維吉尼亞州背景電影](../Category/維吉尼亞州背景電影.md "wikilink")
[Category:有關市郊區的電影](../Category/有關市郊區的電影.md "wikilink")
[Category:導演處女作](../Category/導演處女作.md "wikilink")
[Category:1980年代背景電影](../Category/1980年代背景電影.md "wikilink")
[Category:蟲洞題材作品](../Category/蟲洞題材作品.md "wikilink")
[Category:已逝人物旁白作品](../Category/已逝人物旁白作品.md "wikilink")