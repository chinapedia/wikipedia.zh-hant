**32位元**也是一種稱呼[電腦](../Page/電腦.md "wikilink")[世代的名詞](../Page/世代.md "wikilink")，在於以32位元處理器為準則的時間點。

32位元可以儲存的[整數範圍是](../Page/整數.md "wikilink")0到4294967295，或使用[二的補數是](../Page/二的補數.md "wikilink")-2147483648到2147483647。因此，32位元[記憶體位址可以直接存取](../Page/記憶體位址.md "wikilink")4[GiB以](../Page/GiB.md "wikilink")[位元組定址的記憶體](../Page/位元組定址.md "wikilink")。

外部的記憶體和[資料匯流排通常都比](../Page/資料匯流排.md "wikilink")32位元還寬，但是兩者在處理器內部儲存或是操作時都當作32位元的數量。舉例來說，[Pentium
Pro處理器是](../Page/Pentium_Pro.md "wikilink")32位元機器，但是外部的位址匯流排是36位元寬，外部的資料匯流排是64位元寬。**32位元應用程式**是指那些在
**32位元**平面[位址空間](../Page/位址空間.md "wikilink")（[平面記憶體模式](../Page/平面記憶體模式.md "wikilink")）的[軟體](../Page/軟體.md "wikilink")。

## 32位元應用程式

32位元應用程式這個名詞的出現，是由於原先為[Intel
8088和](../Page/Intel_8088.md "wikilink")[Intel
80286](../Page/80286.md "wikilink")[微處理器所撰寫的](../Page/微處理器.md "wikilink")[DOS和](../Page/DOS.md "wikilink")[微軟](../Page/微軟.md "wikilink")[Windows](../Page/Windows.md "wikilink")。這些是[16位元的區段位址空間定址的微處理器](../Page/16位元.md "wikilink")。擁有大於64[KB](../Page/KB.md "wikilink")
的程式和資料因此必須要經常地在不同區段間切換。相對於其他的機器運作，這些操作是相當的耗時，因此應用程式的效能可能變得較差。再者，使用到區段的[程式設計比起平面記憶體空間的方式](../Page/程式設計.md "wikilink")，會導致某些程式語言上的複雜性，像是[C語言和](../Page/C語言.md "wikilink")[C++語言的](../Page/C++.md "wikilink")“[記憶體模式](../Page/記憶體模式.md "wikilink")”。
在 IBM 相容系統上，從[16位元軟體轉移到](../Page/16位元應用程式.md "wikilink")32位元軟體，隨著 [Intel
80386](../Page/80386.md "wikilink")
[微處理器的推出而變成可能](../Page/微處理器.md "wikilink")。這個微處理器和他的後代支援16位元和32位元節區的區段記憶體空間（更精確地說，是有16或32位元位址偏移量的區段）。如果全部的32位元區段的基底位址都設定為0，那麼區段暫存器就不用明確地使用，這些區段可以被遺忘掉，處理器就像是擁有一個簡單的線性32位元位址空間。然而為了相容性的因素，大多數軟體仍以16位元模式撰寫。

像是[Windows或是](../Page/Windows.md "wikilink")[OS/2](../Page/OS/2.md "wikilink")[作業系統提供了也讓](../Page/作業系統.md "wikilink")16位元（區段）程式可以像32位元程式執行的可能性。前者16位元的相容性之所以存在是因為要提供向前相容性，而後者32位元是用來作為新的軟體發者使用。

[iOS](../Page/iOS.md "wikilink") 11起停止支持32位应用程序\[1\]。

## 相关条目

  - [16位元](../Page/16位元.md "wikilink")
  - [64位元](../Page/64位元.md "wikilink")
  - [32位元應用程式](../Page/32位元應用程式.md "wikilink")
  - [Gibibyte](../Page/Gibibyte.md "wikilink")

## 参考资料

[Category:資訊單位](../Category/資訊單位.md "wikilink")

1.