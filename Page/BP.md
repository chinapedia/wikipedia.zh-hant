**BP**（正式英文全称：BP p.l.c.。前稱：British
Petroleum（英国石油），簡稱：BP，后BP简称成为正式名称），是[世界最大私營](../Page/世界.md "wikilink")[石油](../Page/石油.md "wikilink")[公司之一](../Page/公司.md "wikilink")（即國際石油[七姊妹之一](../Page/七姊妹_\(石油\).md "wikilink")），也是世界前十大私營企業集團之一，2012年營業額達到3882.85億美元。該公司前身是創立於1908年的盎格魯·波斯石油公司（Anglo-Persian
Oil Company），1954年改稱“British
Petroleum”（英國石油公司），同时“BP”缩写依然常用。該公司整合探油、探氣、煉油、儲油、售油、零售等營業領域。在1998年一次購併行動後，“BP”不再作為“British
Petroleum”的縮寫，而成為公司正式名稱。在廣告上，該公司有時使用「超越石油」「Beyond Petroleum」來宣傳業務。

## 歷史

### 1909-1955

1908年，英國人William Knox
D'Arcy獲得[伊朗國王批准](../Page/伊朗.md "wikilink")，開始商業性地開發他在1901年於[波斯灣發現的石油資源](../Page/波斯灣.md "wikilink")。1909年4月14日，新組建的「昂格魯-波斯石油公司」（Anglo-Persian
Oil
Company，APOC）投入此項開發。初時業務進展不大，但[第一次世界大戰爆發時](../Page/第一次世界大戰.md "wikilink")，英國政府認識到了石油資源的重要性，開始收購該公司股權，該公司也開始向[皇家海軍提供燃油](../Page/皇家海軍.md "wikilink")。戰後，英國政府已經擁有達51%之股份，並開展在歐洲各地的業務。但公司關注焦點仍然放在波斯灣。

1931年，該公司購併了當時與其合作從事行銷的[殼牌公司](../Page/殼牌公司.md "wikilink")，改組出一家「殼牌麥克斯英國石油公司」（Shell-Mex
and BP
Ltd），但負責在伊朗（當時仍稱[波斯](../Page/波斯.md "wikilink")）的業務的仍是APOC。殼牌公司在1975年與英國石油分家。

出於對[帝國主義不滿情緒的高漲](../Page/帝國主義.md "wikilink")，伊朗曾在1932年戰時撤回對APOC的特許，經過一年談判，雙方協議給予伊朗更多股份和收益，該公司在灣區的業務才恢復。1936年波斯改稱伊朗，該公司亦改稱「昂格魯-伊朗石油公司」（"Anglo-Iranian
Oil Company"，AIOC）。

[二戰以後區域內的民族情緒高漲](../Page/二戰.md "wikilink")。親西方的伊朗政府在1949年曾短暫與該公司達成新協議。但1951年3月，親西方的伊朗首相Ali
Razmara被刺殺，一個月後，伊朗國會通過議案將AIOC[國有化](../Page/國有化.md "wikilink")，並推舉民族主義政治家[穆罕默德·摩萨台](../Page/穆罕默德·摩萨台.md "wikilink")（Mohammed
Mossaddegh）為新首相。摩萨台認為，該公司是帝國主義剝削伊朗的代表，唯有國有化政策才能使伊朗人民享有應有權益。但英國政府成功地說服[美國共同出面維護西方在海灣的利益](../Page/美國.md "wikilink")，以抵制可能逐漸增加的[蘇聯的影響](../Page/蘇聯.md "wikilink")，並聲稱摩萨台政權是親共政府。1953年8月在美國[中情局協助下](../Page/中情局.md "wikilink")，通过一場政變推翻摩萨台，「昂格魯-伊朗石油公司」重回海灣，以40%外資股權方式在伊朗恢復業務，並正式改稱「英國石油公司」（**BP**）。新方案雖使該公司稍受打擊，但[世界銀行協助了BP在海灣其他國家的投資計畫](../Page/世界銀行.md "wikilink")，使其利潤不墜。
[BP_stasjon,_Nøtterøy.JPG](https://zh.wikipedia.org/wiki/File:BP_stasjon,_Nøtterøy.JPG "fig:BP_stasjon,_Nøtterøy.JPG")

### 1960-1979

1960年代起，英國石油公司開始關注美國[阿拉斯加和歐洲](../Page/阿拉斯加.md "wikilink")[北海的油源開發](../Page/北海_\(大西洋\).md "wikilink")。這一新創舉在1970年代實現量產，使英國石油公司能抗擊後來發生的兩次[石油危機](../Page/石油危機.md "wikilink")。但阿拉斯加的原住民聲稱，該公司在當地的開發是非法的掠奪。英國石油公司為拓展美國業務，1970年代又購併了美國「俄亥俄標準石油公司」（Standard
Oil of
Ohio）。1971年开始，在[中东国家石油公司的股权已部分或全部被当地政府收归国有](../Page/中东.md "wikilink")，1979年伊朗发生革命，英国石油在伊朗的霸权地位才告结束。

### 1980-1999

彼得·沃尔特斯爵士在1981年至1990年担任英国石油的[董事长](../Page/董事长.md "wikilink")\[1\]那是[撒切尔政府实施](../Page/撒切尔.md "wikilink")[私有化战略的时代](../Page/私有化.md "wikilink")。英国政府从1979年到1987年分批将其持有的所有英国石油的资产出售\[2\]。整个过程以[科威特投资办公室试图购买并获得英国石油控制权为标志](../Page/科威特投资局.md "wikilink")\[3\]。

这一交易最终因英国政府的强烈反对而中断。1987年，英国石油为并购[布里特奥尔石油公司进行谈判](../Page/布里特奥尔石油公司.md "wikilink")\[4\]并保留俄亥俄标准石油的公开交易的股份。

[A_BP_Shop_Petrol_Station_in_Havant,_Hampshire.jpg](https://zh.wikipedia.org/wiki/File:A_BP_Shop_Petrol_Station_in_Havant,_Hampshire.jpg "fig:A_BP_Shop_Petrol_Station_in_Havant,_Hampshire.jpg")本土的英国石油加油站。\]\]

沃尔特斯爵士于1989年被[罗伯特·霍顿接替董事长职务](../Page/罗伯特·霍顿.md "wikilink")。霍顿进行了一项重大裁员措施，把英国石油总部旗下的多级管理部门撤销\[5\]。

从1991年担任董事会常务董事的[马丁格利布朗爵士](../Page/约翰·布朗，马丁格利布朗男爵.md "wikilink")，1995年被任命为集团执行总裁\[6\]。布朗负责了三次大的并购案，阿莫科、阿科和[嘉实多](../Page/嘉实多.md "wikilink")（如下）。

## 目前

英国石油于1998年12月与阿莫科公司（曾为印第安纳标准石油）合并\[7\]，成为英国石油阿莫科公司，而2000年，新公司又重新改名为英国石油公司，并采纳了“超越石油”的宣传口号，一直延续至今，表明BP不是宣传口号的缩写。大多数美国的阿莫科加油站改变了外观并把名字换成BP品牌。虽然在许多州，BP还在出售阿莫科牌的汽油，因为阿莫科连续16年被评为排名第一的石油品牌（加油站本身还是BP），并且在美国阿莫科牌汽油与[雪佛龙和](../Page/雪佛龙.md "wikilink")[壳牌一样](../Page/壳牌石油.md "wikilink")，具有最高的品牌忠诚度。2008年5月，阿莫科几乎被停止使用，取而代之的是“具有活力的BP汽油”，用以推销BP新的燃油添加剂。然而在美国最好的BP汽油依然被称为阿莫科旗舰。2000年，BP并购了[阿科（大西洋里奇菲尔德公司](../Page/阿科.md "wikilink")\[8\]和嘉实多\[9\]。

[Steven_Koonin_BP_2005-02-22.jpg](https://zh.wikipedia.org/wiki/File:Steven_Koonin_BP_2005-02-22.jpg "fig:Steven_Koonin_BP_2005-02-22.jpg")

2004年4月，BP决定把其大部分的石化产业整合成集团下一个单独实体，名为“[亿诺](http://www.innovene.com)”。目的是通过在美国[首次公开募股来出售新公司](../Page/首次公开募股.md "wikilink")，实际上他们在2005年9月12日向[纽约证券交易所递交了首次公开募股计划](../Page/纽约证券交易所.md "wikilink")。然后而，当年的10月7日，BP宣布他们同意以总价值90亿美元将亿诺卖给英国一家私营化工公司[英力士](../Page/英力士.md "wikilink")，因此放弃了首次公开募股计划\[10\]。

2005年3月23日，位于[德克萨斯城的](../Page/德克萨斯城，德州.md "wikilink")[BP的德克萨斯城炼油厂发生爆炸](../Page/德克萨斯城炼油厂（BP）.md "wikilink")。该炼油厂为美国第三大炼油厂，也是世界上最大的炼油厂之一，日处理原油，占美国汽油供应量的3%。100多人受伤，15人已确认罹难，除BP员工外，还包括[福乐公司的员工](../Page/福乐公司.md "wikilink")。BP已经接受了事故由于管理不当引起。液面指示器失灵，导致加热器超量填充，轻烃蔓延整个地区。一个无法确认的火源引起了爆炸\[11\]。

2005年，BP宣布要放弃[科罗拉多市场](../Page/科罗拉多.md "wikilink")\[12\]。许多加油站换成[康纳的牌子](../Page/康纳.md "wikilink")\[13\]。

根据一些大亚特兰大地区的个人BP品牌加油站运营商说，BP打算以后几年撤离南部市场。所有通常被成为BP连锁的其集团所属的所有加油站会卖给当地的汽油批发商\[14\]。

2006年，BP與[中國海洋石油](../Page/中國海洋石油.md "wikilink")、[香港中華煤氣等公司合作發展的](../Page/香港中華煤氣.md "wikilink")[廣東大鵬液化天然氣接收站正式啟用供氣](../Page/廣東大鵬液化天然氣接收站.md "wikilink")，成為中國第一座海運天然氣接收站。

2006年3月，美国阿拉斯加北坡的BP石油管线泄露引起原油泄漏到冻土地带，导致BP承诺更换超过的联邦规定的原油转运管道。截止2007年底，一半的管道已经更换，并且所有的管道经测试均正常\[15\]。

BP最近已经注意到通过加大开发如前苏联等边区来增加其未来石油储备\[16\]。

在[俄罗斯](../Page/俄罗斯.md "wikilink")，BP和三个俄罗斯亿万富翁分别拥有[TNK-BP的一半股份](../Page/秋明英国石油.md "wikilink")。TNK-BP占BP全球原油储备的1/5，产量的1/4，差不多1/10的利润\[17\]。

2006年7月19日，BP宣布将关掉大部分位于[阿拉斯加](../Page/阿拉斯加.md "wikilink")[普拉德霍湾](../Page/普拉德霍湾.md "wikilink")57口泄露油井中的最后12口。这些油井和冰层间含有[原油和](../Page/原油.md "wikilink")[柴油的保温材料会泄露](../Page/柴油.md "wikilink")\[18\]。

2007年1月12日，BP宣布布朗勋爵于2007年7月底退休\[19\]。新的首席执行官为现任勘探开发总负责人，托尼·海伍德。之前，布朗勋爵预计在2008年2月他达到60岁这一BP标准退休年龄的时候退休。2007年5月1日，在一项旨在防止[联合报业出版他私生活的禁令撤销后](../Page/联合报业.md "wikilink")，布朗突然宣布辞职，随即海伍德继任布朗的职务\[20\]。

2012年10月23日，英国石油同意以270億美元將[TNK-BP](../Page/秋明英国石油.md "wikilink")50%的售予Rosneft。Rosneft將用171億美元（約1325億港元）現金和12.84%股份，收購BP持有的50%TNK-BP股權。BP則計劃用部分交易所得，額外購入俄油5.66%股份。一旦交易完成，BP在俄油的持股比例將增至19.75%

2018年6月28日英國石油公司收購英國最大電動車充電公司Chargemaster，但沒有披露作價。Chargemaster在英國經營逾6,500個充電點，被收購後將易名BP
Chargemaster，並與公司現有1200個油站結合

2018年7月27日英國石油以105億美元收購[必和必拓大部分在美國的陸上石油和天然氣資產](../Page/必和必拓.md "wikilink")，當中包括鷹堡（Eagle
Ford）、海恩斯維爾（ Haynesville）和新墨西哥州東部的二疊紀盆地（Permian basin）

## 2010年墨西哥灣漏油事故

墨西哥灣漏油事件，又稱英国石油漏油事故，是2010年4月20日發生的一起[墨西哥灣外海油污外漏事件](../Page/墨西哥灣.md "wikilink")。起因是英國石油公司所屬一個名為「[深水地平線](../Page/深水地平線鑽油平臺.md "wikilink")」（Deepwater
Horizon）的外海[鑽油平臺故障並爆炸](../Page/鑽油平臺.md "wikilink")，導致了此次漏油事故。爆炸同时導致了11名工作人員死亡及17人受傷。

據估計每天平均有12,000到100,000[桶原油漏到墨西哥灣](../Page/桶_\(单位\).md "wikilink")，導致至少2,500[平方公里的海水被石油覆蓋着](../Page/平方公里.md "wikilink")。專家們擔心此次漏油会導致一场環境災难影響多種[生物](../Page/生物.md "wikilink")。此次漏油还影響了當地的漁業和旅游業。

## 引用

## 外部連結

  - [BP公司官網](https://web.archive.org/web/20060623045737/http://www.bp.com/home.do?categoryId=1)
  - [BP公司Youtube頻道](http://www.youtube.com/beyondpetroleum)
  - [BP中国公司官網](http://www.bp.com.cn)

[\*](../Category/BP.md "wikilink")
[Category:石油公司](../Category/石油公司.md "wikilink")
[Category:英國化學工業公司](../Category/英國化學工業公司.md "wikilink")
[Category:总部在伦敦的跨国公司](../Category/总部在伦敦的跨国公司.md "wikilink")
[BP](../Category/英國品牌.md "wikilink")
[Category:東京證券交易所已除牌公司](../Category/東京證券交易所已除牌公司.md "wikilink")
[Category:1909年成立的公司](../Category/1909年成立的公司.md "wikilink")

1.  [TNK appoints Sir Peter
    Walters](http://www.prnewswire.co.uk/cgi/news/release?id=86627)
2.  [Privitisation](http://www.econlib.org/library/Enc/Privatization.html)
3.  [Kuwait has 10% of
    BP](http://query.nytimes.com/gst/fullpage.html?res=9B0DE6DC1E31F93AA25752C1A961948260&sec=&spon=)
4.  [Britain drops a barrier to BP
    bid](http://query.nytimes.com/gst/fullpage.html?res=940DE7DF1F30F936A35751C0A96E948260)
5.  [Organising for performance: how BP did
    it](http://www.gsb.stanford.edu/news/bmag/sbsm0502/feature_bp.shtml)

6.  [Royal Academy of
    Engineering](http://www.raeng.org.uk/about/fellowship/fame/browne.htm)
7.  [BP and Amoco in oil
    mega-merger](http://news.bbc.co.uk/1/hi/business/the_company_file/149139.stm)
8.  [BP strikes it rich in
    America](http://findarticles.com/p/articles/mi_qn4158/is_19990404/ai_n14224761)
9.  [BP Amoco to buy Burmah
    Castrol](http://findarticles.com/p/articles/mi_qa3755/is_200004/ai_n8888405)
10. [BP sells chemical unit for
    £5bn](http://news.bbc.co.uk/1/hi/business/4321046.stm)
11. [Errors led to BP refinery
    blast](http://news.bbc.co.uk/2/hi/business/4557201.stm)
12. [BP puts 100 gas stations up for sale in Colorado.（British Petroleum
    Company
    PLC）](http://www.accessmylibrary.com/premium/0286/0286-8415795.html)
13. [Gas station signs of
    change](http://nl.newsbank.com/nl-search/we/Archives?p_product=RM&p_theme=rm&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=10DF72B65CF64A98&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM)
14. [BP to Sell Most Company-Owned, Company-Operated Convenience Stores
    to
    Franchisees](http://www.bp.com/genericarticle.do?categoryId=2012968&contentId=7038464)

15. [Oil Gushes into Arctic Ocean from BP
    Pipeline](http://www.commondreams.org/headlines06/0321-06.htm)
16.
17. "BP Set to Leave Russia Gas Project" by Guy Chazan and Gregory
    White, Wall Street Journal, 2007-06-22 p. A3.
18.
19. [BP CEO set to
    retire](http://findarticles.com/p/articles/mi_qa5322/is_200702/ai_n21282860)
20. [BP's Browne quits over
    lie](http://www.guardian.co.uk/business/2007/may/02/media.pressandpublishing)