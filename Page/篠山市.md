**篠山市**（）是位於[日本](../Page/日本.md "wikilink")[兵庫縣東部的城市](../Page/兵庫縣.md "wikilink")。轄區東西寬30[公里](../Page/公里.md "wikilink")，南北長20公里，主要市區位於篠山盆地內，四周有207.63平方公里的區域為山林，擁有豐富的自然環境。特產為被稱為「丹波-{黑}-」的。

## 歷史

在16世紀初，[波多野稙通曾在此築有](../Page/波多野稙通.md "wikilink")，此後此地由治理直到16世紀末遭[織田信長佔領](../Page/織田信長.md "wikilink")。

由於位於[京都前往](../Page/京都.md "wikilink")[山陰道的要衝](../Page/山陰道_\(日本\).md "wikilink")，[江戶時代初期由](../Page/江戶時代.md "wikilink")[德川家康指派](../Page/德川家康.md "wikilink")[松平康重在此新建](../Page/松平康重.md "wikilink")[篠山城取代原本的八上城](../Page/篠山城.md "wikilink")，同時設立；此後篠山藩雖然經歷多次轉封，但藩主皆由[譜代大名擔任](../Page/譜代大名.md "wikilink")。

1871年[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，最初曾短暫設置篠山縣，但旋即被併入[豐岡縣](../Page/豐岡縣.md "wikilink")，1876年再併入[兵庫縣](../Page/兵庫縣.md "wikilink")。1889年，日本實施[町村制](../Page/町村制.md "wikilink")，[多紀郡下設置了](../Page/多紀郡.md "wikilink")****、、、、、、、、、、、、、、、、、今田村，共1町17村。

1950年代的[昭和大合併期間](../Page/市町村合併.md "wikilink")，多紀郡下的町村被整併為**篠山町**、、、、、，共6町。1975年，其中的篠山町、城東町、多紀町再次整併，合併後的名稱仍為**篠山町**。

1999年，多紀郡下全部的4個町決定合併為**篠山市**，當時合併後人口並未達5萬，但因適用「行政區劃合併後人口超過4萬可設置為市」的特別規定，也成為全日本第一個適用此特別規定的行政區劃。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1926年</p></th>
<th><p>1926年-1944年</p></th>
<th><p>1944年-1954年</p></th>
<th><p>1954年-1989年</p></th>
<th><p>1989年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>篠山町</p></td>
<td><p>1955年4月20日<br />
篠山町</p></td>
<td><p>1975年3月28日<br />
篠山町</p></td>
<td><p>1999年4月1日<br />
篠山市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>八上村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>畑村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>城北村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>岡野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>日置村</p></td>
<td><p>日置村</p></td>
<td><p>1955年4月10日<br />
城東村</p></td>
<td><p>1960年1月1日<br />
城東町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1892年1月8日<br />
後川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>雲部村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>福住村</p></td>
<td><p>1955年4月15日<br />
多紀村</p></td>
<td><p>1960年1月1日<br />
多紀町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大芋村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>村雲村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>南河內村</p></td>
<td><p>1955年1月1日<br />
西紀村</p></td>
<td><p>1960年1月1日<br />
西紀町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>北河內村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>草山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大山村</p></td>
<td><p>1955年4月15日<br />
丹南町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>味間村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>城南村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>古市村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>今田村</p></td>
<td><p>1960年4月1日<br />
今田町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）
      - [福知山線](../Page/福知山線.md "wikilink")：（←[三田市](../Page/三田市.md "wikilink")）
        -  -  -  - [篠山口車站](../Page/篠山口車站.md "wikilink") -  -
        （[丹波市](../Page/丹波市.md "wikilink")→）

過去還曾有[日本國有鐵道](../Page/日本國有鐵道.md "wikilink")[篠山線](../Page/篠山線.md "wikilink")，從篠山口車站往東穿越市區；但此路線已於1972年停駛。

<File:Kusano> stn Hyogo pref.jpg|草野車站 <File:Furuichi> station.jpg|古市車站
<File:MinamiYashiro> station.jpg|南矢代車站 <File:Sasayamaguchi> west
01.JPG|篠山口車站

### 道路

  - 高速道路

<!-- end list -->

  - [舞鶴若狹自動車道](../Page/舞鶴若狹自動車道.md "wikilink")：（三田市） -
    [丹南篠山口交流道](../Page/丹南篠山口交流道.md "wikilink") - （丹波市）

<!-- end list -->

  - 一般國道

<!-- end list -->

  - [國道173號](../Page/國道173號.md "wikilink")、、

## 觀光資源

  - 遺跡

  - [篠山城遺跡](../Page/篠山城.md "wikilink")

  -   -
  - ：舊篠山藩廳

  - 本堂

  -
  -
  -
  -
  - ：1923年完工、舊篠山町役場

  -
  -
  -
  -
  -
  -
  - ：觀賞[雲海的景點](../Page/雲海.md "wikilink")

### 祭典、活動

  -
<File:SAKAZUKIGATAKE> UNKAI2.jpg|杯岳的雲海 <File:大正ロマン館（旧篠山町役場>）.jpg|大正浪漫馆
[File:篠山城夜桜.JPG|筱山城和夜里的樱花](File:篠山城夜桜.JPG%7C筱山城和夜里的樱花) <File:Sasayama>
Okachi-machi05st3200.jpg|御徒士町武家屋敷群 <File:Sasayama>
Kawara-machi09s4592.jpg|河原町妻入商家群 <File:140510> Tamba Traditional Art
Craft Park Sue no Sato Sasayama Hyogo pref Japan02s3.jpg|立杭陶之鄉
<File:140510The> Museum of Ceramic Art, Hyogo Sasayama Hyogo pref
Japan01bs5.jpg|兵庫陶藝美術館 \[<File:Sasayama> Municipal Museum of
History04st3200.jpg|篠山市立歴史美術館
[File:デカンショ祭り.JPG|デカンショ祭り](File:デカンショ祭り.JPG%7Cデカンショ祭り)

## 姊妹、友好城市

  - 日本

<!-- end list -->

  - [犬山市](../Page/犬山市.md "wikilink")（[愛知縣](../Page/愛知縣.md "wikilink")）
  - [常滑市](../Page/常滑市.md "wikilink")（愛知縣）
  - 以「丹波的小京都」加盟[全國京都會議](../Page/全國京都會議.md "wikilink")

<!-- end list -->

  - 海外

<!-- end list -->

  - [瓦拉瓦拉](../Page/瓦拉瓦拉_\(华盛顿州\).md "wikilink")（[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[瓦拉瓦拉縣](../Page/瓦拉瓦拉縣_\(華盛頓州\).md "wikilink")）

  - [埃皮達魯斯](../Page/埃皮達魯斯.md "wikilink")（[希臘](../Page/希臘.md "wikilink")[伯羅奔尼撒](../Page/伯羅奔尼撒_\(大區\).md "wikilink")）

## 本地出身之名人

  - [本庄繁](../Page/本庄繁.md "wikilink")：帝國陸軍大將，曾任[關東軍司令官](../Page/關東軍.md "wikilink")，[九一八事變和侵占中國東北的主要策劃者之一](../Page/九一八事變.md "wikilink")。

  - [河合隼雄](../Page/河合隼雄.md "wikilink")：心理學者、曾任[文化廳長官](../Page/文化廳.md "wikilink")

  - [河合雅雄](../Page/河合雅雄.md "wikilink")：動物學家

  - ：歌手、模特兒、[girl next door成員](../Page/girl_next_door.md "wikilink")

## 參考資料

## 相關條目

  - [篠山城](../Page/篠山城.md "wikilink")

  -
  - [多紀郡](../Page/多紀郡.md "wikilink")

## 外部連結

  - [篠山市觀光情報](http://tourism.sasayama.jp/)

  -