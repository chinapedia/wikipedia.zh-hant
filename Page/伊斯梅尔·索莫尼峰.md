**伊斯梅爾索莫尼峰**（，[拉丁化](../Page/塔吉克文拉丁化.md "wikilink")：Qullai Ismoili
Somonī；），[塔吉克](../Page/塔吉克.md "wikilink")[戈爾諾-巴達赫尚自治州西北部的](../Page/戈爾諾-巴達赫尚自治州.md "wikilink")[科学院山脉的](../Page/科学院山脉.md "wikilink")[山峰](../Page/山峰.md "wikilink")，位於[帕米爾高原上](../Page/帕米爾高原.md "wikilink")，[海拔](../Page/海拔.md "wikilink")，相對高度，為塔吉克的[最高峰](../Page/各国最高点列表.md "wikilink")。也是前苏联境内的最高峰。

## 名稱

伊斯梅爾索莫尼峰曾被懷疑與[加爾莫峰為異名同峰](../Page/加爾莫峰.md "wikilink")，但於1932年確認它們是2個不同的山峰。伊斯梅爾索莫尼峰在1933－1962年稱為「**史太林峰**」（以[蘇共總書記](../Page/蘇聯領導人列表.md "wikilink")[約瑟夫·史太林命名](../Page/約瑟夫·史太林.md "wikilink")）（，[拉丁化](../Page/俄文拉丁化.md "wikilink")：Pik
Stalina），1962－1998年稱為「**共產主義峰**」（；，拉丁化：Pik Kommunizma）。

## 攀登史

1933年9月9日，[蘇聯攀山家](../Page/蘇聯.md "wikilink")[葉夫根尼·阿巴拉科夫首次登頂](../Page/葉夫根尼·阿巴拉科夫.md "wikilink")\[1\]。

## 參考資料

  - [The Free Dictionary](../Page/TheFreeDictionary.com.md "wikilink"):
    *[Imeni Ismail Samani
    Peak](http://encyclopedia2.thefreedictionary.com/Ismoil+Somoni+Peak)*

## 外部資訊

  -
  - ["Pik Imeni Ismail
    Samani"](http://www.peakware.com/peaks.html?pk=52)

[分類:塔吉克山峰](../Page/分類:塔吉克山峰.md "wikilink")

1.  The Great Soviet Encyclopaedia. Communizma Peak