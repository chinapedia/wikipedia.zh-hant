**无政府主义**（），又译作**安那其主义**，是一系列[政治哲学思想](../Page/政治哲学.md "wikilink")。其目的在於提升個人自由及廢除政府當局與所有的政府管理機構\[1\]。英语中的无政府主义“Anarchism”源于[希腊语单词](../Page/希腊语.md "wikilink")“”，意思是没有统治者。所以被翻译成中文时，根据这一最基本的特征译成“无政府主义”，也有文献音译为“安那其主义”。無政府主義包含了众多[哲学体系和](../Page/哲学.md "wikilink")[社会运动实践](../Page/社会运动.md "wikilink")。它的基本立场是反对包括[政府在内的一切统治和](../Page/政府.md "wikilink")[权威](../Page/权威.md "wikilink")，提倡个体之间的自助关系，关注[个体的](../Page/个体.md "wikilink")[自由和](../Page/自由.md "wikilink")[平等](../Page/平等.md "wikilink")；其政治诉求是消除政府以及社会上或经济上的任何[独裁统治关系](../Page/独裁.md "wikilink")。對大多數無政府主義者而言，「無政府」一詞並不代表混亂[無政府狀態](../Page/無政府狀態.md "wikilink")、[虛無](../Page/虚无主义.md "wikilink")、或[道德淪喪的狀態](../Page/道德.md "wikilink")，而是一種由自由的個體自願結合，以建立互助、自治、反獨裁主義的和諧社會。有人认为[莊子是最早的无政府主义者](../Page/莊子.md "wikilink")\[2\]。雖然他們都有著反對[國家的共同特色](../Page/國家.md "wikilink")，但卻在其他議題上有著不同的立場，包括是否進行武裝鬥爭、或以不合作運動建立社會的問題上產生分歧，而在[經濟的觀點上也有顯著的差異](../Page/經濟.md "wikilink")，從主張[財產徹底](../Page/財產.md "wikilink")[公有化的](../Page/公有化.md "wikilink")[集體主義流派](../Page/集體主義.md "wikilink")，至主張[私人財產和](../Page/私人財產.md "wikilink")[自由市場的](../Page/自由市場.md "wikilink")[個人主義流派](../Page/個人主義.md "wikilink")，[政治光譜分布相當廣泛](../Page/政治光譜.md "wikilink")。

## 起源

在[信史前的](../Page/信史.md "wikilink")[狩猎时代](../Page/狩猎时代.md "wikilink")，人类主要自发地以大家庭的形式聚居；没有所谓[政府和](../Page/政府.md "wikilink")[国家](../Page/国家.md "wikilink")，只有小规模的财富积累和劳动分工，也没有法令和统治者。無政府主義者主張這段時期為原始的无政府状态。\[3\]
[WilliamGodwin.jpg](https://zh.wikipedia.org/wiki/File:WilliamGodwin.jpg "fig:WilliamGodwin.jpg")
有人认为可以从[道家圣人老子](../Page/道家思想.md "wikilink")\[4\]、庄子的作品发现无政府主义的宗旨，例如“闻在宥天下，不闻治天下也”\[5\]，“彼窃钩者诛，窃国者为诸侯”\[6\]\[7\]。同样，无政府主义倾向也可以溯源至[古代希腊哲学家的观点](../Page/古希腊.md "wikilink")，比如[斯多葛派哲学创始人](../Page/斯多葛派.md "wikilink")[芝诺](../Page/季蒂昂人芝诺.md "wikilink")，还有[阿瑞斯提普斯](../Page/阿瑞斯提普斯.md "wikilink")，他说智者不应该为政府放弃自己的[自由](../Page/自由.md "wikilink")。之后的社会运动參與者包括[中世纪的](../Page/中世纪.md "wikilink")[自由人](../Page/自由人.md "wikilink")（Freien
Geistes）、[再洗礼派教徒](../Page/再洗礼派.md "wikilink")、[英格蘭的](../Page/英格蘭.md "wikilink")[掘地者和激进者](../Page/掘地者.md "wikilink")（Ranters）團體，也都被认为具有无政府主义的概念。

最早的“无政府”的使用出现在前467年[埃斯库罗斯的戏剧](../Page/埃斯库罗斯.md "wikilink")《[七将攻忒拜](../Page/七将攻忒拜.md "wikilink")》。其中，[安提戈涅公主的哥哥](../Page/安提戈涅.md "wikilink")[波吕尼刻斯参与攻打](../Page/波吕尼刻斯.md "wikilink")[忒拜](../Page/忒拜.md "wikilink")，统治者因此禁止埋葬波吕尼刻斯的尸体作为惩罚，安提戈涅公开拒绝接受禁令，说“就算没有别人来，我也要自己埋葬他，并独自承担埋葬我的亲兄弟所带来的危险。对这种挑衅对抗城市统治者的无政府行为，我并不羞耻（*ekhous
apiston tênd anarkhian polei*）。”

[古希腊也被西方认为是首创了作为哲学思想的无政府主义](../Page/古希腊.md "wikilink")，斯多葛派哲学家芝诺作出了这一贡献，他被[克鲁泡特金称为](../Page/克鲁泡特金.md "wikilink")“古希腊最好的无政府主义诠释者”。按克鲁泡特金的总结，芝诺“批判了国家万能、干涉和管制，并提出了个人的道德法则的重要”。在[古希腊哲学体系之内](../Page/古希腊哲学.md "wikilink")，芝诺无政府自由社群的主张和政府论相对，后者的代表性表述为[柏拉图共和政体和](../Page/柏拉图#柏拉图思想.md "wikilink")[乌托邦](../Page/乌托邦.md "wikilink")。芝诺提出人有自卫本能，这让人变得[利己](../Page/利己主义.md "wikilink")，自然也相应的赋予了人类另一本能——[社会性](../Page/社会性.md "wikilink")。像一些现代无政府主义者一样，他相信如果人们按照本能行事，就不需要[法律](../Page/法律.md "wikilink")、[法院或是](../Page/法院.md "wikilink")[警察](../Page/警察.md "wikilink")，不需要[寺庙和公共](../Page/寺庙.md "wikilink")[偶像崇拜](../Page/偶像崇拜.md "wikilink")，也不用[钱](../Page/钱.md "wikilink")（[礼物经济代替交换](../Page/礼物经济.md "wikilink")）。年代久远，我们只是通过引文支离破碎地了解芝诺的信仰。

16世纪[欧洲的](../Page/欧洲.md "wikilink")[再洗礼派教徒往往被认为是现代无政府主义的](../Page/再洗礼派.md "wikilink")[宗教先驱](../Page/宗教.md "wikilink")。[罗素在](../Page/伯特蘭·羅素.md "wikilink")《[西方哲学史](../Page/西方哲學史_\(羅素\).md "wikilink")》中说[再洗礼派教徒](../Page/再洗礼派.md "wikilink")“批判所有的法律，他们认为所有好人都被[圣灵指引](../Page/圣灵.md "wikilink")……以这个前提他们到达了[共产主义](../Page/共产主义.md "wikilink")……”\[8\]。小说《[Q](../Page/Q_\(小说\).md "wikilink")》则描述了这一活动及其革命性的意识形态。1548年，[埃蒂安·德拉博埃蒂写了](../Page/埃蒂安·德拉博埃蒂.md "wikilink")《》，一篇探究人们服从统治者的文章。17世纪[英國內戰時](../Page/英國內戰.md "wikilink")，[掘地者中的Gerrard](../Page/掘地者.md "wikilink")
Winstanley则出版了一个小册子，提倡公社所有制、以小块耕地为单位的社会组织和经济组织，它也被认为是现代无政府主义的先驱。\[9\]

第一篇明确提出没有政府的文章是[埃德蒙·伯克的](../Page/埃德蒙·伯克.md "wikilink")《[为自然社会而辩护](https://web.archive.org/web/20060827072509/http://oll.libertyfund.org/Texts/LFBooks/Burke0061/Vindication/0339_Bk.html)》（1756年），他还匿名写过其他一些文章，但他声称那都是讽刺文学。尽管如此，一些学者怀疑伯克的声明是由于详尽论述的论点和文章的严肃性。1793年[威廉·戈德温出版了](../Page/威廉·戈德温.md "wikilink")《[对政治正义的评论](https://web.archive.org/web/20051217131905/http://web.bilkent.edu.tr/Online/www.english.upenn.edu/jlynch/Frank/Godwin/pjtp.html)》，其中他借批评政府发表他的自由社会的观点。有些人认为这是首次无政府主义论述，把戈德温称作“无政府哲学的创立者”\[10\]。除了出于个人在面对外来侵犯时会自发联合，戈德温反对其它任何有组织的个人劳动合作，认为这会妨碍善行，他甚至反对[管弦乐团](../Page/管弦乐团.md "wikilink")。他是早期的自由主义支持者，支持个人私有产权，并定义它为一种“帝权，使每个人有通过勤劳获取产出的资格”。但直到這個時期無政府主義的運動仍未存在，而「無政府主義者」一詞也仍然是[法國大革命中](../Page/法國大革命.md "wikilink")[資產階級的](../Page/資產階級.md "wikilink")[吉倫特派用以羞辱其他較激進派系的負面用語](../Page/吉倫特派.md "wikilink")。

## 個人主義流派

### 麥克斯·施蒂納的利己主義

[Max_stirner.jpg](https://zh.wikipedia.org/wiki/File:Max_stirner.jpg "fig:Max_stirner.jpg")所繪\]\]
[麥克斯·施蒂納在他所著的](../Page/麥克斯·施蒂納.md "wikilink")《自我及所有》（Der Einzige und
sein
Eigentum）一書中，主張一般社會所公認的制度—包括國家的概念、「財產是一種權利」的概念、自然權利的概念、和一般對社會的概念—都只是幽靈在腦海裡作祟的幻象，主張「個人便是社會的本質」。他主張利己主義和一種形式的非道德主義，個人只有在切及他們自身利益的情況下才會聯合起來成為「利己主義者們的組織」。對他而言，財產只是由能力決定的：「知道如何去擁有它們、保護它們，那便是他的財產」並且「在我能力所及之內的，便是我的東西。只要我能顯示出自己佔有者的身分，那我便是這個東西的所有人。」

施蒂納從沒有自稱他自己為無政府主義者—他只接受「利己主義者」這個稱號。不過他仍被許多人視為是無政府主義者，因為他反對國家、法律、和政府，同時他的理念也影響了許多無政府主義者，雖然對他的理念的解讀有著許多分歧。許多無政府主義者如[班傑明·塔克都宣稱受了施蒂納的影響](../Page/班傑明·塔克.md "wikilink")，[埃玛·戈尔德曼也在演講中宣揚他的理念](../Page/埃玛·戈尔德曼.md "wikilink")。\[11\]

### 個人無政府主義

[LysanderSpooner.jpg](https://zh.wikipedia.org/wiki/File:LysanderSpooner.jpg "fig:LysanderSpooner.jpg")\]\]

個人無政府主義是一種反抗[集體主義的哲學思想](../Page/集體主義.md "wikilink")，以及特別強調[個人地位和個人自治權的無政府主義流派](../Page/個人主義.md "wikilink")。個人無政府主義主要是在[美國發展](../Page/美國.md "wikilink")，他們最顯著的特色是擁護對勞動產品的私人所有權。個人無政府主義也源自於一些歐洲的思想家，包括了威廉·戈德溫、皮埃爾-約瑟夫·普魯東、[王尔德](../Page/王尔德.md "wikilink")、麥克斯·施蒂納等人，不過個人無政府主義的大部分理論主要還是源自於美國的思想家如[萊桑德·斯波納](../Page/萊桑德·斯波納.md "wikilink")、[約書亞·沃倫](../Page/約書亞·沃倫.md "wikilink")、[班傑明·塔克](../Page/班傑明·塔克.md "wikilink")、[史蒂芬·皮爾·安德魯斯](../Page/史蒂芬·皮爾·安德魯斯.md "wikilink")、和[亨利·戴维·梭罗等人的影響](../Page/亨利·戴维·梭罗.md "wikilink")。個人無政府主義有時也被視為是[古典自由主義的分支](../Page/古典自由主義.md "wikilink")，因此也被一些人稱為「自由無政府主義」[2](http://www.weisbord.org/conquest8.htm)。其他19世紀的個人主義者包括了[赫伯特·斯宾塞](../Page/赫伯特·斯宾塞.md "wikilink")、[史蒂芬·皮爾·安德魯斯](../Page/史蒂芬·皮爾·安德魯斯.md "wikilink")、。

個人主義的經濟理論（互助主義）則是根基於[劳动价值理論上](../Page/劳动价值.md "wikilink")，認為產品的價值是根基於生產過程中的勞動力量大小所決定的，因此他們認為將一件商品的價格調高超過了生產的成本是不道德的，這也被稱為「成本即為價格限制」理論。為了確保勞工能取得「產品的全部價值」，他們主張商品被收購時的價格應該與生產時的勞動程度相等。如此一來勞工們取得的價值便與勞動程度相等，而那些沒有勞動的人則不會取得任何價值。這樣一來雇主便無法透過僱佣關係從雇員的勞動中取得任何利益，以避免剝削的產生。而貸款所獲得的利息通常被認為是高利貸的行為，因為這種收入被視為是不勞而獲的。對個人主義者而言，來自利息、僱佣、和土地租金的利潤只有可能在政府主導的「壟斷」（[強迫性壟斷](../Page/強迫性壟斷.md "wikilink")）和「特權」下才有可能產生，因為只有政府的力量才能限制市場的競爭並且集中財富於少數人手上。

### 美國個人無政府主義

美國個人無政府主義的背景可以追溯至[美國革命](../Page/美國革命.md "wikilink")，以及1794年抗繳酒稅的[威士忌酒反抗](../Page/威士忌酒反抗.md "wikilink")，和其他在美國獨立後類似的地區性反抗行動。[社會契約論正當化了北美殖民地從大英帝國分離的舉動](../Page/社會契約論.md "wikilink")，西部邊疆的開拓者進一步延伸這個理論，主張開拓者們在以村莊為根基的小規模社區生活，這些郡和市鎮應有權脫離美國聯邦。雖然威士忌酒反抗被[喬治·華盛頓平息了](../Page/喬治·華盛頓.md "wikilink")，[西維吉尼亞州還是在](../Page/西維吉尼亞州.md "wikilink")[美國內戰時從](../Page/美國內戰.md "wikilink")[維吉尼亞州裡分離出來](../Page/維吉尼亞州.md "wikilink")，維吉尼亞州则加入了脫離聯邦的[美利堅聯盟國](../Page/美利堅聯盟國.md "wikilink")。
[BenjaminTucker.jpg](https://zh.wikipedia.org/wiki/File:BenjaminTucker.jpg "fig:BenjaminTucker.jpg")\]\]
美國的個人無政府主義的特色是極為支持對於勞動產品的私人所有權，和一個競爭性的自由市場經濟\[12\]。在1825年，[约书亚·沃伦參與了](../Page/约书亚·沃伦.md "wikilink")[罗伯特·欧文所領導的](../Page/罗伯特·欧文.md "wikilink")[社群主義實驗](../Page/社群主義.md "wikilink")，試圖建立一個和諧的移民社區，但不久後實驗由於眾多內部的衝突而告失敗。沃倫指責失敗的原因是因為社群裡的成員缺乏自我所有權，以及缺乏私人的財產。沃倫繼續組織了幾次在他看來是尊重「自我所有權」的無政府主義實驗社區。在1833年沃倫撰寫出版了*The
Peaceful
Revolutionist*期刊，一些人認為這是第一份無政府主義的期刊。本傑明·塔克稱沃倫是「第一個闡述並公式化了無政府主義原則的人。」(Liberty
XIV (1900年12月):1)

[本傑明·塔克在認識了约书亚](../Page/本傑明·塔克.md "wikilink")·沃伦和[威廉·貝奇德·葛林後開始對無政府主義產生興趣](../Page/威廉·貝奇德·葛林.md "wikilink")。他編輯並出版了*Liberty*期刊，從1881年8月至1908年4月為止，這份期刊被廣泛認為是英語世界裡所發行過最好的個人無政府主義期刊。塔克的個人無政府主義概念結合了許多理論家的概念：葛林的互助銀行的概念、沃倫的「成本即為價格限制」的概念（一種非正統的[劳动价值理論](../Page/劳动价值.md "wikilink")）、普魯東的市場無政府主義、麥克斯·施蒂納的利己主義、和[赫伯特·斯宾塞的](../Page/赫伯特·斯宾塞.md "wikilink")「平等自由的法律」概念。塔克強烈主張個人應有權佔有他們勞動所得的產品為他們的「[私人財產](../Page/所有权.md "wikilink")」，並相信應以市場經濟來貿易這些產品\[13\]。不過，如同其他的無政府主義者，他反對[智慧財產的權利](../Page/智慧財產權.md "wikilink")，也反對其他在土地和自然資源上的所有權，而支持所謂的「佔有並使用」的概念。他主張在一個沒有國家控制的真正自由的市場經濟體制裡，缺乏競爭的存在將能消除利潤、利息、和租金的產生，並能保證所有勞工可獲得他們勞動的全部成果。許多個人主義者（如塔克和尤拉斯）支持市場保護那些進行生產的勞工、佔有並使用土地的農民。

### 無政府資本主義

[Murray_Rothbard.jpg](https://zh.wikipedia.org/wiki/File:Murray_Rothbard.jpg "fig:Murray_Rothbard.jpg")

無政府資本主義是以美國為根基發展的一種理論，主張[自由放任的](../Page/自由放任.md "wikilink")[資本主義經濟體制](../Page/資本主義.md "wikilink")，在這樣的體制裡將沒有權力機構能禁止任何人經由[自由市場提供各種服務](../Page/自由市場.md "wikilink")—也就是那些在現代大多數政府都允許自由提供的服務，除了某些由國家[壟斷的服務如國防](../Page/壟斷.md "wikilink")、對私人財產的保護（警察）、立法/管理機構（法庭）和環境的保護。由於無政府資本主義並不反對[利潤](../Page/利潤.md "wikilink")、租金、利息、或[資本主義](../Page/資本主義.md "wikilink")，許多無政府主義者拒絕承認無政府資本主義是無政府主義的一種形式\[14\]。一些無政府主義的組織如西班牙的全國勞工聯盟和英國的[無政府主義者聯盟](../Page/無政府主義者聯盟.md "wikilink")（Anarchist
Federation）都明確表示反資本主義的立場。不過，一些學者的確將無政府資本主義看作是無政府主義的一種形式。\[15\]

[穆瑞·羅斯巴德綜合了](../Page/穆瑞·羅斯巴德.md "wikilink")[古典自由主義和](../Page/古典自由主義.md "wikilink")[奧地利經濟學派的觀念](../Page/奧地利經濟學派.md "wikilink")，發展成為當代無政府資本主義的理論。羅斯巴德派的無政府資本主義者相信私人財產只有可能是從勞工們生產的產品裡產生，而這只能透過貿易、送禮、或拋棄的程序來進行轉換。一些[小政府主義者如](../Page/小政府主義.md "wikilink")[艾茵·兰德和](../Page/艾茵·兰德.md "wikilink")[罗伯特·海因莱茵也影響了無政府資本主義](../Page/罗伯特·海因莱茵.md "wikilink")。但絕大多數的無政府資本主義者都相信，如果沒有了資本主義，無政府主義絕不可能存在，因為若沒有國家的阻擋，任何自由的社會將自然地而不可避免地發展出資本主義。也因此，羅斯巴德稱「資本主義是無政府主義的最佳路線，而無政府主義也是資本主義的最佳路線。」\[16\]

無政府資本主義通常根基於[互不侵犯原則上](../Page/互不侵犯原則.md "wikilink")，先行使用暴力便是在道德上錯誤的行為。這和[正義戰爭理論類似](../Page/正義戰爭理論.md "wikilink")，先行侵略是錯誤的，但報復的反擊（同比例的）是被允許的。支持這種[自然法原則的無政府資本主義者指出大多數人都能接受在個人層次的互不侵犯原則](../Page/自然法.md "wikilink")，但卻無法接受其套用在政府的層面，也因此給予了政府一種超高道德的地位。無政府資本主義者堅持應該將互不侵犯原則套用在政府層面上。其他的無政府資本主義者則以功利主義的基礎來解釋，認為政府的壟斷有著和其他壟斷一樣的缺點，例如昂貴卻品質低下的公共服務。

一些無政府資本主義者，包括一些自由意志主義的歷史學家，認為類似無政府資本主義的哲學在羅斯巴德之前便已存在，例如[古斯塔夫·德·莫利納里和](../Page/古斯塔夫·德·莫利納里.md "wikilink")\[17\]\[18\]。莫利納里和赫伯特都曾明白拒絕無政府主義的標籤，將無政府主義與當時盛行的社會主義流派畫上等號。莫利納里只自稱為經濟學家，赫伯特則自稱為[自願主義者](../Page/自願主義.md "wikilink")（Voluntaryism）。

无政府资本主义的理论难题在于，如何在无政府的前提下维持资本主义体制。

## 集體主義流派

### 皮埃尔-约瑟夫·普鲁东

[Proudhon-children.jpg](https://zh.wikipedia.org/wiki/File:Proudhon-children.jpg "fig:Proudhon-children.jpg")
（1809-1865）\]\]
[皮埃尔-约瑟夫·普鲁东通常被認為是第一名自稱無政府主義的人](../Page/皮埃尔-约瑟夫·普鲁东.md "wikilink")，他在1840年出版的《[什么是财产？](../Page/什么是所有权？.md "wikilink")》一書中以此自稱，因此一些人主張普鲁东是現代無政府主義思想的創立者\[19\]。普魯東發展了一種以自發性秩序為根基的社會理論，稱為[互助主義](../Page/互助主義.md "wikilink")（mutuellisme），在這種社會裡，並沒有類似政府的中央控制機構，使個體得以依照各自的意願追求他們自身的利益。他對於資本主義、國家、和既定宗教制度的反抗啟發了許多之後的無政府主義者，也使他成為當時主要的社會主義思想家之一。對普魯東而言：

> 「資本……在政治領域上是和政府類似的東西……經濟上的資本主義、政府或權力機構的政治、和教會在神學上的觀點三者都是同樣一種概念，以許多形式互相連結。攻擊其中任何一者便等同攻擊全部三者……如同資本對勞工的影響、國家對自由的影響、教會對心靈的影響一般。這種三位一體的專制制度在實踐上和理論上都是有害的。壓迫一個人最有效的方法便是同時地奴役他的身體、他的意志、他的理性。」

\[20\]

在《什么是财产？》一書中普魯東著名的宣稱「財產權是一種偷竊」，在書裡他反對建立一種「財產權」（propriété）的制度，以避免財產所有人得以隨意「使用和濫用」他們的財產—例如剝削勞工以求取利潤\[21\]。然而普魯東的看法在後來完全改變，他改為宣稱「財產權是一種自由」，認為個人能以財產權來抗衡國家的權力。表面上看來普魯東的理論是自相矛盾的，但如同在他死後才出版的《財產權理論》（Theory
of
Property）一書裡主張的，普魯東「強烈地對比他所謂的『財產』—依據平等和正義的原則有限制的運用資源、資本和產品的權利—與現代『財產權』的形式」，但他最後也「承認了只有在所有權不受任何大小限制的情況下，自由才得以獲得保證」。\[22\]在這個理論基礎上，普魯東說：

> 「我們應該從哪裡找尋對抗國家的制衡力量？只有財產才能辦的到…完全的國家權利是與完全的財產所有權利相衝突的。財產是有史以來最偉大的革命力量。」

普魯東的互助主義根基於在工作場所、土地、和房屋上對於私人財產的對抗，主張這些東西應該被他們的使用者「佔有」（「所有累積的資本屬於社會的財產，沒有人可以獨占其所有權。」\[23\]）同時他也反對共產主義，主張應該支付勞工報酬，但他也反對資本主義的[僱傭勞動制](../Page/僱傭勞動制.md "wikilink")（如某人純粹藉由他人的勞動賺取利潤）\[24\]。他進一步主張勞工們應該「將他們自行組織起來成為一個民主社會，所有成員應享有同等的身分，以免再次淪為封建制度」

普魯東的觀念影響了法國的勞動階級運動，他的追隨者也在法國的[1848年革命和](../Page/1848年革命.md "wikilink")1871年的[巴黎公社中相當活躍](../Page/巴黎公社.md "wikilink")。無政府共產主義者如彼得·克魯泡特金則反對普魯東支持勞動產品的「私人財產」（如報酬，或稱之「勞動結果的酬勞」），而是主張由勞工們自由的分配產品。\[25\]

### 無政府共產主義

普魯東厭惡共產主義在集體主義上的觀點\[26\]。而巴枯寧原本還相當同情之，將[共产党宣言翻譯成了俄文](../Page/共产党宣言.md "wikilink")。之後巴枯寧也與共產主義斷絕了關係，但他仍然保持中央集權的概念，成為[無政府集體主義](../Page/無政府集體主義.md "wikilink")。無政府集體主義支持支付勞工酬勞，但仍堅持在革命後轉型為共產主義體制的可能性，由共產體制依據每個人的需求來分配產品。巴枯寧的學生將巴枯寧的概念寫入論文裡*[Ideas
on Social
Organization](http://www.marxists.org/reference/archive/guillaume/works/ideas.htm)*(1876):「當……生產的速度高過消耗的速度時……所有人將能從社會所儲備的商品裡拿取他所需要的部分，無須擔憂匱乏；如此一來將能大幅提升自由而平等的勞工們的道德感，將能避免、或大幅減少濫用和浪費的現象。」

Joseph Déjacque是早期的無政府共產主義者之一，他也是第一個自稱為自由社會主義（Libertarian
socialism）的人\[27\]。與普魯東不同的，他主張「擁有產品的權利並不屬於生產它的勞工，而是屬於那些需要使用它的人」[4](http://www.blackrosebooks.net/anarism1.htm)。

[克鲁泡特金通常被視為無政府共產主義最重要的理論家](../Page/克鲁泡特金.md "wikilink")，在他所著的*The
Conquest of Bread*和*Fields, Factories and
Workshops*裡描繪出他的經濟理想，克魯泡特金覺得合作會比競爭更有益，主張合作是自然狀態的現象。他主張藉由人們自行「沒收全社會的財富」來廢止私人財產\[28\]，並以一個由人們自願組織、無階層分別的網絡來協調經濟運作\[29\]。他主張在無政府共產主義裡「房屋、田地、和工廠都不再是私人財產，而是歸屬於公社或國家的。」而貨幣、工資、和貿易將會被廢止\[30\]。個人和團體將會使用並控制他們所需要的資源，無政府共產主義的目標便是將「收割或製造出的產品分配給所有人，讓每個人自由的使用他們。」\[31\]除此之外，他重複的強調道（如其他無政府共產主義者）將不會強迫個人接受共產主義，主張「佔有他們所能耕種的土地大小」的小耕農們、「居住在對他們人數而言大小適當的房屋」的家庭們、和「使用他們自己的工具或紡織機」的工匠們都能自由的選擇他們想要的生活\[32\]。一些人認為無政府共產主義在佔有和財產的觀念上與普魯東的一些理念類似。

其他重要的無政府共產主義者還包括[埃玛·戈尔德曼](../Page/埃玛·戈尔德曼.md "wikilink")、[亞歷山大·貝克曼](../Page/亞歷山大·貝克曼.md "wikilink")、和。許多無政府工團主義者也認同無政府共產主義的目標。

一些無政府主義者則厭惡將共產主義與無政府主義融合。一些個人無政府者批評廢止私人財產是和自由不相容的。例如本傑明·塔克在他出版的作品裡\[[http://www.zetetics.com/mac/libdebates/apx1pubs.html\]批評無政府共產主義是「假冒的無政府主義](http://www.zetetics.com/mac/libdebates/apx1pubs.html%5D批評無政府共產主義是「假冒的無政府主義)」。\[33\]

### 第一國際

[Bakuninfull.jpg](https://zh.wikipedia.org/wiki/File:Bakuninfull.jpg "fig:Bakuninfull.jpg")
1814-1876\]\]
在歐洲，1848年的革命招致了嚴厲的鎮壓。到了1864年，[国际工人联合会](../Page/第一国际.md "wikilink")—有時簡稱為「第一國際」成立了，聯合了歐洲各地革命的流派，包括法國那些普魯東和[巴枯寧理念的擁護者](../Page/米哈依尔·巴枯宁.md "wikilink")、英國的工會主義者、[社會主義者](../Page/社會主義.md "wikilink")、和[社會民主主義者](../Page/社會民主主義.md "wikilink")。由於它確實連結了各地激進的勞工運動者，第一國際成了相當重要的組織。

[卡尔·马克思是當時第一國際裡的領導人和委員會裡的成員](../Page/卡尔·马克思.md "wikilink")。普魯東互助主義的擁護者則反對馬克思的[科學社會主義](../Page/科學社會主義.md "wikilink")，支持政治的節制（abstentionism）和小資產的擁有人。米哈依尔·巴枯宁在1868年也加入反對馬克思的行列，結合了第一國際裡反獨裁主義的社會主義區塊。首先，擁護馬克思的集產主義勞工將第一國際推向更具革命性質的方向，接著第一國際開始分裂為兩個派系，兩派各自以馬克思和巴枯寧為領導人。兩派最明顯的差異是在戰略上的，圍繞著巴枯寧的無政府集產主義者（依據克魯泡特金的說法）「只在經濟上直接與資本主義鬥爭，而不去介入在政治上的議會體制」[5](http://dwardmac.pitzer.edu/Anarchist_Archives/kropotkin/britanniaanarchy.html)。相反的馬克思和其擁護者則專注於在議會政體的活動，並尋求一個有著中央領導和掌控的政黨組織以進行鬥爭。

巴枯寧批評馬克思的理想為獨裁主義，並預言如果馬克思主義的政黨奪取了權力，他們將會變的和他們所反抗的[統治階級一樣糟糕](../Page/統治階級.md "wikilink")\[34\]。在1872年，[第一国际中冲突达到顶点](../Page/第一国际.md "wikilink")，马克思主义者靠人数优势在[海牙大会
(1872年)投票逐出了巴枯宁和巴枯宁主义者](../Page/海牙大会_\(1872年\).md "wikilink")，並將第一國際的總部遷往紐約。这通常被认为是无政府主义者和马克思主义者之间冲突的由来。

許多現代左翼內部的爭論都可以追溯回當初巴枯寧和馬克思的爭論，他們對於革命政黨所應扮演的角色以及革命後的社會問題上產生分歧。許多左翼份子、共產主義者和無政府主義者，反對傳統的[列宁主义和](../Page/列宁主义.md "wikilink")[布尔什维克以領導革命的政黨來組成新國家的概念](../Page/布尔什维克.md "wikilink")，這一部分是因為歷史上[蘇聯的失敗](../Page/蘇聯.md "wikilink")，也是因為這樣的革命只是以一個新的官僚階級來取代原有的獨裁官僚罷了。

### 無政府主義與勞工運動

[無政府工團主義是一種將勞工運動作為革命主要角色的無政府主義形式](../Page/無政府工團主義.md "wikilink")（「工團主義」一詞源於法文*syndicalisme*，意味著「工會主義」）。無政府工團主義主張建立革命性質的工會以爭取更好的工作條件、奪取對生產工具的控制、並以直接行動和總罷工等手段來推翻國家。無政府工團主義追求廢止工資制度，並廢止對生產工具的私人所有權—他們認為那是造成階級分裂的原因。
在[1871年的鎮壓後](../Page/巴黎公社.md "wikilink")，法國的無政府主義再度出現，影響了*Bourses de
Travails*自治勞工團體和工會。到了1895年，[法國總工會](../Page/法國總工會.md "wikilink")（Confédération
Générale du Travail,
CGT）成立了，成為第一個無政府工團主義的主要運動。1914年後CGT開始遠離無政府工團主義，在第一次世界大戰中採納了合作主義。在俄國的[布尔什维克革命成功後開始與馬克思主義的第三國際合作](../Page/布尔什维克.md "wikilink")。

西班牙的無政府主義工會聯盟在1870年代至1910年代間形成，最成功的是在1910年成立的[全國勞工聯盟](../Page/全國勞工聯盟_\(西班牙\).md "wikilink")（Confederación
Nacional del Trabajo,
CNT）。在1940年代前CNT是西班牙勞工階層最主要的政治勢力，在1934年擁有158萬名會員，CNT在[西班牙内战中扮演了重要的角色](../Page/西班牙内战.md "wikilink")。

[拉丁美洲的無政府主義也有著強烈的無政府工團主義成分](../Page/拉丁美洲.md "wikilink")。拉丁美洲主要的工會聯盟都採納了無政府工團主義的計畫，最顯著的是墨西哥、巴西、阿根廷、和烏拉圭[6](http://www.blackrosebooks.net/anarism1.htm)。在葡萄牙、義大利、德國、和日本也有顯著的無政府工團主義運動。CNT在1922年加入了[國際勞工協會](../Page/國際勞工協會.md "wikilink")（International
Workers
Association）—無政府工團主義為取代[第一国际而創建的聯盟](../Page/第一国际.md "wikilink")，在歐洲和拉丁美洲等15個不同的國家有將近2百萬名成員。

目前世界上最大的無政府主義運動是在西班牙，由西班牙[聯合總工會](../Page/聯合總工會_\(西班牙\).md "wikilink")（Confederación
General del Trabajo,
CGT）和CNT組成。CGT宣稱擁有定期繳交會費的成員60,000人，並在選舉中獲得超過一百萬張選票。其他活躍的工團主義運動還包括美國的[勞工團結聯盟](../Page/勞工團結聯盟.md "wikilink")（Workers
Solidarity
Alliance）和英國的[團結聯盟](../Page/團結聯盟_\(英國\).md "wikilink")。具革命性質的產業工會主義的[世界產業工人聯盟](../Page/世界產業工人聯盟.md "wikilink")（Industrial
Workers of the World）也依然存在，宣稱擁有2,000名定期繳交會費的成員。

### 俄國革命

1917年俄國革命}} 1917年的俄國革命對無政府主義的運動和理論都產生極大震撼。

在二月革命和十月革命中，許多無政府主義者原本支持布爾什維克的政變。然而，布爾什維克很快就轉過頭來對付無政府主義者和其他左翼反對派，衝突在1921年的[克倫斯塔叛變中達到頂點](../Page/克倫斯塔叛變.md "wikilink")。在俄國的無政府主義者被大量逮捕，或者轉為地下組織，有的人則加入了掌權的布爾什維克。在[烏克蘭](../Page/烏克蘭.md "wikilink")，由[內斯托爾·馬克諾所領導的無政府主義農民軍隊在](../Page/內斯托爾·馬克諾.md "wikilink")[俄国内战中同時與白軍與布爾什維克作戰](../Page/俄国内战.md "wikilink")。

被驅逐出境的美國無政府主義者[爱玛·戈尔德曼和她的丈夫亞歷山大](../Page/爱玛·戈尔德曼.md "wikilink")·貝克曼回到俄國後，與其他無政府主義者一同見證了布爾什維克的政策和對克倫斯塔叛變的鎮壓，兩人都在這時寫下了他們對俄國革命的看法，以揭露被布爾什維克掩蓋的真相。對他們而言，巴枯寧當初所預言的馬克思主義掌權的後果可說是再真實不過了。

布爾什維克在10月革命和接下來的俄國內戰中的勝利嚴重損害了國際間的無政府主義運動，許多勞工和激進份子將布爾什維克的勝利當作是革命的榜樣，共產主義的政黨大幅成長，大幅削弱了無政府主義和其他社會主義運動。在法國和美國，主要的工團主義運動如法國總工會和世界產業工人聯盟都重新進行組織，遠離無政府主義的路線，向[第三国际靠攏](../Page/第三国际.md "wikilink")。

包括內斯托爾·馬克諾在內的許多俄國無政府主義者流亡至巴黎，他們認為必須發展一種新形式的組織以與布爾什維克作區隔。他們在1926年發表了宣言，被稱為「自由共產主義組織綱領」（Organisational
Platform of the Libertarian Communists）\[35\]，被許多無政府共產主義者支持，不過也被許多人反對。

這份綱領持續影響一些現代的無政府主義團體，他們相信無政府主義運動應該圍繞著「理論的團結」、「戰術的團結」、「集體責任」、和「聯邦主義」的原則。以綱領為原則的團體還包括愛爾蘭的[勞工團結運動](../Page/勞工團結運動_\(愛爾蘭\).md "wikilink")（Workers
Solidarity
Movement）和北美的[東北部無政府共產主義聯盟](../Page/東北部無政府共產主義聯盟.md "wikilink")（North
Eastern Federation of Anarchist Communists）。

### 與法西斯主義的鬥爭

在1920和1930年代，無政府主義與國家的衝突本質被歐洲[法西斯主義的崛起改變了](../Page/法西斯主義.md "wikilink")。在許多情況下，歐洲的無政府主義面臨不同的選擇—是否要加入由改革派民主主義者和傾向蘇聯的共產主義者聯合組成的人民陣線同盟以對抗法西斯主義。從義大利流亡的無政府主義者Luigi
Fabbri對此說道：

  -
    「法西斯主義並非只是另一種形式的政府而已，它如同其他所有政府一般使用暴力，它是所能想像中最獨裁而又最暴力的政府。它代表了對權力原則的理論和實踐的極度讚揚。」[7](http://www.blackrosebooks.net/anarism1.htm)

無政府主義與法西斯主義的鬥爭首先在義大利出現。無政府主義者在反法西斯的組織“Arditi del
Popolo”中扮演了重要角色，這個團體在義大利是無政府主義色彩最濃厚的一個，並且取得了多次的勝利，包括1922年8月在[帕爾瑪的堡壘擊敗了數千名法西斯黑衫黨黨員](../Page/帕尔马_\(意大利\).md "wikilink")。不幸地，無政府主義提出的聯合起來對抗法西斯主義的呼籲被社會主義者和共產主義者們所忽略，直到後來大勢已去\[36\]。在西班牙，西班牙全國勞工聯盟（CNT）最初拒絕加入反法西斯的人民陣線選舉聯盟，CNT的棄權導致右翼在選舉中獲勝。但在1936年CNT改變其政策，無政府主義者們投票支持人民陣線重新掌權，一個月後，統治階層發動了政變，[西班牙内战](../Page/西班牙内战.md "wikilink")（1936-1939）爆發。

為了對抗軍隊的叛變，由無政府主義主導的農民和勞工們在武裝民兵的支持下攻佔了主要的城市[巴塞罗那](../Page/巴塞罗那.md "wikilink")，以及其他面積廣大的鄉村地區，在鄉村實行集體化。但最後法西斯主義還是在1939年獲勝，無政府主義者在與[斯大林主义者的鬥爭中也節節敗退](../Page/斯大林主义.md "wikilink")。CNT的領導階層往往陷入混亂和分裂，一些成員參與政府導致極大爭議。史達林主義的軍隊也迫害反對派的馬克思主義者與無政府主義者。

自從1970年代以來，無政府主義者又開始與新崛起的[新法西斯主義團體鬥爭](../Page/新法西斯主義.md "wikilink")。在德國和英國，一些無政府主義者與激進的左翼馬克思主義團體合作，一起對抗法西斯主義。他們主張直接以自身的力量與法西斯主義戰鬥，而不是依賴政府採取行動。自從1990年代後期以來，美國的無政府主義者也產生類似的傾向。

## 行動宣傳

由於許多引人注目的暴力行動包括[騷亂](../Page/騷亂.md "wikilink")、[暗殺](../Page/刺客.md "wikilink")、[暴動](../Page/暴動.md "wikilink")，和一些人採取的[恐怖主义行動](../Page/恐怖主义.md "wikilink")，無政府主義者時常被描繪為是危險而暴力的。一些19世紀後期的[革命者鼓勵政治上的暴力行動](../Page/革命.md "wikilink")，例如進行[炸弹攻擊和暗殺](../Page/炸弹.md "wikilink")[國家元首](../Page/國家元首.md "wikilink")。這類行動通常被稱為「行動宣傳」（Propaganda
of the
deed）。這一詞的原本涵義是指做出示範性的直接行動，以鼓舞群眾進行革命。不過[行動宣傳可以是暴力的](../Page/行動宣傳.md "wikilink")、也可以是非暴力的。

無政府主義者們並沒有對使用暴力的效用和正當性有一定的共識，以米哈依尔·巴枯宁\[37\]和艾力格·馬拉泰斯塔為例，兩人認為暴力是必要的，有時也是革命中的理想手段。但他們也指責單獨的個人恐怖主義行為(巴枯寧,
"The Program of the International Brotherhood"
(1869)\[[http://www.marxists.org/reference/archive/bakunin/works/1869/program.htm\]和馬拉泰斯塔](http://www.marxists.org/reference/archive/bakunin/works/1869/program.htm%5D和馬拉泰斯塔),
"Violence as a Social Factor"
(1895))[8](http://www.blackrosebooks.net/anarism1.htm)。

其他一些無政府主義者則被看作是和平無政府主義者，主張徹底的非暴力。[列夫·托爾斯泰的理念通常被視為](../Page/列夫·托爾斯泰.md "wikilink")[基督教無政府主義](../Page/基督教無政府主義.md "wikilink")，他便是著名的[非暴力抵抗的擁護者](../Page/非暴力抵抗.md "wikilink")。美國個人無政府主義者也強烈譴責行動宣傳。

## 宗教無政府主義

大多數無政府主義者傾向於現世的文化，要不就是徹底的[無神論者](../Page/無神論.md "wikilink")。不過，一些宗教信仰裡服從上帝的傳統，也與服從國家和人治政府有著衝突。

[基督教無政府主義相信沒有任何權威能高過](../Page/基督教無政府主義.md "wikilink")[上帝](../Page/上帝.md "wikilink")，並反對世俗間的政府和[國教的權威](../Page/國教.md "wikilink")。他們相信[耶穌的教誨以及早期](../Page/耶穌.md "wikilink")[基督教会的實踐是無政府主義的](../Page/基督教会.md "wikilink")，但在「[基督教](../Page/基督教.md "wikilink")」成为[羅馬的國教後便被腐化了](../Page/古羅馬.md "wikilink")。基督教無政府主義者相信耶穌所指示的「有人打你的左邊臉頰，你就把右邊臉頰也給他打」，堅持嚴格的[和平主义](../Page/和平主义.md "wikilink")。最知名的基督教無政府主義者是[列夫·托爾斯泰](../Page/列夫·托爾斯泰.md "wikilink")，他寫了《上帝之國在你心中》一書，主張社會應該以憐憫、非暴力原則和[自由為根基](../Page/自由.md "wikilink")。基督教無政府主義也時常組織實驗性的無政府社區。他們有時也會抗繳[稅賦](../Page/稅賦.md "wikilink")。

除了基督教之外，佛教無政府主義以1920年代的中國影響力最為深遠，代表性人物為[释太虚](../Page/释太虚.md "wikilink")，其思想受到了托爾斯泰和[井田制度的影響](../Page/井田制.md "wikilink")。除此，佛教無政府主義在1960年代由[盖瑞·施耐德等作家所復興](../Page/盖瑞·施耐德.md "wikilink")。

[新異教主義則注重於](../Page/新異教主義.md "wikilink")[環境保護和社會的平等](../Page/環境保護.md "wikilink")，以及強調分權的自然狀態，使許多新異教徒成為新異教無政府主義者。

## 無政府主義和女性主義

[Portrait_Emma_Goldman.jpg](https://zh.wikipedia.org/wiki/File:Portrait_Emma_Goldman.jpg "fig:Portrait_Emma_Goldman.jpg")\]\]

無政府女性主義是一種[激進女性主義](../Page/激進女性主義.md "wikilink")，主張[父權是這個社會最根本的問題](../Page/父權.md "wikilink")。不過要直到1970年代無政府女性主義一詞才被明白地闡述\[38\]，在[第二波女性主義運動中](../Page/第二波女性主義.md "wikilink")，無政府女性主義將父權視為人類歷史上最明顯的等級制度；因此，首先產生的壓迫形式便是男性支配女性的壓迫。無政府女性主義總結認為如果女性主義者對抗父權，那麼她們必然是在對抗所有形式的[等级制度](../Page/等级制度.md "wikilink")，也因此必然是在對抗國家和資本主義的獨裁主義。

[無政府原始主義](../Page/無政府原始主義.md "wikilink")（Anarcho-primitivism）將性別角色的差異和父權視為是[文明發展的開端](../Page/文明.md "wikilink")，因此原始主義也被認為是和女性主義有關的無政府主義流派。[生態女性主義也常被看作是綠色無政府主義的女性主義流派](../Page/生態女性主義.md "wikilink")。

早期的第一波女性主義者[玛丽·沃斯通克拉夫特抱持著原始的無政府主義觀點](../Page/玛丽·沃斯通克拉夫特.md "wikilink")，[威廉·戈德温也時常被看作是無政府女性主義的先驅](../Page/威廉·戈德温.md "wikilink")。無政府女性主義藉由一些19世紀後期美國個人無政府主義的理論更嚴密地公式化，當時大多數無政府主義者都沒有認真地看待那些概念，其他一些人如Moses
Harman和Florence Finch
Kelly則將性別的平等看作是重要的議題\[39\]。20世紀早期的作家和理論家如[爱玛·戈尔德曼等人的作品也使無政府女性主義獲得更多注意](../Page/爱玛·戈尔德曼.md "wikilink")。在西班牙內戰中，一個名為「自由婦女」的無政府女性主義團體組織了起來，捍衛無政府主義和女性主義兩者的概念。

在現代的無政府主義運動中，許多無論男女的無政府主義者自認為是女性主義者，無政府女性主義的觀念逐漸成長。無政府女性主義的期刊Quiet
Rumors也進一步的散播各種反獨裁的無政府女性主義觀點以擴展這項運動。

## 無政府主義與環保

自從1970年代以來，歐美國家的無政府主義者們開始專注於自然環境的保護上，[生態無政府主義和](../Page/生態無政府主義.md "wikilink")[綠色無政府主義](../Page/綠色無政府主義.md "wikilink")。另一方面，支持自由市場的環境保護者認為大多數的環境問題都是因為[公共災難](../Page/公共災難.md "wikilink")（Tragedy
of the
commons）和缺乏對財產權利的適當定義而造成的。兩種說法都支持生態的[多樣性和](../Page/生物多樣性.md "wikilink")[永續性](../Page/生態永續性.md "wikilink")。生態無政府主義者通常以直接行動來對抗那些被他們視為是地球破壞者的制度。當中最突出的是1980年代的[地球優先！運動](../Page/地球優先！.md "wikilink")，他們採取了佔據樹林以阻止伐木作業等行動。另一個重要的流派是[生態女性主義](../Page/生態女性主義.md "wikilink")，她們認為壓迫自然界便等同壓迫婦女。[米哈依尔·巴枯宁針對](../Page/米哈依尔·巴枯宁.md "wikilink")[社會生態學的著作](../Page/社會生態學.md "wikilink")、[大衛·華生](../Page/大衛·華生.md "wikilink")（David
Watson）在雜誌上的文章、[史蒂夫·布斯](../Page/史蒂夫·布斯.md "wikilink")（Steve
Booth）針對綠色無政府主義的著作等都促成了綠色無政府主義和生態無政府主義的思想和運動。

原始主義（Primitivism）是西方哲學中一種主張回歸工業時代和農業時代前的社會的思想。它發展出對工業文明的批判，批判人類的[科技和進步導致人與人的疏離](../Page/科技.md "wikilink")。這個理論在19世紀英國的[盧德派](../Page/盧德派.md "wikilink")（Luddite）破壞紡織機器的行動中展現出來，也在[让-雅克·卢梭的著作裡出現](../Page/让-雅克·卢梭.md "wikilink")。原始主義也在[地球優先！](../Page/地球優先！.md "wikilink")、[收復街道和](../Page/收復街道.md "wikilink")[地球解放阵线等運動中發展](../Page/地球解放阵线.md "wikilink")。無政府原始主義主張許多「原始」和狩獵時代的社會都具有反獨裁的本質，也因此是無政府主義的理想中的社會。

## 其他流派和分支

無政府主義產生了許多折衷和融合的理論及運動。自從西方國家在1960年代和1970年代的社會騷亂以來，有許多新的運動和學派陸續出現。這些立場發展的規模大多相當有限。

  - **後左翼無政府**（Post-left anarchy） -
    後左翼無政府試圖將自身與傳統的「左翼」劃清界線—共產主義、社會主義、社會民主主義等等。也試圖劃清自身與一般[意識形態的界線](../Page/意識形態.md "wikilink")。後左翼無政府主張，無政府主義已經被固執的「左翼」運動和一些個別的議題（反戰、反核能等等）長期附著而逐漸弱化。他們主張綜合無政府主義的思想以及一些在左翼環境以外的特定反獨裁革命運動。主要的團體和個人包括*Anarchy:
    A Journal of Desire
    Armed*雜誌，和雜誌的編輯如[鮑伯·布萊克](../Page/鮑伯·布萊克.md "wikilink")（Bob
    Black）、[哈基姆·貝](../Page/哈基姆·貝.md "wikilink")（Hakim Bey）等。
  - **後無政府主義**（Post-Anarchism） -
    後無政府主義一詞原本是由[所羅·紐曼](../Page/所羅·紐曼.md "wikilink")（Saul
    Newman）提出，指一種融合了古典無政府主義與[後結構主義的思想](../Page/後結構主義.md "wikilink")。之後這一詞的涵義被改變了，用以稱呼包括[自治主義](../Page/自治主義.md "wikilink")（Autonomism）、後左翼無政府、[境遇主義](../Page/境遇主義.md "wikilink")（situationism）和[後殖民主義](../Page/後殖民主義.md "wikilink")（Post-colonialism）。在本質上後無政府主義反對那些有著一定宗旨和理念的共識。也因此人們很難判別後無政府主義的歸屬定義。但主要的思想家包括[所羅·紐曼](../Page/所羅·紐曼.md "wikilink")、[吉爾·德勒茲](../Page/吉爾·德勒茲.md "wikilink")、[菲力克斯·加達里](../Page/菲力克斯·加達里.md "wikilink")（Felix
    Guattari）。
  - **暴動無政府主義**（Insurrectionary anarchism） -
    暴動無政府主義是革命無政府主義的一種形式，批判傳統的無政府工會和聯盟。暴動無政府主義提倡非正式的組織，包括小型的聯繫團體，以各種形式的反抗行動來進行鬥爭，以群眾的組織作為基本架構，可以募集那些並非無政府主義者但也遭受剝削的個人。

## 其他議題

  - **無政府社會的概念** -
    許多政治哲學家支持以國家來作為制止暴力的手段，以此將人類衝突所造成損害降至最低，並能建立公平的人際關係。無政府主義者則認為追求這些目標並不能正當化國家的存在；許多無政府主義者主張國家與這些目標是不相容的，而且也正是造成混亂、暴力和戰爭的原因。無政府主義者主張國家造成了對暴力使用的壟斷，並以暴力來增進菁英階級的利益。
  - **反種族主義、反法西斯主義和反壓迫** -
    [黑人無政府主義](../Page/黑人無政府主義.md "wikilink")（Black
    anarchism）反對國家和[資本主義的存在](../Page/資本主義.md "wikilink")、以及對有色人種的征服與支配，主張一個沒有等級制度的社會。這些理論家包括[阿山提·阿爾斯通](../Page/阿山提·阿爾斯通.md "wikilink")（Ashanti
    Alston）、[洛倫佐·高保·歐文](../Page/洛倫佐·高保·歐文.md "wikilink")（Lorenzo Komboa
    Ervin）等。其中一些理論家也曾是美國[黑豹黨的成員](../Page/黑豹黨_\(美國\).md "wikilink")，但在他們被黑豹黨裡的馬克思—列寧主義流派批評後轉為無政府主義。[有色人種無政府主義者](../Page/有色人種無政府主義者.md "wikilink")（Anarchist
    People of Color, APOC）於是成立，成為美國非白種人的無政府主義團體。
  - **新殖民主義和全球化** -
    幾乎所有無政府主義者都反對[新殖民主义](../Page/新殖民主义.md "wikilink")，視其為一種全球性的經濟壓迫力量，如[世界银行](../Page/世界银行.md "wikilink")、[世界贸易组织](../Page/世界贸易组织.md "wikilink")、[八國首腦高峰會議和](../Page/八國首腦高峰會議.md "wikilink")[世界經濟論壇](../Page/世界經濟論壇.md "wikilink")。[全球化是相當含糊不清的字眼](../Page/全球化.md "wikilink")，在不同的無政府主義流派裡也有著不同的涵義。大多數無政府主義者用這一詞來形容新殖民主義以及/或是[文化帝國主義](../Page/文化帝國主義.md "wikilink")—當中一些人認為兩者是相關的。許多人也積極參與了[反全球化運動](../Page/反全球化.md "wikilink")。其他的無政府主義者，尤其是無政府資本主義者，以「全球化」一詞來形容遍及全球的分工和貿易擴張，他們認為只要政府不介入這個過程，全球化是有益的。
  - **平行架構** -
    許多無政府主義者創建了一些非國家支撐的機構和「前哨」，比如分發食物的[要食物不要炸彈組織](../Page/要食物不要炸彈.md "wikilink")、勞工議會、獨立媒體、互助計畫、資訊中心（Infoshop）等，以及免費學校等教育系統，和鄰近社區協調/仲裁團體等等。這個概念是以建設一個新的反獨裁社會來取代舊的獨裁結構。
  - **科技** -
    近年來科技的發展使得無政府主義者得以更簡易而更清楚的向大眾傳達他們的理想。當中許多人以網際網路建立線上的社區，大量侵蝕[知识产权](../Page/知识产权.md "wikilink")，透過檔案交流、協力軟體的發展、和[自由软件](../Page/自由软件.md "wikilink")（又稱為[开放源代码软件](../Page/开放源代码软件.md "wikilink")）。這些網路社區包括了[GNU](../Page/GNU.md "wikilink")、[Linux](../Page/Linux.md "wikilink")、和[Wiki](../Page/Wiki.md "wikilink")。一些無政府主義者將[信息技术視為是擊敗獨裁主義的最佳武器](../Page/信息技术.md "wikilink")。一些人甚至認為資訊年代的來臨最終將能達成無政府的狀態\[40\]。*參見*：[網路無政府主義](../Page/網路無政府主義.md "wikilink")
  - **和平主義** -
    一些無政府主義者的理論被視為是[和平主义者](../Page/和平主义.md "wikilink")（反對[戰爭](../Page/戰爭.md "wikilink")）。[無政府和平主義擁護](../Page/無政府和平主義.md "wikilink")[列夫·托爾斯泰非暴力的理念](../Page/列夫·托爾斯泰.md "wikilink")。雖然許多無政府主義的計畫是以非暴力的手段為主的（如[地球優先！](../Page/地球優先！.md "wikilink")、要食物不要炸彈等）。但許多無政府主義者也排斥和平主義，也不認為毀滅財產是屬於暴力的行為。無政府主義者將戰爭視為是國家用以增長和鞏固其權力的行動—國內和國外都是如此，並同意[藍道夫·伯爾那](../Page/藍道夫·伯爾那.md "wikilink")（Randolph
    Bourne）所說的：「戰爭是對國家的治療」\[41\]。許多無政府主義者以反戰作為行動的根基，大部分是反戰和反[徵兵的抗議行動](../Page/徵兵制.md "wikilink")。
  - **議會政治** -
    在一般的情況下，無政府主義反對在政府選舉中投票。這有三個原因：第一，投票是無效的，充其量只能達成極小的改革。第二，參與選舉最終將導致自身成為那個制度的一部分，而不是終結掉這個制度\[42\]。第三，因為投票將等於認同了國家的存在\[43\]。無政府主義團體CrimethInc.在2004年美國總統選舉時發起了一項名為「別只是投票，要行動」（Don't
    Just Vote, Get Active）的活動，提倡直接行動的重要性，反對只是以選舉來改變現狀。
  - **派別主義** -
    大多數無政府主義的流派在一些程度是屬於[派別主義](../Page/派別主義.md "wikilink")（Sectarianism）的。通常在每個流派裡對於如何與其他學派進行接觸和互動都有不同看法。一些無政府主義者視其他流派的理念是不可能實現的，並拒絕與其交流；其他一些人則接受與別的流派結盟的機會，或至少在短時間內結盟以達成共同的特定目標。

## 文化現象

[Noam_chomsky.jpg](https://zh.wikipedia.org/wiki/File:Noam_chomsky.jpg "fig:Noam_chomsky.jpg")
(1928–)\]\]
最容易在大眾文化裡代表無政府主義形象的，便是那些自稱為無政府主義的名人，不過一些無政府主義者仍拒絕聚焦於某些知名的在世人物以避免菁英主義。以下的人物是一些相當突出的無政府主義者：

  - [麻省理工學院](../Page/麻省理工學院.md "wikilink")[语言学教授](../Page/语言学.md "wikilink")[诺姆·乔姆斯基](../Page/诺姆·乔姆斯基.md "wikilink")
  - [科幻小说作家](../Page/科幻小说.md "wikilink")[娥蘇拉·勒瑰恩](../Page/娥蘇拉·勒瑰恩.md "wikilink")
  - 社會歷史學家[霍華德·津恩](../Page/霍華德·津恩.md "wikilink")
  - 人種研究學教授[華德·邱吉爾](../Page/華德·邱吉爾.md "wikilink")（Ward Churchill）
  - 作家[愛德華·艾比](../Page/愛德華·艾比.md "wikilink")（Edward Albee）
  - 作家[羅伯特·安頓·威爾森](../Page/羅伯特·安頓·威爾森.md "wikilink")（Robert Anton
    Wilson）—雖然威爾森的理念可能較接近[左翼自由意志主義](../Page/左翼自由意志主義.md "wikilink")
  - 圖畫小說家[愛倫·摩爾](../Page/愛倫·摩爾.md "wikilink")（Alan Moore）
  - 演員和作家[漢斯·艾爾佛森](../Page/漢斯·艾爾佛森.md "wikilink")（Hans Alfredsson）
  - [終極格鬥大賽](../Page/終極格鬥大賽.md "wikilink")（UFC）拳擊手和冠軍[傑夫·孟森](../Page/傑夫·孟森.md "wikilink")（Jeff
    Monson）
  - [女性主義者](../Page/女性主義.md "wikilink")、作家和評論家[吉曼·基爾](../Page/吉曼·基爾.md "wikilink")

在[丹麥](../Page/丹麥.md "wikilink")，[哥本哈根的下城於](../Page/哥本哈根.md "wikilink")1971年形成了一個自治形式的[克里斯欽自由城](../Page/克里斯欽自由城.md "wikilink")。在大多數[西歐國家的房屋和失業危機產生了許多的自治社區和開墾區](../Page/西歐.md "wikilink")。雖然他們並非全都自稱為無政府主義者，一些在德國的反新納粹團體、和在法國及義大利的自治馬克思主義暴動、境遇主義者、和自治論者的運動也將反獨裁、反資本主義的思想進一步宣揚。

在許多風格的音樂裡，無政府主義的主題開始流行，音樂開始與無政府主義相連結。當中最知名的是龐克搖滾，不過近年來hip
hop和民俗音樂也成為散佈無政府主義理念的重要媒介。在英國，無政府主義與[朋克文化相連結](../Page/朋克文化.md "wikilink")，Crass樂團便以其無政府主義及和平主義的理念聞名。*參見[無政府龐克](../Page/無政府龐克.md "wikilink")*

## 参见

  - [自由社會主義](../Page/自由社會主義.md "wikilink")
  - [生態女性主義](../Page/生態女性主義.md "wikilink")
  - [無政府資本主義](../Page/無政府資本主義.md "wikilink")
  - [参与型经济](../Page/参与型经济.md "wikilink")
  - [无支配体制](../Page/无支配体制.md "wikilink")
  - [信息无政府主义](../Page/信息无政府主义.md "wikilink")——一个相似的术语，指反对[版权](../Page/版权.md "wikilink")、[专利](../Page/专利.md "wikilink")、[审查等对信息实行](../Page/审查.md "wikilink")“管制”的思想。

### 主要事件

  - [巴黎公社](../Page/巴黎公社.md "wikilink") (1871)
  - [干草市场暴乱](../Page/干草市场暴乱.md "wikilink") (1886)
  - [克倫斯塔叛變](../Page/克倫斯塔叛變.md "wikilink") (1921)
  - [五月风暴](../Page/五月风暴.md "wikilink"), 法國(1968)
  - [西雅圖WTO會議](../Page/世界貿易組織第三次部長級會議.md "wikilink") (1999)

### 人物

  - [米哈依尔·巴枯宁](../Page/米哈依尔·巴枯宁.md "wikilink"), *God and the State*
    [9](http://dwardmac.pitzer.edu/Anarchist_Archives/bakunin/godandstate/godandstate_ch1.html)
  - [爱玛·戈尔德曼](../Page/爱玛·戈尔德曼.md "wikilink"), *Anarchism & Other Essays*
    [10](http://dwardmac.pitzer.edu/Anarchist_Archives/goldman/GoldmanCW.html)
  - [彼得·克魯泡特金](../Page/彼得·克魯泡特金.md "wikilink"), *Mutual Aid: A Factor of
    Evolution|Mutual Aid* [11](http://www.gutenberg.org/etext/4341)
  - [皮埃尔-约瑟夫·普鲁东](../Page/皮埃尔-约瑟夫·普鲁东.md "wikilink"), *What is
    Property?* [12](http://www.gutenberg.org/etext/360)
  - [鲁多夫·洛克尔](../Page/鲁多夫·洛克尔.md "wikilink"), *Anarcho-Syndicalism*
    [13](http://www.spunk.org/library/writers/rocker/sp001495/rocker_as1.html)
  - [穆瑞·羅斯巴德](../Page/穆瑞·羅斯巴德.md "wikilink"), *Man, Economy, and State*
    [14](http://www.mises.org/rothbard/mes.asp)
  - [麥克斯·施蒂納](../Page/麥克斯·施蒂納.md "wikilink"), *The Ego And Its Own*
    [15](http://www.df.lth.se/~triad/stirner/)
  - [列夫·托爾斯泰](../Page/列夫·托爾斯泰.md "wikilink"), *The Kingdom of God is
    Within You*
    [16](https://web.archive.org/web/20120205200941/http://www.kingdomnow.org/withinyou.html)
  - [尼采](../Page/尼采.md "wikilink")，[无政府主义和尼采](../Page/无政府主义和尼采.md "wikilink")
  - John Zerzan, *Future Primitive*

### 国际组织

  - [无政府主义国际](../Page/无政府主义国际.md "wikilink")（1872年－1877年）
  - [国际劳动人民协会](../Page/国际劳动人民协会.md "wikilink")（1881年－1887年）
  - [国际工人协会 (1922年)](../Page/国际工人协会_\(1922年\).md "wikilink")（1922年至今）
  - [无政府主义联合会国际](../Page/无政府主义联合会国际.md "wikilink")（1968年至今）
  - [黑桥国际](../Page/黑桥国际.md "wikilink")（2001年－2004年）
  - [国际自由意志团结](../Page/国际自由意志团结.md "wikilink")（2001年－2005年）
  - [Anarkismo.net](../Page/Anarkismo.net.md "wikilink")（2005年至今）
  - [无政府主义者国际联盟](../Page/无政府主义者国际联盟.md "wikilink")（2011年至今）

### 地區/文化上的無政府主義

  - [美國個人無政府主義](../Page/美國個人無政府主義.md "wikilink")
  - [中國無政府主義](../Page/中國無政府主義.md "wikilink")

## 參考文獻

## 延伸閱讀

  - Arif Dirlik著，孫宜學譯：《中國革命中的無政府主義》（桂林：廣西師範大學出版社，2006）。

## 外部連結

網際網路上有著各式各樣的無政府主義網站，以下只是其中幾個:

  - ["Anarchism Theory FAQ"
    version 5.2](http://www.gmu.edu/departments/economics/bcaplan/anarfaq.htm)
    - 無政府資本主義的問答集
  - ["An Anarchist FAQ"
    version 11.4](https://web.archive.org/web/20060701042515/http://www.infoshop.org/faq/)
    - 社會主義的「無政府主義問題集」 - 包括50頁的目錄
  - [安那其百科](http://zh.anarchopedia.org) - 多语种入门无政府主义
  - [anarchism.net](http://www.anarchism.net) - 反國家網
  - [Anarchy Archives](http://dwardmac.pitzer.edu/Anarchist_Archives/)
    知名無政府主義者的龐大資料庫，包括許多他們的著作
  - [at the Daily Bleed's Anarchist
    Encyclopedia](https://web.archive.org/web/20150502052423/http://recollectionbooks.com/bleed/gallery/galleryindex.htm)超過1,500名無政府主義者列表，包括簡短傳記、連結
  - [About Market Anarchism](http://praxeology.net/anarcres.htm) – 虚拟图书馆

[無政府主義](../Category/無政府主義.md "wikilink")
[Category:政治理论](../Category/政治理论.md "wikilink")
[Category:无政府主义历史](../Category/无政府主义历史.md "wikilink")

1.  [藝術與建築索引典—無政府主義](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300055517)
     於2011年4月11日查閱

2.  [Murray Rothbard](../Page/穆瑞·罗斯巴德.md "wikilink").[Concepts of the
    role of intellectuals in social change toward laissez
    faire](http://www.mises.org/journals/jls/9_2/9_2_3.pdf)\[J\].The
    Journal of Libertarian Studies, 1990, 9(2):43.

3.

4.

5.  原文：“There has been such a thing as letting mankind alone; there has
    never been such a thing as governing mankind \[with
    success\]”，出自《庄子·外篇·在宥》．

6.  原文：“A petty thief is put in jail. A great brigand becomes a ruler of
    a Nation”，出自《庄子·外篇·胠箧》．

7.
8.  [Russell](../Page/Bertrand_Russell.md "wikilink"), Bertrand.
    *"Ancient philosophy"* in *A History of Western Philosophy, and its
    connection with political and social circumstances from the earliest
    times to the present day*, 1945.

9.  [An Anarchist Timeline](http://www.zpub.com/notes/aan-hist.html),
    from Encyclopaedia Britannica, 1994.

10. [1](http://plato.stanford.edu/entries/godwin/)

11.

12.

13. [Tucker](../Page/Benjamin_Tucker.md "wikilink"), Benjamin. *"[Labor
    and Its
    Pay](http://www.blackcrayon.com/page.jsp/library/tucker/tucker37.htm)
    "* Individual Liberty: Selections From the Writings of Benjamin R.
    Tucker, Vanguard Press, New York, 1926, Kraus Reprint Co., Millwood,
    NY, 1973.

14. Peter Marshall, **Demanding the Impossible**; John Clark, **The
    Anarchist Moment**; Albert Meltzer, **Anarchism: Arguments for and
    Against**; Noam Chomsky, **Understanding Power**, David Weick,
    *Anarchist Justice*; Brian Morris, *"Anthropology and Anarchism,"*
    **Anarchy: A Journal of Desire Armed** (no. 45); Peter Sabatini,
    *Libertarianism: Bogus Anarchy*; Donald Rooum, **What is
    Anarchism?**; Bob Black, *Libertarian as Conservative*

15. 1\) Perlin, Terry M. Contemporary Anarchism. Transaction Books, New
    Brunswick, NJ 1979, p. 7.
    「無政府主義解決社會問題的方式可以分為完全的共產主義到同樣徹底的個人主義。所使用的手段也不相同，從無政府工團主義至無政府資本主義」
    2) Richard Sylvan, Richard. *Anarchism*. A Companion to Contemporary
    Political Philosophy, editors Goodin, Robert E. and Pettit, Philip.
    Blackwell Publishing, 1995, p.231.
    「無政府主義有許多公認的流派，包括了：個人無政府主義、無政府資本主義、無政府共產主義、互助主義…」

16. [*Exclusive Interview With Murray
    Rothbard*](http://www.lewrockwell.com/rothbard/rothbard103.html)
    The New Banner: A Fortnightly Libertarian Journal ([25
    February](../Page/25_February.md "wikilink") 1972)

17. Hart, David [Gustave de Molinari and the Anti-Statist Liberal
    Tradition](https://web.archive.org/web/20031122152135/http://homepage.mac.com/dmhart/Molinari/)
    Journal of Libertarian Studies, in three parts, (Summer 1981), V,
    no. 3: 263-290; (Fall 1981), V. no. 4: 399-434; (Winter 1982), VI,
    no. 1: 83-104

18. [Raico, Ralph](../Page/Ralph_Raico.md "wikilink") [*Authentic German
    Liberalism of the 19th Century*](http://www.mises.org/story/1787)
    Ecole Polytechnique, Centre de Recherce en Epistemologie Appliquee,
    Unité associée au CNRS (2004).

19. Daniel Guerin, *Anarchism: From Theory to Practice* (New York:
    Monthly Review Press, 1970).

20. quoted by Max Nettlau, A Short History of Anarchism, pp. 43-44

21. [Proudhon](../Page/Pierre-Joseph_Proudhon.md "wikilink"),
    Pierre-Joseph. *"[Chapter 3. Labour as the efficient cause of the
    domain of
    property](http://www.marxists.org/reference/subject/economics/proudhon/property/ch03.htm)"*
    from *"[What is
    Property?](../Page/What_is_Property?.md "wikilink")"*, 1840

22. Edwards, Stewart. Introduction to *Selected Writings of
    Pierre-Joseph Proudhon*, Anchor Books, Doubleday & Company, Inc.
    1969, p. 33

23. Chapter 3, section 6 of "What is Property?", p. 130

24. [Proudhon](../Page/Pierre-Joseph_Proudhon.md "wikilink"),
    Pierre-Joseph. *The General Idea of the Revolution*, p. 281

25. Kropotkin, Peter. [Chapter 13 *The Collectivist Wages System* from
    *The Conquest of
    Bread*](http://dwardmac.pitzer.edu/Anarchist_Archives/kropotkin/conquest/ch13.html),
    G. P. Putnam's Sons, New York and London, 1906.

26. [3](http://www.marxists.org/reference/subject/economics/proudhon/property/ch05.htm)

27. [De l'être-humain mâle et femelle - Lettre à P.J. Proudhon par
    Joseph
    Déjacque](http://joseph.dejacque.free.fr/ecrits/lettreapjp.htm) (in
    [French](../Page/French_language.md "wikilink"))

28. [Kropotkin](../Page/Peter_Kropotkin.md "wikilink"), Peter. *Words of
    a Rebel*, p99.

29. [Kropotkin](../Page/Peter_Kropotkin.md "wikilink"), Peter. *[The
    Conquest of Bread](../Page/The_Conquest_of_Bread.md "wikilink")*,
    p145.

30. Kropotkin, Peter. [Chapter 13 *The Collectivist Wages System* from
    *The Conquest of
    Bread*](http://dwardmac.pitzer.edu/Anarchist_Archives/kropotkin/conquest/ch13.html),
    G. P. Putnam's Sons, New York and London, 1906.

31. *The Place of Anarchism in the Evolution of Socialist Thought*

32. *Act for Yourself*, pp. 104-5

33.
34.

35.

36. [A.5.5 Anarchists in the Italian Factory
    Occupations.](http://www.infoshop.org/faq/secA5.html#seca55)

37. [Mikhail](http://www.marxists.org/reference/archive/guillaume/works/bakunin.htm)

38. [Anarcho-Feminism - Two Statements - Who we are: An Anarcho-Feminist
    Manifesto](http://www.anarcha.org/sallydarity/Anarcho-FeminismTwoStatements.htm)


39. Marsh, Margaret S. *Anarchist Women, 1870-1920*. Philadelphia:
    Temple University Press, 1981.

40. [The Sovereign Individual -- Mastering the transition to the
    information
    age](http://www.modulaware.com/a/?m=select&id=0684832720)

41. [War is the Health of the
    State](http://struggle.ws/hist_texts/warhealthstate1918.html)

42. [J.2 What is direct action?](http://www.infoshop.org/faq/secJ2.html)


43. "Voting Anarchists: An Oxymoron or What?" by [Joe
    Peacott](../Page/Joe_Peacott.md "wikilink"), and writings by [Fred
    Woodworth](../Page/Fred_Woodworth.md "wikilink").