**王家瑞**（），[河北](../Page/河北.md "wikilink")[秦皇島人](../Page/秦皇島.md "wikilink")。1970年6月參加工作，1973年10月加入[中國共產黨](../Page/中國共產黨.md "wikilink")。[吉林大学经济管理系国民经济管理专业](../Page/吉林大学.md "wikilink")、[复旦大學研究生院經濟管理專業畢業](../Page/复旦大學.md "wikilink")，[在職研究生學歷](../Page/在職研究生.md "wikilink")，经济学[博士](../Page/博士.md "wikilink")，[高级经济师](../Page/职称.md "wikilink")，教授，博士生导师。曾任[中共中央对外联络部部长](../Page/中共中央对外联络部.md "wikilink")、[第十二届全国政协副主席](../Page/第十二届全国政协.md "wikilink")。

## 簡歷

1970年6月，任[吉林省](../Page/吉林省.md "wikilink")[長春市郵局投遞員](../Page/長春市.md "wikilink")、會計。1972年4月，在[上海海運學院遠洋系國際郵政英語班學習](../Page/上海海運學院.md "wikilink")。1973年10月，任長春市郵局國際聯郵員。1974年4月，任長春市郵局副科長。1976年9月，任吉林省郵電管理局郵政處負責人。1978年4月，任吉林省郵電管理局報刊發行科副科長。1980年11月，任吉林省郵電管理局郵政處副處長。1982年4月，任長春市郵政局副局長。1985年10月，任[郵電部報刊發行局副局長](../Page/郵電部.md "wikilink")、局長(1983年9月至1987年7月，吉林大學經濟管理系國民經濟管理專業[在職研究生](../Page/在職研究生.md "wikilink"))。

1992年7月，任[國務院經濟貿易辦公室商業司副司長](../Page/國務院經濟貿易辦公室.md "wikilink")。1993年6月，任[國家經濟貿易委員會綜合司副司長](../Page/國家經濟貿易委員會.md "wikilink")（1988年9月至1994年12月[吉林大學經濟管理學院](../Page/吉林大學.md "wikilink")[在職研究生](../Page/在職研究生.md "wikilink")，獲[碩士、博士學位](../Page/在職研究生.md "wikilink")）。1994年12月，任國家經濟貿易委員會市場流通司司長。

1995年8月，任中共[山東省](../Page/山東省.md "wikilink")[青島市委常委](../Page/青島市.md "wikilink")。1995年9月，任中共青島市委常委、青島市副市長（其間：1995年9月至1997年12月在[復旦大學](../Page/復旦大學.md "wikilink")[管理學院](../Page/復旦大學管理學院.md "wikilink")[產業經濟](../Page/產業經濟.md "wikilink")[博士後流動站從事在職研究工作](../Page/博士後.md "wikilink")）。1997年12月，任中共青島市委副書記、青島市副市長。1998年2月，任中共青島市委副書記、[青島市市長](../Page/青島市市長.md "wikilink")(其間：2000年3月至5月在[中共中央黨校進修一班學習](../Page/中共中央黨校.md "wikilink"))。

2000年9月，任[中共中央对外联络部副部長](../Page/中共中央对外联络部.md "wikilink")。2003年3月，任中共中央對外聯絡部部長。

2013年3月，任[第十二届全国政协副主席](../Page/第十二届全国政协.md "wikilink")。2016年11月，当选[中国宋庆龄基金会理事会主席](../Page/中国宋庆龄基金会.md "wikilink")\[1\]。

2017年9月，当选[中国福利会主席](../Page/中国福利会.md "wikilink")\[2\]。

[中共第十六届中央候补委员](../Page/中国共产党第十六届中央委员会候补委员列表.md "wikilink")，[第十七届](../Page/中国共产党第十七届中央委员会委员列表.md "wikilink")、[十八届中央委員](../Page/中国共产党第十八届中央委员会候补委员列表.md "wikilink")。

## 参考文献

{{-}}

[J](../Category/王姓.md "wikilink")
[Category:秦皇岛人](../Category/秦皇岛人.md "wikilink")
[Category:上海海运学院校友](../Category/上海海运学院校友.md "wikilink")
[Category:吉林大学校友](../Category/吉林大学校友.md "wikilink")
[Category:复旦大学校友](../Category/复旦大学校友.md "wikilink")
[Category:中国共产党第十六届中央委员会候补委员](../Category/中国共产党第十六届中央委员会候补委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中国共产党第十八届中央委员会委员](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:中华人民共和国青岛市市长](../Category/中华人民共和国青岛市市长.md "wikilink")
[Category:中共中央对外联络部部长](../Category/中共中央对外联络部部长.md "wikilink")
[Category:第十二届全国政协副主席](../Category/第十二届全国政协副主席.md "wikilink")
[Category:中国宋庆龄基金会理事会主席](../Category/中国宋庆龄基金会理事会主席.md "wikilink")

1.
2.