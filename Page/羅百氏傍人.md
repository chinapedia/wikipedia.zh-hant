**羅百氏傍人**（*Paranthropus
robustus*），又名**粗壯傍人**，是於1938年在[南部非洲發現的](../Page/南部非洲.md "wikilink")[化石](../Page/化石.md "wikilink")。羅伯氏傍人的[頭顱骨發展傾向成為重型的咀嚼工具](../Page/頭顱骨.md "wikilink")。由於牠明顯與[南方古猿有關](../Page/南方古猿.md "wikilink")，故[羅伯特·布魯姆](../Page/羅伯特·布魯姆.md "wikilink")（Robert
Broom）為牠成立了獨立的[傍人屬](../Page/傍人屬.md "wikilink")。

羅百氏傍人估計是屬於200-120萬年前。牠有大的[矢狀嵴](../Page/矢狀嵴.md "wikilink")、[顎骨](../Page/顎骨.md "wikilink")、顎肌及後[犬齒](../Page/犬齒.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")，故適合生活在乾旱的環境。

## 發現歷史

在[雷蒙·達特](../Page/雷蒙·達特.md "wikilink")（Raymond
Dart）發現[非洲南猿後](../Page/非洲南猿.md "wikilink")，[羅伯特·布魯姆支持達特指非洲南猿是](../Page/羅伯特·布魯姆.md "wikilink")[智人的祖先](../Page/智人.md "wikilink")。布魯姆在[南部非洲進一步發現了更多非洲南猿的標本](../Page/南部非洲.md "wikilink")。於1938年，布魯姆以70歲的高齡，在[南非](../Page/南非.md "wikilink")[斯瓦特克朗發現一個很像非洲南猿的](../Page/斯瓦特克朗.md "wikilink")[頭顱骨及](../Page/頭顱骨.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")，但卻更為粗壯。這個化石包括了部份頭顱骨及牙齒，全部可以追溯至200萬年前。發現類似化石的位點包括有南非的[克羅姆德萊](../Page/克羅姆德萊.md "wikilink")、斯瓦特克朗、[德里莫倫](../Page/德里莫倫.md "wikilink")、[岡多林及Coopers洞穴](../Page/岡多林.md "wikilink")。在斯瓦特克朗的洞穴中，一共發現了130個個體。[人科](../Page/人科.md "wikilink")[齒列的研究發現這些化石很小是老於](../Page/齒列.md "wikilink")17歲。

羅百氏傍人是較[鮑氏傍人及](../Page/鮑氏傍人.md "wikilink")[衣索比亞傍人更早發現的](../Page/衣索比亞傍人.md "wikilink")[人科粗壯種](../Page/人科.md "wikilink")。布魯姆的這個發現是首個粗壯[南方古猿](../Page/南方古猿.md "wikilink")，並是繼達特的非洲南猿後第二個南方古猿。布魯姆從南方古猿發現[智人的](../Page/智人.md "wikilink")[演化並非單一直線](../Page/演化.md "wikilink")，而是複雜的分化而成。

## 形態

羅百氏傍人的[頭顱骨有點像](../Page/頭顱骨.md "wikilink")[大猩猩的](../Page/大猩猩.md "wikilink")，而[顎骨及](../Page/顎骨.md "wikilink")[牙齒比](../Page/牙齒.md "wikilink")[人屬的更為堅固](../Page/人屬.md "wikilink")。[羅伯特·布魯姆亦發現牠們的頭蓋頂開始有](../Page/羅伯特·布魯姆.md "wikilink")[矢狀嵴](../Page/矢狀嵴.md "wikilink")，作為[肌肉的連接點](../Page/肌肉.md "wikilink")。編號DNH
7的羅百氏傍人頭顱骨是於1994年在[南部非洲的](../Page/南部非洲.md "wikilink")[德里莫倫洞穴發現](../Page/德里莫倫.md "wikilink")，估計屬於230萬年前，可能是一頭雌性。其他羅百氏傍人的遺骸亦有在南部非洲發現。

羅百氏傍人的牙齒比[南方古猿更大及厚](../Page/南方古猿.md "wikilink")，最初布魯姆根據其形態而將牠分類為[粗壯南方古猿](../Page/粗壯南方古猿.md "wikilink")。牠的牙齒差不多與[鮑氏傍人的一樣大](../Page/鮑氏傍人.md "wikilink")，而[臼齒更像大猩猩的](../Page/臼齒.md "wikilink")。雄性羅百氏傍人站立時只有4呎高及重54公斤，而雌性站立時則矮於1米及重40公斤。羅百氏傍人明顯的是[兩性異形](../Page/兩性異形.md "wikilink")。

羅百氏傍人的[腦部平均約有](../Page/腦部.md "wikilink")410-530立方厘米大，差不多等於[黑猩猩的腦部](../Page/黑猩猩.md "wikilink")。有些學者指羅百氏傍人是吃硬的食物，如[堅果等](../Page/堅果.md "wikilink")，因為牠們是生活在林地及[大草原](../Page/大草原.md "wikilink")。另有研究指牠們是[雜食性的](../Page/雜食性.md "wikilink")。\[1\]而其他學者則指牠們只會在有食物壓力下吃硬的食物。\[2\]

## 參考

## 外部連結

  - [MNSU](https://web.archive.org/web/20060306192409/http://www.mnsu.edu/emuseum/biology/humanevolution/robustus.html)
  - [Archaeology
    Info](http://www.archaeologyinfo.com/australopithecusrobustus.htm)
  - [Smithsonian](http://www.mnh.si.edu/anthro/humanorigins/ha/rob.htm)
  - [Most complete ape-man skull found - but he is a
    she](http://www.exn.ca/Stories/2000/04/27/55.asp)
  - [Researchers discuss ape-man fossil
    find](http://www.trussel.com/prehist/news195.htm)
  - \[<http://www.profleeberger.com/coopersmain.html>| Coopers Cave Home
    Page\]

[Category:早期人類](../Category/早期人類.md "wikilink")
[Category:傍人](../Category/傍人.md "wikilink")

1.
2.