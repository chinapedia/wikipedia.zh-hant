[Positions_of_ancient_continents,_550_million_years_ago.jpg](https://zh.wikipedia.org/wiki/File:Positions_of_ancient_continents,_550_million_years_ago.jpg "fig:Positions_of_ancient_continents,_550_million_years_ago.jpg")
[Pannotia_-_2.png](https://zh.wikipedia.org/wiki/File:Pannotia_-_2.png "fig:Pannotia_-_2.png")

**潘諾西亞大陸**（Pannotia）是個理論上的史前[超大陸](../Page/超大陸.md "wikilink")，最初是由[地質學家Ian](../Page/地質學家.md "wikilink")
W. D.
Dalziel在1997年提出，形成於6億年前的[泛非造山作用](../Page/泛非造山作用.md "wikilink")（Pan-African
orogeny），並在5億4000萬年前的[前寒武紀分裂](../Page/前寒武紀.md "wikilink")。\[1\]

## 形成

大約7.5億年前，[羅迪尼亞大陸分裂成](../Page/羅迪尼亞大陸.md "wikilink")[原勞亞大陸](../Page/原勞亞大陸.md "wikilink")、[剛果克拉通](../Page/剛果克拉通.md "wikilink")、[原岡瓦那大陸](../Page/原岡瓦那大陸.md "wikilink")（[岡瓦那大陸除去剛果地盾與](../Page/岡瓦那大陸.md "wikilink")[南極洲的範圍](../Page/南極洲.md "wikilink")）。

原勞亞大陸進一步分裂，朝[南極移動](../Page/南極.md "wikilink")。原盤古大陸逆時針反轉。在6億年前，剛果克拉通位於原勞亞大陸各大陸與原岡瓦那大陸之間，三者聚合成潘諾西亞大陸。潘諾西亞大陸的大部分位於極區之內，而證據顯示這個時代有大面積的冰河覆蓋者，遠大於[地質時代的任何時期](../Page/地質時代.md "wikilink")。\[2\]

## 地質學與演變

潘諾西亞大陸的形狀類似**V**字形，開口往東北。開口內側為[泛大洋](../Page/泛大洋.md "wikilink")，海底有[中洋脊](../Page/中洋脊.md "wikilink")，是今日[太平洋的前身](../Page/太平洋.md "wikilink")。潘諾西亞大陸的外側環繞者[泛非洋](../Page/泛非洋.md "wikilink")。

潘諾西亞大陸的存在時間很短。組合潘諾西亞的各大陸，是以錯動方式聚合。在5.4億年前，或潘諾西亞大陸形成的6000萬年後，潘諾西亞大陸分裂成四個大陸：[勞倫大陸](../Page/勞倫大陸.md "wikilink")、[波羅地大陸](../Page/波羅地大陸.md "wikilink")、[西伯利亞大陸](../Page/西伯利亞大陸.md "wikilink")、[岡瓦那大陸](../Page/岡瓦那大陸.md "wikilink")。泛大洋隨者潘諾西亞大陸的分裂而擴張。這四個大陸在2.5億年前再度聚合，形成[盤古大陸](../Page/盤古大陸.md "wikilink")。\[3\]

在1994年，有科學家提出類似的概念，認為在[新元古代末期](../Page/新元古代.md "wikilink")，岡瓦那大陸形成，而岡瓦那大陸原本屬於一個更為大型的超大陸。

## 外部連結

  - [前寒武紀晚期的潘諾西亞大陸(圖片)](http://scotese.com/precambr.htm)

## 參考資料

<references />

[P](../Category/超大陸.md "wikilink") [P](../Category/元古宙.md "wikilink")

1.
2.
3.