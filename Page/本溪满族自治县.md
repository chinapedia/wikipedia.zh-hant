**本溪满族自治县**是[中国](../Page/中国.md "wikilink")[辽宁省](../Page/辽宁省.md "wikilink")[本溪市所辖的一个](../Page/本溪市.md "wikilink")[自治县](../Page/自治县.md "wikilink")。

## 行政区划

本溪满族自治县辖：

  - 10个镇：[小市镇](../Page/小市镇_\(本溪县\).md "wikilink")、[田师傅镇](../Page/田师傅镇.md "wikilink")、[南甸镇](../Page/南甸镇_\(本溪县\).md "wikilink")、[碱厂镇](../Page/碱厂镇.md "wikilink")、[高官镇](../Page/高官镇.md "wikilink")、[清河城镇](../Page/清河城镇.md "wikilink")、[草河掌镇](../Page/草河掌镇.md "wikilink")、[草河城镇](../Page/草河城镇.md "wikilink")、[草河口镇](../Page/草河口镇.md "wikilink")、[连山关镇](../Page/连山关镇.md "wikilink")。
  - 1个乡：[东营坊乡](../Page/东营坊乡.md "wikilink")。
  - 1个街道办事处：[观音阁街道办事处](../Page/观音阁街道_\(本溪县\).md "wikilink")。

## 外部链接

  - [本溪市本溪满族自治县政府网站](http://www.bx.gov.cn/)

[辽](../Page/category:中国满族自治县.md "wikilink")

[本溪满族自治县](../Category/本溪满族自治县.md "wikilink")
[县/自治县](../Category/本溪区县.md "wikilink")
[本溪](../Category/辽宁省民族自治县.md "wikilink")