**李玉**，字玄玉，號蘇門嘯侶，又號一笠庵主人。[江蘇](../Page/江蘇.md "wikilink")[吳縣人](../Page/吳縣.md "wikilink")。清初著名戲曲作家。約生於明萬曆年間，卒於康熙二十年（1681）前後。\[1\]

## 生平

平生好奇學古，管花腸篆。[吳偉業稱](../Page/吳偉業.md "wikilink")“其才足以上下千載，其學足以囊括藝林”，但命運數奇不偶，連厄於有司。遭官府壓制。\[2\]玄玉之父係萬曆首輔[申時行的家奴](../Page/申時行.md "wikilink")，爲申府孫公子所抑，不得應科舉。晚年參加鄉試，僅得副榜。明亡後絕意仕進，故一生未曾爲官。\[3\]在蘇州戲曲氛圍中，李玉於詞曲獨有癖好。他既富才情，又嫻音律，發爲聲歌，每借韻人韻事，譜之宮商，以抒壘塊之氣。

李玉是明清以來創作最宏富的傳奇作家。\[4\]作品題材廣泛，結構精鍊，適合舞臺表演，以致“每一紙落，雞林好事者爭被管絃，如達夫、昌齡，聲高當代”（錢謙益《眉山秀題詞》），於劇壇獨樹一幟，爲蘇州派作家的主要代表。

## 作品

**存世作品**

  - [一捧雪](../Page/一捧雪.md "wikilink")
  - [人獸關](../Page/人獸關.md "wikilink")
  - [永團圓](../Page/永團圓.md "wikilink")
  - [占花魁](../Page/占花魁.md "wikilink")
  - [麒麟閣](../Page/麒麟閣.md "wikilink")
  - [眉山秀](../Page/眉山秀.md "wikilink")
  - [牛頭山](../Page/牛頭山.md "wikilink")
  - [昊天塔](../Page/昊天塔.md "wikilink")
  - [風雲會](../Page/風雲會.md "wikilink")
  - [五高風](../Page/五高風.md "wikilink")
  - [連城璧](../Page/連城璧.md "wikilink")
  - [七國傳](../Page/七國傳.md "wikilink")
  - [萬里圓](../Page/萬里圓.md "wikilink")
  - [兩鬚眉](../Page/兩鬚眉.md "wikilink")
  - [太平錢](../Page/太平錢.md "wikilink")
  - [千忠錄](../Page/千忠錄.md "wikilink")
  - [清忠譜](../Page/清忠譜.md "wikilink")（與[畢魏](../Page/畢魏.md "wikilink")、[葉時章](../Page/葉時章.md "wikilink")、[朱㿥合撰](../Page/朱㿥.md "wikilink")）
  - [一品爵](../Page/一品爵.md "wikilink")（與[朱良卿合撰](../Page/朱良卿.md "wikilink")）
  - [埋輪亭](../Page/埋輪亭.md "wikilink")（與[朱佐朝合撰](../Page/朱佐朝.md "wikilink")，殘）
  - [洛陽橋](../Page/洛陽橋.md "wikilink")（殘）

**佚作**

  - [三生果](../Page/三生果.md "wikilink")
  - [長生像](../Page/長生像.md "wikilink")
  - [鳳雲翹](../Page/鳳雲翹.md "wikilink")
  - [雙龍佩](../Page/雙龍佩.md "wikilink")
  - [千里舟](../Page/千里舟.md "wikilink")
  - [虎丘山](../Page/虎丘山.md "wikilink")
  - [武當山](../Page/武當山.md "wikilink")
  - [掛玉帶](../Page/掛玉帶.md "wikilink")
  - [意中緣](../Page/意中緣.md "wikilink")
  - [萬民安](../Page/萬民安.md "wikilink")
  - [麒麟種](../Page/麒麟種.md "wikilink")
  - [秦樓月](../Page/秦樓月.md "wikilink")
  - [上苑春](../Page/上苑春.md "wikilink")
  - [清平調](../Page/清平調.md "wikilink")
  - [五侯封](../Page/五侯封.md "wikilink")
  - [洪都賦](../Page/洪都賦.md "wikilink")
  - [燕雙飛](../Page/燕雙飛.md "wikilink")
  - [銅雀臺](../Page/銅雀臺.md "wikilink")
  - [洛神廟](../Page/洛神廟.md "wikilink")
  - [珊瑚屏](../Page/珊瑚屏.md "wikilink")

**評點**

  - [玉簪記](../Page/玉簪記.md "wikilink")（存蘇州寧致堂刊一笠庵評本）

**曲譜**

  - [北詞廣正譜](../Page/北詞廣正譜.md "wikilink")

**參與曲譜編撰**

  - 張大復《[寒山堂南曲譜](../Page/寒山堂南曲譜.md "wikilink")》
  - 沈自晉《[南詞新譜](../Page/南詞新譜.md "wikilink")》
  - 鈕少雅《[南曲九宮正始](../Page/南曲九宮正始.md "wikilink")》

[category:中國劇作家](../Page/category:中國劇作家.md "wikilink")
[category:清朝作家](../Page/category:清朝作家.md "wikilink")

## 注釋

<div class="references-small">

<references />

</div>

[Category:李姓](../Category/李姓.md "wikilink")
[L](../Category/清朝戲曲家.md "wikilink")
[L](../Category/吳縣人.md "wikilink")
[Y玉](../Category/李姓.md "wikilink")

1.  李玉的生卒年至今仍無法確知。吴新雷在《李玉生平、交游、作品考》一文考证李玉约生于明万历十九年（1591年），卒年推定在清康熙十年（1671年）。歐陽代发《李玉生卒年考辨》一文推定李玉约生于明万历三十九年（1611年），卒于清康熙十六年（1677年）。
2.  吳偉業：《北詞廣正譜序》。
3.  焦循：《劇說》卷四。
4.  《传奇汇考标目》记载李玉作传奇三十二本。吴新雷认为李玉一生的戏剧作品共四十二种。