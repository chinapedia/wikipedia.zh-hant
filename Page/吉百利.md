[HK_Food_Cadbury_Schweppes_Time_Out_Chocolate_ab.jpg](https://zh.wikipedia.org/wiki/File:HK_Food_Cadbury_Schweppes_Time_Out_Chocolate_ab.jpg "fig:HK_Food_Cadbury_Schweppes_Time_Out_Chocolate_ab.jpg")產品\]\]
**吉百利股份有限公司**（英文：**Cadbury
Plc**）。是[英國](../Page/英國.md "wikilink")[伯明翰](../Page/伯明翰.md "wikilink")[糖果製造商](../Page/糖果.md "wikilink")，也是英國[皇家認證供應商](../Page/皇家認證.md "wikilink")，公司創立於1824年，現屬於[億滋國際旗下全資品牌](../Page/億滋國際.md "wikilink")。

## 歷史

1824年，創辦人約翰‧吉百利於[伯明翰街邊銷售飲用巧克力](../Page/伯明翰.md "wikilink")。[1831年](../Page/1831年.md "wikilink")，他開設工廠生產飲用巧克力。至[1849年](../Page/1849年.md "wikilink")，吉百利工廠推出塊狀巧克力，並於伯明翰交易會上公開展出。[1854年獲英國皇室青睞](../Page/1854年.md "wikilink")，成為御用巧克力品牌。

2007年，私募基金[黑石集团](../Page/黑石集团.md "wikilink")、KKR与Lion
Capital正考虑再向吉百利玉泉提出收购汽水业务计划，由黑石为首的财团曾提出以介乎64-69亿英镑作价收购该等汽水业务，但有关方案于9月初已遭吉百利玉泉拒绝。

2008年4月11日吉百利分拆美國飲料業務子公司Dr Pepper Snapple
Group，成為一家公開上市公司。根據分拆方案，每100股吉百利股票在拆分後將分得64股倫敦上市的吉百利股票和12股紐約上市的Dr
Pepper股票。

2008年12月24日[朝日啤酒有限公司](../Page/朝日啤酒.md "wikilink")，同意以5.50億英鎊，735億[日圓](../Page/日圓.md "wikilink")（8.08億美元）收購吉百利在澳洲的飲料業務和史威士公司的非酒精飲料品牌。

2010年1月19日，[卡夫同意以每股](../Page/卡夫.md "wikilink")8.4英鎊，收購價包括5英鎊現金及0.1874股卡夫新股換取1股吉百利股票總價值約117億英鎊（197億美元）收購吉百利。

## 旗下品牌

### 飲料品牌

  - [Dr Pepper](../Page/Dr_Pepper.md "wikilink")
  - Snapple
  - A\&W Root Beer
  - [玉泉](../Page/玉泉汽水.md "wikilink")（Schweppes）
  - [新奇士](../Page/:en:Sunkist_\(soft_drink\).md "wikilink")（Sunkist）

### 其他

  - [荷氏薄荷糖](../Page/荷氏薄荷糖.md "wikilink")（Hall's）

## 沙門氏菌醜聞

2006年6月23日，英國吉百利宣布在[英國及](../Page/英國.md "wikilink")[愛爾蘭全面回收](../Page/愛爾蘭.md "wikilink")7款逾100萬[朱古力條](../Page/朱古力.md "wikilink")。事緣該公司位於[赫里福德郡的馬爾布魯克](../Page/赫里福德郡.md "wikilink")（Marlbrook）工場，一條喉管早前爆裂，漏出清洗儀器的污水，污染朱古力輸送帶上。廠方早於1月中已發現有朱古力受感染[沙門氏菌](../Page/沙門氏菌.md "wikilink")，追蹤後得知該工場是源頭，但直至6月19日始通報英國衛生當局，最終在政府施壓下，產品需要[回收](../Page/回收.md "wikilink")，當中包括250克裝土耳其牛奶朱古力（Dairy
Milk Turkish）、焦糖牛奶朱古力（Dairy Milk Caramel）、薄荷牛奶朱古力（Dairy Milk Mint）、Dairy
Milk 8 chunk牛奶朱古力、1,000克裝牛奶朱古力、105克裝牛奶朱古力復活蛋（Dairy Milk Buttons Easter
Eggs）和Freddo朱古力等。

吉百利指該批朱古力中，每100克只含0.3個單位的沙門氏菌，它們要含有100萬單位才會引致輕微肚痛，該公司認為中毒風險很低，但英國食物標準管理局質疑細菌未必會平均分佈在朱古力上，有可能危害公眾，要求回收。這亦成為英國零售業繼回收「[蘇丹一號](../Page/蘇丹一號.md "wikilink")」產品後，當時第二場最大規模的回收行動。英國[衛報指](../Page/衛報.md "wikilink")，英格蘭與威爾士自3月起已發現45宗感染罕有「[蒙得維的亞沙門氏菌](../Page/蒙得維的亞沙門氏菌.md "wikilink")」（Salmonella
Montevideo）的食物中毒事件，病者包括8名未滿周歲的嬰兒及22名4歲以下的小童，懷疑事件是否與吉百利有關。

## 參考資料

## 外部链接

  -
[Category:英國食品公司](../Category/英國食品公司.md "wikilink")
[Category:巧克力糖](../Category/巧克力糖.md "wikilink")
[Category:巧克力公司](../Category/巧克力公司.md "wikilink")
[Category:1824年成立的公司](../Category/1824年成立的公司.md "wikilink")
[C](../Category/英國品牌.md "wikilink") [C](../Category/巧克力品牌.md "wikilink")
[Category:倫敦證券交易所已除牌公司](../Category/倫敦證券交易所已除牌公司.md "wikilink")
[Category:英國皇家御用企業](../Category/英國皇家御用企業.md "wikilink")
[Category:1824年英格蘭建立](../Category/1824年英格蘭建立.md "wikilink")