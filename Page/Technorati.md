**Technorati**是一个著名的[网志](../Page/网志.md "wikilink")[搜索引擎](../Page/搜索引擎.md "wikilink")，截至2007年4月，Technorati已经索引了超过7千万个网志網站\[1\]。可以说，Technorati已经成为世界上最重要的一个网志搜索引擎。Technorati由Dave
Sifry建立，总部位于[美国](../Page/美国.md "wikilink")[加州的](../Page/加州.md "wikilink")[旧金山](../Page/旧金山.md "wikilink")，其竞争对手包括[Google](../Page/Google.md "wikilink")、[Yahoo](../Page/Yahoo.md "wikilink")、[PubSub](../Page/PubSub.md "wikilink")、[IceRocket等](../Page/IceRocket.md "wikilink")。

Technorati在2006年西南西音乐节（SXSW）中获得技术最高分奖（Technical Achievement）和最佳宠爱奖（Best
of Show）。

## 注釋

<div class="references-small">

<references/>

</div>

## 外部链接

  - [Technorati网站](http://technorati.com)
  - [Technorati网志](https://web.archive.org/web/20060610120346/http://technorati.com/weblog/)

[Category:網誌](../Category/網誌.md "wikilink")
[Category:網路搜尋引擎](../Category/網路搜尋引擎.md "wikilink")
[Category:Web 2.0](../Category/Web_2.0.md "wikilink")

1.  Technorati的每季統計報告[The State of the Live Web,
    April 2007](http://www.sifry.com/alerts/archives/000493.html)