**少孢根霉**（[學名](../Page/學名.md "wikilink")：*Rhizopus
oligosporus*）是[根黴屬的一種真菌](../Page/根黴屬.md "wikilink")，常作為製作[丹貝的](../Page/丹貝.md "wikilink")[發酵劑](../Page/發酵劑.md "wikilink")，它可以長出白色蓬鬆的[菌絲體](../Page/菌絲體.md "wikilink")，與被部份分解的[大豆結合成白色的餅狀食品](../Page/大豆.md "wikilink")\[1\]，這種真菌可以在[印尼攝氏](../Page/印尼.md "wikilink")30-40度的環境下快速生長，且與[蛋白酶解的活性高](../Page/蛋白酶解.md "wikilink")，又可以分泌抑制其他微生物生長的物質，因而適合用來生產丹貝\[2\]\[3\]。少孢根霉可能於數個世紀前，在印尼由[馴化而來](../Page/馴化.md "wikilink")，並在馴化的過程中丟失了製造與rhizonin等毒素的基因\[4\]。

少孢根霉被食用後，會在人體中繼續分泌[抗生素](../Page/抗生素.md "wikilink")，抑制[金黃色葡萄球菌與](../Page/金黃色葡萄球菌.md "wikilink")[枯草桿菌等](../Page/枯草桿菌.md "wikilink")[革蘭氏陽性菌的生長](../Page/革蘭氏陽性菌.md "wikilink")，有研究顯示常食用丹貝的人較不易發生腸道感染\[5\]。

## 分類

有研究將少孢根霉視為的[變種](../Page/變種_\(植物學\).md "wikilink")（*Rhizopus microsporus
var.
oligosporus*）\[6\]。2014年，[分子種系發生學的研究結果顯示少孢根霉與微孢毛黴是同一種物種](../Page/分子種系發生學.md "wikilink")，兩者互為[同物異名](../Page/同物異名.md "wikilink")\[7\]。

## 參考資料

## 外部連結

  - [分類地位](http://www.indexfungorum.org/Names/NamesRecord.asp?RecordID=116951)

[Category:根黴属](../Category/根黴属.md "wikilink")
[Category:用於食品產業的黴菌](../Category/用於食品產業的黴菌.md "wikilink")

1.  Shurtleff, W. & Aoyagi, A. 2001. The book of tempeh. 2 2. Ten Speed
    Press. Berkeley, California pp.
2.
3.
4.
5.
6.
7.