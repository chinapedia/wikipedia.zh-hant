**人机互动**（\[1\]，缩写：HCI，或
，缩写：HMI），是一門研究[系统與用户之間的互動關係的學問](../Page/系统.md "wikilink")。系统可以是各种各样的[机器](../Page/机器.md "wikilink")，也可以是[计算机化的系统和](../Page/计算机.md "wikilink")[软件](../Page/计算机软件.md "wikilink")。人机交互界面通常是指用户可见的部分。用户通过人机交互界面与系统交流，並進行操作。小如收音機的播放按鍵，大至飛機上的儀表板、或是發電廠的控制室。

人机交互界面的设计要包含用户对系统的理解（即[心智模型](../Page/心智模型.md "wikilink")），那是为了系统的[可用性或者](../Page/可用性.md "wikilink")[用户友好性](../Page/用户友好性.md "wikilink")。

## 人機界面

[Scada_std_anim_no_lang.gif](https://zh.wikipedia.org/wiki/File:Scada_std_anim_no_lang.gif "fig:Scada_std_anim_no_lang.gif")

**人機界面 MMI: man–machine
interface**或**人機互動界面**（或）大量運用在[工業與](../Page/工業.md "wikilink")[商業上](../Page/商業.md "wikilink")，簡單的區分為「輸入」（Input）與「輸出」（Output）兩種，輸入指的是由人來進行[機械或設備的操作](../Page/機械.md "wikilink")，如把手、開關、門、指令
(命令)的下達或保養維護等，而輸出指的是由機械或設備發出來的通知，如故障、警告、操作說明提示等，好的人機介面會幫助使用者更簡單、更正確、更迅速的操作機械，也能使機械發揮最大的效能並延長使用壽命，而目前市面上所指的人機介面則多是狹義的指在[軟體人性化的操作介面上](../Page/軟體.md "wikilink")。

## 互動模式

互動（Interaction）至少包括兩個要素，一個是使用者（User），一個是[系統](../Page/系統.md "wikilink")。

### Norman的[互動模式](../Page/互動模式.md "wikilink")

在人機互動的領域裡，Norman的互動模式或許是最有影響力的一種模式了，Norman的互動模式被區分成兩個主要的階段：「執行」（Execution）和「評估」（Evaluation），還可以進一步被區分成七個部份\[2\]：

1.  訂立目標
2.  組成意圖
3.  標明行動順序
4.  執行
5.  觀察系統
6.  詮釋系統狀態
7.  評估

其實以[電腦而言](../Page/電腦.md "wikilink")，電腦互動主要意義是以使用者為考量，而不是從設計者的[概念模型去切入](../Page/概念模型.md "wikilink")，如何讓使用者能控制系統的順序、速度，怎麼注意[資訊](../Page/資訊.md "wikilink")，都是人機互動中所關心的。人機互動的關鍵在於使用者瞭解了電腦能替我們做些什麼，及如何處理資訊，我們就可將大部份時間放在「人」的身上，而不是技術領域，所以一個成功的互動系统指人們告訴電腦如何去工作，而不是在技術上打轉。\[3\]

電腦界面的設計不光是單向的，而是設計者必須重視使用者的[回饋](../Page/回饋.md "wikilink")，並且進行調整，運用適當的設計，比如[美工圖案選單或是簡易的操作](../Page/美工圖案.md "wikilink")，讓使用者可以輕鬆的滿足自己的需求，有效率的執行工作，才能讓機器發揮最大的功能。\[4\]

## 概念模型

設計者或其他相關[工程人員](../Page/工程.md "wikilink")，所共同[設計的操作介面稱為概念模型](../Page/設計.md "wikilink")。使用者並無法直接與設計者進行對話，也鮮少透過說明書準確而有效地了解設計者的概念；透過與[介面的互動](../Page/介面.md "wikilink")，進而對該設計產品的功能及操作所形成的了解，則稱為使用者對於該產品所形成的[心智模型](../Page/心智模型.md "wikilink")。一般而言，使用者的[心智模型是在使用中自然逐步形成的](../Page/心智模型.md "wikilink")，同時，藉由與系統不斷地互動，使用者會不斷修正其心智模型。如果到了最後，使用者的[心智模型與](../Page/心智模型.md "wikilink")[設計師的](../Page/設計師.md "wikilink")[概念模型能一致或相當接近](../Page/概念模型.md "wikilink")，那麼，[介面設計就算是相當成功了](../Page/介面.md "wikilink")。\[5\]

概念模型可分為依據活動所設計或物件所設計的兩種導向。想要設計出具有使用性的[介面](../Page/介面.md "wikilink")，必須要了解活動的類型，以及人們在目前的[工具中發生了什麼問題](../Page/工具.md "wikilink")。\[6\]

### 活動型概念模型

根據活動所設計的[概念模型可分為四類](../Page/概念模型.md "wikilink")：

  - 指令型（Instructing）:
    一個指令一個動作，簡單有效率，例如[DOS作業系統及](../Page/DOS.md "wikilink")[word](../Page/word.md "wikilink")。
    對話型（Conversing）:
    系統與使用者進行對話，雙向互動，但容易發生系統誤解使用者的狀況，或是造成電話語音系統單向互動的問題。
    操作導航型（Manipulating & Navigating）:
    讓使用者用最自然的直覺去操作介面，如蘋果開創的圖形化作業系統，還有[電腦輔助設計系統都屬此類型的設計](../Page/電腦輔助設計.md "wikilink")。
    搜尋瀏覽型（Exploring & Browsing）:
    使用者依此系統搜尋[資訊](../Page/資訊.md "wikilink")，如google等[搜尋引擎及](../Page/搜尋引擎.md "wikilink")[入口網站](../Page/入口網站.md "wikilink")。\[7\]

### 物件型概念模型

[物件導向的概念模型將重點放在處於某些特定](../Page/物件導向.md "wikilink")[背景情境下使用的特定物件](../Page/背景.md "wikilink")，它往往與其在[現實生活中的原型極為類似](../Page/現實.md "wikilink")。\[8\]

## 参考文献

## 外部連結

  - [Human-Centered Computing Education Digital
    Library](https://web.archive.org/web/20110817181257/http://hccedl.cc.gatech.edu/)
  - [人機介面互動設計趨勢](https://www.digitimes.com.tw/iot/article.asp?cat=158&id=0000233000_m5clfkdw1esc67lrl9uhx)
  - [人機介面-電子工程專輯](https://web.archive.org/web/20160304202807/http://www.eettaiwan.com/SEARCH/ART/%E4%BA%BA%E6%A9%9F%E4%BB%8B%E9%9D%A2.HTM?jumpto=view_welcomead_1371543046333)

## 参见

  - [人机工程学](../Page/人机工程学.md "wikilink")
  - [用户界面](../Page/用户界面.md "wikilink")
  - [交互设计](../Page/交互设计.md "wikilink")
  - [游戏](../Page/游戏.md "wikilink")
  - [游戏设计](../Page/游戏设计.md "wikilink")
  - [互動模式](../Page/互動模式.md "wikilink")
  - [概念模型](../Page/概念模型.md "wikilink")

{{-}}

[人機互動](../Category/人機互動.md "wikilink")

1.  [人機互動](http://www.nsc.gov.tw/_newfiles/popular_science.asp?add_year=2003&popsc_aid=267)
    －[行政院國家科學委員會](../Page/行政院國家科學委員會.md "wikilink")

2.  Dix et al. (1998) Human-Computer Interaction, Ch3 : The interaction

3.
4.
5.  《科學發展》雜誌2003年8月，368期，18至23頁（pdf檔）

6.  Preece(2002)Interaction design:Beyond human-computer
    interaction,Ch2:Understanding and conceptualizing interaction

7.
8.