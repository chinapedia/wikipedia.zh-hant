**玉名市**（）是一個位於[日本](../Page/日本.md "wikilink")[熊本縣北部的](../Page/熊本縣.md "wikilink")[城市](../Page/城市.md "wikilink")。在2005年10月3日由舊玉名市與相鄰的[岱明町](../Page/岱明町.md "wikilink")、[橫島町](../Page/橫島町.md "wikilink")、[天水町合併而成](../Page/天水町.md "wikilink")。

## 地理

  - 山：筒岳
  - 河川：[菊池川](../Page/菊池川.md "wikilink")

### 市內主要地區

  - [中](../Page/中_\(玉名市\).md "wikilink")：市中心所在地
  - [松木](../Page/松木_\(玉名市\).md "wikilink")：社會保險事務所及水淨化中心廠所在地
  - [繁根木](../Page/繁根木_\(玉名市\).md "wikilink")：市政府、武道館、市內污水處理場所在地
  - [築地](../Page/築地_\(玉名市\).md "wikilink")
  - [亀甲](../Page/亀甲_\(玉名市\).md "wikilink")：市內最大的超級市場所在地
  - [大倉](../Page/大倉_\(玉名市\).md "wikilink")
  - [伊倉](../Page/伊倉_\(玉名市\).md "wikilink")

## 歷史

[戰國時代由於多次戰爭始終無法順利發展](../Page/戰國時代_\(日本\).md "wikilink")，直到[加藤清正進入](../Page/加藤清正.md "wikilink")[肥後國](../Page/肥後國.md "wikilink")，致力於各項開發，使得人口增加開始繁榮。\[1\]

### 沿革

  - 1889年4月1日：實施町村制，在現在的轄區在當時分屬：[玉名郡](../Page/玉名郡.md "wikilink")[高瀨町](../Page/高瀨町.md "wikilink")、彌富村、滑石村、豐水村、石貫村、玉名村、小田村、梅林村、伊倉村、築山村、大濱町、八嘉村、月瀨村、大野村、睦合村、高道村、鍋村、小天村、玉水村、橫島村
  - 1899年2月2日：伊倉村改制為[伊倉町](../Page/伊倉町.md "wikilink")。
  - 1942年5月20日：高瀨町與彌富村[合併為](../Page/市町村合併.md "wikilink")**玉名町**。
  - 1954年4月1日：玉名町、滑石村、豐水村、石貫村、玉名村、小田村、梅林村、伊倉町、築山村、大濱町、八嘉村、月瀨村合併為**玉名市**。
  - 1954年10月1日：小天村與玉水村合併為天水村。
  - 1955年4月1日：大野村、睦合村、高道村、鍋村合併為岱明村。
  - 1956年1月1日：南關町的部份轄區被併入玉名市。
  - 1960年10月1日：天水村改制為[天水町](../Page/天水町.md "wikilink")。
  - 1965年4月1日：岱明村改制為[岱明町](../Page/岱明町.md "wikilink")。
  - 1968年11月1日：橫島村改制為[横島町](../Page/横島町.md "wikilink")。
  - 2005年10月3日：玉名市、岱明町、横島町、天水町合併為新設置的**玉名市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>高瀨町</p></td>
<td><p>1942年5月20日<br />
玉名町</p></td>
<td><p>1954年4月1日<br />
玉名市</p></td>
<td><p>2005年10月3日<br />
玉名市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>彌富村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>大濱町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>伊倉村</p></td>
<td><p>1899年2月2日<br />
改制為伊倉町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>築山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>滑石村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>豐水村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>八嘉村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>梅林村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>小田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>月瀨村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>玉名村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>石貫村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>小天村</p></td>
<td><p>1954年10月1日<br />
天水村</p></td>
<td><p>1960年10月1日<br />
改制為天水町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>玉水村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大野村</p></td>
<td><p>1955年4月1日<br />
岱明村</p></td>
<td><p>1965年4月1日<br />
改制為岱明町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>睦合村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>鍋村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>高道村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>橫島村</p></td>
<td><p>1968年11月1日<br />
改制為橫島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 產業

  - 全市總生產額：1,931億[日圓](../Page/日圓.md "wikilink")（2004年）
  - 特産：[海苔](../Page/海苔.md "wikilink")、[蜂蜜](../Page/蜂蜜.md "wikilink")、[辛子蓮根](../Page/辛子蓮根.md "wikilink")、[玉名拉麵](../Page/玉名拉麵.md "wikilink")
  - 玉名市內主要企業工廠
      - [凸版印刷熊本工場](../Page/凸版印刷_\(公司\).md "wikilink")
      - [普利司通熊本工場](../Page/普利司通.md "wikilink")

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")（JR九州）
      - [九州新幹線](../Page/九州新幹線.md "wikilink")：[新玉名站](../Page/新玉名站.md "wikilink")
      - [鹿兒島本線](../Page/鹿兒島本線.md "wikilink")：[大野下站](../Page/大野下站.md "wikilink")
        - [玉名站](../Page/玉名站.md "wikilink") -
        [肥後伊倉站](../Page/肥後伊倉站.md "wikilink")

市中心的主要車站為玉名站，從玉名車站至[熊本站搭乘普通列車約需](../Page/熊本站.md "wikilink")30分鐘，搭乘[特急列車約需](../Page/特別急行列車.md "wikilink")20分鐘；至[博多站搭乘特急列車大約需](../Page/博多站.md "wikilink")1小時5分鐘。

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速道路</dt>

</dl>
<p>最近交流道為<a href="../Page/九州高速公路.md" title="wikilink">九州高速公路</a><a href="../Page/菊水交流道.md" title="wikilink">菊水交流道</a>、<a href="../Page/南關交流道.md" title="wikilink">南關交流道</a>、<a href="../Page/植木交流道.md" title="wikilink">植木交流道</a>。</p>
<dl>
<dt>國道</dt>

</dl>
<ul>
<li><a href="../Page/國道208號.md" title="wikilink">國道208號</a></li>
<li><a href="../Page/國道501號.md" title="wikilink">國道501號</a></li>
</ul></td>
<td><dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li><a href="../Page/熊本縣道1號熊本玉名線.md" title="wikilink">熊本縣道1號熊本玉名線</a></li>
<li><a href="../Page/熊本縣道4號玉名八女線.md" title="wikilink">熊本縣道4號玉名八女線</a></li>
<li><a href="../Page/熊本縣道6號玉名立花線.md" title="wikilink">熊本縣道6號玉名立花線</a></li>
<li><a href="../Page/熊本縣道16號玉名山鹿線.md" title="wikilink">熊本縣道16號玉名山鹿線</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光資源

### 景點

  - [玉名溫泉](../Page/玉名溫泉.md "wikilink")
  - 大野下大蘇鐵
  - [小天溫泉](../Page/小天溫泉.md "wikilink")
  - 草枕溫泉天水
  - 笠智衆生家、記念碑

### 祭典、活動

  - 玉名大俵祭
  - 肥後高瀨花しょうぶ祭

## 教育

### 大學

  - [九州看護福祉大學](../Page/九州看護福祉大學.md "wikilink")

### 高等學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/熊本縣立玉名高等學校.md" title="wikilink">熊本縣立玉名高等學校</a></li>
<li><a href="../Page/熊本縣立北稜高等學校.md" title="wikilink">熊本縣立北稜高等學校</a></li>
<li><a href="../Page/熊本縣立玉名工業高等學校.md" title="wikilink">熊本縣立玉名工業高等學校</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/專修大學玉名高等學校.md" title="wikilink">專修大學玉名高等學校</a></li>
<li><a href="../Page/玉名女子高等學校.md" title="wikilink">玉名女子高等學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>玉名市立玉名中學校</li>
<li>玉名市立玉陵中學校</li>
<li>玉名市立玉南中學校</li>
</ul></td>
<td><ul>
<li>玉名市立有明中學校</li>
<li>玉名市立岱明中學校</li>
<li>玉名市立天水中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 29%" />
<col style="width: 29%" />
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>玉名市立玉名町小學校</li>
<li>玉名市立築山小學校</li>
<li>玉名市立滑石小學校</li>
<li>玉名市立大濱小學校</li>
<li>玉名市立豐水小學校</li>
<li>玉名市立八嘉小學校</li>
<li>玉名市立伊倉小學校</li>
</ul></td>
<td><ul>
<li>玉名市立月瀨小學校</li>
<li>玉名市立梅林小學校</li>
<li>玉名市立玉名小學校</li>
<li>玉名市立石貫小學校</li>
<li>玉名市立三川小學校</li>
<li>玉名市立小田小學校</li>
<li>玉名市立大野小學校</li>
</ul></td>
<td><ul>
<li>玉名市立睦合小學校</li>
<li>玉名市立高道小學校</li>
<li>玉名市立鍋小學校</li>
<li>玉名市立橫島小學校</li>
<li>玉名市立玉水小學校</li>
<li>玉名市立小天小學校</li>
<li>玉名市立小天東小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 友好都市

### 海外

  - [瓦房店市](../Page/瓦房店市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")
    [遼寧省](../Page/遼寧省.md "wikilink")
    [大連市](../Page/大連市.md "wikilink")）：於1994年10月6日締結友好都市。\[2\]

  - [克拉林達](../Page/克拉林達_\(愛阿華州\).md "wikilink")（[美國](../Page/美國.md "wikilink")
    [愛荷華州](../Page/愛荷華州.md "wikilink")）：於1996年4月締結友好都市。\[3\]

## 玉名市出身的名人

  - [大麻唯男](../Page/大麻唯男.md "wikilink")：[政治家](../Page/政治家.md "wikilink")、前[國務大臣](../Page/國務大臣.md "wikilink")、國家公安委員長
  - [福田良三](../Page/福田良三.md "wikilink")：[大日本帝國海軍軍人](../Page/大日本帝國海軍.md "wikilink")
  - [井上聡](../Page/井上聡.md "wikilink")：[喜劇演員](../Page/喜劇演員.md "wikilink")
  - [富田浩太郎](../Page/富田浩太郎.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [笠智衆](../Page/笠智衆.md "wikilink")：俳優
  - [前田智徳](../Page/前田智徳.md "wikilink")：[職業棒球選手](../Page/職業棒球選手.md "wikilink")
  - [松岡健一](../Page/松岡健一.md "wikilink")：職業棒球選手
  - [天水山正則](../Page/天水山正則.md "wikilink")：前[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")
  - [普天王水](../Page/普天王水.md "wikilink")：大相撲力士
  - [田尻義博](../Page/田尻義博.md "wikilink")：[職業摔角手](../Page/職業摔角手.md "wikilink")
  - [植尾勝浩](../Page/植尾勝浩.md "wikilink")：[賽車手](../Page/賽車手.md "wikilink")
  - [金森通倫](../Page/金森通倫.md "wikilink")：[牧師](../Page/牧師.md "wikilink")

## 參考資料

## 外部連結

  - [玉名觀光協會](https://web.archive.org/web/20081210164657/http://www.d3.dion.ne.jp/~kankou1/)

  - [玉名地域振興局](https://web.archive.org/web/20090103003332/http://www.pref.kumamoto.jp/shinkoukyoku/tamana_hp/)

  - [天水町商工會](http://www.kumashoko.or.jp/tensui/)

  - [荒尾、玉名地區觀光推進協進會](https://web.archive.org/web/20090107093451/http://www.aratama.hinokuni-net.jp/)

  - [玉名溫泉觀光旅館協同組合](http://www.tamanaonsen.jp/)

  - [玉名商工會議所](http://www.tamana-cci.or.jp/)

<!-- end list -->

1.
2.
3.