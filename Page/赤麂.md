**赤麂**（學名*Muntiacus
muntjak*），又称**印度麂**，属[鹿科](../Page/鹿科.md "wikilink")[麂属](../Page/麂属.md "wikilink")，目前可分15個[亞種](../Page/亞種.md "wikilink")。

## 特征

赤麂是一種体型较大的[麂类](../Page/麂.md "wikilink")，体长1～1.2米，肩高50～55厘米。雄麂有小角，角冠基部分出一小枝，角柄前方有一黑色纵纹。長有突出上顎的犬齒，每年會脫角及重長；雌性無角。

赤麂眼下長有发达的眶下香腺，额腺较长，會分泌香油用以標示領域。

## 習性

赤麂性格溫馴細膽，受驚或遇到危險時會發出如狗吠的聲音，所以又有**吠鹿**(Barking
Deer)的稱呼。除了[交配季節外](../Page/交配.md "wikilink")，牠們一般獨居，但也有少數群居，甚至與其他大型動物一齊進食。[野狗與](../Page/野狗.md "wikilink")[緬甸蟒蛇是赤麂的主要天敵](../Page/緬甸蟒.md "wikilink")。

另外，由於赤麂天生心臟有問題，當受驚嚇時，心臟跳動可以超過二百下，若承受不了時可致急性心臟病而死。

赤麂為[植食性動物](../Page/植食性.md "wikilink")，主要以野果及幼葉為食糧，有時會捕捉昆蟲來吃。

[File:HK_MMuntjak_Male.JPG|雄性赤麂](File:HK_MMuntjak_Male.JPG%7C雄性赤麂)
[File:HK_MMuntjak_Female.JPG|雌性赤麂](File:HK_MMuntjak_Female.JPG%7C雌性赤麂)

## 分佈

赤麂主要分佈於[印度](../Page/印度.md "wikilink")、[東南亞及](../Page/東南亞.md "wikilink")[華南地區](../Page/華南.md "wikilink")。

## 亞種

這是15個亞種：

  - *M. m. annamensis*， [印度支那](../Page/印度支那.md "wikilink")
  - *M. m. aureus*， [印度半島](../Page/印度.md "wikilink")
  - *M. m. bancanus*
  - *M. m. curvostylis*， [泰國](../Page/泰國.md "wikilink")
  - *M. m. grandicornis*， 緬甸麂， [緬甸](../Page/緬甸.md "wikilink")
  - *M. m. malabaricus*， 南印度及[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")
  - *M. m. montanus*， 山羌， [蘇門答臘](../Page/蘇門答臘.md "wikilink")
  - *M. m. muntjak*， 爪哇麂，
    [爪哇及](../Page/爪哇.md "wikilink")[蘇門答臘南部](../Page/蘇門答臘.md "wikilink")
  - *M. m. nainggolani*， [巴里島](../Page/巴里島.md "wikilink")
  - *M. m. nigripes*， 黑腳麂，
    [越南及](../Page/越南.md "wikilink")[海南島](../Page/海南島.md "wikilink")
  - *M. m. peninsulae*， [馬來西亞](../Page/馬來西亞.md "wikilink")
  - *M. m. pleicharicus*， [婆羅洲南部](../Page/婆羅洲.md "wikilink")
  - *M. m. robinsoni*
  - *M. m. rubidus*， [婆羅洲北部](../Page/婆羅洲.md "wikilink")
  - *M. m. vaginalis*，
    [緬甸及](../Page/緬甸.md "wikilink")[中國西南部](../Page/中國.md "wikilink")

### 與山羌的分別

許多人會將赤麂與[山羌混淆](../Page/山羌.md "wikilink")，甚至誤將兩者視為同一種動物，但其實不然。事實上赤麂和[山羌是不同品種](../Page/山羌.md "wikilink")，兩者外形很相似，[山羌身形較小](../Page/山羌.md "wikilink")，膚色由黃色至棕色都有；赤麂身形較龐大，一般呈紅棕色。在2006年香港漁護署做大型普查，才發現在港的所謂「[山羌](../Page/山羌.md "wikilink")」，全部是赤麂。\[1\]
\[2\]

## 資料來源

<div class="references-small">

<references />

  - 《模糊的腳印》，ISBN 962-86308-2-2
  - 《香港陸上哺乳動物圖鑑》，ISBN 978-988-211-331-2，ISBN 988-211-331-1

</div>

## 參見

  - [香港哺乳動物](../Page/香港哺乳動物.md "wikilink")

[Category:麂屬](../Category/麂屬.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:中國哺乳動物](../Category/中國哺乳動物.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:泰國動物](../Category/泰國動物.md "wikilink")
[Category:印度哺乳動物](../Category/印度哺乳動物.md "wikilink")
[Category:斯里蘭卡動物](../Category/斯里蘭卡動物.md "wikilink")
[Category:越南動物](../Category/越南動物.md "wikilink")
[Category:緬甸動物](../Category/緬甸動物.md "wikilink")
[Category:寮國動物](../Category/寮國動物.md "wikilink")
[Category:馬來西亞動物](../Category/馬來西亞動物.md "wikilink")
[Category:印尼動物](../Category/印尼動物.md "wikilink")
[Category:柬埔寨動物](../Category/柬埔寨動物.md "wikilink")

1.  <http://hk.myblog.yahoo.com/littlematch2048/article?mid=19237&fid=-1&action=next>
2.  <http://news.sina.com.hk/cgi-bin/nw/show.cgi/2/1/1/709350/1.html>