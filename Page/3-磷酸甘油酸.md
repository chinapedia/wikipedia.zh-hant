**3-磷酸甘油酸**（或）是生物細胞中常見的分子之一，也是[糖解作用與](../Page/糖解作用.md "wikilink")[卡爾文循環過程裡的中間產物](../Page/卡爾文循環.md "wikilink")。(註：在[卡爾文循環當中簡寫為](../Page/卡爾文循環.md "wikilink")**PGA**)

在糖解作用中，3-磷酸甘油酸是由[1,3-雙磷酸甘油酸在](../Page/1,3-雙磷酸甘油酸.md "wikilink")[磷酸甘油酸激酶](../Page/磷酸甘油酸激酶.md "wikilink")（Phosphoglycerate
kinase）的催化中產生的。每一分子1,3-雙磷酸甘油酸會使一分子的[ADP轉變成為的](../Page/ADP.md "wikilink")[ATP](../Page/三磷酸腺苷.md "wikilink")，原理是接在1,3-雙磷酸甘油酸上的兩個[磷酸根](../Page/磷酸根.md "wikilink")，其中有一個轉移到ADP之上。這個反應需要[鎂離子](../Page/鎂.md "wikilink")（Mg<sup>2+</sup>）的幫助。

接下來3-磷酸甘油酸將會在[磷酸甘油酸變位酶](../Page/磷酸甘油酸變位酶.md "wikilink")（Phosphoglycerate）的催化下生成2-磷酸甘油酸，在此反應中，原本接在3-磷酸甘油酸的第3個碳上的磷酸根，將會轉移到變位酶上；然後原本在變位酶上的磷酸根，則會接到3-磷酸甘油酸的第2個碳上，反應前後的變位酶整體結構沒有變化。與上一步驟相同，此反應同樣需要Mg<sup>2+</sup>。

[Category:糖酵解](../Category/糖酵解.md "wikilink")
[Category:羧酸阴离子](../Category/羧酸阴离子.md "wikilink")
[Category:磷酸酯](../Category/磷酸酯.md "wikilink")
[Category:光合作用](../Category/光合作用.md "wikilink")