**叶赤铁路**是连接叶柏寿（现为[建平县](../Page/建平县.md "wikilink")）与[赤峰两地的铁路](../Page/赤峰市.md "wikilink")。自东南[辽宁省建平县起](../Page/辽宁省.md "wikilink")，向东北经[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")[宁城县](../Page/宁城县.md "wikilink")、[元宝山区至赤峰市](../Page/元宝山区.md "wikilink")。全长138.5公里\[1\]，其中内蒙古自治区境内104.9公里。全线由[沈阳铁路局叶柏寿车务段管辖](../Page/沈阳铁路局.md "wikilink")。

## 历史

叶赤铁路于1934年3月开工建设，1935年11月建成，当时称叶峰线。\[2\]。1945年日本投降时全线遭破坏停运。1946年国民党当局在叶柏寿成立[锦古线修复指挥部](../Page/锦古铁路.md "wikilink")，把叶峰线改称叶赤线，但并未修复叶赤铁路。

1952年8月，锦州铁路局组织2000多人修复叶赤线，11月竣工，1953年1月交付通车。属于锦州铁路局承德铁路分局叶柏寿各段管辖。

## 与其它铁路线的连接

  - [赤峰东站](../Page/赤峰东站.md "wikilink")：[京通铁路](../Page/京通铁路.md "wikilink")。
  - [叶柏寿站](../Page/叶柏寿站.md "wikilink")：[锦承铁路](../Page/锦承铁路.md "wikilink")。

## 参考资料

[Category:辽宁省铁路线](../Category/辽宁省铁路线.md "wikilink")
[Category:内蒙古自治区铁路线](../Category/内蒙古自治区铁路线.md "wikilink")
[Category:单线铁路](../Category/单线铁路.md "wikilink")
[Category:内燃化铁路](../Category/内燃化铁路.md "wikilink")
[Category:中国铁路沈阳局集团有限公司](../Category/中国铁路沈阳局集团有限公司.md "wikilink")
[Category:朝阳交通](../Category/朝阳交通.md "wikilink")
[Category:赤峰交通](../Category/赤峰交通.md "wikilink")

1.  [草原明珠赤峰站（铁流网）](http://www.tieliu.com.cn/cmtd2/2007/200701/2007-01-12/20070112100903_22715.html)
2.  [中国铁路百年（2）（青藏铁路网）](http://www.qh.xinhua.org/qztlw/2003-06/25/content_639501.htm)