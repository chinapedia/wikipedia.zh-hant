[Celery_cross_section.jpg](https://zh.wikipedia.org/wiki/File:Celery_cross_section.jpg "fig:Celery_cross_section.jpg")莖的横截面，可見維管束，其中包括[韌皮部和](../Page/韌皮部.md "wikilink")[木質部](../Page/木質部.md "wikilink")。\]\]
**维管束**是指维管植物（包括[蕨类植物](../Page/蕨类植物.md "wikilink")、[裸子植物和](../Page/裸子植物.md "wikilink")[被子植物](../Page/被子植物.md "wikilink")）的[维管组织](../Page/维管组织.md "wikilink")，由[木质部和](../Page/木质部.md "wikilink")[韧皮部成束状排列形成的结构](../Page/韧皮部.md "wikilink")。维管束多存在于[茎](../Page/茎.md "wikilink")（[草本植物和](../Page/草本植物.md "wikilink")[木本植物幼体](../Page/木本植物.md "wikilink")）、[叶](../Page/叶.md "wikilink")（叶中的维管束又称为**叶脉**）等器官中。维管束相互连接构成维管系统主要作用是为植物体输导[水分](../Page/水分.md "wikilink")、[无机盐和](../Page/无机盐.md "wikilink")[有机养料等](../Page/有机物.md "wikilink")，也有支持植物体的作用。

## 类型

维管束根据木质部和韧皮部的排列方式可以被分为三大类：\[1\]\[2\]

  - 外韧型：韧皮部位于木质部的外侧。
  - 双韧型：维管束内外周均为韧皮部（内生韧皮部和外生韧皮部）。如[南瓜的茎](../Page/南瓜.md "wikilink")。
  - 同心型：由一种维管组织包着另一种维管组织。又可细分为两类：
      - 周木型：木质部在外周，成圈状包围里面的韧皮部。
      - 周韧型：韧质部在外周，成圈状包围里面的木皮部。

此外，根据能否继续发育，还可以将维管束分为有限维管束和无限维管束两类：

  - 有限维管束：不具有[形成层](../Page/形成层.md "wikilink")，不能发育出新的木质部和韧皮部。蕨类植物和[单子叶植物的维管束多属于此类](../Page/单子叶植物.md "wikilink")。
  - 无限维管束：在木质部和韧皮部之间有形成层，可以产生新的木质部和韧皮部。这类维管束可以使植物的枝干不断加粗。一般存在于裸子植物和[双子叶植物中](../Page/双子叶植物.md "wikilink")。

由許多上下排的細胞所構成，並貫穿植物的根莖葉

## 发育

维管束由[顶端分生组织分化的](../Page/顶端分生组织.md "wikilink")[原形成层产生](../Page/形成层.md "wikilink")，属初生维管组织。

## 叶脉

在[叶中](../Page/叶.md "wikilink")，维管束又被称为**叶脉**。叶脉通常位于叶肉的[海绵组织中](../Page/海绵组织.md "wikilink")，其木质部朝向叶的正面，而韧皮部则朝向叶的背面。由于韧皮部在植物中是用于传输糖分等营养物质的，所以经常可以发现[蚜虫多聚集于叶子的背面而非正面](../Page/蚜虫.md "wikilink")。植物的葉脈通常有平行脈和網狀脈兩種形式。

## 参见

  - [维管植物](../Page/维管植物.md "wikilink")
  - [木质部](../Page/木质部.md "wikilink")
  - [韧皮部](../Page/韧皮部.md "wikilink")
  - [形成层](../Page/形成层.md "wikilink")
  - [分生组织](../Page/分生组织.md "wikilink")
  - [植物體無機鹽運送途徑](../Page/植物體無機鹽運送途徑.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - 维管束的切片照片：[1](http://botweb.uwsp.edu/anatomy/primaryxylem.htm%5D和%5Bhttp://www.sbs.utexas.edu/mauseth/weblab/webchap8phloem/chapter_8.htm)

[U](../Category/植物解剖學.md "wikilink")
[U](../Category/植物生理学.md "wikilink")
[U](../Category/組織_\(生物\).md "wikilink")

1.
2.