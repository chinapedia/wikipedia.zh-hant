[Osaka_Expo'70_Korean_Pavilion.jpg](https://zh.wikipedia.org/wiki/File:Osaka_Expo'70_Korean_Pavilion.jpg "fig:Osaka_Expo'70_Korean_Pavilion.jpg")館\]\]
[Osaka_Expo'70_Kodak+Ricoh_Pavilion.jpg](https://zh.wikipedia.org/wiki/File:Osaka_Expo'70_Kodak+Ricoh_Pavilion.jpg "fig:Osaka_Expo'70_Kodak+Ricoh_Pavilion.jpg")館
& [理光館](../Page/理光.md "wikilink")\]\]

**1970年世界博覽會**，正式名稱為**日本萬國博覽會**，簡稱**大阪萬博**、**Expo
'70**，1970年3月15日至9月13日在[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[吹田市的](../Page/吹田市.md "wikilink")[千里丘陵舉行](../Page/千里丘陵.md "wikilink")，為期183日，是日本首次主辦的[世界博覽會](../Page/世界博覽會.md "wikilink")。主辦單位為「日本萬國博覽會協會」（今「[獨立行政法人日本萬國博覽會紀念機構](../Page/獨立行政法人.md "wikilink")」）。

大阪万博的主题是「人类的进步和协调」，有77个[国家](../Page/国家.md "wikilink")、4个[国际组织参加](../Page/国际组织.md "wikilink")，會場占地約350[公頃](../Page/公頃.md "wikilink")，入场者多達6421万8770人次。会场由日本建築師[丹下健三设计](../Page/丹下健三.md "wikilink")。

## 展覽品

  - 作為題目館一部分的[太陽之塔](../Page/太陽之塔.md "wikilink")，由[岡本太郎的匠心建造](../Page/岡本太郎.md "wikilink")。現在也保存下來，作為[萬博紀念公園的標記](../Page/萬博紀念公園.md "wikilink")。
  - 美國館展出了[阿波羅計劃帶回來的](../Page/阿波羅計劃.md "wikilink")[月球石頭](../Page/月球.md "wikilink")，成為話題。（1969年，阿波羅11號土產）
  - 在[松下馆进行展示的](../Page/Panasonic.md "wikilink")[时间胶囊是将当时的各种物品合在一起](../Page/时间胶囊.md "wikilink")，分为相同的两个埋在[大阪城公园内](../Page/大阪城公园.md "wikilink")。其中一个预定在5000年后的6970年开封。另外一个为了确认内装物体的状态，预备在2000年以后每隔100年开封一次。2000年按照原计劃进行了第一次开封。
  - 澳洲館閉幕後，移師[三重縣](../Page/三重縣.md "wikilink")[四日市市重建](../Page/四日市市.md "wikilink")，成為澳大利亞紀念館。
  - 當時領土僅於[台澎金馬的](../Page/台澎金馬.md "wikilink")[中華民國也以正式國名參加](../Page/中華民國.md "wikilink")。中華民國館的建築由[貝聿銘設計](../Page/貝聿銘.md "wikilink")。

## 在萬博登場，其後普及的展品

  - [電動步道](../Page/電動步道.md "wikilink")
  - [鍋爐咖啡](../Page/鍋爐咖啡.md "wikilink")（[UCC上島咖啡](../Page/UCC上島咖啡.md "wikilink")）
  - Air
    Doom（；[美國館](../Page/美國.md "wikilink")、[芙蓉小組帳幕](../Page/芙蓉小組.md "wikilink")）
  - [連鎖快餐店](../Page/連鎖快餐店.md "wikilink")（[肯德基](../Page/肯德基.md "wikilink")、[ドムドムバーガー](../Page/ドムドムバーガー.md "wikilink")）
  - [咖啡味](../Page/咖啡.md "wikilink")[軟雪糕](../Page/軟雪糕.md "wikilink")（[埃塞俄比亞館](../Page/埃塞俄比亞.md "wikilink")）

## 在萬博登場，但并未普及的展品

  - [磁浮列車](../Page/磁浮列車.md "wikilink")（日本館、[日本國有鐵道](../Page/日本國有鐵道.md "wikilink")）
      -
        在日本仍由[JR東海測試開發中](../Page/JR東海.md "wikilink")，預定2020年代完工通車的[中央新幹線將為日本第一條磁浮列車路線](../Page/中央新幹線.md "wikilink")。至2013年為止，僅[上海的](../Page/上海.md "wikilink")[上海磁浮示範運營線為全世界唯一高速](../Page/上海磁浮示範運營線.md "wikilink")（250km/h以上）商轉營運的磁浮列車。
  - 空中餐室（會場各處）[1](http://www.bekkoame.ne.jp/~kakikuke/banpakusunac.html)
  - [傳真送信型](../Page/傳真.md "wikilink")[新聞](../Page/新聞.md "wikilink")（）
      -
        [全球資訊網普及後](../Page/全球資訊網.md "wikilink")，直接由新聞網站所取代。
  - [電視電話](../Page/電視電話.md "wikilink")
      -
        [網際網路普及後](../Page/網際網路.md "wikilink")，直接由[視訊會議及](../Page/視訊會議.md "wikilink")[Skype等軟體所取代](../Page/Skype.md "wikilink")
  - [電動汽車](../Page/電動汽車.md "wikilink")（[大發工業](../Page/大發工業.md "wikilink")）

## 出席人士

當時的[日本首相是長期執政的](../Page/日本首相.md "wikilink")[佐藤榮作](../Page/佐藤榮作.md "wikilink")（1964－1972年總理大臣）。萬國博覽會的博覽會秘書長[鈴木俊一](../Page/鈴木俊一.md "wikilink")（後來的[東京都知事](../Page/東京都知事.md "wikilink")）。為萬博會場提供綜合設計的是建築家[丹下健三](../Page/丹下健三.md "wikilink")（鈴木和丹下的關係延續至新[東京都廳舍](../Page/東京都廳舍.md "wikilink")）。

## 紀念發行物品

[Banpaku100.jpg](https://zh.wikipedia.org/wiki/File:Banpaku100.jpg "fig:Banpaku100.jpg")

  - 紀念[郵票](../Page/郵票.md "wikilink")
      - 7日圓、15日圓、50日圓三種，於1970年3月14日發行，1970年6月15日再度發行。
  - [紀念硬幣](../Page/紀念硬幣.md "wikilink")
      - 100日圓白銅幣，於1970年3月10日（7月9日追加發行）發行。

## 主題曲

  - 你好，世界各国()（作詞：岛田阳子、作曲：[中村八大](../Page/中村八大.md "wikilink")）

### 其他主題曲

  - 万国博音頭（作詞：三宅立美、補作詞：西沢爽、作曲：[古賀政男](../Page/古賀政男.md "wikilink")）

<!-- end list -->

  -
    1966年發售。歌手是[村田英雄](../Page/村田英雄.md "wikilink")，此曲不是競賽作品。

## 期間交通

[sharjah-expo70_stamp.jpg](https://zh.wikipedia.org/wiki/File:sharjah-expo70_stamp.jpg "fig:sharjah-expo70_stamp.jpg")

  - 鐵路
      - [北大阪急行電鐵](../Page/北大阪急行電鐵.md "wikilink")（）
      - [阪急千里線](../Page/阪急千里線.md "wikilink")
  - 巴士
      - [阪急巴士](../Page/阪急巴士.md "wikilink")
      - [近鐵巴士](../Page/近鐵巴士.md "wikilink")
      - [京阪巴士](../Page/京阪巴士.md "wikilink")（只在萬博期間服務）

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 40%" />
<col style="width: 30%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>前大会</strong>:<br />
<a href="../Page/蒙特利爾萬國博覽會.md" title="wikilink">蒙特利爾萬國博覽會</a></p></td>
<td style="text-align: center;"><p><a href="../Page/国際博覧会.md" title="wikilink">一般博</a><br />
<a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/大阪府.md" title="wikilink">大阪府</a></p></td>
<td style="text-align: center;"><p><strong>次大会</strong>:<br />
<a href="../Page/塞維利亞萬國博覽會.md" title="wikilink">塞維利亞萬國博覽會</a></p></td>
</tr>
</tbody>
</table>

## 参见

  - [國際博覽會](../Page/國際博覽會.md "wikilink")
  - [萬博紀念公園](../Page/萬博紀念公園.md "wikilink")
  - [河內家菊水丸](../Page/河內家菊水丸.md "wikilink")
  - [半田健人](../Page/半田健人.md "wikilink")
  - [2005年日本國際博覽會](../Page/2005年日本國際博覽會.md "wikilink")
  - [大阪博覽會](../Page/大阪博覽會.md "wikilink")（1903年舉辦）
  - [20世紀少年](../Page/20世紀少年.md "wikilink")
  - [蠟筆小新：風起雲湧 猛烈！大人帝國的反擊](../Page/蠟筆小新：風起雲湧_猛烈！大人帝國的反擊.md "wikilink")
  - [萬博紀念競技場](../Page/萬博紀念競技場.md "wikilink")
  - [太陽之塔](../Page/太陽之塔.md "wikilink")

## 外部連結

  - [日本萬國博覽會紀念機構](https://web.archive.org/web/20060207071527/http://www.expo70.or.jp/)
  - [獨立行政法人日本萬國博覽會紀念機構](https://web.archive.org/web/20060207071527/http://www.expo70.or.jp/)
  - [懐かしの大阪万博](http://www.expo70.jp/)
  - [スイタウェブ万博情報](http://suitaweb.net/expo/)
  - [追憶　日本萬國博覽會](https://web.archive.org/web/20060206140804/http://expo70-web.hp.infoseek.co.jp/)
  - [OSAKA BAN PACK\!](http://expo70.fc2web.com/)
  - [万博バンバン！日本万国博覧会](https://web.archive.org/web/20050527202533/http://the-mqr.hp.infoseek.co.jp/banpaku/expo70.htm)
  - [蛞蝓万博 EXPO
    '70](https://web.archive.org/web/20051225214532/http://odasan.s48.xrea.com/expo70/index.html)
  - [ノスタル爺劇場　まぼろしの万博](https://web.archive.org/web/20060211034020/http://www.maboroshi-ch.com/tre/cin_04.htm)
  - [萬博紀念館](http://www.bekkoame.ne.jp/~kakikuke/banpaku.html)
  - [EXPO
    '70設計展](https://web.archive.org/web/20060215012841/http://homepage1.nifty.com/kitaosaka/expo70.htm)
  - [万博（Expo'70）を見た乗物たち](http://www.bekkoame.ne.jp/~kakikuke/tecexpo70.html)
  - [「公式長編記録映画　日本万国博覧会」DVD　公式HP](https://web.archive.org/web/20060213052905/http://www.geneon-ent.co.jp/movie/topics/expo.html)
  - [公式記録映画
    日本万国博覧会](https://web.archive.org/web/20060110235641/http://expo70-web.hp.infoseek.co.jp/koushikikirokueiga.html)
  - [大阪万博　「サンヨー館の思い出」展](https://web.archive.org/web/20101005002928/http://sanyo.com/museum/jp/exhibition/expo70.html)
  - [江崎グリコ：タイムスリップグリコ　大阪万博編](http://www.butsuyoku.net/shokugan/expo/index.html)
  - [大阪万博パピリオン3DCG（壁紙使用可）](http://www.konoike.org/3d/index.html)
  - [失われた未来　LOST
    FUTURE 2000](https://web.archive.org/web/20060209091023/http://www.netcity.or.jp/OTAKU/okada/library/priodical/future/index.html)
  - [万博・地方博＠2ch掲示板](http://travel2.2ch.net/expo/)

[Category:世界博覽會](../Category/世界博覽會.md "wikilink")
[Category:1970年日本](../Category/1970年日本.md "wikilink")
[Category:大阪府歷史](../Category/大阪府歷史.md "wikilink")
[Category:吹田市](../Category/吹田市.md "wikilink")