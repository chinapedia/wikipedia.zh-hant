**艾爾頓·赫拉克勒斯·約翰爵士**，[CBE](../Page/大英帝国勋章.md "wikilink")（****，），[英國籍](../Page/英國.md "wikilink")[搖滾樂](../Page/搖滾樂.md "wikilink")[唱作人](../Page/唱作人.md "wikilink")、[作曲家](../Page/作曲家.md "wikilink")、[鋼琴家和](../Page/鋼琴家.md "wikilink")[演員](../Page/演員.md "wikilink")。自1967年以來，艾爾頓·約翰爵士與[作詞人伯尼](../Page/作詞人.md "wikilink")·陶平（）成為創作夥伴，迄今二人已經合作完成三十餘張專輯。

在艾爾頓·約翰四十年的音樂生涯中，他共計售出超過二億五千萬張唱片，使得他成為“史上最成功的藝人”之一。\[1\]
他的[單曲](../Page/單曲.md "wikilink")《[風中之燭（1997）](../Page/風中之燭.md "wikilink")》在全球已成功售出3300萬張，成為“[告示牌百强单曲榜](../Page/告示牌百强单曲榜.md "wikilink")”歷史上最暢銷的單曲。\[2\]
在艾爾頓·約翰爵士的音樂歷史中，共計有超過50首歌曲進入各大主流電臺的TOP40單曲，其中還有一張在美國連續七年蟬聯榜首的專輯。而在“[告示牌百强单曲榜](../Page/告示牌百强单曲榜.md "wikilink")”的歷史中，艾爾頓·約翰爵士共有五十六首歌曲進入前四十；十六首進入前十；四首亞軍單曲和九首冠軍單曲。並且他還贏得了六座[格萊美獎盃](../Page/格萊美.md "wikilink")、四座[全英音樂獎獎盃](../Page/全英音樂獎.md "wikilink")、一座[奧斯卡獎盃](../Page/奧斯卡.md "wikilink")、一座[金球獎獎盃和一座](../Page/金球獎.md "wikilink")[托尼獎獎盃](../Page/托尼獎.md "wikilink")。在2004年，《[滾石](../Page/滾石_\(雜誌\).md "wikilink")》將艾爾頓·約翰爵士列為“史上最偉大的百位藝人”中的第49位。\[3\]

艾爾頓·約翰爵士在1994年進入[摇滚名人堂](../Page/摇滚名人堂.md "wikilink")，\[4\]
在1996年獲得[大英帝國司令勳章](../Page/大英帝國勳章.md "wikilink")，而在1998年又因“對音樂和慈善的突出貢獻”獲得[英國女王](../Page/英國女王.md "wikilink")[伊莉莎白二世頒發的](../Page/伊莉莎白二世.md "wikilink")[皇家嘉許](../Page/英國授勳及嘉獎制度.md "wikilink")。\[5\]
艾爾頓·約翰爵士還曾參與過一些英國王室演出，例如：1997年在[西敏寺的](../Page/西敏寺.md "wikilink")[戴安娜王妃葬禮和](../Page/戴安娜王妃.md "wikilink")2012年在[白金漢宮的伊莉莎白二世](../Page/白金漢宮.md "wikilink")“鑽禧慶典音樂會”。
\[6\] \[7\]

自20世紀80年代後期以來，艾爾頓·約翰爵士致力於抗擊[艾滋病的公益活動](../Page/艾滋病.md "wikilink")，\[8\]
并在1992年成立了“[艾爾頓·約翰艾滋病基金會](../Page/艾爾頓·約翰艾滋病基金會.md "wikilink")”。從1993年開始，該基金會會舉辦年度“奧斯卡獎派對”，這已經成為[好萊塢上極為矚目的活動](../Page/好萊塢.md "wikilink")。自成立以來，“艾爾頓·約翰艾滋病基金會”迄今已經募集了兩億美元的善款。\[9\]

2005年12月21日，艾爾頓·約翰爵士與[電影製片人大衛](../Page/電影製片人.md "wikilink")·弗尼西（）結成[民事伴侶](../Page/英國民事伴侶關係.md "wikilink")，\[10\]
這被視為LGBT社會運動的一個標誌。

2008年，《[告示牌](../Page/告示牌_\(雜誌\).md "wikilink")》將艾爾頓·約翰爵士評為“最成功的獨唱男歌手”而列入“告示牌百強單曲榜之史上最成功藝人榜”。\[11\]
在約翰爵士之前，只有[披頭四樂隊和](../Page/披頭四樂隊.md "wikilink")[麥當娜入選](../Page/麥當娜.md "wikilink")。

2018年，艾爾頓·約翰爵士宣布為了多陪家人，將於年底舉辦為期三年，至2021年，共300場的世界告別巡迴演唱。未來將不再巡迴表演，但仍會繼續從事音樂創作。

## 職業生涯

### 早年生活

艾爾頓·約翰爵士本名雷金納德·肯尼思·德懷特（），1947年3月25日出生在英國米德爾塞克斯郡皮納縣他外祖父母的房子里。他是斯坦利·德懷特的長子，同時也是希拉·艾琳·哈里斯的獨生子。\[12\]\[13\]\[14\]
他父母生他的時候並沒有結婚，直到他6歲的時候，他們才從約翰爵士的外祖父母家中搬出到一間半獨立的寓所內。\[15\]\[16\]\[17\]
約翰爵士在皮納縣伍德小學、雷迪福德中學和皮納縣[預科學校接受教育](../Page/預科.md "wikilink")，但是他在[高考前選擇退學去追尋他的音樂夢想](../Page/英國普通教育高級程度證書.md "wikilink")。\[18\]\[19\]\[20\]

### 乐团时代

### 七零年代前期

### 七零年代后期

### 八零年代

### 九零年代

### 新世纪

## 個人風格

### 創作風格

[Elton_John_Bernie_Taupin_1971.JPG](https://zh.wikipedia.org/wiki/File:Elton_John_Bernie_Taupin_1971.JPG "fig:Elton_John_Bernie_Taupin_1971.JPG")

艾爾頓·約翰爵士與他的專屬填詞人伯尼·陶平自1967年便開始了合作，自由唱片公司藝人與曲目部負責人雷·威廉姆斯（）在《[新音乐快递](../Page/新音乐快递.md "wikilink")》（**）上讚譽他倆是英國流行音樂界最天才的組合。\[21\]
他們迄今已合作完成超過30張專輯。\[22\]

在1991年的電影紀錄片《兩間房》（**）-{zh-hans:里;zh-hant:裡;}-，講述了兩人的合作風格。陶平在一間房里寫下他的歌詞，然後約翰爵士在另一間房裡為歌詞譜曲。這個過程中，兩人幾乎不會在同一個房間里會面。有時陶平也會將歌詞寄送給正忙於全球巡演的約翰爵士，而約翰爵士只要接到歌詞，無論多忙碌都會第一時間為這首歌詞進行譜曲工作。<ref>Syd
Field

`(2008). `[`"The``   ``Definitive``   ``Guide``   ``To``
 ``Screenwriting"`](http://books.google.com/books?id=PlQxwXiBniYC&pg=PA351&dq=elton+john+and+bernie+taupin+-+never+collaborated+in+same+room&hl=en&sa=X&ei=fPL6Toj_M5CDhQeT7-3yAQ&ved=0CFcQ6AEwCA#v=onepage&q=elton%20john%20and%20bernie%20taupin%20-%20never%20collaborated%20in%20same%20room&f=false)`. 351頁. `[`蘭登書屋`](../Page/蘭登書屋.md "wikilink")`, 2008`</ref>` 約翰爵士英國唱作人、作曲家和作詞家學會（``）的成員。`\[23\]

### 演唱風格

一度被歸為[男高音的艾爾頓](../Page/男高音.md "wikilink")·約翰爵士目前是一名[男中音歌手](../Page/男中音.md "wikilink")。\[24\]
他的鋼琴演奏風格深受[古典音樂和](../Page/古典音樂.md "wikilink")[福音音樂的影響](../Page/福音音樂.md "wikilink")。\[25\]
20世紀70年代，約翰爵士的[錄音室專輯大多由保羅](../Page/錄音室專輯.md "wikilink")·巴克馬斯特（）進行[編曲](../Page/編曲.md "wikilink")。\[26\]

## 私人生活

### 婚姻家庭

20世紀60年代末期，艾爾頓·約翰爵士曾經打算與他的初戀情人兼[秘書琳達](../Page/秘書.md "wikilink")·伍德羅（）結婚，他還將這段回憶寫入到他的歌曲《》中。\[27\]\[28\]
1984年2月14日，艾爾頓·約翰爵士在[悉尼迎娶了他的首任妻子](../Page/悉尼.md "wikilink")，[德國錄音師雷娜特](../Page/德國.md "wikilink")·布勞爾（），但是很快這段婚姻因為約翰爵士的[性取向而出現問題](../Page/性取向.md "wikilink")。在1976年接受《[滾石](../Page/滾石_\(雜誌\).md "wikilink")》的一次採訪中，約翰爵士承認自己是一名“[雙性戀](../Page/雙性戀.md "wikilink")”，\[29\]\[30\]
但他隨後還是與雷娜特結婚并直到1988年才離婚。他在離婚後接受雜誌訪問的時候說起，他覺得當一名[同性戀會](../Page/同性戀.md "wikilink")“更舒服”，因而之後被稱爲「樂壇大姑媽」。\[31\]

1993年，艾爾頓·約翰爵士與大衛·弗尼西陷入愛河幷於2005年12月21日結成[民事伴侶](../Page/英國民事伴侶關係.md "wikilink")。他們在[溫莎市市政廳低調登記](../Page/溫莎市.md "wikilink")，但隨後卻在他們位於伯克希爾的豪宅里舉行了一場奢華派對，\[32\]
據傳他們為此次派對支付了100萬[英鎊](../Page/英鎊.md "wikilink")。\[33\]
通過一位[代孕母親](../Page/代孕.md "wikilink")，約翰爵士與弗尼西的兒子，扎克里·傑克遜·利翁·弗尼西-約翰于在[加州](../Page/加州.md "wikilink")2010年12月25日出生。\[34\]\[35\]
約翰與弗尼西讓、雜誌編輯英格麗·斯西及斯西的伴侶桑迪·勃蘭特成為扎克里·弗尼西-約翰的教母。\[36\]

2009年9月，約翰爵士曾經宣佈他打算從[烏克蘭的一個艾滋病孤兒院里收養一個](../Page/烏克蘭.md "wikilink")14個月大的嬰兒，利夫。但是由於約翰爵士的年齡和婚姻狀況，他的收養請求被拒絕了。\[37\]
弗尼西表示，他們將繼續在資金上對利夫予以幫助，同時他們也打算資助一些政治團體參與競選以求改變烏克蘭的收養法規。\[38\]
約翰爵士擁有十個著名的[教子](../Page/教子.md "wikilink")：[約翰·列儂的兒子肖恩](../Page/約翰·列儂.md "wikilink")·列儂、[大衛·貝克漢姆和](../Page/大衛·貝克漢姆.md "wikilink")[維多利亞·貝克漢姆夫婦的兒子布魯克林及羅密歐](../Page/維多利亞·貝克漢姆.md "wikilink")、[伊麗莎白·赫莉的兒子達米恩](../Page/伊麗莎白·赫莉.md "wikilink")·查理斯，以及西摩·斯坦（）的女兒。\[39\]\[40\]\[41\]

### 个人资产

[Elton_John_on_stage,_2008.jpg](https://zh.wikipedia.org/wiki/File:Elton_John_on_stage,_2008.jpg "fig:Elton_John_on_stage,_2008.jpg")

2009年4月，艾爾頓·約翰爵士以個人資產1.75億英鎊（約合2.65億美元）榮登“《[星期日泰晤士報](../Page/星期日泰晤士報.md "wikilink")》富人排行榜”，排名英國最富裕人士中的第322位。\[42\]
2011年，約翰爵士再度以1.95億英鎊的身家入榜，成為英國音樂界十位最富裕的人士之一。\[43\]
除了在伯尼希爾老溫莎的住宅外，約翰爵士還擁有在[亞特蘭大](../Page/亞特蘭大.md "wikilink")、[尼斯](../Page/尼斯.md "wikilink")、[倫敦荷蘭公園和](../Page/倫敦.md "wikilink")[威尼斯的房產](../Page/威尼斯.md "wikilink")。同時，艾爾頓·約翰爵士也是一名[收藏家](../Page/收藏家.md "wikilink")，他被認為是全球藏品最豐富的個人[攝影作品收藏家](../Page/攝影.md "wikilink")。\[44\]

在2000年，艾爾頓·約翰爵士承認他在兩年內以平均每月150萬英鎊的速度揮霍了3000萬英鎊。在1996年1月到1997年7月期間，約翰爵士在物業上花費了960萬英鎊，而在鮮花採購上花費了29.3萬英鎊。\[45\]
2001年6月，約翰爵士通過[佳士得出售他的](../Page/佳士得.md "wikilink")20輛愛車，他聲稱由於自己經常出國而無法使用它們。\[46\]
約翰爵士拍賣的汽車包括：一輛單價價值234,750英鎊的[捷豹XJ](../Page/捷豹.md "wikilink")220型跑車；另外還包括[法拉利](../Page/法拉利.md "wikilink")、[勞斯萊斯](../Page/勞斯萊斯.md "wikilink")、[賓利等品牌的跑車](../Page/賓利.md "wikilink")，折合價值近200萬英鎊。\[47\]
2003年，約翰爵士通過[蘇富比拍賣其在荷蘭花園的住所](../Page/蘇富比.md "wikilink")，預估可以收益80萬英鎊。約翰爵士打算用這筆資金擴充其在英國年輕藝術家作品的收藏，這其中包括薩姆·泰勒-伍德（）、翠西·艾敏（）等人的作品。\[48\]
2004年，約翰爵士開設了一家名為“約翰衣櫥”的商店，用於出售他的二手衣服。\[49\]

### 其他事迹

艾爾頓·約翰爵士在他的職業生涯里，一度因[酒精和](../Page/酒精.md "wikilink")[可卡因而](../Page/可卡因.md "wikilink")[上癮](../Page/上癮.md "wikilink")。到了1975年，明星的身份開始給他帶來壓力。在那一年的洛杉磯“約翰音樂周”中，他因[吸毒過量而入院治療](../Page/吸毒過量.md "wikilink")。\[50\]
同時艾爾頓·約翰爵士還罹患[神經性暴食症](../Page/神經性暴食症.md "wikilink")。在2002年接受[拉里·金的採訪中](../Page/拉里·金.md "wikilink")，金詢問他是否知道[戴安娜王妃患有飲食失調的毛病](../Page/戴安娜王妃.md "wikilink")。約翰爵士回答道：“是的，我知道。我們都有[神經性暴食症](../Page/神經性暴食症.md "wikilink")。”\[51\]

艾爾頓·約翰爵士是一名[網球愛好者](../Page/網球.md "wikilink")，他曾經為自己的好友兼世界著名網球選手[比莉·珍·金](../Page/比莉·珍·金.md "wikilink")（）撰寫了一首歌曲名為《自由的費城》（）。約翰爵士與金還會每年舉辦一次為艾滋病募款的慈善網球賽，他們會組成一對職業與業餘混合選手。同時比莉·珍·金還是“[艾爾頓·約翰艾滋病基金會](../Page/艾爾頓·約翰艾滋病基金會.md "wikilink")”的副主席。約翰爵士的球技是由唐納德·懷特（）指導，他是一名活躍在20世紀80年代到90年代的網球教練。\[52\]
約翰爵士在1991年搬家到[佐治亞州](../Page/佐治亞州.md "wikilink")[亞特蘭大市之後](../Page/亞特蘭大市.md "wikilink")，成為了[美國職棒大聯盟中](../Page/美國職棒大聯盟.md "wikilink")[亞特蘭大勇士隊的球迷](../Page/亞特蘭大勇士隊.md "wikilink")。\[53\]

## 興趣主張

### 樂隊

自20世紀70年代開始，艾爾頓·約翰爵士就組成了屬於他自己的樂隊，他在樂隊中擔任主唱與鋼琴演奏，這支樂隊很快就被冠以“艾爾頓·約翰樂隊（）”的名號而享譽英倫。\[54\]\[55\]

儘管樂隊成員在不斷變換，但是奈傑爾·奧爾森（）、戴維·約翰斯通（）和雷·庫珀（）一直是樂隊的成員之一。奧爾森是1970年加入樂隊，而約翰斯通和庫珀則是在1972年加入樂隊。除了樂隊成員之外，約翰爵士在他的職業生涯里也與許多職業樂手合作過。例如：[蒂姆·赖斯](../Page/蒂姆·赖斯.md "wikilink")、[約翰·列儂](../Page/約翰·列儂.md "wikilink")、[小野洋子](../Page/小野洋子.md "wikilink")、[比利·乔尔](../Page/比利·乔尔.md "wikilink")、[乔治·迈克尔](../Page/乔治·迈克尔.md "wikilink")、、[史提夫·汪达和](../Page/史提夫·汪达.md "wikilink")[尼尔·萨达卡等](../Page/尼尔·萨达卡.md "wikilink")。

現今成員:

  - Elton John – lead vocals, piano
  - [Nigel Olsson](../Page/Nigel_Olsson.md "wikilink") – drums, vocals
  - [Davey Johnstone](../Page/Davey_Johnstone.md "wikilink") – guitar,
    musical director, vocals
  - [John Mahon](../Page/John_Mahon.md "wikilink") – percussion, vocals
  - [Kim Bullard](../Page/Kim_Bullard.md "wikilink") – keyboards
  - [Matt Bissonette](../Page/Matt_Bissonette.md "wikilink") – bass
  - [Ray Cooper](../Page/Ray_Cooper.md "wikilink") – percussion

### 足球

艾爾頓·約翰爵士在1976年成為英格蘭球隊[屈福特的主席和運營總監](../Page/沃特福德足球俱乐部.md "wikilink")，并任命[格拉咸·泰萊為球隊經理](../Page/格拉咸·泰萊.md "wikilink")（即教練）。在[格拉咸·泰萊的帶領和約翰爵士的大筆資金資助下](../Page/格拉咸·泰萊.md "wikilink")，[沃特福德足球俱乐部從當時的英格蘭丁級聯賽](../Page/沃特福德足球俱乐部.md "wikilink")（當時聯賽的第三級）升入到英格蘭甲級聯賽（當時聯賽的第二級）。\[56\]
球隊隨後達到球隊歷史上的巔峰，即1983年升入英格蘭甲級聯賽當年就拿下聯賽亞軍（當年聯賽冠軍為[利物浦](../Page/利物浦足球俱樂部.md "wikilink")）并在次年入圍[足總杯總決賽](../Page/足總杯.md "wikilink")（當年足總杯冠軍為[埃弗頓](../Page/埃弗頓足球俱樂部.md "wikilink")）。

1987年的時候，約翰爵士將俱樂部出售給傑克·柏奇利（），同時出任俱樂部的終身名譽會長。\[57\]

1997年，約翰爵士再度入主[沃特福德足球俱乐部](../Page/沃特福德足球俱乐部.md "wikilink")，重新從傑克·柏奇利手中購回俱樂部。在2002年的時候，由於球隊需要一個全職的俱樂部主席負責俱樂部事宜，因此約翰爵士請辭球隊主席而擔任名譽會長。\[58\]

雖然約翰爵士現在不再是球隊的大股東，但是任積極為球隊籌措資金。2005年6月，約翰爵士在俱樂部主場[維卡拉格路體育場舉辦演唱會](../Page/維卡拉格路體育場.md "wikilink")，為球隊募集資金。後來他又在2010年5月的一場演唱會上為球隊籌集資金。\[59\]

### 慈善

由於好友[瑞安·怀特和](../Page/瑞安·怀特.md "wikilink")[弗雷迪·默丘里的去世](../Page/弗雷迪·默丘里.md "wikilink")，艾爾頓·約翰爵士一直致力於與[艾滋病機構的合作](../Page/艾滋病.md "wikilink")。他為許多艾滋病機構募集善款并利用他的公共形象進行宣傳，以促使公眾更多地瞭解這種疾病。例如：1986年，約翰爵士聯合狄翁·華薇克（）、[葛蕾蒂絲·奈特和](../Page/葛蕾蒂絲·奈特.md "wikilink")[史提夫·汪达錄製了一首](../Page/史提夫·汪达.md "wikilink")[單曲](../Page/單曲.md "wikilink")，名為《這才是朋友》（**），約翰爵士等人將這首歌的全部收益捐贈給了美國艾滋病研究基金會（）。同時這首單曲榮膺當年的格萊美最佳組合及樂隊獎并格萊美年度最佳歌曲獎。1990年4月，在[瑞安·怀特的葬禮上](../Page/瑞安·怀特.md "wikilink")，艾爾頓·爵士為這個罹患血友病并在輸血治療中不幸感染艾滋病的少年演奏了《藍天白鴿》（**）。

約翰爵士在1992年成立了“[艾爾頓·約翰艾滋病基金會](../Page/艾爾頓·約翰艾滋病基金會.md "wikilink")”，這是一個旨在資助艾滋病研究機構、消除艾滋病歧視和干預艾滋病高危人群的慈善機構。2006年，約翰爵士拍賣了兩架他在[拉斯維加斯演出所用的紅色](../Page/拉斯維加斯.md "wikilink")[雅馬哈](../Page/雅馬哈.md "wikilink")[鋼琴](../Page/鋼琴.md "wikilink")，他希望借此喚起民眾對艾滋病的認知并為基金會募集善款。

為了募集更多的善款，艾爾頓·約翰爵士自1997年開始舉辦“”慈善晚會，旨在向社會名流募集捐款。2007年6月28日，在第九屆“”慈善晚會上，一輛[勞斯萊斯幻影敞篷跑車和一副翠西](../Page/勞斯萊斯.md "wikilink")·艾敏的作品都拍出了80萬英鎊的高價。這次慈善晚會共計為基金會募得善款3500萬英鎊。\[60\]
晚會門票的售價為1000英鎊，這項收入在2006年為他的基金會帶來460萬英鎊的收入。\[61\]

### 其他

2010年4月1日，艾爾頓·約翰爵士加入了[辛蒂·羅波的](../Page/辛蒂·羅波.md "wikilink")“我在乎運動”（）。這是一個旨在消除公眾對[LGBT社群歧視的社會運動](../Page/LGBT.md "wikilink")，同時也是[辛蒂·羅波本色基金會](../Page/辛蒂·羅波.md "wikilink")（True
Colors Fund）項目的一部份。\[62\]
約翰在這個項目的推廣廣告中說道：“想像一下，僅僅因為你是你自己而會在街上行走的時候被毆打，甚至被殺害。”\[63\]
這項活動讓異性戀者進入到LGBT社群，并消除他們對LGBT人群的歧視。這項運動的參與者還包括：[乌比·戈德堡](../Page/乌比·戈德堡.md "wikilink")、[傑森·瑪耶茲](../Page/傑森·瑪耶茲.md "wikilink")、朱迪絲·萊特（）、[辛西雅·尼克森](../Page/辛西雅·尼克森.md "wikilink")、[金·卡戴珊](../Page/金·卡戴珊.md "wikilink")、[克萊·艾肯](../Page/克萊·艾肯.md "wikilink")、莎朗·奧斯朋（）、凱利·奧斯朋（）和[安娜·派昆](../Page/安娜·派昆.md "wikilink")。\[64\]

約翰爵士也會偶爾在《[衛報](../Page/衛報.md "wikilink")》上擔任[專欄作家](../Page/專欄作家.md "wikilink")。\[65\]

## 榮譽獎項

艾爾頓·約翰爵士在1994年入選[搖滾名人堂](../Page/搖滾名人堂.md "wikilink")，而在1992年的時候，他已經與伯尼·陶平入選創作名人堂（）。1995年，約翰爵士獲得[大英帝國司令勳章](../Page/大英帝國勳章.md "wikilink")。\[66\]
1998年2月24日，約翰爵士由於“他對慈善工作的傑出貢獻”而被[伊莉莎白二世](../Page/伊莉莎白二世.md "wikilink")[授勳](../Page/下級勳位爵士.md "wikilink")。而早在1975年10月，約翰爵士就成為第1662名入選[好萊塢星光大道的演藝圈人士](../Page/好萊塢星光大道.md "wikilink")。\[67\]

2004年，約翰爵士榮獲[肯尼迪中心榮譽獎章](../Page/肯尼迪中心荣誉奖.md "wikilink")；2006年，榮獲[迪士尼傳奇大獎](../Page/迪士尼傳奇.md "wikilink")（）；2010年，榮獲英國音樂版權協會（）音樂遺產獎（）。\[68\]

1994年，艾爾頓·約翰爵士因《[獅子王](../Page/獅子王.md "wikilink")》主題曲《[今夜你可曾感受到愛](../Page/今夜感覺我的愛.md "wikilink")》（）獲得[奧斯卡獎最佳原創歌曲和](../Page/奧斯卡獎.md "wikilink")[金球獎最佳原創歌曲](../Page/金球獎_\(影視獎項\).md "wikilink")；2000年，約翰爵士因[音樂劇](../Page/音樂劇.md "wikilink")《阿依達》而獲得[托尼獎最佳原創配樂](../Page/托尼獎.md "wikilink")。

艾爾頓·約翰爵士擁有六座[格萊美獎盃](../Page/格萊美.md "wikilink")：

1.  1987年，因歌曲《這才是朋友》》（）而獲得格萊美最佳組合及團隊獎；
2.  1991年，因歌曲《巴斯克人》（）而獲得格萊美最佳作曲；
3.  1994年，因歌曲《今夜你可曾感受到愛》（）而獲得格萊美最佳流行表演；
4.  1997年，因歌曲《[風中之燭](../Page/風中之燭.md "wikilink")》而再獲格萊美最佳流行表演；
5.  1999年，獲得格萊美傳奇大獎；
6.  2001年，因音樂劇《阿依達》獲得格萊美最佳音樂劇專輯。

## 個人作品

### 錄音室專輯

1.  Empty Sky （1969）
2.  Elton John （1970）
3.  Tumbleweed Connection （1970）
4.  Madman Across the Water （1971）
5.  Honky Château （1972）
6.  Don't Shoot Me I'm Only the Piano Player （1973）
7.  Goodbye Yellow Brick Road （1973）
8.  Caribou （1974）
9.  Captain Fantastic and the Brown Dirt Cowboy （1975）
10. Rock of the Westies （1975）
11. Blue Moves （1976）
12. A Single Man （1978）
13. Victim of Love （1979）
14. 21 at 33 （1980）
15. The Fox （1981）
16. Jump Up\! （1982）
17. Too Low for Zero （1983）
18. Breaking Hearts （1984）
19. Ice on Fire （1985）
20. Leather Jackets （1986）
21. Reg Strikes Back （1988）
22. Sleeping with the Past （1989）
23. The One （1992）
24. Duets （1993）
25. Made in England （1995）
26. The Big Picture （1997）
27. Songs from the West Coast （2001）
28. Peachtree Road （2004）
29. The Captain & the Kid （2006）
30. The Diving Board （2013）
31. Wonderful Crazy Night （2016）

### 合輯

1.  Live in Australia with the Melbourne Symphony Orchestra (1986)
2.  Duets (1993)
3.  The Union with Leon Russell (2010)
4.  Good Morning to the Night with Pnau (2012)

### 精選輯

1.  Elton John's Greatest Hits (1974)
2.  Elton John's Greatest Hits Volume II （1977）
3.  Lady Samantha （1980）
4.  The Very Best of Elton John（1980）
5.  Love Songs （1982）
6.  The Superior Sound of Elton John (1970–1975) （1983）
7.  Your Songs（1985）
8.  Elton John's Greatest Hits Volume III （1987）
9.  The Very Best of Elton John （1990）
10. To Be Continued... （1990）
11. Rare Masters （1992）
12. Greatest Hits 1976–1986（1992）
13. Chartbusters Go Pop （1994）
14. Love Songs （1995）
15. Greatest Hits 1970–2002 （2002）
16. Rocket Man: The Definitive Hits （2007）
17. Diamonds（2017）

### 原聲帶及歌舞劇配曲

1.  《好朋友电影原声带》 （1971）
2.  《[獅子王電影原聲帶](../Page/獅子王.md "wikilink")》 （1994）
3.  《阿依達歌劇配樂輯》 （1998）
4.  《第六感女神電影原聲帶》（1999）
5.  《勇闖黃金城電影原聲帶》 （2000）
6.  《[跳出我天地電影原聲帶](../Page/跳出我天地.md "wikilink")》 （2000）
7.  《吸血鬼萊斯特歌劇配樂輯》（2005）
8.  《[傻密歐與茱麗葉電影原聲帶](../Page/傻密歐與茱麗葉.md "wikilink")》 （2011）

### 致敬專輯

1.  [Two Rooms: Celebrating the Songs of Elton John & Bernie
    Taupin](../Page/Two_Rooms:_Celebrating_the_Songs_of_Elton_John_&_Bernie_Taupin.md "wikilink")
    （1991）
2.  [Revamp: Reimagining the Songs of Elton John & Bernie
    Taupin](../Page/Revamp:_Reimagining_the_Songs_of_Elton_John_&_Bernie_Taupin.md "wikilink")（2018）
3.  [Restoration: Reimagining the Songs of Elton John and Bernie
    Taupin](../Page/Restoration:_Reimagining_the_Songs_of_Elton_John_and_Bernie_Taupin.md "wikilink")
    （2018）

### 電影作品

1.  1972年，《生来不羁》（），饰演他自己。
2.  1975年，《冲破黑暗谷》（），饰演弹球巫师（）。
3.  1997年，《天旋地转》（），饰演他自己。
4.  2002年，《草地英熊》（），饰演他自己。
5.  2007年，《艾尔顿·约翰传》（），个人传记片。
6.  2017年，《[金牌特務：機密對決](../Page/金牌特務：機密對決.md "wikilink")》，飾演他自己

## 參考文獻

### 相關傳記

  - Goodall, Nigel. *Elton John: A Visual Documentary*, Omnibus Press,
    1993. ISBN 978-0-7119-3078-0
  - Rosenthal, Elizabeth. *His Song: The Musical Journey of Elton John*,
    Billboard Books, 2001. ISBN 978-0-8230-8892-8

## 外部連結

  - [艾爾頓·約翰爵士的官方網站](http://www.eltonjohn.com/)

  -
  -
  - 艾爾頓·約翰爵士在《[衛報](../Page/衛報.md "wikilink")》[專欄](http://www.guardian.co.uk/profile/eltonjohn)

[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")
[Category:艾弗·诺韦洛奖获得者](../Category/艾弗·诺韦洛奖获得者.md "wikilink")
[Category:奥斯卡最佳歌曲奖获奖作曲家](../Category/奥斯卡最佳歌曲奖获奖作曲家.md "wikilink")
[Category:英国钢琴家](../Category/英国钢琴家.md "wikilink")
[Category:英国音乐制作人](../Category/英国音乐制作人.md "wikilink")
[Category:英國搖滾歌手](../Category/英國搖滾歌手.md "wikilink")
[Category:英格蘭男歌手](../Category/英格蘭男歌手.md "wikilink")
[Category:英格蘭創作歌手](../Category/英格蘭創作歌手.md "wikilink")
[Category:英格蘭節奏藍調歌手](../Category/英格蘭節奏藍調歌手.md "wikilink")
[Category:英格蘭吉他手](../Category/英格蘭吉他手.md "wikilink")
[Category:英格蘭鍵盤手](../Category/英格蘭鍵盤手.md "wikilink")
[Category:英國足球球會班主](../Category/英國足球球會班主.md "wikilink")
[Category:雙性戀歌手](../Category/雙性戀歌手.md "wikilink")
[Category:雙性戀演員](../Category/雙性戀演員.md "wikilink")
[Category:英格蘭LGBT人物](../Category/英格蘭LGBT人物.md "wikilink")
[Category:英國LGBT音樂家](../Category/英國LGBT音樂家.md "wikilink")
[Category:英國LGBT演員](../Category/英國LGBT演員.md "wikilink")
[Category:英國LGBT權利運動家](../Category/英國LGBT權利運動家.md "wikilink")
[Category:LGBT作曲家](../Category/LGBT作曲家.md "wikilink")
[Category:摇滚名人堂入选者](../Category/摇滚名人堂入选者.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12. ["'I Have So Much More To
    Do'"](http://www.parade.com/celebrity/2010/02/elton-john.html).
    *Parade*. 2010-02-21. 2010-08-31查閱.

13. ["Elton John Rock's Captain
    Fantastic"](http://www.time.com/time/magazine/article/0,9171,913239,00.html).
    1975-07-07. 2010-08-31查閱.

14. ["Sir Elton John's Hollywood
    Pad"](http://omg.yahoo.com/blogs/pepsi/sir-elton-johns-hollywood-pad/45)
    . *omg\!* 2009-12-21. 2010-08-31查閱.

15.

16.

17.

18. Elton John, [Philip
    Norman](../Page/Philip_Norman_\(author\).md "wikilink"), Fireside,
    1991

19. His Song: The Musical Journey of Elton John, Elizabeth Rosenthal,
    Billboard Books, 2001

20.

21. [Billboard 4 Oct 1997, 30 Years of Music: Elton John with Bernie
    Taupin](http://books.google.co.uk/books?id=1wkEAAAAMBAJ&pg=PA56&dq=bernie+taupin+-+london&hl=en&ei=7-0UTZT2JsHDhAf-6523Dg&sa=X&oi=book_result&ct=result&redir_esc=y#v=onepage&q=bernie%20taupin%20-%20london&f=false)
    《[告示牌](../Page/告示牌_\(雜誌\).md "wikilink")》. 2011-12-28查閱

22.

23. [`"Fellows``   ``-``   ``The``   ``British``   ``Academy``   ``of``
     ``Songwriters,``   ``Composers``   ``and``
     ``Authors"`](http://www.basca.org.uk/about-us/people/fellows/)`.``
     ``BASCA.``   ``2012-05-30查閱`

24.
25. Claude Bernardin, Tom Stanton (1996) [Rocket man: Elton John from
    A-Z](http://books.google.com/books?id=udJV_IQ86GgC&pg=PA18&dq=elton+john+influenced+by+classical+music&hl=en&ei=oczJTNS9GcyTjAew5qzuDw&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDYQ6AEwAQ#v=onepage&q=elton%20john%20influenced%20by%20classical%20music&f=false)
    Greenwood Publishing Group, 1996

26.

27.

28.

29.
30.
31. Mary Rhiel, David Bruce Suchoff,

32.

33.

34.

35. Levy, Glen [Welcome, Tiny Dancer: Elton John Becomes a
    Dad](http://newsfeed.time.com/2010/12/28/welcome-tiny-dancer-elton-john-becomes-a-dad/)
    [時代週刊](../Page/時代週刊.md "wikilink") 2010-12-28查閱

36.

37.

38.

39. Holly George-Warren *The Rolling stone encyclopedia of rock & roll*
    p.501. Fireside, 2001

40. Richard Simpson [Sir Elton to become godfather to Liz's
    Damian](http://www.dailymail.co.uk/tvshowbiz/article-109035/Sir-Elton-godfather-Lizs-Damian.html)
    [每日郵報](../Page/每日郵報.md "wikilink") 2010-12-28查閱

41. [Brooklyn to get Elton's
    millions](http://www.thisislondon.co.uk/showbiz/article-13535150-brooklyn-to-get-eltons-millions.do)
    倫敦標準晚報 2010-12-28查閱

42.

43.

44.

45.

46.

47.

48. [Sir Elton to sell home's
    contents](http://news.bbc.co.uk/1/hi/entertainment/3104246.stm)
    [BBC](../Page/BBC.md "wikilink") (2003-09-13)

49.

50. "Elton John", Biography Channel, 2005

51.

52.

53.

54. The Rolling Stone Encyclopedia of Rock & Roll. p.501. Fireside, 2001

55. Mike Clifford, Pete Frame (1992). The Harmony Illustrated
    Encyclopedia of Rock. p. 88. Harmony Books, 1992

56.

57.
58.
59. [Elton John gig boost for
    Watford](http://news.bbc.co.uk/sport1/hi/football/teams/w/watford/8710452.stm)
    [英國廣播公司](../Page/英國廣播公司.md "wikilink") 2010-12-28查閱

60.

61.

62. [True Blood star Anna Paquin reveals she's
    bisexual](http://www.dailymail.co.uk/tvshowbiz/article-1262918/True-Blood-star-Anna-Paquin-reveals-shes-bisexual.html)
    [每日郵報](../Page/每日郵報.md "wikilink") 2010-12-28查閱.

63.
64.
65.

66.

67. [Elton John receiving his star on the Hollywood Walk of
    Fame, 1975](http://unitproj.library.ucla.edu/dlib/lat/display.cfm?ms=uclalat_1429_b754_281662&searchType=subject&subjectID=214016)
    , [洛杉磯時報](../Page/洛杉磯時報.md "wikilink")

68.