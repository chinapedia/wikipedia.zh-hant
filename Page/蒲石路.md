**蒲石路**（**Rue
Bourgeat**）是[中国](../Page/中国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[长乐路](../Page/长乐路_\(上海\).md "wikilink")（跨[黄浦区](../Page/黄浦区.md "wikilink")、[徐汇区和](../Page/徐汇区.md "wikilink")[静安区](../Page/静安区.md "wikilink")）在1914年—1943年之间的路名，前[上海法租界内一条重要东西向街道](../Page/上海法租界.md "wikilink")。东起萨坡赛路（Rue
Chapsal）\[1\]，西至海格路（Avenue Haig）\[2\]。全长3327米。

## 历史

蒲石路最早建成的一段是位于杜美路（Route
Doumer）\[3\]和善钟路\[4\]之间的部分，1902年由上海法租界公董局越界修筑，当时是杜美路的一部分。1914年，[上海法租界向西大规模拓展后划入界内](../Page/上海法租界.md "wikilink")。同年，法租界公董局修筑圣母院路（Route
des
Soeurs）\[5\]至迈尔西爱路段，以法国律师名命名。以后向两端延伸，东端通至萨坡赛路，西端与杜美路、善钟路相接。1943年，又从善钟路继续向西修筑至麦琪路（Route
Alfread Magy
）\[6\]，新修路段称为刘家弄。同年，汪精卫政府接收上海法租界，将蒲石路全路连同刘家弄一并改名为长乐路。1946年，长乐路继续向西延伸至华山路。

## 交汇道路

  - [萨坡赛路](../Page/萨坡赛路.md "wikilink")（Rue
    Chapsal，今[淡水路](../Page/淡水路.md "wikilink")）
  - [白尔部路](../Page/白尔部路.md "wikilink")（Rue Paul
    Beau，[重庆中路](../Page/重庆中路.md "wikilink")）
  - [贝禘鏖路](../Page/贝禘鏖路.md "wikilink")（Rue Lieutenant
    Petiot，[成都南路](../Page/成都南路.md "wikilink")）
  - [圣母院路](../Page/圣母院路.md "wikilink")（Route des
    Soeurs，[瑞金一路](../Page/瑞金一路.md "wikilink")）
  - [迈尔西爱路](../Page/迈尔西爱路.md "wikilink")（Route Rue Cardinal
    Mercier，今[茂名南路](../Page/茂名南路.md "wikilink")）
  - [亚尔培路](../Page/亚尔培路.md "wikilink")（Avenue du Roi
    Albert，[陕西南路](../Page/陕西南路.md "wikilink")）
  - [劳尔登路](../Page/劳尔登路.md "wikilink")（Rue
    L.Lorton，[襄阳北路](../Page/襄阳北路.md "wikilink")）
  - [古拔路](../Page/古拔路.md "wikilink")（Route
    Courbet，[富民路](../Page/富民路.md "wikilink")）
  - [杜美路](../Page/杜美路.md "wikilink")（Route
    Doumer，今[东湖路](../Page/东湖路.md "wikilink")）
  - [麦阳路](../Page/麦阳路.md "wikilink")（Route
    Mayen，[华亭路](../Page/华亭路.md "wikilink")）
  - [善钟路](../Page/善钟路.md "wikilink")（今[常熟路](../Page/常熟路.md "wikilink")）
  - [麦琪路](../Page/麦琪路.md "wikilink")（Route Alfread Magy
    ，今[乌鲁木齐中路](../Page/乌鲁木齐中路.md "wikilink")）
  - [海格路](../Page/海格路.md "wikilink")（Avenue
    Haig，今[华山路](../Page/华山路.md "wikilink")）

## 沿路近代建筑

蒲石路沿路主要为住宅区，在[迈尔西爱路](../Page/迈尔西爱路.md "wikilink")（Route Rue Cardinal
Mercier）\[7\]路口附近，有法国总会\[8\]、[华懋公寓](../Page/华懋公寓.md "wikilink")\[9\][兰心大戏院](../Page/兰心大戏院.md "wikilink")、天主教[君王堂](../Page/君王堂.md "wikilink")\[10\]等重要机构。

  - 109号 [华懋公寓](../Page/华懋公寓.md "wikilink")（1929年，今锦江宾馆北楼）
  - 339弄 蒲石村
  - 401弄 储康里
  - 570弄 蒲园
  - 611弄 永存坊
  - 613弄 沪江别墅 1939年
  - 637弄 [友华村](../Page/友华村.md "wikilink")
  - 672弄 留园、履安村
  - 752-762号（富民路210弄2-14号）住宅
  - 764弄 杜美新村 长乐新村
  - 784－786号 刘氏住宅（今市房地产协会）
  - 788号 [周信芳故居](../Page/周信芳.md "wikilink") 1895年
  - 800号 住宅 华园物业公司办公楼

## 注释

<references />

[Category:上海租界](../Category/上海租界.md "wikilink")

1.  今淡水路
2.  今华山路
3.  今东湖路
4.  今常熟路
5.  今瑞金一路
6.  今乌鲁木齐中路
7.  今茂名南路
8.  今花园酒店
9.  今[锦江宾馆](../Page/锦江宾馆.md "wikilink")
10. 原址已经改建为[新锦江大酒店](../Page/新锦江大酒店.md "wikilink")