**卡茨-穆迪代数**是一個[李代數](../Page/李代數.md "wikilink")，通常無限維，其定義自（Victor
Kac所謂的）[廣義根系](../Page/廣義根系.md "wikilink")。卡茨-穆迪代数的應用遍及[數學和](../Page/數學.md "wikilink")[理論物理學](../Page/理論物理學.md "wikilink")。

## 定義

假定以下材料：

  - \(C = (c_{ij})\)
    ——一個*r*階[廣義嘉當矩陣](../Page/廣義嘉當矩陣.md "wikilink")(**generalised
    Cartan matrix**) \(C = (c_{ij})\) *r*.
  - \(\mathfrak{h}\) ———— 一個 2*n* − *r*維複向量空間 \(\mathfrak{h}\).
  - \(\mathfrak{h}^*\) ————
    \(\mathfrak{h}\)的[對偶空間](../Page/對偶空間.md "wikilink")
  - \(\alpha_i\\) ————\(\mathfrak{h}\) 中 *n*
    枚相互[獨立的元](../Page/線性獨立.md "wikilink")，稱為*對偶根*(**co-root**)
  - \(\alpha_i^*\) ————\(\mathfrak{h}^*\) 中*n* 枚線性相互獨立的元
    ,稱為**根**(**root**)
  - 上述各元滿足 \(\alpha_i^*(\alpha_j) = c_{ij}\).

卡茨-穆迪代数\(\mathfrak{g}\) 由符號 \(e_i\) , \(f_i\) (*i=1,..,n*)
及空間\(\mathfrak{h}\) 生成：

以上各元滿足以下關係：

  - \([e_i,f_i] = \alpha_i.\\)
  - \([e_i,f_j] = 0\\) ；其中 \(i \neq j.\)
  - \([e_i,x]=\alpha_i^*(x)e_i\), 其中\(x \in \mathfrak{h}.\)
  - \([f_i,x]=-\alpha_i^*(x)f_i\), 其中 \(x \in \mathfrak{h}.\)
  - \([x,x'] = 0\\) ；其中 \(x,x' \in \mathfrak{h}.\)
  - \([e_i,[e_i,\ldots,[e_i,e_j]]] = \mathcal C_{e_i}^{1-c_{ij}}\;e_j = 0\)
    ;其中\(e_i.\\)出現 \(1-c_{ij}\\) 次;
  - \([f_i,[f_i,\ldots,[f_i,f_j]]] = \mathcal C_{f_i}^{1-c_{ij}}\;f_j = 0\)
    ;其中\(f_i.\\)出現 \(1-c_{ij}\\) 次;

(其中 \(\mathcal C_{x}\;y = [x,y]\).)

一個 [實](../Page/實數.md "wikilink")(維數可以無限)李代數亦可稱為 Kac–Moody代數，若其
[複化](../Page/複化.md "wikilink") 是個 Kac–Moody代數.

## 釋義

  - \(\mathfrak{h}\) 是此卡茨-穆迪代数的一[嘉當子代數](../Page/嘉當子代數.md "wikilink")。
  - 若 *g* 是 Kac–Moody 代數的一元，使得

\[\forall x\in \mathfrak{h}\,[g,x]=\omega(x)g\]

其中 ω 是 \(\mathfrak{h}^*\)的一元，

則稱*g* 為 [權](../Page/權_\(李代數\).md "wikilink")(weight) ω的. 我们可分解一Kac–Moody
代數成其冪空間，則嘉當子代數
\(\mathfrak{h}\)的冪为零，*e*<sub>*i*</sub>的冪为α\*<sub>*i*</sub>，而*f*<sub>*i*</sub>的冪为−α\*<sub>*i*</sub>。若二冪特徵向量的[李括號非零](../Page/李括號.md "wikilink")，則其冪是二冪之和。（若
\(i \neq j\) ) 則 \([e_i,f_j] = 0\\) 一條件即指 α\*<sub>*i*</sub> 都是簡單根。

## 分類

我们可分解廣義嘉當矩陣 C 成矩陣積 DS, 其中 D 是 正[對角矩陣](../Page/對角矩陣.md "wikilink"), S 是
[對稱矩陣](../Page/對稱矩陣.md "wikilink")。 然則有三種可能:

  - \(\mathfrak{g}\) 有限維 [單李代數](../Page/單李代數.md "wikilink") (S
    [正定](../Page/正定矩陣.md "wikilink"))
  - \(\mathfrak{g}\) 是 [仿射李代數](../Page/仿射李代數.md "wikilink") (S
    [正半定](../Page/正半定.md "wikilink"))
  - 雙曲 (S 不定)

S 不可能 [負定](../Page/負定.md "wikilink") 或 [負半定](../Page/負半定.md "wikilink")
因其對角元皆正.

## 參見

  - [外爾-卡茨特徵標公式](../Page/外爾特徵標公式#外爾-卡茨特徵標公式.md "wikilink")
  - [广义卡茨-穆迪代数](../Page/广义卡茨-穆迪代数.md "wikilink")

## 參考

  - \<<Infinite-Dimensional Lie Algebras>\>, Victor Kac, Cambridge
    University Press
  - [Encyclopaedia of Mathematics, Springer On-line
    References](http://eom.springer.de/K/k055050.htm)

[Category:李代数](../Category/李代数.md "wikilink")
[Category:代數結構](../Category/代數結構.md "wikilink")