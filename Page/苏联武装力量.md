**苏维埃社会主义共和国联盟武装力量**（）是指[苏维埃社会主义共和国联盟在](../Page/苏维埃社会主义共和国联盟.md "wikilink")1946年2月到1991年12月[苏联解体前的武装部队](../Page/苏联解体.md "wikilink")，其前身为[苏联工农红军和](../Page/苏联工农红军.md "wikilink")[苏联海军](../Page/苏联海军.md "wikilink")。

根据1925年的全联盟兵役法，苏联军事力量分为[地面部队](../Page/地面部队.md "wikilink")、[空军部队](../Page/苏联空军.md "wikilink")、[苏联海军](../Page/苏联海军.md "wikilink")、[国家政治保卫局和](../Page/国家政治保卫局.md "wikilink")[内卫部队](../Page/俄罗斯内卫部队.md "wikilink")5大部分，其中地面部队和空军部队统称[苏联工农红军](../Page/苏联工农红军.md "wikilink")。[国家政治保卫局在](../Page/国家政治保卫局.md "wikilink")1934年归属[内务人民委员部](../Page/内务人民委员部.md "wikilink")。二战结束后苏联进行军事改组，由陆军和空军部队组成的[苏联工农红军和](../Page/苏联工农红军.md "wikilink")[苏联海军合并成为苏联武装力量](../Page/苏联海军.md "wikilink")。[国土防空军](../Page/苏联国土防空军.md "wikilink")、[战略火箭军和](../Page/苏联战略火箭军.md "wikilink")[全联盟国家民事防御部队先后在](../Page/全联盟国家民事防御部队.md "wikilink")1948、1960和1970年成立并归属苏联武装力量\[1\]。在顶峰时期，苏联武装力量共下辖6个兵种，按照苏联官方对其重要性的排序分别为[战略火箭军](../Page/苏联战略火箭军.md "wikilink")、[陆军](../Page/苏联陆军.md "wikilink")、[国土防空军](../Page/苏联国土防空军.md "wikilink")、[空军](../Page/苏联空军.md "wikilink")、[海军和](../Page/苏联海军.md "wikilink")[民防部队](../Page/全联盟国家民事防御部队.md "wikilink")。除了受[国防委员会指挥的苏联武装力量外](../Page/苏联国防委员会.md "wikilink")，广义上的苏联军事力量还包括受[克格勃指挥的](../Page/克格勃.md "wikilink")[边防军和受](../Page/苏联边防军.md "wikilink")[内务部指挥的](../Page/俄罗斯内卫部队.md "wikilink")[内卫部队](../Page/俄罗斯内卫部队.md "wikilink")。

1988年，不含边防和内卫部队，苏联武装力量的兵力达到513万人\[2\]。

## 军事体制

### 指挥结构

苏联武装力量的最高领导是[苏共中央](../Page/苏共中央.md "wikilink")、[最高苏维埃和](../Page/最高苏维埃.md "wikilink")[部长会议](../Page/苏联部长会议.md "wikilink")。[国防会议于](../Page/苏联国防会议.md "wikilink")1974年成立，是平时的最高国防决策机构，战时是军事最高领导机关，国防会议主席由[苏共中央总书记担任](../Page/苏共中央总书记.md "wikilink")。国防部是武装力量的中央领导机关，[总参谋部是主要指挥机关](../Page/总参谋部.md "wikilink")，[总政治部是武装力量中党的领导机关](../Page/总政治部.md "wikilink")。实行普遍义务兵役制，海军服役3年，其他军兵种2年

### 历任国防会议主席

1.  [勃列日涅夫](../Page/勃列日涅夫.md "wikilink")
2.  [安德罗波夫](../Page/安德罗波夫.md "wikilink")
3.  [契尔年科](../Page/契尔年科.md "wikilink")
4.  [戈尔巴乔夫](../Page/戈尔巴乔夫.md "wikilink")

## 军事实力

[缩略图](https://zh.wikipedia.org/wiki/File:T-80_during_maneuvers.JPEG "fig:缩略图")演習的蘇聯軍[T-80戰車](../Page/T-80.md "wikilink")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Военный_Билет_СССР.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:1938_CPA_594.jpg "fig:缩略图")

### 陆军

[缩略图](https://zh.wikipedia.org/wiki/File:1991_coup_attempt5.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:2S9_Nona-S.png "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:BTRTHeavyAPC1.jpg "fig:缩略图")

1988年时，[苏联陆军人数](../Page/苏联陆军.md "wikilink")199万，分为16个军区。编为51个坦克师、142个摩托化步兵师、7个空降师、约10个空中突击旅和16个方面军炮兵师，装备[主战坦克](../Page/主战坦克.md "wikilink")5.3万辆、装甲战斗车辆6.3万辆、火炮2.9万门、地对地[导弹发射架约](../Page/导弹.md "wikilink")1570部。

苏联陆军军区为：

1.  [莫斯科军区](../Page/莫斯科军区.md "wikilink")，也叫中央陆军
2.  [白俄罗斯军区](../Page/白俄罗斯军区.md "wikilink")
3.  [列宁格勒军区](../Page/列宁格勒军区.md "wikilink")
4.  [基辅军区](../Page/基辅军区.md "wikilink")
5.  [波罗的海军区](../Page/波罗的海军区.md "wikilink")
6.  [伏尔加河沿岸军区](../Page/伏尔加河沿岸军区.md "wikilink")
7.  [乌拉尔军区](../Page/乌拉尔军区.md "wikilink")
8.  [外高加索军区](../Page/外高加索军区.md "wikilink")
9.  [北高加索军区](../Page/北高加索军区.md "wikilink")
10. [敖德萨军区](../Page/敖德萨军区.md "wikilink")
11. [喀尔巴阡军区](../Page/喀尔巴阡军区.md "wikilink")
12. [突厥斯坦军区](../Page/突厥斯坦军区.md "wikilink")
13. [中亚军区](../Page/中亚军区.md "wikilink")
14. [西伯利亚军区](../Page/西伯利亚军区.md "wikilink")
15. [外贝加尔军区](../Page/外贝加尔军区.md "wikilink")
16. [远东军区](../Page/远东军区.md "wikilink")

### 海军

1988年时，[苏联海军人数为](../Page/苏联海军.md "wikilink")45.1万，分为4个舰队和一个区舰队。装备弹道导弹潜艇79艘，巡航导弹潜艇和攻击潜艇268艘，主要水面作战舰只193艘，小型水面作战舰只797艘，登陆舰艇187艘，主要辅助船只269艘。
5个舰队分别为：

1.  [波罗的海舰队](../Page/波罗的海舰队.md "wikilink")
2.  [黑海舰队](../Page/黑海舰队.md "wikilink")
3.  [太平洋舰队](../Page/太平洋舰队.md "wikilink")
4.  [北方舰队](../Page/北方舰队.md "wikilink")
5.  [裡海區舰队](../Page/裡海區舰队.md "wikilink")

### 空军

1988年时，[苏联空军人数为](../Page/苏联空军.md "wikilink")45.3万，远程航空兵约10万人，编为5个戰略航空军，装备中远程[轰炸机](../Page/轰炸机.md "wikilink")752架；方面军航空兵约31.5万人，编入12个军区和驻东欧四国苏军集群的空军序列，装备[战斗机](../Page/战斗机.md "wikilink")、[战斗轰炸机和](../Page/战斗轰炸机.md "wikilink")[攻擊機](../Page/攻擊機.md "wikilink")5167架。

### 国土防空军

1988年时，[苏联国土防空军人数为](../Page/苏联国土防空军.md "wikilink")63.5万，编入5个防空区和10个军区的防空军序列。拥有各型[战斗机](../Page/战斗机.md "wikilink")1300架，[防空导弹发射架](../Page/防空导弹.md "wikilink")9600部，雷达约7000部，有[反弹道导弹发射架](../Page/反弹道导弹.md "wikilink")100部。

### 战略火箭军

1988年时，[苏联战略火箭军人数](../Page/战略火箭军.md "wikilink")29.8万，编为6个火箭集团军，有发射控制司令部300个。拥有洲际导弹1398枚，战略作战飞机1690架。战略火箭军掌握着令人生畏苏联核武器库，是苏联的战略部队。

### 边防军

1988年时，[苏联边防军人数为](../Page/苏联边防军.md "wikilink")25万。

### 内卫部队

1988年时，[苏联内卫部队人数为](../Page/俄羅斯內衛部隊.md "wikilink")35万。

### 驻外部队

苏联是[华沙条约组织的创始国](../Page/华沙条约组织.md "wikilink")，1988年，苏在[德意志民主共和国驻军约](../Page/德意志民主共和国.md "wikilink")38万人，[波兰约](../Page/波兰人民共和国.md "wikilink")4万、[捷克斯洛伐克](../Page/捷克斯洛伐克社会主义共和国.md "wikilink")8万、[匈牙利](../Page/匈牙利人民共和国.md "wikilink")6.5万。苏联另在1987年前在[阿富汗有兵力](../Page/阿富汗.md "wikilink")11.8万，在[蒙古有](../Page/蒙古人民共和国.md "wikilink")6.5万。在[越南](../Page/越南.md "wikilink")、[古巴及其他一些](../Page/古巴.md "wikilink")[亚](../Page/亚洲.md "wikilink")[非](../Page/非洲.md "wikilink")[拉国家也驻有军队或军事顾问和技术人员](../Page/拉丁美洲.md "wikilink")。

## 军衔制度

苏军分海、陆、空军军衔三类，有七等22级。其等级次序为：[苏联大元帅](../Page/苏联大元帅.md "wikilink")、[苏联元帅](../Page/苏联元帅.md "wikilink")（[苏联海军元帅](../Page/苏联海军元帅.md "wikilink")）、[军兵种主帅](../Page/苏联军兵种主帅.md "wikilink")、[大将和](../Page/大将.md "wikilink")[军兵种元帅](../Page/苏联军兵种元帅.md "wikilink")、[上将](../Page/上将.md "wikilink")、[中将](../Page/中将.md "wikilink")、[少将](../Page/少将.md "wikilink")、[上校](../Page/上校.md "wikilink")、[中校](../Page/中校.md "wikilink")、[少校](../Page/少校.md "wikilink")、[大尉](../Page/大尉.md "wikilink")、[上尉](../Page/上尉.md "wikilink")、[中尉](../Page/中尉.md "wikilink")、[少尉](../Page/少尉.md "wikilink")、[高级准尉](../Page/准尉.md "wikilink")、[准尉](../Page/准尉.md "wikilink")、[大士](../Page/大士.md "wikilink")、[上士](../Page/上士.md "wikilink")、[中士](../Page/中士.md "wikilink")、[下士](../Page/下士.md "wikilink")、[上等兵](../Page/上等兵.md "wikilink")、[列兵](../Page/列兵.md "wikilink")。<small>请参见[苏联元帅列表](../Page/苏联元帅列表.md "wikilink")</small>
[缩略图](https://zh.wikipedia.org/wiki/File:Kerch2007Sevastopol.jpg "fig:缩略图")\]\]

## 武装力量历史

### 内战时期

1917年11月7日（[儒略历](../Page/儒略历.md "wikilink")10月25日），[十月革命爆发](../Page/十月革命.md "wikilink")，[布尔什维克夺取了权力](../Page/布尔什维克.md "wikilink")，1918年1月28日，[苏俄人民委员会组建了](../Page/俄罗斯苏维埃联邦社会主义共和国人民委员会.md "wikilink")[工农红军](../Page/苏联红军.md "wikilink")\[3\]，几个月后又组建了[工农红海军](../Page/苏联海军.md "wikilink")。随后[工农红军](../Page/苏联红军.md "wikilink")、[工农红海军与](../Page/苏联海军.md "wikilink")[白军和](../Page/白军.md "wikilink")[波兰第二共和国](../Page/波兰第二共和国.md "wikilink")、[不列颠帝国](../Page/不列颠帝国.md "wikilink")、[法兰西第三共和国](../Page/法兰西第三共和国.md "wikilink")、[美利坚合众国](../Page/美利坚合众国.md "wikilink")、[大日本帝国等](../Page/大日本帝国.md "wikilink")14国的派遣军展开了一系列激烈的战斗，并击败了[白军与外国干涉势力的军队](../Page/白军.md "wikilink")，1920年，[俄国内战基本结束](../Page/俄国内战.md "wikilink")，1922年，[俄国内战彻底停止](../Page/俄国内战.md "wikilink")。[苏维埃社会主义共和国联盟成立](../Page/苏维埃社会主义共和国联盟.md "wikilink")。

### 波苏战争

从1919年开始，为争夺西乌克兰地区的控制权，[苏维埃俄国与](../Page/苏维埃俄国.md "wikilink")[波兰第二共和国开始爆发冲突](../Page/波兰第二共和国.md "wikilink")，1920年4月，毕苏斯基的向东攻入乌克兰，[苏维埃俄国与](../Page/苏维埃俄国.md "wikilink")[波兰第二共和国的战争公开化](../Page/波兰第二共和国.md "wikilink")，[苏俄的](../Page/苏俄.md "wikilink")[工农红军随后开始](../Page/苏联红军.md "wikilink")[反攻](../Page/反攻.md "wikilink")，并进攻至波兰首都[华沙附近](../Page/华沙.md "wikilink")，但波兰军队在[华沙战役中取得了胜利](../Page/华沙战役_\(1920年\).md "wikilink")。向东前进的波军节节胜利，苏俄提出求和，1920年10月，双方[停火](../Page/停火.md "wikilink")。1921年3月18日签订了正式的和平条约——《[里加条约](../Page/里加条约.md "wikilink")》，划分了波兰和苏联之间有争议的边界。

### 二战时期

#### 冬季战争

[Winter-War-Overview.png](https://zh.wikipedia.org/wiki/File:Winter-War-Overview.png "fig:Winter-War-Overview.png")
1939年，苏联军队进攻芬兰，在付出了巨大的代价后迫使[芬兰签订了和约](../Page/芬兰.md "wikilink")，在这次战争中苏联红军损失惨重，48,000人阵亡，27万人失踪。芬兰方面则有22,830人阵亡。

#### 卫国战争

1941年6月22日凌晨4时30分，[纳粹德国入侵苏联](../Page/纳粹德国.md "wikilink")。由于苏联领导人准备不足，苏联红军在战争初期节节败退，1941年9月底，纳粹德国的中央集团军群大举进攻莫斯科，苏军凭借极其坚强的抵抗和熟悉的自然条件顶住了德军进攻，并于1941年12月转入反攻。

[Bundesarchiv_Bild_183-E0406-0022-011,_Russland,_deutscher_Kriegsgefangener.jpg](https://zh.wikipedia.org/wiki/File:Bundesarchiv_Bild_183-E0406-0022-011,_Russland,_deutscher_Kriegsgefangener.jpg "fig:Bundesarchiv_Bild_183-E0406-0022-011,_Russland,_deutscher_Kriegsgefangener.jpg")
1943年，苏联红军赢得了斯大林格勒战役的胜利，使[苏德战争出现了历史性转折](../Page/苏德战争.md "wikilink")。1943年7月，库尔斯克战役打响，8月23日，[苏联红军取得了胜利](../Page/苏联红军.md "wikilink")，至此，苏联完全赢得了战争主动权。

1944年，苏联收复了本国的全部领土，并将战线推移到境外，进入[波兰](../Page/波兰.md "wikilink")、[罗马尼亚](../Page/罗马尼亚.md "wikilink")、[保加利亚](../Page/保加利亚.md "wikilink")、[南斯拉夫等国](../Page/南斯拉夫.md "wikilink")。1945年春天，苏军在东欧各国军民的配合下，通过[布達佩斯戰役](../Page/布達佩斯戰役.md "wikilink")、[維斯瓦河-奧得河攻勢](../Page/維斯瓦河-奧得河攻勢.md "wikilink")、[西里西亞攻勢](../Page/西里西亞攻勢.md "wikilink")、[東波美拉尼亞攻勢](../Page/東波美拉尼亞攻勢.md "wikilink")，消灭大量德军有生力量，占领波兰、匈牙利、奥地利东部及[维也纳](../Page/维也纳.md "wikilink")、[捷克斯洛伐克一部分和德国东部](../Page/捷克斯洛伐克.md "wikilink")。1945年4月16日，[苏联红军开始进攻](../Page/苏联红军.md "wikilink")[柏林](../Page/柏林.md "wikilink")，5月2日下午3时，德军停止抵抗，[柏林战役结束](../Page/柏林战役.md "wikilink")。

1945年5月8日24时，德国无条件投降仪式在柏林正式举行。参加仪式的苏方代表是[朱可夫元帅和外交部副部长](../Page/朱可夫.md "wikilink")[维辛斯基](../Page/维辛斯基.md "wikilink")。仪式由朱可夫主持。盟军最高统帅部的代表为：英国空军上将[特德](../Page/特德.md "wikilink")、美国战略空军司令[斯帕茨将军](../Page/斯帕茨将军.md "wikilink")、法军总司令[塔西尼](../Page/塔西尼.md "wikilink")，代表德国在投降书上签字的是：陆军元帅[威廉·凱特爾](../Page/威廉·凱特爾.md "wikilink")、海军上将[弗雷德堡](../Page/弗雷德堡.md "wikilink")、空军上将[施通普夫](../Page/施通普夫.md "wikilink")。德国投降书于1945年5月9日零时生效。欧洲战争到此结束。

### 冷战时期

苏联军队在二战后直接或间接地参加了[朝鲜战争](../Page/朝鲜战争.md "wikilink")、[匈牙利事件](../Page/匈牙利事件.md "wikilink")、[布拉格之春](../Page/布拉格之春.md "wikilink")、[中苏边界冲突](../Page/中苏边界冲突.md "wikilink")、[越南战争和](../Page/越南战争.md "wikilink")[阿富汗战争等](../Page/1979年阿富汗战争.md "wikilink")。

### 解散

1991年底，[苏联解体](../Page/苏联解体.md "wikilink")，苏军装备被15个独立的原苏联加盟共和国的武装力量所继承，核武器则被俄罗斯、乌克兰、白俄罗斯和哈萨克斯坦四国继承，但[白俄罗斯](../Page/白俄罗斯.md "wikilink")、[乌克兰和](../Page/乌克兰.md "wikilink")[哈萨克斯坦在](../Page/哈萨克斯坦.md "wikilink")1996年将境内所有[核武器移交给俄罗斯](../Page/核武器.md "wikilink")，三国成为无核国家。原先驻扎在东欧国家的苏联军队在苏联解体后依旧驻扎在这些国家，在之后的几年里这些军队才陆续撤回俄罗斯境内，最后一名驻扎在东欧国家的苏军士兵在1999年离开波罗的海国家返回俄罗斯。

## 著名武器

  - [T-34坦克](../Page/T-34坦克.md "wikilink")
  - [KV系列坦克](../Page/KV系列坦克.md "wikilink")
  - [IS系列坦克](../Page/IS系列坦克.md "wikilink")
  - [R7閃電號運載火箭](../Page/R7閃電號運載火箭.md "wikilink")
  - [伊爾攻擊機](../Page/伊爾攻擊機.md "wikilink")
  - [喀秋莎火箭炮](../Page/喀秋莎火箭炮.md "wikilink")
  - [ISU-152式重型突擊炮](../Page/ISU-152式重型突擊炮.md "wikilink")
  - [SU-85驅逐戰車](../Page/SU-85驅逐戰車.md "wikilink")
  - [AK-47突击步枪](../Page/AK-47突击步枪.md "wikilink")
  - [MON-200反人員地雷](../Page/MON-200反人員地雷.md "wikilink")
  - [T-54/55系列主战坦克](../Page/T-54/55系列主战坦克.md "wikilink")
  - [T-72主戰坦克](../Page/T-72主戰坦克.md "wikilink")
  - [T-80主戰坦克](../Page/T-80主戰坦克.md "wikilink")
  - [Mi-24雌鹿直升機](../Page/Mi-24雌鹿直升機.md "wikilink")
  - [米格-21戰鬥機](../Page/米格-21戰鬥機.md "wikilink")
  - [米格-29戰鬥機](../Page/米格-29戰鬥機.md "wikilink")
  - [苏-27战斗机](../Page/苏-27战斗机.md "wikilink")
  - [Tu-95轟炸機](../Page/Tu-95轟炸機.md "wikilink")
  - [M-50轟炸機](../Page/M-50轟炸機.md "wikilink")
  - [颱風級核潛艇](../Page/颱風級核潛艇.md "wikilink")
  - [ASU-57空降自走炮](../Page/ASU-57空降自走炮.md "wikilink")

## 参考文献

## 外部链接

  - [喀山之战：托洛茨基组建苏俄第一支真正红军部队](http://news.ifeng.com/mil/history/200912/1216_1567_1477040.shtml)

## 参见

  - [苏联红军](../Page/苏联红军.md "wikilink")
  - [俄罗斯联邦武装力量](../Page/俄罗斯联邦武装力量.md "wikilink")

{{-}}

[USSR](../Category/各国武装力量.md "wikilink")
[苏联武装力量](../Category/苏联武装力量.md "wikilink")
[Category:1918年俄羅斯建立](../Category/1918年俄羅斯建立.md "wikilink")
[Category:1991年蘇聯廢除](../Category/1991年蘇聯廢除.md "wikilink")

1.  斯高特, 《The Armed Forces of the Soviet Union, Westview Press》, 1979,
    p.13
2.  [余一中：苏联经济真的曾经世界第二吗？](http://news.163.com/10/0920/01/6H03HE9U00012Q9L.html)
3.  [俄罗斯举行活动庆祝祖国保卫者日](http://news.ifeng.com/world/detail_2011_02/23/4821503_0.shtml)