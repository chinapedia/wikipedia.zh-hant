[MK19946_Franz_Josef_Jung.jpg](https://zh.wikipedia.org/wiki/File:MK19946_Franz_Josef_Jung.jpg "fig:MK19946_Franz_Josef_Jung.jpg")

**弗朗茨·約瑟夫·榮格**（Franz Josef
Jung，），出生于[黑森州小城](../Page/黑森.md "wikilink")[埃爾巴哈](../Page/埃爾巴哈.md "wikilink")（），现為[德國的一名](../Page/德國.md "wikilink")[政治家](../Page/政治家.md "wikilink")，屬於[德國基督民主聯盟的其中一員](../Page/德國基督民主聯盟.md "wikilink")。

2005年11月2日，他作為[默克爾內閣中的成員擔任](../Page/默克爾內閣.md "wikilink")[德國國防部部長一職](../Page/德國國防部.md "wikilink")，接替前任的[彼得·施特魯克](../Page/彼得·施特魯克.md "wikilink")。1999年至2000年間，他曾担任黑森州联邦和欧洲事务部长和黑森州[政府主管](../Page/政府主管.md "wikilink")（）的职务。

## 生平、职业

1968年荣格通過[德國大學入學資格考試](../Page/德國大學入學資格考試.md "wikilink")（），並在兩年兵役屆滿后，於1970年开始进入[美因茨大学学习](../Page/美因茨大学.md "wikilink")[法学](../Page/法学.md "wikilink")，1974年他就通过了第一級的[德國國家司法考試](../Page/德國國家司法考試.md "wikilink")（），1976年通过了第二级的司法考試。1978年在美因茨大学完成题为《Die
Regionalplanung in Hessen, dargestellt am Beispiel der Regionalen
Planungsgemeinschaft
Rhein-Main-Taunus》的论文，更获得法学[博士](../Page/博士.md "wikilink")。荣格曾在[埃爾特維勒](../Page/埃爾特維勒.md "wikilink")（）担任[律师和](../Page/律师.md "wikilink")[公证员](../Page/公证员.md "wikilink")。2000年8月－2005年12月12日期间起初在管委会，2003年5月之后在[法兰克福足球俱乐部监事会工作](../Page/法兰克福足球俱乐部.md "wikilink")。

弗朗茨·约瑟夫·荣格已婚并有三个孩子。

[J](../Category/德國國防部長.md "wikilink")
[Category:美茵茨大学校友](../Category/美茵茨大学校友.md "wikilink")