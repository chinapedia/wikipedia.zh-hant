<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>史托港舊會徽</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.worldfootball.org/logo.php/country/ENG/club/1000000137">www.worldfootball.org:Stockport County</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>2007.11.21</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>作者</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>

### Fair-use rationale

1.  the logo is only being used for informational purposes
2.  its inclusion in the article adds significantly to the article
    because it is the primary means of identifying the subject of this
    article