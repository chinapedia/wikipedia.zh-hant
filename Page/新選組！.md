《**新選組！**》是[NHK於](../Page/日本放送協會.md "wikilink")2004年播出的第43作[大河劇](../Page/NHK大河劇.md "wikilink")。原作與劇本是[三谷幸喜](../Page/三谷幸喜.md "wikilink")，主演是[香取慎吾](../Page/香取慎吾.md "wikilink")。

## 作品背景與反響

以[近藤勇為主人翁](../Page/近藤勇.md "wikilink")，以及京都守護職下警備組織、即有名的[新選組為題材](../Page/新選組.md "wikilink")。以幕末做為題材的前一檔是1998年的[德川慶喜](../Page/德川慶喜_\(大河劇\).md "wikilink")。主演者香取慎吾是第一次演出大河劇。編劇是創作許多知名[電視劇的三谷幸喜](../Page/電視劇.md "wikilink")。近藤與[坂本龍馬](../Page/坂本龍馬.md "wikilink")、[桂小五郎](../Page/木戶孝允.md "wikilink")（木戶孝允）在江戶相識，再加上許多史實的角色，形成青春性與娛樂性都很高的故事。

大河劇本身則是自從《[信長](../Page/信長_KING_OF_ZIPANGU.md "wikilink")》以來11年後再次使用有聲樂的主題曲，歌詞出自編劇三谷之手。第26回以後，片頭加上了歌詞的字幕。

演員方面，很積極的使用了具有廣大影迷的年輕演員與[舞台劇演員](../Page/舞台劇.md "wikilink")，這是因為以往大河劇的觀眾都是偏向高齡層的關係。然而本劇終究還是苦於平均收視率只有17.4%的低迷景象，評價上被認為很謹慎認真的對每一個登場角色做了描寫。本作當中，以[堺雅人與](../Page/堺雅人.md "wikilink")[山本耕史等這類由劇團出身的演員受到矚目](../Page/山本耕史.md "wikilink")。

2006年1月1日預定播出的正月時代劇，是續編「新選組\!\!
土方歲三的最後一日」，這也是大河劇僅見的製作續集。原作、劇本還是三谷幸喜，主演者是[山本耕史](../Page/山本耕史.md "wikilink")，而本作的部份出演者亦會登場。

香取曾於[朝日電視台的節目](../Page/朝日電視台.md "wikilink")「SmaSTATION-4」中將本劇出演者（新選組隊士與三谷）集結在一起，展開少見的談話性節目，而在富士電視台系列的[SMAP×SMAP](../Page/SMAP×SMAP.md "wikilink")，曾用三谷寫作的「局長！」當作短劇。「森田一義的時間：笑也是很好的！」裡，香取更是穿著戲服登場。

## 花絮

  - 電影[壬生義士傳裡](../Page/壬生義士傳.md "wikilink")，佐藤浩市飾演齋藤一，堺雅人飾演沖田總司。堺雅人在本劇演出山南一角時，此片還在拍攝中。
  - 飾演土方歲三之兄的栗塚旭與飾演沖田療養所植木屋的島田順司，以前在[NET](../Page/NET.md "wikilink")･[東映的](../Page/東映.md "wikilink")『新選組血風錄』分別飾演土方歲三與沖田總司。
  - 飾演近藤勇之兄的阿南健治，曾在三谷幸喜編劇的電視劇「龍馬委託！」裡飾演近藤勇。
  - 飾演坂本龍馬的江口洋介，在三谷幸喜原作及編劇－電影「龍馬之妻，她的丈夫與她的愛人」裡飾演一位因為長得很像龍馬而成為[龍馬遺孀愛人的男子虎藏](../Page/楢崎龍.md "wikilink")，在本作被拔擢為龍馬一角時，他說「我終於變成龍馬本人了」。
  - 電視劇版[壬生義士傳裡](../Page/壬生義士傳.md "wikilink")，伊原剛志飾演土方歲三。
  - 瑳川哲朗在NHK大河劇「[三姊妹](../Page/三姊妹.md "wikilink")」（1967年）裡飾演近藤勇。
  - 佐佐木功飾演的內山彥次郎，在劇中討伐了由小田切讓飾演的齋藤一。但佐佐木功曾在動畫電影「宇宙戰艦大和號
    愛的戰士們」也擔任過一位叫「齋藤始」的角色（與齋藤一的日文讀音相同，不過是造形與角色設定完全不同的人物）。

## 工作人員

  - 作・・・[三谷幸喜](../Page/三谷幸喜.md "wikilink")
  - 音樂・・・[服部隆之](../Page/服部隆之.md "wikilink")　
  - 主題曲演奏・・・[NHK交響樂團](../Page/NHK交響樂團.md "wikilink")
  - 主題曲指揮・・・廣上淳一
  - 男高音獨唱・・・[約翰・健・努梭](../Page/約翰・健・努梭.md "wikilink")
  - 演奏・・・フェイス・ミュージック
  - 時代考証・・・大石學、山村龍也
  - 建築考証・・・平井聖
  - 服裝考証・・・小泉清子
  - 音樂考証・・・奧中康人
  - 殺陣武術指導・・・林邦史朗
  - 所作指導・・・西川箕乃助
  - 邦樂指導・・・本條秀太郎
  - 資料提供・・・三野行德
  - 題字・・・荻野丹雪
  - 版畫・・・木田安彥
  - 京語言指導・・・井上裕季子
  - 土佐語言指導・・・岡林桂子
  - 薩摩語言指導・・・西田聖志郎
  - 御所語言指導・・・堀井令以知
  - 協力・・・[京都府](../Page/京都府.md "wikilink")、[京都市](../Page/京都市.md "wikilink")、[東京都](../Page/東京都.md "wikilink")[調布市](../Page/調布市.md "wikilink")・[日野市](../Page/日野市.md "wikilink")、[茨城縣](../Page/茨城縣.md "wikilink")、[千葉縣](../Page/千葉縣.md "wikilink")、[神奈川縣](../Page/神奈川縣.md "wikilink")[小田原市](../Page/小田原市.md "wikilink")、[大本山金戒光明寺](../Page/金戒光明寺.md "wikilink")
  - 制作統括・・・吉川幸司
  - 導演・・・**[放送日程](../Page/#放送日程.md "wikilink")**記載
  - 旁白・・・[小寺康雄](../Page/小寺康雄.md "wikilink")（開場）、[伊東敏惠](../Page/伊東敏惠.md "wikilink")（本編後的新選組之行）

## 出演　

### 新選組

**[試衛館的朋友們](../Page/試衛館.md "wikilink")（主要人物）**

  - 島崎勝太→[近藤勇](../Page/近藤勇.md "wikilink")（大久保大和）（[香取慎吾](../Page/香取慎吾.md "wikilink")（[SMAP](../Page/SMAP.md "wikilink")））
  - [土方歲三](../Page/土方歲三.md "wikilink")（內藤隼人）（[山本耕史](../Page/山本耕史.md "wikilink")）
  - [山南敬助](../Page/山南敬助.md "wikilink")（[堺雅人](../Page/堺雅人.md "wikilink")）
  - 沖田惣次郎→[沖田總司](../Page/沖田總司.md "wikilink")（[田邊季正](../Page/田邊季正.md "wikilink")→[藤原龍也](../Page/藤原龍也.md "wikilink")）
  - [永倉新八](../Page/永倉新八.md "wikilink")（[山口智充](../Page/山口智充.md "wikilink")（[DonDokoDon](../Page/DonDokoDon_\(搞笑團體\).md "wikilink")））
  - 山口一→[齋藤一](../Page/齋藤一.md "wikilink")（[小田切讓](../Page/小田切讓.md "wikilink")）
  - [井上源三郎](../Page/井上源三郎.md "wikilink")（[小林隆](../Page/小林隆.md "wikilink")）
  - [藤堂平助](../Page/藤堂平助.md "wikilink")（[中村勘太郎](../Page/中村勘太郎_\(2代目\).md "wikilink")）
  - [原田左之助](../Page/原田左之助.md "wikilink")（[山本太郎](../Page/山本太郎.md "wikilink")）

**[水戶藩出身者](../Page/水戶藩.md "wikilink")**

  - [芹澤鴨](../Page/芹澤鴨.md "wikilink")（[佐藤浩市](../Page/佐藤浩市.md "wikilink")）
  - [新見錦](../Page/新見錦.md "wikilink")（[相島一之](../Page/相島一之.md "wikilink")）
  - [平間重助](../Page/平間重助.md "wikilink")（[剛州](../Page/剛州.md "wikilink")）
  - [平山五郎](../Page/平山五郎.md "wikilink")（[坂田聰](../Page/坂田聰.md "wikilink")）
  - [野口健司](../Page/野口健司.md "wikilink")（[岡本幸作](../Page/岡本幸作.md "wikilink")）
  - [佐伯又三郎](../Page/佐伯又三郎.md "wikilink")（[松谷賢示](../Page/松谷賢示.md "wikilink")）

**[御陵衛士等等的人們](../Page/御陵衛士.md "wikilink")**

  - 伊東大藏→[伊東甲子太郎](../Page/伊東甲子太郎.md "wikilink")（[谷原章介](../Page/谷原章介.md "wikilink")）
  - [三木三郎](../Page/鈴木三樹三郎.md "wikilink")（[平泉陽太](../Page/平泉陽太.md "wikilink")）
  - [加納鷲雄](../Page/加納鷲雄.md "wikilink")（[小原雅人](../Page/小原雅人.md "wikilink")）
  - [篠原泰之進](../Page/篠原泰之進.md "wikilink")（[小梶直人](../Page/小梶直人.md "wikilink")）
  - [服部武雄](../Page/服部武雄.md "wikilink")（[梶浦昭生](../Page/梶浦昭生.md "wikilink")）

**其他的新選組隊士**

  - [松原忠司](../Page/松原忠司.md "wikilink")（[甲本雅裕](../Page/甲本雅裕.md "wikilink")）
  - [武田觀柳齋](../Page/武田觀柳齋.md "wikilink")（[八島智人](../Page/八島智人.md "wikilink")）
  - [谷三十郎](../Page/谷三十郎.md "wikilink")（[前田豐](../Page/前田豐.md "wikilink")）
  - [尾形俊太郎](../Page/尾形俊太郎.md "wikilink")（[飯田基祐](../Page/飯田基祐.md "wikilink")）
  - [島田魁](../Page/島田魁.md "wikilink")（[照英](../Page/照英.md "wikilink")）
  - [葛山武八郎](../Page/葛山武八郎.md "wikilink")（[平畠啟史](../Page/平畠啟史.md "wikilink")（[DonDokoDon](../Page/DonDokoDon_\(搞笑團體\).md "wikilink")））
  - [山崎烝](../Page/山崎烝.md "wikilink")（[桂吉彌](../Page/桂吉彌.md "wikilink")）
  - [河合耆三郎](../Page/河合耆三郎.md "wikilink")（[大倉孝二](../Page/大倉孝二.md "wikilink")）
  - [谷萬太郎](../Page/谷萬太郎.md "wikilink")（[若松力](../Page/若松力.md "wikilink")）
  - 谷昌武→[谷周平](../Page/谷周平.md "wikilink")⇔近藤周平（[淺利陽介](../Page/淺利陽介.md "wikilink")）
  - [大石鍬次郎](../Page/大石鍬次郎.md "wikilink")（[根本慎太郎](../Page/根本慎太郎.md "wikilink")）
  - [淺野薰](../Page/淺野薰.md "wikilink")（[中村俊太](../Page/中村俊太.md "wikilink")）
  - [尾關雅次郎](../Page/尾關雅次郎.md "wikilink")（[熊面鯉](../Page/熊面鯉.md "wikilink")）
  - [安藤早太郎](../Page/安藤早太郎.md "wikilink")（[河西祐樹](../Page/河西祐樹.md "wikilink")）

**[浪士組同志](../Page/浪士組.md "wikilink")**

  - [殿內義雄](../Page/殿內義雄.md "wikilink")（[生瀨勝久](../Page/生瀨勝久.md "wikilink")）
  - [家里次郎](../Page/家里次郎.md "wikilink")（[大田慶文](../Page/大田慶文.md "wikilink")）
  - [根岸友山](../Page/根岸友山.md "wikilink")（[奧村公延](../Page/奧村公延.md "wikilink")）
  - [粕谷新五郎](../Page/粕谷新五郎.md "wikilink")（[伊吹吾朗](../Page/伊吹吾朗.md "wikilink")）
  - [祐天仙之助](../Page/祐天仙之助.md "wikilink")（[渡部雄作](../Page/渡部雄作.md "wikilink")）
  - [大村達尾](../Page/大村達尾.md "wikilink")（[江畑浩規](../Page/江畑浩規.md "wikilink")）
  - [池田德太郎](../Page/池田德太郎.md "wikilink")（[野中功](../Page/野中功.md "wikilink")）
  - [村上俊五郎](../Page/村上俊五郎.md "wikilink")（[立川政市](../Page/立川政市.md "wikilink")）
  - [阿比留銳三郎](../Page/阿比類銳三郎.md "wikilink")（[矢部太郎](../Page/矢部太郎.md "wikilink")（[カラテカ](../Page/カラテカ_\(搞笑團體\).md "wikilink")））

### 多摩的人們

  - 近藤勇的養父
    近藤周助→[近藤周齋](../Page/近藤周助.md "wikilink")（[田中邦衛](../Page/田中邦衛.md "wikilink")）
  - 近藤勇的養母
    [近藤筆](../Page/近藤筆.md "wikilink")（[野際陽子](../Page/野際陽子.md "wikilink")）
  - 近藤勇之妻
    [近藤常](../Page/松井常.md "wikilink")（[田畑智子](../Page/田畑智子.md "wikilink")）
  - 近藤勇之女
    [近藤玉](../Page/近藤玉.md "wikilink")（[山城毬花](../Page/山城毬花.md "wikilink")→[鎗田千裕](../Page/鎗田千裕.md "wikilink")→[松元環季](../Page/松元環季.md "wikilink")）
  - 沖田總司之姊
    [沖田光](../Page/沖田光.md "wikilink")（[澤口靖子](../Page/澤口靖子.md "wikilink")）
  - 沖田總司的姐夫
    [沖田林太郎](../Page/沖田林太郎.md "wikilink")（[日野陽仁](../Page/日野陽仁.md "wikilink")）
  - 近藤勇之兄
    [宮川音五郎](../Page/宮川音五郎.md "wikilink")（[阿南健治](../Page/阿南健治.md "wikilink")）
  - 多摩的名主
    [小島鹿之助](../Page/小島鹿之助.md "wikilink")（[小野武彥](../Page/小野武彥.md "wikilink")）
  - 土方歲三之兄
    [土方為次郎](../Page/土方為次郎.md "wikilink")（[栗塚旭](../Page/栗塚旭.md "wikilink")）
  - 土方歲三的姐夫
    [佐藤彥五郎](../Page/佐藤彥五郎.md "wikilink")（[小日向文世](../Page/小日向文世.md "wikilink")）
  - 土方歲三之姊
    [佐藤信](../Page/佐藤信.md "wikilink")（[淺田美代子](../Page/淺田美代子.md "wikilink")）
  - 近藤勇的小時玩伴
    [瀧本捨助](../Page/瀧本捨助.md "wikilink")（[中村獅童](../Page/中村獅童_\(2代目\).md "wikilink")）
  - 瀧本捨助之父 瀧本繁藏（[沼田曜一](../Page/沼田曜一.md "wikilink")）
  - 土方歲三行商的朋友 紐爺（[江幡高志](../Page/江幡高志.md "wikilink")）
  - 阿常之父
    [松井八十五郎](../Page/松井八十五郎.md "wikilink")（[淺沼晉平](../Page/淺沼晉平.md "wikilink")）
  - 阿常之母
    [松井絹](../Page/松井絹.md "wikilink")（[蘆澤孝子](../Page/蘆澤孝子.md "wikilink")）
  - 阿筆的老友 夏（[岩崎加根子](../Page/岩崎加根子.md "wikilink")）
  - 土方歲三前女友 阿靜（[乙葉](../Page/乙葉.md "wikilink")）
  - 紅組總大將 萩原糺（[榎木兵衛](../Page/榎木兵衛.md "wikilink")）
  - 土方歲三的未婚妻
    [阿琴](../Page/阿琴.md "wikilink")（[田丸麻紀](../Page/田丸麻紀.md "wikilink")）
  - 沖田的療養人
    [植木屋平五郎](../Page/柴田平五郎.md "wikilink")（[島田順司](../Page/島田順司.md "wikilink")）

※瀧本捨助是以真實人物・[松本捨助為原型的角色](../Page/松本捨助.md "wikilink")

### 京都的人們

  - 壬生的鄉士
    [八木源之丞](../Page/八木源之丞.md "wikilink")（[伊東四朗](../Page/伊東四朗.md "wikilink")）
  - 源之丞之妻
    [八木雅](../Page/八木雅.md "wikilink")（[松金米子](../Page/松金米子.md "wikilink")）
  - 雅之母 八木久（[正司歌江](../Page/正司歌江.md "wikilink")）
  - 源之丞女兒 八木秀（八木秀二郎）（[吹石一惠](../Page/吹石一惠.md "wikilink")）
  - 源之丞之子
    [八木為三郎](../Page/八木為三郎.md "wikilink")（[卷島一將](../Page/卷島一將.md "wikilink")）
  - 八木家奉公人 房吉 （[星路易斯](../Page/星路易斯.md "wikilink")）
  - 醫師 孝庵（[笹野高史](../Page/笹野高史.md "wikilink")）
  - 西本願寺 僧侶（[大門伍朗](../Page/大門伍朗.md "wikilink")）
  - 西本願寺侍臣
    [西村兼文](../Page/西村兼文.md "wikilink")（[本間憲一](../Page/本間憲一.md "wikilink")）

※八木家其實是子女皆有的，但是這個女兒早死了。當時在葬禮的時候，近藤與芹澤都有前往的紀錄，於是根據這些紀錄，就有了「秀」與「久」的創作誕生。劇中「秀」在女扮男裝時使用化名「八木秀二郎」，其實是八木源之丞長男的名字，是史實有的人物。

### 京都的女子們

  - 近藤勇的愛人
    阿幸⇔[深雪太夫](../Page/深雪太夫.md "wikilink")（[枚田菜菜子](../Page/枚田菜菜子.md "wikilink")→[優香](../Page/優香.md "wikilink")）
  - 桂小五郎之妻
    [幾松](../Page/木戶松子.md "wikilink")（[菊川怜](../Page/菊川怜.md "wikilink")）
  - 坂本龍馬之妻
    [阿龍](../Page/阿龍.md "wikilink")([楢崎龍](../Page/楢崎龍.md "wikilink"))（[麻生久美子](../Page/麻生久美子.md "wikilink")）
  - 寺田屋女老闆
    [登勢](../Page/登勢.md "wikilink")（[戶田恵子](../Page/戶田恵子.md "wikilink")）
  - 芹澤鴨的愛人 阿梅（[鈴木京香](../Page/鈴木京香.md "wikilink")）
  - 原田左之助之妻
    [政](../Page/原田政.md "wikilink")（[橋野惠美](../Page/橋野惠美.md "wikilink")）
  - 永倉新八的愛人
    小常→[阿園](../Page/小常.md "wikilink")（[小西美帆](../Page/小西美帆.md "wikilink")）
  - 山南敬助的愛人
    [明里](../Page/明里.md "wikilink")（[鈴木砂羽](../Page/鈴木砂羽.md "wikilink")）
  - 千波甲太郎之妻 阿初（[清水美那](../Page/清水美那.md "wikilink")）
  - 深雪太夫之妹
    [阿孝](../Page/阿孝.md "wikilink")（[金子莉菜](../Page/金子莉菜.md "wikilink")→優香）

### 當時的名人

  - 土佐藩士
    [坂本龍馬](../Page/坂本龍馬.md "wikilink")（[江口洋介](../Page/江口洋介.md "wikilink")）
  - 土佐藩士
    [武市半平太](../Page/武市半平太.md "wikilink")（[大衛伊東](../Page/大衛伊東.md "wikilink")）
  - 土佐藩士
    [谷守部](../Page/谷干城.md "wikilink")（[粟根誠](../Page/粟根誠.md "wikilink")）
  - 土佐脱藩
    [望月龜彌太](../Page/望月龜彌太.md "wikilink")（[三宅弘城](../Page/三宅弘城.md "wikilink")）
  - 土佐脱藩
    [北添佶摩](../Page/北添佶摩.md "wikilink")（[松井工](../Page/松井工.md "wikilink")）
  - 土佐脱藩
    [中岡慎太郎](../Page/中岡慎太郎.md "wikilink")（[增澤望](../Page/增澤望.md "wikilink")）
  - 長州藩士
    [桂小五郎](../Page/桂小五郎.md "wikilink")→木戶貫治→木戶孝允（[石黑賢](../Page/石黑賢.md "wikilink")）
  - 長州藩士
    [久坂玄瑞](../Page/久坂玄瑞.md "wikilink")（[池內博之](../Page/池內博之.md "wikilink")）
  - 長州藩士
    [吉田稔磨](../Page/吉田稔磨.md "wikilink")（[佐藤一平](../Page/佐藤一平.md "wikilink")）
  - 長州藩士
    [寺島忠三郎](../Page/寺島忠三郎.md "wikilink")（[加藤大治郎](../Page/加藤大治郎_\(俳優\).md "wikilink")）
  - 長州藩士
    [入江九一](../Page/入江九一.md "wikilink")（[渡邊航](../Page/渡邊航.md "wikilink")）
  - 長州脱藩
    [千波甲太郎](../Page/千波甲太郎.md "wikilink")（[宅間孝行](../Page/宅間孝行.md "wikilink")）
  - 薩摩藩士
    大島吉之助→[西鄉吉之助](../Page/西鄉隆盛.md "wikilink")（[宇梶剛士](../Page/宇梶剛士.md "wikilink")）
  - 薩摩藩士
    [大久保一藏](../Page/大久保利通.md "wikilink")（[保村大和](../Page/保村大和.md "wikilink")）
  - 薩摩藩士
    [黑田了介](../Page/黑田清隆.md "wikilink")（[峰尾進](../Page/峰尾進.md "wikilink")）
  - 薩摩藩士
    [有馬藤太](../Page/有馬藤太.md "wikilink")（[古田新太](../Page/古田新太.md "wikilink")）
  - 肥後藩士
    [河上彥齋](../Page/河上彥齋.md "wikilink")（[高杉亘](../Page/高杉亘.md "wikilink")）
  - 肥後脱藩
    [宮部鼎藏](../Page/宮部鼎藏.md "wikilink")（[四方堂亘](../Page/四方堂亘.md "wikilink")）
  - 水戶脱藩
    [廣岡子之次郎](../Page/廣岡子之次郎.md "wikilink")（[橋本潤](../Page/橋本潤.md "wikilink")）
  - 福井藩士
    [橋本左內](../Page/橋本左內.md "wikilink")（[山內圭哉](../Page/山內圭哉.md "wikilink")）
  - 近江鄉士
    [古高俊太郎](../Page/古高俊太郎.md "wikilink")（[中山俊](../Page/中山俊.md "wikilink")）
  - 出羽鄉士
    [清河八郎](../Page/清河八郎.md "wikilink")（[白井晃](../Page/白井晃.md "wikilink")）
  - 久留米脱藩
    [真木和泉](../Page/真木和泉.md "wikilink")（[大谷亮介](../Page/大谷亮介.md "wikilink")）
  - 兵學者
    [佐久間象山](../Page/佐久間象山.md "wikilink")（[石坂浩二](../Page/石坂浩二.md "wikilink")）
  - 新政府軍・軍監 [香川敬三](../Page/香川敬三.md "wikilink")
    （[松本今次](../Page/松本今次.md "wikilink")）
  - 繪師
    [板倉槐堂](../Page/淡海槐堂.md "wikilink")（[舟田走](../Page/舟田走.md "wikilink")）
  - 阿園未婚夫
    市川宇八郎→[芳賀宜道](../Page/芳賀宜道.md "wikilink")（[八十田勇一](../Page/八十田勇一.md "wikilink")）

### 會津藩

  - 京都守護職・會津藩主
    [松平容保](../Page/松平容保.md "wikilink")（[筒井道隆](../Page/筒井道隆.md "wikilink")）
  - 會津藩小用方
    [廣澤富次郎](../Page/廣澤富次郎.md "wikilink")（[矢島健一](../Page/矢島健一.md "wikilink")）
  - 會津藩小用方
    [秋月悌次郎](../Page/秋月悌次郎.md "wikilink")（[堀部圭亮](../Page/堀部圭亮.md "wikilink")）
  - 會津藩小用人
    [小森久太郎](../Page/小森久太郎.md "wikilink")（[荒木優騎](../Page/荒木優騎.md "wikilink")）
  - 京都所司代・桑名藩主
    [松平定敬](../Page/松平定敬.md "wikilink")（[高橋一生](../Page/高橋一生.md "wikilink")）

### 幕府

  - 京都見廻組隊長
    [佐佐木只三郎](../Page/佐佐木只三郎.md "wikilink")（[伊原剛志](../Page/伊原剛志.md "wikilink")）
  - 海軍總裁
    [勝海舟](../Page/勝海舟.md "wikilink")（[野田秀樹](../Page/野田秀樹.md "wikilink")）
  - 幕臣
    山岡鉄太郎→[山岡鐵舟](../Page/山岡鐵舟.md "wikilink")（[羽場裕一](../Page/羽場裕一.md "wikilink")）
  - 浪士組取扱
    [鵜殿鳩翁](../Page/鵜殿鳩翁.md "wikilink")（[梅野泰靖](../Page/梅野泰靖.md "wikilink")）
  - 講武所教授方→浪士組取扱
    松平主稅助→[松平上總介](../Page/松平忠敏.md "wikilink")（[藤木孝](../Page/藤木孝.md "wikilink")）
  - 大坂西町奉行所與力
    [內山彥次郎](../Page/內山彥次郎.md "wikilink")（[佐佐木功](../Page/佐佐木功.md "wikilink")）
  - 幕府醫師
    [松本良順](../Page/松本良順.md "wikilink")（[田中哲司](../Page/田中哲司.md "wikilink")）
  - 大目付
    [永井尚志](../Page/永井尚志.md "wikilink")（[佐藤B作](../Page/佐藤B作.md "wikilink")）
  - 廣島藩役人（[濱口優](../Page/濱口優.md "wikilink")）
  - 第15代将軍
    [德川慶喜](../Page/德川慶喜.md "wikilink")（[今井朋彥](../Page/今井朋彥.md "wikilink")）
  - 海軍奉行
    [榎本武揚](../Page/榎本武揚.md "wikilink")（[草彅剛](../Page/草彅剛.md "wikilink")（[SMAP](../Page/SMAP.md "wikilink")））（友情出演）
  - 菜葉隊隊長・小松（[Bibiru大木](../Page/Bibiru大木.md "wikilink")）（友情出演）

※史實上的菜葉隊，是以橫濱居留地擔任警備的旗本部隊（組成比新選組早）做為母體的集團，而Bibiru大木飾演的小松則是三谷創作的人物。

### 朝廷

  - 第121代天皇
    [孝明天皇](../Page/孝明天皇.md "wikilink")（[中村福助](../Page/中村福助.md "wikilink")）
  - 公家
    [岩倉友山](../Page/岩倉具視.md "wikilink")（[中村有志](../Page/中村有志.md "wikilink")）
  - 第122代天皇
    [明治天皇](../Page/明治天皇.md "wikilink")（[中川景四](../Page/中川景四.md "wikilink")）
  - 公家
    [中山忠能](../Page/中山忠能.md "wikilink")（[仲恭司](../Page/仲恭司.md "wikilink")）
  - 皇族
    [中川宮](../Page/中川宮朝彥親王.md "wikilink")（[濱口悟](../Page/濱口悟.md "wikilink")）

### 町眾

  - ヒュースケン的戀人 阿富（[木村多江](../Page/木村多江.md "wikilink")）
  - 大坂小野川部屋親方
    [小野川親方](../Page/小野川親方.md "wikilink")（[瑳川哲朗](../Page/瑳川哲朗.md "wikilink")）
  - 小野川力士
    [熊川熊五郎](../Page/熊川熊五郎.md "wikilink")（[舞之海秀平](../Page/舞之海秀平.md "wikilink")）
  - 大坂横綱　黑神（[大至](../Page/大至.md "wikilink")）
  - 行司　[鵜池保介](../Page/木村庄之助_\(30代\).md "wikilink")([第30代
    木村庄之助](../Page/木村庄之助_\(30代\).md "wikilink"))

### 外國人

  - 美國總領事 [Townsend
    Harris](../Page/Townsend_Harris.md "wikilink")（[Marty
    Kuehnert](../Page/Marty_Kuehnert.md "wikilink")）
  - 美國公使館書記 [Henry Conrad Joannes
    Heusken](../Page/Henry_Conrad_Joannes_Heusken.md "wikilink")（[川平慈英](../Page/川平慈英.md "wikilink")）
  - 英國水兵（[Ken Cogger](../Page/Ken_Cogger.md "wikilink")）
  - 英國水兵（Matthew）

## 放送

### 放送日程

第1回與最終回為一小時加長版。第27回因[参議院選舉特別節目而改為](../Page/第20回參議院議員通常選舉.md "wikilink")7:15～8:00播出。

| 放送回  | 放送日         | 標題        | 導演    | 收視率   |
| ---- | ----------- | --------- | ----- | ----- |
| 第1回  | 2004年1月11日  | 黑船前來      | 清水一彥  | 26.3% |
| 第2回  | 2004年1月18日  | 多摩的驕傲     | 23.9% |       |
| 第3回  | 2004年1月25日  | 母親離家出走    | 20.3% |       |
| 第4回  | 2004年2月1日   | 天地倒轉      | 20.6% |       |
| 第5回  | 2004年2月8日   | 婚禮之日      | 20.4% |       |
| 第6回  | 2004年2月15日  | 休斯肯快逃     | 伊勢田雅也 | 19.9% |
| 第7回  | 2004年2月22日  | 繼承四代目     | 伊勢田雅也 | 19.7% |
| 第8回  | 2004年2月29日  | 變成怎樣的日本   | 伊勢田雅也 | 18.5% |
| 第9回  | 2004年3月7日   | 全部就在這封信   | 伊勢田雅也 | 19.8% |
| 第10回 | 2004年3月14日  | 終於結成浪士組   | 伊勢田雅也 | 15.6% |
| 第11回 | 2004年3月21日  | 母親大人前來    | 清水一彥  | 17.5% |
| 第12回 | 2004年3月28日  | 往西行！      | 清水一彥  | 14.0% |
| 第13回 | 2004年4月4日   | 芹澤鴨、爆發    | 伊勢田雅也 | 15.6% |
| 第14回 | 2004年4月11日  | 抵達京都      | 伊勢田雅也 | 14.4% |
| 第15回 | 2004年4月18日  | 去或留       | 清水一彥  | 17.6% |
| 第16回 | 2004年4月25日  | 一筆啟上、阿常様  | 清水一彥  | 16.6% |
| 第17回 | 2004年5月2日   | 首次死亡      | 伊勢田雅也 | 15.6% |
| 第18回 | 2004年5月9日   | 初出動！壬生浪士  | 伊勢田雅也 | 15.4% |
| 第19回 | 2004年5月16日  | 告別式的夜晚    | 吉田浩樹  | 15.5% |
| 第20回 | 2004年5月23日  | 芹澤鴨醉了     | 吉田浩樹  | 15.3% |
| 第21回 | 2004年5月30日  | 且慢事件      | 清水一彥  | 14.2% |
| 第22回 | 2004年6月6日   | 屋頂上的芹澤鴨   | 伊勢田雅也 | 17.9% |
| 第23回 | 2004年6月13日  | 政變、八月十八日  | 伊勢田雅也 | 16.8% |
| 第24回 | 2004年6月20日  | 避開不能通行的道路 | 清水一彥  | 17.8% |
| 第25回 | 2004年6月27日  | 新選組誕生     | 清水一彥  | 19.3% |
| 第26回 | 2004年7月4日   | 局長 近藤勇    | 伊勢田雅也 | 17.4% |
| 第27回 | 2004年7月11日  | 池田屋事件前夕   | 伊勢田雅也 | 13.4% |
| 第28回 | 2004年7月18日  | 奔向池田屋     | 清水一彥  | 16.6% |
| 第29回 | 2004年7月25日  | 討伐長州      | 山本敏彥  | 14.4% |
| 第30回 | 2004年8月1日   | 永倉新八、叛亂   | 山本敏彥  | 17.6% |
| 第31回 | 2004年8月8日   | 回到江戶      | 伊勢田雅也 | 16.5% |
| 第32回 | 2004年8月15日  | 山南脫逃      | 伊勢田雅也 | 14.8% |
| 第33回 | 2004年8月22日  | 朋友之死      | 伊勢田雅也 | 16.1% |
| 第34回 | 2004年8月29日  | 寺田屋大騷動    | 山本敏彥  | 18.1% |
| 第35回 | 2004年9月5日   | 再見了，壬生村   | 山本敏彥  | 18.7% |
| 第36回 | 2004年9月12日  | 對決見廻組！    | 伊勢田雅也 | 16.0% |
| 第37回 | 2004年9月19日  | 薩長同盟締結！   | 清水一彥  | 15.9% |
| 第38回 | 2004年9月26日  | 那個隊士的切腹   | 山本敏彥  | 16.2% |
| 第39回 | 2004年10月3日  | 將軍死去      | 伊勢田雅也 | 14.5% |
| 第40回 | 2004年10月10日 | 平助的旅程     | 清水一彥  | 14.2% |
| 第41回 | 2004年10月17日 | 落魄的觀柳齋    | 小林大児  | 16.0% |
| 第42回 | 2004年10月24日 | 龍馬暗殺      | 清水一彥  | 16.5% |
| 第43回 | 2004年10月31日 | 決戰、油小路    | 伊勢田雅也 | 19.6% |
| 第44回 | 2004年11月7日  | 局長襲撃      | 土井祥平  | 17.9% |
| 第45回 | 2004年11月14日 | 源桑之死      | 清水一彥  | 16.0% |
| 第46回 | 2004年11月21日 | 東去        | 清水一彥  | 18.5% |
| 第47回 | 2004年11月28日 | 再會        | 清水一彥  | 17.3% |
| 第48回 | 2004年12月5日  | 流山        | 吉川邦夫  | 16.2% |
| 最終回  | 2004年12月12日 | 親愛的朋友啊    | 清水一彥  | 21.8% |

### 新選組！特別篇

2004年12月26日、29日、2006年1月3日播出。包括未公開畫面與主要隊士成員的對談。

  - 第1部「成為武士！」
  - 第2部「新選組誕生」
  - 第3部「愛的朋友啊」

### 你的安可2004

2004年12月30日播出，有堺雅人的留言。

  - どんとこい新選組！隊士座談會
  - 新選組！朋友之死(第33回)

## 週邊產品

### NHK大河劇導覽書

  - 新選組！　前編・後編

### CD

  - NHK大河劇新選組！　原聲帶

### DVD

  - 新選組！完全版 第壹集
  - 新選組！完全版 第貳集
  - 新選組！特別篇

## 關連項目

  - [新選組\!\! 土方歲三 最期的一日](../Page/新選組!!_土方歲三_最期的一日.md "wikilink")

## 外部連結

  - [新選組\!\!土方歲三最期的一日
    NHK正月時代劇](https://web.archive.org/web/20051217101656/http://www.nhk.or.jp/drama/html_news_shinsen.html)
  - [株式會社Video
    Research](https://web.archive.org/web/20051218025057/http://www.videor.co.jp/index.htm)

[Category:新選組題材作品](../Category/新選組題材作品.md "wikilink")
[Category:戊辰戰爭題材作品](../Category/戊辰戰爭題材作品.md "wikilink")
[Category:2004年日本電視劇集](../Category/2004年日本電視劇集.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:日劇學院賞最佳作品](../Category/日劇學院賞最佳作品.md "wikilink")
[Category:堺雅人](../Category/堺雅人.md "wikilink")
[Category:草彅剛](../Category/草彅剛.md "wikilink")