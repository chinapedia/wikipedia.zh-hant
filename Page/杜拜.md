**杜拜**（；-{zh-hans:[台湾及](../Page/台湾.md "wikilink")[香港译作](../Page/香港.md "wikilink")**杜-{}-拜**;zh-hant:[中國大陸譯作](../Page/中國大陸.md "wikilink")**迪-{}-拜**}-）是[阿拉伯联合酋长国人口最多的](../Page/阿拉伯联合酋长国.md "wikilink")[城市](../Page/城市.md "wikilink")，\[1\]也是[中东地区的经济和金融中心](../Page/中东地区.md "wikilink")，位於[波斯湾東南岸](../Page/波斯湾.md "wikilink")。

作為[阿拉伯联合酋长国的经济中心](../Page/阿拉伯联合酋长国.md "wikilink")，迪拜已经成为[全球城市](../Page/全球城市.md "wikilink")\[2\]，同时也是主要的游客和货物枢纽。1960年代，迪拜经济开始趋向贸易，并且开始实施小規模的石油勘探。1966年发现石油，1969年起開始出口石油。\[3\]石油收益加速杜拜早期城市發展，但石油儲量有限且產量不高，目前經濟以資本投資和旅遊貿易、服務等驅動，石油收益佔比低於5%。\[4\]

杜拜採行西方商業模式，經濟發展主要依賴、航空業、房地產及金融服務業。\[5\]\[6\]\[7\]根據[萨拉姆标准](../Page/萨拉姆标准.md "wikilink")，杜拜被認為是[穆斯林遊客的最佳旅遊地點](../Page/穆斯林.md "wikilink")。\[8\]隨著創新大型建築計畫、舉辦運動賽事提高城市關注度，杜拜也成為的指標城市，世界最高大樓[哈里發塔也位於杜拜](../Page/哈里發塔.md "wikilink")。但杜拜對於大量[南亚及](../Page/南亚.md "wikilink")[菲律宾勞動者不良的](../Page/菲律宾人.md "wikilink")[人權紀錄](../Page/杜拜人權.md "wikilink")，亦遭批評。\[9\]於[2007年-2008年環球金融危機後](../Page/2007年-2008年環球金融危機.md "wikilink")，杜拜房地市場在2008–09間面臨嚴重衰退，\[10\]隨著經濟恢復成長，2015年財政出現盈餘。\[11\]

## 名称

「杜拜」这一名称被认为有着许多不同的来源。其意思可能是用来指“集市”（سوق‎,
souk），或者是指“金钱”。也有学者指出可能意为“爬”，指流动速度缓慢的迪拜河。

## 历史

[AlRas_Deira_Mid1960s.jpg](https://zh.wikipedia.org/wiki/File:AlRas_Deira_Mid1960s.jpg "fig:AlRas_Deira_Mid1960s.jpg")
[Mid-20th_century_Dubai.JPG](https://zh.wikipedia.org/wiki/File:Mid-20th_century_Dubai.JPG "fig:Mid-20th_century_Dubai.JPG")

公元前三世纪，迪拜就有人类活动。这几年来，考古学家在迪拜久迈拉地区发现公元五世纪人类住区遗址及文物。

最早提到迪拜的文献为1095年阿拉伯学者Abu Abdullah al-Bakri所著的《地理书》（Book of
Geography）。威尼斯珍珠商人Gaspero Balbi于1580年访问了此地，并提到了迪拜。

迪拜从1799年开始有村庄出现的记录。在18世纪早期，巴尼亚斯部落的阿尔·阿布·法拉沙后裔开始迁徙至迪拜，而他们一直在[阿布扎比居住直至](../Page/阿布扎比.md "wikilink")1833年。

1820年1月8日，杜拜的[酋长](../Page/酋长.md "wikilink")[谢赫·拉希德二世·本·赛义德·阿勒马克图姆与](../Page/拉希德·本·赛义德·阿勒马克图姆.md "wikilink")[英国签订了基础和约](../Page/英国.md "wikilink")。1833年，巴尼亚斯部落的阿勒马克图姆王朝在没有抵抗的情形之下离开了阿布扎比并迁移至迪拜。1835年，迪拜与[特鲁西尔酋长国的其余部分与英国签订了海上停战协定并在二十年后签订](../Page/阿拉伯联合酋长国.md "wikilink")“永久停战协议”。1852年，马克图姆成为酋长，开创了该家族在迪拜的统治。从此，迪拜作为新独立的酋长国，正式与阿布扎比酋长国分离。迪拜以其在1892年所订的秘密协议受到英国的保护。1892年3月，[特鲁西尔阿曼成立](../Page/阿拉伯联合酋长国.md "wikilink")。多年之后，迪拜得到很快的发展，成为定居在城市中的外国商人的重要的海上贸易港口，它的宽松的自由贸易价格和合理的税制，吸引着来自各个地区的冒险家们相继来到这里寻求发展。与周围市镇不同，迪拜的管理者鼓励商业贸易。迪拜曾是一个吸引（以印度人为主）定居在城市中的外国商人的重要港口。直到1930年代，它都以珍珠出口而闻名。

1966年，在离杜拜海岸线120千米的地方发现了石油，之后迪拜获得石油开采权。随着石油的开采与出口，迪拜的经济和城市基础建设得到了更大的发展，1968年至1975年間，杜拜人口成長超過三倍。\[12\]1971年，英国作为保护国离开[波斯湾](../Page/波斯湾.md "wikilink")，迪拜联合阿布扎比和其它五个酋长国于1971年12月2日成立了[阿拉伯联合酋长国](../Page/阿拉伯联合酋长国.md "wikilink")，1973年杜拜與其他酋長國開始使用共同貨幣[阿联酋迪拉姆](../Page/阿联酋迪拉姆.md "wikilink")，\[13\]同年，原先杜拜與卡達組成的貨幣聯盟宣告解散。\[14\]。在20世纪70年代后，迪拜得到快速的发展。其国民收入的发展基础来源于它的石油贸易，迪拜的发展得益于国家的领导者，通过其成功地管理和运作，将其将储存量并不十分丰富的石油的最大收益地给予了人民。并利用石油所带了发展前景迅速地发展城市基础建设，使得国家发展繁荣。此后迪拜利用它的国际性的贸易市场在发展经济的同时大力发展旅游业，每年都有很多的的国际游客来这里观光和度假，而且游客数量随年增长。\[15\]

## 地理

杜拜位于[阿拉伯半岛中部](../Page/阿拉伯半岛.md "wikilink")、[阿拉伯湾南岸](../Page/阿拉伯湾.md "wikilink")，[波斯湾](../Page/波斯湾.md "wikilink")、[沙迦的西南](../Page/沙迦.md "wikilink")、阿布扎比的东北，与[南亚次大陆隔海相望](../Page/南亚次大陆.md "wikilink")，并往陆地内延伸。哈塔（阿拉伯语：حتا）作为杜拜的[外飛地](../Page/外飛地.md "wikilink")，为[阿曼所包围](../Page/阿曼.md "wikilink")。

杜拜海岸线长734公里，它沿海岸线呈西南到东北的走向，长30公里，最宽处10余公里。一条长约14公里的海汊将它分为两部分，西南部分称为迪拉，东北部叫巴尔杜拜（阿拉伯语：بر
دبي ）靠海汊的迪拉地段最为繁华。海汊从南到北，建有戈尔胡德桥、马克西姆桥和山代盍隧道，将西部连在一起。

### 气候

杜拜属于[亚热带气候](../Page/亚热带气候.md "wikilink")，杜拜的夏季（四至十月）气温高达45℃以上，局部沙漠地区有小沙暴。十一月至次年三月为冬季，最低气温一般在11至13℃左右，历史最低气温为7℃。杜拜降水量在最近幾十年稍微增加，年降雨量為。\[16\]杜拜夏季濕度偏中高，導致體感較為不舒適。\[17\]

## 政府及政治

目前迪拜的领导人是[谢赫·穆罕默德·本·拉希德·阿勒马克图姆酋长](../Page/谢赫·穆罕默德·本·拉希德·阿勒马克图姆.md "wikilink")，與哥哥谢赫·[马克图姆·本·拉希德·阿勒马克图姆酋长一样](../Page/马克图姆·本·拉希德·阿勒马克图姆.md "wikilink")，也是阿联酋的副总统和总理。迪拜现任王储是酋长次子[哈姆丹·本·穆罕默德·本·拉希德·阿勒马克图姆](../Page/哈姆丹·本·穆罕默德·本·拉希德·阿勒马克图姆.md "wikilink")。杜拜任命[联邦国民议会](../Page/联邦国民议会.md "wikilink")40席代表中的8席，該機構為阿拉伯聯合大公國的國家立法機關。\[18\]

### 伊斯蘭教法

在部分地方进行親吻行為属非法，且可能遭[驱逐出境](../Page/驱逐出境_\(处分\).md "wikilink")。\[19\]\[20\]\[21\]\[22\]\[23\]\[24\]飯店以外的餐廳基本上不能販賣酒精飲料。\[25\]如同其他國家，[醉酒駕駛在杜拜也是違法行為](../Page/醉酒駕駛.md "wikilink")。\[26\]

## 区划

[Dubai_map_city.svg](https://zh.wikipedia.org/wiki/File:Dubai_map_city.svg "fig:Dubai_map_city.svg")
迪拜划分为九个区：一区、二区、三区、四区和六区为市区，七区和九区为郊区，八区是杰贝阿里。每个区位以社区规模与主要公路的界线来划分。目前，迪拜有132个社区\[27\]。

## 经济

杜拜為世界上發展最快的經濟體之一。\[28\]
杜拜生產總值為1,107億美元，2014年經濟成長率為6.1%。\[29\]雖然許多杜拜核心貿易基礎建設係為了支持石油工業，\[30\]
但石油、天然氣佔杜拜營收已低於5%，\[31\]石油產量約每天，\[32\]大多來自海上油田，天然氣佔營收約2%，杜拜石油儲量顯著下降，預期僅能再開採20年。\[33\]房地產及營造業(22.6%)、\[34\]貿易(16%)、[轉口港](../Page/轉口港.md "wikilink")(15%)及金融服務業(11%)為杜拜經濟最主要產業。\[35\]

迪拜，以及交叉于迪拜河的德拉（独立的时候），是西方厂商的重要通商港口。许多的城市新金融中心都设在港区，成为1970年代与1980年代的重要贸易中心。直到1990年代，迪拜市区仍允许[黄金自由贸易](../Page/黄金.md "wikilink")，因印度的黄金进口受限，所以迪拜成為走私黃金到印度的地點。\[36\]

迪拜的石油蕴藏量比[阿布扎比还少百分十几至二十几](../Page/阿布扎比.md "wikilink")，杜拜政府不想倚重不多的石油存量，所以致力使經濟多元化，大力發展商業及刺激公司活動，以增加财政收入来源。迪拜经济从依赖石油向服务、观光业转变，使得杜拜[不动产升值](../Page/不动产.md "wikilink")，建筑业的快速增长，让城市建设经历了前所未有的发展。旅遊業隨著[迪拜樂園及其他主題公園](../Page/迪拜樂園.md "wikilink")、度假勝地、體育場地等旅游基础设施的建造也得到了快速發展。

今日，迪拜不仅成为重要的观光胜地与港口（[杰贝拉里人工港](https://web.archive.org/web/20100827061438/http://www.db1001.cn/uae/94.html)，自1970年代起建设，世界最大的人工港）\[37\]，更是[資訊技术与](../Page/資訊技术.md "wikilink")[金融的产业重镇](../Page/金融.md "wikilink")，并将有[迪拜国际金融中心](../Page/迪拜国际金融中心.md "wikilink")（DIFC）。1985年在迪拜成立的[阿聯酋國際航空](../Page/阿聯酋國際航空.md "wikilink")，坐落于[迪拜国际机场](../Page/迪拜国际机场.md "wikilink")，在2005年搭载了1200万名的旅客。迪拜機場為中東最重要的航空中转站之一。石油经济现在只占迪拜经济的小部分。

### 地产业和建筑业

[Dubai's_Rapid_Growth.ogv](https://zh.wikipedia.org/wiki/File:Dubai's_Rapid_Growth.ogv "fig:Dubai's_Rapid_Growth.ogv")
[谢赫·穆罕默德·本·拉希德·阿勒马克图姆极大渴望藉由](../Page/谢赫·穆罕默德·本·拉希德·阿勒马克图姆.md "wikilink")[摩天大楼来展现城市的超现代化](../Page/摩天大楼.md "wikilink")。從2000年起，杜拜政府開始了大規模的建設與計畫，这不仅提升了杜拜房地产的热度，也使得迪拜成为世界成长最快速的城市之一。在很多地區裡，抬頭往上看，除了映入眼簾的杜拜天空之外還少不了至少一台起重機；業界的專家保守估計全世界的起重機大概有15-25%在杜拜\[38\]。在杜拜與阿酋聯合大公國的建設速度通常都比西方國家要快得多。這可能是因為從印度次大陸來的勞工能夠接受比其他國家還要低的薪水。

2002年，[阿聯酋修改法例](../Page/阿聯酋.md "wikilink")，准許非阿聯國民在杜拜擁有物业（不包含土地），不動產的永久持有權及99年的長期租賃權仍准許售予擁有私人公司的個人。各國[移民大量移居杜拜](../Page/移民.md "wikilink")，帶來鉅額資本並刺激當地的經濟發展，估計光是來自伊朗的移民就在當地投資了2,000億美元以上<small></small>。第一個由非[阿拉伯聯合大公國國民所擁有的不動產](../Page/阿拉伯聯合大公國.md "wikilink")，是由綠地、噴泉和湖所組成、Emmar
Properties設計，稱為Emirates
Hills的別墅社區。杜拜的投資者同時也在世界各地如紐約、倫敦等世界主要城市，大量購入房地產，其中具代表性的一個案例是2005年購入位於紐約市公園大道230號的建築（通常被稱為紐約中心大樓或Helmsley大廈），及中央公園南端的Essex
House。

由于迪拜政府在2018-1-1之前都是免税（之后增加了5%的增值税），杜拜房屋供不應求，租金亦上揚。雖然在酋長謝赫·穆罕默德的命令下，到2006年為止平均薪酬每年都已經調漲15%了。薪資的調漲說明了失控的租金上漲將會斲傷經濟發展。此地的立法工作則詳實反映了這裡的資產市場仍然處於新生階段，而承租人及地主的權利則植基於借鏡於歐洲法規的不穩定原則。多數的契約及承租協定若以國際法標準來看其實站不住腳，而且內容總是過於有利出租人或販售土地資產的公司。
[Emirates_Towers_in_Dubai_at_dawn.jpg](https://zh.wikipedia.org/wiki/File:Emirates_Towers_in_Dubai_at_dawn.jpg "fig:Emirates_Towers_in_Dubai_at_dawn.jpg")

  - 杜拜大規模建設與計畫，主要集中于Mina Seyahi地區和Jebel Ali區。

<!-- end list -->

  - 人工岛：[棕榈岛](../Page/棕櫚群島.md "wikilink")
    (Jumeirah)、[世界群岛](../Page/世界群岛.md "wikilink")
  - [哈里發塔](../Page/哈里發塔.md "wikilink")（原名杜拜塔），於2010年1月4日完工，高度為828公尺，为世界最高的建筑。
  - [迪拜购物中心](../Page/迪拜购物中心.md "wikilink")，正在建造中的世界上最大的购物中心，位于[哈里發塔附近](../Page/哈里發塔.md "wikilink")。
  - [阿联酋大厦](../Page/阿联酋大厦.md "wikilink")：一座54层355米，另一座56层309米。分别为2005年世界第12与第24高的大楼。\[39\]
  - [阿拉伯塔又名帆船酒店](../Page/阿拉伯塔.md "wikilink")（Burj
    Arab），坐落于[波斯湾上自有岛屿](../Page/波斯湾.md "wikilink")，曾经为世界最高级的饭店。
  - [迪拜码头](../Page/迪拜码头.md "wikilink")（Dubai Marina），具有无与伦比城市夜景的高级住宅区。
  - 商业湾（Business Bay），迪拜的新中心商业区，全部完工后，迪拜河畔地区将矗立约500栋摩天大楼。
  - [迪拜园](../Page/迪拜园.md "wikilink")
  - 世贸中心（Dubai World Trade Centre），举行世界级商业展会的中心。
  - 2006年五月一日，當局提出一個名為""的計劃，其中包括投資二百七十億元以提升杜拜飯店客房總數至29,000個，為現有的一倍；其中最大的一個飯店將命名為"[Aisa,
    Asia](../Page/Aisa,_Asia.md "wikilink")"，預計提供超過6,500個客房，屆時將成為全球規模最大的飯店。
  - 迪拜生物科技中心（），意在吸引各大生物科技企业进入，这些企业涉及[制药](../Page/制药.md "wikilink")、医疗、[基因科技甚至](../Page/基因科技.md "wikilink")[生化武器防御](../Page/生化武器防御.md "wikilink")（biodefense）。建立该中心的目的在于加快迪拜在生物科技的发展，并利用迪拜的[区位优势](../Page/区位优势.md "wikilink")，让这一快速发展的产业在此地立足。
  - 迪拜国际媒体城（Dubai Media
    City）是迪拜的另一项工程，它旨在建立一个世界级的媒体网络集中地，涉及印刷、出版、传媒作品以及相关的诸多产业。这项工程于2003年启动，在2006年全部完成。有众多国际知名传媒公司入驻，如[阳狮集团](../Page/阳狮集团.md "wikilink")、[三星集团等跨国企业](../Page/三星集团.md "wikilink")。
  - [迪拜滨水城区](../Page/迪拜滨水城区.md "wikilink")（Dubai
    Waterfront），2005年2月建设宣布动工，
    它有华盛顿特区的两倍半大小，大约为曼哈顿岛的七倍。建成后的迪拜滨水城区将增加500公里的人工滨水区，届时一个岛上布满宾馆和居民区、岛屿与运河交织的混合体将呈现在世人面前，而世界另一个最高建筑Al
    Burj也将被包含其中。

<!-- end list -->

  - 迪拜較大的房產公司：

<!-- end list -->

  - [AlNakheel](../Page/AlNakheel.md "wikilink")，阿联酋最大的房产公司之一。
  - [伊玛尔地产](../Page/伊玛尔地产.md "wikilink")(Emaar Properties)
  - [迪拜地产](../Page/迪拜地产.md "wikilink")（Dubai Properties）

### 商业和金融业

政府成立了许多的[自由贸易区](../Page/自由贸易区.md "wikilink")。

  - [迪拜网络城](../Page/迪拜网络城.md "wikilink")（Dubai Internet City
    ）现在结合了[迪拜媒体城](../Page/迪拜媒体城.md "wikilink")（Dubai
    Media
    City），即TECOM（迪拜科技、电子商务、媒体权威自由区），此内部包含了许多电脑公司如[EMC](../Page/EMC.md "wikilink")、[甲骨文](../Page/甲骨文公司.md "wikilink")、[微软与](../Page/微软.md "wikilink")[IBM](../Page/IBM.md "wikilink")，与一些媒体机构如[中东广播中心](../Page/中东广播中心.md "wikilink")、[CNN](../Page/CNN.md "wikilink")、[路透社](../Page/路透社.md "wikilink")、[ARY](https://web.archive.org/web/20060207032121/http://www.arydigital.tv/index.php)与[美联社](../Page/美联社.md "wikilink")。
  - [迪拜知识村](../Page/迪拜知识村.md "wikilink")（Knowledge
    Village）是座教育训练中心并为补足自由贸易区的其中两群，网络城与媒体城，以其设备来训练两地劳工的未来知识。

2009年11月25日，杜拜官方宣佈杜拜國營事業[杜拜世界](../Page/杜拜世界.md "wikilink")（Dubai
World）將展延償債期限至少半年。此舉造成世界各國震撼，杜拜信用評等也隨之滑落，各國股市更是應聲而倒。\[40\]据《纽约时报》报道，2008年迪拜的经济危机后，许多债务缠身的外籍人士为了避免无法偿还欠款而被监禁，纷纷逃离该国，导致超过3000辆汽车被遗弃在迪拜机场的停车场。有些人将被刷暴的信用卡留在车内，有些人则将道歉字条贴在挡风玻璃上。虽然没有人知道实际情况到底有多糟，但数以百万计的外籍人士因失业而离开迪拜是不争的事实。这导致该国的消费减少，空置的房屋使部分地区犹如鬼城，房地产价格在两、三个月内狂跌至少30%。同时，豪华汽车的售价也比两个月前下跌了40%，原本堵满车辆的道路变得静悄悄。此外，许多大型建设项目都暂停或取消。迪拜每天都取消1500个工作签证，但该国劳工部发言人对此不置可否。迪拜政府不仅拒绝提供有关该国经济情况的资料，甚至还拟定了一个新的媒体法草案，凡是涉嫌破坏国家声誉或经济的媒体，将被罚款100万迪拉姆。若不是[阿布扎比的及时援助](../Page/阿布扎比.md "wikilink")，迪拜可能化为乌有。但是现在迪拜再次恢复了往日的荣光，正在为2020[世界博览会作准备](../Page/世界博览会.md "wikilink")。

## 交通

道路及運輸局（Roads and Transport
Authority，RTA）為杜拜交通的主管機構，於2005年根據皇家法律而成立。\[41\]預期城市於2020年人口將超過350萬，政府投入700億阿聯酋迪拉姆，以改善城市交通。\[42\]杜拜2009年車輛數為1,021,880。\[43\]2010年1月，杜拜居民使用大眾運輸比例約6%。\[44\]

### 航空

[迪拜国际机场](../Page/迪拜国际机场.md "wikilink")（[IATA](../Page/国际航空运输协会机场代码.md "wikilink"):
[DXB](../Page/迪拜国际机场.md "wikilink")）為[阿联酋航空及](../Page/阿联酋航空.md "wikilink")[杜拜航空的樞紐機場](../Page/杜拜航空.md "wikilink")，服務杜拜及其他酋長國，，該機場為[旅客量居世界第7](../Page/全球机场客运吞吐量列表.md "wikilink")，2014年共接待7,040萬人次，也是[国际客量最多的機場之一](../Page/全球机场国际客量列表.md "wikilink")。\[45\]除旅客運輸外，該機場[貨運量居世界第7](../Page/全球机场货运量列表.md "wikilink")，2014年處理量達237萬噸。\[46\][阿联酋航空為杜拜國有航空公司](../Page/阿联酋航空.md "wikilink")，於2014年，該公司航點遍布六大洲，於超過70個國家，通航142個城市。\[47\]

### 地鐵

[杜拜地鐵耗資](../Page/杜拜地鐵.md "wikilink")38.9億美元，由紅線及綠線兩條路線組成，行經商業區及住宅區，於2009年9月啟用。\[48\]杜拜地鐵為[阿拉伯半岛第一個開通的城市軌道交通系統](../Page/阿拉伯半岛.md "wikilink")。\[49\]

## 争议

### 环境

迪拜没有水资源，降雨量非常稀薄，于是进行[海水淡化](../Page/海水淡化.md "wikilink")，生产淡水同时排出大量的二氧化碳，加上該地奢華的生活方式，导致迪拜平均[碳足迹世界最高](../Page/碳足迹.md "wikilink")，甚至超过美国人的两倍以上。城市废水经常不作处理就排入大海。

### 人权

[阿联酋宪法第](../Page/阿联酋.md "wikilink")25条提到要公平对待各宗教、残疾人、民族，然而據人權團體觀察，外籍劳工（多来自孟加拉、巴基斯坦、印度等地）生活却相当不人道，\[50\]\[51\]\[52\]\[53\]他们处于严密监视之下，没有组织工会权力，也没有机会成为[阿联酋公民](../Page/阿联酋公民.md "wikilink")。

迪拜没有破产概念，当一个人债务缠身而又无力偿还时就得去坐牢。即使辞职雇主必须通知银行此时有未偿还的债务而数额又超过帐面储蓄额，所有财产都会被冻结，而他也不能离开迪拜。
迪拜生活着超过200个不同国籍、不同宗教信仰的人，虽然是穆斯林国家，但十分尊重不同国籍、信仰之间的差异。比如在基督徒的圣诞节、印度的[排灯节](../Page/排灯节.md "wikilink")、中国人的農曆新年时，各大娱乐景点、商场都会发出对该民族的祝福，同时举行各种盛典。哈利法塔也时常会准备精彩的灯光秀，对一些历史上具有影响力的人物表达敬意，或是呼吁对疾病的关注如乳腺癌[粉红丝带运动](../Page/粉红丝带.md "wikilink")。迪拜政府在[穆罕默德·本·拉希德·阿勒马克图姆的](../Page/穆罕默德·本·拉希德·阿勒马克图姆.md "wikilink")“愿景2021”下，旨在于2021年前将阿联酋建设为“世界上最好的国家之一。”

## 人口

根据迪拜统计中心所进行的人口普查，截至2009年，迪拜酋长国的人口总数为1,771,000人，包括1,370,000名男性和401,000名女性，\[54\]截至1998年，总人口中土生土长的阿联酋人仅占17％，至2013年約佔15%。\[55\]迪拜的人口结构与其它阿联酋的酋长国不同的在于它的外籍人士比例相当大，許多外籍人士長久生活於杜拜，甚至在杜拜出生，\[56\]\[57\]主要来自于[南亚与](../Page/南亚.md "wikilink")[东南亚](../Page/东南亚.md "wikilink")。从报告中，约莫四分之一的人口是从它的近邻[伊朗所迁入的](../Page/伊朗.md "wikilink")。\[58\]約85%的外籍人士為亞洲人（約佔杜拜總人口71%），主要族群包括印度人（51%）、巴基斯坦人（16%）、孟加拉人（9%）、菲律賓人（3%），另外約有3萬名索馬利亞人及其他較小的族群。\[59\]迪拜也是大约十万英国人与其他西方人的移民之地。阿联酋政府并不总是允许永久移民、以及放弃国籍。

### 语言

[阿拉伯语為國家及官方语言](../Page/阿拉伯语.md "wikilink")，為阿拉伯聯合大公國本地區民使用，\[60\][英语則做為第二語言](../Page/英语.md "wikilink")，另外，移民使用的語言包括[马拉雅拉姆语](../Page/马拉雅拉姆语.md "wikilink")、[印地语](../Page/印地语.md "wikilink")-[乌尔都语](../Page/乌尔都语.md "wikilink")（或[印度斯坦语](../Page/印度斯坦语.md "wikilink")）、[古吉拉特语](../Page/古吉拉特语.md "wikilink")、[波斯语](../Page/波斯语.md "wikilink")、[信德语](../Page/信德语.md "wikilink")、[泰米尔语](../Page/泰米尔语.md "wikilink")、[旁遮普語](../Page/旁遮普語.md "wikilink")、[普什图语](../Page/普什图语.md "wikilink")、[孟加拉语](../Page/孟加拉语.md "wikilink")、[俾路支语](../Page/俾路支语.md "wikilink")、、\[61\][卡纳达语](../Page/卡纳达语.md "wikilink")、[僧伽罗语](../Page/僧伽罗语.md "wikilink")、[马拉地语](../Page/马拉地语.md "wikilink")、[泰卢固语](../Page/泰卢固语.md "wikilink")、[他加祿語及](../Page/他加祿語.md "wikilink")[汉语等](../Page/汉语.md "wikilink")。\[62\]。

### 宗教

[伊斯兰教是所有酋长国的官方](../Page/伊斯兰教.md "wikilink")[宗教](../Page/宗教.md "wikilink")，主要的教派为[逊尼派](../Page/逊尼派.md "wikilink")。不少外籍移工為穆斯林。这里也有少数的[印度教徒](../Page/印度教.md "wikilink")、[锡克教徒](../Page/锡克教.md "wikilink")、[基督徒及](../Page/基督徒.md "wikilink")[佛教徒](../Page/佛教.md "wikilink")。\[63\]迪拜也是所有酋长国内唯一有印度教、锡克教庙宇的地方。

迪拜城内的米娜市集有[印度教](../Page/印度教.md "wikilink")[湿婆与](../Page/湿婆.md "wikilink")[奎师那的庙宇](../Page/奎师那.md "wikilink")，它们的建立相信是受到迪拜前任领导者，谢赫·马克图姆·本·拉希德·阿勒马克图姆得允许。这里也有专属印度移民的电动[火葬场](../Page/火葬.md "wikilink")。非[穆斯林者可自由信仰他们的宗教](../Page/穆斯林.md "wikilink")，但不得以公开[传教或散布其书籍](../Page/传教.md "wikilink")。政府对于「非穆斯林」以及其他宗教的传教士其实是很宽容的，很少干预「非穆斯林」的宗教活动。

2001年初，政府在杰贝阿里为4个[基督新教与](../Page/基督新教.md "wikilink")1个[天主教教会所捐助的教堂动土施工](../Page/天主教.md "wikilink")，而第一个东正教教堂已于2005年底开始建造，阿联酋[东正教的成员在之前必须与其他基督派别共同使用教堂](../Page/东正教.md "wikilink")，直到谢赫·穆罕默德·本·拉希德·阿勒马克图姆在杰贝阿里捐赠了一小块的土地。

除了捐出土地兴建教堂等宗教设施，政府不会补贴非伊斯兰的团体、墓地等等，政府補貼約95%的清真寺，僅約5%完全為私人籌建。\[64\]不过，該等團體可以从海外筹集资金，从而得到财政支持，政府亦允许基督教会公开在报纸上刊登具纪念性质的教会活动。

## 文化

杜拜文化主要受伊斯蘭文化及阿拉伯文化影響，如建築、音樂、服裝、美食和生活方式等方面。隨著觀光需求、創業發展對生活品質的要求，杜拜文化逐漸朝奢華、富裕、重視休閒演變。\[65\]\[66\]\[67\]年度娛樂活動包括（DSF）及\[68\]杜拜夏季驚喜（Dubai
Summer Surprises，DSS），活動吸引約400萬遊客造訪，並創造超過27億美元營收。\[69\]\[70\]

## 国际交流

### 友好城市

  - 西班牙[加泰罗尼亚](../Page/加泰罗尼亚.md "wikilink")[巴塞罗那省](../Page/巴塞罗那省.md "wikilink")[巴塞罗那](../Page/巴塞罗那.md "wikilink")(2006)\[71\]

  - 南韓[釜山廣域市](../Page/釜山廣域市.md "wikilink")(2006)\[72\]

  - 美國[密歇根州](../Page/密歇根州.md "wikilink")[底特律](../Page/底特律.md "wikilink")(2003)\[73\]

  - 德國[黑森](../Page/黑森.md "wikilink")[法蘭克福](../Page/法蘭克福.md "wikilink")(2005)\[74\]

  - 澳大利亞[昆士蘭州](../Page/昆士蘭州.md "wikilink")[黄金海岸](../Page/黄金海岸_\(澳大利亚\).md "wikilink")(2001)\[75\]

  - 中國[上海市](../Page/上海市.md "wikilink")(2009)\[76\]

  - 土耳其[伊斯坦堡](../Page/伊斯坦堡.md "wikilink")(1997)\[77\]

  - 印度尼西亞[巴東](../Page/巴東_\(西蘇門答臘省\).md "wikilink")(2015)\[78\]

  - 馬來西亞[吉隆坡](../Page/吉隆坡.md "wikilink")(2010)\[79\]

  - 中國[深圳市](../Page/深圳市.md "wikilink")(2018)\[80\]

## 參見

  - [擁有最多摩天大樓的城市列表](../Page/擁有最多摩天大樓的城市列表.md "wikilink")

## 参考文献

## 外部链接

  -
  - \[//maps.google.com/maps?q=Dubai 谷歌地圖\]

  - [維客旅行上的](../Page/維客旅行.md "wikilink")[杜拜](http://wikitravel.org/en/Dubai)

[杜拜](../Category/杜拜.md "wikilink")
[Category:阿拉伯聯合大公國行政區劃](../Category/阿拉伯聯合大公國行政區劃.md "wikilink")

1.

2.

3.

4.

5.  [Oil share dips in Dubai GDP](http://www.ameinfo.com/122863.html)
    ** (9 June 2007) Retrieved on 15 October 2007.

6.  [Dubai economy set to treble
    by 2015](http://www.arabianbusiness.com/dubai-economy-set-treble-by-2015-149721.html)
    *ArabianBusiness.com* (3 February 2007) Retrieved on 15 October
    2007.

7.

8.

9.

10.

11.

12.

13. ["Dubayy"](http://concise.britannica.com/dday/print?articleId=31319&fullArticle=true&tocId=9031319).
    Encyclopædia Britannica. 2008

14.

15.

16.

17.

18. US Library of Congress – Legislative Branches

19.

20.

21.

22.

23.

24. <http://english.alarabiya.net/en/perspective/features/2014/05/04/Nose-kiss-anyone-How-the-Gulf-Arab-greeting-has-evolved.html>

25.

26.

27. [迪拜行政边界](http://www.gis.gov.ae/en/content.asp?DocID=115&Cat=18&npage=3)
    ，迪拜地理信息系统中心

28.

29.

30.

31.
32.

33.

34.
35. [Prospects of Dubai Economic
    Sectors](http://www.dcci.gov.ae/content/Bulletin/Issue10/SectorMonEn_ISSUE10.pdf)
    . Dubai Chamber of Commerce. 2003

36.
37.

38. [Burj cranes of
    Dubai](http://www.gulfnews.com/nation/Society/10039528.html), by
    Emmanuelle Landais, *Gulf News*, May 13, 2006

39. \[<http://news.enquirer.com/apps/pbcs.dll/article?AID=/20050204/BIZ/502040340/1001>“
    迪拜建设大型计划”\]，Jim Krane，*The Enquirer*，2005年2月4日

40.

41. <http://www.rta.ae>

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57.

58. *"[年轻的伊朗人到迪拜追寻梦想](http://select.nytimes.com/gst/abstract.html?res=F00E1EF839550C778CDDAB0994DD404482)"*
    [纽约时报](../Page/纽约时报.md "wikilink")，HASSAN M. FATTAH作。出版：2005年12月4日

59. ["Country and Metropolitan Stats in
    Brief](http://www.migrationinformation.org/dataHub/GCMM/Dubaidatasheet.pdf).
    MPI Data Hub

60.

61.

62.

63. [Religion in
    Dubai](http://www.dubaidreams.net/465/about/religion-in-dubai/) .
    Dubaidreams

64. [Religion in
    Dubai](http://www.dubaidreams.net/465/about/religion-in-dubai/) .
    Dubaidreams

65. Luxury Fashion Branding: Trends, Tactics, Techniques – Page 80, Uché
    Okonkwo – 2007

66. Dubai – Page 100, Terry Carter – 2009

67. Introduction to Sociology – Page 14, George Ritzer – 2012

68. [Dubai Shopping
    Festival 2011](http://www.traveldealsfinder.com/travel-packages/dubai-shopping-festival)
    More Details

69. [DSF
    Milestones](http://www.dubaicityguide.com/site/dsf/milestones.asp).
    Dubaicityguide

70.

71. [Dubai is sister city to Barcelona, on the municipality's website
    (in
    Catalan)](https://web.archive.org/web/20090216085914/http://w3.bcn.es/XMLServeis/XMLHomeLinkPl/0%2C4022%2C229724149_257215678_1%2C00.html),
    retrieved on 28 October 2015.

72. [Dubai and Busan are sister cities, on the municipality's
    website](http://english.busan.go.kr/SubPage.do?pageid=sub020927&pagecode=sub020927_01)
    , 13 November 2006.

73. [Sister Cities Agreement between Detroit and
    Dubai](http://www.sister-cities.org/sites/default/files/Detroit%2C%20MI-Dubai%2C%20United%20Arab%20Emirates.pdf),
    28 September 2003.

74.

75. [Dubai and Gold Coast are sister cities, on the municipality's
    website](http://www.goldcoast.qld.gov.au/business/sister-cities-international-partnerships-15053.html).
    Retrieved on 28 October 2015.

76. [Dubai is sister city to Shanghai, on the municipality's
    site](http://www.shfao.gov.cn/wsb/english/Sister_Cities/u1a14233.html),
    27 July 2009.

77. [Dubai and Istanbul are sister cities, on the municipality's
    website](http://www.ibb.gov.tr/tr-TR/kurumsal/Birimler/DisIliskilerMd/Documents/guncelleme08032010/kardes_sehir_isbirligi_ve_iyi_niyet_anlasmasi_imzalanan_seh.pdf)
    (in Turkish), 22 March 1997.

78.

79.

80.