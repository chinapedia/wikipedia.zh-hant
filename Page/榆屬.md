**榆樹**，是[榆科下](../Page/榆科.md "wikilink")**榆屬**植物的統稱，主要在[北半球的](../Page/北半球.md "wikilink")[溫帶地區生長](../Page/溫帶.md "wikilink")。一般高約25米，樹皮粗糙。具高度實用、葯用及食用價值。

榆樹的葉呈[橢圓形或橢圓狀波針形](../Page/橢圓形.md "wikilink")，葉長2-8厘米、寬1.5-2.5厘米，兩面葉面無毛，或背面脈腋有毛。葉側脈有9-16對，葉緣有單鋸齒，很少有重鋸齒。葉柄長約2-10毫米。

榆樹在早春發葉前先開花，花呈簇狀生成聚傘花序，花被鐘形，開4-5花瓣，每朵花有雄蕊約4-5條。翅果近圓形或寬倒卵形，長約1.3-1.5厘米，果皮表面無毛，頂端凹缺。內藏種子，近翅果中部，很少接近凹缺處；果柄長約2毫米。

## 品種

由於各種榆樹易於[雜交](../Page/雜交.md "wikilink")，有些地區以人工方式繁殖了很多特別的品種，使榆樹品種的界定变得較为困難。一般来讲，榆樹可分為約20-45個品種，視乎其界定方法之异同。

### 分類

主要有如下品種：

  - [美國榆](../Page/美國榆.md "wikilink")
  - [黑榆](../Page/黑榆.md "wikilink")
  - [春榆](../Page/春榆.md "wikilink")
  - [歐洲白榆](../Page/歐洲白榆.md "wikilink")
  - [榔榆](../Page/榔榆.md "wikilink")
  - [垂枝榆](../Page/垂枝榆.md "wikilink")

[Elm_in_beijing.JPG](https://zh.wikipedia.org/wiki/File:Elm_in_beijing.JPG "fig:Elm_in_beijing.JPG")称为榆荚，因为圆小似[铜钱](../Page/铜钱.md "wikilink")，又称榆钱|170px\]\]
[Wych_elm_flower.jpg](https://zh.wikipedia.org/wiki/File:Wych_elm_flower.jpg "fig:Wych_elm_flower.jpg")的花\]\]

## 種植

榆樹是一種[溫帶植物](../Page/溫帶植物.md "wikilink")，生命力強，較為耐寒，適合於肥沃的沙壤土上生長，生長速度快。榆樹為中國北方重要綠化樹木，亦常見於民居村落前後。亦可培植成[盆景](../Page/盆景.md "wikilink")。

榆樹花期約在3月上旬，果熟期在4月上旬。

榆樹成熟果實可隨採隨播，亦可使用[播種或](../Page/播種.md "wikilink")[插枝等人工繁殖方法栽種](../Page/插枝.md "wikilink")。

### 荷蘭榆樹病

荷蘭榆樹病曾經廣泛摧毀全[歐洲及](../Page/歐洲.md "wikilink")[北美洲的榆樹](../Page/北美洲.md "wikilink")。這植物疾病的致病原是一種叫做(Ophiostoma
nova-ulmi)的[霉菌](../Page/霉菌.md "wikilink")。透過兩種生活在榆樹樹幹的榆小蠹作[媒介傳播](../Page/媒介.md "wikilink")。霉菌被媒介帶進榆樹表面的傷口，再進入樹幹輸送水份營養往樹葉和樹端的篩管內繁殖。受感染的榆樹能在短短3個星期之內被切斷其水份傳輸系統，導致死亡。有研究顯示，亞洲品種的榆樹有抗致病霉菌的[基因](../Page/基因.md "wikilink")。

## 用途

### 木材

榆樹木材有連續相扣的木紋，品質堅直，可供建房、製傢具及農具使用。在西方，榆木亦用来制作[棺材](../Page/棺材.md "wikilink")。

### 食用價值

[炒榆钱.jpg](https://zh.wikipedia.org/wiki/File:炒榆钱.jpg "fig:炒榆钱.jpg")
榆樹的嫩果和幼葉可以食用或作飼料，中国北方有些地方也会将榆树皮磨成粉制成各种面食。有文獻記載，在19世紀中期[挪威大](../Page/挪威.md "wikilink")[饑荒時](../Page/饑荒.md "wikilink")，挪威農民以水煮熟榆樹樹幹來充饑。

### 其他用途

堅韌的榆樹皮可以製成[繩索](../Page/繩索.md "wikilink")。

榆樹也是抗[有毒氣體如](../Page/有毒氣體.md "wikilink")[二氧化碳及](../Page/二氧化碳.md "wikilink")[氯氣的樹種](../Page/氯氣.md "wikilink")。其內含beta-固淄醇、植物醇、豆淄醇等多種[淄醇及](../Page/淄醇.md "wikilink")[韖質](../Page/韖質.md "wikilink")、[樹膠及](../Page/樹膠.md "wikilink")[脂肪油](../Page/脂肪油.md "wikilink")。

在[中世紀時代的歐洲](../Page/中世紀.md "wikilink")，榆木被製成[水管](../Page/水管.md "wikilink")，榆木水管在長期濕潤的情況下亦不會腐化。

## 榆樹遷播史

自18世紀至20世紀早期，榆樹在[歐洲和北美洲被廣泛種植](../Page/歐洲.md "wikilink")，作為街道裝飾樹種。由於榆樹樹幹高，樹冠伸展闊，可以形成一列「榆樹隧道」的感覺。

在歐洲，[無毛榆](../Page/無毛榆.md "wikilink") (Wych Elm，即*U.
glabra*)和[歐洲光葉榆](../Page/歐洲光葉榆.md "wikilink") (Smooth-leaved
Elm，即*U.
minor*)都是這些被廣植的品種，其中無毛榆常見於[北歐](../Page/北歐.md "wikilink")[斯堪的那維亞半島地區](../Page/斯堪的那維亞半島.md "wikilink")、[英國北部](../Page/英國.md "wikilink")，而滑葉榆則常見於較南的地區。而因該兩種榆樹品種自然雜交而生的[荷蘭榆](../Page/荷蘭榆.md "wikilink")
(Dutch Elm，即*U. × hollandica*)亦常見於歐洲。

北美洲常見的榆樹品種稱為[美洲榆](../Page/美洲榆.md "wikilink") (American Elm，即*U.
americana*)，擁有生長速度快，對不同[氣候和](../Page/氣候.md "wikilink")[土壤適應力強](../Page/土壤.md "wikilink")，堅直的樹幹能经受強風吹襲，及樹型瓶狀不用時常修剪的優點，故成為街道裝飾樹種的首選。現存生長在[紐約市](../Page/紐約市.md "wikilink")[華盛頓廣場公園西北角的](../Page/華盛頓廣場公園.md "wikilink")「吊人榆」(Hangman's
Elm)，是一棵估計約在1679年開始生長的古老英格蘭榆樹(English Elm)。

自1850年至1920年間，最知名的榆樹品種為[蘇格蘭榆](../Page/蘇格蘭榆.md "wikilink")，為一種經培育變種而來的「變種無毛榆」嫁接到純種無毛榆的樹幹而成，其樹型被改造成類似噴泉狀，在当時的大花園里都採用這種榆樹品種作裝飾樹種。

在20世紀，英國殖民者開發[澳大利亞](../Page/澳大利亞.md "wikilink")，他們帶了大量[英格蘭榆](../Page/英格蘭榆.md "wikilink")(English
Elm，即*U. minor var.
vulgaris*)的樹苗到那裡種植，這些「外來」品種在澳大利亞落地生根，並由於地緣關係，避過了曾經在歐洲發生的荷蘭榆樹病。現在[墨爾本的街道仍能發現英格蘭榆樹巍然屹立](../Page/墨爾本.md "wikilink")。

## 外部連結

  - [榆樹,
    Yushu](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00844)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[Category:榆属](../Category/榆属.md "wikilink")