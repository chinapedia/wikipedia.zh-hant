****（Minerva）是一顆位于[小行星帶的](../Page/小行星帶.md "wikilink")[小行星](../Page/小行星.md "wikilink")，編號93，由[詹姆斯·克雷格·沃森于](../Page/詹姆斯·克雷格·沃森.md "wikilink")1867年8月24日发现。的[直径为](../Page/直径.md "wikilink")141.0千米，[质量为](../Page/质量.md "wikilink")2.9×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1669.541天。

## 參考文獻

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1867年发现的小行星](../Category/1867年发现的小行星.md "wikilink")