**桃園天皇**（），日本第116代天皇，於[1747年](../Page/1747年.md "wikilink")[6月9日](../Page/6月9日.md "wikilink")
-
[1762年](../Page/1762年.md "wikilink")[8月31日間在位](../Page/8月31日.md "wikilink")。

幼名八穗宮（やほのみや），後為茶地宮（さちのみや），名諱**遐仁**（とおひと）。

## 簡歷

[1747年](../Page/1747年.md "wikilink")（延享4年）被立為太子，同年，接受父親[櫻町天皇讓位](../Page/櫻町天皇.md "wikilink")，三年後上皇過世。桃園天皇在位期間，曾於寶曆八年（1758年）發生過所謂的「」，因為這件事，許多公卿遭到處份。

[1762年](../Page/1762年.md "wikilink")，於22歲時駕崩，葬於[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[東山區今熊野泉山町](../Page/東山區_\(日本\).md "wikilink")[泉涌寺内的](../Page/泉涌寺.md "wikilink")[月輪陵](../Page/月輪陵.md "wikilink")。

## 世系圖

[櫻町天皇第一皇子](../Page/櫻町天皇.md "wikilink")，生母是[權大納言姉小路實武的女兒](../Page/權大納言.md "wikilink")，典侍[姉小路定子](../Page/姉小路定子.md "wikilink")（開明門院）。五歲時由櫻町天皇嫡妻，即女御[二條舍子](../Page/二條舍子.md "wikilink")（青綺門院）收為嫡子。

### 后妃

  - 女御：[一條富子](../Page/一條富子.md "wikilink")（恭礼門院）（1743-1795）
      - 第一皇子：英仁親王（[後桃園天皇](../Page/後桃園天皇.md "wikilink")）（1758-1779）
      - 第二皇子：[伏見宮第十七代](../Page/伏見宮.md "wikilink")[貞行親王](../Page/貞行親王.md "wikilink")（1760-1772）

### 譜系圖

## 在位期間年號

  - [延享](../Page/延享.md "wikilink")：（[1744年](../Page/1744年.md "wikilink")2月21日）
    - [1748年](../Page/1748年.md "wikilink")7月12日
  - [寬延](../Page/寬延.md "wikilink")：[1748年](../Page/1748年.md "wikilink")7月12日
    - [1751年](../Page/1751年.md "wikilink")10月27日
  - [寶曆](../Page/宝历_\(桃园天皇\).md "wikilink")： [1751年](../Page/1751年.md "wikilink")10月27日
    - （[1764年](../Page/1764年.md "wikilink")6月2日）

[Category:江戶時代天皇](../Category/江戶時代天皇.md "wikilink")