**恩波里亞**（Emporia,
Virginia）是[美國](../Page/美國.md "wikilink")[維吉尼亞州南部的一個獨立城市](../Page/維吉尼亞州.md "wikilink")、[格林斯維爾縣縣治](../Page/格林斯維爾縣_\(維吉尼亞州\).md "wikilink")。面積18.1平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口5,665人。

恩波里亞成立於1967年7月31日。城名是[拉丁語](../Page/拉丁語.md "wikilink")「貿易場所」的意思。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[E](../Category/弗吉尼亚州城市.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.