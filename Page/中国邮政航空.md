**中國郵政航空**（）是一家总部设于[北京的貨運航空公司](../Page/北京.md "wikilink")，成立於1996年11月25日，並於1997年2月27日正式投入運營。采用“全夜航”集散模式，以[南京禄口国际机场为](../Page/南京禄口国际机场.md "wikilink")[枢纽机场](../Page/枢纽机场.md "wikilink")，[上海](../Page/上海.md "wikilink")、[武汉为辅助中心](../Page/武汉.md "wikilink")，主要為[中國郵政運送郵件及貨物](../Page/中國郵政.md "wikilink")。中國郵政航空中國郵政佔有51%股權，而[中國南方航空佔有](../Page/中國南方航空.md "wikilink")49%股權。2006年開始經營國際航線，開通[韓國及](../Page/韓國.md "wikilink")[日本的航線](../Page/日本.md "wikilink")。

2008年9月由[中国邮政回购](../Page/国家邮政局.md "wikilink")[南方航空股份](../Page/南方航空.md "wikilink")，改由[中国邮政集团公司独资控股](../Page/中国邮政.md "wikilink")。

## 企業文化

[缩略图](https://zh.wikipedia.org/wiki/File:Boeing_737-45R\(SF\),_China_Postal_Airlines_JP7359488.jpg "fig:缩略图")

目標：持續安全、持續發展、持續效益、持續和諧

願景：致力於成為快遞、物流服務的首選提供商

使命：為客戶提供“迅速、準確、安全、方便”的最佳快遞物流服務

精神：安全、創新、包容、奉獻

## 機隊

**中国邮政航空机队**\[1\] 截止至2017年9月，中国邮政航空共拥有飞机28架。

| 机型                                            | 数量 | 注册号(B)                                                                |
| --------------------------------------------- | -- | --------------------------------------------------------------------- |
| [波音 B737-300F](../Page/波音737.md "wikilink")   | 14 | 2526 2527 2528 2655 2656 2661 2662 2961 2962 2968 2996 5065 5071 5072 |
| [波音 B737-400F](../Page/波音737.md "wikilink")   | 8  | 2135 2513 2525 2881 2882 2887 2891 2892                               |
| [波音 B757-200PCF](../Page/波音757.md "wikilink") | 6  | 2823 2824 2825 2827 2831 2835                                         |
| 总计                                            | 28 |                                                                       |

## 航点

## 参考文献

1\.
[公司簡介](http://www.cnpostair.com/gk.asp?xwClass=%B9%AB%CB%BE%BC%F2%BD%E9)

## 参见

  - [中国邮政](../Page/中国邮政.md "wikilink")

{{-}}

[Category:总部位于北京市的中华人民共和国国有企业](../Category/总部位于北京市的中华人民共和国国有企业.md "wikilink")
[Category:中国邮政航空](../Category/中国邮政航空.md "wikilink")
[Category:货运航空公司](../Category/货运航空公司.md "wikilink")
[Category:1996年成立的航空公司](../Category/1996年成立的航空公司.md "wikilink")

1.  [China Postal Airlines
    Fleet](http://www.ch-aviation.ch/portal/airline.php?cha=CYZ)
    *ch-aviation.ch*