**开滦（集团）有限责任公司**，簡稱**開滦（集團）**，以及**開滦集團**（****），是[中華人民共和國内的一家以礦業等行業爲主的大型國有独资公司](../Page/中華人民共和國.md "wikilink")。

## 历史

开滦集团公司的前身为[开滦矿务局下属的煤矿](../Page/开滦矿务局.md "wikilink")，是[清末](../Page/清.md "wikilink")[洋务运动中](../Page/洋务运动.md "wikilink")，[北洋通商事务大臣](../Page/北洋.md "wikilink")[李鸿章兴办的重要实业项目之一](../Page/李鸿章.md "wikilink")。成立于1878年。兴办的目的主要是为了解决[中國南方地区](../Page/中國.md "wikilink")[工业化的快速发展而带来的](../Page/工业化.md "wikilink")[煤炭紧缺问题](../Page/煤炭.md "wikilink")。

开平煤矿在[八國聯軍之役被俄軍佔領](../Page/八國聯軍之役.md "wikilink")，開平礦務局總辦張翼擔心煤礦被沒收作為戰爭賠款，1900年未經清廷准許將開平礦務局資產賣給英國公司，使中國權利遭受重大損失。後來擔任美国总统的[赫伯特·胡佛也參與了此事](../Page/赫伯特·胡佛.md "wikilink")。\[1\]\[2\]\[3\]\[4\]

1948年12月12日[中國人民解放軍进驻唐山后](../Page/中國人民解放軍.md "wikilink")，唐山市军管会派军代表进驻；1952年被收归[国有中央企业](../Page/中國中央企業.md "wikilink")，1998年8月下放[河北省管理](../Page/河北省.md "wikilink")。1999年实行了国有独资公司制，更名为**开滦（集团）有限责任公司**。

由于开滦煤矿的发展，矿区周围逐渐出现了大量的工业企业，并进而形成了今天人口过百万的特大城市[唐山市](../Page/唐山市.md "wikilink")。而开滦建造的[秦皇岛港](../Page/秦皇岛港.md "wikilink")，今天也已成为了世界最大的[能源储运港和一座大型城市](../Page/能源.md "wikilink")。2010年收入933億人民幣。

## 经营范围

美國《財富雜誌》全球500強企業2012年将其排名為490位\[5\]，現時業除了經營國內煤炭开采、洗选加工、煤质化验外，還包括储运、住宿、餐饮、港口装卸及相关服务、銷售煤炭产品、矾土产品、建筑材料、机械设备及配件的制造、销售、安装与修理等\[6\]。而旗下[开滦能源化工股份有限公司](../Page/开滦能源化工股份有限公司.md "wikilink")，則在2004年6月2日分拆於[上海證券交易所上市](../Page/上海證券交易所.md "wikilink")。

## 参考资料

## 参见

  - [原開灤礦務局大樓](../Page/原開灤礦務局大樓.md "wikilink")

## 外部链接

  - [胡佛总统厚积薄发在中国创业发财](https://web.archive.org/web/20080426081355/http://www.youthcy.com/Html/gushi22/2006-12/14/1491634H7.html)
  - 吉井文美：〈[日中战争下的开滦煤矿](http://www.nssd.org/articles/article_read.aspx?id=48277091)〉。

[Category:中華人民共和國国有企业](../Category/中華人民共和國国有企业.md "wikilink")
[Category:洋務運動](../Category/洋務運動.md "wikilink")
[Category:煤炭公司](../Category/煤炭公司.md "wikilink")
[Category:河北公司](../Category/河北公司.md "wikilink")
[Category:1878年成立的公司](../Category/1878年成立的公司.md "wikilink")
[Category:河北省人民政府国有资产监督管理委员会监管企业](../Category/河北省人民政府国有资产监督管理委员会监管企业.md "wikilink")

1.  <http://news.sina.com.cn/c/2004-05-10/00213191970.shtml> 几行字 卖掉开平煤矿
2.  [旧开滦的经理层设置及任职（二）](http://www.kailuan.com.cn/shownr.aspx?id=515)
3.
4.
5.  [开滦集团进入世界500强](http://www.kailuan.com.cn/wbwzgl/files/2012/7/1_2012710100029.shtm)
6.  [開滦（集團）有限責任公司簡介](http://www.kailuan.com.cn/gsjj.aspx)