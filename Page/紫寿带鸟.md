**紫壽帶鳥**（[學名](../Page/學名.md "wikilink")：）又名**-{zh-cn:紫绶带;zh-tw:紫壽帶鳥;}-**，是中型的[雀形目](../Page/雀形目.md "wikilink")[王鶲科](../Page/王鶲科.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")。

紫壽帶鳥是候鳥，於[日本](../Page/日本.md "wikilink")、[南韓](../Page/南韓.md "wikilink")、[台灣及](../Page/台灣.md "wikilink")[菲律賓北部的陰涼闊葉森林中繁殖](../Page/菲律賓.md "wikilink")。牠們遷徙經過亦會在[中國東部](../Page/中國.md "wikilink")、[香港](../Page/香港.md "wikilink")，南遷至[泰國](../Page/泰國.md "wikilink")、[老撾](../Page/老撾.md "wikilink")、[越南](../Page/越南.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡及](../Page/新加坡.md "wikilink")[蘇門答臘等地越冬](../Page/蘇門答臘.md "wikilink")。

紫壽帶鳥其下有三個[亞種](../Page/亞種.md "wikilink")：**紫壽帶鳥**（[指名亞種](../Page/指名亞種.md "wikilink")，*T.
a. atrocaudata*）棲息在日本及南韓的大部份地區；**琉球綬帶**（西北太平洋亞種，*T. a.
illex*）分布於[琉球群島和部分](../Page/琉球群島.md "wikilink")[東南亞地區](../Page/東南亞.md "wikilink")；**黑壽帶鳥**（南方亞種，*T.
a.
periophthalmica*）則僅見於台灣東南的[蘭嶼與](../Page/蘭嶼.md "wikilink")[菲律賓的北部和西部](../Page/菲律賓.md "wikilink")。

紫壽帶鳥的外觀像[壽帶鳥](../Page/壽帶鳥.md "wikilink")，但較為細小。成年雄鳥[胸部的](../Page/胸部.md "wikilink")[羽毛呈黑灰色](../Page/羽毛.md "wikilink")，有紫藍色的光澤，下身則是白色。紫壽帶鳥胸口的深色區域和腹部白色區域的界限明顯，而壽帶鳥下體深色區域和白色區域的界限較為模糊。翼、[背部及](../Page/背部.md "wikilink")[臀部呈黑栗色](../Page/臀部.md "wikilink")。尾巴中央有極長的黑色尾羽，雄雛鳥的此尾羽則較短。牠們不像壽帶鳥，並沒有白色的色型。雌鳥像雄鳥，但胸部呈較深的褐色。牠們的腳呈黑色，有一對大[眼睛](../Page/眼睛.md "wikilink")，及細小藍色的鳥喙。

紫壽帶鳥的叫聲像「[月](../Page/月.md "wikilink")－[日](../Page/日.md "wikilink")－[星](../Page/星.md "wikilink")」的[日文發音](../Page/日文.md "wikilink")，故牠們在日本亦被稱為「三光鳥」。

近期的研究發現紫壽帶鳥急遽減少，這可能是由於森林的消失及[冬天棲息地的退化所致](../Page/冬天.md "wikilink")。

## 參考

  - *Birds of East Asian*, Mark Brazil, 2007 (in press).
  - *Sibagu: Bird Names in Oriental Languages*
    (http://www.sibagu.com/china/monarchidae.html) (accessed 24 November
    2014)

[Category:壽帶屬](../Category/壽帶屬.md "wikilink")