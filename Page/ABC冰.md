[Ais_Kacang.jpg](https://zh.wikipedia.org/wiki/File:Ais_Kacang.jpg "fig:Ais_Kacang.jpg")

**ABC冰**（**A**ir **B**atu **C**ampur、Ais Kacang），又名**娘惹紅豆冰**
(在[东南亚](../Page/东南亚.md "wikilink")，[闽南语或](../Page/闽南语.md "wikilink")[福建话称为](../Page/福建话.md "wikilink")**红豆霜**（[閩南語](../Page/閩南語.md "wikilink")：Ang
Dao
Nseng，即[红豆](../Page/红豆.md "wikilink")[刨冰之意](../Page/刨冰.md "wikilink")），是[馬來西亞和](../Page/馬來西亞.md "wikilink")[新加坡的一種廣受歡迎的](../Page/新加坡.md "wikilink")[刨冰](../Page/刨冰.md "wikilink")[甜品](../Page/甜品.md "wikilink")。它更被馬來西亞官方譽為「國民甜品」\[1\]

## 釋義

於其馬來語名稱中，**Air Batu** 意指「冰」而 **Campur** 則有「混合」的意思。

至於其另外的名稱：**Ais Kacang**，在馬來語中則指「豆冰」。

## 發展

初時候，ABC冰只是在[刨冰上加上](../Page/刨冰.md "wikilink")[紅豆](../Page/紅豆.md "wikilink")。到了後來，加在刨冰上的配料愈來愈多，如多種[水果](../Page/水果.md "wikilink")、[蘆薈之類](../Page/蘆薈.md "wikilink")，使現今ABC冰顏色變得色彩斑斕。

## 成份

正宗的ABC冰的底部會放滿[棕櫚籽](../Page/棕櫚.md "wikilink")、[紅豆](../Page/紅豆.md "wikilink")、甜[玉米](../Page/玉米.md "wikilink")、[涼粉](../Page/涼粉.md "wikilink")、[大菜糕以及](../Page/大菜糕.md "wikilink")[珍多冰等多種配料](../Page/珍多冰.md "wikilink")。中間是[刨冰](../Page/刨冰.md "wikilink")。[刨冰的面上再會淋上](../Page/刨冰.md "wikilink")[煉奶](../Page/煉奶.md "wikilink")。部分商店的ABC冰會在刨冰上放上各式[水果](../Page/水果.md "wikilink")、[巧克力糖漿](../Page/巧克力.md "wikilink")、草莓果酱、[蘆薈](../Page/蘆薈.md "wikilink")、或[雪糕](../Page/雪糕.md "wikilink")。也有商店在刨冰上淋上多種顏色的[糖漿](../Page/糖漿.md "wikilink")，再佐以（[椰糖](../Page/椰.md "wikilink")）伴食。

## 普及

在很多東南亞地區的[咖啡店](../Page/咖啡店.md "wikilink")、[小販中心及](../Page/小販中心.md "wikilink")[美食廣場都會有這種甜品售賣](../Page/美食廣場.md "wikilink")。而這種甜品相當受民眾及遊客歡迎，也是不少人必享用的饭后甜点。

## 相关条目

  - [紅豆冰](../Page/紅豆冰.md "wikilink")

  - ：日式紅豆刨冰

  - [琉球式紅豆刨冰](../Page/琉球式紅豆刨冰.md "wikilink")

  - ：韓式紅豆刨冰

## 參見

<div class="references-small">

<references />

</div>

[Category:馬來西亞食品](../Category/馬來西亞食品.md "wikilink")
[Category:新加坡食品](../Category/新加坡食品.md "wikilink")
[Category:刨冰](../Category/刨冰.md "wikilink")

1.