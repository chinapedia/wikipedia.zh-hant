**本杰明·内森·卡多佐**（，）是位美国[法学家](../Page/法学家.md "wikilink")，曾任[纽约上诉法院法官和](../Page/纽约上诉法院.md "wikilink")[美国最高法院大法官](../Page/美国最高法院大法官.md "wikilink")。卡多佐对美国二十世纪[法律发展产生重要影响](../Page/英美法系.md "wikilink")，并因其哲理和文采被人敬仰。卡多佐在[最高法院任职六年](../Page/最高法院.md "wikilink")，自1932年至1938年去世。他许多重要判决则是在纽约上诉法院的18年间做出的。纽约上诉法院是该州终审法院。

## 出生

1870年，丽贝卡·华盛顿（内森氏）（Rebecca Washington （née Nathan））和阿尔伯特·雅各·卡多佐（Albert
Jacob
Cardozo）的儿子本杰明·卡多佐出生。\[1\]卡多佐的祖父母和外祖父母\[2\]都来自葡萄牙犹太人社区，参加曼哈顿犹太教堂（Congregation
Shearith Israel）。在美国独立之前，全家从英国伦敦移民至此。

卡多佐家人为犹太裔新基督徒皈依者，在[西班牙宗教裁判所折腾之时离开](../Page/西班牙宗教裁判所.md "wikilink")[伊比利亚半岛前往荷兰](../Page/伊比利亚半岛.md "wikilink")，\[3\]而后回归[犹太教](../Page/犹太教.md "wikilink")。卡多佐家庭传统可以追溯到葡萄牙玛拉诺人（marrano，新基督徒皈依者但秘密保持犹太教信仰），\[4\]不过卡多佐先祖并非严格意义上的葡萄牙本地人。\[5\]不过，“Cardozo”（“Cardoso”的古语）、“Seixas”和“Mendes”都是葡萄牙语伊比利亚常见姓氏，而非西班牙语。

本杰明·卡多佐和妹妹艾米丽是双胞胎。兄妹一共四人，前面有一兄一姐。表亲中的一位是诗人艾玛·拉撒路（Emma
Lazarus）；另一位是弗朗西斯·刘易斯·卡多佐，职业为教士、政治家和教师。本杰明从他叔叔本杰明·内森得名，后者是[纽约证交所副主席](../Page/纽约证交所.md "wikilink")，死于1870年未解谋杀案。\[6\]

父亲阿尔伯特·卡多佐是纽约最高法院（初审法院）的法官。1868年，他卷入[伊利铁路接管纠纷引发的腐败丑闻而辞职](../Page/伊利铁路.md "wikilink")。丑闻导致纽约市律师协会创立。离开法院后，他又从事法律业务近二十年，直至1885年去世。

## 早年

1879年，丽贝卡·卡多佐去世，留下年幼的本杰明和艾米丽。年仅11岁的姐姐内尔（Nell）将他们拉扯成人。本杰明的一位家教是[霍瑞修·爱尔杰](../Page/霍瑞修·爱尔杰.md "wikilink")。\[7\]本杰明15岁就上了大学，在[哥伦比亚大学就读](../Page/哥伦比亚大学.md "wikilink")\[8\]，并进入美国大学优等生荣誉学会（ΦΒΚ），\[9\]之后于1889年考入该校[法学院](../Page/哥伦比亚法学院.md "wikilink")。卡多佐希望自己的职业能够养家糊口，但也希望能够为父亲的污点雪耻。当卡多佐进入法学院时，学历不过要两年；但学了一半时，学制被延长到三年。卡多佐坐不住最后一年，肄业离开。\[10\]
1891年，他通过律师考试，与他的哥哥一道从事上诉法律业务。\[11\]本杰明在纽约市从事律师业务到1914年。\[12\]1913年11月，他竞选纽约最高法院勉强获胜，任期14年，于1914年1月1日入职。

## 纽约上诉法院

1914年2月，根据1899年修正案，卡多佐被任命至[纽约上诉法院](../Page/纽约上诉法院.md "wikilink")，\[13\]成为该院第一位犹太裔法官。1917年1月，山姆·西伯利（Samuel
Seabury）辞职，卡多佐接任。1917年11月，在民主党和共和党选举中，他获得上诉法院14年任期。1926年，他选举再次获胜，得到上诉法院大法官14年任期。1927年1月1日入职；1932年3月7日辞职，升任最高法院。

在任期中，他在许多[侵权法和](../Page/侵权行为.md "wikilink")[合同法的判决上开了先河](../Page/合同.md "wikilink")。部分原因是时事；大规模工业化促使法院重审之前的法律，适应新的环境。\[14\]1921年，卡多佐在[耶鲁大学做斯托尔斯演讲](../Page/耶鲁大学.md "wikilink")（Storrs），之后被整理出版为《论司法过程的性质》,
该书至今仍具价值。不久，卡多佐成为[美国律师协会承办成员之一](../Page/美国律师协会.md "wikilink")，协会发表侵权、合同及其它私法的法律重述。他的其它三本著作成为现今法学界标准。\[15\]

在上诉法院时，卡多佐批评联邦法院的[证据排除法则](../Page/证据排除法则.md "wikilink")，称：“警察犯错放走了罪犯。”他注意到许多州反对该法则，但称联邦法院干预了地方。\[16\]\[17\]\[18\]\[19\]

## 最高法院

[Justice_Benjamin_N_Cardozo_2.jpg](https://zh.wikipedia.org/wiki/File:Justice_Benjamin_N_Cardozo_2.jpg "fig:Justice_Benjamin_N_Cardozo_2.jpg")
[Cardozo_Nomination.JPG](https://zh.wikipedia.org/wiki/File:Cardozo_Nomination.JPG "fig:Cardozo_Nomination.JPG")
1932年，[胡佛总统提名卡多佐担任最高法院大法官](../Page/胡佛.md "wikilink")，接替退休的[霍姆斯大法官](../Page/小奥利弗·温德尔·霍姆斯.md "wikilink")，获得参议院通过，他是继[路易斯·布兰戴斯之后](../Page/路易斯·布兰戴斯.md "wikilink")，最高法院的第二位犹太人大法官。《[纽约时报](../Page/纽约时报.md "wikilink")》称任命卡多佐是“人人赞同的，这在法院任命上是历史上少有的。”\[20\]卡多佐是民主党，却被共和党总统提名，常被最高法院历史引述为抛开党派政治动机，严格以候选人对法律的贡献而定。\[21\]
不过，胡佛正在竞选连任，最终对付[富兰克林·罗斯福](../Page/富兰克林·罗斯福.md "wikilink")，或许背后有更深的算盘。

2月24日，卡多佐在参议院获全票通过。\[22\]1932年3月1日，卡多佐正式任命，在广播里华盛顿民主党参议员克拉伦斯·C·迪尔（Clarence
C.
Dill）称胡佛任命卡多佐是“他总统生涯中最佳决定”。\[23\][芝加哥大学法学院全体教职人员敦促胡佛提名](../Page/芝加哥大学法学院.md "wikilink")，哈佛、耶鲁和哥伦比亚大学法学院长也积极推荐。法官[哈伦·菲斯克·斯通强烈敦促胡佛提名卡多佐](../Page/哈伦·菲斯克·斯通.md "wikilink")，甚至称如果胡佛找别人，就辞职让贤。（1925年，斯通曾向[卡尔文·柯立芝坦言应该提名卡多佐而非自己任职](../Page/卡尔文·柯立芝.md "wikilink")）。\[24\]然而，胡佛最开始是不同意的；法院已经有两位纽约来的法官，一位犹太人；另外，法官[詹姆斯·克拉克·麦克雷诺兹的反犹主义臭名昭著](../Page/詹姆斯·克拉克·麦克雷诺兹.md "wikilink")。当参议员外事委员会主席，爱达荷的威廉·E·博拉强力支持卡多佐时，胡佛最终让步。

卡多佐、布兰迪斯、斯通是最高法院三剑客，倾向于自由派。在任时，他强调严格执行[宪法第十修正案](../Page/美国宪法第十修正案.md "wikilink")。

## 去世

1937年末，卡多佐犯了[心肌梗死](../Page/心肌梗死.md "wikilink")，1938年初，他犯了[中风](../Page/中风.md "wikilink")。1938年7月9日，卡多佐去世，享年68岁，葬在皇后区贝丝·欧兰姆（Beth
Olam）公墓。\[25\]\[26\]他去世时正好是法院换届之时，许多法官在十九世纪三十年代末到四十年代初去世或是退休。

## 私生活

[2101_Connecticut_Ave.JPG](https://zh.wikipedia.org/wiki/File:2101_Connecticut_Ave.JPG "fig:2101_Connecticut_Ave.JPG")
成人后，卡多佐放弃信仰（持不可知论），但他对自己犹太传统十分自豪。\[27\]

阿尔伯特和丽贝卡·卡多佐的六个孩子中只有艾米丽结婚，但她和丈夫没有生育。本杰明·卡多佐独身禁欲。由于他终身未婚，又师从霍瑞修·爱尔杰（被指娈童），一些传记作家暗指卡多佐是同性恋，但苦于没有证据。Richard
Polenberg写了关于卡多佐的书，宪法学者杰弗里·罗森（Jeffrey Rosen）在《纽约时报》书评上称：

## 名言

卡多佐的自我评价和他的法律天资不相上下：

> In truth, I am nothing but a plodding mediocrity—please observe, a
> plodding mediocrity—for a mere mediocrity does not go very far, but a
> plodding one gets quite a distance. There is joy in that success, and
> a distinction can come from courage, fidelity and industry.
> 真的，我不过是单调平庸的 — 请仔细看看，单调平庸 —
> 只是平庸的话走不了多远，但单调平庸能走很长。成功很让人快乐，勇敢、忠诚和努力则能分出高下。\[28\]

[Cardozo_Hotel_(Miami_Beach).jpg](https://zh.wikipedia.org/wiki/File:Cardozo_Hotel_\(Miami_Beach\).jpg "fig:Cardozo_Hotel_(Miami_Beach).jpg")

**命名**

  - 纽约市[叶史瓦大学的](../Page/叶史瓦大学.md "wikilink")[本杰明·卡多佐法学院](../Page/本杰明·卡多佐法学院.md "wikilink")
  - [石溪大学卡多佐学院宿舍楼](../Page/石溪大学.md "wikilink")。
  - 纽约市皇后区本杰明·N·卡多佐高中
  - 佛罗里达迈阿密的卡多佐旅馆
  - 华盛顿特区本杰明·卡多佐高中

## 文献

  - Cardozo, Benjamin N. (1921), *[The Nature of the Judicial
    Process](../Page/The_Nature_of_the_Judicial_Process.md "wikilink")*,
    The Storrs Lectures Delivered at [Yale
    University](../Page/Yale_University.md "wikilink"). -- 《论司法过程的性质》
    [On line
    version](http://xroads.virginia.edu/~HYPER/CARDOZO/CarNat.html)

  -
  - Cardozo, Benjamin N. (1889), *The Altruist in Politics*,
    commencement oration at Columbia College, [Gutenberg Project
    version](http://www.gutenberg.org/files/1341/1341-h/1341-h.htm).

## 脚注

## 延伸閱讀

  -
  - Cardozo, Benjamin N. (1957). *An Introduction to Law*. Cambridge:
    Harvard Law Review Association. (Chapters by eight distinguished
    American judges).

  -
  - Cardozo, Benjamin N. \[1870–1938\]. *Essays Dedicated to Mr. Justice
    Cardozo*. \[N.p.\]: Published by [Columbia Law
    Review](../Page/Columbia_Law_Review.md "wikilink"), [Harvard Law
    Review](../Page/Harvard_Law_Review.md "wikilink"), [Yale Law
    Journal](../Page/Yale_Law_Journal.md "wikilink"), 1939. \[143\]
    pp. Contributors: [Harlan Fiske
    Stone](../Page/Harlan_Fiske_Stone.md "wikilink"), the Rt. Hon. [Lord
    Maugham](../Page/Lord_Maugham.md "wikilink"), [Herbert Vere
    Evatt](../Page/Herbert_Vere_Evatt.md "wikilink"), [Learned
    Hand](../Page/Learned_Hand.md "wikilink"), [Irving
    Lehman](../Page/Irving_Lehman.md "wikilink"), [Warren
    Seavey](../Page/Warren_Seavey.md "wikilink"), [Arthur L.
    Corbin](../Page/Arthur_L._Corbin.md "wikilink"), [Felix
    Frankfurter](../Page/Felix_Frankfurter.md "wikilink"). Also includes
    a reprint of Cardozo’s essay "Law And Literature" with a foreword by
    [James M. Landis](../Page/James_M._Landis.md "wikilink").

  -
  -
  - [Frankfurter, Felix](../Page/Felix_Frankfurter.md "wikilink"), *Mr.
    Justice Cardozo and Public Law*, [Columbia Law
    Review](../Page/Columbia_Law_Review.md "wikilink") 39 (1939):
    88–118, [Harvard Law
    Review](../Page/Harvard_Law_Review.md "wikilink") 52 (1939):
    440–470, [Yale Law
    Journal](../Page/Yale_Law_Journal.md "wikilink") 48 (1939): 458–488.

  -
  -
  -
  -
  -
  -
  - Seavey, Warren A., *Mr. Justice Cardozo and the Law of Torts*,
    [Columbia Law Review](../Page/Columbia_Law_Review.md "wikilink") 39
    (1939): 20–55, [Harvard Law
    Review](../Page/Harvard_Law_Review.md "wikilink") 52 (1939):
    372–407, [Yale Law
    Journal](../Page/Yale_Law_Journal.md "wikilink") 48 (1939): 390–425

  -
## 外部連結

  -
  -
  -
  - [Benjamin Cardozo Memorial
    at](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=172) [Find
    A Grave](../Page/Find_A_Grave.md "wikilink").

  - [Benjamin Cardozo at Michael
    Ariens.com.](https://web.archive.org/web/20090826222955/http://www.michaelariens.com/ConLaw/justices/cardozo.htm)

  - [History of the Court, the Hughes Court
    at](https://web.archive.org/web/20090207190754/http://www.supremecourthistory.org/02_history/subs_history/02_c11.html)
    [Supreme Court Historical
    Society](../Page/Supreme_Court_Historical_Society.md "wikilink").

  - [Listing and portrait of Benjamin N.
    Cardozo,](http://www.courts.state.ny.us/history/elecbook/thereshallbe/pg100.htm)
    [New York Court of
    Appeals](../Page/New_York_Court_of_Appeals.md "wikilink") judge at
    [Historical Society of the Courts of the State of New
    York](../Page/Historical_Society_of_the_Courts_of_the_State_of_New_York.md "wikilink").

  - [Oyez Project](../Page/Oyez.org.md "wikilink"), [U.S. Supreme Court
    media, Benjamin N.
    Cardozo.](http://www.oyez.org/justices/benjamin_n_cardozo/)

[C](../Category/美国法学家.md "wikilink")
[C](../Category/哥倫比亞大學校友.md "wikilink")
[C](../Category/葡萄牙裔美國人.md "wikilink")
[C](../Category/美國犹太人.md "wikilink")
[C](../Category/紐約市人.md "wikilink")
[C](../Category/美国最高法院大法官.md "wikilink")

1.

2.  Cardozo's maternal grandparents, Sara Seixas and Isaac Mendes Seixas
    Nathan, and his paternal grandparents, Ellen Hart and Michael H.
    Cardozo

3.
4.
5.  [Mark Sherman, 'First Hispanic justice? Some say it was Cardozo',
    The Associated
    Press, 2009.](https://www.google.com/hostednews/ap/article/ALeqM5gAI5nmLX1RQsIwqKl2XwUCMt3uwAD98E7O5O0)

6.

7.

8.
9.  [Supreme Court Justices Who Are Phi Beta Kappa
    Members](http://www.pbk.org/userfiles/file/Famous%20Members/PBKSupremeCourtJustices.pdf)
    , ‘’Phi Beta Kappa website’’, accessed Oct 4, 2009

10.

11.
12.
13. [Designation](https://query.nytimes.com/mem/archive-free/pdf?res=9B0DEED81F3BE633A25750C0A9649C946596D6CF)
    in NYT on February 3, 1914

14.
15.
16. *People of the State of New York v. John Defore*, 150 N.E. 585
    (1926).

17.

18.  ISBN 978-0976682608.

19.  ISBN 978-0674960527

20.

21.

22. (*New York Times*, February 25, 1932, p. 1)

23. (*New York Times*, March 2, 1932, p. 13)

24. (Handler, 1995)

25.  [Supreme Court Historical
    Society](../Page/Supreme_Court_Historical_Society.md "wikilink") at
    [Internet Archive](../Page/Internet_Archive.md "wikilink").

26. *See also*, Christensen, George A., *Here Lies the Supreme Court:
    Revisited*, *Journal of Supreme Court History*, Volume 33 Issue 1,
    Pages 17 – 41 (19 Feb 2008), [University of
    Alabama](../Page/University_of_Alabama.md "wikilink").

27. [Benjamin
    Cardozo.](https://www.jewishvirtuallibrary.org/jsource/biography/cardozo.html),
    Jewish Virtual Library,

28. As quoted in *Nine Old Men* (1936) by Drew Pearson and Robert Sharon
    Allen, p. 221.