[Nevins_Library_First_Librarians.jpg](https://zh.wikipedia.org/wiki/File:Nevins_Library_First_Librarians.jpg "fig:Nevins_Library_First_Librarians.jpg")管理員\]\]
[Leitstand_2.jpg](https://zh.wikipedia.org/wiki/File:Leitstand_2.jpg "fig:Leitstand_2.jpg")發電廠的管理员\]\]

**管理员**，在平常的生活中一般指对人事、资本的[管理人员](../Page/管理.md "wikilink")，在[计算机行业中](../Page/计算机.md "wikilink")，管理员指维护[软件或者](../Page/软件.md "wikilink")[硬件设备正常工作的专业人员](../Page/硬件.md "wikilink")。

## 公共管理

  - [社会科学內的](../Page/社会科学.md "wikilink")[公务员](../Page/公务员.md "wikilink")

## 工商管理

参与[商业竞争的](../Page/商业.md "wikilink")[企业中的管理人员](../Page/企业.md "wikilink")，此类型的管理人员分类较多，各个部门各有分工。

  - [生产管理](../Page/生产管理.md "wikilink")
  - [经营管理](../Page/经营管理.md "wikilink")
  - [财务管理](../Page/财务管理.md "wikilink")
  - [质量管理](../Page/质量管理.md "wikilink")
  - [成本管理](../Page/成本管理.md "wikilink")
  - [人力资源管理](../Page/人力资源管理.md "wikilink")
  - [研发管理](../Page/研发管理.md "wikilink")
  - [营销管理](../Page/营销管理.md "wikilink")
  - [物资管理](../Page/物资管理.md "wikilink")
  - [设施管理](../Page/设施管理.md "wikilink")
  - [品牌管理](../Page/品牌管理.md "wikilink")
  - [物業管理](../Page/物業管理.md "wikilink")

## 计算机

在计算机行业中“管理员”指以下类型的管理人员，通常拥有较高的权限：

  - [系统管理员](../Page/系统管理员.md "wikilink")（[SYSOP](../Page/SYSOP.md "wikilink")，**System
    administrator**），维护企业[服务器](../Page/服务器.md "wikilink")[操作系统正常运行与安全](../Page/操作系统.md "wikilink")
  - [网络管理员](../Page/网络管理员.md "wikilink")（**Network
    administrator**），维护网络设备与[网络安全](../Page/网络安全.md "wikilink")
  - [数据库管理员](../Page/数据库管理员.md "wikilink")（**Database
    administrator**），主管[数据库的维护](../Page/数据库.md "wikilink")
  - [论坛管理员](../Page/论坛管理员.md "wikilink")（**Forum
    administrator**），维护网络[社区](../Page/社区.md "wikilink")[论坛运转](../Page/论坛.md "wikilink")
  - [web2.0当中](../Page/web2.0.md "wikilink")，[wiki与](../Page/wiki.md "wikilink")[blog也会有管理人员](../Page/blog.md "wikilink")。blog中的管理员一般指用户自己和blog服务提供商，类似于[Windows操作系统管理员](../Page/Windows.md "wikilink")“Administrator”帐号，一般比访客（guest）多拥有写入与删除权限，而匿名的访客大多只有唯读权限。

## 参见

  - [职业列表](../Page/职业列表.md "wikilink")

[Category:职业](../Category/职业.md "wikilink")
[Category:网络安全](../Category/网络安全.md "wikilink")
[Category:计算机专家](../Category/计算机专家.md "wikilink")