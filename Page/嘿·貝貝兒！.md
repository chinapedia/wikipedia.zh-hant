**嘿·貝貝兒！**（***Heyy
Babyy***）是2007年的印度電影，是[沙吉·罕](../Page/沙吉·罕.md "wikilink")（Sajid
Khan）導演的第一部電影，本片題材於[澳大利亞](../Page/澳大利亞.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")，描寫三個男人照顧一個嬰兒的故事。講述所有孩子都需要母愛、也需要父愛這個道理。

## 故事梗概

影片開頭是：
        一二三四五，
        三個王老五，
        （有一天突然）拾到個貝貝，
        （竟然）不知誰是父（親）！
 
貝貝清晨被放在單身公寓的門口，嬰兒籃子僅一張沒有指明誰是父親的謾罵字條，隨後這三個無拘無束的大男人生活被徹底打亂，但是也漸漸煥發出他們人性中美麗的一面。直到孩子的媽媽出現，真正的好戲劇才算開場。。。

## 卡士

  - [阿克夏·庫馬](../Page/阿克夏·庫馬.md "wikilink") ... [Akshay
    Kumar](../Page/Akshay_Kumar.md "wikilink") ... 飾演阿廬什（Arush）
  - [法丁·罕](../Page/法丁·罕.md "wikilink") ... [Fardeen
    Khan](../Page/Fardeen_Khan.md "wikilink") ... 飾演艾爾（Al）
  - [利特什·德斯穆科](../Page/利特什·德斯穆科.md "wikilink") ... [Ritesh
    Deshmukh](../Page/Ritesh_Deshmukh.md "wikilink") ... 飾演坦美（Tanmay）
  - [維荻婭·巴蘭](../Page/維荻婭·巴蘭.md "wikilink") ... [Vidya
    Balan](../Page/Vidya_Balan.md "wikilink") ... 飾演伊斯娜（Esha）
  - [胡安娜·桑喡](../Page/胡安娜·桑喡.md "wikilink") ... [Juanna
    Sanghvi](../Page/Juanna_Sanghvi.md "wikilink") ... 小貝貝兒安祁（Angel）
  - 特別嘉賓[沙·茹克·罕](../Page/沙·茹克·罕.md "wikilink") ... [Shahrukh
    Khan](../Page/Shahrukh_Khan.md "wikilink") ... 在一首歌里出現（Raj Malhotra）
    \[1\]

## 影片歌曲

影片總共有九首歌曲，全部由[Shankar-Ehsaan-Loy音樂提供](../Page/Shankar-Ehsaan-Loy音樂.md "wikilink")。

| 歌曲名                                  | 歌手名                                                                                                                                                                                                                     | 填詞者                                                            |
| ------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------- |
| *Dholna*                             | [Sonu Nigam](../Page/Sonu_Nigam.md "wikilink") & [Shreya Ghosal](../Page/Shreya_Ghosal.md "wikilink")                                                                                                                   | [Sameer](../Page/Sameer.md "wikilink")                         |
| *Dholna - Love Is In The Air Remix*  | [Sonu Nigam](../Page/Sonu_Nigam.md "wikilink") & [Shreya Ghosal](../Page/Shreya_Ghosal.md "wikilink")                                                                                                                   | [Sameer](../Page/Sameer.md "wikilink")                         |
| *Heyy Babyy*                         | [Neeraj Shridhar](../Page/Neeraj_Shridhar.md "wikilink") & [Raman](../Page/Raman.md "wikilink") & [Pervez Quadri](../Page/Pervez_Quadri.md "wikilink")                                                                  | [Sameer](../Page/Sameer.md "wikilink")                         |
| *Heyy Babyy - The Big 'O' Remix*     | [Neeraj Shridhar](../Page/Neeraj_Shridhar.md "wikilink") & [Raman](../Page/Raman.md "wikilink") & [Pervez Quadri](../Page/Pervez_Quadri.md "wikilink")                                                                  | [Sameer](../Page/Sameer.md "wikilink")                         |
| *Heyy Babyy Featuring Girl Band*     | [Akbar Sami](../Page/Akbar_Sami.md "wikilink")                                                                                                                                                                          | [Anvita Dutt Guptan](../Page/Anvita_Dutt_Guptan.md "wikilink") |
| *Jaane Bhi De - Hiphop Hiccup Remix* | [Shankar Mahadevan](../Page/Shankar_Mahadevan.md "wikilink") & [Loy Mendonsa](../Page/Loy_Mendonsa.md "wikilink")                                                                                                       | [Sameer](../Page/Sameer.md "wikilink")                         |
| *Jaane Bhi De*                       | [Shankar Mahadevan](../Page/Shankar_Mahadevan.md "wikilink")                                                                                                                                                            | [Sameer](../Page/Sameer.md "wikilink")                         |
| *Mast Kalandar*                      | [Salim Shahzada](../Page/Salim_Shahzada.md "wikilink") & [Rehan Khan](../Page/Rehan_Khan.md "wikilink") & [Shankar Mahadevan](../Page/Shankar_Mahadevan.md "wikilink") & [Sajid Khan](../Page/Sajid_Khan.md "wikilink") | [Sameer](../Page/Sameer.md "wikilink")                         |
| *Meri Duniya Tu Hi Re*               | [Sonu Nigam](../Page/Sonu_Nigam.md "wikilink") & [Shaan](../Page/Shaan.md "wikilink") & [Shankar Mahadevan](../Page/Shankar_Mahadevan.md "wikilink")                                                                    | [Sameer](../Page/Sameer.md "wikilink")                         |
|                                      |                                                                                                                                                                                                                         |                                                                |

## 票房記錄

## 參照

## 外部連結

  - [貝貝牆畫](http://www.allpunjabi.com/wallpapers.php?cat=17&scat=21&sscat=173)

  -
  - [更多關於《Heyy
    Babyy》資訊](http://www.dctorrent.com/showthread.php?p=662676#post662676)

[H](../Category/印度電影作品.md "wikilink")
[H](../Category/2007年電影.md "wikilink")

1.