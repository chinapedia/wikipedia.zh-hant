[HK_ChaiWanKok_Catholic_PrimarySchool.JPG](https://zh.wikipedia.org/wiki/File:HK_ChaiWanKok_Catholic_PrimarySchool.JPG "fig:HK_ChaiWanKok_Catholic_PrimarySchool.JPG")

**柴灣角天主教小學**（**Chai Wan Kok Catholic Primary
School**）位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[荃灣區](../Page/荃灣區.md "wikilink")[荃景圍](../Page/荃景圍.md "wikilink")，隸屬[香港](../Page/香港.md "wikilink")[教育局](../Page/教育局.md "wikilink")「小一入學」第六十二校網\[1\]，於1979年成立，為一所設備完善之政府津貼男女小學。全校有24班，校訓為「勇、智、仁」\[2\]。

## 前身

柴灣角天主教小學前身為位處[牛頭角龍山復華村](../Page/牛頭角.md "wikilink")[平房區的](../Page/平房區.md "wikilink")[庇護十二學校](../Page/庇護十二學校.md "wikilink")。[復華村](../Page/復華村.md "wikilink")[平房區於](../Page/平房區.md "wikilink")1978年開始被清拆（香港政府清拆[復華村後在原址建成](../Page/復華村.md "wikilink")[樂華村](../Page/樂華村.md "wikilink")）。[庇護十二學校亦於](../Page/庇護十二學校.md "wikilink")1979年被清拆，部份教職員轉往[瑪利諾會其他學校](../Page/瑪利諾會.md "wikilink")，而校監兼校長顯主會修女戴振容修女帶同少數庇護十二老師到青衣郭怡雅紀念小學，而盧世恪主任和陳兆鎕主任則和劉玉亭神父申請開辦**柴灣角天主教小學**。1979至1980年新校舍還未落成前曾借用祖堯天主教小學校舍上課。1980年柴灣角校舍落成，約20位前[庇護十二學校老師歸隊在柴灣角開展新一頁](../Page/庇護十二學校.md "wikilink")，盧世恪和陳兆鎕分別出任柴灣角上午及下午校校長\[3\]。2009年柴灣角上午校在鐘名輝校長領導下又遷至[深井並改名為](../Page/深井.md "wikilink")[深井天主教小學](../Page/深井天主教小學.md "wikilink")\[4\]\[5\]\[6\]。柴灣角天主教小學下午校則留於原址繼續辦學同時實施全日制。因這段淵源[深井天主教小學和柴灣角天主教小學歷史均可追溯至](../Page/深井天主教小學.md "wikilink")[瑪利諾會](../Page/瑪利諾會.md "wikilink")。

## 聖母像

[Our_Lady_Statue_in_Chai_Wan_Kok_P1000845rocc.jpg](https://zh.wikipedia.org/wiki/File:Our_Lady_Statue_in_Chai_Wan_Kok_P1000845rocc.jpg "fig:Our_Lady_Statue_in_Chai_Wan_Kok_P1000845rocc.jpg")遷往柴灣角天主教小學的聖母像\]\]
柴灣角天主教小學內的聖母像是從[庇護十二學校所搬遷到現址的](../Page/庇護十二學校.md "wikilink")，己有近五十年歷史。

## 辦學宗旨

此校自稱以[基督為模範](../Page/基督.md "wikilink")，傳揚福音，引導學生認識並履行[天主誡命](../Page/天主.md "wikilink")，培育學生道德勇氣、認知發展及仁愛品德、認識祖國、關心人類福祉\[7\]。

## 歷任校監、校長

### 歷任校監

  - 劉玉亭神父
  - 王保誠神父
  - 羅國輝神父 --曾公開批評首富[李嘉誠為富不仁](../Page/李嘉誠.md "wikilink")，有「敢言神父」美譽。
  - 宋雲龍執事
  - 潘樹照先生

### 上午校歷任校長

  - 盧世恪先生 --
    盧世恪先生原為[庇護十二學校主任](../Page/庇護十二學校.md "wikilink")，庇護被清拆時與陳兆鎕先生向劉玉亭神父申請開辦該校並帶領[庇護十二學校老師一同開創柴灣角天主教小學](../Page/庇護十二學校.md "wikilink")
  - 鐘名揮先生 --
    鐘校長於2009年將學校遷往深井並出任[深井天主教小學創校校長](../Page/深井天主教小學.md "wikilink")

### 下午校歷任校長

  - 陳兆鎕先生 --
    陳兆鎕先生原為[庇護十二學校主任](../Page/庇護十二學校.md "wikilink")，庇護清拆時與盧世恪先生向劉玉亭神父申請開辦該校並帶領[庇護十二學校老師一同開創柴灣角天主教小學](../Page/庇護十二學校.md "wikilink")
  - 廖翔顯先生
  - 梁淑儀女士
  - 陳慧珍女士
  - 何凱瑤女士 -- 2009年柴灣角天主教小學下午校成為全日制時何凱瑤女士出任全日制第一任校長

## 校舍

[CWKCPS_entrance_P1000853.JPG](https://zh.wikipedia.org/wiki/File:CWKCPS_entrance_P1000853.JPG "fig:CWKCPS_entrance_P1000853.JPG")

  - 佔地面積：約4100平方米\[8\]
  - 課室數目：24間
  - 校園設備：[操場](../Page/操場.md "wikilink")(2)、禮堂、[圖書館](../Page/圖書館.md "wikilink")、音樂室、電腦室、多媒體學習室、科學探知室、創藝天地、環保室、加強輔導室、家長資源室、會議室。\[9\]

## 著名/傑出校友

  - [譚竣浩](../Page/譚竣浩.md "wikilink")：香港童星
  - [陸浩明](../Page/陸浩明.md "wikilink")：香港男藝人\[10\]（1991年）
  - [孔德賢](../Page/孔德賢.md "wikilink")：香港男主持和模特兒（2009年下午校）\[11\]
  - [蔣家旻](../Page/蔣家旻.md "wikilink")：香港女演員（2001年上午校）\[12\]
  - [龔嘉欣](../Page/龔嘉欣.md "wikilink")：香港女演員（2001年下午校）

[HK_Tsuen_Wan_安賢街_On_Yin_Street_柴灣角天主教小學_Chai_Wan_Kok_Catholic_Primary_School_facade.JPG](https://zh.wikipedia.org/wiki/File:HK_Tsuen_Wan_安賢街_On_Yin_Street_柴灣角天主教小學_Chai_Wan_Kok_Catholic_Primary_School_facade.JPG "fig:HK_Tsuen_Wan_安賢街_On_Yin_Street_柴灣角天主教小學_Chai_Wan_Kok_Catholic_Primary_School_facade.JPG")
[CWKCPS_P1000836.JPG](https://zh.wikipedia.org/wiki/File:CWKCPS_P1000836.JPG "fig:CWKCPS_P1000836.JPG")
[CWKCPSP1000838.JPG](https://zh.wikipedia.org/wiki/File:CWKCPSP1000838.JPG "fig:CWKCPSP1000838.JPG")

## 外部連結

  - [柴灣角天主教小學](http://www.cwk.edu.hk//)

## 參考文獻

[Category:柴灣角](../Category/柴灣角.md "wikilink")
[c](../Category/荃灣區小學.md "wikilink")
[柴](../Category/香港天主教學校.md "wikilink")
[Category:瑪利諾外方傳教會](../Category/瑪利諾外方傳教會.md "wikilink")
[柴](../Category/1980年創建的教育機構.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.

9.
10.

11.

12. [Angel Chiang's primary school, print screened
    on 2018-8-4](https://drive.google.com/file/d/12CnN7CixOt2kFV6GnxVtwutka-yPpBCv/view?usp=sharing)