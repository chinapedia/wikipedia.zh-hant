[1936臺灣原住民泰雅族領袖樂信．瓦旦（林瑞昌）與部落頭目等於新竹州廳前_Loshin_Wadan_（Atayal）_with_Indigenous_Taiwanese_Leaders_in_front_of_Shinchiku_Prefecture_building.JPG](https://zh.wikipedia.org/wiki/File:1936臺灣原住民泰雅族領袖樂信．瓦旦（林瑞昌）與部落頭目等於新竹州廳前_Loshin_Wadan_（Atayal）_with_Indigenous_Taiwanese_Leaders_in_front_of_Shinchiku_Prefecture_building.JPG "fig:1936臺灣原住民泰雅族領袖樂信．瓦旦（林瑞昌）與部落頭目等於新竹州廳前_Loshin_Wadan_（Atayal）_with_Indigenous_Taiwanese_Leaders_in_front_of_Shinchiku_Prefecture_building.JPG")[泰雅族領袖](../Page/泰雅族.md "wikilink")[樂信·瓦旦與部落頭目等於新竹州廳前](../Page/樂信·瓦旦.md "wikilink")\]\]
[新竹州廳車寄.jpg](https://zh.wikipedia.org/wiki/File:新竹州廳車寄.jpg "fig:新竹州廳車寄.jpg")

**新竹州廳**是[臺灣](../Page/臺灣.md "wikilink")[新竹市的](../Page/新竹市.md "wikilink")[古蹟之一](../Page/古蹟.md "wikilink")，興建於[臺灣日治時期](../Page/臺灣日治時期.md "wikilink")，保存至今。新竹州廳始建於1925年（[大正](../Page/大正.md "wikilink")14年），於1927年（[昭和](../Page/昭和.md "wikilink")2年）11月26日完成上棟儀式啟用，1932年（昭和7年）再擴建入口門廊部分。1945年[新竹市政府成立](../Page/新竹市政府.md "wikilink")，沿用州廳做為辦公廳舍。1950年新竹縣、市合併，新竹縣政府遷回州廳辦公。1982年[新竹市改制為](../Page/新竹市.md "wikilink")[省轄市](../Page/省轄市.md "wikilink")，州廳轉作市政府辦公廳舍至今。於1998年7月在[蔡仁堅市長任內](../Page/蔡仁堅.md "wikilink")，為了表彰新竹州廳的建築藝術與歷史價值，[臺灣省政府依](../Page/臺灣省政府.md "wikilink")[文化資產保存法](../Page/wikisource:zh:文化資產保存法.md "wikilink")，將新竹州廳列為「省定古蹟」；[精省之後](../Page/精省.md "wikilink")，新竹州廳升格為國定古蹟，並在2000年起進行古蹟保存修護工程，至2005年完工，重現古蹟風采。新竹州廳採西洋建築風格，為二層樓加強磚造建築物，地板與橫樑主要使用鐵筋[混凝土](../Page/混凝土.md "wikilink")，兩個坡式斜屋頂由木屋架組立而成，屋面材料使用日本黑瓦。整體而言，新竹州廳是保存良好的官廳建築，是臺灣重要且珍貴的古蹟之一。\[1\]

## 週邊設施

  - [新竹車站](../Page/新竹車站.md "wikilink")
  - [新竹之心（迎曦門）](../Page/竹塹城.md "wikilink")
  - [北門街商圈](../Page/北門街商圈.md "wikilink")
  - [巨城購物中心](../Page/巨城購物中心.md "wikilink")
  - [新竹地方法院](../Page/新竹地方法院.md "wikilink")
  - [兆豐銀行北新竹分行](../Page/兆豐銀行.md "wikilink")
  - [臺灣銀行新竹分行](../Page/臺灣銀行.md "wikilink")
  - [林務局新竹林區管理處](../Page/林務局.md "wikilink")
  - [開台進士古蹟群](../Page/鄭用錫#故居.md "wikilink")
  - [中正路](../Page/中正路_\(新竹市\).md "wikilink")（原名：驛前大道）

## 参考文献

## 外部連結

  - [新竹市政府](http://www.hccg.gov.tw/)

## 参见

  - [臺灣日治時期](../Page/臺灣日治時期.md "wikilink")
      - [新竹州](../Page/新竹州.md "wikilink")
  - [新竹市](../Page/新竹市.md "wikilink")
  - [臺灣古蹟列表](../Page/臺灣古蹟列表.md "wikilink")

{{-}}

[Category:台灣日治時期官署建築](../Category/台灣日治時期官署建築.md "wikilink")
[Category:新竹市古蹟](../Category/新竹市古蹟.md "wikilink")
[Category:台灣西式建築](../Category/台灣西式建築.md "wikilink")
[Category:1927年完工的政府建築物](../Category/1927年完工的政府建築物.md "wikilink")
[Category:1927年台灣建立](../Category/1927年台灣建立.md "wikilink")

1.  參考自[新竹市發展](http://disp.cc/b/215-1t7M)。