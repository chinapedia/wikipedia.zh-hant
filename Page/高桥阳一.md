**高橋陽一**（，），[日本漫畫家](../Page/日本.md "wikilink")。出生於[東京都](../Page/東京都.md "wikilink")[葛飾区](../Page/葛飾区.md "wikilink")。畢業於東京都立南葛飾高等学校。妻子為[足球小將主角大空翼的配音員](../Page/足球小將.md "wikilink")[日比野朱里](../Page/日比野朱里.md "wikilink")（原名：小粥洋子）。血型A型。

主要執筆作品以運動為題材。代表作是以足球作故事題材的《[足球小將](../Page/足球小將.md "wikilink")》、《[HUNGRY
HEART 野驁射手](../Page/HUNGRY_HEART_野驁射手.md "wikilink")》。

## 作品列表

### 長篇作品

除《[飆悍射將](../Page/HUNGRY_HEART_野驁射手.md "wikilink")》由[秋田書店發行之外](../Page/秋田書店.md "wikilink")，全由[集英社發行](../Page/集英社.md "wikilink")。

  - [足球小將系列](../Page/足球小將.md "wikilink")，原名《》（1981年－1988年）種類：男子[足球](../Page/足球.md "wikilink")

      - 足球小將 世青篇，原名《》（1994年－1997年）
      - 足球小將 Road to 2002、曾譯《邁向2002之路》，原名《》（2001年－2004年）
      - 足球小將 Golden-23 奧運篇，原名《》（2005年－2008年）
      - 足球小將 海外激鬥篇 IN CALCIO，原名《》（2009年）
      - 足球小將 海外激鬥篇 EN LA LIGA，原名《》（2010年－2011年）
      - 足球小將 Rising Sun，原名《》（2014年－**連載中**）

  - 翔的傳說，原名《》（1988年－1989年）種類：[網球](../Page/網球.md "wikilink")

  - （1990年－1991年）種類：[棒球](../Page/棒球.md "wikilink")

  - 、原名《CHIBI》（1992年－1993年）種類：[拳擊](../Page/拳擊.md "wikilink")

  - ，原名《》（1999年）種類：足球

  - [飆悍射將](../Page/HUNGRY_HEART_野驁射手.md "wikilink")，原名《》。台灣中文版由[長鴻出版社正式授權發行](../Page/長鴻出版社.md "wikilink")（2002年－2004年）種類：足球

### 短篇作品

  - [足球小將關聯作品](../Page/足球小將.md "wikilink")

<!-- end list -->

  - 我是岬太郎，原名《》
      - 《我是岬太郎》收錄：
          - －卒業－ 100MジャンパーⅡ 種類：跳台滑雪
          - BASUKE-バスケ- 種類：[籃球](../Page/籃球.md "wikilink")
  - 足球小將 世青特別篇 最強之敵\!荷蘭青年軍，原名《》
      - 《足球小將 世青特別篇 最強之敵\!荷蘭青年軍》收錄：
          - 北壁ダウンヒラー 種類：[高山滑雪](../Page/高山滑雪.md "wikilink")
          - 冬瓜拳王 短篇版 種類：[拳擊](../Page/拳擊.md "wikilink")
  - 足球小將 海外激鬥篇 EN LA LIGA 最終章，《》（2012年）
  - 足球小將短篇集 DREAM FIELD，原名《》現今刊行2巻。

<!-- end list -->

  -  種類：[跳台滑雪](../Page/跳台滑雪.md "wikilink")

<!-- end list -->

  - 《100Mジャンパー》收錄
      - 足球小將 短編版 種類：男子[足球](../Page/足球.md "wikilink")
      - 昴（すばる）種類：[拳擊](../Page/拳擊.md "wikilink")
      - 初戀同士 種類：[劍道](../Page/劍道.md "wikilink")
      - 轟元太一本勝負 種類：[柔道](../Page/柔道.md "wikilink")
  - 誇り 〜プライド〜（2011年 - ）種類：足球
  - 門將教練（）種類：足球，[文化傳信出版](../Page/文化傳信.md "wikilink")。
  - 攻・守！－KOSHU！－ 種類：足球
  - 傲-PRIDE：[玉皇朝出版](../Page/玉皇朝.md "wikilink")。

### 小說

  - ゴールデンキッズ（ゴマブックス、2008年7月）種類：足球
      - 表紙與插畫由高橋擔任。[守門員當主角的作品](../Page/守門員_\(足球\).md "wikilink")。
  - 足球少女 楓，原名《サッカー少女
    楓》（[講談社](../Page/講談社.md "wikilink")、2011年8月）種類：[女子足球](../Page/女子足球.md "wikilink")

## 廣告

  - TOYOTA「トビラを開けよう」キャンペーン（2006年）
  - [brazuca Around The World: Japan -- adidas
    Football](https://www.youtube.com/watch?v=FU6nOces9Ns) adidas
    Youtube (2014)

## 關連項目

**老師**

  -
  -
**助手**

  - [和月伸宏](../Page/和月伸宏.md "wikilink")

## 外部連結

  -
[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:1960年出生](../Category/1960年出生.md "wikilink")