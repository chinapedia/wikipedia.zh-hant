**英格蘭足球冠軍聯賽**（）或加冠名[贊助稱為](../Page/贊助.md "wikilink")**天空博彩冠軍聯賽**（**Sky
Bet Football League Championship**），簡稱**英冠**（**The
Championship**）。是[英格蘭足球聯賽第二級別的聯賽](../Page/英格蘭足球聯賽.md "wikilink")，僅次於[英格蘭超級聯賽](../Page/英格蘭超級聯賽.md "wikilink")。

## 賽制

英冠共有 24 支球隊，在同一球季內，球隊之間互相對賽兩次，主客各一，勝方獲得 3 分，負方 0 分，打和各得 1
分，球隊以獲得分數排列而成績分榜。聯賽績分榜以獲得分數、[得失球差及入球數目順序編排名次](../Page/得失球差.md "wikilink")，如以上三項完全相同，需以附加賽決定名次。

英冠採用升三降三制，球季結束時績分榜最高的頭兩名，連同第三至第六名透過附加賽勝出的球隊升上[英超](../Page/英格蘭超級聯賽.md "wikilink")，其位置由[英超排尾的三隊取代](../Page/英格蘭超級聯賽.md "wikilink")。同時榜末的三隊就會降入[英甲](../Page/英格蘭足球甲級聯賽.md "wikilink")，其位置由[英甲頭兩名及第三至第六名透過附加賽勝出的球隊取代](../Page/英格蘭足球甲級聯賽.md "wikilink")。

## 歷史

英冠在2004/05年球季成立取代之前的[甲組足球聯賽（第二級）](../Page/英格兰足球联赛.md "wikilink")。首屆冠軍由[-{zh-hans:桑德兰;
zh-hk:新特蘭;}-奪得](../Page/桑德兰足球俱乐部.md "wikilink")。

英冠在首個賽季共吸引980萬球迷入場觀賞比賽，在歐洲排名第四位最受歡迎的足球聯賽，僅次於同屬英格蘭的[英超聯](../Page/英格蘭超級聯賽.md "wikilink")（1,288萬）、西班牙的[西甲](../Page/西班牙足球甲級聯賽.md "wikilink")（1157萬）及德國的[德甲](../Page/德國足球甲級聯賽.md "wikilink")（1092萬），而壓過義大利的[意甲](../Page/意大利足球甲级联赛.md "wikilink")（977萬）及法國的[法甲](../Page/法國足球甲級聯賽.md "wikilink")（817萬）。

### 獎盃

英冠的獎盃亦即其前身甲組聯賽頒發的獎盃，暱稱｢夫人｣（*The
Lady*），是全球體育項目最古老的錦標之一，由位於[伯明罕的家族公司瓦格顿](../Page/伯明罕.md "wikilink")（Vaughton's）的工匠為1890/91年球季的冠軍設計和製造，雖然[普雷斯頓贏得首兩屆的足球聯賽冠軍](../Page/普雷斯顿足球俱乐部.md "wikilink")，[-{zh-hans:埃弗顿;zh-hk:愛華頓;}-是首支高舉這個獎盃的球隊](../Page/埃弗顿足球俱乐部.md "wikilink")。獎盃正面刻有12支足球聯賽創始成員名字，而背面則為一場足球比賽的情景\[1\]。

### 贊助

2007年3月12日[可口可樂與英格蘭足球聯賽延長贊助合約到](../Page/可口可樂.md "wikilink")2009/10年球季\[2\]。2009年9月30日[可口可樂與英格蘭足球聯賽發表聯合聲明表示不會延續於](../Page/可口可樂.md "wikilink")2009/10年屆滿的冠名[贊助合約](../Page/贊助.md "wikilink")<small>\[3\]</small>。由2010/11年球季與[電力供應商](../Page/電力.md "wikilink")「*npower*」開始為期三年的冠名贊助<small>\[4\]</small>。

2013年2月26日，[英冠](../Page/英冠.md "wikilink")、[英甲及](../Page/英甲.md "wikilink")[英乙冠名贊助商](../Page/英乙.md "wikilink")「*npower*」宣佈不再續約贊助<small>\[5\]</small>。

2013年7月15日，與博彩公司「Sky Bet」由2014/15年球季開始為期五年的冠名贊助。

<sup>備註：英冠有24支參賽球隊，而意甲及法甲只有20隊。</sup>

## 參賽球隊

2018–19年英格蘭足球冠軍聯賽參賽隊伍共有 24 隊。

<table>
<thead>
<tr class="header">
<th><p>中文名稱</p></th>
<th><p>英文名稱</p></th>
<th><p>所在城市</p></th>
<th><p>上季成績</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/史雲斯城足球會.md" title="wikilink">史雲斯</a></p></td>
<td><p>Swansea City A.F.C.</p></td>
<td><p><a href="../Page/史雲斯.md" title="wikilink">史雲斯</a></p></td>
<td><p>英超，第 18 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯托克城足球俱樂部.md" title="wikilink">史篤城</a></p></td>
<td><p>Stoke City F.C.</p></td>
<td><p><a href="../Page/斯托克.md" title="wikilink">斯托克</a></p></td>
<td><p>英超，第 19 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西布羅姆維奇足球俱樂部.md" title="wikilink">西布朗</a></p></td>
<td><p>West Bromwich Albion F.C.</p></td>
<td><p><a href="../Page/西布羅米奇.md" title="wikilink">西布羅米奇</a></p></td>
<td><p>英超，第 20 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿斯顿維拉足球會.md" title="wikilink">阿士東維拉</a></p></td>
<td><p>Aston Villa F.C.</p></td>
<td><p><a href="../Page/伯明翰.md" title="wikilink">伯明翰</a></p></td>
<td><p>第 4 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/米德爾斯堡足球俱樂部.md" title="wikilink">米杜士堡</a></p></td>
<td><p>Middlesbrough F.C.</p></td>
<td><p><a href="../Page/米德爾斯堡.md" title="wikilink">米德爾斯堡</a></p></td>
<td><p>第 5 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/德比郡足球俱樂部.md" title="wikilink">打吡郡</a></p></td>
<td><p>Derby County F.C.</p></td>
<td><p><a href="../Page/德比.md" title="wikilink">德比</a></p></td>
<td><p>第 6 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/普雷斯頓足球俱樂部.md" title="wikilink">普雷斯頓</a></p></td>
<td><p>Preston North End F.C.</p></td>
<td><p><a href="../Page/普雷斯頓.md" title="wikilink">普雷斯頓</a></p></td>
<td><p>第 7 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/米爾沃爾足球俱樂部.md" title="wikilink">米禾爾</a></p></td>
<td><p>Millwall F.C.</p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
<td><p>第 8 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/謝菲爾德聯足球俱樂部.md" title="wikilink">錫菲聯</a></p></td>
<td><p>Sheffield United F.C.</p></td>
<td><p><a href="../Page/謝菲爾德.md" title="wikilink">謝菲爾德</a></p></td>
<td><p>第 9 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布倫特福德足球俱樂部.md" title="wikilink">賓福特</a></p></td>
<td><p>Brentford F.C.</p></td>
<td><p><a href="../Page/布倫特福德.md" title="wikilink">布倫特福德</a></p></td>
<td><p>第 10 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布里斯托爾城足球俱樂部.md" title="wikilink">布里斯托城</a></p></td>
<td><p>Bristol City F.C.</p></td>
<td><p><a href="../Page/布里斯托爾.md" title="wikilink">布里斯托爾</a></p></td>
<td><p>第 11 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伊普斯維奇城足球俱樂部.md" title="wikilink">葉士域治</a></p></td>
<td><p>Ipswich Town F.C.</p></td>
<td><p><a href="../Page/伊普斯威奇.md" title="wikilink">伊普斯威奇</a></p></td>
<td><p>第 12 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/列斯聯足球會.md" title="wikilink">列斯聯</a></p></td>
<td><p>Leeds United F.C.</p></td>
<td><p><a href="../Page/列斯.md" title="wikilink">列斯</a></p></td>
<td><p>第 13 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/諾里奇城足球俱樂部.md" title="wikilink">諾域治</a></p></td>
<td><p>Norwich City F.C.</p></td>
<td><p><a href="../Page/諾里奇.md" title="wikilink">諾里奇</a></p></td>
<td><p>第 14 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/謝周三足球俱樂部.md" title="wikilink">錫周三</a></p></td>
<td><p>Sheffield Wednesday F.C.</p></td>
<td><p><a href="../Page/謝菲爾德.md" title="wikilink">謝菲爾德</a></p></td>
<td><p>第 15 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/女王公園巡游者足球俱樂部.md" title="wikilink">昆士柏流浪</a></p></td>
<td><p>Queens Park Rangers F.C.</p></td>
<td><p><a href="../Page/倫敦.md" title="wikilink">倫敦</a></p></td>
<td><p>第 16 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/諾丁漢森林足球俱樂部.md" title="wikilink">諾定咸森林</a></p></td>
<td><p>Nottingham Forest F.C.</p></td>
<td><p><a href="../Page/諾定咸.md" title="wikilink">諾定咸</a></p></td>
<td><p>第 17 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/赫爾城足球俱樂部.md" title="wikilink">候城</a></p></td>
<td><p>Hull City A.F.C.</p></td>
<td><p><a href="../Page/赫爾河畔京士頓.md" title="wikilink">赫爾河畔京士頓</a></p></td>
<td><p>第 18 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伯明翰足球會.md" title="wikilink">伯明翰</a></p></td>
<td><p>Birmingham City F.C.</p></td>
<td><p><a href="../Page/伯明翰.md" title="wikilink">伯明翰</a></p></td>
<td><p>第 19 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷丁足球俱樂部.md" title="wikilink">雷丁</a></p></td>
<td><p>Reading F.C.</p></td>
<td><p><a href="../Page/雷丁.md" title="wikilink">雷丁</a></p></td>
<td><p>第 20 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/博爾頓足球俱樂部.md" title="wikilink">保頓</a></p></td>
<td><p>Bolton Wanderers F.C.</p></td>
<td><p><a href="../Page/博爾頓.md" title="wikilink">博爾頓</a></p></td>
<td><p>第 21 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/威根競技足球俱樂部.md" title="wikilink">韋根</a></p></td>
<td><p>Wigan Athletic F.C.</p></td>
<td><p><a href="../Page/威根.md" title="wikilink">威根</a></p></td>
<td><p>英甲，第 1 位</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布萊克本流浪者足球俱樂部.md" title="wikilink">布力般流浪</a></p></td>
<td><p>Blackburn Rovers F.C.</p></td>
<td><p><a href="../Page/布萊克本.md" title="wikilink">布萊克本</a></p></td>
<td><p>英甲，第 2 位</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅瑟勒姆足球俱樂部.md" title="wikilink">洛達咸</a></p></td>
<td><p>Rotherham United F.C.</p></td>
<td><p><a href="../Page/羅瑟勒姆.md" title="wikilink">羅瑟勒姆</a></p></td>
<td><p>英甲，附加賽勝方</p></td>
</tr>
</tbody>
</table>

## 英冠盟主

<table>
<thead>
<tr class="header">
<th><p>賽季</p></th>
<th><p>冠軍</p></th>
<th><p>亞軍</p></th>
<th><p>附加賽冠軍</p></th>
<th><p>其他附加賽參賽球隊</p></th>
<th><p>神射手</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/2004年至2005年英格蘭足球冠軍聯賽.md" title="wikilink">2004–05</a></p></td>
<td><p><a href="../Page/桑德蘭足球俱樂部.md" title="wikilink">新特蘭</a></p></td>
<td><p><a href="../Page/威根競技足球俱樂部.md" title="wikilink">韋根</a></p></td>
<td><p><a href="../Page/西漢姆聯足球俱樂部.md" title="wikilink">韋斯咸</a></p></td>
<td><p><a href="../Page/普雷斯頓足球俱樂部.md" title="wikilink">普雷斯頓</a>、<a href="../Page/德比郡足球俱樂部.md" title="wikilink">打比郡</a>、<a href="../Page/伊普斯維奇城足球俱樂部.md" title="wikilink">葉士域治</a></p></td>
<td><p>，<a href="../Page/維甘竞技.md" title="wikilink">韋根</a>（24 球）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2005年至2006年英格蘭足球冠軍聯賽.md" title="wikilink">2005–06</a></p></td>
<td><p><a href="../Page/雷丁足球俱樂部.md" title="wikilink">雷丁</a></p></td>
<td><p><a href="../Page/謝菲爾德聯足球俱樂部.md" title="wikilink">錫菲聯</a></p></td>
<td><p><a href="../Page/沃特福德足球俱樂部.md" title="wikilink">屈福特</a></p></td>
<td><p><a href="../Page/水晶宫足球俱樂部.md" title="wikilink">水晶宮</a> 、<a href="../Page/利兹聯足球俱樂部.md" title="wikilink">列斯聯</a>、<a href="../Page/普雷斯頓足球俱樂部.md" title="wikilink">普雷斯頓</a></p></td>
<td><p><a href="../Page/馬龍·京治.md" title="wikilink">-{zh-hans:马龙·京治; zh-hk:馬龍·京治;}-</a>，<a href="../Page/沃特福德足球俱樂部.md" title="wikilink">屈福特</a>（21 球）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2006年至2007年英格蘭足球冠軍聯賽.md" title="wikilink">2006–07</a></p></td>
<td><p><a href="../Page/桑德蘭足球俱樂部.md" title="wikilink">新特蘭</a></p></td>
<td><p><a href="../Page/伯明翰足球俱樂部.md" title="wikilink">伯明翰</a></p></td>
<td><p>| <a href="../Page/德比郡足球俱樂部.md" title="wikilink">打比郡</a></p></td>
<td><p><a href="../Page/西布羅姆維奇足球俱樂部.md" title="wikilink">西布朗</a>、<a href="../Page/南安普頓足球俱樂部.md" title="wikilink">修咸頓</a>、<a href="../Page/伍爾弗漢普頓流浪足球俱樂部.md" title="wikilink">狼隊</a></p></td>
<td><p>，<a href="../Page/科爾切斯特聯足球俱樂部.md" title="wikilink">高車士打</a>（23 球）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年至2008年英格蘭足球冠軍聯賽.md" title="wikilink">2007–08</a></p></td>
<td><p><a href="../Page/西布羅姆維奇足球俱樂部.md" title="wikilink">西布朗</a></p></td>
<td><p><a href="../Page/斯托克城足球俱樂部.md" title="wikilink">史篤城</a></p></td>
<td><p><a href="../Page/赫爾城足球俱樂部.md" title="wikilink">侯城</a></p></td>
<td><p><a href="../Page/布里斯托爾城足球俱樂部.md" title="wikilink">布里斯托城</a>、<a href="../Page/水晶宫足球俱樂部.md" title="wikilink">水晶宮</a>、<a href="../Page/沃特福德足球俱樂部.md" title="wikilink">屈福特</a></p></td>
<td><p><a href="../Page/西爾万·埃班克斯-布萊克.md" title="wikilink">-{zh-hans:西爾万·埃班克斯-布萊克; zh-hant:比歷克;}-</a>，<a href="../Page/伍爾弗漢普頓流浪足球俱樂部.md" title="wikilink">狼隊</a>（23 球）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008年至2009年英格蘭足球冠軍聯賽.md" title="wikilink">2008–09</a></p></td>
<td><p><a href="../Page/伍爾弗漢普頓流浪足球俱樂部.md" title="wikilink">狼隊</a></p></td>
<td><p><a href="../Page/伯明翰城足球俱樂部.md" title="wikilink">伯明翰</a></p></td>
<td><p><a href="../Page/伯恩利足球俱樂部.md" title="wikilink">般尼</a></p></td>
<td><p><a href="../Page/謝菲爾德聯足球俱樂部.md" title="wikilink">錫菲聯</a>、<a href="../Page/雷丁足球俱樂部.md" title="wikilink">雷丁</a>、<a href="../Page/普雷斯頓足球俱樂部.md" title="wikilink">普雷斯頓</a></p></td>
<td><p><a href="../Page/西爾万·埃班克斯-布萊克.md" title="wikilink">-{zh-hans:西爾万·埃班克斯-布萊克; zh-hant:比歷克;}-</a>，<a href="../Page/伍爾弗漢普頓流浪足球俱樂部.md" title="wikilink">狼隊</a>（25 球）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年至2010年英格蘭足球冠軍聯賽.md" title="wikilink">2009–10</a></p></td>
<td><p><a href="../Page/紐卡斯爾聯足球俱樂部.md" title="wikilink">紐卡素</a></p></td>
<td><p><a href="../Page/西布羅姆維奇足球俱樂部.md" title="wikilink">西布朗</a></p></td>
<td><p><a href="../Page/黑池足球會.md" title="wikilink">黑池</a></p></td>
<td><p><a href="../Page/卡迪夫城足球俱樂部.md" title="wikilink">卡迪夫城</a>、<a href="../Page/萊斯特城足球俱樂部.md" title="wikilink">李斯特城</a>、<a href="../Page/諾丁漢森林足球俱樂部.md" title="wikilink">諾定咸森林</a></p></td>
<td><p><a href="../Page/彼得·惠廷厄姆.md" title="wikilink">-{zh-hans:惠廷厄姆; zh-hk:韋定咸;}-</a>，<a href="../Page/卡迪夫城足球俱樂部.md" title="wikilink">卡迪夫城</a>（21 球）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年至2011年英格蘭足球冠軍聯賽.md" title="wikilink">2010–11</a></p></td>
<td><p><a href="../Page/女王公園巡游者足球俱樂部.md" title="wikilink">昆士柏流浪</a></p></td>
<td><p><a href="../Page/諾維奇城足球俱樂部.md" title="wikilink">諾域治</a></p></td>
<td><p><a href="../Page/斯旺西足球俱樂部.md" title="wikilink">史雲斯</a></p></td>
<td><p><a href="../Page/卡迪夫城足球俱樂部.md" title="wikilink">卡迪夫城</a>、<a href="../Page/雷丁足球俱樂部.md" title="wikilink">雷丁</a>、<a href="../Page/諾丁漢森林足球俱樂部.md" title="wikilink">諾定咸森林</a></p></td>
<td><p><a href="../Page/丹尼·格拉咸.md" title="wikilink">-{zh-hans:丹尼·格雷厄姆; zh-hk:丹尼·格拉咸;}-</a>，<a href="../Page/沃特福德足球俱樂部.md" title="wikilink">屈福特</a>（24 球）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2011年至2012年英格蘭足球冠軍聯賽.md" title="wikilink">2011–12</a></p></td>
<td><p><a href="../Page/雷丁足球俱樂部.md" title="wikilink">雷丁</a></p></td>
<td><p><a href="../Page/南安普頓足球俱樂部.md" title="wikilink">修咸頓</a></p></td>
<td><p><a href="../Page/西漢姆聯足球俱樂部.md" title="wikilink">韋斯咸</a></p></td>
<td><p><a href="../Page/伯明翰城足球俱樂部.md" title="wikilink">伯明翰</a>、<a href="../Page/黑池足球會.md" title="wikilink">黑池</a>、<a href="../Page/卡迪夫城足球俱樂部.md" title="wikilink">卡迪夫城</a></p></td>
<td><p><a href="../Page/瑞奇·蘭伯特.md" title="wikilink">瑞奇·蘭伯特</a>，<a href="../Page/南安普頓足球俱樂部.md" title="wikilink">修咸頓</a>（27 球）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2012年至2013年英格蘭足球冠軍聯賽.md" title="wikilink">2012–13</a></p></td>
<td><p><a href="../Page/卡迪夫城足球俱樂部.md" title="wikilink">卡迪夫城</a></p></td>
<td><p><a href="../Page/赫爾城足球俱樂部.md" title="wikilink">侯城</a></p></td>
<td><p><a href="../Page/水晶宫足球俱樂部.md" title="wikilink">水晶宮</a></p></td>
<td><p><a href="../Page/沃特福德足球俱樂部.md" title="wikilink">屈福特</a>、<a href="../Page/布萊頓足球俱樂部.md" title="wikilink">白禮頓</a>、<a href="../Page/萊斯特城足球俱樂部.md" title="wikilink">李斯特城</a></p></td>
<td><p><a href="../Page/格連·梅利.md" title="wikilink">-{zh-hans:格連·梅利; zh-hk:格連·梅利;}-</a>，<a href="../Page/水晶宫足球俱樂部.md" title="wikilink">水晶宮</a>（30 球）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2013年至2014年英格蘭足球冠軍聯賽.md" title="wikilink">2013–14</a></p></td>
<td><p><a href="../Page/萊斯特城足球俱樂部.md" title="wikilink">萊斯特城</a></p></td>
<td><p><a href="../Page/伯恩利足球俱樂部.md" title="wikilink">般尼</a></p></td>
<td><p><a href="../Page/女王公園巡游者足球俱樂部.md" title="wikilink">昆士柏流浪</a></p></td>
<td><p><a href="../Page/德比郡足球俱樂部.md" title="wikilink">打比郡</a> 、<a href="../Page/威根競技足球俱樂部.md" title="wikilink">韋根</a> 、<a href="../Page/白禮頓足球俱樂部.md" title="wikilink">白禮頓</a></p></td>
<td><p><a href="../Page/羅斯·麥哥馬克.md" title="wikilink">-{zh-hans:羅斯·麦科马克; zh-hant:羅斯·麥科馬克; zh-hk:羅斯·麥哥馬克;}-</a>，<a href="../Page/列斯聯足球會.md" title="wikilink">列斯聯</a>（28 球）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年至2015年英格蘭足球冠軍聯賽.md" title="wikilink">2014–15</a></p></td>
<td><p><a href="../Page/伯恩茅斯足球俱樂部.md" title="wikilink">般尼茅夫</a></p></td>
<td><p><a href="../Page/沃特福德足球俱樂部.md" title="wikilink">屈福特</a></p></td>
<td><p><a href="../Page/諾里奇城足球俱樂部.md" title="wikilink">諾里奇城</a></p></td>
<td><p><a href="../Page/米德爾斯堡足球俱樂部.md" title="wikilink">米杜士堡</a>、<a href="../Page/布倫特福德足球俱樂部.md" title="wikilink">賓福特</a>、<a href="../Page/伊普斯維奇城足球俱樂部.md" title="wikilink">葉士域治</a></p></td>
<td><p><a href="../Page/戴利·梅菲.md" title="wikilink">-{zh-hans:达里爾·默菲; zh-hk:戴利·梅菲;}-</a>，<a href="../Page/伊普斯維奇城足球俱樂部.md" title="wikilink">葉士域治</a>（27 球）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2015年至2016年英格蘭足球冠軍聯賽.md" title="wikilink">2015–16</a></p></td>
<td><p><a href="../Page/伯恩利足球俱樂部.md" title="wikilink">般尼</a></p></td>
<td><p><a href="../Page/米德爾斯堡足球俱樂部.md" title="wikilink">米杜士堡</a></p></td>
<td><p><a href="../Page/赫爾城足球俱樂部.md" title="wikilink">侯城</a></p></td>
<td><p><a href="../Page/布萊頓足球俱樂部.md" title="wikilink">白禮頓</a>、<a href="../Page/德比郡足球俱樂部.md" title="wikilink">打比郡</a>、<a href="../Page/謝周三足球俱樂部.md" title="wikilink">錫周三</a></p></td>
<td><p><a href="../Page/安德烈·格雷.md" title="wikilink">安德烈·格雷</a>，<a href="../Page/伯恩利足球俱樂部.md" title="wikilink">般尼</a>（25 球）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2016年至2017年英格蘭足球冠軍聯賽.md" title="wikilink">2016–17</a></p></td>
<td><p><a href="../Page/紐卡斯爾聯足球俱樂部.md" title="wikilink">紐卡素</a></p></td>
<td><p><a href="../Page/布萊頓足球俱樂部.md" title="wikilink">白禮頓</a></p></td>
<td><p><a href="../Page/哈特斯菲爾德足球俱樂部.md" title="wikilink">哈特斯菲爾德</a></p></td>
<td><p><a href="../Page/雷丁足球俱樂部.md" title="wikilink">雷丁</a>、<a href="../Page/富咸足球會.md" title="wikilink">富咸</a>、<a href="../Page/謝周三足球俱樂部.md" title="wikilink">錫周三</a></p></td>
<td><p><a href="../Page/克里斯·伍德_(足球运动员).md" title="wikilink">基斯·活特</a>，<a href="../Page/列斯聯足球會.md" title="wikilink">列斯聯</a>（27 球）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2017年至2018年英格蘭足球冠軍聯賽.md" title="wikilink">2017–18</a></p></td>
<td><p><a href="../Page/伍爾弗漢普頓流浪足球俱樂部.md" title="wikilink">狼隊</a></p></td>
<td><p><a href="../Page/卡迪夫城足球俱樂部.md" title="wikilink">卡迪夫城</a></p></td>
<td><p><a href="../Page/富勒姆足球俱樂部.md" title="wikilink">富咸</a></p></td>
<td><p><a href="../Page/阿士東維拉.md" title="wikilink">阿士東維拉</a>、<a href="../Page/米德爾斯堡足球俱樂部.md" title="wikilink">米杜士堡</a>、<a href="../Page/德比郡足球俱樂部.md" title="wikilink">打比郡</a></p></td>
<td><p><a href="../Page/马泰伊·維德拉.md" title="wikilink">馬迪·維達</a>，<a href="../Page/德比郡足球俱樂部.md" title="wikilink">打比郡</a>（21 球）</p></td>
</tr>
</tbody>
</table>

## 参考資料

## 外部連結

  - [英冠聯官方網站](https://www.efl.com/)

[冠](../Category/英格蘭足球聯賽.md "wikilink")

1.
2.  [英聯賽延長贊助合約](http://news.bbc.co.uk/sport2/hi/football/6442317.stm)
3.
4.
5.