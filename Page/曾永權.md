**曾永權**（），出生於[臺灣](../Page/臺灣.md "wikilink")[屏東縣](../Page/屏東縣.md "wikilink")[鹽埔鄉仕絨村](../Page/鹽埔鄉.md "wikilink")，[中華民國政治人物](../Page/中華民國.md "wikilink")，畢業於[逢甲大學紡織工程學系](../Page/逢甲大學.md "wikilink")，現任[中國國民黨](../Page/中國國民黨.md "wikilink")[秘書長](../Page/中國國民黨秘書長.md "wikilink")。曾任[國民大會代表](../Page/國民大會.md "wikilink")、[立法委員](../Page/立法委員.md "wikilink")（1993年－2005年、2008年－2012年）、逢甲大學校友總會總會長、[中華奧林匹克委員會副主席](../Page/中華奧林匹克委員會.md "wikilink")、[立法院副院長](../Page/立法院.md "wikilink")、中國國民黨副主席兼秘書長、[總統府秘書長](../Page/總統府秘書長.md "wikilink")，1989年及1997年兩度參選[屏東縣](../Page/屏東縣.md "wikilink")[縣長](../Page/屏東縣縣長.md "wikilink")，分別敗於[蘇貞昌以及](../Page/蘇貞昌.md "wikilink")[蘇嘉全](../Page/蘇嘉全.md "wikilink")。2012年9月27日，接替[林中森兼任國民黨秘書長](../Page/林中森.md "wikilink")。2012年10月5日，獲[中華民國總統](../Page/中華民國總統.md "wikilink")[馬英九親自贈勳](../Page/馬英九.md "wikilink")、頒發一等[景星勳章](../Page/景星勳章.md "wikilink")。

## 政治爭議

### 疑似介入司法

  - 2007年8月16日，就在檢察官[侯寬仁要不要對](../Page/侯寬仁.md "wikilink")[馬英九特別費官司提起上訴的敏感時刻](../Page/臺北市長馬英九特別費案.md "wikilink")，國民黨立院黨團由政策會執行長曾永權帶隊，包括該黨團書記長[徐少萍](../Page/徐少萍.md "wikilink")、立委[潘維剛](../Page/潘維剛.md "wikilink")、立委[曹爾忠](../Page/曹爾忠.md "wikilink")、立委[周守訓等](../Page/周守訓.md "wikilink")，赴[最高檢察署拜會](../Page/最高檢察署.md "wikilink")[檢察總長](../Page/檢察總長.md "wikilink")[陳聰明](../Page/陳聰明.md "wikilink")，當著鏡頭直接對總長提出要求侯寬仁不要上訴該黨總統候選人馬英九。該黨立委[潘維剛宣稱](../Page/潘維剛.md "wikilink")：「侯寬仁檢察官不該再上訴，是否上訴，檢察總長應該負起整個法律見解，一致性的擔當。」國民黨的護馬大動作，綠營痛批是干預司法、無恥無法！[民進黨立委](../Page/民進黨.md "wikilink")[謝欣霓說](../Page/謝欣霓.md "wikilink")「國民黨才可以這麼汗顏無恥！到陳聰明檢察總長那邊給他施以政治壓力。」\[1\]\[2\]

<!-- end list -->

  - 傳出當日傍晚曾永權奉當時國民黨秘書長[吳敦義之命](../Page/吳敦義.md "wikilink")，與立法院長[王金平會面](../Page/王金平.md "wikilink")，希望由與侯寬仁有遠房姻親關係的王出面，勸侯寬仁不要再上訴馬英九特別費案。曾永權和王金平會晤了約半小時，但遭王金平以不干涉司法、「不能做、也沒能力做」為由斷然拒絕。王金平對黨內高層在他拒絕的次日，就拿他與侯寬仁的親戚關係放話、作文章，感到相當憤怒。他說，侯寬仁是馬英九任法務部長的部屬，關係不是更親密？讓他最生氣的是，那位高層既然要扭曲事實，卻又找人來見他，要他勸請侯寬仁放棄上訴，這到底是什麼意思？\[3\]\[4\]\[5\]\[6\]\[7\]

<!-- end list -->

  - 2013年9月，民進黨立院黨團書記長[吳秉叡以此批評馬英九](../Page/吳秉叡.md "wikilink")，認為此一事證比未經查證、未審先判的王金平狀況嚴重得多了，馬英九如果標準一致的話，就該把曾永權等相關藍委都一併處理一下，難道關說馬英九的案件就沒事嗎？國民黨是這樣的標準嗎？立委[蔡煌瑯則說](../Page/蔡煌瑯.md "wikilink")，王金平一口回絕讓馬英九猶記在心。現在馬英九不必選舉了，就來修理王金平。\[8\]\[9\][國立清華大學學生與社運人士](../Page/國立清華大學.md "wikilink")[陳為廷也在](../Page/陳為廷.md "wikilink")[臉書轉貼新聞道](../Page/臉書.md "wikilink")「2007年總統大選前的新聞。怎麼那時候就不見馬英九出來表態？如果這不叫關說，那什麼才叫關說？如果這不叫無恥，那到底什麼才叫無恥？」\[10\]

### 其他

  - 曾永權在立法院獲選為副院長職位後，購買了4,600CC的轎車；之前因有立委質疑[民進黨官員說](../Page/民進黨.md "wikilink")3000[CC是小車一說](../Page/CC.md "wikilink")，因此，曾永權的4600CC轎車隨即成為爭議焦點；隔了幾日後，[馬英九因參選總統](../Page/馬英九.md "wikilink")，即刻致電曾永權，以「考慮社會觀感」為由，請曾永權將座車換掉，隔日，媒體再不見曾永權的4,600CC轎車\[11\]。

<!-- end list -->

  - [民進黨主席](../Page/民進黨.md "wikilink")[蔡英文在拜會立法院長王金平時](../Page/蔡英文.md "wikilink")，憶起當年與立院副院長曾永權為了通過議案而閉門協商、並且吸著曾永權的二手菸經驗。不過王金平表示「曾永權已經戒菸了！」\[12\]。

<!-- end list -->

  - 2012年9月27日國民黨祕書長交接，黨主席馬英九盛讚新任祕書長曾永權，「個子非常高大、氣宇軒昂、一表人才」，做事細膩，在政界有「媽媽桑」之稱\[13\]。

<!-- end list -->

  - [2014年臺灣高雄氣爆事故](../Page/2014年臺灣高雄氣爆事故.md "wikilink")，曾永權召集黨政小平台會議時，曾對經濟部表示不可讓經濟部處理石化管線，因為這是送舞台給高雄市政府來卸責，引起經濟部長[張家祝心灰](../Page/張家祝.md "wikilink")。\[14\]

<!-- end list -->

  - 2014年11月29日，因國民黨在[九合一大選中慘敗而宣布辭職](../Page/2014年中華民國地方公職人員選舉.md "wikilink")。

### 「不要搭舞台給高雄市政府」

2014年，國民黨執政時有二個黨政平台：中山會報及立院黨團會議，是黨政大平台；各部會次長、主秘與黨秘書長的「協助政務推動工作文宣平台」，則是黨政小平台。

[新新聞報導](../Page/新新聞.md "wikilink")，2014年8月5日，[中國國民黨副主席兼](../Page/中國國民黨副主席.md "wikilink")[秘書長](../Page/中國國民黨秘書長.md "wikilink")、[總統府秘書長](../Page/總統府秘書長.md "wikilink")[曾永權召開黨政小平台會議](../Page/曾永權.md "wikilink")，會議中，曾對經濟部說，「不要搭舞台給高雄市政府，讓經濟部處理石化管線是送舞台給高雄市政府來卸責。」曾永權認為，管線是地方政府的權責，因為縣市政府工務局都有管線科，誰核可就誰管理，高雄市政府責無旁貸，卻在第一時間推給經濟部，經濟部要嚴格防守，不能鬆懈，以免成為地方卸責的最佳藉口。\[15\]

新新聞於報導前曾直接向曾永權查證，惟曾永權當時以正在「主持會報」為由，掛上電話，不再多說。\[16\]

報導刊出後，曾永權僅透過國民黨文傳會以新聞稿做出兩點澄清，未做進一步說明：

## 出席

### 第七屆立法委員任期出席紀錄

  - 2008年2/1\~3/26院會應出席15次，委員會應出席19次，總計34次，缺席19次\[17\]。

### 海峽論壇大會

  - 2011年6月11日，在[中國大陸](../Page/中國大陸.md "wikilink")[福建省](../Page/福建省.md "wikilink")[廈門市出席第三屆](../Page/廈門市.md "wikilink")[海峽論壇大會並發表講話](../Page/海峽論壇大會.md "wikilink")。

## 參考

## 外部連結

  - [立法院副院長 曾永權簡介](http://www.kmt.org.tw/hc.aspx?id=38&pid=112)
  - [立法院 曾永權委員部落格(無名)](http://www.wretch.cc/blog/p30783uc)
  - [立法院 曾永權委員部落格(UDN部落格)](http://blog.udn.com/Tzeng0910)
  - [立法院
    曾永權委員Facebook](http://www.facebook.com/?ref=logo#!/profile.php?id=100000228816350)
  - [立法院副院長 曾永權 Fackbook 粉絲專頁](http://www.facebook.com/Tzeng0910)

|- |colspan="3"
style="text-align:center;"|**[ROC_Legislative_Yuan_Seal.svg](https://zh.wikipedia.org/wiki/File:ROC_Legislative_Yuan_Seal.svg "fig:ROC_Legislative_Yuan_Seal.svg")
[立法院](../Page/中華民國立法院.md "wikilink")** |-    |- |colspan="3"
style="text-align:center;"|**[Emblem_of_Office_of_the_President_ROC.svg](https://zh.wikipedia.org/wiki/File:Emblem_of_Office_of_the_President_ROC.svg "fig:Emblem_of_Office_of_the_President_ROC.svg")
[總統府](../Page/中華民國總統府.md "wikilink")** |-        |- |colspan="3"
style="text-align:center;"|**** |-

[category:中國國民黨黨員](../Page/category:中國國民黨黨員.md "wikilink")

[Category:立法院副院長](../Category/立法院副院長.md "wikilink")
[Category:第2屆中華民國立法委員](../Category/第2屆中華民國立法委員.md "wikilink")
[Category:第3屆中華民國立法委員](../Category/第3屆中華民國立法委員.md "wikilink")
[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:第7屆中華民國立法委員](../Category/第7屆中華民國立法委員.md "wikilink")
[Category:逢甲大學校友](../Category/逢甲大學校友.md "wikilink")
[Category:屏東人](../Category/屏東人.md "wikilink")
[Y永權](../Category/曾姓.md "wikilink")
[Category:中華民國總統府秘書長](../Category/中華民國總統府秘書長.md "wikilink")
[Category:中國國民黨秘書長](../Category/中國國民黨秘書長.md "wikilink")
[Category:中國國民黨副主席](../Category/中國國民黨副主席.md "wikilink")
[Category:太陽花學運反對者](../Category/太陽花學運反對者.md "wikilink")

1.  [特別費／藍見檢察總長　請求放棄上訴馬](http://www.tvbs.com.tw/news/news_list.asp?no=sunkiss20070816165336&dd=201399131830),
    2007/8/16

2.  [馬找王金平叫親戚侯寬仁別再對馬的特別費案上訴不算關說？](http://www.youtube.com/watch?v=aWQszMD2FKc),
    [TVBS](../Page/TVBS.md "wikilink"), 2007/8/16

3.  [綠委：曾永權也曾帶隊關說馬特別費案](http://newtalk.tw/news/2013/09/09/39980.html),
    [新頭殼](../Page/新頭殼.md "wikilink") newtalk, 2013.09.09

4.  [馬特別費也曾涉關說　吳秉叡批藍營標準不一](http://www.ettoday.net/news/20130909/267876.htm),
    [ETtoday](../Page/ETtoday.md "wikilink") newtalk, 2013.09.09

5.  [特別費案：曾永權受命護馬　盼王金平勸侯寬仁別上訴](http://www.ettoday.net/news/20130909/267881.htm),
    [ETtoday](../Page/ETtoday.md "wikilink"), 2013.09.09

6.  [網友翻案　那一年，馬特別費案要王幫忙不成…如今？](http://node.nownews.com.tw/n/2013/09/09/826061),
    [Nownews](../Page/Nownews.md "wikilink"), 2013.09.09

7.
8.
9.
10.
11.

12.

13.

14.

15.
16.

17.