**McFLY**（中文翻譯為：**小飛俠**）在英國倫敦成立於2003年的[英国](../Page/英国.md "wikilink")[流行摇滚樂隊](../Page/流行搖滾.md "wikilink")，凭借着首张单曲《5
Colours In Her Hair》在2004年早期進入流行乐坛，並且一炮而紅。他們是英國最受歡迎和最成功的樂隊之一。
McFLY是由[湯姆·弗萊屈](../Page/湯姆·弗萊屈.md "wikilink")（Tom
Fletcher）創立，4人樂隊成員一直合作至今。[湯姆·弗萊屈](../Page/湯姆·弗萊屈.md "wikilink")（Tom
Fletcher）擔任主唱和結他手，成員[丹尼·瓊斯](../Page/丹尼·瓊斯.md "wikilink")（Danny
Jones）擔任主唱和結他手、他們二人共同為樂隊創作出無數歌曲，包括第一首創作的「Room on the 3rd
Floor」。[道基·波伊特](../Page/道基·波伊特.md "wikilink")（Dougie
Poynter）擔任貝斯手和合音員，以及[哈利·裘德](../Page/哈利·裘德.md "wikilink")（Harry
Judd）擔任鼓手。2008年他们與 Island 唱片公司完成合約後，自行成立名為 Super Record 的唱片公司。他們屬於
Absolute Management 經紀公司旗下。

2004年3月，McFLY得到[霸子樂團](../Page/霸子樂團.md "wikilink")（Busted）的幫助，被邀請作為巡迴演唱會嘉賓亮相表演，開始打響知名度。McFLY的首張專輯《Room
on the 3rd
Floor》登上英國唱片榜第一位和雙白金銷量，打破了[英國著名樂隊](../Page/英國.md "wikilink")[披頭四](../Page/披頭四.md "wikilink")（The
Beatles）的紀錄，成為[英國歷史上推出首張專輯便取得唱片榜第一名的最年輕樂隊](../Page/英國.md "wikilink")。《Room
on the 3rd Floor》發布的一個月之後，樂隊開始了第一個在英國領銜表演的巡迴演唱會。

2005年，第二張專輯《Wonderland》取得英國唱片榜的第1名。同年他們獲得了《BRITISH POP
ACT》[全英音樂獎](../Page/全英音樂獎.md "wikilink")。2006年推出第三張專輯《Motion
in the Ocean》取得第6名。2007年11月5日推出第一張精選專輯《All the Greatest
Hits》取得第4名。他們在2008年7月推出了第4張專輯《Radio:Active》隨《星期日郵報》免費附送，這是他們自立唱片公司後的第一張專輯。他們於2010年11月15日推出他們第五張專輯《Above
the
Noise》取得第4名。截至2014年2月，McFly已經推出了18首連續在英國單曲榜排名前20位的單曲，當中有7首歌曲取得第1名的佳績，以及17首連續前10位的單曲。目前McFLY共推出5張專輯，取得英國唱片榜的第1名包括《Room
on the 3rd Floor》以及《Wonderland》。

2006年，McFLY參與《[幸運咀對咀](../Page/幸運咀對咀.md "wikilink")》（Just My
Luck）的電影演出，電影由[蓮茜·露漢](../Page/蓮茜·露漢.md "wikilink")（Lindsay
Lohan）和[克里斯·潘恩](../Page/克里斯·潘恩.md "wikilink")（Chris
Pine）主演。他們在電影中扮演和現實一樣的角色，飾演McFLY自己，是男主角培訓的樂團McFLY。推出電影同名的全美專輯《Just
My
Luck》作為電影的配樂，把音樂推展到美國。鼓手[哈利·裘德後來承認曾經和](../Page/哈利·裘德.md "wikilink")[蓮茜·露漢約會](../Page/蓮茜·露漢.md "wikilink")。

McFLY熱心公益，參與各種慈善活動的演出，包括: [喜劇救濟](../Page/喜劇救濟.md "wikilink")（Comic
Relief），[有需要的兒童](../Page/有需要的兒童.md "wikilink")（Children in
Need），[現場八方](../Page/現場八方.md "wikilink") （Live 8），Sport
Relief 以及[地球一小時](../Page/地球一小時.md "wikilink")（Earth Hour）。

而McFLY主唱[湯姆·弗萊屈是樂隊歌曲創作的靈魂人物](../Page/湯姆·弗萊屈.md "wikilink")，作為一個專業的作詞人和作曲人，在他十二年的創作生涯，他更是最年輕的歌手於英國單曲榜10次奪冠和寫出了21首十大單曲，打破[羅比·威廉斯的紀錄](../Page/羅比·威廉斯.md "wikilink")。（指參與創作，包括McFLY的7首冠軍單曲及為[霸子所寫的](../Page/霸子.md "wikilink")「Thunderbirds
Are Go」，「Who's David」及 「Crashed The
Wedding」）他其後更為其他年輕樂隊寫歌，包括[一世代](../Page/一世代.md "wikilink")（One
Direction）和[到暑五秒](../Page/到暑五秒.md "wikilink")（5 Seconds of Summer）。

[湯姆·弗萊屈與隊友](../Page/湯姆·弗萊屈.md "wikilink")[道基·波伊特合作在](../Page/道基·波伊特.md "wikilink")2012年發行了第一本兒童小說《*The
Dinosaur that Pooped Christmas*》，銷售超過72000本。

2012年10月11日，McFly發行了他們的第一本自傳《Unsaid Things: Our Story》，以他們第一張專輯《Room on
the 3rd Floor》的第一首歌「Unsaid Things」命名。
他們在青少年的時候就一躍大紅大紫，不得不盡快適應。終於，他們決定講述自己的故事，分享他們的童年故事，不同的個人經歷，愛情故事，樂隊的成功，甚至他們面對沿途的挑戰。當然，還有他們珍貴的友情和重要的音樂。
McFly的自傳首次披露樂隊不為人知知的故事，他們的生活充滿很多笑聲和偶爾出現的淚水，生活點滴由加入McFly的樂隊成員開始成為隊友，並成為生命中最好的朋友。他們的友情都寫在自傳，從每一個頁面直到結局，他們都了解彼此。

2013年11月11日，McFly宣佈與[霸子樂團](../Page/霸子樂團.md "wikilink")（Busted）聯手組成6人組合[McBusted](../Page/McBusted.md "wikilink")，展開了34場的世界巡迴演唱會。但曾作為霸子樂團成員之一的歌手[查理·辛普森](../Page/查理·辛普森.md "wikilink")（Charlie
Simpson）並沒有參與這個新陣容。2014年10月以[McBusted身份推出專輯](../Page/McBusted.md "wikilink")《Air
Guitar》取得第9名。

目前，根據BPI認證，McFLY在英國出售至少150萬張專輯，在全球的唱片銷量超過1000萬張。樂迷仍在期待McFly承諾推出的第六張專輯......

## 历史

### 2002：早期

「McFly」是由乐队的创立者[湯姆·弗萊屈所命名的](../Page/湯姆·弗萊屈.md "wikilink")，来源就是他最喜欢的美国科幻系列喜剧《[回到未来](../Page/回到未来.md "wikilink")》（*Back
to the
Future*）中饰演[马蒂·迈克的](../Page/马蒂·迈克.md "wikilink")[迈克尔·J·福克斯](../Page/迈克尔·J·福克斯.md "wikilink")（Michael
J.
Fox）。湯姆原本是想參加[霸子樂團](../Page/霸子樂團.md "wikilink")（Busted）的成员徵選，但是遭到拒绝。不過霸子樂團的经纪人还是与湯姆签约，并將他安排在另一个類似的樂團中。霸子樂團的主唱[詹姆斯·波尼](../Page/詹姆斯·波尼.md "wikilink")（James
Bourne）这个时候开始和他一起写歌。之後在詹姆斯忙於自己的事業，无暇顾及湯姆时，丹尼出现了。刚开始的时候，他們彼此並不熟悉，丹尼说在霸子的演唱会上看见过湯姆。于是在公司的安排下，二人开始在一起写歌。这个时候他们刚滿十五岁。丹尼的家在[博爾頓](../Page/博爾頓.md "wikilink")（Bolton），而湯姆的家在[伦敦](../Page/伦敦.md "wikilink")，很多的时间都被浪费在搭乘火車交通往返上，丹尼感觉压力很大。

当湯姆从[Silvia
young毕业后](../Page/Silvia_young.md "wikilink")，丹尼还在家乡的酒吧里面唱歌，并时常为他妹妹的朋友写一些东西。公司为了让湯姆和丹尼更好的创作，暑假的时候公司给丹尼在伦敦租了一个旅馆的房间。就这样，湯姆和丹尼度过了一个愉快的暑假。而这也是「Room
on the 3rd
Floor」（意為在三樓的房間）這首歌曲的名稱由来。为了使这个乐队完整，他们俩开始在伦敦中心区的广场招募鼓手和貝斯手。而在那些被拒绝的人当中甚至包括了[史帝芬·派克](../Page/史帝芬·派克.md "wikilink")（Steven
Parker）。[道基·波伊特](../Page/道基·波伊特.md "wikilink")（Dougie
Poynter）和[哈利·裘德](../Page/哈利·裘德.md "wikilink")（Harry
Judd）是在2003年9月份的同一天加入这个乐队的。这时，McFly正式成立了。

### *2003-2004：Room on the 3rd Floor*

乐队早期的风格是[流行和](../Page/流行音樂.md "wikilink")[龐克的混合体](../Page/朋克.md "wikilink")。而在2004年3月24日發行的單曲《*5
Colour In Her
Hair*》，很快就登上[英国单曲榜首位并持续了两周之久](../Page/英国单曲榜.md "wikilink")。在七月的时候《*Obviously*》也成为第一名的单曲。而随后的专辑《*Room
on the 3rd
Floor*》更打破了记录，McFly成为了首张专辑就登上榜单首位的最年轻的乐队。第三首单曲，在2004年9月6日發行的《*That
Girl*》， 输給了[布萊恩·麥克法登的](../Page/布萊恩·麥克法登.md "wikilink")《*Real To
Me*》，屈居于第三位。2004年11月15日發行的第四首单曲《*Room on the 3rd
Floor*》（专辑的主打歌）取得个第五名的成绩。

### *2005-2006：Wonderland*

在2005年3月的时候，他们推出了[雙主打單曲](../Page/雙主打單曲.md "wikilink")《*All About You /
You've Got A
Friend*》，也是他們的第六和第七首单曲，也是他们的第二张专辑。这张专辑的特色是60％都是[交响乐](../Page/交响乐.md "wikilink")。單曲在2005年3月13日獲得該週排行榜的第一名。同时，专辑的收入也捐献给[喜劇救濟](../Page/喜劇救濟.md "wikilink")。为了響應慈善帮助，Mcfly在[非洲的](../Page/非洲.md "wikilink")[乌干达拍摄了](../Page/乌干达.md "wikilink")「*You've
Got A Friend*」的音乐录影带。在2005年10月他们完成首次的舞台演出。《*Wonderland*》这张专辑的第二首单曲「*I'll
Be
OK*」于2005年8月15日在英国推出，并第四次荣膺排行榜第一。而专辑本身则于2005年8月29日第一次登上第一位。《*Wonderland*》的第三首单曲「*I
Wanna Hold You*」于10月17日推出，
给他们带来了第六次榜单前三名的好成绩，[北極潑猴](../Page/北極潑猴.md "wikilink")（Arctic
Monkeys）被从第一的位置挤下来。他们的第四首单曲《*Ultraviolet / The Ballad of Paul
K*》于同年12月12日推出，并得到第九名的成绩。

### *2006-2007：Motion in the Ocean*

《Motion in the Ocean》的第一首单曲叫做《*Please,
Please*》，与[皇后合唱團](../Page/皇后合唱團.md "wikilink")（Queen）的《*Don't
Stop Me Now*》在2006年7月17日作为「Sport Relief
2006」慈善活動的官方歌曲出了一张[雙主打單曲](../Page/雙主打單曲.md "wikilink")。而这張單曲在2006年7月23日獲得英国排行榜第一名的成績，
而乐队也得到了创队以来的第五次排行榜冠军。在與同志夜店「[G-A-Y](../Page/G-A-Y.md "wikilink")」的老闆打赌輸掉之后，McFly不得不光着身子在舞台上表演。在2006年9月4日，McFly的官方网站证实第三张专辑就叫做《*Motion
in the Ocean*》。这张专辑于2006年11月6日推出，达到第六名的成绩。接著《*Star Girl*》于
2006年10月23日發行，《*Star
Girl*》凭借着54,802的销售量第一周就达到冠军头衔。第三首专辑里面的曲目《*Sorry's
Not Good Enough / Friday
Night*》于2006年[12月18推出](../Page/12月18.md "wikilink")，并取得榜单第三的成绩。
而2006年12月22日在[加拿大和](../Page/加拿大.md "wikilink")[美国推出的](../Page/美国.md "wikilink")「*Friday
Night*」则是电影《[博物館驚魂夜](../Page/博物館驚魂夜.md "wikilink")》（*Night at the
Museum*）的主打曲目。他们单曲《*Transylvania*》亦取得第一名。

### *2007-2008：All the Greatest Hits*

**McFly**於2007年11月5日推出精選專輯，內附有三首新歌曲，包括"The Heart Never Lies"、"The Way
You Make Me Feel"及"Don't Wake Me Up".
並且取得[英國唱片榜第三名](../Page/英國.md "wikilink")。其單曲[The
Heart Never
Lies取得第三名的佳績](../Page/The_Heart_Never_Lies.md "wikilink")。其後樂隊舉行了26場的演唱會。

### *2008-2009：Radio:Active*

《Radio:Active》於2008年7月20日隨《星期日郵報》免費附送。這是他們第四張專輯，首支單曲《*One for the
Radio*》暫時只取得第二名，據說因為HMV公司的關係使他們失去3,000張單曲銷量，故只能屈居第二，但未經証實。第二首單曲《*Lies*》將於9月15日發行。9月22日亦將會發行新版本《Radio:Active》，到時隨了保留原本十首歌外，亦會有四首新歌曲、一張DVD以及一本32頁的小冊子。他們亦將於十月首次到巴西舉行3場演唱會。而在11月7日亦會開始他們的巡迴演唱會。

### *2010-2011：Above the Noise*

《Above the Noise》於2010年11月15日推出他們第五張專輯。第一首单曲叫做《*Party
Girl*》獲得英国排行榜第六名的成績。第二首單曲《*Shine A Light*》与
[泰歐·克魯斯](../Page/泰歐·克魯斯.md "wikilink") Taio
Cruz合作，更獲得英国排行榜第四名的成績。其後樂隊首次舉行了28場的世界巡迴演唱會。

### *2011-現在：第六張錄音室專輯*

2011年8月，McFly聲明他們將在2012年的3-4月在英國進行全國巡演。11月1日McFly聯手Gioia
Jewellery發行了印有每個樂隊成員的SuperCity
Logo的珠寶。12月4日，Dougie贏得了第十一季的我是名人…放我出去！/I’m
a Celebrity…Get Me Out of Here\!的冠軍。12月7日，Harry贏得第九季的舞動奇跡/Strictly Come
Dancing冠軍。 現在正在進行的巡演命名為Keep Calm and Play Louder Tour。他們並演唱了樂隊的三首新歌，“Do
What You Want（也記作Do Whatcha）”，“Red”和“Touch the
Rain”。這三首歌將被收錄于第六張錄音室專輯中。Tom在推特上說下一支發行的單曲應是“Red”。
此張專輯在2012年6月10日開始錄製。
McFly想在九月份到美國舉辦幾次專場。“我們想到美國去，告訴那邊的粉絲我們沒有忘記他們。”湯姆說，“我們一直一直都想這樣做，但我們總是沒有機會，要不然就是別的一些事情會找上門來。”“我們在美國的有些粉絲，已經喜歡我們八九年了——就是我們剛剛組團的時候——他們有的還專程跑到英國去看我們。他們這麼熱情，讓我們感到很驚訝。所以我想這是我們欠他們的。”

## 音乐风格

McFly的音乐风格被他们自己和其它从事音乐的人看作是与Nu-Pop，但是通常其风格被认为是介于流行和流行摇滚之间。凭着乐队的流行印象和好影响，与其它的男孩乐队音乐相比，他们还是有很多的不同的。McFly通常被认为与他们同公司的兄弟乐队Busted很像。实际上霸子乐团的詹姆斯·波尼和Son
Of
Dork联手写了很多小飞侠的主打歌，汤姆·弗莱屈也和詹姆斯·波尼一起创作了很多的歌曲。虽然是相似的，Busted与[Blink-182和Sum](../Page/Blink-182.md "wikilink")41一样，其中的力量要远远的大于20世纪60年代的摇滚乐。

## 作品

  - [Room On The 3rd Floor](../Page/Room_On_The_3rd_Floor.md "wikilink")
    (2004)
  - [Wonderland](../Page/Wonderland（McFly專輯）.md "wikilink") (2005)
  - [Motion In The Ocean](../Page/Motion_In_The_Ocean.md "wikilink")
    (2006)
  - [Radio:ACTIVE](../Page/Radio:ACTIVE.md "wikilink") (2008)
  - [Above The Noise](../Page/Above_The_Noise.md "wikilink") (2010)

## 成员

[湯姆·弗萊屈](../Page/湯姆·弗萊屈.md "wikilink") – 主唱，吉他，鋼琴，貝斯，夏威夷四弦琴

[丹尼·瓊斯](../Page/丹尼·瓊斯.md "wikilink") – 主唱，吉他，口琴，鋼琴

[道基·波伊特](../Page/道基·波伊特.md "wikilink") – 貝斯，吉他，和聲

[哈利·裘德](../Page/哈利·裘德.md "wikilink") – 鼓，打擊樂

## Cover songs

McFly录制和演出了很多的官方的和非官方的知名歌曲。

## 外部連結

  - [官方網站](https://web.archive.org/web/20160422042429/http://supercity.mcfly.com/home/)

  - [McFly 在 MySpace 的網頁](http://www.myspace.com/mcfly)

  - [McFly的Facebook](https://www.facebook.com/mcfly/)

  - [湯姆·弗萊屈Instagram](../Page/湯姆·弗萊屈.md "wikilink"): [@Tom
    Fletcher](https://instagram.com/TOMFLETCHER/)

  - [丹尼·瓊斯Instagram](../Page/丹尼·瓊斯.md "wikilink"): [@Danny
    Jones](https://instagram.com/DANNYJONESMCFLY/)

  - [道基·波伊特Instagram](../Page/道基·波伊特.md "wikilink"): [@Dougie
    Poynter](https://instagram.com/IDOUGAHOLE/)

  - [哈利·裘德Instagram](../Page/哈利·裘德.md "wikilink"): [@Harry
    Judd](https://instagram.com/JUDDYMCFLY/)

[Category:McFly](../Category/McFly.md "wikilink")
[Category:英国摇滚乐团](../Category/英国摇滚乐团.md "wikilink")
[Category:2003年成立的音樂團體](../Category/2003年成立的音樂團體.md "wikilink")
[Category:男子演唱團體](../Category/男子演唱團體.md "wikilink")
[Category:搖滾樂團](../Category/搖滾樂團.md "wikilink")
[Category:流行音樂團體](../Category/流行音樂團體.md "wikilink")
[Category:英國歌手](../Category/英國歌手.md "wikilink")
[Category:英國創作歌手](../Category/英國創作歌手.md "wikilink")
[Category:英國音樂家](../Category/英國音樂家.md "wikilink")
[Category:英國結他手](../Category/英國結他手.md "wikilink")
[Category:英國鋼琴家](../Category/英國鋼琴家.md "wikilink")
[Category:英國之最](../Category/英國之最.md "wikilink")
[Category:英國的世界之最](../Category/英國的世界之最.md "wikilink")
[Category:英國另類搖滾樂隊](../Category/英國另類搖滾樂隊.md "wikilink")
[Category:全英音樂獎](../Category/全英音樂獎.md "wikilink")