**民權東路**是[台北市重要東西向道路之一](../Page/台北市.md "wikilink")，路名取自[中華民國](../Page/中華民國.md "wikilink")[國父](../Page/國父.md "wikilink")[孫中山先生所著](../Page/孫中山.md "wikilink")[三民主義](../Page/三民主義.md "wikilink")（民生、民權、民族）。屬雙向道路，分六段，部份路段設有[公車專用道](../Page/公車專用道.md "wikilink")，西接[民權西路](../Page/民權西路.md "wikilink")；東接[成功路](../Page/成功路.md "wikilink")。

## 行經區域

  - [中山區](../Page/中山區_\(臺北市\).md "wikilink")
  - [松山區](../Page/松山區_\(臺北市\).md "wikilink")
  - [內湖區](../Page/內湖區.md "wikilink")

## 沿線設施

（由西至東）

  - 一段
      - [上海商業儲蓄銀行總部大樓](../Page/上海商業儲蓄銀行.md "wikilink")(2號)
      - [晴光商圈](../Page/晴光商圈.md "wikilink")
      - [臺北市中山區中山國民小學](../Page/臺北市中山區中山國民小學.md "wikilink")(69號)
      - [捷運中山國小站](../Page/中山國小站.md "wikilink")(71號B1)
      - [台北市公共自行車租賃系統](../Page/台北市公共自行車租賃系統.md "wikilink")[捷運中山國小站](../Page/捷運中山國小站.md "wikilink")\[4號出口\]
      - [臺北市立圖書館恆安民眾閱覽室](../Page/臺北市立圖書館恆安民眾閱覽室.md "wikilink")(71巷19號)
      - [臺北市立新興國民中學](../Page/臺北市立新興國民中學.md "wikilink")(正門位於林森北路51號)

<!-- end list -->

  - 二段
      - [台灣展翅協會](../Page/台灣展翅協會.md "wikilink")(26號4樓之5)
      - [亞都麗緻大飯店](../Page/亞都麗緻大飯店.md "wikilink")(41號)
      - [行天宮](../Page/行天宮.md "wikilink")(109號)
      - 原[南山人壽總部大樓](../Page/南山人壽.md "wikilink")(144號)
      - [臺北市第一殯儀館](../Page/臺北市第一殯儀館.md "wikilink")(145號)
      - [順益關係企業總部](../Page/順益關係企業.md "wikilink")(150號)

<!-- end list -->

  - 三段
      - [榮星公園](../Page/榮星公園.md "wikilink")(1號)
      - [台北市公共自行車租賃系統榮星公園站](../Page/台北市公共自行車租賃系統.md "wikilink")
      - [自由巷](../Page/自由巷.md "wikilink")(106巷3弄)
      - 財團法人[鄭南榕基金會](../Page/鄭南榕.md "wikilink")(106巷3弄11號3F)
      - [鼎興營區](../Page/鼎興營區.md "wikilink")(正門位於龍江路323號)
      - [臺北市政府警察局松山分局民有派出所](../Page/臺北市政府警察局.md "wikilink")(162號)

<!-- end list -->

  - 四段
      - 敦北公園
      - [松山機場](../Page/松山機場.md "wikilink")(地址位於敦化北路340-9號)
      - 民權公園
      - [臺北市松山區民權國民小學](../Page/臺北市松山區民權國民小學.md "wikilink")(200號)

<!-- end list -->

  - 五段
      - \<font color="\#00AEAE\>**本路段為水族用品街**</font>
      - [臺北市松山區三民國民小學](../Page/臺北市松山區三民國民小學.md "wikilink")(1號)
      - [民權大橋](../Page/民權大橋.md "wikilink")

<!-- end list -->

  - 六段
      - [臺北市政府消防局第三大隊民權分隊](../Page/臺北市政府消防局.md "wikilink")(9號)
      - [壹電視](../Page/壹電視.md "wikilink")（16號）
      - [時報廣場大樓](../Page/時報廣場大樓.md "wikilink")(25號，[中天電視總部](../Page/中天電視.md "wikilink")、[中視新聞部](../Page/中視新聞.md "wikilink"))
      - 臺北市政府警察局內湖分局文德派出所(26號)
      - [中山高速公路涵洞](../Page/中山高速公路.md "wikilink")
      - [士林地方法院內湖簡易庭](../Page/士林地方法院內湖簡易庭.md "wikilink")(91號)
      - [三民國中](../Page/臺北市立三民國民中學.md "wikilink")(45號)
      - 內湖區行政中心(99號)
          - 內湖戶政事務所(3F)
          - 內湖區公所(4F\~5F)
          - [臺北市立圖書館內湖分館](../Page/臺北市立圖書館.md "wikilink")(6F)
      - 臺北市政府警察局內湖分局偵查隊(101號)
      - [財政部臺北國稅局內湖稽徵所](../Page/財政部臺北國稅局.md "wikilink")(114號)
      - [臺北市內湖區新湖國民小學](../Page/臺北市內湖區新湖國民小學.md "wikilink")(138號)
      - [台北市公共自行車租賃系統新湖國小站](../Page/台北市公共自行車租賃系統.md "wikilink")
      - 成功路圓環
      - [國防醫學院](../Page/國防醫學院.md "wikilink")(161號)
      - [台北市公共自行車租賃系統福華商場站](../Page/台北市公共自行車租賃系統.md "wikilink")(180巷6號前)
      - [臺北市網球中心](../Page/臺北市網球中心.md "wikilink")(208號)
      - [民權隧道](../Page/民權隧道.md "wikilink")
      - 康寧圓環

[File:行天宮1.jpg}行天宮](File:行天宮1.jpg%7D行天宮) <File:MSO-FFP> Jingxing Hall
2006-02.jpg|台北市第一殯儀館 <File:Rongxin> Park Southeast Exit.jpg|榮星公園
<File:Dingxing> military camp.jpg|鼎興營區 <File:Taipei> MRT Jhongshan Jr Hi
Sch Station.JPG|捷運中山國中站(下方為復興北路) <File:Taipei> Songshan Airport clock
tower 20051221.jpg|松山機場 <File:Taipei> Municipal Min Quan Elementary
School North Gate.JPG|民權國小 <File:Minsheng> Community and Minquan East
Road Sec. 5 Birdview from Minquan Bridge.jpg|由民權大橋俯望民權東路五段。

## 參見

  - [臺北市主要道路列表](../Page/臺北市主要道路列表.md "wikilink")

## 外部連結

[M民](../Category/臺北市街道.md "wikilink")