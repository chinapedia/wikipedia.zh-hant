[左營區](../Page/左營區.md "wikilink"){{\!w}}[鼓山區](../Page/鼓山區.md "wikilink"){{\!w}}[三民區](../Page/三民區.md "wikilink"){{\!w}}[鹽埕區](../Page/鹽埕區.md "wikilink"){{\!w}}[前金區](../Page/前金區.md "wikilink"){{\!w}}[新興區](../Page/新興區_\(台灣\).md "wikilink"){{\!w}}[苓雅區](../Page/苓雅區.md "wikilink"){{\!w}}[前鎮區](../Page/前鎮區.md "wikilink"){{\!w}}[旗津區](../Page/旗津區.md "wikilink"){{\!w}}[小港區](../Page/小港區.md "wikilink")

|group2 = [鳳山地區](../Page/鳳山郡.md "wikilink") |list2 =
[鳳山區](../Page/鳳山區.md "wikilink"){{\!w}}[大寮區](../Page/大寮區.md "wikilink"){{\!w}}[鳥松區](../Page/鳥松區.md "wikilink"){{\!w}}[林園區](../Page/林園區.md "wikilink"){{\!w}}[仁武區](../Page/仁武區.md "wikilink"){{\!w}}[大樹區](../Page/大樹區.md "wikilink"){{\!w}}[大社區](../Page/大社區.md "wikilink")

|group3 = [岡山地區](../Page/岡山郡.md "wikilink") |list3 =
[岡山區](../Page/岡山區.md "wikilink"){{\!w}}[路竹區](../Page/路竹區.md "wikilink"){{\!w}}[橋頭區](../Page/橋頭區.md "wikilink"){{\!w}}[梓官區](../Page/梓官區.md "wikilink"){{\!w}}[彌陀區](../Page/彌陀區.md "wikilink"){{\!w}}[永安區](../Page/永安區.md "wikilink"){{\!w}}[燕巢區](../Page/燕巢區.md "wikilink"){{\!w}}[田寮區](../Page/田寮區.md "wikilink"){{\!w}}[阿蓮區](../Page/阿蓮區.md "wikilink"){{\!w}}[茄萣區](../Page/茄萣區.md "wikilink"){{\!w}}[湖內區](../Page/湖內區.md "wikilink")

|group4 = [旗山地區](../Page/旗山郡.md "wikilink") |list4 =
[旗山區](../Page/旗山區.md "wikilink"){{\!w}}[美濃區](../Page/美濃區.md "wikilink"){{\!w}}[內門區](../Page/內門區.md "wikilink"){{\!w}}[杉林區](../Page/杉林區.md "wikilink"){{\!w}}[甲仙區](../Page/甲仙區.md "wikilink"){{\!w}}[六龜區](../Page/六龜區.md "wikilink")

|group5 = [原住民區](../Page/直轄市山地原住民區.md "wikilink") |list5 =
[茂林區](../Page/茂林區.md "wikilink"){{\!w}}[桃源區](../Page/桃源區.md "wikilink"){{\!w}}[那瑪夏區](../Page/那瑪夏區.md "wikilink")

|group6 = [南海諸島](../Page/南海諸島.md "wikilink") |list6 =
[東沙群島](../Page/東沙群島.md "wikilink")（隸屬於旗津區）{{\!w}}[南沙群島之](../Page/南沙群島.md "wikilink")[太平島與](../Page/太平島.md "wikilink")[中洲礁](../Page/中洲礁.md "wikilink")（隸屬於旗津區）

|below =
參見：[高雄歷史](../Page/高雄市歷史.md "wikilink")、[高雄地理](../Page/高雄地理.md "wikilink")、[高雄人口](../Page/高雄人口.md "wikilink")、[高雄交通](../Page/高雄市交通.md "wikilink")、[高雄都會區](../Page/高雄都會區.md "wikilink")
}}<noinclude>

</noinclude>

[\*](../Category/高雄市行政區劃.md "wikilink")
[\*](../Category/高雄市行政區劃模板.md "wikilink")