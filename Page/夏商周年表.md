<noinclude></noinclude>

[2000年](../Page/2000年.md "wikilink")，[中国](../Page/中国.md "wikilink")[夏商周断代工程结题](../Page/夏商周断代工程.md "wikilink")，该工程对[盘庚迁殷至周共和元年](../Page/盘庚.md "wikilink")（[前841年](../Page/前841年.md "wikilink")）之间的君主进行了详细的断代，本处的年表，即为该工程的结果。不过，由于夏商周断代工程存在一些争议，该工程的最后报告繁本也一直未能通过。因此，本处所指的共和之前的年表，不能确定是否符合历史。

## [夏代年表](../Page/夏朝.md "wikilink")

（约[前2070年](../Page/前2070年.md "wikilink")－约[前1600年](../Page/前1600年.md "wikilink")）

[禹](../Page/禹.md "wikilink")、[啟](../Page/啟.md "wikilink")、[太康](../Page/太康_\(夏朝\).md "wikilink")、[中康](../Page/中康.md "wikilink")、[相](../Page/相_\(夏朝\).md "wikilink")、([羿](../Page/羿.md "wikilink")、[寒浞](../Page/寒浞.md "wikilink"))、[少康](../Page/少康_\(夏\).md "wikilink")、[杼](../Page/杼.md "wikilink")、[槐](../Page/槐.md "wikilink")、[芒](../Page/芒.md "wikilink")、[泄](../Page/泄.md "wikilink")、[不降](../Page/不降.md "wikilink")、[扃](../Page/扃.md "wikilink")、[廑](../Page/廑.md "wikilink")、[孔甲](../Page/孔甲.md "wikilink")、[皋](../Page/皋.md "wikilink")、[發](../Page/發_\(夏\).md "wikilink")、[履癸(桀)](../Page/桀.md "wikilink")

## [商代年表](../Page/商朝.md "wikilink")

（约[前1600年](../Page/前1600年.md "wikilink")－约[前1046年](../Page/前1046年.md "wikilink")）

  - [汤](../Page/商汤.md "wikilink")、[太丁](../Page/太丁.md "wikilink")、[外丙](../Page/外丙.md "wikilink")、[仲壬](../Page/仲壬.md "wikilink")、[太甲](../Page/太甲.md "wikilink")、[沃丁](../Page/沃丁.md "wikilink")、[太庚](../Page/太庚.md "wikilink")、[小甲](../Page/小甲.md "wikilink")、[雍己](../Page/雍己.md "wikilink")、[太戊](../Page/太戊.md "wikilink")、[中丁](../Page/中丁.md "wikilink")、[外壬](../Page/外壬.md "wikilink")、[河亶甲](../Page/河亶甲.md "wikilink")、[祖乙](../Page/祖乙.md "wikilink")、[祖辛](../Page/祖辛.md "wikilink")、[沃甲](../Page/沃甲.md "wikilink")、[祖丁](../Page/祖丁.md "wikilink")、[南庚](../Page/南庚.md "wikilink")、[阳甲](../Page/阳甲.md "wikilink")、[盘庚](../Page/盘庚.md "wikilink")（盘庚迁[殷前](../Page/安阳.md "wikilink")）
    （约[前1600年](../Page/前1600年.md "wikilink")－约[前1300年](../Page/前1300年.md "wikilink")）

  - [盘庚](../Page/盘庚.md "wikilink")（盘庚迁[殷后](../Page/安阳.md "wikilink")）、[小辛](../Page/小辛.md "wikilink")、[小乙](../Page/小乙_\(商朝\).md "wikilink")
    （[前1300年](../Page/前1300年.md "wikilink")－[前1251年](../Page/前1251年.md "wikilink")）

  - [武丁](../Page/武丁.md "wikilink")
    （[前1250年](../Page/前1250年.md "wikilink")－[前1192年](../Page/前1192年.md "wikilink")）

  - [祖庚](../Page/祖庚.md "wikilink")、[祖甲](../Page/祖甲.md "wikilink")、[廩辛](../Page/廩辛.md "wikilink")、[康丁](../Page/康丁.md "wikilink")
    （[前1191年](../Page/前1191年.md "wikilink")－[前1148年](../Page/前1148年.md "wikilink")）

  - [武乙](../Page/武乙.md "wikilink")
    （[前1147年](../Page/前1147年.md "wikilink")－[前1113年](../Page/前1113年.md "wikilink")）

  - [太丁（文丁）](../Page/文丁.md "wikilink")
    （[前1112年](../Page/前1112年.md "wikilink")－[前1102年](../Page/前1102年.md "wikilink")）

  - [帝乙](../Page/帝乙.md "wikilink")
    （[前1101年](../Page/前1101年.md "wikilink")－[前1076年](../Page/前1076年.md "wikilink")）

  - [帝辛（紂）](../Page/紂王.md "wikilink")
    （[前1075年](../Page/前1075年.md "wikilink")－[前1046年](../Page/前1046年.md "wikilink")）

  -
### 元年

  - 成汤   前1661年
  - 外丙   前1649年
  - 仲壬   前1647年
  - 太甲   前1645年
  - 沃丁   前1633年
  - 小庚   前1614年
  - 小甲   前1609年
  - 雍己   前1592年
  - 太宗太戊 前1580年
  - 仲丁 前1505年
  - 外壬 前1494年
  - 河亶甲 前1479年
  - 中宗祖乙 前1470年
  - 祖辛 前1420年
  - 沃甲 前1404年
  - 祖丁 前1379年
  - 南庚 前1347年
  - 阳甲 前1318年
  - 盘庚 前1301年
  - 小辛 前1273年
  - 小乙 前1270年
  - 武丁 前1260年
  - 祖庚 前1201年
  - 祖甲 前1190年
  - 冯辛 前1157年
  - 庚丁 前1153年
  - 武乙 前1145年
  - 太丁 前1110年
  - 帝乙 前1094年
  - 帝辛 前1085年

## [西周年表](../Page/西周.md "wikilink")

<noinclude>

## 参见

  - [夏商周断代工程](../Page/夏商周断代工程.md "wikilink")
  - [中国历史年表](../Page/中国历史年表.md "wikilink")

</noinclude>

[Category:夏商周](../Category/夏商周.md "wikilink")
[Category:中国史年表](../Category/中国史年表.md "wikilink")