**英照皇太后**（），是[孝明天皇的女御](../Page/孝明天皇.md "wikilink")，[明治天皇的](../Page/明治天皇.md "wikilink")[嫡母](../Page/嫡庶.md "wikilink")。

婚前名字為**九條夙子**（）

## 簡歷

父親是[關白左大臣](../Page/關白.md "wikilink")[九條尚忠](../Page/九條尚忠.md "wikilink")，母親是[賀茂神社氏人](../Page/賀茂神社.md "wikilink")・[南大路長尹之女菅山](../Page/南大路長尹.md "wikilink")。是左大臣[九條道孝的姊姊](../Page/九條道孝.md "wikilink")，而其姪女則是[貞明皇后](../Page/貞明皇后.md "wikilink")。

誕生於[山城國愛宕郡下鴨村](../Page/山城國.md "wikilink")（現為[京都市](../Page/京都市.md "wikilink")[左京區下鴨](../Page/左京區.md "wikilink")）的南大路家，最初名為基君（のりきみ）。

[弘化二年](../Page/弘化.md "wikilink")（[1845年](../Page/1845年.md "wikilink")）九月十四日，正值十二歲之時，成為年長她兩歲的東宮統仁親王（後來的[孝明天皇](../Page/孝明天皇.md "wikilink")）之妃。結婚翌年，孝明天皇即位；[嘉永元年](../Page/嘉永.md "wikilink")（[1848年](../Page/1848年.md "wikilink")）十二月七日，敘[從三位](../Page/從三位.md "wikilink")，同年十二月十五日進宮，封為[女御](../Page/女御.md "wikilink")。孝明天皇原想立夙子為-{后}-，並先敘任[准三宮](../Page/准三宮.md "wikilink")，但遭[幕府反對](../Page/幕府.md "wikilink")。嘉永六年（[1853年](../Page/1853年.md "wikilink")）五月七日，夙子敘[正三位](../Page/正三位.md "wikilink")、[准三宮](../Page/准三宮.md "wikilink")。

婚後分別生下第一皇女[順子內親王](../Page/順子內親王.md "wikilink")、第二皇女[富貴宮](../Page/富貴宮.md "wikilink")，但她們都在幼年期便夭折了。[萬延元年](../Page/萬延.md "wikilink")（1860年）七月十日，孝明天皇敕令[典侍](../Page/典侍.md "wikilink")[中山慶子所生的九歲第二皇子](../Page/中山慶子.md "wikilink")[祐宮睦仁親王](../Page/祐宮睦仁親王.md "wikilink")（後來的[明治天皇](../Page/明治天皇.md "wikilink")）以她的「實子」（親生子）名義撫養。

33歳時，其夫孝明天皇猝逝；明治天皇即位後，於[慶應四年](../Page/慶應.md "wikilink")（1868年）三月十八日，冊立她為[皇太后](../Page/皇太后.md "wikilink")，儘管她未曾當過[皇后](../Page/皇后.md "wikilink")。

遷都[東京後的](../Page/東京.md "wikilink")[明治五年](../Page/明治.md "wikilink")（1872年），遷居到[赤坂離宮](../Page/赤坂離宮.md "wikilink")；同七年，移居赤坂御用地內的[青山御所](../Page/青山御所.md "wikilink")。

明治二十一年（1888年）11月1日，受勳[勳一等](../Page/勳一等.md "wikilink")[寶冠章](../Page/寶冠章.md "wikilink")。明治三十年（[1897年](../Page/1897年.md "wikilink")）1月11日過世，享年62歲。1月30日，賜[追號](../Page/諡號.md "wikilink")「**英照皇太后**」。同年將大喪的紀錄以[和裝本作成](../Page/和裝本.md "wikilink")『英照皇太后大喪記事』、『英照皇太后之御盛徳』、『英照皇太后陛下御大葬写真帖』。

其陵寢在[京都市](../Page/京都市.md "wikilink")[東山區今熊野的](../Page/東山區_\(日本\).md "wikilink")**後月輪東山陵**，與孝明天皇同葬。

附帶一提，京都的[大宮御所](../Page/京都御苑.md "wikilink")，是依皇太后夙子的意見，在慶應三年（[1867年](../Page/1867年.md "wikilink")）建造的。

## 家庭

  - 夫：[孝明天皇](../Page/孝明天皇.md "wikilink")（1831-1867）
      - 第一皇女：順子内親王（1850-1852）
      - 第二皇女：富貴宮（1858-1859）
      - 養子：睦仁親王（[明治天皇](../Page/明治天皇.md "wikilink")）

## 榮譽

  - [1888年](../Page/1888年.md "wikilink")（明治21年）11月1日 - 勳一等[寶冠章](../Page/寶冠章.md "wikilink")(寶冠大綬章)\[1\]

[Category:山城國出身人物](../Category/山城國出身人物.md "wikilink")
[Category:江戶時代皇太后](../Category/江戶時代皇太后.md "wikilink")
[Category:九条家](../Category/九条家.md "wikilink")
[Category:日本親王妃與王妃](../Category/日本親王妃與王妃.md "wikilink")

1.  『官報』第1605号「宮廷録事」1888年11月2日。