**底比斯圣队**（古希腊语 Ιερός Λόχος ， *hieròs
lókhos*）是古希腊城邦[底比斯的一支精锐部队](../Page/底比斯_\(希腊\).md "wikilink")，共300人，由150对「[古希臘少年愛](../Page/古希臘少年愛.md "wikilink")」伴侣组成。这支部队是前4世纪底比斯军队的精英。底比斯将领[高吉达斯于前](../Page/高吉达斯.md "wikilink")378年组织了这支部队。圣队在[留克特拉战役中起了关键作用](../Page/留克特拉战役.md "wikilink")。于前338年的[喀羅尼亞戰役中被由](../Page/喀羅尼亞戰役.md "wikilink")[腓力二世率領的](../Page/腓力二世_\(马其顿\).md "wikilink")[马其顿军全部歼灭](../Page/马其顿.md "wikilink")。

## 历史

[Chaironeia_lion.JPG](https://zh.wikipedia.org/wiki/File:Chaironeia_lion.JPG "fig:Chaironeia_lion.JPG")
底比斯圣队来自古希腊邦国之一的[底比斯](../Page/底比斯_\(希臘\).md "wikilink")。当时希腊主要由雅典，斯巴达，底比斯三大邦国控制。斯巴达的日益强大导致其与底比斯的关系恶化。底比斯圣队最初是由底比斯的军队中选出，安排在军队的前列作冲锋之用。前379年崛起的[同性恋将军](../Page/同性恋.md "wikilink")[伊巴密濃達为充分发挥这支部队的战斗能力](../Page/伊巴密濃達.md "wikilink")，将其由原来与普通士兵的混编中独立出来，实际上成了底比斯的特种部队。前371年与斯巴达人的[留克特拉战役中](../Page/留克特拉战役.md "wikilink")，底比斯圣队趁著斯巴達軍改變陣列方向而陣形不穩之時，冲入敌阵。戰役中斯巴达的國王[克勒姆布羅托一世戰死](../Page/克勒姆布羅托一世.md "wikilink")，极大挫伤了敌人的锐气，对此战中底比斯聖隊的胜利功劳极大。

前338年的[喀羅尼亞戰役中](../Page/喀羅尼亞戰役.md "wikilink")，腓力二世利用自軍右翼部隊的佯退吸引雅典军团前进，然后其子[亞歷山大率騎兵趁機突入雅典方阵右侧的缺口](../Page/亞歷山大帝.md "wikilink")，使其崩溃，亞歷山大再率骑兵绕到底比斯军后面，将底比斯圣队彻底歼灭。腓力二世看到死亡时相拥的底比斯圣队战士，慨叹“无论是谁，只要怀疑这些人的壯烈行为或者经历是卑劣的，都应该被毁灭”（Perish
any man who suspects that these men either did or suffered anything
unseemly.），并吩咐部下妥善埋葬。底比斯圣队的墓地树立了一座獅子雕像，以纪念他们的英勇事迹。\[1\]\[2\]

## 编制

底比斯圣队共300人，由150对少年愛伴侶组成。士兵从底比斯的各个军团里面挑选出来的，并需符合数个标准：「[少年愛](../Page/古希臘少年愛.md "wikilink")」关系、战斗力强悍。\[3\]

## 遗迹发掘

考古学家在1890年發現底比斯圣队的墓葬。其坟墓中只有254副骸骨，同时也发现纪念物狮子像。

## 参见

  - [古希臘同性戀](../Page/古希臘同性戀.md "wikilink")
  - [伊巴密浓达](../Page/伊巴密浓达.md "wikilink")
  - [柏拉图式恋爱](../Page/柏拉图式恋爱.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[Category:古希臘的陣型和軍事單位](../Category/古希臘的陣型和軍事單位.md "wikilink")
[Category:LGBT歷史](../Category/LGBT歷史.md "wikilink")
[Category:古希臘LGBT人物](../Category/古希臘LGBT人物.md "wikilink")

1.

2.  [军事史上唯一一支同性恋军队：底比斯圣军](http://world.zjol.com.cn/05world/system/2010/06/14/016687766.shtml)


3.