**三都澳**是位于中国[福建省](../Page/福建省.md "wikilink")[宁德市](../Page/宁德市.md "wikilink")[蕉城区东南面的一个港口](../Page/蕉城区.md "wikilink")，距城区30公里。由[东冲半岛](../Page/东冲半岛.md "wikilink")、[鉴江半岛合抱而成](../Page/鉴江半岛.md "wikilink")。

三都澳为[闽东沿海的](../Page/闽东.md "wikilink")“出入门户，五邑咽喉”，这里由三都、青山、斗帽、白匏、鸡公山等5个单岛和1个城澳[半岛](../Page/半岛.md "wikilink")、14个[屿](../Page/屿.md "wikilink")、17个[礁](../Page/礁.md "wikilink")、5处[滩涂以及](../Page/滩涂.md "wikilink")[官井洋](../Page/官井洋.md "wikilink")、[复鼎洋组成](../Page/复鼎洋.md "wikilink")。岛屿连缀，水深波平，腹大口小，能停泊数十万吨巨轮，是世界少有的天然良港。

三都澳内有许多形态各异的礁石岸坞，峰奇石怪，景色优美，素有“海上明珠”之称，又被称为“海上天湖”\[1\]。

三都澳是世界八大黄鱼产卵场中。这里所产的[大黄鱼色佳味美](../Page/大黄鱼.md "wikilink")。还有[海藻](../Page/海藻.md "wikilink")、贝壳诸类，均为席上佳肴。

## 历史

三都早在[唐朝以前](../Page/唐朝.md "wikilink")，就已开发。[清](../Page/清.md "wikilink")[康熙二十三年](../Page/康熙.md "wikilink")（1684年）在此设有宁德税务总口，下辖九个口岸。[光绪二十三年](../Page/光绪.md "wikilink")（1897年）正式开放作为对外通商口岸，并成立[福宁海关](../Page/福宁海关.md "wikilink")。嗣后，有[英](../Page/英国.md "wikilink")、[美](../Page/美国.md "wikilink")、[德](../Page/德国.md "wikilink")、[俄](../Page/俄罗斯帝国.md "wikilink")、[日](../Page/日本.md "wikilink")、[荷兰](../Page/荷兰.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[葡萄牙等](../Page/葡萄牙.md "wikilink")13个国家20家公司在三都设立子公司和商行。至今还保留着当时建造的天主教堂、主教楼、修道院、渔民馆等，其中[三都澳天主教堂在省内算为保存最完善的一座](../Page/三都澳天主教堂.md "wikilink")。

## 港口

，海域总面积714平方公里，主航道水深30—115米。口小腹大，处于中国海岸线中点，30万吨船舶可全天候自由进港。

  - 民港
  - 军港

## 景点

  - 海上渔排
  - 三都岛
  - 斗帽岛

## 参考文献

<div class="references-small">

<references />

</div>

[Category:宁德](../Category/宁德.md "wikilink")
[Category:中国港口](../Category/中国港口.md "wikilink")

1.  [郭沫若](../Page/郭沫若.md "wikilink")，《在三都澳水警区》，自《郭沫若闽游诗集》，福建人民出版社，1979年11月