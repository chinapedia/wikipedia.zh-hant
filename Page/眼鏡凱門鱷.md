**眼鏡凱門鱷**（[學名](../Page/學名.md "wikilink")：**），又名**南美短吻鱷**、**眼鏡鱷**，是一種[鱷魚](../Page/鱷魚.md "wikilink")，因為眼球前端有一條橫骨，就像[眼鏡架一樣](../Page/眼鏡.md "wikilink")，因而得名。

眼鏡凱門鱷體長最大可達2.5[米](../Page/米.md "wikilink")，初孵出來的幼鱷有20至25厘米長。眼鏡凱門鱷[嘴長](../Page/嘴.md "wikilink")，背面呈[橄欖](../Page/橄欖.md "wikilink")[綠色](../Page/綠色.md "wikilink")，[頭部](../Page/頭.md "wikilink")、身體、[尾部上有個多深色斑紋](../Page/尾.md "wikilink")，[背部及尾部上則有深](../Page/背.md "wikilink")[棕色或](../Page/棕色.md "wikilink")[黑色的橫帶紋](../Page/黑色.md "wikilink")，肚皮的顏色則是純[米色或者淺](../Page/米色.md "wikilink")[黃色](../Page/黃色.md "wikilink")。初孵出來的幼鱷下[頜兩側有淡色斑](../Page/頜.md "wikilink")，長大到大約35厘米長時，這些斑紋會全部消失。

## 分佈地

眼鏡凱門鱷原產於[中美洲及](../Page/中美洲.md "wikilink")[南美洲一帶](../Page/南美洲.md "wikilink")，不過其後牠們被人類帶到[美國](../Page/美國.md "wikilink")[佛羅里達州及](../Page/佛羅里達州.md "wikilink")[古巴](../Page/古巴.md "wikilink")。

## 習性

眼鏡凱門鱷一般棲居於廣泛的水域棲息地。與其他鱷魚一樣，食物隨年齡、季節及棲息地的不同而變化。幼鱷主要以[無脊椎動物為食](../Page/無脊椎動物.md "wikilink")，特別是[鞘翅目](../Page/鞘翅目.md "wikilink")[昆蟲](../Page/昆蟲.md "wikilink")，也會進食[螺](../Page/螺.md "wikilink")、[蝦及](../Page/蝦.md "wikilink")[蟹等水生動物](../Page/蟹.md "wikilink")。成年鱷則主要以[脊椎動物為食](../Page/脊椎動物.md "wikilink")，包括水生及陸生的脊椎動物。牠們的捕食策略包括靜伏不動，偷襲路過的陸生脊椎動物，於水中偷襲游近的[魚類與其他水生脊椎動物](../Page/魚類.md "wikilink")。此外，牠們有時也用身體和尾巴驅趕魚類到淺水處，或者於開闊的水體將魚類驅趕到狹窄的岸邊後再捕食。

眼鏡凱門鱷的求愛行為包括躍出水面、炫耀尾巴，以及輕咬和摩擦頭部與頸部等。牠們於不同地點的造巢活動的高峰期也有不同，一般多在潮濕季節。牠們的巢由[葉子](../Page/葉子.md "wikilink")、小[樹枝](../Page/樹枝.md "wikilink")、雜草以及泥堆成小丘狀。

雌性的眼鏡凱門鱷每次產卵可達40顆，而整個孵化期雌性會有規律地關心和保護巢址。幼鱷出殼時發出叫聲，雌性將巢挖開護送雛鱷進入水中，而有時雄鱷也協助。雌鱷也會用嘴幫助胚鱷破殼而出，並用嘴攜帶幼鱷進入水中。

眼鏡凱門鱷於其一生中不同時期均有不同敵人。捕食鱷卵的動物包括有[黑點雙領蜥](../Page/黑點雙領蜥.md "wikilink")、[發冠長腳鷹](../Page/發冠長腳鷹.md "wikilink")、[食蟹狐](../Page/食蟹狐.md "wikilink")、[浣熊](../Page/浣熊.md "wikilink")、[長鼻浣熊和](../Page/長鼻浣熊.md "wikilink")[戴帽猴等](../Page/戴帽猴.md "wikilink")。而以雛鱷和幼鱷為食的則有[鯰魚等食肉性魚類](../Page/鯰魚.md "wikilink")、[龜](../Page/龜.md "wikilink")、[鱷魚](../Page/鱷魚.md "wikilink")、[夜鷺](../Page/夜鷺.md "wikilink")、[鸛](../Page/鸛.md "wikilink")、[鷹](../Page/鷹.md "wikilink")、[虎貓以及](../Page/虎貓.md "wikilink")[家豬等](../Page/家豬.md "wikilink")。[美洲豹及](../Page/美洲豹.md "wikilink")[森蚺就會捕食成年鱷魚](../Page/森蚺.md "wikilink")。

## 種群動態

眼鏡凱門鱷分佈廣且數量多，是提供國際[鱷皮貿易的最主要種類](../Page/鱷皮.md "wikilink")。於原生地以外地方，牠們是極具威脅性的[外來物種](../Page/外來物種.md "wikilink")。

## 參考資料

  - 《揚子鱷研究》，ISBN 7-5428-3377-4/Q‧32
  - 《兩生爬行類圖鑑》，ISBN 957-469-519-0

[Category:凱門鱷亞科](../Category/凱門鱷亞科.md "wikilink")