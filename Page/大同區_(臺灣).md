**大同區**位於[中華民國](../Page/中華民國.md "wikilink")[臺北市西邊](../Page/臺北市.md "wikilink")，是臺北市最早發展的區域，[臺北市孔廟](../Page/臺北孔子廟.md "wikilink")、[大龍峒保安宮均位於此區](../Page/大龍峒保安宮.md "wikilink")，涵蓋[大稻埕與](../Page/大稻埕.md "wikilink")[大龍峒兩大區域](../Page/大龍峒.md "wikilink")；[大稻埕曾繁華一時](../Page/大稻埕.md "wikilink")，有許多早期洋房，也是傳統食材（南北貨）、中藥、紡織品及工業用品的集散地，2010年臺北市舉辦[臺北國際花卉博覽會而制定區花](../Page/臺北國際花卉博覽會.md "wikilink")，大同區為[茶花](../Page/茶花.md "wikilink")，[臺北市政府民政局也特別製作區花版門牌](../Page/臺北市政府民政局.md "wikilink")。

## 詞源

本地原名**[巴浪泵社](../Page/大浪泵社.md "wikilink")**，為[臺灣](../Page/臺灣.md "wikilink")[平埔](../Page/平埔族.md "wikilink")[凱達格蘭族的一支系部落](../Page/凱達格蘭族.md "wikilink")。乾隆28年〈西元1763年〉余文儀續修臺灣府志時，載有「-{大浪泵庄}-」。道光24年〈西元1844年〉之契字述及「大隆同四十四坎」，即稱「大隆同」。

[清](../Page/清.md "wikilink")[同治年間](../Page/同治.md "wikilink")，該區北部一帶，屬[淡水廳](../Page/淡水廳.md "wikilink")[大加臘堡](../Page/大加臘堡.md "wikilink")「[大隆同莊](../Page/大隆同莊.md "wikilink")」，後改稱「大龍峒」。

1909年，臺北廳直轄地方分為三大區，即艋舺、大稻埕、大龍峒三區，大龍峒轄區甚廣，包含今大同區、中山區。

1920年，施行市制，台灣總督府始置大龍峒町\[1\]。

1925年，大龍峒地區興建孔廟。

1946年1月，[臺北市被中華民國政府接管而改制](../Page/臺北市.md "wikilink")[省轄市時](../Page/省轄市.md "wikilink")，而臺北市被規劃為十大區，合併日治時代的大龍峒町、蓬萊町、太平町、大橋町、河合町為區，但因轄區內有孔廟及大同路（現大同街），為了紀念孔子天下大同之精神、及取國父世界大同之崇高理念，配合舊地名而命名為「大同區」\[2\]。

## 地理位置

  - 東：以[臺北捷運淡水線用地東側與](../Page/台北捷運淡水線.md "wikilink")[中山區為界](../Page/中山區_\(臺北市\).md "wikilink")。
  - 西：以[淡水河中心線與](../Page/淡水河.md "wikilink")[新北市](../Page/新北市.md "wikilink")[三重區為鄰](../Page/三重區.md "wikilink")。
  - 南：以[市民大道](../Page/市民大道.md "wikilink")、[延平北路一段](../Page/延平北路.md "wikilink")、[忠孝西路二段與](../Page/忠孝西路_\(臺北市\).md "wikilink")[中正區](../Page/中正區_\(臺北市\).md "wikilink")、[萬華區為界](../Page/萬華區.md "wikilink")。
  - 北：以[中山高速公路用地北側及](../Page/中山高速公路.md "wikilink")[基隆河中心線與](../Page/基隆河.md "wikilink")[士林區為界](../Page/士林區.md "wikilink")。

## 人口

## 行政區域

[TaipeiDaTongSubDistrictMap.png](https://zh.wikipedia.org/wiki/File:TaipeiDaTongSubDistrictMap.png "fig:TaipeiDaTongSubDistrictMap.png")
全區共分為25里，各里名稱為：

  - 蘭州次分區：老師里、國慶里、隆和里、[國順里](../Page/國順里.md "wikilink")、景星里、鄰江里，共6里。
  - 大龍次分區：至聖里、重慶里、揚雅里、保安里、斯文里、蓬萊里，共6里。（[大龍峒地區](../Page/大龍峒.md "wikilink")）
  - 延平次分區：大有里、永樂里、南芳里、玉泉里、延平里、[朝陽里](../Page/朝陽里_\(臺北市\).md "wikilink")，共6里。（[大稻埕地區](../Page/大稻埕.md "wikilink")）
  - 建成次分區：民權里、建功里、建泰里、雙連里、光能里、建明-{里}-、星明-{里}-，共7里。

原有文昌里，因戶數過少，在2002年9月1日區里行政區域調整中併入老師里。

## 教育與醫療機構

### 醫療機構

[1898_臺灣總督府關於臺北大稻埕居民對檢疫相關建議書之公文封面_Official_Document_concerning_Taiwan's_Public_Health_Measures.jpg](https://zh.wikipedia.org/wiki/File:1898_臺灣總督府關於臺北大稻埕居民對檢疫相關建議書之公文封面_Official_Document_concerning_Taiwan's_Public_Health_Measures.jpg "fig:1898_臺灣總督府關於臺北大稻埕居民對檢疫相關建議書之公文封面_Official_Document_concerning_Taiwan's_Public_Health_Measures.jpg")關於臺北[大稻埕居民對檢疫相關建議書之公文封面](../Page/大稻埕.md "wikilink")\]\]

  - [臺北市立聯合醫院](../Page/台北市立聯合醫院.md "wikilink") 中興院區（院本部）
  - [行政院衛生署臺北醫院](../Page/行政院衛生署台北醫院.md "wikilink")[城區分院](../Page/行政院衛生署台北醫院城區分院.md "wikilink")

### 高級中等學校

  - [臺北市立成淵高級中學](../Page/臺北市立成淵高級中學.md "wikilink")
  - [臺北市立明倫高級中學](../Page/臺北市立明倫高級中學.md "wikilink")
  - [臺北市私立志仁高中職業進修學校](http://www.cjvs.tp.edu.tw/)
  - [臺北市私立靜修女子高級中學](../Page/臺北市私立靜修女子高級中學.md "wikilink")
  - [臺北市私立稻江高級商業職業學校](../Page/臺北市私立稻江高級商業職業學校.md "wikilink")

### 國民中學

  - [臺北市立成淵高級中學](http://www.cyhs.tp.edu.tw/)附設國民中學
  - [臺北市立建成國民中學](../Page/臺北市立建成國民中學.md "wikilink")
  - [臺北市立重慶國民中學](http://www.cchs.tp.edu.tw/)
  - [臺北市立蘭州國民中學](http://www.lcjh.tp.edu.tw/)
  - [臺北市立民權國民中學](http://www.mqjh.tp.edu.tw/)附設補校
  - [臺北市立忠孝國民中學](http://www.chjh.tp.edu.tw/)
  - [臺北市私立靜修女子高級中學](http://www.bish.tp.edu.tw/)附設國民中學

### 國民小學

  - [臺北市大同區大同國民小學](../Page/臺北市大同區大同國民小學.md "wikilink")[校網](http://www.ttps.tp.edu.tw/)
  - [臺北市大同區大龍國民小學](../Page/臺北市大同區大龍國民小學.md "wikilink")：暨附設補習學校
  - [臺北市大同區太平國民小學](../Page/臺北市大同區太平國民小學.md "wikilink")
  - <s>[臺北市大同區明倫國民小學](../Page/臺北市大同區明倫國民小學.md "wikilink")</s>（廢校）
  - [臺北市大同區雙蓮國民小學](../Page/臺北市大同區雙蓮國民小學.md "wikilink")
  - [臺北市大同區大橋國民小學](../Page/臺北市大同區大橋國民小學.md "wikilink")[校網](http://www.tjps.tp.edu.tw/)
  - [臺北市大同區日新國民小學](../Page/臺北市大同區日新國民小學.md "wikilink")
  - [臺北市大同區永樂國民小學](../Page/臺北市大同區永樂國民小學.md "wikilink")：暨附設補習學校
  - [臺北市大同區延平國民小學](../Page/臺北市大同區延平國民小學.md "wikilink")[校網](http://www.ypps.tp.edu.tw/)
  - [臺北市大同區蓬萊國民小學](../Page/臺北市大同區蓬萊國民小學.md "wikilink")[校網](http://www.plps.tp.edu.tw/)
  - <s>[建成國小](../Page/建成小學校.md "wikilink")</s>(廢校，校舍今分別為[當代藝術館](../Page/當代藝術館.md "wikilink")\`[建成國中使用](../Page/建成國中.md "wikilink"))

### 特殊教育學校

  - [臺北市立啟聰學校](../Page/臺北市立啟聰學校.md "wikilink")

### 圖書館

  - [臺北市立圖書館](../Page/臺北市立圖書館.md "wikilink")[延平分館](../Page/臺北市立圖書館延平分館.md "wikilink")　　[圖網](https://tpml.gov.taipei/News_Content.aspx?n=4F66F55F388033A7&sms=CFFFC938B352678A&s=9705A783EC7DAA93)
  - [臺北市立圖書館](../Page/臺北市立圖書館.md "wikilink")[大同分館](../Page/臺北市立圖書館大同分館.md "wikilink")　　[圖網](https://tpml.gov.taipei/News_Content.aspx?n=4F66F55F388033A7&sms=CFFFC938B352678A&s=139B11B3FE27F959)
  - [臺北市立圖書館](../Page/臺北市立圖書館.md "wikilink")[建成分館](../Page/臺北市立圖書館建成分館.md "wikilink")　　[圖網](https://tpml.gov.taipei/News_Content.aspx?n=4F66F55F388033A7&sms=CFFFC938B352678A&s=9653DABCE4761B8E)
  - [臺北市立圖書館](../Page/臺北市立圖書館.md "wikilink") -
    蘭州民眾閱覽室　　[圖網](https://tpml.gov.taipei/News_Content.aspx?n=4F66F55F388033A7&sms=CFFFC938B352678A&s=A3B82F4BECD6CD1C)

## 交通

### 捷運

  - [台北捷運](../Page/台北捷運.md "wikilink")

<!-- end list -->

  - ：[圓山站](../Page/圓山站.md "wikilink") -
    [民權西路站](../Page/民權西路站.md "wikilink") -
    [雙連站](../Page/雙連站.md "wikilink") -
    [中山站](../Page/中山站_\(台北市\).md "wikilink")

  - ：[北門站](../Page/北門站_\(台北市\).md "wikilink") -
    [中山站](../Page/中山站_\(台北市\).md "wikilink")

  - ：[大橋頭站](../Page/大橋頭站.md "wikilink") -
    [民權西路站](../Page/民權西路站.md "wikilink")

### 道路

  - 國道、省道

<!-- end list -->

  - [TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號](../Page/中山高速公路.md "wikilink")（中山高速公路）：[台北交流道](../Page/台北交流道.md "wikilink")、[環北交流道](../Page/環北交流道.md "wikilink")
  - [TW_PHW1a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW1a.svg "fig:TW_PHW1a.svg")[臺1甲線](../Page/臺1甲線.md "wikilink")：[民權西路](../Page/民權西路.md "wikilink")、[臺北大橋](../Page/臺北大橋.md "wikilink")
  - [TW_PHW2b.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2b.svg "fig:TW_PHW2b.svg")[臺2乙線](../Page/台2乙線.md "wikilink")：[重慶北路](../Page/重慶北路_\(台北市\).md "wikilink")

### 客運

  - [臺北轉運站](../Page/台北轉運站.md "wikilink")：市民大道一段209號（承德路、市民大道口）

### 未來

#### 捷運、輕軌

  - [台北捷運](../Page/台北捷運.md "wikilink")

<!-- end list -->

  - <font color={{台北捷運色彩|民}}>█</font> </small>[民生汐止線](../Page/民生汐止線.md "wikilink")（規劃中）：[<font style="color:#888888;">大稻埕站</font>](../Page/大稻埕站.md "wikilink")
  - <font color={{台北捷運色彩|社南}}>█</font> </small>[社子輕軌](../Page/社子輕軌.md "wikilink")（規劃中）：[大橋頭站](../Page/大橋頭站.md "wikilink")
    -
    [<font style="color:#888888;">大龍峒站</font>](../Page/大龍峒站.md "wikilink")

## 體育場館

  - [大同運動中心](../Page/大同運動中心.md "wikilink")：位於本區[大龍街](../Page/大龍街.md "wikilink")51號
  - 大同第三運動中心：位於重慶北路3段
  - 大同區迪化運動中心：即將於2014年開幕\[3\]
  - 民權西運動中心：將於2015年1月開幕\[4\]

## 政黨團體

  - [民國黨總部](../Page/民國黨.md "wikilink")：位於臺北市大同區承德路一段17號16樓之5

## 文化與相關事物

[臺北孔子廟大成殿.JPG](https://zh.wikipedia.org/wiki/File:臺北孔子廟大成殿.JPG "fig:臺北孔子廟大成殿.JPG")[大成殿](../Page/大成殿.md "wikilink")。\]\]
[廣和堂藥舖(今迪化207博物館).jpg](https://zh.wikipedia.org/wiki/File:廣和堂藥舖\(今迪化207博物館\).jpg "fig:廣和堂藥舖(今迪化207博物館).jpg")廣和堂藥舖（今[迪化207博物館](../Page/迪化207博物館.md "wikilink")）。\]\]
[大稻埕千秋街店屋.jpg](https://zh.wikipedia.org/wiki/File:大稻埕千秋街店屋.jpg "fig:大稻埕千秋街店屋.jpg")。\]\]
[大稻埕仁安醫院.jpg](https://zh.wikipedia.org/wiki/File:大稻埕仁安醫院.jpg "fig:大稻埕仁安醫院.jpg")（今[臺北市社區營造中心](../Page/臺北市社區營造中心.md "wikilink")）。\]\]

### 民宅與祠堂

  - 大稻埕葉金塗宅（改建為大稻埕大樓，由[台北城大飯店進駐](../Page/台北城大飯店.md "wikilink")\[5\]。一至三樓所保留之葉金塗宅立面為星巴克保安門市\[6\]）
  - [大稻埕辜宅](../Page/大稻埕辜宅.md "wikilink")：1920年代建成。
  - [樹人書院文昌祠](../Page/樹人書院文昌祠.md "wikilink")
  - [陳德星堂](../Page/陳德星堂.md "wikilink")：1892年建成。
  - [陳悅記祖宅](../Page/陳悅記祖宅.md "wikilink")（[老師府](../Page/老師府.md "wikilink")）：建於1807年。

### 宗教建築

  - 財團法人[大龍峒保安宮](../Page/大龍峒保安宮.md "wikilink")：建於1921年。
  - [臺北孔子廟](../Page/臺北孔子廟.md "wikilink")：建於1925年。
  - [大稻埕霞海城隍廟](../Page/大稻埕霞海城隍廟.md "wikilink")：建於1859年。
  - [瞿公真人廟](../Page/瞿公真人廟.md "wikilink")：建於1887年。

### 公園

  - [延平河濱公園](../Page/延平河濱公園.md "wikilink")
  - [建成公園](../Page/建成公園.md "wikilink")
  - [臺北花博公園](../Page/花博公園.md "wikilink")
  - [玉泉公園](../Page/臺北市戰前規劃的大型都會公園.md "wikilink")
  - [朝陽公園](../Page/朝陽茶葉公園.md "wikilink")

### 溝渠

  - 番子溝，今已不存在，，大致在重慶北路與淡水河間之高速公路南側一帶。

### 歷史街區

  - [迪化街](../Page/迪化街.md "wikilink")
  - 牛磨車街（今迪化街2段〉
  - [大稻埕](../Page/大稻埕.md "wikilink")
  - [臺北府城北門](../Page/臺北府城北門.md "wikilink")：建於1884年。

### 藝術空間

  - [臺北當代藝術館](../Page/台北當代藝術館.md "wikilink")（日本時代的建成小學校、與舊臺北市政府）

### 古蹟

  - [台灣總督府交通局鐵道部](../Page/臺灣總督府交通局鐵道部.md "wikilink")：建於1919年。
  - [大稻埕圓環防空蓄水池](../Page/大稻埕圓環防空蓄水池.md "wikilink")
  - [原臺北北警察署](../Page/臺北北警察署.md "wikilink")（今[大同分局](../Page/大同分局.md "wikilink")）：建於1933年。
  - [大稻埕千秋街店屋](../Page/大稻埕千秋街店屋.md "wikilink")：清末日治時期初。
  - [台灣基督長老教會大稻埕教會](../Page/臺灣基督長老教會大稻埕教會.md "wikilink")：建於1915年。

### 購物

  - [京站時尚廣場](../Page/京站時尚廣場.md "wikilink")

## 相關條目

  - [禮記](../Page/禮記.md "wikilink")[禮運](../Page/禮運.md "wikilink")（大同篇）
  - [大龍峒](../Page/大龍峒.md "wikilink")
  - [大稻埕](../Page/大稻埕.md "wikilink")
  - [建成區](../Page/建成區_\(台北市\).md "wikilink")
  - [延平區](../Page/延平區_\(台北市\).md "wikilink")
  - [至聖公園](../Page/至聖公園.md "wikilink")
  - [臺灣省宜蘭縣大同鄉是臺灣省宜蘭縣下轄的一個鄉](../Page/大同鄉_\(臺灣\).md "wikilink")。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [大同區公所](http://www.dtdo.gov.taipei)
  - [大同區戶政事務所](http://dthr.gov.taipei)
  - [大同社區大學](http://www.datong.org.tw)

[Category:臺北市行政區劃](../Category/臺北市行政區劃.md "wikilink")
[Datong](../Category/1946年建立的行政區劃.md "wikilink")
[大同區_(臺灣)](../Category/大同區_\(臺灣\).md "wikilink")

1.  [參考政府的官方用詞](http://www.dtdo.gov.taipei/ct.asp?xItem=1605454&CtNode=41198&mp=124031)
2.  曾迺碩、黃得時，《臺北市志：卷一沿革志封域篇》，臺北市文獻委員會，1988年6月，第135頁
3.  《爽報》(台灣)Charm 2013/03/08 第9版
4.  《爽報》(台灣)Charm 2013/03/12 第3版
5.
6.