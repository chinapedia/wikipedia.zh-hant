[Milford_Sound_(New_Zealand).JPG](https://zh.wikipedia.org/wiki/File:Milford_Sound_\(New_Zealand\).JPG "fig:Milford_Sound_(New_Zealand).JPG")

**米爾福德峽灣**或稱**米佛峽灣**（[英文](../Page/英文.md "wikilink")：**Milford
Sound**、[毛利語](../Page/毛利語.md "wikilink")：**Piopiotahi**）是位於[紐西蘭](../Page/紐西蘭.md "wikilink")[南島的西南部](../Page/南島.md "wikilink")[峽灣國家公園內的一處冰河地形](../Page/峽灣國家公園.md "wikilink")，在1990年被列入[世界遺產](../Page/世界遺產.md "wikilink")[蒂瓦希波乌纳穆](../Page/蒂瓦希波乌纳穆.md "wikilink")，在毛利語的意思為「第一隻野生画眉」。

## 概述

米爾福德峽灣距離[蒂阿瑙約有](../Page/蒂阿瑙.md "wikilink")121[公里的路程](../Page/公里.md "wikilink")，在通過長約1.2公里的及谷後即抵達米爾福德峽灣。峽灣位於距離[塔斯曼海內陸](../Page/塔斯曼海.md "wikilink")15公里處，從海面垂直拔地而起的[主教冠峰標高](../Page/主教冠峰.md "wikilink")1,692公尺，是世界上臨海最高的山峰之一，而此區域最大的落差達160公尺。\[1\]其他還有狮子山、象山、仙女瀑布，及落差達146公尺的斯特林瀑布等，都是在冰河地形所產生的特殊景觀。

在峽灣中還可見到[寬吻海豚](../Page/寬吻海豚.md "wikilink")、[峽灣企鵝](../Page/峽灣企鵝.md "wikilink")、[海豹等水中生物](../Page/海豹.md "wikilink")，特別在海豹岬上常有大量的年輕海豹聚集。

峽灣內有南方探索和兩家公司所經營峽灣導覽觀光船可帶旅客繞行峽灣，另外也有水底觀景窗的遊覽船，可讓旅客從水底觀測[珊瑚礁等海中景觀](../Page/珊瑚礁.md "wikilink")，此外還可搭配獨木舟、露營、健行、空中飛行等活動深入認識米爾福德峽灣。

## 氣候

米爾福德峽灣平均每年降雨量為，是紐西蘭最潮濕的地區，也是世界上最潮濕的地方之一。在24小時內降雨量可達。\[2\]

## 照片集

<File:Milford> Sound, 2016-01-31, Mitre Peak from the port.jpg| 主教冠峰
<File:Milford> Sound - Bowen Falls 4.jpg| 博恩瀑布 <File:Stirling> Falls
with Boat.jpg| Stirling瀑布 <File:Fauna> de Nueva Zelanda02.JPG| 米佛峽灣內的海豹

## 參考資料

## 相關條目

  - [蒂瓦希波乌纳穆](../Page/蒂瓦希波乌纳穆.md "wikilink")
  - [菲奥德兰](../Page/菲奥德兰.md "wikilink")
  - [峡湾国家公园](../Page/峡湾国家公园.md "wikilink")
  - [道佛峽灣](../Page/道佛峽灣.md "wikilink")
  - [米爾福德峽灣機場](../Page/米爾福德峽灣機場.md "wikilink")

## 外部連結

  - [米爾福德峽灣旅遊業的網站](https://web.archive.org/web/20060420125417/http://www.fiordland.org.nz/Explore-Fiordland/Milford-Sound/Default.asp)
  - [水底觀景窗網站](http://www.milforddeep.co.nz/)

[Category:紐西蘭地理](../Category/紐西蘭地理.md "wikilink")
[Category:新西兰峡湾](../Category/新西兰峡湾.md "wikilink")
[Category:海洋國家公園](../Category/海洋國家公園.md "wikilink")

1.
2.