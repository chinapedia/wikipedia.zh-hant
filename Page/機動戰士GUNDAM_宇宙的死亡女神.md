《**機動戰士GUNDAM 宇宙的死亡女神**》（）是由[OVA](../Page/OVA.md "wikilink")《[機動戰士GUNDAM
第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")》監督[飯田馬之介於](../Page/飯田馬之介.md "wikilink")2002年起在[GUNDAM
ACE所發表的](../Page/GUNDAM_ACE.md "wikilink")[鋼彈漫畫系列作品](../Page/高達系列作品.md "wikilink")，單行本全4冊。

## 故事背景

本故事講述[一年戰爭初期](../Page/一年戰爭.md "wikilink")[自護軍進行](../Page/自護公國.md "wikilink")「不列顛作戰」時，被作為[質量炸彈的](../Page/質量炸彈.md "wikilink")[SIDE2第八番](../Page/宇宙殖民地_\(GUNDAM世界\)#拉格朗日四號宙域.md "wikilink")[宇宙殖民地](../Page/宇宙殖民地.md "wikilink")「[伊菲修島](../Page/宇宙殖民地_\(GUNDAM世界\)#Side2哈帝.md "wikilink")(Island
Ifficial)」内發生的故事。當正在追擊[SIDE2](../Page/宇宙殖民地_\(GUNDAM世界\)#Side2哈帝.md "wikilink")[自護軍士兵的名田等人](../Page/自護公國.md "wikilink")，被敵人以第一波的突襲攻擊，令名田等人要被迫降落[SIDE2](../Page/宇宙殖民地_\(GUNDAM世界\)#Side2哈帝.md "wikilink")，他們降落後看見[自護軍士兵放毒瓦斯毒殺](../Page/自護公國.md "wikilink")[殖民地市民情景](../Page/宇宙殖民地.md "wikilink")。

## 作品解說

作者[飯田馬之介在](../Page/飯田馬之介.md "wikilink")[機動戰士GUNDAM
第08MS小隊的後期擔任監督](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")，[斯羅·阿瑪達在本作品中盤出現](../Page/斯羅·阿瑪達.md "wikilink")。

故事以主要的白兵戰令變得有緊迫感，可以在各種各樣的地方能夠窺見軍事的知識，如果跟原創故事設定的調整性都很多。由於[自護軍注入毒瓦斯大量虐殺市民](../Page/自護公國.md "wikilink")，而且從準備降落地球的[宇宙殖民地內部狀況下為故事的開始](../Page/宇宙殖民地.md "wikilink")，成為[GUNDAM
ACE刊載作品中也格外放出異彩的展開](../Page/GUNDAM_ACE.md "wikilink")。

在標題裏面的「伊休妲」是指出現於[瑪雅神話的死神名字](../Page/瑪雅文化.md "wikilink")。

[GUNDAM ACE刊載的](../Page/GUNDAM_ACE.md "wikilink")[機動戰士GUNDAM 第08MS小隊
U.C.0079+α](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")，因為成為承繼當年故事的收成品的形式，以細微部分與原作的OVA不同的設定都被採用。

## 人物介紹

### 吉翁軍

  -
    本故事的女主角，有著「[伊休妲](../Page/伊休妲.md "wikilink")」稱號的年輕[自護軍下士女士兵](../Page/自護公國.md "wikilink")，金髮碧眼[巨乳留著幾乎跟身高一樣高的長髮](../Page/巨乳.md "wikilink")(在她著駕駛服時會紮成一束)。出身在小行星帶，因父母雙亡付不出[聯邦軍的空氣](../Page/地球聯邦軍.md "wikilink")、水等重稅以及滯納罰款，而不得不加入[SIDE3的](../Page/宇宙殖民地_\(GUNDAM世界\)#Side3慕佐.md "wikilink")[自護軍當](../Page/自護公國.md "wikilink")[MS駕駛](../Page/MS.md "wikilink")（鐵屑少女的悲哀），對於待在宇宙空間中的感覺比在乘坐駕駛艙更自在，非常善於操縱[MS-05](../Page/MS-05.md "wikilink")，在一年戰爭的開端利用[米粒爐的特性發動了對名田等人所乘坐的](../Page/宇宙世紀科技列表#米諾夫斯基超小型核融合爐.md "wikilink")[麥哲倫級托塔提斯號戰艦發動第一波的突襲攻擊](../Page/麥哲倫級戰艦.md "wikilink")，在無法順利擊沉戰艦後，追擊托塔提斯進入伊菲修島的殖民地。由於缺乏家庭的溫暖和戰爭的理由跟名田形成強烈的對比，她唯一只僅存作為士兵的價值，但為了活下去過渡自己的人生，而展開對托塔提斯殘存成員的追擊。
  -
    [自護軍的少尉](../Page/自護公國.md "wikilink")，帶領三部[姆賽級闕爾號艦載的](../Page/姆賽級輕巡洋艦.md "wikilink")[MS-05小隊進行了對托塔提斯號的突襲](../Page/MS-05.md "wikilink")。因為[同性戀的身分而遭到歧視](../Page/同性戀.md "wikilink")，認為[自護軍所提倡的人類的革新比較能認同自己的價值而加入](../Page/自護公國.md "wikilink")[自護軍](../Page/自護公國.md "wikilink")。在進入伊菲修島就認清了[自護軍的](../Page/自護公國.md "wikilink")「不列顛作戰」所造成大量屠殺非戰鬥人員的平民令她完全地絕望，但因軍人身分而要繼續進行任務。
  - 托普
    [吉翁军所属的女性驾驶员](../Page/吉翁公国.md "wikilink")，一年战争后期少尉，欧洲方面[機動戰士小队长](../Page/機動戰士.md "wikilink")。[機動戰士GUNDAM
    第08MS小隊中登场的冷静透彻的军人角色而不失理性与人性](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")。原本是预定为本作的女主角，而在「已知结局的角色作主角会降低乐趣」的提议下成为艾莉莎·海溫的设计原形。本作中在最后登场，作为魯姆海战前补充入已经晋升为小队长的艾莉莎·海溫的小队的新兵。
  - 德尔
    [吉翁军所属的男性驾驶员](../Page/吉翁公国.md "wikilink")，[機動戰士GUNDAM
    第08MS小隊中登场的托普小队的队员](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")，相貌粗犷但有着温柔宽厚的一面。已婚，在故乡有儿子。本作中在最后登场，与托普一起作为卢姆海战前补充入已经晋升为小队长的艾莉莎·海溫的小队的新兵。

### 聯邦軍

  -
    本故事的男主角，出身[SIDE3的日裔](../Page/宇宙殖民地_\(GUNDAM世界\)#Side3慕佐.md "wikilink")[聯邦中尉](../Page/地球聯邦軍.md "wikilink")，因為出身地的關係，雖然他是以第一名畢業出身卻被上司忽略而一直在中尉。他的妻子和未見面的孩子在地球生活，而老父則留在[SIDE3](../Page/宇宙殖民地_\(GUNDAM世界\)#Side3慕佐.md "wikilink")。他為了妻子和孩子決定要阻止死亡女神「伊休妲」\[1\]。
  -
    托塔提斯號中校女艦長，在突襲中因名田的努力搶救順利逃過一劫，判斷出[自護軍的作戰目標是以殖民地作為](../Page/自護公國.md "wikilink")[質量兵器落下地球](../Page/質量兵器.md "wikilink")，為了阻止[自護軍的作戰而帶領倖存的成員奮戰](../Page/自護公國.md "wikilink")。
  -
    [SIDE2出生的](../Page/宇宙殖民地_\(GUNDAM世界\)#Side2哈帝.md "wikilink")[聯邦校生](../Page/地球聯邦軍.md "wikilink")，托塔提斯號成員在[殖民地入口處遇到的倖存者](../Page/宇宙殖民地.md "wikilink")，在[耶誕節放假的期間在出身](../Page/耶誕節.md "wikilink")[殖民地遭到](../Page/宇宙殖民地.md "wikilink")[自護軍的放毒瓦斯](../Page/自護公國.md "wikilink")\[2\]令家人、好友全部死在面前，他因收到緊急召集換穿「NORMAL
    SUIT」而逃過一劫，因為這件事令他非常痛恨[自護軍](../Page/自護公國.md "wikilink")，之後與名田等人一起行動，多次以肉身的「NORMAL
    SUIT」面對[機動戰士](../Page/機動戰士.md "wikilink")。在[機動戰士GUNDAM
    第08MS小隊登場的男主角](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")。

## 其他

故事主角的稱號「[伊休妲](../Page/伊休妲.md "wikilink")」（Ixtab）是[馬雅神話中掌管](../Page/馬雅神話.md "wikilink")[自殺的女神](../Page/自殺.md "wikilink")。

## 注釋

## 相關條目

  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink") [Category:GUNDAM
ACE](../Category/GUNDAM_ACE.md "wikilink")
[Category:宇宙世紀](../Category/宇宙世紀.md "wikilink")

1.  「伊休妲」的稱號是在戰場上他看到艾莉莎後聯想到他的妻子所在工作的[墨西哥人類博物館](../Page/墨西哥.md "wikilink")，所收藏的馬雅長髮死亡女神浮雕所取的。
2.  他看見執行部隊就是[西瑪所帶的](../Page/西瑪.md "wikilink")[自護軍海軍部隊](../Page/自護公國.md "wikilink")