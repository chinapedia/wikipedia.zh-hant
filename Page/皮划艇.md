[Canoeing.JPG](https://zh.wikipedia.org/wiki/File:Canoeing.JPG "fig:Canoeing.JPG")

**輕艇**，

## 體育項目

亞奧運項目：輕艇靜水競速 Canoe sprint、[輕艇激流標竿](../Page/輕艇激流.md "wikilink") Canoe
slalom。

非亞奧運項目：[龍舟](../Page/龍舟.md "wikilink") Dragon boat、輕艇水球 Canoe polo、激流越野
Wildwater canoeing、輕艇馬拉松 Canoe marathon、輕艇風帆 Canoe sailing、輕艇激流花式 Canoe
freestyle、輕艇海洋競速 Canoe ocean racing、泛舟 Rafting、輕艇衝浪 Waveski。
殘奧輕艇Paracanoe

## 歷史

皮划艇的前身是早期[人類的生存和](../Page/人類.md "wikilink")[交通工具](../Page/交通.md "wikilink")，即[獨木舟](../Page/獨木舟.md "wikilink")。皮划艇是一種前後方向也是尖的[船](../Page/船.md "wikilink")，它的起源是[愛斯基摩人的獸皮船](../Page/愛斯基摩.md "wikilink")。

而皮划艇比賽中共分為兩類，一類是**皮艇（Kayak）**，另一類是**划艇（Canoe）**，兩者都是前後尖的，而皮艇是用雙葉槳推動的愛斯基摩式艇；划艇是為[加拿大式艇](../Page/加拿大.md "wikilink")，用單葉槳推動。男子的皮艇和划艇於[1936年奧運中被列入正式項目](../Page/1936年夏季奧林匹克運動會.md "wikilink")，而女子皮艇則在[1948年才成為正式比賽項目之一](../Page/1948年夏季奧林匹克運動會.md "wikilink")。直到現時，皮划艇並沒有被中止過。

## 規則

皮划艇賽事是乘坐皮艇或是划艇向前方推動，在規定距離中最快到達終點的是優勝者，現時，不少皮划艇賽事中所用的比賽場地都是人工的。

另外，大家常見到的**K-2艇**、**C-1艇**等是指該項目的類別和人數。如K-2艇就是指雙人皮艇；C-1艇是单人划艇。皮划艇的靜水競速中的人數是一人或兩人，[國際輕艇總會承認的正式比賽距離為](../Page/國際輕艇總會.md "wikilink")200、500、1000
和 5000[米](../Page/公尺.md "wikilink")。

而[激流競速賽](../Page/激流.md "wikilink")，同一樣使用皮艇和划艇。在賽事中，選手要在300[米的激流中以最短時間依次序穿過](../Page/公尺.md "wikilink")25個旗門，而激流競速賽中的賽道中包含了逆流和順流。最快而又依次序穿過所有旗門則為冠軍。另外，如選手的救生衣在賽事中掉落、未次序穿過所有旗門、重穿旗門、未穿過所有旗門等犯錯都會被增加時間。

## 外部連結

  - [International Canoe Federation](http://www.canoeicf.com/)
  - [香港獨木舟總會](https://web.archive.org/web/20090710180618/http://www.hkcucanoe.com.hk/00-Main/00-Box.htm)（中國香港體育協會暨奧林匹克委員會會員機構）

[Category:皮划艇](../Category/皮划艇.md "wikilink")
[Category:水上運動](../Category/水上運動.md "wikilink")