**詹姆斯·杜威·沃森**（，），[美國](../Page/美國.md "wikilink")[分子生物學家](../Page/分子生物學.md "wikilink")，20世紀[分子生物學的牽頭人之一](../Page/分子生物學.md "wikilink")。與同僚[佛朗西斯·克里克因為共同發現](../Page/佛朗西斯·克里克.md "wikilink")[DNA的](../Page/DNA.md "wikilink")[雙螺旋結構](../Page/雙螺旋.md "wikilink")，而與[莫里斯·威爾金斯獲得](../Page/莫里斯·威爾金斯.md "wikilink")1962年[諾貝爾生理學或醫學獎](../Page/諾貝爾生理學或醫學獎.md "wikilink")。

## 个人经历

詹姆斯·沃森生於[芝加哥](../Page/芝加哥.md "wikilink")，受其父的影響很大。據他自己在一篇文章中說，得到父親三項真傳：一，相信知識可以使人脫離「迷信」（即[宗教](../Page/基督教.md "wikilink")）；二，熱愛[觀鳥](../Page/觀鳥.md "wikilink")；三，擁護[民主黨](../Page/民主党_\(美国\).md "wikilink")。\[1\]十二歲时，參加一個名為「每事問孩子」(Quiz
Kids)的電台問答遊戲。15歲即以資優生的身分提早入讀[芝加哥大學](../Page/芝加哥大學.md "wikilink")，主修[動物學](../Page/動物學.md "wikilink")。因為讀到物理學家[薛定谔的科普名著](../Page/埃尔温·薛定谔.md "wikilink")《何謂生命？》(*What
is
Life?*)，興趣由[候鳥的遷徙轉到](../Page/候鳥.md "wikilink")[遺傳學](../Page/遺傳學.md "wikilink")。

獲得芝大的學位後，先後申請入讀[加州理工](../Page/加州理工学院.md "wikilink")、[哈佛的研究院](../Page/哈佛大学.md "wikilink")，均不果，改進[印第安納大學](../Page/印第安納大學.md "wikilink")，加入了薩爾瓦多·盧里亞（Salvador
Luria）等人的「[噬菌體集團](../Page/噬菌體.md "wikilink")」，正式涉足遺傳學的研究。受到這個集團的影響，沃森開始相信DNA就是[基因的載體](../Page/基因.md "wikilink")。1950年獲博士學位，到[哥本哈根作博士後研究](../Page/哥本哈根.md "wikilink")，其間在[那不勒斯參加一個學術會議](../Page/那不勒斯.md "wikilink")，從莫里斯·魏爾金斯（Maurice
Wilkins）的演講中得知DNA具規則的結構，堅定了他解決DNA結構的決心。1951年，轉到[劍橋的](../Page/劍橋.md "wikilink")[卡文迪许实验室](../Page/卡文迪许实验室.md "wikilink")，認識克里克，一同利用[X光](../Page/X光.md "wikilink")[繞射的數據](../Page/繞射.md "wikilink")，建構DNA模型。終於，二人在1953年提出DNA的雙螺旋結構，在同年4月25日將結果發表在《[自然](../Page/自然_\(期刊\).md "wikilink")》雜誌。

1956年沃森到哈佛大學當助教，1961年獲升為教授。1962年，與克里克、魏爾金斯因為對DNA結構的研究而共同獲得諾貝爾生理醫學獎。1965年出版了劃時代的教科書《基因的分子生物學》（*Molecular
Biology of the
Gene*），1968年出版《雙螺旋》，同年開始兼任位於[紐約](../Page/紐約.md "wikilink")[長島的](../Page/長島.md "wikilink")[冷泉港實驗室](../Page/冷泉港實驗室.md "wikilink")（Cold
Spring Harbor
Laboratory）的總管，並將研究方向轉移到[癌症](../Page/癌症.md "wikilink")。1976年辭去哈佛的職位，專注於冷泉港的職務。1988年，獲[國立衛生研究院委任為](../Page/國立衛生研究院_\(美國\).md "wikilink")[人类基因组计划的助理主管](../Page/人类基因组计划.md "wikilink")，一年後成為人體基因組研究國家中心的首任主管，擔當此職直至1992年。1994年，成為冷泉港首任總裁。

## 爭議

### 违规使用同行的研究成果

沃森和克里克在未经授权的情况下使用了[罗莎琳·富兰克林和](../Page/罗莎琳·富兰克林.md "wikilink")[雷蒙·葛斯林收集的DNA的X射线衍射数据来构建双螺旋DNA模型](../Page/雷蒙·葛斯林.md "wikilink")。\[2\]

### 攻击同行

在1960年代，[艾德華·威爾森在哈佛大学生物系系务会议上提议聘请一位种群生态学家](../Page/艾德華·威爾森.md "wikilink")，沃森声称「任何聘请生态学家的人都一定疯了」。\[3\]

沃森別開生面的科學自傳 （*The Double
Helix*）中對他自己以外的科學家多所批評，他對女科學家[羅莎琳·富蘭克林的男性沙文主義式評語](../Page/羅莎琳·富蘭克林.md "wikilink")，尤其惹人反感，因此在該書的
("Epilogue")中，他特別澄清說「我當初對她[羅莎琳·富蘭克林的印象](../Page/羅莎琳·富蘭克林.md "wikilink")，不論是科學上的還是個人方面的......都往往是錯的」。(...
my initial impressions of her, both scientific and personal ... were
often wrong ...)\[4\]

### 基因方面的言论

[Watson’s_Inner_Child.jpg](https://zh.wikipedia.org/wiki/File:Watson’s_Inner_Child.jpg "fig:Watson’s_Inner_Child.jpg")

2007年10月14日，《[星期日泰晤士報](../Page/泰晤士报.md "wikilink")》引述了沃森的言論，說[黑人先天](../Page/黑人.md "wikilink")[智力不如](../Page/智力.md "wikilink")[白人](../Page/白人.md "wikilink")，所以令他對非洲的前途未敢樂觀。\[5\]同年10月18日，倫敦的科學博物館(Science
Museum)，因為沃森的言論臨時取消了他的一場演講，表示他的言論已超過可以容忍的限度；\[6\]同時冷泉港也中止了他的職務。\[7\]
沃森隨即表示毫無保留地為他的言論道歉。\[8\]

此外他還覺得肥胖者「讓人看了感覺很糟，不想雇用他們」；對於所有「相貌不佳的女性」，還有「愚笨的人」，他認為應該以基因工程來治療。

2007年沃森最终被迫离开纽约[长岛的](../Page/长岛_\(纽约\).md "wikilink")[冷泉港实验室](../Page/冷泉港实验室.md "wikilink")。2019年1月，該實驗室剝奪他在冷泉港的名譽校長、名譽教授和名譽受託人在內的所有榮譽頭銜\[9\]\[10\]。

## 参见

  - [浙江大学沃森基因组科学研究院](../Page/浙江大学沃森基因组科学研究院.md "wikilink")

## 注釋

[分類:美國國家科學獎獲獎者](../Page/分類:美國國家科學獎獲獎者.md "wikilink")
[分類:印第安那大學校友](../Page/分類:印第安那大學校友.md "wikilink")
[分類:種族與智力爭議](../Page/分類:種族與智力爭議.md "wikilink")

[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:俄羅斯科學院院士](../Category/俄羅斯科學院院士.md "wikilink")
[Category:分子生物學家](../Category/分子生物學家.md "wikilink")
[Category:美國遺傳學家](../Category/美國遺傳學家.md "wikilink")
[Category:美國生物物理學家](../Category/美國生物物理學家.md "wikilink")
[Category:哈佛大学教授](../Category/哈佛大学教授.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[Category:剑桥大学克莱尔学院校友](../Category/剑桥大学克莱尔学院校友.md "wikilink")
[Category:美國自由意志主義者](../Category/美國自由意志主義者.md "wikilink")
[Category:美國無神論者](../Category/美國無神論者.md "wikilink")
[Category:愛爾蘭裔美國人](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:芝加哥人](../Category/芝加哥人.md "wikilink")
[Category:拉斯克基礎醫學研究獎得主](../Category/拉斯克基礎醫學研究獎得主.md "wikilink")
[Category:欧洲分子生物学组织会员](../Category/欧洲分子生物学组织会员.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")
[Category:罗蒙诺索夫金质奖章获得者](../Category/罗蒙诺索夫金质奖章获得者.md "wikilink")

1.  Watson, James D. (2001). *A Passion for DNA: Genes, Genomes, and
    Society*. Cold Spring Harbor, N.Y.: Cold Spring Harbor Laboratory
    Press. Page 3.
2.
3.
4.  Watson, James D. (1999\[1968\]). *The Double Helix*. Penguin Books.
    Page 175.
5.  Hunt-Grubbe, C. ["The elementary DNA of dear Dr.
    Watson"](http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/books/article2630748.ece),
    *Times Online*, October 14, 2007. Retrieved October 24, 2007.
6.  ["Museum drops race row
    scientist"](http://news.bbc.co.uk/2/hi/uk_news/england/london/7050020.stm),
    BBC, October 18, 2007. Retrieved October 24, 2007.
7.  Cold Springs Harbor Laboratory. October 18, 2007. [Statement by Cold
    Spring Harbor Laboratory Board of Trustees and President Bruce
    Stillman, Ph.D. Regarding Dr. Watson’s Comments in The Sunday Times
    on
    October 14, 2007](http://www.cshl.edu/public/releases/07_statement2.html)
    . Press release. Retrieved October 24, 2007.
8.  van Marsh, A. ["Nobel-winning biologist apologizes for remarks about
    blacks"](http://www.cnn.com/2007/WORLD/europe/10/18/nobel.apology/index.html),
    CNN, October 19, 2007. Retrieved October 24, 2007.
9.  [諾貝爾獎得主堅稱黑人智商先天比白人低 遭剝奪頭銜](https://udn.com/news/story/6812/3592199)
10.