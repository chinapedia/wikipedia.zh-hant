《**女人不哭**》（英文：****），是[中國中央電視臺於](../Page/中國中央電視臺.md "wikilink")2006年製作的勵志[電視劇](../Page/電視劇.md "wikilink")，共34集，由[田海蓉](../Page/田海蓉.md "wikilink")、[鄧超](../Page/鄧超.md "wikilink")、[姚芊羽](../Page/姚芊羽.md "wikilink")、[賈乃亮](../Page/賈乃亮.md "wikilink")、[湯唯等主演](../Page/湯唯.md "wikilink")。[田海蓉更憑此劇獲得](../Page/田海蓉.md "wikilink")[上海東方電影頻道](../Page/上海東方電影頻道.md "wikilink")2006年度「最佳收視明星獎」，亦為[湯唯](../Page/湯唯.md "wikilink")[電視上的成名作](../Page/電視.md "wikilink")，在結束拍攝兩個月後，她就被[李安選中成為](../Page/李安.md "wikilink")《[色戒](../Page/色戒.md "wikilink")》的女主角。

此劇曾在[湖南電視台](../Page/湖南電視台.md "wikilink")、[黑龍江電視台](../Page/黑龍江電視台.md "wikilink")、[山東電視台](../Page/山東電視台.md "wikilink")、[江苏卫视](../Page/江苏卫视.md "wikilink")、[湖北卫视](../Page/湖北卫视.md "wikilink")、[河北卫视](../Page/河北卫视.md "wikilink")、[吉林卫视](../Page/吉林卫视.md "wikilink")、[天津卫视](../Page/天津卫视.md "wikilink")、[上海東方電影頻道播映](../Page/上海東方電影頻道.md "wikilink")，於[湖南](../Page/湖南.md "wikilink")、[黑龍江](../Page/黑龍江.md "wikilink")、[山東等地均成為](../Page/山東.md "wikilink")[收視冠軍](../Page/收視.md "wikilink")。而2008年7月又將在[台灣上映](../Page/台灣.md "wikilink")，而於2008年，[香港](../Page/香港.md "wikilink")[亞洲電視購買得播放權](../Page/亞洲電視.md "wikilink")，適逢於[汶川大地震發生後播出](../Page/汶川大地震.md "wikilink")，於2008年6月4日起逢星期一至五晚上9:00-10:30播出，是第三套[Drama90系列劇集](../Page/Drama90.md "wikilink")。

## 演員阵容

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>人物介绍</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/田海蓉.md" title="wikilink">田海蓉</a><br />
<a href="../Page/杨丽晓.md" title="wikilink">杨丽晓</a></p></td>
<td><p>章子君<br />
小子君</p></td>
<td><p>章家大姐，父母在地震中丧生，此后肩负起抚养弟妹的重担</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄧超.md" title="wikilink">鄧超</a><br />
<a href="../Page/邱爽.md" title="wikilink">邱爽</a></p></td>
<td><p>趙劍<br />
小赵剑</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/賈乃亮.md" title="wikilink">賈乃亮</a><br />
<a href="../Page/石云鹏.md" title="wikilink">石云鹏</a><br />
<a href="../Page/陈松.md" title="wikilink">陈松</a></p></td>
<td><p>章子華<br />
童年子华<br />
少年子华</p></td>
<td><p>子君的弟弟，子月的哥哥</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/姚芊羽.md" title="wikilink">姚芊羽</a><br />
</p></td>
<td><p>阿梅<br />
小阿梅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湯唯.md" title="wikilink">湯唯</a></p></td>
<td><p>尚麗</p></td>
<td><p>子华的女朋友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉佳.md" title="wikilink">劉佳</a><br />
林静<br />
张妍</p></td>
<td><p>章子月<br />
童年子月<br />
少年子月</p></td>
<td><p>子君、子华的妹妹</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雷恪生.md" title="wikilink">雷恪生</a></p></td>
<td><p>老申頭</p></td>
<td><p>申家菜第七代传人，晓玉、海天的父亲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/趙亮.md" title="wikilink">趙亮</a></p></td>
<td><p>龐坤</p></td>
<td><p>曾经跟随老申头学习申家菜</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬麗.md" title="wikilink">馬麗</a></p></td>
<td><p>潘紅棉</p></td>
<td><p>潘蒙生的母亲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/赵滨.md" title="wikilink">赵滨</a></p></td>
<td><p>钱大磊</p></td>
<td><p>工厂的工人，后成为子月的丈夫</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李诚儒.md" title="wikilink">李诚儒</a></p></td>
<td><p>舅 舅</p></td>
<td><p>章家姐弟的舅舅</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李勤勤.md" title="wikilink">李勤勤</a></p></td>
<td><p>舅 妈</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杨冬.md" title="wikilink">杨冬</a></p></td>
<td><p>国强</p></td>
<td><p>章家姐弟的表弟</p></td>
</tr>
<tr class="odd">
<td><p>|国芳</p></td>
<td><p>章家姐弟的表妹，国强的妹妹</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/霍焰.md" title="wikilink">霍焰</a><br />
薛咏煜</p></td>
<td><p>潘蒙生<br />
童年蒙生/小华</p></td>
<td><p>子君结拜姐弟，排行老七</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/师春玲.md" title="wikilink">师春玲</a></p></td>
<td><p>申晓玉</p></td>
<td><p>老申头的三女儿</p></td>
</tr>
<tr class="even">
<td><p>刘文凤</p></td>
<td><p>申 静</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>杜鹃</p></td>
<td><p>申阳</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马可.md" title="wikilink">马可</a><br />
王亮</p></td>
<td><p>申海天<br />
童年海天</p></td>
<td><p>老申头的私生子</p></td>
</tr>
<tr class="odd">
<td><p>普超英</p></td>
<td><p>顾大妈</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>陈希光</p></td>
<td><p>成浩</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/丁海峰.md" title="wikilink">丁海峰</a></p></td>
<td><p>子君父</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郑卫莉.md" title="wikilink">郑卫莉</a></p></td>
<td><p>子君母</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>董 涛</p></td>
<td><p>吴杰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>卢 映</p></td>
<td><p>马大顺</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>温 浩</p></td>
<td><p>马小顺</p></td>
<td></td>
</tr>
</tbody>
</table>

## 主題曲

  - [中國](../Page/中國.md "wikilink")、[台灣版](../Page/台灣.md "wikilink") ──前路
      - 作曲：撈仔
      - 填詞：[卢永强](../Page/卢永强.md "wikilink")
      - 主唱：[姚貝娜](../Page/姚貝娜.md "wikilink")
  - [香港版](../Page/香港.md "wikilink") ──女人不哭
      - 作曲：撈仔
      - 填詞：[林若寧](../Page/林若寧.md "wikilink")
      - 主唱：[蔡立](../Page/蔡立.md "wikilink")

## 外部連結

  - [本劇香港aTV網站](https://web.archive.org/web/20080608083807/http://www.hkatv.com/v3/drama/08/silenttear/)


|align=center colspan=7|[師奶愛鬥大](../Page/師奶愛鬥大.md "wikilink")
5月7日- |- |align=center|**上一套：**
[火蝴蝶](../Page/火蝴蝶.md "wikilink")
\-6月3日 |align=center
colspan="5"|**[本港台第三線劇集](../Page/本港台電視劇集列表_\(2008年\)#第三線劇集.md "wikilink")
**女人不哭**
6月4日-7月8日
21:00-22:30 |align=center|**下一套：'''
[生命有明天](../Page/生命有明天.md "wikilink")
7月9日-

[Category:2006年中國電視劇集](../Category/2006年中國電視劇集.md "wikilink")
[Category:亞洲電視外購劇集](../Category/亞洲電視外購劇集.md "wikilink")
[Category:勵志題材電視劇](../Category/勵志題材電視劇.md "wikilink")
[Category:中國偶像劇](../Category/中國偶像劇.md "wikilink")