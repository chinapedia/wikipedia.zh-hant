**8月5日**是[阳历年的第](../Page/阳历.md "wikilink")217天（[闰年是](../Page/闰年.md "wikilink")218天），离一年的结束还有148天。

## 大事记

### 1世紀

  - [25年](../Page/25年.md "wikilink")：[刘秀正式称帝](../Page/刘秀.md "wikilink")，是为[东汉光武帝](../Page/东汉.md "wikilink")。

### 11世紀

  - [1100年](../Page/1100年.md "wikilink")：[亨利一世在](../Page/亨利一世_\(英格兰\).md "wikilink")[威斯敏斯特教堂加冕成为](../Page/威斯敏斯特教堂.md "wikilink")[英格兰国王](../Page/英格兰国王.md "wikilink")。

### 14世紀

  - [1305年](../Page/1305年.md "wikilink")：[苏格兰民族英雄](../Page/苏格兰.md "wikilink")[威廉·华莱士在](../Page/威廉·华莱士.md "wikilink")[格拉斯哥附近被](../Page/格拉斯哥.md "wikilink")[英格兰军队抓获](../Page/英格兰.md "wikilink")，并随即被囚送[伦敦](../Page/伦敦.md "wikilink")。

### 16世紀

  - [1580年](../Page/1580年.md "wikilink")：[漢弗萊·吉爾伯特於今](../Page/漢弗萊·吉爾伯特.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")[紐芬蘭與拉布拉多建立](../Page/紐芬蘭與拉布拉多.md "wikilink")[英國在](../Page/英國.md "wikilink")[北美地區的第一個殖民地](../Page/北美地區.md "wikilink")[聖約翰斯](../Page/聖約翰斯.md "wikilink")。
  - [1582年](../Page/1582年.md "wikilink")：應召前往中華傳教的[葡萄牙教士](../Page/葡萄牙.md "wikilink")[利瑪竇](../Page/利瑪竇.md "wikilink")，到達廣州香山澳（[澳門](../Page/澳門.md "wikilink")），開始了[天主教與](../Page/天主教.md "wikilink")[西學在廣州的首次傳播](../Page/西學東漸.md "wikilink")。
  - [1600年](../Page/1600年.md "wikilink")：苏格兰发生[高里阴谋事件](../Page/高里阴谋事件.md "wikilink")。

### 18世紀

  - [1772年](../Page/1772年.md "wikilink")：第一次[瓜分波蘭](../Page/瓜分波蘭.md "wikilink")－為了維持[哈布斯堡君主國](../Page/哈布斯堡君主國.md "wikilink")、[俄羅斯帝國和](../Page/俄羅斯帝國.md "wikilink")[普魯士王國於](../Page/普魯士王國.md "wikilink")[東歐地區的權力平衡共同簽訂瓜分](../Page/東歐.md "wikilink")[波蘭領土的條約](../Page/波蘭.md "wikilink")。

### 19世紀

  - [1813年](../Page/1813年.md "wikilink")：[中国严禁贩食](../Page/中国.md "wikilink")[鸦片](../Page/鸦片.md "wikilink")。

### 20世紀

  - [1919年](../Page/1919年.md "wikilink")：[山东](../Page/山东.md "wikilink")[济南镇守使](../Page/济南.md "wikilink")[马良制造](../Page/馬良_\(民國\).md "wikilink")“[济南血案](../Page/济南血案.md "wikilink")”。
  - [1949年](../Page/1949年.md "wikilink")：[厄瓜多尔发生里氏](../Page/厄瓜多尔.md "wikilink")6.8级[地震](../Page/1949年安巴托地震.md "wikilink")，约6000人死亡。
  - [1949年](../Page/1949年.md "wikilink")：[英國皇家海軍軍艦](../Page/英國皇家海軍.md "wikilink")「[紫水晶號](../Page/紫水晶號.md "wikilink")」抵達[香港](../Page/香港.md "wikilink")。當年4月，「紫水晶號」沿著[長江運送補給物資予駐南京的英國大使館](../Page/長江.md "wikilink")。當時[中國人民解放軍正發動](../Page/中國人民解放軍.md "wikilink")[渡江戰役](../Page/渡江戰役.md "wikilink")，「紫水晶號」駛到長江下游時，遭遇解放軍炮火攻擊，艦上包括艦長在內有17人喪生，20人受傷，艦身亦嚴重損毀無法航行，後來雙方多次談判才能脫險。
  - [1962年](../Page/1962年.md "wikilink")：[美國電影演员](../Page/美國電影.md "wikilink")[玛丽莲·梦露因过量服用](../Page/玛丽莲·梦露.md "wikilink")[安眠药死于](../Page/安眠药.md "wikilink")[洛杉矶的寓所中](../Page/洛杉矶.md "wikilink")。
  - [1966年](../Page/1966年.md "wikilink")：[文化大革命](../Page/文化大革命.md "wikilink")：[中国共产党中央委员会主席](../Page/中国共产党中央委员会主席.md "wikilink")[毛泽东写下](../Page/毛泽东.md "wikilink")《[炮打司令部——我的一张大字报](../Page/炮打司令部——我的一张大字报.md "wikilink")》在《[人民日报](../Page/人民日报.md "wikilink")》上發表，矛頭直指當時的[國家主席](../Page/中華人民共和國主席.md "wikilink")[刘少奇](../Page/刘少奇.md "wikilink")。指劉少奇站在[資產階級立場](../Page/資產階級.md "wikilink")，鎮壓無產階級文化大革命，阻礙文革的進行。毛澤東這篇文章正式為歷時十年的[文化大革命揭開序幕](../Page/文化大革命.md "wikilink")。
  - [1966年](../Page/1966年.md "wikilink")：[文化大革命](../Page/文化大革命.md "wikilink")：前[北京师范大学附属女子中学](../Page/北京师范大学.md "wikilink")（现称[北京师范大学附属实验中学](../Page/北京师范大学附属实验中学.md "wikilink")）党总支书记兼副校长[卞仲耘](../Page/卞仲耘.md "wikilink")，被该校[红卫兵学生打死于校中](../Page/红卫兵.md "wikilink")。
  - [1967年](../Page/1967年.md "wikilink")：[文化大革命](../Page/文化大革命.md "wikilink")：[刘少奇在](../Page/刘少奇.md "wikilink")[中南海被](../Page/中南海.md "wikilink")“斗争”。
  - [1988年](../Page/1988年.md "wikilink")：[国际中国科技史会议在美国](../Page/国际中国科技史会议.md "wikilink")[加州大学](../Page/加州大学.md "wikilink")[-{zh-hant:聖地牙哥;
    zh-cn:圣迭戈}-分校举行](../Page/加州大學聖地牙哥分校.md "wikilink")。
  - [1993年](../Page/1993年.md "wikilink")：中國[深圳](../Page/深圳.md "wikilink")[清水河街道一間儲存](../Page/清水河街道_\(深圳市\).md "wikilink")[硝酸銨的工廠發生爆炸](../Page/硝酸銨.md "wikilink")，並引發火警，釀成18死873傷。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：中国人工繁育成活第一只[大熊猫](../Page/大熊猫.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")：[印尼](../Page/印尼.md "wikilink")[雅加达的](../Page/雅加达.md "wikilink")[万豪酒店发生自杀式](../Page/JW萬豪酒店.md "wikilink")[汽车炸弹](../Page/汽车炸弹.md "wikilink")，共造成至少12人死亡，150人受伤。
  - [2004年](../Page/2004年.md "wikilink")：[廣州新白雲機場投入服務](../Page/广州白云国际机场.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")：[韓國女子天團](../Page/韓國.md "wikilink")[少女時代宣佈出道](../Page/少女時代.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：因受[债务危机的影响](../Page/2011年美國債務上限危機.md "wikilink")，[标普首次将](../Page/标准普尔.md "wikilink")[美国](../Page/美国.md "wikilink")[主权信用等级自AAA下调至AA](../Page/各國信用評級列表.md "wikilink")+。
  - [2012年](../Page/2012年.md "wikilink")：「[長者及合資格殘疾人士公共交通票價優惠計劃](../Page/長者及合資格殘疾人士公共交通票價優惠計劃.md "wikilink")」第二階段實施，涵蓋範圍除[港鐵外](../Page/港鐵.md "wikilink")，亦加入[九龍巴士](../Page/九龍巴士.md "wikilink")、[龍運巴士](../Page/龍運巴士.md "wikilink")、[城巴及](../Page/城巴.md "wikilink")[新世界第一巴士共](../Page/新世界第一巴士.md "wikilink")4間專營巴士公司。
  - [2013年](../Page/2013年.md "wikilink")：華盛頓郵報公司宣布用2.5億美元出售《華盛頓郵報》及其資產給[Amazon.com](../Page/Amazon.com.md "wikilink")[首席执行官](../Page/首席执行官.md "wikilink")[杰弗里·贝索斯](../Page/杰弗里·贝索斯.md "wikilink")。
  - [2016年](../Page/2016年.md "wikilink")：[夏季奧林匹克運動會在](../Page/夏季奧林匹克運動會.md "wikilink")[巴西](../Page/巴西.md "wikilink")[里約熱內盧的](../Page/里約熱內盧.md "wikilink")[馬拉卡納體育場舉行開幕典禮](../Page/馬拉卡納體育場.md "wikilink")。

## 出生

  - [1844年](../Page/1844年.md "wikilink")：[伊利亞·葉菲莫維奇·列賓](../Page/伊利亞·葉菲莫維奇·列賓.md "wikilink")，[俄羅斯畫家](../Page/俄羅斯.md "wikilink")（逝世於[1930年](../Page/1930年.md "wikilink")）
  - [1850年](../Page/1850年.md "wikilink")：[莫泊桑](../Page/莫泊桑.md "wikilink")，[法国作家](../Page/法国.md "wikilink")（逝世於[1893年](../Page/1893年.md "wikilink")）
  - [1855年](../Page/1855年.md "wikilink")：，英国气象学家。（逝世於[1927年](../Page/1927年.md "wikilink")）
  - [1862年](../Page/1862年.md "wikilink")：[约瑟夫·梅里克](../Page/约瑟夫·梅里克.md "wikilink")，一位被称作“象人”的身体严重畸形的[英国人](../Page/英国.md "wikilink")。（逝世於[1890年](../Page/1890年.md "wikilink")）
  - [1918年](../Page/1918年.md "wikilink")：[利國偉](../Page/利國偉.md "wikilink")，[香港著名銀行家](../Page/香港.md "wikilink")。（逝世於[2013年](../Page/2013年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[賈達德](../Page/賈達德.md "wikilink")，前[香港人民入境事務處處長](../Page/香港人民入境事務處.md "wikilink")。（逝世於[2016年](../Page/2016年.md "wikilink")）
  - [1930年](../Page/1930年.md "wikilink")：[尼尔·阿姆斯特朗](../Page/尼尔·阿姆斯特朗.md "wikilink")，第一位登陸[月球的美国太空人](../Page/月球.md "wikilink")（逝世於[2012年](../Page/2012年.md "wikilink")）
  - [1949年](../Page/1949年.md "wikilink")：[鄭東煥](../Page/鄭東煥.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[派屈克·尤英](../Page/派屈克·尤英.md "wikilink")，已退役[美國](../Page/美國.md "wikilink")[NBA球員](../Page/NBA.md "wikilink")，NBA50大巨星之一
  - [1966年](../Page/1966年.md "wikilink")：[璩美鳳](../Page/璩美鳳.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")
  - 1966年：[王瑞霞](../Page/王瑞霞.md "wikilink")，台灣[台語歌手](../Page/台語.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[王馨平](../Page/王馨平.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[马琳·勒庞](../Page/马琳·勒庞.md "wikilink")，法国政治人物
  - [1967年](../Page/1967年.md "wikilink")：[山內一典](../Page/山內一典.md "wikilink")，[日本電玩製作人](../Page/日本.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[周杰](../Page/周杰.md "wikilink")，[中国男演员](../Page/中国.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[川村美香](../Page/川村美香.md "wikilink")，日本[漫畫家](../Page/漫畫家.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[卡約兒·德烏根](../Page/卡約兒·德烏根.md "wikilink")，印度演員
  - [1975年](../Page/1975年.md "wikilink")：[洪天祥](../Page/洪天祥.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[權相佑](../Page/權相佑.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[廣橋涼](../Page/廣橋涼.md "wikilink")，[日本動畫聲優](../Page/日本動畫.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[郭靜](../Page/郭靜.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[柴崎幸](../Page/柴崎幸.md "wikilink")，日本女演員、歌手
  - [1982年](../Page/1982年.md "wikilink")：[柳承敏](../Page/柳承敏.md "wikilink")，[韩国](../Page/韩国.md "wikilink")[乒乓球](../Page/乒乓球.md "wikilink")[运动员](../Page/运动员.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[廖羽翹](../Page/廖羽翹.md "wikilink")，香港歌手
  - 1985年：[陳以岫](../Page/陳以岫.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[張正偉](../Page/張正偉.md "wikilink")，台灣[棒球選手](../Page/棒球.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[珍妮麗亞·狄索薩](../Page/珍妮麗亞·狄索薩.md "wikilink")，[印度女演员](../Page/印度.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[楊雅筑](../Page/楊雅筑.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - 1990年：[陳融瑩](../Page/陳融瑩.md "wikilink")，[台灣藝人](../Page/台灣.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[陳定](../Page/陳定.md "wikilink")，中國[田徑運動員](../Page/田徑.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[大後壽壽花](../Page/大後壽壽花.md "wikilink")，日本女演員
  - [1997年](../Page/1997年.md "wikilink")：[徐嬌](../Page/徐嬌.md "wikilink")，中國女演員
  - [冬木琉璃香](../Page/冬木琉璃香.md "wikilink")，日本漫畫家

## 逝世

  - [882年](../Page/882年.md "wikilink")：[路易三世](../Page/路易三世_\(西法蘭克\).md "wikilink")，[加洛林王朝的西法蘭克國王](../Page/加洛林王朝.md "wikilink")，與弟弟[卡洛曼二世一同成為西法蘭克的國王](../Page/卡洛曼二世_\(西法蘭克\).md "wikilink")，二王[共治](../Page/共治.md "wikilink")。（生於[863年](../Page/863年.md "wikilink")）
  - [1792年](../Page/1792年.md "wikilink")：[諾斯勳爵](../Page/諾斯勳爵.md "wikilink")，前[英國首相](../Page/英國首相.md "wikilink")（生於[1732年](../Page/1732年.md "wikilink"))
  - [1895年](../Page/1895年.md "wikilink")：[恩格斯](../Page/恩格斯.md "wikilink")，[哲學家](../Page/哲學家.md "wikilink")（生于[1820年](../Page/1820年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[蒋渭水](../Page/蒋渭水.md "wikilink")，[台湾民族运动领袖](../Page/台湾.md "wikilink")（生於[1890年](../Page/1890年.md "wikilink")）
  - [1962年](../Page/1962年.md "wikilink")：[瑪莉蓮·夢露](../Page/瑪莉蓮·夢露.md "wikilink")，美國電影演員（生於[1926年](../Page/1926年.md "wikilink")）
  - [1966年](../Page/1966年.md "wikilink")：[卞仲耘](../Page/卞仲耘.md "wikilink")，前北京师范大学附属女子中学校长兼党委书记（生於[1916年](../Page/1916年.md "wikilink")）
  - [1980年](../Page/1980年.md "wikilink")：[高崗](../Page/高崗_\(藝員\).md "wikilink")，[香港演员](../Page/香港.md "wikilink")（生于[1944年](../Page/1944年.md "wikilink")）
  - [1990年](../Page/1990年.md "wikilink")：[周克芹](../Page/周克芹.md "wikilink")，作家（生于[1937年](../Page/1937年.md "wikilink")）
  - [1991年](../Page/1991年.md "wikilink")：[本田宗一郎](../Page/本田宗一郎.md "wikilink")，[日本](../Page/日本.md "wikilink")[本田公司创始人](../Page/本田公司.md "wikilink")（生於[1906年](../Page/1906年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[陳儀馨](../Page/陳儀馨.md "wikilink")，[香港電視藝員](../Page/香港.md "wikilink")，曾於《[歡樂今宵](../Page/歡樂今宵.md "wikilink")》飾演阿妙（生於[1952年](../Page/1952年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[文英](../Page/文英.md "wikilink")，[台灣資深藝人](../Page/台灣.md "wikilink")（生於[1936年](../Page/1936年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[林-{杰}-樑](../Page/林杰樑.md "wikilink")，[台灣醫師](../Page/台灣.md "wikilink")、教授、毒物學權威（生於[1958年](../Page/1958年.md "wikilink")）\[1\]
  - 2013年：[喬治·杜克](../Page/喬治·杜克.md "wikilink")，美國鋼琴家、作曲家和音樂製作人（生於[1946年](../Page/1946年.md "wikilink")）
  - [2018年](../Page/2018年.md "wikilink")：[盧凱彤](../Page/盧凱彤.md "wikilink")，香港唱作歌手，二人組合[at17一員](../Page/at17.md "wikilink")（生於[1986年](../Page/1986年.md "wikilink")）

## 节假日和习俗

  - [基督教节日](../Page/基督教.md "wikilink")：
      - [雅代](../Page/雅代.md "wikilink")
  - [布基纳法索](../Page/布基纳法索.md "wikilink")：独立日
  - [克罗地亚](../Page/克罗地亚.md "wikilink")：国立感恩节

## 參考文獻

1.  [快訊／林杰樑急救不治　季芹原定探望「錯失最後一面」](http://www.ettoday.net/news/20130805/251158.htm)
    ，ETtoday 新聞雲，2013年08月5日