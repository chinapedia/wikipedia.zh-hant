[Huike_thinking.jpg](https://zh.wikipedia.org/wiki/File:Huike_thinking.jpg "fig:Huike_thinking.jpg")[石恪繪的慧可像](../Page/石恪.md "wikilink")\]\]
**慧可**，又名**僧可**，俗名**姬光**，號**神光**，[虎牢人](../Page/虎牢.md "wikilink")（[河南省](../Page/河南省.md "wikilink")[滎陽縣](../Page/滎陽.md "wikilink")），被尊為[禪宗二祖](../Page/禪宗.md "wikilink")。[唐德宗謚其為](../Page/唐德宗.md "wikilink")**大弘禪師**，塔名**大和之塔**。有「**正宗普覺大師**」與「**大祖禪師**」之諡號。

## 生平

他原是一位精通[儒學與](../Page/儒學.md "wikilink")[佛法的學者](../Page/佛法.md "wikilink")\[1\]，三十歲時至[洛陽](../Page/洛陽.md "wikilink")[龍門香山依寶靜禪師出家](../Page/龍門.md "wikilink")。四十歲時，至[嵩山從學於](../Page/嵩山.md "wikilink")[達摩門下六年](../Page/達摩.md "wikilink")，盡得其心法\[2\]，但是因為達摩的禪法並不為當時的人所接受，慧可也因此受到許多責難\[3\]。

達摩弟子人數並不多，其中[曇林與慧可間有深厚的友誼](../Page/曇林.md "wikilink")。曇林曾經遇賊，被砍一臂（後人稱為「無臂林」），幸好受到慧可的救護。後世傳說，慧可斷臂求法的故事，因是由此訛傳而來\[4\]。曇林長期在譯場擔任「筆受」的工作，是重視「經教」的法師，與慧可專心禪觀不同。

達摩涅槃後，曇林在[東魏](../Page/東魏.md "wikilink")[鄴都](../Page/鄴都.md "wikilink")（河南省[安陽縣](../Page/安陽縣.md "wikilink")）講授《[勝鬘經](../Page/勝鬘經.md "wikilink")》，慧可也在此傳授「達摩禪法」。當時那裏有位道恆禪師，門徒甚多，他指責慧可所傳的達摩禪是「魔語」，與官府結合對他加以迫害，慧可幾乎死去，慧可傳法的態度也因此轉變為低調順俗。

574年，[北周武帝滅佛](../Page/北周武帝滅佛.md "wikilink")，慧可與曇林在鄴都「共護經論」。577年，[北齊亡](../Page/北齊.md "wikilink")，慧可遁隱于[舒州](../Page/舒州.md "wikilink")[皖公山](../Page/天柱山.md "wikilink")（[安徽省](../Page/安徽省.md "wikilink")[潛山縣](../Page/潛山縣.md "wikilink")），度[僧璨出家](../Page/僧璨.md "wikilink")，傳以心法，是為禪宗三祖。579年，[北周宣帝恢復佛教](../Page/北周宣帝.md "wikilink")，慧可重還鄴都。慧可沒有固定寺院與徒眾，隱居在市井之間，有時為人[幫傭](../Page/幫傭.md "wikilink")，隨宜為人說法\[5\]。

[隋文帝](../Page/隋文帝.md "wikilink")[開皇十三年](../Page/開皇.md "wikilink")（593年）[圓寂](../Page/圓寂.md "wikilink")。相傳因為他在鄴都說法，受人嫉妒，遭官府下獄而死\[6\]。

## 教法

因為「達摩禪法」在當時仍不被多數人所接受，慧可的弟子人數不多，記述也不詳細。慧可雖以《楞伽經》教授門下，但是注重的是它的「玄理」，而非章節註疏，不著文字，活用教法，有重宗輕教的傾向\[7\]。在修行上，則是秉持達摩的「[頭陀行](../Page/頭陀.md "wikilink")」，不住聚落，這可能是早期達摩禪流傳不廣的原因之一\[8\]。但到了初唐，達摩禪逐漸發展成一個大的宗派，受到世人所接受。

## 注释

<div class="references-small">

<references />

</div>

[H慧](../Category/禪僧.md "wikilink") [H慧](../Category/南北朝僧人.md "wikilink")
[H慧](../Category/荥阳人.md "wikilink") [H慧](../Category/中国人瑞.md "wikilink")
[\~](../Category/姬姓.md "wikilink")

1.  《續僧傳》〈慧可傳〉：「外覽墳索，內通藏典。」
2.  《續僧傳》〈慧可傳〉：「奉以為師，畢命承旨，從學六年，精究一乘。」
3.  《續僧傳》〈慧可傳〉：「一時令望，咸共非之。」
4.  《楞伽師資記》：「吾本發心時，截一臂。從初夜雪中立，不覺雪過於膝，以求無上道。」
5.  《祖堂集》卷2：「或在城市，隨處任緣，或為人所使，事畢却還。彼所有智者，每勸之曰：『和尚是高人，莫與他所使。』師云：『我自調心，非關他事』。」
6.  《祖堂集》卷2：「時有辯和禪師，於鄴都管成安縣匡救寺講《涅槃經》。是時大師至彼寺門說法，集眾彼多，法師講下人少。辯和恠於師，遂往縣令瞿仲偘說之：『彼邪見道人打破講席。』瞿令不委事由，非理損害而終。」
7.  《續僧傳》〈慧可傳〉：「說此真法皆如實，與真幽理竟不殊。本迷摩尼謂瓦礫，豁然自覺是真珠。無明智慧等無異，當知萬法即皆如。愍此二見之徒輩，申詞措筆作斯書。觀身與佛不差別，何須更覓彼無餘。」
8.  《續僧傳》〈法沖傳〉：「以可常行兼奉頭陀，故其所住，不參邑落。」