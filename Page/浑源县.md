**浑源县**在中国[山西省东北部](../Page/山西省.md "wikilink")，是[大同市所辖的一个](../Page/大同市.md "wikilink")[县](../Page/县.md "wikilink")。面积1965平方公里，2010年共有常住人口343486人。

## 历史

[汉为](../Page/汉.md "wikilink")[崞县](../Page/崞县.md "wikilink")、[繁峙县地](../Page/繁峙县.md "wikilink")；[唐为](../Page/唐.md "wikilink")[云中县地](../Page/云中县.md "wikilink")，后分置浑源县；[元至元四年](../Page/元.md "wikilink")（1267年）并入[浑源州](../Page/浑源州.md "wikilink")，属[大同路](../Page/大同路.md "wikilink")；[民国元年](../Page/民国.md "wikilink")（1912年）五月改为浑源县。

## 行政区划

下辖6个[镇](../Page/行政建制镇.md "wikilink")、12个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 地理

浑源县位于山西省东北部，东连[广灵县](../Page/广灵县.md "wikilink")、[灵丘县](../Page/灵丘县.md "wikilink")，西接[应县](../Page/应县.md "wikilink")，南邻[繁峙县](../Page/繁峙县.md "wikilink")，北与[大同县](../Page/大同县.md "wikilink")、[阳高县毗连](../Page/阳高县.md "wikilink")，总面积1965平方公里。

境内有著名的北岳[恒山](../Page/恒山.md "wikilink")，海拔2016米。

主要河流有[浑河和](../Page/浑河.md "wikilink")[唐河](../Page/唐河.md "wikilink")。

## 交通

  - 公路：203省道、303省道

## 人口

2010年第六次全国人口普查浑源县共有常住人口343486，人口密度175人/平方公里。\[1\]

## 经济

2010年全县实现地区生产总值(GDP)27.3亿元，财政总收入4.06亿元。

## 风景名胜

  - [恒山](../Page/恒山.md "wikilink")
  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[悬空寺](../Page/悬空寺.md "wikilink")、[荆庄大云寺大雄宝殿](../Page/荆庄大云寺大雄宝殿.md "wikilink")、[浑源永安寺](../Page/浑源永安寺.md "wikilink")、[栗毓美墓](../Page/栗毓美墓.md "wikilink")、[浑源圆觉寺塔](../Page/浑源圆觉寺塔.md "wikilink")、[律吕神祠](../Page/律吕神祠.md "wikilink")、[浑源文庙](../Page/浑源文庙.md "wikilink")
  - [山西省文物保护单位](../Page/山西省文物保护单位.md "wikilink")：[麻庄汉墓群](../Page/麻庄汉墓群.md "wikilink")、[界庄遗址](../Page/界庄遗址.md "wikilink")、[古磁窑窑址](../Page/古磁窑窑址.md "wikilink")、[恒山建筑群](../Page/恒山建筑群.md "wikilink")

## 历代名人

  - [高维新](../Page/高维新.md "wikilink")
  - [雷渊](../Page/雷渊.md "wikilink")
  - [雷膺](../Page/雷膺.md "wikilink")
  - [栗毓美](../Page/栗毓美.md "wikilink")
  - [田应璜](../Page/田应璜.md "wikilink")
  - [石作衡](../Page/石作衡.md "wikilink")

## 参考文献

## 外部链接

  - [浑源县政府网站](http://www.hunyuan.gov.cn/)

[Category:大同](../Category/大同.md "wikilink")
[大同](../Category/山西省县份.md "wikilink")
[晋](../Category/国家级贫困县.md "wikilink")

1.  [大同市2010年第六次全国人口普查主要数据公报](http://www.sxxncw.cn/CN/Datong/JRGZ/1152514478630.html)