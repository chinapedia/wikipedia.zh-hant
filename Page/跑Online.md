是由韓國RHAON開發\[1\]，[SmileGate發行的網路競速遊戲](../Page/SmileGate.md "wikilink")，遊戲以跑這種競賽方式為特色吸引玩家，由於其童話風格及多樣化地圖，在各地很受好評。此為免費遊戲，官方不收取玩家費用，玩家可購買此遊戲的收費產品。2016年，[SmileGate宣布與開發商RHAON續約十年](../Page/SmileGate.md "wikilink")，並且將共同開發遊戲續作《TalesRunner
2》\[2\]。

## 伺服器訊息

| 国家／地区                                                         | 官方译名                               | 代理商                                                                                 | 时间           | 備註                                                                                                                                   |
| :------------------------------------------------------------ | :--------------------------------- | :---------------------------------------------------------------------------------- | :----------- | :----------------------------------------------------------------------------------------------------------------------------------- |
| [韩国](../Page/韩国.md "wikilink")                                | 테일즈런너                              | [NOWCOM](../Page/NOWCOM.md "wikilink")→[SmileGate](../Page/SmileGate.md "wikilink") | 2005年\~      | 因[AfreecaTV專心營運旗下項目](../Page/AfreecaTV.md "wikilink")，因此將遊戲代理權出售，[SmileGate於](../Page/SmileGate.md "wikilink")2014年12月 購入代理權並且成為新代理商 |
| [台灣](../Page/台灣.md "wikilink")                                | 新衝天跑                               | [戲谷](../Page/戲谷.md "wikilink")                                                      | 2006年\~      | 1.因合約到期而在於2015年將新衝天跑合併至香港伺服器 2.當前已合併至香港伺服器                                                                                           |
| [香港](../Page/香港.md "wikilink")/[澳門](../Page/澳門.md "wikilink") | \-{zh-hk:跑Online ; zh-tw:跑Online}- | [戲谷](../Page/戲谷.md "wikilink")                                                      | 2006年\~      | 港,澳,台於同一伺服器                                                                                                                          |
| [中國大陸](../Page/中國大陸.md "wikilink")                            | 超級跑跑                               | [盛大網絡](../Page/盛大網絡.md "wikilink")→[QIQU遊戲](../Page/QIQU遊戲.md "wikilink")           | 2007年\~2017年 | 因為QIQU遊戲代理營運效益不佳韓方原廠收回代理權限                                                                                                           |
| [泰國](../Page/泰國.md "wikilink")                                | TalesRunner TH                     | [TOT](../Page/TOT.md "wikilink")→[Asiasoft](../Page/Asiasoft.md "wikilink")         | 2008年\~      | 因為TOT在2016年的時候對於TalesRunner TH的代理合約已到期，所以將由Asiasoft獲得TalesRunner TH代理合約                                                              |
|                                                               |                                    |                                                                                     |              |                                                                                                                                      |

## 故事

整個遊戲的意念設定是將現實世界中小孩所幻想的童話王國實體化。

現實中不存在的童話王國，一直以童話國國民的[夢想以及](../Page/夢想.md "wikilink")[希望作為能量](../Page/希望.md "wikilink")，並創造出以[童話故事的美景點綴而成的人間樂土](../Page/童話故事.md "wikilink")。但因為童話國的國民不再擁有夢想與希望，令童話王國面臨重大危機，若這片土地失去原動力，現實世界的夢想與希望也會隨之而永久消失，童話王國的國王
-
亨利三世，為了改變現狀，決定舉辦一次以「賽跑」為題的比賽，讓參賽者能夠在比賽的過程裡，重拾早已被遺忘的童心，故此，國民都稱呼此次比賽的跑手為「童話跑者」(Tales
Runner)。

## 遊戲人物

本遊戲有不同的[遊戲角色供玩家們選擇及使用](../Page/遊戲角色.md "wikilink")。每個角色各有不同能力、配音，可在不同場地發揮各自的長處，亦可配以裝備及服飾以加強角色實力。

  - 基本能力值為最高速度、加速度、力量及控制，一些角色更具備特殊能力，例如額外憤怒時間、防止被踩機率等等。
  - 遊戲另設星級角色，多數為[明星及](../Page/明星.md "wikilink")[動漫角色](../Page/動漫.md "wikilink")。另外，遊戲中也設有**變身角色**，可以讓遊戲原創角色變成其他角色。

### 原創角色

| 角色名稱(中)                               | 角色名稱(英)      | 角色名稱(韓) | 最高速度 | 加速度 | 力量 | 控制 |
| :------------------------------------ | :----------- | :------ | :--- | :-- | :- | :- |
| 光光                                    | Chowon       | 초원      | 3    | 3   | 3  | 3  |
| 明明                                    | Mingming     | 밍밍      | 3    | 3   | 2  | 4  |
| 莉娜                                    | Lina         | 리나      | 1    | 6   | 2  | 5  |
| \-{zh-tw: 大隻仔; zh-hk: 大隻仔; zh-cn:大熊}- | BigBo        | 빅보      | 5    | 2   | 6  | 1  |
| 蒂蒂                                    | DnD          | DnD     | 4    | 2   | 3  | 3  |
| 納魯西斯                                  | Narucis      | 나르시스    | 6    | 1   | 3  | 4  |
| 琪琪                                    | Maki         | 마키      | 2    | 5   | 1  | 6  |
| \-{zh-tw: 雷; zh-hk: 軒少}-              | Rough        | 러프      | 4    | 3   | 5  | 2  |
| \-{zh-tw: 影之雷; zh-hk: 影子軒少}-          | Hidden Rough | 히든러프    | 6    | 3   | 4  | 2  |
| \-{zh-tw: 貝兒; zh-hk: 安琪}-             | Bada         | 바다      | 5    | 3   | 2  | 4  |
| \-{zh-tw: 凱; zh-hk: 占}-               | Kai          | 카이      | 5    | 5   | 2  | 2  |
| \-{zh-tw: 佐藤雪; zh-hk: 雪姬}-            | Yuki         | 유키      | 6    | 1   | 2  | 5  |
| \-{zh-tw: 佐藤深子; zh-hk: 映雪}-           | Kuro         | 쿠로      | 1    | 5   | 5  | 4  |
| \-{zh-tw: 艾貝爾; zh-hk: 阿貝爾}-           | Abel         | 아벨      | 4    | 5   | 3  | 2  |
| \-{zh-tw: 夏魯; zh-hk: 夏魯; zh-cn:哈魯}-   | Haru         | 하루      | 5    | 3   | 4  | 2  |
| \-{zh-tw: 薇拉; zh-hk: 維拉}-             | Vera         | 베라      | 6    | 5   | 2  | 1  |
| 孫悟空                                   | Ogong        | 손오공     | 5    | 4   | 3  | 2  |
| \-{zh-tw: 修; zh-hk: 阿修}-              | Siho         | 시호      | 6    | 3   | 2  | 3  |
| \-{zh-tw: 星淚; zh-hk: 星淚; zh-cn:露西}-   | Lucy         | 루시      | 4    | 4   | 3  | 3  |
| \-{zh-tw: 小櫻; zh-hk: 小櫻; zh-cn:美狐}-   | Miho         | 미호      | 5    | 6   | 2  | 1  |
| R                                     | R            | 알       | 6    | 1   | 6  | 1  |
| \-{zh-tw: 夏郎; zh-hk: 夏郞}-             | Harang       | 하랑      | 4    | 4   | 4  | 2  |
| \-{zh-tw: *未公布*; zh-hk: *未公布*}-       | Lala         | 라라      | 5    | 2   | 4  | 3  |

### 版本獨家角色

  - 泰國版獨家角色

| 角色名稱(中)                          | 角色名稱(英)  | 角色名稱(韓) | 最高速度 | 加速度 | 力量 | 控制 |
| :------------------------------- | :------- | :------ | :--- | :-- | :- | :- |
| [緊那羅](../Page/緊那羅.md "wikilink") | Kinnaree | 키나리     | 2    | 3   | 7  | 2  |
|                                  |          |         |      |     |    |    |

### 明星角色

  - [KARA角色](../Page/KARA.md "wikilink")
  - 於2009年推出，僅上架100天，已於2015年刪除角色資料。

| 角色名稱(中) | 角色名稱(英)    | 角色名稱(韓) | 最高速度 | 加速度 | 力量 | 控制 |
| :------ | :--------- | :------ | :--- | :-- | :- | :- |
| 奎俐      | Gyu-Ri     | 규리      | 6    | 6   | 7  | 4  |
| 昇延      | Seung-Yeon | 승연      | 6    | 7   | 6  | 4  |
| 荷拉      | Ha-Ra      | 하라      | 7    | 6   | 5  | 5  |
| 妮可      | Nicole     | 니콜      | 6    | 6   | 6  | 5  |
| 知英      | Ji-Young   | 지영      | 5    | 5   | 6  | 7  |
|         |            |         |      |     |    |    |

  - [miss A角色](../Page/miss_A.md "wikilink")
  - 於2012年推出，已於2015年刪除角色資料。

| 角色名稱(中) | 角色名稱(英) | 角色名稱(韓) | 最高速度 | 加速度 | 力量 | 控制 |
| :------ | :------ | :------ | :--- | :-- | :- | :- |
| 秀智      |         |         |      |     |    |    |
| 暎       |         |         |      |     |    |    |
| 佳       |         |         |      |     |    |    |
| 霏       |         |         |      |     |    |    |
|         |         |         |      |     |    |    |

  - [Block B角色](../Page/Block_B.md "wikilink")
  - 於2015年推出，已於2016年下架。

| 角色名稱(中) | 角色名稱(英) | 角色名稱(韓) | 最高速度 | 加速度 | 力量 | 控制 |
| :------ | :------ | :------ | :--- | :-- | :- | :- |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |

### 動漫角色

  - [守護甜心！角色](../Page/守護甜心！.md "wikilink")
  - 已於2016年下架。

| 角色名稱(中) | 角色名稱(英) | 角色名稱(韓) | 最高速度 | 加速度 | 力量 | 控制 |
| :------ | :------ | :------ | :--- | :-- | :- | :- |
| 亞夢      |         |         |      |     |    |    |
|         |         |         |      |     |    |    |

  - [Keroro軍曹角色](../Page/Keroro軍曹.md "wikilink")
  - 已於2016年下架。

| 角色名稱(中) | 角色名稱(英) | 角色名稱(韓) | 最高速度 | 加速度 | 力量 | 控制 |
| :------ | :------ | :------ | :--- | :-- | :- | :- |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |
|         |         |         |      |     |    |    |

## 遊戲特色內容

### 頻道

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>頻道</p></th>
<th style="text-align: left;"><p>說明</p></th>
<th style="text-align: left;"><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>新手頻道</p></td>
<td style="text-align: left;"><p>可以讓新加入的玩家試玩少部份的地圖來熟悉跑法。(此頻道有等級限制)</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>簡易頻道</p></td>
<td style="text-align: left;"><p>所有玩家都可以參加的頻道，難度一般。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>普通頻道</p></td>
<td style="text-align: left;"><p>可以在此頻道開啟Dr.Hell遊戲房，適合高手參加。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>召喚獸頻道</p></td>
<td style="text-align: left;"><p>遊戲時可以騎乘自己在農場裡面動物一起完成比賽。</p></td>
<td style="text-align: left;"><p>模式現已取消</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>吾係小兒科頻道</p></td>
<td style="text-align: left;"><p>此頻道屬於讓玩家猜謎遊戲內容，比賽結束時即可與其他模式一樣獲得TR幣、經驗值、卡片。</p>
<p>另外GM會在部分題目中設置獎勵，玩家答對的話可隨機獲得道具(玩家需持有普通參與獎勵、特殊參與獎勵道具才可獲得)。</p></td>
<td style="text-align: left;"><p>模式現已取消</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>騎士團頻道</p></td>
<td style="text-align: left;"><p>4-8位同一騎士團中的玩家可以組成一隊，與其他騎士團進行比賽。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>遊樂場頻道</p></td>
<td style="text-align: left;"><p>4位玩家可以一起進行小遊戲。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>挑戰頻道</p></td>
<td style="text-align: left;"><p>8位玩家可以一起進行遊戲。</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>競賽頻道</p></td>
<td style="text-align: left;"><p>內有3種模式</p></td>
<td style="text-align: left;"><p>均需以4:4/8:8進行組隊比賽</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>聊天頻道(No Go channel)</p></td>
<td style="text-align: left;"><p>聊天頻道，玩家可在此頻道中開設聊天房間。</p></td>
<td style="text-align: left;"><p>出現在韓國與泰國版本當中</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 模式

| 名稱       | 玩法                                        | 備註     |
| :------- | :---------------------------------------- | :----- |
| 8人競賽     | 由2-8位玩家組成的遊戲局(可設置為個人戰/團體戰)。               |        |
| 30人競賽    | 由9-30位玩家組成的遊戲局，另外也有刺激又好玩的生存地圖。            |        |
| 大魔王激鬥    | 遊戲玩法與魔王城相近，但是該模式是由30位玩家組成一個團隊，並且同心協力打倒魔王。 | 模式現已取消 |
| 魔王降臨     | 由8位玩家組成一個團隊，並且同心協力打倒魔王。                   |        |
| 突襲魔王城    | 由12-20位玩家組成一個團隊，並且同心協力打倒魔王。               |        |
| 阿努比斯頻道   | 由2-8位玩家組成一個團隊，並且同心協力打倒魔王。                 |        |
| 接力賽      | 由3位玩家組成1支隊伍，最多可分為6小隊(由房主決定)。              |        |
| HardCore | 困難版地圖的分類，適合高手參加。                          |        |
| 彩虹聯賽     | 在聯賽頻道自動搜尋與其實力相應的對手進行遊戲，並設有積分制度。           |        |
|          |                                           |        |

### 活動頻道

遊戲商會推出不同的限時頻道。玩家可在活動頻道中獲得頻道專用分數和道具，於兌換商店換取不同獎勵。這些頻道只會開放一段時間，活動時間過去便會關閉，活動頻道的兌換商店則會於頻道完結後一段時間才會關閉。

  - 大運動會
  - \-{zh-tw: 渾沌战争; zh-hk: 混沌戰爭; zh-cn: 渾沌战争}-
  - 小丑戀愛戰場
  - 西遊記
  - \-{zh-tw: 英雄戰1/2/3; zh-hk: 戰女神1/2/3}-
  - 糖果樂園1/2
  - 恐怖樂園
  - 小丑大戰
  - 冰雪女王
  - 男女戰爭(韓國版本專屬)
  - 小丑派對
  - 職業競爭運動會
  - Genesis
  - Alive
  - 花樣騎士(2017年12月\~2018年2月)
  - 混沌 ZERO(2018年7月\~2018年9月)

### 遊戲系統

| 名稱        | 說明                                                                   | 備註                   |
| :-------- | :------------------------------------------------------------------- | :------------------- |
| TalesChat | TalesChat裡，玩家可以和朋友對話，也可以使用裡面的人脈系統尋找朋友。                               |                      |
| 公園        | 玩家可以在公園裡使用各種系統，如扭蛋系統。                                                |                      |
| 農場        | 玩家可以在農場系統來打造屬於自己的風格的農場。                                              |                      |
| 鍊金術       | 玩家可以在這把**部分**時限遊戲裝備和寵物進行合成，令其增加能力。 玩家可以使用遊戲道具進行修復鍊金裝備及復活寵物           |                      |
| 每日點名      | 玩家可以在登入遊戲後打開每日簽到系統進行點名，就可以獲得道具獎勵。 玩家也可以累計登入天數來兌換最大點名獎。               | 目前已知只有韓國、中國大陸、北美版本新增 |
| 升級系統      | 升級系統中設有許多活動或者兌換商店，如Dr.Hell地下商店、童話Hyper賞等兌換商店和活動。                     | 此為香港版特有的系統           |
| Hot Time  | [GM會不定時設置Hot](../Page/GM.md "wikilink") Time活動，玩家可使用HotTime系統領取每日獎勵。 |                      |
| 申訴        | 玩家可以使用此系統，向GM投訴其他玩家使用外掛等問題。                                          |                      |
| 道具輔助系统    | 此系统擁有經驗值加倍、玩家攻擊保護的道具，並且玩家可以使用此系统的道具在遊戲比賽中使用，但是玩家每使用一次必須扣除一次的免費遊戲幣    | 目前已知韓國、中國大陸、北美版本有新增  |
| 守護靈       | 守護靈在裝備後，會自動跟隨玩家。玩家可以在遊戲時變成守護靈1次。                                     |                      |
| DailyBuff | 玩家每天會得到不同加成，在系統中升級可以令加成更強。系統共有五級，每天重置。                               |                      |
| 安全鎖       | 可以在帳號中設定4位密碼安全鎖，大部份功能需要輸入安全鎖才能使用。                                    | 目前僅韓跑推出              |
| 防止掛機系統    | 為防止鼠標自動點擊軟體，每20次相同模式/房間 將會跳出2位數字驗證。                                  |                      |
| 拍賣場       | 可以在拍賣場刊登道具讓其他玩家競標購買;或以最低價購買小丑刊登的道具。                                  | 目前僅韓跑推出              |
| MAU       | 在商店消費點數後的饋返積分，可於Bonus商店等地方使用。                                        | 只有韓國、港台版本新增          |
| VIP       | 經由消費或任務完成可獲得VIP積分，每月重置VIP等級，共分為10個等級。                                | 目前僅韓跑推出              |
| 染色        | 可在公園中-{zh-tw: 小曼; zh-hk: 小天使; zh-cn: 小曼}-的染色沙龍中進行服裝染色。               | 目前僅韓跑推出              |
| 檢舉審判系統    | 由玩家進行檢舉，達300罰分，可自行決定進入審判或直接懲罰，被懲罰會有監牢效果及強制穿上囚服。                      | 目前僅韓跑推出              |
|           |                                                                      |                      |
| 騎士團系統     | 可以派出自己的騎士團與玩家進行PVP亦可在混沌ZERO活動頻道中進行PVE。                               |                      |
|           |                                                                      |                      |

## 發行

  - [日本地區曾兩度推出遊戲](../Page/日本.md "wikilink")《テイルズランナー》、《テイルズランド》，其後因各問題，伺服器相繼終止營運。

<!-- end list -->

  - 北美區《TalesRunner》代理商[gPotato傳出與韓開發商](../Page/gPotato.md "wikilink")[RHAON產生糾紛](../Page/RHAON.md "wikilink")，最終迫使伺服器於2011年終止營運，北美區《TalesRunner》終止營運後，[私服問題惡化](../Page/私服.md "wikilink")。

<!-- end list -->

  - 韓代理商[NOWCOM於](../Page/NOWCOM.md "wikilink")2012年重奪北美區代理權，重開遊戲並且定位為[全球版](../Page/全球.md "wikilink")，但之後因部分原因而關閉[全球版伺服器](../Page/全球.md "wikilink")。

<!-- end list -->

  - 新加坡Winner Online《TalesRunner
    SGP》於2015年3月18日結束在新加坡的營運，伺服器、官方網站、討論區、專頁同步下線。

<!-- end list -->

  - 越南《GoGoRun》於2015年9月下旬突然宣佈終止營運，伺服器、官方網站、討論區下線。

<!-- end list -->

  - 台灣戲谷《-{zh-tw: 新衝天跑; zh-hk: 新衝天跑; zh-cn: 新衝天跑;}-
    》因合約到期，於2015年終止營運，台灣玩家獲安排轉移至香港《-{zh-hk:跑Online
    ; zh-tw:跑Online}-》繼續遊玩，
    同時，香港《-{zh-hk:跑Online ; zh-tw:跑Online}-》開始兼任TR於台灣的營運服務。

<!-- end list -->

  - 中國大陸盛大遊戲《超級跑跑》代理合約到期，因此由QIQU遊戲獲得代理合約。但於2017年終止營運

<!-- end list -->

  - 泰國《TalesRunner TH》於2016年宣布更換代理商。

<!-- end list -->

  - 西班牙《TalesRunner ES》於2016年11月中旬突然宣佈終止營運。

<!-- end list -->

  - 北美區《TalesRunner NA》於2017年3月下旬突然宣佈終止營運

## 官方軼事及爭議

  - 2012年，[AfreecaTV以](../Page/AfreecaTV.md "wikilink")70億韓元收購開發商RHAON
    20%股份，並在新平台上推出本遊戲\[3\]。
  - 2012年6月 開發小組更換
  - 2012年8月
    韓國開發小組([AfreecaTV](../Page/AfreecaTV.md "wikilink"))推出「System」更新
    進行地圖改版 引發玩家不滿
  - 2013年3月 [NOWCOM正式改名為AfreecaTV](../Page/NOWCOM.md "wikilink")\[4\]。
  - 2014年12月 [SmileGate以](../Page/SmileGate.md "wikilink")30億韓元購入代理權
    成為新的全球發行商。
  - 2015年10月 韓版更改Logo為純韓文圖標
  - 2016年 香港版本因應10週年活動而在香港地區舉辦現場電競比賽。
  - 2016年 中國大陸超級跑跑的伺服器將「仙之樹林」及「天使之翼」分流伺服器進行合併，並且將伺服器名稱重新更名為「幻想天使」。
  - 2017年9月
    網上傳出中國大陸超級跑跑將會與香港地區的跑Online合併，引起玩家不滿，事後並沒有真的轉移資料，而是將資料移至官方私服《奔跑吧》，不久後仍宣布結束營運
  - 2018年4月韓國nopp網域轉移至onstove，並由原先的官網啟動遊戲整合至SmileGate自家的Stove平台啟動
  - 2018年6月官方宣布正式展開第二季並於7月修改登入動畫及重塑官網

## 合作代言

  - 2005年
    [디앤디프로젝트成為遊戲代言人](../Page/디앤디프로젝트.md "wikilink")，並成為一般角色蒂蒂(DnD)。\[5\]。
  - 2009年11月 [KARA成為代言人](../Page/KARA.md "wikilink")。
  - 2012年7月 [Miss A成為代言人](../Page/Miss_A.md "wikilink")。
  - 2012年7月 與電視節目[Running Man合作](../Page/Running_Man.md "wikilink")
    推出「Running Man」頻道。
  - 2012年暑假 《RUSH》更新中 男子團體[2PM角色原本在更新計劃之內](../Page/2PM.md "wikilink")。
  - 2015年1月 [Block B成為民選](../Page/Block_B.md "wikilink")2015代言團體。
  - 2015年7月 [B1A4加入代言行列](../Page/B1A4.md "wikilink")。
  - 2015年9月 公佈與[YG娛樂合作](../Page/YG娛樂.md "wikilink")，共同慶祝營運十週年。
  - 2015年9月 [樂童音樂家加入代言行列](../Page/樂童音樂家.md "wikilink")。
  - 2015年11月 柳炳宰、安英美加入代言行列。
  - 2015年11月 [iKON加入代言行列](../Page/iKON.md "wikilink")。
  - 2015年11月 韓版舉行10週年線下派對，[Girl's
    Day加入代言行列](../Page/Girl's_Day.md "wikilink")。
  - 2016年2月 [GFRIEND加入代言行列](../Page/GFRIEND.md "wikilink")。
  - 2016年6月 與[防彈少年團合作](../Page/防彈少年團.md "wikilink")，6月15日公開11週年紀念活動。
  - 2016年9月 與韓劇[步步驚心：麗合作](../Page/步步驚心：麗.md "wikilink")，並公開4種角色寵物。
  - 2016年12月 與[SEVENTEEN合作](../Page/SEVENTEEN.md "wikilink")，推出了Hip-Hop
    Team,Vocal Team和Performance Team的角色寵物。
  - 2017年 與[ASTRO合作](../Page/ASTRO.md "wikilink")。
  - 2017年12月 [鄭彩娟拍攝廣告並成為代言人](../Page/鄭彩娟.md "wikilink")\[6\]。
  - 2018年7月 [路雲拍攝廣告並cosplay遊戲角色Harang](../Page/路雲.md "wikilink")。

## 主題曲

## 關聯產品

### 手機應用

<table>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p>-{zh-tw:語言 ; zh-hk:語系 ; zh-cn:语言}-</p></th>
<th><p>開發商</p></th>
<th><p>所屬地區</p></th>
<th><p>備注</p></th>
<th><p>類型</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>上架中</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-{zh-tw: 테일즈런너 패밀리 ; zh-hk: 테일즈런너 패밀리 ; zh-cn: 테일즈런너 패밀리}-</p></td>
<td><p><a href="../Page/韓文.md" title="wikilink">韓文</a></p></td>
<td><p><a href="../Page/AfreecaTV.md" title="wikilink">AfreecaTV</a></p></td>
<td></td>
<td><p>封鎖國外IP</p></td>
<td><p>Android</p></td>
</tr>
<tr class="odd">
<td><p>-{zh-tw: TalesRunner Family ; zh-hk: TalesRunner Family ; zh-cn: TalesRunner Family }-</p></td>
<td><p><a href="../Page/泰文.md" title="wikilink">泰文</a></p></td>
<td><p><a href="../Page/TOT.md" title="wikilink">TOT</a></p></td>
<td></td>
<td><p>/</p></td>
<td><p>iOS<br />
Android</p></td>
</tr>
<tr class="even">
<td><p>停止服務</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/TalesRunner_RUSH&amp;DASH_for_kakao.md" title="wikilink">TalesRunner RUSH&amp;DASH for kakao</a></p></td>
<td><p><a href="../Page/韓文.md" title="wikilink">韓文</a></p></td>
<td><p><a href="../Page/AfreecaTV.md" title="wikilink">AfreecaTV</a></p></td>
<td></td>
<td><p>於2015年3月17日停止服務</p></td>
<td><p>iOS<br />
Android</p></td>
</tr>
<tr class="even">
<td><p>-{zh-tw: TR Camera ; zh-hk: TR Camera ; zh-cn: TR Camera }-</p></td>
<td><p><a href="../Page/繁體中文.md" title="wikilink">繁體中文</a></p></td>
<td><p><a href="../Page/戲谷.md" title="wikilink">戲谷</a>（香港分公司）</p></td>
<td><p><br />
<br />
</p></td>
<td><p>已停止下載和更新</p></td>
<td><p>iOS<br />
Android</p></td>
</tr>
<tr class="odd">
<td><p>-{zh-tw: <a href="../Page/테일즈런너R.md" title="wikilink">테일즈런너R</a> ; zh-hk: <a href="../Page/테일즈런너R.md" title="wikilink">테일즈런너R</a> ; zh-cn: <a href="../Page/테일즈런너R.md" title="wikilink">테일즈런너R</a> }-</p></td>
<td><p><a href="../Page/韓文.md" title="wikilink">韓文</a></p></td>
<td><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td></td>
<td><p>已於2018年5月停止營運</p></td>
<td><p>iOS<br />
Android</p></td>
</tr>
</tbody>
</table>

### 智能電視應用

<table>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p>-{zh-tw:語言 ; zh-hk:語系 ; zh-cn:语言}-</p></th>
<th><p>開發商</p></th>
<th><p>所屬地區</p></th>
<th><p>備注</p></th>
<th><p>類型</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Tales Survival系列</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>테일즈런너 서바이벌</p></td>
<td><p><a href="../Page/英文.md" title="wikilink">英文</a></p></td>
<td><p><a href="../Page/Rhaon_Plus.md" title="wikilink">Rhaon Plus</a></p></td>
<td></td>
<td><p>共有兩關卡</p></td>
<td><p>Android</p></td>
</tr>
<tr class="odd">
<td><p>Tales Runner Survival For Clown Rescue Mission</p></td>
<td><p><a href="../Page/英文.md" title="wikilink">英文</a></p></td>
<td><p><a href="../Page/Rhaon_Plus.md" title="wikilink">Rhaon Plus</a></p></td>
<td></td>
<td><p>/</p></td>
<td><p>Android</p></td>
</tr>
<tr class="even">
<td><p>Tales Runner Survival For Advanced Mental Arithmetic!</p></td>
<td><p><a href="../Page/英文.md" title="wikilink">英文</a></p></td>
<td><p><a href="../Page/Rhaon_Plus.md" title="wikilink">Rhaon Plus</a></p></td>
<td></td>
<td><p>/</p></td>
<td><p>Android</p></td>
</tr>
</tbody>
</table>

### 教育漫畫

本遊戲目前在各地皆有出版教育圖書，設有不同系列，分別教授數理、歷史、生活常識等知識。
在2010年，有關的教育漫畫在美國、韓國、日本、泰國、台灣等市場累計銷量突破2500萬冊。

| 系列                                                | \-{zh-tw:語言 ; zh-hk:語系 ; zh-cn:语言}- | 作者/譯者     | 出版社          | 範疇 |
| ------------------------------------------------- | ----------------------------------- | --------- | ------------ | -- |
| 以《테일즈런너》名義發行                                      |                                     |           |              |    |
| 테일즈런너 영어킹왕짱                                       | [韓文](../Page/韓文.md "wikilink")      | 이강안       | 거북이북스        | 英語 |
| 코믹 테일즈런너 고고씽                                      | 스토리이펙트                              | 주니어김영사    | 圍棋           |    |
| 테일즈런너 과학 킹왕짱                                      | 이강안                                 | 거북이북스     | 科學           |    |
| 테일즈런너 과학 킹왕짱                                      | 이강안                                 | 거북이북스     | 數學           |    |
| 테일즈런너 나타부한 부수한자                                   | 이준범                                 | 천재코믹스     | 地理           |    |
| 테일즈런너 멘토와 일해보기                                    | 아리스토                                | 아르볼       | 常識           |    |
| 테일즈런너 이야기마법전사                                     | 주수정                                 | 장서가주니어    | 常識           |    |
| 以《-{zh-tw: 超級跑跑; zh-hk: 超級跑跑; zh-cn: 超級跑跑;}-》名義發行 |                                     |           |              |    |
| \-{zh-tw: 超級跑跑; zh-hk: 超級跑跑; zh-cn: 超級跑跑;}-科學王    | [簡體中文](../Page/簡體中文.md "wikilink")  | 數碼接觸      | 湖北少年兒童出版社    | 科學 |
| \-{zh-tw: 超級跑跑; zh-hk: 超級跑跑; zh-cn: 超級跑跑;}-數學王    | 數碼接觸                                | 湖北少年兒童出版社 | 數學           |    |
| \-{zh-tw: 超級跑跑; zh-hk: 超級跑跑; zh-cn: 超級跑跑;}-英語王    | 數碼接觸                                | 湖北少年兒童出版社 | 英語           |    |
| 以《Hiyo衝天跑》名義發行                                    |                                     |           |              |    |
| 衝天跑．數學王                                           | [繁體中文](../Page/繁體中文.md "wikilink")  | 金炫辰       | 廣智文化事業股份有限公司 | 數學 |

### 周邊商品

官方授權發布的其他產品。

<table>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p>販售地區</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Talesrunner蒐藏卡</p></td>
<td></td>
<td><p>卡片包含有造型與禮物卡，一包有4張卡。</p></td>
</tr>
</tbody>
</table>

## 官方網站

  - 韓國TalesRunner(테일즈런너)[官方網站](http://tr.nopp.co.kr/)(封鎖外來IP位址)
  - 香港TalesRunner(-{zh-hk:跑Online ;
    zh-tw:跑Online}-)[官方網站](http://www.talesrunner.com.hk/)(台灣與香港已合併)
  - 中國大陸TalesRunner（超級跑跑）(並未與香港合併)
  - 北美洲TalesRunner(TalesRunner
    NA)[官方網站](http://tr.ogplanet.com/)(封鎖外來IP位址)

## 参考资料

[Category:2005年电子游戏](../Category/2005年电子游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:多人線上競速遊戲](../Category/多人線上競速遊戲.md "wikilink")
[Category:卡通渲染风格电子游戏](../Category/卡通渲染风格电子游戏.md "wikilink")
[Category:韩国开发电子游戏](../Category/韩国开发电子游戏.md "wikilink")
[Category:盛大游戏](../Category/盛大游戏.md "wikilink")

1.  [RHAON Talesrunner頁面](http://www.rhaon.co.kr/games/talesrunner.php)
2.  [韓服“超級跑跑”續約10年2代後續作開發中](http://ol.tgbus.com/news/newgame/hr/201603/13780549.shtml)，著名人如Leo
    Chen 亦表示喜愛玩
3.  [韓服《超級跑跑》運營商收購開發商20%股份(2012-12-13 17173新聞中心)](http://news.17173.com/content/2012-12-13/20121213142329754.shtml)
4.  [나우콤, '아프리카 TV'로 회사명 변경
    추진](http://www.thisisgame.com/webzine/news/nboard/4/?n=42171)
5.  [DnD Project(디앤디프로젝트) 테일즈런너를 아직도 즐겨하세요? 그렇다면
    기억하실거에요\!](https://www.youtube.com/watch?v=_GDmEhHbLMw)
6.  [오,나의 기사님\! 겨울방학 업데이트 홍보영상 (with
    정채연)](https://www.youtube.com/watch?v=Mvclis6d18k)