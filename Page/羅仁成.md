**羅仁權**（1975年2月21日－），為[台灣的](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手之一](../Page/選手.md "wikilink")，曾經效力於[台灣大聯盟](../Page/台灣大聯盟.md "wikilink")[雷公隊](../Page/雷公.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")。

## 經歷

  - [花蓮縣光復國中青少棒隊](../Page/花蓮縣.md "wikilink")
  - [花蓮縣國光商工青棒隊](../Page/花蓮縣.md "wikilink")
  - [輔仁大學棒球隊](../Page/輔仁大學.md "wikilink")
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛隊](../Page/興農牛.md "wikilink")
  - [花蓮縣花蓮體中青棒隊教練兼管理](../Page/花蓮縣.md "wikilink")

## 職棒生涯成績

| 年度    | 球隊                               | 出賽 | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死 | 三振 | 責失 | 投球局數 | 防禦率  |
| ----- | -------------------------------- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | ---- | ---- |
| 2001年 | [興農牛](../Page/興農牛.md "wikilink") | 1  | 0  | 1  | 0  | 0  | 0  | 0  | 4  | 0  | 2  | 3.0  | 6.00 |
| 2002年 | [興農牛](../Page/興農牛.md "wikilink") | 35 | 2  | 5  | 0  | 0  | 0  | 0  | 45 | 31 | 37 | 71.0 | 4.69 |
| 2003年 | [興農牛](../Page/興農牛.md "wikilink") | 12 | 0  | 1  | 0  | 0  | 0  | 0  | 6  | 3  | 9  | 16.2 | 4.86 |
| 合計    | 3年                               | 48 | 2  | 7  | 0  | 0  | 0  | 0  | 55 | 34 | 48 | 90.2 | 4.77 |

## 特殊事蹟

## 外部連結

[R](../Category/羅姓.md "wikilink") [L](../Category/台灣棒球教練.md "wikilink")
[L](../Category/在世人物.md "wikilink")
[L](../Category/1975年出生.md "wikilink")
[L](../Category/台灣棒球選手.md "wikilink")
[L](../Category/興農牛隊球員.md "wikilink")
[L](../Category/輔仁大學校友.md "wikilink")