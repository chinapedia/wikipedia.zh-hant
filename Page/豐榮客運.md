**豐榮汽車客運股份有限公司**（），簡稱**豐榮客運**，是一家主要業務為經營[臺中市市區公車與](../Page/臺中市市區公車.md "wikilink")[公路客運的業者](../Page/台灣汽車客運#公路客運.md "wikilink")。

[豐榮客運_716-FY.JPG](https://zh.wikipedia.org/wiki/File:豐榮客運_716-FY.JPG "fig:豐榮客運_716-FY.JPG")
[豐榮客運_987-U8.jpg](https://zh.wikipedia.org/wiki/File:豐榮客運_987-U8.jpg "fig:豐榮客運_987-U8.jpg")

## 沿革

1968年8月15日，振昌興業股份有限公司（又稱振昌木業，昔日[南投縣](../Page/南投縣.md "wikilink")[水里車埕地區](../Page/水里鄉.md "wikilink")[原木加工產業主要業者](../Page/原木.md "wikilink")）成立「**振昌汽車客運股份有限公司**」，主要經營[南投縣地區的公路客運](../Page/南投縣.md "wikilink")。

1998年，因應都會區域發展，振昌客運由[臺中縣](../Page/臺中縣.md "wikilink")[遊覽車業者](../Page/遊覽客運.md "wikilink")**豐勢通運有限公司**接手更名為「豐榮汽車客運股份有限公司」，並重整公司經營架構，逐年提昇車輛等硬體設備；另外亦結合其遊覽車業務經驗，以超越傳統客運業者經營模式。

2003年，獲政府許可經營[國道路線](../Page/中華民國國道.md "wikilink")「[臺北東區](../Page/東區_\(台北市\).md "wikilink")
- [埔里](../Page/埔里鎮.md "wikilink") - [日月潭](../Page/日月潭.md "wikilink")」。

2004年11月，正式開始經營國道路線，並將該路線設定為[日月潭地區之觀光](../Page/日月潭.md "wikilink")[巴士](../Page/巴士.md "wikilink")。

2006年，開始配合日月潭多家[飯店業者及](../Page/飯店.md "wikilink")[九族文化村推出](../Page/九族文化村.md "wikilink")[自由行套裝行程](../Page/自由行.md "wikilink")。

2012年10月1日，旗下路線臺中市公車[127路開始營運](../Page/台中市公車127路.md "wikilink")。

2013年6月1日，接駛原由[仁友客運經營的台中市公車](../Page/仁友客運.md "wikilink")[40路](../Page/台中市公車40路.md "wikilink")、[48路](../Page/台中市公車48路.md "wikilink")、[89路](../Page/台中市公車89路.md "wikilink")。

2017年11月1日，接使原由[豐原客運經營的](../Page/豐原客運.md "wikilink")[台中市公車228路](../Page/台中市公車228路.md "wikilink")

## 營運資訊

### [臺中市公車](../Page/臺中市公車.md "wikilink")

| 路線編碼                                                                | 營運區間                                                                              | 備註                                                                                                                            |
| ------------------------------------------------------------------- | --------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| **[<font color="red">40</font>](../Page/台中市公車40路.md "wikilink")**   | 臺中車站－中臺新村                                                                         | 2013年6月1日接駛、[標誌(小)mark(S).jpg](https://zh.wikipedia.org/wiki/File:標誌\(小\)mark\(S\).jpg "fig:標誌(小)mark(S).jpg")全部班次配置低地板公車。    |
| **[<font color="red">48</font>](../Page/台中市公車48路.md "wikilink")**   | 臺中車站－嶺東科技大學                                                                       | 2013年6月1日接駛、[標誌(小)mark(S).jpg](https://zh.wikipedia.org/wiki/File:標誌\(小\)mark\(S\).jpg "fig:標誌(小)mark(S).jpg")全部班次配置低地板公車。    |
| **[<font color="red">89</font>](../Page/台中市公車89路.md "wikilink")**   | 嶺東科技大學－東門橋                                                                        | 2013年6月1日接駛、[標誌(小)mark(S).jpg](https://zh.wikipedia.org/wiki/File:標誌\(小\)mark\(S\).jpg "fig:標誌(小)mark(S).jpg")全部班次配置低地板公車。    |
| **[<font color="red">127</font>](../Page/台中市公車127路.md "wikilink")** | [臺中市洲際棒球場](../Page/臺中市洲際棒球場.md "wikilink")－[豐樂雕塑公園](../Page/豐樂雕塑公園.md "wikilink") | 2012年10月1日正式營運、[標誌(小)mark(S).jpg](https://zh.wikipedia.org/wiki/File:標誌\(小\)mark\(S\).jpg "fig:標誌(小)mark(S).jpg")全部班次配置低地板公車。 |
| **[<font color="red">228</font>](../Page/台中市公車228路.md "wikilink")** | 大鵬國小－豐原東站                                                                         | 2017年11月1日接駛、[標誌(小)mark(S).jpg](https://zh.wikipedia.org/wiki/File:標誌\(小\)mark\(S\).jpg "fig:標誌(小)mark(S).jpg")全部班次配置低地板公車。   |

### 公路客運營運路線

| 路線編碼                              | 營運區間       | 備註 |
| --------------------------------- | ---------- | -- |
| **<font color="red">6288</font>** | 水\-{里}-－雙龍 |    |
| **<font color="red">6289</font>** | 水\-{里}-－埔里 |    |

## 使用車輛

### [日本](../Page/日本.md "wikilink")[豐田（TOYOTA）](../Page/豐田.md "wikilink")

[豐榮客運_406-FX_20140315.jpg](https://zh.wikipedia.org/wiki/File:豐榮客運_406-FX_20140315.jpg "fig:豐榮客運_406-FX_20140315.jpg")

  - 原裝進口車([國瑞汽車裝嵌](../Page/國瑞汽車.md "wikilink"))
  - 引擎：TOYOTA NO4C-UH
      - 排氣量：4009cc
      - 最大馬力：150ps(110kW)/2700rpm
      - 最大扭力：40.51kg-m/1800rpm
  - 變速：手排(前5後1)
  - 全/車長：6,990mm
  - 全/車寬：2,025mm
  - 全/車高：2,630mm
  - 軸距：3,935mm
  - 領牌年份：2012
  - 領用車號：403-FX\~407-FX
  - 現有4輛

### [南韓](../Page/南韓.md "wikilink")[大宇（DAEWOO）](../Page/大宇巴士.md "wikilink")（臺灣關係企業：成運汽車）

[豐榮客運_397-FX_20140315.jpg](https://zh.wikipedia.org/wiki/File:豐榮客運_397-FX_20140315.jpg "fig:豐榮客運_397-FX_20140315.jpg")

  - 車體廠：成運
  - 引擎：斗山(Doosan) DL08S
      - 排氣量：7640cc
      - 最大馬力：300ps(221kW)/2200rpm
      - 最大扭力：115kg-m/1200rpm
  - 變速系統：自排(前6後1)
  - 全長：11,990mm
  - 軸距：6,100mm
  - 製造年份：2012
  - 領牌範圍：395-FX\~402-FX、457-FX
  - 現有9輛
  - 395-FX\~402-FX配置於台中市公車48路/127路、457-FX配置於6289路

[豐榮客運DAEWOO_987-U8.JPG](https://zh.wikipedia.org/wiki/File:豐榮客運DAEWOO_987-U8.JPG "fig:豐榮客運DAEWOO_987-U8.JPG")

  - 廠牌：[韓國](../Page/韓國.md "wikilink") [大宇巴士](../Page/大宇巴士.md "wikilink")
  - 總代理：成運汽車
  - 車體廠：成運汽車
  - 引擎：斗山(Doosan) DL08K
      - 排氣量：7640cc
      - 最大馬力：300ps(221kW)/2200rpm
      - 最大扭力：115kg-m/1200rpm
  - 變速系統：Allison自排(前6後1)
  - 歐盟五期排放標準（EURO5）
  - 車長 11,990mm
  - 軸距 6,100mm
  - 製造年份：2015
  - 領牌範圍：987-U8
  - 僅此1輛
  - [三重客運](../Page/三重客運.md "wikilink")、[國光客運](../Page/國光客運.md "wikilink")、[巨業交通](../Page/巨業交通.md "wikilink")、[桃園客運](../Page/桃園客運.md "wikilink")、[鼎東客運](../Page/鼎東客運.md "wikilink")、[金門縣車船管理處亦購買同款車種](../Page/金門縣車船管理處.md "wikilink")
  - 該款車型和其他客運車型不同點為，其它客運配置ZF前軸

### [中國](../Page/中國大陸.md "wikilink")[中通客車（ZHONGTONG）](../Page/中通客車（ZHONGTONG）.md "wikilink")

[豐榮客運_KKA-6028.JPG](https://zh.wikipedia.org/wiki/File:豐榮客運_KKA-6028.JPG "fig:豐榮客運_KKA-6028.JPG")

  - 引擎：康明斯(CUMMINS)ISB4.5E5207B
      - 排氣量：4461.6cc
      - 最大馬力：207hp(147.7kW)/2300rpm
      - 最大扭力：76.07kg-m/1400rpm
  - 變速系統:Allison T270R 自排(前6後1)
  - 全/車長 8,995mm
  - 全/車寬 2,480mm
  - 全/車高 3,280mm
  - 共17輛
  - 交通部公共運輸計畫補助購置
  - 領用車號：
    2015：KKA-6027\~KKA-6029
    2016：KKA-6068\~KKA-6078、KKA-6133、KKA-6216、KKA-6217
    2017：KKA-6287
  - KKA-6216、KKA-6217改裝為自駕公車
  - [南台灣客運亦有購入此款車輛](../Page/南台灣客運.md "wikilink")

## 歷史車輛

### [日本](../Page/日本.md "wikilink")[日野（HINO）](../Page/日野.md "wikilink")

[Green_Transit_711-FY_20131109.jpg](https://zh.wikipedia.org/wiki/File:Green_Transit_711-FY_20131109.jpg "fig:Green_Transit_711-FY_20131109.jpg")
[豐榮客運-716-FY.JPG](https://zh.wikipedia.org/wiki/File:豐榮客運-716-FY.JPG "fig:豐榮客運-716-FY.JPG")

  - 車體廠 豐榮
  - 引擎 [日野](../Page/日野.md "wikilink")（HINO）J08C-TP
  - 最大馬力：203ps
  - 最大扭力：60kg-m
  - 全長 10,500mm(ERK2JML)/11,620mm(ERK2JRL)
  - 軸距 5,100mm(ERK2JML)/5,800mm(ERK2JRL)
  - 變速 手排（前5後1）
  - 底盤年份：2003、2005
  - 領牌年份：2011、2012
  - 領用車號:
      - 2003 ERK2JML：749-FT、750-FT、301-FX\~303-FX
      - 2005 ERK2JRL：KKA-6065(原705-FY換牌)、706-FY\~709-FY、711-FY
      - 2005 ERK2JML：712-FY\~718-FY
  - 前[新店客運](../Page/新店客運.md "wikilink")(2003)、[大南汽車](../Page/大南汽車.md "wikilink")(2005)用車
  - 已全數淘汰
  - 部分車輛轉賣至高鳳駕訓班([高雄客運關係企業](../Page/高雄客運.md "wikilink"))

### [南韓](../Page/南韓.md "wikilink")[大宇（DAEWOO）](../Page/大宇巴士.md "wikilink")（臺灣關係企業：成運汽車）

  - 製造年份：2006
  - 車體廠 成運
  - 引擎 待查
  - 最大馬力 350ps
  - 全長 待查
  - 軸距 待查
  - 領牌範圍:558-FH、559-FH
  - 共計2輛，已全數轉賣至遊覽業者

<!-- end list -->

  - 製造年份：2006
  - 車體廠 成運
  - 引擎 待查
  - 最大馬力 285ps
  - 全長 mm
  - 軸距 mm
  - 領牌範圍：853-U8\~856-U8
  - 原[首都客運](../Page/首都客運.md "wikilink")2xx-FL
  - 共計3輛，已全数淘汰

### [日本](../Page/日本.md "wikilink")[三菱扶桑（MITSUBISHI FUSO）](../Page/三菱扶桑.md "wikilink")

[豐榮客運_252-FC_20140315.jpg](https://zh.wikipedia.org/wiki/File:豐榮客運_252-FC_20140315.jpg "fig:豐榮客運_252-FC_20140315.jpg")

  - 車體廠：詠程車體(仿造 Setra S 415 HD)
  - 座椅：
  - 冷氣：開利冷氣
  - 引擎：
  - 最大馬力：350ps
  - 變速：手排
  - 乘客座位數：
  - 底盤年份：2003年
  - 領牌年份：2004年
  - 領牌範圍：250-FC\~260-FC
  - 共計10輛，已轉賣至[全航客運遊覽部或豐勢通運](../Page/全航客運.md "wikilink")

[Green_Transit_247-FC_20140713.jpg](https://zh.wikipedia.org/wiki/File:Green_Transit_247-FC_20140713.jpg "fig:Green_Transit_247-FC_20140713.jpg")
[Green_Transit_248-FC_20140108.jpg](https://zh.wikipedia.org/wiki/File:Green_Transit_248-FC_20140108.jpg "fig:Green_Transit_248-FC_20140108.jpg")

  - 車體 詠程車體(仿造 Setra S8)
  - 引擎 待查
  - 最大馬力 203ps
  - 全/車長 待查
  - 全/車寬 待查
  - 全/車高 待查
  - 軸距 待查
  - 變速 手排（前5後1）
  - 領牌年份: 2003
  - 領用車號: 247-FC、248-FC
  - 共計2輛，已全數汰換
  - 俗稱龍貓公車

<!-- end list -->

  - 車體 詠程車體
  - 座椅：
  - 冷氣：開利冷氣
  - 引擎：
  - 最大馬力 203ps
  - 全/車長 待查
  - 全/車寬 待查
  - 全/車高 待查
  - 軸距 待查
  - 變速 手排（前5後1）
  - 領牌年份: 2005
  - 領用車號: 523-FH\~527-FH
  - 共計4輛，已全數轉賣至豐勢通運

<!-- end list -->

  - 車體廠 日本三菱 (打造) 健益汽車 (裝嵌)
  - 引擎 三菱 (MITSUBISHI) 4D34-3AT3B
      - 最大馬力 147ps
  - 全/車長 6,990mm
  - 全/車寬 2,010mm
  - 全/車高 2,630mm
  - 軸距 3,995mm
  - 變速 手排（前5後1）
  - 底盤年份 1999
  - 領牌年份 2003
  - 乘客座位數：20座
  - 領牌範圍 232-FC\~236-FC
  - 分類 乙類大客車
  - 共計4輛，已全數汰換

## 營業站

  - 水\-{里}-站：水里鄉民權路89號

[File:GreenBusShueiliStation1.JPG|水-{里}-站外觀](File:GreenBusShueiliStation1.JPG%7C水-%7B里%7D-站外觀)
[File:GreenBusShueiliStation.JPG|水-{里}-站內部與路線圖](File:GreenBusShueiliStation.JPG%7C水-%7B里%7D-站內部與路線圖)
[File:GreenBusShueiliStop.JPG|水-{里}-站站牌](File:GreenBusShueiliStop.JPG%7C水-%7B里%7D-站站牌)

  - 埔里站：埔里鎮中正路324號（全航客運埔里站代售）
  - 日月潭代售處：南投縣魚池鄉中山路163號（日月潭水陸交通聯合服務站）

## 參考來源

  - [豐榮汽車客運股份有限公司全球資訊網](http://www.gbus.com.tw/)
      - [監理服務網](https://www.mvdis.gov.tw/m3-emv-mk3/tourBus/newCarQuery?method=pagination#anchor)

## 相關條目

  - [臺中市公車](../Page/台中市公車.md "wikilink")
  - [南投客運](../Page/南投客運.md "wikilink")
  - [中華民國汽車客運業者列表](../Page/中華民國汽車客運業者列表.md "wikilink")
  - [中華民國汽車客運車站列表](../Page/中華民國汽車客運車站列表.md "wikilink")
  - [GO運輸](../Page/GO運輸.md "wikilink")

## 外部連結

  - [豐榮汽車客運股份有限公司全球資訊網](http://www.gbus.com.tw/)

[F](../Category/台灣客運公司.md "wikilink")
[F](../Category/南投縣交通.md "wikilink")
[F](../Category/總部位於台中市的工商業機構.md "wikilink")
[F](../Category/1968年成立的公司.md "wikilink")
[ㄈ(F)](../Category/台中市公車.md "wikilink")