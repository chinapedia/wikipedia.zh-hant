**《西游记后传》**是导演[曹荣以](../Page/曹榮_\(演員\).md "wikilink")《[西游记](../Page/西游记.md "wikilink")》的故事为背景，拍摄的一部30集古装神话电视连续剧。主要讲述唐僧师徒取经归来后，得知[如来](../Page/如来.md "wikilink")[圆寂](../Page/圆寂.md "wikilink")，魔头无天借此机会想统治三界，最终被孙悟空舍身除掉的故事。本剧于2000年在中国各地电视台播出。

## 演员表

|                                         |                                    |                       |
| --------------------------------------- | ---------------------------------- | --------------------- |
| **演员**                                  | **角色**                             | **简介**                |
| [曹　荣](../Page/曹荣.md "wikilink")         | [孙悟空](../Page/孙悟空.md "wikilink")   | 斗战胜佛→南无大圣舍利尊王佛        |
| [闾汉彪](../Page/闾汉彪.md "wikilink")        | [猪八戒](../Page/猪八戒.md "wikilink")   | 净坛使者菩萨→木母金莲佛          |
| [黄海冰](../Page/黄海冰.md "wikilink")        | [唐　僧](../Page/唐三藏.md "wikilink")   | 旃檀功德佛→无量功德佛           |
| [李　京](../Page/李京.md "wikilink")         | [沙　僧](../Page/沙僧.md "wikilink")    | 南无八宝金身罗汉菩萨→金身光王佛      |
| [张志伟](../Page/张志伟_\(演员\).md "wikilink") | [小白龙](../Page/白龙马.md "wikilink")   | 八部天龙广力菩萨→金身广力龙祖佛      |
| [吴　健](../Page/吴健.md "wikilink")         | 乔灵儿                                | 乔家庄少爷，如来在人界的转世        |
| [黑　子](../Page/黑子_\(演员\).md "wikilink")  | 无　天                                | 紧那罗菩萨→摩罗→无天佛祖→摩罗      |
| [馬雅舒](../Page/馬雅舒.md "wikilink")        | 白莲花                                | 人界女土匪首领→白莲圣母菩萨，乔灵儿的妻子 |
| [涓　子](../Page/涓子.md "wikilink")         | 碧　游                                | 东华山仙女→护法禅菩萨，乔灵儿的恋人    |
| [秋　月](../Page/秋月.md "wikilink")         | [哪　吒](../Page/哪吒.md "wikilink")    | 三坛海会大神                |
| [马杰林](../Page/马杰林.md "wikilink")        | 黑　袍                                | 蛇精，无天部下               |
| [刘　莹](../Page/刘莹.md "wikilink")         | 巨　蝎                                | 蝎子精，无天部下              |
| [单联丽](../Page/单联丽.md "wikilink")        | 赢　妖                                | 无天部下                  |
| [于月仙](../Page/于月仙.md "wikilink")        | 五　真                                | 人界陈官镇的女鬼              |
| [郝一平](../Page/郝一平.md "wikilink")        | [如　来](../Page/如来佛祖.md "wikilink")  |                       |
| [傅羽佳](../Page/傅羽佳.md "wikilink")        | [观　音](../Page/觀世音菩薩.md "wikilink") |                       |
|                                         |                                    |                       |

## 职员表

  - 出品人：李安军
  - 制作人：李源、李蓁、王领、费云、容建林、李西京、高小毛、柴文志、王岱、梁石彦、杨健
  - 总监制：任贤良 延艺云
  - 监制：任贤良、延艺云、李耀武
  - 总协调：张国禄
  - 制作统筹：吴毅
  - 美工：毛怀清、马改娃、郭燕峰
  - 文学统筹：武元
  - 电脑特技：白小俊、曹荣
  - 电脑制作：宫谊凡、张小强、潜雪芬、李欣、刘利、邱锐、叶德勇、曾炯、范杰
  - 化妆设计：刘晓东、马淑英、王丹玲、赵华、王志光
  - 视音频技术：韩琛、张云明、王晓明
  - 头饰：蒋龙才
  - 烟火：郭岩
  - 动效：魏俊、华兰海
  - 财务统筹：李洁、杨民
  - 统筹助理：李斌

## 奖项

<table>
<thead>
<tr class="header">
<th><p>奖项</p></th>
<th><p>类别</p></th>
<th><p>名字</p></th>
<th><p>结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第18届<a href="../Page/中国电视金鹰奖.md" title="wikilink">中国电视金鹰奖</a></p></td>
<td><p>美术片类最佳奖</p></td>
<td><p>《西游记后传》</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 相关

曹荣在本剧中自导自演，在这之前他担任了《[西游记续集](../Page/西游记续集.md "wikilink")》的动作导演\[1\]。

本剧在南京、云南、江苏等地播出后获得了较高收视率，但在北京的收视并不是太好\[2\]。

2015年，编剧钱雁秋以本剧故事改编而成的电视剧《[石敢当之雄峙天东](../Page/石敢当之雄峙天东.md "wikilink")》播出。

## 评价

导演曹荣表示“打斗场数和特技量比补集增加一倍”\[3\]而实际上，电视剧中几乎所有的打斗部分都要连续重复播放三遍（包括正序，反序播放）以上，一些特写镜头还要从几种不同的拍摄角度来展示。这一点使观众普遍不满，但是2016年acfun上已经有纯剧情向无鬼畜版本出现\[4\]。整个剧中颠覆了西游记的理念，从顽劣不堪、任性调皮到正义勇敢、舍己为人的孙悟空，从丑陋异常到英俊潇洒的猪八戒，从佛学理论到领略了武学真谛的唐三藏,从胆小弱势到后来担负重担,勇猛冲锋的沙悟净。此剧的片头曲《我欲成仙》和片尾曲《相思》广受好评。

## 参考文献

## 外部链接

  -
[Category:2000年中國電視劇集](../Category/2000年中國電視劇集.md "wikilink")
[Category:西遊記題材電視劇](../Category/西遊記題材電視劇.md "wikilink")

1.

2.

3.
4.