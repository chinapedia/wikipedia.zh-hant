**黑山学院**（），是一所已结业的美国学校。

## 历史

1933年創立於[美國](../Page/美國.md "wikilink")[北卡羅來納州](../Page/北卡羅來納州.md "wikilink")[艾西維爾市附近](../Page/艾西維爾市.md "wikilink")，是美國一所以引領革新著名的學校。但在1957年結束校務。儘管只有約23個年頭和約近1,200名的學生，黑山大學過去在[藝術的教育與實踐上是最具虛構實驗性制度的](../Page/藝術.md "wikilink")，在60年代的美國造就了數位非凡的前衛派先鋒藝術家。該校以擁有在[視覺](../Page/視覺.md "wikilink")，[文學與](../Page/文學.md "wikilink")[表演藝術上非凡的教程而自豪](../Page/表演藝術.md "wikilink")，而該校所留下的更持續地影響著教育的[哲學或實踐](../Page/哲學.md "wikilink")。

[黑山学院](../Category/黑山学院.md "wikilink")
[Category:北卡羅來納州大學](../Category/北卡羅來納州大學.md "wikilink")
[Category:1933年創建的教育機構](../Category/1933年創建的教育機構.md "wikilink")
[Category:1957年廢除](../Category/1957年廢除.md "wikilink")
[Category:美國文理學院](../Category/美國文理學院.md "wikilink")
[Category:美國藝術學校](../Category/美國藝術學校.md "wikilink")