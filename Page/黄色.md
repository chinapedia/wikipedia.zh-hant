[缩略图](https://zh.wikipedia.org/wiki/File:Bananas.jpg "fig:缩略图")
**黃色**是由波長介於565至590[奈米的光線所形成的顔色](../Page/奈米.md "wikilink")，用色彩的[三原色](../Page/三原色.md "wikilink")[紅](../Page/紅色.md "wikilink")、[綠色光混合可產生黃光](../Page/綠色.md "wikilink")。亦為顏料的三原色之一。黃的[互補色是](../Page/互補色.md "wikilink")[藍](../Page/藍色.md "wikilink")。但傳統上畫師以[紫色作為黃的互補色](../Page/紫色.md "wikilink")。

## 用途和象徵意義

### 東方

  - [中國朝代从](../Page/中國.md "wikilink")[宋朝以后](../Page/宋朝.md "wikilink")，明黃色是[皇帝专用颜色](../Page/皇帝.md "wikilink")。
  - 黄色是類似貴重金屬[黄金的颜色](../Page/黄金.md "wikilink")，因此有财富的含义。
  - 在中国的[五行学说中](../Page/五行.md "wikilink")，黄色是[土的象征](../Page/五行.md "wikilink")，代表着[中央的位置](../Page/中.md "wikilink")。
  - 在[台灣](../Page/台灣.md "wikilink")，黃色在政治界是代表[新黨和](../Page/新黨.md "wikilink")[時代力量的顏色](../Page/時代力量.md "wikilink")、棒球圈是[中華職棒大聯盟](../Page/中華職棒大聯盟.md "wikilink")[中信兄弟的顏色](../Page/中信兄弟.md "wikilink")、[計程車也是用黃色作為統一塗裝](../Page/計程車.md "wikilink")，暱稱小黃。
  - 中国传统葬礼使用[白色為主色調](../Page/白色.md "wikilink")，搭配其它顏色，如黄色等。
  - 在[京剧脸谱代表凶猛](../Page/京剧.md "wikilink")、残暴的角色，如[典韦](../Page/典韦.md "wikilink")。
  - 在[泰国](../Page/泰国.md "wikilink")，黄色是[星期一的幸运色](../Page/星期一.md "wikilink")。

### 西方

  - 英語中黃色和妒忌、懦怯、敗壞有關聯。美俚稱呼膽小鬼做*yellow belly*；恣意煽情的傳媒為*yellow
    journalism*。
  - 在美国，通过发表未经仔细查证内容并以引人注目为题的报道被称为[黄色新闻](../Page/黄色新闻.md "wikilink")。
  - 由西方傳入華人地區，使用「黃色用語」來代表[色情](../Page/色情.md "wikilink")，如黃色雜誌、黃色笑話。（概念源自New
    York, according to Siu Yeuk Yuen）
  - 黄色还有注意、提醒等醒目的作用，比如[红绿燈就用黄色来提醒行人和车辆注意](../Page/红绿燈.md "wikilink")，[英國也規定所有](../Page/英國.md "wikilink")[鐵路車輛頭端若干面積需塗上黃色以收警示路人之效](../Page/鐵路車輛.md "wikilink")，[加拿大和](../Page/加拿大.md "wikilink")[美國的學童](../Page/美國.md "wikilink")[校車則規定使用黃色塗裝](../Page/校車.md "wikilink")。
  - 在美國若有人從危險的地方平安返鄉，會用黄絲帶來裝飾那個人的家。
  - 在[加拿大](../Page/加拿大.md "wikilink")，黃色代表了[民族联盟党的顏色](../Page/民族联盟党.md "wikilink")，其含义为[黄皮肤](../Page/黄肤.md "wikilink")[黑头发](../Page/黑发.md "wikilink")，象征着海外的[华人](../Page/华人.md "wikilink")。
  - [黃頁起源於英國的Yellow](../Page/黃頁.md "wikilink")
    Pages，指電話簿，載有商業機構的電話，分門別類。
  - [國際](../Page/國際.md "wikilink")[政治](../Page/政治.md "wikilink")[團體中](../Page/團體.md "wikilink")，黃色代表[自由主義者](../Page/自由主義.md "wikilink")。

### [科技](../Page/科技.md "wikilink")

  - 在[电阻值中代表](../Page/电阻.md "wikilink")[4](../Page/4.md "wikilink")。
  - 黃色外皮的通信用[光纖為](../Page/光纖.md "wikilink")[單模光纖](../Page/單模光纖.md "wikilink")，以與[橙色外皮的](../Page/橙色.md "wikilink")[多模光纖區分](../Page/多模光纖.md "wikilink")。

#### 颜色代码

  - 網絡編碼 = \#FFFF00
  - [RGB紅綠藍光系統](../Page/RGB.md "wikilink") = （紅255， 綠255， 藍0）
  - [CMYK四色印刷系統](../Page/CMYK.md "wikilink") = （青0， 品紅0， 黃100， 黑0）
  - [HSV系統](../Page/HSV色彩属性模式.md "wikilink") = （原色60， 飽和度100， 亮度100）

## 註釋

在某些黄色的服装及装饰上（比如教宗本笃十六世的某件法衣）使用了金色\[RGB(255, 215,
0)\]来代替。可能原因是，黄色\[RGB（255, 255,
0)\]相对金色显刺眼，金色更加厚重。由此推测，中国古代的“正黄色”就几乎可以等同于金色。

## 相關條目

  - [加德納色度](../Page/加德納色度.md "wikilink")：黃色深淺的度量
  - [可見光](../Page/可見光.md "wikilink")

## 參考資料

[Y](../Category/颜色.md "wikilink") [\*](../Category/黃色系.md "wikilink")
[Y](../Category/色彩系.md "wikilink") [Y](../Category/光谱.md "wikilink")
[Category:网页颜色](../Category/网页颜色.md "wikilink")