**GAINAX**是一間[日本動畫製作公司](../Page/日本動畫.md "wikilink")，於1984年12月24日成立。代表作有《[王立宇宙軍](../Page/王立宇宙軍.md "wikilink")》《[飛越巔峰](../Page/飛越巔峰.md "wikilink")》《[新世纪福音战士](../Page/新世纪福音战士.md "wikilink")》《[天元突破
紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")》等。

GAINAX以顛覆傳統的動畫類型和實驗動畫而聞名，另外因主要成員的興趣而加入大量的[戲仿或](../Page/戲仿.md "wikilink")[致敬](../Page/致敬.md "wikilink")，曾因此被人稱為「[御宅公司](../Page/御宅族.md "wikilink")」。此外，GAINAX亦讓部分動畫師嘗試多個職位，動畫師同時染指劇本、分鏡和作畫監督的情況時常出現。而在傳統之下，各成員都著重手繪動畫方面的能力。

## 歷史

### 同人時代 (DAICON FILM)

1981年，[武田康廣与](../Page/武田康廣.md "wikilink")[岡田斗司夫当时在第](../Page/岡田斗司夫.md "wikilink")20届[日本SF大会](../Page/日本科幻大會.md "wikilink")(DAICON
III)担任企划，邀请[庵野秀明](../Page/庵野秀明.md "wikilink")、[山賀博之](../Page/山賀博之.md "wikilink")、[赤井孝美三人制作大会的开幕动画](../Page/赤井孝美.md "wikilink")，结果三人做出了一部融合SF、特摄、美少女要素的精彩动画短片，大受欢迎\[1\]。《DAICON
III OPENING
ANIMATION》完成后，参加的成员成立了[自主制作团体](../Page/獨立電影.md "wikilink")[DAICON
FILM](../Page/DAICON_FILM.md "wikilink")\[2\]。

1982年，岡田与武田在大阪开设了一间SF周边专卖店：。同时筹备第22届SF大会(DAICON IV)的开幕动画。在这期间与DAICON
FILM制作了三部[特摄片](../Page/特摄片.md "wikilink")《》《》《[DAICON FILM版
归来的奥特曼](../Page/DAICON_FILM版_归来的奥特曼.md "wikilink")》。前两者于1982年8月第21届SF大会(TOCON
VIII)上映，而後者到1983年3月才完成。

为了习得专业的动画技术，山賀博之、庵野秀明赴东京参加《[超时空要塞](../Page/超时空要塞.md "wikilink")》的制作。并拉来了赤井孝美的高中学弟[前田真宏](../Page/前田真宏.md "wikilink")、前田真宏的大学学长[貞本義行等人](../Page/貞本義行.md "wikilink")。随后一同制作DAICON
IV的开幕动画《DAICON IV OPENING ANIMATION》。\[3\]

1984年，1月完成《快杰能天气2》。8月开始拍摄首部原创特摄片《八岐之大蛇的逆襲》，于1985年12月完成。1984年9月开始企画《[王立宇宙軍](../Page/王立宇宙軍.md "wikilink")》。

1985年，General Products主办了第一届[Wonder
Festival](../Page/Wonder_Festival.md "wikilink")。

### GAINAX的设立与发展

为了制作《王立宇宙軍》，於1984年12月设立株式会社GAINAX。創立時的成員包括[岡田斗司夫](../Page/岡田斗司夫.md "wikilink")、[武田康廣](../Page/武田康廣.md "wikilink")、[山賀博之](../Page/山賀博之.md "wikilink")、[庵野秀明](../Page/庵野秀明.md "wikilink")、[貞本義行](../Page/貞本義行.md "wikilink")、[赤井孝美](../Page/赤井孝美.md "wikilink")、[樋口真嗣](../Page/樋口真嗣.md "wikilink")、[村濱章司等](../Page/村濱章司.md "wikilink")。

1985年4月《王立宇宙軍》样片完成，获得[BANDAI投资](../Page/BANDAI.md "wikilink")，决定制作成长篇动画电影。1986年1月工作室转移至东京[吉祥寺
(武藏野市)](../Page/吉祥寺_\(武藏野市\).md "wikilink")，开始制作长篇[动画电影](../Page/动画电影.md "wikilink")《[王立宇宙軍～歐尼亞米斯之翼～](../Page/王立宇宙軍～歐尼亞米斯之翼～.md "wikilink")》([山贺博之导演](../Page/山贺博之.md "wikilink"))，于1987年3月公映。《王立宇宙軍》票房大失败，原先予定制作完便解散的GAINAX为弥补赤字而企画了[OVA动画](../Page/OVA.md "wikilink")《[飛越巔峰](../Page/飛越巔峰.md "wikilink")》([庵野秀明导演](../Page/庵野秀明.md "wikilink"))。《飛越巔峰》虽然叫好叫座，但由于过大投入而赤字未减。接着制作了[NHK](../Page/NHK.md "wikilink")[电视动画](../Page/电视动画.md "wikilink")《[冒險少女娜汀亞](../Page/冒險少女娜汀亞.md "wikilink")》，依然叫好叫座。

GAINAX亦制作[电脑游戏](../Page/电脑游戏.md "wikilink")：1989年发售猜谜游戏《電腦学園》，1991年发售[养成游戏](../Page/养成游戏.md "wikilink")《[美少女夢工場](../Page/美少女夢工場.md "wikilink")》。

1992年2月，General Products併入GAINAX，专注于动画制作和电脑游戏制作两大类别，并将Wonder
Festival的主办权移交给了[海洋堂](../Page/海洋堂.md "wikilink")。同年，部分創立成員村濱章司、[前田真宏](../Page/前田真宏.md "wikilink")、[山口宏和](../Page/山口宏.md "wikilink")[樋口真嗣帶了部分人離開GAINAX](../Page/樋口真嗣.md "wikilink")，創立[GONZO](../Page/GONZO.md "wikilink")，只保留[部頭合約關係](../Page/部頭合約.md "wikilink")。GAINAX社長[岡田斗司夫退社](../Page/岡田斗司夫.md "wikilink")，[澤村武伺接任社長](../Page/澤村武伺.md "wikilink")。1991年发售的OVA《》带有点自传色彩。

1995年，沉寂4年后推出《[新世纪福音战士](../Page/新世纪福音战士.md "wikilink")》，引來廣泛的社會效應，之后推出[剧场版](../Page/新世紀福音戰士劇場版.md "wikilink")。1998年推出首部漫画改编作品《[男女蹺蹺板](../Page/男女蹺蹺板.md "wikilink")》，此后首席导演庵野秀明转向真人电影。

1999年，澤村武伺因漏税问题辞任社長，由山賀博之補上。

### 新世紀 & 次世代

步入新世紀，推出的第一部作品是《[FLCL](../Page/FLCL.md "wikilink")》([鶴卷和哉导演](../Page/鶴卷和哉.md "wikilink"))。[山贺博之时隔十数年再次导演作品](../Page/山贺博之.md "wikilink")《[魔力女管家](../Page/魔力女管家.md "wikilink")》《[阿倍野桥魔法商店街](../Page/阿倍野桥魔法商店街.md "wikilink")》。

2004年，推出20周年纪念作品《[飛越巔峰2](../Page/飛越巔峰2.md "wikilink")》《[這醜陋又美麗的世界](../Page/醜陋美世界.md "wikilink")》。

2006年，庵野秀明成立[khara工作室制作](../Page/khara工作室.md "wikilink")《[福音戰士新劇場版](../Page/福音戰士新劇場版.md "wikilink")》，鶴卷和哉、[摩砂雪等一批人员移籍khara](../Page/摩砂雪.md "wikilink")。

2007年，终于制作出次世代作品《[天元突破
紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")》，形成以[今石洋之为中心的主创团队](../Page/今石洋之.md "wikilink")，2010年推出《[吊带袜天使](../Page/吊带袜天使.md "wikilink")》。

2011年，GAINAX總部由[小金井市轉移至](../Page/小金井市.md "wikilink")[三鷹市下連雀](../Page/三鷹市.md "wikilink")。同年8月，GAINAX再一次出現較大規模的成員離開，曾主力創作《[天元突破
紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")》的[今石洋之和](../Page/今石洋之.md "wikilink")[大塚雅彦等人宣佈獨立](../Page/大塚雅彦.md "wikilink")，創立[TRIGGER](../Page/TRIGGER.md "wikilink")。以及部分人员如：[吉成曜](../Page/吉成曜.md "wikilink")、芳垣裕介、[石崎寿夫等移籍TRIGGER](../Page/石崎寿夫.md "wikilink")；村田康人、井関修一移籍khara；同時期退社的还有：[錦織敦史](../Page/錦織敦史.md "wikilink")、山口智、上村泰、[貞方希久子等](../Page/貞方希久子.md "wikilink")。

### 之后的发展

2015年，推出《[放學後的昴星團](../Page/放學後的昴星團.md "wikilink")》。

## 作品

### 電視動畫

  - （制作協力: [AIC](../Page/動畫國際公司.md "wikilink")，1989年）

  - [冒險少女娜汀亞](../Page/冒險少女娜汀亞.md "wikilink") （共同制作: [Group
    Tac](../Page/Group_Tac.md "wikilink")，1990年）

  - [新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")（共同制作:
    [龍之子](../Page/龍之子.md "wikilink")，1995年）

  - [男女蹺蹺板](../Page/男女蹺蹺板.md "wikilink")（共同制作:
    [J.C.STAFF](../Page/J.C.STAFF.md "wikilink")，1998年）

  - 系列（企劃，1999年）动画制作: Group Tac

      - [看家鼠Ebichu](../Page/看家鼠.md "wikilink")

      -
      -
  - [魔力女管家系列](../Page/魔力女管家.md "wikilink")（共同制作:
    [SHAFT](../Page/SHAFT.md "wikilink")）

      - 魔力女管家（2001年）
      - 魔力女管家 ～更美麗的事物～（2002年）
      - 魔力女管家特別篇「我認為好色是不對的！」（2003年）
      - 魔力女管家特別篇 我回來了◆歡迎回來（2009年）

  - [阿倍野桥魔法商店街](../Page/阿倍野桥魔法商店街.md "wikilink")（共同制作:
    [MADHOUSE](../Page/MADHOUSE.md "wikilink")，2002年）

  - [超齡細公主](../Page/超齡細公主.md "wikilink")（共同制作:
    [AIC](../Page/動畫國際公司.md "wikilink")，2002年）

  - [忘卻的旋律](../Page/忘卻的旋律.md "wikilink")（企劃，2004年）动画制作: J.C.STAFF

  - [這醜陋又美麗的世界](../Page/醜陋美世界.md "wikilink")（共同制作: SHAFT，2004年）

  - [我的主人愛作怪](../Page/我的主人.md "wikilink")（共同制作: SHAFT，2005年）

  - [天元突破 紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")（2007年）

  - [屍姬系列](../Page/屍姬.md "wikilink")（共同制作:
    [feel.](../Page/feel..md "wikilink")）

      - 屍姫 赫（2008年）
      - 屍姫 玄（2009年）

  - [花丸幼稚園](../Page/花丸幼稚園.md "wikilink")（2010年）

  - [吊帶襪天使](../Page/吊帶襪天使.md "wikilink")（2010年）

  - [丹特麗安的書架](../Page/丹特麗安的書架.md "wikilink")（2011年）

  - [最強學生會長](../Page/最強學生會長.md "wikilink")（2012年）

  - [特例措施团体斯特拉女子学院高等科C3部](../Page/特例措施团体斯特拉女子学院高等科C3部.md "wikilink")（2013年）

  - [魔法少女大战](../Page/魔法少女大战.md "wikilink")（2014年）

  - [放学后的昴星团](../Page/放学后的昴星团.md "wikilink") （2015年）

  - [琴之森](../Page/琴之森.md "wikilink")（2018年）

### OVA

  - [飛越巔峰](../Page/飛越巔峰.md "wikilink")（制作協力:
    [Fantasia](../Page/Studio_Fantasia.md "wikilink")，1988年）

  - [爆炎轉校生](../Page/爆炎轉校生.md "wikilink")（制作協力: Fantasia，1991年）

  - 系列（制作協力: Fantasia，1991年）

  - [FLCL](../Page/FLCL.md "wikilink")（共同制作: [Production
    I.G](../Page/Production_I.G.md "wikilink")，2000年）

  - [Re: 甜心戰士](../Page/Re:_甜心戰士.md "wikilink")（共同制作:
    [東映動畫](../Page/東映動畫.md "wikilink")，2004年）

  - [飛越巔峰2](../Page/飛越巔峰2.md "wikilink")（2004年）

### 劇場動畫

  - [王立宇宙軍](../Page/王立宇宙軍.md "wikilink")（1987年）
      - 蒼之URU（，未知）
  - [新世紀福音戰士劇場版](../Page/新世紀福音戰士劇場版.md "wikilink")（共同制作: [Production
    I.G](../Page/Production_I.G.md "wikilink")，1997年）
      - [新世紀福音戰士劇場版：死與新生](../Page/新世紀福音戰士劇場版：死與新生.md "wikilink")
      - [新世紀福音戰士劇場版：THE END OF
        EVANGELION](../Page/新世紀福音戰士劇場版：THE_END_OF_EVANGELION.md "wikilink")
  - 飛越巔峰&飛越巔峰2 合體劇場版（2006年）
  - [天元突破 紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink") 劇場版
      - 劇場版 天元突破 紅蓮螺巖 紅蓮篇（2008年）
      - 劇場版 天元突破 紅蓮螺巖 螺巖篇（2009年）

### 游戏软件

  - [电脑学园](../Page/电脑学园.md "wikilink")（1989年）
  - [美少女夢工場系列](../Page/美少女夢工場.md "wikilink")（1991年）
  - [新世纪福音战士相关](../Page/新世纪福音战士.md "wikilink")
      - [新世紀福音戰士：鋼鐵戀人](../Page/新世紀福音戰士：鋼鐵戀人.md "wikilink")（1997年）
      - [EVA和愉快的伙伴们系列](../Page/EVA和愉快的伙伴们.md "wikilink")（1998年）
      - [新世纪福音战士：绫波育成计划](../Page/新世纪福音战士：绫波育成计划.md "wikilink")（2001年）
      - [新世紀福音戰士：鋼鐵戀人2nd](../Page/新世紀福音戰士：鋼鐵戀人2nd.md "wikilink")（2003年）
      - [新世紀福音戰士：碇真嗣育成計畫](../Page/新世紀福音戰士：碇真嗣育成計畫.md "wikilink")（2004年）

## 参考

## 關連項目

  - [御宅族](../Page/御宅族.md "wikilink")
  - [电车男](../Page/电车男.md "wikilink")
  - [日本動畫工作室列表](../Page/日本動畫工作室列表.md "wikilink")

## 外部連結

  - [GAINAX NET](http://www.gainax.co.jp/) - GAINAX 官方網頁（日文）
  - [Gainax Network
    Systems](https://web.archive.org/web/19961018215821/http://www.gainax.co.jp/menu-e.html)
    - GAINAX 官方網頁（英文）

[Category:1984年成立的公司](../Category/1984年成立的公司.md "wikilink")
[\*](../Category/GAINAX.md "wikilink")
[Category:日本動畫工作室](../Category/日本動畫工作室.md "wikilink")
[Category:東京都公司](../Category/東京都公司.md "wikilink")
[Category:三鷹市公司](../Category/三鷹市公司.md "wikilink")

1.

2.

3.