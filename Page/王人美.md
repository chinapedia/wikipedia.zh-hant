**王人美**（），暱称“**小野猫**”，[中国电影](../Page/中国电影.md "wikilink")[演員和](../Page/演員.md "wikilink")[歌手](../Page/歌手.md "wikilink")，其电影演出生涯活跃期於1930年代，當中最為知名的演出在[蔡楚生执导的](../Page/蔡楚生.md "wikilink")《[渔光曲](../Page/渔光曲.md "wikilink")》，此為中国第一部在国际上获奖的电影。
2005年，王人美入选[中国电影百年百位优秀演员](../Page/中国电影百年百位优秀演员.md "wikilink")。

## 生平

[王人美1926年.jpg](https://zh.wikipedia.org/wiki/File:王人美1926年.jpg "fig:王人美1926年.jpg")

### 幼年

王人美原籍湖南浏阳，在长沙出生并长大\[1\]。父亲王正枢，是[长沙的](../Page/长沙.md "wikilink")[湖南省立第一师范学校的](../Page/湖南省立第一师范学校.md "wikilink")[数学教员](../Page/数学.md "wikilink")，[毛泽东也是其学生](../Page/毛泽东.md "wikilink")，并且当时王家与毛泽东交往密切。王人美原名王庶熙，兄弟姐妹共七人，王人美最小。7岁时，母亲因患[脑溢血而突然病逝](../Page/脑溢血.md "wikilink")，当时王人美已入小学。1926年，王人美考入[湖南省立第一女子师范学校](../Page/湖南省立第一女子师范学校.md "wikilink")，对数学非常感兴趣，理想是以后成为父亲那样的数学教员。同年9月19日，王人美的父亲被[黄蜂螫咬](../Page/黄蜂.md "wikilink")，化脓后病逝。\[2\]

父亲病逝后，王人美随哥哥、姐姐来到[武汉](../Page/武汉.md "wikilink")，哥哥、姐姐都在[汪精卫的](../Page/汪精卫.md "wikilink")[武汉国民政府机关任职](../Page/武汉国民政府.md "wikilink")。1927年，汪精卫的武汉国民政府与[蒋介石南京国民政府](../Page/蒋介石.md "wikilink")[宁汉合流](../Page/宁汉合流.md "wikilink")。王人美哥哥和姐姐是左翼青年，被迫逃离武汉，全家星散。王人美随二哥王人路和三哥王人艺逃到无锡她的二嫂的娘家。\[3\]

### 上海学艺

[Wang_Renmei_and_Bright_Moon_Troupe.jpg](https://zh.wikipedia.org/wiki/File:Wang_Renmei_and_Bright_Moon_Troupe.jpg "fig:Wang_Renmei_and_Bright_Moon_Troupe.jpg")
1928年初，王人美的二哥王人路将王人美和她的三哥王人艺带到上海，王人美和王人艺入[黎锦晖创办的上海美美女校](../Page/黎锦晖.md "wikilink")，王人美学习歌舞，王人艺学习曼陀林琴。
\[4\]黎锦晖曾与王人路是同事，他是中国流行音乐之父。黎锦晖认为王庶熙这个名字太过稳重，不适合当艺名，便给她起了另一个名字——王人美。取这个名字也是为将她列入王家的“人”字辈，以打破女性不入家族辈分的中国传统旧习俗\[5\]。1928年5月，黎锦晖组织中华歌舞团赴南洋演出，在十个月时间内，先后到[新加坡](../Page/新加坡.md "wikilink")、[吉隆坡](../Page/吉隆坡.md "wikilink")、[槟榔屿](../Page/槟榔屿.md "wikilink")、[曼谷](../Page/曼谷.md "wikilink")、[马六甲](../Page/马六甲.md "wikilink")、[雅加达](../Page/雅加达.md "wikilink")、[苏门答腊等城市演出](../Page/苏门答腊.md "wikilink")。王人美作为配角随团参与了全程演出。1929年，中华歌舞团解散，王人美回到上海，入南洋招商附属英文专科学校学习英语一年。王人美在英专组织排演了黎锦晖创作的儿童歌舞剧《[小小画家](../Page/小小画家.md "wikilink")》，取得很大成功，被学校聘为歌唱教员，免学费，并发放薪水。\[6\]1929年冬，黎锦晖组织[明月歌舞团](../Page/明月歌舞团.md "wikilink")。次年春赴[北平](../Page/北平.md "wikilink")、[天津演出](../Page/天津.md "wikilink")，随后又赴[中国东北](../Page/中国东北.md "wikilink")。王人美在演出中开始演主角，并表现卓越，与[黎莉莉](../Page/黎莉莉.md "wikilink")、[薛玲仙](../Page/薛玲仙.md "wikilink")、[胡笳被称为明月歌舞团的](../Page/胡笳.md "wikilink")“四大天王”。\[7\]

### 明星之路

[Wang_Renmei2.jpg](https://zh.wikipedia.org/wiki/File:Wang_Renmei2.jpg "fig:Wang_Renmei2.jpg")张贴的王人美海报\]\]
1931年，明月歌舞團併入[联华影业](../Page/联华影业.md "wikilink")，王人美成为聯華影業的电影演员。1932年，王人美在[孙瑜编导的电影](../Page/孙瑜_\(导演\).md "wikilink")《野玫瑰》中担任主角。\[8\]王人美在片中与当时的明星金焰演对手戏，她的表演收到影评家和观众的一致好评，被赞为亚洲好莱坞新星。\[9\]

1934年，在[蔡楚生导演的电影](../Page/蔡楚生.md "wikilink")《[渔光曲](../Page/渔光曲.md "wikilink")》中，王人美出演主角“小猫”，并演唱电影主题歌。王人美获得极大成功，奠定其在电影界的地位。《渔光曲》在上海创下连演84天的记录，吸引了近百万人走入电影院。\[10\]《渔光曲》在1935年获苏联莫斯科国际电影节“荣誉奖”，成为我国第一部在国际上获奖的影片。王人美拍摄《渔光曲》后“小猫”的名字深入人心，于是人们往后就亲切地叫她“野猫”。\[11\]

### 嫁给金焰

[金焰和王人美.jpg](https://zh.wikipedia.org/wiki/File:金焰和王人美.jpg "fig:金焰和王人美.jpg")
[王人美和她的二哥三哥.jpg](https://zh.wikipedia.org/wiki/File:王人美和她的二哥三哥.jpg "fig:王人美和她的二哥三哥.jpg")

1934年1月1日，在《渔光曲》拍摄过程中，王人美突然宣布他与金焰的婚讯。金焰是王人美主演的《野玫瑰》的男主角，是当时的中国影帝\[12\]。尽管《渔光曲》取得了极大成功，联华影业公司没有与王人美续约，因为公司认为，已婚的女演员会失去对男观众的吸引力\[13\]。1935年，王人美入[电通影片公司](../Page/电通影片公司.md "wikilink")，主演了《[风云儿女](../Page/风云儿女.md "wikilink")》，《[义勇军进行曲](../Page/义勇军进行曲.md "wikilink")》即出自这部电影\[14\]。但王人美自《渔光曲》之后，电影事业走向下坡路，远没有金焰成功\[15\]。

1937年[抗日战争爆发](../Page/抗日战争.md "wikilink")，日军占领上海后，日方软硬兼施，要求金焰为“日中合作”拍片，金焰强硬拒绝，在上海无法生活下去。1938年秋天，在朋友的帮助下，王人美和金焰逃往香港\[16\]。1939年，导演[孙瑜邀请金焰和王人美出演爱国抗日电影](../Page/孙瑜_\(导演\).md "wikilink")《长空万里》。这是中国第一部空战影片他们欣然接受邀请，来到昆明拍片。1940年底又回到香港。[香港沦陷后](../Page/香港保卫战.md "wikilink")，王人美和金焰逃出香港，随难民一路步行来到[桂林](../Page/桂林.md "wikilink")。随后，夫妇二人先后到了[重庆](../Page/重庆.md "wikilink")。1943年年底，金焰去了[成都](../Page/成都.md "wikilink")。1944年春，王人美到了[昆明](../Page/昆明.md "wikilink")，参加了大鹏剧社，演出了话剧《[孔雀胆](../Page/孔雀胆.md "wikilink")》，饰演主角阿盖公主，轰动了昆明。不久，大鹏剧社解散，王人美为生计，考入美军物资供应处昆明基地，做打字员。王人美与金焰经常长期两地分居，金焰不同意王人美出去工作，最终于1945年离婚，但离婚后，二人保持着朋友关系。\[17\]\[18\]

### 抗日战争以后和中华人民共和国时期

抗日战争胜利后，王人美回到上海。由于不满国民党政府严格的审查制度，王人美再次离开上海，去往香港。\[19\]

1949年，中华人民共和国成立。1949年12月，王人美随港澳电影界观光团回[广州参观](../Page/广州.md "wikilink")，受到广州市市长[叶剑英](../Page/叶剑英.md "wikilink")、司令员[陈赓设宴款待](../Page/陈赓.md "wikilink")\[20\]。1950年，王人美满怀热情地回到上海。当时共产党实行的政策受到电影界的欢迎，很多电影人留在或从海外返回上海。\[21\]
共产党和文艺界的蜜月期很快结束。1952年，在电影界文艺整风时，王人美被人诬蔑为曾和[军统首脑](../Page/军统.md "wikilink")[戴笠有来往](../Page/戴笠.md "wikilink")，王人美受到极大刺激，被送入疯人院，经二姐王明霞将其接到北京，住在大哥王人璇家，神经逐渐松弛。病情康复后，王人美被调入[北京电影制片厂工作](../Page/北京电影制片厂.md "wikilink")。\[22\]\[23\]
[Wang_Renmei_and_Ye_Qianyu.jpg](https://zh.wikipedia.org/wiki/File:Wang_Renmei_and_Ye_Qianyu.jpg "fig:Wang_Renmei_and_Ye_Qianyu.jpg")

1955年，在朋友的撮合下，王人美与画家叶浅予结为夫妻。他们婚后经常吵架，但婚姻一直维持，直到去世。王人美称“叶浅予是个好画家，却不是一个好丈夫，他除了懂画，别的什么也不懂。”在1957年的[反右运动中](../Page/反右运动.md "wikilink")，王人美再度受到刺激，随后病情复发，被丈夫[叶浅予送入精神病医院治疗](../Page/叶浅予.md "wikilink")，尽管如此，她仍然多次写入党申请书。\[24\]
[文化大革命时期](../Page/文化大革命.md "wikilink")，王人美被下放到干校劳动，但由于她的家庭与毛泽东的特殊关系，受到迫害较少\[25\]
。叶浅予被被诬为“国民党中将特务”，被关进监狱七年\[26\]
。文化大革命结束后，1979年，王人美获得平反，并获准加入[中国共产党](../Page/中国共产党.md "wikilink")。王人美当选为第五、六届[全国政协委员](../Page/全国政协委员.md "wikilink")、[中国电影家协会名誉理事](../Page/中国电影家协会.md "wikilink")\[27\]\[28\]
。

1980年，王人美[中风](../Page/中风.md "wikilink")，瘫痪在床。1986年12月，再次中风，成为[植物人](../Page/植物人.md "wikilink")。1987年4月12日，王人美在[北京病逝](../Page/北京.md "wikilink")，享年73岁。1987年4月23日，王人美的遗体在[八宝山殡仪馆火化](../Page/八宝山殡仪馆.md "wikilink")。\[29\]\[30\]

## 身后事

王人美在晚年出版了自己口述、解波整理的回忆录，名为《我的成名与不幸》。\[31\]

2005年，王人美被[中国电影表演艺术学会选为](../Page/中国电影表演艺术学会.md "wikilink")[中国电影百年百位优秀演员](../Page/中国电影百年百位优秀演员.md "wikilink")。\[32\]

2013年，[西雅图大学的理查德](../Page/西雅图大学.md "wikilink")·迈耶（Richard J.
Meyer）撰写的王人美的传记《王人美：上海野猫》（*Wang Renmei: The Wildcat of
Shanghai*）由[香港中文大学出版社出版](../Page/香港中文大学.md "wikilink")\[33\]

## 参演作品

[1930年代的王人美.jpg](https://zh.wikipedia.org/wiki/File:1930年代的王人美.jpg "fig:1930年代的王人美.jpg")
[《风云儿女》海报.jpg](https://zh.wikipedia.org/wiki/File:《风云儿女》海报.jpg "fig:《风云儿女》海报.jpg")
[中国1930年代女演员王人美.jpg](https://zh.wikipedia.org/wiki/File:中国1930年代女演员王人美.jpg "fig:中国1930年代女演员王人美.jpg")
[王人美.jpg](https://zh.wikipedia.org/wiki/File:王人美.jpg "fig:王人美.jpg")
[金焰王人美结婚照.jpg](https://zh.wikipedia.org/wiki/File:金焰王人美结婚照.jpg "fig:金焰王人美结婚照.jpg")
[王人美与叶浅予1956年.jpg](https://zh.wikipedia.org/wiki/File:王人美与叶浅予1956年.jpg "fig:王人美与叶浅予1956年.jpg")
[Sons_and_Daughters_in_a_Time_of_Storm.ogv](https://zh.wikipedia.org/wiki/File:Sons_and_Daughters_in_a_Time_of_Storm.ogv "fig:Sons_and_Daughters_in_a_Time_of_Storm.ogv")》片断\]\]
根据王人美的回忆录《我的成名与不幸》，整理了她参演的电影和话剧。\[34\]

### 电影

|                                                |                                                               |                                                                 |                                                                                                                                                                      |                                                                                                                                                                                               |
| ---------------------------------------------- | ------------------------------------------------------------- | --------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **拍摄年份**                                       | **中文片名**                                                      | **導演**                                                          | **角色**                                                                                                                                                               | **合作演員 / 備註**                                                                                                                                                                                 |
| 1931                                           | [野玫瑰](../Page/野玫瑰_\(电影\).md "wikilink")                       | [孙瑜](../Page/孙瑜_\(导演\).md "wikilink")                           | 女主角小凤                                                                                                                                                                | [金焰](../Page/金焰.md "wikilink")、[叶娟娟](../Page/叶娟娟.md "wikilink")、[郑君里](../Page/郑君里.md "wikilink")、[韩兰根](../Page/韩兰根.md "wikilink")、[章志直](../Page/章志直.md "wikilink")                            |
| 1932                                           | [芭蕉叶上诗](../Page/芭蕉叶上诗.md "wikilink")                          |                                                                 |                                                                                                                                                                      | 中国第一部歌舞片                                                                                                                                                                                      |
| [共赴国难](../Page/共赴国难.md "wikilink")             |                                                               | 配角救护队员                                                          |                                                                                                                                                                      |                                                                                                                                                                                               |
| [都会的早晨](../Page/都会的早晨.md "wikilink")           | [蔡楚生](../Page/蔡楚生.md "wikilink")                              | 女主角兰儿                                                           | [高占非](../Page/高占非.md "wikilink")、[袁丛美](../Page/袁丛美.md "wikilink")、[王桂林](../Page/王桂林.md "wikilink")、[唐槐秋](../Page/唐槐秋.md "wikilink")、[汤天绣](../Page/汤天绣.md "wikilink") |                                                                                                                                                                                               |
| [春潮](../Page/春潮.md "wikilink")                 | [郑应时](../Page/郑应时.md "wikilink")                              | 女主角严玉瑛                                                          | 高占非、袁丛美、[李丽](../Page/李丽.md "wikilink")、[尚冠武](../Page/尚冠武.md "wikilink")                                                                                              |                                                                                                                                                                                               |
| 1933                                           | [渔光曲](../Page/渔光曲.md "wikilink")                              | 蔡楚生                                                             | 女主角小猫                                                                                                                                                                | [罗朋](../Page/罗朋.md "wikilink")、袁丛美、[韩兰根](../Page/韩兰根.md "wikilink")、汤天绣                                                                                                                       |
| 1934                                           | 小天使                                                           | [吴永刚](../Page/吴永刚.md "wikilink")                                | 重要角色姐姐                                                                                                                                                               | [葛佐志](../Page/葛佐志.md "wikilink")、[林楚楚](../Page/林楚楚.md "wikilink")、[刘琼](../Page/刘琼.md "wikilink")                                                                                              |
| 1935                                           | [风云儿女](../Page/风云儿女.md "wikilink")                            | [许幸之](../Page/许幸之.md "wikilink")                                | 女主角阿凤                                                                                                                                                                | [袁牧之](../Page/袁牧之.md "wikilink")、[周璇](../Page/周璇.md "wikilink")、[谈瑛](../Page/谈瑛.md "wikilink")、[顾梦鹤](../Page/顾梦鹤.md "wikilink")、[陆露明](../Page/陆露明.md "wikilink")                              |
| 1936                                           | 长恨歌                                                           | [史东山](../Page/史东山.md "wikilink")                                | 女主角马妮娜                                                                                                                                                               | [梅熹](../Page/梅熹.md "wikilink")、[金山](../Page/金山_\(演員\).md "wikilink")、[洪警铃](../Page/洪警铃.md "wikilink")                                                                                         |
| [壮志凌云](../Page/壯志凌雲_\(1936年電影\).md "wikilink") | 吴永刚                                                           | 女主角黑妞                                                           | 金焰、[田方](../Page/田方.md "wikilink")、[王次龙](../Page/王次龙.md "wikilink")、章志直、[金仑](../Page/金仑.md "wikilink")、[陈娟娟](../Page/陈娟娟.md "wikilink")                               |                                                                                                                                                                                               |
| 1937                                           | [黄海大盗](../Page/黄海大盗.md "wikilink")                            | 吴永刚                                                             | 女主角                                                                                                                                                                  | 高占非、田方、章志直、[许曼丽](../Page/许曼丽.md "wikilink")、顾梦鹤、[陈天国](../Page/陈天国.md "wikilink")                                                                                                              |
| 1938                                           | [离恨天](../Page/离恨天.md "wikilink")                              | 吴永刚                                                             | 歌舞母女                                                                                                                                                                 | 刘琼、[殷秀岑](../Page/殷秀岑.md "wikilink")、韩兰根                                                                                                                                                       |
| 1939                                           | [长空万里](../Page/长空万里.md "wikilink")                            | 吴永刚                                                             | 配角燕秀                                                                                                                                                                 | 金焰、高占非、[魏鹤龄](../Page/魏鹤龄.md "wikilink")、[白杨](../Page/白杨_\(演员\).md "wikilink")                                                                                                                 |
| 1947                                           | [关不住的春光](../Page/关不住的春光.md "wikilink")                        | [王为一](../Page/王为一.md "wikilink")、[徐韬](../Page/徐韬.md "wikilink") | 女主角梅春丽                                                                                                                                                               | [赵丹](../Page/赵丹.md "wikilink")、[凤子](../Page/凤子.md "wikilink")、[苏绘](../Page/苏绘.md "wikilink")、[王苹](../Page/王苹.md "wikilink")、[中叔皇](../Page/中叔皇.md "wikilink")、[张雁](../Page/张雁.md "wikilink")   |
| 1949                                           | [王氏四侠](../Page/王氏四侠.md "wikilink")                            | [王次龙](../Page/王次龙.md "wikilink")                                | 四侠之一                                                                                                                                                                 | [王元龙](../Page/王元龙.md "wikilink")、[王引](../Page/王引.md "wikilink")、[王丹凤](../Page/王丹凤.md "wikilink")                                                                                              |
| 1950                                           | [两家春](../Page/两家春.md "wikilink")                              | [瞿白音](../Page/瞿白音.md "wikilink")                                | 配角灵巧                                                                                                                                                                 | [秦怡](../Page/秦怡.md "wikilink")、[王龙基](../Page/王龙基.md "wikilink")、[高博](../Page/高博.md "wikilink")、[戴耘](../Page/戴耘.md "wikilink")、[钱千里](../Page/钱千里.md "wikilink")、[蒋锐](../Page/蒋锐.md "wikilink") |
| 1954                                           | [猛河的黎明](../Page/猛河的黎明.md "wikilink")                          | [鲁韧](../Page/鲁韧.md "wikilink")、[朱丹西](../Page/朱丹西.md "wikilink") | 配角苏虹                                                                                                                                                                 | [刘莲池](../Page/刘莲池.md "wikilink")、[王苏娅](../Page/王苏娅.md "wikilink")、[袁玫](../Page/袁玫.md "wikilink")、[陈颖](../Page/陈颖.md "wikilink")                                                               |
| 1957                                           | [青春的脚步](../Page/青春的脚步.md "wikilink")                          | [严恭](../Page/严恭.md "wikilink")、[苏里](../Page/苏里.md "wikilink")   | 配角淑芳                                                                                                                                                                 | [刘增庆](../Page/刘增庆.md "wikilink")、[林东升](../Page/林东升.md "wikilink")、[袁玫](../Page/袁玫.md "wikilink")、[陈颖](../Page/陈颖.md "wikilink")、[李景波](../Page/李景波.md "wikilink")                              |
| [探亲记](../Page/探亲记.md "wikilink")               | [谢添](../Page/谢添.md "wikilink")、[桑夫](../Page/桑夫.md "wikilink") | 配角黄院长                                                           | 魏鹤龄、[张平](../Page/张平_\(演员\).md "wikilink")、[张园](../Page/张园.md "wikilink")、[赵子岳](../Page/赵子岳.md "wikilink")、[里坡](../Page/里坡.md "wikilink")                             |                                                                                                                                                                                               |
| 1959                                           | [青春之歌](../Page/青春之歌_\(1959年电影\).md "wikilink")                | [崔嵬](../Page/崔嵬.md "wikilink")、[陈怀凯](../Page/陈怀凯.md "wikilink") | 配角王晓燕的母亲                                                                                                                                                             | [谢芳](../Page/谢芳.md "wikilink")、[康泰](../Page/康泰.md "wikilink")、[于洋](../Page/于洋.md "wikilink")、秦怡、[于是之](../Page/于是之.md "wikilink")                                                              |
| 1961                                           | 花儿朵朵                                                          | 谢添、[陈方千](../Page/陈方千.md "wikilink")                             | 配角母亲                                                                                                                                                                 |                                                                                                                                                                                               |
| 1962                                           | [昆仑山上的一棵草](../Page/昆仑山上的一棵草.md "wikilink")                    | [董克娜](../Page/董克娜.md "wikilink")                                | 配角李婉丽的母亲                                                                                                                                                             | [刘燕瑾](../Page/刘燕瑾.md "wikilink")、[李孟尧](../Page/李孟尧.md "wikilink")、[王者兰](../Page/王者兰.md "wikilink")                                                                                            |

### 话剧

|                                    |                                       |                                                               |                                                                    |                                                                                                                                                                                                                                        |
| ---------------------------------- | ------------------------------------- | ------------------------------------------------------------- | ------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **年份**                             | **剧名**                                | **导演**                                                        | **角色**                                                             | **合作演員 / 備註**                                                                                                                                                                                                                          |
| 1934                               | [回春之曲](../Page/回春之曲.md "wikilink")    |                                                               | 女主角梅娘                                                              | [金焰](../Page/金焰.md "wikilink")、[袁牧之](../Page/袁牧之.md "wikilink")、[王莹](../Page/王莹.md "wikilink")                                                                                                                                         |
| 1937                               | [保卫卢沟桥](../Page/保卫卢沟桥.md "wikilink")  |                                                               |                                                                    | [赵丹](../Page/赵丹.md "wikilink")、金焰、[陈波儿](../Page/陈波儿.md "wikilink")、[崔嵬](../Page/崔嵬.md "wikilink")、[施超](../Page/施超.md "wikilink")、[王为一](../Page/王为一.md "wikilink")、[顾而已](../Page/顾而已.md "wikilink")、[唐槐秋](../Page/唐槐秋.md "wikilink")、王莹 |
| 1944                               | [孔雀胆](../Page/孔雀胆.md "wikilink")      | [章泯](../Page/章泯.md "wikilink")                                | 阿盖公主                                                               | [陶金](../Page/陶金.md "wikilink")、[王班](../Page/王班.md "wikilink")、[傅惠珍](../Page/傅惠珍.md "wikilink")                                                                                                                                         |
| [天国春秋](../Page/天国春秋.md "wikilink") |                                       | 洪宣娇                                                           | 王班、[诸葛明](../Page/诸葛明.md "wikilink")、[白璐](../Page/白璐.md "wikilink") |                                                                                                                                                                                                                                        |
| 1958                               | 浮沉                                    | 李露玲                                                           | 女主角简素华                                                             | [李唐](../Page/李唐.md "wikilink")                                                                                                                                                                                                         |
| 1962                               | 兵临城下                                  | [傅杰](../Page/傅杰.md "wikilink")、[于洋](../Page/于洋.md "wikilink") | 杨蕴芳                                                                | 于洋、[张平](../Page/张平_\(演员\).md "wikilink")、[陈强](../Page/陈强.md "wikilink")、[周冠森](../Page/周冠森.md "wikilink")                                                                                                                               |
| 1979                               | [日出](../Page/日出_\(话剧\).md "wikilink") | 王人美、[巴鸿](../Page/巴鸿.md "wikilink")                            |                                                                    |                                                                                                                                                                                                                                        |

## 参考文献

## 外部链接

  - [上海年华王人美](https://web.archive.org/web/20131204100131/http://memoire.digilib.sh.cn/SHNH/star/star_index.jsp?starId=005)

{{-}}

[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:第五届全国政协委员](../Category/第五届全国政协委员.md "wikilink")
[Category:第六届全国政协委员](../Category/第六届全国政协委员.md "wikilink")
[Category:中国共产党党员
(1979年入党)](../Category/中国共产党党员_\(1979年入党\).md "wikilink")
[Category:長沙人](../Category/長沙人.md "wikilink")
[Category:浏阳人](../Category/浏阳人.md "wikilink")
[R人](../Category/王姓.md "wikilink")

1.  [王人美，长沙方志网，2010-09-19](http://szb.changsha.gov.cn/mrfc/201009/t20100919_252963.html)


2.

3.

4.  [叶浅予，叶浅予自传：细数沧桑记流年 第5节
    磕磕碰碰第五课，中国社会科学出版社，2006年](http://www.mcboda.com/book/zgxd/xxcs/069.htm)

5.

6.
7.
8.

9.
10.

11.
12.
13.
14.

15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28. [王人美，网上湖南，2004-09-13](http://hunan.voc.com.cn/gb/content/2004-09/13/content_1572303.htm)


29.
30.
31.
32.

33.
34.