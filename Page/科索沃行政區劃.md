**科索沃**在行政區劃上，是依照在[聯合國託管時期所劃分的](../Page/聯合國.md "wikilink")7州版本，獨立後也暫時沿用這個版本。

## 科索沃現行行政區劃

科索沃目前的行政規劃，是依照在1999年到2008年，聯合國[托管時期所劃分出來的版本](../Page/托管.md "wikilink")。在這個版本中，全科索沃劃分為7州
（[阿爾巴尼亞語](../Page/阿爾巴尼亞語.md "wikilink")：Rreth、[塞爾維亞語](../Page/塞爾維亞語.md "wikilink")：）30縣
（[阿爾巴尼亞語](../Page/阿爾巴尼亞語.md "wikilink")：komuna
e、[塞爾維亞語](../Page/塞爾維亞語.md "wikilink")：），獨立後依然沿用。
[Harta_e_Rajoneve_të_Kosovës.svg](https://zh.wikipedia.org/wiki/File:Harta_e_Rajoneve_të_Kosovës.svg "fig:Harta_e_Rajoneve_të_Kosovës.svg")

<table>
<thead>
<tr class="header">
<th><p>州名</p></th>
<th><p>首府</p></th>
<th><p>面積<br />
（km²）</p></th>
<th><p>2011年人口<br />
（排行）</p></th>
<th><p>人口密度<br />
（每 km²）</p></th>
<th><p>縣</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/賈科維察州.md" title="wikilink">賈科維察州</a><br />
<small>(<strong><em>Đakovički okrug/Gjakova District</em></strong>)</small><br />
<a href="https://zh.wikipedia.org/wiki/File:Rajoni_i_Gjakovës_në_Hartën_e_Kosovës.svg" title="fig:Rajoni_i_Gjakovës_në_Hartën_e_Kosovës.svg">Rajoni_i_Gjakovës_në_Hartën_e_Kosovës.svg</a></p></td>
<td><p><a href="../Page/賈科維察.md" title="wikilink">賈科維察</a></p></td>
<td><p>1129</p></td>
<td><p>194,672</p></td>
<td><p>170</p></td>
<td><ul>
<li><a href="../Page/賈科維察.md" title="wikilink">賈科維察</a></li>
<li><a href="../Page/代查尼.md" title="wikilink">代查尼</a></li>
<li><a href="../Page/奧拉霍瓦茨.md" title="wikilink">奧拉霍瓦茨</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/格尼拉內州.md" title="wikilink">格尼拉內州</a><br />
<small>(<strong><em>Gnjilanski okrug/Gjilani District</em></strong>)</small><br />
<a href="https://zh.wikipedia.org/wiki/File:Rajoni_i_Gjilanit_në_Hartën_e_Kosovës.svg" title="fig:Rajoni_i_Gjilanit_në_Hartën_e_Kosovës.svg">Rajoni_i_Gjilanit_në_Hartën_e_Kosovës.svg</a></p></td>
<td><p><a href="../Page/格尼拉內.md" title="wikilink">格尼拉內</a></p></td>
<td><p>1206</p></td>
<td><p>180,783</p></td>
<td><p>150</p></td>
<td><ul>
<li><a href="../Page/格尼拉內.md" title="wikilink">格尼拉內</a></li>
<li><a href="../Page/卡梅尼察_(格尼拉內州).md" title="wikilink">卡梅尼察</a></li>
<li><a href="../Page/維蒂納.md" title="wikilink">維蒂納</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/米特羅維察州.md" title="wikilink">米特羅維察州</a><br />
<small>(<strong><em>Kosovskomitrovački okrug/Komuna e Mitrovicës</em></strong>)</small><br />
<a href="https://zh.wikipedia.org/wiki/File:Rajoni_i_Mitrovicës_në_Hartën_e_Kosovës.svg" title="fig:Rajoni_i_Mitrovicës_në_Hartën_e_Kosovës.svg">Rajoni_i_Mitrovicës_në_Hartën_e_Kosovës.svg</a></p></td>
<td><p><a href="../Page/科索沃米特羅維察.md" title="wikilink">科索沃米特羅維察</a></p></td>
<td><p>2077</p></td>
<td><p>272,247</p></td>
<td><p>110</p></td>
<td><ul>
<li><a href="../Page/科索沃米特羅維察.md" title="wikilink">科索沃米特羅維察</a></li>
<li><a href="../Page/萊波薩維奇.md" title="wikilink">萊波薩維奇</a></li>
<li><a href="../Page/斯爾比察.md" title="wikilink">斯爾比察</a></li>
<li><a href="../Page/武契特爾恩.md" title="wikilink">武契特爾恩</a></li>
<li><a href="../Page/祖賓波托克.md" title="wikilink">祖賓波托克</a></li>
<li><a href="../Page/茲韋錢.md" title="wikilink">茲韋錢</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佩奇州.md" title="wikilink">佩奇州</a><br />
<small>(<strong><em>Pećki okrug/Peja District</em></strong>)</small><br />
<a href="https://zh.wikipedia.org/wiki/File:Rajoni_i_Pejës_në_Hartën_e_Kosovës.svg" title="fig:Rajoni_i_Pejës_në_Hartën_e_Kosovës.svg">Rajoni_i_Pejës_në_Hartën_e_Kosovës.svg</a></p></td>
<td><p><a href="../Page/佩奇_(科索沃).md" title="wikilink">佩奇</a></p></td>
<td><p>1365</p></td>
<td><p>174,235</p></td>
<td><p>130</p></td>
<td><ul>
<li><a href="../Page/佩奇.md" title="wikilink">佩奇</a></li>
<li><a href="../Page/伊斯托克.md" title="wikilink">伊斯托克</a></li>
<li><a href="../Page/克利納.md" title="wikilink">克利納</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/普里什蒂納州.md" title="wikilink">普里什蒂納州</a><br />
<small>(<strong><em>Prištinski okrug/Komuna e Prishtinës</em></strong>)</small><br />
<a href="https://zh.wikipedia.org/wiki/File:Rajoni_i_Prishtinës_në_Hartën_e_Kosovës.svg" title="fig:Rajoni_i_Prishtinës_në_Hartën_e_Kosovës.svg">Rajoni_i_Prishtinës_në_Hartën_e_Kosovës.svg</a></p></td>
<td><p><a href="../Page/普里什蒂納.md" title="wikilink">普里什蒂納</a></p></td>
<td><p>2470</p></td>
<td><p>477,312</p></td>
<td><p>223</p></td>
<td><ul>
<li><a href="../Page/普里什蒂納.md" title="wikilink">普里什蒂納</a></li>
<li><a href="../Page/格洛科瓦茨.md" title="wikilink">格洛科瓦茨</a></li>
<li><a href="../Page/科索沃波爾耶.md" title="wikilink">科索沃波爾耶</a></li>
<li><a href="../Page/利普連.md" title="wikilink">利普連</a></li>
<li><a href="../Page/新布爾多.md" title="wikilink">新布爾多</a></li>
<li><a href="../Page/奧比利奇.md" title="wikilink">奧比利奇</a></li>
<li><a href="../Page/波杜耶夫.md" title="wikilink">波杜耶夫</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/普里茲倫州.md" title="wikilink">普里茲倫州</a><br />
<small>(<strong><em>Prizrenski okrug/Prizreni District</em></strong>)</small><br />
<a href="https://zh.wikipedia.org/wiki/File:Rajoni_i_Prizrenit_në_Hartën_e_Kosovës.svg" title="fig:Rajoni_i_Prizrenit_në_Hartën_e_Kosovës.svg">Rajoni_i_Prizrenit_në_Hartën_e_Kosovës.svg</a></p></td>
<td><p><a href="../Page/普里茲倫.md" title="wikilink">普里茲倫</a></p></td>
<td><p>1397</p></td>
<td><p>331,670</p></td>
<td><p>240</p></td>
<td><ul>
<li><a href="../Page/普里茲倫.md" title="wikilink">普里茲倫</a></li>
<li><a href="../Page/德拉加什.md" title="wikilink">德拉加什</a> （Dragaš）</li>
<li><a href="../Page/蘇瓦雷卡.md" title="wikilink">蘇瓦雷卡</a></li>
<li><a href="../Page/馬利舍沃.md" title="wikilink">馬利舍沃</a> （Mališevo）</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/烏羅舍瓦茨州.md" title="wikilink">烏羅舍瓦茨州</a><br />
<small>(<strong><em>Uroševački okrug/Ferizaji District</em></strong>)</small><br />
<a href="https://zh.wikipedia.org/wiki/File:Rajoni_i_Ferizajt_në_Hartën_e_Kosovës.svg" title="fig:Rajoni_i_Ferizajt_në_Hartën_e_Kosovës.svg">Rajoni_i_Ferizajt_në_Hartën_e_Kosovës.svg</a></p></td>
<td><p><a href="../Page/烏羅舍瓦茨.md" title="wikilink">烏羅舍瓦茨</a></p></td>
<td><p>1030</p></td>
<td><p>185,806</p></td>
<td><p>180</p></td>
<td><ul>
<li><a href="../Page/烏羅舍瓦茨.md" title="wikilink">烏羅舍瓦茨</a></li>
<li><a href="../Page/什蒂姆列.md" title="wikilink">什蒂姆列</a></li>
<li><a href="../Page/卡查尼克.md" title="wikilink">卡查尼克</a></li>
<li><a href="../Page/什特爾普采.md" title="wikilink">什特爾普采</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 早期[塞爾維亞的版本](../Page/塞爾維亞.md "wikilink")

而在塞爾維亞方面，由於塞爾維亞宣稱科索沃為其一部分，因此塞國官方也有自己的一套劃分版本。在此版本中，稱科索沃為**科索沃與梅托希亞**，劃分為5州
（okrug）30縣。
[M_kosovo02.png](https://zh.wikipedia.org/wiki/File:M_kosovo02.png "fig:M_kosovo02.png")

<table>
<thead>
<tr class="header">
<th><p>州名</p></th>
<th><p>首府</p></th>
<th><p>2002年人口<br />
（排行）</p></th>
<th><p>縣</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/普里什蒂纳州.md" title="wikilink">科索沃州</a><br />
<small>(<em>Kosovski okrug</em>)</small></p></td>
<td><p><a href="../Page/普里什蒂納.md" title="wikilink">普里什蒂納</a></p></td>
<td><p>672,292</p></td>
<td><ul>
<li><a href="../Page/波杜耶夫.md" title="wikilink">波杜耶夫</a> （Podujevo）</li>
<li><a href="../Page/普里什蒂納.md" title="wikilink">普里什蒂納</a></li>
<li><a href="../Page/烏羅舍瓦茨.md" title="wikilink">烏羅舍瓦茨</a></li>
<li><a href="../Page/奧比利奇.md" title="wikilink">奧比利奇</a> （Obilić）</li>
<li><a href="../Page/格洛科瓦茨.md" title="wikilink">格洛科瓦茨</a> （Glogovac）</li>
<li><a href="../Page/利普連.md" title="wikilink">利普連</a> （Lipljan）</li>
<li><a href="../Page/科索沃波爾耶.md" title="wikilink">科索沃波爾耶</a> （Kosovo Polje）</li>
<li><a href="../Page/什蒂姆列.md" title="wikilink">什蒂姆列</a> （Štimlje）</li>
<li><a href="../Page/什特爾普采.md" title="wikilink">什特爾普采</a> （Štrpce）</li>
<li><a href="../Page/卡查尼克.md" title="wikilink">卡查尼克</a> （Kačanik）</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吉尼拉内州.md" title="wikilink">科索沃-波莫拉夫列州</a><br />
<small>(<em>Kosovsko-Pomoravski okrug</em>)</small></p></td>
<td><p><a href="../Page/吉尼拉內.md" title="wikilink">吉尼拉內</a></p></td>
<td><p>217,726</p></td>
<td><ul>
<li><a href="../Page/科索沃-卡梅尼察.md" title="wikilink">科索沃-卡梅尼察</a> （Kosovska Kamenica）</li>
<li><a href="../Page/新布爾多.md" title="wikilink">新布爾多</a> （Novo Brdo）</li>
<li><a href="../Page/吉尼拉內.md" title="wikilink">吉尼拉內</a></li>
<li><a href="../Page/維蒂納.md" title="wikilink">維蒂納</a> （Vitina）</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/科索夫斯卡米特羅維查.md" title="wikilink">科索夫斯卡米特羅維查州</a><br />
<small>(<em>Kosovsko - Mitrovački okrug</em>''')</small></p></td>
<td><p><a href="../Page/科索夫斯卡米特羅維查.md" title="wikilink">科索夫斯卡米特羅維查</a></p></td>
<td><p>275,904</p></td>
<td><ul>
<li><a href="../Page/祖賓波托克.md" title="wikilink">祖賓波托克</a> （Zubin Potok）</li>
<li><a href="../Page/萊波薩維齊.md" title="wikilink">萊波薩維齊</a> （Leposavić）</li>
<li><a href="../Page/茲韋錢.md" title="wikilink">茲韋錢</a> （Zvečan）</li>
<li><a href="../Page/科索夫斯卡米特羅維查.md" title="wikilink">科索夫斯卡米特羅維查</a></li>
<li><a href="../Page/斯爾比察.md" title="wikilink">斯爾比察</a> （Srbica）</li>
<li><a href="../Page/武契特爾恩.md" title="wikilink">武契特爾恩</a> （Vučitrn）</li>
</ul></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佩奇_(科索沃).md" title="wikilink">佩奇州</a><br />
<small>(<em>Pećki okrug</em>)</small></p></td>
<td><p><a href="../Page/佩奇_(科索沃).md" title="wikilink">佩奇</a></p></td>
<td><p>414,187</p></td>
<td><ul>
<li><a href="../Page/佩奇_(科索沃).md" title="wikilink">佩奇</a></li>
<li><a href="../Page/伊斯托克.md" title="wikilink">伊斯托克</a> （Istok）</li>
<li><a href="../Page/克利納.md" title="wikilink">克利納</a> （Klina）</li>
<li><a href="../Page/代查尼.md" title="wikilink">代查尼</a> （Dečani）</li>
<li><a href="../Page/達科維卡.md" title="wikilink">達科維卡</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/普里茲倫州.md" title="wikilink">普里茲倫州</a><br />
<small>(<em>Prizrenski okrug</em>)</small></p></td>
<td><p><a href="../Page/普里茲倫.md" title="wikilink">普里茲倫</a></p></td>
<td><p>376,085</p></td>
<td><ul>
<li><a href="../Page/蘇瓦雷卡.md" title="wikilink">蘇瓦雷卡</a> （Suva Reka）</li>
<li><a href="../Page/奧拉霍瓦茨.md" title="wikilink">奧拉霍瓦茨</a> （Orahovac）</li>
<li><a href="../Page/普里茲倫.md" title="wikilink">普里茲倫</a></li>
<li><a href="../Page/戈拉_(科索沃).md" title="wikilink">戈拉</a> （Gora）</li>
<li><a href="../Page/奧普列.md" title="wikilink">奧普列</a> （Opolje）</li>
</ul></td>
</tr>
</tbody>
</table>

[科索沃行政区划](../Category/科索沃行政区划.md "wikilink")