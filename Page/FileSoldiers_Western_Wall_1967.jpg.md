## Summary

Zion Karasanti, Yitzhak Yifat and Haim Oshri,
[IDF](../Page/Israel_Defense_Forces.md "wikilink") paratroopers at
[Jerusalem](../Page/Jerusalem.md "wikilink")'s [Western
Wall](../Page/Western_Wall.md "wikilink") shortly after its capture.
Picture taken by [David Rubinger](../Page/David_Rubinger.md "wikilink")
and uploaded from Israel's [Knesset](../Page/Knesset.md "wikilink")
official website
([www.knesset.gov.il/history/eng/eng_hist6.htm](http://www.knesset.gov.il/history/eng/eng_hist6.htm)).

## Fair Use Rationale

  - Purpose and character: To illustrate Wikipedia's article on the
    [Six-Day War](../Page/Six-Day_War.md "wikilink"). Wikipedia is a
    free encyclopedia and our articles and illustrations serve
    educational purposes.
  - Nature of the copied work: This image is an iconic photograph of the
    capture of Jerusalem's [Western
    Wall](../Page/Western_Wall.md "wikilink"), a significant event in
    the history of the 20th century. Wikipedia's copy is a
    low-resolution version of the photograph.
  - Amount and substantiality: One image displayed. The image has been
    prolific in Israeli culture, the news media, and scholarly works. It
    has been reproduced on Israeli government websites, newspaper
    websites, etc. Because this is one of the most recognizable images
    of the [Six-Day War](../Page/Six-Day_War.md "wikilink"), displaying
    a low-resolution version on Wikipedia does not lessen the value of
    the image for the copyright holder.
  - Because the image depicts a historic event and is itself a unique
    photo, no opportunity now exists to create a free-licence variant of
    this image.

## Licensing

來源自英文版維基網站