**Authorware**最初是由[Michael
Allen于](../Page/Michael_Allen.md "wikilink")1987年创建的公司，而[multimedia正是Authorware公司的产品](../Page/multimedia.md "wikilink")。1970年代，Allen参加协助PLATO[学习管理系统](../Page/学习管理系统.md "wikilink")（Learning
Management
System，LMS）的开发。Authorware是一种解释型、基于[流程的图形](../Page/流程.md "wikilink")[编程语言](../Page/编程语言.md "wikilink")。Authorware被用于创建互动的程序，其中整合了[声音](../Page/声音.md "wikilink")、文本、[图形](../Page/计算机图形.md "wikilink")、简单[动画](../Page/动画.md "wikilink")，以及数字[电影](../Page/电影.md "wikilink")。

## 历史

1992年，Authorware跟MacroMind-Paracomp合并，组成了[Macromedia公司](../Page/Adobe_Systems.md "wikilink")。2005年，Adobe与Macromedia签署合并协议，新公司名仍旧名为[Adobe
Systems](../Page/Adobe_Systems.md "wikilink")。2007年8月3日，Adobe宣布停止在Authorware的開發計劃\[1\]，而且並沒有為Authorware提供其他相容產品作替代。Adobe解釋：由於科技日新月異，電子學習與傳統學習模式的界線變得愈來愈模糊。所以，現時學習管理系統並不再適用，反而應該著眼於推廣其他可以讓教學人員可以作快速學習的工具，例如：[Acrobat](../Page/Acrobat.md "wikilink")、[Acrobat
Connect](../Page/Acrobat_Connect.md "wikilink")、[Captivate等軟件](../Page/Captivate.md "wikilink")。

## 特性

Authorware程序开始时，新建一个“流程图”，通过直观的流程图来表示用户程序的结构。用户可以增加并管理[文本](../Page/文本.md "wikilink")、[图形](../Page/图形.md "wikilink")、[动画](../Page/动画.md "wikilink")、[声音以及](../Page/声音.md "wikilink")[视频](../Page/视频.md "wikilink")，还可以开发各种交互，以及起导航作用的各种[链接](../Page/超链接.md "wikilink")、按钮、菜单。[Macromedia
Director的电影业可以整合到Authorware项目中](../Page/Macromedia_Director.md "wikilink")。Xtras，或add-ins，也可以用于Authorware功能的扩展，这类似于[HyperCard的XCMD](../Page/HyperCard.md "wikilink")。通过变量、函数以及各种表达式，Authorware的力量可以进一步地被开启。

Adobe
Authorware是當今使用最廣泛的[e-learning編輯軟件](../Page/e-learning.md "wikilink")（authoring
applications）之一。Authorware之類的編輯軟件，主要用於生產[多媒體交互式課件](../Page/多媒體.md "wikilink")。

課件內容可以隨心所欲，從簡單地演示如何換輪胎，到複雜的工業或醫學操作。對於一些簡單的多媒體應用程序而言，還需要使用一些簡單的腳本，這樣看起來會更有感染力。腳本應用的越熟練，Authorware功能被發掘的也就越多。在Auhtorware
7中，腳本除了用Authorware本地腳本語言，還可以用[JavaScript](../Page/JavaScript.md "wikilink")。

光看這個截屏，如果你原來沒有接觸過，可能會感到無從下手，但即使你不懂腳本，通過Authorware提供的簡單[對話框](../Page/對話框.md "wikilink")，也可以對圖標的行為、屬性等進行配置，進而開發出簡單的e-learning應用程序。

Authorware可通過CD、DVD以及網絡，進行e-learning內容發佈。遵照[SCORM標準後](../Page/SCORM.md "wikilink")，Authorware的作品可以放在任何SCORM兼容的[學習管理系統系統上發佈](../Page/學習管理系統.md "wikilink")。許多媒體類型的文件都可以被導入Authorware中，比如[Adobe
Flash](../Page/Adobe_Flash.md "wikilink")、[Adobe
Director](../Page/Adobe_Director.md "wikilink")、各種格式的數字視頻、數字音頻、各種格式的圖片，還有[Powerpoint](../Page/Powerpoint.md "wikilink")。Authorware通過第三方的「Xtras」和「.u32's」，還可以進行更豐富的功能擴充。

## 參考文獻

<references />

## 外部链接

  - [Authorware概览](http://www.macromedia.com/software/authorware/)
  - [Macromedia的Authorware论坛支持](http://webforums.macromedia.com/authorware/)
  - [Macromedia'的Authorware新闻组支持](news://foums.macromedia.com/authorware)
  - [Authorware 资源](http://www.awaretips.net/)
  - [Authorware Web Ring](http://r.webring.com/hub?ring=authoring)

[Category:Macromedia 软件](../Category/Macromedia_软件.md "wikilink")

1.  <http://www.adobe.com/products/authorware/productinfo/faq/eod>