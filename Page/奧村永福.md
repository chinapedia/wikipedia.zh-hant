**奧村
永福**，出生於1541年（[天文](../Page/天文.md "wikilink")10年）卒於1624年7月27日（[寬永元年陰曆](../Page/寬永.md "wikilink")6月12日）。是日本[戰國時代的武將](../Page/戰國時代_\(日本\).md "wikilink")、[前田氏的家臣](../Page/前田氏.md "wikilink")。又稱**助十郎**、**助右衛門**。官拜**伊予守**。號**快心**。其子有[奧村榮明](../Page/奧村榮明.md "wikilink")、[奧村易英](../Page/奧村易英.md "wikilink")、[奧村榮賴](../Page/奧村榮賴.md "wikilink")。

出生於[尾張國](../Page/尾張.md "wikilink")。奧村家世代皆仕奉於[前田氏](../Page/前田氏.md "wikilink")、永福本人自[前田利家父親](../Page/前田利家.md "wikilink")[前田利春時代即開始在前田家任職](../Page/前田利春.md "wikilink")。在1569年（[永禄](../Page/永禄.md "wikilink")12年）因不滿前田利家成為家督而離開前田家成為浪人。

1573年（[天正元年](../Page/天正.md "wikilink")）織田家開始侵略[越前時永福重新回歸侍奉前田利家](../Page/越前.md "wikilink")。在征伐[朝倉氏時多次立功](../Page/朝倉氏.md "wikilink")，利家被封至[加賀國時](../Page/加賀.md "wikilink")，他被任予軍事要衝[末森城城主一職](../Page/末森城_\(能登国\).md "wikilink")。1584年爆發小牧長久手之戰，支持德川方的[越中國主](../Page/越中.md "wikilink")[佐佐成政率兵攻擊歸屬羽柴方的前田利家](../Page/佐佐成政.md "wikilink")，而奧村永福守備的末森城首當其衝，雖然城池的二之丸已經淪陷、永福仍帶領少數的兵力頑強抵抗直到利家的援軍前田慶次郎到來，並成功擊退敵軍。守城戰時、據說永福的賢內助**加藤安**、曾帶著薙刀協助巡守城內、並準備粥食前往提振傷兵的士氣。

之後他與[村井長賴成為加賀前田家的兩大支柱重臣](../Page/村井長賴.md "wikilink")。參與了豐臣秀吉的[九州征伐](../Page/九州征伐.md "wikilink")、[小田原征伐](../Page/小田原征伐.md "wikilink")，利家去世後隱居出家，不久之後在前田利長的邀請之下回鍋前田家，[大坂之陣時擔任](../Page/大坂之陣.md "wikilink")[金澤城城代](../Page/金澤城.md "wikilink")，之後便待在前田家並一路侍奉到三代家督利常，成為前田家年紀與地位都最高的首席家老，後來因年紀老邁辭官退休，享年83歲。法號永福院殿快心宗活居士。

其後子孫皆代代相傳成為[加賀藩的家老](../Page/加賀藩.md "wikilink")。

墓地位在今[石川縣](../Page/石川縣.md "wikilink")[金澤市永福寺](../Page/金澤市.md "wikilink")。寺中並有收藏其肖像畫。

[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:加賀藩](../Category/加賀藩.md "wikilink")
[Category:奧村氏](../Category/奧村氏.md "wikilink")
[Category:1541年出生](../Category/1541年出生.md "wikilink")
[Category:1624年逝世](../Category/1624年逝世.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")