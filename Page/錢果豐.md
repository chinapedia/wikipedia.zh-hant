**錢果豐**<span style="font-size:smaller;">，[CBE](../Page/CBE.md "wikilink")</span>（**Raymond
Chien Kuo Fung**，），[香港私人及公營機構行政人員](../Page/香港.md "wikilink")。

## 生平

生於[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")，籍貫[中國](../Page/中國.md "wikilink")[江蘇](../Page/江蘇.md "wikilink")[蘇州](../Page/蘇州.md "wikilink")\[1\]，其父錢明年為前[中華民國空軍](../Page/中華民國.md "wikilink")、聖公會牧師及前[香港戒毒會總監](../Page/香港戒毒會.md "wikilink")。曾為CDC
Corporation（已破產）主席，[香港鐵路有限公司](../Page/香港鐵路有限公司.md "wikilink")（前[地鐵有限公司](../Page/地鐵有限公司.md "wikilink")）前任董事局非執行主席，[香港上海滙豐銀行有限公司](../Page/香港上海滙豐銀行有限公司.md "wikilink")，以及[恒生銀行](../Page/恒生銀行.md "wikilink")[董事長](../Page/董事長.md "wikilink")，並曾擔任[滙豐控股有限公司](../Page/滙豐控股有限公司.md "wikilink")、[九龍倉集團有限公司](../Page/九龍倉集團有限公司.md "wikilink")（直至2016年1月1日退任\[2\]），以及英之傑集團董事。

1978年，錢果豐於[美國](../Page/美國.md "wikilink")[賓夕法尼亞大學取得經濟學博士學位](../Page/賓夕法尼亞大學.md "wikilink")。他於1993年獲委任為[太平紳士](../Page/太平紳士.md "wikilink")，並於1994年獲頒授[英帝國司令勳章](../Page/英帝國司令勳章.md "wikilink")。1999年，他再獲頒授[金紫荊星章](../Page/金紫荊星章.md "wikilink")。

在公職上，錢果豐為[廉政公署貪污問題諮詢委員會主席](../Page/廉政公署_\(香港\).md "wikilink")、香港/歐盟經濟合作委員會主席，以及[亞太經合組織商業諮詢委員會的香港區成員](../Page/亞太經合組織.md "wikilink")。他亦是[香港工業總會名譽會長及前主席](../Page/香港工業總會.md "wikilink")，香港工業科技中心公司及香港/日本經濟合作委員會前主席。1992年至2002年6月期間，錢果豐先後獲委任為前行政局及[行政會議成員](../Page/行政會議.md "wikilink")。

1998年，錢果豐先後出任前[香港地下鐵路公司](../Page/香港地下鐵路公司.md "wikilink")（此公司在2000年[私有化後改名為地鐵有限公司](../Page/私有化.md "wikilink")）的董事局成員。2003年，他接替[蘇澤光](../Page/蘇澤光.md "wikilink")，成為地鐵公司董事局主席。2007年8月，他接替離任的[邵銘高](../Page/邵銘高.md "wikilink")，出任[恒生銀行董事長](../Page/恒生銀行.md "wikilink")。\[3\]

## 爭議

於[香港高鐵超支延誤事件上](../Page/香港高鐵超支延誤事件.md "wikilink")，錢果豐拒絕出席[立法會專責委員會聆訊](../Page/香港立法會.md "wikilink")，令委員會對他表示失望。\[4\]

## 參考

## 外部連結

  - [MTR Corporation -
    董事局成員](http://www.mtr.com.hk/chi/investrelation/governance.php#04)

{{-}}

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:獲頒授香港金紫荊星章者](../Category/獲頒授香港金紫荊星章者.md "wikilink")
[Category:汇丰人物](../Category/汇丰人物.md "wikilink")
[Category:恒生银行人物](../Category/恒生银行人物.md "wikilink")
[Category:九龍倉集團](../Category/九龍倉集團.md "wikilink")
[Category:港鐵公司](../Category/港鐵公司.md "wikilink")
[Category:日本華人](../Category/日本華人.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:前香港行政局議員](../Category/前香港行政局議員.md "wikilink")
[Category:华润人物](../Category/华润人物.md "wikilink")
[Category:聖士提反書院校友](../Category/聖士提反書院校友.md "wikilink")
[Category:賓夕法尼亞大學校友](../Category/賓夕法尼亞大學校友.md "wikilink")
[Category:蘇州人](../Category/蘇州人.md "wikilink")
[G](../Category/錢姓.md "wikilink")

1.  [錢果豐信步政商界
    樂做公職王](http://paper.wenweipo.com/2008/11/26/MR0811260001.htm)
2.  [九龍倉集團有限公司獨立非執行董事辭任通告](http://www.wharfholdings.com/file/151110%20WHL%20BOD%20c.pdf)
3.  [董事簡介](https://bank.hangseng.com/1/2/chi/about-us/directors-organisation/board-of-directors#raymondchien)
4.