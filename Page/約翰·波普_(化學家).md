*'
約翰·安东尼·波普*'爵士，[KBE](../Page/KBE.md "wikilink")，[FRS](../Page/皇家學會院士.md "wikilink")（，），英国化学家，1998年[諾貝爾化學獎得主之一](../Page/諾貝爾化學獎.md "wikilink")。

## 生平

約翰生於[英格兰](../Page/英格兰.md "wikilink")[索美塞特郡一个名为](../Page/索美塞特郡.md "wikilink")[濱海伯納姆的小镇](../Page/濱海伯納姆.md "wikilink")，他的父亲是一位经营服装店的商人，母亲则来自一个农民家庭。少年时期的波普曾经就读于[布里斯托中学](../Page/布里斯托中学.md "wikilink")，现在在那所中學还有一间计算机房和一项奖学金以波普的名字命名。青年波普进入英国[剑桥大学](../Page/剑桥大学.md "wikilink")，成为家族中第一位大学生，1946年波普获得了他的学士学位，1951年获得数学系的哲學博士学位(PhD)，虽然获得的是数学系的学位，但是波普的博士学位论文却是关于[化学的内容](../Page/化学.md "wikilink")：[水分子的价键结构](../Page/水.md "wikilink")。1960年代早期约翰·波普移民[美国并一直居住在那里直到逝世](../Page/美国.md "wikilink")，但他保留了他的英国国籍。波普本人更倾向于自认为是一位[数学家而非](../Page/数学家.md "wikilink")[化学家](../Page/化学.md "wikilink")，但是理论化学家们却普遍认为，约翰·安东尼·波普是他们当中最重要的成员之一。

約翰·波普作出的第一项重大贡献是在1953年提出的对[π电子体系的](../Page/π电子.md "wikilink")[分子轨道近似理论](../Page/分子轨道.md "wikilink")，由于理论化学家Rudolph
Pariser和[罗伯特·帕尔在同年亦提出了等价于波普理论的类似方法](../Page/罗伯特·帕尔.md "wikilink")，因而这种近似方法现在又被称为[PPP方法](../Page/PPP方法.md "wikilink")。1965年波普又提出了完全忽略异核[双电子积分的](../Page/量子化学中的双电子积分.md "wikilink")[CNDO方法以及部分忽略异核双电子积分的](../Page/CNDO方法.md "wikilink")[INDO方法](../Page/INDO方法.md "wikilink")，CNDO和INDO是至今仍有广泛应用的近似计算三维分子中分子结构性质的半经验量子化学计算方法。此外，波普还对计算化学理论的发展作出了许多重大的贡献。约翰·波普对量子化学从头计算方法作出了改进，他引入了[斯莱特型函数和](../Page/斯莱特型函数.md "wikilink")[高斯型函数组成的](../Page/高斯型函数.md "wikilink")[基组来模拟](../Page/量子化学中的基组.md "wikilink")[波函数](../Page/波函数.md "wikilink")，这大大降低了[从头计算的计算成本](../Page/从头计算.md "wikilink")，而如今迅猛发展的计算机技术更是凸现了波普所引入的新方法在计算中的优势。约翰·波普最为人们所了解的贡献，便是他将计算化学理论方法工具化，开发出了现今应用最广泛的[量子化学计算程序包](../Page/量子化学.md "wikilink")[GAUSSIAN](../Page/GAUSSIAN.md "wikilink")，但是令人遗憾的是，1991年之后，波普不仅被排除出GAUSSIAN软件的开发团队，甚至被公司禁止使用这一软件。

1986年以前波普一直任职于[宾夕法尼亚州](../Page/宾夕法尼亚州.md "wikilink")[匹兹堡的](../Page/匹兹堡.md "wikilink")[卡内基梅隆大学](../Page/卡内基梅隆大学.md "wikilink")，在这里他完成了早年的主要理论贡献。1986年波普离开[卡内基梅隆大学](../Page/卡内基梅隆大学.md "wikilink")，来到芝加哥供职于[西北大学](../Page/西北大学.md "wikilink")。1998年年他由于在量子化学计算方法方面卓越的贡献获得了当年度諾貝爾化學獎；2003年获得封爵。

## 外部链接

  - [約翰·波普自传(英文)](http://nobelprize.org/chemistry/laureates/1998/pople-autobio.html)
  - [1998年度诺贝尔化学奖主页(英文)](http://www.nobel.se/chemistry/laureates/1998/index.html)
  - [PPP方法(英文)](http://www.quantum-chemistry-history.com/Popl_Dat/Pople2.htm)
  - [卫报刊发的讣闻(英文)](http://www.guardian.co.uk/obituaries/story/0,3604,1172894,00.html)

[P](../Category/英國化學家.md "wikilink")
[P](../Category/诺贝尔化学奖获得者.md "wikilink")
[P](../Category/KBE勳銜.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:沃爾夫化學獎得主](../Category/沃爾夫化學獎得主.md "wikilink")
[Category:戴维奖章](../Category/戴维奖章.md "wikilink")