[Whitfield_Road.JPG](https://zh.wikipedia.org/wiki/File:Whitfield_Road.JPG "fig:Whitfield_Road.JPG")
**威非路道**（英文：**Whitfield
Road**；又寫作**威菲路道**）是[香港一條道路](../Page/香港.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[天后](../Page/天后.md "wikilink")，東接[電氣道](../Page/電氣道.md "wikilink")、[屈臣道](../Page/屈臣道.md "wikilink")，西接[興發街](../Page/興發街.md "wikilink")，中間有[玻璃街通往](../Page/玻璃街.md "wikilink")。[銅鑼灣（威非路道）巴士總站正是位於威非路道近玻璃街](../Page/銅鑼灣（威非路道）巴士總站.md "wikilink")。

威非路道是以早期殖民地時代的[威菲路中將命名](../Page/威菲路.md "wikilink")\[1\]。其他以威菲路中將命名的地方有[威非路警署](../Page/威非路警署.md "wikilink")（即[銅鑼灣警署](../Page/銅鑼灣警署.md "wikilink")，已拆卸，原址興建電氣道148）、[威非路軍營](../Page/威非路軍營.md "wikilink")（現址為[九龍公園](../Page/九龍公園.md "wikilink")）等。

## 著名地點

  - [萬國寶通中心](../Page/萬國寶通中心.md "wikilink")

## 途經之公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

## 參考資料

## 外部連結

[Category:銅鑼灣街道](../Category/銅鑼灣街道.md "wikilink")
[Category:天后街道](../Category/天后街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")

1.  [道路及鐵路－道路名稱(五)英官員命名道路](http://www.hk-place.com/view.php?id=330)