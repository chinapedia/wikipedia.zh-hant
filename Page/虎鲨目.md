**虎鲨目**（[學名](../Page/學名.md "wikilink")：Heterodontiformes），又稱為**異齒鮫目**、**異齒鯊目**，是[板鳃亞綱中的一个比较小的](../Page/板鳃亞綱.md "wikilink")[目](../Page/目.md "wikilink")，為現代[鯊魚的](../Page/鯊魚.md "wikilink")[基群](../Page/基群.md "wikilink")。包含**虎鯊科**與**虎鯊屬**，現存共有
9 種。屬於小型鯊魚，體型最大的物種體長僅
，為[熱帶及](../Page/熱帶.md "wikilink")[亞熱帶海域中的底棲物種](../Page/亞熱帶.md "wikilink")。

虎鯊目的化石最早發現於[侏儸紀早期](../Page/侏儸紀.md "wikilink")，遠早於其他。

## 分布

本目廣泛分布於[熱帶及](../Page/熱帶.md "wikilink")[亞熱帶的](../Page/亞熱帶.md "wikilink")[太平洋和西](../Page/太平洋.md "wikilink")[印度洋](../Page/印度洋.md "wikilink")。深度約5至275米，但通常不超過100米。

## 特性

[虎鯊的形態非常獨特](../Page/虎鯊.md "wikilink")。口部位於眼臉的前方，口部前端有唇[軟骨](../Page/軟骨.md "wikilink")。牠有鼻溝，連接鼻孔至口部。鼻腔呈[喇叭型](../Page/喇叭.md "wikilink")，與眼眶完全分開，環鼻有皮褶。雖然[腦顱有腦前溝](../Page/腦顱.md "wikilink")，但卻沒有吻骨。頭蓋上有眶頂冠。虎鯊目的[眼睛沒有](../Page/眼睛.md "wikilink")[瞬膜](../Page/瞬膜.md "wikilink")。牠們有一個細小的[呼吸孔](../Page/呼吸孔.md "wikilink")。第四及第五鰓弓末端在背部附接，並不像[鼠鯊目般是融合成像鶴嘴鋤般](../Page/鼠鯊目.md "wikilink")。虎鯊目有兩扇[背鰭及](../Page/背鰭.md "wikilink")[臀鰭](../Page/臀鰭.md "wikilink")，背鰭都有鰭棘。背鰭及臀鰭亦包含了基本的軟骨，並非只是鰭條。

虎鯊目的[化石可追溯至](../Page/化石.md "wikilink")[侏羅紀早期](../Page/侏羅紀.md "wikilink")，比其他[真鯊首目更為早](../Page/真鯊首目.md "wikilink")。但是，由於牠們並不十分普遍，牠們的起源可能會推得更為後期。这些鲨魚的长度在50至150厘米之间。

虎鯊屬於底棲性鯊魚，夜行性，行動遲緩，有良好的保護色，喜棲於礁岩及沙泥交錯處。卵生，以底棲[無脊椎動物](../Page/無脊椎動物.md "wikilink")，如[甲殼類](../Page/甲殼類.md "wikilink")、[貝類為食](../Page/貝類.md "wikilink")。

## 分類

虎鲨目下面只有一个**虎鲨科**（Heterodontidae，又名**異齒鯊科**、**異齒鮫科**），其下只有一個**虎鯊屬**（*Heterodontus*，又名**異齒鯊屬**、**異齒鮫屬**），包括现存的九个[种](../Page/种.md "wikilink")：

  - [佛氏虎鯊](../Page/佛氏虎鯊.md "wikilink")（*H.
    francisci*）<small>[Girard](../Page/Charles_Frédéric_Girard.md "wikilink"),
    1855</small>：又稱為佛氏異齒鯊。
  - [眶嵴虎鯊](../Page/眶嵴虎鯊.md "wikilink")（*H.
    galeatus*）<small>[Günther](../Page/Albert_Günther.md "wikilink"),
    1870</small>：又稱為眶嵴異齒鯊
  - [宽纹虎鲨](../Page/宽纹虎鲨.md "wikilink")（*H.
    japonicus*）<small>[Maclay](../Page/尼古拉·尼古拉耶维奇·米克卢霍-马克莱.md "wikilink")
    & [W. J. Macleay](../Page/William_John_Macleay.md "wikilink"),
    1884</small>：又稱為日本異齒鯊、日本異齒鮫。
  - [墨西哥虎鯊](../Page/墨西哥虎鯊.md "wikilink")（*H. mexicanus*）<small>[L. R.
    Taylor](../Page/Leighton_R._Taylor_Jr..md "wikilink") &
    [Castro-Aguirre](../Page/José_Luis_Castro-Aguirre.md "wikilink"),
    1972</small>：又稱為墨西哥異齒鯊。
  - [阿曼虎鲨](../Page/阿曼虎鲨.md "wikilink")（*H. omanensis*）<small>[Z. H.
    Baldwin](../Page/Zachary_Hayward_Baldwin.md "wikilink"),
    2005</small>：又稱為阿曼異齒鯊。
  - [澳大利亞虎鯊](../Page/澳大利亞虎鯊.md "wikilink")（*H.
    portusjacksoni*）<small>[F. A. A.
    Meyer](../Page/Friedrich_Albrecht_Anton_Meyer.md "wikilink"),
    1793</small>：又稱傑克遜異齒鯊、傑克遜異齒鮫。
  - [瓜氏虎鯊](../Page/瓜氏虎鯊.md "wikilink")（*H.
    quoyi*）<small>[Fréminville](../Page/Christophe_Paulin_de_la_Poix_de_Fréminville.md "wikilink"),
    1840</small>：又稱加拉巴哥異齒鯊
  - [白點虎鯊](../Page/白點虎鯊.md "wikilink")（*H. ramalheira*）<small>[J. L. B.
    Smith](../Page/James_Smith_\(ichthyologist\).md "wikilink"),
    1949</small>：又稱白點異齒鯊。
  - [狭纹虎鲨](../Page/狭纹虎鲨.md "wikilink")（*H. zebra*）<small>[J. E.
    Gray](../Page/约翰·爱德华·格雷.md "wikilink"), 1831</small>：又稱為斑紋異齒鯊、斑紋異齒鮫。

## 經濟利用

大型魚種，易經由食物链累積大量重金屬，會導致神經系統病變，不宜食用。 其體色多變化，性情尚溫和，亦是水族館常展示的魚種。

## 參見

  - 《[生者對死者無動於衷](../Page/生者對死者無動於衷.md "wikilink")》

## 参考文献

  - [FishBase](http://fishbase.sinica.edu.tw/)
  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[\*](../Category/虎鯊目.md "wikilink") [\*](../Category/鲨鱼.md "wikilink")