{{ 足球聯賽資料 | name = 黑山足球甲級聯賽 | another_name = Montenegrin First League |
image = | pixels = | country =  | confed =
[UEFA](../Page/歐洲足球協會聯盟.md "wikilink")（[歐洲](../Page/歐洲.md "wikilink")）
| first = 2006年 | teams = 10 隊 | relegation =
[黑山足球乙級聯賽](../Page/黑山足球乙級聯賽.md "wikilink")
| level = 第 1 級 | domestic_cup = [黑山盃](../Page/黑山盃.md "wikilink") |
international_cup = [歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")
[歐霸盃](../Page/歐足聯歐洲聯賽.md "wikilink") | season = 2017–18 | champions =
[蘇積斯卡](../Page/尼克希奇足球俱樂部.md "wikilink") | most_successful_club =
[布度洛斯](../Page/布杜克諾斯特足球俱樂部.md "wikilink")、
[蘇積斯卡](../Page/尼克希奇足球俱樂部.md "wikilink")
| no_of_titles = 各 3 次 | website = <http://fscg.co.me/> | current = }}

**黑山足球甲級聯賽**（[黑山語](../Page/黑山語.md "wikilink")：*Prva crnogorska fudbalska
liga*）是[黑山的頂級聯賽](../Page/黑山.md "wikilink")，由[黑山足球協會領導](../Page/黑山足球協會.md "wikilink")，現時黑山甲共有
10 隊球會角逐。

## 聯賽形式

聯賽冠軍可得到[歐洲聯賽冠軍盃第一圈外圍賽的參賽資格](../Page/歐洲聯賽冠軍盃.md "wikilink")，聯賽亞軍、聯賽季軍及[黑山盃冠軍將可獲得](../Page/黑山盃.md "wikilink")**[歐洲賽](../Page/歐洲賽.md "wikilink")**外圍賽的參賽資格\[1\]。至於聯賽排名第
12 名的球隊會直接降級至[黑山足球乙級聯賽](../Page/黑山足球乙級聯賽.md "wikilink")，而第 10 名及第 11
名球隊需與乙組第 2 名及第 3 名進行附加賽，以確定來屆甲組席位。

## 歷史

當黑山共和國還是[南斯拉夫的時候](../Page/南斯拉夫社會主義聯邦共和國.md "wikilink")，黑山的球會都是參與[南斯拉夫足球甲級聯賽的](../Page/南斯拉夫足球甲級聯賽.md "wikilink")。當南斯拉夫解體的時候，蒙特內哥羅的球會轉為參與[南斯拉夫聯盟共和國的頂級聯賽](../Page/南斯拉夫聯盟共和國.md "wikilink")，不過聯賽的名稱依舊不變。到2003年國家改名為[塞爾維亞和黑山的時候](../Page/塞爾維亞和黑山.md "wikilink")，聯賽也隨之而改名。當時塞黑甲有16間球會角逐，而蒙特內哥羅甲則作為塞蒙甲的乙級聯賽之一。當2006年蒙特內哥羅獨立後，黑山甲就成為了該國的頂級聯賽。

### 首季

2006–07球季是蒙特內哥羅甲成立後的首個賽季，雖然聯賽先前已在塞蒙甲時代存在，但隨著蒙特內哥羅獨立，和蒙特內哥羅足協成立，蒙特內哥羅甲脫離塞蒙甲成為了一個獨立聯賽，也是該國的頂級聯賽。

聯賽於2006年8月12日開始，到2007年5月26日結束。薛達成為了這屆的聯賽冠軍，也成了該國首間參與[歐洲聯賽冠軍杯的球會](../Page/歐洲聯賽冠軍杯.md "wikilink")。\[2\]

## 參賽球隊

2018–19年黑山足球甲級聯賽參賽隊伍共有 10 支。

| 中文名稱                                          | 英文名稱                   | 所在城市                                   | 上季成績     |
| --------------------------------------------- | ---------------------- | -------------------------------------- | -------- |
| [蘇積斯卡](../Page/尼克希奇足球俱樂部.md "wikilink")       | FK Sutjeska Niksic     | [尼克希奇](../Page/尼克希奇.md "wikilink")     | 第 1 位    |
| [布度洛斯](../Page/布杜克諾斯特足球俱樂部.md "wikilink")     | FK Buducnost Podgorica | [波德戈里察](../Page/波德戈里察.md "wikilink")   | 第 2 位    |
| [馬拿度斯](../Page/波德戈里察青年足球俱樂部.md "wikilink")    | FK Mladost Podgorica   | [波德戈里察](../Page/波德戈里察.md "wikilink")   | 第 3 位    |
| [加布積](../Page/格爾巴利足球俱樂部.md "wikilink")        | OFK Grbalj             | [科托爾](../Page/科托爾.md "wikilink")       | 第 4 位    |
| [路達普列夫利亞](../Page/普列夫利亞礦工足球俱樂部.md "wikilink") | FK Rudar Pljevlja      | [普列夫利亞](../Page/普列夫利亞.md "wikilink")   | 第 5 位    |
| [薛達](../Page/澤塔足球俱樂部.md "wikilink")           | FK Zeta                | [波德戈里察](../Page/波德戈里察.md "wikilink")   | 第 6 位    |
| [艾斯卡爾](../Page/艾斯卡爾足球會.md "wikilink")         | FK Iskra Danilovgrad   | [達尼洛夫格勒](../Page/達尼洛夫格勒.md "wikilink") | 第 7 位    |
| [帕度華](../Page/帕度華青年足球會.md "wikilink")         | OFK Petrovac           | [佩特羅瓦](../Page/佩特羅瓦.md "wikilink")     | 第 9 位    |
| [摩拿](../Page/莫爾納足球俱樂部.md "wikilink")          | FK Mornar              | [巴爾](../Page/巴爾.md "wikilink")         | 乙組，第 1 位 |
| [洛夫琴](../Page/洛夫琴足球會.md "wikilink")           | FK Lovćen              | [采蒂涅](../Page/采蒂涅.md "wikilink")       | 乙組，第 3 位 |

## 歷屆冠軍

以下為歷屆聯賽冠軍：\[3\]

## 球會奪冠次數

| 球會                                            | 冠軍次數 | 冠軍年份             |
| --------------------------------------------- | ---- | ---------------- |
| [布度洛斯](../Page/布杜克諾斯特足球俱樂部.md "wikilink")     | 3    | 2008, 2012, 2017 |
| [蘇積斯卡](../Page/尼克希奇足球俱樂部.md "wikilink")       | 3    | 2013, 2014, 2018 |
| [莫格倫](../Page/莫格倫足球會.md "wikilink")           | 2    | 2009, 2011       |
| [路達普列夫利亞](../Page/普列夫利亞礦工足球俱樂部.md "wikilink") | 2    | 2010, 2015       |
| [薛達](../Page/澤塔足球俱樂部.md "wikilink")           | 1    | 2007             |
| [馬拿度斯](../Page/波德戈里察青年足球俱樂部.md "wikilink")    | 1    | 2016             |

## 參考

## 外部連結

  - [Football Association of Montenegro - Official
    Site](https://web.archive.org/web/20160111044914/http://www.fscg.cg.yu/)

[it:Campionato montenegrino di
calcio](../Page/it:Campionato_montenegrino_di_calcio.md "wikilink")

[Montenegrin](../Category/歐洲各國足球聯賽.md "wikilink")
[欧](../Category/國家頂級足球聯賽.md "wikilink")
[Category:黑山足球](../Category/黑山足球.md "wikilink")

1.  [2007年至2008年歐洲賽事分配名額](https://web.archive.org/web/20070217073953/http://www.xs4all.nl/~kassiesa/bert/uefa/access2007.html)
2.  [RSSSF-Montenegro 2006/07](http://www.rsssf.com/tablesm/monteg07.html)
3.