[Bulla-Ferdinant.jpg](https://zh.wikipedia.org/wiki/File:Bulla-Ferdinant.jpg "fig:Bulla-Ferdinant.jpg")的葬禮中舉行的安魂彌撒，1914年見於一份俄國報紙。\]\]

**安魂彌撒**（）一詞源自[拉丁文](../Page/拉丁文.md "wikilink")（原意為「安息」），有以下涵义：

1.  指[天主教會為悼念逝者舉行的](../Page/天主教.md "wikilink")[彌撒](../Page/彌撒.md "wikilink")，拉丁文也稱為“”，現多稱為**追思彌撒**。這種彌撒除了用作[葬禮儀式](../Page/葬禮.md "wikilink")，也是每年11月2日的[諸靈節禮儀的一部分](../Page/諸靈節.md "wikilink")。天主教徒相信，為在[煉獄中的逝者舉行彌撒](../Page/煉獄.md "wikilink")，可縮短他們在煉獄的日子、令他們更早進入[天國](../Page/天國.md "wikilink")；但這個儀式並非必須。
2.  指在安魂彌撒中所用的詩歌，稱為**安魂彌撒曲**，簡稱**安魂曲**（），又稱**鎮魂曲**\[1\]。其所用的經文，有些也為一般彌撒所用，有些則專為安魂所用。
3.  也用於稱[正教會的追悼會](../Page/正教會.md "wikilink")，其儀式係為永眠者祈禱，因與天主教的Requiem相似而得名，但該用法鮮見於[西歐和](../Page/西歐.md "wikilink")[日本以外](../Page/日本.md "wikilink")。

## 安魂彌撒

Requiem
一詞源自[進堂詠](../Page/進堂詠.md "wikilink")（Introit）的首句：“”（主啊，請賜予他們永恆的安息，讓永恆的光照耀他們。）安息彌撒與其他彌撒不同，省去如歡讚歌（Alleluia）等愉快的經文，增加了末日經（）等經文。

## 安魂曲

早期的可以追溯到中世纪的[额我略平咏](../Page/额我略平咏.md "wikilink")。后来许多作曲家写过安魂曲，例如[亚历山德罗·斯卡拉蒂](../Page/亚历山德罗·斯卡拉蒂.md "wikilink")、[沃尔夫冈·阿马德乌斯·莫扎特](../Page/沃尔夫冈·阿马德乌斯·莫扎特.md "wikilink")、[加布里埃爾·佛瑞](../Page/加布里埃爾·佛瑞.md "wikilink")、[莫里斯·迪吕弗莱](../Page/莫里斯·迪吕弗莱.md "wikilink")。
《安魂曲》通常由下述几个[乐章组成](../Page/乐章.md "wikilink")：

  - [进堂咏](../Page/进堂咏.md "wikilink")（Introitus）: Requiem æternam dona
    eis, Domine（主啊，賜予他們永恆的安息）...
  - [垂憐經](../Page/垂憐經.md "wikilink")（Kyrie）: Kyrie, eleison（主，憐憫我們）...
  - [继抒咏](../Page/继抒咏.md "wikilink")（Sequentia）：Dies iræ（烈怒之日）...
  - [奉献經](../Page/奉献經.md "wikilink")（Offertorium）：Domine Jesu Christe,
    Rex gloriæ（主耶穌基督，榮耀之王）...
  - [聖三颂](../Page/聖三颂.md "wikilink")（Sanctus）和[降福經](../Page/降福經.md "wikilink")（Benedictus）
  - [羔羊颂](../Page/羔羊颂.md "wikilink")（Agnus Dei）：Agnus Dei, qui tollis
    peccata mundi（神之羔羊，承擔舉世罪孽之人）...
  - [领主颂](../Page/领主颂.md "wikilink")（Communio）：Lux æterna luceat eis,
    Domine（主啊，賜永恆之光照耀他們）...
  - [救主頌](../Page/救主頌.md "wikilink")（Libera Me）：Libera me, Domine, de
    morte æterna（主啊，救我於永恆之亡歿）...
  - [往天經](../Page/往天經.md "wikilink")（In Paradisum）：In paradisum deducant
    te Angeli（願天使領你前去天堂之樂園）...

後來「安魂曲」「鎮魂曲」之名稱逐漸見於其他形式的音樂作品，其中也不乏與宗教、儀式無關者。

## 參考資料

## 参见

  - [弥撒](../Page/弥撒.md "wikilink")
  - [追思会](../Page/追思会.md "wikilink")

## 外部連結

  - [安魂彌撒
    (加布里埃爾·佛瑞).](http://www.rtve.es/alacarta/videos/los-conciertos-de-la-2/conciertos-la2-20120225-0756-169/1332466/)
    RTVE交響樂團. Petri Sakari.
  - [安魂彌撒
    (沃尔夫冈·阿马德乌斯·莫扎特).](http://www.rtve.es/alacarta/videos/los-conciertos-de-la-2/conciertos-2-concierto-rtve-16/2519309/)
    RTVE交響樂團. Carlos Kalmar.
  - [安魂彌撒
    (安东宁·德沃夏克).](http://www.rtve.es/alacarta/videos/los-conciertos-de-la-2/conciertos-2-concierto-rtve-5/2258548/)
    RTVE交響樂團. Carlos Kalmar.
    [1](http://www.rtve.es/alacarta/videos/los-conciertos-de-la-2/conciertos-2-concierto-rtve-5/2300808/)

[Category:基督教禮儀](../Category/基督教禮儀.md "wikilink")
[Category:樂曲形式](../Category/樂曲形式.md "wikilink")
[Category:安魂曲](../Category/安魂曲.md "wikilink")
[Category:弥撒曲](../Category/弥撒曲.md "wikilink")

1.  「鎮魂」是[和製漢語](../Page/和製漢語.md "wikilink")，另外，日本有時會出現「」讀作「」（Rekuiemu，Requiem之音譯）。