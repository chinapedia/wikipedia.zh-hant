[TXMap-doton-ElPaso.png](https://zh.wikipedia.org/wiki/File:TXMap-doton-ElPaso.png "fig:TXMap-doton-ElPaso.png")
**艾尔帕索**（）是[美國](../Page/美國.md "wikilink")[德克薩斯州](../Page/德克薩斯州.md "wikilink")[艾尔帕索縣縣治](../Page/厄尔巴索縣_\(德克薩斯州\).md "wikilink")，位於德州極西部，隔[格兰德河与](../Page/格兰德河.md "wikilink")[墨西哥的](../Page/墨西哥.md "wikilink")[華雷斯城相望](../Page/華雷斯城.md "wikilink")。是该州第六大城、全国第十九大城市
（2006年估計人口為609,415\[1\]）。其都會區人口為736,310\[2\]。

1873年设市，具有浓厚的[西班牙文化色彩](../Page/西班牙文化.md "wikilink")。[埃爾帕索和](../Page/埃爾帕索.md "wikilink")[格蘭德河對岸的](../Page/格蘭德河.md "wikilink")[華雷斯城都在西班牙帝國](../Page/華雷斯城.md "wikilink")[皇家內陸大道上](../Page/皇家內陸大道.md "wikilink")。

得克萨斯大学艾尔帕索分校位于该城，该分校学术在德州大学系统七个分校中位居第三，仅次于奥斯汀和达拉斯。

## 歷史

## 地理

### 氣候

## 教育

## 經濟

## 交通

## 文化

### 旅遊

## 参考文献

[Category:得克萨斯州城市](../Category/得克萨斯州城市.md "wikilink")
[Category:德克薩斯州郡城](../Category/德克薩斯州郡城.md "wikilink")
[Category:美國河畔聚居地](../Category/美國河畔聚居地.md "wikilink")
[Category:美國經濟特區](../Category/美國經濟特區.md "wikilink")
[Category:美墨邊界](../Category/美墨邊界.md "wikilink")
[Category:1659年建立的聚居地](../Category/1659年建立的聚居地.md "wikilink")
[Category:1659年新西班牙建立](../Category/1659年新西班牙建立.md "wikilink")

1.  <http://www.census.gov/popest/cities/SUB-EST2006.html>
2.  <http://www.census.gov/population/www/estimates/metro_general/2006/CBSA-EST2006-01.csv>