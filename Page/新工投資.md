**新工投資有限公司**，()，[香港](../Page/香港.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")，原名禹銘投資有限公司，是一家[物業及](../Page/物業.md "wikilink")[證券](../Page/證券.md "wikilink")[投資公司](../Page/投資公司.md "wikilink")，大股東是(\#0373)[聯合集團有限公司](../Page/聯合集團.md "wikilink")(26.98%)，[馮永祥等](../Page/馮永祥.md "wikilink")。馮永祥是[董事局](../Page/董事局.md "wikilink")[非執行董事](../Page/非執行董事.md "wikilink")，[馮景禧的](../Page/馮景禧.md "wikilink")[次子](../Page/兒子.md "wikilink")，[香港特別行政區第一屆政府推選委員會工商及金融界別成員](../Page/香港特別行政區第一屆政府推選委員會.md "wikilink")。

公司註冊地及[總部在](../Page/總部.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[告士打道](../Page/告士打道.md "wikilink")[聯合鹿島大廈](../Page/聯合鹿島大廈.md "wikilink")。

[法律顧問為](../Page/法律顧問.md "wikilink")[胡關李羅律師行](../Page/胡關李羅律師行.md "wikilink")、[齊伯禮律師行](../Page/齊伯禮律師行.md "wikilink")。

禹銘投資有限公司前身為新鴻基香港工業有限公司，成立於1990年，1997年改名至2009年9月2日。

## 公司改名

在2009年9月2日，此[香港上市公司再改名](../Page/香港上市公司.md "wikilink")。現稱新工投資有限公司 (**SHK
Hong Kong Industries Limited**)。\[1\]因為此公司已經與馮氏家族及禹銘無關。\[2\]

2012年5月4日持有香港身份證的內地建築商劉軍或有關人士，豪斥11.5億元，成功搶購新工投資持有的前泉章居巨舖及毗鄰一舖位，並即以月租500萬元放租、位於波斯富街108至120號寶榮大樓地下A及B號舖、1及2樓及12樓S室及平台，面積23932方呎，成交呎價約4.8萬元

## 參見

  - [中華汽車有限公司](../Page/中華汽車有限公司.md "wikilink")
  - [灣仔道商場](../Page/灣仔道.md "wikilink")[Timeplus及](../Page/Timeplus.md "wikilink")[國泰88](../Page/國泰88.md "wikilink")
  - [新鴻基公司](../Page/新鴻基公司.md "wikilink")
  - [亞洲國際博覽館](../Page/亞洲國際博覽館.md "wikilink")
  - [廣平奈米](../Page/廣平奈米.md "wikilink")

## 外部參考

  - \[<http://www.ymi.com.hk/b5/index.php>? 新工投資有限公司 - 官方網址\]
  - [新工投資有限公司 -
    雅虎財經](https://web.archive.org/web/20070814062425/http://hk.biz.yahoo.com/p/hk/profile/0666.HK.html)

[Category:投資公司](../Category/投資公司.md "wikilink")
[Category:基金公司](../Category/基金公司.md "wikilink")

1.  [更改公司名稱通告](http://www.hkexnews.hk/listedco/listconews/sehk/20090708/LTN20090708464_C.pdf)
2.  [此公司(2009年)改名原因](http://www.hkexnews.hk/listedco/listconews/sehk/20090508/LTN20090508509_C.pdf)