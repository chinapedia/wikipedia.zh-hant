[La_Tour.jpg](https://zh.wikipedia.org/wiki/File:La_Tour.jpg "fig:La_Tour.jpg")'',
1642,
[Louvre](../Page/Louvre.md "wikilink")|thumb|right|300px|《圣若瑟》（1642年）藏于[卢浮宫](../Page/卢浮宫.md "wikilink")\]\]
**乔治·德·拉·图尔**（[法語](../Page/法語.md "wikilink")：，），[法国](../Page/法国.md "wikilink")[巴洛克时代](../Page/巴洛克绘画.md "wikilink")[画家](../Page/画家.md "wikilink")，生於法国[洛林](../Page/洛林.md "wikilink")，以繪畫燭光作為光源的晚景聞名，題材主要為宗教畫和風俗畫。

## 生平

拉圖爾，生於[默尔特-摩泽尔省南部的一个小镇](../Page/默尔特-摩泽尔省.md "wikilink")，父亲是面包师，母亲出身于一个小贵族家庭，全家有7个子女，他是次子。\[1\]

拉圖爾于1617年结婚，1620年他在[呂內維爾设立自己的画室](../Page/呂內維爾.md "wikilink")，主要描绘宗教画和风俗画，1638年被封为“御用画家”，也为洛林公爵作画，但当地的富有阶级是他主要的顾主，使他逐渐出名。从1639年到1642年，他不再出现在当地的记录中，可能这个时期他又外出旅行了，回来后，他卷入了当时洛林[方济各会的宗教复兴运动](../Page/方济各会.md "wikilink")，画了许多[宗教题材的作品](../Page/宗教.md "wikilink")，但明显的有世俗画的痕迹。\[2\]

1652年，他和全家都死于当时吕内维尔流行的传染病。

關於拉圖爾受教育情况，歷史并没有任何记载，但他可能到过[意大利和](../Page/意大利.md "wikilink")[荷兰](../Page/荷兰.md "wikilink")，因為他的画风类似意大利[卡拉瓦乔的](../Page/卡拉瓦乔.md "wikilink")[巴洛克](../Page/巴洛克.md "wikilink")[自然主义风格](../Page/自然主义.md "wikilink")，但也许他是从荷兰的乌得勒支画派或当时其他北方画派中学到的，因为批评家们常把他和荷兰画家[亨德里克·特尔·布吕根相提并论](../Page/亨德里克·特尔·布吕根.md "wikilink")。\[3\]

## 作品

[Georges_de_La_Tour_040.jpg](https://zh.wikipedia.org/wiki/File:Georges_de_La_Tour_040.jpg "fig:Georges_de_La_Tour_040.jpg")

拉圖爾的畫作特色在於：利用燭光來創造獨特的效果，構成充滿氣氛的情境。

### 基督在木匠的店裏

《[基督在木匠的店裏](../Page/基督在木匠的店裏.md "wikilink")》（Joseph the
Carpenter）是拉圖爾的代表作之一，是[宗教作品](../Page/宗教.md "wikilink")，通過燭光來營造出[耶穌的神聖感](../Page/耶穌.md "wikilink")。耶穌頭上沒有頂著[光環或飾物](../Page/光環.md "wikilink")，而是舉著唯一的光源蠟燭，燭光造成了彷彿有光自他的臉龐放射出來的錯覺。

### 抹大拉瑪利亞的懺悔

《[抹大拉瑪利亞的懺悔](../Page/抹大拉瑪利亞的懺悔.md "wikilink")》（The Penitent
Magdalen），拉圖爾創造了一個迥異於一般人對[瑪利亞的印象](../Page/瑪利亞.md "wikilink")。畫作描繪曾當過妓女的瑪利亞在為過去[罪惡閉關苦修](../Page/罪惡.md "wikilink")。膝上的[骷髏提醒自己要正視](../Page/骷髏.md "wikilink")[死亡](../Page/死亡.md "wikilink")。桌子上還有一根[鞭子和一個](../Page/鞭子.md "wikilink")[十字架](../Page/十字架.md "wikilink")，暗示其苦修，[蠟燭火焰靜靜映照在在她憂鬱的臉龐上](../Page/蠟燭.md "wikilink")。

### 油燈前的瑪利亞

《[油燈前的瑪利亞](../Page/油燈前的瑪利亞.md "wikilink")》（Repenting
Magdalene），拉圖爾畫了很多關於瑪利亞的畫。所有瑪利亞的畫作中以拉圖爾的畫作最樸素。他創造了一個流露哀怨之情的樸素形象。不像其他畫作看起來充滿世俗的宴樂、華麗的衣飾，只有清醒、沉靜、悲哀的反省。

## 風格

他的早期作品风格受到卡拉瓦乔的影响，亦受到了荷蘭畫家Hendrick Terbrugghen 和 Gerrit van Honthorst
的影響。，但其风俗画明显的和乌得勒支画派风格相近。他最著名的风格是运用好像壁龛中的光线效果，不管同的宗教画还是风俗画都有这种效果。他的夜景通常只用一盞燭光為光源，因此強烈的表現出亮度和陰影的差距。

从1640年开始，他的创作进入第二阶段，运用明暗对比，三角形稳重的[构图布置](../Page/构图.md "wikilink")，形式简洁，向简单和静止稳定的方向发展。\[4\]

他晚年的作品演變出一種[人像風格](../Page/人像.md "wikilink")，畫裡的[人體都約減成最單純的](../Page/人體.md "wikilink")[幾何造型](../Page/幾何.md "wikilink")，並且安置得詳靜平和。這是法國[古典主義的表現](../Page/古典主義.md "wikilink")。

他经常对同一个主题创作多幅作品，他的儿子是他的学生，两人的作品很难区分。他逝世后，很长时间已经被人们忘记，直到1915年才重新被发掘出来，1935年在[巴黎举办了他的作品画展](../Page/巴黎.md "wikilink")，引起广泛的兴趣，在20世纪，他的作品价值急剧攀升，但是作品的真伪经常引起争议。

## 畫作展示

<File:Georges> de La Tour 035.jpg|掷骰子的人 <File:Georges> de La Tour
016.jpg|算命先生 <File:Georges> de La Tour - Rixe de musiciens - Google Art
Project.jpg|争吵 <File:Georges> de La Tour 003.jpg|圣伊勒内看护圣塞巴斯蒂安
<File:Georges> de La Tour 011.jpg|圣热罗姆 <File:Georges> de La Tour
014.jpg|圣热罗姆 <File:Georges> de La Tour 009.jpg|耐心的马大拉 <File:Georges> de
La Tour - The Cheat with the Ace of Clubs - Google Art
Project.jpg|方塊A的作弊者 <File:Georges> de La Tour - Newlyborn
infant - Musée des Beaux-Arts de Rennes.jpg|圣婴 <File:Georges> de La Tour
022.jpg|圣若瑟的梦 <File:Georges> de La Tour 056.jpg|老人像 <File:Georges> de La
Tour 001.jpg|圣诞

## 收藏拉·图尔作品的博物馆

  - [法国](../Page/法国.md "wikilink")
      - 南锡美术馆，原洛林的首府，藏品最多
      - [巴黎](../Page/巴黎.md "wikilink")，[卢浮宫](../Page/卢浮宫.md "wikilink")，以及其他省的博物馆
  - [英国](../Page/英国.md "wikilink")
      - 普来斯顿宫博物馆，收藏《掷色子的人》
  - [美国](../Page/美国.md "wikilink")
      - [洛杉矶县立艺术博物馆](../Page/洛杉矶县立艺术博物馆.md "wikilink")
      - 洛杉矶，[盖蒂中心](../Page/盖蒂中心.md "wikilink")
      - [纽约](../Page/纽约.md "wikilink")，[大都会博物馆](../Page/大都会博物馆.md "wikilink")
      - 纽约，[弗里克收藏](../Page/弗里克收藏.md "wikilink")
      - [旧金山](../Page/旧金山.md "wikilink")，[笛洋美术馆](../Page/笛洋美术馆.md "wikilink")
      - [华盛顿特区](../Page/华盛顿特区.md "wikilink")，[國家藝廊](../Page/國家藝廊.md "wikilink")

## 参考文献

## 外部链接

  - [美国德克萨斯州坎贝尔博物馆中的拉·图尔作品](https://web.archive.org/web/20070210194756/http://www.kimbellart.org/database/index.cfm?detail=yes&ID=AP%201993.04)

[Category:法国巴洛克画家](../Category/法国巴洛克画家.md "wikilink")

1.  [1](http://etd.lsu.edu/docs/available/etd-04112007-165514/unrestricted/Thesis.pdf)
    Crissy Bergeron Thesis - page 7, and note 4, quoting Thuillier p.19

2.
3.  Anthony Blunt, "Art and Architecture in France, 1500–1700", 1953,
    Penguin

4.