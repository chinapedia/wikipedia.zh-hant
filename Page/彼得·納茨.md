**彼得·納茨**（Pieter
Nuyts，），一譯**彼得·奴易茲**，[台灣荷西殖民時期第](../Page/台灣荷西殖民時期.md "wikilink")3任[台灣長官](../Page/台灣長官.md "wikilink")。同時也是[濱田彌兵衛事件的當事者之一](../Page/濱田彌兵衛事件.md "wikilink")。

## 生平

1627年出任[荷蘭東印度公司的](../Page/荷蘭東印度公司.md "wikilink")[台灣長官](../Page/台灣長官.md "wikilink")，同年7月赴[日本向](../Page/日本.md "wikilink")[江戶幕府和談](../Page/江戶幕府.md "wikilink")，但因素與[荷蘭人有恩怨的日商](../Page/荷蘭人.md "wikilink")[濱田彌兵衛作梗下](../Page/濱田彌兵衛事件.md "wikilink")，和談失敗，12月3日灰頭土臉的回到[台灣](../Page/台灣.md "wikilink")。

因此，奴易茲對濱田深感忿恨，便於1628年5月27日濱田率船來台時將其拘捕，雖不久將其釋放但卻扣留其武器與[軍火](../Page/軍火.md "wikilink")。忿恨的濱田即於6月29日佯稱求見，突然率十餘名[日本人闖進他住處並挾持奴易茲本人及其子](../Page/日本人.md "wikilink")。事後與[日本達成協議](../Page/日本.md "wikilink")，與濱田離[台赴](../Page/台灣.md "wikilink")[日](../Page/日本.md "wikilink")。

但至[日本後](../Page/日本.md "wikilink")，日方竟背約將[荷蘭人質與船員下獄](../Page/荷蘭.md "wikilink")，並封閉荷蘭在日本[平戶的商館](../Page/平戶.md "wikilink")，[東印度公司](../Page/東印度公司.md "wikilink")[巴達維亞方面感到事態嚴重](../Page/巴達維亞.md "wikilink")，遂於1629年派[普特曼斯接任台灣長官](../Page/漢斯·普特曼斯.md "wikilink")、將他遣回，繼而宣判奴易茲兩年[有期徒刑](../Page/有期徒刑.md "wikilink")，以討好[江戶幕府](../Page/江戶幕府.md "wikilink")。

1632年，奴易茲甚至被引渡至日本監禁，4年後的1636年，才被荷蘭人以一座796斤的青銅燭台將其救回，但其子已病死於日本。

1637年被公司解除各種職務，遣返[荷蘭](../Page/荷蘭.md "wikilink")。

1655年逝世，下葬於今荷蘭[澤蘭省](../Page/澤蘭省.md "wikilink")[許爾斯特](../Page/許爾斯特.md "wikilink")。

## 內部連結

  - [濱田彌兵衛事件](../Page/濱田彌兵衛事件.md "wikilink")
  - [荷蘭聯合東印度公司](../Page/荷蘭聯合東印度公司.md "wikilink")

## 參考文獻

  - , (2007) How Taiwan Became Chinese, Dutch, Spanish, and Han
    Colonization in the Seventeenth Century, New York Volledige weergave
    via Gutenberg Project

  - , (2003) Bull in a China Shop: Pieter Nuyts in China and Japan
    (1627-1636), In: Around and About Dutch Formosa. Taipei

  - , (2005) Olifant in de porseleinkast, Pieter Nuyts (1598-1655) en
    zijn avonturen in het Verre Oosten, In: Parmentier, J. (Ed.), (2005)
    Noord-Zuid in Oost-Indisch perspectief. blz. 117-129

  - ; (1998) The Formosan Encounter: Notes on Formosa's Aboriginal
    Society: A Selection of Documents from Dutch Archival Sources,
    1623–1635, 4 Volumes. Taipei

  - , (1903). The Island of Formosa: Past and Present. Londen

  - , (2005). Discovering the Great South Land. Rosenberg

  - , (1899) Het aandeel der Nederlanders in de ontdekking van Australië
    1606-1765 Engelstalige versie digitaal beschikbaar via Gutenberg
    Project

  - , (2005) [Nuyts, Pieter
    (1598-1655)](http://www.adb.online.anu.edu.au/biogs/AS10380b.htm)
    Australian Dictionary of Biography, National Centre of Biography,
    Australian National University

  - . (2003) Interracial intimacy in Japan: western men and Japanese
    women, 1543-1900.

  - , (1853) [Stukken Betrekkelijk Pieter Nuyts, Gouverneur van
    Taqueran 1631-1634.](http://books.google.nl/books?id=rYQ6AAAAcAAJ&pg=PA184&dq=%22Stukken+Betrekkelijk+Pieter+Nuyts%22+Leupe&hl=nl#v=onepage&q=%22Stukken%20Betrekkelijk%20Pieter%20Nuyts%22%20Leupe&f=false)
    In: Kronijk van het Historisch Genootschap gevestigd te Utrecht
    (1853), blz 184 en verder Volledige weergave via Google Books

  - , (2006) [The Dutch down
    under, 1606-2006](http://books.google.com.hk/books?id=K8pyAAAAMAAJ&q=Francois+Thijssen+Nuyts&dq=Francois+Thijssen+Nuyts&hl=en&sa=X&ei=FQ2wUNPhLsWtiAfclIDAAw&ved=0CDcQ6AEwADgK)
    blz. 14 + 30 Fragmentarische weergave via Google Books

  - , (1947) [Geschiedenis van het Katholicisme in Noord-Nederland in
    de 16e en de 17e
    eeuw](http://www.dbnl.org/tekst/rogi002gesc01_01/rogi002gesc01_01_0013.php#1632)
    Volume 2, blz. 787 Via Digitale Bibliotheek voor de Nederlandse
    Letteren en Google Books

  - , (1993) Statecraft and Political Economy on the Taiwan Frontier:
    1600–1800. Taipei

  - , (1976) Nederlanders ontdekken Australië. Scheepsarcheologische
    vondsten op het Zuidland. Bussum

[N](../Page/category:台灣荷西殖民時期人物.md "wikilink")
[N](../Page/category:1598年出生.md "wikilink")
[N](../Page/category:1655年逝世.md "wikilink")
[category:台灣長官](../Page/category:台灣長官.md "wikilink")

[Category:在台灣的荷蘭人](../Category/在台灣的荷蘭人.md "wikilink")
[N](../Category/泽兰省人.md "wikilink")