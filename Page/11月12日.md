**11月12日**是[阳历一年中的第](../Page/阳历.md "wikilink")316天（[闰年第](../Page/闰年.md "wikilink")317天），离全年的结束还有49天。

## 大事记

### 8世紀

  - [725年](../Page/725年.md "wikilink")：[唐朝天文学家僧](../Page/唐朝.md "wikilink")[一行与](../Page/一行.md "wikilink")[梁令瓒研制成功水运](../Page/梁令瓒.md "wikilink")[浑天仪](../Page/浑天仪.md "wikilink")。

### 10世紀

  - [936年](../Page/936年.md "wikilink")：[契丹册](../Page/契丹.md "wikilink")[石敬瑭为大晋皇帝](../Page/石敬瑭.md "wikilink")，晋割[燕云十六州于契丹](../Page/燕云十六州.md "wikilink")。

### 11世紀

  - [1028年](../Page/1028年.md "wikilink")：[君士坦丁八世安排讓](../Page/君士坦丁八世.md "wikilink")[羅曼努斯三世與](../Page/羅曼努斯三世.md "wikilink")[佐伊女皇](../Page/佐伊女皇.md "wikilink")（圖）結婚，不久後兩者共同成為[拜占庭皇帝](../Page/拜占庭皇帝.md "wikilink")。

[EmpZoe.jpg](https://zh.wikipedia.org/wiki/File:EmpZoe.jpg "fig:EmpZoe.jpg")
佐伊女皇

### 19世紀

  - [1893年](../Page/1893年.md "wikilink")：[英属印度和](../Page/英属印度.md "wikilink")[阿富汗签订协议](../Page/阿富汗.md "wikilink")，将[杜兰德线确立为两国的](../Page/杜兰德线.md "wikilink")[边界](../Page/边界.md "wikilink")。

### 20世紀

  - [1912年](../Page/1912年.md "wikilink"):
    救援團隊在[南極洲](../Page/南極洲.md "wikilink")[羅斯冰架附近發現](../Page/羅斯冰架.md "wikilink")[英國](../Page/英國.md "wikilink")[探險家](../Page/探險家.md "wikilink")[羅伯特·史考特與其夥伴的遺體](../Page/羅伯特·史考特.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：[奥地利成为共和国](../Page/奥地利.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：在[德国妇女获得选举权和被选举权](../Page/德国.md "wikilink")。
  - [1920年](../Page/1920年.md "wikilink")：[義大利與](../Page/義大利.md "wikilink")[塞爾維亞人、克羅埃西亞人和斯洛維尼亞人王國簽訂](../Page/塞爾維亞人、克羅埃西亞人和斯洛維尼亞人王國.md "wikilink")《[拉帕羅條約](../Page/拉帕羅條約.md "wikilink")》。
  - [1921年](../Page/1921年.md "wikilink")：英、美、日、法、意、中、荷、葡、比九国在[美国](../Page/美國.md "wikilink")[华盛顿举行](../Page/华盛顿哥伦比亚特区.md "wikilink")“[华盛顿会议](../Page/华盛顿会议.md "wikilink")”。
  - [1927年](../Page/1927年.md "wikilink")：[苏联共产党领袖](../Page/苏联共产党.md "wikilink")[列夫·托洛茨基因反对](../Page/列夫·托洛茨基.md "wikilink")[斯大林的政策而被开除出苏联共产党](../Page/斯大林.md "wikilink")。
  - [1933年](../Page/1933年.md "wikilink")：沙比提大毛拉领导下建立了东突厥斯坦伊斯兰共和国，
    首都为喀什葛尔。
  - [1936年](../Page/1936年.md "wikilink")：位于[美國](../Page/美國.md "wikilink")[旧金山湾](../Page/旧金山湾.md "wikilink")，连结[旧金山和](../Page/旧金山.md "wikilink")[奥克兰的](../Page/奥克兰_\(加利福尼亚州\).md "wikilink")[舊金山-奧克蘭海灣大橋正式通车](../Page/舊金山-奧克蘭海灣大橋.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink")：在新疆伊宁革命军宣布东突厥斯坦共和国的成立， 伊利汗任总统。
  - [1937年](../Page/1937年.md "wikilink")：[淞沪会战结束](../Page/淞沪会战.md "wikilink")。
  - [1938年](../Page/1938年.md "wikilink")：[湖南](../Page/湖南.md "wikilink")[长沙](../Page/长沙.md "wikilink")[文夕大火](../Page/文夕大火.md "wikilink")，成为“二战”期间世界上损失最为惨重的城市之一。
  - [1942年](../Page/1942年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：在[太平洋战争中](../Page/太平洋战争.md "wikilink"),發起[反攻行動的](../Page/反攻行動.md "wikilink")[同盟國軍隊與](../Page/同盟國.md "wikilink")[大日本帝國海軍在](../Page/大日本帝國海軍.md "wikilink")[索羅門群島展開為期數個月的](../Page/索羅門群島.md "wikilink")[瓜達康納爾海戰](../Page/瓜達康納爾海戰.md "wikilink")。
  - [1944年](../Page/1944年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：[英國皇家空軍](../Page/英國皇家空軍.md "wikilink")\]成功在[挪威附近海域擊沉](../Page/挪威.md "wikilink")[納粹德國海軍的](../Page/納粹德國海軍.md "wikilink")[鐵必制號戰艦](../Page/鐵必制號戰艦.md "wikilink")，造成1000多人死亡。
  - [1947年](../Page/1947年.md "wikilink")：[台湾民主自治同盟在](../Page/台湾民主自治同盟.md "wikilink")[香港成立](../Page/香港.md "wikilink")。
  - 1947年：[中国人民解放军攻克](../Page/中国人民解放军.md "wikilink")[石家庄](../Page/石家庄.md "wikilink")（当时名为石门），建立第一个以城市为中心的政权。
  - [1948年](../Page/1948年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")[日本](../Page/日本.md "wikilink")[甲级战犯](../Page/甲级战犯.md "wikilink")[东条英机被判](../Page/东条英机.md "wikilink")[死刑](../Page/死刑.md "wikilink")。
  - [1974年](../Page/1974年.md "wikilink")：[香港大毒梟](../Page/香港.md "wikilink")[吳錫豪被](../Page/吳錫豪.md "wikilink")[警方拘捕](../Page/香港警務處.md "wikilink")。
  - [1980年開始](../Page/1980年.md "wikilink")：[國立中山大學校慶日](../Page/國立中山大學.md "wikilink")（[孫中山誕生日](../Page/孫中山.md "wikilink")）。
  - [1990年](../Page/1990年.md "wikilink")：[日皇](../Page/日本天皇.md "wikilink")[明仁舉行登基大典](../Page/明仁.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[香港賽馬會跑馬地會所啟用](../Page/香港賽馬會跑馬地會所.md "wikilink")。
  - [1996年](../Page/1996年.md "wikilink")：[沙地阿拉伯航空763號班機與哈薩克斯坦航空](../Page/沙地阿拉伯航空763號班機.md "wikilink")1907號班機在[印度首都](../Page/印度.md "wikilink")[新德里附近的](../Page/新德里.md "wikilink")[哈里亞納邦](../Page/哈里亚纳邦.md "wikilink")[查基達里上空相撞](../Page/恰尔基达德里.md "wikilink")。兩航機上合共349人全部罹難，是航空史上最嚴重的空中相撞空難。

### 21世紀

  - [2001年](../Page/2001年.md "wikilink")：一架由[甘迺迪國際機場起飛的](../Page/甘迺迪國際機場.md "wikilink")[美國航空587號班機](../Page/美國航空587號班機.md "wikilink")，在[紐約市](../Page/纽约市.md "wikilink")[皇后區墜毀](../Page/皇后區.md "wikilink")，機上260人全部罹難。
  - 2001年：在[阿富汗战争中](../Page/阿富汗戰爭_\(2001年\).md "wikilink")，[阿富汗](../Page/阿富汗.md "wikilink")[北方联盟的军队击败](../Page/北方聯盟_\(阿富汗\).md "wikilink")[塔利班](../Page/塔利班.md "wikilink")，进入首都[喀布尔](../Page/喀布尔.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")：服務[香港](../Page/香港.md "wikilink")48年，座落[愛丁堡廣場的](../Page/愛丁堡廣場.md "wikilink")[中環天星碼頭作第三度搬遷](../Page/愛丁堡廣場碼頭.md "wikilink")，遷往[國際金融中心二期對開](../Page/國際金融中心_\(香港\).md "wikilink")，並成為[中環碼頭的組成部份](../Page/中環碼頭.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：美国康涅狄格州成为全美第二个将[同性婚姻合法化的州份](../Page/同性婚姻.md "wikilink")。
  - 2010年：[第十六届亚洲运动会在](../Page/第十六届亚洲运动会.md "wikilink")[中国](../Page/中国.md "wikilink")[广州举行开幕式](../Page/广州.md "wikilink")。
  - 2014年：[南韓](../Page/南韓.md "wikilink")[女子團體](../Page/女子團體.md "wikilink")
    [Lovelyz以](../Page/Lovelyz.md "wikilink")[Candy Jelly
    Love一曲出道](../Page/Candy_Jelly_Love.md "wikilink")。

## 出生

[John_William_Strutt.jpg](https://zh.wikipedia.org/wiki/File:John_William_Strutt.jpg "fig:John_William_Strutt.jpg")\]\]

  - [1528年](../Page/1528年.md "wikilink")：[戚繼光](../Page/戚繼光.md "wikilink")，[明代抗倭將領](../Page/明朝.md "wikilink")、[軍事家](../Page/軍事家.md "wikilink")（逝於[1588年](../Page/1588年.md "wikilink")）
  - [1729年](../Page/1729年.md "wikilink")：[布干維爾](../Page/布干維爾.md "wikilink")，[法國探險家](../Page/法國.md "wikilink")（逝於[1811年](../Page/1811年.md "wikilink")）
  - 1755年：**格哈德·约翰·大卫·冯·沙恩霍斯特，普鲁士将军，伯爵，军事改革家（逝於1813年6月28日）**
  - [1833年](../Page/1833年.md "wikilink")：[鮑羅定](../Page/鮑羅定.md "wikilink")，俄國作曲家、化學家（逝於[1887年](../Page/1887年.md "wikilink")）
  - [1840年](../Page/1840年.md "wikilink")：[羅丹](../Page/羅丹.md "wikilink")，法國雕塑家（逝於[1917年](../Page/1917年.md "wikilink")）
  - [1842年](../Page/1842年.md "wikilink")：[瑞利](../Page/瑞利.md "wikilink")，[英國物理學家](../Page/英國.md "wikilink")，[1904年](../Page/1904年.md "wikilink")[諾貝爾物理學獎得主](../Page/諾貝爾物理學獎.md "wikilink")（逝於[1919年](../Page/1919年.md "wikilink")）
  - [1848年](../Page/1848年.md "wikilink")：[李蓮英](../Page/李蓮英.md "wikilink")，晚[清太監](../Page/清.md "wikilink")（逝於[1911年](../Page/1911年.md "wikilink")[3月4日](../Page/3月4日.md "wikilink")）
  - [1866年](../Page/1866年.md "wikilink")：[孫中山](../Page/孫中山.md "wikilink")，[中國近代民主革命家](../Page/中國.md "wikilink")、政治家，[中華民國國父](../Page/中華民國.md "wikilink")，[中國國民黨創黨人](../Page/中國國民黨.md "wikilink")（逝於[1925年](../Page/1925年.md "wikilink")）
  - [1898年](../Page/1898年.md "wikilink")：[黃君璧](../Page/黃君璧.md "wikilink")，台灣畫壇渡海三家之一（逝於[1994年](../Page/1994年.md "wikilink")）
  - [1910年](../Page/1910年.md "wikilink")：[華羅庚](../Page/華羅庚.md "wikilink")，中國[數學家](../Page/數學家.md "wikilink")（逝於[1985年](../Page/1985年.md "wikilink")）
  - [1921年](../Page/1921年.md "wikilink")：[曾灶財](../Page/曾灶財.md "wikilink")，[香港塗鴉者](../Page/香港.md "wikilink")（逝於[2007年](../Page/2007年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[格雷斯·凱利](../Page/格雷斯·凱利.md "wikilink")，美國女演員、[摩納哥大公妃](../Page/摩納哥.md "wikilink")（逝於[1982年](../Page/1982年.md "wikilink")）
  - [1948年](../Page/1948年.md "wikilink")：[銀河萬丈](../Page/銀河萬丈.md "wikilink")，[日本聲優](../Page/日本配音員.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")：[田中秀幸](../Page/田中秀幸.md "wikilink")，[日本聲優](../Page/日本配音員.md "wikilink")
  - 1950年：[李旺陽](../Page/李旺陽.md "wikilink")，[中國工人運動領袖](../Page/中國.md "wikilink")（逝於[2012年](../Page/2012年.md "wikilink")）
  - [1952年](../Page/1952年.md "wikilink")：[劉文正](../Page/劉文正.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[陳秋霞](../Page/陳秋霞.md "wikilink")，[香港](../Page/香港.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1959年](../Page/1959年.md "wikilink")：[佐橋俊彥](../Page/佐橋俊彥.md "wikilink")，[日本作曲家](../Page/日本.md "wikilink")
  - [1959年](../Page/1959年.md "wikilink")：[劉雪華](../Page/劉雪華.md "wikilink")，台灣演員
  - [1964年](../Page/1964年.md "wikilink")：[王光輝](../Page/王光輝_\(棒球選手\).md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球教練](../Page/棒球.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[久川綾](../Page/久川綾.md "wikilink")，日本女性聲優
  - [1971年](../Page/1971年.md "wikilink")：[胡蓓蔚](../Page/胡蓓蔚.md "wikilink")，香港歌手及主持人
  - [1971年](../Page/1971年.md "wikilink")：[陳光誠](../Page/陳光誠.md "wikilink")，中國維權分子
  - [1973年](../Page/1973年.md "wikilink")：[-{zh-tw:朗妲·米契;zh-hk:莉達·美雪;zh-cn:拉妲·米契尔;}-](../Page/朗妲·米契.md "wikilink")，[澳洲女演員](../Page/澳洲.md "wikilink")
  - [1976年](../Page/1976年.md "wikilink")：[許閔嵐](../Page/許閔嵐.md "wikilink")，台灣棒球教練
  - [1978年](../Page/1978年.md "wikilink")：[莎敏·奥巴伊德-奇諾伊](../Page/莎敏·奥巴伊德-奇諾伊.md "wikilink")，[巴基斯坦女导演](../Page/巴基斯坦.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[-{zh-hk:賴恩·高斯寧;zh-tw:雷恩·葛斯林;zh-cn:瑞恩·高斯林;}-](../Page/瑞恩·高斯林.md "wikilink")，[加拿大演員](../Page/加拿大.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[陳意涵](../Page/陳意涵.md "wikilink")，台湾女演员
  - [1982年](../Page/1982年.md "wikilink")：[夏如芝](../Page/夏如芝.md "wikilink")，台湾女演员
  - [1982年](../Page/1982年.md "wikilink")：[-{zh-cn:安妮·海瑟薇; zh-tw:安·海瑟薇;
    zh-hk:安妮·夏菲維;}-](../Page/安妮·海瑟薇_\(演員\).md "wikilink")，美國女演員
  - [1983年](../Page/1983年.md "wikilink")：[吳嘉星](../Page/吳嘉星.md "wikilink")，[香港](../Page/香港.md "wikilink")[亞洲電視女演員](../Page/亞洲電視.md "wikilink")，[2007年](../Page/2007年.md "wikilink")[亞洲小姐競選友誼小姐](../Page/亞洲小姐競選.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[周柏豪](../Page/周柏豪.md "wikilink")，[香港男歌手](../Page/香港.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[Dara](../Page/Dara.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[2NE1前成員](../Page/2NE1.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[關令翹](../Page/關令翹.md "wikilink")，香港配音員
  - [1985年](../Page/1985年.md "wikilink")：[鄭多英](../Page/鄭多英.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[蔡旻佑](../Page/蔡旻佑.md "wikilink")，台灣歌手
  - [1988年](../Page/1988年.md "wikilink")：[陳以桐](../Page/陳以桐.md "wikilink")（[Jason
    Chen](../Page/Jason_Chen.md "wikilink")） ,
    [台灣裔](../Page/台灣裔.md "wikilink")[美國歌手](../Page/美國.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[崔炳燦](../Page/崔炳燦.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[VICTON成員](../Page/VICTON.md "wikilink")
  - [1999年](../Page/1999年.md "wikilink")：[磪有情](../Page/磪有情.md "wikilink")，[韩国女子偶像團體](../Page/韩国.md "wikilink")[I.O.I前成員](../Page/I.O.I.md "wikilink")，現為[Weki
    Meki成員](../Page/Weki_Meki.md "wikilink")

## 逝世

  - [607年](../Page/607年.md "wikilink")：[博義三世](../Page/博義三世.md "wikilink")，[教宗](../Page/教宗.md "wikilink")
  - [1035年](../Page/1035年.md "wikilink")：[克努特大帝](../Page/克努特大帝.md "wikilink")，丹麦和英国国王（[995年出生](../Page/995年.md "wikilink")）
  - [1595年](../Page/1595年.md "wikilink")：[约翰·霍金斯](../Page/约翰·霍金斯.md "wikilink")，[英国](../Page/英国.md "wikilink")[16世纪著名的航海家](../Page/16世纪.md "wikilink")、[海盗](../Page/海盗.md "wikilink")、奴隶贩子（[1532年出生](../Page/1532年.md "wikilink")）
  - [1892年](../Page/1892年.md "wikilink")：[巴哈欧拉](../Page/巴哈欧拉.md "wikilink")，[巴哈伊信仰的创立者](../Page/巴哈伊信仰.md "wikilink")（[1817年出生](../Page/1817年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[尢列](../Page/尢列.md "wikilink")，[中華民國](../Page/中華民國.md "wikilink")[革命黨人](../Page/革命黨.md "wikilink")，[孫中山總統府顧問](../Page/孫中山.md "wikilink")（[1866年出生](../Page/1866年.md "wikilink")）
  - [1939年](../Page/1939年.md "wikilink")：[白求恩](../Page/白求恩.md "wikilink")，[加拿大胸外科医师](../Page/加拿大.md "wikilink")（[1890年出生](../Page/1890年.md "wikilink")）
  - [1969年](../Page/1969年.md "wikilink")：[刘少奇](../Page/刘少奇.md "wikilink")，[中共中央副主席](../Page/中国共产党中央委员会.md "wikilink")、[中华人民共和国主席](../Page/中华人民共和国主席.md "wikilink")（[1898年出生](../Page/1898年.md "wikilink")）
  - [1989年](../Page/1989年.md "wikilink")：[茅以升](../Page/茅以升.md "wikilink")，中國桥梁专家（[1896年出生](../Page/1896年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[张雨生](../Page/张雨生.md "wikilink")，[台灣音樂人](../Page/台灣.md "wikilink")、歌手（[1966年出生](../Page/1966年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[黎礎寧](../Page/黎礎寧.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")，[超級星光大道獲得第三名](../Page/超級星光大道.md "wikilink")（[1984年出生](../Page/1984年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[約翰·塔文納](../Page/約翰·塔文納.md "wikilink")，英國作曲家（[1944年出生](../Page/1944年.md "wikilink")）
  - 2013年：，俄羅斯太空人（1944年出生）
  - [2016年](../Page/2016年.md "wikilink")：[余旭](../Page/余旭.md "wikilink")，中国女子飞行员（1986年出生）
  - [2018年](../Page/2018年.md "wikilink")：[斯坦·李](../Page/斯坦·李.md "wikilink")，美国[漫画家](../Page/漫画家.md "wikilink")，[漫威漫画创办人](../Page/漫威漫画.md "wikilink")（[1922年出生](../Page/1922年.md "wikilink")）

## 节假日和习俗

  - ：[國父誕辰紀念日](../Page/國父誕辰紀念日.md "wikilink")

  - ：[醫師節](../Page/醫師節.md "wikilink")

  - ：[中華文化復興節](../Page/中華文化復興節.md "wikilink")

  - [世界肺炎日](../Page/世界肺炎日.md "wikilink")