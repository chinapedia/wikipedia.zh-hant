**芒康县**()位于[中華人民共和國](../Page/中華人民共和國.md "wikilink")[西藏自治區东部](../Page/西藏自治區.md "wikilink")，海拔约3880米，毗邻四川与云南，是西藏的东大门。是[昌都市的一個](../Page/昌都市.md "wikilink")[縣](../Page/縣.md "wikilink")。地理位置[东经](../Page/东经.md "wikilink")98°00'-99°05'，[北纬](../Page/北纬.md "wikilink")28°37'-30°20'。与之接壤的县市有：东边隔[金沙江](../Page/金沙江.md "wikilink")[四川省](../Page/四川省.md "wikilink")[巴塘县](../Page/巴塘县.md "wikilink")，南边[云南省](../Page/云南省.md "wikilink")[德钦县](../Page/德钦县.md "wikilink")，西边跨[澜沧江](../Page/澜沧江.md "wikilink")、[左贡县](../Page/左贡县.md "wikilink")，北边[贡觉县](../Page/贡觉县.md "wikilink")、[察雅县](../Page/察雅县.md "wikilink")。县城驻嘎托镇。面积11654平方公里，2011年底第六次人口普查，总人口8.1万人，在西藏仅次于拉萨市城关区、日喀则市、昌都县和那曲县4个县市区。

[清末曾设宁静县](../Page/清.md "wikilink")，后为[芒康宗](../Page/芒康宗.md "wikilink")（即江卡宗），1960年與[盐井宗合併](../Page/盐井宗.md "wikilink")，並改名[宁静县](../Page/宁静县.md "wikilink")，1965年改现名。芒康一名在[藏语的意思为](../Page/藏语.md "wikilink")“善妙之地”。盐井以產[井鹽著称](../Page/井鹽.md "wikilink")，有[芒康县盐井古盐田](../Page/芒康县盐井古盐田.md "wikilink")，且有西藏自治区境内唯一的[天主教堂](../Page/天主教.md "wikilink")——[盐井天主堂](../Page/盐井天主堂.md "wikilink")，由[法国传教士创建](../Page/法国.md "wikilink")。曲孜卡有温泉。

## 交通

  - [214國道](../Page/214國道.md "wikilink")：盐井、德钦、丽江、香格里拉。
  - [318國道](../Page/318國道.md "wikilink")：经130km到达四川省巴塘县，再经理塘县（海拔4200米）、雅江县、新都桥、康定、雅安到达距离约1000km的成都市。

## 行政区划

下辖2个镇，14个乡，159个村民委员会：\[1\] 。

## 参考资料

  - A. Gruschke: *The Cultural Monuments of Tibet-{’}-s Outer Provinces:
    Kham - Volume 1. The Xizang Part of Kham (TAR)*, White Lotus Press,
    Bangkok 2004. ISBN 974-480-049-6
  - Tsering Shakya: *The Dragon in the Land of Snows. A History of
    Modern Tibet Since 1947*, London 1999, ISBN 0-14-019615-3

[芒康县](../Category/芒康县.md "wikilink")
[Category:昌都地区县份](../Category/昌都地区县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.