[Mt_lincoln.jpg](https://zh.wikipedia.org/wiki/File:Mt_lincoln.jpg "fig:Mt_lincoln.jpg")
**玛丽·托德·林肯**（，），[美国第十六任](../Page/美国.md "wikilink")[总统](../Page/美国总统.md "wikilink")[亚伯拉罕·林肯的夫人](../Page/亚伯拉罕·林肯.md "wikilink")，[银行家的女儿](../Page/银行家.md "wikilink")。

## 生平

1841年与林肯结婚，有四个儿子。她出身[美国南方](../Page/美国南方.md "wikilink")，在内战中因同情[南方叛军曾经受到批评](../Page/美国南方邦联.md "wikilink")。她在第三个儿子[威廉死后精神崩溃](../Page/威廉·林肯.md "wikilink")，被大儿子[罗伯特·林肯送进精神病院](../Page/罗伯特·林肯.md "wikilink")。

[Category:美国第一夫人](../Category/美国第一夫人.md "wikilink")
[Category:林肯家族](../Category/林肯家族.md "wikilink")