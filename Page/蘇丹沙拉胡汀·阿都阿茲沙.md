**[蘇丹沙拉胡汀](../Page/蘇丹_\(稱謂\).md "wikilink")·阿都阿兹沙**（Almarhum Sultan
Salahuddin Abdul Aziz Shah Al-Haj ibni Almarhum Sultan Sir Hisamuddin
Alam Shah
Al-Haj，）[马来西亚第](../Page/马来西亚.md "wikilink")11任[最高元首和第](../Page/马来西亚最高元首.md "wikilink")8任[雪兰莪苏丹](../Page/雪兰莪苏丹.md "wikilink")。

沙拉胡汀曾就读于[英国](../Page/英国.md "wikilink")[桑赫斯特皇家军事学院和](../Page/桑赫斯特皇家军事学院.md "wikilink")[倫敦大學亞非學院](../Page/倫敦大學亞非學院.md "wikilink")。1950年5月13日被册封为[雪兰莪州王储](../Page/雪兰莪州.md "wikilink")。1961年6月28日就任雪兰莪州第七任苏丹。1994年2月起任马来西亚副最高元首。1999年2月27日当选为最高元首，于同年4月26日就职，任职5年。2001年11月21日因病去世。

[Category:马来西亚最高元首](../Category/马来西亚最高元首.md "wikilink")
[Category:雪兰莪苏丹](../Category/雪兰莪苏丹.md "wikilink")
[Category:倫敦大學亞非學院校友](../Category/倫敦大學亞非學院校友.md "wikilink")