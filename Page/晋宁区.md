[Zheng_He_Park_in_Jinning.jpg](https://zh.wikipedia.org/wiki/File:Zheng_He_Park_in_Jinning.jpg "fig:Zheng_He_Park_in_Jinning.jpg")
**晋宁区**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[昆明市下属的一个](../Page/昆明市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，地处[滇池南岸](../Page/滇池.md "wikilink")，北距省会[昆明市市区](../Page/昆明市.md "wikilink")60公里。东邻[澄江县](../Page/澄江县.md "wikilink")，南连[玉溪市](../Page/玉溪市.md "wikilink")[红塔区和](../Page/红塔区.md "wikilink")[江川区](../Page/江川区.md "wikilink")，西与[安宁市](../Page/安宁市.md "wikilink")、[峨山县](../Page/峨山县.md "wikilink")、[易门县三市县接壤](../Page/易门县.md "wikilink")，北与[昆明市](../Page/昆明市.md "wikilink")[西山区和](../Page/西山区.md "wikilink")[呈贡区交界](../Page/呈贡区.md "wikilink")。总面积1230.86平方千米，人口28.2万。区人民政府驻[昆阳街道](../Page/昆阳街道.md "wikilink")。晋宁区是著名航海家[郑和的故乡](../Page/郑和.md "wikilink")。

## 历史沿革

晋宁区历史悠久，为古[滇国文化发祥地之一](../Page/滇国.md "wikilink")。[汉武帝时设立](../Page/汉武帝.md "wikilink")[益州郡](../Page/益州郡_\(西汉\).md "wikilink")，郡治设在[滇池县](../Page/滇池县.md "wikilink")，即今晋宁区[晋城镇](../Page/晋城镇.md "wikilink")，昆阳则属于[建伶县](../Page/建伶县.md "wikilink")。[三国](../Page/三国.md "wikilink")[蜀汉](../Page/蜀汉.md "wikilink")[建兴三年](../Page/建兴.md "wikilink")（公元225年），改益州郡为建宁郡，郡治迁往味县（今云南省[曲靖市内](../Page/曲靖市.md "wikilink")），滇池县和建伶县仍归建宁郡管辖。西晋永嘉二年（公元308年）改益州郡为[晋宁郡](../Page/晋宁郡.md "wikilink")\[1\]，郡治在滇池县。[唐朝](../Page/唐朝.md "wikilink")[武德四年](../Page/武德.md "wikilink")（公元621年），置[晋宁州](../Page/晋宁州_\(南诏\).md "wikilink")，属[昆州管辖](../Page/昆州.md "wikilink")。昆阳改称望(一作万)水县，属[钩州管辖](../Page/钩州.md "wikilink")。[天宝后期](../Page/天宝_\(唐朝\).md "wikilink")，两地为[南诏所有](../Page/南诏.md "wikilink")，晋宁改称晋川，昆阳改称渠滥川。[大理国时](../Page/大理国.md "wikilink")，晋宁称阳城堡，昆阳叫巨桥城，同属[鄯阐府](../Page/鄯阐府.md "wikilink")。[元代设晋宁州和](../Page/元朝.md "wikilink")[昆阳州](../Page/昆阳州.md "wikilink")，[明](../Page/明朝.md "wikilink")、[清袭用](../Page/清朝.md "wikilink")，直到[民国初](../Page/民国.md "wikilink")，1913年废道改州为县，设晋宁县和昆阳县。1958年两县合并，称晋宁县，县治设在昆阳，属玉溪专区。1960年划归昆明市\[2\]。2016年11月，经国务院批准，撤销晋宁县，设立昆明市晋宁区。\[3\]

## 自然地理

晋宁区全境东西横距66千米，南北纵距33千米，总面积1230.86平方千米，其中山区、半山区占70.7%，坝子、谷地、湖泊占29.3%。地势南高北低，东部为南北走向的关岭山系的黑汉山、老虎山，西南有蛤蟆山头、大黑山横垣昆阳坝子，中部滇池沿岸为湖滨盆地，平均海拔1888米。南部的大梁子山海拔2648米，为全县的最高点；西部的小石板河海拔1340米，为全区最低点。全镜河流可分为3大水系4个流域，东北部属[金沙江水系滇池流域](../Page/金沙江.md "wikilink")，西北属金沙江水系[螳螂川流域](../Page/螳螂川.md "wikilink")，西南属[元江水系](../Page/元江.md "wikilink")[绿汁江流域](../Page/绿汁江.md "wikilink")，南部则属[珠江水系](../Page/珠江.md "wikilink")[南盘江流域](../Page/南盘江.md "wikilink")\[4\]。

气候属低纬高原北亚热带季风气候区。冬暖夏凉，四季如春，多年平均气温14.8℃，无霜期240天左右，多年平均日照时数为2291.2小时，多年平均降水量891.7毫米，风向多为西南风。主要灾害性天气有低温、霜冻、冰雹、旱涝等\[5\]。

## 区划人口

截止2017年4月，晋宁区下辖2个街道，4个镇，2个民族乡：

  - [昆阳街道](../Page/昆阳街道.md "wikilink")，[宝峰街道](../Page/宝峰街道_\(昆明市\).md "wikilink")，[二街镇](../Page/二街镇.md "wikilink")，[晋城镇](../Page/晋城镇.md "wikilink")，[上蒜镇](../Page/上蒜镇.md "wikilink")，[六街镇和](../Page/六街镇.md "wikilink")[双河彝族乡](../Page/双河彝族乡.md "wikilink")，[夕阳彝族乡](../Page/夕阳彝族乡.md "wikilink")

截止2009年末，全县常住总人口为28.2万人，其中男性14.01万人，女性14.19万人。户籍人口27.89万人，其中农业人口23.17万人，非农业人口4.72万人。少数民族人口占人口总数的11%。世居少数民族主要有[彝族](../Page/彝族.md "wikilink")、[回族](../Page/回族.md "wikilink")、[哈尼族等](../Page/哈尼族.md "wikilink")。全县人口自然增长率5‰\[6\]。

## 经济

晋宁工业有磷化工、机械制造等产业。晋宁区境内有探明储量8亿吨的[磷矿石资源](../Page/磷.md "wikilink")，享有“中国磷都”之称。晋宁处于昆明南大门，是“昆玉一体化”的重要节点，交通发达。昆玉等高速公路和[213国道](../Page/213国道.md "wikilink")（昆洛公路）、高海公路、安晋公路及[昆玉铁路等从境内穿过](../Page/昆玉铁路.md "wikilink")，区位优势明显，物流业发达。此外晋宁文化古迹众多，旅游资源丰富，文化旅游产业地位重要\[7\]。

## 参考文献

## 外部链接

  -
[晋宁区](../Category/晋宁区.md "wikilink")
[Category:昆明市辖区](../Category/昆明市辖区.md "wikilink")

1.
2.
3.
4.
5.
6.
7.