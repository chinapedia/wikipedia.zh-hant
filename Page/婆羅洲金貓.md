**婆罗洲金貓**（学名*Catopuma badia*）
又叫**栗猫**，是仅见于[婆罗洲的珍稀动物](../Page/婆罗洲.md "wikilink")。

## 行为

婆罗洲金貓的毛皮通常为红褐色，也有少数灰色变种。研究分析表明它们与[亚洲金貓是近亲](../Page/亚洲金貓.md "wikilink")，但两者的头骨结构有所差别，婆罗洲金貓要小得多。婆罗洲金貓体长50厘米，尾长30厘米。

## 食物

婆罗洲金貓在夜间活动，以[鸟类](../Page/鸟.md "wikilink")、[啮齿类以及小型](../Page/啮齿类.md "wikilink")[猴类为食](../Page/猴.md "wikilink")。

## 栖息地

婆罗洲金貓居住在茂密的[雨林与山岩突起处](../Page/雨林.md "wikilink")。

## 参考

  - [Guide to the Big
    Cats](https://web.archive.org/web/20070514075515/http://www.thebigcats.com/smallcat/baycat.htm)

  - Database entry includes justification for why this species is
    endangered

[Category:金貓屬](../Category/金貓屬.md "wikilink")