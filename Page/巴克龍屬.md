**巴克龍屬**（[學名](../Page/學名.md "wikilink")：*Bactrosaurus*）意為「棍棒[蜥蜴](../Page/蜥蜴.md "wikilink")」，是[草食性](../Page/草食性.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，生活於[上白堊紀的](../Page/白堊紀.md "wikilink")[東亞](../Page/東亞.md "wikilink")，距今約7000萬年前，被認為是最早的[鴨嘴龍超科之一](../Page/鴨嘴龍超科.md "wikilink")。雖然化石不夠完整，巴克龍是研究最多的早期鴨嘴龍類之一\[1\]。

## 敘述

[Bactrosaurus_skull.jpg](https://zh.wikipedia.org/wiki/File:Bactrosaurus_skull.jpg "fig:Bactrosaurus_skull.jpg")\]\]
[Bactrosaurus_johnsoni_1.jpg](https://zh.wikipedia.org/wiki/File:Bactrosaurus_johnsoni_1.jpg "fig:Bactrosaurus_johnsoni_1.jpg")
就像其他的鴨嘴龍超科一樣，巴克龍可以用二足或四足方式行走，但在脊椎有不尋常的大尖刺突出。巴克龍的身長約4-6米長，在四足站立時有2米高，體重1,000到1,500公斤，[股骨長](../Page/股骨.md "wikilink")80厘米\[2\]。

巴克龍是[賴氏龍的早期近親](../Page/賴氏龍.md "wikilink")\[3\]，具有許多[禽龍類的特徵](../Page/禽龍類.md "wikilink")，包含：每列三顆牙齒的齒系、小型的[上頜骨牙齒](../Page/上頜骨.md "wikilink")、及在鴨嘴龍類來說不尋常的巨大體形。巴克龍同時具有[鴨嘴龍亞科](../Page/鴨嘴龍亞科.md "wikilink")、賴氏龍亞科的特徵，可能是禽龍類演化至鴨嘴龍類的過渡物種\[4\]。

巴克龍最初被敘述成頭頂沒有冠狀物，是很典型的[禽龍類特徵](../Page/禽龍類.md "wikilink")，不類似賴氏龍亞科\[5\]。但後來的研究顯示，巴克龍的化石中，有疑似頭冠底部的碎片\[6\]。

在2003年，一個[短冠龍化石發現了](../Page/短冠龍.md "wikilink")[血管瘤](../Page/血管瘤.md "wikilink")、[成纖維性纖維瘤](../Page/成纖維性纖維瘤.md "wikilink")、[轉移癌](../Page/轉移癌.md "wikilink")、[成骨細胞瘤等腫瘤](../Page/成骨細胞瘤.md "wikilink")。B.M.
Rothschild等人使用[電腦斷層掃描與](../Page/電腦斷層掃描.md "wikilink")[螢光鏡成影](../Page/螢光鏡.md "wikilink")，檢驗多種恐龍的脊椎。鴨嘴龍類中共有：[埃德蒙頓龍](../Page/埃德蒙頓龍.md "wikilink")、[計氏龍](../Page/計氏龍.md "wikilink")、巴克龍、短冠龍接受檢驗。在超過一萬個化石中，只有短冠龍與其近親發現了腫瘤。腫瘤的成因可能是環境因素或[遺傳規律](../Page/遺傳規律.md "wikilink")\[7\]。

## 發現

姜氏巴克龍的6個部份骨骼在[中國及](../Page/中國.md "wikilink")[蒙古的](../Page/蒙古.md "wikilink")[戈壁沙漠被發現](../Page/戈壁沙漠.md "wikilink")\[8\]。這些化石來自於不同年齡，從幼年個體到成年個體\[9\]。巴克龍的化石包含四肢、骨盆、大部分頭骨，目前還沒有發現完整的骨骼，但在早期鴨嘴龍類中，是受研究較多的一種\[10\]。

在1933年，[查爾斯·懷特尼·吉爾摩爾](../Page/查爾斯·懷特尼·吉爾摩爾.md "wikilink")（Charles W.
Gilmore）將這些化石敘述、命名。屬名是由[古希臘文的棍棒](../Page/古希臘文.md "wikilink")（*baktron*）與[蜥蜴](../Page/蜥蜴.md "wikilink")（*sauros*）構成，意指形狀類似棍棒的大型背部[神經棘](../Page/神經棘.md "wikilink")\[11\]。

## 參考

## 外部連結

  - [恐龍博物館有關巴克龍](https://web.archive.org/web/20071019013245/http://www.dinosaur.net.cn/museum/Bactrosaurus.htm)
  - [Fukui Pref. Dinosaur Museum site on
    *Bactrosaurus*](https://web.archive.org/web/20061116202831/http://www.dinosaur.pref.fukui.jp/en/ddic/Bactrosaurus.html)
  - [Triebold Paleontology on
    *Bactrosaurus*](https://web.archive.org/web/20070207114326/http://www.trieboldpaleontology.com/casts/probactrosaurus_juvenile.htm)

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:鴨嘴龍超科](../Category/鴨嘴龍超科.md "wikilink")

1.  "Bactrosaurus." In: Dodson, Peter & Britt, Brooks & Carpenter,
    Kenneth & Forster, Catherine A. & Gillette, David D. & Norell, Mark
    A. & Olshevsky, George & Parrish, J. Michael & Weishampel, David B.
    *The Age of Dinosaurs*. Publications International, LTD. p. 131.
    ISBN 978-0-7853-0443-2.

2.
3.
4.

5.
6.
7.

8.
9.
10.
11.