__NOTOC__

|                                           |                                        |                                        |
| ----------------------------------------- | -------------------------------------- | -------------------------------------- |
| [←](../Page/香港2006年8月.md "wikilink")      | **2006年9月**                            | [→](../Page/香港2006年10月.md "wikilink")  |
| <span style="color: red;">日</span>        | 一                                      | 二                                      |
|                                           |                                        |                                        |
| **[{{red](../Page/#9月3日.md "wikilink")**  | **[4](../Page/#9月4日.md "wikilink")**   | **[5](../Page/#9月5日.md "wikilink")**   |
| **[{{red](../Page/#9月10日.md "wikilink")** | **[11](../Page/#9月11日.md "wikilink")** | **[12](../Page/#9月12日.md "wikilink")** |
| **[{{red](../Page/#9月17日.md "wikilink")** | **[18](../Page/#9月18日.md "wikilink")** | **[19](../Page/#9月19日.md "wikilink")** |
| **[{{red](../Page/#9月24日.md "wikilink")** | **[25](../Page/#9月25日.md "wikilink")** | **[26](../Page/#9月26日.md "wikilink")** |

## [9月1日](../Page/9月1日.md "wikilink")

  - 一輛[水務署的](../Page/水務署.md "wikilink")[承辦商工程車](../Page/承辦商.md "wikilink")，於早上8時52分在[上環](../Page/上環.md "wikilink")**[普慶坊](../Page/普慶坊.md "wikilink")**倒車行駛20多米，在與[差館上街交界處](../Page/差館上街.md "wikilink")，近[英皇書院同學會小學第二校之西面馬路](../Page/英皇書院同學會小學第二校.md "wikilink")，意外將一名36歲男子及其手抱的11個月大女嬰捲入車底，女嬰頭部嚴重受傷當場死亡，而男子則被送往[香港島西的](../Page/香港島.md "wikilink")[瑪麗醫院](../Page/瑪麗醫院.md "wikilink")，也證實不治。
    [維基新聞](../Page/n:香港上環貨車_倒車撞死父女.md "wikilink")
  - 為了應付學生在新學年第一天開學帶來的交通負擔，**[九廣鐵路](../Page/九廣鐵路.md "wikilink")**為[西鐵](../Page/西鐵.md "wikilink")、[輕鐵乘客開辦兩條臨時免費接駁巴士綫](../Page/香港輕鐵.md "wikilink")，[新巴和](../Page/新巴.md "wikilink")[城巴也會密切關注路面交通狀況](../Page/城巴.md "wikilink")。[星島日報](http://hk.news.yahoo.com/060831/60/1sap6.html)
  - [中華電力選定在](../Page/中華電力.md "wikilink")**[大鴉洲](../Page/大鴉洲.md "wikilink")**興建液態[天然氣接收站](../Page/天然氣.md "wikilink")，造價80億港元，但[世界自然基金会則反對中電的建站計劃](../Page/世界自然基金会.md "wikilink")。
    [維基新聞](../Page/n:香港大鴉洲倡建天然氣庫.md "wikilink")
  - [天水圍](../Page/天水圍.md "wikilink")**[自閉症](../Page/自閉症.md "wikilink")**男童墮樓死亡案，負責照顧男童的[內地婦人](../Page/內地.md "wikilink")，被控虐兒及過期居留，被處監禁八個月。
    [維基新聞](../Page/n:男童墮斃_香港保母判監8個月.md "wikilink")
  - [香港高等法院經過內庭聆訊後](../Page/香港高等法院.md "wikilink")，向《**[壹本便利](../Page/壹本便利.md "wikilink")**》發出臨時禁制令，禁止刊登藝人[鍾欣桐的更衣照](../Page/鍾欣桐.md "wikilink")。另外，[工商及科技局局長](../Page/香港工商及科技局.md "wikilink")[王永平表示會考慮修改](../Page/王永平_\(香港\).md "wikilink")「淫褻及不雅管制條例」並加強罰則，以加強阻嚇力。
    [維基新聞](../Page/n:鍾欣桐獲頒臨時令禁制《壹本便利》.md "wikilink")
  - 一名八歲的香港男童，在[福建](../Page/福建省.md "wikilink")[福州遭人](../Page/福州市.md "wikilink")**[綁架](../Page/綁架.md "wikilink")**，男童後來自行掙脫綑綁逃脫，福州警方已拘捕一名疑犯，香港[入境處則表示沒有收到相關資料或求助個案](../Page/入境事務處.md "wikilink")。男童及其母親居於[福州](../Page/福州市.md "wikilink")[晋安区](../Page/晋安区.md "wikilink")，男童於星期三晚在寓所外玩耍時遭一名保安員綁架，綁匪向家人勒索五十萬[人民幣](../Page/人民幣.md "wikilink")，男童的母親報警求助，男童最後自行掙脫綑綁逃脫，被在場調查的民警發現。[明報](http://hk.news.yahoo.com/060901/12/1scni.html)
  - [衛生署衛生防護中心證實本年首宗](../Page/香港衛生署.md "wikilink")[霍亂個案](../Page/霍亂.md "wikilink")，患者是一名29歲男子，他在上月28日發病，並出現腹瀉、作嘔、肚痛及發燒等病徵。衛生防護中心化驗後，證實他感染**[小川型霍亂弧菌](../Page/小川型霍亂弧菌.md "wikilink")**，現時在[瑪嘉烈醫院留醫](../Page/瑪嘉烈醫院.md "wikilink")，情況穩定。患者居於內地，但他每日會返回香港，並到[屯門區工作](../Page/屯門區.md "wikilink")，現階段未知道他的感染源頭，而患者家人並沒有病徵。[明報](http://hk.news.yahoo.com/060901/12/1scnd.html)
  - [尖沙咀](../Page/尖沙咀.md "wikilink")[彌敦道的](../Page/彌敦道.md "wikilink")[聖公會](../Page/聖公會.md "wikilink")**[聖安德烈堂](../Page/聖安德烈堂.md "wikilink")**獲[聯合國](../Page/聯合國.md "wikilink")[教育科技及文化組織](../Page/联合国教育科学文化组织.md "wikilink")[亞太地區文化遺產保護獎優秀獎](../Page/亞太地區文化遺產保護獎.md "wikilink")。
    [星島日報](https://web.archive.org/web/20070121035917/http://hk.news.yahoo.com/060901/60/1sd1f.html)

## [9月2日](../Page/9月2日.md "wikilink")

## [9月3日](../Page/9月3日.md "wikilink")

## [9月4日](../Page/9月4日.md "wikilink")

## [9月5日](../Page/9月5日.md "wikilink")

  - 男藝人以[手提電話](../Page/手提電話.md "wikilink")[偷拍女藝人更衣短片](../Page/偷拍.md "wikilink")[勒索案審結](../Page/勒索.md "wikilink")，[區域法院在](../Page/區域法院.md "wikilink")[灣仔根據若干](../Page/灣仔.md "wikilink")[香港法例判](../Page/香港法例.md "wikilink")41歲的被告郭金勝入[獄](../Page/監獄.md "wikilink")28個月。
    [明報1](https://web.archive.org/web/20060912115647/http://hk.news.yahoo.com/060905/12/1sjsv.html)
    [明報2](https://web.archive.org/web/20061110091808/http://hk.news.yahoo.com/060905/12/1sjl3.html)
    [明報3](http://hk.news.yahoo.com/060905/12/1sjl4.html)
    [星島日報1](https://web.archive.org/web/20070320063300/http://hk.news.yahoo.com/060905/60/1sk0q.html)
    [太陽報](http://hk.news.yahoo.com/060905/197/1sjzv.html)
    [東方日報](http://hk.news.yahoo.com/060905/10/1sjo9.html)
    [星島日報2](https://web.archive.org/web/20070705101837/http://hk.news.yahoo.com/060905/60/1siqn.html)
    [明報4](https://web.archive.org/web/20070530011618/http://hk.news.yahoo.com/060905/12/1sjlr.html)[星島日報3](http://hk.news.yahoo.com/060905/60/1sinn.html)

## [9月6日](../Page/9月6日.md "wikilink")

  - [匯豐銀行亞洲私人銀行服務行政總裁](../Page/匯豐銀行.md "wikilink")[王以智入稟](../Page/王以智.md "wikilink")[香港法院](../Page/香港法院.md "wikilink")，向前[拉丁舞導師Mirko](../Page/拉丁舞.md "wikilink")
    Saccani索償共6,259萬港元的預繳學費，法院判王以智勝訴，可取回款項。[明報](https://web.archive.org/web/20060908052344/http://hk.news.yahoo.com/060906/12/1skft.html)、[星島日報](http://hk.news.yahoo.com/060906/60/1slde.html)
  - [香港仔晚上發生槍擊案](../Page/香港仔.md "wikilink")，一名正在休班的便衣警員在香港仔中心行人隧道內被[警察機動部隊警員截停調查時](../Page/警察機動部隊.md "wikilink")，突然搶去一名警員的配槍，並槍傷一名警員，己送往[瑪麗醫院救治](../Page/瑪麗醫院.md "wikilink")。[星島日報](http://hk.news.yahoo.com/060906/60/1slxp.html)，[明報](https://web.archive.org/web/20070225091708/http://hk.news.yahoo.com/060906/12/1sm3m.html)

## [9月7日](../Page/9月7日.md "wikilink")

  - 政府舉行新聞發佈會，建議大幅縮減**[邊境禁區](../Page/香港邊境禁區.md "wikilink")**的覆蓋範圍。[香港政府新聞處](http://www.info.gov.hk/gia/general/200609/07/P200609070224.htm)、[星島日報](https://web.archive.org/web/20060912120202/http://hk.news.yahoo.com/060907/60/1sou7.html)、[明報](https://web.archive.org/web/20070527233302/http://hk.news.yahoo.com/060907/12/1soh3.html)

## [9月8日](../Page/9月8日.md "wikilink")

  - 兩名分別[麥姓及](../Page/麥姓.md "wikilink")[陳姓的](../Page/陳姓.md "wikilink")[皇仁書院學生](../Page/皇仁書院.md "wikilink")，於[年初串謀搶劫](../Page/1月14日.md "wikilink")[中醫師](../Page/中醫師.md "wikilink")，於[區域法院進行最後求情及接受判刑](../Page/區域法院.md "wikilink")，多名皇仁書院學生及校友替兩名被告求情，稱被告犯案是讀書壓力大及好奇心所致，希望法官予以輕判。法官最後分別處兩名被告進入[教導所及接受](../Page/教導所.md "wikilink")[社會服務令](../Page/社會服務令.md "wikilink")。【香港司法機構案件編號：DCCC294/2006】[太陽報](http://the-sun.orisun.com/channels/news/20060117/20060117022030_0000.html)
    [明報1](https://web.archive.org/web/20070521184859/http://hk.news.yahoo.com/060907/12/1sogf.html)
    [文匯報](http://master.wwpnews.net/news.phtml?news_id=NS0608100002&loc=any&cat=037NS&no_combo=1&PHPSESSID=0f8ce308ecf757d6221e14f7017367e5)
    [明報通識網](http://life.mingpao.com/cfm/interact3.cfm?File=20060805/infeature/20060805.txt)
    [成報](https://web.archive.org/web/20070625202352/http://www.singpao.com/20060908/local/872968.html)
    [明報2](http://news.sina.com.hk/cgi-bin/news/show_news.cgi?ct=headlines&type=headlines&date=2006-08-10&id=2154265)
    [頭條網](http://www.hkheadline.com/news/headline_news_detail.asp?id=10135)
    [香港商报](https://web.archive.org/web/20070927033442/http://hk.sznews.com/20060810/ca2408794.htm)
    [中國日報](http://news.sina.com/chinesedaily/103-000-101-109/2006-08-18/16161224892.html)
    [星島日報](http://www.singtao.com/index_archive.asp?d_str=20060810&htmlpage=main&news=0810ao02.html)

## [9月9日](../Page/9月9日.md "wikilink")

## [9月10日](../Page/9月10日.md "wikilink")

  - [東區](../Page/東區_\(香港\).md "wikilink")[區議會](../Page/香港區議會.md "wikilink")**[翠灣](../Page/翠灣邨.md "wikilink")**選區補選，[四五行動的](../Page/四五行動.md "wikilink")[古桂耀以](../Page/古桂耀.md "wikilink")1733票，擊敗[工聯會的](../Page/工聯會.md "wikilink")[姜淑敏而當選](../Page/姜淑敏.md "wikilink")，任期至[2007年底](../Page/2007年.md "wikilink")。是次補選是要填補前區議員[鄧禮明因](../Page/鄧禮明.md "wikilink")[破產而請辭所留下的議席空缺](../Page/破產.md "wikilink")。[明報](http://hk.news.yahoo.com/060910/12/1ss8d.html)
  - 2006/07年**[馬季](../Page/香港賽馬.md "wikilink")**開鑼，[沙田馬場入場人次約](../Page/沙田馬場.md "wikilink")3.4萬人，較去年增加18.6%。全日總投注額為7.3億港元，較去年增加7.2%。[明報](http://hk.news.yahoo.com/060910/12/1ss8y.html)

## [9月11日](../Page/9月11日.md "wikilink")

  - [香港政府在](../Page/香港政府.md "wikilink")[香港會議展覽中心舉辦](../Page/香港會議展覽中心.md "wikilink")「十一·五與香港發展經濟高峰會」，由[行政長官](../Page/行政長官.md "wikilink")[曾蔭權主持](../Page/曾蔭權.md "wikilink")。[國家法改委財政金融司司長](../Page/國家發展改革委員會.md "wikilink")[徐林](../Page/徐林.md "wikilink")，及[廣東省發展和改革委員會主任](../Page/廣東省.md "wikilink")[陳善如擔任嘉賓講者](../Page/陳善如.md "wikilink")。[星島日報](http://hk.news.yahoo.com/060911/60/1suir.html)
  - 行政長官[曾蔭權在](../Page/曾蔭權.md "wikilink")《十一·五與香港發展》經濟高峰會中，明確表示[特區政府早已不奉行](../Page/香港特別行政區政府.md "wikilink")**[積極不干預](../Page/積極不干預.md "wikilink")**政策，令多名政界人士感意外。
    [明報](https://web.archive.org/web/20070517160736/http://hk.news.yahoo.com/060911/12/1supl.html)
  - 綽號「蝦叔」的著名[粵劇紅伶](../Page/粵劇.md "wikilink")、電視劇及電影演員**[關海山](../Page/關海山.md "wikilink")**，今早在[廣華醫院病逝](../Page/廣華醫院.md "wikilink")，終年80歲。[明報](https://archive.is/20130105113501/http://hk.news.yahoo.com/060911/12/1sute.html)

## [9月12日](../Page/9月12日.md "wikilink")

  - [地政總署在](../Page/地政總署.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[伊利沙伯體育館舉行](../Page/伊利沙伯體育館.md "wikilink")**[土地拍賣](../Page/香港土地拍賣.md "wikilink")**，是次出售位於[荃灣](../Page/荃灣.md "wikilink")[青龍頭地段第](../Page/青龍頭.md "wikilink")68號地皮，結果由[新鴻基地產以五千三百萬港元投得](../Page/新鴻基地產.md "wikilink")。[星島日報](http://hk.news.yahoo.com/060912/60/1sxml.html)
    [大公報](https://web.archive.org/web/20070928023158/http://www.takungpao.com.hk/news/06/09/08/HD-619356.htm)

## [9月13日](../Page/9月13日.md "wikilink")

  - 有報章報道，約有70名[香港藝人住址被轉貼至某](../Page/香港藝人.md "wikilink")[網上討論區](../Page/網上討論區.md "wikilink")，報道同時証實六、七成資料是真確的，不過此舉可能侵犯**[個人私隱](../Page/個人私隱.md "wikilink")**。法律界建議有關藝人可以經[民事法追討損失](../Page/民事法.md "wikilink")。
    [明報1](https://web.archive.org/web/20070927230351/http://www.mpinews.com/htm/INews/20060914/gb40832t.htm)
    [明報2](http://www.mingpaonews.com/20060914/gaa1h.htm)

## [9月14日](../Page/9月14日.md "wikilink")

  - [香港以南水域在晚上](../Page/香港地理.md "wikilink")7時53分發生[-{黎克特制}-](../Page/芮氏地震規模.md "wikilink")3.5級**[地震](../Page/地震.md "wikilink")**，歷時2-3秒，部分香港市民能感受震動。震央位於北緯22.0度，東經114.3度，即香港東南偏南約36[公里](../Page/公里.md "wikilink")，屬[廣東省](../Page/廣東省.md "wikilink")[珠海市管轄範圍內](../Page/珠海市.md "wikilink")。香港上一次發生地震是[1995年](../Page/1995年.md "wikilink")。
    [明報](http://hk.news.yahoo.com/060914/12/1t20i.html)、[香港電台新聞](https://web.archive.org/web/20131020062350/http://www.rthk.org.hk/rthk/news/expressnews/news.htm?expressnews&20060914&55&342123)、[香港天文台地震新聞稿](http://www.hko.gov.hk/gts/equake/eqpress_c/eqpress_c.20060914.2042.htm)，[珠海資訊網](https://web.archive.org/web/20070701002050/http://www.zhuhai.gov.cn/waiwang/72621647797944320/20030904/2001826.html)

## [9月15日](../Page/9月15日.md "wikilink")

## [9月16日](../Page/9月16日.md "wikilink")

  - [香港政府](../Page/香港政府.md "wikilink")[運輸署舉行](../Page/運輸署.md "wikilink")**[自訂車牌號碼](../Page/自訂車輛登記號碼計劃.md "wikilink")**首次[拍賣](../Page/拍賣.md "wikilink")，近千人到場參與競投，氣氛激烈。210個車牌為政府庫房帶來港幣1,125萬4千元收入。當中寓意「我愛你」的「1
    L0VE
    U」車牌以140萬由一名買家以140萬港元投得，為全日成交價最高。而代表[無綫電視](../Page/電視廣播有限公司.md "wikilink")「TVB」及[保時捷](../Page/保時捷.md "wikilink")「P0RSCHE」的車牌則由一名楊姓男子以港幣80萬及70萬元所投得，為全日成交價第二及第三高。[東方日報](http://hk.news.yahoo.com/060916/10/1t5el.html)、[明報](https://web.archive.org/web/20070518064203/http://hk.news.yahoo.com/060916/12/1t58p.html)

## [9月17日](../Page/9月17日.md "wikilink")

  - 屹立[銅鑼灣](../Page/銅鑼灣.md "wikilink")25年的[三越百貨因](../Page/三越百貨.md "wikilink")[興利中心需要重建](../Page/興利中心.md "wikilink")，在下午六時正式結業，暫別香江，等待物色新地址後重新營業。二十世紀八十年代以來，[大丸](../Page/大丸.md "wikilink")、[松板屋](../Page/松板屋.md "wikilink")、[崇光](../Page/崇光.md "wikilink")、三越等日資百貨在銅鑼灣一帶開張，故此銅鑼灣有「小東京」之美譽。隨著大丸、松板屋撤離[香港](../Page/香港.md "wikilink")，[崇光於](../Page/崇光.md "wikilink")[2000年轉為港資](../Page/2000年.md "wikilink")，宣布日資百貨公司在香港的輝煌時代正式步入歷史。[明報](https://web.archive.org/web/20070501052642/http://hk.news.yahoo.com/060917/12/1t5wq.html)、[星島日報](http://hk.news.yahoo.com/060917/60/1t698.html)

## [9月18日](../Page/9月18日.md "wikilink")

  - [東涌](../Page/東涌.md "wikilink")**[昂坪360](../Page/昂坪360.md "wikilink")**下午1時正式啟用，開幕儀式由[地鐵工程總監](../Page/地鐵有限公司.md "wikilink")[柏立恒及昂坪](../Page/柏立恒.md "wikilink")360常務董事[高德偉主持](../Page/高德偉.md "wikilink")，政府由[旅遊事務專員](../Page/旅遊事務專員.md "wikilink")[區璟智代表出席](../Page/區璟智.md "wikilink")。首批乘客獲派發紀念品。[明報](http://hk.news.yahoo.com/060918/12/1t7ec.html)
  - [旺角](../Page/旺角.md "wikilink")[德興街發生垃圾車倒車撞死途人慘劇](../Page/德興街.md "wikilink")，一名因為危險駕駛而被罰停牌的司機於[豉油街倒駛入](../Page/豉油街.md "wikilink")[德興街時](../Page/德興街.md "wikilink")，疑因未有留意有人在車尾過路，將一名50歲[楊姓婦人推倒並捲入車底](../Page/楊姓.md "wikilink")，女事主當場死亡。警方到場後揭發司機在停牌期間駕駛，而且垃圾車沒有倒車警號及倒車燈損壞，將車拖走檢驗，並控以司機多項罪名。這個已經是近一個月第二宗因倒車而引起的交通意外。[明報](http://hk.news.yahoo.com/060918/12/1t8ge.html)
  - [煤氣公司宣佈](../Page/中華煤氣.md "wikilink")，由於該公司由2006年10月起加入[天然氣作原料](../Page/天然氣.md "wikilink")，致使[煤氣的生產成本降低](../Page/煤氣.md "wikilink")，故該公司由下月開始透過燃料調整費機制，將減省的成本回贈予客戶，預計煤氣費將會減價約10%。煤氣公司發言人指，該公司將於下月推出全新賬單，以列明採用天然氣後客戶所節省的煤氣費開支。[明報](http://hk.news.yahoo.com/060918/12/1t8g9.html)
  - 全國人大常委[曾憲梓的二媳婦](../Page/曾憲梓.md "wikilink")[吳嘉寶](../Page/吳嘉寶.md "wikilink")，昨天在[明報刊登分居啟事](../Page/明報.md "wikilink")，指其丈夫[曾智雄與一楊姓女藝人發展婚外情](../Page/曾智雄.md "wikilink")，多名女藝人及事件主角[曾智雄均否認事件](../Page/曾智雄.md "wikilink")。現年44歲的[曾智雄](../Page/曾智雄.md "wikilink")，現為[中國世貿集團主席](../Page/中國世貿集團.md "wikilink")，並於1988年與[吳嘉寶結婚](../Page/吳嘉寶.md "wikilink")，但兩人於[2004年開始分居](../Page/2004年.md "wikilink")。[明報1](http://hk.news.yahoo.com/060917/12/1t5x7.html)、[2](https://web.archive.org/web/20070703104636/http://hk.news.yahoo.com/060918/12/1t8jb.html)

## [9月19日](../Page/9月19日.md "wikilink")

## [9月20日](../Page/9月20日.md "wikilink")

  - 一名男子在網上聲稱號召組織另類「[快閃黨](../Page/快閃黨.md "wikilink")」，在[高等法院被裁定](../Page/香港高等法院.md "wikilink")「[違反公德行為](../Page/違反公德行為.md "wikilink")」罪名成立。法官等候被告背景[感化官報告](../Page/感化官.md "wikilink")，押後至[10月4日判刑](../Page/10月4日.md "wikilink")。42歲的被告[陳鍚明正職是餐廳東主](../Page/陳鍚明.md "wikilink")，他被控於去年8月13至15日，在網上兩度發出邀約他人[強姦女子的訊息](../Page/強姦.md "wikilink")。主審法官[陸啟康指](../Page/陸啟康.md "wikilink")，辯方雖然能夠證明被告純屬開玩笑，但被告的言論則可能鼓吹好此道者犯案，而部分網民亦信以為真，己令人構成不安。【案件編號：DCCC196/06】[明報](http://hk.news.yahoo.com/060920/12/1tdjj.html)

## [9月21日](../Page/9月21日.md "wikilink")

  - [香港海關今天落案起訴一名](../Page/香港海關.md "wikilink")[落馬洲管制站入境的](../Page/落馬洲.md "wikilink")20歲[中國內地領隊](../Page/中國內地.md "wikilink")，她涉嫌[走私超過一萬支未完](../Page/走私.md "wikilink")[稅](../Page/稅.md "wikilink")[香煙](../Page/香煙.md "wikilink")，總值25,000多元。
    她昨天被捕之時，正帶領一團為數22人的訪港旅行團準備入境。案件下午在[粉嶺法院提堂](../Page/粉嶺法院.md "wikilink")。
    [香港電台新聞](http://www.rthk.org.hk/rthk/news/expressnews/20060921/news_20060921_55_343847.htm)、[明報](https://web.archive.org/web/20070529170236/http://hk.news.yahoo.com/060921/12/1tg1o.html)

## [9月22日](../Page/9月22日.md "wikilink")

  - [香港海關對](../Page/香港海關.md "wikilink")**[SK-II](../Page/SK-II.md "wikilink")**產品樣本的化驗結果顯示，9個樣本證實含微量的[重金屬](../Page/重金屬.md "wikilink")[鉻和](../Page/鉻.md "wikilink")[釹](../Page/釹.md "wikilink")，但並未超出安全標準。當中8個樣本所含鉻及釹由百萬分之0.01至0.4，美白粉餅樣本則含百萬分之5.2的鉻和4.2的釹。[明報](https://archive.is/20130106092525/http://hk.news.yahoo.com/060922/12/1tid8.html)
  - 著名電視劇甘草演員**[鮑方](../Page/鮑方.md "wikilink")**於22日下午在[香港](../Page/香港.md "wikilink")[律敦治醫院因為肺功能衰退而病逝](../Page/律敦治醫院.md "wikilink")，享年八十四歲。[商業電台](http://pshweb02.881903.com/apps/news/html/news/20060922/2006092221590152200.htm)、[明報](https://web.archive.org/web/20070514023103/http://hk.news.yahoo.com/060922/12/1til5.html)
  - [香港警方以涉嫌企圖導致爆炸及意圖危害生命或財產](../Page/香港警察.md "wikilink")，將一名21歲的網民拘捕。該網民自稱「[真主教](../Page/真主黨.md "wikilink")[恐怖分子](../Page/恐怖分子.md "wikilink")」，詢問如何製造[炸彈炸毀](../Page/炸彈.md "wikilink")「**迪迪尼**」。雖然有關人士否認迪迪尼是指**[香港迪士尼樂園](../Page/香港迪士尼樂園.md "wikilink")**，但是有很多人相信他指的迪迪尼是指香港迪士尼樂園。
    [維基新聞](../Page/n:香港網民揚言炸「迪迪尼」被捕.md "wikilink")、[星島日報](https://web.archive.org/web/20061111140251/http://hk.news.yahoo.com/060922/60/1tiw3.html)、[東方日報](http://hk.news.yahoo.com/060922/10/1tiv0.html)、[明報](https://web.archive.org/web/20070107021158/http://hk.news.yahoo.com/060922/12/1tiik.html)
  - [日本進口的](../Page/日本.md "wikilink")**[秋刀魚](../Page/秋刀魚.md "wikilink")**被[中國大陸驗出](../Page/中國大陸.md "wikilink")[重金屬](../Page/重金屬.md "wikilink")[砷及](../Page/砷.md "wikilink")[鎘含量超標](../Page/鎘.md "wikilink")22倍，香港各大[超級市場已相繼停售及回收有關產品](../Page/超級市場.md "wikilink")。
    [星島日報](https://web.archive.org/web/20061111140546/http://hk.news.yahoo.com/060922/60/1tivz.html)

## [9月23日](../Page/9月23日.md "wikilink")

  - 前[政務司司長](../Page/政務司司長.md "wikilink")**[陳方安生](../Page/陳方安生.md "wikilink")**舉行記者招待會，正式宣布不會參加[2007年的](../Page/2007年.md "wikilink")[行政長官選舉及公佈成立](../Page/行政長官.md "wikilink")[陳方安生核心小組](../Page/陳方安生核心小組.md "wikilink")、小組成員及成立目的，[陳方安生表示該小組的成立目的是推動香港的民主](../Page/陳方安生.md "wikilink")。[明報1](http://hk.news.yahoo.com/060923/12/1tj6q.html)、[2](https://web.archive.org/web/20061116040038/http://hk.news.yahoo.com/060923/12/1tjbb.html)

## [9月24日](../Page/9月24日.md "wikilink")

  - 一名[陳姓](../Page/陳姓.md "wikilink")[雙程證孕婦經](../Page/港澳居民來往內地通行證.md "wikilink")[羅湖過關](../Page/羅湖.md "wikilink")，[大學站臨盆](../Page/大學站_\(新界\).md "wikilink")，[嚴姓九鐵售票員接生](../Page/嚴姓.md "wikilink")，誕下男嬰，母子皆平安。其丈夫陪妻往[沙田威爾斯親王醫院](../Page/沙田威爾斯親王醫院.md "wikilink")[婦產科入院休養](../Page/婦產科.md "wikilink")。
    [明報](http://hk.news.yahoo.com/060924/12/1tk0b.html)
    [太陽報](http://hk.news.yahoo.com/060924/197/1tkew.html)

## [9月25日](../Page/9月25日.md "wikilink")

  - 警方在早上拘捕一名新界[的士司機](../Page/香港的士.md "wikilink")，懷疑他與上周四[上水一宗致命交通意外有關](../Page/上水.md "wikilink")。涉事的司機姓廖，56歲。他在上周四在[青山公路近燕崗村撞死一名](../Page/青山公路.md "wikilink")73歲老婦[鮑惠英後不顧而去](../Page/鮑惠英.md "wikilink")。警方表示，他們昰在有熱心市民提供情報，指目睹該男子駕駛涉案的士到車房維修損毁的車牌而起疑，警方取得該車的車牌號碼後派探員跟蹤廖某，乘廖某在錦綉花園的住所外，準備修理車頭燈時將他拘捕。[明報](https://web.archive.org/web/20070908130931/http://hk.news.yahoo.com/060925/12/1tmsb.html)
  - 古物諮詢委員會宣佈移位於[深井的](../Page/深井.md "wikilink")**[龍圃花園](../Page/龍圃花園.md "wikilink")**大宅列為二級歷史建築，大宅由己故慈善家[李耀祥所建](../Page/李耀祥.md "wikilink")，並由當時中國著名建築師朱彬所設計。委員會認為大宅內的中式庭園設計在香港較為罕見，故給予高度評價。而[李耀祥孫女](../Page/李耀祥.md "wikilink")[李康意則表示已成立](../Page/李康意.md "wikilink")「龍圃慈善基金」，並建議由基金長期管理古物，但她亦批評[地政總署人員進入龍圃調查時](../Page/地政總署.md "wikilink")，不慎把八角亭的木門損毀。[明報](https://web.archive.org/web/20061109044020/http://hk.news.yahoo.com/060925/12/1tmsm.html)、[香港政府新聞稿](http://www.info.gov.hk/gia/general/200609/25/P200609250216.htm)
  - [行政長官](../Page/香港特別行政區行政長官.md "wikilink")[曾蔭權與](../Page/曾蔭權.md "wikilink")[國務院副總理](../Page/中华人民共和国国务院.md "wikilink")[吳儀會面](../Page/吳儀.md "wikilink")，吳儀讚賞曾蔭權自上任以來，在其領導下克服種種的困難及各方的挑戰，使[香港經濟得到明顯的復蘇](../Page/香港經濟.md "wikilink")，為此感到非常高興。另外，[吳儀在談到前](../Page/吳儀.md "wikilink")[衛生署署長](../Page/香港衞生署.md "wikilink")[陳馮富珍參選](../Page/陳馮富珍.md "wikilink")[世衛總幹事一職時表示](../Page/世界衛生組織.md "wikilink")，中央政府會用一切可行的方式支持[陳馮富珍參選](../Page/陳馮富珍.md "wikilink")，並稱「中央對Margaret(陳馮富珍)的支持，也昰對[香港的支持](../Page/香港.md "wikilink")」。[明報](http://hk.news.yahoo.com/060925/12/1tmsf.html)
  - 為方便粵港跨境車輛繳付廣東省内高速公路路費，香港[快易通及广东粵通卡公司合作](../Page/快易通.md "wikilink")，聯合推出「快易通－粵通卡」繳費服務。跨境車輛只須向快易通公司申請内地繳費功能，便可於广东省境內利用原有的快易通作粵通卡使用。然而部份司機認為此服務可能引起分帳及收費上的混亂，故暫時不會採用。[明報](http://hk.news.yahoo.com/060925/12/1tmu3.html)
  - [地鐵行政總裁](../Page/香港地鐵.md "wikilink")[周松崗表示](../Page/周松崗.md "wikilink")，[昂坪360纜車間中暫停服務屬正常](../Page/昂坪360.md "wikilink")，他在[湖南出席經貿交流活動時表示](../Page/湖南.md "wikilink")，纜車容易受[天氣影響](../Page/天氣.md "wikilink")，當有強風時都會減慢車速甚至暫時停駛，地鐵在投資時已預料這情况，並希望乘客諒解。他又認為纜車運作良好，相信項目會是一個成功景點。當他被問及[東涌纜車站早前出現裂紋](../Page/東涌站_\(昂坪360\).md "wikilink")，[周松崗認為對纜車站結構沒影響](../Page/周松崗.md "wikilink")，但長遠會加強對車站的維修。[明報](http://hk.news.yahoo.com/060925/12/1tmrl.html)

## [9月26日](../Page/9月26日.md "wikilink")

## [9月27日](../Page/9月27日.md "wikilink")

  - [公共廣播服務檢討委員會屬下的管治架構專題小組建議](../Page/香港公共廣播服務檢討委員會.md "wikilink")，日後**[香港電台](../Page/香港電台.md "wikilink")**脫離政府，由獨立的董事局監管，行政總裁為總編輯，委員會主席[黃應士指有關建議為初步建議](../Page/黃應士.md "wikilink")，最終建議會在[2006年下半年提交](../Page/2006年.md "wikilink")[行政長官](../Page/香港特別行政區行政長官.md "wikilink")。[明報](https://archive.is/20130105132115/http://hk.news.yahoo.com/060927/12/1trvk.html)
  - 有[澳門報章指出](../Page/澳門.md "wikilink")**[西北航運](../Page/西北航運.md "wikilink")**來往[屯門碼頭及](../Page/屯門碼頭.md "wikilink")[澳門的航線已遭](../Page/港澳碼頭_\(澳門\).md "wikilink")[澳門政府否決](../Page/澳門政府.md "wikilink")。
    [澳門日報](http://www.qoos.com/news/show.php?a=116187)

## [9月28日](../Page/9月28日.md "wikilink")

  - **[國泰航空](../Page/國泰航空.md "wikilink")**完成收購**[港龍航空](../Page/港龍航空.md "wikilink")**，港龍隨即宣佈裁員191人。[東方日報](http://hk.news.yahoo.com/060928/10/1tu71.html)
  - **[香港賽馬會](../Page/香港賽馬會.md "wikilink")**宣佈任命馬會賽馬事務執行總監[應家柏接替](../Page/應家柏.md "wikilink")[黃至剛出任馬會新行政總裁](../Page/黃至剛.md "wikilink")。[星島日報](http://hk.news.yahoo.com/060928/60/1tuol.html)、[香港賽馬會](http://www.hkjc.com/chinese/news/news_2006092814039.htm)
  - [食物環境衞生署證實香港約](../Page/食物環境衞生署.md "wikilink")2000個[誘蚊產卵器當中有約](../Page/誘蚊產卵器.md "wikilink")200個受到人為干擾，使**[蚊患指數](../Page/蚊患指數.md "wikilink")**急跌。
    [明報](http://hk.news.yahoo.com/060928/12/1tuat.html)
  - 2006年第116期**[六合彩](../Page/六合彩.md "wikilink")**，攪出號碼5、15、22、30、42、49，特別號碼37。頭獎一注獨得61,711,125港元，為歷來第二高彩金。
    [明報](http://hk.news.yahoo.com/060928/12/1tuc3.html)
  - **[香港迪士尼樂園](../Page/香港迪士尼樂園.md "wikilink")**宣佈推出年票，成人票價由650港元至1800港元不等。
    [明報](http://hk.news.yahoo.com/060928/12/1tuba.html)
  - [台灣衛生署宣佈](../Page/台灣衛生署.md "wikilink")，**[保濟丸](../Page/保濟丸.md "wikilink")**及**[保心安油](../Page/保心安油.md "wikilink")**等10種港產中成藥因未獲[藥品優良製造標準](../Page/藥品優良製造標準.md "wikilink")（GMP）認證，由[2006年](../Page/2006年.md "wikilink")[10月1日起禁止](../Page/10月1日.md "wikilink")[進口](../Page/進口.md "wikilink")。
    [明報](http://hk.news.yahoo.com/060928/12/1tub8.html)
  - 政府決定於[2007年起在](../Page/2007年.md "wikilink")[屯門區](../Page/屯門區.md "wikilink")、[灣仔區](../Page/灣仔區.md "wikilink")、[黃大仙區和](../Page/黃大仙區.md "wikilink")[西貢區的](../Page/西貢區.md "wikilink")**[區議會](../Page/香港區議會.md "wikilink")**試行改革，讓其管理區內社區設施。
    [明報](http://hk.news.yahoo.com/060928/12/1tuav.html)
  - [中環](../Page/中環.md "wikilink")**[香港文華東方酒店](../Page/香港文華東方酒店.md "wikilink")**重新開放，由[利孝和夫人](../Page/利孝和.md "wikilink")、[張曼玉等嘉賓進行開幕剪綵](../Page/張曼玉.md "wikilink")。
    [明報](http://hk.news.yahoo.com/060928/12/1tu9r.html)

## [9月29日](../Page/9月29日.md "wikilink")

  - [行政長官](../Page/香港特別行政區行政長官.md "wikilink")**[曾蔭權](../Page/曾蔭權.md "wikilink")**在[中大為](../Page/香港中文大學.md "wikilink")[逸夫書院擔任月會演講嘉賓](../Page/逸夫書院.md "wikilink")，講述自己「務實領導」的政治理念。十多名[民間爭取最低工資聯盟的成員在會場外示威](../Page/民間爭取最低工資聯盟.md "wikilink")，向曾蔭權遞交請願信。[明報](https://web.archive.org/web/20070518150911/http://hk.news.yahoo.com/060929/12/1twp6.html)、[東方日報](http://hk.news.yahoo.com/060929/10/1tx1w.html)

## [9月30日](../Page/9月30日.md "wikilink")

  - 因遇上強風，[昂坪360早上延遲開放](../Page/昂坪360.md "wikilink")40分鐘。上午10時50分，[纜車訊號故障](../Page/纜車.md "wikilink")，450名乘客被困空中23分鐘。[星島日報](http://hk.news.yahoo.com/060930/60/1txsa.html)
  - [香港藝術館由即日至](../Page/香港藝術館.md "wikilink")[12月3日展出](../Page/12月3日.md "wikilink")[法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[龐比度中心五十八項展品](../Page/龐比度中心.md "wikilink")，有三分之一的作品首次於[亞洲地區展出](../Page/亞洲.md "wikilink")。[香港政府新聞稿](http://www.info.gov.hk/gia/general/200609/29/P200609290141.htm)

-----

[Category:2006年香港](../Category/2006年香港.md "wikilink")
[Category:2006年9月](../Category/2006年9月.md "wikilink")