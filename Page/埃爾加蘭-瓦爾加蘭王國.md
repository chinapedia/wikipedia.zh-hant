[Flag_of_Elgaland_Vargaland.jpg](https://zh.wikipedia.org/wiki/File:Flag_of_Elgaland_Vargaland.jpg "fig:Flag_of_Elgaland_Vargaland.jpg")

**埃爾加蘭-瓦爾加蘭王國**（）是[瑞典](../Page/瑞典.md "wikilink")[艺术家](../Page/艺术家.md "wikilink")[雷夫·埃尔格伦](../Page/雷夫·埃尔格伦.md "wikilink")（Leif
Elggren）和[卡尔·米歇尔·冯·豪斯沃尔夫](../Page/卡尔·米歇尔·冯·豪斯沃尔夫.md "wikilink")（Carl
Michael von
Hausswolff）在1992年构想并实施的建国计划，该国物质领土涵盖地球上所有[国家之间的边境区域](../Page/国家.md "wikilink")，所有国家[领海外的海域](../Page/领海.md "wikilink")；并涵盖所有心理感知领土，数字领土。

該國家並非實質存在的國家，而是兩位藝術家的藝術概念，兩位藝術家設計了該國的[國歌](../Page/國歌.md "wikilink")、[郵票](../Page/郵票.md "wikilink")、[護照與](../Page/護照.md "wikilink")[憲法等等內容](../Page/憲法.md "wikilink")。2007年，兩位藝術家宣稱，[圣米凯莱岛公墓上的](../Page/圣米凯莱岛.md "wikilink")「居民」皆擁有該國國籍，除非他們提出拒絕申請。當然，目前沒有收到任何拒絕國籍的申請。

## 參考文獻

  - 兰蒂·肯尼迪著，魏保珠译，〈[威尼斯双年展
    艺术家为自己的领土插上国旗](https://web.archive.org/web/20090108195249/http://www.chinaculture.org/gb/cn_focus/2007-06/11/content_99408.htm)〉

## 外部链接

  - [埃爾加蘭-瓦爾加蘭王國官方网站](http://www.elgaland-vargaland.org/)
  - [埃爾加蘭-瓦爾加蘭王國宪法](https://web.archive.org/web/20060525232926/http://www.elgaland-vargaland.org/constitution/index.html)
  - [埃爾加蘭-瓦爾加蘭王國历史](https://web.archive.org/web/20060512120647/http://www.elgaland-vargaland.org/history/index.html)

{{-}}

[Category:藝術作品](../Category/藝術作品.md "wikilink")
[Category:王國](../Category/王國.md "wikilink")
[Category:私人國家](../Category/私人國家.md "wikilink")
[Category:網絡國家](../Category/網絡國家.md "wikilink")