[Undertaker_oldschool.jpg](https://zh.wikipedia.org/wiki/File:Undertaker_oldschool.jpg "fig:Undertaker_oldschool.jpg")[選手](../Page/選手.md "wikilink")[Undertaker正在使用Old](../Page/送葬者_\(摔角手\).md "wikilink")
School进行职业摔跤比赛\]\] **職業摔跤**（[英語](../Page/英語.md "wikilink")：Professional
Wrestling），為任何以[表演方式進行的](../Page/表演.md "wikilink")[摔角比賽](../Page/摔角.md "wikilink")，而比賽內容及結果都經事先計劃\[1\]。

## 相關條目

  - [摔角](../Page/摔角.md "wikilink")
  - [美國職業摔角冠軍爭霸戰](../Page/美國職業摔角冠軍爭霸戰.md "wikilink")

## 參考來源

## 外部連結

  - [世界摔角娛樂](http://www.wwe.com)
  - [新日本職業摔角](http://www.njpw.co.jp/)
  - [全日本職業摔角](http://www.all-japan.co.jp/)
  - [NOAH 日本職業摔角](http://www.noah.co.jp/)

[Category:表演艺术](../Category/表演艺术.md "wikilink")
[職業摔跤](../Category/職業摔跤.md "wikilink")

1.  [摔角是真打還是假打？](http://www.chinatimes.com/realtimenews/20150927002582-260403)