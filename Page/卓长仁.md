**卓长仁**（），[中國](../Page/中國.md "wikilink")[遼寧省](../Page/遼寧省.md "wikilink")[沈阳市人](../Page/沈阳市.md "wikilink")。1982年於[中國大陸犯案後](../Page/中國大陸.md "wikilink")，試圖逃避法律制裁，劫持從[瀋陽飛往](../Page/瀋陽.md "wikilink")[上海的](../Page/上海.md "wikilink")[中國民航296號航班飛往](../Page/中国民航296号航班劫机事件.md "wikilink")[韓國](../Page/韓國.md "wikilink")，後被[中華民國政府接往](../Page/中華民國政府.md "wikilink")[台灣](../Page/台灣.md "wikilink")，尊為[反共義士](../Page/反共義士.md "wikilink")。1991年因犯下綁架殺人罪，被判決[死刑](../Page/死刑.md "wikilink")，2001年伏法。

## 生平

卓长仁原在[中國大陸擔任](../Page/中國大陸.md "wikilink")[監獄管理員](../Page/監獄.md "wikilink")，1982年於大陆犯法後，為逃避法律制裁，夥同姜洪軍、[高東萍](../Page/高東萍.md "wikilink")、王豔大、安偉建及吳雲飛共六人，於1983年5月5日劫持沈阳飞往[上海的](../Page/上海.md "wikilink")[中国民航296号航班](../Page/六義士.md "wikilink")，飞往当时尚未与中華人民共和國建交的[大韩民国](../Page/大韩民国.md "wikilink")。六人後被韓方依[劫機罪判處二年到六年不等的](../Page/劫機.md "wikilink")[有期徒刑](../Page/有期徒刑.md "wikilink")，但當時[中華民國政府出面營救六人並給予](../Page/中華民國政府.md "wikilink")[政治庇護](../Page/政治庇護.md "wikilink")，六人在韓被[羈押一年三個月後](../Page/羈押.md "wikilink")，於1984年8月獲韓方[特赦](../Page/特赦.md "wikilink")，[驅逐出境并被遣送到台湾](../Page/驅逐出境.md "wikilink")\[1\]。六人抵台後，中華民國政府給予高規格接待，把六人冠上「[六义士](../Page/六义士.md "wikilink")」美名，並依《[反共義士起義來歸獎勵條例](../Page/反共義士.md "wikilink")》給六人豐渥的千[兩](../Page/兩.md "wikilink")[黃金等巨額獎金](../Page/黃金.md "wikilink")\[2\]。身為劫機主謀的卓長仁不顧在瀋陽已有妻室，於1988年與當年一起劫機的高東萍在台結婚並育有兩子。卓長仁元配後來雖跨海控告兩人[重婚罪並勝訴](../Page/重婚.md "wikilink")，但卓高兩人始終維持[同居關係](../Page/同居.md "wikilink")\[3\]。

卓长仁、姜洪军與另一名「反共義士」[施小宁三人後因在台工作始終不穩定](../Page/施小宁.md "wikilink")，所獲得的巨額獎金又因揮霍及不當投资[期货等因素迅速消耗殆盡](../Page/期货.md "wikilink")，三人於负债累累情況下乃决定铤而走险，於1991年8月16日共同[绑架经营土地仲介的前](../Page/绑架.md "wikilink")[台北市](../Page/台北市.md "wikilink")[国泰医院副院长王欲明之子王俊傑](../Page/国泰医院.md "wikilink")，並將其撕票（殺害）。翌年，全案在時任[桃園地檢署](../Page/桃園地檢署.md "wikilink")[檢察官邱鎮北主導下偵破](../Page/檢察官.md "wikilink")，後經四次發回[台灣高等法院更審](../Page/台灣高等法院.md "wikilink")，[中華民國最高法院於](../Page/中華民國最高法院.md "wikilink")2000年9月22日判處卓长仁、姜洪军[死刑](../Page/死刑.md "wikilink")，施小宁[無期徒刑確定](../Page/無期徒刑.md "wikilink")\[4\]。高東萍於判決確定後極力營救卓长仁，後經[監察院同意重新調查此一案件](../Page/監察院.md "wikilink")，[法務部罕見地於當時暫緩兩人死刑執行](../Page/法務部.md "wikilink")\[5\]。後因監察院歷經八個月調查後仍未能找出足以改變判決結果的新事證，時任法務部部長[陳定南於](../Page/陳定南.md "wikilink")2001年8月9日批准卓姜兩人死刑執行令，翌日晚間兩人於[台北看守所執行](../Page/台北看守所.md "wikilink")[槍決](../Page/槍斃.md "wikilink")\[6\]，卓长仁時年53歲、姜洪军時年41歲。

## 参考文献

[Category:中華民國罪犯](../Category/中華民國罪犯.md "wikilink")
[Category:中華人民共和國罪犯](../Category/中華人民共和國罪犯.md "wikilink")
[Category:被中華民國處決者](../Category/被中華民國處決者.md "wikilink")
[Category:被處決的中華民國人](../Category/被處決的中華民國人.md "wikilink")
[Category:中華人民共和國脫逃者](../Category/中華人民共和國脫逃者.md "wikilink")
[Category:台灣戰後東北移民](../Category/台灣戰後東北移民.md "wikilink")
[Category:沈陽人](../Category/沈陽人.md "wikilink")
[Chang長](../Category/卓姓.md "wikilink")
[Category:台灣死刑案件](../Category/台灣死刑案件.md "wikilink")
[Category:反共義士](../Category/反共義士.md "wikilink")

1.  [李登輝「重要記事」手稿](http://web.drnh.gov.tw/%E6%9D%8E%E7%99%BB%E8%BC%9D/museum/museum_3.htm)

2.  [六義士境遇大不同　有人遭槍決、有人變老闆、有人成貧戶](http://www.nownews.com/2005/06/10/545-1802000.htm)

3.  [救夫失敗
    高東萍直說不甘心](http://www.libertytimes.com.tw/2001/new/aug/11/today-c7.htm)


4.  [卓長仁死刑定讞　卓妻高東萍決提非常上訴](http://www.nownews.com/2000/09/27/322-218134.htm)

5.
6.  [綁架撕票
    卓長仁、姜洪軍槍決](http://www.libertytimes.com.tw/2001/new/aug/11/today-c6.htm)