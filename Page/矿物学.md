**礦物學**是運用[物理學](../Page/物理學.md "wikilink")（如[X光繞射](../Page/X光.md "wikilink")）、[化學方法](../Page/化學.md "wikilink")（化學計量）等不同領域來研究[礦物的物理性質](../Page/礦物.md "wikilink")（包括光學性質）、化學性質、[晶體結構](../Page/晶體結構.md "wikilink")、自然分布和狀態的一門科學。在礦物學中，具體研究包括礦物的起源和形成的過程，礦物質的分類，它們的地理分佈，以及它們的利用率。

## 礦物與對稱概念

晶格概念、基本[對稱元素](../Page/對稱.md "wikilink")。推測並模擬礦物生成的環境，在實驗室中生成礦物。

## 晶体结构

  - 複合對稱元素、對稱元素的組合。
  - 對稱元素的組合、三十二个[點群的導出](../Page/點群.md "wikilink")。
  - 它们一起组成一个数学对象称为**[晶体学点群](../Page/晶体学点群.md "wikilink")**或**晶类**。
  - [米勒指數和極線赤平投影](../Page/米勒指數.md "wikilink")。
  - 晶類、晶軸和晶系。
  - 晶形。
  - 晶體之內部有序對稱。
  - 晶類的鑑定。
  - 晶體之共生。

## [結晶化學](../Page/結晶化學.md "wikilink")

  - 原子結構、原子和離子半徑、以及[化學鍵結](../Page/化學鍵.md "wikilink")。
  - [金屬鍵晶體](../Page/金屬鍵.md "wikilink")、[共價鍵晶體和](../Page/共價鍵.md "wikilink")[離子鍵晶體](../Page/離子鍵.md "wikilink")。
  - [鲍林酸碱经验规律和](../Page/鲍林酸碱经验规律.md "wikilink") AX 結晶構造類型。
  - AX2、A2X 和 A2X3 結晶構造類型。
  - AmBnXz 結晶構造類型和衍生結晶構造。

## [矽酸鹽礦物學](../Page/矽酸鹽礦物學.md "wikilink")

  - 島狀、單鏈狀和雙鏈狀矽酸鹽礦物。
  - 片狀、環狀矽酸鹽礦物。
  - 架狀矽酸鹽礦物。

## [金屬礦物學](../Page/金屬礦物學.md "wikilink")

各金屬性質及用途

## [非金屬礦物學](../Page/非金屬礦物學.md "wikilink")

矽酸鹽外之非金屬礦物性質及用途

## 参见

  - [矿物列表](../Page/矿物列表.md "wikilink")
  - [冶金学](../Page/冶金学.md "wikilink")
  - [材料科学](../Page/材料科学.md "wikilink")
  - [岩石学](../Page/岩石学.md "wikilink")

## 参考资料

[\*](../Category/礦物學.md "wikilink")
[Kuang4](../Category/矿物.md "wikilink")