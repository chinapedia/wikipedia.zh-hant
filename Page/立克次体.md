**立克次体**（学名：**），或者称**立克次氏体**，是一类[细菌](../Page/细菌.md "wikilink")，但许多特征和[病毒一样](../Page/病毒.md "wikilink")，如不能在[培养基上培养](../Page/培养基.md "wikilink")，可以通过[瓷濾器过滤](../Page/瓷濾器.md "wikilink")，只能在[动物](../Page/动物.md "wikilink")[细胞内寄生繁殖等](../Page/细胞.md "wikilink")。直径只有0.3-1[μm](../Page/μm.md "wikilink")，小于绝大多数细菌。立克次体有细胞形态，除[恙虫病立克次体外](../Page/恙蟲東方體.md "wikilink")，[细胞壁含有细菌特有的](../Page/细胞壁.md "wikilink")[肽聚糖](../Page/肽聚糖.md "wikilink")\[1\]。细胞壁为双层结构，其中[脂类含量高于一般细菌](../Page/脂类.md "wikilink")，无[鞭毛](../Page/鞭毛.md "wikilink")。同时有[DNA和](../Page/DNA.md "wikilink")[RNA两种](../Page/RNA.md "wikilink")[核酸](../Page/核酸.md "wikilink")，但没有核仁及核膜，属于适应了寄生生活的[α-变形菌](../Page/α-变形菌.md "wikilink")，經研究[粒線體的祖先可能是由立克次體演化而來](../Page/粒線體.md "wikilink")\[2\]。革兰染色呈阴性，效果不明显。立克次体取名是为了纪念[美国病理学家](../Page/美国.md "wikilink")[霍華德·泰勒·立克次](../Page/霍華德·泰勒·立克次.md "wikilink")（Howard
Taylor Ricketts，1871年2月9日 -
1910年5月3日），立克次在[芝加哥大学工作期间发现了](../Page/芝加哥大学.md "wikilink")[落磯山斑點熱和](../Page/落磯山斑點熱.md "wikilink")[鼠型斑疹伤寒的病原体](../Page/鼠型斑疹伤寒.md "wikilink")（立克次体）和传播方式，由于工作原因，他自己也死于斑疹伤寒。他所发现的病原体被命名为立克次体属。

## 生长及传播

大多数立克次体需要[寄宿于活体有核细胞](../Page/寄宿.md "wikilink")，繁殖方式为二分裂，约6至10小时繁殖一代。不同的立克次体在细胞内的分布亦有所不同，如普氏立克次体为分散分布，恙虫病立克次体则聚集在[细胞核外表面附近](../Page/细胞核.md "wikilink")，五日热巴通体可粘附在细胞外表面，也可在无细胞的培养基中生长。在[pH为](../Page/pH.md "wikilink")8时生长稳定，因此可算作[嗜碱性细菌](../Page/嗜碱性细菌.md "wikilink")。适宜在32至35摄氏度时生存，也可耐低温与干旱。\[3\]在实验室内多采用鸡[胚胎细胞或小](../Page/胚胎细胞.md "wikilink")[鼠细胞培育](../Page/鼠.md "wikilink")。许多种立克次体可引起人类和动物的严重疾病，有的立克次体对干燥的抵抗能力极强，许多立克次体可侵入[节肢动物体内](../Page/节肢动物.md "wikilink")，如[虱](../Page/虱.md "wikilink")、[蚤](../Page/蚤.md "wikilink")、[蜱](../Page/蜱.md "wikilink")、[螨等](../Page/螨.md "wikilink")，当这些节肢动物叮咬人类或动物时，就会引起疾病，如多种[斑疹伤寒](../Page/斑疹伤寒.md "wikilink")、[斑点热](../Page/斑点热.md "wikilink")、猫抓病、[Q热](../Page/Q型流感.md "wikilink")、埃里希体病、巴通体病等。Q热在少数情况下甚至能通过空气传播\[4\]。致病性立克次体感染人体后，大多会出现头痛不适、发热、出疹等共同症状。立克次体可感染貓，家鼠，負鼠等，被感染的動物不會出現病症，所以不能通過動物外表判斷是否感染立克次體。

## 致病机制

在进入体内后，立克次体先与宿主细胞上的受体结合，进入宿主细胞内，接下来会在局部[淋巴组织或](../Page/淋巴组织.md "wikilink")[血管内表皮组织内繁殖](../Page/血管.md "wikilink")。然后经由淋巴液和血液扩散至全身血管系统内，导致大量细胞破损、出血。血管壁细胞破损后，血管通透性增强，血液渗出，在皮肤上表现为皮疹。\[5\]有些立克次体在侵入宿主时，会释放出溶解磷脂的[磷脂酶A](../Page/磷脂酶.md "wikilink")，大量聚集后会导致细胞破裂。立克次体还会释放[脂多糖](../Page/脂多糖.md "wikilink")，因而导致内皮细胞损伤，出现中毒休克等症状。虽然不同的立克次体症状不同，但主要症状都为血管病变，有时还会出现[血栓](../Page/血栓.md "wikilink")。由血管病变，立克次体还会引起[神经](../Page/神经.md "wikilink")、呼吸、循环系统的并发症。\[6\]

## 症状

被立克次体感染后有约为10天的[潜伏期](../Page/潜伏期.md "wikilink")。发病后的初期症状为发热、头疼，还会出现毒血症症状，如头晕耳鸣、四肢酸疼等。4至5天后感染者开始出现粉红色皮疹，随着时间推移皮疹开始蔓延，严重者全身均可见到皮疹。这一阶段的患者仍有高温，并伴随着神经精神症状，严重者可能精神错乱或昏迷。也可能会伴随着中毒性[心肌炎](../Page/心肌炎.md "wikilink")。4至8日后皮疹开始褪色，体温恢复，症状好转，精神症状的恢复则需要更多时间。年老体弱、[免疫系统受损者可能会发生严重症状乃至死亡](../Page/免疫系统.md "wikilink")。对该类微生物的免疫以[细胞免疫为主](../Page/细胞免疫.md "wikilink")，感染后可获得较强免疫力。\[7\]

## 来源

[Category:細菌](../Category/細菌.md "wikilink")

1.  [第十八章　立克次体](http://www.foodmate.net/lesson/47/18.php)
2.  [粒線體的祖先](http://sa.ylib.com/MagArticle.aspx?Unit=newscan&id=2652)
3.  [Rickettsia
    prowazekii](http://microbiology.scu.edu.tw/micro/bacteria/R1.htm)
4.  [摸狗胎盤！男子染Q熱　高燒不退](http://www.98173.com.tw/forum/Forum_Page.aspx?tid=57922)
5.  [立克次体病科普知识专栏--立克次体与立克次体病](http://www.icdc.cn/news_view.asp?newsid=314)

6.  [立克次体病](http://www.fha.org.cn:8080/jibingku/M/likecitibing.html)
7.  [立克次体感染](http://www.daifumd.com/_daifumd/blog/html/592/article_48795.html)