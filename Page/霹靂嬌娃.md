《**霹靂嬌娃**》，-{zh-hans:港译《**神探俏娇娃**》;zh-hant:港譯《**神探俏嬌娃**》;zh-tw:港譯《**神探俏嬌娃**》;zh-hk:台譯《**霹靂嬌娃**》;zh-mo:台譯《**霹靂嬌娃**》;}-，原名是，是一部喜劇動作影集。這部影集最初播放的時間是1976年至1981年，是一個描述三個女生為一個虛構的[私家偵探機構工作的故事](../Page/私家偵探.md "wikilink")，她們的老闆名叫查理（配音），是一個從來不在影片中現身的角色，僅用電話與她們聯絡。在影集結束時他曾以一個全副裝備打扮的-{zh-hant:[外科醫生](../Page/外科醫生.md "wikilink");zh-hans:[外科医生](../Page/外科医生.md "wikilink");}-現身。

最初的三位嬌娃是由[凱蒂·傑克遜](../Page/凱蒂·傑克遜.md "wikilink")、[花拉·科茜](../Page/花拉·科茜.md "wikilink")、[賈桂琳·史密斯](../Page/賈桂琳·史密斯.md "wikilink")。之後當演員因為其他因素離開劇組，會有新的演員來遞補嬌娃的位子，使成員保持在三個人的狀態。之後演出嬌娃的演員尚有[雪莉·賴德](../Page/雪莉·賴德.md "wikilink")、[Shelley
Hack和](../Page/Shelley_Hack.md "wikilink")[譚雅·羅拔絲](../Page/譚雅·羅拔絲.md "wikilink")；[David
Doyle扮演嬌娃們的助手](../Page/David_Doyle.md "wikilink")"Bosley"。

## 電影

以電視影集為主幹發展出了兩部電影，分別是2000年的《**[霹靂嬌娃](../Page/霹靂嬌娃_\(電影\).md "wikilink")**》（*Charlie's
Angels*）和2003年的《**[霹靂嬌娃2：全速進攻](../Page/霹靂嬌娃2：全速進攻.md "wikilink")**》（*Charlie's
Angels: Full
Throttle*）。兩片皆由[麥克G執導](../Page/麥克G.md "wikilink")，由[卡麥蓉·狄亞](../Page/卡麥蓉·狄亞.md "wikilink")、[茱兒·芭莉摩和](../Page/茱兒·芭莉摩.md "wikilink")[劉玉玲主演三位嬌娃](../Page/劉玉玲.md "wikilink")，並請回[John
Forsythe來為查理配音](../Page/John_Forsythe.md "wikilink")。主题曲为[天命真女演唱的](../Page/天命真女.md "wikilink")《[Independent
woman](../Page/Independent_woman.md "wikilink")》。亞洲版[劉玉玲](../Page/劉玉玲.md "wikilink")、臺灣女星[方季惟](../Page/方季惟.md "wikilink")。

## 美國以外播出記錄

  - 香港：[電視廣播有限公司](../Page/電視廣播有限公司.md "wikilink")
  - 台灣：[中華電視公司](../Page/中華電視公司.md "wikilink")（播出時間：1977年4月30日－1981年11月22日）

## 外部連結

  -
  -
  -
  -
  -
[Category:1976年開播的美國電視影集](../Category/1976年開播的美國電視影集.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:華視外購電視劇](../Category/華視外購電視劇.md "wikilink")
[Category:间谍题材电视剧](../Category/间谍题材电视剧.md "wikilink")
[Category:美国犯罪电视剧](../Category/美国犯罪电视剧.md "wikilink")
[Category:英语电视剧](../Category/英语电视剧.md "wikilink")
[Category:虚构私家侦探](../Category/虚构私家侦探.md "wikilink")
[Category:行動電話遊戲](../Category/行動電話遊戲.md "wikilink")
[Category:索尼影视电视公司制作的电视节目](../Category/索尼影视电视公司制作的电视节目.md "wikilink")
[Category:洛杉磯背景電視節目](../Category/洛杉磯背景電視節目.md "wikilink")
[Category:夏威夷州背景电视节目](../Category/夏威夷州背景电视节目.md "wikilink")
[Category:美国动作电视剧](../Category/美国动作电视剧.md "wikilink")
[Category:被取消后又复活的电视节目](../Category/被取消后又复活的电视节目.md "wikilink")
[Category:美國廣播公司電視節目](../Category/美國廣播公司電視節目.md "wikilink")
[Category:偵探主角題材電視劇](../Category/偵探主角題材電視劇.md "wikilink")
[Category:1981年停播的美國電視影集](../Category/1981年停播的美國電視影集.md "wikilink")