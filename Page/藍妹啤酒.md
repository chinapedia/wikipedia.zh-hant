[HK_CWB_Pinnington_Street_Blue_Girl_捷成洋行_Jebsen_&_Co_Vehicle.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_Pinnington_Street_Blue_Girl_捷成洋行_Jebsen_&_Co_Vehicle.JPG "fig:HK_CWB_Pinnington_Street_Blue_Girl_捷成洋行_Jebsen_&_Co_Vehicle.JPG")
[HK_drink_SW_Parkn_shop_goods_Beer_bottles_藍妹啤酒_Blue_Girl_June-2013.JPG](https://zh.wikipedia.org/wiki/File:HK_drink_SW_Parkn_shop_goods_Beer_bottles_藍妹啤酒_Blue_Girl_June-2013.JPG "fig:HK_drink_SW_Parkn_shop_goods_Beer_bottles_藍妹啤酒_Blue_Girl_June-2013.JPG")
**藍妹啤酒**（）隶属于[香港](../Page/香港.md "wikilink")[捷成洋行](../Page/捷成洋行.md "wikilink")，是一个源于德国的具有百年历史的啤酒品牌。

蓝妹啤酒源于德国北部城市-[不莱梅](../Page/不莱梅.md "wikilink")，自19世纪初酿至今，已拥有超过百年的历史。早在1895年以前，蓝妹啤酒已经出口到中国青岛，专门供当地的德国官员飲用。

1906年，源于香港的捷成洋行收购了蓝妹啤酒，再沿用其德国传统秘方酿制，并正式引入香港及大陆市场销售。现今蓝妹啤酒由香港捷成洋行经营，在韓國釀造，在中国大陆、香港、澳门和台湾市场都有销售。据公司公开资料，蓝妹啤酒在香港市场占有率超过20%\[1\]。

原装进口蓝妹啤酒采用黄金麦芽、啤酒花、水、酵母及玉米酿制而成，属于皮尔森拉格型啤酒。蓝妹啤酒酒色金黄，入口带浓郁的麦花味，清新爽口。

## 产品资料

原料: 水，麦芽，啤酒花，酵母菌，谷物

酒精含量: 5% alc./vol (香港，澳門及台灣)，4.5%./vol (中國)

產品包裝:
640毫升 x 12樽
330毫升 x 24罐
330毫升 x 24樽
500毫升 x 24罐

## 参考

<references/>

## 外部連結

  - [藍妹啤酒中国大陆官網](http://www.bluegirl.com.cn/)
  - [藍妹啤酒香港官網](http://www.bluegirlbeer.com/)
  - [藍妹啤酒台灣官網](http://www.bluegirlbeer.com.tw/)

[Category:啤酒品牌](../Category/啤酒品牌.md "wikilink")
[Category:香港飲料](../Category/香港飲料.md "wikilink")
[Category:香港品牌](../Category/香港品牌.md "wikilink")

1.