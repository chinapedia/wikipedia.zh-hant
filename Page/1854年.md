## 大事记

  - [3月27日](../Page/3月27日.md "wikilink")－[法國參與](../Page/法國.md "wikilink")[克里米亞戰爭](../Page/克里米亞戰爭.md "wikilink")。
  - [3月31日](../Page/3月31日.md "wikilink")－[美國代表](../Page/美國.md "wikilink")[马休·佩里與](../Page/马休·佩里.md "wikilink")[日本代表](../Page/日本.md "wikilink")[林复斋於](../Page/林复斋.md "wikilink")[神奈川簽訂](../Page/神奈川縣.md "wikilink")《[日美親善條約](../Page/日美神奈川條約.md "wikilink")》，[德川幕府的](../Page/江户幕府.md "wikilink")[鎖國政策結束](../Page/鎖國政策.md "wikilink")。
  - [7月5日](../Page/7月5日.md "wikilink")－[广东省](../Page/广东省.md "wikilink")[天地会首领](../Page/天地会.md "wikilink")[陈开在](../Page/陈开.md "wikilink")[佛山起义](../Page/佛山市.md "wikilink")。
  - [7月5日](../Page/7月5日.md "wikilink")－[中国](../Page/中国.md "wikilink")《[上海英法美租地章程](../Page/上海英法美租地章程.md "wikilink")》公布。
  - [7月11日](../Page/7月11日.md "wikilink")，[美國代表](../Page/美國.md "wikilink")[马休·佩里與](../Page/马休·佩里.md "wikilink")[琉球国方面代表](../Page/琉球国.md "wikilink")[尚宏勋](../Page/尚宏勋.md "wikilink")、[马良才于](../Page/马良才.md "wikilink")[首里城签署](../Page/首里城.md "wikilink")[琉美修好条约](../Page/琉美修好条约.md "wikilink")。
  - 首部烟草主题的医学著作《烟草论》在法国发表\[1\]。

## 出生

  - [1月8日](../Page/1月8日.md "wikilink")——[嚴復](../Page/嚴復.md "wikilink")，[中國](../Page/中國.md "wikilink")「西學啟蒙」思想家、翻譯家。（[1921年逝世](../Page/1921年.md "wikilink")）。
  - [4月29日](../Page/4月29日.md "wikilink")——[亨利·庞加莱](../Page/庞加莱.md "wikilink")，[法国](../Page/法国.md "wikilink")[数学家](../Page/数学家.md "wikilink")、[物理学家和](../Page/物理学家.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")。
  - [7月25日](../Page/7月25日.md "wikilink")——[孝哲毅皇后](../Page/孝哲毅皇后.md "wikilink")，同治帝皇后，光绪元年自杀身亡（[1875年逝世](../Page/1875年.md "wikilink")）
  - [10月16日](../Page/10月16日.md "wikilink")——[考茨基](../Page/卡爾·考茨基.md "wikilink")，[德国](../Page/德国.md "wikilink")[社会主义活动家](../Page/社会主义.md "wikilink")。（[1938年逝世](../Page/1938年.md "wikilink")）
  - [10月16日](../Page/10月16日.md "wikilink")——[奥斯卡·王尔德](../Page/奥斯卡·王尔德.md "wikilink")，[爱尔兰](../Page/爱尔兰.md "wikilink")[作家](../Page/作家.md "wikilink")。
  - [10月20日](../Page/10月20日.md "wikilink")——[阿尔图尔·兰波](../Page/阿尔图尔·兰波.md "wikilink")，[法国](../Page/法国.md "wikilink")[诗人](../Page/诗人.md "wikilink")。（[1891年逝世](../Page/1891年.md "wikilink")）
  - [10月27日](../Page/10月27日.md "wikilink")——[威廉·亞力山大·史勿夫](../Page/威廉·亞力山大·史勿夫.md "wikilink")，[英國爵士](../Page/英国.md "wikilink")。[基督少年軍的創辦人](../Page/基督少年軍.md "wikilink")。（[1914年逝世](../Page/1914年.md "wikilink")）
  - [11月8日](../Page/11月8日.md "wikilink")——[里德伯](../Page/约翰尼斯·里德伯.md "wikilink")，[瑞典](../Page/瑞典.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")。
  - [11月21日](../Page/11月21日.md "wikilink")——[本篤十五世](../Page/本篤十五世.md "wikilink")，[教皇](../Page/教宗.md "wikilink")。（[1922年逝世](../Page/1922年.md "wikilink")）

## 逝世

  - [7月7日](../Page/7月7日.md "wikilink")——[欧姆](../Page/歐姆.md "wikilink")，[德国](../Page/德国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，逝世于[慕尼黑](../Page/慕尼黑.md "wikilink")。（[1787年出生](../Page/1787年.md "wikilink")）
  - [8月20日](../Page/8月20日.md "wikilink")——[弗里德里希·谢林](../Page/弗里德里希·谢林.md "wikilink")，德国[哲学家](../Page/哲学家.md "wikilink")，在前往[瑞士的旅行途中逝世于](../Page/瑞士.md "wikilink")[巴德拉格斯](../Page/巴德拉格斯.md "wikilink")（Bad
    Ragaz）。（[1775年出生](../Page/1775年.md "wikilink")）

## 参考资料

[\*](../Category/1854年.md "wikilink")
[4年](../Category/1850年代.md "wikilink")
[5](../Category/19世纪各年.md "wikilink")

1.  《烟火撩人-香烟的历史》（法）[迪迪埃·努里松P](../Page/迪迪埃·努里松.md "wikilink")52