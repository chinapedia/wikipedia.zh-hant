**大阪工业大学**（[大坂工业大学](../Page/大坂工业大学.md "wikilink"), おおさかこうぎょうだいがく，Osaka
Institute of Technology，簡稱**大工大**,
**阪工大**、**OIT**）是一所位於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市的](../Page/大阪市.md "wikilink")[私立大学](../Page/私立大学.md "wikilink")，創立於1949年。是日本[关西地区历史悠久的](../Page/关西地区.md "wikilink")[工业大学](../Page/工业大学.md "wikilink")。

## 历史

**大阪工业大学**（[OIT](../Page/OIT.md "wikilink")）最初由“本庄京叁郎”(Kyosaburo
Honjo)和“[片冈安](../Page/片冈安.md "wikilink")”（Yasushi
Kataoka)于[1922年成立为](../Page/1922年.md "wikilink")**关西工学专修学校**。“[片冈安](../Page/片冈安.md "wikilink")”是著名的[建筑师](../Page/建筑师.md "wikilink")，毕业于[东京帝国大学](../Page/东京帝国大学.md "wikilink")(\*现任[东京大学](../Page/东京大学.md "wikilink"))，成为第一任校长。任务是“发展具有科学实践技能的专家，为世界，为人民群众和社区发挥重要作用”。
[1927年](../Page/1927年.md "wikilink")[关西高等工学校创建](../Page/关西高等工学校.md "wikilink")。其后，从关西高等工学校改变于[1940年到](../Page/1940年.md "wikilink")[关西高等工业学校](../Page/关西高等工业学校.md "wikilink")。关西高等工业学校于[1942年更名为摄南高等工业学校](../Page/1942年.md "wikilink")，并于[1944年更名为摄南工业专门学校](../Page/1944年.md "wikilink")。[1949年](../Page/1949年.md "wikilink")4月，摄南工业专门学校升级为摄南工业大学，这是一所隶属于日本新教育系统的四年制大学。
[1949年](../Page/1949年.md "wikilink")10月，它更名为**大阪工业大学**（**[大工大](../Page/大工大.md "wikilink")**）。
当时只有工程学院作为单科的大学。其后，[1996年设置了信息科学学院](../Page/1996年.md "wikilink")。[2003年设置了](../Page/2003年.md "wikilink")[日本第一个知识产权学院](../Page/日本第一个.md "wikilink")。[2017年设置了机器人学](../Page/2017年.md "wikilink")＆设计工程学院。为了学习掌握理论及其实践技术，致力于培养在实际社会中活跃的专业人材。大工大也设有工程研究生院，机器人学＆设计工程研究生院，信息科学研究生院和知识产权研究生院。还设有多个本科，硕士和博士专业，如都市设计工程，建筑学，电子信息通信工程，电气电子系统工程，机械工程，应用化学，环境工程，生体医工程，机器人工程，空间设计，系统设计工程，计算机科学，信息网路，信息系统，信息媒体，知识产权学等。特别是，[知识产权学院和](../Page/知识产权学院.md "wikilink")[知识产权研究生院是](../Page/知识产权研究生院.md "wikilink")[日本唯一的第一所教师](../Page/日本唯一的.md "wikilink")。此外，[机器人学&设计工程学院提供](../Page/机器人学&设计工程学院.md "wikilink")”[設計思考](../Page/設計思考.md "wikilink")”教育作为创新和创造性的方法。
大工大的教学理念是要像在现场一样教授现场最先进的理论和技术。

## 概况

截至2017年4月，大工大有4个学院和4个研究生院，以及4个领域提供16个本科学位，6个硕士学位和5个博士学位。
截至2018年5月，在校生总数约7,200多人。本科生6,753人（其中：工程学院3,950人，机器人学&设计工程学院568人，信息科学学院1,652人，知识产权学院583人）,
硕士和博士研究生452人。

## 学院

  - [知识产权学院](../Page/知识产权学院.md "wikilink")
      - [知识产权学](../Page/知识产权.md "wikilink")

<!-- end list -->

  - [工程学院](../Page/工程.md "wikilink")
      - [都市設計工程](../Page/都市設計.md "wikilink")
      - [建築学](../Page/建築学.md "wikilink")
      - [电子](../Page/电子.md "wikilink")[信息](../Page/信息.md "wikilink")[通信工程](../Page/通信工程.md "wikilink")
      - [電氣](../Page/電氣.md "wikilink")[電子系統工程](../Page/電子.md "wikilink")
      - [機械工程](../Page/機械工程.md "wikilink")
      - 應用[化学](../Page/化学.md "wikilink")
      - [環境工程](../Page/環境工程.md "wikilink")
      - 生體醫工程

<!-- end list -->

  - [机器人学](../Page/机器人学.md "wikilink")&[设计工程学院](../Page/设计工程学院.md "wikilink")
      - [机器人工程](../Page/机器人工程.md "wikilink")
      - [空間设计](../Page/空間设计.md "wikilink")
      - [系统设计工程](../Page/系统设计.md "wikilink")

<!-- end list -->

  - [信息科学学院](../Page/信息科学.md "wikilink")
      - [计算机科学](../Page/计算机科学.md "wikilink")
      - 信息[網路](../Page/網路.md "wikilink")
      - [信息系統](../Page/信息系統.md "wikilink")
      - [信息媒體](../Page/信息媒體.md "wikilink")

## 研究生院

  - [知识产权研究生院](../Page/知识产权研究生院.md "wikilink")
      - [知识产权专攻](../Page/知识产权专攻.md "wikilink")
  - 工程研究生院
      - 建筑学・都市设计工程专攻
      - 电气电子・机械工程专攻
      - 化学・环境・生体医工程专攻
  - [机器人学](../Page/机器人学.md "wikilink")＆[设计工程研究生院](../Page/设计工程研究生院.md "wikilink")
      - 机器人学＆设计工程专攻
  - 信息科学研究生院
      - 信息科学专攻

## 校区

大工大有三个校区。

### 大宫校区

〒535-8585　大阪市旭区大宮5丁目16-1
使用学院：工程学院、知识产权学院
使用研究生院：工程研究生院、知识产权研究生院
交通：

  - 大阪市营地铁谷町线千林大宮车站下车，或者谷町线・今里筋线太子桥今市车站下车，分别步行12分钟即达
  - 京阪本线・千林车站下车，步行20分钟
  - 大阪市营巴士・中宮（大阪工大前）下车即到

### 梅田校区

〒530-8568　大阪市北区茶屋町1-45
使用学院：机器人学＆设计工程学院
使用研究生院：机器人学＆设计工程研究生院，知识产权研究生院
交通：

  - JR西日本・大阪车站步行5分钟
  - 阪急・梅田车站步行3分钟
  - 阪神・梅田车站步行7分钟
  - 大阪市営地铁御堂筋线・梅田车站步行5分钟
  - 大阪市営地铁谷町线・东梅田车站步行5分钟

### 枚方校区

〒573-0196　大阪府枚方市北山1丁目79-1
使用学院：信息科学学院
使用研究生院：信息科学研究生院
交通：

  - JR西日本学研都市线（片町线）・長尾车站以及京阪本线・樟叶站到京阪巴士「大阪工大」、或者「北山中央」停留所下车。从長尾车站、步行约24分钟、巴士约7分钟、也有直达巴士。

## 附屬機構

  - 八幡實驗工學實驗場
      - 構造實驗中心
      - 水理實驗中心
      - 高壓電實驗中心
  - 機械工作中心
      - 切削加工部門
      - 鑄造部門
      - 溶接部門
  - 圖書館
      - 大宮本館圖書館
      - [梅田分館圖書館](../Page/梅田.md "wikilink")
      - 枚方分館圖書館

## 评价

大工大是具备强大就业实力的大学。作为大工大最大的魅力之一就在于其就业方面的实力。支撑着高[实际就职率的](../Page/实际就职率.md "wikilink")，是本校历时多年培养出来的，输送有[实力人才的学风](../Page/实力人才.md "wikilink")。

  - 本科/研究生就业人口占有率（96.9％）
    \*2017年3月毕业者（截止2017年5月1日）
  - [关西地区](../Page/关西地区.md "wikilink")[私立大学](../Page/私立大学.md "wikilink")（包含:
    [关关同立等](../Page/关关同立.md "wikilink")）中连续7年[实际就职率](../Page/实际就职率.md "wikilink")”第1名”
    \*2015年度毕业生实绩。Sunday每日2016年7月31日号，特辑“全国240所大学实际就业率排名（毕业生人数在1000人以上的大学）”大学通信调查。实际就业率=就业人数÷（毕业人数－升学人数）
  - 在“[日经职业杂志特别编辑](../Page/日经.md "wikilink")，最具价值的大学2018年版，就业率排名”中名列前茅。
    \*尤其是毕业生在职场得到高度评价的[行动力和](../Page/行动力.md "wikilink")[独创性这两项](../Page/独创性.md "wikilink")，大工大均在[关西地区](../Page/关西地区.md "wikilink")[私立大学](../Page/私立大学.md "wikilink")（包含:
    [关关同立等](../Page/关关同立.md "wikilink")）中名列榜”第1位”。
    \-综合排名”第29名”
    \-计划增加录用的大学排名”第9名”
    \-单项排名（行动力）”第6名”
    \-单项排名（独创性）”第19名”
    \-分行业排名（非制造业）”第19名”
  - 在日本，来自大工大的[董事总经理人数](../Page/董事总经理.md "wikilink")1,934人，这是日本大学排名”第29名”。而且，在日本的[理工学院和](../Page/理工学院.md "wikilink")[理工大学](../Page/理工大学.md "wikilink")，大工大是继[东京理科大学之后](../Page/东京理科大学.md "wikilink")”第2名”。（2017年调查-
    [帝国数据库Teikoku](../Page/帝国数据库.md "wikilink") Databank）\[1\]

## 创新

### 研究

  - 2012年，由大工大研究员和主要来自[机械工程系的学生开发了一种创新的带有](../Page/机械工程.md "wikilink")[Pulsed
    plasma
    thruster](../Page/Pulsed_plasma_thruster.md "wikilink")（PPT）的[小型卫星](../Page/小型卫星.md "wikilink")，名为[PROITERES](../Page/PROITERES.md "wikilink")
    （Project of OIT Electric-Rocket-Engine Onboard Small Space
    Ship），于2012年9月推出来自[萨迪什·达万航天中心](../Page/萨迪什·达万航天中心.md "wikilink")[PSLV](../Page/PSLV.md "wikilink")-C21[火箭](../Page/火箭.md "wikilink")，由[印度空间研究组织运营](../Page/印度空间研究组织.md "wikilink")。
    \[2\]\[3\]\[4\]
  - 2015年，[应用化学系的大工大研究员与](../Page/应用化学.md "wikilink")[马克斯普朗克研究所共同开发了一种创新技术](../Page/马克斯普朗克研究所.md "wikilink")，即[仿生材料领域的所谓](../Page/仿生.md "wikilink")“非粘性粉状粘合剂”。
    它的开发暗示蚜虫覆盖蜂蜜的表面，它们通过固体蜡颗粒排出，并防止由于[Liquid
    marble中的蜂蜜而导致的蜂蜜溺水](../Page/Liquid_marble.md "wikilink")。它被引入[英国皇家化学学会与](../Page/英国皇家化学学会.md "wikilink")[Chemistry
    World出版的期刊封面](../Page/Chemistry_World.md "wikilink")。 \[5\]\[6\]
  - 2016年，[应用化学系的大工大研究员联合开发了一种创新的材料转移技术](../Page/应用化学.md "wikilink")，将液滴（[Liquid
    marble](../Page/Liquid_marble.md "wikilink")）与固体颗粒和光一起与[旭川医科大学和](../Page/旭川医科大学.md "wikilink")[马克斯普朗克研究所结合在一起](../Page/马克斯普朗克研究所.md "wikilink")。
    由于它使光直接转换为推进力，因此没有像光伏发电那样的功率转换过程。它既环保又低成本。 该研究论文发表在德国科学期刊“ [Advanced
    Functional
    Materials](../Page/Advanced_Functional_Materials.md "wikilink") ”上。
    \[7\]
  - 2017年，[电子](../Page/电子.md "wikilink")[信息](../Page/信息.md "wikilink")[通信工程系的大工大研究员联合开发了一种创新的气味测量平台](../Page/通信工程.md "wikilink")，通过[开放式创新](../Page/开放式创新.md "wikilink")，他们可以与[柯尼卡美能达一起客观地分析和评估各种气味](../Page/柯尼卡美能达.md "wikilink")。
    基于该平台，柯尼卡美能达推出了“坤坤”（或“嗅嗅”）机构。“昆坤”身体，只需一个设备和一个[智能手机应用程序](../Page/智能手机.md "wikilink")，可以测量头皮，腋窝，耳朵后面的皮肤和脚的气味。
    测量的气味数据通过蓝牙发送，并在20秒左右的时间内显示在智能手机应用程序中。
    气味分为10个级别，允许用户客观地决定是否需要采取一些除臭措施。
    大工大一直在研究通过使用气体传感器的基于[神经网络的](../Page/神经网络.md "wikilink")[AI技术来区分不同气味的方法](../Page/AI.md "wikilink")。\[8\]

### 业务

  - 2017年，[日本第一个](../Page/日本第一个.md "wikilink")“
    [机器人服务商学院](../Page/机器人服务商学院.md "wikilink")
    ”由[新能源产业技术综合开发机构](../Page/新能源产业技术综合开发机构.md "wikilink")（[NEDO](../Page/NEDO.md "wikilink")）在大工大梅田校区的[机器人与设计中心推出](../Page/机器人与设计中心.md "wikilink")，旨在提供必要的技术，设计和商业专业知识的综合教育[机器人](../Page/机器人.md "wikilink")[创新](../Page/创新.md "wikilink")
    。\[9\]

## 成就和奖项

  - 国际
      - 来自[大阪大学和大工大的学生联合团队](../Page/大阪大学.md "wikilink")“JoiTech”在2013年[机器人世界杯](../Page/机器人世界杯.md "wikilink")（[RoboCup](../Page/RoboCup.md "wikilink")）为埃因霍温的机器人技术赢得了“
        世界第1名 ”（\*类别：
        [類人類循环赛成人大小](../Page/類人類.md "wikilink")）。\[10\]\[11\]
        此外，大工大自己的[机器人学&设计工程学院团队](../Page/机器人学&设计工程学院.md "wikilink")“OIT
        Trial”与[信息科学学院在名古屋](../Page/信息科学学院.md "wikilink")2017年RoboCup中获得了“世界第7名”（\*类别：@Home）。\[12\]
      - [电气](../Page/电气.md "wikilink")[电子](../Page/电子.md "wikilink")[系统工程的大工大学生团队在](../Page/系统.md "wikilink")[密歇根大学](../Page/密歇根大学.md "wikilink")[迪尔伯恩分校](../Page/迪尔伯恩.md "wikilink")
        [IEEE举办的](../Page/IEEE.md "wikilink")2015年[IFEC](../Page/IFEC.md "wikilink")（
        国际未来能源挑战
        ）国际学生竞赛中获得“世界第3名”和“最佳[创新设计奖](../Page/创新.md "wikilink")”。
        主题是“
        [电动汽车](../Page/电动汽车.md "wikilink")（[EV](../Page/EV.md "wikilink")）和其他应用的高效无线充电系统”。
        作为[日本的大学](../Page/日本的大学.md "wikilink")，大工大参加了决赛，并且有史以来第一次获得该奖项。
        \[13\]
      - [电气](../Page/电气.md "wikilink")[电子](../Page/电子.md "wikilink")[系统工程的大工大学生在](../Page/系统.md "wikilink")2014年[上海](../Page/上海.md "wikilink")[IEEE](../Page/IEEE.md "wikilink")
        [PEAC](../Page/PEAC.md "wikilink")（电力电子应用大会）国际会议上获得“优秀论文奖”。
        主题是“具有单端逆变器驱动双向无线谐振IPT的新型[V2H系统](../Page/V2H.md "wikilink")。\[14\]
      - [信息科学学院的大工大学生团队通过了](../Page/信息科学学院.md "wikilink")2015年[ACM国际大学生程序设计竞赛](../Page/ACM国际大学生程序设计竞赛.md "wikilink")（“[ACM-ICPC](../Page/ACM-ICPC.md "wikilink")”）的日本预赛（82所大学，372支日本队），并参加了筑波的[亚洲区域赛](../Page/亚洲区域赛.md "wikilink")。
        来自[日本的私立大学](../Page/日本的私立大学.md "wikilink")，预选赛只有3所大学，
        [庆应义塾大学](../Page/庆应义塾大学.md "wikilink")
        ，[明治大学和大工大](../Page/明治大学.md "wikilink")。\[15\]
        此外，回到2006年[亚洲区域赛](../Page/亚洲区域赛.md "wikilink")，只有[东京工业大学和大工大在日本技术研究所的](../Page/东京工业大学.md "wikilink")“十大”中，大工大也是日本私立大学中唯一的排名。\[16\]经历过此次活动的大工大毕业生加入了[谷歌](../Page/谷歌.md "wikilink")
        ,
        [雅虎和](../Page/雅虎.md "wikilink")[信息技术](../Page/信息技术.md "wikilink")（[IT](../Page/IT.md "wikilink")）公司。\[17\]
      - [电子](../Page/电子.md "wikilink")[信息](../Page/信息.md "wikilink")[通信工程专业的大工大研究生在夏威夷](../Page/通信工程.md "wikilink")[IEEE电子器件协会组织的](../Page/IEEE.md "wikilink")[EIPBN](../Page/EIPBN.md "wikilink")（[电子](../Page/电子.md "wikilink")，[离子和](../Page/离子.md "wikilink")[光子束技术和纳米加工](../Page/光子.md "wikilink")）国际会议上获得了“学生旅行奖”。
        主题是“在扫描电子显微镜中通过雾化电子产生的绝缘膜的表面电位分布的测量”。
        \[18\]在获得支持的学生中代表了各种大学，国家和主题。
        来自10个国家（包括: [美国](../Page/美国.md "wikilink")）和33所大学（包括:
        [麻省理工学院](../Page/麻省理工学院.md "wikilink")
        ，[斯坦福大学](../Page/斯坦福大学.md "wikilink")
        ，[耶鲁大学](../Page/耶鲁大学.md "wikilink")
        ，[卡内基梅隆大学](../Page/卡内基梅隆大学.md "wikilink")
        ，[普林斯顿大学](../Page/普林斯顿大学.md "wikilink")
        ，[不列颠哥伦比亚大学等](../Page/不列颠哥伦比亚大学.md "wikilink")
        ）的学生获得了学生旅行资助。
        来自[亚洲的大学](../Page/亚洲的大学.md "wikilink")，它只颁发给[新加坡国立大学](../Page/新加坡国立大学.md "wikilink")
        ，[北京大学和大工大](../Page/北京大学.md "wikilink")。\[19\](\*page5-6)
      - 2014年获得“Kenchiku
        SHINJINSEN”（[日本建筑新人赛](../Page/日本建筑新人赛.md "wikilink")）”最高奖”后，
        [建筑学系的大工大学生在](../Page/建筑学.md "wikilink")[大连参加了](../Page/大连.md "wikilink")“第三届[亚洲建筑新人赛](../Page/亚洲建筑新人赛.md "wikilink")
        ”，成为日本国家队的一员。\[20\]

## 留学生支援

### 交流

大工大除基于与海外大学的交流协议接收学生外，还通过”[国际大学生技术经验交流协会](../Page/国际大学生技术经验交流协会.md "wikilink")”（[IAESTE](../Page/IAESTE.md "wikilink")），”[世界知识产权组织](../Page/世界知识产权组织.md "wikilink")”（[WIPO](../Page/WIPO.md "wikilink")）和”[国际协力机构](../Page/国际协力机构.md "wikilink")”（[JICA](../Page/JICA.md "wikilink")）接收进修生。
对于接收的留学生，提供在所属研究室开展研究和[实习](../Page/实习.md "wikilink")、开设课程的特别选修、特别进修项目、[国际PBL](../Page/国际PBL.md "wikilink")（[问题导向学习](../Page/问题导向学习.md "wikilink")）项目。
（以下为2016年的实际成绩）

  - 接收特别进修生（开设课程的特别选修）

\-[坦佩雷理工大学](../Page/坦佩雷理工大学.md "wikilink")（[芬兰](../Page/芬兰.md "wikilink")）：2名，为期12个月（在所属研究室开展研究和实习）
\-[慕尼黑联邦国防大学](../Page/慕尼黑联邦国防大学.md "wikilink")（[德国](../Page/德国.md "wikilink")）：1名，为期6个月
\-[泰国国立法政大学](../Page/泰国国立法政大学.md "wikilink")（[泰国](../Page/泰国.md "wikilink")）：
2名，为期2个月
\-[国立台北科技大学](../Page/国立台北科技大学.md "wikilink")（[台湾](../Page/台湾.md "wikilink")）：10名，为期5周
\*[IAESTE进修生](../Page/IAESTE.md "wikilink")（[匈牙利](../Page/匈牙利.md "wikilink")、[捷克](../Page/捷克.md "wikilink")）：2名，为期2个月（另外4所大学、共接收11人）

  - 特别进修制度

\-[香港城市大学](../Page/香港城市大学.md "wikilink")（[香港](../Page/香港.md "wikilink")）：4名，为期15天
\-[国立高雄第一科技大学](../Page/国立高雄第一科技大学.md "wikilink")、[国立云林科技大学](../Page/国立云林科技大学.md "wikilink")、[世新大学](../Page/世新大学.md "wikilink")（[台湾](../Page/台湾.md "wikilink")
）：22名，为期8天
\-[泰日工业大学](../Page/泰日工业大学.md "wikilink")（[泰国](../Page/泰国.md "wikilink")）：10名，为期3周
（另外2所大学、共接收8人）

  - [国际PBL制度](../Page/国际PBL.md "wikilink")

\-[国立台北科技大学](../Page/国立台北科技大学.md "wikilink")（[台湾](../Page/台湾.md "wikilink")）：15名，为期7天
\-[泰国国立法政大学](../Page/泰国国立法政大学.md "wikilink")（[泰国](../Page/泰国.md "wikilink")）：
10名，为期8天

### 研究生院

大工大有[日本唯一的](../Page/日本唯一的.md "wikilink")”[知识产权研究生院](../Page/知识产权研究生院.md "wikilink")”，是自2005年开设以来，接收了很多来自[中国](../Page/中国.md "wikilink")，[台湾以及](../Page/台湾.md "wikilink")[韩国的留学生们](../Page/韩国.md "wikilink")。目的是成为知识产权研究的专业人士。（\*来自[大连理工大学和](../Page/大连理工大学.md "wikilink")[国立交通大学毕业生等](../Page/国立交通大学.md "wikilink")）在本研究生院毕业的留学生中，或回国作为详知日本知识产权制度的实务家话动，或在日本企业，日本知识产权事务所工作，他/她们活跃在很多地方。\[21\]

  - 大工大在中国的几所大学举行了简报会。（\*在[北京大学和](../Page/北京大学.md "wikilink")[中国人民大学等](../Page/中国人民大学.md "wikilink")）\[22\]\[23\]
  - 秋期入学概要\[24\]

秋期入学申请都准备好了。这主要是针对国内外[公司职员的制度](../Page/公司职员.md "wikilink")。因大工大的教授们都是在实务界著名的企业专们家和专利代理人们的，学生们都会获得提高的知识产有关系的知识和实务能力和課題解決力的。授课里用日语的，所以学生需要日语能力2级(N2)以上（最好N1）的能力。
还有很多的特点。例如这考试就只需要小论文和面试，有的人不能来大学面试，可以用skype面试的。
特别是，可以用相网络的技术之[远隔教育制度](../Page/远隔教育.md "wikilink")。如你用该制度，学生在大阪学习半年和在自国学习一年半可收得知识产硕士（专门职）的资格。

### 学生宿舍

为学生能够在此舒适地生活，在各房间配备浴室、厕所、空调、洗手台、床、桌子、冰箱等等。 配备了洗衣房和可以深入交流的谈话室作为公共设施。
管理员常驻宿舍，对学生提供生活方面的支持。 针对外国留学生设立了住宿费补助制度。

## 姊妹学校

  -   - [国立台北科技大学](../Page/国立台北科技大学.md "wikilink") （National Taipei
        University of Technology）
      - [国立台湾科技大学](../Page/国立台湾科技大学.md "wikilink") （National Taiwan
        University of Science and Technology)
      - [国立清华大学](../Page/国立清华大学.md "wikilink") （National Tsing Hua
        University）
      - [国立虎尾科技大学](../Page/国立虎尾科技大学.md "wikilink") （National Formosa
        University）
      - [国立云林科技大学](../Page/国立云林科技大学.md "wikilink") （National Yunlin
        University of Science and Technology）
      - [国立高雄第一科技大学](../Page/国立高雄第一科技大学.md "wikilink") （National
        Kaohsiung First University of Science and Technology）
      - [南臺科技大学](../Page/南臺科技大学.md "wikilink") （Southern Taiwan
        University of Science and Technology）
      - [世新大学](../Page/世新大学.md "wikilink") （Shih Hsin University）
      - [大同大学](../Page/大同大学.md "wikilink") （Tatung University）

  -   - [香港城市大学](../Page/香港城市大学.md "wikilink") （City University of Hong
        Kong）

  -   - [清华大学](../Page/清华大学.md "wikilink") （Tsinghua University）
      - [浙江大学](../Page/浙江大学.md "wikilink") （Zhejiang University）
      - [同济大学](../Page/同济大学.md "wikilink") （Tongji University）
      - [华东理工大学](../Page/华东理工大学.md "wikilink") （East China University of
        Science and Technology）

  -   - [仁济大学](../Page/仁济大学.md "wikilink") （Inje University）

  -   - [慕尼黑工业大学](../Page/慕尼黑工业大学.md "wikilink") （Technical University
        of Munich）
      - [伍珀塔尔大学](../Page/伍珀塔尔大学.md "wikilink") （University of Wuppertal）

  -   - [代尔夫特理工大学](../Page/代尔夫特理工大学.md "wikilink") （Delft University of
        Technology）

  -   - [维也纳科技大学](../Page/维也纳科技大学.md "wikilink") （Technische Universitat
        Wien）

  -   - [萨拉曼卡大学](../Page/萨拉曼卡大学.md "wikilink") （University of Salamanca）
      - [马德里理工大学](../Page/马德里理工大学.md "wikilink") （Technical University
        of Madrid）

  -   - [乌普萨拉大学](../Page/乌普萨拉大学.md "wikilink") （Uppsala University）

  -   - [斯塔万格大学](../Page/斯塔万格大学.md "wikilink") （University of Stavanger）

  -   - [坦佩雷理工大学](../Page/坦佩雷理工大学.md "wikilink") （Tampere University of
        Technology）

  -   - [阿卜杜勒阿齐兹国王大学](../Page/阿卜杜勒阿齐兹国王大学.md "wikilink") （King Abdulaziz
        University）

  -   - [马来西亚工艺大学](../Page/马来西亚工艺大学.md "wikilink") （Universiti Teknologi
        Malaysia）

  -   - [泰日工業大学](../Page/泰日工業大学.md "wikilink") （Thai-Nichi Institute of
        Technology）

  -   - [马尼帕尔大学](../Page/马尼帕尔大学.md "wikilink") （Manipal University）

  -   - [昆士兰科技大学](../Page/昆士兰科技大学.md "wikilink") （Queensland University
        of Technology）
      - [斯威本理工大学](../Page/斯威本理工大学.md "wikilink") （Swinburne University
        of Technology）

  -   - [佐治亚理工学院](../Page/佐治亚理工学院.md "wikilink") （Georgia Institute of
        Technology）
      - [圣荷西州立大学](../Page/圣荷西州立大学.md "wikilink") （San Jose State
        University）
      - [莱斯大学](../Page/莱斯大学.md "wikilink") （Rice University）
      - [克莱门森大学](../Page/克莱门森大学.md "wikilink") （Clemson University）
      - [安吉罗州立大学](../Page/安吉罗州立大学.md "wikilink") （Angelo State
        University）

## 参考文献

## 外部連結

[大阪工業大學)網站](http://www.oit.ac.jp/)

[Category:大阪府的大學](../Category/大阪府的大學.md "wikilink")
[Category:日本私立大學](../Category/日本私立大學.md "wikilink")
[Category:1949年創建的教育機構](../Category/1949年創建的教育機構.md "wikilink")
[Category:旭區 (大阪市)](../Category/旭區_\(大阪市\).md "wikilink")
[Category:大阪市北區建築物](../Category/大阪市北區建築物.md "wikilink")
[Category:枚方市](../Category/枚方市.md "wikilink")

1.  <http://www.tdb.co.jp/report/watching/press/pdf/p170106.pdf>
2.  <http://www.spaceflight101.net/pslv-c21-launch-updates.html>
3.  <http://erps.spacegrant.org/uploads/images/2015Presentations/IEPC-2015-209_ISTS-2015-b-209.pdf>
4.  <https://directory.eoportal.org/web/eoportal/satellite-missions/p/proiteres>
5.  <http://pubs.rsc.org/en/content/articlehtml/2016/MH/C5MH00203F>
6.  <https://www.chemistryworld.com/news/pressure-brings-liquid-marbles-to-a-sticky-end/9054.article>
7.  <https://phys.org/news/2016-04-liquid-marbles-laser-video.html>
8.  <http://www.dailymail.co.uk/sciencetech/article-4694430/Japanese-App-uses-special-add-check-user-s-BO.html>
9.  <http://www.nedo.go.jp/english/news/AA5en_100101.html>
10. <http://www.robocup2013.org/japan-wins-humanoid-adult-size-final-tech-united-fourth/>
11. <http://www.osaka-u.ac.jp/en/campus/activity/2013/08/20130807>
12. <https://www.oit.ac.jp/japanese/topics/index.php?i=4414>
13. <http://www.energychallenge.org>
14. <http://ieeexplore.ieee.org/document/7037928/>
15. <https://icpc.iisf.or.jp/2015-tsukuba/domestic/standings-and-results/?lang=en>
16. <https://icpc.iisf.or.jp/regional/2006r/>
17. <https://www.oit.ac.jp/japanese/topics/index.php?id=201203001>
18. <http://eipbn.org/2012/>
19. <https://www.osti.gov/scitech/servlets/purl/1060876>
20. <http://554c24c85f15be3.lolipop.jp/2015/asia2015/eng.html>
21. <http://www.oit.ac.jp/ip/graduate/admission/international_students_cn.html>
22. <http://www.iplaw.pku.edu.cn/xzdt/1207029.htm>
23. <http://apil.ruc.edu.cn/news/?306.html>
24. <http://www.oit.ac.jp/ip/graduate/admission/autumn_cn.html>