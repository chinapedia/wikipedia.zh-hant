[Marunouchi_2chome_Building_2012-10-08.JPG](https://zh.wikipedia.org/wiki/File:Marunouchi_2chome_Building_2012-10-08.JPG "fig:Marunouchi_2chome_Building_2012-10-08.JPG")[丸之內的](../Page/丸之內.md "wikilink")「丸之內二丁目大樓」（），為瑞穗金融集團總部所在地。\]\]
**瑞穗金融集團**（；簡稱**瑞穗集團**）是[日本第二大的](../Page/日本.md "wikilink")[金融](../Page/金融.md "wikilink")[機構](../Page/機構.md "wikilink")，由多間[銀行](../Page/銀行.md "wikilink")、[證券商以及金融機構合併而成](../Page/證券.md "wikilink")，2000年成立時曾一舉成為世界最大的金融集團，截至2008年12月31日，總資產高達15462.447億美元。根據統計，瑞穗集團總帳戶數高達3,000多萬，及17萬家企業客戶，並和[東京證交所](../Page/東京證交所.md "wikilink")7成的上市公司都有往來。

瑞穗集團主要由[第一勸業銀行](../Page/第一勸業銀行.md "wikilink")、[富士銀行](../Page/富士銀行.md "wikilink")、[日本興業銀行出資組成](../Page/日本興業銀行.md "wikilink")，後來這三家銀行在2002年整併為[瑞穗銀行](../Page/瑞穗銀行.md "wikilink")，成為集團的核心企業。當時之所以會使用「瑞穗」作為企業名稱，是因為在日本古書《[日本書紀](../Page/日本書紀.md "wikilink")》中，美稱日本為「瑞穗國」（日語原文：），這也象徵了三家銀行以成為「日本銀行的代表典範」的目標。

## 旗下企業

  - [瑞穗銀行](../Page/瑞穗銀行.md "wikilink")（<font lang="ja">みずほ銀行</font>）

<!-- end list -->

  -
    日本三大[商業銀行之一](../Page/商業銀行.md "wikilink")，其[法人前身為](../Page/法人.md "wikilink")[瑞穗實業銀行](../Page/瑞穗實業銀行.md "wikilink")，2013年7月1日將瑞穗銀行（舊法人）吸收合併後更名。

<!-- end list -->

  - [瑞穗信託銀行](../Page/瑞穗信託銀行.md "wikilink")
  - [瑞穗證券](../Page/瑞穗證券.md "wikilink")
  - [UC信用卡](../Page/UC信用卡.md "wikilink")（<font lang="ja">ユーシーカード</font>）

## 參考資料

## 參見

  - [金融控股公司](../Page/金融控股公司.md "wikilink")
  - [日本三大银行集团](../Page/日本三大银行集团.md "wikilink")

## 外部連結

  - [瑞穗金融集團](https://www.mizuho-fg.co.jp)

[瑞穗金融集團](../Category/瑞穗金融集團.md "wikilink")
[Category:芙蓉集團](../Category/芙蓉集團.md "wikilink")
[Category:大阪证券交易所上市公司](../Category/大阪证券交易所上市公司.md "wikilink")
[Category:倫敦證券交易所已除牌公司](../Category/倫敦證券交易所已除牌公司.md "wikilink")
[Category:日本金融控股公司](../Category/日本金融控股公司.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")