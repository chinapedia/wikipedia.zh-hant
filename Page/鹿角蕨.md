**鹿角蕨**為[水龍骨科](../Page/水龍骨科.md "wikilink")[鹿角蕨屬植物](../Page/鹿角蕨屬.md "wikilink")，主要產地在熱帶和亞熱帶地區，約有18種。它們生長於熱帶雨林，以樹表的腐爛有機物為營養，用硬而光滑的蕨葉把持在樹上。雖然它原產熱帶高溫的雨林中，但適應力很強。
它們的主要特徵； 葉分二型， 一是孢子葉(生育葉)，大型，伸展於空氣中，分叉，狀似鹿角，
葉面被有茸毛。能生孢子，孢子囊群集生於葉端背面。二是營養葉(不育葉)，葉片較小，呈圓、橢圓形或扇形，
種類不同而異， 密貼著生於附著物，重疊著生， 初生時為嫩綠色，後變為紙質的淺褐色，有貯存養料和水份的功能。

## 相片

[Platyceriumhillii.jpg](https://zh.wikipedia.org/wiki/File:Platyceriumhillii.jpg "fig:Platyceriumhillii.jpg")似[鹿角的鹿角蕨](../Page/鹿角.md "wikilink")\]\]

## 參考來源

[Category:水龍骨科](../Category/水龍骨科.md "wikilink")
[Category:附生植物](../Category/附生植物.md "wikilink")