**Showtime電視網**（），是[美国一家有線電視網](../Page/美国.md "wikilink")，一家[付費](../Page/付費電視.md "wikilink")[有線](../Page/有線電視.md "wikilink")[電視網](../Page/電視網.md "wikilink")，隸屬於[CBS集團](../Page/CBS集團.md "wikilink")，成立於1976年，主要競爭對手是[HBO](../Page/HBO.md "wikilink")。播放各种电影和原创电视剧，播出完結的影集包括《[同志亦凡人](../Page/同志亦凡人.md "wikilink")》、《[嗜血法醫](../Page/嗜血法醫.md "wikilink")》、《[單身毒媽](../Page/單身毒媽.md "wikilink")》等，近期以《[無恥之徒](../Page/无耻之徒_\(美國電視劇\).md "wikilink")》收視亮眼，除影視節目外，也会播放[拳击比赛](../Page/拳击.md "wikilink")。

## 概況

## 下屬頻道

  - [Showtime頻道](../Page/Showtime頻道.md "wikilink")<small>（1983年開播）</small>
      - Showtime第二頻道（）<small>（1994年開播）</small>
      - Showtime櫥窗頻道（）<small>（1998年開播）</small>
      - Showtime卓絕頻道（）<small>（1998年開播）</small>
      - Showtime超越頻道（）<small>（1998年開播）</small>
      - Showtime次世代頻道（）<small>（2001年開播）</small>
      - Showtime家庭頻道（）<small>（2001年開播）</small>
      - Showtime女性頻道（）<small>（2001年開播）</small>

<!-- end list -->

  - <small>（1983年開播）</small><sup>1</sup>

      - 電影附加頻道（The Movie Channel Xtra）<small>（1999年開播）</small>

<!-- end list -->

  - <small>（1992年開播）</small>

<!-- end list -->

  - <small>（2007年開播）</small><sup>2</sup>

## 大變動

## 外部链接

  - [官方网站](http://www.sho.com)

[Category:美國電視台](../Category/美國電視台.md "wikilink")
[Category:無廣告電視網](../Category/無廣告電視網.md "wikilink")