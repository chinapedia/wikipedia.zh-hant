**卡洛·库迪奇尼**（**Carlo
Cudicini**，），[意大利足球運動員](../Page/意大利.md "wikilink")，擔任[守門員](../Page/守門員.md "wikilink")，於2009年由效力長達10年的[英超球隊](../Page/英超.md "wikilink")[切尔西免費轉投](../Page/切尔西足球俱乐部.md "wikilink")[熱刺](../Page/托特纳姆热刺足球俱乐部.md "wikilink")，及後轉投[美職](../Page/美職.md "wikilink")[洛杉磯銀河](../Page/洛杉磯銀河.md "wikilink")。

## 生平

### 球會

古迪仙尼出生於盛產有名門將的[意大利](../Page/意大利.md "wikilink")。库迪奇尼的父親也是門將，效力[AC米蘭](../Page/AC米蘭.md "wikilink")，而库迪奇尼也克紹箕裘，效力[AC米蘭](../Page/AC米蘭.md "wikilink")（1991-95年），之後轉至[意甲上游份子的之一的](../Page/意甲.md "wikilink")[拉素](../Page/拉素足球會.md "wikilink")（1996-97年），可是並不得意，轉會[卡斯圖迪辛盧](../Page/Castel_di_Sangro_Calcio.md "wikilink")（1997-99年），後來車路士於1999年從[意大利丙組聯賽球會以](../Page/意大利丙組聯賽.md "wikilink")13萬[英鎊使該](../Page/英鎊.md "wikilink")[意大利門將從見天日](../Page/意大利.md "wikilink")，库迪奇尼本來擔任前車路士門將[迪高爾的後備](../Page/迪高爾.md "wikilink")。

到了[2000/01年的](../Page/2000年至2001年英格蘭超級聯賽.md "wikilink")[英超](../Page/英超.md "wikilink")，前「藍戰士」領隊[雲尼亞里更把他提拔為正選門將](../Page/雲尼亞里.md "wikilink")。自此之後，库迪奇尼出色的表現使他成為必然正選。他曾兩次獲得英超最佳門將，但表現實際上一直被評為並不穩定。故當[2004/05年球季](../Page/2004年至2005年英格蘭超級聯賽.md "wikilink")，車路士簽入年青鋼門[施治後](../Page/施治.md "wikilink")，捷克青年的神勇表現令库迪奇尼上陣之機會大減。如在[2005/06年球季中](../Page/2005年至2006年英格蘭超級聯賽.md "wikilink")，他主要於本土盃赛上阵。

2009年1月26日自由轉會加盟[熱刺](../Page/托特纳姆热刺足球俱乐部.md "wikilink")<small>\[1\]</small>。2009年11月12日古迪仙尼在其在[倫敦寓所附近駕駛](../Page/倫敦.md "wikilink")[摩托車發生嚴重意外](../Page/摩托車.md "wikilink")，據報其雙腕斷骨及撞傷[骨盆](../Page/髖.md "wikilink")，在東倫敦醫院留院觀察<small>\[2\]</small>。

2012年12月31日[美國職業足球大聯盟球會](../Page/美國職業足球大聯盟.md "wikilink")[洛杉磯銀河與](../Page/洛杉磯銀河.md "wikilink")[英超球會](../Page/英超.md "wikilink")[熱刺達成協議免轉會費簽入古迪仙尼](../Page/熱刺.md "wikilink")，但要收妥轉會証書、辦妥工作簽證及經體能測驗才能作實<small>\[3\]</small>。

### 國家隊

古迪仙尼曾代表義大利U16、U18及[U21參加國際賽](../Page/義大利21歲以下國家足球隊.md "wikilink")，惟僅於2002年代表[大國腳上陣](../Page/義大利國家足球隊.md "wikilink")1次對[土耳其](../Page/土耳其國家足球隊.md "wikilink")<small>\[4\]</small>。

## 榮譽

### 球隊

#### AC米蘭

  - [歐洲聯賽冠軍盃亞軍](../Page/歐洲聯賽冠軍盃.md "wikilink")：[1993年](../Page/1992/93賽季歐洲冠軍聯賽.md "wikilink")

#### 車路士

  - [英格蘭足總盃冠軍](../Page/英格蘭足總盃.md "wikilink")：2000年、2007年
  - [英格蘭社區盾冠軍](../Page/英格蘭社區盾.md "wikilink")：2000年、2005年
  - [歐洲聯賽冠軍盃亞軍](../Page/歐洲聯賽冠軍盃.md "wikilink")：[2008年](../Page/2008年歐洲聯賽冠軍盃決賽.md "wikilink")
  - [英格蘭超級足球聯賽亞軍](../Page/英格蘭超級足球聯賽.md "wikilink")：[2003-04年](../Page/2003年至2004年英格蘭超級聯賽.md "wikilink")
  - [英格蘭足總盃亞軍](../Page/英格蘭足總盃.md "wikilink")：2002年
  - [英格蘭社區盾亞軍](../Page/英格蘭社區盾.md "wikilink")：2007年
  - [英格蘭聯賽盃亞軍](../Page/英格蘭聯賽盃.md "wikilink")：2008年

### 個人

  - ITV金手套獎全年最佳門將：2003年
  - [車路士年度最佳球員](../Page/車路士.md "wikilink")：2002年

## 參考資料

## 外部連結

  -
  - [库迪奇尼](http://www.lagalaxy.com/players/carlo-cudicini)於洛杉磯銀河簡歷

  - [库迪奇尼官方網站](http://www.carlocudicini.co.uk/)

  - [雅虎體育：库迪奇尼](https://web.archive.org/web/20050915195402/http://uk.sports.yahoo.com/fo/profiles/8986.html)

[Category:意大利足球运动员](../Category/意大利足球运动员.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:科木球員](../Category/科木球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:车路士球员](../Category/车路士球员.md "wikilink")
[Category:熱刺球員](../Category/熱刺球員.md "wikilink")
[Category:洛杉磯銀河球員](../Category/洛杉磯銀河球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:美職球員](../Category/美職球員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:米蘭人](../Category/米蘭人.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")

1.   [Cudicini makes Tottenham
    switch](http://news.bbc.co.uk/sport2/hi/football/teams/t/tottenham_hotspur/7851011.stm)，2009年1月26日，[BBC](../Page/BBC.md "wikilink")
    Sport，於2009年1月27日查閱
2.
3.
4.