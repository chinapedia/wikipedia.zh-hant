**永和**（416年二月-417年八月）是[十六国时期](../Page/十六国.md "wikilink")[后秦末主](../Page/后秦.md "wikilink")[姚泓的](../Page/姚泓.md "wikilink")[年号](../Page/年号.md "wikilink")，共计2年。这也是后秦的最后一个年号。

## 纪年

| 永和                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 416年                           | 417年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[永和年号](../Page/永和.md "wikilink")
  - 同期存在的其他政权年号
      - [义熙](../Page/义熙_\(晋安帝\).md "wikilink")（405年-418年）：東晉皇帝[晋安帝司马德宗的年号](../Page/晋安帝.md "wikilink")
      - [玄始](../Page/玄始.md "wikilink")（412年十一月-428年）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [永康](../Page/永康_\(西秦\).md "wikilink")（412年八月-419年）：[西秦政权](../Page/西秦.md "wikilink")[乞伏炽磐年号](../Page/乞伏炽磐.md "wikilink")
      - [建初](../Page/建初.md "wikilink")（405年-417年二月）：[西凉政权](../Page/西凉.md "wikilink")[李暠年号](../Page/李暠.md "wikilink")
      - [嘉兴](../Page/嘉兴.md "wikilink")（417年二月-420年七月）：[西凉政权](../Page/西凉.md "wikilink")[李歆年号](../Page/李歆.md "wikilink")
      - [凤翔](../Page/凤翔.md "wikilink")（413年三月-418年十月）：[夏国政权](../Page/夏国.md "wikilink")[赫连勃勃年号](../Page/赫连勃勃.md "wikilink")
      - [太平](../Page/太平_\(北燕\).md "wikilink")（409年十月-430年）：[北燕政权](../Page/北燕.md "wikilink")[冯跋年号](../Page/冯跋.md "wikilink")
      - [神瑞](../Page/神瑞.md "wikilink")（414年-416年四月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋嗣年号](../Page/拓跋嗣.md "wikilink")
      - [泰常](../Page/泰常.md "wikilink")（416年四月-423年）：[北魏政权](../Page/北魏.md "wikilink")[拓跋嗣年号](../Page/拓跋嗣.md "wikilink")
      - [建平](../Page/建平.md "wikilink")（415年三月-416年九月）：[白亚栗斯](../Page/白亚栗斯.md "wikilink")、[刘虎年号](../Page/刘虎.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:后秦年号](../Category/后秦年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")