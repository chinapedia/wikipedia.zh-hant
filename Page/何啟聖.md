**何啟聖**(英文:Chi-Sheng
Ho)。[台灣](../Page/台灣.md "wikilink")[电视新闻节目主持人](../Page/电视新闻.md "wikilink")。前
[TVBS-NEWS](../Page/TVBS-NEWS.md "wikilink")《整點新聞》主播、[TVBS政论节目](../Page/TVBS_\(頻道\).md "wikilink")《[13招待所](../Page/13招待所.md "wikilink")》主持人。亦是一名资深的[军事记者](../Page/军事.md "wikilink")。

## 經歷

2009年擔任[1111人力銀行副總經理](../Page/1111人力銀行.md "wikilink")。
2012年獲得[馬來西亞國際發明獎大會特別獎金牌](../Page/馬來西亞國際發明獎.md "wikilink")，成為[發明家](../Page/發明家.md "wikilink")。籌拍電影《志氣》及《逗陣ㄟ》客串演出。

2013年5月，擔任[龍巖集團公共事務處總經理](../Page/龍巖集團.md "wikilink")。

2014年，接受[親民黨徵召參選第](../Page/親民黨.md "wikilink")12屆[台北市議員](../Page/台北市議員.md "wikilink")（大安、文山選區），2014年11月29日公布結果，以8664票落選。

## 學歷

  - 台北市私立[立人小學](../Page/臺北市私立立人國際國民中小學.md "wikilink")
  - 南投縣[中興新村光榮國民小學](../Page/中興新村.md "wikilink")
  - 台中市私立[衛道中學](../Page/衛道中學.md "wikilink")[初中部](../Page/初中.md "wikilink")
  - [中正國防幹部預備學校第](../Page/中正國防幹部預備學校.md "wikilink")4期畢業（71年班）
  - [政戰學校第](../Page/政戰學校.md "wikilink")32期新聞系畢業（75年班）
  - [國立臺灣大學政治研究所碩士](../Page/國立臺灣大學.md "wikilink")
  - [中華大學科技管理博士候選人](../Page/中華大學.md "wikilink")

## 主播（持）過的節目與活動

|                    |                                                      |                                              |         |
| ------------------ | ---------------------------------------------------- | -------------------------------------------- | ------- |
| **時間**             | **頻道**                                               | **節目**                                       | **備註**  |
| 時間待查               | [TVBS](../Page/TVBS.md "wikilink")                   | 《無線晚報》                                       | 周末、周日主播 |
|                    | |《大搜-{}-索線》                                          |                                              |         |
|                    | |《訐譙選舉》                                              |                                              |         |
|                    | |《2008咱選總統》                                          |                                              |         |
|                    | |《2008投給誰》                                           |                                              |         |
|                    | |《2008政黨辯論會》                                         |                                              |         |
|                    | |《2100周末開講》                                          |                                              |         |
|                    | |《第十七屆[醫療奉獻獎頒獎典禮](../Page/醫療奉獻獎.md "wikilink")》      |                                              |         |
|                    | |[世界安寧日台灣活動](../Page/世界安寧日.md "wikilink")            |                                              |         |
|                    | |[汶川大地震](../Page/汶川大地震.md "wikilink")「兩岸攜手、重建家園」募款晚會 |                                              |         |
| 時間待查—至今            | [TVBS-NEWS](../Page/TVBS-NEWS.md "wikilink")         | 《整點新聞》                                       |         |
| 2007.02.26～2008.12 | [TVBS](../Page/TVBS.md "wikilink")                   | 《[13招待所](../Page/13招待所.md "wikilink")》       |         |
| 2010.10.11\~ 待查    | [年代新聞台](../Page/年代新聞台.md "wikilink")                 | 《年代夜報》                                       |         |
| 2016.6.12          |                                                      | 發起[612凱道閱兵活動](../Page/612凱道閱兵.md "wikilink") |         |

## 資料來源

## 外部連結

  -
[Q启](../Category/何姓.md "wikilink")
[Category:臺中市私立衛道高級中學校友](../Category/臺中市私立衛道高級中學校友.md "wikilink")
[Category:政治作戰學院校友](../Category/政治作戰學院校友.md "wikilink")
[Category:國立臺灣大學社會科學院校友](../Category/國立臺灣大學社會科學院校友.md "wikilink")
[Category:台湾记者](../Category/台湾记者.md "wikilink")
[Category:台灣電視主播](../Category/台灣電視主播.md "wikilink")
[Category:台灣新聞節目主持人](../Category/台灣新聞節目主持人.md "wikilink")
[Category:TVBS主播](../Category/TVBS主播.md "wikilink")
[Category:親民黨黨員](../Category/親民黨黨員.md "wikilink")
[Category:酒醉駕駛](../Category/酒醉駕駛.md "wikilink")