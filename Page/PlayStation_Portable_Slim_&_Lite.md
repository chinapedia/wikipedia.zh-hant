**PlayStation Portable Slim &
Lite（PSP-2000）**，是[索尼電腦娛樂](../Page/索尼電腦娛樂.md "wikilink")（SCEI）於2007年7月11日宣布推出的[PlayStation
Portable升級版](../Page/PlayStation_Portable.md "wikilink")，2007年9月在全球上市。

## 主要改動及特性

2000型的主要優點包括：比之前的1000型（PSP-1000）輕33%、薄19%、屏幕亮33%；更快的遊戲載入；視頻信號輸出端子；記憶體容量從32[GB升至](../Page/GB.md "wikilink")64[GB](../Page/GB.md "wikilink")；更多不同的新顏色。但本機的電池容量為之前的型號的2/3（1200[mAh](../Page/mAh.md "wikilink")），使用時間較為短。當在瀏覽網頁的時候能夠明顯感覺到速度變快，並且在1000型常出現的「記憶體不足」也獲得改善，重量減少100g，在長時間玩的時候不會感覺到太大負擔（跟1000型差很多）。

## 主機顏色

PSP Slim & Lite 有以下主機顏色可供選擇，分別是：

  - 鋼琴黑（Piano Black）：日本於2007年9月20日發售。
  - 陶瓷白（Ceramic White）：日本於2007年9月20日發售。
  - 冰燦銀（Ice Silver）：日本於2007年9月20日發售。
  - 玫瑰桃（Rose Pink）：日本於2007年9月20日發售。
  - 雛菊藍（Felicia Blue）：日本於2007年9月20日發售。
  - 薰衣紫（Lavender Purple）：日本於2007年9月20日發售。
  - 深遠紅（Deep Red）：日本於2007年12月22日發售。
  - 薄荷綠（Mint Green）：日本於2008年2月28日發售。
  - 無光銅（Matte Bronze）：日本、台灣於2008年4月24日發售。
  - 金屬藍（Metallic Blue）：日本於2008年7月17日發售。

## 註釋

## 外部連結

  - [PlayStation.com (Asia)](http://asia.playstation.com/)
      - [官方網站（香港）](https://web.archive.org/web/20080218085548/http://asia.playstation.com/tch_hk/index.php)

[Category:索尼互動娛樂](../Category/索尼互動娛樂.md "wikilink")
[Category:數字音頻播放器](../Category/數字音頻播放器.md "wikilink")
[Category:Wi-Fi](../Category/Wi-Fi.md "wikilink") [Category:PlayStation
Portable](../Category/PlayStation_Portable.md "wikilink")