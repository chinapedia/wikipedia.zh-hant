**格奥尔格**（**Georg**，），全名**弗里德里希·奥古斯特·格奥尔格·路德维希·威廉·马克西米连·卡尔·玛利亚·奈波穆克·巴普蒂斯特·克萨威尔·基里亚库斯·罗曼努斯**（Friedrich
August GEORG Ludwig Wilhelm Maximilian Karl Maria Nepomuk Baptist Xaver
Cyriacus
Romanus），[萨克森第六任国王](../Page/萨克森.md "wikilink")（1902年－1904年）。前国王[阿尔贝特之弟](../Page/阿尔贝特_\(萨克森\).md "wikilink")，国王[约翰一世与夫人](../Page/约翰_\(萨克森\).md "wikilink")[巴伐利亚公主阿美利亚](../Page/巴伐利亚.md "wikilink")·奥古斯塔第三子。

## 婚姻与子女

格奥尔格1859年5月11日与[葡萄牙女王](../Page/葡萄牙.md "wikilink")[玛利亚二世之女玛利亚](../Page/玛利亚二世_\(葡萄牙\).md "wikilink")·安娜（*Maria
Anna*）结婚，有四子四女：

  - 长女玛利亚·约翰娜（Marie Johanna，1860年6月19日－1861年3月2日）
  - 次女伊莉莎白（Elisabeth，1862年2月14日－1863年5月18日）
  - 三女玛蒂尔德（Mathilde，1863年3月19日－1933年3月27日）
  - 长子[弗里德里希·奥古斯特三世](../Page/弗里德里希·奥古斯特三世_\(萨克森国王\).md "wikilink")（Friedrich
    August
    III，1865年5月25日－1932年2月18日），[萨克森末代国王](../Page/萨克森.md "wikilink")。
  - 四女玛利亚·约瑟法（Maria
    Josepha，1867年5月31日－1944年5月28日），1886年与[奥地利大公奥托结婚](../Page/奥地利.md "wikilink")
  - 次子约翰·格奥尔格（Johann
    Georg，1869年7月10日－1938年2月24日），1894年与[符腾堡女公爵](../Page/符腾堡.md "wikilink")[伊莎贝拉](../Page/伊莎贝拉女公爵_\(符腾堡\).md "wikilink")（Isabella）结婚，1906年与[两西西里公主玛利亚](../Page/两西西里.md "wikilink")·伊玛扣拉塔（Maria
    Immacolata）结婚
  - 三子马克西米连（Maximilian，1870年11月17日－1951年1月12日）
  - 四子阿尔贝特（Albert，1875年2月25日－1900年9月16日）

[Category:萨克森君主](../Category/萨克森君主.md "wikilink")
[Category:韦廷王朝](../Category/韦廷王朝.md "wikilink")