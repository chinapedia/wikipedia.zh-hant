**萨尼巴尔·奥拉霍瓦奇**（****，）是一位[黑山球员](../Page/黑山共和国.md "wikilink")，场上位置为中场。身高1米79

自2008年起为效力[韦恩](../Page/韦恩威斯巴登足球俱乐部.md "wikilink")。之前曾在[贝尔格莱德红星](../Page/贝尔格莱德红星足球俱乐部.md "wikilink")、[卡尔斯鲁厄队及](../Page/卡尔斯鲁厄体育俱乐部.md "wikilink")[-{zh-hans:艾尔格博格;zh-hk:奧厄}-踢球](../Page/艾尔格博格足球俱乐部.md "wikilink")。

[Category:黑山足球运动员](../Category/黑山足球运动员.md "wikilink")
[Category:卡爾斯魯厄球員](../Category/卡爾斯魯厄球員.md "wikilink")
[Category:奧厄球員](../Category/奧厄球員.md "wikilink")
[Category:韋恩球員](../Category/韋恩球員.md "wikilink")
[Category:貝爾格萊德紅星球員](../Category/貝爾格萊德紅星球員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:樸高利卡球員](../Category/樸高利卡球員.md "wikilink")
[Category:甘馬雷斯球員](../Category/甘馬雷斯球員.md "wikilink")
[Category:法蘭克福球員](../Category/法蘭克福球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")