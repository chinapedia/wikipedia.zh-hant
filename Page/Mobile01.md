[Mobile01_homepage_Portable.png](https://zh.wikipedia.org/wiki/File:Mobile01_homepage_Portable.png "fig:Mobile01_homepage_Portable.png")

****，簡稱**01**（**Zero-One**）、俗稱**膜拜01**、**摩比01**、**小惡魔**等。是專門討論各種[行動電話](../Page/行動電話.md "wikilink")、[行動裝置](../Page/行動裝置.md "wikilink")、[3C等產品與介紹](../Page/3C_\(商品代稱\).md "wikilink")[台灣各景點的網路論壇](../Page/台灣.md "wikilink")，由詠勝科技有限公司（位於台灣的[臺南和](../Page/臺南.md "wikilink")[臺北](../Page/臺北.md "wikilink")）維護。在[Alexa的統計中](../Page/Alexa.md "wikilink")，Mobile01是台灣關於3C產品資訊討論的高瀏覽率網站之一。

Mobile01的標誌為[英文的Mobile與一個](../Page/英文.md "wikilink")01所構成，其中01以一個小[惡魔的](../Page/惡魔.md "wikilink")[卡通圖案所化成](../Page/卡通.md "wikilink")。在Mobile01中，小惡魔為站內會員愛好3C產品的精神象徵。

## 發展歷史

Mobile01於2000年，由[蔣叡勝創立](../Page/蔣叡勝.md "wikilink")，並與義務提供服務的Peron和Orpheus二位合作經營站務。蔣某當時於[台灣大哥大擔任手機維修工作多年](../Page/台灣大哥大.md "wikilink")，認為網路社群的發展有利可圖，辭掉原工作後全力經營網站Mobile01，在當時的環境中極為少見。

2001年間，網站成立不到一年，會員已快速成長，取代當時已過氣的[Windows
CE網站並直接與PDAorDie競爭](../Page/Windows_CE.md "wikilink")。PDAorDie最後因諸多因素退出網站經營，從此留下[Mobilewow](../Page/Mobilewow.md "wikilink")、[MML](../Page/MML.md "wikilink")、[MyMobileLife及Mobile](../Page/MyMobileLife.md "wikilink")01這幾個網站為使用者服務。由於[網路泡沫後](../Page/網路泡沫.md "wikilink")，社群網站廣告收入極微，團購及買賣商品亦需要規模，自此開始找尋新的營收方向。首先加入團購活動，並開始規劃線上購物網站；這時期亦開始與多家企業合作。

2002年，開始嘗試開放其他版面，如[相機及](../Page/相機.md "wikilink")[汽車等](../Page/汽車.md "wikilink")。各版主數也快速的增加，經由版主免費的協助，不但創造了使用者支持，也大量的減少初期的人事成本。這策略雖然並非首創，但在當時的社群網站中卻是少數經營出色者。在後來的數次網聚中，單單是參加版聚的各版版主就有百員之多。

2006年，[網路購物的業務已有不錯的回收](../Page/網路購物.md "wikilink")，但站方在思考優勢和報酬後，決定淡化線上購物經營，轉全心經營[廣告業務](../Page/廣告.md "wikilink")，並[外包廣告業務](../Page/外包.md "wikilink")，以保持未來發展的中立性。

於2006年時，因將相關主機置於[樹德科技大學內](../Page/樹德科技大學.md "wikilink")，實質上為盜用[台灣學術網路資源的行為](../Page/台灣學術網路.md "wikilink")。由於大量的照片造成流量的負荷，被發現後才加以改善。

2007年7月2日，開放 Mobile01 Blog 服務，但限定 Mobile01 VIP 會員申請。

2011年7月25日，Mobile01 推出 iOS 以及 Android APP。

2013年，發生[三星寫手門事件](../Page/三星寫手門事件.md "wikilink")\[1\]，[三星旗下行銷公司](../Page/三星電子.md "wikilink")[鵬泰顧問公司雇用工讀生在Mobile](../Page/鵬泰顧問公司.md "wikilink")01抹黑及造假，[公平交易委員會介入調查](../Page/公平交易委員會.md "wikilink")，之後並以違反[公平交易法開罰三星電子以及鵬泰顧問](../Page/公平交易法.md "wikilink")。

2014年3月，[數位時代發表台灣網站](../Page/數位時代.md "wikilink")100強，Mobile01排名第8名。\[2\]

## 基礎功能及服務

  - 討論區

可在此發表新產品[開箱文](../Page/開箱文.md "wikilink")，或是由專業的編輯發表評測文，用戶也可在此發表產品實際使用的心得文章。

  - 違規處分

只要違反相關板規，文章將會被Mobile01系統的網站管理員移動到「蟲洞」之中。若是嚴重違規，帳號會被處以停權數天，甚至永久停權。

  - 市集

可在此區與其他論壇使用者以物易物或交易。

  - 網誌

須有VIP等級的帳號方可使用。

## 趣事

  - Mobile01的會員分享自己的敗家成果時，常常都會不成文規則的將物品疊高，拍出所謂的疊疊樂照片。
  - 會員在分享產品的使用心得時，常會說「早上/回家開門踢到一個箱子」，並會從產品尚未拆封的外箱照說起，被稱為「開箱照」。

## 參考文獻

## 外部連結

  - [Mobile01](http://www.mobile01.com/)

  - [Mobile01](http://www.5i01.cn/)

  - [Mobile01行動版](http://5i01.com/)

  - [Mobile01
    香港](https://web.archive.org/web/20130927150045/http://www.mobile01.hk/)

  -
[Category:台灣網路論壇](../Category/台灣網路論壇.md "wikilink")

1.
2.  [2014年台灣網站100強揭曉！雅虎奇摩以些微差距，擊敗Facebook奪冠](https://www.bnext.com.tw/article/31260/BN-ARTICLE-31260)