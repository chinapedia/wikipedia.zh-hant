**杜伊斯堡迈德里希竞赛俱乐部**（[德語](../Page/德語.md "wikilink")****）簡稱：**杜伊斯堡**（****）是位於[德國](../Page/德國.md "wikilink")[北莱茵-威斯特法伦魯爾河谷](../Page/北莱茵-威斯特法伦.md "wikilink")（）內[杜伊斯堡的](../Page/杜伊斯堡.md "wikilink")-{zh-hans:足球俱乐部;
zh-hk:足球會;}-，於[2006－07年獲得](../Page/2006年至2007年德國足球乙級聯賽.md "wikilink")[德乙季軍](../Page/德乙.md "wikilink")，再次升級到[德甲比賽](../Page/德甲.md "wikilink")。[翌年在德甲敬陪末席](../Page/2007年至2008年德國足球甲級聯賽.md "wikilink")，再度降回[德乙打拚](../Page/德乙.md "wikilink")。

## 球會歷史

杜伊斯堡成立於1902年，初時名為**迈德里希**（**）作為地區代表的球隊，三年後兼併同區的迈德里希維多利亞（**）。直到1967年才探用現今的名稱，證明杜伊斯堡是當地最受歡迎及成功的球隊。

杜伊斯堡的早年歷史曾多次贏得地區錦標賽的冠軍，其中一季更保持不敗，射入113球而僅失掉12球。在三十年代早期球隊發生混亂，直到五十年代才告復原，可以派遣強陣作賽。1963年[德甲成立時](../Page/德甲.md "wikilink")，杜伊斯堡是十六隊創始球隊之一。德甲的首年亦是杜伊斯堡成績最好的一年，僅排於[科隆之後成為亞軍](../Page/科隆足球俱乐部.md "wikilink")。「斑馬兵團」一直維持在頂級聯賽長達二十年，於1982－83年球季才告降級到[德乙](../Page/德乙.md "wikilink")，自始在甲乙組之間浮沉，成為德國足球的升降機球隊。雖則如此，在往後的廿五年間，杜伊斯堡仍有八季留在德甲比賽。

## 球會榮譽

杜伊斯堡所獲得的榮譽屈指可數，包括1963年首屆德甲聯賽亞軍、1978－79年[歐洲足協盃晉級四強](../Page/歐洲足協盃.md "wikilink")（被同國球隊兼最終冠軍[-{zh-hans:门兴格拉德巴赫;zh-hk:慕遜加柏;zh-tw:門興格拉德巴赫;}-淘汰](../Page/门兴格拉德巴赫足球俱乐部.md "wikilink")）及三次[德國盃決賽的負方](../Page/德國盃.md "wikilink")（1966年、1975年及1998年）。1987－88年球季降到第三級的北萊茵業餘上級聯賽（）終於奪得德國業餘聯冠軍（）。杜伊斯堡的青年隊曾奪得多次全國錦標賽冠軍。

杜伊斯堡保有一項德甲紀錄：作客勝球最高，於1966年作客[柏林以](../Page/柏林.md "wikilink")9-0大破柏林塔斯馬尼亞1900（）。

## 主場球場

**MSV竞技场**（****）建於2004年，可以容納31,500名觀眾，是杜伊斯堡的主場球場。球場於2005年曾經主辦[世界運動會](../Page/世界運動會.md "wikilink")。

<File:Haupttribüne> MSV-Arena.jpg|球場外觀 <File:MSV-Arena> Duisburg
01.jpg|球場內觀 <File:MSV-Arena> Duisburg 02.jpg|球場看台 <File:Schema>
MSV-Arena.svg|球場平面圖

## 著名球星

[Bernard_Dietz_1985.jpg](https://zh.wikipedia.org/wiki/File:Bernard_Dietz_1985.jpg "fig:Bernard_Dietz_1985.jpg")

  - [Michael Bella](../Page/Michael_Bella.md "wikilink")
  - [伯纳德·迪茨](../Page/伯纳德·迪茨.md "wikilink")（Bernard Dietz）
  - [Kurt Jara](../Page/Kurt_Jara.md "wikilink")
  - [Detlef Pirsig](../Page/Detlef_Pirsig.md "wikilink")
  - [赫尔穆特·拉恩](../Page/赫尔穆特·拉恩.md "wikilink")（Helmut
    Rahn）：為[西德在](../Page/西德國家足球隊.md "wikilink")1954年[瑞士世界盃決賽射入致勝球](../Page/1954年世界盃足球賽.md "wikilink")，這場比賽被稱為[伯爾尼奇蹟](../Page/伯尔尼的奇迹.md "wikilink")，在杜伊斯堡前身「*Meidericher
    SV*」（1963－65年）結束足球競賽生涯。
  - [Rudolf Seliger](../Page/Rudolf_Seliger.md "wikilink")
  - [Ronald Worm](../Page/Ronald_Worm.md "wikilink")
  - [Klaus Wunder](../Page/Klaus_Wunder.md "wikilink")
  - [安貞桓](../Page/安貞桓.md "wikilink")（Ahn Jung-Hwan）
  - [杜夫亭](../Page/斯蒂格·托夫丁.md "wikilink")（Stig Tøfting）

## 外部連結

  - [Official team site](http://www.msv-duisburg.de)
  - [The Abseits Guide to German
    Soccer](http://www.abseits-soccer.com/clubs/msv.html)
  - [Duisburg
    statistics](http://www.resultsfromfootball.com/bundesliga-team/msvduisburg.html)

[Category:德國足球俱樂部](../Category/德國足球俱樂部.md "wikilink")
[Category:德甲聯賽始創成員](../Category/德甲聯賽始創成員.md "wikilink")
[Category:1902年建立的足球俱樂部](../Category/1902年建立的足球俱樂部.md "wikilink")