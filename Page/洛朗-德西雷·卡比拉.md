**洛朗－德西列·卡比拉**（，），[剛果民主共和國第三任](../Page/剛果民主共和國.md "wikilink")[总统](../Page/总统.md "wikilink")。

1997年5月17日，领导解放刚果民主力量同盟的武装部队攻占首都[金沙萨](../Page/金沙萨.md "wikilink")，宣布就任总统，並且將國名恢復為**刚果民主共和国**。

2001年1月16日卡比拉遭下属军官拉什迪·米泽勒（Rashidi
Mizele）刺杀，两天后被证实在[津巴布韦不治身亡](../Page/津巴布韦.md "wikilink")。这项刺杀行动被认为是一场未遂[政变的一部分](../Page/政变.md "wikilink")。一星期后，其子[约瑟夫·卡比拉返回刚果](../Page/约瑟夫·卡比拉.md "wikilink")，继任总统。

[分類:遇刺身亡的總統](../Page/分類:遇刺身亡的總統.md "wikilink")

[Category:剛果民主共和國遇刺身亡者](../Category/剛果民主共和國遇刺身亡者.md "wikilink")
[Category:刚果民主共和国总统](../Category/刚果民主共和国总统.md "wikilink")
[Category:靠政變上台的領導人](../Category/靠政變上台的領導人.md "wikilink")