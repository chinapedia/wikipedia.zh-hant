[Gliman_Street_Dead-end_part_2015.jpg](https://zh.wikipedia.org/wiki/File:Gliman_Street_Dead-end_part_2015.jpg "fig:Gliman_Street_Dead-end_part_2015.jpg")
[Gilman_Street_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Gilman_Street_\(Hong_Kong\).jpg "fig:Gilman_Street_(Hong_Kong).jpg")的一段\]\]
**機利文街**（**Gilman
Street**），又曾經稱為**花布街**，是[香港島](../Page/香港島.md "wikilink")[中西區的一條](../Page/中西區_\(香港\).md "wikilink")[街道](../Page/街道.md "wikilink")，位於[中環商業中心區](../Page/中環.md "wikilink")，附近甲級[寫字樓林立](../Page/寫字樓.md "wikilink")，由前身為[國際大廈的](../Page/國際大廈.md "wikilink")[中保集團大廈高層北望](../Page/中保集團大廈.md "wikilink")，見[干諾道中](../Page/干諾道中.md "wikilink")、[四季酒店](../Page/四季酒店.md "wikilink")、[海港政府大樓](../Page/海港政府大樓.md "wikilink")、[港鐵](../Page/港鐵.md "wikilink")[中環站](../Page/中環站.md "wikilink")、[中環碼頭及](../Page/中環碼頭.md "wikilink")[維多利亞港等](../Page/維多利亞港.md "wikilink")。由機利文街中段的[南洋商業銀行大廈向南望](../Page/南洋商業銀行.md "wikilink")，見[中環中心](../Page/中環中心.md "wikilink")、[德輔道中及](../Page/德輔道中.md "wikilink")[機利文新街等](../Page/機利文新街.md "wikilink")。機利文街附近由於物業發展（今[中環中心](../Page/中環中心.md "wikilink")），[交通上](../Page/交通.md "wikilink")，現在南段成為一條[死路的後巷](../Page/死路.md "wikilink")\[1\]。一同遭截短的街道還有[永安街](../Page/永安街.md "wikilink")、[同文街和](../Page/同文街.md "wikilink")[興隆街](../Page/興隆街_\(香港\).md "wikilink")。

## 歷史

機利文街的中文是英文譯音，舊稱**機利民街**。Gilman & Bowman
是[香港開埠時代的](../Page/香港開埠.md "wikilink")[洋行之一](../Page/洋行.md "wikilink")，與Gibb
Livingston（現已經給[英之傑合併](../Page/英之傑.md "wikilink")），Jardine
Matheson（[怡和洋行之前身](../Page/怡和洋行.md "wikilink")），及 Butterfield
Swire（[太古集團之前身](../Page/太古集團.md "wikilink")），在當年歷史的[香港](../Page/香港.md "wikilink")，合稱「英資四大洋行」。機利文街可以見證Gilman
&
Bowman在從前曾經存在，此洋行後來成為英之傑的成員，再之後又成為[利豐集團的成員](../Page/利豐.md "wikilink")\[2\]。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [「築電之旅」尋覓消失中的香港](http://www.hkia.net/view_section.php?id=3545&level_id=306)

[Category:中環街道](../Category/中環街道.md "wikilink")

1.  [機利文街地圖](http://www.centamap.com/gc/centamaplocation.aspx?x=834020&y=816303&sx=834020.43138&sy=816303.93859&z=2)
2.  [利豐收購英之傑](http://www.lifunggroup.com/L&FGTraditional/heritage/heritage05.htm)