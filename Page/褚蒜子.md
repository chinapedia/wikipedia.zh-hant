**褚蒜子**，[河南](../Page/河南郡.md "wikilink")[陽翟人](../Page/陽翟.md "wikilink")，父[褚裒](../Page/褚裒.md "wikilink")，母谢真石（[谢鲲的女儿](../Page/谢鲲.md "wikilink")，褚裒继室）。前母荀氏、卞氏。為[東晉](../Page/東晉.md "wikilink")[晉康帝](../Page/晉康帝.md "wikilink")[皇后](../Page/皇后.md "wikilink")。

## 生平

褚蒜子出身望族，成年以後，[晉成帝聞其美麗及才識](../Page/晉成帝.md "wikilink")，將之許配給弟弟琅琊王司馬岳，褚蒜子成為琅琊[王妃](../Page/王妃.md "wikilink")。不久，晉成帝病崩，傳帝位予司馬岳，是為晉康帝。司馬岳登基後冊褚妃為皇后、褚皇后生母謝氏為尋陽[鄉君](../Page/鄉君.md "wikilink")。褚皇后和晉康帝之間育有一子司馬聃。

晉康帝登基未滿2年即病崩，僅與皇后褚蒜子之間遺下一子、1歲的司馬聃，因而由司馬聃繼位，是為[晉穆帝](../Page/晉穆帝.md "wikilink")，褚皇后也升為[太后](../Page/太后.md "wikilink")。由於穆帝年幼，因此由朝臣奏請褚太后[臨朝稱制](../Page/臨朝稱制.md "wikilink")，並由大臣[何充](../Page/何充.md "wikilink")、[蔡謨](../Page/蔡謨.md "wikilink")、[司馬昱等相繼輔政](../Page/司馬昱.md "wikilink")。

褚太后臨朝稱制後，有司奏曰，褚太后亲母謝氏既獲[鄉君封號](../Page/鄉君.md "wikilink")，前母荀氏、卞氏亦應[追贈](../Page/追贈.md "wikilink")，但為褚太后婉拒。晉穆帝成年行[冠禮後](../Page/冠禮.md "wikilink")，褚太后歸政，隱居崇德宮，不再過問政事。

晉穆帝親政不久後駕崩，無子，於是由堂兄司馬丕繼位，是為[晉哀帝](../Page/晉哀帝.md "wikilink")。由於穆、哀兩帝之間為平輩繼位，因此褚蒜子仍為太后。晉哀帝即位不久，迷上[道士傳授的](../Page/道士.md "wikilink")[長生法](../Page/長生法.md "wikilink")，曾嘗試[斷榖](../Page/斷榖.md "wikilink")、服[丹藥](../Page/丹藥.md "wikilink")，結果晉哀帝服藥後藥性大發，無法上朝，大臣們不得已，請褚太后二度臨朝。

不久，哀帝因服藥過度中毒，於太極殿駕崩，其弟司馬奕即位，是為[晉廢帝](../Page/晉廢帝.md "wikilink")。司馬奕即位後，朝政為權臣[桓溫所把持](../Page/桓溫.md "wikilink")，而在晉廢帝在位期間，褚太后也曾再度臨朝。不久，桓溫廢去司馬奕，改立司馬昱為帝，是為[晉簡文帝](../Page/晉簡文帝.md "wikilink")。同時尊褚太后為崇德太后。

晉簡文帝在位時間短暫，即位約8個月後駕崩，其子司馬曜繼位，是為[晉孝武帝](../Page/晉孝武帝.md "wikilink")。孝武帝即位初期，褚太后再度臨朝，3年後，褚太后歸政於14歲的孝武帝，重回崇德宮隱居。同年改元[太元](../Page/太元_\(东晋\).md "wikilink")。

太元九年六月癸丑（384年7月5日），崇德太后褚蒜子於顯陽殿崩逝\[1\]，享年六十一，[諡號](../Page/諡號.md "wikilink")**康獻皇后**。自穆帝開始，往後歷經哀、廢、簡文、孝武等五帝，在五位皇帝即位初期，褚太后都曾應朝臣請求臨朝稱制，總共約四十年。

褚太后逝世後，朝臣議論孝武帝應以何身分服喪。褚蒜子為晉康帝-{后}-，穆帝母，哀、廢兩帝叔母，簡文帝姪媳、孝武帝堂嫂，東晉皇位歷經多代傳承，雖往上追溯仍為元帝一脈，但自穆帝以後，諸帝之間關係錯綜，造成大臣在褚太后逝世後議論孝武帝的服喪身分，最後群臣議定，孝武帝以[齊衰服喪](../Page/五服#中國、朝鮮、越南的五服.md "wikilink")。

## 参考资料

  - 《[晋书](../Page/晋书.md "wikilink")·列传第二》
  - [河南褚氏世系圖](../Page/河南褚氏世系圖.md "wikilink")

[Category:東晉皇后](../Category/東晉皇后.md "wikilink")
[Category:東晉皇太后](../Category/東晉皇太后.md "wikilink")
[Category:东晋女性政治人物](../Category/东晋女性政治人物.md "wikilink")
[Category:中國女性攝政者](../Category/中國女性攝政者.md "wikilink")
[Category:中國皇嫂](../Category/中國皇嫂.md "wikilink")
[S](../Category/河南褚氏.md "wikilink")

1.  《晉書·孝武帝紀》