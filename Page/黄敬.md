**黄敬**（），原名**俞启威**，又名**俞大卫**，[俞明震之孙](../Page/俞明震.md "wikilink")，[浙江](../Page/浙江.md "wikilink")[绍兴人](../Page/绍兴.md "wikilink")。[中国共产党高层领导人之一](../Page/中国共产党.md "wikilink")，是[中华人民共和国首任](../Page/中华人民共和国.md "wikilink")[天津市市長](../Page/天津市市长列表.md "wikilink")。

黄敬早年投身学生运动，加入[中国共产党](../Page/中国共产党.md "wikilink")。1935年，考入[北京大学数学系](../Page/北京大学.md "wikilink")，与[姚依林等领导了](../Page/姚依林.md "wikilink")[一二·九运动](../Page/一二·九运动.md "wikilink")。[抗日战争期间](../Page/抗日战争.md "wikilink")，进入中共根据地，担任中共冀中区委书记、[中共中央平原分局书记](../Page/中共中央冀鲁豫分局.md "wikilink")。[第二次国共内战期间](../Page/第二次国共内战.md "wikilink")，担任[晋察冀边区财经办事处主任](../Page/晋察冀边区财经办事处.md "wikilink")、[华北人民政府公营企业部部长等](../Page/华北人民政府.md "wikilink")。中华人民共和国成立后，先后担任中共天津市委书记兼天津市市长、[中华人民共和国第一机械工业部部长](../Page/中华人民共和国第一机械工业部.md "wikilink")、[国家技术委员会主任等职](../Page/国家技术委员会.md "wikilink")。

其子[俞正声同样是政治人物](../Page/俞正声.md "wikilink")，曾官位高至第十八届[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")、第十二届中共[政协主席](../Page/政协主席.md "wikilink")、[上海市委书记等](../Page/上海市委书记.md "wikilink")。

## 生平

### 早年经历

[Huang_Jing_12-9_Movement.jpg](https://zh.wikipedia.org/wiki/File:Huang_Jing_12-9_Movement.jpg "fig:Huang_Jing_12-9_Movement.jpg")中，黄敬在北平天桥地区电车上演讲\]\]
黄敬是[俞大纯第三个儿子](../Page/俞大纯.md "wikilink")，生于北京，幼年随母住在南京家中。1924年开始，就读于[天津南开中学](../Page/天津市南开中学.md "wikilink")、[汇文中学](../Page/天津汇文中学.md "wikilink")。1930年在[上海参加](../Page/上海市.md "wikilink")[南国社左翼演剧运动](../Page/南国社.md "wikilink")。1931年至1933年，就读于[国立山东大学](../Page/山东大学.md "wikilink")（今[山东大学与](../Page/山东大学.md "wikilink")[中国海洋大学共同的前身](../Page/中国海洋大学.md "wikilink")）物理系。1931年[九一八事变后](../Page/九一八事变.md "wikilink")，领导国立山东大学的学生[罢课](../Page/罢课.md "wikilink")，抢占火车，去南京向[国民政府请愿](../Page/国民政府.md "wikilink")。并组建“海鸥剧社”，演出“进步话剧”。1932年加入[中国共产党](../Page/中国共产党.md "wikilink")，任国立山东大学地下党支部书记\[1\]。

1933年，黄敬任[中共青岛市委宣传部部长](../Page/中国共产党青岛市委员会.md "wikilink")。同年夏被出卖，逮捕入狱，此后被营救出狱，到[上海治病](../Page/上海.md "wikilink")。1935年到[北平](../Page/北平市.md "wikilink")，考入[北京大学数学系](../Page/北京大学.md "wikilink")，与[姚依林等领导了](../Page/姚依林.md "wikilink")[一二·九运动](../Page/一二·九运动.md "wikilink")\[2\]。1936年初，参与组建[中华民族解放先锋队](../Page/中华民族解放先锋队.md "wikilink")。曾任北平学联党团成员，同年4月任[中共北平市委宣传部部长](../Page/中国共产党北京市委员会.md "wikilink")、学委书记，到上海参与筹建全国学生救国联合会和全国各界救国联合会。1937年2月，任中共北平市委书记。5月前往延安，参加中共全国代表会议和白区工作会议\[3\]。

### 抗日战争与第二次国共内战期间

[抗日战争爆发后](../Page/中国抗日战争.md "wikilink")，黄敬离开北平到天津、[济南](../Page/济南市.md "wikilink")、[太原等地](../Page/太原市.md "wikilink")。后任中共[晋察冀区委员会书记](../Page/晋察冀边区.md "wikilink")。1938年春，任冀中区党委书记，同[吕正操](../Page/吕正操.md "wikilink")、[程子华等率部队](../Page/程子华.md "wikilink")、民众展开敌后战争，并组织反[掃蕩战役](../Page/掃蕩.md "wikilink")\[4\]。1942年秋，调任中共冀鲁豫区党委书记，后任[中共中央平原分局书记](../Page/中共中央冀鲁豫分局.md "wikilink")、冀鲁豫军区政委\[5\]。1944年冬，赴延安治疗休养，参加了[中共七大](../Page/中国共产党第七次全国代表大会.md "wikilink")。

[第二次国共内战期间](../Page/第二次国共内战.md "wikilink")，黄敬担任[晋察冀边区财经办事处主任](../Page/晋察冀边区财经办事处.md "wikilink")，[晋察冀中央分局副书记兼](../Page/晋察冀中央分局.md "wikilink")[晋察冀军区副政委](../Page/晋察冀边区.md "wikilink")。1948年夏任[中共中央华北局委员](../Page/中共中央华北局.md "wikilink")、[华北军区后勤司令部政委](../Page/华北野战军.md "wikilink")，[华北人民政府公营企业部部长](../Page/华北人民政府.md "wikilink")，并负责[中国人民解放军华北军区部队的后勤保障工作](../Page/华北野战军.md "wikilink")\[6\]。

### 中华人民共和国成立后

1949年1月15日，[天津被中国人民解放军佔領后](../Page/天津市.md "wikilink")，他被任命为天津市军事管制委员会副主任、[天津市市长](../Page/天津市市长列表.md "wikilink")，兼任[中共天津市委书记](../Page/中国共产党天津市委员会.md "wikilink")\[7\]。解放军佔領后的第一天，天津重新接通了电话，第二天全市通了水、电，第三天电车恢復通车。黄敬在任期间，天津生产的恢复和发展非常迅速，到1949年底，公营工业的生产就已经超过了[国民政府统治时期和日本占领时期的最高水平](../Page/國民政府.md "wikilink")；私营企业发展速度虽较公营工业为慢，但也恢复到国民政府统治时期的水平\[8\]。

1952年，他又被调任[中央人民政府政务院任](../Page/中央人民政府政务院.md "wikilink")[第一机械工业部部长](../Page/中央人民政府第一机械工业部.md "wikilink")，为发展中华人民共和国的机械工业、科学技术和培养科技人材做出了贡献\[9\]。1956年9月，在[中共八大上被选为中央委员](../Page/中国共产党第八次全国代表大会.md "wikilink")。1957年任[国务院科学规划委员会副主任](../Page/国务院科学规划委员会.md "wikilink")，[国家技术委员会主任兼第一机械工业部部长](../Page/国家技术委员会.md "wikilink")。1958年1月11日至22日，中共中央召开[南宁会议](../Page/南宁会议_\(1958年\).md "wikilink")，黄敬参加会议之后，坐飞机从南宁飞往[广州](../Page/广州市.md "wikilink")。在飞机上，黄敬突然旧病复发。飞机抵达广州后，[陶铸派人急送黄敬到](../Page/陶铸.md "wikilink")[广州军区总医院治疗](../Page/中国人民解放军广州总医院.md "wikilink")。1958年2月10日，黄敬病逝于广州；一说，黄敬死于精神疾病\[10\]。

## 家庭与个人生活

黄敬出自著名世家山阴俞氏，不但名人眾多，還跟許多知名的家族結為姻親。父親[俞大純曾任交通部隴海鐵路局局長](../Page/俞大純.md "wikilink")。堂叔[俞大維](../Page/俞大維.md "wikilink")，曾任[中華民國交通部部長](../Page/中華民國交通部.md "wikilink")、[中華民國國防部部長](../Page/中華民國國防部.md "wikilink")。堂弟[俞揚和](../Page/俞揚和.md "wikilink")，娶[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣經國的女兒](../Page/蔣經國.md "wikilink")[蔣孝章為妻](../Page/蔣孝章.md "wikilink")。

1932年，在山东大学求学期间，20岁的俞启威与在国立山东大学图书馆工作的18岁的[李云鹤](../Page/李云鹤.md "wikilink")（[江青](../Page/江青.md "wikilink")）同居，并介绍她加入中国共产党\[11\]。1933年黄敬被捕，[江青去上海加入演艺界](../Page/江青.md "wikilink")，先后改嫁[唐纳](../Page/唐纳.md "wikilink")、[章泯](../Page/章泯.md "wikilink")、[毛澤東](../Page/毛澤東.md "wikilink")。1939年，黄敬在冀中根据地与[范瑾结婚](../Page/范瑾.md "wikilink")。范瑾原名许勉文，叔祖父为[许寿裳](../Page/许寿裳.md "wikilink")，舅父为[范文澜](../Page/范文澜.md "wikilink")\[12\]。黄敬与范瑾育有三子二女：

  - [俞强声](../Page/俞强声.md "wikilink")，1940年生，原北京市公安局官员，[改革开放后](../Page/改革开放.md "wikilink")，任北京市国家安全局处长，[中华人民共和国国家安全部北美情报司司长](../Page/中华人民共和国国家安全部.md "wikilink")、外事局主任。1986年，投奔美国。
  - [俞敏声](../Page/俞敏声.md "wikilink")，1944年1月生，中华人民共和国经济学家、官员，原[全国人大常委会法制委员会委员](../Page/全国人大常委会法制委员会.md "wikilink")、中共人大法工委研究室主任。
  - [俞正声](../Page/俞正声.md "wikilink")，1945年4月生，第十八届[中共中央政治局常委](../Page/中共中央政治局常委.md "wikilink")、第十二届中共[政协主席](../Page/政协主席.md "wikilink")\[13\]。
  - [俞慧声](../Page/俞慧声.md "wikilink")，文革中自杀\[14\]。
  - [俞慈声](../Page/俞慈声.md "wikilink")，原东风无线电一厂工人，曾任北京市经济和信息化委员会党组成员、副主任。

## 参考文献

[Category:中华人民共和国天津市市长](../Category/中华人民共和国天津市市长.md "wikilink")
[Category:中共天津市委书记](../Category/中共天津市委书记.md "wikilink")
[Category:国务院已撤销各部部长](../Category/国务院已撤销各部部长.md "wikilink")
[Category:中国共产党第八届中央委员会委员](../Category/中国共产党第八届中央委员会委员.md "wikilink")
[Category:中国共产党党员
(1932年入党)](../Category/中国共产党党员_\(1932年入党\).md "wikilink")
[Y](../Category/北京大学校友.md "wikilink")
[Category:俞文葆家族](../Category/俞文葆家族.md "wikilink")
[Category:国立山东大学校友](../Category/国立山东大学校友.md "wikilink")
[Y](../Category/绍兴人.md "wikilink") [Q](../Category/俞姓.md "wikilink")

1.

2.

3.
4.
5.

6.
7.

8.

9.
10.

11.

12.

13.

14.