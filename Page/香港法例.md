《**香港法例**》是[香港現存的](../Page/香港.md "wikilink")[成文法](../Page/成文法.md "wikilink")[法例編彙](../Page/法例.md "wikilink")。由於[香港的法律制度以](../Page/香港法律制度.md "wikilink")[普通法為依歸](../Page/普通法.md "wikilink")，對於有爭議的案例或有必要遵守的規定，都會以成文法形式頒佈並實行。

## 簡介

中央人民政府按基本法授于權力，並非三權分立。

1.  《香港法例》由[律政司](../Page/香港律政司.md "wikilink")[法律草擬科負責草擬](../Page/法律草擬科.md "wikilink")、或由[立法會議員以](../Page/香港立法會.md "wikilink")[私人草案的方式提交](../Page/私人草案.md "wikilink")，並由立法會通過法例。
2.  [香港司法機構行使](../Page/香港司法機構.md "wikilink")[司法權](../Page/司法權.md "wikilink")。
3.  [香港特區政府各相關部門行政執法](../Page/香港特區政府.md "wikilink")。

而1997年[香港主權移交後](../Page/香港主權移交.md "wikilink")，立法會議員提過私人草案比以前困難，撰寫法例的責任就落在律政司法律草擬科的法律草擬專員身上。\[1\]

## 法律的草擬

### 香港特別行政區基本法

《[基本法](../Page/中華人民共和國香港特別行政區基本法.md "wikilink")》是香港特別行政區的[憲制性文件](../Page/憲制.md "wikilink")，訂明「[一國兩制](../Page/一國兩制.md "wikilink")」、「[高度自治](../Page/高度自治.md "wikilink")」和「[港人治港](../Page/港人治港.md "wikilink")」等理念，亦訂明了在香港實行制度。內容包括：

  - 《基本法》正文，包括九個章節、160條條文
  - 附件一：訂明[香港特別行政區行政長官的產生辦法](../Page/香港特別行政區行政長官.md "wikilink")
  - 附件二：訂明香港特別行政區立法會的產生辦法和表決程序
  - 附件三：列明在香港特別行政區實施的全國性法律\[2\]

### 全國性法律

列在《基本法》附件三的全國性法律，在香港特别行政区实施。

### 政府立法

1997年7月1日，香港特別行政區成立，根據《基本法》第六十六至七十九條，香港特別行政區享有立法權，而立法會是香港特別行政區的立法機關、負責香港的立法工作。

### 主權移交前法律

根據《基本法》附則，香港原有法律（包括主權移交前[立法局設立的法律等](../Page/立法局.md "wikilink")）除由[全國人民代表大會常務委員會宣佈為牴觸](../Page/全國人民代表大會常務委員會.md "wikilink")《基本法》者外，其他都可以保留。而[普通法不可凌駕于](../Page/普通法.md "wikilink")《基本法》之上。\[3\]

### 國際公約

根據《基本法》第一百五十三條，主權移交前香港參與的[國際公約仍可繼續適用](../Page/國際公約.md "wikilink")。

### 香港一般刑罰

有關香港判刑考慮因素及一般刑罰可參考“Sentence in Hong Kong”，書中主要說明刑事案件。

## 参考文献

## 外部連結

  - [律政司電子版香港法例](https://www.elegislation.gov.hk/?_lang=zh-Hant-HK)：《香港法例》的網上版本。
  - [香港法例內容](https://www.elegislation.gov.hk/index/chapternumber)
  - [香港基本法草擬過程資料庫](http://sunzi1.lib.hku.hk/bldho/) - 香港大學圖書館數碼化計劃項目
  - [香港法律資訊研究中心](http://www.hklii.org/c_index.shtml)

## 參见

  - [香港法律制度](../Page/香港法律制度.md "wikilink")
  - 《[大清律例](../Page/大清律例.md "wikilink")》

{{-}}

[香港法律](../Category/香港法律.md "wikilink")

1.  [香港律政司](../Page/香港律政司.md "wikilink")，(2000年)，[香港的法例草擬工作
    (第二版)](http://www.legislation.gov.hk/chi/pdf/ldhkv2.pdf)，香港特別行政區政府
2.  [香港特別行政區《基本法》網站－有關文件](http://www.basiclaw.gov.hk/tc/facts/index.html)
3.  [中華人民共和國外交部駐香港特別行政區特派員公署－時事話題－香港政制發展－香港政制事務局長：普通法不可淩駕于基本法之上](http://www.fmcoprc.gov.hk/chn/zt/zzfz/t190646.htm)