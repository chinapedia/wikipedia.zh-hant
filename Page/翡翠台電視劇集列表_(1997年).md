本列表列出1997年由[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台所播放的劇集](../Page/翡翠台.md "wikilink")。

## 星期一至五（非黃金時段）

### 早上劇集時段

[香港時間逢星期一至五](../Page/香港時間.md "wikilink")10:40-11:40播出（**逢公眾假期暫停播映**）

<table style="width:344%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 18%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>監製</p></th>
<th style="text-align: center;"><p>網頁</p></th>
<th style="text-align: center;"><p>歌曲</p></th>
<th style="text-align: center;"><p>備-{注}-</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>02/01-<br />
29/01</p></td>
<td style="text-align: center;"><p><a href="../Page/絕代雙驕_(1979年電視劇).md" title="wikilink">絕代雙驕</a><br />
The twin</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><div style="text-align: center;">
<p><span style="font-size:larger;"><strong>重播劇集</strong>：<a href="../Page/翡翠台電視劇集列表_(1979年).md" title="wikilink">1979年無綫劇集</a></span></p>
</div></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>02</p></td>
<td style="text-align: center;"><p><a href="../Page/過客.md" title="wikilink">過客</a></p></td>
<td style="text-align: center;"><p>25</p></td>
<td style="text-align: center;"><div style="text-align: center;">
<p><span style="font-size:larger;"><strong>重播劇集</strong>：<a href="../Page/翡翠台電視劇集列表_(1981年).md" title="wikilink">1981年無綫劇集</a></span></p>
</div></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>03</p></td>
<td style="text-align: center;"><p><a href="../Page/女黑俠木蘭花.md" title="wikilink">女黑俠木蘭花</a></p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><div style="text-align: center;">
<p><span style="font-size:larger;"><strong>重播劇集</strong>：<a href="../Page/翡翠台電視劇集列表_(1981年).md" title="wikilink">1981年無綫劇集</a></span></p>
</div></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 星期一至五晚

### 第一線劇集

[香港時間逢星期一至五](../Page/香港時間.md "wikilink")19:30-20:30播出

<table style="width:344%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 18%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>監製</p></th>
<th style="text-align: center;"><p>網頁</p></th>
<th style="text-align: center;"><p>主題曲<br />
／插曲</p></th>
<th style="text-align: center;"><p>備-{注}-</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/翡翠台電視劇集列表_(1996年).md" title="wikilink">續96年</a><br />
30/12-<br />
24/01</p></td>
<td style="text-align: center;"><p><a href="../Page/有肥人終成眷屬.md" title="wikilink">有肥人終成眷屬</a><br />
In the Name of Love</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/鄭則士.md" title="wikilink">鄭則士</a>、<a href="../Page/曹眾.md" title="wikilink">曹　眾</a>、<a href="../Page/劉雅麗.md" title="wikilink">劉雅麗</a>、<br />
<a href="../Page/陳錦鴻.md" title="wikilink">陳錦鴻</a>、<a href="../Page/李綺虹.md" title="wikilink">李綺虹</a>、<a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a>、<br />
<a href="../Page/傅明憲.md" title="wikilink">傅明憲</a></p></td>
<td style="text-align: center;"><p><a href="../Page/周華宇.md" title="wikilink">周華宇</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:但願有情人(<a href="../Page/巫啟賢.md" title="wikilink">巫啟賢</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>27/01-<br />
21/02</p></td>
<td style="text-align: center;"><p><a href="../Page/醉打金枝_(1997年电视剧).md" title="wikilink">醉打金枝</a><br />
Taming Of The Princess</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a>、<a href="../Page/關詠荷.md" title="wikilink">關詠荷</a>、<a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a>、<br />
<a href="../Page/傅明憲.md" title="wikilink">傅明憲</a>、<a href="../Page/康華.md" title="wikilink">康華</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄺業生.md" title="wikilink">鄺業生</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:皇帝女唔憂嫁(<a href="../Page/呂方.md" title="wikilink">呂方</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>24/02-<br />
21/03</p></td>
<td style="text-align: center;"><p><a href="../Page/樂壇插班生.md" title="wikilink">樂壇插班生</a><br />
Show Time Blues</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/林家棟.md" title="wikilink">林家棟</a>、<a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a>、<a href="../Page/江欣燕.md" title="wikilink">江欣燕</a>、<br />
<a href="../Page/梅小惠.md" title="wikilink">梅小惠</a>、<a href="../Page/郭少芸.md" title="wikilink">郭少芸</a>、<a href="../Page/馬德鐘.md" title="wikilink">馬德鐘</a></p></td>
<td style="text-align: center;"><p><a href="../Page/梅小青.md" title="wikilink">梅小青</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>24/03-<br />
18/04</p></td>
<td style="text-align: center;"><p><a href="../Page/苗翠花_(电视剧).md" title="wikilink">苗翠花</a><br />
Lady Flower Fist</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/江華.md" title="wikilink">江　華</a>、<a href="../Page/關詠荷.md" title="wikilink">關詠荷</a>、<a href="../Page/陳少霞.md" title="wikilink">陳少霞</a>、<br />
<a href="../Page/惠英紅.md" title="wikilink">惠英紅</a>、<a href="../Page/張鳳妮.md" title="wikilink">張鳳妮</a>、<a href="../Page/劉玉翠.md" title="wikilink">劉玉翠</a></p></td>
<td style="text-align: center;"><p><a href="../Page/蕭顯輝.md" title="wikilink">蕭顯輝</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>21/04-<br />
23/05</p></td>
<td style="text-align: center;"><p><a href="../Page/大刺客_(电视剧).md" title="wikilink">大刺客</a><br />
The Hitman Chronicles</p></td>
<td style="text-align: center;"><p>23</p></td>
<td style="text-align: center;"><p><a href="../Page/鄭則士.md" title="wikilink">鄭則士</a>、<a href="../Page/樊少皇.md" title="wikilink">樊少皇</a>、<a href="../Page/張兆輝.md" title="wikilink">張兆輝</a>、<br />
<a href="../Page/傅明憲.md" title="wikilink">傅明憲</a>、<a href="../Page/梁小冰.md" title="wikilink">梁小冰</a>、<a href="../Page/錢小豪.md" title="wikilink">錢小豪</a></p></td>
<td style="text-align: center;"><p><a href="../Page/楊錦泉.md" title="wikilink">楊錦泉</a></p></td>
<td style="text-align: center;"><p><a href="http://www.tvb.com/tvbi/program/hitman/index.htm">網頁</a></p></td>
<td style="text-align: center;"><p>主:英雄熱血(<a href="../Page/巫啓賢.md" title="wikilink">巫啓賢</a>)</p></td>
<td style="text-align: center;"><p>因收視差導致最後要腰斬, 海外版35集</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>26/05-<br />
20/06</p></td>
<td style="text-align: center;"><p><a href="../Page/香港人在廣州.md" title="wikilink">香港人在廣州</a><br />
A Road and A Will</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/鄭丹瑞.md" title="wikilink">鄭丹瑞</a>、<a href="../Page/張可頤.md" title="wikilink">張可頤</a>、<a href="../Page/劉倩怡.md" title="wikilink">劉倩怡</a>、<br />
<a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a>、<a href="../Page/梅小惠.md" title="wikilink">梅小惠</a>、<a href="../Page/曹達華.md" title="wikilink">曹達華</a></p></td>
<td style="text-align: center;"><p><a href="../Page/王心慰.md" title="wikilink">王心慰</a></p></td>
<td style="text-align: center;"><p><a href="https://web.archive.org/web/20070930203731/http://tvcity.tvb.com/drama/road/">網頁</a></p></td>
<td style="text-align: center;"><p>主:熱身(<a href="../Page/張智霖.md" title="wikilink">張智霖</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>23/06-<br />
26/07</p></td>
<td style="text-align: center;"><p><a href="../Page/難兄難弟_(1997年电视剧).md" title="wikilink">難兄難弟</a><br />
Old Time Buddy</p></td>
<td style="text-align: center;"><p>25</p></td>
<td style="text-align: center;"><p><a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a>、<a href="../Page/張可頤.md" title="wikilink">張可頤</a>、<a href="../Page/吳鎮宇.md" title="wikilink">吳鎮宇</a>、<br />
<a href="../Page/宣萱.md" title="wikilink">宣　萱</a>、<a href="../Page/林曉峰.md" title="wikilink">林曉峰</a>、<a href="../Page/滕麗明.md" title="wikilink">滕麗名</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鍾澍佳.md" title="wikilink">鍾澍佳</a></p></td>
<td style="text-align: center;"><p><a href="http://www.tvb.com/tvbi/program/buddy/buddy.htm">網頁</a></p></td>
<td style="text-align: center;"><p>主:難兄難弟(<a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a>、<a href="../Page/吳鎮宇.md" title="wikilink">吳鎮宇</a>、<a href="../Page/宣萱.md" title="wikilink">宣萱</a>、<a href="../Page/張可頤.md" title="wikilink">張可頤</a>)<br />
插:誰更重要(<a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>28/07-<br />
26/09</p></td>
<td style="text-align: center;"><p><a href="../Page/天龍八部_(1997年電視劇).md" title="wikilink">天龍八部</a><br />
The Demi-Gods and Semi-Devils</p></td>
<td style="text-align: center;"><p>45</p></td>
<td style="text-align: center;"><p><a href="../Page/黃日華.md" title="wikilink">黃日華</a>、<a href="../Page/陳浩民.md" title="wikilink">陳浩民</a>、<a href="../Page/樊少皇.md" title="wikilink">樊少皇</a>、<br />
<a href="../Page/劉錦玲.md" title="wikilink">劉錦玲</a>、<a href="../Page/潘志文.md" title="wikilink">潘志文</a>、<a href="../Page/李若彤.md" title="wikilink">李若彤</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李添勝.md" title="wikilink">李添勝</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:難唸的經(<a href="../Page/周華健.md" title="wikilink">周華健</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>29/09-<br />
07/11</p></td>
<td style="text-align: center;"><p><a href="../Page/狀王宋世傑.md" title="wikilink">狀王宋世傑</a><br />
Justice Sung</p></td>
<td style="text-align: center;"><p>30</p></td>
<td style="text-align: center;"><p><a href="../Page/張達明.md" title="wikilink">張達明</a>、<a href="../Page/郭藹明.md" title="wikilink">郭藹明</a>、<a href="../Page/樊少皇.md" title="wikilink">樊少皇</a></p></td>
<td style="text-align: center;"><p><a href="../Page/徐正康.md" title="wikilink">徐正康</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:神奇事(<a href="../Page/陳小春.md" title="wikilink">陳小春</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>10/11-<br />
20/12</p></td>
<td style="text-align: center;"><p><a href="../Page/美味天王.md" title="wikilink">美味天王</a><br />
A Recipe for the Heart</p></td>
<td style="text-align: center;"><p>29</p></td>
<td style="text-align: center;"><p><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a>、<a href="../Page/關詠荷.md" title="wikilink">關詠荷</a>、<a href="../Page/沈殿霞.md" title="wikilink">沈殿霞</a>、<br />
<a href="../Page/秦沛.md" title="wikilink">秦　沛</a>、<a href="../Page/古天樂.md" title="wikilink">古天樂</a>、<a href="../Page/宣萱.md" title="wikilink">宣　萱</a></p></td>
<td style="text-align: center;"><p><a href="../Page/梁家樹.md" title="wikilink">梁家樹</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:Mamma Mia(<a href="../Page/沈殿霞.md" title="wikilink">沈殿霞</a>、<a href="../Page/秦沛.md" title="wikilink">秦沛</a>、<a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a>、<a href="../Page/關詠荷.md" title="wikilink">關詠荷</a>、<a href="../Page/宣萱.md" title="wikilink">宣萱</a>、<a href="../Page/張可頤.md" title="wikilink">張可頤</a>)<br />
插:笑住掛住你(<a href="../Page/沈殿霞.md" title="wikilink">沈殿霞</a>、<a href="../Page/秦沛.md" title="wikilink">秦沛</a>、<a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a>、<a href="../Page/關詠荷.md" title="wikilink">關詠荷</a>、<a href="../Page/宣萱.md" title="wikilink">宣萱</a>、<a href="../Page/張可頤.md" title="wikilink">張可頤</a>)<br />
插:彼此的世界早已拉近(<a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>22/12-<br />
09/01<br />
<a href="../Page/翡翠台電視劇集列表_(1998年).md" title="wikilink">續98年</a></p></td>
<td style="text-align: center;"><p><a href="../Page/真命天師.md" title="wikilink">真命天師</a><br />
Triumph Over Evil</p></td>
<td style="text-align: center;"><p>15</p></td>
<td style="text-align: center;"><p><a href="../Page/黃智賢.md" title="wikilink">黃智賢</a>、<a href="../Page/張家輝.md" title="wikilink">張家輝</a>、<a href="../Page/滕麗明.md" title="wikilink">滕麗明</a></p></td>
<td style="text-align: center;"><p><a href="../Page/蕭顯輝.md" title="wikilink">蕭顯輝</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:人生中(<a href="../Page/譚耀文.md" title="wikilink">譚耀文</a>)</p></td>
<td style="text-align: center;"><p>海外版20集</p></td>
</tr>
</tbody>
</table>

### 第二線劇集

[香港時間逢星期一至五](../Page/香港時間.md "wikilink")21:00-22:00播出，9月29日起21:35-22:35播出

<table style="width:344%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 18%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>監製</p></th>
<th style="text-align: center;"><p>網頁</p></th>
<th style="text-align: center;"><p>主題曲<br />
／插曲</p></th>
<th style="text-align: center;"><p>備-{注}-</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/翡翠台電視劇集列表_(1996年).md" title="wikilink">續96年</a><br />
09/12-<br />
03/01</p></td>
<td style="text-align: center;"><p><a href="../Page/地獄天使.md" title="wikilink">地獄天使</a><br />
One Good Turn Deserves Another</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/張可頤.md" title="wikilink">張可頤</a>、<a href="../Page/陳啟泰.md" title="wikilink">陳啟泰</a>、<a href="../Page/蘇玉華.md" title="wikilink">蘇玉華</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李艷芳.md" title="wikilink">李艷芳</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:回報(<a href="../Page/陳慧嫻.md" title="wikilink">陳慧嫻</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>06/01-<br />
24/01</p></td>
<td style="text-align: center;"><p><a href="../Page/皇家反千組.md" title="wikilink">皇家反千組</a><br />
Corner the Con Man</p></td>
<td style="text-align: center;"><p>15</p></td>
<td style="text-align: center;"><p><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a>、<a href="../Page/古巨基.md" title="wikilink">古巨基</a>、<a href="../Page/陳法蓉.md" title="wikilink">陳法蓉</a>、<br />
<a href="../Page/傅明憲.md" title="wikilink">傅明憲</a>、<a href="../Page/尹揚明.md" title="wikilink">尹揚明</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄺業生.md" title="wikilink">鄺業生</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>|主:精彩故事(<a href="../Page/羅文.md" title="wikilink">羅文</a>)<br />
插:難得有你(<a href="../Page/羅文.md" title="wikilink">羅文</a>)</p></td>
<td style="text-align: center;"><p>海外版20集</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>27/01-<br />
14/02</p></td>
<td style="text-align: center;"><p><a href="../Page/濟公_(1997年電視劇).md" title="wikilink">濟公</a><br />
The Legend of Master Chai</p></td>
<td style="text-align: center;"><p>15</p></td>
<td style="text-align: center;"><p><a href="../Page/梁榮忠.md" title="wikilink">梁榮忠</a>、<a href="../Page/何寶生.md" title="wikilink">何寶生</a>、<a href="../Page/梁小冰.md" title="wikilink">梁小冰</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李元科.md" title="wikilink">李元科</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>|主:瀟灑一派(<a href="../Page/李克勤.md" title="wikilink">李克勤</a>)</p></td>
<td style="text-align: center;"><p>海外版20集</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>17/02-<br />
07/03</p></td>
<td style="text-align: center;"><p><a href="../Page/當女人愛上男人.md" title="wikilink">當女人愛上男人</a><br />
Working Women</p></td>
<td style="text-align: center;"><p>15</p></td>
<td style="text-align: center;"><p><a href="../Page/劉錦玲.md" title="wikilink">劉錦玲</a>、<a href="../Page/江欣燕.md" title="wikilink">江欣燕</a>、<a href="../Page/郭可盈.md" title="wikilink">郭可盈</a>、<a href="../Page/梅小惠.md" title="wikilink">梅小惠</a>、<a href="../Page/張國強.md" title="wikilink">張國強</a>、<a href="../Page/陳啟泰.md" title="wikilink">陳啟泰</a>、<a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a></p></td>
<td style="text-align: center;"><p><a href="../Page/梅小青.md" title="wikilink">梅小青</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>|主：放不低〔<a href="../Page/鄭秀文.md" title="wikilink">鄭秀文</a>〕</p></td>
<td style="text-align: center;"><p>海外版20集</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>10/03-<br />
09/05</p></td>
<td style="text-align: center;"><p><a href="../Page/壹號皇庭V.md" title="wikilink">壹號皇庭V</a><br />
The File of Justice V</p></td>
<td style="text-align: center;"><p>45</p></td>
<td style="text-align: center;"><p><a href="../Page/歐陽震華.md" title="wikilink">歐陽震華</a>、<a href="../Page/陶大宇.md" title="wikilink">陶大宇</a>、<a href="../Page/蘇永康.md" title="wikilink">蘇永康</a>、<br />
<a href="../Page/吳啟華.md" title="wikilink">吳啟華</a>、<a href="../Page/譚耀文.md" title="wikilink">譚耀文</a>、<a href="../Page/林保怡.md" title="wikilink">林保怡</a>、<br />
<a href="../Page/馬浚偉.md" title="wikilink">馬浚偉</a>、<a href="../Page/宣萱.md" title="wikilink">宣　萱</a>、<a href="../Page/蔡少芬.md" title="wikilink">蔡少芬</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄧特希.md" title="wikilink">鄧特希</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主題音樂:壹號皇庭(<a href="../Page/:en:Mark_Knopfler.md" title="wikilink">Mark Knopfler</a>, Francis Law作曲)<br />
插:從來未發生(<a href="../Page/蘇永康.md" title="wikilink">蘇永康</a>)<br />
插:不應該發生(<a href="../Page/馬浚偉.md" title="wikilink">馬浚偉</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>12/05-<br />
06/06</p></td>
<td style="text-align: center;"><p><a href="../Page/迷離檔案.md" title="wikilink">迷離檔案</a><br />
Mystery Files</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/羅嘉良.md" title="wikilink">羅嘉良</a>、<a href="../Page/張家輝.md" title="wikilink">張家輝</a>、<a href="../Page/張可頤.md" title="wikilink">張可頤</a>、<br />
<a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a>、<a href="../Page/關寶慧.md" title="wikilink">關寶慧</a>、<a href="../Page/魯文傑.md" title="wikilink">魯文傑</a></p></td>
<td style="text-align: center;"><p><a href="../Page/叶成康.md" title="wikilink">叶成康</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>09/06-<br />
04/07</p></td>
<td style="text-align: center;"><p><a href="../Page/保護證人組_(电视剧).md" title="wikilink">保護證人組</a><br />
Deadly Protection</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/魏駿傑.md" title="wikilink">魏駿傑</a>、<a href="../Page/王喜.md" title="wikilink">王　喜</a>、<a href="../Page/傅明憲.md" title="wikilink">傅明憲</a></p></td>
<td style="text-align: center;"><p><a href="../Page/李元科.md" title="wikilink">李元科</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:人間好漢(<a href="../Page/林子祥.md" title="wikilink">林子祥</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>07/07-<br />
01/08</p></td>
<td style="text-align: center;"><p><a href="../Page/男人四十打功夫.md" title="wikilink">男人四十打功夫</a><br />
Drunken Angels</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/元華.md" title="wikilink">元　華</a>、<a href="../Page/陳妙瑛.md" title="wikilink">陳妙瑛</a>、<a href="../Page/吳毅將.md" title="wikilink">吳毅將</a>、<br />
<a href="../Page/容錦昌.md" title="wikilink">容錦昌</a>、<a href="../Page/楊玉梅.md" title="wikilink">楊玉梅</a>、<a href="../Page/楊羚.md" title="wikilink">楊　羚</a></p></td>
<td style="text-align: center;"><p><a href="../Page/莊偉建.md" title="wikilink">莊偉建</a></p></td>
<td style="text-align: center;"><p><a href="http://www.tvb.com/tvbi/program/drunken_angels/drunken_angels.htm">網頁</a></p></td>
<td style="text-align: center;"><p>主:蓋世男兒(<a href="../Page/林子祥.md" title="wikilink">林子祥</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>04/08-<br />
29/08</p></td>
<td style="text-align: center;"><p><a href="../Page/大鬧廣昌隆_(电视剧).md" title="wikilink">大鬧廣昌隆</a><br />
Time Before Time</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/林家棟.md" title="wikilink">林家棟</a>、<a href="../Page/周海媚.md" title="wikilink">周海媚</a>、<a href="../Page/郭少芸.md" title="wikilink">郭少芸</a>、<br />
<a href="../Page/楊羚.md" title="wikilink">楊　羚</a>、<a href="../Page/陳啟泰.md" title="wikilink">陳啟泰</a>、<a href="../Page/鄧兆尊.md" title="wikilink">鄧兆尊</a></p></td>
<td style="text-align: center;"><p><a href="../Page/楊錦泉.md" title="wikilink">楊錦泉</a></p></td>
<td style="text-align: center;"><p><a href="http://www.tvb.com/tvbi/program/ghost/ghost.htm">網頁</a></p></td>
<td style="text-align: center;"><p>主:抱緊眼前人(<a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a>)<br />
插:我愛風光好(<a href="../Page/梅艷芳.md" title="wikilink">梅艷芳</a>、<a href="../Page/倫永亮.md" title="wikilink">倫永亮</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>01/09-<br />
24/10</p></td>
<td style="text-align: center;"><p><a href="../Page/刑事偵緝檔案III.md" title="wikilink">刑事偵緝檔案III</a><br />
Detective Investigation Files III</p></td>
<td style="text-align: center;"><p>40</p></td>
<td style="text-align: center;"><p><a href="../Page/陶大宇.md" title="wikilink">陶大宇</a>、<a href="../Page/郭可盈.md" title="wikilink">郭可盈</a>、<a href="../Page/陳法蓉.md" title="wikilink">陳法蓉</a>、<br />
<a href="../Page/梁榮忠.md" title="wikilink">梁榮忠</a>、<a href="../Page/鍾麗淇.md" title="wikilink">鍾麗淇</a>、<a href="../Page/楊婉儀.md" title="wikilink">楊婉儀</a></p></td>
<td style="text-align: center;"><p><a href="../Page/潘嘉德.md" title="wikilink">潘嘉德</a></p></td>
<td style="text-align: center;"><p><a href="http://www.tvb.com/tvbi/program/detect3/detect3.htm">網頁</a></p></td>
<td style="text-align: center;"><p>主:難得情真(<a href="../Page/梁漢文.md" title="wikilink">梁漢文</a>)<br />
插:忘得了，忘不了(<a href="../Page/梁漢文.md" title="wikilink">梁漢文</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>27/10-<br />
21/11</p></td>
<td style="text-align: center;"><p><a href="../Page/隱形怪傑.md" title="wikilink">隱形怪傑</a><br />
The Disappearance</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/林家棟.md" title="wikilink">林家棟</a>、<a href="../Page/陳妙瑛.md" title="wikilink">陳妙瑛</a>、<a href="../Page/陳啟泰.md" title="wikilink">陳啟泰</a>、<br />
<a href="../Page/林保怡.md" title="wikilink">林保怡</a>、<a href="../Page/楊千嬅.md" title="wikilink">楊千嬅</a>、<a href="../Page/梁漢文.md" title="wikilink">梁漢文</a></p></td>
<td style="text-align: center;"><p><a href="../Page/楊錦泉.md" title="wikilink">楊錦泉</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:透明(<a href="../Page/梁漢文.md" title="wikilink">梁漢文</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>24/11-<br />
19/12</p></td>
<td style="text-align: center;"><p><a href="../Page/廉政追緝令.md" title="wikilink">廉政追緝令</a><br />
I Can't Accept Corruption</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/古天樂.md" title="wikilink">古天樂</a>、<a href="../Page/陳法蓉.md" title="wikilink">陳法蓉</a>、<a href="../Page/袁潔瑩.md" title="wikilink">袁潔瑩</a>、<br />
<a href="../Page/何寶生.md" title="wikilink">何寶生</a>、<a href="../Page/張玉珊.md" title="wikilink">張玉珊</a>、<a href="../Page/馬德鐘.md" title="wikilink">馬德鐘</a></p></td>
<td style="text-align: center;"><p><a href="../Page/梅小青.md" title="wikilink">梅小青</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:藏身(<a href="../Page/鄭中基.md" title="wikilink">鄭中基</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>22/12-<br />
16/01<br />
<a href="../Page/翡翠台電視劇集列表_(1998年).md" title="wikilink">續98年</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鑑證實錄.md" title="wikilink">鑑證實錄</a><br />
Untraceable Evidence</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"><p><a href="../Page/林保怡.md" title="wikilink">林保怡</a>、<a href="../Page/陳慧珊.md" title="wikilink">陳慧珊</a>、<a href="../Page/魯文傑.md" title="wikilink">魯文傑</a>、<br />
<a href="../Page/鍾麗淇.md" title="wikilink">鍾麗淇</a>、<a href="../Page/李珊珊_(藝員).md" title="wikilink">李珊珊</a>、<a href="../Page/陳彥行.md" title="wikilink">陳彥行</a></p></td>
<td style="text-align: center;"><p><a href="../Page/潘嘉德.md" title="wikilink">潘嘉德</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:留痕(<a href="../Page/梁漢文.md" title="wikilink">梁漢文</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 第三線劇集

[香港時間逢星期一至五](../Page/香港時間.md "wikilink")22:20-22:45(9月29日-12月31日改为22:35-23:00)播出

<table style="width:344%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 18%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>監製</p></th>
<th style="text-align: center;"><p>網頁</p></th>
<th style="text-align: center;"><p>主題曲<br />
／插曲</p></th>
<th style="text-align: center;"><p>備-{注}-</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/翡翠台電視劇集列表_(1996年).md" title="wikilink">續96年</a><br />
全年<br />
<a href="../Page/翡翠台電視劇集列表_(1998年).md" title="wikilink">續98年</a></p></td>
<td style="text-align: center;"><p><a href="../Page/真情.md" title="wikilink">真情</a><br />
A Kindred Spirit</p></td>
<td style="text-align: center;"><p>1128</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p><a href="../Page/徐遇安.md" title="wikilink">徐遇安</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:無侮愛你一生(<a href="../Page/李樂詩.md" title="wikilink">李樂詩</a>)<br />
插:傳聞(<a href="../Page/呂方.md" title="wikilink">呂方</a>)</p></td>
<td style="text-align: center;"><p>香港主权移交期间停播三天（6月30日-7月2日）。</p></td>
</tr>
</tbody>
</table>

### 其他時段

<table style="width:344%;">
<colgroup>
<col style="width: 60%" />
<col style="width: 17%" />
<col style="width: 50%" />
<col style="width: 24%" />
<col style="width: 90%" />
<col style="width: 70%" />
<col style="width: 18%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"><p>播放<br />
日期</p></th>
<th style="text-align: center;"><p>電視劇集名稱</p></th>
<th style="text-align: center;"><p>集數</p></th>
<th style="text-align: center;"><p>主演</p></th>
<th style="text-align: center;"><p>監製</p></th>
<th style="text-align: center;"><p>網頁</p></th>
<th style="text-align: center;"><p>主題曲<br />
／插曲</p></th>
<th style="text-align: center;"><p>備-{注}-</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>03/07-<br />
30/07</p></td>
<td style="text-align: center;"><p><a href="../Page/圓月彎刀_(電視劇).md" title="wikilink">圓月彎刀</a><br />
Against The Blade Of Honour</p></td>
<td style="text-align: center;"><p>20</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p><a href="../Page/蕭顯輝.md" title="wikilink">蕭顯輝</a></p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>主:意難平(<a href="../Page/伍詠薇.md" title="wikilink">伍詠薇</a>)<br />
插:圓月下你來依我(<a href="../Page/伍詠薇.md" title="wikilink">伍詠薇</a>)</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>05/09-<br />
30/10</p></td>
<td style="text-align: center;"><p><a href="../Page/俠義見青天.md" title="wikilink">俠義見青天</a><br />
</p></td>
<td style="text-align: center;"><p>40</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>外購劇<br />
於17:55-18:25播出</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>06</p></td>
<td style="text-align: center;"><p><a href="../Page/香港的故事.md" title="wikilink">香港的故事</a><br />
</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>外購劇<br />
於17:55-18:25播出</p></td>
</tr>
</tbody>
</table>

## 參見

[1997年無綫電視劇集](../Category/1997年無綫電視劇集.md "wikilink")
[Category:無綫電視劇集列表](../Category/無綫電視劇集列表.md "wikilink")