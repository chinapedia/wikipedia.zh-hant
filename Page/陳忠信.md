<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>私立<a href="../Page/東海大學.md" title="wikilink">東海大學數學系</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li><a href="../Page/美麗島雜誌.md" title="wikilink">美麗島雜誌主編</a></li>
<li><a href="../Page/自立早報.md" title="wikilink">自立早報主筆</a></li>
<li><a href="../Page/民主進步黨.md" title="wikilink">民主進步黨副秘書長</a></li>
<li>立法委員<br />
<span style="color: blue;">(1999年2月1日-2004年5月31日)</li>
<li><a href="../Page/行政院大陸委員會.md" title="wikilink">行政院大陸委員會諮詢委員</a></li>
<li><a href="../Page/海峽交流基金會.md" title="wikilink">海峽交流基金會董事</a></li>
<li><a href="../Page/立法院國防委員會.md" title="wikilink">立法院國防委員會召集委員</a></li>
<li><a href="../Page/國家安全會議_(中華民國).md" title="wikilink">國家安全會議副秘書長</a></li>
<li><a href="../Page/國家安全會議_(中華民國).md" title="wikilink">國家安全會議秘書長</a>(代理)</li>
<li><a href="../Page/國家安全會議_(中華民國).md" title="wikilink">國家安全會議諮詢委員</a></li>
<li>民主進步黨臺中市市長參選人<a href="../Page/林佳龍.md" title="wikilink">林佳龍北區後援會總幹事</a><br />
<span style="color: blue;">(2014年)</li>
</ul></td>
</tr>
</tbody>
</table>

**陳忠信**（），筆名**杭之**，為台灣的政治人物之一，曾經代表[民主進步黨任職](../Page/民主進步黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")。

## 著作

  - 《一葦集──現代化發展的反省片斷》（臺北：允晨文化，1987）
  - 《一葦集‧續篇》（臺北：允晨文化，1987）
  - 《邁向後美麗島的民間社會》（臺北：[唐山出版社](../Page/唐山出版社.md "wikilink")，1990）
  - 《國家政策與批判的公共論述》（臺北：業強出版社，1997）

## 外部連結

  - [立法院](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00128&stage=4)

|- |colspan="3"
style="text-align:center;"|**[中華民國](../Page/中華民國.md "wikilink")[總統府](../Page/總統府.md "wikilink")**


[C陳](../Category/第4屆中華民國立法委員.md "wikilink")
[C陳](../Category/第5屆中華民國立法委員.md "wikilink")
[C陳](../Category/民主進步黨黨員.md "wikilink")
[C陳](../Category/東海大學校友.md "wikilink")
[C陳](../Category/台灣作家.md "wikilink")
[Category:陳姓](../Category/陳姓.md "wikilink")