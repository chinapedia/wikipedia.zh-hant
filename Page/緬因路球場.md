**緬因路球場**（**Maine
Road**）是位於[英國](../Page/英國.md "wikilink")[曼徹斯特市的足球場](../Page/曼徹斯特.md "wikilink")，由落成的1923年至2003年間是[曼城的主場](../Page/曼城.md "wikilink")。緬因路球場建於舊磚塊工廠的故址，於1923年啟用時稱為“北方溫布萊”（位於[倫敦的](../Page/倫敦.md "wikilink")[溫布萊球場剛於數月前完成](../Page/溫布萊球場.md "wikilink")），可容納80,000名觀眾。

首場負於[錫菲聯的比賽入場人數達](../Page/錫菲聯.md "wikilink")56,993人。在[二次大戰後](../Page/第二次世界大戰.md "wikilink")，[曼聯的主場](../Page/曼聯.md "wikilink")[-{zh-hans:老特拉福德球場;
zh-hant:奧脫福球場;}-因空襲遭部份破壞而停用](../Page/老特拉福德球場.md "wikilink")，緬因路球場成為曼城與曼聯的共同主場。緬因路球場錄得最高入場人數是在1934年曼城在[足總杯第六圈對](../Page/英格兰足总杯.md "wikilink")[史篤城](../Page/史篤城.md "wikilink")，共有84,569名觀眾入場。緬因路球場曾成兩次成為[英格蘭國際賽的主場](../Page/英格兰足球代表队.md "wikilink")，1946年11月13日對[威爾斯及](../Page/威爾斯足球代表隊.md "wikilink")1949年11月16日[50年世界杯外圍賽以](../Page/世界杯足球赛.md "wikilink")9-2大破[北愛爾蘭](../Page/北愛爾蘭足球代表隊.md "wikilink")。緬因路球場關閉前最後一場比賽在2003年5月11日舉行，曼城以1-0見負於[修咸頓](../Page/修咸頓.md "wikilink")，[米高史雲遜射入緬因路最後的一球](../Page/米凯尔·斯文森.md "wikilink")。

在[2002-03年英超聯季後緬因路球場隨即被拆除](../Page/2002–03賽季英格蘭超級聯賽.md "wikilink")，遺址將改建為住宅小區作為[摩斯西迪及](../Page/Moss_Side.md "wikilink")[魯什爾姆的改善工程](../Page/Rusholme.md "wikilink")。

## 場地

很長時間緬因路球場擁有英格蘭最闊的場地，但是場地的闊度是經常因領隊的要求而轉變，以適應球隊的踢法。在拆卸前，場地面積是107 x 71米（117
x 77 2/3碼）。

## 外部連結

  -
  -
[Category:1923年完工體育場館](../Category/1923年完工體育場館.md "wikilink")
[Category:英格蘭已不存在的足球場](../Category/英格蘭已不存在的足球場.md "wikilink")
[Category:曼城足球俱乐部](../Category/曼城足球俱乐部.md "wikilink")
[Category:曼联足球俱乐部](../Category/曼联足球俱乐部.md "wikilink")
[Category:英超球場](../Category/英超球場.md "wikilink")
[Category:曼徹斯特歷史](../Category/曼徹斯特歷史.md "wikilink")