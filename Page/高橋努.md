**高橋努**，日本男性[漫畫家](../Page/漫畫家.md "wikilink")。東京都出身。

## 作品

  - [地雷震](../Page/地雷震.md "wikilink")、全19冊、東立、原名《》（1992年～2000年）
  - [ALIVE](../Page/ALIVE.md "wikilink")、全1冊、尖端、原名《》（1999年）
  - [紅粉野球隊](../Page/紅粉野球隊.md "wikilink")、全9冊、東販、原名《》（2000年～2002年）
  - [心魔大審判](../Page/心魔大審判.md "wikilink")、全2冊、尖端、原名《》（2001年～2002年）
      - 心魔大審判-業障、全2冊、尖端、原名《》（2003年）
      - 心魔大審判-新章、全4冊、尖端、原名《》（2003年～2004年）
      - 原名《》（2005年～2010）
  - [BLUE
    HEAVEN](../Page/BLUE_HEAVEN.md "wikilink")、全3冊、原名《》（2002年～2003年）※短編『69』收錄
  - [爆音列島](../Page/爆音列島.md "wikilink")、10冊待續、長鴻、原名《》（2002年）、日文版18冊完結
  - [SIDOOH 士道](../Page/SIDOOH_士道.md "wikilink")、全25冊、原名《》（2005年\~2010）
  - [士道 SUNRISE](../Page/士道_SUNRISE.md "wikilink")、全1冊、原名《》（2011年）
  - 守護靈、全8冊、原名《ヒトヒトリフタリ》（2011年～2013年）

## 外部連結

  - [](http://tao-69.com/)

  - [](http://talent.yahoo.co.jp/pf/detail/pp173279)

  - [](https://web.archive.org/web/20100120071922/http://tunes.sakura.ne.jp/tsutomu/)

  -
[Category:日本漫画家](../Category/日本漫画家.md "wikilink")