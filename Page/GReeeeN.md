**GReeeeN**是[日本的](../Page/日本.md "wikilink")4人組創作樂團。

## 「GReeeeN」的由來

「GReeeeN」 是由「Green Boy」這個字衍生出來的。「Green
Boy」有「新人」、「未成熟」的意思。根據成員HIDE在2009年3月8日BLOG中的解釋，GReeeeN的意思是「尚未完成所以會繼續下去，蘊藏有未知的可能性」。

「GReeeeN」中的4個小寫的「e」則代表成員人數。而且成員都是牙醫學系出身，這亦代表笑的時候能夠看見牙齒，表達出成員「希望能傳達微笑（grin）」的意念。

## 成員

  - **HIDE**（1980年4月3日－）[京都府出身](../Page/京都府.md "wikilink")，負責高音部分
  - **navi**（1980年4月30日－）[宮城縣出身](../Page/宮城縣.md "wikilink")，負責高音部分
  - **92**（1982年3月21日－）[沖繩縣出身](../Page/沖繩縣.md "wikilink")，負責低音部分
  - **SOH**（1981年2月2日－）[佐賀縣出身](../Page/佐賀縣.md "wikilink")，負責低音部分

全部團員畢業於[福島縣](../Page/福島縣.md "wikilink")[奥羽大學的](../Page/奥羽大學.md "wikilink")[牙醫學系](../Page/牙醫.md "wikilink")。而且皆已通過國家考試並取得執照成為執業牙醫。

為防止觸犯法律及影響牙醫工作，因此從不公開任何個人照片，即使MV也不是團員親自演出。甚至是訪問中，除了因病人應約而沒有出席的**SOH**外每個成員都是穿上布偶服接受訪問。

## 音樂作品

### 單曲

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>發行日期</p></th>
<th><p>名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p><a href="../Page/2007年.md" title="wikilink">2007年</a><a href="../Page/1月24日.md" title="wikilink">1月24日</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p><a href="../Page/2007年.md" title="wikilink">2007年</a><a href="../Page/3月28日.md" title="wikilink">3月28日</a></p></td>
<td><p><br />
（）</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p><a href="../Page/2007年.md" title="wikilink">2007年</a><a href="../Page/5月16日.md" title="wikilink">5月16日</a></p></td>
<td><p><a href="../Page/愛歌.md" title="wikilink">愛歌</a><br />
（愛唄）</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p><a href="../Page/2007年.md" title="wikilink">2007年</a><a href="../Page/11月14日.md" title="wikilink">11月14日</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p><a href="../Page/2008年.md" title="wikilink">2008年</a><a href="../Page/1月16日.md" title="wikilink">1月16日</a></p></td>
<td><p><br />
（BE FREE/涙空）</p></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p><a href="../Page/2008年.md" title="wikilink">2008年</a><a href="../Page/3月5日.md" title="wikilink">3月5日</a></p></td>
<td><p><br />
（旅立ち）</p></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p><a href="../Page/2008年.md" title="wikilink">2008年</a><a href="../Page/5月28日.md" title="wikilink">5月28日</a></p></td>
<td><p><a href="../Page/奇蹟_(GReeeeN單曲).md" title="wikilink">奇蹟</a><br />
（）</p></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p><a href="../Page/2008年.md" title="wikilink">2008年</a><a href="../Page/12月3日.md" title="wikilink">12月3日</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9th</p></td>
<td><p><a href="../Page/2009年.md" title="wikilink">2009年</a><a href="../Page/1月28日.md" title="wikilink">1月28日</a></p></td>
<td><p><br />
（歩み）</p></td>
</tr>
<tr class="even">
<td><p>10th</p></td>
<td><p><a href="../Page/2009年.md" title="wikilink">2009年</a><a href="../Page/3月11日.md" title="wikilink">3月11日</a></p></td>
<td><p><a href="../Page/刹那_(單曲).md" title="wikilink">刹那</a><br />
（剎那）</p></td>
</tr>
<tr class="odd">
<td><p>11th</p></td>
<td><p><a href="../Page/2009年.md" title="wikilink">2009年</a><a href="../Page/5月27日.md" title="wikilink">5月27日</a></p></td>
<td><p><a href="../Page/遙遠_(GReeeeN單曲).md" title="wikilink">遙遠</a><br />
（遥か）</p></td>
</tr>
<tr class="even">
<td><p>12th</p></td>
<td><p><a href="../Page/2011年.md" title="wikilink">2011年</a><a href="../Page/6月22日.md" title="wikilink">6月22日</a></p></td>
<td><p><br />
（花唄）</p></td>
</tr>
<tr class="odd">
<td><p>13th</p></td>
<td><p><a href="../Page/2011年.md" title="wikilink">2011年</a><a href="../Page/7月20日.md" title="wikilink">7月20日</a></p></td>
<td><p><br />
（ソラシド）</p></td>
</tr>
<tr class="even">
<td><p>14th</p></td>
<td><p><a href="../Page/2011年.md" title="wikilink">2011年</a><a href="../Page/11月16日.md" title="wikilink">11月16日</a></p></td>
<td><p><br />
（恋文～ラブレター～）</p></td>
</tr>
<tr class="odd">
<td><p>15th</p></td>
<td><p><a href="../Page/2012年.md" title="wikilink">2012年</a><a href="../Page/2月29日.md" title="wikilink">2月29日</a></p></td>
<td><p><a href="../Page/強忍淚水，總有一天.md" title="wikilink">強忍淚水，總有一天</a><br />
（ミセナイナミダハ、きっといつか）</p></td>
</tr>
<tr class="even">
<td><p>16th</p></td>
<td><p><a href="../Page/2012年.md" title="wikilink">2012年</a><a href="../Page/4月25日.md" title="wikilink">4月25日</a></p></td>
<td><p><br />
（オレンジ）</p></td>
</tr>
<tr class="odd">
<td><p>17th</p></td>
<td><p><a href="../Page/2012年.md" title="wikilink">2012年</a><a href="../Page/5月30日.md" title="wikilink">5月30日</a></p></td>
<td><p>{{tsl|ja|OH</p></td>
</tr>
<tr class="even">
<td><p>18th</p></td>
<td><p><a href="../Page/2012年.md" title="wikilink">2012年</a><a href="../Page/12月19日.md" title="wikilink">12月19日</a></p></td>
<td><p><a href="../Page/雪之音.md" title="wikilink">雪之音</a><br />
（雪の音）</p></td>
</tr>
<tr class="odd">
<td><p>19th</p></td>
<td><p><a href="../Page/2013年.md" title="wikilink">2013年</a><a href="../Page/2月27日.md" title="wikilink">2月27日</a></p></td>
<td><p><br />
（桜color）</p></td>
</tr>
<tr class="even">
<td><p>20th</p></td>
<td><p><a href="../Page/2013年.md" title="wikilink">2013年</a><a href="../Page/4月17日.md" title="wikilink">4月17日</a></p></td>
<td><p><br />
（イカロス）</p></td>
</tr>
<tr class="odd">
<td><p>21st</p></td>
<td><p><a href="../Page/2013年.md" title="wikilink">2013年</a><a href="../Page/5月15日.md" title="wikilink">5月15日</a></p></td>
<td><p><br />
（HEROES）</p></td>
</tr>
<tr class="even">
<td><p>22nd</p></td>
<td><p><a href="../Page/2013年.md" title="wikilink">2013年</a><a href="../Page/8月21日.md" title="wikilink">8月21日</a></p></td>
<td><p><br />
（愛し君へ）</p></td>
</tr>
<tr class="odd">
<td><p>23rd</p></td>
<td><p><a href="../Page/2013年.md" title="wikilink">2013年</a><a href="../Page/11月27日.md" title="wikilink">11月27日</a></p></td>
<td><p><br />
（僕らの物語）</p></td>
</tr>
<tr class="even">
<td><p>24th</p></td>
<td><p><a href="../Page/2014年.md" title="wikilink">2014年</a><a href="../Page/3月19日.md" title="wikilink">3月19日</a></p></td>
<td><p><br />
（愛すべき明日、一瞬と一生を）</p></td>
</tr>
<tr class="odd">
<td><p>25th</p></td>
<td><p><a href="../Page/2015年.md" title="wikilink">2015年</a><a href="../Page/8月19日.md" title="wikilink">8月19日</a></p></td>
<td><p><br />
（SAKAMOTO）</p></td>
</tr>
<tr class="even">
<td><p>26th</p></td>
<td><p><a href="../Page/2016年.md" title="wikilink">2016年</a><a href="../Page/2月24日.md" title="wikilink">2月24日</a></p></td>
<td><p><br />
（夢）</p></td>
</tr>
<tr class="odd">
<td><p>27th</p></td>
<td><p><a href="../Page/2016年.md" title="wikilink">2016年</a><a href="../Page/3月30日.md" title="wikilink">3月30日</a></p></td>
<td><p><br />
（始まりの唄）</p></td>
</tr>
<tr class="even">
<td><p>28th</p></td>
<td><p><a href="../Page/2016年.md" title="wikilink">2016年</a><a href="../Page/7月27日.md" title="wikilink">7月27日</a></p></td>
<td><p><br />
（beautiful days）</p></td>
</tr>
<tr class="odd">
<td><p>29th</p></td>
<td><p><a href="../Page/2016年.md" title="wikilink">2016年</a><a href="../Page/12月7日.md" title="wikilink">12月7日</a></p></td>
<td><p><br />
（暁の君に）</p></td>
</tr>
<tr class="even">
<td><p>30th</p></td>
<td><p><a href="../Page/2017年.md" title="wikilink">2017年</a><a href="../Page/5月24日.md" title="wikilink">5月24日</a></p></td>
<td><p><br />
（テトテとテントテン with whiteeeen）</p></td>
</tr>
<tr class="odd">
<td><p>31st</p></td>
<td><p><a href="../Page/2018年.md" title="wikilink">2018年</a><a href="../Page/2月7日.md" title="wikilink">2月7日</a></p></td>
<td><p><br />
（ハロー カゲロウ）</p></td>
</tr>
<tr class="even">
<td><p>32nd</p></td>
<td><p><a href="../Page/2018年.md" title="wikilink">2018年</a><a href="../Page/10月31日.md" title="wikilink">10月31日</a></p></td>
<td><p><br />
（贈る言葉）</p></td>
</tr>
<tr class="odd">
<td><p>33rd</p></td>
<td><p><a href="../Page/2019年.md" title="wikilink">2019年</a><a href="../Page/1月23日.md" title="wikilink">1月23日</a></p></td>
<td><p><br />
（約束 × No title）</p></td>
</tr>
</tbody>
</table>

  - 2015年4月1日開始，為配合[福島縣](../Page/福島縣.md "wikilink")[郡山市的宣傳活動及促進地域活化](../Page/郡山市.md "wikilink")，GReeeeN兩首08年度的單曲[キセキ和](../Page/奇蹟_\(GReeeeN單曲\).md "wikilink")[扉分別設定為](../Page/扉_\(GReeeeN單曲\).md "wikilink")[JR東日本](../Page/東日本旅客鐵道.md "wikilink")[郡山站](../Page/郡山站_\(福島縣\).md "wikilink")[新幹線和](../Page/東北新幹線.md "wikilink")[在來線的](../Page/東北本線.md "wikilink")[發車音樂](../Page/發車音樂.md "wikilink")，取代原有的首都圈型音樂（五感工房製作的JR-SH-2）。

### 原創專輯

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>發售日期</p></th>
<th><p>名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2007年6月27日</p></td>
<td><p><a href="../Page/你好。請多多指教。.md" title="wikilink">你好。請多多指教。</a><br />
（）</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2008年6月25日</p></td>
<td><p><a href="../Page/啊，你好。好久不見。.md" title="wikilink">啊，你好。好久不見。</a><br />
（）</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2009年6月10日</p></td>
<td><p><a href="../Page/胡椒鹽_(GReeeeN專輯).md" title="wikilink">胡椒鹽</a><br />
（）</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2012年6月27日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>2013年6月19日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p>2014年8月6日</p></td>
<td><p><a href="../Page/今から親指が消える手品しまーす.md" title="wikilink">今から親指が消える手品しまーす</a> 。</p></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p>2016年9月14日</p></td>
<td><p><a href="../Page/縁.md" title="wikilink">縁</a></p></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p>2018年4月11日</p></td>
<td><p><a href="../Page/うれＤ.md" title="wikilink">うれＤ</a></p></td>
</tr>
</tbody>
</table>

### 精選輯

<table>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>發售日期</p></th>
<th><p>名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2009年11月25日</p></td>
<td><p><a href="../Page/A、B面精選！？.md" title="wikilink">A、B面精選！？</a><br />
（）</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2015年6月24日</p></td>
<td><p><a href="../Page/C、Dですと!?.md" title="wikilink">C、Dですと!?</a></p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2017年1月24日</p></td>
<td><p><a href="../Page/ALL_SINGLeeeeS_〜&amp;_New_Beginning〜.md" title="wikilink">ALL SINGLeeeeS 〜&amp; New Beginning〜</a></p></td>
</tr>
</tbody>
</table>

### 企劃專輯

| 發售日期       | 名稱                      |
| ---------- | ----------------------- |
| 2010年5月26日 | AB DEST\!? TOUR\!? 2010 |

### DVD

| 發售日期                                                                       | 名稱                              |
| -------------------------------------------------------------------------- | ------------------------------- |
| [2009年](../Page/2009年.md "wikilink")[12月9日](../Page/12月9日.md "wikilink")   | {{lang|ja|これ、PVでSHOWっ           |
| [2012年](../Page/2012年.md "wikilink")[12月31日](../Page/12月31日.md "wikilink") | 『緑一色歌合戦』の思ひ出〜2012年6月27日 NHKホール〜 |

### 電影

<table>
<thead>
<tr class="header">
<th><p>日本的上映日期</p></th>
<th><p>名稱</p></th>
<th><p>导演</p></th>
<th><p>发行商</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2017年1月28日</p></td>
<td><p><br />
(奇迹 ～那时的普通人～)</p></td>
<td></td>
<td><p><a href="../Page/东映.md" title="wikilink">东映</a></p></td>
</tr>
<tr class="even">
<td><p>2019年1月25日</p></td>
<td><p><br />
(爱歌～约定的承诺～)</p></td>
<td></td>
<td><p><a href="../Page/东映.md" title="wikilink">东映</a></p></td>
</tr>
</tbody>
</table>

### 遊戲

<table>
<thead>
<tr class="header">
<th><p>發售日期</p></th>
<th><p>對應機種</p></th>
<th><p>名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2010年4月1日</p></td>
<td><p><a href="../Page/NDS.md" title="wikilink">NDS</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 書籍

<table>
<thead>
<tr class="header">
<th><p>出版日期</p></th>
<th><p>名稱</p></th>
<th><p>体裁</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2016年3月11日</p></td>
<td></td>
<td><p>自传体小说</p></td>
</tr>
<tr class="even">
<td><p>2017年1月6日</p></td>
<td></td>
<td><p>小说</p></td>
</tr>
<tr class="odd">
<td><p>2018年12月7日</p></td>
<td></td>
<td><p>小说</p></td>
</tr>
</tbody>
</table>

### 提供歌曲

  - [NEWS](../Page/NEWS.md "wikilink")《weeeek》（2007年11月7日）
  - [Hi-Fi CAMP](../Page/Hi-Fi_CAMP.md "wikilink")《キズナ》（2008年6月4日）
  - [グッキー](../Page/Becky.md "wikilink")《GOOD LUCKY\!\!\!\!\!》(2011月3月2日)
  - [Becky](../Page/Becky.md "wikilink")(ベッキー♪\#)《風のしらべ》（2011年6月15日）
  - [AAA](../Page/AAA_\(團體\).md "wikilink")《[虹](../Page/虹_\(AAA單曲\).md "wikilink")》（2012年10月31日）
  - [whiteeeen](../Page/whiteeeen.md "wikilink")《ポケット》（2015年8月5日）
  - [whiteeeen](../Page/whiteeeen.md "wikilink")《ゼロ恋》（2016年12月14日）
  - [NEWS](../Page/NEWS.md "wikilink")《U R not alone》（2017年3月22日）
  - [whiteeeen](../Page/whiteeeen.md "wikilink")《テトテ with
    GReeeeN》（2017年5月17日）
  - [冰川清志](../Page/冰川清志.md "wikilink")(氷川きよし)《碧し》（2017年11月21日）
  - [柚子](../Page/柚子_\(組合\).md "wikilink")(ゆず)《イコール》（2018年4月4日）
  - [MISIA](../Page/MISIA.md "wikilink")《アイノカタチ
    feat.HIDE(GReeeeN)》（2018年8月22日）
  - [みゆはん](../Page/みゆはん.md "wikilink")《ストラト》（2019年3月20日）
  - [桃色幸運草Z](../Page/桃色幸運草Z.md "wikilink")(ももいろクローバーZ)《背番号》

## 電影

GReeeeN的出道故事也被改編為電影《（中国大陆：奇迹，那天如此重要。
台灣：唱吧！奇蹟！）》，於2017年1月28日公映，紀念出道十周年。由[松坂桃李和](../Page/松坂桃李.md "wikilink")[菅田將暉主演](../Page/菅田將暉.md "wikilink")。

## 外部連結

  - [官方網站](http://greeeen.co.jp/)
  - [UNIVERSAL MUSIC](http://www.universal-music.co.jp/greeeen/)
  - [UNDER HORSE
    RECORDS](https://web.archive.org/web/20120724123614/http://www.under-horse.com/)

[Category:日本樂團](../Category/日本樂團.md "wikilink")
[Category:日本男子演唱團體](../Category/日本男子演唱團體.md "wikilink")
[Category:日本唱片大獎專輯大獎獲獎者](../Category/日本唱片大獎專輯大獎獲獎者.md "wikilink")
[Category:Japan Hot
100年榜冠軍獲得者](../Category/Japan_Hot_100年榜冠軍獲得者.md "wikilink")
[Category:Oricon卡拉OK年榜冠軍獲得者](../Category/Oricon卡拉OK年榜冠軍獲得者.md "wikilink")
[Category:2007年成立的音樂團體](../Category/2007年成立的音樂團體.md "wikilink")