**扬马延**（）是一個位于[北冰洋的火山島嶼](../Page/北冰洋.md "wikilink")，是[挪威的领土](../Page/挪威.md "wikilink")。島長約，面积约

，[貝倫火山附近約](../Page/貝倫火山.md "wikilink")被[冰川所覆蓋](../Page/冰川.md "wikilink")，約佔島上約1/3的面積。气候恶劣，寒冷多雾。

岛上无常住人口，僅有約18至35名的[挪威國防軍軍人及](../Page/挪威國防軍.md "wikilink")工作人員生活在居民点[奥伦金拜恩](../Page/奥伦金拜恩.md "wikilink")。

## 氣候

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - Umbreit, Andreas (2005) *Spitsbergen : Svalbard - Franz Josef Land -
    Jan Mayen*, 3rd ed., Chalfont St. Peter : Bradt Travel Guides, ISBN
    1-84162-092-0

## 外部連結

  -
  -
  -
  -
  - [www.jan-mayen.no](http://www.jan-mayen.no/)

  - [Jan Mayen year round
    webcam](https://web.archive.org/web/20120905071004/http://www.jan-mayen.no/webcamera/webimage.jpg)

  - [Jan Mayen at Norwegian Polar
    Institute](http://www.npolar.no/en/the-arctic/jan-mayen/)

  - [TopoJanMayen](http://topojanmayen.npolar.no/) – Interactive map of
    Jan Mayen by the [Norwegian Polar
    Institute](../Page/Norwegian_Polar_Institute.md "wikilink")

  - [Photographs and information on Jan
    Mayen](https://web.archive.org/web/20151031225027/http://home.online.no/~vteigen/Piclink.html)

  - [Satellite Radar image of Jan
    Mayen](https://web.archive.org/web/20151031225028/http://home.online.no/~vteigen/satt.html)

  - [Glaciers of Jan Mayen](http://pubs.usgs.gov/pp/p1386e/janmayen.pdf)

  - [www.janmayen2011.org - a site about JX5O - international ham radio
    expedition to Jan Mayen island in 2011](http://janmayen2011.org/)

  -
  - [Weather forecasts for Jan Mayen at
    yr.no](http://www.yr.no/place/Norway/Jan_Mayen/) (Norwegian
    Meteorological institute and NR

{{-}}

[Category:揚馬延](../Category/揚馬延.md "wikilink") [Category:Ridge
volcanoes](../Category/Ridge_volcanoes.md "wikilink")
[J](../Category/挪威島嶼.md "wikilink")
[J](../Category/北冰洋島嶼.md "wikilink") [Category:Integral
overseas
territories](../Category/Integral_overseas_territories.md "wikilink")
[Category:Seabird colonies](../Category/Seabird_colonies.md "wikilink")
[Category:Important Bird Areas of Norwegian overseas
territories](../Category/Important_Bird_Areas_of_Norwegian_overseas_territories.md "wikilink")