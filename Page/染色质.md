[Chromatin_Structures.png](https://zh.wikipedia.org/wiki/File:Chromatin_Structures.png "fig:Chromatin_Structures.png")，[核小體](../Page/核小體.md "wikilink")，10nm“在一条线上的一串珠子”纖維，30nm[染色質纖維](../Page/染色質.md "wikilink")，和中间期(Metaphase)[染色體](../Page/染色體.md "wikilink").\]\]

**染色質**（，或称**核染質**）是在[細胞中發現的大分子復合物](../Page/細胞.md "wikilink")，由[DNA](../Page/DNA.md "wikilink")，[蛋白質和](../Page/蛋白質.md "wikilink")[RNA組成](../Page/RNA.md "wikilink")\[1\]。它也是構成[染色體的結構](../Page/染色體.md "wikilink")。染色質的主要功能是1）將DNA包裝成更緊湊，更緻密的形狀;
2）增強DNA大分子以允許有絲分裂; 3）防止DNA損傷; 4）控制基因表達和DNA複製。
染色質的主要蛋白質組件是緻密DNA的[組織蛋白](../Page/組織蛋白.md "wikilink")。
染色質僅在[真核](../Page/真核生物.md "wikilink")[細胞](../Page/細胞.md "wikilink")（具有確定的細胞核的細胞）中發現。
[原核細胞具有不同的DNA組織](../Page/原核生物.md "wikilink")（原核染色體等同物稱為[拟核](../Page/拟核.md "wikilink")，並且位於類核區內）。[真核](../Page/真核生物.md "wikilink")[細胞的核染質位在](../Page/細胞.md "wikilink")[細胞核內](../Page/細胞核.md "wikilink")；[原核生物的則位於](../Page/原核生物.md "wikilink")[類核](../Page/類核.md "wikilink")（nucleoid）當中\[2\]。

儘管經過深入調查，但目前對染色質的結構了解甚少。 其結構取決於幾個因素。
整體結構取決於[細胞週期的階段](../Page/細胞週期.md "wikilink")。
在[間期](../Page/間期.md "wikilink")，染色質在結構上是鬆散的，以允許獲得轉錄和復制DNA的RNA和DNA聚合酶。
間期染色質的局部結構取決於DNA上存在的基因。

[chromatin_chromosome.png](https://zh.wikipedia.org/wiki/File:chromatin_chromosome.png "fig:chromatin_chromosome.png")
avec <font color="#FF0000">**centromère**</font>. (4) Chromatine
condensée au cours de la [prophase](../Page/prophase.md "wikilink").
(Deux copies de molécules d'ADN sont présentes) (5) au cours de la
[métaphase](../Page/métaphase.md "wikilink")。\]\]

## 参考文献

<div class="references-small">

<references />

</div>

## 參見

  - [真染色質](../Page/真染色質.md "wikilink")
  - [異染色質](../Page/異染色質.md "wikilink")

{{-}}

[Category:核亚结构](../Category/核亚结构.md "wikilink")
[Category:分子遺傳學](../Category/分子遺傳學.md "wikilink")

1.
2.