**彭華英**（），台灣[社會運動者](../Page/社會運動.md "wikilink")，也是[台灣文化協會的重要參與者](../Page/台灣文化協會.md "wikilink")。其父親為[新竹](../Page/新竹縣_\(清朝\).md "wikilink")[客家人](../Page/客家人.md "wikilink")，移居國姓，所以彭華英出生於[清國](../Page/清國.md "wikilink")[福建臺灣省](../Page/福建臺灣省.md "wikilink")[臺灣道](../Page/臺灣道.md "wikilink")[臺灣府](../Page/臺灣府.md "wikilink")[埔里社廳](../Page/埔里社廳.md "wikilink")[北港溪堡](../Page/北港溪堡.md "wikilink")（今[南投縣](../Page/南投縣.md "wikilink")[國姓鄉](../Page/國姓鄉.md "wikilink")）。

彭華英於1918年進入[日本](../Page/日本.md "wikilink")[明治大學經濟科就讀](../Page/明治大學.md "wikilink")，並參與[台灣青年會活動](../Page/台灣青年會.md "wikilink")；1921年，彭華英出任台灣青年會長，並組成[啟發會](../Page/啟發會.md "wikilink")、[新民會](../Page/新民會.md "wikilink")，甚至參與與[日本共產黨有密切關連的](../Page/日本共產黨.md "wikilink")[曉民會](../Page/曉民會.md "wikilink")。由於彭華英在刊物《[臺灣青年](../Page/臺灣青年.md "wikilink")》上撰述社會主義概說一文中，包含[社會主義與反日傾向](../Page/社會主義.md "wikilink")，導致彭華英被日本警察列入注意對象，彭華英也出走[中國](../Page/中國.md "wikilink")[上海](../Page/上海.md "wikilink")，並在上海組成[上海台灣青年會](../Page/上海台灣青年會.md "wikilink")。

1924年彭華英回到台灣，依然投入政治活動與社會運動；彭華英曾經參與台灣文化協會與[台灣民眾黨](../Page/台灣民眾黨.md "wikilink")，但由於與[蔣渭水不合](../Page/蔣渭水.md "wikilink")，彭華英便退出台灣民眾黨。1932年[滿洲國成立之後](../Page/滿洲國.md "wikilink")，彭華英前往滿洲國考察，並且出任[滿洲電信電話公司社長秘書](../Page/滿洲電信電話公司.md "wikilink")。

1945年彭華英回到台灣，並出任[台灣省政府](../Page/台灣省政府.md "wikilink")[民政廳長](../Page/民政廳.md "wikilink")[楊肇嘉的秘書](../Page/楊肇嘉.md "wikilink")；1965年退休，1968年因[腦溢血過世](../Page/腦溢血.md "wikilink")。彭華英的第一任妻子為[蔡阿信](../Page/蔡阿信.md "wikilink")，是台灣第一位女[醫師](../Page/醫師.md "wikilink")，兩人在1924年結婚，但於彭華英赴[中國](../Page/中國.md "wikilink")，之後在[滿洲娶](../Page/滿洲地區.md "wikilink")[京劇花旦](../Page/京劇.md "wikilink")[梁惠燕為妻](../Page/梁惠燕.md "wikilink")，遂與[蔡阿信離婚](../Page/蔡阿信.md "wikilink")。而彭華英的曾孫[彭百顯](../Page/彭百顯.md "wikilink")，則曾出任[立法委員與](../Page/立法委員.md "wikilink")[南投縣縣長](../Page/南投縣縣長.md "wikilink")。

## 文學及影劇中的形象

[東方白的](../Page/東方白.md "wikilink")[大河小說](../Page/大河小說.md "wikilink")《[浪淘沙](../Page/浪淘沙_\(小說\).md "wikilink")》，書中的「彭英」的原型就是源自彭華英。而[民視亦於](../Page/民視.md "wikilink")2005年將《浪淘沙》改編製作成由[霍正奇主演](../Page/霍正奇.md "wikilink")「彭英」的週日十點檔《[浪淘沙](../Page/浪淘沙_\(電視劇\).md "wikilink")》。

## 參考資料

  - [臺灣人物小傳─彭華英](http://memory.ncl.edu.tw/tm_cgi/hypage.cgi?HYPAGE=toolbox_figure_detail.hpg&project_id=twpeop&dtd_id=15&subject_name=%E8%87%BA%E7%81%A3%E4%BA%BA%E7%89%A9%E8%AA%8C%281895-1945%29&subject_url=toolbox_figure.hpg&xml_id=0000295769&who=%E5%BD%AD%E8%8F%AF%E8%8B%B1)
  - [臺灣大百科全書─彭華英](http://nrch.culture.tw/twpedia.aspx?id=5708)

[Category:中華民國地方政府人物](../Category/中華民國地方政府人物.md "wikilink")
[Category:臺灣日治時期社會運動者](../Category/臺灣日治時期社會運動者.md "wikilink")
[Category:明治大學校友](../Category/明治大學校友.md "wikilink")
[Category:滿洲國台灣人](../Category/滿洲國台灣人.md "wikilink")
[Category:客家裔臺灣人](../Category/客家裔臺灣人.md "wikilink")
[Category:國姓人](../Category/國姓人.md "wikilink")
[Hua華](../Category/彭姓.md "wikilink")
[Category:罹患腦溢血逝世者](../Category/罹患腦溢血逝世者.md "wikilink")