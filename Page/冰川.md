\-{H|山岳}-
[Baffin_Island_Northeast_Coast_1997-08-07.jpg](https://zh.wikipedia.org/wiki/File:Baffin_Island_Northeast_Coast_1997-08-07.jpg "fig:Baffin_Island_Northeast_Coast_1997-08-07.jpg")[冰河出海口](../Page/冰河.md "wikilink")\]\]
[Eyjafjallajökull.jpeg](https://zh.wikipedia.org/wiki/File:Eyjafjallajökull.jpeg "fig:Eyjafjallajökull.jpeg")[冰島的冰川](../Page/冰島.md "wikilink")\]\]
**冰河**，或稱**-{zh-cn:冰河; zh-hk:冰河;
zh-tw:冰川}-**是指大量[冰塊堆積形成如同](../Page/冰塊.md "wikilink")[河川般的地理景觀](../Page/河川.md "wikilink")。是一巨大的流動固體，是在高寒地區由層層積雪堆疊而成的巨大冰川冰。在終年冰封的高山或[兩極地區](../Page/兩極地區.md "wikilink")，多年的積雪經[重力或冰河之間的壓力](../Page/重力.md "wikilink")，沿斜坡向下滑形成冰川。受重力作用而移動的冰河稱為山岳冰河或谷冰河，而受冰河之間的壓力作用而移動的則稱為大陸冰河或冰帽。兩極地區的冰川又名[大陸冰川](../Page/大陸冰川.md "wikilink")，覆蓋範圍較廣，是[冰河時期遺留下來的](../Page/冰河時期.md "wikilink")。冰川是[地球上最大的](../Page/地球.md "wikilink")[淡水資源](../Page/淡水.md "wikilink")，也是地球上繼[海洋以後最大的天然](../Page/海洋.md "wikilink")[水庫](../Page/水庫.md "wikilink")。[七大洲都有冰川](../Page/七大洲.md "wikilink")。(大洋洲的冰川都在島嶼上，不在澳洲大陸上。)

由于冰川形成于长年封冻地区，所以对冰川的研究，可以帮我们找到远古时代的[地质信息](../Page/地质.md "wikilink")。由於[溫室效應在](../Page/溫室效應.md "wikilink")[高緯度地區和](../Page/高緯度地區.md "wikilink")[高海拔地區格外明顯](../Page/高海拔地區.md "wikilink")，地球上的冰川正以驚人的速度消失。對於直接流入大海的冰川來說，這意味着巨型[冰山的增多](../Page/冰山.md "wikilink")、[海平面的上升以及](../Page/海平面.md "wikilink")[沿海地區可能遭受到的](../Page/沿海地區.md "wikilink")[氾濫](../Page/氾濫.md "wikilink")；對於高山上的冰川來說，這意味着山腳下河流[水流量的不穩定](../Page/水流量.md "wikilink")，即在大量[融雪時造成](../Page/融雪.md "wikilink")[水災](../Page/水災.md "wikilink")、其餘時間则造成[旱災](../Page/旱災.md "wikilink")。

冰川前進時會切割[山谷两侧的](../Page/山谷.md "wikilink")[岩石](../Page/岩石.md "wikilink")，將它們帶往[下游非常遠的地方](../Page/下游.md "wikilink")。在冰河退縮時，這些巨大的石块就被留在原來冰河的[河道上](../Page/河道.md "wikilink")，包括兩旁山坡上。冰河流經的山谷會由原來的V字型[橫切面变成U字型橫切面](../Page/橫切面.md "wikilink")，千萬年期間其粗糙的山谷岩層表面更能給冰川移動時磨擦至平滑。

## 分布

| 地区   | 冰川面积（KM<sup>2</sup>） |
| ---- | -------------------- |
| 南极洲  | 13 980 000           |
| 格陵兰岛 | 1 802 400            |
| 北极岛屿 | 226 090              |
| 欧洲   | 21 415               |
| 亚洲   | 109 085              |
| 北美洲  | 67 522               |
| 南美洲  | 25 500               |

**世界冰川数量与分布**

[155_-_Glacier_Perito_Moreno_-_Panorama_de_la_partie_nord_-_Janvier_2010.jpg](https://zh.wikipedia.org/wiki/File:155_-_Glacier_Perito_Moreno_-_Panorama_de_la_partie_nord_-_Janvier_2010.jpg "fig:155_-_Glacier_Perito_Moreno_-_Panorama_de_la_partie_nord_-_Janvier_2010.jpg").\]\]
總面積約達1622,7500平方公里，即覆蓋了地球[陸地面積的](../Page/陸地.md "wikilink")11％，約佔地球上淡水總量的69％。現代冰川面積的97％、冰量的99％為南極大陸和格陵蘭兩大冰蓋所佔有，特別是[南極大陸冰蓋面積達到](../Page/南極.md "wikilink")1398萬平方公里（包括冰架），最大冰厚度超過4000米，冰從冰蓋中央向四周流動，最後流到海洋中崩解。

## 对于冰川的认识

[Glacial_scratch_-_guizhou.jpg](https://zh.wikipedia.org/wiki/File:Glacial_scratch_-_guizhou.jpg "fig:Glacial_scratch_-_guizhou.jpg")
人类很早就對于冰川有所认识。[中国](../Page/中國.md "wikilink")[唐朝的](../Page/唐朝.md "wikilink")[玄奘师徒西行時曾把](../Page/玄奘.md "wikilink")[天山](../Page/天山.md "wikilink")[木札尔特冰川描写为](../Page/木札尔特冰川.md "wikilink")：「冰雪所聚，积而为凌，春夏不解……」大意就是说冰雪堆积形成了冰凌，不論春夏都不融化。[欧洲的阿尔卑斯山是现代冰川的研究的起源地](../Page/欧洲.md "wikilink")。人类首次系统研究阿尔卑斯山的冰川19世纪30、40年代[路易士·阿格西建立世界上第一个冰川研究站开始](../Page/路易士·阿格西.md "wikilink")，奠定冰川学的基础。1911年[J·P·科赫和](../Page/J·P·科赫.md "wikilink")[韦格纳开创了对大陆冰盖的研究](../Page/韦格纳.md "wikilink")。20世纪50年代以来几次大规模的国际合作计划，70年代以来[氧](../Page/氧.md "wikilink")[同位素](../Page/同位素.md "wikilink")、[雷达测量](../Page/雷达.md "wikilink")、[卫星遥感和遥测技术的应用](../Page/卫星.md "wikilink")，都有效促进对冰川的认识和研究。\[1\]

## 冰川的分類

冰川依照在地球上分布的位置分为兩種，之所以分類是因為許多地理學者認為，這兩類的冰川地形其作用力、塑造的地形、成因皆有顯著差別。儘管習慣上冰河、-{zh-tw:冰川;
zh-hk:冰河;zh-hans:冰川}-為同義詞，但是以下兩種分類慣用上並沒有冰川的同義詞，也就是說山岳冰河、大陸冰河相比於山岳-{zh-tw:冰川;
zh-hk:冰河;zh-hans:冰川}-、大陸-{zh-tw:冰川;
zh-hk:冰河;zh-hans:冰川}-而言，是較罕用或是[地理研究上並不使用的專有](../Page/地理.md "wikilink")[名詞](../Page/名詞.md "wikilink")。

### 山岳冰川

[Receding_glacier-en.svg](https://zh.wikipedia.org/wiki/File:Receding_glacier-en.svg "fig:Receding_glacier-en.svg")
发育在高山上的冰川，呈舌形，在重力作用下运动。如[中国](../Page/中国.md "wikilink")[青藏高原上的冰川](../Page/青藏高原.md "wikilink")，是世界最大的山岳冰川。範圍通常較大陸冰河小。按其形态、发育和地形特点的差别，山岳冰川又可以分为以下几种：

  - 山谷冰川
  - 悬冰川
  - 冰斗冰川
  - 平顶冰川
  - 山麓冰川

### 大陸冰川

是冰川中最大的一种，呈盾形，这种冰川覆盖着庞大的地面，在许多情况下，并且其厚度足以把全部山系--除最高的山峰外--都埋起来。面积超过14,000,000平方公里的[南极洲](../Page/南极洲.md "wikilink")，差不多全部都被一个平均接近1,980[米厚的冰川覆盖着](../Page/米.md "wikilink")，其东部冰层厚度可达4267米。格陵兰冰盖覆盖的面积超过1,800,000平方公里，实测最大厚度约3,350米。较小的大陆冰盖常被称作冰帽或冰原。地球上有两大冰盖，即南极冰盖和格陵兰冰盖，它们占世界冰川总体积的99％，其中南极冰盖占90％。格陵兰约有83％的面积为冰川覆盖。

## 冰川構造

### 冰磧物

[Cavell_Glacier_with_Crevices_and_Annual_Rings.jpg](https://zh.wikipedia.org/wiki/File:Cavell_Glacier_with_Crevices_and_Annual_Rings.jpg "fig:Cavell_Glacier_with_Crevices_and_Annual_Rings.jpg")和[年輪](../Page/年輪.md "wikilink")\]\]
冰川產生多種岩屑稱為冰積物。冰水冰積物是由稱為冰川融水的融冰中[沉積下來的岩屑](../Page/沉積.md "wikilink")。有些冰積物含石塊和巨礫類似扁礫。冰積物也可能由冰川融水混入稱為冰礫泥的细礫沉積物。**堆積冰磧土**是融化時冰川頂部落下的岩屑。

## 参考文献

### 引用

### 来源

  - [绿色和平：全球变暖，北极冰川消融加速](https://web.archive.org/web/20100414075235/http://www.greenpeace.org/china/zh/news/world-park-arctic)
  - 《世界水量平衡和全球水资源》（1978）

## 外部链接

## 参见

  - [冰川学](../Page/冰川学.md "wikilink")
  - [中国冰川](../Page/中国冰川.md "wikilink")
  - [冰川湖](../Page/冰川湖.md "wikilink") -
    [冰川湖溢出](../Page/冰川湖溢出.md "wikilink")
  - [雪线](../Page/雪线.md "wikilink")

{{-}}

[Category:地形](../Category/地形.md "wikilink")
[冰川](../Category/冰川.md "wikilink")
[Category:冰川学](../Category/冰川学.md "wikilink")
[Category:山地生態學](../Category/山地生態學.md "wikilink")

1.  [智慧藏百科全書網](http://www.wordpedia.com)