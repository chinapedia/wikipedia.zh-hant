[Windsurfing_sport_sculpture,_Cheung_Chau_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Windsurfing_sport_sculpture,_Cheung_Chau_\(Hong_Kong\).jpg "fig:Windsurfing_sport_sculpture,_Cheung_Chau_(Hong_Kong).jpg")[東灣的一座風帆雕塑](../Page/東灣_\(長洲\).md "wikilink")。1996年9月7日揭幕，李麗珊為主持揭幕嘉賓之一。（雕塑為1986年[香港十大傑出青年蔡啟仁的作品](../Page/香港十大傑出青年.md "wikilink")）\]\]
**李麗珊**（），暱稱**珊珊**\[1\]，生於[香港](../Page/香港.md "wikilink")，為[長洲](../Page/長洲_\(香港\).md "wikilink")[原居民](../Page/原居民.md "wikilink")，香港著名[滑浪風帆](../Page/滑浪風帆.md "wikilink")[運動員](../Page/運動員.md "wikilink")\[2\]。她為香港取得歷史性首面[奧運會獎牌兼首面](../Page/奧運會.md "wikilink")[金牌](../Page/金牌.md "wikilink")（[滑浪風帆](../Page/滑浪風帆.md "wikilink")），因而被香港傳媒冠以「**風之-{后}-**」的美譽。她早年就讀[長洲官立中學](../Page/長洲官立中學.md "wikilink")。

[The_2009_EAG_Gallery_09.JPG](https://zh.wikipedia.org/wiki/File:The_2009_EAG_Gallery_09.JPG "fig:The_2009_EAG_Gallery_09.JPG")穿著的背心\]\]

李麗珊自幼在舅父[黎根的指導下練習滑浪風帆](../Page/黎根.md "wikilink")，17歲開始參加比賽，19歲（1989年）成為香港代表隊成員，教練是艾培理。

她多年來獲得數個國際比賽獎項，曾在[1996年阿特蘭大奧運會奪得香港歷史上首面奧運會金牌](../Page/1996年夏季奥林匹克运动会.md "wikilink")，消息轟動香港，當時她居住的島嶼[長洲舉行萬人祝捷會](../Page/長洲_\(香港\).md "wikilink")，全港所有電視台從她落機抵港一直全程直播，這面金牌曾一度在香港牽起學習滑浪風帆熱潮，她奪獎後激動地向記者說：「香港運動員唔係垃圾」（香港運動員不是垃圾），成為傳頌一時的名句。事緣李麗珊在1990年代一次代表香港參加歐洲滑浪風帆錦標賽時，本來大會想讓出多一點非歐洲人士席位予不同人士參加，但後來報到者超過限額60人；引來一些歐洲參賽者對一眾香港代表的微言，直指香港運動員是垃圾，為何還要搶奪參賽資格，直至此次奪獎才一吐烏氣\[3\]。

李麗珊在體育事業的傑出成就，使她獲得不少榮譽，包括於1997年獲[伊利沙伯二世女王頒授](../Page/伊利沙伯二世.md "wikilink")[員佐勳章](../Page/MBE.md "wikilink")、2003年獲[香港行政長官](../Page/香港行政長官.md "wikilink")[董建華頒發](../Page/董建華.md "wikilink")[銅紫荊星章](../Page/銅紫荊星章.md "wikilink")，並多次獲選為[香港傑出運動員](../Page/香港傑出運動員.md "wikilink")。此外，她更在1996年，即為香港奪得首面奧運金牌的一年，獲[香港地鐵公司許可終身免費乘搭地鐵車程](../Page/香港地鐵.md "wikilink")；同時因應李麗珊為香港取得史上首面[奧運金牌的突破性成就](../Page/奧運.md "wikilink")，而隨後舉行的[殘奧亦有兩名運動員為香港取得金牌](../Page/殘奧.md "wikilink")，[香港地鐵遂與](../Page/香港地鐵.md "wikilink")[港協暨奧委會達成協議](../Page/港協暨奧委會.md "wikilink")，於1996年12月16日將原稱「大角咀站」的站點命名為「[奧運站](../Page/奧運站.md "wikilink")」，以表揚香港運動員的輝煌成就。

## 個人生活

1999年1月26日，她與香港同為香港著名風帆運動員的[黃德森結婚](../Page/黃德森.md "wikilink")，被視為模範夫婦，2005年8月28日誕下女兒黃希皚\[4\]。李麗珊亦在[2004年雅典奧運後暫時退出運動員生涯](../Page/2004年雅典奧運.md "wikilink")，現為全職[家庭主婦及母親](../Page/家庭主婦.md "wikilink")，只作簡單訓練。李麗珊於2007年8月12日誕下次女黃嘉怡，所以未能參加2008年的[北京奧運會](../Page/北京奧運會.md "wikilink")，不過仍會到北京出席。\[5\]

2002年，她到[澳洲](../Page/澳洲.md "wikilink")[坎培拉大學攻讀體育管理專業](../Page/坎培拉大學.md "wikilink")，其後[香港中文大學授予榮譽社會科學博士學位](../Page/香港中文大學.md "wikilink")，成為首位獲得這項榮譽的香港運動員。

2007年11月16日，[亞洲電視公布](../Page/亞洲電視.md "wikilink")，李麗珊將於2008年北京奧運期間擔任亞視奧運大使，負責報道開幕及閉幕儀式，及駐青島報道風帆賽事。\[6\]\[7\]

2008年5月2日，李麗珊於[2008年奧運會香港區火炬接力中擔任第一棒火炬手](../Page/2008年奧運會香港區火炬接力.md "wikilink")，從特區行政長官[曾蔭權手中接過奧運聖火](../Page/曾蔭權.md "wikilink")。李麗珊亦於2008年8月9日作為奧帆賽火炬最後一棒的火炬手，當中更手持火炬駕馭風帆飛翔。\[8\]

2008年7月，李麗珊著作[《逆風而上 —
李麗珊》ISBN︰9789626785355](http://www.etpress.com.hk/etpress/bookdetail.do?id=9789626785355)推出。

1990年 北京亞運會 - 銀牌 1992年 巴塞隆納奧運會 - 第11名 1993年 世界錦標賽 - 冠軍 1994年 廣島亞運會 - 銀牌
1995年 世界錦標賽 - 季軍 1996年 世界錦標賽 - 亞軍 1996年 亞特蘭大奧運會 - 金牌 1997年 世界錦標賽 - 冠軍
1998年 曼谷亞運會 - 金牌 2000年 雪梨奧運會 - 第6名 2001年 世界錦標賽 - 冠軍 2001年 廣州第九屆全運會 - 銀牌
2002年 釜山亞運會 - 金牌 2004年 雅典奧運會 - 第4名

## 榮譽

  - **大英帝國勳章**

<!-- end list -->

  -
    [員佐勳章](../Page/員佐勳章.md "wikilink")（MBE）（1996年）

<!-- end list -->

  - '''香港特區政府嘉

## 流行文化

李麗珊獲得奧運金牌以後，成為香港大眾的偶像。例如：本地動畫《[麥兜故事](../Page/麥兜故事.md "wikilink")》，更將李麗珊與黎根作為當中角色，讓觀眾學習其發奮向上的精神。

## 資料

  - [「-{風之后}-」李麗珊
    備戰財務試學理財](http://www.mpfinance.com/htm/Finance/20060904/Invest/mh_mht1.htm)
  - [勝在有你
    李麗珊](https://web.archive.org/web/20111208002535/http://www.jessicahk.com/common/index_1.htm?topic_id=0803&ref=19339&head=)
  - [李麗珊一舉成名天下知](https://web.archive.org/web/20070929205511/http://www.world-talent.org/sport/sport5.htm)

## 注释

<references/>

[category:長洲 (香港)](../Page/category:長洲_\(香港\).md "wikilink")

[Category:香港滑浪風帆運動員](../Category/香港滑浪風帆運動員.md "wikilink")
[Category:香港傑出運動員](../Category/香港傑出運動員.md "wikilink")
[Category:香港中文大學榮譽博士](../Category/香港中文大學榮譽博士.md "wikilink")
[Category:香港新界原居民](../Category/香港新界原居民.md "wikilink")
[L](../Category/MBE勳銜.md "wikilink")
[Category:香港十大傑出青年](../Category/香港十大傑出青年.md "wikilink")
[L](../Category/李姓.md "wikilink") [Lee Lai
Shan](../Category/香港奧林匹克運動會金牌得主.md "wikilink")
[Category:1990年亞洲運動會銀牌得主](../Category/1990年亞洲運動會銀牌得主.md "wikilink")
[Category:1994年亞洲運動會銀牌得主](../Category/1994年亞洲運動會銀牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1998年亞洲運動會金牌得主](../Category/1998年亞洲運動會金牌得主.md "wikilink")
[Category:2002年亞洲運動會金牌得主](../Category/2002年亞洲運動會金牌得主.md "wikilink")
[Category:奧林匹克運動會帆船獎牌得主](../Category/奧林匹克運動會帆船獎牌得主.md "wikilink")

1.  珊珊：[粵語讀作](../Page/粵語.md "wikilink")［saan1 saan4］，「山潺」］。
2.  [多哈亞運會關於香港　的官方介紹](http://www.doha-2006.com/gis/menuroot/countries/country/HKG.aspx)
3.  [奪奧運金牌替港運動員出口氣
    回歸10年百人誌：李麗珊甘為子女放棄風帆](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070519&sec_id=4104&subsec_id=11867&art_id=7119324)
    ，《[蘋果日報](../Page/蘋果日報.md "wikilink")》，2007年5月19日
4.  [李麗珊誔下女兒
    香港電台新聞](http://www.rthk.org.hk/rthk/news/expressnews/20050828/20050828_55_250563.html)
5.  [李麗珊凸肚剪綵賺奶粉錢](http://www.singtao.com/index_archive.asp?d_str=20070319&htmlpage=main&news=0319fo03.html)，《[星島日報](../Page/星島日報.md "wikilink")》，2007年3月19日
6.  [李麗珊
    任亞視奧運主持](http://hk.news.yahoo.com/071116/12/2jnpr.html)明報，2007年11月17日
7.  [李麗珊亞視揚帆講奧運](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20071117&sec_id=462&subsec_id=830&art_id=10429670)
    蘋果日報，2007年11月17日
8.  [李麗珊青島飛帆點燃聖火](http://hk.video.yahoo.com/video/video.html?id=1028316)Yahoo\!
    Video