**enterbrain**（），現是[角川集團製作經手](../Page/角川集團.md "wikilink")[娛樂關連](../Page/娛樂.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")、[書籍的](../Page/書籍.md "wikilink")[品牌](../Page/品牌.md "wikilink")。之前隸屬於[角川控股集團中間持股公司](../Page/角川控股集團.md "wikilink")[MediaLeaves的旗下](../Page/MediaLeaves.md "wikilink")。

本條目包括對法人消滅前的**株式會社enterbrain**（）的介紹。

## 概要

  - 前身主體為[西和彥所設立的](../Page/西和彥.md "wikilink")[ASCII公司其中一個部門的娛樂系編輯部之第二編輯統籌本部](../Page/ASCII_\(公司\).md "wikilink")。
  - 公司名稱寓含有「要成為娛樂業（entertainment）[首腦](../Page/:wikt:首腦.md "wikilink")（brain）」的意思。企業標誌為紅字的**<font size=5 color=red>*e<sub>b</sub>\!*</font>**標記。

## 公司沿革

  - 1987年1月30日－成立ASCII映画株式會社。
  - 1997年－商號變更為株式會社ASCII Visual Entertainment。
  - 2000年4月1日－自株式會社[ASCII](../Page/ASCII_\(公司\).md "wikilink")（之後改組為株式會社[MediaLeaves](../Page/MediaLeaves.md "wikilink")），將[Fami通等娛樂系的編輯部](../Page/Fami通.md "wikilink")，以及營業部門、事務部門讓渡給ASCII
    Visual Entertainment，商號改為**株式会社enterbrain**。
  - 2004年3月18日－連同母公司MediaLeaves被[角川控股收購](../Page/角川控股集團.md "wikilink")。
  - 2005年3月－將本部自[世田谷區若林遷移至](../Page/世田谷區.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")[三番町](../Page/番町.md "wikilink")（本部大樓位於[桂宮官邸的隔壁](../Page/桂宮宜仁親王.md "wikilink")）。
  - 2010年10月1日－MediaLeaves併入。
  - 2011年3月1日－併入。
  - 2013年10月1日－併入「株式会社KADOKAWA」成為事業品牌，公司法人消滅。

## 主要雜誌與書籍

  - [LOGiN](../Page/LOGiN.md "wikilink")（）-
    [FC遊戲機登場以前就以](../Page/FC遊戲機.md "wikilink")[電腦遊戲為中心介紹的](../Page/電腦遊戲.md "wikilink")[遊戲雜誌](../Page/遊戲雜誌.md "wikilink")。由該雜誌專欄所誕生的遊戲雜誌也不少。現在專門介紹[成人遊戲以外的電腦遊戲](../Page/成人遊戲.md "wikilink")。
      - [B's LOG](../Page/B's_LOG.md "wikilink")（）-
        從『LOGiN』一個專欄裡獨立出來，[BL遊戲的専門誌](../Page/BL遊戲.md "wikilink")。
  - [Fami通](../Page/Fami通.md "wikilink")（）-
    具有高知名度的綜合遊戲雜誌。前身也是『LOGiN』的一介專欄。
      - [Fami通文庫](../Page/Fami通文庫.md "wikilink")（）
  - [TECH Win DVD](../Page/TECH_Win_DVD.md "wikilink")（）-
    附DVD-ROM的電腦遊戲雜誌。在enterbrain營運後仍留在ASCII，於2002年5月號轉移到enterbrain。
      - [TECH GIAN](../Page/TECH_GIAN.md "wikilink")（）-
        和『E-LOGiN』是不同系統的美少女電腦遊戲雜誌。
  - [Magi-Cu](../Page/Magi-Cu.md "wikilink")（）-
    集合[美少女角色的視覺系娛樂雜誌](../Page/美少女.md "wikilink")。其實是現在該社的玩具箱。
  - [Comic Beam](../Page/Comic_Beam.md "wikilink")（）- 創刊時的名稱是『ASCII
    Comic』（）。特色是刊載充滿漫畫家個性的濃郁作品。
  - [ARCADIA](../Page/ARCADIA.md "wikilink")（）-
    過去[新聲社所發行的遊戲雜誌](../Page/新聲社.md "wikilink")『[GAMEST](../Page/GAMEST.md "wikilink")』（）編輯小組，在新聲社破產時加入當時的ASCII，經過編輯交替等事情後發行的業務用遊戲專門雜誌。雖然過去被視為[雜誌書](../Page/雜誌書.md "wikilink")，但後來成為月刊。繼承了在『GAMEST』舉行過的高得分統計等活動。此外在本雜誌月刊化以前，曾在『Fami通』一時連載專欄『週刊ARCADIA』。
  - [サラブレ](../Page/サラブレ.md "wikilink") -
    [賽馬雜誌](../Page/賽馬.md "wikilink")，於賽馬全盛時期創刊。雖然該時期各出版社發行了多數雜誌後接踵而來的廢刊，不過該雜誌以獨到的企畫與『[德貝賽馬](../Page/德貝賽馬.md "wikilink")』（）特集等，現在成為僅次於『[優駿](../Page/優駿.md "wikilink")』的賽馬雜誌。
  - [Fami通Connect\!On](../Page/Fami通Connect!On.md "wikilink")（）-
    線上遊戲專門雜誌。適合家用線上遊戲機的軟體。2006年7月21日創刊。
  - [kamipro](../Page/kamipro.md "wikilink") -
    [摔角](../Page/摔角.md "wikilink")、[格鬥技的專門雜誌](../Page/格鬥技.md "wikilink")，以前是[Wanimagazine社發行的雜誌](../Page/Wanimagazine社.md "wikilink")『紙上職業摔角RADICAL』（）。2006年enterbrain接手，變更為現在的雜誌。

## 主要遊戲軟體

※主要發售的是自舊ASCII時代以來的作品。

  - [RPG製作大師系列](../Page/RPG製作大師.md "wikilink")
  - [德貝賽馬系列](../Page/德貝賽馬.md "wikilink")

<!-- end list -->

  -
    於FC遊戲機全盛時代末期誕生的名作。第1作發售當時的開發部門在1996年成立新公司[ParityBit獨立](../Page/ParityBit.md "wikilink")。

<!-- end list -->

  - [ベストプレープロ野球系列](../Page/ベストプレープロ野球.md "wikilink")

<!-- end list -->

  -
    和《德貝賽馬》並列為ParityBit的代表作。德貝賽馬、ベストプレープロ野球在2002年以後由eb\!發售。

<!-- end list -->

  - [ティアリングサーガ ユトナ英雄戦記](../Page/ティアリングサーガ_ユトナ英雄戦記.md "wikilink")

<!-- end list -->

  -
    [任天堂](../Page/任天堂.md "wikilink")《[火焰之紋章](../Page/火焰之紋章.md "wikilink")》開發小組的一部份獨立創設的公司[Tilnanog](../Page/Tilnanog.md "wikilink")（）所開發。任天堂主張其為《火焰之紋章》類似作，而引發[著作權等相關的法律訴訟](../Page/著作權.md "wikilink")。判決裡關於任天堂主張的著作權方面雖然完全未獲承認，在宣傳方面上有部分問題而由enterbrain方賠償約7600萬日圓。

<!-- end list -->

  - [ベルウィックサーガ](../Page/ベルウィックサーガ.md "wikilink") - Tear Ring
    Saga開發團隊製作的Tear Ring Saga系列第2作。2005年5月發售。
  - [真愛故事系列](../Page/真愛故事.md "wikilink") -
    由[ビッツラボラトリー開發](../Page/ビッツラボラトリー.md "wikilink")。
  - [君吻](../Page/君吻.md "wikilink")（）。
  - [聖誕之吻](../Page/聖誕之吻.md "wikilink")
  - [戀曲寫真](../Page/戀曲寫真.md "wikilink")

## 雜記

  - 曾和發行美少女遊戲雜誌的[Coremagazine攜手共同開發](../Page/Coremagazine.md "wikilink")《》（人物設定：）。
  - 關於enterbrain建立之初，和同為角川集團旗下[MediaWorks業務上重複的問題](../Page/MediaWorks.md "wikilink")，當時官方說法是『Fami通』、『電擊』兩品牌短時間內不會有更動。但是在美少女遊戲雜誌方面，有開始「部分整合」。

## 關連項目

  - [角川控股集團](../Page/角川控股集團.md "wikilink")
  - [ASCII](../Page/ASCII_\(公司\).md "wikilink")
  - [MediaWorks](../Page/MediaWorks.md "wikilink")
  - [MSX](../Page/MSX.md "wikilink")
  - [AX](../Page/AX.md "wikilink")
  - [TeX](../Page/TeX.md "wikilink")
      - [LaTeX](../Page/LaTeX.md "wikilink")
      - [Publishing TeX](../Page/Publishing_TeX.md "wikilink")
  - 主要關連[節目](../Page/節目.md "wikilink")
      - [歌番](../Page/歌番.md "wikilink")
      - [ゲームナイト・ニッポン](../Page/ゲームナイト・ニッポン.md "wikilink")
      - [ゲームwave](../Page/ゲームwave.md "wikilink")
      - [ゲームBREAK](../Page/ゲームBREAK.md "wikilink")
  - 動畫
      - [格鬥小霸王](../Page/格鬥小霸王.md "wikilink")
      - [時空偵探](../Page/時空偵探.md "wikilink")
      - [瓶詰妖精](../Page/瓶詰妖精.md "wikilink")
      - [沙漠神行者](../Page/沙漠神行者.md "wikilink")
      - [英國戀物語艾瑪](../Page/艾瑪_\(漫畫\).md "wikilink")
      - [ぺとぺとさん](../Page/ぺとぺとさん.md "wikilink")
      - [吉永家的石像怪](../Page/吉永家的石像怪.md "wikilink")
      - [甜蜜偶像](../Page/甜蜜偶像.md "wikilink")
  - 動畫廣播
      -
  - 線上廣播
      - [Fia通](../Page/Fia通.md "wikilink")（）
  - 過去發行的雜誌
      - [Fami通DC](../Page/Fami通DC.md "wikilink")（）
      - [DearMy...](../Page/DearMy....md "wikilink")
      - [PALETTA](../Page/PALETTA.md "wikilink")
      - [E-LOGIN](../Page/E-LOGIN.md "wikilink")

<!-- end list -->

  - [日本美少女遊戲廣告賞](../Page/日本美少女遊戲廣告賞.md "wikilink")

<!-- end list -->

  - [Tilnanog](../Page/Tilnanog.md "wikilink")（）（開發[Tear Ring
    Saga](../Page/Tear_Ring_Saga.md "wikilink")）

<!-- end list -->

  - [濱村弘一](../Page/濱村弘一.md "wikilink") 代表取締役社長
  - [Fami通文庫](../Page/Fami通文庫.md "wikilink")
  - [Entame大獎小說部門](../Page/Entame大獎小說部門.md "wikilink")（由Enterbrain主辦的輕小說文學獎）

## 外部連結

  - [Enterbrain官方網站](http://www.enterbrain.co.jp/)

[Category:2000年建立](../Category/2000年建立.md "wikilink")
[Category:角川集團](../Category/角川集團.md "wikilink")
[ENTERBRAIN](../Category/ENTERBRAIN.md "wikilink")
[Category:日本出版社](../Category/日本出版社.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")
[Category:2013年結業公司](../Category/2013年結業公司.md "wikilink")