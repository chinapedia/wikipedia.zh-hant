在[西方哲學中](../Page/西方哲學.md "wikilink")，**古典哲學**（），又稱**古代哲学**，為[古希臘至](../Page/古希臘.md "wikilink")[古羅馬時期的哲學](../Page/古羅馬.md "wikilink")，一般包括从[前苏格拉底哲学到](../Page/前苏格拉底哲学.md "wikilink")[基督教兴起之前的时期](../Page/基督教.md "wikilink")。[智者学派兴起之后出现了](../Page/智者学派.md "wikilink")[古希腊甚至整个哲学史上最重要的三位哲学家](../Page/古希腊.md "wikilink")：[苏格拉底](../Page/苏格拉底.md "wikilink")、[柏拉图](../Page/柏拉图.md "wikilink")、[亚里士多德](../Page/亚里士多德.md "wikilink")。其中柏拉图和亚里士多德的作品比较完整地保留了下来，他们系统性的论述给后代哲学家造成了巨大影响。

亚里士多德之后由于社会和政治上的变化，代表文明的[古希腊被](../Page/古希腊.md "wikilink")[马其顿的](../Page/马其顿.md "wikilink")[亚历山大大帝所灭](../Page/亚历山大大帝.md "wikilink")，这一时期出现了另外四大学派：[犬儒学派](../Page/犬儒学派.md "wikilink")、[怀疑派](../Page/怀疑派.md "wikilink")、[伊壁鸠鲁派和](../Page/伊壁鸠鲁派.md "wikilink")[斯多葛派](../Page/斯多葛派.md "wikilink")。他们追问：

  - 在一个罪恶的世界里，人怎样才能够有德？
  - 在一个受苦受难的世界里，人怎样才能够幸福？

这一时期还包括古罗马时代的哲学家，例如[西塞罗的折中主义](../Page/西塞罗.md "wikilink")，他们渗透于同时期的不同学派间。

  - [苏格拉底](../Page/苏格拉底.md "wikilink")*Socrates* (469-399 BC)
  - [柏拉图](../Page/柏拉图.md "wikilink")*Plato* (429-347 BC)
  - [亚里士多德](../Page/亚里士多德.md "wikilink")*Aristotle* (384-322 BC)

<!-- end list -->

  - [犬儒学派](../Page/犬儒学派.md "wikilink")
      - [安提西尼](../Page/安提西尼.md "wikilink")*Antisthenes* (445-360 BC)
      - [狄奥根尼](../Page/狄奥根尼.md "wikilink")*Diogenes of Sinope* (400-325
        BC)

<!-- end list -->

  - [伊壁鸠鲁派](../Page/伊壁鸠鲁派.md "wikilink")
      - [伊壁鸠鲁](../Page/伊壁鸠鲁.md "wikilink")*Epicurus* (341-270 BC)
      - [卢克莱修](../Page/卢克莱修.md "wikilink")*Lucretius*

<!-- end list -->

  - [斯多葛派](../Page/斯多葛派.md "wikilink")
      - [西提姆的芝诺](../Page/西提姆的芝诺.md "wikilink")*Zeno of Citium* (365-263
        BC)
      - [克雷安德](../Page/克雷安德.md "wikilink")*Cleanthes* (331-232 BC)
      - [塞内卡](../Page/塞内卡.md "wikilink")*Seneca*(4 BC-65 AD)
      - [马可·奥勒留](../Page/马可·奥勒留.md "wikilink")*Marcus Aurelius*(121-280
        AD)
      - [爱比克泰德](../Page/爱比克泰德.md "wikilink")*Epictetus*(55-135)

<!-- end list -->

  - [怀疑派](../Page/怀疑派.md "wikilink")
      - [皮浪](../Page/皮浪.md "wikilink")*Pyrrho* (365-275 BC)
      - [阿塞西劳斯](../Page/阿塞西劳斯.md "wikilink")*Arcesilaus* (316-232 BC)

<!-- end list -->

  - [西塞罗](../Page/西塞罗.md "wikilink")*Cicero* (106-43 BC)

<!-- end list -->

  - [新柏拉图主义](../Page/新柏拉图主义.md "wikilink")
      - [阿摩尼阿斯·萨卡斯](../Page/阿摩尼阿斯·萨卡斯.md "wikilink")*Ammonius
        Saccas*(175-242 AD)
      - [普罗提诺](../Page/普罗提诺.md "wikilink")*Plotinus*(204-269 AD)
      - [尤利安](../Page/尤利安.md "wikilink")*Flavius Claudius
        Iulianus*(331-369 AD)
      - [希帕提娅](../Page/希帕提娅.md "wikilink")*Hypatia*(370-415 AD)

## 相關條目

  - [古希臘哲學家列表](../Page/古希臘哲學家列表.md "wikilink")

[ro:Filosofia antică
greco-romană](../Page/ro:Filosofia_antică_greco-romană.md "wikilink")

[Category:哲学史](../Category/哲学史.md "wikilink")