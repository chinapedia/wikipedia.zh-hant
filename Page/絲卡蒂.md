[Skade_by_Frölich.jpg](https://zh.wikipedia.org/wiki/File:Skade_by_Frölich.jpg "fig:Skade_by_Frölich.jpg")
**絲卡蒂**，Skadi、Skaði。是[北歐神話中的一名女巨人](../Page/北歐神話.md "wikilink")，她的父親是[夏基](../Page/夏基.md "wikilink")（Thiazi），第一任丈夫是[尼奧爾德](../Page/尼奧爾德.md "wikilink")（Njord），所以她也是[弗雷](../Page/弗雷.md "wikilink")（Freyr）、[弗蕾亞](../Page/弗蕾亞.md "wikilink")（Freya）的[繼母](../Page/繼母.md "wikilink")。和尼奧爾德分手後據說再嫁給[烏勒爾](../Page/烏勒爾.md "wikilink")（Ullr）或[奧丁](../Page/奧丁.md "wikilink")（）。她熱愛[冬天和](../Page/冬天.md "wikilink")[雪](../Page/雪.md "wikilink")，喜歡[山林和](../Page/山林.md "wikilink")[打獵](../Page/打獵.md "wikilink")，所以被視為[冬天之神](../Page/冬天.md "wikilink")，又被稱為「雪靴女神」（Öndurgud，Snowshoe
Goddess）。

絲卡蒂的父親[夏基](../Page/夏基.md "wikilink")（Thiazi）因為挾持青春女神[伊登](../Page/伊登.md "wikilink")（Idun）而被諸神殺死，憤怒的絲卡蒂穿上全副武裝來到[阿斯嘉特](../Page/阿斯嘉特.md "wikilink")（Asgard）。諸神試圖與她和解：他們答應絲卡蒂提出的要求，讓她在諸神中挑一個為丈夫，並還要讓她大笑一場。但諸神也開出條件：只能看腳來選擇丈夫。絲卡蒂本來想要選最英俊的[巴德爾](../Page/巴德爾.md "wikilink")（Balder）為夫，挑了一雙她覺得最完美的[腳](../Page/腳.md "wikilink")，認為這麼完美的腳應該就屬於巴德爾，但卻挑到了[尼奧爾德](../Page/尼奧爾德.md "wikilink")。

至於大笑一場的部份，則由[洛基](../Page/洛基.md "wikilink")（Loki）找了一頭老山羊和一條[繩子](../Page/繩子.md "wikilink")，將繩子一頭綁在[山羊身上](../Page/山羊.md "wikilink")，另一頭綁在自己的[性器官上進行](../Page/性器官.md "wikilink")[拔河比賽](../Page/拔河.md "wikilink")，這個可笑的畫面讓絲卡蒂笑了出來，終於同意和諸神和解。之後，奧丁還將[夏基的雙眼放到天空作為](../Page/夏基.md "wikilink")[星星作為和解的誠意](../Page/星星.md "wikilink")。

[尼奧爾德是出身](../Page/尼奧爾德.md "wikilink")[華納神族](../Page/華納神族.md "wikilink")（）的近海海神，住在[諾歐通](../Page/諾歐通.md "wikilink")（Noatun）。絲卡蒂住位在[約頓海姆](../Page/約頓海姆.md "wikilink")（Jothuheim）的[索列姆海姆](../Page/索列姆海姆.md "wikilink")（Thrymheim）地方，雖然兩人結了婚但彼此興趣差異頗大。最初雙方說定每九天（也有說是三天）輪流於兩地居住，但彼此都無法適應對方的環境，最後這場婚姻還是以分手作收。另外有說法認為後來絲卡蒂再嫁給[烏勒爾這個也是愛](../Page/烏勒爾.md "wikilink")[雪](../Page/雪.md "wikilink")、愛[打獵的弓箭之神](../Page/打獵.md "wikilink")，另一個說法是再嫁給[奧丁](../Page/奧丁.md "wikilink")。

基於曾和神族[結婚的這個原因](../Page/結婚.md "wikilink")，絲卡蒂有時也被視為[阿薩神族](../Page/阿薩神族.md "wikilink")（）一員。在之後，當[洛基](../Page/洛基.md "wikilink")（Loki）背叛諸神，陰謀殺死[巴德爾](../Page/巴德爾.md "wikilink")（Balder）的事情被發現，諸神於是將[洛基綁在巨](../Page/洛基.md "wikilink")[石上](../Page/石.md "wikilink")，並在他頭上放置一條滴著毒液的毒蛇，這條[毒蛇就是絲卡蒂放置的](../Page/毒蛇.md "wikilink")。

[S](../Category/北歐巨人.md "wikilink")