**斯蒂芬·杰西·杰克逊**（，），[美国职业](../Page/美国.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，场上位置[锋卫摇摆人](../Page/锋卫摇摆人.md "wikilink")。

杰克逊出生于[得克萨斯州](../Page/得克萨斯州.md "wikilink")[亚瑟港](../Page/亚瑟港_\(得克萨斯州\).md "wikilink")，[1997年NBA选秀中被](../Page/1997年NBA选秀.md "wikilink")[菲尼克斯太阳队在第二轮第](../Page/菲尼克斯太阳队.md "wikilink")43位选中，但未能与球队正式签约。其后，杰克逊浪迹天涯，先后在[澳大利亚](../Page/澳大利亚.md "wikilink")、[委内瑞拉和](../Page/委内瑞拉.md "wikilink")[多米尼加共和国的篮球联赛中效力](../Page/多米尼加共和国.md "wikilink")。

2000年，杰克逊被[新泽西网队签下](../Page/新泽西网队.md "wikilink")，终于得到了效力NBA的机会，在新秀赛季，杰克逊逐渐成为篮网队的先发球员之一。赛季结束后，杰克逊被[圣安东尼奥马刺队签下](../Page/圣安东尼奥马刺队.md "wikilink")。2003年，杰克逊帮助球队夺得了当年的[NBA总冠军](../Page/NBA总冠军.md "wikilink")。

2003年赛季结束后，杰克逊作为自由球员与[亚特兰大鹰队签约](../Page/亚特兰大鹰队.md "wikilink")，成为其主力得分手。次年，他被先签后换到[印第安纳步行者队](../Page/印第安纳步行者队.md "wikilink")，交换[阿尔·哈林顿](../Page/阿尔·哈林顿.md "wikilink")。2004年11月19日，由于参与著名的[奥本山宫殿斗殴事件](../Page/奥本山宫殿斗殴事件.md "wikilink")，杰克逊被联盟处以30场禁赛。

2007年1月，杰克逊在一次8人大交易中被交换去金州勇士队。在当年的[NBA季后赛中](../Page/NBA季后赛.md "wikilink")，杰克逊率领勇士队大爆冷门，淘汰了[NBA常规赛胜率第一的](../Page/NBA常规赛.md "wikilink")[达拉斯小牛队](../Page/达拉斯小牛队.md "wikilink")。

赛季结束后，[圣安东尼奥马刺队沒有續留杰克逊](../Page/圣安东尼奥马刺队.md "wikilink")，12月10日被[洛杉磯快艇签下](../Page/洛杉磯快艇.md "wikilink")。

## 参考资料

## 外部链接

  - [NBA.com球员档案](http://www.nba.com/playerfile/stephen_jackson/)
  - [个人技术统计](http://www.basketball-reference.com/players/j/jacksst02.html)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:新泽西网队球员](../Category/新泽西网队球员.md "wikilink")
[Category:圣安东尼奥马刺队球员](../Category/圣安东尼奥马刺队球员.md "wikilink")
[Category:亚特兰大老鹰队球员](../Category/亚特兰大老鹰队球员.md "wikilink")
[Category:印第安纳步行者队球员](../Category/印第安纳步行者队球员.md "wikilink")
[Category:金州勇士队球员](../Category/金州勇士队球员.md "wikilink")
[Category:夏洛特山貓队球员](../Category/夏洛特山貓队球员.md "wikilink")
[Category:密尔沃基雄鹿队球员](../Category/密尔沃基雄鹿队球员.md "wikilink")
[Category:洛杉磯快船隊球員](../Category/洛杉磯快船隊球員.md "wikilink")