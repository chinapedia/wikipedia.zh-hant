**成展權**（**Shing Chin
Kuen**，），小時候曾為[香港童星](../Page/香港童星.md "wikilink")。曾就讀[馬鞍山靈糧小學](../Page/馬鞍山靈糧小學.md "wikilink")，中一至中五曾於[聖公會林裘謀中學就讀](../Page/聖公會林裘謀中學.md "wikilink")，而預科（中六至中七）曾就讀[循道中學](../Page/循道中學.md "wikilink")，現於[香港城市大學就讀政策與行政榮譽社會科學學士課程](../Page/香港城市大學.md "wikilink")，曾於香港電台[教育电视及多部香港](../Page/教育电视.md "wikilink")[無綫電視劇集演出](../Page/無綫電視.md "wikilink")，其中在教育電視中更是多次擔任主角，他的妹妹[成珈瑩也是香港童星](../Page/成珈瑩.md "wikilink")。\[1\]\[2\]

## 曾參演劇集

  - 1999年：《[雙面伊人](../Page/雙面伊人.md "wikilink")》飾 梁浩東
  - 2000年：《[無業樓民](../Page/無業樓民.md "wikilink")》飾 古浪聚
  - 2000年：《[十月初五的月光](../Page/十月初五的月光.md "wikilink")》飾 祝健康
  - 2001年：《[廉政追擊](../Page/廉政追擊.md "wikilink")—超人爸爸》飾 泉仔
  - 2001年：《[勇往直前](../Page/勇往直前.md "wikilink")》飾 康仔
  - 2003年：《[撲水冤家](../Page/撲水冤家.md "wikilink")》飾 常喜朗
  - 2003年：《[帝女花](../Page/帝女花.md "wikilink")》飾 周世顯（童年）
  - 2004年：《[陀槍師姐IV](../Page/陀槍師姐IV.md "wikilink")》飾 程浩光
  - 2006年：《[天幕下的戀人](../Page/天幕下的戀人.md "wikilink")》飾 沈朗（少年）

## 其他

  - 2011年：TVB兔年廣告：《團圓》

## 參考

## 外部連結

  -
[chin](../Category/成姓.md "wikilink")
[Category:香港前兒童演員](../Category/香港前兒童演員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:聖公會林裘謀中學校友](../Category/聖公會林裘謀中學校友.md "wikilink")

1.  [1](https://www.hk01.com/%E5%A8%9B%E6%A8%82/122220/90%E5%B9%B4%E4%BB%A3%E7%AB%A5%E6%98%9F%E5%8E%BB%E6%99%92%E9%82%8A-ETV%E4%B8%80%E5%93%A5-%E6%88%90%E5%B1%95%E6%AC%8A%E5%8E%9F%E4%BE%86%E5%B9%BE%E9%9D%9A%E4%BB%94)
2.  [图揭港剧小童星容貌变化
    对比惊人](http://www.hinews.cn/news/system/2014/08/29/016908885.shtml)