**白肩鵰**（[学名](../Page/学名.md "wikilink")：**）与[金雕非常类似](../Page/金雕.md "wikilink")，个头略小，体长为80厘米，翼展为200厘米，属于[猛禽家族的](../Page/猛禽.md "wikilink")[鹰科](../Page/鹰科.md "wikilink")。

白肩雕分布在欧洲东南部、西亚和中亚。\[1\][西班牙白肩雕](../Page/西班牙白肩雕.md "wikilink")（**）生活在[西班牙和](../Page/西班牙.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")，以前认为和白肩雕是一个物种，如今由于在[形态学](../Page/形态学.md "wikilink")\[2\]、[生态学](../Page/生态学.md "wikilink")\[3\]和特征的不同被分成两个不同的种类。\[4\]\[5\]\[6\]

冬天，白肩鵰迁徙到[非洲](../Page/非洲.md "wikilink")、[印度和](../Page/印度.md "wikilink")[中国](../Page/中国.md "wikilink")。\[7\]欧洲，白肩雕是濒危物种，在它曾经分布的地方如[匈牙利和](../Page/匈牙利.md "wikilink")[奥地利几近消失](../Page/奥地利.md "wikilink")。\[8\]如今欧洲只有[潘诺尼亚平原地区白肩雕的数量在上升](../Page/潘诺尼亚平原.md "wikilink")，主要在匈牙利北部的山区和[斯洛伐克南部](../Page/斯洛伐克.md "wikilink")，其中匈牙利白肩雕的数量约为70-80对。

[Kaiseradler_Aquila_heliaca_2_amk.jpg](https://zh.wikipedia.org/wiki/File:Kaiseradler_Aquila_heliaca_2_amk.jpg "fig:Kaiseradler_Aquila_heliaca_2_amk.jpg")
[奥匈帝国的君主曾经将白肩雕选做](../Page/奥匈帝国.md "wikilink")[纹章上的动物](../Page/纹章学.md "wikilink")，但是并没有对这种动物有所帮助。与其他雕不同的是，白肩雕喜欢栖息在小树林的田野，而不喜欢生活在山脉、大型森林或树木稀少的草原。

白肩鵰将巢筑在树上，周围没有其他树木，因此这些巢在很远处就可以看到，白肩雕也可以观察周围的情况。白肩鵰用树枝筑巢，加以草与羽毛。

每年3-4月，雌雕产2-3枚卵。孵化期为45天，通常只有一只能存活下来，其余在长成过程中死亡。

白肩鵰主要吃[美洲黃鼠](../Page/美洲黃鼠屬.md "wikilink")，也扑食[啮齿目](../Page/啮齿目.md "wikilink")、[貂屬](../Page/貂屬.md "wikilink")、[狐属等哺乳动物及鸟类](../Page/狐狸.md "wikilink")。

## 亚种

  - **白肩鵰指名亚种**（[学名](../Page/学名.md "wikilink")：）。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[新疆](../Page/新疆.md "wikilink")、[辽宁](../Page/辽宁.md "wikilink")、[河北](../Page/河北.md "wikilink")、[河南](../Page/河南.md "wikilink")、[青海](../Page/青海.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[甘肃](../Page/甘肃.md "wikilink")、长江中下游、南至福建、[广东等地](../Page/广东.md "wikilink")。该物种的模式产地在埃及。\[9\]

## 保护

  - [中国国家重点保护动物等级](../Page/中國國家重點保護野生動物名錄.md "wikilink")：一级

## 参考资料

[Category:真鵰屬](../Category/真鵰屬.md "wikilink")
[Category:中國國家一級保護動物](../Category/中國國家一級保護動物.md "wikilink")

1.

2.  Cramp, S. & Simmons, K. E. L. (1980) Birds of the Western
    Palearctic, Vol. 2. Oxford University Press, Oxford.

3.  Meyburg, B. U. (1994): \[210 & 211: Imperial Eagles\]. *In:* del
    Hoyo, Josep; Elliott, Andrew & Sargatal, Jordi (editors): *[Handbook
    of Birds of the
    World](../Page/Handbook_of_Birds_of_the_World.md "wikilink"), Volume
    2: New World Vultures to Guineafowl*: 194-195, plate 20. Lynx
    Edicions, Barcelona. ISBN 84-87334-15-6

4.  Sangster, George; Knox, Alan G.; Helbig, Andreas J. & Parkin, David
    T. (2002): Taxonomic recommendations for European birds.
    *[Ibis](../Page/Ibis_\(journal\).md "wikilink")* **144**(1):
    153–159.  [PDF
    fulltext](http://www.blackwell-synergy.com/doi/pdf/10.1046/j.0019-1019.2001.00026.x)

5.  Padilla, J. A.; Martinez-Trancón, M.; Rabasco, A. &
    Fernández-García, J. L. (1999): The karyotype of the Iberian
    imperial eagle (*Aquila adalberti*) analyzed by classical and DNA
    replication banding. *Cytogenetics and Cell Genetics* **84**: 61–66.
     (HTML abstract)

6.  **Seibold**, I.; Helbig, A. J.; Meyburg, B. U.; Negro, J. J. & Wink,
    M. (1996): Genetic differentiation and molecular phylogeny of
    European Aquila eagles (Aves: Falconiformes) according to
    cytochrome-*b* nucleotide sequences. *In:* Meyburg, B. U. &
    Chancellor, R. D. (eds): *Eagle Studies*: 1–15. Berlin: World
    Working Group on Birds of Prey.

7.
8.
9.