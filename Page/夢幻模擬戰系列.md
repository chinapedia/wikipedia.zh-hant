是由[Masaya](../Page/Masaya_Games.md "wikilink")(NCS，即開發電玩軟體使用的品牌）旗下[Career
soft](https://en.m.wikipedia.org/wiki/Career_Soft)
製作的[战略角色扮演](../Page/戰略角色扮演遊戲.md "wikilink")[電子遊戲系列](../Page/電子遊戲.md "wikilink")。此遊戲系列起用知名漫畫家[漆原智志為人物設計](../Page/漆原智志.md "wikilink")，由於漆原氏以繪畫性感美女[漫畫人物為人熟悉](../Page/漫畫人物.md "wikilink")，所以此系列亦吸納不少漫畫迷。

## 作品名稱

自[Mega Drive首作推出](../Page/Mega_Drive.md "wikilink")，經歷[PC
Engine的移植版](../Page/PC_Engine.md "wikilink")，到在MD推出的第二集，英文名稱一直為LUNGRISSER(即ラングリッサー)\[1\]，直至[超級任天堂的](../Page/超級任天堂.md "wikilink")《Der
Langrisser》推出，系列被正名並沿用至今。

由於系列最初在Mega Drive平台發售時，盒上印有“ファンタジーシミュレーション”（Fantasy
Simulation）因而有了“夢幻模擬戰”的[譯名](../Page/譯名.md "wikilink")，爲盜版商所譯。也有叫蘭古利薩、蘭格莉薩等，皆是基於Langrisser的直接[音譯](../Page/音譯.md "wikilink")。

本系列的[英語版被起名為](../Page/英語.md "wikilink")《War Song》。

## 故事背景

### Langrisser I, II& III

遠古神魔時代的艾魯薩莉亞，為了對抗混沌之神卡奧斯及其創造的魔物，阻止其致力於使世界陷入混亂恐懼与徹底崩壞的行為，光之女神露茜莉絲使用古代偉大光輝裔之王的靈魂製成光之聖劍蘭古利薩（因此只有光之後裔使用），來對抗強大的魔劍阿魯哈薩托。最終在光之女神分身-大魔導師傑西卡則引導下，光明戰勝了黑暗，聖劍和魔劍都被封印，而她則一直轉生以監視着魔族力量的恢複。
圍繞着追求聖劍和魔劍的力量，來滿足自身的願望，多方勢力在不同時代參與爭奪這兩把劍，而一切的陰謀背後總有着卡奧斯的身影。不同時代的光輝裔們在旅途中逐漸擔負起拯救世界的重任，阻止了黑暗与混亂降臨于世界.

### Langrisser IV& V

故事發生在很多年后的另一個叫伊雷斯的大陸上，“賢者的水晶”（被封印的魔劍和聖劍）被邪神甘多拉基路偷到了這個大陸，爭奪又一次開始，而最終正義還是戰勝了黑暗。

## 遊戲玩法

與其他戰略角色扮演遊戲類似，整個遊戲都被分拆成不同的關卡場景，每關都有勝利的條件，大致上都是通過戰鬥打敗敵方軍隊而清關，但在個別關數亦有特定的勝利條件，達成後方可以接續故事的發展。

進入場景之前，可以爲角色裝備武器防具和其他道具。角色的初始位置也可以選擇。傭兵系統是本系列的遊戲特色，升級到不同[職業可以攜帶不同類型傭兵](../Page/職業.md "wikilink")。傭兵需要購買，一般高級兵種需要更高的價格。在角色統帥範圍下傭兵的攻防能力會得到加成，提高取決於攻防修正值，一般高等職業擁有更高的修正值；有些裝備道具也能影響修正值；修正值在滿足某些條件也可以累積提高。中後期修正值對遊戲通關難度有極大影響。
遊戲有豐富的職業，不同角色隨玩家心意可以選擇獨特轉職路線。職業分成諸如戰士系，騎兵系等不同種類，可以攜帶不同的傭兵和裝備，學會獨特的[魔法](../Page/魔法.md "wikilink")，攻擊方式也會有區別。有些角色有其獨有的職業。（如Langrisser
2的只有雪莉可以轉職到公主）。轉職石可以讓高級別職業在10級時轉回到初始職業，用於累積能力或更正選錯的路線。

戰鬥方面，I\&II採用[回合制](../Page/回合制.md "wikilink")，分我方行動回合和敵方行動回合，傭兵和主將獨立單位，每回合開始在主將邊的傭兵會得到少量回複。主將可以選擇移動攻擊，使用魔法和休息，而傭兵只能攻擊和移動。III的系統則進行徹底的變動，採用半[即時制](../Page/即時制.md "wikilink")，所有傭兵和主將同時在一起進行戰鬥。IV\&V恢複到類似前兩代的回合制系統，區別的是使用了“行動值”這一類似[最终幻想VII](../Page/最终幻想VII.md "wikilink")
的ATB系統。\[2\]

兵種剋制在遊戲中起著重要的影響，能力相同的傭兵可以完全壓製被剋制的兵種。（例如槍兵剋騎兵，騎兵剋步兵等）。\[3\]同時地形對有些兵種有屬性加成。

基本上系列的每一集都有相似的[程式彩蛋](../Page/程式彩蛋.md "wikilink")，可以使用秘籍選關，進入隱藏商店購買道具，以及在隱藏關挑戰“兄貴”。

從Der Langrisser開始遊戲可以進行選擇來改變遊戲劇情，最終得到不同的路線的結局
（III除外），遊戲還引入女角色好感度，影響女主角的決定和最終的故事結局。

## 系列作品

作品依系列排序：

  - 《[Langrisser](../Page/梦幻模拟战_\(游戏\).md "wikilink")》（Mega
    Drive、1991年作品、系列的第一集）
      - 《Langrisser 光輝之末裔》（PCE SCD、1993年作品、移植自Mega
        Drive版第一集並加入[配音](../Page/配音.md "wikilink")）
      - 《Langrisser》（[Windows](../Page/Windows.md "wikilink")、2003年作品，移植自[PlayStation版的](../Page/PlayStation.md "wikilink")《I\&II》中的第一集）
      - 《Langrisser》（[i-mode](../Page/i-mode.md "wikilink")、2007年作品，以系列第一集為故事藍本，並將版圖改為半立體，以手機熒幕和操作性為大前提重新製作）
  - 《[Langrisser II](../Page/梦幻模拟战.md "wikilink")》（Mega Drive、1994年作品）
      - 《Der Langrisser》（超級任天堂、1995年作品、改編自Mega
        Drive版第二集；由此集起，此系列的英文名稱被「正名」為《Langrisser》）
      - 《Der Langrisser
        FX》（[PC-FX](../Page/PC-FX.md "wikilink")、1996年作品、移植自SFC版，加入動畫和配音）
      - 《Langrisser I\&II》（PlayStation、1997年作品、改編自PCE版的第一集和PC-FX版的第二集）
      - 《Langrisser Dramatic Edition》（[Sega
        Saturn](../Page/Sega_Saturn.md "wikilink")、1998年作品、移植自PS版的《I\&II》；附送8公分音像CD，收錄外傳[廣播劇](../Page/廣播劇.md "wikilink")）
      - 《Langrisser II》（Windows、2003年作品，移植自PS版的《I\&II》中的第二集）
  - 《Langrisser III》（Sega Saturn、1996年作品、由回合制改為半實時戰鬥）
      - 《Langrisser
        III》（Windows中文版、韓文版，2000年作品，移植自SS版第三集；中文版由[旭力亞有限公司發行](../Page/旭力亞有限公司.md "wikilink")）
      - 《Langrisser III》（[PlayStation
        2](../Page/PlayStation_2.md "wikilink")、2005年作品，移植自SS版第三集）
  - 《Langrisser IV》（Sega Saturn、1997年作品）
  - 《Langrisser V the End of Legend》（Sega Saturn、1998年作品、系列的最終作）
      - 《Langrisser IV\&V Final
        Edition》（PlayStation、1999年作品，移植自SS版的第四、第五集，雙光碟）
  - 《Langrisser Tribute》（Sega Saturn、1999年作品，將SS的《Dramatic
    Edition》、《III》、《IV》和《V》一併推出的合訂本，附有特別包裝和贈品）
  - 《Langrisser
    Millennium》（[Dreamcast](../Page/Dreamcast.md "wikilink")、1999年作品，遊戲系統、世界觀、製作群和人設都與以往不同，屬掛名之作；人設改為[介錯](../Page/介錯_\(漫畫家\).md "wikilink")；另有台灣漫畫家[翔龍改編的漫畫](../Page/翔龍.md "wikilink")《夢幻模擬戰
    千年紀》，[青文出版社出版](../Page/青文出版社.md "wikilink")，單行本全2卷）
      - 《Langrisser Millennium WS the Last Century》（[Wonder
        Swan](../Page/Wonder_Swan.md "wikilink")、2000年作品；遊戲玩法回歸原點，但人設、世界觀等延續自Dreamcast版）
  - 《Langrisser Re:Incarnation(転生)》（[Nintendo
    3DS](../Page/Nintendo_3DS.md "wikilink")、2015年7月23日；时隔15年的系列新作，人设改为カイエダヒロシ）
  - 《Langrisser
    Mobile》（[iOS](../Page/iOS.md "wikilink")、[Android](../Page/Android.md "wikilink")、2018年10月4日；由[紫龍遊戲開發](../Page/紫龍遊戲.md "wikilink")）

## 开发

夢幻模擬戰的世界觀采自于1987年發表在[PC-8801](../Page/PC-8801.md "wikilink")、[PC-9801的兩部作品](../Page/PC-9801.md "wikilink")《艾魯斯里多》和《蓋亞的紋章》，這兩部作品是當時少有的強化劇情的戰略回合遊戲。\[4\]

於Mega
Drive主機製作的第一集誕生於1990年，雖然只有4MB的容量，但由於玩法新穎，畫面和BGM亦製作精良，因此廣受MD用家歡迎；三年後被移植到PC
Engine的SUPER
CD-ROM²，由於媒體改用[唯讀光碟](../Page/唯讀光碟.md "wikilink")，因此在聲畫兩方面都得到強化，亦為更多人所認識。

承接著PC Engine版的好評，系列的第二集亦在翌年(1994年)回歸Mega
Drive主機，並以第一集的四倍容量──16MB製作；在一年後的1995年，NCS將MD的第二集故事加以改編，並加上故事分支概念，故事前的相性診斷等新意念再移植到超級任天堂，並命名為《Der
Langrisser》，由於主角可以在故事的發展過程中選擇自己的職業和前路，因此並不一定像MD的第二集般只能為光之軍勢破關斬將，玩家更可以加入帝國軍、暗之軍勢甚至為自己而戰最後建國。超任版奠定了此系列往後多個續作的遊戲系統。

1996年5月，NEC Home Electronic提供資金予NCS，為其32BIT主機《PC-FX》製作《Der Langrisser
FX》，FX版以超任版為藍本，但在重要情節配上間場動畫和配音，聲優演出令此遊戲更具動畫連續劇之感，亦確立了此系列的鮮明的發展路向。

同年10月，NCS 於[世嘉的次世代主機Sega](../Page/世嘉.md "wikilink")
Saturn推出系列的第三集，雖然命名為《Langrisser
III》，但第三集其實是整個系列的敘事詩，時代設定在第一集之前，故事主要描述聖劍Langrisser的誕生。比較多個前作，第三集的系統大幅度改變，由以前的回合制改為半實時制，戰鬥畫面亦化身為立體。在開場有九十秒的動畫場面，而且有精美的限定版供玩家選購，限定版比普通版多出一本《Fan's
Book》以及一個立體封面。

由於第三集採用半即時制的新系統，相比之前的回合制改動過大，但本身並不平衡完善，所以不為普遍玩家所接受，\[5\]
所以翌年8月在SS推出的《Langrisser
IV》，系統基本回復至一、二集般，但原來的回合制改成“JUDGMENT
PHASE
SYSTEM”。角色、部隊在新系統之下，根據其經驗值和裝備，可以有不只一次的行動機會；戰鬥畫面亦回歸為2D平面。今集的初回限定版比第三集更華麗，附有一個瑰麗的包裝盒、《Fan's
Book》一本以及三個襟針。

而在差不多同一時間，NCS亦為 PlayStation 推出《Langrisser I\&II》的合集，這是將PC
ENGINE的《Lungrisser 光輝之末裔》和PC-FX的《Der Langrisser
FX》合而為一，再作整頓而製作的PS平台版本。合集內所收錄的皆以第二集的系統作為基本，且加入了豐富的動畫。和SS的《Langrisser
IV》一樣，合集亦有推出初回限定版。

在翌年，也就是1998年，NCS再為SS用家推出名為《Dramatic Edition》的復刻作品，這個其實是PS《Langrisser
I\&II》的移植版，。某些角色的“大頭相”以及所有戰鬥畫面背景完全重作，在《II》裡的分支故事亦有增加；值得留意隨此作附有一片廣播劇[CD](../Page/CD.md "wikilink")，遂成為遊戲的綽頭。

1998年下半年，系列的正規最終篇 《Langrisser
V～傳說的終結～》發售，系統還是延續IV的系統和故事，開場畫面極其精美，音效和背景音樂也是如往常一樣高標準，遊戲平臺依舊是Sega
Saturn.

## 參考資料

[Category:1991年首发的电子游戏系列](../Category/1991年首发的电子游戏系列.md "wikilink")
[Category:战略角色扮演游戏](../Category/战略角色扮演游戏.md "wikilink")
[Category:電子遊戲系列](../Category/電子遊戲系列.md "wikilink")
[夢幻模擬戰系列](../Category/夢幻模擬戰系列.md "wikilink")

1.  見於MD版[BGM原聲大碟](../Page/BGM.md "wikilink")、PCE版記錄存檔畫面
2.
3.
4.
5.