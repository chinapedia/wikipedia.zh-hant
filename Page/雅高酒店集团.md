**雅高酒店集团**\[1\]（**Accor**）是一家大型的[法國](../Page/法國.md "wikilink")[跨國企業](../Page/跨國企業.md "wikilink")
，在將近100個[國家中經營](../Page/國家.md "wikilink")，股票列入[巴黎CAC40指数](../Page/巴黎CAC40指数.md "wikilink")。
雅高旗下的**雅高酒店**是[歐洲](../Page/歐洲.md "wikilink")[旅館業的霸主](../Page/旅館.md "wikilink")，在全球擁有超過4000家旅館，涵蓋最豪華的酒店到最經濟的旅社。雅高旗下的**雅高服務**也在全球的企業服務業中首屈一指，以經營票券服務業著名。雅高71%的营业额来自欧洲，11%的营业额来自美洲，18%来自于其他国家及地区\[2\]。
2015年6月1号，集团宣布更名为**AccorHotels**\[3\]。

## 历史

  - 1967年，雅高集团前身，酒店投资开发集团SIEH
    由保尔.杜布吕勒和利奕良创建。同年第一家[诺富特酒店在里尔开业](../Page/诺富特酒店.md "wikilink")。
  - 1974年，在[法国](../Page/法国.md "wikilink")[波尔多地区建立了第一间](../Page/波尔多.md "wikilink")[宜必思酒店](../Page/宜必思酒店.md "wikilink")（ibis）。
  - 1975年，建立了第一间[美居酒店](../Page/美居酒店.md "wikilink")（Mercure）。
  - 1980年，建立了第一间[索菲特酒店](../Page/索菲特酒店.md "wikilink")（Sofitel）。
  - 1983年，成立“雅高酒店管理集团”，并开始了其在酒店业的迅速扩张。
  - 1985年，建立了[一級方程式酒店](../Page/一級方程式酒店.md "wikilink")（Formula 1）品牌。
  - 1990年，收购了[美国连锁酒店品牌](../Page/美国.md "wikilink")[六号汽车旅馆](../Page/六号汽车旅馆.md "wikilink")（Motel
    6）。
  - 2001年，成为[澳大利亚](../Page/澳大利亚.md "wikilink")[悉尼奥运会官方合作伙伴](../Page/悉尼奥运会.md "wikilink")。同年，集团品牌[公寓酒店](../Page/公寓酒店.md "wikilink")（Suitehotel）在[欧洲出现](../Page/欧洲.md "wikilink")。
  - 2002年，[盐湖城冬奥会官方合作伙伴](../Page/盐湖城冬奥会.md "wikilink")。
  - 2003年，[中国大陆第一家宜必思酒店在](../Page/中国大陆.md "wikilink")[天津开业](../Page/天津.md "wikilink")。
  - 2005年，第4000间酒店（Novotel Madrid
    Sanchinarro）在[西班牙](../Page/西班牙.md "wikilink")[马德里开业](../Page/马德里.md "wikilink")，成为首家诺富特品牌酒店。
  - 2007年，集团成立40周年之际，已在全世界拥有4000家酒店和17万员工，遍布五大洲的100个国家，共计45061个房间。
  - 2008年，[乌兰巴托诺富特酒店在](../Page/乌兰巴托诺富特酒店.md "wikilink")[蒙古](../Page/蒙古国.md "wikilink")[乌兰巴托开业](../Page/乌兰巴托.md "wikilink")\[4\]。同年，與[澳博合作的](../Page/澳博.md "wikilink")[澳門索菲特十六浦酒店在](../Page/澳門索菲特十六浦酒店.md "wikilink")[澳門開業](../Page/澳門.md "wikilink")。
  - 2012年雅高酒店集團大中華區運營副總裁利榮達與香港王氏仁森機構董事副總經理王經潑分別代表雙方簽署了《石獅明昇鉑爾曼酒店委托管理合同》合作協議。這是石獅首家引入國際著名品牌冠名管理的五星級酒店。投资五亿元人民币
  - 2015年6月3日，更名為**雅高酒店集團**（），並且啟用全新商標。
  - 2015年12月11日，雅高酒店集團以現金加股票的方式，收購擁有Fairmont、Raffles和Swissotel等豪華酒店品牌的FRHI公司，涉及金額29億美元，賣方為卡塔爾主權投資基金與有「中東畢非德」稱號的沙特阿拉伯巨富阿瓦利德王子旗下Kingdom集團。他們將收取8.4億美元現金與4670萬股Accor股份。因此，交易完成後，卡塔爾投資公司與Kingdom，分別持有10.5%與5.8%的Accor股權
  - 2016年截止到3月31日，[錦江國際集團持有雅高酒店集团](../Page/錦江國際集團.md "wikilink")14.98%的股份，擁有13.07%的投票權，成為雅高酒店集团第一大股東
  - 2018年4月12日完成收購餐桌預定平台ResDiary
  - 2018年4月30日以5.6億瑞士法郎（約合5.63億美元）收購在全球27個國家擁有84間酒店（超過2萬間客房）的瑞士瑞享酒店及度假村（Movenpick
    Hotels and Resorts）

<!-- end list -->

  - 2018年6月5日雅高酒店集团以12亿澳元的价格成功收购Mantra集团。本次收购的品牌包括Mantra、Peppers、BreakFree和Art
    Series，囊括了澳大利亚、新西兰、夏威夷和巴厘岛的138家酒店。

## 標誌變遷

[File:accor.svg|雅高集團第二代商標](File:accor.svg%7C雅高集團第二代商標) (2007年\~2011年)
<File:Accor> 2011.png|雅高集團第三代商標 (2011年\~2015年) <File:AccorHotels> Logo
2016.png|雅高酒店集團現時商標 (2015年6月3日至今)

## 概况

[Accor_global_locations.png](https://zh.wikipedia.org/wiki/File:Accor_global_locations.png "fig:Accor_global_locations.png")

  - 公司性质：[股份公司](../Page/股份公司.md "wikilink")，巴黎CAC40指数中上市公司之一
  - 亚太区主席兼首席执行官： 艾森伯（M. Issenberg）
  - 亚太区总部：[澳大利亚](../Page/澳大利亚.md "wikilink")[悉尼](../Page/悉尼.md "wikilink")
  - 亚洲区总部：[泰国](../Page/泰国.md "wikilink")[曼谷](../Page/曼谷.md "wikilink")
  - 中国区总部：[中国](../Page/中国.md "wikilink")[上海](../Page/上海.md "wikilink")

雅高集团拥有一所自己的学院，于1985年法国埃夫里地区创立——法国雅高学院，这个学院十分靠近雅高集团总部。为集团培养自己的行业人才。

## 标识

[Canada-Goose-Szmurlo.jpg](https://zh.wikipedia.org/wiki/File:Canada-Goose-Szmurlo.jpg "fig:Canada-Goose-Szmurlo.jpg")\]
雅高集团的标识来自于一种
[加拿大野生大雁](http://en.wikipedia.org/wiki/Canada_Goose)，这个标示有以下三种含义。

  - 象征：开放，[自由](../Page/自由.md "wikilink")，[旅行](../Page/旅行.md "wikilink")，互相支持，多种专业技能的交融
  - 表明：持久，全力以赴，友好，团队精神
  - 标志：充满活力，精于传承，体现宁静而温暖的情怀

## 集团战略领域

法国雅高集团擁有两大战略领域：

### 酒店品牌

[Sofitelnanjing.JPG](https://zh.wikipedia.org/wiki/File:Sofitelnanjing.JPG "fig:Sofitelnanjing.JPG")

  - 奢华酒店品牌
      - [莱佛士](../Page/莱佛士酒店.md "wikilink")
      - [费尔蒙](../Page/费尔蒙酒店.md "wikilink")
      - [悦榕庄](../Page/悦榕庄.md "wikilink")

<!-- end list -->

  - 索菲特系列（Sofitel Universe)
      - [索菲特传奇](../Page/索菲特传奇酒店.md "wikilink") （Sofitel Legend ）
      - [索菲特](../Page/索菲特酒店.md "wikilink") （Sofitel）
      - SO/
      - [美憬阁](../Page/美憬阁酒店.md "wikilink")（MGallery by Sofitel）

<!-- end list -->

  - 高档酒店品牌
      - [铂尔曼](../Page/铂尔曼酒店.md "wikilink")（Pullman)
      - [瑞士酒店](../Page/瑞士酒店.md "wikilink")（Swissôtel）
      - [悦椿](../Page/悦椿酒店.md "wikilink")（Angsana)
      - [美爵](../Page/美爵酒店.md "wikilink")（Grand Mecure）
      - [美居](../Page/美居酒店.md "wikilink")（Mecure)
      - [诺富特](../Page/诺富特.md "wikilink")（Novotel）
      - Suitehotel
      - Mantra
      - Rixos
      - The Sebel
      - Art Series
      - Mövenpick
      - Thalassa

<!-- end list -->

  - 经济型酒店品牌
      - [宜必思](../Page/宜必思酒店.md "wikilink")（Ibis）
      - [宜必思尚品](../Page/宜必思尚品.md "wikilink")（Ibis Styles）
      - [阿德吉奥公寓酒店](../Page/阿德吉奥公寓酒店.md "wikilink")（Adagio）
      - 25Hours酒店
      - Peppers
      - Breakfree

<!-- end list -->

  - 简易型酒店品牌
      - Mama Shelter
      - Hotel F1
      - JO\&JOE

雅高已經於2007年賣出 Red Roof Inns（红屋顶旅社），於2012年賣出 Motel 6 酒店集团

  - 其他
      - Le Notre（甜品连锁店）
      - Compagnie des
        Wagons-lits（在欧洲五国奥、西、法、意、葡，提供火车上的餐饮住宿服务）著名的[東方快車是業務的一部分](../Page/東方快車.md "wikilink")。

### 雅高服务/宜睿Edenred

雅高服務的業務範圍遍佈世界40個國家，當中超過43萬的公司和機構的3000萬使用客戶。

該品牌常替這些公司的[人事部門代理員工福利方面的瑣事](../Page/人力資源.md "wikilink")，包括：

  - 餐廳券（Ticket Restaurant）
  - 午餐卷（Luncheon Vouchers）
  - Ticket Alimentaçao
  - Clean Way
  - Ticket Service
  - Childcare Vouchers
  - Eyecare Vouchers
  - Bien-Etre à la Carte
  - Worklife Benefits
  - EAR
  - Accentiv'
  - Académie du Service
  - Tesorus
  - Ticket Compliments

## 地球客人

地球客人（Earth
Guest）是雅高集团于2005年和[联合国环境署](../Page/联合国环境署.md "wikilink")（UNEP）合作创建并实施的一项致力于保护地球环境的环保计划。它是以[ISO14001标准为基础的](../Page/ISO14001.md "wikilink")。\[5\]
地球客人计划的吉祥物是一只白色的鹅，雅高为其取名“鹅妈妈”。

## 忠诚客户计划

雅高集团的忠诚客户计划称为“A Club”,这个计划于2008年9月15日面世，与之前的“Advantage
Plus”共同成为雅高忠诚计划中的一个项目。
其中，“A Club”计划是以消费积分方式运营。“Advantage Plus”计划是以免费券以及折扣消费方式运营的忠诚客户计划。

## 参考资料

## 外部連結

  - [雅高酒店](http://www.accorhotels.com/accorhotels/lien_externe.svlt?code_langue=zh&sourceid=wikipediaZH&merchantid=aff-accorCN)
  - [雅高](https://web.archive.org/web/20080906135915/http://www.accorservices.com/)
  - [索菲特](http://www.sofitel.com)
  - [诺富特](http://www.novotel.com)

[Category:法國餐旅公司](../Category/法國餐旅公司.md "wikilink")
[Category:跨國飯店集團](../Category/跨國飯店集團.md "wikilink")
[Category:1967年成立的公司](../Category/1967年成立的公司.md "wikilink")
[Category:CAC 40](../Category/CAC_40.md "wikilink")
[Category:雅高集團](../Category/雅高集團.md "wikilink")

1.  "[footerImg.jpg](http://www.grandmercurehotels.com.cn/images/common/footerImg.jpg)."
    ([Archive](http://www.webcitation.org/6FexVmDBg)) Grand Mercure
    Hotels China.
2.
3.  <http://www.challenges.fr/conso-et-luxe/20150601.CHA6402/le-groupe-accor-va-changer-de-nom.html>
4.
5.