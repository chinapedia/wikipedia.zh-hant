**南洋杉**（[学名](../Page/学名.md "wikilink")：），又稱**肯氏南洋杉**、**花旗杉**，是[南洋杉科](../Page/南洋杉科.md "wikilink")[南洋杉属下的一个物种](../Page/南洋杉属.md "wikilink")，寿命可长达450年，主要分布在[澳大利亚和](../Page/澳大利亚.md "wikilink")[新几内亚](../Page/新几内亚.md "wikilink")。

## 形态

[Araucaria_cunninghamii_bark.JPG](https://zh.wikipedia.org/wiki/File:Araucaria_cunninghamii_bark.JPG "fig:Araucaria_cunninghamii_bark.JPG")

常绿[乔木](../Page/乔木.md "wikilink")，高度可达60米。枝轮生平展，侧生小枝密集而下垂，近羽状排列。叶有两种类型，幼树的叶排列疏松，开展，针形，老树和花果枝上的叶排列紧密，卵形或三角状卵形，雌雄异株。[球果卵形](../Page/球果.md "wikilink")，果鳞宽而尖，向后弯曲。两侧具结合而生的薄翅。

## 参考文献

[南洋杉](../Category/南洋杉属.md "wikilink")
[Category:新幾內亞植物](../Category/新幾內亞植物.md "wikilink")
[Category:澳洲植物](../Category/澳洲植物.md "wikilink")