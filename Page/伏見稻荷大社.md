[FushimiInari_Taisha_Fox-Statue.jpg](https://zh.wikipedia.org/wiki/File:FushimiInari_Taisha_Fox-Statue.jpg "fig:FushimiInari_Taisha_Fox-Statue.jpg")
[FushimiInari_Taisha_Fox-Ema.jpg](https://zh.wikipedia.org/wiki/File:FushimiInari_Taisha_Fox-Ema.jpg "fig:FushimiInari_Taisha_Fox-Ema.jpg")\]\]

**伏見稻荷大社**（）是一位於[日本](../Page/日本.md "wikilink")[京都市](../Page/京都市.md "wikilink")[伏見區內的](../Page/伏見區.md "wikilink")[神社](../Page/神社.md "wikilink")，是遍及日本全國各地約四萬多所的[稻荷神社之總本社](../Page/稻荷神.md "wikilink")，以境內所擁有的「千本鳥居」聞名。

## 参拜

伏見稻荷大社位於[稻荷山的山麓](../Page/稻荷山.md "wikilink")，在傳統上整個稻荷山的範圍都被視為是神域（聖地）的範圍。伏見稻荷大社主要是祀奉以[宇迦之御魂大神為首的諸位](../Page/宇迦之御魂神.md "wikilink")[稻荷神](../Page/稻荷神.md "wikilink")，自古以來就是[農業與](../Page/農業.md "wikilink")[商業的神明](../Page/商業.md "wikilink")，除此之外也配祀包括[佐田彥大神](../Page/佐田彥大神.md "wikilink")、[大宮能賣大神](../Page/大宮能賣大神.md "wikilink")、[田中大神與](../Page/田中大神.md "wikilink")[四大神等其他的神明](../Page/四大神.md "wikilink")。由於每年都有大量的香客前來神社祭拜求取農作豐收、生意興隆、交通安全，使得該神社成為京都地區香火最盛的神社之一。另外，起源於[江戶時代的習俗](../Page/江戶時代.md "wikilink")，前來此地許願的人們往往會捐款在神社境內豎立一座[鳥居來表達對神明的敬意](../Page/鳥居.md "wikilink")，使得伏見稻荷大社的範圍內豎有數量驚人的大小鳥居，而以「千本鳥居」之名聞名日本全國乃至於海外。捐款豎立鳥居的單位包含個人、公司行號乃至於各地的商會組織，目前現存的鳥居，最早可以追溯到[明治年間](../Page/明治.md "wikilink")。

在神社的分類上，伏見稻荷大社屬於[式內社](../Page/式內社.md "wikilink")（被[延喜式神名帳所收錄的神社](../Page/延喜式神名帳.md "wikilink")）中的[名神大社](../Page/名神大社.md "wikilink")（祭祀知名神明、自古以來就非常靈驗的神社）等級，為[二十二社之一](../Page/二十二社.md "wikilink")，舊時神社社格屬[官幣大社等級](../Page/官幣大社.md "wikilink")。

## 歷史

相傳伏見稻荷大社是在[奈良時代的](../Page/奈良時代.md "wikilink")711年（[和銅](../Page/和銅.md "wikilink")4年）2月19日時，由[伊侶具秦公奉命在伊奈利山](../Page/伊侶具秦公.md "wikilink")（稻荷山）三峰設置，用以祭祀[秦氏世代以來的守護神](../Page/日本秦氏.md "wikilink")（[氏神](../Page/氏神.md "wikilink")）也是農耕之神的稻荷神，並將神社命名為「伊奈利社」（「伊奈利是日文中「稻生」的發音直接轉譯）。

伏見稻荷大社的[社家](../Page/社家.md "wikilink")（世襲神社內神職人員一職的家族）曾經出過多位學者，其中又以江戶時代中期的[國學者](../Page/國學_\(日本\).md "wikilink")[荷田春滿最為知名](../Page/荷田春滿.md "wikilink")。今日伏見稻荷大社境內可以見到被特別保存下來的荷田春滿舊宅，在一旁則可以見到原本是為了祭祀荷田春滿所設、目前已獨立而出的[東丸神社](../Page/東丸神社.md "wikilink")。東丸神社前的小徑旁的牆上可以見到大量的[繪馬](../Page/繪馬.md "wikilink")，是長年來來到此地求取考運的學子為了祈福而懸掛。\[1\]

## 建築

伏見稻荷大社最初的本殿已於[應仁之亂時燒毀](../Page/應仁之亂.md "wikilink")，今日所見到的本殿為[明應](../Page/明應.md "wikilink")8年（1499年）時重建的版本，目前被日本政府指定為[國家重要文化財產](../Page/國家重要文化財產.md "wikilink")。

除了主殿建築之外，伏見稻荷大社境內最受注目的建物莫過於滿佈整座山頭的各式大小鳥居。除此之外，神社的範圍之內也可以見到許多[狐狸](../Page/狐狸.md "wikilink")（稻荷神的使者）的雕像，與非常特殊的狐狸造型繪馬。

## 交通

在許多前往伏見稻荷大社的交通方式之中，鐵路是最為便捷的一種。隸屬於[JR西日本](../Page/JR西日本.md "wikilink")[奈良線沿線的](../Page/奈良線.md "wikilink")[稻荷車站](../Page/稻荷車站.md "wikilink")，與伏見稻荷大社的入口僅有一街之隔，是來此參訪的香客們主要的進出管道，而車站月台的遮雨棚樑柱也特別漆成像鳥居的橘紅色。另外[京阪電鐵亦設有伏見稻荷車站](../Page/京阪電鐵.md "wikilink")，離大社入口約步行5分鐘。
另市內公車則有從京都站開出的南5及105號線兩個選擇，下車後走7分鐘即可到達入口。

## 流行文化中的曝光

  - 以京都的[藝妓生活作為主題的美國電影](../Page/藝妓.md "wikilink")《[藝伎回憶錄](../Page/藝伎回憶錄.md "wikilink")》（Memoirs
    of a Geisha）中，曾出現過以伏見稻荷大社的千本鳥居作為背景的場景。
  - 日本動畫《[名偵探柯南](../Page/名偵探柯南.md "wikilink")》劇場版《[迷宮的十字路口](../Page/名偵探柯南：迷宮的十字路.md "wikilink")》，片尾曲曾出現伏見稻荷大社的千本鳥居作為背景。
  - 日本動畫《名偵探柯南》的旅遊特別篇《[明治維新神秘之旅](../Page/明治維新.md "wikilink")》中出現過。
  - 漫畫及動畫作品《[狐仙的戀愛入門](../Page/狐仙的戀愛入門.md "wikilink")》中的「伊奈里神社」即以此為原型。
  - 《[假面骑士Fourze](../Page/假面骑士Fourze.md "wikilink")》 第32集也是在此地取景。
  - 小說《歡迎光臨陰陽屋》中的主角在書中也是在稻荷神社中找到，可以化身為狐狸。
  - 日本动畫《[此花亭绮譚](../Page/此花亭奇譚.md "wikilink")》第12集出現以千本鳥居作为背景的場景。

## 參考文獻

  - [伏見稻荷大社官方網站](http://inari.jp/)

  - [稻荷信仰與京都伏見稻荷大社](http://blog.livedoor.jp/susanowo/archives/50062442.html)

  - [伏見稻荷大社](http://kamnavi.jp/inari/fusimi.html)

  - [伏見稻荷大社（京都的歷史）](https://web.archive.org/web/20070321082114/http://kyotobook.kyotoinfo.jp/?eid=146478)

## 圖片集

<File:FushimiInari> Taisha TopShrine.jpg|位於稻荷山頂、伏見稻荷大社境內高度最高的「上之神社」
<File:Azumamaro> Jinja.jpg|位在伏見稻荷大社境內，但實際上已獨立運作的東丸神社 <File:FushimiInari>
Taisha Haiden.jpg|拜殿正面 <File:FushimiInari> Taisha
Shrine-of-Torii.jpg|稻荷山境內一個用來祭祀「鳥居」的小祭壇 <File:Fushimi-Inari>
Taishi 1.jpg|千本鳥居
[File:KyotoFushimiInariLarge.jpg|千本鳥居](File:KyotoFushimiInariLarge.jpg%7C千本鳥居)

## 相關條目

  - [宇迦之御魂神](../Page/宇迦之御魂神.md "wikilink")
  - [鳥居](../Page/鳥居.md "wikilink")
  - [稻荷神社](../Page/稻荷神社.md "wikilink")
  - [伏见稻荷社家世系图](../Page/伏见稻荷社家世系图.md "wikilink")

## 外部連結

  - [伏見稻荷大社官方網站](http://inari.jp/)

  -
[Category:京都市神社](../Category/京都市神社.md "wikilink")
[Category:名神大社](../Category/名神大社.md "wikilink")
[Category:二十二社](../Category/二十二社.md "wikilink")
[Category:官幣大社](../Category/官幣大社.md "wikilink")
[Category:單立神社](../Category/單立神社.md "wikilink")
[Category:稻荷神社](../Category/稻荷神社.md "wikilink")
[Category:京都市重要文化財](../Category/京都市重要文化財.md "wikilink")
[Category:伏見區](../Category/伏見區.md "wikilink")
[Category:山城國](../Category/山城國.md "wikilink")

1.