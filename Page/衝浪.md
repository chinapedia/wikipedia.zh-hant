[US_Navy_071227-N-1688B-148_A_Sailor_attached_to_the_Nimitz-class_aircraft_carrier_USS_Harry_S._Truman_(CVN_75)_surfs_an_artificial_wave_pool_during_a_USO_water_park_tour_during_a_port_visit_in_the_Middle_East.jpg](https://zh.wikipedia.org/wiki/File:US_Navy_071227-N-1688B-148_A_Sailor_attached_to_the_Nimitz-class_aircraft_carrier_USS_Harry_S._Truman_\(CVN_75\)_surfs_an_artificial_wave_pool_during_a_USO_water_park_tour_during_a_port_visit_in_the_Middle_East.jpg "fig:US_Navy_071227-N-1688B-148_A_Sailor_attached_to_the_Nimitz-class_aircraft_carrier_USS_Harry_S._Truman_(CVN_75)_surfs_an_artificial_wave_pool_during_a_USO_water_park_tour_during_a_port_visit_in_the_Middle_East.jpg")
[Sufer_carrying_surfboard_along_the_beach.JPG](https://zh.wikipedia.org/wiki/File:Sufer_carrying_surfboard_along_the_beach.JPG "fig:Sufer_carrying_surfboard_along_the_beach.JPG")
**衝浪**是一種衝浪者利用[衝浪板越過湧起浪頭激烈水上運動](../Page/衝浪板.md "wikilink")。主要的配備是衝浪板和繫在腳上的安全繩。

## 衝浪的起源

衝浪是[波里尼西亞人的一項古老文化](../Page/波里尼西亞人.md "wikilink")。他們的酋長是部落中技術最好的駕浪者、並擁有使用最好的樹木所製造最好的衝浪板。統治階級並擁有最好的海灘和板子；一般階級的民眾不准進入他們的沙灘，但民眾可以經由優良的衝浪技術而晉升得到這些特權。

歐洲人最早目擊衝浪是1767年由[Dolphin的船員在](../Page/Dolphin.md "wikilink")[大溪地所記錄](../Page/大溪地.md "wikilink")。之後[占士王中尉於](../Page/占士王.md "wikilink")1779年[庫克船長死亡時在庫克船長的日誌亦記載了衝浪的藝術](../Page/庫克船長.md "wikilink")。

衝浪運動是波里尼西亞人的生活、就像今日西方世界的運動一樣。它影響了波里尼西亞的社會、宗教和神話。波里尼西亞酋長們以展現他們在浪上的特技來作為其威信的象徵。

## 著名的衝浪地點

  - [法國吉隆德灣](../Page/法國.md "wikilink")(Gironde)
  - [澳大利亞](../Page/澳大利亞.md "wikilink")[紐塞](../Page/紐塞.md "wikilink")、[黃金海岸](../Page/黃金海岸.md "wikilink")、[悉尼](../Page/悉尼.md "wikilink")[邦迪海灘](../Page/邦迪海灘.md "wikilink")、Jan
    Juc海灘、[貝爾海灘](../Page/貝爾海灘.md "wikilink")(Bells Beach)
  - [秘魯](../Page/秘魯.md "wikilink")
  - [墨西哥](../Page/墨西哥.md "wikilink")
  - [印尼](../Page/印尼.md "wikilink")
  - [愛爾蘭](../Page/愛爾蘭.md "wikilink")[伊斯基](../Page/伊斯基.md "wikilink")(Easkey)
  - [紐西蘭](../Page/紐西蘭.md "wikilink")[瑪努灣](../Page/瑪努灣.md "wikilink")(Manu
    Bay)
  - [南非](../Page/南非.md "wikilink")[德班](../Page/德班.md "wikilink")
  - [英國Fistral](../Page/英國.md "wikilink") Beach、克羅伊德灣(Croyde Bay)
  - [美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")、[夏威夷](../Page/夏威夷.md "wikilink"):[歐胡島北岸](../Page/歐胡島.md "wikilink")(North
    Shore)
  - [日本宮崎ー日向湾](../Page/日本.md "wikilink")
  - [日本](../Page/日本.md "wikilink")[千葉縣](../Page/千葉縣.md "wikilink")[一宮町](../Page/一宮町.md "wikilink")、釣ヶ崎海岸([2020年夏季奥林匹克运动会](../Page/2020年夏季奥林匹克运动会.md "wikilink"))
  - [台灣](../Page/台灣.md "wikilink")[宜蘭烏石港](../Page/宜蘭縣.md "wikilink")、宜蘭蜜月灣、蘇澳龍骨王海灘、花蓮磯崎、台東宜灣、台北金山、台中大安、墾丁南灣、墾丁佳樂水、高雄旗津。
  - [斯里蘭卡Arugam](../Page/斯里蘭卡.md "wikilink") Bay、Weligama Bay

## 衝浪好手

  - 短板
      - [Kelly Slater](../Page/Kelly_Slater.md "wikilink")
      - [Andy Irons](../Page/Andy_Irons.md "wikilink")
      - [Bruce Irons](../Page/Bruce_Irons.md "wikilink")
      - [Rob Machado](../Page/Rob_Machado.md "wikilink")
      - [Jack Johnson](../Page/Surfer_Jack_Johnson.md "wikilink")
  - 長板
      - [Joel Tudor](../Page/Joel_Tudor.md "wikilink")
      - [Bonga Perkins](../Page/Bonga_Perkins.md "wikilink")
      - [Daize Shayne](../Page/Daize_Shayne.md "wikilink")

[冲浪](../Category/冲浪.md "wikilink")
[Category:水上運動](../Category/水上運動.md "wikilink")
[Category:極限運動](../Category/極限運動.md "wikilink")