[MiniSD_memory_card_including_adapter.jpg](https://zh.wikipedia.org/wiki/File:MiniSD_memory_card_including_adapter.jpg "fig:MiniSD_memory_card_including_adapter.jpg")
**miniSD**
首次於2003年的[CeBIT展覽中由](../Page/CeBIT.md "wikilink")[SanDisk公布](../Page/SanDisk.md "wikilink")，自此它加入了[Memory
Stick](../Page/Memory_Stick.md "wikilink")
Duo和[xD卡這類細小的記憶卡規格中](../Page/xD卡.md "wikilink")。

miniSD卡被[SD協會](../Page/SD協會.md "wikilink") (SD Association)
於2003年時確立為標準SD卡的極細小型規格，這種miniSD卡特別設計於[流動電話上](../Page/流動電話.md "wikilink")，並隨卡附上minSD轉接器，令它能夠兼容所有配置了標準[SD卡插槽的設備中](../Page/SD卡.md "wikilink")。

miniSD卡由數間不同的製造商以及引伸出不同品牌的名稱，但他們的兼容性是通行的。

## 規格

|        |                      |                    |                    |
| ------ | -------------------- | ------------------ | ------------------ |
|        | **SD卡**              | **miniSD卡**        | **microSD卡**       |
| 寬度     | 24 mm                | 20 mm              | 11 mm              |
| 長度     | 32 mm                | 21.5 mm            | 15 mm              |
| 厚度     | 2.1 mm               | 1.4 mm             | 1 mm               |
| 體積     | 1,596 mm<sup>3</sup> | 589 mm<sup>3</sup> | 165 mm<sup>3</sup> |
| 重量     | 接近2g                 | 接近1g               | 接近0.5g             |
| 操作電壓   | 2.7 - 3.6V           | 2.7 - 3.6V         | 2.7 - 3.6V         |
| 寫入裝置保護 | YES                  | NO                 | NO                 |
| 終止保護   | YES                  | NO                 | NO                 |
| 接腳數目   | 9 腳                  | 11 腳               | 8 腳                |

## 容量

miniSD卡的容量由16[MB至](../Page/MB.md "wikilink")8[GB](../Page/GB.md "wikilink")，而miniSDHC卡的容量由4GB至16GB。

## 連結

  - [SD協會](http://www.sdcard.org)
  - [SanDisk 公司](http://www.sandisk.com/)
  - [miniSD 轉接器，miniSD卡，Memory Stick Duo 和
    MMC卡的相片](http://img493.imageshack.us/img493/4533/cimg4308custom3kh.jpg)

[en:Secure Digital](../Page/en:Secure_Digital.md "wikilink") [fi:Secure
Digital\#miniSD](../Page/fi:Secure_Digital#miniSD.md "wikilink")

[Category:記憶卡](../Category/記憶卡.md "wikilink")