**[備後國](../Page/備後國.md "wikilink")**的**福山城**現今位於[日本](../Page/日本.md "wikilink")[廣島縣](../Page/廣島縣.md "wikilink")[福山市丸之内1丁目的一座城堡](../Page/福山市.md "wikilink")。日本國家指定史跡，**久松城**（ひさまつじょう）、**葦陽城**（いようじょう）是此城別名。2006年2月13日被選為[日本100名城](../Page/日本100名城.md "wikilink")。在[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，此國寶天守遭到美軍攻擊而燒毀。


[Category:廣島縣城堡](../Category/廣島縣城堡.md "wikilink")
[Category:1874年廢除](../Category/1874年廢除.md "wikilink")
[Category:江戶時代建築](../Category/江戶時代建築.md "wikilink")
[Category:備後福山藩](../Category/備後福山藩.md "wikilink")
[Category:阿部氏](../Category/阿部氏.md "wikilink")
[Category:日本史跡](../Category/日本史跡.md "wikilink")
[Category:廣島縣重要文化財](../Category/廣島縣重要文化財.md "wikilink")
[Category:奧平松平氏](../Category/奧平松平氏.md "wikilink")
[Category:水野氏](../Category/水野氏.md "wikilink")