**锦绣路站**位于[上海](../Page/上海.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[高科西路](../Page/高科西路.md "wikilink")[锦绣路](../Page/锦绣路.md "wikilink")，为[上海轨道交通7号线的地下车站](../Page/上海轨道交通7号线.md "wikilink")，于2009年12月5日启用。

## 周边

## 車站設計

7号线车站为地下[岛式站台](../Page/岛式站台.md "wikilink")。

  - 北侧
      - <span style="color:{{上海地铁标志色|7}};">■</span>7号线（上行）
      - [耀华路](../Page/耀华路站.md "wikilink")、[肇嘉浜路](../Page/肇嘉浜路站.md "wikilink")、[常熟路](../Page/常熟路站.md "wikilink")、[镇坪路](../Page/镇坪路站.md "wikilink")、[美兰湖方向](../Page/美兰湖站.md "wikilink")

<!-- end list -->

  - 南侧
      - <span style="color:{{上海地铁标志色|7}};">■</span>7号线（下行）
      - [龙阳路](../Page/龙阳路站.md "wikilink")、
        [花木路方向](../Page/花木路站.md "wikilink")

## 公交换乘

184、607、639、715、730、771、792、970、976、992、南川线、徐川专线、北蔡1路、花木1路

## 车站出口

  - 1号口：锦绣路东侧，高科西路北
  - 2号口：锦绣路东侧，高科西路南
  - 4号口：锦绣路西侧，高科西路北

## 邻近车站

[Category:浦东新区地铁车站](../Category/浦东新区地铁车站.md "wikilink")
[Category:2009年启用的铁路车站](../Category/2009年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")