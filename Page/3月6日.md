**3月6日**是[公历一年中的第](../Page/公历.md "wikilink")65天（[闰年第](../Page/闰年.md "wikilink")66天），离全年的结束还有300天。

## 大事记

### 7世紀

  - [619年](../Page/619年.md "wikilink")：[中国](../Page/中国.md "wikilink")[唐朝公布](../Page/唐朝.md "wikilink")[租庸调法](../Page/租庸调法.md "wikilink")。

### 16世紀

  - [1521年](../Page/1521年.md "wikilink")：[葡萄牙航海家](../Page/葡萄牙.md "wikilink")[麦哲伦率领的船队到达](../Page/麦哲伦.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")[马里亚纳群岛中的](../Page/马里亚纳群岛.md "wikilink")[关岛](../Page/关岛.md "wikilink")。

### 19世紀

  - [1834年](../Page/1834年.md "wikilink")：[加拿大最大城市](../Page/加拿大.md "wikilink")[多伦多成立](../Page/多伦多.md "wikilink")。
  - [1836年](../Page/1836年.md "wikilink")：[墨西哥将军](../Page/墨西哥.md "wikilink")[桑塔·安纳所率领的军队在围城](../Page/安东尼奥·洛佩斯·德·桑塔·安纳.md "wikilink")13天之后，终于将[德克萨斯的](../Page/德克萨斯.md "wikilink")[阿拉莫攻陷](../Page/阿拉莫_\(得克薩斯州聖安東尼奧\).md "wikilink")。
  - [1853年](../Page/1853年.md "wikilink")：由[意大利作曲家](../Page/意大利.md "wikilink")[朱塞佩·威尔第作曲](../Page/朱塞佩·威尔第.md "wikilink")，改编自[小仲马](../Page/小仲马.md "wikilink")[小说的](../Page/小说.md "wikilink")[歌剧](../Page/歌剧.md "wikilink")《[茶花女](../Page/茶花女_\(歌劇\).md "wikilink")》在[威尼斯首演](../Page/威尼斯.md "wikilink")。
  - [1853年](../Page/1853年.md "wikilink")：[加納獨立日](../Page/加納.md "wikilink")。
  - [1869年](../Page/1869年.md "wikilink")：[俄国化学家](../Page/俄国.md "wikilink")[-{zh-tw:門得列夫;
    zh-cn:门捷列夫;
    zh-hk:門捷列夫;}-向](../Page/門得列夫.md "wikilink")[化学界公开了他的首张](../Page/化学.md "wikilink")[元素周期表](../Page/元素周期表.md "wikilink")。
  - [1898年](../Page/1898年.md "wikilink")：[清政府派](../Page/清政府.md "wikilink")[李鸿章](../Page/李鸿章.md "wikilink")、[翁同龢与](../Page/翁同龢.md "wikilink")[德国驻华](../Page/德国.md "wikilink")[公使](../Page/公使.md "wikilink")[海靖在](../Page/海靖.md "wikilink")[北京正式签订了中德](../Page/北京.md "wikilink")《[胶澳租界条约](../Page/胶澳租界条约.md "wikilink")》。
  - [1899年](../Page/1899年.md "wikilink")：[阿斯匹靈獲准上市](../Page/阿斯匹靈.md "wikilink")。

### 20世紀

  - [1932年](../Page/1932年.md "wikilink")：[蔣中正出任](../Page/蔣中正.md "wikilink")[國民政府](../Page/國民政府.md "wikilink")[軍事委員會](../Page/軍事.md "wikilink")[委員長](../Page/委員長.md "wikilink")
  - [1933年](../Page/1933年.md "wikilink")：[长城事变](../Page/长城事变.md "wikilink")：[喜峰口战役](../Page/長城戰役#.E5.96.9C.E5.B3.B0.E5.8F.A3.md "wikilink")
  - [1937年](../Page/1937年.md "wikilink")：[老舍发表](../Page/老舍.md "wikilink")[长篇小说](../Page/长篇小说.md "wikilink")《[骆驼祥子](../Page/骆驼祥子.md "wikilink")》。
  - [1945年](../Page/1945年.md "wikilink")：[美国与](../Page/美国.md "wikilink")[拉美国家缔结](../Page/拉美.md "wikilink")[查普特佩克公约](../Page/查普特佩克公约.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[德国军队向](../Page/德国.md "wikilink")[匈牙利](../Page/匈牙利.md "wikilink")[巴拉顿湖地区的](../Page/巴拉顿湖.md "wikilink")[苏联](../Page/苏联.md "wikilink")[乌克兰第三方面军展开大规模的进攻](../Page/乌克兰第三方面军.md "wikilink")，[巴拉顿湖战役爆发](../Page/巴拉顿湖战役.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[中国民主促进会成立](../Page/中国民主促进会.md "wikilink")
  - [1949年](../Page/1949年.md "wikilink")：中国[中华全国学生联合会成立](../Page/中华全国学生联合会.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：[中華民國照會](../Page/中華民國.md "wikilink")[聯合國秘書長](../Page/聯合國秘書長.md "wikilink")，退出[關稅與貿易總協定](../Page/關稅與貿易總協定.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[英屬黃金海岸宣布独立](../Page/英屬黃金海岸.md "wikilink")，改名为[加纳](../Page/加纳.md "wikilink")。
  - [1967年](../Page/1967年.md "wikilink")：[-{zh-hans:斯大林;zh-hk:史太林;zh-tw:史達林;}-的女兒叛逃至美國](../Page/史達林.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[香港](../Page/香港.md "wikilink")[財政司](../Page/財政司.md "wikilink")[翟克誠建議大幅增加](../Page/翟克誠.md "wikilink")[煙草稅](../Page/香港禁煙情況#.E9.A6.99.E6.B8.AF.E7.85.99.E8.8D.89.E7.A8.85.md "wikilink")200%。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：香港[美孚新邨一間銀行發生人肉炸彈案](../Page/美孚新邨.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：香港爆發[反財政預算案大遊行](../Page/反財政預算案大遊行.md "wikilink")，有示威者堵塞[德輔道中](../Page/德輔道中.md "wikilink")，當中113名被捕，是繼[六七暴動後捕獲最多人的一次](../Page/六七暴動.md "wikilink")。

## 出生

  - [1340年](../Page/1340年.md "wikilink")：[岡特的約翰](../Page/岡特的約翰.md "wikilink")，英格蘭國王愛德華三世的兒子（逝於[1399年](../Page/1399年.md "wikilink")）
  - [1405年](../Page/1405年.md "wikilink")：[胡安二世
    (卡斯蒂利亞)](../Page/胡安二世_\(卡斯蒂利亞\).md "wikilink")，[卡斯蒂利亞王國國王](../Page/卡斯蒂利亞王國.md "wikilink")（逝於[1454年](../Page/1454年.md "wikilink")）
  - [1475年](../Page/1475年.md "wikilink")：[米开朗基罗](../Page/米开朗基罗.md "wikilink")，[意大利雕塑家](../Page/意大利.md "wikilink")，文藝復興時期的建築師（逝於[1564年](../Page/1564年.md "wikilink")）
  - [1483年](../Page/1483年.md "wikilink")：[弗朗切斯科·圭恰迪尼](../Page/弗朗切斯科·圭恰迪尼.md "wikilink")，[義大利歷史學家](../Page/義大利.md "wikilink")、政治家（逝於[1540年](../Page/1540年.md "wikilink")）
  - [1495年](../Page/1495年.md "wikilink")：[路易吉·阿拉曼尼](../Page/路易吉·阿拉曼尼.md "wikilink")，[文藝復興時期歐洲](../Page/文藝復興時期.md "wikilink")[佛羅倫斯](../Page/佛羅倫斯.md "wikilink")[人文主義者](../Page/人文主義者.md "wikilink")（逝於[1556年](../Page/1556年.md "wikilink")）
  - [1779年](../Page/1779年.md "wikilink")：[安東莞·亨利·約米尼](../Page/安東莞·亨利·約米尼.md "wikilink")，[法國](../Page/法國.md "wikilink")[拿破崙時期的將軍及軍事家](../Page/拿破崙.md "wikilink")（逝於[1869年](../Page/1869年.md "wikilink")）
  - [1787年](../Page/1787年.md "wikilink")：[约瑟夫·夫琅禾费](../Page/约瑟夫·夫琅禾费.md "wikilink")，[德国物理学家](../Page/德国.md "wikilink")（逝於[1826年](../Page/1826年.md "wikilink")）
  - [1806年](../Page/1806年.md "wikilink")：[伊丽莎白·勃朗宁](../Page/伊丽莎白·巴雷特·勃朗宁.md "wikilink")，[英国女诗人](../Page/英国.md "wikilink")（逝於[1861年](../Page/1861年.md "wikilink")）
  - [1831年](../Page/1831年.md "wikilink")：[菲利普·謝里登](../Page/菲利普·謝里登.md "wikilink")，[美國陸軍職業軍官](../Page/美國.md "wikilink")、[南北戰爭時期聯邦軍將領](../Page/南北戰爭.md "wikilink")（逝於[1888年](../Page/1888年.md "wikilink")）
  - [1865年](../Page/1865年.md "wikilink")：[段祺瑞](../Page/段祺瑞.md "wikilink")，[民國時期](../Page/民國時期.md "wikilink")[政治家](../Page/政治家.md "wikilink")，[皖系軍閥首領](../Page/皖系軍閥.md "wikilink")（逝於[1936年](../Page/1936年.md "wikilink")）
  - [1870年](../Page/1870年.md "wikilink")：[奧斯卡·施特勞斯](../Page/奧斯卡·施特勞斯.md "wikilink")，[奧地利作曲家](../Page/奧地利.md "wikilink")（逝於[1954年](../Page/1954年.md "wikilink")）
  - [1900年](../Page/1900年.md "wikilink")：[吉娜·西格娜](../Page/吉娜·西格娜.md "wikilink")，[義大利](../Page/義大利.md "wikilink")-[法國出生](../Page/法國.md "wikilink")[歌劇聲樂家](../Page/歌劇.md "wikilink")，戲劇[女高音](../Page/女高音.md "wikilink")（逝於[2001年](../Page/2001年.md "wikilink")）
  - [1903年](../Page/1903年.md "wikilink")：[香淳皇后](../Page/香淳皇后.md "wikilink")，[昭和天皇的皇后](../Page/昭和天皇.md "wikilink")（逝於[2000年](../Page/2000年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[鮑勃·威爾斯](../Page/鮑勃·威爾斯.md "wikilink")，美國歌手、作曲家，西部[搖擺樂先驅之一](../Page/搖擺樂.md "wikilink")（逝於[1975年](../Page/1975年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[唐納德·戴維森](../Page/唐納德·戴維森.md "wikilink")，美國[哲學家](../Page/哲學家.md "wikilink")。（逝於[2003年](../Page/2003年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：[威爾·艾斯納](../Page/威爾·艾斯納.md "wikilink")，美國[漫畫家](../Page/漫畫家.md "wikilink")、[編劇](../Page/編劇.md "wikilink")、企業家（逝於[2005年](../Page/2005年.md "wikilink")）
  - [1917年](../Page/1917年.md "wikilink")：，[美國發明家](../Page/美國.md "wikilink")、[瞬間膠發明人](../Page/瞬間膠.md "wikilink")（逝於[2011年](../Page/2011年.md "wikilink")）
  - [1926年](../Page/1926年.md "wikilink")：[安德烈·華依達](../Page/安德烈·華依達.md "wikilink")，[波蘭電影導演](../Page/波蘭.md "wikilink")（逝于[2016年](../Page/2016年.md "wikilink")）
  - [1926年](../Page/1926年.md "wikilink")：[-{zh-hans:阿伦·格林斯潘;zh-hk:艾倫·格林斯潘;zh-tw:艾倫·葛林斯潘;}-](../Page/艾倫·葛林斯潘.md "wikilink")，[美國经济学家](../Page/美國.md "wikilink")
  - [1927年](../Page/1927年.md "wikilink")：[-{zh-hans:加西亚·马尔克斯;
    zh-hant:賈西亞·馬奎斯;}-](../Page/加西亚·马尔克斯.md "wikilink")，[哥倫比亞](../Page/哥倫比亞.md "wikilink")[文學家](../Page/文學家.md "wikilink")、[記者和](../Page/記者.md "wikilink")[社會活動家](../Page/社會活動家.md "wikilink")（逝於[2014年](../Page/2014年.md "wikilink")）
  - [1930年](../Page/1930年.md "wikilink")：[洛林·马泽尔](../Page/洛林·马泽尔.md "wikilink")，[法國](../Page/法國.md "wikilink")[指揮家](../Page/指揮家.md "wikilink")、[作曲家](../Page/作曲家.md "wikilink")（逝於[2014年](../Page/2014年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[哈爾·尼達姆](../Page/哈爾·尼達姆.md "wikilink")，美國男演員（逝於[2013年](../Page/2013年.md "wikilink")）
  - [1933年](../Page/1933年.md "wikilink")：[奧古斯都·奧登](../Page/奧古斯都·奧登.md "wikilink")，義大利經濟學家（逝於[2013年](../Page/2013年.md "wikilink")）
  - [1937年](../Page/1937年.md "wikilink")：[范倫蒂娜·泰勒斯可娃](../Page/范倫蒂娜·泰勒斯可娃.md "wikilink")，[俄國](../Page/俄國.md "wikilink")[太空人](../Page/太空人.md "wikilink")，人類歷史上第一位女性太空人
  - [1940年](../Page/1940年.md "wikilink")：[曾秋坤](../Page/曾秋坤.md "wikilink")，[英國](../Page/英國.md "wikilink")[華人](../Page/華人.md "wikilink")[貴族](../Page/貴族.md "wikilink")（逝於[2006年](../Page/2006年.md "wikilink")）
  - [1950年](../Page/1950年.md "wikilink")：[鈴置洋孝](../Page/鈴置洋孝.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")（逝於[2006年](../Page/2006年.md "wikilink")）
  - [1954年](../Page/1954年.md "wikilink")：[中山星香](../Page/中山星香.md "wikilink")，[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")
  - [1955年](../Page/1955年.md "wikilink")：[西普里安·恩塔里亞米拉](../Page/西普里安·恩塔里亞米拉.md "wikilink")，[布隆迪政治家](../Page/布隆迪.md "wikilink")（逝於[1994年](../Page/1994年.md "wikilink")）
  - [1957年](../Page/1957年.md "wikilink")：[林慕德](../Page/林慕德.md "wikilink")，香港音樂人及教育學者
  - [1959年](../Page/1959年.md "wikilink")：[延斯·斯托尔滕贝格](../Page/延斯·斯托尔滕贝格.md "wikilink")，[挪威首相](../Page/挪威首相.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[盧慶輝](../Page/盧慶輝.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[-{zh-hans:沙奎尔·奥尼尔;zh-hk:沙基爾·奧尼爾;zh-tw:俠客·歐尼爾;}-](../Page/沙奎尔·奥尼尔.md "wikilink")，[美國篮球选手](../Page/美國.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：，[日本樂團](../Page/日本.md "wikilink")[Plastic
    Tree主唱](../Page/Plastic_Tree.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[郭人豪](../Page/郭人豪.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[關悅](../Page/關悅.md "wikilink")，[中國內地女演員](../Page/中國.md "wikilink")、主持人、歌手
  - [1981年](../Page/1981年.md "wikilink")：[張萌](../Page/張萌.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[西瓜哥哥](../Page/西瓜哥哥.md "wikilink")，台灣主持人
  - [1982年](../Page/1982年.md "wikilink")：[妮姆拉·卡爾](../Page/妮姆拉·卡爾.md "wikilink")，[印度女演員](../Page/印度.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[談美琪](../Page/談美琪.md "wikilink")，[香港新聞從業員](../Page/香港.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[修杰楷](../Page/修杰楷.md "wikilink")，[台灣男演員](../Page/台灣.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[Becky](../Page/Becky_\(日本藝人\).md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[林德信](../Page/林德信.md "wikilink")，[香港男歌手](../Page/香港.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[若菜光](../Page/若菜光.md "wikilink")，[日本AV女優](../Page/日本.md "wikilink")
  - [1985年](../Page/1985年.md "wikilink")：[唐嘉麟](../Page/唐嘉麟.md "wikilink")，[香港饒舌](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[弗朗西斯科·瑟維里](../Page/弗朗西斯科·瑟維里.md "wikilink")，現為[紐約洋基隊的](../Page/紐約洋基.md "wikilink")[捕手](../Page/捕手.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[艾力·馬倫斯奧](../Page/艾力·馬倫斯奧.md "wikilink")，[美國男演員](../Page/美國.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[凱文-普林斯·博阿騰](../Page/凱文-普林斯·博阿騰.md "wikilink")，[迦納足球員](../Page/迦納.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[西蒙·米尼奧萊](../Page/西蒙·米尼奧萊.md "wikilink")，[比利時足球運動員](../Page/比利時.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[阿格涅什卡·拉德萬斯卡](../Page/阿格涅什卡·拉德萬斯卡.md "wikilink")，[波蘭女子網球運動員](../Page/波蘭.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：[周放](../Page/周放.md "wikilink")：[中國內地女演員](../Page/中國.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[朴草娥](../Page/朴草娥.md "wikilink")，韓國女子團體[AOA前成員](../Page/AOA.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[妮歌·霍斯](../Page/妮歌·霍斯.md "wikilink")，[美國時尚模特兒](../Page/美國.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[嗣永桃子](../Page/嗣永桃子.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[Luda](../Page/Luda.md "wikilink")，韓國組合[宇宙少女成員](../Page/宇宙少女.md "wikilink")

## 逝世

  - [190年](../Page/190年.md "wikilink")：[刘辩](../Page/刘辩.md "wikilink")，[东汉废帝](../Page/东汉.md "wikilink")、弘农王（[176年出生](../Page/176年.md "wikilink")）
  - [753年](../Page/753年.md "wikilink")：[高阳公主](../Page/高阳公主.md "wikilink")，[唐太宗李世民的女儿](../Page/唐太宗.md "wikilink")（生年不详）
  - [1754年](../Page/1754年.md "wikilink")：[亨利·佩勒姆](../Page/亨利·佩勒姆.md "wikilink")，英國輝格黨政治家（[1694年出生](../Page/1694年.md "wikilink")）
  - [1764年](../Page/1764年.md "wikilink")：[菲利普·約克，第一代哈德威克伯爵](../Page/菲利普·約克，第一代哈德威克伯爵.md "wikilink")，英國大律師、法官及輝格黨政治家（[1690年出生](../Page/1690年.md "wikilink")）
  - [1836年](../Page/1836年.md "wikilink")：[大衛·克拉克](../Page/大衛·克拉克.md "wikilink")，美國政治家（[1786年出生](../Page/1786年.md "wikilink")）
  - [1888年](../Page/1888年.md "wikilink")：[露意莎·奧爾柯特](../Page/露意莎·奧爾柯特.md "wikilink")，美國小說家（[1832年出生](../Page/1832年.md "wikilink")）
  - [1900年](../Page/1900年.md "wikilink")：[戈特利布·戴姆勒](../Page/戈特利布·戴姆勒.md "wikilink")，[德國發明家](../Page/德國.md "wikilink")（[1834年出生](../Page/1834年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[-{zh-hant:蘇沙;
    zh-hans:苏萨;}-](../Page/約翰·菲利普·蘇沙.md "wikilink")，[美國軍人](../Page/美國.md "wikilink")、作曲家、指揮家，被稱為「進行曲之王」（[1854年出生](../Page/1854年.md "wikilink")）
  - [1939年](../Page/1939年.md "wikilink")：[費迪南德·馮·林德曼](../Page/費迪南德·馮·林德曼.md "wikilink")，德國數學家（[1852年出生](../Page/1852年.md "wikilink")）
  - [1967年](../Page/1967年.md "wikilink")：[柯达伊](../Page/柯达伊.md "wikilink")，[匈牙利音樂家](../Page/匈牙利.md "wikilink")（[1882年出生](../Page/1882年.md "wikilink")）
  - [1973年](../Page/1973年.md "wikilink")：[賽珍珠](../Page/賽珍珠.md "wikilink")，美國女作家，[1938年獲](../Page/1938年.md "wikilink")[諾貝爾文學獎](../Page/諾貝爾文學獎.md "wikilink")（[1892年出生](../Page/1892年.md "wikilink")）
  - [1982年](../Page/1982年.md "wikilink")：[艾茵·蘭德](../Page/艾茵·蘭德.md "wikilink")，作家和哲學家（[1905年出生](../Page/1905年.md "wikilink")）
  - [1986年](../Page/1986年.md "wikilink")：[朱光潛](../Page/朱光潛.md "wikilink")，[中國美學家](../Page/中國.md "wikilink")、文藝理論家、教育家（[1897年出生](../Page/1897年.md "wikilink")）
  - [1986年](../Page/1986年.md "wikilink")：[喬治亞·歐姬芙](../Page/喬治亞·歐姬芙.md "wikilink")，美國女畫家（[1887年出生](../Page/1887年.md "wikilink")）
  - [1992年](../Page/1992年.md "wikilink")：[關肅霜](../Page/關肅霜.md "wikilink")，[京劇表演藝術家](../Page/京劇.md "wikilink")（[1929年出生](../Page/1929年.md "wikilink")）
  - [2005年](../Page/2005年.md "wikilink")：[漢斯·貝特](../Page/漢斯·貝特.md "wikilink")，美國物理學家，1967年[諾貝爾物理學獎獲得者](../Page/諾貝爾物理學獎.md "wikilink")（[1906年出生](../Page/1906年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[魏璐詩](../Page/魏璐詩.md "wikilink")，[中國政協委員](../Page/中國政協.md "wikilink")
  - [2006年](../Page/2006年.md "wikilink")：，已故美國影星[克里斯多夫·李維遺孀](../Page/克里斯多夫·李維.md "wikilink")（[1961年出生](../Page/1961年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[尚·布希亞](../Page/尚·布希亞.md "wikilink")，社會學家及哲學家（[1929年出生](../Page/1929年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[廖欽福](../Page/廖欽福.md "wikilink")，福華飯店集團創辦人
  - [2011年](../Page/2011年.md "wikilink")：[布培](../Page/布培.md "wikilink")，香港前地政總署署長（[1946年出生](../Page/1946年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[阿爾文·李](../Page/阿爾文·李.md "wikilink")，英國吉他手暨歌手（[1944年出生](../Page/1944年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[翁大銘](../Page/翁大銘.md "wikilink")，台灣商界與股市聞人、華隆集團前負責人（[1950年出生](../Page/1950年.md "wikilink")）

## 節日、風俗習慣

  - ：[獨立日](../Page/獨立日.md "wikilink")