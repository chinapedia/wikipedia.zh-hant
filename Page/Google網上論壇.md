**Google
網上論壇**（）是[Google提供的在线交流工具](../Page/Google.md "wikilink")，在[Web界面上类似于常见的](../Page/Web.md "wikilink")[网络论坛](../Page/网络论坛.md "wikilink")。但Google
Groups的实质却可以理解为一个[邮件列表](../Page/邮件列表.md "wikilink")。但与传统的邮件列表相比，它提供了基于[Web的更多功能](../Page/Web.md "wikilink")。如共享文件、创建页面、帖子评分等。

## 特色

  - [Usenet存档](../Page/Usenet.md "wikilink")
      -
        Google
        Groups提供了自从1981年以来的[Usenet帖子存档](../Page/Usenet.md "wikilink")\[1\]，保存了许多珍贵史料\[2\]；
  - [HTTPS加密连接](../Page/HTTPS.md "wikilink")
      -
        同[Gmail等](../Page/Gmail.md "wikilink")[Google服务一样](../Page/Google.md "wikilink")，Google
        Groups允许通过[HTTPS协议进行连接](../Page/HTTPS.md "wikilink")，这对处于较为严格的[网络审查之下的中国大陆网民尤为重要](../Page/中华人民共和国网络审查.md "wikilink")。

## 中国大陆访问

很长一段时间里，Google网上论坛不论是明文HTTP访问还是加密[SSL访问](../Page/SSL.md "wikilink")，均会遭到中国官方的防火长城以连接R重置的技术手段屏蔽。但在2013年，中国用户可以较为顺畅地通过HTTPS访问该网站。

## 參考資料

## 参见

  - [Usenet](../Page/Usenet.md "wikilink")
  - [Google产品列表](../Page/Google产品列表.md "wikilink")

## 外部链接

  - [Google Groups](http://groups.google.com/?hl=en)
      - [Google网上论坛](http://groups.google.com/?hl=zh-CN)
      - [Google网上论坛](http://groups.google.com/?hl=zh-TW)
      - [Https安全连接](https://groups.google.com/)

[Category:Google](../Category/Google.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")

1.
2.