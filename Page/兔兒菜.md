**中华小苦荬**（学名：**），俗名**兔兒菜**、**兔仔菜**、**鵝仔菜**、**中华苦藚菜**、**山苦買**，是[菊科](../Page/菊科.md "wikilink")[小苦荬属的](../Page/小苦荬属.md "wikilink")[植物](../Page/植物.md "wikilink")，其[花為黃色](../Page/花.md "wikilink")，常被用作[中藥藥材](../Page/中藥.md "wikilink")，具有止瀉消腫、活血等功能。

## 别名

小苦苣，黄鼠草，山苦荬

## 分佈

分布于[日本](../Page/日本.md "wikilink")、[朝鲜](../Page/朝鲜.md "wikilink")、[台湾岛](../Page/台湾岛.md "wikilink")、[俄罗斯以及](../Page/俄罗斯.md "wikilink")[中国大陆的](../Page/中国大陆.md "wikilink")[贵州](../Page/贵州.md "wikilink")、[福建](../Page/福建.md "wikilink")、[河北](../Page/河北.md "wikilink")、[河南](../Page/河南.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[山东](../Page/山东.md "wikilink")、[江西](../Page/江西.md "wikilink")、[黑龙江](../Page/黑龙江.md "wikilink")、[四川](../Page/四川.md "wikilink")、[山西](../Page/山西.md "wikilink")、[江苏](../Page/江苏.md "wikilink")、[云南](../Page/云南.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[浙江等地](../Page/浙江.md "wikilink")，生长于海拔230米至4,700米的地区，见于田野、山坡路旁、河边灌丛及岩石缝隙中，目前尚未由人工引种栽培。

## 参考文献

## 外部連結

  -

[chinensis](../Category/小苦荬属.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")
[Category:菊科](../Category/菊科.md "wikilink")