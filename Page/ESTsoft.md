**ESTsoft**于1992年建立，是一家[韩国的](../Page/韩国.md "wikilink")[应用软件和解决方案开发公司](../Page/应用软件.md "wikilink")。它的软件包括从最终用户的[PC工具和](../Page/PC.md "wikilink")[应用程序到企业的成套商业解决方案](../Page/应用程序.md "wikilink")。

## 产品

有很多ALTools产品。"Al"在韩语中是"蛋"的意思，因此"ALTools"就是"蛋工具"的意思。很多的此类产品都有蛋或者“蛋头”作为关键的按钮图标甚至吉祥物。ALTools产品包括：

  - [ALZip](../Page/ALZip.md "wikilink") -
    一个压缩归档工具，目前支持超过[36种格式](http://www.altools.net/ALTools/ALZip/Features/Open36Formats/tabid/99/Default.aspx)，包括[ISO](../Page/ISO.md "wikilink")。
  - [ALFTP](../Page/ALFTP.md "wikilink") -
    一个[FTP客户端和服务器](../Page/FTP.md "wikilink")。
  - [ALSee](../Page/ALSee.md "wikilink") - 一个图片查看器和数码照片编辑器。
  - [ALPass](../Page/ALPass.md "wikilink") - 一个网站的安全密码管理器。
  - ALGIF - 一个[GIF动画程序](../Page/GIF.md "wikilink")。
  - [ALSong](../Page/ALSong.md "wikilink") -
    一个[MP3播放器](../Page/MP3.md "wikilink")，支持在线歌词。
  - [ALShow](../Page/ALShow.md "wikilink") -
    一个[媒体播放器](../Page/媒体播放器.md "wikilink")
  - ALMap - 一系列数字地图查看器以及[GPS](../Page/GPS.md "wikilink")（包括硬件和软件）
  - ALX - 一个为内容提供商设计的数字版权管理器（[DRM](../Page/DRM.md "wikilink")）解决方案
  - [ALToolbar](../Page/ALToolbar.md "wikilink") - 一个[Internet
    Explorer插件](../Page/Internet_Explorer.md "wikilink")，包含一些上网的工具。

其他来自ESTsoft的产品和解决方案包括：

  - InternetDISK - [在线存储解决方案](../Page/在线存储.md "wikilink")
  - iDisk -
    一个高可扩展的InternetDISK版本，设计用来让[ISP能给为用户提供](../Page/ISP.md "wikilink")[TB或](../Page/TB.md "wikilink")[PB的上传存储能力](../Page/PB.md "wikilink")。
  - iMan - [即时通讯](../Page/即时通讯.md "wikilink")（IM）软件和解决方案
  - [Cabal](../Page/Cabal_Online.md "wikilink") -
    一个[PC平台上的大型多人在线角色扮演游戏](../Page/PC.md "wikilink")（[MMORPG](../Page/MMORPG.md "wikilink")）

## 外部链接

  - [ESTsoft网站](https://web.archive.org/web/20061107143659/http://www.estsoft.com/en/)
  - [ALTools网站（英文）](https://web.archive.org/web/20180604011117/http://www.altools.com/)
  - [ALTools网站（韩文）](http://www.altools.co.kr/)
  - [ALTools网站（日文）](https://web.archive.org/web/20080621050129/http://www.altools.jp/)
  - [CabalOnline网站](http://www.cabalonline.net/)

[Category:韓國公司](../Category/韓國公司.md "wikilink")