[Classical_spectacular_laser_effects.jpg](https://zh.wikipedia.org/wiki/File:Classical_spectacular_laser_effects.jpg "fig:Classical_spectacular_laser_effects.jpg")
**电灯**是人造[光源之一](../Page/光源.md "wikilink")，是一种将[電能转化为](../Page/電能.md "wikilink")[光能的設備](../Page/光能.md "wikilink")，用于指示或者照明。其中，用[电流把常常以](../Page/电流.md "wikilink")[鎢製成的灯丝加热到](../Page/鎢.md "wikilink")[白炽状态后用来发光的灯叫](../Page/白炽.md "wikilink")[白炽灯](../Page/白炽灯.md "wikilink")。灯泡内填充了卤素和惰性气体的白炽灯称为[卤素灯](../Page/卤素灯.md "wikilink")。

## 发展

1854年[亨利·戈培爾使用一根炭化的](../Page/亨利·戈培爾.md "wikilink")[竹絲](../Page/竹.md "wikilink")，放在[真空的玻璃瓶下通電發光](../Page/真空.md "wikilink")\[1\]。而[美國人湯馬士](../Page/美國.md "wikilink")·愛迪生繼此改良出鎢絲燈泡，但發明電燈的人並不是他。熾燈泡的改进从未停止，例如可以將紅外線反射回燈絲、以減少能耗\[2\]；还可以利用[雷射處理](../Page/雷射.md "wikilink")[鎢絲来節省能源](../Page/鎢.md "wikilink")\[3\]。

## 种类

### 接口

### 原理

<table>
<thead>
<tr class="header">
<th><p>种类</p></th>
<th><p>功率范围（<a href="../Page/W.md" title="wikilink">W</a>）</p></th>
<th><p><a href="../Page/發光效率.md" title="wikilink">發光效率</a> （lm/W）</p></th>
<th><p>节能程度</p></th>
<th><p>节能等级</p></th>
<th><p>造价</p></th>
<th><p>寿命</p></th>
<th><p>可调性</p></th>
<th><p>演色性</p></th>
<th><p>图片</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/白炽灯.md" title="wikilink">白炽灯</a></p></td>
<td><p>40-60</p></td>
<td><p>大约12[4]</p></td>
<td><p>—</p></td>
<td><p>D–G</p></td>
<td><p>低</p></td>
<td><p>短</p></td>
<td><p>是</p></td>
<td><p>至 100</p></td>
<td><p><a href="../Page/file:Gluehlampe_01_KMJ.jpg.md" title="wikilink">100px</a> mit E27-Sockel, 60 W]]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/卤素灯泡.md" title="wikilink">卤素灯泡</a></p></td>
<td><p>7–60</p></td>
<td><p>15–27 [5]</p></td>
<td><p>至30 %</p></td>
<td><p>B–F</p></td>
<td><p>低</p></td>
<td><p>中</p></td>
<td><p>是</p></td>
<td><p>至100</p></td>
<td><p><a href="../Page/file:_Glühlampe_42W_230V_E27_Effi_Klasse_C.jpg.md" title="wikilink">100px</a>-<a href="../Page/Xenon.md" title="wikilink">Xenon</a>-Lampe mit E27-Sockel, 42 W - blauer Kolben korrektur-filtert zu Tageslicht]]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/节能灯.md" title="wikilink">节能灯</a>、<a href="../Page/荧光灯.md" title="wikilink">荧光灯</a></p></td>
<td><p>5–15</p></td>
<td><p>40–65</p></td>
<td><p>至 80 %</p></td>
<td><p>A–B</p></td>
<td><p>高</p></td>
<td><p>长</p></td>
<td><p>很少[6]</p></td>
<td><p>70…84</p></td>
<td><p><a href="../Page/file:Energiesparlampe_01a.jpg.md" title="wikilink">100px</a> mit <a href="../Page/Lampensockel#Edisonsockel.md" title="wikilink">E27-Sockel</a>]]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金屬鹵化物燈.md" title="wikilink">金屬鹵化物燈</a></p></td>
<td><p>20–400 (家用)</p></td>
<td><p>50–117</p></td>
<td><p>超过85 %</p></td>
<td><p>—</p></td>
<td><p>高</p></td>
<td><p>中</p></td>
<td><p> </p></td>
<td><p>60…95</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LED灯.md" title="wikilink">LED灯</a></p></td>
<td><p>3–20</p></td>
<td><p>普通: 27[7]-80[8]<br />
最多超过150[9]</p></td>
<td><p>超过90 %</p></td>
<td><p>A[10][11]</p></td>
<td><p>高</p></td>
<td><p>很长</p></td>
<td><p>大部分[12]</p></td>
<td><p>80…95</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Led-lampa.jpg" title="fig:Led-lampa.jpg">Led-lampa.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鈉燈.md" title="wikilink">鈉燈</a></p></td>
<td><p>35–1000</p></td>
<td><p>100–200</p></td>
<td><p>超过95 %</p></td>
<td><p>—</p></td>
<td><p>中</p></td>
<td><p>长</p></td>
<td><p>部分</p></td>
<td><p>18…30</p></td>
<td><p><a href="../Page/file:_Natriumdampfhochdrucklampe-chtaube050404.jpg.md" title="wikilink">100px</a></p></td>
</tr>
</tbody>
</table>

### 用途

电灯的用途：

  - [交通信号灯](../Page/交通信号灯.md "wikilink")
  - [手电筒](../Page/手电筒.md "wikilink")
  - [發光二極管](../Page/發光二極管.md "wikilink")
  - [高壓氣體放電燈](../Page/高壓氣體放電燈.md "wikilink")
  - [灯笼](../Page/灯笼.md "wikilink")
  - [格柵燈](../Page/格柵燈.md "wikilink")
  - [檯燈](../Page/檯燈.md "wikilink")
  - [霓虹灯](../Page/霓虹灯.md "wikilink")
  - [灭蚊灯](../Page/灭蚊灯.md "wikilink")

## 「電燈泡」的其他含义

  - 人们将出現在[情侶之間的閒雜人等稱為](../Page/情侶.md "wikilink")「電燈泡」。说法有二，一是源於舊時真空的電燈泡，即「不通氣」；二是因为恋爱是隐私的事情，通常情侣谈恋爱是在夜间或较昏暗的地方进行，不喜欢有过于明亮的事物出现。
  - [漫畫常以電燈泡發光的小圖示](../Page/漫畫.md "wikilink")，作為「發現了！」、「有靈感了！」的具體化。
  - 有時人們也用電燈泡代稱[禿頭人士](../Page/禿頭.md "wikilink")。

## 参见

  - [灯](../Page/灯.md "wikilink")
  - [照明](../Page/照明.md "wikilink")

## 参考文献

[+](../Page/category:電光源.md "wikilink")

[Category:照明](../Category/照明.md "wikilink")
[Category:灯](../Category/灯.md "wikilink")
[Category:電子元件](../Category/電子元件.md "wikilink")

1.

2.  [Halogená Energy
    Saver](http://www.lighting.philips.com/us_en/products/halogena_energy_saver/index.php?main=us_en_consumer_lighting&parent=7593748565&id=us_en_products&lang=en)


3.  [Laser boosts light bulb
    efficiency](http://physicsworld.com/cws/article/news/39362)

4.  Martin Schäfer: [Das Leben nach der Glühbirne: Warmes und diffuses
    Licht](http://www.stuttgarter-zeitung.de/inhalt.organische-leuchtdioden-als-zukunftsvision-beleuchtung-wie-durchs-fenster.520d4a9f-4133-4b02-9c28-968fee54f331.html),
    Stuttgarter Zeitung, 2. September 2009

5.  [Osram Familiendatenblatt HALOSTAR
    ECO](http://www.osram.de/appsinfo/pdc/pdf.do?cid=GPS01_1027719&vid=EU_ALL_eCat&lid=DE)

6.  Prinzipiell sind nur wenige Leuchtstofflampen (\~15 %) aber viele
    LED-Lampen (\~45 %) mittels
    [Phasenabschnitt](../Page/Phasenabschnitt.md "wikilink") dimmbar. Ob
    das mit einer gegebenen Lampe möglich ist, hängt von deren internem
    [Vorschaltgerät](../Page/Vorschaltgerät.md "wikilink") ab.

7.

8.  Lampen mittlerer bis schlechter Qualität, vgl. Calderon et al, *LED
    bulbs technical specification and testing procedure for solar home
    systems*. In: *[Renewable and Sustainable Energy
    Reviews](../Page/Renewable_and_Sustainable_Energy_Reviews.md "wikilink")*
    41, (2015), 506–520,
    [<doi:10.1016/j.rser.2014.08.057>](../Page/doi:10.1016/j.rser.2014.08.057.md "wikilink").

9.  [Vincenzo Balzani](../Page/Vincenzo_Balzani.md "wikilink"), Giacomo
    Bergamini und Paola Ceroni, *Light: A Very Peculiar Reactant and
    Product*. In: *[Angewandte Chemie International
    Edition](../Page/Angewandte_Chemie_\(Zeitschrift\).md "wikilink")*
    54, Issue 39, (2015), 11320–11337,
    [<doi:10.1002/anie.201502325>](../Page/doi:10.1002/anie.201502325.md "wikilink").

10. [ErP – die zweite Stufe der gesetzlichen Regelung für den Weg in
    eine effizientere Zukunft steht vor der
    Tür\!](http://www.lighting.philips.at/pwc_li/de_de/connect/ErP/Assets/OnePager_ErP.pdf)
    , Änderungen ab 2010

11. [Ratgeber LED-Lampen und
    LED-Spots](http://www.topten.ch/deutsch/ratgeber/rec_led.html) ,
    topten.ch, abgerufen am 22. April 2012.

12.