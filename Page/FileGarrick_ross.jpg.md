## 摘要

| 描述摘要 | Promotional image for the cover to JSA \# 78 (December 2005). Art by Alex Ross.                |
| ---- | ---------------------------------------------------------------------------------------------- |
| 來源   | <http://www.newsarama.com/dcnew/Oct05/DCUOct05.html>                                           |
| 日期   | 16 JUNE 2007                                                                                   |
| 作者   | DC comics                                                                                      |
| 許可   | All images properly illustrating the character are owned by DC Comics. {{\#switch: 檔案其他版本（可留空） |

## 许可协议

[en:<File:Garrick>
ross.jpg](../Page/en:File:Garrick_ross.jpg.md "wikilink")
[he:קובץ:Garrick
ross.jpg](../Page/he:קובץ:Garrick_ross.jpg.md "wikilink")
[ka:ფაილი:Garrick
ross-flash.jpg](../Page/ka:ფაილი:Garrick_ross-flash.jpg.md "wikilink")