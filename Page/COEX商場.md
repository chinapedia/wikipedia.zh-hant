[COEX_Mall_Central_Plaza_Atrium_2016.jpg](https://zh.wikipedia.org/wiki/File:COEX_Mall_Central_Plaza_Atrium_2016.jpg "fig:COEX_Mall_Central_Plaza_Atrium_2016.jpg")
[COEX_Mall_Live_Plaza_2016.jpg](https://zh.wikipedia.org/wiki/File:COEX_Mall_Live_Plaza_2016.jpg "fig:COEX_Mall_Live_Plaza_2016.jpg")
[SMTOWN_COEX_Artium_2016.jpg](https://zh.wikipedia.org/wiki/File:SMTOWN_COEX_Artium_2016.jpg "fig:SMTOWN_COEX_Artium_2016.jpg")
**COEX商場**是[韓國](../Page/韓國.md "wikilink")[首爾市一座大型](../Page/首爾.md "wikilink")[購物商場](../Page/購物商場.md "wikilink")，位於[江南區](../Page/江南區_\(首爾\).md "wikilink")[世界貿易中心內](../Page/世界貿易中心_\(首爾\).md "wikilink")，是韓國最大的購物商場，面積有85,000[平方米](../Page/平方米.md "wikilink")，是[蠶室體育場的](../Page/蠶室體育場.md "wikilink")23倍，每日遊客有10萬人次。除300間以上商店及餐廳外，亦設有[電影院](../Page/電影院.md "wikilink")、[夜總會](../Page/夜總會.md "wikilink")、[泡菜博物館及](../Page/泡菜博物館.md "wikilink")[水族館等娛樂設施](../Page/水族館.md "wikilink")。總合展示場以及整個商場翻新工程在2013年3月開始，2014年11月底完成。到2017年，中央廣場改建為圖書館「별마당도서관」（STARFIELD
LIBRARY），藏書達50,000本。3個高達13公尺的中央支柱改為的巨大書牆，變成為新景點。\[1\]

## 主要設施

  - [Megabox電影中心](../Page/Megabox電影中心.md "wikilink")
  - SMTOWN COEX Artium，2014年開幕，設SM Entertainment經紀公司所營運的複合式文化設施
  - COEX水族館

<File:COEX> Mall Asem Plaza 2016.jpg|商場 Asem Plaza <File:COEX> Mall
Central Plaza Access 2016.jpg|商場通道 <File:COEX> Mall Airport Plaza
restaurants 2016.jpg|Airport Plaza 的餐廳 <File:COEX> Mall Megabox Cinema
Lobby 2016.jpg|[Megabox電影中心](../Page/Megabox電影中心.md "wikilink")
<File:COEX> Aqurium 2016.jpg|COEX水族館

## 交通

  - [首爾地鐵2號線](../Page/首爾地鐵2號線.md "wikilink")[三成站](../Page/三成站.md "wikilink")

## 參考資料

## 外部連結

  - [COEX商場](https://web.archive.org/web/20060823094913/http://coexmall.com/)

[Category:首爾建築物](../Category/首爾建築物.md "wikilink")
[Category:首爾商場](../Category/首爾商場.md "wikilink")

1.