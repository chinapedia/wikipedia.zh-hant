[Yokohama_Baybridge.jpg](https://zh.wikipedia.org/wiki/File:Yokohama_Baybridge.jpg "fig:Yokohama_Baybridge.jpg")\]\]

**首都高速道路灣岸線**（；）是由[日本](../Page/日本.md "wikilink")[千葉縣](../Page/千葉縣.md "wikilink")[市川市至](../Page/市川市.md "wikilink")[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市](../Page/橫濱市.md "wikilink")[金澤區](../Page/金澤區.md "wikilink")、與灣岸道路（[國道357號](../Page/國道357號.md "wikilink")）平行的[首都高速道路路線](../Page/首都高速道路.md "wikilink")。

考慮到常有大型車通行，使用單側3車道的道幅（辰巳交流道 -
葛西交流道間、單側4車道），與其他首都高速的路線比較，此路直線很長，但規制速度只有80km/h（東京港隧道周邊為70km/h）（[速度規制圖](https://web.archive.org/web/20060808053738/http://www.shutoko.jp/safety/sokudokisei/index.html)）。分類為[道路構造令第](../Page/道路構造令.md "wikilink")2種第1級。途中通過・[橫濱港灣大橋](../Page/橫濱港灣大橋.md "wikilink")。

路線由千葉至神奈川，故不使用「上行、下行」，而是以「東行、西行」稱之。首都高速道路的收費區間分為東京線收費區間與神奈川線收費區間2個，以[川崎浮島系統交流道為界](../Page/川崎浮島JCT.md "wikilink")。

由於此路往橫濱的直線路程很長、夜晚常有[飆車族](../Page/飆車族.md "wikilink")（俗稱灣岸族、由浮島收費站開始向橫濱方向的最高速競賽。）出沒，另外晚間來行駛的話因為可以同步欣賞東京灣與橫濱的都市風光，灣岸也因為高人氣的[夜景](../Page/夜景.md "wikilink")，常在流行影視文化中出現。

## 路線編號

  - B（取自灣岸線的英語名Bayshore Route\[1\]）

## 高速灣岸分岐線

**高速灣岸分岐線**是[1號羽田線](../Page/首都高速1號羽田線.md "wikilink")[昭和島系統交流道至灣岸線東海系統交流道的](../Page/昭和島JCT.md "wikilink")4車道路線。全線位於東京都[大田區內](../Page/大田區.md "wikilink")。

1983年（[昭和](../Page/昭和.md "wikilink")58年）2月開始興建灣岸線東京都內部分至1號羽田線的連絡道路。1994年（[平成](../Page/平成.md "wikilink")6年）12月起灣岸線直接連結東京與橫濱，成為1號羽田線避開[都心環狀線前往東關東道](../Page/首都高速都心環狀線.md "wikilink")、[常磐道](../Page/常磐自動車道.md "wikilink")、[東北道的路徑](../Page/東北自動車道.md "wikilink")。

## 連接的高速道路

灣岸線

  - [首都高速神奈川3號狩場線](../Page/首都高速神奈川3號狩場線.md "wikilink")（本牧系統交流道）
  - [首都高速神奈川5號大黑線](../Page/首都高速神奈川5號大黑線.md "wikilink")（大黑系統交流道）
  - [首都高速神奈川6號川崎線](../Page/首都高速神奈川6號川崎線.md "wikilink")（川崎浮島系統交流道）
  - [東京灣跨海公路](../Page/東京灣跨海公路.md "wikilink")（川崎浮島系統交流道）
  - 首都高速灣岸分岐線（東海系統交流道）
  - [首都高速1號羽田線](../Page/首都高速1號羽田線.md "wikilink")（大井系統交流道）
  - [首都高速中央環狀線](../Page/首都高速中央環狀線.md "wikilink")（大井系統交流道）
  - [首都高速11號台場線](../Page/首都高速11號台場線.md "wikilink")（有明系統交流道）
  - [首都高速10號晴海線](../Page/首都高速道路10號晴海線.md "wikilink")（東雲系統交流道）
  - [首都高速9號深川線](../Page/首都高速9號深川線.md "wikilink")（辰巳系統交流道）
  - 首都高速中央環狀線（葛西系統交流道）

灣岸分岐線

  - 首都高速灣岸線（東海系統交流道）
  - [首都高速1號羽田線](../Page/首都高速1號羽田線.md "wikilink")（昭和島系統交流道）

## 出入口

<table>
<thead>
<tr class="header">
<th><p>出入口<br />
編號</p></th>
<th><p>施設名</p></th>
<th><p>接續路線名</p></th>
<th><p>起點<small>至</small><br />
<small>(<a href="../Page/km.md" title="wikilink">km</a>)</small></p></th>
<th><p>備考</p></th>
<th><p>所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/橫濱橫須賀道路.md" title="wikilink">橫濱橫須賀道路</a> <a href="../Page/橫須賀IC_(神奈川縣).md" title="wikilink">橫須賀</a>・<a href="../Page/三浦市.md" title="wikilink">三浦方向</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>B01</p></td>
<td><p><a href="../Page/幸浦出入口.md" title="wikilink">幸浦出入口</a></p></td>
<td><p><a href="../Page/橫濱八景島海島樂園.md" title="wikilink">八景島</a>、<a href="../Page/海之公園.md" title="wikilink">海之公園方向</a></p></td>
<td><p>0.9</p></td>
<td><p>空港中央・大黑埠頭方向出入口</p></td>
<td><p><a href="../Page/神奈川縣.md" title="wikilink">神奈川縣</a></p></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><a href="../Page/鳥濱町收費站.md" title="wikilink">鳥濱町收費站</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>機場中央・大黑埠頭方向</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>B02</p></td>
<td><p><a href="../Page/杉田出入口.md" title="wikilink">杉田出入口</a></p></td>
<td><p>鳥濱町・杉田方向</p></td>
<td><p>4.0</p></td>
<td><p>幸浦方向出入口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B03</p></td>
<td><p>4.0</p></td>
<td><p>機場中央・大黑埠頭方向出入口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>B05</p></td>
<td><p><a href="../Page/磯子出入口.md" title="wikilink">磯子出入口</a></p></td>
<td><p>（間）産業道路方向</p></td>
<td><p>6.1</p></td>
<td><p>機場中央・大黑埠頭方向出入口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B06</p></td>
<td><p><a href="../Page/三溪園出入口.md" title="wikilink">三溪園出入口</a></p></td>
<td><p><a href="../Page/山下公園.md" title="wikilink">山下公園</a>・<a href="../Page/本牧埠頭.md" title="wikilink">本牧埠頭方向</a></p></td>
<td><p>10.9</p></td>
<td><p>幸浦・杉田方向出入口</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><strong><a href="../Page/本牧系統交流道.md" title="wikilink">本牧系統交流道</a></strong></p></td>
<td><p><strong><a href="../Page/首都高速神奈川3號狩場線.md" title="wikilink">(K3)狩場線</a> <a href="../Page/橫濱站東口出入口.md" title="wikilink">橫濱站東口</a><small>方向</small></strong></p></td>
<td><p>14.3</p></td>
<td><p>連接幸浦・杉田方向</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B07</p></td>
<td><p><a href="../Page/本牧埠頭出入口.md" title="wikilink">本牧埠頭出入口</a></p></td>
<td><p>C・D突堤方向、A・B突堤方向</p></td>
<td><p>機場中央・大黑埠頭方向出入口<br />
不能往<a href="../Page/三澤出入口.md" title="wikilink">三澤</a>・橫濱站東口方向</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><strong>本牧交流道</strong></p></td>
<td><p><strong>(K3)狩場線 橫濱站東口方向</strong></p></td>
<td><p>連接機場中央・大黑埠頭方向</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><strong><a href="../Page/大黑系統交流道.md" title="wikilink">大黑系統交流道</a></strong></p></td>
<td><p><strong><a href="../Page/首都高速神奈川5號大黑線.md" title="wikilink">(K5)大黑線</a></strong></p></td>
<td><p>17.4</p></td>
<td><p>連接本牧埠頭・狩場線方向</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><a href="../Page/大黑停車區.md" title="wikilink">大黑休息區</a></p></td>
<td><p>-</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B08</p></td>
<td><p><a href="../Page/大黑埠頭出入口.md" title="wikilink">大黑埠頭出入口</a></p></td>
<td></td>
<td><p>本牧埠頭・狩場線方向出入口</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>B09</p></td>
<td><p>東京・機場中央方向出入口</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><strong>大黑交流道</strong></p></td>
<td><p><strong>(K5)大黑線</strong></p></td>
<td><p>連接浦安・機場中央方向</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>B10</p></td>
<td><p><a href="../Page/東扇島出入口.md" title="wikilink">東扇島出入口</a></p></td>
<td><p>（間）<a href="../Page/國道132號.md" title="wikilink">國道132號</a></p></td>
<td><p>25.4</p></td>
<td><p>大黑埠頭・本牧埠頭方向出入口</p></td>
<td><p><a href="../Page/川崎市.md" title="wikilink">川崎市</a></p></td>
</tr>
<tr class="odd">
<td><p>B11</p></td>
<td><p>25.4</p></td>
<td><p>東京・機場中央方向出入口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><a href="../Page/川崎航路隧道.md" title="wikilink">川崎航路隧道</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>禁行載運危險物車輛</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><strong><a href="../Page/川崎浮島系統交流道.md" title="wikilink">川崎浮島系統交流道</a></strong></p></td>
<td><p><a href="../Page/東京灣橫斷道路.md" title="wikilink">東京灣Aqua-Line</a>/<strong><a href="../Page/首都高速神奈川6號川崎線.md" title="wikilink">(K6)川崎線</a> 川崎方向</strong></p></td>
<td><p>29.5</p></td>
<td><p>橫濱・大黑埠頭方向</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>B12</p></td>
<td><p><a href="../Page/浮島IC_(神奈川縣).md" title="wikilink">浮島出入口</a></p></td>
<td><p><a href="../Page/國道409號.md" title="wikilink">國道409號</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><a href="../Page/灣岸浮島收費站.md" title="wikilink">灣岸浮島收費站</a></p></td>
<td><p>-</p></td>
<td><p>橫濱方向，停用在2012年1月1日，2013年3月31日之前撤除了。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B13</p></td>
<td><p>浮島出入口</p></td>
<td><p>國道409號</p></td>
<td><p>東京・機場中央方向</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><strong>川崎浮島交流道</strong></p></td>
<td><p><a href="../Page/東京灣跨海公路.md" title="wikilink">東京灣Aqua-Line</a>/<strong>(K6)川崎線 川崎方向</strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><a href="../Page/多摩川隧道.md" title="wikilink">多摩川隧道</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>禁行載運危險物車輛</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td><p><a href="../Page/大田區.md" title="wikilink">大田區</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B14</p></td>
<td><p><a href="../Page/灣岸環八出入口.md" title="wikilink">灣岸環八出入口</a></p></td>
<td><p><a href="../Page/東京都道311號環狀八號線.md" title="wikilink">環八通</a><br />
空港第1大樓方向</p></td>
<td><p>31.8</p></td>
<td><p>大黑埠頭・本牧埠頭方向出入口</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>B16</p></td>
<td><p><a href="../Page/機場中央出入口.md" title="wikilink">機場中央出入口</a></p></td>
<td><p>機場第2航廈・國際大樓方向</p></td>
<td><p>34.5</p></td>
<td><p>橫濱方向出入口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>B17</p></td>
<td><p><a href="../Page/東京國際機場.md" title="wikilink">羽田機場方向</a></p></td>
<td><p>34.5</p></td>
<td><p>銀座・浦安方向出入口</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><a href="../Page/機場北隧道.md" title="wikilink">機場北隧道</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>危險物積載車輛禁止通行</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><strong><a href="../Page/東海系統交流道_(東京都).md" title="wikilink">東海系統交流道</a></strong></p></td>
<td><p><strong>橫濱公園・<a href="../Page/羽田出入口.md" title="wikilink">羽田方向</a>（灣岸分岐線）</strong></p></td>
<td><p>38.6</p></td>
<td><p>連接銀座・浦安方向</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><a href="../Page/大井停車區.md" title="wikilink">大井休息區</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>機場中央・羽田方向</p></td>
<td><p><a href="../Page/品川區.md" title="wikilink">品川區</a></p></td>
</tr>
<tr class="odd">
<td><p>002</p></td>
<td><p><a href="../Page/大井南出入口.md" title="wikilink">大井南出入口</a></p></td>
<td><p>（間）<a href="../Page/勝島.md" title="wikilink">勝島方向</a></p></td>
<td><p>39.4</p></td>
<td><p>機場中央・羽田方向出入口</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>003A</p></td>
<td><p>39.4</p></td>
<td><p>銀座・浦安方向出入口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><a href="../Page/大井收費站.md" title="wikilink">大井收費站</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>銀座・浦安方向</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><strong><a href="../Page/大井系統交流道.md" title="wikilink">大井系統交流道</a></strong></p></td>
<td><p><strong><a href="../Page/首都高速中央環狀線.md" title="wikilink">(C2)中央環狀線</a> <a href="../Page/中央自動車道.md" title="wikilink">中央道方向</a></strong></p></td>
<td><p>41.1</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/首都高速1號羽田線.md" title="wikilink">(1)羽田線</a> <a href="../Page/銀座出入口.md" title="wikilink">銀座方向</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p>大井停車區</p></td>
<td><p>-</p></td>
<td></td>
<td><p>浦安・東關道方向</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>003B</p></td>
<td><p><a href="../Page/大井出入口.md" title="wikilink">大井出入口</a></p></td>
<td><p>（間）<a href="../Page/品川_(東京都).md" title="wikilink">品川</a>・大井町方向</p></td>
<td><p>41.3</p></td>
<td><p>浦安・東關道方向出入口</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><a href="../Page/東京港隧道.md" title="wikilink">東京港隧道</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>禁行載運危險物車輛</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>004</p></td>
<td><p><a href="../Page/臨海副都心出入口.md" title="wikilink">臨海副都心出入口</a></p></td>
<td><p><a href="../Page/東京國際展示場.md" title="wikilink">國際展示場</a>・渡輪埠頭方向</p></td>
<td><p>43.1</p></td>
<td><p>空港中央・羽田方向出入口</p></td>
<td><p><a href="../Page/港區_(東京都).md" title="wikilink">港區</a></p></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><strong><a href="../Page/有明系統交流道.md" title="wikilink">有明系統交流道</a></strong></p></td>
<td><p><strong><a href="../Page/首都高速11號台場線.md" title="wikilink">(11)台場線</a></strong></p></td>
<td><p>44.9</p></td>
<td></td>
<td><p><a href="../Page/江東區.md" title="wikilink">江東區</a></p></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><strong><a href="../Page/東雲系統交流道.md" title="wikilink">東雲系統交流道</a></strong></p></td>
<td><p><strong><a href="../Page/首都高速10號晴海線.md" title="wikilink">(10)晴海線</a></strong></p></td>
<td><p>45.9</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>005</p></td>
<td><p><a href="../Page/有明出入口.md" title="wikilink">有明出入口</a></p></td>
<td><p>(<a href="../Page/台場.md" title="wikilink">台場地區</a>)國際展示場・渡輪埠頭方向</p></td>
<td><p>浦安・東關道方向出入口</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><strong><a href="../Page/辰巳系統交流道.md" title="wikilink">辰巳系統交流道</a></strong></p></td>
<td><p><strong><a href="../Page/首都高速9號深川線.md" title="wikilink">(9)深川線</a> <a href="../Page/銀座出入口.md" title="wikilink">銀座</a>・<a href="../Page/箱崎系統交流道.md" title="wikilink">箱崎方向</a></strong></p></td>
<td><p>47.6</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>006</p></td>
<td><p><a href="../Page/新木場出入口.md" title="wikilink">新木場出入口</a></p></td>
<td></td>
<td><p>49.0</p></td>
<td><p>羽田方向出入口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>007</p></td>
<td><p>49.0</p></td>
<td><p>浦安・東關道方向出入口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><strong><a href="../Page/葛西系統交流道.md" title="wikilink">葛西系統交流道</a></strong></p></td>
<td><p><strong><a href="../Page/首都高速中央環狀線.md" title="wikilink">(C2)中央環狀線</a> <a href="../Page/東北自動車道.md" title="wikilink">東北道</a>・<a href="../Page/常磐自動車道.md" title="wikilink">常磐道方向</a></strong></p></td>
<td><p>50.9</p></td>
<td></td>
<td><p><a href="../Page/江戶川區.md" title="wikilink">江戶川區</a></p></td>
</tr>
<tr class="odd">
<td><p>008</p></td>
<td><p><a href="../Page/葛西出入口.md" title="wikilink">葛西出入口</a></p></td>
<td><p>（間）<a href="../Page/東京都道318號環狀七號線.md" title="wikilink">環七通</a></p></td>
<td><p>52.3</p></td>
<td><p>銀座・<a href="../Page/小菅系統交流道.md" title="wikilink">小菅方向出入口</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>009</p></td>
<td><p>52.3</p></td>
<td><p>浦安・東關道方向出入口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>010A</p></td>
<td><p><a href="../Page/舞濱入口.md" title="wikilink">舞濱入口</a></p></td>
<td><p><a href="../Page/東京迪士尼度假區.md" title="wikilink">東京迪士尼度假區方向</a></p></td>
<td><p>53.7</p></td>
<td><p>銀座・小菅方向入口</p></td>
<td><p><a href="../Page/千葉縣.md" title="wikilink">千葉縣</a></p></td>
</tr>
<tr class="even">
<td><p>010B</p></td>
<td><p><a href="../Page/浦安出入口.md" title="wikilink">浦安出入口</a></p></td>
<td><p>（間）東京迪士尼度假區方向</p></td>
<td><p>55.7</p></td>
<td><p>銀座・小菅方向出入口</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>011</p></td>
<td><p>55.7</p></td>
<td><p>千鳥町・東關道方向出入口</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>012</p></td>
<td><p><a href="../Page/千鳥町出入口.md" title="wikilink">千鳥町出入口</a></p></td>
<td><p><a href="../Page/船橋市.md" title="wikilink">船橋</a>・市川方向</p></td>
<td><p>59.6</p></td>
<td><p>銀座・小菅方向出入口</p></td>
<td><p><a href="../Page/市川市.md" title="wikilink">市川市</a></p></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><a href="../Page/市川停車區.md" title="wikilink">市川休息區</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>東京・川崎方向</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p><a href="../Page/市川收費站.md" title="wikilink">市川收費站</a></p></td>
<td><p>-</p></td>
<td></td>
<td><p>東京・川崎方向</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><a href="../Page/市川系統交流道.md" title="wikilink">高谷系統交流道</a></p></td>
<td><p><a href="../Page/東京外環狀道路.md" title="wikilink">東京外環狀道路</a></p></td>
<td><p>62.1</p></td>
<td><p>建設中</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東關東自動車道.md" title="wikilink">東關東自動車道</a> <a href="../Page/千葉市.md" title="wikilink">千葉</a>・<a href="../Page/成田市.md" title="wikilink">成田方向</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 高速灣岸分岐線

<table>
<thead>
<tr class="header">
<th><p>施設名</p></th>
<th><p>接續路線名</p></th>
<th><p>起點<small>至</small><br />
<small>(<a href="../Page/km.md" title="wikilink">km</a>)</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/昭和島系統交流道.md" title="wikilink">昭和島系統交流道</a></p></td>
<td><p><a href="../Page/首都高速1號羽田線.md" title="wikilink">(1)羽田線</a> <a href="../Page/橫濱公園出入口.md" title="wikilink">橫濱公園</a>・<a href="../Page/羽田出入口.md" title="wikilink">羽田方向</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東海系統交流道_(東京都).md" title="wikilink">東海系統交流道</a></p></td>
<td><p><strong>(B)灣岸線</strong> <a href="../Page/東關東自動車道.md" title="wikilink">東關東道</a>・<a href="../Page/浦安出入口.md" title="wikilink">浦安方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 主要隧道與橋樑

  - （13號地出入口 - 大井出入口）

  - （東海交流道 - 空港中央出入口）

  - （灣岸環八出入口 - 川崎浮島交流道）

  - （川崎浮島交流道 - 東扇島出入口）

  - [鶴見翼橋](../Page/鶴見翼橋.md "wikilink")（東扇島出入口 - 大黑交流道）

  - [橫濱港灣大橋](../Page/橫濱港灣大橋.md "wikilink")（大黑交流道 - 本牧交流道）

（西行順序）

## 历史

  - 1976年（昭和51年）8月12日　大井JCT～[13號地出入口開通](../Page/臨海副都心出入口.md "wikilink")・[東京港隧道開通](../Page/東京港隧道.md "wikilink")。
  - 1978年（昭和53年）1月20日　[新木場出入口](../Page/新木場出入口.md "wikilink")～浦安出入口開通。
  - 1980年（昭和55年）2月　[辰巳JCT](../Page/辰巳JCT.md "wikilink")～新木場出入口開通。
  - 1981年（昭和56年）5月　辰巳JCT～有明出入口開通。
  - 1982年（昭和57年）4月27日　[浦安出入口](../Page/浦安出入口.md "wikilink")～高谷出入口開通。
  - 1983年（昭和58年）2月　[東海JCT](../Page/東海JCT_\(東京\).md "wikilink")～大井JCT開通。
  - 1984年（昭和59年）12月　13號地出入口～有明出入口開通。
  - 1989年（平成元年）9月27日　本牧埠頭出入口～[大黑JCT開通](../Page/大黑JCT.md "wikilink")・[橫濱港灣大橋開通](../Page/橫濱港灣大橋.md "wikilink")。
  - 1993年（平成5年）9月　[空港中央出入口](../Page/空港中央出入口.md "wikilink")～東海JCT開通。
  - 1994年（平成6年）12月21日　空港中央出入口～大黑JCT開通・[鶴見翅膀橋開通](../Page/鶴見翅膀橋.md "wikilink")。
  - 1997年（平成9年）12月12日　[浮島出入口開通](../Page/浮島出入口.md "wikilink")。
  - 1997年（平成9年）12月18日　與[東京灣Aqua-Line接續](../Page/東京灣跨海公路.md "wikilink")。
  - 1999年（平成11年）7月15日　並木出入口～[杉田出入口](../Page/杉田出入口.md "wikilink")、[三溪園仮出入口](../Page/三溪園.md "wikilink")～本牧頭出入口開通。
  - 2001年（平成13年）10月22日　本牧埠頭出入口～[幸浦出入口開通](../Page/幸浦出入口.md "wikilink")，全線開通。
  - 2007年（平成19年）12月22日 : 13號地出入口改稱為臨海副都心出入口。
  - 2008年（平成20年）3月17日 : 東行有明JCT - 辰巳JCT拓寬為4車道。
  - 2009年（平成21年）2月11日 : 新增東雲JCT。
  - 2012年（平成24年）3月21日 : 大井南出口(西行)移動300公尺。
  - 2012年（平成24年）7月17日 : 大井PA東行移動2公里，西行移動600公尺。

## 交通量

24小時交通量（平成17年度道路交通調查）

<table>
<tbody>
<tr class="odd">
<td><p>千葉縣</p>
<ul>
<li>浦安市辯天1丁目 : 115,723</li>
</ul></td>
<td><p>東京都</p>
<ul>
<li>品川區八潮3丁目2 : 121,162</li>
<li>大田區羽田空港3丁目 : 59,683</li>
</ul></td>
<td><p>神奈川縣</p>
<ul>
<li>川崎市川崎區東扇島 : 72,993</li>
<li>橫濱市金澤區鳥濱町 : 48,630</li>
</ul></td>
</tr>
</tbody>
</table>

## 測速照相機

首都高速灣岸線、於以下地點設有測速照相機（2006年6月現在）。

  - 東行（千葉方向）：[灣岸環八出口前](../Page/灣岸環八出入口.md "wikilink")（空港南隧道前）、[臨海副都心出口前](../Page/臨海副都心出入口.md "wikilink")（[富士電視台本社大樓橫](../Page/富士電視台.md "wikilink")）
  - 西行（橫濱方向）：[千鳥町附近](../Page/千鳥町出入口.md "wikilink")（[灣岸市川料金所橫](../Page/灣岸市川出入口.md "wikilink")）

## 圖片

<File:Shutoko> wangan.jpg|夜晚的首都高速灣岸線 <File:Shuto> expressway bay
bridge.jpg|[橫濱港灣大橋](../Page/橫濱港灣大橋.md "wikilink")
[File:Shuto_expressway_higashiogishima.jpg|東扇島周邊](File:Shuto_expressway_higashiogishima.jpg%7C東扇島周邊)
<File:Syutokosoku> lineB Tamagawa Tunnel Ukisima-guchi.JPG|川崎浮島側
<File:Shuto> expressway kuko chuo.jpg|空港中央
[File:Shuto_expressway_wangan_tunnel.jpg|空港北隧道](File:Shuto_expressway_wangan_tunnel.jpg%7C空港北隧道)
<File:Shuto> expressway oi toll booth.jpg|大井收費站 <File:Shuto> expressway
13gouchi.jpg|[富士電視台周邊](../Page/富士電視台.md "wikilink") <File:Shuto>
expressway ariake junction.jpg|[有明系統交流道](../Page/有明JCT.md "wikilink")
<File:Shuto> expressway harumi jct.jpg|東雲系統交流道 <File:Kasai>
JCT1.jpg|葛西系統交流道

## 備註

### 所轄警察

  - 大黑分駐所：[幸浦出入口](../Page/幸浦出入口.md "wikilink")～[川崎浮島系統交流道](../Page/川崎浮島JCT.md "wikilink")\[2\]

  - 新富分駐所：川崎浮島系統交流道～東京都、千葉縣境\[3\]

  - 千葉縣警察高速道路交通警察隊本隊：東京都、千葉縣境～[市川本線收費站](../Page/市川TB.md "wikilink")\[4\]

## 註釋

## 相關條目

  - [日本高速道路列表](../Page/日本高速道路列表.md "wikilink")
  - [東京高速道路](../Page/東京高速道路.md "wikilink")
  - [灣岸競速](../Page/灣岸競速.md "wikilink")

## 外部連結

  - [首都高速道路株式会社](http://www.shutoko.jp/)

[B Wangan](../Category/首都高速道路.md "wikilink")
[Category:東京都道路](../Category/東京都道路.md "wikilink")
[Category:神奈川縣道路](../Category/神奈川縣道路.md "wikilink")
[Category:千葉縣道路](../Category/千葉縣道路.md "wikilink")
[294](../Category/神奈川縣道.md "wikilink")
[294](../Category/千葉縣道.md "wikilink")
[294](../Category/東京都道.md "wikilink")
[Category:橫濱市道](../Category/橫濱市道.md "wikilink")
[Category:金澤區](../Category/金澤區.md "wikilink")
[Category:磯子區](../Category/磯子區.md "wikilink") [Category:中區
(橫濱市)](../Category/中區_\(橫濱市\).md "wikilink") [Category:鶴見區
(橫濱市)](../Category/鶴見區_\(橫濱市\).md "wikilink")
[Category:川崎區](../Category/川崎區.md "wikilink")
[Category:大田區](../Category/大田區.md "wikilink")
[Category:品川區](../Category/品川區.md "wikilink") [Category:江東區
(東京都)](../Category/江東區_\(東京都\).md "wikilink")
[Category:江戶川區](../Category/江戶川區.md "wikilink")
[Category:浦安市](../Category/浦安市.md "wikilink")
[Category:市川市](../Category/市川市.md "wikilink")
[Category:東京灣](../Category/東京灣.md "wikilink")

1.  読めば安心！怖さを解消！首都高運転のコツ１０（平成24年7月版），首都高速道路（株）発行，P.5より。
2.  [神奈川県警察高速道路交通警察隊](https://www.police.pref.kanagawa.jp/mes/mes87001.htm)
3.  [警視庁高速道路交通警察隊](http://www.keishicho.metro.tokyo.jp/kotu/kousoku/kousoku.htm)

4.  [千葉県警察高速道路交通警察隊の運用に関する訓令](http://www.police.pref.chiba.jp/information/orders/pdfs/st/s470401_01.pdf)