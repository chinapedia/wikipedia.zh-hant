**单国**，是中国[周朝时期的一个小型](../Page/周朝.md "wikilink")[诸侯国](../Page/诸侯国.md "wikilink")。

## 起源和历史

「单」字意同「檀」。据《[世本](../Page/世本.md "wikilink")》和《[元和姓纂](../Page/元和姓纂.md "wikilink")》，[周成王封小儿子](../Page/周成王.md "wikilink")[臻于单邑](../Page/单伯臻.md "wikilink")，为甸内侯；而《[路史](../Page/路史.md "wikilink")》、《[通志](../Page/通志.md "wikilink")·氏族略》和《[绎史](../Page/绎史.md "wikilink")》则认为周成王封蔑于单氏。《姓源》对两种说法进行辩证，认为单蔑就是周成王的小儿子单伯臻。它是周畿内诸侯国，与周王室是亲属。一说檀国是[姜姓](../Page/姜姓.md "wikilink")，始封君为檀伯达，在今[河南省](../Page/河南省.md "wikilink")[济源市南](../Page/济源市.md "wikilink")。单国的国君时代继承[卿士的地位](../Page/卿士.md "wikilink")。

2003年，陕西眉县杨家村出土了一批[青铜器](../Page/青铜器.md "wikilink")，根据其中[逨盘的释读铭文](../Page/逨盘.md "wikilink")，单氏早在[周文王](../Page/周文王.md "wikilink")、[周武王时期便已存在](../Page/周武王.md "wikilink")，推翻了以往的所有观点。

## 地理位置

单国原在陕西省眉县，后随周王室东迁至[河南省](../Page/河南省.md "wikilink")[孟津县东南](../Page/孟津县.md "wikilink")。

## 史书记载

1.  《国语·周语下》有[单襄公等论述](../Page/单襄公.md "wikilink")。
2.  [魯莊公元年](../Page/魯莊公.md "wikilink")（前693年），[周庄王把女儿嫁给](../Page/周庄王.md "wikilink")[齐国君主](../Page/齐国.md "wikilink")[齐襄公](../Page/齐襄公.md "wikilink")，由[魯莊公主持](../Page/魯莊公.md "wikilink")。杨树达对这件事作注：*单音善，天子畿内地名。单伯，天子之卿，世仕三朝，此及文公之世皆称单伯，成公以下常称单子。*

## 单国君主列表

1.  [單公](../Page/單公.md "wikilink")，事周文、武二王，曾參與伐商。（《逨盤》、《逨盉》）
2.  [單伯臻](../Page/單伯臻.md "wikilink")，
3.  [單伯](../Page/單伯_\(西周\).md "wikilink")，西周共王、懿王時期單伯。（《裘衛盉》、《揚簋》）
4.  [单伯昊生](../Page/单伯昊生.md "wikilink")，西周晚期單伯。（《單伯昊生鐘》）
5.  [单伯原父](../Page/单伯原父.md "wikilink")，西周晚期單伯。（《單伯原父鬲》）
6.  [單伯](../Page/單伯.md "wikilink")，東周莊王四年（前693年）單伯。（《左傳》莊公元年）
7.  [单襄公](../Page/单襄公.md "wikilink")
8.  [单顷公](../Page/单顷公.md "wikilink")
9.  [单靖公](../Page/单靖公.md "wikilink")
10. [单献公](../Page/单献公.md "wikilink")（？─前535年）
11. [单成公](../Page/单成公.md "wikilink")（前534年─前531年）
12. [单穆公單旗](../Page/单穆公.md "wikilink")（前530年─前520年）
13. [单武公](../Page/单武公.md "wikilink")（前519年─？）
14. [单平公](../Page/单平公.md "wikilink")

## 世系图

### 西周初期

<center>

</center>

### 春秋时期

<center>

</center>

## 参看

  - [单氏取周](../Page/单氏取周.md "wikilink")

## 外部链接

  - [眉县青铜器铭文说些什么?中国学者揭开谜底(图)](http://www.china.com.cn/chinese/CU-c/297457.htm)

[S单](../Category/春秋時期的國家.md "wikilink")