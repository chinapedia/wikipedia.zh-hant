**香槟池**（Champagne
Pool）是[新西兰](../Page/新西兰.md "wikilink")[北岛](../Page/北島_\(新西兰\).md "wikilink")[怀卡托区怀奥塔普](../Page/怀卡托.md "wikilink")[地热区的一个著名景点](../Page/地热.md "wikilink")。[温泉位于](../Page/温泉.md "wikilink")[罗托路亚东南](../Page/罗托路亚.md "wikilink")30公里，[陶波](../Page/陶波.md "wikilink")50公里。香槟池的名称来自大量[二氧化碳的释出如同玻璃杯中](../Page/二氧化碳.md "wikilink")[香槟酒的气泡](../Page/香槟酒.md "wikilink")。温泉形成于900年前一次火山爆发的热液。\[1\]湖的直径约65米，最深处约为65米，估计容积为50,000立方米。\[2\]

## 水质化学

香槟池地下深处的地热水温为260°C，\[3\]但是由于热量散发在空气中，池中水温保持在73°C到75°C。由于二氧化碳的释出，池水[pH值相对恒定在](../Page/pH值.md "wikilink")5.5。释放的气体主要是二氧化碳，也含有少量[氮气](../Page/氮气.md "wikilink")、[甲烷](../Page/甲烷.md "wikilink")、[氢气](../Page/氢气.md "wikilink")、[硫化氢和](../Page/硫化氢.md "wikilink")[氧气](../Page/氧气.md "wikilink")。\[4\]硅酸地热液体中含有过饱和的[类金属化合物](../Page/类金属.md "wikilink")，如[硫化砷和](../Page/硫化砷.md "wikilink")[硫化锑](../Page/硫化锑.md "wikilink")，其沉淀物呈现出橙色。\[5\]色彩艳丽的沉淀物与香槟池周围灰白的[二氧化硅火山灰熔渣形成对比](../Page/二氧化硅.md "wikilink")。

## 生物学

尽管香槟池地球化学特性突出，但是关于它作为微生物形成的研究依然很少。氢气和二氧化碳或者氧气是[产烷生物或氢氧化](../Page/產甲烷作用.md "wikilink")[微生物](../Page/微生物.md "wikilink")[自养生长的](../Page/自养生物.md "wikilink")[新陈代谢能量来源](../Page/新陈代谢.md "wikilink")。非培养生物方法提供了温泉中细丝状的、球菌状的和杆状的细胞形态的证据。\[6\]\[7\]\[8\]两种特别的[细菌和一种](../Page/细菌.md "wikilink")[古菌被成功地从香槟池分离出来](../Page/古菌.md "wikilink")。\[9\]

## 参考资料

[Category:紐西蘭地理](../Category/紐西蘭地理.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.

9.