**天龍座**是在北天的一個星座，它的名稱源自[拉丁文的](../Page/拉丁文.md "wikilink")[dragon](../Page/龍.md "wikilink")。對許多[北半球的觀測者而言](../Page/北半球.md "wikilink")，天龍座是個永遠不會沒入地平線下的[拱極星座](../Page/拱極星座.md "wikilink")。它是天文學家[托勒密在西元二世紀就已經設置的星座](../Page/托勒密.md "wikilink")，[黃道的北極也位於天龍座](../Page/黃道.md "wikilink")。

天龙座所占区域很广，弯曲象一条长龙；头部四星成一四边形，其中最亮的两颗星表示龙眼。

## 值得注意的特色

[DracoCC.jpg](https://zh.wikipedia.org/wiki/File:DracoCC.jpg "fig:DracoCC.jpg")

### 恆星

[右樞](../Page/右樞.md "wikilink")（天龍座α，Thuban），在北極點遠離[天槍三](../Page/天槍三.md "wikilink")（牧夫座θ）之後，是從西元前3,942年至1793年的[北極星](../Page/北極星.md "wikilink")。[埃及金字塔在設計上是有一側是朝向當時的北方](../Page/埃及金字塔.md "wikilink")，因此可以在夜晚導入右樞來觀察。而由於[歲差的效果](../Page/歲差_\(天文\).md "wikilink")，在經過21,000年，他會再度成為北極星。它是顆藍白色的巨星，視星等3.7等，距離地球309光年。右樞固有的英文名字，Thuban，意思是“巨蛇的頭”。

天龍座還有另外兩顆三等以上的恆星。那兩顆星中較亮的一顆－天龍座最亮的恆星－
[天棓四（天龍座γ）](../Page/天棓四.md "wikilink")，英文的固有名稱是Eltamin或Eltanin。它是顆是星等2.2等的橙色巨星，距離地球148光年。[詹姆斯·布蘭得利在](../Page/詹姆斯·布蘭得利.md "wikilink")1728年觀察天棓四時發現恆星的[光行差](../Page/光行差.md "wikilink")。[天棓三（天龍座β）的英文固有名稱是Rastaban](../Page/天棓三.md "wikilink")，它是一顆視星等2.8等，距離地球362光年的黃巨星。它與右樞共享"巨蛇的頭"的意義。雖然[右樞被](../Page/右樞.md "wikilink")[拜耳命名為天龍座α](../Page/拜耳命名法.md "wikilink")，它卻不是天龍座最亮的星。它比最亮的[天棓四](../Page/天棓四.md "wikilink")（天龍座γ，Eltanin）的亮度暗1.5等。

天龍座有幾組值得注意的[雙星和](../Page/雙星.md "wikilink")[聯星](../Page/聯星.md "wikilink")。[天龍座η是雙星](../Page/天龍座η.md "wikilink")，兩星相距4.8[角秒](../Page/角秒.md "wikilink")，主星是2.8等的黃色色調，伴星在主星的南方距離為是8.2等的白色星\[1\]。[天龍座μ的固有名稱為Alrakis](../Page/天龍座μ.md "wikilink")，是兩顆白色恆星組成的聯星，星等分別為5.6和5.7等，互繞的軌道週期670年，距離地球88光年。[天龍座ν是一對相似的聯星](../Page/天龍座ν.md "wikilink")，由兩顆視星等都是4.9的白色恆星組成，距離地球100光年，使用業餘的小望遠鏡或雙筒望遠鏡就可以分辨出來。[天龍座ο是小型天文望遠鏡就可以看見的雙星](../Page/天龍座ο.md "wikilink")，它的主星是4.6等的橘色巨星，距離地球322光年；伴星的星等是7.8等。[天龍座ξ是業餘的小望遠鏡或雙筒望遠鏡就可以分辨出來的聯星](../Page/天龍座ξ.md "wikilink")，距離地球72光年，主星是視星等4.6等的黃白色星，伴星是5.8等的黃色星。[天龍座16和](../Page/天龍座16.md "wikilink")[天龍座17是三合星的一部分](../Page/天龍座17.md "wikilink")，距離地球400光年，要使用中等大小的業餘望遠鏡才能夠分辨出來。主星是5.1等的藍來白色星，本身就是5.4等和6.5等的兩顆星，第二顆恆星的星等是5.5等。[天龍座20是聯星](../Page/天龍座20.md "wikilink")，主星是7.1等的白色星，伴星位於主星的東北偏東，是7.3等的黃色星。這兩顆星最大的分離角度是1.2角秒，軌道週期420年。在2012年，這兩顆星正在接近最大的分離角\[2\]。[天龍座39是距離地球](../Page/天龍座39.md "wikilink")188光年的三合星，業餘的小望遠鏡就可以分辨出來。主星是5.0等的藍色恆星，第二顆星是7.4等的黃色恆星，第三顆是很靠近主星的8.0等恆星。[天龍座40和](../Page/天龍座40.md "wikilink")[天龍座41是小望遠鏡就可以分辨的聯星](../Page/天龍座41.md "wikilink")，這兩顆橘色的恆星分別是5.7等和6.1等，距離地球170光年。

[天龍座R是一顆週期大約](../Page/天龍座R.md "wikilink")8個月（245.5天）的紅色[蒭藁型變星](../Page/米拉變星.md "wikilink")，它的平均最低星等大約是12.4等，最亮大約是7.6等。它是在1876年被[Hans
Geelmuyden發現的變星](../Page/Hans_Geelmuyden.md "wikilink")\[3\]。天龍座T星的星等在7.2和13.5之間，週期421.2天。

最近發現行星的[克卜勒-10也在天龍座內](../Page/克卜勒-10.md "wikilink")，並且[克卜勒-10b的存在也已經被證實了](../Page/克卜勒-10b.md "wikilink")，這顆在太陽系外的岩石行星大小與地球相似。

### 深空天體

#### 神話

[Sidney_Hall_-_Urania's_Mirror_-_Draco_and_Ursa_Minor.jpg](https://zh.wikipedia.org/wiki/File:Sidney_Hall_-_Urania's_Mirror_-_Draco_and_Ursa_Minor.jpg "fig:Sidney_Hall_-_Urania's_Mirror_-_Draco_and_Ursa_Minor.jpg")

在希臘神話中有許多的龍都與星座相關，包括看守[金蘋果的科爾喀斯巨龍](../Page/赫斯珀里得斯#赫斯珀里得斯聖園.md "wikilink")[拉冬](../Page/科爾喀斯凶龍.md "wikilink")。[海克力斯在他的](../Page/海克力斯.md "wikilink")[12件任務中殺死了拉冬](../Page/12件任務.md "wikilink")，順利地偷走了金蘋果。[赫克利斯的星座就在](../Page/武仙座.md "wikilink")[拉冬的旁邊](../Page/天龍座.md "wikilink")。

在希臘-羅馬的傳說中，天龍座是在任務失敗後被女神[密涅瓦殺死](../Page/密涅瓦.md "wikilink")，然後被扔進天空的。這隻龍本來是[泰坦巨人](../Page/泰坦巨人.md "wikilink")，與奧林匹克的眾神戰鬥了10年。當密涅瓦投擲這隻龍時，它將自己扭曲並冰凍在[天球北極](../Page/天極#北天極的定位.md "wikilink")。有時，天龍被描述為大地女神[蓋亞的兒子](../Page/蓋亞.md "wikilink")[堤豐](../Page/堤豐.md "wikilink")。

傳統的阿拉伯天文學家對現代天文學的天龍座有不同的描述，稱這條龍為[母親駱駝](../Page/母親駱駝.md "wikilink")。同樣的，[天龍座η和](../Page/天龍座η.md "wikilink")[天龍座ζ被描述為兩隻攻擊駱駝寶寶](../Page/天龍座ζ.md "wikilink")（靠近[天龍座β附近的暗星](../Page/天龍座β.md "wikilink")）的[鬣狗](../Page/鬣狗.md "wikilink")；[天棓三](../Page/天棓三.md "wikilink")（天龍座β）、[天棓四](../Page/天棓四.md "wikilink")（天龍座γ）、[天龍座ν](../Page/天龍座ν.md "wikilink")、和[天棓一](../Page/天棓一.md "wikilink")（天龍座ξ）是保護寶寶的四隻母駱駝。游牧人自己和它們的駱駝就紮營在附近，[天廚三](../Page/天廚三.md "wikilink")（天龍座ε）、[御女一](../Page/御女一.md "wikilink")（天龍座τ）、和[天廚二](../Page/天廚二.md "wikilink")（天龍座σ）構成烹煮用的三角架。

#### 流星雨

[天龍座η星二月流星雨是在](../Page/天龍座η星二月流星雨.md "wikilink")2011年2月4日新發現的流星雨，觀測者注意到每小時有6顆流星來自一個共同的[輻射點](../Page/輻射點.md "wikilink")。其先前的[母體是一顆未知的](../Page/母體.md "wikilink")[長週期彗星](../Page/彗星#長週期.md "wikilink")。\[4\]

## 外部連結

  - [The Deep Photographic Guide to the Constellations:
    Draco](http://www.allthesky.com/constellations/draco/)
  - [Star Tales – Draco](http://www.ianridpath.com/startales/draco.htm)
  - <http://www.starryskies.com/The_sky/constellations/draco.html>
  - [Draco Constellation at Constellation
    Guide](http://www.constellation-guide.com/constellation-list/draco-constellation/)

## 參考資料

**引文**

**參考書籍**

  -
  -
  -
  -
[D](../Category/北天星座.md "wikilink") [D](../Category/托勒密星座.md "wikilink")
[\*](../Category/天龙座.md "wikilink")
[Tian1](../Category/星座.md "wikilink")
[D](../Category/龍.md "wikilink")

1.
2.
3.
4.