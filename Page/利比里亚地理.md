[Li-map.png](https://zh.wikipedia.org/wiki/File:Li-map.png "fig:Li-map.png")
**[賴比瑞亞](../Page/賴比瑞亞.md "wikilink")**是一個大的[撒哈拉國家](../Page/撒哈拉.md "wikilink")，位於西非。它的形狀呈一個[長方形](../Page/長方形.md "wikilink")，與[薩爾瓦多形狀相似](../Page/薩爾瓦多.md "wikilink")。西邊的[科特迪瓦與](../Page/科特迪瓦.md "wikilink")[大西洋毗鄰](../Page/大西洋.md "wikilink")，其他三邊分別和三國[非洲國家連接](../Page/非洲.md "wikilink")。賴比瑞亞的國界總長度為1,587
公里，其中西北部的306公里與[獅子山共和國相接](../Page/塞拉利昂.md "wikilink")，北部563公里毗連[幾內亞共和國](../Page/几內亚.md "wikilink")，而東部的716公里銜接著[象牙海岸共和國](../Page/科特迪瓦.md "wikilink")。賴比瑞亞的面積為111,370平方公里，其中96,320平方公里為陸地，及15,050平方公里的水域。

賴比瑞亞現時宣稱有一片370公里的海上領土。

## 地勢

賴比瑞亞境內大部份為山地，而海濱一帶則為[平原](../Page/平原.md "wikilink")，中部為地勢平緩的[丘陵地帶](../Page/丘陵.md "wikilink")，而內陸的東北部為[高原地貌](../Page/高原.md "wikilink")。境內最高峰為[武蒂維峰](../Page/武蒂維峰.md "wikilink")，高約1,381米，最大河流為[卡拉瓦河](../Page/卡拉瓦河.md "wikilink")。

## 氣候

賴比瑞亞位於熱帶，氣候偏熱。由於處於[撒哈拉沙漠地帶](../Page/撒哈拉沙漠.md "wikilink")，所以在冬季時晝夜的溫差相對較大；而其海洋性氣候亦在夏季時提供大量雨水，相對的冬季則十分乾燥，首都[蒙罗维亚及西南部地區為非洲多雨區之一](../Page/蒙罗维亚.md "wikilink")。

## 自然資源

當地的自然資源包含[鐵礦](../Page/鐵礦.md "wikilink")、[紅木](../Page/紅木.md "wikilink")、[鑽石](../Page/鑽石.md "wikilink")、[黃金及](../Page/黃金.md "wikilink")[橡膠等](../Page/橡膠.md "wikilink")。其中盛產鐵礦，為非洲第二大鐵礦生產國。糧食方面，賴比瑞亞只能出產[玉米和](../Page/玉米.md "wikilink")[甘薯](../Page/甘薯.md "wikilink")，其他糧食需由外國進口。

## 土地利用和農業

耕地：1%
固定農作物：3%
永久牧場:：9%
森林和森林地帶：18%
其他：19%
*(截至1993年)*

## 自然災害

當地在12月至3月間，會受到來自[撒哈拉沙漠的](../Page/撒哈拉沙漠.md "wikilink")[沙塵暴所侵襲](../Page/沙塵暴.md "wikilink")，在夏季時，亦會出現暴雨。

## 環境問題

賴比瑞亞面臨著[熱帶雨林被砍伐](../Page/熱帶雨林.md "wikilink")、土壤[侵蝕](../Page/侵蝕.md "wikilink")、[生物多樣性消失](../Page/生物多樣性.md "wikilink")、沿海水域受油滓和未經處理的污水所污染、[沙漠化等環境問題](../Page/沙漠化.md "wikilink")。

## 外部链接

  - [WWW.XZQH 利比里亚网頁
    中文](https://web.archive.org/web/20051019132118/http://www.xzqh.org/waiguo/africa/3031.htm)

[Category:利比里亚](../Category/利比里亚.md "wikilink")