**雙帶普提魚**，又稱**雙月斑狐鯛**，俗名紅娘仔、黃鶯魚，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[隆頭魚亞目](../Page/隆頭魚亞目.md "wikilink")[隆頭魚科的其中一](../Page/隆頭魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[東非](../Page/東非.md "wikilink")、[南非](../Page/南非.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[馬爾地夫](../Page/馬爾地夫.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[中國](../Page/中國.md "wikilink")[南海](../Page/南海.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[庫克群島](../Page/庫克群島.md "wikilink")、[夏威夷](../Page/夏威夷.md "wikilink")、[法屬玻里尼西亞](../Page/法屬玻里尼西亞.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[马里亚纳群岛](../Page/马里亚纳群岛.md "wikilink")、[美屬薩摩亞](../Page/美屬薩摩亞.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[諾魯](../Page/諾魯.md "wikilink")、[索羅門群島](../Page/索羅門群島.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[斐濟群島](../Page/斐濟群島.md "wikilink")、[萬那杜](../Page/萬那杜.md "wikilink")、[吉里巴斯](../Page/吉里巴斯.md "wikilink")、[東加等海域](../Page/東加.md "wikilink")。

## 深度

水深3至30公尺。

## 特徵

本魚背鰭硬棘間缺刻明顯。尾鰭截形，上下葉略突出。體長型，側扁。上下頜突出，前側具
4強犬齒，上頜每側具一大圓犬齒。頰部與鰓蓋被鱗；下頜無鱗。魚體呈淡紅色，腹面顏色較淡，在背鰭第一及第二棘間有一黑斑，成魚在背鰭基底末端和側線間有一8至9鱗片長的鞍狀大黑斑。幼魚身上有許多平行的紅色縱紋，體後鞍狀斑寬，和背鰭、臀鰭相連，背鰭末端和臀鰭黑色。背鰭硬棘12枚、背鰭軟條10枚、臀鰭硬棘3枚、臀鰭軟條12枚。體長可達60公分。

## 生態

本魚棲息在岩礁區，以底棲無脊椎動物為食。

## 經濟利用

為食用性魚類，[清蒸](../Page/清蒸.md "wikilink")、[紅燒均可](../Page/紅燒.md "wikilink")，而其幼魚體色鮮明亦可為觀賞用。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[Category:食用魚](../Category/食用魚.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[bilunulatus](../Category/普提魚屬.md "wikilink")