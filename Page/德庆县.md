**德庆县**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[肇庆市下辖的一个县](../Page/肇庆市.md "wikilink")。面积2,258平方千米，人口35万。邮政编码526600。

## 行政区划

下辖1个街道、14个镇：

  - 街道办事处：德城街道。
  - 镇：新圩镇、官圩镇、马圩镇、高良镇、莫村镇、永丰镇、武垄镇、播植镇、凤村镇、九市镇、悦城镇、回龙镇、沙旁镇、古有镇。

县人民政府驻德城街道。

## 歷史

[漢武帝](../Page/漢武帝.md "wikilink")[元鼎六年](../Page/元鼎.md "wikilink")（公元前111年）建縣。唐時劃歸[端州贞观元年](../Page/端州.md "wikilink")（627年）又歸[南康州](../Page/南康州.md "wikilink")，十二年改为康州。
[南宋紹興元年](../Page/南宋.md "wikilink")（1131年）升为德慶府，以示「因德致慶」。[民國元年](../Page/民國.md "wikilink")（1912年）改為德慶縣。

## 地理

德慶縣屬低山[丘陵區](../Page/丘陵.md "wikilink")，東北西三面環山，[西江從南部縣境流過](../Page/西江.md "wikilink")。

## 氣候

1月均溫12.6℃，7月均溫28.6℃，年均降水量1,151毫米。

## 自然資源

礦藏：[石灰石](../Page/石灰石.md "wikilink")、[花崗石](../Page/花崗石.md "wikilink")、[鉭](../Page/鉭.md "wikilink")、[鈮](../Page/鈮.md "wikilink")、[錫](../Page/錫.md "wikilink")、[金等](../Page/金.md "wikilink")。

野生動物：[穿山甲](../Page/穿山甲.md "wikilink")、[黃猄](../Page/黃猄.md "wikilink")、[果子狸](../Page/果子狸.md "wikilink")、[芒花狸](../Page/芒花狸.md "wikilink")、[白筍狸](../Page/白筍狸.md "wikilink")、[貓狸](../Page/貓狸.md "wikilink")、[箭豬](../Page/箭豬.md "wikilink")、[貓頭鷹](../Page/貓頭鷹.md "wikilink")、[石蛤](../Page/石蛤.md "wikilink")、[北鵑](../Page/北鵑.md "wikilink")、[畫眉等](../Page/畫眉.md "wikilink")。

林木：[紅椎](../Page/紅椎.md "wikilink")、[毛椎](../Page/毛椎.md "wikilink")、[椆木](../Page/椆木.md "wikilink")、[格木](../Page/格木.md "wikilink")、[黃杞](../Page/黃杞.md "wikilink")、[大水松](../Page/大水松.md "wikilink")。

## 經濟

[農業](../Page/農業.md "wikilink") -
主種[稻](../Page/稻.md "wikilink")、[木薯](../Page/木薯.md "wikilink")、[甘蔗等](../Page/甘蔗.md "wikilink")，建立了[松脂](../Page/松脂.md "wikilink")、[水果](../Page/水果.md "wikilink")、[南藥](../Page/南藥.md "wikilink")、[肉桂](../Page/肉桂.md "wikilink")、[蠶桑等五大基地](../Page/蠶桑.md "wikilink")，有「綠色銀行」之稱。德庆是全国柑桔产业十强县和中国果蔬无公害十强县。

[工業](../Page/工業.md "wikilink") -
林產[化工](../Page/化工.md "wikilink")、[稀土冶煉](../Page/稀土.md "wikilink")、建材石料、輕紡服裝、食品飲料、機械電子等。

2002年全縣國內生產總值36億2千萬[人民幣](../Page/人民幣.md "wikilink")。

## 交通

  - 公路：[321國道從縣境南部穿越](../Page/321國道.md "wikilink")；
  - 水路：有德慶、[悅城等](../Page/悅城.md "wikilink")[港口](../Page/港口.md "wikilink")
  - 铁路：[柳肇铁路](../Page/柳肇铁路.md "wikilink")（规划中）、[南廣鐵路](../Page/南廣鐵路.md "wikilink")（在縣城西江對岸[南江口設站](../Page/南江口鎮.md "wikilink")\[1\]）

## 风景名胜

  - [德庆学宫](../Page/德庆学宫.md "wikilink")，位于城内朝阳西路，始建于宋，重建于元，大成殿是广东唯一一座元代木构建筑。1996年被列为第四批全国重点文物保护单位。
  - [悦城龙母祖庙](../Page/悦城龙母祖庙.md "wikilink")，位于悦城镇，是祀奉龙母蒲媪的庙宇，现存建筑基本为清代所建，其砖、木、石雕工艺精湛，是岭南古建筑的典范。2001年被列为第五批全国重点文物保护单位。
  - [三元塔](../Page/三元塔_\(德庆\).md "wikilink")，位于城东郊[德庆西江大桥北岸东侧的白沙山上](../Page/德庆西江大桥.md "wikilink")，是一座明代楼阁式砖塔，平面八角，外观9层，内分17层，高58.13米，1989年被列为广东省文物保护单位。
  - [三洲岩摩崖石刻](../Page/三洲岩摩崖石刻.md "wikilink")，位于九市镇三洲村，现存宋至清代摩崖石刻91刻，具有较高的历史、文物价值，2002年被列为广东省文物保护单位。
  - [华表石](../Page/华表石.md "wikilink")，又名锦石山、锦裹石，位于[回龙镇建丰村的](../Page/回龙镇.md "wikilink")[西江北岸](../Page/西江.md "wikilink")。海拔173米。属[丹霞地貌](../Page/丹霞地貌.md "wikilink")。[华表石摩崖石刻为广东古代最大的摩崖石刻](../Page/华表石摩崖石刻.md "wikilink")，2002年被列为广东省文物保护单位。\[2\]
  - [龙山宫](../Page/龙山宫.md "wikilink")，位于官圩镇安良村社咀山，明代建筑，1989年被列为广东省文物保护单位。
  - [盤龍峽瀑布堪稱是目前廣東省內最優美的瀑布](../Page/盤龍峽瀑布.md "wikilink")。

## 特產

  - [贡柑](../Page/贡柑.md "wikilink")、[砂糖桔](../Page/砂糖桔.md "wikilink")、[何首烏](../Page/何首烏.md "wikilink")、[巴戟](../Page/巴戟.md "wikilink")、[巴戟酒](../Page/巴戟酒.md "wikilink")、[巴戟皇](../Page/巴戟皇.md "wikilink")、[桂皮](../Page/桂皮.md "wikilink")、[桂油](../Page/桂油.md "wikilink")、[沙田柚等](../Page/沙田柚.md "wikilink")。
  - 美食：[泥燴雞](../Page/泥燴雞.md "wikilink")、[竹篙粉](../Page/竹篙粉.md "wikilink")

## 參考文獻

## 外部連結

  - [肇慶市德慶縣人民政府](http://www.deqing.gd.cn/)

[德庆县](../Category/德庆县.md "wikilink")
[肇庆](../Category/广东省县份.md "wikilink")
[县](../Category/肇庆区县市.md "wikilink")

1.  [南广铁路云浮段有望今年建成通车
    云浮日报](http://www.yunfudaily.com/Category_138/Index.aspx?stime=2013-03-07&pid=1&gid=126&itemid=145809)
     2013-03-07
2.