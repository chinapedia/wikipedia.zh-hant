**皇后樂團**（）是[英国](../Page/英国.md "wikilink")[摇滚乐](../Page/摇滚乐.md "wikilink")[乐队](../Page/乐队.md "wikilink")，成立於1970年，成員包括主唱[弗雷迪·莫库里](../Page/弗雷迪·莫库里.md "wikilink")、吉他手[布赖恩·梅](../Page/布赖恩·梅.md "wikilink")、鼓手[罗杰·泰勒](../Page/罗杰·泰勒.md "wikilink")、貝斯手[约翰·迪肯](../Page/约翰·迪肯.md "wikilink")，樂團最初期的作品受到[前衛搖滾](../Page/前衛搖滾.md "wikilink")、[硬搖滾和](../Page/硬搖滾.md "wikilink")[重金屬音樂的影響](../Page/重金屬音樂.md "wikilink")，後來逐漸嘗試往傳統和電台廣播的作品邁進，將其他風格像[歌劇](../Page/歌劇.md "wikilink")、[民謠](../Page/民謠.md "wikilink")、[藝術搖滾](../Page/藝術搖滾.md "wikilink")、和[流行搖滾融入音樂中](../Page/流行搖滾.md "wikilink")，對世界樂壇留下深遠影響。

其在[英國國內的地位尤其高](../Page/英國.md "wikilink")，英國史上專輯銷售前十，包括最高位在內，Queen名留兩位(\#1、\#7)\[1\]。生涯專輯於英國專輯銷售榜週數總計達1322周(27年)，為史上最長\[2\]。他們的唱片銷量預計在1.5億至3億張之間，成為世界上最暢銷的音樂藝術家之一。是一個比齊柏林飛船好更多的樂團。皇后樂團於1990年獲得英國唱片業對英國音樂的傑出貢獻獎。他們於2001年入選[搖滾名人堂](../Page/搖滾名人堂.md "wikilink")。他們的每位成員都有創作過多首的熱門單曲，並且在2003年四名樂團成員都入選了。

1991年，[弗雷迪·莫库里過世](../Page/弗雷迪·莫库里.md "wikilink")；1997年，[约翰·迪肯退休](../Page/约翰·迪肯.md "wikilink")。現任團員剩下[布赖恩·梅](../Page/布赖恩·梅.md "wikilink")、[罗杰·泰勒](../Page/罗杰·泰勒.md "wikilink")。

## 成員與名曲創作

[<File:Queen_crest.png>](https://zh.wikipedia.org/wiki/File:Queen_crest.png "fig:File:Queen_crest.png")

  - [弗雷迪·莫库里](../Page/弗雷迪·莫库里.md "wikilink")

<!-- end list -->

  -
    [钢琴手和](../Page/钢琴.md "wikilink")[主唱](../Page/主唱.md "wikilink")。知名詞曲作品有：《[Bohemian
    Rhapsody](../Page/Bohemian_Rhapsody.md "wikilink")》（*波希米亚狂想曲*）、《[We
    are the Champions](../Page/我们是冠军.md "wikilink")》（*我们是冠军*）、《[Somebody
    to
    Love](../Page/Somebody_to_Love_\(皇后乐队歌曲\).md "wikilink")》（*找一个人来爱*）、《[Killer
    Queen](../Page/Killer_Queen.md "wikilink")》（*杀手女王*）、《\[\[:en:Crazy
    Little Thing Called Love|Crazy Little Thing Called Love

`  ]]》（`*`那疯狂的小东西就是爱`*`）等。`

  - [布赖恩·梅](../Page/布赖恩·梅.md "wikilink")

<!-- end list -->

  -
    [吉他手](../Page/吉他.md "wikilink")。知名詞曲作品有：《[We Will Rock
    You](../Page/We_Will_Rock_You.md "wikilink")》（*我们将震撼你*）、《[Tie Your
    Mother
    Down](../Page/:en:Tie_Your_Mther_Down.md "wikilink")》（*把你的母亲關起來*）、《[The
    Show Must Go
    On](../Page/The_Show_Must_Go_On.md "wikilink")》（*表演必須继续*）、《[Hammer
    to Fall](../Page/:en:Hammer_to_Fall.md "wikilink")》（*錘子落下*）、《[Fat
    Bottomed
    Girls](../Page/:en:Fat_Bottomed_Girls.md "wikilink")》（*大屁股女孩*）等。

<!-- end list -->

  - [罗杰·泰勒](../Page/罗杰·泰勒.md "wikilink")

<!-- end list -->

  -
    [鼓手及](../Page/鼓.md "wikilink")[打击乐器](../Page/打击乐器.md "wikilink")。知名詞曲作品有：《》、《》、《》、《》、《》、《》等。

<!-- end list -->

  - [约翰·迪肯](../Page/约翰·迪肯.md "wikilink")

<!-- end list -->

  -
    [贝斯手及節奏吉他](../Page/低音吉他.md "wikilink")。知名詞曲作品有：《》、《》、《》、《》、《》等。

## 經歷

### 樂團組成與早年（1968 - 1973）

1968年，就讀於[伦敦帝国学院的](../Page/伦敦帝国学院.md "wikilink")[吉他手布赖恩](../Page/吉他手.md "wikilink")·梅，和就讀於伊令藝術學院的[貝斯手提姆](../Page/貝斯手.md "wikilink")·史塔費爾（Tim
Staffell）兩人想組樂團，但還缺鼓手，於是貼出告示徵人。這時年輕的牙科學生罗杰·泰勒前來應徵，三人組的樂團成立，此時的團名為「**[Smile](../Page/Smile.md "wikilink")**」。提姆將樂團介紹給他的同學弗雷迪·莫库里，而莫库里立刻愛上了「Smile」的音樂。

1970年，提姆·史塔費爾離團，這時仍只是樂迷身分的莫库里，建議樂團改名為「**Queen**」，在成員同意下正式改名，這也是日後將沿用數十年的名字。不久後莫库里自己加入樂團擔任主唱。之後陸續換了幾個貝斯手，但都不太合，直到1971年2月，貝斯手迪肯加入後成員才定案，並在未來二十年再也沒有更動，這也就是今日一般所稱的四名「皇后樂團」成員：布赖恩·梅、罗杰·泰勒、弗雷迪·莫库里、约翰·迪肯（依加入時間排序）。

1973年，發表出道專輯《[Queen](../Page/Queen_\(同名專輯\).md "wikilink")》，但未取得市場成功。

### 突破與商業成功（1974 - 1979）

1974年，發表第三張專輯《Sheer Heart
Attack》，本作在英國登上第二名並達雙白金唱片，這是樂團第一次得到在商業上得到可觀成功。自此開始，風格由最初兩張作品較前衛另類的風格，轉向較大眾化適於在電台播放的路線。1975年，第一次前往美國與加拿大巡迴演唱。

1975年，發表第四張專輯《A Night At The
Opera》，這是當時史上製作成本最昂貴的唱片。本作獲得極大成功，在英國達四白金，在美國達三白金。其中一曲「[Bohemian
Rhapsody](../Page/Bohemian_Rhapsody.md "wikilink")」更創下當時英國史上單曲銷售最高紀錄（現為第三），並在未來三十餘年不斷被西方樂壇評為史上最偉大的歌曲。該曲的[MV也常被認為是史上第一個真正的MV](../Page/MV.md "wikilink")。

### 新風格，合成器的使用（1980 - 1984）

1980年，發表專輯《The Game》，這是他們使用合成器製作音樂的開始。

1981年，發表第一張精選輯《Greatest Hits》，英國銷量達540萬（十八白金），成為英國史上銷售最高專輯，此紀錄保持至今。

1982年，發表專輯《Hot
Space》。本作加入了許多[Funk](../Page/Funk.md "wikilink")、[Disco元素](../Page/Disco.md "wikilink")，與過去的搖滾風格差異頗大，因此受到不少批評，得到的成功也較小。

### Live Aid與晚期（1985 - 1989）

1985年參加[拯救生命演唱](../Page/拯救生命.md "wikilink")，該演唱日後無數次被評為歷史上最傑出的現場演唱之一。

1986年，發表專輯《A Kind of Magic》，舉辦「[Magic
Tour](../Page/Magic_Tour.md "wikilink")」大型巡迴演唱，總計觀眾超過一百萬，英國國內超過四十萬，創下當時英國史上演唱會觀眾數字記錄。

### 最終作品與莫库里之死及迪肯退休（1990 - 1997）

1988年，此時開始，因莫库里日漸消瘦的外貌，[英國媒體開始傳言他得到了](../Page/英國.md "wikilink")[愛滋病](../Page/愛滋病.md "wikilink")。此傳言甚囂塵上，但他本人仍然否認自己的病情：「我只是太累了」，並且持續拒絕接受訪問。雖然面對這嚴重的問題，樂團仍繼續製作新作。這是莫库里在世時發表的最後一張專輯《Innuendo》，於1991年2月發表。[1991年](../Page/1991.md "wikilink")11月23日，弗雷迪·莫库里公開發表了自己的病情，一天后，弗雷迪便去世了：

1991年11月24日，主唱兼主要作曲者莫库里過世，團員減為三人，同時發表第二張精選輯《Greatest Hits
II》，銷量達360萬，為史上銷售第7專輯。

1997年，[约翰·迪肯與其他團員及](../Page/约翰·迪肯.md "wikilink")[艾爾頓強進行最後一次的現場演出](../Page/艾爾頓強.md "wikilink")（曲目：[The
Show Must Go
On](../Page/The_Show_Must_Go_On.md "wikilink")）之後，於音樂事業退休。團員減為兩人。

## "Queen+"合作計畫（2004 - 現在）

### Queen + Paul Rodgers (2004 - 2009)

2004年，兩名團員和[保羅·羅傑斯](../Page/保羅·羅傑斯.md "wikilink")（Paul
Rodgers）開始固定合作，以「Queen + Paul Rodgers」之名活動。

2008年，在莫库里死後第十七年，發表新原創專輯《The Cosmos
Rocks》。這是十七年來第一張全由現任成員創作的作品。演唱由合作夥伴保羅·羅傑斯負責大部分。

在2009年中期與保羅·羅傑斯分道揚鑣之後，10月份布萊恩在他的網站表示，2010年Queen將不會開任何一場演唱會，但這不表示Queen在這一年不會有任何演出。11月份發表了精選集《Absolute
Greatest》，這是樂團的第六張，也是繼1999年的《Greatest Hits III》以來的第一張精選集。

2010年5月7日兩人表示已經離開[EMI](../Page/EMI.md "wikilink")，結束了長達四十年的合作關係。

### Queen + Adam Lambert（2011 - 2018）

2011年3月14日，環球音樂重新發行了《Queen》、《Queen II》、《Sheer Heart Attack》、《A Night at
the Opera》、《A Day at the Races》這前五張專輯。

2011年4月，樂團與[亞當·藍伯特](../Page/亚当·兰伯特.md "wikilink") (Adam
Lambert)登上美國偶像第十一季，這是他們固定合作的第一次登台。

環球音樂公佈將於2011年6月27日，發行第二波的復刻專輯，分別是《News of the World》、《Jazz》、《The
Game》、《Flash Gordon》、《Hot Space》。

2012年8月12日吉他手布萊恩·梅、鼓手羅傑·泰勒與另一位女歌手Jessie J在倫敦奧運閉幕式表演《We Will Rock You》

2014年吉他手布萊恩·梅宣布將在年內發行一張新專輯名為Queen Forever,裡面收錄了80\~90年代的DEMO。

2016年，Queen和Adam Lambert於9月19日首度來台開唱，吸引逾7000名樂迷一同參予\[3\]。

### 傳記電影《波希米亞狂想曲》（2018- 至今）

2018年， Queen现任成员 Brian May 和 Roger Taylor
监制的傳記電影《[波希米亞狂想曲](../Page/波希米亞狂想曲_\(電影\).md "wikilink")》上映，同时发行同名专辑。年底樂團宣布2019年開始狂想曲巡迴演唱會。

2019年1月，傳記電影《波希米亞狂想曲》获[金球奖最佳戏剧奖和最佳男主演](../Page/金球奖.md "wikilink")\[4\]。

### 專輯

  - 《[Queen](../Page/Queen_\(同名專輯\).md "wikilink")》（1973年7月13日）
  - 《[Queen II](../Page/Queen_II.md "wikilink")》（1974年3月8日）
  - 《[Sheer Heart
    Attack](../Page/Sheer_Heart_Attack.md "wikilink")》（1974年11月8日）
  - 《[A Night at the
    Opera](../Page/A_Night_at_the_Opera.md "wikilink")》（1975年11月21日）
  - 《[A Day at the
    Races](../Page/A_Day_at_the_Races.md "wikilink")》（1976年12月10日）
  - 《[News of the
    World](../Page/News_of_the_World.md "wikilink")》（1977年10月28日）
  - 《[Jazz](../Page/Jazz_\(专辑\).md "wikilink")》（1978年11月10日）
  - 《[The Game](../Page/The_Game_\(专辑\).md "wikilink")》（1980年6月30日）
  - 《[Hot Space](../Page/Hot_Space.md "wikilink")》（1982年5月21日）
  - 《[The Works](../Page/The_Works.md "wikilink")》（1984年2月27日）
  - 《[A Kind of
    Magic](../Page/A_Kind_of_Magic.md "wikilink")》（1986年6月2日）
  - 《[The Miracle](../Page/The_Miracle.md "wikilink")》（1989年5月22日）
  - 《[Innuendo](../Page/Innuendo.md "wikilink")》（1991年2月5日）
  - 《[Made in
    Heaven](../Page/Made_in_Heaven.md "wikilink")》（1995年11月23日）
  - 《[Queen Forever](../Page/Queen_Forever.md "wikilink")》（2014年）
  - 《[Bohemian Rhapsody (The Original
    Soundtrack)](../Page/Bohemian_Rhapsody_\(The_Original_Soundtrack\).md "wikilink")》
    (2018年)

#### Queen + Paul Rodgers

  - 《[The Cosmos
    Rocks](../Page/The_Cosmos_Rocks.md "wikilink")》（2008年9月15日）

#### Queen + Adam Lambert

  - 《Live in Japan》（2016年12月20日）

## 遊戲

  - 《[Queen: The eYe](../Page/Queen:_The_eYe.md "wikilink")》（1998年）
  - 《[SingStar:
    Queen](../Page/SingStar:_Queen.md "wikilink")》（2009年3月20日）

## 参考文献

## 外部链接

  - [皇后官方网站](http://www.queenonline.com/)
  - [The Queen Discography](http://www.jugi3.ch/homepage/queen.htm)

[Category:英国搖滾乐团](../Category/英国搖滾乐团.md "wikilink")
[Category:皇后乐队](../Category/皇后乐队.md "wikilink")
[Category:摇滚名人堂入选者](../Category/摇滚名人堂入选者.md "wikilink")
[Category:艾弗·诺韦洛奖获得者](../Category/艾弗·诺韦洛奖获得者.md "wikilink")
[Category:帕洛風唱片歌手](../Category/帕洛風唱片歌手.md "wikilink")

1.  [BBC NEWS : Queen head all-time sales
    chart](http://news.bbc.co.uk/1/hi/entertainment/6151050.stm)
2.  <http://news.bbc.co.uk/1/hi/entertainment/music/4648611.stm>
3.
4.   Golden Globes|accessdate=|author=|date=|publisher=}}