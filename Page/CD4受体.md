**CD4受体**，全称“表面抗原分化簇4受体”(Cluster of Differentiation 4
receptors）。在分子生物中，CD4是免疫細胞(例如:[輔助T細胞](../Page/輔助型T細胞.md "wikilink")、[單核球](../Page/单核细胞.md "wikilink")、[巨噬細胞和](../Page/巨噬细胞.md "wikilink")[樹突細胞](../Page/樹狀細胞.md "wikilink"))表面的醣蛋白分子。它被發現於1970年代晚期，在1984年以前被稱為leu-3和T4。人類的CD4蛋白質是由CD4基因所製造。

CD4受体是[辅助T细胞的表面標記](../Page/辅助T细胞.md "wikilink")（surface
markers）之一，也是辅助T细胞行使其功能的重要[受體](../Page/受體.md "wikilink")。當[抗原呈递細胞](../Page/抗原呈递細胞.md "wikilink")（主要是[巨噬細胞](../Page/巨噬細胞.md "wikilink")、[棘狀細胞及](../Page/棘狀細胞.md "wikilink")[B細胞本身](../Page/B細胞.md "wikilink")）將外來病菌分解，把抗原與[主要组织相容性复合体結合後](../Page/主要组织相容性复合体.md "wikilink")，呈递給辅助T细胞（即與辅助T细胞表面的CD4受體結合），辅助T细胞再接著刺激B細胞產生[抗體](../Page/抗體.md "wikilink")，此即[體液性免疫反應的基本過程](../Page/體液性免疫反應.md "wikilink")。

CD4+("+"表示陽性，細胞表面存在此蛋白)的輔助T細胞在人類免疫系統中為極重要的白血球。它們通常被稱為「CD4細胞」、「輔助T細胞」或「T4細胞」。它們被稱為「輔助細胞」是因為其中一個主要功能是將訊號送到其他免疫細胞，包括可以殺死感染細胞的CD8胞殺細胞。如果沒有CD4細胞(如HIV感染者、器官移植者)，人體將無法對抗大量的病原菌並暴露於危險中。

## 結構

CD4受體有四個結構域(Domain)，從D<sub>1</sub>到D<sub>4，</sub>可以和其他細胞的細胞表面產生作用。

  - D<sub>1</sub>和D<sub>3</sub>和抗體的變異區類似。
  - D<sub>2</sub>和D<sub>4</sub>和抗體的恆定區類似。

CD4利用D<sub>1</sub>和MHC ll的 β2 domain 產生作用。T細胞表面的CD4受體只能辨識MHC ll
呈現的抗原(而不是MHC l)。

## 功能

CD4受體是一個幫助T細胞受體和抗原呈現細胞產生作用的co-receptor。

## 疾病

## 資料

常用单克隆抗体或代号：T4，Leu3a 主要表达细胞：Tsub，Msub，Thysub T细胞 分子质量(kDa)和结构：gp55(IgSF)
功 能：与MCHn类分子结合，信号转导，HIV受体

## 參見

  - [T細胞](../Page/T細胞.md "wikilink")

## 参考文献

## 延伸閱讀

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  -
  - [Mouse CD Antigen
    Chart](http://www.ebioscience.com/ebioscience/whatsnew/mousecdchart.htm)

  - [Human CD Antigen
    Chart](http://www.ebioscience.com/ebioscience/whatsnew/humancdchart.htm)

  - \*[Human Immunodeficiency Virus
    Glycoprotein 120](http://www.bio.davidson.edu/Courses/Molbio/MolStudents/spring2005/Greendyke/gp120.htm)

[Category:免疫学](../Category/免疫学.md "wikilink")