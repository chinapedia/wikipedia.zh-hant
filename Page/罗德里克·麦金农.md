**罗德里克·麦金农**（，），美国[洛克菲勒大学分子神经生物学和](../Page/洛克菲勒大学.md "wikilink")[生物物理学](../Page/生物物理学.md "wikilink")[教授](../Page/教授.md "wikilink")。因对[细胞膜中的离子通道功能的物理化学属性的研究](../Page/细胞膜.md "wikilink")，尤其是[X射线晶体学的](../Page/X射线晶体学.md "wikilink")[蛋白质结构的研究而分享获得了](../Page/蛋白质结构.md "wikilink")2003年[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。与之分享这一奖项的是[彼得·阿格雷](../Page/彼得·阿格雷.md "wikilink")，他因对[離子通道的相關研究而获奖](../Page/離子通道.md "wikilink")\[1\]\[2\]\[3\]。

## 參見

  - [沼正作](../Page/沼正作.md "wikilink")
  - [彼得·阿格雷](../Page/彼得·阿格雷.md "wikilink")

## 參考文獻

[Category:美国神经科学家](../Category/美国神经科学家.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:拉斯克基礎醫學研究獎得主](../Category/拉斯克基礎醫學研究獎得主.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:布蘭戴斯大學校友](../Category/布蘭戴斯大學校友.md "wikilink")
[Category:塔夫茨大学校友](../Category/塔夫茨大学校友.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:麻薩諸塞州人](../Category/麻薩諸塞州人.md "wikilink")
[Category:霍华德·休斯医学研究所研究员](../Category/霍华德·休斯医学研究所研究员.md "wikilink")
[Category:霍维茨奖获得者](../Category/霍维茨奖获得者.md "wikilink")
[Category:盖尔德纳国际奖获得者](../Category/盖尔德纳国际奖获得者.md "wikilink")

1.
2.  [诺贝尔奖记录](http://nobelprize.org/nobel_prizes/chemistry/laureates/2003/press.html)
3.  <http://www.ch.ntu.edu.tw/nobel/2003.html>