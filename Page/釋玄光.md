**釋玄光**（，），俗名**黎廷閒**，
是一名[越南](../Page/越南.md "wikilink")[大乘佛教](../Page/大乘佛教.md "wikilink")[僧人](../Page/和尚.md "wikilink")，不受官方承认的[越南佛教聯合會的僧統及创办者](../Page/越南佛教聯合會.md "wikilink")。越南异见人士。由于他主张[信仰自由](../Page/信仰自由.md "wikilink")，曾遭到[越南政府的放逐](../Page/越南政府.md "wikilink")。

## 外部链接

  - [数以千计信徒参加越南异议高僧葬礼](http://news.bbc.co.uk/chinese/simp/hi/newsid_7500000/newsid_7501800/7501841.stm)

[Category:越南佛教出家眾](../Category/越南佛教出家眾.md "wikilink")
[Category:平定省人](../Category/平定省人.md "wikilink")
[Category:越南持不同政見者](../Category/越南持不同政見者.md "wikilink")
[Category:越南人權活動家](../Category/越南人權活動家.md "wikilink")
[Category:越南民主運動人物](../Category/越南民主運動人物.md "wikilink")
[Category:越南反共主義者](../Category/越南反共主義者.md "wikilink")