**鄭邦鎮**（），[臺灣](../Page/臺灣.md "wikilink")[彰化縣](../Page/彰化縣.md "wikilink")[員林鎮人](../Page/員林市.md "wikilink")，[臺灣文學研究者](../Page/臺灣文學.md "wikilink")、教育工作者。

1975年以論文〈說文省聲探賾〉得[輔仁大學碩士學位](../Page/輔仁大學.md "wikilink")，1987年以論文〈明代前期八股文形構研究〉得[國立臺灣大學博士學位](../Page/國立臺灣大學.md "wikilink")，歷任[靜宜大學副教授](../Page/靜宜大學.md "wikilink")、兼[中國文學系主任](../Page/中國文學.md "wikilink")、兼臺灣研究中心主任和[國立臺灣文學館館長](../Page/國立臺灣文學館.md "wikilink")。

鄭邦鎮主要研究中國明清[八股文研究](../Page/八股文.md "wikilink")、中國明清[小品文研究與](../Page/小品文.md "wikilink")[作文方法等](../Page/作文.md "wikilink")，對臺灣文學也有研究；在學校開授[應用文](../Page/應用文.md "wikilink")、臺灣文學紀事編年漢語文言、報紙閱讀與議題探索、中國簡體字概論等課程。

他提交給任教學校的自己的學術專長有8，依序是：八股文研究、小品文、[漢語文言](../Page/漢語.md "wikilink")、中國[簡體字](../Page/簡體字.md "wikilink")、應用文書、作文方法、[演說學](../Page/演說.md "wikilink")、臺灣文學。1990年獲得[中國文藝協會所頒發的](../Page/中國文藝協會.md "wikilink")[中國文藝獎章](../Page/中國文藝獎章.md "wikilink")[文學批評獎](../Page/文學批評.md "wikilink")。

鄭邦鎮也是[台灣教授協會的成員之一](../Page/台灣教授協會.md "wikilink")，政治立場傾向[臺灣獨立](../Page/臺灣獨立.md "wikilink")；1996年鄭邦鎮參與發起[建國黨](../Page/建國黨.md "wikilink")，並且於1997年代表建國黨參選-{臺中市}-長，1998年代表建國黨於[臺中市參選第](../Page/臺中市.md "wikilink")4屆[立法委員](../Page/立法委員.md "wikilink")，都未能當選。1999年當選第3屆建國黨主席，並且於同年8月宣佈參選[中華民國總統](../Page/中華民國總統.md "wikilink")，並與[黃玉炎搭檔參選](../Page/黃玉炎.md "wikilink")，但是由於未能超過連署門檻而未能參與總統大選，2000年1月6日宣布退選。

鄭邦鎮前後擔任靜宜大學中國文學系主任9年，大力提倡臺灣中國語文學界改革，及推動臺灣文學、語言系所的設立。期間曾接受文建會和所屬國立臺灣文學館的委託，策劃編纂2001年到2004年的《臺灣文學年鑑》。

鄭邦鎮在2007年8月15日被文建會借調到國立臺灣文學館擔任館長，為該館第1位和僅有的1位副教授級別職稱調任的館長，且大幅延長開放時間，每個開放日都從上午9時開放到晚上9時。2010年2月1日交接卸任，由[李瑞騰接棒](../Page/李瑞騰.md "wikilink")。同時，鄭邦鎮也是自2003年國立臺灣文學館正式開館營運以來的首任館長；並於其卸任前夕，獲頒三等功績獎章。

2010年，鄭邦鎮擔任[臺南市政府教育局首任局長](../Page/臺南市政府.md "wikilink")。

2013年11月1日，鄭邦鎮以臺南市政府教育局局長身份前往探視九名因[食物中毒住院的台南市崇明國中學生](../Page/食物中毒.md "wikilink")，陪伴多時；其後，疑似在閒談間與學生及家長談到學生手抖不停的問題，鄭邦鎮笑談回覆：「拿個[球棒把他們](../Page/球棒.md "wikilink")（學生）打昏，睡著了就不會再抖了！」次日11月2日[臺南市議員](../Page/臺南市議員.md "wikilink")[蔡淑惠質詢此事](../Page/蔡淑惠.md "wikilink")，鄭未承認此事。媒體追蹤採訪後，鄭邦鎮改向家長與學生表達歉意，市府並以「無心之過」希望外界不要過度渲染。

## 外部連結

  - [臺灣靜宜大學研究發展處鄭邦鎮著作網頁](http://alcat.pu.edu.tw/~research/search/index_search_result.html?department=51&teaname=aolj0ppozHvzpkupkuoo)
  - [國立臺灣文學館](http://www.nmtl.gov.tw/)
  - [臺灣作家作品目錄系統](http://www3.nmtl.gov.tw/writer2/)

[Category:臺南市教育局局長](../Category/臺南市教育局局長.md "wikilink")
[Category:國立臺灣文學館館長](../Category/國立臺灣文學館館長.md "wikilink")
[Category:台灣文學家](../Category/台灣文學家.md "wikilink")
[Category:建國黨主席](../Category/建國黨主席.md "wikilink")
[Category:臺灣獨立運動者](../Category/臺灣獨立運動者.md "wikilink")
[Category:靜宜大學教授](../Category/靜宜大學教授.md "wikilink")
[Category:國立臺灣大學文學院校友](../Category/國立臺灣大學文學院校友.md "wikilink")
[Category:輔仁大學校友](../Category/輔仁大學校友.md "wikilink")
[Category:員林人](../Category/員林人.md "wikilink")
[Bang邦](../Category/郑姓.md "wikilink")