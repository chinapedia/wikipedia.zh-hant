[Rhapsody_in_Blue_cover.png](https://zh.wikipedia.org/wiki/File:Rhapsody_in_Blue_cover.png "fig:Rhapsody_in_Blue_cover.png")
《**藍色狂想曲**》（*Rhapsody in
Blue*）是作曲家[喬治·蓋希文](../Page/喬治·蓋希文.md "wikilink")（George
Gershwin）受到於1923年11月1日在風神演奏廳中一場融合古典音樂和爵士樂實驗音樂會所啟發\[1\]，於1924年寫給獨奏[鋼琴及](../Page/鋼琴.md "wikilink")[爵士樂團的樂曲](../Page/爵士樂.md "wikilink")，它融合了[古典音樂的原理以及爵士的元素](../Page/古典音樂.md "wikilink")。

此曲的首演一般是認為在一個標題為「現代音樂實驗」的音樂會中，於1924年2月12日[紐約](../Page/紐約.md "wikilink")[風神音樂廳](../Page/風神音樂廳.md "wikilink")（Aeolian
Hall），由[保羅·懷特曼](../Page/保羅·懷特曼.md "wikilink")（Paul
Whiteman）指揮他的樂團以及蓋希文演奏鋼琴。鋼琴與交響樂團的版本是在1946年由[葛羅菲](../Page/葛羅菲.md "wikilink")（Ferde
Grofé）所改編，它成為了美國音樂會中最受歡迎的曲目之一。

藍色狂想曲讓喬治·蓋希文建立多產作曲家的聲譽。\[2\]

## 管弦代

1924年為23位樂師的懷特曼樂團所做的交響樂版本有\[3\]：

  - [長笛](../Page/長笛.md "wikilink")
  - [雙簧管](../Page/雙簧管.md "wikilink")
  - [豎笛](../Page/豎笛.md "wikilink")
      - [降E調豎笛](../Page/降E調豎笛.md "wikilink")
      - [降B調豎笛](../Page/降B調豎笛.md "wikilink")
      - [次中音豎笛](../Page/次中音豎笛.md "wikilink")
      - [低音豎笛](../Page/低音豎笛.md "wikilink")
  - [赫克管](../Page/赫克管.md "wikilink")
  - [薩克管](../Page/薩克管.md "wikilink")
      - [超高音薩克管](../Page/超高音薩克管.md "wikilink")
      - [高音薩克管](../Page/高音薩克管.md "wikilink")
      - [次中音薩克管](../Page/次中音薩克管.md "wikilink")
      - [中音薩克管](../Page/中音薩克管.md "wikilink")
      - [上低音薩克管](../Page/上低音薩克管.md "wikilink")
  - 2[法國號](../Page/法國號.md "wikilink")
  - 2[小號](../Page/小號.md "wikilink")
  - 2[富魯號](../Page/富魯號.md "wikilink")
  - [上低音號](../Page/上低音號.md "wikilink")
  - 2[長號](../Page/長號.md "wikilink")
  - [低音長號](../Page/低音長號.md "wikilink")
  - [低音號](../Page/低音號.md "wikilink")
  - 2[鋼琴](../Page/鋼琴.md "wikilink")
  - [鐘琴](../Page/鐘琴.md "wikilink")
  - [班卓琴](../Page/班卓琴.md "wikilink")
  - [鼓](../Page/鼓.md "wikilink")
  - [定音鼓](../Page/定音鼓.md "wikilink")
  - [爵士鼓](../Page/爵士鼓.md "wikilink")
  - [小提琴](../Page/小提琴.md "wikilink")
  - [低音提琴](../Page/低音提琴.md "wikilink")
  - [手風琴](../Page/手風琴.md "wikilink")

有樂師演奏2樣以上的樂器。

1942年的交響樂版本有\[4\]：

  - 獨奏鋼琴
  - 2長笛
  - 2雙簧管
  - 2降B調豎笛
  - 低音豎笛
  - 2[低音管](../Page/低音管.md "wikilink")
  - 3法國號
  - 3小號
  - 3長號
  - 低音號
  - 打擊樂器
      - [雙鈸](../Page/雙鈸.md "wikilink")
      - [小鼓](../Page/小鼓.md "wikilink")
      - [大鼓](../Page/大鼓.md "wikilink")
      - [鑼](../Page/鑼.md "wikilink")
      - [三角鐵](../Page/三角鐵.md "wikilink")
      - [鐘琴](../Page/鐘琴.md "wikilink")
      - [繞鈸](../Page/繞鈸.md "wikilink")
      - [定音鼓](../Page/定音鼓.md "wikilink")
  - 鋼琴
  - 2次中音薩克管
  - 中音薩克管
  - 班卓琴
  - [弦樂器](../Page/弦樂器.md "wikilink")
      - 小提琴
      - [中提琴](../Page/中提琴.md "wikilink")
      - [大提琴](../Page/大提琴.md "wikilink")
      - 低音提琴

1942年版本根據1926年版本改編，有1名長笛、雙簧管及巴松管。2名小號、長號及法國號。\[5\]

## 錄音

### 著名錄音版本

  - [James Levine](../Page/James_Levine.md "wikilink")（鋼琴兼指揮）與[Chicago
    Symphony
    Orchestra](../Page/Chicago_Symphony_Orchestra.md "wikilink")
  - [Oscar Levant](../Page/Oscar_Levant.md "wikilink")（鋼琴）、[Eugene
    Ormandy](../Page/Eugene_Ormandy.md "wikilink")（指揮）與[費城管弦樂團](../Page/費城管弦樂團.md "wikilink")
  - [Leonard
    Bernstein](../Page/Leonard_Bernstein.md "wikilink")（鋼琴兼指揮）與[紐約愛樂](../Page/紐約愛樂.md "wikilink")\[6\]，1959年
  - [Earl Wild](../Page/Earl_Wild.md "wikilink")（鋼琴）、[Arthur
    Fiedler](../Page/Arthur_Fiedler.md "wikilink")（指揮）與[Boston Pops
    Orchestra](../Page/Boston_Pops_Orchestra.md "wikilink")，1960年
  - [George
    Gershwin](../Page/George_Gershwin.md "wikilink")（鋼琴）、[Michael
    Tilson Thomas](../Page/Michael_Tilson_Thomas.md "wikilink")（指揮）與[the
    Columbia Jazz
    Band](../Page/the_Columbia_Jazz_Band.md "wikilink")，1976年（首演1924年管弦樂團版本），Columbia
    M34205
  - [Michael Tilson
    Thomas](../Page/Michael_Tilson_Thomas.md "wikilink")（鋼琴兼指揮）與[Los
    Angeles Philharmonic
    Orchestra](../Page/Los_Angeles_Philharmonic_Orchestra.md "wikilink")，1985年
  - [Garrick Ohlsson](../Page/Garrick_Ohlsson.md "wikilink")（鋼琴）、Michael
    Tilson Thomas（指揮）與[三藩市管弦樂團](../Page/三藩市管弦樂團.md "wikilink")

## 花絮

  - 曲子在創作時被稱為《美國幻想曲》。《藍色狂想曲》是由艾拉蓋西文在參觀完[惠斯特的畫展之後所提出的](../Page/惠斯特.md "wikilink")。\[7\]
  - 《藍色狂想曲》的頭開是以[单簧管的](../Page/单簧管.md "wikilink")[顫音接著](../Page/顫音.md "wikilink")17個[圓滑音](../Page/圓滑音.md "wikilink")[音階](../Page/音階.md "wikilink")。在練習中，懷特曼的專業单簧管手[Ross
    Gorman以](../Page/Ross_Gorman.md "wikilink")[滑奏來吹奏開頭的向上音階](../Page/滑奏.md "wikilink")：蓋希文聽到並且一再要求反覆演奏。\[8\]
    [American
    Heritage聲稱這個受歡迎的单簧管圓滑奏開頭已經跟](../Page/American_Heritage.md "wikilink")[貝多芬](../Page/貝多芬.md "wikilink")[第五號交響曲的開頭一樣眾所皆知](../Page/第五號交響曲.md "wikilink")。\[9\]
  - 在1924年，懷特曼的樂團演奏狂想曲84次，並且賣出了百萬張的唱片。\[10\]
    懷特曼稍後以這首曲子當做樂團的主題曲，並且將他的電台節目口號改為「Everything
    new but the ''Rhapsody in Blue」。
  - 雖然蓋希文自己曾說幻想曲就像是美國的音樂萬花筒，《藍色狂想曲》常在[紐約的音樂會演出](../Page/紐約.md "wikilink")。這影響了[伍迪·艾倫的電影](../Page/伍迪·艾倫.md "wikilink")[Manhattan](../Page/Manhattan.md "wikilink")、[迪士尼的電影](../Page/迪士尼.md "wikilink")[幻想曲2000](../Page/幻想曲2000.md "wikilink")\[11\]、還有女性[Barbershop
    quartet](../Page/Barbershop_quartet.md "wikilink")
    Ambiance於1991年所錄制的《藍色狂想曲》[無伴奏合唱版本](../Page/無伴奏合唱.md "wikilink")《紐約狂想曲》。\[12\]
  - 《藍色狂想曲》從1987年成為[聯合航空的廣告音樂](../Page/聯合航空.md "wikilink")。該公司年支付權利金300,000元[美金使用這首樂曲在廣告上](../Page/美金.md "wikilink")。\[13\]
  - [Brian
    Wilson受這個音樂影響很深](../Page/Brian_Wilson.md "wikilink")，在[SMiLE計劃可以說是這個的延伸](../Page/SMiLE.md "wikilink")，它使用了重覆的主題又有明顯的美國歌曲架構。在電影"Beautiful
    Dreamer: Brian Wilson and the Story of SMiLE"中，Brian
    Wilson用鋼琴演奏了《藍色狂想曲》的序奏，接著奏出SMiLE的歌《Heroes and
    Villains》。
  - [Desha Delteil跟](../Page/Desha_Delteil.md "wikilink")[Jean
    Myrio在](../Page/Jean_Myrio.md "wikilink")[Kit-Cat
    Club有根據這首樂曲所做的舞蹈](../Page/Kit-Cat_Club.md "wikilink")，這在[百代電影公司的電影中有記錄](../Page/百代電影公司.md "wikilink")。
  - 1945年由[Robert
    Alda主演關於蓋希文的電影](../Page/Robert_Alda.md "wikilink")，片名叫做[藍色狂想曲](../Page/藍色狂想曲_（電影）.md "wikilink")（Rhapsody
    in Blue）。
  - 在日本，藍色狂想曲被使用在富士電視台的電視劇「[交響情人夢](../Page/交響情人夢.md "wikilink")」（Nodame
    Cantabile）的片尾曲。
  - 2013電影《大亨小傳》（英語：The Great Gatsby），在蓋茨比 - 李奧納多·狄卡皮歐飾演
    首度出場時，背景音樂搭配了藍色狂想曲。
  - 藍色狂想曲的版權在[歐盟於](../Page/歐盟.md "wikilink")2007年到期，在[美國也将于](../Page/美國.md "wikilink")2019年到2027年期间逐步到期。

## 參考資料

  - Downes, Olin (1924). "A Concert of Jazz". *The New York Times*,
    February 13, 1924: p. 16.
  - Greenberg, Rodney (1998). *George Gershwin*. Phaidon Press. ISBN
    0-7148-3504-8.
  - Schiff, David (1997). *Gershwin: Rhapsody in Blue*. Cambridge
    University Press. ISBN 0-521-55077-7.
  - Wood, Ean (1996). *George Gershwin: His Life & Music*. Sanctuary
    Publishing. ISBN 1-960741-74-6.

## 外部連結

  - [Richard Alston
    演奏的《藍色狂想曲》](http://www.youtube.com/watch?v=gmXKalBETSU&NR)於[YouTube](../Page/YouTube.md "wikilink")
  - [Richard Glazier
    演奏的鋼琴獨奏版《藍色狂想曲》](http://www.youtube.com/watch?v=EeS9eT88hFY&mode=related&search=)於[YouTube](../Page/YouTube.md "wikilink")

[分類:钢琴协奏曲](../Page/分類:钢琴协奏曲.md "wikilink")
[分類:富士月九劇主題曲](../Page/分類:富士月九劇主題曲.md "wikilink")
[分類:狂想曲](../Page/分類:狂想曲.md "wikilink")

1.  Schiff p. 53

2.  Schiff back cover

3.  Schiff p. 5-6

4.  Gershwin, George; & Grofé, Ferde (1924, 1942). *George Gershwin's
    Rhapsody in Blue miniature orchestra score*. Warner Brothers.

5.
6.  Greenberg p. 74

7.  Schiff p. 13

8.  Greenberg p. 70

9.  Schwarz, Frederick D. (1999) Time Machine: 1924 Seventy-five Years
    Ago: Gershwin’s Rhapsody. *American Heritage* 50(1), February/March
    1999

10.
11. Solomon, Charles (1999): [Rhapsody in Blue: Fantasia 2000's Jewel in
    the
    Crown](http://www.awn.com/mag/issue4.09/4.09pages/solomonrhapsody.php3)


12. [Ambiance](http://www.singers.com/barbershop/ambiance.html) Quartet
    website, describes the arrangement as "a virtual vocal orchestration
    of relentless intensity and chutzpah"

13. Schiff p. 1