[thumb](../Page/文件:Pyongyang_Metro-1.jpg.md "wikilink")

**平壤地鐵**（）是[朝鮮首都](../Page/朝鮮民主主義人民共和國.md "wikilink")[平壤的](../Page/平壤.md "wikilink")[城市軌道交通系統](../Page/城市軌道交通系統.md "wikilink")，始建於1960年代，1973年9月正式投入營運。地铁隧道为朝鲜战争时期已挖掘好的防空洞\[1\]，深度普遍在22-100米之間，某些山區路段更深入150米。

平壤地鐵於1968年動工，1973年9月6日第一條地鐵線路千里馬線通車；第二條革新線在1975年的朝鮮國慶節通車。現時系統約長24[公里](../Page/公里.md "wikilink")，分成千里馬及革新等兩條支線，合共17個車站，然而其中革新線上的[光明站則在](../Page/光明站_\(平壤\).md "wikilink")[金日成去世後被關閉](../Page/金日成.md "wikilink")，不再使用。

平壤地鐵剛通車時採用了中國[吉林省](../Page/吉林省.md "wikilink")[長春客車廠](../Page/長春.md "wikilink")（現[長春軌道客車股份有限公司](../Page/長春軌道客車.md "wikilink")）的DK4型列車，平壤地鐵目前使用的車廂都是從[德國進口的車廂和朝鮮仿製的德國的車廂](../Page/德國.md "wikilink")，2016年1月1日，朝鮮自主研发生产的新型列車投入營運。

除地下鐵路系統外，平壤亦有三條地面[有軌電車鐵路](../Page/有軌電車.md "wikilink")，採用
[捷克](../Page/捷克.md "wikilink")、德國等國機車以及自行制造的列车。

## 路線


[替代=平壤地鐵路線圖](https://zh.wikipedia.org/wiki/File:P'yongyang_Metro.png "fig:替代=平壤地鐵路線圖")

| 車站名稱                                                                             | 車站接駁路線    | 啟用日期              |
| -------------------------------------------------------------------------------- | --------- | ----------------- |
| 中文                                                                               | 朝鮮文       | 英文                |
| [<span style="color:#FFFFFF;">千里馬線</span>](../Page/千里馬線.md "wikilink")（천리마선，1號線） |           |                   |
| [紅星](../Page/紅星站_\(朝鮮\).md "wikilink")                                           | 붉은별       | Pulgŭnbyŏl        |
| [戰友](../Page/戰友站.md "wikilink")                                                  | 전우        | Chŏnu             |
| [凱旋](../Page/凱旋站_\(平壤\).md "wikilink")                                           | 개선        | Kaesŏn            |
| [統一](../Page/統一站.md "wikilink")                                                  | 통일        | T'ongil           |
| [勝利](../Page/勝利站_\(平壤\).md "wikilink")                                           | 승리        | Sŭngni            |
| [烽火](../Page/烽火站.md "wikilink")                                                  | 봉화        | Ponghwa           |
| [榮光](../Page/荣光站_\(平壤\).md "wikilink")                                           | 영광        | Yŏnggwang         |
| [復興](../Page/復興站.md "wikilink")                                                  | 부흥        | Puhŭng            |
| [<span style="color:#000000;">革新線</span>](../Page/革新線.md "wikilink")（혁신선，2號線）    |           |                   |
| [光復](../Page/光復站_\(平壤\).md "wikilink")                                           | 광복        | Kwangbok          |
| [建國](../Page/建國站_\(平壤\).md "wikilink")                                           | 건국        | Kŏnguk            |
| [黃金谷](../Page/黃金谷站.md "wikilink")                                                | 황금벌       | Hwanggŭmbŏl       |
| [建設](../Page/建設站_\(平壤\).md "wikilink")                                           | 건설        | Kŏnsŏl            |
| [革新](../Page/革新站.md "wikilink")                                                  | 혁신        | Hyŏksin           |
| [戰勝](../Page/戰勝站.md "wikilink")                                                  | 전승        | Chŏnsŭng          |
| [三興](../Page/三興站.md "wikilink")                                                  | 삼흥        | Samhŭng           |
| <s>[光明](../Page/光明站_\(平壤\).md "wikilink")</s>                                    | <s>광명</s> | <s>Kwangmyŏng</s> |
| [樂園](../Page/樂園站.md "wikilink")                                                  | 락원        | Ragwŏn            |

註：自從[朝鮮國家主席](../Page/朝鮮民主主義人民共和國主席.md "wikilink")[金日成去世以後](../Page/金日成.md "wikilink")，[光明站便開始無限期停用](../Page/光明站_\(平壤\).md "wikilink")。

## 平壤地铁车型

[MetroPyongyang.jpg](https://zh.wikipedia.org/wiki/File:MetroPyongyang.jpg "fig:MetroPyongyang.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Pyongyang_metro_station_2.jpg "fig:缩略图")
[People_in_Pyongyang_Metro_01.JPG](https://zh.wikipedia.org/wiki/File:People_in_Pyongyang_Metro_01.JPG "fig:People_in_Pyongyang_Metro_01.JPG")

中國聲稱，1973年平壤地铁正式运营时，在线路上运行的地铁车辆均为新车。其中的4列車廂来自[中国的](../Page/中国.md "wikilink")[DK4车辆](../Page/平壤地铁DK4型电动车组.md "wikilink")，部分于1998年卖回中国，部分几组列车则经过改造，开始运行于[平义线](../Page/平义线.md "wikilink")。平壤地铁车辆宽2.7米，长18.8米（每节），由825伏[直流电](../Page/直流电.md "wikilink")[第三轨驱动](../Page/第三轨供电.md "wikilink")。1998年起平壤市地铁启用了[柏林地铁使用的旧车](../Page/柏林地铁.md "wikilink")。2016年，平壤地铁部分使用本国[金钟泰电力机车联合企业製造的新车](../Page/金钟泰电力机车联合企业.md "wikilink")。

分别使用的如下三种类型的地铁：

  - [Gl](../Page/平壤地铁Gl型电动车组.md "wikilink")（Gisela）“迪斯拉”，源自[东柏林](../Page/东柏林.md "wikilink")，于1978年—1982年间制造，60节双驱动地铁列车，现已停用。
  - [D](../Page/平壤地铁D型电动车组.md "wikilink")（Dora）“多拉”，源自[西柏林](../Page/西柏林.md "wikilink")，于1957年—1965年间制造，108节双驱动地铁列车。

这些车辆原有的广告被覆盖后，外表被重新喷成奶色和红色。部分车厢内部悬掛有[金日成](../Page/金日成.md "wikilink")、[金正日的画像](../Page/金正日.md "wikilink")。由於列車都是[德國制造的](../Page/德國.md "wikilink")，所以控制台上的標籤都用德語書寫。

平壤地铁的新车型於2016年投入服務，外型比舊车型更加现代化，车厢光线更明亮，设有显示停车站和行驶速度等的屏幕，以及老年人和残障人士等专座，朝鲜官方称该车辆其为朝鲜自行生产。\[2\]

## 特点

  - 由於朝鮮政府並不容許商家賣廣告，所以不論地鐵車卡、候車區、車站還是電梯都是有关[朝鮮勞動黨和朝鲜历史的掛畫或壁畫](../Page/朝鮮勞動黨.md "wikilink")。
  - 2013年以前，國外觀光客僅能在導遊的陪伴下，在兩個指定的車站間（[千里馬線](../Page/千里馬線.md "wikilink")[復興站至](../Page/復興站.md "wikilink")[榮光站的路段](../Page/榮光站.md "wikilink")）搭乘平壤地鐵一個站，不過2013年後,朝鮮政府允許遊客坐六個站（[千里馬線](../Page/千里馬線.md "wikilink")[復興站至](../Page/復興站.md "wikilink")[紅星站的路段](../Page/紅星站.md "wikilink")，也就是千里馬線全線）。
  - 大部份地鐵站的候車區都設有多個由[勞動新聞提供的閱報支架](../Page/勞動新聞.md "wikilink")，以供候車的乘客閱讀。

## 相关条目

  - [朝鮮铁路运输](../Page/朝鮮铁路运输.md "wikilink")
  - [韩国首都圈电铁](../Page/韩国首都圈电铁.md "wikilink")
  - [莫斯科地铁](../Page/莫斯科地铁.md "wikilink")
  - [朝鮮旅遊](../Page/朝鮮旅遊.md "wikilink")

## 參考資料

\[3\]

## 外部連結

  - [平壤地下鐵路系統非官方網頁](http://www.pyongyang-metro.com/)
  - [平壤地鐵在urbanrail.com的資料](http://www.urbanrail.net/as/kr/pyongyang/pyongyang.htm)
  - [長春軌道客車股份有限公司](https://web.archive.org/web/20061105184238/http://www.cccar.com.cn/)/

[Category:平壤地铁](../Category/平壤地铁.md "wikilink")
[Category:平壤交通](../Category/平壤交通.md "wikilink")
[Category:亞洲城市軌道交通](../Category/亞洲城市軌道交通.md "wikilink")

1.
2.  [1](http://www.ditiezu.com/thread-431586-1-1.html) 平壤地鐵新車
3.