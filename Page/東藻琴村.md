**東藻琴村**（）為過去位於[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[網走支廳東部的行政區劃](../Page/網走支廳.md "wikilink")，已於2006年3月31日與[女滿別町](../Page/女滿別町.md "wikilink")[合併為新設立的](../Page/市町村合併.md "wikilink")[大空町](../Page/大空町.md "wikilink")。

名稱源自[阿伊努語的](../Page/阿伊努語.md "wikilink")「mokor-to」（睡眠的湖）或「po-kor-to」（有小孩的湖）。

## 歷史

  - 1947年2月11日：網走町改制為[網走市時](../Page/網走市.md "wikilink")，東藻琴村從網走町分村。\[1\]
  - 2006年3月31日：與[女滿別町合併為新設立的](../Page/女滿別町.md "wikilink")「大空町」。

## 產業

以農業、奶酪畜牧業為主，並設有乳酪產品加工廠。

## 交通

### 機場

  - [女滿別機場](../Page/女滿別機場.md "wikilink")（位於[女滿別町](../Page/女滿別町.md "wikilink")）

### 鐵路

  - 過去曾經有[東藻琴村營軌道](../Page/東藻琴村營軌道.md "wikilink")，現已廢止。

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道334號</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a>
<ul>
<li>北海道道102號網走川湯線</li>
</ul></li>
<li>一般道道
<ul>
<li>北海道道767號明生北濱線</li>
<li>北海道道995號東藻琴豐富線</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

  - [藻琴山](../Page/藻琴山.md "wikilink")
  - [藻琴山溫泉芝櫻公園](../Page/藻琴山溫泉芝櫻公園.md "wikilink")

## 教育

### 高等學校

  - 道立北海道東藻琴高等學校

### 中學校

  - 東藻琴村立東藻琴中學校

### 小學校

  - 東藻琴村立東藻琴小學校

## 姊妹市・友好都市

### 日本

  - [冰川町](../Page/冰川町.md "wikilink")（[熊本縣](../Page/熊本縣.md "wikilink")）

## 外部連結

  - [藻琴山溫泉芝櫻公園](http://www.mokotoyama.knc.ne.jp/)

  - [女滿別町・東藻琴村合併協議會](http://www.town.ozora.hokkaido.jp/gappeimh/index.html)

  - [女滿別町・東藻琴村任意合併協議會](http://www.town.ozora.hokkaido.jp/gappeimh/nini/index.html)

  - [大空町社會福祉協議會東藻琴地區事務所](http://blog.livedoor.jp/h_syakyo/)

## 參考資料

<div style="font-size: 85%">

<references />

</div>

[Category:大空町](../Category/大空町.md "wikilink")
[Category:鄂霍次克管內](../Category/鄂霍次克管內.md "wikilink")

1.