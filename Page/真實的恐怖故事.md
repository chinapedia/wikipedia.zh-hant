《**-{真實的恐怖故事}-**》（）簡稱「真恐」，原為[日本](../Page/日本.md "wikilink")[恐怖小說](../Page/恐怖小說.md "wikilink")，其後亦被[富士電視台拍成電視情境劇](../Page/富士電視台.md "wikilink")。

每集都從不同角度看觀眾寄來的實際靈異體驗以及鬼故事，並會請靈異大師解析觀眾寄來的靈異寫真照（），主持者為[稻垣吾郎](../Page/稻垣吾郎.md "wikilink")，2005年和2006年7月曾在[台灣](../Page/台灣.md "wikilink")[緯來日本台以](../Page/緯來日本台.md "wikilink")《**-{毛骨悚然撞鬼經驗}-**》為名播映。

該節目受歡迎的原因除了由[SMAP成員之一的稻垣吾郎主持之外](../Page/SMAP.md "wikilink")，每回擔任助理主持的五位小學生與稻垣吾郎的互動也很有趣。在台灣播出時，曾因有家長檢舉遭[NCC禁止於白天時段重播](../Page/NCC.md "wikilink")，在網路上、相關討論區引發爭議。2012年8月21日再次播出。節目分級改列[保護級](../Page/電視分級制度.md "wikilink")。

## 播出時間

<table>
<thead>
<tr class="header">
<th><p>頻道</p></th>
<th><p>所在地</p></th>
<th><p>播映日期</p></th>
<th><p>播出時間</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p><a href="../Page/緯來日本台.md" title="wikilink">緯來日本台</a></p></td>
<td><center></td>
<td><p>2004年</p></td>
<td></td>
<td><p>秋天正式首播第一季</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td></td>
<td><p>重播第一季</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年7～11月</p></td>
<td></td>
<td><p>第一季重播+第二季首播</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年7月19日～2008年1月24日</p></td>
<td></td>
<td><p>重播第一季+第二季，每週四晚上10點，被NCC處罰停掉的是星期五下午重播時間</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年10月30日～12月6日</p></td>
<td></td>
<td><p>僅重播第一季</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年8月21日～2012年8月30日</p></td>
<td></td>
<td><p>僅「恐怖幽靈郵件」單元中的2006年至2011年，剪輯成共8集</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年9月2日～2012年9月23日</p></td>
<td></td>
<td><p>再度重播</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年8月9日</p></td>
<td></td>
<td><p>僅「恐怖幽靈郵件」單元中的2012年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年7月28日</p></td>
<td></td>
<td><p>僅「恐怖幽靈郵件」單元中的2013年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年7月4日</p></td>
<td></td>
<td><p>僅「恐怖幽靈郵件」單元中的2014年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年10月10日</p></td>
<td></td>
<td><p>僅「恐怖幽靈郵件」單元中的同年作品</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年1月14日</p></td>
<td></td>
<td><p>僅「恐怖幽靈郵件」單元中的2016年作品</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/緯來電影台.md" title="wikilink">緯來電影台</a></p></td>
<td><p>2012年8月23日～2012年9月20日</p></td>
<td></td>
<td><p>重播緯來日本台在2012年播出的部分，剪輯成共8集</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/煲劇1台.md" title="wikilink">煲劇1台</a></p></td>
<td><center></td>
<td><p>2014年3月</p></td>
<td></td>
<td><p>「恐怖幽靈郵件」單元部分中的2009至2012年部分，剪輯成共7集</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - [連續劇](../Page/#連續劇.md "wikilink")：
    [特別篇1](../Page/#特別篇1（1999年8月27日）.md "wikilink")
  - [連續劇](../Page/#連續劇.md "wikilink")：[特別篇2](../Page/#特別篇2（2000年8月25日）.md "wikilink")
  - [連續劇](../Page/#連續劇.md "wikilink")：[春之恐怖懸疑](../Page/#春之恐怖懸疑（2003年4月8日）.md "wikilink")
  - [連續劇](../Page/#連續劇.md "wikilink")：[特別篇3](../Page/#特別篇3（2003年9月5日）.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[01](../Page/#01.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[02](../Page/#02.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[03](../Page/#03.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[04](../Page/#04.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[05](../Page/#05.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[06](../Page/#06.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[07](../Page/#07.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[08](../Page/#08.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[09](../Page/#09.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[10](../Page/#10.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[11](../Page/#11.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[12](../Page/#12.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[13](../Page/#13.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[14](../Page/#14.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[15](../Page/#15.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[16](../Page/#16.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[17](../Page/#17.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[18](../Page/#18.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[19](../Page/#19.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[20](../Page/#20.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[21](../Page/#21.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[22](../Page/#22.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[23](../Page/#23.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[24](../Page/#24.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[25](../Page/#25.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[26](../Page/#26.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[27](../Page/#27.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[28](../Page/#28.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[29](../Page/#29.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[30](../Page/#30.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[31](../Page/#31.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[32](../Page/#32.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[33](../Page/#33.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[34](../Page/#34.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[35](../Page/#35.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[36](../Page/#36.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[37](../Page/#37.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[38](../Page/#38.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[39](../Page/#39.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[40](../Page/#40.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[41](../Page/#41.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[42](../Page/#42.md "wikilink")
  - [恐怖幽靈郵件](../Page/#恐怖幽靈郵件.md "wikilink")：[43](../Page/#43.md "wikilink")
  - [電影](../Page/#電影.md "wikilink")

## 連續劇

### 特別篇

  - 特別篇1（1999年8月27日）

<!-- end list -->

  - 序章（演出：[池脇千鶴](../Page/池脇千鶴.md "wikilink")、）
  - 深夜病房（演出：[黑木瞳](../Page/黑木瞳.md "wikilink")、[竹中直人](../Page/竹中直人.md "wikilink")（特別演出））
  - 公司內部怪事報（演出：[杉本哲太](../Page/杉本哲太.md "wikilink")、、[鷲尾真知子](../Page/鷲尾真知子.md "wikilink")）
  - 白天的鈴鐺（演出：稻垣吾郎、[佐藤仁美](../Page/佐藤仁美.md "wikilink")、）
  - 夏天的訪問者（演出：[中山美穗](../Page/中山美穗.md "wikilink")、[窪塚洋介](../Page/窪塚洋介.md "wikilink")、）

<!-- end list -->

  - 特別篇2（2000年8月25日）

<!-- end list -->

  - 片頭（演出：）
  - 半夜的病房（演出：[淺野溫子](../Page/淺野溫子.md "wikilink")、、）
  - 另一個我（演出：窪塚洋介、[石田百合子](../Page/石田百合子.md "wikilink")、）
  - 十二天的惡夢（演出：[仲村亨](../Page/仲村亨.md "wikilink")、、、）
  - 遙遠的夏天（演出：[酒井法子](../Page/酒井法子.md "wikilink")、[片瀨那奈](../Page/片瀨那奈.md "wikilink")、[岡田奈奈
    (1959年)](../Page/岡田奈奈_\(1959年\).md "wikilink")、、[木村多江](../Page/木村多江.md "wikilink")、[芳賀優里亞](../Page/芳賀優里亞.md "wikilink")）

<!-- end list -->

  - 春之恐怖懸疑（2003年4月8日）

<!-- end list -->

  - 片頭〜虎姑婆（演出：[內山理名](../Page/內山理名.md "wikilink")、）
  - 來自黑暗的電話（演出：、[綾瀨遙](../Page/綾瀨遙.md "wikilink")、（）、）
  - 父親的回憶（演出：[井川遙](../Page/井川遙.md "wikilink")、、、[志田未來](../Page/志田未來.md "wikilink")、、[地井武男](../Page/地井武男.md "wikilink")）

<!-- end list -->

  - 特別篇3（2003年9月5日）

<!-- end list -->

  - 病房奇譚（演出：[仲間由紀惠](../Page/仲間由紀惠.md "wikilink")、）
  - 被附身的房子（演出：、[三浦理惠子](../Page/三浦理惠子.md "wikilink")、）
  - 死者的求婚（演出：[優香](../Page/優香.md "wikilink")、、）
  - 半夜的徘徊者（演出：[阿部寛](../Page/阿部寛.md "wikilink")、、[塚地武雅](../Page/塚地武雅.md "wikilink")（醉龍）、[田中哲司](../Page/田中哲司.md "wikilink")）
  - 稱心如意（演出：[財前直見](../Page/財前直見.md "wikilink")、[高橋克實](../Page/高橋克實.md "wikilink")）

### 恐怖幽靈郵件

該節目從2004年1月10日起，改變型態為電視節目化，並由稻垣吾郎和一些小學生班底主持，並分為兩種單元：由老師們解析靈異照片、靈異景點、及將觀眾投稿信件的體驗拍成模擬影片等方式播出。依官網內容規劃表示，該節目分為兩季，2004年1月10日播出\~2004年3月20日的第11集為第一季，2004年10月11日開始，到2005年3月7日為第二季。2005年1月17日播出開始，「真怖俱樂部」訪問藝人自身的體驗為基礎的「恐怖幽靈郵件」來讀取，並採用訴說的形式，目前該節目日本尚未製播第三季，僅製作特別篇推出。

#### 2004年

第1季，共11集。

  - 01

<!-- end list -->

  - 2004年1月10日播出
      - 破屋的少女（主演：、共演：、[雄貴](../Page/雄貴_\(英吉\).md "wikilink")（）、）
      - 只為了求死（主演：田中哲司、共演：）
      - 深夜的鏡像（主演：[神木隆之介](../Page/神木隆之介.md "wikilink")、共演：[土肥優真](../Page/土肥優真.md "wikilink")、、[下村惠理](../Page/下村惠理.md "wikilink")、）
      - 房間的棲息者（主演：[高岡蒼佑](../Page/高岡蒼佑.md "wikilink")。現：[高岡蒼甫](../Page/高岡蒼甫.md "wikilink")、共演、[齊藤直行](../Page/齊藤直行.md "wikilink")）
      - 黃昏迷路的孩子（主演：[柊瑠美](../Page/柊瑠美.md "wikilink")、共演：久保晶、[熊谷智博](../Page/熊谷智博.md "wikilink")、）

<!-- end list -->

  - 02

<!-- end list -->

  - 2004年1月17日播出
      - 尋找東西（主演：[成宮寬貴](../Page/成宮寬貴.md "wikilink")、共演：[平田龍也](../Page/平田龍也.md "wikilink")）
      - 看不見的居民（主演：、共演：、[大江聰](../Page/大江聰.md "wikilink")、）
      - 臨終的聲音（主演：、共演：[田中千繪](../Page/田中千繪.md "wikilink")）
      - 紅衣少女（主演：、共演：[石川真吾](../Page/石川真吾.md "wikilink")、）

<!-- end list -->

  - 03

<!-- end list -->

  - 2004年1月24日播出
      - 平交道的怪事（主演：、共演：）
      - 未見過的景象（主演：[相武紗季](../Page/相武紗季.md "wikilink")、共演：[土屋美穗子](../Page/土屋美穗子.md "wikilink")）
      - 招書的幽靈（主演：[堀北真希](../Page/堀北真希.md "wikilink")、共演：、[古川真紀](../Page/古川真紀.md "wikilink")）
      - 半夜的奇蹟（主演：、共演：、[大塚千弘](../Page/大塚千弘.md "wikilink")、[內山真人](../Page/內山真人.md "wikilink")、、、[井上浩](../Page/井上浩.md "wikilink")、[茄子忍](../Page/茄子忍.md "wikilink")、[塚本安屋子](../Page/塚本安屋子.md "wikilink")）

<!-- end list -->

  - 04

<!-- end list -->

  - 2004年1月31日播出
      - 病房的夜晚（主演：石橋桂、共演：、[田村真依奈](../Page/田村真依奈.md "wikilink")、）
      - 姊姊（主演：、共演：[田中圭](../Page/田中圭.md "wikilink")）
      - 伸長的手臂（主演：[真木洋子](../Page/真木洋子.md "wikilink")、共演：[中島由紀](../Page/中島由紀.md "wikilink")）
      - 最後買的東西（主演：[森山未來](../Page/森山未來.md "wikilink")、共演：、、、、、[川瀨忠行](../Page/川瀨忠行.md "wikilink")、、、）

<!-- end list -->

  - 05

<!-- end list -->

  - 2004年2月7日播出
      - 折到的護身符（主演：[蒼井優](../Page/蒼井優.md "wikilink")）
      - 後面的女人（主演：[長澤雅美](../Page/長澤雅美.md "wikilink")、、）
      - 迷路的孩子（主演：[佐佐木藏之介](../Page/佐佐木藏之介.md "wikilink")、共演：、[今井悠貴](../Page/今井悠貴.md "wikilink")）

<!-- end list -->

  - 06

<!-- end list -->

  - 2004年2月14日播出
      - 行人穿越道奇譚（主演：[綾瀨遙](../Page/綾瀨遙.md "wikilink")、共演：、[海田葉月](../Page/海田葉月.md "wikilink")）
      - 半夜的白手指（主演：[宮地真緒](../Page/宮地真緒.md "wikilink")、共演：、）
      - 邪惡影子的真面目是（主演：、共演：、[北村修](../Page/北村修.md "wikilink")）
      - 來自深淵（主演：、共演：芳賀優里亞、、[上原袈裟俊](../Page/上原袈裟俊.md "wikilink")、[橋本佳代](../Page/橋本佳代.md "wikilink")、[長野京子](../Page/長野京子.md "wikilink")、[山路和弘](../Page/山路和弘.md "wikilink")）

<!-- end list -->

  - 07

<!-- end list -->

  - 2004年2月21日播出
      - 夜話的窗戶（主演：[塚本雛子](../Page/成海璃子.md "wikilink")、共演：、[小關可奈](../Page/小關可奈.md "wikilink")）
      - 接近的腳步聲（主演：[香椎由宇](../Page/香椎由宇.md "wikilink")、共演：）
      - 二點四十五分的哭泣聲（主演：、共演：、[諸頭未優](../Page/諸頭未優.md "wikilink")）
      - 二樓的恐怖（主演：碇由貴子、共演：[喜多道枝](../Page/喜多道枝.md "wikilink")）

<!-- end list -->

  - 08

<!-- end list -->

  - 2004年2月28日播出
      - 沒有身影的工作人員（主演：[前田亞季](../Page/前田亞季.md "wikilink")、共演：）
      - 領取被附身的住院患者（主演：[勝地涼](../Page/勝地涼.md "wikilink")、共演：、[大崎聖奈](../Page/大崎聖奈.md "wikilink")、[菅原園](../Page/菅原園.md "wikilink")）
      - 穆克的願望（主演：[永井杏](../Page/永井杏.md "wikilink")、共演：、、[鶴田明巳](../Page/鶴田明巳.md "wikilink")、、[金子裕太](../Page/金子裕太.md "wikilink")、[古谷琉翔](../Page/古谷琉翔.md "wikilink")、、）

<!-- end list -->

  - 09

<!-- end list -->

  - 2004年3月6日播出
      - 迎面錯過的紊亂者（主演：[上野樹里](../Page/上野樹里.md "wikilink")、共演：、[清水響](../Page/清水響.md "wikilink")、[立花訓子](../Page/立花訓子.md "wikilink")）
      - 在幻燈之下（主演：、共演：、、[黒川和明](../Page/黒川和明.md "wikilink")）
      - 半夜的稀客（主演：[末永遙](../Page/末永遙.md "wikilink")、共演：、[山田誠吾](../Page/山田誠吾.md "wikilink")）
      - 供養的禮儀（主演：、共演：[水野遙](../Page/水野遙.md "wikilink")）

<!-- end list -->

  - 10

<!-- end list -->

  - 2004年3月13日播出
      - 墓地的女人（主演：[竹田侑美](../Page/竹田侑美.md "wikilink")、共演：[松田惠果](../Page/松田惠果.md "wikilink")、、[鍋田薰](../Page/鍋田薰.md "wikilink")）
      - 京子（主演：三浦理惠子、共演：）
      - 要求多的幽靈（主演：、共演：）
      - 夜晚的重逢（主演：山本道子、共演：、）

<!-- end list -->

  - 11

<!-- end list -->

  - 2004年3月20日播出
      - 幽靈公寓（主演：、共演：、[明日香真由美](../Page/明日香真由美.md "wikilink")）
      - 誰低語呢喃著（主演：、共演：、、[志水惠實子](../Page/志水惠實子.md "wikilink")）
      - 訪問的人們（主演：、共演：[武井証](../Page/武井証.md "wikilink")、、、[江澤規子](../Page/江澤規子.md "wikilink")、）

<!-- end list -->

  - 12

<!-- end list -->

  - 特別篇（2004年4月3日播出）
      - 黃泉的森林（主演：[小栗旬](../Page/小栗旬.md "wikilink")、共演：[加藤夏希](../Page/加藤夏希.md "wikilink")、[永田真理](../Page/永田真理.md "wikilink")）
      - 影子的諧調（主演：、共演：、、、、）

<!-- end list -->

  - 13

<!-- end list -->

  - 特別篇（2004年9月7日播出）
      - 隧道中的少年（主演：、共演：、）
      - 預言家的餘波（主演：、共演：（）、）
      - 怪聲宿舍（主演：、共演：[伊東美知枝](../Page/伊東美知枝.md "wikilink")、[笹井惠子](../Page/笹井惠子.md "wikilink")、[永田真理](../Page/永田真理.md "wikilink")、[前川美枝](../Page/前川美枝.md "wikilink")）
      - 孤獨一人的少女（主演：、共演：[谷村美月](../Page/谷村美月.md "wikilink")、、[下田由香](../Page/下田由香.md "wikilink")、[須山史織](../Page/須山史織.md "wikilink")）
      - 那天的約定（主演：[美山加戀](../Page/美山加戀.md "wikilink")、共演：、[曾川留三子](../Page/曾川留三子.md "wikilink")、[望月瑛蘭](../Page/望月瑛蘭.md "wikilink")、）

第二季，共16集。

  - 14

<!-- end list -->

  - 2004年10月11日播出
      - 死者的演奏（主演：、[上江田美穗](../Page/上江田美穗.md "wikilink")）
      - 寶塚山（主演：和希沙也、共演：）
      - 停車場的夜晚（主演：[岡田義德](../Page/岡田義德.md "wikilink")、共演：、[杉下繪理](../Page/杉下繪理.md "wikilink")）
      - 臨終的離別（主演：、共演：、[永井文敏](../Page/永井文敏.md "wikilink")、[伊藤正美](../Page/伊藤正美.md "wikilink")）

<!-- end list -->

  - 15

<!-- end list -->

  - 2004年10月18日播出
      - 訪問者（主演：[本假屋唯香](../Page/本假屋唯香.md "wikilink")、共演：、[綠川美津惠](../Page/綠川美津惠.md "wikilink")）
      - 幽靈物件（主演：[塚本高史](../Page/塚本高史.md "wikilink")、共演：[村井美樹](../Page/村井美樹.md "wikilink")、、、）
      - 波濤聲的夢（主演：、共演：[大西麻惠](../Page/大西麻惠.md "wikilink")、、、）

<!-- end list -->

  - 16

<!-- end list -->

  - 2004年10月25日播出
      - 放學後的恐怖（主演：、共演：[田代琴惠](../Page/田代琴惠.md "wikilink")、[川口愛實](../Page/川口愛實.md "wikilink")、[佐藤露露](../Page/佐藤露露.md "wikilink")）
      - 黑暗的棲息者（主演：[伊藤淳史](../Page/伊藤淳史.md "wikilink")、共演：、、[中村泉貴](../Page/中村泉貴.md "wikilink")、、[齊藤王路](../Page/齊藤王路.md "wikilink")、[高橋侑吾](../Page/高橋侑吾.md "wikilink")、[後藤健](../Page/後藤健.md "wikilink")）
      - 被附身的騎士（主演：、共演：、、[森惠子](../Page/森惠子.md "wikilink")、）

<!-- end list -->

  - 17

<!-- end list -->

  - 2004年11月1日播出
      - 第四個的單人房（主演：、共演：、[廣川由里香](../Page/假面騎士GIRLS.md "wikilink")、[菊地美香](../Page/菊地美香.md "wikilink")）
      - 迷路的亡靈（主演：窪塚俊介、共演：）
      - 踏上旅程的腳步聲（主演：[福田麻由子](../Page/福田麻由子.md "wikilink")、共演：[森崎芳江](../Page/森崎芳江.md "wikilink")、）

<!-- end list -->

  - 18

<!-- end list -->

  - 2004年11月15日播出
      - 還有誰看見了（主演：[田中律子](../Page/田中律子.md "wikilink")、共演：[井上花菜](../Page/井上花菜.md "wikilink")、、[小島亞矢](../Page/小島亞矢.md "wikilink")）

<!-- end list -->

  - 19

<!-- end list -->

  - 2004年11月22日播出
      - 教室的幽靈（主演：、共演：）
      - 靈魂的分歧點（主演：[水川麻美](../Page/水川麻美.md "wikilink")、共演：[石川素子](../Page/石川素子.md "wikilink")、[川島伸也](../Page/川島伸也.md "wikilink")）
      - 窗戶上的少女（主演：谷村美月、共演：、、）

<!-- end list -->

  - 20

<!-- end list -->

  - 2004年11月29日播出
      - 鏡中的少女（主演：小出早織、共演：[向野澪](../Page/向野澪.md "wikilink")、[室伏由紀江](../Page/室伏由紀江.md "wikilink")）
      - 拒絕的報酬（主演：、共演：[佐藤二朗](../Page/佐藤二朗.md "wikilink")）
      - 在別墅等著我的東西（主演：、共演：山下容莉枝、、[田邊伸之助](../Page/田邊伸之助.md "wikilink")）
      - 被附身的醫院（主演：、共演：[松本純](../Page/松本純.md "wikilink")、[安田洋子](../Page/安田洋子.md "wikilink")、、[石井聰子](../Page/石井聰子.md "wikilink")、[大久保蒼](../Page/大久保蒼.md "wikilink")）
      - 溫暖的葬禮（主演：、共演：、、、、）

<!-- end list -->

  - 21

<!-- end list -->

  - 2004年12月6日播出
      - 臨終的信息（主演：[黑木美沙](../Page/黑木美沙.md "wikilink")、共演：）
      - 看不見的牽絆（主演：、共演：、）
      - 前略，爸爸（主演：、共演：、）

<!-- end list -->

  - 22

<!-- end list -->

  - 2004年12月13日播出
      - 死者去的地方（主演：[真木陽子](../Page/真木陽子.md "wikilink")、共演：、[大草理乙子](../Page/大草理乙子.md "wikilink")）
      - 半夜的警笛（主演：、共演：）
      - 老奶奶的留言（主演：[小野真弓](../Page/小野真弓.md "wikilink")、共演：森康子、[高坂真琴](../Page/高坂真琴.md "wikilink")、）

#### 2005年

  - 23

<!-- end list -->

  - 2005年1月17日播出
      - 跌倒的廁所（主演：[安田美沙子](../Page/安田美沙子.md "wikilink")、共演：、）
      - 共演者（主演：）※本人的體驗
      - 被遺忘的紀念照片（主演：[若槻千夏](../Page/若槻千夏.md "wikilink")、共演：、）

<!-- end list -->

  - 24

<!-- end list -->

  - 2005年1月24日播出
      - 有什麼在那裡（主演：[志田未來](../Page/志田未來.md "wikilink")、共演：[吉田大輝](../Page/吉田大輝.md "wikilink")、[高橋征也](../Page/高橋征也.md "wikilink")、[登野城佑真](../Page/登野城佑真.md "wikilink")、、[矢端名結](../Page/矢端名結.md "wikilink")、、[石井昭子](../Page/石井昭子.md "wikilink")、）
      - 半夜的邀請函（主演：、共演：、、[小野敦子](../Page/小野敦子.md "wikilink")）
      - 奇奇怪怪譚（主演：（搞笑團體「」）、共演：[海田葉月](../Page/海田葉月.md "wikilink")、[森田宗宏](../Page/森田宗宏.md "wikilink")）※本人的體驗
      - 白天的影子（主演：、共演：[吉井怜](../Page/吉井怜.md "wikilink")、）

<!-- end list -->

  - 25

<!-- end list -->

  - 2005年1月31日播出
      - 並不是鬼（主演：、共演：、[奧村勳](../Page/奧村勳.md "wikilink")、、[坂口理香](../Page/坂口理香.md "wikilink")）※本人的體驗
      - 同居人（主演：[青田典子](../Page/青田典子.md "wikilink")、共演：、、[德永淳](../Page/德永淳.md "wikilink")）※本人的體驗
      - 來回的腳步聲（主演：、共演：）

<!-- end list -->

  - 26

<!-- end list -->

  - 2005年2月7日播出
      - 防空洞的姊姊（主演：、共演：、、）
      - 夜裡的叔叔（主演：、共演：、、）※本人的體驗
      - 再見了！俊彥（主演：佐藤仁美、共演：、）

<!-- end list -->

  - 27

<!-- end list -->

  - 2005年2月21日播出
      - 影子（主演：[川上淳子](../Page/川上淳子.md "wikilink")、[芳賀優里亞](../Page/芳賀優里亞.md "wikilink")、共演：）
      - 預見的少女（主演：美山加戀、共演：上野夏妃、、、、[布施幾子](../Page/布施幾子.md "wikilink")、、、[土師孝也](../Page/土師孝也.md "wikilink")）
      - 那天的言靈（主演：、共演：、、[庄司正樹](../Page/庄司正樹.md "wikilink")、[川嵜美佳](../Page/川嵜美佳.md "wikilink")、）※本人的體驗

<!-- end list -->

  - 28

<!-- end list -->

  - 2005年2月28日播出
      - 伸長的頭髮（主演：[安藤櫻](../Page/安藤櫻.md "wikilink")、共演：[檀臣幸](../Page/檀臣幸.md "wikilink")、）
      - 招靈的波長（主演：（[鴕鳥俱樂部](../Page/鴕鳥俱樂部.md "wikilink")）、共演：、、[佐藤露露](../Page/佐藤露露.md "wikilink")）※本人的體驗
      - 通知死亡的呼叫（主演：、共演：、[柳下季里](../Page/柳下季里.md "wikilink")、）

<!-- end list -->

  - 29

<!-- end list -->

  - 2005年3月7日播出
      - 心靈地點（主演：[小池榮子](../Page/小池榮子.md "wikilink")、共演：、[齊藤直行](../Page/齊藤直行.md "wikilink")、[大江聰](../Page/大江聰.md "wikilink")、[永田真理](../Page/永田真理.md "wikilink")）
      - 亡靈棲息的土地（主演：、共演：）※本人的體驗
      - 不分季節的禮物（主演：[黑川智花](../Page/黑川智花.md "wikilink")、共演：、花原照子）

<!-- end list -->

  - 30

<!-- end list -->

  - 特別篇 京都神秘之旅SP（2005年8月23日播出）
      - 一起···搭乘吧！（主演：井上和香）
      - 心靈照片（主演：[山本耕史](../Page/山本耕史.md "wikilink")）
      - 黑髮的女人（主演：[松浦亞彌](../Page/松浦亞彌.md "wikilink")、共演：、[佐藤露露](../Page/佐藤露露.md "wikilink")）
      - 格外便宜物件（主演：[青木沙耶加](../Page/青木沙耶加.md "wikilink")、共演：、（）、、）※本人的體驗
      - 死神（主演：、共演：、田山涼成）

#### 2006年

  - 31

<!-- end list -->

  - 特別篇 鎌倉神秘之旅SP（2006年8月22日播出、收視率15.3%）
      - 在懸崖之下（主演：[伊藤淳史](../Page/伊藤淳史.md "wikilink")、共演：[夏川純](../Page/夏川純.md "wikilink")；<span style="color: blue;">**2012年8月30日**</span>；<span style="color: grey;">**9月16日**</span>）
      - 6號房間（主演：[堀北真希](../Page/堀北真希.md "wikilink")、共演：[桐谷美玲](../Page/桐谷美玲.md "wikilink")、、[鈴木浩介](../Page/鈴木浩介_\(演員\).md "wikilink")、；<span style="color: blue;">**8月30日**</span>；<span style="color: grey;">**9月16日**</span>）
      - 就在那裡！（主演：[高嶋政伸](../Page/高嶋政伸.md "wikilink")、共演：[紗榮子](../Page/達比修紗榮子.md "wikilink")、[溫水洋一](../Page/溫水洋一.md "wikilink")、[稲葉惠子](../Page/稲葉惠子.md "wikilink")；<span style="color: blue;">**8月30日**</span>）
      - 病房的絨毛玩具（主演：[釋由美子](../Page/釋由美子.md "wikilink")、共演：[岩佐真悠子](../Page/岩佐真悠子.md "wikilink")、山下容莉枝、[大島蓉子](../Page/大島蓉子.md "wikilink")、森康子、佐藤仁美、；<span style="color: blue;">**8月27日**</span>；<span style="color: green;">**9月6日**</span>；<span style="color: grey;">**9月23日**</span>）
      - 四塊塌塌米的貴婦人（主演：、共演：、；<span style="color: blue;">**8月30日**</span>；<span style="color: grey;">**9月16日**</span>）※本人的體驗
      - 不可思議的時間（主演：[平岡祐太](../Page/平岡祐太.md "wikilink")、共演：；<span style="color: blue;">**8月30日**</span>；<span style="color: grey;">**9月16日**</span>）

#### 2007年

  - 32

<!-- end list -->

  - 夏天的特別編2007（2007年8月28日播出、收視率16.5%；<span style="color: blue;">**2012年8月28日**</span>；<span style="color: green;">**9月13日**</span>；<span style="color: grey;">**9月9日**</span>）
      - 在叫我嗎···?（主演：[森迫永依](../Page/森迫永依.md "wikilink")、共演：、魏涼子、[松川真之介](../Page/松川真之介.md "wikilink")、[稲葉惠子](../Page/稲葉惠子.md "wikilink")）
      - 幽惑兜風之旅（主演：[增田貴久](../Page/增田貴久.md "wikilink")（[NEWS](../Page/NEWS.md "wikilink")）、共演：、、[嶋崎亞美](../Page/嶋崎亞美.md "wikilink")）
      - 靈魂通過的家（主演：[中川翔子](../Page/中川翔子.md "wikilink")、共演：、[安田洋子](../Page/安田洋子.md "wikilink")、[山口愛](../Page/山口愛.md "wikilink")、[森田宗宏](../Page/森田宗宏.md "wikilink")、）※本人的體驗
      - 半夜的病房（主演：[榮倉奈奈](../Page/榮倉奈奈.md "wikilink")、共演：[佐藤寬子](../Page/佐藤寬子.md "wikilink")、、[小池榮](../Page/小池榮.md "wikilink")）

#### 2008年

  - 33

<!-- end list -->

  - 夏之特別篇2008 心靈與身體的神秘之旅SP（2008年8月26日播出、收視率16.3%）
      - 破屋的螺旋（主演：[高田翔](../Page/高田翔.md "wikilink")（[傑尼斯Jr.](../Page/傑尼斯Jr..md "wikilink")）、共演：、[森富士雄](../Page/森富士雄.md "wikilink")；<span style="color: blue;">**2012年8月29日**</span>；<span style="color: grey;">**9月23日**</span>）
      - 病房的居民（主演：[加藤羅莎](../Page/加藤羅莎.md "wikilink")、共演：[岩佐真悠子](../Page/岩佐真悠子.md "wikilink")、、、[松岡璃奈子](../Page/松岡璃奈子.md "wikilink")、、[野副隼](../Page/野副隼.md "wikilink")；<span style="color: blue;">**8月29日**</span>；<span style="color: grey;">**9月23日**</span>）
      - 路上的回憶（主演：[Becky](../Page/Becky_\(日本藝人\).md "wikilink")、共演：[忍成修吾](../Page/忍成修吾.md "wikilink")、、；<span style="color: blue;">**8月29日**</span>；<span style="color: grey;">**9月23日**</span>）
      - K町的高級公寓（主演：、共演：、、[竹山隆範](../Page/竹山隆範.md "wikilink")；<span style="color: blue;">**8月29日**</span>；<span style="color: grey;">**9月23日**</span>）
      - 簡訊履歷（主演：[木下優樹菜](../Page/木下優樹菜.md "wikilink")、共演：[山田親太朗](../Page/山田親太朗.md "wikilink")、山根和馬；<span style="color: blue;">**8月21日**</span>；<span style="color: green;">**8月23日**</span>；<span style="color: grey;">**9月2日**</span>）※本人的體驗

#### 2009年

  - 34

<!-- end list -->

  - 冬之特別篇2009
    藝能界緊急除靈特別篇（2009年2月3日播出、收視率12.0%；<span style="color: blue;">**2012年8月27日**</span>；<span style="color: green;">**9月6日**</span>；<span style="color: grey;">**9月23日**</span>）
      - 染血的旅館（主演：[吉高由里子](../Page/吉高由里子.md "wikilink")、共演：、）
      - 被附身的森林（主演：[大橋望美](../Page/大橋望美.md "wikilink")、共演：[小原雅人](../Page/小原雅人.md "wikilink")、）

<!-- end list -->

  - 35

<!-- end list -->

  - 10周年紀念 京都能量景點旅行SP（2009年8月25日播出、收視率15.6%）
      - 稻草人（主演：[加藤清史郎](../Page/加藤清史郎.md "wikilink")、共演：；<span style="color: blue;">**2012年8月22日**</span>；<span style="color: green;">**8月30日**</span>；<span style="color: grey;">**9月9日**</span>）
      - 心靈動畫（主演：[志田未來](../Page/志田未來.md "wikilink")、共演：、；<span style="color: blue;">**8月27日**</span>；<span style="color: green;">**9月6日**</span>；<span style="color: grey;">**9月23日**</span>）
      - 顏之道（主演：[佐藤健](../Page/佐藤健.md "wikilink")、共演：；<span style="color: blue;">**8月22日**</span>；<span style="color: green;">**8月30日**</span>；<span style="color: grey;">**9月9日**</span>）
      - 怨恨的代價（主演：[綾瀨遙](../Page/綾瀨遙.md "wikilink")、共演：[市川由衣](../Page/市川由衣.md "wikilink")、、[入山法子](../Page/入山法子.md "wikilink")；<span style="color: blue;">**8月22日**</span>；<span style="color: green;">**8月30日**</span>；<span style="color: grey;">**9月9日**</span>）
      - 附身的男人（主演：[上地雄輔](../Page/上地雄輔.md "wikilink")、共演：、、、；<span style="color: blue;">**8月22日**</span>；<span style="color: green;">**8月30日**</span>；<span style="color: grey;">**9月9日**</span>）

#### 2010年

  - 36

<!-- end list -->

  - 夏之特別篇2010 AKB48
    淨靈全過程特別篇（2010年8月24日播出、收視率12.5%；<span style="color: blue;">**2012年8月21日**</span>；<span style="color: green;">**8月23日**</span>；<span style="color: grey;">**9月2日**</span>）
      - 紅耳環的怪物（主演：[大島優子](../Page/大島優子.md "wikilink")、共演：[山本光](../Page/山本光.md "wikilink")）
      - 嚎叫的廢棄醫院（主演：[溝端淳平](../Page/溝端淳平.md "wikilink")、共演：[中尾明慶](../Page/中尾明慶.md "wikilink")、）
      - 雨衣女（主演：[高橋南](../Page/高橋南.md "wikilink")、共演：[小嶋陽菜](../Page/小嶋陽菜.md "wikilink")、[佐藤由加理](../Page/佐藤由加理.md "wikilink")、[浦野一美](../Page/浦野一美.md "wikilink")、、）※本人的體驗
      - 不能開的房間（主演：[坂口憲二](../Page/坂口憲二.md "wikilink")、共演：、）
      - 死神來臨的夜晚（主演：優香、共演：[西山茉希](../Page/西山茉希.md "wikilink")、、[佐佐木澄江](../Page/佐佐木澄江.md "wikilink")）

#### 2011年

  - 37

<!-- end list -->

  - 夏之特別篇2011（2011年9月3日播出、收視率14.5%）
      - 奇怪的末班公車（主演：[中山優馬](../Page/中山優馬.md "wikilink")、共演：、[鈴野里奈](../Page/鈴野里奈.md "wikilink")；<span style="color: blue;">**2012年8月21日**</span>；<span style="color: green;">**8月23日**</span>；<span style="color: grey;">**9月2日**</span>）
      - 同學會的通知（主演：[武井咲](../Page/武井咲.md "wikilink")、共演：、、；<span style="color: blue;">**8月23日**</span>；<span style="color: green;">**8月30日**</span>；<span style="color: grey;">**9月16日**</span>）
      - 惡夢的十三日（主演：[向井理](../Page/向井理.md "wikilink")、共演：[木村文乃](../Page/木村文乃.md "wikilink")、、、、、、、[齊藤康弘](../Page/齊藤康弘.md "wikilink")、[內野雅央](../Page/內野雅央.md "wikilink")；<span style="color: blue;">**8月23日**</span>；<span style="color: green;">**8月30日**</span>；<span style="color: grey;">**9月16日**</span>）
      - 深淵的迷路孩子（主演：[蘆田愛菜](../Page/蘆田愛菜.md "wikilink")、共演：[南澤奈央](../Page/南澤奈央.md "wikilink")、、[永野芽郁](../Page/永野芽郁.md "wikilink")、、[宇賀神由香里](../Page/宇賀神由香里.md "wikilink")；<span style="color: blue;">**8月24日**</span>；<span style="color: green;">**9月6日**</span>；<span style="color: grey;">**9月2日**</span>）
      - 憤怒的紅寶石（主演：、共演：奧貫薫、、[田中美保](../Page/田中美保.md "wikilink")、；<span style="color: blue;">**8月24日**</span>；<span style="color: green;">**9月6日**</span>；<span style="color: grey;">**9月2日**</span>）

#### 2012年

  - 38

<!-- end list -->

  - 夏之特別篇2012（2012年8月18日播出、收視率16.0%；<span style="color: blue;">**2013年8月8日**</span>）
      - 紅爪（主演：[山下智久](../Page/山下智久.md "wikilink")、共演：[堀內敬子](../Page/堀內敬子.md "wikilink")、[本田翼](../Page/本田翼.md "wikilink")）
      - 被詛咒的病房（主演：[剛力彩芽](../Page/剛力彩芽.md "wikilink")、共演：[木南晴夏](../Page/木南晴夏.md "wikilink")、、）
      - 右肩的女人（主演：[岡田將生](../Page/岡田將生.md "wikilink")、共演：[蓮佛美沙子](../Page/蓮佛美沙子.md "wikilink")、[窪田正孝](../Page/窪田正孝.md "wikilink")、）
      - 半夜的最終列車（主演：[小杉](../Page/杉山英司.md "wikilink")）
      - 某年夏天的事情（主演：[香里奈](../Page/香里奈.md "wikilink")、共演：[菅田將暉](../Page/菅田將暉.md "wikilink")、[高橋光臣](../Page/高橋光臣.md "wikilink")、）

#### 2013年

  - 39

<!-- end list -->

  - 夏之特別篇2013（2013年8月17日播出、收視率13.0%；<span style="color: blue;">**2014年7月28日**</span>）
      - 女子高中大恐慌（主演：[坂口憲二](../Page/坂口憲二.md "wikilink")、共演：[新川優愛](../Page/新川優愛.md "wikilink")、[山田親太朗](../Page/山田親太朗.md "wikilink")、、）
      - 影子的暗示（主演：[深田恭子](../Page/深田恭子.md "wikilink")、共演：、）
      - 會動的娃娃（主演：[指原莉乃](../Page/指原莉乃.md "wikilink")、共演：、[小澤亮太](../Page/小澤亮太.md "wikilink")）
      - 恐怖的二樓（主演：[鈴木福](../Page/鈴木福.md "wikilink")、共演：、）
      - X醫院（主演：[藤谷太輔](../Page/藤谷太輔.md "wikilink")（[Kis-My-Ft2](../Page/Kis-My-Ft2.md "wikilink")）、共演：[柄本時生](../Page/柄本時生.md "wikilink")、[吉田羊](../Page/吉田羊.md "wikilink")）

#### 2014年

  - 40

<!-- end list -->

  - 15周年紀念SP（2014年8月16日播出、收視率14.6%；<span style="color: blue;">**2015年7月4日**</span>）
      - S銅礦山的女人（主演：[石原聰美](../Page/石原聰美.md "wikilink")、共演：[小池徹平](../Page/小池徹平.md "wikilink")）
      - 小悟（主演：[剛力彩芽](../Page/剛力彩芽.md "wikilink")）
      - 犯人是誰（主演：[草彅剛](../Page/草彅剛.md "wikilink")、共演：[北乃綺](../Page/北乃綺.md "wikilink")）
      - 誘惑之森（主演：[島崎遙香](../Page/島崎遙香.md "wikilink")、共演：[廣瀨愛麗絲](../Page/廣瀨愛麗絲.md "wikilink")）
      - 給我手臂（主演：[桐谷美玲](../Page/桐谷美玲.md "wikilink")、共演：[菜菜緒](../Page/菜菜緒.md "wikilink")）
      - 計程車司機的撞鬼經驗（主演：）
      - 通往黑暗的視覺（主演：[黑木瞳](../Page/黑木瞳.md "wikilink")、共演：[町田啓太](../Page/町田啓太.md "wikilink")）

#### 2015年

  - 41

<!-- end list -->

  - 夏之特別篇2015（2015年8月29日播出、收視率11.0%；<span style="color: blue;">**2015年10月10日**</span>）
      - 如影隨形（主演：[玉森裕太](../Page/玉森裕太.md "wikilink")（[Kis-My-Ft2](../Page/Kis-My-Ft2.md "wikilink")）、共演：[谷村美月](../Page/谷村美月.md "wikilink")、）
      - 附身之社（主演：[中條彩未](../Page/中條彩未.md "wikilink")、共演：[宇賀夏美](../Page/宇賀夏美.md "wikilink")、）
      - 奇奇怪怪女生宿舍（主演：[觀月亞里莎](../Page/觀月亞里莎.md "wikilink")、共演：[足立梨花](../Page/足立梨花.md "wikilink")、、、、[七瀨麻美](../Page/七瀨麻美.md "wikilink")、）
      - 暴風雨中的通報（主演：[齋藤工](../Page/齋藤工.md "wikilink")、共演：[武田梨奈](../Page/武田梨奈.md "wikilink")、[足立學](../Page/足立學.md "wikilink")、[鴨志田媛夢](../Page/鴨志田媛夢.md "wikilink")、、[中野麻衣](../Page/中野麻衣.md "wikilink")、[高嶋政伸](../Page/高嶋政伸.md "wikilink")）
      - 黑色日常（主演：[高梨臨](../Page/高梨臨.md "wikilink")、共演：[中村杏](../Page/中村杏.md "wikilink")、、[竹中直人](../Page/竹中直人.md "wikilink")）
      - 鄰接的家族（主演：[又吉直樹](../Page/又吉直樹.md "wikilink")（[Piece](../Page/Piece_\(搞笑組合\).md "wikilink")）、共演：、、[高島禮子](../Page/高島禮子.md "wikilink")）

#### 2016年

  - 42

<!-- end list -->

  - 夏之特別篇2016（2016年8月20日播出；<span style="color: blue;">**2017年1月14日**</span>；<span style="color: grey;">**8月26日**</span>）
      - 恐怖的壁櫥（主演：[中島健人](../Page/中島健人.md "wikilink")、共演：、）
      - 棲息在病房大樓裡的五圓硬幣（主演：[武井咲](../Page/武井咲.md "wikilink")、共演：、綾田俊樹、[板谷由夏](../Page/板谷由夏.md "wikilink")）
      - 詛咒的繪馬（主演：、共演：、[吉本實憂](../Page/吉本實憂.md "wikilink")、）
      - 多了一個人的電梯（主演：[生駒里奈](../Page/生駒里奈.md "wikilink")、[生田繪梨花](../Page/生田繪梨花.md "wikilink")、[齋藤飛鳥](../Page/齋藤飛鳥.md "wikilink")、[白石麻衣](../Page/白石麻衣.md "wikilink")、[西野七瀨](../Page/西野七瀨.md "wikilink")（[乃木坂46](../Page/乃木坂46.md "wikilink")））
      - 誘惑的沼澤（主演：[柳葉敏郎](../Page/柳葉敏郎.md "wikilink")、共演：[志尊淳](../Page/志尊淳.md "wikilink")、）
      - 夏日的告知（主演：[前田敦子](../Page/前田敦子.md "wikilink")、共演：、、、、）

#### 2017年

  - 43

<!-- end list -->

  - 夏之特別篇2017（2017年8月19日播出；<span style="color: blue;">**2017年9月2日**</span>）
      - 公寓（主演：[手越祐也](../Page/手越祐也.md "wikilink")）
      - 影女（主演：[武井咲](../Page/武井咲.md "wikilink")、共演：[石井杏奈](../Page/石井杏奈.md "wikilink")、）
      - 箱子（主演：[野村周平](../Page/野村周平.md "wikilink")、共演：[室井滋](../Page/室井滋.md "wikilink")）
      - 墳墓在哪裡（主演：[遠藤憲一](../Page/遠藤憲一.md "wikilink")、共演：[大友花恋](../Page/大友花恋.md "wikilink")）
      - 護士鈴（主演：[北川景子](../Page/北川景子.md "wikilink")、共演：[志尊淳](../Page/志尊淳.md "wikilink")、[川榮李奈](../Page/川榮李奈.md "wikilink")）

#### 2018年

  - 44

<!-- end list -->

  - 夏之特別篇2018（2018年8月18日播出)
      - 看不見的沉殿（主演：[神木隆之介](../Page/神木隆之介.md "wikilink")、共演：、）
      - 依附在迷道的女人（主演：[平祐奈](../Page/平祐奈.md "wikilink")）
      - 強行得到的容身之處（主演：[菜菜緒](../Page/菜菜緒.md "wikilink")、共演：[飯豐萬理江](../Page/飯豐萬理江.md "wikilink")、[小野武彥](../Page/小野武彥.md "wikilink")）
      - 浪西市區通靈路（主演：[ガンバレルーヤ](../Page/ガンバレルーヤ.md "wikilink")、共演：[桐山漣](../Page/桐山漣.md "wikilink")、[余貴美子](../Page/余貴美子.md "wikilink")）※本篇為主演來賓的本人體驗
      - 穿衣鏡（主演：[葵若菜](../Page/葵若菜.md "wikilink")、共演：[今田美桜](../Page/今田美桜.md "wikilink")、[櫻田通](../Page/櫻田通.md "wikilink")、[宮澤竹美](../Page/宮澤竹美.md "wikilink")、[桜井ユキ](../Page/桜井ユキ.md "wikilink")
        ）
      - 到達盡頭的念波（主演：[北村一輝](../Page/北村一輝.md "wikilink")、共演：[吉田羊](../Page/吉田羊.md "wikilink")）

## 電影

<table>
<thead>
<tr class="header">
<th><p><strong>上映年份</strong></p></th>
<th><p><strong>地區</strong></p></th>
<th><p><strong>片名</strong></p></th>
<th><p><strong>標題</strong></p></th>
<th><p><strong>演員</strong></p></th>
<th><p><strong>備註</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2010年10月16日</p></td>
<td></td>
<td><p>《真實恐怖撞鬼故事3D電影版》<br />
（劇場版ほんとうにあった怖い話3D）</p></td>
<td><p>廢棄工廠</p></td>
<td><p>主演：<br />
共演：<a href="../Page/川村武.md" title="wikilink">川村武</a></p></td>
<td><p>2D版</p></td>
</tr>
<tr class="even">
<td><p>靈異景點</p></td>
<td><p>主演：<a href="../Page/伊達愛.md" title="wikilink">伊達愛</a><br />
共演：<a href="../Page/世良優樹.md" title="wikilink">世良優樹</a></p></td>
<td><p>2D版</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>誰在那裡</p></td>
<td><p>主演：<a href="../Page/高木紗友希.md" title="wikilink">高木紗友希</a><br />
共演：</p></td>
<td><p>2D版</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>死亡土壤</p></td>
<td><p>主演：<a href="../Page/田邊真理.md" title="wikilink">田邊真理</a><br />
共演：<a href="../Page/岩谷美咲.md" title="wikilink">岩谷美咲</a>、</p></td>
<td><p>2D版</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>視線</p></td>
<td><p>主演：<a href="../Page/新垣里沙.md" title="wikilink">新垣里沙</a><br />
共演：<a href="../Page/中島早貴.md" title="wikilink">中島早貴</a>、<a href="../Page/志保.md" title="wikilink">志保</a></p></td>
<td><p>3D版</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016年10月29日</p></td>
<td></td>
<td><p>《真實恐怖撞鬼故事2016電影版》<br />
（劇場版ほんとうにあった怖い話2016）</p></td>
<td><p>紅色圍巾</p></td>
<td><p>主演：<a href="../Page/大場美奈.md" title="wikilink">大場美奈</a><br />
共演：熊谷江里子、小林龍樹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>溫泉</p></td>
<td><p>主演：佐藤輝、空美</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>民宿官網</p></td>
<td><p>主演：松本愛<br />
共演：松本洋平、宮城大樹</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>眼前所見的東西</p></td>
<td><p>主演：夏野香波<br />
共演：五十嵐正貴</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>全新的人</p></td>
<td><p>主演：小橋惠<br />
共演：石澤柊斗</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

這兩部電影並非出自[富士電視台的](../Page/富士電視台.md "wikilink")《毛骨悚然撞鬼經驗》系列電視節目，也不是該節目的正宗劇場版，只是日文原名非常類似（差了一個假名う）。這兩齣電影是百老匯製作公司（株式会社ブロードウェイ）出品的靈異短劇系列[OVA](../Page/OVA.md "wikilink")《[真實恐怖撞鬼故事](../Page/真實恐怖撞鬼故事.md "wikilink")》（ほんとうにあった怖い話）之劇場版，部分該短劇與電影曾在臺灣的[WakuWaku
Japan頻道播放](../Page/WakuWaku_Japan.md "wikilink")。

## 註釋

  - <span style="color: blue;">**藍色字**</span>為緯來日本台首播的日期
  - <span style="color: green;">**綠色字**</span>為緯來電影台首播的日期
  - <span style="color: grey;">**灰色字**</span>為緯來日本台重播的日期

## 外部連結

  - [富士電視台《-{真實的恐怖故事}-》官方網站](http://www.fujitv.co.jp/honkowa/index.html)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》官方網站](http://japan.videoland.com.tw/channel/honkowa/)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2006年版官方網站](http://japan.videoland.com.tw/channel/2012goest/ch08.htm)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2007年版官方網站](http://japan.videoland.com.tw/channel/2012goest/ch06.htm)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2008年版官方網站](http://japan.videoland.com.tw/channel/2012goest/ch07.htm)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2009年版官方網站](http://japan.videoland.com.tw/channel/2012goest/ch05.html)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2010年十週年版官方網站](http://japan.videoland.com.tw/channel/2012goest/ch02.html)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2010年版官方網站](http://japan.videoland.com.tw/channel/2012goest/ch01.html)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2011年版官方網站](http://japan.videoland.com.tw/channel/2012goest/ch03.html)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2012年版官方網站](http://japan.videoland.com.tw/channel/2013goest/)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2013年版官方網站](http://japan.videoland.com.tw/channel/20140702/)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2014年版官方網站](http://japan.videoland.com.tw/channel/20140702/)
  - [緯來日本台《-{毛骨悚然撞鬼經驗}-》2015年版官方網站](http://japan.videoland.com.tw/channel/Ghost2015/)
  - [緯來日本台《-{全新毛骨悚然撞鬼經驗}-》2016年版官方網站](http://japan.videoland.com.tw/channel/20170101/)
  - [緯來日本台《-{全新·毛骨悚然撞鬼經驗}-》2017年版官方網站](http://japan.videoland.com.tw/channel/20170803/)
  - [緯來電影台週四駭電影《-{毛骨悚然撞鬼經驗}-》官方網站](http://movie.videoland.com.tw/channel/201206/default-hib.asp)

## 節目的變遷

[Category:日本恐怖小說](../Category/日本恐怖小說.md "wikilink")
[Category:鬼題材小說](../Category/鬼題材小說.md "wikilink")
[Category:富士電視台電視劇](../Category/富士電視台電視劇.md "wikilink")
[Category:1999年日本電視劇集](../Category/1999年日本電視劇集.md "wikilink")
[Category:2000年代日本電視劇集](../Category/2000年代日本電視劇集.md "wikilink")
[Category:2000年代電視特別劇集](../Category/2000年代電視特別劇集.md "wikilink")
[Category:2010年代日本電視劇集](../Category/2010年代日本電視劇集.md "wikilink")
[Category:2010年代電視特別劇集](../Category/2010年代電視特別劇集.md "wikilink")
[Category:怪談題材作品](../Category/怪談題材作品.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:日本恐怖劇](../Category/日本恐怖劇.md "wikilink")
[Category:單元劇](../Category/單元劇.md "wikilink")
[Category:鬼題材電視劇](../Category/鬼題材電視劇.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:週六獎賞](../Category/週六獎賞.md "wikilink")
[Category:稻垣吾郎](../Category/稻垣吾郎.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")