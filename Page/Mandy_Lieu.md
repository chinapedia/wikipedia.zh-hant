**Mandy
Lieu**（），[香港](../Page/香港.md "wikilink")[混血](../Page/混血.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")，出生于[馬來西亞](../Page/馬來西亞.md "wikilink")[霹靂州](../Page/霹靂州.md "wikilink")[怡保](../Page/怡保.md "wikilink")。已婚澳門賭業大亨[周焯華](../Page/周焯華.md "wikilink")（洗米華）的情婦，育有兩女一子。

## 中文名稱

Mandy曾被指中文名稱為「劉碧麗」，但她於媒體表示，自己沒有正式中文名稱，「劉碧麗」只是網絡上有人為她起的中文名，後誤傳太廣才接受了此名字。\[1\]\[2\]由於在多國語言環境下成長，她能操流利的馬來語、粵語、普通話、英語及日語。

## 經歷

Mandy的生父是美國白人，她未出生已失蹤，母親是[馬來西亞華僑](../Page/馬來西亞華僑.md "wikilink")，在[馬來西亞單親家庭長大](../Page/馬來西亞.md "wikilink")。在[怡保出生](../Page/怡保.md "wikilink")，8歲隨家人移居[吉隆坡](../Page/吉隆坡.md "wikilink")。課餘時間兼職當模特兒。2003年，17歲輟學後到[香港發展](../Page/香港.md "wikilink")，拍過電視廣告，平面廣告等。

2008年7月，获香港青马狮子会的“危校重建计划”颁发委任状，成为亲善大使宣扬为内地儿童重建或修筑学校而筹款。2015年，公開跟已婚澳門娛樂大亨[周焯華](../Page/周焯華.md "wikilink")（洗米華）上演世紀牽手，兩人公開戀情，成為情婦第三者，為周焯華生下二女一兒。

## 感情生活

2008年與台灣藝人[陳柏霖交往](../Page/陳柏霖.md "wikilink")3年，經常形影不離。2011年分手。及後被傳與小超人交住。

2014年與已婚澳门娱乐大亨周焯华（洗米华）牵手出席饭局，恋情正式曝光。

2014年12月，經紀人證實 Mandy Lieu
未婚有孕4個多月，及後回[馬來西亞安胎](../Page/馬來西亞.md "wikilink")，但仍是非不絕。於2015年5月20日，在[英國](../Page/英國.md "wikilink")[倫敦為](../Page/倫敦.md "wikilink")[周焯華產下一女](../Page/周焯華.md "wikilink")，女兒取名周願。

2015年12月17日，公開跟周焯華上演世纪牵手，兩人公開戀情。

2016年6月，為[周焯華產下一名儿子](../Page/周焯華.md "wikilink")。

2017年9月，為[周焯華再產下一名女兒](../Page/周焯華.md "wikilink")。

## 影視節目

### 電視劇([無綫電視](../Page/無綫電視.md "wikilink"))

  - 2008年:《[盛裝舞步愛作戰](../Page/盛裝舞步愛作戰.md "wikilink")》 飾 Mandy

### 司儀

  - 2010年:[香港先生選舉](../Page/香港先生選舉.md "wikilink")([無綫電視](../Page/無綫電視.md "wikilink"))
  - 2013年:[亞洲電影大奬](../Page/亞洲電影大奬.md "wikilink")

### 綜藝節目([無綫電視](../Page/無綫電視.md "wikilink"))

  - 2013年:[超級無敵獎門人終極篇](../Page/超級無敵獎門人終極篇.md "wikilink")
  - 2014年:[快樂聯盟](../Page/快樂聯盟.md "wikilink") 第10集
  - 2014年:[瘋狂夏水禮](../Page/瘋狂夏水禮.md "wikilink") 第6-10集

華麗明星賽

### 節目主持([now寬頻電視](../Page/now寬頻電視.md "wikilink"))

  - 2009年:[Lifetival](../Page/Lifetival.md "wikilink")
  - 2010年:[老虎都要 party](../Page/老虎都要_party.md "wikilink")
  - 2010年:[日本‧不盡的享樂](../Page/日本‧不盡的享樂.md "wikilink")\[3\]

### 電影

  - 2012年：《[太极1从零开始](../Page/太极1从零开始.md "wikilink")》
  - 2012年：《[天生愛情狂](../Page/天生愛情狂_\(香港電影\).md "wikilink")》
  - 2014年: 《[愛．尋．迷](../Page/愛．尋．迷.md "wikilink")》
  - 2014年: 《[盜馬記](../Page/盜馬記.md "wikilink")》
  - 2014年: 《[痞子英雄2：黎明再起](../Page/痞子英雄2：黎明再起.md "wikilink")》
  - 2015年: 《[破風](../Page/破風_\(電影\).md "wikilink")》 飾 Emily

### 廣告

  - [西灣河](../Page/西灣河.md "wikilink")[嘉亨灣](../Page/嘉亨灣.md "wikilink")
  - [生力啤](../Page/生力啤.md "wikilink")
  - [玉蘭油](../Page/玉蘭油.md "wikilink")
  - [亞洲電視](../Page/亞洲電視.md "wikilink")[陽光熱浪嘉年華宣傳片](../Page/陽光熱浪嘉年華.md "wikilink")
  - [瑪花纖體](../Page/瑪花纖體.md "wikilink")
  - [隆力奇音樂電影](../Page/隆力奇.md "wikilink")《江南之恋》
  - [九倉電訊](../Page/九倉電訊.md "wikilink"):ICT (2014)
  - Swisscoat眼鏡
  - Spring Wedding春天婚紗攝影

### 其他演出

  - [古巨基](../Page/古巨基.md "wikilink") 《The Magic Moments 香港演唱會》表演嘉賓
  - 古巨基《The Magic Moments 多倫多演唱會》 表演嘉賓
    2008年5月30日[2008leokuconcert.com](https://web.archive.org/web/20080308225359/http://www.2008leokuconcert.com/)

## 參考連結

<references/>

## 外部連結

  -
  -
  - [mandylieu's
    blog](https://web.archive.org/web/20151127035641/http://blogs.elle.com.hk/mandylieu/)

  -
  -
  - [Mandy Lieu
    forum:)](https://web.archive.org/web/20100618225144/http://mandyforum.6k.cc/)

  - [Mandy
    Lieu-個人檔案](https://web.archive.org/web/20080117083242/http://www.elitehkmodel.com/model-info.php?gender=F&model_type=Asian&available=&lang=en&model_id=59)

  - \[<http://motor.atnext.com/moDspContent.cfm?article_id=(%22%2CG%3ER__>'\!B%3C%20%0A\&CAT_ID=171\&SelectedIndex=74
    及時愛Mandy Lieu\]

  - [Mandy Lieu
    教練派對鞋](http://ladies.sina.com.hk/cgi-bin/nw/show.cgi/105/4/1/177456/1.html)

  - [名模Mandy
    Lieu写真--美腿修长](http://ent.sina.com.cn/s/p/2007-09-05/05511703142.shtml)

  -
  -
  -
  -
  -
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:馬來西亞華人](../Category/馬來西亞華人.md "wikilink")
[Category:華裔混血兒](../Category/華裔混血兒.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:香港電視主持人](../Category/香港電視主持人.md "wikilink")
[Category:歐洲裔混血兒](../Category/歐洲裔混血兒.md "wikilink")
[B](../Category/劉姓.md "wikilink")
[Category:周焯華家族](../Category/周焯華家族.md "wikilink")

1.  2013年8月20日，無綫電視，《[今日VIP](../Page/今日VIP.md "wikilink")》
2.  [Mandy
    Lieu中文名叫劉碧麗？](http://hk.apple.nextmedia.com/enews/realtime/20130313/51298065)
3.  [now101台官方網站 - 2011全新電視體驗](http://now101.now-tv.com/nowHK/)