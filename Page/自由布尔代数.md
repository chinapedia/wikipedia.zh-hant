在[数学分支](../Page/数学.md "wikilink")[抽象代数中](../Page/抽象代数.md "wikilink")，**自由布尔代数**是[布尔代数](../Page/布尔代数.md "wikilink")
\<*B*,*F*\>，使得集合 *B*
(叫做“载体”)有其中元素叫做[生成元的](../Page/生成集合.md "wikilink")[子集](../Page/子集.md "wikilink")。生成元满足下列性质:

  - 不是生成元的每个 *B* 的元素都可被表达为[生成元的使用](../Page/生成集合.md "wikilink") *F*
    的元素的有限组合，*F* 是[运算的集合](../Page/运算.md "wikilink")；
  - 生成元尽可能的独立，因为对从生成元使用 *F*
    中运算形成的有限[项成立的任何等式](../Page/项.md "wikilink")，也要对于所有可能的布尔代数的所有元素成立。

## 例子

<imagemap> Image:Logical connectives Hasse
diagram.svg|thumb|right|300px|有两个产生元 A 和 B
的自由布尔代数的[哈斯图](../Page/哈斯图.md "wikilink")。采用
A 为"John 高" 和 B 为 "Mary 富"。原子是在 FALSE 紧上面的行中的四个元素。 rect 326 28 416 200
[X or ¬X](../Page/:en:Tautology_\(logic\).md "wikilink") rect 81 233 166
409 [¬A or ¬B](../Page/:en:Sheffer_stroke.md "wikilink") rect 260 231
349 409 [A or ¬B](../Page/:en:Converse_implication.md "wikilink") rect
393 230 481 409 [¬A or
B](../Page/:en:Material_implication.md "wikilink") rect 574 232 663 408
[A or B](../Page/:en:Logical_disjunction.md "wikilink") rect 13 436 103
617 [¬B](../Page/:en:Negation.md "wikilink") rect 147 438 235 617
[¬A](../Page/:en:Negation.md "wikilink") rect 279 440 368 616 [A xor
B](../Page/:en:Exclusive_or.md "wikilink") rect 375 440 464 617 [A xnor
B](../Page/:en:Logical_biconditional.md "wikilink") rect 507 439 595 617
[A](../Page/:en:Proposition.md "wikilink") rect 639 438 732 617
[B](../Page/:en:Proposition.md "wikilink") rect 79 647 168 826 [¬A and
¬B](../Page/:en:Logical_NOR.md "wikilink") rect 260 647 349 826 [A and
¬B](../Page/:en:Material_nonimplication.md "wikilink") rect 392 646 482
826 [¬A and B](../Page/:en:Converse_nonimplication.md "wikilink") rect
574 646 663 826 [A and B](../Page/:en:Logical_conjunction.md "wikilink")
rect 327 853 417 1035 [X and
¬X](../Page/:en:Contradiction.md "wikilink") desc none </imagemap>
自由布尔代数的生成元可以代表独立[命题](../Page/命题.md "wikilink")。例如，我们可以考虑两个命题
"John 高" 和 "Mary
富"。这生成了有四个[原子的自由布尔代数](../Page/原子_\(序理论\).md "wikilink")，它们就是

1.  John 高且 Mary 富
2.  John 高且 Mary 不富
3.  John 不高且 Mary 富
4.  John 不高且 Mary 不富

布尔代数的其他元素接着是这些原子的[逻辑析取](../Page/逻辑析取.md "wikilink")，比如 "John 高且 Mary
不富，或者 John 不高且 Mary 富"。除此之外还有一个元素
FALSE，它不是原子的析取(尽管它可以被认为是空析取；就是说没有原子的析取)。

这个例子产生了有 16 个元素的布尔代数；一般的说，对于有限的 *n*，有 *n* 个生成元的自由布尔代数有 2<sup>*n*</sup>
个原子，因此有 \(2^{2^n}\) 个元素。

对于[无限多个生成元](../Page/无限集合.md "wikilink")，情况是非常相似的，除了没有原子之外。布尔代数的所有元素都是有限多个生成命题的组合；两个这种元素被认为是相同的如果它们是逻辑等价的。

## 范畴论定义

更加正式的使用[范畴论的概念](../Page/范畴论.md "wikilink")，在生成元集合 *S* 上自由布尔代数是一个有序对
(π,*B*)，这里有

1.  π: *S* → *B* 是映射，
2.  *B* 是布尔代数，

并且关于这个性质是通用的。这意味着对于任何布尔代数 *B*<sub>1</sub> 和映射 π<sub>1</sub>: S →
*B*<sub>1</sub>，有一个唯一的[同态](../Page/同态.md "wikilink") *f*: *B* →
*B*<sub>1</sub> 使得

\[\pi_1 = f \circ \pi.\]

这个[泛性质也可以公式化为叫做](../Page/泛性质.md "wikilink")[逗号范畴的初始性质](../Page/逗号范畴.md "wikilink")。

“唯一”（在同构的意義下）是从这个泛性质立即得出的性质。注意映射 π 可以被证明是单射的。所以任何自由布尔代数 *B* 都这样的性质，有一个
*B* 的[子集](../Page/子集.md "wikilink") *S*，叫做 *B* 的**生成元**集合，使得从 *S* 到布尔代数
*B*<sub>1</sub> 的任何映射唯一的扩展为从 *B* 到 *B*<sub>1</sub> 的同态。

## 拓扑实现

有κ个[生成元的自由布尔代数](../Page/生成集合.md "wikilink")，这里的κ是有限或无限的[基数](../Page/基数.md "wikilink")，可以被实现为
{0,1}<sup>κ</sup>的[闭开的](../Page/闭开集.md "wikilink")[子集的搜集](../Page/子集.md "wikilink")，给定[乘积拓扑假定](../Page/乘积拓扑.md "wikilink")
{0,1} 有[离散拓扑](../Page/离散拓扑.md "wikilink")。对于每个α\<κ，第α个生成元是其第α个坐标是 1 的
{0,1}<sup>κ</sup>的所有元素的集合。特别是，有 \(\aleph_0\)
个生成元的自由布尔代数是[康托尔空间的所有闭开子集的搜集](../Page/康托尔空间.md "wikilink")。另人惊奇的，这个搜集是[可数的](../Page/可数集合.md "wikilink")。事实上，尽管有有限
*n* 个生成元的布尔代数，*n* 有[势](../Page/势.md "wikilink") \(2^{2^n}\)，带有
\(\aleph_0\) 个生成元的自由布尔代数有势 \(\aleph_0\)。

自由布尔代数的[拓扑方式详情请参见](../Page/拓扑学.md "wikilink")[Stone布尔代数表示定理](../Page/Stone布尔代数表示定理.md "wikilink")。

## 引用

  - [Paul Halmos](../Page/Paul_Halmos.md "wikilink") and Steven Givant
    (1998) *Logic as Algebra*. [Mathematical Association of
    America](../Page/Mathematical_Association_of_America.md "wikilink").
  - [Saunders Mac Lane](../Page/Saunders_Mac_Lane.md "wikilink") (1999)
    *Algebra*, 3d. ed. [American Mathematical
    Society](../Page/American_Mathematical_Society.md "wikilink"). ISBN
    0-821-81646-2.
  - Stoll, R. R., 1963. *Set Theory and Logic*, chpt. 6.7. Dover reprint
    1979.

## 参见

  - [自由对象](../Page/自由对象.md "wikilink")

[Z](../Category/布尔代数.md "wikilink")
[Category:自由代数结构](../Category/自由代数结构.md "wikilink")