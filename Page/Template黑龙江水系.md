<includeonly>{{\#ifeq:|nocat||}}</includeonly><noinclude>

## 使用方法

本模板会自动把引用它的页面加入分类。引用本模板时使用下列语法，可以避免加入分类：<small>（注意nocat必须为英文小写）</small>

{{|nocat}} </noinclude>

[Category:黑龍江水系](../Category/黑龍江水系.md "wikilink")
[Category:中国河流模板](../Category/中国河流模板.md "wikilink")
[Category:俄罗斯河流模板](../Category/俄罗斯河流模板.md "wikilink")