**嘉琳**（**Garlum**），原名**劉嘉琳**，[香港創作](../Page/香港.md "wikilink")[女歌手](../Page/女歌手.md "wikilink")。其兄為[人民力量主席](../Page/人民力量.md "wikilink")[劉嘉鴻](../Page/劉嘉鴻.md "wikilink")
。

嘉琳1999年取得[社會科學學士學位](../Page/社會科學.md "wikilink")，主修[犯罪學](../Page/犯罪學.md "wikilink")，2000年至2002年曾赴[東京](../Page/東京.md "wikilink")[早稻田大學修讀](../Page/早稻田大學.md "wikilink")[性別研究及](../Page/性別研究.md "wikilink")[人類學](../Page/人類學.md "wikilink")。2004年修畢[香港大學](../Page/香港大學.md "wikilink")[哲學](../Page/哲學.md "wikilink")[碩士課程](../Page/碩士.md "wikilink")，論文題目為
The social construction of rave culture in Hong Kong (參香港大學圖書館網頁)。

2002年參加[商台](../Page/商台.md "wikilink")[叱吒903Motclub](../Page/叱吒903.md "wikilink")903「音樂出頭天」比賽，憑《看風景》奪得「出頭天單曲」，被[何韻詩改編成](../Page/何韻詩.md "wikilink")《獨樂樂》。後簽約成為[EMI合約作曲人](../Page/EMI.md "wikilink")，亦參與歌手[和音工作及於公開場合表演](../Page/和音.md "wikilink")。2004年2月14日於香港[Hard
Rock Cafe舉行首個個人演唱會](../Page/Hard_Rock_Cafe.md "wikilink")。

2005年4月至10月，拍攝[香港電台電視部製作之](../Page/香港電台.md "wikilink")《[藝行四方](../Page/藝行四方.md "wikilink")》，到世界各地拍攝[文化藝術場地](../Page/文化.md "wikilink")。

2006年，嘉琳與[陳輝陽主持](../Page/陳輝陽.md "wikilink")[商業電台節目](../Page/商業電台.md "wikilink")《[最佳配樂](../Page/最佳配樂.md "wikilink")》，但她現已向[商業電台自動請辭](../Page/商業電台.md "wikilink")。

## 唱片

  - 2003年11月： 個人大碟《Reach for Your Moon》
  - 2004年12月： 與獨立音樂人合輯《Come out and Play》
  - 2005年1月： [阿麥書房音樂專輯Vol](../Page/阿麥書房.md "wikilink").1《看不見的城市漫遊
    Invisible Cities Journeys》

## 為其他歌手的創作

  - [鄧麗欣](../Page/鄧麗欣.md "wikilink") - 青山散步 Walkin' Aoyama
  - [薛凱琪](../Page/薛凱琪.md "wikilink") - 鳳凰木

## 獎項

**2003年：**

  - [2003年度新城勁爆頒獎禮得獎名單](../Page/2003年度新城勁爆頒獎禮得獎名單.md "wikilink") -
    新城勁爆新登場歌手

## 外部連結

  - [Garlum in pinkwork (sound &
    video)](http://www.pinkwork.com/gar/lum1.htm)
  - [Garlum in PinkWork創作 (sound &
    video)](http://www.pinkwork.com/issue/02/p1.htm)

[lum](../Category/劉姓.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:早稻田大學校友](../Category/早稻田大學校友.md "wikilink")