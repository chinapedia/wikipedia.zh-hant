[Werke_und_Tage.jpg](https://zh.wikipedia.org/wiki/File:Werke_und_Tage.jpg "fig:Werke_und_Tage.jpg")

《**工作與時日**》（Works and
Days）為[古希腊](../Page/古希腊.md "wikilink")[诗人](../Page/诗人.md "wikilink")[赫西俄德於大約公元前](../Page/赫西俄德.md "wikilink")7世紀所写的[田园牧诗](../Page/田园牧诗.md "wikilink")，系统地记录了当时农耕生产的知识，表现了平静而优美的农村生活场景，富于生活气息，包含有很多道德和农业生产方面的警句，是早期一部[百科全书式的著作](../Page/百科全书.md "wikilink")，广泛地为后世古典作家所引用。在其中心，《作品与时日》是一个农民的年鉴，赫西奥德在指导他的兄弟Perses的农艺。
学者们在希腊大陆的土地危机背景下看到了这项作品，这引发了一场[殖民地探险以寻找新土地的浪潮](../Page/古代殖民地#古希腊殖民地.md "wikilink")。
在这首诗中，[诗人](../Page/诗人.md "wikilink")[赫西俄德还向他的兄弟提供了他应该如何过他的生活的广泛的道德建议](../Page/赫西俄德.md "wikilink")。
《作品与时日》可能最出名的是它的两个神话[原因論](../Page/原因論.md "wikilink")：辛苦和痛苦，定义了人类的状况：[普罗米修斯和](../Page/普罗米修斯.md "wikilink")[潘多拉的故事](../Page/潘多拉.md "wikilink")，以及所谓的[五个时代的神话](../Page/人類世紀.md "wikilink")。

## 外部链接

  - [Hesiod, the Homeric Hymns and Homerica, *Works and
    Days*](https://web.archive.org/web/20170602071412/http://omacl.org/Hesiod/works.html)
    - Online Medieval and Classical Library Release \#8

  -
  - [Hesiod: *Works and Days*, Full Text in English, 31
    pages](http://www.ellopos.net/elpenor/greek-texts/ancient-greece/hesiod/works-days.asp)

  -
[分類:史詩](../Page/分類:史詩.md "wikilink")

[Category:希腊神话](../Category/希腊神话.md "wikilink")
[Category:鐵器時代的希臘](../Category/鐵器時代的希臘.md "wikilink")
[Category:希臘殖民](../Category/希臘殖民.md "wikilink")
[Category:前8世紀書籍](../Category/前8世紀書籍.md "wikilink")
[Category:前7世紀書籍](../Category/前7世紀書籍.md "wikilink")