[Wakaba3.jpg](https://zh.wikipedia.org/wiki/File:Wakaba3.jpg "fig:Wakaba3.jpg")
[Office_1719.jpg](https://zh.wikipedia.org/wiki/File:Office_1719.jpg "fig:Office_1719.jpg")
[Typicalbusyoffice20050109.jpg](https://zh.wikipedia.org/wiki/File:Typicalbusyoffice20050109.jpg "fig:Typicalbusyoffice20050109.jpg")
[HK_CWB_Moreton_Road_Bus_Station_Office_Citybus.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_Moreton_Road_Bus_Station_Office_Citybus.JPG "fig:HK_CWB_Moreton_Road_Bus_Station_Office_Citybus.JPG")寫字亭\]\]

**辦公室**（**office**），又稱**寫-{}-字樓**，是一種讓人們在其中[辦公](../Page/辦公.md "wikilink")（[工作](../Page/工作.md "wikilink")）的場所，通常是[房間的形態](../Page/房間.md "wikilink")，但隨著[電腦與](../Page/電腦.md "wikilink")[網路的發達](../Page/網路.md "wikilink")，也漸漸出現不需要實體空間的[虛擬辦公室](../Page/虛擬辦公室.md "wikilink")（Virtual
Office）。內部由辦公室所組成的則稱為**[辦-{}-公大樓](../Page/#辦公大樓.md "wikilink")**（Office
building）。\[1\]

## 歷史

[西方古代的辦公室通常是](../Page/西方世界.md "wikilink")[宮殿或大型](../Page/宮殿.md "wikilink")[廟宇中的一部份](../Page/廟.md "wikilink")。通常是一間存放有大量[捲軸的房間](../Page/捲軸.md "wikilink")，並有[抄寫員在其中進行工作](../Page/抄寫員.md "wikilink")。對一些[考古學家和大眾媒體而言](../Page/考古學家.md "wikilink")，這些房間有時也被稱為「[圖書館](../Page/图书馆.md "wikilink")」（libraries），因為這個場所通常與記載有文學作品的捲軸有關。事實上，除了詩歌或虛構的[文學作品之外](../Page/文學作品.md "wikilink")，自從各種記錄、合約、命令、公告等內容都被記載於捲軸上起，這些存放捲軸的房間就有了現代「辦公室」的定義。

## 空間安排

依照不同的功能，辦公室的空間安排有非常多種方式，這將會影響整個辦公室的氣氛和文化，同時對某些公司（特別是[時裝](../Page/時裝.md "wikilink")、[時尚或文化相關的企業](../Page/時尚.md "wikilink")）來說也非常重要。安排的考量包括：在同一個房間內工作的人數。因此兩種極端的空間安排可能是：每個工作者都有自己的小房間，或是一個大型的空間讓幾十人、甚至幾百人在一起工作。這種開放式的辦公空間，會有許多工作者在同一個空間工作，也會有一個特定的區域，能讓正在進行某種計畫（例如一個新的軟體計畫）的小組團隊使用。同時這也會造成隱私和安全的喪失，可能會使得公司的機密遭竊或遺失。而介於個人和開放辦公空間的形式，就是使用的辦公室，在美國[漫畫](../Page/漫畫.md "wikilink")《[呆伯特](../Page/呆伯特.md "wikilink")》（Dilbert）中經常可以見到這樣的辦公室類型。這樣的辦公室類型可以保有一定程度的視覺隱私，但卻無法防止聲音的傳遞和祕密的外洩（以聲音形式）

## 辦公大樓

### 一般現代辦公大樓內的基本設施

  - [自來水](../Page/自來水.md "wikilink")
  - [洗手間](../Page/洗手間.md "wikilink")
  - [電力](../Page/電力.md "wikilink")（包括多個[插座](../Page/插座.md "wikilink")）
  - 照明
  - 辦公桌
  - [私用自動交換分機](../Page/私用自動交換分機.md "wikilink")（PABX）
  - 連結至當地電信公司的[光纖連線](../Page/光纖.md "wikilink")
  - \-{zh-hant:[寬頻](../Page/寬頻.md "wikilink");zh-hans:[宽带](../Page/宽带.md "wikilink")}-[網路和](../Page/網路.md "wikilink")[電話等通訊的](../Page/電話.md "wikilink")[同軸電纜或](../Page/同軸電纜.md "wikilink")[非屏蔽雙絞線](../Page/非屏蔽雙絞線.md "wikilink")
  - [停車場](../Page/停車場.md "wikilink")
  - [升降機](../Page/升降機.md "wikilink")

## 辦公室裝修佈置

[屏風辦公桌同樣也是辦公空間常見的辦公傢俱](../Page/屏風辦公桌.md "wikilink")，簡單來說它是由屏風和辦公桌組合而成的一種組合型辦公桌。多數是提供給各大中小企業辦公室員工使用的，也是一款必不可少的辦公傢俱。而屏風辦公桌的風格亦是一個辦公環境構成的主心，起重要性說大不大說小不小，是一種形象的展示，一種公司文化的表現，對於外來客戶來說很重要。

辦公屏風桌的常用佈局

一、 蜂巢型佈局

蜂巢型屬於傳統辦公屏風桌擺放的規劃方式

二、小組型佈局

小組型，以小團隊為單位將辦公室傢俱空間劃分成若幹個單元

三、開放型佈局

開放條型是辦公屏風桌規劃運用最為廣泛的形態。

四、獨立型內局

獨立型屬於個人專享的獨立員工工作位

## 参考文献

## 外部链接

[辦公室裝修佈置技巧
辦公屏風桌的選擇，辦公屏風桌的佈局](http://www.eagle-holdings.com.hk/index.php?ac=article&at=read&did=892)

## 参见

  - [辦事處](../Page/辦事處.md "wikilink")
  - [工作室](../Page/工作室.md "wikilink")
  - [虛擬辦公室](../Page/虛擬辦公室.md "wikilink")
  - [甲級寫字樓](../Page/甲級寫字樓.md "wikilink")
  - [辦公室政治](../Page/辦公室政治.md "wikilink")

[辦公室](../Category/辦公室.md "wikilink") [B辦](../Category/管理学.md "wikilink")

1.