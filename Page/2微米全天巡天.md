|                                                                                                      |
| :--------------------------------------------------------------------------------------------------: |
| [<File:2MASS> logo.jpg](https://zh.wikipedia.org/wiki/File:2MASS_logo.jpg "fig:File:2MASS logo.jpg") |
|                                                  組織                                                  |
|                                                  波長                                                  |
|                                                 資料來源                                                 |
|                                                 巡天目的                                                 |
|                                             Dataproducts                                             |
|                                                 其他名稱                                                 |
|                                                  網站                                                  |

**2MASS**

**2微米全天巡天（Two Micron All-Sky
Survey）**（2MASS）的工作開始於1997年，完成於2001年。使用的兩架望遠鏡，分別位於[北半球](../Page/北半球.md "wikilink")[美國](../Page/美國.md "wikilink")[亞利桑那州的](../Page/亞利桑那州.md "wikilink")[霍普金斯山和](../Page/霍普金斯山.md "wikilink")[南半球](../Page/南半球.md "wikilink")[智利](../Page/智利.md "wikilink")[托洛洛山美洲际天文台](../Page/托洛洛山美洲际天文台.md "wikilink")\[1\]，以確保能觀測到全部的天空。這是迄今最雄心勃勃的巡天計畫，經過整理的最後數據已經在2003年公佈。全部的天空都使用[紅外線的](../Page/紅外線.md "wikilink")2微米鄰近的3個波段：J
(1.25μm), H（1.65μm），和K<sub>s</sub>（2.17μm）完成掃描的工作\[2\]。

這次巡天的目的包括：

  - 發現在[隱匿帶內的](../Page/隱匿帶.md "wikilink")[星系](../Page/星系.md "wikilink")，這是在可見光的範圍內被我們的[銀河系遮蔽的天空](../Page/銀河系.md "wikilink")。
  - 第一次對[棕矮星的搜尋](../Page/棕矮星.md "wikilink")。 ,
    2MASS发现了173個[棕矮星](../Page/棕矮星.md "wikilink")，其中包括
    [2MASS 0939-2448](../Page/2MASS_0939-2448.md "wikilink"), [2MASS
    0415-0935](../Page/2MASS_0415-0935.md "wikilink"),
    [2M1207](../Page/2M1207.md "wikilink"), and [2MASS
    J04414489+2301513](../Page/2MASS_J04414489+2301513.md "wikilink").\[3\]
  - 對低質量[恆星的大規模搜尋](../Page/恆星.md "wikilink")，這些恆星在我們的[銀河系和其他的星系中都是很普遍的](../Page/銀河系.md "wikilink")。
  - 建立所有被檢出的恆星和星系的目錄。

令人欽佩的是最後一項目標已經完成了。所有[視亮度在](../Page/視亮度.md "wikilink")14等以上的的光點（[恆星](../Page/恆星.md "wikilink")、[行星](../Page/行星.md "wikilink")、[小行星](../Page/小行星.md "wikilink")）和擴散的光源（[星系](../Page/星系.md "wikilink")、[星雲](../Page/星雲.md "wikilink")），都被電腦自動建立成數位化的目錄資料，總數超過3億個點光源和1億個擴散光源都收錄在目錄中。
在2003年11月，一組科學家宣布發現了[大犬座矮星系](../Page/大犬座矮星系.md "wikilink")，是目前所知最接近我們銀河系的衛星星系，使用的就是2MASS的資料庫。

2MASS所獲得的數據和圖像都屬於[公共領域的資源](../Page/公共領域.md "wikilink")，已經放在網路上供任何人自由的使用。[1](http://www.ipac.caltech.edu/2mass/overview/access.html)\[4\]（但是要運用這些資訊還是需要有相當的專業知識與能力的）。也可以經由下列的網站鏈結到使用這些資料完成的論文：[2MASS
science
publications](http://www.ipac.caltech.edu/2mass/publications/index.html)
\[5\]。

2MASS是由[麻州大學的](../Page/麻州大學.md "wikilink")[紅外線資訊的處理和分析的中心](../Page/紅外線.md "wikilink")（IPAC）提議的計畫，原來的計畫名稱是UMASS。由[噴射推進實驗室](../Page/噴射推進實驗室.md "wikilink")（JPL）、[加州理工學院](../Page/加州理工學院.md "wikilink")、[NASA和](../Page/NASA.md "wikilink")[國家科學基金會](../Page/國家科學基金會.md "wikilink")（NSF）共同執行。
[Milky_Way_infrared.jpg](https://zh.wikipedia.org/wiki/File:Milky_Way_infrared.jpg "fig:Milky_Way_infrared.jpg")

## 参看

  - [:Category:2MASS天体](../Category/2MASS天体.md "wikilink")

## 参考资料

## 外部連結

  - [2MASS at IPAC](http://www.ipac.caltech.edu/2mass/)
  - [2MASS at
    UMass](https://web.archive.org/web/20060711032726/http://pegasus.phast.umass.edu/)
  - [2MASS Atlas Image Gallery: Miscellaneous
    Objects](http://www.ipac.caltech.edu/2mass/gallery/images_misc.html)
  - [Low-Mass Stars and Brown Dwarfs
    in 2MASS](http://www.ipac.caltech.edu/2mass/overview/dwarfs.html)

[Category:天文学目錄](../Category/天文学目錄.md "wikilink")
[Category:巡天项目](../Category/巡天项目.md "wikilink")
[Category:测光系统](../Category/测光系统.md "wikilink")
[Category:2MASS天体](../Category/2MASS天体.md "wikilink")

1.
2.
3.
4.

5.