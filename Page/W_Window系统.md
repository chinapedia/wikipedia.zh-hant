**W Window 系統**是一種圖形的[視窗系統](../Page/視窗系統.md "wikilink")，是現代[X
Window系統在名字和概念上的前輩](../Page/X_Window系統.md "wikilink")。

W原本是由Paul Asente和Brain
Peid於[史丹福大學所發展的](../Page/史丹福大學.md "wikilink")，且原本運行於[V](../Page/V_\(作業系統\).md "wikilink")。在1983年，Paul
Asente和Chris
Kent把系統移植到[VS100的](../Page/VS100.md "wikilink")[Unix上](../Page/Unix.md "wikilink")，給了一份拷貝給[MIT的電腦科學實驗室](../Page/MIT.md "wikilink")。

於1984年，MIT的[Bob
Scheifler把W的](../Page/Bob_Scheifler.md "wikilink")[同步協定換成](../Page/同步.md "wikilink")[非同步的](../Page/非同步.md "wikilink")，並且命名為X。

## 外部連結

  - [Mailing list post on
    linuxhacker.org](https://web.archive.org/web/20040127225048/http://www.linuxhacker.org/cgi-bin/ezmlm-cgi/5/2126)

[Category:視窗系統](../Category/視窗系統.md "wikilink")