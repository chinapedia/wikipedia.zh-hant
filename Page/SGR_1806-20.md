|- style="vertical-align: top;" | '''[距離](../Page/宇宙距離尺度.md "wikilink")
''' |
50,000[光年](../Page/光年.md "wikilink")（14,500[秒差距](../Page/秒差距.md "wikilink")）
  **SGR
1806-20**是一顆位於[人馬座的](../Page/人馬座.md "wikilink")[磁星](../Page/磁星.md "wikilink")（[中子星的一種](../Page/中子星.md "wikilink")），也是人們已知的[软γ射线复现源之一](../Page/软γ射线复现源.md "wikilink")。它距離[地球約](../Page/地球.md "wikilink")50,000[光年](../Page/光年.md "wikilink")，直徑不多於20[公里](../Page/公里.md "wikilink")，自轉週期為7.5秒。

這顆磁星曾於大約50,000年前發生爆炸，至2004年12月27日，它爆發後產生的輻射抵達地球。如果以伽瑪射線的光度作計算，這場爆發比滿月還要亮，其絕對星等約為負29等，也是地球能觀測的所有日外爆炸事件中，源頭最亮的一次，所釋出的功率也大得驚人，達1.3×10<sup>40</sup>
W，相等於[太陽](../Page/太陽.md "wikilink")15萬年的能量總和。這次爆炸的伽瑪射線擾動了地球的[電離層](../Page/電離層.md "wikilink")，使離子數量增加，從而令體積稍微增大。另外，這場爆炸也是自1604年，德國天文學家[開普勒觀測到的超新星](../Page/约翰内斯·开普勒.md "wikilink")[SN
1604爆炸以來](../Page/SN_1604.md "wikilink")，最大規模的一次。

如果同類型的爆炸發生在3[秒差](../Page/秒差距.md "wikilink")（10光年）的距離以內，地球的臭氧層將被摧毀，其威力相等於12,000[公噸的TNT炸藥在](../Page/公噸.md "wikilink")7.5公里上空爆炸（50
TJ）。幸好在此範圍內並沒有這些天體，在現今已知的磁星中，最近的1E 2259+586，距離地球4,000秒差（13,000光年）。

## 相關網站

  - [NASA: Cosmic Explosion Among the Brightest in Recorded
    History](http://www.nasa.gov/vision/universe/watchtheskies/swift_nsu_0205.html)
  - [天文史上銀河系最劇烈的爆發](https://web.archive.org/web/20070928022932/http://www.tam.gov.tw/news/2005/200502/05022002.htm)
  - [Sky and
    Telescope：最明亮的爆发](https://web.archive.org/web/20051128235205/http://www.astron.sh.cn/yiwen/2005/yw050225-BrightestBlast.htm)
  - [中子星爆發橫越銀河系](http://www.starrix.hk/database/star_neutron_star_flash.html)

[Category:人马座](../Category/人马座.md "wikilink")
[Category:磁星](../Category/磁星.md "wikilink")
[Category:软γ射线复现源](../Category/软γ射线复现源.md "wikilink")
[Category:1979年发现的天体](../Category/1979年发现的天体.md "wikilink")