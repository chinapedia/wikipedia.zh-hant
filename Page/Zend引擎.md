**Zend引擎**是一個[开源脚本引擎](../Page/开源.md "wikilink")（一個[虛擬機](../Page/虛擬機器.md "wikilink")），因作为[PHP語言的重要核心而聞名](../Page/PHP.md "wikilink")。它原由仍在以色列技術學院的學生
Andi Gutmans 與 Zeev Suraski
所開發。他們之後在[以色列](../Page/以色列.md "wikilink")[特拉维夫的](../Page/特拉维夫.md "wikilink")[拉馬干](../Page/拉馬干.md "wikilink")（Ramat
Gan）創立了[Zend
技術公司](../Page/Zend_Technologies.md "wikilink")。Zend一名為他們名字Zeev和Andi所組成的新字。

第一版的 Zend
引擎在1999年伴隨著PHP第四版問世。它是高度最佳化的後台[模組](../Page/模組.md "wikilink")。效能、可靠與延展性是它讓PHP更強更大眾化的主要原因。

Zend Engine II 随着 PHP 5 问世。

最新的版本是Zend Engine III，\[1\] 最初代号为 phpng，这是为了PHP7和大幅提高性能而开发。

## 参考资料

## 外部連結

  -
  -
[Category:PHP](../Category/PHP.md "wikilink")

1.