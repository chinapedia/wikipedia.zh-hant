《**ABC世界新聞**》（ABC World News
Tonight）是[美國全國](../Page/美國.md "wikilink")[電視聯播網之一](../Page/電視聯播網.md "wikilink")[美國廣播公司](../Page/美國廣播公司.md "wikilink")（ABC）的旗艦新聞節目。現時其週一至週五主播為[大衛·繆爾](../Page/大衛·繆爾.md "wikilink")（David
Muir），週六主播是塞西莉亞·維加（Cecilia Vega），週日主播則是湯姆·拉馬斯（Tom Llamas）。

節目連同廣告時段全長三十分鐘。值得留意的是，節目的播出時間因地區而異；ABC的某些地區聯播分台安排於傍晚5:30播放本節目，而其他分台則分別於傍晚6:00及6:30播放本節目。其競爭對手《[CBS晚間新聞](../Page/CBS晚間新聞.md "wikilink")》及《[NBC晚間新聞](../Page/NBC晚間新聞.md "wikilink")》亦採用類似安排。

[香港的](../Page/香港.md "wikilink")[無線電視](../Page/無線電視.md "wikilink")[明珠台曾于](../Page/明珠台.md "wikilink")2009年6月前播放《ABC世界新闻》，之后由《NBC世界新聞》代替。[台灣的](../Page/台灣.md "wikilink")[中華電視公司](../Page/中華電視公司.md "wikilink")（華視）曾於1990年代以《美國ABC英語新聞》為名播放《ABC世界新聞》，全程加註華視自行翻譯的中文字幕。

## 历史

ABC從1953年起製作全國新聞聯播，但多年來收視一直欠佳。ABC遂於1978年改革其新聞部，而全國新聞聯播亦於同年7月10日改稱《今夜世界新聞》（World
News
Tonight）。經過一連串改革後，節目的收視漸見起色，更開始超越其對手《[CBS晚間新聞](../Page/CBS晚間新聞.md "wikilink")》及《[NBC晚間新聞](../Page/NBC晚間新聞.md "wikilink")》。《今夜世界新闻》开播初期，每次新闻有三位主播，分别是华盛顿演播室的首席主播、国内新闻主播、国际新闻主播[彼得·詹宁斯](../Page/彼得·詹宁斯.md "wikilink")，另外也会不时播出[芭芭拉·沃尔特斯的专题报道及](../Page/芭芭拉·沃尔特斯.md "wikilink")的评论。

弗兰克·莱纳德去世后，[彼得·詹寧斯於](../Page/彼得·詹寧斯.md "wikilink")1983年8月9日成為《今夜世界新聞》的首席主播，此后在1984年麦克斯·罗宾逊去世后，彼得·詹宁斯成为《今夜世界新闻》的唯一一个常规主播，直至2005年4月5日因癌病纏身而結束其主播生涯，並於同年8月7日去世。

其後和於2006年1月出任節目的常任主播，然而鲍勃·伍德罗弗於1月尾在[伊拉克採訪期間受傷](../Page/伊拉克.md "wikilink")，伊丽莎白·瓦加斯亦於數星期後宣佈其孕訊，[查尔斯·吉布森遂從](../Page/查尔斯·吉布森.md "wikilink")5月29日起取代兩者成為節目的常任主播，節目並於同年7月19日改為《世界新闻》（World
News）。節目從2008年8月25日起（即[美國民主黨全國代表大會首天](../Page/美國民主黨.md "wikilink")）以[高清製作](../Page/高清.md "wikilink")。查尔斯·吉布森於2009年12月18日播報完《世界新聞》後正式展開其退休生涯，而[黛安·索耶則於](../Page/黛安·索耶.md "wikilink")12月21日成為該節目的常規主播。

2014年9月1日，ABC新闻杂志节目《[20/20](../Page/20/20_\(节目\).md "wikilink")》的主播[大卫·缪尔出任ABC晚间全国新闻联播的主播](../Page/大卫·缪尔.md "wikilink")，并将《世界新闻》更名为《今夜世界新闻》（World
News Tonight）。\[1\]

## 参考资料

## 外部連結

  - [ABC News: World News](http://abcnews.go.com/WN/)

[Category:美國電視新聞節目](../Category/美國電視新聞節目.md "wikilink")
[Category:美國廣播公司電視節目](../Category/美國廣播公司電視節目.md "wikilink")
[Category:無綫電視外購節目](../Category/無綫電視外購節目.md "wikilink")
[Category:華視外購電視節目](../Category/華視外購電視節目.md "wikilink")
[Category:英语电视节目](../Category/英语电视节目.md "wikilink")

1.