**濁唇齒擦音**是[輔音的一種](../Page/輔音.md "wikilink")，用於一些[口語中](../Page/口語.md "wikilink")。濁唇齒擦音在[國際音標的符號是](../Page/國際音標.md "wikilink")，X-SAMPA音標的符號則是。[漢語中的](../Page/漢語.md "wikilink")[吳語](../Page/吳語.md "wikilink")、[关中话均有此音](../Page/关中话.md "wikilink")，其餘像[北京官話及](../Page/北京官話.md "wikilink")[粵語均清化為](../Page/粵語.md "wikilink")\[f\]。

## 特徵

## 該音见于漢語

[客家話](../Page/客家話.md "wikilink")\[1\]、[老國音裏保留此音](../Page/老國音.md "wikilink")。

## 該音見於英語

英語中以 v 標示此音，如 *visit* 和
*rave*。以其他缺少此音的語言為母語的人要讀這些字時，可能會以[清唇齒擦音](../Page/清唇齒擦音.md "wikilink")\[f\]、[濁雙唇塞音](../Page/濁雙唇塞音.md "wikilink")\[b\]或者[雙唇無擦通音](../Page/雙唇無擦通音.md "wikilink")\[β̞\]代替。例子有[日語](../Page/日語.md "wikilink")、[漢語族部份方言](../Page/漢語族.md "wikilink")，如[閩語等](../Page/閩語.md "wikilink")，以及[印度-雅利安語支的語言等](../Page/印度-雅利安語支.md "wikilink")。

## 該音見於西班牙語

在前古典時期的歐洲[西班牙語中](../Page/西班牙語.md "wikilink")，[音素](../Page/音素.md "wikilink")/v/和音素/b/是分開的。但在大約十四至十五世紀時/v/就被/b/漸漸取代。但究竟音素v代表的是\[v\]還是[濁雙唇擦音](../Page/濁雙唇擦音.md "wikilink")\[β\]在語音學界仍然有分岐。有語音學家認為v應該是代表\[v\]，
但亦有人持相反意見。 雖然如此，大部分研究都同意到了十七世紀，西班牙人都不再區分音素/v/和/b/。
某些被分隔的口音可能到了十七世紀仍然區分這兩個音素，但到現在，無論是歐洲還是[拉丁美洲的西班牙語都不再有這種差別](../Page/拉丁美洲.md "wikilink")，/v/和/b/都只會讀成[濁雙唇塞音](../Page/濁雙唇塞音.md "wikilink")\[b\]，或者在兩個[元音中間讀成濁雙唇擦音](../Page/元音.md "wikilink")\[β\]。這種轉變令人經常會將字裡面的v和b混淆。例如，*bajar*和*derivada*會被誤寫為"vajar"和"deribada"。因為在小楷字母中b比v高，人在串字時將"b"講成"B
grande"(大字母B)，將"v"講成"B chica"
(小字母B)來避免誤寫。某些受過教育的人，尤其在受英語影響深的[美國會嘗試以串字的規則來區分b和v](../Page/美國.md "wikilink")，但推行西班牙語的機構[皇家西班牙學院已經不再支持這種做法](../Page/皇家西班牙學院.md "wikilink")，並視這為不正統。

[Category:擦音](../Category/擦音.md "wikilink")
[Category:唇齒音](../Category/唇齒音.md "wikilink")

1.  <http://www.hkilang.org/NEW_WEB/page/dict_pinyin> 香港本土語言保育協會 發聲字典