**西維魯縣**
（，或稱）是[愛沙尼亞](../Page/愛沙尼亞.md "wikilink")15个县之一，它位于爱沙尼亚北部，北臨[芬蘭灣](../Page/芬蘭灣.md "wikilink")。往东为[东维鲁县](../Page/东维鲁县.md "wikilink")，往南为[约格瓦县](../Page/约格瓦县.md "wikilink")，往西为[耶尔瓦县和](../Page/耶尔瓦县.md "wikilink")[哈尔尤县](../Page/哈尔尤县.md "wikilink")。面積3,465平方公里。2013年人口為58,806人，占全国人口4.5%。\[1\]首府[拉克韦雷](../Page/拉克韦雷.md "wikilink")。

下分二市十三鎮。

## 参考资料

[Category:愛沙尼亞行政區劃](../Category/愛沙尼亞行政區劃.md "wikilink")

1.