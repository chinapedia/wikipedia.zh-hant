[Homeyotwobuildings.jpg](https://zh.wikipedia.org/wiki/File:Homeyotwobuildings.jpg "fig:Homeyotwobuildings.jpg")航友宾馆与航友宾馆二号楼
(二号楼是春秋航空总部)\]\]
[B-1023@HKG_(20180918133830).jpg](https://zh.wikipedia.org/wiki/File:B-1023@HKG_\(20180918133830\).jpg "fig:B-1023@HKG_(20180918133830).jpg")客機在[香港國際机场](../Page/香港國際机场.md "wikilink")\]\]
[Spring_Airlines_aircraft_with_passenger_boarding_stairs_@_PVG.jpg](https://zh.wikipedia.org/wiki/File:Spring_Airlines_aircraft_with_passenger_boarding_stairs_@_PVG.jpg "fig:Spring_Airlines_aircraft_with_passenger_boarding_stairs_@_PVG.jpg")\]\]

**春秋航空[股份有限公司](../Page/股份有限公司.md "wikilink")**簡稱**春秋航空**（[IATA](../Page/IATA.md "wikilink")：**9C**，[ICAO](../Page/ICAO.md "wikilink")：**CQH**，），是首個[中国大陸民營資本獨資經營的](../Page/中华人民共和国.md "wikilink")[低成本航空公司](../Page/低成本航空公司.md "wikilink")，也是首家由旅行社起家的[廉價航空公司](../Page/廉價航空公司.md "wikilink")。該公司自2004年5月26日得到当时的中國民航總局（现[交通部](../Page/中华人民共和国交通部.md "wikilink")[中国民用航空局](../Page/中国民用航空局.md "wikilink")）批准後開始籌建，約一年左右成功開航。春秋航空公司以[上海為基地](../Page/上海.md "wikilink")，現任董事長為[王煜](../Page/王煜.md "wikilink")。春秋航空公司是中國第一家不參加中國民航聯網銷售系統（CRS）的航空公司。春秋航空首航班機于2005年7月18日上午由[上海虹橋機場起飛前往](../Page/上海虹橋機場.md "wikilink")[山東](../Page/山東.md "wikilink")[煙台](../Page/煙台.md "wikilink")。

春秋航空公司以[上海虹桥国际机场及](../Page/上海虹桥国际机场.md "wikilink")[上海浦东国际机场為主要起降機場](../Page/上海浦东国际机场.md "wikilink")，早期承运的航線全為國內航線，2010年起先後开通到达日本、香港、澳门、泰國、柬埔寨、韩国及馬來西亞的航線，未来还计划开通至其他城市的航线。

## 概述

春秋航空公司是[春秋国旅的子公司](../Page/春秋国际旅行社.md "wikilink")。

这家航空公司的第一架飞机是一架[空客A320](../Page/A320.md "wikilink")，于2005年7月12日从[莲花航空公司购得](../Page/莲花航空.md "wikilink")。首航是2005年7月18日飞行的一架从[上海至](../Page/上海.md "wikilink")[烟台的航班](../Page/烟台.md "wikilink")。同日，春秋航空[上海至](../Page/上海.md "wikilink")[桂林的航线也一同启航](../Page/桂林.md "wikilink")。

作为一家低成本航空公司，为节约成本，春航鼓励乘客从其网站购票，对乘客可免费携带的行李重量及体积作了较其它航空公司更为严格的限制、简化了机票和登机牌，座距為比其他航空公司更小的28英寸且不能调节座椅靠背。同时在飞机上，春航不提供免费的机上膳食和饮料（初期提供免费瓶装水，但现已改为收费）。乘客如有需要，可购买便餐及饮料。春航有时还会在飞行过程中进行名为“空中商城”活动，推銷由其出品的飛機模型等產品（深夜航班上除外）。此外，春秋航空还曾尝试推出“[站票](../Page/站票.md "wikilink")”，通过改装座椅缩短前后排间距，以增加约40%的载客量\[1\]。但该计划因安全问题而搁置\[2\]。

2009年7月下旬，春航开始设立国际航线（首條国际航班飞往[日本的](../Page/日本.md "wikilink")[茨城机场](../Page/茨城机场.md "wikilink")），这使其成为中国大陆第二家可飞行国际航线的民营航空公司。\[3\]。目前，开通了连接中国大陆城市至[港澳地區](../Page/港澳地區.md "wikilink")、[中華民國(臺灣)](../Page/中華民國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[韓國和](../Page/韓國.md "wikilink")[馬來西亞等國家的中](../Page/馬來西亞.md "wikilink")、短途國際航线。

## 航点

## 机队

截至2018年10月，春秋航空机队如下，平均机龄4.7年：

<center>

<table>
<caption><strong>春秋航空机队</strong>[4]</caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><font color=white>機型</p></td>
<td style="text-align: center;"><p><font color=white>數量</p></td>
<td style="text-align: center;"><p><font color=white>訂購中</p></td>
<td style="text-align: center;"><p><font color=white>選購權</p></td>
<td style="text-align: center;"><p><font color=white>載客量</p></td>
<td style="text-align: center;"><p><font color=white>備註</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/空中客车A320.md" title="wikilink">空中巴士A320-214</a></p></td>
<td style="text-align: center;"><center>
<p>80</p>
</center></td>
<td style="text-align: center;"><center>
<p>-</p>
</center></td>
<td style="text-align: center;"><center>
<p>—</p>
</center></td>
<td style="text-align: center;"><p>174</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>180</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>186</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/空中客车A320neo系列.md" title="wikilink">空中巴士A320neo</a>[5]</p></td>
<td style="text-align: center;"><p>1</p></td>
<td style="text-align: center;"><p>44</p></td>
<td style="text-align: center;"><p>—</p></td>
<td style="text-align: center;"><p>186[6]</p></td>
<td style="text-align: center;"><p>—</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/空中客车A320neo系列.md" title="wikilink">空中巴士A321neo</a>[7]</p></td>
<td style="text-align: center;"><p>—</p></td>
<td style="text-align: center;"><p>15</p></td>
<td style="text-align: center;"><p>—</p></td>
<td style="text-align: center;"><p>有待公布</p></td>
<td style="text-align: center;"><p>—</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>總數</p></td>
<td style="text-align: center;"><p>81</p></td>
<td style="text-align: center;"><p>59</p></td>
<td style="text-align: center;"><p>—</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

</center>

## 参考资料

## 外部链接

  - [春秋航空](http://www.ch.com)

  -
  -
  - [春秋航空航班时刻表](https://flights.ch.com/flight-date)

[Category:中国航空公司](../Category/中国航空公司.md "wikilink")
[Category:低成本航空公司](../Category/低成本航空公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:上海公司](../Category/上海公司.md "wikilink")
[Category:2004年成立的航空公司](../Category/2004年成立的航空公司.md "wikilink")
[Category:上海品牌](../Category/上海品牌.md "wikilink")
[Category:沪深300指数成份股](../Category/沪深300指数成份股.md "wikilink")

1.

2.

3.

4.

5.

6.  [春秋航空迎来第一架空客A320NEO](http://www.kankanews.com/a/2018-10-24/0038631222.shtml?appid=411227)

7.