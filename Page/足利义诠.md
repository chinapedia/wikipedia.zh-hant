**足利义诠**（）是[室町幕府的第](../Page/室町幕府.md "wikilink")2代[将军](../Page/征夷大将军.md "wikilink")。[足利尊氏的第三子](../Page/足利尊氏.md "wikilink")、长男。母亲为[北条久时的女儿](../Page/北条久时.md "wikilink")[北条登子](../Page/赤橋登子.md "wikilink")、[镰仓幕府最後](../Page/镰仓幕府.md "wikilink")[执权](../Page/执权.md "wikilink")[赤桥守时的妹妹](../Page/北条守时.md "wikilink")。正室为[涩川义季的女儿](../Page/涩川义季.md "wikilink")[涩川幸子](../Page/涩川幸子.md "wikilink")。幼名**千寿王**。其子为[足利義满](../Page/足利義满.md "wikilink")、[足利满诠](../Page/足利满诠.md "wikilink")。

## 生涯

### 早年生活

1333年，[後醍醐上皇在](../Page/後醍醐天皇.md "wikilink")[伯耆國](../Page/伯耆國.md "wikilink")[船上山舉兵](../Page/船上山.md "wikilink")，[鐮倉幕府派](../Page/鐮倉幕府.md "wikilink")[足利尊氏為總大將鎮壓](../Page/足利尊氏.md "wikilink")。前往[京都之際](../Page/京都.md "wikilink")，千壽王和母親[赤橋登子一起被](../Page/赤橋登子.md "wikilink")[北條氏扣為人質](../Page/北條氏.md "wikilink")，留在[鐮倉](../Page/鐮倉.md "wikilink")。

足利尊氏在[丹波國反叛鐮倉幕府](../Page/丹波國.md "wikilink")，攻佔了京都的[六波羅探題後](../Page/六波羅探題.md "wikilink")，千壽王被足利家的家臣救出鐮倉，[新田義貞奉之為主](../Page/新田義貞.md "wikilink")，攻佔鐮倉。在此期間，千壽王在家臣的輔佐下，以父親尊氏的名義，對參加[鐮倉之戰的武士發放軍忠狀](../Page/鐮倉之戰.md "wikilink")，這使他後來成為了足利氏的棟樑。[後醍醐天皇施行](../Page/後醍醐天皇.md "wikilink")[建武新政時](../Page/建武新政.md "wikilink")，義詮由叔父[足利直義輔佐鎮守鐮倉](../Page/足利直義.md "wikilink")。[足利尊氏反叛後醍醐天皇時](../Page/足利尊氏.md "wikilink")，足利義詮與父親一起同[南朝作戰](../Page/南朝_\(日本\).md "wikilink")，義詮主要負責鎮守鐮倉，統治[關東一帶](../Page/關東.md "wikilink")。

足利尊氏開設[幕府之後](../Page/室町幕府.md "wikilink")，足利家的[執事](../Page/執事.md "wikilink")[高師直同尊氏的弟弟](../Page/高師直.md "wikilink")[足利直義不和](../Page/足利直義.md "wikilink")，引起了[觀應擾亂](../Page/觀應擾亂.md "wikilink")，高師直發動政變，足利直義失勢。足利義詮被召回京都，代理直義管理幕府的政務。1351年（[正平](../Page/正平.md "wikilink")6年
/
[觀應](../Page/觀應.md "wikilink")2年）陰曆8月，足利義詮和足利尊氏決定廢除[北朝朝廷](../Page/北朝_\(日本\).md "wikilink")，向足利直義所支持的南朝投降，史稱[正平一統](../Page/正平一統.md "wikilink")。翌年南朝的[北畠親房和](../Page/北畠親房.md "wikilink")[楠木正儀攻陷京都](../Page/楠木正儀.md "wikilink")，義詮逃往[近江國避難](../Page/近江國.md "wikilink")。結果北朝方面的[光嚴上皇](../Page/光嚴天皇.md "wikilink")、[光明上皇](../Page/光明天皇.md "wikilink")、[崇光天皇以及皇太子](../Page/崇光天皇.md "wikilink")[直仁親王等北朝重要皇族都被南朝俘虜](../Page/直仁親王.md "wikilink")。不久義詮率軍反撲，奪回了京都，並在沒有[三神器的狀態下擁立](../Page/三神器.md "wikilink")[後光嚴天皇重建北朝](../Page/後光嚴天皇.md "wikilink")。1353年陰曆6月，[足利直冬和](../Page/足利直冬.md "wikilink")[山名時氏攻陷京都](../Page/山名時氏.md "wikilink")，但不久被收復。

### 將軍就任後

1358年（正平13年 /
[延文](../Page/延文.md "wikilink")3年）陰曆4月，足利尊氏逝世，12月義詮繼任[征夷大將軍之職](../Page/征夷大將軍.md "wikilink")。當時[中國地方的](../Page/中國地方.md "wikilink")[山名氏和](../Page/山名氏.md "wikilink")[大內氏叛服無常](../Page/大內氏.md "wikilink")，九州地方的[懷良親王等南朝勢力依然健在](../Page/懷良親王.md "wikilink")。義詮繼任後，立即對南朝的[河內和](../Page/河內.md "wikilink")[紀伊發起進攻](../Page/紀伊.md "wikilink")，攻下了[赤坂城等城](../Page/赤坂城.md "wikilink")。而另一方面，1361年（正平16年
/
[康安元年](../Page/康安.md "wikilink")）與[執事](../Page/執事.md "wikilink")[細川清氏](../Page/細川清氏.md "wikilink")、[畠山國清敵對的](../Page/畠山國清.md "wikilink")[仁木義長投奔南朝](../Page/仁木义长.md "wikilink")，不久以後，細川清氏又在[佐佐木道譽的讒言下叛降南朝](../Page/佐佐木道譽.md "wikilink")，並協助南朝再次短暫地攻佔京都。1362年，在細川清氏、畠山國清先後失勢後，足利義詮任命[斯波義將為管領](../Page/斯波義將.md "wikilink")。1363年[大內氏](../Page/大內氏.md "wikilink")、[山名氏先後前來參謁](../Page/山名氏.md "wikilink")，幕府政權得到鞏固；同時仁木義長和[桃井直常](../Page/桃井直常.md "wikilink")、[石塔賴房回歸幕府](../Page/石塔賴房.md "wikilink")，推進了同南朝的講和。同年在義詮的奏請下，後光嚴天皇下詔編纂第19部[敕撰和歌集](../Page/敕撰和歌集.md "wikilink")──《[新拾遺和歌集](../Page/新拾遺和歌集.md "wikilink")》。1365年陰曆2月將邸宅遷到了三條坊門萬里小路的新邸。其間義詮著手整備訴訟制度，縮小了[評定眾](../Page/評定眾.md "wikilink")、[引付眾的規模](../Page/引付眾.md "wikilink")，試圖擴大將軍的親政權（[御前沙汰](../Page/御前沙汰.md "wikilink")）。在同[園城寺和](../Page/園城寺.md "wikilink")[南禪寺的爭端中](../Page/南禪寺.md "wikilink")，義詮派[今川貞世](../Page/今川貞世.md "wikilink")（了俊）管領園城寺，並毀棄了[逢坂關](../Page/逢坂關.md "wikilink")。1366年[斯波氏失權](../Page/斯波氏.md "wikilink")，[細川賴之成為](../Page/細川賴之.md "wikilink")[管領](../Page/管領.md "wikilink")（[貞治之變](../Page/貞治之變.md "wikilink")）。

1367年陰曆11月，義詮將自己和側室[紀良子所生的幼子](../Page/紀良子.md "wikilink")[足利義滿託付給了細川賴之](../Page/足利義滿.md "wikilink")，翌月病死，享年38歲。死前的兩天，足利義詮大量噴出鼻血，這在[三條公忠的日記](../Page/三條公忠.md "wikilink")《[後愚昧記](../Page/後愚昧記.md "wikilink")》中有記載。根據足利義詮的遺言，遺體被埋葬在了觀林寺（今善入山[寶筐院](../Page/寶筐院.md "wikilink")）五輪石塔（[楠木正行之墓](../Page/楠木正行.md "wikilink")）旁邊，稱寶筐印塔。

### 居所的名稱

由於足利義詮的邸宅位於三條坊門，因此被人稱作「**坊門殿**」。另外室町季顯處購買了「花亭」，並以之為別邸。後來足利家將「花亭」獻給[崇光上皇作為](../Page/崇光天皇.md "wikilink")[仙洞御所](../Page/仙洞御所.md "wikilink")（上皇的居住地）。[足利義滿就任將軍之時](../Page/足利義滿.md "wikilink")，天皇將花亭贈送給了義滿，並作為義滿的邸宅。世人稱之為「[花之御所](../Page/花之御所.md "wikilink")」。

## 官歷

※日期＝旧曆

| 西曆     | 南朝                                                                                             | 北朝                               | 月日     | 内容                                                                     |
| ------ | ---------------------------------------------------------------------------------------------- | -------------------------------- | ------ | ---------------------------------------------------------------------- |
| 1335年  | [建武](../Page/建武_\(日本\).md "wikilink")2年                                                        |                                  | 4月7日   | 敘[從五位下](../Page/從五位下.md "wikilink")。                                   |
| 1344年  | [興國](../Page/興國.md "wikilink")5年                                                               | [康永](../Page/康永.md "wikilink")3年 | 3月16日  | 昇敘[正五位下](../Page/正五位下.md "wikilink")。                                  |
| 3月18日  | 任[左馬頭](../Page/馬寮.md "wikilink")。                                                              |                                  |        |                                                                        |
| 1347年  | [正平](../Page/正平_\(日本\).md "wikilink")2年                                                        | [貞和](../Page/貞和.md "wikilink")3年 | 12月3日  | 昇敘[從四位下](../Page/從四位下.md "wikilink")。                                  |
| 1350年  | 正平5年                                                                                           | [觀應元年](../Page/觀應.md "wikilink") | 8月22日  | 補任[参議](../Page/参議.md "wikilink")、兼任[左近衛中将](../Page/近衛府.md "wikilink")。 |
| 1356年  | 正平11年                                                                                          | [延文元年](../Page/延文.md "wikilink") | 8月23日  | 昇敘[從三位](../Page/從三位.md "wikilink")。参議左近衛中将如元。                          |
| 1358年  | 正平13年                                                                                          | 延文3年                             | 12月18日 | 受封[征夷大将軍](../Page/征夷大将軍.md "wikilink")                                 |
| 1359年  | 正平14年                                                                                          | 延文4年                             | 2月4日   | 兼任[武藏守](../Page/武藏国.md "wikilink")。                                    |
| 1363年  | 正平18年                                                                                          | [貞治](../Page/貞治.md "wikilink")2年 | 1月28日  | 轉任[權大納言](../Page/大納言.md "wikilink")。                                   |
| 7月29日  | 昇敘[從二位](../Page/從二位.md "wikilink")。權大納言如元。                                                     |                                  |        |                                                                        |
| 1367年  | 正平22年                                                                                          | 貞治6年                             | 1月5日   | 昇敘[正二位](../Page/正二位.md "wikilink")。                                    |
| 12月7日  | 逝世。                                                                                            |                                  |        |                                                                        |
| 12月20日 | [贈](../Page/贈位.md "wikilink")[從一位](../Page/從一位.md "wikilink")[左大臣](../Page/左大臣.md "wikilink")。 |                                  |        |                                                                        |
|        |                                                                                                |                                  |        |                                                                        |

## 墓葬、肖像畫和木像

[Fujiwara_no_Mitsuyoshi.jpg](https://zh.wikipedia.org/wiki/File:Fujiwara_no_Mitsuyoshi.jpg "fig:Fujiwara_no_Mitsuyoshi.jpg")

  - 法名：寶篋院瑞山道權
  - 墓所：[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[北区萬年山](../Page/北区_\(京都市\).md "wikilink")[等持院](../Page/等持院.md "wikilink")。另有位於[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")[右京区的善入山](../Page/右京区.md "wikilink")[宝筐院或](../Page/宝筐院.md "wikilink")[静岡县](../Page/静岡县.md "wikilink")[三島市](../Page/三島市.md "wikilink")[川原谷的地福山](../Page/川原谷.md "wikilink")[宝鏡院兩種說法](../Page/宝鏡院.md "wikilink")。
  - 肖像畫：[寶筐院所藏](../Page/寶筐院.md "wikilink")（見條目開頭），被日本列為[重要文化財](../Page/重要文化財.md "wikilink")。此外，美術史學者[米倉迪夫認為](../Page/米倉迪夫.md "wikilink")[神護寺所藏的](../Page/神護寺.md "wikilink")[神護寺三像中](../Page/神護寺三像.md "wikilink")，所謂的「[藤原光能像](../Page/藤原光能.md "wikilink")」（見右圖），其實是足利義詮像，因為他與[等持院內的足利義詮木像面部特徵很相似](../Page/等持院.md "wikilink")。
  - 木像：[等持院](../Page/等持院.md "wikilink")、[鑁阿寺](../Page/鑁阿寺.md "wikilink")、[瑞泉寺所藏](../Page/瑞泉寺.md "wikilink")。
      - 1863年（[文久](../Page/文久.md "wikilink")3年），[尊皇攘夷派曾將等持院內的](../Page/尊皇攘夷.md "wikilink")[足利尊氏](../Page/足利尊氏.md "wikilink")、足利義詮、[足利義滿的木像梟首](../Page/足利義滿.md "wikilink")，並在[三条河原示眾](../Page/三条河原.md "wikilink")（[足利三代木像梟首事件](../Page/足利三代木像梟首事件.md "wikilink")）。

## 人物形象

《[太平記](../Page/太平記.md "wikilink")》將足利義詮描繪成一個容易被他人的言語左右、而且沉溺於酒色的愚鈍人物。事實上，足利義詮在父親[尊氏不在的時候發佈](../Page/足利尊氏.md "wikilink")[半濟令](../Page/半濟令.md "wikilink")，確保了武家的經濟實力。另一方面在[足利直冬的進攻下](../Page/足利直冬.md "wikilink")，幕府陷入窘境；而足利義詮在神南之戰中破直冬軍，此後又立下了許多戰功。[細川清氏失勢](../Page/細川清氏.md "wikilink")（[康安政變](../Page/康安政變.md "wikilink")）後，義詮利用[斯波氏的暫時失勢](../Page/斯波氏.md "wikilink")（[貞治之變](../Page/貞治之變.md "wikilink")）這一機會壓制各地[守護的勢力](../Page/守護.md "wikilink")，提高了幕府將軍的權勢。

此外在義詮任將軍期間，原本支持[南朝的強大的守護大名](../Page/南朝_\(日本\).md "wikilink")[大內弘世](../Page/大內弘世.md "wikilink")、[山名時世等人歸順幕府和](../Page/山名時世.md "wikilink")[北朝](../Page/北朝_\(日本\).md "wikilink")；[仁木義長](../Page/仁木義長.md "wikilink")、[桃井直常](../Page/桃井直常.md "wikilink")、[石塔賴房等人再次歸順幕府](../Page/石塔賴房.md "wikilink")。因此不能無視他對[南北朝動亂的平息和幕府政治安定所作出的貢獻](../Page/南北朝_\(日本\).md "wikilink")。此外，義詮向[奧州派遣](../Page/陸奧.md "wikilink")[石橋棟義](../Page/石橋棟義.md "wikilink")，向[九州派遣](../Page/九州島.md "wikilink")[斯波氏經](../Page/斯波氏經.md "wikilink")、[澀川義行](../Page/澀川義行.md "wikilink")，平定了九州。

足利義詮與正室[澀川幸子僅生下了一個兒子](../Page/澀川幸子.md "wikilink")，但卻早夭。此後義詮再也沒有生過兒子，可能是因為他以前與許多公卿的女兒、天皇身邊的女官[性交過度導致了](../Page/性交.md "wikilink")[腎虛](../Page/腎虛.md "wikilink")。因此後來不得不將之前與側室所生的[足利義滿立為繼承人](../Page/足利義滿.md "wikilink")，由[管領](../Page/管領.md "wikilink")[細川賴之輔佐](../Page/細川賴之.md "wikilink")。

## 系谱

  - 父：足利尊氏
  - 母：赤桥登子
  - 兄弟
      - [足利基氏](../Page/足利基氏.md "wikilink")
      - [足利直冬](../Page/足利直冬.md "wikilink")（足利直義的養子）
  - 正室：澀川幸子
      - 子：千寿王
  - 侧室：纪良子（[順德天皇皇孫](../Page/順德天皇.md "wikilink")外孫女）
      - 子：[足利義滿](../Page/足利義滿.md "wikilink")
      - 子：[足利满诠](../Page/足利满诠.md "wikilink")
      - 子：清祖

## 接受義詮偏諱的人物

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [荒川詮賴](../Page/荒川詮賴.md "wikilink")
  - [一色詮範](../Page/一色詮範.md "wikilink")
  - [上野詮兼](../Page/上野詮兼.md "wikilink")
  - [大內義弘](../Page/大內義弘.md "wikilink")
  - [白井詮常](../Page/白井詮常.md "wikilink")
  - [大掾詮國](../Page/大掾詮國.md "wikilink")
  - [中條詮秀](../Page/中條詮秀.md "wikilink")
  - [富樫詮親](../Page/富樫詮親.md "wikilink")
  - [土岐詮直](../Page/土岐詮直.md "wikilink")
  - [細川詮春](../Page/細川詮春.md "wikilink")

</div>

<div style="float: left; vertical-align: top; white-space: nowrap; margin-right: 1em;">

  - [桃井詮信](../Page/桃井詮信.md "wikilink")
  - [湯川詮光](../Page/湯川詮光.md "wikilink")

</div>

除了上面所列出的以外、[高師**詮**和](../Page/高師詮.md "wikilink")[京極高**詮**等](../Page/京極高詮.md "wikilink")，可能也是接受了義詮的偏諱。

[Category:將軍足利氏](../Category/將軍足利氏.md "wikilink")
[Category:室町幕府將軍](../Category/室町幕府將軍.md "wikilink")
[Category:關東公方](../Category/關東公方.md "wikilink")
[Category:山城國出身人物](../Category/山城國出身人物.md "wikilink")