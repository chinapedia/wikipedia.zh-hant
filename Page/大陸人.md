**大陸人**也称为**[内地人](../Page/内地人.md "wikilink")**，是對居住或出身於[中國大陸的人士的非正式稱呼](../Page/中國大陸.md "wikilink")，但稱呼不適用於[香港](../Page/香港居民.md "wikilink")、[澳門兩地的](../Page/澳門居民.md "wikilink")[本地人](../Page/本地人.md "wikilink")。與「[中國大陸](../Page/中國大陸.md "wikilink")」的用法相同，一般是出自政治上的考量而使用此詞。但[海南島的居民習慣把来自北方陸地的人都叫大陆人](../Page/海南島.md "wikilink")。

## 歷史

在1970年代末因為當時有相當部分的[知青偷渡出境](../Page/知青.md "wikilink")，所以在[臺灣](../Page/臺灣.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門開始多人用來指](../Page/澳門.md "wikilink")[中華人民共和國成立之後居住在](../Page/中華人民共和國.md "wikilink")[中國大陸的人民](../Page/中國大陸.md "wikilink")，在香港有「[阿燦](../Page/阿燦.md "wikilink")」的稱謂。

而在臺灣，[中華民國](../Page/中華民國.md "wikilink")[國民政府播遷來臺後的大陸人則被稱為](../Page/國民政府.md "wikilink")「[外省人](../Page/外省人.md "wikilink")」，對於[臺灣](../Page/臺灣.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門來說亦包括來自中國大陸的](../Page/澳門.md "wikilink")[移民](../Page/移民.md "wikilink")。中國的沿海地區及[海南省因為地處海岸或海上](../Page/海南省.md "wikilink")，與內陸地方文化差異很大，當地居民不被稱為大陸人。海南島的居民則稱所有来自其北方的人叫大陆人

## 海外華人

由於在[海外華人中](../Page/海外華人.md "wikilink")，原籍[粤](../Page/粤.md "wikilink")[閩](../Page/福建省.md "wikilink")[港澳臺人士佔有相當重的比例](../Page/港澳臺.md "wikilink")，故“大陸人”的稱謂在海外也得以沿用。

## 傳媒用語

[香港回歸之後](../Page/香港回歸.md "wikilink")，由於地理上香港亦位處「中國大陸」，以「大陸人」稱呼不包括香港在內的國民變得不合適，[香港政府及](../Page/香港政府.md "wikilink")[傳媒改稱](../Page/傳媒.md "wikilink")「中國大陸」為「[中國內地](../Page/中國內地.md "wikilink")」，「大陸人」則相應改稱為「[內地人](../Page/內地人.md "wikilink")」，因此「大陸人」一詞現已較少在香港傳媒聽到，但仍會普遍出現在民間口頭用語中。

不過在[臺灣](../Page/臺灣.md "wikilink")，此稱呼仍是最為普遍。部分親[泛綠傳媒會直呼為](../Page/泛綠.md "wikilink")「中國」，以表明臺灣不是中國一部分，但有時以大陸稱之。

## 臺灣

[臺灣閩南人還有以](../Page/臺灣閩南人.md "wikilink")[臺灣話](../Page/臺灣話.md "wikilink")（[閩南語方言](../Page/閩南語.md "wikilink")）稱呼其為「[阿陸仔](../Page/阿陸仔.md "wikilink")」（或諧音「阿六仔」、26，此詞有可能被認為帶有歧視）。另外常見的稱呼有「大陸客」（簡稱「陸客」，指到臺灣觀光的大陸旅客）、「陸配」（與臺灣人結婚的大陸配偶）、「[大陸妹](../Page/大陸妹.md "wikilink")」（早期指偷渡到臺灣的大陸女子，後來泛指大陸出身的年輕女子）

## 海南

因為地理關係，海南人習慣稱呼[琼州海峡以北的人為](../Page/琼州海峡.md "wikilink")「大陸人」，也稱「[內地人](../Page/內地人.md "wikilink")」，2012年[三亞宰客事件后因為非海南籍人员缺乏瞭解海南當地情況](../Page/三亞宰客事件.md "wikilink")，誤以為搞政治關係，而引發海南版的「兩岸對罵」。

## 参看

  - [中華人民共和國國民](../Page/中華人民共和國國民.md "wikilink")
  - [中華民國國民](../Page/中華民國國民.md "wikilink")
  - [華人](../Page/華人.md "wikilink")
  - [中國人](../Page/中國人.md "wikilink")
  - [臺灣人](../Page/臺灣人.md "wikilink")
  - [香港人](../Page/香港人.md "wikilink")
  - [澳門人](../Page/澳門人.md "wikilink")

[Category:特定人群稱謂](../Category/特定人群稱謂.md "wikilink")
[Category:中国人](../Category/中国人.md "wikilink")
[Category:中国大陆](../Category/中国大陆.md "wikilink")