**九龍巴士68X線**是[香港一條來往](../Page/香港.md "wikilink")[洪水橋](../Page/洪水橋.md "wikilink")（[洪福邨](../Page/洪福邨.md "wikilink")）和[旺角](../Page/旺角.md "wikilink")（[柏景灣](../Page/柏景灣.md "wikilink")）的巴士路線。\[1\]

當年[巴士阿叔事件就是在行駛這條路線的巴士上發生](../Page/巴士阿叔事件.md "wikilink")\[2\]\[3\]，但當時該路線是來往[元朗](../Page/元朗.md "wikilink")（東）至佐敦（匯翔道）。2015年6月6日起，68X全面縮短路線至[旺角（柏景灣）](../Page/旺角（柏景灣）巴士總站.md "wikilink")，不再途經油麻地及佐敦。

值得一提的是，2018年9月29日起往洪水橋方向新增的[紫翠花園站位於屯門區](../Page/紫翠花園.md "wikilink")，代表該線自約20年前大欖隧道通車後，首次再於屯門區設站。

而**九龍巴士268X線**是68X的輔助路線，為特快路線，來往洪水橋（洪福邨）和[佐敦](../Page/佐敦.md "wikilink")（[西九龍站](../Page/香港西九龍站.md "wikilink")），取道[西九龍走廊](../Page/西九龍走廊.md "wikilink")，不經[深水埗及](../Page/深水埗.md "wikilink")[美孚](../Page/美孚.md "wikilink")。

268X線起訖點雖與[63X線相同](../Page/九龍巴士63X線.md "wikilink")，但取道[大欖隧道](../Page/大欖隧道.md "wikilink")，不途經[屯門新市鎮](../Page/屯門新市鎮.md "wikilink")。2015年6月6日起，本線提升至全日服務，並取代主線在油麻地和佐敦的地位。

## 歷史

### 元朗至西九龍的交通歷史

早在1920年代，68X線前身，即[中美運輸公司之](../Page/中美運輸公司.md "wikilink")[元朗至](../Page/元朗.md "wikilink")[旺角之巴士線已投入服務](../Page/旺角.md "wikilink")。1920年代末期，[九巴接管此線](../Page/九巴.md "wikilink")，並改為9號線。在[香港日治時期開辦油麻地來往](../Page/香港日治時期.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")、元朗、[上水及](../Page/上水.md "wikilink")[大埔的巴士線取締](../Page/大埔_\(香港\).md "wikilink")9號線，同年縮短至上水，同年10月改編為5號線，來往[尖沙咀至上水](../Page/尖沙咀.md "wikilink")，1943年縮短為旺角至荃灣，及加開3班長途班次往來上水。

1946年，來往元朗及[尖沙咀碼頭的](../Page/尖沙咀碼頭.md "wikilink")3號線投入服務，每日固定班次7班。1947年10月改稱16號線，並且縮短至[佐敦道碼頭](../Page/佐敦道碼頭.md "wikilink")。16號線於1967年6月25日暫時停駛，直至同年7月25日重投服務，1970年2月15日配合元朗（東）巴士總站啟用，總站遷往元朗東。1973年7月16日，九巴路線進行大規模重組，16號線於當天改稱50號線。到了1978年5月16日，配合[屯門公路通車](../Page/屯門公路.md "wikilink")，九巴開辦[68號線](../Page/九龍巴士68線.md "wikilink")（經屯門公路），全程收費$1.5，50號線作為元朗的大線地位開始受到動搖，直至1982年，50號線縮短至[葵芳站](../Page/葵芳站.md "wikilink")，並改編號為50M號線，1984年9月23日配合新界西部份巴士路線重組而停止服務。

### 現在

1987年1月5日，本線開辦，大大改善了元朗市中心的對外交通，68號線作為元朗區大線的地位遭遇滑鐵盧。由於本線能直接往[西九龍心臟地帶](../Page/西九龍.md "wikilink")（與68相比，本線途經[荃灣路](../Page/荃灣路.md "wikilink")，不停荃灣、葵涌），一直深受乘客歡迎，成為[元朗區皇牌路線之一](../Page/元朗區.md "wikilink")，整個[元朗市中心來往西九龍的乘客已被本線吸納](../Page/元朗市中心.md "wikilink")，甚至在[屯門市中心一帶也有不少乘客上車](../Page/屯門市中心.md "wikilink")，因此68號線在九龍的客量降低。1998年5月26日，由於[大欖隧道啟用](../Page/大欖隧道.md "wikilink")，68號線永久停駛，本線成為元朗市中心至西九龍的唯一巴士線；同一天，本線改經大欖隧道，不再駛經屯門。

由2007年6月24日起，本線由元朗東總站遷往[洪水橋洪元路總站](../Page/洪水橋.md "wikilink")，結束長達20年[元朗（東）駐站生涯](../Page/元朗（東）巴士總站.md "wikilink")\[4\]，而原本於本線行走的[富豪超級奧林比安因為於洪元路總站出現轉向問題](../Page/富豪超級奧林比安.md "wikilink")，所以全部均被換作[丹尼士三叉戟巴士](../Page/丹尼士三叉戟三型.md "wikilink")；翌日起，九巴開辦68X的特別班次，由[山水樓開出](../Page/媽橫路巴士總站.md "wikilink")，經[媽橫路後返回原線](../Page/媽橫路.md "wikilink")\[5\]。

2014年9月13日起，實行68X線第一階段重組，改動如下：

  - 早上媽橫路往[佐敦的特別班次取消](../Page/佐敦.md "wikilink")，由新開辦的268X取代，新線取道[西九龍走廊](../Page/西九龍走廊.md "wikilink")，不途經[美孚](../Page/美孚.md "wikilink")、[深水埗等地](../Page/深水埗.md "wikilink")。

2015年6月6日起，配合洪福邨巴士總站啟用後，實行68X線第二階段重組，改動如下\[6\]：

  - 268X提升為全日服務，全面取代原有68X線[旺角南](../Page/旺角南.md "wikilink")、[油麻地及](../Page/油麻地.md "wikilink")[佐敦來往](../Page/佐敦.md "wikilink")[元朗市及](../Page/元朗市.md "wikilink")[洪水橋之服務](../Page/洪水橋.md "wikilink")
  - 68X縮短路線，九龍區總站縮短至[旺角（柏景灣）](../Page/旺角（柏景灣）巴士總站.md "wikilink")，並加停田心路及田廈路，以及逢星期一至六早上繁忙時間增設兩班由元朗（西）開往旺角（柏景灣）的晨早特別班次。
  - 63X、68X及268X於同日起遷入[洪福邨巴士總站](../Page/洪福邨公共運輸交匯處.md "wikilink")。

2015年8月1日起實施以下改動：

  - 268X取消元朗（西）開出的特別班次，並加密平日早上繁忙時間洪水橋（洪福邨）開出的班次\[7\]\[8\]；
  - 268X往洪水橋方向於[彌敦道近奶路臣街加設中途站](../Page/彌敦道.md "wikilink")\[9\]。
  - 2018年9月16日：268X線九龍區總站由[佐敦（渡華路）巴士總站遷往](../Page/佐敦（渡華路）巴士總站.md "wikilink")[西九龍站巴士總站](../Page/西九龍站巴士總站.md "wikilink")\[10\]。
  - 2018年9月29日：68X線往洪水橋方向實施以下改動：\[11\]
      - 於[櫻桃街後](../Page/櫻桃街.md "wikilink")，改經[大角咀道](../Page/大角咀道.md "wikilink")、櫻桃街、[櫸樹街及](../Page/櫸樹街.md "wikilink")[晏架街返回旺角道原有路線](../Page/晏架街.md "wikilink")：
          - 取消櫻桃街「[銘基書院](../Page/銘基書院.md "wikilink")」及新填地街「[新填地街](../Page/新填地街.md "wikilink")」站，並新增大角咀道「[富貴街](../Page/富貴街.md "wikilink")」、[櫸樹街](../Page/櫸樹街.md "wikilink")「櫸樹街」及旺角道「[旺角道](../Page/旺角道.md "wikilink")」站；
      - 增設青山公路 – 洪水橋段「[紫翠花園](../Page/紫翠花園.md "wikilink")」站。

## 服務時間及班次

  - 68X常規班次

| [洪水橋](../Page/洪水橋.md "wikilink")（[洪福邨](../Page/洪福邨.md "wikilink")）開 | [旺角](../Page/旺角.md "wikilink")（[柏景灣](../Page/柏景灣.md "wikilink")）開 |
| ------------------------------------------------------------------- | ----------------------------------------------------------------- |
| **日期**                                                              | **服務時間**                                                          |
| 星期一至五                                                               | 05:25-06:25                                                       |
| 06:25-18:45                                                         | 10-15                                                             |
| 18:45-00:25                                                         | 20                                                                |
| 星期六                                                                 | 05:25-06:05                                                       |
| 06:05-19:05                                                         | 12-15                                                             |
| 19:05-00:25                                                         | 20                                                                |
| 星期日及公眾假期                                                            | 05:25-08:05                                                       |
| 08:05-19:05                                                         | 12-15                                                             |
| 19:05-00:25                                                         | 20                                                                |
|                                                                     |                                                                   |

  - 68X元朗（西）特別班次

<!-- end list -->

  - 星期一至六：07:40-08:10

<!-- end list -->

  -
    星期日及公眾假期不設服務

<!-- end list -->

  - 268X

| [洪水橋](../Page/洪水橋.md "wikilink")（[洪福邨](../Page/洪福邨.md "wikilink")）開 | [佐敦](../Page/佐敦.md "wikilink")（[西九龍站](../Page/香港西九龍站.md "wikilink")）開 |
| ------------------------------------------------------------------- | --------------------------------------------------------------------- |
| **日期**                                                              | **服務時間**                                                              |
| 星期一至五                                                               | 05:30-06:30                                                           |
| 06:30-07:00                                                         | 15                                                                    |
| 07:00-08:00                                                         | 10                                                                    |
| 08:00-18:00                                                         | 13-15                                                                 |
| 18:00-00:30                                                         | 20-30                                                                 |
| 星期六                                                                 | 05:30                                                                 |
| 07:00-19:00                                                         | 12-20                                                                 |
| 19:00-23:00                                                         | 20                                                                    |
| 23:00-00:30                                                         | 30                                                                    |
| 星期日及公眾假期                                                            | 05:30                                                                 |
| 06:00-09:00                                                         | 20                                                                    |
| 09:00-19:00                                                         | 12-15                                                                 |
| 19:00-23:00                                                         | 20                                                                    |
| 23:00-00:30                                                         | 30                                                                    |

## 收費

全程：$14.2

  - 美孚（68X）往九龍：$6.2
  - 彌敦道（68X）／荔枝角道（268X）往九龍：$5.1
  - 大欖隧道轉車站往洪水橋（洪福邨）：$10.1
  - 過博愛交匯處後往洪水橋（洪福邨）：$4.3

## 使用車輛

由於客量一直都維持高水平，九巴亦相當重視這條皇牌路線，一直以來都是使用最新的車型，甫開辦時仍有[利蘭勝利二型及](../Page/利蘭勝利二型.md "wikilink")[都城嘉慕威曼都城巴士行走](../Page/都城嘉慕威曼都城巴士.md "wikilink")，但很快便採用一代車皇1985年[平治O305巴士](../Page/平治O305巴士.md "wikilink")，後來更成為本線在[屯門公路往返](../Page/屯門公路.md "wikilink")[元朗的標誌](../Page/元朗.md "wikilink")。

1987年，九巴試驗雙層空調巴士時，首輛[MCW雙層空調巴士](../Page/都城嘉慕威曼都城巴士.md "wikilink")（DP1932）獲派行走本線試驗，但行走首天便無法繼續行走，最後DP1932被拆去空調系統，變回普通巴士（車隊編號為S3M145）。

經過一輪試驗後，雙層空調巴士正式出爐，本線於1990年12月1日起，增派6輛11米[利蘭奧林比安雙層空調巴士行走](../Page/利蘭奧林比安.md "wikilink")，是九巴第2條、也是新界區首條行走雙層空調巴士路線，而且空調巴士有其獨立行車班次，由於當時空調巴士不像現時普及，車費達到$12，與普通巴士的車費$6.9相差接近75%，自然不太受歡迎，認為是上等人士的專利品。

本線搬遷洪水橋巴士總站前，主要採用[富豪超級奧林比安](../Page/富豪超級奧林比安.md "wikilink")12米及配電子路線牌箱的都普車身丹尼士三叉戟巴士。於2003年加入[亞歷山大丹尼士Enviro
500行駛](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")。2007年6月24日，本線搬遷至洪水橋後，因為洪水橋巴士總站的調頭位問題，所以原先行駛本線的富豪超級奧林比安12米巴士被迫「全數撤離」，換入前龍運巴士的1998年[丹尼士三叉戟及配電子路線牌箱的](../Page/丹尼士三叉戟三型.md "wikilink")2000年都普車身丹尼士三叉戟巴士。

2008年5月26日，因應駛經彌敦道的巴士路線的低地台掛牌車必須是歐盟3型或以上引擎排放的巴士，所以原有的6輛歐盟2型引擎巴士被換走，由於洪水橋總站的調頭位問題，因此換入了4輛原本行駛[260X線及](../Page/九龍巴士260X線.md "wikilink")2輛原本行駛[269D線的](../Page/九龍巴士269D線.md "wikilink")2002年丹尼士三叉戟巴士。

2013年2月7日，為紀念香港漫畫《[老夫子](../Page/老夫子.md "wikilink")》出版50週年，塗上「老夫子50周年」紀念巴士廣告的歐盟四型引擎[亞歷山大丹尼士Enviro
500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")
12米（ATEU28／PV7695）被調入本路線，該廣告特別之處為採用九巴一代車皇「平治」O305（ME）作車身色彩，向該款曾經服務本路線多年的巴士車款致意。

現時68X、268X兩線使用31輛[亞歷山大丹尼士Enviro 500
MMC和](../Page/亞歷山大丹尼士Enviro_500_MMC.md "wikilink")1輛[富豪B8L巴士](../Page/富豪B8L.md "wikilink")\[12\]
(編制上以68X作為正線，因此兩線以68X名義派車，情況同373、673與978及290系。

正常兩線用車交替行走，直至收車為止。

## 行車路線

  - 68X

**洪水橋（洪福邨）開**經：[洪水橋田心路](../Page/洪水橋田心路.md "wikilink")、[田廈路](../Page/田廈路.md "wikilink")、[青山公路](../Page/青山公路.md "wikilink")（洪水橋、屏山、元朗段）、[朗日路](../Page/朗日路.md "wikilink")、[青山公路—元朗段](../Page/青山公路—元朗段.md "wikilink")、[博愛交匯處](../Page/博愛交匯處.md "wikilink")、[青朗公路](../Page/青朗公路.md "wikilink")、[大欖隧道](../Page/大欖隧道.md "wikilink")、[屯門公路](../Page/屯門公路.md "wikilink")、[荃灣路](../Page/荃灣路.md "wikilink")、[葵涌道](../Page/葵涌道.md "wikilink")、[長沙灣道](../Page/長沙灣道.md "wikilink")、[彌敦道](../Page/彌敦道.md "wikilink")、[旺角道](../Page/旺角道.md "wikilink")、[西洋菜南街](../Page/西洋菜南街.md "wikilink")、[亞皆老街](../Page/亞皆老街.md "wikilink")、[櫻桃街及](../Page/櫻桃街.md "wikilink")[海泓道](../Page/海泓道.md "wikilink")。

**旺角（柏景灣）開**經：海泓道、櫻桃街、[大角咀道](../Page/大角咀道.md "wikilink")、櫻桃街、[櫸樹街](../Page/櫸樹街.md "wikilink")、[晏架街](../Page/晏架街.md "wikilink")、旺角道、彌敦道、[荔枝角道](../Page/荔枝角道.md "wikilink")、[黃竹街](../Page/黃竹街.md "wikilink")、長沙灣道、荔枝角道、葵涌道、荃灣路、屯門公路、大欖隧道、青朗公路、博愛交匯處、青山公路（元朗、屏山、洪水橋段）、田廈路及洪水橋田心路。

  - 268X

**洪水橋（洪福邨）開**經：洪水橋田心路、洪水橋大街、青山公路（洪水橋、屏山、元朗段）、朗日路、青山公路—元朗段、博愛交匯處、元朗公路、青朗公路、大欖隧道、青朗公路、屯門公路、荃灣路、葵涌道、荔枝角道、[西九龍走廊](../Page/西九龍走廊.md "wikilink")、[太子道西](../Page/太子道西.md "wikilink")、荔枝角道、彌敦道、[佐敦道](../Page/佐敦道.md "wikilink")、迴旋處、佐敦道及[海泓道](../Page/海泓道.md "wikilink")。

**佐敦（西九龍站）開**經：海泓道、佐敦道、彌敦道、亞皆老街、櫻桃街、西九龍走廊西、西九龍走廊、葵涌道、荃灣路、屯門公路、青朗公路、大欖隧道、青朗公路、元朗公路、博愛交匯處、青山公路（元朗、屏山段）、[洪天路及洪水橋田心路](../Page/洪天路.md "wikilink")。

### 沿線車站

  - 68X

[KMB68x_RtMap.png](https://zh.wikipedia.org/wiki/File:KMB68x_RtMap.png "fig:KMB68x_RtMap.png")

| [洪水橋](../Page/洪水橋.md "wikilink")（[洪福邨](../Page/洪福邨.md "wikilink")）開 | [旺角](../Page/旺角.md "wikilink")（[柏景灣](../Page/柏景灣.md "wikilink")）開      |
| ------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| **序號**                                                              | **車站名稱**                                                               |
| 1\*                                                                 | [洪水橋](../Page/洪水橋.md "wikilink")（[洪福邨](../Page/洪福邨.md "wikilink")）巴士總站 |
| 2\*                                                                 | 洪水橋[田心路](../Page/田心路.md "wikilink")                                    |
| 3\*                                                                 | [田心](../Page/田心村_\(洪水橋\).md "wikilink")                                |
| 4\*                                                                 | [鄉事委員會](../Page/廈村鄉事委員會.md "wikilink")                                 |
| 5\*                                                                 | [洪水橋站](../Page/洪水橋站.md "wikilink")                                     |
| 6\*                                                                 | [大道村](../Page/大道村.md "wikilink")                                       |
| 7\*                                                                 | [灰沙圍](../Page/灰沙圍.md "wikilink")                                       |
| 8\*                                                                 | [塘坊村站](../Page/塘坊村站.md "wikilink")                                     |
| 9\*                                                                 | [屏山](../Page/屏山站_\(香港\).md "wikilink")                                 |
| 10\*                                                                | [朗庭園](../Page/朗庭園.md "wikilink")                                       |
| @                                                                   | [元朗](../Page/元朗.md "wikilink")（西）巴士總站                                  |
| 11\*                                                                | [喜利徑](../Page/喜利徑.md "wikilink")                                       |
| 12                                                                  | [同樂街](../Page/同樂街.md "wikilink")                                       |
| 13                                                                  | [谷亭街](../Page/谷亭街.md "wikilink")                                       |
| 14                                                                  | [形點 II](../Page/形點_II.md "wikilink")                                   |
| 15                                                                  | [形點I](../Page/YOHO_MALL_形點#形點I擴展部分.md "wikilink")                      |
| 16                                                                  | 大欖隧道                                                                   |
| 17                                                                  | 葵涌交匯處                                                                  |
| 18                                                                  | 美孚站                                                                    |
| 19                                                                  | [荔枝角站](../Page/荔枝角站.md "wikilink")                                     |
| 20                                                                  | [昌華街](../Page/昌華街.md "wikilink")                                       |
| 21                                                                  | [元州邨](../Page/元州邨.md "wikilink")                                       |
| 22                                                                  | [九江街](../Page/九江街.md "wikilink")                                       |
| 23                                                                  | [北河街](../Page/北河街.md "wikilink")                                       |
| 24                                                                  | [楓樹街](../Page/楓樹街.md "wikilink")                                       |
| 25                                                                  | [旺角警署](../Page/旺角警署.md "wikilink")                                     |
| 26                                                                  | [西洋菜南街](../Page/西洋菜南街.md "wikilink")                                   |
| 27                                                                  | [旺角街市](../Page/旺角街市.md "wikilink")                                     |
| 28                                                                  | [銘基書院](../Page/銘基書院.md "wikilink")                                     |
| 29                                                                  | [海富苑](../Page/海富苑.md "wikilink")                                       |
| 30                                                                  | 旺角（柏景灣）巴士總站                                                            |
|                                                                     | 32                                                                     |
| 33                                                                  | 洪水橋（洪福邨）巴士總站                                                           |

  - 由元朗（西）開出的特別班次不停有 \* 號之車站。
  - 由洪水橋（洪福邨）開出的班次不停有 @ 號之車站。

<!-- end list -->

  - 268X

[KMB268x_RtMap.png](https://zh.wikipedia.org/wiki/File:KMB268x_RtMap.png "fig:KMB268x_RtMap.png")

| 洪水橋（洪福邨）開 | [佐敦](../Page/佐敦.md "wikilink")（[西九龍站](../Page/香港西九龍站.md "wikilink")）開 |
| --------- | --------------------------------------------------------------------- |
| **序號**    | **車站名稱**                                                              |
| 1         | 洪水橋（洪福邨）巴士總站                                                          |
| 2         | 洪水橋站                                                                  |
| 3         | 大道村                                                                   |
| 4         | 灰沙圍                                                                   |
| 5         | 塘坊村站                                                                  |
| 6         | 屏山                                                                    |
| 7         | 朗庭園                                                                   |
| 8         | 喜利徑                                                                   |
| 9         | 同樂街                                                                   |
| 10        | 谷亭街                                                                   |
| 11        | 形點 II                                                                 |
| 12        | 形點 I                                                                  |
| 13        | 大欖隧道                                                                  |
| 14        | [鴉蘭街](../Page/鴉蘭街.md "wikilink")                                      |
| 15        | [旺角站](../Page/旺角站.md "wikilink")                                      |
| 16        | [登打士街](../Page/登打士街.md "wikilink")                                    |
| 17        | [加士居道](../Page/加士居道.md "wikilink")                                    |
| 18        | [吳松街](../Page/吳松街.md "wikilink")                                      |
| 19        | [炮台街](../Page/炮台街.md "wikilink")                                      |
| 20        | 佐敦（西九龍站）巴士總站                                                          |
|           | 21                                                                    |
| 22        | 大道村                                                                   |
| 23        | 洪水橋（洪福邨）巴士總站                                                          |

## 客量

由於本線能直接往西九龍心臟地帶，一直深受乘客歡迎，成為元朗區皇牌路線之一，整個元朗市中心來往西九龍的乘客已被本線吸納，甚至在屯門市中心一帶也有不少乘客上車，因此68號線在九龍的客量降低。1998年5月26日，由於大欖隧道啟用，68號線永久停駛，本線成為元朗市中心至西九龍的唯一巴士線；同一天，本線永久不再駛經屯門。

雖然2003年底九廣西鐵（今[西鐵綫](../Page/西鐵綫.md "wikilink")）通車，但由於西鐵車站和[洪水橋及](../Page/洪水橋.md "wikilink")[西九龍鬧市的距離頗遠](../Page/西九龍.md "wikilink")，要多重轉車，加上轉車乘客在本線的客源中佔一定比例（尤其是回程，由西九龍開往天水圍的九巴路線如69X、265B，比本線貴幾角），對本線的影響不大。此路線在任何時間客量皆不俗，非繁忙時間亦不時出現滿座情況。但由於經過市區的繁忙地帶，所以偶爾會出現脫班的情況\[13\]。

而本線的服務時間亦相當長，九龍開出之尾班車時間分別為01:00(268X)及01:05(68X)，比西鐵線尾班列車到達時間00:29為晚，為美孚、深水埗、旺角及佐敦等地的乘客提供比通宵路線更便利的選擇。

## 相關事件

2006年4月27日，轟動一時的巴士阿叔事件發生於68X線的一輛富豪超級奧林比安（3ASV）巴士上，當時正由佐敦（匯翔道）前往元朗（東）。一名青年不滿坐在前排的中年男子電話交談聲量過大，輕拍對方肩膀，勸他降低聲量，但中年漢反應激烈，高聲斥罵少年。

## 參考資料

<references />

  -
  - 《巴士路線發展綱要(2)──荃灣．屯門．元朗》，ISBN 9628414720003，BSI（香港），159頁

  - 《今日九巴》第244期，第17頁「元朗特快線268X起動」

## 外部連結

  - [九巴68X線官方網站路線資料](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=68X)
  - [YouTube：九巴268X線全程行車片段（往元朗）](https://www.youtube.com/watch?v=IdOPpquoUFI)
  - [YouTube：九巴268X線全程行車片段（洪水橋往佐敦）](https://www.youtube.com/watch?v=QsBZrwGaR4I)

[068X](../Category/九龍巴士路線.md "wikilink")
[068X](../Category/香港九龍巴士特快巴士路線.md "wikilink")
[068X](../Category/元朗區巴士路線.md "wikilink")
[068X](../Category/油尖旺區巴士路線.md "wikilink")

1.  [九龍巴士](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=68X)
2.  [新浪網](http://news.sina.com.hk/cgi-bin/news/show_news.cgi?ct=headlines&type=hongkong&date=2006-05-26&id=1989845)
3.
4.  據《二十世紀新界（西）區巴士路線發展史》，在此以前，本線曾搬遷元朗區總站一次，就是在1988年8月3日從[交通廣場附近的](../Page/交通廣場.md "wikilink")[鳳翔路前元朗](../Page/鳳翔路.md "wikilink")（東）巴士總站遷往[輕鐵](../Page/香港輕鐵.md "wikilink")[元朗站旁](../Page/元朗站_\(輕鐵\).md "wikilink")，[新元朗中心外的巴士總站](../Page/新元朗中心.md "wikilink")。
5.  [681巴士總站](http://www.681busterminal.com/68x.html)
6.  [九巴68X及268X線宣傳單張](http://www.kmb.hk/chi/pdf/route_68X_268X_Leaflet.pdf)
7.  [九巴乘客通告](http://www.kmb.hk/loadImage.php?page=1437550984_2579_0.jpg)
8.  [九巴乘客通告](http://www.kmb.hk/loadImage.php?page=1437551218_2044_0.jpg)
9.  [九巴乘客通告](http://www.kmb.hk/loadImage.php?page=1437545755_4050_0.jpg)
10. [九巴乘客通告](http://search.kmb.hk/KMBWebSite/AnnouncementPicture.ashx?url=1536650321_4148_0.pdf)
11. 九龍巴士（一九三三）有限公司，〈[68X
    路線永久改道及巴士站重新安排](http://search.kmb.hk/KMBWebSite/AnnouncementPicture.ashx?url=1537929947_3569_0.pdf)〉［乘客通告］，2018年9月。
12. [1005網站](http://1005.idv.hk/index.php?page=01&search=sch&word=68X)
13.