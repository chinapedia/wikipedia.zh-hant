**宣化区**是[中国](../Page/中华人民共和国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[张家口市下辖的一个](../Page/张家口市.md "wikilink")[区](../Page/市辖区.md "wikilink")。位于北纬40°37′，东经115°03′，东南近临首都北京150公里，西连晋蒙。2016年，原[宣化县撤销](../Page/宣化县.md "wikilink")，并入宣化区。

## 行政区划

下辖7个[街道办事处](../Page/街道办事处.md "wikilink")、7个[镇](../Page/行政建制镇.md "wikilink")、7个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 外部链接

  - [中国张家口宣化区](http://www.zjkxuanhua.gov.cn/)

## 参考文献

[宣化区](../Page/category:宣化区.md "wikilink")
[区](../Page/category:张家口区县.md "wikilink")
[张家口](../Page/category:河北市辖区.md "wikilink")