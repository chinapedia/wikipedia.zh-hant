**国立师范学院**是现在[湖南师范大学的前身](../Page/湖南师范大学.md "wikilink")。建立于1938年7月，地点是[湖南省](../Page/湖南省_\(中華民國\).md "wikilink")[安化县蓝田镇](../Page/安化县.md "wikilink")（今属[湖南省](../Page/湖南省.md "wikilink")[涟源市](../Page/涟源市.md "wikilink")[蓝田街道](../Page/蓝田街道.md "wikilink")），[廖世承为首任院长](../Page/廖世承.md "wikilink")。

国立师范学院当时直接隶属[国民政府](../Page/国民政府.md "wikilink")[教育部](../Page/中華民國教育部.md "wikilink")，行政组织有秘书室、教务处、训导处、总务处、会计室、教育资料室等。廖世承选择蓝田镇，是因其“既偏僻又交通便利”，“偏僻”指远离[京广线](../Page/京广线.md "wikilink")，避免[日军干扰](../Page/日军.md "wikilink")，同时有铁路、公路和涟水水运。最初的校本部“李园”，由[李燮和之子李卓然租借](../Page/李燮和.md "wikilink")，不久又新建校舍，为第二院。所有建筑，现均不存。李园原址现为涟源市人民政府所在，第二院原址为涟源一中\[1\]。

学院建立之初设有[国文](../Page/国文.md "wikilink")、[英语](../Page/英语.md "wikilink")、[教育](../Page/教育.md "wikilink")、史地、[数学](../Page/数学.md "wikilink")、理化及公民训育七个系，后又建立若干专修科和训练班，至1943年，全院共有7系4科3班；院本部有学生770余人。曾在学院任教的知名学者有[钱基博](../Page/钱基博.md "wikilink")、[钱钟书](../Page/钱钟书.md "wikilink")、[储安平等](../Page/储安平.md "wikilink")。

1944年春，由于[豫湘桂会战](../Page/豫湘桂会战.md "wikilink")，日軍攻陷[长沙和](../Page/长沙.md "wikilink")[衡阳](../Page/衡阳.md "wikilink")，学院西迁至湖南[溆浦](../Page/溆浦.md "wikilink")。校址在马田坪，并借用[川军将领](../Page/川军.md "wikilink")[陈遐龄的庄园](../Page/陈遐龄.md "wikilink")\[2\]。1945年秋，[日本投降](../Page/日本投降.md "wikilink")，学院在湖南[衡山复校](../Page/衡山.md "wikilink")。校址在[南岳大庙一带](../Page/南岳大庙.md "wikilink")，校本部即今[黄庭观](../Page/黄庭观.md "wikilink")\[3\]。1949年10月，[解放军已经攻占湖南全省](../Page/中国人民解放军.md "wikilink")。11月，学院被[中华人民共和国政府接管](../Page/中华人民共和国政府.md "wikilink")，与其它诸校一同并入[湖南大学](../Page/湖南大学.md "wikilink")。[1953年院系调整](../Page/中国高等院校院系调整.md "wikilink")，湖南大学撤销，以国立师范学院为基础组建[湖南师范学院](../Page/湖南师范学院.md "wikilink")。

## 参见

  - [湖南师范学院](../Page/湖南师范学院.md "wikilink")

## 注释

[湘](../Category/中華民國已不存在的公立大學.md "wikilink")
[Category:中华民国湖南省](../Category/中华民国湖南省.md "wikilink")
[Category:湖南高等教育史](../Category/湖南高等教育史.md "wikilink")
[Category:湖南师范大学](../Category/湖南师范大学.md "wikilink")
[Category:湖南高等院校](../Category/湖南高等院校.md "wikilink")
[Category:娄底教育史](../Category/娄底教育史.md "wikilink")
[Category:怀化教育史](../Category/怀化教育史.md "wikilink")
[Category:衡阳教育史](../Category/衡阳教育史.md "wikilink")
[Category:1938年创建的教育机构](../Category/1938年创建的教育机构.md "wikilink")
[Category:1949年廢除](../Category/1949年廢除.md "wikilink")

1.

2.
3.