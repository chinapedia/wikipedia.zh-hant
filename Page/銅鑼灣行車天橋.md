[HK_CWB_2_Tai_Hang_Elevated_Road.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_2_Tai_Hang_Elevated_Road.JPG "fig:HK_CWB_2_Tai_Hang_Elevated_Road.JPG")
[HK_CWB_2_Tai_Hang_Elevated_Road_north.JPG](https://zh.wikipedia.org/wiki/File:HK_CWB_2_Tai_Hang_Elevated_Road_north.JPG "fig:HK_CWB_2_Tai_Hang_Elevated_Road_north.JPG")
[OSM_CWB_flyover.png](https://zh.wikipedia.org/wiki/File:OSM_CWB_flyover.png "fig:OSM_CWB_flyover.png")

**銅鑼灣行車天橋**是[香港一條](../Page/香港.md "wikilink")[行車天橋](../Page/行車天橋.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[銅鑼灣北面近](../Page/銅鑼灣.md "wikilink")[銅鑼灣避風塘南面](../Page/銅鑼灣避風塘.md "wikilink")，連接[維園道及](../Page/維園道.md "wikilink")[告士打道](../Page/告士打道.md "wikilink")，似曲尺形，現時為第二代。

新銅鑼灣行車天橋由2006年10月2日早上（大約8時後）正式全面通車。新銅鑼灣行車天橋是單向兩線行車。在近[香港海底隧道的](../Page/香港海底隧道.md "wikilink")[維園道向東行的駕車人士](../Page/維園道.md "wikilink")，可以駛上此行車天橋，經過[銅鑼灣公園](../Page/銅鑼灣公園.md "wikilink")[東與](../Page/東.md "wikilink")[維多利亞公園](../Page/維多利亞公園.md "wikilink")[西之間](../Page/西.md "wikilink")，而轉回維園道西行方向，又或者分支到[告士打道向](../Page/告士打道.md "wikilink")[大坑方向](../Page/大坑.md "wikilink")。

## 外部連結

  - [新銅鑼灣行車天橋（運輸署的官方地圖）](http://www.td.gov.hk/FileManager/EN/UTIL_UTRAFFIC_WF/TOHK/drawings_for_full_opening.pdf)
  - [新銅鑼灣行車天橋（運輸署的官方通告）](http://www.td.gov.hk/traffic_notices/index_tc_inmode_3_trafficid_5991.htm)
  - [明報：新銅鑼灣行車天橋](https://web.archive.org/web/20070518064003/http://hk.news.yahoo.com/060930/12/1txd6.html)
  - [新銅鑼灣行車天橋（中原地圖）](http://www.centamap.com/scripts/centamap2.asp?lg=B5&cx=837113&cy=815881&zm=3&mx=837113&my=816027&ms=2&sx=&sy=&ss=0&sl=&vm=&lb=&ly=)
  - [銅鑼灣行車天橋落斜位現車禍陷阱](https://web.archive.org/web/20061109130756/http://hk.news.yahoo.com/061002/10/1tzcy.html)

[Category:銅鑼灣](../Category/銅鑼灣.md "wikilink")
[Category:香港行車天橋](../Category/香港行車天橋.md "wikilink")