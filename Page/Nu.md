**Nu**或**NU**可能是指下列事物：

## 語言

  - nu，希臘字母[Ν的英文名稱](../Page/Ν.md "wikilink")。
  - nu，日語假名[ぬ的羅馬字](../Page/ぬ.md "wikilink")。

## 大學

  - [納扎爾巴耶夫大學](../Page/納扎爾巴耶夫大學.md "wikilink")（Nazarbayev
    University），位於哈薩克斯坦首都阿斯塔納的一所大學。
  - [尼亞加拉大學](../Page/尼亞加拉大學.md "wikilink")（Niagara
    University），位於美國紐約州尼亞加拉郡的一所天主教大學。
  - [東北大學 (美國)](../Page/東北大學_\(美國\).md "wikilink")（Northeastern
    University），位於美國麻薩諸塞州波士頓市中心的一所大學。
  - [西北大學 (華盛頓州)](../Page/西北大學_\(華盛頓州\).md "wikilink")（Northwest
    University），位於美國華盛頓州柯克蘭的一所私人大學。
  - [西北大學 (伊利諾州)](../Page/西北大學_\(伊利諾州\).md "wikilink")（Northwestern
    University），位於美國伊利諾州芝加哥近郊埃文斯頓市的一所大學。
  - [諾威治大學](../Page/諾威治大學.md "wikilink")（Norwich
    University），位於美國佛蒙特州諾斯菲爾德，是美國最古老的私立軍事院校。
  - [內布拉斯加大學林肯分校](../Page/內布拉斯加大學林肯分校.md "wikilink")（University of
    Nebraska–Lincoln），位於美國內布拉斯加州林肯市的一所大學。
  - [名古屋大學](../Page/名古屋大學.md "wikilink")（Nagoya
    University），位於日本名古屋的一所大學，舊[帝國大學之一](../Page/帝國大學.md "wikilink")。
  - [南京大學](../Page/南京大學.md "wikilink")（Nanjing
    University），位於中國江蘇省南京市的一所大學。
  - [那黎宣大学](../Page/那黎宣大学.md "wikilink")（Naresuan
    University），位於泰國彭世洛的一所公立大學。
  - [菲律賓國民大學](../Page/菲律賓國民大學.md "wikilink")（National
    University），位於菲律賓首都馬尼拉，是一所私立的無宗派大學。
      - [NU鬥牛犬](../Page/NU鬥牛犬.md "wikilink")（NU
        Bulldogs），該校在菲律賓大學運動協會的男子代表隊。
  - [尼羅大學](../Page/尼羅大學.md "wikilink")（Nile
    University），位於埃及吉薩省十月六日城的一所私立大學。
  - [尼爾瑪大學](../Page/尼爾瑪大學.md "wikilink")（Nirma
    University），位於印度古吉拉特邦阿美達巴德的一所私立大學。
  - [Nkumba大學](../Page/Nkumba大學.md "wikilink")（Nkumba
    University），位於烏干達恩德培的一所私立大學。

## 其他縮寫

  - [北方橄欖球聯合會](../Page/北方橄欖球聯合會.md "wikilink")（Northern
    Union，NU），橄欖球聯盟的前稱。
  - [伊斯蘭教士聯合會](../Page/伊斯蘭教士聯合會.md "wikilink")（Nahdlatul
    Ulama，NU），印尼的一個伊斯蘭群眾組織。
  - [努塞尔数](../Page/努塞尔数.md "wikilink")（Nusselt number，Nu），一個無量綱的熱傳導比值。
  - [泊松比](../Page/泊松比.md "wikilink")，定義為材料變形時其橫向變形量與縱向變形量的比值，是一個無量綱量。一般以希臘字母ν（）記之。
  - [珠心胚现象](../Page/珠心胚现象.md "wikilink")（Nucellar
    embryony，Nu+），在一些植物物種出現的種子繁殖形式。
  - [不丹努尔特鲁姆](../Page/不丹努尔特鲁姆.md "wikilink")（Nu.），不丹的貨幣。
  - [東北公用事業](../Page/東北公用事業.md "wikilink")（Northeast
    Utilities，NU），美國東北部的一家電力和天然氣公司。現改名為能源方案。
  - [0号元素](../Page/0号元素.md "wikilink")，指原子中僅含中子，不含質子的一種元素，或純粹只由中子組成的物質。
  - [诺顿电脑医生](../Page/诺顿电脑医生.md "wikilink")（Norton Utilities）

## 代號

  - [尼加拉瓜的NATO國家代碼](../Page/尼加拉瓜.md "wikilink")
  - [紐埃的ISO](../Page/紐埃.md "wikilink") 3166國家代碼
      - [.nu](../Page/.nu.md "wikilink")，紐埃的國碼頂級網域名稱
  - 加拿大[努纳武特](../Page/努纳武特.md "wikilink")（Nunavut）的[邮政简称](../Page/加拿大各省和地区邮政简称.md "wikilink")
  - [日本越洋航空的IATA航空公司代碼](../Page/日本越洋航空.md "wikilink")

## 音樂

  - [新金屬](../Page/新金屬.md "wikilink")（Nu Metal），重金屬音樂的一個融合類型。
  - [新爵士](../Page/新爵士.md "wikilink")（Nu Jazz），爵士音樂的一個融合類型。
  - [新迪斯可](../Page/新迪斯可.md "wikilink")（Nu-disco），一個舞曲類型。
  - [新民謠](../Page/新民謠.md "wikilink")（Nu folk），民謠音樂的一個融合類型。
  - [新瞪鞋](../Page/新瞪鞋.md "wikilink")（Nu gaze），瞪鞋音樂的一個融合類型。
  - [Nu prog](../Page/Nu_prog.md "wikilink")，前衛搖滾的一個子類型。

## 緬甸人

努（，）是一個常見的緬甸人名，另外可以指：

  - [鄂努](../Page/鄂努.md "wikilink")，阿瓦王朝（1367）的王位覬覦者。

<!-- end list -->

  - [米努](../Page/米努.md "wikilink")，國王巴基道（1819－1837年間在位）的王后。
  - [吴努](../Page/吴努.md "wikilink")，緬甸聯邦總理（1948－1958；1960－1962）。其中「吳」為敬稱。

## 其他

  - [努恩 (古埃及)](../Page/努恩_\(古埃及\).md "wikilink")，埃及神話中原始水和混沌之神。
  - 電子遊戲[时空之轮中的一種虛構生物](../Page/时空之轮.md "wikilink")
  - [N.U.
    (電影)](../Page/N.U._\(電影\).md "wikilink")，米開朗基羅·安東尼奧尼於1948年導演的一部記錄片。
  - [怒族](../Page/怒族.md "wikilink")，中華人民共和國的一個民族。
  - [怒江](../Page/怒江.md "wikilink")，薩爾溫江上游中國境內河段的名稱。
  - Mr. Nu，小說[殺手：內部敵人中的一個虛構角色](../Page/殺手：內部敵人.md "wikilink")。
  - [寧語](../Page/寧語.md "wikilink")（Nǀu language）
  - [NuMachine](../Page/NuMachine.md "wikilink")，麻省理工學院於1970年代末開發的電腦系統結構，包括了後來的[NuBus](../Page/NuBus.md "wikilink")。
  - [Nu (程式語言)](../Page/Nu_\(程式語言\).md "wikilink")，一門解釋型的面向對像語言。
  - 動畫電影[機動戰士高達
    馬沙之反擊中的](../Page/機動戰士高達_馬沙之反擊.md "wikilink")[RX-93系列機動戰士](../Page/RX-93系列機動戰士.md "wikilink")
  - [弩](../Page/弩.md "wikilink")，一種射擊兵器。