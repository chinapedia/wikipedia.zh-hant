**艾倫·約翰·珀西瓦爾·泰勒**（
），20世紀最著名和最具爭議性的[英國](../Page/英國.md "wikilink")[歷史學家之一](../Page/歷史學家.md "wikilink")。他在1961年發行的著作，《[第二次世界大戰的起源](../Page/第二次世界大戰的起源.md "wikilink")》一書，至今仍引起不少爭論。

## 生平與著述

### 早年生涯

泰勒在1906年3月25日生於[紹斯波特](../Page/紹斯波特.md "wikilink")[百特戴爾](../Page/百特戴爾.md "wikilink")（Birkdale），在[蘭開夏郡渡過童年](../Page/蘭開夏郡.md "wikilink")，並曾在不同的[貴格會教會學校讀書](../Page/貴格會.md "wikilink")，而其中一所是位於[約克的](../Page/約克.md "wikilink")[布特罕學校](../Page/布特罕學校.md "wikilink")。據聞泰勒還是學生的時候，校長已說他聰明得來，卻又十分反叛。泰勒最初的興趣是[考古學](../Page/考古學.md "wikilink")，年輕的時候曾經是業餘考古學家，對英格蘭北部的教會歷史很有研究。正因為對考古學懷有興趣，泰勒漸漸也對[歷史產生了極濃厚的興趣](../Page/歷史.md "wikilink")。在1924年，他入讀[牛津大學](../Page/牛津大學.md "wikilink")[奧里爾學院](../Page/牛津大學奧里爾學院.md "wikilink")，主修現代歷史。

泰勒生於一個富裕的家庭，但父母都是[左翼的忠實支持者](../Page/左翼.md "wikilink")，而泰勒也繼承了父母的政見。作為和平主義者，泰勒的父母強烈反對[第一次世界大戰](../Page/第一次世界大戰.md "wikilink")，所以才會把他們的兒子送到[貴格會的學校作為](../Page/貴格會.md "wikilink")[反戰的一種表示](../Page/反戰.md "wikilink")。

泰勒的母親，[康斯坦斯·泰勒](../Page/康斯坦斯·泰勒.md "wikilink")（Constance
Taylor）在1920年代是[第三國際的成員](../Page/第三國際.md "wikilink")，而泰勒的其中一位舅父更是[英國共產黨創黨成員](../Page/英國共產黨.md "wikilink")。此外，泰勒的母親也是一位[女性主義者](../Page/女性主義.md "wikilink")，提倡女性應該享有[投票權](../Page/投票權.md "wikilink")，以及支持[自由戀愛](../Page/自由戀愛.md "wikilink")，並與泰勒的父親離婚，另結新歡。泰勒本身自1924年至1926年也是英國共產黨的黨員，但後來認為該黨在[1926年大罷工缺乏給予支持而退黨](../Page/英國1926年大罷工.md "wikilink")。至於退黨後，泰勒則轉而終身支持[英國工黨](../Page/英國工黨.md "wikilink")。泰勒雖然退出共產黨，但他仍分別在1925年和1934年到訪[蘇聯](../Page/蘇聯.md "wikilink")，並對兩次的行程留下了深刻的印象。另外，在1930年代的一段時間，泰勒與他的妻子曾住在[作家](../Page/作家.md "wikilink")[馬爾科姆·馬格里奇夫婦的居所](../Page/馬爾科姆·馬格里奇.md "wikilink")，泰勒和馬格里奇後來因為對蘇聯看法不一，而陷入了畢生的論戰之中，但論戰終究也沒有破壞兩人的友誼。

在1927年，泰勒從牛津大學畢業。畢業後曾短暫地在某律師行任職文員，未幾即前往[維也納](../Page/維也納.md "wikilink")，研究[人民憲章運動對維也納](../Page/人民憲章運動.md "wikilink")[1848年革命的影響](../Page/1848年革命.md "wikilink")，但是後來發現該題目難以研究，轉而研究[意大利的統一](../Page/意大利.md "wikilink")。經過了兩年的研究，他最終在1934年寫成他的第一本著作，《歐洲外交中的意大利問題，1847－49年》（*The
Italian Problem in European Diplomacy,
1847–49*）。在這個時期，[奧地利裔歷史學家](../Page/奧地利.md "wikilink")[艾爾弗雷德·法蘭克·普日布拉姆](../Page/艾爾弗雷德·法蘭克·普日布拉姆.md "wikilink")（Alfred
Francis
Pribram）和波蘭裔歷史學家[路易斯·伯恩斯坦·納米爾](../Page/路易斯·伯恩斯坦·納米爾.md "wikilink")[爵士](../Page/爵士.md "wikilink")（Sir
Lewis Bernstein Namier）都是他的導師。

在[奧匈帝國的歷史議題上](../Page/奧匈帝國.md "wikilink")，泰勒曾反對普日布拉姆和納米爾爵士兩人的觀點，直到泰勒在1941年發行《哈布斯堡王朝
1809－1918年》（*The Habsburg Monarchy
1809–1918*）一書，他的觀點才有所改變，而該書後來又在1948年發行了修訂版。在[哈布斯堡王朝的評價上](../Page/哈布斯堡王朝.md "wikilink")，泰勒在早年的著述中，觀點較接近於普日布拉姆，但他晚年卻又認同納米爾爵士，對哈布斯堡王朝的評價變得較負面。在《哈布斯堡王朝》一書中，泰勒指出哈布斯堡王朝完全視他們的王國為外交政策的工具，因此他們無法真正建立一個民族國家。為了使王國結合在一起，他們對奧匈帝國境內各民族進行分化，並賦予[德意志人和](../Page/德意志人.md "wikilink")[匈牙利人對其他民族擁有支配權](../Page/匈牙利人.md "wikilink")。

泰勒返國後在[曼徹斯特大學擔任歷史講師](../Page/曼徹斯特維多利亞大學.md "wikilink")，後自1938年至1964年在[牛津大學莫德林學院擔任研究員](../Page/牛津大學莫德林學院.md "wikilink")。在1964年，[牛津大學拒絕與他續約](../Page/牛津大學.md "wikilink")，他遂前往[倫敦](../Page/倫敦.md "wikilink")，成為[倫敦大學學院](../Page/倫敦大學學院.md "wikilink")[歷史研究所和](../Page/倫敦大學歷史研究所_\(倫敦大學學院\).md "wikilink")[北倫敦理工學院的講師](../Page/北倫敦大學.md "wikilink")。在牛津大學的時候，泰勒曾經是十分受歡迎的講師，由於他的學生太多，他甚至要在上午8時30分便開始授課。

在1930年代初期，泰勒曾經是[曼徹斯特和平協會的成員](../Page/曼徹斯特和平協會.md "wikilink")，而該會是一個支持[和平主義的](../Page/和平主義.md "wikilink")[左翼組織](../Page/左翼.md "wikilink")。一直至1936年以前，泰勒都反對英國重整軍備，擔心英國[重整軍備會使英國與](../Page/重整軍備.md "wikilink")[德國結盟](../Page/德國.md "wikilink")，繼而不利蘇聯。但到了1936年以後，泰勒轉而強烈抨擊[綏靖政策](../Page/綏靖政策.md "wikilink")。同年，他亦退出了曼徹斯特和平協會，促請英國重整軍備，建立英蘇同盟，以防範他所認為的德國的威脅。1938年，英國和[希特勒簽訂了](../Page/希特勒.md "wikilink")《[慕尼黑協定](../Page/慕尼黑協定.md "wikilink")》，泰勒隨即在不同公開場合對《協定》加以抨擊和譴責。但是到了1961年，他又對英國當年的綏靖政策作出迥護，並指出因為簽訂了《協定》，[捷克的死亡人數才比](../Page/捷克.md "wikilink")[波蘭要少](../Page/波蘭.md "wikilink")。

在1938年10月，泰勒出席了一個晚餐會，該晚餐會乃為紀念一群牛津大學的導師在1688年[光榮革命前夕反抗](../Page/光榮革命.md "wikilink")[詹姆斯二世的統治](../Page/詹姆斯二世_\(英國\).md "wikilink")，而在以後每年的10月舉行。泰勒除了再次在會上抨擊《協定》和支持《協定》的人外，又警告坐上客，若果不立即行動反抗納粹，他們將來的生活會比詹姆斯二世的統治更慘。他的言論結果引起不少爭議。這是因為《協定》在當時十分受大眾歡迎。而且該晚餐會的出席人士一向都只會發表非政黨性和政治性的言論，但泰勒卻借了晚餐會來抨擊政府政策。

### 二次世界大戰與冷戰

在[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，泰勒加入了[英國民團](../Page/英國民團.md "wikilink")，由此結識不少流亡自[東歐的政治家](../Page/東歐.md "wikilink")，而當中更包括了前[匈牙利總統](../Page/匈牙利.md "wikilink")[米哈伊·卡羅伊伯爵和](../Page/米哈伊·卡羅伊.md "wikilink")[捷克總統](../Page/捷克.md "wikilink")[愛德華·貝奈斯博士](../Page/愛德華·貝奈斯.md "wikilink")。從他們的身上，泰勒對東歐局勢有更全面的了解，而因為泰勒與卡羅伊伯爵和貝奈斯博士結成朋友的關係，可能使他在日後對他們的評價更為友善，泰勒後來更把卡羅伊描述得活像一個聖人。此外，泰勒在後來曾自豪地憶述到，是他在背後建議貝奈斯，在大戰後將日爾曼人完全[驅逐出捷克](../Page/二次大戰後對日爾曼人的驅逐.md "wikilink")。

與此同時，泰勒又獲受僱於[政治戰爭行政部](../Page/政治戰爭行政部.md "wikilink")，以專家的身份時常在電台和不同的公眾場合，就[中歐局勢發表言論](../Page/中歐.md "wikilink")。此外，他亦向英國政府遊說，承認[鐵托的遊擊隊為](../Page/鐵托.md "wikilink")[南斯拉夫的合法政府](../Page/南斯拉夫.md "wikilink")。第二次世界大戰使泰勒愈益傾向支持蘇聯，而他更指出[紅軍在摧毀](../Page/苏联红军.md "wikilink")[納粹德國一事上起了十分重要的角色](../Page/納粹德國.md "wikilink")。至於英、蘇兩國在1941年結成盟國，對抗[軸心國以後](../Page/軸心國.md "wikilink")，他又認為這已經達成了他當初所提出的英蘇同盟。但是在1945年以後，泰勒又因為英國選擇美國為最終盟友，放棄與蘇聯的關係而感到十分失望。泰勒又對西方列強在戰後所作的種種決定深感憤怒，他尤其認為美國不應該在戰後建立[西德](../Page/西德.md "wikilink")，他指出這只會為[第四帝國立下基石](../Page/第四帝國.md "wikilink")，最終引發另一次大戰。

終其一生，泰勒縱使對共產主義作出強烈譴責，但他卻對蘇聯的外交政策顯得十分同情。相反地，他卻在1950年代和1960年代批評[美國一手造成](../Page/美國.md "wikilink")[冷戰](../Page/冷戰.md "wikilink")，而且也是[解除核武運動的其中一位領導人物](../Page/解除核武運動.md "wikilink")。雖然泰勒認為[英國在冷戰應保持中立](../Page/英國.md "wikilink")，不過他又同時指出英國自身應與一股強大的勢力結成同盟，至於最理想的結盟對象會是蘇聯，而非美國。放棄美國，是因為泰勒相信，美國實施的對外政策輕率魯莽，最終有機會使世界捲入[第三次世界大戰](../Page/第三次世界大戰.md "wikilink")。儘管如此，泰勒有時也會對對蘇聯的鐵腕鎮壓政策加以批評。在1948年，他就曾前往[波蘭的](../Page/波蘭.md "wikilink")[弗羅茨瓦夫](../Page/弗羅茨瓦夫.md "wikilink")，出席一個[史達林主義者的文化會議](../Page/史達林主義.md "wikilink")，並在會上與與會者大唱反調，指出除當權者以外，每人也可以有不同的聲音，由於他的演講得到波蘭[電台的直播](../Page/電台.md "wikilink")，又在弗羅茨瓦夫街頭的擴音器進行播放，結果他的演講被廣泛傳開之餘，更出奇地獲得了全場如雷轟動的掌聲。事後不少人認為，泰勒這樣做是為了反駁蘇維埃作家[亞歷山大·法捷耶夫在前一天發表的言論](../Page/亞歷山大·法捷耶夫.md "wikilink")，而該篇言論則指出每人也有義務遵從[史達林的話](../Page/史達林.md "wikilink")。泰勒雖然曾獲無數次的邀請，但他一生都未曾踏足美國。

泰勒專精於[中歐歷史](../Page/中歐歷史.md "wikilink")、[英國歷史和](../Page/英國歷史.md "wikilink")[外交歷史](../Page/外交史.md "wikilink")，當中又以[哈布斯堡王朝和](../Page/哈布斯堡王朝.md "wikilink")[俾斯麥的研究最為深入](../Page/俾斯麥.md "wikilink")。此外，他又懷有強烈的[反德情緒](../Page/反德情緒.md "wikilink")。在1944年，他就曾因為在電視的講課節目發表強烈的反德言論，結果他的節目被[BBC腰斬了好一段日子](../Page/BBC.md "wikilink")。他在1945年的著作《德國歷史進程》（*The
Course of German
History*）亦爭論到，[民族社會主義最終必然使整個德國歷史回復到](../Page/納粹主義.md "wikilink")[日爾曼部落的原始時期](../Page/日爾曼人.md "wikilink")。另外，泰勒是早期少有以[特殊道路論](../Page/特殊道路論.md "wikilink")（Sonderweg）來解釋德國歷史的學者，特殊道路論指出[納粹德國之所以形成](../Page/納粹德國.md "wikilink")，是德國文化和歷史在過去百多年發展的必然產物。他又認為[希特勒其實與德國人存在一種象徵關係](../Page/希特勒.md "wikilink")，希特勒需要以德國人來滿足其征討的野心，但事實上德國人其實也是借希特勒來達成征服周邊國家的夢想。當中，泰勒又指控日耳曼人早自[查理曼的時期](../Page/查理曼.md "wikilink")，就採取無止境的[東進政策](../Page/東進.md "wikilink")，威脅著[斯拉夫民族](../Page/斯拉夫人.md "wikilink")。而對泰勒而言，納粹激進的帝國主義，亦只不過是歷任日耳曼君主所行政策的延續。《德國歷史進程》一書在[英國和](../Page/英國.md "wikilink")[美國同時都是暢銷書](../Page/美國.md "wikilink")，而這本書亦使泰勒在美國學界立下了一定的地位。除此之外，這本書也標誌著泰勒與他的導師納米爾分道揚鑣，而納米爾亦曾有意寫一本類似的專書。踏入了1950年代，兩人的關係進一步疏淡，泰勒在1983年發行的自傳《一份個人歷史》（*A
Personal History*）雖然指自己從納米爾學到不少東西，但卻以「自負的煩人」來稱呼他。

### 公眾知識份子

泰勒是一位多產的作家，有達廿多本著作，以及數百篇文章和書評。自1931年，他開始為《[曼徹斯特衛報](../Page/衛報.md "wikilink")》撰寫書評。而自1957年開始，他又成為了《[觀察家報](../Page/觀察家報.md "wikilink")》的專欄作家。另外在1963年起，泰勒亦曾在他的朋友兼贊助人，[畢佛布魯克勳爵的](../Page/麥斯·艾特肯，第一代畢佛布魯克男爵.md "wikilink")《[每日快報](../Page/每日快報.md "wikilink")》擔任專欄作家，可是這在畢佛布魯克勳爵於1964年逝世後便宣告終止。在《每日快報》時期，泰勒的第一篇專欄文章就以《為何我們要善待德國人》為標題，大力抨擊戰後大部份德國人的骨子裡仍然支持納粹主義，他又指出[歐洲經濟共同體的成立](../Page/歐洲經濟共同體.md "wikilink")，實際上只是讓德國人取回他們在[一戰和](../Page/第一次世界大戰.md "wikilink")[二戰所爭不到的貿易利益](../Page/第二次世界大戰.md "wikilink")。

從他的著作中，我們發現泰勒常常以「[建制派](../Page/建制派.md "wikilink")」（the
Establishment）來形容英國的精英階層，也使這個詞語漸漸流行起來。據稱，「建制派」一詞是他最先在1953年8月29日，於《新政治家周刊》使用的，當時他就一本關於[威廉·柯貝特的傳記作評論](../Page/威廉·柯貝特.md "wikilink")。他寫到：
作為一位[小英格蘭主義者](../Page/小英格蘭主義者.md "wikilink")，泰勒亦對[大英帝國表示反感](../Page/大英帝國.md "wikilink")，並反對[英國加入](../Page/英國.md "wikilink")[歐洲經濟共同體和](../Page/歐洲經濟共同體.md "wikilink")[北約](../Page/北約.md "wikilink")。此外他更主張英國須撤離[北愛爾蘭](../Page/北愛爾蘭.md "wikilink")。他曾經在1976年於[都柏林發表一篇言論](../Page/都柏林.md "wikilink")，表示[愛爾蘭共和軍](../Page/愛爾蘭共和軍.md "wikilink")（他視之為自由鬥士）應該驅逐北愛爾蘭境內的全數[新教](../Page/新教.md "wikilink")[聯合主義者](../Page/聯合主義者_\(愛爾蘭\).md "wikilink")，就正如捷克在第二次世界大戰後逐出[蘇台德地區的全數日耳曼人一樣](../Page/蘇台德地區.md "wikilink")。

在國際事務上，泰勒反對[西德的存在](../Page/西德.md "wikilink")，認為那裡是危險的[新納粹國家](../Page/新納粹.md "wikilink")。雖然他曾在1956年反對英、法介入[蘇伊士危機](../Page/蘇伊士危機.md "wikilink")，但他又認同蘇聯鎮壓[1956年匈牙利革命](../Page/匈牙利十月事件.md "wikilink")，指出介入成功避免匈牙利重歸[霍爾西的統治](../Page/霍爾西.md "wikilink")。另外，泰勒又對[以色列表示支持](../Page/以色列.md "wikilink")，認為以色列是社會[民主國家的楷模](../Page/民主.md "wikilink")，對抗[阿拉伯的獨裁政權](../Page/阿拉伯.md "wikilink")。至於[韓戰和](../Page/韓戰.md "wikilink")[越戰方面](../Page/越戰.md "wikilink")，泰勒對兩者都予以譴責。在1950年的時候，他曾希望在[BBC發表英國不應加入韓戰的言論](../Page/BBC.md "wikilink")，結果他曾再次被BBC封杀。但由於社會的輿論表示不滿，結果BBC最後還是播出了他的言論。

泰勒時常不囿於時局，對備受非議的人物加以維護。例如美術史學家[安東尼·布倫特](../Page/安東尼·布倫特.md "wikilink")（Anthony
Blunt）在1979年被[戴卓爾夫人揭露曾充當蘇聯間諜後](../Page/戴卓爾夫人.md "wikilink")，泰勒就指出這樣形同[麥卡錫主義](../Page/麥卡錫主義.md "wikilink")，憤而在1980年退出了[英國學院](../Page/英國學院.md "wikilink")。至於在歷史學界方面，泰勒主張提高政府透明度外，又根據他以往遭BBC禁播的經驗，爭取政府讓更多私營電視台存在。

泰勒亦曾多次遊說英國政府，把政府機密檔案的保密期由100年減至20年，最終成功爭取政府檔案在保密30年後，向公眾解密。他認為保密期減至30年，已經算是一大進步。

### 歷史哲學

泰勒之治史方法近於[民粹主義](../Page/民粹主義.md "wikilink")，覺得歷史是開放給所有人的，正因如此，他常因為人們稱呼他為「人民的歷史學家」（People's
Historian）和「每一人的歷史學家」（Everyman's
Historian）而感到欣慰。泰勒支持反[偉人論](../Page/偉人論.md "wikilink")，並不相信「英雄造時勢」，反而相信歷史是由一大群愚人所建構而成的。他的著述之描述手法充滿[諷刺和](../Page/諷刺.md "wikilink")[幽默之餘](../Page/幽默.md "wikilink")，而具備資料性。特別的是，他更時常從特別的角度出發研究歷史，反駁其他人認為是至關重要的論點，所以有人以「泰勒主義」來稱呼之，以形容他機智、簡練精闢，甚至於神秘的特點。泰勒又常指出當代人們所談及的[國際關係](../Page/國際關係.md "wikilink")，其實都只是一些悖論和荒謬之說。例如他曾在1970年的一個電視節目上評論到[墨索里尼](../Page/墨索里尼.md "wikilink")，認為他「一直也緊守自己的崗位——就是甚麼也沒有造」。他為了要把歷史帶給所有人，使他常常出現在[電台和](../Page/電台.md "wikilink")[電視](../Page/電視.md "wikilink")，從而為公眾所認識。

泰勒是少有早期常在電視節目出現的歷史學家。他最先於1950年在BBC的電視節目《In The
News》擔任講者，以善於辯駁而見稱。但後來由於他在節目上拒絕答謝獲邀的講者，而被傳媒稱為「繃著臉的大學教師」（sulky
don），復在1954年被解僱。自1955年至1961年，泰勒獲[ITV邀請在節目](../Page/英國獨立電視台.md "wikilink")《Free
Speech》中擔任講者，與其他講者進行辯論。另外，他亦曾自1957年、1958年和1961年在ITV的半小時節目中講授歷史，他在節目上不需講稿，仍能侃侃而談，精彩地講授[1917年俄國革命和](../Page/1917年俄國革命.md "wikilink")[第一次世界大戰等課題](../Page/第一次世界大戰.md "wikilink")，結果為電視台帶來了很高的收視。雖然泰勒早年和BBC的關係很差，但他仍分別在1961年、1963年、1976年、1977年和1978年主持歷史節目，另外又在1964年、1966年和1967年為ITV主持節目。後來在1980年，他曾在《Edge
of Britain》節目中，介紹英格蘭北部的旅遊名勝。泰勒最後一次主持電視節目是在1985年，節目名叫《How Wars
End》，但當時他已被[柏金遜病困擾](../Page/柏金遜病.md "wikilink")，嚴重影響他的演出。

在他的電視生涯中，泰勒曾經有不少的對手，如[馬爾科姆·馬格里奇等等](../Page/馬爾科姆·馬格里奇.md "wikilink")。當中，與右翼歷史學家[休·特雷弗-羅珀](../Page/休·特雷弗-羅珀，格蘭頓的戴克男爵.md "wikilink")（Hugh
Trevor-Roper）的長期論戰最為有名。其中在1961年，特雷弗-羅珀曾說：「我恐怕你的著作《第二次世界大戰的起源》會損害你的名聲」，泰勒則反駁到：「你的批評會損害你作為歷史學家的名聲——如果你有的話。」

其實泰勒和特雷弗-羅珀早在1957年因為爭奪牛津大學的[皇家教席而出現分歧](../Page/皇家教席.md "wikilink")。兩人雖然一直對歷史哲學持有不同的觀點，但兩人在1950年代初期曾經是朋友，但在皇家教席一事上，時任首相[麥美倫最終把教席給予特雷弗](../Page/麥美倫.md "wikilink")-羅珀，而非傾向工黨的泰勒，此外當時不少牛津的教授認為泰勒時常現身電視節目，有「貶低」歷史學家地位之嫌，因而遊說不讓他取得教席。經此事以後，兩人關係變得疏淡。

在公眾場合上，泰勒指自己不會從「手上沾滿蘇伊士的鮮血」的英國政府，接受任何榮譽。不過在私人場合上，泰勒則對特雷弗-羅珀取得他認為是自己的皇家教席，而感到十分失望，因為泰勒比特雷弗-羅珀要早10年就已經來到牛津大學執教。從那時開始，泰勒就借所有機會向他個人和他的學術成就作出抨擊，而特雷弗-羅珀亦作出了反駁。兩人的論戰經[電視的播放而廣為流傳](../Page/電視.md "wikilink")，而公眾對他們的論戰亦感到趣味性多於學術性，結果他們倆的長期不和遂為廣大民眾所知。同樣地，他們之間亦展開過不少筆戰，而且也是極富趣味性的。有不少人把泰勒和特雷弗-羅珀的不和形容為兩代人的不和，泰勒本人富有民粹和不恭不敬的色彩，要比特雷弗-羅珀年長10歲，但卻被傳媒形容為較年輕創新的一代。相反，特雷弗-羅珀的作風保守（當時是少有仍然穿著教袍授課的牛津教授），為人較泰勒自負，結果被傳媒形容為較年長守舊的一代。從兩人的論戰的小處，亦不難發現這種分別，正如特雷弗-羅珀會以「泰勒先生」或「泰勒」來稱呼對方，但泰勒卻以「休」來直呼特雷弗-羅珀。

在1954年，泰勒印行了他的鉅著，《爭奪歐洲霸權的鬥爭：1848年－1918年》（*The Struggle for Mastery in
Europe 1848–1918*），接著又在1957年發行《麻煩製造者》（*The Trouble
Makers*）。《麻煩製造者》一書品評英國的外交政策，原本在1955年的一個學術座談會上發表。到了1961年，泰勒發行了他一生之中最具爭議性的著作——《第二次世界大戰的起源》（*The
Origins of the Second World
War*），該書為他贏得「[修正主義者](../Page/歷史修正主義.md "wikilink")」的稱號。

作為一位[社會主義者](../Page/社會主義.md "wikilink")，泰勒視[資本主義在道德原則上是錯誤的](../Page/資本主義.md "wikilink")。他認為西方的現況難以帶來一套公平和道德的國際系統。相反，他認為現狀非常不穩，容易造成意外。他的著作亦不時重申意外決定歷史。以他看來，領袖不會製造歷史，他們只是就不同的事件作出反應。歷史之發生是錯誤和失敗所造成，而且這是非人所能控制的。所以每人只是在透過犯錯來形成歷史。因此，他在其暢銷書之一，[俾斯麥的](../Page/俾斯麥.md "wikilink")[傳記中評論到](../Page/傳記.md "wikilink")，「鐵血宰相」之所以統一德國，是意外多於計劃。

### 《第二次世界大戰的起源》爭議

[origins_of_the_second_world_war.jpeg](https://zh.wikipedia.org/wiki/File:origins_of_the_second_world_war.jpeg "fig:origins_of_the_second_world_war.jpeg")
《第二次世界大戰的起源》一書在1961年出版，是泰勒最具爭議的一本著作，當中的第二次世界大戰，是指1939年9月以後，德國、波蘭、英國和法國之間的戰爭。泰勒大膽地推翻社會主流的觀點，指出第二次世界大戰的爆發，並非[希特勒的計劃](../Page/希特勒.md "wikilink")。他在書的開首首先指出，太多人深信於所謂的「紐倫堡理論」，即二次大戰是希特勒與他的同謀的犯罪陰謀的結果。泰勒認為，這個理論實質過份簡單方便，一筆掩蓋其他國家元首的戰責，亦讓全體德國人民免去了承擔戰責的下場。這個理論也好讓於戰後成立的[西德在](../Page/西德.md "wikilink")[冷戰中加入資本主義陣營](../Page/冷戰.md "wikilink")。

至於根據泰勒本人的理論，希特勒本人並不是二次大戰爆發的核心人物，而且還是一位在外交政策上沒有不妥的德國領袖。他又引德國史家[弗里茨·費歇爾的論點](../Page/弗里茨·費歇爾.md "wikilink")，指出[第三帝國的外交政策實際上與](../Page/第三帝國.md "wikilink")[魏瑪共和國和](../Page/魏瑪共和國.md "wikilink")[第二帝國無異](../Page/德意志第二帝國.md "wikilink")。此外，他又指出希特勒除了是位平常的德國領袖，而且也是位平常的西方領袖。作為一位平常的西方領袖，希特勒著實與[古斯塔夫·施特雷澤曼](../Page/古斯塔夫·施特雷澤曼.md "wikilink")、[尼維爾·張伯倫和](../Page/尼維爾·張伯倫.md "wikilink")[達拉第沒有甚麼分別](../Page/達拉第.md "wikilink")。泰勒的論據是，希特勒希望德國成為歐洲最強的國家，但他卻無意，也沒有計劃去發動戰爭。1939年所爆發的大戰，實乃因意外而不幸造成，而且是每人也應負上責任。

泰勒對希特勒個人有一番獨到的見解，認為希特勒是一位貪婪的機會主義者，除了追求權力和信奉[反猶太主義外](../Page/反猶太主義.md "wikilink")，甚麼也不信。泰勒辯護到希特勒沒有訂下任何計劃，而他的外交政策也只是因應機會而見步行步。他甚至認為，希特勒的反猶太主義情緒根本不是獨一無二的，當時其實成千上萬的德國人和奧地利人其實也懷有強烈的反猶太情緒，因此沒有理由單獨由希特勒負起反猶太人的責任，而廣大民眾卻獨善其身。

泰勒又指出，二次大戰的爆發，應歸根於《[凡爾賽和約](../Page/凡爾賽和約.md "wikilink")》之失當。該條約對德國之苛刻，足以使德國人產生復仇和怨恨之情，卻不足以徹底摧毀德國潛在的軍事威脅，也沒法阻止德國再次成為強國。正因如此，德國人對《凡爾賽和約》所建立的國際系統根本是不公義的，所以就本能地要推翻《和約》，亦所以《和約》本身是造成不穩定的一大因素。泰勒認為，如果《和約》不是如此嚴苛，第二次世界大戰便可能不會發生。

此書在出版的時候引起了社會輿論的激烈反響和爭論，並遭到很多人的大肆抨擊。由於此書發行後不久，信奉[新納粹主義的](../Page/新納粹主義.md "wikilink")[美國史家](../Page/美國.md "wikilink")[大衛·霍根](../Page/大衛·霍根.md "wikilink")（David
Hoggan）在1964年發行《被迫出的戰爭》（*Der Erzwungene
Krieg*）一書，以致泰勒的《第二次世界大戰的起源》被公眾與新納粹主義混為一談。泰勒於是加以反駁和否認，並對霍根的書加以批評，指出該書所說的英、波陰謀使德國無辜牽連一事，乃無稽之談。

輿論對此書有林林總總的批評，包括不滿泰勒所說，[綏靖政策是合理的政治策略](../Page/綏靖政策.md "wikilink")；二次大戰是《凡爾賽和約》所命定，又是外交失敗的「意外」結果；希特勒是一位「平常」的領袖；以及忽略了納粹主義在意識形態上所起的作用。在眾多抨擊中，以休·特雷弗-羅珀最為突出。泰勒在書中指出，從1937年的《[霍斯巴赫備忘錄](../Page/霍斯巴赫備忘錄.md "wikilink")》所見，希特勒並沒有就大戰立下任何計劃。可是特雷弗-羅珀卻反駁，指出希特勒在備忘錄中清楚表達開戰的意欲，而大戰的爆發只不過是時間的問題。另外，其他批評《第二次世界大戰的起源》一書的史家還包括了[艾薩克·德意志](../Page/艾薩克·德意志.md "wikilink")（Isaac
Deutscher）、[芭芭拉·塔奇曼](../Page/芭芭拉·塔奇曼.md "wikilink")（Barbara
Tuchman）、[格哈特·溫伯格](../Page/格哈特·溫伯格.md "wikilink")、[哈利·辛斯利爵士](../Page/哈利·辛斯利.md "wikilink")（Sir
Harry Hinsley）、[約翰·惠勒-班奈特爵士](../Page/約翰·惠勒-班奈特.md "wikilink")（Sir John
Wheeler-Bennett）、[戈洛·曼](../Page/戈洛·曼.md "wikilink")、[戈登·A·克雷格](../Page/戈登·A·克雷格.md "wikilink")（Gordon
A. Craig）和[A·L·羅塞](../Page/A·L·羅塞.md "wikilink")（A. L.
Rowse）等等。此外，亦有不少史家撰寫有關第二次世界大戰起源的書籍，對泰勒的論點加以反駁。

一如英國，當《第二次世界大戰的起源》於1962年1月在[美國發行的時候](../Page/美國.md "wikilink")，也遭到了輿論一致憤怒的抨擊。幾乎每一位美國史家都對該書留下了負面的書評。相反新納粹主義組織卻宣稱泰勒的《第二次世界大戰的起源》已「宣判」希特勒無罪，不須負上戰責，而且更稱譽於泰勒。泰勒對此備受困擾，除了多次強調自己與新納粹主義份子沒有絲毫關係外，又高調表明自己對新納粹主義感到極度反感。

泰勒對[意大利的評價也成為學界的批評要項](../Page/意大利.md "wikilink")。泰勒形容[墨索里尼是一個愛出風頭的人](../Page/墨索里尼.md "wikilink")，但背後卻是一個無能笨拙，沒有任何信仰的領導人。他又點出墨索里尼最初幫助英國和法國組成[斯特雷薩陣線](../Page/斯特雷薩陣線.md "wikilink")，以抗衡德國，只是因為意大利後來因入侵[阿比西尼亞而被](../Page/阿比西尼亞.md "wikilink")[國際聯盟](../Page/國際聯盟.md "wikilink")[制裁一事](../Page/制裁.md "wikilink")，才使墨索里尼轉投納粹德國。近年來，不少[意大利歷史學家對此作出挑戰](../Page/意大利歷史.md "wikilink")，他們認為墨索里尼信奉以「生存空間」為外交方針，並視[地中海](../Page/地中海.md "wikilink")、[巴爾幹半島](../Page/巴爾幹半島.md "wikilink")、[中東和](../Page/中東.md "wikilink")[非洲之角之土地為他們所應得的](../Page/非洲之角.md "wikilink")。只不過是因為英、法兩國盤踞地中海才使意大利之國力不能伸張。

最後，泰勒亦因為以「腐化墮落」（La
décadence）形容[第三共和而備受批評](../Page/第三共和.md "wikilink")。泰勒認為，法國第三共和是一個衰落的政權，政治不穩、黨派分裂、政壇頻頻更迭、貪污嚴重，加上種種失業、罷工、暴動和民眾失望的情緒，使第三共和瀕臨於左翼和右翼間的內戰。在第三共和的眾多總理中，只有對[人民陣線政府的](../Page/人民陣線.md "wikilink")[萊昂·布魯姆較有好感](../Page/萊昂·布魯姆.md "wikilink")，認為他在任內有切實推行社會改革。不少[法國的史學都認同泰勒的觀點](../Page/法國歷史.md "wikilink")，但他們多不認同他以過份諷刺的手法來描寫法國政治。

雖然泰勒因為此書而備受非議，但《第二次世界大戰的起源》一書仍然在[史學史的範疇內](../Page/史學史.md "wikilink")，被視為研究第二次世界大戰起源的分水嶺。以下列出一些史家對此書的一些褒獎梗要：

  - 指出[綏靖政策是一項受歡迎的政策](../Page/綏靖政策.md "wikilink")，而且是英國外交政策在1933年以後的延續。推翻以往認為綏靖主義者只屬少數，神秘地在1930年代中期「騎劫」了英國政府，而且為廣大民眾所反對的觀點。
  - [德奧合併不是](../Page/德奧合併.md "wikilink")[奧地利在不情願的情況下進行](../Page/奧地利.md "wikilink")，所以奧地利不是受害者。
  - 首位以正常人，而非「狂人」來描述希特勒的歷史學家。
  - 指出德國自1871年至1939年外交政策的延續性，雖然這種延續性仍存有很大的爭議。
  - 指出德、意外交政策的飄忽性，繼而引起學界對德、意外交政策是有計劃，抑或是見機行事的辯論。
  - 指出希特勒是機會主義者，從而引發其他史家反駁希特勒是「知行合一」的人，但卻以彈性的手法達至目標。

### 晚年生涯

經過《第二次世界大戰的起源》一書的長期爭議，不少人亦對他作為歷史學家而感到特別失望，結果[牛津大學在](../Page/牛津大學.md "wikilink")1964年拒絕再與泰勒續約。但泰勒不久即憑在1965年出版的《英國歷史
1914年－1945年》而重拾名聲，這也是他唯一一本涉及[社會和](../Page/社會歷史.md "wikilink")[文化歷史的書籍](../Page/文化歷史.md "wikilink")。書中以親切熱情的手法，講述1914年至1945年的[英國歷史](../Page/英國歷史.md "wikilink")，結果該書除了成為暢銷書外，其發行量更比歷年所有版次的《[牛津英國歷史](../Page/牛津英國歷史.md "wikilink")》的發行量總和還要多。雖然泰勒對英國歷史，尤其是和[愛爾蘭有關的歷史感到羞恥](../Page/愛爾蘭.md "wikilink")，但他仍然以自己是英國人，尤其是英格蘭人而感到相當自豪。他亦時常標榜自己不拘泥於傳統，也視自己為偉大的傳統的一部份。他更認為激進的提出反對意見正正就是[英格蘭真正光榮的歷史](../Page/英格蘭.md "wikilink")。

雖然泰勒一直主張歷史不是由個人帶動，但他認為[列寧和](../Page/列寧.md "wikilink")[戴維·勞合·喬治是僅有的例外](../Page/戴維·勞合·喬治.md "wikilink")，而這兩人也是他心目中的英雄。此外，[E·H·卡爾是他最喜愛的歷史學家](../Page/愛德華·霍列特·卡爾.md "wikilink")，兩人也是好朋友。

到了1960年代晚期，泰勒接納了[漢斯·蒙森](../Page/漢斯·蒙森.md "wikilink")（Hans
Mommsen）的觀點，同意1933年2月27日發生的[國會縱火案並非](../Page/國會縱火案.md "wikilink")[希特勒和](../Page/希特勒.md "wikilink")[納粹黨在背後策劃](../Page/納粹黨.md "wikilink")，而是縱火者本人——[荷蘭共產黨黨員](../Page/荷蘭共產黨.md "wikilink")[馬利努斯·范·德爾·盧貝一人所為](../Page/馬利努斯·范·德爾·盧貝.md "wikilink")。泰勒又指出，即使沒有國會縱火案一事，德國納粹黨也能夠輕易摧毀德國的民主。

泰勒曾在1963年為英國共產黨發行的《[震撼世界的十天](../Page/震撼世界的十天.md "wikilink")》作序。另外又在分別在1964年和1970年為《國會縱火案》和《長空勇士：不列顛之戰戰紀》兩書作序。

在1984年，泰勒一次橫過馬路的時候遭[汽車撞倒](../Page/汽車.md "wikilink")。這次車禍加上此後的幾次[中風](../Page/中風.md "wikilink")，促使他在1985年正式退休，不再於電視授課。泰勒在最後的歲月又患上[柏金遜病](../Page/柏金遜病.md "wikilink")，使他喪失寫作能力。泰勒最後一次出現在公眾場合是在他於1986年的80大壽招待會，當時他的不少門生，如[馬丁·吉柏特爵士和](../Page/馬丁·吉柏特.md "wikilink")[保羅·甘迺迪等人也有出席](../Page/保羅·甘迺迪_\(歷史學家\).md "wikilink")。泰勒雖然勞力地背誦出一篇短小的講辭，但仍無法遮掩他的嚴重病情。在1987年，泰勒搬進[倫敦的一間療養院](../Page/倫敦.md "wikilink")。最後於1990年9月7日在療養院去世。終年84歲。

## 婚姻生活

泰勒共有三段婚姻，分別是：

  - 第一任妻子是瑪格麗特·亞當斯，兩人於1931年結婚，1951年離異，共生有3名孩子。雖然瑪格麗特對他不忠，但泰勒對她的感情最為深厚。
  - 第二任妻子是夏娃·克羅斯蘭，兩人於1951年結婚，1974年離異，共生有2名孩子。兩人離異後仍同居一段時間。
  - 第三任妻子是[匈牙利歷史學家夏娃](../Page/匈牙利.md "wikilink")·哈拉茲梯，兩人於1976年結婚。

## 著作

### 原著

<div style="font-size: 85%">

  - *The Italian Problem in European Diplomacy, 1847–1849*, 1934.
  - *Germany's First Bid for Colonies 1884–1885: a Move in Bismarck's
    European Policy*, 1938.
  - *The Habsburg Monarchy 1809–1918*, 1941, revised edition 1948.
  - *The Course of German history: a Survey of the Development of
    Germany since 1815*, 1945.
  - *The Struggle for Mastery in Europe 1848–1918*, 1954.
  - *Bismarck: the Man and Statesman*, 1955.
  - *The Trouble Makers: Dissent over Foreign Policy, 1792–1939*, 1957.
  - *The Origins of the Second World War*, 1961.
  - *The First World War: an Illustrated History*, 1963.
  - *English History 1914–1945* (Volume XV of the *Oxford History of
    England*), 1965.
  - *Europe: Grandeur and Decline*, 1967.
  - *War by Timetable*, 1969.
  - *Beaverbrook*, 1972.
  - *The Second World War: an Illustrated History*, 1975.
  - *The Last of Old Europe: a Grand Tour*, 1976.
  - *The War Lords*, 1977.
  - *How Wars Begin*, 1979.
  - *Politicians, Socialism, and Historians*, 1980.
  - *Revolutions and Revolutionaries*, 1980.
  - *A Personal History*, 1983.
  - *An Old Man's Diary*, 1984.
  - *How Wars End*, 1985.
  - *Letters to Eva: 1969–1983*, edited by Eva Haraszti Taylor, 1991.
  - *From Napoleon to the Second International: Essays on
    Nineteenth-century Europe* edited with an introduction by Chris
    Wrigley, 1993.
  - *From the Boer War to the Cold War: Essays on Twentieth-century
    Europe*, edited with an introduction by Chris Wrigley, 1995.

</div>

### 譯作

  -
  -
  -
  -
  -
  -
## 參考資料及延伸閱讀

<div style="font-size: 85%">

  - Bosworth, Robert *Explaining Auschwitz and Hiroshima: History
    Writing and the Second World War, 1945-90*, London: Routledge, 1993.
  - Boyer, John "A.J.P. Taylor and the Art of Modern History" pages
    40–72 from *Journal of Modern History*, Volume 49, Issue 1, March
    1977.
  - Burk, Kathleen *Troublemaker: The Life And History Of A.J.P. Taylor*
    New Haven: Yale University Press, 2000.
  - Cole, Robert *A.J.P Taylor: The Traitor Within The Gates* London:
    Macmillan, 1993.
  - Cook, Chris and Sked, Alan (editors) *Crisis and Controversy: Essays
    In Honour of A. J. P. Taylor*, London : Macmillan Press, 1976
  - Dray, William "Concepts of Causation in A.J.P. Taylor's Account of
    the Origins of the Second World War" pages 149–172 from *History and
    Theory*, Volume 17, Issue \#1, 1978.
  - Gilbert, Martin (editor) *A Century of Conflict, 1850-1950; Essays
    for A.J.P. Taylor*, London, H. Hamilton 1966.
  - Hauser, Oswald "A.J.P. Taylor" pages 34–39 from *Journal of Modern
    History*, Volume 49, Issue \#1, March 1977.
  - Hett, Benjamin C. "Goak Here: A.J.P. Taylor and the Origins of the
    Second World War" pages 257-280 from *Canadian Journal of History*,
    Volume 32, Issue \#2, 1996.
  - Johnson, Paul "A.J.P. Taylor: A Saturnine Star Who Had Intellectuals
    Rolling In The Aisles" page 31 from *The Spectator*, Volume 300,
    Issue \# 9266, March 11, 2006.
  - Kennedy, Paul "A.J.P. Taylor \`Profound Forces' in History" pages
    9–13 from *History Today*, Volume 33, Issue \#3, March 1986.
  - Kennedy, Paul "The Nonconformist" pages 109-114 from *The Atlantic*,
    Volume 287, Issue \#4, April 2001.
  - Louis, William (editor) *The Origins of the Second World War: A.J.P
    Taylor And His Critics*, New York: Wiley & Sons, 1972.
  - Martel, Gordon (editor) *The Origins Of The Second World War
    Reconsidered: A.J.P. Taylor And The Historians* London; New York:
    Routledge, 1986, revised edition 1999.
  - Mehta, Ved *Fly and Fly Bottle: Encounters with British
    Intellectuals*, London: Weidenfeld & Nicolson, 1962.
  - Pepper, F. S., *Handbook of 20th century Quotations*, Sphere Study
    Aids, 1984, passim.
  - Robertson, Esmonde (editor) *The Origins of the Second World War:
    Historical Interpretations*, London: Macmillan, 1971.
  - Sisman, Adam *A. J. P. Taylor: A Biography* London:
    Sinclair-Stevenson, 1994.
  - Smallwood, J. "A Historical Debate of the 1960s: World War II
    Historiography-the Origins of the Second World War, A.J.P. Taylor
    and his Critics" pages 403–410 from *Australian Journal of Politics
    and History*, Volume 26, Issue \#3, 1980.
  - Watt, D.C. "Some Aspects of AJP Taylor's Work as Diplomatic
    Historian" pages 19–33 from *Journal of Modern History*, Volume 49,
    Issue \#1, March 1977.
  - Williams, H. Russell "A.J.P. Taylor" from *Historians of Modern
    Europe* edited by Hans Schmitt, Baton Rouge: Louisiana State Press,
    1971.
  - Wrigley, Chris (editor) *A.J.P Taylor: A Complete Bibliography and
    Guide to his Historical and Other Writings*, Brighton: Harvester,
    1982.
  - Wrigley, Chris (editor) *Warfare, Diplomacy and Politics : Essays In
    Honour Of A.J.P. Taylor*, London : Hamilton, 1986.
  - Wrighley, Chris 'A. J. P. Taylor: a Nonconforming Radical Historian
    of Europe" pages 74–75 from *Contemporary European History* , Volume
    3, 1994.
  - Wrigley, Chris J. *A.J.P. Taylor—Radical Historian of Europe*.
    London: I.B. Tauris, 2006 (hardcover, ISBN 978-1-86064-286-9).
      - [Reviewed](http://books.guardian.co.uk/reviews/biography/0,,1967714,00.html?gusrc=rss&feed=10)
        by Tristram Hunt in the
        [*Guardian*](http://www.guardian.co.uk/)，December 9, 2006.
  - "Taylor, A(lan) J(ohn) P(ercivale)" pages 389-392 from *Current
    Biography 1983* edited by Charles Moritz, H.W. Wilson Company, New
    York, New York, U.S., 1983, 1984.

</div>

## 外部連結

  - [A·J·P·泰勒](http://www.screenonline.org.uk/people/id/838462/)
  - [A.J.P.
    有關泰勒在《第二次世界大戰的起源》一書的修正主義](http://www.age-of-the-sage.org/history/historian/A_J_P_Taylor.html)
  - [經濟帝國主義](http://www.panarchy.org/taylor/imperialism.1952.html)，由A·J·P·泰勒撰寫
  - [泰勒論文](https://web.archive.org/web/20070125165626/http://history.sandiego.edu/gen/WW2Timeline/Taylorthesis.html)
  - [對A·J·P·泰勒的評論](https://web.archive.org/web/20060312225634/http://www.prospectmagazine.co.uk/article_details.php?id=7362)
  - [希特勒、施特雷澤曼與不連續的德國外交政策](https://web.archive.org/web/20051027175954/http://www.orange.k12.oh.us/teachers/ohs/tshreve/apwebpage/readings/hitlerstresseman.html)
  - [《第二次世界大戰的起源》的三個反思](http://www.airpower.maxwell.af.mil/airchronicles/aureview/1969/jul-aug/detwiler.html)

[T](../Category/英国历史学家.md "wikilink")
[T](../Category/牛津大学奥里尔学院校友.md "wikilink")
[T](../Category/伦敦大学学院校友.md "wikilink")
[Category:牛津大学教授](../Category/牛津大学教授.md "wikilink")
[Category:20世纪历史学家](../Category/20世纪历史学家.md "wikilink")