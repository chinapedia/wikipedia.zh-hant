{{ Original research }}

**葉振邦**（），[香港](../Page/香港.md "wikilink")[配音員](../Page/配音員.md "wikilink")、[廣東的](../Page/廣東.md "wikilink")[播音員](../Page/播音員.md "wikilink")，1980年代[廣東台的粵語主持](../Page/廣東台.md "wikilink")，\[1\]
1990年代加入[亞洲電視配音組](../Page/亞洲電視.md "wikilink")，2000年因為亞視配音組解散後任職自由身配音員，主要擔任記錄片旁白，為[迪士尼動畫電影配音](../Page/迪士尼.md "wikilink")。

## 配音作品

※主要角色以**粗體**顯示

### 電視動畫／OVA

| 首播年份                               | 作品名稱                                             | 配演角色      | 備註                               |
| ---------------------------------- | ------------------------------------------------ | --------- | -------------------------------- |
| **1998**                           | [洛克人](../Page/洛克人.md "wikilink")                 | 偉利博士      |                                  |
| [浪客劍心](../Page/浪客劍心.md "wikilink") | 火男                                               |           |                                  |
| **2000**                           | [名偵探柯南](../Page/名偵探柯南_\(动画\).md "wikilink")      | **毛利小五郎** | [VCD](../Page/VCD.md "wikilink") |
| **2003**                           | [Barbie之天鵝湖篇](../Page/Barbie之天鵝湖篇.md "wikilink") | 依拉斯瑪斯     |                                  |
|                                    |                                                  |           |                                  |

### 剧场版／動畫電影

<table>
<thead>
<tr class="header">
<th><p>首播年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1991</strong></p></td>
<td><p><a href="../Page/美女與野獸_(1991年電影).md" title="wikilink">美女與野獸</a></p></td>
<td><p><strong>盧米亞</strong>（Lumière）</p></td>
<td><p>電影公映版<br />
<a href="../Page/洲立影視.md" title="wikilink">洲立影視DVD</a></p></td>
</tr>
<tr class="even">
<td><p><strong>1995</strong></p></td>
<td><p><a href="../Page/風中奇緣_(电影).md" title="wikilink">風中奇緣</a></p></td>
<td><p><strong>酋長包華頓</strong>[2][3]</p></td>
<td><p>電影公映版<br />
<a href="../Page/洲立影視.md" title="wikilink">洲立影視DVD</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>1998</strong></p></td>
<td><p><a href="../Page/義犬報恩.md" title="wikilink">義犬報恩</a>（劇場版）</p></td>
<td><p><em>積古'爺爺</em>'（Jehan Daas）</p></td>
<td><p>亞視<br />
<a href="https://www.youtube.com/watch?v=lIoqPQ8XQjI">【按此播放】</a></p></td>
</tr>
<tr class="even">
<td><p><strong>1999</strong></p></td>
<td><p><a href="../Page/風中奇緣2：倫敦之旅.md" title="wikilink">風中奇緣2：新世界旅程</a></p></td>
<td><p><strong>酋長包華頓</strong></p></td>
<td><p><a href="../Page/洲立影視.md" title="wikilink">洲立影視DVD</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2000</strong></p></td>
<td></td>
<td><p><strong>盧米亞</strong>（Lumière）[4]</p></td>
<td><p><a href="../Page/洲立影視.md" title="wikilink">洲立影視DVD</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2012</strong></p></td>
<td><p><a href="../Page/勇敢傳說之幻險森林.md" title="wikilink">勇敢傳說之幻險森林</a></p></td>
<td><p><strong>麦葛爵士</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2013</strong></p></td>
<td><p><a href="../Page/魔雪奇緣.md" title="wikilink">魔雪奇緣</a></p></td>
<td><p><strong>佩比</strong>[5]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2017</strong></p></td>
<td><p><a href="../Page/蓝精灵3.md" title="wikilink">藍精靈：迷失的村莊</a></p></td>
<td><p><strong>精靈爸爸</strong>（Papa Smurf）</p></td>
<td><p>電影公映版</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 中／港劇

<table>
<thead>
<tr class="header">
<th><p>首播年份</p></th>
<th><p>劇集名稱</p></th>
<th><p>配演角色</p></th>
<th><p>角色演員</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1995</strong></p></td>
<td><p><a href="../Page/三國演義_(電視劇).md" title="wikilink">三國演義</a></p></td>
<td><p><strong><a href="../Page/司馬懿.md" title="wikilink">司馬懿</a></strong><br />
<a href="../Page/魏延.md" title="wikilink">魏延</a></p></td>
<td><p><a href="../Page/唐振環.md" title="wikilink">唐振環</a>、<a href="../Page/魏宗萬.md" title="wikilink">魏宗萬</a>[6]<br />
<a href="../Page/王心海.md" title="wikilink">王心海</a></p></td>
<td><p><br />
代配64-67集,因場口撞角而改配另一角的<a href="../Page/馮志輝.md" title="wikilink">馮志輝</a></p></td>
</tr>
<tr class="even">
<td><p><strong>1997</strong></p></td>
<td><p><a href="../Page/孫武_(電視劇).md" title="wikilink">孫子</a></p></td>
<td><p><strong>吳王<a href="../Page/闔閭.md" title="wikilink">闔閭</a></strong></p></td>
<td><p><a href="../Page/張甲田.md" title="wikilink">張甲田</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1999</strong></p></td>
<td><p><a href="../Page/雍正王朝.md" title="wikilink">雍正王朝</a></p></td>
<td><p><strong><a href="../Page/康熙.md" title="wikilink">康熙</a></strong></p></td>
<td><p><a href="../Page/焦晃.md" title="wikilink">焦　晃</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2002</strong></p></td>
<td><p><a href="../Page/太平天國_(2000年電視劇).md" title="wikilink">太平天國</a></p></td>
<td><p><a href="../Page/曾國藩.md" title="wikilink">曾國藩</a></p></td>
<td><p><a href="../Page/孫飛虎.md" title="wikilink">孫飛虎</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 新加坡劇

| 首播年份     | 劇集名稱                                               | 配演角色     | 角色演員                             | 備註 |
| -------- | -------------------------------------------------- | -------- | -------------------------------- | -- |
| **1998** | [神鵰俠侶](../Page/神鵰俠侶_\(1998年新加坡電視劇\).md "wikilink") | **金輪法王** | [鄭各評](../Page/鄭各評.md "wikilink") |    |
| **1999** | 哪吒（莲花童子哪吒）                                         | **姜子牙**  | [張鴻鑫](../Page/張鴻鑫.md "wikilink") |    |
|          |                                                    |          |                                  |    |

### 電影

<table>
<thead>
<tr class="header">
<th><p>首播年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>角色演員</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1999</strong></p></td>
<td><p><a href="../Page/七宗罪_(電影).md" title="wikilink">七宗罪</a></p></td>
<td><p><strong>森威廉</strong>（William Somerset）</p></td>
<td><p><a href="../Page/摩根·費曼.md" title="wikilink">摩根·費曼</a></p></td>
<td><p><a href="../Page/ATV.md" title="wikilink">ATV版</a><br />
<a href="https://www.youtube.com/watch?v=2P_E_z0KBUM">【按此收看】</a><br />
2003年重播</p></td>
</tr>
<tr class="even">
<td><p><strong>2014</strong></p></td>
<td><p><a href="../Page/雷神奇俠.md" title="wikilink">雷神奇俠</a></p></td>
<td><p><strong><a href="../Page/奧丁_(漫威漫畫).md" title="wikilink">奧丁</a></strong></p></td>
<td><p><a href="../Page/安東尼·鶴健士.md" title="wikilink">安東尼·鶴健士</a></p></td>
<td><p><a href="../Page/明珠台.md" title="wikilink">明珠台</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 記錄片

<table>
<thead>
<tr class="header">
<th><p>首播年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1997</strong></p></td>
<td><p><a href="../Page/吃在中國.md" title="wikilink">吃在中國</a></p></td>
<td><p>旁白 [7]</p></td>
<td><p><a href="../Page/亞洲電視.md" title="wikilink">亞洲電視</a></p></td>
</tr>
<tr class="even">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/國家地理頻道.md" title="wikilink">國家地理頻道</a>（National Geographical Channel）</p></td>
<td><p>旁白</p></td>
<td><p><a href="../Page/NOW.md" title="wikilink">NOW</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2010</strong></p></td>
<td><p>黃金歲月<br />
第二集《大海之笑顏．一別六年》</p></td>
<td><p><strong>砂川寬良</strong></p></td>
<td><p><a href="../Page/香港電台.md" title="wikilink">香港電台</a>[8][9]<br />
<a href="http://video.tudou.com/v/XMjAxNzEyNjY0OA==.html?spm=a2hzp.8253869.0.0">【按此播放】</a>[10]</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 廣播劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2000</strong></p></td>
<td><p><a href="../Page/鹿鼎記.md" title="wikilink">鹿鼎記</a></p></td>
<td><p><a href="../Page/佟國綱.md" title="wikilink">佟國綱</a><sub><small>（第95回）</small></sub></p></td>
<td><p>香港電台廣播劇</p></td>
</tr>
<tr class="even">
<td><p><strong>2001</strong></p></td>
<td><p><a href="../Page/雍正皇帝_(小说)#.E5.BB.A3.E6.92.AD.E5.8A.87.md" title="wikilink">雍正皇帝</a></p></td>
<td><p><a href="../Page/諾岷.md" title="wikilink">諾　岷</a><sub><small>（33-35回）</small></sub><br />
張興仁<sub><small>（64-65回）</small></sub></p></td>
<td><p>香港電台廣播劇<br />
<a href="https://www.youtube.com/watch?v=h2Oe9P2Dubg">【按此播放第33-35回】</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - 2017年4月1日《搜狐》[对话大师－－粤语讲古艺术家叶振邦：教路青少年讲古！](http://webcache.googleusercontent.com/search?q=cache:QK129H1fiJ8J:www.sohu.com/a/133317378_534419+&cd=1&hl=zh-TW&ct=clnk&gl=hk)：「叶振邦，著名粤语讲古艺术家。香港凤凰卫视首席编审，香港迪士尼粤语**配音**导演，香港亚洲电视监制，广东电视台，电台著名主持人，在省港澳电台播出多部长篇小说。」

[Category:香港配音員](../Category/香港配音員.md "wikilink")
[Category:粵語配音員](../Category/粵語配音員.md "wikilink")

1.  [广东主持人大团圆](http://www.documentsky.com/6862349807/)，金羊网，2007-12-07；｛[網存2](https://web.archive.org/web/20071210071227/http://www.ycwb.com/big5/ycwb/2007-12/07/content_1713533.htm)｝
2.  [報幕截圖2](https://upload.cc/i/ec0C7U.jpg)，[風中奇緣 Pocahontas (1995)
    粵語演出名單 Cantonese singing
    cast](https://www.youtube.com/watch?v=gib7ptxOKZc)，香港版影碟片末報幕。
3.  [曲目與演唱者](https://upload.cc/i/IkiUZA.jpg)，Soundtrack：[迪士尼《風中奇緣》國際廣東版原聲大碟](https://upload.cc/i/LrGD6Y.jpg)，[滾石唱片](../Page/滾石唱片.md "wikilink")，1995。
4.  [報幕截圖1](https://upload.cc/i/CQmUGx.jpg)，[美女與野獸-雪地奇緣 (2000)
    粵語配音名單](https://www.youtube.com/watch?v=HSbIg5Vi3Wc)，香港版影碟片末報幕。
5.  [Frozen魔雪奇緣 - Cantonese Voice
    Cast粵語配音演員表](https://www.youtube.com/watch?v=rmWMDHX9vqk)
6.  [「角色報幕」載圖](https://drive.google.com/open?id=1Haa1uuKNc9KYi0IWBeC3eHqZ1LgS6Pgz)，亞洲電視《三國演義》，「」。
7.  [截圖1](https://upload.cc/i/uYy0O2.jpg)，[吃在中國 之 湯 ,
    刀功 5/5](https://www.youtube.com/watch?v=5pwmPsA6iMM)（Youtube片段）；[截圖2](https://upload.cc/i/H9wpq0.jpg)，[吃在中國
    之烙 , 麻油 , 麻油香酥小鯽魚 ,
    酥皮點心](https://www.youtube.com/watch?v=4x1KVanpDcg)（Youtube片段）。
8.  [黃金歲月：簡介](http://www.rthk.hk/tv/dtt31/programme/goldenage2010)
9.  [黃金歲月
    VI 2010](http://app1.rthk.org.hk/php/tvarchivecatalog/episode.php?progid=695&tvcat=4)
10. [片末截圖](https://upload.cc/i/ntsKzr.jpg)。[譚炳文聲演山川文康](../Page/譚炳文.md "wikilink")。[黎萱聲演金城婆婆](../Page/黎萱.md "wikilink")。