**Netscape
7**是由網景通訊製作的[跨平台](../Page/跨平台.md "wikilink")[網路套件系列](../Page/網路套件.md "wikilink")，修改自[Mozilla
Application
Suite](../Page/Mozilla_Application_Suite.md "wikilink")，可以在[Windows](../Page/Windows.md "wikilink")、[Mac
OS](../Page/Mac_OS.md "wikilink")、[Linux上運行](../Page/Linux.md "wikilink")。

整個套件包含Navigator（[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")）、[Netscape Mail &
Newsgroups](../Page/Netscape_Mail_&_Newsgroups.md "wikilink")（[電子郵件客戶端和](../Page/電子郵件客戶端.md "wikilink")[新聞群組軟體](../Page/Usenet客戶端.md "wikilink")）、Address
Book（通訊錄）、[Netscape
Composer](../Page/Netscape_Composer.md "wikilink")（[HTML編輯器](../Page/HTML編輯器.md "wikilink")）、[AOL
Instant
Messenger](../Page/AOL_Instant_Messenger.md "wikilink")（[即時通訊](../Page/即時通訊.md "wikilink")）、[IRC軟體客戶端](../Page/IRC.md "wikilink")、[Radio@Netscape](../Page/網景#產品與技術.md "wikilink")。

## 歷史和發展

Netscape
7.0於2002年發布。它以更穩定、速度更快的[Mozilla](../Page/Mozilla_Application_Suite.md "wikilink")
1.0核心為基礎，並附帶整合[AOL Instant
Messenger](../Page/AOL_Instant_Messenger.md "wikilink")、[ICQ](../Page/ICQ.md "wikilink")、[Radio@Netscape以及分頁式瀏覽等新功能](../Page/網景#網景後期的產品.md "wikilink")\[1\]。

美國線上於2003年7月15日宣布，將解僱參與開發Netscape版本Mozilla的員工。再加上美國線上與微軟的反壟斷訴訟達成和解，在未來的美國線上軟體中使用Internet
Explorer，這似乎標誌著Netscape
Navigator瀏覽器開發的終結。許多人認為，往後不再有新版本的瀏覽器，並且Netscape品牌名稱只能作為AOL的廉價網路撥號服務的名稱而存在。

Netscape 7.2於2004年8月17日發布，但是美國線上沒有重新啟動Netscape瀏覽器部門，而只是在內部開發。它與Netscape
7.1非常相似，其中唯一的新特性是Netscape
Toolbar，由[mozdev.org所開發](../Page/mozdev.org.md "wikilink")。

儘管許多人認為Netscape 7將成為Netscape的最後一個版本，但美國線上在2005年5月發布了[Netscape
8版本](../Page/Netscape_8.md "wikilink")。Netscape
8改進了安全性，並且能夠同時使用Internet
Explorer的[Trident和](../Page/Trident_\(排版引擎\).md "wikilink")[Gecko排版引擎](../Page/Gecko.md "wikilink")
。這讓使用者可以使用專門用於瀏覽相容Internet Explorer的網頁。

[Netscape
9於](../Page/Netscape_Navigator_9.md "wikilink")2007年10月取代了Netscape
8。

## 版本歷史

  - Netscape 7.0 PR1 – （基於Mozilla Suite）
  - Netscape 7.0 – 2002年8月29日（基於Mozilla Suite 1.0.1）
  - Netscape 7.01 – 2002年12月10日（基於Mozilla Suite 1.0.2）
  - Netscape 7.02 – 2003年2月18日（基於Mozilla Suite 1.0.2）
  - Netscape 7.1 – 2003年6月30日（基於Mozilla Suite 1.4）
  - Netscape 7.2 – 2004年8月17日（基於Mozilla Suite 1.7）

## 參考資料

## 外部連結

  - [下載各舊版本的Netscape瀏覽器和網路套件](http://sillydog.org/narchive/full123.php)
  - [由MozTW製作的非官方中文化版本](http://www.moztw.org/netscape/)

## 相關文章

  - [Netscape](../Page/Netscape.md "wikilink")
  - [網景 (瀏覽器)](../Page/網景_\(瀏覽器\).md "wikilink")
  - [Mozilla Application
    Suite](../Page/Mozilla_Application_Suite.md "wikilink")
  - [电子邮件客户端列表](../Page/电子邮件客户端列表.md "wikilink")
  - [电子邮件客户端比较](../Page/电子邮件客户端比较.md "wikilink")

[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:2002年軟體](../Category/2002年軟體.md "wikilink")
[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")
[Category:電子郵件客戶端](../Category/電子郵件客戶端.md "wikilink")

1.