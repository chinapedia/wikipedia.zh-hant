[FrankKellogg.jpg](https://zh.wikipedia.org/wiki/File:FrankKellogg.jpg "fig:FrankKellogg.jpg")\]\]
[Kellog-Briand_pact.png](https://zh.wikipedia.org/wiki/File:Kellog-Briand_pact.png "fig:Kellog-Briand_pact.png")

《**非战公约**》，全称《**关于废弃战争作为国家政策工具的普遍公约**》，亦称《**巴黎非战公约**》（Pact of
Paris）或《**凯洛格—白里安公约**》（Kellogg-Briand
Pact），是1928年8月27日在[巴黎签署一项](../Page/巴黎.md "wikilink")[国际公约](../Page/国际公约.md "wikilink")，该公约规定放弃以[战争作为国家政策的手段和只能以](../Page/军国主义.md "wikilink")[和平方法解决国际争端](../Page/和平主義.md "wikilink")，由于该公约本身是建立在[理想主义的](../Page/唯心主義.md "wikilink")[国际关系理论下](../Page/国际关系.md "wikilink")，所以该公约没有发挥实际作用，但是该项公约是人类第一次放弃战争做为国家的外交政策。

该公约由[法国外交部长](../Page/法国.md "wikilink")[白里安](../Page/白里安.md "wikilink")、[美国国务卿](../Page/美国国务卿.md "wikilink")[凯洛格于](../Page/弗兰克·B·凯洛格.md "wikilink")1927年发起，目的是法美联手抑制德国的力量。最初的签字国有15个，分别是法国、[美国](../Page/美国.md "wikilink")、[英国](../Page/英国.md "wikilink")、[德国](../Page/德国.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[比利时](../Page/比利时.md "wikilink")、[波兰](../Page/波兰.md "wikilink")、[捷克](../Page/捷克.md "wikilink")、[日本](../Page/日本.md "wikilink")、[印度](../Page/印度.md "wikilink")。1929年7月24日公约正式生效，至1934年5月签字国共达64个。

凯洛格本人因此倡议而获得1929年度[诺贝尔和平奖](../Page/诺贝尔和平奖.md "wikilink")。

## 图片

Kellogg–Briand Pact (1928).jpg BriandKellogg1928a.jpg
BriandKellogg1928b.jpg KingAntiWar.jpg

[Category:1928年条约](../Category/1928年条约.md "wikilink")
[Category:1928年法国](../Category/1928年法国.md "wikilink")
[Category:法兰西第三共和国条约](../Category/法兰西第三共和国条约.md "wikilink")
[Category:美國條約](../Category/美國條約.md "wikilink")
[Category:英國條約](../Category/英國條約.md "wikilink")
[Category:魏玛共和国条约](../Category/魏玛共和国条约.md "wikilink")
[Category:意大利王国条约](../Category/意大利王国条约.md "wikilink")
[Category:比利時條約](../Category/比利時條約.md "wikilink")
[Category:波蘭條約](../Category/波蘭條約.md "wikilink")
[Category:捷克條約](../Category/捷克條約.md "wikilink")
[Category:日本條約](../Category/日本條約.md "wikilink")
[Category:英属印度条约](../Category/英属印度条约.md "wikilink")
[Category:1928年8月](../Category/1928年8月.md "wikilink")