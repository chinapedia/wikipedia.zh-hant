**庫希特語族**（**Cushitic**）又稱作**古實語族**，是[亞非語系](../Page/亞非語系.md "wikilink")（[閃含語系](../Page/閃含語系.md "wikilink")）之下的語族之一，分佈在[埃塞俄比亞](../Page/埃塞俄比亞.md "wikilink")、[蘇丹](../Page/蘇丹.md "wikilink")、[索馬里](../Page/索馬里.md "wikilink")、[肯尼亞和](../Page/肯尼亞.md "wikilink")[坦桑尼亞](../Page/坦桑尼亞.md "wikilink")。可以細分為約四個語支：

  - [東庫希特語支](../Page/東庫希特語支.md "wikilink")(East Cushitic
    languages/Lowland East Cushitic languages)、
  - [中庫希特語支](../Page/中庫希特語支.md "wikilink")(Central Cushitic languages)、
  - [南庫希特語支](../Page/南庫希特語支.md "wikilink")(South Cushitic languages)及
  - [貝扎語支](../Page/貝扎語支.md "wikilink")(Beja language)。

「庫希特」這名字源自《[聖經](../Page/聖經.md "wikilink")》的人物[古實](../Page/古實.md "wikilink")，與[閃語族相對](../Page/閃語族.md "wikilink")。庫希特語族最大的組成部份是[奧羅莫語](../Page/奧羅莫語.md "wikilink")，有2.5千萬語言人口；其次是通行於[索馬里](../Page/索馬里.md "wikilink")、[埃塞俄比亞](../Page/埃塞俄比亞.md "wikilink")、[吉布堤及](../Page/吉布堤.md "wikilink")[肯雅的](../Page/肯雅.md "wikilink")[索馬利亞語](../Page/索馬利亞語.md "wikilink")，有1.5千萬語言人口。

## 分支

[Cusitas.png](https://zh.wikipedia.org/wiki/File:Cusitas.png "fig:Cusitas.png")

  - [中库希特语支](../Page/中库希特语支.md "wikilink")
      - 东部 Eastern
          - Xamtanga ()
      - 北部 Northern
          - [比林语](../Page/比林语.md "wikilink") ()
      - 南部 Southern
          - Awngi ()

<!-- end list -->

  -   - 西部 Western
          - Qimant ()

  - [东库希特语支](../Page/东库希特语支.md "wikilink")

      - Boon ()
      - Dullay
          - Bussa ()
          - Gawwada ()
          - Tsamai ()
      - 高原 Highland
          - 阿拉巴语 Alaba ()
          - 布尔吉语 Burji ()
          - Gedeo ()
          - 哈迪亚语 Hadiyya ()
          - 坎巴塔语 Kambaata ()
          - Libido ()
          - [锡达莫语](../Page/锡达莫语.md "wikilink") ()
      - Konso-Gidole
          - 达拉萨语 Dirasha ()
          - 孔索语Komso ()
      - [奥罗莫语](../Page/奥罗莫语.md "wikilink") ()
          - [波拉納-阿尔西-古吉方言](../Page/波拉納-阿尔西-古吉方言.md "wikilink")
            (Borana-Arsi-Guji，)
          - 中西部奥罗莫语 ()
          - 东奥罗莫语 ()
          - Orma ()
          - Sanye ()
      - Rendille-Boni
          - Boni ()
          - Rendille ()
      - [萨霍-阿法尔语](../Page/萨霍-阿法尔语.md "wikilink")
          - [阿法尔语](../Page/阿法尔语.md "wikilink") Afar()
          - [萨霍语Saho](../Page/萨霍语.md "wikilink") ()
      - [索馬里語Somali](../Page/索馬里語.md "wikilink")
          - Dabarre ()
          - 格里方言Garre ()
          - Jiiddu ()
          - [索馬里語](../Page/索馬里語.md "wikilink") Somali()
          - [通尼语](../Page/通尼语.md "wikilink") Tunni ()
          - Maay ()
      - 西奥莫-塔纳语言Western Omo-Tana
          - Arbore ()
          - Baiso ()
          - 达塞内克语 Daasanach ()
          - El Molo ()
      - Yaaku
          - Yaaku ()

  - [北库希特语支](../Page/北库希特语支.md "wikilink")

      - [贝扎语](../Page/贝扎语.md "wikilink") ()

  - [南库希特语支](../Page/南库希特语支.md "wikilink")

      - Aasáx ()
      - 布龙吉语Burunge ()
      - Dahalo ()
      - Gorowa ()
      - [伊拉庫語](../Page/伊拉庫語.md "wikilink") ()
      - [阿拉瓜語](../Page/阿拉瓜語.md "wikilink")([Alagwa](../Page/:en:Alagwa_language.md "wikilink"))
        ()
      - Kw'adza ()

## 参考文献

## 外部链接

  - [Browse by Language
    Family/Ethnologue](http://www.ethnologue.com/show_family.asp?subid=90095)

## 参见

  - [古實人](../Page/古實人.md "wikilink")：常見於《[聖經](../Page/聖經.md "wikilink")》，現泛指在[非洲之角的](../Page/非洲之角.md "wikilink")[原住民](../Page/原住民.md "wikilink")。

{{-}}

[Category:亞非語系](../Category/亞非語系.md "wikilink")
[庫希特語族](../Category/庫希特語族.md "wikilink")
[Category:语族](../Category/语族.md "wikilink")