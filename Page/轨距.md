\-{zh-hans:[Gauge_ZH-HANS.svg](https://zh.wikipedia.org/wiki/File:Gauge_ZH-HANS.svg "fig:Gauge_ZH-HANS.svg");zh-hant:[Gauge_ZH-HANT.svg](https://zh.wikipedia.org/wiki/File:Gauge_ZH-HANT.svg "fig:Gauge_ZH-HANT.svg");}-
[Rail_gauge_world.png](https://zh.wikipedia.org/wiki/File:Rail_gauge_world.png "fig:Rail_gauge_world.png")

**軌距**（track
gauge）亦即軌道距離，是[鐵路軌道兩條鋼軌之間的距離](../Page/鐵路軌道.md "wikilink")，以鋼軌的內距為準。[國際鐵路聯盟在](../Page/國際鐵路聯盟.md "wikilink")1937年制定的1435mm（4呎8½吋）為[標準軌距](../Page/標準軌距.md "wikilink")，又稱國際軌距，約佔世界上鐵路里數大約59%。比標準軌寬的軌距稱為[寬軌](../Page/寬軌.md "wikilink")，佔全球鐵路23%，主要包括[俄羅斯使用的](../Page/俄羅斯.md "wikilink")5呎（1520mm）和[印度使用的](../Page/印度鐵路運輸.md "wikilink")5呎6吋（1676mm）軌距。比標準軌窄的稱為[窄軌](../Page/窄軌.md "wikilink")，佔全球鐵路16%，主要包括[日本使用的](../Page/日本.md "wikilink")3呎6吋（1067mm）軌距及米軌（1000mm）。

一般窄轨铁路适用於山路，而宽轨铁路则有更好的高速稳定性。[雙軌距或多軌距鐵路舖有三或四條鋼軌](../Page/雙軌距.md "wikilink")，讓使用不同軌距的[列車都可行駛](../Page/列車.md "wikilink")。[日本和](../Page/日本.md "wikilink")[西班牙則採用了](../Page/西班牙.md "wikilink")[軌距可變列車](../Page/軌距可變列車.md "wikilink")，讓列車在不同軌距的路線間[直通運轉](../Page/直通運轉.md "wikilink")。

## 標準軌歷史

標準軌由最先使用鐵路的[英國提出](../Page/英國.md "wikilink")。設計及建造**史托頓－達靈頓鐵路**的[英國工程師](../Page/英國.md "wikilink")[喬治·史提芬遜提出](../Page/乔治·斯蒂芬森.md "wikilink")4呎8½吋的軌距，並成功說服火車製造商生產4呎8½吋（即1435毫米）軌距的機車及車輛。由於史提芬遜成功設計的鐵路是眾人模仿的對像，亦使這軌距變得流行。\[1\]1845年英國皇家專員建議用4呎8½吋（1435mm）作為標準軌距。1846年英國國會通過法案，要求將來所有的鐵路都使用標準軌。除了英國的大西部鐵路（Great
Western Railway）是使用寬軌之外，英國的主要鐵路都是標準軌。大西部鐵路亦於1892年改成標準軌。

有關4呎8½吋的軌距的來源，有人認為是古羅馬的戰車輪距。\[2\]但知名謠言破解網站snopes則認為戰車輪距之說為假。\[3\]

實際上，標準軌距的確定僅僅是設計和實踐綜合考量得出的結果。[喬治·史蒂芬生的火箭號是採用](../Page/喬治·史蒂芬生.md "wikilink")5呎4¾吋長的車軸（採取這個長度的車軸是因為車寬不能超過當時貨運馬車的7呎寬度，以便兼容現成的隧道），以及厚度為8¼吋的動輪。除去1吋的輪緣寬度，理論最小軌距為4呎7¾吋。\[4\]最早期設計的軌距維持在4呎8吋，其中史提芬生預留出了¾吋寬度來避免車輪和軌道卡死。但早期火車並沒有差速裝置和轉向架，實際運行中出現了噪音過大和輪軌磨損的問題，隨後史蒂芬生將軌距拓寬了½吋（相較重新設計機車，改變軌距相對簡單的多），解決了問題。鑑於史蒂芬生的成功，以及該軌距的高度兼容性，皇家計量局決定將4呎8½吋作為標準軌距。

## 各地軌距

[Rail_gauge.svg](https://zh.wikipedia.org/wiki/File:Rail_gauge.svg "fig:Rail_gauge.svg")

### 亞洲

#### 中国大陸

[WheelChanging.jpg](https://zh.wikipedia.org/wiki/File:WheelChanging.jpg "fig:WheelChanging.jpg")
[中國大陸现在的鐵路使用标准轨](../Page/中國大陸.md "wikilink")，宽轨与窄轨仅有极少量运用。

中國早期的鐵路由[英國及](../Page/英國.md "wikilink")[比利時工程師承建](../Page/比利時.md "wikilink")，因此中国境内大部分铁路之軌距沿用英國標準，為標準軌。但在民国时期由于未能推行统一标准，出现了各种轨距的铁路。

[中東鐵路曾經使用過](../Page/中東鐵路.md "wikilink")1524毫米的寬軌。[山西的](../Page/山西.md "wikilink")[同蒲鐵路和](../Page/同蒲鐵路.md "wikilink")[正太鐵路曾用過](../Page/正太鐵路.md "wikilink")1000毫米（已經改造至標準軌），[雲南的](../Page/雲南.md "wikilink")[昆河鐵路仍為米軌](../Page/昆河鐵路.md "wikilink")（1000毫米，合1米，故稱）。[南京六合区](../Page/南京.md "wikilink")[冶山铁路](../Page/冶山铁路.md "wikilink")，[河南许昌](../Page/河南.md "wikilink")－周口郸城[许郸铁路](../Page/许郸铁路.md "wikilink")，[四川犍为嘉阳矿区铁路为](../Page/嘉阳小火车及芭石窄轨铁路.md "wikilink")762毫米。在[满洲国时期](../Page/满洲国.md "wikilink")，[东北地区由日本建设了不少使用窄轨的铁路](../Page/东北.md "wikilink")。

中國大陸現在使用的是軌距為1435毫米標準軌，并逐渐将其他轨距的铁路转换为标准轨，至2003年，中国大陸境内正式营业的铁路中，尚存宽轨9.4公里，窄轨600多公里。\[5\]

#### 香港

[港鐵系統中](../Page/港鐵.md "wikilink")，在合併前屬於[九廣鐵路的](../Page/九廣鐵路.md "wikilink")[東鐵綫](../Page/東鐵綫.md "wikilink")（包括未來通車的[沙中綫](../Page/沙中綫.md "wikilink")[過海段](../Page/東鐵綫過海延綫.md "wikilink")）、[西鐵綫](../Page/西鐵綫.md "wikilink")、[馬鞍山綫](../Page/馬鞍山綫.md "wikilink")（包括未來通車的[屯馬綫](../Page/屯馬綫.md "wikilink")）及[輕鐵均使用](../Page/香港輕鐵.md "wikilink")1435毫米的標準軌，而在合併前屬於[香港地鐵的](../Page/香港地鐵.md "wikilink")[觀塘綫](../Page/觀塘綫.md "wikilink")、[荃灣綫](../Page/荃灣綫.md "wikilink")、[港島綫](../Page/港島綫.md "wikilink")、[東涌綫](../Page/東涌綫.md "wikilink")、[機場快綫及](../Page/機場快綫.md "wikilink")[將軍澳綫則用稍窄的](../Page/將軍澳綫.md "wikilink")1432毫米（假窄軌，理論上可與標準軌（1435毫米）相通），合併後新建的[南港島綫則使用](../Page/南港島綫.md "wikilink")1435毫米的標準軌。

[香港電車則使用](../Page/香港電車.md "wikilink")1067毫米窄軌。而[山頂纜車則採用](../Page/山頂纜車.md "wikilink")1520毫米的寬軌。

#### 日本

[日本的鐵路軌距主要為](../Page/日本.md "wikilink")1067毫米（3呎6吋）。但也有部份鐵道使用762毫米、1372毫米（[京王電鐵](../Page/京王電鐵.md "wikilink")、[東急世田谷線](../Page/東急世田谷線.md "wikilink")、[函館市電](../Page/函館市電.md "wikilink")、東京[都電及](../Page/東京都電車.md "wikilink")[都營新宿線](../Page/都營新宿線.md "wikilink")）和1435毫米（[京成電鐵](../Page/京成電鐵.md "wikilink")、都營[淺草線](../Page/淺草線.md "wikilink")、[京濱急行電鐵](../Page/京濱急行電鐵.md "wikilink")、東京地下鐵[銀座線](../Page/銀座線.md "wikilink")、[丸之內線](../Page/丸之內線.md "wikilink")）。早期在[樺太軍用輕便鐵道也使用](../Page/庫頁島.md "wikilink")600毫米軌距。在建造[高速鐵路](../Page/高速鐵路.md "wikilink")（[新幹線](../Page/新幹線.md "wikilink")）時則統一選用1435毫米為標準軌，以提高車輛行駛的穩定性，但這樣使高速鐵路列車不能以原有的路軌行駛，所以現在正在開發[軌距可變的鐵路車輛](../Page/軌距可變.md "wikilink")。

#### 臺灣

[臺灣在](../Page/臺灣.md "wikilink")[清治時期修築](../Page/臺灣清治時期.md "wikilink")「[臺灣鐵路](../Page/臺灣鐵路_\(清朝\).md "wikilink")」時，由英籍工程師建議下選用1067毫米，正巧與日本軌距相同。1895年[乙未戰爭時](../Page/乙未戰爭.md "wikilink")，日本接收臺灣，同時將火車頭運抵，隨即作為軍事用途行駛。[日治台灣時代結束後軌距亦未變動](../Page/日治台灣.md "wikilink")，並陸續完成[西部鐵路網](../Page/西部幹線.md "wikilink")。

另一方面，由於地形及運量的因素，但同樣由[鐵道部興建的](../Page/臺灣總督府交通局鐵道部.md "wikilink")[花東線](../Page/臺東線.md "wikilink")，與產業鐵道（[糖業鐵路及](../Page/糖業鐵路.md "wikilink")[阿里山森林鐵路等](../Page/阿里山森林鐵路.md "wikilink")）採用762毫米規格[輕便鐵道](../Page/輕便鐵道.md "wikilink")\[6\]。

1958年，[中華民國政府於](../Page/中華民國政府.md "wikilink")《[鐵路法](../Page/s:鐵路法_\(中華民國\).md "wikilink")》內規定以[標準軌](../Page/標準軌.md "wikilink")1435毫米為國家標準。但臺灣早期的鐵路網因繼承日治時期规划，絕大部份都是1067毫米或762毫米軌距，故亦加註「有特別情事，經[交通部核准者](../Page/中華民國交通部.md "wikilink")，不在此限」。1970年代臺東線配合[北迴線興建](../Page/北迴線.md "wikilink")，拓寬成與西部相同的1067毫米\[7\]。也未採1435毫米的軌距；一直到[臺灣高速鐵路](../Page/臺灣高速鐵路.md "wikilink")、[臺北捷運](../Page/臺北捷運.md "wikilink")、[高雄捷運](../Page/高雄捷運.md "wikilink")、[臺中捷運及](../Page/臺中捷運.md "wikilink")[桃園捷運](../Page/桃園捷運.md "wikilink")，台灣才陸續出現標準軌距鐵道。

#### 印尼

[印尼軌距主要是](../Page/印尼.md "wikilink")1067毫米。因此可以使用二手的JR日本列車運行。

#### 菲律賓

[菲律賓軌距主要是](../Page/菲律賓.md "wikilink")1067毫米，而[馬尼拉輕軌及](../Page/馬尼拉輕軌.md "wikilink")[馬尼拉捷運則採用標準軌](../Page/馬尼拉捷運.md "wikilink")。

#### 東盟

[東南亞國家](../Page/東南亞.md "wikilink")，包括[越南](../Page/越南.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[泰國](../Page/泰國.md "wikilink")（[曼谷大眾運輸系統](../Page/曼谷大眾運輸系統.md "wikilink")、[曼谷地鐵與](../Page/曼谷地鐵.md "wikilink")[蘇凡納布機場捷運則採用標準軌](../Page/蘇凡納布機場捷運.md "wikilink")）、[緬甸及](../Page/緬甸.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")（[吉隆坡機場快鐵和](../Page/吉隆坡機場快鐵.md "wikilink")[巴生河流域輕快鐵系統為標準軌](../Page/巴生河流域輕快鐵系統.md "wikilink")）以米軌（1000毫米）鐵路為主。有指越南河內至接壤[中國邊境之同登之間鋪設了標準及米軌雙軌距鐵路](../Page/中華人民共和國.md "wikilink")。計劃中連接[新加坡及中國的](../Page/新加坡.md "wikilink")[泛亞鐵路會是標準軌](../Page/泛亞鐵路.md "wikilink")，或標準及米軌雙軌距。

#### 南亞

[南亞國家的](../Page/南亞.md "wikilink")[印度](../Page/印度.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[孟加拉及](../Page/孟加拉.md "wikilink")[斯里蘭卡的鐵路軌距不一](../Page/斯里蘭卡.md "wikilink")，但多數為1676毫米。印度正在將所有鐵路軌距改成1676毫米。

#### 阿富汗

[阿富汗有個相當特殊的情況](../Page/阿富汗.md "wikilink")，因為它在靠近亞洲中央的位置，並且實際上沒有鐵路。如果它決定修築鐵路，在其接壤的國家之中則有三種不同規格的軌距，不管選擇其中任何一種規格，都會使情況變得複雜。位於阿富汗西部的[伊朗](../Page/伊朗.md "wikilink")、東部的[中國都使用標準軌](../Page/中國.md "wikilink")；阿富汗南部的[巴基斯坦使用](../Page/巴基斯坦.md "wikilink")1676毫米的寬軌；而阿富汗北部的[土庫曼](../Page/土庫曼.md "wikilink")、[烏茲別克斯坦和](../Page/烏茲別克斯坦.md "wikilink")[塔吉克斯坦則是用蘇聯時期的](../Page/塔吉克斯坦.md "wikilink")1520毫米規格。

#### 韓國

[韓國使用](../Page/韓國.md "wikilink")1435毫米標準軌，在[京義線修復後列車可與朝鲜互通](../Page/京義線.md "wikilink")。

#### 朝鲜

[朝鲜使用](../Page/朝鲜.md "wikilink")1435毫米標準軌，列車可與韓國及中國直接互通，因俄羅斯使用寬軌，在國界需更換轉向架後才能繼續行駛。

### 美洲

#### 北美

[美國及](../Page/美國.md "wikilink")[加拿大最初亦使用不同的軌寬](../Page/加拿大.md "wikilink")，後來加拿大追隨英國採取標準軌。美國北部的鐵路因為最初多是從英國入口器材，故亦多為標準軌。美國南部鐵路曾以寬軌為主。[南北內戰之後](../Page/美国南北战争.md "wikilink")，南部的鐵路亦逐漸改成標準軌距。

#### 阿根廷

[阿根廷的鐵路軌距主要為](../Page/阿根廷.md "wikilink")1676毫米（其中和為標準軌）。

#### 智利

[智利的鐵路軌距為](../Page/智利.md "wikilink")1676毫米。

### 歐洲

歐洲大部分國家都是使用標準軌（1435毫米），除了：

  - [波蘭的](../Page/波蘭.md "wikilink")（又名「寬軌冶金業鐵路線」）和，其軌距1520毫米，以便從[烏克蘭進口煤和鐵](../Page/烏克蘭.md "wikilink")。
  - [挪威的](../Page/挪威.md "wikilink")之軌距為1067毫米。

#### 愛爾蘭

[愛爾蘭的軌距是](../Page/愛爾蘭.md "wikilink")1600毫米，但[都柏林现代轻轨系统採用標準軌](../Page/都柏林现代轻轨系统.md "wikilink")。

#### 英國

  - [北愛爾蘭的軌距是](../Page/北愛爾蘭.md "wikilink")1600毫米。
  - 除了北愛之外，英倫三島和曾被英國殖民的地區(如美國、加拿大、紐西蘭、印度等，澳洲除外)都是採用標準軌距。在英國鐵路專家[喬治·史蒂文生的倡導之下](../Page/喬治·史蒂文生.md "wikilink")，英倫三島的鐵路(包含蘇格蘭、威爾斯)都是採用標準軌距。

#### 西班牙

[西班牙的正式標準是](../Page/西班牙.md "wikilink")1674毫米，為了使列車在其與[葡萄牙間跨國運轉](../Page/葡萄牙.md "wikilink")，而開發了變距列車。此外，西班牙正進行改軌工程，將路軌改成標準軌距。

現時標準軌距只見於西班牙之[巴塞隆拿地鐵](../Page/巴塞隆拿地鐵.md "wikilink")[2至](../Page/巴塞隆拿地鐵2號線.md "wikilink")及之、、S5和S55線。另外，西班牙的高鐵採用標準軌。

#### 葡萄牙

[葡萄牙軌距為](../Page/葡萄牙.md "wikilink")1665毫米。

#### 芬蘭

[芬蘭在](../Page/芬蘭.md "wikilink")19世紀是俄國轄下的[大公國](../Page/大公國.md "wikilink")，使用1524毫米（5英尺）軌距；後來為了方便與鄰國[瑞典進行順暢的鐵路運輸](../Page/瑞典.md "wikilink")，芬蘭與瑞典陸上邊界會鋪設雙軌距線路，以及[鐵路渡輪的甲板也會鋪設雙軌距線路](../Page/鐵路渡輪.md "wikilink")。

#### 前蘇聯國家

19世紀的[俄羅斯選用](../Page/俄羅斯.md "wikilink")1524毫米的（5呎）寬軌，一般認為是出於軍事考慮，避免入侵的軍隊可以使用它的鐵路運輸系統。俄羅斯和屬於前[蘇聯的國家](../Page/蘇聯.md "wikilink")，以及[蒙古](../Page/蒙古.md "wikilink")、[芬蘭都是採用俄國的](../Page/芬蘭.md "wikilink")1520毫米軌距。這比1524毫米窄4毫米，有時兩者的車輛可以互換，而[南薩哈林州](../Page/南薩哈林州.md "wikilink")（[庫頁島](../Page/庫頁島.md "wikilink")）則是採用當時[日本占領南庫頁島時期的](../Page/樺太廳.md "wikilink")1067毫米（3呎6吋）軌距，標準軌（1435毫米）只用於和[加里寧格勒境內與](../Page/加里寧格勒.md "wikilink")[波蘭接壤的路段](../Page/波蘭.md "wikilink")。近些年歐盟支持的"波羅的海鐵路計畫"將把波羅的海三國的鐵路改為1435毫米的標準軌.

### 大洋洲

[Rail_gauge_Australia.png](https://zh.wikipedia.org/wiki/File:Rail_gauge_Australia.png "fig:Rail_gauge_Australia.png")

#### 新西蘭

紐西蘭的鐵路為了跨越中部山區地形，採用1067毫米的軌距。為了克服這個地形，需要複雜的工程，例如著名的。在不到4000公里的軌道就有1787座橋梁和150個隧道。這條軌道大約有500公里是電氣化的。

#### 澳大利亞

[澳洲本來採用標準軌](../Page/澳洲.md "wikilink")，後來因為某些原因而在[維多利亞省及](../Page/維多利亞省.md "wikilink")[南澳州出現了](../Page/南澳州.md "wikilink")1600毫米（5呎3吋）的軌距。部分地方亦有1067毫米（3呎6吋）的路軌。[昆士蘭鐵路在建立之初](../Page/昆士蘭鐵路.md "wikilink")，便使用1067毫米（3呎6吋）的窄軌，。

### 非洲

[南非](../Page/南非.md "wikilink")（[豪登列車除外](../Page/豪登列車.md "wikilink")，軌距為1435毫米）及[安哥拉](../Page/安哥拉.md "wikilink")、[博茨瓦納](../Page/博茨瓦納.md "wikilink")、[剛果](../Page/剛果.md "wikilink")、[加納](../Page/加納.md "wikilink")、[贊比亞](../Page/贊比亞.md "wikilink")（包括[坦贊鐵路之](../Page/坦贊鐵路.md "wikilink")[坦桑尼亞段](../Page/坦桑尼亞.md "wikilink")）等國多數使用1067毫米軌距。

## 最佳軌距的爭論

[Different_gauges_in_China_Railway_Museum.jpg](https://zh.wikipedia.org/wiki/File:Different_gauges_in_China_Railway_Museum.jpg "fig:Different_gauges_in_China_Railway_Museum.jpg")\]\]

自19世紀鐵路開始出現，即有爭論何種軌距最佳。從現代角度看，寬軌或窄軌在性能上沒有十分明顯的優點：世上最重的貨車可以在美國及澳洲的標準軌上行走。寬軌不一定可以載重更多。澳洲昆士蘭及南非的窄軌（1067毫米）鐵路上的列車依然是十分重。窄軌不一定載重較少。窄軌鐵路亦可以建成達到標準軌一樣的負載量。

高速鐵路都是採用標準軌，寬軌不一定較快。在採用窄軌的日本，列車也能以160km/h的高速行駛，窄軌不代表較慢。

建造標準軌的輕便鐵路與窄軌鐵路價格相差並不大。只有軌距低於3呎的窄軌鐵路的建造成本才會稍低於標準軌。但這類軌距的運載能力有限，通常只會在運載量有限的登山鐵路使用。

以此看來，選擇寬軌或窄軌不一定比選擇標準軌節省金錢；但卻失去了與標準軌之間的兼容性。但在某些特殊的地理條件下，採用窄軌有助降低工程的難度和成本。

## 未來

隨着各國開始尋求擴充、統合各地的鐵路網絡，軌距在未來可能會進一步標準化。

[澳洲和](../Page/澳洲.md "wikilink")[印度都在進行統一國內鐵路軌距的工程](../Page/印度.md "wikilink")；而[歐盟也計劃讓各成員國的鐵路系統可以互用](../Page/歐盟.md "wikilink")，涉及的工作包括軌距、[訊號系統和](../Page/鐵路訊號.md "wikilink")[供電系統的標準化](../Page/鐵路供電.md "wikilink")。現時[波羅的海三國](../Page/波羅的海三國.md "wikilink")（[立陶宛](../Page/立陶宛.md "wikilink")、[拉脫維亞和](../Page/拉脫維亞.md "wikilink")[愛沙尼亞](../Page/愛沙尼亞.md "wikilink")）已獲得歐盟的資助，把前苏联时代遗留的1520毫米闊軌轉換成標準軌；[西班牙和](../Page/西班牙.md "wikilink")[葡萄牙也在建造連接國內各大城市與](../Page/葡萄牙.md "wikilink")[法國之間的](../Page/法國.md "wikilink")[高速鐵路中獲得歐盟的援助](../Page/高速鐵路.md "wikilink")。歐盟亦計劃提昇西班牙與葡萄牙連接其他歐洲國家的[貨運鐵路](../Page/貨運鐵路.md "wikilink")。

### [高速鐵路](../Page/高速鐵路.md "wikilink")

几乎所有的[高速鐵路](../Page/高速鐵路.md "wikilink")，包括在使用窄軌的[日本](../Page/日本.md "wikilink")、[台灣](../Page/台灣.md "wikilink")，以及使用寬軌的[西班牙等地](../Page/西班牙.md "wikilink")，都是採用標準軌興建的（唯一的例外是[俄羅斯和](../Page/俄羅斯.md "wikilink")[芬蘭採用的](../Page/芬蘭.md "wikilink")5英尺闊軌高速鐵路）。這種標準使一些國家開始為當地原有的非標準軌鐵路進行軌距轉換，使列車能在高速鐵路網絡與現行鐵路網絡之間行走。

### 礦區鐵路

雖然礦區鐵路較少與外面的鐵路系統連接，但現時礦區亦有使用標準軌的趨向，以方便其使用現成的鐵路設備（例如[負載量更大的車輛](../Page/負載量.md "wikilink")）。

### 計畫

  - 在[聯合國經濟和社會委員會的](../Page/聯合國.md "wikilink")[亞洲太平洋計畫中](../Page/联合国亚洲及太平洋经济社会委员会.md "wikilink")，有一項是關於把歐洲和太平洋連接起來的「[亞洲鐵路系統](../Page/亞洲鐵路.md "wikilink")」，當中包括三條走線：北方從歐洲到[朝鮮半島](../Page/朝鮮半島.md "wikilink")，南線從歐洲到[東南亞](../Page/東南亞.md "wikilink")，並設一條南北向走廊從[北歐到](../Page/北歐.md "wikilink")[波斯灣](../Page/波斯灣.md "wikilink")。三條走線都會在橫越[亞洲時使用至少兩條軌距相異的鐵路](../Page/亞洲.md "wikilink")。目前計畫並沒有涉及轉換軌距，而是在軌距相異鐵路的交匯處設置機械設施，以便把[貨櫃在行走不同軌距的列車之間移動](../Page/貨櫃.md "wikilink")。

<!-- end list -->

  - [肯雅](../Page/肯雅.md "wikilink")、[烏干達和](../Page/烏干達.md "wikilink")[蘇丹在](../Page/蘇丹.md "wikilink")2004年10月提出了一項使用[標準軌建造](../Page/標準軌.md "wikilink")[電氣化](../Page/電氣化鐵路.md "wikilink")[高速鐵路的計畫](../Page/高速鐵路.md "wikilink")。雖然肯、烏兩國及蘇丹的鐵路網絡分別採用1000毫米闊及1067毫米闊的窄軌，但它們由於建成年代早，彎位多且負載力低，需要進行改建，加上北方的[埃及是使用標準軌的](../Page/埃及.md "wikilink")，因此統一使用標準軌距建造新鐵路被認為是理所當然。

<!-- end list -->

  - 根據某2007年的計畫，數條來自[剛果](../Page/剛果.md "wikilink")、[坦桑尼亞和](../Page/坦桑尼亞.md "wikilink")[盧旺達](../Page/盧旺達.md "wikilink")、軌距相異（分別為1000毫米及1067毫米）的鐵路將於盧旺達的一個樞紐地點交匯。

## 参见

  - [轨距列表](../Page/轨距列表.md "wikilink")
  - [膠輪路軌系統](../Page/膠輪路軌系統.md "wikilink")
  - [道路通行方向](../Page/道路通行方向.md "wikilink")

## 参考文献

## 外部連結

  - [科普鐵路館](http://www.kepu.net.cn/gb/technology/railway/railway_line/200401160018.html)

{{-}}

[Category:铁路轨道](../Category/铁路轨道.md "wikilink")

1.
2.

3.  <http://www.snopes.com/history/american/gauge.asp>

4.  Science Museum Group. Sectioned conjectural model of 'Rocket' steam
    locomotive. 1909-3. Science Museum Group Collection Online. Accessed
    September 13, 2017.
    <https://collection.sciencemuseum.org.uk/objects/co26970>.

5.  [亲历云南窄轨铁路](http://www.ce.cn/ztpd/hqmt/gnmt/lwdfzk/more/200504/12/t20050412_3581018.shtml)

6.
7.