**鼬鯰科**，學名*Heptapteridae*，是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鯰形目的一個科](../Page/鯰形目.md "wikilink")。

## 分類

**鼬鯰科**其下分有24屬：

### 細棘鯰屬（*Acentronichthys*）

  - [巴西細棘鯰](../Page/巴西細棘鯰.md "wikilink")（*Acentronichthys leptos*）

### 短油鯰屬（*Brachyglanis*）

  - [韁短油鯰](../Page/韁短油鯰.md "wikilink")（*Brachyglanis frenatus*）
  - [麥氏短油鯰](../Page/麥氏短油鯰.md "wikilink")（*Brachyglanis magoi*）
  - [黑短油鯰](../Page/黑短油鯰.md "wikilink")（*Brachyglanis melas*）
  - [小眼短油鯰](../Page/小眼短油鯰.md "wikilink")（*Brachyglanis microphthalmus*）
  - [巴西短油鯰](../Page/巴西短油鯰.md "wikilink")（*Brachyglanis nocturnus*）
  - [禿短油鯰](../Page/禿短油鯰.md "wikilink")（*Brachyglanis phalacra*）

### 短枝鯰屬（*Brachyrhamdia*）

  - [異腹短枝鯰](../Page/異腹短枝鯰.md "wikilink")（*Brachyrhamdia heteropleura*）
  - [仿短枝鯰](../Page/仿短枝鯰.md "wikilink")（*Brachyrhamdia imitator*）
  - [馬撒短枝鯰](../Page/馬撒短枝鯰.md "wikilink")（*Brachyrhamdia marthae*）
  - [米斯短枝鯰](../Page/米斯短枝鯰.md "wikilink")（*Brachyrhamdia meesi*）
  - [拉氏短枝鯰](../Page/拉氏短枝鯰.md "wikilink")（*Brachyrhamdia rambarrani*）

### 鯨油鯰屬（*Cetopsorhamdia*）

  - [博奎爾鯨油鯰](../Page/博奎爾鯨油鯰.md "wikilink")（*Cetopsorhamdia boquillae*）
  - [絲條鯨油鯰](../Page/絲條鯨油鯰.md "wikilink")（*Cetopsorhamdia filamentosa*）
  - [伊氏鯨油鯰](../Page/伊氏鯨油鯰.md "wikilink")（*Cetopsorhamdia iheringi*）
  - [巴西鯨油鯰](../Page/巴西鯨油鯰.md "wikilink")（*Cetopsorhamdia insidiosa*）
  - [莫氏鯨油鯰](../Page/莫氏鯨油鯰.md "wikilink")（*Cetopsorhamdia molinae*）
  - [大鼻鯨油鯰](../Page/大鼻鯨油鯰.md "wikilink")（*Cetopsorhamdia nasus*）
  - [高山鯨油鯰](../Page/高山鯨油鯰.md "wikilink")（*Cetopsorhamdia orinoco*）
  - [華秀鯨油鯰](../Page/華秀鯨油鯰.md "wikilink")（*Cetopsorhamdia phantasia*）
  - [皮克爾氏鯨油鯰](../Page/皮克爾氏鯨油鯰.md "wikilink")（*Cetopsorhamdia picklei*）

### 寬盔鯰屬（*Chasmocranus*）

  - [短線寬盔鯰](../Page/短線寬盔鯰.md "wikilink")（*Chasmocranus brachynemus*）
  - [短身寬盔鯰](../Page/短身寬盔鯰.md "wikilink")（*Chasmocranus brevior*）
  - [委內瑞拉寬盔鯰](../Page/委內瑞拉寬盔鯰.md "wikilink")（*Chasmocranus chimantanus*）
  - [長體寬盔鯰](../Page/長體寬盔鯰.md "wikilink")（*Chasmocranus longior*）
  - [洛氏寬盔鯰](../Page/洛氏寬盔鯰.md "wikilink")（*Chasmocranus lopezi*）
  - [秘魯寬盔鯰](../Page/秘魯寬盔鯰.md "wikilink")（*Chasmocranus peruanus*）
  - [四帶寬盔鯰](../Page/四帶寬盔鯰.md "wikilink")（*Chasmocranus quadrizonatus*）
  - [玫瑰寬盔鯰](../Page/玫瑰寬盔鯰.md "wikilink")（*Chasmocranus rosae*）
  - [蘇利南寬盔鯰](../Page/蘇利南寬盔鯰.md "wikilink")（*Chasmocranus surinamensis*）
  - [截吻寬盔鯰](../Page/截吻寬盔鯰.md "wikilink")（*Chasmocranus truncatorostris*）

### 劍鰭鯰屬（*Gladioglanis*）

  - [多椎劍鰭鯰](../Page/多椎劍鰭鯰.md "wikilink")（*Gladioglanis anacanthus*）
  - [南美劍鰭鯰](../Page/南美劍鰭鯰.md "wikilink")（*Gladioglanis conquistador*）
  - [馬氏劍鰭鯰](../Page/馬氏劍鰭鯰.md "wikilink")（*Gladioglanis machadoi*）

### 戈迪拉鯰屬（*Goeldiella*）

  - [戈迪拉鯰](../Page/戈迪拉鯰.md "wikilink")（*Goeldiella eques*）

### 鼬鯰屬（*Heptapterus*）

  - [布氏鼬鯰](../Page/布氏鼬鯰.md "wikilink")（*Heptapterus bleekeri*）
  - [裂鰭鼬鯰](../Page/裂鰭鼬鯰.md "wikilink")（*Heptapterus fissipinnis*）
  - [阿根廷鼬鯰](../Page/阿根廷鼬鯰.md "wikilink")（*Heptapterus mbya*）
  - [多輻鼬鯰](../Page/多輻鼬鯰.md "wikilink")（*Heptapterus multiradiatus*）
  - [巴西鼬鯰](../Page/巴西鼬鯰.md "wikilink")（*Heptapterus mustelinus*）
  - [飾頭鼬鯰](../Page/飾頭鼬鯰.md "wikilink")（*Heptapterus ornaticeps*）
  - [蛇形鼬鯰](../Page/蛇形鼬鯰.md "wikilink")（*Heptapterus qenqo*）
  - [斯氏鼬鯰](../Page/斯氏鼬鯰.md "wikilink")（*Heptapterus stewarti*）
  - [合鰭鼬鯰](../Page/合鰭鼬鯰.md "wikilink")（*Heptapterus sympterygium*）
  - [蘇里南鼬鯰](../Page/蘇里南鼬鯰.md "wikilink")（*Heptapterus tapanahoniensis*）
  - [薄鼬鯰](../Page/薄鼬鯰.md "wikilink")（*Heptapterus tenuis*）

### 緣鰭鯰屬（*Horiomyzon*）

  - [亞馬遜河緣鰭鯰](../Page/亞馬遜河緣鰭鯰.md "wikilink")（*Horiomyzon retropinnatus*）

### 羽油鯰屬（*Imparfinis*）

  - [博氏羽油鯰](../Page/博氏羽油鯰.md "wikilink")（*Imparfinis borodini*）
  - [南美羽油鯰](../Page/南美羽油鯰.md "wikilink")（*Imparfinis cochabambae*）
  - [斑羽油鯰](../Page/斑羽油鯰.md "wikilink")（*Imparfinis guttatus*）
  - [哈氏羽油鯰](../Page/哈氏羽油鯰.md "wikilink")（*Imparfinis hasemani*）
  - [何氏羽油鯰](../Page/何氏羽油鯰.md "wikilink")（*Imparfinis hollandi*）
  - [線羽油鯰](../Page/線羽油鯰.md "wikilink")（*Imparfinis lineatus*）
  - [長尾羽油鯰](../Page/長尾羽油鯰.md "wikilink")（*Imparfinis longicaudus*）
  - [小眼羽油鯰](../Page/小眼羽油鯰.md "wikilink")（*Imparfinis microps*）
  - [微羽油鯰](../Page/微羽油鯰.md "wikilink")（*Imparfinis minutus*）
  - [米氏羽油鯰](../Page/米氏羽油鯰.md "wikilink")（*Imparfinis mirini*）
  - [阿根廷羽油鯰](../Page/阿根廷羽油鯰.md "wikilink")（*Imparfinis mishky*）
  - [線手羽油鯰](../Page/線手羽油鯰.md "wikilink")（*Imparfinis nemacheir*）
  - [小羽油鯰](../Page/小羽油鯰.md "wikilink")（*Imparfinis parvus*）
  - [皮氏羽油鯰](../Page/皮氏羽油鯰.md "wikilink")（*Imparfinis pijpersi*）
  - [胡椒羽油鯰](../Page/胡椒羽油鯰.md "wikilink")（*Imparfinis piperatus*）
  - [鋸刺羽油鯰](../Page/鋸刺羽油鯰.md "wikilink")（*Imparfinis pristos*）
  - [擬鰍羽油鯰](../Page/擬鰍羽油鯰.md "wikilink")（*Imparfinis pseudonemacheir*）
  - [舒氏羽油鯰](../Page/舒氏羽油鯰.md "wikilink")（*Imparfinis schubarti*）
  - [斯氏羽油鯰](../Page/斯氏羽油鯰.md "wikilink")（*Imparfinis spurrellii*）
  - [背斑羽油鯰](../Page/背斑羽油鯰.md "wikilink")（*Imparfinis stictonotus*）
  - [長身羽油鯰](../Page/長身羽油鯰.md "wikilink")（*Imparfinis timana*）
  - [烏絲馬羽油鯰](../Page/烏絲馬羽油鯰.md "wikilink")（''Imparfinis usmai ''）

### 細盔鯰屬（*Leptorhamdia*）

  - [南美細盔鯰](../Page/南美細盔鯰.md "wikilink")（*Leptorhamdia essequibensis*）
  - [斑細盔鯰](../Page/斑細盔鯰.md "wikilink")（*Leptorhamdia marmorata*）
  - [沙氏細盔鯰](../Page/沙氏細盔鯰.md "wikilink")（*Leptorhamdia schultzi*）

### 鞭鯰屬（*Mastiglanis*）

  - [河神鞭鯰](../Page/河神鞭鯰.md "wikilink")（*Mastiglanis asopos*）

### 鼠油鯰屬（*Myoglanis*）

  - [長體鼠油鯰](../Page/長體鼠油鯰.md "wikilink")（*Myoglanis aspredinoides*）
  - [凱氏鼠油鯰](../Page/凱氏鼠油鯰.md "wikilink")（*Myoglanis koepckei*）
  - [南美鼠油鯰](../Page/南美鼠油鯰.md "wikilink")（*Myoglanis potaroensis*）

### 矮鼬鯰屬（*Nannoglanis*）

  - [條紋矮鼬鯰](../Page/條紋矮鼬鯰.md "wikilink")（*Nannoglanis fasciatus*）

### 線油鯰屬（*Nemuroglanis*）

  - [叉尾線油鯰](../Page/叉尾線油鯰.md "wikilink")（*Nemuroglanis furcatus*）
  - [尖頭線油鯰](../Page/尖頭線油鯰.md "wikilink")（*Nemuroglanis lanceolatus*）
  - [馬氏線油鯰](../Page/馬氏線油鯰.md "wikilink")（*Nemuroglanis mariai*）
  - [巴拿馬線油鯰](../Page/巴拿馬線油鯰.md "wikilink")（*Nemuroglanis panamensis*）
  - [少輻線油鯰](../Page/少輻線油鯰.md "wikilink")（*Nemuroglanis pauciradiatus*）

### 環鼬鯰屬（*Pariolius*）

  - [環鼬鯰](../Page/環鼬鯰.md "wikilink")（*Pariolius armillatus*）

### 準鼬鯰屬（*Phenacorhamdia*）

  - [委內瑞拉準鼬鯰](../Page/委內瑞拉準鼬鯰.md "wikilink")（*Phenacorhamdia anisura*）
  - [玻利維亞準鼬鯰](../Page/玻利維亞準鼬鯰.md "wikilink")（*Phenacorhamdia boliviana*）
  - [霍氏準鼬鯰](../Page/霍氏準鼬鯰.md "wikilink")（*Phenacorhamdia hoehnei*）
  - [馬卡倫準鼬鯰](../Page/馬卡倫準鼬鯰.md "wikilink")（*Phenacorhamdia
    macarenensis*）
  - [黑線準鼬鯰](../Page/黑線準鼬鯰.md "wikilink")（*Phenacorhamdia nigrolineata*）
  - [普氏準鼬鯰](../Page/普氏準鼬鯰.md "wikilink")（*Phenacorhamdia provenzanoi*）
  - [夢準鼬鯰](../Page/夢準鼬鯰.md "wikilink")（*Phenacorhamdia somnians*）
  - [塔氏準鼬鯰](../Page/塔氏準鼬鯰.md "wikilink")（*Phenacorhamdia taphorni*）
  - [暗色準鼬鯰](../Page/暗色準鼬鯰.md "wikilink")（*Phenacorhamdia tenebrosa*）
  - [單紋準鼬鯰](../Page/單紋準鼬鯰.md "wikilink")（*Phenacorhamdia unifasciata*）

### 溝油鯰屬（*Phreatobius*）

  - [南美溝油鯰](../Page/南美溝油鯰.md "wikilink")（*Phreatobius cisternarum*）
  - [大龍溝油鯰](../Page/大龍溝油鯰.md "wikilink")（*Phreatobius dracunculus*）
  - [秀美溝油鯰](../Page/秀美溝油鯰.md "wikilink")（*Phreatobius sanguijuela*）

### 小油鯰屬（*Pimelodella*）

  - [高鰭小油鯰](../Page/高鰭小油鯰.md "wikilink")（*Pimelodella altipinnis*）
  - [澳洲小油鯰](../Page/澳洲小油鯰.md "wikilink")（*Pimelodella australis*）
  - [艾氏小油鯰](../Page/艾氏小油鯰.md "wikilink")（*Pimelodella avanhandavae*）
  - [玻利維亞小油鯰](../Page/玻利維亞小油鯰.md "wikilink")（*Pimelodella boliviana*）
  - [博氏小油鯰](../Page/博氏小油鯰.md "wikilink")（*Pimelodella boschmai*）
  - [巴西小油鯰](../Page/巴西小油鯰.md "wikilink")（*Pimelodella brasiliensis*）
  - [短頭小油鯰](../Page/短頭小油鯰.md "wikilink")（*Pimelodella breviceps*）
  - [巴氏小油鯰](../Page/巴氏小油鯰.md "wikilink")（*Pimelodella buckleyi*）
  - [查氏小油鯰](../Page/查氏小油鯰.md "wikilink")（*Pimelodella chagresi*）
  - [蔡氏小油鯰](../Page/蔡氏小油鯰.md "wikilink")（*Pimelodella chaparae*）
  - [科奎達小油鯰](../Page/科奎達小油鯰.md "wikilink")（*Pimelodella conquetaensis*）
  - [小油鯰](../Page/小油鯰.md "wikilink")（*Pimelodella cristata*）
  - [克勞氏小油鯰](../Page/克勞氏小油鯰.md "wikilink")（*Pimelodella cruxenti*）
  - [藍點小油鯰](../Page/藍點小油鯰.md "wikilink")（*Pimelodella cyanostigma*）
  - [多氏小油鯰](../Page/多氏小油鯰.md "wikilink")（*Pimelodella dorseyi*）
  - [埃格小油鯰](../Page/埃格小油鯰.md "wikilink")（*Pimelodella eigenmanni*）
  - [埃氏小油鯰](../Page/埃氏小油鯰.md "wikilink")（*Pimelodella eigenmanniorum*）
  - [長身小油鯰](../Page/長身小油鯰.md "wikilink")（*Pimelodella elongata*）
  - [伊氏小油鯰](../Page/伊氏小油鯰.md "wikilink")（*Pimelodella enochi*）
  - [真紋小油鯰](../Page/真紋小油鯰.md "wikilink")（*Pimelodella eutaenia*）
  - [菲氏小油鯰](../Page/菲氏小油鯰.md "wikilink")（*Pimelodella figueroai*）
  - [格-{里}-氏小油鯰](../Page/格里氏小油鯰.md "wikilink")（*Pimelodella geryi*）
  - [細小油鯰](../Page/細小油鯰.md "wikilink")（*Pimelodella gracilis*）
  - [格里芬小油鯰](../Page/格里芬小油鯰.md "wikilink")（*Pimelodella griffini*）
  - [格賴斯小油鯰](../Page/格賴斯小油鯰.md "wikilink")（*Pimelodella grisea*）
  - [哈特小油鯰](../Page/哈特小油鯰.md "wikilink")（*Pimelodella harttii*）
  - [哈特韋小油鯰](../Page/哈特韋小油鯰.md "wikilink")（*Pimelodella hartwelli*）
  - [哈斯曼小油鯰](../Page/哈斯曼小油鯰.md "wikilink")（*Pimelodella hasemani*）
  - [豪斯小油鯰](../Page/豪斯小油鯰.md "wikilink")（*Pimelodella howesi*）
  - [山棲小油鯰](../Page/山棲小油鯰.md "wikilink")（*Pimelodella ignobilis*）
  - [貪婪小油鯰](../Page/貪婪小油鯰.md "wikilink")（*Pimelodella itapicuruensis*）
  - [克路氏油鯰](../Page/克路氏油鯰.md "wikilink")（*Pimelodella kronei*）
  - [側帶小油鯰](../Page/側帶小油鯰.md "wikilink")(*Pimelodella lateristriga*)
  - [側頭小油鯰](../Page/側頭小油鯰.md "wikilink")（*Pimelodella laticeps*）
  - [勞倫特小油鯰](../Page/勞倫特小油鯰.md "wikilink")（*Pimelodella laurenti*）
  - [細身小油鯰](../Page/細身小油鯰.md "wikilink")（*Pimelodella leptosoma*）
  - [林氏小油鯰](../Page/林氏小油鯰.md "wikilink")（*Pimelodella linami*）
  - [長鰭小油鯰](../Page/長鰭小油鯰.md "wikilink")（*Pimelodella longipinnis*）
  - [大頭小油鯰](../Page/大頭小油鯰.md "wikilink")（*Pimelodella macrocephala*）
  - [麥氏小油鯰](../Page/麥氏小油鯰.md "wikilink")（*Pimelodella macturki*）
  - [馬氏小油鯰](../Page/馬氏小油鯰.md "wikilink")（*Pimelodella martinezi*）
  - [米克氏小油鯰](../Page/米克氏小油鯰.md "wikilink")（*Pimelodella meeki*）
  - [大眼小油鯰](../Page/大眼小油鯰.md "wikilink")（*Pimelodella megalops*）
  - [大尾小油鯰](../Page/大尾小油鯰.md "wikilink")（*Pimelodella megalura*）
  - [米塔氏小油鯰](../Page/米塔氏小油鯰.md "wikilink")（*Pimelodella metae*）
  - [靜小油鯰](../Page/靜小油鯰.md "wikilink")（*Pimelodella modestus*）
  - [蒙大那小油鯰](../Page/蒙大那小油鯰.md "wikilink")（*Pimelodella montana*）
  - [黏小油鯰](../Page/黏小油鯰.md "wikilink")（*Pimelodella mucosa*）
  - [黑帶小油鯰](../Page/黑帶小油鯰.md "wikilink")（*Pimelodella nigrofasciata*）
  - [黑背小油鯰](../Page/黑背小油鯰.md "wikilink")(*Pimelodella notomelas*)
  - [毒刺小油鯰](../Page/毒刺小油鯰.md "wikilink")(*Pimelodella odynea*)
  - [杏眼小油鯰](../Page/杏眼小油鯰.md "wikilink")(*Pimelodella ophthalmica*)
  - [蒼白小油鯰](../Page/蒼白小油鯰.md "wikilink")(*Pimelodella pallida*)
  - [佩帕小油鯰](../Page/佩帕小油鯰.md "wikilink")（*Pimelodella papariae*）
  - [佩氏小油鯰](../Page/佩氏小油鯰.md "wikilink")（*Pimelodella pappenheimi*）
  - [帕氏小油鯰](../Page/帕氏小油鯰.md "wikilink")（*Pimelodella parnahybae*）
  - [微小油鯰](../Page/微小油鯰.md "wikilink")（*Pimelodella parva*）
  - [梳小油鯰](../Page/梳小油鯰.md "wikilink")（*Pimelodella pectinifer*）
  - [秘魯小油鯰](../Page/秘魯小油鯰.md "wikilink")（*Pimelodella peruana*）
  - [亞馬遜小油鯰](../Page/亞馬遜小油鯰.md "wikilink")（*Pimelodella peruensis*）
  - [高體小油鯰](../Page/高體小油鯰.md "wikilink")（*Pimelodella procera*）
  - [倫氏小油鯰](../Page/倫氏小油鯰.md "wikilink")（*Pimelodella rendahli*）
  - [雷氏小油鯰](../Page/雷氏小油鯰.md "wikilink")（*Pimelodella reyesi*）
  - [羅賓遜小油鯰](../Page/羅賓遜小油鯰.md "wikilink")（*Pimelodella robinsoni*）
  - [羅科小油鯰](../Page/羅科小油鯰.md "wikilink")（*Pimelodella roccae*）
  - [魯氏小油鯰](../Page/魯氏小油鯰.md "wikilink")（*Pimelodella rudolphi*）
  - [鋸小油鯰](../Page/鋸小油鯰.md "wikilink")（*Pimelodella serrata*）
  - [斯佩小油鯰](../Page/斯佩小油鯰.md "wikilink")（*Pimelodella spelaea*）
  - [斯氏小油鯰](../Page/斯氏小油鯰.md "wikilink")（*Pimelodella steindachneri*）
  - [喜鬥小油鯰](../Page/喜鬥小油鯰.md "wikilink")（*Pimelodella taeniophora*）
  - [紋鰭小油鯰](../Page/紋鰭小油鯰.md "wikilink")（*Pimelodella taenioptera*）
  - [塔氏小油鯰](../Page/塔氏小油鯰.md "wikilink")（*Pimelodella tapatapae*）
  - [麥穗小油鯰](../Page/麥穗小油鯰.md "wikilink")（*Pimelodella transitoria*）
  - [飾帶小油鯰](../Page/飾帶小油鯰.md "wikilink")（*Pimelodella vittata*）
  - [韋氏小油鯰](../Page/韋氏小油鯰.md "wikilink")（*Pimelodella wesselii*）
  - [威氏小油鯰](../Page/威氏小油鯰.md "wikilink")（*Pimelodella witmeri*）
  - [沃爾夫小油鯰](../Page/沃爾夫小油鯰.md "wikilink")（*Pimelodella wolfi*）
  - [尤桑小油鯰](../Page/尤桑小油鯰.md "wikilink")（*Pimelodella yuncensis*）

### 小雷氏鯰屬（*Rhamdella*）

  - [艾氏小雷氏鯰](../Page/艾氏小雷氏鯰.md "wikilink")（*Rhamdella aymarae*）
  - [凱氏小雷氏鯰](../Page/凱氏小雷氏鯰.md "wikilink")（*Rhamdella cainguae*）
  - [埃里小雷氏鯰](../Page/埃里小雷氏鯰.md "wikilink")（*Rhamdella eriarcha*）
  - [大頭小雷氏鯰](../Page/大頭小雷氏鯰.md "wikilink")（*Rhamdella exsudans*）
  - [詹尼小雷氏鯰](../Page/詹尼小雷氏鯰.md "wikilink")（*Rhamdella jenynsii*）
  - [巴西小雷氏鯰](../Page/巴西小雷氏鯰.md "wikilink")（*Rhamdella longiuscula*）
  - [高山小雷氏鯰](../Page/高山小雷氏鯰.md "wikilink")（*Rhamdella montana*）
  - [魯斯伯小雷氏鯰](../Page/魯斯伯小雷氏鯰.md "wikilink")（*Rhamdella rusbyi*）

### 雷氏鯰屬（*Rhamdia*）

  - [銀色雷氏鯰](../Page/銀色雷氏鯰.md "wikilink")（*Rhamdia argentina*）
  - [喜蔭雷氏鯰](../Page/喜蔭雷氏鯰.md "wikilink")（*Rhamdia enfurnada*）
  - [鼬雷氏鯰](../Page/鼬雷氏鯰.md "wikilink")（*Rhamdia foina*）
  - [瓜薩雷氏鯰](../Page/瓜薩雷氏鯰.md "wikilink")（*Rhamdia guasarensis*）
  - [矮雷氏鯰](../Page/矮雷氏鯰.md "wikilink")（*Rhamdia humilis*）
  - [亞馬遜河雷氏鯰](../Page/亞馬遜河雷氏鯰.md "wikilink")（*Rhamdia itacaiunas*）
  - [巴西雷氏鯰](../Page/巴西雷氏鯰.md "wikilink")（*Rhamdia jequitinhonha*）
  - [寬頭雷氏鯰](../Page/寬頭雷氏鯰.md "wikilink")（*Rhamdia laluchensis*）
  - [偏尾雷氏鯰](../Page/偏尾雷氏鯰.md "wikilink")（*Rhamdia laticauda*）
  - [勞基雷氏鯰](../Page/勞基雷氏鯰.md "wikilink")（*Rhamdia laukidi*）
  - [墨西哥雷氏鯰](../Page/墨西哥雷氏鯰.md "wikilink")（*Rhamdia macuspanensis*）
  - [米勒雷氏鯰](../Page/米勒雷氏鯰.md "wikilink")（*Rhamdia muelleri*）
  - [尼加拉瓜雷氏鯰](../Page/尼加拉瓜雷氏鯰.md "wikilink")（*Rhamdia nicaraguensis*）
  - [帕里雷氏鯰](../Page/帕里雷氏鯰.md "wikilink")（*Rhamdia parryi*）
  - [波耶雷氏鯰](../Page/波耶雷氏鯰.md "wikilink")（*Rhamdia poeyi*）
  - [克林雷氏鯰](../Page/克林雷氏鯰.md "wikilink")（*Rhamdia quelen*）
  - [短鬚雷氏鯰](../Page/短鬚雷氏鯰.md "wikilink")（*Rhamdia reddelli*）
  - [舒伯格雷氏鯰](../Page/舒伯格雷氏鯰.md "wikilink")（*Rhamdia schomburgkii*）
  - [寶貝雷氏鯰](../Page/寶貝雷氏鯰.md "wikilink")（*Rhamdia velifer*）
  - [秘魯雷氏鯰](../Page/秘魯雷氏鯰.md "wikilink")（*Rhamdia xetequepeque*）
  - [宗哥雷氏鯰](../Page/宗哥雷氏鯰.md "wikilink")（*Rhamdia zongolicensis*）

### 拉迪奧鯰屬（*Rhamdioglanis*）

  - [巴西拉迪奧鯰](../Page/巴西拉迪奧鯰.md "wikilink")（*Rhamdioglanis frenatus*）
  - [橫帶拉迪奧鯰](../Page/橫帶拉迪奧鯰.md "wikilink")（*Rhamdioglanis
    transfasciatus*）

### 雷迪油鯰屬（*Rhamdiopsis*）

  - [克氏雷迪油鯰](../Page/克氏雷迪油鯰.md "wikilink")（*Rhamdiopsis krugi*）
  - [小頭雷迪油鯰](../Page/小頭雷迪油鯰.md "wikilink")（*Rhamdiopsis microcephala*）
  - [莫氏雷迪油鯰](../Page/莫氏雷迪油鯰.md "wikilink")（*Rhamdiopsis moreirai*）

### 項鯰屬（*Taunayia*）

  - [雙帶巴西項鯰](../Page/雙帶巴西項鯰.md "wikilink")（*Taunayia marginata*）

[\*](../Category/鼬鯰科.md "wikilink")