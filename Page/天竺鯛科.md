**天竺鯛科**（[學名](../Page/學名.md "wikilink")：）為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鉤頭魚目](../Page/鉤頭魚目.md "wikilink")的一個[科](../Page/科.md "wikilink")，传统上被归类于[鲈形目](../Page/鲈形目.md "wikilink")。

## 分布

本科魚類廣泛分布於全球各大洋熱帶、亞熱帶水域。

## 深度

深度從潮池至水深100公尺處之[珊瑚礁區皆能發現其蹤跡](../Page/珊瑚礁.md "wikilink")。

## 分類

本科傳統上被歸類於[鱸形目](../Page/鱸形目.md "wikilink")[鲈亚目](../Page/鲈亚目.md "wikilink")，但现在已知本科与[虾虎鱼亚目的关系更亲近](../Page/虾虎鱼亚目.md "wikilink")。依2016年Deepfin《[硬骨鱼支序分类法](../Page/硬骨鱼支序分类法.md "wikilink")》第4版，本科自成一個**天竺鲷亚目**，是[钩头鱼科](../Page/钩头鱼科.md "wikilink")（）的[旁系群](../Page/旁系群.md "wikilink")，两者共同组成钩头鱼目，再与[虾虎鱼目组成](../Page/虾虎鱼目.md "wikilink")[虾虎鱼系](../Page/虾虎鱼系.md "wikilink")。

其下分28個屬，如下：

### 天竺鯛屬(*Apogon*)

  -   - [美紋天竺鯛](../Page/美紋天竺鯛.md "wikilink")(*Apogon abrogramma*)
      - [犬牙天竺鯛](../Page/犬牙天竺鯛.md "wikilink")(*Apogon affinis*)
      - [白斑天竺鯛](../Page/白斑天竺鯛.md "wikilink")(*Apogon albimaculosus*)
      - [白邊天竺鯛](../Page/白邊天竺鯛.md "wikilink")(*Apogon albomarginatus*)
      - [弓線天竺鯛](../Page/弓線天竺鯛.md "wikilink")(*Apogon amboinensis*)
      - [美洲天竺鯛](../Page/美洲天竺鯛.md "wikilink")(*Apogon americanus*)
      - [寬帶天竺鯛](../Page/寬帶天竺鯛.md "wikilink")(*Apogon
        angustatus*)：又稱[縱帶天竺鯛](../Page/縱帶天竺鯛.md "wikilink")。
      - [短齒天竺鯛](../Page/短齒天竺鯛.md "wikilink")(*Apogon
        apogonoides*)：又稱[短牙天竺鯛](../Page/短牙天竺鯛.md "wikilink")。
      - [银腹管竺鲷](../Page/银腹管竺鲷.md "wikilink")(*Apogon argyrogaster*)
      - (*Apogon aroubiensis*)
      - (*Apogon aterrimus*)
      - [黑幼背天竺鯛](../Page/黑幼背天竺鯛.md "wikilink")(*Apogon atradorsatus*)
      - [黑尾天竺鯛](../Page/黑尾天竺鯛.md "wikilink")(*Apogon atricaudus*)
      - [黑腹天竺鯛](../Page/黑腹天竺鯛.md "wikilink")(*Apogon atrogaster*)
      - [黃天竺鯛](../Page/黃天竺鯛.md "wikilink")(*Apogon
        aureus*)：又稱[環尾天竺鯛](../Page/環尾天竺鯛.md "wikilink")。
      - [金線天竺鯛](../Page/金線天竺鯛.md "wikilink")(*Apogon aurolineatus*)
      - [腋天竺鯛](../Page/腋天竺鯛.md "wikilink")(*Apogon axillaris*)
      - [雙斑天竺鯛](../Page/雙斑天竺鯛.md "wikilink")(*Apogon binotatus*)
      - (*Apogon bryx*)
      - (*Apogon campbelli*)
      - [羊角天竺鯛](../Page/羊角天竺鯛.md "wikilink")(*Apogon capricornis*)
      - [單斑天竺鯛](../Page/單斑天竺鯛.md "wikilink")(*Apogon
        carinatus*)：又稱[斑鰭天竺鯛](../Page/斑鰭天竺鯛.md "wikilink")。
      - (*Apogon catalai*)
      - [垂帶天竺鯛](../Page/垂帶天竺鯛.md "wikilink")(*Apogon cathetogramma*)
      - (*Apogon caudicinctus*)
      - [甲米地天竺鯛](../Page/甲米地天竺鯛.md "wikilink")(*Apogon cavitiensis*)
      - [塞蘭島天竺鯛](../Page/塞蘭島天竺鯛.md "wikilink")(*Apogon ceramensis*)
      - (*Apogon chalcius*)
      - [陳氏天竺鯛](../Page/陳氏天竺鯛.md "wikilink")(*Apogon cheni*)
      - [金盖天竺鲷](../Page/金盖天竺鲷.md "wikilink")(*Apogon chrysopomus*)
      - [多線天竺鯛](../Page/多線天竺鯛.md "wikilink")(*Apogon
        chrysotaenia*)：又稱[黃體天竺鯛](../Page/黃體天竺鯛.md "wikilink")。
      - (*Apogon cladophilos*)
      - [紅天竺鯛](../Page/紅天竺鯛.md "wikilink")(*Apogon
        coccineus*)：又稱[小湊天竺鯛](../Page/小湊天竺鯛.md "wikilink")。
      - [裂帶天竺鯛](../Page/裂帶天竺鯛.md "wikilink")(*Apogon compressus*)
      - [庫氏天竺鯛](../Page/庫氏天竺鯛.md "wikilink")(*Apogon cookii*)
      - [堅頭天竺鯛](../Page/堅頭天竺鯛.md "wikilink")(*Apogon
        crassiceps*)：又稱[大鱗天竺鯛](../Page/大鱗天竺鯛.md "wikilink")。
      - [金線天竺鯛](../Page/金線天竺鯛.md "wikilink")(*Apogon
        cyanosoma*)：又稱[金帶天竺鯛](../Page/金帶天竺鯛.md "wikilink")。
      - (*Apogon dammermani*)
      - (*Apogon deetsie*)
      - [阿拉伯海天竺鯛](../Page/阿拉伯海天竺鯛.md "wikilink")(*Apogon dhofar*)
      - (*Apogon dianthus*)
      - [箭矢天竺鯛](../Page/箭矢天竺鯛.md "wikilink")(*Apogon
        dispar*)：又稱[異天竺鯛](../Page/異天竺鯛.md "wikilink")。
      - (*Apogon diversus*)
      - [稻氏天竺鯛](../Page/稻氏天竺鯛.md "wikilink")(*Apogon
        doederleini*)：又稱[斗氏天竺鯛](../Page/斗氏天竺鯛.md "wikilink")。
      - [長棘天竺鯛](../Page/長棘天竺鯛.md "wikilink")(*Apogon doryssa*)
      - [達氏天竺鯛](../Page/達氏天竺鯛.md "wikilink")(*Apogon dovii*)
      - [黑邊天竺鯛](../Page/黑邊天竺鯛.md "wikilink")(*Apogon ellioti*)
      - [小條紋天竺鯛](../Page/小條紋天竺鯛.md "wikilink")(*Apogon
        endekataenia*)：又稱[細線天竺鯛](../Page/細線天竺鯛.md "wikilink")。
      - [粉紅天竺鯛](../Page/粉紅天竺鯛.md "wikilink")(*Apogon erythrinus*)
      - (''Apogon erythrosoma '')
      - [埃氏天竺鯛](../Page/埃氏天竺鯛.md "wikilink")(*Apogon
        evermanni*)：又稱[長柄天竺鯛](../Page/長柄天竺鯛.md "wikilink")。
      - [單線天竺鯛](../Page/單線天竺鯛.md "wikilink")(*Apogon exostigma*)
      - [四線天竺鯛](../Page/四線天竺鯛.md "wikilink")(*Apogon
        fasciatus*)：又稱[寬條天竺鯛](../Page/寬條天竺鯛.md "wikilink")。
      - [絲鰭天竺鯛](../Page/絲鰭天竺鯛.md "wikilink")(*Apogon flagelliferus*)
      - [金黃天竺鯛](../Page/金黃天竺鯛.md "wikilink")(*Apogon flavus*)
      - [黃身天竺鯛](../Page/黃身天竺鯛.md "wikilink")(*Apogon
        fleurieu*)：又稱[斑柄天竺鯛](../Page/斑柄天竺鯛.md "wikilink")。
      - [棘眼天竺鯛](../Page/棘眼天竺鯛.md "wikilink")(*Apogon
        fraenatus*)：又稱[套天竺鯛](../Page/套天竺鯛.md "wikilink")。
      - [弗氏天竺鯛](../Page/弗氏天竺鯛.md "wikilink")(*Apogon franssedai*)
      - [福氏天竺鯛](../Page/福氏天竺鯛.md "wikilink")(*Apogon fukuii*)
      - [棕斑天竺鯛](../Page/棕斑天竺鯛.md "wikilink")(*Apogon fuscomaculatus*)
      - [紡錘天竺鯛](../Page/紡錘天竺鯛.md "wikilink")(*Apogon fusovatus*)
      - [古氏天竺鯛](../Page/古氏天竺鯛.md "wikilink")(*Apogon gouldi*)
      - (*Apogon griffini*)
      - [瓜達盧佩天竺鯛](../Page/瓜達盧佩天竺鯛.md "wikilink")(*Apogon guadalupensis*)
      - [雲紋天竺鯛](../Page/雲紋天竺鯛.md "wikilink")(*Apogon guamensis*)
      - [貪食天竺鯛](../Page/貪食天竺鯛.md "wikilink")(*Apogon gularis*)
      - [哈茨天竺鯛](../Page/哈茨天竺鯛.md "wikilink")(*Apogon hartzfeldii*)
      - [霍氏天竺鯛](../Page/霍氏天竺鯛.md "wikilink")(*Apogon hoevenii*)
      - (*Apogon holotaenia*)
      - [亨氏天竺鯛](../Page/亨氏天竺鯛.md "wikilink")(*Apogon hungi*)
      - [扁頭天竺鯛](../Page/扁頭天竺鯛.md "wikilink")(*Apogon hyalosoma*)
      - [歐洲天竺鯛](../Page/歐洲天竺鯛.md "wikilink")(*Apogon imberbis*)
      - (*Apogon indicus*)
      - [石恒天竺鯛](../Page/石恒天竺鯛.md "wikilink")(*Apogon ishigakiensis*)
      - [紅海天竺鯛](../Page/紅海天竺鯛.md "wikilink")(*Apogon isus*)
      - (*Apogon jenkinsi*)
      - [棘頭天竺鯛](../Page/棘頭天竺鯛.md "wikilink")(*Apogon
        kallopterus*)：又稱[麗鰭天竺鯛](../Page/麗鰭天竺鯛.md "wikilink")。
      - [美身天竺鯛](../Page/美身天竺鯛.md "wikilink")(*Apogon kalosoma*)
      - (*Apogon kautamea*)
      - [中線天竺鯛](../Page/中線天竺鯛.md "wikilink")(*Apogon kiensis*)
      - (*Apogon komodoensis*)
      - [白星天竺鯛](../Page/白星天竺鯛.md "wikilink")(*Apogon lachneri*)
      - [側身天竺鯛](../Page/側身天竺鯛.md "wikilink")(*Apogon
        lateralis*)：又稱[側條天竺鯛](../Page/側條天竺鯛.md "wikilink")。
      - (*Apogon lativittatus*)
      - (*Apogon latus*)
      - [細尾天竺鯛](../Page/細尾天竺鯛.md "wikilink")(*Apogon leptocaulus*)
      - (*Apogon leptofasciatus*)
      - [港神天竺鯛](../Page/港神天竺鯛.md "wikilink")(*Apogon limenus*)
      - [細條紋天竺鯛](../Page/細條紋天竺鯛.md "wikilink")(*Apogon
        lineatus*)：又稱[細條天竺鯛](../Page/細條天竺鯛.md "wikilink")。
      - (*Apogon lineomaculatus*)
      - [斑紋天竺鯛](../Page/斑紋天竺鯛.md "wikilink")(*Apogon maculatus*)
      - [銹斑天竺鯛](../Page/銹斑天竺鯛.md "wikilink")(*Apogon maculiferus*)
      - (*Apogon maculipinnis*)
      - (*Apogon margaritophorus*)
      - (*Apogon marquesensis*)
      - [暗鰭天竺鯛](../Page/暗鰭天竺鯛.md "wikilink")(*Apogon melanopus*)
      - [黑身天竺鯛](../Page/黑身天竺鯛.md "wikilink")(*Apogon melas*)
      - [細斑天竺鯛](../Page/細斑天竺鯛.md "wikilink")(*Apogon micromaculatus*)
      - (*Apogon microspilos*)
      - [摩鹿加天竺鯛](../Page/摩鹿加天竺鯛.md "wikilink")(*Apogon
        moluccensis*)：又稱[蘇拉威西天竺鯛](../Page/蘇拉威西天竺鯛.md "wikilink")。
      - (*Apogon monospilus*)
      - [莫氏天竺鯛](../Page/莫氏天竺鯛.md "wikilink")(*Apogon mosavi*)
      - [多帶天竺鯛](../Page/多帶天竺鯛.md "wikilink")(*Apogon multilineatus*)
      - [細鱗天竺鯛](../Page/細鱗天竺鯛.md "wikilink")(*Apogon multitaeniatus*)
      - (*Apogon mydrus*)
      - [倭天竺鯛](../Page/倭天竺鯛.md "wikilink")(*Apogon nanus*)
      - [納塔爾天竺鯛](../Page/納塔爾天竺鯛.md "wikilink")(*Apogon natalensis*)
      - [少壯天竺鯛](../Page/少壯天竺鯛.md "wikilink")(*Apogon neotes*)
      - [黑足天竺鯛](../Page/黑足天竺鯛.md "wikilink")(*Apogon nigripes*)
      - [暗條天竺鯛](../Page/暗條天竺鯛.md "wikilink")(*Apogon nigrocincta*)
      - [黑帶天竺鯛](../Page/黑帶天竺鯛.md "wikilink")(*Apogon nigrofasciatus*)
      - [褐尾紋天竺鯛](../Page/褐尾紋天竺鯛.md "wikilink")(*Apogon
        nitidus*)：又稱[褐條天竺鯛](../Page/褐條天竺鯛.md "wikilink")。
      - [諾福天竺鯛](../Page/諾福天竺鯛.md "wikilink")(*Apogon norfolcensis*)
      - [雙點天竺鯛](../Page/雙點天竺鯛.md "wikilink")(*Apogon
        notatus*)：又稱[黑點天竺鯛](../Page/黑點天竺鯛.md "wikilink")。
      - (*Apogon noumeae*)
      - (*Apogon novaeguineae*)
      - [九帶天竺鯛](../Page/九帶天竺鯛.md "wikilink")(*Apogon
        novemfasciatus*)：又稱[九線天竺鯛](../Page/九線天竺鯛.md "wikilink")。
      - [睛尾天竺鯛](../Page/睛尾天竺鯛.md "wikilink")(*Apogon ocellicaudus*)
      - [阿曼天竺鯛](../Page/阿曼天竺鯛.md "wikilink")(*Apogon omanensis*)
      - (*Apogon oxina*)
      - (*Apogon oxygrammus*)
      - [太平洋天竺鯛](../Page/太平洋天竺鯛.md "wikilink")(*Apogon pacificus*)
      - [線帶天竺鯛](../Page/線帶天竺鯛.md "wikilink")(*Apogon pallidofasciatus*)
      - [小天竺鯛](../Page/小天竺鯛.md "wikilink")(*Apogon parvulus*)
      - [月天竺鯛](../Page/月天竺鯛.md "wikilink")(*Apogon phenax*)
      - (*Apogon photogaster*)
      - [寬鞍天竺鯛](../Page/寬鞍天竺鯛.md "wikilink")(*Apogon pillionatus*)
      - [淺色天竺鯛](../Page/淺色天竺鯛.md "wikilink")(*Apogon planifrons*)
      - [側帶天竺鯛](../Page/側帶天竺鯛.md "wikilink")(*Apogon pleuron*)
      - [雜色天竺鯛](../Page/雜色天竺鯛.md "wikilink")(*Apogon poecilopterus*)
      - [黃帶天竺鯛](../Page/黃帶天竺鯛.md "wikilink")(*Apogon
        properuptus*)：又稱[橙帶天竺鯛](../Page/橙帶天竺鯛.md "wikilink")。
      - [臂飾天竺鯛](../Page/臂飾天竺鯛.md "wikilink")(*Apogon pselion*)
      - [擬斑天竺鯛](../Page/擬斑天竺鯛.md "wikilink")(*Apogon pseudomaculatus*)
      - [四線天竺鯛](../Page/四線天竺鯛.md "wikilink")(*Apogon
        quadrifasciatus*)：又稱[四眼天竺鯛](../Page/四眼天竺鯛.md "wikilink")。
      - [鋸頰天竺鯛](../Page/鋸頰天竺鯛.md "wikilink")(*Apogon quadrisquamatus*)
      - (*Apogon quartus*)
      - [奎氏天竺鯛](../Page/奎氏天竺鯛.md "wikilink")(*Apogon queketti*)
      - (*Apogon quinquestriatus*)
      - (*Apogon radcliffei*)
      - (*Apogon regula*)
      - (*Apogon relativus*)
      - [後鞍天竺鯛](../Page/後鞍天竺鯛.md "wikilink")(*Apogon retrosella*)
      - [玫鰭天竺鯛](../Page/玫鰭天竺鯛.md "wikilink")(*Apogon rhodopterus*)
      - [羅比氏天竺鯛](../Page/羅比氏天竺鯛.md "wikilink")(*Apogon robbyi*)
      - [粗唇天竺鯛](../Page/粗唇天竺鯛.md "wikilink")(*Apogon robinsi*)
      - (*Apogon rubellus*)
      - (*Apogon rubrifuscus*)
      - (*Apogon rubrimacula*)
      - [拉氏天竺鯛](../Page/拉氏天竺鯛.md "wikilink")(*Apogon rueppelli*)
      - (*Apogon rufus*)
      - [沙巴天竺鯛](../Page/沙巴天竺鯛.md "wikilink")(*Apogon sabahensis*)
      - [頭帶天竺鯛](../Page/頭帶天竺鯛.md "wikilink")(*Apogon sangiensis*)
      - [西爾天竺鯛](../Page/西爾天竺鯛.md "wikilink")(*Apogon sealei*)
      - [亮天竺鯛](../Page/亮天竺鯛.md "wikilink")(*Apogon selas*)
      - [半線天竺鯛](../Page/半線天竺鯛.md "wikilink")(*Apogon semilineatus*)
      - [雙斜帶天竺鯛](../Page/雙斜帶天竺鯛.md "wikilink")(*Apogon
        semiornatus*)：又稱[半飾天竺鯛](../Page/半飾天竺鯛.md "wikilink")。
      - (*Apogon seminigracaudus*)
      - [七帶天竺鯛](../Page/七帶天竺鯛.md "wikilink")(*Apogon septemstriatus*)
      - (*Apogon sialis*)
      - (*Apogon sinus*)
      - [史密斯氏天竺鯛](../Page/史密斯氏天竺鯛.md "wikilink")(*Apogon smithi*)
      - [污斑天竺鯛](../Page/污斑天竺鯛.md "wikilink")(*Apogon spilurus*)
      - [綿居天竺鯛](../Page/綿居天竺鯛.md "wikilink")(*Apogon spongicolus*)
      - [印度洋天竺鯛](../Page/印度洋天竺鯛.md "wikilink")(*Apogon striatodes*)
      - [條紋天竺鯛](../Page/條紋天竺鯛.md "wikilink")(*Apogon
        striatus*)：又稱[橫帶天竺鯛](../Page/橫帶天竺鯛.md "wikilink")。
      - (*Apogon susanae*)
      - [褐帶天竺鯛](../Page/褐帶天竺鯛.md "wikilink")(*Apogon
        taeniophorus*)：又稱[九帶天竺鯛](../Page/九帶天竺鯛.md "wikilink")。
      - [條鰭天竺鯛](../Page/條鰭天竺鯛.md "wikilink")(*Apogon taeniopterus*)
      - [塔氏天竺鯛](../Page/塔氏天竺鯛.md "wikilink")(*Apogon talboti*)
      - (*Apogon tchefouensis*)
      - [條腹天竺鯛](../Page/條腹天竺鯛.md "wikilink")(*Apogon thermalis*)
      - [湯氏天竺鯛](../Page/湯氏天竺鯛.md "wikilink")(*Apogon townsendi*)
      - [三斑天竺鯛](../Page/三斑天竺鯛.md "wikilink")(*Apogon trimaculatus*)
      - [一色天竺鯛](../Page/一色天竺鯛.md "wikilink")(*Apogon
        unicolor*)：又稱[單色天竺鯛](../Page/單色天竺鯛.md "wikilink")。
      - [單帶天竺鯛](../Page/單帶天竺鯛.md "wikilink")(*Apogon unitaeniatus*)
      - [腹紋天竺鯛](../Page/腹紋天竺鯛.md "wikilink")(*Apogon ventrifasciatus*)
      - [紅帶天竺鯛](../Page/紅帶天竺鯛.md "wikilink")(*Apogon victoriae*)
      - (*Apogon wassinki*)
      - [威爾遜氏天竺鯛](../Page/威爾遜氏天竺鯛.md "wikilink")(*Apogon wilsoni*)
      - (*Apogon zebrinus*)

### 擬天竺魚屬(*Apogonichthyoides*)

  -   - [牛眼擬天竺鯛](../Page/牛眼擬天竺鯛.md "wikilink")(*Apogonichthyoides
        atripes*)
      - [短尾天竺鯛](../Page/短尾天竺鯛.md "wikilink")(*Apogonichthyoides
        brevicaudatus*)
      - (*Apogonichthyoides cantoris*)
      - [金黃擬天竺鯛](../Page/金黃擬天竺鯛.md "wikilink")(*Apogonichthyoides
        chrysurus*)
      - (*Apogonichthyoides euspilotus*)
      - (*Apogonichthyoides gardineri*)
      - [七冥天竺鯛](../Page/七冥天竺鯛.md "wikilink")(*Apogonichthyoides
        heptastygma*)
      - (*Apogonichthyoides miniatus*)
      - [黑天竺鯛](../Page/黑天竺鯛.md "wikilink")(*Apogonichthyoides niger*)
      - [黑鰭天竺鯛](../Page/黑鰭天竺鯛.md "wikilink")(*Apogonichthyoides
        nigripinnis*)：又稱睛斑天竺鯛。
      - [有蓋天竺鯛](../Page/有蓋天竺鯛.md "wikilink")(*Apogonichthyoides
        opercularis*)
      - (*Apogonichthyoides pharaonis*)
      - [擬雙帶天竺鯛](../Page/擬雙帶天竺鯛.md "wikilink")(*Apogonichthyoides
        pseudotaeniatus*)
      - (*Apogonichthyoides regani*)
      - (*Apogonichthyoides sialis*)
      - [雙帶天竺鯛](../Page/雙帶天竺鯛.md "wikilink")(*Apogonichthyoides
        taeniatus*)
      - [帝汶擬天竺鯛](../Page/帝汶擬天竺鯛.md "wikilink")(*Apogonichthyoides
        timorensis*)
      - (*Apogonichthyoides umbratilis*)
      - [擬天竺魚](../Page/擬天竺魚.md "wikilink")(*Apogonichthyoides
        uninotata*)

### 天竺魚屬(*Apogonichthys*)

  -   - (*Apogonichthys landoni*)
      - [鰭斑天竺魚](../Page/鰭斑天竺魚.md "wikilink")(*Apogonichthys
        ocellatus*)：又稱眼斑原天竺鯛。
      - [夏威夷天竺魚](../Page/夏威夷天竺魚.md "wikilink")(*Apogonichthys
        perdix*)：又稱鳩斑原天竺鯛、鳩斑天竺魚。

### 長鰭天竺鯛屬(*Archamia*)

  -   - (*Archamia ataenia*)
      - [雙斑長鰭天竺鯛](../Page/雙斑長鰭天竺鯛.md "wikilink")(*Archamia
        biguttata*)：又稱黑尾長鰭天竺鯛、暗體長鰭天竺鯛。
      - [雙線長鰭天竺鯛](../Page/雙線長鰭天竺鯛.md "wikilink")(*Archamia bilineata*)
      - [布氏長鰭天竺鯛](../Page/布氏長鰭天竺鯛.md "wikilink")(*Archamia
        bleekeri*)：又稱龔氏長鰭天竺鯛。
      - [橫帶長鰭天竺鯛](../Page/橫帶長鰭天竺鯛.md "wikilink")(*Archamia buruensis*)
      - [褐斑長鰭天竺鯛](../Page/褐斑長鰭天竺鯛.md "wikilink")(*Archamia
        fucata*)：又稱紅紋長鰭天竺鯛、橫紋長鰭天竺鯛、虹彩長鰭天竺鯛。
      - (*Archamia flavofasciata*)
      - [利氏長鰭天竺鯛](../Page/利氏長鰭天竺鯛.md "wikilink")(*Archamia leai*)
      - [原長鰭天竺鯛](../Page/原長鰭天竺鯛.md "wikilink")(*Archamia lineolata*)
      - [真長鰭天竺鯛](../Page/真長鰭天竺鯛.md "wikilink")(*Archamia macroptera*)
      - [莫桑比克長鰭天竺鯛](../Page/莫桑比克長鰭天竺鯛.md "wikilink")(*Archamia
        mozambiquensis*)
      - [蒼白長鰭天竺鯛](../Page/蒼白長鰭天竺鯛.md "wikilink")(*Archamia pallida*)
      - [黑帶長鰭天竺鯛](../Page/黑帶長鰭天竺鯛.md "wikilink")(*Archamia
        zosterophora*)

### 星天竺鯛屬(*Astrapogon*)

  -   - [青銅星天竺鯛](../Page/青銅星天竺鯛.md "wikilink")(*Astrapogon alutus*)
      - [黑鰭星天竺鯛](../Page/黑鰭星天竺鯛.md "wikilink")(*Astrapogon
        puncticulatus*)
      - [星天竺鯛](../Page/星天竺鯛.md "wikilink")(*Astrapogon stellatus*)

### 比安魚屬(*Beanea*)

  -   - [比安魚](../Page/比安魚.md "wikilink")(*Beanea trivittata*)

### 梭竺鯛屬(*Cercamia*)

  -   - [僻梭竺鯛](../Page/僻梭竺鯛.md "wikilink")(*Cercamia eremia*)

### 巨牙天竺鯛屬(*Cheilodipterus*)

  -   - [艾倫氏巨牙天竺鯛](../Page/艾倫氏巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        alleni*)
      - [縱帶巨牙天竺鯛](../Page/縱帶巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        artus*)：又稱縱帶巨齒天竺鯛。
      - [犬狀巨牙天竺鯛](../Page/犬狀巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        caninus*)
      - [中間巨牙天竺鯛](../Page/中間巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        intermedius*)：又稱中間巨齒天鯛。
      - [等斑巨牙天竺鯛](../Page/等斑巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        isostigmus*)
      - [箭齒巨牙天竺鯛](../Page/箭齒巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        lachneri*)
      - [線紋巨牙天竺鯛](../Page/線紋巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        lineatus*)
      - [巨牙天竺鯛](../Page/巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        macrodon*)：又稱巨齒天竺鯛。
      - [黑帶巨牙天竺鯛](../Page/黑帶巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        nigrotaeniatus*)
      - [九帶巨牙天竺鯛](../Page/九帶巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        novemstriatus*)
      - [副條巨牙天竺鯛](../Page/副條巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        parazonatus*)
      - [桃色巨牙天竺鯛](../Page/桃色巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        persicus*)
      - [矮身巨牙天竺鯛](../Page/矮身巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        pygmaios*)
      - [五帶巨牙天竺鯛](../Page/五帶巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        quinquelineatus*)：又稱五線巨齒天竺鯛。
      - [新加坡巨牙天竺鯛](../Page/新加坡巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        singapurensis*)
      - [圓蓋巨牙天竺鯛](../Page/圓蓋巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        subulatus*)
      - [帶紋巨牙天竺鯛](../Page/帶紋巨牙天竺鯛.md "wikilink")(*Cheilodipterus
        zonatus*)

### 多刺天竺鯛屬(*Coranthus*)

  -   - [多刺天竺鯛](../Page/多刺天竺鯛.md "wikilink")(*Coranthus polyacanthus*)

### 腭竺魚屬(*Foa*)

  -   - [駝峰腭竺魚](../Page/駝峰腭竺魚.md "wikilink")(*Foa abocellata*)
      - [短線腭竺魚](../Page/短線腭竺魚.md "wikilink")(*Foa
        brachygramma*)：又稱短線小天竺鯛。
      - [菲律賓腭竺魚](../Page/菲律賓腭竺魚.md "wikilink")(*Foa fo*)

### 乳突天竺鯛屬(*Fowleria*)

  -   - [金色乳突天竺鯛](../Page/金色乳突天竺鯛.md "wikilink")(*Fowleria aurita*)
      - [短斑乳突天竺鯛](../Page/短斑乳突天竺鯛.md "wikilink")(*Fowleria
        brachygramma*)
      - [金焰乳突天竺鯛](../Page/金焰乳突天竺鯛.md "wikilink")(*Fowleria flammea*)
      - [顯乳突天竺鯛](../Page/顯乳突天竺鯛.md "wikilink")(*Fowleria marmorata*)
      - [眼斑乳突天竺鯛](../Page/眼斑乳突天竺鯛.md "wikilink")(*Fowleria ocellata*)
      - [等斑乳突天竺鯛](../Page/等斑乳突天竺鯛.md "wikilink")(*Fowleria punctulata*)
      - [維拉乳突天竺鯛](../Page/維拉乳突天竺鯛.md "wikilink")(*Fowleria vaiulae*)
      - [多斑乳突天竺鯛](../Page/多斑乳突天竺鯛.md "wikilink")(*Fowleria variegata*)

### 舌天竺鯛屬(*Glossamia*)

  -   - [舌天竺鯛](../Page/舌天竺鯛.md "wikilink")(*Glossamia aprion*)
      - [橫帶舌天竺鯛](../Page/橫帶舌天竺鯛.md "wikilink")(*Glossamia beauforti*)
      - [吉氏舌天竺鯛](../Page/吉氏舌天竺鯛.md "wikilink")(*Glossamia gjellerupi*)
      - [新畿內亞舌天竺鯛](../Page/新畿內亞舌天竺鯛.md "wikilink")(*Glossamia
        narindica*)
      - [桑德氏舌天竺鯛](../Page/桑德氏舌天竺鯛.md "wikilink")(*Glossamia sandei*)
      - [三帶舌天竺鯛](../Page/三帶舌天竺鯛.md "wikilink")(*Glossamia trifasciata*)
      - [威氏舌天竺鯛](../Page/威氏舌天竺鯛.md "wikilink")(*Glossamia wichmanni*)

### 裸天竺鯛屬(*Gymnapogon*)

  -   - [非洲裸天竺鯛](../Page/非洲裸天竺鯛.md "wikilink")(*Gymnapogon africanus*)
      - [無斑裸天竺鯛](../Page/無斑裸天竺鯛.md "wikilink")(*Gymnapogon annona*)
      - [日本裸天竺鯛](../Page/日本裸天竺鯛.md "wikilink")(*Gymnapogon japonicus*)
      - [菲律賓裸天竺鯛](../Page/菲律賓裸天竺鯛.md "wikilink")(*Gymnapogon
        philippinus*)
      - [尾斑裸天竺鯛](../Page/尾斑裸天竺鯛.md "wikilink")(*Gymnapogon urospilotus*)
      - [範氏裸天竺鯛](../Page/範氏裸天竺鯛.md "wikilink")(*Gymnapogon vanderbilti*)

### 全芒天竺鯛屬(*Holapogon*)

  -   - [全芒天竺鯛](../Page/全芒天竺鯛.md "wikilink")(*Holapogon maximus*)

### *Jaydia*屬

  -   - [截尾天竺鲷](../Page/截尾天竺鲷.md "wikilink")(*Jaydia truncata*)：又稱黑鰓天竺鯛。

### 鬼幻茸竺鯛屬(*Lachneratus*)

  -   - [鬼幻茸竺鯛](../Page/鬼幻茸竺鯛.md "wikilink")(*Lachneratus phasmaticus*)

### 扁天竺鯛屬(*Neamia*)

  -   - [環紋天竺鯛](../Page/環紋天竺鯛.md "wikilink")(*Neamia annularis*)
      - [八棘扁天竺鯛](../Page/八棘扁天竺鯛.md "wikilink")(*Neamia
        octospina*)：又稱菲律賓八棘天竺鯛。

### *Nectamia*屬

  -   - [頰紋天竺鯛](../Page/頰紋天竺鯛.md "wikilink")(*Nectamia bandanensis*)
      - [魔鬼天竺鯛](../Page/魔鬼天竺鯛.md "wikilink")(*Nectamia fuscus*)：又稱棕天竺鯛。
      - [薩摩亞天竺鯛](../Page/薩摩亞天竺鯛.md "wikilink")(*Nectamia
        savayensis*)：又稱側紋天竺鯛。

### 鸚竺鯛屬(*Ostorhinchus*)

  -   - [康氏鸚竺鯛](../Page/康氏鸚竺鯛.md "wikilink")(*Ostorhinchus conwaii*)

### 副天竺鯛屬(*Paramina*)

  -   - [五帶副天竺鯛](../Page/五帶副天竺鯛.md "wikilink")(*Paramina
        quinquelineatus*)

### 褶竺鯛屬(*Phaeoptyx*)

  -   - [褶竺鯛](../Page/褶竺鯛.md "wikilink")(*Phaeoptyx conklini*)
      - [噬色褶竺鯛](../Page/噬色褶竺鯛.md "wikilink")(*Phaeoptyx pigmentaria*)
      - [異域褶竺鯛](../Page/異域褶竺鯛.md "wikilink")(*Phaeoptyx xenus*)

### 擬天竺鯛屬(*Pseudamia*)

  -   - [鈍尾擬天竺鯛](../Page/鈍尾擬天竺鯛.md "wikilink")(*Pseudamia
        amblyuroptera*)
      - [犬牙擬天竺鯛](../Page/犬牙擬天竺鯛.md "wikilink")(*Pseudamia
        gelatinosa*)：又稱鈍尾擬天竺鯛。
      - [圓鱗擬天竺鯛](../Page/圓鱗擬天竺鯛.md "wikilink")(*Pseudamia hayashii*)
      - [黑擬天竺鯛](../Page/黑擬天竺鯛.md "wikilink")(*Pseudamia nigra*)
      - [多邊擬天竺鯛](../Page/多邊擬天竺鯛.md "wikilink")(*Pseudamia polystigma*)
      - [桔紅擬天竺鯛](../Page/桔紅擬天竺鯛.md "wikilink")(*Pseudamia rubra*)
      - [塔氏鰜鮃](../Page/塔氏鰜鮃.md "wikilink")(*Pseudamia tarri*)
      - [黑帶擬天竺鯛](../Page/黑帶擬天竺鯛.md "wikilink")(*Pseudamia zonata*)

### 准天竺鯛屬(*Pseudamiops*)

  -   - [准天竺鯛](../Page/准天竺鯛.md "wikilink")(*Pseudamiops gracilicauda*)
      - [長體准天竺鯛](../Page/長體准天竺鯛.md "wikilink")(*Pseudamiops pellucidus*)

### 鰭竺鯛屬(*Pterapogon*)

  -   - [考氏鰭竺鯛](../Page/考氏鰭竺鯛.md "wikilink")(*Pterapogon kauderni*)

### *Quinca*屬

  -   - [鰭竺鯛](../Page/鰭竺鯛.md "wikilink")(*Quinca mirifica*)

### 箭天竺鯛屬(*Rhabdamia*)

  -   - [燕尾箭天竺鯛](../Page/燕尾箭天竺鯛.md "wikilink")(*Rhabdamia
        cypselura*)：又稱短箭天竺鯛。
      - [柔箭天竺鯛](../Page/柔箭天竺鯛.md "wikilink")(*Rhabdamia eremia*)
      - [箭天竺鯛](../Page/箭天竺鯛.md "wikilink")(*Rhabdamia
        gracilis*)：又稱細箭天竺鯛。
      - [黑頦箭天竺鯛](../Page/黑頦箭天竺鯛.md "wikilink")(*Rhabdamia nigrimentum*)
      - [斑箭天竺鯛](../Page/斑箭天竺鯛.md "wikilink")(*Rhabdamia spilota*)

### 管竺鯛屬(*Siphamia*)

  -   - [銀腹管竺鯛](../Page/銀腹管竺鯛.md "wikilink")(*Siphamia argyrogaster*)
      - [黑帶管竺鯛](../Page/黑帶管竺鯛.md "wikilink")(*Siphamia cephalotes*)
      - [珊瑚管竺鯛](../Page/珊瑚管竺鯛.md "wikilink")(*Siphamia corallicola*)
      - [楔首管竺鯛](../Page/楔首管竺鯛.md "wikilink")(*Siphamia cuneiceps*)
      - [簫狀管竺鯛](../Page/簫狀管竺鯛.md "wikilink")(*Siphamia fistulosa*)
      - [棕線管竺鯛](../Page/棕線管竺鯛.md "wikilink")(*Siphamia
        fuscolineata*)：又稱棕線管天竺鯛。
      - [側點管竺鯛](../Page/側點管竺鯛.md "wikilink")(*Siphamia guttulatus*)
      - [傑布氏管竺鯛](../Page/傑布氏管竺鯛.md "wikilink")(*Siphamia jebbi*)
      - [馬島氏管竺鯛](../Page/馬島氏管竺鯛.md "wikilink")(*Siphamia
        majimai*)：又稱馬島氏管天竺鯛。
      - [莫桑管竺鯛](../Page/莫桑管竺鯛.md "wikilink")(*Siphamia mossambica*)
      - [紅海管竺鯛](../Page/紅海管竺鯛.md "wikilink")(*Siphamia permutata*)
      - [紋腹管竺鯛](../Page/紋腹管竺鯛.md "wikilink")(*Siphamia roseigaster*)
      - [管竺鯛](../Page/管竺鯛.md "wikilink")(*Siphamia tubulata*)
      - [變色管竺鯛](../Page/變色管竺鯛.md "wikilink")(*Siphamia
        versicolor*)：又稱變色管天竺鯛。

### 圓竺鯛屬(*Sphaeramia*)

  -   - [絲鰭圓天竺鯛](../Page/絲鰭圓天竺鯛.md "wikilink")(*Sphaeramia
        nematoptera*)：又稱絲鰭圓竺鯛。
      - [紅尾圓竺鯛](../Page/紅尾圓竺鯛.md "wikilink")(*Sphaeramia
        orbicularis*)：又稱環紋圓天竺鯛。

### 柄竺鯛屬(*Vincentia*)

  -   - [巴地柄竺鯛](../Page/巴地柄竺鯛.md "wikilink")(*Vincentia badia*)
      - [金色柄竺鯛](../Page/金色柄竺鯛.md "wikilink")(*Vincentia chrysura*)
      - [小斑柄竺鯛](../Page/小斑柄竺鯛.md "wikilink")(*Vincentia conspersa*)
      - [大尾柄竺鯛](../Page/大尾柄竺鯛.md "wikilink")(*Vincentia macrocauda*)
      - [凹鰭柄竺鯛](../Page/凹鰭柄竺鯛.md "wikilink")(*Vincentia
        novaehollandiae*)
      - [橫斑柄竺鯛](../Page/橫斑柄竺鯛.md "wikilink")(*Vincentia punctata*)

### *Zoramia*屬

  -   - [脆棘天竺鯛](../Page/脆棘天竺鯛.md "wikilink")(*Zoramia fragilis*)
      - [齊氏天竺鯛](../Page/齊氏天竺鯛.md "wikilink")(*Zoramia gilberti*)
      - [弱棘天竺鯛](../Page/弱棘天竺鯛.md "wikilink")(*Zoramia leptacanthus*)
      - [珍珠天竺鯛](../Page/珍珠天竺鯛.md "wikilink")(*Zoramia perlitus*)

## 特徵

本科魚類體略延長側扁，有比較大形的頭部及[眼睛](../Page/眼睛.md "wikilink")。口大，上下頷及鋤骨有絨毛狀齒帶，鰓蓋主骨有2小棘，體側被櫛鱗或圓鱗。背鰭2枚互相遠離，第一背鰭6至9枚硬棘；第二背鰭1枚硬棘及8至14枚軟條，體常很少超過12公分。

## 生態

本科魚類為夜行性，夜間外出覓食，晝間則單獨或成群穴居在礁岩遮蔽處或其周圍的水層中。其聚集的數量可達數千尾之多，另外本科魚有另一特性，就是親魚會以口孵卵，且多半由雄魚負責。

## 經濟利用

為食用魚，適合油炸食用，部分種類也可做為觀賞魚。

## 参考文献

[\*](../Category/天竺鯛科.md "wikilink")