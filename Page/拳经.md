《**拳經**》於1917年由[上海大聲圖書局出版](../Page/上海大聲圖書局.md "wikilink")。內容主要分「拳經」及「少林拳術精義」兩個部分。[陸士諤是清末民初居於上海的](../Page/陸士諤.md "wikilink")[中醫跌打骨科](../Page/中醫.md "wikilink")。

### 拳經內容

共分四卷

  - 卷一有十八字訣及[擒拿解法](../Page/擒拿.md "wikilink")。
  - 卷二有[潭腿十二路](../Page/潭腿.md "wikilink")、宋太祖[長拳三十二勢圖](../Page/長拳.md "wikilink")、內家[張三峰拳法](../Page/張三峰.md "wikilink")、外家[少林宗法](../Page/少林宗法.md "wikilink")（十八手及[五拳練法](../Page/五拳.md "wikilink")）、行拳、金鎗手、魯智深下山、[羅漢拳二則等目](../Page/羅漢拳.md "wikilink")。
  - 卷三有氣血說、五臟說及玉環穴說等。
  - 卷四為跌打骨科藥方。

### 少林拳術精義

有[易筋經](../Page/易筋經.md "wikilink")[李靖及](../Page/李靖.md "wikilink")[牛皋序](../Page/牛皋序.md "wikilink")（[唐豪經考證為托古偽作](../Page/唐豪.md "wikilink")）

  - 分兩部：－
  - 伏氣圖說（習動氣功圖譜）
  - 易筋經義

後有[天台](../Page/天台.md "wikilink")[紫凝道人及](../Page/紫凝道人.md "wikilink")[祝文瀾之兩篇跋](../Page/祝文瀾.md "wikilink")。

## 拳經（張孔昭）

於1929年由「大聲書局」以石印出版了《拳經》一書，說是由[乾隆四十九年時曹煥斗作注](../Page/乾隆.md "wikilink")，收集[清](../Page/清朝.md "wikilink")[康熙時張孔昭之拳技整理而成](../Page/康熙.md "wikilink")，可惜未有佐證。

拳經內說由民間的張鳴鴞發展而成。未有從[少林寺發展出來的證據](../Page/少林寺.md "wikilink")。

### 拳經內容

1.  問答歌訣廿款
2.  周身秘訣十二項
3.  [下盤細密秘訣](../Page/下盤.md "wikilink")
4.  又附張先生原歌訣\[1\]
5.  [少林寺短打身法統宗拳譜](../Page/少林寺.md "wikilink")（十則）
6.  場中切要（手法，步法）
7.  總論入身
8.  張橫秋先生傳授習練手法秘要\[2\]
9.  口傳百法

### 拳經內之拳套

1.  迷拳（六節）
2.  醉八仙（八節）
3.  死手解救（散手）

## 拳法備要

直到，1936年「蟫隱廬」又出版了較《拳經》完備之手抄本《拳經，[拳法備要](../Page/拳法備要.md "wikilink")，二卷》。由**羅振常**在序中分析，認為拳法備要全為曹作，即後來加上。

又說拳經中之雙管秘法，付張之原歌訣，可見拳經亦非張氏原文也。

在拳經曹序中言讀張氏之書，揣摩而成。

## 参看

  - [南少林](../Page/南少林.md "wikilink")
  - [洪門](../Page/洪門.md "wikilink")
  - [南拳](../Page/南拳.md "wikilink")
  - [少林派](../Page/少林派.md "wikilink")
  - [中國武術](../Page/中國武術.md "wikilink")
  - [中國武術門派](../Page/中國武術門派.md "wikilink")

## 外部連結

  - [策略研究中心：拳經](http://www.cos.url.tw/fight/fist.htm)
  - [策略研究中心：拳法備要](https://web.archive.org/web/20160304070326/http://www.cos.url.tw/fight/fistb.htm)

## 注釋

[Q](../Page/category:中国武术.md "wikilink")

[Q](../Category/中文書籍.md "wikilink")

1.  可見拳經不是張孔昭原著。
2.  可見拳經不是張孔昭原著。