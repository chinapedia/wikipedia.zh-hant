**昆嵛山**地处[胶东半岛东端](../Page/胶东半岛.md "wikilink")，跨[山东省的](../Page/山东省.md "wikilink")[威海](../Page/威海市.md "wikilink")[文登市和](../Page/文登市.md "wikilink")[烟台市](../Page/烟台市.md "wikilink")[牟平区](../Page/牟平区.md "wikilink")，总面积24万余亩。主峰泰礴顶，海拔923米，巍峨耸立，为胶东群山之首。昆嵛山与艾山、牙山、大泽山等横贯半岛的中部和北部，构成半岛南北水系的分水岭。昆嵛山有[老子](../Page/老子.md "wikilink")《[道德经](../Page/道德经.md "wikilink")》的[摩崖石刻](../Page/摩崖石刻.md "wikilink")，汉代[永康石刻](../Page/永康石刻.md "wikilink")，金元[圣旨碑](../Page/圣旨碑.md "wikilink")，[懿旨碑](../Page/懿旨碑.md "wikilink")，丘处机手书石碑等。[昆嵛山是](../Page/昆嵛山.md "wikilink")[道教名山](../Page/道教.md "wikilink")，[全真派的发源地](../Page/全真派.md "wikilink")。《[齐乘](../Page/齐乘.md "wikilink")》云：昆嵛山“秀拔为群山之冠”，北魏史学家[崔鸿称昆嵛山为](../Page/崔鸿.md "wikilink")“海上诸山之祖”。此山峰峦绵延，林深谷幽，多有清泉飞瀑，遍布文物古迹，素有“不似泰山，胜似泰山”之美誉。[秦始皇](../Page/秦始皇.md "wikilink")，[汉武帝曾多次东寻游历昆嵛山](../Page/汉武帝.md "wikilink")，寻觅长生不老之术。

## 宗教名山

昆嵛山是全国闻名的道教圣地，根据《[宁海州志](../Page/宁海州志.md "wikilink")》所载：自隋唐以来，昆嵛山便寺观林立，洞庵毗连，香火缭绕，朝暮不断。[北五祖之首](../Page/北五祖.md "wikilink")，东华帝君[王玄甫曾隐于](../Page/王玄甫.md "wikilink")[昆嵛山内](../Page/昆嵛山.md "wikilink")，修仙炼性。全真祖师[王重阳曾在此山中收](../Page/王重阳.md "wikilink")[全真七子为徒](../Page/全真七子.md "wikilink")，开创了[全真教](../Page/全真教.md "wikilink")。始建于800多年之前的[神清观是全真教的祖庭](../Page/神清观.md "wikilink")，在道教历史上拥有重要的地位，是我国道教宗奉的圣地。[无染寺始建于东汉永康年间](../Page/无染寺.md "wikilink")，曾经是香火盛极一时的胶东第一古刹。今多数寺观已毁于[文革期间](../Page/文革.md "wikilink")，神清观现已重建修复。

## 仙女麻姑的传说

相传东汉女子[麻姑](../Page/麻姑.md "wikilink")，经[王母娘娘指点](../Page/王母娘娘.md "wikilink")，来此山中修行。数年后，终于[昆嵛山中得道成仙](../Page/昆嵛山.md "wikilink")。

## 地理环境

昆嵛山属[长白山系](../Page/长白山系.md "wikilink")、[崂山山脉](../Page/崂山山脉.md "wikilink")。岩石主要为[寒武纪的古火山](../Page/寒武纪.md "wikilink")[侵入岩](../Page/侵入岩.md "wikilink")，以[花岗岩分布最广](../Page/花岗岩.md "wikilink")，[片麻岩和](../Page/片麻岩.md "wikilink")[石英岩亦有少量分布](../Page/石英岩.md "wikilink")。土壤类型属于[森林棕壤](../Page/森林棕壤.md "wikilink")。以沙质壤为主，成土母质主要为花岗岩，结构疏松，层次不分明，呈酸性或微酸性。气候属[暖温带海洋性季风气候](../Page/暖温带.md "wikilink")。年平均温度11.8°C，年降水量800-1000mm，无霜期200-220天。一年四季分明，雨量比较充沛，气候温和，可满足暖温带各种植物生长的需要。

区域的植物种类繁多，区系成分比较复杂，是山东植物资源最丰富的基因库。植物品种1000多种。有狼、梅花鹿等兽鸟200多种，昆嵛山是中国南北植物的交汇点，有分布于中国最北界的[刺杉](../Page/刺杉.md "wikilink")，最南界的[赤松](../Page/赤松.md "wikilink")。

## 地理位置

昆嵛山国家森林公园地理位置处于E121°40￠342-121°48￠042，N37°11￠502-37°17￠222。东西13公里，南北14公里，总面积71,000余亩。

[K](../Category/中国山峰.md "wikilink") [K](../Category/威海地理.md "wikilink")
[K](../Category/烟台地理.md "wikilink") [K](../Category/文登市.md "wikilink")
[K](../Category/牟平区.md "wikilink")