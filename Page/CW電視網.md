[总裁](../Page/总裁.md "wikilink"){{\\}}[CEO](../Page/CEO.md "wikilink")）</small>
}} | replaced = [WB电视网](../Page/WB电视网.md "wikilink")
[联合派拉蒙电视网](../Page/联合派拉蒙电视网.md "wikilink") | servicename1 = 图像格式 |
service1 =
[480i](../Page/480i.md "wikilink")（[标清电视](../Page/标清电视.md "wikilink")）
[1080i](../Page/1080i.md "wikilink"){{\\}}[720p](../Page/720p.md "wikilink")（[高清电视](../Page/高清电视.md "wikilink")）
| servicename2 = 主要语言 | service2 = [英语](../Page/英语.md "wikilink") |
website =  }}

**CW电视网**（The CW Television Network
也称为：**CW电视台**）是[美國的一家電視台](../Page/美國.md "wikilink")。

## 简介

CW电视台開播于2006年9月18日，由[哥伦比亚广播公司](../Page/哥伦比亚广播公司.md "wikilink")（CBS）及[華納兄弟](../Page/華納兄弟.md "wikilink")（Warnar
Bros.）共同經營。其前身是[華納電視台](../Page/WB電視台.md "wikilink") (The WB)
與[聯合派拉蒙電視台](../Page/UPN.md "wikilink") (UPN)
，由于這兩家電視台長期低收視率而決定合併成新的[電視網CW電視台](../Page/電視聯播網.md "wikilink")。其名称"CW"源自**C**BS与**W**B的開頭字母。

2006年9月15至17日,The WB和UPN分別關閉；次日 (9月18日) CW电视台正式开播。

由于CW电视台的“CW”来源如果对调就成了“WC电视台”；而CW电视台成立后取消了很多收视主体人群年龄偏大的剧集，主打年轻人，现今剧集收视人群偏向年轻人\[1\]，因此CW电视台也被网络上一些华语社区的网友称为“厕所台”或“青春台”\[2\]。

**CW電視台**播放过比较有代表性的剧集有《[七重天](../Page/七重天.md "wikilink")》、《[吉尔莫女孩](../Page/吉尔莫女孩.md "wikilink")》、《[超人前傳](../Page/超人前傳.md "wikilink")》、《[篮球兄弟](../Page/篮球兄弟.md "wikilink")》、《[邪恶力量](../Page/邪恶力量.md "wikilink")》、《[花邊教主](../Page/花邊教主.md "wikilink")》、《[新飞跃比弗利](../Page/新飞跃比弗利.md "wikilink")》、《[吸血鬼日记](../Page/吸血鬼日记.md "wikilink")》及《[不期而至](../Page/不期而至.md "wikilink")》等；比较有代表性的[情景喜剧有](../Page/情景喜剧.md "wikilink")《[人人都恨克里斯](../Page/人人都恨克里斯.md "wikilink")》和《[橄榄球之恋](../Page/橄榄球之恋.md "wikilink")》；真人秀有《[全美超级模特儿新秀大赛](../Page/全美超模.md "wikilink")》和《[世界摔跤联合会-激爆职业摔跤](../Page/世界摔跤联合会-激爆职业摔跤.md "wikilink")》。

## 时间表

**时间表节目背景色含义**

  -

    普通剧集

    该季度新剧

    真人秀

### 2010年秋季節目播放時間表

| CW  | 8:00 p.m.                                                           | 8:30 p.m.                                              | 9:00 p.m. | 9:30 p.m. |
| --- | ------------------------------------------------------------------- | ------------------------------------------------------ | --------- | --------- |
| 星期一 | [新飞跃比弗利](../Page/新飞跃比弗利.md "wikilink") (90210)                      | [花邊教主](../Page/花邊教主.md "wikilink") (Gossip Girl)       |           |           |
| 星期二 | [篮球兄弟](../Page/篮球兄弟.md "wikilink") (One Tree Hill)                  | [不期而至](../Page/不期而至.md "wikilink") (Life Unexpected)   |           |           |
| 星期三 | [超級名模生死鬥](../Page/超級名模生死鬥.md "wikilink") (America's Next Top Model) | 啦啦小野貓 (Hellcats)                                       |           |           |
| 星期四 | [吸血鬼日記](../Page/吸血鬼日記.md "wikilink") (The Vampire Diaries)          | [尼基塔](../Page/尼基塔_\(2010年电视剧\).md "wikilink") (Nikita) |           |           |
| 星期五 | [超人前傳](../Page/超人前傳.md "wikilink") (Smallville)                     | [邪恶力量](../Page/邪恶力量.md "wikilink") (Supernatural)      |           |           |

### 2011年春季節目播放時間表

| CW  | 8:00 p.m.                                                           | 8:30 p.m.                                              | 9:00 p.m. | 9:30 p.m. |
| --- | ------------------------------------------------------------------- | ------------------------------------------------------ | --------- | --------- |
| 星期一 | [新飞跃比弗利](../Page/新飞跃比弗利.md "wikilink") (90210)                      | [花邊教主](../Page/花邊教主.md "wikilink") (Gossip Girl)       |           |           |
| 星期二 | [篮球兄弟](../Page/篮球兄弟.md "wikilink") (One Tree Hill)                  | 啦啦小野貓 (Hellcats)                                       |           |           |
| 星期三 | [超級名模生死鬥](../Page/超級名模生死鬥.md "wikilink") (America's Next Top Model) | 为结婚掉层肉 (Shedding for the Wedding)                      |           |           |
| 星期四 | [吸血鬼日記](../Page/吸血鬼日記.md "wikilink") (The Vampire Diaries)          | [尼基塔](../Page/尼基塔_\(2010年电视剧\).md "wikilink") (Nikita) |           |           |
| 星期五 | [超人前傳](../Page/超人前傳.md "wikilink") (Smallville)                     | [邪恶力量](../Page/邪恶力量.md "wikilink") (Supernatural)      |           |           |

### 2011年秋季節目播放時間表

| CW  | 8:00 p.m.                                                  | 8:30 p.m.                                                           | 9:00 p.m. | 9:30 p.m. |
| --- | ---------------------------------------------------------- | ------------------------------------------------------------------- | --------- | --------- |
| 星期一 | [花邊教主](../Page/花邊教主.md "wikilink") (Gossip Girl)           | [南国医恋](../Page/南国医恋.md "wikilink") (Hart of Dixie)                  |           |           |
| 星期二 | [新飞跃比弗利](../Page/新飞跃比弗利.md "wikilink") (90210)             | [替身姐妹](../Page/替身姐妹.md "wikilink") (Ringer)                         |           |           |
| 星期三 | H8R                                                        | [超級名模生死鬥](../Page/超級名模生死鬥.md "wikilink") (America's Next Top Model) |           |           |
| 星期四 | [吸血鬼日記](../Page/吸血鬼日記.md "wikilink") (The Vampire Diaries) | [神秘圈](../Page/神秘圈.md "wikilink") (The Secret Circle)                |           |           |
| 星期五 | [尼基塔](../Page/尼基塔_\(2010年电视剧\).md "wikilink") (Nikita)     | [邪恶力量](../Page/邪恶力量.md "wikilink") (Supernatural)                   |           |           |

### 2012年春季節目播放時間表

| CW  | 8:00 p.m.                                                  | 8:30 p.m.                                            | 9:00 p.m. | 9:30 p.m. |
| --- | ---------------------------------------------------------- | ---------------------------------------------------- | --------- | --------- |
| 星期一 | [花邊教主](../Page/花邊教主.md "wikilink") (Gossip Girl)           | [南国医恋](../Page/南国医恋.md "wikilink") (Hart of Dixie)   |           |           |
| 星期二 | [新飞跃比弗利](../Page/新飞跃比弗利.md "wikilink") (90210)             | [替身姐妹](../Page/替身姐妹.md "wikilink") (Ringer)          |           |           |
| 星期三 | [篮球兄弟](../Page/篮球兄弟.md "wikilink") (One Tree Hill)         | ReModeled                                            |           |           |
| 星期四 | [吸血鬼日記](../Page/吸血鬼日記.md "wikilink") (The Vampire Diaries) | [神秘圈](../Page/神秘圈.md "wikilink") (The Secret Circle) |           |           |
| 星期五 | [尼基塔](../Page/尼基塔_\(2010年电视剧\).md "wikilink") (Nikita)     | [邪恶力量](../Page/邪恶力量.md "wikilink") (Supernatural)    |           |           |

## 参考文献

## 外部連結

  - [官方網站](http://www.cwtv.com/)
  - [官方網站](http://www.yourcwtv.com/)（CW+）
  - [CW电视网新闻稿发布页面](http://www.cwtvpr.com/)

[Category:美國電視台](../Category/美國電視台.md "wikilink")

1.  [2010-2011美剧收视人群平均年龄最高最低Top15](http://news.mtime.com/2011/06/02/1460930.html)
2.  [好汉贤妻罪案大爆炸
    CBS2011秋季档期出炉](http://news.mtime.com/2011/06/30/1463526.html)