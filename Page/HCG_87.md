**HCG
87**是登錄在[希克斯緻密星系群目錄上的一個緻密](../Page/希克斯緻密星系群.md "wikilink")[星系團](../Page/星系團.md "wikilink")，這個集團位於[摩羯座](../Page/摩羯座.md "wikilink")，距離大約是4億[光年遠](../Page/光年.md "wikilink")。

## 成員

<table>
<caption><strong>HCG 87的成員</strong></caption>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p><a href="../Page/星系分類.md" title="wikilink">類型</a><ref name="ned2">{{cite web</p></th>
<th><p>title=NASA/IPAC Extragalactic Database</p></th>
<th><p>work=Results for various galaxies</p></th>
<th><p>url=<a href="http://nedwww.ipac.caltech.edu/">http://nedwww.ipac.caltech.edu/</a></p></th>
<th><p>accessdate=2006-10-19}}</ref></p></th>
<th><p><a href="../Page/赤經.md" title="wikilink">R.A.</a>（<a href="../Page/J2000.md" title="wikilink">J2000</a>）[1]</p></th>
<th><p><a href="../Page/赤緯.md" title="wikilink">Dec.</a>（<a href="../Page/J2000.md" title="wikilink">J2000</a>）[2]</p></th>
<th><p><a href="../Page/紅移.md" title="wikilink">紅移</a>（<a href="../Page/公里.md" title="wikilink">公里</a>／秒）[3]</p></th>
<th><p><a href="../Page/視星等.md" title="wikilink">視星等</a>[4]</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/HCG_87a.md" title="wikilink">HCG 87a</a></p></td>
<td><p>S0 pec</p></td>
<td></td>
<td></td>
<td><p>8443 ± 14</p></td>
<td><p>15.3</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HCG_87b.md" title="wikilink">HCG 87b</a></p></td>
<td><p>SA(r)0 pec</p></td>
<td></td>
<td></td>
<td><p>8740 ± 20</p></td>
<td><p>15.4</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HCG_87c.md" title="wikilink">HCG 87c</a></p></td>
<td><p>Sb</p></td>
<td></td>
<td></td>
<td><p>8914 ± 7</p></td>
<td><p>16.1</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HCG_87d.md" title="wikilink">HCG 87d</a></p></td>
<td><p>Sd</p></td>
<td></td>
<td></td>
<td><p>10200 ± 160</p></td>
<td><p>17.8</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [NASA APOD: Galaxy Group HCG 87 -
    July 31, 2003](http://antwrp.gsfc.nasa.gov/apod/ap030731.html)
  - [Close-ups of
    HCG 87](http://heritage.stsci.edu/1999/31/closeup.html)
  - [Galactic
    Clusters](https://web.archive.org/web/20060503230456/http://www.cosmiclight.com/imagegalleries/gclusters.htm)
  - [Studies of Hickson Compact
    Groups](http://heritage.stsci.edu/1999/31/supplemental.html)

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:螺旋星系](../Category/螺旋星系.md "wikilink")
[87](../Category/希克斯緻密星系群.md "wikilink")
[Category:星系团](../Category/星系团.md "wikilink")
[Category:摩羯座](../Category/摩羯座.md "wikilink")

1.
2.
3.
4.