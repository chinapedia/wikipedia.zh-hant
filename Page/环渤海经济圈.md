**环渤海经济区**指[渤海周边地区构成的经济带](../Page/渤海.md "wikilink")，为[欧亚大陆桥东部起点之一](../Page/欧亚大陆桥.md "wikilink")。狭义上指[京津冀地区](../Page/京津冀城市群.md "wikilink")、[山东半岛地区和](../Page/山东半岛蓝色经济区.md "wikilink")[辽中南地区构成的](../Page/辽中南地区.md "wikilink")[经济圈](../Page/经济圈.md "wikilink")；广义上包括[内蒙古自治区中部和](../Page/内蒙古自治区.md "wikilink")[山西省](../Page/山西省.md "wikilink")。区域协调组织有[环渤海区域合作市长联席会](../Page/环渤海区域合作市长联席会.md "wikilink")。

該經濟區跨越[華北地區](../Page/華北地區.md "wikilink")、[東北地區以及](../Page/東北地區.md "wikilink")[華東地區](../Page/華東地區.md "wikilink")；其包括[北京市](../Page/北京市.md "wikilink")、[瀋陽市](../Page/瀋陽市.md "wikilink")、[天津市](../Page/天津市.md "wikilink")、[石家莊市](../Page/石家莊市.md "wikilink")、[青島市](../Page/青島市.md "wikilink")、[大連市](../Page/大連市.md "wikilink")、[濟南市等位于渤海沿岸的城市和位於](../Page/濟南市.md "wikilink")[黃海沿岸但环绕渤海的沿海城市](../Page/黃海.md "wikilink")。\[1\]。

## 主要组成城市

  - [直辖市](../Page/直辖市.md "wikilink")：[北京市](../Page/北京市.md "wikilink")、[天津市](../Page/天津市.md "wikilink")
  - [副省級城市和](../Page/副省級城市.md "wikilink")[計劃單列市](../Page/計劃單列市.md "wikilink")：[瀋陽市](../Page/瀋陽市.md "wikilink")、[济南市](../Page/济南市.md "wikilink")、[青岛市](../Page/青岛市.md "wikilink")、[大连市](../Page/大连市.md "wikilink")
  - [辽宁省](../Page/辽宁省.md "wikilink")：[丹东市](../Page/丹东市.md "wikilink")、[营口市](../Page/营口市.md "wikilink")、[盘锦市](../Page/盘锦市.md "wikilink")、[阜新市](../Page/阜新市.md "wikilink")、[锦州市](../Page/锦州市.md "wikilink")、[葫芦岛市](../Page/葫芦岛市.md "wikilink")、[朝阳市](../Page/朝阳市.md "wikilink")
  - [河北省](../Page/河北省.md "wikilink")：[石家庄市](../Page/石家庄市.md "wikilink")、[保定市](../Page/保定市.md "wikilink")、[唐山市](../Page/唐山市.md "wikilink")、[秦皇岛市](../Page/秦皇岛市.md "wikilink")、[沧州市](../Page/沧州市.md "wikilink")、[廊坊市](../Page/廊坊市.md "wikilink")、[承德市](../Page/承德市.md "wikilink")、[张家口市](../Page/张家口市.md "wikilink")、[邯郸市](../Page/邯郸市.md "wikilink")、[邢台市](../Page/邢台市.md "wikilink")、[衡水市](../Page/衡水市.md "wikilink")
  - [山东省](../Page/山东省.md "wikilink")：[烟台市](../Page/烟台市.md "wikilink")、[潍坊市](../Page/潍坊市.md "wikilink")、[威海市](../Page/威海市.md "wikilink")、[日照市](../Page/日照市.md "wikilink")、[东营市](../Page/东营市.md "wikilink")、[滨州市](../Page/滨州市.md "wikilink")

## 交通

### 航空

  - [北京首都国际机场](../Page/北京首都国际机场.md "wikilink")、[天津滨海国际机场](../Page/天津滨海国际机场.md "wikilink")、[沈阳桃仙国际机场](../Page/沈阳桃仙国际机场.md "wikilink")、[大连周水子国际机场](../Page/大连周水子国际机场.md "wikilink")、[石家庄正定国际机场](../Page/石家庄正定国际机场.md "wikilink")、[唐山三女河机场](../Page/唐山三女河机场.md "wikilink")、[秦皇岛北戴河机场](../Page/秦皇岛北戴河机场.md "wikilink")、[邯郸机场](../Page/邯郸机场.md "wikilink")、[张家口宁远机场](../Page/张家口宁远机场.md "wikilink")、[济南遥墙国际机场](../Page/济南遥墙国际机场.md "wikilink")、[青岛流亭国际机场](../Page/青岛流亭国际机场.md "wikilink")、[烟台莱山国际机场](../Page/烟台莱山国际机场.md "wikilink")、[東營勝利機場](../Page/東營勝利機場.md "wikilink")、[日照山字河机场](../Page/日照山字河机场.md "wikilink")、[北京南苑机场等](../Page/北京南苑机场.md "wikilink")

### 港口

  - [丹东港](../Page/丹东港.md "wikilink")、[大连港](../Page/大连港.md "wikilink")、[营口港](../Page/营口港.md "wikilink")、[秦皇岛港](../Page/秦皇岛港.md "wikilink")、[唐山港](../Page/唐山港.md "wikilink")、[天津港](../Page/天津港.md "wikilink")、[黄骅港](../Page/黄骅港.md "wikilink")、[龙口港](../Page/龙口港.md "wikilink")、[烟台港](../Page/烟台港.md "wikilink")
    、[青岛港](../Page/青岛港.md "wikilink")、[董家口港](../Page/董家口港.md "wikilink")、[日照港](../Page/日照港.md "wikilink")、[葫芦岛港等](../Page/葫芦岛港.md "wikilink")

### 铁路

  - 高速铁路：[京津城际铁路](../Page/京津城际铁路.md "wikilink")、[京石高铁](../Page/京石高铁.md "wikilink")、[津秦客运专线](../Page/津秦客运专线.md "wikilink")、[胶济客运专线](../Page/胶济客运专线.md "wikilink")、[环渤海城际铁路](../Page/环渤海城际铁路.md "wikilink")、[青荣城际铁路](../Page/青荣城际铁路.md "wikilink")、[津保高铁](../Page/津保铁路.md "wikilink")、[石济高铁等](../Page/石济客运专线.md "wikilink")
  - 普速铁路：[京沪铁路](../Page/京沪铁路.md "wikilink")、[胶济铁路](../Page/胶济铁路.md "wikilink")、[京广铁路](../Page/京广铁路.md "wikilink")、[京九铁路](../Page/京九铁路.md "wikilink")、[京哈铁路](../Page/京哈铁路.md "wikilink")、[石德铁路等](../Page/石德铁路.md "wikilink")

天津夜景航拍20110419.JPG|太空拍攝的天津

## 相關條目

  - [长江三角洲经济圈](../Page/长江三角洲经济圈.md "wikilink")
  - [珠三角经济圈](../Page/珠三角经济圈.md "wikilink")
  - [京津冀一體化](../Page/京津冀一體化.md "wikilink")
  - [京津冀协同发展](../Page/京津冀协同发展.md "wikilink")
  - [珠江三角洲](../Page/珠江三角洲.md "wikilink")
  - [成渝城市群](../Page/成渝城市群.md "wikilink")

## 参考文献

<references/>

## 外部鏈接

  - [央視-京津冀一體化](http://news.cntv.cn/2015/08/24/VIDE1440418405692286.shtml)

[Category:山东经济](../Category/山东经济.md "wikilink")
[Category:辽宁经济](../Category/辽宁经济.md "wikilink")
[Category:河北经济](../Category/河北经济.md "wikilink")
[Category:天津经济](../Category/天津经济.md "wikilink")
[Category:北京市经济](../Category/北京市经济.md "wikilink")
[Category:渤海](../Category/渤海.md "wikilink")
[Category:中国城市群](../Category/中国城市群.md "wikilink")
[Category:中国经济地理大区](../Category/中国经济地理大区.md "wikilink")

1.  [环渤海经济圈介绍：天然的聚宝盆](http://he.ce.cn/ztpd/hbyhkfx/yhxt/201305/26/t20130526_910421.shtml)
     中国经济网 2014-06-08