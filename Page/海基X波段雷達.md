[Blue_Marlin_transporting_sea-based_X-band_radar.jpg](https://zh.wikipedia.org/wiki/File:Blue_Marlin_transporting_sea-based_X-band_radar.jpg "fig:Blue_Marlin_transporting_sea-based_X-band_radar.jpg")（MV
Blue
Marlin）載運，進入[珍珠港進行運補](../Page/珍珠港.md "wikilink")，並在之後繼續前往[阿拉斯加](../Page/阿拉斯加.md "wikilink")[艾達克島](../Page/艾達克島.md "wikilink")。\]\]
**海基X波段雷達**（Sea-Based X-Band Radar,
**SBX**），是一種可在遠海的大型風浪中維持運作、以[螺旋槳推進的自走機動](../Page/螺旋槳.md "wikilink")[X波段](../Page/X波段.md "wikilink")[雷達站](../Page/雷達.md "wikilink")，是[美國國家飛彈防禦署](../Page/美國.md "wikilink")[陸基中段防禦系統](../Page/陸基中段防禦系統.md "wikilink")（NMD）的一環。

海基X波段雷達安裝在一個[挪威設計](../Page/挪威.md "wikilink")、[俄國製造的第五代可半潛雙船體](../Page/俄國.md "wikilink")[鑽油平台](../Page/鑽油平台.md "wikilink")。鑽油平台的改造在[德州](../Page/德州.md "wikilink")[布朗斯維爾的](../Page/布朗斯維爾_\(德克薩斯州\).md "wikilink")[AmFELS造船廠完成](../Page/吉寶AmFELS造船廠.md "wikilink")，雷達的製造和安裝則是在德州完成。此雷達將部署在[阿拉斯加](../Page/阿拉斯加.md "wikilink")[艾達克島](../Page/艾達克島.md "wikilink")（Adak
Island），但也可以漫遊太平洋以偵測[彈道飛彈的入侵](../Page/彈道飛彈.md "wikilink")。

## 基本資料

  - **平台長度**: 116公尺（380英尺）
  - **平台高度**: 85公尺（280英尺）從[龍骨至雷達罩頂端](../Page/龍骨.md "wikilink")
  - **價格**: 九億美金
  - **成員編制**: 約75名
  - **雷達探測距離**: 2000公里（1242英里）
  - **排水量**：50000吨

## 詳細介紹

此雷達包含許多小的[雷達罩和一個重](../Page/雷達罩.md "wikilink")1814公噸（4百萬磅）的[相位陣列雷達天線](../Page/相位陣列.md "wikilink")，此相位陣列雷達佔地384平方公尺，擁有超過3萬組傳送接收（T/R）模組，這些傳送接收模組大間格分佈，這種分佈方式使雷達可以追蹤極遠距離的目標，以支援[戰區高度區域防禦系統](../Page/戰區高度區域防禦系統.md "wikilink")（THAAD）外大氣層目標導引所需，這個陣列雷達需要超過一百萬瓦的電力運作。

此雷達從[神盾戰鬥系統使用的雷達變化而來](../Page/神盾戰鬥系統.md "wikilink")，是[美國](../Page/美國.md "wikilink")[飛彈防禦局](../Page/飛彈防禦局.md "wikilink")（MDA）為防禦彈道飛彈而部署。為了有效對應短程與較小型的彈道飛彈，此雷達與神盾系統的一個重要不同在於使用高解析度的[X波段雷達](../Page/X波段.md "wikilink")。
神盾系統使用[S波段雷達](../Page/S波段.md "wikilink")，[愛國者飛彈系統使用](../Page/愛國者飛彈.md "wikilink")[C波段雷達](../Page/C波段.md "wikilink")。此平台是[雷神公司的防衛系統部門為](../Page/雷神公司.md "wikilink")[波音公司設計和製造的](../Page/波音公司.md "wikilink")，波音公司是飛彈防禦局此計劃的主要合約商。

此平台是飛彈防禦局[陸基中途防禦系統](../Page/陸基中途防禦系統.md "wikilink")（GMD）的一部份，身為海基使得此平台可以移動到需要加強飛彈防禦的地區，因為地球曲率的關係，固定式雷達能含蓋的範圍有很大限制。

第一艘此類船隻將部署在阿拉斯加艾達克島，此島是[阿留申群島的一部分](../Page/阿留申群島.md "wikilink")，在這個位置她可以追蹤在[北韓和](../Page/北韓.md "wikilink")[中國上方的飛彈](../Page/中國.md "wikilink")。雖然她的母港是在阿拉斯加，但她會被付予需要移動到[太平洋各地完成的任務](../Page/太平洋.md "wikilink")。

按照飛彈防禦局局長崔·歐貝林（Trey
Obering）中將的形容，SBX可以從[契沙比克灣追蹤在](../Page/契沙比克灣.md "wikilink")[舊金山上方](../Page/舊金山.md "wikilink")[棒球大小的物體](../Page/棒球.md "wikilink")（距離約），此雷達將導引美國從阿拉斯加和[加州發射的飛彈](../Page/加州.md "wikilink")，以及戰區內的軍事裝備。

從2002年開始，此系統已經完成六次成功攔截測試。

2015年[洛杉磯時報報導稱在對數千頁專家報告](../Page/洛杉磯時報.md "wikilink")、國會聽證、各部委文件和對數十名國防及航天技術領域專家採訪的基礎上做出結論，即「海基X波段雷達」是一沒有效率的系統。原應在2005年列裝，但至今仍停在夏威夷的[珍珠港待命](../Page/珍珠港.md "wikilink")。\[1\]\[2\]

<File:Sbx> underway.jpg|在海上以自身動力前進中的SBX。 <File:US> Navy
060131-N-8157C-019 The Sea-Based Radar(SBX) successfully completed its
15,000-mile journey from Texas to Pearl Harbor aboard the heavy lift
vessel M-V Blue
Marlin.jpg|2006年1月時，正在[珍珠港內進行小幅度改裝的SBX](../Page/珍珠港.md "wikilink")。
<File:SBX> Pearl Harbor.jpg|在歷經為期三個月的整補檢修之後，SBX於2006年3月31日啟程離開珍珠港。

## 參考文獻

## 外部連結

  - [United States Missile Defense
    Agency](https://web.archive.org/web/20060301165436/http://www.mda.mil/)
  - [Boeing Multimedia Sea-Based X-band Radar Image
    Gallery](http://www.boeing.com/defense-space/space/gmd/gallery/photos1.html)
  - [Sea-Based X-Band Radar Arrives in Pearl
    Harbor](http://www.news.navy.mil/search/display.asp?story_id=21914),
    2006-01-10
  - [– MDA announces arrival of SBX at Pearl Harbor,
    Hawaii](https://web.archive.org/web/20060316131932/http://www.mda.mil/mdalink/pdf/05news0013.pdf)
    (PDF)
  - [About Raytheon IDS](http://www.raytheon.com/businesses/rids/about/)

[Category:軍用雷達](../Category/軍用雷達.md "wikilink")
[Category:美國軍事](../Category/美國軍事.md "wikilink")

1.  楊家鑫,"外媒：五角大廈耗資682億的雷達沒有效率"[1](http://www.chinatimes.com/realtimenews/20150406001535-260408),中國時報,2015年04月06日09:59.
2.  David William/Reporting from Washington,"The Pentagon's $10-billion
    bet gone bad - Los Angeles
    Times"[2](http://graphics.latimes.com/missile-defense/),[洛杉磯時報](../Page/洛杉磯時報.md "wikilink"),APRIL
    5,2015.