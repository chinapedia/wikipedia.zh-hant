[1965-4_1965年安徽宿县顺河人民公社.jpg](https://zh.wikipedia.org/wiki/File:1965-4_1965年安徽宿县顺河人民公社.jpg "fig:1965-4_1965年安徽宿县顺河人民公社.jpg")
**宿县**，中国旧县名，今[安徽省](../Page/安徽省.md "wikilink")[宿州市](../Page/宿州市.md "wikilink")[埇桥区和](../Page/埇桥区.md "wikilink")[淮北市的前身](../Page/淮北市.md "wikilink")。

宿县及前身[宿州的名称起源于](../Page/宿州_\(古代\).md "wikilink")[春秋时期的](../Page/春秋时期.md "wikilink")“[宿国](../Page/宿国.md "wikilink")”，当时是一个[驿站](../Page/驿站.md "wikilink")。民国初年，全国[废府州厅改县](../Page/废府州厅改县.md "wikilink")，于1912年改[宿州为宿县](../Page/宿州_\(古代\).md "wikilink")。

1949年置[宿县专区](../Page/宿县专区.md "wikilink")，属[皖北行署区](../Page/皖北行署区.md "wikilink")，专员公署驻**宿县**（今[宿州市](../Page/宿州市.md "wikilink")[埇桥区](../Page/埇桥区.md "wikilink")）。辖[宿县](../Page/宿县.md "wikilink")、[灵璧](../Page/灵璧县.md "wikilink")、[泗县](../Page/泗县.md "wikilink")、[泗洪](../Page/泗洪县.md "wikilink")、[五河](../Page/五河县.md "wikilink")、[怀远](../Page/怀远县.md "wikilink")、[砀山](../Page/砀山县.md "wikilink")、[萧县](../Page/萧县.md "wikilink")(萧砀2县1953年3月6日，复属[江苏省](../Page/江苏省.md "wikilink")[徐州专区](../Page/徐州专区.md "wikilink")；1955年2月17日改属安徽省宿县专署)、[永城](../Page/永城县.md "wikilink")9县。1950年7月1日，以宿县西境析置[宿西县](../Page/宿西县.md "wikilink")，同年改名[濉溪县](../Page/濉溪县.md "wikilink")(1971年4月，[濉溪市更名为](../Page/濉溪市.md "wikilink")[淮北市](../Page/淮北市.md "wikilink"))；由**宿县**析设[宿城市](../Page/宿城市.md "wikilink")。

1952年，宿县专区属[安徽省](../Page/安徽省.md "wikilink")。同年，永城县划归[河南省](../Page/河南省.md "wikilink")[商丘专区](../Page/商丘专区.md "wikilink")；砀山、萧县2县划归[江苏省](../Page/江苏省.md "wikilink")[徐州专区](../Page/徐州专区.md "wikilink")。1953年，撤销宿城市，并入**宿县**，[宿县专区辖](../Page/宿县专区.md "wikilink")7县。1955年，原属江苏省徐州专区的砀山、萧县2县县划入；[泗洪县划归江苏省](../Page/泗洪县.md "wikilink")[淮阴专区](../Page/淮阴专区.md "wikilink")。1956年，撤销[宿县专区](../Page/宿县专区.md "wikilink")，所辖8县划归[蚌埠专区](../Page/蚌埠专区.md "wikilink")。

1961年，复设宿县专区，专署驻宿县，辖原蚌埠专区所属宿县、灵璧、泗县、五河、怀远、濉溪、萧县、砀山8县。1964年，析宿县、灵璧、五河、怀远4县设[固镇县](../Page/固镇县.md "wikilink")。宿县专区辖9县。

1971年，撤销宿县专区，改置[宿县地区](../Page/宿县地区.md "wikilink")。
1977年2月，原属[宿县地区的](../Page/宿县地区.md "wikilink")[濉溪县划归](../Page/濉溪县.md "wikilink")[淮北市](../Page/淮北市.md "wikilink")。1981年，由[萧县](../Page/萧县.md "wikilink")[龙城镇](../Page/龙城镇_\(萧县\).md "wikilink")，析出[段园镇划入淮北煤矿](../Page/段园镇.md "wikilink")（今[淮北市](../Page/淮北市.md "wikilink")[杜集区的飞地](../Page/杜集区.md "wikilink")）。

宿县这个名称一直使用到1990年代撤县併市，[宿县地区相当于今天的宿州市和](../Page/宿县地区.md "wikilink")[淮北市](../Page/淮北市.md "wikilink")，宿县相当于今天的[埇桥区和](../Page/埇桥区.md "wikilink")[淮北市](../Page/淮北市.md "wikilink")。自此之后，**宿县**名称就消失了。

宿县，为世人所知的事情，在古代有[大泽乡的](../Page/大泽乡.md "wikilink")[陈胜吴广起义](../Page/陈胜吴广起义.md "wikilink")，[项羽](../Page/项羽.md "wikilink")[垓下落败的地方](../Page/垓下.md "wikilink")，[竹林七贤中的](../Page/竹林七贤.md "wikilink")[嵇康](../Page/嵇康.md "wikilink")[刘伶也是宿县人](../Page/刘伶.md "wikilink")，以及“蓠蓠原上草，一岁一枯荣”（[白居易诗](../Page/白居易.md "wikilink")，描写的是现在称为符离的区域）。

在近代有[赛珍珠以此地为蓝本所写长篇小说](../Page/赛珍珠.md "wikilink")《[大地](../Page/大地_\(小说\).md "wikilink")》（1938年[诺贝尔文学奖](../Page/诺贝尔文学奖.md "wikilink")）。[赛珍珠笔下用的是宿县方言读法](../Page/赛珍珠.md "wikilink")：nan
xu
zhou。即“南徐州”。因为宿县地处[徐州之南几十公里](../Page/徐州.md "wikilink")，而宿县读法近徐州，故称“南徐州”。再近一点，则有淮海战役之[双堆集战役](../Page/双堆集战役.md "wikilink")。

宿县的方言属于北方语系[中原官话](../Page/中原官话.md "wikilink")，据考证，[天津方言源自宿县方言](../Page/天津方言.md "wikilink")。生活风俗习惯大体同北方。

[Category:中华民国安徽省县份](../Category/中华民国安徽省县份.md "wikilink")
[Category:已撤消的中华人民共和国安徽省县份](../Category/已撤消的中华人民共和国安徽省县份.md "wikilink")
[Category:宿州行政区划史](../Category/宿州行政区划史.md "wikilink")
[Category:淮北行政区划史](../Category/淮北行政区划史.md "wikilink")
[Category:埇桥区](../Category/埇桥区.md "wikilink")
[Category:濉溪县](../Category/濉溪县.md "wikilink")
[Category:1912年建立的行政区划](../Category/1912年建立的行政区划.md "wikilink")
[Category:1992年废除的行政区划](../Category/1992年废除的行政区划.md "wikilink")