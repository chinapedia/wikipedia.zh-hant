**鮑屬**（學名：*Haliotis*），古稱**鳆**、**鰒魚**、**海耳**，又名**鏡面魚**、**九孔螺**、**明目魚**，是**鮑科**（Haliotidae）唯一的一個屬。它是一類[海洋](../Page/海洋生物.md "wikilink")[腹足綱](../Page/腹足綱.md "wikilink")[軟體動物](../Page/軟體動物.md "wikilink")，棲石質河岸，以[藻類為食](../Page/藻類.md "wikilink")。

鮑魚是[中國](../Page/中國.md "wikilink")“四大海味”之一。新朝[王莽就愛吃鳆鱼](../Page/王莽.md "wikilink")。\[1\][曹操喜欢吃鳆鱼](../Page/曹操.md "wikilink")，曹操死后，[曹植曾向](../Page/曹植.md "wikilink")[臧霸要二百只來祭祀](../Page/臧霸.md "wikilink")。\[2\]
鮑魚通常在水溫度稍低的海底出產。出產地有[日本北部](../Page/日本.md "wikilink")、中國東北部、广东南海北部湾、[北美洲西岸](../Page/北美洲.md "wikilink")、[南美洲](../Page/南美洲.md "wikilink")、[南非](../Page/南非.md "wikilink")、[澳洲等地](../Page/澳洲.md "wikilink")。公認最佳產地為日本（乾鮑）及[墨西哥](../Page/墨西哥.md "wikilink")（罐頭鮑）。宋朝时日式鲍鱼已输入中国，時稱“倭螺”，[苏轼有](../Page/苏轼.md "wikilink")《鳆鱼行》诗：“东随海舶号倭螺，异方珍宝来更多。”

自古以来人类就有捕捞鲍鱼的行业，但近代以来，由于自然环境变化加剧，鲍的自然资源日益减少\[3\]。

## 食用

鮑魚是四大海味：鮑、[參](../Page/海參.md "wikilink")、[翅](../Page/魚翅.md "wikilink")、[肚之首](../Page/花膠.md "wikilink")。種類甚多，有少部份鮑魚新鮮急凍，亦有製成罐頭，或曬製成乾貨。當中乾鮑按一斤重量有多少個分為「十頭」、「八頭」等，十頭即指一斤內有十個。頭數越少，鮑魚越大，亦越昂貴。另外，[台灣與](../Page/台灣.md "wikilink")[中國大陸之](../Page/中国大陆.md "wikilink")「[斤](../Page/斤.md "wikilink")」定義有別。在[台灣與](../Page/台灣.md "wikilink")[日本一斤為](../Page/日本.md "wikilink")600公克。在中國大陸，一市斤為500公克。同稱「十頭」之鮑魚於大陸會較細小。

台灣產的[九孔](../Page/九孔螺.md "wikilink")（Haliotis diversicolor
supertexta）又稱台灣珍珠鮑魚、台灣鮑魚（Taiwanese abalone）或九孔。

### 乾鮑

上等鮑魚常製成**乾鮑**，一般為「熟曬」：先連瞉用水煮熟或蒸熟後，才開殼取肉曬製。

#### 常見的乾鮑

  - 網鮑
  - 吉品鮑
      - 又名吉濱鮑，產自日本[岩手縣](../Page/岩手縣.md "wikilink")[三陸町的](../Page/三陸町.md "wikilink")[吉濱](../Page/吉濱.md "wikilink")。
  - 禾麻鮑
      - 又稱窩麻鮑，產自日本[青森縣的](../Page/青森縣.md "wikilink")[大間崎](../Page/大間崎.md "wikilink")（Omazaki），兩端有繩孔、肉質柔軟為其特點。
  - 中東鮑
  - 南非鮑
  - 大連鮑
  - 蘇洛鮑、印尼鮑

### 溏心鮑魚

「溏心」是指乾鮑中心部份呈不凝結的半液體狀態，將乾鮑煮至中心部份黏黏軟軟，入口時質感柔軟及有韌度，要製作溏心鮑魚需要經過多次曬乾的程序。鮮鮑魚並沒有溏心。新鮮鮑魚則多以清[蒸方式處理](../Page/蒸.md "wikilink")。

### 鮑魚殼

即[石决明](../Page/石决明.md "wikilink")，洗淨後曬乾即可入藥

## 物種

[Chineseabalonecuisine.jpg](https://zh.wikipedia.org/wiki/File:Chineseabalonecuisine.jpg "fig:Chineseabalonecuisine.jpg")
，[WoRMS紀錄鮑屬包括約](../Page/WoRMS.md "wikilink")60個物種，以下為部分物種或鮑魚的品種：

  - [澳洲鮑魚](../Page/澳洲鮑魚.md "wikilink") *Haliotis australis*
  - *Haliotis ancile*
  - [細紋九孔螺](../Page/細紋九孔螺.md "wikilink") *Haliotis aquatilis*
  - [驢耳鮑螺](../Page/驢耳鮑螺.md "wikilink") *Haliotis asinina*
  - *Haliotis barbouri*
  - *Haliotis brazieri*
  - [格鮑](../Page/格鮑.md "wikilink") *Haliotis clathrata*
  - *Haliotis coccoradiata*
  - [旋風鮑螺](../Page/旋風鮑螺.md "wikilink") *Haliotis conicopora*
  - [桃紅鮑螺](../Page/桃紅鮑螺.md "wikilink") *Haliotis corrugata*
  - [黑鮑螺](../Page/黑鮑螺.md "wikilink") *Haliotis cracherodii*
  - [密彫鮑螺](../Page/密彫鮑螺.md "wikilink") *Haliotis crebrisculpta*
  - [颱風眼鮑螺](../Page/颱風眼鮑螺.md "wikilink") *Haliotis cyclobates*
  - *Haliotis dalli*
  - [盤鮑螺](../Page/盤鮑螺.md "wikilink") *Haliotis discus*
  - *Haliotis dissona*
  - [台灣鮑魚](../Page/台灣鮑魚.md "wikilink") *Haliotis diversicolor
    supertexta*

<!-- end list -->

1.  新北市貢寮區曾是全國臺灣聞名的九孔
    \[台灣鮑魚[1](http://www.setn.com/Mvnews.aspx?NewsID=71327)\]
2.  全國臺灣新品種藍殼九孔鮑
    \[藍殼九孔鮑記錄片[2](https://www.youtube.com/watch?v=DyOtPR82MEc)\]

<!-- end list -->

  - [九孔螺](../Page/九孔螺.md "wikilink") *Haliotis diversicolor*
  - [南洋鮑螺](../Page/南洋鮑螺.md "wikilink") *Haliotis dohrniana*
  - [優雅鮑螺](../Page/優雅鮑螺.md "wikilink") *Haliotis elegans*
  - [塔斯馬尼亞鮑螺](../Page/塔斯馬尼亞鮑螺.md "wikilink") *Haliotis emmae*
  - *Haliotis ethologus*
  - *Haliotis exigua*
  - *Haliotis fatui*
  - [孔雀鮑螺](../Page/孔雀鮑螺.md "wikilink") *Haliotis fulgens*
  - [大鮑螺](../Page/大鮑螺.md "wikilink") *Haliotis gigantea*
  - [菲律賓鮑螺](../Page/菲律賓鮑螺.md "wikilink") *Haliotis glabra*
  - *Haliotis hargravesi*
  - *Haliotis howensis*
  - [紐西蘭鮑螺](../Page/紐西蘭鮑螺.md "wikilink") *Haliotis iris*
  - [侏儒鮑螺](../Page/侏儒鮑螺.md "wikilink")*Haliotis jacnensis*
  - [堪察加鮑螺](../Page/堪察加鮑螺.md "wikilink") *Haliotis kamtschatkana*
  - *Haliotis kamtschatkana assimilis*
  - *Haliotis kamtschatkana kamtschatkana*
  - [平滑鮑螺](../Page/平滑鮑螺.md "wikilink") *Haliotis laevigata*
  - [日本鮑螺](../Page/日本鮑螺.md "wikilink") *Haliotis madaka*
  - *Haliotis mariae*
  - *Haliotis melculus*
  - *Haliotis marfaloni*
  - [南非鮑螺](../Page/南非鮑螺.md "wikilink") *Haliotis midae*
  - *Haliotis multiperforata*
  - [圓鮑螺](../Page/圓鮑螺.md "wikilink")*Haliotis ovina*
  - *Haliotis parva*
  - [泰國鮑螺](../Page/泰國鮑螺.md "wikilink") *Haliotis patamakanthini*
  - [扁鮑螺](../Page/扁鮑螺.md "wikilink") *Haliotis planata*
  - [巴西鮑螺](../Page/巴西鮑螺.md "wikilink") *Haliotis pourtalesii*
  - [集美鮑螺](../Page/集美鮑螺.md "wikilink") *Haliotis pulcherrima*
  - [青苔鮑螺](../Page/青苔鮑螺.md "wikilink") *Haliotis pustulata*
  - [麻繩鮑螺](../Page/麻繩鮑螺.md "wikilink") *Haliotis queketti*
  - *Haliotis roberti*
  - [花輪鮑螺](../Page/花輪鮑螺.md "wikilink") *Haliotis roei*
  - *Haliotis rosacea*
  - *Haliotis rubiginosa*
  - [黑鮑](../Page/黑鮑.md "wikilink") *Haliotis rubra*
  - [紅鮑螺](../Page/紅鮑螺.md "wikilink") *Haliotis rufescens*
  - *Haliotis rugosa*
  - [旋梯鮑螺](../Page/旋梯鮑螺.md "wikilink") *Haliotis scalaris*
  - *Haliotis semiplicata*
  - [白色鮑魚](../Page/白色鮑魚.md "wikilink") *Haliotis sorenseni*
  - *Haliotis spadicea*
  - *Haliotis speciosa*
  - *Haliotis squamata*
  - [粗彫鮑螺](../Page/粗彫鮑螺.md "wikilink") *Haliotis squamosa*
  - *Haliotis thailandis*
  - [歐洲鮑螺](../Page/歐洲鮑螺.md "wikilink") *Haliotis tuberculata*
  - *Haliotis unilateralis*
  - [瘤鮑螺](../Page/瘤鮑螺.md "wikilink") *Haliotis varia*
  - [優美鮑螺](../Page/優美鮑螺.md "wikilink") *Haliotis venusta*
  - [少女鮑螺](../Page/少女鮑螺.md "wikilink") *Haliotis virginea*
  - [北國鮑螺](../Page/北國鮑螺.md "wikilink") *Haliotis walallensis*
  - [螺紋鮑螺](../Page/螺紋鮑螺.md "wikilink") *Haliotis assimilis*

## 逸事

  - 《史記》載[秦始皇去世時](../Page/秦始皇.md "wikilink")，丞相[李斯秘不发丧](../Page/李斯.md "wikilink")，利用一石鮑魚以亂其臭\[4\]。這裡的鮑魚是指[鹹魚](../Page/鹹魚.md "wikilink")、醃魚，非現今的鮑魚；又如"入鮑魚之肆，久而不覺其臭"亦同。[明朝时](../Page/明朝.md "wikilink")，逐渐改称鳆鱼为鲍鱼。[谢肇淛](../Page/谢肇淛.md "wikilink")《五杂俎》卷九记载：“鳆音扑，入声，今人读作鲍”。

## 俗语

  - 由於外形相似，鮑魚在香港、日本和臺灣文化中亦被用作[女陰的](../Page/女陰.md "wikilink")[諱語](../Page/諱語.md "wikilink")。
  - 鮑魚在粵語是汽車的[煞車系統](../Page/煞車.md "wikilink")／[懸掛系統的俗稱](../Page/懸掛系統.md "wikilink")。

## 圖集

<center>

<File:White> abalone Haliotis sorenseni.jpg|*Haliotis sorenseni*
<File:Pāua> foot.JPG|*Haliotis sp.* <File:Haliotis> diversicolor
01.jpg|*Haliotis diversicolor* <File:Blacklip> abalone.jpg|*Haliotis
rubra* [File:AbaloneMeat.jpg|鮑肉](File:AbaloneMeat.jpg%7C鮑肉)
[File:AbaloneOutside.jpg|鮑魚殼外](File:AbaloneOutside.jpg%7C鮑魚殼外)
[File:AbaloneInside.jpg|鮑魚殼內](File:AbaloneInside.jpg%7C鮑魚殼內)

</center>

## 参考

  -
  - [鑑定海味及滋補食品平台之初階段開發](http://www.hkctc.gov.hk/en/doc/DNA_marker_sequence_database.pdf)

### 引用

## 外部連結

  -
{{-}}

[鮑屬](../Category/鮑屬.md "wikilink")
[Category:海味](../Category/海味.md "wikilink")
[Category:單屬科軟體動物](../Category/單屬科軟體動物.md "wikilink")
[Category:可食用軟體動物](../Category/可食用軟體動物.md "wikilink")

1.  《[汉书](../Page/:s:漢書.md "wikilink")·王莽传》:王莽事将败，悉不下饭，唯饮酒，啖鳆鱼。
2.  《[太平御覽](../Page/:s:太平御覽.md "wikilink")·[卷九百三十八·鳞介部十](../Page/:s:太平御覽/0938.md "wikilink")·[鰒魚條](../Page/:s:太平御覽/0938#鰒魚〈步角反〉.md "wikilink")》：陈思王《求祭先主表》曰：先主喜食鳆鱼，前已表徐州臧霸送鳆鱼二百，足自供事。
3.
4.  《史記·秦始皇本紀》：“七月丙寅，始皇崩於沙丘平臺。……棺載轀涼車中，……抵九原。會暑，上轀車臭，乃詔從官令車載一石鮑魚，以亂其臭。”