**辛丑**为[干支之一](../Page/干支.md "wikilink")，顺序为第38个。前一位是[庚子](../Page/庚子.md "wikilink")，后一位是[壬寅](../Page/壬寅.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之辛屬陰之金](../Page/天干.md "wikilink")，[地支之](../Page/地支.md "wikilink")-{丑}-屬陰之土，是土生金相生。

## 辛丑年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")38年称“**辛丑年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘41，或年份數減3，除以10的餘數是8，除以12的餘數是2，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“辛丑年”：

<table>
<caption><strong>辛丑年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/41年.md" title="wikilink">41年</a></li>
<li><a href="../Page/101年.md" title="wikilink">101年</a></li>
<li><a href="../Page/161年.md" title="wikilink">161年</a></li>
<li><a href="../Page/221年.md" title="wikilink">221年</a></li>
<li><a href="../Page/281年.md" title="wikilink">281年</a></li>
<li><a href="../Page/341年.md" title="wikilink">341年</a></li>
<li><a href="../Page/401年.md" title="wikilink">401年</a></li>
<li><a href="../Page/461年.md" title="wikilink">461年</a></li>
<li><a href="../Page/521年.md" title="wikilink">521年</a></li>
<li><a href="../Page/581年.md" title="wikilink">581年</a></li>
<li><a href="../Page/641年.md" title="wikilink">641年</a></li>
<li><a href="../Page/701年.md" title="wikilink">701年</a></li>
<li><a href="../Page/761年.md" title="wikilink">761年</a></li>
<li><a href="../Page/821年.md" title="wikilink">821年</a></li>
<li><a href="../Page/881年.md" title="wikilink">881年</a></li>
<li><a href="../Page/941年.md" title="wikilink">941年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1001年.md" title="wikilink">1001年</a></li>
<li><a href="../Page/1061年.md" title="wikilink">1061年</a></li>
<li><a href="../Page/1121年.md" title="wikilink">1121年</a></li>
<li><a href="../Page/1181年.md" title="wikilink">1181年</a></li>
<li><a href="../Page/1241年.md" title="wikilink">1241年</a></li>
<li><a href="../Page/1301年.md" title="wikilink">1301年</a></li>
<li><a href="../Page/1361年.md" title="wikilink">1361年</a></li>
<li><a href="../Page/1421年.md" title="wikilink">1421年</a></li>
<li><a href="../Page/1481年.md" title="wikilink">1481年</a></li>
<li><a href="../Page/1541年.md" title="wikilink">1541年</a></li>
<li><a href="../Page/1601年.md" title="wikilink">1601年</a></li>
<li><a href="../Page/1661年.md" title="wikilink">1661年</a></li>
<li><a href="../Page/1721年.md" title="wikilink">1721年</a></li>
<li><a href="../Page/1781年.md" title="wikilink">1781年</a></li>
<li><a href="../Page/1841年.md" title="wikilink">1841年</a></li>
<li><a href="../Page/1901年.md" title="wikilink">1901年</a></li>
<li><a href="../Page/1961年.md" title="wikilink">1961年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2021年.md" title="wikilink">2021年</a></li>
<li><a href="../Page/2081年.md" title="wikilink">2081年</a></li>
<li><a href="../Page/2141年.md" title="wikilink">2141年</a></li>
<li><a href="../Page/2201年.md" title="wikilink">2201年</a></li>
<li><a href="../Page/2261年.md" title="wikilink">2261年</a></li>
<li><a href="../Page/2321年.md" title="wikilink">2321年</a></li>
<li><a href="../Page/2381年.md" title="wikilink">2381年</a></li>
<li><a href="../Page/2441年.md" title="wikilink">2441年</a></li>
<li><a href="../Page/2501年.md" title="wikilink">2501年</a></li>
<li><a href="../Page/2561年.md" title="wikilink">2561年</a></li>
<li><a href="../Page/2621年.md" title="wikilink">2621年</a></li>
<li><a href="../Page/2681年.md" title="wikilink">2681年</a></li>
<li><a href="../Page/2741年.md" title="wikilink">2741年</a></li>
<li><a href="../Page/2801年.md" title="wikilink">2801年</a></li>
<li><a href="../Page/2861年.md" title="wikilink">2861年</a></li>
<li><a href="../Page/2921年.md" title="wikilink">2921年</a></li>
<li><a href="../Page/2981年.md" title="wikilink">2981年</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 1901年 - [辛丑條約](../Page/辛丑條約.md "wikilink")

## 辛丑月

天干丙年和辛年，[小寒到](../Page/小寒.md "wikilink")[立春的時間段](../Page/立春.md "wikilink")，就是**辛丑月**：

  - ……
  - [1977年](../Page/1977年.md "wikilink")1月小寒到2月立春
  - [1982年](../Page/1982年.md "wikilink")1月小寒到2月立春
  - [1987年](../Page/1987年.md "wikilink")1月小寒到2月立春
  - [1992年](../Page/1992年.md "wikilink")1月小寒到2月立春
  - [1997年](../Page/1997年.md "wikilink")1月小寒到2月立春
  - [2002年](../Page/2002年.md "wikilink")1月小寒到2月立春
  - [2007年](../Page/2007年.md "wikilink")1月小寒到2月立春
  - ……

## 辛丑日

## 辛丑時

天干丁日和壬日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）1時到3時，就是**辛丑時**。

## 辛丑年辛丑月辛丑日

  - 1902年1月18日
  - 2082年2月2日

## 參考資料

1.
\]

[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/辛丑年.md "wikilink")