**安藤百福**（，原名**吳百福**，），是一位生於[日治臺灣](../Page/日治臺灣.md "wikilink")[嘉義廳](../Page/嘉義廳.md "wikilink")[樸仔腳街的](../Page/朴子市.md "wikilink")[華裔](../Page/華裔.md "wikilink")[日籍](../Page/日本華人.md "wikilink")[企業家與](../Page/企業家.md "wikilink")[發明家](../Page/發明家.md "wikilink")，曾協助改良麵條製成現代化的[泡麵](../Page/泡麵.md "wikilink")，並創辦[日清食品公司](../Page/日清食品.md "wikilink")，[泡麵和](../Page/即食麵.md "wikilink")[杯麵的主要發明人](../Page/杯麵.md "wikilink")，有「速食麵之父」的稱號\[1\]。

## 生平

[Cupnoodles_seafood_taste.jpg](https://zh.wikipedia.org/wiki/File:Cupnoodles_seafood_taste.jpg "fig:Cupnoodles_seafood_taste.jpg")
安藤百福，原名為吳百福，為台灣嘉義人。生於[明治43年](../Page/明治43年.md "wikilink")（1910年）[日治臺灣](../Page/日治臺灣.md "wikilink")[嘉義廳樸仔腳支廳樸仔腳區](../Page/嘉義廳.md "wikilink")[樸仔腳街](../Page/樸仔腳街.md "wikilink")（今[中華民國](../Page/中華民國.md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")[朴子市](../Page/朴子市.md "wikilink")）。父親是吳獅玉（又名吳阿獅）、母親是吳千綠，自幼父母雙亡。在[臺南廳](../Page/臺南廳.md "wikilink")[臺灣府城](../Page/臺灣府城.md "wikilink")（今[臺南](../Page/臺南.md "wikilink")）由經營[布料批发店的祖父吳武照顧之下成長](../Page/布料.md "wikilink")。[昭和7年](../Page/昭和7年.md "wikilink")（1932年），22歳的安藤百福以父亲的遗产为本钱，在台北永樂町的市場经营纤维公司「东洋莫大小」。當時，處理[針織品的商社稀少](../Page/針織品.md "wikilink")，安藤的事业因此得以扩大規模，[昭和8年](../Page/昭和8年.md "wikilink")（1933年）他在[大阪設立日東商會](../Page/大阪.md "wikilink")。安藤经营針織品貿易、[光學機器和精密](../Page/光學.md "wikilink")[機械的製造](../Page/機械.md "wikilink")，在公司擴大規模的期間，他同时就讀於[立命館大學附屬商專的經濟科](../Page/立命館大學.md "wikilink")。

[第二次世界大戰中的](../Page/第二次世界大戰.md "wikilink")[空襲炸毁了安藤的事務所和](../Page/空襲.md "wikilink")[工廠](../Page/工廠.md "wikilink")，爾後[日本向同盟國](../Page/日本.md "wikilink")[投降](../Page/投降.md "wikilink")，臺灣出身者需在日本及中華民國之間選擇，吳百福選擇中華民國國籍，後來1966年才以第三任妻子安藤仁子之姓，[歸化日本籍](../Page/归化.md "wikilink")，成為日本公民\[2\]。安藤轉換事業跑道，開始經營[百貨公司和](../Page/百貨公司.md "wikilink")[食品事業](../Page/食品.md "wikilink")。[昭和23年](../Page/昭和23年.md "wikilink")（1948年）設立[中交總社](../Page/中交總社.md "wikilink")（現在的日清食品），在[大阪府南部的](../Page/大阪府.md "wikilink")[海岸上排列鐵板](../Page/海岸.md "wikilink")，用倒海水製造鹽的獨特方法進行製[鹽事業](../Page/食鹽.md "wikilink")。

當時，[厚生省](../Page/厚生省.md "wikilink")（今厚生勞動省）得到了美國的援助物資—[小麥粉來製造](../Page/小麥.md "wikilink")[麵包](../Page/麵包.md "wikilink")，並舉行做麵包、吃麵包的獎勵運動。安藤對「吃麵包獎」表示不滿，認為既然同樣是以小麥作為原料，應該以日本的傳統[麵食為獎勵](../Page/面食.md "wikilink")，並對厚生省職員進行質詢。該職員表示，日本的供給體制尚有問題，允許以安藤自己的力量加以推廣。但當時的安藤没有充裕的資金來進行其它事業，計劃也就無疾而终。

安藤向[信用合作社懇求金援并得到了同意](../Page/信用合作社.md "wikilink")，但此信用合作社於[昭和32年](../Page/昭和32年.md "wikilink")（1957年）倒閉，負擔[無限責任的理事長安藤被迫賣掉自己的事業償還負債](../Page/無限責任.md "wikilink")，只留下了位于大阪府[池田市自宅](../Page/池田市.md "wikilink")。

安藤於是在自宅的庭園中建了小房以研究速食麵（泡麵），並與其他同鄉台灣人陳榮泰、張國文等一起研製這項新產品（因此有說法是泡麵的實際發明人為屏東人張國文，只是他後來將專利權賣給安藤）。[雞味泡麵](../Page/雞味泡麵.md "wikilink")（）產品於[昭和33年](../Page/昭和33年.md "wikilink")（1958年）8月25日成功批發製作，速食麵瞬時成為了受歡迎的商品。同年12月，變更[商號為](../Page/商號.md "wikilink")[日清食品公司](../Page/日清食品.md "wikilink")，公司順利擴大事業。[昭和38年](../Page/昭和38年.md "wikilink")（1963年），日清食品在[東京證券交易所及](../Page/東京證券交易所.md "wikilink")[大阪證券交易所上市](../Page/大阪證券交易所.md "wikilink")。

由於速食麵的廣受好評，出現大量仿製者，安藤因此致力於申請[商標和](../Page/商標.md "wikilink")[專利](../Page/專利.md "wikilink")，保持公司[信用](../Page/信用.md "wikilink")。日清食品于[昭和36年](../Page/昭和36年.md "wikilink")（1961年）正式登記「」商標，[昭和37年](../Page/昭和37年.md "wikilink")（1962年）得到速食麵的製造[專利](../Page/專利.md "wikilink")，並對113家仿製公司發出警告。但[昭和39年](../Page/昭和39年.md "wikilink")（1964年）安藤中止了專利壟斷，設立日本拉麵工業協會，轉讓並公開專利\[3\]。

安藤開始考慮將泡麵打入美國市場，但在[昭和41年](../Page/昭和41年.md "wikilink")（1966年）赴美時，他注意到日本與美國從基本的[餐具使用上便不同](../Page/餐具.md "wikilink")，於是安藤產生了用纸杯來泡麵的想法。[昭和46年](../Page/昭和46年.md "wikilink")（1971年）9月18日，在世界首次發售纸杯麵條「日清杯裝速食麵」，打開日本以外的市場。

[平成11年](../Page/平成11年.md "wikilink")（1999年），為紀念安藤業績的「即食麵發明紀念館」在[大阪府](../Page/大阪府.md "wikilink")[池田市設立](../Page/池田市.md "wikilink")。[平成18年](../Page/平成18年.md "wikilink")（2006年）7月28日，紀念館設立六年半後，入場者突破了100萬人。2017年更名為[安藤百福發明紀念館
大阪池田](../Page/安藤百福發明紀念館_大阪池田.md "wikilink")。

[平成17年](../Page/平成17年.md "wikilink")（2005年）6月，安藤時年95歲，他以「日清食品已培育年輕經營團隊，可放心託付經營。我想在自己還有活力之時進行交棒」的理由，在6月29日卸任[取締役](../Page/取締役.md "wikilink")，改任創業者[會長](../Page/會長.md "wikilink")。安藤表示，自己的健康秘訣是每週一次的[高爾夫球和每日必食的](../Page/高爾夫球.md "wikilink")[雞味泡麵](../Page/雞味泡麵.md "wikilink")。

[平成19年](../Page/平成19年.md "wikilink")（2007年）1月5日日本時間傍晚6時40分，安藤因急性[心肌梗塞](../Page/心肌梗塞.md "wikilink")，於大阪府池田市市立池田病院去世，享壽97歲。

他本身亦是日本即食食品工業協會會長、安藤體育與飲食文化振興財團理事長、漢方醫藥研究振興財團會長、世界[拉麵協會會長](../Page/拉面.md "wikilink")、[平成8年](../Page/平成8年.md "wikilink")（1996年）成為[立命館大學名譽博士](../Page/立命館大學.md "wikilink")。

2010年（平成22年）是安藤百福100歲冥誕\[4\]、發售記念商品\[5\]\[6\]\[7\]\[8\]。

2011年9月17日，[橫濱市](../Page/橫濱市.md "wikilink")[中區的](../Page/中區.md "wikilink")「安藤百福發明紀念館」開館。2017年更名為。

2015年3月5日，[Google在首页放置](../Page/Google.md "wikilink")[涂鸦](../Page/Doodle.md "wikilink")，纪念安藤诞辰105周年\[9\]。

2016年7月5日，日清食品特別做了一個動畫廣告片，將安藤百福被塑造為[武士的模樣以紀念安藤百福](../Page/武士.md "wikilink")，內容大致上是安藤百福發明泡麵的過程\[10\]。

## 獲得榮譽

  - 藍綬褒章（[昭和52年](../Page/昭和52年.md "wikilink")，1977年）
  - 勋二等[瑞宝章](../Page/瑞寶大綬章.md "wikilink")（[昭和57年](../Page/昭和57年.md "wikilink")，1982年）
  - 紺綬褒章（[昭和59年](../Page/昭和59年.md "wikilink")，1983年）
  - 科學技術廳長官賞「功勞者賞」（[平成4年](../Page/平成4年.md "wikilink")，1992年）
  - 勋二等旭日重光章（[平成14年](../Page/平成14年.md "wikilink")，2002年）
  - [正四位](../Page/正四位.md "wikilink")（[平成19年](../Page/平成19年.md "wikilink")，2007年）

## 語錄

  - 企業在人，成業在天。
  - 企業家須有挑戰精神；沒有這種精神，就不會搞出熱門商品。

## 著作

  - 《》（2006年、PHP研究所）
  - 《》（2005年、中央公論新社）
  - 《》（2002年、日本経済新聞社）
  - 《》（1998年、日清、共著：奥村彪生、安藤百福）
  - 《》（1999年、旭屋出版）
  - 《》（1992年、日清）
  - 《》（1991年、日清）
  - 《》（1990年、日清）
  - 《》（1989年、日清）
  - 《》（1988年、講談社）
  - 《》（1987年、日清食品）
  - 《》（1987年、講談社）
  - 《》（1985年、講談社）
  - 《》（1983年、講談社）

## 參見條目

  - [出前一丁](../Page/出前一丁.md "wikilink")
  - [安藤百福奖](../Page/安藤百福奖.md "wikilink")
  - [安藤百福發明紀念館](../Page/安藤百福發明紀念館.md "wikilink")
      - [安藤百福發明紀念館 大阪池田](../Page/安藤百福發明紀念館_大阪池田.md "wikilink")

      -
  - [晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")：[開朗家族](../Page/開朗家族.md "wikilink")
  - [晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")：[萬福](../Page/萬福_\(電視劇\).md "wikilink")

## 參考

## 外部連結

  - [安藤百福照片](http://www.instantramen.or.jp/event/summit/img/ando.gif)
  - [速食麵之父的豪門恩怨 吳百福三妻一秘
    父子陌路](http://www.chinareviewnews.com/doc/23_296_100296586_1_0911123624.html)，[中國時報](../Page/中國時報.md "wikilink")2007年1月26日

  - [速食麵之父
    吳百福病逝](http://news.ltn.com.tw/news/focus/paper/110197)，[自由時報](../Page/自由時報.md "wikilink")2007年1月7日


[安藤百福](../Category/安藤百福.md "wikilink")
[Category:日本企业家](../Category/日本企业家.md "wikilink")
[Category:日本首席执行官](../Category/日本首席执行官.md "wikilink")
[Category:日本發明家](../Category/日本發明家.md "wikilink")
[Category:拉麵相關人物](../Category/拉麵相關人物.md "wikilink")
[Category:閩南人](../Category/閩南人.md "wikilink")
[Category:台灣裔日本人](../Category/台灣裔日本人.md "wikilink")
[Bai百福](../Category/吳姓.md "wikilink")
[Category:日清食品](../Category/日清食品.md "wikilink")
[安藤](../Category/幼年失親者.md "wikilink")
[Category:朴子人](../Category/朴子人.md "wikilink")
[Category:立命館大學校友](../Category/立命館大學校友.md "wikilink")
[Category:紺綬褒章獲得者](../Category/紺綬褒章獲得者.md "wikilink")
[Category:藍綬褒章獲得者](../Category/藍綬褒章獲得者.md "wikilink")
[Category:安藤家族](../Category/安藤家族.md "wikilink")

1.  [RTHK-香港故事(第19輯)07-速食速決-2012-4-02](http://www.youtube.com/watch?v=sb0LjDs1U2Q)
2.  官報 昭和41年3月1日
3.
4.  チキンラーメン、カップヌードルの各記念パッケージ（チキンラーメンは発売開始当時（1958年）の35円、カップヌードルは同（1971年）100円の特別価格が設定された）、特別企画商品「**百福長寿麺**」（鶏だし塩ラーメン、鴨だしそばの2種。麺の長さはカップ麺史上最長の100cm）。
5.  [-インスタントラーメンの父 安藤百福 生誕百年-
    記念商品の発売について](http://www.nissinfoods.co.jp/com/news/news_release.html?yr=2010&mn=1&nid=1789)
    - 日清食品株式会社、2010年1月12日
6.  [「即席めんの父」生誕100年　チキンラーメンを「35円」に](http://www.j-cast.com/mono/2010/01/13057809.html)
    - J-CASTニュース、2010年1月13日
7.  [日清食品が安藤百福生誕100周年記念配当](http://jp.ibtimes.com/article/biznews/100113/48037.html)
    - IBTimes、2010年1月13日
8.  [インスタント麺誕生100周年「百福長寿麺」](http://enjoy.nikkansports.com/topics/eat/1001130722.html)
     - 日刊スポーツ、2010年1月13日
9.  [Google Doodles: Momofuku Ando’s 105th
    Birthday](http://www.google.com/doodles/momofuku-andos-105th-birthday)
10. [SAMURAI NOODLES "THE
    ORIGINATOR"](https://www.youtube.com/watch?v=WNbb9qixsRQ)