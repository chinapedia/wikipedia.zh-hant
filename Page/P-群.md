在[數學裡](../Page/數學.md "wikilink")，給定一[質數](../Page/質數.md "wikilink")*p*，***p*-群**即是指一個其每個元素都有*p*的次方階的[週期群](../Page/週期群.md "wikilink")。亦即，對每個群內的元素*g*，都存在一個[正整數](../Page/正整數.md "wikilink")*n*使得*g*的*p<sup>n</sup>*[次方等於其](../Page/冪.md "wikilink")[單位元素](../Page/單位元素.md "wikilink")。

若*G*是有限的，則其會和*G*自身的[階為](../Page/階_\(群論\).md "wikilink")*p*的次方之敘述相等價。關於有限*p*-群的結構已知道了許多，其中第一個使用[類方程的標準結論為一個非當然有限](../Page/類方程.md "wikilink")*p*-群的[中心不可能為一個當然子群](../Page/中心_\(群論\).md "wikilink")。一個*p<sup>n</sup>*階的*p*-群會包含著*p<sup>i</sup>*階的子群，其中0
≤ *i* ≤
*n*。更一般性地，每一個有限*p*-群都會是[冪零群](../Page/冪零群.md "wikilink")，且因此都會是[可解群](../Page/可解群.md "wikilink")。

有相同階的*p*-群不一定會互相[同構](../Page/同構.md "wikilink")；例如，[循環群](../Page/循環群.md "wikilink")*C*<sub>4</sub>和[克萊因四元群都是](../Page/克萊因四元群.md "wikilink")4階的2-群，但兩者並不同構。一個*p*-群不一定要是[阿貝爾群](../Page/阿貝爾群.md "wikilink")；如8階的[二面體群即為一個非可換](../Page/二面體群.md "wikilink")2-群。（但每個*p*<sup>2</sup>階的群都會是可換的。）

以趨進的觀點來看，幾乎所有的有限群都會是*p*-群。實際上，幾乎所有的有限群都是2-群：2-群的[同構類與其階至多為](../Page/同構類.md "wikilink")*n*之群的同構類的比例在當*n*趨進於無限大時會趨進於1。例如，其階至多為2000的所有不同的群會有99%為1024階的2-群。\[1\]

每一個非當然有限群都會包括一個為非當然*p*-群之子群。詳述請見[西洛定理](../Page/西洛定理.md "wikilink")。

無限群的例子，見[普呂弗群](../Page/普呂弗群.md "wikilink")。

## 另見

  - [冪零群](../Page/冪零群.md "wikilink")
  - [西洛子群](../Page/西洛子群.md "wikilink")
  - [普呂弗秩](../Page/普呂弗秩.md "wikilink")
  - [超特別群](../Page/超特別群.md "wikilink")

## 參考

<references />

[Category:群論](../Category/群論.md "wikilink")

1.  Besche, Hans Ulrich, Bettina Eick and Eamonn O'Brien. (2001)
    [小群圖書館](http://www.tu-bs.de:8080/~hubesche/small.html)