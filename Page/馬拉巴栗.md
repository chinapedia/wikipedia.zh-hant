[Pachira_aquatica,_tree.jpg](https://zh.wikipedia.org/wiki/File:Pachira_aquatica,_tree.jpg "fig:Pachira_aquatica,_tree.jpg")
[Pachira_aquatica,_fruit.jpg](https://zh.wikipedia.org/wiki/File:Pachira_aquatica,_fruit.jpg "fig:Pachira_aquatica,_fruit.jpg")
**馬拉巴栗**（[學名](../Page/學名.md "wikilink")：**，），又名**發財樹**、**招財樹**、**美國花生**、**瓜栗**，属[木棉科](../Page/木棉科.md "wikilink")[馬拉巴栗属的一种植物](../Page/馬拉巴栗属.md "wikilink")。這個[屬的名稱取自](../Page/屬.md "wikilink")[圭亚那語](../Page/圭亚那.md "wikilink")，意思是「在水中」。馬拉巴栗原産於[中美洲及](../Page/中美洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")。

## 形态

常绿[乔木](../Page/乔木.md "wikilink")，树皮为绿色，干基部常膨大。掌状复叶，长椭圆形至狭倒卵形小叶5～9枚，革质有光泽。绿白色至黄白色杯状花，很少为浅红色。[蒴果长](../Page/蒴果.md "wikilink")10～30厘米，褐色球形种子，直径1～2厘米。

## 用途

常盆栽作为室内[觀葉植物](../Page/觀葉植物.md "wikilink")。[種子煮熟後可食用](../Page/種子.md "wikilink")，其味如[花生](../Page/花生.md "wikilink")。樹幹可製成[紙及漿糊](../Page/紙.md "wikilink")。南韓農業當局每天進行8小時的觀測，發現馬拉巴栗能減少一半以上[細懸浮微粒](../Page/細懸浮微粒.md "wikilink")。\[1\]

## 文化

### 在東南亞的角色

在[日本](../Page/日本.md "wikilink")，馬拉巴栗作為觀賞植物般培植已有很長歷史，但未受普及。在1986年，恰逢[韋恩颱風來襲](../Page/颱風韋恩_\(1986年\).md "wikilink")，[台灣的](../Page/台灣.md "wikilink")[貨櫃車](../Page/貨櫃車.md "wikilink")[司機王清富無法出車](../Page/司機.md "wikilink")，待在家中幫從事美髮工作的太太陳美華編[辮子](../Page/辮子.md "wikilink")。一時突發奇想，隨手把五棵馬拉巴栗種在同一盆子，並把它們的枝幹屈曲成辮子狀，改名為「發財樹」出售。這種紮枝的方法傳至[日本](../Page/日本.md "wikilink")，隨即受到熱烈的歡迎，此後成為[東南亞最普遍的觀賞植物之一](../Page/東南亞.md "wikilink")。在[中國這種植物象徵著財運](../Page/中國.md "wikilink")，故很多經營[商店的店主都在店內種植一棵](../Page/商店.md "wikilink")，有時候更在樹枝上綁上紅絲帶等不同的妝飾。馬拉巴栗在台灣有很重要的經濟地位，在2005年的[出口收入達](../Page/出口.md "wikilink")2億5千萬[新台幣](../Page/新台幣.md "wikilink")。

\[2\]

## 参考文献

<div class="references-small">

  -

</div>

  - 《台灣蔬果實用百科第三輯》，薛聰賢 著，薛聰賢出版社，2003年

## 外部链接

  - [Pachira Aquatica
    Care](http://www.houseintohome.co.za/plants/money-tree-pachira/)
  - [更詳細的馬拉巴栗介紹](http://digiarch.sinica.edu.tw/content.jsp?option_id=2442&index_info_id=1504)

[Category:瓜栗属](../Category/瓜栗属.md "wikilink")
[Category:觀葉植物](../Category/觀葉植物.md "wikilink")

1.
2.