「**Mars**」的英文意思為[火星](../Page/火星.md "wikilink")。「**Mars**」或「**MARS**」也可以指：

  - [海事移动接入与搜索系统](../Page/海事移动接入与搜索系统.md "wikilink")（**M**aritime mobile
    **A**ccess and **R**etrieval **S**ystem）

  - [机器检索系统](../Page/机器检索系统.md "wikilink")（**Ma**chinery **R**etrieval
    **S**ystem）

  - [管理分析报告系统](../Page/管理分析报告系统.md "wikilink")（**M**anagement
    **A**nalysis **R**eporting **S**ystem）

  - [存贮器地址寄存器存贮](../Page/存贮器地址寄存器存贮.md "wikilink")（**M**emory-**A**ddress
    **R**egister **S**torage）

  - [军事业余无线电爱好者网](../Page/军事业余无线电爱好者网.md "wikilink")（**M**ilitary
    **A**mateur **R**adio **S**ystem）

  - [多路存取检索系统](../Page/多路存取检索系统.md "wikilink")（**M**ultiple **A**ccess
    **R**etrieval **S**ystem）

  - [旅客販賣綜合系統](../Page/旅客販賣綜合系統.md "wikilink")（**M**ulti **A**ccess seat
    **R**eservation **S**ystem）：日本國鐵及JR集團所使用的旅客票務系統

  - [多孔磁阻开关](../Page/多孔磁阻开关.md "wikilink")（**M**ultiple-**A**pertured
    **R**eluctance **S**witch）

  - [最快存取检索系统](../Page/最快存取检索系统.md "wikilink")（**M**inimum **A**ccess
    **R**etrieval **S**ystem）

  - ，[IBM設計的](../Page/IBM.md "wikilink")[密碼](../Page/密碼.md "wikilink")

  - [MARS
    (專輯)](../Page/MARS_\(專輯\).md "wikilink")，[Gackt的](../Page/Gackt.md "wikilink")[音樂](../Page/音樂.md "wikilink")[專輯](../Page/音樂專輯.md "wikilink")

  - [MARS
    (漫畫)](../Page/MARS_\(漫畫\).md "wikilink")，由[惣領冬實創作的一系列](../Page/惣領冬實.md "wikilink")[日本](../Page/日本.md "wikilink")[漫畫](../Page/日本漫畫.md "wikilink")

  - [中產美國激進派](../Page/中產美國激進派.md "wikilink")/[戰神階級](../Page/戰神階級.md "wikilink")（Middle
    American
    Radicals），近年來美國有給貧窮者相當補貼及對有錢人減稅，但是中產階級獲得的減稅及補貼很少、又要承擔產業外移及各種政策的惡果，因此原本立場較溫和的中產階級開始激進化，全球都有類似狀況

  - [玛氏食品](../Page/玛氏食品.md "wikilink")（Mars, Inc），总部位于美国的世界级食品公司

  - 羅馬神話中的戰神[馬爾斯](../Page/馬爾斯.md "wikilink")(Mars)

## 另见

  - [火星 (消歧義)](../Page/火星_\(消歧義\).md "wikilink")