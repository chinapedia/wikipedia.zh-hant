[HK_Sheung_Wan_morning_Des_Voeux_Road_Central_富邦銀行_RMB_Fixed_Deposit_Rate_定期存款_Nov-2011.jpg](https://zh.wikipedia.org/wiki/File:HK_Sheung_Wan_morning_Des_Voeux_Road_Central_富邦銀行_RMB_Fixed_Deposit_Rate_定期存款_Nov-2011.jpg "fig:HK_Sheung_Wan_morning_Des_Voeux_Road_Central_富邦銀行_RMB_Fixed_Deposit_Rate_定期存款_Nov-2011.jpg")充裕時期，[銀行願意支付的存款利息並不高](../Page/銀行.md "wikilink")\]\]

**利息**，指[负债方为借债向](../Page/负债.md "wikilink")[债权人所付的补偿性费用](../Page/债权人.md "wikilink")。对于借债方来说，利息是借钱的代价；对于提供贷款或购买[债券的投资者来说](../Page/债券.md "wikilink")，利息可以部分抵消[债务投资的](../Page/债务.md "wikilink")[信用风险和](../Page/信用风险.md "wikilink")[机会成本](../Page/机会成本.md "wikilink")。

利息主要分為單利息和[複利息兩種](../Page/复利.md "wikilink")。

## 宗教的利息观

### 伊斯蘭教

[伊斯蘭教禁止收取利息](../Page/伊斯蘭教.md "wikilink")，《[古蘭經](../Page/古蘭經.md "wikilink")》第2章275節說「[真主准許](../Page/真主.md "wikilink")[買賣](../Page/買賣.md "wikilink")，而禁止利息」，又說知道此教義而再犯的人「是[火獄的居民](../Page/火獄.md "wikilink")，他們將永居其中」。

### 基督教

在[中世纪的](../Page/中世纪.md "wikilink")[欧洲](../Page/欧洲.md "wikilink")，[高利贷是罪恶的](../Page/高利贷.md "wikilink")，[天主教对此极力反对](../Page/天主教.md "wikilink")。在1179年3月召开的[第三次拉特朗公会议上](../Page/第三次拉特朗公会议.md "wikilink")，主教指高利贷是“人神共愤，受教义谴责”，与[基督教慈善之心相悖的](../Page/基督教.md "wikilink")“毒瘤”。本次大会制定了27条[教会法条款](../Page/教会法.md "wikilink")，其中第25条的内容，就是把高利贷的放贷者逐出教会。

当时的欧洲社会有[交易和](../Page/交易.md "wikilink")[买卖](../Page/买卖.md "wikilink")，但对经济的观念和金钱捆绑得不是十分紧密，世俗奉行“朋友间无需金钱”。放贷取息在当时被大众所厌弃，[莎士比亚的名著](../Page/莎士比亚.md "wikilink")《[威尼斯商人](../Page/威尼斯商人.md "wikilink")》中，就对放贷者[犹太人](../Page/犹太人.md "wikilink")[夏洛克进行了深刻地抨击和无情地嘲弄](../Page/威尼斯商人.md "wikilink")，夏洛克这一犹太食利者的形象坐上了[四大著名吝嗇鬼的头把交椅](../Page/四大著名吝嗇鬼_\(小說\).md "wikilink")。

而放贷取息的行为，也给犹太人在欧洲各国遭到不同程度的[歧视和](../Page/歧视.md "wikilink")[排挤](../Page/反犹太主义.md "wikilink")，甚至发展到[二战中遭](../Page/二战.md "wikilink")[纳粹](../Page/纳粹.md "wikilink")[大规模灭族](../Page/猶太人大屠殺.md "wikilink")，埋下了祸根。

### 佛教

## 参考

  - [孳息](../Page/孳息.md "wikilink")
  - [利率](../Page/利率.md "wikilink")
  - [复利](../Page/复利.md "wikilink")
  - [高利貸](../Page/高利貸.md "wikilink")

[Category:金融](../Category/金融.md "wikilink")
[Category:收入](../Category/收入.md "wikilink")