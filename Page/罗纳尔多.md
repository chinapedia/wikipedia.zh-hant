**-{zh-hans:罗纳尔多·路易斯·纳萨里奥·德·利马;zh-hk:朗拿度·路易斯·拿渣利奧·迪利馬;zh-tw:羅納度·路易斯·那扎里奧·德·利馬;}-**（，），[巴西著名的足球運動員](../Page/巴西國家足球隊.md "wikilink")，司职[前锋](../Page/前鋒_\(足球\).md "wikilink")。是世界球壇中的傳奇巨星之一。

\-{zh-hans:罗纳尔多;zh-hk:朗拿度;zh-tw:羅納度;}-是1990年代至2000年代最成功的足球运动员之一，亦被廣泛認為是[有史以來最偉大的足球運動員之一](https://en.wikipedia.org/wiki/List_of_association_football_players_considered_the_greatest_of_all_time)。因为其強悍恐怖的攻擊力被冠以“[外星人](../Page/外星人.md "wikilink")”称号。罗纳尔多曾三度當選[世界足球先生](../Page/世界足球先生.md "wikilink")、两度獲得[金球獎](../Page/金球奖_\(足球\).md "wikilink")，為巴西奪得兩次[世界盃冠軍及一次亞軍](../Page/世界盃足球賽.md "wikilink")，1998年當選世界盃最佳球員，2002年獲得[世界盃金靴獎](../Page/世界盃金靴獎.md "wikilink")。其中[2002年世界盃更加是罗纳尔多球員生涯的代表作](../Page/2002年世界盃足球賽.md "wikilink")。他於3屆世界盃賽事中取得15個入球，[2006年打破西德球員](../Page/2006年世界盃足球賽.md "wikilink")[蓋德·穆勒的紀錄](../Page/蓋德·穆勒.md "wikilink")，成為[累積最多入球的球員](../Page/世界盃足球賽紀錄#累積最多入球的球員.md "wikilink")，此紀錄於[2014年由德國球員](../Page/2014年世界盃足球賽.md "wikilink")[高路斯打破](../Page/高路斯.md "wikilink")。無論在球會及國家隊，他同樣能夠以出色的球技、驚人的[速度](../Page/速度.md "wikilink")（100米最快記錄為10.3秒）、強大的爆發力以及精準的射門令對手聞風喪膽並為球隊取得勝利。2010年朗拿度獲得巴西傳奇巨星獎。罗纳尔多于2011年2月14日退役。羅納爾多於2018年9月在購買巴利亞多利德51％的股份後，成為西甲俱樂部[巴利亞多利德的大股東](../Page/巴利亞多利德足球俱樂部.md "wikilink")。

## 生平

### 克鲁赛罗（1991～1995）

罗纳尔多出生于巴西大城市[里约热内卢的一個貧民區](../Page/里约热内卢.md "wikilink")，少年時已跟一众巴西人一樣，經常在街頭踢球。14歲時朗拿度被巴西名將[雅伊尔津霍發掘](../Page/雅伊尔津霍.md "wikilink")，並推薦到他曾經效力的[高士路](../Page/高士路.md "wikilink")，加盟他首間的職業足球會。1993年，他以16歲之齡，已為巴西17歲以下國家隊上陣7場，射入5球。

當時他的出色表現吸引多間歐洲大球會垂青。1993年12月首次入選巴西國家隊。[1994年世界盃他有份出席](../Page/1994年世界盃足球賽.md "wikilink")，但未有出場比賽。

### PSV燕豪芬、巴塞罗那（1995～1997）

朗拿度的職業生涯，在遠赴歐洲加盟[荷蘭班霸](../Page/荷蘭.md "wikilink")[PSV燕豪芬後](../Page/PSV燕豪芬.md "wikilink")，便正式起飛。之後轉會至[西班牙的](../Page/西班牙.md "wikilink")[巴塞罗那](../Page/巴塞罗那.md "wikilink")。

在巴塞罗那时期，朗拿度驚人的速度與技術令對手大感頭痛，並在37場比賽中共攻入34球，创造了当时[西甲联赛进球最多的记录](../Page/西甲联赛.md "wikilink")（直到10/11赛季才有人将此记录打破），罗纳尔多迅速成为世界足坛的焦点。在1996年10月12日的一场西甲联赛中，罗纳尔多半场连过5人后将球打进，时任巴萨主帅[波比·笠臣惊呼](../Page/波比·笠臣.md "wikilink")：“很难想象这样的进球是由[地球人打进的](../Page/地球人.md "wikilink")。”罗纳尔多由此得来“[外星人](../Page/外星人.md "wikilink")”的称号。96/97赛季的[欧洲优胜者杯决赛中](../Page/欧洲优胜者杯.md "wikilink")，罗纳尔多罚入点球，帮助巴萨击败[巴黎圣日耳曼夺得冠军](../Page/巴黎圣日耳曼.md "wikilink")。這期间朗拿度被FIFA評選為1996年[世界足球先生](../Page/世界足球先生.md "wikilink")。

不过罗纳尔多在巴萨时期有一些纪律问题，不但夜生活频繁，甚至在赛季中期不断以“疗养”为名请假回国（实为幽会女友），导致了俱乐部和球迷的不满。在一场[西班牙国家德比赛后](../Page/西班牙打吡.md "wikilink")，罗纳尔多与当时效力于[皇马的巴西老乡](../Page/皇家马德里足球俱乐部.md "wikilink")[罗伯托·卡洛斯交换球衣](../Page/罗伯托·卡洛斯.md "wikilink")，激起轩然大波，从此罗纳尔多在巴萨便再无立足之地。

### 國際米蘭（1997～2002）

[Ronaldo_(brazil)_(cropped).JPG](https://zh.wikipedia.org/wiki/File:Ronaldo_\(brazil\)_\(cropped\).JPG "fig:Ronaldo_(brazil)_(cropped).JPG")
1997年，[国际米兰以当时创纪录的](../Page/国际米兰.md "wikilink")3200万美元转会费用签下罗纳尔多。在加盟意甲的第一个赛季，罗纳尔多继续发挥神勇，44场比赛攻入34球，并获得了1997年的[世界足球先生](../Page/世界足球先生.md "wikilink")、[欧洲金球奖和](../Page/金球奖_\(足球\).md "wikilink")[欧洲金靴奖](../Page/欧洲金靴奖.md "wikilink")。罗纳尔多在[欧洲联盟杯上的精彩表现使其获得了该赛季的](../Page/欧洲联盟杯.md "wikilink")[欧足联最有价值球员](../Page/欧洲足联俱乐部足球奖.md "wikilink")。这个时期的罗纳尔多被公认为当时全世界最强的足球运动员。

[1998年世界盃朗拿度一度被外間認為會大放異彩](../Page/1998年世界盃足球賽.md "wikilink")，但在決賽前夕因為动脉疾病以致全身抽筋，后被队医误诊为[癫痫而开错药方](../Page/癫痫.md "wikilink")，\[1\]最终致使决赛时臨場表現失準，間接導致[巴西隊敗給](../Page/巴西國家足球隊.md "wikilink")[法國隊而失落冠軍](../Page/法國國家足球隊.md "wikilink")。不过凭借在赛事中其他场次的表现，罗纳尔多获得了1998年的[世界杯金球奖](../Page/世界杯金球奖.md "wikilink")。

从第二个赛季开始，频繁的俱乐部、国家队赛事令罗纳尔多身体日趋疲劳，加之意甲球员凶悍之作风，罗纳尔多开始遭遇膝伤困扰。1999年11月在与莱切的比赛中，罗纳尔多感到膝盖疼痛并被换下场，赛后被诊断为膝盖粉碎性骨折，并接受了手术。2000-01年球季朗拿度傷癒復出，首場比賽乃意大利杯對[拉齐奥](../Page/拉素.md "wikilink")，然而上陣僅7分鐘便再一次觸傷舊患，这一次罗纳尔多的右膝十字韧带完全断裂，結果再養傷长达一年半的时间。养伤期间有人断言称罗纳尔多将就此退役。

经过了一段艰苦的恢复期后，罗纳尔多在01/02赛季复出，状态迅速回升，重现高速盘带过人、大力抽射入球等绝技，并帮助国米成为该赛季联赛夺冠热门球队。然而时任国米主帅的[阿根廷人](../Page/阿根廷.md "wikilink")[库珀却开始处处针对罗纳尔多](../Page/埃克托·庫珀.md "wikilink")，包括不准他与队友一起训练、干扰其赴巴西国家队比赛等，为罗纳尔多日后出走买下伏笔。2002年5月5日意甲最后一轮，国际米兰客场2:4遭[拉齐奥逆转无缘冠军](../Page/拉齐奥.md "wikilink")，罗纳尔多被换下后坐在替补席上流泪的画面令全世界动容。

[2002年世界盃](../Page/2002年世界盃足球賽.md "wikilink")，他再一次入選[巴西國家隊](../Page/巴西國家足球隊.md "wikilink")，一般人都認為巴西這次的決定過於冒險，但結果卻出人意表。赛事中罗纳尔多势不可挡，一路过关斩将连续入球，帶領巴西殺入決賽，最終贏得世界杯冠軍。罗纳尔多在该届世界杯中入了8球，成為2002年[世界杯金靴奖得主](../Page/世界杯金靴奖.md "wikilink")。

### 皇家馬德里（2002～2007）

[Ronaldo_em_campo.jpg](https://zh.wikipedia.org/wiki/File:Ronaldo_em_campo.jpg "fig:Ronaldo_em_campo.jpg")
2002年世界杯后，罗纳尔多与国际米兰主教练库珀的矛盾已经完全爆发，其一度向俱乐部主席[莫拉蒂至最后通牒称](../Page/马西莫·莫拉蒂.md "wikilink")，要么解雇主教练库珀，要么让他转会。最终莫拉蒂选择让库珀留任，并在夏季转会窗口最后一天接受了[皇家马德里对罗纳尔多](../Page/皇家马德里.md "wikilink")3500万欧元的报价。罗纳尔多的离开招致了国际米兰球迷的激烈反应，但不论如何，他还是在一片争议声中转会皇家马德里。

在效力[西班牙](../Page/西班牙.md "wikilink")[皇家马德里之后](../Page/皇家马德里.md "wikilink")，为保护自己不受伤害，罗纳尔多改变了踢球风格，开始更倾向于利用嗅觉捕捉进球机会。不过这不影响罗纳尔多重回巅峰，02/03赛季44场比赛攻入30球，帮助球队夺得[西甲联赛冠军](../Page/西甲联赛.md "wikilink")，[洲際盃冠军](../Page/洲際盃足球賽.md "wikilink")，[西班牙超級盃冠軍](../Page/西班牙超級盃.md "wikilink")，并再度荣登世界足球先生的宝座。03/04赛季，罗纳尔多曾豪言将在联赛中攻入30球，不过虽然未能如愿，但其仍然成为了该季西甲最佳射手。

然而皇马虽然堆积了大量球星，但成绩始终不佳，外界批评不断。這時罗纳尔多還不時在場外製造花边新聞，同时状态开始逐渐下滑，04/05赛季罗纳尔多遭遇了长达13场比赛的进球荒，此乃其职业生涯前所未有之事。頻密的负面曝光令罗纳尔多形象大減，2005-06年球季下旬更揚言要前任教練[-{zh-hans:博斯克;
zh-hk:迪保斯基;}-重返球會才繼續留效](../Page/迪保斯基.md "wikilink")，可見其在皇馬之跋扈。越來越差的竞技状态加之不断增加的伤病，逐漸使罗纳尔多成為被清洗對象。

他在[2006年世界盃足球賽決賽週十六強對加納的賽事中射入的第一球](../Page/2006年世界盃足球賽決賽週賽程紀錄.md "wikilink")，是罗纳尔多在世界盃決賽週中的第15個入球，打破了前西德名將[-{zh-hans:盖德·穆勒;
zh-hk:梅拿;}-保持世界盃決賽週內攻入最多入球](../Page/盖德·穆勒.md "wikilink")（前紀錄為14球）的紀錄；並以62球成為[巴西队總射手榜第二位](../Page/巴西國家足球隊.md "wikilink")，僅次於[比利](../Page/比利.md "wikilink")。但是在該届杯赛上，他狀態平平，亦受傷患困扰而表现一般。巴西队也終在八強被法国所淘汰。

### AC米蘭（2007～2008）

2006年以後[皇家馬德里出現大動盪](../Page/皇家馬德里.md "wikilink")，球隊教練由[意大利名帥](../Page/意大利.md "wikilink")[-{zh-hans:卡佩罗;zh-hk:卡比路;zh-tw:卡比路;}-出掌](../Page/卡佩罗.md "wikilink")。[-{zh-hans:卡佩罗;zh-hk:卡比路;zh-tw:卡比路;}-以鐵腕手段治軍](../Page/卡佩罗.md "wikilink")，使朗拿度常抱怨得不到上陣機會。於是於2007年1月30日，[皇家馬德里與](../Page/皇家馬德里.md "wikilink")[AC米蘭簽訂協議](../Page/AC米蘭.md "wikilink")，以750萬[歐元的價格](../Page/歐元.md "wikilink")，將他轉往[AC米蘭](../Page/AC米蘭.md "wikilink")。罗纳尔多因而成為了唯一效力過皇家馬德里、巴塞隆納、國際米蘭、AC米蘭等四大豪門的球員。\[2\]

在[AC米蘭](../Page/AC米蘭.md "wikilink")，他大部分時間都是與傷病鬥爭，上陣次數寥寥。在極少出場的機會中仍然為[AC米蘭作出了極大的貢獻](../Page/AC米蘭.md "wikilink")，為[AC米蘭能獲得聯賽第四名](../Page/AC米蘭.md "wikilink")，並取得2007-2008年度冠軍聯賽資格，起了決定性的作用。但是進入2007-2008賽季後，傷病使他长期遠離賽場。2008年2月13日，在對[里窝那的](../Page/里窝那.md "wikilink")[意甲賽事中](../Page/意甲.md "wikilink")，他在下半場入替[吉拉迪诺後僅](../Page/吉拉迪诺.md "wikilink")3分鐘，就因為左膝十字韧带折斷而退下火線，預計要養傷最少9個月。这次与他之前在国际米兰时受的伤几乎完全一样，然而此时罗纳尔多已经32岁，因此不少媒體認為他再也无法像年轻时那样恢复，罗纳尔多會就此掛靴\[3\]。不過，他在出院時重申，他恢復良好，無意掛靴。\[4\]。[AC米蘭曾表示不会放弃罗纳尔多](../Page/AC米蘭.md "wikilink")，但随后罗纳尔多在巴西国内爆出[人妖丑闻](../Page/人妖.md "wikilink")，令AC米蘭沒耐性再等下去，隨即解約。解約之後，罗纳尔多到[法林明高疗养训练](../Page/法林明高.md "wikilink")。

### 哥連泰斯（2009～2011）

於2008年12月9日，朗拿度與[巴西球會](../Page/巴西.md "wikilink")[哥連泰斯簽了為期一年的短期合約](../Page/科林蒂安.md "wikilink")。而球會則極力希望朗拿度能在09年2月正式復出。復出後他有不錯的表現，連續取得入球，一扫公众对他能否重返球场的质疑。

2009年7月26日，他在[科林蒂安同](../Page/科林蒂安.md "wikilink")[帕尔梅拉斯的比赛中受伤离场](../Page/帕尔梅拉斯.md "wikilink")，9月16日，罗纳尔多向巴西电台Bandeirantes证实，他复出想重返国家队出战[南非世界杯](../Page/南非世界杯.md "wikilink")。但巴西主教练[邓加宣布将不会征召罗纳尔多进入国家队](../Page/邓加.md "wikilink")\[5\]。

2011年2月14日，罗纳尔多宣布退役。伤病是罗纳尔多决定提前退役的一大原因。罗纳尔多在记者招待会上，首次透露4年前，他在[AC米兰时发现患上了甲状腺功能减退症](../Page/AC米兰.md "wikilink")。这是一种内分泌疾病，能让人体的新陈代谢下降，服用药物控制是必要的，因此他的体重也因为激素的原因有了较大增长，這亦等於揭開了為何由幾年前開始，他的體型不時有發福情況出現的主因。他表示：“在过去两年时间里，我每天都在忍受伤痛的煎熬，但现在，我终于体会到了离别的滋味，我将一生献给了足球，但我毫不后悔，它太美妙也太精彩。”過去幾年，全世界都因他增肥而嘲笑他為「肥羅」，孰不知「外星人」承受常人所無法理解的痛苦。他力圖克服傷病，但在退役前一周的訓練中，他再次拉傷了大腿，這最终迫使已經做過四次腿部手術的「外星人」永远告别綠茵場。
\[6\] \[7\] \[8\] \[9\] \[10\] \[11\]

### 2014年巴西世界杯形象大使

2011年2月18日，罗纳尔多已经被任命为巴西2014年世界杯形象大使，是巴西世界杯组委会成员之一。德国对巴西的比赛中，他是特邀嘉賓，並親眼目睹了德国前锋克洛泽的世界杯第16枚进球，在此之前，他一直是世界盃15枚進球的記錄保持者。

### 2018年俄羅斯世界杯開幕式

[Sdm_4649.jpg](https://zh.wikipedia.org/wiki/File:Sdm_4649.jpg "fig:Sdm_4649.jpg")
朗拿度作为“世界杯大使”受邀出场，開幕式上罗纳尔多携一名穿着“Russia
2018”衬衫的儿童亮相。英国流行歌手[罗比·威廉斯演唱](../Page/罗比·威廉斯.md "wikilink")《》，随后俄罗斯女高音[艾达·加里富林娜出现在球场上](../Page/艾达·加里富林娜.md "wikilink")，身后观众席出现“火烈鸟”的背景板。威廉斯演唱了《Feel》后，和加里富林娜合唱《Angels》，期间拿着32支参赛球队名牌和国旗的伴舞登场。俄罗斯超模携曾于3月登上[国际空间站的官方用球](../Page/国际空间站.md "wikilink")[愛迪達电视之星18入场](../Page/电视之星_18.md "wikilink")。之后再次入场的罗纳尔多与吉祥物小狼“扎比瓦卡”一道，跟一名小男孩共同完成了踢球表演\[12\]\[13\]。

## 技術風格

作為前鋒的羅納度是名超級攻擊手，有強壯的身體、飛快的速度與爆發力、花俏出色的腳法與精準強力的射門嗅覺，超強的身體平衡性搭配飛快穩健的盤帶讓扯衣服或幅度不大的伸腿暗拐等小動作完全無法阻止他，反被他撞開硬吃的防守球員不計其數，亦能變速兼施牛尾巴等過人腳法甩開多人包夾，加上鬼神級的射門能力而被尊稱為"外星人"，是綠茵場上力與美的完美結合。

但是羅納度超強悍的球風實際上對身體負擔極大（尤其是腳），導致羅納度傷病不斷，巔峰期相當短促。

## 生涯数据

[PSG-Barcelone_1997.png](https://zh.wikipedia.org/wiki/File:PSG-Barcelone_1997.png "fig:PSG-Barcelone_1997.png")
[Ronaldo_Cannes.jpg](https://zh.wikipedia.org/wiki/File:Ronaldo_Cannes.jpg "fig:Ronaldo_Cannes.jpg")
[Ronaldo_Real_Madrid.jpg](https://zh.wikipedia.org/wiki/File:Ronaldo_Real_Madrid.jpg "fig:Ronaldo_Real_Madrid.jpg")

### 俱乐部

截止至2011年2月14日

| 赛季        | 俱乐部                                      | 联赛                            | 杯赛                      | 洲际赛事 | 总计 |
| --------- | ---------------------------------------- | ----------------------------- | ----------------------- | ---- | -- |
| 出场        | 进球                                       | 出场                            | 进球                      | 出场   | 进球 |
| 巴西足球锦标赛   | 巴西杯                                      | 南美洲                           | 总计                      |      |    |
| 1993-94   | [高士路](../Page/高士路.md "wikilink")         | 34<sup><small>1</small></sup> | 34<sup><small>1</small> | 1    | 0  |
| 荷兰足球甲级联赛  | 荷兰杯                                      | 欧洲                            | 总计                      |      |    |
| 1994-95   | [PSV埃因霍温](../Page/PSV埃因霍温.md "wikilink") | 33                            | 30                      | 1    | 2  |
| 1995-96   | 13                                       | 12                            | 3                       | 1    | 5  |
| 西班牙足球甲级联赛 | 国王杯                                      | 欧洲                            | 总计                      |      |    |
| 1996-97   | [巴塞罗那](../Page/巴塞罗那队.md "wikilink")      | 37                            | 34                      | 5    | 8  |
| 意大利足球甲级联赛 | 意大利杯                                     | 欧洲                            | 总计                      |      |    |
| 1997-98   | [国际米兰](../Page/国际米兰.md "wikilink")       | 32                            | 25                      | 4    | 3  |
| 1998-99   | 19                                       | 14                            | 3                       | 0    | 6  |
| 1999-00   | 7                                        | 3                             | 1                       | 0    | 0  |
| 2000-01   | 0                                        | 0                             | 0                       | 0    | 0  |
| 2001-02   | 10                                       | 7                             | 1                       | 0    | 5  |
| 西班牙足球甲级联赛 | 国王杯                                      | 欧洲                            | 总计                      |      |    |
| 2002-03   | [皇家马德里](../Page/皇家马德里.md "wikilink")     | 31                            | 23                      | 1    | 0  |
| 2003-04   | 32                                       | 24                            | 7                       | 3    | 9  |
| 2004-05   | 34                                       | 21                            | 1                       | 0    | 10 |
| 2005-06   | 23                                       | 14                            | 2                       | 1    | 2  |
| 2006-07   | 7                                        | 1                             | 2                       | 1    | 4  |
| 意大利足球甲级联赛 | 意大利杯                                     | 欧洲                            | 总计                      |      |    |
| 2006-07   | [AC米兰](../Page/AC米兰.md "wikilink")       | 14                            | 7                       | 0    | 0  |
| 2007-08   | 6                                        | 2                             | 0                       | 0    | 0  |
|           |                                          |                               |                         |      |    |
| 巴西足球甲级联赛  | 巴西杯                                      | 南美洲                           | 总计                      |      |    |
| 2009      | [哥連泰斯](../Page/哥連泰斯.md "wikilink")       | 30<sup><small>2</small></sup> | 20<sup><small>2</small> | 8    | 3  |
| 2010      | 20<sup><small>2</small>                  | 9<sup><small>2</small>        | \-                      | \-   | 7  |
| 2011      | 2<sup><small>2</small>                   | 0<sup><small>2</small>        | \-                      | \-   | 2  |
| **总计**    | **巴西**                                   | 88                            | 63                      | 13   | 4  |
| **荷兰**    | 46                                       | 42                            | 4                       | 4    | 7  |
| **西班牙**   | 164                                      | 117                           | 18                      | 13   | 44 |
| **意大利**   | 88                                       | 58                            | 9                       | 3    | 22 |
| 生涯总计      | 384                                      | 280                           | 40                      | 22   | 94 |

<sup>1</sup><small>包括在米纳斯吉拉斯州内的18场比赛打进的22个球</small>
<sup>2</sup><small>包括州内联赛进球</small>

  - 杯赛包括国内一般的杯赛和超级杯
  - 洲际比赛包括欧洲的洲际杯赛，南美的洲际杯赛和国际杯赛

**职业生涯总计：**

  - 俱乐部：518场比赛352个进球
  - 巴西国家队103场比赛67个进球
  - 巴西U-23国家队：8场比赛6个进球

**总计：629场比赛425个进球**

### 世界盃入球

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>日期</p></th>
<th><p>場地</p></th>
<th><p>對手</p></th>
<th><p>比分</p></th>
<th><p>賽果</p></th>
<th><p>屆別</p></th>
<th><p>階段</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1.</p></td>
<td><p>1998年6月16日</p></td>
<td><p><a href="../Page/法國.md" title="wikilink">法國</a><a href="../Page/南特.md" title="wikilink">南特</a><a href="../Page/貝若利體育場.md" title="wikilink">貝若利體育場</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>3 – 0</p></td>
<td><center>
<p><a href="../Page/1998年世界盃足球賽.md" title="wikilink">1998</a></p></td>
<td><center>
<p><a href="../Page/1998年世界盃足球賽#A組.md" title="wikilink">分組賽</a></p></td>
</tr>
<tr class="even">
<td><p>2.</p></td>
<td><p>1998年6月27日</p></td>
<td><p>法國<a href="../Page/巴黎.md" title="wikilink">巴黎</a><a href="../Page/王子公園體育場.md" title="wikilink">王子公園體育場</a></p></td>
<td></td>
<td><center>
<p><strong>3</strong> – 0</p></td>
<td><center>
<p>4 – 1</p></td>
<td><center>
<p><a href="../Page/1998年世界盃足球賽#淘汰賽階段.md" title="wikilink">十六強賽</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3.</p></td>
<td><center>
<p><strong>4</strong> – 1</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4.</p></td>
<td><p>1998年7月7日</p></td>
<td><p>法國<a href="../Page/馬賽.md" title="wikilink">馬賽</a><a href="../Page/韋洛德羅姆球場.md" title="wikilink">韋洛德羅姆球場</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>1 – 1</p></td>
<td><center>
<p><a href="../Page/1998年世界盃足球賽#淘汰賽階段.md" title="wikilink">準決賽</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5.</p></td>
<td><p>2002年6月3日</p></td>
<td><p><a href="../Page/南韓.md" title="wikilink">南韓</a><a href="../Page/蔚山.md" title="wikilink">蔚山</a><a href="../Page/蔚山文殊足球競技場.md" title="wikilink">蔚山文殊足球競技場</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 1</p></td>
<td><center>
<p>2 – 1</p></td>
<td><center>
<p><a href="../Page/2002年世界盃足球賽.md" title="wikilink">2002</a></p></td>
<td><center>
<p><a href="../Page/2002年世界盃足球賽#C組.md" title="wikilink">分組賽</a></p></td>
</tr>
<tr class="even">
<td><p>6.</p></td>
<td><p>2002年6月8日</p></td>
<td><p>南韓<a href="../Page/西歸浦市.md" title="wikilink">西歸浦</a><a href="../Page/濟州世界盃競技場.md" title="wikilink">濟州世界盃競技場</a></p></td>
<td></td>
<td><center>
<p><strong>4</strong> – 0</p></td>
<td><center>
<p>4 – 0</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7.</p></td>
<td><p>2002年6月13日</p></td>
<td><p>南韓<a href="../Page/水原市.md" title="wikilink">水原</a><a href="../Page/水原世界盃競技場.md" title="wikilink">水原世界盃競技場</a></p></td>
<td></td>
<td><center>
<p>0 – <strong>1</strong></p></td>
<td><center>
<p>2 – 5</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8.</p></td>
<td><center>
<p>0 – <strong>2</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9.</p></td>
<td><p>2002年6月17日</p></td>
<td><p><a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/神戶.md" title="wikilink">神戶</a><a href="../Page/御崎公園球技場.md" title="wikilink">神戶飛翼體育場</a></p></td>
<td></td>
<td><center>
<p><strong>2</strong> – 0</p></td>
<td><center>
<p>2 – 0</p></td>
<td><center>
<p><a href="../Page/2002年世界盃足球賽#淘汰賽階段.md" title="wikilink">十六強賽</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10.</p></td>
<td><p>2002年6月26日</p></td>
<td><p>日本<a href="../Page/埼玉市.md" title="wikilink">埼玉</a><a href="../Page/埼玉2002體育場.md" title="wikilink">埼玉體育場</a></p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>1 – 0</p></td>
<td><center>
<p><a href="../Page/2002年世界盃足球賽#淘汰賽階段.md" title="wikilink">準決賽</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11.</p></td>
<td><p>2002年6月30日</p></td>
<td><p>日本<a href="../Page/橫濱市.md" title="wikilink">橫濱</a><a href="../Page/橫濱國際綜合競技場.md" title="wikilink">橫濱國際綜合競技場</a></p></td>
<td></td>
<td><center>
<p>0 – <strong>1</strong></p></td>
<td><center>
<p>0 – 2</p></td>
<td><center>
<p><a href="../Page/2002年世界盃足球賽決賽.md" title="wikilink">決賽</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12.</p></td>
<td><center>
<p>0 – <strong>2</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13.</p></td>
<td><p>2006年6月22日</p></td>
<td><p>德國<a href="../Page/多蒙特.md" title="wikilink">多蒙特</a><a href="../Page/西格納伊度納公園.md" title="wikilink">威斯法倫球場</a></p></td>
<td></td>
<td><center>
<p>1 – <strong>1</strong></p></td>
<td><center>
<p>1 – 4</p></td>
<td><center>
<p><a href="../Page/2006年世界盃足球賽.md" title="wikilink">2006</a></p></td>
<td><center>
<p><a href="../Page/2006年世界盃足球賽決賽週_-_F組.md" title="wikilink">分組賽</a></p></td>
</tr>
<tr class="even">
<td><p>14.</p></td>
<td><center>
<p>1 – <strong>4</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15.</p></td>
<td><p>2006年6月27日</p></td>
<td></td>
<td><center>
<p><strong>1</strong> – 0</p></td>
<td><center>
<p>3 – 0</p></td>
<td><center>
<p><a href="../Page/2006年世界盃足球賽決賽週_-_淘汰賽階段#巴西對加納.md" title="wikilink">十六強賽</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 评价

  - 前國際足聯主席哈維蘭治:「朗拿度是歷史上最偉大的足球運動員，[馬勒當拿也是位足球天才](../Page/馬勒當拿.md "wikilink")，但他的技術未像朗拿度那樣給足壇以持久的震撼。」
  - [馬勒當拿](../Page/馬勒當拿.md "wikilink"):「朗拿度受傷了，足球世界裡失去了他的上帝。」（2000年）
  - [比利](../Page/比利.md "wikilink"):「我的唯一傳人已經問世，他就是朗拿度。」
  - [碧根鮑華](../Page/碧根鮑華.md "wikilink")：「防守朗拿度幾乎是不可能的任務，他的速度和球技無人能比。」
  - [摩連奴](../Page/摩連奴.md "wikilink"):「我看過馬勒當拿、[尤西比奧](../Page/尤西比奧.md "wikilink")、施丹和[古列治踢球](../Page/古列治.md "wikilink")，當時我從來沒有看到任何人能比得上96年的朗拿度，和他一同工作的每一天我都要為他的表現驚訝，他能做出的動作一次次讓我感到不可思議。」
  - [文仙尼](../Page/羅拔圖·文仙尼.md "wikilink"):「朗拿度是世界足壇的一個神話，他的速度和爆發力幾乎都達到了完美的程度。就像只有一個比利、一個馬勒當拿一樣，朗拿度也是獨一無二的。」
  - [波比笠臣](../Page/波比笠臣.md "wikilink"):「我觉得朗拿度不属于我们这个星球，他太不可思议了。朗拿度用一只脚就足够了，他整个人都是为足球度身订造的，速度快，力量足，意识好，有自信心和霸气，这些都是朗拿度成为世界顶级前锋的天赋。」
  - [施丹](../Page/施丹.md "wikilink"):「當他有踢球的慾望時，是夢幻般的、無可比擬的！另外，他也是個很好的伙伴，是我們的開心果。和他一起在場上，我都成了觀眾。訓練時就更糟了，我看過他做過很多令人難以置信的動作，簡直匪夷所思！。」
  - [費高](../Page/費高.md "wikilink"):「朗拿度是未來足球的主宰者，足球界的[米高佐敦](../Page/米高佐敦.md "wikilink")。」
    （1996年）
  - [魯爾](../Page/劳尔·冈萨雷斯·布兰科.md "wikilink"):「朗拿度是位偉大的球員，他有打破場上平衡的能力，很多比賽里，他能依靠施丹或者[古迪的兩腳傳球就破門得分](../Page/古迪.md "wikilink")，並幫助球隊獲勝。」
  - [巴治奧](../Page/羅拔圖·巴治奧.md "wikilink"):「朗拿度是强大得让人嫉妒的天才，是效率最高的前锋。」
  - 阿根廷戰神[巴迪斯圖達說](../Page/巴迪斯圖達.md "wikilink")，"因為朗拿度就是足球。
  - [卡路士說](../Page/羅拔圖卡路士.md "wikilink"):「他能夠深刻領會足球之道，腳法如此之快。當他控球面對防守隊員時，他的跑動和移位是如此短暫，如此犀利，如此迅雷不及掩耳，當他帶球全速奔跑時，沒有人能夠阻擋他，不管後衛是如何如何出色，一切都得聽天由命。在他之前，我們見過禁區殺手，但只有他拿球，盤帶射門樣樣行，既能一對一過人，又能傳出好球，還是一個極其優秀的終結者，這就是為什麼只有他被稱作是‘現象級’。」
  - [里瓦尔多曾表示](../Page/里瓦尔多.md "wikilink")，若不是朗拿度，他還會得到更高的認可。
  - [朗拿甸奴](../Page/朗拿甸奴.md "wikilink"):「我和他之間沒有可比性。他一直是我的偶像。」
  - [哥斯達古達](../Page/哥斯達古達.md "wikilink"):「作為一名後衛，我認為朗拿度是那種你任何時候都不想遭遇的前鋒，因為他無論何時都有創造奇蹟的可能性，而[阿祖安奴不同](../Page/阿祖安奴.md "wikilink")，儘管他非常難於盯防，但是你還是能夠在隊友的幫助下把他防住的。」
  - [簡拿華路](../Page/簡拿華路.md "wikilink"):「我的任務就是盡可能緊地貼住朗拿度，我想在他進球之前，我完成的不錯，但他還是進球了……現在我是徹底地筋疲力盡了。」
  - [馬甸尼](../Page/馬甸尼.md "wikilink"):「他是曾經與我交手過的前鋒中，最令我頭疼的一個，不過我永遠也不會忘記當年的那個在國際米蘭的朗拿度，“現象”是對他最合適的描述。」
  - [佩奧爾](../Page/佩奧爾.md "wikilink"):「誰也阻擋不了他，因為他是外星人」
  - [韋利](../Page/韋利.md "wikilink"):「朗拿度可能是足球有史以来技术最好的球员。」
  - [舒夫真高](../Page/舒夫真高.md "wikilink"):「[阿祖安奴是個美妙之極的球員](../Page/阿祖安奴.md "wikilink")，但不能拿他和朗拿度比較。朗拿度就是全部：力量、創造力和精妙的腳法，而阿祖安奴則是個非常強壯的前鋒，未來會證明他可能是世界第一。但朗拿度仍是當今的世界第一前鋒，他仍在享受著足球，我這兩天剛看到他在進球同時為隊友助攻。」
  - [舒寧咸](../Page/舒寧咸.md "wikilink"):「有的時候，如果一個球員有著高速的腳步的話，他的頭腦的反應速度就會相對遜於他的雙腳，但是朗拿度卻將速度和意識結合得相當完美。即使他處於每小時100英里的高速之中，他也知道自己正在做什麼，將要做什麼，他是一個最好的終結者。他能夠迅速將球卸到自己的腳下，只需要一個瞬間的調整，他就可以帶球突進並且晃過守門員。上帝知道他肯定和從前一樣令人吃驚。」
  - [高路斯](../Page/高路斯.md "wikilink"):「朗拿度是個非常優秀的球員，我在意大利踢球的時候，人們都告訴我朗拿度是他們在意大利見過的最強的球員，對我來說，他就是史上最完美的球員。」
  - [亨利](../Page/亨利.md "wikilink"):「是我見過的最出色的選手，我認為他比同年齡的比利和馬勒當拿都更加出色。」
  - [范尼斯特鲁伊](../Page/范尼斯特鲁伊.md "wikilink"):「我認為朗拿度是有史以來最好的前鋒，當他在皇馬傷癒的時候，我曾經與他一起訓練比賽，儘管他沒有恢復到100%的狀態，依然能夠做出一些不可思議的事情，他是我所搭檔過的最厲害的前鋒。
    ”」
  - [迈克尔·欧文](../Page/迈克尔·欧文.md "wikilink"):「前鋒我只甘願打一個人的替補，他就是朗拿度。」
  - [卡卡](../Page/卡卡.md "wikilink")：「“讓我來評價可不太合適，我可是他最大的球迷。當他已經成為足球界的'現象'之時，我才剛剛在巴西開始自己的職業生涯，能和他效力同一家俱樂部是我長久以來的夢想，今天可算是夢想成真了。」
  - [伊巴謙莫域](../Page/伊巴謙莫域.md "wikilink"):「朗拿度是我的偶像，也是我所遇到過的最強對手。」\[14\]
  - [美斯](../Page/美斯.md "wikilink")：「朗拿度是我的偶像，在足球世界裡，他才是我崇拜的英雄人物。」\[15\]
  - [朗尼](../Page/朗尼.md "wikilink")：「當我成長時，巴西是我最喜愛看的一支球隊，像朗拿度、[羅馬里奧等球員](../Page/羅馬里奧.md "wikilink")，朗拿度是最好的，我昨天才看過一齣他的紀錄片，我是在按摩室的電視上觀看的，看到他怎樣比賽實在太好了，有時你會忘記了他有多好，作為一位前鋒，他快速、強壯，能射出不同類型的入球，是位不折不扣的前鋒，他大概是最佳的。」
  - [羅賓奴](../Page/羅賓奴.md "wikilink")：「朗拿度是一個現象，他總是教會我很多東西，給我一些建議，我總是嘗試著去實行它們，對我來說，與朗拿度搭檔是一個夢想。」
  - [賓施馬可不像施丹在場下那麼溫和](../Page/賓施馬.md "wikilink")，他不止一次地在媒體上公開表示：「“不要跟我提施丹，我不是新施丹，我的偶像是朗拿度，他才是世界上有史以來最偉大的球員，我收集了很多他比賽的DVD，我一直是在向他學習”。」
  - [巴洛迪利](../Page/巴洛迪利.md "wikilink")：「我的偶像是朗拿度，不論他為哪支球隊效力。我保留著他踢球的所有錄像帶，當他受傷的時候，我總是非常傷心。」

## 獎項

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 75%" />
</colgroup>
<thead>
<tr class="header">
<th><p><strong>獎項</strong></p></th>
<th><p><strong>效力球會</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>高士路</strong></p></td>
<td><p><strong>PSV燕豪芬</strong></p></td>
</tr>
<tr class="even">
<td><p>個人獎項</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/世界足球先生.md" title="wikilink">世界足球先生</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/金球奖_(足球).md" title="wikilink">歐洲金球奖</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/歐洲金靴獎.md" title="wikilink">歐洲金靴獎</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/世界盃金靴獎.md" title="wikilink">世界盃金靴獎</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/世界盃金球獎.md" title="wikilink">世界盃金球獎</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界杯金球獎.md" title="wikilink">世界盃銀球獎</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/:en:List_of_Copa_América_top_scorers.md" title="wikilink">美洲杯金靴奖</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/:en:List_of_Copa_América_top_scorers.md" title="wikilink">美洲杯金球奖</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/:en:List_of_Copa_América_top_scorers.md" title="wikilink">美洲杯银球奖</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/:en:List_of_Copa_América_top_scorers.md" title="wikilink">美洲杯银靴奖</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/歐洲足協最有價值球員.md" title="wikilink">歐洲足協最有價值球員</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/:en:List_of_Copa_América_top_scorers.md" title="wikilink">欧洲联盟杯最佳球员</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/:en:List_of_Copa_América_top_scorers.md" title="wikilink">欧洲优胜者杯最佳射手/最佳球员</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/:en:List_of_Copa_América_top_scorers.md" title="wikilink">意甲奥斯卡十年最佳球员</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/:en:Pichichi_Trophy.md" title="wikilink">西班牙甲組聯賽最佳射手</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>球會獎項</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/西甲.md" title="wikilink">西班牙甲組聯賽冠軍</a></strong></p></td>
<td><hr /></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/西班牙國王盃.md" title="wikilink">西班牙國王盃冠軍</a></strong></p></td>
<td><hr /></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/西班牙超級盃.md" title="wikilink">西班牙超級盃冠軍</a></strong></p></td>
<td><hr /></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/荷蘭盃.md" title="wikilink">荷蘭盃冠軍</a></strong></p></td>
<td><hr /></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/米納斯吉拉斯.md" title="wikilink">米納斯吉拉斯州聯賽冠軍</a></strong></p></td>
<td><p>1994年</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/聖保羅州.md" title="wikilink">聖保羅州聯賽冠軍</a></strong></p></td>
<td><hr /></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/巴西盃.md" title="wikilink">巴西盃冠軍</a></strong></p></td>
<td><p>1993年</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/歐洲盃賽冠軍盃.md" title="wikilink">歐洲盃賽冠軍盃冠軍</a></strong></p></td>
<td><hr /></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/歐洲足協盃.md" title="wikilink">歐洲足協盃冠軍</a></strong></p></td>
<td><hr /></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/洲際盃足球賽.md" title="wikilink">洲際盃冠軍</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>國家隊獎項（巴西）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/美洲國家盃.md" title="wikilink">美洲國家盃冠軍</a></strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/世界盃.md" title="wikilink">世界盃冠軍</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世界盃.md" title="wikilink">世界盃亞軍</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 廣告

[Ronaldinho06Jun2005Abr.jpeg](https://zh.wikipedia.org/wiki/File:Ronaldinho06Jun2005Abr.jpeg "fig:Ronaldinho06Jun2005Abr.jpeg")

  - Nike
  - 百事可乐
  - BenQ
  - 金嗓子喉宝

## 社会活动

2002年8月2日，罗纳尔多与米莲妮、儿子罗纳德一起出席了向巴西全国癌症研究所捐赠10万[雷亚尔的仪式](../Page/雷亚尔.md "wikilink")，并去该研究所下属的里约[癌症防治医院看望了小患者们](../Page/癌症.md "wikilink")。

2004年8月，作为联合国开发计划署的“亲善大使”的罗纳尔多率巴西国家队与[海地国家队在](../Page/海地國家足球隊.md "wikilink")[太子港进行了一场义赛](../Page/太子港.md "wikilink")，所得款项全部捐给海地人民，用于消除[贫困](../Page/贫困.md "wikilink")。

2008年12月5日，罗纳尔多向巴西南部[圣卡塔琳娜州水灾地区捐赠救灾物资](../Page/圣卡塔琳娜州.md "wikilink")。罗纳尔多捐赠的物资主要有食品，玩具，卫生和清洁用品，总重30吨，装了满满两卡车。

2015年4月21日，2015年度[联合国发展计划署](../Page/联合国.md "wikilink")“对抗贫穷”足球慈善赛在法国圣埃蒂安举行，罗纳尔多偕同[齐达内](../Page/齐达内.md "wikilink")、[特雷泽盖等众球星参赛](../Page/特雷泽盖.md "wikilink")。

目前投資電子競技，在2017年[英雄聯盟季中邀請賽出席頒獎典禮](../Page/英雄聯盟.md "wikilink")。

## 个人生活

罗纳尔多1997年与巴西女演员相恋，两人于1999年初分手。1999年4月罗纳尔多与巴西女足队员结婚，他们的儿子 Ronald
于2000年4月6日在米兰出生。两人后因性格不合最终离婚。2005年罗纳尔多与巴西模特[达尼埃拉结婚](../Page/丹妮埃拉·西卡雷莉.md "wikilink")，婚礼成本据说高达70万欧元，然而这段婚姻只持续了86天。

罗纳尔多现在的妻子是 ，生下两个女儿 Maria Sophia 和 Maria Alice 。

2010年12月，罗纳尔多在DNA测验中被证实是一个名叫亚历山大的男孩的父亲。该男孩是罗纳尔多和一个名叫 Michele Umezu
的巴西女服务员的私生子，两人2002年世界杯期间在[东京相识后建立了关系](../Page/东京.md "wikilink")。\[16\]罗纳尔多事后称“我已经有了四个孩子，这足够多了”，遂在2010年12月26日做了[输精管结扎手术](../Page/输精管.md "wikilink")。\[17\]

罗纳尔多是的拥有者之一。

## 参考文献

## 外部連結

  - [朗拿度國家隊的詳細資料](http://www.rsssf.com/miscellaneous/ronaldo-intlg.html)
  - [朗拿度在歐洲賽事的詳細資料](http://www.rsssf.com/players/ronaldo-in-ec.html)
  - [Ronaldo tribute
    site](https://web.archive.org/web/20061108213003/http://www.ronaldo-web.com.ar/)

[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:1994年世界盃足球賽球員](../Category/1994年世界盃足球賽球員.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")
[Category:1999年美洲盃球員](../Category/1999年美洲盃球員.md "wikilink")
[Category:1997年美洲盃球員](../Category/1997年美洲盃球員.md "wikilink")
[Category:1995年美洲盃球員](../Category/1995年美洲盃球員.md "wikilink")
[Category:1996年夏季奧林匹克運動會足球運動員](../Category/1996年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:巴西奧林匹克運動會銅牌得主](../Category/巴西奧林匹克運動會銅牌得主.md "wikilink")
[Category:巴西足球运动员](../Category/巴西足球运动员.md "wikilink")
[Category:巴西奧運足球運動員](../Category/巴西奧運足球運動員.md "wikilink")
[Category:FIFA 100](../Category/FIFA_100.md "wikilink")
[Category:世界足球先生](../Category/世界足球先生.md "wikilink")
[Category:歐洲足球先生](../Category/歐洲足球先生.md "wikilink")
[Category:高士路球員](../Category/高士路球員.md "wikilink")
[Category:PSV燕豪芬球員](../Category/PSV燕豪芬球員.md "wikilink")
[Category:巴塞隆拿球員](../Category/巴塞隆拿球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:皇家馬德里球員](../Category/皇家馬德里球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:非洲裔西班牙人](../Category/非洲裔西班牙人.md "wikilink")
[Category:歸化西班牙公民](../Category/歸化西班牙公民.md "wikilink")
[Category:非洲裔巴西人](../Category/非洲裔巴西人.md "wikilink")
[Category:里約熱內盧州人](../Category/里約熱內盧州人.md "wikilink")
[Category:巴西旅外足球運動員](../Category/巴西旅外足球運動員.md "wikilink")
[Category:荷蘭外籍足球運動員](../Category/荷蘭外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:奧林匹克運動會足球獎牌得主](../Category/奧林匹克運動會足球獎牌得主.md "wikilink")

1.  [大罗98世界杯决赛之谜解开
    心跳18次/分差点骤停](http://sports.sina.com.cn/g/2012-02-03/10375928455.shtml)

2.

3.  [羅納度右膝蓋肌腱斷裂
    足球生涯恐告終](http://hk.sports.yahoo.com/080215/167/2oxq4.html)

4.  [大哨今出院無意掛靴](http://soccer.atnext.com/showart.cfm?sec_id=25391&art_id=10776936&iss_id=20080221)

5.  [罗纳尔多南非世界杯梦碎
    邓加表态不会征其入队](http://sports.china.com/zh_cn/football/inter/other/11062542/20091125/15712159.html)


6.
7.

8.

9.

10.

11.

12.

13.

14.

15.

16. [Nas bancas: Tudo sobre o primeiro encontro de Ronaldo com
    Alex](http://revistaquem.globo.com/Revista/Quem/0,,EMI193960-9531,00-NAS+BANCAS+TUDO+SOBRE+O+PRIMEIRO+ENCONTRO+DE+RONALDO+COM+ALEX.html)

17. [After fourth child, Ronaldo performes
    vasectomy](http://beta.antaranews.com/en/news/1293410162/after-fourth-child-ronaldo-performes-vasectomy)