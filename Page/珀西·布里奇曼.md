**珀西·威廉姆斯·布里奇曼**（，），[美国](../Page/美国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，因他在高壓物理方面的貢獻，1946年获得[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。布里奇曼對[科學方法及](../Page/科學方法.md "wikilink")[科學哲學的一些觀點有相當廣泛的著述](../Page/科學哲學.md "wikilink")。\[1\]\[2\]\[3\]

## 参考资料

## 外部链接

  - [诺贝尔官方网站关于珀西·布里奇曼简介](http://nobelprize.org/nobel_prizes/physics/laureates/1946/bridgman-bio.html)

[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:哈佛大學教授](../Category/哈佛大學教授.md "wikilink")
[Category:皇家学会外籍会员](../Category/皇家学会外籍会员.md "wikilink")
[Category:自殺科學家](../Category/自殺科學家.md "wikilink")
[Category:热力学家](../Category/热力学家.md "wikilink")
[Category:美国国家科学院院士](../Category/美国国家科学院院士.md "wikilink")
[Category:拉姆福德奖获得者](../Category/拉姆福德奖获得者.md "wikilink")

1.
2.
3.