**海伍德县**（**Haywood County,
Tennessee**）是[美國](../Page/美國.md "wikilink")[田納西州西部的一個縣](../Page/田納西州.md "wikilink")。面積1,383平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口19,797人。縣治[布朗斯維爾](../Page/布朗斯維爾_\(田納西州\).md "wikilink")（Brownsville）。

成立於1823年11月3日。縣名紀念[北卡羅萊納州司法部長](../Page/北卡羅萊納州.md "wikilink")、[田納西州最高法院法官約翰](../Page/田納西州最高法院.md "wikilink")·海伍德（John
Haywood，1753-1826）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[H](../Category/田納西州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.