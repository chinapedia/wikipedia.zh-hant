**焦耳**（簡稱**焦**）是[國際單位制中](../Page/國際單位制.md "wikilink")[能量](../Page/能量.md "wikilink")、[功或](../Page/功.md "wikilink")[热量的](../Page/热量.md "wikilink")[導出單位](../Page/國際單位制導出單位.md "wikilink")，符号為**J**。在[古典力學裏](../Page/古典力學.md "wikilink")，1焦耳等於施加1[牛頓](../Page/牛頓_\(單位\).md "wikilink")[作用力經過](../Page/作用力.md "wikilink")1公尺[距離所需的能量](../Page/距離.md "wikilink")（或做的機械功）。在[電磁學裏](../Page/電磁學.md "wikilink")，1焦耳等於將1[安培](../Page/安培.md "wikilink")[電流通過](../Page/電流.md "wikilink")1[歐姆](../Page/歐姆.md "wikilink")[電阻](../Page/電阻.md "wikilink")1秒時間所需的能量。焦耳是因紀念物理學家[詹姆斯·焦耳而命名](../Page/詹姆斯·焦耳.md "wikilink")\[1\]\[2\]。

以其它單位表示，

\[\rm 1\ J = 1\ N \cdot m = \left ( \frac{kg \cdot m}{s^2} \right ) \cdot m = \frac{kg \cdot m^2}{s^2}=Pa \cdot m^3= 1\ W \cdot s\,\!\]。

1焦耳也可以定義為

  - 移動1[庫侖](../Page/庫侖.md "wikilink")[電荷通過](../Page/電荷.md "wikilink")1[伏特](../Page/伏特.md "wikilink")[電壓差所需做的功](../Page/電壓.md "wikilink")，或1庫侖·伏特。這關係也可以逆反過來定義伏特。
  - 產生1[瓦特](../Page/瓦特.md "wikilink")[功率](../Page/功率.md "wikilink")1秒時間所需做的功，或1瓦特·秒。這關係也可以逆反過來定義瓦特。

## 与力矩单位的混淆

雖然在單位方面，表示焦耳為牛頓·公尺是正確的，為了避免與[力矩單位發生混淆](../Page/力矩.md "wikilink")，[國際度量衡局並不鼓勵這種用法](../Page/國際度量衡局.md "wikilink")\[3\]。力矩與能量的物理意義完全不同。

就物理量本身来说，能量由力和距离[点乘得到](../Page/数量积.md "wikilink")，是[标量](../Page/标量.md "wikilink")；力矩由力和距离[叉乘得到](../Page/叉乘.md "wikilink")，是[矢量](../Page/矢量.md "wikilink")，两者有根本的不同。

## 實際例子

一焦耳能量相等於施加一牛頓的作用力於任意物件，使之移動一米距離；也等於在[地心引力影響下將一個質量為](../Page/地心引力.md "wikilink")102克的蘋果抬高一米；或是1[瓦特的功率作](../Page/瓦特.md "wikilink")1秒所释放的能量。

## 單位轉換

一焦耳相等於：

  - 1×10<sup>7</sup> [爾格](../Page/爾格.md "wikilink")
  - 6.24150974×10<sup>18</sup> [電子伏特](../Page/電子伏特.md "wikilink")
  - 0.239 小[卡路里](../Page/卡路里.md "wikilink")
  - 9.4782×10<sup>−4</sup> [英國熱量單位](../Page/英國熱量單位.md "wikilink")（，）
  - 0.737 [呎·磅 力](../Page/呎·磅_力.md "wikilink")（）
  - 23.7 [呎·磅](../Page/呎·磅.md "wikilink")（）
  - 2.7778×10<sup>−7</sup> [千瓦·小時](../Page/千瓦·時.md "wikilink")
  - 2.7778×10<sup>−4</sup> [瓦特·小時](../Page/千瓦·時.md "wikilink")

以焦耳定義的單位

  - 1 小[卡路里](../Page/卡路里.md "wikilink")= 4.184焦耳\[4\]
  - 1 watt hour = 3600焦耳
  - 1 [kilowatt hour](../Page/千瓦·時.md "wikilink") = 3.6×10<sup>6</sup>焦耳
  - 1 [噸黃色炸藥](../Page/爆炸當量.md "wikilink") = 4.184×10<sup>9</sup>焦耳

## 參閱

  - [單位轉換](../Page/單位轉換.md "wikilink")
  - [數量級 (能量)](../Page/數量級_\(能量\).md "wikilink")
  - [瓦特](../Page/瓦特.md "wikilink")
  - [能量](../Page/能量.md "wikilink")
  - [牛頓 (單位)](../Page/牛頓_\(單位\).md "wikilink")

## 參考文獻

## 外部連結

  - 焦耳的[單位轉換](http://formularium.org/?go=122)
  - 加拿大的Worsley小學解釋[質能方程式的](../Page/質能方程式.md "wikilink")[網頁](http://www.worsleyschool.net/science/files/emc2/emc2.html)。

[Category:能量單位](../Category/能量單位.md "wikilink")
[Category:国际单位制导出单位](../Category/国际单位制导出单位.md "wikilink")

1.  *The American Heritage Dictionary*, Second College Edition (1985).
    Boston: Houghton Mifflin Co., p. 691.
2.  *McGraw-Hill Dictionary of Physics*, Fifth Edition (1997).
    McGraw-Hill, Inc., p. 224.
3.  國際度量衡局[官方網頁](http://www.bipm.org/en/si/si_brochure/chapter2/2-2/2-2-2.html)表明："通過綜合幾個基本單位，衍生單位通常可以以不同方式表達。例如，焦耳可以正式地寫為牛頓·公尺或公斤·公尺<sup>2</sup>／秒<sup>2</sup>。但是，這種代數自由還必須考慮到符合實際狀況；在某種狀況，使用某特定形式所得到的益處會比較多。實際而言，為了區分擁有同樣[因次的不同物理量](../Page/因次.md "wikilink")，有些物理量必須取特定的衍生單位為其單位。"
4.  一篇關於將營養單位從卡路里改變到焦耳的報告：[The adoption of joules as units of
    energy](http://www.fao.org/docrep/meeting/009/ae906e/ae906e17.htm),
    FAO/WHO Ad Hoc Committee of Experts on Energy and Protein, 1971.