[Wiener_brukner_in_taiwan_2004.JPG](https://zh.wikipedia.org/wiki/File:Wiener_brukner_in_taiwan_2004.JPG "fig:Wiener_brukner_in_taiwan_2004.JPG")
[Wiener_Saengerknaben.jpg](https://zh.wikipedia.org/wiki/File:Wiener_Saengerknaben.jpg "fig:Wiener_Saengerknaben.jpg")
[Musikverein_Wien_2009_04_30.JPG](https://zh.wikipedia.org/wiki/File:Musikverein_Wien_2009_04_30.JPG "fig:Musikverein_Wien_2009_04_30.JPG")
\]\]

**維也納少年合唱團**（[德語](../Page/德語.md "wikilink")：****，簡稱
**WSK**；[英語](../Page/英語.md "wikilink")：，簡稱
VBC）是由一群身穿藍色水手服的[男孩所組成的](../Page/男孩.md "wikilink")，這項特殊的打扮也成為了他們的標記。他們以優雅而又輕盈無比的童聲[女高音與童聲](../Page/女高音.md "wikilink")[女中音](../Page/女中音.md "wikilink")，演唱著各式各樣的[聖歌](../Page/聖歌.md "wikilink")、[圓舞曲](../Page/圓舞曲.md "wikilink")、[藝術歌曲](../Page/藝術歌曲.md "wikilink")、[民謠與](../Page/民謠.md "wikilink")[彌撒曲目](../Page/彌撒.md "wikilink")。他們平日居住在[奧地利的](../Page/奧地利.md "wikilink")[奧嘉登宮](../Page/奧嘉登宮.md "wikilink")（Augarten
Palast），而暑假時則是到奧國境內的[阿爾卑斯山地的克恩頓](../Page/阿爾卑斯山.md "wikilink")（Carinthia）河畔與後布呂爾(Hinterbruhl)
避暑，而在這裡，他們也有自己的私人別墅供度假之用。

其發聲方式採用「美聲法」

## 歷史

維也納少年合唱團成立於1498年，著名[音樂家](../Page/音樂家.md "wikilink")[海頓曾經是團員](../Page/海頓.md "wikilink")。海頓所居住的村莊中每逢節日都有樂隊慣例的遊行。有一次，負責打鼓的樂師因生病而無法參與遊行，只好請年幼的海頓來幫忙；興奮的海頓不負眾望的表現十分出色。有一次，海頓父親一位擔任「維也納史德分教堂合唱團」指揮的[親戚來到了海頓家中](../Page/親戚.md "wikilink")，對海頓的[音樂才華感到訝異](../Page/音樂.md "wikilink")，就央求把海頓的父親把海頓送進合唱團。維也納史德分教堂合唱團是維也納少年合唱團的前身。當然，維也納少年合唱團不只培養出海頓一人而已，五百年的歷史中也出現了其它優秀音樂家，因為沒有任何一個合唱團能像維也納少年合唱團一樣將[音樂教育實行得如此徹底](../Page/音樂教育.md "wikilink")。

維也納少年合唱團打從創團以來，演出範圍原本都僅僅限制在維也納的[教堂中](../Page/教堂.md "wikilink")，演唱的曲目也只有宗教歌曲，許許多多的愛樂者都無福親耳聽到。但[第一次世界大戰後](../Page/第一次世界大戰.md "wikilink")，他們開始在祖國所在地[歐洲](../Page/歐洲.md "wikilink")，甚至遠至[美國等地方演唱](../Page/美國.md "wikilink")，受到大眾的歡迎。而他們如何的受觀眾喜愛，可以藉由一些比較傑出的獨唱歌手，居然一躍而成為電視明星中了解。在[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，合唱團轉型而成為了算是“非營利性的半個國家機構”，舉團遷移至奧嘉頓宮殿（建造於16世紀），從此開始了一連串堪稱專業的訓練，和聲以及音樂演唱的技巧有著顯著的進步。由於世界各地的演出邀約不斷，維也納少年合唱團便把百位團員分成了4大團：[莫札特](../Page/莫札特.md "wikilink")、[布魯克納](../Page/布魯克納.md "wikilink")、[舒伯特及海頓](../Page/舒伯特.md "wikilink")，以應付忙碌的生活，並在世界各地輪流演出，使足跡得以遍佈世界各國。

## 人數

1498年創始之初，只有八人。而現在合唱學校約為250人，而團員則有約一百多人，分成四個團：Bruckner、Mozart、Haydn、Schubert。每年輪流有兩團在國外巡迴演出（為期三至四個月），一團留守維也納教堂（Hofburgkapelle）參加彌撒演出，而一團則在[維也納國立歌劇院參加表演](../Page/維也納國立歌劇院.md "wikilink")。2004年，則是有99位團員。

## 服裝

最早以前，WSK是為宮廷音樂會需求而設立，因而制服為宮廷服。[二十世紀初時服裝改為大部分教堂裡頭的唱詩班一樣為教袍](../Page/二十世紀.md "wikilink")，爾後則改為軍服，1930年代再由當時的軍服改為現今人們所熟知的水手服。據說當初改為水手服的原因，跟[唐老鴨那時風靡世界有著密切的關係](../Page/唐老鴨.md "wikilink")。
但WSK則表示是他們先走紅而才影響唐老鴨的穿著。

原本WSK左胸前是沒有團徽的，而到1960年代以後奧國政府宣佈今後奧地利的國徽亦為WSK團徽。

而如今WSK有自己私人的製衣商，而服裝也有兩種形式，深藍色的水手服(正式場合、下午的音樂會和大部分的教堂活動穿著)和白色的水手服(巡迴演出時在晚間的音樂會所穿)。
　 　

## 加入方法

每年該團都有兩次的入團考，7歲或8歲的小孩測驗著試唱、聽音、[樂理等基本測驗](../Page/樂理.md "wikilink")，通過後成為預備團員，而又經過兩年的訓練後，團員來到九歲，再通過正式的入學考，才能成為正式的团员，可以說各個都是萬中選一。而通常入團時是在十歲，大約十四歲[青春期變聲時再到合唱團的養老之家繼續學業](../Page/青春期.md "wikilink")，不過也有例外，像是Max
Emanuel Cencic就在該團待到16歲，也有团员11岁变声就开始养老。

然而現今時代改變，入團的競爭強度由1960年代的16倍成為現今的2至3倍。

進入何團時是隨著四個團的空缺來決定，不過有時候因為兄長已經在該團的因素，也會成為考慮因素，像Friesacher家：Rene、Kento、Akio和Guteirrez家：Konard
Gutierrez、Mirko Gutierrez。

而在以往，他們只願意接受奧地利的小孩，但在1990年初期，奧地利鄰近的國家像是[德國](../Page/德國.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[瑞士也都進入了該團](../Page/瑞士.md "wikilink")，而近幾年更是有遠從[美國](../Page/美國.md "wikilink")，甚至[中國的小孩來加入](../Page/中國.md "wikilink")。種族跟宗教原因已不再是問題。

## 規定

1.  錄取工作以男孩的聲樂和音樂能力為依據。
2.  男孩一旦入團，家長應該同意他們的兒子隨合唱團在本地或外國演出。
3.  組織者有義務為孩子們提供食宿、制服。
4.  孩子們的暑假應隨團在Hinterbichl的“合唱團之家”度過，這時不會安排旅行演出。
5.  所有男孩都要住宿。只要不與演出衝突，孩子們每週末可以回家，時間是星期六中午到星期天晚上。
6.  週一至週五的時間段內，家長不可以看望自己的兒子。若情況緊急，可以向指揮說明，允許見面。
7.  家長可以在任何時候要求孩子退出合唱團，同時，組織者也可以不說明理由把某個孩子開除出團。

## 工作

學校分成為四個學期，其中一個學期由其中一團在國外表演，而剩下的三團則在學校繼續學業。

在學校時，早上他們接受一般學科的教育，而下午則接受兩個小時的音樂課程。在中午午餐時間及晚上睡前有短暫的休息時間。而平日他們所從事的休閒如：[足球](../Page/足球.md "wikilink")、[棒球以及在奧嘉登宮內的私人](../Page/棒球.md "wikilink")[游泳池](../Page/游泳池.md "wikilink")[游泳](../Page/游泳.md "wikilink")。

平均來說，WSK一年有大約三百場[音樂會](../Page/音樂會.md "wikilink")（1998年：322場，1999年：284場），而這項數據也包含了一些私人性質的音樂會。也就是說平均一個團一年要表演75場音樂會，WSK演唱的日子是非常辛苦的。
　 　

## WSK過去曾有的名人

音樂家：舒伯特、海頓兄弟、Heinrich Isaac、Paul Hofhaimer、Heinrich Igbaz Franz
Biber、Johann Joseph Fux、Antonio Caldara、Anton Bruckner、Clemens
Kraus、Felix Mottl等。但莫札特從未加入過該團。

[指揮家](../Page/指揮家.md "wikilink")：李希特（Hans Richter）

## 錄製發行作品

曾經出過的LP黑膠可能數以百計。

## 外部連結

  - [官方網站](http://www.wsk.at/)
  - [維也納少年合唱團](https://web.archive.org/web/20060618112725/http://wsk.boytreble.net/)
  - [Sangerknaben
    \!](https://web.archive.org/web/20060719162304/http://www.boytreble.net/)

[Category:兒童合唱團](../Category/兒童合唱團.md "wikilink")
[Category:奧地利音樂](../Category/奧地利音樂.md "wikilink")
[Category:奥地利合唱团](../Category/奥地利合唱团.md "wikilink")