[Auntie_Tigress.jpg](https://zh.wikipedia.org/wiki/File:Auntie_Tigress.jpg "fig:Auntie_Tigress.jpg")表演的虎姑婆\]\]
《**虎姑婆**》是早期流傳在[臺灣等地的民間故事](../Page/臺灣.md "wikilink")，在臺灣和[白賊七](../Page/白賊七.md "wikilink")、[李田螺](../Page/李田螺.md "wikilink")、[賣香屁](../Page/賣香屁.md "wikilink")、[好鼻師及](../Page/好鼻師.md "wikilink")[水鬼城隍等一樣](../Page/水鬼城隍.md "wikilink")，家喻戶曉\[1\]。故事敘述山上的老虎精化身為老太婆，在夜裡拐騙小孩並吞食裹腹，常被用來哄騙小孩趕快入睡。\[2\]

目前所知最早有紀錄的同類故事，是清代[黃之雋所著](../Page/黃之雋.md "wikilink")〈虎媼傳〉\[3\]
，講述安徽一帶老母虎扮成外婆害人的故事。這類故事傳至台灣後，版本曾多達百餘種，但內容大同小異\[4\]，最為人知悉的版本是由臺灣作家[王詩琅所編撰](../Page/王詩琅.md "wikilink")\[5\]，故事背景為臺灣客家聚落\[6\]。

## 故事大綱

虎姑婆最常聽到的故事版本：

[老虎精在修行](../Page/老虎.md "wikilink")，必須吃幾個小孩才能完全變成人，所以下山找小孩吃。下山後，牠躲在一戶人家門外偷聽，知道媽媽要外出，屋子裡只有一對姐弟，於是就變成姑婆的模樣騙小孩開門進到屋子裡去。睡到半夜，虎姑婆吃了弟弟，發出咀嚼的聲音，姐姐聽到後問虎姑婆在吃什麼，虎姑婆說在吃[花生](../Page/花生.md "wikilink")，接著丟一塊弟弟的手指頭給姐姐，姐姐鎮定的假裝要上廁所，然後躲到門外的樹上，等到虎姑婆發現要吃她時，她機智的要求虎姑婆燒一鍋熱水（另一說是熱油）給她，並要求虎姑婆將熱水吊到樹上給她，她要自己跳到鍋裡，當虎姑婆把熱水用繩子吊到樹上時，姐姐叫虎姑婆閉上眼睛，張開嘴巴，然後把熱水淋在虎姑婆喉嚨裡，虎姑婆便因此喪命了。

但隨著地區不同各地也有不同版本的故事，故事也因此有不同的開頭與結局。諸如某些版本中屋裡的小孩是一對姊妹而非姐弟，而收服虎精的過程則是神仙化身為[老鼠指點兩姊妹如何逃出虎口](../Page/老鼠.md "wikilink")，最後用[符收服老虎精](../Page/符令.md "wikilink")。另外也有版本為熱油燙傷老虎精後，姊妹再用棍棒打死老虎。

## 評論

類似的故事情節從虎外婆變異為狼外婆的型式流傳於[中國各地](../Page/中國.md "wikilink")，早期的學者將它稱為「老虎外婆型」\[7\]，如前述〈虎媼傳〉
。[韓國](../Page/韓國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[越南等地也有類似情節的故事流傳](../Page/越南.md "wikilink")，與[歐洲](../Page/歐洲.md "wikilink")[小紅帽的故事相似](../Page/小紅帽.md "wikilink")。是屬於狼與七隻小羊和[小紅帽的複合型](../Page/小紅帽.md "wikilink")。此外，類似這樣的以兩個孩子與意圖謀害他們的成人的情節演繹的故事，同樣地在《[格林童話](../Page/格林童話.md "wikilink")》尚有〈韓賽爾與格麗特〉(Hänsel
und
Gretel；即[糖果屋](../Page/糖果屋.md "wikilink"))，在韓國則有〈〉的民間故事，這類故事的目的可能旨在警示孩子，不要隨便相信說是孩子的親人或藉機想進家門的陌生人，甚至開門讓他進到家裡來。

## 不同版本

  - 由《虎媼傳》可知，[中國](../Page/中國.md "wikilink")[安徽流傳的虎外婆故事](../Page/安徽.md "wikilink")，孩子是被挑擔人從樹上救走，而老母虎是被老虎同伴殺死的。

  -
  - [中國西南地區流傳的則是以黑熊爲主角的](../Page/中國.md "wikilink")“[熊家婆](../Page/熊家婆.md "wikilink")”（家婆即外婆）故事，情節大體相同。

## 現代通俗文化中的虎姑婆

在過去的幾十年中，虎姑婆的故事一直在臺灣的[表演藝術中不斷出現](../Page/表演藝術.md "wikilink")，較知名的包括有：歌手[金智娟的](../Page/金智娟.md "wikilink")[童謠](../Page/童謠.md "wikilink")、臺灣[公共電視臺所製播的](../Page/公共電視台.md "wikilink")[黏土](../Page/黏土.md "wikilink")[動畫](../Page/動畫.md "wikilink")，以及以驚悚手法所拍攝的電影。

### 童謠

虎姑婆的同名[童謠](../Page/童謠.md "wikilink")「[虎姑婆](http://www.youtube.com/watch?v=-Ms0Xq6hbHU)」是在1986年時收錄在[歌手](../Page/歌手.md "wikilink")[金智娟與丘丘哈唱團所推出的](../Page/金智娟.md "wikilink")[流行音樂](../Page/流行音樂.md "wikilink")[專輯中](../Page/專輯.md "wikilink")。

### 黏土動畫

臺灣[公共電視臺和](../Page/公共電視台.md "wikilink")[英國威爾斯電視臺曾合作製作虎姑婆的黏土動畫](../Page/英國.md "wikilink")，並在2000年10月22日獲得第17屆芝加哥國際兒童影展（the
17th Chicago International Children's Film Festival）的**電視類最佳動畫片獎**\[8\]。

### 電影

在2005年時，臺灣導演[王毓雅曾以驚悚方式重新編劇拍攝虎姑婆](../Page/王毓雅.md "wikilink")（Aunt
Tigress）的[電影](../Page/電影.md "wikilink")\[9\]。

### 童書

虎姑婆的童書在臺灣以不同型式廣泛流傳。在2007年時，臺灣插畫家王家珠（Eva
Wang）所繪製的虎姑婆畫冊\[10\]入選[義大利](../Page/義大利.md "wikilink")[波隆那世界兒童插畫展](../Page/波隆那世界兒童插畫展.md "wikilink")\[11\]，並譯為英文在美國發售。

### 动画

[學校怪談第](../Page/学校怪谈_\(动画\).md "wikilink")6话

## 相關條目

  - [坎卜斯](../Page/坎卜斯.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[Category:台灣妖怪](../Category/台灣妖怪.md "wikilink")
[Category:客家文化](../Category/客家文化.md "wikilink")
[Category:台灣民間故事](../Category/台灣民間故事.md "wikilink")
[Category:臺灣女性](../Category/臺灣女性.md "wikilink")
[Category:貓科主角故事](../Category/貓科主角故事.md "wikilink")
[Category:神话传说中的猫科动物](../Category/神话传说中的猫科动物.md "wikilink")

1.  [公共電視臺 聽故事遊世界：虎姑婆](http://www.pts.org.tw/~web02/26_animation/tw.htm)
2.  [虎姑婆](http://twblg.dict.edu.tw/holodict_new/result_detail.jsp?n_no=4573&source=9&level1=18&level2=67&level3=0&curpage=0&sample=0&radiobutton=0&querytarget=0&limit=1&pagenum=0&rowcount=0&cattype=1),
    臺灣閩南語常用辭典, [教育部](../Page/中華民國教育部.md "wikilink")
3.  [虎媼傳](http://tw.myblog.yahoo.com/jw!BlsnzqKBQE4XrkMloawpkGwNRA--/article?mid=597&next=596&l=f&fid=18)
4.  [梁姿茵，《虎姑婆故事類型》](http://www.uijin.idv.tw/student/yzcc213/2003%B0%AD%A9%C7%B6%C7%BB%A1/%B1%E7%AB%BA%AF%F4.%AA%EA%A9h%B1C%ACG%A8%C6%C3%FE%AB%AC.htm)
5.  王詩琅，《臺灣民間故事》，玉山社，ISBN 9578246048
6.  [傅斌暉，《虎姑婆來作客》](http://www.ntnu.edu.tw/art/tank/LessonPlans/plan086_aunt_tiger/086.htm)

7.  [老虎外婆故事](http://163.17.79.102/%A4%A4%B0%EA%A4j%A6%CA%AC%EC/Content.asp?ID=64034&Query=8)
8.  [公視「虎姑婆」再獲國際大獎](http://cgi.educities.edu.tw/cgi-bin/cgiwrap/jasonshome/cgi-bin/webbs/webbs.pl?bd=art-culture&log=32&crlist=1&crpage=2)
9.  [虎姑婆（2005）](http://movie.iamcool.net/tw/coolmovie_show_movie.php?movieID=3132)

10. [王家珠，《虎姑婆》](http://tiger33.myweb.hinet.net/newbook1.htm)
11. [Bologna Children's Book
    Fair](http://www.bookfair.bolognafiere.it/page.asp?m=52&l=2&a=&ma=6&c=620&p=522005selected)