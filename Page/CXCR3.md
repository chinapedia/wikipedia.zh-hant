**趋化因子受体CXCR3**是G蛋白偶连的七跨膜域受体选择性的与CXC[趋化因子](../Page/趋化因子.md "wikilink")（[CXCL9](../Page/CXCL9.md "wikilink")，[CXCL10](../Page/CXCL10.md "wikilink")，和[CXCL11](../Page/CXCL11.md "wikilink")）\[1\]。
CXCR3又称G蛋白偶联受体9(GPR9)和CD183。有两种变异的CXCR3受体。CXCR3－A与CXCL9，
CXCL10，CXCL11结合；而CXCR3-B除了与CXCL9，
CXCL10，CXCL11结合外还可以与[CXCL4结合](../Page/CXCL4.md "wikilink")\[2\]。

## 表达

CXCR3主要表达在[T细胞](../Page/T细胞.md "wikilink")、[自然杀伤细胞活性上](../Page/自然杀伤细胞.md "wikilink")\[3\]，有的[上皮细胞和一些](../Page/上皮细胞.md "wikilink")[内皮细胞也表达CXCR](../Page/内皮细胞.md "wikilink")3。
I型[辅助T细胞](../Page/辅助T细胞.md "wikilink")（Th1）优先表达CXCR3及[CCR5](../Page/CCR5.md "wikilink")\[4\]，而II型辅助T细胞（Th2）表达[CCR3和](../Page/CCR3.md "wikilink")[CCR4](../Page/CCR4.md "wikilink")。
CXCR3与配体结合后在诱导I型辅助T细胞（Th1）迁移的同时又阻止II型辅助T细胞（Th2）的迁徙。从而增强T细胞的分化效应。

## 信号传递

CXCR3与其配体CXCL9，
CXCL10，CXCL11的结合，能引起细胞钙离子的内流，启动肌醇磷脂3-激酶和丝裂原活化蛋白激酶（MAPK）\[5\]。详细的信号通路尚未确立，但与其他的趋化因子受体的信号传递有类似的激酶。

## 功能

CXCR3调节[白细胞迁徙](../Page/白细胞.md "wikilink")。CXCR3与配体相互作用引起I型辅助T细胞（Th1）的迁移，并促进I型辅助T细胞（Th1）成熟。

## 疾病

CXCR3可能在下列疾病中起作用，包括[动脉粥样硬化](../Page/动脉粥样硬化.md "wikilink")\[6\]、[多发性硬化](../Page/多发性硬化.md "wikilink")\[7\]、[肺纤维化](../Page/肺纤维化.md "wikilink")\[8\],
I型[糖尿病](../Page/糖尿病.md "wikilink")\[9\]，重症肌无力、急性心脏移植排斥\[10\]。开发阻断CXCR3与其配体相互作用的药物可提供治疗这些疾病的新途径。

## 参见

  - [趋化因子受体](../Page/趋化因子受体.md "wikilink")
  - [趋化因子](../Page/趋化因子.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Charo IF, Ransohoff RM. The many roles of chemokines and chemokine
    receptors in inflammation. N Engl J Med. 2006
    Feb 9;354(6):610-21.](http://content.nejm.org/cgi/content/extract/354/6/610)

[Category:细胞因子受体](../Category/细胞因子受体.md "wikilink")
[Category:免疫学](../Category/免疫学.md "wikilink")

1.  Clark-Lewis I, Mattioli I, Gong JH, Loetscher P. Structure-function
    relationship between the human chemokine receptor CXCR3 and its
    ligands. J Biol Chem. 2003 Jan 3;278(1):289-95.
2.  Lasagni L, Francalanci M, Annunziato F, Lazzeri E, Giannini S, Cosmi
    L, Sagrinati C, Mazzinghi B, Orlando C, Maggi E, Marra F, Romagnani
    S, Serio M, Romagnani P. An alternatively spliced variant of CXCR3
    mediates the inhibition of endothelial cell growth induced by IP-10,
    Mig, and I-TAC, and acts as functional receptor for platelet factor
    4. J Exp Med. 2003 Jun 2;197(11):1537-49.
3.  Qin S, Rottman JB, Myers P, Kassam N, Weinblatt M, Loetscher M, Koch
    AE, Moser B, Mackay CR. The chemokine receptors CXCR3 and CCR5 mark
    subsets of T cells associated with certain inflammatory reactions. J
    Clin Invest. 1998 Feb 15;101(4):746-54.
4.  Qin S, Rottman JB, Myers P, Kassam N, Weinblatt M, Loetscher M, Koch
    AE, Moser B, Mackay CR. The chemokine receptors CXCR3 and CCR5 mark
    subsets of T cells associated with certain inflammatory reactions. J
    Clin Invest. 1998 Feb 15;101(4):746-54.
5.  Smit MJ, Verdijk P, van der Raaij-Helmer EM, Navis M, Hensbergen PJ,
    Leurs R, Tensen CP. CXCR3-mediated chemotaxis of human T cells is
    regulated by a Gi- and phospholipase C-dependent pathway and not via
    activation of MEK/p44/p42 MAPK nor Akt/PI-3 kinase. Blood. 2003 Sep
    15;102(6):1959-65.
6.  Mach F, Sauty A, Iarossi AS, Sukhova GK, Neote K, Libby P, Luster
    AD. Differential expression of three T lymphocyte-activating CXC
    chemokines by human atheroma-associated cells. J Clin Invest. 1999
    Oct;104(8):1041-50.
7.  Sorensen TL, Tani M, Jensen J, Pierce V, Lucchinetti C, Folcik VA,
    Qin S, Rottman J, Sellebjerg F, Strieter RM, Frederiksen JL,
    Ransohoff RM. Expression of specific chemokines and chemokine
    receptors in the central nervous system of multiple sclerosis
    patients. J Clin Invest. 1999 Mar;103(6):807-15.
8.  Jiang D, Liang J, Hodge J, Lu B, Zhu Z, Yu S, Fan J, Gao Y, Yin Z,
    Homer R, Gerard C, Noble PW. Regulation of pulmonary fibrosis by
    chemokine receptor CXCR3. J Clin Invest. 2004 Jul;114(2):291-9.
9.  Frigerio S, Junt T, Lu B, Gerard C, Zumsteg U, Hollander GA, Piali
    L. Beta cells are responsible for CXCR3-mediated T-cell infiltration
    in insulitis. Nat Med. 2002 Dec;8(12):1414-20.
10. Hancock WW, Lu B, Gao W, Csizmadia V, Faia K, King JA, Smiley ST,
    Ling M, Gerard NP, Gerard C. Requirement of the chemokine receptor
    CXCR3 for acute allograft rejection. J Exp Med. 2000 Nov
    20;192(10):1515-20.