**西條市**（）為位於愛媛縣[東予地方的城市](../Page/東予地方.md "wikilink")。沿海地區與鄰近的[四國中央市](../Page/四國中央市.md "wikilink")、[新居濱市](../Page/新居濱市.md "wikilink")、[今治市](../Page/今治市.md "wikilink")、[松山市共同連成中四國地區最大規模的工業區](../Page/松山市.md "wikilink")，產業包括材料、半導体、造船、飲料、發電、鋼鐵等，工業產值為全四國地區最大。

## 地理

  - 山：　[石鎚山](../Page/石鎚山.md "wikilink")、[瓶森](../Page/瓶森.md "wikilink")、[笹峰](../Page/笹峰.md "wikilink")、[伊予富士](../Page/伊予富士.md "wikilink")
  - 河川：　[加茂川](../Page/加茂川_\(愛媛縣\).md "wikilink")、[中山川](../Page/中山川.md "wikilink")、渦井川

## 歷史

在7世紀時，由於日本在[白江口之战戰敗](../Page/白江口之战.md "wikilink")，為了預防中國的侵略，在此也建築了山城[永納山城作為防禦準備](../Page/永納山城.md "wikilink")。\[1\]

[江戶時代開始成為](../Page/江戶時代.md "wikilink")[西條藩的領地](../Page/西條藩.md "wikilink")，最初由[一柳氏統治](../Page/一柳氏.md "wikilink")，後來則成為[紀州德川家的支藩](../Page/紀州德川家.md "wikilink")，由[松平氏統治](../Page/松平氏.md "wikilink")。

### 年表

  - 1889年12月15日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：
      - [新居郡](../Page/新居郡.md "wikilink")：**西條町**、神拝村、大町村、玉津村、飯岡村、冰見村、橘村、神戶村、加茂村、大保木村。
      - [周敷郡](../Page/周敷郡.md "wikilink")：吉井村、周布村、多賀村、小松村、石根村、千足山村、福岡村、田野村、中川村。
      - [桑村郡](../Page/桑村郡.md "wikilink")：壬生川村、吉岡村、國安村、三芳村、庄內村、楠河村、德田村。
  - 1897年4月18日：周敷郡和桑村郡合併為[周桑郡](../Page/周桑郡.md "wikilink")。
  - 1898年1月22日：小松村改制為[小松町](../Page/小松町_\(愛媛縣\).md "wikilink")。
  - 1901年6月14日：壬生川村改制為[壬生川町](../Page/壬生川町.md "wikilink")。
  - 1908年1月1日：冰見村升改制為[冰見町](../Page/冰見町.md "wikilink")。
  - 1913年12月13日：福岡村改制並改名為[丹原町](../Page/丹原町.md "wikilink")。
  - 1925年2月11日：神拜村、大町村、玉津村被[併入西條町](../Page/市町村合併.md "wikilink")。
  - 1940年10月1日：多賀村被併入壬生川町。
  - 1941年4月29日：飯岡村、冰見町、橘村、神戶村和西條町合併為**西條市**。
  - 1951年8月10日：千足山村改名為石鎚村。
  - 1955年1月1日：
      - 周布村、吉井村、壬生川町、吉岡村、國安村[合併為新設置的壬生川町](../Page/市町村合併.md "wikilink")。
      - 三芳村、楠河村、庄內村合併為[三芳町](../Page/三芳町_\(愛媛縣\).md "wikilink")。
  - 1955年4月25日：
      - 石鎚村、小松町、石根村合併為新設置的小松町。
      - 德田村和丹原町合併為新設置的丹原町。
  - 1955年7月20日：中川村和周桑郡櫻樹村合併為中川村。
  - 1956年9月1日：
      - 丹原町、田野村和中川村的部分地區合併為新設置的丹原町，中川村的其餘地區（舊櫻樹村地區）與[溫泉郡川內村合併為](../Page/溫泉郡_\(日本\).md "wikilink")[川內町](../Page/川內町_\(愛媛縣\).md "wikilink")（現已合併為[東溫市](../Page/東溫市.md "wikilink")）。
  - 1956年9月28日：加茂村、大保木村和[新居濱市的部份地區被併入西條市](../Page/新居濱市.md "wikilink")。
  - 1956年9月30日：丹原町的明河字鹽獄地區被併入溫泉郡川內町。
  - 1971年1月1日：壬生川町和三芳町合併為東予町。
  - 1972年10月1日：東予町改制為[東予市](../Page/東予市.md "wikilink")。
  - 2004年11月1日：小松町、丹原町和西條市、東予市合併為新設置的**西條市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>川上村</p></td>
<td><p>1955年4月25日<br />
合併為溫泉郡川內村</p></td>
<td><p>1956年9月1日<br />
合併為溫泉郡川內町</p></td>
<td><p>2004年9月21日<br />
合併為東溫市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>三內村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>櫻樹村</p></td>
<td><p>1955年7月20日<br />
合併為中川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中川村</p></td>
<td><p>1956年9月1日<br />
合併為丹原町</p></td>
<td><p>2004年11月1日<br />
合併為西條市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>福岡村</p></td>
<td><p>1913年12月13日<br />
改制並改名為丹原町</p></td>
<td><p>1955年4月25日<br />
合併為丹原町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>德田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>田野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>小松村</p></td>
<td><p>1898年11月22日<br />
小松町</p></td>
<td><p>1955年4月25日<br />
合併為小松町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>石根村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>千足山村</p></td>
<td><p>1951年8月10日<br />
改名為石鎚村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>吉井村</p></td>
<td><p>1955年1月1日<br />
合併為壬生川町</p></td>
<td><p>1971年1月1日<br />
合併為東予町</p></td>
<td><p>1972年10月1日<br />
東予市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>周布村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>多賀村</p></td>
<td><p>1940年10月1日<br />
併入壬生川町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>壬生川村</p></td>
<td><p>1901年6月14日<br />
壬生川町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>吉岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>國安村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>三芳村</p></td>
<td><p>1955年1月1日<br />
合併為三芳町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>庄內村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>楠河村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>西條町</p></td>
<td><p>1941年4月29日<br />
合併為西條市</p></td>
<td><p>西條市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>神拝村</p></td>
<td><p>1925年2月11日<br />
併入西條町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>大町村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>玉津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>飯岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>冰見村</p></td>
<td><p>1908年1月1日<br />
冰見町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>橘村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>神戶村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>加茂村</p></td>
<td><p>1956年9月28日<br />
併入西條市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大保木村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歴任市長

| 屆 | 姓名                                   | 上任年月日       | 卸任年月日       |
| - | ------------------------------------ | ----------- | ----------- |
| 1 | 高橋作一郎                                | 1941年7月3日   | 1945年7月3日   |
| 2 | [十河信二](../Page/十河信二.md "wikilink")   | 1945年7月3日   | 1946年4月17日  |
| 3 | 高橋初次郎                                | 1946年4月17日  | 1951年4月4日   |
| 4 | 岡本達一                                 | 1951年4月23日  | 1955年4月22日  |
| 5 | 文野俊一郎                                | 1955年5月2日   | 1959年11月16日 |
| 6 | 村上德太郎                                | 1959年12月18日 | 1971年12月17日 |
| 7 | 伊藤一                                  | 1971年12月18日 | 1979年12月17日 |
| 8 | 桑原富雄                                 | 1979年12月18日 | 1995年12月17日 |
| 9 | [伊藤宏太郎](../Page/伊藤宏太郎.md "wikilink") | 1995年11月28日 | 現任          |

## 交通

### 鐵路

  - [四國旅客鐵道](../Page/四國旅客鐵道.md "wikilink")
      - [予讚線](../Page/予讚線.md "wikilink")：[伊予西條車站](../Page/伊予西條車站.md "wikilink")
        - [石鎚山車站](../Page/石鎚山車站.md "wikilink") -
        [伊予冰見車站](../Page/伊予冰見車站.md "wikilink") -
        [伊予小松車站](../Page/伊予小松車站.md "wikilink") -
        [玉之江車站](../Page/玉之江車站.md "wikilink") -
        [壬生川車站](../Page/壬生川車站.md "wikilink") -
        [伊予三芳車站](../Page/伊予三芳車站.md "wikilink")

|                                                                                                                   |                                                                                                                  |
| ----------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| [Iyosaizyou_station.JPG](https://zh.wikipedia.org/wiki/File:Iyosaizyou_station.JPG "fig:Iyosaizyou_station.JPG") | [Iyohimi20080301.JPG](https://zh.wikipedia.org/wiki/File:Iyohimi20080301.JPG "fig:Iyohimi20080301.JPG")          |
| [Iyo-KomatsuStation.JPG](https://zh.wikipedia.org/wiki/File:Iyo-KomatsuStation.JPG "fig:Iyo-KomatsuStation.JPG")  | [Iyomiyoshi20080301.JPG](https://zh.wikipedia.org/wiki/File:Iyomiyoshi20080301.JPG "fig:Iyomiyoshi20080301.JPG") |

### 道路

  - 高速道路

<!-- end list -->

  - [松山自動車道](../Page/松山自動車道.md "wikilink")：[伊予西條交流道](../Page/伊予西條交流道.md "wikilink")
    - [石鎚山服務區](../Page/石鎚山服務區.md "wikilink") -
    [伊予小松系統交流道](../Page/伊予小松系統交流道.md "wikilink")
  - [今治小松自動車道](../Page/今治小松自動車道.md "wikilink")（今治小松道路）：[東予丹原交流道](../Page/東予丹原交流道.md "wikilink")
    - [伊予小松北交流道](../Page/伊予小松北交流道.md "wikilink")

### 港口

  - [東予港](../Page/東予港.md "wikilink")：有[四國開發渡輪經營往返](../Page/四國開發渡輪.md "wikilink")[大阪南港之定期渡輪](../Page/大阪南港.md "wikilink")

## 觀光資源

[四国鉄道文化館で展示されているDF50-1.JPG](https://zh.wikipedia.org/wiki/File:四国鉄道文化館で展示されているDF50-1.JPG "fig:四国鉄道文化館で展示されているDF50-1.JPG")
[Maeɡamizi0ɨ.jpg](https://zh.wikipedia.org/wiki/File:Maeɡamizi0ɨ.jpg "fig:Maeɡamizi0ɨ.jpg")\]\]

### 景點

  - [四國鐵道文化館](../Page/四國鐵道文化館.md "wikilink")
  - 西條市立鄉土博物館
  - 西條市考古歴史館
  - 史跡近藤篤山舊邸
  - 愛媛民藝館
  - [四國八十八箇所](../Page/四國八十八箇所.md "wikilink")
      - 60番札所 石鎚山 [横峰寺](../Page/横峰寺.md "wikilink")
      - 61番札所 栴檀山 [香園寺](../Page/香園寺.md "wikilink")
      - 62番札所 天養山 [寶壽寺](../Page/寶壽寺.md "wikilink")
      - 63番札所 密教山 [吉祥寺](../Page/吉祥寺.md "wikilink")
      - 64番札所 石鎚山 [前神寺](../Page/前神寺.md "wikilink")
  - [湯之谷温泉](../Page/湯之谷温泉_\(愛媛縣\).md "wikilink")
  - [本谷温泉](../Page/本谷温泉.md "wikilink")
  - 石鎚温泉

### 祭典、活動

[Saijou_matsuri_isono.JPG](https://zh.wikipedia.org/wiki/File:Saijou_matsuri_isono.JPG "fig:Saijou_matsuri_isono.JPG")

  - [西條祭](../Page/西條祭.md "wikilink")：正式名稱為[石岡神社](../Page/石岡神社.md "wikilink")[例大祭](../Page/例大祭.md "wikilink")、[伊曾乃神社例大祭](../Page/伊曾乃神社.md "wikilink")、[飯積神社例大祭](../Page/飯積神社.md "wikilink")，習慣上合稱為「西條祭」。
  - 丹原七夕夏祭：每年8月5日〜7日期間舉辦
  - [小松](../Page/小松.md "wikilink")[三嶋神社例大祭](../Page/三嶋神社.md "wikilink")：毎年10月16、17日舉辦
  - 壬生川祭
      - 鶴岡八幡神社：毎年10月12、13日舉辦
      - 宇賀神社：毎年10月12、13日舉辦
      - 三保神社：毎年10月15、16日舉辦
      - [五所神社](../Page/五所神社.md "wikilink")：毎年10月15、16日舉辦
      - 周敷神社：毎年10月15、16日舉辦
  - 丹原祭：
      - 福岡八幡神社：毎年10月16、17日舉辦
  - 西條山祭

## 教育

[Saijo_entry.jpg](https://zh.wikipedia.org/wiki/File:Saijo_entry.jpg "fig:Saijo_entry.jpg")

  - [愛媛縣立西條高等學校](../Page/愛媛縣立西條高等學校.md "wikilink")
  - [愛媛縣立西條農業高等學校](../Page/愛媛縣立西條農業高等學校.md "wikilink")
  - [愛媛縣立東予高等學校](../Page/愛媛縣立東予高等學校.md "wikilink")
  - [愛媛縣立小松高等學校](../Page/愛媛縣立小松高等學校.md "wikilink")
  - [愛媛縣立丹原高等學校](../Page/愛媛縣立丹原高等學校.md "wikilink")

## 友好、姐妹城市

### 海外

  - [保定市](../Page/保定市.md "wikilink")（[中華人民共和國](../Page/中華人民共和國.md "wikilink")[河北省](../Page/河北省.md "wikilink")）

## 本地出身之名人

  - [菊川剛](../Page/菊川剛.md "wikilink")：[日本](../Page/日本.md "wikilink")[奧林巴斯股份有限公司前董事長及總裁](../Page/奧林巴斯股份有限公司.md "wikilink")
  - [德川茂承](../Page/德川茂承.md "wikilink")：[紀州德川家最後一任](../Page/紀州德川家.md "wikilink")[藩主](../Page/藩主.md "wikilink")。
  - [三浦安](../Page/三浦安.md "wikilink")：曾任明治政府[大藏省官吏](../Page/大藏省.md "wikilink")、元老院議官、[貴族院議員](../Page/貴族院_\(日本\).md "wikilink")、[東京府知事](../Page/東京府.md "wikilink")、[宮中顧問官](../Page/宮中顧問官.md "wikilink")
  - [佐伯勇](../Page/佐伯勇.md "wikilink")：[近畿日本鐵道名譽會長](../Page/近畿日本鐵道.md "wikilink")
  - [岡田和一郎](../Page/岡田和一郎.md "wikilink")：醫師、日本耳鼻喉科始祖
  - [眞鍋かをり](../Page/眞鍋かをり.md "wikilink")：藝人
  - [關行男](../Page/關行男.md "wikilink")：日軍首次特別攻擊隊「[神風特攻隊](../Page/神風特攻隊.md "wikilink")」中「敷島小隊」的隊長。
  - [朝潮太郎
    (2代)](../Page/朝潮太郎_\(2代\).md "wikilink")：[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")、最高位階為[大關](../Page/大關.md "wikilink")，退休後成為第3代高砂親方，經營[高砂部屋](../Page/高砂部屋.md "wikilink")。
  - [千葉茂](../Page/千葉茂.md "wikilink")：職業棒球選手、教練
  - [森本潔](../Page/森本潔.md "wikilink")：職業棒球選手
  - [松木幹一郎](../Page/松木幹一郎.md "wikilink")：東京市電氣局局長、山下汽船副社長、帝都復興院副總裁、台灣電力株式會社社長
  - [秦豐](../Page/秦豐.md "wikilink")：前[參議院](../Page/參議院_\(日本\).md "wikilink")[議員](../Page/議員.md "wikilink")
  - [潮見佳男](../Page/潮見佳男.md "wikilink")：[民法](../Page/民法.md "wikilink")[學者](../Page/學者.md "wikilink")
  - [村上龍司](../Page/村上龍司.md "wikilink")：空手道教練、[士道館士魂村上塾長](../Page/士道館.md "wikilink")
  - [沖原佳典](../Page/沖原佳典.md "wikilink")：職業棒球選手
  - [野口茂樹](../Page/野口茂樹.md "wikilink")：職業棒球選手
  - [芥川澄夫](../Page/芥川澄夫.md "wikilink")：[歌手](../Page/歌手.md "wikilink")
  - [秋川雅史](../Page/秋川雅史.md "wikilink")：男高音
  - [西興一朗](../Page/西興一朗.md "wikilink")：演員
  - [宮崎謙彦](../Page/宮崎謙彦.md "wikilink")：排球選手
  - [長友佑都](../Page/長友佑都.md "wikilink")：職業足球選手
  - [川又堅碁](../Page/川又堅碁.md "wikilink")：職業足球選手
  - [酒井貴浩](../Page/酒井貴浩.md "wikilink")：演員

## 參考資料

## 外部連結

  - [西條市官方網頁](https://web.archive.org/web/20120709073005/http://www.city.saijo.ehime.jp/chaina/index.htm)

  - [西条市觀光協會](http://www.saijo-imadoki.jp/)

  - [西條旅館公會](http://saijo-yado.visithp.jp/index.html)

  - [西條商工會議所](http://www.saijocci.or.jp/)

  -
  -
<!-- end list -->

1.