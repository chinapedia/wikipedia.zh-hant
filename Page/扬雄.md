[Yang_Xiong.jpg](https://zh.wikipedia.org/wiki/File:Yang_Xiong.jpg "fig:Yang_Xiong.jpg")

**扬雄**（），一作**杨雄**，字**子雲**，[西汉哲学家](../Page/西汉.md "wikilink")、文学家、语言学家，蜀郡成都（今[四川](../Page/四川.md "wikilink")[成都](../Page/成都.md "wikilink")[郫都区](../Page/郫都区.md "wikilink")）人。

## 生平

为人[口吃](../Page/口吃.md "wikilink")，相貌平平，身高偏矮，不能剧谈，专心于潜心思考，早年倾慕[司马相如](../Page/司马相如.md "wikilink")，模仿司马相如的《子虚》、《上林》等赋，常作[辞赋](../Page/辞赋.md "wikilink")，名声远播，如《蜀都賦》，此賦開啟了京都一派題材，[班固](../Page/班固.md "wikilink")《兩都賦》、[張衡](../Page/張衡.md "wikilink")《二京賦》以及[晉代](../Page/晉代.md "wikilink")[左思](../Page/左思.md "wikilink")《三都賦》皆受其影響。

大司馬車騎將軍[王音召為門下史](../Page/王音.md "wikilink")。后经蜀人[楊莊推荐](../Page/楊莊.md "wikilink")，[汉成帝命他随侍左右](../Page/汉成帝.md "wikilink")。前11年正月与成帝前往[甘泉宫](../Page/甘泉宫.md "wikilink")，作《甘泉赋》讽刺成帝铺张。十二月又作《羽猎赋》仍然以劝谏为主题。被封[黄门侍郎](../Page/黄门侍郎.md "wikilink")，与[王莽](../Page/王莽.md "wikilink")、[刘歆等为同僚](../Page/刘歆.md "wikilink")。前10年扬雄作《长杨赋》，继续对成帝铺张奢侈提出批评。

扬雄后来认为辞赋为“雕虫篆刻”，“壮夫不为”，转而研究[哲学](../Page/哲学.md "wikilink")。仿《[论语](../Page/论语.md "wikilink")》作《[法言](../Page/法言.md "wikilink")》，模仿《[易经](../Page/易经.md "wikilink")》作《[太玄](../Page/太玄.md "wikilink")》。提出以“玄”作为宇宙万物根源之学说。有人笑他\[1\]，於是他寫了一篇《解嘲》。為了寬慰自己，又寫了一篇《[逐貧賦](../Page/s:逐貧賦.md "wikilink")》。[王莽当政](../Page/王莽.md "wikilink")，拉拢扬雄，任他为中散大夫。他写过《剧秦美新》美化[王莽](../Page/王莽.md "wikilink")，但此文是主动投靠还是避祸之作，还有争论\[2\]。

后在[天祿閣校书](../Page/天祿閣.md "wikilink")，写作，进行[语言学研究](../Page/语言学.md "wikilink")。曾著《[方言](../Page/輶轩使者绝代语释别国方言.md "wikilink")》，叙述[西汉时代各地方言](../Page/西汉.md "wikilink")，为研究古代语言的重要资料。但是因弟子[劉棻牵连](../Page/劉棻.md "wikilink")，有人前来[逮捕扬雄](../Page/逮捕.md "wikilink")，扬雄恐惧而[跳楼](../Page/跳楼.md "wikilink")，未死，后得免，[京師諺曰](../Page/京師.md "wikilink")：“惟寂寞，自投閣。”後召為[大夫](../Page/大夫.md "wikilink")，默默无闻而终。

## 思想

[Yang_xiong.jpg](https://zh.wikipedia.org/wiki/File:Yang_xiong.jpg "fig:Yang_xiong.jpg")

他提出以“玄”作为[宇宙万物根源的学说](../Page/宇宙.md "wikilink")，强调如实的认识自然现象，并认为“有生者必有死，有始者必有终”，驳斥了[方士的学说](../Page/方士.md "wikilink")。

在[人性論上](../Page/人性.md "wikilink")，揚雄於《法言·修身》說：「人之性也，善惡混，修其善，則為善人；修其惡，則為惡人。」他以氣稟言人的實然之性，採[告子](../Page/告子.md "wikilink")、[荀子](../Page/荀子.md "wikilink")「化性起偽」的思路，謂善惡相混的人性有待後天外鑠性的教化。他標準「[君子](../Page/君子.md "wikilink")」為教育的目標，教學的內容是禮義，「視、聽、言、貌、思」的導正是陶塑品德的必要重點。其中「思」是心的發用所在，揚雄稱心的難測之用為「神」，為教化的關鍵所在。

## 評價

  - [張衡稱他](../Page/張衡.md "wikilink")「妙極道術」。
  - [朱熹認為他](../Page/朱熹.md "wikilink")「拙底工夫」。

## 文学形象

扬雄的形象常作为潜心于学术，不慕名利，不谙世故的学者的典型出现在后世的文学作品中。

  - [左思](../Page/左思.md "wikilink")《咏史·济济京城内》（八首其四）:“寂寂扬子宅，门无卿相舆。寥寥空宇中，所讲在玄虚。言论准仲尼，辞赋拟相如。悠悠百世后，英名擅八区。”
  - [卢照邻](../Page/卢照邻.md "wikilink")《长安古意》:“寂寂寥寥扬子居，年年岁岁一床书。独有南山桂花发，飞来飞去袭人裾。”
  - [李白](../Page/李白.md "wikilink")《古风·咸阳二三月》（五十九首其八）:“子云不晓事，晚献《长杨》辞。赋达身已老，草《玄》鬓若丝。投阁良可叹，但为此辈嗤。”
  - [李白](../Page/李白.md "wikilink")《古风·一百四十年》（五十九首其四十六）:“独有扬执戟，闭关草《太玄》。”
  - [李白](../Page/李白.md "wikilink")《侠客行》：“谁能书阁下，白首《太玄》经？”
  - [李贺](../Page/李贺.md "wikilink")《绿章封事》：“金家香衖千轮鸣，扬雄秋室无俗声。”
  - [戴叔伦](../Page/戴叔伦.md "wikilink")《行路难》：“扬雄闭门空读书，门前碧草春离离。”

## 参考文献

## 主要著作

  - 《[法言](../Page/法言.md "wikilink")》
  - 《[太玄](../Page/太玄.md "wikilink")》
  - 《[方言](../Page/方言_\(著作\).md "wikilink")》

{{-}}

[Y扬](../Page/category:汉朝作家.md "wikilink")
[Y扬](../Page/category:中国哲学家.md "wikilink")
[Y扬](../Page/category:郫县人.md "wikilink")
[X雄](../Page/category:扬姓.md "wikilink")
[X雄](../Page/category:杨姓.md "wikilink")

[Y扬](../Category/中国语言学家.md "wikilink")

1.  [陳善](../Page/陳善.md "wikilink")《捫蝨新話》卷一說：“楊子雲作法言，以擬論語。……可發千載一笑。”
2.  [洪邁在](../Page/洪邁.md "wikilink")《容齋隨筆》中為其辯護說：“揚雄仕漢，親蹈王莽之變，退托其身於列大夫中，抱道沒齒。世儒或以《劇秦美新》貶之；是不然，此雄不得已而作也。夫誦述[新莽之德](../Page/新莽.md "wikilink")，止能美于暴秦，其深意固可知矣。序所言配五帝冠三王，開闢以來未之聞，直以戲莽爾。”