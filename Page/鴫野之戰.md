**鴫野之戰**是[大坂冬之陣其中一場戰爭](../Page/大坂冬之陣.md "wikilink")。

大坂城東北、大和川的北岸為今福村、南岸為鴫野村。地域屬低溼地帶、軍隊展開不易，僅能在堤防上防備。豐臣方在鴫野村設有三重柵欄、由[井上賴次率兵](../Page/井上賴次.md "wikilink")2,000守備。[德川家康則在今福村築城](../Page/德川家康.md "wikilink")、並命人奪取今福、鴫野。鴫野方為[上杉景勝勢](../Page/上杉景勝.md "wikilink")5,000兵，後援有[堀尾忠晴](../Page/堀尾忠晴.md "wikilink")、[丹羽長重](../Page/丹羽長重.md "wikilink")、[榊原康勝等](../Page/榊原康勝.md "wikilink")。

11月26日早上，上杉勢向鴫野攻擊，上杉麾下的[安田能元](../Page/安田能元.md "wikilink")、[須田長義攻占柵欄](../Page/須田長義.md "wikilink")，井上賴次被討死。豐臣軍[大野治長率軍](../Page/大野治長.md "wikilink")12,000來援並反擊。上杉勢自第一柵欄後退，[水原親憲在崩潰前聚攏左右鐵砲隊向豐臣軍一齊射擊](../Page/水原親憲.md "wikilink")。後來安田能元隊也加入射擊，而堀尾、丹羽兩隊支援上杉景勝，在到達前，豐臣軍已經撤退。

## 參考資料

  - 《戦況図録 大坂の陣》（新人物往来社 別冊歴史読本56）　ISBN 4404030568
  - 《激闘 大坂の陣》，[學習研究社](../Page/學習研究社.md "wikilink")　ISBN 4056022364

[Category:大坂之役](../Category/大坂之役.md "wikilink")
[Category:米澤上杉氏](../Category/米澤上杉氏.md "wikilink")
[Category:丹羽氏](../Category/丹羽氏.md "wikilink")
[Category:攝津國](../Category/攝津國.md "wikilink")
[Category:堀尾氏](../Category/堀尾氏.md "wikilink")
[Category:榊原氏](../Category/榊原氏.md "wikilink")
[Category:長井氏](../Category/長井氏.md "wikilink")
[Category:渡邊氏](../Category/渡邊氏.md "wikilink")