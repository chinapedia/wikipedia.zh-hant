**盧淑儀**（英文名：，），[华裔加拿大人](../Page/华裔加拿大人.md "wikilink")，1996年參加[多倫多華裔小姐競選奪得冠軍](../Page/多倫多華裔小姐競選.md "wikilink")，繼而在1997年代表[多倫多參加](../Page/多倫多.md "wikilink")[國際華裔小姐競選奪冠](../Page/國際華裔小姐競選.md "wikilink")。她並不是香港著名歌手[李克勤的太太](../Page/李克勤.md "wikilink")[盧淑儀](../Page/盧淑儀_\(香港\).md "wikilink")。當選國際華裔小姐奪得冠軍後，她投入了演藝工作，曾經與她演出的著名影星由香港著名演員[吳彥祖](../Page/吳彥祖.md "wikilink")，以至[荷里活著名動作影星](../Page/荷里活.md "wikilink")[史蒂芬·席格](../Page/史蒂芬·席格.md "wikilink")。2007年她和男朋友參加了[AXN電視節目](../Page/AXN.md "wikilink")[极速前进亚洲版2](../Page/极速前进亚洲版2.md "wikilink")，首集已於2007年11月22日播出。
\[1\] \[2\]

## 獎項

  - 1996年多倫多華裔小姐競選冠軍
  - 1997年國際華裔小姐競選冠軍

## 演出作品

  - [跑馬地的月光](../Page/跑馬地的月光.md "wikilink")（2000）

  - [重裝警察](../Page/重裝警察.md "wikilink")（2001）

  - [月滿抱西環](../Page/月滿抱西環.md "wikilink")（2001）

  - [7號差館](../Page/7號差館.md "wikilink")（2001）

  - [風流家族](../Page/風流家族.md "wikilink")（2002）

  - [乾柴烈火](../Page/乾柴烈火.md "wikilink")（2002）

  - [夏日殺手](../Page/夏日殺手.md "wikilink")（2002）

  - [這個夏天有異性](../Page/這個夏天有異性.md "wikilink")（2002）

  - [赤裸特工](../Page/赤裸特工.md "wikilink")（2002）

  - [手足情深](../Page/手足情深.md "wikilink")（2002）

  - [賭俠2002](../Page/賭俠2002.md "wikilink")（2002）

  - [偷窺無罪II之誘人犯罪](../Page/偷窺無罪II之誘人犯罪.md "wikilink")（2003）

  - [非典人生](../Page/非典人生.md "wikilink")（2003）

  - （2003）

  - [我的大嚿父母](../Page/我的大嚿父母.md "wikilink")（2004）

  - [煎釀叄寶](../Page/煎釀叄寶.md "wikilink")（2004）

  - [墨斗先生](../Page/墨斗先生.md "wikilink")（2004）

  - [Legacy](../Page/Legacy.md "wikilink")（2007）

## 參考資料

## 外部連結

  -
  -
  -
  -
  -
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[前新時代電視藝員](../Category/前新時代電視藝員.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:多倫多人](../Category/多倫多人.md "wikilink")
[S](../Category/盧姓.md "wikilink")
[Category:在香港的加拿大人](../Category/在香港的加拿大人.md "wikilink")

1.
2.  <http://misshongkongpageant.tripod.com/chinese1997.htm>