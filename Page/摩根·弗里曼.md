**摩根·弗里曼**（，）是一位[美國男](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")、[導演](../Page/導演.md "wikilink")，曾分別獲得一次[奧斯卡金像獎與](../Page/奧斯卡金像獎.md "wikilink")[金球獎](../Page/金球獎.md "wikilink")。摩根·弗里曼於90年代初成名，並演出過許多著名的[好萊塢電影](../Page/好萊塢.md "wikilink")。

## 經歷

### 早年

摩根·弗里曼出生在[田納西州的](../Page/田納西州.md "wikilink")[孟菲斯](../Page/孟菲斯_\(田納西州\).md "wikilink")，父親摩根·波特菲爾德·弗里曼（Morgan
Porterfield
Freeman）為理髮師，於1961年死於[肝硬化](../Page/肝硬化.md "wikilink")\[1\]\[2\]\[3\]，母親梅米·愛德娜（Mayme
Edna）則是[清潔工](../Page/清潔工.md "wikilink")。摩根·弗里曼排行第四，也是老么。摩根·弗里曼全家在兒童時期經常搬家，包括[密西西比州的](../Page/密西西比州.md "wikilink")[綠林塢](../Page/綠林塢.md "wikilink")（Greenwood）、[加里與](../Page/加里_\(印第安納州\).md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")\[4\]。摩根·弗里曼在8歲時首次登台表演，在學校戲劇中扮演主角。12歲時贏得州際戲劇競賽，並且在高中時於[納許維爾的廣播劇表演](../Page/納許維爾.md "wikilink")。在1955年，摩根·弗里曼拒絕[傑克森州立大學部分戲劇的獎學金](../Page/傑克森州立大學.md "wikilink")，選擇成為[美國空軍的技工](../Page/美國空軍.md "wikilink")。

弗里曼在1960年代早期遷居到[洛杉磯](../Page/洛杉磯.md "wikilink")，在洛杉磯社區大學中工作。在這時期，他也住在[紐約](../Page/紐約.md "wikilink")，並且是1964年紐約[世界博覽會的舞者](../Page/世界博覽會.md "wikilink")。而他也是[舊金山的歌劇音樂團體的成員](../Page/舊金山.md "wikilink")。

他在1967年時於[外百老匯音樂劇初次登臺演出](../Page/外百老匯音樂劇.md "wikilink")，在《The Nigger
Lovers》（有關[非裔美國人民權運動中的](../Page/非裔美國人民權運動.md "wikilink")[自由行](../Page/自由行.md "wikilink")）中飾演加維葳卡·林德弗斯（Viveca
Lindfors）的對手\[5\]\[6\]。弗里曼在1968年於[百老匯初次登臺](../Page/百老匯.md "wikilink")，演出全部由黑人演出的《[你好，多莉！](../Page/你好，多莉！.md "wikilink")》（*Hello,
Dolly\!*），由凱伯·凱洛威（Cab Calloway）與琵兒·貝利（Pearl Bailey）所主演。

### 演員生涯

[Morgan_Freeman_Cannes.jpg](https://zh.wikipedia.org/wiki/File:Morgan_Freeman_Cannes.jpg "fig:Morgan_Freeman_Cannes.jpg")\]\]
雖然摩根·弗里曼第一次確切在電影中演出是1971年的《Who Says I Can't Ride a
Rainbow?》，不過他第一次在美國媒體上出現則是在肥皂劇《Another
World》與[公共廣播協會的兒童劇場](../Page/公共廣播協會.md "wikilink")《電力公司》（*The Electric
Company*）中\[7\]，並且曾經宣稱他應該比計畫中還要早離開《電力公司》。

從1980年代中期開始，摩根·弗里曼開始在許多劇情片中演出重要的配角，扮演一位睿智的長者為他贏得良好的名聲\[8\]。因此，他在電影中扮演的角色也越來越重要，例如《[溫馨接送情](../Page/溫馨接送情.md "wikilink")》（*Driving
Miss Daisy*）中的霍克（Chauffeur
Hoke）、《[光榮戰役](../Page/光榮戰役.md "wikilink")》中的羅林斯軍曹，這兩部電影都是在1989年上映\[9\]。

摩根·弗里曼的明星氣質已經在1990年代的幾部他所主演的著名電影中顯露出來，例如《[俠盜王子羅賓漢](../Page/俠盜王子羅賓漢.md "wikilink")》（*Robin
Hood: Prince of
Thieves*）、《[火線追緝令](../Page/火線追緝令.md "wikilink")》與《[彗星撞地球](../Page/彗星撞地球.md "wikilink")》（*Deep
Impact*）。摩根·弗里曼在1994年極受好評的電影《[刺激1995](../Page/刺激1995.md "wikilink")》中扮演囚犯瑞德的角色，並且入圍了奧斯卡最佳男主角獎。

摩根·弗里曼及洛莉·麥克里（Lori
McCreary）在1997年成立了Revelations娛樂製片公司與線上電影下載公司ClickStar。

摩根·弗里曼與喜劇演員[金·凯瑞在](../Page/金·凯瑞.md "wikilink")2003年時共同主演了賣座喜劇片《[王牌天神](../Page/王牌天神.md "wikilink")》，在劇中費里曼扮演[上帝的角色](../Page/上帝.md "wikilink")。這部片後來在上映的第一週奪下北美票房的冠軍，續集《[冒牌天神2](../Page/冒牌天神2.md "wikilink")》則於2007年上映，並再度成為當週的票房冠軍。

摩根·弗里曼也曾與中國武打演員[李連杰一起演出電影](../Page/李連杰.md "wikilink")《[狼犬丹尼](../Page/狼犬丹尼.md "wikilink")》（*Danny
The Dog*），飾演一位失明的修琴師傅。

由於摩根·弗里曼獨特的聲音容易被人們辨認出來，所以他經常擔任旁白的角色。單單在2005年，摩根·弗里曼就在兩部知名電影中擔任旁白，分別是《[強戰世界](../Page/世界大戰_\(2005年電影\).md "wikilink")》與贏得奧斯卡獎的紀錄片《[企鵝寶貝](../Page/企鵝寶貝.md "wikilink")》（*March
of the
Penguins*）。在入圍3次奧斯卡獎（分別是1987年的《惡街實況》、1989年的《山水喜相逢》與1994年的《[刺激1995](../Page/刺激1995.md "wikilink")》）之後，摩根·弗里曼終於在2005年以《[登峰造擊](../Page/登峰造擊.md "wikilink")》獲得第77屆的[奧斯卡最佳男配角獎](../Page/奧斯卡最佳男配角獎.md "wikilink")\[10\]。

摩根·弗里曼在《[遗愿清单](../Page/遗愿清单.md "wikilink")》中與[杰克·尼科尔森演出對手戲](../Page/杰克·尼科尔森.md "wikilink")，扮演一位末期的癌症病患，這部電影於2008年上映。

摩根·弗里曼預計將在2008年4月重回百老匯演出[麥克·尼克斯](../Page/麥克·尼克斯.md "wikilink")（Mike
Nichols）執導[克里夫·歐戴茲](../Page/克里夫·歐戴茲.md "wikilink")（Clifford
Odets）的戲劇《鄉村少女》（*The Country
Girl*）。[法蘭西絲·麥朵曼與](../Page/法蘭西絲·麥朵曼.md "wikilink")[彼得·葛拉罕](../Page/彼得·葛拉罕.md "wikilink")（Peter
Gallagher）也將主演這部作品。

2012年1月，他获得[第69屆金球獎终身成就奖](../Page/第69屆金球獎.md "wikilink")。

### 個人生活

摩根·弗里曼與珍娜（Jeanette Adair Bradshaw）在1967年10月22日結婚，在1979年離婚。

後來在1984年6月16日，摩根·弗里曼與Myrna Colley-Lee成為夫妻。

目前弗里曼有2個兒子：阿方索（Alfonso Freeman）與Saifoulaye Freeman，也抚養了前妻的女兒迪娜（Deena
Freeman），後來又有第2個女兒摩卡娜（Morgana Freeman）。

摩根·弗里曼目前住在查理斯敦與紐約市。

摩根·弗里曼公開地反對慶祝[黑人歷史月](../Page/黑人歷史月.md "wikilink")（Black History
Month），並且不會參加任何相關的活動。他說「我不需要黑人歷史月，黑人歷史就是美國歷史。」摩根·弗里曼認為終止[種族主義的唯一途徑就是停止談論它](../Page/種族主義.md "wikilink")，他並注意到沒有「白種人歷史月」。

摩根·弗里曼曾在[麥克·華勒斯](../Page/麥克·華勒斯.md "wikilink")（Mike
Wallace）的60分鐘訪談中說「我將停止稱呼你為白種人，並且要求你停止稱我為黑人」\[11\]。

摩根·弗里曼於2006年10月28日在[傑克遜接受](../Page/傑克遜_\(密西西比州\).md "wikilink")[密西西比藝術與文學院的終身成就獎](../Page/密西西比藝術與文學院.md "wikilink")，表彰其演藝事業貢獻，摩根·弗里曼並也在2006年5月13日[三角洲州立大學舉行畢業典禮的期間](../Page/三角洲州立大學.md "wikilink")，接受該校藝術與文學的榮譽博士學位。

摩根·弗里曼於2008年8月3日凌晨在[密西西比州位於曼菲斯南方約一六零公里處的](../Page/密西西比州.md "wikilink")[塔拉哈奇市發生車禍](../Page/塔拉哈奇市.md "wikilink")，傷及手部，沒有生命危險。

2014年，[英國](../Page/英國.md "wikilink")[倫敦市授予費里曼](../Page/倫敦市.md "wikilink")「」（榮譽市民）頭銜\[12\]\[13\]。

2018年，據美國有線電視新聞網CNN調查報導，至少8名女性指控演技派巨星摩根費里曼性騷擾，此外還有8人曾親眼目睹他的不當行為。

## 演出的作品

  - 《[電力公司](../Page/電力公司.md "wikilink")》（*The Electric
    Company*，1971年）－電視連續劇（1971至1977年）
  - 《[黑獄風雲](../Page/黑獄風雲.md "wikilink")》（*Brubaker*，1980年）
  - 《[學店](../Page/學店_\(電影\).md "wikilink")》（*Teachers*，1984年）
  - 《[恕難從命](../Page/恕難從命.md "wikilink")》 （*Marie*，1985年）
  - 《*That Was Then... This Is Now*》 （1985年）
  - 《[惡街實況](../Page/惡街實況.md "wikilink")》（*Street Smart*，1987年）
      - 入圍[奧斯卡最佳男配角獎](../Page/奧斯卡最佳男配角獎.md "wikilink")
      - 入圍[金球獎最佳電影男配角](../Page/金球獎最佳電影男配角.md "wikilink")
  - 《[光榮戰役](../Page/光榮戰役.md "wikilink")》 （*Glory*，1989年）
  - 《[溫馨接送情](../Page/溫馨接送情.md "wikilink")》 （*Driving Miss Daisy*，1989年）
      - [金球獎最佳音樂及喜劇類電影男主角](../Page/金球獎最佳音樂及喜劇類電影男主角.md "wikilink")
      - 入圍[奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")
  - 《[鐵腕校長](../Page/鐵腕校長.md "wikilink")》 （*Lean on Me*，1989年）
  - 《[走夜路的男人](../Page/走夜路的男人.md "wikilink")》 （*The Bonfire of the
    Vanities*，1990年）
  - 《[俠盜王子羅賓-{}-漢](../Page/俠盜王子羅賓漢.md "wikilink")》 （*Robin Hood: Prince
    of Thieves*，1991年）
  - 《[小子要自強](../Page/小子要自強.md "wikilink")》 （*The Power of One*，1992年）
  - 《[不可饶恕](../Page/不可饶恕.md "wikilink")》 （*Unforgiven*，1992年）
  - 《[逮捕](../Page/逮捕_\(電影\).md "wikilink")》 （*Bopha\!*，1993年）－擔任導演
  - 《[刺激1995](../Page/刺激1995.md "wikilink")》（*Shawshank
    Redemption*，1994年）
      - 入圍[奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")
      - 入圍[金球獎最佳戲劇類電影男主角](../Page/金球獎最佳戲劇類電影男主角.md "wikilink")
      - 入圍[美國演員工會獎最佳男主角](../Page/美國演員工會獎最佳男主角.md "wikilink")
  - 《[肖申克的救赎](../Page/肖申克的救赎.md "wikilink")》（“The Shawshank
    Redemption”，1994年）
      - 获得奥斯卡金像奖提名
  - 《[火線追緝令](../Page/火線追緝令.md "wikilink")》 （*Seven*，1995年）
  - 《[危機總動員](../Page/危機總動員.md "wikilink")》 （*Outbreak*1995年）
  - 《[連鎖反應](../Page/連鎖反應_\(電影\).md "wikilink")》 （*Chain Reaction*，1996年）
  - 《[情婦法蘭德絲](../Page/情婦法蘭德絲.md "wikilink")》 （*Moll Flanders*，1996年）
  - 《[斷鎖怒潮](../Page/勇者無懼_\(電影\).md "wikilink")》 （1997年）
  - 《[桃色追捕令](../Page/桃色追捕令.md "wikilink")》/《[驚唇劫](../Page/驚唇劫.md "wikilink")》
    （*Kiss the Girls*，1997年）
  - 《[彗星撞地球](../Page/彗星撞地球.md "wikilink")》 （*Deep Impact*，1998年）
  - 《[驚濤毀滅者：大洪水](../Page/驚濤毀滅者：大洪水.md "wikilink")》 （*Hard Rain*，1998年）
  - 《[懸疑對戰](../Page/懸疑對戰.md "wikilink")》 （*Under Suspicion*，2000年）
  - 《[真愛來找碴](../Page/真愛來找碴.md "wikilink")》 （*Nurse Betty*，2000年）
  - 《[全面追緝令](../Page/全面追緝令.md "wikilink")》 （*Along Came a Spider*，2001年）
  - 《[恐懼的總和](../Page/恐懼的總和_\(電影\).md "wikilink")》 （*The Sum of All
    Fears*，2002年）
  - 《[案藏玄機](../Page/案藏玄機.md "wikilink")》 （*High Crimes*，2002年）
  - 《[王牌天神](../Page/王牌天神.md "wikilink")》 （*Bruce Almighty*，2003年）
  - 《[捕夢網](../Page/捕夢網_\(電影\).md "wikilink")》 （*Dreamcatcher*，2003年）
  - 《[登峰造擊](../Page/登峰造擊.md "wikilink")》 （2004年）
      - [奧斯卡最佳男配角獎](../Page/奧斯卡最佳男配角獎.md "wikilink")
      - [美國演員工會獎最佳男配角](../Page/美國演員工會獎最佳男配角.md "wikilink")
      - 入圍[金球獎最佳電影男配角](../Page/金球獎最佳電影男配角.md "wikilink")
  - 《[夏威夷生死鬥](../Page/夏威夷生死鬥.md "wikilink")》 （*The Big Bounce*，2004年）
  - 《[企鵝寶貝](../Page/企鵝寶貝.md "wikilink")》 （*March of the
    Penguins*，2005年）－擔任旁白主述
  - 《[美麗待續](../Page/美麗待續.md "wikilink")》 （*An Unfinished Life*，2005年）
  - 《[強戰世界](../Page/世界大戰_\(2005年電影\).md "wikilink")》 （2005年）－擔任旁白
  - 《[蝙蝠俠：開戰時刻](../Page/蝙蝠俠：開戰時刻.md "wikilink")》 （2005年）
  - 《[狼犬丹尼](../Page/狼犬丹尼.md "wikilink")》 （*Danny The Dog*，2005年）
  - 《*10 Items or Less*》（2006年）
  - 《[驚爆頭條內幕](../Page/驚爆頭條內幕.md "wikilink")》 （*Edison Force*，2006年）
  - 《[神鬼運轉](../Page/神鬼運轉.md "wikilink")》 （*Lucky Number Slevin*，2006年）
  - 《[愛情盛宴](../Page/愛情盛宴.md "wikilink")》 （*The Feast of Love*，2007年）
  - 《[冒牌天神2](../Page/冒牌天神2.md "wikilink")》 （*Evan Almighty*，2007年）
  - 《[天敵](../Page/天敵_\(電影\).md "wikilink")》（*The Contract*，2007年）
  - 《[失蹤人口](../Page/失蹤人口_\(電影\).md "wikilink")》（*Gone Baby Gone*，2007年）
  - 《[遗愿清单](../Page/遗愿清单.md "wikilink")》（The Bucket List，2008年）
  - 《[刺客聯盟](../Page/刺客聯盟.md "wikilink")》（*Wanted*，2008年）
  - 《[蝙蝠俠：黑暗骑士](../Page/蝙蝠俠：黑暗骑士.md "wikilink")》 （*The Dark
    Knight*，2008年）
  - 《[偷天密碼](../Page/偷天密碼.md "wikilink")》（*The Code*，2009年）
  - 《[打不倒的勇者](../Page/打不倒的勇者.md "wikilink")》（*Invictus*，2009年，[纳尔逊·曼德拉](../Page/纳尔逊·曼德拉.md "wikilink")）
      - 入圍[奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")
      - 入圍[金球獎最佳戲劇類電影男主角](../Page/金球獎最佳戲劇類電影男主角.md "wikilink")
      - 入圍[美國演員工會獎最佳男主角](../Page/美國演員工會獎最佳男主角.md "wikilink")
  - 《[超危險特工](../Page/超危險特工.md "wikilink")》（*Red*，2010年）
  - 《[王者之劍3D](../Page/王者之劍3D.md "wikilink")》（*Conan the
    Barbarian*，2011年）
  - 《》（*Dolphin Tale*，2011年）
  - 《[貝拉的魔法](../Page/貝拉的魔法.md "wikilink")》（2011年）
  - 《[蝙蝠俠：黑暗骑士崛起](../Page/蝙蝠俠：黑暗骑士崛起.md "wikilink")》 （*The Dark Knight
    Rises*，2012年）
  - 《[再造奇蹟](../Page/再造奇蹟.md "wikilink")》 （*The Magic of Belle
    Isle*，2012年）
  - 《[全面攻佔：倒數救援](../Page/全面攻佔：倒數救援.md "wikilink")》 （*Olympus Has
    Fallen*，2013年）
  - 《[出神入化](../Page/出神入化.md "wikilink")》 （*Now You See Me*，2013年）
  - 《[遺落戰境](../Page/遺落戰境.md "wikilink")》 （*Oblivion*，2013年）
  - 《[樂高玩電影](../Page/樂高玩電影.md "wikilink")》 （*LEGO*，2013年/ voice only)
  - 《[賭城大丈夫](../Page/賭城大丈夫.md "wikilink")》 （*Last Vegas*，2013年)
  - 《[摩根費里曼之穿越蟲洞](../Page/摩根費里曼之穿越蟲洞.md "wikilink")》 （*Through the
    wormhole with Morgan Freeman*，2012年)
  - 《[全面進化](../Page/全面進化.md "wikilink")》 （*Transcedence*，2014年)
  - 《[-{zh-hk:LUCY：超能煞姬;zh-tw:露西;zh-cn:超体;}-](../Page/露西_\(电影\).md "wikilink")》
    （*Lucy*，2014年）
  - 《[溫特的故事：泳不放棄2](../Page/溫特的故事：泳不放棄2.md "wikilink")》 （*Dolphin Tale
    2*，2014年）
  - 《[魯斯與亞歷克斯](../Page/魯斯與亞歷克斯.md "wikilink")》 （*5 Flights
    Up*，2014年）－兼執行製片人
  - 《[第七軍團：最後戰役](../Page/第七軍團：最後戰役.md "wikilink")》 （*Last
    Knights*，2015年）
  - 《[熊麻吉2](../Page/熊麻吉2.md "wikilink")》 （*Ted 2*，2015年）
  - 《[即刻殺機](../Page/即刻殺機.md "wikilink")》 （*Momentum*，2015年）
  - 《[全面攻佔2：倫敦救援](../Page/全面攻佔2：倫敦救援.md "wikilink")》 （*London Has
    Fallen*，2016年）
  - 《[出神入化2](../Page/出神入化2.md "wikilink")》 （*Now You See Me 2*，2016年）
  - 《[賓漢](../Page/賓漢_\(2016年電影\).md "wikilink")》 （*Ben-Hur*，2016年）
  - 《[瀟灑搶一回](../Page/瀟灑搶一回.md "wikilink")》 （*Going in Style*，2017年）
  - 《[對頭冤家](../Page/對頭冤家.md "wikilink")》 （*Just Getting Started*，2018年）

## 獎項

  - 1987年：以《[惡街實況](../Page/惡街實況.md "wikilink")》（*Street
    Smart*）入圍[奧斯卡最佳男配角獎](../Page/奧斯卡最佳男配角獎.md "wikilink")
  - 1989年：以《[溫馨接送情](../Page/溫馨接送情.md "wikilink")》（*Driving Miss
    Daisy*）入圍[奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")
  - 1989年：以《[溫馨接送情](../Page/溫馨接送情.md "wikilink")》獲得[金球獎最佳男主角獎-喜劇與音樂劇類](../Page/金球獎最佳男主角獎-喜劇與音樂劇類.md "wikilink")
  - 1994年：以《[刺激1995](../Page/刺激1995.md "wikilink")》入圍奧斯卡最佳男主角獎
  - 2004年：以《[登峰造擊](../Page/登峰造擊.md "wikilink")》獲得奧斯卡最佳男配角獎
  - 2011年：获得[美国电影学会终身成就奖](../Page/美国电影学会终身成就奖.md "wikilink")
  - 2012年：获得[第69屆金球獎终身成就奖](../Page/第69屆金球獎.md "wikilink")
  - 2018年：獲得[演員工會獎](../Page/演員工會獎.md "wikilink") 終身成就獎

## 参考文献

## 外部链接

  - [摩根·弗里曼](../Page/:imdbname:0000151.md "wikilink")
    [網際網路電影資料庫中的相關信息](../Page/網際網路電影資料庫.md "wikilink")

  - [ClickStar](http://www.cstar.com/) 摩根·弗里曼與洛莉·麥克里創辦的公司

  - [摩根·弗里曼](http://www.allmovie.com/cg/avg.dll?p=avg&sql=2:90514) [All
    Movie Guide中的資料](../Page/All_Movie_Guide.md "wikilink")

  - [Revelations Entertainment](http://www.revelationsent.com/)
    摩根·弗里曼創辦的製片公司

  - [摩根·弗里曼演員生涯的詳細介紹](https://web.archive.org/web/20060217195229/http://www.tiscali.co.uk/entertainment/film/biographies/morgan_freeman_biog.html)

{{-}}

[Category:美國導演](../Category/美國導演.md "wikilink")
[Category:非洲裔美國電影演員](../Category/非洲裔美國電影演員.md "wikilink")
[Category:非洲裔美國電視演員](../Category/非洲裔美國電視演員.md "wikilink")
[Category:奧斯卡最佳男配角獎獲獎演員](../Category/奧斯卡最佳男配角獎獲獎演員.md "wikilink")
[Category:金球奖最佳音乐或喜剧男主角奖得主](../Category/金球奖最佳音乐或喜剧男主角奖得主.md "wikilink")
[Category:金球獎終身成就獎獲得者](../Category/金球獎終身成就獎獲得者.md "wikilink")
[Category:田納西州人](../Category/田納西州人.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:倫敦榮譽市民](../Category/倫敦榮譽市民.md "wikilink")
[Category:印第安纳州男演员](../Category/印第安纳州男演员.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美国不可知论者](../Category/美国不可知论者.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國空軍士兵](../Category/美國空軍士兵.md "wikilink")
[Category:美國飛行員](../Category/美國飛行員.md "wikilink")
[Category:美國舞台男演員](../Category/美國舞台男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:独立精神奖获得者](../Category/独立精神奖获得者.md "wikilink")
[Category:肯尼迪中心荣誉奖得主](../Category/肯尼迪中心荣誉奖得主.md "wikilink")

1.  <http://www.filmreference.com/film/24/Morgan-Freeman.html>

2.  <http://www.hellomagazine.com/profiles/morganfreeman/>

3.  Father's name was stated as "Morgan" on *[Inside the Actors
    Studio](../Page/Inside_the_Actors_Studio.md "wikilink")*

4.  Stated in interview on *[Inside the Actors
    Studio](../Page/Inside_the_Actors_Studio.md "wikilink")*

5.  [Biography for Morgan
    Freeman](http://www.imdb.com/name/nm0000151/bio), IMDB.com database,
    retrieved 16 Mar 2007

6.  [Morgan Freeman
    Biography](http://www.tiscali.co.uk/entertainment/film/biographies/morgan_freeman_biog/3)
    , tiscali.co.uk Film & TV, retrieved 16 Mar 2007

7.
8.
9.
10.
11. <http://msnbc.msn.com/id/10482634/>

12.

13. <http://www.dailymail.co.uk/wires/ap/article-2831704/City-London-makes-Morgan-Freeman-freeman.html>