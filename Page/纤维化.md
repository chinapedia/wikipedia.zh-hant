**纤维化**(Fibrosis)是指在一个器官或组织为修复或反应过程而过度地形成纤维[结缔组织的过程](../Page/结缔组织.md "wikilink")。

## 纤维化相关疾病

  - （肺，胰腺）[囊肿性纤维化或](../Page/囊肿性纤维化.md "wikilink")[囊性纤维化](../Page/囊性纤维化.md "wikilink")(Cystic
    fibrosis)
  - [心内膜纤维化](../Page/心内膜纤维化.md "wikilink")(Endomyocardial fibrosis)
  - [肝硬化](../Page/肝硬化.md "wikilink")(Cirrhosis)
  - [特发性肺纤维化](../Page/特发性肺纤维化.md "wikilink") (Idiopathic pulmonary
    fibrosis)
  - [間質性肺病](../Page/間質性肺病.md "wikilink")(Diffuse parenchymal lung
    disease)
  - [纵隔纤维化](../Page/纵隔纤维化.md "wikilink")(Mediastinal fibrosis)
  - [腹膜纤维化](../Page/腹膜纤维化.md "wikilink")(Retroperitoneal fibrosis)
  - [尘肺病](../Page/尘肺病.md "wikilink")(Pneumoconiosis)
  - [肿瘤纤维化](../Page/肿瘤纤维化.md "wikilink")(Neoplastic fibrosis)
  - 脾脏纤维化（[镰状细胞贫血的并发症](../Page/鐮刀型紅血球疾病.md "wikilink")）(Sickle-cell
    anemia)

[Category:症狀](../Category/症狀.md "wikilink")