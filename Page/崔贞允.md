**崔贞允**（，），[韩国](../Page/韩国.md "wikilink")[女演员](../Page/女演员.md "wikilink")。因高中时拍摄汉城牛奶广告而走上演艺道路，常以清纯甜美的扮相出现于螢幕。

## 演出作品

### 电视剧

  - 1996年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[浪漫風暴](../Page/浪漫風暴.md "wikilink")》
  - 1998年：SBS《[愛上Mr.Q](../Page/愛上Mr.Q.md "wikilink")》
  - 2000年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[新贵公子](../Page/新贵公子.md "wikilink")》
  - 2000年：[KBS](../Page/韓國放送公社.md "wikilink")《[雷聲](../Page/雷聲_\(電視劇\).md "wikilink")》
  - 2001年：KBS《[愛情奇蹟](../Page/愛情奇蹟.md "wikilink")》飾 申熙
  - 2001年：KBS《[东方剧场](../Page/东方剧场.md "wikilink")》
  - 2002年：SBS《[認真生活](../Page/認真生活.md "wikilink")》
  - 2003年：MBC《[屋塔房小猫](../Page/屋塔房小猫.md "wikilink")》飾 羅惠蓮
  - 2005年：MBC《[泰陵選手村](../Page/泰陵選手村.md "wikilink")》飾 方秀娥
  - 2006年：MBC《[愛誰都不能阻止](../Page/愛誰都不能阻止.md "wikilink")》飾 徐恩珠
  - 2007年：[tvN](../Page/tvN.md "wikilink")《[浪漫獵手](../Page/浪漫獵手.md "wikilink")》飾
    洪英珠
  - 2007年：SBS《[不良情侶](../Page/不良情侶.md "wikilink")》飾 韓英
  - 2007年：SBS《[那女人好可怕](../Page/殘酷的愛.md "wikilink")》飾 白銀愛
  - 2009年：SBS《[你笑了](../Page/你笑了.md "wikilink")》飾 徐真靜
  - 2011年：tvN《[Manny](../Page/型男保姆到我家.md "wikilink")》飾 徐道瑛
  - 2011年：KBS《[烏鵲橋兄弟](../Page/烏鵲橋兄弟.md "wikilink")》飾 車琇瑩
  - 2012年：MBC《[天使的選擇](../Page/天使的選擇.md "wikilink")》飾 崔恩雪
  - 2012年：KBS《[加油，金先生！](../Page/加油，金先生！.md "wikilink")》飾 千志瑩
  - 2013年：MBC《[Drama
    Festival](../Page/Drama_Festival.md "wikilink")－[少年，再次遇見少女](../Page/Drama_Festival#第一季.md "wikilink")》飾
    申娜
  - 2014年：[JTBC](../Page/JTBC.md "wikilink")《[我們能相愛嗎](../Page/我們能相愛嗎.md "wikilink")》飾
    權智賢
  - 2014年：SBS《[清潭洞醜聞](../Page/清潭洞醜聞.md "wikilink")》飾演 殷賢秀

### 电影

  - 1997年：《[父親](../Page/父親_\(韓國電影\).md "wikilink")》
  - 2000年：《[凶咒](../Page/凶咒.md "wikilink")》
  - 2000年：《[錯愛雙魚座](../Page/錯愛雙魚座.md "wikilink")》(客串)
  - 2002年：《[鬼鈴](../Page/鬼鈴.md "wikilink")》
  - 2003年：《[輪迴](../Page/輪迴_\(韓國電影\).md "wikilink")》
  - 2004年：《》
  - 2006年：《[廣播明星](../Page/廣播明星.md "wikilink")》
  - 2007年：《[原聲追兇](../Page/原聲追兇.md "wikilink")》（客串）
  - 2012年：《[我是爸爸](../Page/我是爸爸.md "wikilink")》

### 綜藝節目

  - 2015年：SBS《[握拳少林寺](../Page/握拳少林寺.md "wikilink")》女子篇 \[1\]

## 獲獎紀錄

  - 2011年 [KBS演技大賞](../Page/KBS演技大賞.md "wikilink") - 最佳情侶獎 《烏鵲巢兄弟》
  - 2014年 [SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－長篇劇 女子優秀演技賞《清潭洞醜聞》

## 外部連結

  -
  -
## 参考文献

[Category:韓國電影演員](../Category/韓國電影演員.md "wikilink")
[Category:韩国電視演员](../Category/韩国電視演员.md "wikilink")
[Category:韓國中央大學校友](../Category/韓國中央大學校友.md "wikilink")
[Jeong](../Category/崔姓.md "wikilink")

1.  [《握拳少林寺》名單確定
    尚餘一人協調中](http://www.allthatstar.com/tw/view.php?action=5205&postID=124406)全星網