**艾尔湖**（Lake Eyre，又稱**艾耳湖**, 當地語言： Kati Thanda），位于[-{zh-hans:澳大利亚;
zh-hant:澳洲;}-的中部地区](../Page/澳大利亚.md "wikilink")，是一个[时令湖](../Page/时令湖.md "wikilink")。在罕有情況注滿時，是[澳洲最大的](../Page/澳洲.md "wikilink")[湖泊](../Page/湖泊.md "wikilink")。湖的最低點位於海平線下15公尺(50英呎)，是[艾耳湖盆地的焦點](../Page/艾耳湖盆地.md "wikilink")。

1840年，英国探险家艾尔(E.J.Eyre)首先发现该湖而得名。分南北二湖。

艾爾湖位於澳洲中部的沙漠，澳洲大陸最低方，湖面比海平面低12米。艾爾湖盆地是湖床附近的大型內流湖系統，最低的部分是因季節增加和減少水體的[淺鹽湖](../Page/淺鹽湖.md "wikilink")。附近干旱地区年平均降雨量不到120毫米，年蒸发量达2,500毫米。在乾旱季節時，当河流从山地向西流时，一路上因蒸发和渗漏损失很大，往往在半路上就消失了，艾爾湖湖岸的小湖盛著剩下的少量水份，湖水面经常干涸，湖面缩小成盐池；在雨季時，河流由東北流進湖泊，季候風帶來的雨量決定河水會否抵達艾耳湖及其深度，附近地區的雨水也會令湖泊中小型泛濫，每3年平均泛濫1.5公尺，每10年泛濫4公尺一次，在一個世紀會完全注滿4次。湖水會在下年夏天末的中小型泛濫後被蒸發。艾爾湖盆地没有出海口，是世界最大的内流盆地之一。主要连接河道有[庫珀溪](../Page/庫珀溪.md "wikilink")、[沃伯頓河等](../Page/沃伯頓河.md "wikilink")。

艾爾湖的湖水主要来自河水及雨水。它的面积变化很大，降雨量较大时，面积可达8200平方公里。降水较少时便出现干涸。按照其平均面积它是世界第19大湖，如果按其最大面积来算，它是[大洋洲最大的湖泊](../Page/大洋洲.md "wikilink")。

艾爾湖帆船會是一個特別的團體，會員會在該湖小型泛濫時航行，包括1997年、2000年、2001年和2004年的航程。

## 參見

  - [艾爾湖國家公園](../Page/艾爾湖國家公園.md "wikilink")
  - [雨成湖](../Page/雨成湖.md "wikilink")
  - [湖泊列表](../Page/湖泊列表.md "wikilink")
  - [世界大型湖泊列表](../Page/世界大型湖泊列表.md "wikilink")

## 外部連結

  - [美國太空總署有關艾耳湖網頁](../Page/美國太空總署.md "wikilink")[1](http://earthobservatory.nasa.gov/Newsroom/NewImages/images.php3?img_id=10841),
    [2](http://earthobservatory.nasa.gov/Newsroom/NewImages/images.php3?img_id=11763)
  - [艾耳湖帆船會網站](http://www.LakeEyreYC.com)
  - [谷歌地圖](http://maps.google.com/?ll=-28.173718,137.427979&spn=3.389282,3.856201&t=k)
  - [艾耳湖謎團](https://web.archive.org/web/20060410065635/http://www.aussiepelicans.com/lakeeyre.html)
  - [艾耳湖泛濫 - Vincent Kotwicki
    博士網站](https://web.archive.org/web/20060821181629/http://k26.com/eyre/)

[Category:內流湖](../Category/內流湖.md "wikilink")
[Category:澳大利亚湖泊](../Category/澳大利亚湖泊.md "wikilink")
[Category:南澳大利亞州地理](../Category/南澳大利亞州地理.md "wikilink")