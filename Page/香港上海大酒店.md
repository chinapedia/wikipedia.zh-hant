**香港上海大酒店有限公司**（）是一家在[香港交易所上市的](../Page/香港交易所.md "wikilink")[酒店公司](../Page/酒店.md "wikilink")。主要業務是酒店投資管理服務、物業投資及發展、經營餐廳、會所管理等。

## 历史

於1866年3月2日創辦，前身是**[香港大酒店](../Page/香港大酒店.md "wikilink")**，1890年代開始由[猶太人](../Page/猶太人.md "wikilink")[嘉道理家族持有](../Page/嘉道理家族.md "wikilink")，1922年併購**上海大酒店**並於翌年使用現名。旗下的半島酒店品牌，在世界各地管理多家豪華酒店，包括香港[尖沙咀](../Page/尖沙咀.md "wikilink")[半島酒店](../Page/半島酒店.md "wikilink")。

公司在[香港註冊](../Page/香港.md "wikilink")，[董事局主席為](../Page/董事局主席.md "wikilink")[米高·嘉道理](../Page/米高·嘉道理.md "wikilink")。持股53.82％

2009年1月20日香港上海大酒店向[卡塔爾投資局購入購入](../Page/卡塔爾投資局.md "wikilink")[法國](../Page/法國.md "wikilink")[巴黎Majestic酒店項目的](../Page/巴黎.md "wikilink")20%股權，總代價1.5億[歐元](../Page/歐元.md "wikilink")，相當於約15.36億[港元](../Page/港元.md "wikilink")，並將該酒店重新發展為巴黎半島酒店\[1\]，總代價包括以1億歐元（約10.24億港元）買入20%股權，及5,000萬歐元（約5.12億港元）翻新成本，並擁有30年管理權。

2013年4月18日，大酒店與[祐瑪戰略控股有限公司就有關緬甸仰光的酒店發展建議項目訂立無約束力的框架協議](../Page/祐瑪戰略控股有限公司.md "wikilink")。據協議，大酒店已同意認購將與祐瑪及其指定聯屬公司成立的合營企業佔70%的大部分權益，以共同重新發展該樓宇為半島酒店。

## 旗下酒店

  - [香港](../Page/香港.md "wikilink")[半島酒店](../Page/半島酒店.md "wikilink")100%
  - [紐約半島酒店](../Page/紐約.md "wikilink")：100%
  - [芝加哥半島酒店](../Page/芝加哥.md "wikilink")：100%
  - [比華利山半島酒店](../Page/比華利山.md "wikilink")：20%
  - [曼谷半島酒店](../Page/曼谷.md "wikilink")：75%
  - [北京王府半岛酒店](../Page/北京.md "wikilink")（原王府饭店）：76.6%
  - [馬尼拉半島酒店](../Page/馬尼拉.md "wikilink")：77.4%
  - [東京半島酒店](../Page/東京.md "wikilink")：100%
  - [上海半島酒店](../Page/上海.md "wikilink")：50%
  - [巴黎半島酒店](../Page/巴黎.md "wikilink")：20%

### 建構中

  - [仰光半島酒店](../Page/仰光.md "wikilink")（前緬甸鐵路公司總部）：70%
  - [倫敦半島酒店](../Page/倫敦.md "wikilink")（位於Belgravia地區名為1-5 Grosvenor
    Place的地塊）：50%
  - [伊斯坦堡半島酒店](../Page/伊斯坦堡.md "wikilink")：50%

## 其他業務

  - 淺水灣[影灣園總樓面面積](../Page/影灣園.md "wikilink")：住宅995,546平方呎，商場：62,909平方呎
  - [凌霄閣](../Page/凌霄閣.md "wikilink")：總樓面面積：116,768平方呎
  - [聖約翰大廈總樓面面積](../Page/聖約翰大廈.md "wikilink")：71,400平方呎
  - 胡志明市The Landmark：70%：總樓面面積：寫字樓106,153平方呎，住宅69,750平方呎
  - 半島辦公大樓：總樓面面積：75,082平方呎
  - 鶉園高爾夫球及鄉村俱樂部
  - 泰國鄉村俱樂部：75%
  - 半島商品有限公司
  - 大班洗衣
  - [山頂纜車](../Page/山頂纜車.md "wikilink")
  - [太古坊](../Page/太古坊.md "wikilink")[Butterfield's私人會所](../Page/Butterfield's.md "wikilink")（地址﹕香港英皇道979號[太古坊多盛大廈](../Page/太古坊.md "wikilink")2至4樓）
  - [香港會所](../Page/香港會所.md "wikilink")
  - [半島商品](../Page/半島商品.md "wikilink")
  - [法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[克勒貝爾大街](../Page/克勒貝爾大街.md "wikilink")21號：總樓面面積：43,163平方呎
  - [英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")1-5
    Grosvenor Place：總樓面面積：246,192平方呎 擁有權：100%

## 参考文献

## 外部連結

  - [香港上海大酒店有限公司（英）](http://www.hshgroup.com/)
  - [香港上海大酒店有限公司（繁中）](http://www.hshgroup.com/zh-hk)
  - [香港上海大酒店有限公司
    基本資料（中文版）雅虎財經](https://web.archive.org/web/20060504210253/http://hk.biz.yahoo.com/p/hk/profile/0045.hk.html)
  - [HKEX.COM.HK
    香港上海大酒店有限公司年報 2005年大事記](https://web.archive.org/web/20070930014719/http://www.hkex.com.hk/listedco/listconews/sehk/20060413/00045/CWF104.pdf)

[category:香港酒店連鎖集團](../Page/category:香港酒店連鎖集團.md "wikilink")

[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市酒店公司](../Category/香港上市酒店公司.md "wikilink")
[Category:香港上海大酒店](../Category/香港上海大酒店.md "wikilink")
[Category:前恒生指數成份股](../Category/前恒生指數成份股.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:嘉道理家族](../Category/嘉道理家族.md "wikilink")
[Category:1866年成立的公司](../Category/1866年成立的公司.md "wikilink")

1.  公司2011年年報 第49頁