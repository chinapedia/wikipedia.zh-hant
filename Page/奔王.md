**奔王**（）是[日本將棋的棋子之一](../Page/日本將棋.md "wikilink")。有在[中將棋](../Page/中將棋.md "wikilink")、[大將棋](../Page/大將棋.md "wikilink")、[大大將棋](../Page/大大將棋.md "wikilink")、[天竺大將棋](../Page/天竺大將棋.md "wikilink")、[摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")、[泰將棋和](../Page/泰將棋.md "wikilink")[大局將棋出現](../Page/大局將棋.md "wikilink")。

## 奔王在中將棋、大將棋、天竺大將棋、大大將棋、泰將棋

在中將棋簡稱為**奔**。中將棋、大將棋[鳳凰](../Page/鳳凰_\(日本將棋\).md "wikilink")（）與大大將棋、泰將棋[馬麟](../Page/馬麟_\(日本將棋\).md "wikilink")（）升級而成。不得升級。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>奔王</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>＼</p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p>／</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>＼</p></td>
<td><p>│</p></td>
<td><p>／</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>─</p></td>
<td><p>─</p></td>
<td><p><strong>璶</strong></p></td>
<td><p>─</p></td>
<td><p>─</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>／</p></td>
<td><p>│</p></td>
<td><p>＼</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>／</p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p>＼</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 奔王在天竺大將棋

[鳳凰的升級棋](../Page/鳳凰_\(日本將棋\).md "wikilink")。可以升級成為[奔鷲](../Page/奔鷲.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>奔王</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>＼</p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p>／</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>＼</p></td>
<td><p>│</p></td>
<td><p>／</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>─</p></td>
<td><p>─</p></td>
<td><p><strong>璶</strong></p></td>
<td><p>─</p></td>
<td><p>─</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>／</p></td>
<td><p>│</p></td>
<td><p>＼</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>／</p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p>＼</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循八方自由行走。不得越過其他棋子。</p></td>
<td><p>奔鷲</p></td>
</tr>
</tbody>
</table>

## 奔王在大局將棋

[奔獏](../Page/奔獏.md "wikilink")（）、[奔鬼](../Page/奔鬼.md "wikilink")（）、馬麟的升級棋。可以升級成為[大將](../Page/大將_\(日本將棋\).md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>奔王</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>＼</p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p>／</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>＼</p></td>
<td><p>│</p></td>
<td><p>／</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>─</p></td>
<td><p>─</p></td>
<td><p><strong>璶</strong></p></td>
<td><p>─</p></td>
<td><p>─</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td><p>／</p></td>
<td><p>│</p></td>
<td><p>＼</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p>／</p></td>
<td><p> </p></td>
<td><p>│</p></td>
<td><p> </p></td>
<td><p>＼</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循八方自由行走。不得越過其他棋子。</p></td>
<td><p>大將</p></td>
</tr>
</tbody>
</table>

## 參見

  - [日本將棋變體](../Page/日本將棋變體.md "wikilink")
  - [中將棋](../Page/中將棋.md "wikilink")
  - [大將棋](../Page/大將棋.md "wikilink")
  - [大大將棋](../Page/大大將棋.md "wikilink")
  - [天竺大將棋](../Page/天竺大將棋.md "wikilink")
  - [摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")
  - [泰將棋](../Page/泰將棋.md "wikilink")
  - [大局將棋](../Page/大局將棋.md "wikilink")
  - [國際象棋](../Page/國際象棋.md "wikilink") - 當中的后與奔王步法相同

[Category:日本將棋棋子](../Category/日本將棋棋子.md "wikilink")
[Category:中將棋棋子](../Category/中將棋棋子.md "wikilink")
[Category:大將棋棋子](../Category/大將棋棋子.md "wikilink")
[Category:大大將棋棋子](../Category/大大將棋棋子.md "wikilink")
[Category:天竺大將棋棋子](../Category/天竺大將棋棋子.md "wikilink")
[Category:摩訶大大將棋棋子](../Category/摩訶大大將棋棋子.md "wikilink")
[Category:泰將棋棋子](../Category/泰將棋棋子.md "wikilink")
[Category:大局將棋棋子](../Category/大局將棋棋子.md "wikilink")