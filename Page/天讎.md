《**天讎——一個中國青年的自述**》，英文著作[文革紀實長篇小說](../Page/文革.md "wikilink")，作者[凌耿以](../Page/凌耿.md "wikilink")[紅衛兵](../Page/紅衛兵.md "wikilink")（「廈八中」頭目）第一人稱紀錄，詳實寫出[文革初期](../Page/文革.md "wikilink")（1966年—1968年）經過，包括：[福建](../Page/福建.md "wikilink")[紅衛兵鬥爭省黨委](../Page/紅衛兵.md "wikilink")[韓先楚](../Page/韓先楚.md "wikilink")、全中國9次紅衛兵「大串聯」、[波希米亞式的浪漫到](../Page/波希米亞.md "wikilink")[北京謁見](../Page/北京.md "wikilink")[毛主席](../Page/毛主席.md "wikilink")，[天安門廣場百萬人頭鑽動](../Page/天安門廣場.md "wikilink")、一片紅（[毛語錄](../Page/毛語錄.md "wikilink")）盛大熱烈場面、批鬥[王光美](../Page/王光美.md "wikilink")（[清華大學紅衛兵主鬥](../Page/清華大學.md "wikilink")，廈門第八中學「紅衛兵829公社」至北京助鬥）、數百名不同派系紅衛兵持械武鬥廝殺殘忍場面、最後18歲女友「梅梅」擔任戰場護士卻不幸中彈身亡，導致18歲的凌耿萬念俱灰跳海游泳至[金門大擔島](../Page/金門.md "wikilink")，投奔[台灣成為當年](../Page/台灣.md "wikilink")[國民黨樣板宣傳人物](../Page/國民黨.md "wikilink")……

[美國](../Page/美國.md "wikilink")《[紐約時報](../Page/紐約時報.md "wikilink")》（[New
York
Times](../Page/New_York_Times.md "wikilink")）、中華民國前總統[蔣經國當年皆推薦此部小說](../Page/蔣經國.md "wikilink")。最初發表時，為單篇英文文章"The
Making of a Red
Guard"，由《[紐約時報](../Page/紐約時報.md "wikilink")》於1970年發表，隨後於1972年發表"The
Revenge of Heaven：Journal of a young
Chinese."與中文譯本是[香港](../Page/香港.md "wikilink")[新境傳播出版發行的天讎](../Page/新境傳播出版.md "wikilink")，譯者為[台灣大學外文系的](../Page/台灣大學.md "wikilink")[劉昆生和](../Page/劉昆生.md "wikilink")[丁廣馨](../Page/丁廣馨.md "wikilink")。\[1\]
2016年2月，台灣大塊文化出版社邀請原作者[郭坤仁重新出版中文原著版本](../Page/郭坤仁.md "wikilink")，並加1970年後的紀實與當時照片，命名為《[從前從前有個紅衛兵](../Page/從前從前有個紅衛兵.md "wikilink")》。\[2\]

## 查禁

  - 因為小說裡記敘紅衛兵串聯時食宿極佳，與當年台灣政府宣傳之粗食破陋不符合，凌耿也拒絕再版修改，曾在台灣被禁止發行。\[3\]

## 讚譽

  -
  - 小說一開始，是[文革爆發初的](../Page/文革.md "wikilink")6月，作者在[廈門家鄉](../Page/廈門.md "wikilink")，剛起床聽著[鼓浪嶼法國](../Page/鼓浪嶼.md "wikilink")[殖民地時期的鋼琴樂音與海](../Page/殖民地.md "wikilink")，想著[蘇聯藝術電影](../Page/蘇聯.md "wikilink")《追紅汽球的小孩》，相當詩情畫意。

  - 台灣[中央研究院文哲所研究員](../Page/中央研究院.md "wikilink")[李奭學於](../Page/李奭學.md "wikilink")2006年5月29日於台灣[中國時報標題](../Page/中國時報.md "wikilink")「中國文學不能承受的重
    文革小說40年」裡亦讚：「……扣人心弦的程度絕不輸[金庸](../Page/金庸.md "wikilink")」云云。

## 參考

  - [凌耿](../Page/凌耿.md "wikilink")，天仇——一个中国青年的自述，香港新境传播公司；从前从前有个红卫兵，台湾大块文化出版社，2016。ISBN
    978-986-213-675-1
  - 電影[皇天后土](../Page/皇天后土.md "wikilink") 台灣
    [中央電影公司](../Page/中央電影公司.md "wikilink")
    1980年代拍攝
    主角：[秦祥林](../Page/秦祥林.md "wikilink")、[胡慧中](../Page/胡慧中.md "wikilink")（現[江蘇人代](../Page/江蘇.md "wikilink")）
  - 小說《[反修樓](../Page/反修樓.md "wikilink")》

## 外部連結

  - [凌耿【天讎】](http://www.haodoo.net/?M=book&P=559)--《天讎－－一個中國青年的自述》
  - [凌耿《從前從前有個紅衛兵》 | THE STORY OF A RED GUARD
    ISBN：978-986-213-675-1](http://www.locuspublishing.com/Produkt.aspx?bokId=1111MA112)

[Category:傳記](../Category/傳記.md "wikilink")
[Category:傷痕文學](../Category/傷痕文學.md "wikilink")
[Category:文革相关书籍](../Category/文革相关书籍.md "wikilink")
[Category:中國小說](../Category/中國小說.md "wikilink")
[T](../Category/中文长篇小说.md "wikilink")
[Category:台灣禁書](../Category/台灣禁書.md "wikilink")

1.  [1](http://www.storm.mg/lifestyle/83384)
2.  [大塊文化的影片《從前從前有個紅衛兵》](https://www.facebook.com/locuspublish/videos/10153193470631895/)
3.  [翻譯偵探事務所：原文也是中文，譯文也是中文:天讎](http://tysharon.blogspot.tw/2014/03/1972-19661968-1972-1970-new-york-times.html)