**美国自然史博物馆**（American Museum of Natural
History）位于[美国](../Page/美国.md "wikilink")[纽约](../Page/纽约.md "wikilink")[曼哈顿](../Page/曼哈顿.md "wikilink")[上西城](../Page/上西城.md "wikilink")[第八大道及第](../Page/第八大道.md "wikilink")79街交界，是专注于[天文学](../Page/天文学.md "wikilink")、[地球科学](../Page/地球科学.md "wikilink")、[人类学](../Page/人类学.md "wikilink")、[古生物学](../Page/古生物学.md "wikilink")、[生物学等的博物馆](../Page/生物学.md "wikilink")。建馆于1869年，现有馆员超过1200人，每年承办超过100场特别展览。该馆的主要收藏项目包括于各大洲哺乳动物的收集，以及人类学的馆藏。其人类起源馆（Hall
of Human
Origin）是全美唯一此领域的专项展览，展示了人类进化过程中的各个阶段。除了展览各种展品外，这家博物馆还有各种培养中学生探索科学的兴趣的教育活动，还会给教师们提供培训。\[1\]美国自然史博物馆是世界上浏览人数最多的博物馆之一，2010年时有超过500万人参观过这个博物馆。\[2\]

## 规模与展品

这家博物馆由建造在25栋建筑内的46个展厅组成，展品超过了3600万件。这些展品囊括了[天文学](../Page/天文学.md "wikilink")、[地球科学](../Page/地球科学.md "wikilink")、[人类学](../Page/人类学.md "wikilink")、[古生物学](../Page/古生物学.md "wikilink")、[生物学等多个范围](../Page/生物学.md "wikilink")，其中生物学展品包括12米长，由真正的[化石制作的](../Page/化石.md "wikilink")[重龙骨架](../Page/重龙.md "wikilink")。1925年在美国南部海岸捕获的，重大150吨，高26米的[蓝鲸的模型](../Page/蓝鲸.md "wikilink")。而天文学方面则有31吨重的世界最大[陨石](../Page/陨石.md "wikilink")，地球科学领域展品包括563克拉的[蓝宝石](../Page/蓝宝石.md "wikilink")“印度之星”等等。除了这些展品外，博物馆内还包括[海登天文台](../Page/海登天文台.md "wikilink")、一座播放各种科学教育节目的[IMAX影院以及一个拥有](../Page/IMAX.md "wikilink")48万本藏书以及各种相片、手稿的自然科学图书馆，博物馆还负责编撰一本月刊。博物馆的这些藏品来源众多，有些是私人捐赠，有些来自于博物馆自己组织的科学考察队，还有一些来自于关闭的小型博物馆。虽然藏品众多，但实际上观众能见到的只有大约2%，剩下来的藏品大多用作科研。\[3\]
[Willamette_Meteorite_AMNH.jpg](https://zh.wikipedia.org/wiki/File:Willamette_Meteorite_AMNH.jpg "fig:Willamette_Meteorite_AMNH.jpg")
[Barosaurus_mount_1.jpg](https://zh.wikipedia.org/wiki/File:Barosaurus_mount_1.jpg "fig:Barosaurus_mount_1.jpg")
[Amnh_fg08.jpg](https://zh.wikipedia.org/wiki/File:Amnh_fg08.jpg "fig:Amnh_fg08.jpg")模型，分別是科普研究的正模標本(四足)，以及布郎研究的近模標本(二足)\]\]

## 学位

美国自然史博物馆2006年获得纽约州教育理事会授权，下设里查德·吉尔德研究生院（Richard Gilder Graduate
School），开办了[比较生物学专业](../Page/比较生物学.md "wikilink")。这是博物馆目前唯一提供的学术项目。2009年11月，州政府核准馆方颁发[博士学位](../Page/博士学位.md "wikilink")。美国自然史博物馆因而正式成为美国第一个、也是唯一授与博士学位的美国博物馆。\[4\]\[5\]

## 相關影視作品

  - [博物館驚魂夜](../Page/博物館驚魂夜.md "wikilink")
  - [挪威自然史博物館](../Page/挪威自然史博物館.md "wikilink")

## 参考文献

## 外部链接

[美国自然史博物馆网站](http://www.amnh.org/)

{{-}}

[Category:人類學博物館](../Category/人類學博物館.md "wikilink")
[Category:1869年紐約州建立](../Category/1869年紐約州建立.md "wikilink")
[Category:美國自然史博物館](../Category/美國自然史博物館.md "wikilink")
[Category:曼哈顿博物馆](../Category/曼哈顿博物馆.md "wikilink")
[Category:天象儀](../Category/天象儀.md "wikilink")
[Category:美国恐龙博物馆](../Category/美国恐龙博物馆.md "wikilink")
[Category:贝壳博物馆](../Category/贝壳博物馆.md "wikilink")

1.  [通讯：科学教育
    美国自然历史博物馆的核心使命](http://news.xinhuanet.com/tech/2011-11/24/c_122330622.htm)
2.  [World's Most-Visited
    Museums](http://www.travelandleisure.com/articles/worlds-most-visited-museums/8)
3.  [　美国自然历史博物馆](http://www.yn.chinanews.com/pub/special/2011/1215/6203.html)
4.  [美国自然历史博物馆开设全球首家研究生院](http://www.chnmuseum.cn/tabid/138/InfoID/80094/)
5.  [美国第一个博物馆颁博士学位](http://www1.voanews.com/chinese/news/education/First-US-Museum-to-Award-PhD-Degree-20100224-85250322.html)