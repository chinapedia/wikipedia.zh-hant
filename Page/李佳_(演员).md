**李佳**（，）\[1\]，曾名**李柯竺**，籍贯[中国](../Page/中国.md "wikilink")[云南](../Page/云南.md "wikilink")，[演技派](../Page/演技派.md "wikilink")[女演员](../Page/女演员.md "wikilink")\[2\]，[身高](../Page/身高.md "wikilink")1.68米。2000年毕业于[北京电影学院表演系](../Page/北京电影学院.md "wikilink")，与[赵薇](../Page/赵薇.md "wikilink")、[陈坤](../Page/陈坤.md "wikilink")、[黄晓明](../Page/黄晓明.md "wikilink")、[郭晓冬](../Page/郭晓冬.md "wikilink")、[何琳](../Page/何琳.md "wikilink")、[颜丹晨为同班同学](../Page/颜丹晨.md "wikilink")，與[靳东育有两子](../Page/靳东.md "wikilink")\[3\]。

## 经历

  - 1988年－1996年
    [中国戏曲学院附中](../Page/中国戏曲学院.md "wikilink")-京剧[刀马旦](../Page/刀马旦.md "wikilink")
  - 1996年－2000年 北京电影学院-表演[学士学位](../Page/学士.md "wikilink")
  - 2000年－2004年 [中国人民解放军总政治部话剧团演员](../Page/中国人民解放军总政治部.md "wikilink")
  - 2003年正式成为[中国电影家协会成员](../Page/中国电影家协会.md "wikilink")
  - 2004年至今 [中國人民对外友好协会](../Page/中國人民对外友好协会.md "wikilink")

## 作品

<table>
<tbody>
<tr class="odd">
<td><p>时间</p></td>
<td><p>类型</p></td>
<td><p>片名</p></td>
<td><p>角色</p></td>
<td><p>備註</p></td>
</tr>
<tr class="even">
<td><p>未知</p></td>
<td><p>电影</p></td>
<td><p><a href="../Page/二胡.md" title="wikilink">二胡</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>电影</p></td>
<td><p>三利股份今日涨停</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>电视剧</p></td>
<td><p>日照街</p></td>
<td><p>方茜</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p>电视剧</p></td>
<td><p>风云密支那</p></td>
<td><p>西莱</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>电视剧</p></td>
<td><p>有情人</p></td>
<td><p>方茜</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p>电视剧</p></td>
<td><p>家园</p></td>
<td><p>韩海霞</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p>电影</p></td>
<td><p>蓝色爱情</p></td>
<td><p>文竹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>电视剧</p></td>
<td><p>民国大劫案/火车大劫案</p></td>
<td><p>林室雅</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td><p>电影</p></td>
<td><p><a href="../Page/暖.md" title="wikilink">暖</a></p></td>
<td><p>暖</p></td>
<td><p>获第十六届<a href="../Page/东京电影节.md" title="wikilink">东京电影节金麒麟大奖</a><br />
<a href="../Page/戛纳电影节.md" title="wikilink">戛纳电影节参展影片</a><br />
<a href="../Page/澳门电影节.md" title="wikilink">澳门电影节参展影片</a></p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>电视剧</p></td>
<td><p><a href="../Page/天地有爱.md" title="wikilink">天地有爱</a></p></td>
<td><p>闻彩香</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p>电视剧</p></td>
<td><p><a href="../Page/遍地英雄.md" title="wikilink">遍地英雄</a></p></td>
<td><p>杨秀</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>电视剧</p></td>
<td><p>移植成功的生活/相逢在雨后</p></td>
<td><p>张华</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>电视剧</p></td>
<td><p><a href="../Page/出乎意料.md" title="wikilink">出乎意料</a></p></td>
<td><p>夏之兰</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>电视剧</p></td>
<td><p>秘密</p></td>
<td><p>艾云</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>电影</p></td>
<td><p>公园</p></td>
<td><p>高小君</p></td>
<td><p>2007年6月21日上映<br />
15届<a href="../Page/大学生电影节.md" title="wikilink">大学生电影节最佳女主角提名</a><br />
<a href="../Page/德国曼海姆国际电影节.md" title="wikilink">德国曼海姆国际电影节评委会大奖</a><br />
44届<a href="../Page/金橙国际电影节.md" title="wikilink">金橙国际电影节评委会奖</a><br />
2007<a href="../Page/日本琦玉电影节.md" title="wikilink">日本琦玉电影节特别奖</a></p></td>
</tr>
<tr class="odd">
<td><p>电视剧</p></td>
<td><p>远山/守山</p></td>
<td><p>黄鹂鸣</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>电视剧</p></td>
<td><p>最后的谎言/秋去秋来</p></td>
<td><p>苏小薇</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p>电视剧</p></td>
<td><p>金色农家</p></td>
<td><p>金谷</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>电影</p></td>
<td><p>浮出水面的影子</p></td>
<td><p>周露</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>电影</p></td>
<td><p><a href="../Page/爱有来生.md" title="wikilink">爱有来生</a>/银杏银杏</p></td>
<td><p>小兰、雅萍</p></td>
<td><p>2009年9月3日上映</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>电影</p></td>
<td><p>伟大的伟</p></td>
<td><p>摄影师</p></td>
<td><p>2009年10月17日上映</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>电视剧</p></td>
<td><p>青春不言败</p></td>
<td><p>阿梅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>电视剧</p></td>
<td><p>极致情感</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>电视剧</p></td>
<td><p>天地有爱/深圳有爱</p></td>
<td><p>黎明明</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>电视剧</p></td>
<td><p><a href="../Page/到爱的距离.md" title="wikilink">到爱的距离</a></p></td>
<td><p>林念初</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>电视剧</p></td>
<td><p><a href="../Page/女人帮.md" title="wikilink">女人帮</a></p></td>
<td><p>毕然</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>电视剧</p></td>
<td><p><a href="../Page/虎妈猫爸.md" title="wikilink">虎妈猫爸</a></p></td>
<td><p>罗丹</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 获奖

  - 第二十三届[金鸡奖最佳女主角提名](../Page/金鸡奖.md "wikilink")
  - 2004年[大学生电影节最佳女主角提名](../Page/大学生电影节.md "wikilink")
  - 第二届英国[万像国际华语电影节最佳女演员奖](../Page/万像国际华语电影节.md "wikilink") 《浮出水面的影子》

## 外部链接

  - [李佳_新浪博客](http://blog.sina.com.cn/lijiablog)

## 参考

[Category:中国电影女演员](../Category/中国电影女演员.md "wikilink")
[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")

1.  [光影会客厅：11日导演霍建起携演员李佳做客-搜狐娱乐频道](http://yule.sohu.com/20050308/n224593067.shtml)
2.  [程青松：春《暖》花开-搜狐娱乐频道](http://yule.sohu.com/20050309/n224604767.shtml)
3.  [](http://fj.people.com.cn/BIG5/n/2015/0925/c363308-26526265.html)