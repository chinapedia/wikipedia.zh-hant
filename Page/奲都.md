**奲都**（1057年-1062年）是[西夏毅宗](../Page/西夏.md "wikilink")[李諒祚的](../Page/李諒祚.md "wikilink")[年號](../Page/年號.md "wikilink")，共計6年。

## 纪年

<table>
<thead>
<tr class="header">
<th><p>奲都</p></th>
<th><p>元年</p></th>
<th><p>二年</p></th>
<th><p>三年</p></th>
<th><p>四年</p></th>
<th><p>五年</p></th>
<th><p>六年</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/公元纪年.md" title="wikilink">公元</a></p></td>
<td><p>1057年</p></td>
<td><p>1058年</p></td>
<td><p>1059年</p></td>
<td><p>1060年</p></td>
<td><p>1061年</p></td>
<td><p>1062年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/干支纪年.md" title="wikilink">干支</a></p></td>
<td><p><a href="../Page/丁酉.md" title="wikilink">丁酉</a></p></td>
<td><p><a href="../Page/戊戌.md" title="wikilink">戊戌</a></p></td>
<td><p><a href="../Page/己亥.md" title="wikilink">己亥</a></p></td>
<td><p><a href="../Page/庚子.md" title="wikilink">庚子</a></p></td>
<td><p><a href="../Page/辛丑.md" title="wikilink">辛丑</a></p></td>
<td><p><a href="../Page/壬寅.md" title="wikilink">壬寅</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北宋.md" title="wikilink">北宋</a></p></td>
<td><p><a href="../Page/嘉祐.md" title="wikilink">嘉祐二年</a></p></td>
<td><p>嘉祐三年</p></td>
<td><p>嘉祐四年</p></td>
<td><p>嘉祐五年</p></td>
<td><p>嘉祐六年</p></td>
<td><p>嘉祐七年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/契丹.md" title="wikilink">契丹</a></p></td>
<td><p><a href="../Page/清寧.md" title="wikilink">清寧三年</a></p></td>
<td><p>清寧四年</p></td>
<td><p>清寧五年</p></td>
<td><p>清寧六年</p></td>
<td><p>清寧七年</p></td>
<td><p>清寧八年</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李朝.md" title="wikilink">李朝</a></p></td>
<td><p><a href="../Page/龍瑞太平.md" title="wikilink">龍瑞太平四年</a></p></td>
<td><p>龍瑞太平五年</p></td>
<td><p><a href="../Page/彰聖嘉慶.md" title="wikilink">彰聖嘉慶元年</a></p></td>
<td><p>彰聖嘉慶二年</p></td>
<td><p>彰聖嘉慶三年</p></td>
<td><p>彰聖嘉慶四年</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
<td><p><a href="../Page/天喜.md" title="wikilink">天喜五年</a></p></td>
<td><p>天喜六年<br />
<a href="../Page/康平_(後冷泉天皇).md" title="wikilink">康平元年</a></p></td>
<td><p>康平二年</p></td>
<td><p>康平三年</p></td>
<td><p>康平四年</p></td>
<td><p>康平五年</p></td>
</tr>
</tbody>
</table>

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")

[Category:西夏年号](../Category/西夏年号.md "wikilink")
[Category:11世纪中国年号](../Category/11世纪中国年号.md "wikilink")
[Category:1050年代中国政治](../Category/1050年代中国政治.md "wikilink")
[Category:1060年代中国政治](../Category/1060年代中国政治.md "wikilink")