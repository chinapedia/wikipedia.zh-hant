**查爾斯·愛德華·京士福特·史密斯**爵士，[MC](../Page/軍功十字勳章.md "wikilink")，[AFC](../Page/空軍十字勳章_\(英國\).md "wikilink")（，），常稱作Sir
Charles
Kingsford-Smith，暱稱Smithy。他是[澳大利亞早期最有名的](../Page/澳大利亞.md "wikilink")[飛行員](../Page/飛行員.md "wikilink")，曾譽為當時最偉大的飛行員。

## 生平

史密斯生於[昆士蘭](../Page/昆士蘭.md "wikilink")[布里斯班](../Page/布里斯班.md "wikilink")[哈密爾頓市](../Page/哈密爾頓市.md "wikilink")。16歲在[悉尼技術學院畢業](../Page/悉尼技術高校.md "wikilink")，成為[電子工程師](../Page/電子工程.md "wikilink")。

1915年，宣誓加入澳大利亞軍隊。1917年加入[皇家飛行團](../Page/皇家飛行團.md "wikilink")，在[法國當戰機師](../Page/法國.md "wikilink")。20歲的他因戰功獲喬治五世頒予[軍隊十字章](../Page/軍隊十字章.md "wikilink")。

第一次世界大戰後，他往[美國](../Page/美國.md "wikilink")[荷里活當特技飛行員](../Page/荷里活.md "wikilink")。當另一個特技飛行員為這個危險職業賠上性命後，1921年他回到澳洲。

後來他以[空郵服務為職](../Page/空郵.md "wikilink")。1927年，他赴美購買了一部Fokker
Trimotor機，他將之名為「南部十字」（*Southern Cross*）。

1928年5月31日，史密斯和副機師Charles Ulm、領航員Harry Lyon和廣播控制員James
Warner在[加州](../Page/加州.md "wikilink")[奧克蘭國際機場起飛](../Page/奧克蘭國際機場_\(美國\).md "wikilink")，約一天後到達[夏威夷](../Page/夏威夷.md "wikilink")，短暫休息後飛往[斐濟首都](../Page/斐濟.md "wikilink")[蘇瓦](../Page/蘇瓦.md "wikilink")，那是當時在水上飛越的最長距離。6月9日抵布里斯班。

1930年，他勝出一項由[英國飛往澳大利亞的空中比賽](../Page/英國.md "wikilink")。兩星期後和Mary
Powell結婚。兩人生有一子。

1933年，他又一次破了由英國飛往澳大利亞的紀錄，擁有許多長距離飛行紀錄的他被視為世界上最偉大的飛行員。

1935年11月6日，他和其副機師John
Pethybridge在英國起飛，準備去澳大利亞。在[印度](../Page/印度.md "wikilink")[阿拉哈巴德](../Page/阿拉哈巴德.md "wikilink")（Allahabad）停留。飛往[新加坡途中](../Page/新加坡.md "wikilink")，他們在[孟加拉灣上空遇到](../Page/孟加拉灣.md "wikilink")[颱風](../Page/颱風.md "wikilink")，自此音訊全無。

悉尼的主要機場以他取名為[悉尼金斯福德·史密斯国际机场](../Page/悉尼金斯福德·史密斯国际机场.md "wikilink")。其飛機「南部十字」現時放在[布里斯班國際機場內](../Page/布里斯班國際機場.md "wikilink")。

## 紀錄

他作了飛行界三個「首次」：

  - 不停留橫越澳大利亞的內地
  - 由澳大利亞飛往[新西蘭](../Page/新西蘭.md "wikilink")
  - 由西向東橫越[太平洋](../Page/太平洋.md "wikilink")，即由澳大利亞飛往[美國](../Page/美國.md "wikilink")

## 外部連結

  - [一首關於京士福特·史密斯的詩（英語）](http://oldpoetry.com/poetry/25788)

[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:軍功十字勳章](../Category/軍功十字勳章.md "wikilink")
[Category:航空先驅](../Category/航空先驅.md "wikilink")
[Category:澳大利亞飛行員](../Category/澳大利亞飛行員.md "wikilink")
[Category:英國皇家空軍軍官](../Category/英國皇家空軍軍官.md "wikilink")
[Category:澳大利亞第一次世界大戰人物](../Category/澳大利亞第一次世界大戰人物.md "wikilink")
[Category:英格蘭裔澳大利亞人](../Category/英格蘭裔澳大利亞人.md "wikilink")
[Category:布里斯本人](../Category/布里斯本人.md "wikilink")
[Category:澳大利亞空難身亡者](../Category/澳大利亞空難身亡者.md "wikilink")