**本杰明·哈里森**（，），是[美國第二十三任](../Page/美國.md "wikilink")[總統](../Page/美國總統.md "wikilink")。其曾祖父[本杰明·哈里森五世為](../Page/本杰明·哈里森五世.md "wikilink")[美國獨立宣言簽署人之一](../Page/美國獨立宣言.md "wikilink")，祖父為[威廉·亨利·哈里森第九任美國總統](../Page/威廉·亨利·哈里森.md "wikilink")。他是第二個輸掉普選、但贏得選舉人票而勝出的總統（另外三位為1876年的[拉瑟福德·B·海斯](../Page/拉瑟福德·B·海斯.md "wikilink")、2000年的[喬治·W·布希](../Page/喬治·W·布希.md "wikilink")、2016年的[唐納·川普](../Page/唐納·川普.md "wikilink")，三人同屬共和黨）。

哈里森生於[俄亥俄州](../Page/俄亥俄州.md "wikilink")[北本德](../Page/北本德.md "wikilink")，21歲時全家搬到[印地安那州](../Page/印地安那州.md "wikilink")[印第安納波利斯](../Page/印第安納波利斯.md "wikilink")，最終成為一個著名的政治家。[南北戰爭期間](../Page/南北戰爭.md "wikilink")，他是20軍所屬的坎伯蘭陸軍的[准將](../Page/准將.md "wikilink")。

## 外部連結

  - [美國白宮的介紹](http://www.whitehouse.gov/history/presidents/bh23.html)

[Category:美国总统](../Category/美国总统.md "wikilink")
[Category:哈里森家族](../Category/哈里森家族.md "wikilink")
[Category:美國歷史
(1865年-1918年)](../Category/美國歷史_\(1865年-1918年\).md "wikilink")
[Category:死於流行性感冒的人](../Category/死於流行性感冒的人.md "wikilink")
[Category:美国共和党联邦参议员](../Category/美国共和党联邦参议员.md "wikilink")
[Category:19世纪美国政治家](../Category/19世纪美国政治家.md "wikilink")
[Category:1892年美国总统选举候选人](../Category/1892年美国总统选举候选人.md "wikilink")
[Category:死于肺炎的人](../Category/死于肺炎的人.md "wikilink")
[Category:美國共和黨總統候選人](../Category/美國共和黨總統候選人.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:美國長老宗教徒](../Category/美國長老宗教徒.md "wikilink")
[Category:印第安納州聯邦參議員](../Category/印第安納州聯邦參議員.md "wikilink")
[Category:葬于克朗山公墓](../Category/葬于克朗山公墓.md "wikilink")
[Category:俄亥俄州南北战争人物](../Category/俄亥俄州南北战争人物.md "wikilink")
[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")
[Category:北军将领](../Category/北军将领.md "wikilink")