**埃德加·阿德里安，第一代阿德里安男爵**，[OM](../Page/功績勳章_\(英國\).md "wikilink")，[PRS](../Page/皇家學會.md "wikilink")（，），[英国电生理学家](../Page/英国.md "wikilink")，曾任[剑桥大学教授](../Page/剑桥大学.md "wikilink")、[英国皇家学会会长](../Page/英国皇家学会.md "wikilink")。

他和[查尔斯·斯科特·谢灵顿](../Page/查尔斯·斯科特·谢灵顿.md "wikilink")，由于“关于神经功能方面的发现”共获1932年[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。

运用单纤维记录技术，在传入神经纤维上证明感觉刺激的强度是以冲动发放频率编码的；后又证明中枢通过运动神经纤维对肌肉传送信息，也用同样编码原则。

[Category:英国科学家](../Category/英国科学家.md "wikilink")
[Category:諾貝爾生理學或醫學獎獲得者](../Category/諾貝爾生理學或醫學獎獲得者.md "wikilink")
[Category:聯合王國男爵](../Category/聯合王國男爵.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:英国皇家学会会长](../Category/英国皇家学会会长.md "wikilink")
[Category:劍橋大學三一學院校友](../Category/劍橋大學三一學院校友.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")