**费县**（）位于中国[山东省南部](../Page/山东省.md "wikilink")，是[临沂市下辖的一个](../Page/临沂市.md "wikilink")[县](../Page/县.md "wikilink")；地处[蒙山](../Page/蒙山.md "wikilink")[沂水](../Page/沂水.md "wikilink")，蒙山南部。

耕地面积101万亩，山丘面积占76.4%，辖18个乡镇。

## 行政区划

费县辖1个开发区，1个街道、9个镇、2个乡。

开发区：费县经济开发区（省级经济开发区）

街道：费城街道

镇：上冶镇、薛庄镇、探沂镇、朱田镇、梁邱镇、新庄镇、马庄镇、胡阳镇、石井镇

乡：大田庄乡、南张庄乡

## 讀音

"費"在此做地名的老派[文讀音是bi](../Page/文讀.md "wikilink")4，因爲《[廣韻](../Page/廣韻.md "wikilink")》記載“**鄪**：邑名，在魯。**費**，上同。兵媚[切](../Page/反切.md "wikilink")”。兵媚切，就是和“秘”等字同音。當地人本來也是讀bi4。外地人包括普通話審音者，不知道“費”做地名時還有bi4這個讀音，只知道fei4。以訛傳訛，廣爲流傳。其實fei4源自《廣韻》芳未切，另有字義。

## 历史

费县具有悠久的历史。[春秋时期称鄪邑](../Page/春秋.md "wikilink")，[战国时期称鄪国](../Page/战国.md "wikilink")，[西汉初年设立县级行政单位](../Page/西汉.md "wikilink")。

费县是革命老区。1929年建立共产党的组织，[陈毅](../Page/陈毅.md "wikilink")、[罗荣桓](../Page/罗荣桓.md "wikilink")、[徐向前](../Page/徐向前.md "wikilink")、[谷牧等老一辈革命家曾在这里战斗生活](../Page/谷牧.md "wikilink")，进行过大青山突围战、柱子山歼灭战等大小战斗百余次。

## 地理

费县县城距临沂市40千米；京沪高速与日东高速公路在境内交汇，327国道、017省道、文泗公路横穿东西。

费县素有“奇石之乡”的美誉，集瘦、漏、皱、透、丑诸特点于一身，被誉为“世纪之交在中国北方发现的最伟大的自然奇迹”。

## 古迹

费县境内存有大量历史古迹，有[大汶口文化](../Page/大汶口文化.md "wikilink")、[龙山文化等遗址](../Page/龙山文化.md "wikilink")150多处，有国家二级文物[徐子鼎](../Page/徐子鼎.md "wikilink")、[王莽新币等馆藏文物](../Page/王莽.md "wikilink")2000多种，有[乾隆皇帝下江南时留下的诗文和](../Page/乾隆皇帝.md "wikilink")[米芾](../Page/米芾.md "wikilink")、[秦观等历史名人留下的墨宝](../Page/秦观.md "wikilink")。

费县是唐代政治家、书法家[颜真卿的故里](../Page/颜真卿.md "wikilink")。

## 旅游特色

  - 费县奇石
  - [费县塔山森林公园](../Page/费县塔山森林公园.md "wikilink")
  - [沂蒙云瀑洞天景区](../Page/沂蒙云瀑洞天景区.md "wikilink")

## 名人

  - [颜真卿](../Page/颜真卿.md "wikilink")

## 歌曲

  - [沂蒙山小调诞生地](../Page/沂蒙山小调.md "wikilink")

## 教育

  - [青岛理工大学费县校区位于费县东外环开发区](../Page/青岛理工大学.md "wikilink")
  - [临沂大学费县分校位于费县文化路中段](../Page/临沂大学.md "wikilink")

## 外部链接

  - [临沂信息港](http://ly-www.sd.cninfo.net/)
  - [费县人民政府公众信息网](https://web.archive.org/web/20071101081335/http://www.feixian.gov.cn:8080/)

## 参考文献

  - [费县概况](https://web.archive.org/web/20070902042948/http://www.feixian.gov.cn:8080/introduction/briefshow.top?ParameterPath=YnJpZWY=)

[费县](../Category/费县.md "wikilink") [县](../Category/临沂区县.md "wikilink")
[临沂](../Category/山东省县份.md "wikilink")