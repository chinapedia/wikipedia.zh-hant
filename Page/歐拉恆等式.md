[EulerIdentity2.svg](https://zh.wikipedia.org/wiki/File:EulerIdentity2.svg "fig:EulerIdentity2.svg")

**歐拉恆等式**是指下列的[關係式](../Page/恆等式.md "wikilink")：

  -
    \(e^{i \pi}+1=\,\){{\#invoke:Complex Number/Calculate|calculate|

`   e^(i*pi)+1`

|class=cmath|use math=yes}}

其中\(e\,\)是[自然對數的底](../Page/e_\(數學常數\).md "wikilink")，\(i \,\)是[虛數單位](../Page/虛數.md "wikilink")，\(\pi \,\)是[圓周率](../Page/圓周率.md "wikilink")。

這條恆等式第一次出現於1748年瑞士數學、物理學家[萊昂哈德·歐拉](../Page/萊昂哈德·歐拉.md "wikilink")（）在[洛桑出版的書](../Page/洛桑.md "wikilink")\(Introductio \,\)。這是[複分析的](../Page/複分析.md "wikilink")[歐拉公式的特殊情況](../Page/歐拉公式.md "wikilink")。

美國物理學家[理查德·費曼](../Page/理查德·費曼.md "wikilink")（）稱這恆等式為「數學最奇妙的公式」，因為它把5個最基本的[數學常數簡潔地連繫起來](../Page/數學常數.md "wikilink")。

## 證明

  -
    \(e^{ix} = \cos x + i \sin x \,\!\)（[歐拉公式](../Page/歐拉公式.md "wikilink")）

<!-- end list -->

  -
    \(e^{i \pi}=\cos \pi+ i \sin \pi\,\)（代入\(x=\pi \,\)）

<!-- end list -->

  -
    \(e^{i \pi}=\,\){{\#invoke:Complex Number/Calculate|calculate|

`  exp(1)^(i*pi)`

|class=cmath|use
math=yes}}（因\(\cos \pi = -1  \, \!\)和\(\sin \pi = 0\,\!\)）

  -
    \(e^{i \pi}+1=\,\){{\#invoke:Complex Number/Calculate|calculate|

`  exp(1)^(i*pi)+1`

|class=cmath|use math=yes}}

## 與歐拉恆等式有關的文學作品

《[博士熱愛的算式](../Page/博士熱愛的算式.md "wikilink")》，[小川洋子著](../Page/小川洋子.md "wikilink")，臺灣版本由王蘊潔翻譯，二版，麥田出版社，2008年，ISBN
978-986-173-408-8。

## 参见

  - [欧拉公式](../Page/欧拉公式.md "wikilink")

## 參考文獻

1.  [Conway, John H.](../Page/John_Horton_Conway.md "wikilink"), and
    [Guy, Richard K.](../Page/Richard_K._Guy.md "wikilink") (1996),
    *[The Book of
    Numbers](https://books.google.com/books?id=0--3rcO7dMYC&pg=PA254)*,
    Springer

2.  [Crease, Robert P.](../Page/Robert_P._Crease.md "wikilink") (10 May
    2004), "[The greatest equations
    ever](http://physicsworld.com/cws/article/print/2004/may/10/the-greatest-equations-ever)",
    *[Physics World](../Page/Physics_World.md "wikilink")*
    \[registration required\]

3.  [Dunham,
    William](../Page/William_Dunham_\(mathematician\).md "wikilink")
    (1999), *Euler: The Master of Us All*, [Mathematical Association of
    America](../Page/Mathematical_Association_of_America.md "wikilink")

4.  Euler, Leonhard (1922), *[Leonhardi Euleri opera omnia. 1, Opera
    mathematica. Volumen VIII, Leonhardi Euleri introductio in analysin
    infinitorum. Tomus
    primus](http://gallica.bnf.fr/ark:/12148/bpt6k69587.image.r=%22has+celeberrimas+formulas%22.f169.langEN)*,
    Leipzig: B. G. Teubneri

5.  [Kasner, E.](../Page/Edward_Kasner.md "wikilink"), and [Newman,
    J.](../Page/James_R._Newman.md "wikilink") (1940), *[Mathematics and
    the
    Imagination](../Page/Mathematics_and_the_Imagination.md "wikilink")*,
    [Simon & Schuster](../Page/Simon_&_Schuster.md "wikilink")

6.  [Maor, Eli](../Page/Eli_Maor.md "wikilink") (1998), *: The Story of
    a number*, [Princeton University
    Press](../Page/Princeton_University_Press.md "wikilink")

7.  Nahin, Paul J. (2006), *Dr. Euler's Fabulous Formula: Cures Many
    Mathematical Ills*, [Princeton University
    Press](../Page/Princeton_University_Press.md "wikilink")

8.  [Paulos, John Allen](../Page/John_Allen_Paulos.md "wikilink")
    (1992), *Beyond Numeracy: An Uncommon Dictionary of Mathematics*,
    [Penguin Books](../Page/Penguin_Books.md "wikilink")

9.  Reid, Constance (various editions), *From Zero to Infinity*,
    [Mathematical Association of
    America](../Page/Mathematical_Association_of_America.md "wikilink")

10. Sandifer, C. Edward (2007), *[Euler's Greatest
    Hits](https://books.google.com/books?id=sohHs7ExOsYC&pg=PA4)*,
    [Mathematical Association of
    America](../Page/Mathematical_Association_of_America.md "wikilink")

11.
12. Wells, David (1990), "Are these the most beautiful?", *[The
    Mathematical
    Intelligencer](../Page/The_Mathematical_Intelligencer.md "wikilink")*,
    12: 37–41,

13.
14.
[pl:Wzór Eulera\#Tożsamość
Eulera](../Page/pl:Wzór_Eulera#Tożsamość_Eulera.md "wikilink")

[Category:数学恒等式](../Category/数学恒等式.md "wikilink")
[Category:指数](../Category/指数.md "wikilink")