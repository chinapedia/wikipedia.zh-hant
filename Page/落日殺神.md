是一部在2004年上映的美國犯罪[電影](../Page/電影.md "wikilink")，由[梦工厂與](../Page/梦工厂.md "wikilink")[派拉蒙电影公司出品](../Page/派拉蒙电影公司.md "wikilink")，[麥可·曼恩執導](../Page/麥可·曼恩.md "wikilink")，[汤姆·克鲁斯以及](../Page/汤姆·克鲁斯.md "wikilink")[杰米·福克斯主演](../Page/杰米·福克斯.md "wikilink")。此片獲不少電影獎項提名，包括[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")、[土星獎及](../Page/土星獎.md "wikilink")[英國電影和電視藝術學院獎](../Page/英國電影和電視藝術學院.md "wikilink")。

[汤姆·克鲁斯少有地在其中扮演負面角色](../Page/汤姆·克鲁斯.md "wikilink")：一名凶残的职业杀手，而扮演計程車司機的[杰米·福克斯也因其出色的表現而深受好评](../Page/杰米·福克斯.md "wikilink")，并获得[奥斯卡最佳男配角的提名](../Page/奥斯卡最佳男配角.md "wikilink")。

## 情節

文森（[汤姆·克鲁斯](../Page/汤姆·克鲁斯.md "wikilink")
飾演）是一个仇恨社会的职业[杀手](../Page/杀手.md "wikilink")，平日靠著自己的基本武器「[HK
USP](../Page/HK_USP.md "wikilink")45標準型手槍」與「MK4魯格標準消音器手槍」打遍天下，他總是能憑藉出色的身手和兇殘的手段完成任務，拿到酬金，然後迅速脫身。麦克斯（[杰米·福克斯](../Page/杰米·福克斯.md "wikilink")
飾演）是一個在洛杉矶工作的計程車司机，他靠從事這個行業賺錢糊口已經十二年了，每天和形形色色的乘客打交道，穿梭於喧鬧浮躁的都市中。即使如此，麦克斯仍然對生活抱持平靜樂觀的態度。這兩個原本互不相干的人，卻因為一個殺人滅口的任務而有了交集。

這天晚上，文森上了麦克斯的計程車，他要求麦克斯載他去五個地方。麦克斯一開始很猶豫，因為這樣做是違反車行規定的，不過為了生計，麦克斯還是依約載文森去他指定的目的地。抵達第一個目的地後，文森走進一棟房子裡面，而麦克斯則在外面等著。在麦克斯等待的時候，一具屍體突然從天而降，跌落在計程車的车顶上，讓目睹此景的麦克斯嚇了一跳。過沒多久，文森大搖大擺的從房子裡走出來，說剛才墜樓的人是他殺的，並表示他受一個海外販毒集團的委託執行殺人任務，要在天亮之前奪走五條人命。文森知道自己的杀手身分已經暴露，便威脅麦克斯繼續開車載他去執行殺人任務，麦克斯為了活命，只得乖乖就範。途中文森為了不讓不知情的外人起疑心，還陪麦克斯去探望其生病中的母親艾達（艾爾瑪·霍爾
飾演）。在醫院裡，文森對艾達頻頻噓寒問暖，但一旁的麦克斯看在眼裡卻是有苦說不出。

麦克斯几次试图逃脱并且阻止文森接二连三的杀人，却都没成功，而接到報案的[洛杉矶警察局和](../Page/洛杉矶警察局.md "wikilink")[聯邦調查局也在他們後面窮追不捨](../Page/聯邦調查局.md "wikilink")。隨著時間一分一秒的過去，文森只剩下最後一個下手的目標了。面對多次試圖逃脱的失败和目睹文森杀人的血腥模樣，讓一向温顺谦和的麦克斯再也壓抑不住心中的怒火，他決定要与文森同归于尽。麦克斯故意讓車子擦撞工地設施，车子因此翻了好几次才停了下來，不過文森還是奮力爬出车子，去執行最后一个殺人任務。不久，一名警察看到這輛四腳朝天的計程車，便上前查看狀況。原本警察認為是一个普通的交通意外，但當他发现在后车厢裡面的尸体時，卻以為麦克斯是凶手。而麦克斯也在此时發現文森的最后一个目标是自己心儀的女检察官安妮（[潔達·蘋姬·史密斯](../Page/潔達·蘋姬·史密斯.md "wikilink")
飾演），情急之下，他用手銬控制警察的行動，並搶走對方的槍。在前去尋找安妮的途中，麦克斯特地打電話通知安妮，要她小心防範。為了安妮的一條命，文森和麦克斯展開一場驚心動魄的追逐戰，從安妮工作的大廈一路追到地鐵列車，經過一段時間的纏鬥後，麦克斯終於打敗文森，拯救了安妮的性命。最後，麦克斯帶著安妮離開地鐵站，留下生命走到盡頭的文森孤獨地坐在列車上。

## 演員表

| 演員                                                                  | 角色                       |
| ------------------------------------------------------------------- | ------------------------ |
| [汤姆·克鲁斯](../Page/汤姆·克鲁斯.md "wikilink")（Tom Cruise）                  | 文森 Vincent               |
| [杰米·福克斯](../Page/杰米·福克斯.md "wikilink")（Jamie Foxx）                  | 麥克斯 Max Durocher         |
| [潔達·蘋姬·史密斯](../Page/潔達·蘋姬·史密斯.md "wikilink")（Jada Pinkett Smith）    | Annie                    |
| [馬克·路法羅](../Page/馬克·路法羅.md "wikilink")（Mark Ruffalo）                | Fanning                  |
| [彼得·博格](../Page/彼得·博格.md "wikilink")（Peter Berg）                    | Richard Weidner          |
| [布魯斯·麥克吉爾](../Page/布魯斯·麥克吉爾.md "wikilink") （Bruce McGill）           | Pedrosa                  |
| [艾爾瑪·霍爾](../Page/艾爾瑪·霍爾.md "wikilink") （Irma P. Hall）               | Ida                      |
| [貝瑞·夏巴卡·亨利](../Page/貝瑞·夏巴卡·亨利.md "wikilink") （Barry Shabaka Henley） | Daniel                   |
| [哈維爾·巴登](../Page/哈維爾·巴登.md "wikilink")（Javier Bardem）               | Felix                    |
| [理查德·瓊斯](../Page/理查德·瓊斯.md "wikilink") （Richard T. Jones）           | Traffic Cop \#1          |
| [科里·斯科特](../Page/科里·斯科特.md "wikilink") （Klea Scott）                 | Fed \#1                  |
| [保迪·艾爾夫曼](../Page/保迪·艾爾夫曼.md "wikilink") （Bodhi Elfman）             | Young Professional Man   |
| [戴比·馬扎](../Page/戴比·馬扎.md "wikilink") （Debi Mazar）                   | Young Professional Woman |
| [埃米利奧·里維拉](../Page/埃米利奧·里維拉.md "wikilink") （Emilio Rivera）          | Paco                     |
| [傑米·麥克布萊德](../Page/傑米·麥克布萊德.md "wikilink") （Jamie McBride）          | Traffic Cop \#2          |
| [傑森·史塔森](../Page/傑森·史塔森.md "wikilink") （Jason Statham）              | Airport Man              |

## 製作

电影在[洛杉矶拍摄完成而不是原剧本裡的城市](../Page/洛杉矶.md "wikilink")[纽约](../Page/纽约.md "wikilink")。【落日杀神】也是第一部用高清晰电影流摄像机(Viper
FilmStream High-Definition
Camera)拍摄完成。此外電影海報上面出現湯姆克魯斯手上拿的手槍，也就是文森在酒吧裡槍殺Daniel用的手槍，很少有人知道那是「魯格標準系列消音手槍」，他是用槍管最長的版本。

幕後特輯有出現，導演[麥可·曼恩為了拍這部電影](../Page/麥可·曼恩.md "wikilink")，還特別受雇[英國陸軍](../Page/英國陸軍.md "wikilink")[空降特勤隊的退伍軍人](../Page/空降特勤隊.md "wikilink")，來給[湯姆克魯斯軍事訓練](../Page/湯姆克魯斯.md "wikilink")，包刮：快速拔槍跟快速反應[射擊與](../Page/射擊.md "wikilink")[武術搏擊和](../Page/武術.md "wikilink")[近距離戰鬥](../Page/近距離戰鬥.md "wikilink")。

## 評價

此片在电影评论网站[烂番茄](../Page/烂番茄.md "wikilink")(Rotten
Tomatoes)保持了86%的正评价，深受影评人喜爱。

## 外部連結

  -
  -
  -
  -
  -
  -
[Category:2004年電影](../Category/2004年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:2000年代犯罪片](../Category/2000年代犯罪片.md "wikilink")
[Category:美國犯罪驚悚片](../Category/美國犯罪驚悚片.md "wikilink")
[Category:洛杉磯背景電影](../Category/洛杉磯背景電影.md "wikilink")
[Category:黑色電影](../Category/黑色電影.md "wikilink")
[Category:夢工廠電影](../Category/夢工廠電影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:英国电影学院奖最佳摄影获奖电影](../Category/英国电影学院奖最佳摄影获奖电影.md "wikilink")
[Category:殺手主角題材作品](../Category/殺手主角題材作品.md "wikilink")
[Category:新黑色電影](../Category/新黑色電影.md "wikilink")