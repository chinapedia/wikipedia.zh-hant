《**时光倒流七十年**》（），是[1980年](../Page/1980年電影.md "wikilink")[吉诺特·兹瓦克执导的浪漫奇幻电影](../Page/吉诺特·兹瓦克.md "wikilink")。改编自[理察·麥瑟森](../Page/理察·麥瑟森.md "wikilink")1975年小说《重返的时刻》（）。

獲[第53屆](../Page/第53屆奧斯卡金像獎.md "wikilink")[奧斯卡](../Page/奧斯卡金像獎.md "wikilink")[最佳服装设计奖提名及](../Page/奥斯卡最佳服装设计奖.md "wikilink")[第38屆](../Page/第38屆金球獎.md "wikilink")[金球獎](../Page/金球獎_\(影視獎項\).md "wikilink")[最佳原创音乐奖提名](../Page/金球奖最佳原创音乐.md "wikilink")。

## 劇情概要

男主角Richard
Collier在1972年的大學畢業聚會上遇到一個神秘的老婦人，她走到Richard的跟前，輕聲低喃：「回到我身邊來吧！(Come
back to me.)」又交給Richard一個老舊的[懷錶後](../Page/懷錶.md "wikilink")，便於夜色中緩緩的離開。

Richard對此感到困惑，但他漸漸淡忘了這回事。八年之後，已成為劇作家的Richard與女友分手後決定前往麥克金納島度假，在他下榻的Grand
Hotel的歷史文物廳中掛著一幅年輕女子的[相片](../Page/相片.md "wikilink")，Richard注視著相片中那張美麗的臉，久久無法自拔，他莫名的感到似曾相識。

經過Richard的多方查證，原來相片上的人是已故女演員Elise
McKenna，也就是八年前給他懷錶的那個老婦人，她在見到Richard的當晚在家中過世。更深入的調查後，Richard發現，原來自己曾與Elise相識相戀。

自己竟會和一位70年前的女子相戀，這簡直不可思議，但Richard對此深信不疑。為此廢寢忘食的Richard，得到從前就讀大學的物理學教授的幫助，他希望能透過時光倒流的方法回到過去。

改變房裡的擺設，穿著70多年前流行款式的西裝，躺在床上自我[催眠的Richard](../Page/催眠.md "wikilink")，多次嘗試未果後，終於成功進入夢鄉回到過去。在那個時空的陌生人群中四處追尋，他找到了Elise，很快他們便陷入熱戀。

Elise的經紀人William卻想盡辦法要將兩人分開，因為他害怕墜入愛河的Elise會為了Richard離開她的演藝之路，然而這一切並不能阻止他們相見。就在他們擺脫了William，愉快地談論著將來時，Richard意外地從上衣口袋裡掏出一枚1979年的硬幣，瞬間把他帶回現代。

無法再回到過去見到愛人的Richard將自己反鎖在房間裡，幾天後，Grand
Hotel的老員工救出了昏迷的Richard，但此時的Richard因為整整一個星期沒有進食而進入[彌留狀態](../Page/彌留.md "wikilink")。此時的他出現了幻覺，看見年輕的Elise正向他招手，他走上前去緊握其手。

相隔近70年，二人終於再次團聚。

全劇在此完結。

## 主要角色

  - [克里斯托弗·里夫](../Page/克里斯托弗·里夫.md "wikilink") 饰
    -{zh-hans:理查德·科里耶;zh-hk:高李察;zh-tw:理察·柯里爾}-（Richard
    Collier）

  - [珍·西摩兒](../Page/珍·西摩兒.md "wikilink") 饰
    -{zh-hans:爱丽斯·麦克坎娜;zh-hk:麥伊莉;zh-tw:艾莉絲·麥肯納}-（Elise
    McKenna）

  - [克里斯多弗·普拉玛](../Page/克里斯多弗·普拉玛.md "wikilink") 饰
    -{zh-hans:威廉·菲塞特·罗宾森;zh-hk:羅威廉;zh-tw:威廉·佛塞特·羅賓森}-（William
    Fawcett Robinson）

  - 饰 劳拉·罗伯茲（Laura Roberts）

## 影片製作

[Mackinac_Island_Grand_Hotel.jpg](https://zh.wikipedia.org/wiki/File:Mackinac_Island_Grand_Hotel.jpg "fig:Mackinac_Island_Grand_Hotel.jpg")麥基諾島的格蘭特大酒店拍攝。\]\]
此片改编自[小说](../Page/小说.md "wikilink")《重返的时刻》，[作者](../Page/作者.md "wikilink")[理察·麥瑟森在小说的前序中坦言](../Page/理察·麥瑟森.md "wikilink")，一次他与家人的旅行途中，偶然在[弗吉尼亚城的一家](../Page/弗吉尼亚.md "wikilink")[歌剧院门口看到一张海报](../Page/歌剧院.md "wikilink")，那是已故[美国著名女](../Page/美国.md "wikilink")[演员](../Page/演员.md "wikilink")的海报\[1\]
，在那一刹那，他爱上了她，而《时光倒流七十年》中那段动人的故事也因此产生了。

《時光倒流七十年》的制作成本只有500万，这在现今乃至当时都只能算一个很微不足道的数目，而且其中的五分之一是作为男主角的扮演者[基斯杜化·李夫的片酬](../Page/基斯杜化·李夫.md "wikilink")，同时，由于拍摄时间十分紧湊，因此所有工作人员及演员必须一星期工作六天，每天工作16小时。在这样艰苦的条件下，却没有一个人抱怨，摄像师甚至表示他愿意无偿的工作，而凭借《[超人](../Page/超人_\(電影\).md "wikilink")》一片迅速竄红的里维则无数次拒绝了一些片酬优厚多倍的剧组的邀请，这其中的原因只能够在剧本中寻找。

### 配樂

除了[约翰·拜瑞的配樂外](../Page/约翰·拜瑞.md "wikilink")，電影亦引用了[拉赫曼尼諾夫的](../Page/拉赫曼尼諾夫.md "wikilink")《[帕格尼尼主題狂想曲变奏十八](../Page/帕格尼尼主題狂想曲.md "wikilink")》。

## 反響

電影在發行之初並不賣座，就連[導演](../Page/導演.md "wikilink")[吉诺特·兹瓦克後來也不得不承認](../Page/吉诺特·兹瓦克.md "wikilink")，儘管他與全体制作人员都对这部片有莫大期望，《時光倒流七十年》的票房以及奖项都曾令他感到難過。当时影片试映時，并没有受到观众的爱戴，所以也就沒辦法在戏院播放。

事隔多年，这部影片无意间被一个电视台高层工作者发现了，他給这段动人的爱情故事深深打动，於是將之在有线频道播放，結果观众反应热烈。《时光倒流七十年》這部差点被埋没的好片子就这样重见天日，成为经典電影。

## 外部連結

  -
  -
  -
  -
  -
  -
  - {{@movies|fNatm0704002}}

  -
  -
## 參考文獻

[Category:1980年電影](../Category/1980年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1980年代劇情片](../Category/1980年代劇情片.md "wikilink")
[Category:美國浪漫劇情片](../Category/美國浪漫劇情片.md "wikilink")
[Category:時間旅行電影](../Category/時間旅行電影.md "wikilink")
[Category:1910年代背景電影](../Category/1910年代背景電影.md "wikilink")
[Category:1980年代背景電影](../Category/1980年代背景電影.md "wikilink")
[Category:密西根州背景電影](../Category/密西根州背景電影.md "wikilink")
[Category:環球影業電影](../Category/環球影業電影.md "wikilink")
[Category:密西根州取景電影](../Category/密西根州取景電影.md "wikilink")
[Category:芝加哥取景電影](../Category/芝加哥取景電影.md "wikilink")

1.  [The character of Elise McKenna in the movie Somewhere:Maude
    Adams](http://www.bookmice.net/darkchilde/maude/adams.html)