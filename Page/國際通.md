[Kokusai-dori08s3s4440.jpg](https://zh.wikipedia.org/wiki/File:Kokusai-dori08s3s4440.jpg "fig:Kokusai-dori08s3s4440.jpg")
**國際通**（）是[沖繩縣](../Page/沖繩縣.md "wikilink")[那霸市](../Page/那霸市.md "wikilink")[縣廳前交差點至](../Page/縣廳前交差點.md "wikilink")[安里三叉路之間的一段約](../Page/安里三叉路.md "wikilink")1.6[公里的大街](../Page/公里.md "wikilink")。該大街是那霸市最繁華的商業街。

國際通最早建成於1933年，是當時連接原那霸市中心至[首里市的最短的路線](../Page/首里市.md "wikilink")，當地人慣稱「新縣道」。此路甫建成之時，其周圍人煙稀少。1944年[沖繩島戰役之後](../Page/沖繩島戰役.md "wikilink")，[美國佔領了](../Page/美國.md "wikilink")[琉球群島](../Page/琉球群島.md "wikilink")。美軍在這條路旁建立了「[厄内·派爾國際劇場](../Page/:en:Ernie_Pyle.md "wikilink")」，當地逐漸繁榮，路名也隨著國際劇場而得名「國際通」。沖繩人也紛紛來到這裡經商，[百貨](../Page/百貨.md "wikilink")、[商場等拔地而起](../Page/商場.md "wikilink")，國際通成為那霸最繁華的地段，被人稱為「**奇跡的一英里**」。

## 外部連結

  - [](http://www.kokusaidori.org/)
  - [](http://www.kotsu-okinawa.org/)

[Category:沖繩縣建築物](../Category/沖繩縣建築物.md "wikilink")
[Category:沖繩縣觀光地](../Category/沖繩縣觀光地.md "wikilink")
[39](../Category/沖繩縣道.md "wikilink")
[Category:那霸市](../Category/那霸市.md "wikilink")