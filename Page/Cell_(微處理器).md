**Cell**[微處理器架構](../Page/微處理器.md "wikilink")（**Cell Broadband Engine
Architecture**，通常簡稱**Cell
BE**\[1\]或**CBEA**\[2\]）由[索尼](../Page/索尼.md "wikilink")、[索尼電腦娛樂](../Page/索尼電腦娛樂.md "wikilink")、[东芝](../Page/东芝.md "wikilink")、[國際商業機器](../Page/國際商業機器.md "wikilink")（IBM）公司联合开发\[3\]。它是以[RISC指令体系的](../Page/RISC.md "wikilink")[Power架构为基础来设计的](../Page/IBM_POWER.md "wikilink")，并具有高[时钟频率](../Page/时钟频率.md "wikilink")、高执行效率等特点。主要应用于[PlayStation
3和](../Page/PlayStation_3.md "wikilink")[刀鋒服务器之上](../Page/刀鋒服务器.md "wikilink")。而CELL處理器的第二代版本，提高了双精度[浮点运算性能](../Page/浮点.md "wikilink")。以往的CELL處理器，[双精度的性能只有](../Page/双精度.md "wikilink")[單精度的十分之一](../Page/單精確浮點數.md "wikilink")。而新的CELL處理器，可以使到双精度性能有五倍的提升。\[4\]

## 發展歷史

[Peter_Hofstee.jpg](https://zh.wikipedia.org/wiki/File:Peter_Hofstee.jpg "fig:Peter_Hofstee.jpg")

在2000年年中，[索尼電腦娛樂](../Page/索尼電腦娛樂.md "wikilink")，[東芝公司和](../Page/東芝.md "wikilink")[IBM結成一個被稱為](../Page/IBM.md "wikilink")「STI」的聯盟，用以設計和製造的[處理器](../Page/微處理器.md "wikilink")。\[5\]

STI設計中心於2001年3月開業。Cell使用[POWER4處理器的設計工具的增強版本](../Page/POWER4.md "wikilink")，其設計期間為期四年。有三家公司超過400名的工程師一起在[奧斯汀工作](../Page/奧斯汀.md "wikilink")，並有11個IBM設計中心的重要支援。\[6\]在此期間，IBM提出了Cell架構，製造工藝和[軟體環境有關的多項](../Page/軟體.md "wikilink")[專利](../Page/專利.md "wikilink")。早期專利版本的Broadband
Engine被證實是一個晶片封裝，包括“處理單元（Processing Elements）”，這是該專利的描述，是目前已知的Power
Processing Element（PPE）。目前在Broadband
Engine晶片上，每個“處理單元”包含8個[加速處理單元](../Page/加速處理單元.md "wikilink")，而它被簡稱為SPE。\[7\]

2007年3月，IBM將Cell微處理器由第一代產品的90[奈米製程推進到](../Page/奈米.md "wikilink")65奈米製程，由位於美國[紐約州](../Page/紐約州.md "wikilink")
East Fishkill
的12吋[晶圓廠展開](../Page/晶圓.md "wikilink")65奈米製程Cell微處理器的生產。製程推進到65奈米之後，Cell的晶片面積與耗電量將可進一步壓低，有利於[數位家電等相關應用的發展](../Page/數位.md "wikilink")。\[8\]\[9\]

2008年2月，IBM宣布該公司將開始製造的45奈米制程的Cell處理器。\[10\]\[11\]2009年8月，輕薄版PlayStation
3－PS3 Slim同時採用45奈米Cell處理器。\[12\]

2008年5月，IBM推出了高效能的的[雙精度浮點版本的Cell處理器](../Page/雙精度.md "wikilink")，以65奈米為主要尺寸的PowerXCell
8i。\[13\]

2008年5月，以一個[Opteron處理器和PowerXCell](../Page/Opteron.md "wikilink")
8i為基礎的[超級電腦](../Page/超級電腦.md "wikilink")，IBM[走鵑系統](../Page/走鵑_\(超級電腦\).md "wikilink")，成為世界上第一個達成[PetaFLOPS的系統](../Page/FLOPS.md "wikilink")\[14\]，它當時是世界上最快的[電腦](../Page/電腦.md "wikilink")，而此紀錄一直維持到2009年第3季。

2009年，IBM于SC09高性能计算会议上宣布，放弃基于Cell架构处理器的开发\[15\]\[16\]，Cell架构及设计理念将融合到IBM的其他处理器产品中。\[17\]該公司原計劃開發
PowerXCell-8i 處理器的後續產品，內含雙PowerPC處理器核心，並設置了32個 SPE
單元的下一代Cell處理器將已被取消。\[18\]

## 概要

  - Cell包含9个核心：1个64位的[PPE控制核心和](../Page/PPE.md "wikilink")8个完全一样的[SPE运算核心](../Page/SPE.md "wikilink")。
  - 拥有2亿5千万个[晶体管](../Page/晶体管.md "wikilink")（DD2量产版）。核心面积为235平方毫米，采用IBM的[SOI](../Page/SOI.md "wikilink")
    90纳米铜互连工艺制造，在此之后采用65纳米工艺制造。
  - 首批工程样品工作频率为4.06 GHz，工作电压1.1[伏特](../Page/伏特.md "wikilink")；此后有4.6
    GHz。
  - 每秒可进行2560亿次浮点计算（256 GFlops）。
  - 支持[网格运算](../Page/网格运算.md "wikilink")，具备灵活的[并行](../Page/平行運算.md "wikilink")、[分布式的计算结构](../Page/分散運算.md "wikilink")。

2005年8月25日，[IBM](../Page/IBM.md "wikilink")、[SONY](../Page/SONY.md "wikilink")、[SCEI與](../Page/SCEI.md "wikilink")[東芝等](../Page/東芝.md "wikilink")
4 家公司正式公開「Cell」，該次所公布的規格資料參考文件，共分為 5 份，包括 1 份說明 Cell
基於[分散式處理與多媒體應用所定義的整體架構](../Page/分布式計算.md "wikilink")，另外
4 份則是關於 Cell 獨立[浮點數運算單元](../Page/浮點數運算.md "wikilink") SPU
的[指令集架構](../Page/指令集架構.md "wikilink")，低階[組合語言](../Page/組合語言.md "wikilink")，高階
C/[C++](../Page/C++.md "wikilink")
程式語言擴充規格，以及應用程式[二進位介面](../Page/二進位.md "wikilink")（Application
Binary Interface）的規格書與說明文件。\[19\]

Cell結合了通用的[Power
Architecture內核](../Page/Power_Architecture.md "wikilink")，適中的性能與流線型的[協處理器](../Page/協處理器.md "wikilink")，大大加快[多媒體和](../Page/多媒體.md "wikilink")[向量處理的應用程序](../Page/向量.md "wikilink")，以及許多其他形式的專用計算。\[20\]

### 處理器架構

PPE 可以作为資源管理使用，SPE 可以作为数据处理器使用。PPE
上的[程序可以将任务分解到](../Page/程序.md "wikilink") SPE
上完成，然后相互传输数据。SPE
缺少一般处理器中的大部分通用特性，它们根本不能执行常见的操作系统任务，没有[虚拟内存的支持](../Page/虚拟内存.md "wikilink")，不能直接访问计算机的[RAM](../Page/隨機存取記憶體.md "wikilink")，[中断支持也非常有限](../Page/中断.md "wikilink")。将
SPE、PPE 和[主存控制器连接在一起的是一个名为](../Page/主存.md "wikilink") Element
Interconnect Bus 的[总线](../Page/总线.md "wikilink")，这是数据传输的主要通道。\[21\]

#### PPE

Power Processor Element（簡稱為PPE）：PPE
包含一個64[位元雙](../Page/位元.md "wikilink")[執行緒PowerPC結構的](../Page/執行緒.md "wikilink")[RISC內核](../Page/RISC.md "wikilink")，並支援[PowerPC的虛擬記憶體子系統](../Page/PowerPC.md "wikilink")。它具有32[KB的](../Page/KB.md "wikilink")
L1 指令[快取](../Page/CPU快取.md "wikilink")，以及一個32 KB
資料[快取](../Page/CPU快取.md "wikilink")，以及512 KB
L2的共用[快取](../Page/快取.md "wikilink")。Cell中的PPE包含VMX指令集（Vector
Multimedia eXtensions：[AltiVec技術](../Page/AltiVec.md "wikilink")）。\[22\]

#### SPE

Synergistic Processing Elements，简称SPE。\[23\]每个 SPE 包括：

  - 一个[向量处理器](../Page/向量.md "wikilink")，称为 Synergistic Processing Unit，或
    SPU
  - SPU 中的一个私有内存区域，称为本地存储（PS3 上这个区域的大小是 256K）
  - 用来联系外部世界的一组通信通道
  - 一组 128 个[寄存器](../Page/寄存器.md "wikilink")，每个 128 位宽（每个寄存器通常都可以用来同时保存
    4 个 32 位的值）
  - 一个記憶體流控制器（MFC），它负责管理 SPU 的本地存储和主存之间的
    [DMA](../Page/DMA.md "wikilink") 传输

#### EIB

Element Interconnect Bus（簡稱
EIB）：透過該匯流排，每個DMA控制器取得與SPE相關的指令和資料。DMA控制器也將結果送到通用匯流排，使其可輸出到晶片外，以傳送給晶片上的週邊設備或PPE快取記憶體。PPE可意識到SPE所傳輸的資料，但SPE則完全不知道毗鄰的任何流量；這將保持SPE的簡易性，並限制在其執行時的中斷或不必要的影響，如果SPE需要知道外部資料的變化，其各自的DMA控制器便負責擷取資訊。\[24\]

#### 記憶體與I/O控制器

Cell
BE内置2.5兆字节内存，通过[Rambus的](../Page/Rambus.md "wikilink")[XDR和](../Page/XDR_DRAM.md "wikilink")[FlexIO技术](../Page/FlexIO.md "wikilink")，每秒可与外部内存交换100吉字节Gbytes的数据。XDR記憶體控制器介面(XIO)為72位元寬，可以在3.2Gbps資料速率下運作并提供25.6GB/s的總記憶體頻寬。\[25\]

### PowerXCell 8i

2008年，IBM公佈了經修訂的變種Cell，它被稱作**PowerXCell
8i**，並從IBM的[刀鋒伺服器QS](../Page/刀鋒伺服器.md "wikilink")22開始採用。\[26\]花費超過一億[美元的](../Page/美元.md "wikilink")
[走鵑](../Page/走鵑_\(超級電腦\).md "wikilink") 是世界第一台
[Linpack](../Page/Linpack.md "wikilink") 達成 1
peta[flops](../Page/flops.md "wikilink")
的[超級電腦](../Page/超級電腦.md "wikilink")，採用雙核 AMD
[Opteron](../Page/Opteron.md "wikilink") 加上PowerXCell 8i
處理器混合為一個[節點的設計](../Page/節點.md "wikilink")\[27\]，[走鵑共有](../Page/走鵑_\(超級電腦\).md "wikilink")6563顆雙核的
AMD Opteron，以及12240顆IBM PowerXCell
8i。\[28\]國際超級電腦大會發布的綠色超級電腦500大（Green500）名單，IBM的PowerXCell
8i獨占前3名及5至7名，該排名以平均每瓦電力每秒所提供的浮點運算能力(MFLOPS/W)為基準。\[29\]除了QS22和超級電腦，PowerXCell也被做成一張[PCI-E介面的](../Page/PCI-E.md "wikilink")[加速處理器](../Page/加速處理單元.md "wikilink")，並在[QPACE專案作為核心處理器](../Page/QPACE.md "wikilink")。\[30\]

## 商業化

### PS3

STI 將 Cell
應用於[高畫質數位影音家電](../Page/高畫質.md "wikilink")、[遊樂器](../Page/遊樂器.md "wikilink")、[電腦繪圖](../Page/電腦繪圖.md "wikilink")、[科學運算等領域](../Page/科學運算.md "wikilink")，其中以
SCE 所推出的 [PS3](../Page/PS3.md "wikilink") 主機為相關應用中最受矚目、規模也最為龐大的產品。第一代的
Cell 微處理器將具備1個 PPE 微處理器核心，與8個 SPE 協同處理器（保留1個 SPE 作為備援，實際可用的 SPE 為7個），由
2.5 億[電晶體所構成](../Page/電晶體.md "wikilink")，PS3 則採用時脈 3.2GHz 的版本。\[31\]

### SpursEngine

[东芝推出基於CELL的](../Page/东芝.md "wikilink")[輔助處理器](../Page/輔助處理器.md "wikilink")，名為SpursEngine。\[32\]這與真正的CELL處理器不同，它只有1個PPE和4個SPE核心，但額外增加了[MPEG-2和](../Page/MPEG-2.md "wikilink")[H.264的硬件编解码器](../Page/H.264.md "wikilink")。它可以用來加速图片和視頻播放（包括[MPEG-2和](../Page/MPEG-2.md "wikilink")[H.264](../Page/H.264.md "wikilink")），並有自己的[XDR記憶體](../Page/XDR_DRAM.md "wikilink")。接口方面，可以採用PCI-E
x1或者x4。东芝的平板电视都會採用相關的處理器，将标清分辨率插值至1080p。並同時進行細節修補，改善色彩，边缘锐利化等工作\[33\]。[麗台已推出採用SpursEngine晶片的加速卡](../Page/麗台.md "wikilink")，用作協助[電腦作影像編輯](../Page/電腦.md "wikilink")。\[34\]\[35\]

### 家用電視

東芝公司推出Cell
TV，它能把[2D畫面轉換成](../Page/2D.md "wikilink")[3D](../Page/3D.md "wikilink")。Cell
T還可升級數位視訊，甚至補強[畫素](../Page/畫素.md "wikilink")，強化低品質的[串流](../Page/串流.md "wikilink")，並測知房間的燈光，調整[螢幕的顯示品質](../Page/螢幕.md "wikilink")。Cell
TV尚有內建[攝影機](../Page/攝影機.md "wikilink")、[麥克風與軟體](../Page/麥克風.md "wikilink")，可以撥打[網路視訊電話](../Page/IP電話.md "wikilink")。\[36\]\[37\]这是东芝以Cell为核心开发的首款产品，重点显然是将[电视机作为一个范例](../Page/电视机.md "wikilink")，用来说明[硬件和](../Page/硬件.md "wikilink")[软件工程师能够合力开发出什么样的产品](../Page/软件工程师.md "wikilink")。例如：利用Cell电视机的连网功能建立一种商业模式，这款电视机显然可以成为一大批付费服务的载体。\[38\]

## 參見

  - [走鵑 (超級電腦)](../Page/走鵑_\(超級電腦\).md "wikilink")
  - [PlayStation 3](../Page/PlayStation_3.md "wikilink")

## 參考資料

## 外部链接

  - [Cell Broadband Engine resource
    center](http://www.ibm.com/developerworks/power/index.html)

  - [索尼電腦娛樂
    Cell處理器專屬網站](https://web.archive.org/web/20050923093239/http://cell.scei.co.jp/)

  - [Cellブロードバンド・エンジン・マイクロプロセッサ向けにLinux機能拡張を含む主要ソフトウェア開発ツールを公開](http://www.sony.co.jp/SonyInfo/News/Press/200511/05-1110/)

  - [Cmpware Configurable Multiprocessor Development Kit for Cell
    BE](http://www.cmpware.com/Docs/ProductBrief_3.0_CellBE.pdf)

  - [ISSCC 2005: The CELL
    Microprocessor](http://www.realworldtech.com/cell/)

  - [The little broadband engine that could: Reviewing the newest little
    SDK that installs natively on
    PS3](http://www.ibm.com/developerworks/power/library/pa-tacklecell5/index.html)

  - [Introducing the IBM/Sony/Toshiba Cell Processor ? Part I: the SIMD
    processing units](http://arstechnica.com/features/2005/02/cell-1/)

  - [Introducing the IBM/Sony/Toshiba Cell Processor -- Part II: The
    Cell
    Architecture](http://arstechnica.com/articles/paedia/cpu/cell-2.ars)

[Category:IBM处理器](../Category/IBM处理器.md "wikilink")
[Category:索尼](../Category/索尼.md "wikilink")
[Category:索尼互動娛樂](../Category/索尼互動娛樂.md "wikilink")
[Category:PlayStation 3](../Category/PlayStation_3.md "wikilink")

1.  [Linux on Cell BE-based
    Systems](http://www.bsc.es/computer-sciences/programming-models/linux-cell)


2.  [What does CBEA stand for? Cell Broadband Engine Architecture
    (IBM)](http://www.acronymfinder.com/Cell-Broadband-Engine-Architecture-%28IBM%29-%28CBEA%29.html)

3.

4.  [第二代Cell处理器 IBM新刀片服务器登场](http://news.mydrivers.com/1/106/106072.htm)

5.  Krewell, Kevin (14 February 2005). "Cell Moves Into the Limelight".
    **.

6.

7.

8.
9.

10.

11. [IBM shrinks Cell to 45nm. Cheaper PS3s will
    follow](http://arstechnica.com/gaming/2008/02/ibm-shrinks-cell-to-45nm-cheaper-ps3s-will-follow/)

12.

13.

14.

15.

16.

17. [世界第一超算“走鹃”成为绝唱
    IBM放弃Cell研发](http://server.51cto.com/News-164509.htm)

18. [天驕的隕落：IBM放棄Cell處理器開發計劃](http://www.chinesebiznews.com/index.php/component/content/article/42-gszx/8495-ibmcell.html)


19.

20.

21. [在 Cell BE
    处理器上编写高性能的应用程序](https://www.ibm.com/developerworks/cn/linux/pa-linuxps3-1/)

22.

23.
24.

25. [Rambus - Sony
    PlayStation 3](http://www.rambus.com/tw/technology/applications/gaming/ps3.html)


26.

27.

28.

29.

30. [QPACE: power-efficient parallel architecture based on IBM
    PowerXCell 8i](http://link.springer.com/content/pdf/10.1007/s00450-010-0122-4)

31.

32. [Toshiba 發表 SpursEngine
    圖形協同處理晶片](http://chinese.engadget.com/2008/04/08/toshiba-releases-spursengine-graphics-co-processor-for-testing/)

33. [东芝重量级ZF平板电视采用Cell处理器](http://www.hardspell.com/doc/hard/81085.htm)

34. [丽台发布Cell核心视频加速卡](http://news.mydrivers.com/1/116/116171.htm)

35. [Toshiba 的
    SpursEngine](http://www.techbang.com/posts/175-toshibas-spursengine)

36.

37. [東芝Cell電視可將數字視頻轉換為3D視頻](http://financenews.sina.com/sinacn/304-000-106-109/2010-01-07/00481227166.html)

38. [商刊：东芝欲借Cell电视机提高品牌知名度](http://news.ccidnet.com/art/1032/20091007/1902565_1.html)