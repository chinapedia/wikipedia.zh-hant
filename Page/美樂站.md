**美樂站**（英文：**Melody Garden
Stop**）是[輕鐵的車站之一](../Page/香港輕鐵.md "wikilink")，代號010，屬單程車票[第1收費區](../Page/輕鐵第1收費區.md "wikilink")，共有2個月台，共有3條輕鐵路綫途經此站；車站名稱來自旁邊之[美樂花園](../Page/美樂花園.md "wikilink")。

## 趣文

本站曾經為港產電影一蚊雞保鑣取景

## 車站結構

### 車站樓層

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td></td>
<td><p><a href="../Page/美樂花園.md" title="wikilink">美樂花園</a></p></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p> 往<a href="../Page/兆康站_(輕鐵).md" title="wikilink">兆康</a>（<a href="../Page/蝴蝶站.md" title="wikilink">蝴蝶</a>）|}}</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p> 往<a href="../Page/屯門碼頭站.md" title="wikilink">屯門碼頭</a>（總站）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>湖翠路、<a href="../Page/蝴蝶邨.md" title="wikilink">蝴蝶邨</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 車站周邊

  - [蝴蝶邨](../Page/蝴蝶邨.md "wikilink")
  - [美樂花園](../Page/美樂花園.md "wikilink")
  - [蝴蝶灣泳灘](../Page/蝴蝶灣.md "wikilink")
  - [蝴蝶灣公園](../Page/蝴蝶灣公園.md "wikilink")
  - [屯門公眾騎術學校](../Page/屯門公眾騎術學校.md "wikilink")

## 鄰接車站

## 接駁交通

<div class="NavFrame collapsed" style="clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

接駁交通列表

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [港鐵巴士](../Page/港鐵巴士.md "wikilink")（[八達通免費轉乘優惠](../Page/八達通.md "wikilink")）：
      - [K52](../Page/港鐵巴士K52綫.md "wikilink") -
      - [506](../Page/港鐵巴士506綫.md "wikilink") -

</div>

</div>

## 外部連結

  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵街道圖](http://www.mtr.com.hk/archive/ch/services/maps/01.gif)
  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵行車時間表](http://www.mtr.com.hk/chi/lr_bus/schedule/schedule_index.html)

[Category:蝴蝶灣](../Category/蝴蝶灣.md "wikilink")
[Category:以屋苑命名的香港輕鐵車站](../Category/以屋苑命名的香港輕鐵車站.md "wikilink")
[Category:建在填海/填塘地的港鐵車站](../Category/建在填海/填塘地的港鐵車站.md "wikilink")
[Category:1988年启用的铁路车站](../Category/1988年启用的铁路车站.md "wikilink")
[Category:屯門區鐵路車站](../Category/屯門區鐵路車站.md "wikilink")