**南羽生站**（）是一個位於[埼玉縣](../Page/埼玉縣.md "wikilink")[羽生市南羽生一丁目](../Page/羽生市.md "wikilink")，屬於[東武鐵道](../Page/東武鐵道.md "wikilink")[伊勢崎線的](../Page/伊勢崎線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。車站編號是**TI
06**。

## 歷史

  - 1903年（明治36年）9月13日 - **須影站**開業。
  - 1968年（昭和43年）9月1日 - 改稱**南羽生站**。

## 車站結構

[側式月台](../Page/側式月台.md "wikilink")2面2線的地面車站。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_(TI)_symbol.svg" title="fig:Tobu_Isesaki_Line_(TI)_symbol.svg">Tobu_Isesaki_Line_(TI)_symbol.svg</a> 伊勢崎線</p></td>
<td><p>下行</p></td>
<td><p><a href="../Page/館林站.md" title="wikilink">館林</a>、<a href="../Page/足利市站.md" title="wikilink">足利市</a>、<a href="../Page/太田站_(群馬縣).md" title="wikilink">太田方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/久喜站.md" title="wikilink">久喜</a>、<a href="../Page/東武動物公園站.md" title="wikilink">東武動物公園</a>、<br />
<span style="font-size:small"><a href="https://zh.wikipedia.org/wiki/File:Tobu_Skytree_Line_(TS)_symbol.svg" title="fig:Tobu_Skytree_Line_(TS)_symbol.svg">Tobu_Skytree_Line_(TS)_symbol.svg</a> <a href="../Page/伊勢崎線.md" title="wikilink">東武晴空塔線</a></span> <a href="../Page/北千住站.md" title="wikilink">北千住</a>、<a href="../Page/東京晴空塔站.md" title="wikilink">東京晴空塔</a>、<a href="../Page/淺草站.md" title="wikilink">淺草方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 使用情況

2016年度1日平均上下車人次為**3,857人**\[1\]。

近年的1日平均上下車人次、上車人次的推移如下表｡

<table>
<caption>各年度1日平均上下、上車人次[2]</caption>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>1日平均<br />
上下車人次[3]</p></th>
<th><p>1日平均<br />
上車人次[4]</p></th>
<th><p>來源</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1998年（平成10年）</p></td>
<td><p>2,814</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年（平成11年）</p></td>
<td><p>2,801</p></td>
<td><p>1,443</p></td>
<td><p>[5]</p></td>
</tr>
<tr class="odd">
<td><p>2000年（平成12年）</p></td>
<td><p>2,977</p></td>
<td><p>1,533</p></td>
<td><p>[6]</p></td>
</tr>
<tr class="even">
<td><p>2001年（平成13年）</p></td>
<td><p>3,111</p></td>
<td><p>1,581</p></td>
<td><p>[7]</p></td>
</tr>
<tr class="odd">
<td><p>2002年（平成14年）</p></td>
<td><p>3,160</p></td>
<td><p>1,606</p></td>
<td><p>[8]</p></td>
</tr>
<tr class="even">
<td><p>2003年（平成15年）</p></td>
<td><p>3,172</p></td>
<td><p>1,610</p></td>
<td><p>[9]</p></td>
</tr>
<tr class="odd">
<td><p>2004年（平成16年）</p></td>
<td><p>3,224</p></td>
<td><p>1,636</p></td>
<td><p>[10]</p></td>
</tr>
<tr class="even">
<td><p>2005年（平成17年）</p></td>
<td><p>3,190</p></td>
<td><p>1,619</p></td>
<td><p>[11]</p></td>
</tr>
<tr class="odd">
<td><p>2006年（平成18年）</p></td>
<td><p>3,312</p></td>
<td><p>1,681</p></td>
<td><p>[12]</p></td>
</tr>
<tr class="even">
<td><p>2007年（平成19年）</p></td>
<td><p>3,445</p></td>
<td><p>1,758</p></td>
<td><p>[13]</p></td>
</tr>
<tr class="odd">
<td><p>2008年（平成20年）</p></td>
<td><p>3,522</p></td>
<td><p>1,785</p></td>
<td><p>[14]</p></td>
</tr>
<tr class="even">
<td><p>2009年（平成21年）</p></td>
<td><p>3,596</p></td>
<td><p>1,819</p></td>
<td><p>[15]</p></td>
</tr>
<tr class="odd">
<td><p>2010年（平成22年）</p></td>
<td><p>3,703</p></td>
<td><p>1,871</p></td>
<td><p>[16]</p></td>
</tr>
<tr class="even">
<td><p>2011年（平成23年）</p></td>
<td><p>3,802</p></td>
<td><p>1,902</p></td>
<td><p>[17]</p></td>
</tr>
<tr class="odd">
<td><p>2012年（平成24年）</p></td>
<td><p>3,806</p></td>
<td><p>1,898</p></td>
<td><p>[18]</p></td>
</tr>
<tr class="even">
<td><p>2013年（平成25年）</p></td>
<td><p>3,822</p></td>
<td><p>1,911</p></td>
<td><p>[19]</p></td>
</tr>
<tr class="odd">
<td><p>2014年（平成26年）</p></td>
<td><p>3,775</p></td>
<td><p>1,887</p></td>
<td><p>[20]</p></td>
</tr>
<tr class="even">
<td><p>2015年（平成27年）</p></td>
<td><p>3,885</p></td>
<td><p>1,943</p></td>
<td><p>[21]</p></td>
</tr>
<tr class="odd">
<td><p>2016年（平成28年）</p></td>
<td><p>3,857</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 相鄰車站

  - [Tōbu_Tetsudō_Logo.svg](https://zh.wikipedia.org/wiki/File:Tōbu_Tetsudō_Logo.svg "fig:Tōbu_Tetsudō_Logo.svg")
    東武鐵道
    [Tobu_Isesaki_Line_(TI)_symbol.svg](https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_\(TI\)_symbol.svg "fig:Tobu_Isesaki_Line_(TI)_symbol.svg")
    伊勢崎線
      -

        區間急行、區間準急（平日只有1班上行列車）、普通

          -
            [加須](../Page/加須站.md "wikilink")（TI-05）－**南羽生（TI-06）**－[羽生](../Page/羽生站.md "wikilink")（TI-07）

## 參考資料

  - 埼玉縣統計年鑑

## 外部連結

  - [東武鐵道 南羽生站](http://www.tobu.co.jp/station/info/1607.html)

[NamiHanyuu](../Category/日本鐵路車站_Mi.md "wikilink")
[Category:埼玉縣鐵路車站](../Category/埼玉縣鐵路車站.md "wikilink")
[Category:伊勢崎線車站](../Category/伊勢崎線車站.md "wikilink")
[Category:羽生市](../Category/羽生市.md "wikilink")
[Category:1903年啟用的鐵路車站](../Category/1903年啟用的鐵路車站.md "wikilink")
[Category:1903年日本建立](../Category/1903年日本建立.md "wikilink")

1.  [駅情報（乗降人員）](http://www.tobu.co.jp/corporation/rail/station_info/)  -
    東武鉄道
2.  [統計はにゅう](http://www.city.hanyu.lg.jp/categories/bunya/shisei/gyosei/tokei/)
    - 羽生市
3.  [関東交通広告協議会レポート](http://www.train-media.net/report/)
4.  [埼玉県統計年鑑](http://www.pref.saitama.lg.jp/site/a310/)
5.  [埼玉県統計年鑑（平成12年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20091230-813.html)
6.  [埼玉県統計年鑑（平成13年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20100105-832.html)
7.  [埼玉県統計年鑑（平成14年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20100105-852.html)
8.  [埼玉県統計年鑑（平成15年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20100106-872.html)
9.  [埼玉県統計年鑑（平成16年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20100106-892.html)
10. [埼玉県統計年鑑（平成17年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20100107-912.html)
11. [埼玉県統計年鑑（平成18年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20100107-934.html)
12. [埼玉県統計年鑑（平成19年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20100107-957.html)
13. [埼玉県統計年鑑（平成20年）](http://www.pref.saitama.lg.jp/a0206/a310/911-20100108-980.html)
14. [埼玉県統計年鑑（平成21年）](http://www.pref.saitama.lg.jp/a0206/a310/a310a200908.html)
15. [埼玉県統計年鑑（平成22年）](http://www.pref.saitama.lg.jp/a0206/a310/a310a201008.html)
16. [埼玉県統計年鑑（平成23年）](http://www.pref.saitama.lg.jp/a0206/a310/a310a201108.html)
17. [埼玉県統計年鑑（平成24年）](http://www.pref.saitama.lg.jp/a0206/a310/a310a201208.html)
18. [埼玉県統計年鑑（平成25年）](http://www.pref.saitama.lg.jp/a0206/a310/a310a201308.html)
19. [埼玉県統計年鑑（平成26年）](http://www.pref.saitama.lg.jp/a0206/a310/a310a201408.html)
20. [埼玉県統計年鑑（平成27年）](http://www.pref.saitama.lg.jp/a0206/a310/a2015ubbyutuusinn.html)
21. [埼玉県統計年鑑（平成28年）](http://www.pref.saitama.lg.jp/a0206/a310/a2016ubbyutuusinn.html)