[Gakushuin-seimon.JPG](https://zh.wikipedia.org/wiki/File:Gakushuin-seimon.JPG "fig:Gakushuin-seimon.JPG")
**学习院大学**
（[日文](../Page/日文.md "wikilink")：；[英文](../Page/英文.md "wikilink")：Gakushuin
University）是由[日本](../Page/日本.md "wikilink")“学校法人学习院”设置、位於[东京都](../Page/东京都.md "wikilink")[丰岛区内的一所](../Page/丰岛区.md "wikilink")[私立大学](../Page/私立大学.md "wikilink")，與學習院女子大學、學習院中學（高等科與中等科）、學習院幼稚園等機構同屬學校法人學習院所有。由于日本的大多数[皇族就读于此](../Page/日本皇室.md "wikilink")，所以学习院大学作为皇家大学而在日本有很高的知名度。学校的学生数相对说来人数较少，在1万人左右。

## 沿革

学习院最早源于1847年由[仁孝天皇在](../Page/仁孝天皇.md "wikilink")[京都御所内以朝廷贵族为对象而设立的教育机关](../Page/京都御所.md "wikilink")**学习所**。经历了[明治维新之后](../Page/明治维新.md "wikilink")，学习所于1876年改名为**[华族学校](../Page/華族_\(日本\).md "wikilink")**，在第二年的1877年改名为**学习院**。

[第二次世界大战前的学习院](../Page/第二次世界大战.md "wikilink")，不归属于[文部省](../Page/文部省.md "wikilink")，而是在[宫内省](../Page/宫内省.md "wikilink")（[宮內廳前身](../Page/宮內廳.md "wikilink")）的管理之下。贵族子弟原则上可以免费进入学习院就学；而[平民就学学习院则是极其有限的少数](../Page/平民.md "wikilink")，不仅学费自负，而且不能进幼儿园及高中等；这种貴族、平民之間的的差别对待情况，即使在1924年制度改革以后还继续保留着。

第二次世界大战以后，日本废除了“华族制度”；1949年，学习院也再生为私立的**学校法人学习院**，一般市民子弟与日本皇族子弟之间的入学待遇差别也被取消；这就使得在现在的学习院裡，无论是皇族还是平民都能并排座着进行听讲、学习。

## 成立

學習院之成立可以追溯至兩個源頭：

1.  [幕末時於](../Page/幕末.md "wikilink")[京都設立的](../Page/京都.md "wikilink")[公卿學校](../Page/公卿.md "wikilink")；
2.  由[華族會館中的](../Page/華族會館.md "wikilink")「勉學局」發展而成。

「學習院」一名的由來，最早可以追溯至公元[八世紀時期](../Page/八世紀.md "wikilink")，即日本之[平安時代](../Page/平安時代.md "wikilink")
，[嵯峨天皇](../Page/嵯峨天皇.md "wikilink")（809年-823年在位）之皇后[橘嘉智子](../Page/橘嘉智子.md "wikilink")（[檀林皇后](../Page/檀林皇后.md "wikilink")）為了[橘家之子弟而設立的私學](../Page/橘家.md "wikilink")。與現代學習院更有關連的是於1847年，[孝明天皇遵循仁孝天皇](../Page/孝明天皇.md "wikilink")（1817年-1846年在位）的遺志而於[御所](../Page/御所.md "wikilink")（[皇宮](../Page/皇宮.md "wikilink")）的[建春門外成立的](../Page/建春門.md "wikilink")「學習所」，當時是以皇族及[公家為對象的學校](../Page/公家.md "wikilink")（該校於[明治初與](../Page/明治.md "wikilink")[皇學所合併為京都府立中學](../Page/皇學所.md "wikilink")）。

學習院的實際制度以及組織則是發展自華族會館的勉學局。華族會館於1874年成立，目的是「居於四民之首的華族作為萬民之榜樣」，由於明治時期著重開明思想，因此要由華族帶頭去實踐「實學精神」、「海外留學」及「女子教育」，華族會館之設立緣由始於此。華族會館內細分為七局，各有所司，集教育及社交場所的功能一身。學習院即發展自華族會館中的勉學局，負有教育華族子弟的任務。

1876年，華族會館開始以華族學校為名籌辦，並由[明治天皇御賜位於現今東京都](../Page/明治天皇.md "wikilink")[神田錦町的](../Page/神田錦町.md "wikilink")8,000[坪土地](../Page/坪.md "wikilink")。於1877年正式成立，明治天皇親臨開校禮並命名為「學習院」，又撥下年經費十五萬[日圓](../Page/日圓.md "wikilink")
，其餘的經費則向各會員（即華族）收取。為了吸引高水平的師資，因此學習院的教師不限於華族，士族和平民皆可任學習院教職，而且有著比華族更高的工資，目的是以此提高教師的水準。

## 歷代院長

現任院長：[波多野敬雄](../Page/波多野敬雄.md "wikilink")

現任大學校長：[福井憲彦](../Page/福井憲彦.md "wikilink")

| 院長                                   | 在任期間        | 曾任職務                                                                                                                  | [爵位](../Page/爵位.md "wikilink") |
| ------------------------------------ | ----------- | --------------------------------------------------------------------------------------------------------------------- | ------------------------------ |
| [立花種恭](../Page/立花種恭.md "wikilink")   | 1877年-1884年 | [陸奧](../Page/陸奧.md "wikilink")[下手藩藩主](../Page/下手藩.md "wikilink")、外國奉行                                                 | 未叙爵                            |
| [谷干城](../Page/谷干城.md "wikilink")     | 1884年-1885年 | [土佐](../Page/土佐.md "wikilink")[高知藩藩士](../Page/高知藩.md "wikilink")、陸軍中將、[陸軍士官學校校長](../Page/陸軍士官學校_\(日本\).md "wikilink") | 子爵                             |
| [大鳥圭介](../Page/大鳥圭介.md "wikilink")   | 1886年-1888年 | [工部大學校長](../Page/工部大學.md "wikilink")                                                                                  | 未叙爵                            |
| [三浦梧楼](../Page/三浦梧楼.md "wikilink")   | 1888年-1892年 | [長州藩藩士](../Page/長州藩.md "wikilink")、陸軍中將                                                                               | 子爵                             |
| [岩倉具定](../Page/岩倉具定.md "wikilink")   | 1892年       | 帝室制度取調委員                                                                                                              | 公爵                             |
| [田中光顕](../Page/田中光顕.md "wikilink")   | 1892年-1895年 | 内閣書記官長（現內閣官房長官）、警視總監、宮內大臣                                                                                             | 子爵                             |
| [近衛篤麿](../Page/近衛篤麿.md "wikilink")   | 1895年-1904年 | 貴族院議長                                                                                                                 | 公爵                             |
| [菊池大麓](../Page/菊池大麓.md "wikilink")   | 1904年-1905年 | [東京大學校長](../Page/東京大學.md "wikilink")、數學學家                                                                             | 男爵                             |
| [山口鋭之助](../Page/山口鋭之助.md "wikilink") | 1906年-1907年 | [京都大學教授](../Page/京都大學.md "wikilink")、物理學家                                                                             |                                |
| [乃木希典](../Page/乃木希典.md "wikilink")   | 1907年-1912年 | 陸軍大將                                                                                                                  | 男爵                             |
| [大迫尚敏](../Page/大迫尚敏.md "wikilink")   | 1912年-1917年 | 陸軍大將                                                                                                                  | 男爵                             |
| [北条時敬](../Page/北条時敬.md "wikilink")   | 1917年-1920年 | [東北大學校長](../Page/東北大學.md "wikilink")、數學學家                                                                             |                                |
| [一戸兵衛](../Page/一戸兵衛.md "wikilink")   | 1920年-1922年 | 陸軍大將                                                                                                                  |                                |
| [福原鐐二郎](../Page/福原鐐二郎.md "wikilink") | 1922年-1929年 | 東北大學校長                                                                                                                |                                |
| [荒木寅三郎](../Page/荒木寅三郎.md "wikilink") | 1929年-1937年 | 京都大學校長、著名西醫                                                                                                           |                                |
| [野村吉三郎](../Page/野村吉三郎.md "wikilink") | 1937年-1939年 | 海軍大將                                                                                                                  |                                |
| [山梨勝之進](../Page/山梨勝之進.md "wikilink") | 1939年-1946年 | 海軍大將                                                                                                                  |                                |
| [安倍能成](../Page/安倍能成.md "wikilink")   | 1946年-1966年 | 文部大臣                                                                                                                  |                                |
| [麻生磯次](../Page/麻生磯次.md "wikilink")   | 1966年-1970年 |                                                                                                                       |                                |
| [櫻井和市](../Page/櫻井和市.md "wikilink")   | 1970年-1981年 |                                                                                                                       |                                |
| [磯部忠正](../Page/磯部忠正.md "wikilink")   | 1981年-1987年 |                                                                                                                       |                                |
| [内藤頼博](../Page/内藤頼博.md "wikilink")   | 1987年-1993年 |                                                                                                                       |                                |
| [島津久厚](../Page/島津久厚.md "wikilink")   | 1993年-2002年 |                                                                                                                       |                                |
| [田島義博](../Page/田島義博.md "wikilink")   | 2002年-2006年 |                                                                                                                       |                                |
| [波多野敬雄](../Page/波多野敬雄.md "wikilink") | 2006年-2014年 |                                                                                                                       |                                |
| [内藤政武](../Page/内藤政武.md "wikilink")   | 2014年-      |                                                                                                                       |                                |

## 其他

  - 2006年元旦，「[人間宣言](../Page/人間宣言.md "wikilink")」發布六十週年當天，日本[每日新聞發表了由學習院院史資料室保存的](../Page/每日新聞.md "wikilink")「人間宣言」的英文草擬資料以及日文譯本，還有當時的經過紀錄。文件表示了當時的學習院院長[山梨勝之進有參與其中](../Page/山梨勝之進.md "wikilink")，更記載了「人間宣言」的草擬是由[盟軍主導的](../Page/盟軍.md "wikilink")，解答了六十年來對於「人間宣言」草擬過程的種種問題。原文件由已過世的學習院事務官[淺野長光所保管](../Page/淺野長光.md "wikilink")，他去世後捐給學習院保存。
  - 二戰結束以前，在學習院進讀的學生不僅有日本皇族和[華族](../Page/華族_\(日本\).md "wikilink")，還包括了當時被日本吞併了的[朝鮮王族](../Page/李王家.md "wikilink")。朝鮮王族成員是以日本皇族的身份在學習院學習的。[琉球王族作為華族之一](../Page/第二尚氏.md "wikilink")，也在學習院學習。
  - 1882年，學習院脫離華族會館獨立，歸文部卿所管。但於1884年改為宮內省所管，為戰前唯一一所由宮內省管理的公立學校。
  - 戰前學習院只有幼稚園至高等科，雖然曾經於1893年嘗試開辦大學科，但於1905年廢止。
  - 2006年4月日本新學年開始時有五名日本皇族於學習院之下的學校就讀，包括2006年4月剛入讀學習院幼稚園的[敬宮愛子内親王](../Page/敬宮愛子内親王.md "wikilink")，另外有兩名就讀於高等科，其餘兩名分別於女子中等科及初等科就讀。

## 学部（大學部／本科）・学科

  - 法学部
      - 法学科
      - 政治学科
  - 经济学部
      - 经济学科
      - 经营学科
  - 文学部
      - 哲学科
      - 史学科
      - 日本语日本文学科
      - 英语英美文化学科
      - 法语圈文化学科
      - 德语圈文化学科
      - 心理学科
  - 理学部
      - 物理学科
      - 化学科
      - 数学科
      - 生命科学科

## 大学院（研究所／研究生院）

  - 人文科学研究科
  - 法学研究科
  - 政治学研究科
  - 经济学研究科
  - 经营学研究科
  - 自然科学研究科
  - 法科大学院

## 著名校友

### 皇室

  - [今上天皇](../Page/今上天皇.md "wikilink")[明仁](../Page/明仁.md "wikilink")
    （入讀時為繼宮明仁親王）
  - [常陸宮正仁親王](../Page/常陸宮正仁親王.md "wikilink") （入讀時為義宮正仁親王）
  - [島津貴子](../Page/島津貴子.md "wikilink") （入讀時為清宮貴子内親王）
  - [近衛甯子](../Page/近衛甯子.md "wikilink") （入讀時為甯子内親王）
  - [寬仁親王](../Page/寬仁親王.md "wikilink")
  - [桂宮宜仁親王](../Page/桂宮宜仁親王.md "wikilink") （入讀時為宜仁親王）
  - [千容子](../Page/千容子.md "wikilink") （入讀時為容子内親王）
  - [高圓宮憲仁親王](../Page/高圓宮憲仁親王.md "wikilink") （入讀時為憲仁親王）
  - [皇太子德仁親王](../Page/皇太子德仁.md "wikilink") （入讀時為浩宮德仁親王）
  - [秋篠宮文仁親王](../Page/秋篠宮文仁親王.md "wikilink") （入讀時為禮宮文仁親王）
  - [文仁親王妃紀子](../Page/秋篠宮妃紀子.md "wikilink") （入讀時為川嶋紀子）
  - [黑田清子](../Page/黑田清子.md "wikilink") （入讀時為紀宮清子內親王）
  - [彬子女王](../Page/彬子女王.md "wikilink")
  - [典子女王](../Page/典子女王.md "wikilink")
  - [爱新觉罗·慧生](../Page/爱新觉罗·慧生.md "wikilink")（满洲皇室）
  - [爱新觉罗嫮生](../Page/福永嫮生.md "wikilink")（满洲皇室）

### 政界

  - [吉田茂](../Page/吉田茂.md "wikilink") -
    第45、48－51期日本[内阁总理大臣](../Page/内阁总理大臣.md "wikilink")
  - [麻生太郎](../Page/麻生太郎.md "wikilink") - 第92期日本内阁总理大臣
  - [都丸哲也](../Page/都丸哲也.md "wikilink") -
    原東京都[保谷市](../Page/保谷市.md "wikilink")（现・[西东京市](../Page/西东京市.md "wikilink")）市長
  - [美田长彦](../Page/美田长彦.md "wikilink") -
    [埼玉县](../Page/埼玉县.md "wikilink")[三乡市长](../Page/三乡市.md "wikilink")
  - [岛村宜伸](../Page/岛村宜伸.md "wikilink") - 政治家
  - [野口忠直](../Page/野口忠直.md "wikilink") -
    东京都[府中市长](../Page/府中市_\(東京都\).md "wikilink")
  - [龟井久兴](../Page/龟井久兴.md "wikilink") -
    众议院议员（[自民党](../Page/自由民主黨_\(日本\).md "wikilink")）
  - [小川元](../Page/小川元.md "wikilink") -
    驻[智利大使](../Page/智利.md "wikilink")、原众议院议员
  - [黑田慶樹](../Page/黑田慶樹.md "wikilink") - 東京都職員，黑田清子（舊稱紀宮清子內親王）夫婿
  - [張燕卿](../Page/張燕卿.md "wikilink") -
    原[滿洲國實業總長](../Page/滿洲國.md "wikilink")（大臣）、外交大臣

### 商界

  - [中条高德](../Page/中条高德.md "wikilink") -
    [朝日啤酒名誉顾问](../Page/朝日啤酒.md "wikilink")
  - [户田寿一](../Page/户田寿一.md "wikilink") -
    [SECOM创业者](../Page/SECOM.md "wikilink")、董事最高顾问
  - [饭田亮](../Page/饭田亮.md "wikilink") - SECOM创业者、董事最高顾问
  - [古屋胜彦](../Page/古屋胜彦.md "wikilink") -
    [松屋董事长会长](../Page/松屋.md "wikilink")
  - [松下武义](../Page/松下武义.md "wikilink") -
    [德间书店总经理](../Page/德间书店.md "wikilink")、原[加州](../Page/加州.md "wikilink")[住友银行会长](../Page/住友银行.md "wikilink")
  - [森田清](../Page/森田清.md "wikilink") -
    [第一制药总经理](../Page/第一制药.md "wikilink")
  - [清水样二](../Page/清水样二.md "wikilink") - [TBS Radio &
    Communications,Inc董事长](../Page/TBS_Radio_&_Communications,Inc.md "wikilink")・[东京放送董事](../Page/东京放送.md "wikilink")
  - [福田贞男](../Page/福田贞男.md "wikilink") -
    [札幌啤酒董事顾问](../Page/札幌啤酒.md "wikilink")
  - [桥本晃明](../Page/桥本晃明.md "wikilink") -
    原[东洋水产总经理](../Page/东洋水产.md "wikilink")
  - [新町敏行](../Page/新町敏行.md "wikilink") -
    [日本航空总经理](../Page/日本航空.md "wikilink")
  - [浅野茂太郎](../Page/浅野茂太郎.md "wikilink") -
    [明治乳业总经理](../Page/明治乳业.md "wikilink")

### 研究学者

#### 諾貝爾獎

  - ：已故的[諾貝爾物理學獎候選人](../Page/諾貝爾物理學獎.md "wikilink")

#### 法学・政治学・社会学

  - [武者小路公秀](../Page/武者小路公秀.md "wikilink") -
    [中部大学中部高等学术研究所教授](../Page/中部大学.md "wikilink")・[大阪经济法科大学亚太研究中心所长](../Page/大阪经济法科大学.md "wikilink")・原[联合国大学副校长](../Page/联合国大学.md "wikilink")
  - [杉原泰雄](../Page/杉原泰雄.md "wikilink") -
    [一桥大学名誉教授](../Page/一桥大学.md "wikilink")

### 其他

  - [志賀直哉](../Page/志賀直哉.md "wikilink") - 作家
  - [吉村昭](../Page/吉村昭.md "wikilink") - 作家
  - [田中芳樹](../Page/田中芳樹.md "wikilink") - 作家
  - [宮崎駿](../Page/宮崎駿.md "wikilink") - 动画片导演
  - [小野洋子](../Page/小野洋子.md "wikilink") -
    艺术家、[約翰·藍儂的遗孀](../Page/约翰·列侬.md "wikilink")
  - [三木邦彦](../Page/三木邦彦.md "wikilink")　- 山梨県警警務部長
  - [井上麻里奈](../Page/井上麻里奈.md "wikilink")　- 聲優
  - [菅井友香](../Page/菅井友香.md "wikilink") - 女子組合
    [欅坂46](../Page/欅坂46.md "wikilink") 成員
  - [和田紗代子](../Page/和田紗代子.md "wikilink") -
    橫綱[白鵬的妻子](../Page/白鵬.md "wikilink")
  - [壁井有可子](../Page/壁井有可子.md "wikilink") - 小說家

## 交通

到本部目白校園的所在地，可以於[東京都內乘坐](../Page/東京都.md "wikilink")[JR山手線至](../Page/JR山手線.md "wikilink")[目白站](../Page/目白站.md "wikilink")，[學習院大學出口步行一分鐘即達側門](../Page/學習院大學.md "wikilink")。

## 关联学校

  - [学习院幼儿园](../Page/学习院幼儿园.md "wikilink")
  - [学习院小学](../Page/学习院小学.md "wikilink")
  - [学习院中学校](../Page/学习院中学校.md "wikilink")
  - [学习院女子中学校](../Page/学习院女子中学校.md "wikilink")
  - [学习院高等学校](../Page/学习院高等学校.md "wikilink")
  - [学习院女子高等学校](../Page/学习院女子高等学校.md "wikilink")
  - [学习院女子大学](../Page/学习院女子大学.md "wikilink")

## 參見

  - [國學院大學](../Page/國學院大學.md "wikilink")
  - [皇學館大學](../Page/皇學館大學.md "wikilink")

## 外部链接

  - [學校法人学习院](http://www.gakushuin.ac.jp/)

{{-}}

[学习院大学](../Category/学习院大学.md "wikilink")
[Category:東京都的大學](../Category/東京都的大學.md "wikilink")
[Category:日本私立大學](../Category/日本私立大學.md "wikilink")
[Category:1949年創建的教育機構](../Category/1949年創建的教育機構.md "wikilink")
[Category:登錄有形文化財](../Category/登錄有形文化財.md "wikilink")