**人民广场街道**，是[中华人民共和国原](../Page/中华人民共和国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[黄浦区下属的一个](../Page/黄浦区_\(2000–2011\).md "wikilink")[街道](../Page/街道_\(行政区划\).md "wikilink")，面积1.28平方公里，人口30,368人（2005年）。街道办事处驻[大沽路](../Page/大沽路.md "wikilink")123号。已于2007年撤销，并入[南京东路街道](../Page/南京东路街道.md "wikilink")。由于[人民广场地区以及](../Page/人民广场.md "wikilink")[上海市人民政府位于本街道内](../Page/上海市人民政府.md "wikilink")，使得本街道成为上海市政治中心之一。

## 历史沿革

1988年8月，撤销牯岭路、龙门路2个街道，设立人民广场街道，以[人民广场命名](../Page/人民广场.md "wikilink")。辖区东起[西藏中路](../Page/西藏中路.md "wikilink")、[西藏南路](../Page/西藏南路.md "wikilink")，与[南京东路街道和](../Page/南京东路街道.md "wikilink")[金陵东路街道为邻](../Page/金陵东路街道.md "wikilink")；西至[成都北路](../Page/成都北路.md "wikilink")，与[静安区接壤](../Page/静安区.md "wikilink")；南到[金陵西路](../Page/金陵西路.md "wikilink")、[金陵中路](../Page/金陵中路.md "wikilink")，与[卢湾区分界](../Page/卢湾区.md "wikilink")；北沿[北京西路南侧](../Page/北京西路.md "wikilink")，与[南京东路街道相毗](../Page/南京东路街道.md "wikilink")。2007年，经[上海市人民政府批准](../Page/上海市人民政府.md "wikilink")\[1\]，同意调整黄浦区部分街道行政区划。2月28日，《上海市黄浦区人民政府关于调整黄浦区部分街道行政区划的通告》\[2\]，撤销人民广场街道，并入[南京东路街道](../Page/南京东路街道.md "wikilink")。

## 行政区划

人民广场街道下辖8个[居民委员会](../Page/居民委员会.md "wikilink")：长江居委会、新中居委会、江阴居委会、均乐居委会、新昌居委会、振兴居委会、定兴居委会、顺天村居委会。

## 参考文献

## 外部链接

  - [黄浦区志·人民广场街道](http://www.shtong.gov.cn/newsite/node2/node4/node2249/huangpu/node34381/node34400/node34402/userobject1ai19218.html)

## 参见

  - [黄浦区 (2000年–2011年)](../Page/黄浦区_\(2000年–2011年\).md "wikilink")

{{-}}

[Category:上海市已撤销街道 (行政区划)](../Category/上海市已撤销街道_\(行政区划\).md "wikilink")
[Category:黄浦区行政区划史](../Category/黄浦区行政区划史.md "wikilink")
[Category:上海人民广场](../Category/上海人民广场.md "wikilink")
[Category:1988年建立的行政區劃](../Category/1988年建立的行政區劃.md "wikilink")
[Category:2007年废除的乡级行政区划](../Category/2007年废除的乡级行政区划.md "wikilink")

1.  沪府\[2007\]8号
2.  黄府发\[2007\]7号