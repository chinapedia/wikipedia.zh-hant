**1-丙醇**（*Propan-1-ol*）是一種有三個[碳原子的](../Page/碳.md "wikilink")[醇類](../Page/醇.md "wikilink")[有機化合物](../Page/有機化合物.md "wikilink")。
簡單的化學式為[C](../Page/碳.md "wikilink")<sub>3</sub>[H](../Page/氫.md "wikilink")<sub>7</sub>[OH](../Page/羟.md "wikilink")。分子式為CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>OH，依按[IUPAC命名法稱作丙](../Page/IUPAC命名法.md "wikilink")-1-醇。是[一氧化碳和](../Page/一氧化碳.md "wikilink")[氫合成](../Page/氫.md "wikilink")[甲醇時的副產物](../Page/甲醇.md "wikilink")。其键线式为[Propan-1-ol.svg](https://zh.wikipedia.org/wiki/File:Propan-1-ol.svg "fig:Propan-1-ol.svg")。

## 物理化學特性

具有[醇的通性](../Page/醇.md "wikilink")。1-丙醇在[室溫及](../Page/室溫.md "wikilink")[常壓下](../Page/常壓.md "wikilink")，是無色的透明液體，有香味。

### 毒性資料

  - [LD50](../Page/LD50.md "wikilink")：1870 mg/kg（大鼠经口）；5040
    mg/kg（[兔经皮](../Page/兔.md "wikilink")）
  - [LC50](../Page/LC50.md "wikilink")：48000
    mg/m3（[小鼠](../Page/小鼠.md "wikilink")[吸入](../Page/吸入.md "wikilink")）

## 用途

在日常用途中，丙醇可作化學溶劑，或作清潔物品用。
[Propanol_reactions.png](https://zh.wikipedia.org/wiki/File:Propanol_reactions.png "fig:Propanol_reactions.png")

[Category:醇](../Category/醇.md "wikilink")
[Category:醇类溶剂](../Category/醇类溶剂.md "wikilink")
[Category:三碳有机物](../Category/三碳有机物.md "wikilink")