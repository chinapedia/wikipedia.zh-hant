**聯俄容共**是1927年[吳稚暉首先使用後約定俗成](../Page/吳稚暉.md "wikilink")，[中國共產黨稱](../Page/中國共產黨.md "wikilink")**聯俄聯共**，是[孫文晚年所推動](../Page/孫文.md "wikilink")，引入[蘇聯政治訓練](../Page/蘇聯.md "wikilink")、軍事及財務的外援以協助[中國國民黨的](../Page/中國國民黨.md "wikilink")[國民政府政策](../Page/國民政府.md "wikilink")。\[1\]\[2\]

由於中國國民黨[軍事力量不足以反抗当时](../Page/軍事.md "wikilink")[北洋政府](../Page/北洋政府.md "wikilink")，兩次保護《[中華民國臨時約法](../Page/中華民國臨時約法.md "wikilink")》的[護法運動均以失敗告終](../Page/護法運動.md "wikilink")，孫文認為必須建造自己的[軍隊](../Page/軍隊.md "wikilink")。在[西方国家拒絕給予孫文援助情況下](../Page/西方国家.md "wikilink")，新成立的[蘇俄却宣布废除前](../Page/蘇俄.md "wikilink")[沙俄与中国部分](../Page/沙俄.md "wikilink")[不平等条约](../Page/不平等条约.md "wikilink")，透過中國國民黨发展起组织输出[共产](../Page/共产.md "wikilink")[革命](../Page/革命.md "wikilink")。1922年8月，[越飛到](../Page/越飛.md "wikilink")[北京任苏联駐華全權代表](../Page/北京.md "wikilink")，首先致函正在[洛阳军事实力最强的](../Page/洛阳.md "wikilink")[吴佩孚将军](../Page/吴佩孚.md "wikilink")，希望建立合作关系，但遭到吴的拒绝。

## 孫對蘇聯之態度

1923年1月26日，孫和蘇聯特使越飛發表聯合聲明\[3\]。越飛在[上海與](../Page/上海.md "wikilink")[孫中山會面](../Page/孫中山.md "wikilink")，與孫中山會面後發表“[孫文越飛聯合宣言](../Page/孫文越飛聯合宣言.md "wikilink")”（[孫越宣言](../Page/孫越宣言.md "wikilink")），開始國民黨與蘇聯及[中國共產黨](../Page/中國共產黨.md "wikilink")（[共產國際的中國分部](../Page/第三國際.md "wikilink")）的合作關係。孫認為蘇維埃制度不能引入中國，中國最大與最迫切問題，乃在完成全國家統一，與獲得完全國家獨立\[4\]。蘇俄表示，要廢除沙皇時期與中國簽訂之所有不平等條約，外蒙是中國領土一部分，不能與中國分離\[5\]。在孫越宣言中，苏联承认中国对外蒙古的[主权](../Page/主权.md "wikilink")，承诺不在[中国进行共产革命](../Page/中国.md "wikilink")。孫根據馬克思理論认为共产主义制度不适合當時中国国情。孫文與蘇俄的關係才日益密切。

## 經過

1923年，孫文與[共產國際合作](../Page/共產國際.md "wikilink")，將大本營遷回[廣州](../Page/廣州市.md "wikilink")，對抗[北洋政府](../Page/北洋政府.md "wikilink")。列強多不支持孫，孫僅獲得蘇聯支持以因應情勢。孫在改組中國國民黨同時，實行「聯俄容共」。蘇俄給予孫大量[武器和財政援助](../Page/武器.md "wikilink")，並派出軍事及政治顧問[鮑羅廷幫助孫建軍北伐](../Page/鮑羅廷.md "wikilink")。隔年，[黃埔軍校成立](../Page/黃埔軍校.md "wikilink")。在苏联的影响下，孫文力排眾議允許[共產黨員以個人身份加入中國國民黨](../Page/中國共產黨.md "wikilink")。史稱“聯俄容共”，中國共產黨在後來將其稱爲[第一次國共合作](../Page/第一次國共合作.md "wikilink")。中國共產黨方面日後單方宣稱孫文政策是「[聯俄聯共扶助農工三大政策](../Page/聯俄聯共.md "wikilink")」（毛澤東提出的[新三民主義或蘇聯人所說](../Page/新三民主義.md "wikilink")[三大政策](../Page/三大政策.md "wikilink")），而這與中國國民黨人對“容共”政策理解並不一樣：中國國民黨人認為，孫允許中國共產黨員以個人名義加入中國國民黨是真，但其至始至終只有一個[三民主義](../Page/三民主義.md "wikilink")，即民族、民權、民生主義，無所謂新舊。

1923年6月，[中共三大确定全体中国共产党员以个人名义加入中国国民党](../Page/中共三大.md "wikilink")，与中国国民党建立“革命[统一战线](../Page/统一战线.md "wikilink")”方针。1924年1月20日至1月30日，孙在[广州召开](../Page/广州.md "wikilink")[中国国民党第一次全国代表大会](../Page/中国国民党第一次全国代表大会.md "wikilink")，选举出中国国民党中央执行委员会。在25名中央执行委员中，[谭平山](../Page/谭平山.md "wikilink")、[李大钊](../Page/李大钊.md "wikilink")、[于树德为中国共产党员](../Page/于树德.md "wikilink")；在17名候补委员中，[沈定一](../Page/沈定一.md "wikilink")、[林祖涵](../Page/林祖涵.md "wikilink")、[毛泽东](../Page/毛泽东.md "wikilink")、[于方舟](../Page/于方舟.md "wikilink")、[瞿秋白](../Page/瞿秋白.md "wikilink")、[韩麟符](../Page/韩麟符.md "wikilink")、[张国焘为中国共产党员](../Page/张国焘.md "wikilink")。在中国国民党中央党部担任重要职务的中国共产党员有：组织部长谭平山、农民部长林祖涵、宣传部代理部长毛泽东等。[中國國民黨中央執行委員會常务委员共三人](../Page/中國國民黨中央執行委員會.md "wikilink")，[廖仲恺](../Page/廖仲恺.md "wikilink")、[戴季陶为中国国民党员](../Page/戴季陶.md "wikilink")，谭平山为中共党员。随后，全国大部分地区以中国共产党员和[國民黨左派親共親蘇勢力为骨干](../Page/國民黨左派.md "wikilink")，改组或建立了中国国民党各级党部。

## 結果

[1927_Chiang_Kai-shek_and_Soviet_advisor_at_Whampoa_Military_Academy.jpg](https://zh.wikipedia.org/wiki/File:1927_Chiang_Kai-shek_and_Soviet_advisor_at_Whampoa_Military_Academy.jpg "fig:1927_Chiang_Kai-shek_and_Soviet_advisor_at_Whampoa_Military_Academy.jpg")（左二）與蔣介石（左三）在[黃埔軍校](../Page/黃埔軍校.md "wikilink")，1927年\]\]

孫逝世后，中國共產黨人在中國國民黨内部發展迅速，控制中國國民黨諸多重要職務，引起中國國民黨内激烈政治分歧，造成[寧漢分裂](../Page/寧漢分裂.md "wikilink")。在[蔣中正率領](../Page/蔣中正.md "wikilink")[國民革命軍佔領](../Page/國民革命軍.md "wikilink")[上海後](../Page/上海.md "wikilink")，[鲍罗廷秘密策动](../Page/米哈伊尔·馬尔科维奇·鲍罗廷.md "wikilink")[郭松龄反对](../Page/郭松龄.md "wikilink")[张作霖](../Page/张作霖.md "wikilink")。张蒋两人1926年秋天就开始秘密接触，两人分别派[唐生智和](../Page/唐生智.md "wikilink")[杨宇霆作为代表](../Page/杨宇霆.md "wikilink")，展开秘密会晤，协议共同驱逐[共产国际势力](../Page/共产国际.md "wikilink")，因此“南京事件”后不到两周，张作霖便在4月6日得到[公使團同意](../Page/公使.md "wikilink")，派遣軍警突袭[北京的苏联大使馆](../Page/北京.md "wikilink")、遠東銀行、[中東鐵路辦公處](../Page/中東鐵路.md "wikilink")，逮捕[李大钊等中国共产党人](../Page/李大钊.md "wikilink")，并搜出共产国际发来大量指示、训令、颠覆材料（與[馮玉祥合作颠覆文件](../Page/馮玉祥.md "wikilink")、[紅槍會及煽動農民紀錄](../Page/紅槍會.md "wikilink")、中共文件等，「[蘇聯陰謀文證彙編](../Page/蘇聯陰謀文證彙編.md "wikilink")」）和武器弹药。其中一份训令内称“必须设定一切办法，激动国民群众排斥外国人”，“不惜任何办法，甚至抢劫及多数惨杀亦可实行”\[6\]，證實蘇聯全面指揮顛覆合法的北洋当局的暴力、排外運動。4月12日，蒋在上海发动“[清黨](../Page/中国国民党清党.md "wikilink")”。蘇聯與中國共產黨則強烈譴責中國國民黨粗暴侵犯蘇聯使館尊嚴，並稱此事件「乃[帝國主義的挑撥](../Page/帝國主義.md "wikilink")，中國政府已淪為帝國主義者的工具」。4月19日，蘇聯召回北京駐華代辦及大使館職員\[7\]。

[武漢親共派首領](../Page/武漢.md "wikilink")[汪精衛在偶然獲得共產國際關於在顛覆中國政府的策略文件後](../Page/汪精衛.md "wikilink")，也開始[和平分共](../Page/和平分共.md "wikilink")。中國共產黨在上海、[廣州](../Page/廣州.md "wikilink")、[南昌等地組織起义](../Page/南昌.md "wikilink")（[南昌起義](../Page/南昌起義.md "wikilink")），失敗後進入農村地區發展，继续接受蘇俄援助，推動反[国民党政府與](../Page/国民党政府.md "wikilink")[土地革命的活动](../Page/土地革命.md "wikilink")。中國國民黨聯俄容共政策到此暫停。

## 参考文献

## 外部連結

  - 狭间直树：〈[武汉时期国共两党关系与孙中山思想](http://www.nssd.org/articles/Article_Read.aspx?id=1003369917)〉。
  - [何為國共合作？何為新三民主義？](https://web.archive.org/web/20111016125201/http://www.huanghuagang.org/hhgMagazine/issue13/big5/42.htm)

## 參見

  - [國共關係](../Page/國共關係.md "wikilink")
  - [四一二事變](../Page/四一二事變.md "wikilink")
  - [中国国民党清党](../Page/中国国民党清党.md "wikilink")
  - [武装保卫苏联](../Page/武装保卫苏联.md "wikilink")
  - [反共抗俄](../Page/反共抗俄.md "wikilink")

{{-}}

[Category:国共关系](../Category/国共关系.md "wikilink")
[Category:中華民國與蘇聯關係](../Category/中華民國與蘇聯關係.md "wikilink")
[Category:中华民国大陆时期政府](../Category/中华民国大陆时期政府.md "wikilink")
[Category:中華民國接受的外國援助](../Category/中華民國接受的外國援助.md "wikilink")
[Category:孫中山](../Category/孫中山.md "wikilink")

1.

2.

3.  孫穗芳著，《我的祖父孫中山》下集，台北：禾馬文化出版，1995年4月，ISBN 978-957-799-020-4

4.
5.
6.  Keiji Furaya, Chiang Kai-Shek His Life and Times, translated by
    Chun-Ming Chang, (St.John’s Universtity 1981)p.189

7.  郭廷以，俄帝侵略中國簡史，1985年6月，文海出版社，第65頁