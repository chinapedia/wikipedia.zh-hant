**关节炎**是全世界最常见的[慢性疾病之一](../Page/慢性疾病.md "wikilink")，主要是关节的[软骨退化或者](../Page/软骨.md "wikilink")[结缔组织](../Page/结缔组织.md "wikilink")[发炎](../Page/发炎.md "wikilink")，導致关节疼痛從而干擾关节的正常運動就叫关节炎，總共有一百多個種類\[1\]。导致关节炎的原因很多，引起关节损伤也各有不同。全世界约有3.55亿关节炎患者，其中在[中國就占了一億以上](../Page/中华人民共和国.md "wikilink")\[2\]。在[美国](../Page/美国.md "wikilink")，每五个人中就有一人出现关节炎的疼痛與不适；而在[亚洲](../Page/亚洲.md "wikilink")，每六個人中就有一個人在人生某個時刻罹患此病。关节炎並不是[老年人獨有的疾病](../Page/老年.md "wikilink")，它可以影響包括[兒童在内的各个年齡層](../Page/兒童.md "wikilink")\[3\]。

## 概述

關節炎可以發生在[背](../Page/背.md "wikilink")、[颈](../Page/颈.md "wikilink")、[膝盖](../Page/膝盖.md "wikilink")、[肩关节](../Page/肩关节.md "wikilink")、[手](../Page/手.md "wikilink")、[髋关节](../Page/髋关节.md "wikilink")、[足踝](../Page/足踝.md "wikilink")\[4\]。很多的關節炎和人體老化有關聯，六十歲以上人群会有某种類型的关节炎但年輕人一樣會患關節炎\[5\]。有一百多個種類多见的是[骨性關節炎](../Page/骨性關節炎.md "wikilink")、类风湿性、风湿性、化脓性关节炎、外伤性骨性关节炎、自身免疫性關節炎，僵直性關節炎也是關節炎

根據病癥與患病時間可分成急性與慢性這兩種，急性关节炎為突發疾病，病患的关节會發紅變腫且痛热并會發生功能障碍與全身发热；慢性关节炎則是顯示出关节肿痛和[畸形](../Page/畸形.md "wikilink")、不同水平的功能障碍，早期关节炎病变只侵擾关节[滑膜](../Page/滑膜.md "wikilink")，如果到了晚期关节软骨和[骨质都可能會出現变化甚至被破坏](../Page/骨质.md "wikilink")\[6\]。

最常見的骨性關節炎和[类风湿性關節炎的并發癥不相同](../Page/类风湿性關節炎.md "wikilink")，骨性關節炎會并发肢体关节活动障礙，嚴重的時候会出現肢体内翻、屈曲挛缩畸形，最終導致關節殘疾。而类风湿性关节炎如果長時間在床上休養又或者[激素的服用時間過長可導致](../Page/激素.md "wikilink")[免疫功能下降而出现一些并发症](../Page/免疫功能.md "wikilink")，主要會有[肺炎](../Page/肺炎.md "wikilink")、[泌尿系统感染](../Page/泌尿系统.md "wikilink")、柯兴氏综合征、口腔溃疡、[传染病等](../Page/传染病.md "wikilink")\[7\]。

## 症狀

關節炎的症狀非常多樣，主要出現受累关节發红、肿、热、痛與功能障碍，身體检查可以觀察到病变关节出現異常現象，像是关节局部[皮肤发红和温度發生变化](../Page/皮肤.md "wikilink")、周围[软组织發腫發漲](../Page/软组织.md "wikilink")、关节腔积水、用手指按病人的关节局部可產生[疼痛現象](../Page/疼痛.md "wikilink")，如果严重的會有关节畸形、关节活动範圍變小等\[8\]。

其他臨床現象還有肌肉酸痛、面部红斑、無力、[发热](../Page/发热.md "wikilink")、尿路刺激、咽痛、复发性[口腔溃疡](../Page/口腔溃疡.md "wikilink")、[腹泻](../Page/腹泻.md "wikilink")、眼口干燥，有的甚至會出現[贫血](../Page/贫血.md "wikilink")、肾發炎、浆膜發炎等，這些現象可以單獨出現也可同時出現，這些現象為醫生判斷疾病的重要證據\[9\]。

'''常見種類症狀\[10\]

|         |                                                                                    |
| ------- | ---------------------------------------------------------------------------------- |
| 风湿性关节炎  | 游走性关节痛、發肿和发热及其它风湿热出現的現象                                                            |
| 类风湿性关节炎 | 一開始关节也出現红、肿、痛和活动受阻，時間長了关节畸形或者强直                                                    |
| 外伤性关节炎  | 关节發肿、發痛和活動障碍，多發生在[青少年和](../Page/青少年.md "wikilink")[運動員](../Page/運動員.md "wikilink") |
| 骨性关节炎   | 关节疼痛、僵硬（稍微運動後感到疼痛變輕）嚴重的會發生关节肿胀、肌肉萎缩等                                               |
| 化脓性关节炎  | 主要是局部红、肿、痛、热與功能障碍和高热等全身[中毒現象](../Page/中毒.md "wikilink")                            |

## 病因

目前醫學界中對關節炎的產生原因不是了解的很清楚，有些专家相信[自由基是導致](../Page/自由基.md "wikilink")[退行性疾病的主謀](../Page/退行性疾病.md "wikilink")，關節炎是其中一種。

如果缺少必需[礦物質那](../Page/礦物質.md "wikilink")[胶原蛋白基质中容有滑液的囊便無法形成](../Page/胶原蛋白.md "wikilink")，滑液可以使[骨骼末端不至于受到相鄰骨骼與軟骨的沖擊](../Page/骨骼.md "wikilink")。

### 类风湿性

类风湿性关节炎為免疫系统疾病，病因目前不清楚還沒有根治方法。医学研究认为类风湿性关节炎和[遗传有關係](../Page/遗传.md "wikilink")，因為自身免疫系统失调，從而錯誤攻擊关节软骨和骨骼组织。病患全身关节滑膜常常会被侵犯而发炎，要長期服藥才能控制\[11\]。类风湿性常和受寒、受潮、外伤、精神刺激有關聯，這些因素可能是誘因而不是病因。

一些致病原像是[细菌](../Page/细菌.md "wikilink")、[病毒](../Page/病毒.md "wikilink")、[衣原体](../Page/衣原体.md "wikilink")、[螺旋体等都可導致不同动物出現类风湿性样病癥](../Page/螺旋体.md "wikilink")。临床也發現一些类风湿性发生在某些感染後，像是结核杆菌、奇异变形杆菌、[链球菌](../Page/链球菌.md "wikilink")、EB病毒、衣原体感染等。於病人的[血清或滑膜液里面觀察到相對应抗原之抗体提高](../Page/血清.md "wikilink")，但還沒理清致病的抗原成分\[12\]。感染因子可能在类风湿起病中有重要作用\[13\]。

### 骨性關節炎

骨性關節炎分為繼發性骨性关节炎和原发性骨性关节炎，继发性骨性关节炎局部病因基礎上因软骨退变而发生的骨性关节炎，導致原因有关节内骨折、关节[发育不良](../Page/发育不良.md "wikilink")、关节外畸形、关节不稳定、医源性、[肥胖超重](../Page/肥胖.md "wikilink")、過去有关节感染、关节过度使用、肌肉无力，骨性关节炎通常發病原因不是一種而是多種因素相互交織產生\[14\]。

原发性骨性关节炎致病原因不清楚，為生理性骨关节软骨退化，在体力劳动者和妇女上比較常見，通常和年龄、遗传、体质肥胖超重的老年人、免疫异常、软骨代谢和其他因素有關聯。也和骨内[静脉淤滞](../Page/静脉.md "wikilink")、骨内压作用、遗传有關係\[15\]。

## 检查

要做一般检查如观察病人走路姿態、面色、皮膚，如懷疑患有炎癥性關節炎則要細心檢查[指甲和頭部](../Page/指甲.md "wikilink")、肘膝伸侧是否有[银屑病](../Page/银屑病.md "wikilink")。還有关节檢查如紅斑活动范围、畸形软组织肿胀、关节渗液、骨性肿胀、骨擦音压痛肌萎缩等等\[16\]

[实验室检查需要和患者的](../Page/实验室.md "wikilink")[临床表现相符合](../Page/临床.md "wikilink")，因為整套的筛选实验检查沒有必要，實驗室檢查需要說明一個具體問題如最終结果應支持或推翻一個臨床結果，大多數的人只需要做些簡單的实验检查，而对所有人做急性反应检测是比較必要的，如果不尋常的升高則要考慮是炎症性关节炎。大部分的人需要做[血常规检查如果贫血說明患有系统性或炎症性疾病](../Page/血常规.md "wikilink")，但也可能是為继发抗炎药導致的的上[消化道出血](../Page/消化道.md "wikilink")\[17\]。

有炎症性关节炎的临床現象應該做[免疫学检查](../Page/免疫.md "wikilink")，不過没有关节炎的人也会有些低度阳性自身抗体，要與临床表现相結合否则“假阳”性结果常常会引起誤診，如症状倾向于骨关节炎那除了血沉和血常规外那就不需要做其他檢查否則會誤導\[18\]。

影像学检查如[B超](../Page/B超.md "wikilink")、[X光片](../Page/X光.md "wikilink")、[CT](../Page/CT.md "wikilink")、[MRI等](../Page/MRI.md "wikilink")\[19\]。

## 治療

  - 一般療法：經常休息要保护关节，避免劇烈和過度活動，严重損傷时要臥床休息支架固定防止畸形\[20\]

<!-- end list -->

  - 药物療法：[非類固醇消炎止痛藥](../Page/非類固醇消炎止痛藥.md "wikilink")（NSAID）能缓解痛楚抗發炎，[中草药内服和外部热敷](../Page/中草药.md "wikilink")、熏洗浸泡能緩解病癥延遲病程。关节內部注射[玻尿酸](../Page/透明质酸.md "wikilink")，這是靠它的流变特性用作粘弹性物质补充，作用是润滑关节保护关节软骨。在關節內注射[皮质激素类药物能於短期内缓解症状](../Page/皮质激素.md "wikilink")，但对软骨的损害隨著注射次數增多而增加\[21\]。治療關節炎的药可分成非特异性治疗药物（对症治疗）、特异性治疗药物（对因治疗）\[22\]

<!-- end list -->

  - 手術療法：骨关节炎晚期有畸形或持续性疼痛現象且生活不能自理的時候可以[手术治疗](../Page/手术.md "wikilink")，如果膝内翻畸形可以于胫骨上實行高位截骨术、髋关节炎晚期可實行截骨术等，按年級和職業與生活作息等可选用关节置换术髋关节置换术等\[23\]。
  - 物理治疗：[微波热疗是近年來國內外比較成功的理療手段](../Page/微波.md "wikilink")，能對一些藥物無法治療的部分起到效果\[24\]
  - 輔助治療：烤电、[推拿](../Page/推拿.md "wikilink")、[按摩](../Page/按摩.md "wikilink")、[针灸等等](../Page/针灸.md "wikilink")，雖沒有根本療效但起到鎮靜、松弛的作用\[25\]。

## 膏药穴位贴敷法

穴位贴敷疗法通过药物直接刺激穴位，并通过透皮吸收，使局部药物浓度明显高于其他部位，作用较为直接，其适应证遍及临床各科，“可与内治并行，而能补内治之不及”，对许多沉疴痼疾常能取得意想不到的显著功效。穴位贴敷疗法不经胃肠给药，无损伤脾胃之弊，治上不犯下，治下不犯上，治中不犯上下。即使在临床应用时出现皮肤过敏或水泡，亦可及时中止治疗，给予对症处理，症状很快就可消失，并可继续使用。穴位贴敷有许多较简单的药物配伍及制作，易学易用，不需特殊的医疗设备和仪器。无论是医生还是患者或家属，多可兼学并用，随学随用。穴位贴敷法所用药物除极少数是名贵药材外（如麝香），绝大多数为常见中草药，价格低廉，甚至有一部分来自于生活用品，如葱、姜、蒜、花椒等。且本法用药量很少，既能减轻患者的经济负担，又可节约大量药材。贴敷疗法集针灸和药物治疗之所长，所用药方配伍组成多来自于临床经验，经过了漫长岁月和历史的验证，疗效显著，且无创伤无痛苦，对惧针者，老幼虚弱之体，补泻难施之时，或不肯服药之人，不能服药之症，尤为适宜。
\[26\]

## 預防

多吃新鮮蔬果，攝取[維生素](../Page/維生素.md "wikilink")，[葡萄胺](../Page/葡萄胺.md "wikilink")、软骨素、鸡软骨能起到修復和再生的作用。肥胖也會使患關節炎的機會增加，經常運動及減少關節承重壓力（如游泳。關節炎患者避免强烈運動如跑步等，以减少骨头間的摩擦），多運動能保持關節彈性防止軟骨退化。

## 類型

  - [骨性關節炎](../Page/骨性關節炎.md "wikilink")（[退化性關節炎](../Page/退化性關節炎.md "wikilink")，osteoarthritis）
  - [類風濕性關節炎](../Page/類風濕性關節炎.md "wikilink")（rheumatoid arthritis）
  - [敗血性關節炎](../Page/敗血病關節炎.md "wikilink")（細菌性關節炎，septic arthritis）
  - [痛風性關節炎](../Page/痛風.md "wikilink")，英文為gout（學名：metabolic
    arthritis）及[假性痛風](../Page/假性痛風.md "wikilink")
  - [幼年特發性關節炎](../Page/幼年特發性關節炎.md "wikilink")（juvenile idiopathic
    arthritis）
  - [強直性脊柱炎](../Page/強直性脊柱炎.md "wikilink")（ankylosing spondylitis）

## 参考文献

## 外部連結

  - [非類固醇消炎止痛藥-香港醫院藥劑師學會](https://web.archive.org/web/20080929063552/http://www.derchk.org/modules/wfsection/viewarticles.php?category=92)

## 参见

  - [非類固醇消炎止痛藥](../Page/非類固醇消炎止痛藥.md "wikilink")（NSAID）

[Category:肌肉骨骼系统和结缔组织疾病](../Category/肌肉骨骼系统和结缔组织疾病.md "wikilink")
[關節炎](../Category/關節炎.md "wikilink")

1.

2.
3.
4.
5.
6.

7.

8.

9.
10.

11.

12.

13.
14.

15.
16.

17.
18.
19.
20.
21.
22.
23.
24.

25.
26. 孙六合, 曹健美. 浅谈穴位贴敷疗法\[J\]. 河南中医药学刊, 2001, (2).