《**藍宇**》（又名：《**北京故事**》）是[中國大陸作者筱禾的一部有關LGBT愛情題材的](../Page/中國大陸.md "wikilink")[小說](../Page/小說.md "wikilink")。該小說最初發表于網絡，后被導演關錦鵬改編為同名電影而走紅。\[1\]\[2\]
2002年，該小說推出正式文本版。

## 小說概況

## 情節簡介

## 影視改編

《藍宇》原本以《北京故事》為名義刊登在網絡上，後被[製片人张永宁相中](../Page/電影監製.md "wikilink")。\[3\]
在其推薦下，由導演關錦鵬執導了同名電影并獲得極大成功。\[4\]

## 參考資料

## 外部連結

  -
  -
  - [《藍宇》網絡版](http://bbs.jjwxc.com/showmsg.php?board=175&id=11)

[Category:LGBT文學](../Category/LGBT文學.md "wikilink")
[Category:北京市背景小說](../Category/北京市背景小說.md "wikilink")
[Category:2002年中国小说](../Category/2002年中国小说.md "wikilink")
[Category:1990年代背景作品](../Category/1990年代背景作品.md "wikilink")
[Category:中国LGBT题材作品](../Category/中国LGBT题材作品.md "wikilink")
[Category:改编成电影的中华人民共和国小说](../Category/改编成电影的中华人民共和国小说.md "wikilink")

1.

2.

3.
4.