**加拉廷縣**（**Gallatin County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州北部的一個縣](../Page/肯塔基州.md "wikilink")，北隔[俄亥俄河與](../Page/俄亥俄河.md "wikilink")[印地安納州相望](../Page/印地安納州.md "wikilink")。面積271平方公里，是該州面積最小的縣。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口7,870人。縣治[華沙](../Page/華沙_\(肯塔基州\).md "wikilink")
(Warsaw)。

建於1798年12月14日。縣名紀念代表[賓夕法尼亞州的](../Page/賓夕法尼亞州.md "wikilink")[聯邦眾議員](../Page/美國眾議院.md "wikilink")、[財政部長](../Page/美國財政部長.md "wikilink")、駐[英國](../Page/英國.md "wikilink")、[法國大使的](../Page/法國.md "wikilink")[阿伯特·加拉廷](../Page/阿伯特·加拉廷.md "wikilink")
（Albert Gallatin）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/肯塔基州行政區劃.md "wikilink")

1.  [Kentucky County
    History](http://freepages.genealogy.rootsweb.com/~harrisonrep/Census/kycounty.htm)