[SOS_morse_code.ogg](https://zh.wikipedia.org/wiki/File:SOS_morse_code.ogg "fig:SOS_morse_code.ogg")
[SOS_morse_code_from_a_flashlight.ogv](https://zh.wikipedia.org/wiki/File:SOS_morse_code_from_a_flashlight.ogv "fig:SOS_morse_code_from_a_flashlight.ogv")发送\]\]

**SOS**（[摩斯電碼](../Page/摩斯電碼.md "wikilink")：`···- - -
···`）是目前[国际通用的](../Page/国际.md "wikilink")[摩斯电码求救信号](../Page/摩斯电码.md "wikilink")。

## 历史

[Thesos.jpg](https://zh.wikipedia.org/wiki/File:Thesos.jpg "fig:Thesos.jpg")
，並于1906年11月3日成为国际标准（之前国际常用[CQD求救信号](../Page/CQD.md "wikilink")，但并未成为标准）。在1908年7月1日后被广泛采用。

之所以制订新遇难信号为SOS，是因为当时的抗[干扰技术很差](../Page/干扰.md "wikilink")，之前常用的CQD信号，由于发送码长，而且电码的符号不一致，而S与O是摩斯电码中少有的连续码而且对操作员来说较易使用。而SOS在之后也成为一个易记、实用的求救方式。

有记录的首次使用SOS求救信號的时间是在1909年8月11日，当时汽船阿拉帕霍号（S.S.
*Arapahoe*）驶至[北卡罗来纳州的](../Page/北卡罗来纳州.md "wikilink")后燃油殆尽，发出救援36小时后被救。\[1\]

其中一次较知名的利用SOS求救信號就是在1912年4月14日晚上英國郵輪[鐵達尼號将在北大西洋沉沒時](../Page/鐵達尼號.md "wikilink")。當時英國人多數是利用CQD求救信号，不過當晚也用了新的SOS求救信號，希望有郵輪來拯救他們。不過该求救信號直到第二天早上才被[加州人号收到](../Page/加州人号.md "wikilink")，因為[加州人号的电报员关了电报机下班睡觉去了](../Page/加州人号.md "wikilink")。而鐵達尼號的無線電首席官員杰克·菲利普起初一直在發送傳統的CQD遇難信號，直到下級無線電操作員哈羅德·布萊德建議他：「發送SOS吧，這是新的呼叫信號，這也可能是你最後的機會來發送它了！」然後菲利普才在傳統的CQD求救信號中夾雜SOS信號。

由於現代無線電通訊技術的發展，船舶與航空器越來越少使用摩斯電碼通訊，因此除了[業餘無線電外](../Page/業餘無線電.md "wikilink")，SOS已逐漸停用，改以國際海事衛星電子通信設備的（GMDSS）代替。

## SOS的含義

日常中，SOS通常被理解为：“**S**ave **O**ur **S**hip”（拯救我们的船）或“**S**ave **O**ur
**S**ouls”（拯救我们的灵魂）。但当初制定规范时是沒有意思的，純粹因摩斯電碼是全點及全橫而方便記載。

## SOS的摩斯電碼

S、O、S這三個字母的摩爾斯電碼分別是「`···`」、「`---`」、「`···`」，念為「滴滴滴」、「答答答」、「滴滴滴」，也就是「三短音」、「三長音」、「三短音」。

極為簡潔明確，即便是未經訓練的民眾，也可以輕易的利用手邊的器物發出這樣的信號，或是加以辨識。

## 实际应用

在1980年代的香港，由於社會開始富裕，不少人都愛在週末郊遊或露營。由於當時的通訊科技未有今日的發達，所以警方在《[警訊](../Page/警訊.md "wikilink")》節目裡教導市民通过利用开关[手電筒發出SOS訊號](../Page/手電筒.md "wikilink")（三短三长三短的光闪烁），以便一旦在山野間迷路時，亦懂得如何向周邊發出求救訊號。

## 参见

  - [CQD](../Page/CQD.md "wikilink")
  - [鐵達尼號](../Page/鐵達尼號.md "wikilink")
  - [Mayday](../Page/Mayday.md "wikilink")

## 注释

<references/>

[de:Morsecode\#SOS](../Page/de:Morsecode#SOS.md "wikilink")

[Category:無線電](../Category/無線電.md "wikilink")
[Category:灾难](../Category/灾难.md "wikilink")
[Category:摩斯電碼](../Category/摩斯電碼.md "wikilink")

1.