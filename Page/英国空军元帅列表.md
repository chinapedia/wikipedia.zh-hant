[UK-Air-OF10.svg](https://zh.wikipedia.org/wiki/File:UK-Air-OF10.svg "fig:UK-Air-OF10.svg")
[UK-Air-OF10-Flag.svg](https://zh.wikipedia.org/wiki/File:UK-Air-OF10-Flag.svg "fig:UK-Air-OF10-Flag.svg")
以下是英國**皇家空军元帅**列表。

## 皇家空军元帅

  - [休·特伦查德子爵](../Page/休·特伦查德.md "wikilink")（1927年1月1日）
  - [约翰·萨尔蒙德爵士](../Page/约翰·萨尔蒙德.md "wikilink")（1933年1月1日）
  - [爱德华八世](../Page/爱德华八世.md "wikilink")（1936年1月21日）
  - [乔治六世](../Page/乔治六世.md "wikilink")（1936年12月11日）
  - [爱德华·莱昂纳德·埃灵顿爵士](../Page/爱德华·埃灵顿.md "wikilink")（1937年1月1日）
  - [西里尔·内维尔男爵](../Page/西里尔·内维尔.md "wikilink")（1940年10月4日）
  - [查尔斯·波特尔子爵](../Page/查尔斯·波特尔.md "wikilink")（1944年6月1日）
  - [亚瑟·泰德男爵](../Page/亚瑟·泰德.md "wikilink")（1945年9月12日）
  - [威廉·舒尔托·道格拉斯男爵](../Page/威廉·舒尔托·道格拉斯.md "wikilink")（1946年1月1日）
  - [亚瑟·哈里斯爵士](../Page/亚瑟·哈里斯，第一代男爵.md "wikilink")（1946年1月1日）
  - [约翰·斯莱塞爵士](../Page/约翰·斯莱塞.md "wikilink")（1950年6月8日）
  - [菲利普亲王殿下（爱丁堡公爵）](../Page/菲利普亲王_\(爱丁堡公爵\).md "wikilink")（1953年1月15日）
  - [威廉·迪克森爵士](../Page/威廉·迪克森.md "wikilink")（1954年6月1日）
  - [德莫特·博伊尔爵士](../Page/德莫特·博伊尔.md "wikilink")（1958年1月1日）
  - [亨利王子殿下（格洛斯特公爵）](../Page/亨利王子_\(告羅士打公爵\).md "wikilink")（1958年）
  - [托马斯·派克爵士](../Page/托马斯·派克.md "wikilink")（1962年[4月6日6](../Page/4月6日6.md "wikilink")）
  - [Charles Elworthy, Baron Elworthy of
    Timaru](../Page/Charles_Elworthy.md "wikilink")（1967年4月1日）
  - [约翰·格兰迪爵士](../Page/约翰·格兰迪.md "wikilink")（1971年4月1日）
  - [丹尼尔·斯波茨伍德爵士](../Page/丹尼尔·斯波茨伍德.md "wikilink")（1974年3月31日）
  - [安德鲁·汉弗莱爵士](../Page/安德鲁·汉弗莱.md "wikilink")（1976年8月6日）
  - [尼尔·卡梅伦（卡梅伦男爵）](../Page/尼尔·卡梅伦.md "wikilink")（1977年7月31日）
  - [迈克尔·比瑟姆爵士](../Page/迈克尔·比瑟姆.md "wikilink")（1982年10月14日）
  - [基思·威廉姆森爵士](../Page/基思·威廉姆森.md "wikilink")（1985年10月15日）
  - [戴维·克雷格（克雷格男爵）](../Page/戴维·克雷格.md "wikilink")（1988年11月14日）
  - [彼得·哈丁爵士](../Page/彼得·哈丁.md "wikilink")（1993年1月）
  - [查爾斯 (威爾斯親王)](../Page/查爾斯_\(威爾斯親王\).md "wikilink")（2012年6月16日）

[英國皇家空軍元帥](../Category/英國皇家空軍元帥.md "wikilink")
[军](../Category/英國相關列表.md "wikilink")
[Category:职业列表](../Category/职业列表.md "wikilink")