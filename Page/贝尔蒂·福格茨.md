**贝尔蒂·福格茨**(，)，前[德國足球員](../Page/德國.md "wikilink")，現任足球教練。

## 球員生涯

### 球會

出生於[-{zh-hans:门兴格拉德巴赫;zh-hk:慕遜加柏;zh-tw:門興格拉德巴赫;}-的福格茨是著名的德國足球員](../Page/门兴格拉德巴赫.md "wikilink")，他一生之中就只效力過[-{zh-hans:门兴格拉德巴赫;
zh-hant:慕遜加柏;zh-tw:門興格拉德巴赫}-](../Page/慕遜加柏足球會.md "wikilink")。司職[右边后卫的福士於](../Page/右边后卫.md "wikilink")1970年協助球會贏得了[德甲冠軍](../Page/德國足球甲級聯賽.md "wikilink")，當時福士只是25歲不到。當時门兴格拉德巴赫擁有一支強大的兵團，除了福士之外，還包括了門將克里夫、後防球員韋迪金、中場球員[內策爾與兩位前線球員威馬與軒加斯](../Page/君特·內策爾.md "wikilink")。

1971年，福士等猛將為球會再次贏得聯賽[冠軍](../Page/冠軍.md "wikilink")，而福士更於同年首次奪得[德國足球先生的個人至高榮譽](../Page/德國足球先生.md "wikilink")。後來兩年，慕遜加柏得到了[德國盃的冠軍](../Page/德國盃.md "wikilink")，並打入了[歐洲足協杯](../Page/歐洲足協杯.md "wikilink")，更晉身了決賽，可惜最終不敵[利物浦](../Page/利物浦足球俱乐部.md "wikilink")，與冠軍無緣。

福士再次參與歐洲足協杯的決賽要到1975年，這一年，福士在隊中奪得了聯賽的冠軍，又於足協杯中兩回合以5:1擊敗[荷蘭球會的](../Page/荷兰足球甲级联赛.md "wikilink")[屯特](../Page/特温特足球俱乐部.md "wikilink")，贏得了該錦標賽，這個冠軍不僅是隊史，也是福士的第一個歐洲賽事冠軍。

這次的勝利不但是由於球會的前線球員表現出色，當中，其他的隊員也功不可沒，冠軍與福士防守的技術不無關係，慕遜加柏也逐漸成為歐洲的強隊之一。後年於1977年，福士以隊長的身份為球會獲得聯賽的三連冠，同年，慕遜加柏晉身[歐聯的決賽](../Page/歐聯.md "wikilink")，但再次敗為[利物浦的腳下](../Page/利物浦足球俱乐部.md "wikilink")，當時的賽果是1:3。

1979年，福士再次贏得了歐洲足協杯的冠軍，又第二次得到德國足球先生的獎項。

### 國家隊

在國家隊方面，該位右後防球員，首次為國家隊上陣是於1967年5月30日對[南斯拉夫](../Page/南斯拉夫國家足球隊.md "wikilink")。在[1970年世界杯](../Page/1970年世界杯足球賽.md "wikilink")，福士以正選之名代表[西德出賽](../Page/德國國家足球隊.md "wikilink")，在四強賽中最終以3:4不敵[意大利](../Page/意大利國家足球隊.md "wikilink")，只能得到季軍。

兩年後的[-{zh-hans:欧洲足球锦标赛;
zh-hant:歐洲國家盃;}-](../Page/欧洲足球锦标赛.md "wikilink")，西德雖然得到了冠軍，但事實上，福士因國家隊的激烈鬥爭而失去了正選地位。[1974年世界杯](../Page/1974年世界杯足球賽.md "wikilink")，福士奪回正選，代表西德以主場之利出賽，決賽中他對[克魯伊夫嚴密的貼身看防成效顯著](../Page/约翰·克鲁伊夫.md "wikilink")，成功癱瘓了這位連三屆歐洲足球先生的荷蘭隊長，而荷蘭在克魯伊夫無法有效組織進攻的情況下戰術大亂，最後西德在決賽中以2:1險勝[荷蘭](../Page/荷蘭國家足球隊.md "wikilink")，贏得了世界球壇的最高寶座，福格茨也成功入選全明星隊。

1976年歐洲國家盃，福士再次成為了後備球員。兩年後的1978年世界杯，由於西德正處於青黃不接的時期，福士雖然是該屆唯一入選全明星隊的西德選手，最終還是有心無力，未能蟬聯冠軍。出局後，福士退出了國家隊，1年後福士掛靴，正式告別球員生涯。

## 教練生涯

結束球員生涯後，-{zh-hans:福格茨;
zh-hant:福士;}-成為領隊，八十年代執教[西德青年隊](../Page/西德青年隊.md "wikilink")。1990年世界杯後福士執掌[德國國家隊](../Page/德國國家足球隊.md "wikilink")，曾取得[1992年歐洲國家杯亞軍和](../Page/1992年歐洲國家杯.md "wikilink")[1996年歐洲國家杯冠軍](../Page/1996年歐洲國家杯.md "wikilink")，但在世界杯福士的表現卻未能令人滿意，1994年和1998年世界杯都在八強出局，隨後他更宣佈辭職。

兩年後福士接掌[-{zh-hans:勒沃库森;
zh-hant:利華古遜;}-](../Page/利華古遜足球會.md "wikilink")，可惜執教期間與隊中一眾球員發生嚴重糾紛，結果只執教後一年便宣佈辭職，更公開指責陣中球員。不久福士亦執教[科威特以及](../Page/科威特國家足球隊.md "wikilink")[蘇格蘭](../Page/蘇格蘭足球代表隊.md "wikilink")，同樣戰績慘淡。直到2006年4月一日，德國首都報章曾經傳出他將在7月接替艾歷臣出任英格蘭國家隊教練一職，後來其空缺改由[麥卡倫和](../Page/麥卡倫.md "wikilink")[卡比路接任](../Page/卡比路.md "wikilink")。

福士雖然經驗豐富，但與一眾球員的不咬弦亦是福士的最大問題。

2007年1月，福士執教[尼日利亞](../Page/尼日利亞國家足球隊.md "wikilink")，尼日利亞在[2008年非洲國家盃八強出局後](../Page/2008年非洲國家盃.md "wikilink")，福士辭職，斯間曾傳出他將轉任車路士教練，後來在2008年4月改任[阿塞拜疆教練](../Page/阿塞拜疆國家足球隊.md "wikilink")，2年後他獲續約至2012年，並粉碎他接替卡比路執掌英格蘭國家足球隊教練之謠言。

## 外部連結

  - [Unofficial fansite
    (German)](http://www.top-info.de/thein/gs/berti.html) - Vogts has no
    official website
  - [The Scottish Football Association's page on
    Vogts](https://web.archive.org/web/20041209070645/http://www.scottishfa.co.uk/squad/vogts.htm)
  - [Leverkusen who's
    who](http://www.leverkusen.com/whoiswho/whoiswho.php4?view=VogtsBer)

[Category:1998年世界盃足球賽主教練](../Category/1998年世界盃足球賽主教練.md "wikilink")
[Category:1994年世界盃足球賽主教練](../Category/1994年世界盃足球賽主教練.md "wikilink")
[Category:1996年歐洲國家盃主教練](../Category/1996年歐洲國家盃主教練.md "wikilink")
[Category:1992年歐洲國家盃主教練](../Category/1992年歐洲國家盃主教練.md "wikilink")
[Category:歐洲國家盃冠軍隊主教練](../Category/歐洲國家盃冠軍隊主教練.md "wikilink")
[Category:阿塞拜疆國家足球隊主教練](../Category/阿塞拜疆國家足球隊主教練.md "wikilink")
[Category:奈及利亞國家足球隊主教練](../Category/奈及利亞國家足球隊主教練.md "wikilink")
[Category:蘇格蘭足球代表隊領隊](../Category/蘇格蘭足球代表隊領隊.md "wikilink")
[Category:科威特國家足球隊主教練](../Category/科威特國家足球隊主教練.md "wikilink")
[Category:德國國家足球隊主教練](../Category/德國國家足球隊主教練.md "wikilink")
[Category:德國足球主教練](../Category/德國足球主教練.md "wikilink")
[Category:1978年世界盃足球賽球員](../Category/1978年世界盃足球賽球員.md "wikilink")
[Category:1974年世界盃足球賽球員](../Category/1974年世界盃足球賽球員.md "wikilink")
[Category:1970年世界盃足球賽球員](../Category/1970年世界盃足球賽球員.md "wikilink")
[Category:1976年歐洲國家盃球員](../Category/1976年歐洲國家盃球員.md "wikilink")
[Category:1972年歐洲國家盃球員](../Category/1972年歐洲國家盃球員.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")
[Category:歐洲國家盃冠軍隊球員](../Category/歐洲國家盃冠軍隊球員.md "wikilink")
[Category:德國國家足球隊球員](../Category/德國國家足球隊球員.md "wikilink")
[Category:德国足球运动员](../Category/德国足球运动员.md "wikilink")
[Category:利華古遜主教練](../Category/利華古遜主教練.md "wikilink")
[Category:慕遜加柏球員](../Category/慕遜加柏球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:在英國的德國人](../Category/在英國的德國人.md "wikilink")
[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")
[Category:在阿塞拜疆教的外籍足球主敎練](../Category/在阿塞拜疆教的外籍足球主敎練.md "wikilink")