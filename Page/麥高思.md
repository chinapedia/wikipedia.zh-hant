**麥高思**(**Mark
McGowan**，1970年-)，[英國著名的](../Page/英國.md "wikilink")[行為藝術家](../Page/行為藝術家.md "wikilink")，由於其[行為藝術怪異出位](../Page/行為藝術.md "wikilink")，被部份人認為譁眾取寵\[1\]。2007年2月，麥高思在[美國的最大城市](../Page/美國.md "wikilink")[紐約中扮演該國](../Page/紐約.md "wikilink")[總統](../Page/總統.md "wikilink")[乔治·沃克·布什](../Page/喬治·沃克·布希.md "wikilink")，並任由路人踢他的[臀部](../Page/臀部.md "wikilink")，以「治療美國人在喬治布殊統治下的不滿情緒」\[2\]。

## 行為藝術表演

於[倫敦藝術大學](../Page/倫敦藝術大學.md "wikilink")[坎貝威爾藝術學院](../Page/坎貝威爾藝術學院.md "wikilink")(*Camberwell
College of
Arts*)工作的麥高思成長於[倫敦的帕咸](../Page/倫敦.md "wikilink")(*Peckham*)行政區\[3\]，於2001年開始從在倫敦等地從事行為藝術\[4\]。

### 2001年

英國人麥高思在2001年「出道」時，於藝術館門外穿起[警察制服](../Page/警察.md "wikilink")，並要求市民以棍打他。

### 2002年

麥高思在當年的[聖誕節在大象堡](../Page/聖誕節.md "wikilink")(*The Elephant and
Castle*)滾動至巴斯陸公園(*Bethnal Green*)，並唱著聖誕歌，以歌頌於聖誕節當值的清潔工人。

### 2003年

事隔一年後，麥高思為了反對英國人的[英式早餐過度豐富](../Page/英式早餐.md "wikilink")，於是坐在滿著[豆的](../Page/豆.md "wikilink")[浴缸中](../Page/浴缸.md "wikilink")，並且頭頂48條[培根以及](../Page/培根.md "wikilink")[鼻子插兩塊](../Page/鼻子.md "wikilink")[馬鈴薯片](../Page/馬鈴薯片.md "wikilink")，供他人指拍照。

隨後，他以鼻子把一顆[花生由金史密夫氏書院](../Page/花生.md "wikilink")(*Goldsmiths
College*)一下一下的推至[唐寧街](../Page/唐寧街.md "wikilink")10號，以呼籲社會對資質高的貧窮學生要面對高昂的學費上予以財政援助。

同年，麥高思頭頂27盎司的[火雞](../Page/火雞.md "wikilink")，並一邊反方向的步行以及一邊對著肥胖的途人大喊，以表達[癡肥問題在英國與全球的嚴重性](../Page/癡肥.md "wikilink")。

聖誕節後的3天(12月28日)，麥高思坐著[超級市場的](../Page/超級市場.md "wikilink")[手推車於他成長的行政區帕咸出發](../Page/手推車.md "wikilink")，試圖靠手推車付加的帆到達[蘇格蘭的大城市](../Page/蘇格蘭.md "wikilink")[格拉斯哥](../Page/格拉斯哥.md "wikilink")，以緩和英蘇之間對[威廉·华莱士的紛爭](../Page/威廉·華萊士.md "wikilink")。最後，基於[天氣和儀器上的問題](../Page/天氣.md "wikilink")，麥高思在起程的17天(1月13日)後放棄了滑行至格拉斯哥的計劃。

### 2004年

麥高思在這一年先在自己家中烹殺和品嚐[狐狸](../Page/狐狸.md "wikilink")，嘲諷社會上人們對過於保護狐狸，但卻對[毒品](../Page/毒品.md "wikilink")[可卡因引申的害處不聞不問](../Page/可卡因.md "wikilink")。

同年，麥高思於[意大利首都](../Page/意大利.md "wikilink")[米蘭借](../Page/米蘭.md "wikilink")[耳朵之力由米蘭中央車站拉一台](../Page/耳朵.md "wikilink")[電視至意大利總理](../Page/電視.md "wikilink")[-{zh-hans:西爾維奥·貝盧斯科尼;zh-hk:
盧斯科尼家中](../Page/西爾維奧·貝盧斯科尼.md "wikilink")，抗議他控制眾多的[傳媒](../Page/傳媒.md "wikilink")。

返國後，麥高思到藝術館到自己的腳釘在壁上。其後他用幼繩把自己的腳趾與[巴士綁在一起](../Page/巴士.md "wikilink")，並拉了約30[米](../Page/米.md "wikilink")，以抗議該巴士公司濫增路線，增加利潤。

### 2005年

麥高思這年在倫敦街頭隨意的用[鑰匙刮出街上的](../Page/鑰匙.md "wikilink")[汽車](../Page/汽車.md "wikilink")；開啟[奧迪80汽車的](../Page/奧迪80.md "wikilink")[引擎一年](../Page/引擎.md "wikilink")，產生不必要的廢氣，以提醒公眾對[全球增溫等環保問題的存要和威脅性](../Page/全球增溫.md "wikilink")。

隨後，他為了表達[私營水公司泰萊用水](../Page/私營.md "wikilink")(*Thomus
Water*)的不滿，把水長開半[年](../Page/年.md "wikilink")，但由於該公司後來和議，於是暫停行動。後來他又從[賴布頓中打斛至倫敦](../Page/賴布頓.md "wikilink")，但在演出後的第4天因傷入院。

另外，他在英國大選(3月5日)當天到唐寧街親吻首相[-{zh:貝理雅;zh-hans:布莱尔;zh-hk:貝理雅;zh-tw:布萊爾;}-的照片十萬次](../Page/貝理雅.md "wikilink")，以響應大選。另一方面，他又曾邀請[朋友在他身上夾](../Page/朋友.md "wikilink")17200塊布，但因過程太悶至中止。

### 2006年

麥高思於2005年12月26日開始口咬[玫瑰](../Page/玫瑰.md "wikilink")、腳拖18盒[朱古力的由倫敦爬行到](../Page/朱古力.md "wikilink")[堪特伯雷](../Page/堪特伯雷.md "wikilink")，為期12天。後來，麥高思在達斯奴公園(*Bethnal
Green*)穿起交通警察的制服8個小時，任由路人打他。

其後，為反映對英國派軍到[伊拉克帶來大量的死傷](../Page/伊拉克.md "wikilink")，他扮演[士兵由](../Page/士兵.md "wikilink")11月14日臥於[伯明翰的道路一周至](../Page/伯明翰.md "wikilink")11月20日。同年的聖誕節，他把手掛在電燈柱中兩星期，以宣揚[和平](../Page/和平.md "wikilink")。

### 2007年

1月，麥高思在自己的官方網頁中要求著名電視節目「名人版老大哥」中艾麗絲謝(*Shilpa
Shetty*)的支持者燒她的雕刻，但後來古迪對艾麗絲謝的[種族歧視漸加嚴重](../Page/種族歧視.md "wikilink")，於是自動的刪去有關要求。

1個月後，麥高思到達[紐約](../Page/紐約.md "wikilink")，扮演美國[總統](../Page/總統.md "wikilink")[喬治布殊](../Page/喬治布殊.md "wikilink")，並於[林肯中心出發](../Page/林肯中心.md "wikilink")，在其[屁股貼上](../Page/屁股.md "wikilink")「踢我的屁股」(*kick
my ass*)的字樣，任由路人踢他，聲稱這可以治療美國人在喬治布殊統治下的不滿。

## 資料來源

## 外部連結

  - [官方網頁](https://web.archive.org/web/20070928032117/http://www.markmcgowan.org/index.html)
  - [有關他扮士兵的BBC新聞](http://news.bbc.co.uk/1/hi/england/west_midlands/5320866.stm)
  - [有關他以腳拉巴士的新聞](http://news.bbc.co.uk/1/hi/england/london/3574586.stm)
  - [以匙刮車的新聞](http://news.bbc.co.uk/1/hi/england/london/4454485.stm)

[Category:1970年出生](../Category/1970年出生.md "wikilink")
[Category:英国行为艺术家](../Category/英国行为艺术家.md "wikilink")
[Category:英国网路红人](../Category/英国网路红人.md "wikilink")
[Category:英国共和主义者](../Category/英国共和主义者.md "wikilink")
[Category:英国社会主义者](../Category/英国社会主义者.md "wikilink")
[Category:社會評論家](../Category/社會評論家.md "wikilink")

1.  [英國](../Page/英國.md "wikilink")[衛報](../Page/衛報.md "wikilink")
2.  蘋果日報 A20 2007年2月24日
3.  [My London: Mark
    McGowan](http://www.bbc.co.uk/london/yourlondon/my_london/mylondon_mark_mcgowan.shtml)
4.  [Mark McGowan](http://clublet.com/c/c/house?page=MarkMcGowan)