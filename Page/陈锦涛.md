**陈锦涛**（）\[1\]，字**澜生**，[广东省](../Page/广东省.md "wikilink")[南海县人](../Page/南海县.md "wikilink")。[中华民国政治家](../Page/中华民国.md "wikilink")、经济学家。\[2\]\[3\]\[4\]\[5\]

## 生平

[Chen_Jintao.jpg](https://zh.wikipedia.org/wiki/File:Chen_Jintao.jpg "fig:Chen_Jintao.jpg")》中的陳錦濤照片\]\]
1885年（[光緒](../Page/光緒.md "wikilink")11年），陳錦濤入[香港](../Page/香港.md "wikilink")[皇仁书院学习](../Page/皇仁书院.md "wikilink")。1890年（光緒16年）毕业后留校任教，后转任天津[北洋大学堂教习](../Page/北洋大学堂.md "wikilink")。1901年（光緒27年）以官费赴[美國学习政治经济学](../Page/美國.md "wikilink")，获[哥伦比亚大学](../Page/哥伦比亚大学.md "wikilink")[理学碩士](../Page/理学碩士.md "wikilink")。1906年（光緒32年），获[美國](../Page/美國.md "wikilink")[耶鲁大学哲学博士学位](../Page/耶鲁大学.md "wikilink")，師從著名經濟學家[歐文·費雪](../Page/歐文·費雪.md "wikilink")。9月回国应清廷部试，获法政进士。嗣任[度支部预算司司长](../Page/度支部.md "wikilink")、统计局局长、印铸局局长、币制改良委员会会长、大清銀行副監督、[资政院议员等职务](../Page/资政院.md "wikilink")。1911年（[宣統](../Page/宣統.md "wikilink")3年）10月，出任[袁世凱内閣](../Page/袁世凱内閣.md "wikilink")[度支部副大臣](../Page/度支部.md "wikilink")。\[6\]\[7\]

1912年（[民国元年](../Page/民国紀元.md "wikilink")）1月，[南京臨時政府成立](../Page/南京臨時政府.md "wikilink")，陳錦濤出任財政部总長。1912年3月，与[許世英组织](../Page/許世英.md "wikilink")[国民共進会](../Page/国民共進会.md "wikilink")。1912年9月，出任[北京政府財政部審計处总办](../Page/北洋政府.md "wikilink")。1913年（民国2年）10月，任財政部駐外財政員，驻[英国](../Page/英国.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")。1916年（民国5年）6月，任[段祺瑞内閣財政总長兼署理外交总長](../Page/第一次段祺瑞内阁.md "wikilink")。1917年4月，因受賄罪被收監。\[8\]\[9\]\[10\]\[11\]当时，1917年4月上海《[申报](../Page/申报.md "wikilink")》揭露北京政府财政部在受理商人[张兴汉开办炼铜厂申请的过程中](../Page/张兴汉.md "wikilink")，总长陈锦涛、次长[殷汝骊等主要官员涉嫌收受张兴汉的商业贿赂](../Page/殷汝骊.md "wikilink")。该报道发表后，[国会众议院提出质问](../Page/民元国会.md "wikilink")，经司法调查属实，陈锦涛入狱，[殷汝骊潜逃](../Page/殷汝骊.md "wikilink")。\[12\]1918年（民国7年），陈锦涛获得[大总统](../Page/中华民国大总统.md "wikilink")[冯国璋赦免](../Page/冯国璋.md "wikilink")。\[13\]\[14\]

此後，陳錦濤赴[广州参加](../Page/广州.md "wikilink")[護法軍政府](../Page/護法軍政府.md "wikilink")。1920年（民国9年）4月，继[伍廷芳之後出任](../Page/伍廷芳.md "wikilink")[護法軍政府財政部長](../Page/護法軍政府.md "wikilink")\[15\]\[16\]。同年11月，主席总裁[岑春煊失势](../Page/岑春煊.md "wikilink")，陳錦濤遂辞職，此後寓居上海。1925年任[西北银行经理](../Page/西北银行.md "wikilink")。1925年（民国14年）12月，陈錦濤在[段祺瑞手下再度获得起用](../Page/段祺瑞.md "wikilink")，出任財政总長兼盐务署督办。1926年，与[胡光鹿等人在](../Page/胡光鹿.md "wikilink")[天津合办](../Page/天津.md "wikilink")[中国无线电业公司](../Page/中国无线电业公司.md "wikilink")。\[17\]\[18\]\[19\]\[20\]

[国民政府時期](../Page/国民政府.md "wikilink")，1930年（民国19年），陳錦濤出任[国立清華大学经济系教授](../Page/国立清華大学.md "wikilink")，并兼任[南京国民政府財政部幣制研究委員会主席](../Page/南京国民政府.md "wikilink")。1935年（民国24年），任[南京国民政府財政部幣制委員会主席](../Page/南京国民政府.md "wikilink")。1938年（民国27年）3月，参加[南京的](../Page/南京.md "wikilink")[中華民国維新政府](../Page/中華民国維新政府.md "wikilink")，任財政部長，5月兼任[華興銀行总裁](../Page/華興銀行.md "wikilink")。\[21\]\[22\]\[23\]\[24\]

1939年（民国28年）6月12日，陳錦濤在上海病逝。享年69岁（满67歳）。\[25\]\[26\]

## 著作

陈锦涛著有《均富》等书。

## 参考文献

{{-}}

| （[北京政府](../Page/北洋政府.md "wikilink")）                                                                              |
| ----------------------------------------------------------------------------------------------------------------- |
| [CJZ1.png](https://zh.wikipedia.org/wiki/File:CJZ1.png "fig:CJZ1.png") [中華民国維新政府](../Page/中華民国維新政府.md "wikilink") |

[Category:皇仁書院校友](../Category/皇仁書院校友.md "wikilink")
[Category:华人经济学家](../Category/华人经济学家.md "wikilink")
[Category:清华大学学者](../Category/清华大学学者.md "wikilink")
[Category:國立清華大學教授](../Category/國立清華大學教授.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:资政院议员](../Category/资政院议员.md "wikilink")
[Category:中華民國外交總長](../Category/中華民國外交總長.md "wikilink")
[Category:中華民國財政總長](../Category/中華民國財政總長.md "wikilink")
[Category:中華民國審計部首長](../Category/中華民國審計部首長.md "wikilink")
[Category:中华民国维新政府人物](../Category/中华民国维新政府人物.md "wikilink")
[Category:中华民国经济学家](../Category/中华民国经济学家.md "wikilink")
[Category:南海人](../Category/南海人.md "wikilink")
[J錦](../Category/陈姓.md "wikilink")

1.  徐友春主編『民国人物大辞典
    増訂版』1493頁、財政部財政史料陳列室・財政人物索引（中華民国財政部网站）作生于1871年6月20日。『東京朝日新聞』昭和13年（1938年）3月29日夕刊記事以及Who's
    Who in China 3rd ed.,p.110作1870年生。

2.

3.

4.  [「陳錦濤」（財政部財政史料陳列室・財政人物索引）](http://www.mof.gov.tw/museum/ct.asp?xItem=3063&ctNode=63&mp=1)[中華民国財政部网站](../Page/中華民国財政部.md "wikilink")

5.

6.
7.
8.
9.
10.
11.
12. [严泉，议员质询竟扳倒受贿的财政部长，南方周末2010年9月23日，第E24版](http://news.ifeng.com/history/zhongguojindaishi/detail_2010_09/30/2679756_0.shtml)

13.
14.
15. 邵桂花「温宗尧」722頁作此。但是，劉寿林主編『民国職官年表』中的「中華民国軍政府職官表」（139頁）、郭卿友主編『中華民国時期軍政職官誌
    上』中的「政務院曁行政各部（1918.5-1921.5）」（372頁）均无陳錦濤就任財務部長的記載。

16. 邵桂花「温宗尧」

17.
18.
19.
20.
21.
22.
23.
24.
25.
26.