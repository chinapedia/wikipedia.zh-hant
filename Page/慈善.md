[Ravi_Varma-Lady_Giving_Alms_at_the_Temple.jpg](https://zh.wikipedia.org/wiki/File:Ravi_Varma-Lady_Giving_Alms_at_the_Temple.jpg "fig:Ravi_Varma-Lady_Giving_Alms_at_the_Temple.jpg")

**慈善**是一種善良意願的社會活動，可能包含[布施](../Page/布施.md "wikilink")、救濟貧苦、[動物保護甚至](../Page/動物保護.md "wikilink")[環境保護等](../Page/環境保護.md "wikilink")。慈善事業的實踐意味著自願向有需要的人提供幫助，作為一種人道主義行為。

**慈善**關係「[施予者](../Page/慈善家.md "wikilink")」和「受施者」。在[社會上](../Page/社會.md "wikilink")，不問物質回報地，給予有需要的社群幫助、[贊助等](../Page/贊助.md "wikilink")。是促進人類福利進步的利他關懷，通常透過捐贈金錢、資產或工作活動，由教育或醫療機構捐贈予有需求的人，或公益協助其他社會需求。\[1\]

[中國的慈善最主要的莫過於布施](../Page/中國.md "wikilink")，尤其是[饑荒或戰亂時的布施食物](../Page/饑荒.md "wikilink")，《[礼记](../Page/礼记.md "wikilink")·檀弓》記載有“嗟来之食”的典故，[黔敖所主持的就是一種慈善事業](../Page/黔敖.md "wikilink")。後世的[佛教](../Page/佛教.md "wikilink")、[道教認為行善可以累積](../Page/道教.md "wikilink")[功德](../Page/功德.md "wikilink")，清朝康熙年間，[大學士](../Page/大學士.md "wikilink")[李光地在饑荒時](../Page/李光地.md "wikilink")，设置粥厂煮粥散给饑民吃。\[2\][江苏巡抚](../Page/江苏巡抚.md "wikilink")[张伯行倡导](../Page/张伯行.md "wikilink")“担粥法”\[3\]。[陆世仪在](../Page/陆世仪.md "wikilink")《劝施米汤约》文中提出施米汤法，就是家里做饭时，多放点水，把米汤舀出来，再放點[雜糧](../Page/雜糧.md "wikilink")，施舍给没有饭吃的人\[4\]。光绪中九年（1883）水灾，顺天府尹[周家楣奏准在各乡镇及京城六门外设立粥厂](../Page/周家楣.md "wikilink")。

## 相關

  - [慈善机构](../Page/慈善机构.md "wikilink")
  - [中國慈善事業史](../Page/中國慈善事業史.md "wikilink")
  - [志願工作者](../Page/志願工作者.md "wikilink")
  - [香港賽馬會慈善](../Page/香港賽馬會.md "wikilink")[信託](../Page/信託.md "wikilink")[基金](../Page/基金.md "wikilink")
  - [東華三院](../Page/東華三院.md "wikilink")
  - [六合彩](../Page/六合彩.md "wikilink")[獎券基金](../Page/獎券基金.md "wikilink")，[愛國獎券](../Page/愛國獎券.md "wikilink")
  - [公益金](../Page/公益金.md "wikilink")
  - [非營利組織](../Page/非營利組織.md "wikilink")
  - [麦当劳](../Page/麦当劳.md "wikilink")
  - [金球奖](../Page/金球奖.md "wikilink")
  - [麥可·傑克森](../Page/麥可·傑克森.md "wikilink")
  - [奥普拉·温芙瑞](../Page/奥普拉·温芙瑞.md "wikilink")
  - [美国政治](../Page/美国政治.md "wikilink")
  - [伍宜孫](../Page/伍宜孫.md "wikilink")
  - [艾茵·兰德](../Page/艾茵·兰德.md "wikilink")
  - [红卍字会](../Page/红卍字会.md "wikilink")
  - [宗教社會主義](../Page/宗教社會主義.md "wikilink")
  - [海伦·克拉克](../Page/海伦·克拉克.md "wikilink")
  - [天津教案](../Page/天津教案.md "wikilink")
  - [李陞](../Page/李陞.md "wikilink")
  - [柏林西非會議](../Page/柏林西非會議.md "wikilink")
  - [社会团体](../Page/社会团体.md "wikilink")
  - [救世軍](../Page/救世軍.md "wikilink")
  - [胡永輝](../Page/胡永輝.md "wikilink")
  - [蓋茨](../Page/蓋茨.md "wikilink")
  - [龍騰燈耀慶千禧](../Page/龍騰燈耀慶千禧.md "wikilink")
  - [英國三軍廣播服務](../Page/英國三軍廣播服務.md "wikilink")
  - [朱孟依](../Page/朱孟依.md "wikilink")
  - [蓬蝶普·娜矶朗嘉诺克](../Page/蓬蝶普·娜矶朗嘉诺克.md "wikilink")
  - [華懋慈善基金](../Page/華懋慈善基金.md "wikilink")
  - [莊朱玉女](../Page/莊朱玉女.md "wikilink")

## 注釋

[慈善活动](../Category/慈善活动.md "wikilink")

1.  [藝術與建築索引典－慈善](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300253947)於2011年3月31日查閱
2.  《文贞公年谱》记载：“贫民赖以存济，故岁虽荒，而途无殍者。”
3.  《救荒事宜十条·担粥法》，《清经世文编》卷四十一
4.  《清经世文编》卷四十二