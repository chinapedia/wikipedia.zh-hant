**李渔**（），初名**仙侣**，后改名**渔**，字**谪凡**，号**笠翁**，後人常稱之蟹仙。明末清初文学家、戏曲家，曾經評定[四大奇書](../Page/四大奇書.md "wikilink")，祖籍[浙江省](../Page/浙江省.md "wikilink")[兰溪县](../Page/兰溪县.md "wikilink")（今[浙江省](../Page/浙江省.md "wikilink")[兰溪市](../Page/兰溪市.md "wikilink")）夏李村，后来祖父随“兰溪帮”到了江苏如皋做种药材生意。李渔的童年、少年是在如皋度过的，后来又娶妻生女，前后在如皋生活23年，中年之后又在南京生活了14年，之后在杭州生活。

## 生平

明萬曆三九年(1611年), 八月初七生於南直隸雉皋（今江蘇省如皋市）。祖籍是浙江金華府蘭溪縣夏李村人。

18岁，补博士弟子员。后居于[南京](../Page/南京.md "wikilink")，把居所命名为「芥子园」，并开设书铺，编刻图籍，广交达官贵人、文坛名流。李漁嗜食[螃蟹](../Page/螃蟹.md "wikilink")，每年於螃蟹未上市時即儲錢以待，自稱購蟹之錢為“買命錢”，每日餐桌上不可無蟹，人稱“蟹仙”。

康熙十六年（1677），遷回杭州。買下了吳山東北麓張侍衛的舊宅，建造了「層園」，時年六十七。

康熙十九年（1680），正月十三日大雪紛飛的清晨，與世長辭，享年七十。死後安葬在杭州方家峪九曜山上，錢塘縣令梁允植為他題碣：湖上笠翁之墓。

## 著作

著有《[凰求鳳](../Page/凰求鳳.md "wikilink")》、《[玉搔头](../Page/玉搔头.md "wikilink")》等戏剧，《[肉蒲团](../Page/肉蒲团.md "wikilink")》、《[觉世名言十二楼](../Page/觉世名言十二楼.md "wikilink")》、《[无声戏](../Page/无声戏.md "wikilink")》、《[連城璧](../Page/連城璧.md "wikilink")》等小说，與《[闲情偶寄](../Page/闲情偶寄.md "wikilink")》等书。

《闲情偶寄》是他主要的对自己的生活所得所闻见之事物总结的书。包含有对戏曲的看法，批评。从舞台的实际出发，注重戏曲的结构、中心事件的选择安排等，是中国戏曲批评史上，重要的著作之一。其中，还涉及到生活中的如饮食、作卧等方面的审美感受。

《笠翁传奇十种》剧目为《[奈何天](../Page/奈何天.md "wikilink")》、《[比目鱼](../Page/比目鱼.md "wikilink")》、《[蜃中楼](../Page/蜃中楼.md "wikilink")》（改編自《[柳毅傳書](../Page/柳毅傳書.md "wikilink")》與《[張生煮海](../Page/張生煮海.md "wikilink")》\[1\]）、《[怜香伴](../Page/怜香伴.md "wikilink")》、《[风筝误](../Page/风筝误.md "wikilink")》、《[慎鸾交](../Page/慎鸾交.md "wikilink")》、《[凰求鳳](../Page/凰求鳳.md "wikilink")》、《[巧团圆](../Page/巧团圆.md "wikilink")》、《[玉搔头](../Page/玉搔头.md "wikilink")》、《[意中缘](../Page/意中缘.md "wikilink")》。

著名的传统绘画教材《[芥子园画谱](../Page/芥子园画谱.md "wikilink")》在成书阶段得到李渔的大力支持，故其书以李渔居所名之。画谱的编者之一[沈心友是李渔的女婿](../Page/沈心友.md "wikilink")。

  -
  -
  -
  -
  -
  -
## 译本

  - 《肉蒲团》一书有美、德、俄、法译本：
      -
      - Li Yü: Jou pu tuan, Franz Kuhn, Fischer Taschenbuch Verlag
        Frankfurt 1979 ISBN 978-3-596-22451-7

      - Воскресенский Д.Н. Ли Юй. Полуночник Вэйян или подстилка из
        плоти. (пер. с кит., предисл., коммент.) М., Гудьял-Пресс

      - LI‑YU « Jeou-P'ou-T'ouan, la chair comme tapis de prière »,
        traduit par Pierre Klossowski;Éditions Jean-Jacques Pauvert,
        Paris, 1979
  - 《觉世名言十二楼》 译本：
      - Franz Kuhn: Die Dreizehnstöckige Pagode,Steiniger Verlag,1940.
      - Tower for the Summer Heat,Li Yu; Translation, preface, and notes
        by Patrick Hanan,Columbia University Press,1998 ISBN
        978-0-231-11384-7.
      - Li Yu: Twelve Towers: Short Stories, Tr.Mao, Nathan K. Hong Kong
        Chinese University Press. 1979 ISBN 978-962-201-170-0
      - Воскресенский Д.Н. Ли Юй. Двенадцать башен (повести XVII в.).
        (пер. с кит., предисл., коммент.) М., Гудьял-Пресс
      - 法、日等译本
  - 《无声戏》
      - Li Yu:Silent Operas (ed. by Patrick Hanan). Hong Kong:
        Renditions Paperbacks, 1990.
  - 《闲情偶寄》
      - Les carnets secrets de Li Yu, un art du bonheur en Chine,
        PHILIPPE PICQUIER, Edité par Jacques Dars, 2004 ISBN
        978-2-87730-664-5
  - Li Yu: À mari jaloux, femme fidèle,par Pascale Frey 1998

## 研究書目

  - Patrick Hanan（韓南）著，楊光輝譯：《創造李漁》（上海：上海教育出版社，2010）。

  - Patrick Hanan: The Invention of Li Yu. Harvard edition 1988, ISBN
    978-0-674-46425-4

  -
  - 陳建華：〈[凝視與窺視：李漁〈夏宜樓〉與明清視覺文化](http://ctma.nccu.edu.tw/chibulletin/app/paper.php?action=list_content&Sn=9)〉。

  -
  - Andrea Stocken: Das Ästhetikkonzept des Li Yu (1610 –1680) im
    Xianqing ouji im Zusammenhang von Leben und Werk. 2005 ISBN
    978-3-447-05120-0

  - HENRY, Eric:Chinese Amusement - The Lively Plays of Li Yü.Archon
    Books Hamden, CT 1980

英文:

  - Owen, Stephen, "Li, Yu, *Silent Operas (Wu-sheng xi)*," in Stephen
    Owen, ed. *An Anthology of Chinese Literature: Beginnings to 1911*.
    New York: (), 1997.
    [p. 915-941](http://courses.washington.edu/chin463/OwenSilentOpera.pdf).
    ([Archive](http://www.webcitation.org/6PoJk3VFf)).

## 参考资料

[Category:明朝劇作家](../Category/明朝劇作家.md "wikilink")
[Category:清朝劇作家](../Category/清朝劇作家.md "wikilink")
[Category:清朝小說家](../Category/清朝小說家.md "wikilink")
[Y](../Category/李姓.md "wikilink")
[Category:兰溪人](../Category/兰溪人.md "wikilink")
[Category:中國小說家](../Category/中國小說家.md "wikilink")
[Category:浙江作家](../Category/浙江作家.md "wikilink")

1.  [pdf](http://140.115.103.241/web/Book/albBook.aspx?p0=2896)（試閱）