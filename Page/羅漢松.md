**罗汉松**（[學名](../Page/學名.md "wikilink")：）是[罗汉松科](../Page/罗汉松科.md "wikilink")[罗汉松属植物的一種](../Page/罗汉松属.md "wikilink")，又名**羅漢杉**、**長青羅漢杉**、**土杉**、**金錢松**、**仙柏**、**羅漢柏**、**江南柏**。

## 形态

[常綠](../Page/常綠植物.md "wikilink")[喬木](../Page/喬木.md "wikilink")，可高達18公尺，通常會修剪以保持低矮，葉為線狀披針形，長7至10厘米，寬7至10毫米，全緣，有明顯中肋，螺旋互生。初夏开花，亦分雌雄，雄花圆柱形，3-5个簇生在叶腋，雌花单生在叶腋；種托大於[種子](../Page/種子.md "wikilink")，種托成熟呈紅紫色，加上綠色的種子，好似光頭的和尚穿著紅色僧袍，故名羅漢松。種子為紅色[漿果](../Page/漿果.md "wikilink")，以吸引[鳥進食散播種子](../Page/鳥.md "wikilink")。\[1\]

## 习性

罗汉松属于中性偏阴性树种，能接受较强光照，也能在较荫的环境下生长。不同於其他松柏門的植物，羅漢松喜歡溫暖濕潤的氣候、稍耐寒，低於零度易凍傷。樹枝柔韌，抗風性強，亦因為這特性，羅漢松經常被人塑造成不同形態的盆景及用作防風樹。主要病蟲害為粉蝨及橙帶藍尺蛾。

## 分布

羅漢松原產於[中國](../Page/中國.md "wikilink")[華南地區](../Page/華南.md "wikilink")，現在世界各地[熱帶及](../Page/熱帶.md "wikilink")[溫帶地區都有栽種](../Page/溫帶.md "wikilink")，尤其是在[日本](../Page/日本.md "wikilink")。由於盆植羅漢松可供觀賞，其[木材供建築](../Page/木材.md "wikilink")、藥用和雕刻，所以其價值甚高。

## 變種

羅漢松常見變種有[小葉羅漢松](../Page/小葉羅漢松.md "wikilink")（*P. m.* var. *maki*
Endl.），葉較小，長4至7厘米，寬3至7毫米。原產於[日本](../Page/日本.md "wikilink")。中國長江以南普遍栽培觀賞。[短小葉羅漢松](../Page/短小葉羅漢松.md "wikilink")（'Condensatus'）葉短小，長在3.5cm以下，密生。[狹葉羅漢松](../Page/狹葉羅漢松.md "wikilink")（var.angustifolius
Bl.）葉較狹窄，長5至10厘米，寬3至6毫米，葉先端漸尖，產於[四川](../Page/四川.md "wikilink")、[貴州](../Page/貴州.md "wikilink")、[江西等省](../Page/江西.md "wikilink")；[日本也有分佈](../Page/日本.md "wikilink")。[柱冠羅漢松](../Page/柱冠羅漢松.md "wikilink")（var.chingii
NEGray）樹冠為柱狀，葉較狹小，產於[浙江](../Page/浙江.md "wikilink")。\[2\]

## 參考文獻

## 外部連結

  - [羅漢松,
    Luohansong](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00813)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [羅漢松葉 Luo Han Song
    Ye](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00802)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

[Category:羅漢松科](../Category/羅漢松科.md "wikilink")
[Category:罗汉松属](../Category/罗汉松属.md "wikilink")
[Category:树](../Category/树.md "wikilink")
[Category:中国植物](../Category/中国植物.md "wikilink")
[Category:日本植物](../Category/日本植物.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:雌雄異株植物](../Category/雌雄異株植物.md "wikilink")
[Category:觀賞植物](../Category/觀賞植物.md "wikilink")
[Category:觀葉植物](../Category/觀葉植物.md "wikilink")

1.  [中国高等植物图鉴 653.土杉Podocarpus macrophyllus (Thunb.) D.
    Don](http://www.eflora.cn/sptujian/Podocarpus%20macrophyllus)
2.  [eFlora 中国在线植物志-Podocarpus
    macrophyllus](http://www.eflora.cn/sp/Podocarpus%20macrophyllus)