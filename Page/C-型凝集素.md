**C-型凝集素**(C-type
Lectins)是一类可以和[糖类结合的](../Page/糖类.md "wikilink")[蛋白质](../Page/蛋白质.md "wikilink")。它们区别于其他类型的[凝集素是在于它们含有大约](../Page/凝集素.md "wikilink")120个氨基酸序列的钙依赖型的[糖类识别区域](../Page/糖类识别区域.md "wikilink")（carbohydrate
recognition domain，CRD）.

## 參照

  -
  -
  -
  -
  -
  -
## 参考文献

[C-型凝集素](../Page/分类:C-型凝集素.md "wikilink")
[分类:人类蛋白质](../Page/分类:人类蛋白质.md "wikilink")
[分类:蛋白质域](../Page/分类:蛋白质域.md "wikilink")
[分类:单次跨膜蛋白](../Page/分类:单次跨膜蛋白.md "wikilink")