**縣道157號
斗南－過溝**，北起[雲林縣](../Page/雲林縣.md "wikilink")[斗南鎮](../Page/斗南鎮.md "wikilink")，南至[嘉義縣](../Page/嘉義縣.md "wikilink")[布袋鎮過溝](../Page/布袋鎮.md "wikilink")，全長共計44.742公里（公路總局資料）。

## 行經行政區域

  - [雲林縣](../Page/雲林縣.md "wikilink")

<!-- end list -->

  - [斗南鎮](../Page/斗南鎮.md "wikilink")：斗南0.0k（叉路）
  - [大埤鄉](../Page/大埤鄉.md "wikilink")：大埤4.9k→雲嘉縣界8.7k

<!-- end list -->

  - [嘉義縣](../Page/嘉義縣.md "wikilink")

<!-- end list -->

  - [溪口鄉](../Page/溪口鄉_\(台灣\).md "wikilink")：溪口市區11.1k（左叉路）
  - [新港鄉](../Page/新港鄉.md "wikilink")：中庄（右終點叉路）→奉天宮19.2k（叉路）→新港20.0k（左叉路）→大客（叉路）→安和25.3k（與共線起點）
  - [六腳鄉](../Page/六腳鄉.md "wikilink")：雙涵（與共線終點）→朴子溪自行車道路口29.5k→蒜頭29.792k
  - [朴子市](../Page/朴子市.md "wikilink")：朴子公園（與共線起點）→朴子35.4k（叉路）
  - [東石鄉](../Page/東石鄉_\(台灣\).md "wikilink")：港墘38.5k（與共線終點）→39k與叉路
  - [布袋鎮](../Page/布袋鎮.md "wikilink")：半月
  - [東石鄉](../Page/東石鄉_\(台灣\).md "wikilink")：栗仔崙42.7k（叉路）
  - [布袋鎮](../Page/布袋鎮.md "wikilink")：過溝莊44.664k（叉路）

## 沿線風景

## 參見

## 相關連結

[Category:台灣縣道](../Category/台灣縣道.md "wikilink")
[Category:雲林縣道路](../Category/雲林縣道路.md "wikilink")
[Category:嘉義縣道路](../Category/嘉義縣道路.md "wikilink")