**瑞士首都球場**（**Stade de Suisse
Wankdorf**）又譯為**雲克多夫球場**，位於瑞士首都[伯爾尼](../Page/伯爾尼.md "wikilink")，是[伯尔尼年轻人体育俱乐部的主場](../Page/伯尔尼年轻人体育俱乐部.md "wikilink")，是現時瑞士第二大的全座席專業足球場。

瑞士首都球場是於[伯尔尼年轻人的舊主場雲克多夫球場](../Page/伯尔尼年轻人体育俱乐部.md "wikilink")（）重建而成，於2005年7月1日正式開幕使用。

瑞士首都球場是[2008年歐洲足球錦標賽其中一個比賽場地](../Page/2008年歐洲足球錦標賽.md "wikilink")。

## 外部連結

  - [球場官方網頁](http://www.stadedesuisse.ch/)

  - [球場官方網頁](http://www.stadedesuisse.ch/fr/index.htm)

[Category:瑞士足球場](../Category/瑞士足球場.md "wikilink")
[Category:2005年完工建築物](../Category/2005年完工建築物.md "wikilink")