**嘉峪关市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省下辖的](../Page/甘肃省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于甘肃省西北部，以[萬里長城西端](../Page/萬里長城.md "wikilink")[嘉峪关命名](../Page/嘉峪关.md "wikilink")。

## 名称由来

嘉峪关是古“[丝绸之路](../Page/丝绸之路.md "wikilink")”的交通要冲，又是[明代](../Page/明代.md "wikilink")[万里长城的西端起点](../Page/万里长城.md "wikilink")。在这里，两千多年前开辟的中国与西方经济文化交流的的“丝绸古道”及历代兵家征战的“古战场”烽燧依稀可见。这里是中国丝路文化和长城文化的交汇点，素有“河西重镇，边陲锁钥”之称。

## 建置沿革

嘉峪关市历史上无郡县设置，是1958年伴随着[中华人民共和国](../Page/中华人民共和国.md "wikilink")[“一五”重点建设项目](../Page/第一个五年计划.md "wikilink")“[酒泉钢铁公司](../Page/酒泉钢铁公司.md "wikilink")”的建设而逐步发展起来的一座新兴的现代化城市。1965年设市，1971年经[中华人民共和国国务院批准为](../Page/中华人民共和国国务院.md "wikilink")[地级市](../Page/地级市.md "wikilink")。全市总面积2935平方公里，其中城区规划面积80平方公里，已建成面积35平方公里。

## 地理

嘉峪关市位于甘肃省西北部，[河西走廊中部](../Page/河西走廊.md "wikilink")，东临[酒泉市](../Page/酒泉市.md "wikilink")，距省会[兰州](../Page/兰州.md "wikilink")776公里；西连石油城[玉门市](../Page/玉门市.md "wikilink")，至新疆[哈密](../Page/哈密.md "wikilink")650公里；南倚终年积雪的[祁连山与](../Page/祁连山.md "wikilink")[肃南裕固族自治县接壤](../Page/肃南裕固族自治县.md "wikilink")，与[青海相距](../Page/青海.md "wikilink")300余公里；北枕色如铸铜的黑山，与酒泉[金塔县](../Page/金塔县.md "wikilink")、[酒泉卫星发射基地和](../Page/酒泉卫星发射基地.md "wikilink")[内蒙](../Page/内蒙.md "wikilink")[额济纳旗相连接](../Page/额济纳旗.md "wikilink")，中部为酒泉绿洲西缘。中心位置为东经98°17'，北纬39°47'。全市海拔在1412－2722米之间，绿洲分布于海拔1450－1700米之间，城市中心海拔1462米。境内地势平坦，土地类型多样。城市的中西部多为戈壁，是市区和工业企业所在地；东南、东北为绿洲，是农业区，绿洲随地貌被戈壁分割为点、块、条、带状，占总土地面积的1.9％。嘉峪关市南面是[祁连山雪峰](../Page/祁连山.md "wikilink")，北面是[龙首山和](../Page/龙首山.md "wikilink")[马鬃山](../Page/马鬃山.md "wikilink")，由于地理位置的特殊性，自[秦](../Page/秦.md "wikilink")[汉以后的历代王朝都在这里驻扎兵力](../Page/汉.md "wikilink")。

### 气候

<center>

</center>

## 矿产资源

已探明[矿产资源有](../Page/矿产.md "wikilink")21个矿种，产地40多处，其中[铁](../Page/铁.md "wikilink")、[锰](../Page/锰.md "wikilink")、[铜](../Page/铜.md "wikilink")、[金](../Page/金.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[芒销](../Page/芒销.md "wikilink")、造型[粘土](../Page/粘土.md "wikilink")、[重晶石等为本市优势矿产](../Page/重晶石.md "wikilink")。[镜铁山矿铁矿石总储量为](../Page/镜铁山.md "wikilink")4.83亿吨，现已建成500万吨的生产能力，是国内最大的坑采冶金矿山；西沟石灰石矿储量为2.06亿吨，为露天开采，年产量80万吨；大草滩造型粘土总储量为9800万吨。邻近地区还有储量可观的芒销矿及可供开采的铬、锰、莹石、冰川石等矿藏。

## 政治

### 现任领导

<table>
<caption>嘉峪关市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党嘉峪关市委员会.md" title="wikilink">中国共产党<br />
嘉峪关市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/嘉峪关市人民代表大会.md" title="wikilink">嘉峪关市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/嘉峪关市人民政府.md" title="wikilink">嘉峪关市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议嘉峪关市委员会.md" title="wikilink">中国人民政治协商会议<br />
嘉峪关市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/李忠科.md" title="wikilink">李忠科</a>[1]</p></td>
<td><p><a href="../Page/梁洪涛_(1967年).md" title="wikilink">梁洪涛</a>（女）[2]</p></td>
<td><p><a href="../Page/丁巨胜.md" title="wikilink">丁巨胜</a>[3]</p></td>
<td><p><a href="../Page/边玉广.md" title="wikilink">边玉广</a>[4]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/甘肃省.md" title="wikilink">甘肃省</a><a href="../Page/白银市.md" title="wikilink">白银市</a></p></td>
<td><p>甘肃省<a href="../Page/酒泉市.md" title="wikilink">酒泉市</a></p></td>
<td><p>甘肃省<a href="../Page/靖远县.md" title="wikilink">靖远县</a></p></td>
<td><p>甘肃省<a href="../Page/金塔县.md" title="wikilink">金塔县</a></p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2018年1月</p></td>
<td><p>2016年10月</p></td>
<td><p>2017年6月</p></td>
<td><p>2016年10月</p></td>
</tr>
</tbody>
</table>

### 行政区划

<table>
<thead>
<tr class="header">
<th><p>嘉峪关市行政区划</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="position: relative;">
<p><a href="https://zh.wikipedia.org/wiki/File:Subdivisions_of_Jiayuguan,_China.png" title="fig:Subdivisions_of_Jiayuguan,_China.png">Subdivisions_of_Jiayuguan,_China.png</a>   </p></td>
</tr>
</tbody>
</table>

嘉峪关市是全国几个不设[市辖区的](../Page/市辖区.md "wikilink")[地级市之一](../Page/地级市.md "wikilink")，下辖[新城镇](../Page/新城镇_\(嘉峪关市\).md "wikilink")、[峪泉镇](../Page/峪泉镇.md "wikilink")、[文殊镇](../Page/文殊镇_\(嘉峪关市\).md "wikilink")3个[建制镇](../Page/建制镇.md "wikilink")、29个[居民委员会](../Page/居民委员会.md "wikilink")、17个[村民委员会](../Page/村民委员会.md "wikilink")。

2009年12月1日，嘉峪关市成立[雄关区](../Page/雄关区.md "wikilink")、[长城区](../Page/长城区.md "wikilink")、[镜铁区](../Page/镜铁区.md "wikilink")3个管理区，属于嘉峪关市设立的县级行政管理区，属于嘉峪关市人民政府派出机构，其性质相当于各地自行设立的开发区、高新区，而并非国务院批准、民政部在册的作为地方人民政府的市辖区。

长城、镜铁、雄关3个新区委员会（工作办公室），是在整合原城区工委（城区工作办公室）、郊区工委（郊区工作办公室）和工业园区管委会办公室社会管理职能的基础上组建的，作为市委、市政府的**派出机构**，县级建制，履行区一级党委、政府职能。区党委和区行政机构合署办公，职能上各有侧重，主要履行经济发展、市场监管、民生保障、社会管理、公共服务和党的建设等职能。

长城、镜铁、雄关3个区，都不能设立区人民政府、不能具有区人民代表大会常务委员会、区政协。

2012年，经报请市政府同意，撤销新华、五一、胜利、前进、建设、朝阳、峪苑7个[街道办事处](../Page/街道办事处.md "wikilink")，设立29个大社区\[5\]。

## 人口

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")231853人，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加72287人，增长45.30%。年平均人口增长率为3.81%。其中，男性人口为123806人，占53.40%；女性人口为108047人，占46.60%。人口性别比（以女性为100）为114.59。0－14岁人口为34160人，占14.73%；15－64岁人口为179940人，占77.61%；65岁及以上人口为17753人，占7.66%。\[6\]

在[三沙市成立之前](../Page/三沙市.md "wikilink")，嘉峪关市曾是中国人口最少的地级市。

## 交通

### 航空

[Jiayuguan_Airport_01.jpg](https://zh.wikipedia.org/wiki/File:Jiayuguan_Airport_01.jpg "fig:Jiayuguan_Airport_01.jpg")

  - [嘉峪关机场](../Page/嘉峪关机场.md "wikilink")，可达[兰州](../Page/兰州.md "wikilink")、[郑州](../Page/郑州.md "wikilink")、[北京](../Page/北京.md "wikilink")、[西安](../Page/西安.md "wikilink")、[上海](../Page/上海.md "wikilink")、[成都](../Page/成都.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[深圳](../Page/深圳.md "wikilink")、[乌鲁木齐](../Page/乌鲁木齐.md "wikilink")、[广州等](../Page/广州.md "wikilink")。\[7\]

[缩略图](https://zh.wikipedia.org/wiki/File:Jiayuguan_IGP4324.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Jiayuguan-028.JPG "fig:缩略图")

### 铁路

[Jiayuguan_Railway_Station_02.jpg](https://zh.wikipedia.org/wiki/File:Jiayuguan_Railway_Station_02.jpg "fig:Jiayuguan_Railway_Station_02.jpg")

  - [兰新铁路](../Page/兰新铁路.md "wikilink")、[兰新铁路第二双线](../Page/兰新铁路第二双线.md "wikilink")、[嘉镜铁路](../Page/嘉镜铁路.md "wikilink")、[嘉策铁路](../Page/嘉策铁路.md "wikilink")
  - [嘉峪关站](../Page/嘉峪关站.md "wikilink")

### 公路

  - [国道312线](../Page/国道312线.md "wikilink")、[连霍高速公路](../Page/连霍高速公路.md "wikilink")（G30）

## 工业发展

嘉峪关的铁矿、重晶石、石灰石、白云岩、造型粘土5种矿种居甘肃省前三位。全市已形成以冶金工业为主体，化工、电力、建材、机械、轻纺、食品为辅的工业体系。

## 城市发展

嘉峪关市是以举世闻名的“[天下第一雄关](../Page/天下第一雄关.md "wikilink")”—[嘉峪关命名的工业旅游城市](../Page/嘉峪关.md "wikilink")。又因他是西北最大的[钢铁联合企业](../Page/钢铁.md "wikilink")——[酒泉钢铁集团公司所在地](../Page/酒泉钢铁集团公司.md "wikilink")，故又被称为“[戈壁](../Page/戈壁.md "wikilink")[钢城](../Page/钢城.md "wikilink")”，并因此而享誉中外。

经过四十多年的建设，特别是改革开放以来，嘉峪关市的经济建设取得了长足发展，现已基本形成了以冶金工业为主导、商贸旅游业为支柱、城郊型农业为特色的经济发展格局。

## 参考文献

### 出处

### 网页

  - [城市概况](https://web.archive.org/web/20100513011657/http://www.jyg.gansu.gov.cn/chengshigaik/chengshigk.htm)

## 外部链接

  - [嘉峪关市旅游信息网](https://web.archive.org/web/20080821100656/http://www.jygtour.com.cn/)

{{-}}

[嘉峪关市](../Category/嘉峪关市.md "wikilink")
[Category:河西走廊](../Category/河西走廊.md "wikilink")
[Category:甘肃地级市](../Category/甘肃地级市.md "wikilink")
[陇](../Category/国家卫生城市.md "wikilink")
[5](../Category/全国文明城市.md "wikilink")

1.
2.
3.
4.
5.  嘉民发〔2012〕48号
6.  《嘉峪关市2010年第六次全国人口普查主要数据公报》
7.