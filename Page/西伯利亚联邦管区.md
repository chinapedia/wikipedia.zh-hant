**西伯利亞聯邦管區**（）位於[俄羅斯](../Page/俄羅斯.md "wikilink")[亞洲部份中部](../Page/亞洲.md "wikilink")，是目前俄罗斯的[聯邦管區之一](../Page/俄羅斯聯邦管區.md "wikilink")。

2018年11月5日，俄总统普京签署法令，[布里亚特共和国与](../Page/布里亚特共和国.md "wikilink")[外贝加尔边疆区划归](../Page/外贝加尔边疆区.md "wikilink")[远东联邦管区](../Page/远东联邦管区.md "wikilink")。

## 联邦管区下的联邦主体

| [Siberian_Federal_District_(numbered,_2018_composition).svg](https://zh.wikipedia.org/wiki/File:Siberian_Federal_District_\(numbered,_2018_composition\).svg "fig:Siberian_Federal_District_(numbered,_2018_composition).svg") |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| \#                                                                                                                                                                                                                                  |
| 1                                                                                                                                                                                                                                   |
| 2                                                                                                                                                                                                                                   |
| 3                                                                                                                                                                                                                                   |
| 4                                                                                                                                                                                                                                   |
| 5                                                                                                                                                                                                                                   |
| 6                                                                                                                                                                                                                                   |
| 7                                                                                                                                                                                                                                   |
| 8                                                                                                                                                                                                                                   |
| 9                                                                                                                                                                                                                                   |
| 10                                                                                                                                                                                                                                  |

總面積5,114,800 平方公里，人口20,064,274 (2002年)。現任總督為Anatoly Kvashnin。

注意不要與地理上[西伯利亞混淆](../Page/西伯利亞.md "wikilink")
(後者涵蓋了除[太平洋沿岸的整個俄羅斯亞洲部份](../Page/太平洋.md "wikilink"))。

## 参考文献

## 参见

  - [俄罗斯联邦管区](../Page/俄罗斯联邦管区.md "wikilink")
  - [西伯利亚](../Page/西伯利亚.md "wikilink")

{{-}}

[Category:俄羅斯聯邦管區](../Category/俄羅斯聯邦管區.md "wikilink")
[西伯利亚联邦管区](../Category/西伯利亚联邦管区.md "wikilink")
[Category:2000年俄羅斯建立](../Category/2000年俄羅斯建立.md "wikilink")