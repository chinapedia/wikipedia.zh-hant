[Zhenbao_island.png](https://zh.wikipedia.org/wiki/File:Zhenbao_island.png "fig:Zhenbao_island.png")
**珍宝岛**，满语**古斯库瓦郎**（；意为“军队营盘”），俄语**达曼斯基岛**（），位于[中国](../Page/中国.md "wikilink")[黑龙江省](../Page/黑龙江省.md "wikilink")[虎林市](../Page/虎林市.md "wikilink")，中俄界河[乌苏里江主航道中心线中国一侧](../Page/乌苏里江.md "wikilink")，全岛面积0.74平方公里，长约1700米，宽约500米。距西岸200米，距东岸300米。地理坐标为46°29'8.13"N
, 133°50'40.04"E。该岛在枯水期和中方陆地相连。\[1\]

## 珍宝岛事件

[中华人民共和国和](../Page/中华人民共和国.md "wikilink")[苏联曾因珍宝岛归属问题于](../Page/苏联.md "wikilink")1969年在岛上发生武装冲突。中苏双方均声称是对方先行开火，并都宣布在此次冲突中取得了胜利。1991年俄國承認該島歸屬中國。\[2\]

## 现状

2005年4月27日，中国[全国人大批准](../Page/全国人大.md "wikilink")《[中华人民共和国和俄罗斯联邦关于中俄国界东段的补充协定](../Page/中华人民共和国和俄罗斯联邦关于中俄国界东段的补充协定.md "wikilink")》。2005年5月20日，俄罗斯[国家杜马表决批准该协议](../Page/国家杜马.md "wikilink")。根据该协议，珍宝岛是中国的领土。

目前岛上环岛公路两旁的树丛下还埋着当年双方布下的2000多枚[反坦克地雷](../Page/反坦克地雷.md "wikilink")，大雨过后有些还会露出地表\[3\]。

## 参考文献

## 外部链接

  - [Chinese-Soviet border
    clashes](http://www.cnn.com/SPECIALS/cold.war/episodes/15/spotlight/)
    CNN 相关报道

## 参见

  - [中苏边界冲突](../Page/中苏边界冲突.md "wikilink")

{{-}}

[Category:黑龙江岛屿](../Category/黑龙江岛屿.md "wikilink")
[Category:鸡西地理](../Category/鸡西地理.md "wikilink")
[Category:虎林市](../Category/虎林市.md "wikilink")
[Category:中俄边界问题](../Category/中俄边界问题.md "wikilink")

1.  [珍宝岛简介](http://jingqu.travel.sohu.com/j-106329.shtml)
2.  А.Елизаветин，
    Переговоры　А.Н.Косыгина　и　Чжоу　Эньлая　в　Пекинском　Аэропорту,
    Проблемы Дальнего Востока，1992，№5，44-62.
    “3月15日以后，中国人实际控制了达曼斯基岛（注：即珍宝岛），苏方对此的反应很克制，开始，边防军还向中国人提出了正式抗议，后来连抗议也没再提了。”
3.