[武昌起义.png](https://zh.wikipedia.org/wiki/File:武昌起义.png "fig:武昌起义.png")
**陆军第八镇**，[清朝末年军队现代化改革之后的](../Page/清朝.md "wikilink")[新军编制之一](../Page/新军.md "wikilink")，相当于现在的师的规模。第八镇驻[湖北](../Page/湖北.md "wikilink")[武汉](../Page/武汉.md "wikilink")。辛亥革命前夕，[端方入川](../Page/端方.md "wikilink")，带走第十六协（第31标及32标）。[武昌起义第一枪是工程第八营正目](../Page/武昌起义.md "wikilink")（班长）[共进会代表](../Page/共进会.md "wikilink")[熊秉坤发出](../Page/熊秉坤.md "wikilink")。（新说是士兵[程正瀛发第一枪](../Page/程正瀛.md "wikilink")）该镇于1908年2月工程营首建气球队，是中国最原始的空军。

1896年湖广总督[张之洞按照德国制式改造湖北旧军为新式陆军](../Page/张之洞.md "wikilink")。[湖北新军经清政府中央练兵处统一编为陆军第八镇和](../Page/湖北新军.md "wikilink")[第二十一混成协](../Page/第二十一混成协.md "wikilink")。张之洞招募知识青年加入新军，这些知识青年日后成为新军中革命势力的积极分子。

辛亥革命之后，第八镇改编为湖北地方部队。

而**中央陆军第八师**是由保定陆军军官学校毕业生为核心于1913年组成的新部队，与第八镇没有渊源关系。1914年，第八师师长[王汝贤](../Page/王汝贤.md "wikilink")，该师参加追剿白朗起义军。1916年初，师长[李长泰](../Page/李长泰.md "wikilink")，第八师奉命入川，增援张敬尧对护国军作战。帝制失败以后，该师回驻天津马厂，1917年参加段祺瑞讨伐张勋复辟。

## 武昌起义发生时的编制

[武昌起义时期](../Page/武昌起义.md "wikilink")，其统制是[张彪](../Page/張彪_\(山西\).md "wikilink")，辖：

  - 步兵第十五协：协统[王得胜](../Page/王得胜.md "wikilink")，辖：29标（标统[张景良](../Page/张景良.md "wikilink")）、30标（标统[杨开甲](../Page/杨开甲.md "wikilink")）
  - 步兵第十六协：协统[邓成拔](../Page/邓成拔.md "wikilink")，辖：31标，32标
  - 马军第八标：标统[喻化龙](../Page/喻化龙.md "wikilink")
  - 炮兵第八标：
      - 炮兵第八标一营：管带杨揆一
      - 炮兵第八标二营：
      - 炮兵第八标三营：
  - 工程第八营：代理管带[阮荣发](../Page/阮荣发.md "wikilink")
  - 辎重第八营：管带[萧安国](../Page/萧安国.md "wikilink")
  - 宪兵营：管带[果兴阿](../Page/果兴阿.md "wikilink")
  - 教练营

## 第二十一混成协

在武汉，另有独立第二十一混成协，协统是[黎元洪](../Page/黎元洪.md "wikilink")。辖：步兵第41、42标，炮兵第11营，马军第11营，工程第11队，辎重第11队
　　

  - 步41标：第1营调宜昌，第2营调岳州，第3营驻武昌城内左旗下
  - 步42标：第1营驻汉口，第2营驻京汉铁路，第3营驻汉阳兵工厂
  - 马11营：驻武昌城外南湖
  - 炮11营：驻武昌城外塘角，管带[张正基](../Page/张正基.md "wikilink")，
      - 工11队：驻武昌城外塘角
      - 辎11队：驻武昌城外塘角

## 巡防营

[巡防营统领](../Page/巡防营.md "wikilink")[刘温玉](../Page/刘温玉.md "wikilink")。

## [武昌起义主要人物](../Page/武昌起义.md "wikilink")

  - [熊秉坤](../Page/熊秉坤.md "wikilink")，八镇工程第八营后队正目（班长）
  - [徐兆宾](../Page/徐兆宾.md "wikilink")，八镇工程第八营后队士兵
  - [金兆龙](../Page/金兆龙.md "wikilink")，八镇工程第八营后队正目
  - [程正瀛](../Page/程正瀛.md "wikilink")，八镇工程第八营后队士兵

<!-- end list -->

  - [吴兆麟](../Page/吴兆麟.md "wikilink")，八镇工程第八营左队队官
  - [罗炳顺](../Page/罗炳顺.md "wikilink")，八镇工程第八营左队士兵
  - [马荣](../Page/马荣.md "wikilink")，，八镇工程第八营左队士兵
  - [林振邦](../Page/林振邦.md "wikilink")，八镇工程第八营左队士兵

<!-- end list -->

  - [徐少斌](../Page/徐少斌.md "wikilink")，八镇工程第八营前队士兵
  - [吕中秋](../Page/吕中秋.md "wikilink")，八镇工程第八营前队士兵

<!-- end list -->

  - [邝名功](../Page/邝名功.md "wikilink")，八镇工程第八营右队排长，
  - [周荣棠](../Page/周荣棠.md "wikilink")，八镇工程第八营战马饲养员

<!-- end list -->

  - [蔡济民](../Page/蔡济民.md "wikilink")，八镇第十五协二十九标二营司务长（排长）
  - [吴醒汉](../Page/吴醒汉.md "wikilink")，八镇第十五协三十标
  - [张廷辅](../Page/张廷辅.md "wikilink")，八镇第十五协三十标排长
  - [杨洪胜](../Page/杨洪胜.md "wikilink")，八镇第十五协三十标正目（班长）
  - [张鹏程](../Page/张鹏程.md "wikilink")，八镇第十五协三十标，武昌城通湘门守备

<!-- end list -->

  - [蔡汉卿](../Page/蔡汉卿.md "wikilink")，八镇炮兵第八标
  - [杨揆一](../Page/杨揆一.md "wikilink")，八镇炮兵第八标管带
  - [孟华臣](../Page/孟华臣.md "wikilink")，八镇炮兵第八标
  - [赵楚屏](../Page/赵楚屏.md "wikilink")，八镇炮兵第八标
  - [徐万年](../Page/徐万年.md "wikilink")，八镇炮兵第八标，营代表。
  - [张文鼎](../Page/张文鼎.md "wikilink")，八镇炮兵第八标

<!-- end list -->

  - [彭楚藩](../Page/彭楚藩.md "wikilink")，八镇宪兵营，营代表。

<!-- end list -->

  - [蒋翊武](../Page/蒋翊武.md "wikilink")，第二十一混成协四十一标三营左队充正目，文学社负责人
  - [刘复基](../Page/刘复基.md "wikilink")，第二十一混成协四十一标三营左队士兵
  - [王世龙](../Page/王世龙.md "wikilink")，第二十一混成协四十一标
  - [阙龙](../Page/阙龙.md "wikilink")，，第二十一混成协四十一标
  - [何海鸣](../Page/何海鸣.md "wikilink")，第二十一混成协四十一标一营

<!-- end list -->

  - [胡玉珍](../Page/胡玉珍.md "wikilink")，第二十一混成协第四十二标一营士兵，文学社标代表
  - [邱文彬](../Page/邱文彬.md "wikilink")，第二十一混成协第四十二标一营士兵，文学社副代表
  - [赵承武](../Page/赵承武.md "wikilink")，第二十一混成协第四十二标第二营无线电学兵，文学社营代表

<!-- end list -->

  - [李鹏升](../Page/李鹏升.md "wikilink")，第二十一混成协辎重队

<!-- end list -->

  - [张振武](../Page/张振武.md "wikilink")，陆军小学

<!-- end list -->

  - [李翊东](../Page/李翊东.md "wikilink")， 陆军测绘学堂学生

## 注释

## 参考文献

  - 《中秋夺城夜－－民国开国第一战及功臣们》

[Category:新建陆军](../Category/新建陆军.md "wikilink")
[Category:武汉军事史](../Category/武汉军事史.md "wikilink")