**[城巴X](../Page/城巴.md "wikilink")11線**是[香港的一條巴士路線](../Page/香港.md "wikilink")，由[機場博覽館開往](../Page/亞洲國際博覽館.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")（摩頓台），提供亞洲國際博覽館開往旺角、油麻地及銅鑼灣的散場巴士服務。

現時此路線屬於「有線無車」狀態，被博覽館散場特別班次取代。

## 歷史

  - 2005年12月24日：開始投入服務，只在機場博覽館舉行大型活動時，於散場後提供服務。
  - 2008年8月5日：路線最後一次服務。

## 行車路線

[航展道](../Page/航展道.md "wikilink")、[航天城東路](../Page/航天城東路.md "wikilink")、[航天城迴旋處](../Page/航天城迴旋處.md "wikilink")、[暢連路](../Page/暢連路.md "wikilink")、[機場南交匯處](../Page/機場南交匯處.md "wikilink")、[機場路](../Page/機場路_\(香港\).md "wikilink")、[北大嶼山公路](../Page/北大嶼山公路.md "wikilink")、[青嶼幹線](../Page/青嶼幹線.md "wikilink")、[青衣西北交匯處](../Page/青衣西北交匯處.md "wikilink")、[長青公路](../Page/長青公路.md "wikilink")、[長青隧道](../Page/長青隧道.md "wikilink")、[青葵公路](../Page/青葵公路.md "wikilink")、[西九龍公路](../Page/西九龍公路.md "wikilink")、[連翔道](../Page/連翔道.md "wikilink")、[東京街西](../Page/東京街西.md "wikilink")、[通州街](../Page/通州街.md "wikilink")、[西九龍走廊](../Page/西九龍走廊.md "wikilink")、[太子道西](../Page/太子道西.md "wikilink")、[荔枝角道](../Page/荔枝角道.md "wikilink")、[彌敦道](../Page/彌敦道.md "wikilink")、[加士居道](../Page/加士居道.md "wikilink")、[漆咸道南](../Page/漆咸道南.md "wikilink")、[康莊道](../Page/康莊道.md "wikilink")、[紅磡海底隧道](../Page/紅磡海底隧道.md "wikilink")、[堅拿道天橋](../Page/堅拿道天橋.md "wikilink")、[堅拿道東](../Page/堅拿道東.md "wikilink")、[禮頓道](../Page/禮頓道.md "wikilink")、[摩利臣山道](../Page/摩利臣山道.md "wikilink")、[天樂里](../Page/天樂里.md "wikilink")、[軒尼詩道](../Page/軒尼詩道.md "wikilink")、[怡和街及](../Page/怡和街.md "wikilink")[銅鑼灣道](../Page/銅鑼灣道.md "wikilink")。

### 車站一覽

| [銅鑼灣](../Page/銅鑼灣.md "wikilink")（[摩頓台](../Page/摩頓台.md "wikilink")）方向 |
| -------------------------------------------------------------------- |
| **車站**                                                               |
| 1                                                                    |
| 2                                                                    |
| 3                                                                    |
| 4                                                                    |
| 5                                                                    |
| 6                                                                    |
| 7                                                                    |
| 8                                                                    |
| 9                                                                    |
| 10                                                                   |
| 11                                                                   |
| 12                                                                   |
| 13                                                                   |
| 14                                                                   |
| 15                                                                   |

## 車費

### 往銅鑼灣（摩頓台）

| 往銅鑼灣（摩頓台） |
| --------- |
| 亞洲國際博覽館   |
| HK$27.00  |
| HK$27.00  |
| HK$27.00  |
| HK$27.00  |

  - 全程：HK$27.00
  - 青嶼幹線收費廣場往銅鑼灣（摩頓台）：HK$26.00
  - 荔枝角道往銅鑼灣（摩頓台）：HK$10.00
  - 摩利臣山道往銅鑼灣（摩頓台）：HK$5.00

十二歲以下小童及六十五歲以上長者半價

本路線接受八達通繳費服務

## 上一代城巴X11線

第一代X11線於1997年6月1日至12月7日期間的假日服務，循環來往銅鑼灣（摩頓台）及青嶼幹線（東涌端），以應付青嶼幹線通車後出現的遊覽人潮。

## 參見

  - [亞洲國際博覽館特別巴士路線](../Page/亞洲國際博覽館特別巴士路線.md "wikilink")

## 參考資料

  - 《香港巴士回顧(2013)》，標題：赤鱲角機場巴士15載，尚線出版，ISBN 9789881648372，52-65頁
  - [681巴士總站 城巴X11線](http://www.681busterminal.com/x11.html)
  - [巴士資訊網
    城巴X11線](https://web.archive.org/web/20080929083520/http://www.i-busnet.com/busroute/ctb/ctbrx11.php)

## 外部連結

  - 城巴

<!-- end list -->

  - [城巴X11線](http://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=X11&route=X11&routetype=H&company=5&exactMatch=yes)

[X11](../Category/已取消城巴路線.md "wikilink")