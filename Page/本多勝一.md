**本多勝一**（）是一位[日本](../Page/日本.md "wikilink")[作家](../Page/作家.md "wikilink")、[记者](../Page/记者.md "wikilink")，出生於[日本](../Page/日本.md "wikilink")[長野縣](../Page/長野縣.md "wikilink")[下伊那郡大島村](../Page/下伊那郡.md "wikilink")（現[松川町](../Page/松川町.md "wikilink")）。

## 作品

  -
  -
  -
  - 　ISBN 4061838520　　1986年

  - 　ISBN 4061838539　　1986年

  - 　ISBN 4022608161　1986年

  - 　ISBN 402260817X　1986年

  - 　ISBN 4022545259　1967年

  - 　ISBN 4022591048　1974年

  - 　ISBN 4022608021　1981年

  - 　ISBN 4022608048　1981年

  - 　ISBN 402260803X　1981年

  - 　ISBN 4022608056　1981年

  - 　ISBN 4087506568　1983年

  - 　ISBN 4022608064　1984年

  - 　ISBN 4022608072　1982年

  - 　ISBN 4022608102　1983年

  - 　ISBN 4022608145　1984年

  - 　ISBN 4061834894　1985年

  - 　ISBN 4022608153　1986年

  - 　ISBN 4022607629　1993年

  - 　ISBN 4022608080　1982年

  - 　ISBN 4022608099　1983年

  - 　ISBN 4022608110　1984年

  - 　ISBN 4022608137　1984年

  - 　ISBN 4022557494　1987年

  - 　ISBN 4635330141　1987年

  - 　ISBN 4408007196　1988年

  -
  -
  -
  -
## 參見條目

  - [日本文学](../Page/日本文学.md "wikilink")

[Category:日本作家](../Category/日本作家.md "wikilink")
[Category:日本戰地記者](../Category/日本戰地記者.md "wikilink")
[Category:越南战争战地记者](../Category/越南战争战地记者.md "wikilink")
[Category:京都大學校友](../Category/京都大學校友.md "wikilink")
[Category:長野縣出身人物](../Category/長野縣出身人物.md "wikilink")
[Category:南京大屠殺肯定者](../Category/南京大屠殺肯定者.md "wikilink")
[Category:朝日新聞社人物](../Category/朝日新聞社人物.md "wikilink")