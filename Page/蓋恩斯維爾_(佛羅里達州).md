**蓋恩斯維爾**（）是[美國](../Page/美國.md "wikilink")[佛羅里達州](../Page/佛羅里達州.md "wikilink")[阿拉楚阿縣的縣治](../Page/阿拉楚阿縣_\(佛羅里達州\).md "wikilink")，也是蓋恩斯維爾[大都市统计区的核心城市](../Page/大都市统计区.md "wikilink")。面積127.2平方公里，2010年人口统计为124,354人。\[1\]

1869年4月15日建市。美国学生人数第六多的[佛羅里達大學以及](../Page/佛羅里達大學.md "wikilink")[Santa
Fe学院都位於這裡](../Page/Santa_Fe学院.md "wikilink")。

## 姐妹城市

  - [新羅西斯克](../Page/新羅西斯克.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/佛羅里達州城市.md "wikilink")

1.