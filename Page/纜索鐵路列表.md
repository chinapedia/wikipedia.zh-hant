這是以國家地方排列的**[纜索鐵路列表](../Page/纜索鐵路.md "wikilink")**。纜索鐵路由短距離的市區線到多節的山區鐵路都有。

## [中國](../Page/中國.md "wikilink")

### [大陸](../Page/中華人民共和國.md "wikilink")

[DocksideFunicular.jpg](https://zh.wikipedia.org/wiki/File:DocksideFunicular.jpg "fig:DocksideFunicular.jpg")

  - [重慶](../Page/重庆市.md "wikilink")（從浮趸運載內河船乘客到岸上）
  - [长寿缆车](../Page/长寿缆车.md "wikilink")（连接河街至望江路间280米长的陡坡，现免费运营）
  - [太原](../Page/太原市.md "wikilink")[西铭矿缆车](../Page/西铭矿.md "wikilink")（矿山职工通勤用，也对外开放）

### [香港](../Page/香港.md "wikilink")

  - [香港](../Page/香港.md "wikilink")[山頂纜車](../Page/山頂纜車.md "wikilink")
  - [香港海洋公園](../Page/香港海洋公園.md "wikilink")[海洋列車](../Page/海洋列車.md "wikilink")

## 日本

[Yakuri_Cable_Railway.JPG](https://zh.wikipedia.org/wiki/File:Yakuri_Cable_Railway.JPG "fig:Yakuri_Cable_Railway.JPG")

  - [筑波市](../Page/筑波市.md "wikilink") - 筑波山鋼索鐵道線
  - [八王子市](../Page/八王子市.md "wikilink") - 高尾登山電鐵
  - [青梅市](../Page/青梅市.md "wikilink") - 御岳登山鐵道
  - [箱根町](../Page/箱根町.md "wikilink") -
    [箱根登山鐵道鋼索線](../Page/箱根登山鐵道鋼索線.md "wikilink")
  - [伊勢原市](../Page/伊勢原市.md "wikilink") - 大山觀光電鐵大山鋼索線
  - [函南町](../Page/函南町.md "wikilink") - 伊豆箱根鐵道十国鋼索線
  - [立山町](../Page/立山町.md "wikilink") -
    [立山黑部貫光鋼索線](../Page/立山黑部阿爾卑斯山脈路線.md "wikilink")
    (黑部纜車)
  - 立山町 - [立山黑部貫光鋼索線](../Page/立山黑部阿爾卑斯山脈路線.md "wikilink") (立山纜車)
  - [大津市](../Page/大津市.md "wikilink") - 比叡山鐵道比叡山鐵道線
  - [京都市](../Page/左京區.md "wikilink") - 京福電氣鐵道鋼索線
  - 京都市 - 鞍馬山鋼索鐵道
  - [宮津市](../Page/宮津市.md "wikilink") - 天橋立鋼索鐵道
  - [生駒市](../Page/生駒市.md "wikilink") -
    [近鐵生駒鋼索線](../Page/近畿日本鐵道.md "wikilink")
  - [八尾市](../Page/八尾市.md "wikilink") - 近鐵西信貴鋼索線
  - [高野町](../Page/高野町.md "wikilink") -
    [南海鋼索線](../Page/南海電氣鐵道.md "wikilink")
  - [八幡市](../Page/八幡市.md "wikilink") -
    [京阪鋼索線](../Page/京阪電氣鐵道.md "wikilink")
  - [川西市](../Page/川西市.md "wikilink") - 能勢電鐵妙見鋼索線
  - [神戶市](../Page/灘區.md "wikilink") - 六甲摩耶鐵道六甲纜車
  - 神戶市 - 神戶市都市整備公社摩耶纜車
  - [高松市](../Page/高松市.md "wikilink") - 八栗纜車
  - [北九州市](../Page/八幡東區.md "wikilink") - 帆柱纜車
  - [別府市](../Page/別府市.md "wikilink") - 別府樂天地纜車

## [法國](../Page/法國.md "wikilink")

  - [貝桑松Funiculaire](../Page/貝桑松.md "wikilink") de Beauregard-Bregille
  - [埃維昂萊班](../Page/埃維昂萊班.md "wikilink"), le petit métro éviannais
  - Langres, Funiculaire Panoramique Sous-Bie
  - [勒阿弗爾](../Page/勒阿弗爾.md "wikilink") Funiculaire du Havre
  - Le Tréport
  - Les Arcs
  - [盧爾德](../Page/盧爾德.md "wikilink"), Funiculaire du Pic du Jer
  - [里昂](../Page/里昂.md "wikilink")
      - St Jean - St Just Funicular
      - St Jean - Fourvière Funicular
  - [巴黎](../Page/巴黎.md "wikilink") Montmartre纜車
  - [波城](../Page/波城.md "wikilink") Funiculaire de Pau
  - Saint-Hilaire du Touvet, Funiculaire de Saint-Hilaire du Touvet
  - Thonon-les-Bains
  - Tignes, Funiculaire Perce-Neige
  - Val-d'Isère, Funival

## [德國](../Page/德國.md "wikilink")

  - [巴登-巴登](../Page/巴登-巴登.md "wikilink") merkurbergbahn

## [澳大利亞](../Page/澳大利亞.md "wikilink")

  - 新南威爾士

<!-- end list -->

  - Katoomba, Katoomba Scenic Railway

## [俄罗斯](../Page/俄罗斯.md "wikilink")

  - [海参崴](../Page/海參崴.md "wikilink")

[纜索鐵路](../Category/纜索鐵路.md "wikilink")
[Category:鐵路運輸相關列表](../Category/鐵路運輸相關列表.md "wikilink")