**姜至奐**\[1\]（，），本名**趙泰圭**，[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。

2002年，通過音樂劇《Rocky
Horror》出道，並於2003年參演[MBC的](../Page/文化廣播_\(韓國\).md "wikilink")《[Nonstop
4](../Page/男生女生向前走.md "wikilink")》，成為他第一部電視劇。2005年，藉由主演《[加油！金順](../Page/加油！金順.md "wikilink")》而得到很高的人氣，並於當年底獲得MBC演技大賞的新人獎及優秀男演員獎。

## 演出作品

### 音樂劇

  - 2002年：《Rocky Horror》
  - 2004年：《Grease》
  - 2010年：《Cafe in》

### 電視劇

  - 2003年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[Nonstop
    4](../Page/男生女生向前走.md "wikilink")》
  - 2003年：[KBS](../Page/韓國放送公社.md "wikilink")《[夏日香氣](../Page/夏日香氣.md "wikilink")》
  - 2004年：KBS《[比花還美](../Page/比花還美.md "wikilink")》飾演 在植
  - 2004年：KBS《你會知道的》
  - 2004年：SBS《[最後的舞請與我一起](../Page/最後的舞請與我一起.md "wikilink")》飾演 申政奎
  - 2005年：MBC《[加油！金順](../Page/加油！金順.md "wikilink")》飾演 具再熙
  - 2006年：MBC《[火花遊戲](../Page/火花遊戲.md "wikilink")》飾演 羅仁宰
  - 2006年：MBC《[90天，相愛的時間](../Page/90天，相愛的時間.md "wikilink")》飾演 玄至錫
  - 2007年：KBS《[京城绯聞](../Page/京城绯聞.md "wikilink")》飾演 鮮-{于}-莞
  - 2008年：KBS《[快刀洪吉童](../Page/快刀洪吉童.md "wikilink")》飾演
    [洪吉童](../Page/洪吉童.md "wikilink")
  - 2009年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[Style](../Page/Style_\(韓國電視劇\).md "wikilink")》客串
  - 2010年：SBS《[Coffee House](../Page/Coffee_House.md "wikilink")》飾演 李辰秀
  - 2011年：SBS《[對我說謊試試](../Page/對我說謊試試.md "wikilink")》飾演 玄基俊
  - 2013年：SBS《[錢的化身](../Page/錢的化身.md "wikilink")》飾演 李次敦
  - 2014年：KBS《[Big Man](../Page/Big_Man.md "wikilink")》飾演 金智赫/姜智赫
  - 2016年：MBC《[Monster](../Page/Monster_\(電視劇\).md "wikilink")》飾演
    姜基嘆/李國哲
  - 2018年：[OCN](../Page/OCN.md "wikilink")《[小神的孩子們](../Page/小神的孩子們.md "wikilink")》飾演
    千在仁
  - 2018年：KBS《[就算死也喜歡](../Page/就算死也喜歡.md "wikilink")》飾演 白鎮尚

### 電影

  - 2006年：《訪問者》
  - 2006年：《那天的氣氛》
  - 2008年：《電影就是電影》
  - 2009年：《[七級公務員](../Page/七級公務員.md "wikilink")》
  - 2009年：《我眼裡的豆莢》飾 姜泰豐
  - 2012年：《[車警官](../Page/車警官.md "wikilink")》
  - 2014年：《射向太陽》
  - 2015年：《天降大咖》

### MV

  - 2009年：《So I'm loving you》
  - 2011年：Soulstar＆Wanted《愛不能結束》
  - 2012年：[金亨俊](../Page/金亨俊.md "wikilink")《Sorry I'm
    Sorry》（與[李己雨](../Page/李己雨.md "wikilink")）

### 綜藝

  - 2017年：[tvN](../Page/tvN.md "wikilink")《[島劍客](../Page/島劍客.md "wikilink")》(第21\~24集)
  - 2018年：[MBC](../Page/MBC.md "wikilink")《[真正的男人300](../Page/真正的男人300.md "wikilink")》，(第1\~集)

## 獲得獎項

### 2005年

  - [MBC演技大賞](../Page/MBC演技大賞.md "wikilink")—新人獎《加油！金順》
  - MBC演技大賞—優秀男演員賞《加油！金順》

### 2007年

  - [KBS演技大賞](../Page/KBS演技大賞.md "wikilink")—優秀演技賞（《京城绯聞》）
  - KBS演技大賞—最佳情侶賞（[韓志旼](../Page/韓志旼.md "wikilink")）

### 2008年

  - KBS演技大賞—最佳情侶賞《快刀洪吉童》（與[成宥利共同](../Page/成宥利.md "wikilink")）
  - KBS演技大賞—網路人氣賞《快刀洪吉童》
  - 第四十四屆[百想藝術大賞](../Page/百想藝術大賞.md "wikilink")－最佳人氣男演員奬《京城绯聞》
  - 第二十八屆－最佳男新人演員奬《電影就是電影》
  - 第七屆－最佳男新人演員獎《電影就是電影》
  - 第29屆韓國[青龍電影獎](../Page/青龍電影獎.md "wikilink")－最佳男新人演員獎《電影就是電影》（與[蘇志燮共同](../Page/蘇志燮.md "wikilink")）

### 2009年

  - 第45屆百想藝術大賞電影部門－男新人演技賞《電影就是電影》（與[蘇志燮共同](../Page/蘇志燮.md "wikilink")）
  - 第10屆－最佳男新人獎《電影就是電影》（與[蘇志燮共同](../Page/蘇志燮.md "wikilink")）
  - 第46屆[大鐘獎](../Page/大鐘獎.md "wikilink")－最佳男新人賞《七級公務員》

## 腳註

[Category:首爾特別市出身人物](../Category/首爾特別市出身人物.md "wikilink")
[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[Category:韓國電影演員](../Category/韓國電影演員.md "wikilink")
[Category:韓國音樂劇演員](../Category/韓國音樂劇演員.md "wikilink")

1.  [姜至奐搶玄彬迷
    拉抬金順收視率](http://www.libertytimes.com.tw/2006/new/mar/25/today-show8.htm)
     自由電子報