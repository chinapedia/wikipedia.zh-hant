**XYZ謀殺案**（***Crimewave***）即美國電影導演[-{zh-hans:萨姆·莱米;zh-hk:森·溫美;zh-tw:山姆·雷米;}-](../Page/山姆·雷米.md "wikilink")，開創黑色喜劇風格的電影作品。亦獲得少數支持者的喜愛及推崇，常被歸類於[次文化之列](../Page/次文化.md "wikilink")。

## 概述

[山姆·雷米與](../Page/山姆·雷米.md "wikilink")[獨立電影界竄紅的](../Page/獨立電影.md "wikilink")[喬·柯恩](../Page/柯恩兄弟.md "wikilink")（Joel
Coen）和[伊森·柯恩](../Page/柯恩兄弟.md "wikilink")（Ethan
Coen）首度合作，激發出黑色喜劇風格的電影。故事劇情圍繞在一群複雜古怪的人物，瘋狂荒謬的曲折、撲朔迷離的情節，略帶有著天馬行空般的異想空間，卻也隱含著深刻的人性剖析。

## 劇情簡介

兩位滅蟲專家受雇於一位做防盜系統生意的商人，命令他們殺掉其生意拍檔，並將罪名推向一為保安人員身上。為此，無辜的保安人員也此案件，被法院判處死刑。

此刻，這名保安人員正坐在執行死刑電椅上，講述著他的一切遭遇……

## 相關條目

  - [邪典電影](../Page/邪典電影.md "wikilink")

## 備註

<references/>

## 外部連結

  - [XYZ謀殺案（Crimewave）](http://www.imdb.com/title/tt0088967/)，{{〈}}[網路電影資料庫](../Page/網路電影資料庫.md "wikilink"){{〉}}

[Category:1985年電影](../Category/1985年電影.md "wikilink")
[Category:英语电影](../Category/英语电影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:美國喜劇片](../Category/美國喜劇片.md "wikilink")
[Category:美國驚悚片](../Category/美國驚悚片.md "wikilink")
[Category:美國犯罪片](../Category/美國犯罪片.md "wikilink")
[Category:环球影业电影](../Category/环球影业电影.md "wikilink")
[Category:山姆·雷米电影](../Category/山姆·雷米电影.md "wikilink")