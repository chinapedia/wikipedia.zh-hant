**阿卜杜勒-哈米德二世**（，）為[奥斯曼帝国的](../Page/奥斯曼帝国.md "wikilink")[苏丹和](../Page/苏丹_\(称谓\).md "wikilink")[哈里发](../Page/哈里发.md "wikilink")（1876年—1909年在位）。

阿卜杜勒-哈米德二世是苏丹[阿卜杜勒-迈吉德一世的幼子](../Page/阿卜杜勒-迈吉德一世.md "wikilink")。母亲为[亚美尼亚人](../Page/亚美尼亚人.md "wikilink")。在兄长[穆拉德五世苏丹被宣布患有](../Page/穆拉德五世.md "wikilink")[精神病而遭废黜后](../Page/精神病.md "wikilink")，在[新奥斯曼党人支持下](../Page/新奥斯曼党人.md "wikilink")，阿卜杜勒·哈米德二世被拥上王位（1876年8月31日）。

即位同年12月23日，在宰相[米德哈特帕夏主持下](../Page/米德哈特帕夏.md "wikilink")，颁布帝国第一部宪法（又称《[米德哈特宪法](../Page/米德哈特宪法.md "wikilink")》）。

在阿卜杜勒-哈米德二世统治时期，奥斯曼帝国继续受到[欧洲列强](../Page/欧洲.md "wikilink")，尤其是[俄国的宰割](../Page/俄国.md "wikilink")。俄国人为了报复奥斯曼帝国在[巴尔干地区对其](../Page/巴尔干地区.md "wikilink")[斯拉夫臣民的暴政](../Page/斯拉夫人.md "wikilink")，在阿卜杜勒－哈米德二世即位后的第二年向奥斯曼帝国宣战。像以前一样，腐朽的奥斯曼军队遭到一系列惨重失败。到1878年签署[圣斯特凡诺条约时](../Page/圣斯特凡诺条约.md "wikilink")，苏丹实际上已经失去了他的绝大部分欧洲领土。对俄作战失败后，他解散议会，将宰相[米德哈特放逐国外](../Page/米德哈特帕夏.md "wikilink")。恢复专制制度，建立恐怖统治，推行[泛伊斯兰主义](../Page/泛伊斯兰主义.md "wikilink")，迫害少数民族。

## 統治成績

[1877年的俄土戰爭失敗後](../Page/俄土戰爭_\(1877年-1878年\).md "wikilink")，阿卜杜勒-哈米德二世認為要挽救帝國衰落需要強而有力的領導，他不信任前任蘇丹的大臣及官員，逐漸削減他們在政府裡的角色，集中帝國的統治權於手中(違反憲法，30年沒召開一次議會)\[1\]，堅決抵制西方干預帝國事務，強調帝國的伊斯蘭特色\[2\]，並重申其[哈里發的地位](../Page/哈里發.md "wikilink")，呼籲穆斯林要團結一致，[泛伊斯蘭主義因此被大幅推廣](../Page/泛伊斯蘭主義.md "wikilink")。

阿卜杜勒-哈米德二世透過興建大量學校\[3\]、減少國債及重整架構一度中興及強化了帝國的地位\[4\]，但他的專制模式統治引起強烈反對，導致他的統治結束。

## 垮台

他經常以暴力鎮壓政治對手、屠殺少數民族。1876年發生了[保加利亞大屠殺](../Page/保加利亞大屠殺.md "wikilink")；1895年至1896年，在[西亚的奥斯曼军队对](../Page/西亚.md "wikilink")[亚美尼亚人进行了可怕的屠杀](../Page/哈米德大屠殺.md "wikilink")。尽管面临着国际上的强烈抗议，阿卜杜勒-哈米德二世並未采取措施制止这些暴行。

阿卜杜勒-哈米德二世在非穆斯林地區实行的政策是如此不得人心，以至人们称他为血腥的[苏丹](../Page/苏丹_\(称谓\).md "wikilink")。他覺得削弱舊的鄂圖曼方式只會導致毀滅，畏懼任何抑制他本人胡思亂想或是限制他權力的行動。他感到西方化的土耳其人日益威脅其統治，因此將一個叫[青年土耳其黨人的團體](../Page/青年土耳其黨人.md "wikilink")，以及西化改革者都驅逐出境，並抵制所有現代社會生活和文化生活的潮流。\[5\]

晚年他的暴君式的统治导致[土耳其人普遍的不满情绪](../Page/土耳其人.md "wikilink")，这种情绪支持[青年土耳其党的革命团体的成长](../Page/青年土耳其黨人.md "wikilink")。1908年7月青年土耳其党人领导的武装革命爆发，他被迫宣布恢复1876年宪法，再次召开议会。1909年4月27日，青年土耳其党人废黜了阿卜杜勒-哈米德二世([331事件](../Page/331事件.md "wikilink"))。阿卜杜勒-哈米德二世先被軟禁在[薩洛尼卡](../Page/薩洛尼卡.md "wikilink")，後轉移至[伊斯坦布爾](../Page/伊斯坦布爾.md "wikilink")，1918年2月10日去世。

## 軍事思想

蘇丹阿卜杜勒-哈米德二世採用大陸軍主義，不信任海軍，認為大規模、昂貴的海軍在俄土戰爭裡毫無用處，於是將大部分戰艦鎖定在[金角灣](../Page/金角灣.md "wikilink")，[戰艦在及後的三十年裡逐漸腐爛](../Page/戰艦.md "wikilink")。1908年青年土耳其人革命後，[聯合進步委員會欲發展一支強大的海軍力量](../Page/聯合進步委員會.md "wikilink")。於是為了收集公共捐獻購買船隻而成立奧斯曼海軍基金。

## 参考資料

### 腳註

### 書目

  - R.R.Palmer等著、孫福生等譯，《現代世界史（後篇）──1870年起》，台北：五南，2013。

[category:1842年出生](../Page/category:1842年出生.md "wikilink")
[category:1918年逝世](../Page/category:1918年逝世.md "wikilink")

[Category:奥斯曼帝国苏丹](../Category/奥斯曼帝国苏丹.md "wikilink")
[Category:哈里發](../Category/哈里發.md "wikilink")
[Category:切爾克斯裔土耳其人](../Category/切爾克斯裔土耳其人.md "wikilink")

1.
2.
3.
4.
5.  R.R.Palmer等著、孫福生等譯，《現代世界史（後篇）──1870年起》，頁63