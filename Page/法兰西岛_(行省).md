[Carte_de_l'Ile_de_France.svg](https://zh.wikipedia.org/wiki/File:Carte_de_l'Ile_de_France.svg "fig:Carte_de_l'Ile_de_France.svg")
**法兰西岛省**（法语：Province de
l'Île-de-France），[法国](../Page/法国.md "wikilink")[旧制度下的](../Page/旧制度.md "wikilink")**国王领地**，是自公元[十世纪起由](../Page/十世纪.md "wikilink")[卡佩王朝的国王们建立的](../Page/卡佩王朝.md "wikilink")。它的界限历史上一直在变动，其向西和（特别是）向北比今天的[法兰西岛延伸更远](../Page/法兰西岛.md "wikilink")，而东面和南面则相反。它提供了各[巴黎商人组织以一个经济特区](../Page/巴黎.md "wikilink")，而后者也助其确定了界限。

[法国大革命后](../Page/法国大革命.md "wikilink")，它被拆分成了三个[省](../Page/省_\(法国\).md "wikilink")：[塞纳省](../Page/塞纳省.md "wikilink")（已废）、[塞纳-瓦兹省](../Page/塞纳-瓦兹省.md "wikilink")（已废）和[塞纳-马恩省](../Page/塞纳-马恩省.md "wikilink")。

[Category:法国行省](../Category/法国行省.md "wikilink")