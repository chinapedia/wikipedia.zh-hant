[HK_長洲公共圖書館_Cheung_Chau_Public_Library_金庸_Louis_Cha_Leung-yung_射鵰英雄傳_bookbacks_Dec-2013.JPG](https://zh.wikipedia.org/wiki/File:HK_長洲公共圖書館_Cheung_Chau_Public_Library_金庸_Louis_Cha_Leung-yung_射鵰英雄傳_bookbacks_Dec-2013.JPG "fig:HK_長洲公共圖書館_Cheung_Chau_Public_Library_金庸_Louis_Cha_Leung-yung_射鵰英雄傳_bookbacks_Dec-2013.JPG")
**武俠小說**，是[中國文學中的](../Page/中國文學.md "wikilink")[大眾文學](../Page/大眾文學.md "wikilink")，是自民國以後风行於華人地區中的一種結合歷史與幻想的[小說類型](../Page/小說.md "wikilink")，追溯起源可至唐代傳奇虬髯客传之前，故事主要描寫武林幫派之間的爭鬥、江湖中人之間的恩怨情仇，民族國家的俠義精神。

武侠小说的历史背景多为[中国古代](../Page/中国历史.md "wikilink")，尤以改朝换代、汉族与外族战争之乱世为多，故而[时势造英雄](../Page/时势造英雄.md "wikilink")；也有以描述帮派斗争为主，而没有强调时代。盖因乱世之中，人更加需要以武力证明自身价值。以侠义为主题，武功为手段，是武侠小说独特的文化特征，兼以中国古代[医学](../Page/医学.md "wikilink")、[佛学](../Page/佛学.md "wikilink")、[道家](../Page/道家.md "wikilink")、[易术及各种神秘学等](../Page/易术.md "wikilink")。因此，武侠小说中人物的[价值观](../Page/价值观.md "wikilink")，乃至于整部作品的精神基调，是建立在行侠仗义的基本思想上的。

## 武侠文化

武侠文化的存在，是中国特有的历史和文化现象，其出现及崛起有复杂的社会背景、文化和心理因素。当下，武侠文化似乎正出现一种热热闹闹的场面，说它是复兴或崛起也不为过，无论是武侠文学的创作或者是各种武侠电影电视的热炒，武侠出版、武侠网络小说大量的涌现等等，都凸现出这一趋势的迅猛发展。[华罗庚对梁羽生说过一句话](../Page/华罗庚.md "wikilink")：“武侠小说是成年人的[童话](../Page/童话.md "wikilink")”。

## 歷史

### 古典武俠時期

中國最早出現的長篇武俠小說為[清代古典名著](../Page/清代.md "wikilink")《[三俠五義](../Page/三俠五義.md "wikilink")》，這是因為晚清的社會生活矛盾，體現底層人民意願的英雄俠士和體現市民上層理想的清官奇妙地在小說裏結合，以集合的方式反映了晚清社會的市俗願望。

### 民國武俠時期

#### 首次出現

「武俠小說」專稱的首次出現於1915年12月的《小说大观》（季刊）（第三集）（上海，[包天笑主编](../Page/包天笑.md "wikilink")）上，[林纾](../Page/林纾.md "wikilink")（琴南）的文言文短篇《傅眉史》首次以“武侠小说”的专称发表。\[1\]

### 近代武俠時期

#### 新派武俠

「新派武俠小說」是指自1950年代開始以[金庸](../Page/金庸.md "wikilink")、[古龍及](../Page/古龍.md "wikilink")[梁羽生等港台作家為代表所創作的武俠小說](../Page/梁羽生.md "wikilink")，以區別於戰前[還珠樓主及](../Page/還珠樓主.md "wikilink")[平江不肖生等人所寫的武俠小說](../Page/平江不肖生.md "wikilink")。新派武俠小說在傳統武俠小說的基礎上加上現代（甚至西方）的元素，不囿於傳統觀念及陳腔濫調，用較現代的文字及角度去演繹中國歷史。\[2\]

新派武俠的出現，主要原因在市場的需求，民眾的娛樂意識，隨著物質生活水平的提高而強化，迫切需要新奇的作品來消遣，武俠小說正是充滿了想像，詭譎多變的作品。

香港大武俠時期湧現相當多的作家，在五十年代於報紙之間連載，如[牟松庭](../Page/牟松庭.md "wikilink")、蹄風、黃鷹、牟松庭、龍乘風、西門丁、張夢還、馬雲；還有金鋒、江一明、避秦樓主、風雨樓主、高峰、石沖。

新派武俠在思想上，「俠」的觀念進化為獨立人格的堅持，這是對[民主思想](../Page/民主.md "wikilink")、[自由精神的呼應](../Page/自由.md "wikilink")，[俠客成為一種新理想人格的化身](../Page/俠客.md "wikilink")。在藝術上，作者大多從外國文學中吸取新穎的表現技巧，常把[歷史](../Page/歷史.md "wikilink")、[愛情](../Page/愛情.md "wikilink")、[神魔](../Page/神魔.md "wikilink")、[武俠四者結合起來](../Page/武俠.md "wikilink")，這些[俠客在其行俠的過程中](../Page/俠客.md "wikilink")，滲入了豐富的思想感情、內心矛盾和人性變化。在故事形式上，一改舊派武俠結構鬆散、枝蔓雜出的問題，情節較為緊湊，而且曲折、奇譎、多變，且文字講究趣味性和意境。

新派武俠的崛起，還在於它們具有濃重豐厚的傳統文化涵量。不但作品用傳統小說的語言寫成，而且舉凡[中國傳統文化中一切最具特色的成分](../Page/中國傳統文化.md "wikilink")，如[詩](../Page/詩.md "wikilink")[詞](../Page/詞.md "wikilink")[曲](../Page/曲.md "wikilink")[賦](../Page/賦.md "wikilink")、[琴](../Page/琴.md "wikilink")[棋](../Page/棋.md "wikilink")[書](../Page/書.md "wikilink")[畫](../Page/畫.md "wikilink")、[儒](../Page/儒.md "wikilink")[道](../Page/道.md "wikilink")[墨](../Page/墨.md "wikilink")[釋](../Page/釋.md "wikilink")、[醫](../Page/醫.md "wikilink")[卜](../Page/卜.md "wikilink")[星](../Page/星.md "wikilink")[相](../Page/相.md "wikilink")、[傳說掌故](../Page/傳說.md "wikilink")、[典](../Page/典.md "wikilink")[庫](../Page/庫.md "wikilink")[文物](../Page/文物.md "wikilink")、[風俗民情](../Page/風俗.md "wikilink")……無不與故事情節的展開，武技較量的描寫，人物性格的刻畫，作品題旨的展示，相融合滲透，成為有機的組成部分，使人嘆為觀止。它們構成了新派武俠小說的一大優勢，從而使自己在中華民族和海外華人社會中深深扎下根來。

新派武俠小說內容豐富且變化萬千，極富娛樂價值，傳統文化的內蘊，也使得閱讀作品的華人更能融入其中，感動深刻。

#### 港台兩地

值得注意的是，台灣武俠小說與香港武俠小說的發展，雙方內容特色自成一格。

香港武俠名家[金庸](../Page/金庸.md "wikilink")、[梁羽生等的武俠作品](../Page/梁羽生.md "wikilink")，其創作背景多與中國歷史緊密結合，作品中的歷史痕跡相當濃厚；而台灣武俠作家或許是基於當時政治禁忌的關係\[3\]，或許是作者個人對武林世界、對中國的想像不同，作品的創作背景少與中國歷史相結合，故事的歷史背景模糊，注重故事情節的奇詭曲折，較偏於奇情武俠。

#### 繼續發展

武俠小說風靡於海內外華人文壇，至今讀者仍熱忱未衰。兼且由於[電視](../Page/電視.md "wikilink")、[電腦相繼普及](../Page/電腦.md "wikilink")，武俠小說超越了以往只以文字符號作為表現手段，更以多元、多種的符號形式，配合聲音、彩色畫面、彩色圖片、動作等，在社會上掀起一股前所未有的武俠熱潮，諸如最近風行的[電腦遊戲軟體](../Page/電腦遊戲.md "wikilink")、武俠[漫畫](../Page/漫畫.md "wikilink")、武俠[電影](../Page/電影.md "wikilink")、武俠[電視劇等都是](../Page/電視劇.md "wikilink")。

## 著名的武侠小说作者

當中新派武俠以金古梁温黄（金庸、古龍、梁羽生、溫瑞安、黃易）最出眾。

<table>
<tbody>
<tr class="odd">
<td><p>:; 中国大陆</p>
<ul>
<li><a href="../Page/平江不肖生.md" title="wikilink">平江不肖生</a>（<strong>南向北赵</strong>之一）</li>
<li><a href="../Page/赵焕亭.md" title="wikilink">赵焕亭</a>（<strong>南向北赵</strong>之一）</li>
<li><a href="../Page/还珠楼主.md" title="wikilink">还珠楼主</a>（<strong>北派五大家</strong>之一）</li>
<li><a href="../Page/白羽.md" title="wikilink">白羽</a>（<strong>北派五大家</strong>之一）</li>
<li><a href="../Page/王度庐.md" title="wikilink">王度庐</a>（<strong>北派五大家</strong>之一）</li>
<li><a href="../Page/郑证因.md" title="wikilink">郑证因</a>（<strong>北派五大家</strong>之一）</li>
<li><a href="../Page/朱贞木.md" title="wikilink">朱贞木</a>（<strong>北派五大家</strong>之一）</li>
<li><a href="../Page/顾明道.md" title="wikilink">顾明道</a></li>
<li><a href="../Page/姚民哀.md" title="wikilink">姚民哀</a></li>
<li><a href="../Page/徐春羽.md" title="wikilink">徐春羽</a></li>
<li><a href="../Page/沧浪客.md" title="wikilink">沧浪客</a></li>
<li><a href="../Page/戊戟.md" title="wikilink">戊戟</a></li>
<li><a href="../Page/小椴.md" title="wikilink">小椴</a></li>
<li><a href="../Page/沧月.md" title="wikilink">沧月</a></li>
<li><a href="../Page/步非烟.md" title="wikilink">步非烟</a></li>
<li><a href="../Page/凤歌.md" title="wikilink">凤歌</a></li>
<li><a href="../Page/慕容美.md" title="wikilink">慕容美</a></li>
<li><a href="../Page/龍人.md" title="wikilink">龍人</a></li>
<li><a href="../Page/趙晨光.md" title="wikilink">趙晨光</a></li>
</ul></td>
<td><p>:; 香港</p>
<ul>
<li><a href="../Page/邓羽公.md" title="wikilink">邓羽公</a>（香港武侠开创者）</li>
<li><a href="../Page/高小峰.md" title="wikilink">高小峰</a>（<strong>广派</strong>开创者）</li>
<li><a href="../Page/毛聊生.md" title="wikilink">毛聊生</a></li>
<li><a href="../Page/我是山人.md" title="wikilink">我是山人</a></li>
<li><a href="../Page/我佛山人.md" title="wikilink">我佛山人</a></li>
<li><a href="../Page/念佛山人.md" title="wikilink">念佛山人</a></li>
<li><a href="../Page/梁羽生.md" title="wikilink">梁羽生</a>（<strong>新派</strong>开创者）</li>
<li><a href="../Page/金庸.md" title="wikilink">金庸</a></li>
<li><a href="../Page/倪匡.md" title="wikilink">倪匡</a></li>
<li><a href="../Page/牟松亭.md" title="wikilink">牟松亭</a></li>
<li><a href="../Page/商清.md" title="wikilink">商清</a></li>
<li><a href="../Page/蹄风.md" title="wikilink">蹄风</a></li>
<li><a href="../Page/林梦.md" title="wikilink">林梦</a></li>
<li><a href="../Page/张梦还.md" title="wikilink">张梦还</a></li>
<li><a href="../Page/江一明.md" title="wikilink">江一明</a></li>
<li><a href="../Page/风雨楼主.md" title="wikilink">风雨楼主</a></li>
<li><a href="../Page/杨剑豪.md" title="wikilink">杨剑豪</a></li>
<li><a href="../Page/唐斐.md" title="wikilink">唐斐</a></li>
<li><a href="../Page/温瑞安.md" title="wikilink">温瑞安</a></li>
<li><a href="../Page/西门丁.md" title="wikilink">西门丁</a>（<strong>新三剑客</strong>之一）</li>
<li><a href="../Page/黄鹰.md" title="wikilink">黄鹰</a>（<strong>新三剑客</strong>之一）</li>
<li><a href="../Page/龙乘风.md" title="wikilink">龙乘风</a>（<strong>新三剑客</strong>之一）</li>
<li><a href="../Page/黃易_(香港).md" title="wikilink">黃易</a></li>
<li><a href="../Page/喬靖夫.md" title="wikilink">喬靖夫</a></li>
<li><a href="../Page/周顯.md" title="wikilink">周顯</a></li>
</ul></td>
<td><p>:; 台灣</p>
<ul>
<li><a href="../Page/柳殘陽.md" title="wikilink">柳殘陽</a></li>
<li><a href="../Page/張大春.md" title="wikilink">張大春</a></li>
<li><a href="../Page/臥龍生.md" title="wikilink">臥龍生</a>（<strong>三剑客</strong>之一）</li>
<li><a href="../Page/司馬翎.md" title="wikilink">司馬翎</a>（<strong>三剑客</strong>之一）</li>
<li><a href="../Page/諸葛青雲.md" title="wikilink">諸葛青雲</a>（<strong>三剑客</strong>之一）</li>
<li><a href="../Page/獨孤红.md" title="wikilink">獨孤红</a></li>
<li><a href="../Page/雪雁.md" title="wikilink">雪雁</a></li>
<li><a href="../Page/郎红浣.md" title="wikilink">郎红浣</a></li>
<li><a href="../Page/陳青雲.md" title="wikilink">陳青雲</a></li>
<li><a href="../Page/伴霞樓主.md" title="wikilink">伴霞樓主</a></li>
<li><a href="../Page/古龍.md" title="wikilink">古龍</a></li>
<li><a href="../Page/萧逸.md" title="wikilink">萧逸</a></li>
<li><a href="../Page/上官鼎.md" title="wikilink">上官鼎</a></li>
<li><a href="../Page/东方玉.md" title="wikilink">东方玉</a></li>
<li><a href="../Page/慕容美.md" title="wikilink">慕容美</a></li>
<li><a href="../Page/雲中岳.md" title="wikilink">雲中-{岳}-</a></li>
<li><a href="../Page/曹若冰.md" title="wikilink">曹若冰</a></li>
<li><a href="../Page/易容.md" title="wikilink">易容</a></li>
<li><a href="../Page/宇文瑤璣.md" title="wikilink">宇文瑤璣</a></li>
<li><a href="../Page/東方英.md" title="wikilink">東方英</a></li>
<li><a href="../Page/東方白.md" title="wikilink">東方白</a></li>
<li><a href="../Page/田歌.md" title="wikilink">田歌</a></li>
<li><a href="../Page/劍虹.md" title="wikilink">劍虹</a></li>
<li><a href="../Page/陆鱼.md" title="wikilink">陆鱼</a></li>
<li><a href="../Page/秦红.md" title="wikilink">秦红</a></li>
<li><a href="../Page/奇儒.md" title="wikilink">奇儒</a></li>
<li><a href="../Page/孫曉.md" title="wikilink">孫曉</a></li>
<li><a href="../Page/鄭丰.md" title="wikilink">鄭丰</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 资料来源

## 参见

  - [骑士文学](../Page/骑士文学.md "wikilink")

## 外部連結

  - 鄭樹森：〈[大眾文學．敘事．文類──武俠小說札記三則](http://www.cuhk.edu.hk/ics/21c/media/articles/c004-199100029.pdf)〉。
  - 盧建榮：〈\[<http://readopac1.ncl.edu.tw/nclJournal/search/detail.jsp?sysId=0006544607&dtdId=000040&search_type=detail&la=ch&checked=&unchecked=0010006557509,0020006544607,0030006547190,0040006412183,0050006412047,0060006181206,0070006098501,0080005924602,0090005739320,0100004825367,0110004806734,0120004807286,0130004778020,0140004750246,0150004730098,0160004716395,0170004684867,0180004706449,0190004651444,0200005628798>,
    臺灣武俠小說中的歷史建構(1951-1991)\]〉。

[W](../Category/小說類型.md "wikilink") [\*](../Category/武侠小说.md "wikilink")

1.  周清霖(1996年)，《中国武侠小说名著大观》，前言，第一页，上海书店出版社
2.  有關新舊兩派的異同，參[岡崎由美](../Page/岡崎由美.md "wikilink")，2002：頁208-70。
3.  葉洪生，〈中國武俠小說史論〉，收錄於《武俠小說談藝錄－葉洪生論劍》，台北，聯經出版社，1994 年 11 月初版，頁 75。