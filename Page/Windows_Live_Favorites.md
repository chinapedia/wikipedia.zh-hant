**Windows Live
Favorites**是[微軟推出的一系列線上應用服務的其中一項軟體服務](../Page/微軟.md "wikilink")。這項服務可以讓用戶儲存[我的最愛在網路伺服器上](../Page/我的最愛.md "wikilink")。用戶不管到那一台電腦上，只要有網路連線，用戶都可以使用事先儲存在網路上的我的最愛。走到那裡，用到那裡。2005年11月1日這個產品開始了[Beta版本的測試](../Page/Beta版本.md "wikilink")。

**Windows Live Favorites Tab on Live Messenger:**
Windows Live Favorites Messenger Tab 最近在Messenger US
Market发布了Beta版本。美国Messenger用户可以看见有一个新的Tab在4月20日的时候出现。特色功能如下：

  - Favories Tab将Windows Live
    Favorites和Messenger集成，这样就不用再次登入就可以直接在Messenger里面浏览我的最愛。
  - Windows Live Favorites可以让你把IE, Netscape, Firefox等各种浏览器的收藏夹上传到Windows
    Live Favorites，所以可以把所有最愛統一。
  - Favorites Tab
    有搜索功能，这个也是这个在线收藏夹最有特色的地方，每鍵入一个字符都会有相应的搜索结果出现，这种动态的效果和Windows
    Live Messenger的搜索如出一辙。

## 外部連結

  - [Windows Live Favorites 官方網站](http://favorites.live.com/)

[Category:Windows Live](../Category/Windows_Live.md "wikilink")
[Category:Web 2.0](../Category/Web_2.0.md "wikilink")