**基督教香港信義會心誠中學**（，簡稱：FLSS、心誠），是位於[香港](../Page/香港.md "wikilink")[新界東](../Page/新界東.md "wikilink")[北區](../Page/北區_\(香港\).md "wikilink")[粉嶺](../Page/粉嶺.md "wikilink")[粉嶺圍的一間](../Page/粉嶺圍.md "wikilink")[基督教](../Page/基督教.md "wikilink")[直資](../Page/直資.md "wikilink")[中學](../Page/中學.md "wikilink")。學校為粉嶺區內第一所中學，由基督教香港[信義會於](../Page/信義會.md "wikilink")1964年創辦，並由教會內的學校教育部統籌管理，辦學宗旨是「提供優質全人教育、服務社會、見證基督」。現任校長是麥沃華。校內之基督教香港信義會榮光堂有90多年歷史，被[古物古蹟辦事處列為](../Page/古物古蹟辦事處.md "wikilink")[香港三級歷史建築](../Page/香港三級歷史建築.md "wikilink")。\[1\]</ref>

## 簡史

1978年，政府實施[九年免費教育](../Page/九年免費教育.md "wikilink")，由於當時公營學位不足，政府向私立中學大量購買學位。基督教香港信義會心誠中學由一所全[私立學校轉為政府買位中學](../Page/私立學校.md "wikilink")。

1990年9月，與教育署（[教育局前身](../Page/教育局_\(香港\).md "wikilink")）簽訂了新買位合約，為期10年，於2000年終止。

1997年，校方向當時的[教育署申請加入直資計劃](../Page/教育署.md "wikilink")，並獲得批准，在1998年9月1日正式成為新界區第一所直資學校。

2011年，完成校舍的擴建工程，增加了一幢樓高6層的大型校舍及一座可容納1200至1500人同時可用作室內體育館及演奏廳的劇院式[禮堂](../Page/禮堂.md "wikilink")。

## 學校活動及架構

學校採用平衡班級結構，開辦中一至中六共6級，每級開設5至6班，總共31班。該校設有[學生會及](../Page/學生會.md "wikilink")「仁、愛、信、義」四社以幫助學生發展領導才能和組織能力。該校有50多個學會，亦有四個制服團體，包括[童軍](../Page/童軍.md "wikilink")、[基督少年軍和](../Page/基督少年軍.md "wikilink")[紅十字會](../Page/紅十字會.md "wikilink")。而同學每個學期須參加2次長跑活動，校長希望可讓同學有「成功、快樂，同時亦會經歷失敗、沮喪的體驗」。\[2\]
而體育方面，在手球和箭藝方面在學界有相當不俗的成績。\[3\]

## 校內設施

中學總面積共17,000平方米。設有苗圃、綠化天台、地理室、5個科學[實驗室及](../Page/實驗室.md "wikilink")[太陽能和](../Page/太陽能.md "wikilink")[風力發電設施](../Page/風力發電.md "wikilink")。校園設有不公開之無綫上網，並擁有可作全校廣播的校園電視台。所有課室及特別室均均有[空調設備](../Page/空調.md "wikilink")、LCD投影機、銀幕、影音器材、已接駁無線寬頻和中央廣播的電腦系統，加上大型電腦室、多媒體學習室及語言學習中心。\[4\]

校內的劇院式禮堂超過900平方米，可容納1,200人。舞台面積約200平方米，可以作為[管樂團](../Page/管樂團.md "wikilink")、大型戲劇和其他藝術表演場地，新禮堂內設有符合標準規格的室內籃球場，排球場和羽毛球場。擴建校舍部份更設有露天廣場，加上原禮堂的多用途體藝場地以及總面積超過700平方米的新落成有蓋[操場](../Page/操場.md "wikilink")，可提供充足場地及設施。

校園內有4個多用途球場，可供[手球](../Page/手球.md "wikilink")、小型[足球](../Page/足球.md "wikilink")、[排球](../Page/排球.md "wikilink")、[籃球](../Page/籃球.md "wikilink")、[羽毛球和](../Page/羽毛球.md "wikilink")[網球練習與比賽之用](../Page/網球.md "wikilink")。原有200米環型練習跑道，在新校舍落成後接通新的400米練習跑道，可提供全長共600米的田徑練習跑道。

學校另外設有由信義會開辦的[榮光堂教會](../Page/榮光堂.md "wikilink")，為學生提供校牧室及教堂設施進行娛樂活動。

## 主要行政人員

校長：麥沃華 副校長：黎志榮、廖慶新 助理校長:李志明 升學及就業輔導主任：吳華強 訓導主任：呂明德、敖卓綾

## 開設科目

### 中一至中三

|          |                                                       |
| -------- | ----------------------------------------------------- |
| 以中文為教學語言 | 中國語文、體育、視覺藝術、宗教與倫理、普通話、音樂、歷史與文化\*、綜合人文科、世界歷史\#、中國歷史\# |
| 以英文為教學語言 | 英國語文、英國文學、數學、綜合科學\* 、電腦、生物\#、化學\#、物理\#                |

\*代表只限中一及中二 、 \#代表只限中三

### 中四至中六新高中課程

|          |                                                                |
| -------- | -------------------------------------------------------------- |
| 以中文為教學語言 | 中國語文\*、中國歷史、中國文學、體育\*、體育文憑、視覺藝術、旅遊與款待、歷史、音樂 、通識\*              |
| 以英文為教學語言 | 英國語文\*、數學\*、歷史、地理、物理、化學、生物、資訊及通訊科技、經濟、企業會計及財務概論、組合科學，數學選修單元一及二 |

\*代表必修科目（高中），學生須另外選擇2個選修科

## 著名/傑出校友

  - [陳景輝](../Page/陳景輝.md "wikilink")：香港政治及文化評論人
  - [黃乃平](../Page/黃乃平.md "wikilink")：[太平紳士](../Page/太平紳士.md "wikilink")
  - [趙鳳儀](../Page/趙鳳儀.md "wikilink")：[海天堂龜苓膏集團創辦人](../Page/海天堂.md "wikilink")
  - [吳苑清](../Page/吳苑清.md "wikilink")：[海天堂龜苓膏集團行政總裁](../Page/海天堂.md "wikilink")
  - [周永康](../Page/周永康_\(香港\).md "wikilink")
    ：前[香港學聯](../Page/香港專上學生聯會.md "wikilink")[秘書長](../Page/秘書長.md "wikilink")
  - [余文樂](../Page/余文樂.md "wikilink")：藝人
  - [莊元生](../Page/莊元生.md "wikilink")：知名文化人，[MyRadio主持人](../Page/MyRadio.md "wikilink")
  - [鄧汝超](../Page/鄧汝超.md "wikilink")：前無綫電視男演員
  - 余智成：前新界東區議員
  - [焦媛](../Page/焦媛.md "wikilink")：香港女演員（首屆預科畢業）

## 附近

  - [港鐵](../Page/港鐵.md "wikilink")[粉嶺站](../Page/粉嶺站.md "wikilink")
  - [粉嶺圍](../Page/粉嶺圍.md "wikilink")
  - [北區政府合署](../Page/北區政府合署.md "wikilink")
  - [北區公園](../Page/北區公園.md "wikilink")
  - [粉嶺裁判法院](../Page/粉嶺裁判法院.md "wikilink")
  - [前粉嶺裁判法院](../Page/前粉嶺裁判法院.md "wikilink")
  - [匡智粉嶺綜合復康中心](../Page/匡智粉嶺綜合復康中心.md "wikilink")
  - [聯和墟](../Page/聯和墟.md "wikilink")

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFdd; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFdd; margin: 0 auto; padding: 0 10px; font-weight:normal;">

交通路線列表

</div>

<div class="NavContent" style="background-color: #FFFFdd; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{東鐵綫色彩}}">█</font>[東鐵綫](../Page/東鐵綫.md "wikilink")：[粉嶺站](../Page/粉嶺站.md "wikilink")

<!-- end list -->

  - 巴士

</div>

</div>

## 注釋

## 參考來源

[Category:香港教育小作品](../Category/香港教育小作品.md "wikilink")
[Category:香港直資學校](../Category/香港直資學校.md "wikilink")
[F](../Category/香港北區中學.md "wikilink")
[Category:1964年創建的教育機構](../Category/1964年創建的教育機構.md "wikilink")
[Category:基督教香港信義會學校](../Category/基督教香港信義會學校.md "wikilink")

1.
2.

3.

4.