分隔條目名稱和簡稱--\> | home_state1 = | leaders_seat1 = | running_mate1 = |
last_election1 = | seats_before1 = | seats_needed1 = |
electoral_vote1 = 1012票 | states_carried1 = | seats1 = | seats_after1
= | seat_change1 = | popular_vote1 = | percentage1 = 95.11% | swing1 =
| image2 = | colour2 = | nominee2 = | leader2 = | leader_since2 = |
party2 = | alliance2 = | home_state2 = | leaders_seat2 = |
running_mate2 = | last_election2 = | seats_before2 = | seats_needed2
= | electoral_vote2 = | states_carried2 = | seats2 = | seats_after2 =
| seat_change2 = | popular_vote2 = | percentage2 = | swing2 = |
map_image = | map_size = | map_caption = | title = 總統 | posttitle =
總統 | before_election = [蔣經國](../Page/蔣經國.md "wikilink") |
before_party = | after_election = [蔣經國](../Page/蔣經國.md "wikilink") |
after_party = }}
**1984年中華民國總統選舉**為[中華民國第七届總統選舉](../Page/中華民國.md "wikilink")，選舉方式為[中華民國](../Page/中華民國.md "wikilink")[國民大會代表](../Page/國民大會.md "wikilink")（以下簡稱[國大代表](../Page/國大代表.md "wikilink")）參與投票的[間接選舉](../Page/間接選舉.md "wikilink")，選舉地點則是在[台北市](../Page/台北市.md "wikilink")[陽明山的](../Page/陽明山.md "wikilink")[中山樓](../Page/中山樓.md "wikilink")。時間則為1984年3月21日，而該任副總統選舉，則於隔日的3月22日舉行。選舉結果，時任[總統](../Page/總統.md "wikilink")[蔣經國連任第七届總統](../Page/蔣經國.md "wikilink")，時任[台灣省主席](../Page/台灣省主席.md "wikilink")[李登輝則當選副總統](../Page/李登輝.md "wikilink")。\[1\]

這是戒嚴時期的最後一次、也是迄今最近一次有[蔣中正家族成員參加的總統選舉](../Page/蔣中正家族.md "wikilink")。

## 背景

1984年，面臨[中美斷交挫折的時任總統蔣經國](../Page/台美關係.md "wikilink")，並不出人意料的參與第七任總統選舉。不過，在選擇副手上，在考慮象徵本省籍的副總統[謝東閔年事已高](../Page/謝東閔.md "wikilink")，不能再連任情況下，他選擇[台灣省主席](../Page/台灣省主席.md "wikilink")[李登輝](../Page/李登輝.md "wikilink")，擔任副總統的搭檔。本省籍的李登輝，當時不但聲望資歷上低於外省籍的時任行政院長[孫運璿](../Page/孫運璿.md "wikilink")，也在本省人從政菁英地位上遜於謝東閔與時任[內政部部長](../Page/中華民國內政部.md "wikilink")[林洋港](../Page/林洋港.md "wikilink")，甚至也不及時任[台灣省議會議長](../Page/台灣省議會.md "wikilink")[高育仁及前](../Page/高育仁.md "wikilink")[高雄市市長](../Page/高雄市市長.md "wikilink")[蘇南成等](../Page/蘇南成.md "wikilink")。因此蔣經國這個選擇，頗令人意外。\[2\]\[3\]

## 過程

3月21日早上，中華民國第七任總統選舉開始舉行。主席為國大代表團推舉出來的國大代表[孔德成](../Page/孔德成.md "wikilink")，參選情況為國民黨總統候選人蔣經國的同額競選。該選舉中，共有1064人參加投票。經唱票後，時任總統蔣經國以1012票的絕對多數獲得連任。另外，他所搭檔的時任台灣省主席李登輝，也於隔日的副總統選舉中（[薛岳為主席](../Page/薛岳.md "wikilink")）以超過半數的873票獲得當選。

## 得票情形

| style="background-color:\#E9E9E9" align="center" rowspan= 2 colspan=2 | 候選人 | style="background-color:\#E9E9E9" align="center" rowspan= 2 colspan=2 | 政黨 | 得票 | style="background-color:\#E9E9E9" align="center" rowspan= 2 | 當選 |
| --------------------------------------------------------------------------- | -------------------------------------------------------------------------- | :-: | ---------------------------------------------------------------- |
| 票數                                                                          | 得票率                                                                        |    |                                                                  |
| 總統                                                                          | [蔣經國](../Page/蔣經國.md "wikilink")                                           |    | [中國國民黨](../Page/中國國民黨.md "wikilink")                             |
| 副總統                                                                         | [李登輝](../Page/李登輝.md "wikilink")                                           |    | [中國國民黨](../Page/中國國民黨.md "wikilink")                             |

## 備註

  - 1948年於中國[南京集會確認的國大代表為](../Page/南京.md "wikilink")2841名。1954年2月於台北報到的國大代表人數為1578名，到1984年為止，1064名國大代表中約有八成以上仍是由大陸選出的所謂「[萬年國代](../Page/萬年國代.md "wikilink")」
  - 1950年中華民國立法院修法，將國民大會開會人數從二分之一改成三分之一。
  - 1960年3月舉行的[總統選舉之後](../Page/1960年中華民國總統選舉.md "wikilink")，國民大會代表與總統選舉選舉人的法定總人數改採「報到人數」，即不採計滯留在中國大陸的國大代表人數。
  - 依法，即使同額競選，該總統選舉首輪投票仍需過半數方能當選，否則需要舉行第二輪。
  - 1988年1月13日，[蔣經國總統在台北病逝](../Page/蔣經國.md "wikilink")，享年78歲。[李登輝依](../Page/李登輝.md "wikilink")[憲法繼任為](../Page/憲法.md "wikilink")[中華民國總統](../Page/中華民國總統.md "wikilink")，副總統一職則空缺，至1990年5月20日才由[李元簇繼任](../Page/李元簇.md "wikilink")。

## 參考資料

<div style="font-size: small;">

<references />

</div>

## 參見

  - [台灣選舉](../Page/台灣選舉.md "wikilink")
  - [國民大會](../Page/國民大會.md "wikilink")

[分類:等額選舉](../Page/分類:等額選舉.md "wikilink")

[Category:中華民國總統選舉](../Category/中華民國總統選舉.md "wikilink")
[Category:1984年選舉](../Category/1984年選舉.md "wikilink")
[Category:1984年台灣](../Category/1984年台灣.md "wikilink")
[Category:蔣經國](../Category/蔣經國.md "wikilink")

1.  中央選舉委員會，《中華民國選舉史》，1987年，台北，中央選舉委員會印行
2.  [孫運璿與接班人](http://www.southnews.com.tw/specil_coul/nan/00/00103.htm)
    《[南方快報](../Page/南方快報.md "wikilink")》南嘉生專欄，2006.02.20。查證日期：2006.11.11
3.  [小蔣防跛
    沒讓他當副總統](http://www.libertytimes.com.tw/2006/new/feb/15/today-p1.htm)
    《[自由時報](../Page/自由時報.md "wikilink")》鄒景雯，2006.02.15。查證日期：2006.11.11