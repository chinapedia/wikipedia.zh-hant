[Flagstaff_House.jpg](https://zh.wikipedia.org/wiki/File:Flagstaff_House.jpg "fig:Flagstaff_House.jpg")
[Flagstaff_House_Original_Plan_1844.jpg](https://zh.wikipedia.org/wiki/File:Flagstaff_House_Original_Plan_1844.jpg "fig:Flagstaff_House_Original_Plan_1844.jpg")
[Flagstaff_House_Museum_of_Tea_ware_Interior1_2012.jpg](https://zh.wikipedia.org/wiki/File:Flagstaff_House_Museum_of_Tea_ware_Interior1_2012.jpg "fig:Flagstaff_House_Museum_of_Tea_ware_Interior1_2012.jpg")
**茶具文物館**（），位於[香港島](../Page/香港島.md "wikilink")[中區](../Page/中區_\(香港\).md "wikilink")[紅棉路](../Page/紅棉路.md "wikilink")10號，[香港公園內](../Page/香港公園.md "wikilink")。

這座兩層高的建築是[香港](../Page/香港.md "wikilink")[康樂及文化事務署轄下的博物館之一](../Page/康樂及文化事務署.md "wikilink")，亦是[香港藝術館的分館](../Page/香港藝術館.md "wikilink")，展覽各種[中國](../Page/中國.md "wikilink")[茶具文物](../Page/茶具.md "wikilink")。

它曾經是[香港殖民地時期駐香港英軍三軍總司令的官邸](../Page/香港.md "wikilink")，所以又稱**旗桿屋**（）。

建築物為香港現存最古老的[英國殖民地色彩建築物](../Page/英國.md "wikilink")，見證香港百年殖民歷史，歷史價值極高，已列為[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")。

## 歷史

官邸建於1846年，當時稱為**司令總部大樓**，是[域多利兵房的一部份](../Page/域多利兵房.md "wikilink")，成為了駐香港英軍總司令的辦公及居住地方，首個使用大樓的將領是英國駐華陸軍總司令[德忌笠少將](../Page/德忌笠.md "wikilink")。1932年，官邸改稱三軍司令官邸(早期無空軍，故不會稱三軍司令)。在二次大戰時，其樓頂曾被日軍炮彈炸毀，後由日軍修復用作為司令官邸。戰後大樓再度成為英國三軍司令官邸。1978年，這座建築隨著前[域多利軍營移交給](../Page/域多利軍營.md "wikilink")[香港政府發展而改變用途](../Page/香港政府.md "wikilink")。1984年7月，原址改為茶具文物館。1995年，茶具文物館增建了新翼**羅桂祥茶藝館**，在同年的12月14日落成啟用，以展示香港茶具文物收藏家[羅桂祥捐出的展品](../Page/羅桂祥.md "wikilink")。

## 展覽內容

兩層的展館共分為六個展區，展覽介紹中國人的飲茶歷史，展出由唐代至近代的各式茶具。

基本藏品由羅桂祥捐贈，其中以宜興茶具最富代表性，亦有展出中國陶瓷及印章。

該館定期舉辦陶藝示範、茶藝活動及講座等活動，以推廣[陶瓷](../Page/陶瓷.md "wikilink")[藝術和](../Page/藝術.md "wikilink")[中國茶文化](../Page/中國茶文化.md "wikilink")。

## 建築特色

建築物屬希臘[古典復興式風格的建築](../Page/古典復興式.md "wikilink")。整幢建築以[花崗岩製造](../Page/花崗岩.md "wikilink")、採用本地出產的花崗岩建成、設計簡潔、具英國殖民地色彩。\[1\]
由於缺乏建築師，香港早期的西式建築大都仿照英國本土的建築風格，並適應本地技術、材料和炎熱潮濕的氣候而作出適當的修改。舊三軍司令官邸就是由英軍的工程師參考軍用的《[模式手冊](../Page/模式手冊.md "wikilink")》設計，仿照[格林威治的](../Page/格林威治.md "wikilink")[王后宮建造](../Page/王后宮.md "wikilink")，再因應香港的[亞熱帶氣候和環境改良](../Page/亞熱帶氣候.md "wikilink")，例如加入寬闊[遊廊](../Page/遊廊.md "wikilink")、高樓底、金字瓦頂、木製百葉窗等，有助納涼及避開陽光直射。

<File:Museum> of Tea Ware Past\&Present.jpg|茶具文物館的今昔比較 <File:Flagstaff>
Hse Museum of Tea ware 60401 22.jpg|茶具文物館內佈置 (1) <File:Flagstaff> House
Museum of Tea ware Interior 2012.jpg|茶具文物館內佈置 (2) <File:Flagstaff> Hse
Museum of Tea ware 60401 sight.jpg|茶具文物館外形典雅 <File:Flagstaff> Hse Museum
of Tea ware 60401 26.jpg|茶具文物館藏品之一 <File:K.S>. Lo Gallery
201212.jpg|茶具文物館新翼
羅桂祥茶藝館

## 入場資料

  - 開放時間：
      - 每日10:00-18:00
      - 逢星期二休館（公眾假期除外）
      - 聖誕節前夕及農歷年除夕提早於17:00休館
      - 農曆年初一、二全日休館
  - 收費：免費入場

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")、<font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")、<font color="{{南港島綫色彩}}">█</font>[南港島綫](../Page/南港島綫.md "wikilink")：[金鐘站C](../Page/金鐘站.md "wikilink")1出口

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小型巴士.md "wikilink")

</div>

</div>

## 參見

  - [香港公園](../Page/香港公園.md "wikilink")
  - [香港法定古蹟](../Page/香港法定古蹟.md "wikilink")
  - [香港藝術館](../Page/香港藝術館.md "wikilink")
  - [域多利軍營](../Page/域多利軍營.md "wikilink")

## 外部連結

  - [茶具文物館](http://hk.art.museum/zh_TW/web/ma/tea-ware.html)

## 参考文献

  - [香港三聯書店出版](../Page/香港三聯書店.md "wikilink")
    《香港史新編》，[王賡武主編](../Page/王賡武.md "wikilink")，ISBN
    962-04-1255-9 第258頁

{{-}}

[Category:香港文物館](../Category/香港文物館.md "wikilink")
[Category:以茶为主题的博物馆](../Category/以茶为主题的博物馆.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")
[Category:香港法定古蹟](../Category/香港法定古蹟.md "wikilink")
[Category:香港官邸](../Category/香港官邸.md "wikilink")
[Category:香港西式建築](../Category/香港西式建築.md "wikilink")
[Category:香港美术馆](../Category/香港美术馆.md "wikilink")

1.