**謝萊夫特奧**（[瑞典文](../Page/瑞典文.md "wikilink")：）是[瑞典](../Page/瑞典.md "wikilink")[西博滕省的東部城市](../Page/西博滕省.md "wikilink")，距[岸邊約](../Page/岸.md "wikilink")7[公里](../Page/公里.md "wikilink")。人口35,525，是[謝萊夫特奧自治市的市治](../Page/謝萊夫特奧自治市.md "wikilink")。

## 歷史

[薩米人在](../Page/薩米人.md "wikilink")1000年時已居住於謝萊夫特奧。

## 注釋

## 外部連結

  - [謝萊夫特奧](http://www.skelleftea.se) 政府網站

[category:瑞典城市](../Page/category:瑞典城市.md "wikilink")