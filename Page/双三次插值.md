在[数值分析这个](../Page/数值分析.md "wikilink")[数学分支中](../Page/数学.md "wikilink")，**双三次插值**（）是[二维空间中最常用的](../Page/二维.md "wikilink")[插值方法](../Page/插值.md "wikilink")。在这种方法中，[函数](../Page/函数.md "wikilink")
*f* 在点 (*x*, *y*)
的值可以通过[矩形](../Page/矩形.md "wikilink")[网格中最近的十六个采样点的](../Page/网格.md "wikilink")[加权平均得到](../Page/加权平均.md "wikilink")，在这里需要使用两个[多项式插值](../Page/多项式插值.md "wikilink")[三次函数](../Page/三次函数.md "wikilink")，每个方向使用一个。

## 属性

通过双三次插值可以得到一个连续的插值函数，它的一阶偏导数连续，并且交叉导数处处连续。

## 公式

双三次插值通过下式进行计算：

\(a_{00} + a_{10} x + a_{01} y + a_{20} x^2 + a_{11} x y + a_{02} y^2 + a_{21} x^2 y + a_{12} x y^2 + a_{22} x^2 y^2 + a_{30} x^3 + a_{03} y^3 + a_{31} x^3 y + a_{13} x y^3 + a_{32} x^3 y^2 + a_{23} x^2 y^3 + a_{33} x^3 y^3\)

或者用一种更加紧凑的形式，

\(\sum_{i=0}^3 \sum_{j=0}^3 a_{ij} x^i y^j\)

计算系数 \(a_{ij}\)
的过程依赖于插值数据的特性。如果已知插值函数的导数，常用的方法就是使用四个顶点的高度以及每个顶点的三个导数。一阶导数
\(h' x\) 与 \(h' y\) 表示 x 与 y 方向的表面斜率，二阶相互导数 \(h'' xy\) 表示同时在 x 与 y
方向的斜率。这些值可以通过分别连续对 x 与 y 向量取微分得到。对于网格单元的每个顶点，将局部坐标（0,0, 1,0,
0,1 和 1,1) 带入这些方程，再解这 16 个方程。

## 在计算机图形学中的应用

双三次插值算法经常用于图像或者视频的缩放，它能比占主导地位的[双线性滤波算法保留更好的细节质量](../Page/双线性滤波.md "wikilink")。

## 参见

  - [立方埃尔米特样条](../Page/立方埃尔米特样条.md "wikilink")，一维空间中的类似形式
  - [双线性插值](../Page/双线性插值.md "wikilink")
  - [样条插值](../Page/样条插值.md "wikilink")
  - [抗混叠](../Page/抗混叠.md "wikilink")
  - [Sinc滤波器](../Page/Sinc滤波器.md "wikilink")
  - [Lanczos resampling](../Page/Lanczos_resampling.md "wikilink")

## 外部链接

  - [Application of interpolation to elevation
    samples](https://web.archive.org/web/20051024202307/http://www.geovista.psu.edu/sites/geocomp99/Gc99/082/gc_082.htm)

[Category:插值论](../Category/插值论.md "wikilink")