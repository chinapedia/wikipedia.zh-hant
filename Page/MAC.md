**Mac**或**MAC**可以指：

  - 计算机

<!-- end list -->

  - [Macintosh](../Page/麦金塔电脑.md "wikilink")：[蘋果公司所推出的](../Page/蘋果公司.md "wikilink")[个人电脑](../Page/个人电脑.md "wikilink")，簡稱Mac。

  - [Mac
    OS](../Page/Mac_OS.md "wikilink")：[蘋果公司所开发的](../Page/蘋果公司.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")。

  - [介质访问控制层](../Page/介质访问控制.md "wikilink")（Media Access
    Control）：[OSI模型中](../Page/OSI模型.md "wikilink")，[数据链路层的子層](../Page/数据链路层.md "wikilink")。

  - [MAC地址](../Page/MAC地址.md "wikilink")：OSI模型中對應數據鏈路層的地址，每個網路位置都有一個唯一的編號。

  - [訊息鑑別碼](../Page/訊息鑑別碼.md "wikilink")（Message authentication
    code）：在密码学中用来提供消息验证的一段短数据块。

  - [強制存取控制](../Page/強制存取控制.md "wikilink")（Mandatory Access
    Control）：一种计算机安全机制。

  - [乘积累加](../Page/乘积累加.md "wikilink")（Multiply-accumulate）：[數位信號處理器中的一種特殊指令](../Page/數字信號處理器.md "wikilink")。

  - （Multiplexed Analogue
    Components）：一種[卫星电视的傳輸標準](../Page/卫星电视.md "wikilink")。

<!-- end list -->

  - 化学、生物

<!-- end list -->

  - [MAC
    (化妝品)](../Page/MAC_\(化妝品\).md "wikilink")，：[加拿大](../Page/加拿大.md "wikilink")[化妝品](../Page/化妝品.md "wikilink")[品牌](../Page/品牌.md "wikilink")。

  - （Maximum Allowable Concentration）：在環境或產品中，特定化學物質的最大容許量。

  - [補體系統膜攻擊複合物](../Page/補體系統膜攻擊複合物.md "wikilink")(membrane attack
    complex)的英文縮寫。

<!-- end list -->

  - 武器

<!-- end list -->

  - [MAC-10](../Page/MAC-10衝鋒槍.md "wikilink")、[MAC-11](../Page/MAC-11衝鋒槍.md "wikilink")：同一系列的兩款[冲锋枪](../Page/冲锋枪.md "wikilink")（或稱[冲锋手枪](../Page/全自動手槍.md "wikilink")）。

<!-- end list -->

  - 地区、机构

<!-- end list -->

  - [澳门的地区代码](../Page/澳门.md "wikilink")。
  - [行政院大陸委員會](../Page/行政院大陸委員會.md "wikilink")（）：中華民國統籌處理[海峽兩岸事務](../Page/海峽兩岸.md "wikilink")（包括中國大陸與香港、澳門）的專責行政機關。