**表情圖標**（，），是使用在网页和聊天中的[形意符号](../Page/形意符号.md "wikilink")，最初是[日本在](../Page/日本.md "wikilink")[无线通信中所使用的](../Page/无线通信.md "wikilink")[视觉情感符号](../Page/表情符号.md "wikilink")（圖畫文字）。**表情**意指面部表情，**圖標**则是图形標誌的意思，可用來代表多种表情，如笑脸表示笑、蛋糕表示食物等。在[中国大陆](../Page/中国大陆.md "wikilink")，表情圖標通常叫做“小黄脸”\[1\]，或者直称emoji\[2\]。

## 歷史

第一个表情符号是由[栗田穰崇于](../Page/栗田穰崇.md "wikilink")1998年或1999年创造，他当时隶属于[NTT
DoCoMo公司](../Page/NTT_DoCoMo.md "wikilink")[i-mode移动互联网平台的团队](../Page/i-mode.md "wikilink")。第一套表情符号包括172个12×12[像素的图标](../Page/像素.md "wikilink")，设计初衷是作为i-mode消息功能的一部分帮助促进电子通讯，并作为区别于其他业务的特色功能。
然而在1997年，Nicolas
Loufrani注意到，[ASCII表情符号在移动技术中的使用正在增加](../Page/ASCII.md "wikilink")，他开始尝试动画效果的笑脸表情，目的是使用纯标点符号设计一套与现有ASCII表情对应的彩色图标，以促进其在數位领域的使用。Loufrani由此创造了第一套图形化表情，并编译了在线表情符号词典，
将这些符号分成不同类别，包括：经典类、情绪类、旗标类、庆贺类、娱乐类、体育类、[天气类](../Page/天气.md "wikilink")、[动物类](../Page/动物.md "wikilink")、饮食类、民族类、职业类、行星类、[星座类](../Page/星座.md "wikilink")、婴儿类等，这些设计最初于1997年在美国[版权局注册](../Page/版权局.md "wikilink")，随后全套图标于1998年以.[gif格式文件在网络上发布](../Page/GIF.md "wikilink")，成为科技行业中使用的第一套图形化表情符号。2000年，Loufrani创建的表情目录开始提供下载，用户开始可从互联网上透過smileydictionary.com为手机下载Loufrani创建的表情目录，该目录编译了超过1000个笑脸图形表情符号及其ASCII版本。该目录在2002年由Marabout以书籍形式出版，名称为Dico
Smileys。2001年，表情符号公司Smiley Company
开始向各家电信公司的手机提供Loufrani图形表情符号的授权下载，这些公司包括[诺基亚](../Page/诺基亚.md "wikilink")、[摩托罗拉](../Page/摩托罗拉.md "wikilink")、[三星](../Page/三星電子.md "wikilink")、SFR（沃达丰）和SkyTelemedia。其中，「[😂](../Page/😂.md "wikilink")」（，，[Unicode碼位](../Page/Unicode.md "wikilink")：U+1F602\[3\]\[4\]）被《[牛津詞典](../Page/牛津詞典.md "wikilink")》評選為2015年度詞彙。\[5\]\[6\]\[7\]\[8\]

## 膚色

Unicode 8.0中加入了5個修飾符，用來調節人形表情的膚色。這些叫做繪文字菲茨帕特裡克修飾符（EMOJI MODIFIER
FITZPATRICK）類型-1-2、-3、-4、-5和-6（U+1F3FB \~ U+1F3FF）：🏻 🏼 🏽 🏾
🏿。對應了對人類膚色的分類。沒有后綴膚色代碼的繪文字會顯示非真實的通用膚色例如亮黃色（<span style="color:#FC2;font-size:large;">█</span>）、藍色（<span style="color:#39C;font-size:large;">█</span>）或灰色（<span style="color:#CCC;font-size:large;">█</span>）。\[9\]非人形表情則不受修飾符影響。在Unicode
9.0中菲茨帕特裡克修飾符可以和86個人形繪文字一起使用。

|             |    |        |      |      |      |      |
| ----------- | -- | ------ | ---- | ---- | ---- | ---- |
| 代碼位         | 默認 | 菲茨-1-2 | 菲茨-3 | 菲茨-4 | 菲茨-5 | 菲茨-6 |
| U+1F466: 男孩 | 👦  | 👦🏻     | 👦🏼   | 👦🏽   | 👦🏾   | 👦🏿   |
| U+1F467: 女孩 | 👧  | 👧🏻     | 👧🏼   | 👧🏽   | 👧🏾   | 👧🏿   |
| U+1F468: 男人 | 👨  | 👨🏻     | 👨🏼   | 👨🏽   | 👨🏾   | 👨🏿   |
| U+1F469: 女人 | 👩  | 👩🏻     | 👩🏼   | 👩🏽   | 👩🏾   | 👩🏿   |

style="font-size:small" | 菲茨帕特裡克修飾符使用示例

## 组合

可以使用[U+200D零宽连字](../Page/零宽连字.md "wikilink")(ZWJ)将两个绘文字连起来，使其看起来像是一个绘文字。\[10\]（不支持的系统会忽略零宽连字）

例如U+1F468男人、U+200D ZWJ、U+1F469女人、U+200D
ZWJ、U+1F467女孩(👨‍👩‍👧)在系统支持的情况下会显示为一个男人一个女人和一个女孩组成的家庭绘文字，而不支持的系统则会顺序显示这三个绘文字(👨👩👧)。

## Unicode 区块

2010年10月发布的Unicode
6.0版首次收录繪文字编码，其中582個繪文字符号，66個已在其他位置[编码](../Page/编码.md "wikilink")，保留作相容用途的繪文字符号。在Unicode
9.0 用22区块中共计1,126个字符表示绘文字，其中1,085个是独立绘文字字符，26个是用来显示旗帜的以及 12 个(\#, \* and
0-9)键帽符号。\[11\]\[12\]

[杂项符号及图形](../Page/Template:Unicode_chart_Miscellaneous_Symbols_and_Pictographs.md "wikilink")768个字符中有637是绘文字；[增补符号及图形](../Page/Template:Unicode_chart_Supplemental_Symbols_and_Pictographs.md "wikilink")82个字符中有80个是绘文字；所有80个[表情符号都是绘文字](../Page/Template:Unicode_chart_Emoticons.md "wikilink")；[交通及地图符号](../Page/Template:Unicode_chart_Transport_and_Map_Symbols.md "wikilink")103个字符中有92个是绘文字；[杂项符号](../Page/Template:Unicode_chart_Miscellaneous_Symbols.md "wikilink")256个字符中有80个是绘文字；[装饰符号](../Page/Template:Unicode_chart_Dingbats.md "wikilink")192个字符中有33个是绘文字。

## 参见

  - [表情圖標百科](../Page/表情圖標百科.md "wikilink")
  - [表情符号](../Page/表情符号.md "wikilink")
  - [世界表情符号日](../Page/世界表情符号日.md "wikilink")

## 參考資料

## 外部連結

  - [Emojipedia](https://emojipedia.org)
  - [au/KDDI的繪文字](http://www.au.kddi.com/ezfactory/tec/spec/3.html)，[第一款](http://www.au.kddi.com/ezfactory/tec/spec/icon_mono.html)[第二款](http://www.au.kddi.com/ezfactory/tec/spec/icon_color.html)。

[W](../Category/日語詞彙.md "wikilink")
[Emoji](../Category/日語書寫系統.md "wikilink")
[日](../Category/互联网用语.md "wikilink")
[日](../Category/網上聊天.md "wikilink")
[日](../Category/表情符號.md "wikilink")
[日](../Category/行動電話.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.
11.

12.