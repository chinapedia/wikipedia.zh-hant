[The_Lily_201501.jpg](https://zh.wikipedia.org/wiki/File:The_Lily_201501.jpg "fig:The_Lily_201501.jpg")

**The
Lily**\[1\]又名**百合花**，是[香港一座](../Page/香港.md "wikilink")[建築物](../Page/建築物.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區_\(香港\).md "wikilink")[淺水灣](../Page/淺水灣.md "wikilink")[淺水灣道](../Page/淺水灣道.md "wikilink")129號，分為4座，是除了[影灣園外當地的一個](../Page/影灣園.md "wikilink")[地標](../Page/地標.md "wikilink")。發展商為[華懋集團](../Page/華懋集團.md "wikilink")，經歷了11年的洗禮，項目最終由出售更改為[酒店](../Page/酒店.md "wikilink")，再更改為出租單位，[管理公司為](../Page/管理公司.md "wikilink")[仲量聯行](../Page/仲量聯行.md "wikilink")；於2010年6月1日啟用。

## 歷史

該處前身是一座80個單位名為**萊敦閣**（[英文](../Page/英文.md "wikilink")：**Royen
Court**）的英國軍官宿舍，於1995年拆除。1997年8月，該地皮被[香港政府以住宅用地拍賣](../Page/香港政府.md "wikilink")，最後由[華懋集團以](../Page/華懋集團.md "wikilink")55.5億港元投得，每平方呎的價格約16,000港元，打破當時香港住宅地皮呎價的紀錄。翌年年中，工程展開，耗資25億港元，於2006年2月落成，惟華懋集團一直未有出售任何單位。至2007年，才申請將其改變用途為酒店。不過截至2010年1月啟用前，該建築物一直空置。

## 特色

The
Lily由[英國著名](../Page/英國.md "wikilink")[建築師](../Page/建築師.md "wikilink")[-{zh-hans:诺曼·福斯特;zh-hk:霍朗明;zh-tw:諾曼·福斯特;}-設計](../Page/諾曼·福士特.md "wikilink")，利用[力學原理大樓由下而上漸進式地向外展開](../Page/力學.md "wikilink")，像一朵盛放的[百合花](../Page/百合花.md "wikilink")。大樓樓高30層，共有184個單位，所有單位均擁有海景。

大廈的外牆裝設了由電腦控制的彩燈系統，在每晚的下午5時半至晚上8時半，系統會以燈光在大廈外牆上顯示出不同的圖案。

## 發展

2010年4月，[華懋集團宣佈The](../Page/華懋集團.md "wikilink")
Lily提供共100個出租單位，當中34伙作[服務式住宅](../Page/服務式住宅.md "wikilink")，單位面積最小為1,800方[呎](../Page/呎.md "wikilink")，月租約9萬港元起，於2010年6月入伙。

## 交通

## 註腳

## 參見

  - [影灣園](../Page/影灣園.md "wikilink")
  - [龔如心遺產爭奪事件](../Page/龔如心遺產爭奪事件.md "wikilink")

[Category:南區單幢式住宅 (香港)](../Category/南區單幢式住宅_\(香港\).md "wikilink")
[Category:淺水灣](../Category/淺水灣.md "wikilink")
[Category:華懋集團物業](../Category/華懋集團物業.md "wikilink")

1.