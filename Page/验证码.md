[Captcha.jpg](https://zh.wikipedia.org/wiki/File:Captcha.jpg "fig:Captcha.jpg")
[Modern-captcha.jpg](https://zh.wikipedia.org/wiki/File:Modern-captcha.jpg "fig:Modern-captcha.jpg")
[KCAPTCHA_with_crowded_symbols.gif](https://zh.wikipedia.org/wiki/File:KCAPTCHA_with_crowded_symbols.gif "fig:KCAPTCHA_with_crowded_symbols.gif")
[12306验证码界面（模拟）.jpg](https://zh.wikipedia.org/wiki/File:12306验证码界面（模拟）.jpg "fig:12306验证码界面（模拟）.jpg")网站的验证界面\]\]

**全自动区分计算机和人类的公開[图灵测试](../Page/图灵测试.md "wikilink")**（，簡稱**CAPTCHA**），俗称**验证码**，是一种区分用户是[计算机或](../Page/计算机.md "wikilink")[人的公共全自动程序](../Page/人.md "wikilink")。在CAPTCHA测试中，作为[服务器的计算机会自动生成一个问题由用户来解答](../Page/服务器.md "wikilink")。这个问题可以由计算机生成并评判，但是必须只有人类才能解答。由于计算机无法解答CAPTCHA的问题，所以回答出问题的用户就可以被认为是人类。

## 簡介

CAPTCHA这个词最早是在2002年由[卡内基梅隆大学的](../Page/卡内基梅隆大学.md "wikilink")[路易斯·馮·安](../Page/路易斯·馮·安.md "wikilink")、Manuel
Blum、Nicholas J.Hopper以及IBM的John
Langford所提出。卡内基梅隆大学曾试图申請此詞使其成为[注册商标](../Page/注册商标.md "wikilink")\[1\]，
但该申请于2008年4月21日被拒绝\[2\]。一种常用的CAPTCHA测试是让用户输入一个扭曲变形的图片上所显示的文字或数字，扭曲變形是為了避免被[光學字元識別](../Page/光学字符识别.md "wikilink")（OCR,
Optical Character
Recognition）之類的電腦程式自動辨識出圖片上的文數字而失去效果。由于这个测试是由计算机来考人类，而不是标准[图灵测试中那样由人类来考计算机](../Page/图灵测试.md "wikilink")，人们有时称CAPTCHA是一种反向图灵测试。

為了無法看到圖像的[身心障礙者](../Page/身心障礙者.md "wikilink")，替代的方法是改用語音讀出文數字，為了防止[語音辨識分析聲音](../Page/語音辨識.md "wikilink")，聲音的內容會有雜音或仍可以被人类接受的[变声](../Page/变声.md "wikilink")。

根據CAPTCHA測試的定義，產生驗證碼圖片的算法必須公開，即使該算法可能有專利保護。這樣做是證明想破解就需要解決一個不同的人工智能難題，而非僅靠發現原來的（秘密）算法，而後者可以用[逆向工程等途徑得到](../Page/逆向工程.md "wikilink")。

## 運用

CAPTCHA目前廣泛用於網站的留言板，許多留言板為防止有人利用電腦程式大量在留言板上張貼廣告或其他垃圾訊息，因此會放置CAPTCHA要求留言者必需輸入图片上所显示的文数字或是算術題才可完成留言。而一些網路上的交易系統（如訂票系統、[網路銀行](../Page/網路銀行.md "wikilink")）也為避免被電腦程式以暴力法大量嘗試交易也會有CAPTCHA的機制。加值的運用有：

  - [reCAPTCHA是利用CAPTCHA技術來幫助典籍數位化的進行](../Page/reCAPTCHA.md "wikilink")。
  - SolveMedia，captcha.tw\[3\] 等廣告聯播網利用驗證碼展示廣告。

## 破解

一些曾经或者正在使用中的验证码系统已被破解。这包括Yahoo验证码的一个早期版本
EZ-Gimpy\[4\]，PayPal使用的验证码\[5\]，LiveJournal、phpBB使用的验证码，很多金融机构（主要是银行）使用的网银验证码\[6\]以及很多其他网站使用的验证码\[7\]\[8\]\[9\]。

[俄罗斯的一个](../Page/俄罗斯.md "wikilink")[黑客组织使用一个自动识别软件在](../Page/黑客.md "wikilink")2006年破解了[Yahoo的CAPTCHA](../Page/Yahoo.md "wikilink")。准确率大概是15%，但是攻击者可以每天尝试10万次，相对来说成本很低。\[10\]而在2008年，[Google的CAPTCHA也被俄罗斯黑客所破解](../Page/Google.md "wikilink")。攻击者使用两台不同的电脑来调整破解进程，可能是用第二台电脑学习第一台对CAPTCHA的破解，或者是对成效进行监视\[11\]。

也有一种类似分布式的人工识别方法，多用于在自动化运行程序防止被反自动化机制拦截，如游戏自动外挂等，就是通过将需要验证的图片等数据转发到有人值勤的终端，由其他人类代为识别回答后再把答案转发回验证发送端回答。\[12\]\[13\]Gmail郵箱註冊驗證系統的破解可能即是經由此方法\[14\]。已经有专门的公司提供专门的人员识别服务。\[15\][reCAPTCHA](../Page/reCAPTCHA.md "wikilink")（）的一種，由CAPTCHA的发明者之一[Luis
von Ahn所發起](../Page/Luis_von_Ahn.md "wikilink")。）亦是人工破解驗證碼的應用。

## 参考文献

[Category:人工智慧](../Category/人工智慧.md "wikilink")
[Category:電腦安全](../Category/電腦安全.md "wikilink")
[Category:图灵测试](../Category/图灵测试.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12. [解析验证码人工破解内幕！什么是打码任务](http://blog.sina.com.cn/s/blog_579be2410100cwbd.html)
    打码任务（captcha human bypass project）
13. [关于打码内幕](http://wap.u8881.com/discuz/viewthread.php?tid=596634)
14. [](http://www.gseeker.com/50226711/googlecgmaileeccaeaece_141576.php)
     Brad Taylor稱垃圾信息發送者採用了原始的"人肉破解"，即花錢僱傭了大量來自第三世界的廉價勞動者，讓他們逐個對進行
    CAPTCHA系統進行人手破解。
15.