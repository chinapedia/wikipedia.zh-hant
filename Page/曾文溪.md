**曾文溪**位於[臺灣南部](../Page/臺灣.md "wikilink")，是全臺灣第四長的河流，屬於[中央管河川](../Page/中央管河川.md "wikilink")。曾文溪發源於[嘉義縣](../Page/嘉義縣.md "wikilink")[阿里山鄉的東水山](../Page/阿里山鄉.md "wikilink")\[1\]。流經[台南市](../Page/台南市.md "wikilink")[楠西區](../Page/楠西區.md "wikilink")、[玉井區](../Page/玉井區.md "wikilink")、[大內區](../Page/大內區.md "wikilink")、[山上區](../Page/山上區.md "wikilink")、[善化區](../Page/善化區.md "wikilink")、[官田區](../Page/官田區.md "wikilink")、[麻豆區](../Page/麻豆區.md "wikilink")、[安定區](../Page/安定區_\(臺南市\).md "wikilink")、[西港區](../Page/西港區.md "wikilink")、[七股區](../Page/七股區.md "wikilink")、最後在[安南區和](../Page/安南區.md "wikilink")[七股區之間](../Page/七股區.md "wikilink")，流入[臺灣海峽](../Page/臺灣海峽.md "wikilink")，全長138.5公里，流域面積1,176.7平方公里，源頭海拔高2,440公尺，其主要支流有塔乃庫溪、普亞女溪、草蘭溪、後堀溪、菜寮溪、官田溪等。歷史上曾文溪頻頻改道，被居民戲稱是「青瞑蛇」。[昭和十三年](../Page/昭和.md "wikilink")（1938年）堤防竣工之後，河道趨於固定。

[Three_dams_Tainan_from_airplane_window.jpg](https://zh.wikipedia.org/wiki/File:Three_dams_Tainan_from_airplane_window.jpg "fig:Three_dams_Tainan_from_airplane_window.jpg")（左上）、[烏山頭水庫](../Page/烏山頭水庫.md "wikilink")（左下）、[南化水庫](../Page/南化水庫.md "wikilink")（右上）與曾文溪蜿蜒的河道（右下）。\]\]

[臺南市常以曾文溪為分界](../Page/臺南市.md "wikilink")，劃分溪北、溪南地區。溪南地區似近都會區型態，以[原臺南省轄市為中心](../Page/臺南市_\(省轄市\).md "wikilink")；溪北則較近農村，中心聚落為原[臺南縣政府所在地](../Page/臺南縣.md "wikilink")－[新營](../Page/新營區.md "wikilink")，在生活圈及文化上與[嘉義都會區關係密切](../Page/嘉義都會區.md "wikilink")。

曾文溪有豐富的水力資源，全臺灣最大的水庫[曾文水庫](../Page/曾文水庫.md "wikilink")，即在曾文溪上游，此外，在曾文溪的支流上，尚有[南化水庫及](../Page/南化水庫.md "wikilink")[烏山頭水庫等](../Page/烏山頭水庫.md "wikilink")，除了供水發電之外，也都成為重要的觀光景點。此外，由於曾文溪挾帶砂石與生物碎屑在出海口沉積，提供了大量養分，因而蘊育了河口地區豐富的底棲生物與[浮游生物](../Page/浮游生物.md "wikilink")，也吸引了大批水鳥在此處棲息。著名的珍貴鳥類[黑面琵鷺即棲息在曾文溪河口北岸](../Page/黑面琵鷺.md "wikilink")，因此設有[七股黑面琵鷺保護區](../Page/七股黑面琵鷺保護區.md "wikilink")。台灣第八座國家公園[台江國家公園範圍也包括曾文溪口](../Page/台江國家公園.md "wikilink")。

[二重溪_(_曾文溪、南瀛天文館_附近_).jpg](https://zh.wikipedia.org/wiki/File:二重溪_\(_曾文溪、南瀛天文館_附近_\).jpg "fig:二重溪_(_曾文溪、南瀛天文館_附近_).jpg")二重溪（地名）附近的曾文溪\]\]

## 名稱演變

在[荷蘭時期](../Page/臺灣荷治時期.md "wikilink")，曾文溪在歐洲古地圖上記為「River
Soulang」（蕭壠溪），另外也有稱為「Zant
River」（砂河）。而根據《[諸羅縣志](../Page/諸羅縣志.md "wikilink")》記載，當時的曾文溪在上游到石仔瀨（在大內區）這段稱為「灣裏溪」，往西到蕭壠渡這段叫「加拔溪」，再往西到入海這段稱為「歐汪溪」。

至於「曾文」之名的由來，在連橫《雅堂文集》中有記載相傳是荷蘭時期有一個名叫「曾文」在此開墾，並設置渡口，該溪因此得名。《大臺南的河川》一書則認為是道光三年（1823年）曾文溪改道後，溪上出現了重要渡口「罾門渡」，灣裏溪因而改稱罾門溪。之後因發音相似，又有記做「層門」，後來再變成「曾文溪」。

## 改道歷史

早期的曾文溪改道頻仍，沒有固定的流路而迫使下游附近村落遷居，1930年的日治時期才開始有系統的治理，自縱貫鐵路橋以下至河口之間的兩岸築堤以約束水流，才有今日曾文溪的面貌\[2\]。治水計畫整體於1939年大功告成，目前在[台19線西港大橋下游北岸尚立有](../Page/台19線.md "wikilink")「曾文溪治水工事紀念碑」，敘明完成兩岸堤防長達39公里\[3\]。

## 曾文溪水系主要河川

以下由下游至源頭列出水系主要河川，其中粗體字為主流河道：

  - **曾文溪**：[臺南市](../Page/臺南市.md "wikilink")、[嘉義縣](../Page/嘉義縣.md "wikilink")、[高雄市](../Page/高雄市.md "wikilink")
      - [官田溪](../Page/官田溪.md "wikilink")：臺南市[官田區](../Page/官田區.md "wikilink")、[六甲區](../Page/六甲區.md "wikilink")、[大內區](../Page/大內區.md "wikilink")、[東山區](../Page/東山區_\(臺南市\).md "wikilink")
      - [渡子頭溪](../Page/渡子頭溪.md "wikilink")（雙溪）：臺南市官田區、大內區
      - [菜寮溪](../Page/菜寮溪.md "wikilink")：臺南市[山上區](../Page/山上區.md "wikilink")、[左鎮區](../Page/左鎮區.md "wikilink")、[玉井區](../Page/玉井區.md "wikilink")、[南化區](../Page/南化區.md "wikilink")
          - [岡林溪](../Page/岡林溪.md "wikilink")：臺南市左鎮區
          - [紅水泉溪](../Page/紅水泉溪.md "wikilink")：臺南市左鎮區
          - [草山溪](../Page/草山溪.md "wikilink")：臺南市左鎮區
      - [盧芝坑溝](../Page/盧芝坑溝.md "wikilink")：臺南市
      - [後堀溪](../Page/後堀溪.md "wikilink")：臺南市玉井區、南化區、嘉義縣[大埔鄉](../Page/大埔鄉.md "wikilink")
          - [坑內溝](../Page/坑內溝.md "wikilink")：
          - [鹽水坑溪](../Page/鹽水坑溪.md "wikilink")：臺南市南化區
          - [番子坑溪](../Page/番子坑溪.md "wikilink")：臺南市南化區
          - [竹坑溪](../Page/竹坑溪.md "wikilink")：臺南市南化區
          - [鳳梨坑溪](../Page/鳳梨坑溪.md "wikilink")：臺南市南化區、嘉義縣大埔鄉
      - **[大埔溪](../Page/大埔溪.md "wikilink")**：臺南市玉井區、[楠西區](../Page/楠西區.md "wikilink")、東山區、嘉義縣大埔鄉、[番路鄉](../Page/番路鄉.md "wikilink")、高雄市[那瑪夏區](../Page/那瑪夏區.md "wikilink")、嘉義縣[阿里山鄉](../Page/阿里山鄉.md "wikilink")
          - [後旦溪](../Page/後旦溪.md "wikilink")：臺南市玉井區、楠西區
          - [油車溪](../Page/油車溪.md "wikilink")：臺南市楠西區
          - [格子坑溪](../Page/格子坑溪.md "wikilink")：
          - [密枝溪](../Page/密枝溪.md "wikilink")：臺南市楠西區
              - [灣丘溪](../Page/灣丘溪.md "wikilink")：臺南市楠西區
          - [長枝坑溪](../Page/長枝坑溪.md "wikilink")：嘉義縣大埔鄉
          - [竹坑溪](../Page/竹坑溪.md "wikilink")：嘉義縣大埔鄉
          - [匏子寮溪](../Page/匏子寮溪.md "wikilink")：嘉義縣大埔鄉
          - [沙美箕溪](../Page/沙美箕溪.md "wikilink")：嘉義縣大埔鄉、番路鄉
          - [二寮溪](../Page/二寮溪.md "wikilink")：嘉義縣大埔鄉、高雄市那瑪夏區
              - [草蘭溪](../Page/草蘭溪.md "wikilink")：高雄市那瑪夏區
          - [黃狗坑溪](../Page/黃狗坑溪.md "wikilink")：高雄市那瑪夏區
          - [托亞伊奇溪](../Page/托亞伊奇溪.md "wikilink")：高雄市那瑪夏區、嘉義縣阿里山鄉
          - [普亞女溪](../Page/普亞女溪.md "wikilink")：嘉義縣阿里山鄉
          - [塔乃庫溪](../Page/塔乃庫溪.md "wikilink")：嘉義縣阿里山鄉
          - [烏奇哈溪](../Page/烏奇哈溪.md "wikilink")：嘉義縣阿里山鄉
          - [特富野溪](../Page/特富野溪.md "wikilink")：嘉義縣阿里山鄉
              - [伊斯基安那溪](../Page/伊斯基安那溪.md "wikilink")：嘉義縣阿里山鄉
          - **[後大埔溪](../Page/後大埔溪.md "wikilink")**：嘉義縣阿里山鄉

## 主要橋樑

以下由河口至源頭列出主流上之主要橋樑：

### 曾文溪河段

[曾文溪橋_(臺一線).JPG](https://zh.wikipedia.org/wiki/File:曾文溪橋_\(臺一線\).JPG "fig:曾文溪橋_(臺一線).JPG")
[臺鐵曾文溪橋.JPG](https://zh.wikipedia.org/wiki/File:臺鐵曾文溪橋.JPG "fig:臺鐵曾文溪橋.JPG")

  - [國姓大橋](../Page/國姓大橋.md "wikilink")（[TW_PHW17.svg](https://zh.wikipedia.org/wiki/File:TW_PHW17.svg "fig:TW_PHW17.svg")[台17線](../Page/台17線.md "wikilink")）
  - [西港大橋](../Page/西港大橋.md "wikilink")（[TW_PHW19.svg](https://zh.wikipedia.org/wiki/File:TW_PHW19.svg "fig:TW_PHW19.svg")[台19線](../Page/台19線.md "wikilink")）
  - [國道一號曾文溪橋](../Page/國道一號曾文溪橋.md "wikilink")（[TWHW1.svg](https://zh.wikipedia.org/wiki/File:TWHW1.svg "fig:TWHW1.svg")[國道一號](../Page/中山高速公路.md "wikilink")）
  - [麻善大橋](../Page/麻善大橋.md "wikilink")（[TW_PHW19a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW19a.svg "fig:TW_PHW19a.svg")[台19甲線](../Page/台19線#甲線.md "wikilink")）
  - [高鐵曾文溪橋](../Page/高鐵曾文溪橋.md "wikilink")（[台灣高鐵](../Page/台灣高鐵.md "wikilink")）
  - [台鐵曾文溪橋](../Page/台鐵曾文溪橋.md "wikilink")（[台鐵](../Page/台鐵.md "wikilink")[西部幹線](../Page/西部幹線.md "wikilink")）
  - [曾文二號橋](../Page/曾文二號橋.md "wikilink")（[TW_PHW1.svg](https://zh.wikipedia.org/wiki/File:TW_PHW1.svg "fig:TW_PHW1.svg")[台1線](../Page/台1線.md "wikilink")）
  - [嘉南大圳曾文溪橋](../Page/嘉南大圳曾文溪橋.md "wikilink")
  - [曾文溪橋](../Page/曾文溪橋.md "wikilink")
  - [國道三號曾文溪橋](../Page/國道三號曾文溪橋.md "wikilink")（[TWHW3.svg](https://zh.wikipedia.org/wiki/File:TWHW3.svg "fig:TWHW3.svg")[國道三號](../Page/福爾摩沙高速公路.md "wikilink")）
  - [大內橋](../Page/大內橋.md "wikilink")（[TW_THWtn182.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn182.svg "fig:TW_THWtn182.svg")南182線）
  - [二溪大橋](../Page/二溪大橋.md "wikilink")（[TW_THWtn182.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn182.svg "fig:TW_THWtn182.svg")南182線）
  - [台84線曾文溪一號橋](../Page/台84線曾文溪一號橋.md "wikilink")（[TW_PHW84.svg](https://zh.wikipedia.org/wiki/File:TW_PHW84.svg "fig:TW_PHW84.svg")[台84線](../Page/台84線.md "wikilink")）
  - [台84線曾文溪二號橋](../Page/台84線曾文溪二號橋.md "wikilink")（[TW_PHW84.svg](https://zh.wikipedia.org/wiki/File:TW_PHW84.svg "fig:TW_PHW84.svg")[台84線](../Page/台84線.md "wikilink")）
  - 無名橋（[TW_THWtn182.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn182.svg "fig:TW_THWtn182.svg")南182線）
  - [走馬瀨大橋](../Page/走馬瀨大橋.md "wikilink")（[TW_THWtn182-1.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn182-1.svg "fig:TW_THWtn182-1.svg")南182-1線）

### 大埔溪河段

  - [豐里吊橋](../Page/豐里吊橋.md "wikilink")
  - [玉豐大橋](../Page/玉豐大橋.md "wikilink")（[TW_THWtn183.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn183.svg "fig:TW_THWtn183.svg")南183線）
  - [台84線大埔溪橋](../Page/台84線大埔溪橋.md "wikilink")（[TW_PHW84.svg](https://zh.wikipedia.org/wiki/File:TW_PHW84.svg "fig:TW_PHW84.svg")[台84線](../Page/台84線.md "wikilink")）
  - [中正橋](../Page/中正橋_\(曾文溪\).md "wikilink")（[TW_THWtn189.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn189.svg "fig:TW_THWtn189.svg")南189線）
  - [永興吊橋](../Page/永興吊橋.md "wikilink")
  - [曾文一號橋](../Page/曾文一號橋.md "wikilink")（[TW_CHW174.svg](https://zh.wikipedia.org/wiki/File:TW_CHW174.svg "fig:TW_CHW174.svg")[市道174號](../Page/市道174號.md "wikilink")）
  - [曾文二號橋](../Page/曾文二號橋.md "wikilink")（[TW_THWtn183.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn183.svg "fig:TW_THWtn183.svg")南183線）
  - [曾文三號橋](../Page/曾文三號橋.md "wikilink")（[TW_THWtn183.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn183.svg "fig:TW_THWtn183.svg")南183線）
  - [曾文五號橋](../Page/曾文五號橋.md "wikilink")（[TW_THWtn183.svg](https://zh.wikipedia.org/wiki/File:TW_THWtn183.svg "fig:TW_THWtn183.svg")南183線）
  - [曾文水庫壩底道路](../Page/曾文水庫.md "wikilink")
  - 曾文水庫壩頂道路
  - [大埔橋](../Page/大埔橋.md "wikilink")（[TW_PHW3.svg](https://zh.wikipedia.org/wiki/File:TW_PHW3.svg "fig:TW_PHW3.svg")[台3線](../Page/台3線.md "wikilink")）
  - [內葉翅吊橋](../Page/內葉翅吊橋.md "wikilink")
  - [射兔潭吊橋](../Page/射兔潭吊橋.md "wikilink")
  - [茶山橋](../Page/茶山橋.md "wikilink")（[鄉道嘉](../Page/嘉義縣鄉道列表.md "wikilink")129線）
  - [山美大橋](../Page/山美大橋.md "wikilink")（[鄉道嘉](../Page/嘉義縣鄉道列表.md "wikilink")129線）
  - [達邦橋](../Page/達邦橋.md "wikilink")（[TW_CHW169.svg](https://zh.wikipedia.org/wiki/File:TW_CHW169.svg "fig:TW_CHW169.svg")[縣道169號](../Page/縣道169號.md "wikilink")）

### 後大埔溪河段

  - [巴沙娜大橋](../Page/巴沙娜大橋.md "wikilink")

## 相關條目

  - [中央管河川](../Page/中央管河川.md "wikilink")
  - [台灣河流列表](../Page/台灣河流列表.md "wikilink")
  - [台灣河流長度列表](../Page/台灣河流長度列表.md "wikilink")

## 註釋

## 參考來源

## 外部連結

  - [讓我們看河去(重要河川)--
    曾文溪-經濟部水利署](https://web.archive.org/web/20111020050550/http://www.wra.gov.tw/ct.asp?xItem=20081&CtNode=4362)

[曾文溪水系](../Category/曾文溪水系.md "wikilink")
[Category:台南市河川](../Category/台南市河川.md "wikilink")
[Category:嘉義縣河川](../Category/嘉義縣河川.md "wikilink")
[Category:高雄市河川](../Category/高雄市河川.md "wikilink")

1.  [〈曾文溪 南瀛第一河〉，《川流台灣》](http://www.rhythmsmonthly.com/?p=212)

2.
3.