**深雪**(1970年08月14日)，原名**羅穎思**，英文名**Zita
Law**，[香港女作家](../Page/香港.md "wikilink")。

## 生平簡介

出生時受洗為[天主教徒](../Page/天主教.md "wikilink")，但非常喜歡研究其他[宗教](../Page/宗教.md "wikilink")。深雪曾就讀[德雅中學](../Page/德雅中學.md "wikilink")，畢業於[香港大學歷史系](../Page/香港大學.md "wikilink")，另外又修畢一個法律文憑課程。早於中七時就參加了《東方日報全港學生暑假徵文比賽》高級組獲得亞軍，然後在大學二年級的暑假，寫了兩篇5000字的小說，一篇寄去《明報週刊》，另一篇寄了給《清新週刊》；三天之後，《清新週刊》的負責人找她見面，然後便開始每星期寫一篇5000字的大學生短篇小說，那些文章後來給[勤十緣結集成書了成了她的第一本小說](../Page/勤十緣.md "wikilink")《絕對討厭的女孩》。21歲大學畢業後，在《[Yes\!](../Page/Yes!.md "wikilink")》雜誌社做了一年記者，以「深雪」筆名寫鬼怪故事的專欄。在兩年後，[皇冠出版社替她的鬼故事出結集](../Page/皇冠出版社.md "wikilink")《夜霜艷》。廿四歲開始寫《Amoeba》。在九十年代中，深雪寫了大量的短篇小說，分別刊載於《Amoeba》、《姊妹》、《星島日報》、《星島晚報》、《明報》等等各大媒體。後來，在《東方日報》寫男女感情專欄。

深雪的小說《第8號當舖》於2004 年在台灣拍成電視連續劇，成績斐然，風靡了台灣、中國大陸、東南亞等地，唯獨香港沒有播放過。
《第8號當舖》也被改編為舞台劇，2017年6月開始，將會在中國國內巡演100場
。同於2004年，她亦於[TeenPower與](../Page/TeenPower.md "wikilink")[王貽興主持](../Page/王貽興.md "wikilink")《文字影院I》。2012年，舊作《兜售回憶》被改編拍成微電影。深雪榮獲路訊網
Roadshow 2013 一路最愛品牌大獎中的「人氣博客大獎」，她在 roadshow
網站寫的小說，是該網站人氣之冠。2015年，深雪替[盛君創作](../Page/盛君.md "wikilink")《誰懂我》MV微電影。2016年4月，中篇小說作品《借用下一世的男朋友》成為舞台劇。深雪是2018年香港書展年度作家之一。

## 筆名由來

深雪這個筆名最初是因為《鬼世界》雜誌。該雜誌編輯認為深雪的原名並不適合寫鬼故事，於是深雪就提供了深雪、小夜子，夜嬰三個筆名給編輯挑選。最後編輯選擇了深雪。

## 高陞戲院的雞人

深雪在1991年《鬼世界》寫的虛構短篇小說《雞人》被網民誤傳為香港真事，被網友評為
"十大香港都市傳說"，2013年9月24日深雪在其網站澄清《高陞戲院的雞人》故事純屬虛構。

## 作品

### 短篇小說

  - 《絕對討厭的女孩》
  - 《夜霜艷》
  - 《樹熊症病患者》
  - 《[我的左眼愛上你的右眼](../Page/我的左眼愛上你的右眼.md "wikilink")》
  - 《[戀愛動物不吃素](../Page/戀愛動物不吃素.md "wikilink")》
  - 《[我的愛情大減價](../Page/我的愛情大減價.md "wikilink")》
  - 《[我們都是粉紅色的女巫](../Page/我們都是粉紅色的女巫.md "wikilink")》
  - 《[送你一個夢想的手袋](../Page/送你一個夢想的手袋.md "wikilink")》
  - 《[夜嬰異色傳說](../Page/夜嬰異色傳說.md "wikilink")》
  - 《[樹熊的左眼愛上右眼](../Page/樹熊的左眼愛上右眼.md "wikilink")》（《樹熊症病患者》、《我的左眼愛上你的右眼》結集，再版）
  - 《[戀愛動物愛情大減價](../Page/戀愛動物愛情大減價.md "wikilink")》（《戀愛動物不吃素》、《我的愛情大減價》結集，再版）
  - 《[借用下一世的男朋友](../Page/借用下一世的男朋友.md "wikilink")》
  - 《[夜嬰十四夜](../Page/夜嬰十四夜.md "wikilink")》
  - 《[三個公主殺王子](../Page/三個公主殺王子.md "wikilink")》
  - 《[男朋友是無用鬼](../Page/男朋友是無用鬼.md "wikilink")》

### 合著作品

  - 《[床 - 都市愛情劇場](../Page/床_-_都市愛情劇場.md "wikilink")》
  - 《[完全自殺小說](../Page/完全自殺小說.md "wikilink")》

### 長篇小說

  - 《[我們戀愛](../Page/我們戀愛.md "wikilink")》
  - 《[貓的眼睛是紅色](../Page/貓的眼睛是紅色.md "wikilink")》
  - 《[藍色心形會所](../Page/藍色心形會所.md "wikilink")》
  - 《[櫻桃街的禮物](../Page/櫻桃街的禮物.md "wikilink")》
  - 《[飯盒五歲了](../Page/飯盒五歲了.md "wikilink")》
  - 《[兜售回憶](../Page/兜售回憶.md "wikilink")》
  - 《[新娘娃娃不祝福](../Page/新娘娃娃不祝福.md "wikilink")》
  - 《[早餐B](../Page/早餐B.md "wikilink")》
  - 《[月夜遺留了死心不息的眼睛](../Page/月夜遺留了死心不息的眼睛.md "wikilink")》
  - 《[當魔鬼談戀愛](../Page/當魔鬼談戀愛.md "wikilink")》
  - 《[貓眼二說](../Page/貓眼二說.md "wikilink")》
  - 《[深夜與早晨的周記](../Page/深夜與早晨的周記.md "wikilink")》
  - 《[第8號當鋪](../Page/第8號當舖_\(小說\).md "wikilink")》
  - 《[月日，別消失 1](../Page/月日，別消失_1.md "wikilink")》
  - 《[月日，別消失 2](../Page/月日，別消失_2.md "wikilink")》
  - 《[玫瑰奴隸王](../Page/玫瑰奴隸王.md "wikilink")》
  - 《[另一半的翅膀](../Page/另一半的翅膀.md "wikilink")》
  - 《[愛經述異](../Page/愛經述異.md "wikilink")》
  - 《[二姝夢1](../Page/二姝夢1.md "wikilink")》
  - 《[二姝夢2](../Page/二姝夢2.md "wikilink")》
  - 《[死神首曲1](../Page/死神首曲1.md "wikilink")》
  - 《[死神首曲2](../Page/死神首曲2.md "wikilink")》
  - 《[靈魂舞會](../Page/靈魂舞會.md "wikilink")》
  - 《[貓眼二說](../Page/貓眼二說.md "wikilink")》
  - 《[女神門](../Page/女神門.md "wikilink")》
  - 《[早餐B](../Page/早餐B.md "wikilink")》(再版)
  - 《[靈魂舞會2](../Page/靈魂舞會2.md "wikilink")》
  - 《[飯盒五歲了](../Page/飯盒五歲了.md "wikilink")》(再版)
  - 《[貓的眼睛是紅色](../Page/貓的眼睛是紅色.md "wikilink")》(再版)
  - 《[兜售回憶](../Page/兜售回憶.md "wikilink")》(再版)
  - 《[迷失在煙薰裡的夜](../Page/迷失在煙薰裡的夜.md "wikilink")》(再版)
  - 《[藍色心形會所](../Page/藍色心形會所.md "wikilink")》(再版)
  - 《[櫻桃街的禮物](../Page/櫻桃街的禮物.md "wikilink")》(再版)
  - 《[選新娘](../Page/選新娘.md "wikilink")》
  - 《[閃閃鑽石糖](../Page/閃閃鑽石糖.md "wikilink")》
  - 《[朝聖．愛．傳說](../Page/朝聖．愛．傳說.md "wikilink")》
  - 《[月夜遺留了死心不息的眼睛](../Page/月夜遺留了死心不息的眼睛.md "wikilink")》(聖藏版)
  - 《[幸謠說](../Page/幸謠說.md "wikilink")》
  - 《[人生拍賣會](../Page/人生拍賣會.md "wikilink")》
  - 《[人生拍賣會2之褫魂奪愛](../Page/人生拍賣會2之褫魂奪愛.md "wikilink")》
  - 《[三界愛事](../Page/三界愛事.md "wikilink")》
  - 《[人生拍賣會3之驕傲之旅](../Page/人生拍賣會3之驕傲之旅.md "wikilink")》
  - 《[神與人的遊戲](../Page/神與人的遊戲.md "wikilink")》
  - 《[神與人的遊戲2 光魅力暗魅力](../Page/神與人的遊戲2_光魅力暗魅力.md "wikilink")》
  - 《[盛女婚紗店](../Page/盛女婚紗店.md "wikilink")》
  - 《[邪惡家族I](../Page/邪惡家族I.md "wikilink")》
  - 《[第8號當舖](../Page/第8號當舖.md "wikilink")》(復刻版)
  - 《[邪惡家族II](../Page/邪惡家族II.md "wikilink")》
  - 《[另一半的翅膀](../Page/另一半的翅膀.md "wikilink")》(復刻版)
  - 《[玫瑰奴隸王](../Page/玫瑰奴隸王.md "wikilink")》(復刻版)
  - 《[邪惡家族III](../Page/邪惡家族III.md "wikilink")》
  - 《[愛經述異](../Page/愛經述異.md "wikilink")》(復刻版)
  - 《[烏托邦遊樂場](../Page/烏托邦遊樂場.md "wikilink")》
  - 《[烏托邦遊樂場II](../Page/烏托邦遊樂場II.md "wikilink")》
  - 《[幸愛花謠](../Page/幸愛花謠.md "wikilink")》
  - 《[幸愛花謠II](../Page/幸愛花謠II.md "wikilink")》

### 散文

  - 《[眼睛，不要愛上我的長髮](../Page/眼睛，不要愛上我的長髮.md "wikilink")》
  - 《[問題男女 - 238 條相處守則](../Page/問題男女_-_238_條相處守則.md "wikilink")》
  - 《[問題男女 - 戀愛四部曲](../Page/問題男女_-_戀愛四部曲.md "wikilink")》
  - 《[我的私密 CATALOG](../Page/我的私密_CATALOG.md "wikilink")》
  - 《[問題男女 - 戀愛說明書](../Page/問題男女_-_戀愛說明書.md "wikilink")》
  - 《[美女的交換禮物](../Page/美女的交換禮物.md "wikilink")》
  - 《[問題男女 - 愛情啟示錄](../Page/問題男女_-_愛情啟示錄.md "wikilink")》
  - 《[乳尖上的愛情腦](../Page/乳尖上的愛情腦.md "wikilink")》
  - 《[淺雪紀念中學](../Page/淺雪紀念中學.md "wikilink")》
  - 《[聰明女孩笨男友](../Page/聰明女孩笨男友.md "wikilink")》
  - 《[一人睡、二人睡](../Page/一人睡、二人睡.md "wikilink")》
  - 《[閃閃眼睛看法國](../Page/閃閃眼睛看法國.md "wikilink")》
  - 《[吃一粒愛情勝利糖](../Page/吃一粒愛情勝利糖.md "wikilink")》
  - 《[愛情冠后加冕日](../Page/愛情冠后加冕日.md "wikilink")》
  - 《[男人由好到賤](../Page/男人由好到賤.md "wikilink")》
  - 《[女人一秒都不能佔下風](../Page/女人一秒都不能佔下風.md "wikilink")》
  - 《[女人由愛到恨](../Page/女人由愛到恨.md "wikilink")》
  - 《[女人開放但不豪放](../Page/女人開放但不豪放.md "wikilink")》
  - 《[愛情 Last Card \!?](../Page/愛情_Last_Card_!?.md "wikilink")》
  - 《[男人是鞋，刮腳就要丟！](../Page/男人是鞋，刮腳就要丟！.md "wikilink")》
  - 《[那些女人是迷藥 這些男人是毒藥](../Page/那些女人是迷藥_這些男人是毒藥.md "wikilink")》
  - 《[魅幻女神的愛情魔力](../Page/魅幻女神的愛情魔力.md "wikilink")》
  - 《[有些男人是要來吃](../Page/有些男人是要來吃.md "wikilink")》
  - 《[那個最Man的人 原來是女人自己](../Page/那個最Man的人_原來是女人自己.md "wikilink")》
  - 《[相愛，不相欠](../Page/相愛，不相欠.md "wikilink")》(台版)
  - 《[核心問題不是你個胸](../Page/核心問題不是你個胸.md "wikilink")》
  - 《[男人的愛自己的愛，通殺](../Page/男人的愛自己的愛，通殺.md "wikilink")》
  - 《[自己眼淚自己吞](../Page/自己眼淚自己吞.md "wikilink")》
  - 《[人生，是一個決定](../Page/人生，是一個決定.md "wikilink")》(內地版)
  - 《[男人不過是一場幻覺](../Page/男人不過是一場幻覺.md "wikilink")》

## 外部連結

  - [深雪官方書迷會- 雪貓府](http://www.zitacatloft.com/) 官方網站
  - [深雪 Facebook](http://www.facebook.com/zitalaw.hk?ref=ts&fref=ts/)
  - [深雪的新浪微博](http://weibo.com/zitalaw/)

[category:香港小說家](../Page/category:香港小說家.md "wikilink")

[Category:羅姓](../Category/羅姓.md "wikilink")
[Category:香港女性作家](../Category/香港女性作家.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:德雅中學校友](../Category/德雅中學校友.md "wikilink")