**Unix
shell**，一種[殼層與](../Page/殼層.md "wikilink")[命令行界面](../Page/命令行界面.md "wikilink")，是[UNIX](../Page/UNIX.md "wikilink")[操作系统下传统的用户和计算机的交互界面](../Page/操作系统.md "wikilink")。第一個用户直接输入[命令来执行各种各样的任务](../Page/命令_\(计算机\).md "wikilink")。

普通意义上的shell就是可以接受用户输入命令的程序。它之所以被称作shell是因为它隐藏了操作系统低层的细节。同样的Unix下的图形用户界面[GNOME和](../Page/GNOME.md "wikilink")[KDE](../Page/KDE.md "wikilink")，有时也被叫做「虚拟shell」或「图形shell」。

Unix操作系统下的shell既是用户交互的界面，也是控制系统的[脚本语言](../Page/脚本语言.md "wikilink")。当然在这点也有别于Windows下的命令行，虽然也提供了很简单的控制语句。在Windows操作系统下，可能有些用户从来都不会直接的使用shell，然而在Unix系列操作系统下，shell仍然是控制系统启动、[X
Window启动和很多其他实用工具的脚本解释程序](../Page/X_Window.md "wikilink")。

## 各种Unix shell

第一個Unix
shell是由[肯·汤普逊](../Page/肯·汤普逊.md "wikilink")，仿效Multic上的shell所實作出來，稱為sh。

### Bourne shell兼容

  - [Bourne
    shell](../Page/Bourne_shell.md "wikilink")（sh）[史蒂夫·伯恩在](../Page/史蒂夫·伯恩.md "wikilink")[贝尔实验室时编写](../Page/贝尔实验室.md "wikilink")。1978年随[Version
    7 Unix首次发布](../Page/Version_7_Unix.md "wikilink")。
      - [Almquist shell](../Page/Almquist_shell.md "wikilink")（ash）
      - [Bourne-Again shell](../Page/bash.md "wikilink")（bash）
      - [Debian Almquist
        shell](../Page/Debian_Almquist_shell.md "wikilink")（dash）
      - [Korn
        shell](../Page/Korn_shell.md "wikilink")（ksh）在[贝尔实验室时编写](../Page/贝尔实验室.md "wikilink")。
      - [Z shell](../Page/Z_shell.md "wikilink")（zsh）

### C shell兼容

  - [C
    shell](../Page/C_shell.md "wikilink")（csh）[比尔·乔伊在](../Page/比尔·乔伊.md "wikilink")[加州大學伯克利分校时编写](../Page/加州大學伯克利分校.md "wikilink")。1979年随[BSD首次发布](../Page/BSD.md "wikilink")。
      - [TENEX C shell](../Page/tcsh.md "wikilink")（tcsh）

### 其他

  - [fish](../Page/Friendly_interactive_shell.md "wikilink")，第一次发布于2005年。

  - （rc）[九號計畫系统的shell](../Page/貝爾實驗室九號計畫.md "wikilink")，由在[贝尔实验室时编写](../Page/贝尔实验室.md "wikilink")。随后移植回Unix和其他的操作系统。

      - （es）一个[函数式编程的rc兼容shell](../Page/函数式编程.md "wikilink")，编写于二十世纪九十年代中期。

  - （Scheme Shell）

### 仅存于历史的

  - [Thompson shell](../Page/Thompson_shell.md "wikilink")（sh）第一个Unix
    shell，由[肯·汤普逊在](../Page/肯·汤普逊.md "wikilink")[贝尔实验室时编写](../Page/贝尔实验室.md "wikilink")。1971年至1975年随Unix第一版至第六版发布。

  - （sh）Thompson
    shell的一个版本，由和他人在[贝尔实验室时改进](../Page/贝尔实验室.md "wikilink")。1976年随PWB
    UNIX发布。

## 外部链接

  - [Linux Shell Scripting Tutorial - A Beginner's
    handbook](http://www.freeos.com/guides/lsst/)
  - [The Unix Shell:
    Introduction](https://web.archive.org/web/20130820203444/http://software-carpentry.org/v4/shell/intro.html)

{{-}}

[Unix_shells](../Category/Unix_shells.md "wikilink")