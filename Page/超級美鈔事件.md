**超級美鈔**（）泛指像真度近乎完美的[美元](../Page/美元.md "wikilink")[偽鈔](../Page/偽鈔.md "wikilink")，據稱是由[北韓印製](../Page/北韓.md "wikilink")。[美國政府指出北韓製造偽鈔的兩大原因](../Page/美國政府.md "wikilink")，其一是增加收入，其二是拖累[美國經濟](../Page/美國經濟.md "wikilink")。這些偽鈔於1980年代後期開始流通。北韓方面則否認有關指控，稱這是「純屬謊言」，以及美國想藉此案件挑起[戰爭](../Page/戰爭.md "wikilink")。最近北韓更指這些「超級美鈔」為[美國中情局自身印製](../Page/美國中情局.md "wikilink")，並企圖把案件轉嫁給[北韓政府](../Page/北韓政府.md "wikilink")。

## 鈔票

這些鈔票以最高級油墨及紙張印製，代碼為Q17、R18、T20、U21、V22、Y25和Z26，以試圖完整複製整套防偽特徵，包括紅藍纖維、防偽線、防偽[浮水印等](../Page/浮水印.md "wikilink")，使鑑證專家在檢驗鈔票前，也需作全面的仔細研究，方能鑑別真假。[美國經濟情報局把這些超級偽鈔分類為PN](../Page/美國經濟情報局.md "wikilink")-14342，這些鈔票的印程應用了[凹版印刷及](../Page/凹版印刷.md "wikilink")[字體排印技術](../Page/字体排印学.md "wikilink")。

不少逃出北韓的人指稱這些鈔票確實是來自北韓，廠房別名為「39辦公室」\[1\]，位於[平安南道的](../Page/平安南道.md "wikilink")[平城市](../Page/平城市.md "wikilink")（）。其中一名逃出北韓的人把偽鈔送交予[南韓](../Page/南韓.md "wikilink")[国家情报院的鑑證專家](../Page/韩国國家情報院.md "wikilink")，他們也不相信這些均是偽鈔。\[2\]《舊金山記事報》（*San
Francisco
Chronicle*）曾訪問一名知情的北韓化學家，他供出鈔票的技術詳情，散佈方面則是透過北韓的外交官員及國際犯罪組織將之流通的。

根据情报人士透露，“超级美元”最早是由中情局在1970年代向[伊朗](../Page/伊朗.md "wikilink")[巴勒維](../Page/巴勒維.md "wikilink")[國王输出的技术](../Page/國王.md "wikilink")，用于购买[軍火](../Page/軍火.md "wikilink")、设备，以镇压伊朗革命；然而[伊朗革命胜利后](../Page/伊朗革命.md "wikilink")，新領導人[霍梅尼就利用和改良从美国获得假美元技术](../Page/霍梅尼.md "wikilink")，疯狂开动印刷机器。一方面是伊朗宗教[革命输出需要经费](../Page/革命输出.md "wikilink")，另一方面也是报复和打击美国。

## 散佈方法

這些「超級偽鈔」以下列兩條途徑散佈：

  - 北韓的[外交官員如要造訪其他國家](../Page/外交官.md "wikilink")，必先途經[莫斯科](../Page/莫斯科.md "wikilink")。他們從各北韓大使館接收美元作開支，這些偽鈔會以大約一比一的比例混在真鈔中，不少外交官員均不知悉存在偽鈔。

<!-- end list -->

  - [英國的地下犯罪網絡](../Page/英國.md "wikilink")。武裝組織「[愛爾蘭共和軍](../Page/愛爾蘭共和軍.md "wikilink")」參謀長Seán
    Garland曾與部分前[克格勃人員到訪莫斯科的北韓領事館](../Page/克格勃.md "wikilink")，使館委托Garland把偽鈔帶往[都柏林及](../Page/都柏林.md "wikilink")[伯明翰](../Page/伯明翰.md "wikilink")，並兌換為[英鎊及其他真鈔](../Page/英鎊.md "wikilink")。\[3\]\[4\]除此之外，他還涉及其他跨國犯罪活動，金額數以百萬計。Garland後來在[北愛爾蘭的](../Page/北愛爾蘭.md "wikilink")[貝爾法斯特被捕](../Page/貝爾法斯特.md "wikilink")，但透過保外就醫得以飛往[愛爾蘭共和國](../Page/愛爾蘭共和國.md "wikilink")。愛爾蘭並未與美國簽署引渡條約，但Garland表示，如果他受愛爾蘭法庭審判，便會向當局投降。

在調查人員確認源頭為北韓之前，曾懷疑這些偽鈔來自[伊朗或](../Page/伊朗.md "wikilink")[敘利亞](../Page/敘利亞.md "wikilink")。

## 打擊偽鈔

美國自2004年起便把目標鎖定為北韓，試圖遏止偽鈔流通，並調查[中國銀行](../Page/中國銀行.md "wikilink")、[匯業銀行及](../Page/匯業銀行.md "wikilink")[誠興銀行](../Page/誠興銀行.md "wikilink")\[5\]\[6\]，以及禁止美國國民使用[匯業銀行理財](../Page/匯業銀行.md "wikilink")。\[7\]\[8\]並威脅向北韓實施有關核武以外的制裁。

另在美國兩次執法行動中，合計把87名涉嫌藏有偽鈔的人士拘捕。

不過，有關超級美鈔的來源亦眾說紛紜。2007年1月，德國最大報章[法蘭克福彙報引述美國政府匿名消息指](../Page/法蘭克福彙報.md "wikilink")，這批偽鈔來源華盛頓附近一間由[中情局擁有的印刷廠](../Page/中情局.md "wikilink")，並質疑中情局利用偽鈔作為經費，避免受制於國會的預算，但目前美國政府仍未對指控作回應。\[9\]

[中國大陸和](../Page/中國大陸.md "wikilink")[南韓起初不大相信北韓是這些偽鈔的源頭](../Page/南韓.md "wikilink")，2006年1月11日，[北京方面發表自身調查結果](../Page/北京.md "wikilink")，指北韓不能逃避罪責，並會向北韓施壓促使停止有關活動。南韓則試圖扮演中間人角色，聯合各政黨並以溫和態度發表可接受的結果。\[10\]同年1月22日的大型會議中，美國供出有關證據，並要求南韓制裁北韓，但被拒絕。\[11\]\[12\]南韓稱會向北韓有關組織作出譴責，而非北韓政府，認為不使用強硬措辭可說服北韓繼續[六方會談](../Page/朝核六方会谈.md "wikilink")。有人認為，南韓此舉有損[韓美關係](../Page/韓美關係.md "wikilink")，並矮化在北韓問題中的扮演角色。\[13\]

不少北韓[外交官員曾因藏有偽鈔而被捕](../Page/外交官.md "wikilink")，但他們透過[外交豁免權避過執法部門的指控](../Page/外交豁免權.md "wikilink")。\[14\]

2006年2月2日，[日本各銀行跟隨美國](../Page/日本.md "wikilink")，自發性向匯業銀行實施制裁。\[15\]同年8月日本《[產經新聞](../Page/產經新聞.md "wikilink")》報導指北韓在10個國家共開設23個賬戶，主要用作[清洗黑錢](../Page/洗錢.md "wikilink")。\[16\]

## 參考資料

## 外部連結

  - [造假鈔疑雲：美國特務在朝鮮供出“驚人證據”
    (多維新聞網) 2010-3-01 22:58](http://global.dwnews.com/big5/news/2010-03-01/55311542.html)

[Category:美國貨幣](../Category/美國貨幣.md "wikilink")
[Category:經濟犯罪](../Category/經濟犯罪.md "wikilink")
[Category:朝鮮民主主義人民共和國經濟](../Category/朝鮮民主主義人民共和國經濟.md "wikilink")
[Category:朝美關係](../Category/朝美關係.md "wikilink")
[Category:偽造貨幣](../Category/偽造貨幣.md "wikilink")

1.
2.
3.
4.
5.  <http://english.chosun.com/w21data/html/news/200509/200509080007.html>
6.
7.  \[ *Treasury Designates Banco Delta Asia as Primary Money Laundering
    Concern under USA PATRIOT Act*, [美國財政部](../Page/美國財政部.md "wikilink")
    新聞發佈
8.  <http://www.banktech.com/aml/showArticle.jhtml?articleID=175002415>
9.  <http://www.globalresearch.ca/index.php?context=viewArticle&code=BEN20070109&articleId=4393>
10.
11.
12.
13.
14.
15.
16.