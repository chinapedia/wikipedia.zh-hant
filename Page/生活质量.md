<div style="float:right;width:400px;padding-left:15px">

<center>

**[马斯洛的](../Page/亚伯拉罕·马斯洛.md "wikilink")[需求金字塔上達到越高層的人](../Page/需求層次理論.md "wikilink")
可以視同生活品質越高**

|                                       |
| :-----------------------------------: |
| [自我实现的需求](../Page/自我实现.md "wikilink") |

|              |
| :----------: |
| 尊重需求（社会承认需求） |

|              |
| :----------: |
| 社交需求（社会关系需求） |

|       |
| :---: |
| 安全的需求 |

|               |
| :-----------: |
| 生理的需求（身体基本需求） |

</center>

</div>

**生活質素**（Quality of
Life，簡稱QOL），又称**生活素質**或**生活品質**，是对人们生活好坏程度的一个[衡量](../Page/衡量.md "wikilink")。生活品質与客观意义上的[生活水平有关](../Page/生活水平.md "wikilink")，但也有所区别。简单地说，一定程度的生活水平较高生活品質的[必要条件](../Page/必要条件.md "wikilink")，但不是[充分条件](../Page/充分条件.md "wikilink")。除了保持基本的物质生活水平及身心[健康之外](../Page/健康.md "wikilink")，生活品質也取决于人们是否能够获得[快乐](../Page/快乐.md "wikilink")、[幸福](../Page/幸福.md "wikilink")、是与人的精神文化方面的追求，对[社会与](../Page/社会.md "wikilink")[环境的认同有着密切关系](../Page/环境.md "wikilink")。「生活素質」是包括了人們在「生活」和「物質生活」的滿足程度；生活素質主要指對生活、家庭、工作和健康等領域的滿意程度，以及對幸福和快樂等的主觀感受。

## 参见

  - [人类发展指数](../Page/人类发展指数.md "wikilink")
  - [生活质量指数](../Page/生活质量指数.md "wikilink")
  - [生活满意度指数](../Page/生活满意度指数.md "wikilink")
  - [国民幸福总值](../Page/国民幸福总值.md "wikilink")
  - [生命的意义](../Page/生命的意义.md "wikilink")
  - [预期寿命](../Page/预期寿命.md "wikilink")
  - [休闲](../Page/休闲.md "wikilink")
  - [社会保障](../Page/社会保障.md "wikilink")、[社会福利与](../Page/社会福利.md "wikilink")[社会支持](../Page/社会支持.md "wikilink")
  - [生活水平](../Page/生活水平.md "wikilink")

[Category:生活](../Category/生活.md "wikilink")
[Category:品質](../Category/品質.md "wikilink")
[生活质量](../Category/生活质量.md "wikilink")
[Category:健康、教育和福利经济学](../Category/健康、教育和福利经济学.md "wikilink")
[Category:简单生活](../Category/简单生活.md "wikilink")
[Category:积极心态](../Category/积极心态.md "wikilink")