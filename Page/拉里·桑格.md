**拉里·桑格**，全名**劳伦斯·马克·桑格**（，）生於[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")，是美国哲学家、[Nupedia和](../Page/Nupedia.md "wikilink")[维基百科的創立者之一](../Page/维基百科.md "wikilink")，并建立了[大众百科](../Page/大众百科.md "wikilink")\[1\]\[2\]\[3\]。

## 生平

出生於華盛頓州，在[阿拉斯加州](../Page/阿拉斯加州.md "wikilink")[安克拉治成長](../Page/安克拉治.md "wikilink")，畢業於[俄亥俄州立大學](../Page/俄亥俄州立大學.md "wikilink")，2000年3月，拉里·桑格受聘於[吉米·威爾士經營的網路公司](../Page/吉米·威爾士.md "wikilink")[Bomis](../Page/Bomis.md "wikilink")，協助創立一個線上[百科全書](../Page/百科全書.md "wikilink")
—
Nupedia（Wikipedia前身），並擔任該百科全書的主編。但因Nupedia的内容是由各領域專家透過[同儕評閱後才發佈的](../Page/同儕評閱.md "wikilink")，所以進度緩慢，發展過程並不十分順利。拉里·桑格便於2001年1月向吉米·威爾士提議以[Wiki作為百科全書平台](../Page/Wiki.md "wikilink")，發起另一個稱為Wikipedia的旁系計畫，並全力投入Nupedia和Wikipedia的工作，直至2002年3月1日離職為止。2006年9月，他宣佈發行一個新計劃，名為[大眾百科](../Page/大眾百科.md "wikilink")。他在2017年12月加入[Everipedia擔任](../Page/Everipedia.md "wikilink")[資訊總監](../Page/資訊總監.md "wikilink")\[4\]。

## 参考文献

[Category:维基百科人物](../Category/维基百科人物.md "wikilink")
[Category:美國企業家](../Category/美國企業家.md "wikilink")
[Category:華盛頓州人](../Category/華盛頓州人.md "wikilink")
[Category:阿拉斯加州人](../Category/阿拉斯加州人.md "wikilink")
[Category:里德學院校友](../Category/里德學院校友.md "wikilink")
[Category:俄亥俄州立大學校友](../Category/俄亥俄州立大學校友.md "wikilink")

1.
2.
3.
4.