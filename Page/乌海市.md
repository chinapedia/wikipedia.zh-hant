**乌海市**-{（）}-是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[内蒙古自治区下辖的](../Page/内蒙古自治区.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于内蒙古西部，地处[黄河上游](../Page/黄河.md "wikilink")，[乌兰布和沙漠边缘](../Page/乌兰布和沙漠.md "wikilink")，总面积1,669平方公里，人口55.58万。是一座立足于[煤炭开采的](../Page/煤.md "wikilink")[资源型城市](../Page/资源枯竭城市.md "wikilink")。

## 历史

  - 1955年2月成立[伊克昭盟](../Page/伊克昭盟.md "wikilink")[桌子山矿区办事处](../Page/桌子山矿区.md "wikilink")。1958年11月设立[阿拉善旗](../Page/阿拉善旗.md "wikilink")**乌达镇**；
  - 1961年7月9日，经[国务院批准建立](../Page/国务院.md "wikilink")[乌达市](../Page/乌达市.md "wikilink")（原乌达镇）和[海勃湾市](../Page/海勃湾市.md "wikilink")（原桌子山矿区），10月1日正式成立，分别隶属于[巴彦淖尔盟和](../Page/巴彦淖尔盟.md "wikilink")[伊克昭盟](../Page/伊克昭盟.md "wikilink")；
  - 1975年8月30日，经国务院批准海勃湾市和乌达市合并建立**乌海市**，1976年1月10日正式成立，为内蒙古自治区[省辖市](../Page/省辖市.md "wikilink")，市[人民政府设在海勃湾](../Page/人民政府.md "wikilink")，下辖[乌达](../Page/乌达.md "wikilink")、[海勃湾](../Page/海勃湾.md "wikilink")、[拉僧庙](../Page/拉僧庙县.md "wikilink")3个[县级办事处](../Page/县.md "wikilink")；
  - 1979年12月，将3个办事处改设为区，同时，将拉僧庙办事处更名为[海南区](../Page/海南区.md "wikilink")。<ref name="历史沿革">

</ref>

## 地理

乌海位于内蒙古西部，[黄河上游](../Page/黄河.md "wikilink")，[黄河穿市而过](../Page/黄河.md "wikilink")，西岸是[乌达](../Page/乌达.md "wikilink")，东岸是[海勃湾和海南](../Page/海勃湾.md "wikilink")，毗邻：内蒙古[鄂尔多斯市](../Page/鄂尔多斯市.md "wikilink")（东，北）、[宁夏](../Page/宁夏.md "wikilink")[石嘴山市](../Page/石嘴山市.md "wikilink")（南）、内蒙古[阿拉善盟](../Page/阿拉善盟.md "wikilink")（西），属[荒漠化草原](../Page/荒漠化草原.md "wikilink")、[草原化荒漠过渡带](../Page/草原化荒漠.md "wikilink")，平均海拔1150米。

### 气候

乌海气候属于典型的[温带干旱气候](../Page/温带干旱气候.md "wikilink")，干燥少[雨](../Page/雨.md "wikilink")，大[风多沙](../Page/风.md "wikilink")，[日照时间长](../Page/日照.md "wikilink")，年平均气温7.8-8.1℃\[1\]。建设在黄河干流上的[海勃湾水利枢纽工程蓄水后形成](../Page/海勃湾水利枢纽.md "wikilink")118平方公里的[乌海湖改善当地气候](../Page/乌海湖.md "wikilink")。\[2\]

### 自然资源

[煤炭已探明储量](../Page/煤炭.md "wikilink")30亿[吨](../Page/吨.md "wikilink")，以优质焦煤为主。但经过多年开采，[煤炭资源渐近枯竭](../Page/煤炭.md "wikilink")，现有储量仅够开采20多年\[3\]。[铁矿石](../Page/铁矿石.md "wikilink")600多万吨；[煤系](../Page/煤.md "wikilink")[高岭土](../Page/高岭土.md "wikilink")11亿吨以上，约占全国探明储量的1/5。[石灰石远景储量在](../Page/石灰石.md "wikilink")200亿吨以上，高品质的[石英砂](../Page/石英砂.md "wikilink")、[石英岩总储量达](../Page/石英岩.md "wikilink")50亿吨，[白云岩](../Page/白云岩.md "wikilink")、[耐火粘土](../Page/耐火粘土.md "wikilink")、[硅石储量也很丰富](../Page/硅石.md "wikilink")。\[4\]

## 政治

### 现任领导

<table>
<caption>乌海市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党乌海市委员会.md" title="wikilink">中国共产党<br />
乌海市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/乌海市人民代表大会.md" title="wikilink">乌海市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/乌海市人民政府.md" title="wikilink">乌海市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议乌海市委员会.md" title="wikilink">中国人民政治协商会议<br />
乌海市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/史万钧.md" title="wikilink">史万钧</a>[5]</p></td>
<td><p><a href="../Page/郝健君.md" title="wikilink">郝健君</a>[6]</p></td>
<td><p><a href="../Page/高世宏.md" title="wikilink">高世宏</a>[7]</p></td>
<td><p><a href="../Page/王文杰_(1964年).md" title="wikilink">王文杰</a>[8]</p></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/蒙古族.md" title="wikilink">蒙古族</a></p></td>
<td><p>蒙古族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/内蒙古自治区.md" title="wikilink">内蒙古自治区</a><a href="../Page/赤峰市.md" title="wikilink">赤峰市</a></p></td>
<td><p>内蒙古自治区<a href="../Page/准格尔旗.md" title="wikilink">准格尔旗</a></p></td>
<td><p><a href="../Page/河北省.md" title="wikilink">河北省</a><a href="../Page/辛集市.md" title="wikilink">辛集市</a></p></td>
<td><p>内蒙古自治区赤峰市</p></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年12月</p></td>
<td><p>2017年2月</p></td>
<td><p>2017年2月</p></td>
<td><p>2018年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖3个[市辖区](../Page/市辖区.md "wikilink")：[海勃湾区](../Page/海勃湾区.md "wikilink")、[乌达区](../Page/乌达区.md "wikilink")、[海南区](../Page/海南区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>乌海市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[9]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>150300</p></td>
</tr>
<tr class="odd">
<td><p>150302</p></td>
</tr>
<tr class="even">
<td><p>150303</p></td>
</tr>
<tr class="odd">
<td><p>150304</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>乌海市各区人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[10]（2010年11月）</p></th>
<th><p>户籍人口[11]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>乌海市</p></td>
<td><p>532902</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>海勃湾区</p></td>
<td><p>296177</p></td>
<td><p>55.58</p></td>
</tr>
<tr class="even">
<td><p>海南区</p></td>
<td><p>103355</p></td>
<td><p>19.39</p></td>
</tr>
<tr class="odd">
<td><p>乌达区</p></td>
<td><p>133370</p></td>
<td><p>25.03</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")532902人\[12\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加97977人，增长22.53%。年平均增长率为2.05%。其中，男性人口为282326人，占52.98%；女性人口为250576人，占47.02%。总人口性别比（以女性为100）为112.67。0－14岁人口为74284人，占13.94%；15－64岁人口为421704人，占79.13%；65岁及以上人口为36914人，占6.93%。

### 民族

常住人口中，[汉族人口为](../Page/汉族.md "wikilink")496725人，占93.21%；[蒙古族人口为](../Page/蒙古族.md "wikilink")18952人，占3.56%；其他[少数民族人口为](../Page/少数民族.md "wikilink")17225人，占3.23%。

乌海的人口大多以外来移民为主，海南区以来自[河南的移民占多数](../Page/河南.md "wikilink")。

{{-}}

| 民族名称         | [汉族](../Page/汉族.md "wikilink") | [蒙古族](../Page/蒙古族.md "wikilink") | [回族](../Page/回族.md "wikilink") | [满族](../Page/满族.md "wikilink") | [藏族](../Page/藏族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | [东乡族](../Page/东乡族.md "wikilink") | [达斡尔族](../Page/达斡尔族.md "wikilink") | [朝鲜族](../Page/朝鲜族.md "wikilink") | [苗族](../Page/苗族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | -------------------------------- | -------------------------------- | ---------------------------------- | -------------------------------- | ------------------------------ | ---- |
| 人口数          | 496725                         | 18952                            | 11732                          | 4127                           | 311                            | 141                              | 129                              | 124                                | 97                               | 91                             | 473  |
| 占总人口比例（%）    | 93.21                          | 3.56                             | 2.20                           | 0.77                           | 0.06                           | 0.03                             | 0.02                             | 0.02                               | 0.02                             | 0.02                           | 0.09 |
| 占少数民族人口比例（%） | \---                           | 52.39                            | 32.43                          | 11.41                          | 0.86                           | 0.39                             | 0.36                             | 0.34                               | 0.27                             | 0.25                           | 1.31 |

**乌海市民族构成（2010年11月）**\[13\]

## 交通

[乌海黄河落日.JPG](https://zh.wikipedia.org/wiki/File:乌海黄河落日.JPG "fig:乌海黄河落日.JPG")
[乌海甘德尔山.JPG](https://zh.wikipedia.org/wiki/File:乌海甘德尔山.JPG "fig:乌海甘德尔山.JPG")
乌海地处[华北和](../Page/华北.md "wikilink")[西北的交汇处](../Page/西北.md "wikilink")，是[华北与西北之间重要的交通枢纽](../Page/华北.md "wikilink")。

### 公路

  -   - 从海勃湾区北部进入市区，经过海南，穿过黄河到[阿拉善左旗](../Page/阿拉善左旗.md "wikilink")[乌斯太镇](../Page/乌斯太镇.md "wikilink")

  -   - [荣成](../Page/荣成.md "wikilink") - 乌海（乌海段在建）

  -   - 经过海勃湾区和乌达区

  -   - 经过海南区[公乌素镇和](../Page/公乌素镇.md "wikilink")[拉僧庙镇进入](../Page/拉僧庙镇.md "wikilink")[鄂尔多斯市](../Page/鄂尔多斯市.md "wikilink")[棋盘井镇](../Page/棋盘井镇.md "wikilink")

### 铁路

  - [包兰铁路](../Page/包兰铁路.md "wikilink")
      - 乌海的第一条铁路，和[110国道并行](../Page/110国道.md "wikilink")，设[乌海站](../Page/乌海站.md "wikilink")、[乌海西站](../Page/乌海西站.md "wikilink")、[乌海北站三个站](../Page/乌海北站.md "wikilink")。
  - [东乌铁路](../Page/东乌铁路.md "wikilink")
      - [东胜](../Page/东胜.md "wikilink") - 乌海，2008年建成通车。
  - [包](../Page/包头市.md "wikilink") [银高铁](../Page/银川市.md "wikilink")
      - 规划中，预计2018年底开始建设。

### 航空

[乌海机场于](../Page/乌海机场.md "wikilink")2003年12月通航，位于[乌海市以北](../Page/乌海市.md "wikilink")15公里处，建设为3C级飞行区，[航站楼面积](../Page/航站楼.md "wikilink")12500平方米，现开通[呼和浩特](../Page/呼和浩特.md "wikilink")，[北京](../Page/北京.md "wikilink")，[西安](../Page/西安.md "wikilink")，[广州](../Page/广州.md "wikilink")，[上海](../Page/上海.md "wikilink")，[包头](../Page/包头.md "wikilink")，[榆林](../Page/榆林.md "wikilink")，[银川](../Page/银川.md "wikilink")，[太原](../Page/太原.md "wikilink")，[海口](../Page/海口.md "wikilink")，[鄂尔多斯](../Page/鄂尔多斯.md "wikilink")，[石家庄](../Page/石家庄.md "wikilink")，[杭州](../Page/杭州.md "wikilink")，[成都航线](../Page/成都.md "wikilink")。

## 经济

**乌海市**[煤炭资源丰富](../Page/煤炭.md "wikilink")，炼焦用煤占[内蒙古已探明焦煤储量的](../Page/内蒙古.md "wikilink")60%，[经济以](../Page/经济.md "wikilink")[能源](../Page/能源.md "wikilink")、[化工](../Page/化工.md "wikilink")、[建材](../Page/建材.md "wikilink")、[特色冶金为主](../Page/特色冶金.md "wikilink")。属于典型的[资源型经济](../Page/资源型经济.md "wikilink")。

### 工业

[工业主要有煤炭化工](../Page/工业.md "wikilink")、氯碱化工、建材、特色冶金。

主要的工业产品有[电石](../Page/电石.md "wikilink")、金属[钠](../Page/钠.md "wikilink")、[平板玻璃](../Page/平板玻璃.md "wikilink")、[PVC](../Page/PVC.md "wikilink")、[焦炭](../Page/焦炭.md "wikilink")、[铁合金](../Page/铁合金.md "wikilink")、[水泥](../Page/水泥.md "wikilink")、[石灰氮](../Page/石灰氮.md "wikilink")、[双氰胺](../Page/双氰胺.md "wikilink")、[草酸](../Page/草酸.md "wikilink")。

[工业污染](../Page/工业污染.md "wikilink")
[乌海玻璃厂污染.JPG](https://zh.wikipedia.org/wiki/File:乌海玻璃厂污染.JPG "fig:乌海玻璃厂污染.JPG")\]\]

### 农业

  - 乌海[葡萄](../Page/葡萄.md "wikilink")
  - [高效农业](../Page/高效农业.md "wikilink")（[海勃湾区高效农业示范区](../Page/海勃湾区高效农业示范区.md "wikilink")）

## 文化

该市的文化以移民文化为主。

2008年9月10日乌海市被[中国书法家协会命名为中国书法城](../Page/中国书法家协会.md "wikilink")。 \[14\]

### 旅游

  - 金沙湾生态旅游区
  - [胡杨岛](../Page/胡杨.md "wikilink")
  - 桌子山岩画
  - 乌海湖
  - [四合木自然保护区](../Page/四合木.md "wikilink")
  - 龙游湾国家湿地公园
  - 满巴拉僧庙
  - 金沙湾

## 教育

### 高等院校

  - [乌海市职业技术学院](../Page/乌海市职业技术学院.md "wikilink")

### 中等职业学校

  - [乌海市职业技术学校](../Page/乌海市职业技术学校.md "wikilink")

### 高中

  - [乌海市第一中学](../Page/乌海市第一中学.md "wikilink")
  - [乌海市滨河中学](../Page/乌海市滨河中学.md "wikilink")(又名[乌海市第六中学](../Page/乌海市第六中学.md "wikilink"))
  - [乌海市第十中学](../Page/乌海市第十中学.md "wikilink")（原[乌达区高级中学](../Page/乌达区高级中学.md "wikilink")）

### 初中

  - [乌海市第二中学](../Page/乌海市第二中学.md "wikilink")
  - [乌海市第三中学](../Page/乌海市第三中学.md "wikilink")
  - [乌海市第四中学](../Page/乌海市第四中学.md "wikilink")
  - [乌海市第五中学](../Page/乌海市第五中学.md "wikilink")
  - [乌海市第六中学](../Page/乌海市第六中学.md "wikilink")
  - [乌海市第八中学](../Page/乌海市第八中学.md "wikilink")
  - [乌海市第九中学](../Page/乌海市第九中学.md "wikilink")
  - [乌海市第十八中学](../Page/乌海市第十八中学.md "wikilink")

### 小学

  - [乌海市实验小学](../Page/乌海市实验小学.md "wikilink")
  - [乌海市实验小学滨河校区](../Page/乌海市实验小学滨河校区.md "wikilink")
  - [海勃湾区第一小学](../Page/海勃湾区第一小学.md "wikilink")
  - [海勃湾区第二小学](../Page/海勃湾区第二小学.md "wikilink")
  - [海勃湾区第三小学](../Page/海勃湾区第三小学.md "wikilink")
  - [海勃湾区第四小学](../Page/海勃湾区第四小学.md "wikilink")
  - [海勃湾区第五小学](../Page/海勃湾区第五小学.md "wikilink")
  - [海勃湾区第六小学](../Page/海勃湾区第六小学.md "wikilink")
  - [海勃湾区第七小学](../Page/海勃湾区第七小学.md "wikilink")
  - [海勃湾区第八小学](../Page/海勃湾区第八小学.md "wikilink")
  - [海勃湾区第九小学](../Page/海勃湾区第九小学.md "wikilink")
  - [乌海市第九中学小学部](../Page/乌海市第九中学小学部.md "wikilink")

## 参考文献

## 外部链接

  - [Panoramio中的](../Page/Panoramio.md "wikilink")[乌海图片](https://web.archive.org/web/20141219052551/http://www.panoramio.com/map#lt=39.588757&ln=106.816635&z=6&k=2&a=1&tab=1#lt=39.588757&ln=106.816635&z=6&k=2&a=1&tab=1)
  - [乌海市政府信息网](http://www.wuhai.gov.cn)
  - [乌海日报](http://www.wuhaidaily.com)
  - [乌海电视台](http://www.nmwhtv.com)

[\*](../Category/乌海.md "wikilink")
[Category:内蒙古地级市](../Category/内蒙古地级市.md "wikilink")
[蒙](../Category/中国中等城市.md "wikilink")
[Wuhai](../Category/1976年建立的行政區劃.md "wikilink")
[Category:中华人民共和国第三批资源枯竭型城市](../Category/中华人民共和国第三批资源枯竭型城市.md "wikilink")

1.
2.  <http://www.hwcc.com.cn/newsdisplay/newsdisplay.asp?Id=221836>
3.  <http://www.hwcc.com.cn/newsdisplay/newsdisplay.asp?Id=221836>
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.