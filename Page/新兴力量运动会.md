**新兴力量运动会**（Games of the New Emerging
Forces，简称GANEFO），是在[印度尼西亚总统](../Page/印度尼西亚总统.md "wikilink")[苏加诺的倡导下举行的世界性运动会](../Page/苏加诺.md "wikilink")，举行一届后即告夭折。

## 背景

1962年，印度尼西亚举办[第四届亚洲运动会](../Page/第四届亚洲运动会.md "wikilink")，以[政治原因拒绝接受](../Page/政治.md "wikilink")[中华民国代表团以](../Page/中华民国.md "wikilink")[中华民国奥林匹克委员会的名义参加比赛](../Page/中华民国奥林匹克委员会.md "wikilink")，以[宗教原因拒绝接受](../Page/宗教.md "wikilink")[以色列参赛](../Page/以色列.md "wikilink")，导致[国际奥委会宣布不承认该届亚运会](../Page/国际奥委会.md "wikilink")。次年2月，国际奥委会宣布無限期暫停印尼参加[奥林匹克运动会](../Page/奥林匹克运动会.md "wikilink")。在此情况下，印尼总统苏加诺提出自行举办新兴力量运动会的倡议，希望能够创建一个国际奥委会之外的独立体育运动。

该倡议立即得到了已经退出国际奥委会的[中华人民共和国的欢迎](../Page/中华人民共和国.md "wikilink")。1962年11月，印尼和[中华人民共和国政府发表联合声明](../Page/中华人民共和国政府.md "wikilink")，宣布将于次年11月在印尼举办首届新兴力量运动会。

## 筹备

1963年4月，新兴力量运动会筹备会议在印尼[雅加达举行](../Page/雅加达.md "wikilink")，[柬埔寨](../Page/柬埔寨.md "wikilink")、中华人民共和国、[几内亚](../Page/几内亚.md "wikilink")、印度尼西亚、[伊拉克](../Page/伊拉克.md "wikilink")、[马里](../Page/马里.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[越南民主共和国](../Page/越南民主共和国.md "wikilink")、[阿拉伯联合共和国和](../Page/阿拉伯联合共和国.md "wikilink")[苏联十国代表](../Page/苏联.md "wikilink")，以及[锡兰和](../Page/斯里兰卡.md "wikilink")[南斯拉夫的观察员出席筹备会议](../Page/南斯拉夫.md "wikilink")。在筹备会议上，苏联代表提出将国际奥委会的宗旨写入新兴力量运动会章程，以表示新兴力量运动会是[奥林匹克运动的一部分](../Page/奥林匹克运动.md "wikilink")。该提议遭到了中华人民共和国等国的反对，最后经过妥协，各国一致同意以[奥林匹克理想同](../Page/奥林匹克精神.md "wikilink")[万隆会议精神并列](../Page/万隆会议.md "wikilink")，作为新兴力量运动会的宗旨，并决定新兴力量运动会四年举行一次。

## 举办

1963年11月10日，首届新兴力量运动会在雅加达开幕，共有48个国家和地区派遣代表团，共有2404名运动员参加了[篮球](../Page/篮球.md "wikilink")、[排球](../Page/排球.md "wikilink")、[足球](../Page/足球.md "wikilink")、[乒乓球](../Page/乒乓球.md "wikilink")、[羽毛球](../Page/羽毛球.md "wikilink")、[网球](../Page/网球.md "wikilink")、[曲棍球](../Page/曲棍球.md "wikilink")、[游泳](../Page/游泳.md "wikilink")、[水球](../Page/水球.md "wikilink")、[跳水](../Page/跳水.md "wikilink")、[举重](../Page/举重.md "wikilink")、[体操](../Page/体操.md "wikilink")、[射击](../Page/射击.md "wikilink")、[射箭](../Page/射箭.md "wikilink")、[拳击](../Page/拳击.md "wikilink")、[自行车](../Page/自行车.md "wikilink")、[击剑](../Page/击剑.md "wikilink")、[柔道](../Page/柔道.md "wikilink")、[摔跤和](../Page/摔跤.md "wikilink")[帆船共](../Page/帆船.md "wikilink")20个大项的比赛。尽遣精英出赛的中华人民共和国代表团独得60余枚金牌，位居金牌榜第一。苏联虽然派队参赛，但迫于国际奥委会的压力，仅派出不具奥运参赛资格的运动员。11月20日，运动会闭幕，在此次运动会上共打破了5项世界纪录。

</div>

</div>

会议期间，各国代表团决定第二届运动会将于1967年举行，首选地点是[阿拉伯联合共和国的](../Page/阿拉伯联合共和国.md "wikilink")[开罗](../Page/开罗.md "wikilink")，备选地点是中华人民共和国的[北京](../Page/北京.md "wikilink")。

## 夭折

新兴力量运动会的举办，招致了国际奥委会及其下属单项组织的反对，认为其将政治和[体育相联系](../Page/体育.md "wikilink")，违背了政治不干预体育的奥林匹克精神。新兴力量运动会举办之后，国际奥委会立即宣布全面封杀所有参加新兴力量运动会的运动员，取消他们参加奥运会的资格，一些单项组织也对参赛国进行了处罚。为此，中华人民共和国便出面举行了一些单项新兴力量运动会，比如举重、游泳，以安抚参赛各国，并协助[柬埔寨于](../Page/柬埔寨.md "wikilink")1966年11月在[金边举行了一届亚洲新兴力量运动会](../Page/金边.md "wikilink")，以对抗[亚运会](../Page/亚运会.md "wikilink")。

1964年，[阿拉伯联合共和国要求中华人民共和国援助其建设体育场馆](../Page/阿拉伯联合共和国.md "wikilink")，否则将放弃举办新兴力量运动会，由于其要求金额较高，双方迟迟不能达成协议，最后决定由北京接办第二届新兴力量运动会。北京为此紧急开工修建一批体育设施，著名的有[北京首都体育馆等](../Page/北京首都体育馆.md "wikilink")。但是1965年，印尼发生[九三〇事件](../Page/印尼九三零事件.md "wikilink")，苏加诺被推翻，[苏哈托上台后](../Page/苏哈托.md "wikilink")，对中华人民共和国持敌视态度，退出了新兴力量运动会。1966年，中华人民共和国国内发生[文化大革命](../Page/文化大革命.md "wikilink")，局势陷入混乱，举办第二届新兴力量运动会一事从此不了了之。随着印尼和中国的退出，新兴力量运动会也随之宣告夭折。

## 纪念

[中國人民郵政于](../Page/中國人民郵政.md "wikilink")1963年發行一套編號為紀100的名為「第一屆新興力量運動會」的[紀念郵票](../Page/紀念郵票.md "wikilink")，共5枚。設計者為周令钊、孙鸿年、孔绍惠、高品璋、唐霖坤。\[1\]

中國人民郵政于1966年發行一套編號為紀121的名為「第一屆亚洲新興力量運動會」的[紀念郵票](../Page/紀念郵票.md "wikilink")，共4枚。設計者為万维生、陈晓聪、李大玮。\[2\]

## 注释

## 参考文献

  - [何振梁传·梁丽娟著](http://book.sina.com.cn/nzt/cha/hezhenliang/34.shtml)
  - [中国奥委会·中国与奥林匹克运动史话](https://web.archive.org/web/20070823231328/http://www.olympic.cn/china/Olympics.html)
  - [YouTube Video of the 1st Asian
    GANEFO](http://www.youtube.com/watch?v=jbrXMREobwE)
  - Stefan Huebner, *Pan-Asian Sports and the Emergence of Modern Asia,
    1913-1974.* Singapore: NUS Press, 2016.

[Category:印尼主辦的國際體育賽事](../Category/印尼主辦的國際體育賽事.md "wikilink")
[Category:雅加达历史](../Category/雅加达历史.md "wikilink")
[Category:中华人民共和国体育史](../Category/中华人民共和国体育史.md "wikilink")
[Category:1963年11月](../Category/1963年11月.md "wikilink")
[Category:亞洲綜合運動會](../Category/亞洲綜合運動會.md "wikilink")
[Category:1963年建立的週期性體育事件](../Category/1963年建立的週期性體育事件.md "wikilink")
[Category:第三世界主義](../Category/第三世界主義.md "wikilink")
[Category:體育與政治](../Category/體育與政治.md "wikilink")

1.
2.