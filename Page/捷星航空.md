**捷星航空**（），也音译作“**捷達航空**”，是[澳洲航空旗下的一家全資子公司](../Page/澳洲航空.md "wikilink")，創立於2003年，總部設於[澳洲](../Page/澳洲.md "wikilink")[墨爾本](../Page/墨爾本.md "wikilink")，提供多條澳洲國內與國際的航班服務。

捷星航空最初设立的目的就是为與澳洲市场上的另一家低成本航空公司[维珍蓝](../Page/维珍蓝.md "wikilink")（Virgin
Blue）展开竞争，其机队拥有空中巴士A320、A330甚至波音B787夢幻飛機，捷星航空集团（Jetstar Group）又细分为\[1\]：

1.  澳大利亚和新西兰捷星航空（JQ）；
2.  新加坡的[捷星亚洲航空](../Page/捷星亚洲航空.md "wikilink")（Jetstar Asia
    Airways，3K，2004年成立）和[惠旅航空](../Page/惠旅航空.md "wikilink")（Valuair，VF，2005年成立），捷星航空集團均占股49%；
3.  越南的[捷星太平洋航空](../Page/捷星太平洋航空.md "wikilink")（Jetstar Pacific
    Airlines，BL,2007年成立），捷星航空集團占30%股权，越南航空占70%股权；
4.  [捷星日本航空](../Page/捷星日本航空.md "wikilink")（Jetstar
    Japan，GK），捷星航空集團占股33.3%，日本航空占股33.3%，其它股東還有[三菱商事等公司](../Page/三菱商事.md "wikilink")\[2\]；
5.  [捷星香港航空](../Page/捷星香港航空.md "wikilink")（Jetstar Hong
    Kong，JM，2012年成立），捷星航空集團占33.3%股权，[中国东方航空占](../Page/中国东方航空.md "wikilink")33.3%股权。

## 機隊

截至2017年11月，捷星航空機隊如下：

<table>
<caption><strong>捷星航空机队</strong></caption>
<thead>
<tr class="header">
<th><p><font style="color:white;">机型</p></th>
<th><p><font style="color:white;">总数</p></th>
<th><p><font style="color:white;">订购中</p></th>
<th><p><font style="color:white;">载客量</p></th>
<th><p><font style="color:white;">备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><abbr title="商务舱"><font style="color:white;">B</abbr></p></td>
<td><p><abbr title="经济舱"><font style="color:white;">E</abbr></p></td>
<td><p><font style="color:white;">总数</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/空中巴士A320.md" title="wikilink">空中巴士A320-200</a></p></td>
<td><center>
<p>52</p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>180<br />
186</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/空中巴士A321.md" title="wikilink">空中巴士A321-200</a></p></td>
<td><center>
<p>8</p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>220<br />
230</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/空中巴士A320neo系列.md" title="wikilink">空中巴士A320neo</a></p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>44</p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>186</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/空中巴士A320neo系列.md" title="wikilink">空中巴士A321neoLR</a></p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>45</p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>232</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/波音787.md" title="wikilink">波音787-8</a></p></td>
<td><center>
<p>11</p></td>
<td><center>
<p>—</p></td>
<td><center>
<p>21</p></td>
<td><center>
<p>314</p></td>
</tr>
<tr class="odd">
<td><center>
<p><strong>总计</strong></p></td>
<td><center>
<p><strong>71</strong></p></td>
<td><center>
<p><strong>99</strong></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 航線（捷星集團全部航空公司成員航線）

  - 數據截至2014年4月

[VH-VQH_-roos-_at_Melbourne_Airport.jpg](https://zh.wikipedia.org/wiki/File:VH-VQH_-roos-_at_Melbourne_Airport.jpg "fig:VH-VQH_-roos-_at_Melbourne_Airport.jpg")

### 亞洲

  - [緬甸](../Page/緬甸.md "wikilink")
      - [仰光](../Page/仰光.md "wikilink")-[仰光國際機場](../Page/仰光國際機場.md "wikilink")
  - [日本](../Page/日本.md "wikilink")
      - [札幌](../Page/札幌.md "wikilink")-[新千歲機場](../Page/新千歲機場.md "wikilink")
      - [大阪](../Page/大阪.md "wikilink")-[關西國際機場](../Page/關西國際機場.md "wikilink")
      - [東京](../Page/東京.md "wikilink")-[成田國際機場](../Page/成田國際機場.md "wikilink")
      - [名古屋](../Page/名古屋.md "wikilink")-[中部國際機場](../Page/中部國際機場.md "wikilink")
      - [高松](../Page/高松.md "wikilink")-[高松機場](../Page/高松機場.md "wikilink")
      - [大阪](../Page/大阪.md "wikilink")-[關西國際機場](../Page/關西國際機場.md "wikilink")
      - [松山](../Page/松山市.md "wikilink")-[愛媛縣松山機場](../Page/松山機場_\(日本\).md "wikilink")
      - [國東市](../Page/國東市.md "wikilink")-[大分機場](../Page/大分機場.md "wikilink")
      - [福岡](../Page/福岡.md "wikilink")-[福岡機場](../Page/福岡機場.md "wikilink")
      - [鹿兒島](../Page/鹿兒島.md "wikilink")-[鹿兒島機場](../Page/鹿兒島機場.md "wikilink")
      - [沖繩](../Page/沖繩.md "wikilink")-[那霸國際機場](../Page/那霸國際機場.md "wikilink")
  - [新加坡](../Page/新加坡.md "wikilink")
      - [新加坡](../Page/新加坡.md "wikilink")-[新加坡樟宜機場](../Page/新加坡樟宜機場.md "wikilink")
  - [中國大陸](../Page/中國大陸.md "wikilink")
      - [杭州](../Page/杭州.md "wikilink")-[萧山國際機場](../Page/萧山國際機場.md "wikilink")
      - [海口](../Page/海口.md "wikilink")-[美兰国际机场](../Page/美兰国际机场.md "wikilink")
      - [揭阳](../Page/揭阳.md "wikilink")-[潮汕機場](../Page/潮汕機場.md "wikilink")
      - [郑州](../Page/郑州.md "wikilink")-[新郑国际机场](../Page/新郑国际机场.md "wikilink")
  - [香港](../Page/香港.md "wikilink")
      - [香港](../Page/香港.md "wikilink")-[香港國際機場](../Page/香港國際機場.md "wikilink")
  - [澳門](../Page/澳門.md "wikilink")
      - [澳門](../Page/澳門.md "wikilink")-[澳門國際機場](../Page/澳門國際機場.md "wikilink")
  - [臺灣](../Page/臺灣.md "wikilink")
      - [臺北](../Page/臺北.md "wikilink")-[桃園國際機場](../Page/桃園國際機場.md "wikilink")
  - [泰國](../Page/泰國.md "wikilink")
      - [曼谷](../Page/曼谷.md "wikilink")-[蘇凡納布機場](../Page/蘇凡納布機場.md "wikilink")
      - [普吉府](../Page/普吉府.md "wikilink")-[普吉國際機場](../Page/普吉國際機場.md "wikilink")
  - [馬來西亞](../Page/馬來西亞.md "wikilink")
      - [吉隆坡](../Page/吉隆坡.md "wikilink")-[吉隆坡國際機場](../Page/吉隆坡國際機場.md "wikilink")
      - [檳城](../Page/檳城.md "wikilink")-[檳城國際機場](../Page/檳城國際機場.md "wikilink")
  - [越南](../Page/越南.md "wikilink")
      - [胡志明市](../Page/胡志明市.md "wikilink")-[新山一國際機場](../Page/新山一國際機場.md "wikilink")
      - [河內](../Page/河內.md "wikilink")-[內排國際機場](../Page/內排國際機場.md "wikilink")
      - [海防](../Page/海防.md "wikilink")-[吉碑國際機場](../Page/吉碑國際機場.md "wikilink")
      - [榮市](../Page/榮市.md "wikilink")-[榮市國際機場](../Page/榮市國際機場.md "wikilink")
      - [峴港](../Page/峴港.md "wikilink")-[峴港國際機場](../Page/峴港國際機場.md "wikilink")
      - [邦美蜀市](../Page/邦美蜀市.md "wikilink")-[邦美蜀機場](../Page/邦美蜀機場.md "wikilink")
      - [富國島](../Page/富國島.md "wikilink")-[富國國際機場](../Page/富國國際機場.md "wikilink")
  - [柬埔寨](../Page/柬埔寨.md "wikilink")
      - [暹粒](../Page/暹粒.md "wikilink")-[吳哥國際機場](../Page/吳哥國際機場.md "wikilink")
      - [金邊](../Page/金邊.md "wikilink")-[金邊國際機場](../Page/金邊國際機場.md "wikilink")
  - [菲律賓](../Page/菲律賓.md "wikilink")
      - [馬尼拉](../Page/馬尼拉.md "wikilink")-[尼诺伊·阿基诺國際機場](../Page/尼诺伊·阿基诺國際機場.md "wikilink")
  - [印度尼西亞](../Page/印度尼西亞.md "wikilink")
      - [雅加達](../Page/雅加達.md "wikilink")-[蘇加諾-哈達國際機場](../Page/蘇加諾-哈達國際機場.md "wikilink")
      - [泗水](../Page/泗水.md "wikilink")-[朱安達國際機場](../Page/朱安達國際機場.md "wikilink")
      - [登巴薩](../Page/登巴薩.md "wikilink")-[巴厘國際機場](../Page/巴厘國際機場.md "wikilink")
      - [龍目](../Page/龍目.md "wikilink")-[龍目國際機場](../Page/龍目國際機場.md "wikilink")
      - [棉蘭](../Page/棉蘭.md "wikilink")-[棉蘭國際機場](../Page/棉蘭國際機場.md "wikilink")

### 北美洲

  - [美國](../Page/美國.md "wikilink")
      - [夏威夷](../Page/夏威夷.md "wikilink")
          - [檀香山](../Page/檀香山.md "wikilink")-[檀香山國際機場](../Page/檀香山國際機場.md "wikilink")

### 大洋洲

  - [澳洲](../Page/澳洲.md "wikilink")
      - [新南威爾士州](../Page/新南威爾士州.md "wikilink")
          - [巴利納](../Page/巴利納.md "wikilink")
          - [紐卡斯爾](../Page/紐卡斯爾.md "wikilink")（[紐卡斯爾機場](../Page/紐卡斯爾機場.md "wikilink")）
          - [悉尼](../Page/悉尼.md "wikilink")（[金斯福德·史密斯國際機場](../Page/金斯福德·史密斯國際機場.md "wikilink")）
      - [北領地](../Page/北領地.md "wikilink")
          - [達爾文](../Page/達爾文.md "wikilink")（[達爾文國際機場](../Page/達爾文國際機場.md "wikilink")）
      - [昆士蘭州](../Page/昆士蘭州.md "wikilink")
          - [布里斯班](../Page/布里斯班.md "wikilink")（[布里斯本國際機場](../Page/布里斯本國際機場.md "wikilink")）
          - [赫維灣](../Page/赫維灣.md "wikilink")（[赫維灣機場](../Page/赫維灣機場.md "wikilink")）
          - [黃金海岸](../Page/黃金海岸_\(澳洲\).md "wikilink")（[黃金海岸機場](../Page/黃金海岸機場.md "wikilink")）
          - [哈密爾頓島](../Page/哈密爾頓島.md "wikilink")（[大堡礁機場](../Page/大堡礁機場.md "wikilink")）
          - [麥凱](../Page/麥凱.md "wikilink")（[麥凱機場](../Page/麥凱機場.md "wikilink")）
          - [羅克漢普頓](../Page/羅克漢普頓.md "wikilink")（[羅克漢普頓機場](../Page/羅克漢普頓機場.md "wikilink")）
          - [陽光海岸](../Page/陽光海岸_\(澳洲\).md "wikilink")（[陽光海岸機場](../Page/陽光海岸機場.md "wikilink")）
          - [湯斯維爾](../Page/湯斯維爾.md "wikilink")（[湯斯維爾機場](../Page/湯斯維爾機場.md "wikilink")）
          - Proserpine（[聖靈海岸機場](../Page/聖靈海岸機場.md "wikilink")）
      - [南澳大利亞州](../Page/南澳大利亞州.md "wikilink")
          - [阿德萊德](../Page/阿德萊德.md "wikilink")（[阿德萊德國際機場](../Page/阿德萊德國際機場.md "wikilink")）
      - [塔斯曼尼亞](../Page/塔斯曼尼亞.md "wikilink")
          - [霍巴特](../Page/霍巴特.md "wikilink")（[霍巴特國際機場](../Page/霍巴特國際機場.md "wikilink")）
          - [朗塞斯頓](../Page/朗塞斯頓.md "wikilink")（[朗塞斯頓機場](../Page/朗塞斯頓機場.md "wikilink")）
      - [維多利亞州](../Page/維多利亞州.md "wikilink")
          - [墨爾本](../Page/墨爾本.md "wikilink")
              - [愛華隆機場](../Page/愛華隆機場_\(澳洲\).md "wikilink")
              - [墨爾本國際機場](../Page/墨爾本國際機場.md "wikilink") - **樞紐機場**
      - [西澳大利亞州](../Page/西澳大利亞州.md "wikilink")
          - [珀斯](../Page/珀斯.md "wikilink")（[珀斯機場](../Page/珀斯機場.md "wikilink")）
  - [紐西蘭](../Page/紐西蘭.md "wikilink")
      - [北島](../Page/北島_\(紐西蘭\).md "wikilink")
          - [奧克蘭](../Page/奧克蘭.md "wikilink")（[奧克蘭機場](../Page/奧克蘭機場.md "wikilink")）
          - [威靈頓](../Page/威靈頓.md "wikilink")（[威靈頓國際機場](../Page/威靈頓國際機場.md "wikilink")）
      - [南島](../Page/南島.md "wikilink")
          - [基督城](../Page/基督城.md "wikilink")（[基督城國際機場](../Page/基督城國際機場.md "wikilink")）
          - [皇后鎮](../Page/皇后鎮.md "wikilink")（[皇后鎮國際機場](../Page/皇后鎮國際機場.md "wikilink")）

### 已取消航線

  - [澳洲](../Page/澳洲.md "wikilink")
      - [科夫斯海岸](../Page/科夫斯海岸.md "wikilink")（[科夫斯海岸機場](../Page/科夫斯海岸機場.md "wikilink")）

## 參考資料

## 外部連結

  - [官方網站](https://www.jetstar.com)

[\*](../Category/捷星航空.md "wikilink")
[Category:2003年成立的航空公司](../Category/2003年成立的航空公司.md "wikilink")
[Category:低成本航空公司](../Category/低成本航空公司.md "wikilink")
[Category:2003年澳大利亞建立](../Category/2003年澳大利亞建立.md "wikilink")

1.
2.