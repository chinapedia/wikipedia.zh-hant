**鳳凰衛視資訊台**於2001年1月1日正式开播，是[鳳凰衛視全日](../Page/鳳凰衛視.md "wikilink")24小時播放來自全球各地時事新聞與財經資訊的频道。重點在兩岸以至全球[華人地區的新聞資訊報道及评论](../Page/華人地區.md "wikilink")，2003年在中國大陸落地。而广东有线仅有中文台，没有资讯台。

鳳凰衛視資訊台於2013年开播高清版，此时标清版为切边播出。2014年起开始标清16:9播出。

另外，凤凰卫视资讯台会在每晚20点档《凤凰正点播报》开始前播放国歌，播放版本与香港电视台主流频道一致。

## 覆盖

鳳凰衛視資訊台於[亞洲3號S衛星通過](../Page/亚洲3号卫星.md "wikilink")[C波段向整個](../Page/C波段.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")、[澳洲及](../Page/澳洲.md "wikilink")[非洲北部廣播](../Page/非洲北部.md "wikilink")\[1\]\[2\]，以數碼制式的[DVB-S調制及](../Page/DVB-S.md "wikilink")[MPEG-2編碼](../Page/MPEG-2.md "wikilink")，香港部分大廈使用衛星電視共用天線系統將訊號轉為[PAL制式模擬信號供住戶收看](../Page/PAL制式.md "wikilink")。\[3\]

[中国国际电视总公司境外卫星代理部接收凤凰卫视资讯台信号](../Page/中国国际电视总公司.md "wikilink")，通过亚太6号卫星（东经134度）发射KU波段信号。该服务一般提供给中國境內三星级或以上的涉外宾馆酒店、外国人居住区、领事馆及大使馆。部分中國城市更可以在有线电视上免费或额外付费观看。

在北美地区收费电视平台，亦会看到当地电视转播机构屏蔽原有广告并插播当地的中文商业广告。

## 節目

### 新聞播報和天氣報告

  - 《[周末子夜播報](../Page/周末子夜播報.md "wikilink")》（Weekend Midnight
    Express）：周一和周日00:00-00:30。
  - 《[凤凰子夜快车](../Page/凤凰子夜快车.md "wikilink")》（Phoenix Midnight
    Express）：周二至周六00:00-00:20。
  - 《[凤凰早班车](../Page/凤凰早班车.md "wikilink")》（Phoenix Morning
    Express）：平日07:00-07:30、周二至周五07:30-08:00。
  - 《[周末晨早播报](../Page/周末晨早播报.md "wikilink")》（Weekend Morning
    News）：周末07:00-07:30首播及07:30-08:00重播。
  - 《[凤凰午间特快](../Page/凤凰午间特快.md "wikilink")》（The Midday
    Express）：平日12:00-12:25。
  - 《[周末正午播报](../Page/周末正午播报.md "wikilink")》（Weekend Midday
    News）：周末12:00-12:25。
  - 《[凤凰正点播报](../Page/凤凰正点播报.md "wikilink")》（News On The
    Hour）：每日01:00-01:30、05:00-05:30、10:00-10:30；
    星期一03:00-03:30、09:00-09:15、11:00-11:25、13:00-13:15、14:00-14:55、15:00-16:45、17:00-17:30；
    周二至周五03:00-03:30、09:00-09:30、11:00-11:50、13:00-13:15、14:00-14:55、15:00-16:45、17:00-17:30；
    星期六09:00-09:30、11:00-11:45、13:00-13:15、14:00-14:30、15:00-15:30、17:00-17:15、23:00-23:15；
    星期日09:00-09:30、11:00-11:30、13:00-13:30、14:00-14:30、15:00-15:30、17:00-17:15、23:00-23:15。
  - 《[时事直通车](../Page/时事直通车.md "wikilink")》（Phoenix Evening
    Express）：平日21:00-21:58、周末21:00-21:40。
  - 《[鳳凰焦點新聞](../Page/鳳凰焦點新聞.md "wikilink")》（Phoenix
    Focus）：每日18:00-18:30、20:00-20:30、平日23:00-23:15、周末22:25-22:29。
  - 《[凤凰气象站](../Page/凤凰气象站.md "wikilink")》（Phoenix Weather
    Report）：在《凤凰早班车》最后、《周末晨早播报》7:00首播时段之后、大部分时段《凤凰正点播报》及《凤凰焦点新闻》之后播出无主持版本，除此以外在《周末晨早播报》7:30重播时段之后、《凤凰午间特快》之后、《华闻大直播》之后及《时事直通车》最后另有播出主持版本。
  - 《[海洋預報](../Page/海洋預報.md "wikilink")》（Ocean Forecast）：每日14:29及22:29。
  - 《[新聞看板](../Page/新聞看板.md "wikilink")》（News
    Bulletin）：平日11:30、14:30、16:30、18:30、20:30、21:58、22:30，每节时长1分30秒。
  - 《[突发事件直播](../Page/突发事件直播.md "wikilink")》（Breaking News）：在突发事件时段播出。
  - 《[焦点直播](../Page/焦点直播.md "wikilink")》（Live Focus）：在焦点事件时段播出。

### 財經

  - 《[子夜財經](../Page/子夜財經.md "wikilink")》（Midnight Finance
    Express）：作为《凤凰子夜快车》的一个环节播出。
  - 《[晨早財經](../Page/晨早財經.md "wikilink")》（Morning Financial
    News）：作为《凤凰早班车》的一个环节播出。
  - 《[午间財經](../Page/午间財經.md "wikilink")》（Noontime Financial
    News）：作为《凤凰午间特快》的一个环节播出。
  - 《[金石财经](../Page/金石财经.md "wikilink")》（Financial
    Journal）：首播：平日17:30-18:00。重播：周二至周六01:30-02:00及09:30-10:00。
  - 《[凤凰财经日报](../Page/凤凰财经日报.md "wikilink")》（Phoenix Financial Daily
    Report）：首播：平日20:31-21:00。重播：周二至周六02:30-03:00。
  - 《[财智菁英汇](../Page/财智菁英汇.md "wikilink")》（Elite
    Converge）：首播：星期六12:30-13:00。重播：星期六20:30-21:00及星期日00:30-01:00。
  - 《[中国深度财经](../Page/中国深度财经.md "wikilink")》（China Financial
    Intelligence）：首播：星期六21:40-22:25。重播：星期日17:15-18:00及星期一08:15-09:00。
  - 《[大政商道](../Page/大政商道.md "wikilink")》（Approaches to Politics &
    Business）：首播：星期日12:30-13:00。重播：星期日20:30-21:00及星期一00:30-01:00。

### 评论

  - 《[时事辩论会](../Page/时事辩论会.md "wikilink")》（Current Affairs
    Debate）：首播：平日12:30-13:00。重播：周二至周六00:30-01:00及06:30-07:00。
  - 《[新聞朋友圈](../Page/新聞朋友圈.md "wikilink")》（The News
    Circle）：首播：平日16:45-17:00。重播：平日19:45-20:00、周二至周六08:45-09:00、星期一02:15-02:30及04:45-05:00（重播上周四节目）、星期六19:15-19:30（重播周四节目）、周二至周日04:00-04:15（周日重播周四节目）。
  - 《[新闻今日谈](../Page/新闻今日谈.md "wikilink")》（News
    Talk）：首播：每日18:30-19:00。重播：每日22:30-23:00及05:30-06:00。
  - 《[华闻大直播](../Page/华闻大直播.md "wikilink")》（China News
    Live）：首播：平日19:00-19:45、周末19:00-19:15。重播：周二至周六08:00-08:45、周日至周一08:00-08:15。
  - 《[总编辑时间](../Page/总编辑时间.md "wikilink")》（Time of
    Chief-Editor）：首播：平日22:00-22:30。重播：周二至周六03:30-04:00及10:30-11:00。
  - 《[天下被网罗](../Page/天下被网罗.md "wikilink")》（Global
    Online）：首播：平日23:15-00:00。重播：周二至周六04:15-05:00及13:15-14:00。
  - 《[周末龙门阵](../Page/周末龙门阵.md "wikilink")》（Weekend Hot
    Talk）：首播：星期六17:15-18:00。重播：星期日21:40-22:25、星期一04:00-04:45及13:15-14:00。
  - 《[台湾一周重点](../Page/台湾一周重点.md "wikilink")》（Taiwan Weekly
    Focus）：首播：星期六23:15-00:00。重播：星期日04:15-05:00及08:15-09:00。
  - 《[问答神州](../Page/问答神州.md "wikilink")》（Mainland Q &
    A）：首播：星期日16:30-17:00。重播：星期日23:30-00:00及星期一11:30-12:00。
  - 《[大新闻大历史](../Page/大新闻大历史.md "wikilink")》（News
    Time）：首播：星期日19:15-20:00。重播：星期一01:30-02:15及09:15-10:00。

### 时事专题

  - 《[记者再报告](../Page/记者再报告.md "wikilink")》（Journalist on the
    Spot）：首播：星期六15:30-16:00。重播：星期日06:30-07:00、10:30-11:00。
  - 《[香港新視點](../Page/香港新視點.md "wikilink")》（Hong Kong
    Perspectives）：首播：星期六16:30-17:00。重播：星期日02:30-03:00、11:30-12:00及14:30-15:00。

### 其他节目

  - 《[领航者](../Page/领航者.md "wikilink")》：首播：星期一10:30-11:00；重播：星期日15:30-16:00。
  - 《[投资收藏](../Page/投资收藏.md "wikilink")》：首播：星期六19:30-20:00。重播：星期日01:30-02:00及09:30-10:00。
  - 《[筑梦天下](../Page/筑梦天下.md "wikilink")》（Architecture's
    Dream）：星期日13:30-14:00。
  - 《[英闻解码](../Page/英闻解码.md "wikilink")》（Decoding News
    Terms）：首播：星期一11:25-11:30，星期二至星期五11:50-11:55；重播：星期一至星期五14:55-14:58、20:26-20:28，星期二至星期六3:27-3:30，星期六11:45-12:00（精编合成版），星期日23:15-23:30（精编合成版）。

## 广播时间表

| 时间    | 星期一                                         | 周二至周四                                       | 星期五                                         | 星期六 | 星期日 |
| ----- | ------------------------------------------- | ------------------------------------------- | ------------------------------------------- | --- | --- |
| 00:00 | [周末子夜播報](../Page/周末子夜播報.md "wikilink")      | [凤凰子夜快车](../Page/凤凰子夜快车.md "wikilink")      | [周末子夜播報](../Page/周末子夜播報.md "wikilink")      |     |     |
| 00:30 | [大政商道](../Page/大政商道.md "wikilink")          | [时事辩论会](../Page/时事辩论会.md "wikilink")        | [财智菁英汇](../Page/财智菁英汇.md "wikilink")        |     |     |
| 01:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0100 |                                             |                                             |     |     |
| 01:30 | [投资收藏](../Page/投资收藏.md "wikilink")          | [金石财经](../Page/金石财经.md "wikilink")          | [大新闻大历史](../Page/大新闻大历史.md "wikilink")      |     |     |
| 02:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0100 |                                             |                                             |     |     |
| 02:15 | [新聞朋友圈](../Page/新聞朋友圈.md "wikilink")（星期四）   |                                             |                                             |     |     |
| 02:30 | [香港新視點](../Page/香港新視點.md "wikilink")        | [凤凰财经日报](../Page/凤凰财经日报.md "wikilink")      | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0100 |     |     |
| 03:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0300 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0100 |                                             |     |     |
| 03:30 | [记者再报告](../Page/记者再报告.md "wikilink")        | [总编辑时间](../Page/总编辑时间.md "wikilink")        | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0100 |     |     |
| 04:00 | [周末龙门阵](../Page/周末龙门阵.md "wikilink")        | [新聞朋友圈](../Page/新聞朋友圈.md "wikilink")        | [新聞朋友圈](../Page/新聞朋友圈.md "wikilink")（星期四）   |     |     |
| 04:15 | [天下被网罗](../Page/天下被网罗.md "wikilink")        | [台湾一周重点](../Page/台湾一周重点.md "wikilink")      |                                             |     |     |
| 04:45 | [新聞朋友圈](../Page/新聞朋友圈.md "wikilink")（星期四）   |                                             |                                             |     |     |
| 05:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0500 |                                             |                                             |     |     |
| 05:30 | [新闻今日谈](../Page/新闻今日谈.md "wikilink")        |                                             |                                             |     |     |
| 06:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0500 |                                             |                                             |     |     |
| 06:30 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0500 | [时事辩论会](../Page/时事辩论会.md "wikilink")        | [记者再报告](../Page/记者再报告.md "wikilink")        |     |     |
| 07:00 | [凤凰早班车](../Page/凤凰早班车.md "wikilink")        | [周末晨早播报](../Page/周末晨早播报.md "wikilink")      |                                             |     |     |
| 07:30 | [周末晨早播报](../Page/周末晨早播报.md "wikilink")      |                                             |                                             |     |     |
| 08:00 | [华闻大直播](../Page/华闻大直播.md "wikilink")        | [华闻大直播](../Page/华闻大直播.md "wikilink")        | [华闻大直播](../Page/华闻大直播.md "wikilink")        |     |     |
| 08:15 | [中国深度财经](../Page/中国深度财经.md "wikilink")      | [台湾一周重点](../Page/台湾一周重点.md "wikilink")      |                                             |     |     |
| 08:45 | [新聞朋友圈](../Page/新聞朋友圈.md "wikilink")        |                                             |                                             |     |     |
| 09:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0900 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 0900 |                                             |     |     |
| 09:15 | [大新闻大历史](../Page/大新闻大历史.md "wikilink")      |                                             |                                             |     |     |
| 09:30 | [金石财经](../Page/金石财经.md "wikilink")          | [投资收藏](../Page/投资收藏.md "wikilink")          |                                             |     |     |
| 10:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1000 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1000 |                                             |     |     |
| 10:30 | [领航者](../Page/领航者.md "wikilink")            | [总编辑时间](../Page/总编辑时间.md "wikilink")        | [记者再报告](../Page/记者再报告.md "wikilink")        |     |     |
| 11:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1100 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1100 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1100 |     |     |
| 11:30 | [问答神州](../Page/问答神州.md "wikilink")          | [香港新視點](../Page/香港新視點.md "wikilink")        |                                             |     |     |
| 12:00 | [凤凰午间特快](../Page/凤凰午间特快.md "wikilink")      | [周末正午播报](../Page/周末正午播报.md "wikilink")      |                                             |     |     |
| 12:30 | 一带一路大畅想                                     | [财智菁英汇](../Page/财智菁英汇.md "wikilink")        | [大政商道](../Page/大政商道.md "wikilink")          |     |     |
| 13:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1300 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1300 |                                             |     |     |
| 13:15 | [周末龙门阵](../Page/周末龙门阵.md "wikilink")        | [天下被网罗](../Page/天下被网罗.md "wikilink")        |                                             |     |     |
| 13:30 | [筑梦天下](../Page/筑梦天下.md "wikilink")          |                                             |                                             |     |     |
| 14:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1400 |                                             |                                             |     |     |
| 14:29 | [海洋預報](../Page/海洋預報.md "wikilink")          |                                             |                                             |     |     |
| 14:30 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1400 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1400 | [香港新視點](../Page/香港新視點.md "wikilink")        |     |     |
| 15:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1500 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1500 |                                             |     |     |
| 15:30 | [记者再报告](../Page/记者再报告.md "wikilink")        | [领航者](../Page/领航者.md "wikilink")            |                                             |     |     |
| 16:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1600 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1500 |                                             |     |     |
| 16:30 | [香港新視點](../Page/香港新視點.md "wikilink")        | [问答神州](../Page/问答神州.md "wikilink")          |                                             |     |     |
| 16:45 | [新聞朋友圈](../Page/新聞朋友圈.md "wikilink")        |                                             |                                             |     |     |
| 17:00 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1700 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 1700 |                                             |     |     |
| 17:15 | [周末龙门阵](../Page/周末龙门阵.md "wikilink")        | [中国深度财经](../Page/中国深度财经.md "wikilink")      |                                             |     |     |
| 17:30 | [金石财经](../Page/金石财经.md "wikilink")          |                                             |                                             |     |     |
| 18:00 | [鳳凰焦點新聞](../Page/鳳凰焦點新聞.md "wikilink")      |                                             |                                             |     |     |
| 18:30 | [新闻今日谈](../Page/新闻今日谈.md "wikilink")        |                                             |                                             |     |     |
| 19:00 | [华闻大直播](../Page/华闻大直播.md "wikilink")        | [华闻大直播](../Page/华闻大直播.md "wikilink")        |                                             |     |     |
| 19:15 | [新聞朋友圈](../Page/新聞朋友圈.md "wikilink")（星期四）   | [大新闻大历史](../Page/大新闻大历史.md "wikilink")      |                                             |     |     |
| 19:30 | [投资收藏](../Page/投资收藏.md "wikilink")          |                                             |                                             |     |     |
| 19:45 | [新聞朋友圈](../Page/新聞朋友圈.md "wikilink")        |                                             |                                             |     |     |
| 20:00 | [鳳凰焦點新聞](../Page/鳳凰焦點新聞.md "wikilink")      |                                             |                                             |     |     |
| 20:30 | [凤凰财经日报](../Page/凤凰财经日报.md "wikilink")      | [财智菁英汇](../Page/财智菁英汇.md "wikilink")        | [大政商道](../Page/大政商道.md "wikilink")          |     |     |
| 21:00 | [时事直通车](../Page/时事直通车.md "wikilink")        | [时事直通车](../Page/时事直通车.md "wikilink")        |                                             |     |     |
| 21:40 | [中国深度财经](../Page/中国深度财经.md "wikilink")      | [周末龙门阵](../Page/周末龙门阵.md "wikilink")        |                                             |     |     |
| 22:00 | [总编辑时间](../Page/总编辑时间.md "wikilink")        |                                             |                                             |     |     |
| 22:25 | [鳳凰焦點新聞](../Page/鳳凰焦點新聞.md "wikilink")      |                                             |                                             |     |     |
| 22:29 | [海洋預報](../Page/海洋預報.md "wikilink")          |                                             |                                             |     |     |
| 22:30 | [新闻今日谈](../Page/新闻今日谈.md "wikilink")        |                                             |                                             |     |     |
| 23:00 | [鳳凰焦點新聞](../Page/鳳凰焦點新聞.md "wikilink")      | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 2300 | [凤凰正点播报](../Page/凤凰正点播报.md "wikilink") 2300 |     |     |
| 23:15 | [天下被网罗](../Page/天下被网罗.md "wikilink")        | [台湾一周重点](../Page/台湾一周重点.md "wikilink")      |                                             |     |     |
| 23:30 | [问答神州](../Page/问答神州.md "wikilink")          |                                             |                                             |     |     |

<table>
<tbody>
<tr class="odd">
<td><p>：直播/首播节目</p></td>
<td><p>：重播（录播）节目</p></td>
<td></td>
</tr>
</tbody>
</table>

## 記者（中文台／資訊台）

  - [胡小童](../Page/胡小童.md "wikilink")
  - [梁華](../Page/梁華.md "wikilink")
  - [全荃](../Page/全荃.md "wikilink")（兼大夜班主播）
  - [王春雨](../Page/王春雨.md "wikilink")（兼大夜班主播；前[鳳凰URadio主持](../Page/鳳凰URadio.md "wikilink")）
  - [刘睿](../Page/刘睿.md "wikilink")（兼大夜班主播）
  - [黃芷淵](../Page/黃芷淵.md "wikilink")
  - [魏穎茵](../Page/魏穎茵.md "wikilink")
  - [潘楚源](../Page/潘楚源.md "wikilink")
  - [潘玥](../Page/潘玥.md "wikilink")
  - [金小菲](../Page/金小菲.md "wikilink")
  - [韓盛偉](../Page/韓盛偉.md "wikilink")（前[鳳凰URadio主持](../Page/鳳凰URadio.md "wikilink")）
  - [羅羽鳴](../Page/羅羽鳴.md "wikilink")（中國組駐廣州記者）
  - [張慧](../Page/張慧.md "wikilink")（中國組駐廣州記者）
  - [馬騁](../Page/馬騁.md "wikilink")（中國組駐廣州記者）
  - [陳鉞](../Page/陳鉞.md "wikilink")（中國組駐廣州記者）

註：部分記者會兼以粵語於[香港台作報導](../Page/鳳凰衛視香港台.md "wikilink")。

## 外部連結

  - [鳳凰衛視官方网站](http://phtv.ifeng.com/)

## 參考來源

[Category:2001年成立的电视台或电视频道](../Category/2001年成立的电视台或电视频道.md "wikilink")
[頻](../Category/鳳凰衛視.md "wikilink")
[Category:广东省可收视境外电视频道](../Category/广东省可收视境外电视频道.md "wikilink")
[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:新加坡電視播放頻道](../Category/新加坡電視播放頻道.md "wikilink")
[Category:华语电视频道](../Category/华语电视频道.md "wikilink")

1.
2.  [亞洲3號S衛星頻道表](http://www.asiasat.com/asiasat/contentView.php?section=69&lang=1)

3.  [在香港可經衛星電視共用天線系統接收的衛星電視頻道](http://www.ofca.gov.hk/filemanager/ofca/tc/content_295/st_smatv.pdf)