**梅江区**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[梅州市中部的一个市辖区](../Page/梅州市.md "wikilink")，位于[广东省东北部](../Page/广东省.md "wikilink")，是梅州市党政机关所在地和经济、文化和交通中心，与[梅县区并称](../Page/梅县区.md "wikilink")"梅城"。东边、北边和西边与梅州市[梅县区相邻](../Page/梅县区.md "wikilink")，南边与梅州市[大埔县](../Page/大埔县.md "wikilink")、[丰顺县相邻](../Page/丰顺县.md "wikilink")。总面积为570.9平方公里。2012年户籍人口为41万。区府驻金山街道。

## 行政区划历史

1988年1月广东实行市管县体制后，梅江区成为[梅州市辖区](../Page/梅州市.md "wikilink")。

2012年12月辖

  - 3个街道：[金山](../Page/金山街道_\(梅州市\).md "wikilink")、[江南](../Page/江南街道_\(梅州市\).md "wikilink")、[西郊](../Page/西郊街道.md "wikilink")
  - 4个镇：[长沙镇](../Page/长沙镇.md "wikilink")、[三角镇](../Page/三角镇_\(梅州市\).md "wikilink")、[城北镇](../Page/城北镇.md "wikilink")、[西阳镇](../Page/西阳镇.md "wikilink")

## 人口

### 民族

梅江区以[客家人为主](../Page/客家人.md "wikilink")。

### 语言

梅江区大部分居民讲通用梅州市区（梅江区、[梅县区](../Page/梅县区.md "wikilink")）的[客家语](../Page/客家语.md "wikilink")[梅州话](../Page/梅州话.md "wikilink")。

### 经济

[从梅江区泮坑度假村远望的梅江区农村景象和高速公路.jpg](https://zh.wikipedia.org/wiki/File:从梅江区泮坑度假村远望的梅江区农村景象和高速公路.jpg "fig:从梅江区泮坑度假村远望的梅江区农村景象和高速公路.jpg")
梅州市梅江区2004年[国内生产总值为](../Page/国内生产总值.md "wikilink")48.8148亿元，[商业](../Page/商业.md "wikilink")、[餐饮业](../Page/餐饮业.md "wikilink")、[城郊农业和](../Page/农业.md "wikilink")[旅游业都发展很快](../Page/旅游业.md "wikilink")。区内矿藏资源丰富，以[煤炭](../Page/煤炭.md "wikilink")、[石灰石](../Page/石灰石.md "wikilink")、[铅锌矿](../Page/铅锌矿.md "wikilink")、[钨矿和](../Page/钨矿.md "wikilink")[花岗岩为主](../Page/花岗岩.md "wikilink")。

### 文化

  - 梅江区是梅州市“客家之都”的中心地带，被誉为“文化之乡”，“足球之乡”，“华侨之乡”。
  - 梅江区内有一所本科院校——[嘉应学院](../Page/嘉应学院.md "wikilink")，还有多间中等专科学院。

## 景点

梅州市梅江区名胜古迹众多，出名的景点有客天下、[黄遵宪故居](../Page/黄遵宪.md "wikilink")，[归读公园](../Page/围龙屋.md "wikilink")，泮坑公园、[千佛塔](../Page/千佛塔_\(梅州\).md "wikilink")、百岁山、剑英公园、文化公园、马鞍山公园、芹洋半岛湿地公园等。

## 交通

梅江区交通便利，[205国道](../Page/205国道.md "wikilink")，[206国道](../Page/206国道.md "wikilink")，[广梅汕铁路](../Page/广梅汕铁路.md "wikilink")，[梅坎铁路](../Page/梅坎铁路.md "wikilink")，[梅汕高铁和](../Page/梅汕高铁.md "wikilink")[梅河](../Page/梅河高速公路.md "wikilink")、[梅州环城](../Page/梅州环城高速公路.md "wikilink")、[梅龙](../Page/梅龙高速公路.md "wikilink")、梅平、[梅汕高速公路均贯穿全区](../Page/梅汕高速公路.md "wikilink")。[梅县机场亦位于梅江区境内](../Page/梅县长岗岌机场.md "wikilink")。

## 外部链接

  - [梅州市梅江区信息网](https://web.archive.org/web/20090901203720/http://www.meijiang.gov.cn/)

[Category:梅州](../Category/梅州.md "wikilink")
[Category:梅江区](../Category/梅江区.md "wikilink")
[区](../Category/梅州区县市.md "wikilink")
[梅州](../Category/广东市辖区.md "wikilink")