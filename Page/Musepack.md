**Musepack**（早前称作MPEGplus、MPEG+或MP+）是一种基于[MP2算法的有损压缩音频格式](../Page/MPEG-1_Audio_Layer_II.md "wikilink")。它的编码方式着重听觉上的[穿透感](../Page/穿透感（编码）.md "wikilink")，在160[kbit/s或以上的表现尤为出色](../Page/比特率.md "wikilink")。

Musepack最初由[Andree
Buschmann提出和开发](../Page/Andree_Buschmann.md "wikilink")，其后经[Frank
Klemm接手](../Page/Frank_Klemm.md "wikilink")，如今在Frank
Klemm的帮助下由Musepack开发团队（Musepack Development
Team，MDT）维护。在[微軟視窗](../Page/微軟視窗.md "wikilink")、[Linux和](../Page/Linux.md "wikilink")[Mac
OS
X等平臺上](../Page/Mac_OS_X.md "wikilink")，在Musepack的官方網站上，除了有Musepack的編碼器和解碼器，還有為數款[媒體播放器專用的第三方插件](../Page/媒體播放器.md "wikilink")，均以[LGPL或](../Page/LGPL.md "wikilink")[BSD許可證发布](../Page/BSD許可證.md "wikilink")。

## 技術明細

Musepack由[MP2算法發展而來](../Page/MPEG-1_Audio_Layer_II.md "wikilink")，它的主要特點包括：

  - 選擇性子帶（subband）M/S編碼（如[AAC](../Page/AAC.md "wikilink")）
  - 比起[MP3](../Page/MP3.md "wikilink")／[AAC更為高效的](../Page/AAC.md "wikilink")[哈夫曼编码](../Page/哈夫曼树.md "wikilink")
  - 噪音替換技術（如ATSC A-52和[MPEG-4](../Page/MPEG-4.md "wikilink") AAC V2）
  - 3kbit/s到1300kbit/s的純變碼率編碼

Musepack采用[APEv2標簽](../Page/APEv2_tag.md "wikilink")。

儘管Musepack對聽覺上穿透感的優化預設在175-185kbit/s的編碼率內，許多的測試表明，Musepack在預設和較低（比如128kbit/s）的編碼率上，均有非常出色的表現。

## 外部連結

  - [官方網站](http://www.musepack.net)

[Category:數位音訊](../Category/數位音訊.md "wikilink")
[Category:文件格式](../Category/文件格式.md "wikilink")
[Category:音频格式](../Category/音频格式.md "wikilink")
[Category:音频编解码器](../Category/音频编解码器.md "wikilink")
[Category:有损压缩算法](../Category/有损压缩算法.md "wikilink")