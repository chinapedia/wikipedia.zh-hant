《**新科學人**》（也作《**新科學家**》）（），創刊於1956年，由Reed Business Information
Ltd.出版發行的國際性科學雜誌。每週發刊一次。並於1996年設立網路版，每日發佈科學新聞。

它並不是[同行評審的科學期刊](../Page/同行評審.md "wikilink")，不過仍廣為科學和非科學領域的人士閱讀，以接軌非專門或有興趣領域的最新發展。

## 參見

  - [PLOS ONE](../Page/PLOS_ONE.md "wikilink")

## 註釋

## 外部連結

  - [New Scientist](http://www.newscientist.com/home.ns)
      - [宇宙專題](http://www.newscientistspace.com)
      - [科技專題](http://www.newscientisttech.com/home.ns)
      - [每周播客](http://www.newscientist.com/podcast.ns)

[Category:科普雜誌](../Category/科普雜誌.md "wikilink")