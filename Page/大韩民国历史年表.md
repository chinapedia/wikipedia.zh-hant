**大韩民国歷史年表**

  - [1910年](../Page/1910年.md "wikilink")8月：日本政府與朝鮮政府簽定《[日韓合併條約](../Page/日韓合併條約.md "wikilink")》，朝鮮併入日本
  - [1919年](../Page/1919年.md "wikilink")[3月1日](../Page/3月1日.md "wikilink")：[三一运动爆发](../Page/三一运动.md "wikilink")
  - 同年：朝鲜半岛独立运动领导人先后在[海参崴](../Page/海参崴.md "wikilink")、[上海](../Page/上海.md "wikilink")、[汉城成立临时政府](../Page/汉城.md "wikilink")
  - [1941年](../Page/1941年.md "wikilink")[12月9日](../Page/12月9日.md "wikilink")：[大韩民国临时政府向日本宣战](../Page/大韩民国临时政府.md "wikilink")
  - [1948年](../Page/1948年.md "wikilink")[8月15日](../Page/8月15日.md "wikilink")：[大韓民國成立](../Page/大韓民國.md "wikilink")，[李承晚为第一任总统](../Page/李承晚.md "wikilink")
  - [1950年](../Page/1950年.md "wikilink")[6月25日](../Page/6月25日.md "wikilink")：[朝鲜战争爆发](../Page/朝鲜战争.md "wikilink")
  - [1953年](../Page/1953年.md "wikilink")[6月27日](../Page/6月27日.md "wikilink")：南北双方签订停火协议
  - [1960年](../Page/1960年.md "wikilink")4月：[四一九學運](../Page/四一九學運.md "wikilink")，李承晚下台
  - [1961年](../Page/1961年.md "wikilink")[5月16日](../Page/5月16日.md "wikilink")：[朴正熙将军发动](../Page/朴正熙.md "wikilink")[五一六軍事政變](../Page/五一六軍事政變.md "wikilink")，军事政权成立
  - 1964年：[派兵前往越南](../Page/韓國出兵越南.md "wikilink")
  - 1965年6月：與[日本建交](../Page/日本.md "wikilink")
  - 1972年11月：维新独裁體制成立　
  - [1973年](../Page/1973年.md "wikilink")[8月8日](../Page/8月8日.md "wikilink")：[金大中事件](../Page/金大中事件.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")[8月15日](../Page/8月15日.md "wikilink")：[朴正熙夫人陆英修被杀](../Page/文世光事件.md "wikilink")，韓國首條[地鐵線通車](../Page/首都圈電鐵.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")[10月26日](../Page/10月26日.md "wikilink")：朴正熙遇刺身亡
  - [1979年](../Page/1979年.md "wikilink")[12月12日](../Page/12月12日.md "wikilink")：[全斗焕将军发动](../Page/全斗焕.md "wikilink")[肃军政变](../Page/肃军政变.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")[5月18日](../Page/5月18日.md "wikilink")：[光州事件](../Page/光州事件.md "wikilink")
  - 1980年：8月：全斗焕将军成为总统
  - [1983年](../Page/1983年.md "wikilink")[9月1日](../Page/9月1日.md "wikilink")：[苏联国土防空军击落](../Page/苏联国土防空军.md "wikilink")[大韓航空007號班機](../Page/大韓航空007號班機空難.md "wikilink")
  - 1984年：5月：[若望·保祿二世访问南韓](../Page/若望·保祿二世.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")[6月18日](../Page/6月18日.md "wikilink")：发生[六月抗争](../Page/六月抗争.md "wikilink")
  - 1987年[11月29日](../Page/11月29日.md "wikilink")：[朝鲜间谍爆破](../Page/朝鲜.md "wikilink")[大韩航空858号班机](../Page/大韩航空858号班机空难.md "wikilink")
  - 1987年12月：[卢泰愚当选为总统](../Page/卢泰愚.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")[9月17日](../Page/9月17日.md "wikilink")：[汉城奥运会](../Page/1988年夏季奥林匹克运动会.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")[9月30日](../Page/9月30日.md "wikilink")：与[苏联建交](../Page/苏联.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")9月17日：韓國和朝鲜民主主義人民共和國同时加入[联合国](../Page/联合国.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")[8月24日](../Page/8月24日.md "wikilink")：與[中华人民共和国建交](../Page/中华人民共和国.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：2月：[金泳三就任总统](../Page/金泳三.md "wikilink")，开始韩国民主化
  - [1995年](../Page/1995年.md "wikilink")[6月29日](../Page/6月29日.md "wikilink")，[汉城](../Page/汉城.md "wikilink")[三丰百货店倒塌事件](../Page/三丰百货店.md "wikilink")
  - 1995年11月：[卢泰愚被捕](../Page/卢泰愚.md "wikilink")
  - 1995年12月：[全斗焕被捕](../Page/全斗焕.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")7月：[亚洲金融危机发生](../Page/亚洲金融危机.md "wikilink")
  - [1998年](../Page/1998年.md "wikilink")2月：[金大中就任总统](../Page/金大中.md "wikilink")
  - [2000年](../Page/2000年.md "wikilink")6月：金大中访问[平壤](../Page/平壤.md "wikilink")，发表南北共同声明
  - 2000年10月：金大中獲得[诺贝尔和平奖](../Page/诺贝尔和平奖.md "wikilink")
  - [2002年](../Page/2002年.md "wikilink")2月：金大中因兒子貪污退出政黨
  - 2002年7月：世界盃足球韓國進入四強
  - [2003年](../Page/2003年.md "wikilink")3月：[盧武鉉就任总统](../Page/盧武鉉.md "wikilink")
  - [2004年](../Page/2004年.md "wikilink")8月：派兵前往[伊拉克](../Page/伊拉克.md "wikilink")
  - [2005年](../Page/2005年.md "wikilink")，首都汉城的中文名称更改为首尔

<!-- end list -->

  - [2008年](../Page/2008年.md "wikilink")[2月11日](../Page/2月11日.md "wikilink")：韩国第一号国宝[崇礼门火灾被毁](../Page/崇礼门.md "wikilink")
  - 2008年2月25日：[李明博就任第十七届总统](../Page/李明博.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")[5月23日](../Page/5月23日.md "wikilink")：韩国前总统卢武铉跳崖自杀亡。

<!-- end list -->

  - [2013年](../Page/2013年.md "wikilink")[2月25日](../Page/2月25日.md "wikilink")，韩国前总统朴正熙长女[朴槿惠就任第](../Page/朴槿惠.md "wikilink")18任总统。

[Category:各国年表](../Category/各国年表.md "wikilink")
[Category:韓國歷史](../Category/韓國歷史.md "wikilink")
[Category:韓國相關列表](../Category/韓國相關列表.md "wikilink")