**陈中伟**（），[浙江](../Page/浙江.md "wikilink")[宁波人](../Page/宁波.md "wikilink")。[中国科学院院士](../Page/中国科学院院士.md "wikilink")、[第三世界科学院院士](../Page/第三世界科学院.md "wikilink")。骨科专家，有“断肢再植之父”之称。

## 生平

陈中伟1954年毕业于[上海第二医学院医疗系](../Page/上海第二医学院.md "wikilink")。大学毕业后于[上海市第六人民医院工作](../Page/上海市第六人民医院.md "wikilink")，1984年起任[上海医科大学教授](../Page/上海医科大学.md "wikilink")、附属中山医院骨科主任。曾先后任国际显微重建外科学会执行委员(1984-88年曾任该学会主席)、国际显微外科学会创始委员、国际外科学会中国委员会理事、卫生部医学科学[显微外科副主任委员](../Page/显微外科.md "wikilink")、卫生部医学科学委员会委员、[中华医学会理事](../Page/中华医学会.md "wikilink")、中华医学会外科学会副主任委员，兼任世界12所著名医科大学[客座教授](../Page/客座教授.md "wikilink")。

2004年3月23日陈中伟先生因不慎坠楼逝世，其骨灰撒于其故乡宁波。\[1\]

## 成就

[1963-11_1963年王存柏右手手术两月后的骨骼动脉成像证明血液流通.jpg](https://zh.wikipedia.org/wiki/File:1963-11_1963年王存柏右手手术两月后的骨骼动脉成像证明血液流通.jpg "fig:1963-11_1963年王存柏右手手术两月后的骨骼动脉成像证明血液流通.jpg")右手手术两月后的骨骼动脉成像证明血液流通\]\]
他在1963年成功进行了世界首例“断肢再植”手术，成为世界上第一例成功接活断肢的医生，其[断肢再植术改写了世界的](../Page/断肢再植术.md "wikilink")[骨外科历史](../Page/骨外科.md "wikilink")。陈中伟主持的主指[移植](../Page/器官移植.md "wikilink")、大块[肌肉](../Page/肌肉.md "wikilink")[游离移植和](../Page/游离移植.md "wikilink")[腓骨移植技术在世界上一直处于领先地位](../Page/腓骨.md "wikilink")。在当时他的断肢再植连同[人工合成胰岛素和一万二千吨水压机成为随后几年](../Page/人工合成胰岛素.md "wikilink")[中国向世界展示自己的三项主要事件](../Page/中国.md "wikilink")。

## 參考資料

[Category:中国医学家](../Category/中国医学家.md "wikilink")
[Category:宁波人](../Category/宁波人.md "wikilink")
[Category:葬于宋庆龄陵园](../Category/葬于宋庆龄陵园.md "wikilink")
[Category:世界科学院院士](../Category/世界科学院院士.md "wikilink")
[Category:骨科](../Category/骨科.md "wikilink")
[Z](../Category/陈姓.md "wikilink")

1.