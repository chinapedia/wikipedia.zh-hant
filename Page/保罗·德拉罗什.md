[DelarocheKingEdward.jpg](https://zh.wikipedia.org/wiki/File:DelarocheKingEdward.jpg "fig:DelarocheKingEdward.jpg")和他的兄弟\]\]
**H·保罗·德拉罗什**（，）是[法国著名的](../Page/法国.md "wikilink")[学院派](../Page/学院艺术.md "wikilink")[画家之一](../Page/画家.md "wikilink")。

德拉罗什出身于一个富裕家庭，师从于画家[格罗学画](../Page/安东尼·让·格罗.md "wikilink")，1822年首次在沙龙中展出自己的作品，在当时非常出名，一直从事[美术](../Page/美术.md "wikilink")[教育工作](../Page/教育.md "wikilink")，直到1843年。由于一次事故导致一位学生死亡，被迫关闭了自己的画室。他的学生中包括著名的[现实主义画家](../Page/现实主义.md "wikilink")[米勒](../Page/米勒.md "wikilink")。

德拉罗什的[绘画风格介于](../Page/绘画.md "wikilink")[古典主义画派和](../Page/古典主义画派.md "wikilink")[浪漫主义画派之间](../Page/浪漫主义画派.md "wikilink")，受[大卫](../Page/雅克·路易·大卫.md "wikilink")、[安格尔和](../Page/安格尔.md "wikilink")[德拉克罗瓦的共同影响](../Page/德拉克罗瓦.md "wikilink")。作品造型稳定，立体感强，他在创作构思时，经常先用蜡制作模型研究[构图](../Page/构图.md "wikilink")，他尤其对[历史题材感兴趣](../Page/历史.md "wikilink")，创作了大量的历史题材作品，作品富于戏剧性，表现历史上重要的瞬间。他的作品经常被用[版画复制](../Page/版画.md "wikilink")，作为书籍的插图，或作为单张绘画被收藏。

## 參看

  - [画家列表](../Page/画家列表.md "wikilink")

## 外部链接

[Category:法国画家](../Category/法国画家.md "wikilink")
[Category:學院藝術](../Category/學院藝術.md "wikilink")