**爱尔兰岛**（，[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：Éire），简称**爱尔兰**，是位於[欧洲西北部的](../Page/欧洲.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")，以面積計算是歐洲第二大岛。島上大部分區域屬於[爱尔兰共和国的領土](../Page/爱尔兰共和国.md "wikilink")，但位於島上東北部的[北愛爾蘭地區](../Page/北愛爾蘭.md "wikilink")，其主權屬於[聯合王國](../Page/聯合王國.md "wikilink")。

整個愛爾蘭島的[人口大約為](../Page/人口.md "wikilink")620萬人。其中居住在愛爾蘭共和國的人口有445萬人（包括居住在[都柏林都會區約](../Page/都柏林.md "wikilink")166萬的人口），177萬人居住在北愛爾蘭地區（包括居住在[貝爾法斯特都會區約](../Page/貝爾法斯特.md "wikilink")57萬的人口）。

政治上，愛爾蘭島上分為[愛爾蘭共和國](../Page/愛爾蘭共和國.md "wikilink")，包括島上的六分之五，[北愛爾蘭只涵蓋了其餘在島的東北部的一部分](../Page/北愛爾蘭.md "wikilink")。

相對低矮的山與周圍有幾個河流通往島嶼內。島上有茂密的植被，溫和但多變的海洋氣候。直至17世紀還有茂密的森林地覆蓋島上。今天，它是歐洲伐木量最高的地區之一。現存二十六種愛爾蘭原生動植物。

13世紀時中世紀的諾曼入侵給了[蓋爾族復甦的機會](../Page/蓋爾族.md "wikilink")。1603年後超過60年在16世紀的間歇戰導致英國的主導地位。在1690年間，英國對愛爾蘭實行新教統治制度，新教徒與天主教徒的鬥爭延長至18世紀。1801年，愛爾蘭成為英國的一部分。20世紀初的獨立戰爭導致島上的分裂，並建立了愛爾蘭自由邦。北愛爾蘭仍是英國的一部分，90年代鬥爭突然蔓延，進而發生[北愛爾蘭問題](../Page/北愛爾蘭問題.md "wikilink")。北愛衝突後簽訂[和平協議](../Page/貝爾法斯特協議.md "wikilink")。1973年，愛爾蘭共和國與北愛爾蘭共同加入[歐洲經濟共同體](../Page/歐洲經濟共同體.md "wikilink")。

愛爾蘭文化中遺留有傳統的凱爾特文化，例如蓋爾運動、愛爾蘭音樂和愛爾蘭語；同時也承襲了許多西方主流文化，並與鄰近的英國有著共同分享的文化以及共通的運動，比如[足球](../Page/足球.md "wikilink")、[橄欖球](../Page/橄欖球.md "wikilink")、[賽馬](../Page/賽馬.md "wikilink")、[高爾夫球和](../Page/高爾夫球.md "wikilink")[英語等等](../Page/英語.md "wikilink")。

## 相關條目

  - [愛爾蘭歷史](../Page/愛爾蘭歷史.md "wikilink")
  - [愛爾蘭共和國](../Page/愛爾蘭共和國.md "wikilink")
  - [北愛爾蘭](../Page/北愛爾蘭.md "wikilink")
  - [貝爾法斯特協議](../Page/貝爾法斯特協議.md "wikilink")
  - [英愛戰爭](../Page/英愛戰爭.md "wikilink")
  - [天主教](../Page/天主教.md "wikilink")
  - [新教](../Page/新教.md "wikilink")
  - [英國](../Page/英國.md "wikilink")

## 註釋

## 参考文献

{{-}}

[愛爾蘭](../Category/愛爾蘭.md "wikilink")
[Category:愛爾蘭島嶼](../Category/愛爾蘭島嶼.md "wikilink")
[Category:大西洋島嶼](../Category/大西洋島嶼.md "wikilink")
[Category:不列顛群島](../Category/不列顛群島.md "wikilink")
[Category:英國島嶼](../Category/英國島嶼.md "wikilink")
[Category:跨國島嶼](../Category/跨國島嶼.md "wikilink")
[Category:分裂地區](../Category/分裂地區.md "wikilink")