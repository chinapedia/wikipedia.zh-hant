**東洋町**（）是位于[高知縣東端的一](../Page/高知縣.md "wikilink")[町](../Page/町.md "wikilink")。產業以沿海漁業為主。

## 历史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：[安藝郡甲浦村](../Page/安藝郡_\(高知縣\).md "wikilink")、野根村。
  - 1916年4月1日：甲浦村改制為[甲浦町](../Page/甲浦町.md "wikilink")。
  - 1938年2月11日：野根村改制為[野根町](../Page/野根町.md "wikilink")。
  - 1959年7月1日：甲浦町和野根町[合併為](../Page/市町村合併.md "wikilink")**東洋町**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>甲浦村</p></td>
<td><p>1916年4月1日<br />
甲浦町</p></td>
<td><p>1959年7月1日<br />
東洋町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>野根村</p></td>
<td><p>1938年2月11日<br />
野根町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

[Asa-Kaigan-Kannoura-Station-20100526.jpg](https://zh.wikipedia.org/wiki/File:Asa-Kaigan-Kannoura-Station-20100526.jpg "fig:Asa-Kaigan-Kannoura-Station-20100526.jpg")

### 鐵路

  - [阿佐海岸鐵道](../Page/阿佐海岸鐵道.md "wikilink")
      - [阿佐東線](../Page/阿佐東線.md "wikilink")：[甲浦站](../Page/甲浦站.md "wikilink")

### 港灣

  - [甲浦港](../Page/甲浦港.md "wikilink")：過去曾有渡輪網反[大阪南港](../Page/大阪南港.md "wikilink")、[高知港](../Page/高知港.md "wikilink")、[足摺港](../Page/足摺港.md "wikilink")，但現已停止營運

## 觀光資源

  - 白濱海水浴場
  - 生見海岸
  - 甲浦八幡宮
  - 五社神社
  - 熊野神社
  - [野根山街道](../Page/野根山街道.md "wikilink")

## 本地出身之名人

  - [圓廣志](../Page/圓廣志.md "wikilink")：[歌手](../Page/歌手.md "wikilink")、[作曲家](../Page/作曲家.md "wikilink")

## 外部連結

  - [東洋町商工會](http://www.kochi-shokokai.jp/toyo/)