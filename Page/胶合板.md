[Birch_plywood.jpg](https://zh.wikipedia.org/wiki/File:Birch_plywood.jpg "fig:Birch_plywood.jpg")

LVL, Laminated Veneer Lumber,更通用的英文用語是Plywood.
**胶合板**也叫**夾板**、多层材是第一种发明的科技木材。由比较薄的木单板制作。由相邻两层木纹相互垂直的木片（单板）叠合。木片之间涂上强力胶，在一定温度一定压力下压合而成。通常用[酚醛胶](../Page/酚醛.md "wikilink")、[脲醛胶和](../Page/脲醛.md "wikilink")[三聚氰胺胶](../Page/三聚氰胺.md "wikilink")。使用胶合板代替木板的普遍原因是因为胶合板具有很强的抗破裂、抗收缩、抗扭曲和普通的高强度属性。LVL
区别于 Glulam，Glued laminated timber 。
[Lampa_ze_sklejki.jpg](https://zh.wikipedia.org/wiki/File:Lampa_ze_sklejki.jpg "fig:Lampa_ze_sklejki.jpg")

## 胶合板类型

胶合板具有很广泛的分类和用途。[软木胶合板通常由](../Page/软木.md "wikilink")[花旗杉](../Page/花旗杉.md "wikilink")[杨木和](../Page/杨木.md "wikilink")[松木](../Page/松木.md "wikilink")，
这些木材被广泛应用在建筑和工业目的上。装饰板通常使用[硬木做胶合板的面皮](../Page/硬木.md "wikilink")。包括[红橡](../Page/红橡.md "wikilink")、[桦木](../Page/桦木.md "wikilink")、[枫木](../Page/枫木.md "wikilink")、[柳桉](../Page/柳桉.md "wikilink")（[菲律宾](../Page/菲律宾.md "wikilink")[桃花心木](../Page/桃花心木.md "wikilink")）和其它的硬木树种。

胶合板如果用在室内，一般使用较便宜的脲醛胶。这种胶防水性能有限。室外用和[海洋板由于要防腐通常使用酚醛胶](../Page/海洋板.md "wikilink")。以抵抗胶合板分层开合，并在高湿情况下保持强度。

最普通的软木胶合板有三种：三层、五层和七层的芯板黏合。最外是注重美觀的面板、和次美觀的底板。大小为1220x2440mm（4x8英尺）每一层芯板的厚度从1.2到4.2mm不等。天花板一般用厚度小于9.5mm，地板由于地板宽度不等而使用厚度至少11.8mm，
地板通常使用[开槽胶合板每一张一面是凸起另一面是凹槽](../Page/开槽胶合板.md "wikilink")。两块可以拼合成一块。至於超耐磨地板用的是更薄的纤維板，非胶合板。

高强度胶合板，像[航空胶合板](../Page/航空胶合板.md "wikilink")，由桃花心木或者桦木制成。曾用在二战航空器上。包括[英国建造的](../Page/英国.md "wikilink")[蚊式轰炸机](../Page/蚊式轰炸机.md "wikilink")。[貨櫃底胶合板](../Page/貨櫃底胶合板.md "wikilink")、[貨車斗胶合板](../Page/貨車斗胶合板.md "wikilink")，則多由橡胶木制成。

## 胶合板产品

胶合板产品要求很好的原木，锯成要求的长度，剥去树皮。旋切或刨切，干燥，修补然后涂胶（冷压）热压， 经过热压再次修补，锯边，分检就成成品了。

## 历史

胶合板制作已有数千年的历史；已知最早的胶合板在大约公元前3500年的[古埃及](../Page/古埃及.md "wikilink")，当木制品被锯开的木皮交错的胶合在一起。这本来是由于好木材不足引起的。高质量的胶合在低质量的木片上，起到了装饰效果，以及结构的互补。

现代的胶合板的单板由[旋切机制作](../Page/旋切机.md "wikilink")。这种机器由[Immanuel
Nobel发明](../Page/Immanuel_Nobel.md "wikilink")。第一台在19世纪中期在[美国造成](../Page/美国.md "wikilink")。胶合板几十年来都是最重要的建筑材料。

比较OSB（[定向刨花板](../Page/定向刨花板.md "wikilink")）
和MDF（[中密度纤维板](../Page/中密度纤维板.md "wikilink")）。

## 胶合板应用

胶合板要求高质量的板材的地方有很多应用，高质量是指抗破裂，抗收缩，抗扭曲。

胶合板也被用来做为工程材料。从二战以来也被用在海洋和航空领域。

## 相关

  - [人造板](../Page/人造板.md "wikilink")
  - [科技木](../Page/科技木.md "wikilink")
  - [纤维板](../Page/纤维板.md "wikilink")
  - [胶合层集材](../Page/胶合层集材.md "wikilink")
  - [硬质纤维板](../Page/硬质纤维板.md "wikilink")
  - [美森耐](../Page/美森耐.md "wikilink")
  - [海洋板](../Page/海洋板.md "wikilink")
  - [中密度纤维板](../Page/中密度纤维板.md "wikilink")
  - [定向刨花板](../Page/定向刨花板.md "wikilink")
  - [刨花板](../Page/刨花板.md "wikilink")
  - [Pressed wood](../Page/Pressed_wood.md "wikilink")

## 木业杂志

  - [国际木业](https://web.archive.org/web/20070124000320/http://www.chinawood.org/website/index.jsp)
  - [中国人造板](https://web.archive.org/web/20070805113934/http://www.wbp.com.cn/)

## 更多

  - [The Engineering Wood Association (formerly known as the American
    Plywood Association)](http://www.apawood.org/)
  - [Canadian Plywood Association - Plywood Manufacturing Process，Grades
    and Specifications，Engineering Design Values，Technical
    Literature，Mill Contact Info，Environmental Practices，International
    Certification，Hobby Ply-Plans](http://www.canply.org/)
  - Sellers，Terry。1985。*Plywood and Adhesive Technology.* New York:
    Dekker。ISBN 0-8247-7407-8
  - [www.BQwood.com](http://www.BQwood.com/)

[Category:合成材料](../Category/合成材料.md "wikilink")
[Category:木材](../Category/木材.md "wikilink")
[Category:科技木](../Category/科技木.md "wikilink")