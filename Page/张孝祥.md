**张孝祥**（），字安国，号于湖居士，[歷陽](../Page/歷陽.md "wikilink")[烏江](../Page/烏江.md "wikilink")（今安徽和縣東北）人。[南宋詞人](../Page/南宋.md "wikilink")、政治家。

先祖曾居历阳乌江（今[安徽](../Page/安徽.md "wikilink")[和县](../Page/和县.md "wikilink")），再迁[明州](../Page/明州.md "wikilink")（今[宁波](../Page/宁波.md "wikilink")），为唐代诗人[张籍的七世孙](../Page/张籍.md "wikilink")\[1\]。

生于明州鄞县桃源鄉（今[宁波市](../Page/宁波市.md "wikilink")[海曙区横街镇](../Page/海曙区.md "wikilink")）。[宋高宗](../Page/宋高宗.md "wikilink")[绍兴二十四年](../Page/绍兴_\(南宋\).md "wikilink")（1154年），廷试第一。唱第之時，[秦檜黨羽曹詠](../Page/秦檜.md "wikilink")（參見《樹倒猢猻散》賦）于殿廷請婚。孝祥受揖不答。于是檜黨搜羅其父罪狀下獄。孝祥本有可意者，此時無奈，娶次舅時氏女為妻，遣同居李氏入浮山為女道士（近人考證）。

時氏早卒，被[喻樗选为女婿](../Page/喻樗.md "wikilink")。后因赞成[张浚北伐](../Page/张浚.md "wikilink")，事败被革职。又为荆南湖北路[安抚使](../Page/安抚使.md "wikilink")，興修水利，颇有治绩。紹興二十九年（1159），[御史中丞](../Page/御史中丞.md "wikilink")[汪徹弹劾](../Page/汪徹.md "wikilink")[汤思退](../Page/汤思退.md "wikilink")，孝祥受牵连被免官。不久又起知撫州。孝宗即位，出知平江府。召為[中書舍人](../Page/中書舍人.md "wikilink")，再遷直學士院兼都督府參贊軍事。[乾道五年](../Page/乾道.md "wikilink")（1169年），以疾請歸，以顯謨閣直學士致仕。是年，與[虞允文在](../Page/虞允文.md "wikilink")[芜湖舟中飲食](../Page/芜湖.md "wikilink")，中暑卒。\[2\]

孝祥才思足敏，词風清俊爽朗，佳處直逼[苏轼](../Page/苏轼.md "wikilink")。史稱其“尝慕东坡，每作为诗文，必问门人曰：‘比东坡如何？’”\[3\]今存《[-{于}-湖集](../Page/s:于湖集_\(四庫全書本\).md "wikilink")》40卷、《[-{于}-湖词](../Page/s:於湖詞_\(四庫全書本\).md "wikilink")》1卷。
中秋過洞庭調寄《[念奴嬌](../Page/s:念奴嬌_\(張孝祥\).md "wikilink")》見其本領。

李氏生子張同之，1971年，張同之夫婦墓于南京江浦黃葉嶺被發現，出土墓誌確定與嗣父張孝祥的本生父子關系。

## 注釋

## 參考書目

  - 《宋史》卷三八九
  - [周密](../Page/周密.md "wikilink")：《絕妙好詞箋》卷一

[Category:南宋政治人物](../Category/南宋政治人物.md "wikilink")
[Category:宋朝词人](../Category/宋朝词人.md "wikilink")
[Category:鄞县人](../Category/鄞县人.md "wikilink")
[Category:紹興二十四年甲戌科進士](../Category/紹興二十四年甲戌科進士.md "wikilink")
[Category:和县人](../Category/和县人.md "wikilink")
[Category:宁波人](../Category/宁波人.md "wikilink")
[X](../Category/张姓.md "wikilink")

1.  究其世系，恐非七代，今存之以俟後考。
2.  [王质](../Page/王质.md "wikilink")《雪山集》卷5《-{于}-湖文集序》：“岁己丑（1169年）……公没于当涂之芜湖。”[周密](../Page/周密.md "wikilink")《齐东野语》：“以当暑送虞雍公，饮芜湖舟中，中暑卒。”
3.  《四朝闻见录》