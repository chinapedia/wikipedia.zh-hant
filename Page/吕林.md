**吕林**（），[浙江](../Page/浙江.md "wikilink")[温岭](../Page/温岭.md "wikilink")[泽国镇人](../Page/泽国镇.md "wikilink")\[1\]，[中國著名](../Page/中國.md "wikilink")[乒乓球运动员](../Page/乒乓球.md "wikilink")。

兒子[呂翔也同樣是乒乓球運動員](../Page/呂翔_\(乒乓球運動員\).md "wikilink")。

## 技术特点

吕林是右手直拍[弧圈球结合快攻打法](../Page/弧圈球.md "wikilink")，以步伐见长。

## 生平

吕林1976年开始学习乒乓球，次年接受专业训练，1979年进入浙江队，1988年进入中国国家队。

由于单打能力并不出众，吕林鲜有参加国际乒乓球单打比赛的机会。他唯一一次作为单打主力队员参加单打比赛的经历，为[1992年巴塞罗那奥运会的单打比赛](../Page/1992年巴塞罗那奥运会.md "wikilink")。他以中国队的第三单打身份出战，仅取得所在小组的第三名，无缘十六强。

而在双打赛场，吕林则取得相当辉煌的战绩。1991年，吕林与[王涛合作参加](../Page/王涛_\(乒乓球运动员\).md "wikilink")[世界乒乓球锦标赛双打比赛](../Page/世界乒乓球锦标赛.md "wikilink")，于决赛中负于[瑞典的](../Page/瑞典.md "wikilink")[彼得·卡尔松](../Page/彼得·卡尔松.md "wikilink")/[托马斯·冯·舍组合](../Page/托马斯·冯·舍.md "wikilink")，获得亚军。次年，两人又再次合作，杀入[巴塞罗那奥运会男双决赛](../Page/1992年夏季奥林匹克运动会乒乓球比赛.md "wikilink")，并以3:2险胜德国的[罗斯科夫](../Page/约尔格·罗斯科夫.md "wikilink")/[费茨内尔组合](../Page/斯蒂芬·费茨内尔.md "wikilink")，首夺世界冠军。\[2\]其后，吕林/[王涛连夺两届世乒赛男双冠军](../Page/王涛_\(乒乓球运动员\).md "wikilink")。1996年，吕林和王涛在[亞特蘭大](../Page/亞特蘭大.md "wikilink")[奥运会的男双決賽中](../Page/1996年夏季奥林匹克运动会.md "wikilink")1:3负于[孔令輝](../Page/孔令輝.md "wikilink")/[劉國梁組合](../Page/劉國梁.md "wikilink")，奪得銀牌。\[3\]

吕林退役后曾担任国家女乒二队主教练。现任浙江体育职业技术学院竞技体育三系（浙江省小球运动管理中心）主任。\[4\]

## 参考资料

[Category:中国奥运乒乓球运动员](../Category/中国奥运乒乓球运动员.md "wikilink")
[Category:中国退役乒乓球运动员](../Category/中国退役乒乓球运动员.md "wikilink")
[Category:中国乒乓球教练](../Category/中国乒乓球教练.md "wikilink")
[Category:温岭人](../Category/温岭人.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:中国奥林匹克运动会银牌得主](../Category/中国奥林匹克运动会银牌得主.md "wikilink")
[Category:1992年夏季奧林匹克運動會獎牌得主](../Category/1992年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1996年夏季奧林匹克運動會獎牌得主](../Category/1996年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奥林匹克运动会乒乓球奖牌得主](../Category/奥林匹克运动会乒乓球奖牌得主.md "wikilink")
[Category:1992年夏季奥林匹克运动会乒乓球运动员](../Category/1992年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:1996年夏季奥林匹克运动会乒乓球运动员](../Category/1996年夏季奥林匹克运动会乒乓球运动员.md "wikilink")
[Category:亞洲運動會桌球獎牌得主](../Category/亞洲運動會桌球獎牌得主.md "wikilink")
[Category:1994年亚洲运动会金牌得主](../Category/1994年亚洲运动会金牌得主.md "wikilink")
[Category:浙江籍运动员](../Category/浙江籍运动员.md "wikilink")

1.  [搜狐体育·1992年巴塞罗那奥运会中国军团明星：吕林](http://sports.sohu.com/2004/06/17/58/news220585822.shtml)
2.  [1992年奥运乒乓球男双决赛录像](http://v.youku.com/v_show/id_XNTI1OTIzNTY=.html)
3.  [1996年奥运乒乓球男双决赛录像](http://v.youku.com/v_show/id_XNTI1OTk0NjA=.html)
4.  [我的奥林匹克-吕林](http://space.tv.cctv.com/act/video.jsp?videoId=VIDE1209735529237845)