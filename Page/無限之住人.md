《**無限之住人**》是[日本漫画家](../Page/日本漫画家.md "wikilink")[沙村广明的古装剑侠](../Page/沙村广明.md "wikilink")[漫畫作品](../Page/日本漫畫.md "wikilink")。於2008年7月至12月播放改編[电视动画](../Page/日本电视动画.md "wikilink")、全13話。

## 故事簡介

日本[江户时代的](../Page/江户.md "wikilink")1792年，拥有不死之身的剑士万次，发誓杀一千个恶人来赎百人斩的罪。遇到身负血仇的孤女凜后，他决定帮助其报仇。一路遭遇凜的仇人「逸刀流」，旨在剿灭逸刀流的[幕府密探](../Page/幕府.md "wikilink")「无骸流」以及其他种种势力或个人。

设定中的不死之身，是通过一种[西藏的秘术](../Page/西藏.md "wikilink")──血仙虫来实现。一旦成为血仙虫的寄主，无论是脑袋上开个洞，或是手脚砍断，都能及时被血仙虫修补好。为万次种上血仙虫的八百比丘尼就是这样活了八百岁。

## 風格與評價

沙村广明的画风非常写实，而设定奇诡、想象肆意，独创的武器层出不穷，被称为「新时代剧」。作者受过正规绘画教育，对人体解剖把握精准。作品不同于一般漫画的墨线勾勒，而用铅笔描绘出微妙的阴影和浓淡，也是为人所称道的。

## 登場人物

### 主要人物

  -
    右眼失明、鼻梁上有刀疤、衣后有卍形，武器是十二样奇形兵刃。万次原本是[同心](../Page/同心_\(官职\).md "wikilink")，为[旗本堀井重信杀了不少人后发现自己成了重信种种恶行的帮凶](../Page/旗本.md "wikilink")，于是斩了重信成为逃犯。逃亡过程中杀了百人，甚至杀了妹妹町的丈夫，致使町受到刺激发疯。之后万次遇到了八百比丘尼，她在万次身上种下「血仙虫」，给了他不死之身。万次立誓：「两年前，连阿町的丈夫在内，我一共杀了一百名同伴；现在，我赔上我的下半辈子，杀一千个恶人来赎罪！」而八百比丘尼也答应他，到时候「血仙虫」就会消失。

<!-- end list -->

  -
    身负血仇的16岁女孩。是「无天一流」统主浅野虎严的女儿。14岁时「无天一流」被逸刀流灭门。目睹惨剧的凛矢志向天津影久报仇。八百比丘尼介绍她找万次帮忙报仇。整个故事也就此展开。她的必杀技是「杀阵黄金虫」，準確度並不高，屬於亂槍打鳥型的散彈型攻擊。雖然藉著以量取勝多半可以傷到對方，但命中要害的機會幾乎沒有，穿透力跟殺傷力也不高，無法留下嚴重傷害。另外帶著一把來歷不明的家傳之劍「苦渡子死璃火」。頭上編了兩條麻花辮，辮尾置上圈形裝飾物
    （可藏藥丸）。

### 逸刀流

  -
    22岁的天才剑士、纤细清秀冷静寡言的少年，也是席卷[江户的剑士集团](../Page/江户.md "wikilink")「逸刀流」的统主。他继承祖父天津三郎的遗志，率领「逸刀流」以统一各流派为目标追求至强的剑法。他鄙视传统的武士精神、纯粹追求剑技，为了这个理想杀人无数。但天津并不嗜血，当年他没有杀那个14岁的小女孩凜，见到晕在路边的路人（还是凜）也会伸出援手。武器是一柄形状奇特的斧头和剑，名「頭椎」。

<!-- end list -->

  -
    出身是千束村的农民，妹妹被武士无端杀害后加入了打击武士的逸刀流。凶个性爽直，和万次不打不相识，颇有惺惺相惜之意。使用西洋引進的直刀（三重子母劍）「」。

<!-- end list -->

  -
    20多岁的卖笑女，也是最强的剑客，劍術更在天津影久及萬次之上。作为武士春川義明的女儿，10岁的槇絵在众人面前三次打败了下任统主的哥哥，致其[切腹自杀](../Page/切腹.md "wikilink")。她和母亲駒会被恼羞成怒的父亲赶出了家门。母亲为了养活她，只能去做妓女。11岁时，槇絵偶然救了正接受试练的年幼的影久，被天津三郎扔在野兽堆里，以一把剑斩了50多头野狼。使用一把三節槍，藏在三弦琴裡，名「春翁」，運用時以離心力助之，殺傷力極高。不幸患上肺癆……

<!-- end list -->

  -
    万次接受委托后第一个杀死的逸刀流高手。当年杀死凜的父亲的人。同时也是最早跟随天津创立逸刀流的人。將妻子及凛的母親的頭硝制縫在兩肩上。使用分別打造成三刃及四刃的飛鏢形武器「烏」。

<!-- end list -->

  -
    和万次一样种有血仙虫而拥有不死之身。他出生于[战国时代](../Page/战国.md "wikilink")，已经超过200岁了，在战场上杀死了1000人以上。武器名「井上真改
    蟲殺」，刀背上綴有毛髮，以作上毒之用。

<!-- end list -->

  -
    一个人带着孩子生活的面具贩子。執迷於人體上繪畫圖騰。凌辱過凛的母親。

<!-- end list -->

  -
    天津去加贺期间逸刀流的代理统主。65岁。老谋深算，在前往水戶路上與偽一對戰，激戰數回，仍不敵歲月的痕跡敗於偽一之手。

<!-- end list -->

  -
    影久的祖父。当年在无天一流门下时他的实力虽然比凛的祖父浅野虎行高，却因为行事违背武士传统而被逐出师门。此后处处被人鄙视排挤，因而性格越发偏激。

<!-- end list -->

  -
    身材矮小、性格急躁的少女剑士，左颊有刺青，戴着藏有铁板的格纹帽。在虾夷长大，与夷作一起来到日本做了山贼。试图打劫天津影久时被制服，加入了逸刀流。

<!-- end list -->

  -
    与瞳阿一起旅行的高大男子，是西方[传教士之子](../Page/传教士.md "wikilink")，性格淳朴。为了救瞳阿，而被囚禁到江户城，沦为吐鉤群不死试验的试验品。一度生死未卜。不使用武器仅装备防御用的护手,除了保护同伴外不与人动手,在逸刀流為不杀人的异数。

<!-- end list -->

  -
    在江户以「水科先生」为名行医，暗地里为逸刀流收集情报。在築波山南麗自己的故鄉率領自己村落的村民與吐 鉤群所率領之六鬼團中的伴 殷六 吐
    鉤群之女 燎對戰，雖然成功戰勝伴 殷六，但中計 燎所設下的陷阱，不幸陣亡。
  -
    [南蛮装束的男子](../Page/南蛮.md "wikilink")，戴着面具，用特殊的笛子使敌人陷入混乱。果心派他跟着凜一起潜入江户城。在逸刀流中为公认的"不想跟他并肩作战"第一名

### 无骸流

  -
    无骸流首领。表面上代表[幕府跟逸刀流谈判](../Page/幕府.md "wikilink")，邀请逸刀流作幕府开办的「讲剑所」的师傅，暗地里操纵着旨在剿灭逸刀流的秘密组织「无骸流」。后来他解散了无骸流，进而为了研究万次的不死身而秘密开始残忍的人体试验。在地窖戰鬥中被萬次奪去一目，並因城內的騷動而被解去職務。利用僅剩的30天，率領六鬼討伐逸刀流。

<!-- end list -->

  -
    无骸流中一位擅长易容的女性，武器是弓弩（折叠收在左臂）和毒药，藏在衣袖下，名「群踏」。她原是武士家的媳妇，本名「早川
    百」。三年前，26岁的她杀死了殺害兒子的丈夫早川霞江斎元京，无骸流的人把她从刑场救下，从此成了幕府剿除逸刀流的棋子。她的头发是用药物漂掉颜色的，所以身上有药味。在和逸刀流的戰鬥中，被擄強暴，後被偽一救出。

<!-- end list -->

  -
    无骸流一员。混血兒，母親是外國人。总是跟在百琳身边的小跟班。剑术不算高超但身手灵活，非常擅长狭窄构造的室内战斗，最后为了保护百琳，在屋內玄關處利用狹窄地形面對十名逸刀流成员並成功殺死其中四名，可惜最後仍被逸刀流的高手鬼拔斩杀。

<!-- end list -->

  -
    无骸流剑士，嗜殺成性。16岁就开始杀人换取奖金，使用一柄锯子般的刀。万次斩断他的右臂后他把右臂的残骨削尖作为武器。他想向万次寻仇，而凶则想为被他虐杀的阿恋报仇追杀他。凶得万次提供線索後，找到尸良對決，最後連尸良的左手也斬去，跌下深谷。然而尸良並未死去，在江戶城再度出現。因人體試驗得到不死身，最終在雪地上被野狗咬死。

<!-- end list -->

  -
    无骸流剑士。戴墨镜，沉默寡言。为了给孩子治病而充当杀手。杀了逸刀流无数高手。使用奇怪的武器「錦連・」。在成功完成无駭流最後一次任務(將萬次交給吐鉤群)後，贖回自己和百琳，並照顧懷孕了的她。
    在知道恩人吐鉤群危急的狀態下，再次披上戰袍準備出發。

<!-- end list -->

  -
    无骸流一员。看上去是无害的小厮，实际是潜伏在逸刀流内部的间谍。不过后来被识破了。最後死在阿葉山宗介之手。

### 心形唐流

  -
    心形唐流第二代掌门。60岁。一心要完成师傅「」的遗言，把师傅托付给自己的孙女密花嫁给了天津影久。后来幕府以密花的药相逼让他刺杀天津，无法两全的研水选择了[剖腹](../Page/剖腹.md "wikilink")。

<!-- end list -->

  -
    心形唐流的创始者、伊羽研秋的孙女。由于患有癆病一直服用进口药物，导致视力逐渐丧失。嫁給天津影久為妻。協助天津逃亡後，舉刀自殺。

### 无天一流

  -
    凜的父亲。「无天一流」统主。被黑衣鲭人杀害。

<!-- end list -->

  -
    凜的母亲。灭门之日被新夜侮辱后又被黑衣砍下头缝在身上。

<!-- end list -->

  -
    凜的祖父。上一代「无天一流」统主。当年武功比他高的师兄天津三郎被逐出师门后才得以当上统主，因當時無力阻止，心怀愧意，曾告诫凛无论发生什么事也不要憎恨天津。

### 其他

  -
    萬次的妹妹。因親眼目睹其丈夫齊藤辰政死於哥哥萬次的刀下而發瘋。

<!-- end list -->

  -
    活了八百岁的神秘尼姑。给万次种了「血仙虫」。

<!-- end list -->

  -
    画师。浅野虎严的幼时好友。曾做过幕府的密探。他的剑法非常高明，但是痴迷于绘画，為了得到西洋畫材器具而加入幕府，更為了繪畫向凜買下敵人的屍體以作顏料之用。不太管其他事。家事都由14岁的女儿阿辰操持。照顾过凛和万次。

<!-- end list -->

  -
    为了追求医术不择手段的医生。吐鉤群让他主持残酷的人体试验以解开万次不死身的秘密。在江戶城實驗曝光之後，被親哥哥救起，與哥哥對談後反省發願，行腳全國為自己所犯下的錯誤行醫。

<!-- end list -->

  -
    刽子手、试刀人。是本作中唯一有历史基础的人物。帮助吐鉤群进行人体试验。技能有「斬鉄」、「蝙蝠」等。於江戶城地下牢獄中與萬次對戰，敗於萬次之手。

<!-- end list -->

  -
    川上新夜之子。在懂事之前就與母親生活，母親逝世後回到素未謀面的父親身邊。對父親是逸刀流的成員並不知情，目擊萬次打倒父親後，刺了萬次並以為已經成功復仇。幾經波折還是被關進江戶城地下的監獄，替失去雙手的屍良跑腿，最後投入宗理的門下，專心於繪畫上。

<!-- end list -->

  -
    凶戴斗的戀人 在妓院工作 被尸良殺死

## 出版書籍

<table>
<thead>
<tr class="header">
<th><p>冊數</p></th>
<th><p><a href="../Page/講談社.md" title="wikilink">講談社</a></p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
<th><p><a href="../Page/天下出版.md" title="wikilink">天下出版</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p><a href="../Page/ISBN.md" title="wikilink">ISBN</a></p></td>
<td><p>發售日期</p></td>
<td><p>ISBN / <a href="../Page/EAN.md" title="wikilink">EAN</a></p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>1994年9月19日</p></td>
<td><p>ISBN 978-4-06-314090-3</p></td>
<td><p>1996年3月10日</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>1994年12月15日</p></td>
<td><p>ISBN 978-4-06-314101-6</p></td>
<td><p>1996年8月5日</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>1995年4月17日</p></td>
<td><p>ISBN 978-4-06-314109-2</p></td>
<td><p>1996年10月10日</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>1995年10月18日</p></td>
<td><p>ISBN 978-4-06-314119-1</p></td>
<td><p>1997年4月15日</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>1996年8月20日</p></td>
<td><p>ISBN 978-4-06-314137-5</p></td>
<td><p>1997年7月5日</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>1997年6月19日</p></td>
<td><p>ISBN 978-4-06-314151-1</p></td>
<td><p>1998年1月15日</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>1997年10月16日</p></td>
<td><p>ISBN 978-4-06-314165-8</p></td>
<td><p>1998年11月20日</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>1998年7月17日</p></td>
<td><p>ISBN 978-4-06-314183-2</p></td>
<td><p>1999年7月20日</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>1999年6月18日</p></td>
<td><p>ISBN 978-4-06-314210-5</p></td>
<td><p>2000年4月15日</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>2000年4月17日</p></td>
<td><p>ISBN 978-4-06-314238-9</p></td>
<td><p>2001年2月10日</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>2001年1月19日</p></td>
<td><p>ISBN 978-4-06-314259-4</p></td>
<td><p>2001年4月19日</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>2002年2月19日</p></td>
<td><p>ISBN 978-4-06-314268-6</p></td>
<td><p>2002年7月29日</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>2002年11月20日</p></td>
<td><p>ISBN 978-4-06-314306-5</p></td>
<td><p>2003年7月17日</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>2003年7月17日</p></td>
<td><p>ISBN 978-4-06-314326-3</p></td>
<td><p>2004年2月8日</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>2004年1月23日</p></td>
<td><p>ISBN 978-4-06-314337-9</p></td>
<td><p>2004年6月26日</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>2004年5月21日</p></td>
<td><p>ISBN 978-4-06-314348-5</p></td>
<td><p>2004年9月27日</p></td>
</tr>
<tr class="even">
<td><p>17</p></td>
<td><p>2004年11月22日</p></td>
<td><p>ISBN 978-4-06-314363-8</p></td>
<td><p>2005年3月17日</p></td>
</tr>
<tr class="odd">
<td><p>18</p></td>
<td><p>2005年6月23日</p></td>
<td><p>ISBN 978-4-06-314380-5</p></td>
<td><p>2005年8月23日</p></td>
</tr>
<tr class="even">
<td><p>19</p></td>
<td><p>2006年4月21日</p></td>
<td><p>ISBN 978-4-06-314409-3</p></td>
<td><p>2006年7月26日</p></td>
</tr>
<tr class="odd">
<td><p>20</p></td>
<td><p>2006年10月23日</p></td>
<td><p>ISBN 978-4-06-314430-7</p></td>
<td><p>2007年2月7日</p></td>
</tr>
<tr class="even">
<td><p>21</p></td>
<td><p>2007年6月22日</p></td>
<td><p>ISBN 978-4-06-314455-0</p></td>
<td><p>2007年10月15日</p></td>
</tr>
<tr class="odd">
<td><p>22</p></td>
<td><p>2007年12月21日</p></td>
<td><p>ISBN 978-4-06-314480-2</p></td>
<td><p>2008年3月31日</p></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td><p>2008年6月23日</p></td>
<td><p>ISBN 978-4-06-314509-0</p></td>
<td><p>2008年11月14日</p></td>
</tr>
<tr class="odd">
<td><p>24</p></td>
<td><p>2009年2月23日</p></td>
<td><p>ISBN 978-4-06-314548-9</p></td>
<td><p>2009年6月24日</p></td>
</tr>
<tr class="even">
<td><p>25</p></td>
<td><p>2009年9月23日</p></td>
<td><p>ISBN 978-4-06-314591-5</p></td>
<td><p>2010年1月16日</p></td>
</tr>
<tr class="odd">
<td><p>26</p></td>
<td><p>2010年5月21日</p></td>
<td><p>ISBN 978-4-06-310654-1</p></td>
<td><p>2010年8月25日</p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td><p>2011年1月21日</p></td>
<td><p>ISBN 978-4-06-310722-7</p></td>
<td><p>2011年6月21日</p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p>2011年10月21日</p></td>
<td><p>ISBN 978-4-06-310775-3</p></td>
<td><p>2012年10月17日</p></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p>2012年5月23日</p></td>
<td><p>ISBN 978-4-06-387818-9</p></td>
<td><p>2013年3月1日</p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td><p>2013年2月22日</p></td>
<td><p>ISBN 978-4-06-387869-1</p></td>
<td><p>2013年6月21日</p></td>
</tr>
</tbody>
</table>

  - 新裝版（豪華版）

<table>
<thead>
<tr class="header">
<th><p>冊數</p></th>
<th><p>講談社</p></th>
<th><p><a href="../Page/青文出版社.md" title="wikilink">青文出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>2016年8月23日</p></td>
<td><p>ISBN 978-4-06-388177-6</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2016年8月23日</p></td>
<td><p>ISBN 978-4-06-388178-3</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2016年8月23日</p></td>
<td><p>ISBN 978-4-06-388179-0</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2016年9月23日</p></td>
<td><p>ISBN 978-4-06-393031-3</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>ISBN 978-4-06-393032-0</p></td>
<td><p>2017年7月25日</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>2016年10月21日</p></td>
<td><p>ISBN 978-4-06-393057-3</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>ISBN 978-4-06-393058-0</p></td>
<td><p>EAN 471-801-602-451-9</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>2016年11月22日</p></td>
<td><p>ISBN 978-4-06-393079-5</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>ISBN 978-4-06-393080-1</p></td>
<td><p>EAN 471-801-602-536-3</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>2016年12月22日</p></td>
<td><p>ISBN 978-4-06-393107-5</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>ISBN 978-4-06-393108-2</p></td>
<td><p>2017年11月16日</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>2017年1月23日</p></td>
<td><p>ISBN 978-4-06-393119-8</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>ISBN 978-4-06-393120-4</p></td>
<td><p>2017年12月18日</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p>2017年2月23日</p></td>
<td><p>ISBN 978-4-06-393151-8</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>ISBN 978-4-06-393152-5</p></td>
<td><p>EAN 471-801-602-741-1</p></td>
</tr>
</tbody>
</table>

## 相關商品

  - 無限住人Postcard Book 三十二幻唱（）

<!-- end list -->

  -
    使用作品內的插圖和扉頁編輯而成，共34張明信片的硬殼精裝書。

<table>
<thead>
<tr class="header">
<th><p>冊數</p></th>
<th><p>講談社</p></th>
<th><p>東立出版社</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>全</p></td>
<td><p>1996年3月28日</p></td>
<td><p>ISBN 978-4-06-330009-3</p></td>
</tr>
</tbody>
</table>

  - （1996年、[波麗佳音](../Page/波麗佳音.md "wikilink")）

<!-- end list -->

  -
    [人間椅子的音樂專輯CD](../Page/人間椅子.md "wikilink")。

<!-- end list -->

  - [侍](../Page/侍_\(遊戲軟體\).md "wikilink")（2002年、[PlayStation
    2專用遊戲軟體](../Page/PlayStation_2.md "wikilink")、[Spike](../Page/Spike_\(公司\).md "wikilink")）

<!-- end list -->

  -
    不為人知的万次另一面首次登場。

<!-- end list -->

  - （2006年、Zenrin Promo）

<!-- end list -->

  -
    作中所登場的武器圖鑑。由沙村広明親自監修。全11種。

<!-- end list -->

  - （2008年、講談社）

<!-- end list -->

  -
    收錄大量彩圖和鉛筆素描畫。

<!-- end list -->

  - 小說 無限住人 刃獸異聞（）

<!-- end list -->

  -
    [大迫純一將原著內容改編的小說化作品](../Page/大迫純一.md "wikilink")。由[講談社輕小說文庫負責發行](../Page/講談社輕小說文庫.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>冊數</p></th>
<th><p>講談社</p></th>
<th><p>東立出版社</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>全</p></td>
<td><p>2008年7月19日</p></td>
<td><p>ISBN 978-4-06-373323-5</p></td>
</tr>
</tbody>
</table>

  - 文庫版
    2013年2月21日發行、ISBN 978-4-06-375283-0

<!-- end list -->

  - （2008年、講談社）

<!-- end list -->

  -
    幫[手機遊戲網頁上的會員制限定版遊戲繪製內容](../Page/手機遊戲.md "wikilink")。

## 電視動畫

### 製作人員

  - 原作：[沙村廣明](../Page/沙村廣明.md "wikilink")《無限之住人》
  - 執行製作人：針生雅行、關澤新二、石川
  - 製作人：松下卓也、木下哲哉、森下勝司、山田昇
  - 動畫製作人：村岡秀昭、丸亮二、清水孝泰、田中憲一朗
  - 系列構成：川崎裕之
  - 人物設定：山下喜光
  - 武器設計：肥塚正史
  - 色彩設計：小島真喜子
  - 特殊效果：加茂
  - 美術監督：海野
  - 攝影監督：五十嵐慎一、齋藤仁
  - 音響監督：中野徹
  - 音樂：[大谷幸](../Page/大谷幸.md "wikilink")
  - 監督：真下耕一
  - 動畫製作協力：[Production I.G](../Page/Production_I.G.md "wikilink")
  - 動畫製作：Bee Train
  - 製作：[淺野道場復興會](../Page/製作委員會方式.md "wikilink")

### 主題曲

  - 片頭曲：「****」

<!-- end list -->

  -
    歌：枕草子
    作詞：愛華、作曲：[大谷幸](../Page/大谷幸.md "wikilink")

<!-- end list -->

  - 片尾曲：「**wants**」

<!-- end list -->

  -
    歌：GRAPEVINE
    作詞：田中和將、作曲：龜井亨、編曲：長田達\&GRAPEVINE

### 各集標題

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>日文標題</p></th>
<th><p>中文標題</p></th>
<th><p>劇本</p></th>
<th><p>分鏡</p></th>
<th><p>演出</p></th>
<th><p>作畫監督</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一話</p></td>
<td></td>
<td><p>罪人</p></td>
<td><p><a href="../Page/川崎裕之.md" title="wikilink">川崎裕之</a></p></td>
<td><p><a href="../Page/真下耕一.md" title="wikilink">真下耕一</a></p></td>
<td><p>黑川智之</p></td>
<td><p><a href="../Page/山下喜光.md" title="wikilink">山下喜光</a></p></td>
</tr>
<tr class="even">
<td><p>第二話</p></td>
<td></td>
<td><p>征服</p></td>
<td><p>澤井幸次</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第三話</p></td>
<td></td>
<td><p>戀詠</p></td>
<td><p>有江勇樹</p></td>
<td><p>天崎<br />
<a href="../Page/門智昭.md" title="wikilink">門智昭</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第四話</p></td>
<td></td>
<td><p>天才</p></td>
<td><p>守岡博</p></td>
<td><p>津幡佳明</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第五話</p></td>
<td></td>
<td><p>執人</p></td>
<td><p>黑川智之</p></td>
<td><p><a href="../Page/佐佐木睦美.md" title="wikilink">佐佐木睦美</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第六話</p></td>
<td></td>
<td><p>蟲之歌</p></td>
<td><p>有江勇樹</p></td>
<td><p>天崎</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第七話</p></td>
<td></td>
<td><p>三途</p></td>
<td><p>澤井幸次</p></td>
<td><p>山下喜光</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第八話</p></td>
<td></td>
<td><p>爪彈</p></td>
<td><p>守岡博</p></td>
<td><p>落合瞳</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第九話</p></td>
<td></td>
<td><p>夢彈</p></td>
<td><p>黑川智之</p></td>
<td><p>津幡佳明</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第十話</p></td>
<td></td>
<td><p>變臉</p></td>
<td><p><a href="../Page/金卷兼一.md" title="wikilink">金卷兼一</a></p></td>
<td><p>澤井幸次<br />
有江勇樹</p></td>
<td><p>澤井幸次</p></td>
<td><p>佐佐木睦美</p></td>
</tr>
<tr class="odd">
<td><p>第十一話</p></td>
<td></td>
<td><p>羽毛</p></td>
<td><p>澤井幸次</p></td>
<td><p>天崎</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第十二話</p></td>
<td></td>
<td><p>斜凜</p></td>
<td><p>川崎裕之</p></td>
<td><p>守岡博</p></td>
<td><p>落合瞳</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第十三話</p></td>
<td></td>
<td><p>風</p></td>
<td><p>澤井幸次</p></td>
<td><p>黑川智之</p></td>
<td><p>山下喜光</p></td>
<td></td>
</tr>
</tbody>
</table>

## 電影

於2015年10月，宣布本作將改編為真人電影版的消息。由[三池崇史執導](../Page/三池崇史.md "wikilink")，[大石哲也編劇](../Page/大石哲也.md "wikilink")，[木村拓哉](../Page/木村拓哉.md "wikilink")、[杉咲花主演](../Page/杉咲花.md "wikilink")。真人版於同年11月開拍，2017年4月29日上映\[1\]\[2\]。台灣則於2017年11月6日，於金馬國際影展中首映。

### 演員陣容

  - 万次 - [木村拓哉](../Page/木村拓哉.md "wikilink")
  - 浅野凜 - [杉咲花](../Page/杉咲花.md "wikilink")\[3\]
  - 天津影久 - [福士蒼汰](../Page/福士蒼汰.md "wikilink")
  - 乙橘槇繪 - [戶田惠梨香](../Page/戶田惠梨香.md "wikilink")
  - 尸良 - [市原隼人](../Page/市原隼人.md "wikilink")
  - 閑馬永空 - [市川海老藏](../Page/市川海老藏.md "wikilink")
  - 伊羽研水 - [山崎努](../Page/山崎努.md "wikilink")
  - 黑衣鯖人 - [北村一輝](../Page/北村一輝.md "wikilink")
  - 吐鉤群 - [田中泯](../Page/田中泯.md "wikilink")
  - 百琳 - [栗山千明](../Page/栗山千明.md "wikilink")
  - 凶戴斗 - [滿島真之介](../Page/滿島真之介.md "wikilink")

### 製作團隊

  - 原作 - [沙村廣明](../Page/沙村廣明.md "wikilink")《無限之住人》
  - 導演 - [三池崇史](../Page/三池崇史.md "wikilink")
  - 編劇 - [大石哲也](../Page/大石哲也.md "wikilink")
  - 發行 - [華納兄弟電影](../Page/華納娛樂日本.md "wikilink")
  - 生產製作 - [OLM](../Page/OLM.md "wikilink")
  - 製作 - 電影《無限之住人》製作委員会

## 舞台劇

### 演員陣容

  - 万次 - [關智一](../Page/關智一.md "wikilink")\[4\]
  - 凛 - [福圓美里](../Page/福圓美里.md "wikilink")\[5\]
  - 天津影久 - [浪川大輔](../Page/浪川大輔.md "wikilink")\[6\]
  - 八百比丘尼 - [大場達也](../Page/大場達也.md "wikilink")\[7\]
  - 凶戴斗 - [沖野晃司](../Page/沖野晃司.md "wikilink")\[8\]
  - 乙橘槇繪 - [長澤美樹](../Page/長澤美樹.md "wikilink")\[9\]
  - 百林 - [那珂村貴子](../Page/那珂村貴子.md "wikilink")\[10\]
  - 偽一 - [大高雄一郎](../Page/大高雄一郎.md "wikilink")\[11\]
  - 虎杖 - [置鮎龍太郎](../Page/置鮎龍太郎.md "wikilink") /
    [湯田昌次](../Page/湯田昌次.md "wikilink")（W Cast）\[12\]
  - 入谷 - [木村昴](../Page/木村昴.md "wikilink") /
    [岩崎諒太](../Page/岩崎諒太.md "wikilink")（W Cast）\[13\]
  - 吐鉤群 - [中尾隆聖](../Page/中尾隆聖.md "wikilink")\[14\]

## 获得奖项

  - Afternoon四季奖1993年的大奖（同年在《月刊Afternoon》开始连载）。
  - 1997年第一届文化厅媒体艺术节漫画部优秀奖作品。
  - 英语翻译版本于2000年获得美国EISNER漫画奖的最佳国际作品奖。

## 參考資料

## 外部連結

  - [動畫官方網站](https://web.archive.org/web/20140818180518/http://mugen.kc.kodansha.co.jp/)

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:講談社漫畫](../Category/講談社漫畫.md "wikilink")
[Category:青年漫畫](../Category/青年漫畫.md "wikilink")
[Category:改编成电影的日本漫画](../Category/改编成电影的日本漫画.md "wikilink")
[Category:歷史漫畫](../Category/歷史漫畫.md "wikilink")
[Category:2008年日本電視動畫](../Category/2008年日本電視動畫.md "wikilink")
[Category:月刊Afternoon](../Category/月刊Afternoon.md "wikilink")
[Category:江戶時代背景電影](../Category/江戶時代背景電影.md "wikilink")
[Category:AT-X動畫](../Category/AT-X動畫.md "wikilink") [Category:BEE
TRAIN](../Category/BEE_TRAIN.md "wikilink")
[Category:2017年日本電影](../Category/2017年日本電影.md "wikilink")
[Category:日本漫畫改編真人電影](../Category/日本漫畫改編真人電影.md "wikilink")
[Category:三池崇史電影](../Category/三池崇史電影.md "wikilink")
[Category:華納兄弟日本電影](../Category/華納兄弟日本電影.md "wikilink")
[Category:漫畫改編舞台劇](../Category/漫畫改編舞台劇.md "wikilink")
[Category:木村拓哉](../Category/木村拓哉.md "wikilink")

1.

2.

3.

4.  [ヘロＱ公式twitter【情報解禁！③】](https://twitter.com/1Heroq/status/640710246835490816)

5.
6.
7.
8.  [【キャスト発表&ビジュアル解禁②】](https://twitter.com/1Heroq/status/678227152634970112)

9.  [【キャスト発表&ビジュアル解禁！】](https://twitter.com/1Heroq/status/678225111724376064)

10. [【キャスト発表&ビジュアル解禁その2】](https://twitter.com/1Heroq/status/679671646978838528)

11. [【キャスト発表&ビジュアル解禁その2】](https://twitter.com/1Heroq/status/679671022186934272)

12. [【キャスト発表&ビジュアル解禁その3】](https://twitter.com/1Heroq/status/681102087975911424)

13. [【キャスト発表&ビジュアル解禁その３】](https://twitter.com/1Heroq/status/681103296824655872)

14. [【キャスト発表&ビジュアル解禁】](https://twitter.com/1Heroq/status/682546904417177600)