**萊伊什郡**（County Laois；[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：Contae
Laoise），是[愛爾蘭的一個郡](../Page/愛爾蘭共和國.md "wikilink")，位於[愛爾蘭島中部](../Page/愛爾蘭島.md "wikilink")，[巴羅河和](../Page/巴羅河.md "wikilink")[諾爾河上游](../Page/諾爾河.md "wikilink")。歷史上屬[倫斯特省](../Page/倫斯特省.md "wikilink")。

面積1,719平方公里，2006年人口69,012人。首府[萊伊什港](../Page/萊伊什港.md "wikilink")。

## 參考文獻

## 外部連結

  - [Official website of Laois County Council](http://www.laois.ie)

  -
  - [Laois Tourism Website](http://www.laoistourism.ie)

  - [Things to do in Laois - Tourist
    guide](http://beautifulstay.holiday/things-to-do-in-Laois)

  - [Map of castles, fortified houses and ruins in
    Laois](http://beautifulstay.holiday/Castles-in-Laois)

[\*](../Category/萊伊什郡.md "wikilink")
[Category:倫斯特省](../Category/倫斯特省.md "wikilink")
[Category:愛爾蘭共和國的郡](../Category/愛爾蘭共和國的郡.md "wikilink")