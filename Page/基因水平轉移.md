[Tree_Of_Life_(with_horizontal_gene_transfer).svg](https://zh.wikipedia.org/wiki/File:Tree_Of_Life_\(with_horizontal_gene_transfer\).svg "fig:Tree_Of_Life_(with_horizontal_gene_transfer).svg")
[Elysia_chlorotica_by_Gould_1.jpg](https://zh.wikipedia.org/wiki/File:Elysia_chlorotica_by_Gould_1.jpg "fig:Elysia_chlorotica_by_Gould_1.jpg")（Elysia
chlorotica）擁有[盜食體質](../Page/盜食體質.md "wikilink")，可吸收[藻類中的](../Page/藻類.md "wikilink")[葉綠體](../Page/葉綠體.md "wikilink")。使用[葉綠體基因的](../Page/葉綠體.md "wikilink")[光合作用繼續可達](../Page/光合作用.md "wikilink")12個月，這是因為藻類核基因被轉移到定向海蛞蝓細胞核內。\[1\]\]\]

**基因水平轉移**（，[縮寫](../Page/縮寫.md "wikilink")：****）又稱**水平基因轉移**或**基因側向轉移**（****，[縮寫](../Page/縮寫.md "wikilink")：****），指生物將遺傳物質傳遞給其他細胞而非其子代的過程，例如：[接合](../Page/接合.md "wikilink")、[转导及](../Page/转导.md "wikilink")[转化](../Page/轉化_\(生物\).md "wikilink")。與此相對，“基因垂直传递”指生物由其祖先繼承遺傳物質。[遺傳學一般關心更爲普遍的垂直傳遞](../Page/遺傳學.md "wikilink")，但目前的知識表明，基因水平轉移是一個重要的現象\[2\]。由於此現象的存在，使生物早期的演化關係更為複雜。

水平基因轉移是細菌[抗生素抗藥性的主要原因](../Page/抗生素抗藥性.md "wikilink")\[3\]\[4\]\[5\]\[6\]，並且在細菌可降解新型化合物例如人類創建的殺蟲劑進化中起著重要作用\[7\]，並在進化，維護和傳輸毒性的重要原因
\[8\]</ref>。這種基因水平轉移經常涉及溫和的[噬菌體和](../Page/噬菌體.md "wikilink")[質粒](../Page/質粒.md "wikilink")\[9\]\[10\]。

大多數的思維在遺傳學一直專注於垂直傳遞，但是人們日益認識到基因水平轉移是一種非常顯著的現象，以及是在單細胞生物之間或許是基因轉移主要形式\[11\]\[12\]。

人工的基因水平轉移屬於[基因工程的一種](../Page/基因工程.md "wikilink")。

## 基因水平轉移生物

  - [蛭形輪蟲](../Page/蛭形輪蟲.md "wikilink")\[13\]
  - [水熊](../Page/水熊.md "wikilink")
  - [綠葉海天牛](../Page/綠葉海天牛.md "wikilink")

## 参考文献

## 参见

  - [土壤杆菌属](../Page/土壤杆菌属.md "wikilink")，眾所周知的細菌，因為其本身和植物之間傳遞DNA的能力。

  - [转基因生物](../Page/转基因生物.md "wikilink")

  - [可动遗传因子](../Page/可动遗传因子.md "wikilink")

  - [演化網路](../Page/演化網路.md "wikilink")

  - [演化](../Page/演化.md "wikilink")

  - [系統發生樹](../Page/系統發生樹.md "wikilink"), 又稱演化樹

  - [原病毒](../Page/原病毒.md "wikilink")

  - [反转录转座子](../Page/反转录转座子.md "wikilink")

  -
{{-}}

[Category:遗传学](../Category/遗传学.md "wikilink")
[Category:微生物种群生物学](../Category/微生物种群生物学.md "wikilink")
[Category:微生物学](../Category/微生物学.md "wikilink")
[Category:抗生素抗藥性](../Category/抗生素抗藥性.md "wikilink")

1.

2.  [Horizontal and vertical: The evolution of evolution - life - 26
    January 2010 - New
    Scientist](http://www.newscientist.com/article/mg20527441.500-horizontal-and-vertical-the-evolution-of-evolution.html?full=true&print=true)

3.  OECD, *Safety Assessment of Transgenic Organisms, Volume 4: OECD
    Consensus Documents,* 2010, pp.171-174

4.

5.

6.

7.

8.
9.

10.

11.

12.

13.