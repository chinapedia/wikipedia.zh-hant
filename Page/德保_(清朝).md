**德保**（，），字仲容，一字潤亭，號定圃，[索綽絡氏](../Page/索綽絡氏.md "wikilink")，[滿洲](../Page/滿洲.md "wikilink")[正白旗人](../Page/正白旗.md "wikilink")，[中國](../Page/中國.md "wikilink")[清朝政治人物](../Page/清朝.md "wikilink")。

## 生平

德保為[乾隆二年](../Page/乾隆.md "wikilink")（1737年）丁巳恩科三甲進士，改[庶吉士](../Page/庶吉士.md "wikilink")，[散館授](../Page/散館.md "wikilink")[翰林院檢討](../Page/翰林院.md "wikilink")。乾隆九年（1744年）任[日講](../Page/日講官.md "wikilink")[起居注官](../Page/起居注.md "wikilink")，次年值[南書房](../Page/南書房.md "wikilink")。乾隆十一年（1746年）升侍講，次年提督山西[學政](../Page/學政.md "wikilink")，升侍讀。乾隆十五年（1750年）提督[山東](../Page/山東.md "wikilink")[學政](../Page/學政.md "wikilink")，升侍講學士。乾隆十七年（1752年）任[工部侍郎](../Page/工部侍郎.md "wikilink")。乾隆二十六年（1761年）充[經筵講官](../Page/經筵.md "wikilink")。乾隆三十四年（1769年）升翰林院掌院學士，同年出任廣東巡撫，乾隆三十六年（1771年）署[兩廣總督](../Page/兩廣總督.md "wikilink")。乾隆四十一年（1776年）代理[福建巡撫](../Page/福建巡撫.md "wikilink")、[漕運總督](../Page/漕運總督.md "wikilink")，次年兼署[江南河道總督](../Page/江南河道總督.md "wikilink")。乾隆四十三年（1778年）署[閩浙總督](../Page/閩浙總督.md "wikilink")，同年任[禮部尚書](../Page/禮部尚書.md "wikilink")，卒謚**文莊**。

纂有《音韻述微》，纂辦《[日下舊聞考](../Page/日下舊聞考.md "wikilink")》。

兒子[英和](../Page/英和.md "wikilink")，官至軍機大臣，協辦大學士；女兒為乾隆帝之[瑞貴人](../Page/瑞貴人.md "wikilink")。

## 參考文獻

  - 《福建省志》，福建省地方誌編纂委員會編，1992年。
  - 清人室名別稱字號索引：增補本（下冊）858頁.
  - 復初齋詩集
  - 纂修四庫全書檔案
  - [德保](http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%BCw%ABO)
    中央研究院歷史語言研究所藏內閣大庫檔案

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝山西學政](../Category/清朝山西學政.md "wikilink")
[Category:清朝翰林院侍講學士](../Category/清朝翰林院侍講學士.md "wikilink")
[Category:清朝山東學政](../Category/清朝山東學政.md "wikilink")
[Category:清朝工部侍郎](../Category/清朝工部侍郎.md "wikilink")
[Category:清朝翰林院掌院學士](../Category/清朝翰林院掌院學士.md "wikilink")
[Category:清朝廣東巡撫](../Category/清朝廣東巡撫.md "wikilink")
[Category:清朝福建巡抚](../Category/清朝福建巡抚.md "wikilink")
[Category:清朝两广总督](../Category/清朝两广总督.md "wikilink")
[Category:清朝漕運總督](../Category/清朝漕運總督.md "wikilink")
[Category:清朝閩浙總督](../Category/清朝閩浙總督.md "wikilink")
[Category:清朝禮部尚書](../Category/清朝禮部尚書.md "wikilink")
[Category:正白旗满洲佐领下人](../Category/正白旗满洲佐领下人.md "wikilink")
[Category:吉林人](../Category/吉林人.md "wikilink")
[D](../Category/索绰罗氏.md "wikilink")
[Category:南書房行走](../Category/南書房行走.md "wikilink")