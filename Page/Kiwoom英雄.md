**Kiwoom英雄**（韓文：키움
히어로즈）是一支[韓國棒球委員會的球隊](../Page/韓國棒球委員會.md "wikilink")，母企業為[韓國的投資公司](../Page/韓國.md "wikilink")**Centennial
Investment**，成立於2008年，為[現代獨角獸宣布解散後](../Page/現代獨角獸.md "wikilink")，由投資公司**Centennial
Investment**獲邀組隊，成員大多來自原[現代獨角獸隊](../Page/現代獨角獸.md "wikilink")，目前該隊以出售冠名贊助權為隊名，成立之初由[我們煙草公司](../Page/我們煙草公司.md "wikilink")（韓文：우리담배）取得冠名贊助權，後由[耐克森輪胎取得](../Page/耐克森輪胎.md "wikilink")，2018年底改和[Kiwoom證券簽約](../Page/Kiwoom證券.md "wikilink")，並更為現名。

## 球隊歷史

2007年底[現代集團爆發財務及經營危機](../Page/現代集團.md "wikilink")，因此宣布解散旗下的[現代獨角獸隊](../Page/現代獨角獸.md "wikilink")，但始終無任何下文或是出現破局，最後投資公司Centennial獲邀組隊，宣布不承接[現代獨角獸的隊史](../Page/現代獨角獸.md "wikilink")，並出售球隊冠名權尋求贊助，但母企業仍為Centennial
Investment旗下的子公司「**首爾英雄職棒公司**」，2008年由我們煙草公司買下3年冠名權，隊名為**我們英雄**（우리
히어로즈），根據地為[首爾特別市的](../Page/首爾特別市.md "wikilink")[木洞棒球場](../Page/木洞棒球場.md "wikilink")，2008年中由於我們菸草公司爆發醜聞，嚴重打擊球隊形象，球團遂於8月26日宣布與我們菸草公司結束贊助關係，隔日起改名為首爾英雄隊（히어로즈）。

2010年將球隊冠名權售出給[輪胎製造商](../Page/輪胎.md "wikilink")[耐克森輪胎](../Page/耐克森輪胎.md "wikilink")（Nexen
Tire），並更名為**耐克森英雄**（넥센
히어로즈），2016年也將主場遷至新建好的[首爾高尺洞巨蛋](../Page/高尺天空巨蛋.md "wikilink")
，直到2018年底Centennial
Investment所屬的首爾英雄職棒公司宣布和[Kiwoom證券簽約](../Page/Kiwoom證券.md "wikilink")，並更名為**Kiwoom英雄**（키움
히어로즈）。

## 歷代監督

|     |                                      |                                      |     |    |
| --- | ------------------------------------ | ------------------------------------ | --- | -- |
| 教練名 | 接任年份                                 | 解任年份                                 | 總冠軍 | 備註 |
| 金始真 | [2008年](../Page/2008年.md "wikilink") | [2012年](../Page/2012年.md "wikilink") |     |    |
| 金性甲 | [2012年](../Page/2012年.md "wikilink") | [2012年](../Page/2012年.md "wikilink") |     | 代理 |
| 廉京燁 | [2013年](../Page/2013年.md "wikilink") | [2016年](../Page/2016年.md "wikilink") |     |    |
| 張正錫 | [2017年](../Page/2017年.md "wikilink") |                                      |     | 現任 |

<noinclude></noinclude>

## 現役成員

## 外部連結

  - [Kiwoom英雄官方網站](http://www.heroesbaseball.co.kr/index.do)
  - [Kiwoom英雄官方臉書](https://www.facebook.com/kiwoomheroesbaseballclub/?__tn__=HHH-R)
  - [Kiwoom英雄官方YouTube](https://www.youtube.com/channel/UC_MA8-XEaVmvyayPzG66IKg)
  - [Kiwoom英雄官方IG](https://www.instagram.com/heroesbaseballclub/)

[Category:紀錄模板](../Category/紀錄模板.md "wikilink")
[Category:韓國職棒球隊](../Category/韓國職棒球隊.md "wikilink")