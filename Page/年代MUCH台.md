**年代MUCH台**，是臺灣[年代集團旗下所屬一有線衛星頻道](../Page/年代集團.md "wikilink")。

## 年代MUCH臺的歷史

### 歡樂無線臺時期

  - 時間：1994年3月15日至1995年5月，臺灣第一個專門直播體育比賽的衛星頻道
  - 主播群：
      - [陶晶瑩](../Page/陶晶瑩.md "wikilink")（主持[Hit\!Hit\!紅不讓](../Page/Hit!Hit!紅不讓.md "wikilink")　與職棒球星互動的娛樂節目）
      - [錢定遠](../Page/錢定遠.md "wikilink")（1989-1993年為[中廣主播](../Page/中廣.md "wikilink")，因轉播職棒出色，1994年由邱復生挖角；現為[博斯電視網主播](../Page/博斯運動頻道.md "wikilink")）
      - \-{[蔡明里](../Page/蔡明里.md "wikilink")}-（1989-1993年任職於[中華職棒競紀組](../Page/中華職棒.md "wikilink")，1994年由邱復生挖角；現[緯來體育台主播](../Page/緯來體育台.md "wikilink")）

### TVIS時期

  - 時間：1995年5月至1998年5月31日
  - 主播群：
      - [詹慶齡](../Page/詹慶齡.md "wikilink")（現中天新聞部總監）
      - [錢定遠](../Page/錢定遠.md "wikilink")
      - [鄧國雄](../Page/鄧國雄.md "wikilink")（現[FOX體育台主播](../Page/FOX體育台.md "wikilink")）
      - \-{[蔡明里](../Page/蔡明里.md "wikilink")}-（現[緯來體育臺主播](../Page/緯來體育台.md "wikilink")）
      - [張致平](../Page/張致平.md "wikilink")（現[FOX體育台副總經理](../Page/FOX體育台.md "wikilink")）
      - [徐展元](../Page/徐展元.md "wikilink")（現[博斯無限台台長](../Page/博斯無限台.md "wikilink")）
      - [林煒珽](../Page/林煒珽.md "wikilink")（現[FOX體育台主播](../Page/FOX體育台.md "wikilink")）
      - [林志隆](../Page/林志隆.md "wikilink")（現[愛爾達體育台球評及udn](../Page/愛爾達體育台.md "wikilink")
        tv新聞台體育中心主任）
      - [俞聖律](../Page/俞聖律.md "wikilink")（現[蘋果日報記者](../Page/蘋果日報.md "wikilink")）
      - [丁元凱](../Page/丁元凱\(主播\).md "wikilink")（現[東森新聞台記者](../Page/東森新聞台.md "wikilink")）
      - [陳怡安](../Page/陳怡安.md "wikilink")（1988年[漢城奧運](../Page/漢城奧運.md "wikilink")[跆拳道金牌](../Page/跆拳道.md "wikilink")）

### ERA SPORTS 年代體育臺時期

  - 時間：1998年6月1日至2000年
      - [錢定遠](../Page/錢定遠.md "wikilink")（現[博斯運動頻道主播及顧問](../Page/博斯運動頻道.md "wikilink")）
      - [蔣佩棻](../Page/蔣佩棻.md "wikilink")（曾任[緯來體育臺體育新聞主播](../Page/緯來體育台.md "wikilink")、[TVBS新聞台主播](../Page/TVBS.md "wikilink")）

### MUCH TV時期

  - 時間：2000年至2003年4月
  - 轉播：[臺灣大聯盟四年至六年](../Page/台灣大聯盟.md "wikilink")、[中華職棒十四年賽程](../Page/中華職棒.md "wikilink")、[第37屆金馬獎頒獎典禮](../Page/第37屆金馬獎.md "wikilink")
  - 取得[2002年世界盃足球賽轉播權](../Page/2002年世界盃足球賽.md "wikilink")。

### 年代MUCH台（2003年4月至2005年）

  - 繼續轉播[中華職棒賽事](../Page/中華職棒.md "wikilink")。
  - 由於政論主持人**[汪笨湖](../Page/汪笨湖.md "wikilink")**入股年代的關係，開始大量製播[閩南語節目](../Page/閩南語.md "wikilink")，知名節目為《**[臺灣心聲](../Page/臺灣心聲.md "wikilink")**》。
  - 2004年9月1日起，該臺嘗試使用[臺語播報臺灣股市即時看盤節目](../Page/臺語.md "wikilink")，節目名稱為《**[股市錢潮](../Page/股市錢潮.md "wikilink")**》，主播群為**[陳書賢](../Page/陳書賢.md "wikilink")**、**[陳斐娟](../Page/陳斐娟.md "wikilink")**、**[黃靜雪](../Page/黃靜雪.md "wikilink")**、**[林志冠](../Page/林志冠.md "wikilink")**。
  - 自2005年起，為因應[行政院新聞局的](../Page/行政院新聞局.md "wikilink")**同類節目區塊定頻政策**，從原先的第45頻道搬移至第38頻道，原**[年代新聞臺](../Page/年代新聞臺.md "wikilink")**於臺灣股市開盤時的國語股市看盤節目《**[股市線上](../Page/股市線上.md "wikilink")**》亦搬移過來，《股市錢潮》則停播。
  - 2005年不再轉播[中華職棒賽事](../Page/中華職棒.md "wikilink")，[錢定遠和](../Page/錢定遠.md "wikilink")[徐展元離開該台](../Page/徐展元.md "wikilink")。[徐展元後來進入](../Page/徐展元.md "wikilink")[緯來體育台](../Page/緯來體育台.md "wikilink")，任職至2014年3月。

### 年代MUCH台（2006年起）

  - 2005年底，汪笨湖離開年代電視，頻道節目因此全面改版，臺名雖維持**年代MUCH臺**不變，但更改[標識形式](../Page/標識.md "wikilink")
  - 取得[2006年世界盃足球賽臺灣唯一轉播權](../Page/2006年世界盃足球賽.md "wikilink")。
  - 2009年8月31日：年代MUCH臺《[股市線上](../Page/股市線上.md "wikilink")》停播，三位主持人[陳書賢](../Page/陳書賢.md "wikilink")、[李傳慧](../Page/李傳慧.md "wikilink")、[黃洛婷去職](../Page/黃洛婷.md "wikilink")。根據《[經濟日報](../Page/經濟日報_\(臺灣\).md "wikilink")》8月19日報導指出，有關股市盤中的即時節目，只剩下[非凡商業台](../Page/非凡商業台.md "wikilink")、[東森財經新聞台](../Page/東森財經新聞台.md "wikilink")；該報分析，東森財經新聞台的崛起，挖走不少原本年代的[後製團隊](../Page/後製.md "wikilink")、主播，甚至連市場知名的分析師也被迫選邊站；非凡盤中節目則固守地盤，導致年代在同時段的財經節目[市占率只剩下](../Page/市占率.md "wikilink")15%，收視不佳，不得不吹熄燈號。
  - 取得[2010年世界杯足球賽臺灣唯一轉播權](../Page/2010年世界杯足球賽.md "wikilink")。
  - 取得[2014年世界杯足球賽臺灣有線電視獨家轉播權](../Page/2014年世界杯足球賽.md "wikilink")，由於合約爭議經法院裁示，2014年世界杯足球賽於6月27日起，台灣總代理[愛爾達電視正式停止傳送訊號予年代電視](../Page/愛爾達電視.md "wikilink")，並解除轉播合約。
  - 2015年9月25日起，啟用高畫質版本「**年代MUCH台[HD](../Page/高解析度電視.md "wikilink")**」。
  - 2015年11月27日上午08:00降回[SD畫質播出](../Page/標準畫質電視.md "wikilink"),同集團的[年代新聞](../Page/年代新聞.md "wikilink")，[壹新聞也同時降回](../Page/壹電視新聞台.md "wikilink")[SD畫質播出](../Page/標準畫質電視.md "wikilink")。
  - 2016年3月11日恢復為HD高畫質播出，同集團[年代新聞](../Page/年代新聞.md "wikilink")，[壹新聞](../Page/壹新聞.md "wikilink")，類比訊號為SD畫質播出。

## 節目

### 播出中

### 綜藝

<table>
<thead>
<tr class="header">
<th><p>節目名稱</p></th>
<th><p>播出時間</p></th>
<th><p>主持人</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p>《<a href="../Page/健康好生活.md" title="wikilink">健康好生活</a>》</p></td>
<td><p>週一至週五 18:00 ~ 19:00</p></td>
<td><p><a href="../Page/陳凝觀.md" title="wikilink">陳凝觀</a></p></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/荳荳快樂學堂.md" title="wikilink">荳荳快樂學堂</a>》</p></td>
<td><p>週六、日 09:00 ~ 09:30</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>《動物方城市》</p></td>
<td><p>週六18:00 ~ 19:00</p></td>
<td><p>|<a href="../Page/李李仁.md" title="wikilink">李李仁</a>、<a href="../Page/朱蕾安.md" title="wikilink">朱蕾安</a></p></td>
</tr>
</tbody>
</table>

### 戲劇

  - 中國大陸

<table>
<thead>
<tr class="header">
<th><p>中國大陸名稱</p></th>
<th><p>播出時間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>《新上海灘》</p></td>
<td><p>週一至週五 20：00~21：00</p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [年代MUCH台](http://muchtv.eracom.com.tw/)

  -
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:年代MUCH台](../Category/年代MUCH台.md "wikilink")
[Category:年代電視旗下頻道](../Category/年代電視旗下頻道.md "wikilink")
[Category:年代電視](../Category/年代電視.md "wikilink")