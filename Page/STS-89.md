****是历史上第八十八次航天飞机任务，也是[奋进号航天飞机的第十二次太空飞行](../Page/奮進號太空梭.md "wikilink")。

## 任务成员

  - **[泰伦斯·威尔卡特](../Page/泰伦斯·威尔卡特.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[乔·爱德华兹](../Page/乔·爱德华兹.md "wikilink")**（，曾执行任务），飞行员
  - **[波妮·唐巴尔](../Page/波妮·唐巴尔.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[迈克尔·安德森](../Page/迈克尔·安德森.md "wikilink")**（，曾执行以及任务），任务专家
  - **[詹姆斯·莱利](../Page/詹姆斯·莱利.md "wikilink")**（，曾执行、、任务），任务专家
  - **[萨里占·沙里波夫](../Page/萨里占·沙里波夫.md "wikilink")**（，[俄罗斯宇航员](../Page/俄罗斯.md "wikilink")，曾执行、[联盟TMA-5](../Page/联盟TMA-5.md "wikilink")、[远征10号](../Page/远征10号.md "wikilink")、[联盟TMA-13以及](../Page/联盟TMA-13.md "wikilink")[远征18号任务](../Page/远征18号.md "wikilink")），任务专家

### 发射后停留在和平号空间站

  - **[安德鲁·托马斯](../Page/安德鲁·托马斯.md "wikilink")**（，曾执行、、[和平号](../Page/和平號太空站.md "wikilink")、、以及任务），任务专家

### 从和平号空间站返回

  - **[大卫·沃尔夫](../Page/大卫·沃尔夫.md "wikilink")**（，曾执行、以及任务），任务专家

[Category:1998年佛罗里达州](../Category/1998年佛罗里达州.md "wikilink")
[Category:奋进号航天飞机任务](../Category/奋进号航天飞机任务.md "wikilink")
[Category:1998年科學](../Category/1998年科學.md "wikilink")
[Category:1998年1月](../Category/1998年1月.md "wikilink")