**洛杉矶县**（），简称**洛县**，[美国](../Page/美国.md "wikilink")[加利福尼亚州的一个县](../Page/加利福尼亚州.md "wikilink")，也是[美国人口最多的县](../Page/美国人口.md "wikilink")，其县治為[洛杉矶](../Page/洛杉矶.md "wikilink")\[1\]。根据[2010年人口普查顯示](../Page/美國2010年人口普查.md "wikilink")，洛杉矶县共有人口9,818,605人，其中[歐裔美國人占](../Page/歐裔美國人.md "wikilink")48.71%、[亚裔美国人占](../Page/亚裔美国人.md "wikilink")11.95%、[非裔美国人占](../Page/非裔美国人.md "wikilink")9.78%。其总面积为4,752.32平方英里，约12,308平方公里，是[全國面積第74大的縣份](../Page/美國面積最大縣份列表.md "wikilink")。

洛杉矶县擁有接近1千萬人口，比美國人口最少的42個[州份的人口都多](../Page/美國州份.md "wikilink")。洛杉矶县的縣治[洛杉矶是洛杉矶县和加州的最大城市](../Page/洛杉矶.md "wikilink")，亦是美國第二大城市（僅次於[紐約市](../Page/紐約市.md "wikilink")）。

洛杉矶县亦擁有兩個島嶼，分別為[聖克利門蒂島和](../Page/聖克利門蒂島.md "wikilink")[圣卡塔利娜岛](../Page/圣卡塔利娜岛.md "wikilink")。洛杉矶县擁有高達88個[城市](../Page/城市.md "wikilink")。洛杉矶县的面積為，其領土比較[罗得岛州和](../Page/罗得岛州.md "wikilink")[特拉华州合起來的面積還要大](../Page/特拉华州.md "wikilink")。

洛杉矶县擁有逾1/4加州居民，是加州民族最多樣化的縣份。\[2\]

## 地理

[Los_Angeles,_CA_from_the_air.jpg](https://zh.wikipedia.org/wiki/File:Los_Angeles,_CA_from_the_air.jpg "fig:Los_Angeles,_CA_from_the_air.jpg")
[L.A._County_Fair_at_Dusk.JPG](https://zh.wikipedia.org/wiki/File:L.A._County_Fair_at_Dusk.JPG "fig:L.A._County_Fair_at_Dusk.JPG")
[Lightmatter_disneyhall5.jpg](https://zh.wikipedia.org/wiki/File:Lightmatter_disneyhall5.jpg "fig:Lightmatter_disneyhall5.jpg")
根據[2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，洛杉矶县總面積為。其中有，即85.45%為陸地；，即14.55%為水域。\[3\]洛杉矶县的[海岸線長達](../Page/海岸線.md "wikilink")。

### 十二大城市

| 城市                                              | 人口        |
| ----------------------------------------------- | --------- |
| [洛杉矶](../Page/洛杉矶.md "wikilink")                | 3,847,400 |
| [長滩](../Page/長滩_\(加利福尼亚州\).md "wikilink")       | 461,522   |
| [格蘭岱尔](../Page/格倫代爾_\(加利福尼亞州\).md "wikilink")   | 205,300   |
| [聖塔克拉利塔](../Page/聖塔克拉利塔.md "wikilink")          | 151,088   |
| [波莫纳](../Page/波莫纳_\(加利福尼亚州\).md "wikilink")     | 149,473   |
| [棕櫚谷](../Page/棕櫚谷_\(加利福尼亚州\).md "wikilink")     | 143,227   |
| [托倫斯](../Page/托倫斯_\(加利福尼亚州\).md "wikilink")     | 137,946   |
| [帕萨迪纳](../Page/帕萨迪纳_\(加利福尼亚州\).md "wikilink")   | 133,936   |
| [兰卡斯特](../Page/蘭開斯特_\(加利福尼亞州\).md "wikilink")   | 129,019   |
| [艾尔蒙地](../Page/艾尔蒙地_\(加利福尼亚州\).md "wikilink")   | 119,900   |
| [英格尔伍德](../Page/英格尔伍德_\(加利福尼亚州\).md "wikilink") | 114,914   |
| 唐尼                                              | 107,323   |

### 城市

1.  [阿古拉山／阿格拉山](../Page/阿古拉山_\(加利福尼亚州\).md "wikilink")（Agoura Hills）
2.  [阿罕布拉／阿尔汉布拉](../Page/阿罕布拉.md "wikilink")（Alhambra）
3.  [亚凯迪亚／阿卡迪亚](../Page/亚凯迪亚.md "wikilink")（Arcadia）
4.  [阿蒂西亚／阿提夏](../Page/阿蒂西亚_\(加利福尼亚州\).md "wikilink")（Artesia）
5.  [阿瓦隆](../Page/阿瓦隆_\(加利福尼亚州\).md "wikilink")（Avalon）
6.  [阿苏萨](../Page/阿苏萨_\(加利福尼亚州\).md "wikilink")（Azusa）
7.  [鲍德温公园](../Page/鲍德温公园_\(加利福尼亚州\).md "wikilink")（Baldwin Park）
8.  [贝尔／风铃市](../Page/贝尔_\(加利福尼亚州\).md "wikilink")（Bell）
9.  [贝尔花园／风铃花园](../Page/贝尔花园.md "wikilink")（Bell Gardens）
10. [贝尔弗劳尔／风铃草](../Page/贝尔弗劳尔_\(加利福尼亚州\).md "wikilink")（Bellflower）
11. [比佛利山／比华利山](../Page/比佛利山_\(加利福尼亞州\).md "wikilink")（Beverly Hills）
12. [白普理／布雷德伯里](../Page/白普理_\(加利福尼亚州\).md "wikilink")（Bradbury）
13. [伯班克](../Page/伯班克_\(加利福尼亚州\).md "wikilink")（Burbank）
14. [卡拉巴萨斯](../Page/卡拉巴萨斯.md "wikilink")（Calabasas）
15. [卡森](../Page/卡森_\(加利福尼亚州\).md "wikilink")（Carson）
16. [喜瑞都](../Page/喜瑞都.md "wikilink")（Cerritos）
17. [克莱蒙特](../Page/克莱蒙特_\(加利福尼亚州\).md "wikilink")（Claremont）
18. [科默斯／康墨斯／商业市](../Page/科默斯_\(加利福尼亚州\).md "wikilink")（Commerce）
19. [康普頓](../Page/康普顿_\(加利福尼亚州\).md "wikilink")（Compton）
20. [科维纳／柯汶那](../Page/柯汶那.md "wikilink")（Covina）
21. [卡德希](../Page/卡德希_\(加利福尼亚州\).md "wikilink")（Cudahy）
22. [卡尔弗城](../Page/卡尔弗城_\(加利福尼亚州\).md "wikilink")（Culver City）
23. [钻石吧／戴蒙德吧](../Page/钻石吧_\(加利福尼亚州\).md "wikilink")（Diamond Bar）
24. [唐尼／丹尼](../Page/唐尼_\(加利福尼亞州\).md "wikilink")（Downey）
25. [杜阿尔特／杜阿提](../Page/杜阿尔特_\(加利福尼亚州\).md "wikilink")（Duarte）
26. [埃尔蒙地／艾尔蒙地](../Page/艾尔蒙地.md "wikilink")（El Monte）
27. [埃尔塞贡多](../Page/埃尔塞贡多_\(加利福尼亚州\).md "wikilink")（El Segundo）
28. [加迪纳](../Page/加迪纳_\(加利福尼亚州\).md "wikilink")（Gardena）
29. [格伦代尔](../Page/格伦代尔_\(加利福尼亚州\).md "wikilink")（Glendale）
30. [格兰多拉](../Page/格伦多拉_\(加利福尼亚州\).md "wikilink")（Glendora）
31. [夏威夷花园](../Page/夏威夷花园_\(加利福尼亚州\).md "wikilink")（Hawaiian Gardens）
32. [霍桑](../Page/霍桑_\(加利福尼亚州\).md "wikilink")（Hawthorne）
33. [何尔摩沙滩](../Page/何尔摩沙海滩_\(加利福尼亚州\).md "wikilink")（Hermosa Beach）
34. [隐山](../Page/隐山_\(加利福尼亚州\).md "wikilink")（Hidden Hills）
35. [亨廷顿公园](../Page/亨廷顿公园_\(加利福尼亚州\).md "wikilink")（Huntington Park）
36. [因达斯特里／工业市](../Page/工業市_\(加利福尼亞州\).md "wikilink")（Industry）
37. [英格尔伍德](../Page/英格尔伍德_\(加利福尼亚州\).md "wikilink")（Inglewood）
38. [欧文代尔](../Page/欧文代尔.md "wikilink")（Irwindale）
39. [拉肯亚达─弗林楚奇／拉肯纳达石岭](../Page/拉肯亚达─弗林楚奇.md "wikilink")（La Cañada
    Flintridge）
40. [拉哈布拉高地／拉哈布拉岗](../Page/拉哈布拉高地_\(加利福尼亚州\).md "wikilink")（La Habra
    Heights）
41. [拉米拉达](../Page/拉米拉达_\(加利福尼亚州\).md "wikilink")（La Mirada）
42. [拉蓬特](../Page/拉蓬特.md "wikilink")（La Puente）
43. [拉文](../Page/拉文.md "wikilink")（La Verne）
44. [莱克伍德](../Page/莱克伍德_\(加利福尼亚州\).md "wikilink")（Lakewood）
45. [兰开斯特](../Page/兰开斯特_\(加利福尼亚州\).md "wikilink")（Lancaster）
46. [朗代尔](../Page/朗代尔.md "wikilink")（Lawndale）
47. [洛米塔](../Page/洛米塔.md "wikilink")（Lomita）
48. [长滩／长堤／朗比奇](../Page/长滩_\(加利福尼亚州\).md "wikilink")（Long Beach）
49. [洛杉矶](../Page/洛杉矶.md "wikilink")（Los Angeles）
50. [林伍德](../Page/林伍德_\(加利福尼亚州\).md "wikilink")（Lynwood）
51. [马利布](../Page/马里布_\(加利福尼亚州\).md "wikilink")（Malibu）
52. [曼哈顿滩](../Page/曼哈顿海滩_\(加利福尼亚州\).md "wikilink")（Manhattan Beach）
53. [梅伍德](../Page/梅伍德_\(加利福尼亚州\).md "wikilink")（Maywood）
54. [蒙罗维亚](../Page/蒙罗维亚_\(加利福尼亚州\).md "wikilink")（Monrovia）
55. [蒙提贝罗／蒙提贝罗](../Page/蒙提贝罗.md "wikilink")（Montebello）
56. [蒙特利公园／蒙市](../Page/蒙特利公园.md "wikilink")（Monterey Park）
57. [诺沃克](../Page/诺沃克_\(加利福尼亚州\).md "wikilink")（Norwalk）
58. [帕姆代尔](../Page/棕榈谷_\(加利福尼亚州\).md "wikilink")（Palmdale）
59. [帕洛斯弗迪斯庄园](../Page/帕洛斯弗迪斯庄园.md "wikilink")（Palos Verdes Estates）
60. [派拉蒙](../Page/派拉蒙_\(加利福尼亚州\).md "wikilink")（Paramount）
61. [帕萨迪纳](../Page/帕萨迪纳_\(加利福尼亚州\).md "wikilink")（Pasadena）
62. [皮科里韦拉](../Page/皮科里韦拉.md "wikilink")（Pico Rivera）
63. [波莫纳](../Page/波莫纳_\(加利福尼亚州\).md "wikilink")（Pomona）
64. [兰乔帕洛斯弗迪斯](../Page/兰乔帕洛斯弗迪斯.md "wikilink")（Rancho Palos Verdes）
65. [雷东多滩](../Page/雷东多海滩_\(加利福尼亚州\).md "wikilink")（Redondo Beach）
66. [罗灵山／绵延山](../Page/罗灵丘陵_\(加利福尼亚州\).md "wikilink")（Rolling Hills）
67. [罗灵山庄园／绵延山庄园](../Page/罗灵丘陵庄园_\(加利福尼亚州\).md "wikilink")（Rolling Hills
    Estates）
68. [柔似蜜／罗斯米德](../Page/柔似蜜.md "wikilink")（Rosemead）
69. [圣迪马斯](../Page/圣迪马斯_\(加利福尼亚州\).md "wikilink")（San Dimas）
70. [圣费尔南多](../Page/圣费尔南多_\(加利福尼亚州\).md "wikilink")（San Fernando）
71. [圣盖博／圣加布里埃尔／圣加百列](../Page/圣盖博.md "wikilink")（San Gabriel）
72. [圣玛利诺／圣马力诺](../Page/圣玛利诺_\(加利福尼亚州\).md "wikilink")（San Marino）
73. [圣塔克拉里塔／圣克拉里塔](../Page/圣塔克拉利塔.md "wikilink")（Santa Clarita）
74. [圣塔菲斯普林斯／圣菲斯普林斯](../Page/圣菲斯普林斯_\(加利福尼亚州\).md "wikilink")（Santa Fe
    Springs）
75. [圣塔莫尼卡／圣莫尼卡](../Page/圣莫尼卡.md "wikilink")（Santa Monica）
76. [谢拉马德雷](../Page/马德雷_\(加利福尼亚州\).md "wikilink")（Sierra Madre）
77. [信号山／斯格纳山](../Page/信号山_\(加利福尼亚州\).md "wikilink")（Signal Hill）
78. [南埃尔蒙地／南艾尔蒙地](../Page/南艾尔蒙地.md "wikilink")（South El Monte）
79. [南门／南盖特](../Page/南门_\(加利福尼亚州\).md "wikilink")（South Gate）
80. [南帕萨迪纳](../Page/南帕萨迪纳_\(加利福尼亚州\).md "wikilink")（South Pasadena）
81. [天普城／天普市／坦普尔城](../Page/天普市.md "wikilink")（Temple City）
82. [托伦斯](../Page/托伦斯.md "wikilink")（Torrance）
83. [弗农](../Page/弗农_\(加利福尼亚州\).md "wikilink")（Vernon）
84. [沃尔纳特／核桃市](../Page/沃尔纳特_\(加利福尼亚州\).md "wikilink")（Walnut）
85. [西科维纳／西柯汶那](../Page/西柯汶那.md "wikilink")（West Covina）
86. [西好莱坞／西荷里活](../Page/西好莱坞_\(加利福尼亚州\).md "wikilink")（West Hollywood）
87. [西湖村](../Page/西湖村_\(加利福尼亚州\).md "wikilink")（Westlake Village）
88. [惠提尔／惠蒂尔](../Page/惠提尔.md "wikilink")（Whittier）

## 人口

### 2000年

[LACountyPopDensity.png](https://zh.wikipedia.org/wiki/File:LACountyPopDensity.png "fig:LACountyPopDensity.png")
根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，洛杉矶县擁有9,519,338居民、3,133,774住戶和2,137,233家庭。\[4\]。其[人口密度為每平方英里](../Page/人口密度.md "wikilink")2,344居民（每平方公里905居民）。洛杉矶县擁有3,270,909間房屋单位，其密度為每平方英里806間（每平方公里311間）。\[5\]洛杉矶县的人口是由48.7%[白人](../Page/歐裔美國人.md "wikilink")\[6\]\[7\]、11.0%[黑人](../Page/非裔美國人.md "wikilink")、0.8%[美國原住民](../Page/美國原住民.md "wikilink")、10.0%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.3%[太平洋岛民](../Page/太平洋岛民.md "wikilink")、23.5%其他[種族和](../Page/種族.md "wikilink")4.9%[混血兒組成](../Page/混血兒.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")44.6%。在[白人當中](../Page/歐裔美國人.md "wikilink")，有6%為[德國人](../Page/德國人.md "wikilink")、5%為[愛爾蘭人](../Page/愛爾蘭人.md "wikilink")、4%為[英國人以及](../Page/英國人.md "wikilink")3%為[意大利人](../Page/意大利人.md "wikilink")。於9,519,338居民當中，有45.9%母語為[英語](../Page/英語.md "wikilink")、37.9%為[西班牙語](../Page/西班牙語.md "wikilink")、2.22%為[他加祿語](../Page/他加祿語.md "wikilink")、2.0%為[中文](../Page/中文.md "wikilink")、1.9%為[韓文](../Page/韓文.md "wikilink")、以及1.87%為[亚美尼亚语](../Page/亚美尼亚语.md "wikilink")。\[8\]

洛杉矶县亦是整個美國中土著人口多的縣份。根據2000年人口普查，洛杉矶县擁有153,550土著，並且大部份來自[拉丁美洲](../Page/拉丁美洲.md "wikilink")。\[9\]

在3,133,774住户中，有36.80%擁有一個或以上的兒童（18歲以下）、47.6%為夫妻、14.7%為單親家庭、而有31.8%為非家庭。其中24.6%住户為獨居，7.1%為同居長者。平均每戶有2.98人，而平均每個家庭則有3.61人。在9,519,338居民中，有28.0%為18歲以下、10.3%為18至24歲、32.6%為25至44歲、19.4%為45至64歲以及9.7%為65歲以上。人口的年齡中位數為32歲，每100個女性就有97.7個男性。而每100個成年女性（18歲以上）則有95.0個成年男性。\[10\]

洛杉矶县的住戶收入中位數為$42,189，而家庭收入中位數則為$46,452。男性的收入中位數為$36,299，而女性的收入中位數則為$30,981。洛杉矶县的[人均收入為](../Page/人均收入.md "wikilink")$20,683。約14.4%家庭和17.9%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")。\[11\]

根據2007[泰勒尼尔森索福瑞金融服务統計](../Page/泰勒尼尔森索福瑞.md "wikilink")，洛杉矶县擁有全美最多百萬富豪，總計為261,081住戶。\[12\]另一方面，洛杉矶县亦擁有全美最多流浪漢，總計有48,000人，其中包括6,000位[退伍军人](../Page/退伍军人.md "wikilink")。\[13\]

### 2010年

根據[2010年人口普查](../Page/2010年美國人口普查.md "wikilink")，洛杉矶县擁有人口9,818,605人。其人口是由4,936,599（50.3%）[白人](../Page/歐裔美國人.md "wikilink")、856,874（8.7%）[黑人](../Page/非裔美國人.md "wikilink")、72,828（0.7%）[美國原住民](../Page/美國原住民.md "wikilink")、1,346,865（13.7%）[亞洲人](../Page/亞裔美國人.md "wikilink")（4.0%[華人](../Page/華人.md "wikilink")、3.3%[菲律賓人](../Page/菲律賓人.md "wikilink")、2.2%[韓國人](../Page/韓國人.md "wikilink")、1.0%[日本人](../Page/日本人.md "wikilink")、0.9%[越南人](../Page/越南人.md "wikilink")、0.8%[印度人](../Page/印度人.md "wikilink")、0.3%[柬埔寨人](../Page/柬埔寨人.md "wikilink")、0.3%[泰國人以及](../Page/泰國人.md "wikilink")0.1%[巴基斯坦人](../Page/巴基斯坦人.md "wikilink")）、26,094（0.3%）[太平洋岛民](../Page/太平洋岛民.md "wikilink")（0.1%[萨摩亚人](../Page/萨摩亚人.md "wikilink")）、2,140,632（21.8%）其他[種族以及](../Page/種族.md "wikilink")438,713（4.5%）[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。非[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人的白人佔有總人口的](../Page/拉丁美洲人.md "wikilink")27.8%\[14\]。另一方面，[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人有](../Page/拉丁美洲人.md "wikilink")4,687,889人（47.7%），其中有35.8%居民為[墨西哥人](../Page/墨西哥人.md "wikilink")、3.7%[萨尔瓦多人](../Page/萨尔瓦多人.md "wikilink")、2.2%[危地马拉人](../Page/危地马拉人.md "wikilink")、0.5%[波多黎各人](../Page/波多黎各人.md "wikilink")、0.4%[古巴人](../Page/古巴人.md "wikilink")、0.4%[洪都拉斯人](../Page/洪都拉斯人.md "wikilink")、0.4%[尼加拉瓜人](../Page/尼加拉瓜人.md "wikilink")、0.3%[秘鲁人](../Page/秘鲁人.md "wikilink")、0.3%[哥伦比亚人](../Page/哥伦比亚人.md "wikilink")、以及0.2%[厄瓜多尔人](../Page/厄瓜多尔人.md "wikilink")。\[15\]

## 參考文獻

## 外部連結

[L](../Category/加利福尼亞州行政區劃.md "wikilink")
[\*](../Category/洛杉矶县.md "wikilink")
[洛杉矶县城市](../Category/洛杉矶县城市.md "wikilink")
[Category:大洛杉矶地区](../Category/大洛杉矶地区.md "wikilink")
[Category:南加利福尼亚州行政区划](../Category/南加利福尼亚州行政区划.md "wikilink")

1.  [Los Angeles County
    History](http://www.lacounty.gov/wps/portal/!ut/p/c5/7c-xDoIwEMbxZ_EBSI8WKoxEsTRBYhOJyGIY1BClMBif3xoWF2Rw9Lsbbvjnl6asZm5t82yvzaPtbXNnFavlKaJxeJTvBfHcxEW5S0jp0PXjZw_MxvU0KnUoOBk5ow_v96b91v_eTTjT5dhpYhJiRdZ3Zzb3b7VKA-LrpRJZGfukBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBATEH4qhq6jVntddBitrnJ_OLVm8ADLRNk8!/dl3/d3/L2dJQSEvUUt3QS9ZQnZ3LzZfODAwMDAwMDAyT01RNjAyTExKRUdSNzMwVjc!/?WCM_GLOBAL_CONTEXT=/wps/wcm/connect/lacounty+content/lacounty+site/home/government/about+la+county/categorylink_history)

2.

3.

4.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

5.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

6.  [2000 Census fact sheet
    table](http://factfinder.census.gov/servlet/DTTable?_bm=y&-context=dt&-reg=DEC_2000_SF2_U_PCT007:001%7C004%7C014%7C063%7C064%7C066%7C068%7C229%7C235%7C405%7C406%7C413%7C423%7C424%7C453;&-ds_name=DEC_2000_SF3_U&-CONTEXT=dt&-mt_name=DEC_2000_SF3_U_PCT019&-mt_name=DEC_2000_SF3_U_PCT018&-tree_id=403&-redoLog=true&-all_geo_types=N&-_caller=geoselect&-geo_id=05000US06037&-geo_id=05000US36027&-geo_id=05000US36079&-search_results=01000US&-format=&-_lang=en)

7.  [Census.gov](http://www.census.gov/prod/2001pubs/c2kbr01-1.pdf)

8.

9.  ["The Invisible
    Minority"](http://www.indiancountrytoday.com/archive/68873882.html),
    *Indian Country Today*, 2009-11-09

10.

11.
12.

13.

14.

15. ["2010 Census P.L. 94-171 Summary File Data". United States Census
    Bureau.](http://www2.census.gov/census_2010/01-Redistricting_File--PL_94-171/California/)