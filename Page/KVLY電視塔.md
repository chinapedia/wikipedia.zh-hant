**KVLY 電視塔**（[英語](../Page/英語.md "wikilink")：**KVLY-TV mast**），前身為
**KTHI電視塔**（[英語](../Page/英語.md "wikilink")：**KTHI-TV
mast**）是一個在[美國](../Page/美國.md "wikilink")[北達科塔州](../Page/北達科塔州.md "wikilink")[特雷尔郡的電視傳送塔](../Page/特雷尔县_\(北达科他州\).md "wikilink")，使用[北達科塔州](../Page/北達科塔州.md "wikilink")[法戈](../Page/法戈_\(北达科他州\).md "wikilink")[KVLY第](../Page/KVLY-TV.md "wikilink")
11 號頻道。電視塔高 628.8
米，曾是全世界由地面上算起最高的人造建構物（1963年－1974年，1991年－2008年），塔中有[升降機可以直達塔尖](../Page/升降機.md "wikilink")。

## 簡介

這電視塔距離[布蘭切特西邊約](../Page/布蘭切特.md "wikilink") 3
里，約在[佛哥及](../Page/佛哥.md "wikilink")[大福克斯兩地的中間](../Page/大福克斯_\(北達科他州\).md "wikilink")。它在1963年建成的時候曾是世界最高的人造結構，但在1974年，[華沙電台廣播塔建成](../Page/華沙電台廣播塔.md "wikilink")，以
18
米高度之距成為世界第一；可是它在1991年倒塌，而美國的KVLY塔則重新奪回首位，直至2008年被[阿拉伯聯合酋長國](../Page/阿拉伯聯合酋長國.md "wikilink")[迪拜的](../Page/迪拜.md "wikilink")[哈利法塔](../Page/哈利法塔.md "wikilink")（當時
629 米，建成後為 828 米）取代。

該塔是由[漢密爾頓監督所及](../Page/漢密爾頓監督所.md "wikilink")[克萊恩鋼鐵公司興建](../Page/克萊恩鋼鐵公司.md "wikilink")，用了
30 天完成，而當時的造價則為 $500,000 美元（換算為現在的 $320 萬美元）。

該塔現在是由[俾斯麥市北達科塔電視台所擁有](../Page/俾斯麥_\(北達科塔州\).md "wikilink")，用以播放[KVLY第](../Page/KVLY.md "wikilink")
11 頻道；它的播放功率為 316 [千瓦](../Page/千瓦.md "wikilink")，廣播範圍則約 78,000 平方公里。

在該塔完成一段時間後，[聯邦航空管理局以塔的高度](../Page/聯邦航空管理局.md "wikilink")（628.8米）作為日後天线塔高度的一般限制\[1\]。

## 参见

  - [KXJB電視塔](../Page/KXJB電視塔.md "wikilink")
  - [華沙電台廣播塔](../Page/華沙電台廣播塔.md "wikilink")
  - [哈里發塔](../Page/哈里發塔.md "wikilink")
  - [世界最高結構物列表](../Page/世界最高結構物列表.md "wikilink")

## 参考资料

## 外部連結

  - [Structurae: KVLY
    Tower](http://en.structurae.de/structures/data/index.cfm?ID=s0000675)
  - [Tower web page at
    KVLY-TV](https://web.archive.org/web/20081009185656/http://www.valleynewslive.tv/info/info_tower.html)
  - [FCC
    listing](http://wireless2.fcc.gov/UlsApp/AsrSearch/asrRegistration.jsp?regKey=608746)
  - [Listing on the Skyscraper
    Page](http://www.skyscraperpage.com/cities/?buildingID=471)
  - <http://www.skyscraperpage.com/diagrams/?b471>
  - <http://www.pbase.com/talshiarr/kvly>

[Category:美國電視](../Category/美國電視.md "wikilink")
[Category:世界之最](../Category/世界之最.md "wikilink")
[Category:北達科他州建築物](../Category/北達科他州建築物.md "wikilink")
[Category:1963年建造](../Category/1963年建造.md "wikilink")

1.