[Bulletproof_glass_window_after_a_burglary_attempt.jpg](https://zh.wikipedia.org/wiki/File:Bulletproof_glass_window_after_a_burglary_attempt.jpg "fig:Bulletproof_glass_window_after_a_burglary_attempt.jpg")過後，[珠寶店的防彈玻璃窗](../Page/珠寶店.md "wikilink")\]\]

**防弹玻璃**是对[小型武器的射击或棍棒等器具的打擊提供一定程度保护能力的](../Page/小型武器.md "wikilink")[玻璃](../Page/玻璃.md "wikilink")，通常是透明的材料，譬如[聚碳酸酯](../Page/聚碳酸酯.md "wikilink")[热塑性塑料](../Page/热塑性塑料.md "wikilink")（一般为[力显树脂](../Page/力显树脂.md "wikilink")（Lexan））。它具有普通玻璃的外观和传送光的行为。

防弹玻璃通常包括聚碳酸酯纤维层夹在普通玻璃层之中。子弹容易击穿外表的一层玻璃，但坚固的聚碳酸酯纤维层会在它可能击穿玻璃内在的层之前停止子弹的运动。防弹玻璃通常厚达70毫米至75毫米。

历史上，防弹玻璃曾由液体橡胶粘在一起的玻璃板料制成。这些大块的防弹玻璃已经在在第二次世界大战期间做了公共的用途，通常厚达100毫米至120毫米，而且极端重。

## 单向防弹玻璃

譬如一些银行[防弹车的用途促进了单向防弹玻璃的发明](../Page/防弹车.md "wikilink")。这种玻璃将抵抗接踵而来的[小型武器射击玻璃的外侧](../Page/小型武器.md "wikilink")，却会允许在玻璃的对侧（里侧）的射击，譬如防卫者从防弹车的里侧射击，从而通过玻璃射击在外部威胁者上。

单向防弹玻璃通常被制成二层：在外部的易碎的一层和在里面的柔韧的一层。当一个子弹从外部射出时，子弹会先击中易碎的一层，并打碎一个区域。这会吸收一些子弹的[动能](../Page/动能.md "wikilink")，并且在一个更大的区域传播动能。当减慢的子弹击中柔韧的一层时，它就被挡住了。但是，当子弹从里面射出，它会先击中柔韧的一层。子弹能把柔韧的一层击穿是因为它的能量集中于一个小范围，脆的一层在柔韧的那层向外弹时向外碎裂，而且不妨害子弹的前进。

## 行业标准

  - [中国](../Page/中国.md "wikilink")[防弹玻璃行业标准](../Page/防弹玻璃行业标准.md "wikilink")
  - [美国UL752:1995《防弹玻璃》](../Page/美国UL752:1995《防弹玻璃》.md "wikilink")
  - [英国BS 5051:1988《防弹玻璃》](../Page/英国BS_5051:1988《防弹玻璃》.md "wikilink")
  - [ASTM
    F1233:1995《防弹玻璃材料和系统的试验方法》](../Page/ASTM_F1233:1995《防弹玻璃材料和系统的试验方法》.md "wikilink")
  - [国军标防弹玻璃标准--GB-1997](../Page/国军标防弹玻璃标准--GB-1997.md "wikilink")
  - [公安部防弹玻璃标准--GA-165](../Page/公安部防弹玻璃标准--GA-165.md "wikilink")
  - [铁道车辆安全玻璃--GB18045-2000](../Page/铁道车辆安全玻璃--GB18045-2000.md "wikilink")
  - [文物系统博物馆安全防范工程设计规范--GB-T16571—1996](../Page/文物系统博物馆安全防范工程设计规范--GB-T16571—1996.md "wikilink")
  - [夹层玻璃](../Page/夹层玻璃.md "wikilink")
  - [汽车用安全玻璃--GB 9695-1996](../Page/汽车用安全玻璃--GB_9695-1996.md "wikilink")
  - [汽车安全玻璃光学性能试验方法](../Page/汽车安全玻璃光学性能试验方法.md "wikilink")
  - [机车船舶用电加温玻璃](../Page/机车船舶用电加温玻璃.md "wikilink")
  - [NIJ-0108.01（美国标准）](../Page/NIJ-0108.01（美国标准）.md "wikilink")
  - [EUROPEAN Standard（欧标）](../Page/EUROPEAN_Standard（欧标）.md "wikilink")
  - [UL 752](../Page/UL_752.md "wikilink")
  - [German Standard DIN
    52290](../Page/German_Standard_DIN_52290.md "wikilink")
  - [安防行业现行行业标准目录](../Page/安防行业现行行业标准目录.md "wikilink")

[Category:玻璃](../Category/玻璃.md "wikilink")
[Category:裝甲](../Category/裝甲.md "wikilink")
[Category:玻璃涂层与表面改性](../Category/玻璃涂层与表面改性.md "wikilink")