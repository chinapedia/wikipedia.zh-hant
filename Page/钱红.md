**钱红**（），[河北](../Page/河北.md "wikilink")[保定人](../Page/保定.md "wikilink")，原[中国](../Page/中国.md "wikilink")[游泳运动员](../Page/游泳.md "wikilink")。中国泳坛1990年代“五朵金花”辉煌时代成员之一。职业生涯共获得了30个全国冠军、51个世界冠军。被誉为“水蝴蝶”。

## 生涯

钱红9岁开始练习游泳。1988年[汉城奥运会](../Page/1988年夏季奥林匹克运动会.md "wikilink")，初出茅庐的钱红勇夺一枚铜牌。1991年，钱红在[澳大利亚](../Page/澳大利亚.md "wikilink")[珀斯举行的第六届世界游泳锦标赛中夺得](../Page/珀斯.md "wikilink")100米蝶泳冠军，并打破世界纪录。1992年[巴塞罗那奥运会](../Page/1992年夏季奥林匹克运动会.md "wikilink")，钱红于女子100米蝶泳决赛中，冒着窒息的危险，采用五次划水换一次气的方式进行最后冲刺，成功超越对手夺得第一名而获得金牌。\[1\]

钱红于1993年退役，其后定居于[北京](../Page/北京.md "wikilink")。钱红现为商界女强人，创办了一家游泳俱乐部，代理一家国际著名泳装品牌。

## 外部链接

  - [钱红做客搜狐谈奥运
    希望能为中国游泳队做贡献](http://2008.sohu.com/20060808/n244687040.shtml)
  - [【冠军访谈】钱红1](http://sports.cctv.com/20080809/105779.shtml).cctv.2004年
  - [【冠军访谈】钱红2](http://sports.cctv.com/20080809/105823.shtml).cctv.2004年

## 参考

[Hong](../Page/category:钱姓.md "wikilink")

[Category:河北籍游泳运动员](../Category/河北籍游泳运动员.md "wikilink")
[Category:中国奥运游泳运动员](../Category/中国奥运游泳运动员.md "wikilink")
[Category:中国奥林匹克运动会铜牌得主](../Category/中国奥林匹克运动会铜牌得主.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:1988年夏季奧林匹克運動會獎牌得主](../Category/1988年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1992年夏季奧林匹克運動會獎牌得主](../Category/1992年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奧林匹克運動會游泳金牌得主](../Category/奧林匹克運動會游泳金牌得主.md "wikilink")
[Category:奥林匹克运动会游泳铜牌得主](../Category/奥林匹克运动会游泳铜牌得主.md "wikilink")
[Category:1988年夏季奧林匹克運動會游泳運動員](../Category/1988年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:1992年夏季奧林匹克運動會游泳運動員](../Category/1992年夏季奧林匹克運動會游泳運動員.md "wikilink")
[Category:世界游泳錦標賽游泳賽事獎牌得主](../Category/世界游泳錦標賽游泳賽事獎牌得主.md "wikilink")
[Category:亞洲運動會游泳獎牌得主](../Category/亞洲運動會游泳獎牌得主.md "wikilink")
[Category:1986年亞洲運動會金牌得主](../Category/1986年亞洲運動會金牌得主.md "wikilink")
[Category:1986年亞洲運動會銀牌得主](../Category/1986年亞洲運動會銀牌得主.md "wikilink")
[Category:1990年亞洲運動會金牌得主](../Category/1990年亞洲運動會金牌得主.md "wikilink")
[Category:1990年亞洲運動會銀牌得主](../Category/1990年亞洲運動會銀牌得主.md "wikilink")
[Category:保定籍运动员](../Category/保定籍运动员.md "wikilink")

1.  [巴塞罗那奥运会游泳冠军：钱红](http://www.cntv.cn/program/documentary/20040323/101008.shtml).央视国际.2004年03月23日