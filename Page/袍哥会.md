**袍哥会**，即**哥老会**，与[洪门](../Page/洪门.md "wikilink")、[青帮为](../Page/青帮.md "wikilink")[清朝三大](../Page/清朝.md "wikilink")[秘密结社](../Page/秘密结社.md "wikilink")，袍哥会的儀式、章程等，與洪门相當類似，袍哥会在清代初期的四川曾经是少部分人的秘密结社，后期与[湘军裁卒关系密切](../Page/湘军.md "wikilink")，清末成為半公開的組織，在[辛亥革命之后](../Page/辛亥革命.md "wikilink")，它长期成为[四川大多数成年男性都直接加入或间接受其控制的公开性组织](../Page/四川.md "wikilink")。袍哥会对[四川](../Page/四川.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[湖南社会各方面都有极为重要的影响](../Page/湖南.md "wikilink")，这一特点是其他地区未有过的。[邓小平的父亲](../Page/邓小平.md "wikilink")[邓文明就是四川袍哥会的成员](../Page/邓文明.md "wikilink")。

在四川（以下四川均含重庆地区）的哥老会成员被称为**袍哥**，有两种解释，一说是取《[诗经](../Page/诗经.md "wikilink")·无衣》:“与子同袍”之义，表示是同一袍色之哥弟；另一说是袍与胞谐音，表示有如同胞之哥弟。两种解释相差無幾。

## 产生

[啯噜](../Page/啯噜.md "wikilink")，或作[哥老](../Page/哥老.md "wikilink")、[哥佬](../Page/哥佬.md "wikilink")，原是[方言](../Page/方言.md "wikilink")，意指「[结拜](../Page/结拜.md "wikilink")[兄弟](../Page/兄弟.md "wikilink")」或「[賭徒](../Page/賭.md "wikilink")」。是自[清朝](../Page/清朝.md "wikilink")[乾隆以后在](../Page/乾隆.md "wikilink")[四川](../Page/四川.md "wikilink")、[陕南](../Page/陕.md "wikilink")、[湘西](../Page/湘.md "wikilink")、[鄂西](../Page/鄂.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[云南中出现的以劫夺谋生的异姓](../Page/云南.md "wikilink")[结拜团体](../Page/结拜.md "wikilink")；啯噜具有分散性，各群之间没有联系，成员被称为**啯噜子**。
《[清史稿](../Page/清史稿.md "wikilink")》：「[蜀民失業無賴者](../Page/蜀.md "wikilink")，多習拳勇，嗜飲博，寖至劫殺，號為嘓嚕子。」

这些啯噜[游民团伙](../Page/游民.md "wikilink")，主要从事[赌博](../Page/赌博.md "wikilink")、[搶劫](../Page/搶劫.md "wikilink")、[綁票](../Page/綁票.md "wikilink")、[謀杀](../Page/謀杀.md "wikilink")、[强奸](../Page/强奸.md "wikilink")、[人口販賣或者逼良](../Page/人口販賣.md "wikilink")[為娼等各种](../Page/賣淫.md "wikilink")[犯罪活动](../Page/犯罪.md "wikilink")，早期，无明確的[政治目标和](../Page/政治目标.md "wikilink")[宗教信仰](../Page/宗教.md "wikilink")，後期的啯噜受到[洪門传入的](../Page/洪門.md "wikilink")[反清復明思想影響](../Page/反清復明.md "wikilink")，演變為具有[川](../Page/四川.md "wikilink")、[湖特色的](../Page/湖廣.md "wikilink")[天地會次團體](../Page/天地會.md "wikilink")[哥老会](../Page/哥老会.md "wikilink")。

[嘉庆年间](../Page/嘉庆.md "wikilink")，[四川](../Page/四川.md "wikilink")、[湖北等地](../Page/湖北.md "wikilink")[白莲教起义时](../Page/白莲教.md "wikilink")，大批啯噜子加入，有的还成为主力和首领。随着[白莲教起义的失败](../Page/白莲教.md "wikilink")，啯噜在各地都遭到镇压，但仍然在各地活动，还有不少人渗入到清军与[衙役之中](../Page/衙役.md "wikilink")。为了加强他们内部的组织联系，提高其生存、发展和与[官家对抗的能力](../Page/官家.md "wikilink")，他们逐步大量吸收了早已流传入川的[天地会的组织形式与联络办法](../Page/天地会.md "wikilink")，或直接与[天地会徒融为一家](../Page/天地会.md "wikilink")，在[道光年间发展成为](../Page/道光.md "wikilink")[帮会组织](../Page/帮会.md "wikilink")[哥老会](../Page/哥老会.md "wikilink")。

哥老会成立的标志的开山堂，即一个哥老会独立组织的成立仪式。最早在[四川的开山堂一般被认为是](../Page/四川.md "wikilink")[清](../Page/清.md "wikilink")[道光二十八年](../Page/道光.md "wikilink")（1848年）[永宁](../Page/永宁.md "wikilink")（今[叙永](../Page/叙永.md "wikilink")）[郭永泰开荩忠山](../Page/郭永泰.md "wikilink")，始有山水香堂，以[延平郡王招讨大将军印信相号召](../Page/延平郡王.md "wikilink")。据后来袍哥首领人物的记述：“会盟者即达四千余人，不久荩忠山[兄弟遍及各](../Page/兄弟.md "wikilink")[行省](../Page/行省.md "wikilink")，而开山立堂者尤以[四川为最](../Page/四川.md "wikilink")。”

哥老会产生之后，在各地迅速发展，[中國南方多數省份和](../Page/中國南方.md "wikilink")[中國北方部分省份都有它的组织](../Page/中國北方.md "wikilink")，从总的来看，哥老会的发展有三个特点：

1.  [四川数量最多](../Page/四川.md "wikilink")，川东尤其是[重庆力量最大](../Page/重庆.md "wikilink")。
2.  在[湘军等](../Page/湘军.md "wikilink")[團練营中发展极快](../Page/團練.md "wikilink")。
3.  哥老会虽然分布面广，但仍有很强烈的反清情绪，[咸丰年间爆发的历时](../Page/咸丰.md "wikilink")6年、以[四川为主战场的](../Page/四川.md "wikilink")[李永和](../Page/李永和.md "wikilink")、[蓝朝鼎](../Page/蓝朝鼎.md "wikilink")(另参见[二郎场之戰](../Page/二郎场之戰.md "wikilink"))[李蓝农民起义的基本力量就是哥老会](../Page/李蓝农民起义.md "wikilink")。以后的历次以反洋教为中心的反清[教案的主力也是哥老会](../Page/教案_\(宗教\).md "wikilink")，如“一绅二粮三袍哥”，就是[四川最大教案](../Page/四川.md "wikilink")[大足教案的主力](../Page/大足教案.md "wikilink")。

## 成员

最初的袍哥会成员大多是社会底层人员，[太平天国运动与](../Page/太平天国.md "wikilink")[李蓝起义失败之后](../Page/李蓝起义.md "wikilink")，社会更加动荡，大批的[流民與退伍的](../Page/流民.md "wikilink")[團練軍人加入袍哥会](../Page/團練.md "wikilink")，无钱无势者当袍哥为了求得结援互助。有钱有势的[門閥](../Page/門閥.md "wikilink")[地主](../Page/地主.md "wikilink")，为了控制地方，培植羽翼，也积极加入袍哥队伍。在这种风潮之下，不入袍哥会就很难在社会上立足。

### 五堂與分類

由于参加人数越来越多，袍哥会在[四川的各山堂逐渐形成五个有高低之分的堂口](../Page/四川.md "wikilink")，即仁、义、礼、智、信五个班辈，参加仁字堂者以士绅为多；义字堂以商人为主；礼字堂则较多匪盗、地痞和士兵；智字堂多为贫苦农民、手工业者、船夫、车夫；信字堂人数不多，是下九流者，如卖唱、搓澡之流。[袍哥一般都不以此为职](../Page/袍哥.md "wikilink")，但也有少数组织者或骨干以此为职业。还有一部份乡镇山区的袍哥会完全就是[绿林作风](../Page/绿林.md "wikilink")，占山为王，以抢劫为生，这一类被称为**浑水袍哥**，另外那些不搞盗、抢等以武力掠夺他人财物的袍哥被称为**清水袍哥**。从人数上说清水袍哥占大多数。

### 规模

从[清朝中期开始](../Page/清朝.md "wikilink")[哥老会在](../Page/哥老会.md "wikilink")[四川开山立堂开始](../Page/四川.md "wikilink")，清朝政府都把它视作反清、反社会团体，严加打击，因此人员并不是很多，到了[清朝末期](../Page/清朝.md "wikilink")，因社会动荡且政府对底层的控制力日渐减弱，袍哥会人员迅速增加，最后到了半公开的地步。[民国时期](../Page/民国.md "wikilink")，因袍哥会在四川[保路运动发挥了积极作用](../Page/保路运动.md "wikilink")，再加上[民国初期](../Page/民国.md "wikilink")，时局混乱，袍哥完全成了公开组织。发展到最后的1940年代末，据估计全川成年男子有30-50%参加了袍哥会（也有人估计川东地区，重庆占70-80%）。

## 组织形式

### 称呼

袍哥会的最初的联络的聚点为**山头**或**香堂**，成立一个袍哥会组织就叫**开山立堂**，山头和香堂的名称就作为袍哥会一个组织的称呼，如**莲花山富贵堂**、**[峨眉山顺德堂](../Page/峨眉山.md "wikilink")**。后来随着参加人员的逐步增多，聚会场所的变化，袍哥会的组织又改称为**码头**、**公口**、**社**等。

### 地位

袍哥会没有全国性的组织，甚至没有地区性的组织，每一个袍哥会组织都是独立存在的，没有依存、隶属关系，但会经常有人员联系。因此袍哥会的数量众多，一般一个[乡或](../Page/乡.md "wikilink")[城市的一条](../Page/城市.md "wikilink")[街道就有一个](../Page/街道.md "wikilink")，甚至还更多，如1949年[成都有公口](../Page/成都.md "wikilink")（包括分社、支社）1000多个，[重庆有公口](../Page/重庆.md "wikilink")300多个，然而重庆公口下辖的袍哥组织接近整个街道，总人数接近成都每个公口的10倍，占重庆市区男性居民的2/3之多。

到了[民国后期曾经有些实力较大的袍哥试图进行合并](../Page/民国.md "wikilink")，达到地区性的统一，如曾由[国民政府社会部出面支持](../Page/国民政府.md "wikilink")，重庆袍哥各界成立了**国民自强社**，成都相仿成立了**国民互助社**，但都是只有名义而已，实际仍是独立存在、活动的。

### 内部等级

袍哥组织的内部排行分五个等级，分别称为头排、三排、五排、六排、十排。

  - 头排：头排大哥即舵头，也称舵把子、社长。另有闲位大哥，亦如一般社会组织的名誉理事，多为有声望的人，也有绅、商依靠袍哥关系便于在社会上活动的，他们挂个名，赞助若干钱取得“大爷”资格，俗称绅夹皮。
  - 三排：又称三哥、钱粮。掌管一社经济及经营的商业（如[茶馆](../Page/茶馆.md "wikilink")、[赌场](../Page/赌场.md "wikilink")、[栈房](../Page/栈房.md "wikilink")）。
  - 五排：又称五哥、管事、红旗大管事行交际、执法等职，在袍哥中最有社会力量，不少为职业袍哥，也有绅夹皮五哥、闲五。
  - 六排：副六也可称五哥，是一般成员。[绿林则称蓝旗](../Page/绿林.md "wikilink")，是负责巡风探事的小头领。
  - 十排：统称老么，有凤尾老么、执法老么、跑腿老么之分。凤尾老么是有家资的年轻后生，可“一步登天海大哥”。执法老么多为流氓凶神，袍哥传堂把守辕门，制裁叛徒充当杀手就是此辈。跑腿老么如茶堂馆、[赌场杂务均是](../Page/赌场.md "wikilink")。一般老么则是新人组织者。

排行中无二、四、七、八、九。二是不敢僭越[关帝君](../Page/关帝君.md "wikilink")（即关羽，因為《[三國演義](../Page/三國演義.md "wikilink")》的關係，民间称关羽为关二哥、关二爷）。四和七是[洪門所忌諱的叛徒之數](../Page/洪門.md "wikilink")（[洪門中四](../Page/洪門.md "wikilink")、七兩排只能由女性擔任，已婚的稱四姊，未婚的稱七妹）。八、九忌[杨家将八姐九妹之称](../Page/杨家将.md "wikilink")。

### 三大会

袍哥一年集会三次：最隆重的一次[农历五月十三的](../Page/农历.md "wikilink")**单刀会**（祭[关羽](../Page/关羽.md "wikilink")）。七月称[中元会](../Page/中元.md "wikilink")，为已故弟兄设祭。[腊月称吃](../Page/腊月.md "wikilink")[团年饭](../Page/团年饭.md "wikilink")。实则都是为吸收新成员，商谈一些有关香堂内部内部及对外事务。新参加袍哥得有人引荐，在香堂上行叩拜礼，听舵爷赏封“排行座次”，之后就是袍哥了。

## 历史事件

### 保路运动

1911年5月9日，[清政府接受](../Page/清政府.md "wikilink")[盛宣怀的建议](../Page/盛宣怀.md "wikilink")，收回地方筑路权，宣布铁路国有，将集股商办的[川汉铁路路权拱手让给外国列强](../Page/川汉铁路.md "wikilink")。由于[四川人民对于清政府的股权赔偿不满](../Page/四川.md "wikilink")，反清情绪迅速高涨，[四川的](../Page/四川.md "wikilink")[保路运动在极短期内如火如荼地迅猛展开](../Page/保路运动.md "wikilink")。两个月中，全川大多数州县建立了保路同志会，其中有[同盟会](../Page/同盟会.md "wikilink")，有立宪派，但最主要的力量是各地的[袍哥](../Page/袍哥.md "wikilink")，[同盟军所依靠的力量](../Page/同盟军.md "wikilink")，也就是袍哥与[新军](../Page/新军.md "wikilink")，四川[同盟会负责人](../Page/同盟会.md "wikilink")[熊克武](../Page/熊克武.md "wikilink")、[尹昌衡](../Page/尹昌衡.md "wikilink")、[杨庶堪](../Page/杨庶堪.md "wikilink")、[吴玉章等人也都加入了袍哥](../Page/吴玉章.md "wikilink")。1911年8月4日，川西各地袍哥大爷到资州（今[资中](../Page/资中.md "wikilink")）罗泉井召开袍哥**攒堂大会**，商议保路反清事宜，决定将保路同志会改为[保路同志军](../Page/保路同志军.md "wikilink")，在川西地区举行反清大起义。9月7日[成都血案爆发后](../Page/成都血案.md "wikilink")，以袍哥会为主保路同志军开始全省武装起义。11月22日[重庆宣布独立](../Page/重庆.md "wikilink")，成立[蜀军政府](../Page/蜀军.md "wikilink")，[张培爵被推为蜀军](../Page/张培爵.md "wikilink")[都督](../Page/都督.md "wikilink")，[夏之时为副都督](../Page/夏之时.md "wikilink")。12月8日[成都兵变之后](../Page/成都.md "wikilink")，各路同志军进入成都，[尹昌衡改组](../Page/尹昌衡.md "wikilink")[四川军政府](../Page/四川.md "wikilink")，由[尹昌衡](../Page/尹昌衡.md "wikilink")、[罗纶分任正副都督](../Page/罗纶.md "wikilink")，至此，清政府在[四川的统治宣告结束](../Page/四川.md "wikilink")。

[保路同志军起事成功后](../Page/保路同志军起事.md "wikilink")，[四川各地成立了大量的袍哥组织](../Page/四川.md "wikilink")，此时的[四川](../Page/四川.md "wikilink")，已经成了袍哥的世界。

### 护国战争

[袁世凯迫使](../Page/袁世凯.md "wikilink")[宣統退位之后](../Page/宣統退位.md "wikilink")，就任[中华民国临时大总统](../Page/中华民国.md "wikilink")。1915年12月12日[袁世凯称帝](../Page/袁世凯.md "wikilink")，改[中华民国为](../Page/中华民国.md "wikilink")[中華帝國](../Page/中華帝國_\(國號\).md "wikilink")。前[云南督军](../Page/云南.md "wikilink")[蔡锷](../Page/蔡锷.md "wikilink")、云南将军[唐继尧于](../Page/唐继尧.md "wikilink")1915年12月25日在[昆明宣布](../Page/昆明.md "wikilink")[云南独立](../Page/云南.md "wikilink")，并即组识约二万人之[护国军讨伐](../Page/护国军.md "wikilink")[袁世凯](../Page/袁世凯.md "wikilink")，分由[蔡锷](../Page/蔡锷.md "wikilink")、[李烈钧](../Page/李烈钧.md "wikilink")、[唐继尧任一](../Page/唐继尧.md "wikilink")、二、三军总司令；[唐继尧兼任](../Page/唐继尧.md "wikilink")[云南都督府都督](../Page/云南.md "wikilink")。[袁世凯调派八万人进攻云南](../Page/袁世凯.md "wikilink")，在[四川被](../Page/四川.md "wikilink")[蔡锷大败](../Page/蔡锷.md "wikilink")。在这次护国战争中，同盟会方面又再次拉拢、依靠[袍哥](../Page/袍哥.md "wikilink")，[袍哥武装再次集聚](../Page/袍哥.md "wikilink")，在各地作战。**四川护国军**司令部就设在[温江](../Page/温江.md "wikilink")、[崇州](../Page/崇州.md "wikilink")、[双流三县交界的](../Page/双流.md "wikilink")[袍哥码头](../Page/袍哥.md "wikilink")[刘家濠](../Page/刘家濠.md "wikilink")，司令、副司令、参谋长都是原同志军首领、袍哥大爷。

## 消亡

[国共战争末期](../Page/国共战争.md "wikilink")，[国民政府希望借助](../Page/国民政府.md "wikilink")[四川特殊的地形延续政权](../Page/四川.md "wikilink")，就如[抗日战争时一样](../Page/抗日战争.md "wikilink")，把[首都从](../Page/首都.md "wikilink")[南京迁移到](../Page/南京.md "wikilink")[重庆](../Page/重庆.md "wikilink")、[成都](../Page/成都.md "wikilink")，后来又迁到[西昌](../Page/西昌.md "wikilink")，试图在[四川与](../Page/四川.md "wikilink")[共产党部队抗争下去](../Page/共产党.md "wikilink")。大多数[袍哥大爷都参加了](../Page/袍哥.md "wikilink")[国民政府所组织抵抗](../Page/国民政府.md "wikilink")[共产党武力進攻的部队](../Page/共产党.md "wikilink")，五期“游击干部训练班”的学员大多是[袍哥](../Page/袍哥.md "wikilink")，各地成立的**[反共救国军](../Page/反共救国军.md "wikilink")**、**游击挺进军**之类基本上全是袍哥武装。[解放军奪下](../Page/中国人民解放军.md "wikilink")[四川之后不久](../Page/四川.md "wikilink")，1950年2月，[国民党残余力量在川西各地组织了一次反攻](../Page/国民党.md "wikilink")，其主要力量也都是[袍哥](../Page/袍哥.md "wikilink")。后来被[共产党作为典型批判的大地主](../Page/共产党.md "wikilink")[刘文彩的弟弟](../Page/刘文彩.md "wikilink")[刘文辉组织策划执行了这次反攻](../Page/刘文辉.md "wikilink")，并且拱手交给了[共产党](../Page/共产党.md "wikilink")。[刘文彩就是当时当地](../Page/刘文彩.md "wikilink")[哥老会的主要干部](../Page/哥老会.md "wikilink")。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，中国政府取缔一切民间黑社会组织以及非法社团，并且对抵抗[解放军的武装袍哥会进行镇压](../Page/解放军.md "wikilink")，其一百多年的风光历史就此基本结束。

## 其他

### 嗨袍哥

在[四川话裡把加入袍哥会称为](../Page/四川话.md "wikilink")“嗨袍哥”、“嗨皮”；未入会者叫“空子”。

### 共進會

### 茶馆

在袍哥会中[茶馆是重要活动场所](../Page/茶馆.md "wikilink")。各个山头多称XX公，XX社，各占一方“码头”，设茶馆为联络地，也就是香堂所在地，集会多在茶馆中举行。执事袍哥每天必去茶馆处理组织有关事务，接待江湖朋友，称为把水口。茶馆也是袍哥平理、调解纠纷的场所。袍哥会的每年的三大集会也在茶馆举行。

## 影响

### 语言文化

袍哥会的[黑话给](../Page/黑话.md "wikilink")[四川话带来许多新词](../Page/四川话.md "wikilink")，如光棍（会员自称）、氉皮（丢脸、伤面子，俗写爲臊皮）、落教（合规矩）、不认黄（不客气或不认账）、地皮风（诽言谤语）、搭手（帮忙）、扎起（大力帮助、袒护）、打平火（分摊伙食费、[AA制](../Page/AA制.md "wikilink")，俗写爲打平伙）等。\[1\]\[2\]

## 相关重要人物

  - [马福益](../Page/马福益.md "wikilink")
  - [焦达峰](../Page/焦达峰.md "wikilink")
  - [張堯卿](../Page/張堯卿.md "wikilink")
  - [楊森](../Page/楊森.md "wikilink")
  - [朱德](../Page/朱德.md "wikilink")
  - [杨庶堪](../Page/杨庶堪.md "wikilink")
  - [吴玉章](../Page/吴玉章.md "wikilink")
  - [张培爵](../Page/张培爵.md "wikilink")
  - [刘文辉](../Page/刘文辉.md "wikilink")
  - [刘文彩](../Page/刘文彩.md "wikilink")
  - [鄧紹昌](../Page/鄧紹昌.md "wikilink")（[鄧小平之父](../Page/鄧小平.md "wikilink")）
  - [王亞樵](../Page/王亞樵.md "wikilink")
  - [范绍增](../Page/范绍增.md "wikilink")
  - [毕永平](../Page/毕永平.md "wikilink")
  - [杨鸿钧](../Page/杨鸿钧.md "wikilink")
  - [李云彪](../Page/李云彪.md "wikilink")
  - [李堃山](../Page/李堃山.md "wikilink")
  - [林述唐](../Page/林述唐.md "wikilink")
  - [冷开泰](../Page/冷开泰.md "wikilink")
  - [陈俊珊](../Page/陈俊珊.md "wikilink")
  - [杜重石](../Page/杜重石.md "wikilink")

## 参考文献

  - 王笛：〈[乡村秘密社会的多种叙事——1940年代四川袍哥的文本解读](http://www.nssd.org/articles/article_read.aspx?id=665379307)〉。
  - 王笛：〈[“吃讲茶”：成都茶馆、袍哥与地方政治空间](http://www.nssd.org/articles/article_read.aspx?id=32843020)〉。

## 注释和引用

[哥老会](../Category/哥老会.md "wikilink")
[Category:會道門](../Category/會道門.md "wikilink")
[Category:清朝组织](../Category/清朝组织.md "wikilink")
[Category:中華民國大陸時期組織](../Category/中華民國大陸時期組織.md "wikilink")
[Category:中华人民共和国非法组织](../Category/中华人民共和国非法组织.md "wikilink")
[Category:四川组织](../Category/四川组织.md "wikilink")
[Category:重庆组织](../Category/重庆组织.md "wikilink")
[Category:中国秘密组织](../Category/中国秘密组织.md "wikilink")

1.
2.