**桑谷夏子**，[日本女性](../Page/日本.md "wikilink")[配音員](../Page/配音員.md "wikilink")。出身於[東京都](../Page/東京都.md "wikilink")\[1\][青梅市](../Page/青梅市.md "wikilink")。[身高](../Page/身高.md "wikilink")148cm。O型[血](../Page/血型.md "wikilink")\[2\]\[3\]。

## 簡介

原[ARTSVISION所屬](../Page/ARTSVISION.md "wikilink")，現在是[I'm
Enterprise所屬](../Page/I'm_Enterprise.md "wikilink")。、[日本播音演技研究所出身](../Page/日本播音演技研究所.md "wikilink")。

為人熟悉的代表配音作品有《[妹妹公主](../Page/妹妹公主.md "wikilink")》女主角可憐、《[純情房東俏房客
Again](../Page/純情房東俏房客.md "wikilink")》的浦島可奈子、《[閃靈二人組](../Page/閃靈二人組.md "wikilink")》的工藤卑彌呼、《[最後流亡](../Page/最後流亡.md "wikilink")》的艾莉絲蒂亞·艾格紐、《[完美超人Joe](../Page/完美超人Joe.md "wikilink")》的席薇亞、《[薔薇少女](../Page/薔薇少女.md "wikilink")》的[翠星石](../Page/薔薇少女角色列表#翠星石.md "wikilink")、《[魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink")》的艾爾芙&《[魔法少女奈葉StrikerS](../Page/魔法少女奈葉StrikerS.md "wikilink")》的璐缇希雅·阿爾菲諾、《[魔法老師！](../Page/魔法老師_\(動畫\).md "wikilink")》的[綾瀨夕映](../Page/魔法老師角色列表#綾瀨夕映.md "wikilink")、《[涼宮春日](../Page/涼宮春日系列.md "wikilink")》系列的[朝倉涼子](../Page/涼宮春日系列角色列表#朝倉涼子.md "wikilink")、《[瀨戶的花嫁](../Page/瀨戶的花嫁_\(漫畫\).md "wikilink")》的卷、《[夜櫻四重奏](../Page/夜櫻四重奏.md "wikilink")》的士夏彥八重、《[戰國BASARA](../Page/戰國BASARA系列.md "wikilink")》系列的[春日](../Page/戰國BASARA系列角色列表#上杉軍.md "wikilink")、《[向陽素描×☆☆☆](../Page/向陽素描_\(動畫\).md "wikilink")》系列的桑原老師\[4\]、《[未來日記](../Page/未來日記_\(漫畫\).md "wikilink")》的美神愛、《[銀河戰警](../Page/銀河戰警.md "wikilink")》的特威德魯蒂、《[咲-Saki-](../Page/咲-Saki-.md "wikilink")》的蒲原智美等。

### 來歷

桑谷於1998年參加雜誌的一般募集下，以同年播出的[電視動畫](../Page/電視動畫.md "wikilink")《[男女蹺蹺板](../Page/男女蹺蹺板.md "wikilink")》（聲演路人少女A）成為她的聲優出道作。在那之後因聲演《[妹妹公主](../Page/妹妹公主.md "wikilink")》的女主角可憐而一舉成名，同時與在同名作品合作的同行[望月久代](../Page/望月久代.md "wikilink")、[小林由美子](../Page/小林由美子.md "wikilink")、[水樹奈奈共](../Page/水樹奈奈.md "wikilink")4人一起组成聲優團體「（<small></small>）」。

另一方面，為紀念廣播節目《》延長持續播出，桑谷與當時一起搭檔主持的[齋藤千和兩人一起組成](../Page/齋藤千和.md "wikilink")「**coopee**（<small></small>）」。

2008年3月1日，桑谷從出道以來所屬的[ARTSVISION轉籍到現在的](../Page/ARTSVISION.md "wikilink")[I'm
Enterprise](../Page/I'm_Enterprise.md "wikilink")。

### 人物、逸事

桑谷喜歡戴[太陽眼鏡](../Page/太陽眼鏡.md "wikilink")\[5\]，討厭所有的蟲子\[6\]。另外，雖然是在8月出生而取名為“夏子”，但她自己不是很喜歡夏天\[7\]。相反的她很喜歡冬天，因此去過各家滑雪場練[滑雪](../Page/滑雪.md "wikilink")。

高中時期曾經立志要當[漫畫家](../Page/漫畫家.md "wikilink")，因此高2的時候有投稿作品至出版社。但到了畢業卻沒能以漫畫家出道而選擇放棄，轉念以聲優為目標。成為聲優之後，參加動畫《[魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink")》系列的相關活動時，曾有在活動舞台現場展示自己畫的插圖並獲得同席的女性聲優說「好棒\!\!」等掌聲的逸事\[8\]。並在2003年8月至2005年8月間在雑誌《》常發表插圖和「話」的感想短文。

《[涼宮春日](../Page/涼宮春日系列.md "wikilink")》系列動畫（含衍生作品《[小長門有希的消失](../Page/小長門有希的消失.md "wikilink")》）播出以來，被聲演主角[阿虛的](../Page/涼宮春日系列角色列表#阿虛.md "wikilink")[杉田智和認定是位具存在感但很可靠的人](../Page/杉田智和.md "wikilink")，因此杉田給予她「小巨人（<small></small>）」的稱號。

桑谷曾在[電腦遊戲](../Page/電腦遊戲.md "wikilink")《鳥籠的另一邊》（聲演露媞雃·布蘭結）念劇本時說[法語](../Page/法語.md "wikilink")。她說到目前為止自己最投入的角色是《[銀河戰警](../Page/銀河戰警.md "wikilink")》的特威德魯蒂。

### 交友關係

友人有[中原麻衣](../Page/中原麻衣.md "wikilink")、[齋藤千和](../Page/齋藤千和.md "wikilink")、[石毛佐和](../Page/石毛佐和.md "wikilink")、[小林由美子](../Page/小林由美子.md "wikilink")、[釘宮理惠](../Page/釘宮理惠.md "wikilink")、[力丸乃梨子](../Page/力丸乃梨子.md "wikilink")、[野川櫻](../Page/野川櫻.md "wikilink")、[堀江由衣](../Page/堀江由衣.md "wikilink")、[田村由香里等人](../Page/田村由香里.md "wikilink")。另外，她也自稱和[清水香里](../Page/清水香里.md "wikilink")、[植田佳奈是三姐妹](../Page/植田佳奈.md "wikilink")。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 1998年

<!-- end list -->

  - [男女蹺蹺板](../Page/男女蹺蹺板.md "wikilink")（女學生A 等）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [心之圖書館](../Page/心之圖書館.md "wikilink")（小兔）
  - [妹妹公主](../Page/妹妹公主.md "wikilink")（**可憐**\[9\]）
  - [可愛巧虎島](../Page/可愛巧虎島.md "wikilink")（ ）
  - [七虹香電擊作戰](../Page/七虹香電擊作戰.md "wikilink")（御津星奇拉拉\[10\]、ZK）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [Weiß kreuz Glühen](../Page/白色十字架.md "wikilink")
  - [銀河戰警](../Page/銀河戰警.md "wikilink")（特威德魯蒂）
  - [閃靈二人組](../Page/閃靈二人組.md "wikilink")（**工藤卑彌呼**）
  - [妹妹公主 RePure](../Page/妹妹公主_RePure.md "wikilink")（**可憐**）
  - [噴嚏大魔王之小呵欠](../Page/噴嚏大魔王.md "wikilink")（千代田小百合 ）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [聖槍修女](../Page/聖槍修女.md "wikilink")（費歐蕾〈芙羅蕾特·哈本海特〉\[11\]）
  - [廢棄公主](../Page/廢棄公主.md "wikilink")（菲伏爾·阿瑪賴特）
  - [急救超人兵團](../Page/急救超人兵團.md "wikilink")（**可憐**）
  - [初音島](../Page/初音島.md "wikilink")（紫和泉子、小學生）
  - [偵探學園Q](../Page/偵探學園Q.md "wikilink")（望、護理師）
  - [成惠的世界](../Page/成惠的世界.md "wikilink")（音無麗）
  - [驚爆危機？校園篇](../Page/驚爆危機#驚爆危機?校園篇（ふもっふ）.md "wikilink")（料理社社員）
  - [真珠美人魚](../Page/真珠美人魚.md "wikilink")（女學生C）
  - [最後流亡](../Page/最後流亡.md "wikilink")（**艾莉絲蒂亞·艾格紐**\[12\]）

<!-- end list -->

  - 2004年

<!-- end list -->

  - （）

  - [犬夜叉](../Page/犬夜叉.md "wikilink")（志麻 ）

  - [女孩萬歲 first season](../Page/女孩萬歲.md "wikilink")（梨梨花·史戴希）

  - [完美超人Joe](../Page/完美超人Joe.md "wikilink")（**席薇亞**）

  - [雙戀](../Page/雙戀.md "wikilink")（千草戀）

  - [魔法少女奈葉](../Page/魔法少女奈葉.md "wikilink")（**艾爾芙**）

  - [瑪莉亞的凝望](../Page/瑪莉亞的凝望.md "wikilink")（友子）

  - [美鳥的日記](../Page/美鳥的日記.md "wikilink")（岩崎紅子）

  - [薔薇少女](../Page/薔薇少女.md "wikilink")（**[翠星石](../Page/薔薇少女角色列表#翠星石.md "wikilink")**）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [我太太是高中生](../Page/我太太是高中生.md "wikilink")（女服務生）
  - 女孩萬歲 second season（梨梨花·史戴希）
  - [我的主人愛作怪](../Page/我的主人.md "wikilink")（艾莉西雅）
  - [曙光少女](../Page/曙光少女.md "wikilink")（卡夏·瑪比利古\[13\]）
  - [初音島 Second Season](../Page/初音島.md "wikilink")（紫和泉子）
  - [變形金剛：銀河之力](../Page/變形金剛：銀河之力.md "wikilink")（克羅米亞）
  - [蜂蜜與四葉草](../Page/蜂蜜與四葉草.md "wikilink")（女學生C）
  - [雙戀 Alternative](../Page/雙戀.md "wikilink")（千草戀）
  - [怪醫黑傑克](../Page/怪醫黑傑克_\(動畫\).md "wikilink")（學生）
  - [黏黏妖美少女](../Page/黏黏妖美少女.md "wikilink")（赤澤清美\[14\]）
  - [魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")（艾爾芙）
  - [魔法老師！](../Page/魔法老師_\(動畫\).md "wikilink")（**[綾瀨夕映](../Page/魔法老師角色列表#綾瀨夕映.md "wikilink")**、女僕）
  - 薔薇少女 彷如夢境（**翠星石**）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [櫻蘭高校男公關部](../Page/櫻蘭高校男公關部.md "wikilink")（春日崎奏子／嘟嘟鳥）
  - [Canvas2 ～彩虹色的圖畫～](../Page/Canvas2.md "wikilink")（菜乃花惠里）
  - [Keroro軍曹](../Page/Keroro軍曹_\(動畫\).md "wikilink")（Chiroro）
  - [涼宮春日的憂鬱
    (2006年版)](../Page/涼宮春日的憂鬱.md "wikilink")（**[朝倉涼子](../Page/涼宮春日系列角色列表#朝倉涼子.md "wikilink")**）
  - [草莓危機](../Page/草莓危機.md "wikilink")（南都夜夜）
  - 魔法老師\!?（**綾瀨夕映**）\[15\]
  - [魔法美少女](../Page/魔法美少女.md "wikilink")（魔宮滿）
  - [甜蜜偶像](../Page/甜蜜偶像.md "wikilink")（結城瞳子）
  - 薔薇少女 序曲（**翠星石**）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [地獄少女 二籠](../Page/地獄少女.md "wikilink")（清）
  - [少年陰陽師](../Page/少年陰陽師.md "wikilink")（天后）
  - [Sketchbook ～full
    color's～](../Page/Sketch_book～素描簿～.md "wikilink")（冰室風）
  - [瀨戶的花嫁](../Page/瀨戶的花嫁_\(漫畫\).md "wikilink")（**卷**）
  - [Devil May Cry](../Page/惡魔獵人系列.md "wikilink")（安潔莉娜）
  - [魔法少女奈葉StrikerS](../Page/魔法少女奈葉StrikerS.md "wikilink")（**璐缇希雅·阿爾菲諾**、奧莉絲·蓋茲、塞緹、艾爾芙）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [S·A特優生 ～Special A～](../Page/S·A特優生.md "wikilink")（**牛窪櫻**）
  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")（）
  - [×××HOLiC◆繼](../Page/×××HOLiC.md "wikilink")（茅乃）
  - [夜櫻四重奏](../Page/夜櫻四重奏.md "wikilink")（**士夏彥八重**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [犬夜叉 完結篇](../Page/犬夜叉.md "wikilink")（阿貉）
  - [KIDDY GiRL-AND](../Page/KIDDY_GiRL-AND.md "wikilink")（特威德魯蒂）
  - [CLANNAD ～AFTER STORY～](../Page/CLANNAD.md "wikilink")（木村）
  - [咲-Saki-](../Page/咲-Saki-.md "wikilink")（蒲原智美）
  - 地獄少女 三鼎（宮嶋幸）
  - [戰國BASARA](../Page/戰國BASARA系列.md "wikilink")（**[春日](../Page/戰國BASARA系列角色列表#上杉軍.md "wikilink")**\[16\]、夢吉）
  - [天體戰士桑雷德](../Page/天體戰士桑雷德.md "wikilink")（）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [玩伴貓耳娘](../Page/玩伴貓耳娘.md "wikilink")（女性廣播）
  - [異世界聖機師物語](../Page/異世界聖機師物語.md "wikilink")（凱雅·弗朗）
  - 戰國BASARA貳（**春日**\[17\]、夢吉）
  - [哆啦A夢 (水田山葵版)](../Page/哆啦A夢_\(動畫\).md "wikilink")（良子、女孩子）
  - [向陽素描×☆☆☆](../Page/向陽素描_\(動畫\).md "wikilink")（桑原老師〈代替〉\[18\]、）
  - 向陽素描×☆☆☆ 特別篇（桑原老師）\[19\]
  - [夢色蛋糕師](../Page/夢色蛋糕師.md "wikilink")（知念美果、）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [神樣DOLLS](../Page/神樣DOLLS.md "wikilink")（綾女）
  - [塔麻可吉！](../Page/寵物反斗星.md "wikilink")（帕里香）
  - [日常](../Page/日常.md "wikilink")（連裙裝女性）
  - 向陽素描×SP（桑原老師〈第2代〉）\[20\]
  - [結界女王](../Page/結界女王.md "wikilink")（\[21\]）
  - [迷茫管家與膽怯的我](../Page/迷茫管家與膽怯的我.md "wikilink")（瑪莉亞）
  - [未來日記](../Page/未來日記_\(漫畫\).md "wikilink")（**美神愛**〈**7th**〉）
  - [最後流亡 -銀翼的飛夢-](../Page/最後流亡-銀翼的飛夢-.md "wikilink")（艾莉絲蒂亞·艾格紐\[22\]）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [翡翠森林狼與羊 ～秘密的朋友～](../Page/翡翠森林狼與羊.md "wikilink")（莉莉）
  - [境界線上的地平線II](../Page/境界線上的地平線.md "wikilink")（威廉·塞西爾、[F·沃爾辛厄姆](../Page/弗朗西斯·沃尔辛厄姆.md "wikilink")）
  - [咲-Saki- 阿知賀篇 episode of
    side-A](../Page/咲-Saki-.md "wikilink")（蒲原智美）
  - [人類衰退之後](../Page/人類衰退之後.md "wikilink")（女醫、B學姊）
  - 塔麻可吉！Yume Kira Dream（、、帕里香、黛西）
  - [探險托里蘭托](../Page/探險托里蘭托.md "wikilink")（莉娜）
  - 向陽素描×蜂窩（桑原老師）
  - [武裝神姫](../Page/武裝神姫.md "wikilink")（瑪莉·瀧川·賽蕾絲／賽蓮型神姬）
  - [Persona4 the ANIMATION](../Page/女神異聞錄4.md "wikilink")（上原小夜子）
  - [希望宅邸 3D](../Page/希望宅邸.md "wikilink")（**川野沙英**）
  - 希望宅邸 3D PLUS（**川野沙英**）

<!-- end list -->

  - 2013年

<!-- end list -->

  - 塔麻可吉！Miracle Friends（、）
  - 塔麻可吉！Yume Kira Dream（、）
  - 夜櫻四重奏 -花之歌-（士夏彥八重\[23\]）
  - [薔薇少女 (新作)](../Page/薔薇少女.md "wikilink")（**翠星石**\[24\]）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（桃子）
  - GO-GO 塔麻可吉！（、帕里香）
  - [咲-Saki- 全國篇](../Page/咲-Saki-.md "wikilink")（蒲原智美）
  - [人生諮詢電視動畫「人生」](../Page/人生_\(輕小說\).md "wikilink")（三上菜菜子）
  - 戰國BASARA Judge End（**春日**\[25\]）
  - 哆啦A夢 (水田山葵版)（洒落子）
  - [HAMATORA -超能偵探社-](../Page/HAMATORA_─超能偵探社─.md "wikilink")（米絲緹）

<!-- end list -->

  - 2015年

<!-- end list -->

  - GO-GO 塔麻可吉！（）
  - [Charlotte](../Page/Charlotte_\(動畫\).md "wikilink")（西森柚咲母親）
  - [小長門有希的消失](../Page/小長門有希的消失.md "wikilink")（**朝倉涼子**\[26\]）
  - [放學後的昴星團](../Page/放學後的昴星團.md "wikilink")（護士）
  - [魔法少女奈葉ViVid](../Page/魔法少女奈葉ViVid.md "wikilink")（璐緹希雅·阿爾菲諾）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [ViVid Strike！](../Page/ViVid_Strike!.md "wikilink")（璐緹希雅·阿爾菲諾）
  - [Schwarzesmarken](../Page/Schwarzesmarken.md "wikilink")（Suzy Cave）
  - [半田君傳說](../Page/元氣囝仔#半田君傳說.md "wikilink")（半田清舟的母親〈半田繪美\[27\]）
  - [名偵探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")（須田久美 ）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [青之驅魔師 京都不淨王篇](../Page/青之驅魔師.md "wikilink")（聰的母親 ）
  - [櫻花任務](../Page/櫻花任務.md "wikilink")（柊聖美\[28\] ）
  - [悠久持有者！ ～魔法老師！2～](../Page/悠久持有者.md "wikilink")（綾瀨夕映\[29\] ）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [HUG！光之美少女](../Page/HUG！光之美少女.md "wikilink")（野乃堇\[30\]）
  - [ONE PIECE](../Page/ONE_PIECE_\(動畫\).md "wikilink")（葛德）
  - [Free！ -Dive to the Future-](../Page/Free!.md "wikilink")（桐嶋郁彌〈幼少期〉）
  - [我喜歡的妹妹不是妹妹](../Page/我喜歡的妹妹不是妹妹.md "wikilink")（可憐）
  - [學園BASARA](../Page/戰國BASARA系列.md "wikilink")（**春日**\[31\]）

### OVA

  - 2002年

<!-- end list -->

  - [純情房東俏房客 Again](../Page/純情房東俏房客.md "wikilink")（**浦島可奈子**\[32\]）

<!-- end list -->

  - 2003年

<!-- end list -->

  - （楠瀨緋菜）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [鴉 -KARAS-](../Page/鴉_-KARAS-.md "wikilink")

<!-- end list -->

  - 2006年

<!-- end list -->

  - [魔法老師！春](../Page/魔法老師_\(動畫\).md "wikilink")（**[綾瀨夕映](../Page/魔法老師角色列表#綾瀨夕映.md "wikilink")**）
  - 魔法老師！夏（**綾瀨夕映**）
  - [秋之回憶 \#5 中斷的影片 THE
    ANIMATION](../Page/秋之回憶5：中斷的影片.md "wikilink")（觀島香月）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [瀨戶的花嫁OVA](../Page/瀨戶的花嫁_\(漫畫\).md "wikilink")（**卷**）
  - [魔法老師！ ～白之翼 ALA
    ALBA～](../Page/魔法老師_\(動畫\)#白之翼_ALA_ALBA.md "wikilink")（**綾瀨夕映**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - （凪紗里佐子）

  - [異世界聖機師物語](../Page/異世界聖機師物語.md "wikilink")（**凱雅·弗朗**）

  - [魔法老師！ ～另一個世界～](../Page/魔法老師_\(動畫\)#另一個世界.md "wikilink")（**綾瀨夕映**）

  - [希望宅邸](../Page/希望宅邸.md "wikilink")（**川野沙依**）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [魔法老師！ 另一個世界Extra
    魔法少女夕映♥](../Page/魔法老師_\(動畫\)#另一個世界Extra_魔法少女夕映.md "wikilink")（**綾瀨夕映**）
  - [夜樱四重奏 ～星之海～](../Page/夜櫻四重奏#夜櫻四重奏_～星之海～.md "wikilink")（士夏彥八重）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [Baby Princess 3D樂園0（LOVE）](../Page/Baby_Princess.md "wikilink")（媽媽）
  - 希望宅邸？（**川野沙依**）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [夜櫻四重奏 ～月之泣～](../Page/夜櫻四重奏#夜櫻四重奏_～月之泣～.md "wikilink")（士夏彥八重）\[33\]

<!-- end list -->

  - 2014年

<!-- end list -->

  - [鎖鏈戰記 ～短篇動畫～](../Page/鎖鏈戰記#OVA.md "wikilink")（艾蕾努斯、小孩\[34\]）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [咲-Saki-](../Page/咲-Saki-.md "wikilink")（蒲原智美）
  - [小長門有希的消失](../Page/小長門有希的消失.md "wikilink")（**[朝倉涼子](../Page/涼宮春日系列角色列表#朝倉涼子.md "wikilink")**）\[35\]

<!-- end list -->

  - 2016年

<!-- end list -->

  - [斬首循環 藍色學者與戲言跟班](../Page/戲言系列.md "wikilink")（千賀明\[36\]）

### 電影動畫

  - 2010年

<!-- end list -->

  - [涼宮春日的消失](../Page/涼宮春日的消失.md "wikilink")（**[朝倉涼子](../Page/涼宮春日系列角色列表#朝倉涼子.md "wikilink")**\[37\]）
  - [魔法少女奈葉 The MOVIE
    1st](../Page/魔法少女奈葉_The_MOVIE_1st.md "wikilink")（**艾爾芙**）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [劇場版 戰國BASARA -The Last
    Party-](../Page/戰國BASARA系列#劇場版.md "wikilink")（夢吉\[38\]）
  - [劇場版 魔法老師！ ANIME
    FINAL](../Page/劇場版_魔法老師！_ANIME_FINAL.md "wikilink")（**[綾瀨夕映](../Page/魔法老師角色列表#綾瀨夕映.md "wikilink")**）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [魔法少女奈葉 The MOVIE 2nd
    A's](../Page/魔法少女奈葉_The_MOVIE_2nd_A's.md "wikilink")（艾爾芙）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [最後流亡 -銀翼的飛夢- Over The
    Wishes](../Page/最後流亡-銀翼的飛夢-.md "wikilink")（艾莉斯堤雅·艾格纽\[39\]）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [魔法少女奈葉Reflection](../Page/魔法少女奈葉Reflection.md "wikilink")（艾爾芙）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [Code Geass反叛的魯路修](../Page/Code_Geass反叛的魯路修.md "wikilink") II
    叛道（老師\[40\]）

### 網路動畫

  - 2009年

<!-- end list -->

  - [小涼宮春日的憂鬱](../Page/涼宮春日系列#網路動畫.md "wikilink")（[朝倉涼子／小朝倉](../Page/涼宮春日系列角色列表#朝倉涼子.md "wikilink")、小孩）
  - 小鶴屋學姊的四格（小朝倉）

<!-- end list -->

  - 2011年

<!-- end list -->

  - （琪琪、秘書）

### 遊戲

  - 2001年

<!-- end list -->

  - [妹妹公主](../Page/妹妹公主.md "wikilink")（**可憐**）

<!-- end list -->

  - 2002年

<!-- end list -->

  - （**艾克蕾爾**／暗黑艾克蕾爾）

<!-- end list -->

  - 2003年

<!-- end list -->

  - 妹妹公主2（**可憐**）

  - [初音島Plus Situation](../Page/初音島Plus_Situation.md "wikilink")（紫和泉子）

  - Dream Collection Vol.2（尤莉艾爾·艾連庫來因）

  - （**楠瀨緋菜**）

  - for Symphony ～with all one's heart～（**高坂香月**）

<!-- end list -->

  - 2004年

<!-- end list -->

  - （尤莉艾爾·艾連庫來因）

  - [雙戀](../Page/雙戀.md "wikilink")（千草戀）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [戰國BASARA](../Page/戰國BASARA.md "wikilink")（[春日](../Page/戰國BASARA系列角色列表#上杉軍.md "wikilink")）

  - （紫和泉子）

  - [完美超人Joe 戰鬥嘉年華](../Page/完美超人Joe.md "wikilink")（席薇亞）

  - [雙戀
    Alternative：戀愛、少女與機關槍](../Page/雙戀_Alternative.md "wikilink")（千草戀）

  - [雙戀島 ～戀愛與泳裝的生存遊戲～](../Page/雙戀.md "wikilink")（千草戀）

  - [魔法老師！第1堂課
    這個小孩子老師是魔法使！](../Page/魔法老師商品列表#遊戲.md "wikilink")（**[綾瀨夕映](../Page/魔法老師角色列表#綾瀨夕映.md "wikilink")**）

  - 魔法老師！第2堂課 戰鬥吧少女們！麻帆良大運動會特輯（**綾瀨夕映**）

  - MEDICAL91（三條明日香）

  - [秋之回憶 \#5 中斷的影片](../Page/秋之回憶5：中斷的影片.md "wikilink")（觀島香月）

  - （克洛媞婭）

  - [甜蜜偶像](../Page/甜蜜偶像.md "wikilink")（**結城瞳子**）

  - （芝浦八重）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [高機動交響曲GPO](../Page/高機動交響曲GPO.md "wikilink")

  - [召喚夜響曲4](../Page/召喚夜響曲4.md "wikilink")（愛普榭特）

  - [草莓危機](../Page/草莓危機.md "wikilink")（南都夜夜）

  - 戰國BASARA2（春日）

  - 鳥籠的另一邊（露媞雃·布蘭結）

  - 魔法老師\!? 第3堂課 戀愛和魔法和世界樹傳說（**綾瀨夕映**）

  - 魔法老師\!? 超麻帆良大戰（**綾瀨夕映**）

  - 巨蟲魔島（米雪兒）

  - 魔法老師！課外活動 少女們心動的海灘風情（**綾瀨夕映**）

  - （藤島瞳、狐狸真帆）

<!-- end list -->

  - 2007年

<!-- end list -->

  - （天后）

  - 戰國BASARA2 英雄外傳（春日）

  - 魔法老師\!? 超麻帆良大戰2 CHECK IN 溫泉全員集合！（**綾瀨夕映**）

  - 魔法老師\!? 新契約對戰（**綾瀨夕映**）

  - 魔法老師\!? 夢乙女（**綾瀨夕映**）

  - [火焰之纹章 曉之女神](../Page/火焰之纹章_曉之女神.md "wikilink")（**美加雅**）

  - [秋之回憶 \#5 encore](../Page/秋之回憶5：encore.md "wikilink")（観島香月）

  - 妖鬼姬傳 ～妖魔幻燈話～（椿）

<!-- end list -->

  - 2008年

<!-- end list -->

  - （長宗我部雪江）

  - （**[朝倉涼子](../Page/涼宮春日系列角色列表#朝倉涼子.md "wikilink")**）

  - （Henne Valkyria、蘿希·密斯托拉爾）

  - 摔角天使生存者2（藤島瞳、狐狸真帆）

<!-- end list -->

  - 2009年

<!-- end list -->

  - （**朝倉涼子**）

  - 戰國BASARA 熱戰英雄（春日）

  - （〈廣播劇CD、PS2〉）

  - 甜蜜戀情（冴木）\[41\]

  - [水之魔石](../Page/水之魔石.md "wikilink")（**梅爾羅茲·基修**）

<!-- end list -->

  - 2010年

<!-- end list -->

  - （**凡妮莎**）

  - （蒲原智美）

  - 戰國BASARA3（春日）

  - [初音島Plus Situation
    Portable](../Page/初音島Plus_Situation.md "wikilink")（**紫和泉子**\[42\]）

  - [超次元戰記 戰機少女](../Page/超次元戰記_戰機少女.md "wikilink")（）

  - （瑪莉·瀧川·賽蕾絲）

  - [魔界戰記2 PORTABLE](../Page/魔界戰記2.md "wikilink")（暗黑意可蕾）\[43\]

  - （艾爾芙）

  - （美宇）

  - （Henne Valkyria、蘿希·密斯托拉爾）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [海貓鳴泣之時 ～真實與幻想的夜想曲～](../Page/海貓悲鳴時.md "wikilink")（古戶繪梨花）

  - [涼宮春日的追憶](../Page/涼宮春日的追憶.md "wikilink")（**朝倉涼子**）

  - 戰國BASARA 年代群雄（春日）

  - 戰國BASARA3 宴（春日）

  - [超次元戰記 戰機少女mk2](../Page/超次元戰記_戰機少女mk2.md "wikilink")（）

  - （艾爾芙）

<!-- end list -->

  - 2012年

<!-- end list -->

  - （艾爾芙）

  - [未來日記 第13位日記持有者 RE: WRITE](../Page/未來日記#遊戲.md "wikilink")（美神愛）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [境界線上的地平線
    PORTABLE](../Page/境界線上的地平線.md "wikilink")（威廉·塞西爾、[F·沃爾辛厄姆](../Page/弗朗西斯·沃尔辛厄姆.md "wikilink")\[44\]）
  - 幻獸姬（巴哈姆特）

<!-- end list -->

  - 2014年

<!-- end list -->

  - （**風槍美奈**\[45\]）

  - 戰國BASARA4（春日\[46\]）

  - 82H（小鳥遊希實\[47\]）

  - （翠星石）

  - [薔薇少女 扭轉命運](../Page/薔薇少女.md "wikilink")（**翠星石**\[48\]）

  - 御城收藏 ～CASTLE DEFENSE～（小谷城\[49\])

<!-- end list -->

  - 2015年

<!-- end list -->

  - 咲-Saki- 全國篇（**蒲原智美**\[50\]）
  - XUCCESS HEAVEN（天地怜美、倉科椿姫\[51\]）
  - 戰國BASARA4 皇（**春日**\[52\]）

<!-- end list -->

  - 2016年

<!-- end list -->

  - 【】（淺倉咪咪）

  - [超級機器人大戰V](../Page/超級機器人大戰V.md "wikilink")（胡蝶）

  - 雀刑事（**北方字美**\[53\]）

  - 戰國BASARA 真田幸村傳（春日）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [火焰之纹章 英雄](../Page/火焰之纹章_英雄.md "wikilink")（米卡雅）
  - [Negimate\! UQ HOLDER\!
    ～魔法老師！2～](../Page/UQ_HOLDER！悠久持有者！.md "wikilink")（綾瀨夕映）
  - [超級機器人大戰X](../Page/超級機器人大戰X.md "wikilink")（胡蝶）

### 同人遊戲

  - 2010年

<!-- end list -->

  - [黃金夢想曲](../Page/海貓悲鳴時.md "wikilink")（古戶繪梨花）

### 外語配音

※作品依英文名稱及英文原名排序。

#### 電影

  - （峨媚妹）

  - ※DVD版。

  - （Sandy）

  - [何處是我家](../Page/何處是我家.md "wikilink")

#### 電視影集

  - [珍·瑪波：書房女屍](../Page/珍·瑪波.md "wikilink")
  - [流星花園](../Page/流星花園_\(台灣電視劇\).md "wikilink")（松岡優紀〈[楊丞琳](../Page/楊丞琳.md "wikilink")
    飾演〉）
  - [惡作劇之吻](../Page/惡作劇之吻_\(韓國電視劇\).md "wikilink")（獨孤敏兒〈[尹承雅](../Page/尹承雅.md "wikilink")
    飾演〉）
  - 希望之歌（徐嘉熙〈[金素妍](../Page/金素妍.md "wikilink") 飾演〉）

#### 動畫

  - [鬼馬小精靈 (1995年版)](../Page/鬼馬小精靈.md "wikilink")（Poil）※[Cartoon
    Network版](../Page/卡通頻道.md "wikilink")。
  - [少年悍將](../Page/少年悍將_\(動畫\).md "wikilink")（Terra〈Ashley Johnson 配音〉）

### 特攝影集

  - 2007年

<!-- end list -->

  - （**撫子**的配音）

### 影片

  - [妹妹公主 -the Eve-](../Page/妹妹公主.md "wikilink")

  - [魔法老師！](../Page/魔法老師.md "wikilink") 麻帆良學園中等部2-A：一學期休業式

  - 魔法老師！ 麻帆良學園 大麻帆良祭

  - 魔法老師\!? 3-A 1學期開學日

  - 魔法老師\!?  DVD

  - ☆\!? vol.1

  -
  - [雙戀](../Page/雙戀.md "wikilink") 情人節恐慌 DVD（2005年6月22日）

### 廣播節目

  - （[文化放送](../Page/文化放送_\(日本\).md "wikilink")、[大阪放送](../Page/大阪放送.md "wikilink")、：2001年10月7日－2002年10月6日）

  - （：2001年10月7日－2002年9月29日，第9代節目主持人／與[齋藤千和共同](../Page/齋藤千和.md "wikilink")）

  - （文化放送、大阪放送、東海電台：2002年10月13日－2004年6月27日）

  - ♪（文化放送：2003年1月－6月，與[志倉千代丸](../Page/志倉千代丸.md "wikilink")、[村田步共同](../Page/村田步.md "wikilink")）

  - （：2003年4月17日－8月7日，與[川澄綾子輪班擔任](../Page/川澄綾子.md "wikilink")）

  - （文化放送：2003年10月3日 - 2004年3月25日）

  - chica☆chica（）（：2005年7月7日－2006年6月23日）

  - （[Star
    Child](../Page/Star_Child.md "wikilink")：2006年12月15日－2007年3月30日）

  - ～夏子～（：2008年4月5日－2009年3月26日）

  - 夏子與[千和的](../Page/齋藤千和.md "wikilink")（文化放送：2008年10月24日－2014年6月30日）\[54\]

  - 與[長嶋陽香的](../Page/長嶋陽香.md "wikilink")（[關西電台](../Page/關西電台.md "wikilink")：2009年2月6日，嘉賓）

  - [網路廣播
    電視動畫「戰國BASARA」【銀】](../Page/戰國BASARA系列.md "wikilink")（animateTV：2009年4月9日－9月24日）

  - [寶貝公主Radio 樂園0［LOVE］](../Page/寶貝公主.md "wikilink")（：2011年2月14日－8月15日）

  - （：2012年8月8日－）

  - [小長門有希的消失
    北高文藝社廣播支社](../Page/小長門有希的消失.md "wikilink")（：2015年4月20日－12月28日）\[55\]

### 廣播劇

  - [TBS廣播](../Page/TBS廣播.md "wikilink") 「我會保護你的。」\[56\]

### 電子漫畫

  - [VOMIC](../Page/VOMIC.md "wikilink") [Switch Girl\!\!
    ～變身指令～](../Page/Switch_Girl!!～變身指令～.md "wikilink")（2007年，妮諾）

### 廣播劇CD

  - [S·A特優生 ～Special A～](../Page/S·A特優生.md "wikilink") 廣播劇CD1
    「慧·宙·純·龍」篇（牛窪櫻）

  - 廣播劇CD [EREMENTAR GERAD The
    original](../Page/武器種族傳說.md "wikilink")（**奇雅**〈**奇理特·恩芭蒂莉雅**〉）

  - （莉莉加）

  - 廣播劇CD [Sketch Book Stories
    ～前夜祭～](../Page/Sketch_book～素描簿～.md "wikilink")（冰室楓）

  - （**田宮禮子**）

  - 水手服與重戰車（**竹內麻里子**）

  - 廣播劇CD （貓神理沙）

  - [Dear 親愛的](../Page/Dear_親愛的.md "wikilink") 廣播劇CD系列（**布莉諾·哈威爾**）

      - dear 【親愛的】
      - dear ～親愛的～ A story of the next day

  - 廣播劇CD （水乃緒玲）

  - 玩家角色系列 Vol.1

  - （艾利絲）

  - VoiceCD Happy Girl's ～我的王子～（）

  - [魔法少女奈葉 Sound Stage系列](../Page/魔法少女奈葉_Sound_Stage.md "wikilink")

      - 魔法少女奈葉 Sound Stage 02、03（艾爾芙）
      - 魔法少女奈葉 Sound Stage 01－03（艾爾芙）
      - 魔法少女奈葉 Sound Stage 01（艾爾芙）

  - [魔法老師！系列](../Page/魔法老師.md "wikilink")（**[綾瀨夕映](../Page/魔法老師角色列表#綾瀨夕映.md "wikilink")**）

  - （巴）\[57\]

  - [薔薇少女](../Page/薔薇少女.md "wikilink")（**翠星石**\[58\]）

### 音樂CD

  - 專輯「Memories of Series」（觀島香月）

  - [妹妹公主系列相關CD](../Page/妹妹公主.md "wikilink")

      - 妹妹公主 ～12位天使們～（2001年2月7日 KICA-532）
      - VoiceCD 妹妹公主 ～最喜歡哥哥了～ 系列
      - Prologue of Sister Princess ～Dear My Brother～（2001年4月4日
        KICM-3007）
      - 笑顔がNO.1！（）（2001年7月4日 KICM-3011）
      - Sister Princess Kaleidoscope（2001年9月29日 KICA-556）

  - [涼宮春日系列](../Page/涼宮春日系列.md "wikilink")（[朝倉涼子](../Page/涼宮春日系列角色列表#朝倉涼子.md "wikilink")）

      - [涼宮春日的憂鬱 角色歌曲 Vol.5
        朝倉涼子](../Page/涼宮春日的憂鬱_角色CD#Vol.5_朝倉涼子.md "wikilink")（2006年12月6日）
      - [雪花飄搖直落的未來](../Page/小長門有希的消失音樂作品#雪花飄搖直落的未來.md "wikilink")（2015年4月29日）
      - [小長門有希的消失 CHARACTER SONG SERIES “In Love” case.2 ASAKURA
        RYOKO](../Page/小長門有希的消失音樂作品#case.2_Asakura_Ryoko（朝倉涼子）.md "wikilink")（2015年5月27日）

  - [瀨戶的花嫁](../Page/瀨戶的花嫁_\(漫畫\).md "wikilink") 角色歌曲4 HITMAN\!\! 卷

  - Dream Collection Vol.2（尤莉艾爾·艾連庫來因）

  - D→A Vocal Collection（尤莉艾爾·艾連庫來因）

  - （[望月久代](../Page/望月久代.md "wikilink")、[小林由美子](../Page/小林由美子.md "wikilink")、[水樹奈奈](../Page/水樹奈奈.md "wikilink")、桑谷夏子）相關CD

      - Sakura Revolution（2002年1月1日 KICM-3019）

      - （2002年2月1日 KICM-3023）

      - （2002年3月1日 KICM-3026）

      - （2002年12月25日 KICA-589）

  - [魔法老師！系列相關CD](../Page/魔法老師.md "wikilink")

      - Invitation ～歡迎來到圖書館～（2004年3月24日）

          - 圖書館探險社（[能登麻美子](../Page/能登麻美子.md "wikilink")、[石毛佐和](../Page/石毛佐和.md "wikilink")、桑谷夏子）

      - 魔法老師！ 麻帆良學園中等部2-A系列

      - （1月度：Original Version）（2005年2月16日）

          - 麻帆良學園中等部2-A（、、[白鳥由里](../Page/白鳥由里.md "wikilink")、[山川琴美](../Page/山川琴美.md "wikilink")、[淺倉杏美](../Page/山本杏美.md "wikilink")、桑谷夏子）

      - （2005年5月11日）

          - 師傅和煩惱的少女組（[松岡由貴](../Page/松岡由貴.md "wikilink")、笹川亞矢奈、、[渡邊明乃](../Page/渡邊明乃.md "wikilink")、桑谷夏子）

      - 1000%SPARKING！／A-LY-YA！（2007年 能登麻美子、石毛佐和）

      - 最佳專輯「真夜中Philosophy／隊／Hello Again」

      - Happy☆Material Return（2008年）

      - Theme Song Collection「Magical Twinkle Sky／Good
        vibration\!\!」（2010年）

      - 風約束 -旅立歌-（2011年8月24日）

### 廣播CD

  - 按常理思考是呼叫桑谷小姐的時候了吧CD ～『』 DJ-CD vol.2～（2008年）\[59\]

### 廣告

  - 東京電視台廣告「」PV的露面出演（聲優組合成員名義）
  - [週刊少年Magazine](../Page/週刊少年Magazine.md "wikilink")
    [魔法老師！](../Page/魔法老師.md "wikilink")（2006年11月－12月度）
  - 魔法老師！ 麻帆良學園 大麻帆良祭
    廣告（[綾瀨夕映](../Page/魔法老師角色列表#綾瀨夕映.md "wikilink")）\[60\]

### 柏青哥遊戲

  - （2009年，**麻比奈百合奈**）

  - [CR魔法老師！](../Page/魔法老師.md "wikilink")（2017年，綾瀨夕映\[61\]）

### 其它

  - Sister princess Valentine Party
  - [NHK教育](../Page/NHK教育頻道.md "wikilink")「新日曜美術館」（聲優組合成員名義出演）
  - [東京臨海新交通臨海線新豐洲車站站內指南旁述](../Page/東京臨海新交通臨海線.md "wikilink")
  - [水樹奈奈](../Page/水樹奈奈.md "wikilink")『Song
    Communication』[幕後和聲](../Page/和聲歌手.md "wikilink")（後收錄在水樹她的第21張單曲『[PHANTOM
    MINDS](../Page/PHANTOM_MINDS.md "wikilink")』）
  - 心跳時計（艾里西亞·桑特雷爾〈公主〉）
  - 幻想鄉御伽草子 ～Witches gathering～（艾莉絲·馬加特洛伊徳）

## 腳註

### 注釋

### 來源

## 外部連結

  - [Natsuko Kuwatani](http://www.imdb.com/name/nm1385874/)
    －[網路電影資料庫](../Page/互聯網電影資料庫.md "wikilink")

  - [I'm
    Enterprise公式官網的聲優簡介](http://www.imenterprise.jp/data.php?id=74&sid=91ceb1f42156f0f1f7dac141a3576486)


  - －（舊）桑谷夏子的官方個人部落格。

      - －桑谷夏子的官方個人部落格。

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")

1.

2.
3.
4.  原本幫在身體不適休養的這段期間代演，結果必須要無限期休養而接任。

5.

6.
7.  『』第4回，2005年6月24日播出。後來該錄音內容也收錄在同名動畫的廣播劇CD「『薔薇少女 網路廣播 薔薇香味的花園派對』
    CD特別篇」。

8.  來自「[活動『魔法少女奈葉☆Party』紀錄](http://www.nanoha.com/archive/topics/lyricalparty_report1.html)」、「[魔法少女奈葉](http://www.nanoha.com/archive/)官方網站2005年3月17日的最新消息。「[](http://www.nanoha.com/archive2/topics/topics_01b.html)」、「[魔法少女奈葉A's
    公式官網](http://www.nanoha.com/archive2/)」2005年9月24日的最新消息。「[ITmedia +D
    Games：人氣聲優群的華麗競演――「魔法少女奈葉A's」放送直前活動「魔法少女奈葉☆Party
    SP」紀錄](http://plusd.itmedia.co.jp/games/articles/0509/29/news082.html)」，，2005年9月29日更新。

9.

10.

11.

12.

13.

14.

15. 同時為第12話負責標題命名和提供片尾插圖。

16.

17.

18.
19.
20.
21.

22.

23.

24.

25.

26.

27. 工作人員表以「**半田母**」表示。

28.

29.

30.

31.

32.

33. 隨著單行本第14冊限定版的發行附贈DVD。

34.

35. 隨著單行本第9冊限量版的發行附贈原裝BD。

36.

37.

38.

39.

40. 電視動畫系列是由[國府田麻理子擔任](../Page/國府田麻理子.md "wikilink")。

41. 但在2009年12月宣布發售中止。

42.

43. 追加下載內容登場。

44. 《[電擊PlayStation](../Page/電擊PlayStation.md "wikilink")》Vol.534，[ASCII
    Media Works](../Page/ASCII_Media_Works.md "wikilink")，2013年1月17日。

45.

46. 《[週刊Fami通](../Page/Fami通.md "wikilink")》2013年12月19日號，[enterbrain](../Page/enterbrain.md "wikilink")，2013年12月5日。

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57. 隨著遊戲初回限定版的發行附贈廣播劇CD。

58.

59.

60. 扮演自己配音的角色露面出演。

61.