**勞滕区**（Lautém）是[東帝汶的一個區域](../Page/東帝汶.md "wikilink")，在[帝汶島的最東端](../Page/帝汶島.md "wikilink")。人口有57,453人（2004年人口普查），面積為1,702平方公里。它的首府是[聖保羅](../Page/聖保羅_\(東帝汶\).md "wikilink")，離[帝力](../Page/帝力.md "wikilink")248[公里](../Page/公里.md "wikilink")。下辖有[伊利奥马尔县](../Page/伊利奥马尔县.md "wikilink")、[劳滕县](../Page/劳滕县.md "wikilink")、[聖保羅](../Page/聖保羅_\(東帝汶\).md "wikilink")、[卢罗县以及](../Page/卢罗县.md "wikilink")[Tutuala](../Page/Tutuala.md "wikilink")。

勞滕区邊界的最西面為[包考区和](../Page/包考区.md "wikilink")[維克克区](../Page/維克克区.md "wikilink")，北面是[班達海](../Page/班達海.md "wikilink")，南面是[帝汶海](../Page/帝汶海.md "wikilink")。該區包括她的最東邊的島嶼——Kap
Cutcha，位於下級區域[Tutuala](../Page/Tutuala.md "wikilink")，及一個小島嶼[雅科島](../Page/雅科島.md "wikilink")。

|                                                                                                    |                                                                                                    |
| -------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| [Lautém_klein1.jpg](https://zh.wikipedia.org/wiki/File:Lautém_klein1.jpg "fig:Lautém_klein1.jpg") | [Lautém_klein2.jpg](https://zh.wikipedia.org/wiki/File:Lautém_klein2.jpg "fig:Lautém_klein2.jpg") |

## 外部連結

  - [Fataluku language
    website](https://web.archive.org/web/20051218172306/http://www.ling.hawaii.edu/~uhdoc/FatalukuWeb/Fataluku.html)
  - [Ethnologue page for
    Fataluku](http://www.ethnologue.com/show_language.asp?code=ddg)

[Category:东帝汶行政区划](../Category/东帝汶行政区划.md "wikilink")