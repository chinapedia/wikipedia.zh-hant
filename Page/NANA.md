《**NANA**》（）是日本漫畫家[矢澤愛的](../Page/矢澤愛.md "wikilink")[少女漫畫作品](../Page/少女漫畫.md "wikilink")，自2000年起開始連載於《[Cookie](../Page/Cookie_\(雜誌\).md "wikilink")》，2009年時因作者健康問題休載至今。故事的主角是兩個名字同樣讀作「NANA」的少女。漫畫推出之後大受歡迎，除了在2006年時被改編成電視動畫外，也在2005年與2006年時分別改拍成兩部電影。

## 主要角色

## 漫畫原作

《NANA》從1999年開始在屬於[集英社漫畫月刊](../Page/集英社.md "wikilink")《[Cookie](../Page/Cookie_\(雜誌\).md "wikilink")》上連載。但由於矢澤愛的健康問題，該漫畫已於2009年六月起休載，復刊日期未定。截至休載為止，共計連載84回。

自開始連載以來已經出版21本單行本，累計的發行本數達到4,360萬本\[1\]。該漫畫涉及的話題十分廣泛，涵蓋友情、愛情、人生理想、搖滾音樂、流行品牌、生活態度等領域，故事中人物的身份從學生到樂團、藝人都有。豐富的內容主題，再加上劇情鋪陳上使用了漫畫作品少見的倒敘與跳敘法，使得此作品得以吸引許多不同領域的讀者群，除了原本少女漫畫既有的讀者群外，在成年女性乃至於男性讀者間也獲得相當程度支持。在2008年一個由[Oricon所舉辦](../Page/Oricon.md "wikilink")、針對中學至20歲世代女性讀者的問卷調查中，《NANA》在「最適合當作戀愛範本的漫畫」排行中獲得冠軍。

《NANA》的漫畫隨後在台灣、香港等地以中文版本發行。並且2005年3月在美國少女漫畫雜誌《少女Beat》中正式連載。

該漫畫獲得2002年度（第48回）[小學館漫畫獎](../Page/小學館漫畫獎.md "wikilink")。

### 改編作品

#### 製作人員

  - 原作：[矢澤愛](../Page/矢澤愛.md "wikilink")
  - 監督：[浅香守生](../Page/浅香守生.md "wikilink")
  - 系列構成：[金春智子](../Page/金春智子.md "wikilink")
  - 角色設計：[濱田邦彥](../Page/濱田邦彥.md "wikilink")
  - 美術監督：清水友幸
  - 色彩設計：角本百合子
  - 音響監督：[三間雅文](../Page/三間雅文.md "wikilink")
  - 攝影監督：增元由紀大
  - 音樂：[長谷川智樹](../Page/長谷川智樹.md "wikilink")
  - 動畫製作：[MADHOUSE](../Page/MADHOUSE.md "wikilink")
  - 製作著作：[日本電視台](../Page/日本電視台.md "wikilink")、[VAP](../Page/VAP.md "wikilink")、[集英社](../Page/集英社.md "wikilink")、MADHOUSE

### 動畫集數列表

<table>
<thead>
<tr class="header">
<th><p>播放順序</p></th>
<th><p>標題（日文）</p></th>
<th><p>標題（中文）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1話</p></td>
<td></td>
<td><p>序章 奈奈和娜娜 相遇 |- </p></td>
</tr>
<tr class="even">
<td><p>第3話</p></td>
<td></td>
<td><p>奈奈和章司 愛情進展 |- </p></td>
</tr>
<tr class="odd">
<td><p>第5話</p></td>
<td></td>
<td><p>蓮的夢想 娜娜的思念 |- </p></td>
</tr>
<tr class="even">
<td><p>第7話</p></td>
<td></td>
<td><p>707號房間 泰的登場 |- </p></td>
</tr>
<tr class="odd">
<td><p>第9話</p></td>
<td></td>
<td><p>伸夫上京 娜娜的歌聲 |- </p></td>
</tr>
<tr class="even">
<td><p>第11話</p></td>
<td></td>
<td><p>幸子 故意的 |- </p></td>
</tr>
<tr class="odd">
<td><p>第13話</p></td>
<td></td>
<td><p>急接近 章司和幸子 |- </p></td>
</tr>
<tr class="even">
<td><p>第15話</p></td>
<td></td>
<td><p>家庭餐館的修羅戰塵 |- </p></td>
</tr>
<tr class="odd">
<td><p>第17話</p></td>
<td></td>
<td><p>娜娜愛的去向 |- </p></td>
</tr>
<tr class="even">
<td><p>第19話</p></td>
<td></td>
<td><p>奈奈的祈禱 娜娜的想法 |- </p></td>
</tr>
<tr class="odd">
<td><p>第21話</p></td>
<td></td>
<td><p>急展開 阿八的命運 |- </p></td>
</tr>
<tr class="even">
<td><p>第23話</p></td>
<td></td>
<td><p>淳子的房間(2) |- </p></td>
</tr>
<tr class="odd">
<td><p>第25話</p></td>
<td></td>
<td><p>不想讓給任何人 |- </p></td>
</tr>
<tr class="even">
<td><p>第27話</p></td>
<td></td>
<td><p>反覆無常 任性的男人 |- </p></td>
</tr>
<tr class="odd">
<td><p>第29話</p></td>
<td></td>
<td><p>阿八想要的未來 |- </p></td>
</tr>
<tr class="even">
<td><p>第31話</p></td>
<td></td>
<td><p>愛情表現的問題 |-</p></td>
</tr>
<tr class="odd">
<td><p>第33話</p></td>
<td></td>
<td><p>八子懷孕 |-</p></td>
</tr>
<tr class="even">
<td><p>第35話</p></td>
<td></td>
<td><p>阿八的選擇 |-</p></td>
</tr>
<tr class="odd">
<td><p>第37話</p></td>
<td></td>
<td><p>蕾拉的孤獨 |-</p></td>
</tr>
<tr class="even">
<td><p>第39話</p></td>
<td></td>
<td><p>淳子的部屋(3) |-</p></td>
</tr>
<tr class="odd">
<td><p>第41話</p></td>
<td></td>
<td><p>命運的扳機 |-</p></td>
</tr>
<tr class="even">
<td><p>第43話</p></td>
<td></td>
<td><p>BLAST,出道 |-</p></td>
</tr>
<tr class="odd">
<td><p>第45話</p></td>
<td></td>
<td><p>娜娜,突然病發 |-</p></td>
</tr>
<tr class="even">
<td><p>第47話</p></td>
<td></td>
<td><p>BLAST VS TRAPNEST |-</p></td>
</tr>
<tr class="odd">
<td><p>第49話</p></td>
<td></td>
<td><p>再會！阿八和章司 |-</p></td>
</tr>
</tbody>
</table>

#### 播映時間

該作品動畫版的播映時間如下：

  - 日本：2006年4月5日起，由[日本電視台全國聯播網](../Page/日本電視台.md "wikilink") -
    [日本時間每星期三](../Page/日本標準時間.md "wikilink")23:55播出（部分地區播映時間不同）。目前第一季（共50集）已播映完畢。
  - [台灣](../Page/台灣.md "wikilink")：2007年1月11日起，由[中視無線台](../Page/中視無線台.md "wikilink")
    -
    [台灣時間每星期四](../Page/台灣時間.md "wikilink")21:30至22:30雙語播出，後改為每星期五23:30至24:00以日語原聲播出。已播映完畢。
  - [香港](../Page/香港.md "wikilink")：2008年8月6日起，由[無綫電視](../Page/無綫電視.md "wikilink")[J2台](../Page/J2台.md "wikilink")
    -
    [香港時間每星期三](../Page/香港時間.md "wikilink")、四23:30至23:55，以粵／日雙語播出，重播時段為每星期四、五04:30至04:55。已播映完畢。

### 電影

  - 《[NANA](../Page/Nana_\(電影\).md "wikilink")》：2005年9月3日上映，[東京放送發行](../Page/東京放送.md "wikilink")。主要演員包括[中島美嘉](../Page/中島美嘉.md "wikilink")、[宫崎葵](../Page/宫崎葵.md "wikilink")、[松田龍平](../Page/松田龍平.md "wikilink")、[平岡祐太](../Page/平岡祐太.md "wikilink")、[成宫寬貴](../Page/成宫寬貴.md "wikilink")、[丸山智己](../Page/丸山智己.md "wikilink")、[松山研一等人](../Page/松山研一.md "wikilink")。
  - 《[NANA
    2](../Page/NANA_2.md "wikilink")》：2006年12月6日上映，[東寶發行](../Page/東寶株式會社.md "wikilink")。主要演員包括[中島美嘉](../Page/中島美嘉.md "wikilink")、[市川由衣](../Page/市川由衣.md "wikilink")、[成宮寬貴](../Page/成宮寬貴.md "wikilink")、[玉山鐵二](../Page/玉山鐵二.md "wikilink")、[本鄉奏多](../Page/本鄉奏多.md "wikilink")、[姜暢雄](../Page/姜暢雄.md "wikilink")、[丸山智己等人](../Page/丸山智己.md "wikilink")。

### 音樂專輯

  - 《**Punk Night from NANA**》(2003年9月26日)

<!-- end list -->

  -
    根據NANA第四集中的live演唱為主題

<!-- end list -->

  - 《**NANA's song is my song**》(2003年11月6日)

<!-- end list -->

1.  **マーブル**/Battle Bomb Rounge
2.  **君ノ名前**/SAVEGE GENIUS
3.  **楽園の扉**/プルカブ
4.  **親愛**/ちひろいず
5.  **Lotus Blues**/Olive
6.  **Thanks**/Bama☆sister
7.  **問い**/ブルウフロッグ
8.  **Not be mine**/GREEN BEAR
9.  **奏でる神**/RK ROSEBUD
10. **NO TITLE**/山岡千春
11. **真夜中の明日**/柿本七恵
12. **MY WAY**/REALIZE POWER WAVE
13. **あたしの花**/Ann

<!-- end list -->

  - 《**LOVE for NANA\~Only 1 Tribute\~**》(2005年3月16日)

<!-- end list -->

  -
    分為「BLACK STONE」與「TRAPNEST」兩種包裝，但所收錄的曲目相同。

<!-- end list -->

1.  **BEAT 7〜The Theme of LOVE for NANA〜**/ 高見沢俊彦
2.  **GIMME ALL OF YOUR LOVE \!\!**/ Tommy heavenly6 for BLACK STONES
3.  **Twinkle**/ 木村カエラ for BLACK STONES
4.  **REVERSE**/ TETSU69 for TRAPNEST
5.  **stay away**/ abingdon boys school for BLACK STONES
6.  **I miss you?**/ Do As Infinity for BLACK STONES
7.  **バンビーノ**/ 布袋寅泰 featuring もりばやしみほ for TRAPNEST
8.  **Sleepwalking**/ グレン・マトロック& The Philistines featuring Holly Coock
    (from セックス・ピストルズ)
9.  **Sugar Guitar**/ SKYE SWEETNAM for TRAPNEST
10. **黎明時代-レイメイジダイ-**/ ジャパハリネット for BLACK STONE
11. **BLACK CROW**/ SEX MACHINEGUNS for BLACK STONES
12. **Two Hearts**/ ZONE for TRAPNEST
13. **Cherish**/ 大塚愛 for TRAPNEST

<!-- end list -->

  - 《**NANA BEST**》(2007年3月21日)

<!-- end list -->

  -
    收錄由土屋安娜和Olivia為動畫主唱的所有片頭曲和片尾曲、插曲和未發表過的歌曲。

<!-- end list -->

1.  **rose**/ ANNA TSUCHIYA inspi'NANA(BLACK STONES)
2.  **zero**/ ANNA TSUCHIYA inspi'NANA(BLACK STONES)
3.  **Wish**/ OLIVIA inspi'REIRA(TRAPNEST)
4.  **Starless Night**/ OLIVIA inspi'REIRA(TRAPNEST)
5.  **黒い涙**/ ANNA TSUCHIYA inspi'NANA(BLACK STONES)
6.  **LUCY**/ ANNA TSUCHIYA inspi'NANA(BLACK STONES)
7.  **stand by me**/ ANNA TSUCHIYA inspi'NANA(BLACK STONES)
8.  **Shadow of Love**/ OLIVIA inspi'REIRA(TRAPNEST)
9.  **Winter sleep**/ OLIVIA inspi'REIRA(TRAPNEST)
10. **ANARCHY IN THE UK**/ ANNA TSUCHIYA inspi'NANA(BLACK STONES)
11. **LUCY-Studio Live Version-**/ ANNA TSUCHIYA inspi'NANA(BLACK
    STONES)
12. **Nothing's gonna take my love**/ OLIVIA inspi'REIRA(TRAPNEST)
13. **Recorded Butterflies-Live Version-**/ OLIVIA inspi'REIRA(TRAPNEST)
14. **A little pain**/OLIVIA inspi'REIRA

### 電子遊戲

  - 《NANA》，[PS2遊戲](../Page/PS2.md "wikilink")，2005年3月17日由[科乐美發行](../Page/科乐美.md "wikilink")。
  - 《NANA 全都是大魔王的導引嗎！？》，[PSP遊戲](../Page/PSP.md "wikilink")，2006年6月7日發行。
  - 《NANA
    現場演唱會工作人員大募集！歡迎新手》，[NDS遊戲](../Page/NDS.md "wikilink")，2007年6月21日發行。

## 參考文獻

## 外部連結

  - [NANA漫畫官方網站](http://annex.s-manga.net/s-nana/index.html)

  - [KONAMI遊戲官方網站](http://www.konami.jp/gs/game/nana/)

[\*](../Category/NANA.md "wikilink")

1.