**柳葉菜科**（Onagraceae），一年生[挺水植物](../Page/挺水植物.md "wikilink")，高約10至兩公尺以上。莖通常有稜，在葉子方面則呈現互生，花單生、果通常為橢圓。一般來說，該種植物都生長在季節性溼地，水田溝渠。

## 属

### Ludwigioideae

  - **[丁香蓼屬](../Page/丁香蓼屬.md "wikilink")**（Ludwigia）
    <small>[L.](../Page/卡尔·林奈.md "wikilink")</small>\[1\]

### Onagroideae

<table>
<tbody>
<tr class="odd">
<td><dl>
<dt>族（Tribe） <a href="../Page/露珠草族.md" title="wikilink">露珠草族</a>（Circaeeae）</dt>

</dl>
<ul>
<li><a href="../Page/露珠草屬.md" title="wikilink">露珠草屬</a>(<em>Circaea</em>)<small>L.</small></li>
<li><a href="../Page/倒掛金鐘屬.md" title="wikilink">倒掛金鐘屬</a>（<em>Fuchsia</em>） <small>L.</small>[2]</li>
</ul>
<dl>
<dt><a href="../Page/柳叶菜族.md" title="wikilink">柳叶菜族</a>（Epilobieae）</dt>

</dl>
<ul>
<li>(<em>Chamerion</em>) <small>Raf. ex Holub</small></li>
<li><a href="../Page/柳葉菜屬.md" title="wikilink">柳葉菜屬</a>（<em>Epilobium</em>） <small>L.</small>[3]</li>
</ul>
<dl>
<dt><a href="../Page/Gongylocarpus.md" title="wikilink">Gongylocarpeae族</a></dt>

</dl>
<ul>
<li><em>Gongylocarpus</em> <small>Schltdl. &amp; Cham.</small>[4]</li>
</ul>
<dl>
<dt><a href="../Page/Hauya.md" title="wikilink">Hauyeae族</a></dt>

</dl>
<ul>
<li><em>Hauya</em> <small>DC.</small>[5]</li>
</ul>
<dl>
<dt>Lopezieae族</dt>

</dl>
<ul>
<li><em>Lopezia</em> <small>Cav.</small></li>
<li><em>Megacorax</em> <small>Elizondo et al.</small>[6]</li>
</ul>
<dl>
<dt><a href="../Page/Onagreae.md" title="wikilink">Onagreae族</a></dt>

</dl></td>
<td><ul>
<li><em><a href="../Page/Camissonia.md" title="wikilink">Camissonia</a></em> <small>Link</small></li>
<li><em><a href="../Page/Camissoniopsis.md" title="wikilink">Camissoniopsis</a></em> <small>W.L.Wagner &amp; Hoch</small></li>
<li><em><a href="../Page/Chylismia.md" title="wikilink">Chylismia</a></em> <small>(Torr. &amp; A.Gray) Raim.</small></li>
<li><em><a href="../Page/Chylismiella.md" title="wikilink">Chylismiella</a></em> <small>(Munz) W.L.Wagner &amp; Hoch</small></li>
<li><a href="../Page/克拉花屬.md" title="wikilink">克拉花屬</a>（<em>Clarkia</em>）<small>Pursh</small></li>
<li><em><a href="../Page/Eremothera.md" title="wikilink">Eremothera</a></em> <small>(P.H.Raven) W.L.Wagner &amp; Hoch</small></li>
<li><em><a href="../Page/Eucharidium.md" title="wikilink">Eucharidium</a></em> <small>Fisch. &amp; C.A.Mey.</small></li>
<li><em><a href="../Page/Eulobus.md" title="wikilink">Eulobus</a></em> <small>Nutt. ex Torr. &amp; A.Gray</small></li>
<li><em><a href="../Page/Gayophytum.md" title="wikilink">Gayophytum</a></em> <small>A.Juss.</small></li>
<li><em><a href="../Page/Neoholmgrenia.md" title="wikilink">Neoholmgrenia</a></em> <small>W.L.Wagner &amp; Hoch</small></li>
<li><a href="../Page/月見草屬.md" title="wikilink">月見草屬</a>(<em>Oenothera</em>) <small>L.</small></li>
<li><em><a href="../Page/Taraxia.md" title="wikilink">Taraxia</a></em> <small>(Torr. &amp; A.Gray) Raim.</small></li>
<li><em><a href="../Page/Tetrapteron.md" title="wikilink">Tetrapteron</a></em> <small>(Munz) W.L. Wagner &amp; Hoch</small></li>
<li><em><a href="../Page/Xylonagra.md" title="wikilink">Xylonagra</a></em> <small>Donn.Sm. &amp; Rose</small>[7]</li>
</ul></td>
</tr>
</tbody>
</table>

Several other genera are synonymized in the classification presented
above, but commonly appear in the literature. Synonymized genera
include:

  - *[Calylophus](../Page/Calylophus.md "wikilink")*
    <small>Spach</small> (= *Oenothera*)\[8\]
  - [山桃草屬](../Page/山桃草屬.md "wikilink")（*Gaura*） <small>L.</small> (=
    *Oenothera*)\[9\]

## 图库

Circaea alpina 6769.JPG|*[高山露珠草](../Page/高山露珠草.md "wikilink")* Epilobium
alpestre 2.jpg|*[Epilobium
alpestre](../Page/Epilobium_alpestre.md "wikilink")* Fuchsia Magellanica
Tas.jpg|*[倒挂金钟](../Page/倒挂金钟.md "wikilink")* IMG 5989-Hauya
heydeana.jpg|*[Hauya heydeana](../Page/Hauya_heydeana.md "wikilink")*
Antioch Dunes Evening Primrose.jpg|[*Oenothera deltoides* ssp.
*howellii*](../Page/Oenothera_deltoides_subsp._howellii.md "wikilink")
Oenothera rubricaulis 2014 G1.jpg|*[月见草](../Page/月见草.md "wikilink")*

## 參考文獻

  -
## 外部链接

  - [Smithsonian Institution: Onagraceae
    classification](https://web.archive.org/web/20070819170606/http://ravenel.si.edu/botany/onagraceae/index.htm)

[柳叶菜科](../Category/柳叶菜科.md "wikilink")
[Category:一年生植物](../Category/一年生植物.md "wikilink")
[Category:挺水植物](../Category/挺水植物.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.