**白腹鸛**（[學名](../Page/學名.md "wikilink")：*Ciconia
abdimii*），是原產[非洲和西南](../Page/非洲.md "wikilink")[阿拉伯半島的一種小型鸛類](../Page/阿拉伯.md "wikilink")。棲較乾旱的地帶。食昆蟲，尤喜[蝗蟲](../Page/蝗蟲.md "wikilink")。

## 外形

它是[鸛屬中最小的鹳](../Page/鸛屬.md "wikilink")，成体长约73厘米，重量仅1千克。\[1\]

## 图集

Image:Abdim's Stork RWD.jpg|[圣迭戈动物园的白腹鸛](../Page/圣迭戈动物园.md "wikilink")
Image:Cigogne d'Abdim MHNT.jpg|[圖盧茲博物馆](../Page/圖盧茲.md "wikilink")

## 参考文献

[Category:鸛屬](../Category/鸛屬.md "wikilink")
[Category:撒哈拉以南非洲鳥類](../Category/撒哈拉以南非洲鳥類.md "wikilink")

1.