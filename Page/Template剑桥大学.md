**校長：**

|group2 = [成員书院
<small>又譯“學院”</small>](../Page/剑桥大学书院列表.md "wikilink") |list2 =
[基督学院](../Page/剑桥大学基督学院.md "wikilink"){{.w}}[丘吉尔学院](../Page/剑桥大学丘吉尔学院.md "wikilink"){{.w}}[克莱尔学院](../Page/剑桥大学克莱尔学院.md "wikilink"){{.w}}[克莱尔学堂](../Page/剑桥大学克莱尔学堂.md "wikilink"){{.w}}[科珀斯克里斯蒂学院](../Page/剑桥大学科珀斯克里斯蒂学院.md "wikilink"){{.w}}[达尔文学院](../Page/剑桥大学达尔文学院.md "wikilink"){{.w}}[唐宁学院](../Page/剑桥大学唐宁学院.md "wikilink"){{.w}}[伊曼纽尔学院](../Page/剑桥大学伊曼纽尔学院.md "wikilink"){{.w}}[菲茨威廉学院](../Page/剑桥大学菲茨威廉学院.md "wikilink"){{.w}}[格顿学院](../Page/剑桥大学格顿学院.md "wikilink"){{.w}}[冈维尔与凯斯学院](../Page/劍橋大學岡維爾與凱斯學院.md "wikilink"){{.w}}[哈默顿学院](../Page/剑桥大学哈默顿学院.md "wikilink"){{.w}}[休斯学院](../Page/剑桥大学休斯学院.md "wikilink"){{.w}}[耶稣学院](../Page/剑桥大学耶稣学院.md "wikilink"){{.w}}[英皇学院](../Page/剑桥大学英皇学院.md "wikilink"){{.w}}[露西·卡文迪许学院](../Page/剑桥大学露西·卡文迪许学院.md "wikilink"){{.w}}[莫德林学院](../Page/剑桥大学莫德林学院.md "wikilink"){{.w}}[默里·爱德华兹学院](../Page/剑桥大学默里·爱德华兹学院.md "wikilink"){{.w}}[纽纳姆学院](../Page/剑桥大学纽纳姆学院.md "wikilink"){{.w}}[彭布罗克学院](../Page/剑桥大学彭布罗克学院.md "wikilink"){{.w}}[彼得学院](../Page/剑桥大学彼得学院.md "wikilink"){{.w}}[王后学院](../Page/剑桥大学王后学院.md "wikilink"){{.w}}[罗宾森学院](../Page/剑桥大学罗宾森学院.md "wikilink"){{.w}}[圣凯瑟琳学院](../Page/剑桥大学圣凯瑟琳学院.md "wikilink"){{.w}}[圣埃德蒙学院](../Page/剑桥大学圣埃德蒙学院.md "wikilink"){{.w}}[圣约翰学院](../Page/剑桥大学圣约翰学院.md "wikilink"){{.w}}[塞尔文学院](../Page/剑桥大学塞尔文学院.md "wikilink"){{.w}}[悉尼·萨塞克斯学院](../Page/剑桥大学悉尼·萨塞克斯学院.md "wikilink"){{.w}}[三一学院](../Page/剑桥大学三一学院.md "wikilink"){{.w}}[三一学堂](../Page/剑桥大学三一学堂.md "wikilink"){{.w}}[沃尔森学院](../Page/剑桥大学沃尔森学院.md "wikilink")

|group3 = **學術學院**/
**院系** | list3 =

  - 艺术与人文学院
  - 人文与社会科学学院
  - 科技学院
  - 物理科学学院
  - 临床医学学院
  - 生物科学学院
  - [賈吉商學院](../Page/賈吉商學院.md "wikilink")
  - [劍橋大學出版社](../Page/劍橋大學出版社.md "wikilink")
  - [卡文迪许实验室](../Page/卡文迪许实验室.md "wikilink")
  - [分子生物学实验室](../Page/分子生物学实验室.md "wikilink")
  - [史考特極地研究中心](../Page/史考特極地研究中心.md "wikilink")
  - [维康桑格研究所](../Page/维康桑格研究所.md "wikilink")

|group4 = **学生生活** | list4 =

  -
  -
  -
  - [剑桥使徒](../Page/剑桥使徒.md "wikilink")

  - [脚灯社](../Page/脚灯社.md "wikilink")

|group5 = **隶属** |list5 =

  - [阿登布鲁克医院](../Page/阿登布鲁克医院.md "wikilink")

|belowclass = hlist |below =

  - **[剑桥大学](http://www.cam.ac.uk)**
  - **[剑桥学生会](http://www.cusu.cam.ac.uk/)**
  - **[研究生会](http://www.gradunion.cam.ac.uk/)**

}}<noinclude>

</noinclude>

[\*](../Category/剑桥大学.md "wikilink")
[Category:英国高校导航模板](../Category/英国高校导航模板.md "wikilink")