**周明镇**（），[古脊椎动物学家](../Page/古脊椎动物学家.md "wikilink")，[中国](../Page/中国.md "wikilink")[恐龙研究之父](../Page/恐龙.md "wikilink")，1980年当选中国科学院学部委员（院士）\[1\]。

## 生平

1943年毕业于[重庆大学地质系](../Page/重庆大学.md "wikilink")，[邁阿密大學硕士](../Page/邁阿密大學_\(佛罗里达州\).md "wikilink")，1950年在美国[理海大学获生物学博士学位](../Page/理海大学.md "wikilink")。曾在[重庆大学](../Page/重庆大学.md "wikilink")、台湾地质调查所、[山东大学](../Page/山东大学.md "wikilink")、[南开大学任教和研究](../Page/南开大学.md "wikilink")。曾任[中国科学院古脊椎动物与古人类研究所所长](../Page/中国科学院.md "wikilink")，国际古生物协会（）副主席、人类起源研究所（美国）研究员、中国自然科学博物馆协会理事长、中国兽类学会副理事长。

1996年1月4日上午11时20分在北京逝世，享年77岁。

## 参考文献

[Category:中華人民共和國地質學家](../Category/中華人民共和國地質學家.md "wikilink")
[Category:中国古生物学家](../Category/中国古生物学家.md "wikilink")
[Category:重庆大学教授](../Category/重庆大学教授.md "wikilink")
[Category:山东大学教授](../Category/山东大学教授.md "wikilink")
[Category:南开大学教授](../Category/南开大学教授.md "wikilink")
[Category:重庆大学校友](../Category/重庆大学校友.md "wikilink")
[Category:迈阿密大学校友](../Category/迈阿密大学校友.md "wikilink")
[Category:国立山东大学校友](../Category/国立山东大学校友.md "wikilink")
[Category:山东大学校友](../Category/山东大学校友.md "wikilink")
[Category:理海大學校友](../Category/理海大學校友.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:九三学社社员](../Category/九三学社社员.md "wikilink")
[Category:周姓](../Category/周姓.md "wikilink")

1.