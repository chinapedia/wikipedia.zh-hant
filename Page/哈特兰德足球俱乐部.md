**哈特蘭德足球俱樂部**（**Heartland FC**）
是尼日利亚足球俱乐部，位于[尼日利亚](../Page/尼日利亚.md "wikilink")[奥韦里](../Page/奥韦里.md "wikilink")，球队最初叫做斯巴达足球俱乐部，之后更名为
Iwuanyanwu 民族，俱乐部在 1987–1990年连续 4
次夺得[尼日利亞足球甲級聯賽冠军](../Page/尼日利亞足球甲級聯賽.md "wikilink")
3
次蝉联[1](http://sports123.com/foo/mngr.html)。球队的主场为[丹·安伊亚姆体育场](../Page/丹·安伊亚姆体育场.md "wikilink")(Dan
Anyiam Stadium).球會在2006年将球队名字更名为现在的「哈特兰德」。

## 战绩

  - **[尼日利亚足球甲级联赛](../Page/尼日利亚足球甲级联赛.md "wikilink"): 5次**

<!-- end list -->

  -

      -
        1987年, 1988年, 1989年, 1990年, 1993年

<!-- end list -->

  - **[尼日利亚足协杯](../Page/尼日利亚足协杯.md "wikilink"): 1次**

<!-- end list -->

  -

      -
        1988年

[Category:尼日利亚足球俱乐部](../Category/尼日利亚足球俱乐部.md "wikilink")