**肉豆蔻科**(Myristicaceae)是一类[显花植物](../Page/被子植物门.md "wikilink")，《[克朗奎斯特分类法](../Page/克朗奎斯特分类法.md "wikilink")》和2003年的《[APG
II
分类法](../Page/APG_II_分类法.md "wikilink")》都将其分入[木兰目](../Page/木兰目.md "wikilink")，是[被子植物中比较原始的一类](../Page/被子植物.md "wikilink")。

肉豆蔻科包括大约有20个[属](../Page/属.md "wikilink")，几百[种](../Page/种.md "wikilink")，有[灌木也有](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")，主要生长在[热带地区](../Page/热带.md "wikilink")，全球的热带地区都有分布，最常见的是[肉豆蔻属和](../Page/肉豆蔻属.md "wikilink")[蔻木属](../Page/蔻木属.md "wikilink")。

## 分类

各属：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><em><a href="../Page/Bicuiba.md" title="wikilink">Bicuiba</a></em></li>
<li><em><a href="../Page/Brochoneura.md" title="wikilink">Brochoneura</a></em></li>
<li><em><a href="../Page/Cephalosphaera.md" title="wikilink">Cephalosphaera</a></em></li>
<li><em><a href="../Page/Coelocaryon.md" title="wikilink">Coelocaryon</a></em></li>
<li><em><a href="../Page/Compsoneura.md" title="wikilink">Compsoneura</a></em></li>
<li><a href="../Page/内毛楠属.md" title="wikilink">内毛楠属</a> <em>Endocomia</em></li>
<li><em><a href="../Page/Gymnacranthera.md" title="wikilink">Gymnacranthera</a></em></li>
</ul></td>
<td><ul>
<li><em><a href="../Page/Haematodendron.md" title="wikilink">Haematodendron</a></em></li>
<li><a href="../Page/风吹楠属.md" title="wikilink">风吹楠属</a> <em>Horsfieldia</em></li>
<li><a href="../Page/热美肉豆蔻属.md" title="wikilink">热美肉豆蔻属</a> <em>Iryanthera</em></li>
<li><a href="../Page/红光树属.md" title="wikilink">红光树属</a> <em>Knema</em></li>
<li><em><a href="../Page/Maloutchia.md" title="wikilink">Maloutchia</a></em></li>
<li><a href="../Page/肉豆蔻属.md" title="wikilink">肉豆蔻属</a> <em>Myristica</em></li>
</ul></td>
<td><ul>
<li><em><a href="../Page/Osteophloeum.md" title="wikilink">Osteophloeum</a></em></li>
<li><em><a href="../Page/Otoba.md" title="wikilink">Otoba</a></em></li>
<li><em><a href="../Page/Pycnanthus.md" title="wikilink">Pycnanthus</a></em></li>
<li><em><a href="../Page/Scyphocephalium.md" title="wikilink">Scyphocephalium</a></em></li>
<li><a href="../Page/西非肉豆蔻属.md" title="wikilink">西非肉豆蔻属</a> <em>Staudtia</em></li>
<li><a href="../Page/蔻木属.md" title="wikilink">蔻木属</a><em>Virola</em></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [肉豆蔻科](http://delta-intkey.com/angio/www/myristic.htm)
  - [植物网](http://www.efloras.org/florataxon.aspx?flora_id=900&taxon_id=10596)
  - [NCBI
    分类法](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=22274&lvl=3&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/肉豆蔻科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")