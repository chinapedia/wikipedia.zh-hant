**临澧縣**位于[湖南省西北部](../Page/湖南省.md "wikilink")，居[武陵山古陆与](../Page/武陵山.md "wikilink")[洞庭湖断陷盆地连接部位](../Page/洞庭湖.md "wikilink")。[亚热带气候](../Page/亚热带气候.md "wikilink")，四季温差明显。地貌以微[丘](../Page/丘.md "wikilink")、[平原为主](../Page/平原.md "wikilink")，间有[山岳](../Page/山岳.md "wikilink")。古属荆楚之地。[雍正七年](../Page/雍正.md "wikilink")（1729年），始设县，称安福县。[中华民国](../Page/中华民国.md "wikilink")3年（1914年），改称临澧县。现为[常德市管辖](../Page/常德市.md "wikilink")。全[县总](../Page/县.md "wikilink")[面积](../Page/面积.md "wikilink")1209.58[平方公里](../Page/平方公里.md "wikilink")；耕地面积36750[公顷](../Page/公顷.md "wikilink")（2009年）；全年[GDP](../Page/GDP.md "wikilink")（国内生产总值）总量30.91亿元（2003年），人口451,435人（2009年）。

## 行政区划

辖18个[乡](../Page/乡.md "wikilink")[镇](../Page/镇.md "wikilink")（区），341个[村](../Page/村.md "wikilink")[（居）委会](../Page/居民委员会.md "wikilink")：安福镇(原城关镇)、合口镇、新安镇、佘市桥镇、太浮镇、四新岗镇、停弦渡镇、修梅镇、杉板乡、文家乡、陈二乡、柏枝乡、烽火乡、望城乡、杨板乡、官亭乡、九里乡。

## 历史

**临澧**作为[行政建制最早始于](../Page/行政区划.md "wikilink")[清朝](../Page/清朝.md "wikilink")[雍正七年](../Page/雍正.md "wikilink")（1729年），裁[九溪卫](../Page/九溪卫.md "wikilink")、[永定卫和](../Page/永定卫.md "wikilink")[澧州地一部设](../Page/澧州.md "wikilink")[县](../Page/县.md "wikilink")，取安福旧所名为“安福县”；[民国](../Page/民国.md "wikilink")3年（1914年）1月，因与[江西省](../Page/江西省.md "wikilink")[安福县同名](../Page/安福县.md "wikilink")，改名“临澧县”至今。

## 外部链接

  - [临澧县人民政府](http://www.linli.gov.cn/)
  - [临澧新闻网](http://www.linli.cn/)

[临澧县](../Category/临澧县.md "wikilink")
[县](../Category/常德区县市.md "wikilink")
[常德](../Category/湖南省县份.md "wikilink")