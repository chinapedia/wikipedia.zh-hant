[John_Henry_Newman_by_Sir_John_Everett_Millais,_1st_Bt.jpg](https://zh.wikipedia.org/wiki/File:John_Henry_Newman_by_Sir_John_Everett_Millais,_1st_Bt.jpg "fig:John_Henry_Newman_by_Sir_John_Everett_Millais,_1st_Bt.jpg")[Coat_of_arms_of_John_Henry_Newman.svg](https://zh.wikipedia.org/wiki/File:Coat_of_arms_of_John_Henry_Newman.svg "fig:Coat_of_arms_of_John_Henry_Newman.svg")
真福**若望·亨利·紐曼**（，）原為[聖公會的](../Page/聖公會.md "wikilink")[牧師](../Page/牧師.md "wikilink")，在1845年皈依[羅馬天主教](../Page/羅馬天主教.md "wikilink")，並領司鐸品，成為一位天主教神父。後來於1879年被[教宗](../Page/教宗.md "wikilink")[良十三世擢升為](../Page/良十三世.md "wikilink")[樞機](../Page/樞機.md "wikilink")，不過他並未被祝聖為主教，而是以司鐸的身分獲得執事級樞機頭銜。他學問淵博，且勇敢討論許多有關宗教信仰等問題，深入探討信仰本質及教義的發展。他可以深刻討論理性、情感、想像力與信仰的關係，雖然他知道人的智力是有極限但他仍勇敢地為理智辯護。年青時已是英國教會[牛津運動的重要人物](../Page/牛津運動.md "wikilink")，帶領被新教同化了的英國教會重拾[大公教會的源頭與核心價值](../Page/大公教會.md "wikilink")，重整短暫失落了的禮儀、體制、神學和聖樂。他對於羅馬天主教的影響相當大，尤其在[第二次梵蒂岡會議紐曼的思想深具影響力](../Page/第二次梵蒂岡會議.md "wikilink")，所以又有人稱梵蒂岡第二次會議為紐曼大會。\[1\]

紐曼在2010年9月19日獲[教宗](../Page/教宗.md "wikilink")[本篤十六世主持](../Page/本篤十六世.md "wikilink")[宣福禮](../Page/宣福禮.md "wikilink")，冊封為真福品。[慈光歌為其著名讚美詩歌](../Page/慈光歌.md "wikilink")。

## 生平

  - 1801年2月21日：出生於英國倫敦。
  - 1808年5月：進入伊林（Ealing）學校求學。
  - 1816年：在伊林的暑假期間，開始對神職生涯產生興趣。
  - 1817年6月：進入[牛津大學](../Page/牛津大學.md "wikilink")[聖三學院](../Page/聖三學院.md "wikilink")
  - 1824年6月：獲英國聖公會任命為執事，並在牛津大學聖格來孟教堂任職。
  - 1829年：被任命為牛津聖瑪麗教堂管理牧師。
  - 1832年：在[羅馬會晤](../Page/羅馬.md "wikilink")[Nicholas
    Wiseman](../Page/Nicholas_Wiseman.md "wikilink")。
  - 1833年7月：自義大利歸來後，在哈德萊（Hadleigh）會議中開始舉行[牛津運動](../Page/牛津運動.md "wikilink")。
  - 1843年：辭去聖瑪麗教堂的職務。
  - 1845年10月：與他的幾名學生一起進入天主教，赴[羅馬](../Page/羅馬.md "wikilink")[宗座傳信大學學習神哲學](../Page/宗座傳信大學.md "wikilink")。
  - 1847年：在[羅馬](../Page/羅馬.md "wikilink")[傳信部內的三賢士堂晉鐸為天主教神父](../Page/傳信部.md "wikilink")。
  - 1851年：任為[愛爾蘭](../Page/愛爾蘭.md "wikilink")[都柏林天主教大學校長](../Page/都柏林天主教大學.md "wikilink")。
  - 1858年：辭去都柏林天主教大學校長職務。
  - 1879年：教宗良十三（1878－1903）選任紐曼為樞機。
  - 1890年8月11日：在[艾格巴斯離世](../Page/艾格巴斯.md "wikilink")，享年八十九歲。

## 著作

  - *Apologia Pro Vita Sua*（意译「我為我的一生辯論」），1864年出版
  - 《大学的理想》(*The Idea of a University*)------此书为其一生最有名之著作

## 参考文献

## 参见

  - [牛津运动](../Page/牛津运动.md "wikilink")、[天主教沃尔辛厄姆圣母特别主教辖区](../Page/天主教沃尔辛厄姆圣母特别主教辖区.md "wikilink")
  - [第二次梵蒂冈大公会议](../Page/第二次梵蒂冈大公会议.md "wikilink")
  - [慈光歌](../Page/慈光歌.md "wikilink")

{{-}}

[Category:英國樞機](../Category/英國樞機.md "wikilink")
[Category:英国天主教神学家](../Category/英国天主教神学家.md "wikilink")
[Category:英格蘭天主教徒](../Category/英格蘭天主教徒.md "wikilink")
[Category:從聖公會皈依天主教](../Category/從聖公會皈依天主教.md "wikilink")
[Category:牛津运动](../Category/牛津运动.md "wikilink")
[Category:牛津大学三一学院校友](../Category/牛津大学三一学院校友.md "wikilink")
[Category:倫敦人](../Category/倫敦人.md "wikilink")
[Category:真福者](../Category/真福者.md "wikilink")

1.  麥可·柯林斯、馬修·普瑞斯《基督教的故事》，謝青峰、李文茹（台北市：台灣麥可股份有限公司，2005），181。