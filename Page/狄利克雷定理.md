在[數論中](../Page/數論.md "wikilink")，**狄利克雷定理**說明對於任意[互質的正整數](../Page/互質.md "wikilink")\(a,d\)，有無限多個[質數的形式如](../Page/質數.md "wikilink")\(a+nd\)，其中\(n\)為正整數，即在[算術級數](../Page/算術級數.md "wikilink")\(a+d,a+2d,a+3d,...\)中有無限多個質數——有無限個質數模\(d\)[同餘](../Page/同餘.md "wikilink")\(a\)。

## 相關定理

  - [歐幾里得證明了有無限個質數](../Page/歐幾里得.md "wikilink")，即有無限多個質數的形式如\(2n+1\)。
  - 算術級數的質數定理：若\(a,d\)互質，則有

<!-- end list -->

  -
    \(\sum_{p \le x \quad p \equiv a \pmod{d} } 1 \sim \frac{1}{\phi(d)} \frac{x}{\ln(x)}\)。

其中φ是[歐拉φ函數](../Page/歐拉φ函數.md "wikilink")。取\(d=2\)，可得一般的[質數定理](../Page/質數定理.md "wikilink")。

  - [Linnik定理說明了級數中最小的質數的範圍](../Page/Linnik定理.md "wikilink")：算術級數\(a+nd\)中最小的質數少於\(cd^L\)，其中\(L\)和\(c\)均為常數，但這兩個常數的最小值尚未找到。
  - [Chebotarev密度定理是在狄利克雷定理在](../Page/Chebotarev密度定理.md "wikilink")[伽羅瓦擴張的推廣](../Page/伽羅瓦擴張.md "wikilink")。

## 歷史

歐拉曾以\(\sum \frac{1}{p} = \infty\)，來證明質數有無限個。[約翰·彼得·狄利克雷得以靈感](../Page/約翰·彼得·狄利克雷.md "wikilink")，借助證明\(\sum_{p \equiv a \pmod{d} } {1/p} = \infty\)來證明算術級數中有無限個質數。這個定理的證明中引入了[狄利克雷L函數](../Page/狄利克雷L函數.md "wikilink")，應用了一些解析數學的技巧，是[解析數論的重要里程碑](../Page/解析數論.md "wikilink")。

## 參考

  - T. M. Apostol (1976). Introduction to Analytic Number Theory.
    Springer-Verlag. ISBN 0-387-90163-9. Chapter 7

[Category:解析数论](../Category/解析数论.md "wikilink")
[D](../Category/数学定理.md "wikilink")
[Category:素数](../Category/素数.md "wikilink")