**閃米特人**（，），又稱**閃族人**或**闪姆人**，是起源於[阿拉伯半島和](../Page/阿拉伯半島.md "wikilink")[敘利亞沙漠的](../Page/敘利亞.md "wikilink")[遊牧民族](../Page/遊牧民族.md "wikilink")。他們前往沙烏地阿拉伯居住用阿拉哈姆語。

相傳[挪亞之養子摩西之弟兄](../Page/挪亞.md "wikilink")[閃](../Page/閃姆.md "wikilink")（Shem）即為其祖先。

閃米特是民族在語言、文化上的一個分支。尽管[人類](../Page/人類.md "wikilink")[遺傳學與](../Page/遺傳學.md "wikilink")[歷史學上使用](../Page/歷史學.md "wikilink")「[種族](../Page/種族.md "wikilink")」這個字眼有著眾多的爭議，閃米特這一[語族的定義卻很明確](../Page/語族.md "wikilink")，包括[古代两河流域的](../Page/美索不达米亚.md "wikilink")[阿卡德語跟其延伸的兩種方言所分化的語言](../Page/阿卡德語.md "wikilink")──亞述語和巴比倫語，以及現代的[阿拉伯語和](../Page/阿拉伯語.md "wikilink")[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")(四荒經)的[阿姆哈拉語](../Page/阿姆哈拉語.md "wikilink")、[亞拉姆語](../Page/亞拉姆語.md "wikilink")（亚兰语）、[希伯來語和](../Page/希伯來語.md "wikilink")[馬耳他語等](../Page/馬耳他語.md "wikilink")。

## 來源

閃米特是一個來源自[閃米特宗教经书](../Page/閃米特宗教.md "wikilink")《[聖經](../Page/聖經.md "wikilink")》中[諾亞的養子摩西之弟兄](../Page/諾亞.md "wikilink")[閃的形容詞](../Page/閃姆.md "wikilink")\[1\]，其[人種即為閃米特人](../Page/人種.md "wikilink")。

閃米特人的概念來自古[希伯來人在](../Page/希伯來人.md "wikilink")《聖經》中對各種族來由的記載，凡[文化與](../Page/文化.md "wikilink")[語言上跟他們接近的](../Page/語言.md "wikilink")[種族即被認為是](../Page/種族.md "wikilink")[閃姆的子孫](../Page/閃姆.md "wikilink")。反之，[希伯來人的敵人經常被他們認為是](../Page/希伯來.md "wikilink")[含的後代](../Page/含.md "wikilink")。在中，閃姆的兒子是以攔（[埃蘭](../Page/埃蘭.md "wikilink")，Elam）、[亞述](../Page/亞述.md "wikilink")（Asshur）、[亞法撒](../Page/亞法撒.md "wikilink")（Arphaxad）、路德（Lud）與[亞蘭](../Page/亞蘭.md "wikilink")（亞拉姆，Aram）：被認為是[亚述人](../Page/亚述.md "wikilink")、[迦勒底人](../Page/迦勒底.md "wikilink")（新巴比伦）、塞巴人、[希伯來人與阿拉姆人的祖先](../Page/希伯來.md "wikilink")，其語言有著緊密的關係，固此他們的語言都被納入[閃語族中](../Page/閃語族.md "wikilink")。值得注意的是，[迦南人與](../Page/迦南.md "wikilink")[亞摩利人](../Page/亞摩利.md "wikilink")（古巴比伦）的語言同樣為閃族語，但在《聖經》中被認為是[含的後代](../Page/含.md "wikilink")，可能是希伯來文化的衝擊。反之，閃姆在《聖經》中同樣被認為是[埃蘭人的祖先](../Page/埃蘭.md "wikilink")，而[埃蘭語並非閃語族的一支](../Page/埃蘭語.md "wikilink")，現代研究認為有可能與[南印度](../Page/南印度.md "wikilink")[達羅毗荼語系有關](../Page/達羅毗荼語系.md "wikilink")。

## 語言

在現代語言學中，「閃米特」源自《聖經》的词汇用法，但並非等同。[語言學上](../Page/語言學.md "wikilink")，閃米特語言是[非亞語系](../Page/非亞語系.md "wikilink")
的一個子群。根據[約瑟．格林堡廣為人接受的分類](../Page/約瑟·哈羅德·格林伯格.md "wikilink")，也包括了[阿卡德語](../Page/阿卡德語.md "wikilink")、[巴比倫的古語](../Page/巴比倫.md "wikilink")、埃塞俄比亞與阿拉伯的官方語言，最廣泛的當代閃米特語，[阿拉姆語](../Page/阿拉姆語.md "wikilink")（[亞蘭語](../Page/亞蘭語.md "wikilink")，耶穌的母語）、[迦南語](../Page/迦南語.md "wikilink")、[吉茲語](../Page/吉茲語.md "wikilink")、埃塞俄比亞古卷的古語、[希伯來語](../Page/希伯來語.md "wikilink")、[腓尼基語](../Page/腓尼基語.md "wikilink")、[迦太基語](../Page/迦太基語.md "wikilink")、[南阿拉伯語](../Page/南阿拉伯語.md "wikilink")，還有今日包括[馬里語](../Page/馬里語.md "wikilink")，只有[阿拉伯半岛南部少數人還在使用的沙巴古語](../Page/阿拉伯半岛.md "wikilink")。

閃米特語多被人用作第二語言，數目遠遠高於當代的閃米特語母語使用者。現時，西方最大的幾種宗教的典籍，用的就是某幾種閃米特語，包括了[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")（[阿拉伯語](../Page/阿拉伯語.md "wikilink")），[猶太教](../Page/猶太教.md "wikilink")（[希伯來語和](../Page/希伯來語.md "wikilink")[阿拉姆語](../Page/阿拉姆語.md "wikilink")），與[基督教](../Page/基督教.md "wikilink")（阿拉姆語和[吉茲語](../Page/吉茲語.md "wikilink")）。數千萬人把它當作[第二語言來學習](../Page/第二語言.md "wikilink")（或當作他們所說的現代語言的古老版本）：有無數伊斯蘭教徒學習和背誦古阿拉伯語——《[古蘭經](../Page/古蘭經.md "wikilink")》的語言，而在以色列以外全世界的，說別種母語的猶太人，也會使用和學習希伯來語——《[摩西五經](../Page/摩西五經.md "wikilink")》的與其他重要猶太經典的語言。

值得注意的是，[柏柏尔語](../Page/柏柏尔語.md "wikilink")、[科普特语](../Page/科普特语.md "wikilink")、[古埃及语](../Page/古埃及语.md "wikilink")、[豪薩語](../Page/豪薩語.md "wikilink")、[索馬里語](../Page/索馬里語.md "wikilink")，與很多[北非與](../Page/北非.md "wikilink")[西亞地區的相關語言並非閃米特語](../Page/西亞.md "wikilink")，而是在一個更大的[亞非語系之內](../Page/亞非語系.md "wikilink")，閃米特語是當中的一個子群。其他古代或現代的西亞語言如[亞美尼亞語](../Page/亞美尼亞語.md "wikilink")、[庫德語](../Page/庫德語.md "wikilink")、[波斯語](../Page/波斯語.md "wikilink")、[土耳其語](../Page/土耳其語.md "wikilink")、古蘇美尔語和努比亞語——都不是亞非語系的一員，甚至全無關係（或者，更確切一點說，是關係更加遙遠）。[亞美尼亞語](../Page/亞美尼亞語.md "wikilink")、[庫德語和](../Page/庫德語.md "wikilink")[波斯語現時都是](../Page/波斯語.md "wikilink")[印歐語系的語言](../Page/印歐語系.md "wikilink")；而[土耳其語被視為突厥語族的一支](../Page/土耳其語.md "wikilink")。

## 宗教

宗教上，「閃米特」指與閃米特語言有關的宗教：所以猶太教、基督教與伊斯蘭教經常被引為「**閃米特宗教**」，雖然現時人們多用「亚伯拉罕諸教」來形容。一個真正全面的閃米特宗教會包括在亚伯拉罕諸教興起前在西亚流行的一些多神論的宗教（例如Adad、Hadad），因此准确地说，亚伯拉罕諸教为「**[闪米特一神诸教](../Page/闪米特一神诸教.md "wikilink")**」，属閃米特诸教，因西亚的多神論宗教大多已经消失，故常常直接称为「閃米特宗教」。

## 種族劃分

[MPP-Sm1.jpg](https://zh.wikipedia.org/wiki/File:MPP-Sm1.jpg "fig:MPP-Sm1.jpg")

在[中世紀的](../Page/中世紀.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，所有亞洲人都被認為是閃姆的後裔。直至[十九世紀](../Page/十九世紀.md "wikilink")，閃米特這個名詞仍然局限於歷史上使用閃米特語言的民族。這些人被認為是一個與別不同的種族。可是，那時有些反閃米特的種族理論家提出，閃米特人是在之前分裂的種族愈來愈模糊的區別中產生的一群。這種假定的過程被種族理論家[哥比諾稱為](../Page/哥比諾.md "wikilink")「閃米特化」。後來，納粹思想家[羅辛堡亦支持這種認為閃米特產生自種族](../Page/阿爾弗雷德·羅森堡.md "wikilink")「混亂」的觀念，

相反，現代科學以基因研究鑑定民族的共有生理遺傳，而對閃米特人的分析，顯示出他們明顯有有著同一先祖。雖然還沒有找到任何明顯的共同[粒線體](../Page/粒線體.md "wikilink")，但巴勒斯坦人、叙利亞人與猶太族裔有著非常緊密的[Y染色體連繫](../Page/Y染色體.md "wikilink")，縱使其他族群對它們有著種種的影響（見）。雖然[族群遺傳學仍然是一門新興的科學](../Page/族群遺傳學.md "wikilink")，它似乎指出了這些人中，有一部份人的祖先來自閃米特中的近東人口（雖然《聖經》中的系譜與此相異）。

## 参考文献

## 外部链接

  - [閃米特語譜系](http://www.ethnologue.com/show_family.asp?subid=89998)

## 参见

  - [闪含语系](../Page/闪含语系.md "wikilink")
  - [閃米特诸教](../Page/閃米特诸教.md "wikilink")
  - [閃姆](../Page/閃姆.md "wikilink")
  - [挪亞眾子](../Page/挪亞眾子.md "wikilink")
  - [反闪米特主义](../Page/反闪米特主义.md "wikilink")
  - [含米特人](../Page/含米特人.md "wikilink")

{{-}}

[Category:中东民族](../Category/中东民族.md "wikilink")
[Category:闪米特人](../Category/闪米特人.md "wikilink")

1.  參见：[創世記](../Page/創世記.md "wikilink")5:32,6:10,10:21