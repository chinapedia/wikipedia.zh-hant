**前田利家**（），[日本戰國時代武將](../Page/日本戰國時代.md "wikilink")。父親是[前田利昌](../Page/前田利昌.md "wikilink")，母親是長齡院，兄[前田利久](../Page/前田利久.md "wikilink")。幼名犬千代，元服後稱**前田又左衞門利家**，年輕時是著名的[傾奇者](../Page/傾奇者.md "wikilink")。[豐臣政權下](../Page/豐臣政權.md "wikilink")[五大老之一](../Page/五大老.md "wikilink")，加賀藩始祖。

## 生平

### 幼年

出生於[尾張國海东郡荒子村](../Page/尾張國.md "wikilink")（今[愛知縣](../Page/愛知縣.md "wikilink")[名古屋市](../Page/名古屋市.md "wikilink")[中川區](../Page/中川區.md "wikilink")），幼名犬千代，荒子城主[前田利昌的第四子](../Page/前田利昌.md "wikilink")，母亲是长龄院。

利家原本為織田家首席家老林秀貞的與力，1551年时开始在[织田信长麾下擔任小姓](../Page/织田信长.md "wikilink")，翌年的[萱津之战为其初阵](../Page/萱津之战.md "wikilink")，元服後改名為前田又左衞門利家，因善使長槍而有「槍之又左」的外號。弘治2年（1556年），在信長與織田信勝爭奪織田家家督的[稻生之戰中殺死信勝小姓頭宮井勘兵衛而建立功績](../Page/稻生之戰.md "wikilink")。永祿元年（1558年）與上尾張守護代主織田信安之子織田信賢爭戰建立軍功，因而擔任信長赤[母衣眾隊長](../Page/母衣.md "wikilink")，1558年时与表妹[阿松结婚](../Page/阿松.md "wikilink")。

年輕的利家脾氣不好常惹事生非，被外界認為是傾奇者，曾與信长異母弟[十阿弥發生口角而將其斩杀](../Page/十阿弥.md "wikilink")，差点因此被信长处死。后来戴罪立功才得到赦免，隨著年齡增長利家收起以往的衝動暴躁，变得稳重可靠。

利家是極少數早期就與秀吉有深交的的人，當時兩人地位都不高，但已是非常好的朋友，兩人的妻子阿松與寧寧也是感情很好的姊妹淘。

### 平步青雲

利昌过世后，由利家的长兄[前田利久继承前田家](../Page/前田利久.md "wikilink")。但9年後信長[罷黜利久](../Page/罷黜.md "wikilink")，改命利家擔任前田家[家督](../Page/家督.md "wikilink")，理由是利久体弱多病且不會打仗，利久被迫讓位後離開織田家，許多支持利久的前田家臣也因此負氣離開前田家，但利家因有信長支持故未因此元氣大傷。

元龜元年（1570年）4月，織田信長與淺井氏、朝倉氏作戰（[金崎之戰](../Page/金崎之戰.md "wikilink")）敗北撤退時擔當信長的護衛，6月[姉川之戰殺死淺井助七郎立功](../Page/姉川之戰.md "wikilink")。

天正2年（1574年）擔任[柴田勝家之與力](../Page/柴田勝家.md "wikilink")，負責鎮壓越前一向一揆，翌年9月平定越前[一向一揆後](../Page/一向一揆.md "wikilink")，前田利家与[佐佐成政](../Page/佐佐成政.md "wikilink")、[不破光治一起獲封府中](../Page/不破光治.md "wikilink")10萬石，有「府中三人衆」之稱。越前平定後協助[柴田胜家经略](../Page/柴田胜家.md "wikilink")[北陆與上杉家對戰](../Page/北陆.md "wikilink")，因功受封越前今立、南条两郡，後來更趁上杉謙信病死之際攻占不少上衫家在北陸的領地。

天正10年（1582年）爆发[本能寺之变](../Page/本能寺之变.md "wikilink")，後來在[清須會議後仍然劃分為勝家與力](../Page/清須會議.md "wikilink")。秀吉與胜家決裂開戰後，於天正11年（1583年）率兵参加[贱岳之战](../Page/贱岳之战.md "wikilink")，但是戰意消極、不斷迴避與羽柴方交戰且故意不支援苦戰的友軍導致柴田方潰敗，後來在[堀秀政的勸說下歸順秀吉](../Page/堀秀政.md "wikilink")，擔任進攻北之-{庄}-城的先鋒，勝家兵敗與阿市一起自殺。戰後领[加贺](../Page/加贺.md "wikilink")、[能登两国](../Page/能登.md "wikilink")，把根據地從小丸山城移往加賀的尾山城（金澤城）並迎回哥哥利久。

天正12年（1584年）秀吉與[德川家康爆發](../Page/德川家康.md "wikilink")[小牧長久手之戰](../Page/小牧長久手之戰.md "wikilink")，利家於末森城擊敗支持德川方的舊同僚佐佐成政，之後聯繫[上杉景勝共同夾擊越中](../Page/上杉景勝.md "wikilink")。同年8月由利家擔任先鋒、秀吉親率大軍進攻越中降伏佐佐成政（[富山之役](../Page/富山之役.md "wikilink")），戰後利家嫡子[前田利長獲封越中國](../Page/前田利長.md "wikilink")3郡共32萬石，並在征討北条家的[小田原征伐擔任北國勢的總指揮](../Page/小田原征伐.md "wikilink")｡

文祿元年（1592年）在豐臣秀吉出兵朝鮮期間，利家率兵留守肥前名護屋，與德川家康共理政務，文祿2年（1593年）1月，原本打算渡海進攻朝鮮，但後來與明朝講和後取消渡海，8月之後因秀賴誕生，隨同秀吉返回大坂城，11月返回金澤。

文祿3年（1594年）1月5日，利家、上杉景勝及[毛利輝元同日敘任從三位官居](../Page/毛利輝元.md "wikilink")，後來4月7日利家先敘任從二位[權大纳言](../Page/權大纳言.md "wikilink")。

慶長3年（1598年）4月20日將家督讓給嫡長子利長後宣布隱居並前往草津進行溫泉治療，隱居俸祿為1萬5千石。自草津返回後把心力放在大阪，用心經營前田家在豐臣政權下的政治地位，利家與秀吉共同規劃五大老、五奉行制度，利家本人位居五大老之次。

### 秀吉死後

慶長3年（1598年）8月[豐臣秀吉去世](../Page/豐臣秀吉.md "wikilink")，利家隨其子[豐臣秀賴進入大坂城](../Page/豐臣秀賴.md "wikilink")。利家成為秀賴的傅役，秀賴正式成為大坂城主，繼承豐臣家。

自此德川家康势力抬头，首先破壞秀吉的法度，分別與伊達政宗、[蜂須賀家政及](../Page/蜂須賀家政.md "wikilink")[福島正則聯婚](../Page/福島正則.md "wikilink")，利家反對並集結上杉景勝、毛利輝元、宇喜多秀家等三大老、五奉行的石田三成，以及[細川忠興](../Page/細川忠興.md "wikilink")、[淺野幸長](../Page/淺野幸長.md "wikilink")、[加藤清正及](../Page/加藤清正.md "wikilink")[加藤嘉明等武將](../Page/加藤嘉明.md "wikilink")，雙方互不相让。後來於2月2日，由利家代表四大老與五奉行，與德川家康協調並交換誓紙和解，雙方矛盾暫時壓下；但此事後利家病情惡化，在閏3月3日於[大阪城病逝](../Page/大阪城.md "wikilink")，法號「高德院傳桃雲淨見居士」，葬於野田山前田家墓地。利家是當時唯一有能力制約家康的重量級人物，他的去世讓德川家康再無忌憚，直接导致[丰臣家灭亡](../Page/丰臣家.md "wikilink")。

利家病時，家康曾往探病。利家死後，家康藉口當初去給利家探病時，懷疑利家與利長合謀暗殺自己而準備征討加賀，前田家收到風聲後，利家的夫人[阿松當機立斷](../Page/芳春院.md "wikilink")，自願前往江戶城當[政治人質](../Page/政治人質.md "wikilink")，家康也因前田家俯首稱臣而心滿意足取消攻打加賀，還把孫女[子子姬嫁給前田利家之子](../Page/子子姬.md "wikilink")[前田利光](../Page/前田利光.md "wikilink")。

## 與慶次的關係

利家與[前田慶次的關係相當糟糕](../Page/前田慶次.md "wikilink")，慶次認為養父[前田利久的前田家繼承權被利家所奪](../Page/前田利久.md "wikilink")，因此懷恨在心，利久也因此舉家離開織田家旅居京都十餘年（慶次也在京都生活期間學習風雅技能），雖然後來利久被利家請回加賀照料，但是慶次還是打從心裏不喜歡這位沒有血緣關係年紀又比自己小的叔父，兩人不論在行事風格或理念上都相去甚遠，時常發生摩擦。利久病死後，本來就討厭利家的慶次再無理由待在前田家而出奔，而利家也為此大怒揚言要殺了慶次。

慶次離開前田家時隻身一人，家中妻小都沒帶上，所以慶次長子[前田正虎繼承祖父利久的封地](../Page/前田正虎.md "wikilink")，並一路侍奉利家到利常成為加賀藩的開藩藩士。

## 系譜

  - 正室：[阿松](../Page/芳春院.md "wikilink")

<!-- end list -->

  - 側室
      - 壽福院
      - 隆興院
      - 金晴院
      - 明運院
      - 逞正院

<!-- end list -->

  - 子
      - [前田利長](../Page/前田利長.md "wikilink")
      - [前田利政](../Page/前田利政.md "wikilink")
      - 前田知好
      - [前田利常](../Page/前田利常.md "wikilink")
      - 前田利孝
      - 前田利貞

<!-- end list -->

  - 女
      - 幸姬（前田長種正室）
      - 蕭姬 (中川光重正室)
      - 摩阿姬（[豐臣秀吉側室](../Page/豐臣秀吉.md "wikilink")）
      - 豪姬（秀吉養女、[宇喜多秀家正室](../Page/宇喜多秀家.md "wikilink")）
      - 與免
      - 千世（細川忠隆正室）
      - 菊（秀吉養女）
      - 保智
      - 福
      - 他

## 家臣

  - 譜代
      - [奥村永福](../Page/奥村永福.md "wikilink")
      - [篠原一孝](../Page/篠原一孝.md "wikilink")
      - [木村三蔵](../Page/木村三蔵.md "wikilink")

<!-- end list -->

  - 荒子众
      - [村井長賴](../Page/村井長賴.md "wikilink")
      - [高畠定吉](../Page/高畠定吉.md "wikilink")

<!-- end list -->

  - 与力众
      - [岡島一吉](../Page/岡島一吉.md "wikilink")
      - [長連龍](../Page/長連龍.md "wikilink")
      - [富田重政](../Page/富田重政.md "wikilink")

## 登場作品

  - 小說

<!-- end list -->

  - 前田利家（、著）
  - 前田利家（[講談社](../Page/講談社.md "wikilink")、著）
  - 前田利家（[小学館](../Page/小学館.md "wikilink")、著）
  - 前田利家（[幻冬舍](../Page/幻冬舍.md "wikilink")、著）
  - 前田利家（[学研](../Page/学研.md "wikilink")、著）
  - 前田利家：秀吉最倚賴的男人（[PHP研究所](../Page/PHP研究所.md "wikilink")、著）
  - 前田利家與妻阿松：築起「加賀百萬石」的二人三脚 （PHP研究所、著）
  - 百萬石異聞：前田利家與松 （、野村昭子著）
  - 前田利家物語：加賀百萬石之祖（北國出版社、北村魚泡洞著）

<!-- end list -->

  - 影视剧

<!-- end list -->

  - 出世太閤記（1938年、日活、演：尾上菊太郎）

  - [織田信長](../Page/:JA:織田信長_\(映画\).md "wikilink")（1940年、日活、演：）

  - （1953年、大映、演：）

  - （1955年、東映、演：）

  - 太閤記（1958年、松竹、演：）

  - （1959年、MBS、演：西村幹雄）

  - 德川家康（1964年、NET、演：）

  - （1964年、東宝、演：）

  - [太閤記](../Page/:JA:太閤記_\(NHK大河ドラマ\).md "wikilink")（1965年、[NHK大河劇](../Page/NHK大河劇.md "wikilink")、演：）

  - 戰國太平記 真田幸村（1966年、TBS、演：）

  - （1969年、ABC、演：）

  - （1969年、ABC、演：）

  - （1970年、NTV、演：[森次浩司](../Page/森次晃嗣.md "wikilink")）

  - （1970年、KTV、演：）

  - （1971年、NHK大河劇、演：）

  - （1973年、NHK大河劇、演：）

  - （1973年、NET、演：）

  - （1978年、NHK大河劇、演：）

  - [女太閤記](../Page/女太閤記.md "wikilink")（1981年、NHK大河劇、演：）

  - [關原](../Page/:JA:関ヶ原_\(テレビドラマ\).md "wikilink")（1981年、TBS、演：）

  - [德川家康](../Page/德川家康_\(大河劇\).md "wikilink")（1983年、NHK大河劇、演：）

  - [真田太平記](../Page/:JA:真田太平記_\(テレビドラマ\).md "wikilink")（1985年、NHK水曜時代劇、演：）

  - [太閤記](../Page/:JA:太閤記_\(1987年のテレビドラマ\).md "wikilink")（1987年、TBS、演：）

  - [獨眼龍政宗](../Page/獨眼龍政宗.md "wikilink")（1987年、NHK大河劇、演：）

  - [武田信玄](../Page/武田信玄_\(大河劇\).md "wikilink")（1988年、NHK大河劇、演：瀧口剛）

  - [德川家康](../Page/:JA:徳川家康_\(1988年のテレビドラマ\).md "wikilink")（1988年、TBS、演：）

  - [織田信長](../Page/:JA:織田信長_\(1989年のテレビドラマ\).md "wikilink")（1989年、TBS、演：）

  - [春日局](../Page/春日局_\(大河劇\).md "wikilink")（1989年、NHK大河劇、演：嶺田則夫）

  - [信長KING OF
    ZIPANGU](../Page/信長KING_OF_ZIPANGU.md "wikilink")（1992年、NHK大河劇、演：）

  - 德川家康 戰國最後的勝利者（1992年、ANB、演：）

  - （1993年、TBS、演：）

  - 獨眼龍的野望 伊達政宗（1993年、ANB、演：[北大路欣也](../Page/北大路欣也.md "wikilink")）

  - [織田信長](../Page/:JA:織田信長_\(1994年のテレビドラマ\).md "wikilink")（1994年、TX、演：）

  - （1995年、TX、演：）

  - [秀吉](../Page/秀吉_\(大河劇\).md "wikilink")（1996年、NHK大河劇、演：）

  - （1999年、NHK、演：[原田芳雄](../Page/原田芳雄.md "wikilink")）

  - [梟之城](../Page/梟之城#梟之城_owl's_castle.md "wikilink")（1999年、東寶、演：）

  - [葵德川三代](../Page/葵德川三代.md "wikilink")（2000年、NHK大河劇、演：）

  - [利家與松](../Page/利家與松.md "wikilink")（2002年、NHK大河劇、演：[唐澤壽明](../Page/唐澤壽明.md "wikilink")）

  - （2003年、CX、演：[小原雅人](../Page/小原雅人.md "wikilink")）

  - （2006年、EX、演：）

  - [功名十字路](../Page/功名十字路_\(大河劇\).md "wikilink")（2006年、NHK大河劇、演：[唐澤壽明](../Page/唐澤壽明.md "wikilink")）

  - （2006年、EX、演：）

  - [寧寧：女太閤記](../Page/寧寧：女太閤記.md "wikilink")（2009年、TX、演：[原田泰造](../Page/原田泰造.md "wikilink")）

  - [天地人](../Page/天地人_\(大河劇\).md "wikilink")（2009年、NHK大河劇、演：[宇津井健](../Page/宇津井健.md "wikilink")）

  - [江\~公主們的戰國\~](../Page/江~公主們的戰國~.md "wikilink")（2011年、NHK大河劇、演：）

  - [女信長](../Page/女信長.md "wikilink")（2013年、CX、演：）

  - [清須會議](../Page/:JA:清須会議_\(小説\)#映画.md "wikilink")（2013年、東寶、演：[淺野忠信](../Page/淺野忠信.md "wikilink")）

  - [軍師官兵衛](../Page/軍師官兵衛.md "wikilink") （2014年、NHK大河劇、演：）

  - [信長協奏曲](../Page/信長協奏曲#電視劇.md "wikilink")（2014年、CX、演：[藤谷太輔](../Page/藤谷太輔.md "wikilink")）

  - （2016年、TX、演：）

  - [真田丸](../Page/真田丸_\(大河劇\).md "wikilink")（2016年、NHK大河劇、演：）

  - （2017年、東映、演：[佐佐木藏之介](../Page/佐佐木藏之介.md "wikilink")）

  - [關原](../Page/:JA:関ヶ原_\(映画\).md "wikilink")（2017年、東寶、演：）

<!-- end list -->

  - 漫畫

<!-- end list -->

  - 前田利家（、[永井豪作](../Page/永井豪.md "wikilink")）
  - 利家與松（講談社、立木美和作）

## 资料来源

1.  《前田利家》作者：津本阳 出版社：讲谈社出版
2.  [前田利家](http://www.sekigun.com/history/yuanchuang/qtlj-new.htm)

<references/>

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**前田利家** |-

[Category:戰國大名](../Category/戰國大名.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Toshiie](../Category/加賀前田氏.md "wikilink")
[Category:1539年出生](../Category/1539年出生.md "wikilink")
[Category:1599年逝世](../Category/1599年逝世.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")
[Category:日本雙性戀者](../Category/日本雙性戀者.md "wikilink")
[Category:日本人物神](../Category/日本人物神.md "wikilink")
[Category:織田信長](../Category/織田信長.md "wikilink")