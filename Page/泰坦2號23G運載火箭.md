**泰坦23G運載火箭**由[格倫·L·馬丁公司](../Page/格倫·L·馬丁公司.md "wikilink")（Lockheed
Martain）建造，發射地點為加利福尼亞州的范登堡空軍基地（Vandenberg）。泰坦23G運載火箭使用數位電腦及慣性指引引導，承包商為迪洛克電機（Delco）；衛星酬載艙長6.1[公尺](../Page/公尺.md "wikilink")、[直徑](../Page/直徑.md "wikilink")3[公尺](../Page/公尺.md "wikilink")，由[波音公司承包](../Page/波音公司.md "wikilink")；外殼及股價建造為三扇企劃（Tri-Sector）承包；業泰引擎為氣體噴射工業設備公司改良，泰坦二號運載火箭之洲際飛彈引擎，第一枚泰坦23G運載火箭於1988年研發完成。

[Category:運載火箭](../Category/運載火箭.md "wikilink")
[Category:化學火箭](../Category/化學火箭.md "wikilink")
[Category:一次性火箭](../Category/一次性火箭.md "wikilink")
[Category:泰坦運載火箭](../Category/泰坦運載火箭.md "wikilink")